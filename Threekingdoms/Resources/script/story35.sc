
// PART I
// 开启屏幕上下的黑边
MovieStart()

// 为剧情，清除场景
ClearScene()

// 创建角色
// dir参数的说明  up: 0, left: 1, down: 2, right: 3
CreateNPC(1, animation/generals/zsgg_cwj, 864, 816, 圣女娘娘, 2)
CreateNPC(2, animation/generals/zsgg_mc, 1120, 848, 苍龙, 1)
MoveCam(1120, 848, 1000)
Wait(1000)

AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.1, 1, 1)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.9, 0.9, 2)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.1, 0.1, 3)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.9, 0.1, 4)

Dialog(不要管我了，我已身受魔星的九星诛阵之伤，命不久矣。, general/machao.png, 0,苍龙)
MoveNPC(1, 992, 880, 1200)

Wait(1200)

Dialog(我是断不会放弃于你的，我定会想办法治愈你的 , general/caiwenji.png, 0,圣女娘娘)
Dialog(若要治愈此伤，只能将你多年修炼的五颗灵珠传与我。可是一旦如此你必死无疑，我不会让你这么做!, general/machao.png, 0,苍龙)
Dialog(你还有拯救苍生的使命，而我当圣女太久了，久到也该歇歇了。, general/caiwenji.png, 0,圣女娘娘)
PlayAct(1, 2)
SetSE(animation/texiao/renwutexiao/BZ2/bz2.anm, 1120, 848)
Wait(500)


// 开启黑屏
BlackScreenStart()
// 屏幕中心出现简单对话
SimpleDialog(/#fefe33 眼前渐渐模糊……/,500)
// 关闭黑屏
BlackScreenEnd()
DeleteNPC(1)
DeleteNPC(2)
RemoveScreenEffect(1)
RemoveScreenEffect(2)
RemoveScreenEffect(3)
RemoveScreenEffect(4)

//Wait(1000)

// 剧情结束，恢复为正常场景
ResumeScene()
Wait(500)
// 关闭屏幕上下的黑边
MovieEnd()
