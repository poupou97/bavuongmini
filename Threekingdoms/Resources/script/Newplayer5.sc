// 开启屏幕上下的黑边
MovieStart()

// 为剧情，清除场景
ClearScene()


//=================================张角受伤========================================
// dir参数的说明  up: 0, left: 1, down: 2, right: 3
CreateNPC(3, animation/generals/zsgg_zgl, 940, 651, 诸葛亮, 3)
CreateNPC(4, animation/generals/zsgg_zhangjiao, 1178, 664, 张角, 1)
CreateNPC(hero,hero,1023,700,hero,3)
//=================掠阵武将================

CreateNPC(7, animation/generals/zsgg_gy, 841, 715, 关羽, 3)
CreateNPC(8, animation/generals/zsgg_lbu, 970, 747, 吕布, 3)
CreateNPC(9, animation/generals/zsgg_zy, 850, 592, 赵云, 3)
CreateNPC(10, animation/generals/zsgg_dc, 890, 809, 貂蝉, 3)

Wait(1500)

Dialog(今日算你们厉害。出来混，迟早要还的。/#ff2a2a 苍天已死，黄天当立！/,general/zhangjiao.png, 0,张角)

FlyNPC(4,1490,507,1000)
DeleteNPC(4)
Wait(1500)
Say(3,穷寇莫追！,2500)
Wait(1000)
SetDir(hero,1)
Wait(1000)

Dialog(多谢众英雄相救，敢问高姓大名？, hero, 1,hero)
Dialog(我等皆路过此地，素未谋面，不知彼此姓名，听闻妖人作乱，自发前来相助。 ,general/zhaoyun.png, 0,赵云)
Dialog(举手之劳，何必留名！如若有缘，他日必定相见！ ,general/guanyu.png, 0,关羽)
Dialog(今日与众英雄一战，真是痛快！我决定不考公务员啦，期待日后能与众英雄相遇，共闯天下！今日就此别过，/#ff2a2a 后会有期/！, hero, 1,hero)

Say(3,后会有期,2500)
Say(7,后会有期,2500)
Say(8,后会有期,2500)
Say(9,后会有期,2500)
Say(10,后会有期,2500)

Wait(3000)

// BlackScreenStart(2500)
// SimpleDialog(多年以后,1000)

DeleteNPC(3)
DeleteNPC(7)
DeleteNPC(8)
DeleteNPC(9)
DeleteNPC(10)
ResumeScene()
MovieEnd()