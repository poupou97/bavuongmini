
// PART I
// 开启屏幕上下的黑边
MovieStart()

// 为剧情，清除场景
ClearScene()


MoveCam(2592, 432, 1000)
Wait(1000)
// 创建角色
// dir参数的说明  up: 0, left: 1, down: 2, right: 3
CreateNPC(1, animation/generals/zsgg_dq, 2656, 432, 左慈仙子, 1)
CreateNPC(2, animation/npc/zsgn_NewbieGuide, 2592, 368, 于吉仙子, 0)
CreateNPC(3, animation/npc/zsgn_cz, 2528, 432, 南华仙翁, 0)

AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.1, 1, 1)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.9, 0.9, 2)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.1, 0.1, 3)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.9, 0.1, 4)
Dialog(南华老头，你终于出现了，我们还以为你要把这个烂摊子全都丢给我们姐妹了呢。, general/daqiao.png, 0,左慈仙子)
Dialog(非也非也，我只是在潜心寻觅对付魔星之法。, general/cz.png, 0,南华仙翁)
Dialog(如今我们找到了可以拯救苍生的人，只是他的仙缘还没开启，所以还需要再历练一些时日。, general/newbieguide.png, 0,于吉仙子)
Dialog(我们姐妹暂时还不能离开碧落仙境，指引之事都劳烦仙翁了。, general/newbieguide.png, 0,于吉仙子)
Dialog(你们放心吧，我会指引他做他该做的事情。, general/cz.png, 0,南华仙翁)
MoveCam(2336, 1040, 1000)
Wait(1000)
DeleteNPC(1)
DeleteNPC(2)
DeleteNPC(3)

RemoveScreenEffect(1)
RemoveScreenEffect(2)
RemoveScreenEffect(3)
RemoveScreenEffect(4)



//Wait(1000)

// 剧情结束，恢复为正常场景
ResumeScene()
Wait(500)
// 关闭屏幕上下的黑边
MovieEnd()
