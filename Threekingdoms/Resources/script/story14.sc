
// PART I
// 开启屏幕上下的黑边
MovieStart()

Dialog(人生匆匆数十年，找个自己喜欢的事业是很重要的，能有一位伯乐更重要。, general/simayi.png, 0,司马懿)
Dialog(现在诸侯争霸，已经有很多人来找过我，但是能够找到真正的明主才是王道啊。 , general/simayi.png, 1,司马懿)
Dialog(是啊，司马先生，曹将军的确是真心实意想要先生帮助，希望先生能够答应。, hero, 0,hero)
Dialog(争夺天下，自然每个人都是真心实意。, general/simayi.png, 1,司马懿)
Dialog(那先生究竟怎样才肯助我完成大业呢？, general/caocao.png, 0,曹操)
Dialog(这个世界从来都是胜者为王，只要战胜了我，我自然相信你是能够统一天下的明主！, general/simayi.png, 1,司马懿)
Dialog(没问题，司马先生，那就得罪了！, hero, 0,hero)
// 开启黑屏
BlackScreenStart()
// 屏幕中心出现简单对话
SimpleDialog(/#ff0000 经过了一场恶战！/,500)
// 关闭黑屏
BlackScreenEnd()
Dialog(你们……竟然如此厉害，也罢，这也许都是天意吧！一言既出，驷马难追，我会跟你们走的！, general/simayi.png, 1,司马懿)
//Wait(1000)

// 关闭屏幕上下的黑边
MovieEnd()
