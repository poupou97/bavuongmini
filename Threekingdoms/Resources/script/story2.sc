
// PART I
// 开启屏幕上下的黑边
MovieStart()

// 为剧情，清除场景
ClearScene()

// 创建角色
// dir参数的说明  up: 0, left: 1, down: 2, right: 3

SetDir(hero, 3)

CreateNPC(2, animation/generals/zsgg_zf, 672, 400, 张飞, 2)

CreateNPC(4, animation/generals/zsgg_lbu, 992, 656, 吕布, 1)
MoveNPC(2, 800, 528, 2000)
MoveNPC(4, 928, 624, 2000)
MoveCam(864, 560, 2000)
Wait(2000)
AddScreenEffect(animation/texiao/particledesigner/huo2.plist, 0.3, 0, 1)
AddScreenEffect(animation/texiao/particledesigner/huo2.plist, 0.7, 0, 2)
Dialog(吕布小儿，受死吧！, general/zhangfei.png, 0,张飞)


// 2: attack action
// 张飞
PlayAct(2, 2)
SetSE(animation/texiao/renwutexiao/AX1/ax1.anm, 928, 625)
Wait(800)


// 吕布
Say(4, 哼，雕虫小技，也敢在我面前卖弄！, 1800)
Wait(1800)
PlayAct(4, 2)
SetSE(animation/texiao/renwutexiao/AX3/ax3.anm, 800, 529)
Wait(1000)
SetSE(animation/texiao/renwutexiao/CM_CXSH/cm_cxsh.anm, 800, 529)
Wait(700)
SetSE(animation/texiao/renwutexiao/CM_CXSH/cm_cxsh.anm, 800, 529)
Wait(700)
Dialog(可恶，动弹不得！, general/zhangfei.png, 0,张飞)
//Wait(1500)
CreateNPC(1, animation/generals/zsgg_lb, 736, 368, 刘备, 2)
CreateNPC(3, animation/generals/zsgg_gy, 608, 432, 关羽, 2)
MoveCam(672, 400, 1000)
Wait(1000)
Dialog(快看，张大哥在那！, hero, 0,hero)
Dialog(吕布那厮，休要伤我三弟！, general/guanyu.png, 0,关羽)
Dialog(三弟速撤到我身后！, general/liubei.png, 0,刘备)
Dialog(吕布，你的死期到了！, general/zhangfei.png, 0,张飞)
MoveNPC(1, 800, 528, 1800)
MoveNPC(2, 800, 432, 1300)
MoveNPC(3, 672, 528, 1800)
Wait(1800)
MoveNPC(1, 928, 592, 1300)
MoveNPC(2, 928, 528, 1300)
MoveNPC(3, 800, 592, 1300)
MoveNPC(4, 1056, 688, 1300)
MoveCam(928, 592, 1300)
Wait(1500)

// 吕布
Say(4, 高顺、陈宫何在？, 1800)
Wait(1800)

CreateNPC(5, animation/monster/zsgr_ss, 1056, 752, 陈宫, 0)

CreateNPC(6, animation/monster/zsgr_tq, 1184, 688, 高顺, 0)
Wait(1300)
MoveNPC(4, 992, 656, 500)
Wait(1000)
PlayAct(4, 2)
SetSE(animation/texiao/renwutexiao/AX3/ax3.anm, 928, 593)
Wait(1000)
PlayAct(5, 2)
SetSE(animation/texiao/renwutexiao/BY3/by3.anm, 800, 593)
Wait(1000)
PlayAct(6, 2)
SetSE(animation/texiao/renwutexiao/AX3/ax3.anm, 928, 529)
Wait(1000)


Say(1, 兄弟齐心，其利断金！, 1400)
Say(2, 兄弟齐心，其利断金！, 1400)
Say(3, 兄弟齐心，其利断金！, 1400)
Wait(1400)

PlayAct(2, 2)
SetSE(animation/texiao/renwutexiao/AY1/ay1.anm, 1184, 689)

PlayAct(3, 2)
SetSE(animation/texiao/renwutexiao/AX3/ax3.anm, 1056, 753)

PlayAct(1, 2)
SetSE(animation/texiao/renwutexiao/DX3/dx3_1.anm, 992, 657)
Wait(1200)

Dialog(嘶~哪里冒出的这三人，竟然有些本事！, general/lvbu.png, 1,吕布)
Dialog(主公，形势于我们不利，不宜恋战。我们还是速速撤退吧。, general/guojia.png, 1,陈宫)

Wait(300)

RemoveScreenEffect(1)
RemoveScreenEffect(2)

DeleteNPC(1)
DeleteNPC(2)
DeleteNPC(3)
DeleteNPC(4)
DeleteNPC(5)
DeleteNPC(6)

MoveCam(-1,-1,1000)

Wait(1000)

ResumeScene()

// 关闭屏幕上下的黑边
MovieEnd()
