
// PART I
// 开启屏幕上下的黑边
MovieStart()

// 为剧情，清除场景
ClearScene()

// 创建角色
// dir参数的说明  up: 0, left: 1, down: 2, right: 3
CreateNPC(1, animation/generals/zsgg_lb, 1056, 560, 刘备, 2)
CreateNPC(2, animation/generals/zsgg_zf, 675, 622, 张飞, 3)
CreateNPC(3, animation/generals/zsgg_gy, 1056, 813, 关羽, 1)

//Dialog(来战个痛快!, general/zhangfei.png, 0,张飞)
//Dialog(来战个痛快!, general/guanyu.png, 1,关羽)
MoveCam(651, 811, 2500)
MoveNPC(2, 498, 798, 1500)
MoveNPC(3, 741, 826, 2500)
Wait(1500)
MoveNPC(2, 574, 801, 1000)
Wait(1000)

Say(2, 承让, 1000)
Say(3, 承让, 1000)
Wait(1000)

// 2: attack action
// 张飞
PlayAct(2, 2)
SetSE(animation/texiao/renwutexiao/AX1/ax1.anm, 741, 827)
Say(2, 看招, 500)
Wait(500)

// 关羽
Say(3, 力道不小啊，看我的, 500)
PlayAct(3, 2)
SetSE(animation/texiao/renwutexiao/AX3/ax3.anm, 574, 802)
Wait(500)

// 张飞
PlayAct(2, 2)
SetSE(animation/texiao/renwutexiao/AY1/ay1.anm, 741, 827)
Say(2, 看招, 500)

// 关羽
PlayAct(3, 2)
SetSE(animation/texiao/renwutexiao/AZ2/az2.anm, 574, 802)
Say(3, 看招, 500)
Wait(500)

Dialog(哈哈，真痛快!, general/zhangfei.png, 0,张飞)
Dialog(哈哈，好久没碰到这样的对手了!, general/guanyu.png, 1, 关羽)

// 剧情结束，恢复为正常场景
ResumeScene()
Wait(250)
DeleteNPC(1)
DeleteNPC(2)
DeleteNPC(3)

// 关闭屏幕上下的黑边
MovieEnd()
