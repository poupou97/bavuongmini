
// PART I
// 开启屏幕上下的黑边
MovieStart()

Dialog(王郡守也要来阻拦关某么？, general/guanyu.png, 0,关羽)
Dialog(关将军义薄云天，王某甚是钦佩，曹将军也下令不许我们为难关将军，我是来迎接关将军出关的！, general/xiahouyuan.png, 1,王植)
Dialog(这么说来，倒是我误解曹将军和王郡守了。言语上多有得罪还望担待！我们着急赶路，还请王郡守放行。, general/guanyu.png, 0,关羽)
Dialog(关将军这一路车马劳顿，就在这里多留一晚，让我也尽一下地主之谊。, general/xiahouyuan.png, 1,王植)
Dialog(打了一路，突然碰到这个不打的，我还稍微有点不适应。无事献殷勤，非奸即盗。, hero, 1,hero)
Dialog(你给的蒙汗药没有问题吧？, general/xiahouyuan.png, 1,王植)
Dialog(我的蒙汗药一滴就能放倒一头水牛！我在关羽的酒里放了一瓶。这剂量已经足够他睡到明年冬天了。, general/zhanghe.png, 0,刺客)
Dialog(这个王植果然不安好心！, hero, 1,hero)

//Wait(1000)

// 关闭屏幕上下的黑边
MovieEnd()

