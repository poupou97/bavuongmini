//
//  ExtenalClass.m
//  Threekingdoms
//
//  Created by 刘亮 on 14-4-30.
//
//

#import "ExtenalClass.h"
#import "SetUI.h"
#import "GoldStoreUI.h"

#import "../Classes/login_state/Login.h"
#import "../Classes/login_state/LoginState.h"
#import "../Classes/messageclient/GameMessageProcessor.h"

#include "../Classes/GameView.h"


static ExtenalClass *sharedObj = nil;

static int s_nCharge = 0;

//static NSString *s_guestAccount = nil;
//
//static long long s_payNum = 0;
//
//static NSString *s_nsMacStr = nil;

//static int s_nMachineType = 0;

@implementation ExtenalClass

+(ExtenalClass*) sharedIntance
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            [[self alloc] init];
        }
    }
    return sharedObj;
}

+(id) allocWithZone:(struct _NSZone *)zone
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            sharedObj = [super allocWithZone:zone];
            return sharedObj;
        }
    }
    
    return nil;
}

-(id) copyWithZone:(NSZone *)zone
{
    return self;
}

-(id) retain
{
    return self;
}

-(unsigned) retainCount
{
    return UINT_MAX;
}

-(oneway void) release
{
    
}

-(id) autorelease
{
    return self;
}

-(id) init
{
    @synchronized(self)
    {
        [super init];
        return self;
    }
}

+(int) get_Charge
{
    return s_nCharge;
}

+(void) set_Charge:(int)nCharge
{
    s_nCharge = nCharge;
}

//+(long long) get_PayNum
//{
//    return s_payNum;
//}
//
//+(void) set_PayNum:(long long)tmpPayNum
//{
//    s_payNum = tmpPayNum;
//}
//
//+(NSString *) get_macStr
//{
//    return s_nsMacStr;
//}
//
//+(void) set_macStr:(NSString *)nsMac
//{
//    s_nsMacStr = nsMac;
//}


///////////////////////////////////////////////

+(void)messageBox:(NSString*)stringTip
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:stringTip
													message:nil
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"确定", nil];
	[alert show];
	[alert release];
}

//字符串登录成功回调【实现其中一个就可以】
- (void)ppLoginStrCallBack:(NSString *)paramStrToKenKey
{
    NSString * nsUserName = [[PPAppPlatformKit sharedInstance] currentUserName];
    
    const char * charUserName = [nsUserName UTF8String];
    const char * charToken = [paramStrToKenKey UTF8String];
    
    Login::getInstance()->getLoginInfo(charUserName, charToken);
    
    // 提示信息
//    NSString * strLoginTip = @"登录中，请稍后...";
//    
//    const char * charLoginTip = [strLoginTip UTF8String];
//    
//    GameView::getInstance()->showAlertDialog(charLoginTip);
    
}

//关闭WEB页面回调方法
- (void)ppCloseWebViewCallBack:(PPWebViewCode)paramPPWebViewCode
{
    //可根据关闭的WEB页面做你需要的业务处理
//    NSLog(@"当前关闭的WEB页面回调是%d", paramPPWebViewCode);
//    _showLabelForPropView.text = [NSString stringWithFormat:@"当前关闭的WEB页面回调是%d", paramPPWebViewCode];
//    _showLabelForBgloginImageView.text = [NSString stringWithFormat:@"当前关闭的WEB页面回调是%d", paramPPWebViewCode];
}

//兑换回调接口【只有兑换会执行此回调】
- (void)ppPayResultCallBack:(PPPayResultCode)paramPPPayResultCode
{
    NSLog(@"兑换回调返回编码%d",paramPPPayResultCode);
    //回调购买成功。其余都是失败
    //_showLabelForPropView.text = [NSString stringWithFormat:@"兑换回调返回编码%d",paramPPPayResultCode];
    //_showLabelForBgloginImageView.text = [NSString stringWithFormat:@"兑换回调返回编码%d",paramPPPayResultCode];
    if(paramPPPayResultCode == PPPayResultCodeSucceed)
    {
        //购买成功发放道具
        // 提示信息
        NSString * strLoginTip = @"购买成功，发放道具";
        
        const char * charLoginTip = [strLoginTip UTF8String];
        
        GameView::getInstance()->showAlertDialog(charLoginTip);
    }
    else
    {
        NSString * strLoginTip = @"购买失败";
        
        const char * charLoginTip = [strLoginTip UTF8String];
        
        GameView::getInstance()->showAlertDialog(charLoginTip);
    }
}

//注销回调方法
- (void)ppLogOffCallBack
{
//    NSLog(@"注销的回调");
//    _showLabelForPropView.text = @"注销的回调";
//    _showLabelForBgloginImageView.text = @"注销的回调";
//    _showLabelForPropView.text = @"注销的回调";
//    _showLabelForBgloginImageView.text = @"注销的回调";
//    [bgGanmeCenterImageView setHidden:YES];
//    [bgloginImageView setHidden:NO];
    
    // 注销账号的通知
    cocos2d::CCUserDefault::sharedUserDefault()->flush();
    GameMessageProcessor::sharedMsgProcessor()->sendReq(1002);
}

//关闭客户端页面回调方法
-(void)ppClosePageViewCallBack:(PPPageCode)paramPPPageCode
{
//    //可根据关闭的VIEW页面做你需要的业务处理
//    NSLog(@"当前关闭的VIEW页面回调是%d", paramPPPageCode);
//    _showLabelForPropView.text = [NSString stringWithFormat:@"当前关闭的VIEW页面回调是%d", paramPPPageCode];
//    _showLabelForBgloginImageView.text = [NSString stringWithFormat:@"当前关闭的VIEW页面回调是%d", paramPPPageCode];
}

-(void)ppVerifyingUpdatePassCallBack
{
//    NSLog(@"验证游戏版本完毕回调");
//    _showLabelForPropView.text = @"验证游戏版本完毕回调";
//    _showLabelForBgloginImageView.text = @"验证游戏版本完毕回调";
//    [[PPAppPlatformKit sharedInstance] showLogin];
}



@end

