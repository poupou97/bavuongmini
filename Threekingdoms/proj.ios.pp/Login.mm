#include "Login.h"
#include <string>

#include "../Classes/login_state/LoginState.h"
#include "../Classes/GameView.h"
#include "../Classes/messageclient/protobuf/LoginMessage.pb.h"
#include "../Classes/messageclient/reqsender/ReqSenderProtocol.h"
#include "../Classes/ui/extensions/RichTextInput.h"
#include "SimpleAudioEngine.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/AppMacros.h"
#include "../Classes/GameUserDefault.h"
#include "../Classes/legend_engine/CCLegendAnimation.h"
#include "GameAudio.h"
#include "../Classes/utils/GameUtils.h"
#include "../Classes/login_state/MoutainBackGroundLayer.h"

#include "../Classes/messageclient/GameMessageProcessor.h"
#include "../Classes/messageclient/ClientNetEngine.h"

#include <PPAppPlatformKit/PPAppPlatformKit.h>
#import "ExtenalClass.h"

#define IS_CLEAR_ENABLE 0

using namespace CocosDenshion;

USING_NS_CC;
USING_NS_CC_EXT;

ServerListRsp* Login::m_pServerListRsp = new ServerListRsp();
long long Login::userId = 1;
string Login::sessionId = "";
//std::string Login::s_loginserver_ip = THREEKINGDOMS_LOGINSERVER_OFFICIAL;
std::string Login::s_loginserver_ip = THREEKINGDOMS_LOGINSERVER_WANMEI2;

Login * Login::s_login = NULL;

Login::Login() 
: m_labelStatusCode(NULL)
{
	//stop the backgroundmusic when the Socket Closed         by:liutao  2014-1-23
	//SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	SimpleAudioEngine::sharedEngine()->stopAllEffects();
	GameUtils::playGameSound(MUSIC_KING, 1, true);

    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

	GameView::getInstance()->ResetData();

    const int MARGIN = 40;
    const int SPACE = 35;

	// game logo
	/*CCSprite* pSprite = CCSprite::create("res_ui/first/first_di.png");
	pSprite->setPosition(ccp(winSize.width/2, winSize.height/2));
	pSprite->setAnchorPoint(ccp(0.5f, 0.5f));
	if(((float)winSize.width/(float)854) > ((float)winSize.height/(float)480))
	{
		pSprite->setScale((float)winSize.width/(float)854);
	}
	else
	{
		pSprite->setScale((float)winSize.height/(float)480);
	}
	addChild(pSprite, 0);
	*/

	CCLayer* pBackGround = MoutainBackGroundLayer::create();
	addChild(pBackGround);

	//gamelogo
	CCSprite* pSpriteLogo = CCSprite::create("res_ui/select_the_sercer/gamelogo.png");
	pSpriteLogo->setPosition(ccp(winSize.width/2+10, winSize.height/2+100));
	pSpriteLogo->setAnchorPoint(ccp(0.5f, 0.5f));
	addChild(pSpriteLogo, 1);

	// game code version
	CCLabelTTF * labelVersion = CCLabelTTF::create(GAME_CODE_VERSION, APP_FONT_NAME, 20);
	ccColor3B blackColor = ccc3(0,0,0);   // black
	labelVersion->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, blackColor);
	labelVersion->setAnchorPoint(ccp(0, 1));
	labelVersion->setPosition(ccp(3, winSize.height));
	addChild(labelVersion);

	// game res version
	int resVersion = CCUserDefault::sharedUserDefault()->getIntegerForKey(KEY_OF_RES_VERSION, GAME_RES_VERSION);
	char resVerStr[20];
	sprintf(resVerStr, "res version: %d", resVersion);
	labelVersion = CCLabelTTF::create(resVerStr, APP_FONT_NAME, 20);
	labelVersion->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, blackColor);
	labelVersion->setAnchorPoint(ccp(0, 1));
	labelVersion->setPosition(ccp(3, winSize.height - 25));
	addChild(labelVersion);

	// reset the delta time for smooth particle animation
	// because the previous scene is a loading scene, the delta time is stop when loading resource
	CCDirector::sharedDirector()->setNextDeltaTimeZero(true);

	UILayer* layer= UILayer::create();

	// Create the button
    cocos2d::extension::UIButton *button = cocos2d::extension::UIButton::create();
    button->setTouchEnable(true);
    button->loadTextures("res_ui/button_di_1.png", "res_ui/button_di_1.png", "");
    //button->setPosition(ccp(winSize.width / 2.0f, winSize.height / 2.0f));
	//button->setAnchorPoint(ccp(0.5,0.5));
	button->setPosition(ccp(0,-150));
	button->addReleaseEvent(this, coco_releaseselector(Login::onMenuGetLoginClicked));
	button->setPressedActionEnabled(true);
	
	cocos2d::extension::UIImageView * loginText = cocos2d::extension::UIImageView::create();
	loginText->setTexture("res_ui/first/button_denglu.png");
	loginText->setVisible(true);
	//loginText->setAnchorPoint(ccp(0,0));
	//loginText->setPosition(ccp(2,3));
	button->addChild(loginText);

	cocos2d::extension::UIButton *buttonRegister = cocos2d::extension::UIButton::create();
    buttonRegister->setTouchEnable(true);
    buttonRegister->loadTextures("res_ui/button_di_1.png", "res_ui/button_di_1.png", "");
	//buttonRegister->setPosition(ccp(50,-150));
	buttonRegister->addReleaseEvent(this, coco_releaseselector(Login::onMenuGetRegisterClicked));
	buttonRegister->setPressedActionEnabled(true);

	cocos2d::extension::UIImageView * registerImage = cocos2d::extension::UIImageView::create();
	registerImage->setTexture("res_ui/first/button_zhuce.png");
	registerImage->setVisible(true);
	buttonRegister->addChild(registerImage);

#if IS_CLEAR_ENABLE
	// clear account and password
	UIButton *buttonClear = UIButton::create();
    buttonClear->setTouchEnable(true);
    buttonClear->loadTextures("res_ui/new_button_7.png", "res_ui/new_button_7.png", "");
	buttonClear->setPosition(ccp(150,-50));
	buttonClear->addReleaseEvent(this, coco_releaseselector(Login::onMenuClear));
	buttonClear->setPressedActionEnabled(true);

	UIImageView * deleteImage = UIImageView::create();
	deleteImage->setTexture("res_ui/liaotian/delete.png");
	deleteImage->setVisible(true);
	buttonClear->addChild(deleteImage);
	layer->addWidget(buttonClear);
#endif

    layer->addWidget(button);
	//layer->addWidget(buttonRegister);
	layer->setPosition(ccp(winSize.width/2,winSize.height/2));
	addChild(layer);

	ccColor3B shadowColor = ccc3(0,0,0);   // black

	CCLabelTTF * account = CCLabelTTF::create("账号", APP_FONT_NAME, 20);
	account->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	account->setPosition(ccp(-130,-30));
	account->setColor(ccc3(255,255,255));
	account->enableShadow(CCSizeMake(2.0, -2.0), 0.5, 0.0, shadowColor);
	//layer->addChild(account);

	CCLabelTTF * password = CCLabelTTF::create("密码", APP_FONT_NAME, 20);
	password->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	password->setPosition(ccp(-130,-80));
	password->setColor(ccc3(255,255,255));
	password->enableShadow(CCSizeMake(2.0, -2.0), 0.5, 0.0, shadowColor);
	//layer->addChild(password);
	
	cocos2d::extension::UIImageView *inputBox = cocos2d::extension::UIImageView::create();
	inputBox->setTexture("res_ui/liaotian/shurukuang.png");
	inputBox->setAnchorPoint(ccp(0.5f,0.5f));
	inputBox->setPosition(ccp(0,-30));
	inputBox->setScale9Enable(true);
	inputBox->setScale9Size(CCSizeMake(200,34));
	//layer->addWidget(inputBox);

	cocos2d::extension::UIImageView *inputPin = cocos2d::extension::UIImageView::create();
	inputPin->setTexture("res_ui/liaotian/shurukuang.png");
	inputPin->setAnchorPoint(ccp(0.5f,0.5f));
	inputPin->setPosition(ccp(0,-80));
	inputPin->setScale9Enable(true);
	inputPin->setScale9Size(CCSizeMake(200,34));
	//layer->addWidget(inputPin);
	
	pAccountInputBox = new RichTextInputBox();
	pAccountInputBox->setCharLimit(20);
	pAccountInputBox->setInputBoxWidth(200);
	
	pAccountInputBox->setPosition(ccp(-100,-45));
	//layer->addChild(pAccountInputBox);
	pAccountInputBox->autorelease();

	pPinInputBoxr = new RichTextInputBox();
	pPinInputBoxr->setCharLimit(20);
	pPinInputBoxr->setInputBoxWidth(200);
	
	pPinInputBoxr->setPosition(ccp(-100,-95));
	//layer->addChild(pPinInputBoxr);
	pPinInputBoxr->autorelease();
}

Login::~Login()
{
    //CCHttpClient::getInstance()->destroyInstance();
}

Login * Login::getInstance()
{
    if (NULL == s_login)
    {
        s_login = new Login();
    }
    
    return s_login;
}

void Login::onEnter()
{
	CCLayer::onEnter();
    
    // LiuLiang++(登录接口)
    [[PPAppPlatformKit sharedInstance] showLogin];

//	string account = CCUserDefault::sharedUserDefault()->getStringForKey("account");
//	string password = CCUserDefault::sharedUserDefault()->getStringForKey("password");
//	if(NULL == pAccountInputBox->getInputString())
//	{
//		pAccountInputBox->onTextFieldInsertText(NULL, account.c_str(), account.size());
//	}
//	if(NULL == pPinInputBoxr->getInputString())
//	{
//		pPinInputBoxr->onTextFieldInsertText(NULL, password.c_str(), password.size());
//	}
}

void Login::onVersionCheck()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = Login::s_loginserver_ip;
		//std::string url = "192.168.1.122";
        url.append(":6666/versioncheck");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(Login::onVersionCheckCompleted));

		VersionCheckReq httpReq;
		httpReq.set_clienttype("");
		httpReq.set_clientversion(GAME_CODE_VERSION);
		CCLOG(GAME_CODE_VERSION);
		int resVersion = CCUserDefault::sharedUserDefault()->getIntegerForKey(KEY_OF_RES_VERSION, GAME_RES_VERSION);
		httpReq.set_resversion(resVersion);
		httpReq.set_channelid(0);
		httpReq.set_batchid(0);
		httpReq.set_recommendid(0);
		httpReq.set_extensionfield("");
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
		httpReq.set_platform("");
#else
		httpReq.set_platform(OPERATION_PLATFORM);
#endif

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void Login::onVersionCheckCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());

        return;
    }
    
	VersionCheckRsp versionCheckRsp;
	if(response->getResponseData()->size() <= 0)
	{
		versionCheckRsp.set_result(1);   // server no response, so the default value is 1, indicate that there is no update
	}
	else
	{
		//CCAssert(response->getResponseData()->size() > 0, "should not be empty");
		versionCheckRsp.ParseFromString(response->getResponseData()->data());

		CCLOG("response.result = %d", versionCheckRsp.result());
	}

	int result = versionCheckRsp.result();
	if(1 == result)
	{
		onMenuGetTestClicked();
	}
	else if(2 == result)
	{
		GameView::getInstance()->showAlertDialog("请到PP助手下载最新客户端");
	}
}

void Login::onMenuGetTestClicked()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = s_loginserver_ip;
        url.append(":6666/serverlist");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestCompleted));

		ServerListReq httpReq;
		httpReq.set_userid(userId);
		httpReq.set_sessionid(sessionId);

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void Login::onHttpRequestCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	m_pServerListRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pServerListRsp->result());
	/*
	CCLOG("m_pServerListRsp->serverinfos().roleid() = %d",m_pServerListRsp->serverinfos(0).roleid());
	CCLOG("response.serverinfos_size = %d", m_pServerListRsp->serverinfos_size());
	CCLOG("m_pServerListRsp->serverinfos(0).has_roleid() = %d",m_pServerListRsp->serverinfos(0).has_roleid());
	CCLOG("m_pServerListRsp->serverinfos(0).roleid() = %lld\n",m_pServerListRsp->serverinfos(0).roleid());*/
	if(1 == m_pServerListRsp->result())
	{
		GameState* pScene = new LoginState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
	else
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_server_list"));
	}
}

void Login::onMenuGetLoginClicked(cocos2d::CCObject *sender)
{    
//	if(NULL == pAccountInputBox->getInputString())
//	{
//        GameView::getInstance()->showAlertDialog("帐号不能为空");
//		return;
//	}
//	else
//	{
//		CCUserDefault::sharedUserDefault()->setStringForKey("account", pAccountInputBox->getInputString());
//	}
//	if(NULL == pPinInputBoxr->getInputString())
//	{
//        GameView::getInstance()->showAlertDialog("密码不能为空");
//		return;
//	}
//	else
//	{
//		CCUserDefault::sharedUserDefault()->setStringForKey("password", pPinInputBoxr->getInputString());
//	}
//
//    // test 1
//    {
//        CCHttpRequest* request = new CCHttpRequest();
//        std::string url = s_loginserver_ip;
//		url.append(":6666/login");
//        request->setUrl(url.c_str());
//        request->setRequestType(CCHttpRequest::kHttpPost);
//        request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestLogin));
//
//		std::string strUniqueId = GameUtils::getAppUniqueID();
//
//		LoginReq httpReq;
//		httpReq.set_account(pAccountInputBox->getInputString());
//		httpReq.set_authenticid(pPinInputBoxr->getInputString());
//		//useless element
//		httpReq.set_accountnum(0);
//		httpReq.set_accountpt("test");
//		httpReq.set_imei(strUniqueId);
//		httpReq.set_ua(strUniqueId);
//		httpReq.set_channelid(LoginReq::QIANLIANG);
//		httpReq.set_batchid(0);
//		httpReq.set_recommendid(0);
//		httpReq.set_systemversion("test");
//		httpReq.set_nettype(0);
//		httpReq.set_apkwithres(0);
//		httpReq.set_platformuserid("test");
//
//		string msgData;
//		httpReq.SerializeToString(&msgData);
//
//		request->setRequestData(msgData.c_str(), msgData.size());
//        request->setTag("POST");
//		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
//        CCHttpClient::getInstance()->send(request);
//        request->release();
//    }
    
    // LiuLiang++(登录接口)
    [[PPAppPlatformKit sharedInstance] showLogin];
}

void Login::onHttpRequestLogin(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	LoginRsp* m_pRegisterRsp = new LoginRsp();
	m_pRegisterRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pRegisterRsp->result());

	if(1 == m_pRegisterRsp->result())
	{
		userId = m_pRegisterRsp->userid();
		sessionId = m_pRegisterRsp->sessionid();
		onVersionCheck();
	}
	else
	{
		GameView::getInstance()->showAlertDialog(m_pRegisterRsp->resultmessage());
	}
}


void Login::onMenuClear(cocos2d::CCObject *sender)
{
	pAccountInputBox->deleteAllInputString();
	pPinInputBoxr->deleteAllInputString();
}

void Login::onMenuGetRegisterClicked(cocos2d::CCObject *sender)
{    
	if(NULL == pAccountInputBox->getInputString())
	{
        GameView::getInstance()->showAlertDialog("帐号不能为空");
		return;
	}
	if(NULL == pPinInputBoxr->getInputString())
	{
        GameView::getInstance()->showAlertDialog("密码不能为空");
		return;
	}
    

    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = s_loginserver_ip;
		url.append(":6666/auth_regist");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestRegister));

		AuthRegistReq httpReq;
		httpReq.set_account(pAccountInputBox->getInputString());
		httpReq.set_password(pPinInputBoxr->getInputString());

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void Login::onHttpRequestRegister(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	AuthRegistRsp* m_pLoginRsp = new AuthRegistRsp();
	m_pLoginRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pLoginRsp->result());

	if(1 == m_pLoginRsp->result())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_register_success"));
	}
	else
	{
		GameView::getInstance()->showAlertDialog(m_pLoginRsp->resultmessage());
	}
}


void Login::getServerListInfo(ServerListRsp* httpRsp)
{
	*httpRsp = *m_pServerListRsp;
}

void Login::onSocketOpen(ClientNetEngine* socketEngine)
{
}
void Login::onSocketClose(ClientNetEngine* socketEngine)
{
}
void Login::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

/*
void runLogin()
{
    CCScene *pScene = CCScene::create();
    Login *pLayer = new Login();
    pScene->addChild(pLayer);
    
    CCDirector::sharedDirector()->replaceScene(pScene);
    pLayer->release();
}
*/

enum {
	kTagMainLayer = 1,
};

LoginGameState::~LoginGameState()
{
}

void LoginGameState::runThisState()
{
	CCLayer* pLayer = new Login();
	addChild(pLayer, 0, kTagMainLayer);
	pLayer->release();

	if(CCDirector::sharedDirector()->getRunningScene() != NULL)
		CCDirector::sharedDirector()->replaceScene(this);
	else
		CCDirector::sharedDirector()->runWithScene(this);
}

void LoginGameState::onSocketOpen(ClientNetEngine* socketEngine)
{
}
void LoginGameState::onSocketClose(ClientNetEngine* socketEngine)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	Login* layer = (Login*)this->getChildByTag(kTagMainLayer);
	layer->onSocketClose(socketEngine);
}
void LoginGameState::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

void Login::getLoginInfo(std::string strAccount, std::string strSessionId)
{
    CCHttpRequest* request = new CCHttpRequest();
    std::string url = s_loginserver_ip;
    url.append(":6666/login");
    request->setUrl(url.c_str());
    request->setRequestType(CCHttpRequest::kHttpPost);
    request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestLogin));
    
    std::string strUniqueId = GameUtils::getAppUniqueID();
    
    int nToken = atoi(strSessionId.c_str());
    
    // PP
    PPLoginReq * pPPLoginReq = new PPLoginReq();
    //pPPLoginReq->set_tokentype(nToken);
    //pPPLoginReq->set_token(<#int index#>, <#::google::protobuf::int32 value#>)
    
    
    
    pPPLoginReq->set_tokentype(1);
    pPPLoginReq->set_tokenstr(strSessionId);
    
    //    int nSizeToken = strSessionId.size();
    //    for (int i = 0; i < nSizeToken; i++)
    //    {
    //        std::string charStr = strSessionId.substr(i, 1);
    //        int nTokenChar = atoi(charStr.c_str());
    //        pPPLoginReq->add_token(nTokenChar);
    //    }
    
    LoginReq httpReq;
    //httpReq.set_account(pAccountInputBox->getInputString());
    //httpReq.set_authenticid(pPinInputBoxr->getInputString());
    // 快用这里不用传入account，服务器会自动填入
    httpReq.set_account(strAccount);
    httpReq.set_accountnum(0);
    httpReq.set_accountpt("test");
    httpReq.set_imei(strUniqueId);
    httpReq.set_ua(strUniqueId);
    httpReq.set_channelid(LoginReq_Channel_PP);
    httpReq.set_batchid(0);
    httpReq.set_recommendid(0);
    httpReq.set_systemversion("test");
    httpReq.set_nettype(0);
    httpReq.set_apkwithres(0);
    httpReq.set_platformuserid("test");
    //httpReq.set_allocated_nd91loginreq(nd91LoginReq);
    //httpReq.set_allocated_laohuloginreq(laohuLoginReq);
    //httpReq.set_allocated_tongbuloginreq(pTongBuLoginReq);
    //httpReq.set_allocated_kuaiyongloginreq(pKuaiYongLoginReq);
    httpReq.set_allocated_pploginreq(pPPLoginReq);
    
    string msgData;
    httpReq.SerializeToString(&msgData);
    
    request->setRequestData(msgData.c_str(), msgData.size());
    request->setTag("POST");
    CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
    CCHttpClient::getInstance()->send(request);
    request->release();
}
