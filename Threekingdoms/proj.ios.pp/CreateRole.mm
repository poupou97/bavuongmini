#include "CreateRole.h"

#include "../Classes/gamescene_state/GameSceneState.h"
#include "../Classes/loadscene_state/LoadSceneState.h"
#include "../Classes/legend_engine/CCLegendAnimation.h"
#include "../Classes/legend_engine/LegendLevel.h"
#include "../Classes/ui/extensions/CCRichLabel.h"
#include "../Classes/ui/extensions/RichElement.h"

#include "../Classes/utils/GameUtils.h"

#include "../Classes/messageclient/GameMessageProcessor.h"
#include "../Classes/messageclient/ClientNetEngine.h"

#include "../Classes/ui/extensions/RichTextInput.h"
#include "../Classes/GameView.h"
#include "../Classes/messageclient/protobuf/LoginMessage.pb.h"
#include "Login.h"
#include "LoginState.h"
#include "../Classes/ui/country_ui/CountryUI.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/ui/Chat_ui/ChatCell.h"
#include "../Classes/GameUserDefault.h"
#include "../Classes/utils/GameUtils.h"
#include "../Classes/legend_script/CCTutorialParticle.h"
#include "../Classes/login_state/MoutainBackGroundLayer.h"

#include <PPAppPlatformKit/PPAppPlatformKit.h>

#define SPRITE_MENGJIANG "res_ui/creating_a_role/mengjiang.png"
#define SPRITE_GUIMOU "res_ui/creating_a_role/guimou.png"
#define SPRITE_HAOJIE "res_ui/creating_a_role/haojie.png"
#define SPRITE_SHENSHE "res_ui/creating_a_role/shenshe.png"
#define PROFESSION_MENGJIANG "res_ui/creating_a_role/mengjiang1.png"
#define PROFESSION_GUIMOU "res_ui/creating_a_role/guimou1.png"
#define PROFESSION_HAOJIE "res_ui/creating_a_role/haojie1.png"
#define PROFESSION_SHENSHE "res_ui/creating_a_role/shenshe1.png"
#define INTRODUCTION_MENGJIANG "res_ui/creating_a_role/01-shi.png"
#define INTRODUCTION_GUIMOU "res_ui/creating_a_role/04-shi.png"
#define INTRODUCTION_HAOJIE "res_ui/creating_a_role/02-shi.png"
#define INTRODUCTION_SHENSHE "res_ui/creating_a_role/03-shi.png"
#define SPRITE_TABLE "res_ui/creating_a_role/taizi.png"
#define FRAME_NONE "res_ui/round_none.png"
#define FRAME_GREEN "res_ui/round_green.png"

#define ROLECOLOR 100,100,100
#define ACTION_TAG 77
#define PROFESSION_ICON 100
#define SPRITE_ICON_TAG 13
#define SPRITE_FRONT_TAG 200
#define RUNACTION_TIME 0.5f
#define DYNAMIC_EFFECT_FADETO 1.1f

#define COMMON_TAG 9996
#define COUNTRY_TAG 9999
#define LAYER_TAG 9998
#define PROMPT_LOG_TAG 9997
#define FRONT_TAG 800
#define MIDDLE_TAG 500
#define BACK_TAG 201
#define COUNTRY_1_TAG 1
#define COUNTRY_2_TAG 2
#define COUNTRY_3_TAG 3
#define HIGHLIGHT_TAG 55

#define CCTUTORIALPARTICLE_TAG 56

#define IS_CLEAR_ENABLE 0

enum {
	kStateRoleList = 0,
	kStateConnectingGameServer,
};

enum {
	kTagPreparingBackGround = 2,
	kTagPreparingTip = 3,
	kTagServerTip = 4,
	kTagRoleListMenu = 16,   // last one !! please add more tag before it
};

enum
{
    kTagSprite1 = 1,
	kTagSprite2 = 2,
	kTagSprite3 = 3,
	kTagSprite4 = 4,
};

enum
{
	kTagStatus1 = 1,
	kTagStatus2 = 2,
	kTagStatus3 = 3,
	kTagStatus4 = 4,
};

enum
{
	kTagTouchedNot = 0,
	kTagTouchedLeft = 1,
	kTagTouchedRight = 2,
};

CreateRoleLayer::CreateRoleLayer()
: m_state(kStateRoleList)
, mCurrentServerId(0)
,m_tBeginPos(CCPointZero)
,mSpriteStatus(LoginLayer::mProfessionMin)
,mCountryFlag(LoginLayer::mCountryMin)
,isTouchSlipped(kTagTouchedNot)
,mZOrder(1)
,isRunActionTwice(false)
{
	setTouchEnabled(true);
	scheduleUpdate();

    CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	
	m_pServerListRsp = new ServerListRsp();
	Login::getServerListInfo(m_pServerListRsp);

	m_pRandomNameRsp = new RandomNameRsp();

	mCurrentServerId = LoginLayer::getCurrentServerId();
	
	//CCSprite* pSprite = CCSprite::create("res_ui/creating_a_role/di.jpg");
	//pSprite->setPosition(ccp(s.width/2, s.height/2));
	//pSprite->setAnchorPoint(ccp(0.5f, 0.5f));
	//if(((float)s.width/(float)1136) > ((float)s.height/(float)640))
	//{
	//	pSprite->setScaleX((float)s.width/(float)1136);
	//	pSprite->setScaleY((float)s.width/(float)1136);
	//}
	//else
	//{
	//	pSprite->setScaleX((float)s.height/(float)640);
	//	pSprite->setScaleY((float)s.height/(float)640);
	//}
	//addChild(pSprite, 0);

	CCLayerRGBA* pBackGround = MoutainBackGroundLayer::create();
	//pBackGround->setCascadeColorEnabled(true);
	//pBackGround->setColor(ccc3(224, 224, 224));
	addChild(pBackGround);

	//CCParticleSystem* particleRose = CCParticleSystemQuad::create("animation/texiao/particledesigner/meiguihua1.plist");
	//particleRose->setPositionType(kCCPositionTypeFree);
	//particleRose->setPosition(ccp(1200,600));
	//particleRose->setAnchorPoint(ccp(0.5f,0.5f));
	//particleRose->setVisible(true);
	////particleFire->setLife(0.5f);
	//particleRose->setLifeVar(0);
	//particleRose->setScale(0.7f);
	//particleRose->setScaleX(-0.7f);
	//pSprite->addChild(particleRose, 1);

	CCParticleSystem* particle = CCParticleSystemQuad::create("animation/texiao/particledesigner/xin1.plist");
	particle->setPositionType(kCCPositionTypeFree);
	particle->setPosition(ccp(s.width/2-40, s.height/2-120));
	particle->setAnchorPoint(ccp(0.5f,0.5f));
	particle->setVisible(true);
	//particleFire->setLife(0.5f);
	particle->setLifeVar(0);
	particle->setScale(0.7f);
	addChild(particle, COMMON_TAG);

	CCParticleSystem* particle1 = CCParticleSystemQuad::create("animation/texiao/particledesigner/xin1.plist");
	particle1->setPositionType(kCCPositionTypeFree);
	particle1->setPosition(ccp(s.width/2+40, s.height/2-120));
	particle1->setAnchorPoint(ccp(0.5f,0.5f));
	particle1->setVisible(true);
	//particleFire->setLife(0.5f);
	particle1->setLifeVar(0);
	particle1->setScale(0.7f);
	addChild(particle1, COMMON_TAG);
	
	UILayer* layer= UILayer::create();
	addChild(layer, LAYER_TAG, 255);

	initProfessionSprite(mSpriteStatus);

	//initCountrySprite(mCountryFlag);

	initProfessionIcon(mSpriteStatus);

	cocos2d::extension::UIButton *country1 = cocos2d::extension::UIButton::create();
	country1->setTouchEnable(true);
	country1->loadTextures("res_ui/creating_a_role/wei.png", "res_ui/creating_a_role/wei.png", "");
	country1->setTag(COUNTRY_1_TAG);
	country1->setPosition(ccp(70, s.height-80));
	country1->setScale(0.6f);
	country1->addReleaseEvent(this, coco_releaseselector(CreateRoleLayer::menuSelectCountry));
	country1->setPressedActionEnabled(true);

	cocos2d::extension::UIImageView * highLightFrame1 = cocos2d::extension::UIImageView::create();
	highLightFrame1->setTexture("res_ui/creating_a_role/ground_a.png");
	highLightFrame1->setPosition(ccp(0.5f, 0.5f));
	highLightFrame1->setZOrder(-1);
	highLightFrame1->setVisible(false);
	highLightFrame1->setTag(HIGHLIGHT_TAG);
	country1->addChild(highLightFrame1);

	cocos2d::extension::UIImageView * frame1 = cocos2d::extension::UIImageView::create();
	frame1->setTexture("res_ui/creating_a_role/country.png");
	frame1->setPosition(ccp(0.5f, 0.5f));
	frame1->setZOrder(-2);
	country1->addChild(frame1);

	cocos2d::extension::UIButton *country2 = cocos2d::extension::UIButton::create();
	country2->setTouchEnable(true);
	country2->loadTextures("res_ui/creating_a_role/shu.png", "res_ui/creating_a_role/shu.png", "");
	country2->setTag(COUNTRY_2_TAG);
	country2->setPosition(ccp(70, s.height-175));
	country2->setScale(0.6f);
	country2->addReleaseEvent(this, coco_releaseselector(CreateRoleLayer::menuSelectCountry));
	country2->setPressedActionEnabled(true);

	cocos2d::extension::UIImageView * highLightFrame2 = cocos2d::extension::UIImageView::create();
	highLightFrame2->setTexture("res_ui/creating_a_role/ground_a.png");
	highLightFrame2->setPosition(ccp(0.5f, 0.5f));
	highLightFrame2->setZOrder(-1);
	highLightFrame2->setVisible(false);
	highLightFrame2->setTag(HIGHLIGHT_TAG);
	country2->addChild(highLightFrame2);

	cocos2d::extension::UIImageView * frame2 = cocos2d::extension::UIImageView::create();
	frame2->setTexture("res_ui/creating_a_role/country.png");
	frame2->setPosition(ccp(0.5f, 0.5f));
	frame2->setZOrder(-2);
	country2->addChild(frame2);

	cocos2d::extension::UIButton *country3 = cocos2d::extension::UIButton::create();
	country3->setTouchEnable(true);
	country3->loadTextures("res_ui/creating_a_role/wu.png", "res_ui/creating_a_role/wu.png", "");
	country3->setTag(COUNTRY_3_TAG);
	country3->setPosition(ccp(70, s.height-270));
	country3->setScale(0.6f);
	country3->addReleaseEvent(this, coco_releaseselector(CreateRoleLayer::menuSelectCountry));
	country3->setPressedActionEnabled(true);

	cocos2d::extension::UIImageView * highLightFrame3 = cocos2d::extension::UIImageView::create();
	highLightFrame3->setTexture("res_ui/creating_a_role/ground_a.png");
	highLightFrame3->setPosition(ccp(0.5f, 0.5f));
	highLightFrame3->setZOrder(-1);
	highLightFrame3->setVisible(false);
	highLightFrame3->setTag(HIGHLIGHT_TAG);
	country3->addChild(highLightFrame3);

	cocos2d::extension::UIImageView * frame3 = cocos2d::extension::UIImageView::create();
	frame3->setTexture("res_ui/creating_a_role/country.png");
	frame3->setPosition(ccp(0.5f, 0.5f));
	frame3->setZOrder(-2);
	country3->addChild(frame3);

	layer->addWidget(country1);
	layer->addWidget(country2);
	layer->addWidget(country3);

	updateCountryButton(mCountryFlag);
	/*
	UIButton *country = UIButton::create();
    country->setTouchEnable(true);
    country->loadTextures("res_ui/creating_a_role/country.png", "res_ui/creating_a_role/country.png", "");
	country->setTag(99);
	country->setPosition(ccp(70, s.height-120));
	country->setScale(0.8f);
	country->addReleaseEvent(this, coco_releaseselector(CreateRoleLayer::menuSelectCountry));
	country->setPressedActionEnabled(true);
	
	CountryUI::mCountry = mCountryFlag;
	UIImageView *selectCountry=UIImageView::create();
	switch(mCountryFlag)
	{
		case 1:
			selectCountry->setTexture("res_ui/map/world_map/yizhou_zi.png");
			break;
		case 2:
			selectCountry->setTexture("res_ui/map/world_map/yangzhou_zi.png");
			break;
		case 3:
			selectCountry->setTexture("res_ui/map/world_map/jingzhou_zi.png");
			break;
		case 4:
			selectCountry->setTexture("res_ui/map/world_map/youzhou_zi.png");
			break;
		case 5:
			selectCountry->setTexture("res_ui/map/world_map/liangzhou_zi.png");
			break;
		default:
			break;
	}
	selectCountry->setTag(55);
	selectCountry->setAnchorPoint(ccp(0.5f,0.5f));
	selectCountry->setPosition(ccp(-2, -5));
	country->addChild(selectCountry);
	*/
	cocos2d::extension::UIImageView *inputBox = cocos2d::extension::UIImageView::create();
	inputBox->setTexture("res_ui/liaotian/shurukuang.png");
	inputBox->setAnchorPoint(ccp(0.5f,0.5f));
	inputBox->setPosition(ccp(s.width/2+33,30));
	inputBox->setScale9Enable(true);
	inputBox->setScale9Size(CCSizeMake(206,34));
	layer->addWidget(inputBox);

	cocos2d::extension::UIImageView *inputName = cocos2d::extension::UIImageView::create();
	inputName->setTexture("res_ui/creating_a_role/name_di.png");
	inputName->setAnchorPoint(ccp(0.5f,0.5f));
	inputName->setPosition(ccp(s.width/2-115,30));
	inputName->setScale(0.8f);
	/*inputName->setScale9Enable(true);
	inputName->setScale9Size(CCSizeMake(100,34));*/
	layer->addWidget(inputName);

	cocos2d::extension::UIButton *randomName = cocos2d::extension::UIButton::create();
    randomName->setTouchEnable(true);
    randomName->loadTextures("res_ui/creating_a_role/button1up.png", "res_ui/creating_a_role/button1up.png", "");
	randomName->setPosition(ccp(s.width/2+105,30));
	//randomName->setScale(1.5f);
	randomName->addReleaseEvent(this, coco_releaseselector(CreateRoleLayer::onTouchedRandomName));
	randomName->setPressedActionEnabled(true);

	pNameLayer = new RichTextInputBox();
	pNameLayer->setCharLimit(5);
	pNameLayer->setInputBoxWidth(155);
	
	pNameLayer->setPosition(ccp(s.width/2-70,15));
	layer->addChild(pNameLayer);
	pNameLayer->autorelease();

	cocos2d::extension::UIButton *enter = cocos2d::extension::UIButton::create();
    enter->setTouchEnable(true);
    enter->loadTextures("res_ui/creating_a_role/button_di.png", "res_ui/creating_a_role/button_di.png", "");
	enter->setPosition(ccp(s.width-80, 80));
	enter->addReleaseEvent(this, coco_releaseselector(CreateRoleLayer::menuSelectRoleCallback));
	enter->setPressedActionEnabled(true);

	CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("xuanzhuan.plist",48,72);
	tutorialParticle->setPosition(ccp(0, -36));
	tutorialParticle->setAnchorPoint(ccp(0.5f, 0.5f));
	enter->addCCNode(tutorialParticle);
	
	cocos2d::extension::UIImageView * enterText = cocos2d::extension::UIImageView::create();
	enterText->setTexture("res_ui/creating_a_role/enter_game.png");
	enterText->setVisible(true);
	enter->addChild(enterText);

	cocos2d::extension::UIButton *back = cocos2d::extension::UIButton::create();
    back->setTouchEnable(true);
    back->loadTextures("res_ui/creating_a_role/button_di.png", "res_ui/creating_a_role/button_di.png", "");
	back->setPosition(ccp(80, 80));
	back->addReleaseEvent(this, coco_releaseselector(CreateRoleLayer::onButtonGetClicked));
	back->setPressedActionEnabled(true);
	
	cocos2d::extension::UIImageView * backText = cocos2d::extension::UIImageView::create();
	backText->setTexture("res_ui/creating_a_role/back.png");
	backText->setVisible(true);
	back->addChild(backText);

	layer->addWidget(randomName);
	//layer->addWidget(country);
	layer->addWidget(enter);
	layer->addWidget(back);
	layer->setPosition(ccp(0,0));

	//ClientNetEngine::sharedSocketEngine()->setAddress(THREEKINGDOMS_HOST_STUDIO, THREEKINGDOMS_PORT_STUDIO);
}

CreateRoleLayer::~CreateRoleLayer()
{
	delete m_pServerListRsp;
	delete  m_pRandomNameRsp;
}

void CreateRoleLayer::updateCountryButton(int country)
{
	UILayer* layer = (UILayer*)getChildByTag(255);
	for(int i = 1; i <= 3; i++)
	{
		cocos2d::extension::UIButton * btn = (cocos2d::extension::UIButton *)layer->getWidgetByTag(i);
		cocos2d::extension::UIImageView* highlightframe = (cocos2d::extension::UIImageView*)btn->getChildByTag(HIGHLIGHT_TAG);
		if(i == country)
		{
			btn->setColor(ccc3(255, 255, 255));
			highlightframe->setVisible(true);
		}
		else
		{
			btn->setColor(ccc3(ROLECOLOR));
			highlightframe->setVisible(false);
		}
	}
}

/*
void CreateRoleLayer::initCountrySprite(int minCountry)
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	CCSprite* pSpriteText = CCSprite::create();
	pSpriteText->setPosition(ccp(70, s.height-280));
	//pSpriteText->setScale(0.9f);
	pSpriteText->setAnchorPoint(ccp(0.5f, 0.5f));
	addChild(pSpriteText, 1);

	switch(minCountry)
	{
	case 1:
		pSpriteText->initWithFile("res_ui/creating_a_role/yizhou.png");
		break;
	case 2:
		pSpriteText->initWithFile("res_ui/creating_a_role/yangzhou.png");
		break;
	case 3:
		pSpriteText->initWithFile("res_ui/creating_a_role/jingzhou.png");
		break;
	case 4:
		pSpriteText->initWithFile("res_ui/creating_a_role/youzhou.png");
		break;
	case 5:
		pSpriteText->initWithFile("res_ui/creating_a_role/liangzhou.png");
		break;
	default:
		break;
	}
}
*/
void CreateRoleLayer::spriteRunAction(float time, int mode)
{
	CCSize size = CCDirector::sharedDirector()->getVisibleSize();
	
	CCNode* sprite1 = getChildByTag(kTagSprite1);
	CCNode* sprite2 = getChildByTag(kTagSprite2);
	CCNode* sprite3 = getChildByTag(kTagSprite3);
	CCNode* sprite4 = getChildByTag(kTagSprite4);
	CCNode* profession1 = getChildByTag(5);
	CCNode* profession2 = getChildByTag(6);
	CCNode* profession3 = getChildByTag(7);
	CCNode* profession4 = getChildByTag(8);
	CCNode* introduction1 = getChildByTag(9);
	CCNode* introduction2 = getChildByTag(10);
	CCNode* introduction3 = getChildByTag(11);
	CCNode* introduction4 = getChildByTag(12);

	if(mode == kTagTouchedRight)
	{
		CCAction* action = (CCActionInterval*)CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2+180, size.height/2+80) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL);
		action->setTag(ACTION_TAG);
		switch(mSpriteStatus)
		{
			case 1:
				sprite1->runAction(action);
				sprite4->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.9f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite2->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2+120) ) ,
					CCFadeTo::create(time, 10),
					CCScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite3->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2-180, size.height/2+80) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));

				profession4->setScale(1);
				profession1->setScale(0);
				profession3->setScale(0);
				profession2->setScale(0);
				introduction4->setScale(1);
				introduction1->setScale(0);
				introduction3->setScale(0);
				introduction2->setScale(0);
				break;
			case 2:
				sprite2->runAction(action);
				sprite1->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.9f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite3->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2+120) ) ,
					CCFadeTo::create(time, 10),
					CCScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite4->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2-180, size.height/2+80) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
					
				profession1->setScale(1);
				profession3->setScale(0);
				profession4->setScale(0);
				profession2->setScale(0);
				introduction1->setScale(1);
				introduction3->setScale(0);
				introduction4->setScale(0);
				introduction2->setScale(0);
				break;
			case 3:
				sprite3->runAction(action);
				sprite2->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.9f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite4->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2+120) ) ,
					CCFadeTo::create(time, 10),
					CCScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite1->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2-180, size.height/2+80) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
					
				profession2->setScale(1);
				profession1->setScale(0);
				profession4->setScale(0);
				profession3->setScale(0);
				introduction2->setScale(1);
				introduction1->setScale(0);
				introduction4->setScale(0);
				introduction3->setScale(0);
				break;
			case 4:
					
				sprite4->runAction(action);
				sprite3->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.9f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite1->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2+120) ) ,
					CCFadeTo::create(time, 10),
					CCScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite2->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2-180, size.height/2+80) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
					
				profession3->setScale(1);
				profession1->setScale(0);
				profession4->setScale(0);
				profession2->setScale(0);
				introduction3->setScale(1);
				introduction1->setScale(0);
				introduction4->setScale(0);
				introduction2->setScale(0);
				break;

			default:
				break;
		}
		mActionSpriteTag = mSpriteStatus;
		if(1==mSpriteStatus)
			mSpriteStatus = 4;
		else
			mSpriteStatus--;

		isTouchSlipped = mode;
		updateProfessionIcon(mSpriteStatus);
	}
	else if(mode == kTagTouchedLeft)
	{
		CCAction* action = (CCActionInterval*)CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2-180, size.height/2+80) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL);
		action->setTag(ACTION_TAG);
		switch(mSpriteStatus)
		{
			case 1:
				sprite1->runAction(action);
				sprite2->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.9f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite4->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2+120) ) ,
					CCFadeTo::create(time, 10),
					CCScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite3->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2+180, size.height/2+80) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
					
				profession2->setScale(1);
				profession1->setScale(0);
				profession4->setScale(0);
				profession3->setScale(0);
				introduction2->setScale(1);
				introduction1->setScale(0);
				introduction4->setScale(0);
				introduction3->setScale(0);
				break;
			case 2:
				sprite2->runAction(action);
				sprite3->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.9f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite1->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2+120) ) ,
					CCFadeTo::create(time, 10),
					CCScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite4->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2+180, size.height/2+80) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
					
				profession3->setScale(1);
				profession1->setScale(0);
				profession4->setScale(0);
				profession2->setScale(0);
				introduction3->setScale(1);
				introduction1->setScale(0);
				introduction4->setScale(0);
				introduction2->setScale(0);
				break;
			case 3:
				sprite3->runAction(action);
				sprite4->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.9f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite2->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2+120) ) ,
					CCFadeTo::create(time, 10),
					CCScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite1->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2+180, size.height/2+80) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
					
				profession4->setScale(1);
				profession3->setScale(0);
				profession2->setScale(0);
				profession1->setScale(0);
				introduction4->setScale(1);
				introduction3->setScale(0);
				introduction2->setScale(0);
				introduction1->setScale(0);
				break;
			case 4:
				sprite4->runAction(action);
				sprite1->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.9f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite3->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2, size.height/2+120) ) ,
					CCFadeTo::create(time, 10),
					CCScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite2->runAction( CCSpawn::create(
					CCMoveTo::create(time, ccp(size.width/2+180, size.height/2+80) ) ,
					CCFadeTo::create(time, 255),
					CCScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
					
				profession1->setScale(1);
				profession3->setScale(0);
				profession4->setScale(0);
				profession2->setScale(0);
				introduction1->setScale(1);
				introduction3->setScale(0);
				introduction4->setScale(0);
				introduction2->setScale(0);
				break;
			default:
				break;
		}
		mActionSpriteTag = mSpriteStatus;
		if(4==mSpriteStatus)
			mSpriteStatus = 1;
		else
			mSpriteStatus++;

		isTouchSlipped = mode;
		updateProfessionIcon(mSpriteStatus);
	}
}

void CreateRoleLayer::onClickIconButton(CCObject *obj)
{
	cocos2d::extension::UIButton * btn =(cocos2d::extension::UIButton *)obj;
	cocos2d::extension::UIImageView* imageView = (cocos2d::extension::UIImageView*)btn->getChildByTag(SPRITE_ICON_TAG);
	imageView->setScale(0.85f);

	int btnTag = btn->getTag()-PROFESSION_ICON;
	
	if(isTouchSlipped||btnTag == mSpriteStatus)
	{
		return;
	}

	if((btnTag-mSpriteStatus)%2)
	{
		if(btnTag-mSpriteStatus == -1 || btnTag-mSpriteStatus == 3)
		{
			spriteRunAction(RUNACTION_TIME, kTagTouchedRight);
		}
		else if(btnTag-mSpriteStatus == 1 || btnTag-mSpriteStatus == -3)
		{
			spriteRunAction(RUNACTION_TIME, kTagTouchedLeft);
		}
	}
	else
	{
		isRunActionTwice = true;
		spriteRunAction(RUNACTION_TIME, kTagTouchedLeft);
	}
}

void CreateRoleLayer::initProfessionIcon(int profession)
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	UILayer* pLayer = (UILayer*)getChildByTag(255);

	cocos2d::extension::UIButton* pSpriteFrame1 = cocos2d::extension::UIButton::create();
	pSpriteFrame1->setTouchEnable(true);
    pSpriteFrame1->loadTextures(FRAME_NONE, FRAME_NONE, "");
	pSpriteFrame1->setPosition(ccp(s.width/2 - pSpriteFrame1->getContentSize().width*1.5f - 10, s.height-30));
	pSpriteFrame1->setAnchorPoint(ccp(0.5f, 0.5f));
	pSpriteFrame1->setPressedActionEnabled(true);
	pSpriteFrame1->setTag(PROFESSION_ICON+kTagSprite1);
	pSpriteFrame1->addReleaseEvent(this, coco_releaseselector(CreateRoleLayer::onClickIconButton));
	pLayer->addWidget(pSpriteFrame1);

	cocos2d::extension::UIImageView* highlightFrame1 = cocos2d::extension::UIImageView::create();
	highlightFrame1->setTexture("res_ui/creating_a_role/ground_b.png");
	highlightFrame1->setAnchorPoint(ccp(0.5f, 0.5f));
	highlightFrame1->setTag(HIGHLIGHT_TAG);
	highlightFrame1->setVisible(false);
	pSpriteFrame1->addChild(highlightFrame1);

	highlightFrame1->runAction(CCRepeatForever::create(
		CCSequence::create(
		CCScaleTo::create(0.5f, DYNAMIC_EFFECT_FADETO),
		CCScaleTo::create(0.5f, 1.0f),
		NULL)));
	/*
	CCTutorialParticle * tutorialParticle1 = CCTutorialParticle::create("xuanzhuan.plist",35,52);
	tutorialParticle1->setPosition(ccp(s.width/2 - pSpriteFrame1->getContentSize().width*1.5f - 10, s.height-56));
	tutorialParticle1->setTag(CCTUTORIALPARTICLE_TAG+kTagSprite1);
	tutorialParticle1->setAnchorPoint(ccp(0.5f, 0.5f));
	tutorialParticle1->setVisible(false);
	pLayer->addChild(tutorialParticle1);
	*/
	std::string iconPath = BasePlayer::getHeadPathByProfession(1);
	cocos2d::extension::UIImageView* pSprite1 = cocos2d::extension::UIImageView::create();
	pSprite1->setTexture(iconPath.c_str());
	//pSprite1->setPosition(ccp(pSprite1->getContentSize().width/2, pSprite1->getContentSize().height/2));
	pSprite1->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite1->setScale(0.85f);
	pSprite1->setColor(ccc3(ROLECOLOR));
	pSprite1->setTag(SPRITE_ICON_TAG);
	pSpriteFrame1->addChild(pSprite1);
	
	cocos2d::extension::UIButton* pSpriteFrame2 = cocos2d::extension::UIButton::create();
	pSpriteFrame2->setTouchEnable(true);
    pSpriteFrame2->loadTextures(FRAME_NONE, FRAME_NONE, "");
	pSpriteFrame2->setPosition(ccp(s.width/2 - pSpriteFrame2->getContentSize().width/2 - 5, s.height-30));
	pSpriteFrame2->setAnchorPoint(ccp(0.5f, 0.5f));
	pSpriteFrame2->setPressedActionEnabled(true);
	pSpriteFrame2->setTag(PROFESSION_ICON+kTagSprite2);
	pSpriteFrame2->addReleaseEvent(this, coco_releaseselector(CreateRoleLayer::onClickIconButton));
	pLayer->addWidget(pSpriteFrame2);

	cocos2d::extension::UIImageView* highlightFrame2 = cocos2d::extension::UIImageView::create();
	highlightFrame2->setTexture("res_ui/creating_a_role/ground_b.png");
	highlightFrame2->setAnchorPoint(ccp(0.5f, 0.5f));
	highlightFrame2->setTag(HIGHLIGHT_TAG);
	highlightFrame2->setVisible(false);
	pSpriteFrame2->addChild(highlightFrame2);

	highlightFrame2->runAction(CCRepeatForever::create(
		CCSequence::create(
		CCScaleTo::create(0.5f, DYNAMIC_EFFECT_FADETO),
		CCScaleTo::create(0.5f, 1.0f),
		NULL)));
	/*
	CCTutorialParticle * tutorialParticle2 = CCTutorialParticle::create("xuanzhuan.plist",35,52);
	tutorialParticle2->setPosition(ccp(0,-26));
	tutorialParticle2->setTag(CCTUTORIALPARTICLE_TAG);
	tutorialParticle2->setAnchorPoint(ccp(0.5f, 0.5f));
	tutorialParticle2->setVisible(false);
	pSpriteFrame2->addCCNode(tutorialParticle2);
	*/
	iconPath = BasePlayer::getHeadPathByProfession(2);
	cocos2d::extension::UIImageView* pSprite2 = cocos2d::extension::UIImageView::create();
	pSprite2->setTexture(iconPath.c_str());
	//pSprite2->setPosition(ccp(pSprite2->getContentSize().width/2, pSprite2->getContentSize().height/2));
	pSprite2->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite2->setScale(0.85f);
	pSprite2->setColor(ccc3(ROLECOLOR));
	pSprite2->setTag(SPRITE_ICON_TAG);
	pSpriteFrame2->addChild(pSprite2);

	cocos2d::extension::UIButton* pSpriteFrame3 = cocos2d::extension::UIButton::create();
	pSpriteFrame3->setTouchEnable(true);
    pSpriteFrame3->loadTextures(FRAME_NONE, FRAME_NONE, "");
	pSpriteFrame3->setPosition(ccp(s.width/2 + pSpriteFrame2->getContentSize().width/2 + 5, s.height-30));
	pSpriteFrame3->setAnchorPoint(ccp(0.5f, 0.5f));
	pSpriteFrame3->setPressedActionEnabled(true);
	pSpriteFrame3->setTag(PROFESSION_ICON+kTagSprite3);
	pSpriteFrame3->addReleaseEvent(this, coco_releaseselector(CreateRoleLayer::onClickIconButton));
	pLayer->addWidget(pSpriteFrame3);

	cocos2d::extension::UIImageView* highlightFrame3 = cocos2d::extension::UIImageView::create();
	highlightFrame3->setTexture("res_ui/creating_a_role/ground_b.png");
	highlightFrame3->setAnchorPoint(ccp(0.5f, 0.5f));
	highlightFrame3->setTag(HIGHLIGHT_TAG);
	highlightFrame3->setVisible(false);
	pSpriteFrame3->addChild(highlightFrame3);

	highlightFrame3->runAction(CCRepeatForever::create(
						CCSequence::create(
							CCScaleTo::create(0.5f, DYNAMIC_EFFECT_FADETO),
							CCScaleTo::create(0.5f, 1.0f),
							NULL)));
	/*
	CCTutorialParticle * tutorialParticle3 = CCTutorialParticle::create("xuanzhuan.plist",35,52);
	tutorialParticle3->setPosition(ccp(0,-26));
	tutorialParticle3->setTag(CCTUTORIALPARTICLE_TAG);
	tutorialParticle3->setAnchorPoint(ccp(0.5f, 0.5f));
	tutorialParticle3->setVisible(false);
	pSpriteFrame3->addCCNode(tutorialParticle3);
	*/
	iconPath = BasePlayer::getHeadPathByProfession(3);
	cocos2d::extension::UIImageView* pSprite3 = cocos2d::extension::UIImageView::create();
	pSprite3->setTexture(iconPath.c_str());
	//pSprite3->setPosition(ccp(pSprite3->getContentSize().width/2, pSprite3->getContentSize().height/2));
	pSprite3->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite3->setScale(0.85f);
	pSprite3->setColor(ccc3(ROLECOLOR));
	pSprite3->setTag(SPRITE_ICON_TAG);
	pSpriteFrame3->addChild(pSprite3);

	cocos2d::extension::UIButton* pSpriteFrame4 = cocos2d::extension::UIButton::create();
	pSpriteFrame4->setTouchEnable(true);
    pSpriteFrame4->loadTextures(FRAME_NONE, FRAME_NONE, "");
	pSpriteFrame4->setPosition(ccp(s.width/2 + pSpriteFrame2->getContentSize().width*1.5f + 10, s.height-30));
	pSpriteFrame4->setAnchorPoint(ccp(0.5f, 0.5f));
	pSpriteFrame4->setPressedActionEnabled(true);
	pSpriteFrame4->setTag(PROFESSION_ICON+kTagSprite4);
	pSpriteFrame4->addReleaseEvent(this, coco_releaseselector(CreateRoleLayer::onClickIconButton));
	pLayer->addWidget(pSpriteFrame4);

	cocos2d::extension::UIImageView* highlightFrame4 = cocos2d::extension::UIImageView::create();
	highlightFrame4->setTexture("res_ui/creating_a_role/ground_b.png");
	highlightFrame4->setAnchorPoint(ccp(0.5f, 0.5f));
	highlightFrame4->setTag(HIGHLIGHT_TAG);
	highlightFrame4->setVisible(false);
	pSpriteFrame4->addChild(highlightFrame4);

	highlightFrame4->runAction(CCRepeatForever::create(
		CCSequence::create(
		CCScaleTo::create(0.5f, DYNAMIC_EFFECT_FADETO),
		CCScaleTo::create(0.5f, 1.0f),
		NULL)));
	/*
	CCTutorialParticle * tutorialParticle4 = CCTutorialParticle::create("xuanzhuan.plist",35,52);
	tutorialParticle4->setPosition(ccp(0,-26));
	tutorialParticle4->setTag(CCTUTORIALPARTICLE_TAG);
	tutorialParticle4->setAnchorPoint(ccp(0.5f, 0.5f));
	tutorialParticle4->setVisible(false);
	pSpriteFrame4->addCCNode(tutorialParticle4);
	*/
	iconPath = BasePlayer::getHeadPathByProfession(4);
	cocos2d::extension::UIImageView* pSprite4 = cocos2d::extension::UIImageView::create();
	pSprite4->setTexture(iconPath.c_str());
	//pSprite4->setPosition(ccp(pSprite4->getContentSize().width/2, pSprite4->getContentSize().height/2));
	pSprite4->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite4->setScale(0.85f);
	pSprite4->setColor(ccc3(ROLECOLOR));
	pSprite4->setTag(SPRITE_ICON_TAG);
	pSpriteFrame4->addChild(pSprite4);

	switch(profession)
	{
	case 1:
		pSpriteFrame1->loadTextures(FRAME_GREEN, FRAME_GREEN, "");
		pSprite1->setColor(ccc3(255, 255, 255));
		highlightFrame1->setVisible(true);
		//tutorialParticle1->setVisible(true);
		break;
	case 2:
		pSpriteFrame2->loadTextures(FRAME_GREEN, FRAME_GREEN, "");
		pSprite2->setColor(ccc3(255, 255, 255));
		highlightFrame2->setVisible(true);
		//tutorialParticle2->setVisible(true);
		break;
	case 3:
		pSpriteFrame3->loadTextures(FRAME_GREEN, FRAME_GREEN, "");
		pSprite3->setColor(ccc3(255, 255, 255));
		highlightFrame3->setVisible(true);
		//tutorialParticle3->setVisible(true);
		break;
	case 4:
		pSpriteFrame4->loadTextures(FRAME_GREEN, FRAME_GREEN, "");
		pSprite4->setColor(ccc3(255, 255, 255));
		highlightFrame4->setVisible(true);
		//tutorialParticle4->setVisible(true);
		break;
	default:
		break;
	}
}

void CreateRoleLayer::updateProfessionIcon(int profession)
{
	UILayer* pLayer = (UILayer*)getChildByTag(255);

	cocos2d::extension::UIButton* spriteFrame1 = (cocos2d::extension::UIButton*)pLayer->getWidgetByTag(PROFESSION_ICON+kTagSprite1);
	cocos2d::extension::UIImageView* sprite1 = (cocos2d::extension::UIImageView*)spriteFrame1->getChildByTag(SPRITE_ICON_TAG);
	cocos2d::extension::UIImageView* highlightFrame1 = (cocos2d::extension::UIImageView*)spriteFrame1->getChildByTag(HIGHLIGHT_TAG);
	cocos2d::extension::UIButton* spriteFrame2 = (cocos2d::extension::UIButton*)pLayer->getWidgetByTag(PROFESSION_ICON+kTagSprite2);
	cocos2d::extension::UIImageView* sprite2 = (cocos2d::extension::UIImageView*)spriteFrame2->getChildByTag(SPRITE_ICON_TAG);
	cocos2d::extension::UIImageView* highlightFrame2 = (cocos2d::extension::UIImageView*)spriteFrame2->getChildByTag(HIGHLIGHT_TAG);
	cocos2d::extension::UIButton* spriteFrame3 = (cocos2d::extension::UIButton*)pLayer->getWidgetByTag(PROFESSION_ICON+kTagSprite3);
	cocos2d::extension::UIImageView* sprite3 = (cocos2d::extension::UIImageView*)spriteFrame3->getChildByTag(SPRITE_ICON_TAG);
	cocos2d::extension::UIImageView* highlightFrame3 = (cocos2d::extension::UIImageView*)spriteFrame3->getChildByTag(HIGHLIGHT_TAG);
	cocos2d::extension::UIButton* spriteFrame4 = (cocos2d::extension::UIButton*)pLayer->getWidgetByTag(PROFESSION_ICON+kTagSprite4);
	cocos2d::extension::UIImageView* sprite4 = (cocos2d::extension::UIImageView*)spriteFrame4->getChildByTag(SPRITE_ICON_TAG);
	cocos2d::extension::UIImageView* highlightFrame4 = (cocos2d::extension::UIImageView*)spriteFrame4->getChildByTag(HIGHLIGHT_TAG);

	spriteFrame1->setTextures(FRAME_NONE, FRAME_NONE, "");
	spriteFrame2->setTextures(FRAME_NONE, FRAME_NONE, "");
	spriteFrame3->setTextures(FRAME_NONE, FRAME_NONE, "");
	spriteFrame4->setTextures(FRAME_NONE, FRAME_NONE, "");
	sprite1->setColor(ccc3(ROLECOLOR));
	sprite2->setColor(ccc3(ROLECOLOR));
	sprite3->setColor(ccc3(ROLECOLOR));
	sprite4->setColor(ccc3(ROLECOLOR));
	highlightFrame1->setVisible(false);
	highlightFrame2->setVisible(false);
	highlightFrame3->setVisible(false);
	highlightFrame4->setVisible(false);

	switch(profession)
	{
	case 1:
		spriteFrame1->setTextures(FRAME_GREEN, FRAME_GREEN, "");
		sprite1->setColor(ccc3(255, 255, 255));
		highlightFrame1->setVisible(true);
		break;
	case 2:
		spriteFrame2->setTextures(FRAME_GREEN, FRAME_GREEN, "");
		sprite2->setColor(ccc3(255, 255, 255));
		highlightFrame2->setVisible(true);
		break;
	case 3:
		spriteFrame3->setTextures(FRAME_GREEN, FRAME_GREEN, "");
		sprite3->setColor(ccc3(255, 255, 255));
		highlightFrame3->setVisible(true);
		break;
	case 4:
		spriteFrame4->setTextures(FRAME_GREEN, FRAME_GREEN, "");
		sprite4->setColor(ccc3(255, 255, 255));
		highlightFrame4->setVisible(true);
		break;
	default:
		break;
	}
}

void CreateRoleLayer::updateSpriteFront(int profession)
{
	CCSprite* pSprite = (CCSprite*)getChildByTag(profession);
	pSprite->setZOrder(++mZOrder);
	//pSprite->setVisible(true);
}

void CreateRoleLayer::initProfessionSprite(int minProfession)
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	/*
	CCSprite* pSpriteFront = CCSprite::create();
	pSpriteFront->setPosition(ccp(s.width/2, s.height/2));
	pSpriteFront->setScale(0.9f);
	pSpriteFront->setAnchorPoint(ccp(0.5f, 0.5f));
	pSpriteFront->setVisible(true);
	addChild(pSpriteFront, 100, SPRITE_FRONT_TAG);
	*/

	const float MOVE_UP_AND_DOWN_DURATION = 2.5f;
	const int MOVE_UP_AND_DOWN_DISTANCE = 5;

	CCSprite* pSprite1 = CCSprite::create(SPRITE_MENGJIANG);
	pSprite1->setCascadeColorEnabled(true);
	pSprite1->setCascadeOpacityEnabled(true);
	pSprite1->setPosition(ccp(s.width/2, s.height/2));
	pSprite1->setScale(0.9f);
	pSprite1->setAnchorPoint(ccp(0.5f, 0.5f));
	addChild(pSprite1, 1, kTagSprite1);

	CCMoveBy * moveUpAction = CCMoveBy::create(MOVE_UP_AND_DOWN_DURATION, ccp(0, MOVE_UP_AND_DOWN_DISTANCE));
	CCMoveBy * moveDownAction = CCMoveBy::create(MOVE_UP_AND_DOWN_DURATION, ccp(0, -MOVE_UP_AND_DOWN_DISTANCE));
	CCRepeatForever * repeapAction = CCRepeatForever::create(CCSequence::create(moveUpAction, moveDownAction, NULL));
	pSprite1->runAction(repeapAction);

	CCSprite* pSprite2 = CCSprite::create(SPRITE_GUIMOU);
	pSprite2->setCascadeColorEnabled(true);
	pSprite2->setCascadeOpacityEnabled(true);
	pSprite2->setPosition(ccp(s.width/2+180, s.height/2+80));
	pSprite2->setScale(0.5f);
	pSprite2->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite2->setColor(ccc3(ROLECOLOR));
	addChild(pSprite2, 1, kTagSprite2);

	moveUpAction = CCMoveBy::create(MOVE_UP_AND_DOWN_DURATION, ccp(0, -MOVE_UP_AND_DOWN_DISTANCE));
	moveDownAction = CCMoveBy::create(MOVE_UP_AND_DOWN_DURATION, ccp(0, MOVE_UP_AND_DOWN_DISTANCE));
	repeapAction = CCRepeatForever::create(CCSequence::create(moveUpAction, moveDownAction, NULL));
	pSprite2->runAction(repeapAction);

	CCSprite* pSprite3 = CCSprite::create(SPRITE_HAOJIE);
	pSprite3->setCascadeColorEnabled(true);
	pSprite3->setCascadeOpacityEnabled(true);
	pSprite3->setPosition(ccp(s.width/2, s.height/2+120));
	pSprite3->setScale(0);
	pSprite3->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite3->setColor(ccc3(ROLECOLOR));
	addChild(pSprite3, 1, kTagSprite3);

	moveUpAction = CCMoveBy::create(MOVE_UP_AND_DOWN_DURATION, ccp(0, MOVE_UP_AND_DOWN_DISTANCE));
	moveDownAction = CCMoveBy::create(MOVE_UP_AND_DOWN_DURATION, ccp(0, -MOVE_UP_AND_DOWN_DISTANCE));
	repeapAction = CCRepeatForever::create(CCSequence::create(moveUpAction, moveDownAction, NULL));
	pSprite3->runAction(repeapAction);

	CCSprite* pSprite4 = CCSprite::create(SPRITE_SHENSHE);
	pSprite4->setCascadeColorEnabled(true);
	pSprite4->setCascadeOpacityEnabled(true);
	pSprite4->setPosition(ccp(s.width/2-180, s.height/2+80));
	pSprite4->setScale(0.5f);
	pSprite4->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite4->setColor(ccc3(ROLECOLOR));
	addChild(pSprite4, 1, kTagSprite4);

	moveUpAction = CCMoveBy::create(MOVE_UP_AND_DOWN_DURATION, ccp(0, -MOVE_UP_AND_DOWN_DISTANCE));
	moveDownAction = CCMoveBy::create(MOVE_UP_AND_DOWN_DURATION, ccp(0, MOVE_UP_AND_DOWN_DISTANCE));
	repeapAction = CCRepeatForever::create(CCSequence::create(moveUpAction, moveDownAction, NULL));
	pSprite4->runAction(repeapAction);

	CCSprite* pSpriteTable1 = CCSprite::create(SPRITE_TABLE);
	pSpriteTable1->setPosition(ccp(pSprite1->getContentSize().width/2, -40));
	pSpriteTable1->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite1->addChild(pSpriteTable1, -1);
	
	CCSprite* pSpriteTable2 = CCSprite::create(SPRITE_TABLE);
	pSpriteTable2->setPosition(ccp(pSprite1->getContentSize().width/2, -40));
	pSpriteTable2->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite2->addChild(pSpriteTable2, -1);

	CCSprite* pSpriteTable3 = CCSprite::create(SPRITE_TABLE);
	pSpriteTable3->setPosition(ccp(pSprite1->getContentSize().width/2, -40));
	pSpriteTable3->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite3->addChild(pSpriteTable3, -1);

	CCSprite* pSpriteTable4 = CCSprite::create(SPRITE_TABLE);
	pSpriteTable4->setPosition(ccp(pSprite1->getContentSize().width/2, -40));
	pSpriteTable4->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite4->addChild(pSpriteTable4, -1);

	pSprite2->setColor(ccc3(ROLECOLOR));
	pSprite3->setColor(ccc3(ROLECOLOR));
	pSprite4->setColor(ccc3(ROLECOLOR));

	int profession_icon_pos_x = s.width-100;
	CCSprite *profession1=CCSprite::create(PROFESSION_MENGJIANG);
	profession1->setAnchorPoint(ccp(0.5f,0.5f));
	profession1->setPosition(ccp(profession_icon_pos_x, s.height-55));
	profession1->setScale(1.0f);
	addChild(profession1, COMMON_TAG, 5);
	
	CCSprite *profession2=CCSprite::create(PROFESSION_GUIMOU);
	profession2->setAnchorPoint(ccp(0.5f,0.5f));
	profession2->setPosition(ccp(profession_icon_pos_x, s.height-55));
	profession2->setScale(0);
	addChild(profession2, COMMON_TAG, 6);
	
	CCSprite *profession3=CCSprite::create(PROFESSION_HAOJIE);
	profession3->setAnchorPoint(ccp(0.5f,0.5f));
	profession3->setPosition(ccp(profession_icon_pos_x, s.height-55));
	profession3->setScale(0);
	addChild(profession3, COMMON_TAG, 7);
	
	CCSprite *profession4=CCSprite::create(PROFESSION_SHENSHE);
	profession4->setAnchorPoint(ccp(0.5f,0.5f));
	profession4->setPosition(ccp(profession_icon_pos_x, s.height-55));
	profession4->setScale(0);
	addChild(profession4, COMMON_TAG, 8);
	
	int profession_introduction_pos_x = s.width-70;
	CCSprite *introduction1=CCSprite::create(INTRODUCTION_MENGJIANG);
	introduction1->setAnchorPoint(ccp(0.5f,0.5f));
	introduction1->setPosition(ccp(profession_introduction_pos_x, s.height/2+20));
	introduction1->setScale(1.0f);
	addChild(introduction1, COMMON_TAG, 9);
	
	CCSprite *introduction2=CCSprite::create(INTRODUCTION_GUIMOU);
	introduction2->setAnchorPoint(ccp(0.5f,0.5f));
	introduction2->setPosition(ccp(profession_introduction_pos_x, s.height/2+20));
	introduction2->setScale(0);
	addChild(introduction2, COMMON_TAG, 10);
	
	CCSprite *introduction3=CCSprite::create(INTRODUCTION_HAOJIE);
	introduction3->setAnchorPoint(ccp(0.5f,0.5f));
	introduction3->setPosition(ccp(profession_introduction_pos_x, s.height/2+20));
	introduction3->setScale(0);
	addChild(introduction3, COMMON_TAG, 11);
	
	CCSprite *introduction4=CCSprite::create(INTRODUCTION_SHENSHE);
	introduction4->setAnchorPoint(ccp(0.5f,0.5f));
	introduction4->setPosition(ccp(profession_introduction_pos_x, s.height/2+20));
	introduction4->setScale(0);
	addChild(introduction4, COMMON_TAG, 12);

	switch(minProfession)
	{
	case 1:
		pSprite1->setZOrder(FRONT_TAG);
		pSprite2->setZOrder(MIDDLE_TAG);
		pSprite4->setZOrder(MIDDLE_TAG);
		pSprite3->setZOrder(BACK_TAG);
		break;
	case 2:
		pSprite2->setZOrder(FRONT_TAG);
		pSprite3->setZOrder(MIDDLE_TAG);
		pSprite1->setZOrder(MIDDLE_TAG);
		pSprite4->setZOrder(BACK_TAG);

		pSprite2->setPosition(ccp(s.width/2, s.height/2));
		pSprite2->setScale(0.9f);
		pSprite2->setColor(ccc3(255, 255, 255));
		pSprite3->setPosition(ccp(s.width/2+180, s.height/2+80));
		pSprite3->setScale(0.5f);
		//pSprite3->setOpacity(0.7f*256);
		pSprite3->setColor(ccc3(ROLECOLOR));
		pSprite4->setPosition(ccp(s.width/2, s.height/2+120));
		pSprite4->setScale(0);
		pSprite4->setOpacity(10);
		pSprite4->setColor(ccc3(ROLECOLOR));
		pSprite1->setPosition(ccp(s.width/2-180, s.height/2+80));
		pSprite1->setScale(0.5f);
		//pSprite1->setOpacity(0.7f*256);
		pSprite1->setColor(ccc3(ROLECOLOR));
		profession1->setScale(0);
		profession2->setScale(1.0f);
		profession3->setScale(0);
		profession4->setScale(0);
		introduction1->setScale(0);
		introduction2->setScale(1.0f);
		introduction3->setScale(0);
		introduction4->setScale(0);
		break;
	case 3:
		pSprite3->setZOrder(FRONT_TAG);
		pSprite4->setZOrder(MIDDLE_TAG);
		pSprite2->setZOrder(MIDDLE_TAG);
		pSprite1->setZOrder(BACK_TAG);

		pSprite3->setPosition(ccp(s.width/2, s.height/2));
		pSprite3->setScale(0.9f);
		pSprite3->setColor(ccc3(255, 255, 255));
		pSprite4->setPosition(ccp(s.width/2+180, s.height/2+80));
		pSprite4->setScale(0.5f);
		//pSprite4->setOpacity(0.7f*256);
		pSprite4->setColor(ccc3(ROLECOLOR));
		pSprite1->setPosition(ccp(s.width/2, s.height/2+120));
		pSprite1->setScale(0);
		pSprite1->setOpacity(10);
		pSprite1->setColor(ccc3(ROLECOLOR));
		pSprite2->setPosition(ccp(s.width/2-180, s.height/2+80));
		pSprite2->setScale(0.5f);
		//pSprite2->setOpacity(0.7f*256);
		pSprite2->setColor(ccc3(ROLECOLOR));
		profession1->setScale(0);
		profession2->setScale(0);
		profession3->setScale(1.0f);
		profession4->setScale(0);
		introduction1->setScale(0);
		introduction2->setScale(0);
		introduction3->setScale(1.0f);
		introduction4->setScale(0);
		break;
	case 4:
		pSprite4->setZOrder(FRONT_TAG);
		pSprite1->setZOrder(MIDDLE_TAG);
		pSprite3->setZOrder(MIDDLE_TAG);
		pSprite2->setZOrder(BACK_TAG);

		pSprite4->setPosition(ccp(s.width/2, s.height/2));
		pSprite4->setScale(0.9f);
		pSprite4->setColor(ccc3(255, 255, 255));
		pSprite1->setPosition(ccp(s.width/2+180, s.height/2+80));
		pSprite1->setScale(0.5f);
		//pSprite1->setOpacity(0.7f*256);
		pSprite1->setColor(ccc3(ROLECOLOR));
		pSprite2->setPosition(ccp(s.width/2, s.height/2+120));
		pSprite2->setScale(0);
		pSprite2->setOpacity(10);
		pSprite2->setColor(ccc3(ROLECOLOR));
		pSprite3->setPosition(ccp(s.width/2-180, s.height/2+80));
		pSprite3->setScale(0.5f);
		//pSprite3->setOpacity(0.7f*256);
		pSprite3->setColor(ccc3(ROLECOLOR));
		profession1->setScale(0);
		profession2->setScale(0);
		profession3->setScale(0);
		profession4->setScale(1.0f);
		introduction1->setScale(0);
		introduction2->setScale(0);
		introduction3->setScale(0);
		introduction4->setScale(1.0f);
		break;
	default:
		break;
	}
}

void CreateRoleLayer::connectGameServer()
{
	ClientNetEngine::sharedSocketEngine()->init(*(GameView::getInstance()));
}

void CreateRoleLayer::update(float dt)
{
	/*if(mCountryFlag != CountryUI::mCountry)
	{
		UILayer* pLayer = (UILayer*)getChildByTag(255);
		UIImageView* pCountryName = (UIImageView*)pLayer->getWidgetByTag(99)->getChildByTag(55);
		switch(CountryUI::mCountry)
		{
			case 1:
				pCountryName->setTexture("res_ui/map/world_map/yizhou_zi.png");
				break;
			case 2:
				pCountryName->setTexture("res_ui/map/world_map/yangzhou_zi.png");
				break;
			case 3:
				pCountryName->setTexture("res_ui/map/world_map/jingzhou_zi.png");
				break;
			case 4:
				pCountryName->setTexture("res_ui/map/world_map/youzhou_zi.png");
				break;
			case 5:
				pCountryName->setTexture("res_ui/map/world_map/liangzhou_zi.png");
				break;
			default:
				break;
		}
		mCountryFlag = CountryUI::mCountry;
		initCountrySprite(mCountryFlag);
	}
	*/
	if(isTouchSlipped)
	{
		CCNode* sprite = getChildByTag(mActionSpriteTag);
		CCAction* action = sprite->getActionByTag(ACTION_TAG);
		CCNode* spriteFront = getChildByTag(mSpriteStatus);
		if(spriteFront->getScale() >= sprite->getScale())
		{
			int iSpriteMiddle, iSpriteBack;

			spriteFront->setZOrder(FRONT_TAG);
			sprite->setZOrder(MIDDLE_TAG);
			if(isTouchSlipped == kTagTouchedLeft)
			{
				if(mSpriteStatus == 4)
				{
					iSpriteMiddle = 1;
				}
				else
				{
					iSpriteMiddle = mSpriteStatus+1;
				}

				if(mActionSpriteTag == 1)
				{
					iSpriteBack = 4;
				}
				else
				{
					iSpriteBack = mActionSpriteTag-1;
				}
				CCNode* spriteMiddle = getChildByTag(iSpriteMiddle);
				CCNode* spriteBack = getChildByTag(iSpriteBack);
				spriteMiddle->setZOrder(MIDDLE_TAG);
				spriteBack->setZOrder(BACK_TAG);
			}
			else if(isTouchSlipped == kTagTouchedRight)
			{
				if(mSpriteStatus == 1)
				{
					iSpriteMiddle = 4;
				}
				else
				{
					iSpriteMiddle = mSpriteStatus-1;
				}

				if(mActionSpriteTag == 4)
				{
					iSpriteBack = 1;
				}
				else
				{
					iSpriteBack = mActionSpriteTag+1;
				}
				CCNode* spriteMiddle = getChildByTag(iSpriteMiddle);
				CCNode* spriteBack = getChildByTag(iSpriteBack);
				spriteMiddle->setZOrder(MIDDLE_TAG);
				spriteBack->setZOrder(BACK_TAG);
			}
		}
		if(action == NULL)
		{
			isTouchSlipped = kTagTouchedNot;
			if(isRunActionTwice)
			{
				spriteRunAction(RUNACTION_TIME, kTagTouchedLeft);
				isRunActionTwice = false;
			}
		}
	}
}

void CreateRoleLayer::onButtonGetClicked(cocos2d::CCObject *sender)
{
    // SDK需求加入（注销接口）
    [[PPAppPlatformKit sharedInstance] PPlogout];
    
    
	//runLogin();
	GameState* pScene = new LoginGameState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}
}

void CreateRoleLayer::onTouchedRandomName(CCObject* pSender)
{
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = Login::s_loginserver_ip;
        url.append(":6666/randomname");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(CreateRoleLayer::onHttpRequestCompleted));

		RandomNameReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);
		if((mSpriteStatus == 1)||(mSpriteStatus == 4))
		{
			httpReq.set_gender(1);
		}
		else
		{
			httpReq.set_gender(0);
		}
		httpReq.set_serverid(m_pServerListRsp->serverinfos(mCurrentServerId).serverid());

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(),msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void CreateRoleLayer::onHttpRequestCompletedCreateRole(CCHttpClient *sender, CCHttpResponse *response)
{
	if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }

	CreateRoleRsp createRoleRsp;
	CCAssert(response->getResponseData()->size() > 0, "should not be empty");
	createRoleRsp.ParseFromString(response->getResponseData()->data());

	CCLOG("createRoleRsp.result() = %d\n", createRoleRsp.result());
	CCLOG("createRoleRsp.resultmessage() = %s\n",createRoleRsp.resultmessage().c_str());

   if(1 == createRoleRsp.result())
	{
		LoginLayer::mRoleId = createRoleRsp.roleid();
		CCLOG("LoginLayer::mRoleId = %d\n", LoginLayer::mRoleId);

		CCSize s = CCDirector::sharedDirector()->getVisibleSize();
		CCRect insetRect = CCRectMake(32,15,1,1);
		CCScale9Sprite* backGround = CCScale9Sprite::create("res_ui/kuang_1.png"); 
		backGround->setCapInsets(insetRect);
		backGround->setPreferredSize(CCSizeMake(550, 50));
		backGround->setPosition(ccp(s.width/2, s.height/2));
		backGround->setAnchorPoint(ccp(0.5f, 0.5f));
		backGround->setTag(kTagPreparingBackGround);
		addChild(backGround, PROMPT_LOG_TAG);

		// show tip
		CCLabelTTF* label = CCLabelTTF::create(StringDataManager::getString("con_enter_game"), "Arial", 25);
		addChild(label,PROMPT_LOG_TAG);
		label->setTag(kTagPreparingTip);
		label->setAnchorPoint(ccp(0.5f, 0.5f));
		label->setPosition( ccp(s.width/2, s.height/2) );


		CCActionInterval *action = (CCActionInterval*)CCSequence::create
			(
				CCShow::create(),
				CCDelayTime::create(0.5f),
				CCCallFunc::create(this, callfunc_selector(CreateRoleLayer::connectGameServer)), 
				NULL
			);
		label->runAction(action);

		m_state = kStateConnectingGameServer;
        
        //PP助手没有浮动工具条
        
	}
	else
	{
		GameView::getInstance()->showAlertDialog(createRoleRsp.resultmessage());
	}

}

void CreateRoleLayer::onHttpRequestCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }

   
    // dump data
	m_pRandomNameRsp->ParseFromString(response->getResponseData()->data());

	if(NULL != pNameLayer->getInputString())
	{
		pNameLayer->deleteAllInputString();
	}
	pNameLayer->onTextFieldInsertText(NULL, m_pRandomNameRsp->rolename().c_str(), m_pRandomNameRsp->rolename().size());
	/*
	CCLabelTTF* labelRandomName ;
	labelRandomName = (CCLabelTTF*)getChildByTag(55);

	labelRandomName->setString(m_pRandomNameRsp->rolename().c_str());
	*/
	CCLOG("response.rolename = %s", m_pRandomNameRsp->rolename().c_str());
}

void CreateRoleLayer::menuSelectCountry(CCObject* pSender)
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	cocos2d::extension::UIButton * btn = (cocos2d::extension::UIButton *)pSender;
	mCountryFlag = btn->getTag();

	updateCountryButton(mCountryFlag);

	//initCountrySprite(mCountryFlag);
	/*
	CountryUI * countryui=CountryUI::create();
	countryui->ignoreAnchorPointForPosition(false);
	countryui->setAnchorPoint(ccp(0.5f,0.5f));
	countryui->setPosition(ccp(winSize.width/2,winSize.height/2));
	this->addChild(countryui, COUNTRY_TAG);*/
}

void CreateRoleLayer::menuSelectRoleCallback(CCObject* pSender)
{
	//CCLabelTTF* labelRandomName ;
	//labelRandomName = (CCLabelTTF*)getChildByTag(55);
    
    //char name[100];
    //sprintf(name, "random %d", CCRANDOM_0_1()*500);
    //labelRandomName->setString(name);
    
    
	if(NULL == pNameLayer->getInputString())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_random_name_tip"));
		return;
	}
	CCHttpRequest* request = new CCHttpRequest();
    std::string url = Login::s_loginserver_ip;
    url.append(":6666/createrole");
    request->setUrl(url.c_str());
    request->setRequestType(CCHttpRequest::kHttpPost);
    request->setResponseCallback(this, httpresponse_selector(CreateRoleLayer::onHttpRequestCompletedCreateRole));

	CreateRoleReq httpReq;
	httpReq.set_userid(Login::userId);
	httpReq.set_sessionid(Login::sessionId);
	httpReq.set_serverid(m_pServerListRsp->serverinfos(mCurrentServerId).serverid());
	CCLOG("m_pServerListRsp->serverinfos(0).serverid() = %d", m_pServerListRsp->serverinfos(mCurrentServerId).serverid());
	httpReq.set_mode(0);

	httpReq.set_rolename(pNameLayer->getInputString());
	httpReq.set_profession(mSpriteStatus);
	if((mSpriteStatus == 1)||(mSpriteStatus == 4))
	{
		httpReq.set_gender(1);
	}
	else
	{
		httpReq.set_gender(0);
	}
	httpReq.set_faceid("test");
	httpReq.set_country(mCountryFlag);

	// mac
	std::string macId = GameUtils::getAppUniqueID();
	httpReq.set_mac(macId);

	// os ∫Õ platform
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
	httpReq.set_os(1);
	httpReq.set_platform("qianliang");
#elif defined(CC_TARGET_OS_IPHONE)
	httpReq.set_os(MACHINE_TYPE);
	httpReq.set_platform(OPERATION_PLATFORM);
#endif

	string msgData;
	httpReq.SerializeToString(&msgData);

	request->setRequestData(msgData.c_str(), msgData.size());
    request->setTag("POST");
	CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
    CCHttpClient::getInstance()->send(request);
    request->release();

	/*
	std::vector<ChatCell * >::iterator iter;
	for (iter = GameView::getInstance()->chatCell_Vdata.begin(); iter != GameView::getInstance()->chatCell_Vdata.end(); ++ iter)
	{
		delete * iter;
	}
	GameView::getInstance()->chatCell_Vdata.clear();
	*/
}

void CreateRoleLayer::onSocketOpen(ClientNetEngine* socketEngine)
{
	// request to enter game
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1001, this);
}
void CreateRoleLayer::onSocketClose(ClientNetEngine* socketEngine)
{	
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	// player can re-connect
	if(m_state == kStateConnectingGameServer)
	{
		this->getChildByTag(kTagPreparingBackGround)->removeFromParent();
		this->getChildByTag(kTagPreparingTip)->removeFromParent();
		// enable role list
		/*for(int i = 0; i < MAX_LOGIN_ROLE_NUM; i++)
		{
			CCMenu* menu = (CCMenu*)this->getChildByTag(kTagRoleListMenu+i);
			menu->setEnabled(true);
		}
		*/
		m_state = kStateRoleList;
	}
}
void CreateRoleLayer::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

void CreateRoleLayer::ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent)
{
    CCSetIterator it = pTouches->begin();
    CCTouch* touch = (CCTouch*)(*it);

    m_tBeginPos = touch->getLocation();    

	struct timeval m_sStartTime;
	gettimeofday(&m_sStartTime, NULL);
	m_dStartTime = m_sStartTime.tv_sec*1000 + m_sStartTime.tv_usec/1000;
}

void CreateRoleLayer::ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent)
{

}

void CreateRoleLayer::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{
    CCSetIterator it = pTouches->begin();
    CCTouch* touch = (CCTouch*)(*it);
    
    CCPoint location = touch->getLocation();

	struct timeval m_sEndedTime;
	gettimeofday(&m_sEndedTime, NULL);
	m_dEndedTime = m_sEndedTime.tv_sec*1000 + m_sEndedTime.tv_usec/1000;

	float velocity = (float)(location.x - m_tBeginPos.x)/(float)(m_dEndedTime - m_dStartTime);

	if((velocity > 0.25f||velocity < -0.25f)&&(!isTouchSlipped))
	{
		this->stopAllActions();
		if(velocity > 0.0f)
		{
			float time = (200.0f/velocity)/1000.0f;
			spriteRunAction(time, kTagTouchedRight);
		}
		else if(velocity < 0.0f)
		{
			float time = -(200.0f/velocity)/1000.0f;
			spriteRunAction(time, kTagTouchedLeft);
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////

enum {
	kTagMainLayer = 1,
};

CreateRoleState::~CreateRoleState()
{
}

void CreateRoleState::runThisState()
{
    CCLayer* pLayer = new CreateRoleLayer();
    addChild(pLayer, 0, kTagMainLayer);
	pLayer->release();

	if(CCDirector::sharedDirector()->getRunningScene() != NULL)
		CCDirector::sharedDirector()->replaceScene(this);
	else
		CCDirector::sharedDirector()->runWithScene(this);
}

void CreateRoleState::onSocketOpen(ClientNetEngine* socketEngine)
{
	CreateRoleLayer* layer = (CreateRoleLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketOpen(socketEngine);
}
void CreateRoleState::onSocketClose(ClientNetEngine* socketEngine)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	CreateRoleLayer* layer = (CreateRoleLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketClose(socketEngine);
}
void CreateRoleState::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}
