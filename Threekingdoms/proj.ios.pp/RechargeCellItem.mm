#include "../Classes/ui/recharge_ui/RechargeCellItem.h"
#include "../Classes/messageclient/element/CLableGoods.h"
#include "CCMoveableMenu.h"
#include "../Classes/messageclient/element/GoodsInfo.h"
#include "../Classes/loadscene_state/LoadSceneState.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/gamescene_state/GameSceneState.h"
#include "../Classes/GameView.h"
#include "AppMacros.h"
#include "../Classes/utils/GameUtils.h"
#include "../Classes/ui/backpackscene/EquipmentItem.h"
#include "../Classes/ui/backpackscene/GoodsItemInfoBase.h"
#include "../Classes/messageclient/element/CRechargeInfo.h"

#include <PPAppPlatformKit/PPAppPlatformKit.h>

#import "ExtenalClass.h"
#import "../Classes/login_state/Login.h"
#import "../Classes/login_state/LoginState.h"
#import "../Classes/utils/GameConfig.h"

#define HIGHLIGHT_FIRST_TAG 100
#define HIGHLIGHT_SECOND_TAG 101

int RechargeCellItem::curOrderId = 1;

RechargeCellItem::RechargeCellItem()
{
}


RechargeCellItem::~RechargeCellItem()
{
	delete curRechargeInfo;
}

RechargeCellItem* RechargeCellItem::create( CRechargeInfo * rechargeInfo )
{
	RechargeCellItem * rechargeCellItem = new RechargeCellItem();
	if (rechargeCellItem && rechargeCellItem->init(rechargeInfo))
	{
		rechargeCellItem->autorelease();
		return rechargeCellItem;
	}
	CC_SAFE_DELETE(rechargeCellItem);
	return NULL;
}

bool RechargeCellItem::init( CRechargeInfo * rechargeInfo )
{
	if (UIScene::init())
	{
		/////////////////////////////////////////////////////////////////////////////////////

		// ∏√UILayerŒª”⁄CCTableView÷Æƒ⁄£¨Œ™¡À’˝»∑œÏ”¶TableViewµƒÕœ∂Ø ¬º˛£¨π …Ë÷√Œ™≤ªÕÃµÙ¥•√˛ ¬º˛
		m_pUiLayer->setSwallowsTouches(false);

		curRechargeInfo = new CRechargeInfo(*rechargeInfo);

		orderId = curRechargeInfo->get_orderId();

		cocos2d::extension::UIButton * btn_buy  = cocos2d::extension::UIButton::create();
		btn_buy->setTextures("res_ui/LV4_red.png","res_ui/LV4_red.png","");
		btn_buy->setTouchEnable(true);
		//btn_buy->setPressedActionEnabled(true);
		btn_buy->setScale9Enable(true);
		btn_buy->setScale9Size(CCSizeMake(248,76));
		//btn_buy->setCapInsets(CCRectMake(18,9,2,23));
		btn_buy->setAnchorPoint(ccp(0.5f,0.5f));
		btn_buy->setPosition(ccp(124,38));
		btn_buy->addReleaseEvent(this,coco_releaseselector(RechargeCellItem::BuyEvent));
		m_pUiLayer->addWidget(btn_buy);

		cocos2d::extension::UIImageView * imageViewLight = cocos2d::extension::UIImageView::create();
		imageViewLight->setTexture("res_ui/Prepaid/light_round.png");
		imageViewLight->setScale9Enable(true);
		imageViewLight->setScale9Size(CCSizeMake(73,74));
		imageViewLight->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewLight->setPosition(ccp(-84,-2));
		btn_buy->addChild(imageViewLight);

		cocos2d::extension::UIImageView * imageViewGold = cocos2d::extension::UIImageView::create();
		std::string strGold;
		strGold.append("res_ui/Prepaid/");
		strGold.append(curRechargeInfo->get_icon());
		strGold.append(".png");
		imageViewGold->setTexture(strGold.c_str());
		imageViewGold->setScale9Enable(true);
		imageViewGold->setScale9Size(CCSizeMake(64,55));
		imageViewGold->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewGold->setPosition(ccp(0,3));
		imageViewLight->addChild(imageViewGold);
		
		cocos2d::extension::UIImageView * imageViewFrame = cocos2d::extension::UIImageView::create();
		imageViewFrame->setTexture("res_ui/di_yy.png");
		imageViewFrame->setScale9Enable(true);
		//imageViewFrame->setScale9Size(CCSizeMake(141,27));
		imageViewFrame->setScale9Size(CCSizeMake(85,27));
		imageViewFrame->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewFrame->setPosition(ccp(-4,13));
		btn_buy->addChild(imageViewFrame);
		
		cocos2d::extension::UIImageView * imageViewGoldMini = cocos2d::extension::UIImageView::create();
		imageViewGoldMini->setTexture("res_ui/ingot.png");
		imageViewGoldMini->setScale9Enable(true);
		imageViewGoldMini->setScale9Size(CCSizeMake(23,15));
		imageViewGoldMini->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewGoldMini->setPosition(ccp(-29,0));
		imageViewFrame->addChild(imageViewGoldMini);
		
		cocos2d::extension::UILabel * goldSum = cocos2d::extension::UILabel::create();
        goldSum->setStrokeEnabled(true);
		char aucBuf[6];
		memset(aucBuf, 0, sizeof(aucBuf));
		sprintf(aucBuf, "%d", curRechargeInfo->get_recharge_value());
		goldSum->setText(aucBuf);
		goldSum->setAnchorPoint(ccp(0,0.5f));
		goldSum->setFontName(APP_FONT_NAME);
		goldSum->setFontSize(18);
		goldSum->setPosition(ccp(-19,0));
		goldSum->setColor(ccc3(255, 255, 255));
		imageViewFrame->addChild(goldSum);
		
		cocos2d::extension::UIImageView * imageViewFrameRMB = cocos2d::extension::UIImageView::create();
		imageViewFrameRMB->setTexture("res_ui/di_yy.png");
		imageViewFrameRMB->setScale9Enable(true);
		//imageViewFrame->setScale9Size(CCSizeMake(141,27));
		imageViewFrameRMB->setScale9Size(CCSizeMake(56,27));
		imageViewFrameRMB->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewFrameRMB->setPosition(ccp(78,13));
		btn_buy->addChild(imageViewFrameRMB);
		
		cocos2d::extension::UIImageView * imageViewRMB = cocos2d::extension::UIImageView::create();
		imageViewRMB->setTexture("res_ui/vip/qian2.png");
		imageViewRMB->setScale9Enable(true);
		imageViewRMB->setScale9Size(CCSizeMake(15,17));
		imageViewRMB->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewRMB->setPosition(ccp(22,0));
		imageViewFrameRMB->addChild(imageViewRMB);
		
		cocos2d::extension::UILabel * moneySum = cocos2d::extension::UILabel::create();
        moneySum->setStrokeEnabled(true);
		memset(aucBuf, 0, sizeof(aucBuf));
		int a = atoi(VipValueConfig::s_vipValue[2].c_str())*100;
		sprintf(aucBuf, "%d", curRechargeInfo->get_recharge_value()/a);
		moneySum->setText(aucBuf);
		moneySum->setAnchorPoint(ccp(1,0.5f));
		moneySum->setFontName(APP_FONT_NAME);
		moneySum->setFontSize(18);
		moneySum->setPosition(ccp(15,0));
		moneySum->setColor(ccc3(255, 255, 255));
		imageViewFrameRMB->addChild(moneySum);

		char aucLable[6];
		memset(aucLable, 0, sizeof(aucLable));
		sprintf(aucLable,"%d",curRechargeInfo->get_send_value());
		std::string strLabel;
		strLabel.append(StringDataManager::getString("RechargeUI_SendLabel_front"));
		strLabel.append(aucLable);
		strLabel.append(StringDataManager::getString("RechargeUI_SendLabel_back"));
		UILabelBMFont* sendValue = UILabelBMFont::create();
		sendValue->setText(strLabel.c_str());
		sendValue->setFntFile("res_ui/font/ziti_3.fnt");
		//sendValue->setScale(0.7f);
		sendValue->setAnchorPoint(ccp(0.5f,0.5f));
		sendValue->setPosition(ccp(34,-17));
		btn_buy->addChild(sendValue);
		if(!curRechargeInfo->get_send_value())
		{
			sendValue->setVisible(false);
		}
		
		cocos2d::extension::UIImageView * imageViewSuggest = cocos2d::extension::UIImageView::create();
		imageViewSuggest->setTexture("res_ui/Prepaid/tuijian.png");
		//imageViewSuggest->setScale9Enable(true);
		//imageViewSuggest->setScale9Size(CCSizeMake(20,49));
		imageViewSuggest->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewSuggest->setPosition(ccp(12,64));
		m_pUiLayer->addWidget(imageViewSuggest);
		switch(curRechargeInfo->get_suggest())
		{
		case 0:
			imageViewSuggest->setVisible(false);
			break;
		case 1:
			imageViewSuggest->setVisible(true);
			break;
		default:
			break;
		}

		this->setContentSize(CCSizeMake(241,76));

		return true;
	}
	return false;
}

void RechargeCellItem::BuyEvent( CCObject *pSender )
{
	//remove last highlight frame
	setHighLightFrameVisible(curOrderId, false);
	//add current highlight frame
	setHighLightFrameVisible(orderId, true);

    bool bChargeEnabled = GameConfig::getBoolForKey("charge_enable");
	if (true == bChargeEnabled)
	{
		CRechargeInfo * pClickRechargeInfo = this->curRechargeInfo;
        // 元宝数
        int nRechargeValue = pClickRechargeInfo->get_recharge_value();
        // 价格（元）
        int nCharge = nRechargeValue / 100;
        
        //RechargeCellItem::s_nCharge = nCharge;
        [ExtenalClass set_Charge:nCharge];
        
        // 支付
        requestPay();
	}
	else
	{
		const char * charChargeEnabled = StringDataManager::getString("RechargeUI_disable");
		GameView::getInstance()->showAlertDialog(charChargeEnabled);
	}
}

void RechargeCellItem::setHighLightFrameVisible(int orderId, bool mode)
{
	CCTableViewCell* cell = m_tableView->cellAtIndex((orderId-1)/2);
	if(1 == orderId%2)
	{
		CCScale9Sprite* highLightLast = (CCScale9Sprite*)cell->getChildByTag(HIGHLIGHT_FIRST_TAG);
		highLightLast->setVisible(mode);
	}
	else
	{
		CCScale9Sprite* highLightLast = (CCScale9Sprite*)cell->getChildByTag(HIGHLIGHT_SECOND_TAG);
		highLightLast->setVisible(mode);
	}
}

void RechargeCellItem::requestPay()
{
    CCHttpRequest* request = new CCHttpRequest();
    //std::string url = "http://210.14.129.115";
    std::string url = Login::s_loginserver_ip;
    url.append(":6666/gen_order");
    request->setUrl(url.c_str());
    request->setRequestType(CCHttpRequest::kHttpPost);
    request->setResponseCallback(this, httpresponse_selector(RechargeCellItem::onHttpResponsePay));
    
    
    char charServerId[20];
    sprintf(charServerId, "%d", Login::m_pServerListRsp->serverinfos(LoginLayer::getCurrentServerId()).serverid());
    
    char charRoleId[20];
    sprintf(charRoleId, "%ld", LoginLayer::mRoleId);
    
    
    
    GenOrderReq genOrderReq;
    genOrderReq.set_channelid(8);       // PP的 号是8
    genOrderReq.set_userid(Login::userId);
    genOrderReq.set_sessionid(Login::sessionId);
    genOrderReq.set_commodity(GenOrderReq_Commodity_GOLDINGOT);
    //genOrderReq.set_commodity(GenOrderReq_Commodity_VIP2);
    genOrderReq.set_serverid(Login::m_pServerListRsp->serverinfos(LoginLayer::getCurrentServerId()).serverid());
    genOrderReq.set_roleid(LoginLayer::mRoleId);
    
    string msgData;
    genOrderReq.SerializeToString(&msgData);
    
    request->setRequestData(msgData.c_str(), msgData.size());
    request->setTag("POST");
    CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
    CCHttpClient::getInstance()->send(request);
    request->release();
}

void RechargeCellItem::onHttpResponsePay(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag()))
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed())
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
    
    // 返回的协议
	GenOrderRsp* m_pGenOrderRsp = new GenOrderRsp();
	m_pGenOrderRsp->ParseFromString(strdata);
    
    // 返回的订单号
    long long tmpNum = m_pGenOrderRsp->ordernum();
    
    char charOrderNum[20];
    sprintf(charOrderNum, "%lld", tmpNum);
    
    NSString * nsOrderNum = [NSString stringWithUTF8String:charOrderNum];
    
    //[ExtenalClass set_PayNum:tmpNum];
    
    
    // 返回的商品价钱
    //int nGoodsPrice = m_pGenOrderRsp->price();
    int nGoodsPrice = [ExtenalClass get_Charge];
    
    char charGoodsPrice[20];
    sprintf(charGoodsPrice, "%d", nGoodsPrice * 100);
    
    
    // 返回的商品名称
    //std::string strGoodsName = m_pGenOrderRsp->commodity();
    //NSString * NsGoodsName = [NSString stringWithUTF8String:strGoodsName.c_str()];
    std::string strGoodsName = "";
    strGoodsName.append(charGoodsPrice);
    strGoodsName.append("元宝");
    NSString * nsGoodsName = [NSString stringWithUTF8String:strGoodsName.c_str()];
    
    
    // roleId
    char charRoleId[20];
    sprintf(charRoleId, "%ld", LoginLayer::mRoleId);
    
    NSString * nsRoleId = [NSString stringWithUTF8String:charRoleId];
    
    
    if (0 == tmpNum)
    {
        // 提示信息
        NSString * strPayErrorTip = @"订单请求失败，请重试或联系客服";
        
        const char * charPayErrorTip = [strPayErrorTip UTF8String];
        
        GameView::getInstance()->showAlertDialog(charPayErrorTip);
        
        return;
    }
    
    
    // 支付方法
    //[[WmComPlatform defaultPlatform] WmUniPay:[[ExtenalClass sharedIntance] getBuyInfoFromUI:1:@"春哥"]];
    //[[WmComPlatform defaultPlatform] WmUniPay:[[ExtenalClass sharedIntance] getBuyInfo:nGoodsPrice:NsGoodsName]];
    
    
    //int time = [[NSDate date] timeIntervalSince1970];
    //NSString *billNO = [NSString stringWithFormat:@"%d",time];
    // 1表示元
    [[PPAppPlatformKit sharedInstance] exchangeGoods:nGoodsPrice BillNo:nsOrderNum BillTitle:nsGoodsName RoleId:nsRoleId ZoneId:0];
    
}