//
//  ExtenalClass.h(For 91SDK)
//  Threekingdoms
//
//  Created by 刘亮 on 14-4-30.
//
//

#import <Foundation/Foundation.h>

@interface ExtenalClass : NSObject

+(ExtenalClass*) sharedIntance;
+(id) allocWithZone:(struct _NSZone *)zone;
-(id) copyWithZone:(NSZone *)zone;
-(id) retain;
-(unsigned) retainCount;
-(oneway void) release;
-(id) autorelease;
-(id) init;

-(void)NdLoginResult:(NSNotification *)notify;
-(void)NdInitResult:(NSNotification *)notify;
-(void)NdUniPayAysnResult:(NSNotification *)notify;


+(int) get_Charge;                                                  // get所付金额
+(void) set_Charge:(int)nCharge;                                    // set所付金额



@end
