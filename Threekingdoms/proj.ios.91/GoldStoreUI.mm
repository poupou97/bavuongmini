#include "../Classes/ui/goldstore_ui/GoldStoreUI.h"
#include "../Classes/loadscene_state/LoadSceneState.h"
#include "../Classes/GameView.h"
#include "../Classes/ui/extensions/UITab.h"
#include "../Classes/messageclient/element/CLable.h"
#include "../Classes/ui/goldstore_ui/GoldStoreCellItem.h"
#include "../Classes/messageclient/element/CLableGoods.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/gamescene_state/MainScene.h"
#include "../Classes/messageclient/element/GoodsInfo.h"
#include "../Classes/ui/goldstore_ui/GoldStoreMarquee.h"
#include "../Classes/utils/StrUtils.h"
#include "../Classes/ui/vip_ui/VipDetailUI.h"
#include "../Classes/ui/vip_ui/VipEveryDayRewardsUI.h"
#include "../Classes/ui/vip_ui/FirstBuyVipUI.h"

#import <NdComPlatform/NdComPlatform.h>
#import <NdComPlatform/NdComPlatformAPIResponse.h>
#import <NdComPlatform/NdCPNotifications.h>

#import "ExtenalClass.h"
#import "../Classes/login_state/Login.h"
#import "../Classes/login_state/LoginState.h"

#define MARQUEE_LABLE_WIDTH 596
#define GOLD_STORE_MAX_FILTER 6

GoldStoreUI::GoldStoreUI():
curLableId(0),
m_strMarquee("")
{
}


GoldStoreUI::~GoldStoreUI()
{
	std::vector<GoodsInfo *>::iterator iterGoodsInfo;
	for (iterGoodsInfo = m_vector_goodsInfo.begin(); iterGoodsInfo != m_vector_goodsInfo.end(); iterGoodsInfo++)
	{
		delete *iterGoodsInfo;
	}
	m_vector_goodsInfo.clear();
}

GoldStoreUI* GoldStoreUI::create()
{
	GoldStoreUI * goldStoreUI = new GoldStoreUI();
	if (goldStoreUI && goldStoreUI->init())
	{
		goldStoreUI->autorelease();
		return goldStoreUI;
	}
	CC_SAFE_DELETE(goldStoreUI);
	return goldStoreUI;
}

bool GoldStoreUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		cocos2d::extension::UIImageView *mengban = cocos2d::extension::UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(800, 480));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		//º”‘ÿUI
		if(LoadSceneLayer::GoldStoreLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::GoldStoreLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::GoldStoreLayer;
		ppanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->setPosition(CCPointZero);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		const char * firstStr = StringDataManager::getString("UIName_yuan");
		const char * secondStr = StringDataManager::getString("UIName_bao");
		const char * thirdStr = StringDataManager::getString("UIName_shang");
		const char * fourthStr = StringDataManager::getString("UIName_cheng");
		CCArmature * atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,fourthStr);
		atmature->setPosition(ccp(30,240));
		u_layer->addChild(atmature);

		//πÿ±’∞¥≈•
		cocos2d::extension::UIButton * Button_close = (cocos2d::extension::UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(GoldStoreUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		cocos2d::extension::UIButton * Button_recharge = (cocos2d::extension::UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_recharge");
		Button_recharge->setTouchEnable(true);
		Button_recharge->addReleaseEvent(this, coco_releaseselector(GoldStoreUI::RechargeEvent));
		Button_recharge->setPressedActionEnabled(true);

		cocos2d::extension::UIButton * Button_vip = (cocos2d::extension::UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_vip");
		Button_vip->setTouchEnable(true);
		Button_vip->addReleaseEvent(this, coco_releaseselector(GoldStoreUI::VipEvent));
		Button_vip->setPressedActionEnabled(true);

		l_ingot_value = (cocos2d::extension::UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_IngotValue");
		char str_ingot[20];
		sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
		l_ingot_value->setText(str_ingot);

		// –°¿Æ∞»
		m_imageView_horn = (cocos2d::extension::UIImageView *)UIHelper::seekWidgetByName(ppanel, "ImageView_horn");
		m_imageView_horn->setVisible(true);

		int _num = GameView::getInstance()->goldStoreList.size();
		const char * normalImage = "res_ui/tab_s_off.png";
		const char * selectImage = "res_ui/tab_s.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_s.png";
		CCAssert(_num <= GOLD_STORE_MAX_FILTER, "out of range");
		char * ItemName[GOLD_STORE_MAX_FILTER];
		for (int i =0;i<_num;++i)
		{
			std::string imagePath = "res_ui/rmbshop/";
			imagePath.append(GameView::getInstance()->goldStoreList.at(i)->name().c_str());

			ItemName[i] = new char [50];
			strcpy(ItemName[i],imagePath.c_str());
		}
		if (_num > 0)
		{
			mainTab  = UITab::createWithImage(_num,normalImage,selectImage,finalImage,ItemName,VERTICAL,-10);
			mainTab->setAnchorPoint(ccp(0,0));
			mainTab->setPosition(ccp(753,420));
			mainTab->setHighLightImage((char * )highLightImage);
			mainTab->setDefaultPanelByIndex(0);
			mainTab->addIndexChangedEvent(this,coco_indexchangedselector(GoldStoreUI::IndexChangedEvent));
			mainTab->setPressedActionEnabled(true);
			u_layer->addWidget(mainTab);
		}
		
		for (int i = 0;i<_num;i++)
		{
			delete ItemName[i];
		}

		//delete old data
// 		DataSource.erase(DataSource.begin(),DataSource.end());
// 		std::vector<CLableGoods*>::iterator iter;
// 		for (iter = DataSource.begin(); iter != DataSource.end(); ++iter)
// 		{
// 			delete *iter;
// 		}
// 		DataSource.clear();
// 		

		m_tableView = CCTableView::create(this,CCSizeMake(449,317));
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0,0));
		m_tableView->setPosition(ccp(284,81));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		u_layer->addChild(m_tableView);

		//this->scheduleUpdate();

		return true;
	}
	return false;
}

void GoldStoreUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	//add new data
	if (GameView::getInstance()->goldStoreList.size() > 0)
	{
		for (int i = 0;i<GameView::getInstance()->goldStoreList.at(0)->commdity_size();++i)
		{
			CLableGoods * _LableGoods = new CLableGoods();
			_LableGoods->CopyFrom(GameView::getInstance()->goldStoreList.at(0)->commdity(i));
			DataSource.push_back(_LableGoods);
		}
	}

	// ≥ı ºªØ≈‹¬Ìµ∆ ˝æ›
	initMarquee();

	m_tableView->reloadData();
}

void GoldStoreUI::onExit()
{
	UIScene::onExit();
}

bool GoldStoreUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void GoldStoreUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void GoldStoreUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void GoldStoreUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void GoldStoreUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void GoldStoreUI::RechargeEvent( CCObject *pSender )
{
// 	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
// 	MainScene *mainscene = (MainScene *)GameView::getInstance()->getGameScene()->getChildByTag(GameSceneLayer::kTagMainScene);
// 	CCLayer *vipEveryDayRewardScene = (CCLayer*)mainscene->getChildByTag(kTagVipEveryDayRewardsUI);
// 	if(vipEveryDayRewardScene == NULL)
// 	{
// 		VipEveryDayRewardsUI * vpEveryDayRewardsUI = VipEveryDayRewardsUI::create();
// 		mainscene->addChild(vpEveryDayRewardsUI,0,kTagVipEveryDayRewardsUI);
// 		vpEveryDayRewardsUI->ignoreAnchorPointForPosition(false);
// 		vpEveryDayRewardsUI->setAnchorPoint(ccp(0.5f,0.5f));
// 		vpEveryDayRewardsUI->setPosition(ccp(winSize.width/2, winSize.height/2));
// 	}
// 	
// 	CCLayer *firstBuyVipScene = (CCLayer*)mainscene->getChildByTag(kTagFirstBuyVipUI);
// 	if(firstBuyVipScene == NULL)
// 	{
// 		FirstBuyVipUI* firstBuyVipUI = FirstBuyVipUI::create();
// 		mainscene->addChild(firstBuyVipUI,0,kTagFirstBuyVipUI);
// 		firstBuyVipUI->ignoreAnchorPointForPosition(false);
// 		firstBuyVipUI->setAnchorPoint(ccp(0.5f,0.5f));
// 		firstBuyVipUI->setPosition(ccp(winSize.width/2, winSize.height/2));
// 	}
    
    // 支付接口
    requestPay();
    
}

void GoldStoreUI::VipEvent( CCObject *pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getGameScene()->getChildByTag(GameSceneLayer::kTagMainScene);
	CCLayer *vipScene = (CCLayer*)mainscene->getChildByTag(kTagVipDetailUI);
	if(vipScene == NULL)
	{
		VipDetailUI * vipDetailUI = VipDetailUI::create();
		mainscene->addChild(vipDetailUI,0,kTagVipDetailUI);
		vipDetailUI->ignoreAnchorPointForPosition(false);
		vipDetailUI->setAnchorPoint(ccp(0.5f,0.5f));
		vipDetailUI->setPosition(ccp(winSize.width/2, winSize.height/2));
	}
}

void GoldStoreUI::IndexChangedEvent( CCObject* pSender )
{
	int idx = mainTab->getCurrentIndex();
	curLableId = GameView::getInstance()->goldStoreList.at(idx)->id();
	//delete old data
	DataSource.erase(DataSource.begin(),DataSource.end());
	std::vector<CLableGoods*>::iterator iter;
	for (iter = DataSource.begin(); iter != DataSource.end(); ++iter)
	{
		delete *iter;
	}
	DataSource.clear();

	//add new data
	for (int i = 0;i<GameView::getInstance()->goldStoreList.at(idx)->commdity_size();++i)
	{
		CLableGoods * _LableGoods = new CLableGoods();
		_LableGoods->CopyFrom(GameView::getInstance()->goldStoreList.at(idx)->commdity(i));
		DataSource.push_back(_LableGoods);
	}

	this->m_tableView->reloadData();
}

void GoldStoreUI::scrollViewDidScroll( CCScrollView* view )
{

}

void GoldStoreUI::scrollViewDidZoom( CCScrollView* view )
{

}

void GoldStoreUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{

}

cocos2d::CCSize GoldStoreUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(444,120);
}

cocos2d::extension::CCTableViewCell* GoldStoreUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	if (idx == DataSource.size()/2)
	{
		if (DataSource.size()%2 == 0)
		{
			GoldStoreCellItem * cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
			cellItem1->setPosition(ccp(15,0));
			cell->addChild(cellItem1);

			GoldStoreCellItem * cellItem2 = GoldStoreCellItem::create(DataSource.at(2*idx+1));
			cellItem2->setPosition(ccp(226,0));
			cell->addChild(cellItem2);
		}
		else
		{
			GoldStoreCellItem * cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
			cellItem1->setPosition(ccp(15,0));
			cell->addChild(cellItem1);
		}
	}
	else
	{
		GoldStoreCellItem * cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
		cellItem1->setPosition(ccp(15,0));
		cell->addChild(cellItem1);

		GoldStoreCellItem * cellItem2 = GoldStoreCellItem::create(DataSource.at(2*idx+1));
		cellItem2->setPosition(ccp(226,0));
		cell->addChild(cellItem2);
	}
	
	return cell;
}

unsigned int GoldStoreUI::numberOfCellsInTableView( CCTableView *table )
{
	if (DataSource.size()%2 == 0)
	{
		return DataSource.size()/2;
	}
	else
	{
		return DataSource.size()/2 + 1;
	}
}

void GoldStoreUI::update(float dt)
{
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	l_ingot_value->setText(str_ingot);
}

void GoldStoreUI::initMarquee()
{
	// ªÒ»°¥Ú’€ŒÔ∆∑¡–±Ì
	int nDataSourceSize = DataSource.size();
	for (int i = 0; i < nDataSourceSize; i++)
	{
		bool bIsHot = DataSource.at(i)->ishot();
		if (true == bIsHot)
		{
			GoodsInfo * goodsInfo = new GoodsInfo();
			goodsInfo->CopyFrom(DataSource.at(i)->goods());

			m_vector_goodsInfo.push_back(goodsInfo);
		}
	}


	//GameView::getInstance()->getGoodsColorByQuality(lableGoods->goods().quality())

	// º”»ÎµΩ“™œ‘ æµƒ◊÷∑˚¥Æ÷–
	const char * str_tip = StringDataManager::getString("goldStore_buy_marquee");
	const char * str_tip_left = StringDataManager::getString("goldStore_buy_goodsInfoShowBegin");
	const char * str_tip_right = StringDataManager::getString("goldStore_buy_goodsInfoShowEnd");
	const char * str_tip_seperateSignal = StringDataManager::getString("goldStore_buy_dunhao");
	const char * str_tip_fullstopSignal = StringDataManager::getString("goldStore_buy_junhao");

	m_strMarquee = "";
	m_strMarquee.append(str_tip);

	int nVectorSize = m_vector_goodsInfo.size();
	for (int i = 0; i < nVectorSize; i++)
	{
		std::string strGoodsName = m_vector_goodsInfo.at(i)->name();
		ccColor3B colorFont = GameView::getInstance()->getGoodsColorByQuality(m_vector_goodsInfo.at(i)->quality());
		
		std::string strGoodsNameWithColor = StrUtils::applyColor(strGoodsName.c_str(), colorFont);
		std::string strTipLeft = StrUtils::applyColor(str_tip_left, colorFont);
		std::string strTipRight = StrUtils::applyColor(str_tip_right, colorFont);

		m_strMarquee.append(strTipLeft.c_str());
		m_strMarquee.append(strGoodsNameWithColor.c_str());
		m_strMarquee.append(strTipRight.c_str());

		if (i == nVectorSize - 1)
		{
			m_strMarquee.append(str_tip_fullstopSignal);
		}
		else 
		{
			m_strMarquee.append(str_tip_seperateSignal);
		}
	}

	int nPositionX = m_imageView_horn->getPosition().x + m_imageView_horn->getContentSize().width;
	int nPositionY = m_imageView_horn->getPosition().y;

	GoldStoreMarquee  * m_marqueeLabel = GoldStoreMarquee::create(nPositionX, nPositionY, MARQUEE_LABLE_WIDTH, 30);
	m_marqueeLabel->initLabelStr(m_strMarquee.c_str(), 25);
	u_layer->addChild(m_marqueeLabel);
}

void GoldStoreUI::requestPay()
{
    CCHttpRequest* request = new CCHttpRequest();
    //std::string url = "http://210.14.129.115";
    std::string url = Login::s_loginserver_ip;
    url.append(":6666/gen_order");
    request->setUrl(url.c_str());
    request->setRequestType(CCHttpRequest::kHttpPost);
    request->setResponseCallback(this, httpresponse_selector(GoldStoreUI::onHttpResponsePay));
    
    //    Nd91LoginReq* nd91LoginReq = new Nd91LoginReq();
    //    nd91LoginReq->set_uin(strLoginUin);
    //    nd91LoginReq->set_sessionid(strSessionId);
    
    //    const char * charLoginUin = strLoginUin.c_str();
    //
    //    LaoHuLoginReq * laohuLoginReq = new LaoHuLoginReq();
    //    laohuLoginReq->set_userid(atoll(charLoginUin));
    //    laohuLoginReq->set_token(strSessionId);
    //
    //    LoginReq httpReq;
    //    //httpReq.set_account(pAccountInputBox->getInputString());
    //    //httpReq.set_authenticid(pPinInputBoxr->getInputString());
    //    httpReq.set_account(strAccount);
    //    httpReq.set_accountnum(0);
    //    httpReq.set_accountpt("test");
    //    httpReq.set_imei("test");
    //    httpReq.set_ua("test");
    //    httpReq.set_channelid(LoginReq_Channel_LAOHU);
    //    httpReq.set_batchid(0);
    //    httpReq.set_recommendid(0);
    //    httpReq.set_systemversion("test");
    //    httpReq.set_nettype(0);
    //    httpReq.set_apkwithres(0);
    //    httpReq.set_platformuserid("test");
    //    //httpReq.set_allocated_nd91loginreq(nd91LoginReq);
    //    httpReq.set_allocated_laohuloginreq(laohuLoginReq);
    
    //    NSString * strLoginUin = [[WmComPlatform defaultPlatform] loginUin];
    //    NSString * strSessionId = [[WmComPlatform defaultPlatform] sessionId];
    //
    //    const char * charLoginUin = [strLoginUin UTF8String];
    //    const char * charSessionId = [strSessionId UTF8String];
    
    char charServerId[20];
    sprintf(charServerId, "%d", Login::m_pServerListRsp->serverinfos(LoginLayer::getCurrentServerId()).serverid());
    
    char charRoleId[20];
    sprintf(charRoleId, "%ld", LoginLayer::mRoleId);
    

    
    GenOrderReq genOrderReq;
    genOrderReq.set_channelid(3);                           // 注意channelid
    genOrderReq.set_userid(Login::userId);
    genOrderReq.set_sessionid(Login::sessionId);
    genOrderReq.set_commodity(GenOrderReq_Commodity_GOLDINGOT);
    //genOrderReq.set_commodity(GenOrderReq_Commodity_VIP2);
    genOrderReq.set_serverid(Login::m_pServerListRsp->serverinfos(LoginLayer::getCurrentServerId()).serverid());
    genOrderReq.set_roleid(LoginLayer::mRoleId);
    
    string msgData;
    genOrderReq.SerializeToString(&msgData);
    
    request->setRequestData(msgData.c_str(), msgData.size());
    request->setTag("POST");
    CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
    CCHttpClient::getInstance()->send(request);
    request->release();
}

void GoldStoreUI::onHttpResponsePay(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag()))
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed())
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
    
    // 返回的协议
	GenOrderRsp* m_pGenOrderRsp = new GenOrderRsp();
	m_pGenOrderRsp->ParseFromString(strdata);
    
    // 返回的订单号
    long long tmpNum = m_pGenOrderRsp->ordernum();
    
    char charOrderNum[20];
    sprintf(charOrderNum, "%lld", tmpNum);

    //[ExtenalClass set_PayNum:tmpNum];
    
    // 返回的商品名称
    std::string strGoodsName = m_pGenOrderRsp->commodity();
    NSString * NsGoodsName = [NSString stringWithUTF8String:strGoodsName.c_str()];
    
    // 返回的商品价钱
    int nGoodsPrice = m_pGenOrderRsp->price();
    
    
    
    
    if (0 == tmpNum)
    {
        // 提示信息
        NSString * strPayErrorTip = @"订单请求失败，请重试或联系客服";
        
        const char * charPayErrorTip = [strPayErrorTip UTF8String];
        
        GameView::getInstance()->showAlertDialog(charPayErrorTip);
        
        return;
    }
    

    // 支付方法
    //[[WmComPlatform defaultPlatform] WmUniPay:[[ExtenalClass sharedIntance] getBuyInfoFromUI:1:@"春哥"]];
    //[[WmComPlatform defaultPlatform] WmUniPay:[[ExtenalClass sharedIntance] getBuyInfo:nGoodsPrice:NsGoodsName]];
    
    NdBuyInfo *pBuyInfo = [[NdBuyInfo new] autorelease];
    pBuyInfo.cooOrderSerial = [NSString stringWithUTF8String:charOrderNum];         // 订单号
    pBuyInfo.productId = @"1";                                                      // 自定义的产品ID
    pBuyInfo.productName = @"1元宝";                                                 // 产品名称
    pBuyInfo.productPrice = 0.01f;                                                  // 产品现价，价格大于等于0.01，支持价格以此为准
    pBuyInfo.productOrignalPrice = 0.01f;                                           // 产品原价，价格同现价保持一致
    pBuyInfo.productCount = 1;                                                      // 产品数量
    pBuyInfo.payDescription = @"chibi";                                             // 服务器分区，不超过20个字符，只允许英文或数字
    
    // 发起请求并检查返回值。注意！该返回值并不是交易结果！（此处只是提交订单前的检验）
    int resCode = [[NdComPlatform defaultPlatform] NdUniPayAsyn:pBuyInfo];
    if (resCode < 0)
    {
        // 输入参数有错！无法提交购买请求
        // 提示信息
        NSString * strPayErrorTip = @"输入参数有错！无法提交购买请求";
        
        const char * charPayErrorTip = [strPayErrorTip UTF8String];
        
        GameView::getInstance()->showAlertDialog(charPayErrorTip);
        
        return;
    }
    
    
}
