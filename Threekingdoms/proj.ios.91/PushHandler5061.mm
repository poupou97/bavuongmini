
#include "PushHandler5061.h"

#include "../Classes/messageclient/protobuf/CollectMessage.pb.h"
#include "../Classes/gamescene_state/GameSceneState.h"
#include "../Classes/gamescene_state/role/PickingActor.h"
#include "../Classes/messageclient/element/CPickingInfo.h"
#include "../Classes/GameView.h"
#include "../Classes/legend_engine/GameWorld.h"
#include "../Classes/messageclient/element/CPushCollectInfo.h"
#include "../Classes/gamescene_state/role/MyPlayer.h"
#include "../Classes/messageclient/protobuf/RecruitMessage.pb.h"
#include "../Classes/gamescene_state/MainScene.h"
#include "../Classes/ui/generals_ui/GeneralsUI.h"
#include "../Classes/ui/generals_ui/GeneralsRecuriteUI.h"
#include "../Classes/messageclient/element/CGeneralDetail.h"
#include "../Classes/ui/generals_ui/RecuriteActionItem.h"
#include "../Classes/utils/GameUtils.h"
#include "../Classes/messageclient/GameMessageProcessor.h"
#include "../Classes/ui/generals_ui/GeneralsListUI.h"
#include "../Classes/messageclient/element/CActionDetail.h"

#include "NotificationClass.h"

IMPLEMENT_CLASS(PushHandler5061)

PushHandler5061::PushHandler5061() 
{

}
PushHandler5061::~PushHandler5061() 
{

}
void* PushHandler5061::createInstance()
{
	return new PushHandler5061() ;
}
void PushHandler5061::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5061::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5061::handle(CommonMessage* mb)
{
	ResRecruit5061 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	ActionType actionType = bean.type();
	long long cdTime = bean.cdtime();
	int playerRemianNumber = bean.playerremiannumber();

	for(int i = 0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		CActionDetail * temp = GameView::getInstance()->actionDetailList.at(i);
		if (actionType == temp->type())
		{
			if (actionType == BASE)
			{
				temp->set_playerremainnumber(playerRemianNumber);
			}

			if (cdTime > 0)
			{
				temp->set_cdtime(cdTime);
			}	
		}
	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	if (scene->getMainUIScene()->getChildByTag(kTagGeneralsUI) != NULL)
	{
		GeneralsUI * generalui = (GeneralsUI *)scene->getMainUIScene()->getChildByTag(kTagGeneralsUI);
		if(actionType == TEN_TIMES)
		{
			std::vector<CGeneralDetail * >tempVector ;
			for (int i = 0;i<bean.generals_size();++i)
			{
				CGeneralDetail * tempGD = new CGeneralDetail();
				tempGD->CopyFrom(bean.generals(i));
				tempVector.push_back(tempGD);
			}
			//œ‘ æ Æ¡¨≥ÈµƒΩ·π˚ΩÁ√Ê
			GeneralsUI::generalsRecuriteUI->presentTenTimesRecruiteResult(tempVector);

			std::vector<CGeneralDetail*>::iterator iter_generalDetail;
			for (iter_generalDetail = tempVector.begin(); iter_generalDetail != tempVector.end(); ++iter_generalDetail)
			{
				delete *iter_generalDetail;
			}
			tempVector.clear();
		}
		else
		{
			if (bean.generals_size() == 1)
			{
				CGeneralDetail * tempData = new CGeneralDetail();
				tempData->CopyFrom(bean.generals(0));
				GeneralsUI::generalsRecuriteUI->presentRecruiteResult(tempData,bean.type());
				delete tempData;
			}
			else
			{}
		}
		GeneralsUI::generalsListUI->isReqNewly = true;
		GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
		temp->page = 0;
		temp->pageSize = GeneralsUI::generalsListUI->everyPageNum;
		temp->type = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
		delete temp;
	}
    
    // 武将招募 本地推送
    std::vector<CActionDetail*> actionDetailList = GameView::getInstance()->actionDetailList;
	int nSize = actionDetailList.size();
	if (4 == nSize)
	{
		for (int i = 0; i < nSize; i++)
		{
			CActionDetail * pActionDetail = actionDetailList.at(i);
			if (BASE == pActionDetail->type())
			{
                [NotificationClass unregisterNotificationGeneral:1];
			}
			else if (BETTER == pActionDetail->type())
			{
				[NotificationClass unregisterNotificationGeneral:2];
			}
			else if (BEST == pActionDetail->type())
			{
				[NotificationClass unregisterNotificationGeneral:3];
			}
		}
        
		for (int i = 0; i < nSize; i++)
		{
			CActionDetail * pActionDetail = actionDetailList.at(i);
			if (BASE == pActionDetail->type())
			{
				long long longCDTime = pActionDetail->cdtime();
                [NotificationClass pushNotificationGeneral:1 andCDTime:longCDTime];
			}
			else if (BETTER == pActionDetail->type())
			{
				long long longCDTime = pActionDetail->cdtime();
				[NotificationClass pushNotificationGeneral:2 andCDTime:longCDTime];
			}
			else if (BEST == pActionDetail->type())
			{
				long long longCDTime = pActionDetail->cdtime();
				[NotificationClass pushNotificationGeneral:3 andCDTime:longCDTime];
			}
		}
	}
}
