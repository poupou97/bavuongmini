
#include "PushHandler5060.h"

#include "../Classes/messageclient/protobuf/CollectMessage.pb.h"
#include "../Classes/gamescene_state/GameSceneState.h"
#include "../Classes/gamescene_state/role/PickingActor.h"
#include "../Classes/messageclient/element/CPickingInfo.h"
#include "../Classes/GameView.h"
#include "../Classes/legend_engine/GameWorld.h"
#include "../Classes/messageclient/element/CPushCollectInfo.h"
#include "../Classes/gamescene_state/role/MyPlayer.h"
#include "../Classes/messageclient/protobuf/RecruitMessage.pb.h"
#include "../Classes/messageclient/element/CActionDetail.h"
#include "../Classes/gamescene_state/MainScene.h"
#include "../Classes/ui/generals_ui/GeneralsUI.h"
#include "../Classes/messageclient/GameMessageProcessor.h"

#include "../Classes/ui/generals_ui/GeneralsListUI.h"
#include "NotificationClass.h"

IMPLEMENT_CLASS(PushHandler5060)

PushHandler5060::PushHandler5060() 
{

}
PushHandler5060::~PushHandler5060() 
{

}
void* PushHandler5060::createInstance()
{
	return new PushHandler5060() ;
}
void PushHandler5060::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5060::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5060::handle(CommonMessage* mb)
{
	PushRecruitPreview5060 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	std::vector<CActionDetail*>::iterator iter_actionDetail;
	for (iter_actionDetail = GameView::getInstance()->actionDetailList.begin(); iter_actionDetail != GameView::getInstance()->actionDetailList.end(); ++iter_actionDetail)
	{
		delete *iter_actionDetail;
	}
	GameView::getInstance()->actionDetailList.clear();

	for (int i = 0;i<bean.actions_size();++i)
	{
		CActionDetail * actionDetail = new CActionDetail();
		actionDetail->CopyFrom(bean.actions(i));
		GameView::getInstance()->actionDetailList.push_back(actionDetail);
	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	if (MainScene::GeneralsScene)
	{

	}
	else
	{
		MainScene::GeneralsScene = GeneralsUI::create();
		MainScene::GeneralsScene->retain();
	}

	// «Î«Û’Û∑®¡–±Ì
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5071, this);
	//Œ‰Ω´¡–±Ì 
	GeneralsUI::generalsListUI->isReqNewly = true;
	GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
	temp->page = 0;
	temp->pageSize = GeneralsUI::generalsListUI->everyPageNum;
	temp->type = 1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
	delete temp;
    
    
    // 武将招募 本地推送
    std::vector<CActionDetail*> actionDetailList = GameView::getInstance()->actionDetailList;
	int nSize = actionDetailList.size();
	if (4 == nSize)
	{
		for (int i = 0; i < nSize; i++)
		{
			CActionDetail * pActionDetail = actionDetailList.at(i);
			if (BASE == pActionDetail->type())
			{
                [NotificationClass unregisterNotificationGeneral:1];
			}
			else if (BETTER == pActionDetail->type())
			{
				[NotificationClass unregisterNotificationGeneral:2];
			}
			else if (BEST == pActionDetail->type())
			{
				[NotificationClass unregisterNotificationGeneral:3];
			}
		}
        
		for (int i = 0; i < nSize; i++)
		{
			CActionDetail * pActionDetail = actionDetailList.at(i);
			if (BASE == pActionDetail->type())
			{
				long long longCDTime = pActionDetail->cdtime();
                [NotificationClass pushNotificationGeneral:1 andCDTime:longCDTime];
			}
			else if (BETTER == pActionDetail->type())
			{
				long long longCDTime = pActionDetail->cdtime();
				[NotificationClass pushNotificationGeneral:2 andCDTime:longCDTime];
			}
			else if (BEST == pActionDetail->type())
			{
				long long longCDTime = pActionDetail->cdtime();
				[NotificationClass pushNotificationGeneral:3 andCDTime:longCDTime];
			}
		}
	}
}
