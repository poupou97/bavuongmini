//
//  ExtenalClass.m
//  Threekingdoms
//
//  Created by 刘亮 on 14-4-30.
//
//

#import "ExtenalClass.h"

#import <NdComPlatform/NdComPlatform.h>
#import <NdComPlatform/NdComPlatformAPIResponse.h>
#import <NdComPlatform/NdCPNotifications.h>

#import "DemoComFunc.h"
#import "../Classes/login_state/Login.h"
#import "../Classes/login_state/LoginState.h"
#import "../Classes/messageclient/GameMessageProcessor.h"

#import "../Classes/GameView.h"


static ExtenalClass *sharedObj = nil;

static int s_nCharge = 0;

@implementation ExtenalClass

+(ExtenalClass*) sharedIntance
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            [[self alloc] init];
        }
    }
    return sharedObj;
}

+(id) allocWithZone:(struct _NSZone *)zone
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            sharedObj = [super allocWithZone:zone];
            return sharedObj;
        }
    }
    
    return nil;
}

-(id) copyWithZone:(NSZone *)zone
{
    return self;
}

-(id) retain
{
    return self;
}

-(unsigned) retainCount
{
    return UINT_MAX;
}

-(oneway void) release
{
    
}

-(id) autorelease
{
    return self;
}

-(id) init
{
    @synchronized(self)
    {
        [super init];
        return self;
    }
}

+(int) get_Charge
{
    return s_nCharge;
}

+(void) set_Charge:(int)nCharge
{
    s_nCharge = nCharge;
}


-(void)NdLoginResult:(NSNotification *)notify
{
    NSDictionary *dict = [notify userInfo];
	BOOL success = [[dict objectForKey:@"result"] boolValue];
	NdGuestAccountStatus* guestStatus = (NdGuestAccountStatus*)[dict objectForKey:@"NdGuestAccountStatus"];

	
    //登录成功后处理
	if([[NdComPlatform defaultPlatform] isLogined] && success)
    {
		
		//也可以通过[[NdComPlatform defaultPlatform] getCurrentLoginState]判断是否游客登录状态
//		if (guestStatus)
//        {
//			NSString* strUin = [[NdComPlatform defaultPlatform] loginUin];
//			NSString* strTip = nil;
//			if ([guestStatus isGuestLogined]) {
//				strTip = [NSString stringWithFormat:@"游客账号登录成功,\n uin = %@", strUin];
//			}
//			else if ([guestStatus isGuestRegistered]) {
//				strTip = [NSString stringWithFormat:@"游客成功注册为普通账号,\n uin = %@", strUin];
//			}
//			
//			if ([strTip length] > 0) {
//				[DemoComFunc messageBox: strTip];
//			}
//		}
//		else
//        {
//			// 普通账号登录成功!
//		}
        
        // LiuLiang++
        NSString* strUin = [[NdComPlatform defaultPlatform] loginUin];
        NSString* strSessionId = [[NdComPlatform defaultPlatform] sessionId];
        
        const char * charUin = [strUin UTF8String];
        const char * charSessionId = [strSessionId UTF8String];
        
        //Login::getLoginUinAndSessionId(charUin, charSessionId);
        //Login::getInstance()->getLoginUinAndSessionId(charUin, charSessionId);
        Login::getInstance()->getLoginInfo("", charUin, charSessionId);
        
		//[self updateView];
		//[self dismissModalViewControllerAnimated:YES];
	}
	//登录失败处理和相应提示
	else
    {
		int error = [[dict objectForKey:@"error"] intValue];
		NSString* strTip = [NSString stringWithFormat:@"登录失败, error=%d", error];
		switch (error)
        {
			case ND_COM_PLATFORM_ERROR_USER_CANCEL://用户取消登录
				if (([[NdComPlatform defaultPlatform] getCurrentLoginState] == ND_LOGIN_STATE_GUEST_LOGIN))
                {
					strTip =  @"当前仍处于游客登录状态";
				}
				else
                {
					strTip = @"用户取消登录";
                    
                    cocos2d::CCUserDefault::sharedUserDefault()->flush();
                    GameMessageProcessor::sharedMsgProcessor()->sendReq(1002);
                    
                    GameState* pScene = new LoginGameState();
                    if (pScene)
                    {
                        pScene->runThisState();
                        pScene->release();
                    }
                    
                    //runLogin();
                    
                    //LoginLayer * pLoginLayer = (LoginLayer*)CCDirector::sharedDirector()->getRunningScene();
//                    if (pLoginLayer == nil)
//                    {
////                        runLogin();
//                        
//                        cocos2d::CCUserDefault::sharedUserDefault()->flush();
//                        GameMessageProcessor::sharedMsgProcessor()->sendReq(1002);
//                    }
//                    else
//                    {
////                        cocos2d::CCUserDefault::sharedUserDefault()->flush();
////                        GameMessageProcessor::sharedMsgProcessor()->sendReq(1002);
//                        
//                        runLogin();
//                    }
                    
//                    if ((LoginLayer*)CCDirector::sharedDirector()->getRunningScene() != nil)
//                    {
//                        runLogin();
//                    }
//                    else
//                    {
//                        cocos2d::CCUserDefault::sharedUserDefault()->flush();
//                        GameMessageProcessor::sharedMsgProcessor()->sendReq(1002);
//                    }
                    
                    //return;
                    
				}
				break;
				
				// {{ for demo tip
			case ND_COM_PLATFORM_ERROR_APP_KEY_INVALID://appId未授权接入, 或appKey 无效
				strTip = @"登录失败, 请检查appId/appKey";
				break;
			case ND_COM_PLATFORM_ERROR_CLIENT_APP_ID_INVALID://无效的应用ID
				strTip = @"登录失败, 无效的应用ID";
				break;
			case ND_COM_PLATFORM_ERROR_HAS_ASSOCIATE_91:
				strTip = @"有关联的91账号，不能以游客方式登录";
				break;
				
				// }}
			default:
				break;
		}
		//[DemoComFunc messageBox:strTip];
        
        // 提示信息
        //NSString * strLoginTip = @"登录中，请稍后...";
        
        const char * charLoginTip = [strTip UTF8String];
        
        GameView::getInstance()->showAlertDialog(charLoginTip);
	}
}

-(void) NdInitResult:(NSNotification *)notify
{
    cocos2d::CCApplication::sharedApplication()->run();
    
    // 设置 工具条的方位
    //[[NdComPlatform defaultPlatform] NdShowToolBar:NdToolBarAtMiddleRight];
    //[[NdComPlatform defaultPlatform] NdHideToolBar];
}

-(void)NdUniPayAysnResult:(NSNotification *)notify
{
    
}

@end

