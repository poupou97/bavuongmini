//
//  ExtenalClass.h(For 91SDK)
//  Threekingdoms
//
//  Created by 刘亮 on 14-4-30.
//
//

#import <Foundation/Foundation.h>

@interface NotificationClass : NSObject

+(NotificationClass*) sharedIntance;
+(id) allocWithZone:(struct _NSZone *)zone;
-(id) copyWithZone:(NSZone *)zone;
-(id) retain;
-(unsigned) retainCount;
-(oneway void) release;
-(id) autorelease;
-(id) init;

// 显示报错信息
+(void)messageBox:(NSString*)stringTip;

+(void)checkNotification;
+(void)pushNotification;
+(void)unregisterNotification;

+(void)pushNotificationGeneral:(int)nType andCDTime:(long long)longCDTime;
+(void)unregisterNotificationGeneral:(int)nType;

//+(long long)getNotificationPowerCDTime:(int)nHour andMinute:(int)nMinute;
//+(long long)getNotificationPowerCDTime;
+(void)pushNotificationPowerSend;
+(void)pushNotificationPowerSend:(int)nId andCDTime:(long long)longCDTime;
+(void)unregisterNotificationPowerSend:(int)nType;

+(void)receiveLocalNotification;


@end
