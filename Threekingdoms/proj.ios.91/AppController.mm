#import "AppController.h"
#import "EAGLView.h"
#import "cocos2d.h"
#import "AppDelegate.h"
#import "RootViewController.h"

#import <NdComPlatform/NdComPlatform.h>

#import "ExtenalClass.h"
#import "NotificationClass.h"

@implementation AppController

#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
static AppDelegate s_sharedApplication;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after application launch.

    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    
    // Init the EAGLView
    EAGLView *__glView = [EAGLView viewWithFrame: [window bounds]
                                     pixelFormat: kEAGLColorFormatRGB565
                                     depthFormat: GL_DEPTH24_STENCIL8_OES
                              preserveBackbuffer: NO
                                      sharegroup: nil
                                   multiSampling: NO
                                 numberOfSamples: 0];

    // Use RootViewController manage EAGLView 
    viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    viewController.wantsFullScreenLayout = YES;
    viewController.view = __glView;

    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:viewController];
    }
    
    
    
    [[UIApplication sharedApplication] setStatusBarHidden:true];
	
	// to prevent the phone goes into auto-lock and auto-dimming 
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    [window makeKeyAndVisible];
    
    // LiuLiang++
    NdInitConfigure *cfg = [[[NdInitConfigure alloc] init] autorelease];
    //cfg.appid = 113949;
    //cfg.appKey = @"8872f58fdeccd0fb79bdf80204f34eef5a89d77a3c3ec762";
    cfg.appid = 114498;
    cfg.appKey = @"fecd8942f570d0c0729245cbe69e05e3a5716c81a7662c9e";
    cfg.versionCheckLevel = ND_VERSION_CHECK_LEVEL_NORMAL;
    cfg.orientation = UIInterfaceOrientationMaskLandscape;
    
    [[NdComPlatform defaultPlatform] NdInit:cfg];

    
    // 旋转方向
//    [[NdComPlatform defaultPlatform] NdSetScreenOrientation:UIInterfaceOrientationLandscapeLeft];
    [[NdComPlatform defaultPlatform] NdSetScreenOrientation:UIInterfaceOrientationMaskLandscape];
    [[NdComPlatform defaultPlatform] NdSetAutoRotation:YES];
    
    // 设置调试模式（用于自动更新时）；产品上线后，务必去掉该代码
    //[[NdComPlatform defaultPlatform] NdSetDebugMode:0];
    
    // 设置 登陆（及登陆状态）的监听
    [[NSNotificationCenter defaultCenter] addObserver:[ExtenalClass sharedIntance] selector:@selector(NdLoginResult:) name:(NSString *)kNdCPLoginNotification object:nil];
    
    // 设置 初始化结束 的监听
    [[NSNotificationCenter defaultCenter] addObserver:[ExtenalClass sharedIntance] selector:@selector(NdInitResult:) name:(NSString *)
        kNdCPInitDidFinishNotification object:nil];
    
    // 设置 购买结果 的监听
    [[NSNotificationCenter defaultCenter] addObserver:[ExtenalClass sharedIntance] selector:@selector(NdUniPayAysnResult:) name:(NSString *)kNdCPBuyResultNotification  object:nil];
    
    
//    // 设置 工具条的方位
    //[[NdComPlatform defaultPlatform] NdShowToolBar:NdToolBarAtMiddleRight];
    //[[NdComPlatform defaultPlatform] NdHideToolBar];
    
    
//    cocos2d::CCApplication::sharedApplication()->run();

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    cocos2d::CCDirector::sharedDirector()->pause();
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    cocos2d::CCDirector::sharedDirector()->resume();
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    cocos2d::CCApplication::sharedApplication()->applicationDidEnterBackground();

}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    cocos2d::CCApplication::sharedApplication()->applicationWillEnterForeground();
    
    // 目前没有效果
    [[NdComPlatform defaultPlatform] NdPause];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}

-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    return UIInterfaceOrientationMaskAll;
}




#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
    [window release];
    [super dealloc];
}

-(void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    [NotificationClass receiveLocalNotification];
}


@end
