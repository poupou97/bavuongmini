//
//  NotificationClass.mm
//  Threekingdoms
//
//  Created by 刘亮 on 14-4-30.
//
//

#import "NotificationClass.h"
#import "SetUI.h"
#import "GoldStoreUI.h"

#import "../Classes/login_state/Login.h"
#import "../Classes/login_state/LoginState.h"
#import "../Classes/messageclient/GameMessageProcessor.h"

#include "../Classes/GameView.h"

#include "../Classes/messageclient/element/CPowerSendInfo.h"
#include "../Classes/utils/StaticDataManager.h"





static NotificationClass *sharedObj = nil;

static int s_nCharge = 0;

//static NSString *s_guestAccount = nil;
//
//static long long s_payNum = 0;
//
//static NSString *s_nsMacStr = nil;
//
//static int s_nMachineType = 0;


@implementation NotificationClass

+(NotificationClass*) sharedIntance
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            [[self alloc] init];
        }
    }
    return sharedObj;
}

+(id) allocWithZone:(struct _NSZone *)zone
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            sharedObj = [super allocWithZone:zone];
            return sharedObj;
        }
    }
    
    return nil;
}

-(id) copyWithZone:(NSZone *)zone
{
    return self;
}

-(id) retain
{
    return self;
}

-(unsigned) retainCount
{
    return UINT_MAX;
}

-(oneway void) release
{
    
}

-(id) autorelease
{
    return self;
}

-(id) init
{
    @synchronized(self)
    {
        [super init];
        return self;
    }
}

+(void)checkNotification
{
//    UIApplication * pApp = [UIApplication sharedApplication];
//    
//    // 获取本地推送数组
//    NSArray * localArr = [pApp scheduledLocalNotifications];
//    
//    // 声明本地通知对象
//    UILocalNotification * localNotification;
//    
//    if (localArr)
//    {
//        for (UILocalNotification * noti in localArr)
//        {
//            NSDictionary * dict = noti.userInfo;
//            if (dict)
//            {
//                NSString * inKey = [dict objectForKey:@"战三国"];
//                if ([inKey isEqualToString:@"战三国"])
//                {
//                    if (localNotification)
//                    {
//                        //[localNotification release];
//                        //localNotification = nil;
//                        //GameView::getInstance()->showAlertDialog("111");
//                        NSLog(@"11");
//                    }
//                    
//                    //localNotification = [noti reatin];
//                    
//                    // GameView::getInstance()->showAlertDialog("222");
//                    NSLog(@"22");
//                    
//                    break;
//                }
//            }
//        }
//        
//        // 判断是否找到已经存在的相同的key的推送
//        if (!localNotification)
//        {
//            // 不存在 初始化
//            //localNotification = [[UILocalNotification alloc] init];
//            
//             //GameView::getInstance()->showAlertDialog("22");
//            NSLog(@"33");
//        }
//        
//        
//    }
    
    
    UIApplication * pApp = [UIApplication sharedApplication];
    
    // 获取本地推送数组
    NSArray * localArr = [pApp scheduledLocalNotifications];
    
    // 声明本地通知对象
    UILocalNotification * localNotification;
    
    if (localArr)
    {
        for (UILocalNotification * noti in localArr)
        {
//            NSString * nsStringBody = noti.alertBody;
//            NSLog(nsStringBody);
            
            NSDictionary * dict = noti.userInfo;
            if (dict)
            {
                NSString * inKey = [dict objectForKey:@"key"];
                
                NSLog(inKey);
            }
        }
    }
}

+(void)pushNotification
{
    // 设置10秒之后
    NSDate * date = [NSDate dateWithTimeIntervalSinceNow:10];
    
    // 创建一个本地推送
    UILocalNotification * noti = [[[UILocalNotification alloc] init] autorelease];
    if (noti)
    {
        // 设置推送时间
        noti.fireDate = date;
        
        // 设置时区
        noti.timeZone = [NSTimeZone defaultTimeZone];
        
        // 设置重复间隔
        noti.repeatInterval = NSCalendarUnitMinute;
        
        // 设置推送声音
        noti.soundName = UILocalNotificationDefaultSoundName;
        
        // 内容
        noti.alertBody = @"我是刘亮";
        
        // 显示在icon上的红色圈中的数字
        noti.applicationIconBadgeNumber = 1;
        
        // 设置userInfo 方便在之后需要撤销的时候使用
        NSDictionary * infoDic = [NSDictionary dictionaryWithObject:@"liuliang" forKey:@"nihao"];
        noti.userInfo = infoDic;
        
        // 添加推送到UIApplication
        UIApplication * app = [UIApplication sharedApplication];
        [app scheduleLocalNotification:noti];
    }
    
}

+(void)unregisterNotification
{
    UIApplication * pApp = [UIApplication sharedApplication];
    
    // 获取本地推送数组
    NSArray * localArr = [pApp scheduledLocalNotifications];
    
    // 声明本地通知对象
    UILocalNotification * localNotification;
    
    if (localArr)
    {
        for (UILocalNotification * noti in localArr)
        {

            NSDictionary * dict = noti.userInfo;
            if (dict)
            {
                NSString * inKey = [dict objectForKey:@"nihao"];
                
                const char * charKey = [inKey UTF8String];
                
                int a = 20;
                char charStr[20];
                sprintf(charStr, "%d", a);
                
                if ([inKey isEqualToString:@"liuliang"])
                {
                    if (localNotification)
                    {
                        [localNotification release];
                        localNotification = nil;
                        
                        NSLog(@"11");
                    }
                    
                    localNotification = [noti retain];

                    NSLog(@"22");
                                       
                    break;
                }
            }
        }
        
        // 判断是否找到已经存在的相同key的推送
        if (!localNotification)
        {
            //不存在初始化
            localNotification = [[UILocalNotification alloc] init];
            
             NSLog(@"4444");
        }
        
        if (localNotification)
        {
            // 不推送 取消推送
            [pApp cancelLocalNotification:localNotification];
            [localNotification release];
            
            pApp.applicationIconBadgeNumber = 0;
            
            NSLog(@"55555");
            
            return ;
        }
        
    }
    
    NSLog(@"333");
}

+(void)pushNotificationGeneral:(int)nType andCDTime:(long long)longCDTime
{
    // 设置推送时间（单位是秒）
    NSDate * date = [NSDate dateWithTimeIntervalSinceNow:longCDTime / 1000];
    
    // 创建一个本地推送
    UILocalNotification * noti = [[[UILocalNotification alloc] init] autorelease];
    if (noti)
    {
        UIApplication * pApp = [UIApplication sharedApplication];
        
        // 设置推送时间
        noti.fireDate = date;
        
        // 设置时区
        noti.timeZone = [NSTimeZone defaultTimeZone];
        
        // 设置重复间隔
        noti.repeatInterval = NSCalendarUnitYear;
        
        // 设置推送声音
        noti.soundName = UILocalNotificationDefaultSoundName;
        
//        // 内容
//        noti.alertBody = @"现在可以免费招募武将了，机会难得，不要错过哦！";
        
        // 显示在icon上的红色圈中的数字
        //noti.applicationIconBadgeNumber = pApp.applicationIconBadgeNumber + 1;
        if (0 == pApp.applicationIconBadgeNumber)
        {
            noti.applicationIconBadgeNumber = 1;
        }
        
        
        NSDictionary * infoDic = nil;
        
        if (1 == nType)
        {
            // 内容
            noti.alertBody = @"现在可以免费【普通招募】武将了，机会难得，不要错过哦！";
            
            infoDic = [NSDictionary dictionaryWithObject:@"base" forKey:@"general"];
        }
        else if (2 == nType)
        {
            // 内容
            noti.alertBody = @"现在可以免费【优秀招募】武将了，机会难得，不要错过哦！";
            
            infoDic = [NSDictionary dictionaryWithObject:@"better" forKey:@"general"];
        }
        else if (3 == nType)
        {
            // 内容
            noti.alertBody = @"现在可以免费【极品招募】武将了，机会难得，不要错过哦！";
            
            infoDic = [NSDictionary dictionaryWithObject:@"best" forKey:@"general"];
        }
        
        if (infoDic)
        {
            noti.userInfo = infoDic;
        }
        else
        {
            // 异常
            NSLog(@"error pushNotifaction");
            
            return;
        }
        
        NSLog(@"u55555");
        
        // 添加推送到UIApplication
        [pApp scheduleLocalNotification:noti];
    }
}

+(void)unregisterNotificationGeneral:(int)nType
{
    UIApplication * pApp = [UIApplication sharedApplication];
    
    // 获取本地推送数组
    NSArray * localArr = [pApp scheduledLocalNotifications];
    
    // 声明本地通知对象
    UILocalNotification * localNotification;
    
    if (localArr)
    {
        for (UILocalNotification * noti in localArr)
        {
            
            NSDictionary * dict = noti.userInfo;
            if (dict)
            {
                NSString * inKey = [dict objectForKey:@"general"];
                
                NSString * nsStringName = nil;
                
                if (1 == nType)
                {
                    nsStringName = @"base";
                }
                else if (2 == nType)
                {
                    nsStringName = @"better";
                }
                else if (3 == nType)
                {
                    nsStringName = @"best";
                }
                else
                {
                    NSLog(@"unregister general push error");
                    
                    return;
                }
                
                if ([inKey isEqualToString:nsStringName])
                {
                    if (localNotification)
                    {
                        [localNotification release];
                        localNotification = nil;
                        
                        NSLog(@"u11");
                    }
                    
                    localNotification = [noti retain];
                    
                    NSLog(@"u22");
                    
                    break;
                }
            }
        }
        
        // 判断是否找到已经存在的相同key的推送
        if (!localNotification)
        {
            //不存在初始化
            localNotification = [[UILocalNotification alloc] init];
            
            NSLog(@"u4444");
        }
        
        if (localNotification)
        {
            // 不推送 取消推送
            [pApp cancelLocalNotification:localNotification];
            [localNotification release];
            
            if (pApp.applicationIconBadgeNumber > 0)
            {
                pApp.applicationIconBadgeNumber = pApp.applicationIconBadgeNumber - 1;
            }
            
            NSLog(@"u55555");
            
            return ;
        }
        
    }
    
    NSLog(@"u333");
}

+(void)pushNotificationPowerSend
{
    int nSize = PowerSendConfig::s_map_powerSendConfig.size();
    for (int i = 0; i < nSize; i++)
    {
        CPowerSendInfo * pPowerSendInfo = PowerSendConfig::s_map_powerSendConfig[i + 1];
        int nId = pPowerSendInfo->get_id();
        std::string strSendTime = pPowerSendInfo->get_sendTime();
        //int nPowerValue = pPowerSendInfo->get_powerValue();
        
        int nIndex = strSendTime.find(":");
        if (nIndex > 0 && nIndex + 1 != strSendTime.size())
        {
            std::string strHour = strSendTime.substr(0, nIndex);
            std::string strMinute = strSendTime.substr(nIndex + 1, strSendTime.size() - 1);
            
            NSString * nsHour = [NSString stringWithUTF8String:strHour.c_str()];
            NSString * nsMinute = [NSString stringWithUTF8String:strMinute.c_str()];
            
            NSLog(@"Hour:%@", nsHour);
            NSLog(@"Minute:%@", nsMinute);
            
            int nTmpHour = atoi(strHour.c_str());
            int nTmpMinute = atoi(strMinute.c_str());
            
            NSLog(@"nHour:%d", nTmpHour);
            NSLog(@"nMinute:%d", nTmpMinute);
            
            // 取消监听
            [NotificationClass unregisterNotificationPowerSend:nId];
            
            
            
            NSCalendar *calendar = [NSCalendar currentCalendar];
            NSDate *date = [NSDate date];
            //NSDateComponents *compt = [calendar components:NSDayCalendarUnit fromDate:date];
            NSDateComponents *compt = [calendar components:(NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit|NSHourCalendarUnit|NSMinuteCalendarUnit|NSSecondCalendarUnit) fromDate:date];
            
            //NSLog(@"%d,%@",[compt year],date);
            // NSLog(@"%d,%@",[compt month],date);
            // NSLog(@"%d,%@",[compt day],date);
            NSLog(@"%d,%@",[compt hour],date);
            NSLog(@"%d,%@",[compt minute],date);
            NSLog(@"%d,%@",[compt second],date);
            
            int nTargetHour = nTmpHour;
            int nTargetMinute = nTmpMinute;
            int nTargetSecond = 60;
            
            int nNowHour = [compt hour];
            int nNowMinute = [compt minute];
            int nNowSecond = [compt second];
            
            int nMinusHour = 0;
            int nMinusMinute = 0;
            int nMinusSecond = 0;
            
            long long nAllSecond = 0;
            
            // 当天更新
            if (nNowHour < nTargetHour || (nNowHour == nTargetHour && nNowMinute <= nTargetMinute))
            {
                //if (nTargetHour > 0)
                {
                    nTargetHour = nTargetHour - 1;
                    nTargetMinute = nTargetMinute + 59;
                    nTargetSecond = nTargetSecond + 60;
                    
                    nMinusHour = nTargetHour - nNowHour;
                    nMinusMinute = nTargetMinute - nNowMinute;
                    nMinusSecond = nTargetSecond - nNowSecond;
                    
                    nAllSecond = nMinusHour * 60 * 60 + nMinusMinute * 60 + nMinusSecond;
                    
                    NSLog(@"小时:%d",nMinusHour);
                    NSLog(@"分钟:%d",nMinusMinute);
                    NSLog(@"秒:%d",nMinusSecond);
                    NSLog(@"总共多少秒:%lld",nAllSecond);
                }
            }
            else    // 隔天更新
            {
                int nTmpHour = 0;
                int nTmpMinute = 0;
                int nTmpSecond = 0;
                
                nTmpHour = 23 - nNowHour;
                nTmpMinute = 59 - nNowMinute;
                nTmpSecond = 60 - nNowSecond;
                
                NSLog(@"tmp小时:%d",nTmpHour);
                NSLog(@"tmp分钟:%d",nTmpMinute);
                NSLog(@"tmp秒:%d",nTmpSecond);
                
                
                nMinusHour = nTmpHour + nTargetHour;
                nMinusMinute = nTmpMinute + nTargetMinute;
                nMinusSecond = nTmpSecond + nTargetSecond;
                
                nAllSecond = nMinusHour * 60 * 60 + nMinusMinute * 60 + nMinusSecond;
                
                NSLog(@"小时:%d",nMinusHour);
                NSLog(@"分钟:%d",nMinusMinute);
                NSLog(@"秒:%d",nMinusSecond);
                NSLog(@"总共多少秒:%lld",nAllSecond);
            }

            // 注册监听
            [NotificationClass pushNotificationPowerSend:nId andCDTime:nAllSecond];
            
        }
    
    }
}

+(void)pushNotificationPowerSend:(int)nId andCDTime:(long long)longCDTime
{
    // 设置推送时间（单位是秒）
    NSDate * date = [NSDate dateWithTimeIntervalSinceNow:longCDTime] ;
    
    // 创建一个本地推送
    UILocalNotification * noti = [[[UILocalNotification alloc] init] autorelease];
    if (noti)
    {
        UIApplication * pApp = [UIApplication sharedApplication];
        
        // 设置推送时间
        noti.fireDate = date;
        
        // 设置时区
        noti.timeZone = [NSTimeZone defaultTimeZone];
        
        // 设置重复间隔
        noti.repeatInterval = NSCalendarUnitDay;
        
        // 设置推送声音
        noti.soundName = UILocalNotificationDefaultSoundName;
        
        // 内容
        noti.alertBody = @"每日领取体力时间又到了，主公一定不要错过哦，武将们都蠢蠢欲动了呢！";
        
        // 显示在icon上的红色圈中的数字
        if (0 == pApp.applicationIconBadgeNumber)
        {
            noti.applicationIconBadgeNumber = 1;
        }
        
        char charId[20];
        sprintf(charId, "%d", nId);
        
        NSString * nsStringId = [NSString stringWithUTF8String:charId];
        
        NSDictionary * infoDic = nil;
        infoDic = [NSDictionary dictionaryWithObject:nsStringId forKey:@"power"];
        
        //
        noti.userInfo = infoDic;
        
        NSLog(@"u55555");
        
        // 添加推送到UIApplication
        [pApp scheduleLocalNotification:noti];
    }
}

+(void)unregisterNotificationPowerSend:(int)nType
{
    UIApplication * pApp = [UIApplication sharedApplication];
    
    // 获取本地推送数组
    NSArray * localArr = [pApp scheduledLocalNotifications];
    
    // 声明本地通知对象
    UILocalNotification * localNotification;
    
    if (localArr)
    {
        for (UILocalNotification * noti in localArr)
        {
            
            NSDictionary * dict = noti.userInfo;
            if (dict)
            {
                NSString * inKey = [dict objectForKey:@"power"];
                
                NSString * nsStringName = nil;
                
                char charTypeName[20];
                sprintf(charTypeName, "%d", nType);
                nsStringName = [NSString stringWithUTF8String:charTypeName];
                
                if ([inKey isEqualToString:nsStringName])
                {
                    if (localNotification)
                    {
                        [localNotification release];
                        localNotification = nil;
                        
                        NSLog(@"find register for power");
                    }
                    
                    localNotification = [noti retain];
                    
                    NSLog(@"find register for power and retain");
                    
                    break;
                }
            }
        }
        
        // 判断是否找到已经存在的相同key的推送
        if (!localNotification)
        {
            //不存在初始化
            localNotification = [[UILocalNotification alloc] init];
            
            NSLog(@"u4444");
        }
        
        if (localNotification)
        {
            // 不推送 取消推送
            [pApp cancelLocalNotification:localNotification];
            [localNotification release];
            
            if (pApp.applicationIconBadgeNumber > 0)
            {
                pApp.applicationIconBadgeNumber = pApp.applicationIconBadgeNumber - 1;
            }
            
            NSLog(@"cancel push notification PowerSend");
            
            return ;
        }
        
    }
    
    NSLog(@"unregisterNotificationPowerSend end");
}

+(void)receiveLocalNotification
{
    UIApplication * pApp = [UIApplication sharedApplication];
    
    // 获取本地推送数组
    NSArray * localArr = [pApp scheduledLocalNotifications];
    
    // 声明本地通知对象
    UILocalNotification * localNotification;
    
    if (localArr)
    {
        for (UILocalNotification * noti in localArr)
        {
            
            NSDictionary * dict = noti.userInfo;
            if (dict)
            {
                NSString * inKey = [dict objectForKey:@"general"];
                
                NSLog(@"%@", inKey);
                
                if ([inKey isEqualToString:@"base"] || [inKey isEqualToString:@"better"] || [inKey isEqualToString:@"best"] )
                {
                    localNotification = [noti retain];
                    
                    // 不推送 取消推送
                    [pApp cancelLocalNotification:localNotification];
                    [localNotification release];
                    
                    if (pApp.applicationIconBadgeNumber > 0)
                    {
                        pApp.applicationIconBadgeNumber = pApp.applicationIconBadgeNumber - 1;
                    }
                    
                    NSLog(@"u22");
                    
                    return;
                }
                
                NSString * inKeyPower = [dict objectForKey:@"power"];
                NSLog(@"%@", inKeyPower);
                for (int i = 0; i < 20; i++)
                {
                    NSString * nsStringName = nil;
                    
                    char charTypeName[20];
                    sprintf(charTypeName, "%d", i + 1);
                    nsStringName = [NSString stringWithUTF8String:charTypeName];
                    
                    if ([inKeyPower isEqualToString:nsStringName])
                    {
                        localNotification = [noti retain];
                        
                        // 不推送 取消推送
                        [pApp cancelLocalNotification:localNotification];
                        [localNotification release];
                        
                        if (pApp.applicationIconBadgeNumber > 0)
                        {
                            pApp.applicationIconBadgeNumber = pApp.applicationIconBadgeNumber - 1;
                        }
                        
                        // 再次推送
                        [NotificationClass pushNotificationPowerSend];
                    }
                }
            }
        }
    }

}

@end

