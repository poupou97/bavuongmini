//
//  DemoComFunc.m
//  NdCP_Game_Demo
//
//  Created by  hiyo on 12-6-19.
//  Copyright 2012 Nd. All rights reserved.
//

#import "DemoComFunc.h"
#import <NdComPlatform/NDComPlatform.h>

@implementation DemoComFunc

+ (BOOL)isIpadProject
{
	return [[[UIDevice currentDevice] model] hasPrefix:@"iPad"] && (UIUserInterfaceIdiomPad == [UIDevice currentDevice].userInterfaceIdiom);
}

+ (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)ori
{
	return UIInterfaceOrientationIsLandscape(ori);
}

+ (void)messageBox:(NSString*)stringTip
{
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:stringTip
													message:nil
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"确定", nil];
	[alert show];
	[alert release];	
}

+ (BOOL)showMessageBoxIfNotLogin
{
	if (![[NdComPlatform defaultPlatform] isLogined]) {
		[DemoComFunc messageBox:@"请先登录"];
		return YES;
	}
	else {
		return NO;
 	}
}

#define AUTO_ROTATION  @"AUTO_ROTATION"
+ (BOOL)is91SDKAutoRotateMode
{
	NSNumber* num = [[NSUserDefaults standardUserDefaults] objectForKey:AUTO_ROTATION];
	return (num ? [num boolValue] : YES);
}

+ (void)save91SDKAutoRotateMode:(BOOL)autoRotate
{
	NSNumber* num = [NSNumber numberWithBool:autoRotate];
	[[NSUserDefaults standardUserDefaults] setObject:num forKey:AUTO_ROTATION];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

#define  ORIENTATION_KEY	@"cmplfmOrientation"
+ (UIInterfaceOrientation)orientationFor91SDK
{
    return UIInterfaceOrientationLandscapeRight;
    
	NSNumber* num = [[NSUserDefaults standardUserDefaults] objectForKey:ORIENTATION_KEY];
	return (num ? (UIInterfaceOrientation)[num intValue] : UIInterfaceOrientationLandscapeRight);
}

+ (void)saveOrientationFor91SDK:(UIInterfaceOrientation)ori
{
	switch (ori) {
		case	UIInterfaceOrientationPortrait:
		case	UIInterfaceOrientationPortraitUpsideDown:	
		case	UIInterfaceOrientationLandscapeLeft:
		case	UIInterfaceOrientationLandscapeRight:
			break;
		default:
			ori = UIInterfaceOrientationPortrait;
			break;
	}
	
	NSNumber* num = [NSNumber numberWithInt:ori];
	[[NSUserDefaults standardUserDefaults] setObject:num forKey:ORIENTATION_KEY];
	[[NSUserDefaults standardUserDefaults] synchronize];
}


#pragma mark -
#define  DEMO_APP_ID	@"appId"
#define  DEMO_APP_KEY	@"appKey"
+ (NSArray*)arrayAppInfosFromFile;
{
	NSString* str = [[NSBundle mainBundle] pathForResource:@"customedAppProperty" ofType:@"plist"];
	NSArray* arr = [NSArray arrayWithContentsOfFile:str];
	if ([arr count] <= 0) {
		NSDictionary* dic = [NSDictionary dictionaryWithObjectsAndKeys:
							 @"100010",  DEMO_APP_ID,
							 @"C28454605B9312157C2F76F27A9BCA2349434E546A6E9C75", DEMO_APP_KEY,
							 nil];
		arr = [NSArray arrayWithObject:dic];
	}
	return arr;
}

+ (BOOL)fetchAppIdAndAppKeyFromArrayAppInfos:(NSArray*)arr  withRowIndex:(NSUInteger)row 
								   destAppId:(NSMutableString*)strAppId  destAppKey:(NSMutableString*)strAppKey
{
	if (row >= [arr count]) {
		return NO;
	}
	NSDictionary* dic = [arr objectAtIndex:row];
	[strAppId setString:[dic objectForKey:DEMO_APP_ID]];
	[strAppKey setString:[dic objectForKey:DEMO_APP_KEY]];
	return YES;
}


+ (NSDictionary*)dictionaryAppInfoForDemo
{
	NSString* strAppId = [[NSUserDefaults standardUserDefaults] objectForKey:DEMO_APP_ID];
	NSString* strAppKey = [[NSUserDefaults standardUserDefaults] objectForKey:DEMO_APP_KEY];
	NSDictionary* dicCurrentAppInfo = nil;
	if (nil == strAppId || nil == strAppKey) {
		NSArray* arrInfo = [self arrayAppInfosFromFile];
		if ([arrInfo count] > 0) {
			dicCurrentAppInfo = [arrInfo objectAtIndex:0];
		}
		
		strAppId = [dicCurrentAppInfo objectForKey:DEMO_APP_ID];
		strAppKey = [dicCurrentAppInfo objectForKey:DEMO_APP_KEY];
		[self saveAppInfoForDemoWithAppId:strAppId appKey:strAppKey];
	}
	else {
		dicCurrentAppInfo = [NSDictionary dictionaryWithObjectsAndKeys:
							 strAppId,  DEMO_APP_ID,
							 strAppKey, DEMO_APP_KEY,
							 nil];
	}
	return dicCurrentAppInfo;
}

+ (NSString*)appIdForDemo
{
	NSString* strAppId = [[NSUserDefaults standardUserDefaults] objectForKey:DEMO_APP_ID];
	return strAppId ? strAppId : [[self dictionaryAppInfoForDemo] objectForKey:DEMO_APP_ID];
}

+ (NSString*)appKeyForDemo
{
	NSString* strAppKey = [[NSUserDefaults standardUserDefaults] objectForKey:DEMO_APP_KEY];
	return strAppKey ? strAppKey : [[self dictionaryAppInfoForDemo] objectForKey:DEMO_APP_KEY];
}

+ (void)saveAppInfoForDemoWithAppId:(NSString*)strAppId  appKey:(NSString*)strAppKey
{
	if ([strAppId length] > 0 && [strAppKey length] > 0) {
		[[NSUserDefaults standardUserDefaults] setObject:strAppId forKey:DEMO_APP_ID];
		[[NSUserDefaults standardUserDefaults] setObject:strAppKey forKey:DEMO_APP_KEY];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}
}

+ (NSString*)stringTipForBuyResultInfo:(NSDictionary*)dic
{
	BOOL bSuccess = [[dic objectForKey:@"result"] boolValue];
	NSString* str = bSuccess ? @"购买成功" : @"购买失败";
	
	if (!bSuccess) {
		NSString* strError = nil;
		int nErrorCode = [[dic objectForKey:@"error"] intValue];
		switch (nErrorCode) {
			case ND_COM_PLATFORM_ERROR_USER_CANCEL:
				strError = @"用户取消操作";
				break;
			case ND_COM_PLATFORM_ERROR_NETWORK_FAIL:
				strError = @"网络连接错误";
				break;
			case ND_COM_PLATFORM_ERROR_SERVER_RETURN_ERROR:
				strError = @"服务端处理失败";
				break;
			case ND_COM_PLATFORM_ERROR_ORDER_SERIAL_SUBMITTED:
				strError = @"支付订单已提交";
				break;
				
				
			case ND_COM_PLATFORM_ERROR_PARAM:
				strError = @"购买的虚拟商品91豆价格总额不合法"; //可能是虚拟商品价格为0，或者总额超过最大值
				break;
			case ND_COM_PLATFORM_ERROR_VG_MONEY_TYPE_FAILED:
				strError = @"查询虚拟商品币种失败";
				break;
			case ND_COM_PLATFORM_ERROR_VG_ORDER_FAILED:
				strError = @"获取虚拟商品订单号失败";
				break;
			case ND_COM_PLATFORM_ERROR_VG_BACK_FROM_RECHARGE:
				strError = @"有进入虚拟币直充界面";
				break;
			case ND_COM_PLATFORM_ERROR_PAY_FAILED:
				strError = @"购买虚拟商品失败";
				break;
				
			default:
				strError = [NSString stringWithFormat:@"errorCode = %d", nErrorCode];
				break;
		}
		
		if ([strError length] > 0) {
			str = [str stringByAppendingFormat:@"\n%@", strError];
		}
	}
	
	/*
	 如果购买失败，可能无法得到cooOrderSerial, productPrice等字段值
	 */
	NdBuyInfo* buyInfo = (NdBuyInfo*)[dic objectForKey:@"buyInfo"];
	str = [str stringByAppendingFormat:@"\n<productId = %@, productCount = %d, cooOrderSerial = %@>",
		   buyInfo.productId, buyInfo.productCount, buyInfo.cooOrderSerial];
	
	
	/*
	 如果购买虚拟商品失败，可以从vgErrorInfo获取具体失败详情
	 */
	NdVGErrorInfo* vgErrInfo = (NdVGErrorInfo*)[dic objectForKey:@"vgErrorInfo"];
	if (vgErrInfo) {
		str = [str stringByAppendingFormat:@"\n\n vgErr = %d, %@", vgErrInfo.nErrCode, vgErrInfo.strErrDesc];
	}
	
	return str;
}

@end
