//
//  ExtenalClass.h(For 91SDK)
//  Threekingdoms
//
//  Created by 刘亮 on 14-4-30.
//
//

#import <Foundation/Foundation.h>
#import <TBPlatform/TBPlatform.h>

@interface ExtenalClass : NSObject

+(ExtenalClass*) sharedIntance;
+(id) allocWithZone:(struct _NSZone *)zone;
-(id) copyWithZone:(NSZone *)zone;
-(id) retain;
-(unsigned) retainCount;
-(oneway void) release;
-(id) autorelease;
-(id) init;

// 显示报错信息
+(void)messageBox:(NSString*)stringTip;

-(void)TBTSdkInitFinish:(NSNotification *)notify;                    // SDK初始化完成通知
-(void)TBTLoginResult:(NSNotification *)notify;                      // 登录结果通知
-(void)TBTLogoutPlatform:(NSNotification *)notify;                   // 注销账号通知
-(void)TBTUniPayResult:(NSNotification *)notify;                     // 支付付款返回结果通知

/**
 *  查询订单结束
 *
 *  @param orderString 订单号
 *  @param amount      订单金额（单位：分）
 *  @param statusType  订单状态（详见TBPlatformDefines.h）
 */
- (void)TBCheckOrderFinishedWithOrder:(NSString *)orderString
                               amount:(int)amount
                               status:(TBCheckOrderStatusType)statusType;

/**
 *  @brief 查询订单失败（网络不通畅，或服务器返回错误）
 */
- (void)TBCheckOrderDidFailed:(NSString*)order;




//// 静态变量
//+(NSString *) s_guestAccount;                                       // 记录游客账号
//+(NSString *) s_nsMacStr;                                           // 设备号
//+(int) s_nMachineType;                                              // 设备类型（1：ios越狱，2：安卓国内；3：ios官方简体；4：ios官方繁体）
//
//// 静态方法
//+(long long) get_PayNum;                                            // get订单号
//+(void) set_PayNum:(long long)tmpPayNum;                            // set订单号
//
//+(NSString *) get_macStr;                                           // get设备号
//+(void) set_macStr:(NSString *)nsMac;                               // set设备号
//
//+(int) get_MachineType;                                             // get设备类型
//+(void) set_MachineType:(int)nMachineType;                          // set设备类型

+(int) get_Charge;                                                  // get所付金额
+(void) set_Charge:(int)nCharge;                                    // set所付金额

@end
