//
//  ExtenalClass.m
//  Threekingdoms
//
//  Created by 刘亮 on 14-4-30.
//
//

#import "ExtenalClass.h"
#import "SetUI.h"
#import "GoldStoreUI.h"

#import <TBPlatform/TBPlatform.h>

#import "../Classes/login_state/Login.h"
#import "../Classes/login_state/LoginState.h"
#import "../Classes/messageclient/GameMessageProcessor.h"

#include "../Classes/GameView.h"




static ExtenalClass *sharedObj = nil;

static int s_nCharge = 0;

//static NSString *s_guestAccount = nil;
//
//static long long s_payNum = 0;
//
//static NSString *s_nsMacStr = nil;
//
//static int s_nMachineType = 0;
@interface ExtenalClass ()<TBBuyGoodsProtocol,TBCheckOrderDelegate>

@end


@implementation ExtenalClass

+(ExtenalClass*) sharedIntance
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            [[self alloc] init];
        }
    }
    return sharedObj;
}

+(id) allocWithZone:(struct _NSZone *)zone
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            sharedObj = [super allocWithZone:zone];
            return sharedObj;
        }
    }
    
    return nil;
}

-(id) copyWithZone:(NSZone *)zone
{
    return self;
}

-(id) retain
{
    return self;
}

-(unsigned) retainCount
{
    return UINT_MAX;
}

-(oneway void) release
{
    
}

-(id) autorelease
{
    return self;
}

-(id) init
{
    @synchronized(self)
    {
        [super init];
        return self;
    }
}

+(int) get_Charge
{
    return s_nCharge;
}

+(void) set_Charge:(int)nCharge
{
    s_nCharge = nCharge;
}

//+(long long) get_PayNum
//{
//    return s_payNum;
//}
//
//+(void) set_PayNum:(long long)tmpPayNum
//{
//    s_payNum = tmpPayNum;
//}
//
//+(NSString *) get_macStr
//{
//    return s_nsMacStr;
//}
//
//+(void) set_macStr:(NSString *)nsMac
//{
//    s_nsMacStr = nsMac;
//}
//
//+(int) get_MachineType
//{
//    return s_nMachineType;
//}
//
//+(void) set_MachineType:(int)nMachineType
//{
//    s_nMachineType = nMachineType;
//}

///////////////////////////////////////////////

+(void)messageBox:(NSString*)stringTip
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:stringTip
													message:nil
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"确定", nil];
	[alert show];
	[alert release];
}

-(void)TBTSdkInitFinish:(NSNotification *)notify
{
    cocos2d::CCApplication::sharedApplication()->run();
}

-(void)TBTLoginResult:(NSNotification *)notify
{
    NSDictionary *dict = [notify userInfo];
    
    NSLog(@"login:%@", dict);
    

    if ([[TBPlatform defaultPlatform] TBIsLogined])
    {
        TBPlatformUserInfo *pUserInfo = [[TBPlatform defaultPlatform] TBGetMyInfo];
        
        NSString * nsSessionId = [pUserInfo sessionID];
        // 不知道userId是否为账号（待验证）
        NSString * nsUserId = [pUserInfo userID];

        const char * charUserId = [nsUserId UTF8String];
        const char * charSessionId = [nsSessionId UTF8String];
        
        Login::getInstance()->getLoginInfo(charUserId, charSessionId);
    }
    else
    {
        int b = 5;
    }
}

-(void)TBTLogoutPlatform:(NSNotification *)notify
{
    // SDK调用注销接口，游戏里的逻辑
    cocos2d::CCUserDefault::sharedUserDefault()->flush();
    GameMessageProcessor::sharedMsgProcessor()->sendReq(1002);
    
    //runLogin();
}

/**
 *  查询订单结束
 *
 *  @param orderString 订单号
 *  @param amount      订单金额（单位：分）
 *  @param statusType  订单状态（详见TBPlatformDefines.h）
 */
- (void)TBCheckOrderFinishedWithOrder:(NSString *)orderString
                               amount:(int)amount
                               status:(TBCheckOrderStatusType)statusType
{
//    [self quickShowMessage:[NSString stringWithFormat:@"订单号:%@\n金额:%d\n状态:%d",
//                            orderString,amount,statusType]
//                     title:@"查询成功"];
}
/**
 *  @brief 查询订单失败（网络不通畅，或服务器返回错误）
 */
- (void)TBCheckOrderDidFailed:(NSString*)order
{
//    [self quickShowMessage:[NSString stringWithFormat:@"订单号:%@",order]
//                     title:@"查询失败"];
}

-(void)TBTUniPayResult:(NSNotification *)notify                     // 支付付款返回结果通知
{
    
}

/**
 *	@brief	使用推币直接购买商品失败
 *
 *	@param 	order 	订单号
 *	@param 	errorType  错误类型，见TB_BUYGOODS_ERROR
 */
- (void)TBBuyGoodsDidFailedWithOrder:(NSString *)order
                          resultCode:(TB_BUYGOODS_ERROR)errorType
{
//    [self quickShowMessage:[NSString stringWithFormat:@"订单号:%@\n错误类型:%d",
//                            order,errorType]
//                     title:@"购买失败"];
    
    // 提示信息
    NSString * strLoginTip = @"购买失败";
    const char * charLoginTip = [strLoginTip UTF8String];
    GameView::getInstance()->showAlertDialog(charLoginTip);
}

/**
 *	@brief	使用推币直接购买商品成功
 *
 *	@param 	order 	订单号
 */
- (void)TBBuyGoodsDidSuccessWithOrder:(NSString*)order
{
//    [self quickShowMessage:[NSString stringWithFormat:@"订单号:%@",order]
//                     title:@"购买成功"];
    
    // 提示信息
    NSString * strLoginTip = @"购买成功";
    const char * charLoginTip = [strLoginTip UTF8String];
    GameView::getInstance()->showAlertDialog(charLoginTip);
}

@end

