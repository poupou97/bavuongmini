#include "SetUI.h"
#include "GameStateBasic.h"
#include "../Classes/login_state/LoginState.h"
#include "../Classes/gamescene_state/GameSceneState.h"
#include "../Classes/loadscene_state/LoadSceneState.h"
#include "../Classes/messageclient/GameMessageProcessor.h"
#include "../Classes/ui/extensions/UITab.h"
#include "SimpleAudioEngine.h"
#include "../Classes/login_state/Login.h"
#include "GameAudio.h"
#include "../Classes/GameUserDefault.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/GameView.h"
#include "../Classes/utils/GameUtils.h"

#import <TBPlatform/TBPlatform.h>
#import "ExtenalClass.h"
#import "../Classes/login_state/Login.h"

using namespace CocosDenshion;

SetUI::SetUI(void)
{
}


SetUI::~SetUI(void)
{
}

SetUI * SetUI::create()
{
	SetUI * setui=new SetUI();
	if (setui && setui->init())
	{
		setui->autorelease();
		return setui;
	}
	CC_SAFE_DELETE(setui);
	return NULL;
}

bool SetUI::init()
{
	if (UIScene::init())
	{
		CCSize size=CCDirector::sharedDirector()->getVisibleSize();
		//get userdefault.xml by:liutao
		SetUI::getUserDefault();
        
		//SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(m_nMusic);
		//SimpleAudioEngine::sharedEngine()->playBackgroundMusic("music0.mid", true);
        
		UILayer * layer=UILayer::create();
		layer->ignoreAnchorPointForPosition(false);
		layer->setAnchorPoint(ccp(0.5f,0.5f));
		layer->setPosition(ccp(size.width/2,size.height/2));
		layer->setContentSize(CCSizeMake(800,480));
		addChild(layer);
        
		cocos2d::extension::UIImageView *mengban = cocos2d::extension::UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(size);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);
        
		if(LoadSceneLayer::setUiPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::setUiPanel->removeFromParentAndCleanup(false);
		}
        
		UIPanel *setPanel=LoadSceneLayer::setUiPanel;
		setPanel->setAnchorPoint(ccp(0.5f,0.5f));
		setPanel->setPosition(ccp(size.width/2,size.height/2));
		m_pUiLayer->addWidget(setPanel);
        
        // close btn
        cocos2d::extension::UIButton * btn_Exit= (cocos2d::extension::UIButton *)UIHelper::seekWidgetByName(setPanel,"Button_close");
		btn_Exit->setTouchEnable(true);
		btn_Exit->setPressedActionEnabled(true);
		btn_Exit->addReleaseEvent(this,coco_releaseselector(SetUI::callBackExit));
        
        // 更换账号
		cocos2d::extension::UIButton * btn_reload= (cocos2d::extension::UIButton *)UIHelper::seekWidgetByName(setPanel,"Button_return_landing");
		btn_reload->setTouchEnable(true);
		btn_reload->setPressedActionEnabled(true);
		btn_reload->addReleaseEvent(this,coco_releaseselector(SetUI::reLoading));
        
        
        // 联系我们
        //		cocos2d::extension::UIButton * btn_ContactUs = (cocos2d::extension::UIButton *)UIHelper::seekWidgetByName(setPanel,"Button_ContactUs");
        //		btn_ContactUs->setTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");
        //		btn_ContactUs->setTouchEnable(true);
        //		//btn_ContactUs->setPressedActionEnabled(true);
        //		btn_ContactUs->addReleaseEvent(this,coco_releaseselector(SetUI::callBackComeSoon));
        
        // 个人中心
        cocos2d::extension::UIButton * btn_personCenter = (cocos2d::extension::UIButton
                                                           *)UIHelper::seekWidgetByName(setPanel, "Button_personCenter");
        btn_personCenter->setTouchEnable(true);
        btn_personCenter->setPressedActionEnabled(true);
        btn_personCenter->addReleaseEvent(this, coco_releaseselector(SetUI::callBackPersonCenter));
        btn_personCenter->setVisible(false);
        
        // 老虎社区
        cocos2d::extension::UIButton * btn_laohuSquare = (cocos2d::extension::UIButton
                                                          *)UIHelper::seekWidgetByName(setPanel, "Button_laohuSquare");
        btn_laohuSquare->setTouchEnable(true);
        btn_laohuSquare->setPressedActionEnabled(true);
        btn_laohuSquare->addReleaseEvent(this, coco_releaseselector(SetUI::callBackLaohuSquare));
        btn_laohuSquare->setVisible(false);
        
		cocos2d::extension::UISlider * slider_Music = (cocos2d::extension::UISlider *)UIHelper::seekWidgetByName(setPanel,"Slider_music");
		slider_Music->setTouchEnable(true);
		slider_Music->setPercent(m_nMusic);
		slider_Music->addEventListenerSlider(this, sliderpercentchangedselector(SetUI::sliderMusicEvent));
        
		cocos2d::extension::UISlider * slider_Sound = (cocos2d::extension::UISlider *)UIHelper::seekWidgetByName(setPanel,"Slider_sound");
		slider_Sound->setTouchEnable(true);
		slider_Sound->setPercent(m_nSound);
		slider_Sound->addEventListenerSlider(this, sliderpercentchangedselector(SetUI::sliderSoundEvent));
        
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(size);
		return true;
	}
	return false;
}

void SetUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void SetUI::onExit()
{
	UIScene::onExit();
}

 void SetUI::tabIndexChangedEvent(CCObject* pSender)
 {

 }

void SetUI::callBackComeSoon(CCObject * obj)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillui_nothavefuction"));
    
    // 暂时在此处调用支付接口
    //[[WmComPlatform defaultPlatform] WmUniPay:[[ExtenalClass sharedIntance] getBuyInfoFromUI:1:@"春哥"]];
    
    //requestPay();
}

void SetUI::callBackPersonCenter(CCObject * obj)
{
    // 老虎SDK 个人中心
    //[[WmComPlatform defaultPlatform] WmEnterUserSetting];
}

void SetUI::callBackLaohuSquare(CCObject * obj)
{
    // 老虎SDK 老虎社区
    //[[WmComPlatform defaultPlatform] WmEnterDiscuzPage];
}

void SetUI::callBackExit(CCObject * obj)
{
	float volume = (float)m_nMusic/100.0f;
	SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(volume);
	CCUserDefault::sharedUserDefault()->setIntegerForKey(MUSIC, m_nMusic);

	float effectVolume = (float)m_nSound/100.0f;
	SimpleAudioEngine::sharedEngine()->setEffectsVolume(effectVolume);
	CCUserDefault::sharedUserDefault()->setIntegerForKey(SOUND, m_nSound);

	CCUserDefault::sharedUserDefault()->flush();

	if(volume <= 0)
		SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	
	this->closeAnim();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

void SetUI::reLoading( CCObject * obj )
{
    GameView::getInstance()->showPopupWindow(StringDataManager::getString("con_sureToChangeId"),2,this,coco_selectselector(SetUI::SureToReLoading),NULL);
}
/*
void SetUI::closeWindow(CCObject * obj)
{
	CCUserDefault::sharedUserDefault()->flush();
	//SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	CCDirector::sharedDirector()->end();
}
*/

void SetUI::sliderMusicEvent(CCObject *pSender, SliderEventType type)
{
    switch (type)
    {
        case cocos2d::extension::SLIDER_PERCENTCHANGED:
        {
            cocos2d::extension::UISlider* slider = dynamic_cast<cocos2d::extension::UISlider*>(pSender);
            int percent = slider->getPercent();
			m_nMusic = percent;
			//float volume = (float)percent/100.0f;
			//SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(volume);
			//CCLOG("%d", percent);
			//CCUserDefault::sharedUserDefault()->setIntegerForKey(MUSIC, percent);
			//music regulate
        }
            break;
            
        default:
            break;
    }
}

void SetUI::sliderSoundEvent(CCObject *pSender, SliderEventType type)
{
    switch (type)
    {
        case cocos2d::extension::SLIDER_PERCENTCHANGED:
        {
            cocos2d::extension::UISlider* slider = dynamic_cast<cocos2d::extension::UISlider*>(pSender);
            int percent = slider->getPercent();
			m_nSound = percent;
			//float volume = (float)percent/100.0f;
			//SimpleAudioEngine::sharedEngine()->setEffectsVolume(volume);
			//SimpleAudioEngine::sharedEngine()->playEffect("event.mid", true);

			//CCLog("%d", percent);
			//CCUserDefault::sharedUserDefault()->setIntegerForKey(SOUND, percent);
			//sound regulate
        }
            break;
            
        default:
            break;
    }
}

void SetUI::getUserDefault()
{
	m_nMusic = CCUserDefault::sharedUserDefault()->getIntegerForKey(MUSIC,50);
    CCLog("m_nMusic is %d", m_nMusic);

	m_nSound = CCUserDefault::sharedUserDefault()->getIntegerForKey(SOUND,50);
    CCLog("m_nSound is %d", m_nSound);
}

void SetUI::SureToReLoading( CCObject *obj )
{
	float volume = (float)m_nMusic/100.0f;
	SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(volume);
	CCUserDefault::sharedUserDefault()->setIntegerForKey(MUSIC, m_nMusic);
    
	float effectVolume = (float)m_nSound/100.0f;
	SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(effectVolume);
	CCUserDefault::sharedUserDefault()->setIntegerForKey(SOUND, m_nSound);
    
	//CCUserDefault::sharedUserDefault()->flush();
	//SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	//GameMessageProcessor::sharedMsgProcessor()->sendReq(1002);   // request logout
    
	// when the player want to re-login, the disconnection tip will not be shown
	GameView::getInstance()->showSocketCloseInfo = false;
    
	//GameState* pScene = new LoginState();
	//if (pScene)
	//{
	//	pScene->runThisState();
	//	pScene->release();
	//}
    

    // 注销接口
    [[TBPlatform defaultPlatform] TBLogout:1];
}