#include "LoginState.h"

#include "../Classes/gamescene_state/GameSceneState.h"
#include "../Classes/loadscene_state/LoadSceneState.h"
#include "../Classes/legend_engine/CCLegendAnimation.h"
#include "../Classes/legend_engine/LegendLevel.h"
#include "../Classes/ui/extensions/CCRichLabel.h"
#include "../Classes/ui/extensions/RichElement.h"

#include "../Classes/utils/GameUtils.h"

#include "../Classes/messageclient/GameMessageProcessor.h"
#include "../Classes/messageclient/ClientNetEngine.h"

#include "../Classes/ui/extensions/RichTextInput.h"
#include "../Classes/GameView.h"
#include "../Classes/messageclient/protobuf/LoginMessage.pb.h"
#include "../Classes/login_state/Login.h"
#include "../Classes/login_state/CreateRole.h"
#include "../Classes/ui/generals_ui/GeneralsUI.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/gamescene_state/role/BasePlayer.h"
#include "../Classes/AppMacros.h"
#include "../Classes/gamescene_state/role/MyPlayerAIConfig.h"
#include "../Classes/gamescene_state/role/MyPlayer.h"
#include "../Classes/gamescene_state/role/MyPlayerAI.h"
#include "../Classes/GameUserDefault.h"

#import "WmComPlatform.h"

#define THREEKINGDOMS_HOST_INTERNET "192.168.1.138"//"61.149.218.125"//"qianliang2013.vicp.cc"//"192.168.1.117"
#define THREEKINGDOMS_PORT_INTERNET 8088
#define THREEKINGDOMS_HOST_WANMEI "124.202.137.33"
#define THREEKINGDOMS_PORT_WANMEI 8088
#define THREEKINGDOMS_HOST_STUDIO "192.168.1.125"  
#define THREEKINGDOMS_PORT_STUDIO 8088
#define THREEKINGDOMS_HOST_GBK "192.168.1.118"  
#define THREEKINGDOMS_PORT_GBK 8088  
#define THREEKINGDOMS_HOST_ZHAGNGJING "192.168.1.132"  
#define THREEKINGDOMS_PORT_ZHAGNGJING 8088  
#define THREEKINGDOMS_HOST_LOCAL "127.0.0.1"  
#define THREEKINGDOMS_PORT_LOCAL 8088  

#define MAX_LOGIN_ROLE_NUM 24

#define BACKGROUND_ROLL_TIME 100.0f
#define BACKGROUND_TAG 123

enum {
	kStateRoleList = 0,
	kStateConnectingGameServer,
};

enum {
	kTagPreparingBackGround = 2,
	kTagPreparingTip = 3,
	kTagServerTip = 4,
	kTagRoleListMenu = 16,   // last one !! please add more tag before it
};

long LoginLayer::mRoleId = 0;
int LoginLayer::mCurrentServerId = 0;
int LoginLayer::mCountryMin = 1;
int LoginLayer::mProfessionMin = 1;

LoginLayer::LoginLayer()
: m_state(kStateRoleList)
{
	setTouchEnabled(true);
	scheduleUpdate();

    CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	
	m_pServerListRsp = new ServerListRsp();
	Login::getServerListInfo(m_pServerListRsp);

	CCSprite* pSprite = CCSprite::create("images/denglu.jpg");
	pSprite->setPosition(ccp(0, 0));
	pSprite->setAnchorPoint(ccp(0, 0));
	float scaleValue;
	if(((float)s.width/(float)1136) > ((float)s.height/(float)640))
	{
		scaleValue = (float)s.width/(float)1136;
	}
	else
	{
		scaleValue = (float)s.height/(float)640;
	}
	pSprite->setScale(scaleValue);
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)s.width;
	//pSprite->setColor(ccc3(100, 100, 100));
	addChild(pSprite, 0);

	CCSprite* pSpriteNext = CCSprite::create("images/denglu.jpg");
	pSpriteNext->setPosition(ccp(s.width-1, 0));
	pSpriteNext->setAnchorPoint(ccp(0, 0));
	pSpriteNext->setScale(scaleValue);
	pSpriteNext->setTag(BACKGROUND_TAG);
	addChild(pSpriteNext, 0);

	CCMoveTo * moveToAction = CCMoveTo::create(BACKGROUND_ROLL_TIME,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturn = CCMoveTo::create(0,ccp(s.width-1, 0));
	CCMoveTo * moveToStart = CCMoveTo::create(BACKGROUND_ROLL_TIME/widthMultiple,ccp(0, 0));
	CCRepeatForever * repeapAction = CCRepeatForever::create(CCSequence::create(moveToAction,moveToReturn,CCDelayTime::create(BACKGROUND_ROLL_TIME*(widthMultiple-1)/widthMultiple),moveToStart,NULL));
	pSprite->runAction(repeapAction);
	
	CCSequence * nextAction = CCSequence::create(CCDelayTime::create(BACKGROUND_ROLL_TIME*(widthMultiple-1)/widthMultiple), CCCallFunc::create(this, callfunc_selector(LoginLayer::repeatFunc)), NULL);
	pSpriteNext->runAction(nextAction);

	Login::addBirds(this);

	// game code version
	CCLabelTTF * labelVersion = CCLabelTTF::create(GAME_CODE_VERSION, APP_FONT_NAME, 20);
	ccColor3B blackColor = ccc3(0,0,0);   // black
	labelVersion->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, blackColor);
	labelVersion->setAnchorPoint(ccp(0, 1));
	labelVersion->setPosition(ccp(3, s.height));
	addChild(labelVersion);

	// game res version
	int resVersion = CCUserDefault::sharedUserDefault()->getIntegerForKey(KEY_OF_RES_VERSION, GAME_RES_VERSION);
	char resVerStr[20];
	sprintf(resVerStr, "res version: %d", resVersion);
	labelVersion = CCLabelTTF::create(resVerStr, APP_FONT_NAME, 20);
	labelVersion->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, blackColor);
	labelVersion->setAnchorPoint(ccp(0, 1));
	labelVersion->setPosition(ccp(3, s.height - 25));
	addChild(labelVersion);
	
	UILayer* layer= UILayer::create();
	layer->setPosition(ccp(0,0));
	addChild(layer);

	UILayer* m_pUiLayer= UILayer::create();
	m_pUiLayer->ignoreAnchorPointForPosition(false);
	// 锚点在中心，并放置在屏幕中心点
	m_pUiLayer->setAnchorPoint(ccp(0.5f,0.5f));
	m_pUiLayer->setPosition(ccp(s.width/2,s.height/2));  
	m_pUiLayer->setTag(255);
	// 设置基准分辨率
	m_pUiLayer->setContentSize(CCSizeMake(UI_DESIGN_RESOLUTION_WIDTH, UI_DESIGN_RESOLUTION_HEIGHT));
	addChild(m_pUiLayer);

	UIPanel *serverListPanel=(UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/Select_the_server_1.json");
	serverListPanel->setAnchorPoint(ccp(0.5f,0.5f));
	serverListPanel->setPosition(ccp(s.width/2,s.height/2));
	/*if(((float)s.width/(float)serverListPanel->getContentSize().width) > ((float)s.height/(float)serverListPanel->getContentSize().height))
	{
		serverListPanel->setScaleX((float)s.width/(float)serverListPanel->getContentSize().width);
		serverListPanel->setScaleY((float)s.width/(float)serverListPanel->getContentSize().width);
	}
	else
	{
		serverListPanel->setScaleX((float)s.height/(float)serverListPanel->getContentSize().height);
		serverListPanel->setScaleY((float)s.height/(float)serverListPanel->getContentSize().height);
	}*/
	layer->addWidget(serverListPanel);
	
	//UIImageView * ImageView_biankuang= (UIImageView *)UIHelper::seekWidgetByName(serverListPanel,"ImageView_biankuang1");
	int lastServerIndex, i;
	for(i = 0; i < m_pServerListRsp->serverinfos_size(); i++)
	{
		if(m_pServerListRsp->lastserverid() == m_pServerListRsp->serverinfos(i).serverid())
		{
			lastServerIndex = i;
			break;
		}
	}
	initLastServer(lastServerIndex);

	UITextButton* textbutton = UITextButton::create();
    //button->setTouchEnable(true);
    textbutton->loadTextures("res_ui/new_button_12pro.png", "res_ui/new_button_12pro.png", "");
	textbutton->setText("青龙偃月");
	textbutton->setFontSize(18);
	textbutton->setScale9Enabled(true);
	textbutton->setSize(CCSizeMake(86, 43));
	textbutton->setPosition(ccp(201,265));
	textbutton->setTouchEnable(true);
	textbutton->setPressedActionEnabled(true);
	m_pUiLayer->addWidget(textbutton);
	
	tableView = CCTableView::create(this, CCSizeMake(400, 240));
	tableView->setSelectedEnable(true);
	tableView->setSelectedScale9Texture("res_ui/kuang_0_on.png", CCRectMake(7, 7, 1, 1), ccp(0,0));
	tableView->setDirection(kCCScrollViewDirectionVertical);
	tableView->setAnchorPoint(ccp(0.5f,0.5f));
	tableView->setPosition(ccp(271,57));
	tableView->setDelegate(this);
	tableView->selectCell(lastServerIndex);
	tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	tableView->setPressedActionEnabled(true);
	//this->addChild(tableView);
	m_pUiLayer->addChild(tableView);
	tableView->reloadData();
	
	cocos2d::extension::UIButton *enter = (cocos2d::extension::UIButton *)UIHelper::seekWidgetByName(serverListPanel,"Button_enter");
	enter->setTouchEnable(true);
	enter->setPressedActionEnabled(true);
	enter->addReleaseEvent(this,coco_releaseselector(LoginLayer::onEnterButtonGetClicked));

	cocos2d::extension::UIButton *back = (cocos2d::extension::UIButton *)UIHelper::seekWidgetByName(serverListPanel,"Button_back");
    back->setTouchEnable(true);
	back->addReleaseEvent(this, coco_releaseselector(LoginLayer::onButtonGetClicked));
	back->setPressedActionEnabled(true);

}

LoginLayer::~LoginLayer()
{

}

void LoginLayer::initLastServer(int idx)
{
	if(idx<0||idx>=m_pServerListRsp->serverinfos_size())
	{
		return;
	}

	UILayer* m_pUiLayer = (UILayer*)getChildByTag(255);

	CCScale9Sprite *sprite = CCScale9Sprite::create("res_ui/kuang_0.png");
    sprite->setAnchorPoint(ccp(0, 0));
	sprite->setPreferredSize(CCSizeMake(370,55));
    sprite->setPosition(ccp(271, 310));
	sprite->setTag(123);
    m_pUiLayer->addChild(sprite);

    CCLabelTTF *label = CCLabelTTF::create(m_pServerListRsp->serverinfos(idx).name().c_str(), APP_FONT_NAME, 18);
	label->setColor(ccc3(47,93,13));
    label->setPosition(ccp(94, 27));
	label->setAnchorPoint(ccp(0, 0.5f));
    sprite->addChild(label);
		
	CCSprite * serverStatus = CCSprite::create();
	switch(m_pServerListRsp->serverinfos(idx).status())
	{
		case 1:
			serverStatus->initWithFile("res_ui/select_the_sercer/green.png");
			break;
		case 2:
			serverStatus->initWithFile("res_ui/select_the_sercer/yellow.png");
			break;
		case 3:
			serverStatus->initWithFile("res_ui/select_the_sercer/red.png");
			break;
		case 4:
			serverStatus->initWithFile("res_ui/select_the_sercer/gray.png");
			break;
		case 5:
			serverStatus->initWithFile("res_ui/select_the_sercer/gray.png");
			break;
		default:
			break;
	}
	serverStatus->setVisible(true);
	serverStatus->setAnchorPoint(ccp(0.5f,0.5f));
	serverStatus->setPosition(ccp(39, 27));
	sprite->addChild(serverStatus);

	if(1 == m_pServerListRsp->serverinfos(idx).newopen())
	{
		CCSprite * serverNewOpen = CCSprite::create("res_ui/select_the_sercer/new.png");
		serverNewOpen->setVisible(true);
		serverNewOpen->setPosition(ccp(366,45));
		serverNewOpen->setAnchorPoint(ccp(0.5f, 0.5f));
		sprite->addChild(serverNewOpen);
	}

	if(1 == m_pServerListRsp->serverinfos(idx).has_roleid())
	{
		int profession = m_pServerListRsp->serverinfos(idx).roleprofession();
		std::string headPicture = BasePlayer::getSmallHeadPathByProfession(profession);

		CCSprite * serverIcon = CCSprite::create();
		serverIcon->initWithFile(headPicture.c_str());

		serverIcon->setVisible(true);
		serverIcon->setPosition(ccp(205,29));
		sprite->addChild(serverIcon);
		
		CCLabelTTF *labelName = CCLabelTTF::create(m_pServerListRsp->serverinfos(idx).rolename().c_str(), APP_FONT_NAME, 16);
		labelName->setColor(ccc3(47,93,13));
		labelName->setPosition(ccp(231, 27));
		labelName->setAnchorPoint(ccp(0, 0.5f));
		sprite->addChild(labelName);
		
		char roleLevelArray[8];
        //itoa(m_pServerListRsp->serverinfos(idx).rolelevel(), roleLevelArray, 8);
        sprintf(roleLevelArray,"LV%d",m_pServerListRsp->serverinfos(idx).rolelevel());
		CCLabelTTF *label = CCLabelTTF::create(roleLevelArray, APP_FONT_NAME, 14);
		label->setColor(ccc3(47,93,13));
		label->setPosition(ccp(320, 26));
		label->setAnchorPoint(ccp(0.5f, 0.5f));
		sprite->addChild(label);
		
	}
}

void LoginLayer::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
{

}

void LoginLayer::repeatFunc()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCSprite* pSprite = (CCSprite*)getChildByTag(BACKGROUND_TAG);
	float scaleValue = pSprite->getScale();
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;

	CCMoveTo * moveToActionNext = CCMoveTo::create(BACKGROUND_ROLL_TIME*(widthMultiple+1)/widthMultiple,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturnNext = CCMoveTo::create(0,ccp(winSize.width-1, 0));
	CCRepeatForever * repeapActionNext = CCRepeatForever::create(CCSequence::create(moveToActionNext,moveToReturnNext,CCDelayTime::create(BACKGROUND_ROLL_TIME*(widthMultiple-1)/widthMultiple),NULL));
	pSprite->runAction(repeapActionNext);
}

void LoginLayer::tableCellHighlight(CCTableView* table, CCTableViewCell* cell)
{
    // please set cell's content size
	cell->setContentSize(tableCellSizeForIndex(table, cell->getIdx()));

    // highlight sprite
	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(11, 11, 1, 1) , "gamescene_state/zhujiemian3/renwuduiwu/light_1.png");
	pHighlightSpr->setPreferredSize(cell->getContentSize());
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setTag(55);
	cell->addChild(pHighlightSpr);
}

void LoginLayer::tableCellUnhighlight(CCTableView* table, CCTableViewCell* cell)
{
    cell->removeChildByTag(55);
}

void LoginLayer::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
	mCurrentServerId = cell->getIdx();
	CCLOG("mCurrentServerId = %d\n",mCurrentServerId);
    /*CCScale9Sprite *sprite = (CCScale9Sprite*)cell->getChildByTag(123);
	sprite->initWithFile("res_ui/kuang_0_on.png");
	sprite->setAnchorPoint(ccp(0, 0));
	sprite->setPreferredSize(CCSizeMake(380,58));
    sprite->setPosition(ccp(0, 0));*/
}

CCSize LoginLayer::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
    return CCSizeMake(370,56);
}

CCTableViewCell* LoginLayer::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
    CCTableViewCell *cell = table->dequeueCell();

    cell = new CCTableViewCell();
    cell->autorelease();
    CCScale9Sprite *sprite = CCScale9Sprite::create("res_ui/kuang_0.png");
    sprite->setAnchorPoint(ccp(0, 0));
	sprite->setPreferredSize(CCSizeMake(370,55));
    sprite->setPosition(ccp(0, 0));
	sprite->setTag(123);
    cell->addChild(sprite);

    CCLabelTTF *label = CCLabelTTF::create(m_pServerListRsp->serverinfos(idx).name().c_str(), APP_FONT_NAME, 18);
	label->setColor(ccc3(47,93,13));
    label->setPosition(ccp(94, 27));
	label->setAnchorPoint(ccp(0, 0.5f));
    cell->addChild(label);
		
	CCSprite * serverStatus = CCSprite::create();
	switch(m_pServerListRsp->serverinfos(idx).status())
	{
		case 1:
			serverStatus->initWithFile("res_ui/select_the_sercer/green.png");
			break;
		case 2:
			serverStatus->initWithFile("res_ui/select_the_sercer/yellow.png");
			break;
		case 3:
			serverStatus->initWithFile("res_ui/select_the_sercer/red.png");
			break;
		case 4:
			serverStatus->initWithFile("res_ui/select_the_sercer/gray.png");
			break;
		case 5:
			serverStatus->initWithFile("res_ui/select_the_sercer/gray.png");
			break;
		default:
			break;
	}
	serverStatus->setVisible(true);
	serverStatus->setAnchorPoint(ccp(0.5f,0.5f));
	serverStatus->setPosition(ccp(39, 27));
	cell->addChild(serverStatus);

	if(1 == m_pServerListRsp->serverinfos(idx).newopen())
	{
		CCSprite * serverNewOpen = CCSprite::create("res_ui/select_the_sercer/new.png");
		serverNewOpen->setVisible(true);
		serverNewOpen->setPosition(ccp(366,45));
		serverNewOpen->setAnchorPoint(ccp(0.5f, 0.5f));
		cell->addChild(serverNewOpen);
	}

	if(1 == m_pServerListRsp->serverinfos(idx).has_roleid())
	{
		int profession = m_pServerListRsp->serverinfos(idx).roleprofession();
		std::string headPicture = BasePlayer::getSmallHeadPathByProfession(profession);

		CCSprite * serverIcon = CCSprite::create();
		serverIcon->initWithFile(headPicture.c_str());

		serverIcon->setVisible(true);
		serverIcon->setPosition(ccp(205,29));
		cell->addChild(serverIcon);
		
		CCLabelTTF *labelName = CCLabelTTF::create(m_pServerListRsp->serverinfos(idx).rolename().c_str(), APP_FONT_NAME, 16);
		labelName->setColor(ccc3(47,93,13));
		labelName->setPosition(ccp(231, 27));
		labelName->setAnchorPoint(ccp(0, 0.5f));
		cell->addChild(labelName);
		
		char roleLevelArray[8];
        //itoa(m_pServerListRsp->serverinfos(idx).rolelevel(), roleLevelArray, 8);
        sprintf(roleLevelArray,"LV%d",m_pServerListRsp->serverinfos(idx).rolelevel());
		CCLabelTTF *label = CCLabelTTF::create(roleLevelArray, APP_FONT_NAME, 14);
		label->setColor(ccc3(47,93,13));
		label->setPosition(ccp(320, 26));
		label->setAnchorPoint(ccp(0.5f, 0.5f));
		cell->addChild(label);
		
	}

    return cell;
}

unsigned int LoginLayer::numberOfCellsInTableView(CCTableView *table)
{
	//CCLOG("m_pServerListRsp->serverinfos_size() = %d\n",m_pServerListRsp->serverinfos_size());

    return m_pServerListRsp->serverinfos_size();
}

void LoginLayer::connectGameServer()
{
	CCLOG("connectGameServer\n");
	if(ClientNetEngine::sharedSocketEngine()->getReadyState() == ClientNetEngine::kStateOpen)
		return;
	ClientNetEngine::sharedSocketEngine()->init(*(GameView::getInstance()));
}

void LoginLayer::update(float dt)
{
	if(m_state == kStateRoleList)
	{

	}
	else if(m_state == kStateConnectingGameServer)
	{
		// todo
		// if timeout, back to kStateRoleList
	}
}

void LoginLayer::onButtonGetClicked(cocos2d::CCObject *sender)
{
	// SDK需求加入（注销接口）
    [[WmComPlatform defaultPlatform] WmLogout:0];
    
	runLogin();
}

void LoginLayer::onEnterButtonGetClicked(cocos2d::CCObject *sender)
{
	if(4 == m_pServerListRsp->serverinfos(mCurrentServerId).status())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("server_status_maintain"));
		return;
	}
	else if(5 == m_pServerListRsp->serverinfos(mCurrentServerId).status())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("server_status_willopen"));
		return;
	}

	CCLOG("m_pServerListRsp->serverinfos(0).ip().c_str() = %s \n", m_pServerListRsp->serverinfos(mCurrentServerId).ip().c_str());
	CCLOG("m_pServerListRsp->serverinfos(0).port() = %d \n", m_pServerListRsp->serverinfos(mCurrentServerId).port());
	ClientNetEngine::sharedSocketEngine()->setAddress(m_pServerListRsp->serverinfos(mCurrentServerId).ip().c_str(), m_pServerListRsp->serverinfos(mCurrentServerId).port());

	if(1 == m_pServerListRsp->serverinfos(mCurrentServerId).has_roleid())
	{
		mRoleId = m_pServerListRsp->serverinfos(mCurrentServerId).roleid();

		// tip's background
		CCSize s = CCDirector::sharedDirector()->getVisibleSize();
		CCRect insetRect = CCRectMake(32,15,1,1);
		CCScale9Sprite* backGround = CCScale9Sprite::create("res_ui/kuang_1.png"); 
		backGround->setCapInsets(insetRect);
		backGround->setPreferredSize(CCSizeMake(550, 50));
		backGround->setPosition(ccp(s.width/2, s.height/2));
		backGround->setAnchorPoint(ccp(0.5f, 0.5f));
		backGround->setTag(kTagPreparingBackGround);
		addChild(backGround);

		// show tip
		CCLabelTTF* label = CCLabelTTF::create(StringDataManager::getString("con_enter_game"), "Arial", 25);
		addChild(label);
		label->setTag(kTagPreparingTip);
		label->setAnchorPoint(ccp(0.5f, 0.5f));
		label->setPosition( ccp(s.width/2, s.height/2) );

		CCActionInterval *action = (CCActionInterval*)CCSequence::create
			(
				CCShow::create(),
				CCDelayTime::create(0.5f),
				CCCallFunc::create(this, callfunc_selector(LoginLayer::connectGameServer)), 
				NULL
			);
		label->runAction(action);

		m_state = kStateConnectingGameServer;
		/*
		MyPlayerAIConfig::setAutomaticSkill(0);
		MyPlayerAIConfig::enableRobot(false);
		
		std::vector<ChatCell * >::iterator iter;
		for (iter = GameView::getInstance()->chatCell_Vdata.begin(); iter != GameView::getInstance()->chatCell_Vdata.end(); ++ iter)
		{
			delete * iter;
		}
		GameView::getInstance()->chatCell_Vdata.clear();
		*/
		CCUserDefault::sharedUserDefault()->setIntegerForKey("general_mode",1);
	}
	else
	{
		onRoleListReq();
	}
}


void LoginLayer::onRoleListReq()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = Login::s_loginserver_ip;
        url.append(":6666/rolelist");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(LoginLayer::onRoleListRequestCompleted));

		RoleListReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);
		httpReq.set_serverid(m_pServerListRsp->serverinfos(mCurrentServerId).serverid());

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void LoginLayer::onRoleListRequestCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());

	RoleListRsp roleListRsp;
	CCAssert(response->getResponseData()->size() > 0, "should not be empty");
	roleListRsp.ParseFromString(strdata);

	CCLOG("response.result = %d", roleListRsp.result());

	if(1 == roleListRsp.result()&&roleListRsp.roleinfos().size())
	{
		mRoleId = roleListRsp.roleinfos(0).id();

		// tip's background
		CCSize s = CCDirector::sharedDirector()->getVisibleSize();
		CCRect insetRect = CCRectMake(32,15,1,1);
		CCScale9Sprite* backGround = CCScale9Sprite::create("res_ui/kuang_1.png"); 
		backGround->setCapInsets(insetRect);
		backGround->setPreferredSize(CCSizeMake(550, 50));
		backGround->setPosition(ccp(s.width/2, s.height/2));
		backGround->setAnchorPoint(ccp(0.5f, 0.5f));
		backGround->setTag(kTagPreparingBackGround);
		addChild(backGround);

		// show tip
		CCLabelTTF* label = CCLabelTTF::create(StringDataManager::getString("con_enter_game"), "Arial", 25);
		addChild(label);
		label->setTag(kTagPreparingTip);
		label->setAnchorPoint(ccp(0.5f, 0.5f));
		label->setPosition( ccp(s.width/2, s.height/2) );

		CCActionInterval *action = (CCActionInterval*)CCSequence::create
			(
				CCShow::create(),
				CCDelayTime::create(0.5f),
				CCCallFunc::create(this, callfunc_selector(LoginLayer::connectGameServer)), 
				NULL
			);
		label->runAction(action);

		m_state = kStateConnectingGameServer;

		MyPlayerAIConfig::setAutomaticSkill(0);
		MyPlayerAIConfig::enableRobot(false);
	}
	else
	{
		onMenuGetTestClicked();
	}
}

void LoginLayer::onMenuGetTestClicked()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = Login::s_loginserver_ip;
        url.append(":6666/countryprofession");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(LoginLayer::onHttpRequestCompleted));

		CountryProfessionReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);
		httpReq.set_serverid(m_pServerListRsp->serverinfos(mCurrentServerId).serverid());

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void LoginLayer::onHttpRequestCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());

	CountryProfessionRsp countryProfessionRsp;
	CCAssert(response->getResponseData()->size() > 0, "should not be empty");
	countryProfessionRsp.ParseFromString(strdata);

	CCLOG("response.result = %d", countryProfessionRsp.result());

	if(1 == countryProfessionRsp.result())
	{
		mCountryMin = countryProfessionRsp.country();
		mProfessionMin = countryProfessionRsp.profession();

		GameState* pScene = new CreateRoleState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
	else
	{
		GameView::getInstance()->showAlertDialog(countryProfessionRsp.resultmessage());
	}
}


void LoginLayer::menuSelectRoleCallback(CCObject* pSender)
{

	CCNode* button = (CCNode*)pSender;
	mSelectedId = button->getTag();

	//
	// changet to kStateConnectingGameServer
	//
	// disable role list
	for(int i = 0; i < MAX_LOGIN_ROLE_NUM; i++)
	{
		CCMenu* menu = (CCMenu*)this->getChildByTag(kTagRoleListMenu+i);
		menu->setEnabled(false);
	}
	
	// tip's background
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	CCRect insetRect = CCRectMake(32,15,1,1);
	CCScale9Sprite* backGround = CCScale9Sprite::create("res_ui/kuang_1.png"); 
	backGround->setCapInsets(insetRect);
	backGround->setPreferredSize(CCSizeMake(550, 50));
	backGround->setPosition(ccp(s.width/2, s.height/2));
	backGround->setAnchorPoint(ccp(0.5f, 0.5f));
	backGround->setTag(kTagPreparingBackGround);
	addChild(backGround);

	// show tip
	CCLabelTTF* label = CCLabelTTF::create(StringDataManager::getString("con_enter_game"), "Arial", 25);
	addChild(label);
	label->setTag(kTagPreparingTip);
	label->setAnchorPoint(ccp(0.5f, 0.5f));
	label->setPosition( ccp(s.width/2, s.height/2) );

    CCActionInterval *action = (CCActionInterval*)CCSequence::create
        (
            CCShow::create(),
			CCDelayTime::create(0.5f),
            CCCallFunc::create(this, callfunc_selector(LoginLayer::connectGameServer)), 
            NULL
        );
	label->runAction(action);

	m_state = kStateConnectingGameServer;
}

void LoginLayer::onSocketOpen(ClientNetEngine* socketEngine)
{
    //显示浮动工具条
//    [[TBPlatform defaultPlatform] TBShowToolBar:TBToolBarAtMiddleRight
//                                  isUseOldPlace:YES];
    
	// request to enter game
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1001, this);
}
void LoginLayer::onSocketClose(ClientNetEngine* socketEngine)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	// player can re-connect
	if(m_state == kStateConnectingGameServer)
	{
		this->getChildByTag(kTagPreparingBackGround)->removeFromParent();
		this->getChildByTag(kTagPreparingTip)->removeFromParent();
		// enable role list
		/*for(int i = 0; i < MAX_LOGIN_ROLE_NUM; i++)
		{
			CCMenu* menu = (CCMenu*)this->getChildByTag(kTagRoleListMenu+i);
			menu->setEnabled(true);
		}
		*/
		m_state = kStateRoleList;
	}
}
void LoginLayer::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

///////////////////////////////////////////////////////////////////////////////////////

enum {
	kTagMainLayer = 1,
};

LoginState::~LoginState()
{
}

void LoginState::runThisState()
{
    CCLayer* pLayer = new LoginLayer();
    addChild(pLayer, 0, kTagMainLayer);
	pLayer->release();

	if(CCDirector::sharedDirector()->getRunningScene() != NULL)
		CCDirector::sharedDirector()->replaceScene(this);
	else
		CCDirector::sharedDirector()->runWithScene(this);
}

void LoginState::onSocketOpen(ClientNetEngine* socketEngine)
{
	LoginLayer* layer = (LoginLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketOpen(socketEngine);
}
void LoginState::onSocketClose(ClientNetEngine* socketEngine)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	LoginLayer* layer = (LoginLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketClose(socketEngine);
}
void LoginState::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}
