#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../Classes/ui/extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
class SetUI:public UIScene
{
public:
	SetUI(void);
	~SetUI(void);

	static SetUI *create();
	bool init();
	void onEnter();
	void onExit();

	void reLoading(CCObject * obj);                                        // 更换账号
    
	void callBackExit(CCObject * obj);
	void callBackComeSoon(CCObject * obj);                                 // 即将开放
    
    void callBackPersonCenter(CCObject * obj);                             // 个人中心
    void callBackLaohuSquare(CCObject * obj);                              // 老虎社区

	 void tabIndexChangedEvent(CCObject* pSender);
	//start by:liutao
	void closeWindow(CCObject * obj);

	void sliderMusicEvent(CCObject *pSender, SliderEventType type);

	void sliderSoundEvent(CCObject *pSender, SliderEventType type);

	void getUserDefault();
    
    void requestPay();
    void onHttpResponsePay(CCHttpClient *sender, CCHttpResponse *response);
    
    static long long s_orderNum;
    
protected:
	int m_nMusic;
	int m_nSound;
	//above  by:liutao
};

