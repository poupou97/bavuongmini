//
//  ExtenalClass.h(For 91SDK)
//  Threekingdoms
//
//  Created by 刘亮 on 14-4-30.
//
//

#import <Foundation/Foundation.h>
#import "WmComPlatform.h"

@interface ExtenalClass : NSObject

+(ExtenalClass*) sharedIntance;
+(id) allocWithZone:(struct _NSZone *)zone;
-(id) copyWithZone:(NSZone *)zone;
-(id) retain;
-(unsigned) retainCount;
-(oneway void) release;
-(id) autorelease;
-(id) init;

// 显示报错信息
+(void)messageBox:(NSString*)stringTip;

//-(WmBuyInfo *)getBuyInfoFromUI:(int)tag:(NSString *)info;
-(WmBuyInfo *)getBuyInfo:(int)goodsPrice:(NSString *)goodsName;

-(void)WMLoginResult:(NSNotification *)notify;                      // 登录结果通知
-(void)WMLeavePlatform:(NSNotification *)notify;                    // 注销账号通知
-(void)WMUserImproveAccount:(NSNotification *)notify;               // 游客完善资料通知
-(void)WmUniPayResult:(NSNotification *)notify;                     // 支付付款返回结果通知



// 静态变量
+(NSString *) s_guestAccount;                                       // 记录游客账号
+(NSString *) s_nsMacStr;                                           // 设备号
+(int) s_nMachineType;                                              // 设备类型（1：ios越狱，2：安卓国内；3：ios官方简体；4：ios官方繁体）
+(NSString *) s_nsPlatformStr;                                      // 渠道（渠道缩写，以给的文档为主）

// 静态方法
+(long long) get_PayNum;                                            // get订单号
+(void) set_PayNum:(long long)tmpPayNum;                            // set订单号

+(NSString *) get_macStr;                                           // get设备号
+(void) set_macStr:(NSString *)nsMac;                               // set设备号

+(int) get_MachineType;                                             // get设备类型
+(void) set_MachineType:(int)nMachineType;                          // set设备类型

+(NSString *) get_platformStr;                                      // get渠道
+(void) set_PlatformStr:(NSString *)nsPlatform;                     // set渠道

+(int) get_Charge;                                                  // get所付金额
+(void) set_Charge:(int)nCharge;                                    // set所付金额

@end
