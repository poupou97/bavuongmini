//
//  ExtenalClass.m
//  Threekingdoms
//
//  Created by 刘亮 on 14-4-30.
//
//

#import "ExtenalClass.h"
#import "SetUI.h"
#import "GoldStoreUI.h"

#import "WmComPlatform.h"
#import "WXApi.h"

#import "../Classes/login_state/Login.h"
#import "../Classes/login_state/LoginState.h"
#import "../Classes/messageclient/GameMessageProcessor.h"


#include "../Classes/GameView.h"




static ExtenalClass *sharedObj = nil;

static NSString *s_guestAccount = nil;

static long long s_payNum = 0;

static NSString *s_nsMacStr = nil;

static int s_nMachineType = 0;

static NSString *s_nsPlatformStr = nil;

static int s_nCharge = 0;

@implementation ExtenalClass

+(ExtenalClass*) sharedIntance
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            [[self alloc] init];
        }
    }
    return sharedObj;
}

+(id) allocWithZone:(struct _NSZone *)zone
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            sharedObj = [super allocWithZone:zone];
            return sharedObj;
        }
    }
    
    return nil;
}

-(id) copyWithZone:(NSZone *)zone
{
    return self;
}

-(id) retain
{
    return self;
}

-(unsigned) retainCount
{
    return UINT_MAX;
}

-(oneway void) release
{
    
}

-(id) autorelease
{
    return self;
}

-(id) init
{
    @synchronized(self)
    {
        [super init];
        return self;
    }
}

+(long long) get_PayNum
{
    return s_payNum;
}

+(void) set_PayNum:(long long)tmpPayNum
{
    s_payNum = tmpPayNum;
}

+(int) get_Charge
{
    return s_nCharge;
}

+(void) set_Charge:(int)nCharge
{
    s_nCharge = nCharge;
}

+(NSString *) get_macStr
{
    return s_nsMacStr;
}

+(void) set_macStr:(NSString *)nsMac
{
    s_nsMacStr = nsMac;
}

+(int) get_MachineType
{
    return s_nMachineType;
}

+(void) set_MachineType:(int)nMachineType
{
    s_nMachineType = nMachineType;
}

+(NSString *) get_platformStr
{
    return s_nsPlatformStr;
}

+(void) set_PlatformStr:(NSString *)nsPlatform
{
    s_nsPlatformStr = nsPlatform;
}

///////////////////////////////////////////////

+(void)messageBox:(NSString*)stringTip
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:stringTip
													message:nil
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"确定", nil];
	[alert show];
	[alert release];
}

-(void)WMLoginResult:(NSNotification *)notify
{
    NSDictionary *dict = [notify userInfo];
    
    NSLog(@"login:%@", dict);
    

    
    int error_code = [[dict objectForKey:@"error_code"] intValue];
    
    // 登录成功后处理
    if ([[WmComPlatform defaultPlatform] isLogined] && !error_code)
    {
        int userType = [[dict objectForKey:@"userType"] intValue];                // -1:游客 0：173，1：QQ，2：新浪微博，3：完美账号
        if (userType == -1)
        {
            //			NSString *strUin = [[WmComPlatform defaultPlatform] loginUin];

            //			NSString *strTip = [NSString stringWithFormat:@"游客账号登录成功"];
            
            s_guestAccount = [[WmComPlatform defaultPlatform] userName];
            
            int a = 10;
            
        } else
        {

            // 普通账号登录成功!
            int b = 5;
        }

        NSString * strAccount = [[WmComPlatform defaultPlatform] userName];
        NSString * strLoginUin = [[WmComPlatform defaultPlatform] loginUin];
        NSString * strSessionId = [[WmComPlatform defaultPlatform] sessionId];
        
        const char * charAccount = [strAccount UTF8String];
        const char * charUin = [strLoginUin UTF8String];
        const char * charSessionId = [strSessionId UTF8String];

        Login::getInstance()->getLoginInfo(charAccount, charUin, charSessionId);
        
        // 提示信息
//        NSString * strLoginTip = @"登录中，请稍后...";
//        
//        const char * charLoginTip = [strLoginTip UTF8String];
//        
//        GameView::getInstance()->showAlertDialog(charLoginTip);
        
        //toolbar
        //[[WmComPlatform defaultPlatform] WmToolBarHide:NO];
        
    }
    // 登录失败处理和相应提示
    else
    {
        //		int error = [[dict objectForKey:@"error"] intValue];
        
        NSString* strTip = @"";
        
        switch (error_code)
        {
            case WM_COM_PLATFORM_ERROR_USER_CANCEL:                        // 取消操作
                strTip = @"取消操作";
                
                return ;
                
                break;
            
            case WM_COM_PLATFORM_ERROR_NOT_LOGINED:                        // 未登录
                strTip = @"未登录";
                break;
            case WM_COM_PLATFORM_ERROR_ACTIVE_FAILURE:                     // 用户激活失败
                strTip = @"用户激活失败";
                break;
            case WM_COM_PLATFORM_ERROR_EMAIL_SAVE:                         // 邮件保存
                strTip = @"邮件保存";
                break;
            case WM_COM_PLATFORM_ERROR_NETWORK_FAIL:                       // 网络不可用
                strTip = @"网络不可用";
                break;
            case WM_COM_PLATFORM_ERROR_UNKNOWN:                            // 未知错误
                strTip = @"未知错误";
                break;
            case WM_COM_PLATFORM_ERROR_CLIENT_APP_ID_INVALID:              // 无效的应用ID接入
                strTip = @"无效的应用ID接入";
                break;
            case WM_COM_PLATFORM_ERROR_USER_NOT_EXIST:                     // 该用户不存在
                strTip = @"该用户不存在";
                break;
            case WM_COM_PLATFORM_ERROR_SESSIONID_INVALID:                  // SessionId（用户的会话标识）无效
                strTip = @"SessionId无效";
                break;
            case WM_COM_PLATFORM_ERROR_VERIFY_ACCOUNT_FAIL:                // 账号验证失败
                strTip = @"账号验证失败";
                break;
            case WM_COM_PLATFORM_ERROR_PARAM:                              // 参数值错误或非法，请检查参数值是否有效
                strTip = @"参数值错误或非法";
                break;
            case WM_COM_PLATFORM_ERROR_IMAGE_SIZE_TOO_LARGE:               // 发送的图片数据超过了服务器允许的大小
                strTip = @"发送的图片数据超过了服务器允许的大小";
                break;
            case WM_COM_PLATFORM_ERROR_PHONE_NUM_BE_REGISTERED:            // 邮箱或者手机号已经存在
                strTip = @"邮箱或者手机号已经存在";
                break;
            case WM_COM_PLATFORM_ERROR_OLD_PASSWORD_ERROR:                 // 原密码错误
                strTip = @"原密码错误";
                break;
            case WM_COM_PLATFORM_ERROR_GUEST_NOT_EXIST:                    // 临时用户不存在
                strTip = @"临时用户不存在";
                break;
            case WM_COM_PLATFORM_ERROR_NOT_CURENT_RECOVERY_CODE:           // 恢复码与游戏不对应
                strTip = @"恢复码与游戏不对应";
                break;
            case WM_COM_PLATFORM_ERROR_FORBIDDEN_USER_LOGIN:               // 用户已被禁止登录
                strTip = @"用户已被禁止登录";
                break;
            case WM_COM_PLATFORM_ERROR_FORBIDDEN_DEVICE_LOGIN:             // 设备已被禁止登录
                strTip = @"设备已被禁止登录";
                break;
            case WM_COM_PLATFORM_ERROR_FORBIDDEN_USER_PAY:                 // 用户已被禁止支付
                strTip = @"用户已被禁止支付";
                break;
            case WM_COM_PLATFORM_ERROR_FORBIDDEN_DEVICE_PAY:               // 设备已被禁止支付
                strTip = @"设备已被禁止支付";
                break;
                
            default:
                //            {
                //                NSString    *strTip = [NSString stringWithFormat:@"登录失败，%@", error_info];
                //                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:strTip
                //                                                                message:nil
                //                                                               delegate:nil
                //                                                      cancelButtonTitle:nil
                //                                                      otherButtonTitles:@"确定", nil];
                //                [alert show];
                //                [alert release];
                //            }
                break;
        }
        
        const char * charTip = [strTip UTF8String];
        
        GameView::getInstance()->showAlertDialog(charTip);
        
        //[ExtenalClass messageBox:strTip];
    }
}

-(void)WMLeavePlatform:(NSNotification *)notify
{
    cocos2d::CCUserDefault::sharedUserDefault()->flush();
    GameMessageProcessor::sharedMsgProcessor()->sendReq(1002);
    
    //runLogin();
}

-(void)WMUserImproveAccount:(NSNotification *)notify
{
    NSString * strAccount = [[WmComPlatform defaultPlatform] userName];
    NSString * strLoginUin = [[WmComPlatform defaultPlatform] loginUin];
    NSString * strSessionId = [[WmComPlatform defaultPlatform] sessionId];
    
    const char * charOfficalAccount = [strAccount UTF8String];
    const char * charGuestAccount = [s_guestAccount UTF8String];
    const char * charUin = [strLoginUin UTF8String];
    const char * charSessionId = [strSessionId UTF8String];
    
    //Login::getInstance()->getLoginInfo(charAccount, charUin, charSessionId);
    
    Login::getInstance()->getGuestLoginInfo(charOfficalAccount, charGuestAccount, charUin, charSessionId);
}

-(void)WmUniPayResult:(NSNotification *)notify                     // 支付付款返回结果通知
{
    NSDictionary *dic = notify.object;
    BOOL bSuccess = [[dic objectForKey:@"result"] boolValue];
    NSString *str = bSuccess ? @"购买成功" : @"购买失败";
    
    if (!bSuccess)
    {
        // TODO: 购买失败处理
        NSString *strError = [dic objectForKey:@"errorDescription"];
        
        if (!strError)
        {
            
            int nErrorCode = [[dic objectForKey:@"error"] intValue];
            switch (nErrorCode)
            {
                case WM_COM_PLATFORM_ERROR_PAY_CANCELED:
                    strError = @"用户取消支付操作";
                    
                    return;
                    
                case WM_COM_PLATFORM_ERROR_NETWORK_FAIL:
                    strError = @"网络连接错误";
                    break;
                case WM_COM_PLATFORM_ERROR_FAILED:
                    strError = @"支付失败";
                    break;
                default:
                    strError = @"购买过程发生错误"; break;
            }
        }
        
        str = [str stringByAppendingFormat:@": %@", strError];
        
        // 提示信息
        NSString * strTip = str;
        const char * charChargeTip = [strTip UTF8String];
        GameView::getInstance()->showAlertDialog(charChargeTip);
        
    } else
    {
        // TODO: 购买成功处理
        
        // 提示信息
        NSString * strTip = @"支付成功";
        const char * charChargeTip = [strTip UTF8String];
        GameView::getInstance()->showAlertDialog(charChargeTip);
    }
    
    // 本次购买的请求参数
//    WmBuyInfo *buyInfo = (WmBuyInfo *)[dic objectForKey:@"buyInfo"];
//    str = [str stringByAppendingFormat:@"\n<productPrice = %f, payDescription = %@, cooOrderSerial = %@>", (float)buyInfo.productPrice / 100, buyInfo.payDescription, buyInfo.cooOrderSerial];
//    NSLog(@"WmUiPayResult: %@", str);
    
//    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"支付结果" message:str delegate:nil cancelButtonTitle:@"确 定" otherButtonTitles:nil, nil];
//    [alertView show];
//    [alertView release];
}

//-(WmBuyInfo *)getBuyInfoFromUI:(int)tag:(NSString *)info
//{
////    CFUUIDRef theUUID = CFUUIDCreate(NULL);
////    CFStringRef guid = CFUUIDCreateString(NULL, theUUID);
////    
////    CFRelease(theUUID);
////    NSString *uuidString = [((NSString *)guid)stringByReplacingOccurrencesOfString : @"-" withString : @""];
////    CFRelease(guid);
////    
////    WmBuyInfo *buyInfo = [[WmBuyInfo new] autorelease];
////    buyInfo.cooOrderSerial = [uuidString lowercaseString];
//////    buyInfo.productPrice = tag * 10;
////    buyInfo.productPrice = tag;
////    buyInfo.payDescription = @"元宝";
////    buyInfo.ext = @"123";
////    buyInfo.serverId = @"1234";
////    
////    if ([buyInfo isValidBuyInfo] ) {
////        return buyInfo;
////    } else {
////        NSLog(@"商品信息不全");
////        return nil;
////    }
//    
//    char charOrderNum[20];
//    sprintf(charOrderNum, "%lld", s_payNum);
//    
//    char charUserId[20];
//    sprintf(charUserId, "%lld", Login::userId);
//    
//   // NSString * strAccount = [[WmComPlatform defaultPlatform] userName];
//   
//    //NSString * strSessionId = [[WmComPlatform defaultPlatform] sessionId];
//    
//    //const char * charAccount = [strAccount UTF8String];
//    //const char * charUin = [strLoginUin UTF8String];
//    //const char * charSessionId = [strSessionId UTF8String];
// 
//    
//    WmBuyInfo *buyInfo = [[WmBuyInfo new] autorelease];
//    buyInfo.cooOrderSerial = [NSString stringWithUTF8String:charOrderNum];
//    //buyInfo.productPrice = tag * 10;
//    buyInfo.productPrice = 1;
//    buyInfo.payDescription = @"1元宝";
//    //buyInfo.ext = [NSString stringWithUTF8String:charUserId];
//    //buyInfo.serverId = @"1234";
//    
//    if ([buyInfo isValidBuyInfo] )
//    {
//        return buyInfo;
//    }
//    else
//    {
//        NSLog(@"商品信息不全");
//        return nil;
//    }
//}

-(WmBuyInfo *)getBuyInfo:(int)goodsPrice:(NSString *)goodsName;
{
    // 订单号
    char charOrderNum[20];
    sprintf(charOrderNum, "%lld", s_payNum);
    
    char charGoodsPrice[20];
    sprintf(charGoodsPrice, "%d", goodsPrice);
    
    const char * charGoodsName = [goodsName UTF8String];

    
    WmBuyInfo *buyInfo = [[WmBuyInfo new] autorelease];
    buyInfo.cooOrderSerial = [NSString stringWithUTF8String:charOrderNum];                // 订单号
    //buyInfo.productPrice = goodsPrice;                                                  // 商品价格（以分为单位，显示时是元，例如：goodsPrice为1的话，则显示0.01元
    buyInfo.productPrice = goodsPrice;
    //buyInfo.payDescription = @"1元宝";
    buyInfo.payDescription = goodsName;                                                   // 商品名称
    //buyInfo.ext = [NSString stringWithUTF8String:charUserId];
    //buyInfo.serverId = @"1234";
    
    if ([buyInfo isValidBuyInfo] )
    {
        return buyInfo;
    }
    else
    {
        NSLog(@"商品信息不全");
        return nil;
    }
}


@end

