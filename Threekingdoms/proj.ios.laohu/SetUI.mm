#include "SetUI.h"
#include "GameStateBasic.h"
#include "../Classes/login_state/LoginState.h"
#include "../Classes/gamescene_state/GameSceneState.h"
#include "../Classes/loadscene_state/LoadSceneState.h"
#include "../Classes/messageclient/GameMessageProcessor.h"
#include "../Classes/ui/extensions/UITab.h"
#include "SimpleAudioEngine.h"
#include "../Classes/login_state/Login.h"
#include "GameAudio.h"
#include "../Classes/GameUserDefault.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/GameView.h"
#include "../Classes/utils/GameUtils.h"

#import "WmComPlatform.h"
#import "ExtenalClass.h"
#import "../Classes/login_state/Login.h"

using namespace CocosDenshion;

long long SetUI::s_orderNum = 0;

SetUI::SetUI(void)
{
}


SetUI::~SetUI(void)
{
}

SetUI * SetUI::create()
{
	SetUI * setui=new SetUI();
	if (setui && setui->init())
	{
		setui->autorelease();
		return setui;
	}
	CC_SAFE_DELETE(setui);
	return NULL;
}

bool SetUI::init()
{
	if (UIScene::init())
	{
		CCSize size=CCDirector::sharedDirector()->getVisibleSize();
		//get userdefault.xml by:liutao
		SetUI::getUserDefault();
        
		//SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(m_nMusic);
		//SimpleAudioEngine::sharedEngine()->playBackgroundMusic("music0.mid", true);
        
		UILayer * layer=UILayer::create();
		layer->ignoreAnchorPointForPosition(false);
		layer->setAnchorPoint(ccp(0.5f,0.5f));
		layer->setPosition(ccp(size.width/2,size.height/2));
		layer->setContentSize(CCSizeMake(800,480));
		addChild(layer);
        
		cocos2d::extension::UIImageView *mengban = cocos2d::extension::UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(size);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);
        
		if(LoadSceneLayer::setUiPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::setUiPanel->removeFromParentAndCleanup(false);
		}
        
		UIPanel *setPanel=LoadSceneLayer::setUiPanel;
		setPanel->setAnchorPoint(ccp(0.5f,0.5f));
		setPanel->setPosition(ccp(size.width/2,size.height/2));
		m_pUiLayer->addWidget(setPanel);
        
        // close btn
        cocos2d::extension::UIButton * btn_Exit= (cocos2d::extension::UIButton *)UIHelper::seekWidgetByName(setPanel,"Button_close");
		btn_Exit->setTouchEnable(true);
		btn_Exit->setPressedActionEnabled(true);
		btn_Exit->addReleaseEvent(this,coco_releaseselector(SetUI::callBackExit));
        
        // 更换账号
		cocos2d::extension::UIButton * btn_reload= (cocos2d::extension::UIButton *)UIHelper::seekWidgetByName(setPanel,"Button_return_landing");
		btn_reload->setTouchEnable(true);
		btn_reload->setPressedActionEnabled(true);
		btn_reload->addReleaseEvent(this,coco_releaseselector(SetUI::reLoading));
        
        
        // 联系我们
        //		cocos2d::extension::UIButton * btn_ContactUs = (cocos2d::extension::UIButton *)UIHelper::seekWidgetByName(setPanel,"Button_ContactUs");
        //		btn_ContactUs->setTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");
        //		btn_ContactUs->setTouchEnable(true);
        //		//btn_ContactUs->setPressedActionEnabled(true);
        //		btn_ContactUs->addReleaseEvent(this,coco_releaseselector(SetUI::callBackComeSoon));
        
        // 个人中心
        cocos2d::extension::UIButton * btn_personCenter = (cocos2d::extension::UIButton
                                                           *)UIHelper::seekWidgetByName(setPanel, "Button_personCenter");
        btn_personCenter->setTouchEnable(true);
        btn_personCenter->setPressedActionEnabled(true);
        btn_personCenter->addReleaseEvent(this, coco_releaseselector(SetUI::callBackPersonCenter));
        btn_personCenter->setVisible(true);
        
        // 老虎社区
        cocos2d::extension::UIButton * btn_laohuSquare = (cocos2d::extension::UIButton
                                                          *)UIHelper::seekWidgetByName(setPanel, "Button_laohuSquare");
        btn_laohuSquare->setTouchEnable(true);
        btn_laohuSquare->setPressedActionEnabled(true);
        btn_laohuSquare->addReleaseEvent(this, coco_releaseselector(SetUI::callBackLaohuSquare));
        btn_laohuSquare->setVisible(true);
        
		cocos2d::extension::UISlider * slider_Music = (cocos2d::extension::UISlider *)UIHelper::seekWidgetByName(setPanel,"Slider_music");
		slider_Music->setTouchEnable(true);
		slider_Music->setPercent(m_nMusic);
		slider_Music->addEventListenerSlider(this, sliderpercentchangedselector(SetUI::sliderMusicEvent));
        
		cocos2d::extension::UISlider * slider_Sound = (cocos2d::extension::UISlider *)UIHelper::seekWidgetByName(setPanel,"Slider_sound");
		slider_Sound->setTouchEnable(true);
		slider_Sound->setPercent(m_nSound);
		slider_Sound->addEventListenerSlider(this, sliderpercentchangedselector(SetUI::sliderSoundEvent));
        
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(size);
		return true;
	}
	return false;
}

void SetUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void SetUI::onExit()
{
	UIScene::onExit();
}

 void SetUI::tabIndexChangedEvent(CCObject* pSender)
 {

 }

void SetUI::callBackComeSoon(CCObject * obj)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillui_nothavefuction"));
    
    // 暂时在此处调用支付接口
    //[[WmComPlatform defaultPlatform] WmUniPay:[[ExtenalClass sharedIntance] getBuyInfoFromUI:1:@"春哥"]];
    
    //requestPay();
}

void SetUI::callBackPersonCenter(CCObject * obj)
{
    // 老虎SDK 个人中心
    [[WmComPlatform defaultPlatform] WmEnterUserSetting];
}

void SetUI::callBackLaohuSquare(CCObject * obj)
{
    // 老虎SDK 老虎社区
    [[WmComPlatform defaultPlatform] WmEnterDiscuzPage];
}

void SetUI::callBackExit(CCObject * obj)
{
	float volume = (float)m_nMusic/100.0f;
	SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(volume);
	CCUserDefault::sharedUserDefault()->setIntegerForKey(MUSIC, m_nMusic);

	float effectVolume = (float)m_nSound/100.0f;
	SimpleAudioEngine::sharedEngine()->setEffectsVolume(effectVolume);
	CCUserDefault::sharedUserDefault()->setIntegerForKey(SOUND, m_nSound);

	CCUserDefault::sharedUserDefault()->flush();

	if(volume <= 0)
		SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	
	this->closeAnim();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

void SetUI::reLoading( CCObject * obj )
{
	float volume = (float)m_nMusic/100.0f;
	SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(volume);
	CCUserDefault::sharedUserDefault()->setIntegerForKey(MUSIC, m_nMusic);

	float effectVolume = (float)m_nSound/100.0f;
	SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(effectVolume);
	CCUserDefault::sharedUserDefault()->setIntegerForKey(SOUND, m_nSound);

    // 注销
	[[WmComPlatform defaultPlatform] WmLogout:0];
}
/*
void SetUI::closeWindow(CCObject * obj)
{
	CCUserDefault::sharedUserDefault()->flush();
	//SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	CCDirector::sharedDirector()->end();
}
*/

void SetUI::sliderMusicEvent(CCObject *pSender, SliderEventType type)
{
    switch (type)
    {
        case cocos2d::extension::SLIDER_PERCENTCHANGED:
        {
            cocos2d::extension::UISlider* slider = dynamic_cast<cocos2d::extension::UISlider*>(pSender);
            int percent = slider->getPercent();
			m_nMusic = percent;
			//float volume = (float)percent/100.0f;
			//SimpleAudioEngine::sharedEngine()->setBackgroundMusicVolume(volume);
			//CCLOG("%d", percent);
			//CCUserDefault::sharedUserDefault()->setIntegerForKey(MUSIC, percent);
			//music regulate
        }
            break;
            
        default:
            break;
    }
}

void SetUI::sliderSoundEvent(CCObject *pSender, SliderEventType type)
{
    switch (type)
    {
        case cocos2d::extension::SLIDER_PERCENTCHANGED:
        {
            cocos2d::extension::UISlider* slider = dynamic_cast<cocos2d::extension::UISlider*>(pSender);
            int percent = slider->getPercent();
			m_nSound = percent;
			//float volume = (float)percent/100.0f;
			//SimpleAudioEngine::sharedEngine()->setEffectsVolume(volume);
			//SimpleAudioEngine::sharedEngine()->playEffect("event.mid", true);

			//CCLog("%d", percent);
			//CCUserDefault::sharedUserDefault()->setIntegerForKey(SOUND, percent);
			//sound regulate
        }
            break;
            
        default:
            break;
    }
}

void SetUI::getUserDefault()
{
	m_nMusic = CCUserDefault::sharedUserDefault()->getIntegerForKey(MUSIC,50);
    CCLog("m_nMusic is %d", m_nMusic);

	m_nSound = CCUserDefault::sharedUserDefault()->getIntegerForKey(SOUND,50);
    CCLog("m_nSound is %d", m_nSound);
}

void SetUI::requestPay()
{
    CCHttpRequest* request = new CCHttpRequest();
    std::string url = "http://124.202.137.34";
    url.append(":6666/gen_order");
    request->setUrl(url.c_str());
    request->setRequestType(CCHttpRequest::kHttpPost);
    request->setResponseCallback(this, httpresponse_selector(SetUI::onHttpResponsePay));
    
    //    Nd91LoginReq* nd91LoginReq = new Nd91LoginReq();
    //    nd91LoginReq->set_uin(strLoginUin);
    //    nd91LoginReq->set_sessionid(strSessionId);
    
    //    const char * charLoginUin = strLoginUin.c_str();
    //
    //    LaoHuLoginReq * laohuLoginReq = new LaoHuLoginReq();
    //    laohuLoginReq->set_userid(atoll(charLoginUin));
    //    laohuLoginReq->set_token(strSessionId);
    //
    //    LoginReq httpReq;
    //    //httpReq.set_account(pAccountInputBox->getInputString());
    //    //httpReq.set_authenticid(pPinInputBoxr->getInputString());
    //    httpReq.set_account(strAccount);
    //    httpReq.set_accountnum(0);
    //    httpReq.set_accountpt("test");
    //    httpReq.set_imei("test");
    //    httpReq.set_ua("test");
    //    httpReq.set_channelid(LoginReq_Channel_LAOHU);
    //    httpReq.set_batchid(0);
    //    httpReq.set_recommendid(0);
    //    httpReq.set_systemversion("test");
    //    httpReq.set_nettype(0);
    //    httpReq.set_apkwithres(0);
    //    httpReq.set_platformuserid("test");
    //    //httpReq.set_allocated_nd91loginreq(nd91LoginReq);
    //    httpReq.set_allocated_laohuloginreq(laohuLoginReq);
    
    //    NSString * strLoginUin = [[WmComPlatform defaultPlatform] loginUin];
    //    NSString * strSessionId = [[WmComPlatform defaultPlatform] sessionId];
    //
    //    const char * charLoginUin = [strLoginUin UTF8String];
    //    const char * charSessionId = [strSessionId UTF8String];
    
    GenOrderReq genOrderReq;
    genOrderReq.set_channelid(3);
    genOrderReq.set_userid(Login::userId);
    genOrderReq.set_sessionid(Login::sessionId);
    genOrderReq.set_commodity(GenOrderReq_Commodity_GOLDINGOT);
    
    string msgData;
    genOrderReq.SerializeToString(&msgData);
    
    request->setRequestData(msgData.c_str(), msgData.size());
    request->setTag("POST");
    CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
    CCHttpClient::getInstance()->send(request);
    request->release();
}

void SetUI::onHttpResponsePay(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag()))
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed())
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
    
	GenOrderRsp* m_pGenOrderRsp = new GenOrderRsp();
	m_pGenOrderRsp->ParseFromString(strdata);
    
    long long tmpNum = m_pGenOrderRsp->ordernum();
    
    char charOrderNum[20];
    sprintf(charOrderNum, "%lld", tmpNum);
    
    s_orderNum = tmpNum;
    
    //s_orderNum = m_pGenOrderRsp->ordernum();
    
    //	if(1 == m_pGenOrderRsp->result())
    //	{
    //		//userId = m_pRegisterRsp->userid();
    //		//sessionId = m_pRegisterRsp->sessionid();
    //		//onMenuGetTestClicked();
    //
    //        longOrderNum = m_pGenOrderRsp->ordernum();
    //	}
    //	else
    //	{
    //		GameView::getInstance()->showAlertDialog(m_pRegisterRsp->resultmessage());
    //	}
    
    
    
    [[WmComPlatform defaultPlatform] WmUniPay:[[ExtenalClass sharedIntance] getBuyInfoFromUI:1:@"春哥"]];
}