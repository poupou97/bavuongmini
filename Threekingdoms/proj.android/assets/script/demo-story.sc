
// PART I
// 开启屏幕上下的黑边
MovieStart()

BlackScreenStart()
// SimpleDialog(突然，远处传来一声女子的/#ff0000 尖叫/！)
SimpleDialog(兄弟们，开始结拜了。)
BlackScreenEnd()

Dialog(哈哈，少侠来得正好!, npc/LiuBei.png, 0,刘备)
//Wait(1000)
AddScreenEffect(animation/texiao/particledesigner/taohua3.plist, 0.25, 1.0, 1)
AddScreenEffect(animation/texiao/particledesigner/taohua3.plist, 0.75, 1.0, 2)
Dialog(刚刚我们三个相谈甚欢，已经决定要在此结为异姓兄弟了！, npc/GuanYu.png, 1,关羽)
Dialog(四海之内能找到知己当真是乐事，我当然愿意作见证！, hero, 0,hero)
Dialog(今日念刘备、关羽、张飞，虽然异姓，既结为兄弟！, npc/LiuBei.png, 0,刘备)
Dialog(不求同年同月同日生, npc/GuanYu.png, 1,关羽)
Dialog(但求同年同月同日死, npc/ZhangFei.png, 1,张飞)
Dialog(皇天后土，实鉴此心。, npc/LiuBei.png, 0,刘备)
Dialog(刘兄宅心仁厚，又有关兄和张兄武艺高强，将来必能成就一番大事！, hero, 0,hero)
RemoveScreenEffect(1)
RemoveScreenEffect(2)

BlackScreenStart()
SimpleDialog(这就是被传为佳话的桃园结义，义从这里开始。。。)
BlackScreenEnd()

// 关闭屏幕上下的黑边
MovieEnd()
