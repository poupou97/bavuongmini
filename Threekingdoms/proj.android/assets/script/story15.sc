
// PART I
// 开启屏幕上下的黑边
MovieStart()

Dialog(嘿嘿，年轻人，你我终于见面了。, npc/cz.png, 0,算命先生)
Dialog(老先生您认识我？您知道我是怎么来到这个世界的么？您有没有办法让我离开这个时代呢？, hero, 1,hero)
Dialog(真亦假时假亦真，你以为是真实的说不定却只是个梦，年轻人，既来之，则安之吧。, npc/cz.png, 0,算命先生)
Dialog(可是现在还在打黄巾军，这乱世分明是刚刚开始啊。我这人生地不熟的，到底投靠谁呢。, hero, 1,hero)
Dialog(合久必分，分久必合，这你不用担心，以后当你困惑应该怎么做的时候就可以来找我，我自会帮你解答。, npc/cz.png, 0,算命先生)
Dialog(这么灵，那现在黄巾军已经被灭的差不多了，您跟我说说接下来我需要做什么呢，, hero, 1,hero)
Dialog(接下来当你回去就会有人告诉你最近黄巾贼被铲除了，但是让董卓那厮占了便宜，现在竟挟天子以令诸侯。, npc/cz.png, 0,算命先生)
Dialog(这董卓简直是没人性！接下来你的任务就是做掉他，事不宜迟，出发吧骚年！, npc/cz.png, 0,算命先生)

//Wait(1000)

// 关闭屏幕上下的黑边
MovieEnd()
