
// PART I
// 开启屏幕上下的黑边
MovieStart()

// 为剧情，清除场景
ClearScene()

// 创建角色
// dir参数的说明  up: 0, left: 1, down: 2, right: 3
CreateNPC(2, animation/generals/zsgg_zf, 675, 622, 张飞, 3)
CreateNPC(3, animation/generals/zsgg_gy, 1056, 813, 关羽, 1)
CreateNPC(4, animation/monster/zsgr_qfjd, 928, 976, 村民, 0)
CreateNPC(5, animation/monster/zsgr_lrj, 416, 944, 村民, 0)
CreateNPC(6, animation/monster/zsgr_bn, 544, 592, 村民, 2)
CreateNPC(7, heroine, 928, 720, heroine, 2)
CreateNPC(8, animation/monster/zsgr_ch, 305, 712, 村民, 3)

SetDir(hero, 1)

MoveCam(651, 811, 2500)
MoveNPC(2, 574, 801, 2500)
MoveNPC(3, 741, 826, 2500)
Wait(2500)
SetDir(2, 3)
Dialog(今天赢不了你，我头割下来给你！, general/zhangfei.png, 0,张飞)
Dialog(来来来，我们大战三百回合！, general/guanyu.png, 1,关羽)

Say(4, 有好戏看了，大家快来啊！, 1000)
MoveNPC(4, 864, 912, 1200)
MoveNPC(5, 480, 912, 1200)
MoveNPC(6, 608, 656, 1200)
MoveNPC(8, 428, 738, 1200)



Say(2, 哇呀呀呀~, 1000)
Say(3, 放着我来！, 1000)
Wait(1000)

// 2: attack action
// 张飞
PlayAct(2, 2)
SetSE(animation/texiao/renwutexiao/AX1/ax1.anm, 741, 827)
Wait(500)

// 关羽
Say(3, 力道不小啊，看我的, 500)
PlayAct(3, 2)
SetSE(animation/texiao/renwutexiao/BY3/by3.anm, 574, 802)
Wait(500)

// 张飞
PlayAct(2, 2)
SetSE(animation/texiao/renwutexiao/BX3/BX3.anm, 741, 827)
Wait(500)

// 关羽
PlayAct(3, 2)
SetSE(animation/texiao/renwutexiao/BX2/bx2.anm, 574, 802)
Wait(500)
// 张飞
PlayAct(2, 2)
SetSE(animation/texiao/renwutexiao/AY1/ay1.anm, 741, 827)
Wait(500)

// 关羽
PlayAct(3, 2)
SetSE(animation/texiao/renwutexiao/AZ2/az2.anm, 574, 802)
Wait(500)
CreateNPC(1, animation/generals/zsgg_lb, 928, 656, 刘备, 2)

// Wait(1000)
MoveNPC(1, 672, 720, 2000)
Wait(2000)
SetDir(1, 2)
SetDir(2, 0)
SetDir(3, 0)
Dialog(二位英雄好汉，且先停手！如今黄巾猖獗，天下大乱，正是我们报效国家，一展抱负之时，何必在这为了小事自相残杀。, general/liubei.png, 0,刘备)
Dialog(哈哈，这位大哥误会了，我们只是切磋一下，好久没碰到这样的高手了!, general/guanyu.png, 1, 关羽)
Dialog(打的痛快！这朋友俺老张是交定了！, general/zhangfei.png, 0,张飞)
Dialog(/e25太好了，误会解除！还以为你俩真的在打架，吓得人家小心肝扑通扑通的, heroine, 1,heroine)


// 剧情结束，恢复为正常场景
ResumeScene()
Wait(500)
DeleteNPC(1)
DeleteNPC(2)
DeleteNPC(3)
DeleteNPC(4)
DeleteNPC(5)
DeleteNPC(6)
DeleteNPC(7)

// 关闭屏幕上下的黑边
MovieEnd()
