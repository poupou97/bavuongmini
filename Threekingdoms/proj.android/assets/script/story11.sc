
// PART I
// 开启屏幕上下的黑边
MovieStart()

// 为剧情，清除场景
ClearScene()

// 创建角色
// dir参数的说明  up: 0, left: 1, down: 2, right: 3
CreateNPC(1, animation/generals/zsgg_dq, 1056, 496, 左慈仙子, 3)
CreateNPC(2, animation/npc/zsgn_NewbieGuide, 1248, 400, 于吉仙子, 0)
MoveNPC(1, 1312, 464, 1200)

Wait(1200)
SetDir(1, 3)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.1, 1, 1)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.9, 0.9, 2)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.1, 0.1, 3)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.9, 0.1, 4)
Dialog(姐姐，他竟真的能拔出/#00ffff 龙吟仙剑/，剑还发出/#00ffff 龙吟之声/，看来它已认定它的主人了。总算是没有枉费我们姐妹在此苦等多年。, general/daqiao.png, 0,左慈仙子)
Dialog(当年南华老头收徒不严，他出于好意将/#00ffff 《太平要术》/传与/#00ff00 张角/，虽只是极小一部分法术，但张角日夜修习，竟修得呼风唤雨。 , general/newbieguide.png, 0,于吉仙子)
Dialog(谁知张角竟四处散播瘟疫后假借散施符水为人治病，让百姓喝下他的蛊惑之水，激发起人类心底最邪恶的贪欲并为他所用。, general/newbieguide.png, 0,于吉仙子)
Dialog(更糟的是贪念的剧增竟给/#ff0000 魔星/增添了能量，最近竟然异常闪耀。不久后天下必然大乱。, general/newbieguide.png, 0,于吉仙子)
Dialog(此事均因南华老头将/#00ffff 《太平要术》/传与自称/#00ffff “大贤良师”/的/#00ff00 张角/而起，如今他倒不知去哪躲了清静。, general/daqiao.png, 0,左慈仙子)
Dialog(如今只有/#fefe33 苍龙大帝/才能亲自解救这苍生疾苦。你既然是/#fefe33 苍龙大帝/转世，必要担起/#fefe33 拯救苍生/的大任，现在你需斩除张角及他的黄巾军。, general/daqiao.png, 0,左慈仙子)
Dialog(我？张角可以呼风唤雨，我既不会武功又不会法术，哪有那个本事除掉他呢。 , hero, 1,hero)
Dialog(张角如今已病死于军中，也是应验了南华老头的那句/#00ffff “若萌异心，必获恶报”/吧。只是他的手下残余如今仍在作乱，需要你去彻底铲除罢了。 , general/daqiao.png, 0,左慈仙子)
PlayAct(1, 2)
SetSE(animation/texiao/renwutexiao/BZ2/bz2.anm, 1312, 336)
Wait(500)
Dialog(放心吧，我已给你注入了/#00ffff 真气/，我们不能离开碧落仙境，回到凡间后如果你遇到什么问题……你……可去找……, general/newbieguide.png, 0,于吉仙子)
// 开启黑屏
BlackScreenStart()
// 屏幕中心出现简单对话
SimpleDialog(/#fefe33 眼前渐渐模糊……/,500)
SimpleDialog(/#fefe33 什么也听不清楚……/,500)
// 关闭黑屏
BlackScreenEnd()
DeleteNPC(1)
DeleteNPC(2)
Dialog(/e12不是吧！怎么话没说完就都不见了……到底让我有困难去找谁呢……唉，只有走一步看一步吧。 , hero, 1,hero)
RemoveScreenEffect(1)
RemoveScreenEffect(2)
RemoveScreenEffect(3)
RemoveScreenEffect(4)

//Wait(1000)

// 剧情结束，恢复为正常场景
ResumeScene()
Wait(500)
// 关闭屏幕上下的黑边
MovieEnd()
