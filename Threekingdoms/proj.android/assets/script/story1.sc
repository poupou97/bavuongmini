
// PART I
// 开启屏幕上下的黑边
MovieStart()

// 为剧情，清除场景
ClearScene()

SetDir(hero, 2)

MoveCam(928, 688, 600)
Wait(1000)
// 创建角色
// dir参数的说明  up: 0, left: 1, down: 2, right: 3
CreateNPC(1, animation/generals/zsgg_lb, 1056, 560, 刘备, 2)
CreateNPC(2, animation/generals/zsgg_zf, 576, 765, 张飞, 3)
CreateNPC(3, animation/generals/zsgg_gy, 764, 860, 关羽, 1)

Dialog(哈哈，少侠来得正好!二位贤弟也同来吧。, general/liubei.png, 0,刘备)
MoveNPC(1, 864, 720, 2000)
MoveNPC(2, 672, 720, 900)
MoveNPC(3, 864, 816, 900)

Wait(2200)

Dialog(刚刚我们三个相谈甚欢，已经决定要在此结为异姓兄弟了！, general/guanyu.png, 0,关羽)
Dialog(正是，所以俺们想请少侠做个见证！, general/zhangfei.png, 0,张飞)
Dialog(四海之内能找到知己当真是乐事，我当然愿意作见证！, hero, 1,hero)



AddScreenEffect(animation/texiao/particledesigner/taohua3.plist, 0.25, 1.0, 1)
AddScreenEffect(animation/texiao/particledesigner/taohua3.plist, 0.75, 1.0, 2)

Wait(800)

//up: 0, left: 1, down: 2, right: 3
SetDir(1, 3)

Wait(800)
MoveNPC(1, 928, 688, 1000)
MoveNPC(2, 736, 688, 1000)
MoveNPC(3, 928, 784, 1000)
Wait(2000)

Dialog(今日念刘备、关羽、张飞，虽然异姓，既结为兄弟！, general/liubei.png, 0,刘备)

Wait(800)

//Dialog(不求同年同月同日生, general/guanyu.png, 1,关羽)
//Dialog(但求同年同月同日死, general/zhangfei.png, 1,张飞)
Say(1, 不求, 1100)
Wait(1100)
Say(2, 同年同月同日生！, 1800)
//Say(1, 同！, 1000)
//Say(2, 日！, 1000)
//Say(3, 生！, 1000)
Wait(1800)
Say(1, 但求, 1100)
Wait(1100)
Say(3, 同年同月同日死！, 1800)
//Say(1, 同！, 1000)
//Say(2, 日！, 1000)
//Say(3, 死！, 1000)
Wait(2500)
Dialog(皇天后土，实鉴此心，背义忘恩，天人共戮！, general/liubei.png, 0,刘备)
Dialog(刘兄宅心仁厚，又有关兄和张兄武艺高强，将来必能成就一番大事！, hero, 1,hero)
RemoveScreenEffect(1)
RemoveScreenEffect(2)

DeleteNPC(1)
DeleteNPC(2)
DeleteNPC(3)

//镜头移动回主角所在地
MoveCam(-1,-1,1000)

Wait(1000)

// 剧情结束，恢复为正常场景
ResumeScene()
// 关闭屏幕上下的黑边
MovieEnd()
