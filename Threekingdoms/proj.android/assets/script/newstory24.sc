
// PART I
// 开启屏幕上下的黑边
MovieStart()
Dialog(我观察你很久了，资质不错。可以教你一点带兵打仗的技巧了。, general/zhugeliang.png, 0,诸葛亮)
Dialog(精髓就在右上角的【武将模式】按钮。主动攻击，指武将出战后会主动选择目标进行攻击；协助攻击，指武将会与你攻击相同的目标。, general/zhugeliang.png, 0,诸葛亮)
Dialog(自动出战，指进入战斗后武将会自动上场出战，无需手动点击；手动出战，指武将需要你点击才会出战，更有策略性。, general/zhugeliang.png, 0,诸葛亮)
Dialog(切记这些技巧，勤加练习，自能体会其中的奥妙。, general/zhugeliang.png, 0,诸葛亮)



// 关闭屏幕上下的黑边
MovieEnd()
