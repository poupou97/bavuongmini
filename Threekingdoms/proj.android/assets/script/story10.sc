
// PART I
// 开启屏幕上下的黑边
MovieStart()

ClearScene()

CreateNPC(1,animation/npc/zsgn_DiaoXiaoChan, 1248, 1072, 浣洗女子, 0)
CreateNPC(3, heroine, 1312, 1168, heroine, 0)
Wait(1000)
Say(1, 你看我是谁？, 1000)
SetSE(animation/texiao/renwutexiao/HMX/hmx.anm, 1248, 1071)
Wait(1000)
DeleteNPC(1)
CreateNPC(2,animation/generals/zsgg_dq , 1248, 1072, 左慈仙子, 2)
//Wait(500)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.1, 1, 1)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.9, 0.9, 2)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.1, 0.1, 3)
AddScreenEffect(animation/texiao/particledesigner/xiaoxingxing1.plist, 0.9, 0.1, 4)
Dialog(哎呦，你这是在变魔术么？吓了伦家一跳~, heroine, 0,heroine)
Dialog(刚才的姑娘和土匪均是我幻化出来的，想不到你有颇高的资质，亦无色与利的贪欲，如此看来你定是有些/#00ffff 仙缘/的，只是不知你是不是就是我们要等之人。, general/daqiao.png, 0,左慈仙子)
Dialog(能不能再讲的直白些呀，你们究竟在等谁呢？, hero, 1,hero)
Dialog(告诉你倒也无妨，此前南华老头，不，南华老仙集我三人之/#00ffff 仙书/，借助/#00ffff 三垣四象/之力/#00ffff 祈天降才/，之后却了无踪影，于是我和姐姐便在此苦等。 , general/daqiao.png, 0,左慈仙子)
Dialog(只有集天下帝王智勇于一身的/#fefe33 苍龙大帝/转世于世间方可解救众生于水火，还百姓太平盛世。, general/daqiao.png, 0,左慈仙子)
Dialog(如今你还需通过一项考验，也是最难的考验方可证明身份，此前已有几十人虽智勇过人，最后却因通不过此考验而/#00ffff 前功尽弃/，甚是可惜。如果通不过考验你也会……, general/daqiao.png, 0,左慈仙子)
Dialog(/e07 你可要好好加油了呦，不要给我丢脸哦！, heroine, 0,heroine)
Dialog(神仙姐姐，如今我都不知何去何从，倒是不妨一试吧。如此，你倒是说说这考验究竟是什么？, hero, 1,hero)

DeleteNPC(2)
DeleteNPC(3)

RemoveScreenEffect(1)
RemoveScreenEffect(2)
RemoveScreenEffect(3)
RemoveScreenEffect(4)

ResumeScene()
//Wait(1000)

// 关闭屏幕上下的黑边
MovieEnd()
