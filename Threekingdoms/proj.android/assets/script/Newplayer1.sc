// 开启屏幕上下的黑边
MovieStart()

// 为剧情，清除场景
ClearScene()

//=========================张角现身=========================
// dir参数的说明  up: 0, left: 1, down: 2, right: 3
CreateNPC(1, animation/generals/zsgg_zhangjiao, 543, 301, 张角, 1)
SetSE(animation/texiao/renwutexiao/CY2/cy2.anm, 543, 302)


//前排
CreateNPC(2, animation/monster/zsgr_hjjsb, 350, 205, , 1)
CreateNPC(3, animation/monster/zsgr_hjjsb, 350, 270, , 1)
CreateNPC(4, animation/monster/zsgr_hjjsb, 350, 335, , 1)


//中排
CreateNPC(5, animation/monster/zsgr_nvgjs, 415, 205, , 1)
CreateNPC(6, animation/monster/zsgr_nvgjs, 415, 270, , 1)
CreateNPC(7, animation/monster/zsgr_nvgjs, 415, 335, , 1)


//后排
CreateNPC(8, animation/monster/zsgr_ss, 480, 205, , 1)
CreateNPC(9, animation/monster/zsgr_ss, 480, 270, , 1)
CreateNPC(10, animation/monster/zsgr_ss, 480, 335, , 1)

Wait(500)
// //=========================主角出现=========================
CreateNPC(hero,hero,165,239,hero,3)
SetSE(animation/texiao/renwutexiao/SJTX/sjtx1.anm, 165, 240)
Wait(1000)

Dialog(张角，休走！ , hero, 1,hero)

Dialog(小盆友，是来加入我们黄巾军的吗？终身制，报名费只要998哦！,general/zhangjiao.png, 0,张角)

Dialog(妈妈说了，绝不能加入黑社会。我要报效国家，当一名公务员。今天抓了你，怎么也算个二等功吧~ , hero, 1,hero)

Dialog(真是自寻死路！给我灭了他！（我先溜）,general/zhangjiao.png, 0,张角)

MoveNPC(1,860,845,2000)
Wait(200)
MoveCam(400,580,500)

Wait(2100)
DeleteNPC(1)

Wait(500)

//SetSE(animation/texiao/changjingtexiao/ZHAMEN/zhamen1.anm, 370, 640)
//Wait(1300)
//SetSELoop(animation/texiao/changjingtexiao/ZHAMEN/zhamen2.anm, 370, 640)
//Wait(1500)


MoveCam(-1,-1,500)

Wait(1500)

ResumeScene()

MovieEnd()