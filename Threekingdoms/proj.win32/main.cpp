#include "main.h"
#include "AppDelegate.h"
#include "CCEGLView.h"

#include "GameUserDefault.h"

//#include <vld.h>

USING_NS_CC;

int APIENTRY _tWinMain(HINSTANCE hInstance,
                       HINSTANCE hPrevInstance,
                       LPTSTR    lpCmdLine,
                       int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);

    // create the application instance
    AppDelegate app;
    CCEGLView* eglView = CCEGLView::sharedOpenGLView();

	// "Threekingdoms ( 0.3.7 )"
	std::string appNameStr = "Threekingdoms";
	appNameStr.append(" ( ");
	appNameStr.append(GAME_CODE_VERSION);
	appNameStr.append(" )");
	eglView->setViewName(appNameStr.c_str());
	// android
    //eglView->setFrameSize(960, 540);
	//eglView->setFrameSize(800, 480);

	// ios
	//eglView->setFrameSize(1024, 768);      //ipad
	//eglView->setFrameSize(960, 640);        //iphone 4
	eglView->setFrameSize(1136, 640);   //iphone 5
	//eglView->setFrameSize(1334, 750);   //iphone 6
	//eglView->setFrameSize(1920, 1080);   //iphone 6 Plus

    return CCApplication::sharedApplication()->run();
}
