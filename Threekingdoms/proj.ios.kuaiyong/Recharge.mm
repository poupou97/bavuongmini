#include "Recharge.h"
#include "../Classes/loadscene_state/LoadSceneState.h"
#include "../Classes/GameView.h"
#include "UITab.h"
#include "../Classes/messageclient/element/CLable.h"
#include "../Classes/messageclient/element/CLableGoods.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/gamescene_state/MainScene.h"
#include "../Classes/messageclient/element/GoodsInfo.h"
#include "../Classes/utils/StrUtils.h"
#include "../Classes/ui/vip_ui/VipDetailUI.h"
#include "../Classes/ui/vip_ui/VipEveryDayRewardsUI.h"
#include "../Classes/ui/vip_ui/FirstBuyVipUI.h"
#include "RechargeCellItem.h"
#include "Counter.h"

#import "KYSDK.h"
#import "KYMobilePay.h"

#import "ExtenalClass.h"
#import "../Classes/login_state/Login.h"
#import "../Classes/login_state/LoginState.h"
#import "../Classes/utils/GameConfig.h"

#define MARQUEE_LABLE_WIDTH 596
#define GOLD_STORE_MAX_FILTER 6

RechargeUI::RechargeUI():
curLableId(0),
m_strMarquee("")
{
}


RechargeUI::~RechargeUI()
{

}

RechargeUI* RechargeUI::create()
{
	RechargeUI * rechargeUI = new RechargeUI();
	if (rechargeUI && rechargeUI->init())
	{
		rechargeUI->autorelease();
		return rechargeUI;
	}
	CC_SAFE_DELETE(rechargeUI);
	return rechargeUI;
}

bool RechargeUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		cocos2d::extension::UIImageView *mengban = cocos2d::extension::UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(800, 480));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		//º”‘ÿUI
		if(LoadSceneLayer::RechargePanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::RechargePanel->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::RechargePanel;
		ppanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->setPosition(CCPointZero);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		//πÿ±’∞¥≈•
		cocos2d::extension::UIButton * Button_close = (cocos2d::extension::UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(RechargeUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		cocos2d::extension::UIButton * Button_recharge = (cocos2d::extension::UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_buy_0");
		Button_recharge->setTouchEnable(true);
		Button_recharge->addReleaseEvent(this, coco_releaseselector(RechargeUI::RechargeEvent));
		Button_recharge->setPressedActionEnabled(true);

		cocos2d::extension::UIButton * Button_vip = (cocos2d::extension::UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_buy");
		Button_vip->setTouchEnable(true);
		Button_vip->addReleaseEvent(this, coco_releaseselector(RechargeUI::VipEvent));
		Button_vip->setPressedActionEnabled(true);

		l_ingot_value = (cocos2d::extension::UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_myIngotValue");
		char str_ingot[20];
		sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
		l_ingot_value->setText(str_ingot);
		
		UILabelBMFont* label_first = (UILabelBMFont*)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_1293");
		std::string strLabel;
		strLabel.append(StringDataManager::getString("RechargeUI_First_front"));
		memset(str_ingot, 0, sizeof(str_ingot));
		int a = atoi(VipValueConfig::s_vipValue[1].c_str());
		sprintf(str_ingot,"%d",a*100);
		strLabel.append(str_ingot);
		strLabel.append(StringDataManager::getString("RechargeUI_First_back"));
		label_first->setText(strLabel.c_str());
		
		UILabelBMFont* label_second = (UILabelBMFont*)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_1293_0");
		label_second->setText(StringDataManager::getString("RechargeUI_Second"));
		
		m_tableView = CCTableView::create(this,CCSizeMake(515,249));
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0.5f,0.5f));
		m_tableView->setPosition(ccp(145,83));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		u_layer->addChild(m_tableView);
		m_tableView->reloadData();

		//this->scheduleUpdate();

		return true;
	}
	return false;
}

void RechargeUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RechargeUI::onExit()
{
	UIScene::onExit();
}

bool RechargeUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void RechargeUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void RechargeUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void RechargeUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void RechargeUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void RechargeUI::callBackCounter(CCObject *obj)
{
	Counter * priceCounter =(Counter *)obj;
	int price = priceCounter->getInputNum();
	CCLOG("to SDK recharge");
    
    if(price > 100000)
    {
        NSString * strLoginTip = @"超过单笔金额上限100000，请您重试";
        const char * charLoginTip = [strLoginTip UTF8String];
        GameView::getInstance()->showAlertDialog(charLoginTip);
        
        return ;
    }
    
    //RechargeCellItem::s_nCharge = nCharge;
    [ExtenalClass set_Charge:price];
    
    requestPay();
}

void RechargeUI::RechargeEvent( CCObject *pSender )
{
	bool bChargeEnabled = GameConfig::getBoolForKey("charge_enable");
	if (true == bChargeEnabled)
	{
		GameView::getInstance()->showCounter(this, callfuncO_selector(RechargeUI::callBackCounter));
	}
	else
	{
		const char * charChargeEnabled = StringDataManager::getString("RechargeUI_disable");
		GameView::getInstance()->showAlertDialog(charChargeEnabled);
	}
}

void RechargeUI::VipEvent( CCObject *pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getGameScene()->getChildByTag(GameSceneLayer::kTagMainScene);
	CCLayer *vipScene = (CCLayer*)mainscene->getChildByTag(kTagVipDetailUI);
	if(vipScene == NULL)
	{
		VipDetailUI * vipDetailUI = VipDetailUI::create();
		mainscene->addChild(vipDetailUI,0,kTagVipDetailUI);
		vipDetailUI->ignoreAnchorPointForPosition(false);
		vipDetailUI->setAnchorPoint(ccp(0.5f,0.5f));
		vipDetailUI->setPosition(ccp(winSize.width/2, winSize.height/2));
	}
}

void RechargeUI::scrollViewDidScroll( CCScrollView* view )
{

}

void RechargeUI::scrollViewDidZoom( CCScrollView* view )
{

}

void RechargeUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{

}

cocos2d::CCSize RechargeUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	//return CCSizeMake(444,120);
	return CCSizeMake(490, 83);
}

cocos2d::extension::CCTableViewCell* RechargeUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();
	
	if (idx == RechargeConfig::s_rechargeValue.size()/2)
	{
		if (RechargeConfig::s_rechargeValue.size()%2 == 0)
		{
			RechargeCellItem * cellItem1 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+19]);
			cellItem1->setPosition(ccp(10,0));
			cell->addChild(cellItem1);

			RechargeCellItem * cellItem2 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+20]);
			cellItem2->setPosition(ccp(256,0));
			cell->addChild(cellItem2);
		}
		else
		{
			RechargeCellItem * cellItem1 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+19]);
			cellItem1->setPosition(ccp(10,0));
			cell->addChild(cellItem1);
		}
	}
	else
	{
		RechargeCellItem * cellItem1 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+19]);
		cellItem1->setPosition(ccp(10,0));
		cell->addChild(cellItem1);

		RechargeCellItem * cellItem2 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+20]);
		cellItem2->setPosition(ccp(256,0));
		cell->addChild(cellItem2);
	}
	
	return cell;
}

unsigned int RechargeUI::numberOfCellsInTableView( CCTableView *table )
{
	if (RechargeConfig::s_rechargeValue.size()%2 == 0)
	{
		return RechargeConfig::s_rechargeValue.size()/2;
	}
	else
	{
		return RechargeConfig::s_rechargeValue.size()/2 + 1;
	}
}

void RechargeUI::update(float dt)
{
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	l_ingot_value->setText(str_ingot);
}

void RechargeUI::requestPay()
{
    CCHttpRequest* request = new CCHttpRequest();
    //std::string url = "http://210.14.129.115";
    std::string url = Login::s_loginserver_ip;
    url.append(":6666/gen_order");
    request->setUrl(url.c_str());
    request->setRequestType(CCHttpRequest::kHttpPost);
    request->setResponseCallback(this, httpresponse_selector(RechargeUI::onHttpResponsePay));
    
    
    char charServerId[20];
    sprintf(charServerId, "%d", Login::m_pServerListRsp->serverinfos(LoginLayer::getCurrentServerId()).serverid());
    
    char charRoleId[20];
    sprintf(charRoleId, "%ld", LoginLayer::mRoleId);
    
    
    
    GenOrderReq genOrderReq;
    genOrderReq.set_channelid(6);                       // 注意此处的channelid
    genOrderReq.set_userid(Login::userId);
    genOrderReq.set_sessionid(Login::sessionId);
    genOrderReq.set_commodity(GenOrderReq_Commodity_GOLDINGOT);
    //genOrderReq.set_commodity(GenOrderReq_Commodity_VIP2);
    genOrderReq.set_serverid(Login::m_pServerListRsp->serverinfos(LoginLayer::getCurrentServerId()).serverid());
    genOrderReq.set_roleid(LoginLayer::mRoleId);
    
    string msgData;
    genOrderReq.SerializeToString(&msgData);
    
    request->setRequestData(msgData.c_str(), msgData.size());
    request->setTag("POST");
    CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
    CCHttpClient::getInstance()->send(request);
    request->release();
}

void RechargeUI::onHttpResponsePay(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag()))
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed())
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
    
    // 返回的协议
	GenOrderRsp* m_pGenOrderRsp = new GenOrderRsp();
	m_pGenOrderRsp->ParseFromString(strdata);
    
    // 返回的订单号
    long long tmpNum = m_pGenOrderRsp->ordernum();
    
    char charOrderNum[20];
    sprintf(charOrderNum, "%lld", tmpNum);
    
    NSString * nsOrderNum = [NSString stringWithUTF8String:charOrderNum];
    
    [ExtenalClass set_PayNum:tmpNum];
    
    //    // 返回的商品名称
    //    std::string strGoodsName = m_pGenOrderRsp->commodity();
    //    NSString * NsGoodsName = [NSString stringWithUTF8String:strGoodsName.c_str()];
    //
    //    // 返回的商品价钱
    //    int nGoodsPrice = m_pGenOrderRsp->price();
    //
    //    char charGoodsPrice[20];
    //    sprintf(charGoodsPrice, "%d", nGoodsPrice);
    
    
    // 返回的商品价钱
    //int nGoodsPrice = m_pGenOrderRsp->price();
    int nGoodsPrice = [ExtenalClass get_Charge];
    
    char charInputGoodsPrice[20];
    sprintf(charInputGoodsPrice, "%d", nGoodsPrice);
    NSString * nsGoodsPrice = [NSString stringWithUTF8String:charInputGoodsPrice];
    
    char charGoodsPrice[20];
    sprintf(charGoodsPrice, "%d", nGoodsPrice * 100);
    
    
    // 返回的商品名称
    //std::string strGoodsName = m_pGenOrderRsp->commodity();
    //NSString * NsGoodsName = [NSString stringWithUTF8String:strGoodsName.c_str()];
    std::string strGoodsName = "";
    strGoodsName.append(charGoodsPrice);
    strGoodsName.append("元宝");
    NSString * nsGoodsName = [NSString stringWithUTF8String:strGoodsName.c_str()];
    
    if (0 == tmpNum)
    {
        // 提示信息
        NSString * strPayErrorTip = @"订单请求失败，请重试或联系客服";
        
        const char * charPayErrorTip = [strPayErrorTip UTF8String];
        
        GameView::getInstance()->showAlertDialog(charPayErrorTip);
        
        return;
    }
    
    
    // 支付方法
    //[[WmComPlatform defaultPlatform] WmUniPay:[[ExtenalClass sharedIntance] getBuyInfoFromUI:1:@"春哥"]];
    //[[WmComPlatform defaultPlatform] WmUniPay:[[ExtenalClass sharedIntance] getBuyInfo:nGoodsPrice:NsGoodsName]];
    
    
    /**
     显示支付
     dealseq：订单号
     fee：金额
     game：http://payquery.bppstore.com上面对应ID
     gamesvr：多个通告地址的选择设置，
     md5Key：http://payquery.bppstore.com该网址对应的密匙
     appScheme：支付宝快捷支付对应的回调应用名称，要与targets-》info-》url types中的 url schemes中设置的对应
     userid:账户名，单机游戏必须传入值
     **/
    // 单位是元
    //在此处调用支付接口
    [[KYSDK instance]showPayWith:nsOrderNum fee:nsGoodsPrice game:@"4956" gamesvr:@"" subject:nsGoodsName md5Key:@"PGZu02dLFfTTcpif6xNqRqCDAHhL3dJS"  userId:@""];
}
