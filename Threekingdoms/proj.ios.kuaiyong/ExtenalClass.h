//
//  ExtenalClass.h(For 91SDK)
//  Threekingdoms
//
//  Created by 刘亮 on 14-4-30.
//
//

#import <Foundation/Foundation.h>
#import "KYMobilePay.h"
#import "KYSDK.h"

@interface ExtenalClass : UIView<KYSDKDelegate>

+(ExtenalClass*) sharedIntance;
+(id) allocWithZone:(struct _NSZone *)zone;
-(id) copyWithZone:(NSZone *)zone;
-(id) retain;
-(unsigned) retainCount;
-(oneway void) release;
-(id) autorelease;
-(id) init;

// 显示报错信息
+(void)messageBox:(NSString*)stringTip;

//-(void)KYLoginResult:(NSNotification *)notify;                      // 登录结果通知
//-(void)KYLogoutPlatform:(NSNotification *)notify;                    // 注销账号通知




// 静态变量
+(NSString *) s_guestAccount;                                       // 记录游客账号
+(NSString *) s_nsMacStr;                                           // 设备号
+(int) s_nMachineType;                                              // 设备类型（1：ios越狱，2：安卓国内；3：ios官方简体；4：ios官方繁体）

// 静态方法
+(long long) get_PayNum;                                            // get订单号
+(void) set_PayNum:(long long)tmpPayNum;                            // set订单号

+(NSString *) get_macStr;                                           // get设备号
+(void) set_macStr:(NSString *)nsMac;                               // set设备号

+(int) get_MachineType;                                             // get设备类型
+(void) set_MachineType:(int)nMachineType;                          // set设备类型

+(int) get_Charge;                                                  // get所付金额
+(void) set_Charge:(int)nCharge;                                    // set所付金额

@end
