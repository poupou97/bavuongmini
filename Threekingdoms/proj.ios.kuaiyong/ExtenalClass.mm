//
//  ExtenalClass.m
//  Threekingdoms
//
//  Created by 刘亮 on 14-4-30.
//
//

#import "ExtenalClass.h"
#import "SetUI.h"
#import "GoldStoreUI.h"

#import "../Classes/login_state/Login.h"
#import "../Classes/login_state/LoginState.h"
#import "../Classes/messageclient/GameMessageProcessor.h"

#include "../Classes/GameView.h"


static ExtenalClass *sharedObj = nil;

static NSString *s_guestAccount = nil;

static long long s_payNum = 0;

static NSString *s_nsMacStr = nil;

static int s_nMachineType = 0;

static int s_nCharge = 0;

@implementation ExtenalClass

+(ExtenalClass*) sharedIntance
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            [[self alloc] init];
        }
    }
    return sharedObj;
}

+(id) allocWithZone:(struct _NSZone *)zone
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            sharedObj = [super allocWithZone:zone];
            return sharedObj;
        }
    }
    
    return nil;
}

-(id) copyWithZone:(NSZone *)zone
{
    return self;
}

-(id) retain
{
    return self;
}

-(unsigned) retainCount
{
    return UINT_MAX;
}

-(oneway void) release
{
    
}

-(id) autorelease
{
    return self;
}

-(id) init
{
    @synchronized(self)
    {
        [super init];
        return self;
    }
}

+(long long) get_PayNum
{
    return s_payNum;
}

+(void) set_PayNum:(long long)tmpPayNum
{
    s_payNum = tmpPayNum;
}

+(int) get_Charge
{
    return s_nCharge;
}

+(void) set_Charge:(int)nCharge
{
    s_nCharge = nCharge;
}

+(NSString *) get_macStr
{
    return s_nsMacStr;
}

+(void) set_macStr:(NSString *)nsMac
{
    s_nsMacStr = nsMac;
}

+(int) get_MachineType
{
    return s_nMachineType;
}

+(void) set_MachineType:(int)nMachineType
{
    s_nMachineType = nMachineType;
}

///////////////////////////////////////////////

+(void)messageBox:(NSString*)stringTip
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:stringTip
													message:nil
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"确定", nil];
	[alert show];
	[alert release];
}

// 登录后回调方法(SDK接口已定义接口)
-(void)loginCallBack:(NSString *)tokenKey
{
    NSLog(@"log  log log!!!");
    
    // 7659_SDK
    const char * charToKenKey = [tokenKey UTF8String];
    
    const char * charAccount = "";
    
    Login::getInstance()->getLoginInfo(charAccount, charToKenKey);
    //Login::getInstance()->getLoginToKenKey(charToKenKey);
}

-(void)quickLogCallBack:(NSString *)tokenKey
{
    NSLog(@"log  log log!!!");
    
    // 7659_SDK
    const char * charToKenKey = [tokenKey UTF8String];
    
    const char * charAccount = "";
    
    Login::getInstance()->getLoginInfo(charAccount, charToKenKey);
}

// 游戏账号登录后回调，返回为@“suc”时，代表正确，其他为错误信息。并显示在登录界面(SDK已定义此接口)
-(void)gameLoginCallback:(NSString *)username password:(NSString *)password
{
    NSLog(@"name  %@  pwd  %@",username,password);
    
}

// 点击注销按钮贺调用注销方法后的回调方法
-(void) logOutCallBack:(NSString *)guid
{
    cocos2d::CCUserDefault::sharedUserDefault()->flush();
    GameMessageProcessor::sharedMsgProcessor()->sendReq(1002);
}



@end

