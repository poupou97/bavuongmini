#include "LoadResState.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "../messageclient/reqsender/ReqSenderProtocol.h"
#include "../login_state/Login.h"
#include "AppMacros.h"
#include "../logo_state/LogoState.h"
#include "../GameUserDefault.h"
#include "../GameView.h"
#include "../utils/StaticDataManager.h"
#include "../legend_engine/CCLegendAnimation.h"
#include <stdlib.h>

#if CC_TARGET_PLATFORM!=CC_PLATFORM_WIN32
#include <stdarg.h>  
#include <sys/stat.h>  
#else
#include <direct.h>  
#include <io.h>  
#endif  

#if CC_TARGET_PLATFORM!=CC_PLATFORM_WIN32
#define ACCESS access
//#define MKDIR(a) mkdir((a),0755)
#define MKDIR(a) mkdir((a),S_IRWXU | S_IRWXG | S_IRWXO)
#else
#define ACCESS _access  
#define MKDIR(a) _mkdir((a))  
#endif  

#define KEY_OF_VERSION   "current-version-code"
#define IS_RESSTATE_NOTLOAD 0
#define WAIT_TIME 1000
#define EXCEPTIONAL_WAIT_TIME 60*1000

#define BACKGROUND_ROLL_TIME 100.0f
#define BACKGROUND_TAG 123
#define PANEL_TAG 55
#define LABEL_TAG 56
#define FRAME_LABEL_TAG 57
#define RECONNECT_NUM 3
#define MENGBAN_TAG 60


enum {
	kStateStart = 0,
	kStateWait = 1,
	kStateIdle = 2,
	kStateLoad = 3,
	kStateExceptional = 4,
	kStateReconnect = 5,
	kStateConnectFail = 6,
};

LoadResLayer::LoadResLayer()
:m_iState(kStateStart)
,downloadUrl("")
,m_llAllResSize(0)
,m_hasDownloadSize(0)
,_percent(0)
,m_iReconnectNum(0)
{
	 StringDataManager::load();
	 // login server ip
	 Login::s_loginserver_ip = CCUserDefault::sharedUserDefault()->getStringForKey("login_server_ip", Login::s_loginserver_ip);
	 CCUserDefault::sharedUserDefault()->setStringForKey("login_server_ip", Login::s_loginserver_ip);

	m_isResLoadComplete = false;

	onVersionCheck();

    setTouchEnabled(true);

    CCSize s = CCDirector::sharedDirector()->getVisibleSize();
    
    CCLOG("s = %f * %f", s.width, s.height);

	CCSprite* pSprite = CCSprite::create("images/denglu.jpg");
    float scaleValue;
#if defined(CC_TARGET_OS_IPHONE)
    if(s.width == 827)
    {
        pSprite = CCSprite::create("Default-Landscape~ipad.png");
        scaleValue = (float)s.width/(float)1024;
        //pSprite->setRotation(90.0f);
    }
    else if(s.width == 852)
    {
        pSprite = CCSprite::create("Default-568h@2x.png");
        scaleValue = (float)s.width/(float)1136;
        pSprite->setRotation(90.0f);
    }
    else if(s.width == 800)
    {
        pSprite = CCSprite::create("Default@2x.png");
        scaleValue = (float)s.width/(float)960;
        pSprite->setRotation(90.0f);
    }
    else
    {
        pSprite = CCSprite::create("Default.png");
        scaleValue = (float)s.width/(float)480;
        pSprite->setRotation(90.0f);
    }
    pSprite->setPosition(ccp(s.width/2, s.height/2));
    pSprite->setAnchorPoint(ccp(0.5f, 0.5f));
#else
    pSprite = CCSprite::create("images/denglu.jpg");
    if(((float)s.width/(float)1136) > ((float)s.height/(float)640))
    {
        scaleValue = (float)s.width/(float)1136;
    }
    else
    {
        scaleValue = (float)s.height/(float)640;
    }
    pSprite->setPosition(ccp(0, 0));
    pSprite->setAnchorPoint(ccp(0, 0));
#endif
    pSprite->setScale(scaleValue);
	addChild(pSprite, 0);

	layer= UILayer::create();

	//layer->setPosition(ccp(s.width/2,30));
	layer->setPosition(ccp(s.width/2,s.height/2));
	addChild(layer);
	
	float posY = s.height * 0.3f;
	UIImageView *mengban_check = UIImageView::create();
	mengban_check->setTexture("res_ui/zhezhao80_new.png");
	mengban_check->setScale9Enable(true);
	mengban_check->setScale9Size(ccp(s.width, 100));
	mengban_check->setAnchorPoint(ccp(0.5f,0.5f));
	mengban_check->setPosition(ccp(0,-posY));
	mengban_check->setOpacity(130);
	mengban_check->setTag(MENGBAN_TAG);
	layer->addWidget(mengban_check);

	CCLabelTTF* pLabel = CCLabelTTF::create(StringDataManager::getString("CheckVersion"), APP_FONT_NAME, 25);
	pLabel->setPosition(ccp(0, -posY));
	pLabel->setTag(LABEL_TAG);
    layer->addChild(pLabel, 1);
	
	// Create the slider
    UISlider* slider = UISlider::create();

	slider->setName("slider");
    //slider->setTouchEnabled(true);
    slider->loadBarTexture("");
    slider->loadSlidBallTextures("res_ui/LOADING/light.png", "res_ui/LOADING/light.png", "");
    slider->loadProgressBarTexture("res_ui/progress_bar.png");
	slider->setScale9Enabled(true);
	slider->setSize(CCSizeMake(s.width, 40));
    slider->setPosition(ccp(0,-(posY+50)));
	slider->setPercent(0);
	slider->setVisible(false);
    layer->addWidget(slider);

	UIPanel * panel_ =UIPanel::create();
	panel_->setAnchorPoint(ccp(0.5f,0.5f));
	panel_->setPosition(ccp(0,0));
	panel_->setVisible(false);
	panel_->setTag(PANEL_TAG);
	layer->addWidget(panel_);

	UIImageView * imageBackGround = UIImageView::create();
	imageBackGround->setTexture("res_ui/zhezhao80.png");
	imageBackGround->setScale9Enable(true);
	imageBackGround->setScale9Size(CCSizeMake(120,70));
	imageBackGround->setAnchorPoint(ccp(0.5f,0.5f));
	imageBackGround->setPosition(ccp(0,0));
	panel_->addChild(imageBackGround);

	UIImageView * imageMainFrame = UIImageView::create();
	imageMainFrame->setTexture("res_ui/LV2_dikuang2.png");
	imageMainFrame->setScale9Size(CCSizeMake(120,60));
	imageMainFrame->setScale9Enable(true);
	imageMainFrame->setCapInsets(CCRect(14,14,1,1));
	imageMainFrame->setAnchorPoint(ccp(0.5f,0.5f));
	imageMainFrame->setPosition(ccp(0,0));
	panel_->addChild(imageMainFrame);

	UIImageView * imageOutFrame = UIImageView::create();
	imageOutFrame->setTexture("res_ui/bian_1.png");
	imageOutFrame->setScale9Enable(true);
	imageOutFrame->setScale9Size(CCSizeMake(130,69));
	imageOutFrame->setAnchorPoint(ccp(0.5f,0.5f));
	imageOutFrame->setPosition(ccp(0,0));
	panel_->addChild(imageOutFrame);
	
	UIButton * buttonDownload= UIButton::create();
	buttonDownload->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	buttonDownload->setTouchEnable(true);
	buttonDownload->setPressedActionEnabled(true);
	buttonDownload->addReleaseEvent(this,coco_releaseselector(LoadResLayer::callbackDownload));
	buttonDownload->setAnchorPoint(ccp(0.5f,0.5f));
	buttonDownload->setScale9Enable(true);
	buttonDownload->setScale9Size(CCSizeMake(100,43));
	buttonDownload->setCapInsets(CCRect(18,9,2,23));
	buttonDownload->setPosition(ccp(0,0));
	panel_->addChild(buttonDownload);

	UILabel * btnLabel_=UILabel::create();
	btnLabel_->setText(StringDataManager::getString("EnterDownload"));
	btnLabel_->setAnchorPoint(ccp(0.5f,0.5f));
	btnLabel_->setPosition(ccp(0,0));
	btnLabel_->setFontSize(18);
	buttonDownload->addChild(btnLabel_);

	UIImageView *mengban = UIImageView::create();
	mengban->setTexture("res_ui/zhezhao80_new.png");
	mengban->setScale9Enable(true);
	mengban->setScale9Size(ccp(s.width, 100));
	mengban->setAnchorPoint(ccp(0.5f,0.5f));
	mengban->setPosition(ccp(0,0));
	mengban->setOpacity(130);
	mengban->setVisible(false);
	mengban->setTag(FRAME_LABEL_TAG);
	layer->addWidget(mengban);

	pProgressLabel=CCLabelTTF::create(StringDataManager::getString("ExceptionalExit"), APP_FONT_NAME, 30);
	pProgressLabel->setPosition(ccp(0,0));
	ccColor3B shadowColor = ccc3(0,0,0);   // black
	pProgressLabel->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	pProgressLabel->setVisible(false);
	layer->addChild(pProgressLabel);
	
	CCLegendAnimation* pBird = CCLegendAnimation::create("animation/texiao/jiemiantexiao/bird/fly.anm");
	pBird->setPlayLoop(true);
	pBird->setReleaseWhenStop(false);
	pBird->setAnchorPoint(ccp(0.5f,0.5f));
	pBird->setPosition(ccp(s.width/2 - 80, -(posY)));
	pBird->setScale(0.67f);
	layer->addChild(pBird);

	this->scheduleUpdate();
}

void LoadResLayer::callbackDownload(CCObject* pSender)
{
	CCApplication::sharedApplication()->openURL(downloadUrl.c_str());
}

void LoadResLayer::repeatFunc()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCSprite* pSprite = (CCSprite*)getChildByTag(BACKGROUND_TAG);
	float scaleValue = pSprite->getScale();
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;

	CCMoveTo * moveToActionNext = CCMoveTo::create(BACKGROUND_ROLL_TIME*(widthMultiple+1)/widthMultiple,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturnNext = CCMoveTo::create(0,ccp(winSize.width-1, 0));
	CCRepeatForever * repeapActionNext = CCRepeatForever::create(CCSequence::create(moveToActionNext,moveToReturnNext,CCDelayTime::create(BACKGROUND_ROLL_TIME*(widthMultiple-1)/widthMultiple),NULL));
	pSprite->runAction(repeapActionNext);
}

AssetsManager* LoadResLayer::getAssetsManager(int index)
{
    static AssetsManager *pAssetsManager = NULL;
    
    //if (! pAssetsManager)
    {
		//m_strResUrl += "/";
		string addr = m_strResUrl + m_vecResFile.at(index).resfilepath();

		std::string filePath = m_vecResFile.at(index).resfilepath();
		filePath = filePath.substr(10, filePath.size());
		createDownloadedDir(filePath.c_str());

		pAssetsManager = new AssetsManager(addr.c_str(),
											m_vecResFile.at(index).resversion(),
                                           pathToSave.c_str());
        pAssetsManager->setDelegate(this);
        pAssetsManager->setConnectionTimeout(3);
    }
    
    return pAssetsManager;
}

void LoadResLayer::createDownloadedDir(const char* filePath)
{
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
	pathToSave = CCFileUtils::sharedFileUtils()->fullPathForFilename(filePath);
	//pathToSave = CCFileUtils::sharedFileUtils()->getWritablePath()+filePath;
#else 
	pathToSave = CCFileUtils::sharedFileUtils()->getWritablePath()+filePath;
#endif

	int lastSlashIndex = pathToSave.find_last_of("/\\");
	std::string pathdir;
	if(string::npos != lastSlashIndex)
	{
		pathdir = pathToSave.substr(0, lastSlashIndex);
	}
	else
	{
		pathdir = "";
	}

	//pathToSave = "E:\\SVN\\client\\cocos2d-x-2.2.0\\projects\\Threekingdoms\\";
	//pathToSave += "resource";
    /*
    // Create the folder if it doesn't exist
#if (CC_TARGET_PLATFORM != CC_PLATFORM_WIN32)
    DIR *pDir = NULL;
    
    pDir = opendir (pathToSave.c_str());
    if (! pDir)
    {
        mkdir(pathToSave.c_str(), S_IRWXU | S_IRWXG | S_IRWXO);
    }
#else
	if ((GetFileAttributesA(pathToSave.c_str())) == INVALID_FILE_ATTRIBUTES)
	{
		CreateDirectoryA(pathToSave.c_str(), 0);
	}
#endif
	*/
    // Create the folder if it doesn't exist, it can make recursive dir
	CreateDir(pathdir.c_str());
}

int LoadResLayer::CreateDir(const char *pDir)
{  
    int i = 0;
    int iRet;
    int iLen;
    char* pszDir;
    
    if(NULL == pDir)
    {
        return 0;
    }
    pszDir = strdup(pDir);
    iLen = strlen(pszDir);
    
    for (i = 0;i < iLen;i ++)
    {
        if (pszDir[i] == '\\' || pszDir[i] == '/')
        {
            pszDir[i] = '\0';
            
            iRet = ACCESS(pszDir,0);
            if (iRet != 0)
            {
                iRet = MKDIR(pszDir);
                if (iRet != 0)
                {
                    //CCLOG("CreateDir failed, path name: %s", pszDir);
                }
            }
            
            pszDir[i] = '/';
        }
    }
    
    iRet = ACCESS(pszDir,0);
    if(iRet != 0)
    {
        iRet = MKDIR(pszDir);
        if (iRet != 0)
        {
            CCLOG("CreateDir failed, path name: %s", pszDir);
        }
    }
    free(pszDir);
    return iRet;
}  

void LoadResLayer::onVersionCheck()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        //std::string url = Login::s_loginserver_ip;
		std::string url = "139.99.49.200";
        url.append(":48688/versioncheck");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(LoadResLayer::onVersionCheckCompleted));

		VersionCheckReq httpReq;
		httpReq.set_clienttype("");
		httpReq.set_clientversion(GAME_CODE_VERSION);
		CCLOG(GAME_CODE_VERSION);
		int resVersion = CCUserDefault::sharedUserDefault()->getIntegerForKey(KEY_OF_RES_VERSION, GAME_RES_VERSION);
		httpReq.set_resversion(resVersion);
		httpReq.set_channelid(0);
		httpReq.set_batchid(0);
		httpReq.set_recommendid(0);
		httpReq.set_extensionfield("");
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
		httpReq.set_platform("laoh");
#elif defined(CC_TARGET_OS_IPHONE)
		httpReq.set_platform(OPERATION_PLATFORM);
#endif

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void LoadResLayer::onVersionCheckCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
		m_iState = kStateExceptional;
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
		m_iState = kStateReconnect;

        return;
    }
    
	VersionCheckRsp versionCheckRsp;
	if(response->getResponseData()->size() <= 0)
	{
		versionCheckRsp.set_result(1);   // server no response, so the default value is 1, indicate that there is no update
	}
	else
	{
		//CCAssert(response->getResponseData()->size() > 0, "should not be empty");
		versionCheckRsp.ParseFromString(response->getResponseData()->data());

		CCLOG("response.result = %d", versionCheckRsp.result());
		CCLOG("response.clienturl = %s", versionCheckRsp.clienturl().c_str());
		CCLOG("response.resurl = %s", versionCheckRsp.resurl().c_str());
		CCLOG("response.filelist = %d", versionCheckRsp.filelist().size());
		CCLOG("response.extensionfield = %s", versionCheckRsp.extensionfield().c_str());
	}

	int result = versionCheckRsp.result();
#if IS_RESSTATE_NOTLOAD
	if(result != 1)
	{
		result = 1;
	}
#endif

	if(1 == result)
	{
		m_iState = kStateWait;
	}
	else if(2 == result)
	{
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("PleaseDownloadNewClient"));
#elif defined(CHANNEL_LAOHU_1)
		downloadUrl = versionCheckRsp.clienturl();
		UIPanel* panel =(UIPanel*)(layer->getWidgetByTag(PANEL_TAG));
		panel->setVisible(true);
		CCLabelTTF* label =(CCLabelTTF*)(layer->getChildByTag(LABEL_TAG));
		UIImageView * mengban = (UIImageView *)layer->getWidgetByTag(MENGBAN_TAG);
		mengban->setVisible(false);
		label->setVisible(false);
#elif defined(CHANNEL_DANGLE_1)
		downloadUrl = versionCheckRsp.clienturl();
		UIPanel* panel =(UIPanel*)(layer->getWidgetByTag(PANEL_TAG));
		panel->setVisible(true);
		CCLabelTTF* label =(CCLabelTTF*)(layer->getChildByTag(LABEL_TAG));
		UIImageView * mengban = (UIImageView *)layer->getWidgetByTag(MENGBAN_TAG);
		mengban->setVisible(false);
		label->setVisible(false);
#else
		m_iState = kStateWait;
#endif
	}
	else if(3 == result)
	{
		m_iState = kStateLoad;
		UISlider* slider =(UISlider*)(layer->getWidgetByName("slider"));
		slider->setVisible(true);
		//set "do not back up" attribute by:liutao  20140619
#if CC_TARGET_PLATFORM==CC_PLATFORM_IOS
		CCFileUtils::sharedFileUtils()->addSkipBackupAttributeToPath(CCFileUtils::sharedFileUtils()->getWritablePath());
#endif

		m_strResUrl = versionCheckRsp.resurl();
		m_vecResFile.clear();
		m_iSizeResFile = versionCheckRsp.filelist().size();

		for(int index = 0; index < m_iSizeResFile; index++)
		{
			m_vecResFile.push_back(versionCheckRsp.filelist(m_iSizeResFile-index-1));
			m_llAllResSize += versionCheckRsp.filelist(m_iSizeResFile-index-1).ressize();
		}

		//pthread_t *_tid = new pthread_t();
		pthread_t _tid;
		pthread_create(&_tid, NULL, thread_load, this);
		//pthread_join(*_tid, NULL);
		//delete _tid;
	}
}

void* LoadResLayer::thread_load(void* arg)
{
	LoadResLayer* self = static_cast<LoadResLayer*>(arg);
	self->loadAllRes();

	return NULL;
}

void LoadResLayer::loadAllRes()
{
	for(int index = 0; index < m_iSizeResFile; index++)
	{
		getAssetsManager(index)->update();

		onSuccess(index);
	}
}

void LoadResLayer::onError(AssetsManager::ErrorCode errorCode)
{
    if (errorCode == AssetsManager::kNoNewVersion)
    {
        pProgressLabel->setString("no new version");
    }
    
    if (errorCode == AssetsManager::kNetwork)
    {
        pProgressLabel->setString("network error");
    }
}

void LoadResLayer::onProgress(int percent)
{
	UISlider* slider =(UISlider*)(layer->getWidgetByName("slider"));
	slider->setPercent(percent);
}

void LoadResLayer::onSuccess(int index)
{
	m_hasDownloadSize += m_vecResFile.at(index).ressize();

	float num = (float)m_hasDownloadSize/m_llAllResSize;
	_percent = (int)(num*100);

	UISlider* slider =(UISlider*)(layer->getWidgetByName("slider"));
	slider->setPercent(_percent);
	
	if(index == m_iSizeResFile-1)
	{
		m_isResLoadComplete = true;
	}
}

void LoadResLayer::updateLabel()
{
	CCLabelTTF* label = (CCLabelTTF*)(layer->getChildByTag(LABEL_TAG));
	std::string string;

	char aucRefresh[40];
	memset(aucRefresh, 0, sizeof(aucRefresh));
	int mb, kb;
	mb = 1024*1024;
	kb = 1024;
		
	string.append(StringDataManager::getString("HasDownload"));
	if(m_llAllResSize >= mb)
	{
		if(m_hasDownloadSize >= mb)
		{
			sprintf(aucRefresh, "%d%%, %.2f M / %.2f M", _percent, (float)m_hasDownloadSize/mb, (float)m_llAllResSize/mb);
		}
		else if(m_hasDownloadSize < mb && m_hasDownloadSize >= kb)
		{
			sprintf(aucRefresh, "%d%%, %.2f K / %.2f M", _percent, (float)m_hasDownloadSize/kb, (float)m_llAllResSize/mb);
		}
		else
		{
			sprintf(aucRefresh, "%d%%, %d B / %.2f M", _percent, m_hasDownloadSize, (float)m_llAllResSize/mb);
		}
	}
	else if(m_llAllResSize < mb && m_llAllResSize >= kb)
	{
		if(m_hasDownloadSize >= kb)
		{
			sprintf(aucRefresh, "%d%%, %.2f K / %.2f K", _percent, (float)m_hasDownloadSize/kb, (float)m_llAllResSize/kb);
		}
		else
		{
			sprintf(aucRefresh, "%d%%, %d B / %.2f K", _percent, m_hasDownloadSize, (float)m_llAllResSize/kb);
		}
	}
	else
	{
		sprintf(aucRefresh, "%d%%, %d B / %d B", _percent, m_hasDownloadSize, m_llAllResSize);
	}
	string.append(aucRefresh);
	label->setString(string.c_str());
}

void LoadResLayer::update(float dt)
{
	switch(m_iState)
	{
	case kStateStart:
		struct timeval m_sStartTime;
		gettimeofday(&m_sStartTime, NULL);
		m_dStartTime = m_sStartTime.tv_sec*1000 + m_sStartTime.tv_usec/1000;

		m_iState = kStateIdle;
		break;
	case kStateWait:
		struct timeval m_sEndedTime;
		gettimeofday(&m_sEndedTime, NULL);
		m_dEndedTime = m_sEndedTime.tv_sec*1000 + m_sEndedTime.tv_usec/1000;
		//wait 1 second to convert scene
		if(m_dEndedTime - m_dStartTime >= WAIT_TIME)
		{
			GameState* pScene = new LogoState();
			if (pScene)
			{
				pScene->runThisState();
				pScene->release();
			}
		}
		break;
	case kStateIdle:
		//I am a lazy state zzzzz
		break;
	case kStateLoad:
		updateLabel();
		if(m_isResLoadComplete)
		{
			GameState* pScene = new LogoState();
			if (pScene)
			{
				pScene->runThisState();
				pScene->release();
			}
		}
		break;
	case kStateExceptional:
		struct timeval m_sExceptionalEndedTime;
		gettimeofday(&m_sExceptionalEndedTime, NULL);
		m_dEndedTime = m_sExceptionalEndedTime.tv_sec*1000 + m_sExceptionalEndedTime.tv_usec/1000;
		//wait 60 second to convert scene
		if(m_dEndedTime - m_dStartTime >= EXCEPTIONAL_WAIT_TIME)
		{
			UIImageView* frame = (UIImageView*)layer->getWidgetByTag(FRAME_LABEL_TAG);
			frame->setVisible(true);
			pProgressLabel->setVisible(true);

			CCLabelTTF* label =(CCLabelTTF*)(layer->getChildByTag(LABEL_TAG));
			UIImageView * mengban = (UIImageView *)layer->getWidgetByTag(MENGBAN_TAG);
			mengban->setVisible(false);
			label->setVisible(false);

			m_iState = kStateIdle;
		}
		break;
	case kStateReconnect:
		++m_iReconnectNum;
		CCLOG("m_iReconnectNum = %d", m_iReconnectNum);
		if(m_iReconnectNum >= RECONNECT_NUM)
		{
			CCLOG("reconnect server failed!");
			m_iState = kStateConnectFail;
		}
		else
		{
			onVersionCheck();
			m_iState = kStateIdle;
		}
		break;
	case kStateConnectFail:
		{
			UIImageView* frame = (UIImageView*)layer->getWidgetByTag(FRAME_LABEL_TAG);
			frame->setVisible(true);

			CCLabelTTF* pLabel = CCLabelTTF::create(StringDataManager::getString("ServerError"), APP_FONT_NAME, 30);
			pLabel->setPosition(ccp(0,0));
			ccColor3B shadowColor = ccc3(0,0,0);   // black
			pLabel->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
			layer->addChild(pLabel);

			CCLabelTTF* label =(CCLabelTTF*)(layer->getChildByTag(LABEL_TAG));
			UIImageView * mengban = (UIImageView *)layer->getWidgetByTag(MENGBAN_TAG);
			mengban->setVisible(false);
			label->setVisible(false);

			m_iState = kStateIdle;
		}
		break;
	default:
		break;
	}
	
}

////////////////////////////////////////////////////////////////////////////

LoadResState::LoadResState()
{
}

void LoadResState::runThisState()
{
    /*CCLayer* pLayer = new LoadResLayer();
    addChild(pLayer);

    CCDirector::sharedDirector()->replaceScene(this);
    pLayer->release();
	*/
	CCLayer* pLayer = new LoadResLayer();
    addChild(pLayer, 0, 1);

	if(CCDirector::sharedDirector()->getRunningScene() != NULL)
		CCDirector::sharedDirector()->replaceScene(this);
	else
		CCDirector::sharedDirector()->runWithScene(this);
    pLayer->release();
}
