
#ifndef _ROOT_GAMEVIEW_H_
#define _ROOT_GAMEVIEW_H_

#include "cocos2d.h"
#include "messageclient/ClientNetEngine.h"
#include "ui/extensions/PopupWindow.h"

USING_NS_CC;

class GameActor;
class MyPlayer;
class GameSceneLayer;
class MainScene;
class MainAnimationScene;
class CMapInfo;
class PackageScene;
class MissionManager;
class PacPageView;
class FolderInfo;
class CEquipment;
class MissionInfo;
class CActiveRole;
class Counter;
class GameFightSkill;
class ModelMessage;
class CRelationPlayer;
class CShortCut;
class CBaseSkill;
class ChatCell;
class CMapTeam;
class CTeamMember;
class CAroundPlayer;
class CAuctionInfo;
class CActionDetail;
class CGeneralBaseMsg;
class CGeneralDetail;
class CSalableCommodity;
class CGuildRecord;
class CFivePersonInstanceEndInfo;
class CLable;
class CMailInfo;
class StoreHousePageView;
class COneVipGift;
class COfflineExpPuf;
class PrivateCell;
class CPlayerGetPhypower;
class CRewardBase;
class CStrengthenEquipment;class GameView : public ClientNetEngine::Delegate
{
public:
	// all states used in this game
	// one state is an instance of class GameState which is from CCScene
	enum {
		STATE_LOGO = 0,
		STATE_LOGIN,
		STATE_ROLE_SELECT,
		STATE_ROLE_CREATE,
		STATE_LOAD_GAME,
		STATE_GAME,
	};

	enum GameViewTag {
		// enough big!!! this alert dialog tag must be biggest in CCDirector::getRunningScene()
		kTagAlertDialogBaseId = 9999,    
	};

public:
	GameView();
	virtual ~GameView();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

    /** returns a shared instance of the GameView */
    static GameView* getInstance();

	void init();

	/**
	* check if MyPlayer or not
	* @return
	*/
	bool isOwn(long long roleId);
	bool isOwn(GameActor* a);

	CMapInfo* getMapInfo();

	GameSceneLayer* getGameScene();
	MainScene* getMainUIScene();
	MainAnimationScene * getMainAnimationScene();

	void showPopupWindow(std::string string,int buttonNum,CCObject* pSender,SEL_CallFuncO pSelectorYes,SEL_CallFuncO pSelectorNo,PopupWindow::PopWindowType winType = PopupWindow::KtypeNeverRemove, int remainTime = 0);
	void showAlertDialog(std::string string);

	void showCounter(CCObject * pSender,SEL_CallFuncO pSelector,int defaultNum =0);

	void ResetData();

	ccColor3B getGoodsColorByQuality(int goodsQuality); 
	ccColor3B getGeneralsColorByQuality(int generalsQuality); 

	// show reconnect button 
	void showReconnectButton();
	void callBackReconnect(cocos2d::CCObject *sender);
private:
	int m_playerGoldNUm;
	int m_playerGoldIngotNUm;
	int m_playerBindGoldIngotNUm;
	ccColor3B m_goodsColor;
	ccColor3B m_generalsColor;

public:
	void setPlayerGold(int _value);
	void setPlayerGoldIngot(int _value);
	void setPlayerBindGoldIngot(int _value);

	int getPlayerGold();
	int getPlayerGoldIngot();
	int getPlayerBindGoldIngot();

	bool m_checkRemind;
	void setIsCheckRemind(bool _value);
	bool getIsCheckRemind();

	long long m_flowerParticleTime;
	void setFlowerParticleTime(long long time_);
	long long getFlowerParticleTime();
	
	int m_flowerNum;
	void setFlowerNum(int time_);
	int getFlowerNum();
public:
	MyPlayer* myplayer;
	MissionManager* missionManager;
	// backpack instance
	PacPageView * pacPageView;

	//storeHousePageView
	StoreHousePageView * storeHousePageView;
	
	// all packs
	std::vector<FolderInfo*> AllPacItem;
	std::vector<FolderInfo*> AllEquipItem;
	std::vector<FolderInfo*> AllGoodsItem;
	//current equipment
	std::vector<CEquipment *> EquipListItem;

	int selectFolderId;

	//CBaseSkillList
	std::vector<CBaseSkill *>CBaseSkillList;
	//GameFightSkillList
	std::map<std::string,GameFightSkill*> GameFightSkillList;

	/// ChatUI
	std::vector<ChatCell*> chatCell_Vdata;
	std::vector<ChatCell *> selectCell_vdata;
	bool hasPrivatePlaer;
	bool isShowPrivateChatBtn;
	std::vector<PrivateCell *> chatPrivateContentVector; 
	std::vector<CRelationPlayer *>chatPrivateSpeakerVector;
	std::vector<PrivateCell *> chatPrivateContentSourceVector;
	std::vector<CRelationPlayer *>chatPrivateNewMessageInfo;
	//// Friend
	std::vector<CRelationPlayer *>relationSourceVector;
	std::vector<CRelationPlayer *>relationFriendVector;
	std::vector<CRelationPlayer *>relationBlackVector;
	std::vector<CRelationPlayer *>relationEnemyVector;
	std::vector<CRelationPlayer *>relationTempVector;
	//friend phypower
	std::vector<CPlayerGetPhypower *>playerPhypowervector;
	
	//team
	std::vector<CMapTeam *>mapteamVector;
	std::vector<CAroundPlayer *>aroundPlayerVector;
	std::vector<CTeamMember *>teamMemberVector;
	////consign
	std::vector<CAuctionInfo *>auctionConsignVector;
	std::vector<CAuctionInfo *>auctionSourceVector;
	std::vector<CAuctionInfo *>selfConsignVector;
	////////equipment
	std::vector<CStrengthenEquipment *>equipMentBody;
	std::vector<CStrengthenEquipment *>equipMentBackpack;

	//family record
	std::vector<CGuildRecord *>guildRecordVector;
	//family applyPlayersize
	int applyPlayersize;

	//equip compair vector,different role or general
	std::vector<CEquipment *>compairEquipVector;

	// NPC in one game scene
	std::vector<CActiveRole*> allNPCsVector;

	//shortcutList
	std::vector<CShortCut*> shortCutList;

	//武将列表
	std::vector<CGeneralBaseMsg *>generalBaseMsgList;
	//上阵武将列表
	std::vector<CGeneralBaseMsg *>generalsInLineList;
	//上阵武将详细信息
	std::vector<CGeneralDetail *>generalsInLineDetailList;
	//武将技能列表
	std::map<std::string,GameFightSkill*> GeneralsGameFightSkillList;

	//招贤馆信息
	std::vector<CActionDetail*> actionDetailList;

	// 仓库信息
	std::vector<FolderInfo * > storeHouseItemList;

	// NPC出售的商品信息
	std::vector<CSalableCommodity * > shopItemList;
	// 商品回购列表
	std::vector<CSalableCommodity * > buyBackList;

	// 元宝商城信息
	std::vector<CLable *> goldStoreList;
	// 绑定元宝商城信息
	std::vector<CLable *> bingGoldStoreList;

	//mail
	//std::vector<CMailInfo *> mailVectorOfRemind;
	std::vector<CMailInfo *> mailVectorOfSystem;
	std::vector<CMailInfo *> mailVectorOfPlayer;
	//general
	std::vector<int> generalIndexvector;
	//remind vector : use remind list
	std::vector<int > remindvector;
	//reward vector: use reward list
	std::vector<CRewardBase * > rewardvector;

	//正在出战的武将（有顺序）
	std::map<int,CGeneralBaseMsg*> m_aliveGeneralBaseList;

	//龙魂天赋
	std::map<int,int> m_musouTalentList;

	//每日vip奖励
	std::vector<COneVipGift*>m_vipEveryDayRewardsList;
	//竞技场可挑战
	int m_remainTimes;
	void setRemainTimesOfArena(int times);
	int getRemainTimesOfArena();
	//离线经验
	std::vector<COfflineExpPuf*>m_offLineExpPuf;
	int m_nRequiredGoldToGetExp;
	int m_nRequiredGoldInGotToGetExp;

	//技能快捷栏的状态
	bool isShortCutLayerHidden;
	//registerNotice
	std::vector<std::string> registerNoticeVector;
	bool registerNoticeIsShow;
	//repairEquipment remind
	std::vector<long long > repairEquipInstanceIdVector;
	bool isShowRepairEquip;

	// when socket closed, show the disconnection tip or not
	bool showSocketCloseInfo;

	// when socket closed, reconnect GameServer;
	bool reconnectGameServer;
private:
	CMapInfo* m_pMapInfo;
	int m_nAlertDialogCreatedId;
} ;

#endif
