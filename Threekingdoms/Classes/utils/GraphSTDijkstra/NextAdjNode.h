#ifndef _GAMEUTILS_GRAPH_NEXTADJNODE_H_
#define _GAMEUTILS_GRAPH_NEXTADJNODE_H_

/**
 * 邻接结点
 */
class NextAdjNode {
public:
	/**
	 * 邻接结点的索引
	 */
	int nodeNum;
	/**
	 * 结点与其邻接结点之间的边的权值
	 */
	int edgeWeight;
	
	/**
	 * 结点所在邻接链表中的下一个邻接结点及其信息
	 */
	NextAdjNode* nextNode;

	/**
	 * 
	 * @param node
	 * 邻接结点索引
	 * @param weight
	 * 邻接结点与表头结点之间的边的权值
	 */
	NextAdjNode(int node, int weight);

	int getNodeNum();

	void setNodeNum(int nodeNum);

	int getEdgeWeight();

	void setEdgeWeight(int edgeWeight);

	NextAdjNode* getNextNode();

	void setNextNode(NextAdjNode* nextNode);
};

#endif