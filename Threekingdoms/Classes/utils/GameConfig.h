#ifndef _UTILS_GAMECONFIG_H
#define _UTILS_GAMECONFIG_H

/*
 * this class managers the game config
 * @Author, zhaogang
 * @Date, 2014/5/22
 */

#include "cocos2d.h"
#include <map>

using namespace cocos2d;

class GameConfig
{
public:
	static void load();
	static const char* getStringForKey(const char* pKey);
	static bool getBoolForKey(const char* pKey);
	static int getIntForKey(const char* pKey);

private:
	static CCDictionary *s_configData;
};

#endif
