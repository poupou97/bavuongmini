#include "GameConfig.h"

////////////////////////////////////////////////////////////

CCDictionary* GameConfig::s_configData = NULL;

void GameConfig::load()
{
	s_configData = CCDictionary::createWithContentsOfFile("game_config.xml");
	s_configData->retain();
}

const char* GameConfig::getStringForKey(const char* pKey)
{
	const char *str  = ((CCString*)s_configData->objectForKey(pKey))->m_sString.c_str();
	return str;
}

bool GameConfig::getBoolForKey(const char* pKey)
{
	const char *str  = ((CCString*)s_configData->objectForKey(pKey))->m_sString.c_str();

	bool ret = false;
	if (str)
	{
		ret = (! strcmp(str, "true"));
	}

	return ret;
}

int GameConfig::getIntForKey( const char* pKey )
{
	const char *str  = ((CCString*)s_configData->objectForKey(pKey))->m_sString.c_str();

	int ret = atoi(str);

	return ret;
}
