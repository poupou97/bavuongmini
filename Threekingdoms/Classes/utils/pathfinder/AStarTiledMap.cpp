/*
 *  AStarTiledMap.cpp
 *
 *  Created by Zhao Gang on 07-31-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#include "AStarTiledMap.h"
#include "cocos2d.h"
	
AStarTiledMap::AStarTiledMap() {
	this->horizontalTilesNum = 0;
	this->verticalTilesNum = 0;
	this->mapData = NULL;
}
	
AStarTiledMap::AStarTiledMap(int hNum, int vNum) {
	this->horizontalTilesNum = hNum;
	this->verticalTilesNum = vNum;
	// new 2d array
	this->mapData = new char*[vNum];
	for(int i = 0; i < vNum; i++) {
		this->mapData[i] = new char[hNum];
	}

	clear();
}

AStarTiledMap::~AStarTiledMap() {
	// delete 2d array
	for(int i = 0; i < verticalTilesNum; i++)   
		CC_SAFE_DELETE_ARRAY(mapData[i]);  
	CC_SAFE_DELETE_ARRAY(mapData);
}
	
void AStarTiledMap::clear() {
	for (int y = 0; y < verticalTilesNum; ++y) {
		for (int x = 0; x < horizontalTilesNum; ++x) {
			mapData[y][x] = ASTARTILEDMAP_PATH;
		}
	}
}

void AStarTiledMap::setData(char** data, int hNum, int vNum) {
	mapData = data;
	horizontalTilesNum = hNum;
	verticalTilesNum = vNum;
}
	
char** AStarTiledMap::getData() {
	return mapData;
}

int AStarTiledMap::getHorizontalTilesNum() {
	return horizontalTilesNum;
}

int AStarTiledMap::getVerticalTilesNum() {
	return verticalTilesNum;
}
	
char AStarTiledMap::get(int x, int y) {
	return mapData[y][x];
}
	
void AStarTiledMap::set(int x, int y, int v) {
	mapData[y][x] = v;
}

bool AStarTiledMap::isBarrier(int x, int y) {
	return get(x, y) != ASTARTILEDMAP_PATH;
}
	
bool AStarTiledMap::isPath(int x, int y) {
	return get(x, y) == ASTARTILEDMAP_PATH;
}
	
void AStarTiledMap::toggleBarrier(int x, int y) {
	set(x, y, isBarrier(x, y) ? ASTARTILEDMAP_PATH : ASTARTILEDMAP_BARRIER);
}
	
void AStarTiledMap::setToBarrierNode(int x, int y) {
	set(x, y, ASTARTILEDMAP_BARRIER);
}