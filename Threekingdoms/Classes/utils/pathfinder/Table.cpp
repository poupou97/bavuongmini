/*
 *  Table.cpp
 *
 *  Created by Zhao Gang on 07-31-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#include "Table.h"

#include "AStarNode.h"
	
Table::Table() {
	map = CCDictionary::create();
	map->retain();
}

Table::~Table() {
	CC_SAFE_RELEASE(map);
}

AStarNode* Table::get(int x, int y) {
	return (AStarNode*)map->objectForKey(this->getKey(x, y));
}
	
void Table::put(int x, int y, AStarNode* node) {
	map->setObject(node, getKey(x, y));
}
	
AStarNode* Table::remove(int x, int y) {
	AStarNode* node = (AStarNode*)map->objectForKey(this->getKey(x, y));
	node->retain();
	map->removeObjectForKey(this->getKey(x, y));
	return (AStarNode*)node->autorelease();
}
	
void Table::clear() {
	map->removeAllObjects();
}
	
//Collection<AStarNode> getAllNodes() {
//	return map.values();
//}
CCDictionary* Table::getMap() {
	return map;
}
	
bool Table::contains(int x, int y) {
	if(map->objectForKey(getKey(x, y)) == NULL) {
        return false;
    } else {
        return true;
    }
}
	
int Table::size() {
	return map->count();
}
	
long Table::getKey(short x, short y) {
	long key = x; 
	return key << 16 | y;
}