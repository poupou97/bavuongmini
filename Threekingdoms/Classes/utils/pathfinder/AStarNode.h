/*
 *  AStarNode.h
 *
 *  Created by Zhao Gang on 07-31-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#ifndef PATHFINDER_ASTARNODE_H
#define PATHFINDER_ASTARNODE_H

#include "cocos2d.h"
#include "Comparable.h"

USING_NS_CC;

class Table;

class AStarNode : public CCObject, public Comparable<AStarNode*> {
public:
	static AStarNode* getNode(int x, int y);
	static void clearAllNodes();
	static AStarNode* getNode(short* posi);
	static AStarNode* newNode(int x, int y);

	AStarNode();
	virtual ~AStarNode();

	AStarNode(int x, int y);
	AStarNode(short* posi);
	
	void clear();
	
	short* toShortArray();

	int getX();
	void setX(int x);
	int getY();
	void setY(int y);

	AStarNode* getFather();

	void setFather(AStarNode* father);

	void init(AStarNode* target);

	/**
	 * 计算H
	 * 
	 * @param source
	 * @param target
	 * @return
	 */
	int heuristicCostEstimate(AStarNode* source, AStarNode* target);

	virtual int compareTo(AStarNode* o);

	virtual bool isEqual(const CCObject* pObject);

	//@Override
	//public String toString() {
	//	return new StringBuilder().append(getX()).append(',').append(getY()).toString();
	//}

	int calculatorF(AStarNode* father, AStarNode* target);

	int getDistinctG(AStarNode* father);

	int getG();

	/**
	 * 是否比指定的点更好
	 * 
	 * @param node
	 * @return
	 */
	bool isBetter(AStarNode* node);

	bool isGBetter(AStarNode* node);

	bool isFBetter(AStarNode* node);

	int getF();

private:
	short x, y;
	short* position;
	
	int g; // 从起点source移动到当前点的耗费
	int h; // 从当前点到终点的估值耗费
	int f; // f = g + h
	AStarNode* father; // 父结点
};

#endif