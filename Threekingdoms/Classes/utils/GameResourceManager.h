
#ifndef _UTILS_GAMERESOURCEMANAGER_H
#define _UTILS_GAMERESOURCEMANAGER_H

#include "cocos2d.h"

USING_NS_CC;

/*
 * this class manager all resources in this game
 * this class will include some file wrap, for instance, copyfile, delete file, create directory etc.
 * Created by zhao gang at 12-6-2013. 
 *
 */

class GameResourceManager
{
public:
	GameResourceManager();
	~GameResourceManager();

	void copyFile(const char* pFileName);

	void prepareStaticData();
};

#endif