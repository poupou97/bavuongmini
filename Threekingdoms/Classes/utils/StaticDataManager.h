#ifndef _UTILS_STATICDATAMANAGER_H
#define _UTILS_STATICDATAMANAGER_H

/*
 * this class managers the static data used in this game
 * these data is from sqlite db
 * @Author, zhaogang
 * @Date, 2013/12/7
 */

#include "cocos2d.h"
#include <map>



using namespace cocos2d;

class CBaseSkill;
class CFightSkill;
class SkillBin;
class ExStatus;
class CGeneralBaseMsg;
class CGeneralDetail;
class CGeneralsEvolution;
class CGeneralsTeach;
class CFivePersonInstance;
class CFightWayBase;
class CFightWayGrowingUp;
class CFateBaseMsg;
class CDrug;
class MapRobotDrug;
class CFunctionOpenLevel;
class MapStrngthStone;
class CMonsterBaseInfo;
class CMusouTalent;
class MapEquipAddExp;
class MapStarPickAmount;
class MapStrengthGem;
class MapMarquee;
class MapRobotPotionLimit;
class MapRefineDiffAddValue;
class CAdditionProperty;
class MapRefineValueFactor;
class MapWeapEffect;
class CNewFunctionRemind;
class CVipInfo;
class CCollectInfo;
class CRechargeInfo;
class CGeneralTeachAddedProperty;
class CPowerSendInfo;
class CSysthesisInfo;
class MapStrengthUseStone;
class CSingCopyClazz;
class CSingCopyLevel;
class CGemPropertyMap;
class CRewardBase;
class CPropInfo;

class StaticDataBaseSkill
{
public:
	static void load(const char* configFileName);

	static std::map<std::string, CBaseSkill*> s_baseSkillData;
};

class StaticDataFightSkill
{
public:
	static void load(const char* configFileName);

	static std::map<std::string, std::map<int, CFightSkill*>*> s_data;
};

class StaticDataMonsterFightSkill
{
public:
	static void load(const char* configFileName);

	static std::map<std::string, CFightSkill*> s_data;
};

class StaticDataSkillBin
{
public:
	static void load(const char* configFileName);

	static std::map<std::string, SkillBin*> s_data;
};

class ExStatusConfigData
{
public:
	static void load(const char* configFileName);

	static std::map<int,ExStatus *> s_exstatusData;
};

class GeneralsConfigData
{
public:
	static void load(const char* configFileName);

	static std::map<int ,CGeneralBaseMsg *> s_generalsBaseMsgData;

};

class GeneralsDialogData
{
public:
	static void load(const char* configFileName);

	static std::map<int, std::map<int, std::vector<std::string>*>*> s_generalsDialogData;

	// general dialog config
	static int s_dialog_stay_duration;
	static int s_dialog_interval_duration;
	static float s_chance_gossip;
	static float s_chance_attack;
	static float s_chance_lowhp;
	static int s_lowhp_percent;
};

class GeneralEvolutionConfigData
{
public:

	struct ProAndRare{
		int profession;
		int rare;
		int evolution;

		bool operator <(const ProAndRare& other) const
		{
			if (profession < other.profession)        //类型按profession升序排序
			{
				return true;
			}
			else if (profession == other.profession)  //如果profession相同，按rare升序排序
			{
				if (rare < other.rare)      
				{
					return true;
				}
				else if (rare == other.rare)//如果rare相同，按level升序排序
				{
					return evolution < other.evolution;
				}
			}

			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<ProAndRare,CGeneralsEvolution*> s_generalEvolution;

};

class GeneralTeachConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<int,CGeneralsTeach*> s_generalTeach;

};

class FivePersonInstanceConfigData
{
public:
	static void load(const char* configFileName);
	//static std::map<int,CFivePersonInstance*> s_fivePersonInstance;
	static std::vector<CFivePersonInstance*> s_fivePersonInstance;
};

class FightWayConfigData
{
public:
	static void load(const char* configFileName);
	//static std::map<int,CFivePersonInstance*> s_fivePersonInstance;
	static std::vector<CFightWayBase*> s_fightWayBase;
};

class FightWayGrowingUpConfigData
{
public:
	static void load(const char* configFileName);

	static std::map<int, std::map<int, CFightWayGrowingUp*>*> s_fightWayGrowingUp;
};


class FateBaseMsgConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<std::string,CFateBaseMsg*> s_fateBaseMsg;

};

class CDrugMsgConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<std::string,CDrug*> s_drugBaseMsg;

};

class StringDataManager
{
public:
	static void load();
	static const char* getString(const char* stringId);

private:
	static CCDictionary *s_stringsData;
};

class RobotDrugConfigData
{
public:
	//map<int type,map<int level ,MapRobotDrug*>*>
	static void load(const char* configFileName);
	static std::map<int, std::map<int,MapRobotDrug*>*> s_RobotDrugConfig;

};

//t_map_property	*	get map_name by map_id	*	by:liutao	140211
class StaticDataMapName
{
public:
	static void load(const char* configFileName);

	static std::map<std::string, std::string> s_mapname;
};

class StaticDataMonsterBaseInfo
{
public:
	static void load(const char* configFileName);

	static std::map<int, CMonsterBaseInfo*> s_monsterBase;
};

/*读取精炼材料所增加的经验*/
///精炼石增加的经验
class StrengthStoneConfigData
{
public:
	
	static void load(const char* configFileName);
	static std::map<std::string,MapStrngthStone*> s_StrengthStrone;
};

/////////////////////功能入口/////////////////////////
class FunctionOpenLevelConfigData
{
public:
	static void load(const char* configFileName);
	static std::vector<CFunctionOpenLevel*> s_functionOpenLevelMsg;

};

/////////////////////快捷栏开放等级/////////////////////////
class ShortcutSlotOpenLevelConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<int,int> s_shortcutSlotOpenLevelMsg;

};

/////////////////////龙魂天赋/////////////////////////
class MusouTalentConfigData
{
public:

	struct IdAndLevel{
		int idx;
		int level;

		bool operator <(const IdAndLevel& other) const
		{
			if (idx < other.idx)        //类型按id升序排序
			{
				return true;
			}
			else if (idx == other.idx)  //如果id相同，按level升序排序
			{
				if (level < other.level)      
				{
					return true;
				}
			}

			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<IdAndLevel,CMusouTalent*> s_musouTalentMsg;
};

/////////////////////加载界面文本/////////////////////////
class LoadingTextData
{
public:
	static void load(const char* configFileName);
	static std::vector<std::string> s_LoadingTexts;

};

///装备升级所需要的经验
class StrengthEquipmentConfigData
{
public:

	static void load(const char* configFileName);
	static std::map<int ,MapEquipAddExp*> s_EquipUpLevelExp;
};

//摘星
class StrengthStarConfigData
{
public:

	static void load(const char* configFileName);
	static std::map<int ,MapStarPickAmount*> s_EquipStarAmount;
};
////宝石
class StrengthGemConfigData
{
public:

	static void load(const char* configFileName);
	static std::map<std::string ,MapStrengthGem*> s_EquipSGemAmount;
};
////跑马灯
class MarqueeStringConfigData
{
public:

	static void load(const char* configFileName);
	static std::map<int ,MapMarquee*> s_MarqueeStrings;
};
//////////
class RobotPotionMilitData
{
public:

	static void load(const char* configFileName);
	static std::map<std::string ,MapRobotPotionLimit*> s_RobotPotionMilit;
};
/////equip refine addValue baseValue

class EquipRefineAddValueConfigData
{
	enum valueType
	{
		weapon = 1,//武器
		dress =2 ,//铠甲
		cap =3,//头盔
		shoes =8,//鞋子
		necklace=5,//项链
		belt = 7,//腰带
	};
public:
	//clazz level get deff addValue
	static void load(const char* configFileName);
	static std::map<int, std::map<int,MapRefineDiffAddValue*>*> s_EquipRefineValue;

	static std::vector<CAdditionProperty*> getAddvalueByDiffClazzAndLevel(int clazz,int level);
	
};
///////equip refine addValue factor
class EquipRefineFactorConfigData
{
public:

	struct useLevelAndQuality{
		int useLevel;
		int quality;

		bool operator <(const useLevelAndQuality& other) const
		{
			if (useLevel < other.useLevel)        //类型按uselevel升序排序
			{
				return true;
			}
			else if (useLevel == other.useLevel)  //如果level相同，按quality升序排序
			{
				if (quality < other.quality)      
				{
					return true;
				}
			}

			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<useLevelAndQuality, MapRefineValueFactor*> s_EquipRefineValueFactor;
};


///////equip refine effect cfg
class EquipRefineEffectConfigData
{
public:
	
	struct weapEffect{
		int weapPression;
		int refineLevel;
		
		bool operator <(const weapEffect& other) const
		{
			if (weapPression < other.weapPression)
			{
				return true;
			}
			else if (weapPression == other.weapPression)  
			{
				if (refineLevel < other.refineLevel)
				{
					return true;
				}
			}
			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<weapEffect,std::string> s_EquipmentRefineEffect;
};

///////equip star effect cfg
class EquipStarEffectConfigData
{
public:
	
	struct weapEffect{
		int weapPression;
		int starLevel;
		
		bool operator <(const weapEffect& other) const
		{
			if (weapPression < other.weapPression)
			{
				return true;
			}
			else if (weapPression == other.weapPression)  
			{
				if (starLevel < other.starLevel)
				{
					return true;
				}
			}
			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<weapEffect,std::string> s_EquipmentStarEffect;
};


/////////////////////新功能提醒/////////////////////////
class NewFunctionRemindConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<int, std::vector<CNewFunctionRemind*> > s_newFunctionRemindList;
};

/////////////////////系统描述/////////////////////////
class SystemInfoConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<int, std::string> s_systemInfoList;
};

/////////////////////按钮开启等级/////////////////////////
class BtnOpenLevelConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<int, int> s_btnOpenLevel;
};

/////////////////////vip表/////////////////////////
class VipConfigData
{
public:

	struct TypeIdAndVipLevel{
		std::string typeName;
		int vipLevel;

		bool operator <(const TypeIdAndVipLevel& other) const
		{
			if (typeName < other.typeName)        //类型按id升序排序
			{
				return true;
			}
			else if (typeName == other.typeName)  //如果id相同，按level升序排序
			{
				if (vipLevel < other.vipLevel)      
				{
					return true;
				}
			}

			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<TypeIdAndVipLevel,CVipInfo*> s_vipConfig;

};

/////////////////////vip描述信息表/////////////////////////
class VipDescriptionConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int, std::string> s_vipDes;
};

/////////////////////采集物信息表/////////////////////////
class CollectInfoConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int, CCollectInfo*> s_collectInfo;
};

/////////////////////配置信息表/////////////////////////
class VipValueConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int, std::string> s_vipValue;
};


/////////////////////VIP价格表/////////////////////////
class VipPriceConfig
{
public:
	struct priceValueAndType{
		int priceValue;
		int priceType;

		bool operator <(const priceValueAndType& other) const
		{
			if (priceValue < other.priceValue)        //类型按id升序排序
			{
				return true;
			}
			else if (priceType == other.priceType)  //如果id相同，按level升序排序
			{
				if (priceType < other.priceType)      
				{
					return true;
				}
			}

			return false;
		}
	};
	static void load(const char* configFileName);
	static std::map<int, priceValueAndType*> s_vipPriceValue;
};

/////////////////////充值界面信息/////////////////////////
class RechargeConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int, CRechargeInfo*> s_rechargeValue;
};

/////////////////////////武将传功属性变化表////////////////////////////////

class GeneralTeachAddedPropertyConfigData
{
public:

	struct TeachAddedKey{
		int profession;
		int star;
		int quality;

		bool operator <(const TeachAddedKey& other) const
		{
			if (profession < other.profession)        //类型按profession升序排序
			{
				return true;
			}
			else if (profession == other.profession)  //如果profession相同，按star升序排序
			{
				if (star < other.star)      
				{
					return true;
				}
				else if (star == other.star)//如果star相同，按quality升序排序
				{
					return quality < other.quality;
				}
			}

			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<TeachAddedKey,CGeneralTeachAddedProperty*> s_generalTeachAdded;

};

/////////////////////配置信息表/////////////////////////
class PowerSendConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int, CPowerSendInfo*> s_map_powerSendConfig;
};
/////////////////////合成系统道具信息/////////////////////////
class SysthesisItemConfig
{
public:
	static void load(const char* configFileName);
	static std::vector<CSysthesisInfo*> s_systhesisInfo;
};

///////strength stone level
class EquipStrengthStoneLevelConfigData
{
public:

	struct typeAndLevle{
		int type_;
		int level_;

		bool operator <(const typeAndLevle& other) const
		{
			if (type_ < other.type_)
			{
				return true;
			}
			else if (type_ == other.type_)
			{
				if (level_ < other.level_)      
				{
					return true;
				}
			}
			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<typeAndLevle, MapStrengthUseStone*> s_EquipStrengthUseStone;
};

/////////////////////Monster dialog/////////////////////////
class MonsterDialogData
{
public:
	static void load(const char* configFileName);
	static std::vector<std::string> s_monsterDialogData;
};
//////////单人副本 关卡
class SingleCopyClazzConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int ,CSingCopyClazz *> s_SingCopyClazzMsgData;
};

class SingleCopyLevelConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int ,CSingCopyLevel *> s_SingCopyLevelMsgData;
};
/////get cur gem property by gemId
class gempropertyConfig
{
public:
	static void load(const char* configFileName);
	static std::map<std::string ,CGemPropertyMap *> s_GemPropertyMsgData;
};
// get cur reward des by index
class RewardListConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int ,CRewardBase *> s_RewardListMsgData;
};

//get function_npc's icon
class NpcData
{
public:
	static void load(const char* configFileName);

	static std::map<int, std::string> s_npcFunctionIcon;
};
//get propInfo in t_prop
class StaticPropInfoConfig
{
public:
	static void load(const char* configFileName);

	static std::map<std::string, CPropInfo*> s_propInfoData;
};

#endif

