/*
*  Joystick.cpp
*  YaoGan
*
*  Created by Liu Yanghui on 11-10-27.
*  Copyright 2011 ard8. All rights reserved.
*
*/

#include "Joystick.h"
#include "GameView.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../gamescene_state/role/BaseFighterConstant.h"

#define JOYSTICK_BG_TAG 100

Joystick::Joystick()
: active(false)
{
}

void Joystick::updatePos(float delta){
	jsSprite->setPosition(ccpAdd(jsSprite->getPosition(),ccpMult(ccpSub(currentPoint, jsSprite->getPosition()),0.5)));
}

void Joystick::Active()
{
	if (!active) {
		active = true;
		schedule(schedule_selector(Joystick::updatePos));//添加刷新函数

		CCDirector* pDirector = CCDirector::sharedDirector();
		pDirector->getTouchDispatcher()->addTargetedDelegate(this, 0, true);
	}else {

	}
}
//冻结摇杆
void   Joystick::Inactive()
{
	if (active) {
		active=false;
		currentPoint = centerPoint;
		CCNode* bg = this->getChildByTag(JOYSTICK_BG_TAG);
		if(bg != NULL)
			bg->setVisible(false);

		this->unschedule(schedule_selector(Joystick::updatePos));//删除刷新

		CCDirector* pDirector = CCDirector::sharedDirector();
		pDirector->getTouchDispatcher()->removeDelegate(this);//删除委托
	}else {

	}
}

bool Joystick::isActive()
{
	return active;
}

bool Joystick::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
	if (!active)
		return false;

	CCPoint touchPoint = touch->getLocation();
	if (ccpDistance(touchPoint, centerPoint) > radius)
		return false;
	currentPoint = touchPoint;

	CCNode* bg = this->getChildByTag(JOYSTICK_BG_TAG);
	if(bg != NULL)
		bg->setVisible(true);

	// 去掉 “自动战斗”的动画（如果有的话）
	MyPlayer* pMyPlayer = GameView::getInstance()->myplayer;
	CCNode* pNode = pMyPlayer->getChildByTag(PLAYER_AUTO_FIGHT_ANIMATION_TAG);
	if (NULL != pNode)
	{
		pMyPlayer->removeAutoCombatFlag();
	}

	return true;
}

void  Joystick::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
	CCPoint touchPoint = touch->getLocation();
	if (ccpDistance(touchPoint, centerPoint) > radius)
	{
		currentPoint =ccpAdd(centerPoint,ccpMult(ccpNormalize(ccpSub(touchPoint, centerPoint)), radius));
	}else {
		currentPoint = touchPoint;
	}
}

void  Joystick::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	currentPoint = centerPoint;

	CCNode* bg = this->getChildByTag(JOYSTICK_BG_TAG);
	if(bg != NULL)
		bg->setVisible(false);

	
}

//获取摇杆方位,注意是单位向量
CCPoint Joystick::getDirection()
{
	CCPoint curPoint = ccpSub(currentPoint, centerPoint);
	if(curPoint.x == 0 && curPoint.y == 0)
		return curPoint;
	else
		return ccpNormalize(ccpSub(currentPoint, centerPoint));
}

//获取摇杆力度
float Joystick::getVelocity()
{
	return ccpDistance(centerPoint, currentPoint);
}

Joystick* Joystick:: JoystickWithCenter(CCPoint aPoint ,float aRadius ,CCSprite* aJsSprite,CCSprite* aJsBg){
	Joystick *jstick = new Joystick();
	jstick->autorelease();
	jstick->initWithCenter(aPoint,aRadius,aJsSprite,aJsBg);
	return jstick;
}

Joystick* Joystick::initWithCenter(CCPoint aPoint ,float aRadius ,CCSprite* aJsSprite,CCSprite* aJsBg){
	active = false;
	radius = aRadius;
	centerPoint = aPoint;
	currentPoint = centerPoint;
	jsSprite = aJsSprite;
	jsSprite->setPosition(centerPoint);
	this->addChild(jsSprite, 1);
	if(aJsBg != NULL)
	{
		aJsBg->setVisible(false);   // default, the bg is not visible
		aJsBg->setTag(JOYSTICK_BG_TAG);
		aJsBg->setPosition(centerPoint);
		this->addChild(aJsBg, 0);
	}
	
	return this;
}
