#ifndef _NEWCOMMERSTORY_NEWCOMMERSTORYMANAGER_H_
#define _NEWCOMMERSTORY_NEWCOMMERSTORYMANAGER_H_

#include "cocos2d.h"
USING_NS_CC;

class NewCommerStory;

class NewCommerStoryManager
{
public:
	enum StoryState 
	{
		ss_notStart = 0,
		ss_running,
		ss_ended
	};
	NewCommerStoryManager();
	~NewCommerStoryManager();

	static NewCommerStoryManager * s_instance;
	static NewCommerStoryManager* getInstance();

	void initData();

	inline NewCommerStory* getCurStory(){return m_pCurStory;};

	inline void setIsNewComer(bool _value){m_bIsNewComer = _value;};
	inline bool IsNewComer(){return m_bIsNewComer;};

	inline void setIsSelectCountryFinished(bool _value){m_bIsSelectCountryFinished = _value;};
	inline bool IsSelectCountryFinished(){return m_bIsSelectCountryFinished;};

	inline void setIsReqForUseMusouSkill(bool _value){m_bIsReqForUseMusouSkill = _value;};
	inline bool IsReqForUseMusouSkill(){return m_bIsReqForUseMusouSkill;};

	std::string getBestWeaponByProfession(int profession);

	void startAutoGuideToCallGeneral();
	void endAutoGuideToCallGeneral();

	/**
	 * 更新
	 */
	void update();

	int m_storyState ;
	void startStory();
	void endStory();

	void clear();

	/**********1.正常状态  2.新手剧情状态***********/
	void setMainSceneState(int state);
	void setMyPlayerEffect(int state);
public:
	//在新手教学中死亡的怪物ID列表
	std::map<long long,int> m_pDiedEmptyIdList;

private:
	/** 所有命令数据 */
	std::vector<NewCommerStory*> mAllStoryCommandList;
	NewCommerStory * m_pCurStory;

	bool m_bIsNewComer;
	bool m_bIsSelectCountryFinished;
	//是否发了请求使用龙魂技能
	bool m_bIsReqForUseMusouSkill;
};

#endif