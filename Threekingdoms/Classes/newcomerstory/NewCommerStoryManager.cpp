#include "NewCommerStoryManager.h"
#include "NewCommerStory.h"
#include "command/NSTextDesAnm.h"
#include "../GameView.h"
#include "../gamescene_state/GameSceneState.h"
#include "command/NSRunMovieScript.h"
#include "command/NSFight.h"
#include "command/NSMove.h"
#include "../gamescene_state/MainScene.h"
#include "../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../gamescene_state/role/ActorUtils.h"
#include "../gamescene_state/role/BasePlayer.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../messageclient/GameMessageProcessor.h"
#include "command/NSSelectCountry.h"
#include "../gamescene_state/sceneelement/TriangleButton.h"
#include "../messageclient/element/CGeneralBaseMsg.h"
#include "../messageclient/element/CGeneralDetail.h"
#include "../gamescene_state/sceneelement/GeneralsInfoUI.h"
#include "../gamescene_state/role/MyPlayerAIConfig.h"
#include "../gamescene_state/role/MyPlayerAI.h"

NewCommerStoryManager* NewCommerStoryManager::s_instance = NULL;

NewCommerStoryManager::NewCommerStoryManager():
m_bIsNewComer(false),
m_bIsSelectCountryFinished(false),
m_bIsReqForUseMusouSkill(false)
{
	m_storyState = ss_notStart;
	m_pCurStory = new NewCommerStory();
}


NewCommerStoryManager::~NewCommerStoryManager()
{
	delete m_pCurStory;
}

NewCommerStoryManager* NewCommerStoryManager::getInstance() {
	if (s_instance == NULL) {
		s_instance = new NewCommerStoryManager();
	}
	return s_instance;
}	

void NewCommerStoryManager::initData()
{
	//delete old data
	std::vector<NewCommerStory*>::iterator iter;
	for (unsigned int i = 0; i < mAllStoryCommandList.size(); i++) {
		iter = mAllStoryCommandList.begin() + i;
		NewCommerStory* ns = *iter;
		mAllStoryCommandList.erase(iter);
		i--;
		continue;
	}
	mAllStoryCommandList.clear();

	//add new data
	//文字
	NSTextDesAnm1 * textDesAnm = new NSTextDesAnm1();
	textDesAnm->init();
	mAllStoryCommandList.push_back(textDesAnm);
 	//播放剧情动画1——出现张角和第一波黄巾军
 	NSRunMovieScript * runMovieScript = new NSRunMovieScript();
 	runMovieScript->init("Newplayer1.sc");
 	mAllStoryCommandList.push_back(runMovieScript);
	// 玩家操作部分1——技能教学，杀第一波黄巾军
	std::map<long long,int >tempList1;
	tempList1.insert(make_pair(528,3));
	tempList1.insert(make_pair(529,3));
	tempList1.insert(make_pair(530,3));
	NSFight5 * fight1 = new NSFight5();
	fight1->init(tempList1);
	mAllStoryCommandList.push_back(fight1);
	//移动到楼梯下
	NSMoveToStairs * moveToStair  = new NSMoveToStairs();
	moveToStair->init();
	mAllStoryCommandList.push_back(moveToStair);
	//播放剧情动画6——出现第二波黄巾军
	runMovieScript = new NSRunMovieScript();
	runMovieScript->init("Newplayer2.sc");
	mAllStoryCommandList.push_back(runMovieScript);
	//玩家操作部分2——武将上阵。。。武将出战教学
	std::map<long long,int >tempList2;
	tempList2.insert(make_pair(531,3));
	tempList2.insert(make_pair(532,3));
	tempList2.insert(make_pair(533,3));
	NSFight6 * fight2 = new NSFight6();
	fight2->init(tempList2);
	mAllStoryCommandList.push_back(fight2);
	//播放剧情动画6——提示传送门
	runMovieScript = new NSRunMovieScript();
	runMovieScript->init("Newplayer3.sc");
	mAllStoryCommandList.push_back(runMovieScript);
	//玩家操作部分2——移动至传送门
	NSMove * move  = new NSMove();
	move->init();
	mAllStoryCommandList.push_back(move);
	//播放剧情动画1——英雄难过美人关
	runMovieScript = new NSRunMovieScript();
	runMovieScript->init("Newplayer4.sc");
	mAllStoryCommandList.push_back(runMovieScript);
	//玩家操作部分——全部武将上阵。。。武将出战第二个第三个教学
	std::map<long long,int >tempList3;
	tempList3.insert(make_pair(499,1));
	tempList3.insert(make_pair(534,6));
	tempList3.insert(make_pair(535,5));
	tempList3.insert(make_pair(536,5));
	tempList3.insert(make_pair(537,9));
	NSFight7 * fight3 = new NSFight7();
	fight3->init(tempList3);
	mAllStoryCommandList.push_back(fight3);
	//播放剧情动画1——最后一个剧情
	runMovieScript = new NSRunMovieScript();
	runMovieScript->init("Newplayer5.sc");
	mAllStoryCommandList.push_back(runMovieScript);
	//选择国家
	NSSelectCountry * selectCountry = new NSSelectCountry();
	selectCountry->init();
	mAllStoryCommandList.push_back(selectCountry);

// 	//播放剧情动画1——苍龙出场、与护法战斗
// 	NSRunMovieScript * runMovieScript = new NSRunMovieScript();
// 	runMovieScript->init("Newplayer1.sc");
// 	mAllStoryCommandList.push_back(runMovieScript);
// 	// 玩家操作部分1——移动、单体技能教学，杀前护法
// 	std::map<long long,int >tempList1;
// 	tempList1.insert(make_pair(500,1));
// 	NSFight1 * fight1 = new NSFight1();
// 	fight1->init(tempList1);
// 	mAllStoryCommandList.push_back(fight1);
// 	//剩余三个护法
// 	runMovieScript = new NSRunMovieScript();
// 	runMovieScript->init("Newplayer6.sc");
// 	mAllStoryCommandList.push_back(runMovieScript);
// 	// 玩家操作部分1——群体技能教学，杀剩余三个护法
// 	std::map<long long,int >tempList4;
// 	tempList4.insert(make_pair(501,1));
// 	tempList4.insert(make_pair(502,1));
// 	tempList4.insert(make_pair(503,1));
// 	NSFight4 * fight4 = new NSFight4();
// 	fight4->init(tempList4);
// 	mAllStoryCommandList.push_back(fight4);
// 	//播放剧情动画6——提示传送门
// 	runMovieScript = new NSRunMovieScript();
// 	runMovieScript->init("Newplayer2.sc");
// 	mAllStoryCommandList.push_back(runMovieScript);
// 	//玩家操作部分2——移动至传送门
// 	NSMove * move  = new NSMove();
// 	move->init();
// 	mAllStoryCommandList.push_back(move);
// 	//对话5——圣女娘娘出现，众人对话
// 	runMovieScript = new NSRunMovieScript();
// 	runMovieScript->init("Newplayer3.sc");
// 	mAllStoryCommandList.push_back(runMovieScript);
// 	//玩家操作部分3——与魔星、贴身侍女战斗
// 	std::map<long long,int >tempList2;
// 	tempList2.insert(make_pair(499,1));
// 	tempList2.insert(make_pair(504,2));
// 	tempList2.insert(make_pair(505,2));
// 	tempList2.insert(make_pair(506,2));
// 	tempList2.insert(make_pair(507,2));
// 	tempList2.insert(make_pair(508,2));
// 	tempList2.insert(make_pair(509,2));
// 	tempList2.insert(make_pair(510,2));
// 	tempList2.insert(make_pair(511,2));
// 	NSFight2 *fight2 = new NSFight2();
// 	fight2->init(tempList2);
// 	mAllStoryCommandList.push_back(fight2);
// 	//对话7——引入龙魂技能
// 	runMovieScript = new NSRunMovieScript();
// 	runMovieScript->init("Newplayer4.sc");
// 	mAllStoryCommandList.push_back(runMovieScript);
// 	//玩家操作部分4——施放龙魂技能
// 	std::map<long long,int >tempList3;
// 	tempList3.insert(make_pair(499,1));
// 	tempList3.insert(make_pair(504,5));
// 	tempList3.insert(make_pair(505,5));
// 	tempList3.insert(make_pair(506,5));
// 	tempList3.insert(make_pair(507,5));
// 	tempList3.insert(make_pair(508,2));
// 	tempList3.insert(make_pair(509,2));
// 	tempList3.insert(make_pair(510,2));
// 	tempList3.insert(make_pair(511,2));
// 	NSFight3 *fight3 = new NSFight3();
// 	fight3->init(tempList3);
// 	mAllStoryCommandList.push_back(fight3);
// 	//对话8——魔星受伤
// 	runMovieScript = new NSRunMovieScript();
// 	runMovieScript->init("Newplayer5.sc");
// 	mAllStoryCommandList.push_back(runMovieScript);
// 	//选择国家
// 	NSSelectCountry * selectCountry = new NSSelectCountry();
// 	selectCountry->init();
// 	mAllStoryCommandList.push_back(selectCountry);
}

void NewCommerStoryManager::update()
{
	if (m_storyState != ss_running)
		return;

    //更新执行Command
	std::vector<NewCommerStory*>::iterator iter;
	for (unsigned int i = 0; i < mAllStoryCommandList.size(); i++) {
		iter = mAllStoryCommandList.begin() + i;
		NewCommerStory* ns = *iter;
		if (ns == NULL) {
			mAllStoryCommandList.erase(iter);
			i--;
			continue;
		}
		if (ns->isEnd()) {
			// 指令已经结束，释放
			mAllStoryCommandList.erase(iter);
			i--;
			continue;
		}
		m_pCurStory = ns;
		ns->update();
		break;
	}

	if (mAllStoryCommandList.size() <= 0)
	{
		//endStory();
		m_storyState = ss_ended;
		// login in game
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5106);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1001);
	}
}

void NewCommerStoryManager::startStory()
{
	//把所有命令加入vector中
	initData();

	m_storyState = ss_running;
}

void NewCommerStoryManager::endStory()
{
	//select country
	//GameMessageProcessor::sharedMsgProcessor()->sendReq(5103, (void*)1);

// 	// login in game
// 	GameMessageProcessor::sharedMsgProcessor()->sendReq(5106);
// 	GameMessageProcessor::sharedMsgProcessor()->sendReq(1001);

	m_bIsNewComer = false;
	m_storyState = ss_ended;
	m_bIsSelectCountryFinished = false;
	m_bIsReqForUseMusouSkill = false;

	//把客户端存的武将信息置空
	std::vector<CGeneralBaseMsg*>::iterator iter_base;
	for (iter_base = GameView::getInstance()->generalsInLineList.begin(); iter_base != GameView::getInstance()->generalsInLineList.end(); ++iter_base)
	{
		delete *iter_base;
	}
	GameView::getInstance()->generalsInLineList.clear();

	std::vector<CGeneralDetail*>::iterator iter_detail;
	for (iter_detail = GameView::getInstance()->generalsInLineDetailList.begin(); iter_detail != GameView::getInstance()->generalsInLineDetailList.end(); ++iter_detail)
	{
		delete *iter_detail;
	}
	GameView::getInstance()->generalsInLineDetailList.clear();

	//set autoKill off
	MyPlayerAIConfig::setAutomaticSkill(0);
	MyPlayerAIConfig::enableRobot(false);
	GameView::getInstance()->myplayer->getMyPlayerAI()->stop();

	this->setMainSceneState(1);
}

void NewCommerStoryManager::setMainSceneState( int state )
{
	MainScene * mainScene = GameView::getInstance()->getMainUIScene();
	if (!mainScene)
		return;

	if(state == 1)
	{
		CCLayer * myHeadLayer = (CCLayer*)mainScene->getChildByTag(kTagMyHeadInfo);
		if (myHeadLayer)
		{
			myHeadLayer->setVisible(true);
		}

		CCLayer * headMenuLayer = (CCLayer*)mainScene->getChildByTag(kTagHeadMenu);
		if (headMenuLayer)
		{
			headMenuLayer->setVisible(true);
		}

		mainScene->Button_mapOpen->setVisible(true);
		CCLayer * guideMapLayer = (CCLayer*)mainScene->getChildByTag(kTagGuideMapUI);
		if (guideMapLayer)
		{
			guideMapLayer->setVisible(true);
		}

		mainScene->generalHeadLayer->setVisible(true);
		mainScene->targetInfoLayer->setVisible(true);
		mainScene->chatLayer->setVisible(true);
		mainScene->ExpLayer->setVisible(true);

		CCLayer * missionAndTeamLayer = (CCLayer*)mainScene->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeamLayer)
		{
			missionAndTeamLayer->setVisible(true);
		}

		ShortcutLayer * shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
		if (shortcutLayer)
		{
			shortcutLayer->setVisible(true);
			shortcutLayer->setMusouShortCutSlotPresent(true);
		}

		CCNode * pJoystick = (CCLayer*)mainScene->getChildByTag(kTagVirtualJoystick);
		if (pJoystick)
		{
			pJoystick->setVisible(true);
		}

		//右下角三角按钮
		TriangleButton * triangleBtn = (TriangleButton*)mainScene->getChildByTag(78);
		if (triangleBtn)
		{
			triangleBtn->setVisible(true);
		}
	}
	else if (state == 2)
	{
		CCLayer * myHeadLayer = (CCLayer*)mainScene->getChildByTag(kTagMyHeadInfo);
		if (myHeadLayer)
		{
			myHeadLayer->setVisible(false);
		}

		CCLayer * headMenuLayer = (CCLayer*)mainScene->getChildByTag(kTagHeadMenu);
		if (headMenuLayer)
		{
			headMenuLayer->setVisible(false);
		}

		mainScene->Button_mapOpen->setVisible(false);
		CCLayer * guideMapLayer = (CCLayer*)mainScene->getChildByTag(kTagGuideMapUI);
		if (guideMapLayer)
		{
			guideMapLayer->setVisible(false);
		}

		mainScene->generalHeadLayer->setVisible(false);
		mainScene->targetInfoLayer->setVisible(false);
		mainScene->chatLayer->setVisible(false);
		mainScene->ExpLayer->setVisible(false);

		CCLayer * missionAndTeamLayer = (CCLayer*)mainScene->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeamLayer)
		{
			missionAndTeamLayer->setVisible(false);
		}

		ShortcutLayer * shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
		if (shortcutLayer)
		{
			shortcutLayer->setVisible(false);
			shortcutLayer->setMusouShortCutSlotPresent(false);
		}

		CCNode * pJoystick = (CCLayer*)mainScene->getChildByTag(kTagVirtualJoystick);
		if (pJoystick)
		{
			pJoystick->setVisible(false);
		}

		//右下角三角按钮
		TriangleButton * triangleBtn = (TriangleButton*)mainScene->getChildByTag(78);
		if (triangleBtn)
		{
			triangleBtn->setVisible(false);
		}
	}
	else
	{

	}
}

void NewCommerStoryManager::setMyPlayerEffect( int state )
{
	if (!GameView::getInstance()->myplayer->getAnim())
		return;

	std::string pressionStr = GameView::getInstance()->myplayer->getActiveRole()->profession();
	int pression_ = BasePlayer::getProfessionIdxByName(pressionStr);

	if(state == 1)
	{
		BasePlayer::switchWeapon("", GameView::getInstance()->myplayer->getAnim(), pression_);
		GameView::getInstance()->myplayer->getActiveRole()->set_hand("");
		GameView::getInstance()->myplayer->initWeaponEffect();
	}
	else if (state == 2)
	{
		int refineLevel_ = 20;
		int starLevel_ = 20;

		BasePlayer::switchWeapon(this->getBestWeaponByProfession(pression_).c_str(), GameView::getInstance()->myplayer->getAnim(), pression_);
		GameView::getInstance()->myplayer->getActiveRole()->set_hand(this->getBestWeaponByProfession(pression_).c_str());

		std::string effectNameForRefine = ActorUtils::getWeapEffectByRefineLevel(pression_,refineLevel_);
		GameView::getInstance()->myplayer->addWeaponEffect(effectNameForRefine.c_str());

		std::string effectNameForStar = ActorUtils::getWeapEffectByStarLevel(pression_, starLevel_);
		GameView::getInstance()->myplayer->addWeaponEffect(effectNameForStar.c_str());
	}
	else
	{}
}

void NewCommerStoryManager::clear()
{
	this->m_storyState = NewCommerStoryManager::ss_notStart;
	m_bIsNewComer = false;
	m_bIsSelectCountryFinished = false;
	m_bIsReqForUseMusouSkill = false;
	std::vector<NewCommerStory*>::iterator iter;
	for (unsigned int i = 0; i < mAllStoryCommandList.size(); i++) {
		iter = mAllStoryCommandList.begin() + i;
		NewCommerStory* ns = *iter;
		if (ns == NULL) {
			mAllStoryCommandList.erase(iter);
		}
	}

	m_pDiedEmptyIdList.clear();
}

std::string NewCommerStoryManager::getBestWeaponByProfession( int profession )
{
	std::string pressionName_="";
	switch(profession)
	{
	case PROFESSION_MJ_INDEX:
		{
			pressionName_.append("lance08");
		}break;
	case PROFESSION_GM_INDEX:
		{
			pressionName_.append("fan08");
		}break;
	case PROFESSION_HJ_INDEX:
		{
			pressionName_.append("knife08");
		}break;
	case PROFESSION_SS_INDEX:
		{
			pressionName_.append("bow08");
		}break;
	}
	return pressionName_;
}

void NewCommerStoryManager::startAutoGuideToCallGeneral()
{
	MainScene * mainScene = GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI*)mainScene->getGeneralInfoUI();
		if (generalsInfoUI)
		{
			generalsInfoUI->setAutoGuideToCallGeneral(true);
		}
	}
}

void NewCommerStoryManager::endAutoGuideToCallGeneral()
{
	MainScene * mainScene = GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI*)mainScene->getGeneralInfoUI();
		if (generalsInfoUI)
		{
			generalsInfoUI->setAutoGuideToCallGeneral(false);
		}
	}
}
