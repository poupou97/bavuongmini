#ifndef _NEWCOMMERSTORY_COMMAND_NSSELECTCOUNTRY_H_
#define _NEWCOMMERSTORY_COMMAND_NSSELECTCOUNTRY_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include <string>
#include <vector>
#include "../NewCommerStory.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace std;

/**
 * 新手剧情-选择国家逻辑
 * 
 * @author yangjun
 * @date 2014-9-21
 */
class NSSelectCountry : public NewCommerStory
{
public:
	NSSelectCountry(void);
	~NSSelectCountry(void);

	static void* createInstance() ;
	void init();

	virtual void update();

private:
};

#endif
