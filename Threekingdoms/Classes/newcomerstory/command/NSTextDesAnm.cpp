#include "NSTextDesAnm.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../AppMacros.h"
#include "../NewCommerStoryManager.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"

NSTextDesAnm1::NSTextDesAnm1(void)
{
}


NSTextDesAnm1::~NSTextDesAnm1(void)
{
}

void* NSTextDesAnm1::createInstance()
{
	return new NSTextDesAnm1() ;
}

void NSTextDesAnm1::init()
{
	setState(STATE_INITED);
}

void NSTextDesAnm1::update() {

	CCScene* scene = CCDirector::sharedDirector()->getRunningScene();
	
	switch (mState) {
	case STATE_INITED:
		{
			CCDirector::sharedDirector()->setNextDeltaTimeZero(true);
			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSTextDesAnm");

			//主界面不显示
		 	NewCommerStoryManager::getInstance()->setMainSceneState(2);

			CCSize winSize  = CCDirector::sharedDirector()->getVisibleSize();
			NSTextDesAnmUI1 * textDesAnmUI = NSTextDesAnmUI1::create();
			textDesAnmUI->ignoreAnchorPointForPosition(false);
			textDesAnmUI->setAnchorPoint(ccp(.5f,.5f));
			textDesAnmUI->setPosition(ccp(winSize.width/2,winSize.height/2));
			textDesAnmUI->setTag(kTag_NewLayer);
			scene->addChild(textDesAnmUI,10000);

			setState(STATE_UPDATE);
		}
		break;
	case STATE_UPDATE:
		{
			if (scene->getChildByTag(kTag_NewLayer) == NULL)
			{
				GameView::getInstance()->myplayer->setVisible(false);
				setState(STATE_END);
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}



//////////////////////////////////

NSTextDesAnmUI1::NSTextDesAnmUI1( void )
{

}

NSTextDesAnmUI1::~NSTextDesAnmUI1( void )
{

}

NSTextDesAnmUI1 * NSTextDesAnmUI1::create()
{
	NSTextDesAnmUI1 * ui =new NSTextDesAnmUI1();
	if (ui && ui->init())
	{
		ui->autorelease();
		return ui;
	}
	CC_SAFE_DELETE(ui);
	return NULL;
}

bool NSTextDesAnmUI1::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("newcommerstory/zhezhao.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winSize);
		mengban->setAnchorPoint(ccp(0.5f,0.5f));
		mengban->setPosition(ccp(winSize.width/2,winSize.height/2));
		m_pUiLayer->addWidget(mengban);

		CCSequence * sq = CCSequence::create(
			CCDelayTime::create(0.2f),
			CCCallFunc::create(this,callfunc_selector(NSTextDesAnmUI1::createUI)),
			NULL);
		this->runAction(sq);

		this->setTouchEnabled(true);
		this->setContentSize(winSize);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}

void NSTextDesAnmUI1::onEnter()
{
	UIScene::onEnter();
}

void NSTextDesAnmUI1::onExit()
{
	UIScene::onExit();
}

bool NSTextDesAnmUI1::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void NSTextDesAnmUI1::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void NSTextDesAnmUI1::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void NSTextDesAnmUI1::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void NSTextDesAnmUI1::createUI()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

//  	UILabel * l_des1 = UILabel::create();
//  	l_des1->setFontName(APP_FONT_NAME);
//  	l_des1->setFontSize(30);
//  	//l_des1->setColor(ccc3(99,132,18));
//  	l_des1->setText(StringDataManager::getString("NewComerStory_TextAnm_des1"));
//  	l_des1->setAnchorPoint(ccp(.5f,.5f));
//  	l_des1->setPosition(ccp(winSize.width/2,winSize.height/2 + 60));
//  	m_pUiLayer->addWidget(l_des1);
//  	l_des1->setVisible(false);
	UIImageView * imageView_des1 = UIImageView::create();
	imageView_des1->setTexture("newcommerstory/1.png");
	imageView_des1->setAnchorPoint(ccp(.5f,.5f));
	imageView_des1->setPosition(ccp(winSize.width/2,winSize.height/2 + 60));
	m_pUiLayer->addWidget(imageView_des1);
	imageView_des1->setVisible(false);
 	 
//  	UILabel * l_des2 = UILabel::create();
//  	l_des2->setFontName(APP_FONT_NAME);
//  	l_des2->setFontSize(30);
//  	//l_des2->setColor(ccc3(99,132,18));
//  	l_des2->setText(StringDataManager::getString("NewComerStory_TextAnm_des2"));
//  	l_des2->setAnchorPoint(ccp(.5f,.5f));
//  	l_des2->setPosition(ccp(winSize.width/2,winSize.height/2));
//  	m_pUiLayer->addWidget(l_des2);
//  	l_des2->setVisible(false);
	UIImageView * imageView_des2 = UIImageView::create();
	imageView_des2->setTexture("newcommerstory/2.png");
	imageView_des2->setAnchorPoint(ccp(.5f,.5f));
	imageView_des2->setPosition(ccp(winSize.width/2,winSize.height/2));
	m_pUiLayer->addWidget(imageView_des2);
	imageView_des2->setVisible(false);
 	 
//  	UILabel * l_des3 = UILabel::create();
//  	l_des3->setFontName(APP_FONT_NAME);
//  	l_des3->setFontSize(30);
//  	//l_des3->setColor(ccc3(99,132,18));
//  	l_des3->setText(StringDataManager::getString("NewComerStory_TextAnm_des3"));
//  	l_des3->setAnchorPoint(ccp(.5f,.5f));
//  	l_des3->setPosition(ccp(winSize.width/2,winSize.height/2 - 60));
//  	m_pUiLayer->addWidget(l_des3);
//  	l_des3->setVisible(false);
	UIImageView * imageView_des3 = UIImageView::create();
	imageView_des3->setTexture("newcommerstory/3.png");
	imageView_des3->setAnchorPoint(ccp(.5f,.5f));
	imageView_des3->setPosition(ccp(winSize.width/2,winSize.height/2-60));
	m_pUiLayer->addWidget(imageView_des3);
	imageView_des3->setVisible(false);

	UIImageView * imageView_des4 = UIImageView::create();
	imageView_des4->setTexture("newcommerstory/open.png");
	imageView_des4->setAnchorPoint(ccp(.5f,.5f));
	imageView_des4->setPosition(ccp(winSize.width/2,winSize.height/2));
	m_pUiLayer->addWidget(imageView_des4);
	imageView_des4->setVisible(false);
 	 
 	CCSequence * action_l_des1 = CCSequence::create(
 	 	CCDelayTime::create(1.0f),
 	 	CCShow::create(),
 	 	CCFadeIn::create(0.8f),
 	 	CCDelayTime::create(4.0f),
 	 	CCFadeOut::create(0.8f),
 	 	CCRemoveSelf::create(),
 	 	NULL
 	 	);
 	imageView_des1->runAction(action_l_des1);
 	 
 	CCSequence * action_l_des2 = CCSequence::create(
 	 	CCDelayTime::create(2.8f),
 	 	CCShow::create(),
 	 	CCFadeIn::create(0.8f),
 	 	CCDelayTime::create(3.0f),
 	 	CCFadeOut::create(0.8f),
 	 	CCRemoveSelf::create(),
 	 	NULL
 	 	);
 	imageView_des2->runAction(action_l_des2);
 	 
 	CCSequence * action_l_des3 = CCSequence::create(
 	 	CCDelayTime::create(4.6f),
 	 	CCShow::create(),
 	 	CCFadeIn::create(0.8f),
 	 	CCDelayTime::create(2.0f),
 	 	CCFadeOut::create(0.8f),
 	 	CCRemoveSelf::create(),
 	 	NULL
 	 	);
 	imageView_des3->runAction(action_l_des3);

	CCSequence * action_l_des4 = CCSequence::create(
		CCDelayTime::create(8.5f),
		CCShow::create(),
		CCFadeIn::create(0.8f),
		CCDelayTime::create(2.0f),
		CCFadeOut::create(1.3f),
		CCRemoveSelf::create(),
		NULL
		);
	imageView_des4->runAction(action_l_des4);

	CCSequence * action_this = CCSequence::create(
		CCDelayTime::create(13.f),
		CCRemoveSelf::create(),
		NULL
		);
	this->runAction(action_this);

	//addEffect
	float firstPosX = 200*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH);
	float secondPosX = 400*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH);
	float thirdPosX = 600*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH);
	std::string effectPathName = "animation/texiao/particledesigner/huo2.plist";
	// effect animation
	CCParticleSystemQuad* pParticleEffect = CCParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setPosition(ccp(firstPosX,10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = CCParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setPosition(ccp(secondPosX,10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = CCParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setPosition(ccp(thirdPosX,10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	 
	pParticleEffect = CCParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setPosition(ccp(firstPosX,10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = CCParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setPosition(ccp(secondPosX,10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = CCParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setPosition(ccp(thirdPosX,10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	 
	pParticleEffect = CCParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setRotation(180);
	pParticleEffect->setPosition(ccp(firstPosX,winSize.height-10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = CCParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setRotation(180);
	pParticleEffect->setPosition(ccp(secondPosX,winSize.height-10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = CCParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setRotation(180);
	pParticleEffect->setPosition(ccp(thirdPosX,winSize.height-10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	 
	pParticleEffect = CCParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setRotation(180);
	pParticleEffect->setPosition(ccp(firstPosX,winSize.height-10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = CCParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setRotation(180);
	pParticleEffect->setPosition(ccp(secondPosX,winSize.height-10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = CCParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setRotation(180);
	pParticleEffect->setPosition(ccp(thirdPosX,winSize.height-10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);

	for (int i = 0;i<3;i++)
	{
		pParticleEffect = CCParticleSystemQuad::create("animation/texiao/particledesigner/huomiao1.plist");
		pParticleEffect->setPositionType(kCCPositionTypeGrouped);
		pParticleEffect->setPosition(ccp(winSize.width/4*(i+1),winSize.height/2));
		pParticleEffect->setScaleX(1.5f);
		this->addChild(pParticleEffect);
	}
}
