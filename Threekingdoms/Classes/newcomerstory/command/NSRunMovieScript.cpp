#include "NSRunMovieScript.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../legend_script/ScriptManager.h"

NSRunMovieScript::NSRunMovieScript(void)
{
}


NSRunMovieScript::~NSRunMovieScript(void)
{
}

void* NSRunMovieScript::createInstance()
{
	return new NSRunMovieScript() ;
}

void NSRunMovieScript::init( const char * scriptPath )
{
	m_sScriptPath = scriptPath;

	setState(STATE_INITED);
}

void NSRunMovieScript::update() {
	switch (mState) {
	case STATE_INITED:
		{
			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSRunMovieScript");

			std::string scriptFileName = "script/";
			scriptFileName.append(m_sScriptPath);
			ScriptManager::getInstance()->runScript(scriptFileName);

			setState(STATE_UPDATE);
		}
		break;
	case STATE_UPDATE:
		{
			//CCNode* pNode = GameView::getInstance()->getGameScene()->getChildByTag(kTagMovieBaseNode);
// 			if(pNode == NULL)
// 			{
// 				setState(STATE_END);
// 			}
			if (!ScriptManager::getInstance()->isInMovieMode())
			{
				setState(STATE_END);
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}