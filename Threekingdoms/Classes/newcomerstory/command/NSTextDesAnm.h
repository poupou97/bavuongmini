﻿#ifndef _NEWCOMMERSTORY_COMMAND_NSTEXTDESANM_H_
#define _NEWCOMMERSTORY_COMMAND_NSTEXTDESANM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include <string>
#include <vector>
#include "../NewCommerStory.h"
#include "../../ui/extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace std;

/**
 * 新手剧情-文字动画效果逻辑
 * @author yangjun
 * @date 2014-9-11
 */

////////////////////////////////////////////////////////////////////////////////////////////////////
/************** 先缓慢出现【上古】2字，停留2秒钟。再缓慢出现【•魔界】，停留3秒钟************/
class NSTextDesAnm1 : public NewCommerStory
{
public:
	NSTextDesAnm1(void);
	~NSTextDesAnm1(void);

	static void* createInstance() ;
	void init();

	virtual void update();

};

//////////////////////////////////////
/**
 * 新手剧情-文字动画界面
 * @author yangjun
 * @date 2014-9-11
 */
class NSTextDesAnmUI1:public UIScene
{
public:
	NSTextDesAnmUI1(void);
	~NSTextDesAnmUI1(void);

	static NSTextDesAnmUI1 * create();
	bool init();

	void onEnter();
	void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void createUI();
};


#endif
