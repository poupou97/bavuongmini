#ifndef _NEWCOMMERSTORY_COMMAND_NSFIGHT_H_
#define _NEWCOMMERSTORY_COMMAND_NSFIGHT_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include <string>
#include <vector>
#include "../NewCommerStory.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace std;

/**
 * 新手剧情-战斗逻辑
 * @author yangjun
 * @date 2014-9-12
 */

////////////////////////////////////////////////////////////////////////////////////////////////////
/**************玩家操作部分1——移动+单体技能教学，第一次打怪************/
class NSFight1 : public NewCommerStory
{
public:
	NSFight1(void);
	~NSFight1(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/**************玩家操作部分3——与魔星、贴身侍女战斗************/
class NSFight2 : public NewCommerStory
{
public:
	NSFight2(void);
	~NSFight2(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
	float m_nDelayTime;
};


////////////////////////////////////////////////////////////////////////////////////////////////////
/**************玩家操作部分4——施放龙魂技能************/
class NSFight3 : public NewCommerStory
{
public:
	NSFight3(void);
	~NSFight3(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
	float m_nDelayTime;
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/**************玩家操作部分1——群体技能教学，打怪************/
class NSFight4 : public NewCommerStory
{
public:
	NSFight4(void);
	~NSFight4(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
};




////////////////////////////////////////////////////////////////////////////////////////////////////
/**************玩家操作部分—杀第一波黄巾军************/
class NSFight5 : public NewCommerStory
{
public:
	NSFight5(void);
	~NSFight5(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
	float m_nDelayTime;
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/**************玩家操作部分——武将上阵，出战************/
class NSFight6 : public NewCommerStory
{
public:
	NSFight6(void);
	~NSFight6(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
	float m_nDelayTime;
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/**************玩家操作部分——多武将上阵，教学出战第二个武将，第三个武将************/
class NSFight7 : public NewCommerStory,public CCNode
{
public:
	NSFight7(void);
	~NSFight7(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
	float m_nDelayTime;

	bool isRunningAction;
	float actionTime;
	void startSlowMotion();
	void endSlowMotion();
	void actionFinishCallBack();

	void addLightAnimation();
};
#endif
