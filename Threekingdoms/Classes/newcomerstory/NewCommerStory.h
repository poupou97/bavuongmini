#ifndef _NEWCOMMERSTORY_NEWCOMMERSTORY_H_
#define _NEWCOMMERSTORY_NEWCOMMERSTORY_H_

#include "cocos2d.h"
#include <string>
#include <vector>

USING_NS_CC;
using namespace std;

#define kTag_NewLayer 998

/**
 * 新手剧情基类
 * @author yangjun
 * @date 2014-9-11
 */
class NewCommerStory {
public:
	enum story_state {
		STATE_INITED = 0,       //初始化完成
		STATE_START = 1,       //开始创建
		STATE_UPDATE = 2,     //过程(update)
		STATE_END = 3,          //结束
	};

	NewCommerStory();
	virtual ~NewCommerStory();

	int m_nStoryId;

	inline int getId() { return m_nStoryId; };

protected:
	int mState;
	void setState(int state) ;

public:
	inline bool isInit() {
		return mState == STATE_INITED;
	};

	inline bool isStart() {
		return mState == STATE_START;
	};

	inline bool isUpdate() {
		return mState == STATE_UPDATE;
	};

	bool isEnd() {
		return mState == STATE_END;
	};

public:
	virtual void update();
};

#endif