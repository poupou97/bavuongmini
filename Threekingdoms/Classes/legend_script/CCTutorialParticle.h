
#ifndef _LEGEND_SCRIPT_CCTUTORIALPARTICLE_H_
#define _LEGEND_SCRIPT_CCTUTORIALPARTICLE_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define CCTUTORIALPARTICLETAG 875421

class CCTutorialParticle : public CCNodeRGBA
{
public:
	CCTutorialParticle();
	virtual ~CCTutorialParticle();

	static CCTutorialParticle * create(const char* anmName,float controlX, float controlY);
	bool init(const char* anmName,float controlX, float controlY);
};

#endif
