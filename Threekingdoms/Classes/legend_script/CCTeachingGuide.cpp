#include "CCTeachingGuide.h"
#include "CCTutorialIndicator.h"
#include "../ui/GameUIConstant.h"
#include "../gamescene_state/MainScene.h"
#include "GameView.h"
#include "ScriptManager.h"

#define kTagTutorialIndicator 15

CCTeachingGuide::CCTeachingGuide(void)
{
}

CCTeachingGuide::~CCTeachingGuide(void)
{
}

CCTeachingGuide * CCTeachingGuide::create( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction,int clipWidth,int clipHeight,bool isAddToMainScene /*= false*/,bool isDesFrameVisible /*= true*/ ,bool isHandFlip)
{
	CCTeachingGuide * teachingGuide = new CCTeachingGuide();
	if (teachingGuide && teachingGuide->init(content,pos,direction,clipWidth,clipHeight,isAddToMainScene,isDesFrameVisible,isHandFlip))
	{
		teachingGuide->autorelease();
		return teachingGuide;
	}
	CC_SAFE_DELETE(teachingGuide);
	return NULL;
}

bool CCTeachingGuide::init( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction,int clipWidth,int clipHeight,bool isAddToMainScene,bool isDesFrameVisible,bool isHandFlip )
{
	if (CCLayer::init())
	{
		m_nDelayTime = 0;
		m_bIsTimeUP = false;

		m_nClipWidth = clipWidth;
		m_nClipHeight = clipHeight;
		m_curDirection = direction;

		//test by yangjun 
 		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
 		//创建裁剪节点
 		clip = CCClippingNode::create();
 		clip->setInverted(true);
 		clip->setAlphaThreshold(1.0f);
 		addChild(clip);
 		//为裁剪节点添加一个黑色带透明的底板
 		CCLayerColor * back = CCLayerColor::create(ccc4(0,0,0,0),visibleSize.width,visibleSize.height);
 		clip->addChild(back);
 		 
 		front = CCDrawNode::create();
 		ccColor4F yellow = {1,1,0,1};
 		CCPoint rect[4] = {ccp(-m_nClipWidth/2,m_nClipHeight/2),ccp(m_nClipWidth/2,m_nClipHeight/2),ccp(m_nClipWidth/2,-m_nClipHeight/2),ccp(-m_nClipWidth/2,-m_nClipHeight/2)};
 		front->drawPolygon(rect,4,yellow,0,yellow);
 		clip->setStencil(front);

		tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,isAddToMainScene,isDesFrameVisible,isHandFlip);
		//tutorialIndicator->setPosition(ccp(pos.x-45,pos.y+95));
		tutorialIndicator->setTag(kTagTutorialIndicator);
		this->addChild(tutorialIndicator);
		tutorialIndicator->addCenterAnm(m_nClipWidth,m_nClipHeight);

		sp_yellowFrame = CCScale9Sprite::create("res_ui/yellow_frame.png"); 
		sp_yellowFrame->setCapInsets(CCRectMake(5,5,1,1));
		sp_yellowFrame->setPreferredSize(CCSizeMake(m_nClipWidth*6.0f,m_nClipHeight*6.0f));
		sp_yellowFrame->setAnchorPoint(ccp(0.5f, 0.5f));
		this->addChild(sp_yellowFrame);
		sp_yellowFrame->setVisible(false);

		playYellowFrameAnm();
		
		this->schedule(schedule_selector(CCTeachingGuide::updateTime),1.0f);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(visibleSize);

		this->setTouchPriority(TPriority_Tutorial);

		return true;
	}
	return false;
}

void CCTeachingGuide::setDrawNodePos( CCPoint pt )
{
	front->setPosition(pt);
	sp_yellowFrame->setPosition(pt);

	CCTutorialIndicator * temp = (CCTutorialIndicator*)this->getChildByTag(kTagTutorialIndicator);
	if (temp)
	{
		tutorialIndicator->setPosition(ccp(pt.x,pt.y));
	}
}

void CCTeachingGuide::onEnter()
{
	CCLayer::onEnter();
}

void CCTeachingGuide::onExit()
{
	CCLayer::onExit();
}

bool CCTeachingGuide::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	if (!m_bIsTimeUP)
	{
		CCSize visibleSize=CCDirector::sharedDirector()->getVisibleSize();

		CCPoint point=CCDirector::sharedDirector()->convertToGL(pTouch->getLocationInView());
		CCRect rect=CCRectMake(front->getPositionX()-m_nClipWidth/2,front->getPositionY()-m_nClipHeight/2,m_nClipWidth,m_nClipHeight);
		if (rect.containsPoint(point))
		{
			return false;
		}

		playYellowFrameAnm();
		return true;
	}
	else
	{
		return false;
	}
}

void CCTeachingGuide::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void CCTeachingGuide::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void CCTeachingGuide::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

#define ktag_yellowFrame_Anm 69
void CCTeachingGuide::playYellowFrameAnm()
{
	if (sp_yellowFrame->getActionByTag(ktag_yellowFrame_Anm))
		sp_yellowFrame->stopActionByTag(ktag_yellowFrame_Anm);

	sp_yellowFrame->setScale(1.0f);

	CCSequence * sequence = CCSequence::create(
		CCShow::create(),
		CCScaleTo::create(0.5f,1.0f/6.0f,1.0f/6.0f),
		CCHide::create(),
		NULL);
	sp_yellowFrame->runAction(sequence);
	sequence->setTag(ktag_yellowFrame_Anm);
}

void CCTeachingGuide::updateTime( float dt )
{
	if (m_bIsTimeUP)
		return;

	m_nDelayTime++;
	if (m_nDelayTime > SCRIPT_CCTEACHINGGUIDE_DELAY_TIME)
	{
		m_bIsTimeUP = true;
	}
}

void CCTeachingGuide::CloseUIForTutorial()
{
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (!mainScene)
		return;

	for (int i = 0;i<ScriptManager::getInstance()->mCloseUIData.size();i++)
	{
		if (mainScene->getChildByTag(ScriptManager::getInstance()->mCloseUIData.at(i)))
		{
			mainScene->getChildByTag(ScriptManager::getInstance()->mCloseUIData.at(i))->removeFromParent();
		}
	}
}

