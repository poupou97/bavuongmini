
#ifndef _LEGEND_SCRIPT_UITUTORIALINDICATOR_H_
#define _LEGEND_SCRIPT_UITUTORIALINDICATOR_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define UITUTORIALINDICATORTAG 986532

class UITutorialIndicator : public UIWidget
{
public:
	UITutorialIndicator();
	~UITutorialIndicator();

	static UITutorialIndicator * create();
	bool init();

private:
	UIImageView * m_image_indicator;
	UILabel* m_label_des; 
};

#endif