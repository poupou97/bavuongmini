#ifndef _LEGEND_SCRIPT_SCRIPT_H_
#define _LEGEND_SCRIPT_SCRIPT_H_

#include "../common/CKBaseClass.h"

#include "cocos2d.h"
#include <string>
#include <vector>

USING_NS_CC;
using namespace std;

class ScriptManager;

/**
 * 脚本模型
 * @author wangxueping
 * @date 2012-1-6
 */
class Script : public CKBaseClass {
public:
	/** 脚本状态 */
	enum script_state {
		STATE_START = 0,
		STATE_UPDATE = 1,
		STATE_END = 2,
	};

	Script();
	virtual ~Script();

	// when new Script(), s_newScriptId++ will be used for the script's id
	static int s_newScriptId;

	inline int getId() { return id; };

	void set(string& cmd, string& para);

	/** 
      * 是否阻塞function，
	  * 阻塞function，是指该function必须执行完后，才执行下面的function，比如等待function(SCWait)
	  * 非阻塞function，比如createNPC()
      */
	virtual bool isBlockFunction() { return false; };

	/**
      * 一些命令，需要其他对象提供反馈，来表示结束。比如各种教学脚本类，都需要这些handler
      */
	void setCommandHandler(CCObject* owner);
	virtual void endCommand(CCObject* pSender);

protected:
	void setState(int state) ;

public:
	void setScriptManager(ScriptManager* scriptManager);

	inline bool isStart() {
		return mState == STATE_START;
	};

	inline bool isUpdate() {
		return mState == STATE_UPDATE;
	};

	bool isEnd() {
		return mState == STATE_END;
	};

protected:
	void setStartTime(long time) {
		mStartTime = time;
	};

	void setDelayTime(long time) {
		mDelayTime = time;
	};

	/**
	 * 持续时间结束
	 */
	bool goEnd();

public:
	virtual void update() = 0;

	virtual void init() = 0;

	virtual void release() = 0;

	/**
	 * 当前命令是否是cmd
	 */
	inline bool isScriptCMD(string cmd) {
		return mCmd.compare(cmd) == 0;
	};

private:
	/** 脚本命令 */
	string mCmd;

public:
	/** 参数数组 */
	vector<string> mPara;

protected:
	int mState;

private:
	/** 脚本开始时间 */
	long mStartTime;
	/** 脚本持续时间 */
	long mDelayTime;

	
protected:
	/** 脚本管理器 */
	ScriptManager* mScriptManager;

private:
	int id;
	CCObject* m_pHandler;
};

#define REGISTER_SCRIPT_COMMAND(handler, scriptId)\
handler->registerScriptCommand(scriptId) ;        \
this->setCommandHandler(handler) ;

#endif