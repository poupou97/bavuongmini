#include "UITutorialIndicator.h"
#include "AppMacros.h"

UITutorialIndicator::UITutorialIndicator()
{
}


UITutorialIndicator::~UITutorialIndicator()
{
}

UITutorialIndicator * UITutorialIndicator::create()
{
	UITutorialIndicator * tutorialIndicator = new UITutorialIndicator();
	if (tutorialIndicator && tutorialIndicator->init())
	{
		tutorialIndicator->autorelease();
		return tutorialIndicator;
	}
	CC_SAFE_DELETE(tutorialIndicator);
	return NULL;
}

bool UITutorialIndicator::init()
{
	if (UIWidget::init())
	{
		m_image_indicator = UIImageView::create();
		m_image_indicator->setTexture("res_ui/liaotian/jiantou.png");
		m_image_indicator->setAnchorPoint(ccp(0.5f,0.5f));
		m_image_indicator->setPosition(ccp(0,0));
		this->addChild(m_image_indicator);

		m_label_des = UILabel::create();
		m_label_des->setFontName(APP_FONT_NAME);
		m_label_des->setFontSize(30);
		m_label_des->setText("hello");
		m_label_des->setPosition(ccp(0, 0));
		this->addChild(m_label_des);

		return true;
	}
	return false;
}
