#include "SCTutorialFiveInstance.h"
#include "../../utils/GameUtils.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../ui/FivePersonInstance/InstanceDetailUI.h"
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../ui/FivePersonInstance/InstanceMapUI.h"


IMPLEMENT_CLASS(SCTutorialFiveInstance1)

SCTutorialFiveInstance1::SCTutorialFiveInstance1()
{
}

SCTutorialFiveInstance1::~SCTutorialFiveInstance1()
{
}

void* SCTutorialFiveInstance1::createInstance()
{
	return new SCTutorialFiveInstance1() ;
}

void SCTutorialFiveInstance1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene1()");

			std::string content = mPara.at(0);

			if (FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
			{
				setState(STATE_END);
				return;
			}

			if (GameView::getInstance()->getMainUIScene()->isMapOpen)
			{
				setState(STATE_END);
			}
			else
			{
				UIButton * btn_mapOpen = GameView::getInstance()->getMainUIScene()->Button_mapOpen;
				MainScene * mainScene = GameView::getInstance()->getMainUIScene();
				mainScene->addCCTutorialIndicator(content.c_str(),btn_mapOpen->getPosition(),CCTutorialIndicator::Direction_RU);
				//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
				mainScene->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(btn_mapOpen) ;
				setState(STATE_UPDATE);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialFiveInstance1::init() {
	CCLOG("SCTutorialFiveInstance1() init");
}

void SCTutorialFiveInstance1::release() {
	CCLOG("SCTutorialFiveInstance1() release");

	if (GameView::getInstance()->getGameScene())
	{
		MainScene * mainScene = GameView::getInstance()->getMainUIScene();
		if(mainScene != NULL)
			mainScene->removeCCTutorialIndicator();
	}
}


///////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialFiveInstance2)

	SCTutorialFiveInstance2::SCTutorialFiveInstance2()
{
}

SCTutorialFiveInstance2::~SCTutorialFiveInstance2()
{
}

void* SCTutorialFiveInstance2::createInstance()
{
	return new SCTutorialFiveInstance2() ;
}

void SCTutorialFiveInstance2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene1()");

			std::string content = mPara.at(0);

			if (FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
			{
				setState(STATE_END);
				return;
			}

			GuideMap * guideMap = GameView::getInstance()->getMainUIScene()->guideMap;
			guideMap->addCCTutorialIndicator(content.c_str(),guideMap->btn_instance->getPosition(),CCTutorialIndicator::Direction_RU);
			//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
			guideMap->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(guideMap->btn_instance) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialFiveInstance2::init() {
	CCLOG("SCTutorialFiveInstance2() init");
}

void SCTutorialFiveInstance2::release() {
	CCLOG("SCTutorialFiveInstance2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GuideMap * guideMap = GameView::getInstance()->getMainUIScene()->guideMap;
	if(guideMap != NULL)
		guideMap->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialFiveInstance3)

	SCTutorialFiveInstance3::SCTutorialFiveInstance3()
{
}

SCTutorialFiveInstance3::~SCTutorialFiveInstance3()
{
}

void* SCTutorialFiveInstance3::createInstance()
{
	return new SCTutorialFiveInstance3() ;
}

void SCTutorialFiveInstance3::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene1()");

			std::string content = mPara.at(0);
			if (FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
			{
				setState(STATE_END);
				return;
			}

			InstanceDetailUI * instanceDetailUI = (InstanceDetailUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceDetailUI);
			if (!instanceDetailUI)
			{
				setState(STATE_START);
				return;
			}
			else
			{
				if (!instanceDetailUI->isFinishAction())
				{
					setState(STATE_START);
					return;
				}
			}

			instanceDetailUI->addCCTutorialIndicator(content.c_str(),instanceDetailUI->btnEnter->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
			instanceDetailUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(instanceDetailUI->btnEnter) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialFiveInstance3::init() {
	CCLOG("SCTutorialFiveInstance3() init");
}

void SCTutorialFiveInstance3::release() {
	CCLOG("SCTutorialFiveInstance3() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	InstanceDetailUI * instanceDetailUI = (InstanceDetailUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceDetailUI);
	if(instanceDetailUI != NULL)
		instanceDetailUI->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialFiveInstance4)

SCTutorialFiveInstance4::SCTutorialFiveInstance4()
{
}

SCTutorialFiveInstance4::~SCTutorialFiveInstance4()
{
}

void* SCTutorialFiveInstance4::createInstance()
{
	return new SCTutorialFiveInstance4() ;
}

void SCTutorialFiveInstance4::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialFiveInstance4()");

			std::string content = mPara.at(0);
			if (FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
			{
				setState(STATE_END);
				return;
			}

			InstanceMapUI * instanceMapUI = (InstanceMapUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceMapUI);
			if (!instanceMapUI)
			{
				setState(STATE_START);
				return;
			}

			instanceMapUI->ScrollInstanceToCenter(11);
			instanceMapUI->addCCTutorialIndicator(content.c_str(),instanceMapUI->btn_firstInstance->getPosition(),CCTutorialIndicator::Direction_LU);
			//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
			instanceMapUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(instanceMapUI->btn_firstInstance) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialFiveInstance4::init() {
	CCLOG("SCTutorialFiveInstance4() init");
}

void SCTutorialFiveInstance4::release() {
	CCLOG("SCTutorialFiveInstance4() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	InstanceMapUI * instanceMapUI = (InstanceMapUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceMapUI);
	if(instanceMapUI != NULL)
		instanceMapUI->removeCCTutorialIndicator();
}



/////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialFiveInstance5)

SCTutorialFiveInstance5::SCTutorialFiveInstance5()
{
}

SCTutorialFiveInstance5::~SCTutorialFiveInstance5()
{
}

void* SCTutorialFiveInstance5::createInstance()
{
	return new SCTutorialFiveInstance5() ;
}

void SCTutorialFiveInstance5::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialFiveInstance5()");

			std::string content = mPara.at(0);
			if (FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
			{
				setState(STATE_END);
				return;
			}

			InstanceMapUI * instanceMapUI = (InstanceMapUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceMapUI);
			if (!instanceMapUI)
			{
				setState(STATE_START);
				return;
			}

			instanceMapUI->ScrollInstanceToCenter(12);
			instanceMapUI->addCCTutorialIndicator(content.c_str(),instanceMapUI->btn_secondInstance->getPosition(),CCTutorialIndicator::Direction_LU);
			//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
			instanceMapUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(instanceMapUI->btn_secondInstance) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialFiveInstance5::init() {
	CCLOG("SCTutorialFiveInstance5() init");
}

void SCTutorialFiveInstance5::release() {
	CCLOG("SCTutorialFiveInstance5() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	InstanceMapUI * instanceMapUI = (InstanceMapUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceMapUI);
	if(instanceMapUI != NULL)
		instanceMapUI->removeCCTutorialIndicator();
}



/////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialFiveInstance6)

SCTutorialFiveInstance6::SCTutorialFiveInstance6()
{
}

SCTutorialFiveInstance6::~SCTutorialFiveInstance6()
{
}

void* SCTutorialFiveInstance6::createInstance()
{
	return new SCTutorialFiveInstance6() ;
}

void SCTutorialFiveInstance6::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialFiveInstance6()");

			std::string content = mPara.at(0);
			if (FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
			{
				setState(STATE_END);
				return;
			}

			InstanceMapUI * instanceMapUI = (InstanceMapUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceMapUI);
			if (!instanceMapUI)
			{
				setState(STATE_START);
				return;
			}

			instanceMapUI->ScrollInstanceToCenter(13);
			instanceMapUI->addCCTutorialIndicator(content.c_str(),instanceMapUI->btn_thirdInstance->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
			instanceMapUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(instanceMapUI->btn_thirdInstance) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialFiveInstance6::init() {
	CCLOG("SCTutorialFiveInstance6() init");
}

void SCTutorialFiveInstance6::release() {
	CCLOG("SCTutorialFiveInstance6() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	InstanceMapUI * instanceMapUI = (InstanceMapUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceMapUI);
	if(instanceMapUI != NULL)
		instanceMapUI->removeCCTutorialIndicator();
}



/////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialFiveInstance7)

SCTutorialFiveInstance7::SCTutorialFiveInstance7()
{
}

SCTutorialFiveInstance7::~SCTutorialFiveInstance7()
{
}

void* SCTutorialFiveInstance7::createInstance()
{
	return new SCTutorialFiveInstance7() ;
}

void SCTutorialFiveInstance7::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialFiveInstance7()");

			std::string content = mPara.at(0);
			if (FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
			{
				setState(STATE_END);
				return;
			}

			InstanceMapUI * instanceMapUI = (InstanceMapUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceMapUI);
			if (!instanceMapUI)
			{
				setState(STATE_START);
				return;
			}

			instanceMapUI->ScrollInstanceToCenter(1);
			instanceMapUI->addCCTutorialIndicator(content.c_str(),instanceMapUI->btn_fouthInstance->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
			instanceMapUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(instanceMapUI->btn_fouthInstance) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialFiveInstance7::init() {
	CCLOG("SCTutorialFiveInstance7() init");
}

void SCTutorialFiveInstance7::release() {
	CCLOG("SCTutorialFiveInstance7() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	InstanceMapUI * instanceMapUI = (InstanceMapUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceMapUI);
	if(instanceMapUI != NULL)
		instanceMapUI->removeCCTutorialIndicator();
}
