#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALOFFLINEARENA_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALOFFLINEARENA_H_

#include "../Script.h"

/**
 *竞技场教学
 *
 * @author yangjun
 * @date 2014-5-28
 */

//////////////////////点击竞技场按钮////////////////////////
class SCTutorialOffLineArena : public Script {
private:
	DECLARE_CLASS(SCTutorialOffLineArena)

public:
	SCTutorialOffLineArena();
	virtual ~SCTutorialOffLineArena();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////选择一个对手////////////////////////
class SCTutorialOffLineArena1 : public Script {
private:
	DECLARE_CLASS(SCTutorialOffLineArena1)

public:
	SCTutorialOffLineArena1();
	virtual ~SCTutorialOffLineArena1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////点击挑战按钮////////////////////////
class SCTutorialOffLineArena2 : public Script {
private:
	DECLARE_CLASS(SCTutorialOffLineArena2)

public:
	SCTutorialOffLineArena2();
	virtual ~SCTutorialOffLineArena2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif

