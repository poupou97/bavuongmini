#ifndef _LEGEND_SCRIPT_COMMAND_WAIT_H_
#define _LEGEND_SCRIPT_COMMAND_WAIT_H_

#include "../Script.h"

/**
 * 等待
 *
 * @author wangxueping
 * @date 2012-3-29
 */
class SCWait : public Script {
private:
    DECLARE_CLASS(SCWait)

public:
	SCWait();
	virtual ~SCWait();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif