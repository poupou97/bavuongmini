#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALNORMALATTACK_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALNORMALATTACK_H_

#include "../Script.h"

/**
 *普通攻击教学
 *
 * @author yangjun
 * @date 2014-5-4
 */

//////////////////////普通攻击教学////////////////////////
class SCTutorialNormalAttack : public Script {
private:
	DECLARE_CLASS(SCTutorialNormalAttack)

public:
	SCTutorialNormalAttack();
	virtual ~SCTutorialNormalAttack();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif
