#include "SCTutorialMission.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameScene.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/missionscene/HandleMission.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../ui/rewardTask_ui/RewardTaskMainUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/rewardTask_ui/RewardTaskData.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../messageclient/element/CBoardMissionInfo.h"
#include "../../ui/rewardTask_ui/RewardTaskItem.h"

IMPLEMENT_CLASS(SCTutorialGetMission)

	SCTutorialGetMission::SCTutorialGetMission()
{
}

SCTutorialGetMission::~SCTutorialGetMission()
{
}

void* SCTutorialGetMission::createInstance()
{
	return new SCTutorialGetMission() ;
}

void SCTutorialGetMission::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialGetMission()");

			std::string content = mPara.at(0);

			if (!GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission))
			{
				setState(STATE_START);
				return;
			}
			else
			{
				HandleMission * handleMission = (HandleMission*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission);
				if (handleMission->getCurHandlerType() == HandleMission::submitMission)
				{
					setState(STATE_END);
				}
				else
				{
 					handleMission->addCCTutorialIndicator(content.c_str(),handleMission->Button_getMission->getPosition(),CCTutorialIndicator::Direction_LD);
 					//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
 					handleMission->registerScriptCommand(this->getId()) ; 
 					this->setCommandHandler(handleMission->Button_getMission) ;
 					setState(STATE_UPDATE);
				}
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGetMission::init() {
	CCLOG("SCTutorialGetMission() init");
}

void SCTutorialGetMission::release() {
	CCLOG("SCTutorialGetMission() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	HandleMission * handleMission = (HandleMission*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission);
	if(handleMission != NULL)
 		handleMission->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialFinishMission)

SCTutorialFinishMission::SCTutorialFinishMission()
{
}

SCTutorialFinishMission::~SCTutorialFinishMission()
{
}

void* SCTutorialFinishMission::createInstance()
{
	return new SCTutorialFinishMission() ;
}

void SCTutorialFinishMission::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene1()");

			std::string content = mPara.at(0);

			if (!GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission))
			{
				setState(STATE_START);
				return;
			}
			else
			{
				HandleMission * handleMission = (HandleMission*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission);
				if (handleMission->getCurHandlerType() == HandleMission::getMission)
				{
					setState(STATE_END);
				}
				else
				{
					handleMission->addCCTutorialIndicator(content.c_str(),handleMission->Button_finishMission->getPosition(),CCTutorialIndicator::Direction_LD);
					//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
					handleMission->registerScriptCommand(this->getId()) ; 
					this->setCommandHandler(handleMission->Button_finishMission) ;
					setState(STATE_UPDATE);
				}
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialFinishMission::init() {
	CCLOG("SCTutorialFinishMission() init");
}

void SCTutorialFinishMission::release() {
	CCLOG("SCTutorialFinishMission() release");

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene != NULL)
	{
		MainScene* pMainScene = scene->getMainUIScene();
		if(pMainScene != NULL)
		{
			HandleMission * handleMission = (HandleMission*)pMainScene->getChildByTag(kTagHandleMission);
			if(handleMission != NULL)
				handleMission->removeCCTutorialIndicator();
		}
	}
}



/////////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialOpenRewardTaskUI)

SCTutorialOpenRewardTaskUI::SCTutorialOpenRewardTaskUI()
{
}

SCTutorialOpenRewardTaskUI::~SCTutorialOpenRewardTaskUI()
{
}

void* SCTutorialOpenRewardTaskUI::createInstance()
{
	return new SCTutorialOpenRewardTaskUI() ;
}

void SCTutorialOpenRewardTaskUI::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialOpenRewardTaskUI()");

			std::string content = mPara.at(0);

			CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
			RewardTaskMainUI * rewardTaskMainUI = (RewardTaskMainUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskMainUI);
			if (!rewardTaskMainUI)
			{
				rewardTaskMainUI = RewardTaskMainUI::create();
				rewardTaskMainUI->ignoreAnchorPointForPosition(false);
				rewardTaskMainUI->setAnchorPoint(ccp(0.5f,0.5f));
				rewardTaskMainUI->setPosition(ccp(winSize.width/2,winSize.height/2));
				rewardTaskMainUI->setTag(kTagRewardTaskMainUI);
				GameView::getInstance()->getMainUIScene()->addChild(rewardTaskMainUI);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(7014);
			}

			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialOpenRewardTaskUI::init() {
	CCLOG("SCTutorialOpenRewardTaskUI() init");
}

void SCTutorialOpenRewardTaskUI::release() {
	CCLOG("SCTutorialOpenRewardTaskUI() release");
}



/////////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialGetRewardTask)

SCTutorialGetRewardTask::SCTutorialGetRewardTask()
{
}

SCTutorialGetRewardTask::~SCTutorialGetRewardTask()
{
}

void* SCTutorialGetRewardTask::createInstance()
{
	return new SCTutorialGetRewardTask() ;
}

void SCTutorialGetRewardTask::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialGetRewardTask()");

			std::string content = mPara.at(0);

			RewardTaskMainUI * rewardTaskMainUI = (RewardTaskMainUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskMainUI);
			if (!rewardTaskMainUI)
			{
				setState(STATE_START);
				return;
			}

			if (RewardTaskData::getInstance()->p_taskList.size() <= 0)
			{
				setState(STATE_START);
				return;
			}

			RewardTaskItem * temp = dynamic_cast<RewardTaskItem *>(rewardTaskMainUI->m_base_layer->getChildByTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.size()-1));
			if (!temp)
			{
				setState(STATE_START);
				return;
			}

			for(int i = 0;i<(int)GameView::getInstance()->missionManager->MissionList_MainScene.size();++i)
			{
				MissionInfo * tempInfo = GameView::getInstance()->missionManager->MissionList_MainScene.at(i);
				if (tempInfo->missionpackagetype() == 12)  //悬赏任务
				{
					if(tempInfo->missionstate() == accepted || tempInfo->missionstate() == done) //已接或正在做
					{
						setState(STATE_END);
						return;
					}
				}
			}

			//第几个可接
			int index = -1;
			for (int i = 0;i<RewardTaskData::getInstance()->p_taskList.size();i++)
			{
				CBoardMissionInfo * temp = (CBoardMissionInfo*)RewardTaskData::getInstance()->p_taskList.at(i);
				if (temp->state() == dispatched)
				{
					index = i;
					break;
				}
			}

			if (index == -1)
			{
				setState(STATE_END);
				return;
			}

			CBoardMissionInfo * curBoardMissionInfo = (CBoardMissionInfo*)RewardTaskData::getInstance()->p_taskList.at(index);
			RewardTaskItem * rewardTaskItem = dynamic_cast<RewardTaskItem *>(rewardTaskMainUI->m_base_layer->getChildByTag(kTag_RewardTaskItem_Base+curBoardMissionInfo->index()));
			if (!rewardTaskItem)
			{
				setState(STATE_END);
				return;
			}

			CCPoint pos = ccp(0,0);
			CCTutorialIndicator::Direction dir = CCTutorialIndicator::Direction_LU;
			if (index <= 2)
			{
				dir = CCTutorialIndicator::Direction_LU;
				pos = ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*index+120,RewardTastItem_First_Pos_y+110);
			}
			else
			{
				dir = CCTutorialIndicator::Direction_RU;
				pos = ccp(RewardTastItem_First_Pos_x+RewardTastItem_space*index+20,RewardTastItem_First_Pos_y+120);
			}

			//指向一个可以接受的悬赏任务
			rewardTaskMainUI->addCCTutorialIndicator(content.c_str(),pos,dir);
			//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
			rewardTaskMainUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(rewardTaskItem) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGetRewardTask::init() {
	CCLOG("SCTutorialGetRewardTask() init");
}

void SCTutorialGetRewardTask::release() {
	CCLOG("SCTutorialGetRewardTask() release");

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene != NULL)
	{
		MainScene* pMainScene = scene->getMainUIScene();
		if(pMainScene != NULL)
		{
			RewardTaskMainUI * rewardTaskMainUI = (RewardTaskMainUI*)pMainScene->getChildByTag(kTagRewardTaskMainUI);
			if (rewardTaskMainUI)
			{
				rewardTaskMainUI->removeCCTutorialIndicator();
			}
		}
	}
}