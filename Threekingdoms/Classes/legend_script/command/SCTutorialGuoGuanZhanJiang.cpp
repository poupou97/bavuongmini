#include "SCTutorialGuoGuanZhanJiang.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../CCTutorialIndicator.h"
#include "../../gamescene_state/sceneelement/MyHeadInfo.h"
#include "../../ui/challengeRound/ChallengeRoundUi.h"
#include "../../messageclient/protobuf/PlayerMessage.pb.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../ui/Active_ui/ActiveData.h"
#include "../../ui/Active_ui/ActiveLabelData.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/element/CActiveLabel.h"
#include "../../ui/npcscene/NpcTalkWindow.h"
#include "../../ui/challengeRound/ShowLevelUi.h"
#include "../../ui/extensions/PopupWindow.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"

IMPLEMENT_CLASS(SCTutorialGuoGuanZhanJiang1)
SCTutorialGuoGuanZhanJiang1::SCTutorialGuoGuanZhanJiang1()
{
}

SCTutorialGuoGuanZhanJiang1::~SCTutorialGuoGuanZhanJiang1()
{
}

void* SCTutorialGuoGuanZhanJiang1::createInstance()
{
	return new SCTutorialGuoGuanZhanJiang1() ;
}

void SCTutorialGuoGuanZhanJiang1::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);
			
			if (GameView::getInstance()->getMapInfo()->maptype() >= com::future::threekingdoms::server::transport::protocol::coliseum)
			{
				setState(STATE_END);
				return;
			}

			GuideMap * guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			if (guideMap)
			{
				if (!guideMap->getActionLayer()->isVisible())
				{
					setState(STATE_END);
					return;
				}
			}

			UIWidget * btn_activity = (UIWidget*)guideMap->getActionLayer()->getWidgetByName("btn_activeDegree");
			if (!btn_activity)
			{
				setState(STATE_END);
				return;
			}
			
			guideMap->addCCTutorialIndicator1(content.c_str(),btn_activity->getPosition(),CCTutorialIndicator::Direction_RU);
			guideMap->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(btn_activity) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGuoGuanZhanJiang1::init() {
}

void SCTutorialGuoGuanZhanJiang1::release() {
	CCLOG("SCTutorialGuoGuanZhanJiang1 release");
	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GuideMap * guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (guideMap)
	{
		guideMap->removeCCTutorialIndicator();
	}
}
///////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGuoGuanZhanJiang2)

SCTutorialGuoGuanZhanJiang2::SCTutorialGuoGuanZhanJiang2()
{
}

SCTutorialGuoGuanZhanJiang2::~SCTutorialGuoGuanZhanJiang2()
{
}

void* SCTutorialGuoGuanZhanJiang2::createInstance()
{
	return new SCTutorialGuoGuanZhanJiang2() ;
}

void SCTutorialGuoGuanZhanJiang2::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);

			if (GameView::getInstance()->getMapInfo()->maptype() >= com::future::threekingdoms::server::transport::protocol::coliseum)
			{
				setState(STATE_END);
				return;
			}

			GuideMap * guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			if (guideMap)
			{
				if (!guideMap->getActionLayer()->isVisible())
				{
					setState(STATE_END);
					return;
				}
			}

			ActiveUI * activeUI = (ActiveUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
			if (!activeUI)
			{
				setState(STATE_START);
				return;
			}

			activeUI->addCCTutorialIndicatorSingCopy(content.c_str(),ccp(0,0),CCTutorialIndicator::Direction_LD,2);
			activeUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(activeUI) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGuoGuanZhanJiang2::init() {
}

void SCTutorialGuoGuanZhanJiang2::release() {
	CCLOG("SCTutorialGuoGuanZhanJiang2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GuideMap * guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (guideMap)
	{
		guideMap->removeCCTutorialIndicator();
	}
}
/////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGuoGuanZhanJiang3)

SCTutorialGuoGuanZhanJiang3::SCTutorialGuoGuanZhanJiang3()
{
}

SCTutorialGuoGuanZhanJiang3::~SCTutorialGuoGuanZhanJiang3()
{
}

void* SCTutorialGuoGuanZhanJiang3::createInstance()
{
	return new SCTutorialGuoGuanZhanJiang3() ;
}

void SCTutorialGuoGuanZhanJiang3::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);

			if (GameView::getInstance()->getMapInfo()->maptype() >= com::future::threekingdoms::server::transport::protocol::coliseum)
			{
				setState(STATE_END);
				return;
			}

			GuideMap * guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			if (guideMap)
			{
				if (!guideMap->getActionLayer()->isVisible())
				{
					setState(STATE_END);
					return;
				}
			}

			NpcTalkWindow * npcUI = (NpcTalkWindow*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow);
			if (!npcUI)
			{
				setState(STATE_START);
				return;
			}

			npcUI->addCCTutorialIndicatorNpc(content.c_str(),ccp(0,0),CCTutorialIndicator::Direction_LU,1);
			npcUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(npcUI) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGuoGuanZhanJiang3::init() {
}

void SCTutorialGuoGuanZhanJiang3::release() {
	CCLOG("SCTutorialGuoGuanZhanJiang3() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	NpcTalkWindow * mpcui =(NpcTalkWindow *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow);
	if (mpcui)
	{
		mpcui->removeCCTutorialIndicator();
	}
}

/////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGuoGuanZhanJiang4)

SCTutorialGuoGuanZhanJiang4::SCTutorialGuoGuanZhanJiang4()
{
}

SCTutorialGuoGuanZhanJiang4::~SCTutorialGuoGuanZhanJiang4()
{
}

void* SCTutorialGuoGuanZhanJiang4::createInstance()
{
	return new SCTutorialGuoGuanZhanJiang4() ;
}

void SCTutorialGuoGuanZhanJiang4::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);

			if (GameView::getInstance()->getMapInfo()->maptype() >= com::future::threekingdoms::server::transport::protocol::coliseum)
			{
				setState(STATE_END);
				return;
			}
			ChallengeRoundUi * singCopy = (ChallengeRoundUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);
			if (!singCopy)
			{
				setState(STATE_START);
				return;
			}

			singCopy->addCCTutorialIndicator(content.c_str(),ccp(0,0),CCTutorialIndicator::Direction_LD);
			singCopy->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(singCopy) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGuoGuanZhanJiang4::init() {
}

void SCTutorialGuoGuanZhanJiang4::release() {
	CCLOG("SCTutorialGuoGuanZhanJiang3() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	ChallengeRoundUi * singCopy = (ChallengeRoundUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);
	if (singCopy)
	{
		singCopy->removeCCTutorialIndicator();
	}
}

/////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGuoGuanZhanJiang5)

SCTutorialGuoGuanZhanJiang5::SCTutorialGuoGuanZhanJiang5()
{
}

SCTutorialGuoGuanZhanJiang5::~SCTutorialGuoGuanZhanJiang5()
{
}

void* SCTutorialGuoGuanZhanJiang5::createInstance()
{
	return new SCTutorialGuoGuanZhanJiang5() ;
}

void SCTutorialGuoGuanZhanJiang5::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);

			if (GameView::getInstance()->getMapInfo()->maptype() >= com::future::threekingdoms::server::transport::protocol::coliseum)
			{
				setState(STATE_END);
				return;
			}

			ChallengeRoundUi * singCopy = (ChallengeRoundUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);
			ShowLevelUi * singCopyLevel = (ShowLevelUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopylevel);
			if (singCopy && singCopyLevel)
			{
				singCopyLevel->addCCTutorialIndicator(content.c_str(),ccp(0,0),CCTutorialIndicator::Direction_LD);
				singCopyLevel->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(singCopyLevel) ;
			}
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGuoGuanZhanJiang5::init() {
}

void SCTutorialGuoGuanZhanJiang5::release() {
	CCLOG("SCTutorialGuoGuanZhanJiang3() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	ChallengeRoundUi * singCopy = (ChallengeRoundUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);
	ShowLevelUi * singCopyLevel = (ShowLevelUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopylevel);
	if (singCopy && singCopyLevel)
	{
		singCopyLevel->removeCCTutorialIndicator();
	}
}

/////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGuoGuanZhanJiang6)

SCTutorialGuoGuanZhanJiang6::SCTutorialGuoGuanZhanJiang6()
{
}

SCTutorialGuoGuanZhanJiang6::~SCTutorialGuoGuanZhanJiang6()
{
}

void* SCTutorialGuoGuanZhanJiang6::createInstance()
{
	return new SCTutorialGuoGuanZhanJiang6() ;
}

void SCTutorialGuoGuanZhanJiang6::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);

			if (GameView::getInstance()->getMapInfo()->maptype() >= com::future::threekingdoms::server::transport::protocol::coliseum)
			{
				setState(STATE_END);
				return;
			}

			PopupWindow * popupUI = (PopupWindow*)GameView::getInstance()->getMainUIScene()->getChildByTag(101011);
			if (popupUI)
			{
				popupUI->addCCTutorialIndicator(content.c_str(),ccp(0,0),CCTutorialIndicator::Direction_LD);
				popupUI->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(popupUI->buttonSure) ;
			}
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGuoGuanZhanJiang6::init() {
}

void SCTutorialGuoGuanZhanJiang6::release() {
	CCLOG("SCTutorialGuoGuanZhanJiang3() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	PopupWindow * popupUI = (PopupWindow*)GameView::getInstance()->getMainUIScene()->getChildByTag(101011);
	if (popupUI )
	{
		popupUI->removeCCTutorialIndicator();
	}
}

/////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGuoGuanZhanJiang7)

SCTutorialGuoGuanZhanJiang7::SCTutorialGuoGuanZhanJiang7()
{
}

SCTutorialGuoGuanZhanJiang7::~SCTutorialGuoGuanZhanJiang7()
{
}

void* SCTutorialGuoGuanZhanJiang7::createInstance()
{
	return new SCTutorialGuoGuanZhanJiang7() ;
}

void SCTutorialGuoGuanZhanJiang7::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);
			
			GameSceneLayer* scene = GameView::getInstance()->getGameScene();
			if(scene == NULL)
				return;
			GameSceneState* pSceneState = dynamic_cast<GameSceneState*>(scene->getParent());
			if(pSceneState->isInLoadState())
				return;
			GuideMap * guideMap_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			if(GameView::getInstance()->getGameScene()->getParent())
				if (guideMap_ && GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::tower)
				{
					int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
					if (hookIndex == 0)
					{
						guideMap_->addCCTutorialIndicatorSingCopy(content.c_str(),guideMap_->getBtnHangUp()->getPosition(),CCTutorialIndicator::Direction_RU);
						guideMap_->registerScriptCommand(this->getId()) ; 
						this->setCommandHandler(guideMap_->getBtnHangUp()) ;
						setState(STATE_UPDATE);
					}else
					{
						setState(STATE_END);
					}
				}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGuoGuanZhanJiang7::init() {
}

void SCTutorialGuoGuanZhanJiang7::release() {
	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GuideMap * guideMap_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (guideMap_ != NULL)
		guideMap_->removeCCTutorialIndicator();
}