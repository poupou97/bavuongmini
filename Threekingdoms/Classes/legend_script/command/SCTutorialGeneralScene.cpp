
#include "../../utils/GameUtils.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutConfigure.h"
#include "SCTutorialGeneralScene.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/extensions/UITab.h"
#include "../../ui/generals_ui/GeneralsRecuriteUI.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../CCTutorialIndicator.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CFightWayBase.h"
#include "../../ui/generals_ui/GeneralsListBase.h"
#include "../../ui/generals_ui/GeneralsStrategiesUI.h"
#include "../../ui/generals_ui/GeneralsShortcutLayer.h"
#include "../../ui/generals_ui/generals_popup_ui/GeneralsSkillInfoUI.h"
#include "../../ui/generals_ui/SingleRecuriteResultUI.h"
#include "../../ui/generals_ui/generals_popup_ui/StrategiesUpgradeUI.h"

IMPLEMENT_CLASS(SCTutorialGeneralSceneStep1)

SCTutorialGeneralSceneStep1::SCTutorialGeneralSceneStep1()
{
}

SCTutorialGeneralSceneStep1::~SCTutorialGeneralSceneStep1()
{
}

void* SCTutorialGeneralSceneStep1::createInstance()
{
	return new SCTutorialGeneralSceneStep1() ;
}

void SCTutorialGeneralSceneStep1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep1()");

			std::string content = mPara.at(0);

			GeneralsUI * generalUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
			generalUI->addCCTutorialIndicator1(content.c_str(),generalUI->mainTab->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
			generalUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(generalUI->mainTab->getChildByTag(150)) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGeneralSceneStep1::init() {
	CCLOG("SCTutorialGeneralSceneStep1() init");
}

void SCTutorialGeneralSceneStep1::release() {
	CCLOG("SCTutorialTestStep1() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GeneralsUI * generalUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
	if(generalUI != NULL)
		generalUI->removeCCTutorialIndicator();
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGeneralSceneStep2)

	SCTutorialGeneralSceneStep2::SCTutorialGeneralSceneStep2()
{
}

SCTutorialGeneralSceneStep2::~SCTutorialGeneralSceneStep2()
{
}

void* SCTutorialGeneralSceneStep2::createInstance()
{
	return new SCTutorialGeneralSceneStep2() ;
}

void SCTutorialGeneralSceneStep2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialTestStep2()");

			std::string content = mPara.at(0);

			GeneralsRecuriteUI* generalsRecuriteUI = GeneralsUI::generalsRecuriteUI;
			generalsRecuriteUI->addCCTutorialIndicator(content.c_str(),generalsRecuriteUI->Button_wonderfulRecurit->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
			generalsRecuriteUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(generalsRecuriteUI->Button_wonderfulRecurit) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGeneralSceneStep2::init() {
	CCLOG("SCTutorialSkillSceneStep2() init");
}

void SCTutorialGeneralSceneStep2::release() {
	CCLOG("SCTutorialSkillSceneStep2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GeneralsRecuriteUI* generalsRecuriteUI = GeneralsUI::generalsRecuriteUI;
	if(generalsRecuriteUI != NULL)
		generalsRecuriteUI->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGeneralSceneStep3)

	SCTutorialGeneralSceneStep3::SCTutorialGeneralSceneStep3()
{
}

SCTutorialGeneralSceneStep3::~SCTutorialGeneralSceneStep3()
{
}

void* SCTutorialGeneralSceneStep3::createInstance()
{
	return new SCTutorialGeneralSceneStep3() ;
}

void SCTutorialGeneralSceneStep3::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep3()");

			std::string content = mPara.at(0);

			GeneralsUI * generalUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
			generalUI->addCCTutorialIndicator2(content.c_str(),generalUI->mainTab->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
			generalUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(generalUI->mainTab->getChildByTag(151)) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGeneralSceneStep3::init() {
	CCLOG("SCTutorialSkillSceneStep3() init");
}

void SCTutorialGeneralSceneStep3::release() {
	CCLOG("SCTutorialSkillSceneStep3() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GeneralsUI * generalUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
	if(generalUI != NULL)
		generalUI->removeCCTutorialIndicator();
}


///////////////////////////////第一个武将上阵按钮//////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGeneralSceneStep4)

	SCTutorialGeneralSceneStep4::SCTutorialGeneralSceneStep4()
{
}

SCTutorialGeneralSceneStep4::~SCTutorialGeneralSceneStep4()
{
}

void* SCTutorialGeneralSceneStep4::createInstance()
{
	return new SCTutorialGeneralSceneStep4() ;
}

void SCTutorialGeneralSceneStep4::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialGeneralSceneStep4()");

			std::string content = mPara.at(0);

			GeneralsListUI * generalListUI = (GeneralsListUI *)GeneralsUI::generalsListUI;
			if (GameView::getInstance()->generalBaseMsgList.size()<=0)
				return;

			GeneralBaseMsg * generalBaseMsg = GameView::getInstance()->generalBaseMsgList.at(0);
			if (generalBaseMsg->fightstatus() != GeneralsListUI::NoBattle)
			{
				setState(STATE_END);
			}
			else
			{
				generalListUI->addCCTutorialIndicator1(content.c_str(),generalListUI->generalList_tableView->getPosition(),CCTutorialIndicator::Direction_LU,true,true);
				//REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
				generalListUI->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(generalListUI->generalList_tableView) ;
				setState(STATE_UPDATE);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGeneralSceneStep4::init() {
	CCLOG("SCTutorialGeneralSceneStep4() init");
}

void SCTutorialGeneralSceneStep4::release() {
	CCLOG("SCTutorialGeneralSceneStep4() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GeneralsListUI * generalListUI = (GeneralsListUI *)GeneralsUI::generalsListUI;
	if(generalListUI != NULL)
	generalListUI->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGeneralSceneStep5)

	SCTutorialGeneralSceneStep5::SCTutorialGeneralSceneStep5()
{
}

SCTutorialGeneralSceneStep5::~SCTutorialGeneralSceneStep5()
{
}

void* SCTutorialGeneralSceneStep5::createInstance()
{
	return new SCTutorialGeneralSceneStep5() ;
}

void SCTutorialGeneralSceneStep5::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialGeneralSceneStep5()");

			std::string content = mPara.at(0);

			GeneralsListUI * generalListUI = (GeneralsListUI *)GeneralsUI::generalsListUI;
			if (GameView::getInstance()->generalBaseMsgList.size()<=0)
				return;

			GeneralBaseMsg * generalBaseMsg = GameView::getInstance()->generalBaseMsgList.at(0);
			if (generalBaseMsg->fightstatus() == GeneralsListUI::NoBattle)
			{
				setState(STATE_START);
				return;
			}

			if (generalBaseMsg->fightstatus() != GeneralsListUI::HoldTheLine)
			{
				setState(STATE_END);
			}
			else
			{
				generalListUI->addCCTutorialIndicator1(content.c_str(),generalListUI->generalList_tableView->getPosition(),CCTutorialIndicator::Direction_LU,true,false);
				//REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
				generalListUI->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(generalListUI->generalList_tableView) ;
				setState(STATE_UPDATE);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGeneralSceneStep5::init() {
	CCLOG("SCTutorialGeneralSceneStep5() init");
}

void SCTutorialGeneralSceneStep5::release() {
	CCLOG("SCTutorialGeneralSceneStep5() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GeneralsListUI * generalListUI = (GeneralsListUI *)GeneralsUI::generalsListUI;
	if(generalListUI != NULL)
		generalListUI->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGeneralSceneStep6)

	SCTutorialGeneralSceneStep6::SCTutorialGeneralSceneStep6()
{
}

SCTutorialGeneralSceneStep6::~SCTutorialGeneralSceneStep6()
{
}

void* SCTutorialGeneralSceneStep6::createInstance()
{
	return new SCTutorialGeneralSceneStep6() ;
}

void SCTutorialGeneralSceneStep6::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialGeneralSceneStep6()");

			std::string content = mPara.at(0);

			GeneralsUI * generalUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
			generalUI->addCCTutorialIndicator3(content.c_str(),generalUI->Button_close->getPosition(),CCTutorialIndicator::Direction_RU);
			//REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
			generalUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(generalUI->Button_close) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGeneralSceneStep6::init() {
	CCLOG("SCTutorialGeneralSceneStep6() init");
}

void SCTutorialGeneralSceneStep6::release() {
	CCLOG("SCTutorialGeneralSceneStep6() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GeneralsUI * generalUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
	if(generalUI != NULL)
		generalUI->removeCCTutorialIndicator();
}

 void SCTutorialGeneralSceneStep6::endCommand(CCObject* pSender)
 {
	Script::endCommand(pSender);

	release();
 }


 /////////////////////////////////////////////////////////////////////

 IMPLEMENT_CLASS(SCTutorialGeneralSceneStep7)

	 SCTutorialGeneralSceneStep7::SCTutorialGeneralSceneStep7()
 {
 }

 SCTutorialGeneralSceneStep7::~SCTutorialGeneralSceneStep7()
 {
 }

 void* SCTutorialGeneralSceneStep7::createInstance()
 {
	 return new SCTutorialGeneralSceneStep7() ;
 }

 void SCTutorialGeneralSceneStep7::update() {
	 switch (mState) {
	 case STATE_START:
		 {
			 CCLOG("excute SCTutorialTestStep7()");

			 std::string content = mPara.at(0);

			 GeneralsRecuriteUI* generalsRecuriteUI = GeneralsUI::generalsRecuriteUI;
			 generalsRecuriteUI->addCCTutorialIndicator(content.c_str(),generalsRecuriteUI->Button_goodRecurit->getPosition(),CCTutorialIndicator::Direction_LD);
			 //REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
			 generalsRecuriteUI->registerScriptCommand(this->getId()) ; 
			 this->setCommandHandler(generalsRecuriteUI->Button_goodRecurit) ;
			 setState(STATE_UPDATE);
		 }
		 break;

	 case STATE_UPDATE:
		 // wait the player to finish this tutorial
		 break;

	 case STATE_END:
		 break;

	 default:
		 break;
	 }
 }

 void SCTutorialGeneralSceneStep7::init() {
	 CCLOG("SCTutorialGeneralSceneStep7() init");
 }

 void SCTutorialGeneralSceneStep7::release() {
	 CCLOG("SCTutorialSkillSceneStep7() release");

	 if (!GameView::getInstance()->getGameScene())
		 return;

	 if (!GameView::getInstance()->getMainUIScene())
		 return;

	 GeneralsRecuriteUI* generalsRecuriteUI = GeneralsUI::generalsRecuriteUI;
	 if(generalsRecuriteUI != NULL)
		 generalsRecuriteUI->removeCCTutorialIndicator();
 }


 /////////////////////////////////////////////////////////////////////

 IMPLEMENT_CLASS(SCTutorialGeneralSceneStep8)

 SCTutorialGeneralSceneStep8::SCTutorialGeneralSceneStep8()
 {
 }

 SCTutorialGeneralSceneStep8::~SCTutorialGeneralSceneStep8()
 {
 }

 void* SCTutorialGeneralSceneStep8::createInstance()
 {
	 return new SCTutorialGeneralSceneStep8() ;
 }

 void SCTutorialGeneralSceneStep8::update() {
	 switch (mState) {
	 case STATE_START:
		 {
			 std::string content = mPara.at(0);

			 GeneralsListUI * generalListUI = (GeneralsListUI *)GeneralsUI::generalsListUI;
			 if (GameView::getInstance()->generalBaseMsgList.size()<=0)
				 return;

			 if (GameView::getInstance()->generalBaseMsgList.size() >= 3)
			 {
				 GeneralBaseMsg * generalBaseMsg = GameView::getInstance()->generalBaseMsgList.at(2);
				 if (generalBaseMsg->fightstatus() != GeneralsListUI::NoBattle)
				 {
					 setState(STATE_END);
				 }
				 else
				 {
					 generalListUI->addCCTutorialIndicator1(content.c_str(),generalListUI->generalList_tableView->getPosition(),CCTutorialIndicator::Direction_LU,false,true);
					 //REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
					 generalListUI->registerScriptCommand(this->getId()) ; 
					 this->setCommandHandler(generalListUI->generalList_tableView) ;
					 setState(STATE_UPDATE);
				 }
			 }
			 else
			 {
				 setState(STATE_END);
			 }
			 
		 }
		 break;

	 case STATE_UPDATE:
		 // wait the player to finish this tutorial
		 break;

	 case STATE_END:
		 break;

	 default:
		 break;
	 }
 }

 void SCTutorialGeneralSceneStep8::init() {
	 CCLOG("SCTutorialGeneralSceneStep9() init");
 }

 void SCTutorialGeneralSceneStep8::release() {
	 CCLOG("SCTutorialGeneralSceneStep9() release");

	 if (!GameView::getInstance()->getGameScene())
		 return;

	 if (!GameView::getInstance()->getMainUIScene())
		 return;

	 GeneralsListUI * generalListUI = (GeneralsListUI *)GeneralsUI::generalsListUI;
	 if(generalListUI != NULL)
		 generalListUI->removeCCTutorialIndicator();
 }


 /////////////////////////////////////////////////////////////////////

 IMPLEMENT_CLASS(SCTutorialGeneralSceneStep9)

	 SCTutorialGeneralSceneStep9::SCTutorialGeneralSceneStep9()
 {
 }

 SCTutorialGeneralSceneStep9::~SCTutorialGeneralSceneStep9()
 {
 }

 void* SCTutorialGeneralSceneStep9::createInstance()
 {
	 return new SCTutorialGeneralSceneStep9() ;
 }

 void SCTutorialGeneralSceneStep9::update() {
	 switch (mState) {
	 case STATE_START:
		 {
			 CCLOG("excute SCTutorialGeneralSceneStep9()");

			 std::string content = mPara.at(0);

			 GeneralsListUI * generalListUI = (GeneralsListUI *)GeneralsUI::generalsListUI;
			 if (GameView::getInstance()->generalBaseMsgList.size()<=0)
				 return;

			  if (GameView::getInstance()->generalBaseMsgList.size() >= 2)
			  {
				  GeneralBaseMsg * generalBaseMsg = GameView::getInstance()->generalBaseMsgList.at(1);
				  if (generalBaseMsg->fightstatus() == GeneralsListUI::NoBattle)
				  {
					  setState(STATE_START);
					  return;
				  }

				  if (generalBaseMsg->fightstatus() != GeneralsListUI::HoldTheLine)
				  {
					  setState(STATE_END);
				  }
				  else
				  {
					  generalListUI->addCCTutorialIndicator1(content.c_str(),generalListUI->generalList_tableView->getPosition(),CCTutorialIndicator::Direction_LU,false,false);
					  //REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
					  generalListUI->registerScriptCommand(this->getId()) ; 
					  this->setCommandHandler(generalListUI->generalList_tableView) ;
					  setState(STATE_UPDATE);
				  }
			  }
			  else if (GameView::getInstance()->generalBaseMsgList.size() == 1)
			  {
				  GeneralBaseMsg * generalBaseMsg = GameView::getInstance()->generalBaseMsgList.at(0);
				  if (generalBaseMsg->fightstatus() == GeneralsListUI::NoBattle)
				  {
					  setState(STATE_START);
					  return;
				  }

				  if (generalBaseMsg->fightstatus() != GeneralsListUI::HoldTheLine)
				  {
					  setState(STATE_END);
				  }
				  else
				  {
					  generalListUI->addCCTutorialIndicator1(content.c_str(),generalListUI->generalList_tableView->getPosition(),CCTutorialIndicator::Direction_LU,false,false);
					  //REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
					  generalListUI->registerScriptCommand(this->getId()) ; 
					  this->setCommandHandler(generalListUI->generalList_tableView) ;
					  setState(STATE_UPDATE);
				  }
			  }
			  else
			  {
				  setState(STATE_END);
			  }
			
		 }
		 break;

	 case STATE_UPDATE:
		 // wait the player to finish this tutorial
		 break;

	 case STATE_END:
		 break;

	 default:
		 break;
	 }
 }

 void SCTutorialGeneralSceneStep9::init() {
	 CCLOG("SCTutorialGeneralSceneStep9() init");
 }

 void SCTutorialGeneralSceneStep9::release() {
	 CCLOG("SCTutorialGeneralSceneStep9() release");

	 if (!GameView::getInstance()->getGameScene())
		 return;

	 if (!GameView::getInstance()->getMainUIScene())
		 return;

	 GeneralsListUI * generalListUI = (GeneralsListUI *)GeneralsUI::generalsListUI;
	 if(generalListUI != NULL)
		 generalListUI->removeCCTutorialIndicator();
 }

 /////////////////////////////////////////////////////////////////////

 IMPLEMENT_CLASS(SCTutorialGeneralSceneStep10)

	 SCTutorialGeneralSceneStep10::SCTutorialGeneralSceneStep10()
 {
 }

 SCTutorialGeneralSceneStep10::~SCTutorialGeneralSceneStep10()
 {
 }

 void* SCTutorialGeneralSceneStep10::createInstance()
 {
	 return new SCTutorialGeneralSceneStep10() ;
 }

 void SCTutorialGeneralSceneStep10::update() {
	 switch (mState) {
	 case STATE_START:
		 {
			 CCLOG("excute SCTutorialGeneralSceneStep10()");

			 std::string content = mPara.at(0);

			 GeneralsListUI * generalListUI = (GeneralsListUI *)GeneralsUI::generalsListUI;
			 if (GameView::getInstance()->generalBaseMsgList.size() > 0)
			 {
				 if (generalListUI->Layer_generalsList->getChildByTag(1000) != NULL)
				 {
					 GeneralsShortcutLayer * temp = (GeneralsShortcutLayer *)generalListUI->Layer_generalsList->getChildByTag(1000);
					 temp->addCCTutorialIndicator(content.c_str(),temp->layer_shortcut->getChildByTag(300)->getPosition(),CCTutorialIndicator::Direction_LD);
					 //REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
					 temp->registerScriptCommand(this->getId()) ; 
					 this->setCommandHandler(temp->layer_shortcut->getChildByTag(300)) ;
					 setState(STATE_UPDATE);
				 }
				 else
				 {
					 setState(STATE_END);
				 }
			 }
			 else
			 {
				 setState(STATE_END);
			 }

		 }
		 break;

	 case STATE_UPDATE:
		 // wait the player to finish this tutorial
		 break;

	 case STATE_END:
		 break;

	 default:
		 break;
	 }
 }

 void SCTutorialGeneralSceneStep10::init() {
	 CCLOG("SCTutorialGeneralSceneStep10() init");
 }

 void SCTutorialGeneralSceneStep10::release() {
	 CCLOG("SCTutorialGeneralSceneStep10() release");

	 if (!GameView::getInstance()->getGameScene())
		 return;

	 if (!GameView::getInstance()->getMainUIScene())
		 return;

	 GeneralsListUI * generalListUI = (GeneralsListUI *)GeneralsUI::generalsListUI;
	 if(generalListUI != NULL)
	 {
		 if (generalListUI->Layer_generalsList->getChildByTag(1000) != NULL)
		 {
			 GeneralsShortcutLayer * temp = (GeneralsShortcutLayer *)generalListUI->Layer_generalsList->getChildByTag(1000);
			 temp->removeCCTutorialIndicator();
		 }
	 }
		
 }


 /////////////////////////////////////////////////////////////////////

 IMPLEMENT_CLASS(SCTutorialGeneralSceneStep11)

	 SCTutorialGeneralSceneStep11::SCTutorialGeneralSceneStep11()
 {
 }

 SCTutorialGeneralSceneStep11::~SCTutorialGeneralSceneStep11()
 {
 }

 void* SCTutorialGeneralSceneStep11::createInstance()
 {
	 return new SCTutorialGeneralSceneStep11() ;
 }

 void SCTutorialGeneralSceneStep11::update() {
	 switch (mState) {
	 case STATE_START:
		 {
			 CCLOG("excute SCTutorialGeneralSceneStep11()");

			 std::string content = mPara.at(0);

			 GeneralsSkillInfoUI * generalSkillInfoUI = (GeneralsSkillInfoUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSkillInfoUI);
			 generalSkillInfoUI->addCCTutorialIndicator1(content.c_str(),generalSkillInfoUI->btn_superSkill->getPosition(),CCTutorialIndicator::Direction_LD);
			 //REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			 generalSkillInfoUI->registerScriptCommand(this->getId()) ; 
			 this->setCommandHandler(generalSkillInfoUI->btn_superSkill) ;
			 setState(STATE_UPDATE);
		 }
		 break;

	 case STATE_UPDATE:
		 // wait the player to finish this tutorial
		 break;

	 case STATE_END:
		 break;

	 default:
		 break;
	 }
 }

 void SCTutorialGeneralSceneStep11::init() {
	 CCLOG("SCTutorialGeneralSceneStep11() init");
 }

 void SCTutorialGeneralSceneStep11::release() {
	 CCLOG("SCTutorialGeneralSceneStep11() release");

	 if (!GameView::getInstance()->getGameScene())
		 return;

	 if (!GameView::getInstance()->getMainUIScene())
		 return;

	 GeneralsSkillInfoUI * generalSkillInfoUI = (GeneralsSkillInfoUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSkillInfoUI);
	 if(generalSkillInfoUI != NULL)
	 {
		generalSkillInfoUI->removeCCTutorialIndicator();
	 }

 }


 /////////////////////////////////////////////////////////////////////

 IMPLEMENT_CLASS(SCTutorialGeneralSceneStep12)

	 SCTutorialGeneralSceneStep12::SCTutorialGeneralSceneStep12()
 {
 }

 SCTutorialGeneralSceneStep12::~SCTutorialGeneralSceneStep12()
 {
 }

 void* SCTutorialGeneralSceneStep12::createInstance()
 {
	 return new SCTutorialGeneralSceneStep12() ;
 }

 void SCTutorialGeneralSceneStep12::update() {
	 switch (mState) {
	 case STATE_START:
		 {
			 CCLOG("excute SCTutorialSkillSceneStep3()");

			 std::string content = mPara.at(0);

// 			
// 			 while (this) {
// 				setState(STATE_START);
// 
// 				if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSingleRecuriteResultUI))
// 					break;
// 			 }

			 SingleRecuriteResultUI * singleRecuriteResultUI = (SingleRecuriteResultUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSingleRecuriteResultUI);
			 if(singleRecuriteResultUI == NULL)
			 {
				 setState(STATE_START);
				 return;
			 }

			 singleRecuriteResultUI->addCCTutorialIndicator1(content.c_str(),singleRecuriteResultUI->Btn_close->getPosition(),CCTutorialIndicator::Direction_RU);
			 //REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
			 singleRecuriteResultUI->registerScriptCommand(this->getId()) ; 
			 this->setCommandHandler(singleRecuriteResultUI->Btn_close) ;
			 setState(STATE_UPDATE);
		 }
		 break;

	 case STATE_UPDATE:
		 // wait the player to finish this tutorial
		 break;

	 case STATE_END:
		 break;

	 default:
		 break;
	 }
 }

 void SCTutorialGeneralSceneStep12::init() {
	 CCLOG("SCTutorialGeneralSceneStep12() init");
 }

 void SCTutorialGeneralSceneStep12::release() {
	 CCLOG("SCTutorialGeneralSceneStep12() release");

	 if (!GameView::getInstance()->getGameScene())
		 return;

	 if (!GameView::getInstance()->getMainUIScene())
		 return;

	 SingleRecuriteResultUI * singleRecuriteResultUI = (SingleRecuriteResultUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSingleRecuriteResultUI);
	 if(singleRecuriteResultUI != NULL)
		 singleRecuriteResultUI->removeCCTutorialIndicator();
 }


 /////////////////////////////////////////////////////////////////////

 IMPLEMENT_CLASS(SCTutorialGeneralSceneStep13)

	 SCTutorialGeneralSceneStep13::SCTutorialGeneralSceneStep13()
 {
 }

 SCTutorialGeneralSceneStep13::~SCTutorialGeneralSceneStep13()
 {
 }

 void* SCTutorialGeneralSceneStep13::createInstance()
 {
	 return new SCTutorialGeneralSceneStep13() ;
 }

 void SCTutorialGeneralSceneStep13::update() {
	 switch (mState) {
	 case STATE_START:
		 {
			 CCLOG("excute SCTutorialGeneralSceneStep13()");

			 std::string content = mPara.at(0);

			 GeneralsUI * generalUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
			 generalUI->addCCTutorialIndicator4(content.c_str(),generalUI->mainTab->getPosition(),CCTutorialIndicator::Direction_RD);
			 //REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
			 generalUI->registerScriptCommand(this->getId()) ; 
			 this->setCommandHandler(generalUI->mainTab->getChildByTag(153)) ;
			 setState(STATE_UPDATE);
		 }
		 break;

	 case STATE_UPDATE:
		 // wait the player to finish this tutorial
		 break;

	 case STATE_END:
		 break;

	 default:
		 break;
	 }
 }

 void SCTutorialGeneralSceneStep13::init() {
	 CCLOG("SCTutorialGeneralSceneStep13() init");
 }

 void SCTutorialGeneralSceneStep13::release() {
	 CCLOG("SCTutorialGeneralSceneStep13() release");

	 if (!GameView::getInstance()->getGameScene())
		 return;

	 if (!GameView::getInstance()->getMainUIScene())
		 return;

	 GeneralsUI * generalUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
	 if(generalUI != NULL)
		 generalUI->removeCCTutorialIndicator();
 }



 /////////////////////////////////////////////////////////////////////

 IMPLEMENT_CLASS(SCTutorialGeneralSceneStep14)

	 SCTutorialGeneralSceneStep14::SCTutorialGeneralSceneStep14()
 {
 }

 SCTutorialGeneralSceneStep14::~SCTutorialGeneralSceneStep14()
 {
 }

 void* SCTutorialGeneralSceneStep14::createInstance()
 {
	 return new SCTutorialGeneralSceneStep14() ;
 }

 void SCTutorialGeneralSceneStep14::update() {
	 switch (mState) {
	 case STATE_START:
		 {
			 CCLOG("excute SCTutorialGeneralSceneStep14()");

			 std::string content = mPara.at(0);

			 GeneralsUI * generalUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
			 if (!generalUI)
			 {
				 setState(STATE_START);
				 return;
			 }
				 
			 if (GeneralsUI::generalsStrategiesUI->generalsStrategiesList.size()>0)
			 {
				 if (GeneralsUI::generalsStrategiesUI->generalsStrategiesList.at(0)->level()>0)
					 setState(STATE_END);

				 GeneralsUI::generalsStrategiesUI->addCCTutorialIndicator1(content.c_str(),GeneralsUI::generalsStrategiesUI->btn_levelUpStrategies->getPosition(),CCTutorialIndicator::Direction_RD);
				 //REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
				 GeneralsUI::generalsStrategiesUI->registerScriptCommand(this->getId()) ; 
				 this->setCommandHandler(GeneralsUI::generalsStrategiesUI->btn_levelUpStrategies) ;
				 setState(STATE_UPDATE);
			 }
			 else
			 {
				 setState(STATE_START);
			 }
		 }
		 break;

	 case STATE_UPDATE:
		 // wait the player to finish this tutorial
		 break;

	 case STATE_END:
		 break;

	 default:
		 break;
	 }
 }

 void SCTutorialGeneralSceneStep14::init() {
	 CCLOG("SCTutorialGeneralSceneStep14() init");
 }

 void SCTutorialGeneralSceneStep14::release() {
	 CCLOG("SCTutorialGeneralSceneStep14() release");

	 if (!GameView::getInstance()->getGameScene())
		 return;

	 if (!GameView::getInstance()->getMainUIScene())
		 return;

	 GeneralsUI * generalUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
	 if(generalUI != NULL)
		 GeneralsUI::generalsStrategiesUI->removeCCTutorialIndicator();
 }



 /////////////////////////////////////////////////////////////////////

 IMPLEMENT_CLASS(SCTutorialGeneralSceneStep15)

	 SCTutorialGeneralSceneStep15::SCTutorialGeneralSceneStep15()
 {
 }

 SCTutorialGeneralSceneStep15::~SCTutorialGeneralSceneStep15()
 {
 }

 void* SCTutorialGeneralSceneStep15::createInstance()
 {
	 return new SCTutorialGeneralSceneStep15() ;
 }

 void SCTutorialGeneralSceneStep15::update() {
	 switch (mState) {
	 case STATE_START:
		 {
			 CCLOG("excute SCTutorialGeneralSceneStep15()");

			 std::string content = mPara.at(0);

			 GeneralsUI * generalUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
			 if (!generalUI)
			 {
				 setState(STATE_START);
				 return;
			 }

			 if (GeneralsUI::generalsStrategiesUI->generalsStrategiesList.size()>0)
			 {
				 if (GeneralsUI::generalsStrategiesUI->generalsStrategiesList.at(0)->level()<0)
					 setState(STATE_END);

				 GeneralsUI::generalsStrategiesUI->addCCTutorialIndicator1(content.c_str(),GeneralsUI::generalsStrategiesUI->btn_openStrategies->getPosition(),CCTutorialIndicator::Direction_RD);
				 //REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
				 GeneralsUI::generalsStrategiesUI->registerScriptCommand(this->getId()) ; 
				 this->setCommandHandler(GeneralsUI::generalsStrategiesUI->btn_openStrategies);
				 setState(STATE_UPDATE);
			 }
			 else
			 {
				 setState(STATE_START);
			 }
		 }
		 break;

	 case STATE_UPDATE:
		 // wait the player to finish this tutorial
		 break;

	 case STATE_END:
		 break;

	 default:
		 break;
	 }
 }

 void SCTutorialGeneralSceneStep15::init() {
	 CCLOG("SCTutorialGeneralSceneStep15() init");
 }

 void SCTutorialGeneralSceneStep15::release() {
	 CCLOG("SCTutorialGeneralSceneStep15() release");

	 if (!GameView::getInstance()->getGameScene())
		 return;

	 if (!GameView::getInstance()->getMainUIScene())
		 return;

	 GeneralsUI * generalUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
	 if(generalUI != NULL)
		 GeneralsUI::generalsStrategiesUI->removeCCTutorialIndicator();
 }







 /////////////////////////////////////////////////////////////////////

 IMPLEMENT_CLASS(SCTutorialStrategiesUpgradeStep1)

	 SCTutorialStrategiesUpgradeStep1::SCTutorialStrategiesUpgradeStep1()
 {
 }

 SCTutorialStrategiesUpgradeStep1::~SCTutorialStrategiesUpgradeStep1()
 {
 }

 void* SCTutorialStrategiesUpgradeStep1::createInstance()
 {
	 return new SCTutorialStrategiesUpgradeStep1() ;
 }

 void SCTutorialStrategiesUpgradeStep1::update() {
	 switch (mState) {
	 case STATE_START:
		 {
			 CCLOG("excute SCTutorialGeneralSceneStep14()");

			 std::string content = mPara.at(0);

			 StrategiesUpgradeUI * strategiesUpgradeUI = (StrategiesUpgradeUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesUpGradeUI);
			 if (!strategiesUpgradeUI)
			 {
				 setState(STATE_START);
				 return;
			 }

			strategiesUpgradeUI->addCCTutorialIndicator1(content.c_str(),strategiesUpgradeUI->Button_upgrade->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
			strategiesUpgradeUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(strategiesUpgradeUI->Button_upgrade) ;
			setState(STATE_UPDATE);
		 }
		 break;

	 case STATE_UPDATE:
		 // wait the player to finish this tutorial
		 break;

	 case STATE_END:
		 break;

	 default:
		 break;
	 }
 }

 void SCTutorialStrategiesUpgradeStep1::init() {
	 CCLOG("SCTutorialStrategiesUpgradeStep1() init");
 }

 void SCTutorialStrategiesUpgradeStep1::release() {
	 CCLOG("SCTutorialStrategiesUpgradeStep1() release");

	 if (!GameView::getInstance()->getGameScene())
		 return;

	 if (!GameView::getInstance()->getMainUIScene())
		 return;

	 StrategiesUpgradeUI * strategiesUpgradeUI =  (StrategiesUpgradeUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesUpGradeUI);
	 if(strategiesUpgradeUI != NULL)
		 strategiesUpgradeUI->removeCCTutorialIndicator();
 }




 /////////////////////////////////////////////////////////////////////

 IMPLEMENT_CLASS(SCTutorialStrategiesUpgradeStep2)

SCTutorialStrategiesUpgradeStep2::SCTutorialStrategiesUpgradeStep2()
 {
 }

 SCTutorialStrategiesUpgradeStep2::~SCTutorialStrategiesUpgradeStep2()
 {
 }

 void* SCTutorialStrategiesUpgradeStep2::createInstance()
 {
	 return new SCTutorialStrategiesUpgradeStep2() ;
 }

 void SCTutorialStrategiesUpgradeStep2::update() {
	 switch (mState) {
	 case STATE_START:
		 {
			 CCLOG("excute SCTutorialStrategiesUpgradeStep2()");

			 std::string content = mPara.at(0);

			 StrategiesUpgradeUI * strategiesUpgradeUI =  (StrategiesUpgradeUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesUpGradeUI);
			 if (!strategiesUpgradeUI)
			 {
				 setState(STATE_START);
				 return;
			 }

			strategiesUpgradeUI->addCCTutorialIndicator2(content.c_str(),strategiesUpgradeUI->Button_close->getPosition(),CCTutorialIndicator::Direction_RU);
			strategiesUpgradeUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(strategiesUpgradeUI->Button_close) ;
			setState(STATE_UPDATE);
		 }
		 break;

	 case STATE_UPDATE:
		 // wait the player to finish this tutorial
		 break;

	 case STATE_END:
		 break;

	 default:
		 break;
	 }
 }

 void SCTutorialStrategiesUpgradeStep2::init() {
	 CCLOG("SCTutorialStrategiesUpgradeStep2() init");
 }

 void SCTutorialStrategiesUpgradeStep2::release() {
	 CCLOG("SCTutorialStrategiesUpgradeStep2() release");

	 if (!GameView::getInstance()->getGameScene())
		 return;

	 if (!GameView::getInstance()->getMainUIScene())
		 return;

	 StrategiesUpgradeUI * strategiesUpgradeUI =  (StrategiesUpgradeUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesUpGradeUI);
	 if(strategiesUpgradeUI != NULL)
		 strategiesUpgradeUI->removeCCTutorialIndicator();
 }

