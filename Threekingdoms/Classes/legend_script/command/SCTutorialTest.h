#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALTEST_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALTEST_H_

#include "../Script.h"

/**
 * 测试教学
 *
 * @author zhaogang
 * @date 2014-1-3
 */
class SCTutorialTestStep1 : public Script {
private:
    DECLARE_CLASS(SCTutorialTestStep1)

public:
	SCTutorialTestStep1();
	virtual ~SCTutorialTestStep1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////////////////

class SCTutorialTestStep2 : public Script {
private:
    DECLARE_CLASS(SCTutorialTestStep2)

public:
	SCTutorialTestStep2();
	virtual ~SCTutorialTestStep2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif