#include "SCTutorialRobotUIStudy.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/HeadMenu.h"
#include "../../GameView.h"
#include "../../ui/robot_ui/RobotMainUI.h"
#include "../../ui/extensions/UITab.h"
#include "../CCTutorialIndicator.h"


IMPLEMENT_CLASS(SCTutorialRobotStudyStep1)

SCTutorialRobotStudyStep1::SCTutorialRobotStudyStep1()
{
}

SCTutorialRobotStudyStep1::~SCTutorialRobotStudyStep1()
{
}

void* SCTutorialRobotStudyStep1::createInstance()
{
	return new SCTutorialRobotStudyStep1() ;
}

void SCTutorialRobotStudyStep1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep1()");

			std::string content = mPara.at(0);
			
			HeadMenu * headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
			headMenu->addCCTutorialIndicatorRD(content.c_str(),headMenu->Button_hangUp->getPosition(),CCTutorialIndicator::Direction_RD);
			headMenu->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(headMenu->Button_hangUp) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialRobotStudyStep1::init() {
	CCLOG("SCTutorialRobotStep1() init");
}

void SCTutorialRobotStudyStep1::release() {
	CCLOG("SCTutorialRobotStep1() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	HeadMenu * headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
	if (headMenu != NULL)
		headMenu->removeCCTutorialIndicator();
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialRobotStudyStep2)

SCTutorialRobotStudyStep2::SCTutorialRobotStudyStep2()
{
}

SCTutorialRobotStudyStep2::~SCTutorialRobotStudyStep2()
{
}

void* SCTutorialRobotStudyStep2::createInstance()
{
	return new SCTutorialRobotStudyStep2() ;
}

void SCTutorialRobotStudyStep2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialTestStep2()");

			std::string content = mPara.at(0);

			RobotMainUI * Robotui = (RobotMainUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
			if (Robotui != NULL)
			{
				Robotui->addCCTutorialIndicator1(content.c_str(),Robotui->robotTab->getPosition(),CCTutorialIndicator::Direction_LU);
				Robotui->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(Robotui->robotTab->getObjectByIndex(1)) ;
				setState(STATE_UPDATE);
			}else
			{
				setState(STATE_END);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialRobotStudyStep2::init() {
	CCLOG("SCTutorialSkillSceneStep2() init");
}

void SCTutorialRobotStudyStep2::release() {
	CCLOG("SCTutorialSkillSceneStep2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	RobotMainUI * Robotui = (RobotMainUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
	if(Robotui != NULL)
		Robotui->removeCCTutorialIndicator();
}

/////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialRobotStudyStep3)

SCTutorialRobotStudyStep3::SCTutorialRobotStudyStep3()
{
}

SCTutorialRobotStudyStep3::~SCTutorialRobotStudyStep3()
{
}

void* SCTutorialRobotStudyStep3::createInstance()
{
	return new SCTutorialRobotStudyStep3() ;
}

void SCTutorialRobotStudyStep3::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialRobotStudyStep3()");

			std::string content = mPara.at(0);
			RobotMainUI * Robotui = (RobotMainUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
			if (Robotui != NULL)
			{
				Robotui->addCCTutorialIndicator2(content.c_str(),Robotui->robotTab->getPosition(),CCTutorialIndicator::Direction_LU);
				Robotui->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(Robotui->robotTab->getObjectByIndex(2)) ;
				setState(STATE_UPDATE);
			}else
			{
				setState(STATE_END);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialRobotStudyStep3::init() {
	CCLOG("SCTutorialRobotStudyStep3() init");
}

void SCTutorialRobotStudyStep3::release() {
	CCLOG("SCTutorialRobotStudyStep3() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	RobotMainUI * Robotui = (RobotMainUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
	if (Robotui != NULL)
	{
		Robotui->removeCCTutorialIndicator();
	}
}

/////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialRobotStudyStep4)

SCTutorialRobotStudyStep4::SCTutorialRobotStudyStep4()
{
}

SCTutorialRobotStudyStep4::~SCTutorialRobotStudyStep4()
{
}

void* SCTutorialRobotStudyStep4::createInstance()
{
	return new SCTutorialRobotStudyStep4() ;
}

void SCTutorialRobotStudyStep4::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialRobotStudyStep4()");

			std::string content = mPara.at(0);
			RobotMainUI * Robotui = (RobotMainUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
			if (Robotui != NULL)
			{
				Robotui->addCCTutorialIndicatorClose(content.c_str(),Robotui->btn_Exit->getPosition(),CCTutorialIndicator::Direction_RU);
				Robotui->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(Robotui->btn_Exit) ;
				setState(STATE_UPDATE);
			}else
			{
				setState(STATE_END);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialRobotStudyStep4::init() {
	CCLOG("SCTutorialRobotStudyStep4() init");
}

void SCTutorialRobotStudyStep4::release() {
	CCLOG("SCTutorialRobotStudyStep4() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	RobotMainUI * Robotui = (RobotMainUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
	if (Robotui !=NULL)
	{
		Robotui->removeCCTutorialIndicator();
	}
}

void SCTutorialRobotStudyStep4::endCommand( CCObject* pSender )
{
	Script::endCommand(pSender);
	release();
}
