#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALSKILLSCENE_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALSKILLSCENE_H_

#include "../Script.h"

/**
 *技能界面教学
 *
 * @author yangjun
 * @date 2014-1-6
 */


////////////////////////选技能/////////////////////////////////////
class SCTutorialSkillSceneStep1 : public Script {
private:
    DECLARE_CLASS(SCTutorialSkillSceneStep1)

public:
	SCTutorialSkillSceneStep1();
	virtual ~SCTutorialSkillSceneStep1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////选技能(设置无双技能)/////////////////////////////////////
class SCTutorialSkillSceneStepSetMusou1 : public Script {
private:
	DECLARE_CLASS(SCTutorialSkillSceneStepSetMusou1)

public:
	SCTutorialSkillSceneStepSetMusou1();
	virtual ~SCTutorialSkillSceneStepSetMusou1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////学习/////////////////////////////////

class SCTutorialSkillSceneStep2 : public Script {
private:
    DECLARE_CLASS(SCTutorialSkillSceneStep2)

public:
	SCTutorialSkillSceneStep2();
	virtual ~SCTutorialSkillSceneStep2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////快捷//////////////////////////////

class SCTutorialSkillSceneStep3 : public Script {
private:
	DECLARE_CLASS(SCTutorialSkillSceneStep3)

public:
	SCTutorialSkillSceneStep3();
	virtual ~SCTutorialSkillSceneStep3();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////选位置//////////////////////////////

class SCTutorialSkillSceneStep4 : public Script {
private:
	DECLARE_CLASS(SCTutorialSkillSceneStep4)

public:
	SCTutorialSkillSceneStep4();
	virtual ~SCTutorialSkillSceneStep4();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////关闭装配//////////////////////////////

class SCTutorialSkillSceneStep5 : public Script {
private:
	DECLARE_CLASS(SCTutorialSkillSceneStep5)

public:
	SCTutorialSkillSceneStep5();
	virtual ~SCTutorialSkillSceneStep5();

	virtual bool isBlockFunction() { return true; };

	virtual void endCommand(CCObject* pSender);

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////点快捷栏使用技能//////////////////////////////

class SCTutorialSkillSceneStep6 : public Script {
private:
	DECLARE_CLASS(SCTutorialSkillSceneStep6)

public:
	SCTutorialSkillSceneStep6();
	virtual ~SCTutorialSkillSceneStep6();

	virtual bool isBlockFunction() { return true; };

	virtual void endCommand(CCObject* pSender);

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


/////////////////////龙魂/////////////////////////////////

class SCTutorialSkillSceneStep7 : public Script {
private:
	DECLARE_CLASS(SCTutorialSkillSceneStep7)

public:
	SCTutorialSkillSceneStep7();
	virtual ~SCTutorialSkillSceneStep7();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////关闭技能界面//////////////////////////////

class SCTutorialSkillSceneStep8 : public Script {
private:
	DECLARE_CLASS(SCTutorialSkillSceneStep8)

public:
	SCTutorialSkillSceneStep8();
	virtual ~SCTutorialSkillSceneStep8();

	virtual bool isBlockFunction() { return true; };

	virtual void endCommand(CCObject* pSender);

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


////////////////////////选择龙魂标签//////////////////////////////

class SCTutorialSkillSceneStep9 : public Script {
private:
	DECLARE_CLASS(SCTutorialSkillSceneStep9)

public:
	SCTutorialSkillSceneStep9();
	virtual ~SCTutorialSkillSceneStep9();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////推荐设置//////////////////////////////

class SCTutorialSkillSceneStep10 : public Script {
private:
	DECLARE_CLASS(SCTutorialSkillSceneStep10)

public:
	SCTutorialSkillSceneStep10();
	virtual ~SCTutorialSkillSceneStep10();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


////////////////////////点快捷栏使用龙魂//////////////////////////////

class SCTutorialSkillSceneStep11 : public Script {
private:
	DECLARE_CLASS(SCTutorialSkillSceneStep11)

public:
	SCTutorialSkillSceneStep11();
	virtual ~SCTutorialSkillSceneStep11();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();

private:
	long long beginTime;
};


////////////////////////关闭技能界面//////////////////////////////

class SCTutorialSkillSceneStep12 : public Script {
private:
DECLARE_CLASS(SCTutorialSkillSceneStep12)

public:
	SCTutorialSkillSceneStep12();
	virtual ~SCTutorialSkillSceneStep12();

	virtual bool isBlockFunction() { return true; };

	virtual void endCommand(CCObject* pSender);

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif