#include "SCWait.h"

#include "../../utils/GameUtils.h"

IMPLEMENT_CLASS(SCWait)

SCWait::SCWait()
{
}

SCWait::~SCWait()
{
}

void* SCWait::createInstance()
{
	return new SCWait() ;
}

void SCWait::update() {
	switch (mState) {
	case STATE_START:
		{
		CCLOG("excute wait()");

		setStartTime(GameUtils::millisecondNow());
		CCAssert(mPara.size() == 1, "wrong parameter");
		std::string time = mPara.at(0);
		setDelayTime(std::atoi(time.c_str()));
		setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		{
		//CCLOG("waiting...");
		if (goEnd()) {
			CCLOG("wait() end");
			setState(STATE_END);
		}
		}
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCWait::init() {
}

void SCWait::release() {
}
