#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALROBOTSTUDY_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALROBOTSTUDY_H_

#include "../Script.h"

/**
 * 挂机界面教学
 *
 */
class SCTutorialRobotStudyStep1 : public Script {
private:
    DECLARE_CLASS(SCTutorialRobotStudyStep1)

public:
	SCTutorialRobotStudyStep1();
	virtual ~SCTutorialRobotStudyStep1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////////////////

class SCTutorialRobotStudyStep2 : public Script {
private:
    DECLARE_CLASS(SCTutorialRobotStudyStep2)

public:
	SCTutorialRobotStudyStep2();
	virtual ~SCTutorialRobotStudyStep2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};
////////////////////////////////////////////////////////////

class SCTutorialRobotStudyStep3 : public Script {
private:
	DECLARE_CLASS(SCTutorialRobotStudyStep3)

public:
	SCTutorialRobotStudyStep3();
	virtual ~SCTutorialRobotStudyStep3();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};
////////////////////////////////////////////////
class SCTutorialRobotStudyStep4 : public Script {
private:
	DECLARE_CLASS(SCTutorialRobotStudyStep4)

public:
	SCTutorialRobotStudyStep4();
	virtual ~SCTutorialRobotStudyStep4();

	virtual bool isBlockFunction() { return true; };

	virtual void endCommand(CCObject* pSender);

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};
#endif