#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALRECURITESCENE_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALRECURITESCENE_H_

#include "../Script.h"

/**
 * �佫��ļ��ѧ
 * after recurite, click the "continue" button
 *
 * @author yangjun
 * @date 2014-8-12
 */

class SCTutorialRecuriteScene : public Script {
private:
	DECLARE_CLASS(SCTutorialRecuriteScene)

public:
	SCTutorialRecuriteScene();
	virtual ~SCTutorialRecuriteScene();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif
