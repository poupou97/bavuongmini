#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALMAINSCENE_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALMAINSCENE_H_

#include "../Script.h"

/**
 *主界面教学
 *
 * @author yangjun
 * @date 2014-1-7
 */

//////////////////////点击人物头像(弹出菜单)////////////////////////
class SCTutorialMainScene1 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene1)

public:
	SCTutorialMainScene1();
	virtual ~SCTutorialMainScene1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////点击人物头像(收回菜单)////////////////////////
class SCTutorialMainScene1Close : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene1Close)

public:
	SCTutorialMainScene1Close();
	virtual ~SCTutorialMainScene1Close();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////点击人物头像(第一个武将教学)////////////////////////
// class SCTutorialMainSceneGeneral1 : public Script {
// private:
	//DECLARE_CLASS(SCTutorialMainSceneGeneral1)
// 
// public:
// 	SCTutorialMainSceneGeneral1();
// 	virtual ~SCTutorialMainSceneGeneral1();
// 
// 	virtual bool isBlockFunction() { return true; };
// 
// 	static void* createInstance() ;
// 
// 	virtual void update();
// 
// 	virtual void init();
// 
// 	virtual void release();
// };

//////////////////////点击人物头像(设置无双技能时)////////////////////////
class SCTutorialMainSceneSetMusou1 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneSetMusou1)

public:
	SCTutorialMainSceneSetMusou1();
	virtual ~SCTutorialMainSceneSetMusou1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////点击菜单栏上的技能/////////////////////////////

class SCTutorialMainScene2 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene2)

public:
	SCTutorialMainScene2();
	virtual ~SCTutorialMainScene2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


/////////////////////////点击菜单栏上的技能(设置无双技能)/////////////////////////////

class SCTutorialMainSceneSetMusou2 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneSetMusou2)

public:
	SCTutorialMainSceneSetMusou2();
	virtual ~SCTutorialMainSceneSetMusou2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////点击菜单栏上的武将(第一次武将教学：直接进入我的武将列表界面)/////////////////////////////

class SCTutorialMainSceneForFirst : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneForFirst)

public:
	SCTutorialMainSceneForFirst();
	virtual ~SCTutorialMainSceneForFirst();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////点击菜单栏上的武将(第二次武将教学和阵法学习教学)/////////////////////////////

class SCTutorialMainSceneForSecond : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneForSecond)

public:
	SCTutorialMainSceneForSecond();
	virtual ~SCTutorialMainSceneForSecond();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////点击菜单栏上的武将/////////////////////////////

class SCTutorialMainScene4 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene4)

public:
	SCTutorialMainScene4();
	virtual ~SCTutorialMainScene4();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


/////////////////////////点击人物头像（设置强化）/////////////////////////////
//设置 精炼
class SCTutorialMainSceneSetStrength1 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneSetStrength1)

public:
	SCTutorialMainSceneSetStrength1();
	virtual ~SCTutorialMainSceneSetStrength1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

class SCTutorialMainScene5 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene5)

public:
	SCTutorialMainScene5();
	virtual ~SCTutorialMainScene5();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};
//设置 升星
class SCTutorialMainSceneSetStrengthStar1 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneSetStrengthStar1)

public:
	SCTutorialMainSceneSetStrengthStar1();
	virtual ~SCTutorialMainSceneSetStrengthStar1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

class SCTutorialMainSceneStar1 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneStar1)

public:
	SCTutorialMainSceneStar1();
	virtual ~SCTutorialMainSceneStar1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////点击菜单栏上人物/////////////////////////////

class SCTutorialMainScene6 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene6)

public:
	SCTutorialMainScene6();
	virtual ~SCTutorialMainScene6();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////选择任务小面板中的第一个/////////////////////////////

class SCTutorialMissionAndTeam1 : public Script {
private:
	DECLARE_CLASS(SCTutorialMissionAndTeam1)

public:
	SCTutorialMissionAndTeam1();
	virtual ~SCTutorialMissionAndTeam1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////选择任务小面板中的第二个/////////////////////////////

class SCTutorialMissionAndTeam2 : public Script {
private:
	DECLARE_CLASS(SCTutorialMissionAndTeam2)

public:
	SCTutorialMissionAndTeam2();
	virtual ~SCTutorialMissionAndTeam2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


/////////////////////////点击副本信息小面板/////////////////////////////

class SCTutorialMissionAndTeam3 : public Script {
private:
	DECLARE_CLASS(SCTutorialMissionAndTeam3)

public:
	SCTutorialMissionAndTeam3();
	virtual ~SCTutorialMissionAndTeam3();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


/////////////////////////通过操作摇杆和点击地面移动/////////////////////////////

class SCTutorialMainScene7 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene7)

public:
	SCTutorialMainScene7();
	virtual ~SCTutorialMainScene7();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


////////////////////////点快捷栏使用技能-新手剧情中//////////////////////////////

class SCTutorialMainScene8 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene8)

public:
	SCTutorialMainScene8();
	virtual ~SCTutorialMainScene8();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


////////////////////////点快捷栏3号使用单体技能技能-新手剧情中//////////////////////////////

class SCTutorialMainScene9 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene9)

public:
	SCTutorialMainScene9();
	virtual ~SCTutorialMainScene9();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////点快捷栏2号使用群体技能技能-新手剧情中//////////////////////////////

class SCTutorialMainScene10 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene10)

public:
	SCTutorialMainScene10();
	virtual ~SCTutorialMainScene10();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


////////////////////////点第一个武将头像使武将出战-新手剧情中//////////////////////////////

class SCTutorialMainScene11 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene11)

public:
	SCTutorialMainScene11();
	virtual ~SCTutorialMainScene11();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////点第二个武将头像使武将出战-新手剧情中//////////////////////////////

class SCTutorialMainScene12 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene12)

public:
	SCTutorialMainScene12();
	virtual ~SCTutorialMainScene12();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////点第三个武将头像使武将出战-新手剧情中//////////////////////////////

class SCTutorialMainScene13 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene13)

public:
	SCTutorialMainScene13();
	virtual ~SCTutorialMainScene13();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////点第一个武将头像使武将出战-正常游戏过程中//////////////////////////////

class SCTutorialMainScene14 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene14)

public:
	SCTutorialMainScene14();
	virtual ~SCTutorialMainScene14();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif

