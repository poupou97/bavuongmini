#include "SCTutorialRobot.h"
#include "../../utils/GameUtils.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutConfigure.h"
#include "SCTutorialGeneralScene.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/extensions/UITab.h"
#include "../../ui/generals_ui/GeneralsRecuriteUI.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"


IMPLEMENT_CLASS(SCTutorialRobotStep1)

SCTutorialRobotStep1::SCTutorialRobotStep1()
{
}

SCTutorialRobotStep1::~SCTutorialRobotStep1()
{
}

void* SCTutorialRobotStep1::createInstance()
{
	return new SCTutorialRobotStep1() ;
}

void SCTutorialRobotStep1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep1()");

			std::string content = mPara.at(0);
			GuideMap * guideMap_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			//guideMap_->isOpenSkillMonster
			int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
			if (hookIndex == 0)
			{
				guideMap_->addCCTutorialIndicator(content.c_str(),guideMap_->getBtnHangUp()->getPosition(),CCTutorialIndicator::Direction_RU);
				guideMap_->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(guideMap_->getBtnHangUp()) ;
				setState(STATE_UPDATE);
			}else
			{
				setState(STATE_END);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialRobotStep1::init() {
	CCLOG("SCTutorialRobotStep1() init");
}

void SCTutorialRobotStep1::release() {
	CCLOG("SCTutorialRobotStep1() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GuideMap * guideMap_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (guideMap_ != NULL)
		guideMap_->removeCCTutorialIndicator();
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialRobotStep2)

SCTutorialRobotStep2::SCTutorialRobotStep2()
{
}

SCTutorialRobotStep2::~SCTutorialRobotStep2()
{
}

void* SCTutorialRobotStep2::createInstance()
{
	return new SCTutorialRobotStep2() ;
}

void SCTutorialRobotStep2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialTestStep2()");

			std::string content = mPara.at(0);
			GuideMap * guideMap_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			//guideMap_->isOpenSkillMonster
			int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
			if (hookIndex == 1)
			{
				guideMap_->addCCTutorialIndicator(content.c_str(),guideMap_->getBtnHangUp()->getPosition(),CCTutorialIndicator::Direction_RU);
				guideMap_->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(guideMap_->getBtnHangUp()) ;
				setState(STATE_UPDATE);
			}else
			{
				setState(STATE_END);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialRobotStep2::init() {
	CCLOG("SCTutorialSkillSceneStep2() init");
}

void SCTutorialRobotStep2::release() {
	CCLOG("SCTutorialSkillSceneStep2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GuideMap * guideMap_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (guideMap_ != NULL)
		guideMap_->removeCCTutorialIndicator();
}

void SCTutorialRobotStep2::endCommand( CCObject* pSender )
{
	Script::endCommand(pSender);

	release();
}

