#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALGENERALMODE_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALGENERALMODE_H_

#include "../Script.h"

/**
 * 武将界面教学
 *
 * @author yangjun
 * @date 2014-4-18
 */


//////////////////////////武将模式切换到主动模式////////////////////////////////////
class SCTutorialGeneralModeStep1 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralModeStep1)

public:
	SCTutorialGeneralModeStep1();
	virtual ~SCTutorialGeneralModeStep1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


//////////////////////////武将模式切换到协同模式////////////////////////////////////
class SCTutorialGeneralModeStep2 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralModeStep2)

public:
	SCTutorialGeneralModeStep2();
	virtual ~SCTutorialGeneralModeStep2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////点击武将模式////////////////////////////////////
class SCTutorialGeneralModeStep3 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralModeStep3)

public:
	SCTutorialGeneralModeStep3();
	virtual ~SCTutorialGeneralModeStep3();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif
