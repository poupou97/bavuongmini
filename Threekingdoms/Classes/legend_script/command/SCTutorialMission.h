#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALMISSION_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALMISSION_H_

#include "../Script.h"

/**
 *任务教学
 *
 * @author yangjun
 * @date 2014-3-21
 */

//////////////////////点击任务面板的领取任务////////////////////////
class SCTutorialGetMission : public Script {
private:
	DECLARE_CLASS(SCTutorialGetMission)

public:
	SCTutorialGetMission();
	virtual ~SCTutorialGetMission();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////点击任务面板的提交任务////////////////////////
class SCTutorialFinishMission : public Script {
private:
	DECLARE_CLASS(SCTutorialFinishMission)

public:
	SCTutorialFinishMission();
	virtual ~SCTutorialFinishMission();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////打开悬赏任务界面////////////////////////
class SCTutorialOpenRewardTaskUI : public Script {
private:
	DECLARE_CLASS(SCTutorialOpenRewardTaskUI)

public:
	SCTutorialOpenRewardTaskUI();
	virtual ~SCTutorialOpenRewardTaskUI();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


//////////////////////接悬赏任务///////////////////////
class SCTutorialGetRewardTask : public Script {
private:
	DECLARE_CLASS(SCTutorialGetRewardTask)

public:
	SCTutorialGetRewardTask();
	virtual ~SCTutorialGetRewardTask();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif

