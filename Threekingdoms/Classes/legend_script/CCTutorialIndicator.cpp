#include "CCTutorialIndicator.h"

#include "AppMacros.h"
#include "../gamescene_state/SimpleEffectManager.h"
#include "../GameView.h"
#include "../ui/missionscene/MissionManager.h"
#include "../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../gamescene_state/MainScene.h"
#include "../utils/GameUtils.h"

#define HightLightFrameAction_PlaySpaceTime 10000

CCTutorialIndicator::CCTutorialIndicator():
m_direction(Direction_LD)
{
}

CCTutorialIndicator::~CCTutorialIndicator()
{
}

CCTutorialIndicator* CCTutorialIndicator::create(const char* content,CCPoint pos,Direction direction,bool isAddToMainScene,bool isDesFrameVisible ,bool isHandFilp)
{
	CCTutorialIndicator * tutorialIndicator = new CCTutorialIndicator();
	if (tutorialIndicator && tutorialIndicator->init(content,pos,direction,isAddToMainScene,isDesFrameVisible,isHandFilp))
	{
		tutorialIndicator->autorelease();
		return tutorialIndicator;
	}
	CC_SAFE_DELETE(tutorialIndicator);
	return NULL;
}

bool CCTutorialIndicator::init(const char* content,CCPoint pos,Direction direction,bool isAddToMainScene,bool isDesFrameVisible,bool isHandFilp )
{
	if (CCNodeRGBA::init())
	{
		isAutoPlayHightLightAction = false;
		//description
		m_label_des = CCLabelTTF::create(content,APP_FONT_NAME,18,CCSizeMake(190,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentCenter);
		m_label_des->setAnchorPoint(ccp(0.5f,0.5f));
		m_label_des->setColor(ccc3(92,35,15));

		//descriptionFrame
		m_image_desFrame  = CCScale9Sprite::create("res_ui/tutorial/yindaokuang_0.png");
		m_image_desFrame->ignoreAnchorPointForPosition(false);
		m_image_desFrame->setCapInsets(CCRectMake(35,18,1,34));
		if (m_label_des->getContentSize().height>40)  //三行
		{
			m_image_desFrame->setContentSize(CCSizeMake(230,101));
		}
		else if (m_label_des->getContentSize().height>20&&m_label_des->getContentSize().height<=40)//两行
		{
			m_image_desFrame->setContentSize(CCSizeMake(230,81));
		}
		else  //一行
		{
			m_image_desFrame->setContentSize(CCSizeMake(230,69));
		}
		m_image_desFrame->setAnchorPoint(ccp(0.5f,0.5f));


		this->addChild(m_image_desFrame);
		m_image_desFrame->addChild(m_label_des);
		m_label_des->setPosition(ccp(m_image_desFrame->getContentSize().width/2+10,m_image_desFrame->getContentSize().height/2));

		m_anm_indicator = CCLegendAnimation::create("animation/texiao/renwutexiao/shou1/shou.anm");
		m_anm_indicator->setPlayLoop(true);
		m_anm_indicator->setReleaseWhenStop(true);
		this->addChild(m_anm_indicator);
		if (!isHandFilp)
		{
			m_anm_indicator->setScaleX(1.0f);
			switch (direction) 
			{
			case  Direction_LD :
				{
					m_image_desFrame->setPosition(ccp(70+m_image_desFrame->getContentSize().width/2,13));
				}
				break;
			case  Direction_LU :
				{
					m_image_desFrame->setPosition(ccp(70+m_image_desFrame->getContentSize().width/2,-25));
				}
				break;
			case  Direction_RU :
				{
					m_image_desFrame->setPosition(ccp(-40-m_image_desFrame->getContentSize().width/2,-25));
				}
				break;
			case  Direction_RD :
				{
					m_image_desFrame->setPosition(ccp(-40-m_image_desFrame->getContentSize().width/2,13));
				}
				break;
			}
		}
		else
		{
			m_anm_indicator->setScaleX(-1);
			switch (direction) 
			{
			case  Direction_LD :
				{
					m_image_desFrame->setPosition(ccp(30+m_image_desFrame->getContentSize().width/2,13));
				}
				break;
			case  Direction_LU :
				{
					m_image_desFrame->setPosition(ccp(30+m_image_desFrame->getContentSize().width/2,-25));
				}
				break;
			case  Direction_RU :
				{
					m_image_desFrame->setPosition(ccp(-50-m_image_desFrame->getContentSize().width/2,-25));
				}
				break;
			case  Direction_RD :
				{
					m_image_desFrame->setPosition(ccp(-50-m_image_desFrame->getContentSize().width/2,13));
				}
				break;
			}
		}

		//是教学笔还是15级前的指引笔(默认是教学)
		setPresentDesFrame(isDesFrameVisible);

		//当教学笔需要出现时，首先检测是否15级前指引笔存在，如果存在则删除
		if (isDesFrameVisible && isAddToMainScene)//是教学笔并且是添加到主界面上的
		{
			if (GameView::getInstance())
			{
				if (GameView::getInstance()->getGameScene())
				{
					if (GameView::getInstance()->myplayer->getActiveRole()->level() <= K_GuideForMainMission_Level)
					{
						MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
						if (mainScene)
						{
							MissionAndTeam * missionAndTeam = (MissionAndTeam*)mainScene->getChildByTag(kTagMissionAndTeam);
							if (missionAndTeam)
							{
								missionAndTeam->removeGuidePen();
							}
						}
					}
				}
			}
		}

		return true;
	}
	return false;
}



CCSequence* CCTutorialIndicator::RoundRectPathAction(Direction direction,float controlX, float controlY, float w)
{
	switch (direction) 
	{
	case  Direction_LD :
		{
			ccBezierConfig bezier1;
			bezier1.controlPoint_1 = CCPoint(controlX, 0);
			bezier1.controlPoint_2 = CCPoint(controlX, -controlY);
			bezier1.endPosition = CCPoint(0, -controlY);
			CCBezierBy* bezierBy1 = CCBezierBy::create(0.8f, bezier1);

			CCMoveBy* move1 = CCMoveBy::create(0.1f, CCPoint(-w, 0));

			ccBezierConfig bezier2;
			bezier2.controlPoint_1 = CCPoint(-controlX, 0);
			bezier2.controlPoint_2 = CCPoint(-controlX, controlY);
			bezier2.endPosition = CCPoint(0, controlY);
			CCBezierBy* bezierBy2 = CCBezierBy::create(0.8f, bezier2);

			CCMoveBy* move2 = CCMoveBy::create(0.1f, CCPoint(w, 0));

			CCSequence* path = NULL;

			if(w > 0)
				path = CCSequence::create(bezierBy1, move1, bezierBy2, move2, NULL);
			else
				path = CCSequence::create(bezierBy1, bezierBy2, NULL);
			return path;
		}
		break;
	case  Direction_LU :
		{
			ccBezierConfig bezier1;
			bezier1.controlPoint_1 = CCPoint(-controlX, 0);
			bezier1.controlPoint_2 = CCPoint(-controlX, controlY);
			bezier1.endPosition = CCPoint(0, controlY);
			CCBezierBy* bezierBy1 = CCBezierBy::create(0.8f, bezier1);

			CCMoveBy* move1 = CCMoveBy::create(0.1f, CCPoint(-w, 0));

			ccBezierConfig bezier2;
			bezier2.controlPoint_1 = CCPoint(controlX, 0);
			bezier2.controlPoint_2 = CCPoint(controlX, -controlY);
			bezier2.endPosition = CCPoint(0, -controlY);
			CCBezierBy* bezierBy2 = CCBezierBy::create(0.8f, bezier2);

			CCMoveBy* move2 = CCMoveBy::create(0.1f, CCPoint(w, 0));

			CCSequence* path = NULL;

			if(w > 0)
				path = CCSequence::create(move1,bezierBy1, move2, bezierBy2, NULL);
			else
				path = CCSequence::create(bezierBy1, bezierBy2, NULL);
			return path;
		}
		break;
	case  Direction_RD :
		{
			CCMoveBy* move1 = CCMoveBy::create(0.1f, CCPoint(w, 0));

			ccBezierConfig bezier1;
			bezier1.controlPoint_1 = CCPoint(controlX, 0);
			bezier1.controlPoint_2 = CCPoint(controlX, -controlY);
			bezier1.endPosition = CCPoint(0, -controlY);
			CCBezierBy* bezierBy1 = CCBezierBy::create(0.8f, bezier1);

			CCMoveBy* move2 = CCMoveBy::create(0.1f, CCPoint(-w, 0));
			
			ccBezierConfig bezier2;
			bezier2.controlPoint_1 = CCPoint(-controlX, 0);
			bezier2.controlPoint_2 = CCPoint(-controlX, controlY);
			bezier2.endPosition = CCPoint(0, controlY);
			CCBezierBy* bezierBy2 = CCBezierBy::create(0.8f, bezier2);

			CCSequence* path = NULL;

			if(w > 0)
				path = CCSequence::create(move1,bezierBy1, move2, bezierBy2, NULL);
			else
				path = CCSequence::create(bezierBy1, bezierBy2, NULL);
			return path;
		}
		break;
	case  Direction_RU :
		{
			CCMoveBy* move1 = CCMoveBy::create(0.1f, CCPoint(w, 0));

			ccBezierConfig bezier1;
			bezier1.controlPoint_1 = CCPoint(-controlX, 0);
			bezier1.controlPoint_2 = CCPoint(-controlX, controlY);
			bezier1.endPosition = CCPoint(0, controlY);
			CCBezierBy* bezierBy1 = CCBezierBy::create(0.8f, bezier1);

			CCMoveBy* move2 = CCMoveBy::create(0.1f, CCPoint(-w, 0));

			ccBezierConfig bezier2;
			bezier2.controlPoint_1 = CCPoint(controlX, 0);
			bezier2.controlPoint_2 = CCPoint(controlX, -controlY);
			bezier2.endPosition = CCPoint(0, -controlY);
			CCBezierBy* bezierBy2 = CCBezierBy::create(0.8f, bezier2);

			CCSequence* path = NULL;

			if(w > 0)
				path = CCSequence::create(bezierBy1,move1, bezierBy2,move2, NULL);
			else
				path = CCSequence::create(bezierBy2, bezierBy1, NULL);
			return path;
		}
		break;
	}
}

void CCTutorialIndicator::setPresentDesFrame( bool TOrF )
{
	//descriptionFrame
	m_image_desFrame->setVisible(TOrF);
}

#define kTag_HighLightFrame 6984
#define kTag_HighLightFrameAction 6985
void CCTutorialIndicator::addHighLightFrameAction( int width,int height,CCPoint offset,bool isAutoPlay/* = false */)
{
	m_nHightLightWidth = width;
	m_nHightLightHeight = height;
	m_pHightLightOffSet = offset;
	isAutoPlayHightLightAction = isAutoPlay;
	m_nStartTime = GameUtils::millisecondNow();
	playHightLightFrameActionOnce();

	if (isAutoPlay)
	{
		this->schedule(schedule_selector(CCTutorialIndicator::update), 1.0f);
	}
}

void CCTutorialIndicator::playHightLightFrameActionOnce()
{
	if (this->getChildByTag(kTag_HighLightFrame))
	{
		this->getChildByTag(kTag_HighLightFrame)->removeFromParent();
	}

	CCScale9Sprite *sp_yellowFrame = CCScale9Sprite::create("res_ui/yellow_frame.png"); 
	sp_yellowFrame->setCapInsets(CCRectMake(5,5,1,1));
	sp_yellowFrame->setPreferredSize(CCSizeMake(m_nHightLightWidth*6.0f,m_nHightLightHeight*6.0f));
	sp_yellowFrame->setAnchorPoint(ccp(0.5f, 0.5f));
	sp_yellowFrame->setPosition(m_pHightLightOffSet);
	this->addChild(sp_yellowFrame);
	sp_yellowFrame->setTag(kTag_HighLightFrame);
	sp_yellowFrame->setVisible(false);
	sp_yellowFrame->setScale(1.0f);

	CCSequence * sequence = CCSequence::create(
		CCShow::create(),
		CCScaleTo::create(0.5f,1.0f/6.0f,1.0f/6.0f),
		CCHide::create(),
		CCRemoveSelf::create(),
		NULL);
	sp_yellowFrame->runAction(sequence);
	sequence->setTag(kTag_HighLightFrameAction);
}

#define kTag_CenterAnmSprite 6994
void CCTutorialIndicator::addCenterAnm( int width,int height,CCPoint offset /*= ccp(0,0)*/ )
{
	if (this->getChildByTag(kTag_CenterAnmSprite))
	{
		this->getChildByTag(kTag_CenterAnmSprite)->removeFromParent();
	}

	CCScale9Sprite *sp_CenterAnmSprite = CCScale9Sprite::create("gamescene_state/zhujiemian3/other/light_kuang.png"); 
	sp_CenterAnmSprite->setCapInsets(CCRectMake(32,32,1,1));
	sp_CenterAnmSprite->setPreferredSize(CCSizeMake(width,height));
	sp_CenterAnmSprite->setAnchorPoint(ccp(0.5f, 0.5f));
	sp_CenterAnmSprite->setPosition(offset);
	this->addChild(sp_CenterAnmSprite);
	sp_CenterAnmSprite->setVisible(true);
	sp_CenterAnmSprite->setZOrder(-1);
	sp_CenterAnmSprite->setTag(kTag_CenterAnmSprite);

	CCSequence * sequence = CCSequence::create(
		CCScaleTo::create(0.5f,1.05f,1.05f),
		CCScaleTo::create(0.5f,0.95f,0.95f),
		NULL);
	sp_CenterAnmSprite->runAction(CCRepeatForever::create(sequence));
}

void CCTutorialIndicator::update( float delta )
{
	if (isAutoPlayHightLightAction)
	{
		if (GameUtils::millisecondNow() - m_nStartTime >= HightLightFrameAction_PlaySpaceTime)
		{
			m_nStartTime = GameUtils::millisecondNow();

			if (!GameView::getInstance()->getMainUIScene())
			{
				return;
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission))
			{
				return;
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow))
			{
				return;
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc))
			{
				return;
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI))
			{
				return;
			}
			playHightLightFrameActionOnce();
		}
	}
}
