
#ifndef _LEGEND_SCRIPT_CCTUTORIALINDICATOR_H_
#define _LEGEND_SCRIPT_CCTUTORIALINDICATOR_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCLegendAnimation;

#define CCTUTORIALINDICATORTAG 986532

class CCTutorialIndicator : public CCNodeRGBA
{
public:
	CCTutorialIndicator();
	virtual ~CCTutorialIndicator();

	enum Direction{
		Direction_LD = 0,       // ����
		Direction_LU,				//����
		Direction_RU,				//����
		Direction_RD,				//����
	};

	static CCTutorialIndicator * create(const char* content,CCPoint pos,Direction direction,bool isAddToMainScene = false,bool isDesFrameVisible = true,bool isHandFilp = false);
	bool init(const char* content,CCPoint pos,Direction direction,bool isAddToMainScene,bool isDesFrameVisible,bool isHandFilp);

	static CCSequence* RoundRectPathAction(Direction direction,float controlX, float controlY, float w);

	void addHighLightFrameAction(int width,int height,CCPoint offset = ccp(0,0),bool isAutoPlay = false);

	void addCenterAnm(int width,int height,CCPoint offset = ccp(0,0));

private:
	void setPresentDesFrame(bool TOrF);

	void playHightLightFrameActionOnce();
	void update(float delta);

public:
	//CCSprite * m_image_indicator;
	CCLegendAnimation * m_anm_indicator;
	CCScale9Sprite *m_image_desFrame;
	CCLabelTTF * m_label_des;
	//CCParticleSystem* m_ps_particleEffect;
private:
	Direction m_direction;
	
	long long m_nStartTime;

	int m_nHightLightWidth;
	int m_nHightLightHeight;
	CCPoint m_pHightLightOffSet;
	bool isAutoPlayHightLightAction;
};

#endif
