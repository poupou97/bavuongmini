
#include "ReqSender5015.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender5015)

ReqSender5015::ReqSender5015() 
{

}
ReqSender5015::~ReqSender5015() 
{

}
void* ReqSender5015::createInstance()
{
	return new ReqSender5015() ;
}
void ReqSender5015::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5015::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5015::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5015);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}