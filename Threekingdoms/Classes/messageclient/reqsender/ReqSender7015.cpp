
#include "ReqSender7015.h"

#include "../ClientNetEngine.h"
#include "../protobuf/MissionMessage.pb.h"

IMPLEMENT_CLASS(ReqSender7015)

ReqSender7015::ReqSender7015() 
{

}
ReqSender7015::~ReqSender7015() 
{

}
void* ReqSender7015::createInstance()
{
	return new ReqSender7015() ;
}
void ReqSender7015::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender7015::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender7015::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(7015);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	ReqAcceptBoardMission7015 message1;
	message1.set_index((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}