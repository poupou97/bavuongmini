
#include "ReqSender5094.h"

#include "../ClientNetEngine.h"
#include "../protobuf/FightMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5094)

ReqSender5094::ReqSender5094() 
{

}
ReqSender5094::~ReqSender5094() 
{

}
void* ReqSender5094::createInstance()
{
	return new ReqSender5094() ;
}
void ReqSender5094::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5094::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5094::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5094);

	Req5094 message1;
	message1.set_reciever((long long)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}