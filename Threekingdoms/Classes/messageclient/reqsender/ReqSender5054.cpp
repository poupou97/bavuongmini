
#include "ReqSender5054.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../ui/generals_ui/GeneralsSkillsUI.h"

IMPLEMENT_CLASS(ReqSender5054)

	ReqSender5054::ReqSender5054() 
{

}
ReqSender5054::~ReqSender5054() 
{

}
void* ReqSender5054::createInstance()
{
	return new ReqSender5054() ;
}
void ReqSender5054::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5054::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5054::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5054);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5054 message1;
	message1.set_skillid((char *)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}