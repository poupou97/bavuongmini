
#include "ReqSender5202.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../protobuf/DailyGiftSign.pb.h"
#include "../protobuf/VipMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5202)

	ReqSender5202::ReqSender5202() 
{

}
ReqSender5202::~ReqSender5202() 
{

}
void* ReqSender5202::createInstance()
{
	return new ReqSender5202() ;
}
void ReqSender5202::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5202::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5202::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5202);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5202 message1;
	message1.set_rewardtype((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}