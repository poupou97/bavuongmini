
#include "ReqSender1541.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightResultUI.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(ReqSender1541)
	ReqSender1541::ReqSender1541() 
{

}
ReqSender1541::~ReqSender1541() 
{
}
void* ReqSender1541::createInstance()
{
	return new ReqSender1541() ;
}
void ReqSender1541::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1541::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1541::send(void* source, void* source1)
{
// 	FamilyFightResultUI * resultUI = (FamilyFightResultUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightResultUI);
// 	if (resultUI)
// 	{
// 		resultUI->endSlowMotion();
// 	}

	CommonMessage comMessage;
	comMessage.set_cmdid(1541);

	Req1541 message1;
	message1.set_battleid((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}