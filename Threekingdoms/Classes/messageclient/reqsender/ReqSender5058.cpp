
#include "ReqSender5058.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsTeachUI.h"

IMPLEMENT_CLASS(ReqSender5058)

	ReqSender5058::ReqSender5058() 
{

}
ReqSender5058::~ReqSender5058() 
{

}
void* ReqSender5058::createInstance()
{
	return new ReqSender5058() ;
}
void ReqSender5058::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5058::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5058::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5058);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5058 message1;

	GeneralsTeachUI * generalsTeachUI = (GeneralsTeachUI*)source;
	message1.set_generalid(generalsTeachUI->curTeachGeneralId);
	for (int i = 0;i<generalsTeachUI->victimList.size();++i)
	{
		message1.add_eatgeneralid(generalsTeachUI->victimList.at(i));
	}

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}