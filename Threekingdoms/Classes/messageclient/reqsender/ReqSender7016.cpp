
#include "ReqSender7016.h"

#include "../ClientNetEngine.h"
#include "../protobuf/MissionMessage.pb.h"

IMPLEMENT_CLASS(ReqSender7016)

ReqSender7016::ReqSender7016() 
{

}
ReqSender7016::~ReqSender7016() 
{

}
void* ReqSender7016::createInstance()
{
	return new ReqSender7016() ;
}
void ReqSender7016::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender7016::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender7016::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(7016);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	ReqRefreshBoardMission7016 message1;
	int type = (int)(source);
	
	com::future::threekingdoms::server::transport::protocol::RefreshType rt = (com::future::threekingdoms::server::transport::protocol::RefreshType)type;
	message1.set_type(rt);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}