
#ifndef Blog_C___Reflection_ReqSender7007_h
#define Blog_C___Reflection_ReqSender7007_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender7007 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender7007)

public:
	SYNTHESIZE(ReqSender7007, int*, m_pValue)

		ReqSender7007() ;
	virtual ~ReqSender7007() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
