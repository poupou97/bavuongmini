
#include "ReqSender1406.h"

#include "../ClientNetEngine.h" 
#include "../protobuf/RelationMessage.pb.h"



IMPLEMENT_CLASS(ReqSender1406)
ReqSender1406::ReqSender1406() 
{
    
}
ReqSender1406::~ReqSender1406() 
{
}
void* ReqSender1406::createInstance()
{
    return new ReqSender1406() ;
}
void ReqSender1406::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1406::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1406::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1406);
	
	Req1406 message1;

	message1.set_fuctiontype((int)source);//选中的按钮编号 0开除队伍 1队长移交 2解散队伍  3退出队伍
	message1.set_playerid((int)source1);//目标玩家id    队长解散队伍和队员退出队伍的时候为0
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}