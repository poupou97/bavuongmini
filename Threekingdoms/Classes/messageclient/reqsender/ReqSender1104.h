

#ifndef Blog_C___Reflection_ReqSender1102_h
#define Blog_C___Reflection_ReqSender1102_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1104 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1104)
    
public:
    SYNTHESIZE(ReqSender1104, int*, m_pValue)
    
    ReqSender1104() ;
    virtual ~ReqSender1104() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
