
#include "ReqSender7021.h"

#include "../ClientNetEngine.h"
#include "../protobuf/MissionMessage.pb.h"

IMPLEMENT_CLASS(ReqSender7021)

	ReqSender7021::ReqSender7021() 
{

}
ReqSender7021::~ReqSender7021() 
{

}
void* ReqSender7021::createInstance()
{
	return new ReqSender7021() ;
}
void ReqSender7021::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender7021::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender7021::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(7021);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}