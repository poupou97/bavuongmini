
#include "ReqSender1302.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "GameView.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"

IMPLEMENT_CLASS(ReqSender1302)

	ReqSender1302::ReqSender1302() 
{

}
ReqSender1302::~ReqSender1302() 
{

}
void* ReqSender1302::createInstance()
{
	return new ReqSender1302() ;
}
void ReqSender1302::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1302::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1302::send(void* source, void* source1)
{
	// set message id
	CommonMessage comMessage;
	comMessage.set_cmdid(1302);
	Req1302 message1;
	// set message content

	//message1.set_source(2);
	//message1.set_index(packageScene->pacPageView->curPackageItemIndex);
	message1.set_source(2);
	//message1.set_index(GameView::getInstance()->pacPageView->curPackageItemIndex);
	message1.set_generalid((long long)source);
	PackageScene::IdxAndNum * temp = (PackageScene::IdxAndNum*)source1;
	message1.set_index(temp->index);
	message1.set_amount(temp->num);
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
	
}