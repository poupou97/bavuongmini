
#include "ReqSender1607.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1607)
ReqSender1607::ReqSender1607() 
{
    
}
ReqSender1607::~ReqSender1607() 
{
}
void* ReqSender1607::createInstance()
{
    return new ReqSender1607() ;
}
void ReqSender1607::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1607::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1607::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1607);
	
	Req1607 message1;

	
	//message1.set_index((int )source);
	//message1.set_amount((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}