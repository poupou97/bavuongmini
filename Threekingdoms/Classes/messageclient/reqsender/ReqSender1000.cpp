
#include "ReqSender1000.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1000)

ReqSender1000::ReqSender1000() 
{
    
}
ReqSender1000::~ReqSender1000() 
{
    
}
void* ReqSender1000::createInstance()
{
    return new ReqSender1000() ;
}
void ReqSender1000::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1000::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1000::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1000);

	//CCLOG("send msg: %d, heart beat", comMessage.cmdid());
	
	// no content
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}