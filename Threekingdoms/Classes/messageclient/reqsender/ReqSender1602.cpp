
#include "ReqSender1602.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"



IMPLEMENT_CLASS(ReqSender1602)
ReqSender1602::ReqSender1602() 
{
    
}
ReqSender1602::~ReqSender1602() 
{
}
void* ReqSender1602::createInstance()
{
    return new ReqSender1602() ;
}
void ReqSender1602::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1602::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1602::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1602);
	
	Req1602 message1;
	EquipMentUi * equip=(EquipMentUi *)source;
	
	for (int i=0;i<equip->assistVector.size();i++)
	{
		Req1601 * amountEquip=message1.add_equipments();
		amountEquip->set_source(equip->assistVector.at(i).source_);
		if (equip->assistVector.at(i).source_ == 1)
		{
			amountEquip->set_pob(equip->assistVector.at(i).pob_);
			amountEquip->set_petid(equip->assistVector.at(i).petid_);
		}else
		{
			amountEquip->set_index(equip->assistVector.at(i).index_);
		}
 	}
	
	for (int j=0;j<equip->assStoneVector.size();j++)
	{
		Req1602_Stones * stones_ = message1.add_stones();
		if(equip->assStoneVector.at(j).amount_ >0)
		{
			stones_->set_strengthstone(equip->assStoneVector.at(j).index_);//精炼石格子ID
			stones_->set_strengthstoneamount(equip->assStoneVector.at(j).amount_);//精炼石数量
		}
	}
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}