
#include "ReqSender5133.h"

#include "../ClientNetEngine.h"
#include "../protobuf/DailyGift.pb.h"

IMPLEMENT_CLASS(ReqSender5133)

ReqSender5133::ReqSender5133() 
{

}
ReqSender5133::~ReqSender5133() 
{

}
void* ReqSender5133::createInstance()
{
	return new ReqSender5133() ;
}
void ReqSender5133::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5133::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5133::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5133);

	Req5133 message1;
	message1.set_playerid((long long)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}