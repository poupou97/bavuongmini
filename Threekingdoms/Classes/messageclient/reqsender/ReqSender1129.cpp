
#include "ReqSender1129.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/NPCMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(ReqSender1129)

ReqSender1129::ReqSender1129() 
{

}
ReqSender1129::~ReqSender1129() 
{

}
void* ReqSender1129::createInstance()
{
	return new ReqSender1129() ;
}
void ReqSender1129::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1129::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1129::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1129);

	Req1129 message1;

	message1.set_npcid((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}