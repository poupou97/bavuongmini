#include "ReqSender1319.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../../ui/storehouse_ui/StoreHouseUI.h"

IMPLEMENT_CLASS(ReqSender1319)

	ReqSender1319::ReqSender1319() 
{

}
ReqSender1319::~ReqSender1319() 
{

}
void* ReqSender1319::createInstance()
{
	return new ReqSender1319() ;
}
void ReqSender1319::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1319::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1319::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1319);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req1319 message1;

	StoreHouseUI::ReqData * temp = (StoreHouseUI::ReqData*)source;

	message1.set_mode(temp->mode);
	message1.set_source(temp->source);
	message1.set_num(temp->num);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
	
	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}