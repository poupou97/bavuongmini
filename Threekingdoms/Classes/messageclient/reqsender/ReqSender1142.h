

#ifndef Blog_C_CheckOtherEquipMent_ReqSender1142_h
#define Blog_C_CheckOtherEquipMent_ReqSender1142_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

/*
* 向NPC出售商品
*/
class ReqSender1142 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1142)

public:
	SYNTHESIZE(ReqSender1142, int*, m_pValue)

	ReqSender1142() ;
	virtual ~ReqSender1142() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
