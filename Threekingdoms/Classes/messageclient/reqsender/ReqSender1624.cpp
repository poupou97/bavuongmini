
#include "ReqSender1624.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1624)
ReqSender1624::ReqSender1624() 
{
    
}
ReqSender1624::~ReqSender1624() 
{
}
void* ReqSender1624::createInstance()
{
    return new ReqSender1624() ;
}
void ReqSender1624::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1624::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1624::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1624);
	
	Req1624 message1;
	
	message1.set_holeid((int)source);
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}