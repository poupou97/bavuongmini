
#include "ReqSender5055.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5055)

	ReqSender5055::ReqSender5055() 
{

}
ReqSender5055::~ReqSender5055() 
{

}
void* ReqSender5055::createInstance()
{
	return new ReqSender5055() ;
}
void ReqSender5055::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5055::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5055::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5055);
	//CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}