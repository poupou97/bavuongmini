
#include "ReqSender1803.h"

#include "../ClientNetEngine.h"


#include "../protobuf/AuctionMessage.pb.h"
#include "../../ui/Auction_ui/AuctionUi.h"


IMPLEMENT_CLASS(ReqSender1803)
ReqSender1803::ReqSender1803() 
{
    
}
ReqSender1803::~ReqSender1803() 
{
}
void* ReqSender1803::createInstance()
{
    return new ReqSender1803() ;
}
void ReqSender1803::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1803::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1803::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1803);
	
	Req1803 message1;


	AuctionUi* auctionui =(AuctionUi *)source; 

	message1.set_optype(auctionui->setopType);//2、停售 3、取回 4、收钱 5、购买
	message1.set_id(auctionui->goodsid);//寄售品ID
	

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}