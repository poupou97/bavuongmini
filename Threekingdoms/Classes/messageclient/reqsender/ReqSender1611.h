

#ifndef Blog_C___Reflection_ReqSender1611_h
#define Blog_C___Reflection_ReqSender1611_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1611 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1611)
    
public:
    SYNTHESIZE(ReqSender1611, int*, m_pValue)
    
    ReqSender1611() ;
    virtual ~ReqSender1611() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
