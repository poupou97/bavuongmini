
#ifndef Blog_C___Reflection_ReqSender1320_h
#define Blog_C___Reflection_ReqSender1320_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1320 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1320)

public:
	SYNTHESIZE(ReqSender1320, int*, m_pValue)

		ReqSender1320() ;
	virtual ~ReqSender1320() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
