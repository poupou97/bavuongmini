

#ifndef Blog_C___Reflection_ReqSender1920_h
#define Blog_C___Reflection_ReqSender1920_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1920 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1920)
    
public:
    SYNTHESIZE(ReqSender1920, int*, m_pValue)
    
    ReqSender1920() ;
    virtual ~ReqSender1920() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
