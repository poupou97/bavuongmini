
#ifndef Blog_C___Reflection_ReqSender5113_h
#define Blog_C___Reflection_ReqSender5113_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5113: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5113)

public:
	SYNTHESIZE(ReqSender5113, int*, m_pValue)

	ReqSender5113() ;
	virtual ~ReqSender5113() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
