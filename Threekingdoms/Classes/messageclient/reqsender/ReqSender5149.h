
#ifndef Blog_C___Reflection_ReqSender5149_h
#define Blog_C___Reflection_ReqSender5149_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5149: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5149)

public:
	SYNTHESIZE(ReqSender5149, int*, m_pValue)

	ReqSender5149() ;
	virtual ~ReqSender5149() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
