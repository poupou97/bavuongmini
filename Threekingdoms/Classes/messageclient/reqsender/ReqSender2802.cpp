
#include "ReqSender2802.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender2802)
	ReqSender2802::ReqSender2802() 
{

}
ReqSender2802::~ReqSender2802() 
{
}
void* ReqSender2802::createInstance()
{
	return new ReqSender2802() ;
}
void ReqSender2802::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2802::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender2802::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2802);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}