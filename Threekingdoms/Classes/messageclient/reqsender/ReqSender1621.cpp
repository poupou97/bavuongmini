
#include "ReqSender1621.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"

IMPLEMENT_CLASS(ReqSender1621)
ReqSender1621::ReqSender1621() 
{
    
}
ReqSender1621::~ReqSender1621() 
{
}
void* ReqSender1621::createInstance()
{
    return new ReqSender1621() ;
}
void ReqSender1621::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1621::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1621::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1621);
	
	Req1621 message1;
	
	EquipMentUi * equip=(EquipMentUi *)source;

	switch(equip->mes_source)
	{
	case 1:
		{
			message1.set_source(equip->mes_source);
			message1.set_petid(equip->generalMainId);
			message1.set_pob(equip->generalEquipOfPart);
		}break;
	case 2:
		{
			message1.set_source(equip->mes_source);
			message1.set_index(equip->mes_index);
		}break;
	}

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}