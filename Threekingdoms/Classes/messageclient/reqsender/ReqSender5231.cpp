
#include "ReqSender5231.h"

#include "../protobuf/PlayerMessage.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender5231)

ReqSender5231::ReqSender5231() 
{

}
ReqSender5231::~ReqSender5231() 
{

}
void* ReqSender5231::createInstance()
{
	return new ReqSender5231() ;
}
void ReqSender5231::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5231::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5231::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5231);
	
	Req5231 message;
	message.set_seeother((int)source);

	string msgData;
	message.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}