
#ifndef Blog_C___Reflection_ReqSender5133_h
#define Blog_C___Reflection_ReqSender5133_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5133: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5133)

public:
	SYNTHESIZE(ReqSender5133, int*, m_pValue)

	ReqSender5133() ;
	virtual ~ReqSender5133() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
