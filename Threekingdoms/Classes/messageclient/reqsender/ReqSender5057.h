
#ifndef Blog_C___Reflection_ReqSender5057_h
#define Blog_C___Reflection_ReqSender5057_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5057 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5057)

public:
	SYNTHESIZE(ReqSender5057, int*, m_pValue)

		ReqSender5057() ;
	virtual ~ReqSender5057() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
