

#ifndef Blog_C___Reflection_ReqSender1547_h
#define Blog_C___Reflection_ReqSender1547_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1547 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1547)

public:
	SYNTHESIZE(ReqSender1547, int*, m_pValue)

		ReqSender1547() ;
	virtual ~ReqSender1547() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
