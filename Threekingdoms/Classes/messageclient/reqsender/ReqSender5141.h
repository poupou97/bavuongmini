
#ifndef Blog_C___Reflection_ReqSender5141_h
#define Blog_C___Reflection_ReqSender5141_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5141: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5141)

public:
	SYNTHESIZE(ReqSender5141, int*, m_pValue)

	ReqSender5141() ;
	virtual ~ReqSender5141() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
