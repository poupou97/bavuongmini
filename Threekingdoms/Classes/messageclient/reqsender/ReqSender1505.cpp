
#include "ReqSender1505.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1505)
ReqSender1505::ReqSender1505() 
{
    
}
ReqSender1505::~ReqSender1505() 
{
}
void* ReqSender1505::createInstance()
{
    return new ReqSender1505() ;
}
void ReqSender1505::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1505::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1505::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1505);
	
	Req1505 message1;
	
	message1.set_page((int)source);
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}