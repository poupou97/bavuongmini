

#ifndef Blog_C___Family_ReqSender1506_h
#define Blog_C___Family_ReqSender1506_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1506 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1506)
    
public:
	SYNTHESIZE(ReqSender1506, int*, m_pValue)

	ReqSender1506() ;
    virtual ~ReqSender1506() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
