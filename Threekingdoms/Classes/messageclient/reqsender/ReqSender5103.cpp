
#include "ReqSender5103.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(ReqSender5103)

ReqSender5103::ReqSender5103() 
{

}
ReqSender5103::~ReqSender5103() 
{

}
void* ReqSender5103::createInstance()
{
	return new ReqSender5103() ;
}
void ReqSender5103::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5103::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5103::send(void* source, void* source1)
{
	int countryId = *(int*)source;

	CommonMessage comMessage;
	comMessage.set_cmdid(5103);
	CCLOG("send msg: %d", comMessage.cmdid());

	Req5103 message2;
	message2.set_countryid(countryId);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}