
#include "ReqSender1508.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1508)
ReqSender1508::ReqSender1508() 
{
    
}
ReqSender1508::~ReqSender1508() 
{
}
void* ReqSender1508::createInstance()
{
    return new ReqSender1508() ;
}
void ReqSender1508::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1508::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1508::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1508);
	
	Req1508 message1;

	message1.set_type((int)source);
	message1.set_roleid((long long)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}