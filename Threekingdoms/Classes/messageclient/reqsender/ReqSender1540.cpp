
#include "ReqSender1540.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../GameMessageProcessor.h"

IMPLEMENT_CLASS(ReqSender1540)
ReqSender1540::ReqSender1540() 
{

}
ReqSender1540::~ReqSender1540() 
{
}
void* ReqSender1540::createInstance()
{
	return new ReqSender1540() ;
}
void ReqSender1540::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1540::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1540::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1540);

	Req1540 message1;
	message1.set_battleid((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1547,(void *)1);  //请求已方Bosss位置信息
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1547,(void *)2);  //请求敌方Bosss位置信息
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1547,(void *)3);  //请求中立Bosss位置信息
}