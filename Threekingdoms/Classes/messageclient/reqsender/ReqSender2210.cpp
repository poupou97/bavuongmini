
#include "ReqSender2210.h"
#include "../ClientNetEngine.h"
#include "../protobuf/RelationMessage.pb.h"
#include "../../ui/Friend_ui/FriendUi.h"



IMPLEMENT_CLASS(ReqSender2210)
ReqSender2210::ReqSender2210() 
{
    
}
ReqSender2210::~ReqSender2210() 
{
}
void* ReqSender2210::createInstance()
{
    return new ReqSender2210() ;
}
void ReqSender2210::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2210::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender2210::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2210);
	
	Req2210 message1;
	
	FriendUi * friend_ui=(FriendUi *) source;
	message1.set_showonline(friend_ui->showOnlineSelect);
	message1.set_pagenum(friend_ui->pageNumSelect);
	message1.set_pageindex(friend_ui->pageIndexSelect);
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}