
#include "ReqSender1622.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1622)
ReqSender1622::ReqSender1622() 
{
    
}
ReqSender1622::~ReqSender1622() 
{
}
void* ReqSender1622::createInstance()
{
    return new ReqSender1622() ;
}
void ReqSender1622::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1622::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1622::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1622);
	
	Req1622 message1;
	
	//message1.set_holeid((int)source);
	message1.set_nobinding((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}