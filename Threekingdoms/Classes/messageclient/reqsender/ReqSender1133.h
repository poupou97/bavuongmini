

#ifndef Blog_C___Reflection_ReqSender1133_h
#define Blog_C___Reflection_ReqSender1133_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

/*
* 场景进入消息，从客户端向服务器通知"我进场景了"
*/
class ReqSender1133 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1133)

public:
	SYNTHESIZE(ReqSender1133, int*, m_pValue)

		ReqSender1133() ;
	virtual ~ReqSender1133() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
