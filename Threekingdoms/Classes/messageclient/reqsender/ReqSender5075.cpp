
#include "ReqSender5075.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../ui/skillscene/SkillScene.h"

IMPLEMENT_CLASS(ReqSender5075)

ReqSender5075::ReqSender5075() 
{

}
ReqSender5075::~ReqSender5075() 
{

}
void* ReqSender5075::createInstance()
{
	return new ReqSender5075() ;
}
void ReqSender5075::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5075::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5075::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5075);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5075 message1;
	SkillScene::ReqForPreeless * temp = (SkillScene::ReqForPreeless *)source;
	message1.set_skillid(temp->skillid);
	message1.set_grid(temp->grid);
	message1.set_roleid(temp->roleId);
	message1.set_roletype(temp->roleType);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}