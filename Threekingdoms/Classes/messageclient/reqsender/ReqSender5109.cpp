
#include "ReqSender5109.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../GameView.h"
#include "../protobuf/GeneralMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5109)
ReqSender5109::ReqSender5109() 
{

}
ReqSender5109::~ReqSender5109() 
{

}
void* ReqSender5109::createInstance()
{
	return new ReqSender5109() ;
}
void ReqSender5109::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5109::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5109::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5109);
	CCLOG("send msg: %d, logout", comMessage.cmdid());

	// set message content
	Req5109 message1;
	message1.set_generalid((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}