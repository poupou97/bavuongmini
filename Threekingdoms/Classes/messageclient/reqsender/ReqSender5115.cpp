#include "ReqSender5115.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../../ui/Active_ui/ActiveShopItemBuyInfo.h"
#include "../protobuf/AdditionStore.pb.h"

IMPLEMENT_CLASS(ReqSender5115)

ReqSender5115::ReqSender5115() 
{

}
ReqSender5115::~ReqSender5115() 
{

}
void* ReqSender5115::createInstance()
{
	return new ReqSender5115() ;
}
void ReqSender5115::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5115::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5115::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5115);
	CCLOG("send msg: %d", comMessage.cmdid());

	Req5115 message1;
	message1.set_propid((int) source);
	message1.set_number((int) source1);

	std::string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);


	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}