
#include "ReqSender3021.h"
#include "../ClientNetEngine.h"
#include "../protobuf/OfflineFight.pb.h"
#include "../../ui/offlinearena_ui/OffLineArenaResultUI.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(ReqSender3021)
	ReqSender3021::ReqSender3021() 
{

}
ReqSender3021::~ReqSender3021() 
{
}
void* ReqSender3021::createInstance()
{
	return new ReqSender3021() ;
}
void ReqSender3021::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender3021::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender3021::send(void* source, void* source1)
{
// 	OffLineArenaResultUI * resultUI = (OffLineArenaResultUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaResultUI);
// 	if (resultUI)
// 	{
// 		resultUI->endSlowMotion();
// 	} 

	CommonMessage comMessage;
	comMessage.set_cmdid(3021);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}