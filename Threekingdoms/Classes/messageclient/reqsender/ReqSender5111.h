
#ifndef Blog_C___Reflection_ReqSender5111_h
#define Blog_C___Reflection_ReqSender5111_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5111: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5111)

public:
	SYNTHESIZE(ReqSender5111, int*, m_pValue)

	ReqSender5111() ;
	virtual ~ReqSender5111() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
