
#ifndef Blog_C___Reflection_ReqSender5058_h
#define Blog_C___Reflection_ReqSender5058_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5058 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5058)

public:
	SYNTHESIZE(ReqSender5058, int*, m_pValue)

		ReqSender5058() ;
	virtual ~ReqSender5058() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
