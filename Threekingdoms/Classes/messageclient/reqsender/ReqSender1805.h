

#ifndef Blog_C___Reflection_ReqSender1805_h
#define Blog_C___Reflection_ReqSender1805_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1805 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1805)
    
public:
    SYNTHESIZE(ReqSender1805, int*, m_pValue)
    
    ReqSender1805() ;
    virtual ~ReqSender1805() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
