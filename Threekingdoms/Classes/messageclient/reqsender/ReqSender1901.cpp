
#include "ReqSender1901.h"

#include "../ClientNetEngine.h"

#include "../protobuf/CopyMessage.pb.h"  

IMPLEMENT_CLASS(ReqSender1901)
ReqSender1901::ReqSender1901() 
{
    
}
ReqSender1901::~ReqSender1901() 
{
    
}
void* ReqSender1901::createInstance()
{
    return new ReqSender1901() ;
}
void ReqSender1901::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1901::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1901::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1901);

	Req1901 message2;
	//enter copy level
	message2.set_clazz((int)source);
	message2.set_level((int)source1);
	
	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}