#ifndef Blog_C___Reflection_ReqSender5130_h
#define Blog_C___Reflection_ReqSender5130_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5130: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5130)

public:
	SYNTHESIZE(ReqSender5130, int*, m_pValue)

	ReqSender5130() ;
	virtual ~ReqSender5130() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
