
#include "ReqSender3006.h"
#include "../ClientNetEngine.h"
#include "../protobuf/OfflineFight.pb.h"

IMPLEMENT_CLASS(ReqSender3006)
ReqSender3006::ReqSender3006() 
{

}
ReqSender3006::~ReqSender3006() 
{
}
void* ReqSender3006::createInstance()
{
	return new ReqSender3006() ;
}
void ReqSender3006::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender3006::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender3006::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(3006);

	ReqRiposte3006 message1;
	message1.set_partner(int(source));

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}