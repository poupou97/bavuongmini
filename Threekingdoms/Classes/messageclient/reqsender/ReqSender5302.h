
#ifndef Blog_C___Reflection_ReqSender5302_h
#define Blog_C___Reflection_ReqSender5302_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5302: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5302)

public:
	SYNTHESIZE(ReqSender5302, int*, m_pValue)

		ReqSender5302() ;
	virtual ~ReqSender5302() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
