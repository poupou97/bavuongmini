#ifndef Blog_C___Reflection_ReqSender5119_h
#define Blog_C___Reflection_ReqSender5119_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5119: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5119)

public:
	SYNTHESIZE(ReqSender5119, int*, m_pValue)

		ReqSender5119() ;
	virtual ~ReqSender5119() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
