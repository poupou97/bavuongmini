
#include "ReqSender2209.h"

#include "../ClientNetEngine.h"

 
#include "../protobuf/RelationMessage.pb.h"

#include "../../ui/Friend_ui/FriendUi.h"



IMPLEMENT_CLASS(ReqSender2209)
ReqSender2209::ReqSender2209() 
{
    
}
ReqSender2209::~ReqSender2209() 
{
}
void* ReqSender2209::createInstance()
{
    return new ReqSender2209() ;
}
void ReqSender2209::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2209::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender2209::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2209);
	
	Req2209 message1;
	
	FriendUi * friend_ui=(FriendUi *)source; 
	
	message1.set_namekey(friend_ui->nameString_);
		
	for (int i=0;i<friend_ui->professionVector.size();i++)
	{
		message1.add_profession(friend_ui->professionVector.at(i));
	}
	
	for (int i =0;i <friend_ui->countryVector.size();i++)
	{
		message1.add_country(friend_ui->countryVector.at(i));
	}

	message1.set_showonline(friend_ui->showOnlineSelect);
	message1.set_minlevel(friend_ui->minLvSelect);
	message1.set_maxlevel(friend_ui->maxLvSelect);
	message1.set_pagenum(friend_ui->pageNumSelect);
	message1.set_pageindex(friend_ui->pageIndexSelect);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}