#ifndef Blog_C___Family_ReqSender1708_h
#define Blog_C___Family_ReqSender1708_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1708 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1708)
    
public:
	SYNTHESIZE(ReqSender1708, int*, m_pValue)

	ReqSender1708() ;
    virtual ~ReqSender1708() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
