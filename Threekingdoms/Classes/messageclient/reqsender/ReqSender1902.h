

#ifndef Blog_C___Reflection_ReqSender1902_h
#define Blog_C___Reflection_ReqSender1902_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1902 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1902)
    
public:
	SYNTHESIZE(ReqSender1902, int*, m_pValue)

	ReqSender1902() ;
    virtual ~ReqSender1902() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
