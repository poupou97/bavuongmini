

#ifndef Blog_C___Reflection_ReqSender1128_h
#define Blog_C___Reflection_ReqSender1128_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1128 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1128)
    
public:
    SYNTHESIZE(ReqSender1128, int*, m_pValue)
    
    ReqSender1128() ;
    virtual ~ReqSender1128() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
