
#ifndef Blog_C___Reflection_ReqSender5051_h
#define Blog_C___Reflection_ReqSender5051_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5051 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5051)

public:
	SYNTHESIZE(ReqSender5051, int*, m_pValue)

		ReqSender5051() ;
	virtual ~ReqSender5051() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
