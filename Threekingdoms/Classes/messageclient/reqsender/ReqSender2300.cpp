#include "ReqSender2300.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../../ui/Rank_ui/RankUI.h"
#include "../protobuf/DailyMessage.pb.h"

IMPLEMENT_CLASS(ReqSender2300)

ReqSender2300::ReqSender2300() 
{

}
ReqSender2300::~ReqSender2300() 
{

}
void* ReqSender2300::createInstance()
{
	return new ReqSender2300() ;
}
void ReqSender2300::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2300::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender2300::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2300);
	CCLOG("send msg: %d", comMessage.cmdid());

	Req2300 message1;

	int nType = 0;
	int nWork = 0;
	int nPage = 0;

	RankDataStruct * rankDataStruct = (RankDataStruct *)source;
	nType = rankDataStruct->nType;
	nWork = rankDataStruct->nWork;
	nPage = rankDataStruct->nPage;

	message1.set_type(nType);
	message1.set_work(nWork);
	message1.set_pagenumber(nPage);

	std::string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
	
	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}