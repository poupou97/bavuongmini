#ifndef Blog_C___Reflection_ReqSender5075_h
#define Blog_C___Reflection_ReqSender5075_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5075 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5075)

public:
	SYNTHESIZE(ReqSender5075, int*, m_pValue)

		ReqSender5075() ;
	virtual ~ReqSender5075() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
