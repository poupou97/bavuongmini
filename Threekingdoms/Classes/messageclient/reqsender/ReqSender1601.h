

#ifndef Blog_C___Reflection_ReqSender1601_h
#define Blog_C___Reflection_ReqSender1601_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1601 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1601)
    
public:
    SYNTHESIZE(ReqSender1601, int*, m_pValue)
    
    ReqSender1601() ;
    virtual ~ReqSender1601() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

	struct assistEquipStruct
	{
		int source_;
		int  pob_;
		long long petid_;
		int index_;
		int playType;
	};

} ;

#endif
