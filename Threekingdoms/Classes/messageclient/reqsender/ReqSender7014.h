
#ifndef Blog_C___Reflection_ReqSender7014_h
#define Blog_C___Reflection_ReqSender7014_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender7014: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender7014)

public:
	SYNTHESIZE(ReqSender7014, int*, m_pValue)

	ReqSender7014() ;
	virtual ~ReqSender7014() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
