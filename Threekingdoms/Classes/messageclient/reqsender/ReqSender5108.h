

#ifndef Blog_C___Reflection_ReqSender5108_h
#define Blog_C___Reflection_ReqSender5108_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5108 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5108)

public:
	SYNTHESIZE(ReqSender5108, int*, m_pValue)

		ReqSender5108() ;
	virtual ~ReqSender5108() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
