
#include "ReqSender2203.h"

#include "../ClientNetEngine.h"
#include "../protobuf/RelationMessage.pb.h"
#include "../../ui/Chat_ui/ChatUI.h"
#include "../../ui/extensions/RichTextInput.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"

IMPLEMENT_CLASS(ReqSender2203)
ReqSender2203::ReqSender2203() 
{
    
}
ReqSender2203::~ReqSender2203() 
{
}
void* ReqSender2203::createInstance()
{
    return new ReqSender2203() ;
}
void ReqSender2203::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2203::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender2203::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2203);
	

	Req2203 message1;	

	ChatInfoStruct * info_ =(ChatInfoStruct *)source;
	if (info_->channelId ==0 )
	{
		message1.set_channelid(info_->channelId);
		message1.set_content(info_->chatContent);
		message1.set_listenerid(info_->listenerId);//// 点对点聊天接收人的id, 群聊不设置
		message1.set_listenername(info_->listenerName);
	}else
	{
		message1.set_channelid(info_->channelId);
		message1.set_content(info_->chatContent);
	}
	

	/*
	ChatUI * chatui= (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
	
	if (chatui->selectChannelId==0)
	{
		message1.set_channelid(chatui->selectChannelId);
		message1.set_content(chatui->chatContent);
		message1.set_listenerid(-1);//// 点对点聊天接收人的id, 群聊不设置
		message1.set_listenername(chatui->privateName);
	}else
	{
		message1.set_channelid(chatui->selectChannelId);
		message1.set_content(chatui->chatContent);
	}
	*/
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}