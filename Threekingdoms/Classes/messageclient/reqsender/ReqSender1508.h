

#ifndef Blog_C___Family_ReqSender1508_h
#define Blog_C___Family_ReqSender1508_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1508 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1508)
    
public:
	SYNTHESIZE(ReqSender1508, int*, m_pValue)

	ReqSender1508() ;
    virtual ~ReqSender1508() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
