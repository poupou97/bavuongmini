
#ifndef Blog_C___Reflection_ReqSender5202_h
#define Blog_C___Reflection_ReqSender5202_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5202: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5202)

public:
	SYNTHESIZE(ReqSender5202, int*, m_pValue)

		ReqSender5202() ;
	virtual ~ReqSender5202() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
