#ifndef Blog_C___Reflection_ReqSender1925_h
#define Blog_C___Reflection_ReqSender1925_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1925 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1925)

public:
	SYNTHESIZE(ReqSender1925, int*, m_pValue)

		ReqSender1925() ;
	virtual ~ReqSender1925() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
