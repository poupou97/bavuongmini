

#ifndef Blog_C___Reflection_ReqSender1137_h
#define Blog_C___Reflection_ReqSender1137_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

/*
* 向NPC出售商品
*/
class ReqSender1137 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1137)

public:
	SYNTHESIZE(ReqSender1137, int*, m_pValue)

	ReqSender1137() ;
	virtual ~ReqSender1137() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
