#ifndef Blog_C___Family_ReqSender1523_h
#define Blog_C___Family_ReqSender1523_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1523 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1523)
    
public:
	SYNTHESIZE(ReqSender1523, int*, m_pValue)

	ReqSender1523() ;
    virtual ~ReqSender1523() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
