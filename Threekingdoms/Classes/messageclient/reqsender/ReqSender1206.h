

#ifndef Blog_C___Reflection_ReqSender1206_h
#define Blog_C___Reflection_ReqSender1206_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1206 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1206)

public:
	SYNTHESIZE(ReqSender1206, int*, m_pValue)

		ReqSender1206() ;
	virtual ~ReqSender1206() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
