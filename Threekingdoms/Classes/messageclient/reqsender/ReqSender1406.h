

#ifndef Blog_C___Reflection_ReqSender1406_h
#define Blog_C___Reflection_ReqSender1406_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1406 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1406)
    
public:
    SYNTHESIZE(ReqSender1406, int*, m_pValue)
    
    ReqSender1406() ;
    virtual ~ReqSender1406() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
