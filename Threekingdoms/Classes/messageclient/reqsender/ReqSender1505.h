

#ifndef Blog_C___Family_ReqSender1505_h
#define Blog_C___Family_ReqSender1505_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1505 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1505)
    
public:
	SYNTHESIZE(ReqSender1505, int*, m_pValue)

	ReqSender1505() ;
    virtual ~ReqSender1505() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
