#include "ReqSender5138.h"
#include "../protobuf/DailyGift.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender5138)

ReqSender5138::ReqSender5138() 
{

}
ReqSender5138::~ReqSender5138() 
{

}
void* ReqSender5138::createInstance()
{
	return new ReqSender5138() ;
}
void ReqSender5138::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5138::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5138::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5138);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5138 message1;
	message1.set_number((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}