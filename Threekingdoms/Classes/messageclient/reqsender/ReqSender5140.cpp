
#include "ReqSender5140.h"

#include "../protobuf/DailyMessage.pb.h"  
#include "../ClientNetEngine.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/giveFlower/GiveFlower.h"

IMPLEMENT_CLASS(ReqSender5140)

ReqSender5140::ReqSender5140() 
{

}
ReqSender5140::~ReqSender5140() 
{

}
void* ReqSender5140::createInstance()
{
	return new ReqSender5140() ;
}
void ReqSender5140::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5140::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5140::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5140);
	
	Req5140 message1;

	GiveFlower::GiveFlowerStruct * struct_ = (GiveFlower::GiveFlowerStruct *)source;

	message1.set_id(struct_->playerId);
	message1.set_name(struct_->playerName);
	message1.set_number(struct_->flowerNumber);
	message1.set_flowername(struct_->flowerName);
	message1.set_isshowname(struct_->isShowName);
	message1.set_flowersaid(struct_->flowerSaid);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}