
#ifndef Blog_C___Reflection_ReqSender1330_h
#define Blog_C___Reflection_ReqSender1330_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1330 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1330)

public:
	SYNTHESIZE(ReqSender1330, int*, m_pValue)

	ReqSender1330() ;
	virtual ~ReqSender1330() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
