

#ifndef Blog_C___Reflection_ReqSender1207_h
#define Blog_C___Reflection_ReqSender1207_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1207 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1207)
    
public:
    SYNTHESIZE(ReqSender1207, int*, m_pValue)
    
    ReqSender1207() ;
    virtual ~ReqSender1207() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
