#ifndef Blog_C___Reflection_ReqSender5138_h
#define Blog_C___Reflection_ReqSender5138_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5138: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5138)

public:
	SYNTHESIZE(ReqSender5138, int*, m_pValue)

	ReqSender5138() ;
	virtual ~ReqSender5138() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
