

#ifndef Blog_C___Family_ReqSender1533_h
#define Blog_C___Family_ReqSender1533_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1533 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1533)

public:
	SYNTHESIZE(ReqSender1533, int*, m_pValue)

		ReqSender1533() ;
	virtual ~ReqSender1533() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
