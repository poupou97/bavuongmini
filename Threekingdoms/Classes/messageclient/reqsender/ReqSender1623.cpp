
#include "ReqSender1623.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1623)
ReqSender1623::ReqSender1623() 
{
    
}
ReqSender1623::~ReqSender1623() 
{
}
void* ReqSender1623::createInstance()
{
    return new ReqSender1623() ;
}
void ReqSender1623::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1623::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1623::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1623);
	
	Req1623 message1;
	
	message1.set_holeid((int)source);
	message1.set_idx((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}