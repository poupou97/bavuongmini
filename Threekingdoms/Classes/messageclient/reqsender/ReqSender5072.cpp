
#include "ReqSender5072.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5072)

	ReqSender5072::ReqSender5072() 
{

}
ReqSender5072::~ReqSender5072() 
{

}
void* ReqSender5072::createInstance()
{
	return new ReqSender5072() ;
}
void ReqSender5072::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5072::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5072::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5072);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5072 message1;
	message1.set_fightwayid((int)source);
	message1.set_flag((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}