
#ifndef Blog_C___Reflection_ReqSender7012_h
#define Blog_C___Reflection_ReqSender7012_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender7012 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender7012)

public:
	SYNTHESIZE(ReqSender7012, int*, m_pValue)

		ReqSender7012() ;
	virtual ~ReqSender7012() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
