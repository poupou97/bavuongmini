
#include "ReqSender1131.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "GameView.h"

IMPLEMENT_CLASS(ReqSender1131)

	ReqSender1131::ReqSender1131() 
{

}
ReqSender1131::~ReqSender1131() 
{

}
void* ReqSender1131::createInstance()
{
	return new ReqSender1131() ;
}
void ReqSender1131::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1131::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1131::send(void* source, void* source1)
{
	// set message id
	CommonMessage comMessage;
	comMessage.set_cmdid(1131);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}