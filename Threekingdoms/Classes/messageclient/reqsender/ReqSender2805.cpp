#include "ReqSender2805.h"
#include "../ClientNetEngine.h"
#include "../protobuf/QuestionMessage.pb.h"
#include "../../ui/Question_ui/QuestionData.h"

IMPLEMENT_CLASS(ReqSender2805)
	ReqSender2805::ReqSender2805() 
{

}
ReqSender2805::~ReqSender2805() 
{
}
void* ReqSender2805::createInstance()
{
	return new ReqSender2805() ;
}
void ReqSender2805::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2805::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender2805::send(void* source, void* source1)
{
	QuestionData::instance()->set_isGetedResult(false);

	CommonMessage comMessage;
	comMessage.set_cmdid(2805);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}