
#ifndef _MESSAGECLIENT_GAMEMESSAGEPROCESSOR_H_
#define _MESSAGECLIENT_GAMEMESSAGEPROCESSOR_H_

#include "cocos2d.h"
USING_NS_CC;

class GameMessageProcessor
{
public:
	GameMessageProcessor();
	virtual ~GameMessageProcessor();

    /** returns a shared instance of the GameMessageProcessor */
    static GameMessageProcessor* sharedMsgProcessor(void);

	void socketUpdate();

	/* send request( commandId ) to GameServer */
	void sendReq(int commandId, void* source = NULL, void* source1 = NULL);

	void checkNetwork();

	//inline void setPingTime(long time) { m_currentPingTime = time; };

private:
	// 定时发送心跳，用来检测ping值和玩家在线情况
	void sendHeartbeat();

private:
	//long long m_currentPingTime;   // record the system time when the new ping come
	long long m_pingTimeCounter;   // time counter for ping ( heartbeat )
} ;

#endif
