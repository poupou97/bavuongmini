
#include "ClientNetEngine.h"

#include <iostream>
#include <string>
#include <stdio.h>

#include "../utils/GameUtils.h"

MessageQueue::MessageQueue() {
	//mu = PTHREAD_MUTEX_INITIALIZER;
	//cond = PTHREAD_COND_INITIALIZER;
    pthread_mutex_init(&mu, NULL);
    pthread_cond_init(&cond, NULL);
}

MessageQueue::~MessageQueue() {
	pthread_mutex_destroy(&mu);
    pthread_cond_destroy(&cond);
}

void MessageQueue::exit() {
	pthread_cond_signal(&cond);
}

CommonMessage MessageQueue::popMessage() {
    pthread_mutex_lock(&mu);
    CommonMessage msgBean = msgQueue.front();
    msgQueue.pop();
    pthread_mutex_unlock(&mu);
    return msgBean;
}

CommonMessage MessageQueue::waitPopMessage() {
    pthread_mutex_lock(&mu);
    if (msgQueue.empty()) {
        pthread_cond_wait(&cond, &mu);
    }
    CommonMessage msgBean;
	if(!msgQueue.empty())
	{
		msgBean= msgQueue.front();
		msgQueue.pop();
	}
    pthread_mutex_unlock(&mu);
    return msgBean;
}

bool MessageQueue::empty() {
    return msgQueue.empty();
}

int MessageQueue::size() {
    return msgQueue.size();
}

void MessageQueue::push(CommonMessage msgBean) {
    pthread_mutex_lock(&mu);
    msgQueue.push(msgBean);
    pthread_cond_broadcast(&cond);
    pthread_mutex_unlock(&mu);
}

///////////////////////////////////////////////////////////////////////

class WsMessage
{
public:
    WsMessage() : what(0), obj(NULL){}
    unsigned int what; // message type
    void* obj;
};

/**
 *  @brief socket thread helper, it's used for sending message between UI thread and socket engine.
 */
class SocketEngineDelegateHelper : public cocos2d::CCObject
{
public:
    SocketEngineDelegateHelper();
    ~SocketEngineDelegateHelper();
    
    // Schedule callback function
    virtual void update(float dt);
    
    // Sends message to UI thread. It's needed to be invoked in sub-thread.
    void sendMessageToUIThread(WsMessage *msg);

	inline void setNetEngine(ClientNetEngine* engine) { clientNetEngine = engine; };
    
private:
    std::list<WsMessage*>* _UIWsMessageQueue;
    pthread_mutex_t _UIWsMessageQueueMutex;
	ClientNetEngine* clientNetEngine;
};

// Implementation of SocketEngineDelegateHelper
SocketEngineDelegateHelper::SocketEngineDelegateHelper()
: clientNetEngine(NULL)
{
    _UIWsMessageQueue = new std::list<WsMessage*>();
    pthread_mutex_init(&_UIWsMessageQueueMutex, NULL);
    
    CCDirector::sharedDirector()->getScheduler()->scheduleUpdateForTarget(this, 0, false);
}

SocketEngineDelegateHelper::~SocketEngineDelegateHelper()
{
    CCDirector::sharedDirector()->getScheduler()->unscheduleAllForTarget(this);
    pthread_mutex_destroy(&_UIWsMessageQueueMutex);
    delete _UIWsMessageQueue;
}

void SocketEngineDelegateHelper::sendMessageToUIThread(WsMessage *msg)
{
    pthread_mutex_lock(&_UIWsMessageQueueMutex);
    _UIWsMessageQueue->push_back(msg);
    pthread_mutex_unlock(&_UIWsMessageQueueMutex);
}

void SocketEngineDelegateHelper::update(float dt)
{
    WsMessage *msg = NULL;

    // Returns quickly if no message
    pthread_mutex_lock(&_UIWsMessageQueueMutex);
    if (0 == _UIWsMessageQueue->size())
    {
        pthread_mutex_unlock(&_UIWsMessageQueueMutex);
        return;
    }
    
    // Gets message
    msg = *(_UIWsMessageQueue->begin());
    _UIWsMessageQueue->pop_front();
    pthread_mutex_unlock(&_UIWsMessageQueueMutex);
    
    if (clientNetEngine)
    {
        clientNetEngine->onUIThreadReceiveMessage(msg);
    }
    
    CC_SAFE_DELETE(msg);
}

enum WS_MSG {
    WS_MSG_TO_SUBTRHEAD_SENDING_STRING = 0,
    WS_MSG_TO_SUBTRHEAD_SENDING_BINARY,
    WS_MSG_TO_UITHREAD_OPEN,
    WS_MSG_TO_UITHREAD_MESSAGE,
    WS_MSG_TO_UITHREAD_ERROR,
    WS_MSG_TO_UITHREAD_CLOSE
};

////////////////////////////////////////////////////////////////////////

static ClientNetEngine *s_SharedSocketEngine = NULL;

ClientNetEngine* ClientNetEngine::sharedSocketEngine(void)
{
    if (!s_SharedSocketEngine)
    {
		s_SharedSocketEngine = new ClientNetEngine("", 80);   // default ip and port
    }

    return s_SharedSocketEngine;
}

ClientNetEngine::ClientNetEngine(const char *ip, int port) 
: m_socket(NULL) 
, _readyState(kStateConnecting)
, m_lastRecieveTime(-1)
, _delegateHelper(NULL)
, _needQuit(false)
{
    //this->sendMsgs = new MessageQueue();
	sendMsgs = NULL;
	rcvMsgQueue = NULL;
    this->port = port;
    //this->address.SetHostName(ip, false);
    //std::cout << this->address.GetHostName();
	this->address = ip;
    //mu = PTHREAD_MUTEX_INITIALIZER;
	pthread_mutex_init(&mu, NULL);
}

void ClientNetEngine::setAddress(const char* ip, int port)
{
	this->address = ip;
    this->port = port;
}

ClientNetEngine::~ClientNetEngine() {
	close();

	CC_SAFE_RELEASE_NULL(_delegateHelper);

	if(sendMsgs)
		delete sendMsgs;
	if(rcvMsgQueue)
		delete rcvMsgQueue;
    if (this->m_socket) {
        delete this->m_socket;
    }

	pthread_mutex_destroy(&mu);
}

// For converting static function to member function
class SocketThreadEntry
{
public:
    static void* entry(void* arg)
    {
        ClientNetEngine* self = static_cast<ClientNetEngine*>(arg);
        return self->connect(arg);
    }
};

bool ClientNetEngine::init(const Delegate& delegate) {
	clear();   // clear first, mainly used for re-connection

	_needQuit = false;

	_delegate = const_cast<Delegate*>(&delegate);

	sendMsgs = new MessageQueue();
	rcvMsgQueue = new queue<CommonMessage>();

    pthread_attr_t attr;
    pthread_attr_init (&attr);
//    pthread_attr_setdetachstate (&attr, PTHREAD_CREATE_DETACHED);
    
    // Creates websocket thread
	bool ret = false;
	if (0 == pthread_create(&_subThreadInstance, &attr, SocketThreadEntry::entry, this))
    {
		ret = true;
    }

	// WebSocket thread needs to be invoked at the end of this method.
    _delegateHelper = new SocketEngineDelegateHelper();
	_delegateHelper->setNetEngine(this);

	return ret;
}

void* ClientNetEngine::connect(void* engine) {
	_readyState = kStateConnecting;

    //m_socket = new TCPClientSocket(this->address, this->port);
	m_socket = new ODSocket();
	CCLOG("ODSocket has been newed.");

	int ret = m_socket->Init();	
	if(ret == -1)
	{
		//close();
		onSocketCallback(SE_CALLBACK_CLOSED);
		return NULL;
	}

	bool isok = m_socket->Create(AF_INET,SOCK_STREAM,0);
	if(ret == -1)
	{
		CCLOG("socket is not been created.");
		//close();
		onSocketCallback(SE_CALLBACK_CLOSED);
		return NULL;;
	}

	bool iscon = m_socket->Connect(address.c_str(),port);
	if(!iscon)
	{
		CCLOG("ODSocket has not been connected.");
		//close();
		onSocketCallback(SE_CALLBACK_CLOSED);
		return NULL;
	}

    /*
	if (pthread_create(&pthead_rec, NULL, reciveData, this) != 0) {
		CCLOG("creating recieveDataThread failed.");
		//close();
		onSocketCallback(SE_CALLBACK_CLOSED);
		return NULL;
	}

	if (pthread_create(&pthead_send, NULL, sendData, this) != 0) {
		CCLOG("creating sendDataThread failed.");
		//close();
		onSocketCallback(SE_CALLBACK_CLOSED);
		return NULL;
	}
     */

	// connection is established
	onSocketCallback(SE_CALLBACK_CLIENT_ESTABLISHED);

	return (void*)0;
}

void ClientNetEngine::clear()
{
	if(_delegateHelper)
	{
		CCDirector::sharedDirector()->getScheduler()->unscheduleAllForTarget(_delegateHelper);
		CC_SAFE_RELEASE_NULL(_delegateHelper);
	}

	if(m_socket != NULL)
	{
		m_socket->Close();
		delete m_socket;
		m_socket = NULL;
	}

	if(sendMsgs != NULL)
	{
		delete sendMsgs;
		sendMsgs = NULL;
	}

	if(rcvMsgQueue != NULL)
	{
		delete rcvMsgQueue;
		rcvMsgQueue = NULL;
	}

	setLastRecieveTime(-1);

	//void* ret = NULL;
 //   pthread_join(pthead_rec, &ret);
	//pthread_join(pthead_send, &ret);
	//pthread_join(_subThreadInstance, &ret);
}

void ClientNetEngine::quitSubThread() 
{ 
	_needQuit = true; 

	if(sendMsgs != NULL)
		sendMsgs->exit();
}

void ClientNetEngine::close()
{
	CCDirector::sharedDirector()->getScheduler()->unscheduleAllForTarget(_delegateHelper);

	this->quitSubThread();

	CCLOG("socket (%p) connection closed by client", this);
    _readyState = kStateClosed;

    // onClose callback needs to be invoked at the end of this method
    // since socket instance may be deleted in 'onClose'.
    _delegate->onSocketClose(this);
}

void ClientNetEngine::send(CommonMessage msgBean) 
{
	if(_readyState == ClientNetEngine::kStateOpen)
		sendMsgs->push(msgBean);
}

ClientNetEngine::State ClientNetEngine::getReadyState()
{
    return _readyState;
}

void reverseBytesOrder(char carray[], int offset, int len) {
    int half = len >> 1;
    char v;
    for (int i = 0, j = len - 1; i < half; ++i, --j) {
        v = carray[i + offset];
        carray[i + offset] = carray[j + offset];
        carray[j + offset] = v;
    }
}

void* ClientNetEngine::sendData(void* engine) {
    ClientNetEngine *clientNetEngine = (ClientNetEngine*) engine;
    while (true) {
		if(clientNetEngine->isQuitSubThread())
			break;

		if(clientNetEngine->getReadyState() == ClientNetEngine::kStateOpen)
		{
			CommonMessage msgBean = clientNetEngine->getSendMsgs()->waitPopMessage();
			if(!msgBean.has_cmdid())   // no command id, so it's null message
				continue;

			// 根据消息提长度计算出整个发送长度
			int ackSize = msgBean.ByteSize();
			google::protobuf::uint8 target[4];
			::google::protobuf::io::CodedOutputStream::WriteVarint32ToArray(ackSize, target);
			ackSize = ackSize + sizeof (target);
			char* ackBuf = new char[ackSize];

			//write varint delimiter to buffer
			google::protobuf::io::ArrayOutputStream arrayOut(ackBuf, ackSize);
			google::protobuf::io::CodedOutputStream codedOut(&arrayOut);
			codedOut.WriteVarint32(msgBean.ByteSize());

			//write protobuf ack to buffer
			msgBean.SerializeToCodedStream(&codedOut);
			//   char bts[data.length()];
			//clientEngine->getSocket()->SendData(ackBuf, codedOut.ByteCount(), MSG_DONTWAIT);
			int ret = clientNetEngine->getSocket()->Send(ackBuf, codedOut.ByteCount());
			msgBean.release_data();
			delete[] ackBuf;

			if(ret == -1)
			{
				CCLOG("socket send() failed.");
				clientNetEngine->onSocketCallback(SE_CALLBACK_CLOSED);
				break;
			}
		}
    }
	return NULL;
}

void* ClientNetEngine::reciveData(void* engine) {
    ClientNetEngine *clientNetEngine = (ClientNetEngine*) engine;
    if(clientNetEngine == NULL)
       return NULL;
    // 数据长度读取位置索引
    int lenIndex = 0;
    // 读取到的消息长度
    //    int messageLength = 0;
    google::protobuf::uint32 messageSize;
    // 是否在读取消息长度
    bool readMessageLength = true;
    // 存储接收的数据长度
    char recvLenBuf[sizeof (int)];
    char *recvBodyBuf;
    //    int count = 1;
    // 每次接收的一字节数据
    char recvBuf;
    while (true) {
		if(clientNetEngine->isQuitSubThread())
			break;

        if(clientNetEngine->getReadyState() != ClientNetEngine::kStateOpen)
            CCLOG("why not open???");
        //clientEngine->getSocket()->RecvData(&recvBuf, 1, MSG_DONTWAIT);
		if(clientNetEngine->getReadyState() == ClientNetEngine::kStateOpen)
		{
			int len = clientNetEngine->getSocket()->Recv(&recvBuf, 1);
			if(len <= 0)
			{
				CCLOG("socket recv() failed.");
				clientNetEngine->onSocketCallback(SE_CALLBACK_CLOSED);
				break;
			}

			clientNetEngine->setLastRecieveTime(GameUtils::millisecondNow());

			//CCLOG("recv len: %d", len);
			if (readMessageLength) {
				recvLenBuf[lenIndex++] = recvBuf;
				google::protobuf::io::ArrayInputStream arrayInputStream(recvLenBuf, lenIndex);
				google::protobuf::io::CodedInputStream codedInputStream(&arrayInputStream);
				if (codedInputStream.ReadVarint32(&messageSize)) {
					readMessageLength = false;
					lenIndex = 0;
					recvBodyBuf = new char[messageSize];
					//cout << "收到服务端传来数据长度：" << messageSize << endl;
				}
			} else {
				recvBodyBuf[lenIndex++] = recvBuf;
				if (lenIndex == messageSize) {

					// 解析消息
					google::protobuf::io::ArrayInputStream arrayInputStream(recvBodyBuf, messageSize);
					google::protobuf::io::CodedInputStream codedInputStream(&arrayInputStream);
					CommonMessage p;
					p.ParseFromCodedStream(&codedInputStream);

					// 输出消息
					//clientNetEngine->printMessage(p);
					clientNetEngine->addMsg(p);

					delete[] recvBodyBuf;
					p.release_data();
					lenIndex = 0;
					readMessageLength = true;
				}

			}
		}
    }

	return NULL;
}

void ClientNetEngine::printMessage(CommonMessage message) {
    //cout << "CommonMessage: " << endl;
    //cout << "cmdid: " << message.cmdid() << endl;
    //cout << "data: " << message.data() << endl;
    //com::future::nettytest::protocol::MessagePlayer test;
    //test.ParseFromString(message.data());

    //cout << "message1: " << endl;
    //cout << "from: " << test.from() << endl;
    //cout << "to: " << test.to() << endl;
    //cout << "keycode: " << test.keycode() << endl;
    //message.set_cmdid(1002);

}

CommonMessage ClientNetEngine::getMsg() {
	if(this->rcvMsgQueue == NULL)
	{
		CommonMessage p;
		return p;
	}

    pthread_mutex_lock(&mu);
    if (this->rcvMsgQueue->empty()) {
        pthread_mutex_unlock(&mu);
        CommonMessage p;
        //p.set_cmdid(0);
        return p;
    }
    CommonMessage bean = rcvMsgQueue->front();
    rcvMsgQueue->pop();
    pthread_mutex_unlock(&mu);
    return bean;
}

void ClientNetEngine::addMsg(CommonMessage bean) {
    pthread_mutex_lock(&mu);
    rcvMsgQueue->push(bean);
    pthread_mutex_unlock(&mu);
}

int ClientNetEngine::getMsgSize() {
	if(rcvMsgQueue == NULL)
		return 0;

	return rcvMsgQueue->size();
}

int ClientNetEngine::onSocketCallback(enum socket_callback_reasons reason)
{
	switch (reason)
    {
        case SE_CALLBACK_CLIENT_ESTABLISHED:
            {
                if (pthread_create(&pthead_rec, NULL, reciveData, this) != 0) {
                    CCLOG("creating recieveDataThread failed.");
                    //close();
                    onSocketCallback(SE_CALLBACK_CLOSED);
                    return NULL;
                }
                
                if (pthread_create(&pthead_send, NULL, sendData, this) != 0) {
                    CCLOG("creating sendDataThread failed.");
                    //close();
                    onSocketCallback(SE_CALLBACK_CLOSED);
                    return NULL;
                }
                
                WsMessage* msg = new WsMessage();
                msg->what = WS_MSG_TO_UITHREAD_OPEN;
                _readyState = kStateOpen;

				setEstablishedTime(GameUtils::millisecondNow());

                _delegateHelper->sendMessageToUIThread(msg);
            }
            break;
            
        case SE_CALLBACK_CLOSED:
            {
			    CCLOG("%s", "connection closing..");

				quitSubThread();

                if (_readyState != kStateClosed)
                {
                    WsMessage* msg = new WsMessage();
                    _readyState = kStateClosed;
                    msg->what = WS_MSG_TO_UITHREAD_CLOSE;
                    _delegateHelper->sendMessageToUIThread(msg);
                }
			}
			break;
		default:
            break;
	}

	return 0;
}

void ClientNetEngine::onUIThreadReceiveMessage(WsMessage* msg)
{
    switch (msg->what) {
        case WS_MSG_TO_UITHREAD_OPEN:
            {
                _delegate->onSocketOpen(this);
            }
            break;
        case WS_MSG_TO_UITHREAD_MESSAGE:
            {
                //Data* data = (Data*)msg->obj;
                //_delegate->onMessage(this, *data);
                //CC_SAFE_DELETE_ARRAY(data->bytes);
                //CC_SAFE_DELETE(data);
            }
            break;
        case WS_MSG_TO_UITHREAD_CLOSE:
            {
                _delegate->onSocketClose(this);
            }
            break;
        case WS_MSG_TO_UITHREAD_ERROR:
            {
                // FIXME: The exact error needs to be checked.
                ErrorCode err = kErrorConnectionFailure;
                _delegate->onSocketError(this, err);
            }
            break;
        default:
            break;
    }
}