// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: VipMessage.proto

#define INTERNAL_SUPPRESS_PROTOBUF_FIELD_DEPRECATION
#include "VipMessage.pb.h"

#include <algorithm>

#include <google/protobuf/stubs/common.h>
#include <google/protobuf/stubs/once.h>
#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite_inl.h>
#include <google/protobuf/descriptor.h>
#include <google/protobuf/generated_message_reflection.h>
#include <google/protobuf/reflection_ops.h>
#include <google/protobuf/wire_format.h>
// @@protoc_insertion_point(includes)

namespace com {
namespace future {
namespace threekingdoms {
namespace server {
namespace transport {
namespace protocol {

namespace {

const ::google::protobuf::Descriptor* Req5201_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  Req5201_reflection_ = NULL;
const ::google::protobuf::Descriptor* Rsp5201_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  Rsp5201_reflection_ = NULL;
const ::google::protobuf::Descriptor* Req5202_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  Req5202_reflection_ = NULL;
const ::google::protobuf::Descriptor* Rsp5202_descriptor_ = NULL;
const ::google::protobuf::internal::GeneratedMessageReflection*
  Rsp5202_reflection_ = NULL;

}  // namespace


void protobuf_AssignDesc_VipMessage_2eproto() {
  protobuf_AddDesc_VipMessage_2eproto();
  const ::google::protobuf::FileDescriptor* file =
    ::google::protobuf::DescriptorPool::generated_pool()->FindFileByName(
      "VipMessage.proto");
  GOOGLE_CHECK(file != NULL);
  Req5201_descriptor_ = file->message_type(0);
  static const int Req5201_offsets_[1] = {
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Req5201, viplevel_),
  };
  Req5201_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      Req5201_descriptor_,
      Req5201::default_instance_,
      Req5201_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Req5201, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Req5201, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool(),
      ::google::protobuf::MessageFactory::generated_factory(),
      sizeof(Req5201));
  Rsp5201_descriptor_ = file->message_type(1);
  static const int Rsp5201_offsets_[3] = {
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Rsp5201, viplevel_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Rsp5201, rechargeflag_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Rsp5201, vipremaintime_),
  };
  Rsp5201_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      Rsp5201_descriptor_,
      Rsp5201::default_instance_,
      Rsp5201_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Rsp5201, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Rsp5201, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool(),
      ::google::protobuf::MessageFactory::generated_factory(),
      sizeof(Rsp5201));
  Req5202_descriptor_ = file->message_type(2);
  static const int Req5202_offsets_[1] = {
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Req5202, rewardtype_),
  };
  Req5202_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      Req5202_descriptor_,
      Req5202::default_instance_,
      Req5202_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Req5202, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Req5202, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool(),
      ::google::protobuf::MessageFactory::generated_factory(),
      sizeof(Req5202));
  Rsp5202_descriptor_ = file->message_type(3);
  static const int Rsp5202_offsets_[2] = {
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Rsp5202, rewardprops_),
    GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Rsp5202, rewardtype_),
  };
  Rsp5202_reflection_ =
    new ::google::protobuf::internal::GeneratedMessageReflection(
      Rsp5202_descriptor_,
      Rsp5202::default_instance_,
      Rsp5202_offsets_,
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Rsp5202, _has_bits_[0]),
      GOOGLE_PROTOBUF_GENERATED_MESSAGE_FIELD_OFFSET(Rsp5202, _unknown_fields_),
      -1,
      ::google::protobuf::DescriptorPool::generated_pool(),
      ::google::protobuf::MessageFactory::generated_factory(),
      sizeof(Rsp5202));
}

namespace {

GOOGLE_PROTOBUF_DECLARE_ONCE(protobuf_AssignDescriptors_once_);
inline void protobuf_AssignDescriptorsOnce() {
  ::google::protobuf::GoogleOnceInit(&protobuf_AssignDescriptors_once_,
                 &protobuf_AssignDesc_VipMessage_2eproto);
}

void protobuf_RegisterTypes(const ::std::string&) {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    Req5201_descriptor_, &Req5201::default_instance());
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    Rsp5201_descriptor_, &Rsp5201::default_instance());
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    Req5202_descriptor_, &Req5202::default_instance());
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedMessage(
    Rsp5202_descriptor_, &Rsp5202::default_instance());
}

}  // namespace

void protobuf_ShutdownFile_VipMessage_2eproto() {
  delete Req5201::default_instance_;
  delete Req5201_reflection_;
  delete Rsp5201::default_instance_;
  delete Rsp5201_reflection_;
  delete Req5202::default_instance_;
  delete Req5202_reflection_;
  delete Rsp5202::default_instance_;
  delete Rsp5202_reflection_;
}

void protobuf_AddDesc_VipMessage_2eproto() {
  static bool already_here = false;
  if (already_here) return;
  already_here = true;
  GOOGLE_PROTOBUF_VERIFY_VERSION;

  ::com::future::threekingdoms::server::transport::protocol::protobuf_AddDesc_CopyMessage_2eproto();
  ::google::protobuf::DescriptorPool::InternalAddGeneratedFile(
    "\n\020VipMessage.proto\0222com.future.threeking"
    "doms.server.transport.protocol\032\021CopyMess"
    "age.proto\"\033\n\007Req5201\022\020\n\010vipLevel\030\001 \001(\005\"H"
    "\n\007Rsp5201\022\020\n\010vipLevel\030\001 \001(\005\022\024\n\014rechargeF"
    "lag\030\002 \001(\005\022\025\n\rvipRemainTime\030\003 \001(\003\"\035\n\007Req5"
    "202\022\022\n\nrewardType\030\001 \001(\005\"r\n\007Rsp5202\022S\n\013re"
    "wardProps\030\001 \003(\0132>.com.future.threekingdo"
    "ms.server.transport.protocol.RewardProp\022"
    "\022\n\nrewardType\030\002 \001(\005", 339);
  ::google::protobuf::MessageFactory::InternalRegisterGeneratedFile(
    "VipMessage.proto", &protobuf_RegisterTypes);
  Req5201::default_instance_ = new Req5201();
  Rsp5201::default_instance_ = new Rsp5201();
  Req5202::default_instance_ = new Req5202();
  Rsp5202::default_instance_ = new Rsp5202();
  Req5201::default_instance_->InitAsDefaultInstance();
  Rsp5201::default_instance_->InitAsDefaultInstance();
  Req5202::default_instance_->InitAsDefaultInstance();
  Rsp5202::default_instance_->InitAsDefaultInstance();
  ::google::protobuf::internal::OnShutdown(&protobuf_ShutdownFile_VipMessage_2eproto);
}

// Force AddDescriptors() to be called at static initialization time.
struct StaticDescriptorInitializer_VipMessage_2eproto {
  StaticDescriptorInitializer_VipMessage_2eproto() {
    protobuf_AddDesc_VipMessage_2eproto();
  }
} static_descriptor_initializer_VipMessage_2eproto_;

// ===================================================================

#ifndef _MSC_VER
const int Req5201::kVipLevelFieldNumber;
#endif  // !_MSC_VER

Req5201::Req5201()
  : ::google::protobuf::Message() {
  SharedCtor();
}

void Req5201::InitAsDefaultInstance() {
}

Req5201::Req5201(const Req5201& from)
  : ::google::protobuf::Message() {
  SharedCtor();
  MergeFrom(from);
}

void Req5201::SharedCtor() {
  _cached_size_ = 0;
  viplevel_ = 0;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

Req5201::~Req5201() {
  SharedDtor();
}

void Req5201::SharedDtor() {
  if (this != default_instance_) {
  }
}

void Req5201::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* Req5201::descriptor() {
  protobuf_AssignDescriptorsOnce();
  return Req5201_descriptor_;
}

const Req5201& Req5201::default_instance() {
  if (default_instance_ == NULL) protobuf_AddDesc_VipMessage_2eproto();
  return *default_instance_;
}

Req5201* Req5201::default_instance_ = NULL;

Req5201* Req5201::New() const {
  return new Req5201;
}

void Req5201::Clear() {
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    viplevel_ = 0;
  }
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  mutable_unknown_fields()->Clear();
}

bool Req5201::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!(EXPRESSION)) return false
  ::google::protobuf::uint32 tag;
  while ((tag = input->ReadTag()) != 0) {
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional int32 vipLevel = 1;
      case 1: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_VARINT) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &viplevel_)));
          set_has_viplevel();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectAtEnd()) return true;
        break;
      }

      default: {
      handle_uninterpreted:
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          return true;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, mutable_unknown_fields()));
        break;
      }
    }
  }
  return true;
#undef DO_
}

void Req5201::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // optional int32 vipLevel = 1;
  if (has_viplevel()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(1, this->viplevel(), output);
  }

  if (!unknown_fields().empty()) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        unknown_fields(), output);
  }
}

::google::protobuf::uint8* Req5201::SerializeWithCachedSizesToArray(
    ::google::protobuf::uint8* target) const {
  // optional int32 vipLevel = 1;
  if (has_viplevel()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteInt32ToArray(1, this->viplevel(), target);
  }

  if (!unknown_fields().empty()) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        unknown_fields(), target);
  }
  return target;
}

int Req5201::ByteSize() const {
  int total_size = 0;

  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    // optional int32 vipLevel = 1;
    if (has_viplevel()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->viplevel());
    }

  }
  if (!unknown_fields().empty()) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        unknown_fields());
  }
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void Req5201::MergeFrom(const ::google::protobuf::Message& from) {
  GOOGLE_CHECK_NE(&from, this);
  const Req5201* source =
    ::google::protobuf::internal::dynamic_cast_if_available<const Req5201*>(
      &from);
  if (source == NULL) {
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
    MergeFrom(*source);
  }
}

void Req5201::MergeFrom(const Req5201& from) {
  GOOGLE_CHECK_NE(&from, this);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_viplevel()) {
      set_viplevel(from.viplevel());
    }
  }
  mutable_unknown_fields()->MergeFrom(from.unknown_fields());
}

void Req5201::CopyFrom(const ::google::protobuf::Message& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void Req5201::CopyFrom(const Req5201& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool Req5201::IsInitialized() const {

  return true;
}

void Req5201::Swap(Req5201* other) {
  if (other != this) {
    std::swap(viplevel_, other->viplevel_);
    std::swap(_has_bits_[0], other->_has_bits_[0]);
    _unknown_fields_.Swap(&other->_unknown_fields_);
    std::swap(_cached_size_, other->_cached_size_);
  }
}

::google::protobuf::Metadata Req5201::GetMetadata() const {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::Metadata metadata;
  metadata.descriptor = Req5201_descriptor_;
  metadata.reflection = Req5201_reflection_;
  return metadata;
}


// ===================================================================

#ifndef _MSC_VER
const int Rsp5201::kVipLevelFieldNumber;
const int Rsp5201::kRechargeFlagFieldNumber;
const int Rsp5201::kVipRemainTimeFieldNumber;
#endif  // !_MSC_VER

Rsp5201::Rsp5201()
  : ::google::protobuf::Message() {
  SharedCtor();
}

void Rsp5201::InitAsDefaultInstance() {
}

Rsp5201::Rsp5201(const Rsp5201& from)
  : ::google::protobuf::Message() {
  SharedCtor();
  MergeFrom(from);
}

void Rsp5201::SharedCtor() {
  _cached_size_ = 0;
  viplevel_ = 0;
  rechargeflag_ = 0;
  vipremaintime_ = GOOGLE_LONGLONG(0);
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

Rsp5201::~Rsp5201() {
  SharedDtor();
}

void Rsp5201::SharedDtor() {
  if (this != default_instance_) {
  }
}

void Rsp5201::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* Rsp5201::descriptor() {
  protobuf_AssignDescriptorsOnce();
  return Rsp5201_descriptor_;
}

const Rsp5201& Rsp5201::default_instance() {
  if (default_instance_ == NULL) protobuf_AddDesc_VipMessage_2eproto();
  return *default_instance_;
}

Rsp5201* Rsp5201::default_instance_ = NULL;

Rsp5201* Rsp5201::New() const {
  return new Rsp5201;
}

void Rsp5201::Clear() {
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    viplevel_ = 0;
    rechargeflag_ = 0;
    vipremaintime_ = GOOGLE_LONGLONG(0);
  }
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  mutable_unknown_fields()->Clear();
}

bool Rsp5201::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!(EXPRESSION)) return false
  ::google::protobuf::uint32 tag;
  while ((tag = input->ReadTag()) != 0) {
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional int32 vipLevel = 1;
      case 1: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_VARINT) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &viplevel_)));
          set_has_viplevel();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(16)) goto parse_rechargeFlag;
        break;
      }

      // optional int32 rechargeFlag = 2;
      case 2: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_VARINT) {
         parse_rechargeFlag:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &rechargeflag_)));
          set_has_rechargeflag();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(24)) goto parse_vipRemainTime;
        break;
      }

      // optional int64 vipRemainTime = 3;
      case 3: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_VARINT) {
         parse_vipRemainTime:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int64, ::google::protobuf::internal::WireFormatLite::TYPE_INT64>(
                 input, &vipremaintime_)));
          set_has_vipremaintime();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectAtEnd()) return true;
        break;
      }

      default: {
      handle_uninterpreted:
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          return true;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, mutable_unknown_fields()));
        break;
      }
    }
  }
  return true;
#undef DO_
}

void Rsp5201::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // optional int32 vipLevel = 1;
  if (has_viplevel()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(1, this->viplevel(), output);
  }

  // optional int32 rechargeFlag = 2;
  if (has_rechargeflag()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(2, this->rechargeflag(), output);
  }

  // optional int64 vipRemainTime = 3;
  if (has_vipremaintime()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt64(3, this->vipremaintime(), output);
  }

  if (!unknown_fields().empty()) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        unknown_fields(), output);
  }
}

::google::protobuf::uint8* Rsp5201::SerializeWithCachedSizesToArray(
    ::google::protobuf::uint8* target) const {
  // optional int32 vipLevel = 1;
  if (has_viplevel()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteInt32ToArray(1, this->viplevel(), target);
  }

  // optional int32 rechargeFlag = 2;
  if (has_rechargeflag()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteInt32ToArray(2, this->rechargeflag(), target);
  }

  // optional int64 vipRemainTime = 3;
  if (has_vipremaintime()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteInt64ToArray(3, this->vipremaintime(), target);
  }

  if (!unknown_fields().empty()) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        unknown_fields(), target);
  }
  return target;
}

int Rsp5201::ByteSize() const {
  int total_size = 0;

  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    // optional int32 vipLevel = 1;
    if (has_viplevel()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->viplevel());
    }

    // optional int32 rechargeFlag = 2;
    if (has_rechargeflag()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->rechargeflag());
    }

    // optional int64 vipRemainTime = 3;
    if (has_vipremaintime()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int64Size(
          this->vipremaintime());
    }

  }
  if (!unknown_fields().empty()) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        unknown_fields());
  }
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void Rsp5201::MergeFrom(const ::google::protobuf::Message& from) {
  GOOGLE_CHECK_NE(&from, this);
  const Rsp5201* source =
    ::google::protobuf::internal::dynamic_cast_if_available<const Rsp5201*>(
      &from);
  if (source == NULL) {
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
    MergeFrom(*source);
  }
}

void Rsp5201::MergeFrom(const Rsp5201& from) {
  GOOGLE_CHECK_NE(&from, this);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_viplevel()) {
      set_viplevel(from.viplevel());
    }
    if (from.has_rechargeflag()) {
      set_rechargeflag(from.rechargeflag());
    }
    if (from.has_vipremaintime()) {
      set_vipremaintime(from.vipremaintime());
    }
  }
  mutable_unknown_fields()->MergeFrom(from.unknown_fields());
}

void Rsp5201::CopyFrom(const ::google::protobuf::Message& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void Rsp5201::CopyFrom(const Rsp5201& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool Rsp5201::IsInitialized() const {

  return true;
}

void Rsp5201::Swap(Rsp5201* other) {
  if (other != this) {
    std::swap(viplevel_, other->viplevel_);
    std::swap(rechargeflag_, other->rechargeflag_);
    std::swap(vipremaintime_, other->vipremaintime_);
    std::swap(_has_bits_[0], other->_has_bits_[0]);
    _unknown_fields_.Swap(&other->_unknown_fields_);
    std::swap(_cached_size_, other->_cached_size_);
  }
}

::google::protobuf::Metadata Rsp5201::GetMetadata() const {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::Metadata metadata;
  metadata.descriptor = Rsp5201_descriptor_;
  metadata.reflection = Rsp5201_reflection_;
  return metadata;
}


// ===================================================================

#ifndef _MSC_VER
const int Req5202::kRewardTypeFieldNumber;
#endif  // !_MSC_VER

Req5202::Req5202()
  : ::google::protobuf::Message() {
  SharedCtor();
}

void Req5202::InitAsDefaultInstance() {
}

Req5202::Req5202(const Req5202& from)
  : ::google::protobuf::Message() {
  SharedCtor();
  MergeFrom(from);
}

void Req5202::SharedCtor() {
  _cached_size_ = 0;
  rewardtype_ = 0;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

Req5202::~Req5202() {
  SharedDtor();
}

void Req5202::SharedDtor() {
  if (this != default_instance_) {
  }
}

void Req5202::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* Req5202::descriptor() {
  protobuf_AssignDescriptorsOnce();
  return Req5202_descriptor_;
}

const Req5202& Req5202::default_instance() {
  if (default_instance_ == NULL) protobuf_AddDesc_VipMessage_2eproto();
  return *default_instance_;
}

Req5202* Req5202::default_instance_ = NULL;

Req5202* Req5202::New() const {
  return new Req5202;
}

void Req5202::Clear() {
  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    rewardtype_ = 0;
  }
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  mutable_unknown_fields()->Clear();
}

bool Req5202::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!(EXPRESSION)) return false
  ::google::protobuf::uint32 tag;
  while ((tag = input->ReadTag()) != 0) {
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // optional int32 rewardType = 1;
      case 1: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_VARINT) {
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &rewardtype_)));
          set_has_rewardtype();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectAtEnd()) return true;
        break;
      }

      default: {
      handle_uninterpreted:
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          return true;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, mutable_unknown_fields()));
        break;
      }
    }
  }
  return true;
#undef DO_
}

void Req5202::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // optional int32 rewardType = 1;
  if (has_rewardtype()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(1, this->rewardtype(), output);
  }

  if (!unknown_fields().empty()) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        unknown_fields(), output);
  }
}

::google::protobuf::uint8* Req5202::SerializeWithCachedSizesToArray(
    ::google::protobuf::uint8* target) const {
  // optional int32 rewardType = 1;
  if (has_rewardtype()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteInt32ToArray(1, this->rewardtype(), target);
  }

  if (!unknown_fields().empty()) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        unknown_fields(), target);
  }
  return target;
}

int Req5202::ByteSize() const {
  int total_size = 0;

  if (_has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    // optional int32 rewardType = 1;
    if (has_rewardtype()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->rewardtype());
    }

  }
  if (!unknown_fields().empty()) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        unknown_fields());
  }
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void Req5202::MergeFrom(const ::google::protobuf::Message& from) {
  GOOGLE_CHECK_NE(&from, this);
  const Req5202* source =
    ::google::protobuf::internal::dynamic_cast_if_available<const Req5202*>(
      &from);
  if (source == NULL) {
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
    MergeFrom(*source);
  }
}

void Req5202::MergeFrom(const Req5202& from) {
  GOOGLE_CHECK_NE(&from, this);
  if (from._has_bits_[0 / 32] & (0xffu << (0 % 32))) {
    if (from.has_rewardtype()) {
      set_rewardtype(from.rewardtype());
    }
  }
  mutable_unknown_fields()->MergeFrom(from.unknown_fields());
}

void Req5202::CopyFrom(const ::google::protobuf::Message& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void Req5202::CopyFrom(const Req5202& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool Req5202::IsInitialized() const {

  return true;
}

void Req5202::Swap(Req5202* other) {
  if (other != this) {
    std::swap(rewardtype_, other->rewardtype_);
    std::swap(_has_bits_[0], other->_has_bits_[0]);
    _unknown_fields_.Swap(&other->_unknown_fields_);
    std::swap(_cached_size_, other->_cached_size_);
  }
}

::google::protobuf::Metadata Req5202::GetMetadata() const {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::Metadata metadata;
  metadata.descriptor = Req5202_descriptor_;
  metadata.reflection = Req5202_reflection_;
  return metadata;
}


// ===================================================================

#ifndef _MSC_VER
const int Rsp5202::kRewardPropsFieldNumber;
const int Rsp5202::kRewardTypeFieldNumber;
#endif  // !_MSC_VER

Rsp5202::Rsp5202()
  : ::google::protobuf::Message() {
  SharedCtor();
}

void Rsp5202::InitAsDefaultInstance() {
}

Rsp5202::Rsp5202(const Rsp5202& from)
  : ::google::protobuf::Message() {
  SharedCtor();
  MergeFrom(from);
}

void Rsp5202::SharedCtor() {
  _cached_size_ = 0;
  rewardtype_ = 0;
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
}

Rsp5202::~Rsp5202() {
  SharedDtor();
}

void Rsp5202::SharedDtor() {
  if (this != default_instance_) {
  }
}

void Rsp5202::SetCachedSize(int size) const {
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
}
const ::google::protobuf::Descriptor* Rsp5202::descriptor() {
  protobuf_AssignDescriptorsOnce();
  return Rsp5202_descriptor_;
}

const Rsp5202& Rsp5202::default_instance() {
  if (default_instance_ == NULL) protobuf_AddDesc_VipMessage_2eproto();
  return *default_instance_;
}

Rsp5202* Rsp5202::default_instance_ = NULL;

Rsp5202* Rsp5202::New() const {
  return new Rsp5202;
}

void Rsp5202::Clear() {
  if (_has_bits_[1 / 32] & (0xffu << (1 % 32))) {
    rewardtype_ = 0;
  }
  rewardprops_.Clear();
  ::memset(_has_bits_, 0, sizeof(_has_bits_));
  mutable_unknown_fields()->Clear();
}

bool Rsp5202::MergePartialFromCodedStream(
    ::google::protobuf::io::CodedInputStream* input) {
#define DO_(EXPRESSION) if (!(EXPRESSION)) return false
  ::google::protobuf::uint32 tag;
  while ((tag = input->ReadTag()) != 0) {
    switch (::google::protobuf::internal::WireFormatLite::GetTagFieldNumber(tag)) {
      // repeated .com.future.threekingdoms.server.transport.protocol.RewardProp rewardProps = 1;
      case 1: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_LENGTH_DELIMITED) {
         parse_rewardProps:
          DO_(::google::protobuf::internal::WireFormatLite::ReadMessageNoVirtual(
                input, add_rewardprops()));
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectTag(10)) goto parse_rewardProps;
        if (input->ExpectTag(16)) goto parse_rewardType;
        break;
      }

      // optional int32 rewardType = 2;
      case 2: {
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_VARINT) {
         parse_rewardType:
          DO_((::google::protobuf::internal::WireFormatLite::ReadPrimitive<
                   ::google::protobuf::int32, ::google::protobuf::internal::WireFormatLite::TYPE_INT32>(
                 input, &rewardtype_)));
          set_has_rewardtype();
        } else {
          goto handle_uninterpreted;
        }
        if (input->ExpectAtEnd()) return true;
        break;
      }

      default: {
      handle_uninterpreted:
        if (::google::protobuf::internal::WireFormatLite::GetTagWireType(tag) ==
            ::google::protobuf::internal::WireFormatLite::WIRETYPE_END_GROUP) {
          return true;
        }
        DO_(::google::protobuf::internal::WireFormat::SkipField(
              input, tag, mutable_unknown_fields()));
        break;
      }
    }
  }
  return true;
#undef DO_
}

void Rsp5202::SerializeWithCachedSizes(
    ::google::protobuf::io::CodedOutputStream* output) const {
  // repeated .com.future.threekingdoms.server.transport.protocol.RewardProp rewardProps = 1;
  for (int i = 0; i < this->rewardprops_size(); i++) {
    ::google::protobuf::internal::WireFormatLite::WriteMessageMaybeToArray(
      1, this->rewardprops(i), output);
  }

  // optional int32 rewardType = 2;
  if (has_rewardtype()) {
    ::google::protobuf::internal::WireFormatLite::WriteInt32(2, this->rewardtype(), output);
  }

  if (!unknown_fields().empty()) {
    ::google::protobuf::internal::WireFormat::SerializeUnknownFields(
        unknown_fields(), output);
  }
}

::google::protobuf::uint8* Rsp5202::SerializeWithCachedSizesToArray(
    ::google::protobuf::uint8* target) const {
  // repeated .com.future.threekingdoms.server.transport.protocol.RewardProp rewardProps = 1;
  for (int i = 0; i < this->rewardprops_size(); i++) {
    target = ::google::protobuf::internal::WireFormatLite::
      WriteMessageNoVirtualToArray(
        1, this->rewardprops(i), target);
  }

  // optional int32 rewardType = 2;
  if (has_rewardtype()) {
    target = ::google::protobuf::internal::WireFormatLite::WriteInt32ToArray(2, this->rewardtype(), target);
  }

  if (!unknown_fields().empty()) {
    target = ::google::protobuf::internal::WireFormat::SerializeUnknownFieldsToArray(
        unknown_fields(), target);
  }
  return target;
}

int Rsp5202::ByteSize() const {
  int total_size = 0;

  if (_has_bits_[1 / 32] & (0xffu << (1 % 32))) {
    // optional int32 rewardType = 2;
    if (has_rewardtype()) {
      total_size += 1 +
        ::google::protobuf::internal::WireFormatLite::Int32Size(
          this->rewardtype());
    }

  }
  // repeated .com.future.threekingdoms.server.transport.protocol.RewardProp rewardProps = 1;
  total_size += 1 * this->rewardprops_size();
  for (int i = 0; i < this->rewardprops_size(); i++) {
    total_size +=
      ::google::protobuf::internal::WireFormatLite::MessageSizeNoVirtual(
        this->rewardprops(i));
  }

  if (!unknown_fields().empty()) {
    total_size +=
      ::google::protobuf::internal::WireFormat::ComputeUnknownFieldsSize(
        unknown_fields());
  }
  GOOGLE_SAFE_CONCURRENT_WRITES_BEGIN();
  _cached_size_ = total_size;
  GOOGLE_SAFE_CONCURRENT_WRITES_END();
  return total_size;
}

void Rsp5202::MergeFrom(const ::google::protobuf::Message& from) {
  GOOGLE_CHECK_NE(&from, this);
  const Rsp5202* source =
    ::google::protobuf::internal::dynamic_cast_if_available<const Rsp5202*>(
      &from);
  if (source == NULL) {
    ::google::protobuf::internal::ReflectionOps::Merge(from, this);
  } else {
    MergeFrom(*source);
  }
}

void Rsp5202::MergeFrom(const Rsp5202& from) {
  GOOGLE_CHECK_NE(&from, this);
  rewardprops_.MergeFrom(from.rewardprops_);
  if (from._has_bits_[1 / 32] & (0xffu << (1 % 32))) {
    if (from.has_rewardtype()) {
      set_rewardtype(from.rewardtype());
    }
  }
  mutable_unknown_fields()->MergeFrom(from.unknown_fields());
}

void Rsp5202::CopyFrom(const ::google::protobuf::Message& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

void Rsp5202::CopyFrom(const Rsp5202& from) {
  if (&from == this) return;
  Clear();
  MergeFrom(from);
}

bool Rsp5202::IsInitialized() const {

  return true;
}

void Rsp5202::Swap(Rsp5202* other) {
  if (other != this) {
    rewardprops_.Swap(&other->rewardprops_);
    std::swap(rewardtype_, other->rewardtype_);
    std::swap(_has_bits_[0], other->_has_bits_[0]);
    _unknown_fields_.Swap(&other->_unknown_fields_);
    std::swap(_cached_size_, other->_cached_size_);
  }
}

::google::protobuf::Metadata Rsp5202::GetMetadata() const {
  protobuf_AssignDescriptorsOnce();
  ::google::protobuf::Metadata metadata;
  metadata.descriptor = Rsp5202_descriptor_;
  metadata.reflection = Rsp5202_reflection_;
  return metadata;
}


// @@protoc_insertion_point(namespace_scope)

}  // namespace protocol
}  // namespace transport
}  // namespace server
}  // namespace threekingdoms
}  // namespace future
}  // namespace com

// @@protoc_insertion_point(global_scope)
