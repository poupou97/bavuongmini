
#ifndef _MESSAGECLIENT_CATTACHENPROPS_H_
#define _MESSAGECLIENT_CATTACHENPROPS_H_

#include "../GameProtobuf.h"
#include "cocos2d.h"
#include "../protobuf/MailMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CAttachedProps : public AttachedProps
{
public:
	CAttachedProps();
	~CAttachedProps();
};

#endif

