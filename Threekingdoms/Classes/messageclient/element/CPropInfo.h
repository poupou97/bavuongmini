#ifndef _MESSAGECLIENT_CPROPINFO_H_
#define _MESSAGECLIENT_CPROPINFO_H_

#include "../GameProtobuf.h"
#include "../protobuf/RecruitMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CPropInfo
{
public:
	CPropInfo() ;
	virtual ~CPropInfo() ;

private:
	std::string id;
	int idx;
	std::string name;
	int type;
	std::string icon;
	int quality;
public:
	void set_id(std::string strId){id = strId;};
	std::string get_id(){return id;};

	void set_idx(int nIdx){idx = nIdx;};
	int get_idx(){return idx;};

	void set_name(std::string strName){name = strName;};
	std::string get_name(){return name;};

	void set_type(int nType){type = nType;};
	int get_type(){return type;};

	void set_icon(std::string strIcon){icon = strIcon;};
	std::string get_icon(){return icon;};

	void set_quality(int nQuality){quality = nQuality;};
	int get_quality(){return quality;};
} ;

#endif
