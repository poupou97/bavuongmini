#include "CRechargeInfo.h"


CRechargeInfo::CRechargeInfo(void)
{
}


CRechargeInfo::~CRechargeInfo(void)
{
}

void CRechargeInfo::set_id( int _value )
{
	id = _value;
}

int CRechargeInfo::get_id()
{
	return id;
}

void CRechargeInfo::set_recharge_value( int _value )
{
	recharge_value = _value;
}

int CRechargeInfo::get_recharge_value()
{
	return recharge_value;
}

void CRechargeInfo::set_send_value( int _value )
{
	send_value = _value;
}

int CRechargeInfo::get_send_value()
{
	return send_value;
}

void CRechargeInfo::set_suggest( int _value )
{
	suggest = _value;
}

int CRechargeInfo::get_suggest()
{
	return suggest;
}

void CRechargeInfo::set_icon( std::string _value )
{
	icon = _value;
}

std::string CRechargeInfo::get_icon()
{
	return icon;
}

void CRechargeInfo::set_orderId( int _value )
{
	orderId = _value;
}

int CRechargeInfo::get_orderId()
{
	return orderId;
}
