#ifndef _MESSAGECLIENT_ELEMENT_CMONSTERBASEINFO_H_
#define _MESSAGECLIENT_ELEMENT_CMONSTERBASEINFO_H_

#include <string>

class CMonsterBaseInfo
{
public:
	enum monster_clazz {
		// type: 1: normal, 2: guard, 3: elite, 4: boss, 5: world boss
		clazz_normal = 1,
		clazz_guard = 2,
		clazz_elite = 3,
		clazz_boss = 4,
		clazz_world_boss = 5,
	};

public:
	CMonsterBaseInfo();
	~CMonsterBaseInfo();

	int id;
	std::string name;
	int level;
	int clazz;   // enum monster_clazz
};

#endif

