#ifndef _MESSAGECLIENT_CSHOWGENERALINFO_H_
#define _MESSAGECLIENT_CSHOWGENERALINFO_H_

#include "../GameProtobuf.h"
#include "../protobuf/ModelMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CShowGeneralInfo: public ShowGeneralInfo
{
public:
    CShowGeneralInfo() ;
    virtual ~CShowGeneralInfo() ;

} ;

#endif
