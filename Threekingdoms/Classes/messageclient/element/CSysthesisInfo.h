#ifndef _MESSAGECLIENT_ELEMENT_CSYSTHESISINFO_H_
#define _MESSAGECLIENT_ELEMENT_CSYSTHESISINFO_H_

#include <string>
#include <map>

class CSysthesisInfo
{
public:
	CSysthesisInfo(void);
	~CSysthesisInfo(void);

private:
	int id;
	std::string merge_prop_id;
	std::string output_prop_id;
	int need_num;
	int need_price;
	int output_num;

	std::string merge_prop_name;
	std::string merge_prop_icon;
	int merge_prop_quality;
	std::string output_prop_name;
	std::string output_prop_icon;
	int output_prop_quality;

	std::string m_goodsName;

public:
	void set_id(int _value);
	int get_id();
	void set_merge_prop_id(std::string _value);
	std::string get_merge_prop_id();
	void set_output_prop_id(std::string _value);
	std::string get_output_prop_id();
	void set_need_num(int _value);
	int get_need_num();
	void set_need_price(int _value);
	int get_need_price();
	void set_output_num(int _value);
	int get_output_num();
	

	void set_merge_prop_name(std::string _value);
	std::string get_merge_prop_name();
	void set_merge_prop_icon(std::string _value);
	std::string get_merge_prop_icon();
	void set_merge_prop_quality(int _value);
	int get_merge_prop_quality();
	void set_output_prop_name(std::string _value);
	std::string get_output_prop_name();
	void set_output_prop_icon(std::string _value);
	std::string get_output_prop_icon();
	void set_output_prop_quality(int _value);
	int get_output_prop_quality();

	void set_typename(std::string _value);
	std::string get_typename();

	void CopyFrom(CSysthesisInfo* info);
};

#endif

