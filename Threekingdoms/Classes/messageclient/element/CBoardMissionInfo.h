
#ifndef _MESSAGECLIENT_CBOARDMISSIONINFO_H_
#define _MESSAGECLIENT_CBOARDMISSIONINFO_H_

#include "../../common/CKBaseClass.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CBoardMissionInfo : public BoardMissionInfo
{
public:
	CBoardMissionInfo();
	~CBoardMissionInfo();
};
#endif;

