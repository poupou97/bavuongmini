#ifndef _MESSAGECLIENT_CTARGETNPC_H_
#define _MESSAGECLIENT_CTARGETNPC_H_

#include "../GameProtobuf.h"
#include "../protobuf/MissionMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CTargetNpc:public TargetNpc
{
public:
	CTargetNpc(void);
	~CTargetNpc(void);
};
#endif;
