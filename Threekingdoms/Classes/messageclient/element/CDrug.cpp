#include "CDrug.h"
#include "../../utils/GameUtils.h"
#include "../../utils/StaticDataManager.h"

CDrug::CDrug()
: m_bIsInCD(false)
{
}

CDrug::~CDrug()
{
}

void CDrug::set_id( int _value )
{
	id = _value;
}

int CDrug::get_id()
{
	return id;
}

void CDrug::set_prop_id( std::string _value )
{
	prop_id = _value;
}

std::string CDrug::get_prop_id()
{
	return prop_id;
}

void CDrug::set_hp( int _value )
{
	hp = _value;
}

int CDrug::get_hp()
{
	return hp;
}

void CDrug::set_mp( int _value )
{
	mp = _value;
}

int CDrug::get_mp()
{
	return mp;
}

void CDrug::set_once_hp( int _value )
{
	once_hp = _value;
}

int CDrug::get_once_hp()
{
	return once_hp;
}

void CDrug::set_once_mp( int _value )
{
	once_mp = _value;
}

int CDrug::get_once_mp()
{
	return once_mp;
}

void CDrug::set_once_hp_upper( int _value )
{
	once_hp_upper = _value;
}

int CDrug::get_once_hp_upper()
{
	return once_hp_upper;
}

void CDrug::set_once_mp_upper( int _value )
{
	once_mp_upper = _value;
}

int CDrug::get_once_mp_upper()
{
	return once_mp_upper;
}

void CDrug::set_type_flag( int _value )
{
	type_flag = _value;
}

int CDrug::get_type_flag()
{
	return type_flag;
}

void CDrug::set_cool_time( int _value )
{
	cool_time = _value;
}

int CDrug::get_cool_time()
{
	return cool_time;
}

void CDrug::startCD()
{
	coolTimeCount = GameUtils::millisecondNow();
	m_bIsInCD = true;
}
void CDrug::updateCD()
{
	if(m_bIsInCD)
	{
		remainTime = (cool_time + 500) - (GameUtils::millisecondNow() - coolTimeCount);
		if(remainTime <= 0)
		{
			m_bIsInCD = false;
		}
	}
}
bool CDrug::isInCD()
{
	return m_bIsInCD;
}

bool CDrug::isDrug(std::string propId)
{
	std::map<std::string,CDrug*>::const_iterator cIter;
	cIter = CDrugMsgConfigData::s_drugBaseMsg.find(propId);
	if (cIter != CDrugMsgConfigData::s_drugBaseMsg.end())
	{
		return true;
	}

	return false;
}

long long CDrug::getRemainTime()
{
	if(m_bIsInCD)
	{
		return remainTime;
	}
	else
	{
		return 0;
	}
}


////////////////////////////////////////////////////////

DrugManager::DrugManager()
{
}

DrugManager::~DrugManager()
{
}

static DrugManager *s_SharedDrugManager = NULL;

DrugManager* DrugManager::getInstance(void)
{
    if (!s_SharedDrugManager)
    {
		s_SharedDrugManager = new DrugManager();
    }

    return s_SharedDrugManager;
}

void DrugManager::update(float dt)
{
    for (std::map<std::string,CDrug*>::iterator it = m_DrugMap.begin(); it != m_DrugMap.end(); ++it) 
	{
		CDrug* pDrug = it->second;
		if(pDrug != NULL)
			pDrug->updateCD();
	}
}

CDrug* DrugManager::getDrugById(std::string drugId)
{
	if(m_DrugMap[drugId] == NULL)
	{
		CDrug* newDrug = new CDrug();
		newDrug->set_prop_id(drugId);

		std::map<std::string,CDrug*>::const_iterator cIter;
		cIter = CDrugMsgConfigData::s_drugBaseMsg.find(drugId);
		if (cIter != CDrugMsgConfigData::s_drugBaseMsg.end())
		{
			CDrug * pDrug =  cIter->second;
			newDrug->set_cool_time(pDrug->get_cool_time());
		}
		else
		{
			CCAssert(false, "should be found");
		}

		m_DrugMap[drugId] = newDrug;
	}

	return m_DrugMap[drugId];
}
