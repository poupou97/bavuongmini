
#ifndef _MESSAGECLIENT_CFIVEPERSIONINSTANCE_H_
#define _MESSAGECLIENT_CFIVEPERSIONINSTANCE_H_

#include "../GameProtobuf.h"

class CFivePersonInstance
{
public:
	CFivePersonInstance();
	~CFivePersonInstance();

	void set_id(int _value);
	int get_id();
	void set_keyValue(std::string _value);
	std::string get_keyValue();
	void set_name(std::string _value);
	std::string get_name();
	void set_difficult(int _value);
	int get_difficult();
	void set_open_level(int _value);
	int get_open_level();
	void set_day_time(int _value);
	int get_day_time();
	void set_suggest_num(int _value);
	int get_suggest_num();
	void set_start_time(std::string _value);
	std::string get_start_time();
	void set_end_time(std::string _value);
	std::string get_end_time();
	void set_consume(int _value);
	int get_consume();
	void set_map_id(std::string _value);
	std::string get_map_id();
	void set_enter_x(int _value);
	int get_enter_x();
	void set_enter_y(int _value);
	int get_enter_y();
	void set_reward_xp(int _value);
	int get_reward_xp();
	void set_reward_gold(int _value);
	int get_reward_gold();
	void set_reward_gold_type(int _value);
	int get_reward_gold_type();
	void set_description(std::string _value);
	std::string get_description();
	void set_follow_map(std::string _value);
	std::string get_follow_map();
	void set_fight_point(int _value);
	int get_fight_point();
	void set_picture(std::string _value);
	std::string get_picture();
	void set_dragon_value(int _value);
	int get_dragon_value();
	void set_boss_count(int _value);
	int get_boss_count();
	void set_monster_count(int _value);
	int get_monster_count();

private:
	int id;
	std::string keyValue;
	std::string name;
	int difficult;
	int open_level;
	int day_time;
	int suggest_num;
	std::string start_time;
	std::string end_time;
	int consume;
	std::string map_id;
	int enter_x;
	int enter_y;
	int reward_xp;
	int reward_gold;
	int reward_gold_type;
	std::string description;
	std::string follow_map;
	int fight_point;
	std::string picture;
	int dragon_value;
	int boss_count;
	int monster_count;
};

#endif