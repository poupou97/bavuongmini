
#ifndef _SINGCOPYCLAZZ_CLAZZ_H
#define _SINGCOPYCLAZZ_CLAZZ_H

#include "../GameProtobuf.h"

#include "cocos2d.h"
USING_NS_THREEKINGDOMS_PROTOCOL;
class CSingCopyClazz
{
public:
	CSingCopyClazz();
	~CSingCopyClazz();

	void set_clazz(int vale_);
	int get_clazz();

	void set_starLevel(int vale_);
	int get_starLevel();

	void set_endLevel(int vale_);
	int get_endLevel();

	void set_name(std::string value_);
	std::string get_name();

	void set_icon(std::string value_);
	std::string get_icon();

	void set_resume(int value_);
	int get_resume();

	void set_reward(std::string value_);
	std::string get_reward();
private:
	int m_clazz;
	int m_starlv;
	int m_endlv;
	std::string m_name;
	std::string m_icon;
	int m_resume;
	std::string m_reward;
};
#endif
