#include "CPowerSendInfo.h"


CPowerSendInfo::CPowerSendInfo()
{
}


CPowerSendInfo::~CPowerSendInfo()
{
}

void CPowerSendInfo::set_id( int nId )
{
	this->m_nId = nId;
}

int CPowerSendInfo::get_id()
{
	return m_nId;
}

void CPowerSendInfo::set_sendTime( std::string strSendTime )
{
	this->m_strSendTime = strSendTime;
}

std::string CPowerSendInfo::get_sendTime()
{
	return m_strSendTime;
}

void CPowerSendInfo::set_powerValue( int nPowerValue )
{
	this->m_nPowerValue = nPowerValue;
}

int CPowerSendInfo::get_powerValue()
{
	return this->m_nPowerValue;
}
