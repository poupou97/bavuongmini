
#ifndef _MESSAGECLIENT_CMAPINFO_H_
#define _MESSAGECLIENT_CMAPINFO_H_

#include "../protobuf/PlayerMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CMapInfo : public Push1106
{
public:
	// 国家ID
	enum country_define {
		country_none = 0,   // 公共地图
		country_1,
		country_2,
		country_3,
	};

	// 国家总数
	enum {
		country_max = 3,
	};

public:
    CMapInfo() ;
    virtual ~CMapInfo() ;

	static const char* getCountryName(int countryId);
	static const char* getCountrySimpleName(int countryId);

	static const char* getCountryStr(int countryId);			// LiuLiang add

	void setLastMapId(std::string _value);
	std::string getLastMapId();

private:
	std::string m_sLastMapId;
} ;

#endif
