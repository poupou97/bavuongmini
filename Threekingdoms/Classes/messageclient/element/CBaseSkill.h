
#ifndef _MESSAGECLIENT_CBASESKILL_H_
#define _MESSAGECLIENT_CBASESKILL_H_

#include "../../common/CKBaseClass.h"
#include "../protobuf/ModelMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CBaseSkill : public SkillBase
{
public:
	CBaseSkill();
	~CBaseSkill();

	void set_profession(int _value);
	int get_profession();
	void set_quality(int _value);
	int get_quality();
	void set_complex_order(int _value);
	int get_complex_order();
	void set_skill_variety(int _value);
	int get_skill_variety();
	void set_owner(int _value);
	int get_owner();

private:
	int profession;
	int quality;
	int complex_order;
	//1:����2:Ⱥ��3:�أ�4:�ƣ�5:�أ�6:��
	int skill_variety;

	int owner;
};
#endif;

