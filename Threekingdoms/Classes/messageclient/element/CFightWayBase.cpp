#include "CFightWayBase.h"


CFightWayBase::CFightWayBase()
{
}


CFightWayBase::~CFightWayBase()
{
}

void CFightWayBase::set_name( std::string _value )
{
	name = _value;
}

std::string CFightWayBase::get_name()
{
	return name;
}

void CFightWayBase::set_kValue( std::string _value )
{
	kValue = _value;
}

std::string CFightWayBase::get_kValue()
{
	return kValue;
}

void CFightWayBase::set_icon( std::string _value )
{
	icon = _value;
}

std::string CFightWayBase::get_icon()
{
	return icon;
}

void CFightWayBase::set_description( std::string _value )
{
	description = _value;
}

std::string CFightWayBase::get_description()
{
	return description;
}

void CFightWayBase::set_sortingOrder( int _value )
{
	sortingOrder = _value;
}

int CFightWayBase::get_sortingOrder()
{
	return sortingOrder;
}
