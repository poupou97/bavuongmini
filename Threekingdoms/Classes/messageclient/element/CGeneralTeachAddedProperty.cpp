#include "CGeneralTeachAddedProperty.h"

CGeneralTeachAddedProperty::CGeneralTeachAddedProperty()
{
}


CGeneralTeachAddedProperty::~CGeneralTeachAddedProperty()
{
}

int CGeneralTeachAddedProperty::get_id()
{
	return id;
}

void CGeneralTeachAddedProperty::set_id( int _value )
{
	id = _value;
}

int CGeneralTeachAddedProperty::get_quality()
{
	return quality;
}

void CGeneralTeachAddedProperty::set_quality( int _value )
{
	quality = _value;
}

int CGeneralTeachAddedProperty::get_star()
{
	return star;
}

void CGeneralTeachAddedProperty::set_star( int _value )
{
	star = _value;
}

int CGeneralTeachAddedProperty::get_profession()
{
	return profession;
}

void CGeneralTeachAddedProperty::set_profession( int _value )
{
	profession = _value;
}

int CGeneralTeachAddedProperty::get_hp_capacity()
{
	return hp_capacity;
}

void CGeneralTeachAddedProperty::set_hp_capacity( int _value )
{
	hp_capacity = _value;
}

int CGeneralTeachAddedProperty::get_max_attack()
{
	return max_attack;
}

void CGeneralTeachAddedProperty::set_max_attack( int _value )
{
	max_attack = _value;
}

int CGeneralTeachAddedProperty::get_max_magic_attack()
{
	return max_magic_attack;
}

void CGeneralTeachAddedProperty::set_max_magic_attack( int _value )
{
	max_magic_attack = _value;
}