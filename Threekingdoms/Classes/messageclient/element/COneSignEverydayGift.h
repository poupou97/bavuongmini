#ifndef _MESSAGECLIENT_CONELINEGIFT_H_
#define _MESSAGECLIENT_CONELINEGIFT_H_

#include "../GameProtobuf.h"
#include "../protobuf/DailyGiftSign.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class COneSignEverydayGift:public OneSignEverydayGift
{
public:
	COneSignEverydayGift(void);
	~COneSignEverydayGift(void);
};
#endif;
