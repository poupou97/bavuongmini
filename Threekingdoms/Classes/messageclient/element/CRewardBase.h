#ifndef _MESSAGECLIENT_ELEMENT_CREWARDBASE_H_
#define _MESSAGECLIENT_ELEMENT_CREWARDBASE_H_

#include <string>
#include <map>

class CRewardBase
{
public:
	CRewardBase();
	~CRewardBase();

public:
	void setId(int id);
	int getId();

	void setName(std::string name);
	std::string getName();

	void setIcon(std::string icon);
	std::string getIcon();

	void setDes(std::string des);
	std::string getDes();

	void setClose(int value_);
	int getClose();

	void setRewardState(int value_);
	int getRewardState();
private:
	int m_id;
	std::string m_name;
	std::string m_icon;
	std::string m_des;
	int m_isClose;
	int m_state;
};

#endif

