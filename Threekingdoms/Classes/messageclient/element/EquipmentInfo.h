
#ifndef _MESSAGECLIENT_EQUIPMENTINFO_H_
#define _MESSAGECLIENT_EQUIPMENTINFO_H_

#include "../protobuf/ModelMessage.pb.h"

#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class EquipmentInfo : public Equipment
{
public:
	EquipmentInfo();
	~EquipmentInfo();
};
#endif;