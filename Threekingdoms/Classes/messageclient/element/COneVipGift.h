
#ifndef _MESSAGECLIENT_CONEVIPGIFT_H_
#define _MESSAGECLIENT_CONEVIPGIFT_H_

#include "../GameProtobuf.h"
#include "../protobuf/DailyGiftSign.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class COneVipGift : public OneVipGift
{
public:
	COneVipGift();
	~COneVipGift();
};

#endif