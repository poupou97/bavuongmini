
#ifndef _MESSAGECLIENT_CFIGHTWAYBASE_H_
#define _MESSAGECLIENT_CFIGHTWAYBASE_H_

#include "../../common/CKBaseClass.h"
#include "../GameProtobuf.h"
#include "../protobuf/GeneralMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CFightWayBase : public FightWayBase
{
public:
	CFightWayBase();
	virtual ~CFightWayBase();

	void set_name(std::string _value);
	std::string get_name();
	void set_kValue(std::string _value);
	std::string get_kValue();
	void set_icon(std::string _value);
	std::string get_icon();
	void set_description(std::string _value);
	std::string get_description();
	void set_sortingOrder(int _value);
	int get_sortingOrder();

private:
	std::string name ;
	std::string kValue ;
	std::string icon ;
	std::string description ;
	int sortingOrder;
};

#endif
