#include "PushHandler2803.h"
#include "../protobuf/QuestionMessage.pb.h"
#include "../../ui/Question_ui/QuestionData.h"
#include "../../GameView.h"
#include "../GameMessageProcessor.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/Question_ui/QuestionUI.h"
#include "../../gamescene_state/MainScene.h"

#define  PAGE_NUM 10

IMPLEMENT_CLASS(PushHandler2803)

PushHandler2803::PushHandler2803() 
{

}
PushHandler2803::~PushHandler2803() 
{
	
}
void* PushHandler2803::createInstance()
{
	return new PushHandler2803() ;
}
void PushHandler2803::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2803::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2803::handle(CommonMessage* mb)
{
	Rsp2803 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int nResult = bean.result();														// 答题结果
	std::string strMsg = bean.msg();												// 服务器返回的消息（暂时不用）
	int nRightAnswerQuestion = bean.right();									// 已经答对的题目数目
	int nCardNum = bean.rewardnumber();										// 可翻牌次数
	int nHasAnswerQuestion = bean.answertimes();						// 已回答次数

	// QuestionData保存服务器数据
	QuestionData::instance()->set_result(nResult);
	QuestionData::instance()->set_rightAnswerQuestion(nRightAnswerQuestion);
	QuestionData::instance()->set_cardNum(nCardNum);
	QuestionData::instance()->set_hasAnswerQuestion(nHasAnswerQuestion);

	// QuestionUI显示result
	std::string str_icon_path = "res_ui/font/";
	if (0 == nResult)
	{
		str_icon_path.append("answerno.png");
	}
	else if(1 == nResult)
	{
		str_icon_path.append("answeryes.png");
	}
	MainScene* mianScene = GameView::getInstance()->getMainUIScene();
	if(mianScene)
	{
		if (mianScene->getChildByTag(200))
			mianScene->getChildByTag(200)->removeFromParent();

		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		CCSprite * sprite_result_icon = CCSprite::create(str_icon_path.c_str());
		sprite_result_icon->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_result_icon->setPosition(ccp(winSize.width/2,winSize.height*0.7f));
		sprite_result_icon->setTag(200);
		sprite_result_icon->setScale(1.0f);
		sprite_result_icon->setOpacity(0);
		mianScene->addChild(sprite_result_icon);

		CCActionInterval * m_action =(CCActionInterval *)CCSequence::create(
			CCFadeIn::create(0.3f),
			CCDelayTime::create(0.5f),
			CCFadeOut::create(0.3f),
			CCRemoveSelf::create(),
			NULL);
		sprite_result_icon->runAction(m_action);
	}

	// 通知 更新QuestionUI数据
	if (NULL != GameView::getInstance()->getMainUIScene())
	{
		QuestionUI* pQuestionUI = (QuestionUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
		if (NULL != pQuestionUI)
		{
			pQuestionUI->initQuestionInfoFromServer();
			pQuestionUI->initBaseInfoFromServer();

			if (QuestionData::instance()->get_hasAnswerQuestion() <= 20)
			{
				pQuestionUI->HandleResult(nResult,bean.trueoption());
			}
		}
	}
}