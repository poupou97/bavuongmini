
#ifndef Blog_C___Reflection_PushHandler1906_h
#define Blog_C___Reflection_PushHandler1906_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1906 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1906)
    
public:
    SYNTHESIZE(PushHandler1906, int*, m_pValue)
    
    PushHandler1906() ;
    virtual ~PushHandler1906() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
