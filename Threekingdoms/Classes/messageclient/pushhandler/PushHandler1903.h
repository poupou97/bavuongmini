
#ifndef Blog_C___Reflection_PushHandler1903_h
#define Blog_C___Reflection_PushHandler1903_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1903 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1903)
    
public:
    SYNTHESIZE(PushHandler1903, int*, m_pValue)
    
    PushHandler1903() ;
    virtual ~PushHandler1903() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
