
#ifndef Blog_C___Reflection_PushHandler5114_h
#define Blog_C___Reflection_PushHandler5114_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5114 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5114)

public:
	SYNTHESIZE(PushHandler5114, int*, m_pValue)

	PushHandler5114() ;
	virtual ~PushHandler5114() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
