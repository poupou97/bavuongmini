#include "PushHandler1708.h"
#include "../../GameView.h"
#include "../protobuf/AdditionStore.pb.h"
#include "../element/CGuildCommodity.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/family_ui/FamilyShop.h"


IMPLEMENT_CLASS(PushHandler1708)

PushHandler1708::PushHandler1708() 
{
    
}
PushHandler1708::~PushHandler1708() 
{
    
}
void* PushHandler1708::createInstance()
{
    return new PushHandler1708() ;
}
void PushHandler1708::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1708::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1708::handle(CommonMessage* mb)
{
	ResGuildStoreCommodity1708 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	FamilyUI * familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);

	if (familyui == NULL)
	{
		return;
	}
	
	std::vector<CGuildCommodity *>::iterator iter;
	for(iter =familyui->guildCommodityVector.begin(); iter!= familyui->guildCommodityVector.end();++iter )
	{
		delete *iter;
	}
	familyui->guildCommodityVector.clear();

	for (int i=0;i<bean.commoditys_size() ;i++)
	{
		CGuildCommodity * commodity_ =new CGuildCommodity();
		commodity_->CopyFrom(bean.commoditys(i));
		familyui->guildCommodityVector.push_back(commodity_);
	}

	FamilyShop * shop_ = (FamilyShop *)GameView::getInstance()->getMainUIScene()->getChildByTag(familyShoptag);
	if (shop_ != NULL)
	{
		shop_->tableviewMember->reloadData();
	}
}
