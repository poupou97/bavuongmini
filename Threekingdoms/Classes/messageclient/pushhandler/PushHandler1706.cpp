
#include "PushHandler1706.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "../element/CAdditionProperty.h"
#include "GameView.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CEquipHole.h"
#include "../protobuf/AdditionStore.pb.h"
#include "../element/CLable.h"
#include "../element/CHonorCommodity.h"
#include "../../ui/offlinearena_ui/HonorShopUI.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"



IMPLEMENT_CLASS(PushHandler1706)

PushHandler1706::PushHandler1706() 
{

}
PushHandler1706::~PushHandler1706() 
{

}
void* PushHandler1706::createInstance()
{
	return new PushHandler1706() ;
}
void PushHandler1706::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1706::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1706::handle(CommonMessage* mb)
{
	ResHonorStoreCommodity1706 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	if (scene->getMainUIScene() == NULL)
		return;

	if (scene->getMainUIScene()->getChildByTag(kTagOffLineArenaUI) == NULL)
		return;

	OffLineArenaUI * offLineArenaUI = (OffLineArenaUI*)scene->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
	//delete old data
	std::vector<CHonorCommodity*>::iterator iter;
	for (iter = offLineArenaUI->curHonorCommodityList.begin(); iter != offLineArenaUI->curHonorCommodityList.end(); ++iter)
	{
		delete *iter;
	}
	offLineArenaUI->curHonorCommodityList.clear();
	//add new data
	for (int i = 0;i<bean.commoditys_size();++i)
	{
		CHonorCommodity * honorCommodity = new CHonorCommodity();
		honorCommodity->CopyFrom(bean.commoditys(i));
		offLineArenaUI->curHonorCommodityList.push_back(honorCommodity);
	}
}
