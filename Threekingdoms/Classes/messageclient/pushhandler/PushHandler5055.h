
#ifndef Blog_C___Reflection_PushHandler5055_h
#define Blog_C___Reflection_PushHandler5055_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5055 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5055)

public:
	SYNTHESIZE(PushHandler5055, int*, m_pValue)

		PushHandler5055() ;
	virtual ~PushHandler5055() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
