
#ifndef Blog_C___Reflection_PushHandler1302_h
#define Blog_C___Reflection_PushHandler1302_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1302 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1302)

public:
	SYNTHESIZE(PushHandler1302, int*, m_pValue)

	PushHandler1302() ;
	virtual ~PushHandler1302() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
