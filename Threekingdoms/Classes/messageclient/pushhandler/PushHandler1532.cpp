
#include "PushHandler1532.h"
#include "../../GameView.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(PushHandler1532)

PushHandler1532::PushHandler1532() 
{

}
PushHandler1532::~PushHandler1532() 
{

}
void* PushHandler1532::createInstance()
{
	return new PushHandler1532() ;
}
void PushHandler1532::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1532::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1532::handle(CommonMessage* mb)
{
	Rsp1532 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, instance message", mb->cmdid());

	if (bean.status() == 1)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("FamilyFightUI_cancelSignUp_success"));
				
		FamilyUI * familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		if (familyui != NULL)
		{
			char curContributonStr[20];
			sprintf(curContributonStr,"%ld",bean.donate());
			char nectContributonStr[20];
			sprintf(nectContributonStr,"%ld",familyui->nextFamilyContrbution);

			std::string contrbutionStr_ = "";
			contrbutionStr_.append(curContributonStr);
			contrbutionStr_.append("/");
			contrbutionStr_.append(nectContributonStr);

			familyui->label_familyContribution->setText(contrbutionStr_.c_str());
		}
	}
}
