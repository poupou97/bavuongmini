
#ifndef Blog_C___Family_PushHandler1511_h
#define Blog_C___Family_PushHandler1511_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1511 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1511)
    
public:
    SYNTHESIZE(PushHandler1511, int*, m_pValue)
    
    PushHandler1511() ;
    virtual ~PushHandler1511() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
