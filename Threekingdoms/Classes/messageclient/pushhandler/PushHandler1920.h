
#ifndef Blog_C___Reflection_PushHandler1920_h
#define Blog_C___Reflection_PushHandler1920_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
USING_NS_CC;

/**
 * 进入5人副本通知
 */
class PushHandler1920 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1920)
    
public:
    SYNTHESIZE(PushHandler1920, int*, m_pValue)
    
    PushHandler1920() ;
    virtual ~PushHandler1920() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
