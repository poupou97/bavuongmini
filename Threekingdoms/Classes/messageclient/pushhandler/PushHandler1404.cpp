
#include "PushHandler1404.h"

#include "../protobuf/InteractMessage.pb.h"  
#include "../../GameView.h"
#include "../element/CMapTeam.h"
#include "../element/CTeamMember.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Friend_ui/TeamList.h"
#include "../../gamescene_state/sceneelement/MyHeadInfo.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/sceneelement/ShowTeamPlayer.h"

IMPLEMENT_CLASS(PushHandler1404)

PushHandler1404::PushHandler1404() 
{
    
}
PushHandler1404::~PushHandler1404() 
{
    
}
void* PushHandler1404::createInstance()
{
    return new PushHandler1404() ;
}
void PushHandler1404::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1404::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1404::handle(CommonMessage* mb)
{
	Push1404 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int state = bean.stat();/** 成员状态0=恢复正常 1=退出队伍 2=死了 3=成为队长 4=红名  5=掉线 6其他 等等待定*/
	long long playid_ = bean.playerid();/** 变更的成员*/
	bean.mapid();/** 角色所在地图 用来远离队伍判断*/
	bean.x();/** 角色位置 */
	bean.y();
	bean.hp();/** 体力值 */
	bean.mp();/** 精力值 */
	bean.maxhp();/** 最大体力值 */
	bean.maxmp();/** 最大精力值 */
	
	long long selfId_ = GameView::getInstance()->myplayer->getRoleId();
	MainScene * mainscene= (MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainscene == NULL)
	{
		return;
	}
	
	MissionAndTeam * mainSceneTeam_ =(MissionAndTeam *)mainscene->getChildByTag(kTagMissionAndTeam);
	FriendUi * friendui_=(FriendUi *)mainscene->getChildByTag(kTagFriendUi);
	switch(state)
	{
	case 0:
		{
			int m_index = 0;
			for ( int i=0;i<GameView::getInstance()->teamMemberVector.size();i++)
			{
				if (GameView::getInstance()->teamMemberVector.at(i)->roleid() == playid_)
				{
					GameView::getInstance()->teamMemberVector.at(i)->set_mapid(bean.mapid());
					GameView::getInstance()->teamMemberVector.at(i)->set_hp(bean.hp());
					GameView::getInstance()->teamMemberVector.at(i)->set_mp(bean.mp());
					GameView::getInstance()->teamMemberVector.at(i)->set_maxhp(bean.maxhp());
					GameView::getInstance()->teamMemberVector.at(i)->set_maxmp(bean.maxmp());
					GameView::getInstance()->teamMemberVector.at(i)->set_x(bean.x());
					GameView::getInstance()->teamMemberVector.at(i)->set_y(bean.y());
					GameView::getInstance()->teamMemberVector.at(i)->set_isonline(1);

					if (mainSceneTeam_->getCurPresentPanel() == mainSceneTeam_->p_team)
					{
						CCTableViewCell * cell_ = (CCTableViewCell *)mainSceneTeam_->teamTableView->cellAtIndex(m_index);
						if (cell_ != NULL)
						{
							if (mainSceneTeam_->selfTeamPlayerVector.size() >0)
							{
								mainSceneTeam_->selfTeamPlayerVector.at(m_index)->set_isonline(1);
							}
							mainSceneTeam_->teamTableView->updateCellAtIndex(m_index);
							break;
						}
					}
				}
				if (GameView::getInstance()->teamMemberVector.at(i)->roleid() != selfId_)
				{
					m_index++;
				}
			}

		}break;
	case 1:
		{
			if (GameView::getInstance()->teamMemberVector.size() >2)
			{
				if (selfId_ == playid_)
				{
					//mainscene->curMyHeadInfo->teamInLine->setVisible(false);

					std::vector<CTeamMember *>::iterator iterMember;
					for (iterMember = GameView::getInstance()->teamMemberVector.begin();iterMember!=GameView::getInstance()->teamMemberVector.end();++iterMember)
					{
						delete *iterMember;
					}
					GameView::getInstance()->teamMemberVector.clear();

					if (friendui_ !=NULL && friendui_->curType ==3)
					{
						for (int teamIndex =0;teamIndex<GameView::getInstance()->mapteamVector.size();teamIndex++)
						{
							if (GameView::getInstance()->mapteamVector.at(teamIndex)->teamid() == mainSceneTeam_->leaderId)
							{
								std::vector<CMapTeam *>::iterator iter = GameView::getInstance()->mapteamVector.begin()+teamIndex;
								CMapTeam * mapTeam_ = *iter;
								GameView::getInstance()->mapteamVector.erase(iter);
								delete mapTeam_;
							}
						}

						CCTableView * tabeview = (CCTableView *)friendui_->teamList->getChildByTag(TEAMLISTTABVIEW);
						tabeview->reloadData();
						friendui_->showTeamPlayer(0);
					}

				}else
				{
					for ( int i=0;i<GameView::getInstance()->teamMemberVector.size();i++)
					{
						if (GameView::getInstance()->teamMemberVector.at(i)->roleid() == playid_)
						{
							std::vector<CTeamMember *>::iterator iter = GameView::getInstance()->teamMemberVector.begin()+i;
							CTeamMember * mapTeam_ = *iter;
							GameView::getInstance()->teamMemberVector.erase(iter);
							delete mapTeam_;
						}
					}
				}
			}else
			{
				//mainscene->curMyHeadInfo->teamInLine->setVisible(false);
				std::vector<CTeamMember *>::iterator iterMember;
				for (iterMember = GameView::getInstance()->teamMemberVector.begin();iterMember!=GameView::getInstance()->teamMemberVector.end();++iterMember)
				{
					delete *iterMember;
				}
				GameView::getInstance()->teamMemberVector.clear();

				std::vector<CMapTeam *>::iterator iterMapTeam;
				for (iterMapTeam = GameView::getInstance()->mapteamVector.begin();iterMapTeam!=GameView::getInstance()->mapteamVector.end();++iterMapTeam)
				{
					delete *iterMapTeam;
				}
				GameView::getInstance()->mapteamVector.clear();
			}
		}break;
	case 2:
		{
			//GameView::getInstance()->showAlertDialog("skill");
		}break;
	case 3:
		{
			if (mainSceneTeam_ != NULL)
			{
				mainSceneTeam_->leaderId = playid_;
				mainSceneTeam_->refreshTeamPlayer();
			}
			
			if (friendui_ !=NULL && friendui_->curType ==3)
			{
				GameView::getInstance()->mapteamVector.at(0)->set_leaderid(playid_);
				CCTableView * tabeview = (CCTableView *)friendui_->teamList->getChildByTag(TEAMLISTTABVIEW);
				tabeview->reloadData();
				friendui_->showTeamPlayer(0);
			}
		}break;
	case 4:
		{

		}break;
	case 5:
		{
			int m_index = 0;
			for ( int i=0;i<GameView::getInstance()->teamMemberVector.size();i++)
			{
				if (GameView::getInstance()->teamMemberVector.at(i)->roleid() == playid_)
				{
					GameView::getInstance()->teamMemberVector.at(i)->set_mapid(bean.mapid());
					GameView::getInstance()->teamMemberVector.at(i)->set_hp(bean.hp());
					GameView::getInstance()->teamMemberVector.at(i)->set_mp(bean.mp());
					GameView::getInstance()->teamMemberVector.at(i)->set_maxhp(bean.maxhp());
					GameView::getInstance()->teamMemberVector.at(i)->set_maxmp(bean.maxmp());
					GameView::getInstance()->teamMemberVector.at(i)->set_x(bean.x());
					GameView::getInstance()->teamMemberVector.at(i)->set_y(bean.y());
					GameView::getInstance()->teamMemberVector.at(i)->set_isonline(0);

					if (mainSceneTeam_->getCurPresentPanel() == mainSceneTeam_->p_team)
					{
						CCTableViewCell * cell_ = (CCTableViewCell *)mainSceneTeam_->teamTableView->cellAtIndex(m_index);
						if (cell_ != NULL)
						{
							mainSceneTeam_->selfTeamPlayerVector.at(m_index)->set_isonline(0);
							mainSceneTeam_->teamTableView->updateCellAtIndex(m_index);
							break;
						}
					}
				}
				if (GameView::getInstance()->teamMemberVector.at(i)->roleid() != selfId_)
				{
					m_index++;
				}
			}
		}break;
	case 6:
		{
			for ( int i=0;i<GameView::getInstance()->teamMemberVector.size();i++)
			{
				if (GameView::getInstance()->teamMemberVector.at(i)->roleid() == playid_)
				{
					GameView::getInstance()->teamMemberVector.at(i)->set_mapid(bean.mapid());
					GameView::getInstance()->teamMemberVector.at(i)->set_hp(bean.hp());
					GameView::getInstance()->teamMemberVector.at(i)->set_mp(bean.mp());
					GameView::getInstance()->teamMemberVector.at(i)->set_maxhp(bean.maxhp());
					GameView::getInstance()->teamMemberVector.at(i)->set_maxmp(bean.maxmp());
					GameView::getInstance()->teamMemberVector.at(i)->set_x(bean.x());
					GameView::getInstance()->teamMemberVector.at(i)->set_y(bean.y());

					if (mainSceneTeam_->getCurPresentPanel() == mainSceneTeam_->p_team)
					{
						mainSceneTeam_->teamTableView->updateCellAtIndex(i);
					}
				}
			}
		}break;
	}

	if (friendui_ !=NULL && friendui_->curType ==3)
	{
		CCTableView * tabeview = (CCTableView *)friendui_->teamList->getChildByTag(TEAMLISTTABVIEW);
		tabeview->reloadData();
		friendui_->showTeamPlayer(0);
	}

	if (mainSceneTeam_ != NULL)
	{
		mainSceneTeam_->refreshTeamPlayer();
	}
}
