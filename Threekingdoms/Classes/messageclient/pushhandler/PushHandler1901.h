
#ifndef Blog_C___Reflection_PushHandler1901_h
#define Blog_C___Reflection_PushHandler1901_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
USING_NS_CC;

/**
 * 5人副本结束返回
 */
class PushHandler1901 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1901)
    
public:
    SYNTHESIZE(PushHandler1901, int*, m_pValue)
    
    PushHandler1901() ;
    virtual ~PushHandler1901() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
