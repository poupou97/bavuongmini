#include "PushHandler2804.h"
#include "../protobuf/QuestionMessage.pb.h"
#include "../../ui/Question_ui/QuestionData.h"
#include "../../GameView.h"
#include "../element/CPropReward.h"
#include "../../ui/Question_ui/QuestionEndUI.h"
#include "../../gamescene_state/MainScene.h"

#define  PAGE_NUM 10

IMPLEMENT_CLASS(PushHandler2804)

	PushHandler2804::PushHandler2804() 
{

}
PushHandler2804::~PushHandler2804() 
{

}
void* PushHandler2804::createInstance()
{
	return new PushHandler2804() ;
}
void PushHandler2804::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2804::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2804::handle(CommonMessage* mb)
{
	Push2804 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (QuestionData::instance()->get_cardNum() <= 0)
		return;

	std::vector<CPropReward *>tempList;
	// add data
	for (int i = 0;i<bean.propreward_size();i++)
	{
		CPropReward * temp = new CPropReward();
		temp->CopyFrom(bean.propreward(i));
		tempList.push_back(temp);
	}

	//update ui
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	QuestionEndUI *questionEndUI = (QuestionEndUI*)mainscene->getChildByTag(kTagQuestionEndUI);
	if(questionEndUI == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		questionEndUI = QuestionEndUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(questionEndUI,0,kTagQuestionEndUI);
		questionEndUI->ignoreAnchorPointForPosition(false);
		questionEndUI->setAnchorPoint(ccp(0.5f, 0.5f));
		questionEndUI->setPosition(ccp(winSize.width/2, winSize.height/2));
		questionEndUI->initReward(tempList);
		questionEndUI->setVisible(false);

		CCActionInterval * m_action =(CCActionInterval *)CCSequence::create(
			CCDelayTime::create(1.0f),
			CCCallFuncN::create(questionEndUI, callfuncN_selector(PushHandler2804::showQuestionEndUI)),
			NULL);

		questionEndUI->runAction(m_action);
	}

	//delete data
	std::vector<CPropReward*>::iterator iter;
	for (iter = tempList.begin(); iter != tempList.end(); ++iter)
	{
		delete *iter;
	}
	tempList.clear();

// 	CCNode* scene = GameView::getInstance()->getGameScene();
// 	if(scene)
// 	{
// 		CCActionInterval * m_action =(CCActionInterval *)CCSequence::create(
// 			CCDelayTime::create(1.0f),
// 	 		CCCallFuncN::create(scene, callfuncN_selector(PushHandler2804::showQuestionEndUI)),
// 	 		NULL);
// 	 
// 		scene->runAction(m_action);
// 	}

}

void PushHandler2804::showQuestionEndUI( CCNode* sender )
{
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	QuestionEndUI *questionEndUI = (QuestionEndUI*)mainscene->getChildByTag(kTagQuestionEndUI);
	if(questionEndUI)
		questionEndUI->setVisible(true);
}
