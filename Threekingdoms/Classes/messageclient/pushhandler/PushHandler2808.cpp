
#include "PushHandler2808.h"
#include "../protobuf/PlayerMessage.pb.h" 
#include "../../GameView.h"
#include "../protobuf/QuestionMessage.pb.h"
#include "../element/COfflineExpPuf.h"


IMPLEMENT_CLASS(PushHandler2808)

PushHandler2808::PushHandler2808() 
{

}
PushHandler2808::~PushHandler2808() 
{

}
void* PushHandler2808::createInstance()
{
	return new PushHandler2808() ;
}
void PushHandler2808::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2808::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2808::handle(CommonMessage* mb)
{
	Push2808 bean;
	bean.ParseFromString(mb->data());

	//delete old
	std::vector<COfflineExpPuf*>::iterator iter_offLineExp;
	for (iter_offLineExp = GameView::getInstance()->m_offLineExpPuf.begin(); iter_offLineExp != GameView::getInstance()->m_offLineExpPuf.end(); ++iter_offLineExp)
	{
		delete *iter_offLineExp;
	}
	GameView::getInstance()->m_offLineExpPuf.clear();

	for(int i = 0;i<bean.offlineexp_size();++i)
	{
		COfflineExpPuf * temp = new COfflineExpPuf();
		temp->CopyFrom(bean.offlineexp(i));
		GameView::getInstance()->m_offLineExpPuf.push_back(temp);
	}

	GameView::getInstance()->m_nRequiredGoldToGetExp = bean.gold();
	GameView::getInstance()->m_nRequiredGoldInGotToGetExp = bean.goldingot();

}
