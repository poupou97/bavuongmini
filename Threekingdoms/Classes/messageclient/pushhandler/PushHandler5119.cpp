#include "PushHandler5119.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include"../../gamescene_state/role/MyPlayer.h"
#include "../element/COneVipGift.h"
#include "../protobuf/ModelMessage.pb.h"
#include "../../ui/vip_ui/VipEveryDayRewardsUI.h"
#include "../../ui/Reward_ui/RewardUi.h"
#include "../../ui/GameUIConstant.h"
#include "../element/CRewardBase.h"

IMPLEMENT_CLASS(PushHandler5119)

	PushHandler5119::PushHandler5119() 
{

}
PushHandler5119::~PushHandler5119() 
{

}
void* PushHandler5119::createInstance()
{
	return new PushHandler5119() ;
}
void PushHandler5119::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5119::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5119::handle(CommonMessage* mb)
{
	Rsp5119 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (bean.status() != 0)
		return;

	for(int i = 0;i<GameView::getInstance()->m_vipEveryDayRewardsList.size();++i)
	{
		COneVipGift * temp = GameView::getInstance()->m_vipEveryDayRewardsList.at(i);
		if (temp->number() == GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel())
		{
			temp->set_status(3);
		}
	}

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	MainScene * mainscene_ =(MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainscene_ != NULL)
	{
		//delete vip
		RewardUi::removeRewardListEvent(REWARD_LIST_ID_VIPEVERYDAYREWARD);
	}

	VipEveryDayRewardsUI * vipEveryDayRewardsUI = (VipEveryDayRewardsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagVipEveryDayRewardsUI);
	if (!vipEveryDayRewardsUI)
		return;

	vipEveryDayRewardsUI->refreshTableViewWithOutChangeOffSet();
}