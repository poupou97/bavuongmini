#include "PushHandler5113.h"
#include "../protobuf/DailyGiftSign.pb.h"
#include "GameView.h"
#include "../../ui/SignDaily_ui/SignDailyUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/SignDaily_ui/SignDailyData.h"
#include "../element/COneSignEverydayGift.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Reward_ui/RewardUi.h"
#include "../../ui/GameUIConstant.h"
#include "../element/CRewardBase.h"


IMPLEMENT_CLASS(PushHandler5113)

PushHandler5113::PushHandler5113() 
{

}
PushHandler5113::~PushHandler5113() 
{
	
}
void* PushHandler5113::createInstance()
{
	return new PushHandler5113() ;
}
void PushHandler5113::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5113::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5113::handle(CommonMessage* mb)
{
	Rsp5113 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int nNumGift = bean.number();				// 奖励编号(领取的第几天的奖励)
	int nGiftStatus = bean.status();				// 领取状态

	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	SignDailyUI * pSignDailyUI = (SignDailyUI *)mainscene_->getChildByTag(kTagSignDailyUI);
	if(pSignDailyUI == NULL)
		return;
	int nVectorIntentGiftSize =  SignDailyData::instance()->m_vector_internt_sign.size();
	for (int i = 0; i < nVectorIntentGiftSize; i++)
	{
		// 奖励编号（第几天的签到奖励）
		if (nNumGift == SignDailyData::instance()->m_vector_internt_sign.at(i)->number())
		{
			/** 奖励物品状态 ：0.成功，-1.包裹已满*/
			if (0 == nGiftStatus)
			{
				// 奖品领取成功:(1)更新UI状态
				pSignDailyUI->getGiftSuc(nNumGift, 3);

				// signDaily is success delete remind list ----- add by liuzhenxing
				for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
				{
					if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_SIGNDAILY)
					{
						GameView::getInstance()->rewardvector.at(i)->setRewardState(0);

						RewardUi * reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
						if (reward_ui != NULL)
						{
							reward_ui->tablevie_reward->updateCellAtIndex(i);
							RewardUi::setRewardListParticle();
							break;
						}
					}
				}


			}
			else if (-1 == nGiftStatus)
			{
				// 奖品领取失败:(1)弹出提示信息（2）更新当前奖励所需时间，更新面板
				const char* str1 = StringDataManager::getString("label_qiandaobeibaoman");
				char* p1 = const_cast<char*>(str1);
				GameView::getInstance()->showAlertDialog(p1);
			}

		}
	}
}