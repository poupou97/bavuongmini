
#ifndef Blog_C___Reflection_PushHandler5132_h
#define Blog_C___Reflection_PushHandler5132_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5132 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5132)

public:
	SYNTHESIZE(PushHandler5132, int*, m_pValue)

	PushHandler5132() ;
	virtual ~PushHandler5132() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
