
#ifndef Blog_C___Reflection_PushHandler1605_h
#define Blog_C___Reflection_PushHandler1605_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1605 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1605)
    
public:
    SYNTHESIZE(PushHandler1605, int*, m_pValue)
    
    PushHandler1605() ;
    virtual ~PushHandler1605() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
