
#ifndef Blog_C___Reflection_PushHandler1409_h
#define Blog_C___Reflection_PushHandler1409_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1409 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1409)
    
public:
    SYNTHESIZE(PushHandler1409, int*, m_pValue)
    
    PushHandler1409() ;
    virtual ~PushHandler1409() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
