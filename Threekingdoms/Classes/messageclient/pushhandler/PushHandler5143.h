
#ifndef Blog_C___Reflection_PushHandler5143_h
#define Blog_C___Reflection_PushHandler5143_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"

USING_NS_CC;

class PushHandler5143 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5143)

public:
	SYNTHESIZE(PushHandler5143, int*, m_pValue)

	PushHandler5143() ;
	virtual ~PushHandler5143() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);


protected:
	int *m_pValue ;
} ;

#endif
