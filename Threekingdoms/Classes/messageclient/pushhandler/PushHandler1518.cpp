
#include "PushHandler1518.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CGuildDonateOption.h"
#include "../../ui/family_ui/FamilyAlms.h"

IMPLEMENT_CLASS(PushHandler1518)

PushHandler1518::PushHandler1518() 
{
    
}
PushHandler1518::~PushHandler1518() 
{
    
}
void* PushHandler1518::createInstance()
{
    return new PushHandler1518() ;
}
void PushHandler1518::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1518::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1518::handle(CommonMessage* mb)
{
	Rsp1518 bean;
	bean.ParseFromString(mb->data());

	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	FamilyUI * familyui =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	if (familyui == NULL)
	{
		return;
	}
	std::vector<CGuildDonateOption *>::iterator iterGuildDonate;
	for (iterGuildDonate=familyui->guilidDonateVector.begin();iterGuildDonate!=familyui->guilidDonateVector.end();iterGuildDonate++)
	{
		delete * iterGuildDonate;
	}
	familyui->guilidDonateVector.clear();

	int size_ =bean.guilddonateoptions_size();
	for (int i =0;i<size_;i++)
	{
		CGuildDonateOption * guildDonate = new CGuildDonateOption();
		guildDonate->CopyFrom(bean.guilddonateoptions(i));

		familyui->guilidDonateVector.push_back(guildDonate);
	}

	FamilyAlms * familyAlms_ = (FamilyAlms *)GameView::getInstance()->getMainUIScene()->getChildByTag(familyAlms);
	if (familyui->guilidDonateVector.size() > 0 && familyAlms_!= NULL)
	{
		familyAlms_->refreshInfo(true);
	}
}
