
#include "PushHandler3004.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../protobuf/OfflineFight.pb.h"
#include "../../ui/offlinearena_ui/FightNotesUI.h"
#include "../element/CHistory.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../../utils/StaticDataManager.h"

IMPLEMENT_CLASS(PushHandler3004)

	PushHandler3004::PushHandler3004() 
{

}
PushHandler3004::~PushHandler3004() 
{

}
void* PushHandler3004::createInstance()
{
	return new PushHandler3004() ;
}
void PushHandler3004::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler3004::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler3004::handle(CommonMessage* mb)
{
	ResFightHistory3004 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (GameView::getInstance()->getGameScene() == NULL)
		return;

	if (GameView::getInstance()->getMainUIScene() == NULL)
		return;

	FightNotesUI * fightNotesUI = (FightNotesUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagFightNotesUI);
	if (fightNotesUI == NULL)
		return;

	//refreshData
	std::vector<CHistory*>::iterator iter;
	for (iter = fightNotesUI->curHistoryList.begin(); iter != fightNotesUI->curHistoryList.end(); ++iter)
	{
		delete *iter;
	}
	fightNotesUI->curHistoryList.clear();

	for(int i = 0;i<bean.historys_size();++i)
	{
		CHistory * temp = new CHistory();
		temp->CopyFrom(bean.historys(i));
		fightNotesUI->curHistoryList.push_back(temp);
	}

	fightNotesUI->RefreshData();
}
