
#include "PushHandler1507.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../messageclient/element/CGuildBase.h"

IMPLEMENT_CLASS(PushHandler1507)

PushHandler1507::PushHandler1507() 
{
    
}
PushHandler1507::~PushHandler1507() 
{
    
}
void* PushHandler1507::createInstance()
{
    return new PushHandler1507() ;
}
void PushHandler1507::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1507::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1507::handle(CommonMessage* mb)
{
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	GameView::getInstance()->myplayer->getActiveRole()->mutable_playerbaseinfo()->set_factionid(0);
	GameView::getInstance()->myplayer->addFamilyName("", CGuildBase::position_none);
	GameView::getInstance()->myplayer->getActiveRole()->mutable_playerbaseinfo()->set_factionname("");
}
