
#ifndef Blog_C___Reflection_PushHandler3005_h
#define Blog_C___Reflection_PushHandler3005_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"
#include "cocos2d.h"

USING_NS_CC;

class PushHandler3005 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler3005)

public:
	SYNTHESIZE(PushHandler3005, int*, m_pValue)

		PushHandler3005() ;
	virtual ~PushHandler3005() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
