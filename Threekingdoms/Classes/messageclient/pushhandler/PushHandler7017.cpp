
#include "PushHandler7017.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../ui/rewardTask_ui/RewardTaskData.h"
#include "../element/CBoardMissionInfo.h"
#include "../../ui/rewardTask_ui/RewardTaskMainUI.h"

IMPLEMENT_CLASS(PushHandler7017)

	PushHandler7017::PushHandler7017() 
{

}
PushHandler7017::~PushHandler7017() 
{

}
void* PushHandler7017::createInstance()
{
	return new PushHandler7017() ;
}
void PushHandler7017::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler7017::display() 
{
	cout << *getm_pValue() << endl ;
}

bool sortByIndexForTask(CBoardMissionInfo *a,CBoardMissionInfo * b)
{
	return a->index()<b->index();
}

void PushHandler7017::handle(CommonMessage* mb)
{
	PushRefreshBoardMission7017 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//refresh taskList
	RewardTaskData::getInstance()->clearTaskList();
	for(int i = 0;i<bean.missions_size();i++)
	{
		CBoardMissionInfo * temp = new CBoardMissionInfo();
		temp->CopyFrom(bean.missions(i));
		RewardTaskData::getInstance()->p_taskList.push_back(temp);
	}
	//sort
	sort(RewardTaskData::getInstance()->p_taskList.begin(),RewardTaskData::getInstance()->p_taskList.end(),sortByIndexForTask);

	//refresh ui
	RewardTaskMainUI * rewardTaskMainUI = (RewardTaskMainUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskMainUI);
	if (rewardTaskMainUI)
	{
		rewardTaskMainUI->RefreshFiveMissionPresentWithAnm();
	}
}
