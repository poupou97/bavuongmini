#include "PushHandler1115.h"

#include "../protobuf/PlayerMessage.pb.h"  

#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(PushHandler1115)

PushHandler1115::PushHandler1115() 
{

}
PushHandler1115::~PushHandler1115() 
{

}
void* PushHandler1115::createInstance()
{
	return new PushHandler1115() ;
}
void PushHandler1115::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1115::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1115::handle(CommonMessage* mb)
{
	PushHonorChange1115 bean;
	bean.ParseFromString(mb->data());

	CCLOG("msg: %d, get honor", mb->cmdid());

	GameView* gv = GameView::getInstance();
	if (!gv)
		return;

	GameView::getInstance()->myplayer->player->set_honorpoint(bean.honor());
}