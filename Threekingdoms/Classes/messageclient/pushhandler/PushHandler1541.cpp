
#include "PushHandler1541.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(PushHandler1541)

	PushHandler1541::PushHandler1541() 
{

}
PushHandler1541::~PushHandler1541() 
{

}
void* PushHandler1541::createInstance()
{
	return new PushHandler1541() ;
}
void PushHandler1541::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1541::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1541::handle(CommonMessage* mb)
{
	Push1541 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (!GameView::getInstance())
		return;

	GameView::getInstance()->myplayer->getGuardIdList().clear();

	for (int i = 0;i<bean.guards_size();i++)
	{
		GameView::getInstance()->myplayer->getGuardIdList().push_back(bean.guards(i));
	}
}
