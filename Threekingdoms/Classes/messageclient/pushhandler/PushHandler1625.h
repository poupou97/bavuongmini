
#ifndef Blog_C___Reflection_PushHandler1625_h
#define Blog_C___Reflection_PushHandler1625_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1625 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1625)
    
public:
    SYNTHESIZE(PushHandler1625, int*, m_pValue)
    
    PushHandler1625() ;
    virtual ~PushHandler1625() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
