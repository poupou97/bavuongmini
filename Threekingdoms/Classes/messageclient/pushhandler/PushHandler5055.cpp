
#include "PushHandler5055.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsSkillsUI.h"
#include "../../gamescene_state/role/GameActor.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/generals_ui/generals_popup_ui/GeneralsSkillInfoUI.h"

IMPLEMENT_CLASS(PushHandler5055)

	PushHandler5055::PushHandler5055() 
{

}
PushHandler5055::~PushHandler5055() 
{

}
void* PushHandler5055::createInstance()
{
	return new PushHandler5055() ;
}
void PushHandler5055::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5055::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5055::handle(CommonMessage* mb)
{
	Resp5055 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

// 	if (bean.cause() == 0)
// 		return;

	//是否是升级了
	bool isUpgraded = false;
	int b = bean.fightskills_size();
	for (int i = 0;i<b;i++)
	{
		CFightSkill* fightSkill = new CFightSkill();
		fightSkill->CopyFrom(bean.fightskills(i));

		bool isExist = false;
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * tempSkill = it->second;
			if (tempSkill->getId() == fightSkill->id())
			{
				//general's skill update dialog
				if(tempSkill->getLevel() < fightSkill->level())
				{
					if (bean.cause() != 0)
					{
						isUpgraded = true;
						std::string str_dialog ;
						str_dialog.append(tempSkill->getCBaseSkill()->name().c_str());
						str_dialog.append(StringDataManager::getString("generals_skill_isUpdating"));
						char str_level[10];
						sprintf(str_level,"%d",fightSkill->level());
						str_dialog.append(str_level);
						str_dialog.append(StringDataManager::getString("fivePerson_ji"));
						GameView::getInstance()->showAlertDialog(str_dialog.c_str());
					}
				}

				tempSkill->initSkill(fightSkill->id(), fightSkill->id(), fightSkill->level(), GameActor::type_player);
				isExist = true;
				break;
			}
		}
		if (!isExist)
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(fightSkill->id(), fightSkill->id(), fightSkill->level(), GameActor::type_player);
			GameView::getInstance()->GeneralsGameFightSkillList.insert(make_pair(gameFightSkill->getId(),gameFightSkill));
			if (fightSkill->level() == 1)//学会了
			{
				if (bean.cause() != 0)
				{
					std::string str_dialog ;
					str_dialog.append(gameFightSkill->getCBaseSkill()->name().c_str());
					str_dialog.append(StringDataManager::getString("generals_skill_isUpdating"));
					char str_level[10];
					sprintf(str_level,"%d",fightSkill->level());
					str_dialog.append(str_level);
					str_dialog.append(StringDataManager::getString("fivePerson_ji"));
					GameView::getInstance()->showAlertDialog(str_dialog.c_str());
				}
			}
		}

		delete fightSkill;
	}

	if (scene->getMainUIScene()->getChildByTag(kTagGeneralsUI))
	{
		if (bean.has_error())
		{
			GameView::getInstance()->showAlertDialog(bean.error().msg());
		}
		else
		{
			GeneralsUI * genentalsUI = (GeneralsUI*)scene->getMainUIScene()->getChildByTag(kTagGeneralsUI);
			genentalsUI->generalsSkillsUI->RefreshData();

			GeneralsSkillInfoUI * generalsSkillInfoUI = (GeneralsSkillInfoUI*)scene->getMainUIScene()->getChildByTag(kTagGeneralsSkillInfoUI);
			if (generalsSkillInfoUI)
			{
				std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
				for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
				{
					GameFightSkill * tempSkill = it->second;
					if (tempSkill->getId() == generalsSkillInfoUI->curSkillId)
					{
						generalsSkillInfoUI->RefreshSkillInfo(tempSkill);
						break;
					}
				}
			}
		}
	}
}
