
#include "PushHandler7014.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../ui/rewardTask_ui/RewardTaskData.h"
#include "../element/CBoardMissionInfo.h"
#include "../../ui/rewardTask_ui/RewardTaskMainUI.h"

IMPLEMENT_CLASS(PushHandler7014)

PushHandler7014::PushHandler7014() 
{

}
PushHandler7014::~PushHandler7014() 
{

}
void* PushHandler7014::createInstance()
{
	return new PushHandler7014() ;
}
void PushHandler7014::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler7014::display() 
{
	cout << *getm_pValue() << endl ;
}

bool sortByIndex(CBoardMissionInfo *a,CBoardMissionInfo * b)
{
	return a->index()<b->index();
}

void PushHandler7014::handle(CommonMessage* mb)
{
	ResBoardInfo7014 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//refresh taskList
	RewardTaskData::getInstance()->clearTaskList();
	for(int i = 0;i<bean.missions_size();i++)
	{
		CBoardMissionInfo * temp = new CBoardMissionInfo();
		temp->CopyFrom(bean.missions(i));
		RewardTaskData::getInstance()->p_taskList.push_back(temp);
	}
	//sort
	sort(RewardTaskData::getInstance()->p_taskList.begin(),RewardTaskData::getInstance()->p_taskList.end(),sortByIndex);
	//refresh remainTime
	RewardTaskData::getInstance()->setRemainTaskNum(bean.remaintimes());
	//refresh allTime
	RewardTaskData::getInstance()->setAllTTaskNum(bean.totaltimes());
	//refresh remianCDTime
	RewardTaskData::getInstance()->setRemainCDTime(bean.remaincd());

	//refresh ui
	RewardTaskMainUI * rewardTaskMainUI = (RewardTaskMainUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskMainUI);
	if (rewardTaskMainUI)
	{
		rewardTaskMainUI->RefreshFiveMissionPresentWithAnm();
		rewardTaskMainUI->RefreshRemainTaskNum();
	}
}
