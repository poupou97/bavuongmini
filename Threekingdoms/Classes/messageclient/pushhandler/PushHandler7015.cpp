
#include "PushHandler7015.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../ui/rewardTask_ui/RewardTaskData.h"
#include "../element/CBoardMissionInfo.h"
#include "../../ui/rewardTask_ui/RewardTaskMainUI.h"

IMPLEMENT_CLASS(PushHandler7015)

PushHandler7015::PushHandler7015() 
{

}
PushHandler7015::~PushHandler7015() 
{

}
void* PushHandler7015::createInstance()
{
	return new PushHandler7015() ;
}
void PushHandler7015::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler7015::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler7015::handle(CommonMessage* mb)
{
	ResAcceptBoardMission7015 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//refresh remainTime
	RewardTaskData::getInstance()->setRemainTaskNum(bean.remaintimes());
	
	//refresh ui
	RewardTaskMainUI * rewardTaskMainUI = (RewardTaskMainUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskMainUI);
	if (rewardTaskMainUI)
	{
		rewardTaskMainUI->RefreshRemainTaskNum();
	}
}
