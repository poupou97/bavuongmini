#include "PushHandler1124.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"

#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../gamescene_state/role/MonsterOwnedStates.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../element/CFightSkill.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/HeadMenu.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../utils/StaticDataManager.h"
#include "../../utils/StrUtils.h"

IMPLEMENT_CLASS(PushHandler1124)

	PushHandler1124::PushHandler1124() 
{

}
PushHandler1124::~PushHandler1124() 
{

}
void* PushHandler1124::createInstance()
{
	return new PushHandler1124() ;
}
void PushHandler1124::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1124::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1124::handle(CommonMessage* mb)
{
	Push1124 bean;
	bean.ParseFromString(mb->data());

	CCLOG("msg: %d, skill point", mb->cmdid());

	CFightSkill* fightSkill = new CFightSkill();
	fightSkill->CopyFrom(bean.skill());


	if(bean.removeflag() == 1)   //降到了0级，应该移除
	{
		//通知挂机系统该技能已移除
		GameView::getInstance()->myplayer->getMyPlayerAI()->reSetSkillList(bean.skill().id());
		
		//更新剩余技能点数的数据
		int oldskillpoint = GameView::getInstance()->myplayer->player->skillpoint();
		GameView::getInstance()->myplayer->player->set_skillpoint(oldskillpoint + 1);

		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
		{
			GameFightSkill * tempSkill = it->second;
			if (strcmp(tempSkill->getId().c_str(),fightSkill->id().c_str()) == 0)
			{
				std::string str_dialog ;
				str_dialog.append(tempSkill->getCBaseSkill()->name().c_str());
				str_dialog = StrUtils::applyColor(str_dialog.c_str(),ccc3(254,254,51));
				str_dialog.append(StringDataManager::getString("generals_skill_isDegrading"));
				char s_lv[10];
				sprintf(s_lv,"%d",0);
				str_dialog.append(s_lv);
				str_dialog.append(StringDataManager::getString("fivePerson_ji"));
				GameView::getInstance()->showAlertDialog(str_dialog.c_str());

				GameView::getInstance()->GameFightSkillList.erase(it);
				delete tempSkill;
				break;
			}
		}

		if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene))
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(fightSkill->id(),fightSkill->id(),1,GameActor::type_player);
			SkillScene * skillScene = (SkillScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			skillScene->RefreshSkillPoint();
			skillScene->RefreshFucBtn(fightSkill->id());
			skillScene->RefreshOneItem(0,gameFightSkill);
			skillScene->RefreshOneItemInfo(0,gameFightSkill);
			delete gameFightSkill;
		}
	}
	else
	{
		int oldLevel = 0 ;
		//遍历一次，如果已经存在则删除
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
		{
			GameFightSkill * tempSkill = it->second;
			if (strcmp(tempSkill->getId().c_str(),fightSkill->id().c_str()) == 0)
			{
				oldLevel = tempSkill->getLevel();
				GameView::getInstance()->GameFightSkillList.erase(it);
				delete tempSkill;
				break;
			}
		}
		//添加最新的
		GameFightSkill * gameFightSkill = new GameFightSkill();
		gameFightSkill->initSkill(fightSkill->id(),fightSkill->id(),fightSkill->level(),GameActor::type_player);
		GameView::getInstance()->GameFightSkillList.insert(make_pair(gameFightSkill->getId(),gameFightSkill));
		int newLevel = fightSkill->level();

		if (newLevel>oldLevel)   //升级了
		{
			std::string str_dialog ;
			str_dialog.append(gameFightSkill->getCBaseSkill()->name().c_str());
			str_dialog = StrUtils::applyColor(str_dialog.c_str(),ccc3(254,254,51));
			str_dialog.append(StringDataManager::getString("generals_skill_isUpdating"));
			char s_lv[10];
			sprintf(s_lv,"%d",gameFightSkill->getLevel());
			str_dialog.append(s_lv);
			str_dialog.append(StringDataManager::getString("fivePerson_ji"));
			GameView::getInstance()->showAlertDialog(str_dialog.c_str());

			//更新剩余技能点数的数据
			int oldskillpoint = GameView::getInstance()->myplayer->player->skillpoint();
			GameView::getInstance()->myplayer->player->set_skillpoint(oldskillpoint - 1);

			SkillScene * skillScene = (SkillScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			if (skillScene)
			{
				skillScene->AddBonusSpecialEffect(fightSkill->id());
			}

			if (gameFightSkill->getCBaseSkill()->usemodel() == 0) //主动技能
			{
				if(newLevel == 1)//刚学会
				{
					SkillScene::autoEquipSkill(fightSkill->id());
					/*
					SkillScene * skillScene = (SkillScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
					if (skillScene)
					{
						skillScene->autoEquipSkill(fightSkill->id());
					}
					*/
				}
			}
		}
		else  //降级了
		{
			std::string str_dialog ;
			str_dialog.append(gameFightSkill->getCBaseSkill()->name().c_str());
			str_dialog = StrUtils::applyColor(str_dialog.c_str(),ccc3(254,254,51));
			str_dialog.append(StringDataManager::getString("generals_skill_isDegrading"));
			char s_lv[10];
			sprintf(s_lv,"%d",gameFightSkill->getLevel());
			str_dialog.append(s_lv);
			str_dialog.append(StringDataManager::getString("fivePerson_ji"));
			GameView::getInstance()->showAlertDialog(str_dialog.c_str());

			//更新剩余技能点数的数据
			int oldskillpoint = GameView::getInstance()->myplayer->player->skillpoint();
			GameView::getInstance()->myplayer->player->set_skillpoint(oldskillpoint + 1);
		}

		if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene))
		{
			SkillScene * skillScene = (SkillScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			skillScene->RefreshSkillPoint();
			skillScene->RefreshFucBtn(fightSkill->id());
			skillScene->RefreshOneItem(gameFightSkill->getLevel(),gameFightSkill);
			skillScene->RefreshOneItemInfo(gameFightSkill->getLevel(),gameFightSkill);
		}
	}

	delete fightSkill;

	MainScene * mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
	//mainScene->checkIsNewRemind();
	mainScene->remindOfSkill();
}