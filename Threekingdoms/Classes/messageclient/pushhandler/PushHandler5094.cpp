
#include "PushHandler5094.h"

#include "../protobuf/FightMessage.pb.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Friend_ui/FriendWorkList.h"
#include "../element/CRelationPlayer.h"
#include "../../utils/StaticDataManager.h"


IMPLEMENT_CLASS(PushHandler5094)

PushHandler5094::PushHandler5094() 
{

}
PushHandler5094::~PushHandler5094() 
{

}
void* PushHandler5094::createInstance()
{
	return new PushHandler5094() ;
}
void PushHandler5094::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5094::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5094::handle(CommonMessage* mb)
{
	Rsp5094 bean;
	bean.ParseFromString(mb->data());
	
	FriendUi * friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	int temp_ = 0;
	for (int i =0;i<GameView::getInstance()->relationSourceVector.size();i++)
	{
		if (bean.reciever() == GameView::getInstance()->relationSourceVector.at(i)->playerid())
		{
			temp_=i;
			GameView::getInstance()->relationSourceVector.at(i)->set_sendphypowerstatus(1);
		}
	}

	for (int i =0;i<GameView::getInstance()->relationFriendVector.size();i++)
	{
		if (bean.reciever() == GameView::getInstance()->relationFriendVector.at(i)->playerid())
		{
			GameView::getInstance()->relationFriendVector.at(i)->set_sendphypowerstatus(1);
		}
	}


	if (friend_ui != NULL)
	{
		friend_ui->friendWorkList->tableView->updateCellAtIndex(temp_/2);
	}
	
	std::string string_ = StringDataManager::getString("friend_playerOfSendPhyPowerIsSuccess");
	GameView::getInstance()->showAlertDialog(string_);

}