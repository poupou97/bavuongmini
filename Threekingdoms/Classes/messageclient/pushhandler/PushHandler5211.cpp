#include "PushHandler5211.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../element/CTargetPosition.h"
#include "../element/CTargetNpc.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../element/CMapInfo.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../ui/Active_ui/ActiveManager.h"
#include "../../utils/StaticDataManager.h"

IMPLEMENT_CLASS(PushHandler5211)

PushHandler5211::PushHandler5211() 
{

}
PushHandler5211::~PushHandler5211() 
{
	
}
void* PushHandler5211::createInstance()
{
	return new PushHandler5211() ;
}
void PushHandler5211::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5211::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5211::handle(CommonMessage* mb)
{
	Rsp5211 bean;
	bean.ParseFromString(mb->data());
	
	CTargetPosition* pTargetPosition = new CTargetPosition();
	pTargetPosition->CopyFrom(bean.tp());

	CTargetNpc * pTargetNpc = new CTargetNpc();
	pTargetNpc->CopyFrom(pTargetPosition->targetnpc());

	long long targetNpcId = pTargetNpc->npcid();

	std::string targetMapId = pTargetPosition->mapid();
	int xPos = pTargetPosition->x();
	int yPos = pTargetPosition->y();

	int posX = GameView::getInstance()->getGameScene()->tileToPositionX(xPos);
	int posY = GameView::getInstance()->getGameScene()->tileToPositionY(yPos);

	CCPoint targetPos = CCPointMake(posX, posY);

	// 监听自动寻路
	// 判断是否加到mainUIScene
	ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
	if (NULL == pActiveLayer)
	{
		ActiveManager* activeLayer = ActiveManager::create();
		activeLayer->set_targetMapId(targetMapId);
		activeLayer->set_targetPos(targetPos);
		activeLayer->set_targetNpcId(targetNpcId);
		activeLayer->setLayerUserful(true);
		activeLayer->setTag(kTagActiveLayer);

		GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
	}
	else 
	{
		pActiveLayer->set_targetMapId(targetMapId);
		pActiveLayer->set_targetPos(targetPos);
		pActiveLayer->set_targetNpcId(targetNpcId);
		pActiveLayer->setLayerUserful(true);
	}
	
	// 寻路逻辑
	if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	{
		//same map
		CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();
		CCPoint cp_target = targetPos;
		if (ccpDistance(cp_role,cp_target)< 64)
		{
			return;
		}
		else
		{
			if (targetPos.x == 0 && targetPos.y == 0)
			{
				return;
			}

			MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
			// NOT reachable, so find the near one

			cmd->targetPosition = targetPos;
			cmd->method = MyPlayerCommandMove::method_searchpath;
			bool bSwitchCommandImmediately = true;
			if(GameView::getInstance()->myplayer->isAttacking())
				bSwitchCommandImmediately = false;
			GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
		}
	}
	else
	{
		// 不在同一地图，直接飞到目的地
		ActiveManager::AcrossMapTransport(targetMapId, targetPos);

		// 判断是否加到mainUIScene
		ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
		if (NULL == pActiveLayer)
		{
			ActiveManager* activeLayer = ActiveManager::create();
			activeLayer->set_transportFinish(false);
			activeLayer->setTag(kTagActiveLayer);

			GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
		}
		else 
		{
			pActiveLayer->set_transportFinish(false);
		}
	}

	CC_SAFE_DELETE(pTargetPosition);
	CC_SAFE_DELETE(pTargetNpc);

	PackageScene *packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	if (packageScene)
	{
		packageScene->closeEndedEvent(NULL);
	}

	const char *strings_ = StringDataManager::getString("equipInfo_button_repairIng");
	GameView::getInstance()->showAlertDialog(strings_);
}