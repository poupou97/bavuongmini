
#ifndef Blog_C___Reflection_PushHandler1122_h
#define Blog_C___Reflection_PushHandler1122_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

using namespace std;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1122 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1122)
public:
	SYNTHESIZE(PushHandler1122, int*, m_pValue)

		PushHandler1122() ;
	virtual ~PushHandler1122() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;
} ;

#endif