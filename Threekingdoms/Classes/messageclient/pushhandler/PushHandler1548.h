
#ifndef Blog_C___Family_PushHandler1548_h
#define Blog_C___Family_PushHandler1548_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1548 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1548)

public:
	SYNTHESIZE(PushHandler1548, int*, m_pValue)

		PushHandler1548() ;
	virtual ~PushHandler1548() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
