
#include "PushHandler2209.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"

#include "../protobuf/RelationMessage.pb.h"
#include "../../ui/extensions/RichTextInput.h"
#include "GameView.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../ui/Friend_ui/FriendWorkList.h"
#include "../../messageclient/element/CRelationPlayer.h"

IMPLEMENT_CLASS(PushHandler2209)

PushHandler2209::PushHandler2209() 
{
    
}
PushHandler2209::~PushHandler2209() 
{
    
}
void* PushHandler2209::createInstance()
{
    return new PushHandler2209() ;
}
void PushHandler2209::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2209::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler2209::handle(CommonMessage* mb)
{
	Rsp2209 bean;
	bean.ParseFromString(mb->data());
	
	if (bean.pageindex() == 0)
	{
		std::vector<CRelationPlayer *>::iterator iter;
		for(iter=GameView::getInstance()->relationSourceVector.begin();iter!=GameView::getInstance()->relationSourceVector.end();iter++)
		{
			delete *iter;
		}
		GameView::getInstance()->relationSourceVector.clear();
	}
	
	int playListSize = bean.playerlist_size();	
	for (int i=0;i<playListSize;i++)
	{
		CRelationPlayer * relationPlayerinfo = new CRelationPlayer();
		relationPlayerinfo->CopyFrom(bean.playerlist(i));

		GameView::getInstance()->relationSourceVector.push_back(relationPlayerinfo);
	}
	
	FriendUi * friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	if (friend_ui== NULL)
	{
		return;
	}
	
	FriendWorkList * friendworklist_ =(FriendWorkList *)friend_ui->getChildByTag(FRIENDADDLAYER)->getChildByTag(FRIENDWORKLISTTAG);
	CCTableView * friendTab =(CCTableView *)friendworklist_->getChildByTag(FRIENDWORKLISTTAB);
	friendworklist_->totalPageNum = bean.totalpages();
	friend_ui->pageIndexSelect = bean.pageindex();

	int onLineFriendNum = GameView::getInstance()->relationSourceVector.size();
	char friendnum_[10];
	sprintf(friendnum_,"%d",onLineFriendNum);
	friend_ui->label_Friendnum->setText(friendnum_);
	friend_ui->labclCheckResult->setText(friendnum_);
	friendTab->reloadData();

	friend_ui->minLvSelect = 0;
	friend_ui->maxLvSelect = 0;
}
