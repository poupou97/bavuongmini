
#ifndef Blog_C___Reflection_PushHandler3002_h
#define Blog_C___Reflection_PushHandler3002_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"
#include "cocos2d.h"

USING_NS_CC;

class PushHandler3002 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler3002)

public:
	SYNTHESIZE(PushHandler3002, int*, m_pValue)

		PushHandler3002() ;
	virtual ~PushHandler3002() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
