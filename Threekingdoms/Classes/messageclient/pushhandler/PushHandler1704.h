
#ifndef Blog_C___Reflection_PushHandler1704_h
#define Blog_C___Reflection_PushHandler1704_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1704 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1704)

public:
	SYNTHESIZE(PushHandler1704, int*, m_pValue)

		PushHandler1704() ;
	virtual ~PushHandler1704() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
