
#ifndef Blog_C___Reflection_PushHandler1001_h
#define Blog_C___Reflection_PushHandler1001_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1001 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1001)
    
public:
    SYNTHESIZE(PushHandler1001, int*, m_pValue)
    
    PushHandler1001() ;
    virtual ~PushHandler1001() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
