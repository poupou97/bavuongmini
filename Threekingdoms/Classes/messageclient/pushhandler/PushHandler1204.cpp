
#include "PushHandler1204.h"

#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/ModelMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../GameView.h"
#include "../ProtocolHelper.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/SimpleEffectManager.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../element/CActiveRole.h"
#include "../element/CGeneralDetail.h"
#include "../../utils/GameUtils.h"
#include "GameAudio.h"
#include "../../gamescene_state/sceneelement/GeneralsInfoUI.h"

IMPLEMENT_CLASS(PushHandler1204)

PushHandler1204::PushHandler1204() 
{
    
}
PushHandler1204::~PushHandler1204() 
{
    
}
void* PushHandler1204::createInstance()
{
    return new PushHandler1204() ;
}
void PushHandler1204::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1204::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1204::handle(CommonMessage* mb)
{
	Push1204 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, fight info refresh", mb->cmdid());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	//实时更新上阵武将详细信息
	for (int i = 0;i < GameView::getInstance()->generalsInLineDetailList.size();++i)
	{
		if (bean.roleid() == GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
		{
			ActiveRole * activeRole = dynamic_cast<ActiveRole*>(GameView::getInstance()->generalsInLineDetailList.at(i)->mutable_activerole());
			if (activeRole)
			{
				activeRole->set_hp(bean.hp());
				activeRole->set_mp(bean.mp());
				activeRole->set_maxhp(bean.maxhp());
				activeRole->set_maxmp(bean.maxmp());
			}

			if (bean.has_aptitude())
			{
				GameView::getInstance()->generalsInLineDetailList.at(i)->mutable_aptitude()->CopyFrom(bean.aptitude());
			}	

			//add by yangjun 2014.10.09
			MainScene * mainScene = GameView::getInstance()->getMainUIScene();
			if(mainScene != NULL)
			{
				GeneralsInfoUI * generalsUI = (GeneralsInfoUI*)mainScene->getGeneralInfoUI();
				if (generalsUI)
				{
					generalsUI->RefreshGeneralHP(GameView::getInstance()->generalsInLineDetailList.at(i));
				}
			}
		}
	}

	BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(bean.roleid()));
	if(target == NULL)
		return;

	if (target->getActiveRole()->hp() <= 0 && bean.hp() > 0) {
		// 其他角色的复活（玩家自己的复活由消息1209来完成）
		if (!GameView::getInstance()->isOwn(bean.roleid())) {
			// 复活
			if(target->isAction(ACT_DIE))
			{
				target->changeAction(ACT_STAND);
				GameUtils::playGameSound(REVIVAL_TRIGGER, 2, false);
			}
			CCLOG("msg: %d, relive: %lld", mb->cmdid(), bean.roleid());
		}
	}

	if(target->isAction(ACT_DIE))
		return;

	//
	// 1. pre-logic
	//
	// 下面这些状态的判断，必须在ActiveRole->setHp(), setMp等之前
	BaseFighter* attacker = NULL;
	if(bean.has_attackerid())
		attacker = dynamic_cast<BaseFighter*>(scene->getActor(bean.attackerid()));
	// 回血
	int hpNumber = bean.hp() - target->getActiveRole()->hp();
	if (hpNumber > 0 && target->getActiveRole()->maxhp() == bean.maxhp())
		target->onHealed(attacker, hpNumber);
	// 受伤害掉血
	if (hpNumber < 0 && target->getActiveRole()->maxhp() == bean.maxhp())
		target->onDamaged(attacker, -hpNumber, false);
	// 回蓝
	int mpNumber = bean.mp() - target->getActiveRole()->mp();
	if (mpNumber > 0 && target->getActiveRole()->maxmp() == bean.maxmp())
		target->onMPAdded(attacker, mpNumber);

	//
	// 2. update data & UI
	//
	// 更新hp, mp等信息
	ActiveRole * activeRole = dynamic_cast<ActiveRole*>(target->getActiveRole());
	if (activeRole)
	{
		activeRole->set_maxhp(bean.maxhp());
		activeRole->set_maxmp(bean.maxmp());
	}
	ProtocolHelper::sharedProtocolHelper()->updateFighterHPMP(target, bean.hp(), bean.mp());

	// 更新其他属性
	if (GameView::getInstance()->isOwn(bean.roleid())) 
	{
		if (bean.has_aptitude())
		{
			GameView::getInstance()->myplayer->player->mutable_aptitude()->CopyFrom(bean.aptitude());
		}
	}

	// 更新异常状态信息
	ProtocolHelper::sharedProtocolHelper()->updateFighterStatus(bean.mutable_extstatus(), target);

	
}
