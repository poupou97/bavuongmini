
#ifndef Blog_C___Family_PushHandler1523_h
#define Blog_C___Family_PushHandler1523_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1523 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1523)
    
public:
    SYNTHESIZE(PushHandler1523, int*, m_pValue)
    
    PushHandler1523() ;
    virtual ~PushHandler1523() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
