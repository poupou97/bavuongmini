
#include "PushHandler1625.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "../element/CAdditionProperty.h"
#include "GameView.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CEquipHole.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../ui/GameUIConstant.h"
#include "gamescene_state/EffectDispatch.h"



IMPLEMENT_CLASS(PushHandler1625)

PushHandler1625::PushHandler1625() 
{
    
}
PushHandler1625::~PushHandler1625() 
{
    
}
void* PushHandler1625::createInstance()
{
	return new PushHandler1625() ;
}
void PushHandler1625::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1625::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1625::handle(CommonMessage* mb)
{
	Push1625 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	EquipMentUi * equip= (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	
	if (equip->tabviewSelectIndex != 0)
	{
		equip->generalList->generalList_tableView->selectCell(equip->tabviewSelectIndex);
	}

	int size_ =equip->equipmentHoleV.size();
	int isPickGem = false;
	
	for (int index=0;index<size_;index++)
	{
		if (bean.equiphole().id() == equip->equipmentHoleV.at(index)->id())
		{
			int beginEquipHoleState = equip->equipmentHoleV.at(index)->state();
			CEquipHole * equiphole = equip->equipmentHoleV.at(index);
			equiphole->CopyFrom(bean.equiphole());
			int curEquipHoleState = equiphole->state();
			if (beginEquipHoleState == 3 && curEquipHoleState == 2)
			{
				isPickGem = true;
			}
		}
	}

	equip->refreshGemValue(false);
	equip->refreshPlayerMoney();

	if (!isPickGem)
	{
		CCNodeRGBA* pEffect = BonusSpecialEffect::create();
		pEffect->setPosition(ccp(EQUIPMENT_UI_CENTER_SLOT_X, EQUIPMENT_UI_CENTER_SLOT_Y));
		pEffect->setScale(EQUIPMENT_UI_BONUSSPECIALEFFECT_SCALE);
		equip->gemLayer->addChild(pEffect);

		EffectEquipItem::reloadShortItem();
	}
}
