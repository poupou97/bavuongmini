
#include "PushHandler1206.h"

#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/GeneralMessage.pb.h"

IMPLEMENT_CLASS(PushHandler1206)

PushHandler1206::PushHandler1206() 
{

}
PushHandler1206::~PushHandler1206() 
{

}
void* PushHandler1206::createInstance()
{
	return new PushHandler1206() ;
}
void PushHandler1206::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1206::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1206::handle(CommonMessage* mb)
{
	Rsp1206 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	CCUserDefault::sharedUserDefault()->setIntegerForKey("general_mode",bean.petmode());
	CCUserDefault::sharedUserDefault()->setIntegerForKey("general_autoFight",bean.autofight());

	if (!GameView::getInstance()->getGameScene())
		return;

	MainScene * mainscene=(MainScene *) GameView::getInstance()->getMainUIScene();
	if (!mainscene)
		return;

	mainscene->guideMap->applyGeneralMode(bean.petmode(),bean.autofight());
}