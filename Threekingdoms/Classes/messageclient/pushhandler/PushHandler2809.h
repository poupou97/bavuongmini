
#ifndef Blog_C___CheckOtherEquip_PushHandler2809_h
#define Blog_C___CheckOtherEquip_PushHandler2809_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler2809 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler2809)

public:
	SYNTHESIZE(PushHandler2809, int*, m_pValue)

		PushHandler2809() ;
	virtual ~PushHandler2809() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
