
#include "PushHandler1534.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CGuildFightRanking.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightIntegrationRankingUI.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightUI.h"



IMPLEMENT_CLASS(PushHandler1534)

	PushHandler1534::PushHandler1534() 
{

}
PushHandler1534::~PushHandler1534() 
{

}
void* PushHandler1534::createInstance()
{
	return new PushHandler1534() ;
}
void PushHandler1534::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1534::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1534::handle(CommonMessage* mb)
{
	Rsp1534 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, instance message", mb->cmdid());

	FamilyFightData::getInstance()->setFamilyFightRanking(bean.myranking());

	FamilyFightIntegrationRankingUI * rankingUI = (FamilyFightIntegrationRankingUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightIntegrationRankingUI);
	if (!rankingUI)
		return;

	FamilyUI * familyUI = (FamilyUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	if (familyUI)
	{
		FamilyFightUI * familyFightUI = (FamilyFightUI*)familyUI->GetFamilyFightUI();
		if (familyFightUI)
		{
			familyFightUI->RefreshFamilyRankingAndScore();
		}
	}

	//delete old ranking
	if (rankingUI->isReqNewly)
	{
		std::vector<CGuildFightRanking*>::iterator iter_ranking;
		for (iter_ranking = rankingUI->familyFightRankingList.begin(); iter_ranking != rankingUI->familyFightRankingList.end(); ++iter_ranking)
		{
			delete *iter_ranking;
		}
		rankingUI->familyFightRankingList.clear();
	}
	
	//add new
	for (int i = 0;i<bean.rankings_size();++i)
	{
		CGuildFightRanking * temp = new CGuildFightRanking();
		temp->CopyFrom(bean.rankings(i));
		rankingUI->familyFightRankingList.push_back(temp);
	}

	rankingUI->RefreshGeneralsListStatus(bean.pageno(),bean.totalpages());
	rankingUI->refreshUI();
	rankingUI->setIsCanReq(true);
}
