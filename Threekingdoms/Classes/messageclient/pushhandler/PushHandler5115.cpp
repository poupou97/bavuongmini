#include "PushHandler5115.h"
#include "../protobuf/ActiveMessage.pb.h"
#include "../protobuf/AdditionStore.pb.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "../../ui/Active_ui/ActiveData.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../../ui/Active_ui/ActiveShopUI.h"
#include "../../gamescene_state/MainScene.h"


IMPLEMENT_CLASS(PushHandler5115)

PushHandler5115::PushHandler5115() 
{

}
PushHandler5115::~PushHandler5115() 
{
	
}
void* PushHandler5115::createInstance()
{
	return new PushHandler5115() ;
}
void PushHandler5115::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5115::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5115::handle(CommonMessage* mb)
{
	Rsp5115 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//0.成功，,-1包满了，1金钱不足
	int nStatus = bean.type();
	/*if (1 == nStatus)
	{
		const char * str_tmp1 = StringDataManager::getString("activesho_activebuzu");
		char * str_tmpStatus1 = const_cast<char *>(str_tmp1);

		GameView::getInstance()->showAlertDialog(str_tmpStatus1);
	}
	else if (-1 == nStatus)
	{
		const char * str_tmp2 = StringDataManager::getString("activesho_beibaoman");
		char * str_tmpStatus2 = const_cast<char *>(str_tmp2);

		GameView::getInstance()->showAlertDialog(str_tmpStatus2);
	}*/


	// 玩家剩余活跃度
	int nActive = bean.number();
	// 更新到服务器数据
	ActiveData::instance()->setUserActive(nActive);

	ActiveUI* pTmpActiveUI = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	if (NULL != pTmpActiveUI)
	{
		pTmpActiveUI->initUserActiveFromInternet();
		pTmpActiveUI->initTodayActiveFromInternet();
	}

	ActiveShopUI* pTmpActiveShopUI = (ActiveShopUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveShop);
	if (NULL != pTmpActiveShopUI)
	{
		pTmpActiveShopUI->initUserActiveFromInternet();
	}

}