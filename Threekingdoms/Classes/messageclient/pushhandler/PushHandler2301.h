#ifndef Blog_C___Reflection_PushHandler2301_h
#define Blog_C___Reflection_PushHandler2301_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler2301 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler2301)

public:
	SYNTHESIZE(PushHandler2301, int*, m_pValue)

	PushHandler2301() ;
	virtual ~PushHandler2301() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
