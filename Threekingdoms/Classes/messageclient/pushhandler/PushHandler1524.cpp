
#include "PushHandler1524.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/family_ui/ManageFamily.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/CGuildMemberBase.h"
#include "../element/CGuildRecord.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/MainScene.h"



IMPLEMENT_CLASS(PushHandler1524)

PushHandler1524::PushHandler1524() 
{
    
}
PushHandler1524::~PushHandler1524() 
{
    
}
void* PushHandler1524::createInstance()
{
    return new PushHandler1524() ;
}
void PushHandler1524::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1524::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1524::handle(CommonMessage* mb)
{
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameView::getInstance()->applyPlayersize++;
	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	mainscene_->remindFamilyApply();
}
