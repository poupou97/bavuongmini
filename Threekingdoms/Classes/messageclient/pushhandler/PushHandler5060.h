
#ifndef Blog_C___Reflection_PushHandler5060_h
#define Blog_C___Reflection_PushHandler5060_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5060 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5060)

public:
	SYNTHESIZE(PushHandler5060, int*, m_pValue)

	PushHandler5060() ;
	virtual ~PushHandler5060() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
