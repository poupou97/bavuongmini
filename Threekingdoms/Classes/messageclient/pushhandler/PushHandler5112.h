
#ifndef Blog_C___Reflection_PushHandler5112_h
#define Blog_C___Reflection_PushHandler5112_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5112 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5112)

public:
	SYNTHESIZE(PushHandler5112, int*, m_pValue)

	PushHandler5112() ;
	virtual ~PushHandler5112() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
