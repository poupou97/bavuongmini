
#include "PushHandler1301.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/ModelMessage.pb.h"
#include "../protobuf/ItemMessage.pb.h"

#include "../../GameView.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/FolderInfo.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(PushHandler1301)

	PushHandler1301::PushHandler1301() 
{

}
PushHandler1301::~PushHandler1301() 
{

}
void* PushHandler1301::createInstance()
{
	return new PushHandler1301() ;
}
void PushHandler1301::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1301::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1301::handle(CommonMessage* mb)
{
	Rsp1301 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameView::getInstance()->AllPacItem.erase(GameView::getInstance()->AllPacItem.begin(),GameView::getInstance()->AllPacItem.end());
	
	std::vector<FolderInfo*>::iterator iter;
	for (iter = GameView::getInstance()->AllPacItem.begin(); iter != GameView::getInstance()->AllPacItem.end(); ++iter)
	{
		delete *iter;
	}
	GameView::getInstance()->AllPacItem.clear();

	GameView* gameView = GameView::getInstance();
	int a = bean.folders_size();
	for (int i = 0;i<bean.folders_size();i++)
	{
		FolderInfo * _folder = new FolderInfo();
		_folder->CopyFrom(bean.folders(i));
		//_folder = (FolderInfo*)bean.mutable_folders(i);
		gameView->AllPacItem.push_back(_folder);
	}

	gameView->pacPageView->ReloadData();

	// it's not game state, ignore
	CCScene* pScene = CCDirector::sharedDirector()->getRunningScene();
	if(pScene->getTag() != GameView::STATE_GAME)
	{
		return;
	}
	else
	{
		GameSceneState* pState = (GameSceneState*)pScene;
		if(pState->getState() == GameSceneState::state_load)
		{
			return;
		}
	}
	//add by yangjun 2013.11.8
	if (gameView->getMainUIScene()->getChildByTag(kTagShortcutLayer))
	{
		ShortcutLayer * shortcutLayer = (ShortcutLayer *)gameView->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		shortcutLayer->RefreshAll();
	}
}