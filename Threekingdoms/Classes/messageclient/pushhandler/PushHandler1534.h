
#ifndef Blog_C___Family_PushHandler1534_h
#define Blog_C___Family_PushHandler1534_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1534 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1534)

public:
	SYNTHESIZE(PushHandler1534, int*, m_pValue)

		PushHandler1534() ;
	virtual ~PushHandler1534() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
