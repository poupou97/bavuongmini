#include "PushHandler1010.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/GameSceneState.h"


IMPLEMENT_CLASS(PushHandler1010)

PushHandler1010::PushHandler1010() 
{

}
PushHandler1010::~PushHandler1010() 
{
	
}
void* PushHandler1010::createInstance()
{
	return new PushHandler1010() ;
}
void PushHandler1010::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1010::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1010::handle(CommonMessage* mb)
{
	Push1010 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// ��ǰ ս��
	int nPkVale = bean.pkvalue();
	GamePlayer * pPlayer = GameView::getInstance()->myplayer->player;
	pPlayer->set_pkpoint(nPkVale);

	this->addBattleAchMessageToScene(1);
}

void PushHandler1010::addBattleAchMessageToScene( int nPkPoint )
{
	// add message to the game scene
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	MyPlayer* me = GameView::getInstance()->myplayer;

	std::string _str = "+";
	const char * goldStr  = StringDataManager::getString("BattleAchievement_name");
	_str.append(goldStr);
	char diffStr[20];
	sprintf(diffStr,"%d", nPkPoint);
	_str.append(diffStr);

	CCLabelBMFont* pLabel = CCLabelBMFont::create(_str.c_str(), "res_ui/font/ziti_3.fnt");
	pLabel->setAnchorPoint(ccp(0.5f,0.0f));
	pLabel->setPosition(ccp(me->getPositionX() + 80, me->getPositionY()));
	CCAction*  action = CCSequence::create(
		CCMoveBy::create(0.8f, ccp(0, 40)),
		CCRemoveSelf::create(),
		NULL);
	pLabel->runAction(action);
	scene->getActorLayer()->addChild(pLabel, SCENE_TOP_LAYER_BASE_ZORDER);
}
