
#include "PushHandler5111.h"


#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../protobuf/DailyGift.pb.h"
#include "../../ui/OnlineReward_ui/OnLineRewardUI.h"
#include "../../messageclient/element/COneOnlineGift.h"
#include "../GameMessageProcessor.h"
#include "../../ui/OnlineReward_ui/OnlineGiftData.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/GameUIConstant.h"
#include "../../ui/Reward_ui/RewardUi.h"


IMPLEMENT_CLASS(PushHandler5111)

PushHandler5111::PushHandler5111() 
{

}
PushHandler5111::~PushHandler5111() 
{
	
}
void* PushHandler5111::createInstance()
{
	return new PushHandler5111() ;
}
void PushHandler5111::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5111::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5111::handle(CommonMessage* mb)
{
	Rsp5111 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int nNumGift = bean.number();				// 奖励编号
	int nGiftStatus = bean.status();				// 领取状态

	OnLineRewardUI * pOnLineRewardUI = (OnLineRewardUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftUI);
	if(pOnLineRewardUI == NULL)
		return;
	int nVectorOneOnlineGiftSize = OnlineGiftData::instance()->m_vector_oneOnLineGift.size();
	for (int i = 0; i < nVectorOneOnlineGiftSize; i++)
	{
		// 奖励编号
		if (nNumGift == OnlineGiftData::instance()->m_vector_oneOnLineGift.at(i)->number())
		{
			 /** 奖励物品状态 ：0.成功，其他.还需要多长时间才能领取奖品  -1.包裹已满*/
			if (0 == nGiftStatus)
			{
				// 奖品领取成功:(1)开始下一个奖励的倒计时
				pOnLineRewardUI->giftGetSuc(nNumGift, 3);

				RewardUi::removeRewardListEvent(REWARD_LIST_ID_ONLINEREWARD);
			}
			else if(-1 == nGiftStatus)
			{
				// 奖品领取失败:(1)弹出提示信息（2）更新当前奖励所需时间，更新面板
				const char* str1 = StringDataManager::getString("label_bagisfull");
				char* p1 = const_cast<char*>(str1);
				GameView::getInstance()->showAlertDialog(p1);
			}
		}
		
	}

}