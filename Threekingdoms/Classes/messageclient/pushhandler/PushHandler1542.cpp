
#include "PushHandler1542.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightData.h"

IMPLEMENT_CLASS(PushHandler1542)

	PushHandler1542::PushHandler1542() 
{

}
PushHandler1542::~PushHandler1542() 
{

}
void* PushHandler1542::createInstance()
{
	return new PushHandler1542() ;
}
void PushHandler1542::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1542::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1542::handle(CommonMessage* mb)
{
	Push1542 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), "family battle, the boss's hp changed");

	FamilyFightData::setBossOwnerHpInfo(bean.ownerbosshp(),bean.ownerbosshpcap());
	FamilyFightData::setBossCommonHpInfo(bean.commonbosshp(),bean.commonbosshpcap());
	FamilyFightData::setBossEnemyHpInfo(bean.enemybosshp(),bean.enemybosshpcap());

	if (bean.has_lefttime())
	{
		FamilyFightData::setRemainTime(bean.lefttime());
	}

	if (!GameView::getInstance()->getGameScene())
		return;

	MainScene* mainScene = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	if (!mainScene)
		return;

	MissionAndTeam * missionAndTeam = (MissionAndTeam*)mainScene->getChildByTag(kTagMissionAndTeam);
	if (missionAndTeam)
	{
		if (missionAndTeam->getPanelType() == MissionAndTeam::type_FamilyFightAndTeam)
		{
			missionAndTeam->updateFamilyFightSchedult();
		}
	}
}
