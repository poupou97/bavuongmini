
#ifndef Blog_C___Family_PushHandler1508_h
#define Blog_C___Family_PushHandler1508_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1508 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1508)
    
public:
    SYNTHESIZE(PushHandler1508, int*, m_pValue)
    
    PushHandler1508() ;
    virtual ~PushHandler1508() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
