
#include "PushHandler1610.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "../element/CAdditionProperty.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/equipMent_ui/StrengthEquip.h"
#include "../../ui/equipMent_ui/RefreshEquipData.h"


IMPLEMENT_CLASS(PushHandler1610)

PushHandler1610::PushHandler1610() 
{
    
}
PushHandler1610::~PushHandler1610() 
{
    
}
void* PushHandler1610::createInstance()
{
	return new PushHandler1610() ;
}
void PushHandler1610::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1610::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1610::handle(CommonMessage* mb)
{
	Rsp1610 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	EquipMentUi * equip= (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	std::vector<CAdditionProperty *>::iterator iter;
	for (iter = equip->additionProperty_baptize.begin(); iter != equip->additionProperty_baptize.end(); ++iter)
	{
		delete *iter;
	}
	equip->additionProperty_baptize.clear();
	for (int i=0;i<bean.refineproperties_size();i++)
	{
		CAdditionProperty * addproperty_ =new CAdditionProperty();
		addproperty_->CopyFrom(bean.refineproperties(i));
		equip->additionProperty_baptize.push_back(addproperty_);
	}
	int baptizeCost_ = bean.cost();
	equip->baptizeMainEquip =true;
	equip->refreshCurState();
	RefreshEquipData::instance()->getOperationGeneral();
	equip->refreshEquipProperty_baptize(baptizeCost_);
	equip->refreshBackPack(equip->currType,-1);
}
