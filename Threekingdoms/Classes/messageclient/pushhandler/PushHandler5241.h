
#ifndef Blog_C___Reflection_PushHandler5241_h
#define Blog_C___Reflection_PushHandler5241_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

using namespace std;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5241 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5241)
public:
	SYNTHESIZE(PushHandler5241, int*, m_pValue)

	PushHandler5241() ;
	virtual ~PushHandler5241() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;
} ;

#endif