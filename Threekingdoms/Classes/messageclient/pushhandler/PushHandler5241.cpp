#include "PushHandler5241.h"

#include "../protobuf/PlayerMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/LootActor.h"

IMPLEMENT_CLASS(PushHandler5241)

PushHandler5241::PushHandler5241() 
{

}
PushHandler5241::~PushHandler5241() 
{

}
void* PushHandler5241::createInstance()
{
	return new PushHandler5241() ;
}
void PushHandler5241::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5241::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5241::handle(CommonMessage* mb)
{
	Push5241 bean;
	bean.ParseFromString(mb->data());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	#define LOOT_ACTOR_GOLD_ID "jinbi1"
	std::vector<DropProp> allLootsInfo;
	// prepare all loots info, special for the boss
	float drop_range = 128;
	if(bean.has_roleid())   // boss
	{
		drop_range = 600;
		for(int i = 0; i < bean.props_size(); i++)
		{
			// special for the gold
			if(bean.props(i).propid() == LOOT_ACTOR_GOLD_ID)
			{
				const int totalNum = 30;
				int eachAmount = bean.props(i).amount() / totalNum;
				if(eachAmount < 2)
					eachAmount = 2;
				for(int j = 0; j < totalNum; j++)
				{
					DropProp eachDrop;
					eachDrop.set_amount(eachAmount);
					eachDrop.set_propid(bean.props(i).propid());
					allLootsInfo.push_back(eachDrop);
				}
			}
			else
			{
				allLootsInfo.push_back(bean.props(i));
			}
		}
	}
	else
	{
		for(int i = 0; i < bean.props_size(); i++)
			allLootsInfo.push_back(bean.props(i));
	}

	// generate the loot actor
	for(unsigned int i = 0; i < allLootsInfo.size(); i++)
	{
		LootActor* pLoot = new LootActor();
		// first, set world position, then load figure by init()

		float randomX = (CCRANDOM_0_1() - 0.5f) * drop_range;
		float randomY = (CCRANDOM_0_1() - 0.5f) * drop_range;
		CCPoint worldPos = ccp(bean.positionx()+randomX, bean.positiony()+randomY);
		pLoot->setGameScene(scene);
		pLoot->setWorldPosition(worldPos);
		pLoot->init(allLootsInfo.at(i).propid().c_str(), allLootsInfo.at(i).amount());
		//pLoot->setLayerId(0);
		//scene->getActorLayer()->addChild(pLoot, SCENE_GROUND_LAYER_BASE_ZORDER);
		scene->getActorLayer()->addChild(pLoot, SCENE_ROLE_LAYER_BASE_ZORDER);
		pLoot->release();
	}
}
