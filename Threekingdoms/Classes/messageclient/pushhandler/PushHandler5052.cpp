
#include "PushHandler5052.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../ui/generals_ui/GeneralsEvolutionUI.h"
#include "../../ui/generals_ui/GeneralsTeachUI.h"
#include "../element/CGeneralDetail.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../element/CEquipment.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../element/CActiveRole.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"
#include "../element/GoodsInfo.h"
#include "../element/CGeneralBaseMsg.h"
#include "../ProtocolHelper.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../ui/equipMent_ui/RefreshEquipData.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/sceneelement/GeneralsInfoUI.h"
#include "../../gamescene_state/MainAnimationScene.h"
#include "../../gamescene_state/EffectDispatch.h"

IMPLEMENT_CLASS(PushHandler5052)

PushHandler5052::PushHandler5052() 
{

}
PushHandler5052::~PushHandler5052() 
{

}
void* PushHandler5052::createInstance()
{
	return new PushHandler5052() ;
}
void PushHandler5052::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5052::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5052::handle(CommonMessage* mb)
{
	Resp5052 bean;	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (bean.has_error())
	{
		GameView::getInstance()->showAlertDialog(bean.error().msg());
	}
	else
	{
		//get change of before general fightPoint 
		int m_old_AllFightPoint = GameView::getInstance()->myplayer->player->fightpoint();
		for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();++i)
		{
			m_old_AllFightPoint += GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint();
		}

		for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();++i)
		{
			bool isRetain = false;   // «∑Òªπ‘⁄’Û÷–
			for (int j = 0;j<GameView::getInstance()->generalsInLineList.size();++j)
			{
				if (GameView::getInstance()->generalsInLineDetailList.at(i)->generalid() == GameView::getInstance()->generalsInLineList.at(j)->id())
				{
					isRetain = true;
					break;
				}
			}

			if (!isRetain)
			{
				CGeneralDetail * temp = GameView::getInstance()->generalsInLineDetailList.at(i);
				std::vector<CGeneralDetail*>::iterator iter = GameView::getInstance()->generalsInLineDetailList.begin()+i;
				GameView::getInstance()->generalsInLineDetailList.erase(iter);
				delete temp;
			}
		}
		// «∑Ò «…œ’ÛŒ‰Ω´
		bool isInLine = false;
		for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();i++)
		{
			if (GameView::getInstance()->generalsInLineList.at(i)->id() == bean.general().generalid())
			{
				isInLine = true;
				break;
			}
		}
		
		if (isInLine)
		{
			// «∑Ò“—æ≠”–¡ÀœÍœ∏–≈œ¢
			bool isExist = false;
			for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
			{
				CGeneralDetail * temp = GameView::getInstance()->generalsInLineDetailList.at(i);
				if (temp->generalid() == bean.general().generalid())
				{
					temp->CopyFrom(bean.general());
					isExist = true;
					GameSceneLayer* scene = GameView::getInstance()->getGameScene();
					if(scene != NULL)
					{
						BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(bean.general().generalid()));
						if(target != NULL)
						{
							ProtocolHelper::sharedProtocolHelper()->updateFighterHPMP(target, bean.general().activerole().hp(), bean.general().activerole().mp());
						}
					}
						
					break;
				}
			}
			if (!isExist)
			{
				CGeneralDetail * temp = new CGeneralDetail();
				temp->CopyFrom(bean.general());
				GameView::getInstance()->generalsInLineDetailList.push_back(temp);
			}
		}

		GameSceneLayer *scene = (GameSceneLayer*)GameView::getInstance()->getGameScene();
		if (!scene)
			return;

		//add by yangjun 2014.9.30
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene != NULL)
		{
			GeneralsInfoUI * generalsUI = (GeneralsInfoUI*)mainScene->getGeneralInfoUI();
			if (generalsUI)
			{
				CGeneralDetail * temp = new CGeneralDetail();
				temp->CopyFrom(bean.general());
				generalsUI->RefreshSkillByGeneralDetail(temp);
				delete temp;
			}
		}

		//Œ‰Ω´…˝º∂¡À
		if (bean.cause() == 3)
		{
			
			for(int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
			{
				if (bean.general().generalid() == GameView::getInstance()->generalsInLineList.at(i)->id())
				{
					GameView::getInstance()->generalsInLineList.at(i)->set_level(bean.general().activerole().level());
					GameView::getInstance()->generalsInLineList.at(i)->set_fightpoint(bean.general().fightpoint());
					if (GameView::getInstance()->generalsInLineList.at(i)->fightstatus() == 2) //“—≥ˆ’Ω
					{
						MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
						if (mainScene)
						{
							//mainScene->addInterfaceAnm("wjsj/wjsj.anm");

							//MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
							//if (mainAnmScene)
							//{
							//	mainAnmScene->addInterfaceAnm("wjsj/wjsj.anm");
							//}
							GameUtils::playGameSound(MYPLAYER_UPGRADE, 2, false);
						}

						// the level up effect on the general's body
						BaseFighter* pGeneral = dynamic_cast<BaseFighter*>(scene->getActor(bean.general().generalid()));
						if(pGeneral != NULL)
						{
							CCLegendAnimation* effect = CCLegendAnimation::create("animation/texiao/renwutexiao/SJTX/sjtx1.anm");
							pGeneral->addEffect(effect, false, 0);
						}
					}
				}
			}
		}
		

		if (scene->getMainUIScene()->getChildByTag(kTagBackpack) != NULL)
		{
			PackageScene * packageScene = (PackageScene*)scene->getMainUIScene()->getChildByTag(kTagBackpack);
			if (bean.cause() == 3)   //Œ‰Ω´…˝º∂¡À
			{
				//À¢–¬Œ‰Ω´¡–±Ì÷–µƒŒ‰Ω´µ»º∂
				for(int i = 0 ;i<GameView::getInstance()->generalsInLineList.size();++i)
				{
					packageScene->RefreshGeneralLevelInTableView(bean.general().generalid());
				}
				//»Áπ˚ «µ±«∞—°÷–Œ‰Ω´ ‘ÚÀ¢–¬Œ‰Ω´µ»º∂–≈œ¢
				if(packageScene->selectActorId == bean.general().generalid())
				{
					CActiveRole * _activerole = new CActiveRole();
					_activerole->CopyFrom(bean.general().activerole());
					packageScene->RefreshRoleAnimation(_activerole);
					packageScene->RefreshRoleInfo(_activerole,bean.general().fightpoint());
					delete _activerole;
				}
			}
			else
			{
				//»Áπ˚ «µ±«∞—°÷–Œ‰Ω´ ‘ÚÀ¢–¬Œ‰Ω´µ»º∂–≈œ¢
				if(packageScene->selectActorId == bean.general().generalid())
				{
					CActiveRole * _activerole = new CActiveRole();
					_activerole->CopyFrom(bean.general().activerole());
					packageScene->RefreshRoleAnimation(_activerole);
					packageScene->RefreshRoleInfo(_activerole,bean.general().fightpoint());
					delete _activerole;

					std::vector<CEquipment *> temp ;
					for (int i = 0;i<bean.general().equipments_size();++i)
					{
						CEquipment * temp_equipment = new CEquipment();
						temp_equipment->CopyFrom(bean.general().equipments(i));
						temp.push_back(temp_equipment);
					}
					//refresh Equipment
					packageScene->RefreshEquipment(temp);
					//delete vector
					std::vector<CEquipment*>::iterator iter;
					for (iter = temp.begin(); iter != temp.end(); ++iter)
					{
						delete *iter;
					}
					temp.clear();
				}
			}
		}
		else if (scene->getMainUIScene()->getChildByTag(kTagGeneralsUI) != NULL)
		{
			if (GeneralsUI::generalsListUI->isVisible())
			{
				if (bean.cause() == 2)
				{
					CGeneralDetail * temp = new CGeneralDetail();
					temp->CopyFrom(bean.general());
					if (GeneralsUI::generalsListUI->curGeneralBaseMsg->has_id())
					{
						if (GeneralsUI::generalsListUI->curGeneralBaseMsg->id() == bean.general().generalid())
						{
							
							GeneralsUI * generalsUI = (GeneralsUI*)(scene->getMainUIScene()->getChildByTag(kTagGeneralsUI));
							generalsUI->generalsListUI->RefreshGeneralsInfo(temp);
							
						}
					}

					delete temp;
				}
				else
				{
					if (GeneralsUI::generalsListUI->curGeneralBaseMsg->has_id())
					{
						if (GeneralsUI::generalsListUI->curGeneralBaseMsg->id() == bean.general().generalid())
						{
							CGeneralDetail * temp = new CGeneralDetail();
							temp->CopyFrom(bean.general());
							GeneralsUI * generalsUI = (GeneralsUI*)(scene->getMainUIScene()->getChildByTag(kTagGeneralsUI));
							generalsUI->generalsListUI->RefreshGeneralsInfo(temp);
							delete temp;
						}
					}
				}

			}
			
			if (GeneralsUI::generalsTeachUI->isVisible())
			{
				if (bean.cause() == 1)
				{
					GeneralsUI::generalsTeachUI->curTeachGeneralDetail->CopyFrom(bean.general());
					//add AptitudePopupEffect
					GeneralsUI::generalsTeachUI->AddAptitudePopupEffect();
				}
			}

			if (GeneralsUI::generalsEvolutionUI->isVisible())
			{
				if (bean.cause() == 2)
				{
					CGeneralDetail * temp = new CGeneralDetail();
					temp->CopyFrom(bean.general());
					GeneralsUI::generalsEvolutionUI->RefreshGeneralEvolutionInfo(temp);
					//add AptitudePopupEffect
					GeneralsUI::generalsEvolutionUI->AddAptitudePopupEffect();
					delete temp;
				}
			}
		}
		MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
		if (mainscene_ !=NULL)
		{
			if (bean.cause() == 0)
			{
				mainscene_->repairEquipMentRemind();
			}
		}

		///////change by liuzhenxing
		EquipMentUi * equipui = (EquipMentUi*)(scene->getMainUIScene()->getChildByTag(ktagEquipMentUI));
		if (scene->getMainUIScene()->getChildByTag(ktagEquipMentUI) != NULL)
		{
			if(bean.general().generalid()  != equipui->mes_petid)
			{
				return;
			}

			std::vector<CEquipment *> tempVector;
			for (int i = 0;i<bean.general().equipments_size();++i)
			{
				CEquipment * equipments_ = new CEquipment();
				equipments_->CopyFrom(bean.general().equipments(i));

				tempVector.push_back(equipments_);
			}

			for (unsigned int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
			{
				if (bean.general().generalid() == GameView::getInstance()->generalsInLineList.at(i)->id())
				{
					GameView::getInstance()->generalsInLineList.at(i)->set_fightpoint(bean.general().fightpoint());
				}
			}

			//refresh Equipment
			CActiveRole * _activerole = new CActiveRole();
			_activerole->CopyFrom(bean.general().activerole());
			
			equipui->refreshRoleInfo(_activerole,bean.general().fightpoint(),tempVector);

			std::vector<CEquipment*>::iterator iter;
			for (iter = tempVector.begin(); iter != tempVector.end(); ++iter)
			{
				delete *iter;
			}
			tempVector.clear();
			delete _activerole;
		}

		//get change of  general fightPoint 
		int m_new_AllFightPoint = GameView::getInstance()->myplayer->player->fightpoint();
		for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();++i)
		{
			if (GameView::getInstance()->generalsInLineDetailList.at(i)->generalid() == bean.general().generalid())
			{
				GameView::getInstance()->generalsInLineDetailList.at(i)->set_fightpoint(bean.general().fightpoint());
			}
			m_new_AllFightPoint += GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint();
		}
		if (bean.cause() != 0)
		{
			if (m_old_AllFightPoint != m_new_AllFightPoint)
			{
// 				FightPointChangeEffect * fightEffect_ = FightPointChangeEffect::create(m_old_AllFightPoint,m_new_AllFightPoint);
// 				if (fightEffect_)
// 				{
// 					fightEffect_->setTag(kTagFightPointChange);
// 					fightEffect_->setZOrder(SCENE_STORY_LAYER_BASE_ZORDER);
// 					GameView::getInstance()->getMainUIScene()->addChild(fightEffect_);
// 				}
				/*
				MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
				if (mainAnmScene)
				{
					mainAnmScene->addFightPointChangeEffect(m_old_AllFightPoint,m_new_AllFightPoint);
				}
				*/

				EffectOfFightPointChange * temp_effect = new EffectOfFightPointChange();
				temp_effect->setOldFight(m_old_AllFightPoint);
				temp_effect->setNewFight(m_new_AllFightPoint);
				temp_effect->setType(type_fightPoint);
				EffectDispatcher::pushEffectToVector(temp_effect);
			}
		}
	}

	EffectEquipItem::reloadShortItem();
}
