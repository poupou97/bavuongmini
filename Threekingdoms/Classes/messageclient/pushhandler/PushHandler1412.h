
#ifndef Blog_C___Reflection_PushHandler1412_h
#define Blog_C___Reflection_PushHandler1412_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1412 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1412)
    
public:
    SYNTHESIZE(PushHandler1412, int*, m_pValue)
    
    PushHandler1412() ;
    virtual ~PushHandler1412() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
