
#ifndef Blog_C___Reflection_PushHandler7004_h
#define Blog_C___Reflection_PushHandler7004_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler7004 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler7004)

public:
	SYNTHESIZE(PushHandler7004, int*, m_pValue)

	PushHandler7004() ;
	virtual ~PushHandler7004() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
