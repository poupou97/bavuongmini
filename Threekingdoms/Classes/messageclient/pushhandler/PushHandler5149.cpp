#include "PushHandler5149.h"

#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../protobuf/DailyGiftSign.pb.h"
#include "../element/COneMouthSignGift.h"
#include "ui/signMonthly_ui/SignMonthlyData.h"
#include "ui/signMonthly_ui/SignMonthlyUI.h"

IMPLEMENT_CLASS(PushHandler5149)

PushHandler5149::PushHandler5149() 
{

}
PushHandler5149::~PushHandler5149() 
{

}
void* PushHandler5149::createInstance()
{
	return new PushHandler5149() ;
}
void PushHandler5149::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5149::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5149::handle(CommonMessage* mb)
{
	Rsp5149 bean;
	bean.ParseFromString(mb->data());

	for (int i = 0;i<SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size();i++)
	{
		if (bean.day() == SignMonthlyData::getInstance()->m_oneMonthSighGiftData.at(i)->day())
		{
			if (bean.status() == 1)
			{
				SignMonthlyData::getInstance()->m_oneMonthSighGiftData.at(i)->set_status(3);
			}
		}
	}

	//refresh iscanGetGift
	bool isExist = false;
	for (int i = 0;i<SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size();i++)
	{
		if (SignMonthlyData::getInstance()->m_oneMonthSighGiftData.at(i)->status() == 2)
		{
			isExist = true;
			break;
		}
	}
	if (isExist)
	{
		SignMonthlyData::getInstance()->m_bCanGetGift = true;
	}
	else
	{
		SignMonthlyData::getInstance()->m_bCanGetGift = false;
	}

	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainscene == NULL)
		return;

	SignMonthlyUI * signMonthlyUI = (SignMonthlyUI*)mainscene->getChildByTag(kTagSignMonthlyUI);
	if (signMonthlyUI)
	{
		signMonthlyUI->RefreshByDays(bean.day());
	}
}

