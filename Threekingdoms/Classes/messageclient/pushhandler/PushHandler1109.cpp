
#include "PushHandler1109.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/General.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../GameView.h"

IMPLEMENT_CLASS(PushHandler1109)

PushHandler1109::PushHandler1109() 
{
    
}
PushHandler1109::~PushHandler1109() 
{
    
}
void* PushHandler1109::createInstance()
{
    return new PushHandler1109() ;
}
void PushHandler1109::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1109::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1109::handle(CommonMessage* mb)
{
	Push1109 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, movemode: %d", mb->cmdid(), bean.movemode());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	for(int i = 0; i < bean.rolepositions_size(); i++)
	{
		BaseFighter* bf = dynamic_cast<BaseFighter*>(scene->getActor(bean.rolepositions(i).roleid()));
		//CCLOG("role id: %d", bean.rolepositions(i).roleid());
		//CCAssert(bf != NULL, "should not null");
		if(bf == NULL)
			break;

		if(bf->isAction(ACT_DIE))
			break;

		// synchronize the real position
		CCPoint oldRealPostion = bf->getRealWorldPosition();
		bf->setRealWorldPosition(ccp(bean.rolepositions(i).x(), bean.rolepositions(i).y()));

		int actorType = bf->getActiveRole()->type();

		// setup move speed
		if (actorType == GameActor::type_monster) {
			const int MOVE_UNIT = 64 * 3;   // keep the same data with the server
			float speed = MOVE_UNIT / (bf->getActiveRole()->speed() / 1000.f);
			//float speed = 200.f;

			Monster* m = dynamic_cast<Monster*>(bf);
			//if(m->GetFSM()->isInState(*(MonsterStand::Instance())))
			//	speed = speed * 0.1f;
			m->setMoveSpeed(speed);
		}
		else if(actorType == GameActor::type_pet)
		{
			//CCLOG("general move, target: (%d, %d)", bean.rolepositions(i).x(), bean.rolepositions(i).y());
			if(bf->isMyPlayerGroup())
			{
				// the target is same as current position
				if(oldRealPostion.equals(bf->getRealWorldPosition()))
					return;

				bf->setMoveSpeed(GENERAL_FIGHT_MOVESPEED);   // the move speed when fighting
			}
		}
		
		// setup move command
		BaseFighterCommandMove* cmd = new BaseFighterCommandMove();
		if(actorType == GameActor::type_monster
			/*|| actorType == GameActor::type_pet*/)
		{
			cmd->targetPosition = ccp(bean.rolepositions(i).targetx(), bean.rolepositions(i).targety());
			cmd->stopAtEndPoint = true;
		}
		else
		{
			if(bean.movemode() == 1)
			{
				cmd->targetPosition = ccp(bean.rolepositions(i).targetx(), bean.rolepositions(i).targety());
				cmd->stopAtEndPoint = false;

				if(actorType == GameActor::type_pet /*&& bf->isMyPlayerGroup()*/)
				{
					delete cmd;

					General* general = dynamic_cast<General*>(bf);
					general->followMaster(general->getOwnerId(), bean.rolepositions(i).position());
					return;
				}
			}
			else
			{
				float x = bean.rolepositions(i).x();
				float y = bean.rolepositions(i).y();
				if(bf->isMyPlayerGroup())
				{
					// find the random position near the target, then the generals will not stand on the same point
					const int range = 64;
					x += (CCRANDOM_0_1() - 0.5f) * range;
					y += (CCRANDOM_0_1() - 0.5f) * range;
				}
				cmd->targetPosition = ccp(x, y);
				cmd->stopAtEndPoint = true;   // stop when reach target
			}
		}

		//if(bf->isAttacking())
		//	bf->setNextCommand(cmd, false);
		//else
		//	bf->setNextCommand(cmd, true);
		bf->setNextCommand(cmd, true);
	}
}
