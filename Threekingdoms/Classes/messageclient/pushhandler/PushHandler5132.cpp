#include "PushHandler5132.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../protobuf/DailyGift.pb.h"
#include "../element/CPlayerGetPhypower.h"
#include "../../ui/Friend_ui/FriendGetPhyPower.h"

IMPLEMENT_CLASS(PushHandler5132)

PushHandler5132::PushHandler5132() 
{

}
PushHandler5132::~PushHandler5132() 
{
	
}
void* PushHandler5132::createInstance()
{
	return new PushHandler5132() ;
}
void PushHandler5132::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5132::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5132::handle(CommonMessage* mb)
{
	Rsp5132 bean;
	bean.ParseFromString(mb->data());
	
	std::vector<CPlayerGetPhypower*>::iterator iter;
	for (iter = GameView::getInstance()->playerPhypowervector.begin(); iter != GameView::getInstance()->playerPhypowervector.end(); ++iter)
	{
		delete *iter;
	}
	GameView::getInstance()->playerPhypowervector.clear();

	if (bean.playergetphypower_size() > 0)
	{
		for (int i = 0;i<bean.playergetphypower_size();i++)
		{
			CPlayerGetPhypower * phypower_ = new CPlayerGetPhypower();
			phypower_->CopyFrom(bean.playergetphypower(i));
			GameView::getInstance()->playerPhypowervector.push_back(phypower_);
		}

		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		FriendGetPhyPower * phypowerUi =  FriendGetPhyPower::create(bean.number());
		phypowerUi->ignoreAnchorPointForPosition(false);
		phypowerUi->setAnchorPoint(ccp(0.5f,0.5f));
		phypowerUi->setPosition(ccp(winsize.width/2,winsize.height/2));
		phypowerUi->setTag(kTagGetphyPowerUi);
		GameView::getInstance()->getMainUIScene()->addChild(phypowerUi);
	}else
	{
		const char * str_ = StringDataManager::getString("friend_playerOfSendPhyPowerIsNull_ui");
		GameView::getInstance()->showAlertDialog(str_);
	}
}