
#ifndef Blog_C___Reflection_PushHandler5118_h
#define Blog_C___Reflection_PushHandler5118_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5118 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5118)

public:
	SYNTHESIZE(PushHandler5118, int*, m_pValue)

		PushHandler5118() ;
	virtual ~PushHandler5118() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
