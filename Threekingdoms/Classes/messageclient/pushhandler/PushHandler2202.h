
#ifndef Blog_C___Reflection_PushHandler2202_h
#define Blog_C___Reflection_PushHandler2202_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler2202 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler2202)
    
public:
    SYNTHESIZE(PushHandler2202, int*, m_pValue)
    
    PushHandler2202() ;
    virtual ~PushHandler2202() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
