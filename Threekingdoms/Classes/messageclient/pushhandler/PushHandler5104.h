
#ifndef Blog_C___Reflection_PushHandler5104_h
#define Blog_C___Reflection_PushHandler5104_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5104 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5104)

public:
	SYNTHESIZE(PushHandler5104, int*, m_pValue)

	PushHandler5104() ;
	virtual ~PushHandler5104() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
