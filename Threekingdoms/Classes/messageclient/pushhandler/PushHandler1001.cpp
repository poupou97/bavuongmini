
#include "PushHandler1001.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/ModelMessage.pb.h"

#include "../../GameView.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../element/FolderInfo.h"
#include "../element/CShortCut.h"
#include "../element/CEquipment.h"
#include "../GameMessageProcessor.h"
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"
#include "../../ui/GameUIUtils.h"

IMPLEMENT_CLASS(PushHandler1001)

PushHandler1001::PushHandler1001() 
{
    
}
PushHandler1001::~PushHandler1001() 
{
    
}
void* PushHandler1001::createInstance()
{
    return new PushHandler1001() ;
}
void PushHandler1001::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1001::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1001::handle(CommonMessage* mb)
{
	Rsp1001 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, login to game server", mb->cmdid());

	GameView* gameView = GameView::getInstance();

	//gameView->myplayer = new MyPlayer();
	gameView->myplayer->player->CopyFrom(bean.player());

	//add by yangjun 2013.10.11
	//delete old
	std::vector<CEquipment*>::iterator iter;
	for (iter = gameView->EquipListItem.begin(); iter != gameView->EquipListItem.end(); ++iter)
	{
		delete *iter;
	}
	gameView->EquipListItem.clear();

	//add new
	int size_= bean.player().equipments_size();
	for (int i = 0;i<size_;i++)
	{
		CEquipment * _equipment = new CEquipment();
		_equipment->CopyFrom(bean.player().equipments(i));
		gameView->EquipListItem.push_back(_equipment);
	}
		
	// 由于activeRole中的profession没有赋值，默认值是“武士”，这里用外面的值来赋值
	gameView->myplayer->getActiveRole()->set_profession(bean.player().profession());
	gameView->myplayer->setProfession(BasePlayer::getProfessionIdxByName(bean.player().profession()));
	gameView->myplayer->setRoleId(gameView->myplayer->getActiveRole()->rolebase().roleid());
	BaseFighter::setMyPlayerId(gameView->myplayer->getRoleId());

	// init - FivePersonInstance
	FivePersonInstance::init();

	// init AI config
	std::string playerName = gameView->myplayer->getActiveRole()->rolebase().name();
	MyPlayerAIConfig::loadConfig(playerName);

	//std::string body = "";
	//if(gameView->myplayer->getActiveRole()->has_body())
	//	body = gameView->myplayer->getActiveRole()->body();
	//gameView->myplayer->removeAllChildren();
	//gameView->myplayer->init(body.c_str());
		
	//if (bean.getPlayer().getMBActiveRole().getStat() == 1) {
	//	// 死亡
	//	gameView->myplayer.setIsReadyFall(true);
	//}
		
//	if (bean.player.equipments != null) {
////			Debug.LogI("unpakRsp1001", "equipments size============="+equipments.size());
//		for (int i = 0; i < bean.player.equipments.size(); i++) {
//			MBEquipment equip = bean.player.equipments.get(i);
//			/** 穿着的部位 0 头 1 颈 2 身 3 腰 4 手 5 左腕 6 右腕 7 左指 8 右指 9脚 */
//			Goods gds = GameUnpackPush.setEquipment(equip);
//			if (gds != null) {
//				gds.where = Goods.WHERE_ATTRIBUTE;
//				gameView.equipList.add(gds);
//			}
//		}
//	}
	
	/** 快捷栏格子列表 */
	if(gameView->shortCutList.size()>0)
	{
		std::vector<CShortCut*>::iterator _iter;
		for (_iter = gameView->shortCutList.begin(); _iter != gameView->shortCutList.end(); ++_iter)
		{
			delete *_iter;
		}
		gameView->shortCutList.clear();
	}

	if (bean.shortcuts_size()>0)
	{
		for (int i = 0; i < bean.shortcuts_size(); i++) 
		{
			CShortCut *shortCut = new CShortCut();
			shortCut->CopyFrom(bean.shortcuts(i));
			//folder.where = Goods.WHERE_PROP;
			gameView->shortCutList.push_back(shortCut);
		}
	}

	MainScene * mainScene = GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		ShortcutLayer * shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
		if (shortcutLayer)
		{
			//refresh shortcutList
			for (unsigned int i = 0;i<GameView::getInstance()->shortCutList.size();++i)
			{
				CShortCut * shortcut = GameView::getInstance()->shortCutList.at(i);

				ShortcutSlot * temp = (ShortcutSlot*)shortcutLayer->getChildByTag(shortcut->index()+100);
				if (!temp)
					continue;

				if (!temp->isVisible())
					continue;
				
				shortcutLayer->RefreshOneShortcutSlot(shortcut);
			}
		}
	}

	//req package info
	//add by yangjun 2013.11.6
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1301, this);

	//req goldStoreInfo 
	//add by yangjun 2014.1.9
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1704, (void *)0);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1704, (void *)1);

	//req generalsInLineList
	//add by yangjun 2014.1.15
	GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
	temp->page = 0;
	temp->pageSize = 20;
	temp->type = 5;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
	delete temp;

	//req generalsSkillList
	//add by yangjun 2014.3.8
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5055, this);

	ActorUtils::preloadSkillEffects(gameView->myplayer->getProfession());
	GameUIUtils::preloadEffects();
}