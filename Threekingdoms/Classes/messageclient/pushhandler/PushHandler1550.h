
#ifndef Blog_C___Family_PushHandler1550_h
#define Blog_C___Family_PushHandler1550_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1550 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1550)
    
public:
    SYNTHESIZE(PushHandler1550, int*, m_pValue)
    
    PushHandler1550() ;
    virtual ~PushHandler1550() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
