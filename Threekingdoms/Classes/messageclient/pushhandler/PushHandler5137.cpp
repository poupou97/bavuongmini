#include "PushHandler5137.h"
#include "../protobuf/DailyGift.pb.h"
#include "../../ui/moneyTree_ui/MoneyTreeData.h"
#include "GameView.h"
#include "../../ui/moneyTree_ui/MoneyTreeUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/moneyTree_ui/MoneyTreeTimeManager.h"

IMPLEMENT_CLASS(PushHandler5137)

PushHandler5137::PushHandler5137() 
{

}
PushHandler5137::~PushHandler5137() 
{
	
}
void* PushHandler5137::createInstance()
{
	return new PushHandler5137() ;
}
void PushHandler5137::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5137::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5137::handle(CommonMessage* mb)
{
	Push5137 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	MoneyTreeData* pMoneyTreeData = MoneyTreeData::instance();
	pMoneyTreeData->set_maxNum(bean.number());					// 能免费摇钱的最大值
	pMoneyTreeData->set_hasUseNum(bean.usenumber());				// 已经摇钱几次
	pMoneyTreeData->set_needTime(bean.time());					// 下一次需要的时间（毫秒）
	pMoneyTreeData->set_currentCount(bean.usenumber() + 1);		// 当前可摇（第几次，默认从1开始）
	

	// (1)refresh moneyTree ui
	// (2)通知“奖励”面板，更新时间
	
	if(NULL != GameView::getInstance()->getMainUIScene())
	{
		// 与服务器数据同步
		MoneyTreeTimeManager * pMoneyTreeTimeManager = (MoneyTreeTimeManager *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMoneyTreeManager);
		if (NULL != pMoneyTreeTimeManager)
		{
			pMoneyTreeTimeManager->initDataFromIntent();
		}
		{
			MoneyTreeTimeManager* moneyTreeTimeManager = MoneyTreeTimeManager::create();
			moneyTreeTimeManager->initDataFromIntent();
			moneyTreeTimeManager->setTag(kTagMoneyTreeManager);

			GameView::getInstance()->getMainUIScene()->addChild(moneyTreeTimeManager);
		}

		// refresh moneyTree ui
		MoneyTreeUI * pMoneyTreeUI = (MoneyTreeUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMoneyTreeUI);
		if (NULL != pMoneyTreeUI)
		{
			pMoneyTreeUI->initDataFromInternet();
		}
	}
}