
#ifndef Blog_C___Reflection_PushHandler5094_h
#define Blog_C___Reflection_PushHandler5094_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5094 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5094)

public:
	SYNTHESIZE(PushHandler5094, int*, m_pValue)

	PushHandler5094() ;
	virtual ~PushHandler5094() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
