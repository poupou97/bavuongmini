
#ifndef Blog_C___Reflection_PushHandler5103_h
#define Blog_C___Reflection_PushHandler5103_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5103 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5103)

public:
	SYNTHESIZE(PushHandler5103, int*, m_pValue)

	PushHandler5103() ;
	virtual ~PushHandler5103() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
