#include "PushHandler5116.h"
#include "../protobuf/ActiveMessage.pb.h"
#include "../../ui/Active_ui/ActiveData.h"
#include "../element/CActiveLabel.h"
#include "../element/CActiveNote.h"
#include "../element/CActiveLabelToday.h"

IMPLEMENT_CLASS(PushHandler5116)

PushHandler5116::PushHandler5116() 
{

}
PushHandler5116::~PushHandler5116() 
{
	
}
void* PushHandler5116::createInstance()
{
	return new PushHandler5116() ;
}
void PushHandler5116::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5116::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5116::handle(CommonMessage* mb)
{
	Push5116 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// 清空 保存服务器的数据
	ActiveData::instance()->clearLabelAndNote();

	// 保存 服务器数据 到 ActiveData
	int nActiveLabelSize = bean.label_size();
	for (int i = 0; i < nActiveLabelSize; i++)
	{
		CActiveLabel* activeLabel = new CActiveLabel();
		activeLabel->CopyFrom(bean.label(i));

		ActiveData::instance()->m_vector_internet_label.push_back(activeLabel);
	}

	int nActiveNoteSize = bean.note_size();
	for (int j = 0; j < nActiveNoteSize; j++)
	{
		CActiveNote* activeNote = new CActiveNote();
		activeNote->CopyFrom(bean.note(j));

		ActiveData::instance()->m_vector_internet_note.push_back(activeNote);
	}

	// 今日活跃度数据
	int nActiveTodaySize = bean.activelabletoday_size();
	for (int k = 0; k < nActiveTodaySize; k++)
	{
		CActiveLabelToday * activeToday = new CActiveLabelToday();
		activeToday->CopyFrom(bean.activelabletoday(k));

		ActiveData::instance()->m_vector_internet_labelToday.push_back(activeToday);
	}
	

	if (bean.has_numbertoday())																	// 获取今日活跃度
	{
		ActiveData::instance()->set_todayActive(bean.numbertoday());
	}

	if (bean.has_number())																		// 获取活跃度
	{
		ActiveData::instance()->setUserActive(bean.number());
	}

	// 更新 服务器数据 到 native_label 和 native_note
	ActiveData::instance()->initLabelFromInternet();
	ActiveData::instance()->initNoteFromInternet();
}