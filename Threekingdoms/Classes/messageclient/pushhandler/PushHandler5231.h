#ifndef Blog_C___Reflection_PushHandler5231_h
#define Blog_C___Reflection_PushHandler5231_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5231 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5231)

public:
	SYNTHESIZE(PushHandler5231, int*, m_pValue)

	PushHandler5231() ;
	virtual ~PushHandler5231() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
