
#ifndef Blog_C___Reflection_PushHandler5012_h
#define Blog_C___Reflection_PushHandler5012_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5012 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5012)

public:
	SYNTHESIZE(PushHandler5012, int*, m_pValue)

		PushHandler5012() ;
	virtual ~PushHandler5012() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
