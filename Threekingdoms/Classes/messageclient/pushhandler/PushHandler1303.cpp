
#include "PushHandler1303.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../GameView.h"
#include "../../ui/backpackscene/EquipmentItem.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../element/CEquipment.h"
#include "../element/CGeneralBaseMsg.h"
#include "../element/CActiveRole.h"
#include "../GameMessageProcessor.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/equipMent_ui/RefreshEquipData.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../ProtocolHelper.h"
#include "../../utils/GameUtils.h"
#include "GameAudio.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../gamescene_state/role/BaseFighterConstant.h"
#include "../../gamescene_state/role/General.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/EffectDispatch.h"
#include "../../gamescene_state/MainAnimationScene.h"

IMPLEMENT_CLASS(PushHandler1303)

PushHandler1303::PushHandler1303() 
{

}
PushHandler1303::~PushHandler1303() 
{

}
void* PushHandler1303::createInstance()
{
	return new PushHandler1303() ;
}
void PushHandler1303::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1303::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1303::unEquip(GameSceneLayer* scene, int part)
{
	if(scene == NULL)
		return;

	int profession = GameView::getInstance()->myplayer->getProfession();

	// 处理人物背包界面中的角色动画
	PackageScene * packageScene = (PackageScene*)scene->getMainUIScene()->getChildByTag(kTagBackpack);
	if (packageScene != NULL)
	{
		switch (part) {
		case CEquipment::HAND:   // 武器
			BasePlayer::switchWeapon("", packageScene->getRoleAnimation(), profession);
			break;
		default:
			break;
		}
	}

	// 处理场景中的角色动画
	switch (part) {		
	case CEquipment::HAND:   // 武器
		BasePlayer::switchWeapon("", GameView::getInstance()->myplayer->getAnim(), profession);
		GameView::getInstance()->myplayer->getActiveRole()->set_hand("");

		GameView::getInstance()->myplayer->removeAllWeaponEffects();
		break;
	default:
		break;
	}
}

void PushHandler1303::handle(CommonMessage* mb)
{
	Push1303 bean;
	bean.ParseFromString(mb->data());

	int pob = bean.pob();
	CEquipment * equipment = new CEquipment();
	equipment->CopyFrom(bean.equipment());
	Aptitude * aptitude = new Aptitude();
	aptitude->CopyFrom(bean.aptitude());
	int hp = bean.hp();
	int mp = bean.mp();
	int maxhp = bean.maxhp();
	int maxmp = bean.maxmp();
	int wingHaloStyle = bean.winghalostyle();
	int wingColre = bean.wingcolor();
	int fightPoint_ = bean.fightpoint();

	//CCLOG("msg: %d, equipment part: %d, avatar: %s", mb->cmdid(), equipment->part(), equipment->avatar().c_str());
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	// the event for un-equip one equipment
	bool bHasEquipmentInfo = bean.has_equipment();
	if(! bHasEquipmentInfo)
	{
		if (bean.has_generalid())
		{
			if (bean.generalid() == 0)
			{
				unEquip(scene, bean.pob());
			}
		}
		else
		{
			unEquip(scene, bean.pob());
		}
	}

	MainScene * mainscene_ = scene->getMainUIScene();
	
	if(scene != NULL)
	{
		//create new 
		if (bean.has_equipment())
		{
			//update data
			if ((!bean.has_generalid()) || (bean.generalid() == 0))
			{
				for (unsigned int i = 0;i<GameView::getInstance()->EquipListItem.size();i++)
				{
					if (GameView::getInstance()->EquipListItem.at(i)->part() == pob)
					{
						CEquipment * temp = GameView::getInstance()->EquipListItem.at(i);
						std::vector<CEquipment*>::iterator iter = GameView::getInstance()->EquipListItem.begin()+i;
						GameView::getInstance()->EquipListItem.erase(iter);
						delete temp;
					}
				}
				GameView::getInstance()->EquipListItem.push_back(equipment);
			}
			// role update equipment
// 			if (mainscene_ != NULL)
// 			{
// 				mainscene_->repairEquipMentRemind();
// 			}
		}
		else
		{
			//update data
			if ((!bean.has_generalid()) || (bean.generalid() == 0))
			{
				for (unsigned int i = 0;i<GameView::getInstance()->EquipListItem.size();i++)
				{
					if (GameView::getInstance()->EquipListItem.at(i)->part() == pob)
					{
						
						CEquipment * temp = GameView::getInstance()->EquipListItem.at(i);
						std::vector<CEquipment*>::iterator iter = GameView::getInstance()->EquipListItem.begin()+i;
						GameView::getInstance()->EquipListItem.erase(iter);
						delete temp;
					}
				}
			}
			//delete old equipment
// 			if (mainscene_ != NULL)
// 			{
// 				mainscene_->repairEquipMentRemind();
// 			}
		}

		CCSize winSize = CCDirector::sharedDirector()->getWinSize();
		int _w = (winSize.width-800)/2;
		int _h = (winSize.height - 480)/2;

		if ((!bean.has_generalid()) || (bean.generalid() == 0))
		{
			CCNodeRGBA* pEffect = AptitudePopupEffect::create(GameView::getInstance()->myplayer->player->mutable_aptitude(), bean.mutable_aptitude());
			CCNode *backageUI = GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
			CCNode *equpimentUI = GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
			CCNode *equip_remind_ui = GameView::getInstance()->getMainUIScene()->getMainUIElementLayer()->getChildByTag(GameView::getInstance()->getMainUIScene()->getElementLayerOfTag());
			if(backageUI != NULL)
			{
				pEffect->setPosition(220+_w, 250+_h);
				backageUI->addChild(pEffect);
			}
			else if(equpimentUI != NULL)
			{
				pEffect->setPosition(220+_w, 250+_h);
				equpimentUI->addChild(pEffect);
			}
			else if (equip_remind_ui != NULL)
			{
// 				pEffect->setPosition(ccp(winSize.width - 90 - 235,165));
// 				pEffect->setScale(0.8f);
// 				pEffect->setTag(101124);
// 				GameView::getInstance()->getMainUIScene()->addChild(pEffect);
// 				
// 				AptitudePopupEffect * temp_ = (AptitudePopupEffect *)pEffect;
// 				if (temp_)
// 				{
// 					float delay_ = temp_->getActionTotalTime();
// 					CCFiniteTimeAction*  action2 = CCSequence::create(
// 						CCDelayTime::create(delay_),
// 						CCRemoveSelf::create(),
// 						NULL);
// 					equip_remind_ui->runAction(action2);
// 				}
// 				GameView::getInstance()->getMainUIScene()->setElementLayerOfTag(GameView::getInstance()->getMainUIScene()->getElementLayerOfTag() - 1);
			}
			
			
			CCNodeRGBA* pEffectOnHead = AptitudePopupEffect::create(GameView::getInstance()->myplayer->player->mutable_aptitude(), bean.mutable_aptitude());
			GameView::getInstance()->myplayer->addEffect(pEffectOnHead, false, MYPLAYER_APTITUDE_EFFECT_HEIGHT);

			GameView::getInstance()->myplayer->player->mutable_aptitude()->CopyFrom(bean.aptitude());
			GameView::getInstance()->myplayer->getActiveRole()->set_hp(hp);
			GameView::getInstance()->myplayer->getActiveRole()->set_mp(mp);
			GameView::getInstance()->myplayer->getActiveRole()->set_maxmp(maxmp);
			GameView::getInstance()->myplayer->getActiveRole()->set_maxhp(maxhp);
		}
		else
		{
			Aptitude* oldAptitude;
			bool isExist = false;
			for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
			{
				if (bean.generalid() == GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
				{
					isExist = true;
					oldAptitude = GameView::getInstance()->generalsInLineDetailList.at(i)->mutable_aptitude();
				}
			}
			if (isExist)
			{
				CCNodeRGBA* pEffect = AptitudePopupEffect::create(oldAptitude, bean.mutable_aptitude());
				CCNode *backageUI = GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
				CCNode *equpimentUI = GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
				CCNode *equip_remind_ui = GameView::getInstance()->getMainUIScene()->getMainUIElementLayer()->getChildByTag(GameView::getInstance()->getMainUIScene()->getElementLayerOfTag());
				if(backageUI != NULL)
				{
					pEffect->setPosition(220+_w, 250+_h);
					backageUI->addChild(pEffect);
				}
				else if(equpimentUI != NULL)
				{
					pEffect->setPosition(220+_w, 250+_h);
					equpimentUI->addChild(pEffect);
				}
				
				else if (equip_remind_ui != NULL)
				{
					/*
					pEffect->setPosition(ccp(winSize.width - 90 - 235,165));
					pEffect->setScale(0.8f);
					pEffect->setTag(101124);
					GameView::getInstance()->getMainUIScene()->addChild(pEffect);
					//getActionTotalTime
					AptitudePopupEffect * temp_ = (AptitudePopupEffect *)pEffect;
					if (temp_)
					{
						float delay_ = temp_->getActionTotalTime();
						CCFiniteTimeAction*  action2 = CCSequence::create(
							CCDelayTime::create(delay_),
							CCRemoveSelf::create(),
							NULL);
						equip_remind_ui->runAction(action2);
					}
					GameView::getInstance()->getMainUIScene()->setElementLayerOfTag(GameView::getInstance()->getMainUIScene()->getElementLayerOfTag() - 1);
					*/
				}
				

				General* target = dynamic_cast<General*>(scene->getActor(bean.generalid()));
				if(target != NULL)
				{
					CCNodeRGBA* pEffectOnHead = AptitudePopupEffect::create(oldAptitude, bean.mutable_aptitude());
					target->addEffect(pEffectOnHead, false, MYPLAYER_APTITUDE_EFFECT_HEIGHT);
				}

				if(target != NULL)
				{
					oldAptitude->CopyFrom(bean.aptitude());
					target->getActiveRole()->set_hp(hp);
					target->getActiveRole()->set_mp(mp);
					target->getActiveRole()->set_maxmp(maxmp);
					target->getActiveRole()->set_maxhp(maxhp);
				}
			}
		}

		PackageScene * packageScene = (PackageScene *)scene->getMainUIScene()->getChildByTag(kTagBackpack);
		if ((!bean.has_generalid()) || (bean.generalid() == 0))
		{
			
			int m_old_AllFightPoint = GameView::getInstance()->myplayer->player->fightpoint();
			int m_new_AllFightPoint = bean.fightpoint();
			
			for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();++i)
			{
				m_old_AllFightPoint += GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint();
				m_new_AllFightPoint += GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint();
			}
			
			if (m_old_AllFightPoint != m_new_AllFightPoint)
			{
// 				FightPointChangeEffect * fightEffect_ = FightPointChangeEffect::create(m_old_AllFightPoint,m_new_AllFightPoint);
// 				if (fightEffect_)
// 				{
// 					fightEffect_->setTag(kTagFightPointChange);
// 					fightEffect_->setZOrder(SCENE_STORY_LAYER_BASE_ZORDER);
// 					GameView::getInstance()->getMainUIScene()->addChild(fightEffect_);
// 				}
				/*
				MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
				if (mainAnmScene)
				{
					mainAnmScene->addFightPointChangeEffect(m_old_AllFightPoint,m_new_AllFightPoint);
				}
				*/

				EffectOfFightPointChange * temp_effect = new EffectOfFightPointChange();
				temp_effect->setOldFight(m_old_AllFightPoint);
				temp_effect->setNewFight(m_new_AllFightPoint);
				temp_effect->setType(type_fightPoint);
				EffectDispatcher::pushEffectToVector(temp_effect);
			}
			

			GameView::getInstance()->myplayer->player->set_fightpoint(bean.fightpoint());
			CActiveRole * temp = new CActiveRole();
			temp->CopyFrom(*GameView::getInstance()->myplayer->getActiveRole());
			if (packageScene != NULL)
			{
				if(packageScene->selectActorId == 0 || GameView::getInstance()->isOwn(packageScene->selectActorId))
				{
					packageScene->RefreshRoleInfo(temp,GameView::getInstance()->myplayer->player->fightpoint());
					packageScene->ReloadProperty();
					packageScene->RefreshRoleAnimation(temp);

					switch (equipment->part()) {		
					case CEquipment::HAND:
						{
							if (packageScene->getRoleAnimation() != NULL)
							{
								BasePlayer::switchWeapon(equipment->avatar().c_str(), packageScene->getRoleAnimation(), GameView::getInstance()->myplayer->getProfession());
								GameView::getInstance()->myplayer->getActiveRole()->set_hand(equipment->avatar());
							}
						}
						break;
					default:
						break;
					}

					delete temp;

					UIPanel * playerPanel = packageScene->playerPanel;
					//delete old
					if (playerPanel->getChildByName(packageScene->equipmentItem_name[pob].c_str())!=NULL)
					{
						playerPanel->getChildByName(packageScene->equipmentItem_name[pob].c_str())->removeFromParentAndCleanup(true);
						packageScene->setPresentVoidEquipItem(pob,true);
					}

					//create new 
					if (bean.has_equipment())
					{
						EquipmentItem * equip_item = EquipmentItem::create(equipment);
						equip_item->setAnchorPoint(ccp(0,0));
						equip_item->setName(packageScene->equipmentItem_name[equipment->part()].c_str());
						playerPanel->addChild(equip_item);

						packageScene->setPresentVoidEquipItem(equipment->part(),false);
					}
					//release
					delete aptitude;
				}
			}
		}
		else
		{
			int m_old_AllFightPoint = GameView::getInstance()->myplayer->player->fightpoint();
			int m_new_AllFightPoint = GameView::getInstance()->myplayer->player->fightpoint();

			for (unsigned int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();++i)
			{
				//callBack fightValue 
				m_old_AllFightPoint += GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint();

				if (bean.generalid() == GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
				{
					GameView::getInstance()->generalsInLineDetailList.at(i)->set_fightpoint(bean.fightpoint());
				}

				m_new_AllFightPoint += GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint();
			}

			
			if (m_old_AllFightPoint != m_new_AllFightPoint)
			{
// 				FightPointChangeEffect * fightEffect_ = FightPointChangeEffect::create(m_old_AllFightPoint,m_new_AllFightPoint);
// 				if (fightEffect_)
// 				{
// 					fightEffect_->setTag(kTagFightPointChange);
// 					fightEffect_->setZOrder(SCENE_STORY_LAYER_BASE_ZORDER);
// 					GameView::getInstance()->getMainUIScene()->addChild(fightEffect_);
// 				}
				/*
				MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
				if (mainAnmScene)
				{
					mainAnmScene->addFightPointChangeEffect(m_old_AllFightPoint,m_new_AllFightPoint);
				}
				*/
				EffectOfFightPointChange * temp_effect = new EffectOfFightPointChange();
				temp_effect->setOldFight(m_old_AllFightPoint);
				temp_effect->setNewFight(m_new_AllFightPoint);
				temp_effect->setType(type_fightPoint);
				EffectDispatcher::pushEffectToVector(temp_effect);
			}
			
			//请求详细信息
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)bean.generalid());
		}

		// switch weapon for the role who is standing in the scene
		if ((!bean.has_generalid()) || (bean.generalid() == 0))
		{
			// switch equiment for the player
			switch (equipment->part()) {		
			case CEquipment::HAND:
				// 武器
				BasePlayer::switchWeapon(equipment->avatar().c_str(), GameView::getInstance()->myplayer->getAnim(), GameView::getInstance()->myplayer->getProfession());
				GameView::getInstance()->myplayer->getActiveRole()->set_hand(equipment->avatar());
				break;

			default:
				break;
			}
		}
	}
	
	if(pob == CEquipment::HAND)   // weapon
	{
		GameView::getInstance()->myplayer->initWeaponEffect();
	}
	//update role and general equipment
	if (mainscene_ != NULL)
	{
		if ((!bean.has_generalid()) || (bean.generalid() == 0))
		{
			mainscene_->repairEquipMentRemind();
		}
	}

	if(scene != NULL)
	{
		if (scene->getMainUIScene()->getChildByTag(ktagEquipMentUI))
		{
			EquipMentUi * equip = (EquipMentUi *)scene->getMainUIScene()->getChildByTag(ktagEquipMentUI);
			
			for (unsigned int i = 0;i<equip->curMainEquipvector.size();i++)
			{
				if (equip->curMainEquipvector.at(i)->goods().instanceid() == bean.equipment().goods().instanceid())
				{
					std::vector<CEquipment *>::iterator iter= equip->curMainEquipvector.begin() + i;
					CEquipment * equipV =*iter ;
					equip->curMainEquipvector.erase(iter);
					delete equipV;

					CEquipment * temp_ =new CEquipment();
					temp_->CopyFrom(bean.equipment());
					equip->curMainEquipvector.push_back(temp_);
					break;
				}
			}
			
			equip->generalList->generalList_tableView->selectCell(equip->tabviewSelectIndex);
		}
	}

	//updata hp,mp
	if (!bean.has_generalid())
	{
		ProtocolHelper::sharedProtocolHelper()->updateFighterHPMP(GameView::getInstance()->myplayer, bean.hp(), bean.mp());
	}
	else
	{
		if (bean.generalid() == 0)
		{
			ProtocolHelper::sharedProtocolHelper()->updateFighterHPMP(GameView::getInstance()->myplayer, bean.hp(), bean.mp());
		}
		else
		{
			BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(bean.generalid()));
			if(target == NULL)
				return;

			ProtocolHelper::sharedProtocolHelper()->updateFighterHPMP(target, bean.hp(), bean.mp());

		}
	}
}
