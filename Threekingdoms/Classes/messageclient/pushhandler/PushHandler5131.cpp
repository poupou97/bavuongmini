#include "PushHandler5131.h"
#include "../protobuf/ActiveMessage.pb.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Active_ui/ActiveData.h"

IMPLEMENT_CLASS(PushHandler5131)

PushHandler5131::PushHandler5131() 
{

}
PushHandler5131::~PushHandler5131() 
{
	
}
void* PushHandler5131::createInstance()
{
	return new PushHandler5131() ;
}
void PushHandler5131::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5131::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5131::handle(CommonMessage* mb)
{
	Rsp5131 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int nActiveNeed = bean.number();					// 领取的是多少活跃度的奖励
	int nPresentStatus = bean.status();				// 领取状态 0.成功，,-1包满了，1今天领取过了
	
	if (0 == nPresentStatus)
	{
		// 更新btn状态（更新今日活跃度vector）
		ActiveData::instance()->updateTodayActive(nActiveNeed);
	}
	
	ActiveUI* pTmpActiveUI = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	if (NULL != pTmpActiveUI)
	{
		// 更新ActiveUI
		pTmpActiveUI->initActiveUIData();
	}
}