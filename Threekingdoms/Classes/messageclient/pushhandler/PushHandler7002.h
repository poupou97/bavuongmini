
#ifndef Blog_C___Reflection_PushHandler7002_h
#define Blog_C___Reflection_PushHandler7002_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler7002 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler7002)

public:
	SYNTHESIZE(PushHandler7002, int*, m_pValue)

		PushHandler7002() ;
	virtual ~PushHandler7002() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
