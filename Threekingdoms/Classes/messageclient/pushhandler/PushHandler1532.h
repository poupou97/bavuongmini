
#ifndef Blog_C___Family_PushHandler1532_h
#define Blog_C___Family_PushHandler1532_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1532 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1532)

public:
	SYNTHESIZE(PushHandler1532, int*, m_pValue)

		PushHandler1532() ;
	virtual ~PushHandler1532() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
