
#include "PushHandler1209.h"

#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../GameView.h"
#include "../ProtocolHelper.h"
#include "../../utils/GameUtils.h"
#include "GameAudio.h"

IMPLEMENT_CLASS(PushHandler1209)

PushHandler1209::PushHandler1209() 
{
    
}
PushHandler1209::~PushHandler1209() 
{
    
}
void* PushHandler1209::createInstance()
{
    return new PushHandler1209() ;
}
void PushHandler1209::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1209::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1209::handle(CommonMessage* mb)
{
	Push1209 bean;
	bean.ParseFromString(mb->data());
	
	CCLOG("msg: %d, actor ( %lld ) revive.", mb->cmdid(), bean.roleid());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	BaseFighter* bf = dynamic_cast<BaseFighter*>(scene->getActor(bean.roleid()));
	if(bf == NULL)
		return;

	// 复活
	if (bf != NULL) {		
		GameUtils::playGameSound(REVIVAL_TRIGGER, 2, false);
		bf->changeAction(ACT_STAND);
		bf->stopAllCommand();

		ProtocolHelper::sharedProtocolHelper()->updateFighterHPMP(bf, bean.hp(), bean.mp());

		ProtocolHelper::sharedProtocolHelper()->updateFighterStatus(bean.mutable_extstatus(), bf);
	}
}
