
#ifndef Blog_C___Family_PushHandler1533_h
#define Blog_C___Family_PushHandler1533_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1533 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1533)

public:
	SYNTHESIZE(PushHandler1533, int*, m_pValue)

		PushHandler1533() ;
	virtual ~PushHandler1533() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
