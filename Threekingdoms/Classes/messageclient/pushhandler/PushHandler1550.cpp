
#include "PushHandler1550.h"
#include "../protobuf/GuildMessage.pb.h" 
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CRewardBase.h"
#include "../../ui/GameUIConstant.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Reward_ui/RewardUi.h"

IMPLEMENT_CLASS(PushHandler1550)

PushHandler1550::PushHandler1550() 
{
    
}
PushHandler1550::~PushHandler1550() 
{
    
}
void* PushHandler1550::createInstance()
{
    return new PushHandler1550() ;
}
void PushHandler1550::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1550::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1550::handle(CommonMessage* mb)
{
	Push1550 bean;
	bean.ParseFromString(mb->data());
	
	for (int i = 0;i<bean.type_size();i++)
	{
		switch(bean.type(i))
		{
		case GuildFight:
			{
				RewardUi::addRewardListEvent(REWARD_LIST_ID_FAMILYFIGHT);
			}break;
		case WorldBoss:
			{
				RewardUi::addRewardListEvent(REWARD_LIST_ID_WORLDBOSS);
			}break;
		}
	}
}
