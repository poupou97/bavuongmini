
#include "PushHandler1548.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CGuildBattleInfo.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightUI.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightRewardsUI.h"
#include "../../ui/family_ui/familyFight_ui/BattleSituationUI.h"

IMPLEMENT_CLASS(PushHandler1548)

	PushHandler1548::PushHandler1548() 
{

}
PushHandler1548::~PushHandler1548() 
{

}
void* PushHandler1548::createInstance()
{
	return new PushHandler1548() ;
}
void PushHandler1548::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1548::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1548::handle(CommonMessage* mb)
{
	Rsp1548 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	BattleSituationUI* battleSituationUI = (BattleSituationUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBattleSituationUI);
	if (battleSituationUI != NULL)
	{
		if (bean.has_blue())
		{
			battleSituationUI->blueInfo->CopyFrom(bean.blue());
		}

		if (bean.has_red())
		{
			battleSituationUI->redInfo->CopyFrom(bean.red());
		}

		battleSituationUI->RefreshOwnData();
		battleSituationUI->RefreshEmptyData();
		battleSituationUI->RefreshUI();
	}
}
