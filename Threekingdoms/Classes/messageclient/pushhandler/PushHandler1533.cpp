
#include "PushHandler1533.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CGuildFightRecord.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "../../ui/family_ui/familyFight_ui/familyFightHistoryNotesUI.h"
#include "../../ui/generals_ui/GeneralsListBase.h"



IMPLEMENT_CLASS(PushHandler1533)

	PushHandler1533::PushHandler1533() 
{

}
PushHandler1533::~PushHandler1533() 
{

}
void* PushHandler1533::createInstance()
{
	return new PushHandler1533() ;
}
void PushHandler1533::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1533::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1533::handle(CommonMessage* mb)
{
	Rsp1533 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, instance message", mb->cmdid());

	familyFightHistoryNotesUI * historyNotesUI = (familyFightHistoryNotesUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightHistoryNotesUI);
	if (!historyNotesUI)
		return;

	//delete old history
	if (historyNotesUI->isReqNewly)
	{
		std::vector<CGuildFightRecord*>::iterator iter_history;
		for (iter_history = historyNotesUI->familyFightHistoryList.begin(); iter_history != historyNotesUI->familyFightHistoryList.end(); ++iter_history)
		{
			delete *iter_history;
		}
		historyNotesUI->familyFightHistoryList.clear();
	}
	
	//add new
	for (int i = 0;i<bean.records_size();++i)
	{
		CGuildFightRecord * temp = new CGuildFightRecord();
		temp->CopyFrom(bean.records(i));
		historyNotesUI->familyFightHistoryList.push_back(temp);
	}

	historyNotesUI->RefreshGeneralsListStatus(bean.pageno(),bean.totalpages());
	historyNotesUI->refreshUI();
	historyNotesUI->setIsCanReq(true);
}
