
#include "PushHandler1407.h"

#include "../protobuf/InteractMessage.pb.h"  


IMPLEMENT_CLASS(PushHandler1407)

PushHandler1407::PushHandler1407() 
{
    
}
PushHandler1407::~PushHandler1407() 
{
    
}
void* PushHandler1407::createInstance()
{
    return new PushHandler1407() ;
}
void PushHandler1407::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1407::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1407::handle(CommonMessage* mb)
{
	Push1407 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	bean.invitetoteam();//缺人 (邀请) 0为未勾选   1为勾选
	bean.rejectteam();//0为未勾选   1为勾选（拒绝）
}
