
#include "PushHandler1142.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "../element/CEquipment.h"
#include "../../ui/extensions/QuiryUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/CActiveRole.h"
#include "../element/CGeneralDetail.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(PushHandler1142)

PushHandler1142::PushHandler1142() 
{
    
}
PushHandler1142::~PushHandler1142() 
{
    
}
void* PushHandler1142::createInstance()
{
	return new PushHandler1142() ;
}
void PushHandler1142::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1142::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1142::handle(CommonMessage* mb)
{
	Rsp1142 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (!GameView::getInstance()->getGameScene())
		return;

	CActiveRole * activeRole =new CActiveRole();
	activeRole->CopyFrom(bean.activerole());

	
	std::vector<CEquipment *>playerEquipment;
	for(int i=0;i<bean.equipments_size();i++)
	{
		CEquipment * equip_ =new CEquipment();
		equip_->CopyFrom(bean.equipments(i));
		playerEquipment.push_back(equip_);
	}

	std::vector<CGeneralDetail *> generalDetailVector;
	for(int i=0;i<bean.generals_size();i++)
	{
		CGeneralDetail * generalDetail = new CGeneralDetail();
		generalDetail->CopyFrom(bean.generals(i));
		generalDetailVector.push_back(generalDetail);
	}

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->getChildByTag(ktagQuiryUI) == NULL)
		{
			CCSize winsize =CCDirector::sharedDirector()->getVisibleSize();
			QuiryUI * quiryui =QuiryUI::create(activeRole,playerEquipment,generalDetailVector);
			quiryui->ignoreAnchorPointForPosition(false);
			quiryui->setAnchorPoint(ccp(0.5f,0.5f));
			quiryui->setPosition(ccp(winsize.width/2,winsize.height/2));
			quiryui->setTag(ktagQuiryUI);
			mainScene->addChild(quiryui);
		}
	}

	std::vector<CEquipment *>::iterator vectorIter;
	for (vectorIter =playerEquipment.begin();vectorIter!= playerEquipment.end();++vectorIter)
	{
		delete *vectorIter;
	}
	playerEquipment.clear();
	delete activeRole;
}
