
#ifndef Blog_C___PlayerMessage_PushHandler1_h
#define Blog_C___PlayerMessage_PushHandler1_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

using namespace std;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1)
public:
	SYNTHESIZE(PushHandler1, int*, m_pValue)
    
    PushHandler1() ;
    virtual ~PushHandler1() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
} ;

#endif