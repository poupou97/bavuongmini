
#ifndef Blog_C___Reflection_PushHandler5062_h
#define Blog_C___Reflection_PushHandler5062_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5062 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5062)

public:
	SYNTHESIZE(PushHandler5062, int*, m_pValue)

		PushHandler5062() ;
	virtual ~PushHandler5062() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
