
#ifndef Blog_C___Reflection_PushHandler1106_h
#define Blog_C___Reflection_PushHandler1106_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1106 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1106)
    
public:
    SYNTHESIZE(PushHandler1106, int*, m_pValue)
    
    PushHandler1106() ;
    virtual ~PushHandler1106() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
