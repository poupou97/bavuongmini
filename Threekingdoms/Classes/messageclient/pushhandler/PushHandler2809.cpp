
#include "PushHandler2809.h"
#include "../protobuf/PlayerMessage.pb.h" 
#include "../../GameView.h"
#include "../protobuf/QuestionMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/COfflineExpPuf.h"
#include "../../ui/offlineExp_ui/OffLineExpUI.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/GameUIConstant.h"
#include "../element/CRewardBase.h"
#include "../../ui/Reward_ui/RewardUi.h"


IMPLEMENT_CLASS(PushHandler2809)

	PushHandler2809::PushHandler2809() 
{

}
PushHandler2809::~PushHandler2809() 
{

}
void* PushHandler2809::createInstance()
{
	return new PushHandler2809() ;
}
void PushHandler2809::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2809::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2809::handle(CommonMessage* mb)
{
	Rsp2809 bean;
	bean.ParseFromString(mb->data());
	//1 =success 0 =failed
	if (bean.status() == 1)
	{
		for(int i = 0;i<GameView::getInstance()->m_offLineExpPuf.size();++i)
		{
			COfflineExpPuf * temp = GameView::getInstance()->m_offLineExpPuf.at(i);
			if ( i == GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel())
			{
				switch(bean.type())
				{
				case 1:
					{
						temp->m_bIsGeted = true;
					}
					break;
				case 2:
					{
						temp->m_bIsGeted = true;
						GameView::getInstance()->m_nRequiredGoldToGetExp = 0;
					}
					break;
				case 3:
					{
						temp->m_bIsGeted = true;
						GameView::getInstance()->m_nRequiredGoldInGotToGetExp = 0;
					}
					break;
				}
			}
		}

		MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		if (!mainscene)
			return;

		RewardUi::removeRewardListEvent(REWARD_LIST_ID_OFFLINEEXP);

		OffLineExpUI * offLineExpUI = (OffLineExpUI*)mainscene->getChildByTag(kTagOffLineExpUI);
		if (!offLineExpUI)
			return;

		offLineExpUI->RefreshUI();
	}
	


}
