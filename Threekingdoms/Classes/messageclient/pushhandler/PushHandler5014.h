
#ifndef Blog_C___Reflection_PushHandler5014_h
#define Blog_C___Reflection_PushHandler5014_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5014 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler5014)
    
public:
    SYNTHESIZE(PushHandler5014, int*, m_pValue)
    
    PushHandler5014() ;
    virtual ~PushHandler5014() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
