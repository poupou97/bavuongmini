#include "PushHandler1122.h"

#include "../protobuf/PlayerMessage.pb.h"  

#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../gamescene_state/role/MonsterOwnedStates.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutUIConstant.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutConfigure.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../ui/extensions/UITab.h"
#include "../../ui/skillscene/MusouSkillScene.h"
#include "../../ui/skillscene/GeneralMusouSkillListUI.h"

IMPLEMENT_CLASS(PushHandler1122)

PushHandler1122::PushHandler1122() 
{

}
PushHandler1122::~PushHandler1122() 
{

}
void* PushHandler1122::createInstance()
{
	return new PushHandler1122() ;
}
void PushHandler1122::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1122::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1122::handle(CommonMessage* mb)
{
	Rsp1122 bean;
	bean.ParseFromString(mb->data());

	CCLOG("msg: %d, shortcut", mb->cmdid());

	bool isHave = false;
	int index = bean.shortcut().index();

	//setmusouSkill null
	if (index == MUSOUSKILL_INDEX)
	{
		if (bean.shortcut().complexflag()!=1)
		{
			GameView::getInstance()->myplayer->setMusouSkill("");
			SkillScene * skillScene = (SkillScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			if (skillScene)
			{
				skillScene->ImageView_musou->setVisible(false);
			}
		}
		else
		{
			std::string _skillid = bean.shortcut().skillpropid();
			GameView::getInstance()->myplayer->setMusouSkill(_skillid.c_str());

			if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene) != NULL)
			{
				SkillScene * skillScene = (SkillScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
				CShortCut * temp = new CShortCut();
				temp->CopyFrom(bean.shortcut());
				if (skillScene->SkillTypeTab->getCurrentIndex() == 0)
				{
					//刷新人物技能界面的无双标示的位置
					skillScene->RefreshMumouPos(temp);
				}
				else if (skillScene->SkillTypeTab->getCurrentIndex() == 1)
				{
					//刷新龙魂技能界面人物技能的无双标示的位置
					skillScene->musouSkillLayer->generalMusouSkillListUI->AddMusouForRole(temp->skillpropid());
				}

				delete temp;
			}
		}
	}

	if (GameView::getInstance()->shortCutList.size() > 0)
	{
		for (int i = 0;i<GameView::getInstance()->shortCutList.size();++i)
		{
			CShortCut * shortcut = GameView::getInstance()->shortCutList.at(i);
			if (shortcut->index() == index)
			{
				isHave = true;
				shortcut->CopyFrom(bean.shortcut());
				ShortcutLayer * shortcutlayer = (ShortcutLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
				if (shortcutlayer)
				{
					shortcutlayer->RefreshOneShortcutSlot(shortcut);
				}

				if (shortcut->complexflag() != 1)
				{
					ShortcutConfigure * shortcutConfigure = (ShortcutConfigure *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutConfigure);
					if (shortcutConfigure)
					{
						shortcutConfigure->RefreshOneShortcutConfigure(shortcut);
						CCFiniteTimeAction*  action = CCSequence::create(
							CCDelayTime::create(0.5f),
							CCRemoveSelf::create(),
							NULL);
						shortcutConfigure->runAction(action);
					}
				}
			}
		}

		if (!isHave)
		{
			CShortCut * shortcut = new CShortCut();
			shortcut->CopyFrom(bean.shortcut());
			GameView::getInstance()->shortCutList.push_back(shortcut);

			ShortcutLayer * shortcutlayer = (ShortcutLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			if (shortcutlayer)
			{
				shortcutlayer->RefreshOneShortcutSlot(shortcut);
			}

			if (shortcut->complexflag() != 1)
			{
				ShortcutConfigure * shortcutConfigure = (ShortcutConfigure *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutConfigure);
				if (shortcutConfigure)
				{
					shortcutConfigure->RefreshOneShortcutConfigure(shortcut);
					CCFiniteTimeAction*  action = CCSequence::create(
						CCDelayTime::create(0.5f),
						CCRemoveSelf::create(),
						NULL);
					shortcutConfigure->runAction(action);
				}
			}
		}
	}
	else
	{
		CShortCut * shortcut = new CShortCut();
		shortcut->CopyFrom(bean.shortcut());
		GameView::getInstance()->shortCutList.push_back(shortcut);

		ShortcutLayer * shortcutlayer = (ShortcutLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortcutlayer)
		{
			shortcutlayer->RefreshOneShortcutSlot(shortcut);
		}

		if (shortcut->complexflag() != 1)
		{
			ShortcutConfigure * shortcutConfigure = (ShortcutConfigure *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutConfigure);
			if (shortcutConfigure)
			{
				shortcutConfigure->RefreshOneShortcutConfigure(shortcut);
				CCFiniteTimeAction*  action = CCSequence::create(
					CCDelayTime::create(0.5f),
					CCRemoveSelf::create(),
					NULL);
				shortcutConfigure->runAction(action);
			}
		}
		
	}

}