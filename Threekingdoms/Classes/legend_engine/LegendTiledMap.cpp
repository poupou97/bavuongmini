#include "LegendTiledMap.h"

TiledMapLayer::TiledMapLayer()
: m_imgName("")
{

}

TiledMapLayer::~TiledMapLayer()
{
	// delete array
	for(int i = 0; i < this->m_tileRow; i++)   
		CC_SAFE_DELETE_ARRAY(m_tileData[i]);  
	CC_SAFE_DELETE_ARRAY(m_tileData);
}

void TiledMapLayer::load(CCBinaryFileReader* reader)
{
	CCString * name = reader->readString();
	m_imgName = name->getCString();
	m_imgName = m_resPath + m_imgName;

	this->m_tileRow = reader->readShort();
	this->m_tileCol = reader->readShort();
	//this->m_tileHeight = reader->readByte();
	//this->m_tileWidth = reader->readByte();
	this->m_tileHeight = reader->readShort();
	this->m_tileWidth = reader->readShort();
	CCAssert(m_tileHeight > 0 && m_tileWidth > 0, "tile should not less than 0");
	int srcImgHeight = reader->readShort();   //图片原来宽度
	int srcImgWidth = reader->readShort();   //图片原来高度
	m_imgTileCol = (short)(srcImgWidth / this->m_tileWidth);
	m_imgTileRow = (short)(srcImgHeight / this->m_tileHeight);
	
	m_tileData = new short*[this->m_tileRow];
	
	for(int r = 0; r < this->m_tileRow; r++)
	{
		m_tileData[r] = new short[this->m_tileCol];
		for(int c = 0; c < this->m_tileCol; c++)
		{
			m_tileData[r][c] = reader->readShort();
		}
	}
}

std::string TiledMapLayer::getImgNameByTileID(int id)
{
	std::string file = "";

	// image file name format, xxx_01.jpg, xxx_02.jpg, etc.

	std::string imageFullName = getImgName();
	int lastSlashIndex = imageFullName.find_last_of(".");
	std::string imageName = imageFullName.substr(0, lastSlashIndex);
	file.append(imageName);

	file.append("_");

	// number, xxx_01.jpg, xxx_02.jpg, etc.
	char num[4];
	int tmpID = id + 1;   // start from 01
	if(tmpID < 10)
		sprintf(num, "0%d", tmpID);
	else
		sprintf(num, "%d", tmpID);
	std::string number = num;
	file.append(num);

	// 后缀
	std::string suffix = imageFullName.substr(lastSlashIndex);
	file.append(suffix);

	return file;
}

int TiledMapLayer::getWidth()
{
	return this->m_tileWidth * m_tileCol;
}
int TiledMapLayer::getHeight()
{
	return this->m_tileHeight * m_tileRow;
}

TiledMap::TiledMap()
: m_nLayerCnt(0)
, m_pLayers(NULL)
{

}

TiledMap::~TiledMap()
{
	for(int i = 0; i < m_nLayerCnt; i++)   
		CC_SAFE_DELETE(m_pLayers[i]);  
	CC_SAFE_DELETE_ARRAY(m_pLayers);
}

void TiledMap::loadTiledMapData(const char* tiledmapFile)
{
	CCBinaryFileReader* reader = new CCBinaryFileReader(tiledmapFile);

	// 获取文件的路径（不带文件名）
	std::string path = tiledmapFile;
	int lastSlashIndex = path.find_last_of("/\\");
	if(lastSlashIndex == -1) {   // not found
		m_resPath = "";
	}
	else {
		m_resPath = path.substr(0, lastSlashIndex+1);
	}
	
	// load layers info
	m_nLayerCnt = reader->readByte();
	m_pLayers = new TiledMapLayer*[m_nLayerCnt];
	for(int i = 0; i < m_nLayerCnt; i++)
	{
		m_pLayers[i] = new TiledMapLayer();
		m_pLayers[i]->setResPath(m_resPath);
		m_pLayers[i]->load(reader);
	}

	CC_SAFE_DELETE(reader);

	// 默认情况下，map的所有layer的尺寸是一样的
	if(m_pLayers[0] != NULL)
	{
		this->m_mapSize.width = m_pLayers[0]->getWidth();
		this->m_mapSize.height = m_pLayers[0]->getHeight();
	}
}

std::vector<std::string>* TiledMap::generateResourceList()
{
	std::vector<std::string>* list = new std::vector<std::string>();

	int layerCnt = getLayerCnt();
	for(int layerIdx = 0; layerIdx < layerCnt; layerIdx++)
	{
		if(layerIdx != 0)   // 目前，只有第0层是用于场景绘制的
			break;

		TiledMapLayer* layer = m_pLayers[layerIdx];
		if(false/*m_bAtlasEnabled*/)
		{
			//layer->getImgName()
		}
		else
		{
			int totalRow = layer->getTileRow();
			int totalCol = layer->getTileCol();
			for (int i = 0; i < totalRow; i++) {
				for (int j = 0; j < totalCol; j++) {
					short** tileData = layer->getTileData();
					// if -1, then there is no tile, so ignore
					int id = tileData[i][j];
					id = id & 0x1FFF;
					if(id >= 0)
					{
						list->push_back(layer->getImgNameByTileID(id));
					}
				}
			}
		}
	}

	return list;
}