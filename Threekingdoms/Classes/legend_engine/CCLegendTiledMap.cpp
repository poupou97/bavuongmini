#include "CCLegendTiledMap.h"

#include "CCLegendAnimation.h"


enum {
    kTagTiledmapBatchNode = 1,
};

CCLegendTiledMapLayer::CCLegendTiledMapLayer()
: m_bAtlasEnabled(true)
, mTotalRow(0)
, mTotalCol(0)
, m_TileSprites(NULL)
{
	scheduleUpdate();
}

CCLegendTiledMapLayer::~CCLegendTiledMapLayer()
{
	for (int i = 0; i < mTotalRow; i++) {
		for (int j = 0; j < mTotalCol; j++) {
			CCSprite* tile = m_TileSprites[i][j];
			if(tile != NULL)
				tile->release();
		}
	}

	for (int i = 0; i < mTotalRow; i++) {
		CC_SAFE_DELETE_ARRAY(m_TileSprites[i]);
	}
	CC_SAFE_DELETE_ARRAY(m_TileSprites);
}

CCSprite* CCLegendTiledMapLayer::newTile(int r, int c)
{
	//CCSprite *sprite = CCSprite::create();
	CCSprite *sprite = new CCSprite();

	TiledMapLayer* layer = m_tiledmapLayerData;
	short** tileData = layer->getTileData();

	// if -1, then there is no tile, so ignore
	int id = tileData[r][c];
	if(id < 0)
	{
		return NULL;
	}
	
	int tc = layer->getImgTileCol();
	bool bflipX = ( (tileData[r][c] & 0x2000) != 0);
	bool bflipY = ( (tileData[r][c] & 0x4000) != 0);
	if(bflipX && bflipY)
	{
		sprite->setScaleX(-1.f);
		sprite->setScaleY(-1.f);
	}
	else if(bflipX)
	{
		sprite->setScaleX(-1.f);
	}
	else if(bflipY)
	{
		sprite->setScaleY(-1.f);
	}
	
	id = id & 0x1FFF;
	if(id < 0)   // invalid id
		return NULL;

	int tileX, tileY;
	//if(!USE_OVERLAP_METHOD) {
	//	tileX = id % tc * m_tileWidth;
	//	tileY = id / tc * m_tileHeight;
	//}
	//else {
	//	tileX = id % tc * (m_tileWidth+2)+1;
	//	tileY = id / tc * (m_tileHeight+2)+1;
	//}
	tileX = id % tc * layer->getTileWidth();
	tileY = id / tc * layer->getTileHeight();

	//tileX = 0;
	//tileY = 704-32;

	//sprite->initWithTexture(m_pSpriteTexture, CCRectMake(tileX, tileY, layer->getTileWidth(), layer->getTileHeight()));
	if(m_bAtlasEnabled)
	{
		sprite->initWithTexture(m_rootAtlasNode->getTexture(), CCRectMake(tileX, tileY, layer->getTileWidth(), layer->getTileHeight()));
	}
	else
	{
		sprite->initWithFile(layer->getImgNameByTileID(id).c_str());
	}

	return sprite;
}

bool CCLegendTiledMapLayer::initWithLayerData(TiledMapLayer* layerData)
{
	m_tiledmapLayerData = layerData;

	TiledMapLayer* layer = m_tiledmapLayerData;

	mTotalRow = layer->getTileRow();
	mTotalCol = layer->getTileCol();
	mTileWidth = layer->getTileWidth();
	mTileHeight = layer->getTileHeight();

	// create texture
	//if(m_bAtlasEnabled)
	//{
	//	if(getTextureAtlas() == NULL)
	//	{
	//		std::string file = "";
	//		file.append(this->m_map->getResPath());
	//		file.append(layer->getImgName());
	//		this->initWithFile(file.c_str(), 400);
	//	}
	//}
	if(m_bAtlasEnabled)
	{
		//if(getTextureAtlas() == NULL)
		{
			std::string file = "";
			file.append(this->m_map->getTiledMapData()->getResPath());
			file.append(layer->getImgName());

			m_rootAtlasNode = CCSpriteBatchNode::create(file.c_str(), 400);
			addChild(m_rootAtlasNode);
		}
	}
	else
	{
		m_rootNode = CCNode::create();
		addChild(m_rootNode);
	}
	//this->initWithFile("background/xinshoucun.png", 22500);
	//this->setTag(kTagTiledmapBatchNode);

	//m_pSpriteTexture = getTexture();

    //CCSpriteBatchNode *parent = CCSpriteBatchNode::create("background/xinshoucun.png", 400);
    //m_pSpriteTexture = parent->getTexture();
    //addChild(parent, 0, kTagTiledmapBatchNode);
	//parent->setAnchorPoint( CCPointMake(0, 0));
	//parent->setPosition(CCPointMake(0, 500));

	//int tileWidth = 64;
	//int tileHeight = 32;
	//for(int y = 0; y < 22; y++)
	//{
	//	for(int x = 0; x < 11; x++)
	//	{
	//		CCSprite *sprite = new CCSprite();
	//		sprite->initWithTexture(m_pSpriteTexture, CCRectMake(0+x*tileWidth,0+y*tileHeight,tileWidth,tileHeight));
	//		sprite->autorelease();
	//		//sprite->setAnchorPoint(CCPointMake(0, 0));

	//		parent->addChild(sprite);

	//		sprite->setPosition( CCPointMake( x*tileWidth, (22-1-y)*tileHeight) );
	//	}
	//}


	m_layerSize.setSize(mTotalCol * mTileWidth, mTotalRow * mTileHeight);

	m_TileSprites = new CCSprite**[mTotalRow];
	for (int i = 0; i < mTotalRow; i++) {
		m_TileSprites[i] = new CCSprite*[mTotalCol];
		for (int j = 0; j < mTotalCol; j++) {
			int tx = j * mTileWidth;
			int ty = i * mTileHeight;

			CCSprite* tile = newTile(i, j);
			m_TileSprites[i][j] = NULL;
			if(tile != NULL) {
				//tile->setAnchorPoint(CCPointMake(0, 0));
				tile->setPosition( ccp(tx+(mTileWidth>>1), m_layerSize.height - ty - (mTileHeight>>1)) );
				//tile->retain();   // 保存Tile实例，并由自己来管理释放。
				//                  // 因为这些结点可能会被add, remove多次，需要持久保留一份可用的实例。

				if(m_map->isStatic())
				{
					if(m_bAtlasEnabled)
						m_rootAtlasNode->addChild(tile);
					else
						m_rootNode->addChild(tile);

					tile->autorelease();
				}
				else
				{
					m_TileSprites[i][j] = tile;
				}
			}
		}
	}
	//CCLOG("child number: %d", this->getChildrenCount());
	//CCLOG("TiledMap content size: %d, %d", this->getContentSize().width, this->getContentSize().height);

	return true;
}

void CCLegendTiledMapLayer::update(float dt)
{
	if(m_map->isStatic())
		return;

	// first, remove all tiles which have been drawn
	//removeAllChildrenWithCleanup(false);
	if(m_bAtlasEnabled)
		m_rootAtlasNode->removeAllChildrenWithCleanup(true);
	else
		m_rootNode->removeAllChildrenWithCleanup(true);

	//
	// then, add some new tiles to be drawn
	//

	// 将屏幕视口的左上角坐标，由屏幕坐标系，转换到TiledMap的坐标系下
	// 注: cocos2d屏幕坐标系，是以屏幕左下角为原点，向右向上为正
	//     TiledMap的坐标系，以地图的左上角为原点，向右向下为正

	// TiledMap的左下角，在屏幕坐标系下的坐标值
	CCNode* parent = this->getParent()->getParent();
	CCPoint mapScreenPos;
	if(parent == NULL)
		mapScreenPos = this->getParent()->getPosition();
	else
		mapScreenPos = parent->getPosition();

	// 求出TiledMap左上角坐标，在屏幕坐标系下的坐标值
	CCPoint mapLeftTopScreenPos;
	mapLeftTopScreenPos.x = mapScreenPos.x;
	mapLeftTopScreenPos.y = mapScreenPos.y + m_layerSize.height;

	// 屏幕的左上角，在屏幕坐标系下的坐标值
	CCSize screenSize=CCDirector::sharedDirector()->getVisibleSize();
	CCPoint screenPos = ccp(0, screenSize.height);

	// 坐标系转换，求出屏幕左上角在Tiledmap坐标下的坐标值
	CCPoint srcPos;
	srcPos.x = screenPos.x - mapLeftTopScreenPos.x;
	srcPos.y = mapLeftTopScreenPos.y - screenPos.y;

	//
	// 动态添加Tile结点
	//
	int width = screenSize.width, height = screenSize.height;
	int x_src = srcPos.x, y_src = srcPos.y;

	TiledMapLayer* layer = m_tiledmapLayerData;
	int tileWidth = layer->getTileWidth();
	int tileHeight = layer->getTileHeight();
	int tileCol = layer->getTileCol();
	int tileRow = layer->getTileRow();

	int leftTileIdx = x_src / tileWidth - 1;   // -1, 用于扩大一圈Tile，使之画满屏幕
	if(leftTileIdx < 0)
		leftTileIdx = 0;
	int topTileIdx =  y_src / tileHeight - 1;
	if(topTileIdx < 0)
		topTileIdx = 0;
	int t = (x_src + width) / tileWidth;
	int rightTileIdx = (x_src + width) % tileWidth != 0 ? t + 1 : t;
	rightTileIdx += 1;
	if(rightTileIdx > tileCol)
	{
		rightTileIdx = tileCol;
	}
	t = (y_src + height) / tileHeight;
	int bottomTileIdx = (y_src + height) % tileHeight != 0 ? t + 1: t;
	bottomTileIdx += 1;
	if(bottomTileIdx > tileRow)
	{
		bottomTileIdx = tileRow;
	}

	for(int r = topTileIdx; r < bottomTileIdx; r++)
	{
		for(int c = leftTileIdx; c < rightTileIdx; c++)
		{
			CCSprite* tile = m_TileSprites[r][c];
			if(tile != NULL) {
				if(m_bAtlasEnabled)
					m_rootAtlasNode->addChild(tile);
				else
					m_rootNode->addChild(tile);
			}
		}
	}
}

CCLegendTiledMap::CCLegendTiledMap(void)
: m_collisionLayerId(TILEDMAP_COLLISIONLAYER_ID)
, m_pTiledMapData(NULL)
, m_pLayers(NULL)
, m_bStatic(false)
{
}

CCLegendTiledMap::~CCLegendTiledMap()
{
	CC_SAFE_DELETE(m_pTiledMapData);

	CC_SAFE_DELETE_ARRAY(m_pLayers);
}

void CCLegendTiledMap::onEnter()
{
	CCNode::onEnter();

	// refresh the tiled map
	// to avoid the black screen when first enter the game
	update(0.1f);
}

void CCLegendTiledMap::update(float dt)
{
	int layerCnt = m_pTiledMapData->getLayerCnt();
	for(int layerIdx = 0; layerIdx < layerCnt; layerIdx++)
	{
		if(layerIdx == 1)
			break;
		if(layerIdx == m_collisionLayerId)
			break;

		m_pLayers[layerIdx]->update(dt);
	}
}

bool CCLegendTiledMap::initWithTiledmapFilename(const char* filename)
{
	m_pTiledMapData = new TiledMap();
	m_pTiledMapData->loadTiledMapData(filename);

	int layerCnt = m_pTiledMapData->getLayerCnt();
	m_pLayers = new CCLegendTiledMapLayer*[layerCnt];

	for(int layerIdx = 0; layerIdx < layerCnt; layerIdx++)
	{
		if(layerIdx == 1)
			break;
		if(layerIdx == m_collisionLayerId)
			break;

		//if(layerIdx == 0)
		//	continue;
		//if(layerIdx == 1)
		//	continue;

		TiledMapLayer* layerData = m_pTiledMapData->getLayers()[layerIdx];
		m_pLayers[layerIdx] = new CCLegendTiledMapLayer();
		m_pLayers[layerIdx]->setMap(this);
		if(layerIdx == 0)
			m_pLayers[layerIdx]->setAtlasEnabled(false);
		m_pLayers[layerIdx]->initWithLayerData(layerData);
		//m_pLayers[layerIdx]->retain();

		addChild(m_pLayers[layerIdx]);
		m_pLayers[layerIdx]->release();
	}

	return true;
}