
#ifndef __LEGENDANIMATION_CACHE_H__
#define __LEGENDANIMATION_CACHE_H__

#include <map>
#include <string>
#include <vector>

class AAnimation;
class CCLegendAnimation;

/** @brief Singleton that handles the loading of AAnimation
* Once the AAnimation is loaded, the next time it will return
* the previously loaded AAnimation
*/
class LegendAAnimationCache
{
protected:
    std::map<std::string, AAnimation*> m_pAAnimations;

public:
    LegendAAnimationCache();
    virtual ~LegendAAnimationCache();
 

    /** Returns the shared instance of the cache 
     */
    static LegendAAnimationCache * sharedLegendAnimationCache();

    /** purges the cache. It releases the retained instance.
    */
    static void purgeSharedAnimationCache();

    /** Returns a AAnimation object given an file name
    * If the file image was not previously loaded, it will create a new AAnimation
    *  object and it will return it. It will use the filename as a key.
    * Otherwise it will return a reference of a previously loaded animation.
    * Supported animation extensions: .anm
    */
    AAnimation* addAAnimation(const char* path);

    void removeAllAnimations();
};

////////////////////////////////////////////////////////

class CCLegendAnimationCache
{
protected:
    std::map<std::string, std::vector<CCLegendAnimation*> > m_CCLegendAnimationPool;

public:
    CCLegendAnimationCache();
    virtual ~CCLegendAnimationCache();
 

    /** Returns the shared instance of the cache 
     */
    static CCLegendAnimationCache * sharedCache();

    /** purges the cache. It releases the retained instance.
    */
    static void purgeSharedCache();

    /** 
    * Supported animation extensions: .anm
    */
    CCLegendAnimation* addCCLegendAnimation(const char* path);

    void removeAllCache();
};

#endif //__LEGENDANIMATION_CACHE_H__

