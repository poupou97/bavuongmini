
#ifndef __LEGEND_TILDEDMAP_TILEDMAP_H__
#define __LEGEND_TILDEDMAP_TILEDMAP_H__

#include "cocos2d.h"
#include "LegendTiledMap.h"

#define TILEDMAP_COLLISIONLAYER_ID 2

USING_NS_CC;
using namespace std;

/**
 * cocos2d-x的奇怪问题，当CCSpriteBatchNode使用addChild增加CCSprite到16500个后，再增加的CCSprite就绘制不出来了。
 * 还未找到原因
*/
class CCLegendTiledMap;
class CCLegendTiledMapLayer : public CCNode
{
public:
	CCLegendTiledMapLayer();
    ~CCLegendTiledMapLayer();

	virtual void update(float dt);

	bool initWithLayerData(TiledMapLayer* layerData);

	//CCTexture2D* m_pSpriteTexture;

	// layer's container: TiledMap
	CC_SYNTHESIZE(CCLegendTiledMap*, m_map, Map);

	inline void setAtlasEnabled(bool bEnabled) { m_bAtlasEnabled = bEnabled; };

private:
	TiledMapLayer* m_tiledmapLayerData;

	CCSprite* newTile(int r, int c);

	CCSprite*** m_TileSprites;

	CCSize m_layerSize;

	CCNode* m_rootNode;
	CCSpriteBatchNode* m_rootAtlasNode;

	bool m_bAtlasEnabled;

	int mTotalRow;
	int mTotalCol;
	int mTileWidth;
	int mTileHeight;
};

class CCLegendTiledMap : public CCNode
{
public:
	CCLegendTiledMap();
    ~CCLegendTiledMap();

	virtual void update(float dt);

	virtual void onEnter();

	bool initWithTiledmapFilename(const char* filename);

	CC_SYNTHESIZE_READONLY(TiledMap*, m_pTiledMapData, TiledMapData)

	// 碰撞层id，碰撞层不用于绘制
	CC_SYNTHESIZE(int, m_collisionLayerId, CollisionLayerId)

	CC_SYNTHESIZE_READONLY(CCLegendTiledMapLayer**, m_pLayers, Layers)

	// if the tiled map is static, its all tiles will be inited once, and will never be removed and added dynamically
	inline void setStatic(bool bValue) { m_bStatic = bValue; };
	inline bool isStatic() { return m_bStatic; };

private:
	bool m_bStatic;
};

#endif
