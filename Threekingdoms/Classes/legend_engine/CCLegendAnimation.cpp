#include "CCLegendAnimation.h"

#include "LegendAAnimation.h"
#include "LegendAnimationCache.h"
#include "../utils/GameUtils.h"

#define LEGEND_ANIMATION_DEFAULT_FRAME_DEALY 25   // the same as the legend animation editor

#define LEGEND_ANIMATION_USE_AANIMATION_CACHE 1
#define LEGEND_ANIMATION_USE_AMODULE_CACHE 1
#define LEGEND_ANIMATION_SUPPORT_SPECIALAMODULE 1

CCLegendAModule::CCLegendAModule()
{
	setCascadeColorEnabled(true);
	m_sTransform = CCAffineTransformMakeIdentity();
}

CCLegendAModule::~CCLegendAModule()
{

}

// If you return NO, then nodeToParentTransform won't be called.
bool CCLegendAModule::isDirty(void)
{
    return true;
}

CCAffineTransform CCLegendAModule::nodeToParentTransform(void)
{
    return m_sTransform;
}

void CCLegendAModule::reset()
{
	this->setScale(1.0f);
	this->setFlipY(true);
	this->m_sTransform = CCAffineTransformMakeIdentity();
}

void CCLegendAModule::setTransform(float a, float b, float c, float d, float tx, float ty)
{
	// it's very important! 
	// Legend Editor和cocos2d-x所采用的坐标系是不同的
	// Legend Editor采用的坐标系y轴向下为正方向, cocos2d-x采用的坐标系y轴向上为正方向
	// 因此, 需要在绘制前将sprite在y方向上进行翻转
	CCAffineTransform t = CCAffineTransformMake(1, 0, 0, -1, 0, 0);

	m_sTransform = CCAffineTransformMake( a, b, c, d, tx, ty);

	m_sTransform = CCAffineTransformConcat(m_sTransform, t);
}

void CCLegendAModule::setTransform(CCAffineTransform trans)
{
	float a = trans.a;
	float b = trans.b;
	float c = trans.c;
	float d = trans.d;
	float tx = trans.tx;
	float ty = trans.ty;

	setTransform(a, b, c, d, tx, ty);
}

void CCLegendAModule::setModuleRect(int x, int y, int w, int h)
{
	modX = x;
	modY = y;
	modW = w;
	modH = h;
}

void CCLegendAModule::setImageName(std::string& fileName)
{
	m_sFileName = fileName;
}

CCLegendAModule* CCLegendAModule::clone()
{
	CCLegendAModule* pAModule = new CCLegendAModule();

	pAModule->imgIdx = imgIdx;
	pAModule->modIdx = modIdx;

	pAModule->initWithFile(m_sFileName.c_str(), CCRectMake(modX, modY, modW, modH));

	pAModule->setModuleRect(modX, modY, modW, modH);
	pAModule->setImageName(m_sFileName);

	pAModule->setFlipY(this->isFlipY());

	pAModule->setOpacity(this->getOpacity());

	return pAModule;
}

/////////////////////////////////////////////////////////////////////////////

CCLegendAnimation::CCLegendAnimation(void)
: m_delayCounter(0)
, m_aframeIdx(0)
, m_pAnimationData(NULL)
, m_bPlayLoop(true)
, m_bIsPlaying(true)
, m_actIdx(0)
, m_timeElapse(0)
, m_animTick(0)
, m_nPlaySpeed(1.0f)
, m_bReleaseWhenStop(false)
{
	setCascadeOpacityEnabled(true);   // opacity可作用于子结点
	setCascadeColorEnabled(true);
}

CCLegendAnimation::~CCLegendAnimation()
{
	if(m_pAnimationData == NULL)
		return;

#if LEGEND_ANIMATION_USE_AMODULE_CACHE
	// clear cache
	for (std::map<int, std::vector<CCLegendAModule*> >::iterator it = m_CCLegendAModulePool.begin(); it != m_CCLegendAModulePool.end(); ++it) 
	{
		std::vector<CCLegendAModule*>& amoduleVector = it->second;
		for(unsigned int i = 0; i < amoduleVector.size(); i++)
		{
			CCLegendAModule* tmp = amoduleVector.at(i);
			CC_SAFE_RELEASE(tmp);
		}
		amoduleVector.clear();
	}
	m_CCLegendAModulePool.clear();
#else
	// it's so complex...
	for(int i = 0; i < m_pAnimationData->getImgNum(); i++)
	{
		int moduleCnt = m_pAnimationData->getModuleNum(i);
		for(int j = 0; j < moduleCnt; j++)
		{
			CC_SAFE_RELEASE(m_pAllAModules[i][j]);   // pay attention: use CC_SAFE_RELEASE here, not CC_SAFE_DELETE
		}
		CC_SAFE_DELETE_ARRAY(m_pAllAModules[i]);
	}
	CC_SAFE_DELETE_ARRAY(m_pAllAModules);
#endif

	CC_SAFE_RELEASE(m_pAnimationData);
}

CCLegendAnimation* CCLegendAnimation::create(std::string animFile, int actionIdx)
{
	if(!CCFileUtils::sharedFileUtils()->isFileExist(animFile))
		return NULL;

	//CCLegendAnimation* pAnim = new CCLegendAnimation();
	//pAnim->initWithAnmFilename(animFile.c_str());
	CCLegendAnimation* pAnim = CCLegendAnimationCache::sharedCache()->addCCLegendAnimation(animFile.c_str());
	pAnim->retain();

	pAnim->setVisible(true);
	pAnim->setPlayLoop(false);
	pAnim->setReleaseWhenStop(true);   // only play once, then release
	pAnim->setAction(actionIdx);
	pAnim->play();

	pAnim->autorelease();

	return pAnim;
}

void CCLegendAnimation::reset()
{
	// reset animation
	this->setScale(1.0f);
	this->setRotation(0.f);
	this->setSkewX(0);
	this->setSkewY(0);
	this->setOpacity(255);
	this->setColor(ccc3(255, 255, 255));

#if LEGEND_ANIMATION_USE_AMODULE_CACHE
	this->removeAllChildren();

#if LEGEND_ANIMATION_SUPPORT_SPECIALAMODULE
	m_specialAModuleMap.clear();
#endif

#endif
}

std::string CCLegendAnimation::getImageFileName(unsigned int imgIdx)
{
	CCAssert(imgIdx < m_imageFileNames.size(), "out of range");
	return m_imageFileNames.at(imgIdx);
}
void CCLegendAnimation::setImageFileName(unsigned int imgIdx, std::string& newImageFileName)
{
	CCAssert(imgIdx < m_imageFileNames.size(), "out of range");
	m_imageFileNames[imgIdx] = newImageFileName;
}

CCLegendAModule* CCLegendAnimation::createAModule(int imgIdx, int amodIdx)
{
	AAnimation* animData = this->getAnimationData();
	short** moduleData = animData->getModuleData();

	int imgNum = animData->getImgNum();
	int moduleCnt = animData->getModuleNum(imgIdx);
	//CCAssert(imgIdx >= 0 && imgIdx < imgNum && amodIdx >= 0 && amodIdx < moduleCnt, "out of range");
	if(imgIdx < 0 || imgIdx >= imgNum || amodIdx < 0 || amodIdx >= moduleCnt)
		return NULL;

	CCLegendAModule* pTmpAModule = new CCLegendAModule();

	pTmpAModule->imgIdx = imgIdx;
	pTmpAModule->modIdx = amodIdx;

	// module rect
	int modX = moduleData[imgIdx][(amodIdx<<2) + 0];
	int modY = moduleData[imgIdx][(amodIdx<<2) + 1];
	int modW = moduleData[imgIdx][(amodIdx<<2) + 2];
	int modH = moduleData[imgIdx][(amodIdx<<2) + 3];
	
	//pTmpAModule->initWithFile(animData->getImageFileName(imgIdx).c_str(), CCRectMake(modX, modY, modW, modH));
	pTmpAModule->initWithFile(m_imageFileNames.at(imgIdx).c_str(), CCRectMake(modX, modY, modW, modH));
	//CCTexture2D* texture = CCTextureCache::sharedTextureCache()->addImage(animData->getImageFileName(i).c_str());
	//pTmpAModule->initWithTexture(texture, CCRectMake(modX, modY, modW, modH));

	pTmpAModule->setModuleRect(modX, modY, modW, modH);
    std::string imgeFileName = animData->getImageFileName(imgIdx);
	pTmpAModule->setImageName(imgeFileName);

	// it's very important! 
	// Legend Editor和cocos2d-x所采用的坐标系是不同的
	// Legend Editor采用的坐标系y轴向下为正方向, cocos2d-x采用的坐标系y轴向上为正方向
	// 因此, 需要将sprite的纹理在y方向上进行翻转
	pTmpAModule->setFlipY(true);

	return pTmpAModule;
}

int CCLegendAnimation::getKey(int imgIdx, int modIdx)
{
	int key = imgIdx; 
	return key << 16 | modIdx;
}

 CCLegendAModule* CCLegendAnimation::addCCLegendAModule(int imgIdx, int modIdx)
 {
    CCAssert(imgIdx >= 0 && modIdx >= 0, "wrong parameter");

	CCLegendAModule* pAModule = NULL;
	int key = getKey(imgIdx, modIdx);
	std::vector<CCLegendAModule*>& amoduleVector = m_CCLegendAModulePool[key];
	for(unsigned int i = 0; i < amoduleVector.size(); i++)
	{
		CCLegendAModule* tmp = amoduleVector.at(i);
		// 若引用计数为1，则为可用
		//if(tmp->isSingleReference())
		if(tmp->retainCount() == 1)
		{
			pAModule = tmp;
			pAModule->reset();   // reset amodule
			break;
		}
	}

    if (! pAModule) 
    {
		pAModule = this->createAModule(imgIdx, modIdx);
		if(pAModule == NULL)
			return NULL;

		amoduleVector.push_back(pAModule);
    }

    return pAModule;
 }

 /**
   * SpecialAModule只被addChild1次，之后就不再被移除和添加
   * 由于 SpecialAModule 不被移除，那么其RunAction就比较自然，可以随意应用cocos中所有的CCAction
   */
 CCLegendAModule* CCLegendAnimation::addSpecialAModule(int imgIdx, int modIdx)
 {
	// check if invalid
	if(imgIdx >= this->getAnimationData()->getImgNum())
		return NULL;
	if(modIdx >= this->getAnimationData()->getModuleNum(imgIdx))
		return NULL;

	int key = getKey(imgIdx, modIdx);
	CCLegendAModule* pSpecialNode = m_specialAModuleMap[key];
	if(pSpecialNode != NULL)
	{
		return pSpecialNode;
	}

	pSpecialNode = new CCLegendAModule();
	pSpecialNode->initWithTexture(NULL, CCRectZero);   // it's a container, no texture
	pSpecialNode->imgIdx = -1;
	pSpecialNode->modIdx = -1;
	pSpecialNode->setCascadeOpacityEnabled(true);
	pSpecialNode->setCascadeColorEnabled(true);
	CCLegendAModule* pAModule = this->addCCLegendAModule(imgIdx, modIdx);
	if(pAModule != NULL)
		pSpecialNode->addChild(pAModule, CCLEGENDANIMATION_SPECIAL_AMODULE_BASE_ZORDER);
	this->addChild(pSpecialNode);
	pSpecialNode->release();

	m_specialAModuleMap[key] = pSpecialNode;

	return pSpecialNode;
 }

bool CCLegendAnimation::initWithAnmFilename(const char* filename)
{
#if LEGEND_ANIMATION_USE_AANIMATION_CACHE
	m_pAnimationData = LegendAAnimationCache::sharedLegendAnimationCache()->addAAnimation(filename);
	m_pAnimationData->retain();
#else
	m_pAnimationData = new AAnimation();
	m_pAnimationData->loadAnimationData(filename);
#endif

	AAnimation* animData = this->getAnimationData();
	int imgNum = animData->getImgNum();
	// update the image file name
	for(int i = 0; i < imgNum; i++)
	{
		m_imageFileNames.push_back(animData->getImageFileName(i));
	}

#if LEGEND_ANIMATION_USE_AMODULE_CACHE
	short** moduleData = animData->getModuleData();

	// cache all CCLegendAModule
	for(int i = 0; i < imgNum; i++)
	{
		int moduleCnt = animData->getModuleNum(i);
		for(int j = 0; j < moduleCnt; j++)
		{
			this->addCCLegendAModule(i, j);
		}
	}
#else
	short** moduleData = animData->getModuleData();

	m_pAllAModules = new CCLegendAModule**[imgNum];
	for(int i = 0; i < imgNum; i++)
	{
		int moduleCnt = animData->getModuleNum(i);
		m_pAllAModules[i] = new CCLegendAModule*[moduleCnt];
		for(int j = 0; j < moduleCnt; j++)
		{
			CCLegendAModule* pTmpAModule = new CCLegendAModule();
			pTmpAModule->imgIdx = i;
			pTmpAModule->modIdx = j;

			// module rect
			int modX = moduleData[i][(j<<2) + 0];
			int modY = moduleData[i][(j<<2) + 1];
			int modW = moduleData[i][(j<<2) + 2];
			int modH = moduleData[i][(j<<2) + 3];
			
			//pTmpAModule->initWithFile(animData->getImageFileName(i).c_str(), CCRectMake(modX, modY, modW, modH));
			pTmpAModule->initWithFile(m_imageFileNames.at(i).c_str(), CCRectMake(modX, modY, modW, modH));
			//CCTexture2D* texture = CCTextureCache::sharedTextureCache()->addImage(animData->getImageFileName(i).c_str());
			//pTmpAModule->initWithTexture(texture, CCRectMake(modX, modY, modW, modH));

			pTmpAModule->setModuleRect(modX, modY, modW, modH);
			pTmpAModule->setImageName(animData->getImageFileName(i));

			// it's very important! 
			// Legend Editor和cocos2d-x所采用的坐标系是不同的
			// Legend Editor采用的坐标系y轴向下为正方向, cocos2d-x采用的坐标系y轴向上为正方向
			// 因此, 需要将sprite的纹理在y方向上进行翻转
			pTmpAModule->setFlipY(true);

			m_pAllAModules[i][j] = pTmpAModule;
		}
	}

	//// add all CCLegendAModuleNode(s) to CCLegendAnimationNode
	//for(int i = 0; i < imgNum; i++)
	//{
	//	int moduleCnt = animData->getModuleNum(i);
	//	for(int j = 0; j < moduleCnt; j++)
	//	{
	//		CCLegendAModule* amodule = m_pAllAModules[i][j];
	//		amodule->setVisible(false);
	//		addChild(amodule);
	//	}
	//}

	// init all actions
	for(int i = 0; i < m_pAnimationData->getAActionNum(); i++)
	{
		AAction* curAct = m_pAnimationData->getActions()[i];
		CCNodeRGBA* actionNode = CCNodeRGBA::create();
		actionNode->setCascadeOpacityEnabled(true);   // opacity可作用于子结点
		actionNode->setCascadeColorEnabled(true);
		actionNode->setTag(i);
		addChild(actionNode);

		for(int j = 0; j < curAct->aframeNum; j++)
		{
			// display each CCLegendAModule
			AFrame* curAFrame = curAct->aFrames[j];
			CCNodeRGBA* aframeNode = CCNodeRGBA::create();
			aframeNode->setCascadeOpacityEnabled(true);   // opacity可作用于子结点
			aframeNode->setCascadeColorEnabled(true);
			aframeNode->setTag(j);
			actionNode->addChild(aframeNode);

			int aModuleNum = curAFrame->amoduleNum;
			for(int m = 0; m < aModuleNum; m++)
			{
				AModule* curAMod = curAFrame->aModules[m];

				int imgId = curAMod->imageID;
				int moduleId = curAMod->moduleID;
				CCLegendAModule* legendAModule = m_pAllAModules[imgId][moduleId];
				CCLegendAModule* aModule = legendAModule->clone();
				aModule->setTag(m);
				aModule->setVisible(true);
				aModule->setTransform(curAMod->affineTrans);
				//aModule->setOpacity(curAMod->colorValue);

				// alpha value
				int alpha = (curAMod->colorValue >> 24) & 0xFF;
				aModule->setOpacity(alpha);

				// rgb color
				aModule->setColor(GameUtils::convertToColor3B(curAmod->colorValue));

				aframeNode->addChild(aModule);
				aModule->release();
			}
		}
	}
#endif

	return true;
}

void CCLegendAnimation::draw()
{
	// draw the aframe
	// drawAFrame() is moved from CCLegendAnimation::update(folat dt), to resolve the "ghost frame" problem
	// the "ghost frmae" problem is wrong frame is painted when call CCLegendAnimation::setAction()
	drawAFrame(m_actIdx, m_aframeIdx);
}

//void CCLegendAnimation::draw()
//{
//	//m_sprite->draw();
//	
//    CCSize s = CCDirector::sharedDirector()->getWinSize();
//
//    //CHECK_GL_ERROR_DEBUG();
//
//    // draw a simple line
//    // The default state is:
//    // Line Width: 1
//    // color: 255,255,255,255 (white, non-transparent)
//    // Anti-Aliased
//    //    glEnable(GL_LINE_SMOOTH);
//    
//	//ccDrawLine( ccp(0, s.height/2), ccp(s.width, s.height/2) );
//	//ccDrawLine( ccp(s.width/2, 0), ccp(s.width/2, s.height) );
//
//    //CHECK_GL_ERROR_DEBUG();
//}

void CCLegendAnimation::play()
{
	m_bIsPlaying = true;
}

void CCLegendAnimation::stop()
{
	//m_aframeIdx = 0;
	//m_timeElapse = 0;
	//m_animTick = 0;
	//m_delayCounter = 0;

	m_bIsPlaying = false;
}

void CCLegendAnimation::restart()
{
	m_aframeIdx = 0;
	m_timeElapse = 0;
	m_animTick = 0;
	m_delayCounter = 0;
}

void CCLegendAnimation::synchronizeFrom(CCLegendAnimation* other)
{
	m_aframeIdx = other->m_aframeIdx;
	m_timeElapse = other->m_timeElapse;
	m_animTick = other->m_animTick;
	m_delayCounter = other->m_delayCounter;

	m_bIsPlaying = other->m_bIsPlaying;

	setVisible(other->isVisible());
}

void CCLegendAnimation::setTexture(int imgIdx, CCTexture2D* newTexture)
{
#if LEGEND_ANIMATION_USE_AMODULE_CACHE
	for (std::map<int, std::vector<CCLegendAModule*> >::iterator it = m_CCLegendAModulePool.begin(); it != m_CCLegendAModulePool.end(); ++it) 
	{
		int key = it->first;
		int tmpImgIdx = key >> 16;
		if(tmpImgIdx == imgIdx)
		{
			std::vector<CCLegendAModule*>& amoduleVector = it->second;
			for(unsigned int i = 0; i < amoduleVector.size(); i++)
			{
				CCLegendAModule* tmp = amoduleVector.at(i);
				tmp->setTexture(newTexture);
			}
		}
	}
#if LEGEND_ANIMATION_SUPPORT_SPECIALAMODULE
	for (std::map<int, CCLegendAModule*>::iterator it = m_specialAModuleMap.begin(); it != m_specialAModuleMap.end(); ++it)
	{
		int key = it->first;
		int tmpImgIdx = key >> 16;
		if(tmpImgIdx == imgIdx)
		{
			CCLegendAModule* pAModule = it->second;
			pAModule->setTexture(newTexture);
		}
	}
#endif
#else
	//int moduleNum = m_pAnimationData->getModuleNum(imgIdx);
	//for(int i = 0; i < moduleNum; i++)
	//{
	//	CCLegendAModule* legendAModule = m_pAllAModules[imgIdx][i];
	//	legendAModule->setTexture(newTexture);
	//}

	// modify all actions
	for(int i = 0; i < m_pAnimationData->getAActionNum(); i++)
	{
		AAction* curAct = m_pAnimationData->getActions()[i];
		CCNodeRGBA* actionNode = (CCNodeRGBA*)getChildByTag(i);
		for(int j = 0; j < curAct->aframeNum; j++)
		{
			// CCLegendAModule is here
			AFrame* curAFrame = curAct->aFrames[j];
			CCNodeRGBA* aframeNode = (CCNodeRGBA*)actionNode->getChildByTag(j);

			int aModuleNum = curAFrame->amoduleNum;
			for(int m = 0; m < aModuleNum; m++)
			{
				AModule* curAMod = curAFrame->aModules[m];
				if(imgIdx == curAMod->imageID)   // specific image idx
				{
					CCLegendAModule* legendAModule = (CCLegendAModule*)aframeNode->getChildByTag(m);
					legendAModule->setTexture(newTexture);
				}
			}
		}
	}
#endif
}

//void CCLegendAnimation::drawAFrame(int actId, int aFrameId)
//{
//	if(actId < 0 || aFrameId < 0  || actId >= m_pAnimationData->getAActionNum())
//		return;
//
//	AAction* curAct = m_pAnimationData->getActions()[actId];
//
//	if(aFrameId >= curAct->aframeNum)
//		return;
//
//	// init the children, for example, the visibility of all CCLegendAModule
//    CCArray* pArray = this->getChildren();
//    CCObject* pObj = NULL;
//    CCARRAY_FOREACH(pArray, pObj)
//    {
//        CCLegendAModule* child = (CCLegendAModule*)pObj;
//		child->setVisible(false);
//	}
//
//	// display each CCLegendAModule
//	AFrame* curAFrame = curAct->aFrames[aFrameId];
//	int aModuleNum = curAFrame->amoduleNum;
//	for(int i = 0; i < aModuleNum; i++)
//	{
//		AModule* curAMod = curAFrame->aModules[i];
//
//		int imgId = curAMod->imageID;
//		int moduleId = curAMod->moduleID;
//		CCLegendAModule* legendAModule = m_pAllAModules[imgId][moduleId];
//
//		legendAModule->setVisible(true);
//		legendAModule->setTransform(curAMod->affineTrans);
//
//		reorderChild(legendAModule, i);
//	}
//
//	// use sortAllChildren(), to resolve the "ghost frame" problem
//	// the "ghost frmae" problem is some AModule are disappeared for a very short time when the animation is playing
//	sortAllChildren();
//}

#if LEGEND_ANIMATION_USE_AMODULE_CACHE
void CCLegendAnimation::drawAFrame(int actId, int aFrameId)
{
	if(actId < 0 || aFrameId < 0  || actId >= m_pAnimationData->getAActionNum())
		return;

	AAction* curAct = m_pAnimationData->getActions()[actId];

	if(aFrameId >= curAct->aframeNum)
		return;

	// first, clear all children
#if LEGEND_ANIMATION_SUPPORT_SPECIALAMODULE
	CCArray* pArray = this->getChildren();
	std::vector<CCLegendAModule*> needRemoveAModuleVector;
	CCObject* pObj = NULL;
	CCARRAY_FOREACH(pArray, pObj)
	{
		CCLegendAModule* pCClegendAModule = (CCLegendAModule*)(pObj);
		if(pCClegendAModule != NULL)
		{
			// the special AModule will not be removed
			if(pCClegendAModule->imgIdx == -1 && pCClegendAModule->modIdx == -1)
			{
				continue;
			}

			//CCLOG("remove amodule, img idx: %d, mod idx: %d", pCClegendAModule->imgIdx, pCClegendAModule->modIdx);
			needRemoveAModuleVector.push_back(pCClegendAModule);
		}
	}
	for(unsigned int i = 0; i < needRemoveAModuleVector.size(); i++)
	{
		this->removeChild(needRemoveAModuleVector.at(i), true);
	}

	// add the CCLegendAModule again
	AFrame* curAFrame = curAct->aFrames[aFrameId];
	int aModuleNum = curAFrame->amoduleNum;
	for(int i = 0; i < aModuleNum; i++)
	{
		AModule* curAMod = curAFrame->aModules[i];

		int imgId = curAMod->imageID;
		int moduleId = curAMod->moduleID;
		CCLegendAModule* aModule = this->addCCLegendAModule(imgId, moduleId);

		int key = this->getKey(imgId, moduleId);
		CCLegendAModule* pSpecialAModule = NULL;
		//CCLegendAModule* pSpecialAModule = m_specialAModuleMap[key];
		std::map<int, CCLegendAModule*>::const_iterator iter = m_specialAModuleMap.find(key) ; 
		if ( iter != m_specialAModuleMap.end() ) {
			pSpecialAModule = iter->second ;
		}

		if(pSpecialAModule == NULL)
		{
			if(aModule != NULL)
			{
				aModule->setVisible(true);
				aModule->setTransform(curAMod->affineTrans);
				//aModule->setOpacity(curAMod->colorValue);

				// alpha value
				int alpha = (curAMod->colorValue >> 24) & 0xFF;
				aModule->setOpacity(alpha);

				// rgb color
				aModule->setColor(GameUtils::convertToColor3B(curAMod->colorValue));

				addChild(aModule, i);
			}
		}
		else
		{
			pSpecialAModule->setTransform(curAMod->affineTrans);
			this->reorderChild(pSpecialAModule, i );
		}
	}
	// use sortAllChildren(), to resolve the "ghost frame" problem
	// the "ghost frmae" problem is some AModule are disappeared for a very short time when the animation is playing
	sortAllChildren();
#else
	this->removeAllChildrenWithCleanup(true);

	// add the CCLegendAModule again
	AFrame* curAFrame = curAct->aFrames[aFrameId];
	int aModuleNum = curAFrame->amoduleNum;
	for(int i = 0; i < aModuleNum; i++)
	{
		AModule* curAMod = curAFrame->aModules[i];

		int imgId = curAMod->imageID;
		int moduleId = curAMod->moduleID;
		CCLegendAModule* aModule = this->addCCLegendAModule(imgId, moduleId);
		aModule->setVisible(true);
		aModule->setTransform(curAMod->affineTrans);
		//aModule->setOpacity(curAMod->colorValue);

		// alpha value
		int alpha = (curAMod->colorValue >> 24) & 0xFF;
		aModule->setOpacity(alpha);

		// rgb color
		aModule->setColor(GameUtils::convertColor(curAMod->colorValue));

		addChild(aModule);
	}
#endif

	// after re-addChild, must reset some parameter
	setColor(this->getColor());
	setOpacity(this->getOpacity());   // now, this code line is for reflection on the water
}
#else
void CCLegendAnimation::drawAFrame(int actId, int aFrameId)
{
	if(actId < 0 || aFrameId < 0  || actId >= m_pAnimationData->getAActionNum())
		return;

	AAction* curAct = m_pAnimationData->getActions()[actId];

	// show specific action
	for(int i = 0; i < m_pAnimationData->getAActionNum(); i++)
	{
		CCNode* actionNode = this->getChildByTag(i);
		actionNode->setVisible(false);
	}
	CCNode* curActionNode = this->getChildByTag(actId);
	curActionNode->setVisible(true);

	if(aFrameId >= curAct->aframeNum)
		return;

	// show specific aframe
	for(int i = 0; i < curAct->aframeNum; i++)
	{
		CCNode* aFrameNode = curActionNode->getChildByTag(i);
		aFrameNode->setVisible(false);
	}
	CCNode* curAframeNode = curActionNode->getChildByTag(aFrameId);
	curAframeNode->setVisible(true);
}
#endif

CCRect CCLegendAnimation::getSurroundingRect(int actId, int aFrameId)
{
	if(actId < 0 || aFrameId < 0  || actId >= m_pAnimationData->getAActionNum())
		return CCRectZero;

	AAction* curAct = m_pAnimationData->getActions()[actId];

	if(aFrameId >= curAct->aframeNum)
		return CCRectZero;

	// calc each visible CCLegendAModule
	AFrame* curAFrame = curAct->aFrames[aFrameId];

	AModule* firstAMod = curAFrame->aModules[0];
	CCLegendAModule* firstAModule = m_pAllAModules[firstAMod->imageID][firstAMod->moduleID];
	CCRect tmpRect1 = firstAModule->boundingBox();

	int aModuleNum = curAFrame->amoduleNum;
	for(int i = 0; i < aModuleNum; i++)
	{
		AModule* curAMod = curAFrame->aModules[i];

		int imgId = curAMod->imageID;
		int moduleId = curAMod->moduleID;
		CCLegendAModule* legendAModule = m_pAllAModules[imgId][moduleId];

		
		legendAModule->setTransform(curAMod->affineTrans);

		legendAModule->boundingBox();

	}

	return CCRectZero;
}

float CCLegendAnimation::getPlayPercent()
{
	float result = m_animTick * 1.0f / (m_pAnimationData->getAnimTickNum(m_actIdx) - 1);
	return result;
}

void CCLegendAnimation::onEnter()
{
	CCNode::onEnter();

	this->restart();
	// enable its update() function
	//scheduleUpdateWithPriority(1);   // what's the meaning of priority ( 1 )?
	this->scheduleUpdate();
}
void CCLegendAnimation::onExit()
{
	CCNode::onExit();
	this->unscheduleUpdate();
}

void CCLegendAnimation::update(float dt)
{
	if(!isVisible())
		return;

	// update animation's sequence
	if(this->m_bIsPlaying && NULL != this->m_pAnimationData)
	{
		bool bLoop = m_bPlayLoop /*|| m_nPlayCount-- > 0*/;
		if(bLoop)
		{
			if(m_animTick > this->m_pAnimationData->getAnimTickNum(m_actIdx))
				m_animTick = 0;
		}
		else
		{
			int lastTick = this->m_pAnimationData->getAnimTickNum(m_actIdx) - 1;
			if(m_animTick >= lastTick)
			{
				m_animTick = lastTick;

				stop();
				if(m_bReleaseWhenStop)
				{
					this->removeFromParentAndCleanup(true);
				}
			}
		}
		m_aframeIdx = this->m_pAnimationData->getAFrameIdxByAnimTick(m_actIdx, m_animTick);
		//CCLOG("aframeIdx: %d", m_aframeIdx);

		// check if update animation's aframe idx
		m_timeElapse += (dt * 1000 * this->m_nPlaySpeed);   // 1 second = 1000 millisecond
		int animTotalTime = this->m_pAnimationData->getAnimTickNum(m_actIdx) * LEGEND_ANIMATION_DEFAULT_FRAME_DEALY;
		
		if(bLoop)
		{
			if(m_timeElapse > animTotalTime)
				m_timeElapse -= animTotalTime;
		}
		else
		{
			if(m_timeElapse > animTotalTime)
				m_timeElapse = animTotalTime;
		}
		
		m_animTick = m_timeElapse / LEGEND_ANIMATION_DEFAULT_FRAME_DEALY;
	}

	// draw the aframe
	//drawAFrame(m_actIdx, m_aframeIdx);
}