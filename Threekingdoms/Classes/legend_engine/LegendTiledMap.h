#ifndef __LEGEND_LEGENDTILEDMAP_H__
#define __LEGEND_LEGENDTILEDMAP_H__

#include "cocos2d.h"
#include "CCBinaryFileReader.h"

USING_NS_CC;
using namespace std;

/** 
 * class TiledMap, this map indicate the background which is organized by tiles
 * 1 TileMap maybe contains some TiledMapLayer(s)
 * one TiledMap object is the data from *.map (Legend TiledMap file)
 * @author zhaogang
 * @version 0.1.0
*/

class TiledMapLayer
{
public:
    TiledMapLayer();
    virtual ~TiledMapLayer();

	void load(CCBinaryFileReader* reader);

	int getWidth();
	int getHeight();

	CC_SYNTHESIZE_READONLY(short**, m_tileData, TileData)

	CC_SYNTHESIZE_READONLY(int, m_tileWidth, TileWidth)
	CC_SYNTHESIZE_READONLY(int, m_tileHeight, TileHeight)

	CC_SYNTHESIZE_READONLY(short, m_tileCol, TileCol)
	CC_SYNTHESIZE_READONLY(short, m_tileRow, TileRow)

	CC_SYNTHESIZE_READONLY(short, m_imgTileCol, ImgTileCol)
	CC_SYNTHESIZE_READONLY(short, m_imgTileRow, ImgTileRow)

	CC_SYNTHESIZE_READONLY(std::string, m_imgName, ImgName)
	std::string getImgNameByTileID(int id);

	// res path, not file name
	CC_SYNTHESIZE(std::string, m_resPath, ResPath)
};

class TiledMap
{
public:
    TiledMap();
    virtual ~TiledMap();

	void loadTiledMapData(const char* tiledmapFile);

	// res path, not file name
	CC_SYNTHESIZE_READONLY(std::string, m_resPath, ResPath)

	// one tiledmap contains some layers
	CC_SYNTHESIZE(int, m_nLayerCnt, LayerCnt)
	CC_SYNTHESIZE_READONLY(TiledMapLayer**, m_pLayers, Layers)

	CC_SYNTHESIZE_READONLY(CCSize, m_mapSize, MapSize)

	std::vector<std::string>* generateResourceList();

private:
	int m_collisionLayerId;   // 碰撞层，不用于绘制
	
	std::string resPath;   // path

private:
	std::string m_strImageFilenames;
};

#endif