#include "GameSceneCamera.h"

#include "./role/GameActor.h"
#include "GameSceneState.h"
#include "../legend_script/ScriptManager.h"

#define CAMERA_ACTION_SHAKE_SCREEN_TAG 101
#define CAMERA_ACTION_ZOOM_SCREEN_TAG 102

GameSceneCamera::GameSceneCamera()
: m_pCurScene(NULL)
{
}

GameSceneCamera::~GameSceneCamera()
{
}

GameSceneCamera* GameSceneCamera::create(GameSceneLayer* scene, int behaviour)
{
    GameSceneCamera *pNode = new GameSceneCamera();
    if(pNode && pNode->init(scene, behaviour))
    {
        pNode->autorelease();
        return pNode;
    }
    CC_SAFE_DELETE(pNode);
    return NULL;
}

bool GameSceneCamera::init(GameSceneLayer* scene, int behaviour)
{
	m_behaviour = behaviour;
	m_pCurScene = scene;

	return true;
}

void GameSceneCamera::MoveTo(CCPoint target, float time)
{
	// convert world position to cocos position
	CCPoint cocosPosition = m_pCurScene->convertToCocos2DSpace(target);

	if(time > 0)
	{
		CCMoveTo*  action = CCMoveTo::create(time, cocosPosition);
		this->runAction(action);
	}
	else if(time == 0)
	{
		this->setPosition(cocosPosition);
	}

	m_behaviour = camera_behaviour_move;
}

void GameSceneCamera::FollowActor(GameActor* actor)
{
	m_behaviour = camera_behaviour_follow_actor;
}

void GameSceneCamera::restoreCamera(float dt)
{
	// follow the main actor
	FollowActor(NULL);
}

void GameSceneCamera::shakeScreen(GameSceneLayer* scene, float time)
{
	// playing story
	if(ScriptManager::getInstance()->isInMovieMode())
		return;

	CCNode* sceneLayer = scene->getChildByTag(GameSceneLayer::kTagSceneLayer);
	if(sceneLayer->getActionByTag(CAMERA_ACTION_SHAKE_SCREEN_TAG) != NULL)
		return;

	// shake screen behaviour
	m_behaviour = camera_behaviour_shakescreen;

	CCPoint size = sceneLayer->getPosition();
	const int offset = 10;
	const int shake_action_steps = 5;
	const float shake_time = SHAKE_SCREEN_DEFAULT_TIME / shake_action_steps;
	float one_shake_duration = SHAKE_SCREEN_DEFAULT_TIME;
	unsigned int times = time / one_shake_duration;
	if(times <= 0)
		times = 1;
	CCMoveTo* left = CCMoveTo::create(shake_time,ccp(size.x+offset,size.y));
	CCMoveTo* right = CCMoveTo::create(shake_time,ccp(size.x-offset,size.y));
	CCMoveTo* top = CCMoveTo::create(shake_time,ccp(size.x,size.y+offset));
	CCMoveTo* rom = CCMoveTo::create(shake_time,ccp(size.x,size.y-offset));
	CCMoveTo* init = CCMoveTo::create(shake_time,ccp(size.x,size.y));
	CCFiniteTimeAction* shake_action = CCSequence::create(left,right,top,rom,init,NULL);
	CCRepeat *action = CCRepeat::create(shake_action, times);
	action->setTag(CAMERA_ACTION_SHAKE_SCREEN_TAG);
	sceneLayer->runAction(action);

	// restore camera behaviour
	scheduleOnce( schedule_selector(GameSceneCamera::restoreCamera), one_shake_duration * times); 
}

void GameSceneCamera::shakeScreen(float time, int offset)
{
	CCScene* currentScene = CCDirector::sharedDirector()->getRunningScene();
	if(currentScene->getActionByTag(CAMERA_ACTION_SHAKE_SCREEN_TAG) != NULL)
		return;

	CCPoint size = currentScene->getPosition();
	const int shake_action_steps = 5;
	const float shake_time = SHAKE_SCREEN_DEFAULT_TIME / shake_action_steps;
	float one_shake_duration = SHAKE_SCREEN_DEFAULT_TIME;
	unsigned int times = time / one_shake_duration;
	if(times <= 0)
		times = 1;
	CCMoveTo* left = CCMoveTo::create(shake_time,ccp(size.x+offset,size.y));
	CCMoveTo* right = CCMoveTo::create(shake_time,ccp(size.x-offset,size.y));
	CCMoveTo* top = CCMoveTo::create(shake_time,ccp(size.x,size.y+offset));
	CCMoveTo* rom = CCMoveTo::create(shake_time,ccp(size.x,size.y-offset));
	CCMoveTo* init = CCMoveTo::create(shake_time,ccp(size.x,size.y));
	CCFiniteTimeAction* shake_action = CCSequence::create(left,right,top,rom,init,NULL);
	CCRepeat *action = CCRepeat::create(shake_action, times);
	action->setTag(CAMERA_ACTION_SHAKE_SCREEN_TAG);
	currentScene->runAction(action);
}

CCPoint GameSceneCamera::getSceneCameraPosition(GameActor* actor)
{
	return ccp(actor->getPositionX(), actor->getPositionY()+SCENE_CAMERA_OFFSET_Y);
}

void GameSceneCamera::zoomScreen(GameSceneLayer* scene, CCPoint& centerPoint)
{
	// playing story
	if(ScriptManager::getInstance()->isInMovieMode())
		return;

	CCNode* sceneLayer = scene->getChildByTag(GameSceneLayer::kTagSceneLayer);
	if(sceneLayer->getActionByTag(CAMERA_ACTION_ZOOM_SCREEN_TAG) != NULL)
		return;

	sceneLayer->setContentSize(CCSizeMake(scene->getMapSize().width, scene->getMapSize().height/2));
	CCPoint cocosWorldPosition = scene->convertToCocos2DWorldSpace(centerPoint);
	float ax = cocosWorldPosition.x / scene->getMapSize().width;
	float ay = cocosWorldPosition.y / scene->getMapSize().height;
	sceneLayer->setAnchorPoint(ccp(ax, ay));

	// special screen behaviour
	//m_behaviour = camera_behaviour_shakescreen;
	const float zoominDuration = 0.25f;
	const float zoombackDuration = 0.15f;
	CCScaleTo* zoomin = CCScaleTo::create(zoominDuration, 1.25f);
	CCScaleTo* zoomback = CCScaleTo::create(zoombackDuration, 1.0f);
	CCFiniteTimeAction* action = CCSequence::create(zoomin,zoomback,NULL);
	action->setTag(CAMERA_ACTION_ZOOM_SCREEN_TAG);
	sceneLayer->runAction(action);

	// restore camera behaviour
	//scheduleOnce( schedule_selector(GameSceneCamera::restoreCamera), (zoominDuration + zoombackDuration)); 
}

void GameSceneCamera::zoom(GameSceneLayer* scene, CCPoint& centerPoint, float zoomFrom, float zoomTo, float duration)
{
	// playing story
	if(ScriptManager::getInstance()->isInMovieMode())
		return;

	CCNode* sceneLayer = scene->getChildByTag(GameSceneLayer::kTagSceneLayer);
	if(sceneLayer->getActionByTag(CAMERA_ACTION_ZOOM_SCREEN_TAG) != NULL)
		return;

	// calc center point, consider the map edge
	float _from = 1.0f / zoomFrom;
	float _to = 1.0f / zoomTo;
	float zoomValue = MAX(_from, _to);
	if(zoomValue < 1.0f)
		zoomValue = 1.0f;
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint worldPos = centerPoint;
	CCPoint sourcePoint = centerPoint;
	// handle x
	bool exceedLeft = worldPos.x - (winSize.width/2 * zoomValue) < 0;
	bool exceedRight = worldPos.x + (winSize.width/2 * zoomValue) > scene->getMapSize().width;
	if(exceedLeft && exceedRight)
	{
		sourcePoint.x = scene->getMapSize().width/2;
	}
	else
	{
		if(exceedLeft)
		{
			sourcePoint.x = 0;
		}
		else if(exceedRight)
		{
			sourcePoint.x = scene->getMapSize().width;
		}
	}
	// handle y
	bool exceedTop = worldPos.y - (winSize.height*zoomValue) < 0;
	bool exceedBottom = worldPos.y + (winSize.height*zoomValue) > scene->getMapSize().height;
	if(exceedTop && exceedBottom)
	{
		sourcePoint.y = scene->getMapSize().height/2;
	}
	else
	{
		if(exceedTop)
		{
			sourcePoint.y = 0;
		}
		else if(exceedBottom)
		{
			sourcePoint.y = scene->getMapSize().height;
		}
	}
	centerPoint = sourcePoint;

	sceneLayer->setContentSize(CCSizeMake(scene->getMapSize().width, scene->getMapSize().height/2));
	CCPoint cocosWorldPosition = scene->convertToCocos2DWorldSpace(centerPoint);
	float ax = cocosWorldPosition.x / scene->getMapSize().width;
	float ay = cocosWorldPosition.y / scene->getMapSize().height;
	sceneLayer->setAnchorPoint(ccp(ax, ay));

	// special screen behaviour
	//m_behaviour = camera_behaviour_shakescreen;
	CCScaleTo* zoomin = CCScaleTo::create(0.0001f, zoomFrom);
	CCScaleTo* zoomback = CCScaleTo::create(duration, zoomTo);
	CCFiniteTimeAction* action = CCSequence::create(zoomin,zoomback,NULL);
	action->setTag(CAMERA_ACTION_ZOOM_SCREEN_TAG);
	sceneLayer->runAction(action);

	// restore camera behaviour
	//scheduleOnce( schedule_selector(GameSceneCamera::restoreCamera), (zoominDuration + zoombackDuration)); 
}