#include "MyPlayerAI.h"

#include "MyPlayer.h"
#include "MyPlayerOwnedCommand.h"
#include "../GameSceneState.h"
#include "MyPlayerAIConfig.h"
#include "../MainScene.h"
#include "GameView.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../ui/missionscene/HandleMission.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../messageclient/protobuf/GeneralMessage.pb.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CDrug.h"
#include "../../messageclient/element/MapRobotPotionLimit.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../skill/GameFightSkill.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../legend_script/ScriptManager.h"
#include "../../ui/shop_ui/ShopUI.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../ui/offlinearena_ui/OffLineArenaData.h"
#include "../../newcomerstory/NewCommerStoryManager.h"

MyPlayerAI::MyPlayerAI()
: m_pMyPlayer(NULL)
{
	//remindBuyPotionTimeCd = 0;
	//remindUsePotionTimeCd =0;

	remindBuyPotionMoneyIsNotEnoug = true;
}

MyPlayerAI::~MyPlayerAI()
{

}

void MyPlayerAI::setMyPlayer(MyPlayer* self)
{
	m_pMyPlayer = self;
}

void MyPlayerAI::start()
{
	m_startPoint = m_pMyPlayer->getWorldPosition();

	// timer
	this->unschedule(schedule_selector(MyPlayerAI::update));
	this->schedule( schedule_selector(MyPlayerAI::update), 1.5f); 
	//show buy potion is money not enough
	this->setIsShowBuyPotionIsNotEnough(true);

}

void MyPlayerAI::startFight()
{
	MyPlayerAIConfig::setAutomaticSkill(1);
	MyPlayerAIConfig::enableRobot(true);
	GameView::getInstance()->myplayer->getMyPlayerAI()->start();
	GameView::getInstance()->myplayer->getMyPlayerAI()->autoUseSkill();
}

void MyPlayerAI::stop()
{
	this->unschedule(schedule_selector(MyPlayerAI::update));
}

void MyPlayerAI::setStartPoint(CCPoint startPoint)
{
	m_startPoint.setPoint(startPoint.x, startPoint.y);
}

void MyPlayerAI::updateSkillList(std::string* list)
{
	m_skillList.clear();
	for(unsigned int i = 0; i < ROBOT_SETUP_SKILL_MAX; i++)
	{
		if(list[i].size() > 0)
		{
			m_skillList.push_back(list[i]);
		}
	}
}

bool MyPlayerAI::canAutoCombat()
{
	// 正在执行来自玩家的指令，忽略挂机的自动战斗
	if(m_pMyPlayer->getCommand() != NULL 
		&& (m_pMyPlayer->getCommand()->commandSource == ActorCommand::source_from_player))
	{
		return false;
	}

	// 任务面板打开时，停止自动战斗
	HandleMission* handleMission = (HandleMission*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission);
	if (handleMission != NULL)
	{
		return false;
	}

	// 正在播放剧情
	if(ScriptManager::getInstance()->isInMovieMode())
	{
		return false;
	}

	//处于竞技场倒计时状态
	if (OffLineArenaData::getInstance()->getCurStatus() == OffLineArenaData::s_inCountDown)
	{
		return false;
	}

	return true;
}

bool MyPlayerAI::useSkill(std::string& skillId)
{
	// 技能的搜索释放范围
	int rangeInPixel = TILE_WIDTH_HALF *MyPlayerAIConfig::getMoveRange();

	int source = MyPlayerCommandAttack::source_from_robot;
	return m_pMyPlayer->useSkill(skillId, false, true, true, rangeInPixel, source);
}

bool MyPlayerAI::autoUseSkill()	
{
	// 检查是否有可攻击的目标，没有则不使用技能
	int rangeInPixel = TILE_WIDTH_HALF *MyPlayerAIConfig::getMoveRange();   // 技能的搜索释放范围
	if(!m_pMyPlayer->selectEnemy(rangeInPixel))
	{
		// 移除“自动战斗”action
		m_pMyPlayer->removeAutoCombatFlag();

		return false;
	}
	
	// 添加“自动战斗”action
	m_pMyPlayer->addAutoCombatFlag();

	int source = MyPlayerCommandAttack::source_from_robot;

	// 如果设置了挂机技能，则不考虑快捷栏技能
	if(m_skillList.size() > 0)
	{
		// 1. 使用挂机系统配置的技能
		for(unsigned int i = 0; i < m_skillList.size(); i ++)
		{
			std::string skillId = m_skillList.at(i);
			if(BasePlayer::isDefaultAttack(skillId.c_str()))   // 无视普攻
				continue;
			if(useSkill(skillId))
			{
				return true;
			}
		}
	}
	else
	{
		// 2. 使用快捷栏配置的技能
		std::vector<CShortCut*>& list = GameView::getInstance()->shortCutList;
		for(unsigned int i = 0; i < list.size(); i++)
		{
			CShortCut* pShortCut = list.at(i);
			if (pShortCut->complexflag() != 1 && pShortCut->storedtype() == 1)   // 非无双技
			{
				std::string skillId = pShortCut->skillpropid();
				if(BasePlayer::isDefaultAttack(skillId.c_str()))   // 无视普攻
					continue;
				//if short skill level <= 0
				std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
				for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
				{
					GameFightSkill * tempSkill = it->second;
					if (strcmp(tempSkill->getId().c_str(),skillId.c_str())==0 && tempSkill->getLevel()>0)
					{
						if(useSkill(skillId))
							return true;
					}
				}
			}
		}
	}

	// 3. 使用职业默认技能
    std::string skillId = m_pMyPlayer->getDefaultSkill();
	return useSkill(skillId);
}

bool MyPlayerAI::usePotion(const char* potionName, long long roleId)	
{
	// map type: if it's arena ( pvp ), MUST attack!
	if(GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::guildFight
		|| GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::coliseum)
		return false;
	
	for(unsigned int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
	{
		if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
		{
			std::string pacName = GameView::getInstance()->AllPacItem.at(i)->goods().id();
			if (strcmp(potionName, pacName.c_str())==0)
			{
				//等级限制
				bool bInCD = DrugManager::getInstance()->getDrugById(potionName)->isInCD();
				if(bInCD)
				{
					/*
					long long time_ = DrugManager::getInstance()->getDrugById(potionName)->getRemainTime();
					std::string string_ = potionName;
					string_.append("is CD time = ");
					char timeStr[20];
					sprintf(timeStr,"%ld",time_);
					string_.append(timeStr);
					GameView::getInstance()->showAlertDialog(string_);
					*/
					return false;
				}
				else
				{
					int pacGoodsIndex =GameView::getInstance()->AllPacItem.at(i)->id();
					PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
					temp->index = pacGoodsIndex;
					temp->num = 1;
					MapRobotPotionLimit * potionLimit = RobotPotionMilitData::s_RobotPotionMilit[potionName];
					int potionLevel = potionLimit->get_level();
					int roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
					if (roleLevel >= potionLevel)
					{
						DrugManager::getInstance()->getDrugById(potionName)->startCD();
						GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)roleId,temp);
					}
					/*
					char roStr[20];
					sprintf(roStr,"%ld",roleId);
					char numStr[10];
					sprintf(numStr,"%d",pacGoodsIndex);
					std::string string_ = roStr;
					string_.append("----------");
					string_.append(numStr);
					GameView::getInstance()->showAlertDialog(string_);
					*/
					delete temp;

					return true;
				}
			}
		}
	}

	return false;
}

void MyPlayerAI::robotBuyPotion( const char *potionName,int potionAmount )
{
	if (NewCommerStoryManager::getInstance()->IsNewComer())
	{
		return;
	}

	int goodsAmount = 0;
	bool isFull = true;
	//遍历背包 查询挂机设置的药品是否还有 背包是否有空格
	for(unsigned int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
	{
		if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
		{
			std::string pacName = GameView::getInstance()->AllPacItem.at(i)->goods().id();
			if (strcmp(potionName, pacName.c_str())==0)
			{
				goodsAmount = GameView::getInstance()->AllPacItem.at(i)->quantity();	
			}
		}else
		{
			isFull = false;
		}
	}
	if(goodsAmount ==0 && isFull == false)
	{
		MapRobotPotionLimit * potionLimit = RobotPotionMilitData::s_RobotPotionMilit[potionName];
		int pacCount = 0;
		int PotionPrice = potionLimit->get_price();
		int roleMoneyGold = GameView::getInstance()->getPlayerGold();
		if (roleMoneyGold >= PotionPrice*potionAmount)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1137,(void *)potionName, (void *)potionAmount);
		}else
		{
			//is first req  show only once
			if (this->getIsShowBuyPotionIsNotEnough())
			{
				const char *string_  = StringDataManager::getString("robot_notEnoughMoneyBuyPotion");
				GameView::getInstance()->showAlertDialog(string_);
				this->setIsShowBuyPotionIsNotEnough(false);
			}
		}
	}
}


void MyPlayerAI::update(float dt)
{
	if(!MyPlayerAIConfig::isEnableRobot()) {
		return;
	}

	// 自动战斗，包括使用技能、回退等
	if(canAutoCombat())
	{
		if(m_pMyPlayer->isAction(ACT_STAND))
		{
			if(isInRange())
			{
				// do attack
				if (MyPlayerAIConfig::getAutomaticSkill()==1)
				{
					autoUseSkill();
				}
			}
			else
			{
				moveBack();
			}
		}
		else if(m_pMyPlayer->isAction(ACT_RUN))
		{
			if(!isInRange())
			{
				moveBack();
			}
		}
		else if(m_pMyPlayer->isAction(ACT_ATTACK))
		{
		}
		else
		{
		}
	}
	//remindBuyPotionTimeCd -=dt;
	//remindUsePotionTimeCd -=dt;
	// auto use hp potion
	//如果当前是挂机 且选中吃药  获取当前的挂机存档数据
	int checkBoxSetRoleHp = MyPlayerAIConfig::getRoleUseHp();
	if (checkBoxSetRoleHp == 1)
	{
		int m_setsliderHp = MyPlayerAIConfig::getRoleLimitHp();
		if (m_pMyPlayer->getActiveRole()->hp() < (m_setsliderHp *1.0f / 100 * m_pMyPlayer->getActiveRole()->maxhp()))
		{
			std::string useGoodsName = MyPlayerAIConfig::getRoleUseDrugHpName();
			usePotion(useGoodsName.c_str());
		}
	}
	// auto use mp potion
	int checkBoxSetRoleMp = MyPlayerAIConfig::getRoleUseMp();
	if (checkBoxSetRoleMp == 1)
	{
		int m_setsliderMp = MyPlayerAIConfig::getRoleLimitMp();
		if (m_pMyPlayer->getActiveRole()->mp() <(m_setsliderMp *1.0f / 100 * m_pMyPlayer->getActiveRole()->maxmp()))
		{
			std::string useGoodsName = MyPlayerAIConfig::getRoleUseDrugMpName();
			usePotion(useGoodsName.c_str());
		}
	}
	///auto use general hp potion
	int checkBoxSetGeneralHp = MyPlayerAIConfig::getGeneralUseHp();
	if (checkBoxSetGeneralHp == 1)
	{
		int m_setsliderGeneralHp = MyPlayerAIConfig::getGeneralLimitHp();
		std::vector<long long>tempvector;
		//遍历出战的武将
		for (unsigned int i=0;i<GameView::getInstance()->generalsInLineList.size();i++)
		{
			if (GameView::getInstance()->generalsInLineList.at(i)->fightstatus() == 2)
			{
				long long generalId =GameView::getInstance()->generalsInLineList.at(i)->id();
				tempvector.push_back(generalId);
			}
		}
		if (tempvector.size() >0)
		{
			long long generalId =  getUsePotionGeneralIndex(tempvector);
			if (generalId > 0)
			{
				std::string useGoodsName = MyPlayerAIConfig::getGeneralUseDrugHpName();
				usePotion(useGoodsName.c_str(),generalId);
				
				/*
				//showAlertDialog the general name 
				for (int a=0;a< GameView::getInstance()->generalsInLineDetailList.size();a++)
				{
					if (generalId == GameView::getInstance()->generalsInLineDetailList.at(a)->generalid())
					{
						std::string generalName_ = GameView::getInstance()->generalsInLineDetailList.at(a)->activerole().rolebase().name();
						GameView::getInstance()->showAlertDialog(generalName_);
					}
				}
				*/
			}
		}
	}
	///auto use general mp potion
	int checkBoxSetGeneralMp = MyPlayerAIConfig::getGeneralUseMp();
	if (checkBoxSetGeneralMp == 1)
	{
		int m_setsliderGeneralMp = MyPlayerAIConfig::getGeneralLimitMp();
		/*遍历出战的武将 遍历每个武将的血和蓝 */
		for (unsigned int i=0;i<GameView::getInstance()->generalsInLineList.size();i++)
		{
			if (GameView::getInstance()->generalsInLineList.at(i)->fightstatus() == 2)
			{
				long long generalId =GameView::getInstance()->generalsInLineList.at(i)->id();
				for(unsigned int j=0;j<GameView::getInstance()->generalsInLineDetailList.size();j++)
				{
					if (generalId == GameView::getInstance()->generalsInLineDetailList.at(j)->generalid())
					{
						GameSceneLayer* scene = GameView::getInstance()->getGameScene();
						BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(generalId));
                        if(target)
                        {
                            int generalMp  =target->getActiveRole()->mp();
                            
                            //int generalMp  = GameView::getInstance()->generalsInLineDetailList.at(j)->activerole().mp();
                            int generalMaxMp =GameView::getInstance()->generalsInLineDetailList.at(j)->activerole().maxmp();
                            if (generalMp<(m_setsliderGeneralMp *1.0f/100*generalMaxMp ))
                            {
                                std::string useGoodsName = MyPlayerAIConfig::getGeneralUseDrugMpName();
                                usePotion(useGoodsName.c_str(),generalId);
                            }
                        }
					}
				}
			}
		}
	}

	/*远程购买*/
	int checkBoxBuyHp = MyPlayerAIConfig::getRoleBuyHp();
	if (checkBoxBuyHp == 1)
	{
		std::string buyGoodsName = MyPlayerAIConfig::getRoleUseDrugHpName();
		this->robotBuyPotion(buyGoodsName.c_str(),10 );
	}

	int checkBoxBuyMp = MyPlayerAIConfig::getRoleBuyMp();
	if (checkBoxBuyMp == 1)
	{
		std::string buyGoodsName = MyPlayerAIConfig::getRoleUseDrugMpName();
		this->robotBuyPotion(buyGoodsName.c_str(),10 );
	}
	

	int checkBoxGeneralBuyHp = MyPlayerAIConfig::getGeneralBuyHp();
	if (checkBoxGeneralBuyHp == 1)
	{
		std::string buyGoodsName = MyPlayerAIConfig::getGeneralUseDrugHpName();
		this->robotBuyPotion(buyGoodsName.c_str(),10 );
	}


	int checkBoxGeneralBuyMp = MyPlayerAIConfig::getGeneralBuyMp();
	if (checkBoxGeneralBuyMp == 1)
	{
		std::string buyGoodsName = MyPlayerAIConfig::getGeneralUseDrugMpName();
		this->robotBuyPotion(buyGoodsName.c_str(),10 );
	}
	//auto repair equipment
	int checkBoxRepairEquip = MyPlayerAIConfig::getAutoRepairEquip();
	if (checkBoxRepairEquip == 1)
	{
		for (int equipIndex =0; equipIndex < GameView::getInstance()->EquipListItem.size();equipIndex++)
		{
			if (GameView::getInstance()->EquipListItem.at(equipIndex)->goods().equipmentdetail().durable() <= 10)
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1635,(void *)0,(void *)0);
				break;
			}
		}

		for(int generalIndex = 0;generalIndex< GameView::getInstance()->generalsInLineDetailList.size();generalIndex++)
		{
			int equipSize = GameView::getInstance()->generalsInLineDetailList.at(generalIndex)->equipments_size();
			for (int generalEquipIndex = 0;generalEquipIndex< equipSize;generalEquipIndex++ )
			{
				if (GameView::getInstance()->generalsInLineDetailList.at(generalIndex)->equipments(generalEquipIndex).goods().equipmentdetail().durable() <=10)
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1635,(void *)0,(void *)0);
					break;
				}
			}
		}
	}

	//auto sell goods
	int pacSize = GameView::getInstance()->AllPacItem.size();
	for (int checkIndex = 6; checkIndex <9;checkIndex++)
	{
		if (MyPlayerAIConfig::getGoodsPickAndSell(checkIndex) == 1)
		{
			int selectEquipQuality = MyPlayerAIConfig::getEquipMentQualityByRobotAutoSell(checkIndex);
			for (int pacIndex = 0;pacIndex < pacSize;pacIndex++)
			{
				if (GameView::getInstance()->AllPacItem.at(pacIndex)->has_goods())
				{
					int curPacFloderQuality = GameView::getInstance()->AllPacItem.at(pacIndex)->goods().quality();
					int curPacType = GameView::getInstance()->AllPacItem.at(pacIndex)->goods().equipmentclazz();
					if (selectEquipQuality == curPacFloderQuality && curPacType > 0 && curPacType <10)
					{
						int folderId_ = GameView::getInstance()->AllPacItem.at(pacIndex)->id();
						int amount_ = GameView::getInstance()->AllPacItem.at(pacIndex)->quantity();
						ShopUI::sellGoodsStruct goodsStruct = {folderId_,amount_,1};
						GameMessageProcessor::sharedMsgProcessor()->sendReq(1135,&goodsStruct);
					}
				}
			}
		}
	}
}

bool MyPlayerAI::isInRange()
{
	int rangeInPixel = TILE_WIDTH_HALF *MyPlayerAIConfig::getMoveRange();
	if(ccpDistance(m_startPoint, m_pMyPlayer->getWorldPosition()) <= rangeInPixel)
	{
		return true;
	}

	return false;
}

void MyPlayerAI::moveBack()
{
	// setup move command
	MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
	cmd->targetPosition = m_startPoint;
	m_pMyPlayer->setNextCommand(cmd, true, m_pMyPlayer->isAttacking());

	GameView::getInstance()->showAlertDialog(StringDataManager::getString("robot_out_of_range"));
}

void MyPlayerAI::reSetSkillList(std::string skillid)
{
	std::vector<std::string> temp_skillList;

	std::string playerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
	for (int i=0;i<m_skillList.size();i++)
	{
		temp_skillList.push_back(m_skillList.at(i));
	}
	m_skillList.clear();


	for (int j=0;j<temp_skillList.size();j++)
	{
		std::string curSkillId = temp_skillList.at(j);
		char indexStr[3];
		sprintf(indexStr, "%d", j);
		if (strcmp(skillid.c_str(),curSkillId.c_str())==0)
		{
			curSkillId = "";
			//m_skillList.erase(m_skillList.begin()+i);
			//MyPlayerAIConfig::setSkillList(i,"");
		}
		CCUserDefault::sharedUserDefault()->setStringForKey((playerName+ROBOT_SETUP_SKILL_ID_PREFIX+indexStr).c_str(),curSkillId.c_str());
		if (curSkillId.size() > 0)
		{
			m_skillList.push_back(curSkillId);
		}
	}
}

long long MyPlayerAI::getUsePotionGeneralIndex(std::vector<long long> fightGeneral)
{
	int m_setsliderGeneralHp = MyPlayerAIConfig::getGeneralLimitHp();
	int firstGeneralHp = 0;
	int firstGeneralMaxHp = 0;
	int secondGeneralHp = 0;
	int secondGeneralmaxHp = 0;
	for(unsigned int j=0;j<GameView::getInstance()->generalsInLineDetailList.size();j++)
	{
		if (fightGeneral.at(0) == GameView::getInstance()->generalsInLineDetailList.at(j)->generalid())
		{
			GameSceneLayer* scene = GameView::getInstance()->getGameScene();
			BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(fightGeneral.at(0)));
			if(target != NULL)
			{
				firstGeneralHp  =target->getActiveRole()->hp();
				firstGeneralMaxHp =GameView::getInstance()->generalsInLineDetailList.at(j)->activerole().maxhp();
			}
		}
		if (fightGeneral.size() >1)
		{
			if (fightGeneral.at(1) == GameView::getInstance()->generalsInLineDetailList.at(j)->generalid())
			{
				GameSceneLayer* scene = GameView::getInstance()->getGameScene();
				BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(fightGeneral.at(1)));
				if(target != NULL)
				{
					secondGeneralHp  =target->getActiveRole()->hp();
					secondGeneralmaxHp =GameView::getInstance()->generalsInLineDetailList.at(j)->activerole().maxhp();				
				}
			}
		}
	}
	
	if (fightGeneral.size() >1)
	{
		//如果2个武将同时需要吃药 则优先按血量的百分比算
		if ( firstGeneralHp <(m_setsliderGeneralHp *1.0f/100*firstGeneralMaxHp) && 
				secondGeneralHp <(m_setsliderGeneralHp *1.0f/100*secondGeneralmaxHp) &&
				firstGeneralHp >0 && secondGeneralHp > 0)
		{
			float scaleFirstGeneral = firstGeneralHp*1.0f/firstGeneralMaxHp*1.0f;
			float scaleSecondGeneral = secondGeneralHp*1.0f/secondGeneralmaxHp*1.0f;
			if (scaleFirstGeneral > scaleSecondGeneral)
			{
				return fightGeneral.at(1);
			}else
			{
				return fightGeneral.at(0);
			}
		}else
		{
			if ( firstGeneralHp <(m_setsliderGeneralHp *1.0f/100*firstGeneralMaxHp))
			{
				return fightGeneral.at(0);
			}else
			{
				if (secondGeneralHp <(m_setsliderGeneralHp *1.0f/100*secondGeneralmaxHp))
				{
					return fightGeneral.at(1);
				}else
				{
					return -1;
				}
			}
		}
	}else
	{
		//如果返回 -1 则不需要吃药
		if ( firstGeneralHp <(m_setsliderGeneralHp *1.0f/100*firstGeneralMaxHp))
		{
			return fightGeneral.at(0);
		}else
		{
			return -1;
		}
	}

}

void MyPlayerAI::setIsShowBuyPotionIsNotEnough( bool value_ )
{
	remindBuyPotionMoneyIsNotEnoug = value_;
}

bool MyPlayerAI::getIsShowBuyPotionIsNotEnough()
{
	return remindBuyPotionMoneyIsNotEnoug;
}

void MyPlayerAI::autoMove(MyPlayer* me, float range) {
	GameSceneLayer* scene = me->getGameScene();

	// find the wander target
	float random = CCRANDOM_0_1() - 0.5f;
	float x = me->getWorldPosition().x + random * range;
	random = CCRANDOM_0_1() - 0.5f;
	float y = me->getWorldPosition().y + random * range;

	// check the target position
	int tileX = scene->positionToTileX(x);
	int tileY = scene->positionToTileY(y);
	while(scene->isLimitOnGround(tileX, tileY)) 
	{
		// find the wander target
		random = CCRANDOM_0_1() - 0.5f;
		x = me->getWorldPosition().x + random * range;
		random = CCRANDOM_0_1() - 0.5f;
		y = me->getWorldPosition().y + random * range;

		tileX = scene->positionToTileX(x);
		tileY = scene->positionToTileY(y);
	}

	MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
	cmd->targetPosition = ccp(x, y);
	cmd->method = MyPlayerCommandMove::method_searchpath;
	GameView::getInstance()->myplayer->setNextCommand(cmd, true);	
}