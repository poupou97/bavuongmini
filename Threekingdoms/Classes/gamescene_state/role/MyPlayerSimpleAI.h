#ifndef _LEGEND_GAMEACTOR_MYPLAYERSIMPLEAI_H_
#define _LEGEND_GAMEACTOR_MYPLAYERSIMPLEAI_H_

#include "cocos2d.h"
USING_NS_CC;

class MyPlayer;

/**
 * 玩家自己的简单AI类
 * 用于简单AI（非挂机）的一些处理，比如攻击一个敌人直到其死亡、反击等等
 * 
 * MyPlayerSimpleAI方案
 *     优点：易于控制
 *     缺点：1. 若技能（比如冲锋）未施放成功，则不应该进入杀死敌人的状态
 *              2. 攻击动作，衔接不紧
 *     实现思路：每隔固定时间（比如1秒，技能cd时间），检测能否攻击，可以就下达攻击指令
 * 
 * ActorCommand::setNextCommand()方案
 *    优点：可以做到紧密自然的动作衔接，效果好。也不会不按需求进入杀死敌人状态。
 *    缺点：需要处理的情况比较复杂，难于控制。next command很容易被打断。
 * @author zhaogang
 * @version 0.1.0
 */
class MyPlayerSimpleAI : public CCNode
{
public:
	MyPlayerSimpleAI();
	virtual ~MyPlayerSimpleAI();

	virtual void update(float dt);

	void setMyPlayer(MyPlayer* self);

	void stopAll();

	/**
	* 玩家主动攻击目标后，技能释放完成后，开始持续使用普攻，直到将对方杀死
	* 若玩家做了其他操作，比如摇杆或触屏移动后，停止杀死对方的状态
	*/
	void startKill(long long roleId);
	void stopKill();
	void updateKill(float dt);

	/**
	* 自动采集
	* 玩家在任务小面板中，点击了采集类任务，则开始自动采集
	* 若玩家做了其他操作，比如摇杆或触屏移动后，停止自动采集
	*/
	void startPick(long long roleId);
	void stopPick();
	void updatePick(float dt);

public:
	long long targetRoleId;
	long long pickingRoleId;

private:
	MyPlayer* m_pMyPlayer;
};

#endif