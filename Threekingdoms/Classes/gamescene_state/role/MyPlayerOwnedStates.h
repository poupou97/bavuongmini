#ifndef MYPLAYER_OWNED_STATES_H
#define MYPLAYER_OWNED_STATES_H
//------------------------------------------------------------------------
//
//  Name:   MyPlayerOwnedStates.h
//
//  Desc:   All the states that can be assigned to the MyPlayer class
//  该类以状态机的方式，定义了角色的基础行为、或者叫基础动作、或者叫基础能力，
//  比如，站立、跑动、攻击、死亡倒地(包含特殊死亡效果）、采集、跳跃、坐、坐骑动作、特殊技能动作（比如旋风斩、冲锋）等等
//  即使像采集这样的行为可能没有具体动画动作，但由于有采集进度条，因此也理解为一个独立的基础行为
//
// 这些state基本上都会和一个ActionContext关联，每个state会根据ActionContext提供的信息，完成相关动作
// 角色同一时刻只能做1个动作
//  Author: Zhao Gang 2013
//
//------------------------------------------------------------------------
#include "State.h"

class MyPlayer;

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class MyPlayerGlobalState : public State<MyPlayer>
{  
private:
  
  MyPlayerGlobalState(){}
  
  //copy ctor and assignment should be private
  MyPlayerGlobalState(const MyPlayerGlobalState&);
  MyPlayerGlobalState& operator=(const MyPlayerGlobalState&);
 
public:

  static MyPlayerGlobalState* Instance();
  
  virtual void Enter(MyPlayer* self){}

  virtual void Execute(MyPlayer* self);

  virtual void Exit(MyPlayer* self){}
};


//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class MyPlayerStand : public State<MyPlayer>
{
private:
  
  MyPlayerStand(){}

  //copy ctor and assignment should be private
  MyPlayerStand(const MyPlayerStand&);
  MyPlayerStand& operator=(const MyPlayerStand&); 
  
public:

  static MyPlayerStand* Instance();
  
  virtual void Enter(MyPlayer* self);

  virtual void Execute(MyPlayer* self);

  virtual void Exit(MyPlayer* self);

};



//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class MyPlayerRun : public State<MyPlayer>
{
public:
	enum MoveTargetType {
		Type_NoTarget = 0,   // if no target, client will not send target to server, for example, using joystick
		Type_HasTarget = 1,
		Type_Invalid = 3,
	};

private:
  
  MyPlayerRun(){}

  //copy ctor and assignment should be private
  MyPlayerRun(const MyPlayerRun&);
  MyPlayerRun& operator=(const MyPlayerRun&);
 
public:

  static MyPlayerRun* Instance();
  
  virtual void Enter(MyPlayer* self);

  virtual void Execute(MyPlayer* self);

  virtual void Exit(MyPlayer* self);

public:
	inline void setMoveTargetType(int type) { mMoveTargetType = type; }

private:
	double m_dBeginTime;
	int m_nSynchronizeInterval;   // the interval for synchronize MyPlayer's position
	int mMoveTargetType;
};

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class MyPlayerAttack : public State<MyPlayer>
{
private:
  
  MyPlayerAttack(){}

  //copy ctor and assignment should be private
  MyPlayerAttack(const MyPlayerAttack&);
  MyPlayerAttack& operator=(const MyPlayerAttack&);

  void playEffect(int profession);
 
public:

  static MyPlayerAttack* Instance();
  
  virtual void Enter(MyPlayer* self);

  virtual void Execute(MyPlayer* self);

  virtual void Exit(MyPlayer* self);

private:
	bool m_bIsSkillReleased;
	float m_nReleasePhase;

};

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class MyPlayerWhirl : public State<MyPlayer>
{
private:
  
  MyPlayerWhirl(){}

  //copy ctor and assignment should be private
  MyPlayerWhirl(const MyPlayerWhirl&);
  MyPlayerWhirl& operator=(const MyPlayerWhirl&);
 
public:

  static MyPlayerWhirl* Instance();
  
  virtual void Enter(MyPlayer* self);

  virtual void Execute(MyPlayer* self);

  virtual void Exit(MyPlayer* self);

private:
	float m_timer;

};

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class MyPlayerDeath : public State<MyPlayer>
{
private:
  
  MyPlayerDeath(){}

  //copy ctor and assignment should be private
  MyPlayerDeath(const MyPlayerDeath&);
  MyPlayerDeath& operator=(const MyPlayerDeath&);
 
public:

  static MyPlayerDeath* Instance();
  
  virtual void Enter(MyPlayer* self);

  virtual void Execute(MyPlayer* self);

  virtual void Exit(MyPlayer* self);

};

//------------------------------------------------------------------------
// 采集动作
class MyPlayerPick : public State<MyPlayer>
{
private:
  
  MyPlayerPick(){}

  //copy ctor and assignment should be private
  MyPlayerPick(const MyPlayerPick&);
  MyPlayerPick& operator=(const MyPlayerPick&); 
  
public:

  static MyPlayerPick* Instance();
  
  virtual void Enter(MyPlayer* self);

  virtual void Execute(MyPlayer* self);

  virtual void Exit(MyPlayer* self);

private:
	float m_fPickingTime;
	long long m_pickingTargetId;
	float m_ftime;
};

//------------------------------------------------------------------------
// 被击退
class MyPlayerKnockBack : public State<MyPlayer>
{
private:
  
  MyPlayerKnockBack(){}

  //copy ctor and assignment should be private
  MyPlayerKnockBack(const MyPlayerKnockBack&);
  MyPlayerKnockBack& operator=(const MyPlayerKnockBack&); 
  
public:

  static MyPlayerKnockBack* Instance();
  
  virtual void Enter(MyPlayer* self);

  virtual void Execute(MyPlayer* self);

  virtual void Exit(MyPlayer* self);
};

//------------------------------------------------------------------------
class MyPlayerCharge : public State<MyPlayer>
{
private:
  
  MyPlayerCharge(){}

  //copy ctor and assignment should be private
  MyPlayerCharge(const MyPlayerCharge&);
  MyPlayerCharge& operator=(const MyPlayerCharge&); 
  
public:

  static MyPlayerCharge* Instance();
  
  virtual void Enter(MyPlayer* self);
  virtual void Execute(MyPlayer* self);
  virtual void Exit(MyPlayer* self);
};

//------------------------------------------------------------------------
class MyPlayerJump : public State<MyPlayer>
{
private:
  
  MyPlayerJump(){}

  //copy ctor and assignment should be private
  MyPlayerJump(const MyPlayerJump&);
  MyPlayerJump& operator=(const MyPlayerJump&); 
  
public:

  static MyPlayerJump* Instance();
  
  virtual void Enter(MyPlayer* self);
  virtual void Execute(MyPlayer* self);
  virtual void Exit(MyPlayer* self);
};

#endif