#include "General.h"

#include "BaseFighterOwnedCommand.h"
#include "BasePlayer.h"
#include "MyPlayer.h"
#include "../GameSceneState.h"
#include "../skill/GameFightSkill.h"
#include "../MainScene.h"
#include "GameView.h"
#include "ActorUtils.h"
#include "../GameSceneEffects.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "BaseFighterConstant.h"
#include "GameActorAnimation.h"
#include "ActorUtils.h"
#include "../sceneelement/MyPlayerInfoMini.h"
#include "../skill/processor/SkillProcessor.h"
#include "AppMacros.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../../utils/StrUtils.h"
#include "../sceneelement/TargetInfoMini.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../utils/GameUtils.h"
#include "../../GameAudio.h"

#define MYPLAYER_GREEN_BLOOD_DURATION 8.0f
#define Sprite_CountDown_Tag 321

#define GENERAL_CHATBUBBLE_OFFSETY 148

General::General()
: m_bCanShowBubble(true)
{
	//this->scheduleUpdate();
	setType(GameActor::type_pet);

	m_resPath = "animation/generals/";

	setMoveSpeed(GENERAL_FIGHT_MOVESPEED);   // BASEFIGHTER_BASIC_MOVESPEED

	// logic init
	setAnimDir(ANIM_DIR_DOWN);
}

General::~General()
{
}

bool General::init(const char* actorName)
{
	Monster::init(actorName);

	// more nodes
	CCLegendAnimation* animNode = CCLegendAnimation::create("animation/texiao/changjingtexiao/GSGB_a_b/gsgb_d.anm");
	animNode->setPlayLoop(true);
	animNode->setReleaseWhenStop(false);
	animNode->setVisible(false);
    addChild(animNode, GameActor::ACTOR_FOOTFLAG_ZORDER, GameActor::kTagFootFlag);

	return true;
}

void General::onEnter()
{
	CCNode::onEnter();
// 	if (this->isMyPlayerGroup())
// 	{
// 		if (this->getActiveRole()->generalbaseinfo().showlefttime() >0)
// 			this->addPresentCountDown();
// 	}
}
void General::onExit()
{
	CCNode::onExit();
}

void General::showFlag(bool bShow)
{
	CCNode* flagNode = this->getChildByTag(GameActor::kTagFootFlag);
	if(flagNode != NULL)
		flagNode->setVisible(bShow);
}

void General::updateWander()
{
	// the general will not wander
}
void General::updateWorldPosition()
{
	if(this->isMyPlayerGroup())
	{
		CGeneralDetail* generalDetail;
		std::vector<CGeneralDetail *>& generalDetailList = GameView::getInstance()->generalsInLineDetailList;
		for (unsigned int i = 0;i<generalDetailList.size();++i)
		{
			generalDetail = generalDetailList.at(i);
			if (this->getRoleId() == generalDetail->generalid())
			{
				// melee attack profession
				if(generalDetail->profession() == PROFESSION_MJ_NAME || generalDetail->profession() == PROFESSION_HJ_NAME)
				{
					// if the my general is too far to MyPlayer, adjust his position
					const int long_range = 64 * 4;
					if(ccpDistance(GameView::getInstance()->myplayer->getWorldPosition(), this->getWorldPosition()) >= long_range)
					{
						Monster::updateWorldPosition();
					}
				}
				break;
			}
		}
	}
	else
	{
		Monster::updateWorldPosition();
	}
}

void General::update(float dt)
{
	drive();

	this->running(dt);

	this->GetFSM()->Update();

	///////////////////////////

	if(!this->isMyPlayerGroup())
		return;

	// chat bubble: gossip
	if(this->isAction(ACT_STAND) || this->isAction(ACT_RUN))
	{
		float random = CCRANDOM_0_1();
		if(random <= GeneralsDialogData::s_chance_gossip)
		{
			showChat(GENERAL_DIAGLOG_GOSSIP);
		}
	}

	// low hp
	float random = CCRANDOM_0_1();
	if(random <= GeneralsDialogData::s_chance_lowhp)
	{
		if(getActiveRole()->hp() < getActiveRole()->maxhp() * (GeneralsDialogData::s_lowhp_percent * 1.0f / 100))
		{
			showChat(GENERAL_DIAGLOG_LOWHP);
		}
	}

	// chat bubble: waitme
	if(this->getCommand() != NULL)
	{
		BaseFighterCommandFollow* cmd = dynamic_cast<BaseFighterCommandFollow*>(this->getCommand());
		if(cmd != NULL)
		{
			BaseFighter* bf = dynamic_cast<BaseFighter*>(this->getGameScene()->getActor(cmd->targetRoleId));
			if(bf != NULL)
			{
				if(ccpDistance(bf->getWorldPosition(), this->getWorldPosition()) > TILE_SIZE * 3)
				{
					float random = CCRANDOM_0_1();
					if(random < 0.005f)
					{
						// is showing chat bubble
						CCNode* container = this->getChildByTag(GameActor::kTagHeadEffectContainer);
						CCNode* flagNode = container->getChildByTag(GameActor::kTagChatBubble);
						if(flagNode == NULL)
						{
							const char* str = StringDataManager::getString("general_waitme");
							this->addChatBubble(str, 2.0f, GENERAL_CHATBUBBLE_OFFSETY);
						}
					}
				}
			}
		}
	}

	//add by yangjun 2014.9.28
// 	if (m_nRemainCountDown <= 0.f)
// 	{
// 		m_nRemainCountDown = 0.f;
// 	}
// 	else
// 	{
// 		m_nRemainCountDown -= 1000.f/60;
// 	}
	if (!this->isAction(ACT_DISAPPEAR))
	{
		CCLayer* layer_countDown = (CCLayer*)this->getChildByTag(kTagPresentCountDown);
		if (layer_countDown)
		{
			CCSprite * sprite_countDown = (CCSprite*)layer_countDown->getChildByTag(Sprite_CountDown_Tag);
			if (sprite_countDown)
			{
				CGeneralBaseMsg * temp = GeneralsStateManager::getInstance()->getGeneralBaseMsgById( this->getActiveRole()->rolebase().roleid());
				if (temp)
				{
					float blood_scale = temp->getRemainTime() * 1.0f / this->getActiveRole()->generalbaseinfo().showlefttime();
					if (blood_scale > 1.0f)
						blood_scale = 1.0f;

					float vl = 114*blood_scale*1.0f;
					sprite_countDown->setTextureRect(CCRectMake(0,0,vl,sprite_countDown->getContentSize().height));
				}			
			}
		}
	}
}

bool General::isMyPlayerGroup()
{
	if(BaseFighter::getMyPlayerId() == this->getOwnerId())
		return true;

	return false;
}
int General::getPosInGroup()
{
	MyPlayer* myplayer = dynamic_cast<MyPlayer*>(this->getGameScene()->getActor(this->m_ownerId));
	if(myplayer != NULL)
	{
		std::vector<long long>& generals = myplayer->getAliveGenerals();
		for(unsigned int i = 0; i < generals.size(); i++)
		{
			if(generals.at(i) == getRoleId())
				return i + 1;
		}
	}

	return -1;
}

void General::onDamaged(BaseFighter* source, int damageNum, bool bDoubleAttacked, CSkillResult* skillResult)
{
	BaseFighter::onDamaged(source, damageNum, bDoubleAttacked);

	// 武将受到伤害时，获取玩家id找到玩家，然后调用方法，显示玩家 和 武将的 绿色血条
	// 这里的pActor获取的指针是 玩家
	MyPlayer* pActor = dynamic_cast<MyPlayer*>(this->getGameScene()->getActor(BaseFighter::getMyPlayerId()));
	if(pActor != NULL && pActor->isMyPlayerGroup() && this->isMyPlayerGroup())
	{
		pActor->addGreenBlood();
	}

	//if(this->isMyPlayerGroup())
	//{
	//	float random = CCRANDOM_0_1();
	//	if(random <= GeneralsDialogData::s_chance_lowhp)
	//	{
	//		if(getActiveRole()->hp() < getActiveRole()->maxhp() * (GeneralsDialogData::s_lowhp_percent * 1.0f / 100))
	//		{
	//			showChat(GENERAL_DIAGLOG_LOWHP);
	//		}
	//	}
	//}
}

void General::onBorn()
{
	Monster::onBorn();

	// add name action
	CCNodeRGBA* name_node = CCNodeRGBA::create();
	name_node->setCascadeOpacityEnabled(true);

	CCSprite* spr = CCSprite::create("res_ui/generalname_di.png");
	spr->setAnchorPoint(ccp(0.5f,0.5f));
	name_node->addChild(spr);

	std::string general_namestr = this->getActorName();
	std::string general_namestr_nocolor = StrUtils::unApplyColor(general_namestr.c_str());

	CCLabelBMFont* skillNameLabel = CCLabelBMFont::create(general_namestr_nocolor.c_str(), "res_ui/font/ziti_3.fnt");
	skillNameLabel->setAnchorPoint(ccp(0.5f,0.9f));
	name_node->addChild(skillNameLabel);

	CCAction* spawn_action = CCSpawn::create(
		CCFadeOut::create(0.25f),
		CCScaleTo::create(0.25f, 2.0f),
		NULL);

	CCAction*  action = CCSequence::create(
		CCMoveBy::create(0.5f, ccp(0, 50)),
		CCDelayTime::create(0.5f),
		spawn_action,
		CCRemoveSelf::create(),
		NULL);
	name_node->runAction(action);

	name_node->setPosition(ccp(this->getPositionX(), this->getPositionY() + 76));
	this->getGameScene()->getActorLayer()->addChild(name_node, SCENE_TOP_LAYER_BASE_ZORDER);
	
	//add sound effect
	GameUtils::playGameSound(EFFECT_GENERAL, 2, false);

	// add effect on general's foot
	std::string str_anim_fuhuo = "animation/texiao/renwutexiao/FUHUO/fuhuo1.anm";
	CCLegendAnimation* pAnim = CCLegendAnimation::create(str_anim_fuhuo);
	this->addEffect(pAnim, false, 0);

	// add effect to indicate where is the general
	str_anim_fuhuo = "animation/texiao/renwutexiao/FUHUO/light_pillar.anm";
	pAnim = CCLegendAnimation::create(str_anim_fuhuo);
	pAnim->setPlayLoop(true);
	pAnim->setReleaseWhenStop(false);
	pAnim->setVisible(false);
	pAnim->runAction(CCSequence::create(
		CCDelayTime::create(1.0f),   // 1.26 is the duration of "fuhuo1.anm"
		CCShow::create(),
		CCDelayTime::create(3.2f),   // 1.2f is the duration of "light_pillar.anm"
		CCFadeOut::create(0.5f),
		CCRemoveSelf::create(),
		NULL));
	this->addEffect(pAnim, true, 0);

	//// more effect
	//std::string str_anim = "animation/texiao/renwutexiao/WJDLFZ/wjdlfz.png";
	//SkillProcessor::addMagicHaloToGround(this->getGameScene(), this->getWorldPosition(), str_anim, 2.0f, 1.0f, 1.2f, 0.6f);

	// chat bubble: born
	showChat(GENERAL_DIAGLOG_ENTER_FIGHT);

	//// show general's rank
	//std::vector<CGeneralBaseMsg *>& inLineList = GameView::getInstance()->generalsInLineList;
	//for (unsigned int i = 0; i<inLineList.size(); i++)
	//{
	//	if (this->getRoleId() == inLineList.at(i)->id())
	//	{
	//		addRank(inLineList.at(i)->evolution());
	//	}
	//}
}

void General::showActorName(bool bShow)
{
	std::string namestr = getActorName();
	const char* name = namestr.c_str();

	CCRichLabel* nameLabel = (CCRichLabel*)this->getChildByTag(GameActor::kTagActorName);
	if(bShow)
	{
		if(nameLabel == NULL)
		{
			//nameLabel = this->createNameLabel(name);

			nameLabel = CCRichLabel::createWithString(name, CCSizeMake(256,64),NULL,NULL);
			nameLabel->setTag(GameActor::kTagActorName);
			nameLabel->setAnchorPoint(ccp(0.5f,0.5f));
			
			// 设置名字位置时，先判断怪物头顶是否有血条
			TargetInfoMini* pBloodBar = (TargetInfoMini*)this->getChildByTag(kTagBloodBar);
			if(pBloodBar == NULL)
			{
				nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT));
			}
			else
			{
				nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT + 16));
			}

			this->addChild(nameLabel, GameActor::ACTOR_NAME_ZORDER);
		}
		else
		{
			nameLabel->setString(name);
		}
	}
	else
	{
		if(nameLabel != NULL)
			nameLabel->removeFromParent();
	}
}

void General::onAttack(const char* skillId, const char* skillProcessorId, long long targetId, bool bDoubleAttack, int skillType)
{
	BaseFighter::onAttack(skillId, skillProcessorId, targetId, bDoubleAttack, skillType);

	if(this->isMyPlayerGroup())
	{
		ArrowIndicator* pArrow = (ArrowIndicator*)this->getChildByTag(GameActor::kTagArrowIndicator);
		if(pArrow == NULL)
		{
			pArrow = ArrowIndicator::create(targetId, 10);
			addChild(pArrow, ACTOR_ARROW_INDICATOR_ZORDER, kTagArrowIndicator);
		}
		else
		{
			pArrow->setTarget(targetId, 5);
		}

		// chat bubble: attack
		float random = CCRANDOM_0_1();
		if(random <= GeneralsDialogData::s_chance_attack)
		{
			showChat(GENERAL_DIAGLOG_ATTACK);
		}

		if(BasePlayer::isDefaultAttack(skillId) || General::isNormalAttack(skillId))
			return;

		ActorUtils::addSkillName(this, skillId, skillProcessorId);

		// more dynamic effect
		// when the general is using skill, he will be bigger in a short duration
		CCFiniteTimeAction*  action = CCSequence::create(
			CCScaleTo::create(0.3f,1.4f), 
			CCDelayTime::create(1.7f),
			CCScaleTo::create(0.3f,1.0f),   // BASEFIGHTER_BASE_SCALE
			NULL);
		action->setTag(BASEFIGHTER_GROW_ACTION);
		this->getAnim()->runAction(action);
	}

	// 武将攻击时，获取玩家id找到玩家，然后调用方法，显示玩家 和 武将的 绿色血条
	// 这里的pActor获取的指针是 玩家
	MyPlayer* pActor = dynamic_cast<MyPlayer*>(this->getGameScene()->getActor(BaseFighter::getMyPlayerId()));
	if(pActor != NULL && pActor->isMyPlayerGroup() && this->isMyPlayerGroup())
	{
		pActor->addGreenBlood();
	}
}

void General::setGroupPosition(GroupPosition pos)
{
	m_GroupPosition = pos;
}

void General::followMaster(long long masterRoleId, int groupPosition)
{
	BaseFighterCommandFollow* followCommand = new BaseFighterCommandFollow();

	followCommand->targetRoleId = masterRoleId;

	setGroupPosition((GroupPosition)groupPosition);
	if(getGroupPosition() == General::general_grouppos_left) {
		followCommand->offset = ccp(-64, 64);
	} 
	else if (getGroupPosition() == General::general_grouppos_behind){
		followCommand->offset = ccp(-64, 0);
	}
	else if (getGroupPosition() == General::general_grouppos_right){
		followCommand->offset = ccp(-64, -64);
	}

	setNextCommand(followCommand, true);
}

void General::onRemoveFromGameScene()
{
	BasePlayer* pPlayer = dynamic_cast<BasePlayer*>(this->getGameScene()->getActor(this->m_ownerId));
	if(pPlayer != NULL)
	{
		pPlayer->updateAliveGeneralsList();

		//// remove the musou special effect
		//CCNode* actorLayer = this->getGameScene()->getActorLayer();
		//LinkSpecialEffect* effect = dynamic_cast<LinkSpecialEffect*>(actorLayer->getChildByTag((int)myplayer->getRoleId()));
		//if(effect != NULL)
		//{
		//	effect->removeFromParent();
		//}

		// 创建武将头像
		// change by yangjun 2014.3.12
// 			MainScene * mainScene = GameView::getInstance()->getMainUIScene();
// 			if (mainScene != NULL)
// 				mainScene->RefreshGeneral();
	}
}

bool General::isNormalAttack(const char* skillId)
{
	// Patch
	std::string id = skillId;

	if(id == "tongyongchongfeng1")   // warrior's charge skill
		return true;
	if(id == "tongyongtiao1")   // warlock's jump skill
		return true;

	return false;
}

std::string General::getDialogContent(long long roleId, int conditionIdx)
{
	std::vector<CGeneralBaseMsg *>& inLineList = GameView::getInstance()->generalsInLineList;
	for (unsigned int i = 0; i<inLineList.size(); i++)
	{
		if (roleId == inLineList.at(i)->id())
		{
			int modelId = inLineList.at(i)->modelid();
			// general's model id must bigger than 0
			if(modelId <= 0)
				continue;

			CGeneralBaseMsg * generalBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[modelId];
		
			// get the dialog text
			std::map<int, std::vector<std::string>*>* pGeneralDialogMap = GeneralsDialogData::s_generalsDialogData[modelId];
			CCAssert(conditionIdx >=0 && conditionIdx < GENERAL_DIAGLOG_CONDITION_MAX, "invalid idx");
			std::vector<std::string>* allDialogText = pGeneralDialogMap->at(conditionIdx);
			int size = allDialogText->size();
			if(size <= 0)
				return GENERAL_DIAGLOG_NULL_TEXT;

			int index = size * CCRANDOM_0_1();
			if(index >= size)
				index = size - 1;
			return allDialogText->at(index);
		}
	}

	return GENERAL_DIAGLOG_NULL_TEXT;
}

void General::showChat(int conditionIdx, float duration)
{
	// is showing chat bubble
	CCNode* container = this->getChildByTag(GameActor::kTagHeadEffectContainer);
	CCNode* flagNode = container->getChildByTag(GameActor::kTagChatBubble);
	if(flagNode != NULL)
		return;

	if(!m_bCanShowBubble)
		return;

	//const char* str = StringDataManager::getString("general_move");
	std::string str = this->getDialogContent(this->getRoleId(), conditionIdx);
	//CCLOG("chat: %s", str.c_str());
	if(str != GENERAL_DIAGLOG_NULL_TEXT)
		this->addChatBubble(str.c_str(), GeneralsDialogData::s_dialog_stay_duration / 1000.f, GENERAL_CHATBUBBLE_OFFSETY);

	// interval time
	m_bCanShowBubble = false;
	this->unschedule(schedule_selector(General::resetChatInterval));
	this->schedule( schedule_selector(General::resetChatInterval), GeneralsDialogData::s_dialog_interval_duration/1000.f, 1, 0); 
}

void General::resetChatInterval(float dt)
{
	m_bCanShowBubble = true;
}

void General::addRank(int nRank)
{
	if(nRank <= 0)
		return;

	// remove the old one
	CCNode* pRankNode = this->getChildByTag(GENERAL_RANK_TAG);
	if(pRankNode != NULL)
		pRankNode->removeFromParent();

	// create the new one
	const float RankSpriteScale = 0.7f;
	CCSprite* pRankSprite = ActorUtils::createGeneralRankSprite(nRank);
	pRankSprite->setScale(RankSpriteScale);
	pRankSprite->setAnchorPoint(ccp(0.5f, 0.5f));

	// get the actor's name position, and the set the rank's position
	CCNode* pActorName = this->getChildByTag(GameActor::kTagActorName);
	// clac y
	float y = pActorName->getPositionY();
	// calc x
	float x = pActorName->getPositionX() + pActorName->getContentSize().width/2 + pRankSprite->getContentSize().width*RankSpriteScale/2;
	pRankSprite->setPosition(ccp(x, y));

	pRankSprite->setTag(GENERAL_RANK_TAG);
	this->addChild(pRankSprite);
}

void General::addRareIcon(int nRareValue)
{
	if(nRareValue <= 0)
		return;

	// 1. create the rare ( star ) sprite
	const float RareSpriteScale = 0.65f;
	CCSprite * pRareSprite = NULL;
	pRareSprite = CCSprite::create(RecuriteActionItem::getStarPathByNum(nRareValue).c_str());
	pRareSprite->setAnchorPoint(ccp(0.5f,0.5f));
	pRareSprite->setScale(RareSpriteScale);

	/////////////////////////////////////////
	// 2. add the sprite
	// remove the old one
	CCNode* pRareNode = this->getChildByTag(GENERAL_RARE_TAG);
	if(pRareNode != NULL)
		pRareNode->removeFromParent();

	// get the actor's name position, and the set the rank's position
	CCNode* pActorName = this->getChildByTag(GameActor::kTagActorName);
	// clac y
	float y = pActorName->getPositionY();
	// calc x
	float x = pActorName->getPositionX() - pActorName->getContentSize().width/2 - pRareSprite->getContentSize().width*RareSpriteScale/2;
	pRareSprite->setPosition(ccp(x, y));

	pRareSprite->setTag(GENERAL_RARE_TAG);
	this->addChild(pRareSprite);
}

void General::addGreenBlood()
{
	// the green blood bar on the head
	MyPlayerInfoMini* pBloodBar = (MyPlayerInfoMini*)this->getChildByTag(kTagGreenBloodBar);
	if(pBloodBar == NULL)
	{
		pBloodBar = MyPlayerInfoMini::create(this);
		pBloodBar->setAnchorPoint(ccp(0.5f, 0.5f));
		pBloodBar->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT));
		this->addChild(pBloodBar, ACTOR_HEAD_ZORDER, kTagGreenBloodBar);
	}
	// refresh
	if(pBloodBar != NULL)
	{
		// refresh action
		pBloodBar->stopAllActions();
		CCAction* action = CCSequence::create(
			CCDelayTime::create(MYPLAYER_GREEN_BLOOD_DURATION),
			CCCallFunc::create(this, callfunc_selector(General::removeGreenBlood)),
			NULL);
		pBloodBar->runAction(action);

		// refresh info
		pBloodBar->ReloadTargetData(this);
	}

	// reset the postions
	// NAME label
	CCNode* nameLabel = this->getChildByTag(GameActor::kTagActorName);
	if(nameLabel != NULL)
	{
		CCLayer* layer_countDown = (CCLayer*)this->getChildByTag(kTagPresentCountDown);
		if(layer_countDown != NULL)
		{
			layer_countDown->setPosition(ccp(0,BASEFIGHTER_ROLE_HEIGHT));
			pBloodBar->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT+12));
			nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT + 28));
		}
		else
		{
			nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT + 16));
		}

		this->synchronizeHeadLabelPosition();
	}
}

void General::removeGreenBlood()
{
	// remove
	CCNode* pNode = this->getChildByTag(kTagGreenBloodBar);
	if(pNode != NULL)
	{
		pNode->removeFromParent();
	}

	// reset the postions
	// NAME label
	CCNode* nameLabel = this->getChildByTag(GameActor::kTagActorName);
	if(nameLabel != NULL)
	{
		CCLayer* layer_countDown = (CCLayer*)this->getChildByTag(kTagPresentCountDown);
		if(layer_countDown != NULL)
		{
			layer_countDown->setPosition(ccp(0,BASEFIGHTER_ROLE_HEIGHT));
			nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT + 16));
		}
		else
		{
			nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT));
		}

		this->synchronizeHeadLabelPosition();
	}
}

void General::addPresentCountDown()
{
	// red part
	CCLayer* layer_countDown = (CCLayer*)this->getChildByTag(kTagPresentCountDown);
	if(layer_countDown == NULL)
	{
		layer_countDown = CCLayer::create();
		layer_countDown->ignoreAnchorPointForPosition(false);
		layer_countDown->setAnchorPoint(ccp(0.5f, 0.5f));
		layer_countDown->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT));
		layer_countDown->setContentSize(CCSizeMake(112,14));
		this->addChild(layer_countDown, ACTOR_HEAD_ZORDER, kTagPresentCountDown);
		// Frame
		CCScale9Sprite * sprite_countDownFrame = CCScale9Sprite::create("gamescene_state/zhujiemian3/zhujuetouxiang/xuetiao_di.png");
		sprite_countDownFrame->setCapInsets(CCRectMake(7,7,1,1));
		sprite_countDownFrame->setContentSize(CCSizeMake(112,14));
		sprite_countDownFrame->setAnchorPoint(ccp(0,0));
		sprite_countDownFrame->setPosition(ccp(0,0));
		layer_countDown->addChild(sprite_countDownFrame);
		//
		CCSprite * sprite_countDown = CCSprite::create("gamescene_state/zhujiemian3/zhujuetouxiang/yellow_xuetiao.png");
		sprite_countDown->setAnchorPoint(ccp(0,0));
		sprite_countDown->setPosition(ccp(0,0));
		sprite_countDown->setTag(Sprite_CountDown_Tag);
		layer_countDown->addChild(sprite_countDown);

		layer_countDown->setScaleX(0.5f);
		layer_countDown->setScaleY(0.6f);
	}
	else
	{
		CCSprite * sprite_countDown = (CCSprite*)layer_countDown->getChildByTag(Sprite_CountDown_Tag);
		if (sprite_countDown)
		{
			CGeneralBaseMsg * temp = GeneralsStateManager::getInstance()->getGeneralBaseMsgById( this->getActiveRole()->rolebase().roleid());
			if (temp)
			{
				float blood_scale = temp->getRemainTime() * 1.0f / this->getActiveRole()->generalbaseinfo().showlefttime();
				if (blood_scale > 1.0f)
					blood_scale = 1.0f;

				float vl = 114*blood_scale*1.0f;
				sprite_countDown->setTextureRect(CCRectMake(0,0,vl,sprite_countDown->getContentSize().height));
			}
		}
	}

	// reset the postions
	
	// NAME label
	CCNode* nameLabel = this->getChildByTag(GameActor::kTagActorName);
	if(nameLabel != NULL)
	{
		CCNode* pNode = this->getChildByTag(kTagGreenBloodBar);
		if(pNode != NULL)
		{
			pNode->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT + 12));
			nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT + 28));
		}
		else
		{
			nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT + 16));
		}

		this->synchronizeHeadLabelPosition();
	}
}

void General::removePresentCountDown()
{
	// remove
	CCLayer* layer_countDown = (CCLayer*)this->getChildByTag(kTagPresentCountDown);
	if(layer_countDown != NULL)
	{
		layer_countDown->removeFromParent();
	}

	// reset the postions
	// NAME label
	CCNode* nameLabel = this->getChildByTag(GameActor::kTagActorName);
	if(nameLabel != NULL)
	{
		CCNode* pNode = this->getChildByTag(kTagGreenBloodBar);
		if(pNode != NULL)
		{
			pNode->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT));
			nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT + 16));
		}
		else
		{
			nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT));
		}

		nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT + 16));

		this->synchronizeHeadLabelPosition();
	}
}

