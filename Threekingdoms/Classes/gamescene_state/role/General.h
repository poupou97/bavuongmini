#ifndef _LEGEND_GAMEACTOR_GENERAL_H_
#define _LEGEND_GAMEACTOR_GENERAL_H_

#include "Monster.h"
#include "cocos2d.h"

#define GENERAL_FIGHT_MOVESPEED 250.f

#define GENERAL_DIAGLOG_GOSSIP 0
#define GENERAL_DIAGLOG_ATTACK 1
#define GENERAL_DIAGLOG_LOWHP 2
#define GENERAL_DIAGLOG_ENTER_FIGHT 3
#define GENERAL_DIAGLOG_CONDITION_MAX 4
#define GENERAL_DIAGLOG_NULL_TEXT "null"

/**
 * it's the following generals in this game
 * @author zhaogang
 * @version 0.1.1
 */
class General : public Monster
{
public:
	enum GroupPosition {
		general_grouppos_left = 1,
		general_grouppos_behind = 2,
		general_grouppos_right = 3,
		general_grouppos_longbehind = 4,
	};

public:
	General();
	virtual ~General();

	virtual void onEnter();
	virtual void onExit();

	virtual void update(float dt);

	virtual bool init(const char* actorName);

	inline void setOwnerId(long long ownerId) { m_ownerId = ownerId; };
	inline long long getOwnerId() { return m_ownerId; };

	virtual void onRemoveFromGameScene();

	virtual bool isMyPlayerGroup();
	virtual int getPosInGroup();

	virtual void onDamaged(BaseFighter* source, int damageNum, bool bDoubleAttacked, CSkillResult* skillResult = NULL);
	// override
	virtual void onAttack(const char* skillId, const char* skillProcessorId, long long targetId, bool bDoubleAttack, int skillType = -1);
	// override
	virtual void onBorn();

	virtual void showActorName(bool bShow);

	void setGroupPosition(GroupPosition pos);
	inline int getGroupPosition() { return m_GroupPosition; };

	/**
	 * follower will enter following state 
	 * @param master, who will be followed
	 * @param groupPosition, the relative position of the master when following
	 **/
	void followMaster(long long masterRoleId, int groupPosition);

	void showFlag(bool bShow);

	static bool isNormalAttack(const char* skillId);

	void addRank(int nRank);
	void addRareIcon(int nRareValue);

	void addGreenBlood();
	void removeGreenBlood();

	void addPresentCountDown();
	void removePresentCountDown();

private:
	std::string getDialogContent(long long roleId, int conditionIdx);
	void showChat(int conditionIdx, float duration = 2.0f);
	void resetChatInterval(float dt);

protected:
	virtual void updateWander();
	virtual void updateWorldPosition();

private:
	long long m_ownerId;

	GroupPosition m_GroupPosition;

	bool m_bCanShowBubble;
};

#endif