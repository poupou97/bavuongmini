#include "DecorationActor.h"

#include "../../legend_engine/CCLegendAnimation.h"
#include "../GameSceneState.h"

DecorationActor::DecorationActor()
{
	//this->scheduleUpdate();
}

DecorationActor::~DecorationActor()
{
	//CC_SAFE_RELEASE(m_pAnim);
}

void DecorationActor::loadWithInfo(LevelActorInfo* levelActorInfo)
{
	DecorationActorInfo* actorInfo = (DecorationActorInfo*)levelActorInfo;

	// 场景唯一id
	m_roleId = actorInfo->instanceId + SCENE_ACTOR_BASE_ID;

	// actorclass编号
	m_actorClassId = actorInfo->actorClassId;

	// x坐标
	m_worldPosition.x = actorInfo->posX;
	// y坐标
	m_worldPosition.y = (actorInfo->posY * 2);   // 由编辑器坐标转换为游戏世界坐标

	// 动作编号
	m_animActionID = actorInfo->animActionID;

	// FLAG属性
	unsigned char flag = actorInfo->flag;		
	// 翻转状态（0无反转，1有反转）
	bool bFlipX = flag & (1 << 0);
	if(bFlipX)
		this->setScaleX(-1.0f);
	// 可见性（0可见，1不可见）
	this->setVisible((flag & (1 << 3)) == 0);
	
	//参数
	int paramLength = actorInfo->paramLength;
	this->paramData = new short[paramLength];
	for (int i = 0; i < paramLength; i++) {
		this->paramData[i] = actorInfo->paramData[i];
	}

	// 获取layer值
	this->m_layerId = paramData[2];



	// load animation
	m_pAnim = new CCLegendAnimation();
	ActorClass* ac = GameMetaData::getActorClass(m_actorClassId);
	m_pAnim->initWithAnmFilename(ac->aniName.c_str());
	m_pAnim->setAction(m_animActionID);

	addChild(m_pAnim);
	m_pAnim->release();
}
