#ifndef _LEGEND_GAMEACTOR_ACTORCOMMAND_H_
#define _LEGEND_GAMEACTOR_ACTORCOMMAND_H_

#include "cocos2d.h"

USING_NS_CC;

class GameActor;

/**
 * actor要执行的命令，比如攻击、移动、采集、跟随、拾取道具、巡逻、
                          任务跑环、打怪(击杀一定数量的怪物)、收集宠物等等
 * @author zhaogang
 * @version 0.1.0
 */
class ActorCommand
{
public:
	// 命令类型
	enum Type
    {
        type_attack = 0,   // 攻击
        type_move = 1,     // 移动
        type_pick = 2,	   // 采集
        type_follow = 3,   // 跟随
        type_collectPresent =4, // 拾取宝箱
    };

	// 命令执行情况: new, doing, finished
	enum Status
    {
        status_new = 0,
        status_doing = 1,
        status_finished = 2,
		status_waiting = 3
    };

	enum CommandSource
	{
		source_from_player = 0,   // 玩家主动发起
		source_from_robot = 1   // 从挂机系统发起
	};

public:
	ActorCommand();
	virtual ~ActorCommand();

	virtual void Enter(GameActor* mainActor) {};
	virtual void Exit(GameActor* mainActor) {};
	virtual void Execute(GameActor* mainActor) {};

	ActorCommand* getNextCommand();
	// return the next command
	ActorCommand* setNextCommand(ActorCommand* nextCmd);
	bool hasNextCommand();

	virtual ActorCommand* clone();

	int getType();
	void setType(int type);

	virtual bool isSame(ActorCommand* otherCmd) { return false; };

	void setStatus(int status);
	int getStatus();
	bool isFinished();

	bool isWaitingAction;   // if waiting the animation is played over in the the previous command
	bool isFinishImmediately;

	int commandSource;   // 操作的来源：玩家自己，或者挂机模块

protected:
	ActorCommand* m_nextCmd;

private:
	int m_commandType;
	int m_status;
};

#endif