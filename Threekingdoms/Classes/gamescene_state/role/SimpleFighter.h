#ifndef _LEGEND_GAMEACTOR_SIMPLEFIGHTER_H_
#define _LEGEND_GAMEACTOR_SIMPLEFIGHTER_H_

#include "BaseFighter.h"
#include "cocos2d.h"

#define SIMPLEFIGHTER_ANI_STAND 0			
#define SIMPLEFIGHTER_ANI_RUN 1			
#define SIMPLEFIGHTER_ANI_ATTACK 2	
#define SIMPLEFIGHTER_ANI_DEATH 3	
#define SIMPLEFIGHTER_ANI_MAX 4

class CCLegendAnimation;
class GameActorAnimation;
class GameSceneLayer;

/**
 * 简单角色类
 * 目前，主要用于剧情模式
 * @author zhaogang
 * @date 2014/3/20
 */
class SimpleFighter : public BaseFighter
{
friend class SimpleFighterStand;
friend class SimpleFighterRun;
friend class SimpleFighterAttack;

public:
	SimpleFighter();
	SimpleFighter(GameSceneLayer* scene);
	virtual ~SimpleFighter();

	virtual void update(float dt);

	virtual void onEnter();
	virtual void onExit();

	virtual CCPoint getSortPoint();

	//an instance of the state machine class
	StateMachine<SimpleFighter>*  m_pStateMachine;
	StateMachine<SimpleFighter>*  GetFSM()const{return m_pStateMachine;}

	virtual bool init(const char* resPath, const char* actorName);

	virtual void changeAction(int action);
	virtual bool isAction(int action);

	virtual void showActorName(bool bShow);

protected:

};

#endif