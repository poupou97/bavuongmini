#include "SimpleFighterOwnedStates.h"

#include "SimpleFighter.h"
#include "GameActorAnimation.h"
#include "../../utils/GameUtils.h"
#include "BaseFighterOwnedCommand.h"
#include "../skill/GameFightSkill.h"
#include "../GameSceneState.h"
#include "BaseFighterOwnedStates.h"
#include "APPMacros.h"

//-----------------------------------------------------------------------Global state
SimpleFighterGlobalState* SimpleFighterGlobalState::Instance()
{
  static SimpleFighterGlobalState instance;

  return &instance;
}

void SimpleFighterGlobalState::Execute(SimpleFighter* self)
{

}

//---------------------------------------

SimpleFighterStand* SimpleFighterStand::Instance()
{
  static SimpleFighterStand instance;

  return &instance;
}

void SimpleFighterStand::Enter(SimpleFighter* self)
{
	self->changeAnimation(SIMPLEFIGHTER_ANI_STAND, self->getAnimDir(), true, true);
}

void SimpleFighterStand::Execute(SimpleFighter* self)
{
}

void SimpleFighterStand::Exit(SimpleFighter* self)
{
}


//------------------------------------------------------------------------
SimpleFighterRun* SimpleFighterRun::Instance()
{
  static SimpleFighterRun instance;

  return &instance;
}


void SimpleFighterRun::Enter(SimpleFighter* self)
{  
	CCPoint offset = GameUtils::getDirection(self->getWorldPosition(), *self->getPaths()->at(0));
	self->setMoveDir(ccp(offset.x, -offset.y));

	int animDirection = self->getAnimDirection(ccp(0, 0), self->getMoveDir(), DEFAULT_ANGLES_DIRECTION_DATA);
    //CCAssert(animDirection != -1, "invalid anim dir");
    self->setAnimDir(animDirection);

	self->changeAnimation(SIMPLEFIGHTER_ANI_RUN, self->getAnimDir(), true, true);
}


void SimpleFighterRun::Execute(SimpleFighter* self)
{
	if(self->getPaths()->size() <= 0)
	{
		//self->GetFSM()->ChangeState(OtherPlayerStand::Instance());
		//CCLOG("other player( %d ) will stop.", self->getRoleId());
	}
	else
	{
		// according by the move direction to set animation's action
		int animDirection = self->getAnimDirection(ccp(0, 0), self->getMoveDir(), DEFAULT_ANGLES_DIRECTION_DATA);
		//CCLog("run state, move dir: (%f,%f), anim dir: %f", dir.x, dir.y, animDirection);
		self->setAnimDir(animDirection);
		self->getAnim()->setAction(animDirection);
		self->getAnim()->setPlaySpeed(self->getMoveSpeed()/BASEFIGHTER_BASIC_MOVESPEED);
	}
}

void SimpleFighterRun::Exit(SimpleFighter* self)
{
	// restore animation play speed
	self->getAnim()->setPlaySpeed(1.0f);
}


//------------------------------------------------------------------------
SimpleFighterAttack* SimpleFighterAttack::Instance()
{
  static SimpleFighterAttack instance;

  return &instance;
}


void SimpleFighterAttack::Enter(SimpleFighter* self)
{
	self->changeAnimation(SIMPLEFIGHTER_ANI_ATTACK, self->getAnimDir(), false, true);
	self->getAnim()->setPlaySpeed(1.0f);
}

void SimpleFighterAttack::Execute(SimpleFighter* self)
{
	GameSceneLayer* scene = self->getGameScene();
	//CCLegendAnimation* attack = self->m_pAnimBody[ANI_ATTACK_PHY];
	//CCLegendAnimation* attackReflection = self->m_pAnimBodyReflection[ANI_ATTACK_PHY];
	CCLegendAnimation* attack = self->getAnim()->getAnim(ANI_COMPONENT_BODY, SIMPLEFIGHTER_ANI_ATTACK);
	CCLegendAnimation* attackReflection = self->getAnim()->getAnim(ANI_COMPONENT_REFLECTION, SIMPLEFIGHTER_ANI_ATTACK);

	// attack action is stop
	if(!attack->isPlaying())
	{
		self->changeAction(ACT_STAND);
	}
}

void SimpleFighterAttack::Exit(SimpleFighter* self)
{
	self->getAnim()->setPlaySpeed(1.0f);   // restore to default animation speed
}

//------------------------------------------------------------------------
SimpleFighterDeath* SimpleFighterDeath::Instance()
{
  static SimpleFighterDeath instance;

  return &instance;
}

void SimpleFighterDeath::Enter(SimpleFighter* self)
{
}

void SimpleFighterDeath::Execute(SimpleFighter* self)
{
}

void SimpleFighterDeath::Exit(SimpleFighter* self)
{
}
