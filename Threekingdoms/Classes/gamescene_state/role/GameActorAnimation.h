#ifndef _LEGEND_GAMEACTOR_ANIMATION_H_
#define _LEGEND_GAMEACTOR_ANIMATION_H_

#include <vector>

#include "cocos2d.h"
#include "../../legend_engine/CCLegendAnimation.h"

USING_NS_CC;

class GameActor;

/**
 * 复合动画Node
 * 角色动画一般都是由多个动画部件组成的，比如：身体、武器、武器特效、头发、倒影、坐骑等等
 * 该类负责对所有动画进行步调一致的设置，比如：alpha、color、Actoin、PlaySpeed、loop等
 * 其他接口和CCLegendAnimation保持一致，比如setAction() etc.
 * 未来，或许可以考虑引入异步加载的管理
 * @author zhaogang
 * @version 0.1.0
 */
class GameActorAnimation : public CCNodeRGBA
{
public:
	GameActorAnimation();
	virtual ~GameActorAnimation();

	static GameActorAnimation* create(const char* resPath, std::string* animList, const char* roleName, int componentSize, int animSize);

	/*
	* resPath, 资源路径
	* animList, 动画文件列表，包括run, attack, death etc.
	* roleName, 角色名称
	* componentSize, 组件数量
	* animSize, 动画数量
	*/
	bool init(const char* resPath, std::string* animList, const char* roleName, int componentSize, int animSize) ;
	
	void loadAnim(int componentIdx, int animIdx);
	CCLegendAnimation* getAnim(int componentIdx, int animIdx);

	inline int getComponentSize() { return mComponentSize; };
	inline int getAnimSize() { return mAnimSize; };

	void showComponent(int componentIdx, bool bVisible);

	// 设置动画( 跑，站立，攻击等动作，一个动作对应一个动画文件, *.anm）
	// 同一时刻，只有一个动画可用
	void setAnimName(int animName);
	int getAnimName();

	void setScale(float scale);

	void setPlaySpeed(float speed);

	void setPlayLoop(bool loop);
	void play();
	void stop();

	void setAction(int action);
	int getActionIdx();

private:
	CCLegendAnimation*** m_pAllAnims;

	std::string mResPath;
	std::string mRoleName;
	std::vector<std::string> mAnimNameList;
	int mComponentSize;
	int mAnimSize;

	std::vector<bool> mComponentVisible;

	int m_lastAnimName;
	int m_curAnimName;
	int m_actIdx;
};

#endif