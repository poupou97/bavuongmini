#ifndef _GAMESCENESTATE_SHOWBASEGENERAL_H_
#define _GAMESCENESTATE_SHOWBASEGENERAL_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CShowGeneralInfo;
class OtherPlayer;

// “展示”武将的基类
class ShowBaseGeneral: public CCObject
{
public:
	ShowBaseGeneral();
	~ShowBaseGeneral();

public:
	// 根据提供的参数，展示武将
	virtual void generalShow(CShowGeneralInfo* pShowGeneralInfo) {}

	// “展示”武将
	virtual void generalShow() {}

	// “收回”武将
	virtual void generalHide();

	// 判断“展示武将”是否可以展示
	virtual bool canGeneralShow() {return false;}

	virtual bool hasOnShowState(long long generalId);									// 是否处于“展示”状态
	
	// 此方法用于返回武将列表“展示 or 收回”状态
	virtual int getShowState(long long generalId);										// 1:均展示；0：收回；2：展示（按钮隐藏）

	// 清除所有数据
	virtual void clearAllData();

protected:
	virtual void callBackOnBorn(CCObject * pSender, void * pActor) {}

	// 从主角的协议字段初始化“展示武将”
	virtual void initGeneralData() {}

protected:
	int m_nActionType;			// 展示动作 1：展示 0：取消 2:进化或者传功
	long long m_longGeneralId;	// 武将Id
	int m_nTemplateId;			// 武将模板id
	int m_nCurrentQuality;		// 当前品质
	int m_nEvolution;			// 进化等级
	
public:
	void set_actionType(int nActionType);
	int get_actionType();

	void set_generalId(long long longGeneralId);
	long long get_generalId();

	void set_templateId(int nTemplateId);
	int get_templateId();

	void set_currentQuality(int nCurrentQuality);
	int get_currentQuality();

	void set_evolution(int nEvolution);
	int get_evolution();

	enum
	{
		HIDE_ACTIONTYPE = 0,
		SHOW_ACTIONTYPE = 1,
		OTHER_ACTIONTYPE = 2
	};
};

/////////////////////////////////////////////////////////////////////////////

class ShowMyGeneral : public ShowBaseGeneral
{
private:
	ShowMyGeneral();
	~ShowMyGeneral();

public:
	static ShowMyGeneral * s_showMyGeneral;
	static ShowMyGeneral * getInstance();

public:
	// 根据提供的参数，展示武将
	virtual void generalShow(CShowGeneralInfo* pShowGeneralInfo);

	// “展示”武将
	virtual void generalShow();

	// 判断“展示武将”是否可以展示
	virtual bool canGeneralShow();

protected:
	virtual void callBackOnBorn(CCObject * pSender, void * pActor);

	// 从主角的协议字段初始化“展示武将”
	virtual void initGeneralData();
};


/////////////////////////////////////////////////////////////////////////////

class ShowOtherGeneral : public ShowBaseGeneral
{
public:
	ShowOtherGeneral();
	~ShowOtherGeneral();

public:
	// 根据提供的参数，展示武将
	virtual void generalShow(CShowGeneralInfo* pShowGeneralInfo);

	// “展示”武将
	virtual void generalShow();

	// 判断“展示武将”是否可以展示
	virtual bool canGeneralShow();

protected:
	virtual void callBackOnBorn(CCObject * pSender, void * pActor);

private:
	OtherPlayer* m_pOtherPlayer;

public:
	void set_otherPlayer(OtherPlayer* pOtherPlayer);
};

#endif

