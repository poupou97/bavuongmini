#include "MonsterOwnedStates.h"

#include "Monster.h"
#include "GameActorAnimation.h"
#include "../../utils/GameUtils.h"
#include "BaseFighterOwnedCommand.h"
#include "../skill/GameFightSkill.h"
#include "../GameSceneState.h"
#include "BaseFighterOwnedStates.h"
#include "APPMacros.h"
#include "SimpleActor.h"
#include "../../legend_engine/LegendAAnimation.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../../newcomerstory/NewCommerStoryManager.h"
#include "BaseFighterConstant.h"

#define BASIC_MONSTER_MOVESPEED 200.f

//-----------------------------------------------------------------------Global state
MonsterGlobalState* MonsterGlobalState::Instance()
{
  static MonsterGlobalState instance;

  return &instance;
}

void MonsterGlobalState::Execute(Monster* self)
{

}

//---------------------------------------

MonsterStand* MonsterStand::Instance()
{
  static MonsterStand instance;

  return &instance;
}

void MonsterStand::Enter(Monster* self)
{
	// update the display status of the role
	self->updateWaterStatus();
	self->updateCoverStatus();

	self->changeAnimation(MONSTER_ANI_STAND, self->getAnimDir(), true, true);

	// if the actor's anim is doing ACTION, do not reset position
	if(self->getActionByTag(BASEFIGHTER_FLYUP_ACTION) == NULL)
		self->getAnim()->setPosition(ccp(0,0));
	if(self->getActionByTag(BASEFIGHTER_HITBACK_ACTION) == NULL)
		self->getAnim()->setRotation(0);

	ActionStandContext* context = (ActionStandContext*)self->getActionContext(ACT_STAND);
	context->idleStartTime = GameUtils::millisecondNow();
}

void MonsterStand::Execute(Monster* self)
{
	self->updateWander();
	self->updateWorldPosition();

	// if this actor stand on the collide grid, modify it's position to valid position manually
	GameSceneLayer* scene = self->getGameScene();
	CCPoint curWorldPos = self->getWorldPosition();
	int tileX = GameSceneLayer::positionToTileX(curWorldPos.x);
	int tileY = GameSceneLayer::positionToTileY(curWorldPos.y);
	if(scene->isLimitOnGround(tileX, tileY))
	{
		CCPoint validWorldPos = scene->getNearReachablePoint(curWorldPos);
		if(validWorldPos.x == -1 && validWorldPos.y == -1)
		{
			//CCAssert(false, "invalid position");
		}
		else
		{
			CCPoint cocosPos = scene->convertToCocos2DSpace(validWorldPos);
			self->setPositionImmediately(cocosPos);
			self->setWorldPosition(validWorldPos);
		}
	}
}

void MonsterStand::Exit(Monster* self)
{
}


//------------------------------------------------------------------------
MonsterRun* MonsterRun::Instance()
{
  static MonsterRun instance;

  return &instance;
}


void MonsterRun::Enter(Monster* self)
{  
	// confirm the direction
	CCPoint offset = GameUtils::getDirection(self->getWorldPosition(), *self->getPaths()->at(0));
	self->setMoveDir(ccp(offset.x, -offset.y));
	int animDirection = self->getAnimDirection(ccp(0, 0), self->getMoveDir(), DEFAULT_ANGLES_DIRECTION_DATA);
    //CCAssert(animDirection != -1, "invalid anim dir");
    self->setAnimDir(animDirection);

	self->changeAnimation(MONSTER_ANI_RUN, self->getAnimDir(), true, true);

	// if the actor's anim is doing ACTION, do not reset position
	if(self->getActionByTag(BASEFIGHTER_FLYUP_ACTION) == NULL)
		self->getAnim()->setPosition(ccp(0,0));
	if(self->getActionByTag(BASEFIGHTER_HITBACK_ACTION) == NULL)
		self->getAnim()->setRotation(0);
}


void MonsterRun::Execute(Monster* self)
{
	if(self->getPaths()->size() <= 0)
	{
		//self->GetFSM()->ChangeState(OtherPlayerStand::Instance());
		//CCLOG("other player( %d ) will stop.", self->getRoleId());
	}
	else
	{
		// according by the move direction to set animation's action
		int animDirection = self->getAnimDirection(ccp(0, 0), self->getMoveDir(), DEFAULT_ANGLES_DIRECTION_DATA);
		//CCLog("run state, move dir: (%f,%f), anim dir: %f", dir.x, dir.y, animDirection);
		self->setAnimDir(animDirection);
		self->getAnim()->setAction(animDirection);
		self->getAnim()->setPlaySpeed(self->getMoveSpeed()/BASIC_MONSTER_MOVESPEED);
	}
}

void MonsterRun::Exit(Monster* self)
{
	// restore animation play speed
	self->getAnim()->setPlaySpeed(1.0f);
}


//------------------------------------------------------------------------
MonsterAttack* MonsterAttack::Instance()
{
  static MonsterAttack instance;

  return &instance;
}


void MonsterAttack::Enter(Monster* self)
{
	GameSceneLayer* scene = self->getGameScene();

	// init skill behaviour
	BaseFighterCommandAttack* cmd = dynamic_cast<BaseFighterCommandAttack*>(self->getCommand());
	if(cmd != NULL)
	{
		// attack direction
		GameActor* cmdTarget = scene->getActor(cmd->targetId);
		if(cmdTarget != NULL)
		{
			int animDirection = self->getAnimDirection(scene->convertToCocos2DWorldSpace(self->getWorldPosition()), 
				scene->convertToCocos2DWorldSpace(cmdTarget->getWorldPosition()), DEFAULT_ANGLES_DIRECTION_DATA);
			self->setAnimDir(animDirection);
		}
	}

	self->changeAnimation(MONSTER_ANI_ATTACK, self->getAnimDir(), false, true);
	self->getAnim()->setPlaySpeed(cmd->attackSpeed);

	// if the actor's anim is doing ACTION, do not reset position
	if(self->getActionByTag(BASEFIGHTER_FLYUP_ACTION) == NULL)
		self->getAnim()->setPosition(ccp(0,0));
	if(self->getActionByTag(BASEFIGHTER_HITBACK_ACTION) == NULL)
		self->getAnim()->setRotation(0);

	BaseFighter* bf = dynamic_cast<BaseFighter*>(scene->getActor(cmd->targetId));

	CCPoint skillPosition = ActorUtils::getDefaultSkillPosition(self, cmd->skillId.c_str(), cmd->skillProcessorId.c_str());
	self->onSkillBegan(cmd->skillId.c_str(), cmd->skillProcessorId.c_str(), bf, skillPosition.x, skillPosition.y);

	// 为改善技能释放，展示不及时的问题
	// 1. 在收到1202消息后，其他玩家立即调用onSkillReleased()
	// release表示伤害的时间节点
	// 2. 玩家自己在release时，才sendReq1201
	self->onSkillReleased(cmd->skillId.c_str(), cmd->skillProcessorId.c_str(), bf, 0, 0);
}

void MonsterAttack::Execute(Monster* self)
{
	GameSceneLayer* scene = self->getGameScene();
	//CCLegendAnimation* attack = self->m_pAnimBody[ANI_ATTACK_PHY];
	//CCLegendAnimation* attackReflection = self->m_pAnimBodyReflection[ANI_ATTACK_PHY];
	CCLegendAnimation* attack = self->getAnim()->getAnim(ANI_COMPONENT_BODY, MONSTER_ANI_ATTACK);
	CCLegendAnimation* attackReflection = self->getAnim()->getAnim(ANI_COMPONENT_REFLECTION, MONSTER_ANI_ATTACK);

	BaseFighterCommandAttack* cmd = (BaseFighterCommandAttack*)self->getCommand();

	//if(attack->getPlayPercent() >= (m_nReleasePhase / 100.f) && !m_bIsSkillReleased)
	//{
	//	//BaseFighter* bf = dynamic_cast<BaseFighter*>(scene->getActor(cmd->targetId));
	//	////bf = (BaseFighter*)self->getLockedActor();
	//	//self->onSkillReleased(cmd->skillId.c_str(), bf, 0, 0);

	//	m_bIsSkillReleased = true;
	//}

	// attack action is stop
	if(!attack->isPlaying())
	{
		if(cmd != NULL)
		{
			cmd->attackCount++;
		}
	}
}

void MonsterAttack::Exit(Monster* self)
{
	self->getAnim()->setPlaySpeed(1.0f);   // restore to default animation speed
}

//------------------------------------------------------------------------
MonsterDeath* MonsterDeath::Instance()
{
  static MonsterDeath instance;

  return &instance;
}

// flying
#define DEATH_FLYING_DELAY_TIME 0.2f
#define DEATH_FLYING_DURATION 0.35f   // 0.15f
#define DEATH_FLYING_BOUNCE_DURATION 0.15f   // 死亡后，在地上弹2次
// fell to ground
#define DEATH_FELL_FELLTOGROUND_TIME 0.35f
// common
#define DEATH_ONGROUND_DURATION 0.6f
#define DEATH_FADEOUT_DURATION 0.5f
#define DEATH_MAX_TIME 15.f

void MonsterDeath::initDeathAction(ActionDeathContext* context, Monster* self)
{
	// supply some different death actions
	if(context->deathType == 0)
	{
		/*
		CCNode* shadow = self->getChildByTag(BaseFighter::kTagActorShadow);
		shadow->setVisible(false);

		self->getAnim()->setVisible(false);
		*/

		// death animation
		self->m_deathEffect = CCParticleSystemQuad::create("animation/texiao/renwutexiao/death/xuedi.plist");
		self->m_deathEffect->setPositionType(kCCPositionTypeGrouped);
		self->m_deathEffect->setPosition(ccp(0,0));
		self->m_deathEffect->setScale(2.0f);
		self->m_deathEffect->setVisible(true);
		self->m_deathEffect->setAutoRemoveOnFinish(true);
		//self->m_deathEffect->stopSystem();
		self->addChild(self->m_deathEffect, GameActor::ACTOR_HEAD_ZORDER);

		self->getAnim()->runAction(
			CCSpawn::create(
				CCScaleTo::create(0.5f, 0.62f, 1.56f), 
				CCFadeOut::create(0.8f), 
				NULL)
		);
	}
	else if(context->deathType == 1)   // flying and die
	{
		bool flyingToRight = true;
		CCPoint attackWorldPosition;

		// check attacker, if can not find it, random the flying direction
		BaseFighter* attacker = dynamic_cast<BaseFighter*>(self->getGameScene()->getActor(context->attackerRoleId));
		if(attacker == NULL)
		{
			// simulate the attacker's position
			attackWorldPosition = self->getWorldPosition();
			float random = CCRANDOM_0_1();
			if(random <= 0.5f)
				attackWorldPosition.x -= 50;
			else
				attackWorldPosition.x += 50;
		}
		else
		{
			attackWorldPosition = attacker->getWorldPosition();
		}

		// adjust the animation's direction and rotation by attacker and this monster's position
		if(attackWorldPosition.x >=  self->getWorldPosition().x)
		{
			flyingToRight = false;
		}
		else
		{
			flyingToRight = true;
		}

		float targetAngle = flyingToRight ? 90.f : -90.f ;

		int animDir = flyingToRight ? 1 : 3;
		self->setAnimDir(animDir);

		self->changeAnimation(MONSTER_ANI_DEATH, self->getAnimDir(), false, false);

		// get flying offset
		CCPoint dir = GameUtils::getDirection(attackWorldPosition, self->getWorldPosition());
		// check if the target is barrier, if true, try to modify the target to reachable point
		float distance = 32*6;
		CCPoint offset = ccpMult(dir, distance);
		CCPoint targetPos = ccpAdd(self->getWorldPosition(), offset);
		GameSceneLayer* scene = self->getGameScene();
		int tileX = scene->positionToTileX(targetPos.x);
		int tileY = scene->positionToTileY(targetPos.y);
		if(scene->isLimitOnGround(tileX, tileY))
		{
			CCPoint* newTarget = scene->findNearestPositionToTarget(self->getWorldPosition().x, self->getWorldPosition().y, targetPos.x, targetPos.y);
			if(newTarget != NULL)
			{
				distance = ccpDistance(*newTarget, self->getWorldPosition()) - 32;
				distance = (distance > 0) ? distance : 0;
				offset = ccpMult(dir, distance);

				delete newTarget;
			}
			else
			{
				offset.setPoint(0, 0);
			}
		}
		offset.y /= 2;
		offset.y = -offset.y;

		// i am dead, flying...
		CCFiniteTimeAction* action1 = CCSequence::create(
			CCDelayTime::create(DEATH_FLYING_DELAY_TIME),
			CCRotateBy::create(DEATH_FLYING_DURATION, targetAngle),
			NULL);
		self->getAnim()->runAction(action1);

		// flying
		CCFiniteTimeAction* action2 = CCSequence::create(
			CCDelayTime::create(DEATH_FLYING_DELAY_TIME),
			CCMoveBy::create(DEATH_FLYING_DURATION, offset),   // ccp(128, 0)
			NULL);
		self->runAction(action2);

		CCAction* action3 = CCSequence::create(
			CCDelayTime::create(DEATH_FLYING_DELAY_TIME),
			CCJumpBy::create(DEATH_FLYING_DURATION, CCPointZero, 200, 1),
			NULL);
		self->getAnim()->runAction(action3);

		// bounce again
		CCFiniteTimeAction* action4 = CCSequence::create(
			CCDelayTime::create(DEATH_FLYING_DELAY_TIME+DEATH_FLYING_DURATION),
			CCMoveBy::create(DEATH_FLYING_BOUNCE_DURATION, offset/4),   // ccp(128, 0)
			CCDelayTime::create(DEATH_ONGROUND_DURATION),
			CCFadeOut::create(DEATH_FADEOUT_DURATION),
			NULL);
		self->runAction(action4);
		CCFiniteTimeAction* action5 = CCSequence::create(
			CCDelayTime::create(DEATH_FLYING_DELAY_TIME+DEATH_FLYING_DURATION),
			CCJumpBy::create(DEATH_FLYING_BOUNCE_DURATION, CCPointZero, 80, 1),
			NULL);
		self->getAnim()->runAction(action5);

		context->toGroundTime = DEATH_FLYING_DELAY_TIME + DEATH_FLYING_DURATION;
		context->totalTime = DEATH_FLYING_DELAY_TIME + DEATH_FLYING_DURATION + DEATH_FLYING_BOUNCE_DURATION + DEATH_ONGROUND_DURATION + DEATH_FADEOUT_DURATION;
	}
	else if(context->deathType == 2)   // knockback and fell to the ground
	{
		bool flyingToRight = true;
		CCPoint attackWorldPosition;

		// check attacker, if can not find it, random the flying direction
		BaseFighter* attacker = dynamic_cast<BaseFighter*>(self->getGameScene()->getActor(context->attackerRoleId));
		if(attacker == NULL)
		{
			// simulate the attacker's position
			attackWorldPosition = self->getWorldPosition();
			float random = CCRANDOM_0_1();
			if(random <= 0.5f)
				attackWorldPosition.x -= 50;
			else
				attackWorldPosition.x += 50;
		}
		else
		{
			attackWorldPosition = attacker->getWorldPosition();
		}

		// adjust the animation's direction and rotation by attacker and this monster's position
		if(attackWorldPosition.x >=  self->getWorldPosition().x)
		{
			flyingToRight = false;
		}
		else
		{
			flyingToRight = true;
		}

		float targetAngle = flyingToRight ? 90.f : -90.f ;

		int animDir = flyingToRight ? 1 : 3;
		self->setAnimDir(animDir);
		self->changeAnimation(MONSTER_ANI_DEATH, self->getAnimDir(), false, false);

		// get flying offset
		CCPoint dir = GameUtils::getDirection(attackWorldPosition, self->getWorldPosition());
		CCPoint offset = ccpMult(dir, 32 * 9);
		offset.y /= 2;
		offset.y = -offset.y;

		CCFiniteTimeAction* action1 = CCSpawn::create(
				CCRotateBy::create(DEATH_FELL_FELLTOGROUND_TIME, targetAngle),
				NULL);
		self->getAnim()->runAction(action1);

		CCFiniteTimeAction* action2 = CCSequence::create(
				CCMoveBy::create(DEATH_FELL_FELLTOGROUND_TIME, offset),   // ccp(128, 0)
				CCDelayTime::create(DEATH_ONGROUND_DURATION),
				CCFadeOut::create(DEATH_FADEOUT_DURATION),
				NULL);
		self->runAction(action2);

		context->toGroundTime = DEATH_FELL_FELLTOGROUND_TIME;
		context->totalTime = DEATH_FELL_FELLTOGROUND_TIME + DEATH_ONGROUND_DURATION + DEATH_FADEOUT_DURATION;
	}

	context->startTime = GameUtils::millisecondNow();
	context->deathState = ActionDeathContext::death_state_update;
}
void MonsterDeath::updateDeathAction(ActionDeathContext* context, Monster* self)
{
	if(context->deathType == 0)
	{
		//CCLegendAnimation* death = self->m_DeathAnim;
		//// animation is over
		//if(!death->isPlaying())
		//{
		//	self->setDestroy(true);
		//}

		// particle effect
		//if(!self->m_deathEffect->isActive())
		//if(self->m_deathEffect->getParticleCount() == 0)
		if(GameUtils::millisecondNow() - context->startTime > 1.0f * 1000)
		{
			self->setDestroy(true);
		}
	}
	else if(context->deathType == 1 || context->deathType == 2)
	{
		if((GameUtils::millisecondNow() - context->startTime) > (context->toGroundTime * 1000))
		{
			if(!context->bAddDustEffect)
			{
				CCParticleSystem* particleEffect = CCParticleSystemQuad::create("animation/texiao/particledesigner/dust1.plist");
				particleEffect->setPositionType(kCCPositionTypeGrouped);
				particleEffect->setPosition(ccp(0.5f,0.5f));
				particleEffect->setAnchorPoint(ccp(0.5f,0.5f));
				particleEffect->setScaleX(1.2f);
				particleEffect->setScaleY(0.84f);
				particleEffect->setStartSize(150.f);
				//particleEffect->setLife(1.5f);
				//particleEffect->setLifeVar(0);
				particleEffect->setAutoRemoveOnFinish(true);
				self->addChild(particleEffect, GameActor::ACTOR_SHADOW_ZORDER);

				context->bAddDustEffect = true;
			}
		}

		float elapseTime = GameUtils::millisecondNow() - context->startTime;
		if((elapseTime > (context->totalTime * 1000) && self->numberOfRunningActions() == 0)
			|| elapseTime > DEATH_MAX_TIME * 1000)
		{
			self->setDestroy(true);
		}
	}
}

void MonsterDeath::Enter(Monster* self)
{
	ActionDeathContext* context = (ActionDeathContext*)self->getActionContext(ACT_DIE);
	self->addDeadStatus();

	//add by yangjun 2014.10.13
	if (NewCommerStoryManager::getInstance()->IsNewComer())
	{
		if (self->isFirstDead())
		{
			if (NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.size()<=0)
			{
				NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.insert(make_pair(self->getActiveRole()->templateid(),1));
			}
			else
			{
				bool isExist = false;
				for (std::map<long long,int>::iterator it_all = NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.begin() ; it_all != NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.end(); ++ it_all )
				{
					long long templateId_all  = it_all->first;
					int num_all = it_all->second;
					if (self->getActiveRole()->templateid() == templateId_all)
					{
						isExist = true;
						it_all->second++;
						break;
					}
				}

				if (!isExist)
				{
					NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.insert(make_pair(self->getActiveRole()->templateid(),1));
				}
			}
		}
	}

	//this->initDeathAction(context, self);
	if(self->getActionByTag(BASEFIGHTER_FLYUP_ACTION) != NULL)
	{
		context->deathType = 0;
	}

	self->clear();

	// 在被击倒、击飞的过程中，不显示阴影和倒影，否则会显得比较怪
	if(context->deathType == 1 || context->deathType == 2) 
	{
		self->getAnim()->showComponent(ANI_COMPONENT_REFLECTION, false);
#if BASEFIGHTER_SHADOW_ENABLED
		self->getAnim()->showComponent(ANI_COMPONENT_SHADOW, false);
#endif
	}

	// remove danger circle
	self->removeDangerCircle();
}

void MonsterDeath::Execute(Monster* self)
{
	ActionDeathContext* context = (ActionDeathContext*)self->getActionContext(ACT_DIE);

	if(context->deathState == ActionDeathContext::death_state_wait)
	{
		if(self->getActionByTag(BASEFIGHTER_FLYUP_ACTION) == NULL)
		{
			if(self->getAnim()->getActionByTag(BASEFIGHTER_SHAKE_ACTION) != NULL)
				self->getAnim()->stopActionByTag(BASEFIGHTER_SHAKE_ACTION);

			if(self->getAnim()->getActionByTag(BASEFIGHTER_QUAKE_ACTION) != NULL)
				self->getAnim()->stopActionByTag(BASEFIGHTER_QUAKE_ACTION);

			if(self->getAnim()->getActionByTag(BASEFIGHTER_GROW_ACTION) != NULL)
			{
				self->getAnim()->stopActionByTag(BASEFIGHTER_GROW_ACTION);
				//self->getAnim()->setScale(1.0f);   // restore
				// restore to normal size
				CCFiniteTimeAction*  action = CCSequence::create(
					CCScaleTo::create(0.01f,1.0f),
					NULL);
				self->getAnim()->runAction(action);
			}

			initDeathAction(context, self);
		}
	}
	else if(context->deathState == ActionDeathContext::death_state_update)
	{
		updateDeathAction(context, self);
	}
}

void MonsterDeath::Exit(Monster* self)
{

}

//------------------------------------------------------------------------
MonsterKnockBack* MonsterKnockBack::Instance()
{
  static MonsterKnockBack instance;

  return &instance;
}

void MonsterKnockBack::Enter(Monster* self)
{  
	self->changeAnimation(MONSTER_ANI_STAND, self->getAnimDir(), false, false);

	self->clearPath();

	//if(!self->isMyPlayer() && self->getType() == GameActor::type_monster)
	//	self->getGameScene()->addComboEffect();
}

void MonsterKnockBack::Execute(Monster* self)
{
	ActionBeKnockedBackContext* context = (ActionBeKnockedBackContext*)self->getActionContext(ACT_BE_KNOCKBACK);
	if(GameUtils::millisecondNow() - context->startTime > context->beKnockedBackDuration * 1000)
	{
		if(context->bReturnBack)
		{
			if(self->isDead())
				return;

			self->changeAction(ACT_STAND);

			BaseFighterCommandMove* cmd = new BaseFighterCommandMove();
			//cmd->moveSpeed = 300.0f;
			cmd->targetPosition = ccp(self->getRealWorldPosition().x, self->getRealWorldPosition().y);
			self->setNextCommand(cmd, false);
		}
	}
}

void MonsterKnockBack::Exit(Monster* self)
{

}

//------------------------------------------------------------------------
MonsterCharge* MonsterCharge::Instance()
{
  static MonsterCharge instance;

  return &instance;
}

void MonsterCharge::Enter(Monster* self)
{
	self->changeAnimation(MONSTER_ANI_DEATH, self->getAnimDir(), true, true);

	ActionChargeContext* context = (ActionChargeContext*)self->getActionContext(ACT_CHARGE);

	// adjust the direction
	CCPoint targetWorldPosition = self->getGameScene()->convertToGameWorldSpace(context->chargetTargetPosition);
	CCPoint offset = GameUtils::getDirection(self->getWorldPosition(), targetWorldPosition);
	int animDirection = self->getAnimDirection(ccp(0, 0), ccp(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);
	//CCAssert(animDirection != -1, "invalid anim dir");
	self->setAnimDir(animDirection);
	self->getAnim()->setAction(animDirection);

	// 获取动画播放速度
	self->getAnim()->setPlaySpeed(context->chargeMoveSpeed/BASIC_MONSTER_MOVESPEED);

	//CCMoveTo
	CCFiniteTimeAction*  action = CCSequence::create(
		CCMoveTo::create(context->chargeTime, context->chargetTargetPosition),
		NULL);
	self->runAction(action);

	self->clearPath();
}

void MonsterCharge::Execute(Monster* self)
{
	ActionChargeContext* context = (ActionChargeContext*)self->getActionContext(ACT_CHARGE);
	if(context->finished)
		return;

	//if(self->getPosition().equals( context->chargetTargetPosition) )   // using position to decide is not reliable
	if(GameUtils::millisecondNow() - context->startTime >= context->chargeTime * 1000)
	{
		context->finished = true;
	}
}

void MonsterCharge::Exit(Monster* self)
{
	// restore animation play speed
	self->getAnim()->setPlaySpeed(1.0f);
}

//------------------------------------------------------------------------
MonsterJump* MonsterJump::Instance()
{
  static MonsterJump instance;

  return &instance;
}

void MonsterJump::Enter(Monster* self)
{
	BaseFighterJump::setJumpAnimId(MONSTER_ANI_ATTACK);
	BaseFighterJump::Enter(self);   // common handler for the jump action
}

void MonsterJump::Execute(Monster* self)
{
	BaseFighterJump::Execute(self);   // common handler for the jump action
}

void MonsterJump::Exit(Monster* self)
{
	BaseFighterJump::Exit(self);   // common handler for the jump action
}

//------------------------------------------------------------------------
MonsterDisappear* MonsterDisappear::Instance()
{
  static MonsterDisappear instance;

  return &instance;
}

void MonsterDisappear::Enter(Monster* self)
{
	BaseFighterDisappear::Enter(self);   // common handler for the jump action
}

void MonsterDisappear::Execute(Monster* self)
{
	BaseFighterDisappear::Execute(self);   // common handler for the jump action
}

void MonsterDisappear::Exit(Monster* self)
{
	BaseFighterDisappear::Exit(self);   // common handler for the jump action
}