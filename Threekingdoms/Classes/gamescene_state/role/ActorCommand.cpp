#include "ActorCommand.h"

#include "GameActor.h"

ActorCommand::ActorCommand()
: m_status(status_new)
, isFinishImmediately(false)
, isWaitingAction(false)
, m_nextCmd(NULL)
, commandSource(source_from_player)
{
}

ActorCommand::~ActorCommand()
{
	CC_SAFE_DELETE(m_nextCmd);
}

bool ActorCommand::isFinished()
{
	if(m_status == status_finished)
		return true;

	return false;
}

void ActorCommand::setStatus(int status)
{
	m_status = status;
}
int ActorCommand::getStatus()
{
	return m_status;
}

int ActorCommand::getType()
{
	return m_commandType;
}
void ActorCommand::setType(int type)
{
	m_commandType = type;
}

ActorCommand* ActorCommand::setNextCommand(ActorCommand* nextCmd) 
{
	CCAssert(m_nextCmd == NULL, "already set next command, do not re-set it");
	m_nextCmd = nextCmd; 
	return m_nextCmd;
}
ActorCommand* ActorCommand::getNextCommand()
{
	return m_nextCmd;
}
bool ActorCommand::hasNextCommand()
{
	return m_nextCmd != NULL;
}

ActorCommand* ActorCommand::clone()
{
	CCAssert(false, "if sub class use clone(), it must implement it!!!");
	return NULL;
}