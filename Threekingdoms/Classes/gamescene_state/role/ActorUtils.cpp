#include "ActorUtils.h"

#include "GameActorAnimation.h"
#include "BaseFighter.h"
#include "BasePlayer.h"
#include "../../messageclient/element/CActiveRole.h"
#include "AppMacros.h"
#include "../skill/GameFightSkill.h"
#include "../GameSceneState.h"
#include "BaseFighterConstant.h"
#include "WeaponEffect.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/MapWeapEffect.h"
#include "../../messageclient/element/CFightWayBase.h"
#include "../../GameView.h"
#include "../../utils/GameUtils.h"
#include "../SimpleEffectManager.h"
#include "MyPlayer.h"

#define BASEPLAYER_STRATEGY_EFFECT_TAG 1021
#define FLOWERPARTICLEONROLE_ACTORUTILS_TAG  555

static std::string standActionNameList[] = { "stand" }; 
GameActorAnimation* ActorUtils::createActorAnimation(const char* figureName, const char* weaponName, const char* weaponEffectFireName, const char* weaponEffectSparkleName)
{
	std::string str_figureName = figureName;
	std::string name = str_figureName.substr(0,5);
	const int animName = 0;
	GameActorAnimation* pAnim = NULL;

	// 玩家类
	if(strcmp(name.c_str(),"zsgr_") == 0)
	{
		//const char* name = "zsgr_npj";   // zsgr_sym， zsgr_npj
		pAnim = GameActorAnimation::create("animation/player/", standActionNameList, figureName, ANI_COMPONENT_MAX, 1);

		// 脸正面，站立动画
		pAnim->setAnimName(animName);
		pAnim->setAction(ANIM_DIR_DOWN);
		pAnim->play();

		pAnim->showComponent(ANI_COMPONENT_REFLECTION, false);
		pAnim->showComponent(ANI_COMPONENT_SHADOW, true);

		// these call must be excuted after GameActorAnimation::setAnimName()
		int profession = BasePlayer::getProfessionIdxByFigureName(figureName);
		BasePlayer::switchWeapon(weaponName, pAnim, profession);   // "lance02"

		if(strcmp(weaponEffectFireName, "") != 0)
		{
			CCLegendAnimation* pBodyAnim = pAnim->getAnim(ANI_COMPONENT_BODY, animName);
			//weaponEffectName = "animation/texiao/particledesigner/caiji1.plist";
			WeaponEffectDefine effectDef;
			effectDef.weaponEffectType = WeaponEffect::getWeaponEffectType(weaponEffectFireName);
			effectDef.weaponEffectFileName = weaponEffectFireName;
			int weaponType = BasePlayer::getWeaponType(profession);
			WeaponEffect::attach(effectDef, weaponType, pBodyAnim);
		}
		if(strcmp(weaponEffectSparkleName, "") != 0)
		{
			CCLegendAnimation* pBodyAnim = pAnim->getAnim(ANI_COMPONENT_BODY, animName);
			//weaponEffectName = "animation/texiao/particledesigner/caiji1.plist";
			WeaponEffectDefine effectDef;
			effectDef.weaponEffectType = WeaponEffect::getWeaponEffectType(weaponEffectSparkleName);
			effectDef.weaponEffectFileName = weaponEffectSparkleName;
			int weaponType = BasePlayer::getWeaponType(profession);
			WeaponEffect::attach(effectDef, weaponType, pBodyAnim);
		}
	}
	// 武将类
	else if(strcmp(name.c_str(),"zsgg_") == 0)
	{
		pAnim = GameActorAnimation::create("animation/generals/", standActionNameList, figureName, ANI_COMPONENT_MAX, 1);

		// 脸正面，站立动画
		pAnim->setAnimName(animName);
		pAnim->setAction(ANIM_DIR_DOWN);
		pAnim->play();

		pAnim->showComponent(ANI_COMPONENT_REFLECTION, false);
		pAnim->showComponent(ANI_COMPONENT_SHADOW, true);

		// 武将没有切换武器形象的功能
		//BasePlayer::switchWeapon(weaponName, pAnim);   // "lance02"
	}
	else
	{
		CCAssert(false, "wrong figure name");
	}

	if(pAnim != NULL)
	{
		// 阴影
		CCLegendAnimation* pShadowAnim = pAnim->getAnim(ANI_COMPONENT_SHADOW, animName);
		if(pShadowAnim != NULL)
		{
			pShadowAnim->setColor(ccBLACK);
			pShadowAnim->setScaleX(1.1f);
			pShadowAnim->setScaleY(0.5f);
			pShadowAnim->setRotation(15.f);
			pShadowAnim->setSkewX(35.f);
			pShadowAnim->setOpacity(80);
		}
	}

	return pAnim;
}

std::string ActorUtils::getActorFigureName(ActiveRole& activerole)
{
	std::string figureName = "";
	if (activerole.type() == GameActor::type_monster)  {
	}
	else if(activerole.type() == GameActor::type_player) 
	{
		if(activerole.has_body()) {
			figureName = activerole.body();
		}
		else {
			int profession = BasePlayer::getProfessionIdxByName(activerole.profession());
			figureName = BasePlayer::getDefaultAnimation(profession);
		}
	}
	else if(activerole.type() == GameActor::type_pet) {
		figureName = activerole.rolebase().figure();
	}
	else {
		CCAssert(false, "please implement this branch");
	}

	return figureName;
}

CCNodeRGBA* ActorUtils::createSkillNameLabel(const char* skillName)
{
	CCNodeRGBA* node = CCNodeRGBA::create();
	node->setCascadeOpacityEnabled(true);

	//CCSprite* spr = CCSprite::create("res_ui/font/super_di.png");
	//spr->setScaleX(3.0f);
 //   spr->setScaleY(2.0f);
	//spr->setAnchorPoint(ccp(0.5f,0));
	//node->addChild(spr);

	//CCLabelTTF* skillNameLabel = CCLabelTTF::create(skillName, APP_FONT_NAME, 40);
	////skillName->setColor(ccc3(0, 255, 0));
	//skillNameLabel->setAnchorPoint(ccp(0.5f,0));
	//ccColor3B color = ccc3(0, 255, 0);
	//skillNameLabel->enableShadow(CCSizeMake(0,0), 1.0f, 1.0f, color);
	//node->addChild(skillNameLabel);

	// art font for the skill
	// background
	CCSprite* spr = CCSprite::create("res_ui/moji_4.png");
	spr->setScale(1.6f);
	spr->setAnchorPoint(ccp(0.5f,0.5f));
	node->addChild(spr);
	// skill name
	CCLabelBMFont* skillNameLabel = CCLabelBMFont::create(skillName, "res_ui/font/ziti_5.fnt");
	skillNameLabel->setAnchorPoint(ccp(0.5f,0.5f));
	node->addChild(skillNameLabel);
	node->setScale(0.7f);
	
	/**********************这是旧版技能名字的动画action******************************/
	/*CCFiniteTimeAction*  action = CCSequence::create(
		CCScaleTo::create(0.25f*1/2,1.2f), 
		CCScaleTo::create(0.25f*1/3,0.8f),
		CCDelayTime::create(1.5f),
		CCFadeOut::create(0.5f),
		CCRemoveSelf::create(),
		NULL);
	node->runAction(action);*/
	/*************************************************n******************************/

	CCActionInterval * action_1 = CCMoveBy::create(0.7f, ccp(0, 50));
	CCActionInterval * action_2 = CCSequence::create(
		CCDelayTime::create(0.1f),
		CCScaleTo::create(0.15f, 1.1f),
		CCScaleTo::create(0.15f, 0.7f),
		NULL);
	CCFiniteTimeAction * action_spwan_1 = CCSpawn::create(
		action_1,
		action_2,
		NULL);

	CCFiniteTimeAction * action_spwan_2 = CCSpawn::create(
		CCFadeOut::create(0.3f),
		CCScaleTo::create(0.3f, 1.1f),
		NULL);

	CCAction * action_final = CCSequence::create(
		action_spwan_1,
		CCDelayTime::create(0.3f),
		action_spwan_2,
		CCRemoveSelf::create(),
		NULL);

	node->runAction(action_final);
	
	return node;
}

CCPoint ActorUtils::getDefaultSkillPosition(BaseFighter* attacker, const char* skillId, const char* skillProcessorId)
{
	int atk_distance = attacker->getGameFightSkill(skillId, skillProcessorId)->getDistance();

	// calc the target position
	CCPoint targetPos = attacker->getWorldPosition();
	int dir = attacker->getAnimDir();
	if(dir == ANIM_DIR_UP)   // face to up
		targetPos.y -= atk_distance;
	else if(dir == ANIM_DIR_LEFT)   // face to left
		targetPos.x -= atk_distance;
	else if(dir == ANIM_DIR_DOWN)   // face to down
		targetPos.y += atk_distance;
	else if(dir == ANIM_DIR_RIGHT)   // face to right
		targetPos.x += atk_distance;

	CCPoint* target = attacker->getGameScene()->findNearestPositionToTarget(
		attacker->getWorldPosition().x, attacker->getWorldPosition().y,
		targetPos.x, targetPos.y);
	if(target != NULL)
	{
		targetPos.x = target->x;
		targetPos.y = target->y;

		delete target;
	}

	GameSceneLayer* scene = attacker->getGameScene();
	short tileX = scene->positionToTileX(targetPos.x);
	short tileY = scene->positionToTileY(targetPos.y);
	// out of map, ignore
	if(!scene->checkInMap(tileX, tileY))
	{
		return attacker->getWorldPosition();
	}

	return targetPos;
}

CCSprite* ActorUtils::createGeneralRankSprite(int nRank)
{
	if(nRank <= 0)
		return NULL;
	std::string rankPathBase = "res_ui/rank/rank";
	char s[5];
	sprintf(s,"%d", nRank);
	rankPathBase.append(s);
	rankPathBase.append(".png");
	CCSprite * pRankSprite = CCSprite::create(rankPathBase.c_str());
	return pRankSprite;
}

void ActorUtils::addSkillName(BaseFighter* actor, const char* skillId, const char* skillProcessorId)
{
	CBaseSkill* baseSkill = actor->getGameFightSkill(skillId, skillProcessorId)->getCBaseSkill();
	if(baseSkill == NULL)
		return;

	// general says the skill name
	//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
	//const char *str  = ((CCString*)strings->objectForKey("general_useskill"))->m_sString.c_str();
	//std::string str_use_skill = str;
	//str_use_skill.append(" ");
	//str_use_skill.append(baseSkill->name());
	//this->addChatBubble(str_use_skill.c_str(), 1.0f);

	// show the skill name at the head of the general
	//CCNodeRGBA* node = ActorUtils::createSkillNameLabel(baseSkill->name().c_str());
	//this->addEffect(node, false, 130);
	
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	CCNodeRGBA* node = ActorUtils::createSkillNameLabel(baseSkill->name().c_str());
	node->setAnchorPoint(ccp(0.5f, 0.5f));
	//node->setPosition(ccp(s.width - 130, s.height - 100));
	//GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	//scene->addChild(node, SCENE_TOP_LAYER_BASE_ZORDER);

	node->setPosition(ccp(actor->getPositionX(), actor->getPositionY() + BASEFIGHTER_ROLE_HEIGHT));
	CCNode* actorLayer =  actor->getGameScene()->getActorLayer();
	actorLayer->addChild(node, SCENE_TOP_LAYER_BASE_ZORDER + actor->getGameScene()->getMapSize().height);
}

void ActorUtils::addMultiAttackDamageNumber(BaseFighter* defender, int damageNum, int attacktime)
{
	if(defender == NULL)
		return;

	for(int i = 0; i < attacktime; i++)
	{
		int damage = damageNum / attacktime;

		// random number ( fake number )
		if(attacktime > 1)
		{
			const int range = 10;
			float random = CCRANDOM_0_1() - 0.5f;
			damage += random * range;
			if(damage <= 0)
				damage = 1;
		}
					
		// damage number effect
		char str[10];
		sprintf(str,"%d", damage);
		CCLabelBMFont* label = CCLabelBMFont::create(str, "res_ui/font/c123.fnt");   // yellow color
		label->setAnchorPoint(ccp(0.5f,0.0f));
		label->setPosition(ccp(defender->getPosition().x, defender->getPosition().y + BASEFIGHTER_ROLE_HEIGHT*1/3));
		//label->setPosition(label->getPosition() + SimpleEffectManager::getNumberOffset());
		label->setVisible(false);

		/*
		// move up and fade out
		CCFiniteTimeAction* out_sub_action = CCSpawn::create(
			CCMoveBy::create(0.7f, ccp(0, 140)),
			CCFadeOut::create(2.5f),
			NULL);
		CCFiniteTimeAction*  action = CCSequence::create(
			CCDelayTime::create(i*0.2f),
			CCShow::create(),
			CCScaleTo::create(0.25f*1/2,1.7f),   // 2.5f
			CCScaleTo::create(0.25f*1/3,1.2f),   // 1.5f
			out_sub_action,
			CCRemoveSelf::create(),
			NULL);
		label->runAction(action);
		*/

		// scale to big
		CCFiniteTimeAction* step_one_action = CCSpawn::create(
			CCMoveBy::create(0.4f, ccp(0, 100)),
			CCScaleTo::create(0.4f, 1.8f, 2.5f),
			NULL);
		// scale to small
		CCFiniteTimeAction* step_two_action = CCSpawn::create(
			CCMoveBy::create(0.25f, ccp(0, 20)),
			CCScaleTo::create(0.25f, 1.3f, 1.5f),
			NULL);
		// scale to small
		CCFiniteTimeAction* step_three_action = CCSpawn::create(
			CCMoveBy::create(0.3f, ccp(0, -25)),
			CCScaleTo::create(0.3f, 1.3f, 1.3f),
			NULL);
		// scale to big
		CCFiniteTimeAction* step_four_action = CCSpawn::create(
			CCMoveBy::create(0.3f, ccp(0, -25)),
			CCScaleTo::create(0.3f, 1.2f, 1.2f),
			NULL);
		CCFiniteTimeAction* action = CCSequence::create(
			CCDelayTime::create(i*0.2f),
			CCShow::create(),
			step_one_action,
			step_two_action,
			step_three_action,
			step_four_action,
			//CCFadeOut::create(0.7f),
			CCFadeTo::create(1.0f, 192),
			CCRemoveSelf::create(),
			NULL);
		label->runAction(action);

		defender->getGameScene()->getActorLayer()->addChild(label, SCENE_TOP_LAYER_BASE_ZORDER + defender->getGameScene()->getMapSize().height);
	}
}

void ActorUtils::addDoubleAttackDamageNumber(BaseFighter* target, int damageNum, const char* fontName)
{
	char str[10];
	sprintf(str,"%d", damageNum);
	CCLabelBMFont* label = CCLabelBMFont::create(str, fontName);
	label->setAnchorPoint(ccp(0.5f,0.0f));
	//label->setScale(0.8f);

	CCNode* node = CCNode::create();
	node->setAnchorPoint(ccp(0,0));

	// special text
	CCSprite* pSprite = CCSprite::create("res_ui/font/double.png");

	float maxHeight = MAX(label->getContentSize().height, pSprite->getContentSize().height);

	pSprite->setAnchorPoint(ccp(0,0));
	pSprite->setPosition(ccp(0,(maxHeight-pSprite->getContentSize().height)/2));
	node->addChild(pSprite);

	// number
	label->setAnchorPoint(ccp(0,0));
	label->setPosition(ccp(pSprite->getContentSize().width,(maxHeight-label->getContentSize().height)/2));
	node->addChild(label);

	node->setContentSize(CCSizeMake(pSprite->getContentSize().width+label->getContentSize().width, maxHeight));
	node->setAnchorPoint(ccp(0.5f,0));

	SimpleEffectManager::applyEffectAction(node, SimpleEffectManager::EFFECT_BAOJI);
	//this->addEffect(node, false, 80/*BASEFIGHTER_ROLE_HEIGHT*/);
	node->setPosition(ccp(target->getPosition().x, target->getPosition().y + 80));
	target->getGameScene()->getActorLayer()->addChild(node, SCENE_TOP_LAYER_BASE_ZORDER + target->getGameScene()->getMapSize().height);
}

std::string ActorUtils::getWeapEffectByRefineLevel( int weapPression,int equipRefineLevel )
{
	std::string weapRefineEffectPath_ ="";
	std::map<EquipRefineEffectConfigData::weapEffect, std::string >::iterator it = EquipRefineEffectConfigData::s_EquipmentRefineEffect.begin();
	for ( ; it != EquipRefineEffectConfigData::s_EquipmentRefineEffect.end(); ++ it )
	{
		if (equipRefineLevel >= it->first.refineLevel )
		{
			EquipRefineEffectConfigData::weapEffect temp;
			temp.weapPression = weapPression;
			temp.refineLevel = it->first.refineLevel;
	
			std::string weapRefineEffectStr_ = EquipRefineEffectConfigData::s_EquipmentRefineEffect[temp];
			weapRefineEffectPath_ = "animation/weapon/weaponeffect/";
			weapRefineEffectPath_.append(weapRefineEffectStr_);			
		}
	}
	return weapRefineEffectPath_;
}

std::string ActorUtils::getWeapEffectByStarLevel( int weapPression,int equipStarLevel )
{
	std::string weapStarEffectPath_ ="";
	std::map<EquipStarEffectConfigData::weapEffect, std::string >::iterator it = EquipStarEffectConfigData::s_EquipmentStarEffect.begin();
	for ( ; it != EquipStarEffectConfigData::s_EquipmentStarEffect.end(); ++ it )
	{
		if (equipStarLevel >= it->first.starLevel)
		{
			EquipStarEffectConfigData::weapEffect temp;
			temp.weapPression = weapPression;
			temp.starLevel = it->first.starLevel;

			std::string weapStarEffectStr_ = EquipStarEffectConfigData::s_EquipmentStarEffect[temp];
			weapStarEffectPath_ = "animation/weapon/weaponeffect/";
			weapStarEffectPath_.append(weapStarEffectStr_.c_str());			
		}
	}
	return weapStarEffectPath_;
}



void ActorUtils::addMusouBodyEffect(BaseFighter* target, int color)
{
	// effect on body
	int effectTag = BaseFighter::kTagMosouEffect+target->getRoleId();
	CCNode* container = target->getChildByTag(BaseFighter::kTagFootEffectContainer);
	CCLegendAnimation* pBodyEffect = NULL;
	if(container->getChildByTag(effectTag) == NULL)
	{
		//pBodyEffect = CCLegendAnimation::create("images/lightning_body.anm");
		//pBodyEffect->setPlayLoop(true);
		//pBodyEffect->setReleaseWhenStop(false);   // only play once, then release
		//pBodyEffect->setScaleY(1.5f);
		//pBodyEffect->setOpacity(200);
		//pBodyEffect->setTag(kTagMosouEffect+this->getRoleId());
		//this->addEffect(pBodyEffect, false, 30);

		CCSprite* shadow = CCSprite::create("images/light_1.png");
		shadow->setAnchorPoint(ccp(0.5f, 0.5f));
		shadow->setTag(effectTag);
		shadow->setScaleX(1.3f);   // 1.5f  1.3f  1.4f
		shadow->setScaleY(3.9f);   // 4.5f  3.9f  4.2f
		//shadow->setRotation(-60.f);
		ccColor3B color3b = GameUtils::convertToColor3B(color);
		shadow->setColor(color3b);
		//shadow->setPosition(ccp(0, 40));

		CCFadeTo*  action1 = CCFadeTo::create(0.25f,64);
		CCFadeTo*  action2 = CCFadeTo::create(0.25f,192);
		CCFadeTo*  action3 = CCFadeTo::create(0.5f,255);
		CCFadeTo*  action4 = CCFadeTo::create(0.25f,192);
		CCFadeTo*  action5 = CCFadeTo::create(0.25f,64);
		CCDelayTime*  action6 = CCDelayTime::create(0.1f);
		CCRepeatForever * repeapAction = CCRepeatForever::create(
			CCSequence::create(action1,action2,action3,action4,action5,action6,NULL));
		shadow->runAction(repeapAction);

		target->addEffect(shadow, true, 40);
	}
}

void ActorUtils::addStrategyEffect(BaseFighter* player, int fightWayId)
{
	std::string str_icon = "";
	for (unsigned int i = 0;i<FightWayConfigData::s_fightWayBase.size();i++)
	{
		if (fightWayId == FightWayConfigData::s_fightWayBase.at(i)->fightwayid())
		{
			str_icon = FightWayConfigData::s_fightWayBase.at(i)->get_icon();
		}
	}
	if(str_icon == "")
		return;

	std::string str_iconPath = "res_ui/MatrixMethod/";
	str_iconPath.append(str_icon);
	str_iconPath.append("2.png");
	CCSprite* pSprite = CCSprite::create(str_iconPath.c_str());
	pSprite->setAnchorPoint(ccp(0.5f,0.5f));
	pSprite->setVisible(false);

	const float RATATION_SPEED = 112.5f;
	const float FADEIN_DURATION = 1.0f;
	const float ROTATE_DURATION = 5.0f;
	const float FADEOUT_DURATION = 1.0f;

	CCFiniteTimeAction* fadeinAction = CCSpawn::create(
		CCRotateBy::create(FADEIN_DURATION, FADEIN_DURATION*RATATION_SPEED),
		CCFadeIn::create(FADEIN_DURATION),
		NULL);
	CCFiniteTimeAction* fadeoutAction = CCSpawn::create(
		CCRotateBy::create(FADEOUT_DURATION, FADEOUT_DURATION * RATATION_SPEED),
		CCFadeOut::create(FADEOUT_DURATION),
		NULL);
	CCAction* action1 = CCSequence::create(
		CCShow::create(),
		fadeinAction,
		CCRotateBy::create(ROTATE_DURATION, ROTATE_DURATION * RATATION_SPEED),
		fadeoutAction,
		NULL);
	pSprite->runAction(action1);

	CCNode* pNode = CCNode::create();
	pNode->addChild(pSprite);
	pNode->setScaleX(2.1f);   // 3.0f
	pNode->setScaleY(1.05f);   // 1.5f
	pNode->setTag(BASEPLAYER_STRATEGY_EFFECT_TAG);
	CCAction* action2 = CCSequence::create(
		CCDelayTime::create(FADEIN_DURATION+ROTATE_DURATION+FADEOUT_DURATION),
		CCRemoveSelf::create(),
		NULL);
	pNode->runAction(action2);

	player->addEffect(pNode, true, 0);
}
void ActorUtils::removeStrategyEffect(BaseFighter* player)
{
	CCNode* container = player->getChildByTag(BaseFighter::kTagFootEffectContainer);
	CCNode* pNode = container->getChildByTag(BASEPLAYER_STRATEGY_EFFECT_TAG);
	if(pNode != NULL)
		pNode->removeFromParent();
}

void ActorUtils::addFlowerParticle( BaseFighter* player )
{
	//REMOVE FLOWERpARTICLE
	ActorUtils::removeFlowerParticle(player);

	CCParticleSystem* particle1 = WeaponEffect::getParticle("animation/texiao/particledesigner/guanghuan.plist");
	particle1->setPositionType(kCCPositionTypeGrouped);
	particle1->setAnchorPoint(ccp(0.5f,0.5f));
	particle1->setPosition(ccp(0,0));
	particle1->setScaleX(0.3f);
	particle1->setScaleY(0.6f);

	CCSprite* particle2 = CCSprite::create("images/light_1.png");
	particle2->setAnchorPoint(ccp(0.5f, 0.5f));
	particle2->setScaleX(1.3f); 
	particle2->setScaleY(3.9f);
	particle2->setColor(ccc3(255, 153, 204));//set flower particle color

	CCFadeTo*  action1 = CCFadeTo::create(0.25f,64);
	CCFadeTo*  action2 = CCFadeTo::create(0.25f,192);
	CCFadeTo*  action3 = CCFadeTo::create(0.5f,255);
	CCFadeTo*  action4 = CCFadeTo::create(0.25f,192);
	CCFadeTo*  action5 = CCFadeTo::create(0.25f,64);
	CCDelayTime*  action6 = CCDelayTime::create(0.1f);
	CCRepeatForever * repeapAction = CCRepeatForever::create(
		CCSequence::create(action1,action2,action3,action4,action5,action6,NULL));
	particle2->runAction(repeapAction);

	CCNode * node_ = CCNode::create();
	node_->setAnchorPoint(ccp(0.5f,0.5f));
	node_->setPosition(ccp(0,0));
	node_->setTag(FLOWERPARTICLEONROLE_ACTORUTILS_TAG);
	//add all node
	node_->addChild(particle2);
	node_->addChild(particle1);

	player->addEffect(node_, true, 40);

	//add player flowerPacticle
	/*
	MyPlayer* me = GameView::getInstance()->myplayer;
	int flowerNum_ = GameView::getInstance()->getFlowerNum();
	if (flowerNum_ >= 999 )
	{
	}else
	{
		if (flowerNum_ >=365)
		{
		}else
		{
			if (flowerNum_ >=11)
			{
			}else
			{
			}
		}
	}
	*/
}

void ActorUtils::removeFlowerParticle( BaseFighter * player )
{	
	if (player)
	{
		CCNode* pNode = (CCNode*)player->getChildByTag(FLOWERPARTICLEONROLE_ACTORUTILS_TAG);
		if (pNode)
		{
			pNode->removeFromParentAndCleanup(true);
		}
	}
}

std::map<std::string, CCLegendAnimation*> ActorUtils::s_skillsEffectsCache;
void ActorUtils::preloadSkillEffects(int profession)
{
	// 1. get all skills for one profession
	std::vector<std::string> allSkills;
	std::map<std::string, CBaseSkill*>& baseSkillData = StaticDataBaseSkill::s_baseSkillData;
    for (std::map<std::string, CBaseSkill*>::iterator it = baseSkillData.begin(); it != baseSkillData.end(); ++it) 
	{
		CBaseSkill* _skillInfo = it->second;
		if(_skillInfo->get_profession() == profession)
		{
			allSkills.push_back(_skillInfo->id());
		}
	}

	// 2. get all effects' names
	std::vector<std::string> allSkillsEffectNames;
	for(unsigned int i = 0; i < allSkills.size(); i++)
	{
		SkillBin* pSkillBin = StaticDataSkillBin::s_data[allSkills.at(i)];
		if(pSkillBin != NULL)
		{
			std::string effectName = pSkillBin->releaseEffect;
			if(effectName != "" && effectName != "null")
				allSkillsEffectNames.push_back(effectName);

			effectName = pSkillBin->flyingItemEffect;
			if(effectName != "" && effectName != "null")
				allSkillsEffectNames.push_back(effectName);

			effectName = pSkillBin->flyingEndedEffect;
			if(effectName != "" && effectName != "null")
				allSkillsEffectNames.push_back(effectName);
		}
	}

	// 3. load animations
	for(unsigned int i = 0; i < allSkillsEffectNames.size(); i++)
	{
		std::string skillEffectName = allSkillsEffectNames.at(i);
		if(s_skillsEffectsCache[skillEffectName] == NULL)
		{
			CCLegendAnimation* pAnim = CCLegendAnimation::create(skillEffectName);
			//pAnim->retain();
			s_skillsEffectsCache[skillEffectName] = pAnim;
		}
	}
}

void ActorUtils::addGoldMessageToScene(int newGold)
{
	// add message to the game scene
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	MyPlayer* me = GameView::getInstance()->myplayer;

	std::string _str = "+";
	char diffStr[20];
	sprintf(diffStr,"%d", newGold);
	_str.append(diffStr);
	const char * goldStr  = StringDataManager::getString("getPlayerAddOfGold");
	_str.append(goldStr);

	CCLabelBMFont* pLabel = CCLabelBMFont::create(_str.c_str(), "res_ui/font/ziti_3.fnt");
	pLabel->setScale(1.2f);
	pLabel->setAnchorPoint(ccp(0.5f,0.0f));
	pLabel->setPosition(ccp(me->getPositionX() + 80, me->getPositionY()));
	CCAction*  action = CCSequence::create(
		CCMoveBy::create(0.8f, ccp(0, 40)),
		CCRemoveSelf::create(),
		NULL);
	pLabel->runAction(action);
	scene->getActorLayer()->addChild(pLabel, SCENE_TOP_LAYER_BASE_ZORDER);
}