#ifndef _GAMESTATE_LOADSCENE_H_
#define _GAMESTATE_LOADSCENE_H_

#include "cocos2d.h"
#include "cocos-ext.h"

#include "../messageclient/element/CMapInfo.h"

USING_NS_CC;
USING_NS_CC_EXT;

class PushHandler1106;
class GameSceneState;
/**
 * update and display the loading interface
 * Author: Zhao Gang
 * Date: 2013/11/7
 */
class SceneLoadLayer : public CCLayer
{
public:
    SceneLoadLayer();

	virtual void update(float dt);

	void preLoadResource(const char *path);
	void loadingCallBack(cocos2d::CCObject *obj);

	bool isFinishedState();

	static void loadUIPanelFromJson(UIPanel** dest, const char* fileName);

	static CCPoint s_myNewWorldPosition;
	static CMapInfo s_newMapInfo;

	static void initNextLevel();

	// add by liutao
	// get and set the m_state value;
	int getState();
	void setState(int state);
public:
	enum {
		State_WaitAction = 0,   // now, wait camera action
		State_Login,
		State_Init,
		State_LoadResStep_0,
		State_LoadResStep_1,
		State_LoadResStep_2,
		State_LoadResStep_3,
		State_Finished,
		State_Idle,
	};

private:
	void createUI();
	std::string getLoadingText();
	void loadCommonRes();
	void loadUIWidgetRes();

	char* getLoadingImage();

private:
    cocos2d::CCLabelTTF *m_pLabelLoading;
    cocos2d::CCLabelTTF *m_pLabelPercent;
    int m_nNumberOfSprites;
    int m_nNumberOfLoadedSprites;

	int m_state;
	bool m_bLoadResFinished;

	CCProgressTimer *progressTimer;

	UILayer * layer;
	int time_;
};

#endif
