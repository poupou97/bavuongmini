﻿#ifndef _GAMESCENESTATE_GAMESTATISTICS_H_
#define _GAMESCENESTATE_GAMESTATISTICS_H_

#include "cocos2d.h"

USING_NS_CC;

/**
 * 伤害数字统计信息
 * @Author: zhaogang
 * 2013-7-24
 */
class DamageStatistics {
public:
	DamageStatistics();
	virtual ~DamageStatistics();

	static void clear();

	static void collect(long long roleId, int damageNum);

	static int getDamge(long long roleId);

private:
	static std::map<long long, int> s_damageStatistic;
};

#endif