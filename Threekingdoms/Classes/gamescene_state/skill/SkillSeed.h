#ifndef _SKILL_SKILLSEED_H_
#define _SKILL_SKILLSEED_H_

/**
 * 技能描述，向SkillManager传递的参数
 * @author zhao gang
 */
class SkillSeed{
public:
	/** 技能ID */
	std::string skillId;
	/** 客户端使用的技能处理类id
	  该id对应的类位于skill/processor下，用反射机制创建 */
	std::string skillProcessorId;
	/** 攻击者 */
	long long attacker;
	/** 防御者 */
	long long defender;
	/** 施放点 */
	short x;
	short y;
};

#endif