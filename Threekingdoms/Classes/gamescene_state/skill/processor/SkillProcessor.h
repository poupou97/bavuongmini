#ifndef _SKILL_SKILLPROCESSOR_H_
#define _SKILL_SKILLPROCESSOR_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;

/**
 * 技能流程处理，包括 技能的绘制，逻辑等
 * @author zhao gang
 *
 */
class SkillSeed;
class CSkillResult;
class GameSceneLayer;
class BaseFighter;

class SkillProcessor : public CCObject {
public:
	SkillProcessor();
	virtual ~SkillProcessor();

	virtual void init();
	virtual void release() {};

	// callback when BaseFighter began the attack action
	virtual void onSkillBegan(BaseFighter* attacker);
	// callback when BaseFighter finished the attack action
	virtual void onSkillReleased(BaseFighter* attacker);
	// callback when the client revieve the skill push message from server
	virtual void onSkillApplied(BaseFighter* attacker);

	virtual void update(float dt);
	
	/**
	 * 播放技能释放时音效
	 */
	void playFightSound();

	SkillSeed* getSkillSeed();
	void setSkillSeed(SkillSeed* skillSeed);

	CSkillResult* getSkillResult();
	void setSkillResult(CSkillResult* skillResult);
	
	void loadSkillEffect(const char* currEffect);
	CCArray* getApEffects();
	void setApEffects(CCArray* apEffects);
	
	GameSceneLayer* getScene();
	void setScene(GameSceneLayer* scene);

	float getPosX();
	void setPosX(float mX);

	float getPosY();
	void setPosY(float mY);

	void setEffectScaleX(float sx);
	void setEffectScaleY(float sy);
	
	bool isHide();
	void setHide(bool isHide);
	
	bool isRelease();
	void setRelease(bool isRelease);
	
	/**
	 * 玩家自己发送协议给服务器和出招
	 * @param attacker
	 */
	void skillDamage(BaseFighter* attacker);
	
	void sendSkillCommand(BaseFighter* attacker);

	/**
	 * 技能中使用 发送攻击技能到服务器并计算伤害
	 */
	void countDamage(BaseFighter* attacker);
	
	// 超出屏幕则隐藏
	void hideOutScreen();
	
	void updateResume(BaseFighter* attacker);

	/** set the switch which will be used for checking if sending skill request */
	inline void setSendAttackReq(bool bSend) {  m_bSendAttackReq = bSend; };

	static bool knockback(BaseFighter* attacker, BaseFighter* defender, float knockDistance, float backDuration, float hitRecoverTime = 0.5f);

	// 被击打者，仅仅会有距离上的后退，而不会改变当前动作（比如正在攻击，还会接着攻击）
	static bool hitback(BaseFighter* attacker, BaseFighter* defender, float knockDistance, float backDuration);

	// 上下震动，用于表达actor受到某种打击后的效果
	static void quake(BaseFighter* actor, float duration = 0.9f);

	// 左右或前后晃动，用于表达actor受到某种打击后的效果
	// @param shakeLeftRight if true, shake left to right, otherwise, shake forward and backward
	static void shake(BaseFighter* actor, bool shakeLeftRight = false, int shakeTimes = 3, float range = 10.0f);

	// 将敌人高高的挑起来
	static void flyup(BaseFighter* attacker, BaseFighter* defender);

	static void showWeaponEffect(BaseFighter* actor, const char* effectFileName, float stayTime);

	// show ground effect, the groud effect will be disappear after a short while
	void showGroundEffect(const char* effectName, GameSceneLayer* scene, float firstDelayTime, float secondDelayTime, float fadeOutTime);

	// show the magic halo on the ground
	// for example, tqms skill
	static void addMagicHaloToGround(GameSceneLayer* scene, CCPoint worldPosition);
	static void addMagicHaloToGround(GameSceneLayer* scene, CCPoint worldPosition, std::string str_spritePath, 
		float total_duration = 1.5f, float stay_duration = 0.5f, float scale_x = 1.6f, float scale_y = 0.8f);
	// show the "wave halo" on the ground
	static void addWaveHaloToGround(GameSceneLayer* scene, CCPoint worldPosition, std::string spritePathName, float interval, int times = 1, float maxScale = 2.0f);

	static void addBreathingHaloToGround(GameSceneLayer* scene, CCPoint worldPosition, const char* effectPathName);

protected:
	/** request skill damage to server */
	void sendAttackReq(BaseFighter* attacker);

	// knokback the enemy
	bool knockback(BaseFighter* attacker, float backDuration, float hitRecoverTime = 0.5f);
	
	// 尽量离开和队伍其他成员保持分散队形，而非集中于一个点
	void scatter(BaseFighter* attacker);
	// 武将离主角过远，聚集到一起
	void assemble(BaseFighter* attacker);
	// 在某些条件下，武将回归其真实位置( RealWorldPosition )
	void returnHome(BaseFighter* attacker);

	void addEffectToGround(BaseFighter* actor, std::string& effectName);
	void _addEffectToGround(GameSceneLayer* scene, float x, float y, std::string& effectName);

	/**
	   * @param bFoot 技能特效放在身上，还是脚下。默认放在身上
	   **/
	void addEffectToBody(BaseFighter* actor, std::string& effectName, int animDirection, bool bFoot = false);

	static bool isBoss(BaseFighter* actor);

protected:
	// the scale of the effect
	float m_effectScaleX;
	float m_effectScaleY;

private:
	// skill info restore in this variable
	SkillSeed* skillSeed;

	// skill result
	CSkillResult* mSkillResult;

	GameSceneLayer* scene;

	// the position of the skill effect
	float m_x;
	float m_y;
	
	CCArray* apEffects;
	
	bool m_bIsRelease;
	bool m_bIsHide;

	bool m_bSendAttackReq;
};

/////////////////////////////////////////////////////////////////

/** @brief Play the CCLegendAnimation
*/
class CCPlayLegendAnimation : public CCActionInstant
{
public:
    CCPlayLegendAnimation(){}
    virtual ~CCPlayLegendAnimation(){}
    //super methods
    virtual void update(float time);

public:

    /** Allocates and initializes the action */
    static CCPlayLegendAnimation * create();
};

#endif