#ifndef _LEGEND_GAMESCENE_H_
#define _LEGEND_GAMESCENE_H_

#include <map>

class GameActor;

/**
 the scene manager, the scene includes many actors, 
 for example, player, monster, buildings etc.
 */
class GameScene
{
public:
	GameScene();
	~GameScene();

	// 一个场景中，曾经出生过的怪物
	// don't use the GameActor instance, this instance is only used for recording
	static std::map<long long, GameActor*> s_bornMonsterMap;

private:
	GameActor* m_pActors;
};

#endif