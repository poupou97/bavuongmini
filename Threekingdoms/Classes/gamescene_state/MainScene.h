
#ifndef _GAMESCENESTATE_MAINSCENE_H_
#define _GAMESCENESTATE_MAINSCENE_H_

#include "../ui/extensions/uiscene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "../GameStateBasic.h"
#include "GameSceneState.h"
#include "../legend_script/CCTutorialIndicator.h"
//#include "messageclient/element/CRelationPlayer.h"

/** global UI tag*/
enum {
	kTagMainSceneRoot = 0,
	kTagVirtualJoystick = 4,   // 虚拟摇杆
	kTagUITest = 1200,
	kTagBackpack = 1300,
	kTagGoodsItemInfoBase =1310,
	kTagMap = 1400,
	kTabChat = 1500,
	kTagHeadMenu = 1610,
	kTagMyHeadInfo = 1620,
	kTagShortcutLayer = 1700,
	kTagChatWindows =1800,
	kTagMessageWindows =1810,
	kTagMissionScene = 1900,
	kTagMailUi = 2000,
	kTagTalkWithNpc = 2100,
	kTagHandleMission = 2200,
	kTagTalkMissionUI = 2210,
	kTagUseMissionPropUI = 2220,
	kTagMissionAndTeam = 2300,
	kTagFriendUi=2400,
	kTagNpcTalkWindow = 2500,
	kTagAuction = 2600,
	KTagSkillScene = 2700,
	KtagSkillInfoScene = 2800,
	kTagShortcutConfigure = 2900,
	kTagMyBuff = 3000,
	kTagTatgetInfo = 3100,
	kTagPickUI = 3200,
	ktagEquipMentUI=3300,
	kTagGeneralsUI = 3400,
	kTagGeneralsSingleRecuriteResultUI = 3410,
	kTagGeneralsTenTimesRecuriteResultUI = 3420,
	kTagStoreHouseUI = 3500,
	kTagInstanceDetailUI = 3600,
	kTagInstanceFunctionPanel = 3601,
	kTagInstanceEndUI = 3602,
	kTagInstanceMapUI = 3603,
	kTagGeneralsFateInfoUI = 3700,
	kTagShopUI = 3800,
	kTagShopItemBuyInfo = 3810,
	kTagGeneralsSkillInfoUI = 3900,
	kTagGeneralsSkillListUI = 4000,
	kTagGeneralsListForFightWaysUI = 4100,
	kTagStrategiesDetailInfo = 4110,
	kTagStrategiesUpGradeUI = 4200,
	kTagPopFriendListUI = 4300,
	kTagInstanceRewardUI = 4400,
	kTagFamilyUI = 4500,
	kTagApplyFamilyUI = 4510,
	kTagManageFamilyUI = 4520,
	kTagMovieBaseNode = 4600,   // 剧情模式的Root Node（包含对话中屏幕的上下黑边等）
	kTagGoldStoreUI = 4700,
	kTagGoldStoreItemBuyInfo = 4710,
	kTagGoldStoreItemInfo = 4720,
	kTagGuideMapUI = 4800,
	kTagGeneralListForTakeDrug = 4900,
	ktagMainSceneTargetInfo = 4950,
	kTagSetUI = 5000,
	kTagOnLineGiftIcon = 5100,
	kTagOnLineGiftUI = 5110,
	kTagOnLineGiftLayer = 5111,
	kTagSignDailyIcon = 5120,
	kTagSignDailyUI = 5121,
	kTagSignDailyLayer = 5122,
	kTagMoneyTreeManager = 5130,
	kTagMoneyTreeUI = 5131,
	kTagActiveIcon = 5140,
	kTagActiveUI = 5150,
	kTagActiveLayer = 5151,
	kTagActiveShop = 5160,
	kTagActiveShopItemBuyInfo = 5170,
	kTagActiveShopItemInfo = 5180,
	kTagRankUI = 5190,
	kTagExtraRewardsUI = 5200,
	ktagQuiryUI = 5300,
	kTagOffLineArenaUI = 5400,
	kTagHonorShopItemBuyInfo = 5410,
	kTagHonorShopItemInfo = 5420,
	kTagOffLineArenaResultUI = 5430,
	kTagNewFunctionRemindUI = 5500,
	kTagVipDetailUI = 5600,
	kTagVipEveryDayRewardsUI = 5700,
	kTagFirstBuyVipUI = 5800,
	kTagOffLineExpUI = 5900,
	kTagQuestionUI = 6000,
	kTagQuestionEndUI = 6010,
	kTagRechargeUI = 6020,
	kTagWorldBossUI = 6100,
	kTagRewardDesUI = 6110,
	kTagGetRewardUI = 6120,
	kTagDmgRankingUI = 6130,
	kTagChannelUI = 6200,
	ktagPrivateUI = 6300,
	ktagPrivateContentUi = 6350,
	kTagManageFamily = 6400,
	kTagTeachRemind = 6500,
	kTagTeachRemindSkill = 6510,
	kTagTeachRemindGeneral = 6520,
	kTagChessBoardUI = 6600,
	ktagChallengeRoundUi = 6700,
	ktagSingCopylevel = 6720,
	ktagSingCopyRank = 6740,
	ktagSingCopyReward = 6760,
	ktagSingCopyResutUi = 6780,
	kTagSingCopyCountDownUi = 6790,
	kTagFamilyFightRewardUI = 6800,
	kTagFamilyFightHistoryNotesUI = 6810,
	kTagFamilyFightIntegrationRankingUI = 6820,
	kTagFamilyFightResultUI = 6830,
	kTagFamilyFightCountDownUI = 6840,
	kTagBattleSituationUI = 6850,
	kTagBattleAchievementUI = 6860,
	kTagRemindUi = 6900,
	kTagRecuriteGeneralCardUI = 7000,
	kTagShowBigGeneralsHead = 7100,
	kTagGetphyPowerUi = 7200,
	kTagRobotUI = 7300,
	kTagRewardUI = 7400,
	kTagRewardTaskMainUI = 7500,
	kTagRewardTaskDetailUI = 7510,
	kTagFightPointChange = 7600,
	kTagSearchGeneralUI = 7700,
	kTagSignMonthlyUI = 7800,
};

enum {
	kTag_MainUI_Element = 10,
	kTag_MainUI_Animation = 20,
	kTag_MainUI_OtherUI = 30,   
	kTag_MainUI_Tutorial = 40,
};

enum{
	ZOrder_NormalUI = 0,
	ZOrder_ToolTips = 10,
	ZOrder_PopUpWindow = 20,
	ZOrder_Marquee = 30,
	ZOrder_Tutorial = 40,
};

enum{
	TPriority_NormalUI = 0,
	TPriority_ToolTips = -10,
	TPriority_PopUpWindow = -20,
	TPriority_Tutorial = -30,
};

class UITab;
class TargetInfo;
class MyHeadInfo;
class SceneTest;
class MyPlayer;
class MyPlayerOwnedStates;
class Monster;
class MyPlayerOwnedCommand;
class ChatUI;
class UITest;
class GuideMap;
class HeadMenu;
class MyBuff;
class MissionAndTeam;
class ChatWindows;
class GeneralsUI;
class General;
class CRelationPlayer;
class OnlineGiftIcon;
class OnLineRewardUI;
class CCTutorialParticle;
class GeneralsInfoUI;

class MainScene :public UIScene
{
public:
	MainScene();
	~MainScene();
	static MainScene* create();
	bool init();
	void onEnter();
	void update(float delta);
	void updateRecuriteInfo(float dt);

	void ButtonHeadEvent(CCObject *pSender);
	void ButtonGeneralEvent(CCObject *pSender);
	void ButtonChatEvent(CCObject *pSender);
	void ButtonLVTeamEvent(CCObject *pSender);
	void ButtonMapOpenEvent(CCObject *pSender);
	void ButtonSceneTest(CCObject *pSender);
	bool isTestSceneOn;

	void initHead();

	//add by yangjun 2014.9.26
	// add generalsHeadInfoManager
	void initGeneralInfoUI();
	// get generalsInfoUI
	GeneralsInfoUI* getGeneralInfoUI();

	void createTargetInfo(BaseFighter * baseFighter);

	void BuffEvent(CCObject * pSender);
	
	// refresh the user-interface, actor's head pannel
	void ReloadTarget(BaseFighter * baseFighter);
	// refresh the user-buffdata
	void ReloadBuffData();

	long long interactOfPlayer;
	bool isInteractPlayer;

	void setInteractOfPlayerId(long long playerId);
	void setInteractOfPlayer(bool isInteract);
	int getInteractOfPlayerId();
	bool getInteractOfPlayer();
	//玩家之间添加好友后的互动，询问是否添加对方为好友
	void interactRelationOfPlayers(CRelationPlayer * relationPlayer);
	void interactOtherplayerSure(CCObject * obj);
	void interactOtherplayerNo(CCObject * obj);
	
    cocos2d::extension::UIButton * btnRemin;
	CCTutorialParticle * btnReminOfNewMessageParticle;
	void showChatWindows(CCObject *obj);
	cocos2d::extension::UIButton * Button_mapOpen;
	bool isMapOpen;
	GuideMap * guideMap;
	
	//add private chat
	void addPrivateChatUi(long long id,std::string name,int countryId,int vipLv,int lv,int pressionId);

	//教学
	int mTutorialScriptInstanceId;
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	//指向摇杆
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();

	//添加吊坠动画
	static CCArmature * createPendantAnm(const char * firstFontName,const char * secondFontName,const char * thirdFontName,const char * fourthFontName);

	void createRemoteStoreHouseUI();

	static CCNode * addVipInfoByLevelForNode(int level);
	static UIWidget * addVipInfoByLevelForWidget(int level);
	
	void Refresh();

	//用于添加主界面元素(例：弹出的穿装备框)
	CCLayer * getMainUIElementLayer();
	int m_addElememtLyaerTag;
	void setElementLayerOfTag(int tag_);
	int getElementLayerOfTag();
	//用于添加教学指引
	CCLayer * getMainUITuturialLayer();
	//用于添加主界面动画层
	CCLayer * getMainUIAnimationLayer();

private:
	void initVirtualJoystick();
	void newRemoteStoreHouseUI();

public:
	void ReloadTargetData(BaseFighter * baseFighter);
	void ReloadMyPlayerData();
	CCSize winSize;
	//增加跑马灯优先级
	struct marqueeStruct
	{
		std::string marqueeString;
		int marqueepriority;
	};
	std::vector<marqueeStruct> marqueeString;
	int indexMarqee;
	enum MarqueeControlTag {
		kTagMarqueeControlBaseId = 555,    
	};

	//flower marquee
	std::vector<std::string > flowerMarqueeVector;
	enum FlowerMarqueeTag {
		kTagFlowerMarqueeBaseId = 556,    
	};

	//chatui acoustic marquee
	struct chatCellInfo
	{
		std::string strCountry;
		std::string strName;
		std::string strContent;
		int vipLevel;
	};
	std::vector<chatCellInfo> chatInfoMarqueeVector;
	enum ChatAcousticMarqueeTag {
		kTagChatAcousticMarqueeBaseId = 557,    
	};




	#define FLOWERPARTICLEONROLE_TAG 266
	#define FLOWERPARTICLEONSCENE_TAG 277
	#define FLOWERPARTICLE_LASTTIME 5
	

	void addFlowerParticleOnRole(std::string flowerType);
	void removeFlowerParticleOnRole();

	void addFlowerParticleOnScene(std::string flowerType);
	void removeFlowerParticleOnScene();


private:
	CCLayer * backageLayer;
	CCLayer * mapLayer;

	//UIButton * Button_mapOpen;

	cocos2d::extension::UIButton * Button_state;

	std::vector<std::string> sysInfoData;

	cocos2d::extension::UIImageView * ImageView_vipLv;
	cocos2d::extension::UILabel * Label_allBlood;
	cocos2d::extension::UILabel * Label_curBlood;
	cocos2d::extension::UILabel * Label_allMagic;
	cocos2d::extension::UILabel * Label_curMagic;
	cocos2d::extension::UILabel * Label_name;
	cocos2d::extension::UILabel * Label_level;
	cocos2d::extension::UIButton * Button_head;
	cocos2d::extension::UIImageView * ImageView_blood;
	cocos2d::extension::UIImageView * ImageView_magic;

	
	TargetInfo * curTargetInfo;

	SceneTest * testScene;

	char * sysInfo;
	void updateSysInfo(float t);

	//button animation
	void ButtonAnimation(CCObject *pSender);

	//is remind opened
	bool isRemindOpened;
	
public:
	UILayer * targetInfoLayer;

	UILayer * mapCloseLayer;
	UILayer * generalHeadLayer;

	UILayer * ExpLayer;

	bool isHeadMenuOn;
	//cocos2d::extension::UIButton * btn_chatWindows;
	//ChatWindows * chatwindow;
	//UILayer * chatWindowsLayer;
	UILayer * chatLayer;
	MyHeadInfo * curMyHeadInfo;
	HeadMenu * headMenu;

	static GeneralsUI * GeneralsScene;
	//robotui
	bool automaticSkill;

	//经验条
	cocos2d::extension::UIImageView * ImageView_exp;
	bool isLevelUped;

	void addNewRemindOfLabBmfont(CCObject * obj);
	void removeOldRemindOfLabelBmfont(CCObject * obj);
	void addNewRemindOfTab(CCObject * obj);
	void removeOldRemindOfTab(CCObject * obj);
	void addNewRemindOfTabLine(CCObject * obj);
	void removeOldRemindOfTabLine(CCObject * obj);
	void addNewRemind(CCObject * obj);
	void removeOldRemind(CCObject * obj);
	void checkIsNewRemind();
	//remind skill
	void remindOfSkill();
	//remind general
	void remindOfGeneral();
	//remind arena
	void remindOffArena();
	//remind offArea canPrize
	void remindOffArenaPrize();
	//remind signDaily
	void remindOfSignDaily();
	//vip reward
	void remindReward();
	//offLine ex
	void remindOffLineEx();
	//mission 
	void remindMission();
	//remind mail
	void remindMail();
	//remind family apply
	void remindFamilyApply();
	//remind singCopy prize
	void remindSingCopyPrize();
	//friend get phypower
	void remindGetFriendPhypower();

	void updateOffAreaPrizeTime(float delta);
	long long m_offAreaPrizeTime;
	void setOffAreaPrizeTime(long long tempTime);
	long long getOffAreaPrizeTime();

	void reloadRemindTabllview();

	void setRemindOpened(bool value_);
	bool getRemindOpened();
	//equipment durle repair
	cocos2d::extension::UIButton * btnreminRepair;
	void repairEquipMentRemind(long long instanceId = 0);
	void showRepairEquipOfBackPack(CCObject * obj);
	void setBackPacSelectIndex(int generalIndex);
	int getBackPacSelectIndex();
	int m_backPacGeneralIndex;
};

/////////////////////////remind
class RemindUi:public UIScene,public cocos2d::extension::CCTableViewDataSource,public cocos2d::extension::CCTableViewDelegate
{
public:
	RemindUi(void);
	~RemindUi(void);

	static RemindUi * create();

	bool init();
	virtual void onEnter();
	virtual void onExit();

   // virtual void tableCellHighlight(CCTableView* table, CCTableViewCell* cell);
    //virtual void tableCellUnhighlight(CCTableView* table, CCTableViewCell* cell);
    
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
public:
	CCTableView * remind_tableView;
};


#endif

