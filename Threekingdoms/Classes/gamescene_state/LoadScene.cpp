#include "LoadScene.h"
#include "../gamescene_state/GameSceneState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../messageclient/element/CMapInfo.h"
#include "../messageclient/element/CActiveRole.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../gamescene_state/MainScene.h"
#include "../gamescene_state/sceneelement/GuideMap.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../utils/GameUtils.h"
#include "../GameUserDefault.h"
#include "../loadscene_state/LoadSceneState.h"
#include "../ui/extensions/CCRichLabel.h"
#include "../ui/offlinearena_ui/OffLineArenaState.h"
#include "../messageclient/protobuf/PlayerMessage.pb.h"  
#include "../newcomerstory/NewCommerStoryManager.h"
#include "../ui/missionscene/MissionManager.h"
#include "../ui/challengeRound/SingleCopyResult.h"
#include "../ui/Active_ui/ActiveManager.h"

#define LOAD_METHOD_ASYNC 0

#define LOADSCENE_SLIDER_NAME "slider"

using namespace CocosDenshion;

CMapInfo SceneLoadLayer::s_newMapInfo;
CCPoint SceneLoadLayer::s_myNewWorldPosition;

SceneLoadLayer::SceneLoadLayer()
: m_state(State_WaitAction)
, m_bLoadResFinished(false)
, time_(0)
, layer(NULL)
{
	setTouchEnabled(true);

	// close keyboard(change scene need close keyboard)
	CCEGLView * pGlView = CCDirector::sharedDirector()->getOpenGLView();
	if (pGlView)
	{
		pGlView->setIMEKeyboardState(false);
	}

	if(GameView::getInstance()->getMapInfo()->country())
	{
		CCUserDefault::sharedUserDefault()->setIntegerForKey(CURRENT_COUNTRY, GameView::getInstance()->getMapInfo()->country());
		CCUserDefault::sharedUserDefault()->flush();
	}

	if(GameView::getInstance()->getGameScene() == NULL)
	{
		initNextLevel();

		createUI();

		m_state = State_Login;
	}

	time_ = GameUtils::millisecondNow();

	this->scheduleUpdate();
}

void SceneLoadLayer::createUI()
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	// game logo
	CCSprite* pSprite = CCSprite::create(getLoadingImage());
	if(((float)s.width/(float)1136) > ((float)s.height/(float)640))
	{
		pSprite->setScaleX((float)s.width/(float)1136);
		pSprite->setScaleY((float)s.width/(float)1136);
	}
	else
	{
		pSprite->setScaleX((float)s.height/(float)640);
		pSprite->setScaleY((float)s.height/(float)640);
	}
	pSprite->setPosition(ccp(s.width/2, s.height/2));
	pSprite->setAnchorPoint(ccp(0.5f, 0.5f));
	addChild(pSprite, 0);

	layer= UILayer::create();

	//layer->setPosition(ccp(s.width/2,30));
	layer->setPosition(ccp(s.width/2,s.height/2));
	addChild(layer);

	float posY = s.height * 0.3f;
	UIImageView *mengban = UIImageView::create();
	mengban->setTexture("res_ui/zhezhao80_new.png");
	mengban->setScale9Enable(true);
	mengban->setScale9Size(ccp(s.width, 100));
	mengban->setAnchorPoint(ccp(0.5f,0.5f));
	mengban->setPosition(ccp(0,-posY));
	mengban->setOpacity(130);
	layer->addWidget(mengban);

	CCRichLabel *text = CCRichLabel::createWithString(getLoadingText().c_str(),CCSizeMake(s.width, 100),NULL,NULL);
	//CCLabelTTF * text=CCLabelTTF::create(getLoadingText().c_str(), APP_FONT_NAME, 20);
	text->setAnchorPoint(ccp(0.5f, 0.5f));
	text->setPosition(ccp(0,-posY));
	//ccColor3B shadowColor = ccc3(0,0,0);   // black
	//text->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	layer->addChild(text);

	// Create the slider
    UISlider* slider = UISlider::create();

	slider->setName(LOADSCENE_SLIDER_NAME);
    //slider->setTouchEnabled(true);
    slider->loadBarTexture("");
    slider->loadSlidBallTextures("res_ui/LOADING/light.png", "res_ui/LOADING/light.png", "");
    slider->loadProgressBarTexture("res_ui/progress_bar.png");
	slider->setScale9Enabled(true);
	slider->setSize(CCSizeMake(s.width, 40));
    slider->setPosition(ccp(0,-(posY+50)));
	slider->setPercent(0);

    layer->addWidget(slider);
	/*
	CCLegendAnimation* pBird = CCLegendAnimation::create("animation/texiao/jiemiantexiao/bird/fly.anm");
	pBird->setPlayLoop(true);
	pBird->setReleaseWhenStop(false);
	pBird->setAnchorPoint(ccp(0.5f,0.5f));
	pBird->setPosition(ccp(s.width/2 - 20, -(posY+70)));
	pBird->setScale(0.33f);
	layer->addChild(pBird);
	*/
//  CCSprite* pSprite = CCSprite::create("images/mm01.png");
//  pSprite->setPosition(ccp(s.width/2, s.height/2));
// 	pSprite->setAnchorPoint(ccp(0.5f, 0.5f));
//	addChild(pSprite, 0);

//     m_pLabelLoading = CCLabelTTF::create("loading...", "Arial", 50);
//     m_pLabelLoading->setPosition(ccp(s.width / 2, 40));
//     this->addChild(m_pLabelLoading);

#if LOAD_METHOD_ASYNC
	m_pLabelPercent = CCLabelTTF::create("%0", APP_FONT_NAME, 50);
    m_pLabelPercent->setPosition(ccp(s.width / 2, s.height / 2 + 20));
	ccColor3B shadowColor = ccc3(0,0,0);   // black
	m_pLabelPercent->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
    this->addChild(m_pLabelPercent);
#endif
}

void SceneLoadLayer::loadingCallBack(CCObject *obj)
{
    ++m_nNumberOfLoadedSprites;
    char tmp[10];
    sprintf(tmp,"%%%d", (int)(((float)m_nNumberOfLoadedSprites / m_nNumberOfSprites) * 100));
    m_pLabelPercent->setString(tmp);

    if (m_nNumberOfLoadedSprites == m_nNumberOfSprites)
    {
		m_bLoadResFinished = true;
    }
}

int SceneLoadLayer::getState()
{
	return m_state;
}

void SceneLoadLayer::setState(int state)
{
	m_state = state;
}

void SceneLoadLayer::update(float dt)
{
	static int _tick = 0;
	UISlider* slider = NULL;
	if(layer != NULL)
		slider = (UISlider*)(layer->getWidgetByName(LOADSCENE_SLIDER_NAME));
	if(m_state == State_WaitAction)
	{
		if(GameUtils::millisecondNow() - time_ > 1.0f * 350)
		{
			createUI();

			m_state = State_Init;
		}
	}
	else if(m_state == State_Login)
	{
		//do nothing
	}
	else if(m_state == State_Init)
	{
		initNextLevel();

		// remove the game scene
		GameSceneState* pGameSceneState = (GameSceneState*)this->getParent();
		CCNode* pGameSceneNode = pGameSceneState->getChildByTag(GameSceneState::TAG_GAMESCENE);
		if(pGameSceneNode != NULL)
		{
			pGameSceneNode->pauseSchedulerAndActions();
			pGameSceneNode->removeFromParent();
		}

		slider->setPercent(20);
		m_state = State_LoadResStep_0;
	}
	else if(m_state == State_LoadResStep_0)
	{
		loadCommonRes();
		slider->setPercent(40);
		m_state = State_LoadResStep_1;
	}
	else if(m_state == State_LoadResStep_1)
	{
		loadUIWidgetRes();
		slider->setPercent(55);
		m_state = State_LoadResStep_2;
	}
	else if(m_state == State_LoadResStep_2)
	{
		preLoadResource(GameView::getInstance()->getMapInfo()->mapid().c_str());
		slider->setPercent(80);
		m_state = State_LoadResStep_3;
	}
	else if(m_state == State_LoadResStep_3)
	{
		slider->setPercent(99);

		if(m_bLoadResFinished)
		{
			GameSceneState* pGameSceneState = (GameSceneState*)this->getParent();
			pGameSceneState->prepareGameState();

			_tick = 0;
			m_state = State_Finished;
		}
	}
	else if(m_state == State_Finished)
	{
		//this->removeChild(m_pLabelLoading, true);
        //this->removeChild(m_pLabelPercent, true);
		if(_tick++ > 30)
		{
			GameSceneState* pGameSceneState = (GameSceneState*)this->getParent();
			pGameSceneState->changeToGameState();
		}
	}
	else if(m_state == State_Idle)
	{
		//lazy...
	}
}

bool SceneLoadLayer::isFinishedState()
{
	if(m_state == State_Finished)
		return true;

	return false;
}

void SceneLoadLayer::preLoadResource(const char *path)
{
	//std::vector<std::string>* sceneResList = GameSceneLayer::generateResourceList("xsc.level");
	std::vector<std::string>* sceneResList = GameSceneLayer::generateResourceList(path);

	m_nNumberOfSprites = sceneResList->size();;
	m_nNumberOfLoadedSprites = 0;

	// load textrues
	vector<std::string>::iterator iter;
	for (iter = sceneResList->begin(); iter != sceneResList->end(); ++iter)
	{
#if LOAD_METHOD_ASYNC
		// addImageAsync() method maybe has memory lead, so please pay attention to it
		CCTextureCache::sharedTextureCache()->addImageAsync((*iter).c_str(), this, callfuncO_selector(LoadSceneLayer::loadingCallBack));
#else
		// synchronize load resource, please set finished flag immediately
		CCTextureCache::sharedTextureCache()->addImage((*iter).c_str());
		m_bLoadResFinished = true;
#endif
	}

	delete sceneResList;
}

std::string SceneLoadLayer::getLoadingText()
{
	unsigned int size = LoadingTextData::s_LoadingTexts.size();
	if(size <= 0)
	{
		LoadingTextData::load("game.db");
		size = LoadingTextData::s_LoadingTexts.size();
	}

	unsigned int index = GameUtils::getRandomNum(size);
	//CCLog("index: %d, text: %s", index, LoadingTextData::s_LoadingTexts.at(index).c_str());
	CCAssert(index >= 0 && index < size, "out of range");
	return LoadingTextData::s_LoadingTexts.at(index);
}

char* SceneLoadLayer::getLoadingImage()
{
	unsigned int size = 4;
	char* image[] = {"images/loding1.jpg", "images/loding2.jpg", "images/loding3.jpg", "images/loding4.jpg"};
	unsigned int index = GameUtils::getRandomNum(size);
	CCAssert(index >= 0 && index < size, "out of range");
	return image[index];
}

void SceneLoadLayer::loadCommonRes()
{
}

void SceneLoadLayer::loadUIPanelFromJson(UIPanel** dest, const char* fileName)
{
	if(*dest == NULL)
	{
		long long currentTime = GameUtils::millisecondNow();

		std::string fullPathName = "res_ui/";
		fullPathName.append(fileName);

		*dest = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile(fullPathName.c_str());
		(*dest)->retain();

		long deltaTime = GameUtils::millisecondNow() - currentTime;
		CCLOG("load UI, cost time: %d", deltaTime);
	}
}

void SceneLoadLayer::loadUIWidgetRes()
{
	loadUIPanelFromJson(&LoadSceneLayer::s_pPanel, "renwubeibao_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::SkillSceneLayer, "jineng_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::equipMentPanel, "zhuangbei_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::onlineAwardsUIPanel, "OnlineAwards_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::GeneralsUILayer, "wujiang_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::friendPanelBg, "haoyou_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::auctionPanel, "jishouhang_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::mapPanel, "map_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::familyMainPanel, "family_main_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::signDailyUIPanel, "Registration_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::mailPanel, "youjian_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::onHookKillPanel, "on_hook_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::vipEveryDayRewardPanel, "vip1_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::activeUIPanel, "Activity_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::OffLineArenaLayer, "arena_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::SingleRecuriteResultLayer, "wujiang_get_popupo_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::TenTimesRecuriteResultLayer, "wujiang_SuperGet_popupo_1.json");
}

void SceneLoadLayer::initNextLevel()
{
	GameView* gv = GameView::getInstance();

	//set last mapId
	gv->getMapInfo()->setLastMapId(gv->getMapInfo()->mapid());

	gv->getMapInfo()->CopyFrom(s_newMapInfo);

	// init MyPlayer
	gv->myplayer->clear();	
	gv->myplayer->setDissociate(false);
	gv->myplayer->setGameScene(NULL);
	gv->myplayer->setWorldPosition(s_myNewWorldPosition);

	//add by yangjun 2014.9.22
	if (gv->getMapInfo()->mapid() == "mjrk1.level")
	{
		if (NewCommerStoryManager::getInstance()->m_storyState == NewCommerStoryManager::ss_notStart)
		{
			NewCommerStoryManager::getInstance()->setIsNewComer(true);
			NewCommerStoryManager::getInstance()->startStory();
		}
	}

	//add by yangjun 2014.11.17
	if (NewCommerStoryManager::getInstance()->IsNewComer())
	{
		if (gv->getMapInfo()->mapid() != "mjrk1.level"&&gv->getMapInfo()->mapid() != "smjt.level")
		{
			NewCommerStoryManager::getInstance()->endStory();
		}
	}

	if (s_newMapInfo.maptype() == coliseum)
	{
		OffLineArenaState::setIsCanTouch(false);
	}

	if (MissionManager::getStatus() == MissionManager::status_waitForTransport)
	{
		MissionManager::setStatus(MissionManager::status_transportFinished);
	}

	if (ActiveManager::get_status() == ActiveManager::status_waitForTransport)
	{
		ActiveManager::set_status(ActiveManager::status_transportFinished);
	}

	if (s_newMapInfo.maptype() == tower)
	{
		SingleCopyResult * copyResultui = (SingleCopyResult*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyResutUi);
		if (copyResultui)
		{
			copyResultui->closeAnim();
		}
	}
}