#include "GameSceneState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../legend_engine/CCLegendTiledMap.h"
#include "../legend_engine/LegendLevel.h"
#include "../legend_engine/LegendAnimationCache.h"
#include "../utils/JoyStick.h"
#include "../utils/GameUtils.h"
#include "../utils/pathfinder/AStarTiledMap.h"
#include "../utils/pathfinder/AstarPathFinder.h"
#include "MainScene.h"

#include "./role/DecorationActor.h"
#include "./role/Monster.h"
#include "./role/MyPlayer.h"
#include "./role/MyPlayerSimpleAI.h"
#include "./role/MyPlayerAIConfig.h"
#include "./role/MyPlayerAI.h"
#include "./role/General.h"
#include "./role/SimpleFighter.h"
#include "role/FunctionNPC.h"
#include "role/OtherPlayer.h"
#include "role/OtherPlayerOwnedStates.h"
#include "role/MyPlayerOwnedStates.h"
#include "role/MyPlayerOwnedCommand.h"
#include "role/SimpleActor.h"
#include "role/BaseFighterOwnedCommand.h"
#include "./role/ActorCommand.h"
#include "skill/SkillManager.h"
#include "skill/GameFightSkill.h"
#include "GameSceneMusicController.h"
#include "../GameView.h"
#include "role/GameActorAnimation.h"
#include "../messageclient/element/CMapInfo.h"
#include "../messageclient/element/DoorInfo.h"
#include "../messageclient/element/MapInfo.h"
#include "../messageclient/element/MapDoor.h"
#include "../legend_engine/GameWorld.h"
#include "../ui/missionscene/MissionManager.h"

#include "../ui/UITest.h"
#include "../messageclient/element/CNpcDialog.h"
#include "../ui/npcscene/NpcTalkWindow.h"
#include "../login_state/LoginState.h"
#include "../messageclient/GameMessageProcessor.h"
#include "sceneelement/shortcutelement/ShortcutLayer.h"
#include "../messageclient/element/CActiveRole.h"
#include "role/PickingActor.h"
#include "../login_state/Login.h"
#include "../logo_state/LogoState.h"
#include "../login_state/LoginStateTest.h"
#include "../legend_script/ScriptManager.h"
#include "../ui/missionscene/MissionTalkWithNpc.h"
#include "../ui/missionscene/HandleMission.h"
#include "../messageclient/element/CDrug.h"
#include "../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "GameScene.h"
#include "GameSceneCamera.h"
#include "../ui/Active_ui/ActiveMissonManager.h"
#include "../messageclient/element/CEquipment.h"
#include "role/ActorUtils.h"
#include "../utils/GameUtils.h"
#include "GameAudio.h"
#include "exstatus/ExStatusType.h"
#include "exstatus/ExStatus.h"
#include "exstatus/ExStatusFactory.h"
#include "../ui/offlinearena_ui/OffLineArenaState.h"
#include "LoadScene.h"
#include "MainScene.h"
#include "../ui/FivePersonInstance/FivePersonInstance.h"
#include "../ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "role/PresentBox.h"
#include "../ui/mapscene/MapScene.h"
#include "../ui/mapscene/MapAction.h"
#include "../newcomerstory/NewCommerStoryManager.h"
#include "../legend_script/Script.h"
#include "../messageclient/element/CGeneralBaseMsg.h"
#include "sceneelement/GeneralsInfoUI.h"
#include "role/BaseFighterConstant.h"
#include "EffectDispatch.h"
#include "MainAnimationScene.h"

/**
game scene role id 规则:
	 player: 从1 ~ 开始
	 NPC: 从-1 开始
	 monster: 从-10000开始
	 general: 从5000000开始
	 剧情用角色: 从-5000开始 （客户端用）
*/

#define TILEDMAP_COVERLAYER_ID 1   // 遮挡层

USING_NS_CC;

GameSceneState::GameSceneState()
:mainScene(NULL),
mainAnmScene(NULL)
{
	this->setTag(GameView::STATE_GAME);
}

void GameSceneState::runThisState()
{
	changeToLoadState();

    CCDirector::sharedDirector()->replaceScene(this);
}

bool GameSceneState::canHandleMessage()
{
	// do not handle message when it's loading state
	if(m_state == state_load)
	{
		SceneLoadLayer* pLoadSceneNode = (SceneLoadLayer*)this->getChildByTag(GameSceneState::TAG_LOADSCENE);
		if(pLoadSceneNode != NULL && 
			(pLoadSceneNode->isFinishedState()
			||pLoadSceneNode->getState() == SceneLoadLayer::State_Login))
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	return true;
}

int GameSceneState::getState()
{
	return m_state;
}

void GameSceneState::changeToLoadState()
{
	//CCNode* pGameSceneNode = this->getChildByTag(GameSceneState::TAG_GAMESCENE);
	//if(pGameSceneNode != NULL)
	//	pGameSceneNode->removeFromParent();

    CCLayer* pLayer = new SceneLoadLayer();
    addChild(pLayer,3,GameSceneState::TAG_LOADSCENE);
	pLayer->release();

	GameSceneLayer* scene = dynamic_cast<GameSceneLayer*>(this->getChildByTag(GameSceneState::TAG_GAMESCENE));
	if(scene != NULL)
	{
        CCPoint tmpPoint = GameView::getInstance()->myplayer->getWorldPosition();
		scene->getSceneCamera()->zoom(scene, tmpPoint, 1.0f, 0.7f, 1.0f);
	}

	m_state = state_load;
}

void GameSceneState::prepareGameState()
{
	// release the previous scene's resource !!!
	CCDirector::sharedDirector()->purgeCachedData();

	// add MainScene once
	if(mainScene == NULL)
	{
		mainScene = MainScene::create();
 		mainScene->setAnchorPoint(CCPointZero);
 		mainScene->setPosition(CCPointZero);
		addChild(mainScene,1,GameSceneState::TAG_MAINSCENE);
	}
	mainScene->Refresh();
	// when playing the movie, the MainScene will be invisible. 
	// when enter new scene, MainScene should be visible again.
	mainScene->setVisible(true);   
	
	// add MainAnimationScene once
	if(mainAnmScene == NULL)
	{
		mainAnmScene = MainAnimationScene::create();
		mainAnmScene->setAnchorPoint(CCPointZero);
		mainAnmScene->setPosition(CCPointZero);
		addChild(mainAnmScene,2,GameSceneState::TAG_ANIMATIONSCENE);
	}

	// MainScene should be called before GameSceneLayer::create()
	CCLayer* pLayer = GameSceneLayer::create();
	addChild(pLayer,0,GameSceneState::TAG_GAMESCENE);

	//add effect scene
	if (this->getChildByTag(1011) == NULL)
	{
		EffectDispatcher * effectScene = EffectDispatcher::create();
		effectScene->setTag(1011);
		addChild(effectScene);
	}
	
	//切地图后刷新主界面武将区域
	GeneralsInfoUI * temp = (GeneralsInfoUI*)mainScene->getGeneralInfoUI();
	if(temp)
		temp->RefreshAllGeneralItem();

	// show UI
	if(ScriptManager::getInstance()->isInMovieMode())
	{
		mainScene->setVisible(true);
	}

	// record the map which has been reached
	if(!GameView::getInstance()->myplayer->isMapReached(GameView::getInstance()->getMapInfo()->country(), GameView::getInstance()->getMapInfo()->mapid().c_str()))
	{
		GameView::getInstance()->myplayer->addCurrentReachedMap(GameView::getInstance()->getMapInfo()->country(), GameView::getInstance()->getMapInfo()->mapid());
	}

	MapScene * map = (MapScene*)mainScene->getChildByTag(kTagMap);
	if(map)
	{
		map->setCurrentMapId(GameView::getInstance()->getMapInfo()->mapid());
		map->reloadAreaMap(GameView::getInstance()->getMapInfo()->mapid());
		map->initMapButton();
	}


	// when enter the scene, notify the game server
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1113, this);
}
void GameSceneState::changeToGameState()
{
	// remove load scene
	CCNode* pLoadSceneNode = this->getChildByTag(GameSceneState::TAG_LOADSCENE);
	if(pLoadSceneNode != NULL)
		pLoadSceneNode->removeFromParent();

	//prepareGameState();

	//// begin to update()
	//CCNode* pSceneLayer = this->getChildByTag(GameSceneState::TAG_GAMESCENE);
	//pSceneLayer->scheduleUpdateWithPriority(1);

	//// when enter the scene, notify the game server
	//GameMessageProcessor::sharedMsgProcessor()->sendReq(1113, this);

	GameSceneLayer* scene = dynamic_cast<GameSceneLayer*>(this->getChildByTag(GameSceneState::TAG_GAMESCENE));
	if(scene != NULL)
	{
        CCPoint tmpPoint = GameView::getInstance()->myplayer->getWorldPosition();
		scene->getSceneCamera()->zoom(scene, tmpPoint, 0.93f, 1.0f, 1.0f);
	}

	// show the map's name
	MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
	if(mainAnmScene != NULL)
	{
		CCNodeRGBA* pRootNode = CCNodeRGBA::create();
		pRootNode->setCascadeOpacityEnabled(true);

		int countryId = GameView::getInstance()->getMapInfo()->country();

		// map's name
		CCLabelTTF* mapNameLabel = CCLabelTTF::create(GameView::getInstance()->getMapInfo()->mapname().c_str(), APP_FONT_NAME, 30);
		mapNameLabel->setAnchorPoint(ccp(0.5f, 0.5f));
		if (countryId > 0 && countryId < 6)
		{
			int myCountryId = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().country();
			if(myCountryId != countryId)
				mapNameLabel->setColor(ccc3(255, 0, 0));
		}
		pRootNode->addChild(mapNameLabel, 1);

		// country icon
		if (countryId > 0 && countryId < 6)
		{
			std::string countryIdName = "country_";
			char id[2];
			sprintf(id, "%d", countryId);
			countryIdName.append(id);
			std::string iconPathName = "res_ui/country_icon/";
			iconPathName.append(countryIdName);
			iconPathName.append(".png");
			CCSprite* pIconSprite = CCSprite::create(iconPathName.c_str());
			if (pIconSprite)
			{
				pIconSprite->setAnchorPoint(ccp(0.5f, 0.5f));
				float x = mapNameLabel->getPositionX() - mapNameLabel->getContentSize().width/2 - pIconSprite->getContentSize().width/2;
				pIconSprite->setPositionX(x);
				pRootNode->addChild(pIconSprite, 1);
			}
		}

		CCScale9Sprite * background_ = CCScale9Sprite::create("res_ui/paoMaDeng_di.png");
		background_->setCapInsets(CCRect(0,0,0,0));
		background_->setAnchorPoint(ccp(0.5f, 0.5f));
		int width = mapNameLabel->getContentSize().width + 110;
		background_->setPreferredSize(CCSizeMake(width, 45));   // 60
		pRootNode->addChild(background_, 0);

		// fade in an fade out
		CCAction* action = CCSequence::create(
			CCFadeIn::create(0.5f),
			CCDelayTime::create(2.0f),
			CCFadeOut::create(1.0f),
			CCRemoveSelf::create(),
			NULL);
		pRootNode->runAction(action);

		CCSize s = CCDirector::sharedDirector()->getVisibleSize();
		pRootNode->setPosition(ccp(s.width/2, s.height*0.7f));
		mainAnmScene->addChild(pRootNode);
	}

	m_state = state_game;
}

bool GameSceneState::isInLoadState()
{
	if(m_state == state_load)
		return true;

	return false;
}
bool GameSceneState::isInGameState()
{
	if(m_state == state_game)
		return true;

	return false;
}

void GameSceneState::backToLoginState(float dt)
{
#if THREEKINGDOMS_SERVERLIST_MODE
    CCScene *pScene = CCScene::create();
    Login *pLayer = new Login();
    pScene->addChild(pLayer);
    
    CCDirector::sharedDirector()->replaceScene(pScene);
    pLayer->release();
#else
		// create the next scene and run it
    GameState* pScene = new LoginTestState();
    if (pScene)
    {
        pScene->runThisState();
        pScene->release();
    }
#endif
}

void GameSceneState::onSocketOpen(ClientNetEngine* socketEngine)
{
}
void GameSceneState::onSocketClose(ClientNetEngine* socketEngine)
{
	schedule( schedule_selector(GameSceneState::backToLoginState), 2); 
}
void GameSceneState::onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error)
{
}

//////////////////////////////////////////////////////////////////////////////

AStarPathFinder* GameSceneLayer::s_pathFinder = NULL;

GameSceneLayer::GameSceneLayer()
: m_tiledmap(NULL)
, m_actors(NULL)
, m_pSkillManager(NULL)
, m_bInitMovieScene(false)
, m_curLevelData(NULL)
, m_bSynchronizeCocosPosition(false)
, m_pMusicController(NULL)
{
	setTouchEnabled(true);

	scheduleUpdateWithPriority(1);

	if(s_pathFinder == NULL)
		s_pathFinder = new AStarPathFinder();
}

GameSceneLayer::~GameSceneLayer()
{
	LegendAAnimationCache::sharedLegendAnimationCache()->purgeSharedAnimationCache();
	CCLegendAnimationCache::sharedCache()->purgeSharedCache();

	CC_SAFE_DELETE(m_curLevelData);

	CC_SAFE_RELEASE(m_actors);

	CC_SAFE_DELETE(m_searchMap);

	// release all actors
    for (std::map<long long,GameActor*>::iterator it = m_ActorsMap.begin(); it != m_ActorsMap.end(); ++it) 
	{
		GameActor* actor = it->second;
		if(actor != NULL)
			actor->setGameScene(NULL);
		CC_SAFE_RELEASE(actor);
	}
	m_ActorsMap.clear();
	m_pMyPlayer->setGameScene(NULL);

	for (std::vector<GameActor*>::iterator it = m_ClientActorsVector.begin(); it != m_ClientActorsVector.end(); ++it) 
	{
		CC_SAFE_RELEASE(*it);
	}
	m_ClientActorsVector.clear();

	CC_SAFE_DELETE(m_pSkillManager);

	GameScene::s_bornMonsterMap.clear();

	CC_SAFE_DELETE(m_pMusicController);
}

GameSceneLayer* GameSceneLayer::create()
{
	GameSceneLayer *scene = new GameSceneLayer();
	if (scene && scene->init())
	{
		scene->autorelease();
		return scene;
	}
	CC_SAFE_DELETE(scene);
	return NULL;
}

bool GameSceneLayer::init()
{
	m_pSkillManager = new SkillManager();
	m_pMusicController = new GameSceneMusicController();

	LoadScene();
	//LoadMovieScene();   // must after LoadScene()

	return true;
}

void GameSceneLayer::initMyPlayer()
{
	m_pMyPlayer = GameView::getInstance()->myplayer;
	m_pMyPlayer->setMoveSpeed(BASIC_PLAYER_MOVESPEED);

	m_pMyPlayer->setGameScene(this);
	m_pMyPlayer->removeAllChildren();

	// init MyPlayer's body animation
	std::string body = "";
	if(m_pMyPlayer->getActiveRole()->has_body())
		body = m_pMyPlayer->getActiveRole()->body();
	m_pMyPlayer->init(body.c_str());
	m_pMyPlayer->setActorName(m_pMyPlayer->getActiveRole()->rolebase().name().c_str());
	m_pMyPlayer->showActorName(true);

	// 根据坐标位置，更新显示状态
	m_pMyPlayer->updateWaterStatus();
	m_pMyPlayer->updateCoverStatus();

	m_pMyPlayer->GetFSM()->ChangeState(MyPlayerStand::Instance());   // 默认站立状态

	m_pMyPlayer->initWeaponEffect();

	// 切换场景，或者从剧情模式切换回来等情况下，都需要对挂机模块进行重置
	if (MyPlayerAIConfig::isEnableRobot() || MyPlayerAIConfig::getAutomaticSkill() == 1)
	{
		GameView::getInstance()->myplayer->getMyPlayerAI()->start();
	}

	// get the virtual joystick
	Joystick* pJoystick = (Joystick*)this->getMainUIScene()->getChildByTag(kTagVirtualJoystick);
	m_pMyPlayer->setJoyStick(pJoystick);

	//set robot default skill
	for (int i=0;i<ROBOT_SETUP_SKILL_MAX;i++)
	{
		char skillindex[3];
		sprintf(skillindex,"%d",i);
		std::string playerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
		std::string skillName_ = playerName;
		skillName_.append(ROBOT_SETUP_SKILL_ID_PREFIX);
		skillName_.append(skillindex);

		std::string skillId_ = CCUserDefault::sharedUserDefault()->getStringForKey(skillName_.c_str());
		if(skillId_.size() > 0)
			GameView::getInstance()->myplayer->getMyPlayerAI()->m_skillList.push_back(skillId_);
	}

	// init the exstatus ( buff )
	std::vector<ExStatus*>& oldStatus = m_pMyPlayer->getExStatusVector();
	for (std::vector<ExStatus*>::iterator it = oldStatus.begin(); it != oldStatus.end(); ++it) 
	{
		ExStatus* status = (*it);

		ExtStatusInfo statusInfo;
		statusInfo.set_duration(status->duration());
		statusInfo.set_remaintime(status->remaintime());
		statusInfo.set_type(status->type());

		ExStatus* newStatus = ExStatusFactory::getInstance()->getExStatus(statusInfo.type(), statusInfo.duration(), statusInfo.remaintime());
		newStatus->setFighter(m_pMyPlayer);
		newStatus->onAdd(&statusInfo);
		delete newStatus;
	}

	m_pMyPlayer->setPKStatus(false);

	// patch
	// add by yangjun 2014.9.13
	// 主角武器有特效
	if (NewCommerStoryManager::getInstance()->IsNewComer())
	{
		NewCommerStoryManager::getInstance()->setMyPlayerEffect(2);
	}
	//add flower particle
	if (GameView::getInstance()->getFlowerParticleTime() > 0)
	{
		ActorUtils::addFlowerParticle(m_pMyPlayer);
	}
}

void GameSceneLayer::LoadScene( )
{
	// load level
	m_curLevelData = new LegendLevel();
	LegendLevel* pLevelData = m_curLevelData;

	std::string levelFile = "level/";
	levelFile.append(GameView::getInstance()->getMapInfo()->mapid());
	pLevelData->load(levelFile.c_str());   // rom_huanggong.level

	m_actors = CCArray::createWithCapacity(50);
	m_actors->retain();
	// DecorationActor
	std::vector<DecorationActor*> allTeleports;
	for(int i = 0; i < pLevelData->decorationActorsNumber; i++)
	{
		DecorationActor* actor = new DecorationActor();
		//actor->autorelease();
		actor->loadWithInfo(pLevelData->decorationActorsInfo[i]);
		m_actors->addObject(actor);

		// get the teleport actor
		int actorClassId = pLevelData->decorationActorsInfo[i]->actorClassId;
		ActorClass* ac = GameMetaData::getActorClass(actorClassId);
		if(ac->aniName.find("csm") > 0)
		{
			allTeleports.push_back(actor);
		}
	}

	// Scene Layer include tiledmap and actors
	CCLayer* sceneLayer = CCLayer::create();
	addChild(sceneLayer, 0, kTagSceneLayer);
	sceneLayer->setPosition(CCPointZero);

	m_tiledmap = new CCLegendTiledMap();
	//m_tiledmap->initWithTiledmapFilename("background/xsc/xsc.map");
	//std::string mapName = "";
	//mapName.append("background/");
	//mapName.append(m_levelData->tiledmapName);
	//m_tiledmap->initWithTiledmapFilename(mapName.c_str());
	m_tiledmap->initWithTiledmapFilename(pLevelData->tiledmapName.c_str());
	m_mapSize = m_tiledmap->getTiledMapData()->getMapSize();
	m_mapSize.height = m_mapSize.height*2;   // 转换为游戏世界坐标下的尺寸

	m_tiledmap->setPosition(CCPointZero);

	sceneLayer->addChild(m_tiledmap, 0, kTagTiledMapNode);
	m_tiledmap->release();

	generateSearchMap();

	//m_pathFinder = new AStarPathFinder();
	s_pathFinder->setMap(this->m_searchMap->getData(), 
		m_searchMap->getHorizontalTilesNum(), m_searchMap->getVerticalTilesNum());

	/*
	short start[2] = {40, 7};   // y, x
	short end[2] = {29, 35};
	std::vector<short*> *path = m_pathFinder->searchPath(start, end);
	// 展示寻路结果
    vector<short*>::iterator iter;
    for (iter = path->begin(); iter != path->end(); ++iter)
    {
		short x = (*iter)[1];
		short y = (*iter)[0];

		CCSprite* point = CCSprite::create("point.png");
		point->setScale(0.3f);
		sceneLayer->addChild(point, SCENE_UI_LAYER_BASE_ZORDER);

		//CCPoint worldPos = ccp(m_mapSize.width/2, m_mapSize.height/2);
		CCPoint worldPos = ccp(this->tileToPositionX(x), this->tileToPositionY(y));

		point->setPosition(convertToCocos2DSpace(worldPos));
    }
	delete path;
	*/

	// normal actor layer
	CCLayerRGBA* actorLayer = CCLayerRGBA::create();
	actorLayer->setCascadeOpacityEnabled(true);
    sceneLayer->addChild(actorLayer, 1, kTagActorLayer);

	// movie actor layer
	CCLayerRGBA* movieActorLayer = CCLayerRGBA::create();
	movieActorLayer->setVisible(false);   // default visible is false
	movieActorLayer->setCascadeOpacityEnabled(true);
    sceneLayer->addChild(movieActorLayer, 1, kTagMovieActorLayer);

	// add all decoration actors
    CCObject* pObj = NULL;
    CCARRAY_FOREACH(m_actors, pObj)
    {
        DecorationActor* actor = (DecorationActor*)pObj;
		actor->setPosition(convertToCocos2DSpace(actor->getWorldPosition()));
		actorLayer->addChild(actor, getActorZOrder(actor));   // 根据排序点，设置ZOrder
		actor->release();
	}

	// add all NPCs
	GameView* gv = GameView::getInstance();
    for (std::vector<CActiveRole*>::iterator iter = gv->allNPCsVector.begin(); iter != gv->allNPCsVector.end(); ++iter) 
	{
		FunctionNPC* npc = FunctionNPC::create(*iter, this);
		CCAssert(npc != NULL, "should not be null");
	}
	// FunctionNPC Actor
	for(int i = 0; i < pLevelData->npcActorsNumber; i++)
	{
		FunctionNPC* actor = new FunctionNPC();
		actor->setGameScene(this);
		actor->loadWithInfo(pLevelData->npcActorsInfo[i]);

		actor->setPosition(convertToCocos2DSpace(actor->getWorldPosition()));
		actorLayer->addChild(actor, getActorZOrder(actor));   // 根据排序点，设置ZOrder
		actor->release();

		actor->RefreshNpc();

		this->putActor(actor->getRoleId(), actor);
	}

	// add myplayer
	initMyPlayer();
	actorLayer->addChild(m_pMyPlayer, getActorZOrder(m_pMyPlayer));

	

	// add teleport's name
	// 根据当前地图名，获取传送门的总数
	std::string currentMapId = GameView::getInstance()->getMapInfo()->mapid();
	MapInfo* pMapInfo = GameWorld::MapInfos[currentMapId];
	if(pMapInfo != NULL)   // 副本地图可能没有传送门
	{
		for(unsigned int i = 0; i < pMapInfo->doors.size(); i++)
		{
			std::string mapId = pMapInfo->doors[i]->mapId;
			CCPoint centerPoint = getDoorPosition(currentMapId, mapId);

			// patch, find the nearest teleport actor
			DecorationActor* actor = NULL;
			for(unsigned int j = 0; j < allTeleports.size(); j++)
			{
				DecorationActor* tmpActor = allTeleports.at(j);
				if(ccpDistance(centerPoint, tmpActor->getWorldPosition()) < 64 * 3)
				{
					actor = tmpActor;
					break;
				}
			}
			if(actor != NULL)
			{
				centerPoint = actor->getWorldPosition();
			}
			centerPoint.y -= 128;

			// 获取传送门的指向的地图名
			std::string& mapName = StaticDataMapName::s_mapname[mapId];

			CCLabelTTF* labelMapName = CCLabelTTF::create(mapName.c_str(), APP_FONT_NAME, 30);
			labelMapName->setAnchorPoint(ccp(0.5f,0.5f));
			//ccColor3B color = ccc3(0, 255, 0);
			//labelMapName->enableShadow(CCSizeMake(0,0), 1.0f, 1.0f, color);
			labelMapName->setPosition(convertToCocos2DSpace(centerPoint));
			actorLayer->addChild(labelMapName, SCENE_TOP_LAYER_BASE_ZORDER + 5000);
		}
	}

	// add the scene made by cocostudio ( *.json )
	// make the scene json file name ( xsc.map -> xsc.json )
	std::string sceneJsonFileName = pLevelData->tiledmapName;
	int mapSuffixIndex = sceneJsonFileName.find(".map");
	sceneJsonFileName = pLevelData->tiledmapName.substr(0, mapSuffixIndex);
	sceneJsonFileName.append(".json");
	// load json, for example, "scenetest1/TestScene.json"
	if(CCFileUtils::sharedFileUtils()->isFileExist(sceneJsonFileName))
	{
		CCNode *pJsonScene = SceneReader::sharedSceneReader()->createNodeWithSceneFile(sceneJsonFileName.c_str());
		pJsonScene->setPosition(ccp(0,0));
		//actorLayer->addChild(pJsonScene, SCENE_ROLE_LAYER_BASE_ZORDER + 3000);
		sceneLayer->addChild(pJsonScene, SCENE_ROLE_LAYER_BASE_ZORDER + 3000);
	}

	// test case: other player
	//OtherPlayer* otherPlayer = new OtherPlayer();
	//otherPlayer->setMoveSpeed(BASIC_PLAYER_MOVESPEED);
	//otherPlayer->setWorldPosition(ccp(m_pMyPlayer->getWorldPosition().x+100, m_pMyPlayer->getWorldPosition().y));
	//otherPlayer->setPosition(convertToCocos2DSpace(otherPlayer->getWorldPosition()));
	//otherPlayer->setGameScene(this);
	//otherPlayer->setRoleId(100);
	//otherPlayer->GetFSM()->ChangeState(OtherPlayerStand::Instance());   // 默认站立状态
	//actorLayer->addChild(otherPlayer, getActorZOrder(otherPlayer), 1001);
	//otherPlayer->release();

	//otherPlayer->retain();
	//m_ActorsMap[100] = otherPlayer;

	////BaseFighter* test = getBaseFighter(otherPlayer->getRoleId());
	//BaseFighter* test = (BaseFighter*)getActor(101);

	//actorLayer->setPosition(-(m_pMyPlayer->getPositionX() - s.width/2), -(m_pMyPlayer->getPositionY() - s.height/2));
	//actorLayer->setPosition(mapCenter);
	//actorLayer->setPosition(ccp(0,0));
	//actorLayer->setPosition(ccp(-m_mapSize.width/2 + s.width/2, -m_mapSize.height/2+s.height/2));

	// init camera;
	m_pCamera = GameSceneCamera::create(this, GameSceneCamera::camera_behaviour_follow_actor);
	this->addChild(m_pCamera);

	// scene position, according by MyPlayer's position
	//setView(sceneLayer, m_pMyPlayer->getPosition());
	if(m_pCamera->getBehaviour() == GameSceneCamera::camera_behaviour_follow_actor)
	{
		// patch, synchronize the postion forcely
		CCPoint cocosPos = this->convertToCocos2DSpace(m_pMyPlayer->getWorldPosition());
		m_pMyPlayer->setPositionImmediately(cocosPos);

		setView(sceneLayer, m_pCamera->getSceneCameraPosition(m_pMyPlayer));
	}

	// delete level data at last
	//delete pLevelData;
}

void GameSceneLayer::LoadMovieScene( )
{
	if(m_curLevelData == NULL)
		return;

	if(m_bInitMovieScene)
		return;

	m_bInitMovieScene = true;

	// add all decoration actors
	for(int i = 0; i < m_curLevelData->decorationActorsNumber; i++)
	{
		DecorationActor* actor = new DecorationActor();
		actor->loadWithInfo(m_curLevelData->decorationActorsInfo[i]);
		
		actor->setPosition(convertToCocos2DSpace(actor->getWorldPosition()));

		// add to movie actor layer
		this->getMovieActorLayer()->addChild(actor);
		actor->release();
	}

	// add all picking actor
	for (int i = 0; i < m_curLevelData->pickingActorsNumber; i++) 
	{
		PickingActor* actor = new PickingActor();
		actor->loadWithInfo(m_curLevelData->pickingActorsInfo[i]);
		actor->setPosition(this->convertToCocos2DSpace(actor->getWorldPosition()));
		actor->setGameScene(this);

		// add to movie actor layer
		this->getMovieActorLayer()->addChild(actor);
		actor->release();
	}

	//// FunctionNPC Actor
	//for(int i = 0; i < m_curLevelData->npcActorsNumber; i++)
	//{
	//	FunctionNPC* actor = new FunctionNPC();
	//	actor->setGameScene(this);
	//	actor->loadWithInfo(m_curLevelData->npcActorsInfo[i]);

	//	actor->setPosition(convertToCocos2DSpace(actor->getWorldPosition()));
	//	getMovieActorLayer()->addChild(actor);
	//	actor->release();
	//}

	/**
	   * add the scene made by cocostudio ( *.json )
	   * make the scene json file name ( xsc.map -> xsc.json )
	   **/
	//std::string sceneJsonFileName = m_curLevelData->tiledmapName;
	//int mapSuffixIndex = sceneJsonFileName.find(".map");
	//sceneJsonFileName = m_curLevelData->tiledmapName.substr(0, mapSuffixIndex);
	//sceneJsonFileName.append(".json");
	//// load json, for example, "scenetest1/TestScene.json"
	//if(CCFileUtils::sharedFileUtils()->isFileExist(sceneJsonFileName))
	//{
	//	CCNode *pJsonScene = SceneReader::sharedSceneReader()->createNodeWithSceneFile(sceneJsonFileName.c_str());
	//	pJsonScene->setPosition(ccp(0,0));
	//	getMovieActorLayer()->addChild(pJsonScene, SCENE_ROLE_LAYER_BASE_ZORDER + 3000);
	//}
}

void GameSceneLayer::removeMovieActors()
{
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene != NULL)
	{
		std::vector<long long> needRemoveActors;
		CCArray* actorArray = scene->getMovieActorLayer()->getChildren();
		CCObject* pActor = NULL;
		CCARRAY_FOREACH(actorArray, pActor)
		{
			SimpleFighter* actor = dynamic_cast<SimpleFighter*>(pActor);
			if(actor != NULL)
			{
				needRemoveActors.push_back(actor->getRoleId());
			}
		}
		for(unsigned int i = 0; i < needRemoveActors.size(); i++)
		{
			scene->removeActor(needRemoveActors.at(i));
		}
	}
}

MainScene* GameSceneLayer::getMainUIScene()
{
	return GameView::getInstance()->getMainUIScene();
}

CCLayer* GameSceneLayer::getActorLayer()
{
	CCNode* sceneLayer = this->getChildByTag(GameSceneLayer::kTagSceneLayer);
	CCLayer* actorLayer = (CCLayer*)sceneLayer->getChildByTag(GameSceneLayer::kTagActorLayer);
	return actorLayer;
}
CCLayer* GameSceneLayer::getMovieActorLayer()
{
	CCNode* sceneLayer = this->getChildByTag(GameSceneLayer::kTagSceneLayer);
	CCLayer* actorLayer = (CCLayer*)sceneLayer->getChildByTag(GameSceneLayer::kTagMovieActorLayer);
	return actorLayer;
}

MainAnimationScene * GameSceneLayer::getMainAnimationScene()
{
	return GameView::getInstance()->getMainAnimationScene();
}

std::vector<std::string>* GameSceneLayer::generateResourceList(const char* levelName)
{
	std::vector<std::string>* list = new std::vector<std::string>();

	// load level data
	LegendLevel* levelData = new LegendLevel();
	std::string levelFile = "level/";
	levelFile.append(levelName);
	levelData->load(levelFile.c_str());

	TiledMap* tiledMapData = new TiledMap();
	tiledMapData->loadTiledMapData(levelData->tiledmapName.c_str());
	std::vector<std::string>* tiledMapResList = tiledMapData->generateResourceList();
	vector<std::string>::iterator iter;
	for (iter = tiledMapResList->begin(); iter != tiledMapResList->end(); ++iter)
	{
		list->push_back((*iter));
	}
	delete tiledMapResList;

	delete tiledMapData;
	delete levelData;

	return list;
}

void GameSceneLayer::generateSearchMap() {
	TiledMapLayer* layer = m_tiledmap->getTiledMapData()->getLayers()[TILEDMAP_COLLISIONLAYER_ID];
	int mapCol = layer->getTileCol();
	int mapRow = layer->getTileRow();

	this->m_searchMap = new AStarTiledMap(mapCol, mapRow);
	char** mapData = m_searchMap->getData();

	short i = 0;
	for (; i < mapRow; i++) {// y
		for (short j = 0; j < mapCol; j++) {// x
			mapData[i][j] = ASTARTILEDMAP_PATH;

			//if (isCollide(j, i)) {
			if(isLimitOnGround(j, i)) {
				mapData[i][j] = ASTARTILEDMAP_BARRIER;
			}
		}
	}
}

void GameSceneLayer::registerWithTouchDispatcher()
{
    // higher priority than dragging
    CCDirector* pDirector = CCDirector::sharedDirector();
    pDirector->getTouchDispatcher()->addTargetedDelegate(this, 20, true);
}

bool GameSceneLayer::ccTouchBegan(CCTouch* touch, CCEvent* event)
{
    return true;
}

void GameSceneLayer::ccTouchMoved(CCTouch* touch, CCEvent* event)
{
    // If it weren't for the TouchDispatcher, you would need to keep a reference
    // to the touch from touchBegan and check that the current touch is the same
    // as that one.
    // Actually, it would be even more complicated since in the Cocos dispatcher
    // you get CCSets instead of 1 UITouch, so you'd need to loop through the set
    // in each touchXXX method.

    //CCPoint location = touch->getLocation();

	//CCNode* sceneLayer = this->getChildByTag(kTagSceneLayer);
	//CCPoint targetPos = sceneLayer->convertTouchToNodeSpace(touch);
	//targetPos = convertToGameWorldSpace(targetPos);

	//m_pMyPlayer->searchPath(targetPos);

	// setup move command
	//MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
	//cmd->targetPosition = targetPos;
	//m_pMyPlayer->setNextCommand(cmd, true);
}

void GameSceneLayer::ccTouchEnded(CCTouch* touch, CCEvent* event)
{
	if (GameView::getInstance()->getMapInfo()->maptype() == coliseum)
	{
		if (!OffLineArenaState::getIsCanTouch())
			return;
	}

	GameSceneState* pSceneState = dynamic_cast<GameSceneState*>(this->getParent());
	if(pSceneState->isInLoadState())
		return;

	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	Script* sc = ScriptManager::getInstance()->getScriptById(mainScene->mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(mainScene->getChildByTag(kTagVirtualJoystick));

    CCPoint location = touch->getLocation();
	//CCLog("touch position: %f, %f",location.x, location.y);

	CCNode* sceneLayer = this->getChildByTag(kTagSceneLayer);
	CCPoint targetPos = sceneLayer->convertTouchToNodeSpace(touch);
	targetPos = convertToGameWorldSpace(targetPos);

	//long long roleId = pickActor(GameActor::type_monster_mask, targetPos, PICK_ACTOR_ALGORITHM_CROSS, false);
	long long roleId = pickActor(0xFFFFFFFF, targetPos, PICK_ACTOR_ALGORITHM_CROSS, false);
	GameActor* actor = this->getActor(roleId);
	// setup attack command
	if(actor != NULL)
	{
		if(actor->getType() == GameActor::type_npc)
		{
			if(this->m_pMyPlayer->getLockedActorId() == roleId)
			{
				if (GameView::getInstance()->missionManager->isOutOfMissionTalkArea(roleId))
				{}
				else
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1129, (void *)GameView::getInstance()->myplayer->getLockedActorId());
					GameUtils::playGameSound(NPC_CLICK, 2, false);
				}
			}
			else
			{
				if (GameView::getInstance()->missionManager->isOutOfMissionTalkArea(roleId))
				{}
				else
				{
					m_pMyPlayer->setLockedActorId(actor->getRoleId());
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1129, (void *)GameView::getInstance()->myplayer->getLockedActorId());
					GameUtils::playGameSound(NPC_CLICK, 2, false);
				}
				
			}
			return;
		}
		else if(actor->getType() == GameActor::type_monster
			|| actor->getType() == GameActor::type_player)
		{
			bool bMove = false;
			if(this->m_pMyPlayer->getLockedActorId() == roleId) {
				if(m_pMyPlayer->canAttackActor(roleId))
					m_pMyPlayer->useSkill(m_pMyPlayer->getDefaultSkill(), true);
			}
			else {
				bool bCanSelected = !((BaseFighter*)actor)->hasExStatus(ExStatusType::invisible);
				if(bCanSelected)
					m_pMyPlayer->setLockedActorId(actor->getRoleId());
				else
					bMove = true;
			}
			if(!bMove)
				return;
		}
		else if(actor->getType() == GameActor::type_picking)
		{
			bool bCanSelected = ((PickingActor*)actor)->getActivied();
			if (bCanSelected)
			{
				m_pMyPlayer->setLockedActorId(actor->getRoleId());

				MyPlayerCommandPick* cmd = new MyPlayerCommandPick();
				cmd->targetId = roleId;
				cmd->init(MyPlayerCommandPick::PICK_TYPE_GENERAL);
				m_pMyPlayer->setNextCommand(cmd, true);

				return;
			}
		}
	
		else if (actor->getType() == GameActor::type_presentbox)
		{
			bool bCanSelected = ((PresentBox*)actor)->getActivied();
			if (bCanSelected)
			{
				m_pMyPlayer->setLockedActorId(actor->getRoleId());

				MyPlayerCommandPick* cmd = new MyPlayerCommandPick();
				cmd->targetId = roleId;
				cmd->targetPos = actor->getWorldPosition();
				cmd->init(MyPlayerCommandPick::PICK_TYPE_PRESENTBOX);
				m_pMyPlayer->setNextCommand(cmd, true);

				return;
			}
		}
		else if(actor->getType() == GameActor::type_pet)
		{
			// “展示”武将roleId从负的500万开始
			if (roleId < -5000000)
			{
				return;
			}

			if(m_pMyPlayer->getLockedActorId() == roleId)
			{
				if(m_pMyPlayer->canAttackActor(roleId))
					m_pMyPlayer->useSkill(m_pMyPlayer->getDefaultSkill(), true);
			}
			else
			{
				m_pMyPlayer->setLockedActorId(actor->getRoleId());
			}
			return;
		}
	}

	//// select monster
	//CCObject* pObj = NULL;
	//Monster* monster = NULL;
 //   CCARRAY_FOREACH(m_actors, pObj)
 //   {
 //       GameActor* actor = (GameActor*)pObj;
	//	if(actor->getType() == GameActor::type_monster)
	//	{
	//		monster = (Monster*)actor;
	//	}
	//}

	//int width = 60;
	//int height = 100*2;
	//CCRect monsterRect = CCRectMake(monster->getWorldPosition().x - width/2, 
	//	monster->getWorldPosition().y - height, width, height);
	//if(monsterRect.containsPoint(targetPos))
	//{
	//	int actorId = 100;

	//	if(this->m_pMyPlayer->getLockedActorId() == actorId)
	//	{
	//		// setup attack command
	//
	//		MyPlayerCommandAttack* cmd = new MyPlayerCommandAttack();
	//		cmd->skillId = FightSkill_SKILL_ID_PUTONGGONGJI;
	//		cmd->target = monster;
	//		cmd->attackMaxCount = 3;

	//		m_pMyPlayer->setNextCommand(cmd, true);
	//	}
	//	else
	//	{
	//		monster->showCircle(true);
	//		CCLog("monster is selected");
	//		//if select then pop up targetInfo
	//		createTarget();
	//		this->m_pMyPlayer->setLockedActorId(actorId);
	//	}
	//}
	//return;
	{
		// 移动目标点动画
		SimpleActor* simpleActor = new SimpleActor();
		simpleActor->setGameScene(this);
		simpleActor->setWorldPosition(ccp(targetPos.x, targetPos.y));
		simpleActor->loadAnim("animation/texiao/changjingtexiao/SB/sb.anm");
		simpleActor->setScale(1.6f);
		this->getActorLayer()->addChild(simpleActor, SCENE_SKY_LAYER_BASE_ZORDER);
		simpleActor->release();

		// setup move command
		MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
		cmd->autoMove = false;

		// check if the target is barrier, if true, try to modify the target to reachable point
		CCPoint startPos = m_pMyPlayer->getWorldPosition();
		int tileX = this->positionToTileX(targetPos.x);
		int tileY = this->positionToTileY(targetPos.y);
		if(this->isLimitOnGround(tileX, tileY)) 
		{
			CCPoint* newTarget = this->findNearestPositionToTarget(startPos.x, startPos.y, targetPos.x, targetPos.y);
			if(newTarget != NULL)
			{
				targetPos.setPoint(newTarget->x, newTarget->y);
				delete newTarget;
			}

			// if the modified target is too close to the start, modify it again
			if(ccpDistance(targetPos, startPos) < 16)
			{
				newTarget = this->findSecondNearestPositionToTarget(startPos.x, startPos.y, targetPos.x, targetPos.y);
				targetPos.setPoint(newTarget->x, newTarget->y);
				delete newTarget;
			}
		}

		cmd->targetPosition = targetPos;
		cmd->playerOperationMethod = MyPlayerCommandMove::operation_touchscreen;
		//m_pMyPlayer->setNextCommand(cmd, true, m_pMyPlayer->isAttacking());
		m_pMyPlayer->setNextCommand(cmd, true);   // move immediately, do not wait attack action

		m_pMyPlayer->getSimpleAI()->stopAll();

		// 去掉 “自动战斗”的动画（如果有的话）
		CCNode* pNode = m_pMyPlayer->getChildByTag(PLAYER_AUTO_FIGHT_ANIMATION_TAG);
		if (NULL != pNode)
		{
			m_pMyPlayer->removeAutoCombatFlag();
		}
	}
}

//void GameSceneLayer::ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent)
//{
//	//this->getChildByTag(kLegendAnimationTag)->setPosition(ccp(100, 100));
//
//	CCSetIterator it = pTouches->begin();
//    CCTouch* touch = (CCTouch*)(*it);
//    
//    CCPoint location = touch->getLocation();
//	CCLog("touch position: %f, %f",location.x, location.y);
//
//	//this->getChildByTag(kLegendAnimationRunTag)->setPosition(CCPointMake(location.x, location.y));
//}

CCPoint GameSceneLayer::convertToCocos2DSpace(const CCPoint& gameWorldPoint)
{
	return ccp(gameWorldPoint.x, (m_mapSize.height - gameWorldPoint.y)/2);   // 3d 变换为 2.5d
}
CCPoint GameSceneLayer::convertToCocos2DWorldSpace(const CCPoint& gameWorldPoint)
{
	return ccp(gameWorldPoint.x, m_mapSize.height - gameWorldPoint.y);
}
CCPoint GameSceneLayer::convertToGameWorldSpace(const CCPoint& cocosNodePoint)
{
	return ccp(cocosNodePoint.x, m_mapSize.height - cocosNodePoint.y * 2);
}

int GameSceneLayer::getActorZOrder(GameActor* actor)
{
	int zorder = 0;
	int layer = actor->getLayerId();
	if(layer == 0) {
		zorder = SCENE_GROUND_LAYER_BASE_ZORDER + actor->getSortPoint().y;
	} else if(layer == 1) {
		zorder = SCENE_ROLE_LAYER_BASE_ZORDER + actor->getSortPoint().y;
	} else {
		zorder = SCENE_SKY_LAYER_BASE_ZORDER + actor->getSortPoint().y;
	}
	return zorder;
}

void GameSceneLayer::setSynchronizeCocosPosition(bool bSynchronize)
{
	m_bSynchronizeCocosPosition = bSynchronize;
}
bool GameSceneLayer::isSynchronizeCocosPosition()
{
	return m_bSynchronizeCocosPosition;
}

void GameSceneLayer::update(float dt)
{
	Joystick* joystick = (Joystick*)this->getMainUIScene()->getChildByTag(kTagVirtualJoystick);

	// 根据摇杆所处的方位，决定角色移动的目的地
	if(joystick != NULL && joystick->isActive())
	{
		CCPoint joystickDir = joystick->getDirection();
		if(joystickDir.x != 0 || joystickDir.y != 0)
		{
			m_pMyPlayer->getSimpleAI()->stopAll();
			m_pMyPlayer->clearPath();
			//joystickDir.y *= 0.5f;   // 3d 变换为 2.5d
			m_pMyPlayer->runToDirection(joystickDir, dt);

			//add by yangjun 2014.9.12
			MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
			Script* sc = ScriptManager::getInstance()->getScriptById(mainScene->mTutorialScriptInstanceId);
			if(sc != NULL)
				sc->endCommand(mainScene->getChildByTag(kTagVirtualJoystick));

			//add by yangjun 2014.10.13
			if (mainScene)
			{
			 	if (mainScene->isHeadMenuOn)
			 	{
			 		mainScene->ButtonHeadEvent(NULL);
			 		return;
			 	}
			}
		}
	}

	GameSceneState* pSceneState = dynamic_cast<GameSceneState*>(this->getParent());
	if(pSceneState->isInGameState())
		this->m_pMyPlayer->update(dt);
	CCObject* pObj = NULL;
	CCARRAY_FOREACH(m_actors, pObj)
    {
        GameActor* actor = (GameActor*)pObj;
		actor->update(dt);
	}

	CCNode* sceneLayer = this->getChildByTag(kTagSceneLayer);
	CCNode* actorLayer = sceneLayer->getChildByTag(kTagActorLayer);
    for (std::map<long long,GameActor*>::iterator it = m_ActorsMap.begin(); it != m_ActorsMap.end();)//注意此处不能再写it++
	{
		GameActor* actor = it->second;
		if (actor != NULL)
		{
			if(actor->isDestroy())
			{
				actor->removeFromParent();

				m_ActorsMap.erase(it++);

				actor->onRemoveFromGameScene();
				actor->release();
				continue;
			}
			else
			{
				actor->update(dt);
				it++;
			}
		}
		else
		{
			it++;
		}
		
	}

	std::vector<GameActor*>::iterator iter;
	for (unsigned int i = 0; i < m_ClientActorsVector.size(); i++) {
		iter = m_ClientActorsVector.begin() + i;
		GameActor* pActor = *iter;
		if (pActor->isDestroy()) {
			m_ClientActorsVector.erase(iter);

			pActor->removeFromParent();
			pActor->onRemoveFromGameScene();
			pActor->release();

			i--;
			continue;
		}
		pActor->update(dt);
	}

	// skill-update maybe modify the actor's position, so this call should be earlier than "sort actors" below
	this->m_pSkillManager->update(dt);

	// sort actors
	setSynchronizeCocosPosition(true);   // IMPORTANT
	#define MAX_ACTOR_LAYER_NUM 2
	CCNode* allLayers[MAX_ACTOR_LAYER_NUM];
	allLayers[0] = actorLayer;
	allLayers[1] = this->getMovieActorLayer();
	for(int i = 0; i < MAX_ACTOR_LAYER_NUM; i++)
	{
		CCNode* pActorLayer = allLayers[i];
		if(!pActorLayer->isVisible())
			continue;

		CCArray* actorArray = pActorLayer->getChildren();
		CCObject* pActor = NULL;
		CCARRAY_FOREACH(actorArray, pActor)
		{
			GameActor* actor = dynamic_cast<GameActor*>(pActor);
			if(actor != NULL)
			{
				// at last, synchronize the position
				CCPoint cocosPosition = convertToCocos2DSpace(actor->getWorldPosition());
				actor->setPosition(cocosPosition);

				// then, sort
				pActorLayer->reorderChild(actor, getActorZOrder(actor));
			}
		}
	}
	setSynchronizeCocosPosition(false);   // restore, IMPORTANT

	// update the scene's camera
	//setView(sceneLayer, m_pMyPlayer->getPosition());
	int _behaviour = m_pCamera->getBehaviour();
	if(_behaviour == GameSceneCamera::camera_behaviour_follow_actor)
	{
		setView(sceneLayer, m_pCamera->getSceneCameraPosition(m_pMyPlayer));
	}
	else if(_behaviour == GameSceneCamera::camera_behaviour_move)
	{
		setView(sceneLayer, m_pCamera->getPosition());
	}
	else
	{
		// shake the screen
	}

	//m_pMyPlayer->GetFSM()->Update();

	//add by yangjun 2013.10.23
	GameView::getInstance()->missionManager->update();

	//add by liuliang 2014.4.4
	ActiveMissonManager::instance()->update();

	ScriptManager::getInstance()->update();

	DrugManager::getInstance()->update(dt);

	m_pMusicController->update(dt);

	FivePersonInstance::getInstance()->update();

	FamilyFightData::getInstance()->update();

	MapAction::getInstance()->update();

	if(pSceneState->isInGameState())
		NewCommerStoryManager::getInstance()->update();

	GeneralsStateManager::getInstance()->update(dt);
}

void GameSceneLayer::setView(CCNode* sceneLayer, CCPoint viewPos)
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	float scenePlayerX = -(viewPos.x - s.width/2);
	float scenePlayerY = -(viewPos.y - s.height/2);
	//sceneLayer->setPosition(-(viewPos.x - s.width/2), -(viewPos.y - s.height/2));

	// 视口超出地图的处理
	if(scenePlayerX > 0)
		scenePlayerX = 0;
	if(scenePlayerX + m_mapSize.width < s.width)
		scenePlayerX = -(m_mapSize.width - s.width);
	if(scenePlayerY > 0)
		scenePlayerY = 0;
	if(scenePlayerY + m_mapSize.height/2 < s.height)
		scenePlayerY = -(m_mapSize.height/2 - s.height);

	// 地图比视口还小
	// todo
	sceneLayer->setPosition(scenePlayerX, scenePlayerY);
}

bool GameSceneLayer::isOutOfView(GameActor* actor)
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	CCRect viewRect = CCRectMake(m_pMyPlayer->getWorldPosition().x - s.width/2, 
		m_pMyPlayer->getWorldPosition().y - s.height,
		s.width, s.height*2);

	if(viewRect.containsPoint(actor->getWorldPosition()))
		return false;

	return true;
}

short GameSceneLayer::positionToTileX(int x) {
	//return (short) (x / TILE_WIDTH);
	return (short) (x >> TILE_SIZE_SHIFT);
}

short GameSceneLayer::positionToTileY(int y) {
	//return (short) (y / TILE_HEIGHT);
	//return (short) (y / (TILE_HEIGHT<<1));   // 格子尺寸转换到游戏世界坐标系的尺寸
	return (short) (y >> TILE_SIZE_SHIFT);
}

int GameSceneLayer::tileToPositionX(short x) {
	//return x * TILE_WIDTH + TILE_WIDTH_HALF;
	return (x << TILE_SIZE_SHIFT) + TILE_SIZE_HALF;
}

int GameSceneLayer::tileToPositionY(short y) {
	//return y * (TILE_HEIGHT<<1) + (TILE_HEIGHT_HALF<<1);
	return (y << TILE_SIZE_SHIFT) + TILE_SIZE_HALF;
}

bool GameSceneLayer::checkInOtherMap(short tileX, short tileY, TiledMapLayer* collisionLayer) {

	int MAX_COL = collisionLayer->getTileCol();
	int MAX_ROW = collisionLayer->getTileRow();

	return tileX >= 0 && tileX < MAX_COL && tileY >=0 && tileY < MAX_ROW;
}

bool GameSceneLayer::isLimitOnOtherMapGround(short tileX, short tileY, LegendLevel* pLevelData) {

	TiledMap pTiledMap;
	pTiledMap.loadTiledMapData(pLevelData->tiledmapName.c_str());
	TiledMapLayer* collisionLayer = pTiledMap.getLayers()[TILEDMAP_COLLISIONLAYER_ID];

	if(!checkInOtherMap(tileX, tileY, collisionLayer))
	{
		return true;
	}

	short t = collisionLayer->getTileData()[tileY][tileX];   // note: access sequence
	if (t == -1 || t == 1 || t == 2 || t == 3) {   // can pass
		return false;
	} else {
		return true;   // can not pass
	}
}

bool GameSceneLayer::checkInMap(short tileX, short tileY) {

	TiledMapLayer* collisionLayer = m_tiledmap->getTiledMapData()->getLayers()[TILEDMAP_COLLISIONLAYER_ID];

	int MAX_COL = collisionLayer->getTileCol();
	int MAX_ROW = collisionLayer->getTileRow();

	return tileX >= 0 && tileX < MAX_COL && tileY >=0 && tileY < MAX_ROW;
}

bool GameSceneLayer::isLimitOnGround(short tileX, short tileY) {

	TiledMapLayer* collisionLayer = m_tiledmap->getTiledMapData()->getLayers()[TILEDMAP_COLLISIONLAYER_ID];

	if(!checkInMap(tileX, tileY))
		return true;

	short t = collisionLayer->getTileData()[tileY][tileX];   // note: access sequence
	if (t == -1 || t == 1 || t == 2 || t == 3) {   // can pass
		return false;
	} else {
		return true;   // can not pass
	}
}

void GameSceneLayer::setTiledMapLimit(short tileX, short tileY,int tiledData)
{
	TiledMapLayer* collisionLayer = m_tiledmap->getTiledMapData()->getLayers()[TILEDMAP_COLLISIONLAYER_ID];

	if(!checkInMap(tileX, tileY))
		return ;

	collisionLayer->getTileData()[tileY][tileX] = tiledData;
}

bool GameSceneLayer::isWater(short tileX, short tileY)
{
	CCAssert(checkInMap(tileX, tileY), "out of range");

	TiledMapLayer* collisionLayer = m_tiledmap->getTiledMapData()->getLayers()[TILEDMAP_COLLISIONLAYER_ID];
	short t = collisionLayer->getTileData()[tileY][tileX];   // note: access sequence
	if (t == 2) {   // 2, is water
		return true;
	}
	return false;
}

bool GameSceneLayer::isCover(short tileX, short tileY) {
	CCAssert(checkInMap(tileX, tileY), "out of range");

	TiledMapLayer* collisionLayer = m_tiledmap->getTiledMapData()->getLayers()[TILEDMAP_COVERLAYER_ID];
	short t = collisionLayer->getTileData()[tileY][tileX];   // note: access sequence
	if (t == 1) {   // 1, is cover
		return true;
	}
	return false;
}

CCSize GameSceneLayer::getMapSize()
{ 
	return m_mapSize; 
}

bool GameSceneLayer::isLineIntersectBarrier(float startX, float startY, float endX, float endY)
{
	CCPoint start = ccp(startX, startY);
	CCPoint end = ccp(endX, endY);
	CCPoint offset = GameUtils::getDirection(start, end);

	float maxLen = start.getDistance(end);   // 线段总长度

	float currentLen = 0;   // 当前长度
	CCPoint curPoint = start;
	short tileX, tileY;
	while(currentLen < maxLen)
	{
		curPoint = ccpAdd(curPoint, offset);
		tileX = this->positionToTileX(curPoint.x);
		tileY = this->positionToTileY(curPoint.y);
		if(this->isLimitOnGround(tileX, tileY))
		{
			return true;
		}
		currentLen += 1;    // step, small enough
	}
	
	return false;
}

CCPoint* GameSceneLayer::findNearestPositionToTarget(float startX, float startY, float endX, float endY)
{
	// 反过来寻找，从 end 到 start
	CCPoint start = ccp(startX, startY);
	CCPoint end = ccp(endX, endY);
	CCPoint offset = GameUtils::getDirection(end, start);
	int step = 8;
	offset = ccpMult(offset, step);

	float maxLen = start.getDistance(end);   // 线段总长度

	float currentLen = 0;   // 当前长度
	CCPoint curPoint = end;
	short tileX, tileY;
	while(currentLen < maxLen)
	{
		curPoint = ccpAdd(curPoint, offset);
		tileX = this->positionToTileX(curPoint.x);
		tileY = this->positionToTileY(curPoint.y);
		// found!
		if(!this->isLimitOnGround(tileX, tileY))
		{
			return new CCPoint(curPoint.x, curPoint.y);
		}
		currentLen += step;    // step
	}
	
	return NULL;
}

CCPoint* GameSceneLayer::findSecondNearestPositionToTarget(float startX, float startY, float endX, float endY)
{
	std::vector<CCPoint> allReachableAroundPoints;

	TiledMapLayer* collisionLayer = m_tiledmap->getTiledMapData()->getLayers()[TILEDMAP_COLLISIONLAYER_ID];
	int MAX_COL = collisionLayer->getTileCol();
	int MAX_ROW = collisionLayer->getTileRow();

	int startTileX = positionToTileX(startX);
	int startTileY = positionToTileY(startY);

	int currentTileX, currentTileY;
	// get all points around the start tile
	for(int i = -1; i <= 1; i++)
	{
		for(int j = -1; j <= 1; j++)
		{
			// ignore self
			if(i == 0 && j == 0)
				continue;

			currentTileX = startTileX + i;
			currentTileY = startTileY + j;

			// out of range
			if(currentTileX < 0 || currentTileX >= MAX_COL
				|| currentTileY < 0 || currentTileY >= MAX_ROW)
				continue;

			// barrier
			if(isLimitOnGround(currentTileX, currentTileY))
				continue;

			CCPoint candidatePoint = ccp(tileToPositionX(currentTileX), tileToPositionY(currentTileY));
			allReachableAroundPoints.push_back(candidatePoint);
		}
	}
	CCAssert(allReachableAroundPoints.size() > 0, "should not be less than 0");

	// get nearest one
	CCPoint target = ccp(endX, endY);
	int selectedAroundTileIndex = 0;
	float minDistanceToTarget = ccpDistance(target, allReachableAroundPoints.at(selectedAroundTileIndex));
	for(unsigned int i = 1; i < allReachableAroundPoints.size(); i++)
	{
		float distance = ccpDistance(target, allReachableAroundPoints.at(i));
		if(distance < minDistanceToTarget)
			selectedAroundTileIndex = i;
	}

	return new CCPoint(allReachableAroundPoints.at(selectedAroundTileIndex));
}

CCPoint GameSceneLayer::getNearReachablePoint(CCPoint& targetPoint)
{
	short tileX = GameSceneLayer::positionToTileX(targetPoint.x);
	short tileY = GameSceneLayer::positionToTileY(targetPoint.y);
	const int range_offset = 5;   // max range
	const int init_offset = 2;
	short initTileX = tileX - init_offset;
	short initTileY = tileY - init_offset;
	for(short x = initTileX; x < (initTileX+range_offset); x++)
	{
		for(short y = initTileY; y < (initTileY+range_offset); y++)
		{
			// out of map, ignore
			if(!this->checkInMap(x, y))
				continue;

			if(this->isLimitOnGround(x, y))
				continue;

			float posX = GameSceneLayer::tileToPositionX(x);
			float posY = GameSceneLayer::tileToPositionY(y);
			return ccp(posX, posY);
		}
	}

	return ccp(-1,-1);
}

void GameSceneLayer::putActor(long long roleId, GameActor* actor)
{
	actor->retain();   // keep this instance
	m_ActorsMap[roleId] = actor;

	actor->setGameScene(this);
}

GameActor* GameSceneLayer::getActor(long long roleId)
{
	if(roleId == NULL_ROLE_ID)
		return NULL;

	if(GameView::getInstance()->isOwn(roleId))
		return GameView::getInstance()->myplayer;

	if(m_ActorsMap.size() <= 0)
		return NULL;

	return m_ActorsMap[roleId];
}

void GameSceneLayer::getTypeActor(vector<long long>& typeActor, int actorType)
{
	for (std::map<long long,GameActor*>::iterator it = m_ActorsMap.begin(); it != m_ActorsMap.end(); ++it) 
	{
		GameActor* actor = it->second;
		if(actor != NULL)
		{
			if(actor->getType() == actorType)
			{
				typeActor.push_back(it->first);
			}
		}
		//CC_SAFE_RELEASE(actor);
	}
}

void GameSceneLayer::removeActor(long long roleId)
{
	if(GameView::getInstance()->isOwn(roleId))
		CCAssert(false, "maybe do not remove MyPlayer");

	GameActor* actor = m_ActorsMap[roleId];
	if(actor != NULL)
	{
		actor->removeFromParentAndCleanup(true);
		actor->release();
	}

	m_ActorsMap.erase(roleId);
}

void GameSceneLayer::putActorToClientVector(GameActor* actor)
{
	m_ClientActorsVector.push_back(actor);
}

void GameSceneLayer::selectActor(GameActor* actor)
{
	// set new locked flag
	if(actor != NULL)
	{
		if(this->m_pMyPlayer->canAttackActor(actor->getRoleId()))
		{
			actor->showLockedCircle(true, "animation/texiao/changjingtexiao/GSGB_a_b/gsgb_a.anm");   // red circle
		}
		else
		{
			if(actor->getType() == GameActor::type_npc
				|| actor->getType() == GameActor::type_picking)
			{
				actor->showLockedCircle(true, "animation/texiao/changjingtexiao/GSGB_a_b/gsgb_blue.anm");   // blue circle
			}
			else
			{
				actor->showLockedCircle(true, "animation/texiao/changjingtexiao/GSGB_a_b/gsgb_b.anm");   // green circle
			}
		}
		
		// Patch: 采集物的选中圈的尺寸要小一些
		if(actor->getType() == GameActor::type_picking)
		{
			actor->getChildByTag(GameActor::kTagLockedCircle)->setScale(0.5f);
		}

		actor->showActorName(true);
	}

	BaseFighter* bf = dynamic_cast<BaseFighter*>(actor);
	if(bf != NULL)
	{
		if (bf->getType() == GameActor::type_npc || bf->getType() == GameActor::type_monster)
			return;

		getMainUIScene()->createTargetInfo(bf);
	}
		
}
void GameSceneLayer::unselectActor(GameActor* actor)
{
	if(actor != NULL)
	{
		actor->showLockedCircle(false);

		if(actor->getType() == GameActor::type_monster
			|| actor->getType() == GameActor::type_picking)
		actor->showActorName(false);
	}

	if(getMainUIScene()->targetInfoLayer != NULL)
	{
		CCNode* targetInfo = getMainUIScene()->targetInfoLayer->getChildByTag(kTagTatgetInfo);
		if (targetInfo != NULL)
			targetInfo->setVisible(false);
			//targetInfo->removeFromParent();
	}
}

static int sComboNumber = 0;
#define SCENE_COMBOEFFECT_TAG 100021

void GameSceneLayer::addComboEffect()
{
	// remove old combo
	CCNode* oldLabel = getChildByTag(SCENE_COMBOEFFECT_TAG);
	if(oldLabel == NULL)
	{
		sComboNumber = 0;
	}
	else
	{
		oldLabel->removeFromParent();
	}

	// add the combo
	sComboNumber++;
	char comboNumber[5];
	sprintf(comboNumber, "%d", sComboNumber);
	std::string comboText = "COMBO";
	comboText.append(comboNumber);
	int fontSize = 20;
	ccColor3B color = ccc3(255, 255, 255);
	if(sComboNumber < 10)
	{
		fontSize = 20;
		color = ccc3(255, 255, 255);
	}
	else
	{
		fontSize = 30;
		color = ccc3(255, 0, 0);
	}
	CCLabelTTF* comboLabel = CCLabelTTF::create(comboText.c_str(), APP_FONT_NAME, fontSize);
	comboLabel->setColor(color);
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	comboLabel->setPosition(ccp(s.width - 130, s.height - 180));

	CCFiniteTimeAction*  action = CCSequence::create(
		CCShow::create(),
		CCScaleTo::create(0.25f*1/2,1.5f),
		CCScaleTo::create(0.25f*1/3,1.0f),
		CCDelayTime::create(0.75f),
		CCFadeOut::create(0.75f),
		CCCallFuncO::create(this,callfuncO_selector(GameSceneLayer::removeComboEffect),NULL),
		CCRemoveSelf::create(),
		NULL);
	comboLabel->runAction(action);

	comboLabel->setTag(SCENE_COMBOEFFECT_TAG);

	addChild(comboLabel);
}

// the combo will disappear after a short time
void GameSceneLayer::removeComboEffect(CCObject * obj)
{
	sComboNumber = 0;
}

//void GameSceneLayer::initDoors(const char* name){
//    MapInfo* mapInfo = GameWorld::MapInfos[name];
//    if(mapInfo == NULL){
//    	return;
//    }
//    std::vector<DoorInfo*> doors = mapInfo->doors;
//	m_Doors.clear();
//    for (int i = 0; i < (int)doors.size(); i++) {
//    	DoorInfo* doorInfo = doors[i];
//    	for (int k = 0; k < (int)doorInfo->doorXY.size(); k++) {
//	    	MapInfo* mi = GameWorld::MapInfos[doorInfo->mapId];
//	    	if (mi == NULL) {
//	    		//Debug.LogI("initDoors", "doorInfo.mapId===="+doorInfo.mapId+" is nulllllllll");
//	    	} else {
//				short* xy = doorInfo->doorXY.at(k);
//	    		MapDoor* mapDoor = new MapDoor();
//	    		mapDoor->x = xy[0];
//	    		mapDoor->y = xy[1];
//	    		mapDoor->targetMapId = doorInfo->mapId;
//
//		    	mapDoor->targetMapName = mi->name;
//				m_Doors.push_back(mapDoor);
//	    	}
//    	}
//    }	
//}

//CCPoint GameSceneLayer::getDoorPosition(std::string mapId){
//
//    for (std::vector<MapDoor*>::iterator it = m_Doors.begin(); it != m_Doors.end(); ++it) 
//	{
//		MapDoor* md = *it;
//		if(md->targetMapId == mapId)
//		{
//			// 找到传送门位置
//			if(!isLimitOnGround(md->x, md->y)) {
//				return ccp(tileToPositionX(md->x), tileToPositionY(md->y));
//			}
//		}
//	}
//	CCAssert(false, "data error, please check map in editor");
//	return ccp(-1, -1);
//}

CCPoint GameSceneLayer::getDoorPosition(std::string sourceMap, std::string targetMapId, LegendLevel* pLevelData)
{
	MapInfo* pMapInfo = GameWorld::MapInfos[sourceMap];
	CCAssert(pMapInfo != NULL, "should not be nil");
	for(unsigned int i = 0; i < pMapInfo->doors.size(); i++)
	{
		std::string& mapId = pMapInfo->doors[i]->mapId;
		if(mapId != targetMapId)
			continue;

		// traverse all points, find the center point for the teleport
		std::vector<short*>& allPoints = pMapInfo->doors[i]->doorXY;
		int size = allPoints.size();
		CCAssert(size > 0, "should be greater than zero");

		int minRow = allPoints.at(0)[1];
		int minCol = allPoints.at(0)[0];
		int maxRow = 0, maxCol = 0;
		for(int j = 0; j < size; j++)
		{
			minRow = MIN(minRow, allPoints.at(j)[1]);
			maxRow = MAX(maxRow, allPoints.at(j)[1]);
			minCol = MIN(minCol, allPoints.at(j)[0]);
			maxCol = MAX(maxCol, allPoints.at(j)[0]);
		}
		int resultRow = (minRow + maxRow)/2;
		int resultCol = (minCol + maxCol)/2;

		CCPoint p1 = ccp(GameSceneLayer::tileToPositionX(minCol), GameSceneLayer::tileToPositionY(minRow));
		CCPoint p2 = ccp(GameSceneLayer::tileToPositionX(maxCol), GameSceneLayer::tileToPositionY(maxRow));
		CCPoint centerPoint = (p1 + p2)/2;

		int tileX = this->positionToTileX(centerPoint.x);
		int tileY = this->positionToTileY(centerPoint.y);

		if(pLevelData == NULL)
		{
			if(!this->isLimitOnGround(tileX, tileY)) 
				//if(!isLimitOnGround(resultCol, resultRow))
			{
				//CCPoint centerPoint = ccp(GameSceneLayer::tileToPositionX(resultCol), GameSceneLayer::tileToPositionY(resultRow));
				return centerPoint;
			}
		}
		else
		{
			if(!this->isLimitOnOtherMapGround(tileX, tileY, pLevelData)) 
			{
				return centerPoint;
			}
		}
	}

	CCAssert(false, "data error, please check map in editor");
	return ccp(-1, -1);
}

// 仅用于pickActor函数的计算
const static int FINGER_WIDTH = 96;   // 可以考虑Tile的大小，1 tile = 64*32
const static int FINGER_HEIGHT = 96;
const static int FINGER_AREA = FINGER_WIDTH*FINGER_HEIGHT;   // 手指按下去的面积
const static int ACTOR_FINGET_AREA_RATIO = 10; // 角色矩形和手指矩形之间的比例，用于选择算法的决策
const static int FINGER_RATIO_AREA = FINGER_AREA/ACTOR_FINGET_AREA_RATIO;

long long GameSceneLayer::pickActor(int actorTypes, CCPoint targetPos, int algorithmType, bool canSelectSelf) {
	// 参数传来的选择点，在游戏世界坐标系下的位置
	float fingerX = targetPos.x;
	float fingerY = targetPos.y;
	CCRect fingerRect = CCRectMake(fingerX-FINGER_WIDTH/2, fingerY-FINGER_HEIGHT/2, 
			FINGER_WIDTH, FINGER_HEIGHT);

	std::map<long long, float> intersectedActors;

	// 算法 step 1: 判断手指的矩形，和actor的可视矩形是否相交
	GameActor* a = NULL;
	for (std::map<long long,GameActor*>::iterator it = m_ActorsMap.begin(); it != m_ActorsMap.end(); ++it) 
	{
		a = (GameActor*)it->second;
		if (a != NULL) {
			// 不存在于参数指定的actorTypes搜索条件
			int thisActorTypeMask = 1 << a->getType();
			char thisActorMaskValue = (char)((actorTypes & thisActorTypeMask) >> a->getType());
			if(thisActorMaskValue == 0)
				continue;
				
			// 根据参数，决定是否选择玩家自身，这里包含了玩家自己的武将
			if(!canSelectSelf) {
				General* general = dynamic_cast<General*>(a);
				if(a->getRoleId() == this->m_pMyPlayer->getRoleId()
					|| (general != NULL && general->isMyPlayerGroup()) )
					continue;
			}

			// the actor is dead, ignore it
			BaseFighter* bf = dynamic_cast<BaseFighter*>(a);
			if(bf != NULL && bf->isAction(ACT_DIE))
				continue;
				
			// actors's visible rect, for example, (width = 64, height = 64)
			CCRect rect = a->getVisibleRect();
			int width = rect.size.width;
			int height = rect.size.height*2;   // *2, perspective transform
			CCRect actorVisibleRect = CCRectMake(a->getWorldPosition().x - width/2, 
				a->getWorldPosition().y - height, width, height);

			bool bSelected = false;
			if(algorithmType == PICK_ACTOR_ALGORITHM_CROSS) {   // 矩形相交算法
				if(actorVisibleRect.intersectsRect(fingerRect))
				{
					float x = MAX(actorVisibleRect.origin.x, fingerRect.origin.x);
					float y = MAX(actorVisibleRect.origin.y, fingerRect.origin.y);
					float xx = MIN(actorVisibleRect.origin.x+actorVisibleRect.size.width, fingerRect.origin.x+fingerRect.size.width);
					float yy = MIN(actorVisibleRect.origin.y+actorVisibleRect.size.height, fingerRect.origin.y+fingerRect.size.height);

					CCRect intersection = CCRectMake(x, y, xx-x, yy-y);
					// 若actor矩形面积小于手指面积，则相交面积考虑要大于actor矩形的一定比例的面积
					// 若actor矩形面积大于手指面积，则相交面积考虑要大于手指的一定比例面积
					int intersectedArea = intersection.size.width * intersection.size.height;
					if(intersectedArea >= (actorVisibleRect.size.width*actorVisibleRect.size.height/ACTOR_FINGET_AREA_RATIO)
							|| intersectedArea > FINGER_RATIO_AREA) {
						bSelected = true;
					}
				}
			} else {   // 包含算法
				if(actorVisibleRect.containsPoint(targetPos)) {
					bSelected = true;
				}
			}
			if(bSelected) {
				// 计算两个中心点之间的距离
				float distance = ccpDistance(targetPos, ccp(actorVisibleRect.getMidX(), actorVisibleRect.getMidY()));
				intersectedActors[a->getRoleId()] = distance;
			}
		}
	}
		
	// 算法 step 2: 考虑中心点之间的最短距离
	int intersectedNum = intersectedActors.size();
	if(intersectedNum > 1) {   // 多于1个
		// 根据手指中心和可能被选的actor矩形的中心的距离来判定
		// 距离最短的就是希望被选中的actor
		std::map<long long, float>::iterator iter = intersectedActors.begin();
		long long minDistanceRole = iter->first;   // get the first element
		for (std::map<long long, float>::iterator it = intersectedActors.begin(); it != intersectedActors.end(); ++it) {
			// 发现了更小距离的role
			if(it->second < intersectedActors[minDistanceRole]) {
				minDistanceRole = it->first;
			}
		}
		return minDistanceRole;
	}
	else if(intersectedNum == 1) {
		// get the first element
		std::map<long long, float>::iterator iter = intersectedActors.begin();
		return iter->first;   // role id
	} else if(intersectedNum == 0) {
		return NULL_ROLE_ID;
	}
		
	return NULL_ROLE_ID;
}
