﻿#include "SimpleEffectManager.h"

CCNode* SimpleEffectManager::generateTextEffect(int effectType, const char* text, const ccColor3B& color3) {
    CCLabelTTF* label = CCLabelTTF::create(text, "Marker Felt", 50);
	label->setColor(color3);   // ccc3(255, 0, 0)

	label->runAction(getEffectAction(effectType));

	return label;
}

void SimpleEffectManager::applyEffectAction(CCNode* effectNode, int effectType)
{
	effectNode->runAction(getEffectAction(effectType));
}

// 通过这2个控制参数，来模拟数字在角色头顶，在倒锥形的范围来显示
static int s_effectPuTongLayout_target_horizon = 0;   // 控制水平方向出现的位置
static int s_effectPuTongLayout_target_vertical = 0;   // 控制垂直方向出现的位置

CCAction* SimpleEffectManager::getEffectAction(int effectType)
{
    switch (effectType)
    {
    case SimpleEffectManager::EFFECT_BAOJI:
		{
			CCFiniteTimeAction*  action = CCSequence::create(
				CCShow::create(),
				CCScaleTo::create(0.25f*1/2,2.0f),
				CCScaleTo::create(0.25f*1/3,1.0f),
				CCDelayTime::create(0.9f),
				CCFadeOut::create(0.5f),
				CCRemoveSelf::create(),
				NULL);

			return action;
		}
		break;

	case SimpleEffectManager::EFFECT_PUTONG:
		{
			int x = 0;
			if(s_effectPuTongLayout_target_horizon == 0) {
				//x = -15;
				s_effectPuTongLayout_target_horizon = 1;
			}
			else {
				//x = 15;
				s_effectPuTongLayout_target_horizon = 0;
			}

			int y = 0;
			float duration = 0.3f;
			if(s_effectPuTongLayout_target_vertical == 0) {
				y = 0;
				duration = 0.2f;

				if(s_effectPuTongLayout_target_horizon == 0) {
					x = -3;
				}
				else {
					x = 3;
				}

				s_effectPuTongLayout_target_vertical = 1;
			}
			else if(s_effectPuTongLayout_target_vertical == 1) {
				y = 30;
				duration = 0.3f;

				if(s_effectPuTongLayout_target_horizon == 0) {
					x = -8;
				}
				else {
					x = 8;
				}

				s_effectPuTongLayout_target_vertical = 2;
			}
			else {
				y = 60;
				duration = 0.4f;

				if(s_effectPuTongLayout_target_horizon == 0) {
					x = -18;
				}
				else {
					x = 18;
				}

				s_effectPuTongLayout_target_vertical = 0;
			}

			float totalDuration = 1.0f;
			CCActionInterval*  action_step1= CCSpawn::create(
				CCScaleTo::create(0.1f, 1.2f),
				CCMoveBy::create(duration, ccp(x, y)),
				NULL);
			CCActionInterval*  action_step2= CCSpawn::create(
				CCScaleTo::create(0.08f, 0.9f),
				CCMoveBy::create(totalDuration - duration, ccp(0,0)),
				NULL);
			CCAction*  action = CCSequence::create(
				//CCFadeOut::create(1),
				//CCMoveBy::create(duration, ccp(x, y)),
				action_step1,
				//CCMoveBy::create(totalDuration - duration, ccp(0,0)),
				action_step2,
				CCFadeOut::create(0.3f),
				CCRemoveSelf::create(),
				NULL);

			return action;
		}
		break;

	case SimpleEffectManager::EFFECT_FLYINGUP:
		{
			CCAction*  action = CCSequence::create(
				CCMoveBy::create(1.0f, ccp(0, 40)),
				CCFadeTo::create(0.5f, 192),
				CCRemoveSelf::create(),
				NULL);
			return action;
		}
		break;

	default:
		CCAssert(false, "this effect type has not been implemented.");
		break;
	}

	return NULL;
}

CCRepeatForever* SimpleEffectManager::RoundRectPathAction(float controlX, float controlY, float w)
{
    ccBezierConfig bezier1;
    bezier1.controlPoint_1 = CCPoint(-controlX, 0);
    bezier1.controlPoint_2 = CCPoint(-controlX, controlY);
    bezier1.endPosition = CCPoint(0, controlY);
    CCBezierBy* bezierBy1 = CCBezierBy::create(0.8f, bezier1);

    CCMoveBy* move1 = CCMoveBy::create(0.8f, CCPoint(w, 0));
    
    ccBezierConfig bezier2;
    bezier2.controlPoint_1 = CCPoint(controlX, 0);
    bezier2.controlPoint_2 = CCPoint(controlX, -controlY);
    bezier2.endPosition = CCPoint(0, -controlY);
    CCBezierBy* bezierBy2 = CCBezierBy::create(0.8f, bezier2);

    CCMoveBy* move2 = CCMoveBy::create(0.8f, CCPoint(-w, 0));

	CCRepeatForever* path = NULL;
	if(w > 0)
		path = CCRepeatForever::create(CCSequence::create(bezierBy1, move1, bezierBy2, move2, NULL));
	else
		path = CCRepeatForever::create(CCSequence::create(bezierBy1, bezierBy2, NULL));
    
    return path;
}
