#include "ExStatusFactory.h"
#include "cocos2d.h"
#include "ExStatusType.h"

#include "ExStatusInvisible.h"
#include "ExStatusImmobilize.h"
#include "ExStatusDeath.h"
#include "ExStatusMusou.h"

ExStatusFactory* ExStatusFactory::sInstance = NULL;

ExStatusFactory* ExStatusFactory::getInstance() {
	if(sInstance == NULL)
		sInstance = new ExStatusFactory();
	return sInstance;
}
	
ExStatus* ExStatusFactory::getExStatus(int exStatusType, long duration, long remaintime) {
	ExStatus* exStatus = NULL;
	switch(exStatusType) {
	case ExStatusType::invisible:
		exStatus = new ExStatusInvisible();
		break;
	case ExStatusType::immobilize:
		exStatus = new ExStatusImmobilize();
		break;
	case ExStatusType::dead:
		exStatus = new ExStatusDeath();
		break;
	case ExStatusType::musou:
		exStatus = new ExStatusMusou();
		break;
	default:
		//CCLOG("default exstatus: %d", exStatusType);
		exStatus = new ExStatus();
	}
	exStatus->set_type(exStatusType);

	//exStatus.addDuration(duration);
	//exStatus.addValue(value);
	//exStatus.addValueAddition(valueAddition);
	//exStatus.setStartTime(System.currentTimeMillis());
	//exStatus.setLevel(level);
	//exStatus.setMultiple(multiple);
	return exStatus;
}
