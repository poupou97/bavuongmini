#ifndef _GAMESCENESTATE_EXSTATUS_H_
#define _GAMESCENESTATE_EXSTATUS_H_

#include "../../messageclient/element/CExtStatusInfo.h"
#include "../role/BaseFighter.h"
#include "cocos2d.h"

/**
 * 战斗者异常状态基类
 * @author zhaogang
 */
class ExStatus : public ExtStatusInfo {
public:
	ExStatus();
	virtual ~ExStatus();
	
	/**
	 * 获得状态时回调处理
	 */
	virtual void onAdd(ExtStatusInfo* info);
	
	/**
	 * 取消状态时回调处理
	 */
	virtual void onCancel();

	inline BaseFighter* getFighter() { return mFighter; };
	void setFighter(BaseFighter* fighter) { mFighter = fighter; };

	std::string getName();
	void setName(std::string _name);

	std::string getDescription();
	void setDescription(std::string _des);

	std::string getIcon();
	void setIcon(std::string _icon);

	/** art special effect */
	inline std::string getEffect() { return effect; };
	inline void setEffect(std::string effect) { this->effect = effect; };

	inline bool isFootEffect() { return bIsFootEffect; };
	inline void setFootEffect(bool bFoot) { bIsFootEffect = bFoot; };

	inline void setEffectLoop(bool bValue) { m_bEffectLoop = bValue; };
	inline bool isEffectLoop() { return m_bEffectLoop; };

	int getFunctionType();
	void setFunctionType(int type);

protected:
	void addSpecialEffect();
	void removeSpecialEffect();

	void addExstatusName();

protected:
	BaseFighter* mFighter;   // the owner of this exstatus

private:
	std::string name;
	std::string description;
	std::string icon;
	std::string effect;
	bool bIsFootEffect;   // 特效是踩在脚下，还是在身上
	bool m_bEffectLoop;   // special effect is loop or not
	int function_type;
};

#endif