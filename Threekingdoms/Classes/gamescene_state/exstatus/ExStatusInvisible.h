#ifndef _GAMESCENESTATE_EXSTATUS_INVISIBLE_H_
#define _GAMESCENESTATE_EXSTATUS_INVISIBLE_H_

#include "ExStatus.h"

/**
 * 隐身状态
 * @author zhaogang
 */
class ExStatusInvisible : public ExStatus {
public:
	ExStatusInvisible();
	virtual ~ExStatusInvisible();
	
	virtual void onAdd(ExtStatusInfo* info);
	virtual void onCancel();
};

#endif