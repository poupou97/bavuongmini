#ifndef _GAMESCENESTATE_EXSTATUS_MUSOU_H_
#define _GAMESCENESTATE_EXSTATUS_MUSOU_H_

#include "ExStatus.h"

/**
 * 无双状态
 * @author zhaogang
 */
class ExStatusMusou : public ExStatus {
public:
	ExStatusMusou();
	virtual ~ExStatusMusou();
	
	virtual void onAdd(ExtStatusInfo* info);
	virtual void onCancel();
};

#endif