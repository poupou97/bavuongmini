#include "MissionCell.h"
#include "../../messageclient/element/MissionInfo.h"
#include "GameView.h"
#include "../role/MyPlayerOwnedCommand.h"
#include "../role/MyPlayer.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../../utils/StrUtils.h"
#include "../../gamescene_state/SimpleEffectManager.h"

MissionCell::MissionCell()
: curMissionInfo(NULL)
{
}


MissionCell::~MissionCell()
{
	CC_SAFE_DELETE(curMissionInfo);
}

MissionCell* MissionCell::Create(MissionInfo * missionInfo)
{
	MissionCell * missionCell = new MissionCell();
	if (missionCell && missionCell->init(missionInfo))
	{
		missionCell->autorelease();
		return missionCell;
	}
	CC_SAFE_DELETE(missionCell);
	return NULL;
}

bool MissionCell::init(MissionInfo * missionInfo)
{
	if (CCTableViewCell::init())
	{
		curMissionInfo = new MissionInfo();
		curMissionInfo->CopyFrom(*missionInfo);

// 		CCScale9Sprite * cellFrame = CCScale9Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/zhezhao60.png"); 
// 		cellFrame->setPreferredSize(CCSizeMake(155,58));
// 		cellFrame->setAnchorPoint(ccp(0.0f, 1.0f));
// 		cellFrame->setPosition(ccp(0,60));
// 		this->addChild(cellFrame);
		//CCScale9Sprite * cellFrame = CCScale9Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/renwu_di2.png");
		//cellFrame->setPreferredSize(CCSizeMake(170,60));
		//cellFrame->setCapInsets(CCRectMake(12,12,1,1));
		CCScale9Sprite * cellFrame = CCScale9Sprite::create("res_ui/zhezhao70.png");
		cellFrame->setPreferredSize(CCSizeMake(170,60));
		cellFrame->setCapInsets(CCRectMake(7,7,1,1));
		cellFrame->setOpacity(160);
		cellFrame->setAnchorPoint(ccp(0,1.0f));
		cellFrame->setPosition(ccp(0,60));
		this->addChild(cellFrame);


		//mission type
		if (curMissionInfo->missionpackagetype() == 1)
		{
			const char *str_mission_zhu = StringDataManager::getString("mission_zhu");
			missionType = CCLabelTTF::create(str_mission_zhu,APP_FONT_NAME,18);
			ccColor3B shadowColor = ccc3(0,0,0);   // black
			missionType->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		}
		else if(curMissionInfo->missionpackagetype() == 2)
		{
			const char *str_mission_zhi = StringDataManager::getString("mission_zhi");
			missionType = CCLabelTTF::create(str_mission_zhi,APP_FONT_NAME,18);
			ccColor3B shadowColor = ccc3(0,0,0);   // black
			missionType->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		}
		else if(curMissionInfo->missionpackagetype() == 7)
		{
			const char *str_mission_jia = StringDataManager::getString("mission_jia");
			missionType = CCLabelTTF::create(str_mission_jia,APP_FONT_NAME,18);
			ccColor3B shadowColor = ccc3(0,0,0);   // black
			missionType->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		}
		else if(curMissionInfo->missionpackagetype() == 12)
		{
			const char *str_mission_xuan = StringDataManager::getString("mission_xuan");
			missionType = CCLabelTTF::create(str_mission_xuan,APP_FONT_NAME,18);
			ccColor3B shadowColor = ccc3(0,0,0);   // black
			missionType->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		}
		else
		{
			const char *str_mission_ri = StringDataManager::getString("mission_ri");
			missionType = CCLabelTTF::create(str_mission_ri,APP_FONT_NAME,18);
			ccColor3B shadowColor = ccc3(0,0,0);   // black
			missionType->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		}
		
		missionType->setAnchorPoint(ccp(0,1.0f));
		missionType->setPosition(ccp(4,52));
		ccColor3B _color = ccc3(240,0,0);
		missionType->setColor(_color);
		this->addChild(missionType);

		//mission name
		//CCLOG(missionInfo->missionname().c_str());
		missionName = CCLabelTTF::create(missionInfo->missionname().c_str(),APP_FONT_NAME,18);
		missionName->setAnchorPoint(ccp(0,1.0f));
		missionName->setPosition(ccp(missionType->getPositionX()+missionType->getContentSize().width,52));
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		missionName->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		this->addChild(missionName);

		//mission state
// 		missionSummary = CCLabelTTF::create(missionInfo->summary().c_str(),APP_FONT_NAME,14);
// 		missionSummary->setAnchorPoint(ccp(0,1.0f));
// 		missionSummary->setPosition(ccp(4,missionType->getPositionY()-missionType->getContentSize().height+1));
// 		missionSummary->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
// 		this->addChild(missionSummary);

		//CCLOG(missionInfo->summary().c_str());
		missionSummary = CCRichLabel::createWithString(missionInfo->summary().c_str(),CCSizeMake(300,20),this,NULL,0,14);
		missionSummary->setAnchorPoint(ccp(0,1.0f));
		missionSummary->setPosition(ccp(4,missionType->getPositionY()-missionType->getContentSize().height-4));
		this->addChild(missionSummary);

		switch(curMissionInfo->missionstate())
		{
		case dispatched : //已分配  
			missionSummary->setString(StrUtils::applyColor(StringDataManager::getString("mission_state_kejie"),ccc3(254,254,51)).c_str());
			missionType->setColor(ccc3(254,254,51));                       //淡黄色
			missionName->setColor(ccc3(254,254,51));                     //淡黄色
			//missionSummary->setColor(ccc3(254,254,51));              //淡黄色
			break;
		case accepted : //已接受
			missionType->setColor(ccc3(10,227,221));                     //蓝色
			missionName->setColor(ccc3(10,227,221));                   //蓝色
			//missionSummary->setColor(ccc3(255,255,255));          //白色
			break;
		case done ://已完成
			missionSummary->setString(StrUtils::applyColor(StringDataManager::getString("mission_state_wancheng"),ccc3(0,255,0)).c_str());
			missionType->setColor(ccc3(0,255,0));                     //绿色
			missionName->setColor(ccc3(0,255,0));                   //绿色
			//missionSummary->setColor(ccc3(0,255,0));            //绿色
			break;
		//case failed ://已失败
		//	break;
		case canceled ://已取消
			break;
		case submitted ://已提交
			break;
		}

		return true;
	}
	return false;
}

void MissionCell::MissionEvent(CCObject * pSender)
{
}

void MissionCell::addFinishAnm()
{
	std::string _path = "animation/texiao/particledesigner/renwudacheng.plist";
	CCParticleSystem* particleEffect = CCParticleSystemQuad::create(_path.c_str());
	particleEffect->setPositionType(kCCPositionTypeFree);
	particleEffect->setVisible(true);
	particleEffect->setStartSize(30.f);
	particleEffect->setPosition(ccp(85,20));

	CCRepeatForever* path2 = SimpleEffectManager::RoundRectPathAction(100, 55, 0 );
	particleEffect->runAction(path2);

	addChild(particleEffect);
}

void MissionCell::RefreshCell( MissionInfo * missionInfo )
{
	curMissionInfo->CopyFrom(*missionInfo);
	//mission type
	if (missionInfo->missionpackagetype() == 1)
	{
		const char *str_mission_zhu = StringDataManager::getString("mission_zhu");
		missionType->setString(str_mission_zhu);
	}
	else if(missionInfo->missionpackagetype() == 2)
	{
		const char *str_mission_zhi = StringDataManager::getString("mission_zhi");
		missionType->setString(str_mission_zhi);
	}
	else if(missionInfo->missionpackagetype() == 7)
	{
		const char *str_mission_jia = StringDataManager::getString("mission_jia");
		missionType->setString(str_mission_jia);
	}
	else if(missionInfo->missionpackagetype() == 12)
	{
		const char *str_mission_xuan = StringDataManager::getString("mission_xuan");
		missionType->setString(str_mission_xuan);
	}
	else
	{
		const char *str_mission_ri = StringDataManager::getString("mission_ri");
		missionType->setString(str_mission_ri);
	}
	missionType->setColor(ccc3(240,0,0));

	//mission name
	missionName->setString(missionInfo->missionname().c_str());

	missionSummary->setString(missionInfo->summary().c_str());

	switch(missionInfo->missionstate())
	{
	case dispatched : //已分配  
		missionSummary->setString(StrUtils::applyColor(StringDataManager::getString("mission_state_kejie"),ccc3(254,254,51)).c_str());
		missionType->setColor(ccc3(254,254,51));                       //淡黄色
		missionName->setColor(ccc3(254,254,51));                     //淡黄色
		break;
	case accepted : //已接受
		missionType->setColor(ccc3(10,227,221));                     //蓝色
		missionName->setColor(ccc3(10,227,221));                   //蓝色
		break;
	case done ://已完成
		missionSummary->setString(StrUtils::applyColor(StringDataManager::getString("mission_state_wancheng"),ccc3(0,255,0)).c_str());
		missionType->setColor(ccc3(0,255,0));                     //绿色
		missionName->setColor(ccc3(0,255,0));                   //绿色
		break;
		//case failed ://已失败
		//	break;
	case canceled ://已取消
		break;
	case submitted ://已提交
		break;
	}
}
