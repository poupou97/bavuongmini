

#ifndef _GAMESCENESTATE_PKMODE_H_
#define _GAMESCENESTATE_PKMODE_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class PKModeUI :public UIScene
{
public:
	PKModeUI();
	~PKModeUI();
	static PKModeUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
	
	void PeaceEvent(CCObject *pSender);
	void KillEvent(CCObject *pSender);
	void FamilyEvent(CCObject *pSender);
	void CountryEvent(CCObject *pSender);
};
#endif;

