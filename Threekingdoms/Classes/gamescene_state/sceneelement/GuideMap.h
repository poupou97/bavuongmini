
#ifndef _GAMESCENESTATE_GUIDEMAP_H_
#define _GAMESCENESTATE_GUIDEMAP_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class AreaMap;
class MainScene;
class CFunctionOpenLevel;
class CCTutorialParticle;
struct GMCoordinate
{
	float x;
	float y;
};

//当前处于何种界面下（主界面，副本，竞技场）
// enum SceneType
// {
// 	ST_MainScene = 0,
// 	ST_FiveInstance,
// 	ST_Arena,
// };

class GuideMap :public UIScene
{
public:
	GuideMap();
	~GuideMap();

	static GuideMap* create();
	bool init();
	virtual void update(float dt);

	void setSceneType(int mapType);

	//副本
	void InstanceEvent(CCObject *pSender);
	//召唤
	void CallFriendsEvent(CCObject *pSender);
	//退出副本
	void ExitEvent(CCObject *pSender);
	void sureToExit(CCObject *pSender);

	//exit singCopy
	void ExitSingCopy(CCObject * obj);
	void surToExitSingCopy(CCObject * obj);

	void HangUpEvent(CCObject *pSender);
	void GeneralModeEvent(CCObject *pSender);
	void ShopEvent(CCObject *pSender);
	void ActivityEvent(CCObject *pSender);
	void GiftEvent(CCObject *pSender);
	void ActiveDegreeEvent(CCObject *pSender);
	void EveryDayGiftEvent(CCObject *pSender);
	void NewServiceEvent(CCObject *pSender);
	void ArenaEvent(CCObject *pSender);
	//家族战战况
	void BattleSituationEvent(CCObject *pSender);
	//在线奖励
	void OnLineGiftEvent(CCObject *pSender);
	//get reward list
	void callBackGetReward(CCObject * pSender);
	//首充
	void FirstBuyVipEvent(CCObject *pSender);

	void GoToMapScene(CCObject * pSender);

	void addNewFunctionForArea1(CCNode * pNode,void * functionOpenLevel);
	void addNewFunctionForArea2(CCNode * pNode,void * functionOpenLevel);

	//退出竞技场
	void QuitArenaEvent(CCObject *pSender);
	void SureToQuitArena(CCObject *pSender);

	//退出家族战
	void QuitFamilyFightEvent(CCObject *pSender);
	void SureToQuitFamilyFight(CCObject *pSender);

	//武将模式设置默认
	void setDefaultGeneralMode(int idx);
	//武将模式切换
	void applyGeneralMode(int idx,int autoFight);
	int getGeneralMode();
	int getGeneralAutoFight();

	//remind
	UIButton * buttonRemind;
	UIButton * getBtnRemind();
	void showRemindContent(CCObject * obj);
	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	//添加到actionLayer上
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void addCCTutorialIndicatorSingCopy(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();	

	//教学
	int mTutorialScriptInstanceId;
	UIButton * btn_instance;
	UIButton *  btn_offLineArena;
	UIButton * btn_Reward;
	CCTutorialParticle * btn_RewardParticle;
	//robot
	bool isOpenSkillMonster;
	UIButton *btnRobotSkill;
	UIButton *btnRobotset;

	//获取当前显示的layer
	UILayer * getCurActiveLayer();
	//获取当前的Actionlayer
	UILayer * getActionLayer();
	//获取当前的挂机按钮（开始战斗/结束战斗）
	UIButton * getBtnHangUp();
	//设置所有挂机按钮的状态（处于一般情况，处于副本中，处于竞技场中）
	void setAllBtnHangUpTextures(const char * str);
	//获取当前的武将模式按钮（开启主动/开启协助）
	UIButton * getBtnGeneralMode();
	//设置所有武将模式按钮的状态（处于一般情况，处于副本中，处于竞技场中）
	void setAllBtnGeneralModeTextures(const char * str);
	//刷新地图名称
	void RefreshMapName();

	//获取首冲按钮
	UIButton * getBtnFirstReCharge();

private:
	void changeUIBySceneType( int mapType );
	//所有按钮是否显示以主界面为准
	void changeBtnVisibleByType( int mapType );

	void createUIForMainScene();
	void createUIForFiveInstanceScene();
	void createUIForArenaScene();
	void createUIForFamilyFightScene();
	void createUIForSingCopyScene();

	UILayer * u_layer_MainScene;
	UILayer * u_layer_FiveInstanceScene;
	UILayer * u_layer_ArenaScene;
	UILayer * u_layer_FamilyFightScene;
	UILayer * u_layer_SingCopyScene;

	bool isExistUIForMainScene;
	bool isExistUIForFiveInstanceScene;
	bool isExistUIForArenaScene;
	bool isExistUIForFamilyFightScene;
	bool isExistUIForSingCopyScene;
	//可缩回层
	UILayer * layer_action;

	std::string timeFormatToString( int t );
private:
	//MainScene* mainUILayer;
	CCSize winSize;

	/*SceneType m_curSceneType;*/

	std::vector<CFunctionOpenLevel *>GMOpendedFunctionVector_1;   //可以显示的按钮(地图区域1，功能入口类型为2)
	std::vector<CFunctionOpenLevel *>GMOpendedFunctionVector;   //可以显示的按钮(地图区域2，功能入口类型为3)
	std::vector<GMCoordinate> CoordinateVector;

	//武将模式
	int m_generalMode;
	//武将自动出战
	int m_autoFight;

	UILabelBMFont * l_remainTimeValue;
	int m_nRemainTime;
};

#endif

