
#include "cocos2d.h"
#include "../../ui/extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCRichLabel;
class CCMoveableMenu;
class ChatCell;
typedef enum{
	TABVIEWCHATWINDOS_TAG=101,
};

/**
   * 主界面底部的小聊天窗
   **/

class ChatWindows:public UIScene, public cocos2d::extension::CCTableViewDataSource, public cocos2d::extension::CCTableViewDelegate
{
public:
	ChatWindows(void);
	~ChatWindows(void);

	static ChatWindows *create();
	bool init();
	virtual void onEnter();
	virtual void onExit();
	
	enum
	{
		chatWindowsState_show = 0,
		chatWindowsState_showUp,
		chatWindowsState_showDown
	};

	static void addToMiniChatWindow(int channel_id,std::string play_name,std::string player_country,std::string describe,long long playerid,bool isMainChat,int viplevel,long long listenerId,std::string listenerName);

	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);


	// default implements are used to call script callback if exist
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void callBack(CCObject * obj);	
	void showMainUi(float delta);
	void setShowMainScene(CCObject * obj);
	bool showMainScene;
	static std::vector<ChatCell *>s_chatCellsDataVector;
	CCTableView* tableView;
	CCScale9Sprite * pImageView_chatBg;
	int remTime;
	
	void chatPrivate(CCObject * obj);

	void setChatWindowsState(int value_);
	int getChatWindowsState();

private:
	CCLabelTTF *channel_label;
	CCLabelTTF *p_Name;
	CCLabelTTF *label_colon;
	
	int m_chatWindowsState;

	CCRichLabel *labelLink;
	int time_;
	
};

