#include "MyHeadInfo.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "HeadMenu.h"
#include "../MainScene.h"
#include "ChatWindows.h"
#include "../../utils/StaticDataManager.h"
#include "ExStatusItem.h"
#include "../exstatus/ExStatus.h"
#include "MyBuff.h"
#include "../../ui/extensions/UITab.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../legend_script/UITutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../SimpleEffectManager.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/element/CFunctionOpenLevel.h"
#include "../../messageclient/element/CMapInfo.h"
#include "AppMacros.h"
#include "../../utils/GameConfig.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "PKModeUI.h"

#define kTagForPKMode 698

MyHeadInfo::MyHeadInfo():
isTabStateOn(false)
{
}


MyHeadInfo::~MyHeadInfo()
{
}


MyHeadInfo* MyHeadInfo::create()
{
	MyHeadInfo * myHeadInfo = new MyHeadInfo();
	if (myHeadInfo && myHeadInfo->init())
	{
		myHeadInfo->autorelease();
		return myHeadInfo;
	}
	CC_SAFE_DELETE(myHeadInfo);
	return NULL;
}

bool MyHeadInfo::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

		ActiveRole * activeRole = GameView::getInstance()->myplayer->getActiveRole();
		pkModeOpenLevel = 30;
		for(int i = 0;i<(int)FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() != 5)
				continue;

			if (strcmp("btn_pkMode",FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str()) == 0)
			{
				pkModeOpenLevel = FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel();
			}
		}

		layer_pkmode = UILayer::create();
		m_pUiLayer->addChild(layer_pkmode);
		layer_headIndo= UILayer::create();
		m_pUiLayer->addChild(layer_headIndo);

		//和平
		Button_state = UIButton::create();
		Button_state->setTouchEnable(true);
		Button_state->setPressedActionEnabled(true);
		Button_state->setTextures("gamescene_state/zhujiemian3/zhujuetouxiang/heping_di.png", "gamescene_state/zhujiemian3/zhujuetouxiang/heping_di.png", "");
		Button_state->setAnchorPoint(ccp(0.5f,0.5f));
		Button_state->setPosition(ccp(198+Button_state->getContentSize().width/2,33+Button_state->getContentSize().height/2));
		Button_state->addReleaseEvent(this, coco_releaseselector(MyHeadInfo::ButtonStateEvent));
		layer_pkmode->addWidget(Button_state);
		stateBG = UIImageView::create();
		stateBG->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/heping.png");
		stateBG->setAnchorPoint(ccp(0.5f,0.5f));
		stateBG->setPosition(ccp(0,0));
		Button_state->addChild(stateBG);
		applyPKMode(GameView::getInstance()->myplayer->getPKMode());

		//退组
// 		teamInLine = UIButton::create();
// 		teamInLine->setAnchorPoint(ccp(0.5f,0.5f));
// 		teamInLine->setTouchEnable(true);
// 		teamInLine->setTextures("gamescene_state/zhujiemian3/zhujuetouxiang/tuizu_di.png","gamescene_state/zhujiemian3/zhujuetouxiang/tuizu_di.png","");
// 		teamInLine->addReleaseEvent(this,coco_releaseselector(MyHeadInfo::callBackExitTeam));
// 		teamInLine->setPosition(ccp(234,15));
// 		teamInLine->setPressedActionEnabled(true);
// 
// 		UIImageView * imageView_quitTeam = UIImageView::create();
// 		imageView_quitTeam->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/tuizu.png");
// 		imageView_quitTeam->setAnchorPoint(ccp(0.5f,0.5f));
// 		imageView_quitTeam->setPosition(ccp(0,0));
// 		teamInLine->addChild(imageView_quitTeam);
// 		
// 		if(GameView::getInstance()->teamMemberVector.size() >= 2)
// 		{
// 			teamInLine->setVisible(true);
// 		}
// 		else
// 		{
// 			teamInLine->setVisible(false);
// 		}
// 		layer_pkmode->addWidget(teamInLine);

// 		UIImageView *ImageView_background = UIImageView::create();
// 		ImageView_background->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/di.png");
// 		ImageView_background->setName("ImageView_background");
// 		ImageView_background->setAnchorPoint(ccp(0,0));
// 		ImageView_background->setPosition(ccp(0,0));
// 		layer_headIndo->addWidget(ImageView_background);

		UIButton *Button_Frame = UIButton::create();
		Button_Frame->setTextures("gamescene_state/zhujiemian3/zhujuetouxiang/xuetiaodi.png","gamescene_state/zhujiemian3/zhujuetouxiang/xuetiaodi.png","");
		Button_Frame->setName("Button_Frame");
		Button_Frame->setTouchEnable(true);
		Button_Frame->setPressedActionEnabled(true,0.9f,1.1f);
		Button_Frame->setAnchorPoint(ccp(0.5f,0.5f));
		Button_Frame->setPosition(ccp(199-Button_Frame->getContentSize().width/2,74-Button_Frame->getContentSize().height/2));
		Button_Frame->addReleaseEvent(this, coco_releaseselector(MyHeadInfo::ButtonFrameEvent));
		layer_headIndo->addWidget(Button_Frame);

		//头像
		Button_headFrame = UIButton::create();
		Button_headFrame->setTouchEnable(true);
		Button_headFrame->setPressedActionEnabled(true,0.9f,1.1f);
		//Button_headFrame->setTextures(BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str(), BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str(), "");
		Button_headFrame->setTextures("gamescene_state/zhujiemian3/zhujuetouxiang/touxiang_di.png","gamescene_state/zhujiemian3/zhujuetouxiang/touxiang_di.png", "");
		Button_headFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Button_headFrame->setPosition(ccp(Button_headFrame->getContentSize().width/2,Button_headFrame->getContentSize().height/2));
		Button_headFrame->addReleaseEvent(this, coco_releaseselector(MyHeadInfo::ButtonHeadEvent));
		layer_headIndo->addWidget(Button_headFrame);

		UIImageView * imageView_head = UIImageView::create();
		imageView_head->setTexture(BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str());
		imageView_head->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_head->setPosition(ccp(1,6));
		Button_headFrame->addChild(imageView_head);

		UILabel * Label_space = UILabel::create();
		Label_space->setText("/");
		Label_space->setFontName(APP_FONT_NAME);
		Label_space->setFontSize(12); 
		Label_space->setAnchorPoint(CCPointZero);
		Label_space->setPosition(ccp(7,7));
		Label_space->setWidgetZOrder(100);
		Label_space->setStrokeEnabled(true);
		Button_Frame->addChild(Label_space);

		Label_allBlood = UILabel::create();
		char s_maxhp[10];
		sprintf(s_maxhp,"%d",activeRole->maxhp());
		Label_allBlood->setText(s_maxhp);
		Label_allBlood->setFontName(APP_FONT_NAME);
		Label_allBlood->setFontSize(12); 
		Label_allBlood->setAnchorPoint(CCPointZero);
		Label_allBlood->setPosition(ccp(Label_space->getPosition().x+Label_space->getContentSize().width+3,Label_space->getPosition().y));
		Label_allBlood->setWidgetZOrder(100);
		Label_allBlood->setStrokeEnabled(true);
		Button_Frame->addChild(Label_allBlood);
		Label_curBlood = UILabel::create();
		char s_hp[10];
		//itoa(activeRole->hp(),s,10);
		sprintf(s_hp,"%d",activeRole->hp());
		Label_curBlood->setText(s_hp);
		Label_curBlood->setFontName(APP_FONT_NAME);
		Label_curBlood->setFontSize(12); 
		Label_curBlood->setAnchorPoint(ccp(1.0f,0));
		Label_curBlood->setPosition(ccp(Label_space->getPosition().x-3,Label_space->getPosition().y));
		Label_curBlood->setWidgetZOrder(100);
		Label_curBlood->setStrokeEnabled(true);
		Button_Frame->addChild(Label_curBlood);
		

		Label_space = UILabel::create();
		Label_space->setText("/");
		Label_space->setFontName(APP_FONT_NAME);
		Label_space->setFontSize(12); 
		Label_space->setAnchorPoint(CCPointZero);
		Label_space->setPosition(ccp(7,-8));
		Label_space->setWidgetZOrder(100);
		Label_space->setStrokeEnabled(true);
		Button_Frame->addChild(Label_space);

		Label_allMagic = UILabel::create();
		char s_maxmp[10];
		sprintf(s_maxmp,"%d",activeRole->maxmp());
		Label_allMagic->setText(s_maxmp);
		Label_allMagic->setFontName(APP_FONT_NAME);
		Label_allMagic->setFontSize(12); 
		Label_allMagic->setAnchorPoint(CCPointZero);
		Label_allMagic->setPosition(ccp(Label_space->getPosition().x+Label_space->getContentSize().width+3,Label_space->getPosition().y));
		Label_allMagic->setWidgetZOrder(100);
		Label_allMagic->setStrokeEnabled(true);
		Button_Frame->addChild(Label_allMagic);
		Label_curMagic = UILabel::create();
		char s_mp[10];
		sprintf(s_mp,"%d",activeRole->mp());
		Label_curMagic->setText(s_mp);
		Label_curMagic->setFontName(APP_FONT_NAME);
		Label_curMagic->setFontSize(12); 
		Label_curMagic->setAnchorPoint(ccp(1.0f,0));
		Label_curMagic->setPosition(ccp(Label_space->getPosition().x-3,Label_space->getPosition().y));
		Label_curMagic->setWidgetZOrder(100);
		Label_curMagic->setStrokeEnabled(true);
		Button_Frame->addChild(Label_curMagic);

		Label_level = UILabel::create();
		char s_level[10];
		sprintf(s_level,"%d",activeRole->level());
		Label_level->setText(s_level);
		Label_level->setFontName(APP_FONT_NAME);
		Label_level->setFontSize(14); 
		Label_level->setAnchorPoint(ccp(0.5f,0.5f));
		Label_level->setPosition(ccp(14-Button_headFrame->getContentSize().width/2,11-Button_headFrame->getContentSize().height/2));
		Label_level->setStrokeEnabled(true);
		Button_headFrame->addChild(Label_level);

		UIImageView *ImageView_blood_frame = UIImageView::create();
		ImageView_blood_frame->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/xuetiao_di.png");
		ImageView_blood_frame->setScale9Enable(true);
		ImageView_blood_frame->setCapInsets(CCRectMake(7,7,1,1));
		ImageView_blood_frame->setScale9Size(CCSizeMake(118,14));
		ImageView_blood_frame->setAnchorPoint(ccp(0,0.5f));
		ImageView_blood_frame->setPosition(ccp(-52,15));
		Button_Frame->addChild(ImageView_blood_frame);

		ImageView_blood = UIImageView::create();
		ImageView_blood->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/red_xuetiao.png");
		ImageView_blood->setAnchorPoint(ccp(0,0.5f));
		ImageView_blood->setPosition(ccp(-52,15));
		char * a = (char *)Label_curBlood->getStringValue();
		char * b = (char *)Label_allBlood->getStringValue();
		float _a = atof(a);
		float _b = atof(b);
		float blood_scale = _a/_b;
		if (blood_scale > 1.0f)
			blood_scale = 1.0f;

		ImageView_blood->setTextureRect(CCRectMake(0,0,ImageView_blood->getContentSize().width*blood_scale,ImageView_blood->getContentSize().height));
		Button_Frame->addChild(ImageView_blood);
		ImageView_magic = UIImageView::create();
		ImageView_magic->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/blue_xuetiao.png");
		ImageView_magic->setAnchorPoint(ccp(0,0.5f));
		ImageView_magic->setPosition(ccp(-49,0));
		char * c = (char *)Label_curMagic->getStringValue();
		char * d = (char *)Label_allMagic->getStringValue();
		float _c = atof(c);
		float _d = atof(d);
		float magic_scale = _c/_d;
		if (magic_scale > 1.0f)
			magic_scale = 1.0f;

		ImageView_magic->setTextureRect(CCRectMake(0,0,ImageView_magic->getContentSize().width*magic_scale,ImageView_magic->getContentSize().height));
		Button_Frame->addChild(ImageView_magic);

		// 体力
		UILabel * Label_space_phy = UILabel::create();
		Label_space_phy->setText("/");
		Label_space_phy->setFontName(APP_FONT_NAME);
		Label_space_phy->setFontSize(12); 
		Label_space_phy->setAnchorPoint(CCPointZero);
		Label_space_phy->setPosition(ccp(7,-22));
		Label_space_phy->setWidgetZOrder(100);
		Label_space_phy->setStrokeEnabled(true);
		Button_Frame->addChild(Label_space_phy);

		Label_allPhy= UILabel::create();
		char s_maxPhy[10];
		sprintf(s_maxPhy,"%d",GameView::getInstance()->myplayer->getPhysicalCapacity());
		Label_allPhy->setText(s_maxPhy);
		Label_allPhy->setFontName(APP_FONT_NAME);
		Label_allPhy->setFontSize(12); 
		Label_allPhy->setAnchorPoint(CCPointZero);
		Label_allPhy->setPosition(ccp(Label_space_phy->getPosition().x+Label_space_phy->getContentSize().width+3,Label_space_phy->getPosition().y));
		Label_allPhy->setWidgetZOrder(100);
		Label_allPhy->setStrokeEnabled(true);
		Button_Frame->addChild(Label_allPhy);
		Label_curPhy = UILabel::create();
		char s_phy[10];
		sprintf(s_phy,"%d",GameView::getInstance()->myplayer->getPhysicalValue());
		Label_curPhy->setText(s_phy);
		Label_curPhy->setFontName(APP_FONT_NAME);
		Label_curPhy->setFontSize(12); 
		Label_curPhy->setAnchorPoint(ccp(1.0f,0));
		Label_curPhy->setPosition(ccp(Label_space_phy->getPosition().x-3,Label_space_phy->getPosition().y));
		Label_curPhy->setWidgetZOrder(100);
		Label_curPhy->setStrokeEnabled(true);
		Button_Frame->addChild(Label_curPhy);

		ImageView_phy = UIImageView::create();
		ImageView_phy->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/green_xuetiao.png");
		ImageView_phy->setAnchorPoint(ccp(0,0.5f));
		ImageView_phy->setPosition(ccp(-47,-14));
		char * a_phy = (char *)Label_curPhy->getStringValue();
		char * b_phy = (char *)Label_allPhy->getStringValue();
		float _a_phy = atof(a_phy);
		float _b_phy = atof(b_phy);
		float phy_scale = _a_phy/_b_phy;
		if (phy_scale > 1.0f)
			phy_scale = 1.0f;

		ImageView_phy->setTextureRect(CCRectMake(0,0,ImageView_phy->getContentSize().width*phy_scale,ImageView_phy->getContentSize().height));
		Button_Frame->addChild(ImageView_phy);

		for (int i = 0;i<(int)GameView::getInstance()->myplayer->getExStatusVector().size();++i)
		{
			if (i>4)
				break;

			ExStatus * tempExstatus = GameView::getInstance()->myplayer->getExStatusVector().at(i);
			ExStatusItem * exStatusItem = ExStatusItem::create(tempExstatus);
			exStatusItem->setScale(0.5f);
			exStatusItem->setAnchorPoint(ccp(0,0));
			exStatusItem->setPosition(ccp(80+22*i,2));
			exStatusItem->setTag(1000+i);
			addChild(exStatusItem);
		}

		// vipInfo
		ImageView_Vip = UIImageView::create();
		ImageView_Vip->setTexture("");
		ImageView_Vip->setAnchorPoint(ccp(0.5f,0.5f));
		ImageView_Vip->setPosition(ccp(13-Button_headFrame->getContentSize().width/2,61-Button_headFrame->getContentSize().height/2));
		Button_headFrame->addChild(ImageView_Vip);

		lbt_vipLevel = UILabelBMFont::create();
		lbt_vipLevel->setText("");
		lbt_vipLevel->setFntFile("res_ui/font/ziti_3.fnt");
		lbt_vipLevel->setScale(0.7f);
		lbt_vipLevel->setAnchorPoint(ccp(0.5f,0.5f));
		lbt_vipLevel->setPosition(ccp(27-Button_headFrame->getContentSize().width/2,56-Button_headFrame->getContentSize().height/2));
		Button_headFrame->addChild(lbt_vipLevel);

		// vip开关控制
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (vipEnabled)
		{
			ImageView_Vip->setVisible(true);
			lbt_vipLevel->setVisible(true);
			ReloadVipInfo();
		}
		else
		{
			ImageView_Vip->setVisible(false);
			lbt_vipLevel->setVisible(false);
		}

		this->ignoreAnchorPointForPosition(false);
		this->setAnchorPoint(ccp(0,0));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(199,74));

		return true;
	}
	return false;
}

void MyHeadInfo::onEnter()
{
	UIScene::onEnter();
}

void MyHeadInfo::onExit()
{
	UIScene::onExit();
}

bool MyHeadInfo::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return false;

	CCPoint location = pTouch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		//add by yangjun 2014.10.13
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		CCRect rect_l(pos.x, pos.y, 66, size.height);
		CCRect rect_r(pos.x+66, pos.y+31, size.width-80, size.height-31);
		if (rect_l.containsPoint(location))
		{
			this->ButtonHeadEvent(Button_headFrame);
		}
		else if (rect_r.containsPoint(location))
		{
			//弹出Buff列表
			CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
			MyBuff * myBuff = MyBuff::create();
			mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
			myBuff->ignoreAnchorPointForPosition(false);
			myBuff->setAnchorPoint(ccp(0,1.0f));
			myBuff->setPosition(ccp(this->getContentSize().width,winSize.height - this->getContentSize().height));
			mainScene->addChild(myBuff,0,kTagMyBuff);
		}
		
		return true;
	}
	//this->removeFromParent();
	return false;
}

void MyHeadInfo::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void MyHeadInfo::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void MyHeadInfo::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

void MyHeadInfo::ReloadMyPlayerData()
{
	ActiveRole * activeRole = GameView::getInstance()->myplayer->getActiveRole();
	char s_maxhp[20];
	sprintf(s_maxhp,"%d",activeRole->maxhp());
	Label_allBlood->setText(s_maxhp);

	char s_hp[20];
	sprintf(s_hp,"%d",activeRole->hp());
	Label_curBlood->setText(s_hp);

	char s_maxmp[20];
	sprintf(s_maxmp,"%d",activeRole->maxmp());
	Label_allMagic->setText(s_maxmp);

	char s_mp[20];
	sprintf(s_mp,"%d",activeRole->mp());
	Label_curMagic->setText(s_mp);

	char s_level[20];
	sprintf(s_level,"%d",activeRole->level());
	Label_level->setText(s_level);

	char * a = (char *)Label_curBlood->getStringValue();
	char * b = (char *)Label_allBlood->getStringValue();
	float _a = atof(a);
	float _b = atof(b);
	float blood_scale = _a/_b;
	if (blood_scale > 1.0f)
		blood_scale = 1.0f;

	ImageView_blood->setTextureRect(CCRectMake(0,0,ImageView_blood->getContentSize().width*blood_scale,ImageView_blood->getContentSize().height));

	char * c = (char *)Label_curMagic->getStringValue();
	char * d = (char *)Label_allMagic->getStringValue();
	float _c = atof(c);
	float _d = atof(d);
	blood_scale = _c/_d;
	if (blood_scale > 1.0f)
		blood_scale = 1.0f;

	ImageView_magic->setTextureRect(CCRectMake(0,0,ImageView_magic->getContentSize().width*blood_scale,ImageView_magic->getContentSize().height));

	// 体力
	char s_maxPhy[10];
	sprintf(s_maxPhy,"%d",GameView::getInstance()->myplayer->getPhysicalCapacity());
	Label_allPhy->setText(s_maxPhy);
	char s_phy[10];
	sprintf(s_phy,"%d",GameView::getInstance()->myplayer->getPhysicalValue());
	Label_curPhy->setText(s_phy);

	char * e = (char *)Label_curPhy->getStringValue();
	char * f = (char *)Label_allPhy->getStringValue();
	float _e = atof(e);
	float _f = atof(f);
	float phy_scale = _e/_f;
	if (phy_scale > 1.0f)
		phy_scale = 1.0f;

	ImageView_phy->setTextureRect(CCRectMake(0,0,ImageView_phy->getContentSize().width*phy_scale,ImageView_phy->getContentSize().height));
}

void MyHeadInfo::ButtonHeadEvent(CCObject *pSender)
{
	//竞技场中不需要
	if (GameView::getInstance()->getMapInfo()->maptype() == coliseum)
		return;

	mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
	mainScene->ButtonHeadEvent(pSender);

	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
 	if(sc != NULL)
 		sc->endCommand(pSender);
}

void MyHeadInfo::ButtonLVTeamEvent( CCObject *pSender )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	//mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
	mainScene->ButtonLVTeamEvent(pSender);
}

void MyHeadInfo::ReloadBuffData()
{
	for (int i = 0;i<5;++i)
	{
		if (this->getChildByTag(1000+i))
		{
			ExStatusItem * exStatusItem = (ExStatusItem *)this->getChildByTag(1000+i);
			exStatusItem->removeFromParent();
		}
		else break;
	}

	for (int i = 0;i<(int)GameView::getInstance()->myplayer->getExStatusVector().size();++i)
	{
		if (i>4)
			break;

		ExStatus * tempExstatus = GameView::getInstance()->myplayer->getExStatusVector().at(i);

		if (tempExstatus->type()== 37 ||tempExstatus->type() == 38 ||tempExstatus->type() == 39)
			continue;

		ExStatusItem * exStatusItem = ExStatusItem::create(tempExstatus);
		exStatusItem->setScale(0.5f);
		exStatusItem->setAnchorPoint(ccp(0,0));
		exStatusItem->setPosition(ccp(80+22*i,2));
		exStatusItem->setTag(1000+i);
		addChild(exStatusItem);
	}
}

void MyHeadInfo::ButtonStateEvent(CCObject *pSender)
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	if(GameView::getInstance()->myplayer->getActiveRole()->level() < pkModeOpenLevel)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("pkmode_will_be_open"));
		return;
	}

	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	if(GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::guildFight)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("pkmode_will_not_openInFamilyFight"));
		return;
	}

// 	if (isTabStateOn == true)
// 	{
// 		//Tab_state->removeFromParent();
// 		if (this->getChildByTag(kTagForPKMode))
// 			(this->getChildByTag(kTagForPKMode))->removeFromParent();
// 
// 		isTabStateOn = false;
// 	}
// 	else
// 	{
// 		UILayer* layer_temp = UILayer::create();
// 		addChild(layer_temp);
// 		layer_temp->setTag(kTagForPKMode);
// 		//弹出下拉列表
// 		char * TabNames[] = {"gamescene_state/zhujiemian3/zhujuetouxiang/heping.png",
// 			"gamescene_state/zhujiemian3/zhujuetouxiang/pk.png",
// 			"gamescene_state/zhujiemian3/zhujuetouxiang/family.png",
// 			"gamescene_state/zhujiemian3/zhujuetouxiang/country.png"};
// 		Tab_state = UITab::createWithImage(4,"gamescene_state/zhujiemian3/zhujuetouxiang/tuizu_di.png","gamescene_state/zhujiemian3/zhujuetouxiang/tuizu_di.png","",TabNames,VERTICAL_LIST,5);
// 		Tab_state->setAnchorPoint(ccp(0,0));
// 		Tab_state->setPosition(ccp(((UIButton*)pSender)->getPosition().x - ((UIButton*)pSender)->getContentSize().width/2+6,((UIButton*)pSender)->getPosition().y-((UIButton*)pSender)->getContentSize().height/2));
// 		Tab_state->setHighLightImage("gamescene_state/zhujiemian3/zhujuetouxiang/tuizu_di.png");
// 		Tab_state->setDefaultPanelByIndex(0);
// 		Tab_state->addIndexChangedEvent(this,coco_indexchangedselector(MyHeadInfo::TabStateIndexChangedEvent));
// 		Tab_state->setAutoClose(true);
// 		layer_temp->addWidget(Tab_state);
// 		isTabStateOn = true;
// 	}

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagForPKMode) == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		PKModeUI * modeUI = PKModeUI::create();
		modeUI->ignoreAnchorPointForPosition(false);
		modeUI->setAnchorPoint(ccp(0.f,1.0f));
		modeUI->setPosition(ccp(((UIButton*)pSender)->getPosition().x - ((UIButton*)pSender)->getContentSize().width/2+6,winSize.height-((UIButton*)pSender)->getContentSize().height));
		modeUI->setTag(kTagForPKMode);
		GameView::getInstance()->getMainUIScene()->addChild(modeUI);
	}
}

void MyHeadInfo::applyPKMode(int PKMode)
{
	switch(PKMode)
	{
	case 0: 
		stateBG->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/heping.png");
		break;
	case 1: 
		stateBG->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/pk.png");
		break;
	case 2: 
		stateBG->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/family.png");
		break;
	case 3: 
		stateBG->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/country.png");
		break;
	}
}

void MyHeadInfo::TabStateIndexChangedEvent( CCObject* pSender )
{
	//CCLog("TabStateIndexChangedEvent");
// 	int index = Tab_state->getCurrentIndex();
// 
// 	if (index == 2)//family
// 	{
// 		if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionid() == 0)
// 		{
// 			const char *dialog_family = StringDataManager::getString("PKMode_family_dialog");
// 			char* pkMode_dialog_family =const_cast<char*>(dialog_family);
// 			GameView::getInstance()->showAlertDialog(pkMode_dialog_family);
// 			return;
// 		}
// 	}
// 
// 	GameMessageProcessor::sharedMsgProcessor()->sendReq(1207, (void*)index);
// 
// 	applyPKMode(index);
// 
// 	isTabStateOn = false;
// 	//layer_pkmode->removeWidgetAndCleanUp(Tab_state,false);
// 	//Tab_state->removeFromParent();
// 	if (this->getChildByTag(kTagForPKMode))
// 		(this->getChildByTag(kTagForPKMode))->removeFromParent();
}

void MyHeadInfo::callBackExitTeam( CCObject * obj )
{
	int teamWork=3;
	int playerId=0;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)teamWork,(void *)playerId);
}

void MyHeadInfo::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void MyHeadInfo::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,30,70,true);
// 	tutorialIndicator->setPosition(ccp(pos.x+45,pos.y-100));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,76,76,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+76/2,pos.y+76/2));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
	 	mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void MyHeadInfo::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,35,35,true);
// 	tutorialIndicator->setPosition(ccp(pos.x+45,pos.y-85));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,60,30,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+234,pos.y+56));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void MyHeadInfo::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void MyHeadInfo::ReloadVipInfo()
{
	if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() > 0)
	{
		ImageView_Vip->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/vip1.png");
		char s_level[10];
		sprintf(s_level,"%d",GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel());
		lbt_vipLevel->setText(s_level);
	}
	else
	{
		ImageView_Vip->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/vip0.png");
		lbt_vipLevel->setText("");
	}
}

void MyHeadInfo::ButtonFrameEvent( CCObject *pSender )
{
	//弹出Buff列表
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainScene->getChildByTag(kTagMyBuff) == NULL)
	{
		MyBuff * myBuff = MyBuff::create();
		myBuff->ignoreAnchorPointForPosition(false);
		myBuff->setAnchorPoint(ccp(0,1.0f));
		myBuff->setPosition(ccp(this->getContentSize().width,winSize.height - this->getContentSize().height));
		mainScene->addChild(myBuff,0,kTagMyBuff);
	}
	
}

