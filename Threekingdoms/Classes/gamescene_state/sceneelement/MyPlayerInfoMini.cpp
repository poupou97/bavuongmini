#include "MyPlayerInfoMini.h"
#include "../role/BaseFighter.h"
#include "../role/Monster.h"
#include "../../messageclient/element/CMonsterBaseInfo.h"
#include "../../newcomerstory/NewCommerStoryManager.h"

MyPlayerInfoMini::MyPlayerInfoMini()
{
}

MyPlayerInfoMini::~MyPlayerInfoMini()
{
}

MyPlayerInfoMini* MyPlayerInfoMini::create(BaseFighter * baseFighter)
{
	MyPlayerInfoMini * targetInfo = new MyPlayerInfoMini();
	if (targetInfo && targetInfo->init(baseFighter))
	{
		targetInfo->autorelease();
		return targetInfo;
	}
	CC_SAFE_DELETE(targetInfo);
	return NULL;
}

bool MyPlayerInfoMini::init(BaseFighter * baseFighter)
{
	if (UIScene::init())
	{
		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

		baseFighterId = baseFighter->getRoleId();

		// red part
		ImageView_targetBlood = UIImageView::create();
		ImageView_targetBlood->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/green_xuetiao.png");
		ImageView_targetBlood->setAnchorPoint(ccp(0,0));
		ImageView_targetBlood->setPosition(ccp(0,0));
		float blood_scale = baseFighter->getActiveRole()->hp() * 1.0f / baseFighter->getActiveRole()->maxhp();
		if (blood_scale > 1.0f)
			blood_scale = 1.0f;
		ImageView_targetBlood->setTextureRect(CCRectMake(0,0,ImageView_targetBlood->getContentSize().width*blood_scale,ImageView_targetBlood->getContentSize().height));
		
		// background part
		UIImageView *ImageView_blood_frame = UIImageView::create();
		ImageView_blood_frame->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/xuetiao_di.png");
		ImageView_blood_frame->setScale9Enable(true);
		ImageView_blood_frame->setCapInsets(CCRectMake(7,7,1,1));
		int offset = 1;
		int w = ImageView_targetBlood->getContentSize().width + offset*2;
		int h = ImageView_targetBlood->getContentSize().height + offset*2;
		ImageView_blood_frame->setScale9Size(CCSizeMake(w,h));
		ImageView_blood_frame->setAnchorPoint(ccp(0,0));
		ImageView_blood_frame->setPosition(ccp(-offset,-offset));

		m_pUiLayer->addWidget(ImageView_blood_frame);
		m_pUiLayer->addWidget(ImageView_targetBlood);

		this->ignoreAnchorPointForPosition(false);
		this->setAnchorPoint(ccp(0,0));
		this->setTouchEnabled(false);
		this->setContentSize(CCSizeMake(w, h));

		this->setScaleX(0.5f);
		this->setScaleY(0.6f);
		Monster* pBoss = dynamic_cast<Monster*>(baseFighter);
		if(pBoss != NULL)
		{
			if(pBoss->getClazz() == CMonsterBaseInfo::clazz_world_boss)
			{
				this->setScaleX(1.4f);
				this->setScaleY(1.4f);
			}
			else if(pBoss->getClazz() == CMonsterBaseInfo::clazz_boss)
			{
				this->setScaleX(1.2f);
				this->setScaleY(1.2f);
			}
			else if(pBoss->getClazz() == CMonsterBaseInfo::clazz_elite)
			{
				this->setScaleX(1.0f);
				this->setScaleY(1.0f);
			}
		}

		// patch
		if( NewCommerStoryManager::getInstance()->IsNewComer())
		{
			this->setVisible(false);
		}

		return true;
	}
	return false;
}

void MyPlayerInfoMini::onEnter()
{
	UIScene::onEnter();
}

void MyPlayerInfoMini::onExit()
{
	UIScene::onExit();
}

bool MyPlayerInfoMini::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return false;
}

void MyPlayerInfoMini::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void MyPlayerInfoMini::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void MyPlayerInfoMini::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

void MyPlayerInfoMini::ReloadTargetData( BaseFighter * baseFighter)
{
	int nHp = baseFighter->getActiveRole()->hp();
	int nMaxHp = baseFighter->getActiveRole()->maxhp();

	float blood_scale = nHp * 1.0f / nMaxHp;

	if (blood_scale > 1.0f)
	{
		blood_scale = 1.0f;
	}
	
	ImageView_targetBlood->setTextureRect(CCRectMake(0,0,ImageView_targetBlood->getContentSize().width*blood_scale,ImageView_targetBlood->getContentSize().height));
}

