#include "SceneTest.h"
#include "GameView.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CMapInfo.h"


SceneTest::SceneTest()
{
	testString[0]="xsc.level";
	testString[1]="cac.level";
	testString[2]="gd.level";
}


SceneTest::~SceneTest()
{
}

SceneTest* SceneTest::create()
{
	SceneTest * sceneTest = new SceneTest();
	if (sceneTest && sceneTest->init())
	{
		sceneTest->autorelease();
		return sceneTest;
	}
	CC_SAFE_DELETE(sceneTest);
	return NULL;
}

bool SceneTest::init()
{
	if (UIScene::init())
	{
		//进入各个场景
		//////////////////美术测试/////////////////
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		CCSprite * testBg = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/di.png");
		testBg->setAnchorPoint(CCPointZero);
		testBg->setPosition(ccp(0,0));
		testBg->setContentSize(CCSizeMake(132,193));
		testBg->setScaleY(1.3f);
		testBg->setScaleX(0.8f);
		addChild(testBg);

		CCTableView * testTableView = CCTableView::create(this,CCSizeMake(132,193));
		testTableView->setDirection(kCCScrollViewDirectionVertical);
		testTableView->setAnchorPoint(CCPointZero);
		testTableView->setPosition(ccp(0,0));
		testTableView->setDelegate(this);
		testTableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(testTableView);

		this->ignoreAnchorPointForPosition(false);
		this->setAnchorPoint(CCPointZero);
		this->setPosition(CCPointZero);
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(132,193));

		//////////////////美术测试/////////////////
		return true;
	}
	return false;
}


void SceneTest::onEnter()
{
	UIScene::onEnter();
}
void SceneTest::onExit()
{
	UIScene::onExit();
}

bool SceneTest::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	CCPoint location = pTouch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}
	return false;
}
void SceneTest::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void SceneTest::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void SceneTest::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}


////////////////美术测试/////////////////////
CCSize SceneTest::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
	return CCSizeMake(132, 35);
}

CCTableViewCell* SceneTest::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called
	cell=new CCTableViewCell();
	cell->autorelease();

	CCLabelTTF * testCell = CCLabelTTF::create(testString[idx].c_str(),"Arial",18);
	ccColor3B _color=ccc3(255,255,255);
	testCell->setColor(_color);
	testCell->setAnchorPoint(CCPointZero);
	testCell->setPosition(ccp(20,0));
	cell->addChild(testCell);


	return cell;

}

unsigned int SceneTest::numberOfCellsInTableView(CCTableView *table)
{
	return 3;
}

void SceneTest::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
	CCLog("this.cell.id = %d",cell->getIdx());
	CCDirector::sharedDirector()->purgeCachedData();
	GameView::getInstance()->getMapInfo()->set_mapid(testString[cell->getIdx()]);
	// create the next scene and run it
	LoadSceneState* pScene = new LoadSceneState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}

}

void SceneTest::scrollViewDidScroll(CCScrollView* view )
{
}

void SceneTest::scrollViewDidZoom(CCScrollView* view )
{
}

////////////////美术测试/////////////////////
