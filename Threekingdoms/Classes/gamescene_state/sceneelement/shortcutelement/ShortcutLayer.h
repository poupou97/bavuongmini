

#ifndef _GAMESCENESTATE_SHORTCUTLAYER_H_
#define _GAMESCENESTATE_SHORTCUTLAYER_H_

#include "cocos-ext.h"
#include "../../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../../legend_script/CCTutorialIndicator.h"
#include "../../../utils/StaticDataManager.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CShortCut;
class FolderInfo;

class ShortcutLayer : public UIScene
{
public:
	ShortcutLayer();
	~ShortcutLayer();

	static ShortcutLayer* create();
	virtual bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void CloseEvent(CCObject * pSender);

	/******背包格子发生变化时调用（1301,1303）******/
	void RefreshAll();
	void RefreshOne(FolderInfo* folderInfo);

	void RefreshOneShortcutSlot(CShortCut * shortcut);
	void RefreshMusouEffect( float percentage );

	void setInfoByLayerType();

	virtual void update(float dt);
	void setSlotUseType(int index,bool canUse);

	void RefreshCD(std::string skillId);

	void addNewShortcutSlot();
	void PeerlessEvent(CCObject* pSender);

	//教学
	UIButton * btn_close;
	UIButton * btn_first;
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	//新手剧情中（不需要强制）
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

	//教学
	int mTutorialScriptInstanceId;

	/**************用于新手剧情*************/
	void setMusouShortCutSlotPresent(bool isPresent);
	//强制创建龙魂技能按钮，不管等级限制
	void createMusouShortCutSlotForce();
	//龙魂技能按钮恢复到正常状态，受等级限制
	void recoverMusouShortCutSlot();
	//显示几个技能按钮（不包括普通攻击）
	void presentShortCutSlot(int num);
	/*************************************/

public:
	UILayer * u_layer;
	UILayer * u_teachLayer;
	int shortcutSlotNum;
	CCPoint slotPos[5] ;
	UIImageView * bg;
	UIButton * closeButton;

	int shortcurTag[5];

public:
	//判断魔法水是否足够
	bool isEnoughMagic(CShortCut * shortcut);
	//判断是否被眩晕
	bool isImmobilize();
	//判断背包中是否还有该物品
	bool isEnoughGoods(CShortCut * shortcut);

	CCPoint touchBeganPoint;

};
#endif;

