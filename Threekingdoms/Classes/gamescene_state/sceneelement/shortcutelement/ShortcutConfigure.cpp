#include "ShortcutConfigure.h"
#include "ShortcutLayer.h"
#include "ShortcutUIConstant.h"
#include "../../../ui/extensions/UIScene.h"
#include "GameView.h"
#include "../../GameSceneState.h"
#include "../../../ui/skillscene/SkillScene.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../MainScene.h"
#include "../../../messageclient/element/CShortCut.h"
#include "../../skill/GameFightSkill.h"
#include "../../role/BasePlayer.h"
#include "../../role/MyPlayer.h"
#include "../../../ui/backpackscene/PacPageView.h"
#include "../../../messageclient/element/FolderInfo.h"
#include "../../../legend_script/UITutorialIndicator.h"
#include "../../../legend_script/Script.h"
#include "../../../legend_script/ScriptManager.h"
#include "../../../legend_script/CCTutorialIndicator.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../messageclient/element/CBaseSkill.h"
#include "../../../legend_script/CCTutorialParticle.h"
#include "AppMacros.h"

#define WIDGET_TAG_SHORTCUTSLOT_BASE 100

ShortcutConfigure::ShortcutConfigure()
{
}


ShortcutConfigure::~ShortcutConfigure()
{
}

ShortcutConfigure * ShortcutConfigure::create()
{
	ShortcutConfigure * shortcutConfigure = new ShortcutConfigure();
	if (shortcutConfigure && shortcutConfigure->init())
	{
		shortcutConfigure->autorelease();
		return shortcutConfigure;
	}
	CC_SAFE_DELETE(shortcutConfigure);
	return NULL;
}

bool ShortcutConfigure::init()
{
	if (UIScene::init())
	{
		CCSize screenSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

		UIImageView *bg = UIImageView::create();
		bg->setTexture("res_ui/zhezhao80.png");
		bg->setScale9Enable(true);
		bg->setScale9Size(screenSize);
		bg->setAnchorPoint(CCPointZero);
		bg->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(bg);

		for (int i = 0; i<4; ++i)
		{
			UIButton * button_skill = UIButton::create();
			button_skill->setTextures(SKILLEMPTYPATH,SKILLEMPTYPATH,SKILLEMPTYPATH);
			button_skill->setTouchEnable(true);
			button_skill->setAnchorPoint(ccp(0.5f,0.5f));
			button_skill->setPosition(slotPos[i]);
			button_skill->setWidgetTag(shortcurTag[i]+WIDGET_TAG_SHORTCUTSLOT_BASE);
			button_skill->addReleaseEvent(this,coco_releaseselector(ShortcutConfigure::ButtonSkillEvent));
			m_pUiLayer->addWidget(button_skill);
			button_skill->setVisible(false);

			if(i == DEFAULTATTACKSKILL_INDEX)
			{
				button_skill->setScale(SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL);
			}
			else
			{
				button_skill->setScale(SCALE_SHORTCUTSLOT);
			}

			int openlevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[i];
			if (GameView::getInstance()->myplayer->getActiveRole()->level()>=openlevel)
			{
				button_skill->setVisible(true);
			}

			if (i == 1)
			{
				btn_first = button_skill;
			}
		}
		for(int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
		{
			CShortCut * shortcut = new CShortCut();
			shortcut->CopyFrom(*(GameView::getInstance()->shortCutList.at(i)));
			if (shortcut->index() < MUSOUSKILL_INDEX)
			{
				if (shortcut->complexflag() != 1)
				{
					if (shortcut->storedtype() == 1)
					{
						if (shortcut->has_skillpropid())
						{
							std::string skillIconPath = "res_ui/jineng_icon/";

							// 						if (strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
							// 							||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
							// 							||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
							// 							||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
							// 						{
							// 							skillIconPath.append("jineng_2");
							// 						}
							// 						else
							// 						{
							CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[shortcut->skillpropid()];
							if (baseSkill)
							{
								skillIconPath.append(baseSkill->icon());
							}
							else
							{
								skillIconPath.append("jineng_2");
							}
							//}
							skillIconPath.append(".png");

							UIButton * tempButton = (UIButton *)m_pUiLayer->getWidgetByTag(shortcut->index()+WIDGET_TAG_SHORTCUTSLOT_BASE);
							UIImageView * _image = UIImageView::create();
							_image->setTexture(skillIconPath.c_str());
							_image->setAnchorPoint(ccp(0.5f,0.5f));
							_image->setPosition(ccp(0,0));
							tempButton->addChild(_image);
							_image->setScale(1.2f);
						}
					}
					else if (shortcut->storedtype() == 2)
					{
						if (shortcut->has_skillpropid())
						{
							std::string skillIconPath = "res_ui/props_icon/";
							skillIconPath.append(shortcut->icon());
							//skillIconPath.append("yaoshui");
							skillIconPath.append(".png");

							UIButton * tempButton = (UIButton *)m_pUiLayer->getWidgetByTag(shortcut->index()+WIDGET_TAG_SHORTCUTSLOT_BASE);
							UIImageView * _image = UIImageView::create();
							_image->setTexture(skillIconPath.c_str());
							_image->setAnchorPoint(ccp(0.5f,0.5f));
							_image->setPosition(ccp(0,0));
							tempButton->addChild(_image);
							_image->setScale(1.0f);
						}
					}

					UIButton * tempButton = (UIButton *)m_pUiLayer->getWidgetByTag(shortcut->index()+WIDGET_TAG_SHORTCUTSLOT_BASE);
					UIImageView * _hightLight = UIImageView::create();
					_hightLight->setTexture(HIGHLIGHT3);
					_hightLight->setAnchorPoint(ccp(0.5f,0.5f));
					_hightLight->setPosition(ccp(0,3));
					tempButton->addChild(_hightLight);
					_hightLight->setScale(1.0f);

// 					UIImageView * imageView_dressUp = UIImageView::create();
// 					imageView_dressUp->setTexture("gamescene_state/zhujiemian3/jinengqu/lolo.png");
// 					imageView_dressUp->setAnchorPoint(ccp(0.5f, 0.5f));
// 					imageView_dressUp->setPosition(ccp(0,-23));
// 					tempButton->addChild(imageView_dressUp);
// 					if (i == DEFAULTATTACKSKILL_INDEX)
// 					{
// 						imageView_dressUp->setScale(1.0f*SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL);
// 					}
// 					else
// 					{
// 						imageView_dressUp->setScale(0.85f*SCALE_SHORTCUTSLOT);
// 					}

					if (shortcut->storedtype() == 1)
					{
						if (shortcut->has_skillpropid())
						{
							UIWidget * image_skillVariety = SkillScene::GetSkillVarirtyImageView(shortcut->skillpropid().c_str());
							if (image_skillVariety)
							{
								image_skillVariety->setAnchorPoint(ccp(0.5f,0.5f));
								image_skillVariety->setPosition(ccp(15,-17));
								image_skillVariety->setScale(1.15f);
								tempButton->addChild(image_skillVariety);
							}
						}
					}
				}
			}
		
			delete shortcut;
		}
		btn_close = UIButton::create();
		btn_close->setTextures("res_ui/close.png","res_ui/close.png","res_ui/close.png");
		btn_close->setTouchEnable(true);
		btn_close->setAnchorPoint(CCPointZero);
		btn_close->setPosition(ccp(screenSize.width-50,screenSize.height-50));
		btn_close->addReleaseEvent(this,coco_releaseselector(ShortcutConfigure::CloseEvent));
		m_pUiLayer->addWidget(btn_close);

		this->setContentSize(screenSize);
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}



void ShortcutConfigure::onEnter()
{
	UIScene::onEnter();
	//this->openAnim();
}
void ShortcutConfigure::onExit()
{
	UIScene::onExit();
}	

bool ShortcutConfigure::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	return true;
}
void ShortcutConfigure::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void ShortcutConfigure::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void ShortcutConfigure::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}

void ShortcutConfigure::ButtonSkillEvent( CCObject *pSender )
{
	int index = ((UIButton*)pSender)->getTag();
	this->sendReqEquipSkill(index);
}

void ShortcutConfigure::CloseEvent( CCObject *pSender )
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	//this->closeAnim();
	this->removeFromParent();
}

void ShortcutConfigure::sendReqEquipSkill(int index)
{
	CShortCut * tempShortCut = new CShortCut();
	tempShortCut->set_index(index);
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene))
	{
		SkillScene * skillScene = (SkillScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
		tempShortCut->set_skillpropid(skillScene->curFightSkill->getId());
		tempShortCut->set_storedtype(1);
	}
	else if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack))
	{
		tempShortCut->set_skillpropid(GameView::getInstance()->pacPageView->curFolder->goods().id());
		tempShortCut->set_storedtype(2);
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1122, tempShortCut);
	delete tempShortCut;

	if (index-WIDGET_TAG_SHORTCUTSLOT_BASE == 1)
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(btn_first);
	}
}

void ShortcutConfigure::RefreshOneShortcutConfigure( CShortCut * shortcut )
{
	int index = shortcut->index();
	if (index >= MUSOUSKILL_INDEX)
		return;

	//delete
	UIButton* _btn = (UIButton*)m_pUiLayer->getWidgetByTag(index+WIDGET_TAG_SHORTCUTSLOT_BASE);
	_btn->removeFromParent();
	
	//new 
	UIButton * button_skill = UIButton::create();
	button_skill->setTextures(SKILLEMPTYPATH,SKILLEMPTYPATH,SKILLEMPTYPATH);
	button_skill->setTouchEnable(true);
	button_skill->setAnchorPoint(ccp(0.5f,0.5f));
	button_skill->setPosition(slotPos[index]);
	button_skill->setWidgetTag(shortcurTag[index]+WIDGET_TAG_SHORTCUTSLOT_BASE);
	button_skill->addReleaseEvent(this,coco_releaseselector(ShortcutConfigure::ButtonSkillEvent));
	m_pUiLayer->addWidget(button_skill);

	if(index == DEFAULTATTACKSKILL_INDEX)
	{
		button_skill->setScale(SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL);
	}
	else
	{
		button_skill->setScale(SCALE_SHORTCUTSLOT);
	}

	switch((int)shortcut->storedtype())
	{
	case 0 :
		break;
	case 1 :
		if (shortcut->has_skillpropid())
		{
			std::string skillIconPath = "res_ui/jineng_icon/";

// 			if (strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 				||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 				||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 				||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 			{
// 				skillIconPath.append("jineng_2");
// 			}
// 			else
// 			{
				CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[shortcut->skillpropid()];
				if (baseSkill)
				{
					skillIconPath.append(baseSkill->icon());
				}
				else
				{
					skillIconPath.append("jineng_2");
				}
//			}
			skillIconPath.append(".png");

			UIImageView * _image = UIImageView::create();
			_image->setTexture(skillIconPath.c_str());
			_image->setAnchorPoint(ccp(0.5f,0.5f));
			_image->setPosition(ccp(0,0));
			button_skill->addChild(_image);
			_image->setScale(1.2f);
		}

		break;
	case 2 :
		if (shortcut->has_skillpropid())
		{
			std::string skillIconPath = "res_ui/props_icon/";
			skillIconPath.append(shortcut->icon());
			//skillIconPath.append("yaoshui");
			skillIconPath.append(".png");

			UIImageView * _image = UIImageView::create();
			_image->setTexture(skillIconPath.c_str());
			_image->setAnchorPoint(ccp(0.5f,0.5f));
			_image->setPosition(ccp(0,0));
			button_skill->addChild(_image);

			CCLabelTTF *label_goodsNum = CCLabelTTF::create("78",APP_FONT_NAME,14);
			label_goodsNum->setPosition(ccp(0,0));
			ccColor3B shadowColor = ccc3(0,0,0);   // black
			label_goodsNum->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
			this->addChild(label_goodsNum);
			_image->setScale(1.0f);
		}

		break;
	case 3 :
		break;
	}

	UIImageView * _hightLight = UIImageView::create();
	_hightLight->setTexture(HIGHLIGHT3);
	_hightLight->setAnchorPoint(ccp(0.5f,0.5f));
	_hightLight->setPosition(ccp(0,3));
	button_skill->addChild(_hightLight);
	_hightLight->setScale(1.0f);

// 	UIImageView * imageView_dressUp = UIImageView::create();
// 	imageView_dressUp->setTexture("gamescene_state/zhujiemian3/jinengqu/lolo.png");
// 	imageView_dressUp->setAnchorPoint(ccp(0.5f, 0.5f));
// 	imageView_dressUp->setPosition(ccp(0,-23));
// 	button_skill->addChild(imageView_dressUp);
// 	if (index == DEFAULTATTACKSKILL_INDEX)
// 	{
// 		imageView_dressUp->setScale(1.0f*SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL);
// 	}
// 	else
// 	{
// 		imageView_dressUp->setScale(0.85f*SCALE_SHORTCUTSLOT);
// 	}

	if (shortcut->storedtype() == 1)
	{
		if (shortcut->has_skillpropid())
		{
			UIWidget * image_skillVariety = SkillScene::GetSkillVarirtyImageView(shortcut->skillpropid().c_str());
			if (image_skillVariety)
			{
				image_skillVariety->setAnchorPoint(ccp(0.5f,0.5f));
				image_skillVariety->setPosition(ccp(15,-17));
				image_skillVariety->setScale(1.15f);
				button_skill->addChild(image_skillVariety);
			}
		}
	}
}

void ShortcutConfigure::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void ShortcutConfigure::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  )
{
	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,25,60);
	tutorialIndicator->setPosition(ccp(pos.x-45,pos.y+95));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->addChild(tutorialIndicator);

// 	CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("tuowei0.plist",50,70);
// 	tutorialParticle->setPosition(ccp(pos.x,pos.y-30));
// 	tutorialParticle->setTag(CCTUTORIALPARTICLETAG);
// 	this->addChild(tutorialParticle);
}

void ShortcutConfigure::addCCTutorialIndicator2( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  )
{
	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,40,50);
	tutorialIndicator->setPosition(ccp(pos.x-40,pos.y-40));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->addChild(tutorialIndicator);

// 	CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("tuowei0.plist",40,50);
// 	tutorialParticle->setPosition(ccp(pos.x+20,pos.y-5));
// 	tutorialParticle->setTag(CCTUTORIALPARTICLETAG);
// 	this->addChild(tutorialParticle);
}

void ShortcutConfigure::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

// 	CCTutorialParticle* tutorialParticle = dynamic_cast<CCTutorialParticle*>(this->getChildByTag(CCTUTORIALPARTICLETAG));
// 	if(tutorialParticle != NULL)
// 		tutorialParticle->removeFromParent();
}
