#ifndef _GAMESCENE_STATE_SHORTCUT_CONSTANT_H_
#define _GAMESCENE_STATE_SHORTCUT_CONSTANT_H_

#define SKILLEMPTYPATH "gamescene_state/zhujiemian3/jinengqu/jinengdi_1.png"
#define MUSOUSKILL_DI "gamescene_state/zhujiemian3/jinengqu/wushuang_di.png"
#define MUSOUSKILL "gamescene_state/zhujiemian3/jinengqu/wushuang_1.png"
#define MUSOUSKILL_TIME "gamescene_state/zhujiemian3/jinengqu/wushuang_time.png"
#define MUSOUSKILL_OFF "gamescene_state/zhujiemian3/jinengqu/wushuang_off.png"
#define PROGRESS_SKILL "gamescene_state/zhujiemian3/jinengqu/jinengmengban.png"
#define MEDICINAL1 "gamescene_state/zhujiemian3/jinengqu/yaoshui.png"
#define HIGHLIGHT1 "gamescene_state/zhujiemian3/jinengqu/gaoguang_1.png"
#define HIGHLIGHT2 "gamescene_state/zhujiemian3/jinengqu/gaoguang_2.png"
#define HIGHLIGHT3 "gamescene_state/zhujiemian3/jinengqu/gaoguang_3.png"

#define DEFAULTATTACKSKILL_INDEX 0
#define MUSOUSKILL_INDEX 4

#define SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL 1.1f
#define SCALE_SHORTCUTSLOT_MUSOUSKILL 1.1f
#define SCALE_SHORTCUTSLOT 0.95f

#endif