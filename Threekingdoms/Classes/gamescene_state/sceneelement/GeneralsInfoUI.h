#ifndef _GAMESCENESTATE_GENERALSINFOUI_H_
#define _GAMESCENESTATE_GENERALSINFOUI_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../legend_script/CCTutorialIndicator.h"


USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralBaseMsg;
class CGeneralDetail;
class General;

/***********
*单个武将头像
* yangjun 
* 2014.9.25
***********/
class GeneralItem : public UIScene
{
public:

	enum General_Status
	{
		status_InBattle = 0,   //已出战
		status_InLine,           //在队列中，可出战
		status_InRest ,          //休息
		status_Died               //死亡
	};

	GeneralItem();
	~GeneralItem();
	//create generalsItem by generalBaseMsg
	static GeneralItem* create(CGeneralBaseMsg * generalBaseMsg,int index);
	bool init(CGeneralBaseMsg * generalBaseMsg,int index);
	//create void generalsItem by index
	static GeneralItem* create(int index);
	bool init(int index);

	static GeneralItem* createClocked(int index);
	bool initClocked(int index);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent){};
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent){};
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent){};

	virtual void update(float dt);

	//刷新状态
	void RefreshSelfStatus();
	//刷新界面(根据状态)
	void RefreshUI();
	//刷新技能的显示
	void RefreshSkillPresent(CGeneralDetail * generalDetail);
	//刷新血量
	void RefreshGeneralHP(General * general);
	void RefreshGeneralHP(CGeneralDetail * generalDetail);
	//开始跑休息的CD
	void RunRestCD();
	void RestCoolDownCallBack(CCNode* node);

private:
	UILayer * u_layer;
	
	//在场停留时间
	long long m_nBaseTimeInBattle;

public:
	UIButton * btn_headFrame;
	//pos
	int m_nIndex;
	//武将id
	long long m_nGeneralId;
	//当前状态
	int m_nCurStatus;
	//复活倒计时是否完成并发了请求
	bool m_bIsRevevidCountFinish;
private:
	void HeadFrameEvent(CCObject *pSender);
	void ClockedFrameEvent(CCObject *pSender);
};


/***********
*放武将头像的UI层
* yangjun 
* 2014.9.25
***********/

class GeneralsInfoUI : public UIScene
{
public:
	GeneralsInfoUI();
	~GeneralsInfoUI();

	static GeneralsInfoUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void RefreshAllGeneralItem();
	void RefreshOneGeneralItem(long long generalId);
	GeneralItem * getGeneralItemByIndex(int index);

	void ResetCDByGeneralId(long long generalID);

	void RefreshSkillByGeneralDetail(CGeneralDetail * generalDetail);

	void RefreshGeneralHP(General * general);
	void RefreshGeneralHP(CGeneralDetail * generalDetail);

	//重置现有的闪烁的特效，使他们的步伐保持一致
	void ResetAllFadeAction();

	//auto guide for call generals
	void setAutoGuideToCallGeneral(bool visible);
	void AutoGuideToCallGeneral(float dt);
	int curAutoGuideIndex;
private:
	UILayer * u_layer;
	UILayer * u_UpLayer;

public:
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	//新手剧情中（不需要强制）
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

	//教学
	int mTutorialScriptInstanceId;
	int mTutorialIndex;
};

#endif

