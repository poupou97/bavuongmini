
#ifndef _GAMESCENESTATE_HEADMENU_H_
#define _GAMESCENESTATE_HEADMENU_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../legend_script/CCTutorialIndicator.h"


USING_NS_CC;
USING_NS_CC_EXT;

class CFunctionOpenLevel;

class HeadMenu :public UIScene
{
public:
	HeadMenu();
	~HeadMenu();
	static HeadMenu* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);


	void ButtonRoleInfoEvent(CCObject *pSender);
	void ButtonSkillEvent(CCObject *pSender);
	//void ButtonPackageInfoEvent(CCObject *pSender);
	void ButtonStrengthenEvent(CCObject *pSender);
	void ButtonGeneralsEvent(CCObject *pSender);
	//void ButtonStudyEvent(CCObject *pSender);
	//void ButtonTacticalEvent(CCObject *pSender);
	//void ButtonArtOfWarEvent(CCObject *pSender);
	void ButtonMountEvent(CCObject *pSender);
	void ButtonMissionEvent(CCObject *pSender);
	void ButtonFriendEvent(CCObject *pSender);
	void ButtonMailEvent(CCObject *pSender);
	void ButtonFamilyEvent(CCObject *pSender);
	void ButtonRankingsEvent(CCObject *pSender);
	void ButtonHangUpEvent(CCObject *pSender);
	void ButtonSetEvent(CCObject *pSender);
	//void ButtonSoulEvent(CCObject *pSender);

	void addNewFunction(CCNode * pNode,void * functionOpenLevel);

	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	//study button is right 
	void addCCTutorialIndicatorRD( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();
public:
	UIButton * btn_roleIf;
	UIButton * btn_skill;
	UIButton * btn_strengthen;
	UIButton * btn_generals;
	UIButton * Button_mail;
	UIButton * btn_mission;
	UIButton * btn_mount;
	UIButton * Button_hangUp;
	UIButton * Button_family;
	UIButton * Button_rankings;
	UIButton * Button_friend;
private:
	CCSize winSize;
	struct FriendStruct
	{
		int relationType;
		int pageNum;
		int pageIndex;
		int showOnline;
	};

	std::vector<CFunctionOpenLevel *>opendedFunctionVector;   //可以显示的按钮(底部快捷栏)
	//教学
	int mTutorialScriptInstanceId;
};
#endif;
