
#ifndef _GAMESCENESTATE_TARGETINFO_H_
#define _GAMESCENESTATE_TARGETINFO_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class BaseFighter;
class UITab;
class CCRichLabel;

class TargetInfo : public UIScene
{
public:
	TargetInfo();
	~TargetInfo();

	static TargetInfo* create(BaseFighter *baseFighter);
	bool init(BaseFighter *baseFighter);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	virtual void update(float delta);

	UILabel * Label_targetAllBlood;
	UILabel * Label_targetCurBlood;
	UILabel * Label_targetAllMagic;
	UILabel * Label_targetCurMagic;
	CCRichLabel * Label_targetName;
	UIButton * Button_headFrame;
	UIButton * Button_Frame;
	UIImageView * imageView_head;
	UIImageView * ImageView_targetBlood;
	UIImageView * ImageView_targetMagic;
	UIImageView * ImageView_targetBloodGray;
	UILabel *  Label_targetLv;

	void initNewTargetInfo( BaseFighter * baseFighter );
	void ReloadTargetData( BaseFighter * baseFighter, bool bInitNewTarget = false);
	void ReloadBuffData();
	void ButtonTargetEvent(CCObject *pSender);
	void ButtonFrameEvent(CCObject *pSender);

	long long baseFighterId;
	std::string baseFightName;
	int coutryId;
	int viplevel_;
	int level_;
	int pressionId;


	UITab * Tab_target;
	bool isTabTargetOn;


public:
	float m_preScale;
	float m_nowScale;

	float m_timeDelay;

public:
	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};
};

//////////////////targetinfo list
class TargetInfoList : public UIScene
{
public:
	TargetInfoList();
	~TargetInfoList();

	static TargetInfoList* create(long long targetId ,std::string targetName,int countryid,int viplv,int level,int pressionId);
	bool init(long long targetId ,std::string targetName,int countryid,int viplv,int level,int pressionId);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);

	void TabTargetIndexChangedEvent(CCObject * obj);
private:
	CCSize winSize;
	long long objId;
	std::string objName;
	int objCountryId;
	int objViplv;
	int objLv;
	int ObjPressionId;

};
#endif;

