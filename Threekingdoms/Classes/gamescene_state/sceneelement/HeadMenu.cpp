#include "HeadMenu.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "GameView.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../ui/missionscene/MissionScene.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../messageclient/element/CNpcDialog.h"
#include "../../ui/npcscene/NpcTalkWindow.h"
#include "../../ui/mapscene/MapScene.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../ui/Mail_ui/MailUI.h"
#include "../GameSceneState.h"
#include "../../ui/UISceneTest.h"
#include "../../ui/pick_ui/PickUI.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../ui/Set_ui/SetUI.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../ui/generals_ui/GeneralsRecuriteUI.h"
#include "../MainScene.h"
#include "../../utils/GameUtils.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../messageclient/element/CActionDetail.h"
#include "../../messageclient/element/CFunctionOpenLevel.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "../../ui/storehouse_ui/StoreHouseUI.h"
#include "../role/MyPlayer.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../ui/generals_ui/GeneralsStrategiesUI.h"
#include "../../ui/generals_ui/GeneralsListBase.h"
#include "../../ui/family_ui/ApplyFamily.h"
#include "../../ui/family_ui/ManageFamily.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/UITutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../ui/robot_ui/RobotMainUI.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../messageclient/element/CGuildMemberBase.h"
#include "../../ui/Rank_ui/RankUI.h"
#include "../../legend_script/CCTeachingGuide.h"

#define FUNCTIONBUTTON_SPACE 65   //按钮间的间距

HeadMenu::HeadMenu()
{
}


HeadMenu::~HeadMenu()
{
	std::vector<CFunctionOpenLevel*>::iterator _iter;
	for (_iter = opendedFunctionVector.begin(); _iter != opendedFunctionVector.end(); ++_iter)
	{
		delete *_iter;
	}
	opendedFunctionVector.clear();
}

bool SortByPositionIndex(CFunctionOpenLevel * fcOpenLevel1 , CFunctionOpenLevel *fcOpenLevel2)
{
	return (fcOpenLevel1->get_positionIndex() < fcOpenLevel2->get_positionIndex());
}

HeadMenu * HeadMenu::create()
{
	HeadMenu * headMenu = new HeadMenu();
	if (headMenu && headMenu->init())
	{
		headMenu->autorelease();
		return headMenu;
	}
	CC_SAFE_DELETE(headMenu);
	return NULL;
}

bool HeadMenu::init()
{
	if (UIScene::init())
	{
		winSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
		
	
		UIImageView * ImageView_bk = UIImageView::create();
		ImageView_bk->setTexture("gamescene_state/zhujiemian3/renwushuxing/kuang.png");
		ImageView_bk->setScale9Enable(true);
		ImageView_bk->setScale9Size(CCSizeMake(winSize.width-43,27));
		ImageView_bk->setAnchorPoint(ccp(0.5f,0));
		ImageView_bk->setPosition(ccp(winSize.width/2,0));
		m_pUiLayer->addWidget(ImageView_bk);

		UIImageView * ImageView_LeftBG = UIImageView::create();
		ImageView_LeftBG->setTexture("gamescene_state/zhujiemian3/renwushuxing/xiao.png");
		ImageView_LeftBG->setAnchorPoint(CCPointZero);
		ImageView_LeftBG->setPosition(ccp(0,-2));
		m_pUiLayer->addWidget(ImageView_LeftBG);

		UIImageView * ImageView_RightBG = UIImageView::create();
		ImageView_RightBG->setTexture("gamescene_state/zhujiemian3/renwushuxing/xiao.png");
		ImageView_RightBG->setRotationY(180);
		ImageView_RightBG->setAnchorPoint(CCPointZero);
		ImageView_RightBG->setPosition(ccp(winSize.width,-2));
		m_pUiLayer->addWidget(ImageView_RightBG);
		//roleInfo
		btn_roleIf = UIButton::create();
		btn_roleIf->setTouchEnable(true);
		btn_roleIf->setTextures("gamescene_state/zhujiemian3/renwushuxing/renwua.png","gamescene_state/zhujiemian3/renwushuxing/renwua.png","");
		btn_roleIf->setAnchorPoint(ccp(0.5f,0.5f));
		btn_roleIf->setPosition(ccp(72,28));
		btn_roleIf->addReleaseEvent(this,coco_releaseselector(HeadMenu::ButtonRoleInfoEvent));
		btn_roleIf->setPressedActionEnabled(true);
		btn_roleIf->setName("btn_roleIf");
		btn_roleIf->setVisible(false);
		m_pUiLayer->addWidget(btn_roleIf);
		//skill
		btn_skill = UIButton::create();
		btn_skill->setTouchEnable(true);
		btn_skill->setTextures("gamescene_state/zhujiemian3/renwushuxing/skill.png","gamescene_state/zhujiemian3/renwushuxing/skill.png","");
		btn_skill->setAnchorPoint(ccp(0.5f,0.5f));
		btn_skill->setPosition(ccp(btn_roleIf->getPosition().x+btn_roleIf->getContentSize().width+10,btn_roleIf->getPosition().y));
		btn_skill->addReleaseEvent(this,coco_releaseselector(HeadMenu::ButtonSkillEvent));
		btn_skill->setPressedActionEnabled(true);
		btn_skill->setName("btn_skill");
		btn_skill->setVisible(false);
		m_pUiLayer->addWidget(btn_skill);
		//strengthen
		btn_strengthen  = UIButton::create();
		btn_strengthen->setTouchEnable(true);
		btn_strengthen->setTextures("gamescene_state/zhujiemian3/renwushuxing/qianghua.png","gamescene_state/zhujiemian3/renwushuxing/qianghua.png","");
		btn_strengthen->setAnchorPoint(ccp(0.5f,0.5f));
		btn_strengthen->setPosition(ccp(btn_skill->getPosition().x+btn_skill->getContentSize().width+10,btn_roleIf->getPosition().y));
		btn_strengthen->addReleaseEvent(this,coco_releaseselector(HeadMenu::ButtonStrengthenEvent));
		btn_strengthen->setPressedActionEnabled(true);
		btn_strengthen->setName("btn_strengthen");
		btn_strengthen->setVisible(false);
		m_pUiLayer->addWidget(btn_strengthen);

		//generals
		btn_generals = UIButton::create();
		btn_generals->setTouchEnable(true);
		btn_generals->setTextures("gamescene_state/zhujiemian3/renwushuxing/generals.png","gamescene_state/zhujiemian3/renwushuxing/generals.png","");
		btn_generals->setAnchorPoint(ccp(0.5f,0.5f));
		btn_generals->setPosition(ccp(btn_strengthen->getPosition().x+btn_strengthen->getContentSize().width+10,btn_roleIf->getPosition().y));
		btn_generals->addReleaseEvent(this,coco_releaseselector(HeadMenu::ButtonGeneralsEvent));
		btn_generals->setPressedActionEnabled(true);
		btn_generals->setName("btn_generals");
		btn_generals->setVisible(false);
		m_pUiLayer->addWidget(btn_generals);
		//mission
		btn_mission  = UIButton::create();
		btn_mission->setTouchEnable(true);
		btn_mission->setTextures("gamescene_state/zhujiemian3/renwushuxing/renwu.png","gamescene_state/zhujiemian3/renwushuxing/renwu.png","");
		btn_mission->setAnchorPoint(ccp(0.5f,0.5f));
		btn_mission->setPosition(ccp(btn_generals->getPosition().x+btn_generals->getContentSize().width+10,btn_roleIf->getPosition().y));
		btn_mission->addReleaseEvent(this,coco_releaseselector(HeadMenu::ButtonMissionEvent));
		btn_mission->setPressedActionEnabled(true);
		btn_mission->setName("btn_mission");
		btn_mission->setVisible(false);
		m_pUiLayer->addWidget(btn_mission);
		//mount 坐骑 
		btn_mount = UIButton::create();
		btn_mount->setTouchEnable(true);
		btn_mount->setTextures("gamescene_state/zhujiemian3/renwushuxing/zuoqi.png","gamescene_state/zhujiemian3/renwushuxing/zuoqi.png","");
		btn_mount->setAnchorPoint(ccp(0.5f,0.5f));
		btn_mount->setPosition(ccp(btn_mission->getPosition().x+btn_mission->getContentSize().width+10,btn_roleIf->getPosition().y));
		btn_mount->addReleaseEvent(this,coco_releaseselector(HeadMenu::ButtonMountEvent));
		btn_mount->setPressedActionEnabled(true);
		btn_mount->setName("btn_mount");
		btn_mount->setVisible(false);
		m_pUiLayer->addWidget(btn_mount);
		//friend
		Button_friend = UIButton::create();
		Button_friend->setTouchEnable(true);
		Button_friend->setTextures("gamescene_state/zhujiemian3/renwushuxing/haoyou.png","gamescene_state/zhujiemian3/renwushuxing/haoyou.png","");
		Button_friend->setAnchorPoint(ccp(0.5f,0.5f));
		Button_friend->setPosition(ccp(btn_mount->getPosition().x+btn_mount->getContentSize().width+10,btn_roleIf->getPosition().y));
		Button_friend->addReleaseEvent(this,coco_releaseselector(HeadMenu::ButtonFriendEvent));
		Button_friend->setPressedActionEnabled(true);
		Button_friend->setName("btn_friend");
		Button_friend->setVisible(false);
		m_pUiLayer->addWidget(Button_friend);
		//mail
		Button_mail = UIButton::create();
		Button_mail->setTouchEnable(true);
		Button_mail->setTextures("gamescene_state/zhujiemian3/renwushuxing/mail.png","gamescene_state/zhujiemian3/renwushuxing/mail.png","");
		Button_mail->setAnchorPoint(ccp(0.5f,0.5f));
		Button_mail->setPosition(ccp(Button_friend->getPosition().x+Button_friend->getContentSize().width+10,btn_roleIf->getPosition().y));
		Button_mail->addReleaseEvent(this,coco_releaseselector(HeadMenu::ButtonMailEvent));
		Button_mail->setPressedActionEnabled(true);
		Button_mail->setName("btn_mail");
		Button_mail->setVisible(false);
		m_pUiLayer->addWidget(Button_mail);
		//family
		Button_family = UIButton::create();
		Button_family->setTouchEnable(true);
		Button_family->setTextures("gamescene_state/zhujiemian3/renwushuxing/jiazu.png","gamescene_state/zhujiemian3/renwushuxing/jiazu.png","");
		Button_family->setAnchorPoint(ccp(0.5f,0.5f));
		Button_family->setPosition(ccp(Button_mail->getPosition().x+Button_mail->getContentSize().width+10,btn_roleIf->getPosition().y));
		Button_family->addReleaseEvent(this,coco_releaseselector(HeadMenu::ButtonFamilyEvent));
		Button_family->setPressedActionEnabled(true);
		Button_family->setName("btn_family");
		Button_family->setVisible(false);
		m_pUiLayer->addWidget(Button_family);
		//ranking
		Button_rankings = UIButton::create();
		Button_rankings->setTouchEnable(true);
		Button_rankings->setTextures("gamescene_state/zhujiemian3/renwushuxing/paihangbang.png","gamescene_state/zhujiemian3/renwushuxing/paihangbang.png","");
		Button_rankings->setAnchorPoint(ccp(0.5f,0.5f));
		Button_rankings->setPosition(ccp(Button_mail->getPosition().x+Button_mail->getContentSize().width+10,btn_roleIf->getPosition().y));
		Button_rankings->addReleaseEvent(this,coco_releaseselector(HeadMenu::ButtonRankingsEvent));
		Button_rankings->setPressedActionEnabled(true);
		Button_rankings->setName("btn_rankings");
		Button_rankings->setVisible(false);
		m_pUiLayer->addWidget(Button_rankings);
		//hangUp
		Button_hangUp = UIButton::create();
		Button_hangUp->setTouchEnable(true);
		Button_hangUp->setTextures("gamescene_state/zhujiemian3/renwushuxing/guaji.png","gamescene_state/zhujiemian3/renwushuxing/guaji.png","");
		Button_hangUp->setAnchorPoint(ccp(0.5f,0.5f));
		Button_hangUp->setPosition(ccp(Button_family->getPosition().x+Button_family->getContentSize().width+10,btn_roleIf->getPosition().y));
		Button_hangUp->addReleaseEvent(this,coco_releaseselector(HeadMenu::ButtonHangUpEvent));
		Button_hangUp->setPressedActionEnabled(true);
		Button_hangUp->setName("btn_hangUpSet");
		Button_hangUp->setVisible(false);
		m_pUiLayer->addWidget(Button_hangUp);
		//set
		UIButton * Button_set = UIButton::create();
		Button_set->setTouchEnable(true);
		Button_set->setTextures("gamescene_state/zhujiemian3/renwushuxing/set_up.png","gamescene_state/zhujiemian3/renwushuxing/set_up.png","");
		Button_set->setAnchorPoint(ccp(0.5f,0.5f));
		Button_set->setPosition(ccp(Button_hangUp->getPosition().x+Button_hangUp->getContentSize().width+10,btn_roleIf->getPosition().y));
		Button_set->addReleaseEvent(this,coco_releaseselector(HeadMenu::ButtonSetEvent));
		Button_set->setPressedActionEnabled(true);
		Button_set->setName("btn_set");
		Button_set->setVisible(false);
		m_pUiLayer->addWidget(Button_set);

		int roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
		for (int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();i++)
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() == 1)       //是底部菜单栏
			{
				if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel()<= roleLevel)     //可以显示
				{
					CFunctionOpenLevel * tempItem = new CFunctionOpenLevel();
					tempItem->set_functionId(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId());
					tempItem->set_functionName(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionName());
					tempItem->set_functionType(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType());
					tempItem->set_requiredLevel(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel());
					tempItem->set_positionIndex(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_positionIndex());
					opendedFunctionVector.push_back(tempItem);
				}
			}
		}
		sort(opendedFunctionVector.begin(),opendedFunctionVector.end(),SortByPositionIndex);
		for (int i = 0;i<opendedFunctionVector.size();i++)
		{
			if (m_pUiLayer->getWidgetByName(opendedFunctionVector.at(i)->get_functionId().c_str()))
			{
				(m_pUiLayer->getWidgetByName(opendedFunctionVector.at(i)->get_functionId().c_str()))->setVisible(true);
				(m_pUiLayer->getWidgetByName(opendedFunctionVector.at(i)->get_functionId().c_str()))->setPosition(ccp(75+FUNCTIONBUTTON_SPACE*i*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH),28));
			}
		}

		this->ignoreAnchorPointForPosition(false);
		this->setAnchorPoint(ccp(0,0));
		this->setTouchEnabled(false);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(winSize.width,58));

		return true;
	}
	return false;
}

void HeadMenu::onEnter()
{
	UIScene::onEnter();
// 	CCAction * action = CCMoveTo::create(0.3f,ccp(this->getPositionX(),this->getPositionY()+this->getContentSize().height));
// 	runAction(action);
}
void HeadMenu::onExit()
{
	UIScene::onExit();
}	

bool HeadMenu::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	return false;
}
void HeadMenu::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void HeadMenu::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void HeadMenu::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}

void HeadMenu::ButtonRoleInfoEvent( CCObject *pSender )
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);
	CCLOG("ButtonPackageInfoEvent...");
	PackageScene *backageLayer = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	if(backageLayer == NULL)
	{
		backageLayer = PackageScene::create();

		//CCSize s = CCDirector::sharedDirector()->getVisibleSize();

		GameView::getInstance()->getMainUIScene()->addChild(backageLayer,0,kTagBackpack);

		backageLayer->ignoreAnchorPointForPosition(false);
		backageLayer->setAnchorPoint(ccp(0.5f, 0.5f));
		backageLayer->setPosition(ccp(winSize.width/2, winSize.height/2));

		GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
		temp->page = 0;
		temp->pageSize = backageLayer->everyPageNum;
		temp->type = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
		delete temp;
	}

}

void HeadMenu::ButtonStrengthenEvent( CCObject *pSender )
{
	//暂时做地图界面入口
// 	CCLayer *mapLayer = (CCLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMap);
// 	if(mapLayer == NULL)
// 	{
// 		mapLayer = MapScene::create();
// 
// 		CCSize s = CCDirector::sharedDirector()->getVisibleSize();
// 
// 		mapLayer->autorelease();
// 		GameView::getInstance()->getMainUIScene()->addChild(mapLayer,0,kTagMap);
// 
// 		mapLayer->ignoreAnchorPointForPosition(false);
// 		mapLayer->setAnchorPoint(ccp(0.5f, 0.5f));
// 		mapLayer->setPosition(ccp(s.width/2, s.height/2));
// 	}

	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	long long currentTimeCreateUI = GameUtils::millisecondNow();
	EquipMentUi * equipmentUI = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	if (equipmentUI == NULL)
	{
		EquipMentUi * equip=EquipMentUi::create();
		equip->ignoreAnchorPointForPosition(false);
		equip->setAnchorPoint(ccp(0.5f,0.5f));
		equip->setPosition(ccp(winSize.width/2,winSize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(equip,0,ktagEquipMentUI);
		equip->refreshBackPack(0,-1);
	}
	CCLOG("create equipMainUi cost time: %d", GameUtils::millisecondNow() - currentTimeCreateUI);
}


void HeadMenu::ButtonMountEvent( CCObject *pSender )
{
// 	CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
// 	UISceneTest * scenetest =UISceneTest::create();
// 	scenetest->ignoreAnchorPointForPosition(false);
// 	scenetest->setAnchorPoint(ccp(0.5f,0.5f));
// 	scenetest->setPosition(ccp(winSize.width/2,winSize.height/2));
// 	GameView::getInstance()->getMainUIScene()->addChild(scenetest,4000);

	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
}

void HeadMenu::ButtonMissionEvent( CCObject *pSender )
{
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	CCLayer *missionMainLayer = (CCLayer*)mainscene->getChildByTag(kTagMissionScene);
	if(missionMainLayer == NULL)
	{
		missionMainLayer = MissionScene::create();

		//CCSize s = CCDirector::sharedDirector()->getVisibleSize();

		GameView::getInstance()->getMainUIScene()->addChild(missionMainLayer,0,kTagMissionScene);

		missionMainLayer->ignoreAnchorPointForPosition(false);
		missionMainLayer->setAnchorPoint(ccp(0.5f, 0.5f));
		missionMainLayer->setPosition(ccp(winSize.width/2, winSize.height/2));
	}
	mainscene->remindMission();
}

void HeadMenu::ButtonFriendEvent( CCObject *pSender )
{
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	CCLayer *friendUILayer = (CCLayer*)mainscene->getChildByTag(kTagFriendUi);
	if(friendUILayer == NULL)
	{
		FriendUi * friendui=FriendUi::create();
		friendui->ignoreAnchorPointForPosition(false);
		friendui->setAnchorPoint(ccp(0.5f,0.5f));
		friendui->setPosition(ccp(winSize.width/2,winSize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(friendui,0,kTagFriendUi);

		FriendStruct friend1={0,20,0,0};
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);

		mainscene->remindGetFriendPhypower();
	}
}

void HeadMenu::ButtonFamilyEvent( CCObject *pSender )
{
	long long roleGuildId_ = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionid();
	if (roleGuildId_==0)
	{
		const char *strings = StringDataManager::getString("family_enter_gm");
		char * str=const_cast<char*>(strings);
		GameView::getInstance()->showAlertDialog(str);
	}else
	{
		MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		CCLayer *familyUILayer = (CCLayer*)mainscene->getChildByTag(kTagFamilyUI);
		if(familyUILayer == NULL)
		{
			FamilyUI * familyui =FamilyUI::create();
			familyui->ignoreAnchorPointForPosition(false);
			familyui->setAnchorPoint(ccp(0.5f,0.5f));
			familyui->setPosition(ccp(winSize.width/2,winSize.height/2));
			familyui->setTag(kTagFamilyUI);
			GameView::getInstance()->getMainUIScene()->addChild(familyui);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1500,NULL);
			
			mainscene->remindFamilyApply();
		}
	}
}

void HeadMenu::ButtonRankingsEvent( CCObject *pSender )
{
	// 初始化 UI WINDOW数据
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	RankUI* pTmpRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
	if (NULL == pTmpRankUI)
	{
		RankUI * pRankUI = RankUI::create();
		pRankUI->ignoreAnchorPointForPosition(false);
		pRankUI->setAnchorPoint(ccp(0.5f, 0.5f));
		pRankUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		pRankUI->setTag(kTagRankUI);

		GameView::getInstance()->getMainUIScene()->addChild(pRankUI);
	}
}

void HeadMenu::ButtonSkillEvent( CCObject *pSender )
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	CCLayer *skillScene = (CCLayer*)mainscene->getChildByTag(KTagSkillScene);
	if(skillScene == NULL)
	{
		//CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		skillScene = SkillScene::create();
		GameView::getInstance()->getMainUIScene()->addChild(skillScene,0,KTagSkillScene);
		skillScene->ignoreAnchorPointForPosition(false);
		skillScene->setAnchorPoint(ccp(0.5f, 0.5f));
		skillScene->setPosition(ccp(winSize.width/2, winSize.height/2));
	}
	mainscene->remindOfSkill();
}

void HeadMenu::ButtonGeneralsEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI))
		return;

	CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	if (!MainScene::GeneralsScene)
		return;

	if(MainScene::GeneralsScene->getParent() != NULL)
	{
		CCLOG("MainScene::GeneralsScene->getParent() != NULL");
		MainScene::GeneralsScene->removeFromParentAndCleanup(false);
	}
	//CCLOG("(GeneralsUI *)MainScene::GeneralsScene");
	GeneralsUI * generalsUI = (GeneralsUI *)MainScene::GeneralsScene;
	GameView::getInstance()->getMainUIScene()->addChild(generalsUI,0,kTagGeneralsUI);
	generalsUI->ignoreAnchorPointForPosition(false);
	generalsUI->setAnchorPoint(ccp(0.5f, 0.5f));
	generalsUI->setPosition(ccp(s.width/2, s.height/2));

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
// 	if (mainScene->generalHeadLayer->getChildByTag(66))
// 	{
// 		GeneralsHeadManager * generalsHeadManager = (GeneralsHeadManager*)mainScene->generalHeadLayer->getChildByTag(66);
// 		generalsHeadManager->setGeneralHeadTouchEnable(false);
// 	}


	//招募
	for (int i = 0;i<3;++i)
	{
		if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50+i))
		{
			RecuriteActionItem * tempRecurite = ( RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50+i);
			tempRecurite->cdTime -= GameUtils::millisecondNow() - tempRecurite->lastChangeTime;
		}
	}

	generalsUI->setToDefaultTab();
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
	{
		sc->endCommand(pSender);
		//generalsUI->setToTabByIndex(1);
		//GeneralsUI::generalsListUI->isInFirstGeneralTutorial = true;
		if (GeneralsUI::generalsListUI->isInFirstGeneralTutorial )
		{
			generalsUI->setToTabByIndex(1);
		}
	}

	//武将列表 
	GeneralsUI::generalsListUI->isReqNewly = true;
	GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
	temp->page = 0;
	temp->pageSize = generalsUI->generalsListUI->everyPageNum;
	temp->type = 1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
	delete temp;
	//技能列表
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5055, this);
	//阵法列表
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5071, this);
	//已上阵武将列表 
	GeneralsUI::ReqListParameter * temp1 = new GeneralsUI::ReqListParameter();
	temp1->page = 0;
	temp1->pageSize = generalsUI->generalsListUI->everyPageNum;
	temp1->type = 5;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp1);
	delete temp1;

	mainScene->remindOfGeneral();
}

void HeadMenu::ButtonMailEvent( CCObject *pSender )
{
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	CCLayer *mailUILayer = (CCLayer*)mainscene->getChildByTag(kTagMailUi);
	if(mailUILayer == NULL)
	{
		MailUI * mailui=MailUI::create();
		mailui->ignoreAnchorPointForPosition(false);
		mailui->setAnchorPoint(ccp(0.5f,0.5f));
		mailui->setPosition(ccp(winSize.width/2,winSize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(mailui,0,kTagMailUi);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void *)mailui->curMailPage);
	}
	mainscene->remindMail();
}

void HeadMenu::ButtonHangUpEvent(CCObject *pSender)
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);
	MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	CCAssert(mainUILayer != NULL, "should not be nil");

	CCLayer *robotUILayer = (CCLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
	if(robotUILayer == NULL)
	{
		RobotMainUI * _ui = RobotMainUI::create();
		_ui->ignoreAnchorPointForPosition(false);
		_ui->setAnchorPoint(ccp(0.5f,0.5f));
		_ui->setPosition(ccp(winSize.width/2,winSize.height/2));
		_ui->setTag(kTagRobotUI);
		mainUILayer->addChild(_ui);
	}
}

void HeadMenu::ButtonSetEvent( CCObject *pSender )
{
	CCLayer *setUILayer = (CCLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagSetUI);
	if(setUILayer == NULL)
	{
		SetUI * setui=SetUI::create();
		setui->ignoreAnchorPointForPosition(false);
		setui->setAnchorPoint(ccp(0.5f,0.5f));
		setui->setPosition(ccp(winSize.width/2,winSize.height/2));
		setui->setTag(kTagSetUI);
		GameView::getInstance()->getMainUIScene()->addChild(setui);
	}
}

void HeadMenu::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void HeadMenu::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(ccp(pos.x+60,pos.y+80));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,55,55,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x,pos.y+12));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void HeadMenu::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void HeadMenu::addCCTutorialIndicatorRD( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(ccp(pos.x-40,pos.y+80));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,55,55,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x,pos.y+12));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}



void HeadMenu::addNewFunction( CCNode * pNode,void * functionOpenLevel )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	int lastIndex = -5;
	for (int i = 0;i<opendedFunctionVector.size();i++)
	{
		if (opendedFunctionVector.at(i)->get_positionIndex() > ((CFunctionOpenLevel * )functionOpenLevel)->get_positionIndex())
		{
			if (lastIndex == -5)
				lastIndex = i-1;

			if (m_pUiLayer->getWidgetByName(opendedFunctionVector.at(i)->get_functionId().c_str()))
			{
				CCFiniteTimeAction*  action = CCSequence::create(
					CCMoveBy::create(0.5f,ccp(FUNCTIONBUTTON_SPACE*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH),0)),
					NULL);
				(m_pUiLayer->getWidgetByName(opendedFunctionVector.at(i)->get_functionId().c_str()))->runAction(action);
			}
		}
	}

	if(lastIndex == -5)
	{
		if (m_pUiLayer->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))
		{
			(m_pUiLayer->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setVisible(true);
			(m_pUiLayer->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setPosition(ccp(75+FUNCTIONBUTTON_SPACE*(opendedFunctionVector.size()*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH)),28));
		}
	}
	else
	{
		if (m_pUiLayer->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))
		{
			(m_pUiLayer->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setVisible(true);
			(m_pUiLayer->getWidgetByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setPosition(ccp(75+FUNCTIONBUTTON_SPACE*(lastIndex + 1)*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH),28));
		}
	}


	CFunctionOpenLevel * temp = new CFunctionOpenLevel();
	temp->set_functionId(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId());
	temp->set_functionName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionName());
	temp->set_functionType(((CFunctionOpenLevel * )functionOpenLevel)->get_functionType());
	temp->set_requiredLevel(((CFunctionOpenLevel * )functionOpenLevel)->get_requiredLevel());
	temp->set_positionIndex(((CFunctionOpenLevel * )functionOpenLevel)->get_positionIndex());
	opendedFunctionVector.push_back(temp);
	sort(opendedFunctionVector.begin(),opendedFunctionVector.end(),SortByPositionIndex);
}


