

#ifndef _GAMESCENESTATE_BASEBUFF_H_
#define _GAMESCENESTATE_BASEBUFF_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class BaseBuff :
	public UIWidget
{
public:
	BaseBuff();
	~BaseBuff();
	static BaseBuff* create();
	bool init();

	void BuffInfo(CCObject * pSender);
};
#endif

