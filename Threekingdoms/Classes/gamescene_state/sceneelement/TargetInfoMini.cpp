#include "TargetInfoMini.h"
#include "../role/BaseFighter.h"
#include "../role/Monster.h"
#include "../../messageclient/element/CMonsterBaseInfo.h"
#include "../../newcomerstory/NewCommerStoryManager.h"

#define SPEED_ACTION_BLOOD 70.0f

TargetInfoMini::TargetInfoMini()
	:m_preScale(0.0f)
	,m_nowScale(0.0f)
	,m_timeDelay(0.0f)
{
}

TargetInfoMini::~TargetInfoMini()
{
}

TargetInfoMini* TargetInfoMini::create(BaseFighter * baseFighter)
{
	TargetInfoMini * targetInfo = new TargetInfoMini();
	if (targetInfo && targetInfo->init(baseFighter))
	{
		targetInfo->autorelease();
		return targetInfo;
	}
	CC_SAFE_DELETE(targetInfo);
	return NULL;
}

bool TargetInfoMini::init(BaseFighter * baseFighter)
{
	if (UIScene::init())
	{
		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

		baseFighterId = baseFighter->getRoleId();

		// red part
		ImageView_targetBlood = UIImageView::create();
		ImageView_targetBlood->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/red_xuetiao_2.png");
		ImageView_targetBlood->setAnchorPoint(ccp(0,0));
		ImageView_targetBlood->setPosition(ccp(0,0));
		float blood_scale = baseFighter->getActiveRole()->hp() * 1.0f / baseFighter->getActiveRole()->maxhp();
		if (blood_scale > 1.0f)
			blood_scale = 1.0f;
		ImageView_targetBlood->setTextureRect(CCRectMake(0,0,ImageView_targetBlood->getContentSize().width*blood_scale,ImageView_targetBlood->getContentSize().height));

		// red_gray part
		ImageView_targetBloodGray = UIImageView::create();
		ImageView_targetBloodGray->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/red_xuetiao_3.png");
		ImageView_targetBloodGray->setAnchorPoint(ccp(0,0));
		ImageView_targetBloodGray->setPosition(ccp(0,0));
		ImageView_targetBloodGray->setTextureRect(CCRectMake(0,0,ImageView_targetBloodGray->getContentSize().width*blood_scale,ImageView_targetBloodGray->getContentSize().height));
		
		// background part
		UIImageView *ImageView_blood_frame = UIImageView::create();
		ImageView_blood_frame->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/xuetiao_di.png");
		ImageView_blood_frame->setScale9Enable(true);
		ImageView_blood_frame->setCapInsets(CCRectMake(7,7,1,1));
		int offset = 1;
		int w = ImageView_targetBlood->getContentSize().width + offset*2;
		int h = ImageView_targetBlood->getContentSize().height + offset*2;
		ImageView_blood_frame->setScale9Size(CCSizeMake(w,h));
		ImageView_blood_frame->setAnchorPoint(ccp(0,0));
		ImageView_blood_frame->setPosition(ccp(-offset,-offset));

		m_pUiLayer->addWidget(ImageView_blood_frame);
		m_pUiLayer->addWidget(ImageView_targetBloodGray);
		m_pUiLayer->addWidget(ImageView_targetBlood);

		this->ignoreAnchorPointForPosition(false);
		this->setAnchorPoint(ccp(0,0));
		this->setTouchEnabled(false);
		this->setContentSize(CCSizeMake(w, h));

		this->setScaleX(0.5f);
		this->setScaleY(0.6f);
		Monster* pBoss = dynamic_cast<Monster*>(baseFighter);
		if(pBoss != NULL)
		{
			if(pBoss->getClazz() == CMonsterBaseInfo::clazz_world_boss)
			{
				this->setScaleX(1.4f);
				this->setScaleY(1.4f);
			}
			else if(pBoss->getClazz() == CMonsterBaseInfo::clazz_boss)
			{
				this->setScaleX(1.2f);
				this->setScaleY(1.2f);
			}
			else if(pBoss->getClazz() == CMonsterBaseInfo::clazz_elite)
			{
				this->setScaleX(1.0f);
				this->setScaleY(1.0f);
			}
		}

		// patch
		if( NewCommerStoryManager::getInstance()->IsNewComer())
		{
			//// Patch: boss - zhang jiao
			//if(baseFighter->getActiveRole()->templateid() != 499)
			//	this->setVisible(false);

			this->setVisible(false);
		}

		this->scheduleUpdate();

		return true;
	}
	return false;
}

void TargetInfoMini::onEnter()
{
	UIScene::onEnter();
}

void TargetInfoMini::onExit()
{
	UIScene::onExit();
}

bool TargetInfoMini::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return false;
}

void TargetInfoMini::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void TargetInfoMini::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void TargetInfoMini::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

void TargetInfoMini::ReloadTargetData( BaseFighter * baseFighter, bool bInitTargetInfo)
{
	int nHp = baseFighter->getActiveRole()->hp();
	int nMaxHp = baseFighter->getActiveRole()->maxhp();
	int nPreHp = baseFighter->getActiveRole()->lasthp();

	float blood_scale = nHp * 1.0f / nMaxHp;
	float blood_preScale = nPreHp * 1.0f / nMaxHp;

	if (blood_scale > 1.0f)
	{
		blood_scale = 1.0f;
	}

	if (blood_preScale > 1.0f)
	{
		blood_preScale = 1.0f;
	}

	if (0.0f == blood_preScale)
	{
		m_preScale = 1.0f;
		m_nowScale = blood_scale;
		m_timeDelay = 0.0f;

		ImageView_targetBlood->setTextureRect(CCRectMake(0,0,
			ImageView_targetBlood->getContentSize().width * blood_scale,
			ImageView_targetBlood->getContentSize().height));

	}
	else if (blood_preScale == blood_scale)
	{
		m_preScale = blood_preScale;
		m_nowScale = blood_scale;
		m_timeDelay = 0.0f;

		ImageView_targetBlood->setTextureRect(CCRectMake(0,0,
			ImageView_targetBlood->getContentSize().width * blood_scale,
			ImageView_targetBlood->getContentSize().height));
	}
	else
	{
		if (bInitTargetInfo)
		{
			m_preScale = blood_preScale;
			m_nowScale = blood_scale;
			m_timeDelay = 0.0f;

			ImageView_targetBlood->setTextureRect(CCRectMake(0,0,
				ImageView_targetBlood->getContentSize().width * blood_scale,
				ImageView_targetBlood->getContentSize().height));
		}
		else
		{
			if (m_preScale == blood_preScale && m_nowScale == blood_scale)
			{

			}
			else
			{
				m_preScale = blood_preScale;
				m_nowScale = blood_scale;
				m_timeDelay += (blood_preScale - blood_scale) * 100.0f / SPEED_ACTION_BLOOD;
			}

			ImageView_targetBlood->setTextureRect(CCRectMake(0,0,
				ImageView_targetBlood->getContentSize().width * blood_scale,
				ImageView_targetBlood->getContentSize().height));
		}
	}
		

	//ImageView_targetBlood->setTextureRect(CCRectMake(0,0,ImageView_targetBlood->getContentSize().width*blood_scale,ImageView_targetBlood->getContentSize().height));
}

void TargetInfoMini::update( float delta )
{
	if (0.0f >= m_timeDelay)
	{
		ImageView_targetBloodGray->setTextureRect(CCRectMake(0,0,
			ImageView_targetBloodGray->getContentSize().width * m_nowScale,
			ImageView_targetBloodGray->getContentSize().height));
	}
	else
	{
		ImageView_targetBloodGray->setTextureRect(CCRectMake(0,0,
			ImageView_targetBloodGray->getContentSize().width * (m_nowScale + m_timeDelay * SPEED_ACTION_BLOOD * 0.01f),
			ImageView_targetBloodGray->getContentSize().height));

		m_timeDelay -= delta;
	}
}
