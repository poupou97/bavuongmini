#include "MainScene.h"
#include "GameSceneState.h"
#include "GameView.h"
#include "../gamescene_state/sceneelement/ChatWindows.h"
#include "role/GameActorAnimation.h"
#include "../ui/Mail_ui/MailUI.h"
#include "sceneelement/SceneTest.h"
#include "../messageclient/GameMessageProcessor.h"
#include "../ui/Friend_ui/FriendUi.h"
#include "../ui/mapscene/MapScene.h"
#include "../ui/backpackscene/PackageScene.h"
#include <stdlib.h>
#include "../ui/Auction_ui/AuctionUi.h"
#include "sceneelement/shortcutelement/ShortcutLayer.h"
#include "sceneelement/GuideMap.h"
#include "role/BaseFighter.h"
#include "sceneelement/MyHeadInfo.h"
#include "sceneelement/TargetInfo.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "role/MyPlayerOwnedStates.h"
#include "sceneelement/MissionAndTeam.h"
#include "sceneelement/HeadMenu.h"
#include "sceneelement/MyBuff.h"
#include "../ui/Chat_ui/ChatUI.h"
#include "../ui/generals_ui/GeneralsUI.h"
#include "../ui/extensions/MarqueeControl.h"
#include "../legend_script/ScriptManager.h"
#include "../legend_script/CCTutorialIndicator.h"
#include "../messageclient/element/CMapInfo.h"
#include "../messageclient/element/CFunctionOpenLevel.h"
#include "../utils/StaticDataManager.h"
#include "role/General.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "particle_nodes/CCParticleSystem.h"
#include "SimpleEffectManager.h"
#include "../legend_script/CCTutorialParticle.h"
#include "role/MyPlayerAIConfig.h"
#include "role/MyPlayerAI.h"
#include "../messageclient/element/CRelationPlayer.h"
#include "../ui/skillscene/SkillScene.h"
#include "../ui/missionscene/MissionScene.h"
#include "../messageclient/element/MissionInfo.h"
#include "../ui/missionscene/MissionManager.h"
#include "../messageclient/protobuf/MissionMessage.pb.h"
#include "../messageclient/element/CMailInfo.h"
#include "../messageclient/protobuf/MailMessage.pb.h"
#include "../AppMacros.h"
#include "../ui/generals_ui/RecuriteActionItem.h"
#include "../ui/generals_ui/GeneralsRecuriteUI.h"
#include "../ui/OnlineReward_ui/OnlineGiftData.h"
#include "../ui/OnlineReward_ui/OnlineGiftIcon.h"
#include "../ui/generals_ui/GeneralsListUI.h"
#include "../messageclient/element/CGeneralBaseMsg.h"
#include "../messageclient/element/CFunctionOpenLevel.h"
#include "../ui/SignDaily_ui/SignDailyIcon.h"
#include "../ui/SignDaily_ui/SignDailyUI.h"
#include "../ui/SignDaily_ui/SignDailyData.h"
#include "../utils/GameUtils.h"
#include "../messageclient/element/MapMarquee.h"
#include "../ui/Active_ui/ActiveIcon.h"
#include "../ui/OnlineReward_ui/OnlineGiftIconWidget.h"
#include "../ui/family_ui/FamilyUI.h"
#include "../ui/family_ui/FamilyOperatorOfLeader.h"
#include "../messageclient/protobuf/RecruitMessage.pb.h"
#include "../messageclient/element/CActionDetail.h"
#include "../ui/storehouse_ui/StoreHouseUI.h"
#include "../ui/registerNotice/RegisterNoticeui.h"
#include "../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../ui/Active_ui/ActiveUI.h"
#include "../ui/Active_ui/ActiveShopData.h"
#include "../messageclient/element/COneVipGift.h"
#include "../ui/vip_ui/VipEveryDayRewardsUI.h"
#include "../messageclient/element/CEquipment.h"
#include "../messageclient/element/CGeneralDetail.h"
#include "../messageclient/element/COfflineExpPuf.h"
#include "../ui/offlineExp_ui/OffLineExpUI.h"
#include "../ui/vip_ui/VipData.h"
#include "../ui/vip_ui/VipDetailUI.h"
#include "../utils/GameConfig.h"
#include "../ui/Chat_ui/PrivateChatUi.h"
#include "../ui/offlinearena_ui/OffLineArenaData.h"
#include "../ui/challengeRound/RankRewardUi.h"
#include "../ui/family_ui/familyFight_ui/FamilyFightUI.h"
#include "../ui/family_ui/familyFight_ui/FamilyFightResultUI.h"
#include "../messageclient/protobuf/GuildMessage.pb.h"
#include "GameStatistics.h"
#include "../ui/GameUIConstant.h"
#include "../ui/backpackscene/BattleAchievementUI.h"
#include "../ui/FivePersonInstance/InstanceMapUI.h"
#include "../messageclient/element/CRewardBase.h"
#include "../ui/Reward_ui/RewardUi.h"
#include "../ui/OnlineReward_ui/OnlineGiftLayer.h"
#include "../ui/FivePersonInstance/InstanceDetailUI.h"
#include "../newcomerstory/NewCommerStoryManager.h"
#include "sceneelement/GeneralsInfoUI.h"
#include "sceneelement/TriangleButton.h"
#include "../ui/FivePersonInstance/FivePersonInstance.h"
#include "../ui/searchgeneral/SearchGeneral.h"
#include "role/WeaponEffect.h"
#include "role/ActorUtils.h"
#include "../legend_script/CCTeachingGuide.h"
#include "../ui/moneyTree_ui/MoneyTreeTimeManager.h"
#include "../ui/moneyTree_ui/MoneyTreeUI.h"
#include "../ui/moneyTree_ui/MoneyTreeData.h"
#include "../messageclient/element/CBaseSkill.h"
#include "skill/GameFightSkill.h"
#include "GameSceneEffects.h"
#include "MainAnimationScene.h"
#include "ui/signMonthly_ui/SignMonthlyUI.h"

#define GENERALHEADMANAGERTAG 66
#define TAG_GENERALSINFOUI 77
#define TAG_TRIANGLEBUTTON 78
#define TAG_ACTION_GUIDEMAPACTIONLAYER 79

#define BUTTONLAYER_TAG 100
#define CHATBUTTON_TAG 101

GeneralsUI* MainScene::GeneralsScene = NULL;

std::string animActionNameHorseList[] =
{
	"stand_redrabbit",
	"run_redrabbit",
	"attack1_redrabbit",
};

std::string animActionNamePlayList[] =
{
	"stand",
	"run",
	"attack1",
	"whirl",
	"death",
}; 

MainScene::MainScene():
isHeadMenuOn(false),
isMapOpen(true),
isTestSceneOn(false),
backageLayer(NULL),
automaticSkill(false),
isLevelUped(false),
isRemindOpened(false),
generalHeadLayer(NULL),
curMyHeadInfo(NULL),
targetInfoLayer(NULL),
btnreminRepair(NULL),
m_offAreaPrizeTime(9999),
m_addElememtLyaerTag(0)
{
	sysInfo = " add first message ";
	indexMarqee=0;
}


MainScene::~MainScene()
{
}

MainScene * MainScene::create()
{
	MainScene * mainScene = new MainScene();
	if (mainScene && mainScene->init())
	{
		mainScene->autorelease();
		return mainScene;
	}
	CC_SAFE_DELETE(mainScene);
	return NULL;
}

bool MainScene::init()
{
	if (UIScene::init())
	{
		winSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
		//用于添加主界面元素
		CCLayer * layer_mainUIElement = CCLayer::create();
		addChild(layer_mainUIElement,0,kTag_MainUI_Element);
		
		initVirtualJoystick();

		guideMap = GuideMap::create();
		guideMap->setAnchorPoint(ccp(0,0));
		guideMap->setPosition(ccp(0,0));
		guideMap->setTag(kTagGuideMapUI);
		addChild(guideMap,-1);

		UILayer * mapOpenLayer = UILayer::create();
		addChild(mapOpenLayer);
		mapCloseLayer = UILayer::create();
		addChild(mapCloseLayer);
		Button_mapOpen = UIButton::create();
		Button_mapOpen->setTextures("gamescene_state/zhujiemian3/renwuduiwu/button2_off.png","gamescene_state/zhujiemian3/renwuduiwu/button2_off.png","");
		Button_mapOpen->setRotation(0);
		Button_mapOpen->setTouchEnable(true);
		Button_mapOpen->setAnchorPoint(ccp(0.5f,0.5f));
		Button_mapOpen->setPosition(ccp(winSize.width-Button_mapOpen->getContentSize().width/2+origin.x,winSize.height-47-Button_mapOpen->getContentSize().height/2+origin.y));
		Button_mapOpen->addReleaseEvent(this,coco_releaseselector(MainScene::ButtonMapOpenEvent));
		Button_mapOpen->setPressedActionEnabled(true);
		mapOpenLayer->addWidget(Button_mapOpen);
		Button_mapOpen->setVisible(false);

		UIImageView * imageView_flag = UIImageView::create();
		imageView_flag->setTexture("gamescene_state/zhujiemian3/renwuduiwu/arrow.png");
		imageView_flag->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_flag->setPosition(ccp(0,0));
		imageView_flag->setName("jiantou");
		Button_mapOpen->addChild(imageView_flag);


		if (GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::coliseum
			||GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::copy
			||GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::guildFight
			||GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::tower)
		{
			Button_mapOpen->setVisible(false);
		}
		else
		{
			Button_mapOpen->setVisible(true);
			//小地图
			if (isMapOpen)
			{
				//Button_mapOpen->setRotation(0);
				Button_mapOpen->getChildByName("jiantou")->setRotation(0);
// 				if (guideMap->getActionLayer())
// 				{
// 					guideMap->getActionLayer()->setVisible(true);
// 				}
			}
			else
			{
				//Button_mapOpen->setRotation(180);
				Button_mapOpen->getChildByName("jiantou")->setRotation(180);
// 				if (guideMap->getActionLayer())
// 				{
// 					guideMap->getActionLayer()->setVisible(false);
// 				}
			}
		}
		//chatwindows  chatButton privateButton repairButon all add to layer
		chatLayer = UILayer::create();
		addChild(chatLayer);
		/////chatwindows
		ChatWindows * chatwindow=ChatWindows::create();
		chatwindow->ignoreAnchorPointForPosition(false);
		chatwindow->setAnchorPoint(ccp(0.5f,0.5f));
		//chatwindow->setPosition(ccp(winSize.width/2,chatwindow->getContentSize().height/2+83));
		chatwindow->setPosition(ccp(chatwindow->getContentSize().width/2+2,chatwindow->getContentSize().height/2+10));
		chatLayer->addChild(chatwindow,0,kTagChatWindows);

		UILayer * chatButton_layer = UILayer::create();
		chatButton_layer->setTag(BUTTONLAYER_TAG);
		chatLayer->addChild(chatButton_layer,1);
		//chatui button
		UIButton * btn_chatWindows = UIButton::create();
		btn_chatWindows->setTouchEnable(true);
		btn_chatWindows->setTextures("gamescene_state/zhujiemian3/liaotiankuang/icon.png","gamescene_state/zhujiemian3/liaotiankuang/icon.png","");
		btn_chatWindows->setAnchorPoint(ccp(0.5f,0.5f));
		btn_chatWindows->setPosition(ccp(chatwindow->getPositionX() - chatwindow->getContentSize().width/2 + btn_chatWindows->getContentSize().width/2,
			chatwindow->getPositionY()));
		btn_chatWindows->addReleaseEvent(this,coco_cancelselector(MainScene::showChatWindows));
		btn_chatWindows->setPressedActionEnabled(true);
		btn_chatWindows->setZOrder(1);
		btn_chatWindows->setTag(CHATBUTTON_TAG);
		chatButton_layer->addWidget(btn_chatWindows);


		//other button: 1.repair button  2.remind button
		UILayer * allButtonlayer = UILayer::create();
		addChild(allButtonlayer);

		btnRemin = UIButton::create();
		btnRemin->setTouchEnable(true);
		btnRemin->setPressedActionEnabled(true);
		btnRemin->setAnchorPoint(ccp(0.5f,0.5f));
		btnRemin->setTextures("res_ui/chatting.png","res_ui/chatting.png","");
		//btnRemin->setPosition(ccp(chatwindow->getPositionX()+ chatwindow->getContentSize().width/2,btn_chatWindows->getPosition().y+btn_chatWindows->getContentSize().height/2+btnRemin->getContentSize().height/2 ));
		//btnRemin->setPosition(ccp(winSize.width/2,chatwindow->getPosition().y+chatwindow->getContentSize().height/2+btnRemin->getContentSize().height/2 ));
		btnRemin->setPosition(ccp(winSize.width/2,130));//120 = generalHeadLayer.height + btnRemin->getContentSize().height/2;
		btnRemin->addReleaseEvent(this,coco_releaseselector(ChatWindows::chatPrivate));
		allButtonlayer->addWidget(btnRemin);

		btnReminOfNewMessageParticle = CCTutorialParticle::create("tuowei0.plist",32,48);
		btnReminOfNewMessageParticle->setPosition(ccp(btnRemin->getPosition().x,btnRemin->getPosition().y - btnRemin->getContentSize().height/2));
		btnReminOfNewMessageParticle->setTag(CCTUTORIALPARTICLETAG);
		allButtonlayer->addChild(btnReminOfNewMessageParticle);
		
		if (GameView::getInstance()->hasPrivatePlaer == false)
		{
			if (GameView::getInstance()->isShowPrivateChatBtn == true)
			{
				btnRemin->setVisible(true);
			}else
			{
				btnRemin->setVisible(false);
			}
			btnReminOfNewMessageParticle->setVisible(false);
		}else
		{
			btnRemin->setVisible(true);
			btnReminOfNewMessageParticle->setVisible(true);
		}
		
		btnreminRepair = UIButton::create();
		btnreminRepair->setTouchEnable(true);
		btnreminRepair->setPressedActionEnabled(true);
		btnreminRepair->setAnchorPoint(ccp(0.5f,0.5f));
		btnreminRepair->setTextures("res_ui/damage.png","res_ui/damage.png","");
		btnreminRepair->setPosition(ccp(btnRemin->getPosition().x +btnRemin->getContentSize().width/2 + btnreminRepair->getContentSize().width/2+10,btnRemin->getPosition().y));
		btnreminRepair->addReleaseEvent(this,coco_releaseselector(MainScene::showRepairEquipOfBackPack));
		allButtonlayer->addWidget(btnreminRepair);
		btnreminRepair->setVisible(false);

		// message window
		// show the Instant message, for example, the experience when kill one monster
		//CCNode* messageWindow = CCNode::create();
		//messageWindow->setAnchorPoint(ccp(0,0));
		//messageWindow->setPosition(ccp(450, 80));
		//addChild(messageWindow,0,kTagMessageWindows);

		//exp
		ExpLayer = UILayer::create();
		ExpLayer->setAnchorPoint(ccp(0,0));
		ExpLayer->setPosition(ccp(0,0));
		ExpLayer->setContentSize(winSize);
		addChild(ExpLayer);

		UIImageView * ImageView_expLayer  = UIImageView::create();
		ImageView_expLayer->setName("ImageView_exp");
		ImageView_expLayer->setTexture("res_ui/zhezhao80.png");
		ImageView_expLayer->setScale9Enable(true);
		ImageView_expLayer->setScale9Size(CCSizeMake(winSize.width-32,9));
		ImageView_expLayer->setAnchorPoint(ccp(0,0));
		ImageView_expLayer->setPosition(ccp(0,0));
		ExpLayer->addWidget(ImageView_expLayer);
		
		UIImageView * expIcon = UIImageView::create();
		expIcon->setTexture("gamescene_state/zhujiemian3/jingyantiao/exp.png");
		expIcon->setAnchorPoint(ccp(0,0));
		expIcon->setPosition(ccp(0,0));
		ExpLayer->addWidget(expIcon);

		UIImageView * expFrame = UIImageView::create();
		expFrame->setTexture("gamescene_state/zhujiemian3/jingyantiao/kuang.png");
		expFrame->setScale9Enable(true);
		expFrame->setCapInsets(CCRectMake(10,5,1,1));
		expFrame->setScale9Size(CCSizeMake(winSize.width-expIcon->getContentSize().width-3-30+origin.x,expFrame->getContentSize().height+origin.y));
		expFrame->setAnchorPoint(ccp(0,0));
		expFrame->setPosition(ccp(expIcon->getContentSize().width+2+origin.x,0));
		ExpLayer->addWidget(expFrame);

		ImageView_exp = UIImageView::create();
		ImageView_exp->setTexture("gamescene_state/zhujiemian3/jingyantiao/jindu.png");
		ImageView_exp->setScale9Enable(true);
		ImageView_exp->setCapInsets(CCRectMake(7,4,1,1));
		ImageView_exp->setScale9Size(CCSizeMake(winSize.width-expIcon->getContentSize().width-6-30,7));
		ImageView_exp->setAnchorPoint(ccp(0,0.5f));
		ImageView_exp->setPosition(ccp(2,expFrame->getContentSize().height/2));
		expFrame->addChild(ImageView_exp);
		int curexp = GameView::getInstance()->myplayer->player->experience();
		int nextexp = GameView::getInstance()->myplayer->player->nextlevelexperience();
		float scaleValue = (curexp*1.0f)/(nextexp*1.0f);
		ImageView_exp->setScaleX(scaleValue);

		for (int i = 1;i<10;++i)
		{
			UIImageView * ImageView_expbglan = UIImageView::create();
			ImageView_expbglan->setTexture("gamescene_state/zhujiemian3/jingyantiao/n.png");
			ImageView_expbglan->setAnchorPoint(ccp(0,0));
			ImageView_expbglan->setPosition(ccp(expIcon->getContentSize().width + (winSize.width-expIcon->getContentSize().width-30)/10*i,1));
			ExpLayer->addWidget(ImageView_expbglan);
		}

		//initShortcutLayer
		ShortcutLayer * shortcutLayer = ShortcutLayer::create();
		shortcutLayer->ignoreAnchorPointForPosition(false);
		shortcutLayer->setAnchorPoint(ccp(0.5f,0.5f)); 
		shortcutLayer->setPosition(ccp(winSize.width/2,winSize.height/2));
		shortcutLayer->setTag(kTagShortcutLayer);
		addChild(shortcutLayer);
		
		headMenu = HeadMenu::create();
		headMenu->ignoreAnchorPointForPosition(false);
		headMenu->setAnchorPoint(ccp(0,1));
		headMenu->setPosition(ccp(0,0));
		headMenu->setTag(kTagHeadMenu);
		addChild(headMenu);

		int roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
		for (int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();i++)
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() == 2)       //地图区域（2号区域）
			{
				if (strcmp(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str(),"btn_remind") == 0)  //提升
				{
					if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel()<= roleLevel)     //可以显示
					{
						setRemindOpened(true);
					}
				}
			}

			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() == 3) 
			{
				if (strcmp(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str(),"btn_offLineArena") == 0) 
				{
					if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel()<= roleLevel) 
					{
						this->schedule(schedule_selector(MainScene::updateOffAreaPrizeTime),1.0f);
					}
				}
			}

		}

		generalHeadLayer = UILayer::create();
		generalHeadLayer->setAnchorPoint(ccp(0,0));
		generalHeadLayer->setPosition(ccp(0,0));
		generalHeadLayer->setContentSize(winSize);
		addChild(generalHeadLayer);

		//remind check 
		if (GameView::getInstance()->getIsCheckRemind() == true)
		{
			this->checkIsNewRemind();
			GameView::getInstance()->setIsCheckRemind(false);
		}else
		{
			remindOfSkill();
			remindOfGeneral();
			remindOffArena();
			remindOfSignDaily();
			remindReward();
			remindOffLineEx();
			remindMission();
			remindMail();
			remindFamilyApply();
		}

		//任务/组队
		float missionAndTeam_pos_y = (UI_DESIGN_RESOLUTION_HEIGHT-77)*(winSize.height*1.0f/UI_DESIGN_RESOLUTION_HEIGHT);
		MissionAndTeam * missionAndTeam = MissionAndTeam::create();
		missionAndTeam->ignoreAnchorPointForPosition(false);
		missionAndTeam->setAnchorPoint(ccp(0,1.0f));
		missionAndTeam->setPosition(ccp(0,missionAndTeam_pos_y+origin.y));
		//missionAndTeam->setPosition(ccp(0,560));
		missionAndTeam->setTag(kTagMissionAndTeam);
		this->addChild(missionAndTeam);
		//竞技场中不需要
		if (GameView::getInstance()->getMapInfo()->maptype() == coliseum)
		{
			missionAndTeam->setVisible(false);
		}

		//武将
		//change by yangjun 2014.9.26
		//initGeneral();
		//add by yangjun 2014.9.26
		initGeneralInfoUI();


		//头像
		initHead();

		//对方头像
		targetInfoLayer = UILayer::create();
		addChild(targetInfoLayer);

		//add by yangjun 2014.10.15
		TriangleButton * tb = TriangleButton::create();
		tb->ignoreAnchorPointForPosition(false);
		tb->setAnchorPoint(ccp(1.0f,0.f));
		tb->setPosition(ccp(winSize.width,0));
		tb->setTag(TAG_TRIANGLEBUTTON);
		addChild(tb);

		 /*
		// 签到UI（1.第一次登陆时；2.用户等级等于刚登陆时的等级;3.用户等级大于9级时;4.当天的奖励没有领；要自动弹出UI）
		if (1 == SignDailyData::instance()->getIsLogin() &&
			SignDailyData::instance()->getLoginPalyerLevel() == GameView::getInstance()->myplayer->getActiveRole()->level() &&
			GameView::getInstance()->myplayer->getActiveRole()->level() > 9 &&
			SignDailyData::instance()->isCanSign())
		{
			SignDailyData::instance()->setIsLogin(0);

			SignDailyUI * pSignDailyUI = SignDailyUI::create();
			pSignDailyUI->ignoreAnchorPointForPosition(false);
			pSignDailyUI->setAnchorPoint(ccp(0.5f, 0.5f));
			pSignDailyUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
			pSignDailyUI->setTag(kTagSignDailyUI);
			
			addChild(pSignDailyUI);

			pSignDailyUI->refreshUI();
		}
		*/

		// 添加监测 在线奖励 的layer
		OnlineGiftLayer * pOnlineGiftLayer = (OnlineGiftLayer *)this->getChildByTag(kTagOnLineGiftLayer);
		if (NULL == pOnlineGiftLayer)
		{
			OnlineGiftLayer* onLineGiftLayer = OnlineGiftLayer::create();
			onLineGiftLayer->initDataFromIntent();
			onLineGiftLayer->setTag(kTagOnLineGiftLayer);

			this->addChild(onLineGiftLayer);
		}

		// 添加监测 摇钱树 的定时器
		MoneyTreeTimeManager * pMoneyTreeTimeManager = (MoneyTreeTimeManager *)this->getChildByTag(kTagMoneyTreeManager);
		if (NULL == pMoneyTreeTimeManager)
		{
			MoneyTreeTimeManager* moneyTreeTimeManager = MoneyTreeTimeManager::create();
			moneyTreeTimeManager->initDataFromIntent();
			moneyTreeTimeManager->setTag(kTagMoneyTreeManager);

			this->addChild(moneyTreeTimeManager);
		}

		scheduleUpdate();
		this->schedule(schedule_selector(MainScene::updateRecuriteInfo),1.0f);

		//add registerNotice
		int registerSize = GameView::getInstance()->registerNoticeVector.size();
		if (GameView::getInstance()->registerNoticeIsShow == true && registerSize > 0)
		{
			std::string strings_ = GameView::getInstance()->registerNoticeVector.at(0);
			if (strlen(strings_.c_str()) >0)
			{
				GameView::getInstance()->registerNoticeIsShow = false;
				RegisterNoticeui * registerui = RegisterNoticeui::create();
				registerui->ignoreAnchorPointForPosition(false);
				registerui->setAnchorPoint(ccp(0.5f,0.5f));
				registerui->setPosition(ccp(winSize.width/2,winSize.height/2));
				addChild(registerui);
			}
		}

		if (!VipData::vipDetailUI)
		{
			VipData::vipDetailUI = VipDetailUI::create();
			VipData::vipDetailUI->retain();
		}

		//add by yangjun 2014.12.15
		//用于添加主界面动画层
		CCLayer * layer_mainUIAnm = CCLayer::create();
		layer_mainUIAnm->setAnchorPoint(ccp(0,0));
		layer_mainUIAnm->setPosition(ccp(0,0));
		layer_mainUIAnm->setContentSize(winSize);
		layer_mainUIAnm->setTag(kTag_MainUI_Animation);
		addChild(layer_mainUIAnm);

		//add by yangjun 2014.10.10
		//用于添加教学指引
		CCLayer * layer_mainUITuturial = CCLayer::create();
		layer_mainUITuturial->setAnchorPoint(ccp(0,0));
		layer_mainUITuturial->setPosition(ccp(0,0));
		layer_mainUITuturial->setContentSize(winSize);
		layer_mainUITuturial->setTag(kTag_MainUI_Tutorial);
		addChild(layer_mainUITuturial,ZOrder_Tutorial);
		
		return true;
	}
	return false;
}

#define  HeadMenuMovingTag 629

void MainScene::ButtonHeadEvent(CCObject *pSender)
{
	if (headMenu->getActionByTag(HeadMenuMovingTag))
		return;

	if (isHeadMenuOn == true)
	{
		CCMoveTo * moveDown_headmenu = CCMoveTo::create(0.3f,ccp(headMenu->getPositionX(),-13));
		moveDown_headmenu->setTag(HeadMenuMovingTag);
		headMenu->runAction(moveDown_headmenu);
		
		if (headMenu->getChildByTag(CCTUTORIALINDICATORTAG))
		{
			headMenu->getChildByTag(CCTUTORIALINDICATORTAG)->setVisible(false);
		}

		if (headMenu->getChildByTag(CCTUTORIALPARTICLETAG))
		{
			headMenu->getChildByTag(CCTUTORIALPARTICLETAG)->setVisible(false);
		}

		isHeadMenuOn = false;
		//open expLayer
		ExpLayer->setVisible(true);
		//shortcutLayer down
		CCLayer * shortcutLayer = (CCLayer*)this->getChildByTag(kTagShortcutLayer);
		if (shortcutLayer)
		{
// 			CCMoveTo * moveDown = CCMoveTo::create(0.3f,ccp(shortcutLayer->getPositionX(),shortcutLayer->getPositionY()-headMenu->getContentSize().height-11));
// 			shortcutLayer->runAction(moveDown);
			
			CCMoveTo * moveLeft = CCMoveTo::create(0.3f,ccp(shortcutLayer->getPositionX()-200,shortcutLayer->getPositionY()));
			shortcutLayer->runAction(moveLeft);
		}
		//chatlayer down
		CCMoveTo * chatlayer_moveDown = CCMoveTo::create(0.3f,ccp(chatLayer->getPositionX(),chatLayer->getPositionY()-headMenu->getContentSize().height-11));
		chatLayer->runAction(chatlayer_moveDown);

		//generalInfoUI down
		GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI*)this->getGeneralInfoUI();
		if (generalsInfoUI)
		{
// 			CCMoveTo * moveDown = CCMoveTo::create(0.3f,ccp(generalsInfoUI->getPositionX(),generalsInfoUI->getPositionY()-headMenu->getContentSize().height-11));
// 			generalsInfoUI->runAction(moveDown);
			
			CCMoveTo * moveUp = CCMoveTo::create(0.3f,ccp(generalsInfoUI->getPositionX(),0));
			generalsInfoUI->runAction(moveUp);
		}
		//右下角三角按钮
		TriangleButton * triangleBtn = (TriangleButton*)this->getChildByTag(TAG_TRIANGLEBUTTON);
		if (triangleBtn)
		{
			triangleBtn->setVisible(true);
		}
	}
	else 
	{
// 		headMenu = HeadMenu::create();
// 		headMenu->ignoreAnchorPointForPosition(false);
// 		headMenu->setAnchorPoint(ccp(0,1));
// 		headMenu->setPosition(ccp(0,0));
// 		addChild(headMenu);
		isHeadMenuOn = true;
		CCAction * action = CCMoveTo::create(0.3f,ccp(headMenu->getPositionX(),headMenu->getContentSize().height+11));
		action->setTag(HeadMenuMovingTag);
		headMenu->runAction(action);

		if (headMenu->getChildByTag(CCTUTORIALINDICATORTAG))
		{
			headMenu->getChildByTag(CCTUTORIALINDICATORTAG)->setVisible(true);
		}

		if (headMenu->getChildByTag(CCTUTORIALPARTICLETAG))
		{
			headMenu->getChildByTag(CCTUTORIALPARTICLETAG)->setVisible(true);
		}

		//close expLayer
		//ExpLayer->setVisible(false);
		//shortcutLayer up
		CCLayer * shortcutLayer = (CCLayer*)this->getChildByTag(kTagShortcutLayer);
		if (shortcutLayer)
		{
// 			CCMoveTo * moveUp = CCMoveTo::create(0.3f,ccp(shortcutLayer->getPositionX(),shortcutLayer->getPositionY()+headMenu->getContentSize().height+11));
// 			shortcutLayer->runAction(moveUp);
			
			CCMoveTo * moveRightw = CCMoveTo::create(0.3f,ccp(shortcutLayer->getPositionX()+200,shortcutLayer->getPositionY()));
			shortcutLayer->runAction(moveRightw);
		}
		//chatlayer up
		CCMoveTo * chatLayer_moveUp = CCMoveTo::create(0.3f,ccp(chatLayer->getPositionX(),chatLayer->getPositionY()+headMenu->getContentSize().height+11));
		chatLayer->runAction(chatLayer_moveUp);

		//generalInfoUI up
		GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI*)this->getGeneralInfoUI();
		if (generalsInfoUI)
		{
// 			CCMoveTo * moveDown = CCMoveTo::create(0.3f,ccp(generalsInfoUI->getPositionX(),generalsInfoUI->getPositionY()+headMenu->getContentSize().height+11));
// 			generalsInfoUI->runAction(moveDown);
			
			CCMoveTo * moveDown = CCMoveTo::create(0.3f,ccp(generalsInfoUI->getPositionX(),-105));
			generalsInfoUI->runAction(moveDown);
		}
		//右下角三角按钮
		TriangleButton * triangleBtn = (TriangleButton*)this->getChildByTag(TAG_TRIANGLEBUTTON);
		if (triangleBtn)
		{
			triangleBtn->setVisible(false);
		}
	}
}

void MainScene::ButtonGeneralEvent( CCObject *pSender )
{
}

void MainScene::ButtonChatEvent( CCObject *pSender )
{
// 	indexMarqee++;
// 	MapMarquee * marquee_ =MarqueeStringConfigData::s_MarqueeStrings[indexMarqee];
// 	std::string strings_ = marquee_->get_message();
// 	for (int i=0;i<2;i++)
// 	{
// 		std::string str_ = "{}";
// 		int pos_ = strings_.find(str_);
// 		if (pos_!= -1)
// 		{
// 			strings_.replace(pos_,str_.length(),"text");
// 		}
// 	}
// 	MainScene::marqueeStruct struct_ ={strings_.c_str(),1};
// 	marqueeString.push_back(struct_);
}

void MainScene::ButtonLVTeamEvent( CCObject *pSender )
{

}

void MainScene::ButtonMapOpenEvent(CCObject *pSender)
{
// 	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
// 	GetExpEffect * expEffect = GetExpEffect::create(500000,1100000,true);
// 	expEffect->ignoreAnchorPointForPosition(false);
// 	expEffect->setAnchorPoint(ccp(.5f,.5f));
// 	expEffect->setPosition(ccp(winSize.width/2,winSize.height/2));
// 	GameView::getInstance()->getMainAnimationScene()->addChild(expEffect);
	////////////////////////////////////////////////////////////////////////////////////////
// 	//从导出文件异步加载动画
// 	CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("animation/texiao/particledesigner/1230.png","animation/texiao/particledesigner/1230.plist","animation/texiao/particledesigner/123.ExportJson");
// 	//根据动画名称创建动画精灵
// 	CCArmature *armature = CCArmature::create("123");
// 	//播放指定动作
// 	//armature->getAnimation()->playByIndex(0);
// 	//修改属性
// 	armature->setScale(1.0f);
// 	//设置动画精灵位置
// 	armature->setPosition(385,240);
// 	//添加到当前页面
// 	this->addChild(armature);


	//开启等级
	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(23);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[23];
	}

	if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
	{
		char s_des [200];
		const char* str_des = StringDataManager::getString("shrinkage_will_be_open");
		sprintf(s_des,str_des,openlevel);
		GameView::getInstance()->showAlertDialog(s_des);
		return;
	}

	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}
	
	CCLog("ButtonMapOpenEvent");
	if (guideMap->getActionLayer())
	{
		if(guideMap->getActionLayer()->getActionByTag(TAG_ACTION_GUIDEMAPACTIONLAYER))
			return;

		if (isMapOpen)
		{
			//mapCloseLayer->setVisible(true);
			//关闭
			Button_mapOpen->getChildByName("jiantou")->setRotation(180);
			isMapOpen = false;

			//guideMap->getActionLayer()->setVisible(false);
			CCMoveTo * moveOut = CCMoveTo::create(0.3f,ccp(300,0));
			guideMap->getActionLayer()->runAction(moveOut);
			moveOut->setTag(TAG_ACTION_GUIDEMAPACTIONLAYER);
		}
		else
		{
			//mapCloseLayer->setVisible(true);
			Button_mapOpen->getChildByName("jiantou")->setRotation(0);
			isMapOpen = true;

			//guideMap->getActionLayer()->setVisible(true);
			CCMoveTo * moveIn = CCMoveTo::create(0.3f,ccp(0,0));
			guideMap->getActionLayer()->runAction(moveIn);
			moveIn->setTag(TAG_ACTION_GUIDEMAPACTIONLAYER);

			Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
				sc->endCommand(pSender);
		}
	}
}


void MainScene::initHead()
{
	//CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	curMyHeadInfo = MyHeadInfo::create();
	curMyHeadInfo->setPosition(ccp(0,winSize.height-curMyHeadInfo->getContentSize().height));
	curMyHeadInfo->setTag(kTagMyHeadInfo);
	addChild(curMyHeadInfo);

// 	//buff
// 	upBuff = UIButton::create();
// 	upBuff->setTouchEnable(true);
// 	upBuff->setTextures("gamescene_state/zhujuetouxiang/jiahao.png","gamescene_state/zhujuetouxiang/jiahao.png","");
// 	upBuff->setAnchorPoint(CCPointZero);
// 	upBuff->setPosition(ccp(ImageView_blood->getPosition().x+20,ImageView_blood->getPosition().y-33-upBuff->getContentSize().height));
// 	upBuff->addReleaseEvent(this,coco_releaseselector(MainScene::BuffEvent));
// 	ImageView_background->addChild(upBuff);
// 
// 	downBuff = UIButton::create();
// 	downBuff->setTouchEnable(true);
// 	downBuff->setTextures("gamescene_state/zhujuetouxiang/jianhao.png","gamescene_state/zhujuetouxiang/jianhao.png","");
// 	downBuff->setAnchorPoint(CCPointZero);
// 	downBuff->setPosition(ccp(upBuff->getPosition().x+downBuff->getContentSize().width + 20,upBuff->getPosition().y));
// 	downBuff->addReleaseEvent(this,coco_releaseselector(MainScene::BuffEvent));
// 	ImageView_background->addChild(downBuff);
}

void MainScene::createTargetInfo(BaseFighter * baseFighter)
{
	//CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

	// remove the old one
// 	if (targetInfoLayer->getChildByTag(kTagTatgetInfo))
// 	{
// 		TargetInfo * target = (TargetInfo *)targetInfoLayer->getChildByTag(kTagTatgetInfo);
// 		target->removeFromParent();
// 	}
// 
// 	// create the new one
// 	curTargetInfo = TargetInfo::create(baseFighter);
// 	curTargetInfo->setPosition(ccp(265,winSize.height-curTargetInfo->getContentSize().height));
// 	curTargetInfo->setTag(kTagTatgetInfo);
// 	targetInfoLayer->addChild(curTargetInfo);
	if (targetInfoLayer->getChildByTag(kTagTatgetInfo))
	{
		TargetInfo * target = (TargetInfo *)targetInfoLayer->getChildByTag(kTagTatgetInfo);
		target->setVisible(true);
		target->initNewTargetInfo(baseFighter);
		//target->removeFromParent();
	}
	else
	{
		// create the new one
		curTargetInfo = TargetInfo::create(baseFighter);
		curTargetInfo->setPosition(ccp(265,winSize.height-curTargetInfo->getContentSize().height));
		curTargetInfo->setTag(kTagTatgetInfo);
		targetInfoLayer->addChild(curTargetInfo);
	}
}

void MainScene::BuffEvent( CCObject * pSender )
{
	CCLog("BuffEvent");

}

void MainScene::ButtonSceneTest( CCObject *pSender )
{
	if(isTestSceneOn)
	{
		testScene->removeFromParentAndCleanup(true);
		isTestSceneOn = false;
	}
	else
	{
		testScene = SceneTest::create();
		testScene->setPosition(ccp(370,205));
		addChild(testScene,4000);
		isTestSceneOn = true;
	}

	this->ButtonAnimation(pSender);
	
}

void MainScene::updateSysInfo( float t )//每4S刷新一次，更新系统通知
{
	//CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	//系统消息
	if(m_pUiLayer->getWidgetByName("Label_sysInfo") != NULL)
	{
		CCFadeOut *fadeOut = CCFadeOut::create(1);
		CCMoveTo * move = CCMoveTo::create(1,ccp(m_pUiLayer->getWidgetByName("Label_sysInfo")->getPosition().x,m_pUiLayer->getWidgetByName("Label_sysInfo")->getPosition().y+20));
		CCFiniteTimeAction*  action = CCSequence::create(
			move,
			CCRemoveSelf::create(),
			NULL);
		m_pUiLayer->getWidgetByName("Label_sysInfo")->runAction(action);
		m_pUiLayer->getWidgetByName("Label_sysInfo")->runAction(fadeOut);
	}
	if (this->sysInfo)
	{
		UILabel * Label_sysInfo = UILabel::create();
		Label_sysInfo->setText(sysInfo);
		Label_sysInfo->setFontSize(30);
		Label_sysInfo->setAnchorPoint(ccp(0.5f,0.5f));
		Label_sysInfo->setPosition(ccp(winSize.width/2,winSize.height/5*4-20));
		Label_sysInfo->setName("Label_sysInfo");
		m_pUiLayer->addWidget(Label_sysInfo);
		this->sysInfo = NULL;

		CCFadeIn *fadeIn = CCFadeIn::create(1);
		CCMoveTo * move = CCMoveTo::create(1,ccp(m_pUiLayer->getWidgetByName("Label_sysInfo")->getPosition().x,m_pUiLayer->getWidgetByName("Label_sysInfo")->getPosition().y+20));
		m_pUiLayer->getWidgetByName("Label_sysInfo")->runAction(move);
		m_pUiLayer->getWidgetByName("Label_sysInfo")->runAction(fadeIn);
	}
}

void MainScene::ButtonAnimation(CCObject *pSender)
{
	CCFiniteTimeAction*  action = CCSequence::create(
		CCScaleTo::create(0.2f*1/3,1.15f),
		CCScaleTo::create(0.2f*1/2,1.0f),
		NULL);

	((UIButton*)pSender)->runAction(action);
}

void MainScene::ReloadTarget(BaseFighter * baseFighter)
{
	GameView* gv = GameView::getInstance();

	// other player or monster etc.
	if(baseFighter->getRoleId() == gv->myplayer->getLockedActorId())
	{
		ReloadTargetData(baseFighter);
		return;
	}

	// MyPlayer
	if(GameView::getInstance()->isOwn(baseFighter->getRoleId()))
		ReloadMyPlayerData();
}

void MainScene::ReloadTargetData( BaseFighter * baseFighter )
{
	if(targetInfoLayer != NULL)
	{
		if (targetInfoLayer->getChildByTag(kTagTatgetInfo))
		{
			curTargetInfo->ReloadTargetData(baseFighter);
		}
	}
}

void MainScene::ReloadMyPlayerData()
{
	if(curMyHeadInfo != NULL)
		curMyHeadInfo->ReloadMyPlayerData();
}

void MainScene::ReloadBuffData()
{
	if(curMyHeadInfo != NULL)
		curMyHeadInfo->ReloadBuffData();

	if (this->getChildByTag(kTagMyBuff))
	{
		MyBuff * myBuff = (MyBuff*)this->getChildByTag(kTagMyBuff);
		myBuff->tableView->reloadData();
	}

	if(targetInfoLayer != NULL)
	{
		if (targetInfoLayer->getChildByTag(kTagTatgetInfo))
		{
			TargetInfo * target = (TargetInfo *)targetInfoLayer->getChildByTag(kTagTatgetInfo);
			target->ReloadBuffData();
		}
	}
}

// void MainScene::callBackFriendButton( CCObject * obj )
// {
// 	FriendUi * friendui=FriendUi::create();
// 	addChild(friendui,SCENE_UI_LAYER_POPUP_DORDER);
// }



void MainScene::onEnter()
{
	UIScene::onEnter();

	/// online reward
	RewardUi::addRewardListEvent(REWARD_LIST_ID_ONLINEREWARD);
	RewardUi::addRewardListEvent(REWARD_LIST_ID_SIGNMONTHLY);
	//RewardUi::setRewardListParticle();
	//money tree reward
// 	if (GameView::getInstance()->myplayer->getActiveRole()->level() >= 20)
// 	{
// 		if (!MoneyTreeData::instance()->hasAllGiftGet())
// 		{
// 			RewardUi::addRewardListEvent(REWARD_LIST_ID_MONEYTREE);
// 		}
// 	}

	//temp use
	//ScriptManager::getInstance()->runScript("script/strengthStar.sc");
	//ScriptManager::getInstance()->runScript("script/useGetFirstSkill.sc");
	//ScriptManager::getInstance()->runScript("script/getFirstSkill.sc");
	//ScriptManager::getInstance()->runScript("script/strength.sc");
	//ScriptManager::getInstance()->runScript("script/equipment.sc");
	//ScriptManager::getInstance()->runScript("script/robotOpen.sc");
	//ScriptManager::getInstance()->runScript("script/robotUi.sc");
	////ScriptManager::getInstance()->runScript("script/skill.sc");
	////ScriptManager::getInstance()->runScript("script/musouSkill1.sc");
	////ScriptManager::getInstance()->runScript("script/general2.sc");
	////ScriptManager::getInstance()->runScript("script/nsKillMonsterAndGeneralFight.sc");
	////ScriptManager::getInstance()->runScript("script/nsKillMonsterAndGeneralallFight.sc");
	////ScriptManager::getInstance()->runScript("script/nsKillMonster.sc");
	////ScriptManager::getInstance()->runScript("script/nsMove.sc");
	////ScriptManager::getInstance()->runScript("script/instanceGuide.sc");

	////ScriptManager::getInstance()->runScript("script/fiveinstance.sc");
	////ScriptManager::getInstance()->runScript("script/pkmode.sc");
	////ScriptManager::getInstance()->runScript("script/fightwaystudy.sc");
	////ScriptManager::getInstance()->runScript("script/generalMode.sc");

	////ScriptManager::getInstance()->runScript("script/firstGetMission.sc");
	////ScriptManager::getInstance()->runScript("script/firstFinishMission.sc");
	////ScriptManager::getInstance()->runScript("script/normalAttack.sc");
	////ScriptManager::getInstance()->runScript("script/offLineArena.sc");
	////ScriptManager::getInstance()->runScript("script/firstGeneralFight.sc");
	////ScriptManager::getInstance()->runScript("script/newGeneralMode.sc");
	////ScriptManager::getInstance()->runScript("script/guideToCopy3.sc");
	////ScriptManager::getInstance()->runScript("script/guideToXuanshang.sc");
	//ScriptManager::getInstance()->runScript("script/guideToQuestion.sc");
}

void MainScene::showChatWindows( CCObject *obj )
{
	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}
	
	ChatUI * chatui =(ChatUI *)this->getChildByTag(kTabChat);
	if (chatui == NULL)
	{
		chatui = ChatUI::create();
		chatui->ignoreAnchorPointForPosition(false);
		chatui->setAnchorPoint(ccp(0.5f, 0.5f));
		chatui->setPosition(ccp(winSize.width/2,winSize.height/2));
		this->addChild(chatui,0,kTabChat);
	}
	
	
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_relogin"));
	//ScriptManager::getInstance()->runScript("script/guideToGuoguanzhanjiang.sc");
	//ScriptManager::getInstance()->runScript("script/strengthStar.sc");
// 	SearchGeneral * search_ = SearchGeneral::create(1);
// 	search_->ignoreAnchorPointForPosition(false);
// 	search_->setAnchorPoint(ccp(0.5f, 0.5f));
// 	search_->setPosition(ccp(winSize.width/2,winSize.height/2));
// 	this->addChild(search_);
	//addFlowerParticleOnScene("fenmeigui");
	//ScriptManager::getInstance()->runScript("script/getFirstGeneral.sc");
	//flowerMarqueeVector.push_back(StringDataManager::getString("flower_said_flower_said"));
}


void MainScene::update( float delta )
{
	//marquee
	if (marqueeString.size()>0)
	{
		MarqueeControl * marqueeui =(MarqueeControl *)this->getChildByTag(kTagMarqueeControlBaseId);
		if (marqueeui == NULL)
		{
			MarqueeControl * marquee = MarqueeControl::create(marqueeString.at(0).marqueeString,marqueeString.at(0).marqueepriority);
			marquee->ignoreAnchorPointForPosition(false);
			marquee->setAnchorPoint(ccp(0,0));
			marquee->setPosition(ccp(winSize.width/2,winSize.height-100));
			marquee->setTag(kTagMarqueeControlBaseId);
			marquee->setZOrder(ZOrder_Marquee);
			this->addChild(marquee);
			marqueeString.erase(marqueeString.begin());
		}
	}

	//flower marquee
	if (flowerMarqueeVector.size() > 0)
	{
		FlowerMarquee * flowerMarquee_ =(FlowerMarquee *)this->getChildByTag(kTagFlowerMarqueeBaseId);
		if (flowerMarquee_ == NULL)
		{
			flowerMarquee_ = FlowerMarquee::create(flowerMarqueeVector.at(0));
			flowerMarquee_->ignoreAnchorPointForPosition(false);
			flowerMarquee_->setAnchorPoint(ccp(0,0));
			flowerMarquee_->setPosition(ccp(winSize.width/2,winSize.height-150));
			flowerMarquee_->setTag(kTagFlowerMarqueeBaseId);
			flowerMarquee_->setZOrder(SCENE_STORY_LAYER_BASE_ZORDER);
			this->addChild(flowerMarquee_);
			flowerMarqueeVector.erase(flowerMarqueeVector.begin());
		}
	}

	//flower Particle
	GameView::getInstance()->setFlowerParticleTime(GameView::getInstance()->getFlowerParticleTime() - 1000);
	if (GameView::getInstance()->getFlowerParticleTime() <= 0)
	{
		GameView::getInstance()->setFlowerParticleTime(0);
		//remove player flowerPacticle
		this->removeFlowerParticleOnRole();
	}


	//chatui acoustic marquee
	//flower marquee
	if (chatInfoMarqueeVector.size() > 0)
	{
		ChatAcousticMarquee * chatAcousticMarquee_ =(ChatAcousticMarquee *)this->getChildByTag(kTagChatAcousticMarqueeBaseId);
		if (chatAcousticMarquee_ == NULL)
		{
			chatCellInfo info_ = chatInfoMarqueeVector.at(0);
			std::string country_ = info_.strCountry;
			std::string name_ = info_.strName;
			std::string content_ = info_.strContent;
			int vipLv_ = info_.vipLevel;

			chatAcousticMarquee_ = ChatAcousticMarquee::create(country_,name_,content_,vipLv_);
			chatAcousticMarquee_->ignoreAnchorPointForPosition(false);
			chatAcousticMarquee_->setAnchorPoint(ccp(0.5f,0.5f));
			chatAcousticMarquee_->setPosition(ccp(this->getGeneralInfoUI()->getPositionX()+65,120));
			chatAcousticMarquee_->setTag(kTagChatAcousticMarqueeBaseId);
			chatAcousticMarquee_->setZOrder(SCENE_STORY_LAYER_BASE_ZORDER);
			this->addChild(chatAcousticMarquee_);
			chatInfoMarqueeVector.erase(chatInfoMarqueeVector.begin());
		}
	}


	//map id
	if (getRemindOpened()== false)
		return;

	if (GameView::getInstance()->getMapInfo()->maptype() == coliseum)
	{
	}
	else
	{
		if (this->getChildByTag(kTagGuideMapUI))
		{
			if (guideMap->getBtnRemind() != NULL )
			{
				if (GameView::getInstance()->remindvector.size() > 0)
				{
					guideMap->getBtnRemind()->setVisible(true);
					//buttonRemind
					CCTutorialParticle * tutorialParticle = (CCTutorialParticle *)guideMap->getCurActiveLayer()->getChildByTag(CCTUTORIALPARTICLETAG);
					if (tutorialParticle == NULL)
					{
						CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("tuowei0.plist",30,46);
						tutorialParticle->setPosition(ccp(guideMap->getBtnRemind()->getPosition().x,guideMap->getBtnRemind()->getPosition().y-guideMap->getBtnRemind()->getContentSize().height/2));
						tutorialParticle->setTag(CCTUTORIALPARTICLETAG);
						guideMap->getCurActiveLayer()->addChild(tutorialParticle);
					}
				}else
				{
					guideMap->getBtnRemind()->setVisible(false);
					CCTutorialParticle * particle = (CCTutorialParticle *)guideMap->getCurActiveLayer()->getChildByTag(CCTUTORIALPARTICLETAG);
					if (particle != NULL)
					{
						particle->removeFromParentAndCleanup(true);
					}
				}
			}
		}
	}
}

void MainScene::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

//右上角的箭头
void MainScene::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(ccp(pos.x-50,pos.y-40));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,32,45,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+32/2,pos.y+45/2));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->getMainUITuturialLayer()->addChild(tutorialIndicator);
}

//指向摇杆
void MainScene::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(ccp(pos.x+100,pos.y+100));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	tutorialIndicator->addHighLightFrameAction(60,60);
// 	this->addChild(tutorialIndicator);

	float pJoystick_pos_y = (27)*(winSize.height*1.0f/UI_DESIGN_RESOLUTION_HEIGHT);
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,100,100,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+100,pos.y+100+pJoystick_pos_y));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->getMainUITuturialLayer()->addChild(tutorialIndicator);
}

void MainScene::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	CCTutorialParticle* tutorialParticle = dynamic_cast<CCTutorialParticle*>(this->getChildByTag(CCTUTORIALPARTICLETAG));
	if(tutorialParticle != NULL)
		tutorialParticle->removeFromParent();

	CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(this->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
	if(teachingGuide != NULL)
		teachingGuide->removeFromParent();
}

CCArmature * MainScene::createPendantAnm(const char * firstFontName,const char * secondFontName,const char * thirdFontName,const char * fourthFontName)
{
	//从导出文件异步加载动画
	CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("res_ui/uiflash/diaoshi/diaoshi0.png","res_ui/uiflash/diaoshi/diaoshi0.plist","res_ui/uiflash/diaoshi/diaoshi.ExportJson");
	//根据动画名称创建动画精灵
	CCArmature *armature = CCArmature::create("diaoshi");
	//播放指定动作
	armature->getAnimation()->playByIndex(0);
	//修改属性
	armature->setScale(1.0f);
	//设置动画精灵位置
	armature->setPosition(600,240);
	//添加到当前页面
	//this->addChild(armature);
	//first font
	CCBone * bone_first = armature->getBone("Layer22");
	if (strcmp("",firstFontName) == 0)
	{
		CCSprite * sprite_first = CCSprite::create("res_ui/starstar.png");
		sprite_first->setAnchorPoint(ccp(0.5f,0.0f));
		sprite_first->setPosition(ccp(38,0));
		sprite_first->setRotation(-90);
		//armature->addChild(sprite_first);
		cocos2d::extension::CCBone *bone_first_sprite  = cocos2d::extension::CCBone::create("sprite_first");
		bone_first_sprite->addDisplay(sprite_first, 0);
		bone_first_sprite->changeDisplayByIndex(0, true);
		bone_first_sprite->setIgnoreMovementBoneData(true);
		bone_first_sprite->setZOrder(100);
		//bone_first->addChildBone(bone_first_sprite);
		armature->addBone(bone_first_sprite, "Layer22");
		
	}
	else
	{
		CCLabelBMFont * l_first = CCLabelBMFont::create(firstFontName,"res_ui/font/ziti_1.fnt");
		l_first->setAnchorPoint(ccp(0.5f,0.0f));
		l_first->setPosition(ccp(41,1));
		l_first->setRotation(-90);
		l_first->setScale(0.8f);
		//armature->addChild(l_first);
		cocos2d::extension::CCBone *bone_first_bmfont  = cocos2d::extension::CCBone::create("l_first");
		bone_first_bmfont->addDisplay(l_first, 0);
		bone_first_bmfont->changeDisplayByIndex(0, true);
		bone_first_bmfont->setIgnoreMovementBoneData(true);
		bone_first_bmfont->setZOrder(100);
		//bone_first->addChildBone(bone_first_bmfont);
		armature->addBone(bone_first_bmfont, "Layer22");
		
	}

	//second font
	CCBone * bone_second = armature->getBone("Layer20");
	if (strcmp("",secondFontName) == 0)
	{
		CCSprite * sprite_second = CCSprite::create("res_ui/starstar.png");
		sprite_second->setAnchorPoint(ccp(0.5f,0.0f));
		sprite_second->setPosition(ccp(41,0));
		sprite_second->setRotation(-90);
		//armature->addChild(sprite_second);
		cocos2d::extension::CCBone *bone_second_sprite  = cocos2d::extension::CCBone::create("sprite_second");
		bone_second_sprite->addDisplay(sprite_second, 0);
		bone_second_sprite->changeDisplayByIndex(0, true);
		bone_second_sprite->setIgnoreMovementBoneData(true);
		bone_second_sprite->setZOrder(100);
		//bone_second->addChildBone(bone_second_sprite);
		armature->addBone(bone_second_sprite, "Layer20");
		
	}
	else
	{
		CCLabelBMFont * l_second = CCLabelBMFont::create(secondFontName,"res_ui/font/ziti_1.fnt");
		l_second->setAnchorPoint(ccp(0.5f,0.0f));
		l_second->setPosition(ccp(47,1));
		l_second->setRotation(-90);
		l_second->setScale(0.8f);
		//armature->addChild(l_second);
		cocos2d::extension::CCBone *bone_second_bmfont  = cocos2d::extension::CCBone::create("l_second");
		bone_second_bmfont->addDisplay(l_second, 0);
		bone_second_bmfont->changeDisplayByIndex(0, true);
		bone_second_bmfont->setIgnoreMovementBoneData(true);
		bone_second_bmfont->setZOrder(100);
		//bone_second->addChildBone(bone_second_bmfont);
		armature->addBone(bone_second_bmfont, "Layer20");
		
	}

	//third font
	CCBone * bone_third = armature->getBone("Layer18");
	if (strcmp("",thirdFontName) == 0)
	{
		CCSprite * sprite_third = CCSprite::create("res_ui/starstar.png");
		sprite_third->setAnchorPoint(ccp(0.5f,0.0f));
		sprite_third->setPosition(ccp(37,0));
		sprite_third->setRotation(-90);
		//armature->addChild(sprite_third);
		cocos2d::extension::CCBone *bone_third_sprite  = cocos2d::extension::CCBone::create("sprite_third");
		bone_third_sprite->addDisplay(sprite_third, 0);
		bone_third_sprite->changeDisplayByIndex(0, true);
		bone_third_sprite->setIgnoreMovementBoneData(true);
		bone_third_sprite->setZOrder(100);
		//bone_third->addChildBone(bone_third_sprite);
		armature->addBone(bone_third_sprite, "Layer18");
		
	}
	else
	{
		CCLabelBMFont * l_third = CCLabelBMFont::create(thirdFontName,"res_ui/font/ziti_1.fnt");
		l_third->setAnchorPoint(ccp(0.5f,0.0f));
		l_third->setPosition(ccp(45,1));
		l_third->setRotation(-90);
		l_third->setScale(0.8f);
		//armature->addChild(l_third);
		cocos2d::extension::CCBone *bone_third_bmfont  = cocos2d::extension::CCBone::create("l_third");
		bone_third_bmfont->addDisplay(l_third, 0);
		bone_third_bmfont->changeDisplayByIndex(0, true);
		bone_third_bmfont->setIgnoreMovementBoneData(true);
		bone_third_bmfont->setZOrder(100);
		//bone_third->addChildBone(bone_third_bmfont);
		armature->addBone(bone_third_bmfont, "Layer18");
		
	}

	//fourth font
	CCBone * bone_fourth = armature->getBone("Layer16");
	if (strcmp("",fourthFontName) == 0)
	{
		CCSprite * sprite_fourth = CCSprite::create("res_ui/starstar.png");
		sprite_fourth->setAnchorPoint(ccp(0.5f,0.0f));
		sprite_fourth->setPosition(ccp(30,0));
		sprite_fourth->setRotation(-90);
		//armature->addChild(sprite_fourth);
		cocos2d::extension::CCBone *bone_fouth_sprite  = cocos2d::extension::CCBone::create("sprite_fourth");
		bone_fouth_sprite->addDisplay(sprite_fourth, 0);
		bone_fouth_sprite->changeDisplayByIndex(0, true);
		bone_fouth_sprite->setIgnoreMovementBoneData(true);
		bone_fouth_sprite->setZOrder(100);
		//bone_fourth->addChildBone(bone_fouth_sprite);
		armature->addBone(bone_fouth_sprite, "Layer16");
	}
	else
	{
		CCLabelBMFont * l_fourth = CCLabelBMFont::create(fourthFontName,"res_ui/font/ziti_1.fnt");
		l_fourth->setAnchorPoint(ccp(0.5f,0.0f));
		l_fourth->setPosition(ccp(35,1));
		l_fourth->setRotation(-90);
		l_fourth->setScale(0.8f);
		//armature->addChild(l_fourth);
		cocos2d::extension::CCBone *bone_fourth_bmfont  = cocos2d::extension::CCBone::create("l_fourth");
		bone_fourth_bmfont->addDisplay(l_fourth, 0);
		bone_fourth_bmfont->changeDisplayByIndex(0, true);
		bone_fourth_bmfont->setIgnoreMovementBoneData(true);
		bone_fourth_bmfont->setZOrder(100);
		//bone_fourth->addChildBone(bone_fourth_bmfont);
		armature->addBone(bone_fourth_bmfont, "Layer16");
		
	}

	return armature;
}
/////
void MainScene::interactRelationOfPlayers(CRelationPlayer * relationPlayer)
{
	this->setInteractOfPlayerId(relationPlayer->playerid());
	const char *strings_ = StringDataManager::getString("mainscene_interactPOtherlayer");
	std::string playerName = relationPlayer->playername();
	playerName.append(strings_);
	GameView::getInstance()->showPopupWindow(playerName,2,this,coco_selectselector(MainScene::interactOtherplayerSure),
																coco_selectselector(MainScene::interactOtherplayerNo));
}

void MainScene::interactOtherplayerSure(CCObject * obj)
{
	this->setInteractOfPlayer(true);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2206, this);
}

void MainScene::interactOtherplayerNo(CCObject * obj)
{
	this->setInteractOfPlayer(false);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2206, this);
}

void MainScene::setInteractOfPlayerId( long long playerId )
{
	interactOfPlayer = playerId;
}

void MainScene::setInteractOfPlayer( bool isInteract )
{
	isInteractPlayer = isInteract;
}

int MainScene::getInteractOfPlayerId()
{
	return interactOfPlayer;
}

bool MainScene::getInteractOfPlayer()
{
	return isInteractPlayer;
}

void MainScene::checkIsNewRemind()
{
	//skill
	remindOfSkill();
	//mission
	remindMission();
	//general
	remindOfGeneral();
	
	//remindMail();	
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void *)0);
	///remind family applyPlayersize  self is not player
	long long roleGuildId_ = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionid();
	long long roleGuildPost_ = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition();
	if (roleGuildId_ > 0 && roleGuildPost_ != 3 )
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1505,(void*)0);
		//remindFamilyApply
	}
	//--------get reward---------//

	//offline arena  and  offarea prize
	//send req for arenaList 
	for (int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();i++)
	{
		if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() == 3)
		{
			if (strcmp(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str(),"btn_offLineArena") != 0)
				continue;

			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel()<= GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(3000);
				//remindOffArena();
				break;
			}
		}
	}
	
	//every day sign
	remindOfSignDaily();
	//vip reward
	remindReward();
	//offline exp
	remindOffLineEx();

	//singCopyPrize
	for (int i = 0; i<SingleCopyClazzConfig::s_SingCopyClazzMsgData.size();i++)
	{
		int clazz_ = i+1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1905,(void *)clazz_);
	}

	//get friendPhypower
	remindGetFriendPhypower();
}

void MainScene::addNewRemind( CCObject * obj )
{
	UIButton * button_ = (UIButton *)obj;
	UIImageView * imageRemind =(UIImageView *)button_->getChildByTag(5123);
	if (imageRemind == NULL)
	{
		UIImageView * newRemind = UIImageView::create();
		newRemind->setAnchorPoint(ccp(0.5f,0.5f));
		newRemind->setTexture("res_ui/new.png");
		newRemind->setPosition(ccp(button_->getSize().width/2-newRemind->getSize().width/3,
										button_->getSize().height/2 - newRemind->getSize().height/3));
		newRemind->setWidgetTag(5123);
		button_->addChild(newRemind);
	}
}

void MainScene::removeOldRemind( CCObject * obj )
{
	UIButton * button_ = (UIButton *)obj;
	UIImageView * imageRemind =(UIImageView *)button_->getChildByTag(5123);
	if (imageRemind != NULL)
	{
		imageRemind->removeFromParentAndCleanup(true);
	}
}

void MainScene::addNewRemindOfTab( CCObject * obj )
{
	UIButton * button_ = (UIButton *)obj;	
	UIImageView * imageRemind =(UIImageView *)button_->getChildByTag(5123);
	if (imageRemind == NULL)
	{
		UIImageView * newRemind = UIImageView::create();
		newRemind->setTexture("res_ui/new.png");
		newRemind->setAnchorPoint(ccp(0.5f,0.5f));
		newRemind->setPosition(ccp(button_->getContentSize().width/2-newRemind->getContentSize().width/3,
											button_->getContentSize().height-newRemind->getContentSize().height/3));
		newRemind->setWidgetTag(5123);
		button_->addChild(newRemind);
	}
}

void MainScene::removeOldRemindOfTab( CCObject * obj )
{
	UIButton * button_ = (UIButton *)obj;
	UIImageView * imageRemind =(UIImageView *)button_->getChildByTag(5123);
	if (imageRemind != NULL)
	{
		imageRemind->removeFromParentAndCleanup(true);
	}
}

void MainScene::addNewRemindOfTabLine( CCObject * obj )
{
	UIButton * button_ = (UIButton *)obj;	
	UIImageView * imageRemind =(UIImageView *)button_->getChildByTag(5123);
	if (imageRemind == NULL)
	{
		UIImageView * newRemind = UIImageView::create();
		newRemind->setTexture("res_ui/new.png");
		newRemind->setAnchorPoint(ccp(0.5f,0.5f));
		newRemind->setPosition(ccp(button_->getContentSize().width/2+newRemind->getContentSize().width/2,
						button_->getContentSize().height/2-newRemind->getContentSize().height/2));
		newRemind->setWidgetTag(5123);
		button_->addChild(newRemind);
	}
}

void MainScene::removeOldRemindOfTabLine( CCObject * obj )
{
	UIButton * button_ = (UIButton *)obj;
	UIImageView * imageRemind =(UIImageView *)button_->getChildByTag(5123);
	if (imageRemind != NULL)
	{
		imageRemind->removeFromParentAndCleanup(true);
	}
}

void MainScene::addNewRemindOfLabBmfont( CCObject * obj )
{
	UIWidget * widget_ = (UIWidget*)obj;
	UIImageView * imageRemind =(UIImageView *)widget_->getChildByTag(5123);
	if (imageRemind == NULL)
	{
		UIImageView * newRemind = UIImageView::create();
		newRemind->setTexture("res_ui/new.png");
		newRemind->setAnchorPoint(ccp(0.5f,0.5f));
		newRemind->setScale(0.8f);
		newRemind->setPosition(ccp(3,widget_->getContentSize().height));
		newRemind->setWidgetTag(5123);
		widget_->addChild(newRemind);
	}
}
void MainScene::removeOldRemindOfLabelBmfont( CCObject * obj )
{
	UILabelBMFont * labBmfont = (UILabelBMFont *)obj;
	UIImageView * imageRemind =(UIImageView *)labBmfont->getChildByTag(5123);
	if (imageRemind != NULL)
	{
		imageRemind->removeFromParentAndCleanup(true);
	}
}

void MainScene::initVirtualJoystick()
{
	// add virtual joystick
	CCSize screenSize=CCDirector::sharedDirector()->getVisibleSize();
	CCLayer * JoyStickLayer = CCLayer::create();

	CCSprite *pPointL=CCSprite::create("gamescene_state/zhujiemian3/yaogan/yaogan.png");//摇杆
	//testPointL->setOpacity(156);
	CCSprite *pBGL=CCSprite::create("gamescene_state/zhujiemian3/yaogan/di.png");//摇杆背景
	//testBGL->setVisible(false);	
	float pJoystick_pos_y = (27)*(winSize.height*1.0f/UI_DESIGN_RESOLUTION_HEIGHT);
	Joystick *pJoystick=Joystick::JoystickWithCenter(ccp(100,pJoystick_pos_y+100), 60.0f, pPointL, pBGL);
	this->addChild(pJoystick, 0, kTagVirtualJoystick);
	//pJoystick->setPosition(ccp(0,pJoystick_pos_y));
	pJoystick->Active();
}

void MainScene::updateRecuriteInfo( float dt )
{
	for(int i = 0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		CActionDetail * temp = GameView::getInstance()->actionDetailList.at(i);
		if (temp->type() == TEN_TIMES)
			continue;

		if (temp->type() == BASE)
		{
			if (temp->playerremainnumber() <= 0)
			{
				for (int j=0;j<GameView::getInstance()->generalIndexvector.size();j++)
				{
					if (GameView::getInstance()->generalIndexvector.at(j)== 1)
					{
						GameView::getInstance()->generalIndexvector.erase(GameView::getInstance()->generalIndexvector.begin()+j);
						remindOfGeneral();
						break;
					}
				}

				continue;
			}
				

			long long tempTime = temp->cdtime()/1000;
			if(temp->cdtime() <= 0)
			{
				temp->set_cdtime(0);
				bool isExit=false;
				for (int j=0;j<GameView::getInstance()->generalIndexvector.size();j++)
				{
				 	if (GameView::getInstance()->generalIndexvector.at(j)== 1)
				 	{

				 		isExit =true;
						break;
				 	}
				}
				if (!isExit)
				{
				 	GameView::getInstance()->generalIndexvector.push_back(1);
					//checkIsNewRemind();
					remindOfGeneral();
				}
			}
			else
			{
				bool isExit=false;
				for (int j=0;j<GameView::getInstance()->generalIndexvector.size();j++)
				{
					if (GameView::getInstance()->generalIndexvector.at(j)== 1)
					{
						isExit =true;
						GameView::getInstance()->generalIndexvector.erase(GameView::getInstance()->generalIndexvector.begin()+j);
						//checkIsNewRemind();
						remindOfGeneral();
						break;
					}
				}
			}

			tempTime--;
			temp->set_cdtime(tempTime*1000); 
			
		}
		else
		{
			long long tempTime = temp->cdtime()/1000;
			if(temp->cdtime() <= 0)
			{
				temp->set_cdtime(0);
				bool isExit=false;
				for (int j=0;j<GameView::getInstance()->generalIndexvector.size();j++)
				{
					if (GameView::getInstance()->generalIndexvector.at(j)== temp->type())
					{
						isExit =true;
						break;
					}
				}
				if (!isExit)
				{
					GameView::getInstance()->generalIndexvector.push_back(temp->type());
					//checkIsNewRemind();
					remindOfGeneral();
				}
			}
			else
			{
				bool isExit=false;
				for (int j=0;j<GameView::getInstance()->generalIndexvector.size();j++)
				{
					if (GameView::getInstance()->generalIndexvector.at(j)== temp->type())
					{
						isExit =true;
						GameView::getInstance()->generalIndexvector.erase(GameView::getInstance()->generalIndexvector.begin()+j);
						//checkIsNewRemind();
						remindOfGeneral();
						break;
					}
				}
			}

			tempTime--;
			temp->set_cdtime(tempTime*1000); 
			
		}
	}
}

void MainScene::createRemoteStoreHouseUI()
{
	int openLevel = -1;
	VipConfigData::TypeIdAndVipLevel typeAndLevel;
	typeAndLevel.typeName = StringDataManager::getString("vip_function_remoteStoreHouse");

	for(int i = 0;i<=VipDescriptionConfig::s_vipDes.size();++i)
	{
		typeAndLevel.vipLevel = i;

		std::map<VipConfigData::TypeIdAndVipLevel,CVipInfo*>::const_iterator cIter;
		cIter = VipConfigData::s_vipConfig.find(typeAndLevel);
		if (cIter == VipConfigData::s_vipConfig.end()) // 没找到就是指向END了  
		{
			continue;
		}
		else
		{
			openLevel = typeAndLevel.vipLevel;
			break;
		}
	}

	if (openLevel < 0)
		return;

	//vip等级
	if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() < openLevel)
	{
		std::string str_des = StringDataManager::getString("vip_function_des");
		char s_vipLevel[20];
		sprintf(s_vipLevel,"%d",openLevel);
		str_des.append(s_vipLevel);
		str_des.append(StringDataManager::getString("vip_function_willOpen"));
		GameView::getInstance()->showAlertDialog(str_des.c_str());
		return;
	}

	PackageScene *packageScene = (PackageScene*)this->getChildByTag(kTagBackpack);
	if(packageScene != NULL)
	{
		packageScene->closeAnim();
	}

	CCActionInterval * action =(CCActionInterval *)CCSequence::create(CCDelayTime::create(0.5f),
		CCCallFunc::create(this,callfunc_selector(MainScene::newRemoteStoreHouseUI)),
		NULL);
	runAction(action);
}

void MainScene::newRemoteStoreHouseUI()
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1317);
	CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
	CCLayer *storeHouseLayer = (CCLayer*)this->getChildByTag(kTagStoreHouseUI);
	if(storeHouseLayer == NULL)
	{
		storeHouseLayer = StoreHouseUI::create();
		this->addChild(storeHouseLayer,0,kTagStoreHouseUI);
		storeHouseLayer->ignoreAnchorPointForPosition(false);
		storeHouseLayer->setAnchorPoint(ccp(0.5f, 0.5f));
		storeHouseLayer->setPosition(ccp(winSize.width/2, winSize.height/2));
	}
}

void MainScene::remindOfSkill()
{
	for (int i =0; i <GameView::getInstance()->remindvector.size();i++)
	{
		if (GameView::getInstance()->remindvector.at(i) == KTagSkillScene)
		{
			GameView::getInstance()->remindvector.erase(GameView::getInstance()->remindvector.begin()+i);
		}
	}
	int skillRemain = GameView::getInstance()->myplayer->player->skillpoint();	 
	bool isSkillLevel = false;
	
	isSkillLevel = SkillScene::IsExistSkillEnableToLevelUp();

	SkillScene * skillScene =(SkillScene *)this->getChildByTag(KTagSkillScene);
	if (skillRemain > 0 && isSkillLevel == true)
	{
		this->addNewRemind(headMenu->btn_skill);
		if (skillScene != NULL)
		{
			this->addNewRemindOfTab(skillScene->SkillTypeTab->getObjectByIndex(0));
			this->addNewRemindOfLabBmfont(skillScene->image_remainFightpPointFrame);
		}

		GameView::getInstance()->remindvector.push_back(KTagSkillScene);
	}else
	{
		this->removeOldRemind(headMenu->btn_skill);
		if (skillScene != NULL)
		{
			this->removeOldRemindOfTab(skillScene->SkillTypeTab->getObjectByIndex(0));
			this->removeOldRemindOfLabelBmfont(skillScene->image_remainFightpPointFrame);
		}
	}
}

void MainScene::remindOfGeneral()
{
	for (int i = 0;i<GameView::getInstance()->remindvector.size();i++ )
	{
		if (GameView::getInstance()->remindvector.at(i) == kTagGeneralsUI)
		{
			GameView::getInstance()->remindvector.erase(GameView::getInstance()->remindvector.begin()+i);
		}
	}

	GeneralsUI * generalui = (GeneralsUI *)this->getChildByTag(kTagGeneralsUI);
	int generalAmount = GameView::getInstance()->generalIndexvector.size();
	if (generalAmount > 0)
	{
		this->addNewRemind(headMenu->btn_generals);
		if (generalui!= NULL)
		{
			this->addNewRemindOfTabLine(generalui->mainTab->getObjectByIndex(0));

			bool firstGeneralbutton = false;
			bool secondGeneralbutton = false;
			bool thirdGeneralbutton = false;
			for (int indexGeneral_ = 0;indexGeneral_ < generalAmount;indexGeneral_ ++)
			{
				if (GameView::getInstance()->generalIndexvector.at(indexGeneral_) == 1)
				{
					this->addNewRemind(GeneralsUI::generalsRecuriteUI->Button_normalRecurit);
					firstGeneralbutton =true;
				}

				if (GameView::getInstance()->generalIndexvector.at(indexGeneral_) == 2)
				{
					this->addNewRemind(GeneralsUI::generalsRecuriteUI->Button_goodRecurit);
					secondGeneralbutton =true;
				}

				if (GameView::getInstance()->generalIndexvector.at(indexGeneral_) == 3)
				{
					this->addNewRemind(GeneralsUI::generalsRecuriteUI->Button_wonderfulRecurit);
					thirdGeneralbutton =true;
				}
			}
			if (firstGeneralbutton == false)
			{
				this->removeOldRemind(GeneralsUI::generalsRecuriteUI->Button_normalRecurit);
			}
			if (secondGeneralbutton ==false)
			{
				this->removeOldRemind(GeneralsUI::generalsRecuriteUI->Button_goodRecurit);
			}
			if (thirdGeneralbutton ==false)
			{
				this->removeOldRemind(GeneralsUI::generalsRecuriteUI->Button_wonderfulRecurit);
			}
		}

		GameView::getInstance()->remindvector.push_back(kTagGeneralsUI);
	}else
	{
		this->removeOldRemind(headMenu->btn_generals);
		if (generalui!= NULL)
		{
			this->removeOldRemindOfTabLine(generalui->mainTab->getObjectByIndex(0));
			this->removeOldRemind(GeneralsUI::generalsRecuriteUI->Button_normalRecurit);
			this->removeOldRemind(GeneralsUI::generalsRecuriteUI->Button_goodRecurit);
			this->removeOldRemind(GeneralsUI::generalsRecuriteUI->Button_wonderfulRecurit);
		}
	}
}

void MainScene::remindOffArena()
{
	//when self is no1 not Pk
	if(OffLineArenaData::getInstance()->m_nMyRoleRanking != 1)
	{
		for (int i = 0; i<GameView::getInstance()->remindvector.size();i++ )
		{
			if (GameView::getInstance()->remindvector.at(i) == kTagOffLineArenaUI)
			{
				GameView::getInstance()->remindvector.erase(GameView::getInstance()->remindvector.begin()+i);
			}
		}
		if (GameView::getInstance()->getRemainTimesOfArena() > 0)
		{
			GameView::getInstance()->remindvector.push_back(kTagOffLineArenaUI);
		}
	}
}


void MainScene::remindOffArenaPrize()
{
	bool isHave = false;
	for (int i = 0; i<GameView::getInstance()->rewardvector.size();i++ )
	{
		if (GameView::getInstance()->rewardvector.at(i)->getId() ==  REWARD_LIST_ID_OFFARENA)
		{
			isHave = true;
		}
	}

	if (isHave == false)
	{
		if(getOffAreaPrizeTime() <= 0)
		{
			RewardUi::addRewardListEvent(REWARD_LIST_ID_OFFARENA);
		}
	}
}

void MainScene::remindOfSignDaily()
{
	bool isHave = false;
	for (int i = 0; i<GameView::getInstance()->rewardvector.size();i++ )
	{
		if (GameView::getInstance()->rewardvector.at(i)->getId() ==  REWARD_LIST_ID_SIGNDAILY)
		{
			isHave = true;
		}
	}

	if (isHave == false)
	{
		if(GameView::getInstance()->myplayer->getActiveRole()->level() >= SIGN_DAILYGIFT_LEVEL)
		{
			RewardUi::addRewardListEvent(REWARD_LIST_ID_SIGNDAILY);
		}
	}
}

void MainScene::remindReward()
{
	// a包 由于vip原因 屏蔽离线经验系统
	if (GameConfig::getBoolForKey("vip_enable") == false)
	{
		return;
	}

	bool isHave = false;
	for (int i = 0; i<GameView::getInstance()->rewardvector.size();i++ )
	{
		if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_VIPEVERYDAYREWARD)
		{
			isHave = true;
		}
	}
	
	if (isHave == false)
	{
		for(int i =0; i< GameView::getInstance()->m_vipEveryDayRewardsList.size();i++)
		{
			if(GameView::getInstance()->m_vipEveryDayRewardsList.at(i)->status()==2)
			{
				RewardUi::addRewardListEvent(REWARD_LIST_ID_VIPEVERYDAYREWARD);
				break;
			}
		}
	}
}

void MainScene::remindOffLineEx()
{
	// a包 由于vip原因 屏蔽离线经验系统
	if (GameConfig::getBoolForKey("vip_enable") == false)
	{
		return;
	}

	bool isHave = false;
	for (int i = 0; i<GameView::getInstance()->rewardvector.size();i++ )
	{
		if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_OFFLINEEXP)
		{
			isHave = true;
		}
	}

	if (isHave == false)
	{
		bool isGeted = false;
		for (int i=0;i<GameView::getInstance()->m_offLineExpPuf.size();i++)
		{
			COfflineExpPuf * temp = GameView::getInstance()->m_offLineExpPuf.at(i);
			if (temp->m_bIsGeted == true)
			{
				isGeted = true;
				break;
			}
		}
		if (!isGeted)
		{
			for (int i=0;i<GameView::getInstance()->m_offLineExpPuf.size();i++)
			{
				if (i == GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel())
				{
					COfflineExpPuf * temp = GameView::getInstance()->m_offLineExpPuf.at(i);
					if (temp->normal() > 0)
					{
						RewardUi::addRewardListEvent(REWARD_LIST_ID_OFFLINEEXP);
						break;
					}
				}
			}
		}
	}
}

void MainScene::remindMission()
{
	int missionAaccept =0;
	int missionSize = GameView::getInstance()->missionManager->MissionList.size();
	for (int missionIndex = 0;missionIndex< missionSize;missionIndex++)
	{
		int missionState = GameView::getInstance()->missionManager->MissionList.at(missionIndex)->missionstate();
		int missionType = GameView::getInstance()->missionManager->MissionList.at(missionIndex)->missionpackagetype();
		if (missionState == 0 && (missionType == 1 || missionType == 2))
		{
			missionAaccept++;
		}
	}

	for (int i = 0; i<GameView::getInstance()->remindvector.size();i++ )
	{
		if (GameView::getInstance()->remindvector.at(i) == kTagMissionScene)
		{
			GameView::getInstance()->remindvector.erase(GameView::getInstance()->remindvector.begin()+i);
		}
	}

	MissionScene* missionScene = (MissionScene *)this->getChildByTag(kTagMissionScene);
	if (missionAaccept > 0)
	{
		this->addNewRemind(headMenu->btn_mission);
		if (missionScene != NULL)
		{
			this->addNewRemindOfTab(missionScene->mainTab->getObjectByIndex(1));
		}
		GameView::getInstance()->remindvector.push_back(kTagMissionScene);
	}else
	{
		this->removeOldRemind(headMenu->btn_mission);
		if (missionScene != NULL)
		{
			this->removeOldRemindOfTab(missionScene->mainTab->getObjectByIndex(1));
		}
	}
}

void MainScene::remindMail()
{
	int systemMail = 0;
	//int playerMail =0;
	for (int mailIndex =0;mailIndex<GameView::getInstance()->mailVectorOfSystem.size();mailIndex++)
	{
		if (GameView::getInstance()->mailVectorOfSystem.at(mailIndex)->flag() == 0)
		{
			systemMail++;
		}
	}
	/*
	for (int mailIndex =0;mailIndex<GameView::getInstance()->mailVectorOfPlayer.size();mailIndex++)
	{
		if (GameView::getInstance()->mailVectorOfPlayer.at(mailIndex)->flag() == 0)
		{
			playerMail++;
		}
	}
	*/

	MailUI * mailui_ = (MailUI *)this->getChildByTag(kTagMailUi);
	if (systemMail > 0 /*|| playerMail> 0*/ )
	{
		this->addNewRemind(headMenu->Button_mail);
		if (mailui_ != NULL)
		{
			this->addNewRemindOfTab(mailui_->channelLabel->getObjectByIndex(0));

			/*
			if (systemMail > 0)
			{
				this->addNewRemindOfTabLine(mailui_->channelLabelMail->getObjectByIndex(0));
			}

			if (playerMail > 0)
			{
				this->addNewRemindOfTabLine(mailui_->channelLabelMail->getObjectByIndex(1));
			}
			*/
		}
		//GameView::getInstance()->remindvector.push_back(kTagMailUi);
	}else
	{
		this->removeOldRemind(headMenu->Button_mail);
		if(mailui_ != NULL)
		{
			this->removeOldRemindOfTab(mailui_->channelLabel->getObjectByIndex(0));
			/*
			if (systemMail <= 0)
			{
				this->removeOldRemindOfTabLine(mailui_->channelLabelMail->getObjectByIndex(0));
			}
			if (playerMail <= 0)
			{
				this->removeOldRemindOfTabLine(mailui_->channelLabelMail->getObjectByIndex(1));
			}
			*/
		}
	}
}

void MainScene::remindFamilyApply()
{
	//family apply player size
	FamilyUI * familyui = (FamilyUI *)this->getChildByTag(kTagFamilyUI);
	if (GameView::getInstance()->applyPlayersize > 0)
	{
		this->addNewRemind(headMenu->Button_family);
		//GameView::getInstance()->remindvector.push_back(kTagFamilyUI);
		if (familyui != NULL)
		{
			this->addNewRemind(familyui->btn_mamage);
			if (familyui->loadLayer->getChildByTag(familyLeaderManager))
			{
				FamilyOperatorOfLeader * leaderui = (FamilyOperatorOfLeader *)familyui->loadLayer->getChildByTag(familyLeaderManager);
				this->addNewRemind(leaderui->button_check);
			}
		}
	}else
	{
		this->removeOldRemind(headMenu->Button_family);
		if (familyui != NULL)
		{
			this->removeOldRemind(familyui->btn_mamage);
			if (familyui->loadLayer->getChildByTag(familyLeaderManager))
			{
				FamilyOperatorOfLeader * leaderui = (FamilyOperatorOfLeader *)familyui->loadLayer->getChildByTag(familyLeaderManager);
				this->removeOldRemind(leaderui->button_check);
			}
		}
	}
}

void MainScene::remindSingCopyPrize()
{
	//单人副本奖励 提升列表 奖励ui 对应的tag值
	//clazz 1 = 100
	//clazz 2 = 200
	//clazz 3 = 300
	//GameView::getInstance()->remindvector.push_back();
}

void MainScene::remindGetFriendPhypower()
{
	bool isExist = false;
	for ( int  i =0;i<GameView::getInstance()->rewardvector.size();i++)
	{
		if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_PHYSICAL_FRIEND)
		{
			isExist = true;
		}
	}

	FriendUi * friend_ui = (FriendUi *)this->getChildByTag(kTagFriendUi);
	if (isExist == true)
	{
		this->addNewRemind(headMenu->Button_friend);
		if (friend_ui != NULL)
		{
			this->addNewRemindOfTab(friend_ui->friendType->getObjectByIndex(0));
			this->addNewRemind(friend_ui->btn_phypower);
		}
	}else
	{
		this->removeOldRemind(headMenu->Button_friend);
		if (friend_ui != NULL)
		{
			this->removeOldRemindOfLabelBmfont(friend_ui->friendType->getObjectByIndex(0));
			this->removeOldRemind(friend_ui->btn_phypower);
		}
	}
}

void MainScene::repairEquipMentRemind(long long instanceId /* = 0*/)
{
	if(btnreminRepair != NULL)
		btnreminRepair->setVisible(false);

	//role equipment
	for(int i=0;i<GameView::getInstance()->EquipListItem.size();i++)
	{
		int equipDrug_ = GameView::getInstance()->EquipListItem.at(i)->goods().equipmentdetail().durable();
		long long equipInstanceid = GameView::getInstance()->EquipListItem.at(i)->goods().instanceid();
		
		if (equipDrug_<= 10)
		{
			//alway show in game
			btnreminRepair->setVisible(true);
			setBackPacSelectIndex(0);
			return;
		}
	}

	//inLine General equipment
	for (int generalIndex=0;generalIndex<GameView::getInstance()->generalsInLineDetailList.size();generalIndex++)
	{
		int generalSize = GameView::getInstance()->generalsInLineDetailList.at(generalIndex)->equipments_size();
		for (int generalEquipIndex =0;generalEquipIndex < generalSize;generalEquipIndex++)
		{
			int equipDrug_ = GameView::getInstance()->generalsInLineDetailList.at(generalIndex)->equipments(generalEquipIndex).goods().equipmentdetail().durable();
			long long equipInstanceid = GameView::getInstance()->generalsInLineDetailList.at(generalIndex)->equipments(generalEquipIndex).goods().instanceid();

			if (equipDrug_<=10)
			{
				btnreminRepair->setVisible(true);
				//GameView::getInstance()->repairEquipInstanceIdVector.push_back(equipInstanceid);

				for (int a = 0;a<GameView::getInstance()->generalsInLineList.size();a++)
				{
					if (GameView::getInstance()->generalsInLineDetailList.at(generalIndex)->generalid() == GameView::getInstance()->generalsInLineList.at(a)->id())
					{
						setBackPacSelectIndex(a+1);
						return;
					}
				}
			}
		}
	}
}

void MainScene::showRepairEquipOfBackPack(CCObject * obj)
{
	btnreminRepair->setVisible(false);
	PackageScene * backageLayer = (PackageScene*)this->getChildByTag(kTagBackpack);
	if(backageLayer == NULL)
	{
		PackageScene * backageLayer = PackageScene::create();
		backageLayer->ignoreAnchorPointForPosition(false);
		backageLayer->setAnchorPoint(ccp(0.5f, 0.5f));
		backageLayer->setPosition(ccp(winSize.width/2, winSize.height/2));
		this->addChild(backageLayer,0, kTagBackpack);

		backageLayer->generalList_tableView->selectCell(getBackPacSelectIndex());
	}
}

void MainScene::setBackPacSelectIndex( int generalIndex )
{
	m_backPacGeneralIndex = generalIndex;
}

int MainScene::getBackPacSelectIndex()
{
	return m_backPacGeneralIndex;
}

CCNode * MainScene::addVipInfoByLevelForNode( int level )
{
	if (level <= 0)
		return NULL;


	CCSprite * sprite_vip = CCSprite::create("gamescene_state/zhujiemian3/zhujuetouxiang/vip1.png");
	sprite_vip->setAnchorPoint(ccp(0.5f,0.5f));
	sprite_vip->setPosition(ccp(0,0));

	char s_level[10];
	sprintf(s_level,"%d",level);
	CCLabelBMFont *lbt_vipLevel = CCLabelBMFont::create(s_level,"res_ui/font/ziti_3.fnt");
	lbt_vipLevel->setScale(0.7f);
	lbt_vipLevel->setAnchorPoint(ccp(0.5f,0.5f));
	lbt_vipLevel->setPosition(ccp(25,5));
	sprite_vip->addChild(lbt_vipLevel);

	return sprite_vip;
}

UIWidget * MainScene::addVipInfoByLevelForWidget( int level )
{
	if (level <= 0)
		return NULL;

	UIImageView *ImageView_Vip = UIImageView::create();
	ImageView_Vip->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/vip1.png");
	ImageView_Vip->setAnchorPoint(ccp(0.5f,0.5f));
	ImageView_Vip->setPosition(ccp(0,0));

	UILabelBMFont *lbt_vipLevel = UILabelBMFont::create();
	char s_level[10];
	sprintf(s_level,"%d",level);
	lbt_vipLevel->setText(s_level);
	lbt_vipLevel->setFntFile("res_ui/font/ziti_3.fnt");
	lbt_vipLevel->setScale(0.7f);
	lbt_vipLevel->setAnchorPoint(ccp(0.5f,0.5f));
	lbt_vipLevel->setPosition(ccp(15,-5));
	ImageView_Vip->addChild(lbt_vipLevel);

	return ImageView_Vip;
}

void MainScene::addPrivateChatUi( long long id,std::string name,int countryId,int vipLv,int lv,int pressionId )
{
	CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
	bool ishave = false;
	for (int i=0; i<GameView::getInstance()->chatPrivateSpeakerVector.size();i++ )
	{
		if (id == GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playerid())
		{
			ishave = true;
		}
	}

	CRelationPlayer * relationInfo_ = new CRelationPlayer();
	relationInfo_->set_playerid(id);
	relationInfo_->set_playername(name);
	relationInfo_->set_viplevel(vipLv);
	relationInfo_->set_country(countryId);
	relationInfo_->set_level(lv);
	relationInfo_->set_profession(pressionId);

	if (ishave == false)
	{
		GameView::getInstance()->chatPrivateSpeakerVector.push_back(relationInfo_);
	}

	PrivateChatUi * privateUi = PrivateChatUi::create(0);
	privateUi->ignoreAnchorPointForPosition(false);
	privateUi->setAnchorPoint(ccp(0.5f,0.5f));
	privateUi->setPosition(ccp(winSize.width/2,winSize.height/2));
	privateUi->setTag(ktagPrivateUI);
	this->addChild(privateUi);

	for (int i=0; i<GameView::getInstance()->chatPrivateSpeakerVector.size();i++ )
	{
		if (id == GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playerid())
		{
			privateUi->selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playername();
			privateUi->nameTabview->selectCell(i);
			privateUi->showPrivateContent(privateUi->selectPlayerName);
			break;
		}
	}
}

void MainScene::Refresh()
{
	if (NewCommerStoryManager::getInstance()->IsNewComer())
		return;

	if (GameView::getInstance()->getMapInfo()->mapid() != GameView::getInstance()->getMapInfo()->getLastMapId())
	{
		//只要切地图，就消除慢动作
		CCDirector::sharedDirector()->getScheduler()->setTimeScale(1.0f);

		//delete some ui
		if (this->getChildByTag(kTagOffLineArenaResultUI))
		{
			this->getChildByTag(kTagOffLineArenaResultUI)->removeFromParent();
		}
		if (this->getChildByTag(kTagInstanceEndUI))
		{
			this->getChildByTag(kTagInstanceEndUI)->removeFromParent();
		}
		if (this->getChildByTag(kTagFamilyFightResultUI))
		{
			this->getChildByTag(kTagFamilyFightResultUI)->removeFromParent();
		}
	}

	//退出家族战时清除Myplayer的守卫ID列表
	if(GameView::getInstance()->getMapInfo()->maptype() != com::future::threekingdoms::server::transport::protocol::guildFight)
	{
		GameView::getInstance()->myplayer->getGuardIdList().clear();
	}

	//退出竞技场时清除数据信息
	if(GameView::getInstance()->getMapInfo()->maptype() != com::future::threekingdoms::server::transport::protocol::coliseum)
	{
		MissionAndTeam * missionAndTeam = (MissionAndTeam*)this->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeam)
		{
			missionAndTeam->setArenaDataToDefault();
		}
	}
	//refresh guideMap
	if (GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::coliseum
		||GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::copy
		||GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::guildFight
		||GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::tower)
	{
		Button_mapOpen->setVisible(false);
	}
	else
	{
		Button_mapOpen->setVisible(true);
		//小地图
		if (isMapOpen)
		{
			//Button_mapOpen->setRotation(0);
			Button_mapOpen->getChildByName("jiantou")->setRotation(0);
// 			if (guideMap->getActionLayer())
// 			{
// 				guideMap->getActionLayer()->setVisible(true);
// 			}

		}
		else
		{
			//Button_mapOpen->setRotation(180);
			Button_mapOpen->getChildByName("jiantou")->setRotation(180);
// 			if (guideMap->getActionLayer())
// 			{
// 				guideMap->getActionLayer()->setVisible(false);
// 			}
		}
	}

	GuideMap * guideMap = (GuideMap*)this->getChildByTag(kTagGuideMapUI);
	if (guideMap)
	{
		guideMap->setSceneType(GameView::getInstance()->getMapInfo()->maptype());
		guideMap->RefreshMapName();
	}


	//refresh headMenu,shortCurLayer pos
	switch(GameView::getInstance()->getMapInfo()->maptype())
	{
		case com::future::threekingdoms::server::transport::protocol::coliseum:           //竞技场
			{
				isHeadMenuOn = false;
				//open expLayer
				ExpLayer->setVisible(true);

				HeadMenu * headMenu = (HeadMenu*)this->getChildByTag(kTagHeadMenu);
				if (headMenu)
				{
					headMenu->setPosition(ccp(0,0));

					if (headMenu->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						headMenu->getChildByTag(CCTUTORIALINDICATORTAG)->setVisible(false);
					}

					if (headMenu->getChildByTag(CCTUTORIALPARTICLETAG))
					{
						headMenu->getChildByTag(CCTUTORIALPARTICLETAG)->setVisible(false);
					}
				}
				
				//右下角三角按钮
				TriangleButton * triangleBtn = (TriangleButton*)this->getChildByTag(TAG_TRIANGLEBUTTON);
				if (triangleBtn)
				{
					triangleBtn->setAllOpen();
				}

				//chatWindow down
				chatLayer->setPosition(ccp(0,0));
				//refresh missionAndTeam
				MissionAndTeam * missionAndTeam = (MissionAndTeam*)this->getChildByTag(kTagMissionAndTeam);
				if (missionAndTeam)
				{
					missionAndTeam->setVisible(true);
					missionAndTeam->refreshFunctionBtn();
				}

				DamageStatistics::clear();
			}
			break;
		case com::future::threekingdoms::server::transport::protocol::copy:        //副本
			{
				//refresh missionAndTeam
				MissionAndTeam * missionAndTeam = (MissionAndTeam*)this->getChildByTag(kTagMissionAndTeam);
				if (missionAndTeam)
				{
					missionAndTeam->setVisible(true);
					missionAndTeam->refreshFunctionBtn();
				}

				//delete InstanceMapUI
				InstanceMapUI * instanceMapUI = (InstanceMapUI*)this->getChildByTag(kTagInstanceMapUI);
				if (instanceMapUI)
				{
					instanceMapUI->removeFromParent();
				}
				//delete InstanceDetailUI
				InstanceDetailUI * instanceDetailUI = (InstanceDetailUI*)this->getChildByTag(kTagInstanceDetailUI);
				if(instanceDetailUI)
				{
					instanceDetailUI->removeFromParent();
				}

				DamageStatistics::clear();
			}
			break;
		case com::future::threekingdoms::server::transport::protocol::guildFight:        //家族战
			{
				//refresh missionAndTeam
				MissionAndTeam * missionAndTeam = (MissionAndTeam*)this->getChildByTag(kTagMissionAndTeam);
				if (missionAndTeam)
				{
					missionAndTeam->setVisible(true);
					missionAndTeam->refreshFunctionBtn();
				}

				//delete FamilyUI
				FamilyUI * familyUI = (FamilyUI*)this->getChildByTag(kTagFamilyUI);
				if (familyUI)
				{
					familyUI->removeFromParent();
				}

				DamageStatistics::clear();
			}
			break;
		case com::future::threekingdoms::server::transport::protocol::tower:        //singCopy
			{
				//refresh missionAndTeam  of singCopy
				MissionAndTeam * missionAndTeam = (MissionAndTeam*)this->getChildByTag(kTagMissionAndTeam);
				if (missionAndTeam)
				{
					missionAndTeam->setVisible(true);
					missionAndTeam->refreshFunctionBtn();
				}
			}
			break;
		default:
			{
				//refresh missionAndTeam
				MissionAndTeam * missionAndTeam = (MissionAndTeam*)this->getChildByTag(kTagMissionAndTeam);
				if (missionAndTeam)
				{
					missionAndTeam->setVisible(true);
					missionAndTeam->refreshFunctionBtn();
				}
			}
	}

}

void MainScene::setRemindOpened( bool value_ )
{
	isRemindOpened = value_;
}

bool MainScene::getRemindOpened()
{
	return isRemindOpened;
}

void MainScene::updateOffAreaPrizeTime( float delta )
{
 	long long m_time_ =  getOffAreaPrizeTime()/1000;
 	m_time_ --;
 	setOffAreaPrizeTime(m_time_*1000);
	if (getOffAreaPrizeTime() <= 0)
	{
		setOffAreaPrizeTime(0);
		this->remindOffArenaPrize();
	}
}

void MainScene::setOffAreaPrizeTime( long long tempTime )
{
	m_offAreaPrizeTime = tempTime;
}

long long MainScene::getOffAreaPrizeTime()
{
	return m_offAreaPrizeTime;
}

void MainScene::reloadRemindTabllview()
{
// 	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
// 	if (mainscene_)
// 	{
// 		RemindUi * remindui_ = (RemindUi *)mainscene_->getChildByTag(ktagRemindUi);
// 		if (remindui_)
// 		{
// 			if (remindui_->remind_tableView)
// 			{
// 				remindui_->remind_tableView->reloadData();
// 			}
// 		}
// 	}
}

void MainScene::initGeneralInfoUI()
{
	//add by yangjun 2014.9.26
	GeneralsInfoUI * generalsInfoUI = GeneralsInfoUI::create();
	if (!generalsInfoUI)
		return;

	generalsInfoUI->ignoreAnchorPointForPosition(false);
	generalsInfoUI->setAnchorPoint(ccp(0.5f,0));
	generalsInfoUI->setPosition(ccp(winSize.width/2,0));
	generalHeadLayer->addChild(generalsInfoUI,0,TAG_GENERALSINFOUI);
}

GeneralsInfoUI* MainScene::getGeneralInfoUI()
{
	if (NULL == generalHeadLayer)
	{
		return NULL;
	}

	GeneralsInfoUI * temp = (GeneralsInfoUI*)generalHeadLayer->getChildByTag(TAG_GENERALSINFOUI);
	if(temp)
		return temp;

	return NULL;
}

CCLayer * MainScene::getMainUIElementLayer()
{
	CCLayer * temp = (CCLayer*)this->getChildByTag(kTag_MainUI_Element);
	if (temp)
		return temp;

	return NULL;
}

CCLayer * MainScene::getMainUITuturialLayer()
{
	CCLayer * temp = (CCLayer*)this->getChildByTag(kTag_MainUI_Tutorial);
	if (temp)
		return temp;

	return NULL;
}

CCLayer * MainScene::getMainUIAnimationLayer()
{
	CCLayer * temp = (CCLayer*)this->getChildByTag(kTag_MainUI_Animation);
	if (temp)
		return temp;

	return NULL;
}

void MainScene::addFlowerParticleOnRole(std::string flowerType)
{
	//not use 
}

void MainScene::removeFlowerParticleOnRole()
{
	//not use
}

void MainScene::addFlowerParticleOnScene(std::string flowerType)
{
	//remove flower particle
	this->removeFlowerParticleOnScene();

	std::string m_str = "animation/texiao/particledesigner/hongmeigui.plist";
	//hongmeigui
	if (strcmp("hongmeigui",flowerType.c_str()) ==0)
	{
		m_str = "animation/texiao/particledesigner/hongmeigui.plist";
	}

	if (strcmp("fenmeigui",flowerType.c_str()) ==0)
	{
		m_str = "animation/texiao/particledesigner/fenmeigui.plist";
	}

	if (strcmp("baihe",flowerType.c_str()) ==0)
	{
		m_str = "animation/texiao/particledesigner/baihe.plist";
	}

	CCParticleSystem* particleEffect = CCParticleSystemQuad::create(m_str.c_str());
	particleEffect->setPositionType(kCCPositionTypeGrouped);
	particleEffect->setAnchorPoint(ccp(0.5f,0.5f));
	particleEffect->setPosition(ccp(winSize.width/2,winSize.height));
	particleEffect->setStartSize(40);
	particleEffect->setAutoRemoveOnFinish(true);
	particleEffect->setTag(FLOWERPARTICLEONSCENE_TAG);
	this->addChild(particleEffect);


	CCSequence * seq_ = CCSequence::create(CCDelayTime::create(10),
		CCRemoveSelf::create(),
		NULL);
	particleEffect->runAction(seq_);
	
}

void MainScene::removeFlowerParticleOnScene()
{
	CCParticleSystem* particleEffect = (CCParticleSystemQuad*)this->getChildByTag(FLOWERPARTICLEONSCENE_TAG);
	if (particleEffect != NULL)
	{
		particleEffect->removeFromParentAndCleanup(true);
	}
}

void MainScene::setElementLayerOfTag( int tag_ )
{
	m_addElememtLyaerTag = tag_;
}

int MainScene::getElementLayerOfTag()
{
	return m_addElememtLyaerTag;
}

//////////////////////////////////////////////
RemindUi::RemindUi( void )
{

}

RemindUi::~RemindUi( void )
{

}

RemindUi * RemindUi::create()
{
	RemindUi * remindui = new RemindUi();
	if (remindui && remindui->init())
	{
		remindui->autorelease();
		return remindui;
	}
	CC_SAFE_DELETE(remindui);
	return NULL;
}

bool RemindUi::init()
{
	if (UIScene::init())
	{
		CCSize winsize=CCDirector::sharedDirector()->getVisibleSize();
// 		UIPanel * ppanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/remind_1.json");
// 		ppanel->setAnchorPoint(ccp(0,0));
// 		ppanel->setPosition(ccp(0,0));
// 		m_pUiLayer->addWidget(ppanel);
		
		CCScale9Sprite * sp_ = CCScale9Sprite::create("res_ui/boxed.png");
		sp_->setPreferredSize(CCSizeMake(154,220));
		sp_->setAnchorPoint(ccp(0,0));
		sp_->setPosition(ccp(0,0));
		addChild(sp_);

		remind_tableView = CCTableView::create(this, CCSizeMake(142, 205));
		remind_tableView->setDirection(kCCScrollViewDirectionVertical);
		remind_tableView->setAnchorPoint(ccp(0,0));
		remind_tableView->setPosition(ccp(7,7));
		remind_tableView->setDelegate(this);
		remind_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		remind_tableView->setPressedActionEnabled(true);
		remind_tableView->reloadData();
		this->addChild(remind_tableView);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(154,217));
		return true;
	}
	return false;
}

void RemindUi::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RemindUi::onExit()
{
	UIScene::onExit();
}

void RemindUi::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}

void RemindUi::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void RemindUi::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
	MainScene * mainscene = (MainScene *) GameView::getInstance()->getMainUIScene();
	int cellindex_ = cell->getIdx();
	int uitag =  GameView::getInstance()->remindvector.at(cellindex_);
	if (uitag == KTagSkillScene)
	{
		SkillScene *skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
		if(skillScene == NULL)
		{
			skillScene = SkillScene::create();
			GameView::getInstance()->getMainUIScene()->addChild(skillScene,0,KTagSkillScene);
			skillScene->ignoreAnchorPointForPosition(false);
			skillScene->setAnchorPoint(ccp(0.5f, 0.5f));
			skillScene->setPosition(ccp(winsize.width/2, winsize.height/2));
		}
		//mainscene->checkIsNewRemind();
		mainscene->remindOfSkill();
	}
	
	if (uitag == kTagGeneralsUI)
	{
		GeneralsUI * generalsUI = (GeneralsUI *)MainScene::GeneralsScene;
		GameView::getInstance()->getMainUIScene()->addChild(generalsUI,0,kTagGeneralsUI);
		generalsUI->ignoreAnchorPointForPosition(false);
		generalsUI->setAnchorPoint(ccp(0.5f, 0.5f));
		generalsUI->setPosition(ccp(winsize.width/2, winsize.height/2));

		generalsUI->setToDefaultTab();
		//招募
		for (int i = 0;i<3;++i)
		{
			if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50+i))
			{
				RecuriteActionItem * tempRecurite = ( RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50+i);
				tempRecurite->cdTime -= GameUtils::millisecondNow() - tempRecurite->lastChangeTime;
				/*
				if (i == 0)
				{
					CCLOG("MainScene:tempRecurite->cdTime = %ld",tempRecurite->cdTime);
				}
				*/
			}
		}
		//武将列表 
		GeneralsUI::generalsListUI->isReqNewly = true;
		GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
		temp->page = 0;
		temp->pageSize = generalsUI->generalsListUI->everyPageNum;
		temp->type = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
		delete temp;
		//技能列表
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5055, this);
		//阵法列表
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5071, this);
		//已上阵武将列表 
		GeneralsUI::ReqListParameter * temp1 = new GeneralsUI::ReqListParameter();
		temp1->page = 0;
		temp1->pageSize = generalsUI->generalsListUI->everyPageNum;
		temp1->type = 5;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp1);
		delete temp1;
		//mainscene->checkIsNewRemind();
		mainscene->remindOfGeneral();
	}

	if (uitag ==  kTagOffLineArenaUI)
	{
		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
		OffLineArenaUI * offLineArenaUI =OffLineArenaUI::create();
		offLineArenaUI->ignoreAnchorPointForPosition(false);
		offLineArenaUI->setAnchorPoint(ccp(0.5f,0.5f));
		offLineArenaUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		offLineArenaUI->setTag(kTagOffLineArenaUI);
		GameView::getInstance()->getMainUIScene()->addChild(offLineArenaUI);
	}

	if (uitag == kTagMissionScene)
	{
		MissionScene *missionMainLayer = (MissionScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionScene);
		if(missionMainLayer == NULL)
		{
			missionMainLayer = MissionScene::create();
			GameView::getInstance()->getMainUIScene()->addChild(missionMainLayer,0,kTagMissionScene);
			missionMainLayer->ignoreAnchorPointForPosition(false);
			missionMainLayer->setAnchorPoint(ccp(0.5f, 0.5f));
			missionMainLayer->setPosition(ccp(winsize.width/2, winsize.height/2));

			missionMainLayer->mainTab->setDefaultPanelByIndex(1);
			missionMainLayer->updataCurDataSourceByMissionState(0);
			//设置默认
			if (missionMainLayer->curMissionDataSource.size()>0)
			{
				missionMainLayer->curMissionInfo->CopyFrom(*missionMainLayer->curMissionDataSource.at(0));
			}
			//reload
			missionMainLayer->m_tableView->reloadData();
			missionMainLayer->ReloadMissionInfoData();
		}
		mainscene->remindMission();
	}
	/*
	if (uitag == kTagSignDailyUI)
	{
		SignDailyUI * pSignDailyUI = SignDailyUI::create();
		pSignDailyUI->ignoreAnchorPointForPosition(false);
		pSignDailyUI->setAnchorPoint(ccp(0.5f, 0.5f));
		pSignDailyUI->setPosition(ccp(winsize.width / 2, winsize.height / 2));
		pSignDailyUI->setTag(kTagSignDailyUI);
		GameView::getInstance()->getMainUIScene()->addChild(pSignDailyUI);

		pSignDailyUI->refreshUI();
	}
	if (uitag == kTagVipEveryDayRewardsUI)
	{
		CCLayer *vipEveryDayRewardScene = (CCLayer*)mainscene->getChildByTag(kTagVipEveryDayRewardsUI);
		if(vipEveryDayRewardScene == NULL)
		{
			VipEveryDayRewardsUI * vpEveryDayRewardsUI = VipEveryDayRewardsUI::create();
			vpEveryDayRewardsUI->ignoreAnchorPointForPosition(false);
			vpEveryDayRewardsUI->setAnchorPoint(ccp(0.5f,0.5f));
			vpEveryDayRewardsUI->setPosition(ccp(winsize.width/2, winsize.height/2));
			mainscene->addChild(vpEveryDayRewardsUI,0,kTagVipEveryDayRewardsUI);
		}
	}
	if (uitag == kTagOffLineExpUI)
	{
		OffLineExpUI *offLineExpUI = (OffLineExpUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineExpUI);
		if(offLineExpUI == NULL)
		{
			offLineExpUI = OffLineExpUI::create();
			GameView::getInstance()->getMainUIScene()->addChild(offLineExpUI,0,kTagOffLineExpUI);
			offLineExpUI->ignoreAnchorPointForPosition(false);
			offLineExpUI->setAnchorPoint(ccp(0.5f, 0.5f));
			offLineExpUI->setPosition(ccp(winsize.width/2, winsize.height/2));
		}
	}

	//ktag 100 初始牛刀 小有名气 建功立业  400 定点领取体力
	if (uitag == REMIND_SINGCOPYGETREWARD_CHUSHINIUDAO)
	{
		RankRewardUi * ranklist_ = RankRewardUi::create(1);
		ranklist_->ignoreAnchorPointForPosition(false);
		ranklist_->setAnchorPoint(ccp(0.5f,0.5f));
		ranklist_->setPosition(ccp(winsize.width/2,winsize.height/2));
		ranklist_->setTag(ktagSingCopyReward);
		GameView::getInstance()->getMainUIScene()->addChild(ranklist_);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1905,(void *)1);
	}
	
	if (uitag == REMIND_SINGCOPYGETREWARD_XIAOYOUMINGQI)
	{
		RankRewardUi * ranklist_ = RankRewardUi::create(2);
		ranklist_->ignoreAnchorPointForPosition(false);
		ranklist_->setAnchorPoint(ccp(0.5f,0.5f));
		ranklist_->setPosition(ccp(winsize.width/2,winsize.height/2));
		ranklist_->setTag(ktagSingCopyReward);
		GameView::getInstance()->getMainUIScene()->addChild(ranklist_);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1905,(void *)2);
	}

	if (uitag == REMIND_SINGCOPYGETREWARD_JIANGONGLEYE)
	{
		RankRewardUi * ranklist_ = RankRewardUi::create(3);
		ranklist_->ignoreAnchorPointForPosition(false);
		ranklist_->setAnchorPoint(ccp(0.5f,0.5f));
		ranklist_->setPosition(ccp(winsize.width/2,winsize.height/2));
		ranklist_->setTag(ktagSingCopyReward);
		GameView::getInstance()->getMainUIScene()->addChild(ranklist_);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1905,(void *)3);
	}
	
	if (uitag == REMIND_GETPHYSICAL)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5093);
	}

	if (uitag == REMIND_GETPRIZE_OFFAREA)
	{
		OffLineArenaUI * offLineArenaUI =OffLineArenaUI::create();
		offLineArenaUI->ignoreAnchorPointForPosition(false);
		offLineArenaUI->setAnchorPoint(ccp(0.5f,0.5f));
		offLineArenaUI->setPosition(ccp(winsize.width/2,winsize.height/2));
		offLineArenaUI->setTag(kTagOffLineArenaUI);
		GameView::getInstance()->getMainUIScene()->addChild(offLineArenaUI);
	}

	if (uitag == REMIND_FRIENDPHYPOWER)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5132);
	}
	
	if (uitag == kTagBattleAchievementUI)
	{
		BattleAchievementUI *tmpBattleAchUI = (BattleAchievementUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBattleAchievementUI);
		if (NULL == tmpBattleAchUI)
		{
			CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

			BattleAchievementUI* battleAchUI = BattleAchievementUI::create();
			battleAchUI->ignoreAnchorPointForPosition(false);
			battleAchUI->setAnchorPoint(ccp(0.5f, 0.5f));
			battleAchUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
			battleAchUI->setTag(kTagBattleAchievementUI);

			GameView::getInstance()->getMainUIScene()->addChild(battleAchUI);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5101);
		}
	}
	*/
	this->removeFromParentAndCleanup(true);
}

cocos2d::CCSize RemindUi::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(140,50);
}

cocos2d::extension::CCTableViewCell* RemindUi::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString *string =CCString::createWithFormat("%d",idx);
	CCTableViewCell * cell=table->dequeueCell();
	cell=new CCTableViewCell();
	cell->autorelease();

	CCScale9Sprite * imageBackGround = CCScale9Sprite::create("res_ui/new_button_11.png");
	imageBackGround->setPreferredSize(CCSizeMake(140,50));
	imageBackGround->setCapInsets(CCRect(15,21,1,1));
	imageBackGround->setAnchorPoint(ccp(0.5f,0.5f));
	imageBackGround->setPosition(ccp(70,25));
	cell->addChild(imageBackGround);

	CCLabelTTF *label_playName;
	const char *strings_;
	int layerTag =  GameView::getInstance()->remindvector.at(idx);
	if (layerTag ==KTagSkillScene )
	{
		strings_  = StringDataManager::getString("remindOfSkill");
	}
	if (layerTag == kTagGeneralsUI)
	{
		strings_  = StringDataManager::getString("remindOfGeneral");
	}

	if (layerTag == kTagOffLineArenaUI)
	{
		strings_  = StringDataManager::getString("remindOfArena");
	}

	if (layerTag == kTagMissionScene)
	{
		strings_  = StringDataManager::getString("remindOfMission");
	}

// 	if (layerTag == kTagSignDailyUI)
// 	{
// 		strings_  = StringDataManager::getString("btn_qiandao");
// 	}
// 	if (layerTag == kTagVipEveryDayRewardsUI)
// 	{
// 		strings_  = StringDataManager::getString("remindOfVipReward");
// 	}
// 	if (layerTag == kTagOffLineExpUI)
// 	{
// 		strings_  = StringDataManager::getString("remindOfflineExp");
// 	}

	//ktag 100 初始牛刀 小有名气 建功立业  400 定点领取体力
// 	if (layerTag == REMIND_SINGCOPYGETREWARD_CHUSHINIUDAO)
// 	{
// 		strings_  = StringDataManager::getString("remindSingCopyGetPrize");
// 	}
// 
// 	if (layerTag == REMIND_SINGCOPYGETREWARD_XIAOYOUMINGQI)
// 	{
// 		strings_  = StringDataManager::getString("remindSingCopyGetPrize");
// 	}
// 
// 	if (layerTag == REMIND_SINGCOPYGETREWARD_JIANGONGLEYE)
// 	{
// 		strings_  = StringDataManager::getString("remindSingCopyGetPrize");
// 	}
	
// 	if (layerTag == REMIND_GETPHYSICAL)
// 	{
// 		strings_  = StringDataManager::getString("remindgetPrizephysice");
// 	}

// 	if (layerTag == REMIND_GETPRIZE_OFFAREA)
// 	{
// 		strings_  = StringDataManager::getString("remindOffArenaPrize");
// 	}

// 	if (layerTag == REMIND_FRIENDPHYPOWER)
// 	{
// 		strings_  = StringDataManager::getString("remindFriendPhyPower");
// 	}

// 	if (layerTag == kTagBattleAchievementUI)
// 	{
// 		strings_  = StringDataManager::getString("remindBattleAchievement");
// 	}

	label_playName = CCLabelTTF::create(strings_, APP_FONT_NAME, 20);
	label_playName->setAnchorPoint(ccp(0.5f,0.5f));
	label_playName->setPosition(ccp(70,25));
	cell->addChild(label_playName,10);
	return cell;
}

unsigned int RemindUi::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	return GameView::getInstance()->remindvector.size();
}

bool RemindUi::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return resignFirstResponder(pTouch,this,false);
}
