#ifndef __HTTP_LOGIN_H__
#define __HTTP_LOGIN_H__

#include <vector>
#include <string>

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "../GameStateBasic.h"
#include "../messageclient/ClientNetEngine.h"
#include "../messageclient/reqsender/ReqSenderProtocol.h"
#include "../ui/extensions/RichTextInput.h"

#define THREEKINGDOMS_LOGINSERVER_STUDIO "139.99.49.200"
#define THREEKINGDOMS_LOGINSERVER_WANMEI "124.202.137.33"
#define THREEKINGDOMS_LOGINSERVER_WANMEI2 "124.202.137.34"
#define THREEKINGDOMS_LOGINSERVER_OFFICIAL "210.14.129.115"
#define THREEKINGDOMS_LOGINSERVER_OFFICIAL2 "103.244.81.169"
#define THREEKINGDOMS_LOGINSERVER_OFFICIAL3 "ios-zjqj.autopatch.173.com"


class Login : public cocos2d::CCLayer
{
public:
    Login();
    virtual ~Login();
    
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
    static Login * s_login;
    static Login * getInstance();
#endif

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
	virtual void onSocketClose(ClientNetEngine* socketEngine);
	virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

    void toExtensionsMainLayer(cocos2d::CCObject *sender);
    	
//	static long long userId;
    static long long userId;

	static std::string sessionId;

    //Menu Callbacks
    void onMenuGetTestClicked();
    //Http Response Callback
    void onHttpRequestCompleted(cocos2d::extension::CCHttpClient *sender, cocos2d::extension::CCHttpResponse *response);

	void onMenuGetRegisterClicked(cocos2d::CCObject *sender);
	void onHttpRequestRegister(CCHttpClient *sender, CCHttpResponse *response);
	void onMenuClear(cocos2d::CCObject *sender);

	void onMenuGetLoginClicked(cocos2d::CCObject *sender);
	void onHttpRequestLogin(CCHttpClient *sender, CCHttpResponse *response);
    
    void onHttpRequestLoginGuest(CCHttpClient *sender, CCHttpResponse *response);

	virtual void onEnter();
    //virtual void onExit();

	static void getServerListInfo(ServerListRsp* httpRsp);
    
	void gameLogRunAction(CCNode* pTarget);


    // 鐧诲綍淇℃伅浼犻€?    
	void getLoginInfo(std::string strAccount, std::string strLoginUin, std::string strSessionId);
    void getLoginInfo(std::string strAccount, std::string strSessionId);
    // 鐧诲綍淇℃伅浼犻€掞紙鍖呭惈娓稿璐﹀彿锛?    
	void getGuestLoginInfo(std::string strOfficalAccount, std::string strGuestAccount, std::string strLoginUin, std::string strSessionId);
    
	//static int getUserId();
	static ServerListRsp* m_pServerListRsp;
	RichTextInputBox * pAccountInputBox;
	RichTextInputBox * pPinInputBoxr;
    
    static std::string s_loginserver_ip;

private:
	void onVersionCheck();
	void onVersionCheckCompleted(CCHttpClient *sender, CCHttpResponse *response);
private:
    cocos2d::CCLabelTTF* m_labelStatusCode;

	//int m_ilastServerId;
	//std::vector<ServerInfo> m_vServerInfo;
	//int m_iServerInfoNum;
    
   
};

class LoginGameState : public GameState
{
public:
	~LoginGameState();

	virtual void runThisState();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
	virtual void onSocketClose(ClientNetEngine* socketEngine);
	virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

};


//void runLogin();

#endif //__HTTPREQUESTHTTP_H
