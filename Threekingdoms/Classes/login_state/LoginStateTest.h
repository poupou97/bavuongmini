#ifndef _GAMESTATE_LOGINSTATETEST_STATE_H_
#define _GAMESTATE_LOGINSTATETEST_STATE_H_

#include "cocos2d.h"
#include "../GameStateBasic.h"
#include "../messageclient/ClientNetEngine.h"

USING_NS_CC;

/**
 login to the game server
 */
class LoginTestLayer : public CCLayer, public ClientNetEngine::Delegate
{
public:
    LoginTestLayer();
	virtual ~LoginTestLayer();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

	virtual void update(float dt);

	virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);

	inline int getSelectedRoleId() { return mSelectedId; };

private:
	void menuSelectRoleCallback(CCObject* pSender);
	void menuSelectServerCallback(CCObject* pSender);

	void connectGameServer();

private:
	int m_state;

	struct timeval m_sStartTime;
	double m_dStartTime;

	int mSelectedId;

	int mCurrentServerId;
	
};

class LoginTestState : public GameState
{
public:
	~LoginTestState();

    virtual void runThisState();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);
};

#endif // _GAMESTATE_LOGIN_STATE_H_
