#ifndef _GAMESTATE_MOUTAINBACKGROUNDLAYER_STATE_H_
#define _GAMESTATE_MOUTAINBACKGROUNDLAYER_STATE_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;

/**
  * the background layer which scrolls from right to left
 */
class MoutainBackGroundLayer : public CCLayerRGBA
{
public:
    MoutainBackGroundLayer();
	virtual ~MoutainBackGroundLayer();

	virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);
	virtual void ccTouchesBegan(CCSet *pTouches, CCEvent *pEvent);
	virtual void ccTouchesMoved(CCSet *pTouches, CCEvent *pEvent);

	static MoutainBackGroundLayer* create();
	bool init();

	void setBackGroundColor(const ccColor3B& color3);

private:
	void addBirds(CCNode* parent);
	void addWaterfall1(CCNode* parent, CCNode* next);
	void addWaterfall2(CCNode* parent, CCNode* next);
	void addBuddhaLight(CCNode* parent, CCNode* next);
	CCParticleSystem* generateSmokeEffect();
	void addSmoke1(CCNode* parent, CCNode* next);
	void addSmoke2(CCNode* parent, CCNode* next);

	void BG1_scroll();
	void BG1_repeatFunc();

	void BG2_scroll();
	void BG2_repeatFunc();

	void BG3_scroll();
	void BG3_repeatFunc();

	void BG4_scroll();
	void BG4_repeatFunc();
};

#endif
