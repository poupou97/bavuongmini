#include "Login.h"
#include <string>

#include "LoginState.h"
#include "../GameView.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "../messageclient/reqsender/ReqSenderProtocol.h"
#include "../ui/extensions/RichTextInput.h"
#include "SimpleAudioEngine.h"
#include "../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../GameUserDefault.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../utils/GameUtils.h"
#include "GameAudio.h"
#include "MoutainBackGroundLayer.h"

#include "../messageclient/GameMessageProcessor.h"
#include "../messageclient/ClientNetEngine.h"
#include "DefaultServer.h"

#define IS_CLEAR_ENABLE 0
#define CLIP_TIME 3.0f
#define DELAY_TIME 15.0f

using namespace CocosDenshion;

USING_NS_CC;
USING_NS_CC_EXT;

ServerListRsp* Login::m_pServerListRsp = new ServerListRsp();
long long Login::userId = 1;
string Login::sessionId = "";
//std::string Login::s_loginserver_ip = THREEKINGDOMS_LOGINSERVER_OFFICIAL;
std::string Login::s_loginserver_ip = THREEKINGDOMS_LOGINSERVER_STUDIO;

Login::Login() 
: m_labelStatusCode(NULL)
{
	//stop the backgroundmusic when the Socket Closed         by:liutao  2014-1-23
	//SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	SimpleAudioEngine::sharedEngine()->stopAllEffects();
	GameUtils::playGameSound(MUSIC_KING, 1, true);

    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

	GameView::getInstance()->ResetData();

    const int MARGIN = 40;
    const int SPACE = 35;

	CCLayer* pBackGround = MoutainBackGroundLayer::create();
	addChild(pBackGround);
	
// 	CCScale9Sprite* p = CCScale9Sprite::create("res_ui/zhezhao80.png");
// 	p->setPosition(ccp(winSize.width/2,winSize.height/2));
// 	p->setPreferredSize(winSize);
// 	p->setAnchorPoint(ccp(0.5f, 0.5f));
// 	addChild(p);
	//-------------
	/*
	//gamelogo
	CCSprite* pSpriteLogo = CCSprite::create("res_ui/select_the_sercer/gamelogo.png");
	pSpriteLogo->setPosition(ccp(winSize.width/2+10, winSize.height/2+100));
	pSpriteLogo->setAnchorPoint(ccp(0.5f, 0.5f));
	pSpriteLogo->setScale(0.5f);
	pSpriteLogo->setOpacity(64);
	addChild(pSpriteLogo, 1);

	//yanjing
	CCSprite* pSpriteYanjing = CCSprite::create("images/yanjing.png");
	pSpriteYanjing->setPosition(ccp(pSpriteLogo->getContentSize().width/2+5, pSpriteLogo->getContentSize().height - 38));
	pSpriteYanjing->setAnchorPoint(ccp(0.5f, 0.5f));
	pSpriteYanjing->setScaleX(2.0f);
	pSpriteYanjing->setScaleY(1.16f);
	pSpriteLogo->addChild(pSpriteYanjing,1);

	CCFadeTo*  action1 = CCFadeTo::create(1.2f,30);
	CCDelayTime * delay_1 = CCDelayTime::create(1.5f);
	CCFadeTo*  action2 = CCFadeTo::create(1.2f,255);
	CCDelayTime * delay_2 = CCDelayTime::create(0.6f);
	CCRepeatForever * repeapAction = CCRepeatForever::create(CCSequence::create(action1,delay_1,action2,delay_2,NULL));
	pSpriteYanjing->runAction(repeapAction);

	//runaction
	CCActionInterval*  action_scale_and_fadein = CCSpawn::create(
		CCFadeTo::create(1.1f,255),
		CCScaleTo::create(1.1f,1.0f),
		NULL);
	CCFiniteTimeAction*  gameLog_scale_1 = CCSequence::create(
		//CCScaleTo::create(1.5f,1.0f),
		action_scale_and_fadein,
		CCCallFuncN::create( this, callfuncN_selector(Login::gameLogRunAction) ), 
		NULL);
	pSpriteLogo->runAction(gameLog_scale_1);
	
	*/
	//-------------
	LoginState::addAppVersionInfo(this);

	// reset the delta time for smooth particle animation
	// because the previous scene is a loading scene, the delta time is stop when loading resource
	CCDirector::sharedDirector()->setNextDeltaTimeZero(true);

	UILayer* layer= UILayer::create();

	cocos2d::extension::UIButton *enter = cocos2d::extension::UIButton::create();
	enter->setAnchorPoint(ccp(0.5f, 0.5f));
	enter->setTouchEnable(true);
	enter->loadTextures("res_ui/creating_a_role/button_di.png", "res_ui/creating_a_role/button_di.png", "");
	enter->setPosition(ccp(-60,-150));
	enter->addReleaseEvent(this, coco_releaseselector(Login::onMenuGetLoginClicked));
	enter->setPressedActionEnabled(true);
	
	cocos2d::extension::UIImageView * loginText = cocos2d::extension::UIImageView::create();
	loginText->setTexture("res_ui/first/button_denglu.png");
	loginText->setVisible(true);
	//loginText->setAnchorPoint(ccp(0,0));
	//loginText->setPosition(ccp(2,3));
	enter->addChild(loginText);

	cocos2d::extension::UIButton *buttonRegister = cocos2d::extension::UIButton::create();
	buttonRegister->setAnchorPoint(ccp(0.5f, 0.5f));
	buttonRegister->setTouchEnable(true);
	buttonRegister->loadTextures("res_ui/creating_a_role/button_di.png", "res_ui/creating_a_role/button_di.png", "");
	buttonRegister->setPosition(ccp(60,-150));
	buttonRegister->addReleaseEvent(this, coco_releaseselector(Login::onMenuGetRegisterClicked));
	buttonRegister->setPressedActionEnabled(true);
	
	cocos2d::extension::UIImageView * registerImage = cocos2d::extension::UIImageView::create();
	registerImage->setTexture("res_ui/first/button_zhuce.png");
	registerImage->setVisible(true);
	buttonRegister->addChild(registerImage);

#if IS_CLEAR_ENABLE
	// clear account and password
	UIButton *buttonClear = UIButton::create();
    buttonClear->setTouchEnable(true);
    buttonClear->loadTextures("res_ui/new_button_7.png", "res_ui/new_button_7.png", "");
	buttonClear->setPosition(ccp(150,-50));
	buttonClear->addReleaseEvent(this, coco_releaseselector(Login::onMenuClear));
	buttonClear->setPressedActionEnabled(true);

	UIImageView * deleteImage = UIImageView::create();
	deleteImage->setTexture("res_ui/liaotian/delete.png");
	deleteImage->setVisible(true);
	buttonClear->addChild(deleteImage);
	layer->addWidget(buttonClear);
#endif

    layer->addWidget(enter);
	layer->addWidget(buttonRegister);
	layer->setPosition(ccp(winSize.width/2,winSize.height/2));
	addChild(layer);

	ccColor3B shadowColor = ccc3(0,0,0);   // black

	CCLabelTTF * account = CCLabelTTF::create("账号", APP_FONT_NAME, 20);
	account->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	account->setPosition(ccp(-130,-30));
	account->setColor(ccc3(255,255,255));
	account->enableShadow(CCSizeMake(2.0, -2.0), 0.5, 0.0, shadowColor);
	layer->addChild(account);

	CCLabelTTF * password = CCLabelTTF::create("密码", APP_FONT_NAME, 20);
	password->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	password->setPosition(ccp(-130,-80));
	password->setColor(ccc3(255,255,255));
	password->enableShadow(CCSizeMake(2.0, -2.0), 0.5, 0.0, shadowColor);
	layer->addChild(password);
	
	cocos2d::extension::UIImageView *inputBox=cocos2d::extension::UIImageView::create();
	inputBox->setTexture("res_ui/liaotian/shurukuang.png");
	inputBox->setAnchorPoint(ccp(0.5f,0.5f));
	inputBox->setPosition(ccp(0,-30));
	inputBox->setScale9Enable(true);
	inputBox->setScale9Size(CCSizeMake(200,34));
	layer->addWidget(inputBox);

	cocos2d::extension::UIImageView *inputPin=cocos2d::extension::UIImageView::create();
	inputPin->setTexture("res_ui/liaotian/shurukuang.png");
	inputPin->setAnchorPoint(ccp(0.5f,0.5f));
	inputPin->setPosition(ccp(0,-80));
	inputPin->setScale9Enable(true);
	inputPin->setScale9Size(CCSizeMake(200,34));
	layer->addWidget(inputPin);
	
	pAccountInputBox = new RichTextInputBox();
	pAccountInputBox->setCharLimit(20);
	pAccountInputBox->setInputBoxWidth(200);
	
	pAccountInputBox->setPosition(ccp(-100,-45));
	layer->addChild(pAccountInputBox);
	pAccountInputBox->autorelease();

	pPinInputBoxr = new RichTextInputBox();
	pPinInputBoxr->setCharLimit(20);
	pPinInputBoxr->setInputBoxWidth(200);
	
	pPinInputBoxr->setPosition(ccp(-100,-95));
	layer->addChild(pPinInputBoxr);
	pPinInputBoxr->autorelease();
}

Login::~Login()
{
    //CCHttpClient::getInstance()->destroyInstance();
}

void Login::onEnter()
{
	CCLayer::onEnter();

	string account = CCUserDefault::sharedUserDefault()->getStringForKey("account");
	string password = CCUserDefault::sharedUserDefault()->getStringForKey("password");
	if(NULL == pAccountInputBox->getInputString())
	{
		pAccountInputBox->onTextFieldInsertText(NULL, account.c_str(), account.size());
	}
	if(NULL == pPinInputBoxr->getInputString())
	{
		pPinInputBoxr->onTextFieldInsertText(NULL, password.c_str(), password.size());
	}
}

void Login::onVersionCheck()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = Login::s_loginserver_ip;
		//std::string url = "192.168.1.122";
        url.append(":48688/versioncheck");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(Login::onVersionCheckCompleted));

		VersionCheckReq httpReq;
		httpReq.set_clienttype("");
		httpReq.set_clientversion(GAME_CODE_VERSION);
		CCLOG(GAME_CODE_VERSION);
		int resVersion = CCUserDefault::sharedUserDefault()->getIntegerForKey(KEY_OF_RES_VERSION, GAME_RES_VERSION);
		httpReq.set_resversion(resVersion);
		httpReq.set_channelid(0);
		httpReq.set_batchid(0);
		httpReq.set_recommendid(0);
		httpReq.set_extensionfield("");
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
		httpReq.set_platform("");
#else
		httpReq.set_platform(OPERATION_PLATFORM);
#endif

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void Login::onVersionCheckCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());

        return;
    }
    
	VersionCheckRsp versionCheckRsp;
	if(response->getResponseData()->size() <= 0)
	{
		versionCheckRsp.set_result(1);   // server no response, so the default value is 1, indicate that there is no update
	}
	else
	{
		//CCAssert(response->getResponseData()->size() > 0, "should not be empty");
		versionCheckRsp.ParseFromString(response->getResponseData()->data());

		CCLOG("response.result = %d", versionCheckRsp.result());
	}

	int result = versionCheckRsp.result();
	if(1 == result||3 == result)
	{
		onMenuGetTestClicked();
	}
	else if(2 == result)
	{
		GameView::getInstance()->showAlertDialog("请下载最新客户端");
	}
}

void Login::onMenuGetTestClicked()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = s_loginserver_ip;
        url.append(":48688/serverlist");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestCompleted));

		ServerListReq httpReq;
		httpReq.set_userid(userId);
		httpReq.set_sessionid(sessionId);

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void Login::onHttpRequestCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	m_pServerListRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pServerListRsp->result());
	/*
	CCLOG("m_pServerListRsp->serverinfos().roleid() = %d",m_pServerListRsp->serverinfos(0).roleid());
	CCLOG("response.serverinfos_size = %d", m_pServerListRsp->serverinfos_size());
	CCLOG("m_pServerListRsp->serverinfos(0).has_roleid() = %d",m_pServerListRsp->serverinfos(0).has_roleid());
	CCLOG("m_pServerListRsp->serverinfos(0).roleid() = %lld\n",m_pServerListRsp->serverinfos(0).roleid());*/
	if(1 == m_pServerListRsp->result())
	{
		GameView::getInstance()->registerNoticeIsShow = true;

		GameView::getInstance()->registerNoticeVector.clear();

		GameView::getInstance()->registerNoticeVector.push_back(m_pServerListRsp->loginnotice());

		GameState* pScene = new DefaultServerState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
	else
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_server_list"));
	}
}

void Login::onMenuGetLoginClicked(cocos2d::CCObject *sender)
{    
	if(NULL == pAccountInputBox->getInputString())
	{
        GameView::getInstance()->showAlertDialog("帐号不能为空");
		return;
	}
	else
	{
		CCUserDefault::sharedUserDefault()->setStringForKey("account", pAccountInputBox->getInputString());
	}
	if(NULL == pPinInputBoxr->getInputString())
	{
        GameView::getInstance()->showAlertDialog("密码不能为空");
		return;
	}
	else
	{
		CCUserDefault::sharedUserDefault()->setStringForKey("password", pPinInputBoxr->getInputString());
	}

    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = s_loginserver_ip;
		url.append(":48688/login");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestLogin));

		std::string strUniqueId = GameUtils::getAppUniqueID();

		LoginReq httpReq;
		httpReq.set_account(pAccountInputBox->getInputString());
		httpReq.set_authenticid(pPinInputBoxr->getInputString());
		//useless element
		httpReq.set_accountnum(0);
		httpReq.set_accountpt("test");
		httpReq.set_imei(strUniqueId);
		httpReq.set_ua(strUniqueId);
		httpReq.set_channelid(LoginReq::QIANLIANG);
		httpReq.set_batchid(0);
		httpReq.set_recommendid(0);
		httpReq.set_systemversion("test");
		httpReq.set_nettype(0);
		httpReq.set_apkwithres(0);
		httpReq.set_platformuserid("test");

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void Login::onHttpRequestLogin(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	LoginRsp* m_pRegisterRsp = new LoginRsp();
	m_pRegisterRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pRegisterRsp->result());

	if(1 == m_pRegisterRsp->result())
	{
		userId = m_pRegisterRsp->userid();
		sessionId = m_pRegisterRsp->sessionid();
		onVersionCheck();
	}
	else
	{
		GameView::getInstance()->showAlertDialog(m_pRegisterRsp->resultmessage());
	}
}


void Login::onMenuClear(cocos2d::CCObject *sender)
{
	pAccountInputBox->deleteAllInputString();
	pPinInputBoxr->deleteAllInputString();
}

void Login::onMenuGetRegisterClicked(cocos2d::CCObject *sender)
{    
	if(NULL == pAccountInputBox->getInputString())
	{
        GameView::getInstance()->showAlertDialog("帐号不能为空");
		return;
	}
	if(NULL == pPinInputBoxr->getInputString())
	{
        GameView::getInstance()->showAlertDialog("密码不能为空");
		return;
	}
    

    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = s_loginserver_ip;
		url.append(":48688/auth_regist");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestRegister));

		AuthRegistReq httpReq;
		httpReq.set_account(pAccountInputBox->getInputString());
		httpReq.set_password(pPinInputBoxr->getInputString());

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void Login::onHttpRequestRegister(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	AuthRegistRsp* m_pLoginRsp = new AuthRegistRsp();
	m_pLoginRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pLoginRsp->result());

	if(1 == m_pLoginRsp->result())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_register_success"));
	}
	else
	{
		GameView::getInstance()->showAlertDialog(m_pLoginRsp->resultmessage());
	}
}


void Login::getServerListInfo(ServerListRsp* httpRsp)
{
	*httpRsp = *m_pServerListRsp;
}

void Login::onSocketOpen(ClientNetEngine* socketEngine)
{
}
void Login::onSocketClose(ClientNetEngine* socketEngine)
{
}
void Login::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

void Login::gameLogRunAction(CCNode* pTarget)
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();

// 	CCSprite* pSpriteLogo_copy = CCSprite::create("res_ui/select_the_sercer/gamelogo.png");
// 	pSpriteLogo_copy->setPosition(ccp(winSize.width/2+10, winSize.height/2+100));
// 	pSpriteLogo_copy->setAnchorPoint(ccp(0.5f, 0.5f));
// 	addChild(pSpriteLogo_copy, 1);
// 
// 	CCDelayTime * gameLogCopy_delay_1 = CCDelayTime::create(1.0f);
// 	CCAction*  action_ss = CCSpawn::create(
// 		CCFadeTo::create(1.0f,0),
// 		CCScaleTo::create(1.0f,1.3f),
// 		NULL);
// 
// 	CCSequence * gameLog_seq = CCSequence::create(gameLogCopy_delay_1,action_ss,NULL);
// 	pSpriteLogo_copy->runAction(action_ss);

	CCClippingNode* clip = CCClippingNode::create();//创建裁剪节点
	CCSprite* gameTitle = CCSprite::create("res_ui/select_the_sercer/gamelogo_text_mask.png");
	clip->setStencil(gameTitle);//设置裁剪模板
	clip->setAlphaThreshold(0.0f);//设置透明度阈值
	//clip->setInverted(true);
	clip->setContentSize(CCSize(gameTitle->getContentSize().width,gameTitle->getContentSize().height));//设置裁剪节点大小    
	CCSize clipSize = clip->getContentSize();//获取裁剪节点大小
	clip->setPosition(ccp(winSize.width/2+10,winSize.height/2+100));//设置裁剪节点位置


// 	//CCSprite* gameTitle_show = CCSprite::create("res_ui/select_the_sercer/gamelogo.png");//创建要显示的对象
// 	CCSprite* spark = CCSprite::create("images/guangtiao.png");//创建闪亮精灵
// 	//clip->addChild(gameTitle_show,2);//把要显示的内容放在裁剪节点中，其实可以直接clip->addChild(gameTitle,1);此处为了说明更改显示内容
// 	spark->setPosition(ccp(-winSize.width/2, 0));//设置闪亮精灵位置
// 	clip->addChild(spark,3);//添加闪亮精灵到裁剪节点
// 	addChild(clip,1);//添加裁剪节点
// 
// 	CCMoveTo* moveAction = CCMoveTo::create(CLIP_TIME, ccp(clipSize.width, 0));//创建精灵节点的动作
// 	CCMoveTo* moveBack = CCMoveTo::create(0, ccp(-clipSize.width, 0));
// 	CCDelayTime* delayTime = CCDelayTime::create(DELAY_TIME);
// 	CCSequence* seq = CCSequence::create(moveAction, delayTime, moveBack, NULL);
// 	CCRepeatForever* repreatAction = CCRepeatForever::create(seq);
// 	spark->runAction(repreatAction);//精灵节点重复执行
}


/*
void runLogin()
{
    CCScene *pScene = CCScene::create();
    Login *pLayer = new Login();
    pScene->addChild(pLayer);
    
    CCDirector::sharedDirector()->replaceScene(pScene);
    pLayer->release();
}
*/

enum {
	kTagMainLayer = 1,
};

LoginGameState::~LoginGameState()
{
}

void LoginGameState::runThisState()
{
	CCLayer* pLayer = new Login();
	addChild(pLayer, 0, kTagMainLayer);
	pLayer->release();

	if(CCDirector::sharedDirector()->getRunningScene() != NULL)
		CCDirector::sharedDirector()->replaceScene(this);
	else
		CCDirector::sharedDirector()->runWithScene(this);
}

void LoginGameState::onSocketOpen(ClientNetEngine* socketEngine)
{
}
void LoginGameState::onSocketClose(ClientNetEngine* socketEngine)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	Login* layer = (Login*)this->getChildByTag(kTagMainLayer);
	layer->onSocketClose(socketEngine);
}
void LoginGameState::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}
