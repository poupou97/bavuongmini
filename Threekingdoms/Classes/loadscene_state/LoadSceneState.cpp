#include "LoadSceneState.h"
#include "../gamescene_state/GameSceneState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../messageclient/element/CMapInfo.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../gamescene_state/MainScene.h"
#include "../gamescene_state/sceneelement/GuideMap.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../utils/GameUtils.h"
#include "../GameUserDefault.h"

#define LOAD_METHOD_ASYNC 0

#define LOADSCENE_SLIDER_NAME "slider"

using namespace CocosDenshion;

UIPanel* LoadSceneLayer::s_pPanel = NULL;
UIPanel* LoadSceneLayer::MissionSceneLayer = NULL;
UIPanel* LoadSceneLayer::TalkWithNpcLayer = NULL;
UIPanel* LoadSceneLayer::chatPanel = NULL;
UIPanel* LoadSceneLayer::expressPanel = NULL;
UIPanel* LoadSceneLayer::backPacPanel =  NULL;
UIPanel* LoadSceneLayer::mailPanel = NULL;
UIPanel *LoadSceneLayer::mailFriendPopup =NULL;
UIPanel* LoadSceneLayer::friendPanelBg = NULL;
UIPanel* LoadSceneLayer::friendlistPanel = NULL;
UIPanel* LoadSceneLayer::equipMentPanel=NULL;
UIPanel* LoadSceneLayer::auctionPanel = NULL;
UIPanel* LoadSceneLayer::SkillSceneLayer = NULL;
UIPanel* LoadSceneLayer::GeneralsUILayer = NULL;
UIPanel* LoadSceneLayer::StoreHouseLayer = NULL;
UIPanel* LoadSceneLayer::StoreHouseSettingLayer = NULL;
UIPanel* LoadSceneLayer::quiryPanel = NULL;
UIPanel* LoadSceneLayer::GeneralsFateInfoLayer = NULL;
UIPanel* LoadSceneLayer::GeneralsSkillInfoLayer = NULL;
UIPanel* LoadSceneLayer::GeneralsSkillListLayer = NULL;
UIPanel* LoadSceneLayer::StrategiesUpgradeLayer = NULL;
UIPanel* LoadSceneLayer::OnStrategiesGeneralsInfoLayer = NULL;
UIPanel* LoadSceneLayer::SingleRecuriteResultLayer = NULL;
UIPanel* LoadSceneLayer::TenTimesRecuriteResultLayer = NULL;

UIPanel* LoadSceneLayer::ShopLayer=NULL;
UIPanel* LoadSceneLayer::mapPanel=NULL;
UIPanel* LoadSceneLayer::setUiPanel =NULL;
UIPanel* LoadSceneLayer::familyMainPanel=NULL;
UIPanel* LoadSceneLayer::familyManagePanel=NULL;
UIPanel* LoadSceneLayer::familyApplyPanel=NULL;
UIPanel* LoadSceneLayer::instanceDetailUiPanel =NULL;
UIPanel* LoadSceneLayer::instanceEndUiPanel =NULL;
UIPanel* LoadSceneLayer::onHookKillPanel =NULL;

//UIPanel* LoadSceneLayer::onCountryPanel =NULL;

UIPanel* LoadSceneLayer::GoldStoreLayer = NULL;
UIPanel* LoadSceneLayer::BuyCalculatorsLayer = NULL;

UIPanel* LoadSceneLayer::StrategiesDetailInfoPopUpLayer = NULL;

UIPanel* LoadSceneLayer::onlineAwardsUIPanel = NULL;
UIPanel* LoadSceneLayer::signDailyUIPanel = NULL;
UIPanel* LoadSceneLayer::activeUIPanel = NULL;
UIPanel* LoadSceneLayer::activeShopPanel = NULL;
UIPanel* LoadSceneLayer::rankUIPanel = NULL;
UIPanel* LoadSceneLayer::ExtraRewardsLayer = NULL;
UIPanel* LoadSceneLayer::OffLineArenaLayer = NULL;
UIPanel* LoadSceneLayer::RankListLayer = NULL;

UIPanel* LoadSceneLayer::vipDetatilPanel = NULL;
UIPanel* LoadSceneLayer::vipEveryDayRewardPanel = NULL;

UIPanel* LoadSceneLayer::questionUIPanel = NULL;
UIPanel* LoadSceneLayer::RechargePanel = NULL;


enum LoadScene_SubState
{
	State_Init = 0,
	State_LoadResStep_1,
	State_LoadResStep_2,
	State_LoadResStep_3,
	State_Finished,
};

LoadSceneLayer::LoadSceneLayer()
: m_state(State_Init)
, m_bLoadResFinished(false)
, time_(0)
{
	setTouchEnabled(true);

	// close keyboard(change scene need close keyboard)
	CCEGLView * pGlView = CCDirector::sharedDirector()->getOpenGLView();
	if (pGlView)
	{
		pGlView->setIMEKeyboardState(false);
	}

    CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	if(GameView::getInstance()->getMapInfo()->country())
	{
		CCUserDefault::sharedUserDefault()->setIntegerForKey(CURRENT_COUNTRY, GameView::getInstance()->getMapInfo()->country());
		CCUserDefault::sharedUserDefault()->flush();
	}

	// game logo
	CCSprite* pSprite = CCSprite::create(getLoadingImage());
	if(((float)s.width/(float)1136) > ((float)s.height/(float)640))
	{
		pSprite->setScaleX((float)s.width/(float)1136);
		pSprite->setScaleY((float)s.width/(float)1136);
	}
	else
	{
		pSprite->setScaleX((float)s.height/(float)640);
		pSprite->setScaleY((float)s.height/(float)640);
	}
	pSprite->setPosition(ccp(s.width/2, s.height/2));
	pSprite->setAnchorPoint(ccp(0.5f, 0.5f));
	addChild(pSprite, 0);

	layer= UILayer::create();

	//layer->setPosition(ccp(s.width/2,30));
	layer->setPosition(ccp(s.width/2,s.height/2));
	addChild(layer);

	float posY = s.height * 0.3f;
	UIImageView *mengban = UIImageView::create();
	mengban->setTexture("res_ui/zhezhao80_new.png");
	mengban->setScale9Enable(true);
	mengban->setScale9Size(ccp(s.width, 100));
	mengban->setAnchorPoint(ccp(0.5f,0.5f));
	mengban->setPosition(ccp(0,-posY));
	mengban->setOpacity(130);
	layer->addWidget(mengban);

	CCLabelTTF * text=CCLabelTTF::create(getLoadingText().c_str(), APP_FONT_NAME, 20);
	text->setPosition(ccp(0,-posY));
	ccColor3B shadowColor = ccc3(0,0,0);   // black
	text->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	layer->addChild(text);

	// Create the slider
    UISlider* slider = UISlider::create();

	slider->setName(LOADSCENE_SLIDER_NAME);
    //slider->setTouchEnabled(true);
    slider->loadBarTexture("");
    slider->loadSlidBallTextures("res_ui/LOADING/light.png", "res_ui/LOADING/light.png", "");
    slider->loadProgressBarTexture("res_ui/progress_bar.png");
	slider->setScale9Enabled(true);
	slider->setSize(CCSizeMake(s.width, 40));
    slider->setPosition(ccp(0,-(posY+50)));
	slider->setPercent(0);

    layer->addWidget(slider);
	
//  CCSprite* pSprite = CCSprite::create("images/mm01.png");
//  pSprite->setPosition(ccp(s.width/2, s.height/2));
// 	pSprite->setAnchorPoint(ccp(0.5f, 0.5f));
//	addChild(pSprite, 0);

//     m_pLabelLoading = CCLabelTTF::create("loading...", "Arial", 50);
//     m_pLabelLoading->setPosition(ccp(s.width / 2, 40));
//     this->addChild(m_pLabelLoading);

#if LOAD_METHOD_ASYNC
	m_pLabelPercent = CCLabelTTF::create("%0", APP_FONT_NAME, 50);
    m_pLabelPercent->setPosition(ccp(s.width / 2, s.height / 2 + 20));
	ccColor3B shadowColor = ccc3(0,0,0);   // black
	m_pLabelPercent->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
    this->addChild(m_pLabelPercent);
#endif

	this->scheduleUpdate();
}

void LoadSceneLayer::loadingCallBack(CCObject *obj)
{
    ++m_nNumberOfLoadedSprites;
    char tmp[10];
    sprintf(tmp,"%%%d", (int)(((float)m_nNumberOfLoadedSprites / m_nNumberOfSprites) * 100));
    m_pLabelPercent->setString(tmp);

    if (m_nNumberOfLoadedSprites == m_nNumberOfSprites)
    {
		m_bLoadResFinished = true;
    }
}

void LoadSceneLayer::update(float dt)
{
	UISlider* slider =(UISlider*)(layer->getWidgetByName(LOADSCENE_SLIDER_NAME));
	if(m_state == State_Init)
	{
		loadCommonRes();
		slider->setPercent(5);
		m_state = State_LoadResStep_1;
	}
	else if(m_state == State_LoadResStep_1)
	{
		loadUIWidgetRes();
		slider->setPercent(30);
		m_state = State_LoadResStep_2;
	}
	else if(m_state == State_LoadResStep_2)
	{
		preLoadResource(GameView::getInstance()->getMapInfo()->mapid().c_str());
		slider->setPercent(60);
		m_state = State_LoadResStep_3;
	}
	else if(m_state == State_LoadResStep_3)
	{
		if(m_bLoadResFinished)
			m_state = State_Finished;
			slider->setPercent(99);
	}
	else if(m_state == State_Finished)
	{
		//this->removeChild(m_pLabelLoading, true);
        //this->removeChild(m_pLabelPercent, true);

		// create the test scene and run it
		GameState* pScene = new GameSceneState();
		if (pScene)
		{
			if(!GameView::getInstance()->myplayer->isMapReached(GameView::getInstance()->getMapInfo()->country(), GameView::getInstance()->getMapInfo()->mapid().c_str()))
			{
				GameView::getInstance()->myplayer->addCurrentReachedMap(GameView::getInstance()->getMapInfo()->country(), GameView::getInstance()->getMapInfo()->mapid());
			}

			pScene->runThisState();
			pScene->release();
		}
		
	}


}

void LoadSceneLayer::preLoadResource(const char *path)
{
	//std::vector<std::string>* sceneResList = GameSceneLayer::generateResourceList("xsc.level");
	std::vector<std::string>* sceneResList = GameSceneLayer::generateResourceList(path);

	m_nNumberOfSprites = sceneResList->size();;
	m_nNumberOfLoadedSprites = 0;

	// load textrues
	vector<std::string>::iterator iter;
	for (iter = sceneResList->begin(); iter != sceneResList->end(); ++iter)
	{
#if LOAD_METHOD_ASYNC
		// addImageAsync() method maybe has memory lead, so please pay attention to it
		CCTextureCache::sharedTextureCache()->addImageAsync((*iter).c_str(), this, callfuncO_selector(LoadSceneLayer::loadingCallBack));
#else
		// synchronize load resource, please set finished flag immediately
		CCTextureCache::sharedTextureCache()->addImage((*iter).c_str());
		m_bLoadResFinished = true;
#endif
	}

	delete sceneResList;
}

std::string LoadSceneLayer::getLoadingText()
{
	int size = LoadingTextData::s_LoadingTexts.size();
	if(size <= 0)
	{
		LoadingTextData::load("game.db");
		size = LoadingTextData::s_LoadingTexts.size();
	}

	unsigned int index = GameUtils::getRandomNum(size);
	//CCLog("index: %d, text: %s", index, LoadingTextData::s_LoadingTexts.at(index).c_str());
	CCAssert(index >= 0 && index < size, "out of range");
	return LoadingTextData::s_LoadingTexts.at(index);
}

char* LoadSceneLayer::getLoadingImage()
{
	int size = 4;
	char* image[] = {"images/loding1.jpg", "images/loding2.jpg", "images/loding3.jpg", "images/loding4.jpg"};
	unsigned int index = GameUtils::getRandomNum(size);
	CCAssert(index >= 0 && index < size, "out of range");
	return image[index];
}

void LoadSceneLayer::loadCommonRes()
{
}

void LoadSceneLayer::loadUIPanelFromJson(UIPanel** dest, const char* fileName)
{
	if(*dest == NULL)
	{
		long long currentTime = GameUtils::millisecondNow();

		std::string fullPathName = "res_ui/";
		fullPathName.append(fileName);

		*dest = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile(fullPathName.c_str());
		(*dest)->retain();

		long deltaTime = GameUtils::millisecondNow() - currentTime;
		CCLOG("load UI, cost time: %d", deltaTime);
	}
}

void LoadSceneLayer::loadUIWidgetRes()
{
	loadUIPanelFromJson(&LoadSceneLayer::s_pPanel, "renwubeibao_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::SkillSceneLayer, "jineng_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::equipMentPanel, "zhuangbei_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::onlineAwardsUIPanel, "OnlineAwards_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::GeneralsUILayer, "wujiang_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::friendPanelBg, "haoyou_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::auctionPanel, "jishouhang_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::mapPanel, "map_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::familyMainPanel, "family_main_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::signDailyUIPanel, "Registration_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::mailPanel, "youjian_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::onHookKillPanel, "on_hook_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::vipEveryDayRewardPanel, "vip1_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::activeUIPanel, "Activity_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::OffLineArenaLayer, "arena_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::SingleRecuriteResultLayer, "wujiang_get_popupo_1.json");
	loadUIPanelFromJson(&LoadSceneLayer::TenTimesRecuriteResultLayer, "wujiang_SuperGet_popupo_1.json");
}

////////////////////////////////////////////////////////////////////////////

LoadSceneState::LoadSceneState()
{
	this->setTag(GameView::STATE_LOAD_GAME);
}

void LoadSceneState::runThisState()
{
    CCLayer* pLayer = new LoadSceneLayer();
    addChild(pLayer);

    CCDirector::sharedDirector()->replaceScene(this);
    pLayer->release();
}
