#ifndef _GAMESTATE_LOADSCENE_STATE_H_
#define _GAMESTATE_LOADSCENE_STATE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../GameStateBasic.h"

USING_NS_CC;
USING_NS_CC_EXT;

/**
 * update and display the loading interface
 * Author: Zhao Gang
 * Date: 2013/11/7
 */
class LoadSceneLayer : public CCLayer
{
public:
    LoadSceneLayer();

	virtual void update(float dt);

	void preLoadResource(const char *path);
	void loadingCallBack(cocos2d::CCObject *obj);

	static UIPanel* s_pPanel;
	static UIPanel* MissionSceneLayer;
	static UIPanel* TalkWithNpcLayer;
	///////////
	static UIPanel* chatPanel;
	static UIPanel* expressPanel;
	static UIPanel* backPacPanel;
	static UIPanel* mailPanel;
	static UIPanel* mailFriendPopup;
	static UIPanel* friendPanelBg;
	static UIPanel* equipMentPanel;
	static UIPanel* friendlistPanel;
	static UIPanel* familyMainPanel;
	static UIPanel* familyApplyPanel;
	static UIPanel* familyManagePanel;
	static UIPanel* quiryPanel;

	static UIPanel* mapPanel;
	static UIPanel* auctionPanel;
	static UIPanel* SkillSceneLayer;
	static UIPanel* GeneralsUILayer;
	static UIPanel* StoreHouseLayer;
	static UIPanel* StoreHouseSettingLayer;
	static UIPanel* GeneralsFateInfoLayer;
	static UIPanel* GeneralsSkillInfoLayer;
	static UIPanel* GeneralsSkillListLayer;
	static UIPanel* StrategiesUpgradeLayer;
	static UIPanel* OnStrategiesGeneralsInfoLayer;
	static UIPanel* SingleRecuriteResultLayer;
	static UIPanel* TenTimesRecuriteResultLayer;

	static UIPanel* ShopLayer;
	static UIPanel* setUiPanel;
	static UIPanel* instanceDetailUiPanel;
	static UIPanel* instanceEndUiPanel;

	static UIPanel* onHookKillPanel;

	//static UIPanel* onCountryPanel;

	static UIPanel* GoldStoreLayer;
	//带计算器的购买界面
	static UIPanel* BuyCalculatorsLayer;

	static UIPanel* StrategiesDetailInfoPopUpLayer;

	static UIPanel* onlineAwardsUIPanel;
	static UIPanel* signDailyUIPanel;
	static UIPanel* activeUIPanel;
	static UIPanel* activeShopPanel;
	static UIPanel* rankUIPanel;
	static UIPanel* ExtraRewardsLayer;

	static UIPanel* OffLineArenaLayer;
	static UIPanel* RankListLayer;

	static UIPanel* vipDetatilPanel;
	static UIPanel* vipEveryDayRewardPanel;

	static UIPanel* questionUIPanel;
	static UIPanel* RechargePanel;

	static void loadUIPanelFromJson(UIPanel** dest, const char* fileName);

	char * mapPath;

private:
	std::string getLoadingText();
	void loadCommonRes();
	void loadUIWidgetRes();

	char* getLoadingImage();

private:
    cocos2d::CCLabelTTF *m_pLabelLoading;
    cocos2d::CCLabelTTF *m_pLabelPercent;
    int m_nNumberOfSprites;
    int m_nNumberOfLoadedSprites;

	int m_state;
	bool m_bLoadResFinished;

	CCProgressTimer *progressTimer;

	UILayer * layer;
	int time_;
};

class LoadSceneState : public GameState
{
public:
	LoadSceneState();

    virtual void runThisState();
};

#endif
