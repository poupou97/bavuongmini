#include "SignMonthlyData.h"

SignMonthlyData * SignMonthlyData::s_signMonthlyData = NULL;

SignMonthlyData::SignMonthlyData(void):
m_nCurMonth(1),
m_nSignedNum(0),
m_strGeneralId(""),
m_bCanGetGift(false)
{
}


SignMonthlyData::~SignMonthlyData(void)
{
	clearSignGiftData();
}

SignMonthlyData * SignMonthlyData::getInstance()
{
	if (s_signMonthlyData == NULL)
	{
		s_signMonthlyData = new SignMonthlyData();
	}

	return s_signMonthlyData;
}

void SignMonthlyData::clearSignGiftData()
{
	std::vector<COneMouthSignGift *>::iterator iter;
	for (iter = m_oneMonthSighGiftData.begin(); iter != m_oneMonthSighGiftData.end(); iter++)
	{
		delete *iter;
	}
	m_oneMonthSighGiftData.clear();
}
