#include "SignMonthlyUI.h"
#include "SignMonthlyData.h"
#include "messageclient\element\GoodsInfo.h"
#include "ui\backpackscene\EquipmentItem.h"
#include "AppMacros.h"
#include "gamescene_state\MainScene.h"
#include "ui\backpackscene\GoodsItemInfoBase.h"
#include "GameView.h"
#include "messageclient\element\COneMouthSignGift.h"
#include "ui\generals_ui\NormalGeneralsHeadItem.h"
#include "ui\generals_ui\ShowBigGeneralsHead.h"
#include "utils\StaticDataManager.h"
#include "messageclient\element\CGeneralBaseMsg.h"
#include "ui\generals_ui\RecuriteActionItem.h"
#include "legend_engine\CCLegendAnimation.h"
#include "messageclient\element\FolderInfo.h"
#include "..\..\messageclient\GameMessageProcessor.h"

#define kTag_SignMonthlyItem_Base 300

SignMonthlyUI::SignMonthlyUI(void)
{
}


SignMonthlyUI::~SignMonthlyUI(void)
{
}

SignMonthlyUI* SignMonthlyUI::create()
{
	SignMonthlyUI * signMonthlyUI = new SignMonthlyUI();
	if (signMonthlyUI && signMonthlyUI->init())
	{
		signMonthlyUI->autorelease();
		return signMonthlyUI;
	}
	CC_SAFE_DELETE(signMonthlyUI);
	return NULL;
}

bool SignMonthlyUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(800, 480));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winSize.width/2,winSize.height/2));
		addChild(u_layer);

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winSize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		//加载UI
		UIPanel*  ppanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/monthLogon_1.json");
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winSize.width/2,winSize.height/2));
		m_pUiLayer->addWidget(ppanel);

		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnable(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(SignMonthlyUI::CloseEvent));

		UIButton * btn_getGeneral = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_getGeneral");
		btn_getGeneral->setTouchEnable(true);
		btn_getGeneral->setPressedActionEnabled(true);
		btn_getGeneral->addReleaseEvent(this,coco_releaseselector(SignMonthlyUI::GetGeneralEvent));

		UILabelBMFont * lbf_curMonth = (UILabelBMFont*)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_curMonth");
		UILabelBMFont * lbf_curMonth1 = (UILabelBMFont*)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_curMonth1");
		UILabelBMFont * lbf_signedDays = (UILabelBMFont*)UIHelper::seekWidgetByName(ppanel,"LabelBMFont_SignedDays");

		char s_curMonth [5];
		sprintf(s_curMonth,"%d",SignMonthlyData::getInstance()->m_nCurMonth);
		lbf_curMonth->setText(s_curMonth);
		lbf_curMonth1->setText(s_curMonth);

		char s_signedDays [5];
		sprintf(s_signedDays,"%d",SignMonthlyData::getInstance()->m_nSignedNum);
		lbf_signedDays->setText(s_signedDays);

		//refresh GeneralHeadItemInfo
		//NormalGeneralsHeadItemBase * generalHead = NormalGeneralsHeadItemBase::create(50); //SignMonthlyData::getInstance()->m_strGeneralId
// 		GeneralsHeadItemBase * generalHead = GeneralsHeadItemBase::create(50);
// 		generalHead->setAnchorPoint(ccp(0,0));
// 		generalHead->setPosition(ccp(500,100));
// 		u_layer->addChild(generalHead);
		createGenralHead();

		//create scrollerView
		scrollView_info = CCScrollView::create();
		scrollView_info->setViewSize(CCSizeMake(397, 286));
		scrollView_info->setTouchEnabled(true);
		scrollView_info->setDirection(kCCScrollViewDirectionVertical);
		scrollView_info->setPosition(ccp(100,70));
		scrollView_info->setBounceable(true);
		scrollView_info->setClippingToBounds(true);
		u_layer->addChild(scrollView_info);

		//create elements
		int scroll_height = 0;
		int height_everyLine = 90;
		if (SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size() > 0)
		{
			int lineNum = 0;
			if (SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size()%5 == 0)
			{
				lineNum = SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size()/5;
			}
			else
			{
				lineNum = SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size()/5+1;
			}

			scroll_height = lineNum * height_everyLine;

			if (scroll_height > scrollView_info->getViewSize().height)
			{
				scrollView_info->setContentSize(CCSizeMake(scrollView_info->getViewSize().width,scroll_height));
				scrollView_info->setContentOffset(ccp(0,scrollView_info->getViewSize().height-scroll_height));  
			}
			else
			{
				scrollView_info->setContentSize(scrollView_info->getViewSize());
			}

			for (int i = 0;i<SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size();i++)
			{
				GoodsInfo * goodsInfo = new GoodsInfo();
				goodsInfo->CopyFrom(SignMonthlyData::getInstance()->m_oneMonthSighGiftData.at(i)->propid());
				
				SignMonthlyItem * item = SignMonthlyItem::create(SignMonthlyData::getInstance()->m_oneMonthSighGiftData.at(i));
				item->setAnchorPoint(ccp(0,0));
				item->setPosition(ccp(17+(i%5)*73,scroll_height - (i/5+1)*height_everyLine));
				scrollView_info->addChild(item);
				item->setTag(kTag_SignMonthlyItem_Base+i);

				delete goodsInfo;
			}
		}	

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}

void SignMonthlyUI::onEnter()
{
	UIScene::onEnter();
}

void SignMonthlyUI::onExit()
{
	UIScene::onExit();
}

bool SignMonthlyUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void SignMonthlyUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void SignMonthlyUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void SignMonthlyUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void SignMonthlyUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void SignMonthlyUI::update( float dt )
{

}

void SignMonthlyUI::GetGeneralEvent( CCObject *pSender )
{

}

void SignMonthlyUI::createGenralHead()
{
	int generalModelId =  std::atoi(SignMonthlyData::getInstance()->m_strGeneralId.c_str());

	std::map<int ,CGeneralBaseMsg *>::const_iterator cIter;
	cIter = GeneralsConfigData::s_generalsBaseMsgData.find(generalModelId);
	if (cIter == GeneralsConfigData::s_generalsBaseMsgData.end()) // 没找到就是指向END了  
	{
		return;
	}

	CGeneralBaseMsg* generalBaseMsg = GeneralsConfigData::s_generalsBaseMsgData[generalModelId];
	if (generalBaseMsg == NULL)
		return;

	//边框
	UIImageView* frame = UIImageView::create();
	frame->setTexture(BigGeneralsFramePath_White);    //默认是1级（1-10为白色）
	frame->setAnchorPoint(CCPointZero);
	frame->setPosition(ccp(520,158));
	frame->setScale(0.75f);
	u_layer->addWidget(frame);

	//名字
	UILabel *label_name = UILabel::create();
	label_name->setText(generalBaseMsg->name().c_str());
	label_name->setFontSize(20);
	label_name->setAnchorPoint(ccp(0.5f,0.5f));
	label_name->setAnchorPoint(ccp(0.5f,0.5f));
	label_name->setPosition(ccp(frame->getSize().width/2,42));
	label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(1));
	frame->addChild(label_name);
	//头像
	std::string icon_path = "res_ui/general/";
	icon_path.append(generalBaseMsg->get_half_photo());
	//icon_path.append("guanyu");
	icon_path.append(".png");
	UIImageView* imageView_head = UIImageView::create();
	imageView_head->setTexture(icon_path.c_str());
	imageView_head->setAnchorPoint(ccp(0.5f,0));
	imageView_head->setPosition(ccp(frame->getSize().width/2,64));
	frame->addChild(imageView_head);

	//稀有度
	if (generalBaseMsg->rare()>0)
	{
// 		UIImageView* imageView_star = UIImageView::create();
// 		imageView_star->setTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
// 		imageView_star->setAnchorPoint(CCPointZero);
// 		imageView_star->setScale(1.0f);
// 		imageView_star->setPosition(ccp(15,63));
// 		imageView_star->setName("ImageView_Star");
// 		frame->addChild(imageView_star);

		if (generalBaseMsg->rare() >= 5)
		{
			// load animation
			std::string animFileName = "animation/ui/wj/wj2.anm";
			CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
			m_pAnim->setPlayLoop(true);
			m_pAnim->setReleaseWhenStop(true);
			m_pAnim->setScale(0.75f);
			m_pAnim->setPosition(ccp(531,205));
			m_pAnim->setPlaySpeed(.35f);
			u_layer->addChild(m_pAnim);
		}
	}
}

void SignMonthlyUI::RefreshByDays( int curDay )
{
	for (int i = 0;i<SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size();i++)
	{
		if (curDay != SignMonthlyData::getInstance()->m_oneMonthSighGiftData.at(i)->day())
			continue;

		SignMonthlyItem * item = (SignMonthlyItem*)scrollView_info->getChildByTag(kTag_SignMonthlyItem_Base+i);
		if (item)
		{
			item->curOneMonthSighGift->CopyFrom(*SignMonthlyData::getInstance()->m_oneMonthSighGiftData.at(i));
			item->RefreshUIByStatus(item->curOneMonthSighGift->status());
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////
SignMonthlyItem::SignMonthlyItem()
{

}

SignMonthlyItem::~SignMonthlyItem()
{
	CC_SAFE_DELETE(curOneMonthSighGift);
	CC_SAFE_DELETE(curGoods);
}

SignMonthlyItem* SignMonthlyItem::create(COneMouthSignGift * oneMoushSignGift)
{
	SignMonthlyItem * signMonthlyItem = new SignMonthlyItem();
	if (signMonthlyItem && signMonthlyItem->init(oneMoushSignGift))
	{
		signMonthlyItem->autorelease();
		return signMonthlyItem;
	}
	CC_SAFE_DELETE(signMonthlyItem);
	return NULL;
}

bool SignMonthlyItem::init(COneMouthSignGift * oneMoushSignGift)
{
	if (UIScene::init())
	{
		/////////////////////////////////////////////////////////////////////////////////////

		// 该UILayer位于CCTableView之内，为了正确响应TableView的拖动事件，故设置为不吞掉触摸事件
		m_pUiLayer->setSwallowsTouches(false);

		curOneMonthSighGift = new COneMouthSignGift();
		curOneMonthSighGift->CopyFrom(*oneMoushSignGift);

		curGoods = new GoodsInfo();
		curGoods->CopyFrom(oneMoushSignGift->propid());

		image_Frame = UIImageView::create();
		image_Frame->setTexture("res_ui/didd.png");
		image_Frame->setScale9Enable(true);
		image_Frame->setScale9Size(CCSizeMake(70,88));
		image_Frame->setCapInsets(CCRectMake(9,9,1,1));
		image_Frame->setAnchorPoint(ccp(0,0));
		image_Frame->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(image_Frame);

		/*********************判断装备的颜色***************************/
		std::string frameColorPath;
		if (curGoods->quality() == 1)
		{
			frameColorPath = EquipWhiteFramePath;
		}
		else if (curGoods->quality() == 2)
		{
			frameColorPath = EquipGreenFramePath;
		}
		else if (curGoods->quality() == 3)
		{
			frameColorPath = EquipBlueFramePath;
		}
		else if (curGoods->quality() == 4)
		{
			frameColorPath = EquipPurpleFramePath;
		}
		else if (curGoods->quality() == 5)
		{
			frameColorPath = EquipOrangeFramePath;
		}
		else
		{
			frameColorPath = EquipVoidFramePath;
		}

		UIButton *Btn_goodsItemFrame = UIButton::create();
		Btn_goodsItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_goodsItemFrame->setTouchEnable(true);
		Btn_goodsItemFrame->setAnchorPoint(ccp(0.5f, 0.5f));
		Btn_goodsItemFrame->setPosition(ccp(35, 55));
		Btn_goodsItemFrame->setScale(0.85f);
		Btn_goodsItemFrame->setPressedActionEnabled(true);
		Btn_goodsItemFrame->addReleaseEvent(this,coco_releaseselector(SignMonthlyItem::GoodItemEvent));
		m_pUiLayer->addWidget(Btn_goodsItemFrame);

		UIImageView * uiiImageView_goodsItem = UIImageView::create();
		if (strcmp(curGoods->icon().c_str(),"") == 0)
		{
			uiiImageView_goodsItem->setTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(curGoods->icon().c_str());
			_path.append(".png");
			uiiImageView_goodsItem->setTexture(_path.c_str());
		}
		uiiImageView_goodsItem->setScale(0.9f);
		uiiImageView_goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		uiiImageView_goodsItem->setPosition(ccp(0,0));
		Btn_goodsItemFrame->addChild(uiiImageView_goodsItem);

		char s_num[20];
		sprintf(s_num,"%d",curOneMonthSighGift->propnumber());
		UILabel *Lable_num = UILabel::create();
		Lable_num->setText(s_num);
		Lable_num->setFontName(APP_FONT_NAME);
		Lable_num->setFontSize(13);
		Lable_num->setAnchorPoint(ccp(1.0f,0.5f));
		Lable_num->setPosition(ccp(0 + Btn_goodsItemFrame->getContentSize().width / 2 - 7, 
			0 - Btn_goodsItemFrame->getContentSize().height / 2 + Lable_num->getContentSize().height - 2));
		Lable_num->setName("Label_num");
		Btn_goodsItemFrame->addChild(Lable_num);

// 		if (goods->binding() == 1)
// 		{
// 			UIImageView *ImageView_bound = UIImageView::create();
// 			ImageView_bound->setTexture("res_ui/binding.png");
// 			ImageView_bound->setAnchorPoint(ccp(0,1.0f));
// 			ImageView_bound->setScale(0.85f);
// 			ImageView_bound->setPosition(ccp(8,55));
// 			m_pUiLayer->addWidget(ImageView_bound);
// 		}
// 
// 		//如果是装备
// 		if (goods->has_equipmentdetail())
// 		{
// 			if (goods->equipmentdetail().gradelevel() > 0)  //精练等级
// 			{
// 				std::string ss_gradeLevel = "+";
// 				char str_gradeLevel [10];
// 				sprintf(str_gradeLevel,"%d",goods->equipmentdetail().gradelevel());
// 				ss_gradeLevel.append(str_gradeLevel);
// 				UILabel *Lable_gradeLevel = UILabel::create();
// 				Lable_gradeLevel->setText(ss_gradeLevel.c_str());
// 				Lable_gradeLevel->setFontName(APP_FONT_NAME);
// 				Lable_gradeLevel->setFontSize(13);
// 				Lable_gradeLevel->setAnchorPoint(ccp(1.0f,1.0f));
// 				Lable_gradeLevel->setPosition(ccp(Btn_goodsItemFrame->getContentSize().width-10,Btn_goodsItemFrame->getContentSize().height-7));
// 				Lable_gradeLevel->setName("Lable_gradeLevel");
// 				m_pUiLayer->addWidget(Lable_gradeLevel);
// 			}
// 
// 			if (goods->equipmentdetail().starlevel() > 0)  //星级
// 			{
// 				char str_starLevel [10];
// 				sprintf(str_starLevel,"%d",goods->equipmentdetail().starlevel());
// 				UILabel *Lable_starLevel = UILabel::create();
// 				Lable_starLevel->setText(str_starLevel);
// 				Lable_starLevel->setFontName(APP_FONT_NAME);
// 				Lable_starLevel->setFontSize(13);
// 				Lable_starLevel->setAnchorPoint(ccp(0,0));
// 				Lable_starLevel->setPosition(ccp(8,6));
// 				Lable_starLevel->setName("Lable_starLevel");
// 				m_pUiLayer->addWidget(Lable_starLevel);
// 
// 				UIImageView *ImageView_star = UIImageView::create();
// 				ImageView_star->setTexture("res_ui/star_on.png");
// 				ImageView_star->setAnchorPoint(ccp(0,0));
// 				ImageView_star->setPosition(ccp(8+Lable_starLevel->getContentSize().width,8));
// 				ImageView_star->setVisible(true);
// 				ImageView_star->setScale(0.5f);
// 				m_pUiLayer->addWidget(ImageView_star);
// 			}
// 		}

		const char * str_days = StringDataManager::getString("SignMonthly_curdays");
		char s_days [30];
		sprintf(s_days,str_days,curOneMonthSighGift->day());
		UILabel *l_dayIndex = UILabel::create(); 
		l_dayIndex->setText(s_days);
		l_dayIndex->setFontName(APP_FONT_NAME);
		l_dayIndex->setFontSize(16);
		l_dayIndex->setAnchorPoint(ccp(.5f,.5f));
		l_dayIndex->setPosition(ccp(35,13));
		m_pUiLayer->addWidget(l_dayIndex);

		if (curOneMonthSighGift->viplevel()>0)
		{
			UIImageView * image_vipLevel = UIImageView::create();
			image_vipLevel->setAnchorPoint(ccp(0.5f,0.5f));
			image_vipLevel->setPosition(ccp(21,66));
			m_pUiLayer->addWidget(image_vipLevel);
			switch(curOneMonthSighGift->viplevel())
			{
			case 1:
				{
					image_vipLevel->setTexture("res_ui/monthLogon/vip1double.png");
				}
				break;
			case 2:
				{
					image_vipLevel->setTexture("res_ui/monthLogon/vip2double.png");
				}
				break;
			case 3:
				{
					image_vipLevel->setTexture("res_ui/monthLogon/vip3double.png");
				}
				break;
			case 4:
				{
					image_vipLevel->setTexture("res_ui/monthLogon/vip4double.png");
				}
				break;
			case 5:
				{
					image_vipLevel->setTexture("res_ui/monthLogon/vip5double.png");
				}
				break;
			}
		}

		RefreshUIByStatus(curOneMonthSighGift->status());
		this->setContentSize(image_Frame->getSize());

		return true;
	}
	return false;
}

void SignMonthlyItem::onEnter()
{
	UIScene::onEnter();
}

void SignMonthlyItem::onExit()
{
	UIScene::onExit();
}

bool SignMonthlyItem::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void SignMonthlyItem::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void SignMonthlyItem::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void SignMonthlyItem::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void SignMonthlyItem::GoodItemEvent( CCObject *pSender )
{
	if (curOneMonthSighGift->status() == 2)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5149,(void *)curOneMonthSighGift->day());
	}
	else
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase) == NULL)
		{
			CCSize size= CCDirector::sharedDirector()->getVisibleSize();
			GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(curGoods,GameView::getInstance()->EquipListItem,0);
			goodsItemInfoBase->ignoreAnchorPointForPosition(false);
			goodsItemInfoBase->setAnchorPoint(ccp(0.5f,0.5f));
			//goodsItemInfoBase->setPosition(ccp(size.width/2,size.height/2));
			goodsItemInfoBase->setTag(kTagGoodsItemInfoBase);
			GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
		}
	}
}

void SignMonthlyItem::RefreshUIByStatus( int status )
{
	if (m_pUiLayer->getWidgetByName("image_got"))
	{
		m_pUiLayer->getWidgetByName("image_got")->removeFromParent();
	}

	switch(status)
	{
	case 1:     //不可领取奖励
		{
			image_Frame->setTexture("res_ui/didd.png");
		}
		break;
	case 2:   //可领取
		{
			image_Frame->setTexture("res_ui/didd.png");
		}
		break;
	case 3:  //已领取奖励完毕
		{
			image_Frame->setTexture("res_ui/diddd.png");

			UIImageView * image_got= UIImageView::create();
			image_got->setTexture("res_ui/yilingqu.png");
			image_got->setAnchorPoint(ccp(0.5f,0.5f));
			image_got->setPosition(ccp(49,19));
			image_got->setName("image_got");
			m_pUiLayer->addWidget(image_got);
		}
		break;
	}
		
}
