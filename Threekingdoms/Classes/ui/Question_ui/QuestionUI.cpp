#include "QuestionUI.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "QuestionData.h"
#include "../../utils/StaticDataManager.h"

using namespace CocosDenshion;

UIPanel * QuestionUI::s_pPanel;

QuestionUI::QuestionUI(void)
	:m_base_layer(NULL)
{

}

QuestionUI::~QuestionUI(void)
{

}

QuestionUI* QuestionUI::create()
{
	QuestionUI * questionUI = new QuestionUI();
	if (questionUI && questionUI->init())
	{
		questionUI->autorelease();
		return questionUI;
	}
	CC_SAFE_DELETE(questionUI);
	return NULL;
}

bool QuestionUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		m_base_layer = UILayer::create();
		m_base_layer->ignoreAnchorPointForPosition(false);
		m_base_layer->setAnchorPoint(ccp(0.5f, 0.5f));
		m_base_layer->setContentSize(CCSizeMake(800, 480));
		m_base_layer->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		this->addChild(m_base_layer);

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winSize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0, 0));
		m_pUiLayer->addWidget(mengban);

		if(LoadSceneLayer::questionUIPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::questionUIPanel->removeFromParentAndCleanup(false);
		}

		s_pPanel = LoadSceneLayer::questionUIPanel;
		s_pPanel->setAnchorPoint(ccp(0.5f, 0.5f));
		s_pPanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		s_pPanel->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		s_pPanel->setScale(1.0f);
		s_pPanel->setTouchEnable(true);
		m_pUiLayer->addWidget(s_pPanel);


		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		//this->setContentSize(CCSize(800, 480));

		initUI();

		return true;
	}
	return false;
}

void QuestionUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void QuestionUI::onExit()
{
	UIScene::onExit();

	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

void QuestionUI::callBackBtnClolse( CCObject * obj )
{
	this->closeAnim();
}

void QuestionUI::initUI()
{
	// 关闭按钮
	UIButton* pBtnClose = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnable(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addReleaseEvent(this, coco_releaseselector(QuestionUI::callBackBtnClolse));

	// 问题标题
	m_label_questionTitle = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_question_text");
	m_label_questionTitle->setVisible(true);
	m_label_questionTitle->setTextAreaSize(CCSizeMake(633,0));

	// 经验值
	m_label_expValue =(UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_exp_value");
	m_label_expValue->setVisible(true);

	// 金钱
	m_label_conisValue = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_coins_value");
	m_label_conisValue->setVisible(true);

	// 累计答对题数
	m_label_rightAnswerValue = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_rightAnswer_value");
	m_label_rightAnswerValue->setVisible(true);

	// 已答题数
	m_label_hasAnswerValue = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_hasAnswer_value");
	m_label_hasAnswerValue->setVisible(true);

	// 可翻牌次数
	m_label_cardNumValue = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_cardNum_value");
	m_label_cardNumValue->setVisible(true);

	// btn1 A
	m_btn_a = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_a");
	m_btn_a->setTouchEnable(true);
	m_btn_a->setPressedActionEnabled(true);
	m_btn_a->addReleaseEvent(this, coco_releaseselector(QuestionUI::callBackBtnQuestion));
	m_btn_a->setTag(TAG_BTN_QUE_1);
	m_btn_a->setVisible(true);

	// btn2 B
	m_btn_b = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_b");
	m_btn_b->setTouchEnable(true);
	m_btn_b->setPressedActionEnabled(true);
	m_btn_b->addReleaseEvent(this, coco_releaseselector(QuestionUI::callBackBtnQuestion));
	m_btn_b->setTag(TAG_BTN_QUE_2);
	m_btn_b->setVisible(true);

	// btn3 C
	m_btn_c = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_c");
	m_btn_c->setTouchEnable(true);
	m_btn_c->setPressedActionEnabled(true);
	m_btn_c->addReleaseEvent(this, coco_releaseselector(QuestionUI::callBackBtnQuestion));
	m_btn_c->setTag(TAG_BTN_QUE_3);
	m_btn_c->setVisible(true);

	// btn4 D
	m_btn_d = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_d");
	m_btn_d->setTouchEnable(true);
	m_btn_d->setPressedActionEnabled(true);
	m_btn_d->addReleaseEvent(this, coco_releaseselector(QuestionUI::callBackBtnQuestion));
	m_btn_d->setTag(TAG_BTN_QUE_4);
	m_btn_d->setVisible(true);

	// btn1 A text
	m_label_btn1_text = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_answerA");
	m_label_btn1_text->setVisible(true);
	m_label_btn1_text->setTextAreaSize(CCSizeMake(240,0));

	// btn2 B text
	m_label_btn2_text = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_answerB");
	m_label_btn2_text->setVisible(true);
	m_label_btn2_text->setTextAreaSize(CCSizeMake(240,0));

	// btn3 C text
	m_label_btn3_text = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_answerC");
	m_label_btn3_text->setVisible(true);
	m_label_btn3_text->setTextAreaSize(CCSizeMake(240,0));

	// btn4 D text
	m_label_btn4_text = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_answerD");
	m_label_btn4_text->setVisible(true);
	m_label_btn4_text->setTextAreaSize(CCSizeMake(240,0));

	imageView_trueOption = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_trueOption");
	imageView_trueOption->setVisible(false);

	initBaseInfoFromServer();

	//set to default
	QuestionData::instance()->set_isGetedResult(true);
}

bool QuestionUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void QuestionUI::requestQuestion()
{
	// 请求服务器 请求题目
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2802);
}

void QuestionUI::callBackBtnQuestion( CCObject* obj )
{
	if(!QuestionData::instance()->get_isGetedResult())
		return;

	UIButton * pUIBtn = (UIButton *)obj;
	int nTag = pUIBtn->getTag();
	if (TAG_BTN_QUE_1 == nTag)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2803, (void *)0);
	}
	else if (TAG_BTN_QUE_2 == nTag)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2803, (void *)1);
	}
	else if (TAG_BTN_QUE_3 == nTag)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2803, (void *)2);
	}
	else if (TAG_BTN_QUE_4 == nTag)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2803, (void *)3);
	}
}

void QuestionUI::initQuestionInfoFromServer()
{
	// 金钱
	int nMoney = QuestionData::instance()->get_money();
	char charMoney[20];
	sprintf(charMoney, "%d", nMoney);
	m_label_conisValue->setText(charMoney);

	// 经验
	int nExp = QuestionData::instance()->get_exp();
	char charExp[20];
	sprintf(charExp, "%d", nExp);
	m_label_expValue->setText(charExp);

	// 已答题数(已答题数 / 每日答题次数【总次数】)
	int nHasAnswerQuestion = QuestionData::instance()->get_hasAnswerQuestion();
	char charHasAnswerQuestion[20];
	sprintf(charHasAnswerQuestion, "%d", nHasAnswerQuestion);

	int nAllQuestion = QuestionData::instance()->get_allQuestion();											// 每日答题次数
	char charAllQuestion[20];
	sprintf(charAllQuestion, "%d", nAllQuestion);
	
	std::string strAnswerQuestion = "";
	strAnswerQuestion.append(charHasAnswerQuestion);
	strAnswerQuestion.append(" / ");
	strAnswerQuestion.append(charAllQuestion);

	m_label_hasAnswerValue->setText(strAnswerQuestion.c_str());

	// 问题标题
	std::string strTmpQuestionTitle = "";
	std::string strQuestionTitle = QuestionData::instance()->get_questionTitle();

	int m_idx = nHasAnswerQuestion + 1;
	if (m_idx >= QuestionData::instance()->get_allQuestion())
		m_idx = QuestionData::instance()->get_allQuestion();

	char charQuestionTitle[20];
	sprintf(charQuestionTitle, "%d",m_idx);

	strTmpQuestionTitle.append(charQuestionTitle);
	strTmpQuestionTitle.append(". ");
	strTmpQuestionTitle.append(strQuestionTitle);

	m_label_questionTitle->setText(strTmpQuestionTitle.c_str());

	m_btn_a->setVisible(false);																								
	m_btn_b->setVisible(false);																								
	m_btn_c->setVisible(false);																								
	m_btn_d->setVisible(false);			

	// 题目信息
	int nSizeQuestion = QuestionData::instance()->m_vector_questionContent.size();
	std::vector<std::string > vector_questionContent = QuestionData::instance()->m_vector_questionContent;

	switch(nSizeQuestion)
	{
	case 1:
		{
			m_btn_a->setVisible(true);			
			m_label_btn1_text->setText(vector_questionContent.at(0).c_str());
		}
		break;
	case 2:
		{
			m_btn_a->setVisible(true);			
			m_label_btn1_text->setText(vector_questionContent.at(0).c_str());
			m_btn_b->setVisible(true);			
			m_label_btn2_text->setText(vector_questionContent.at(1).c_str());
		}
		break;
	case 3:
		{
			m_btn_a->setVisible(true);			
			m_label_btn1_text->setText(vector_questionContent.at(0).c_str());
			m_btn_b->setVisible(true);			
			m_label_btn2_text->setText(vector_questionContent.at(1).c_str());
			m_btn_c->setVisible(true);			
			m_label_btn3_text->setText(vector_questionContent.at(2).c_str());
		}
		break;
	case 4:
		{
			m_btn_a->setVisible(true);			
			m_label_btn1_text->setText(vector_questionContent.at(0).c_str());
			m_btn_b->setVisible(true);			
			m_label_btn2_text->setText(vector_questionContent.at(1).c_str());
			m_btn_c->setVisible(true);			
			m_label_btn3_text->setText(vector_questionContent.at(2).c_str());
			m_btn_d->setVisible(true);			
			m_label_btn4_text->setText(vector_questionContent.at(3).c_str());
		}
		break;
	}
	
}

void QuestionUI::initBaseInfoFromServer()
{
	int nAllQuestion = QuestionData::instance()->get_allQuestion();											// 每日答题次数
	int nHasAnswerQuestion = QuestionData::instance()->get_hasAnswerQuestion();					// 已回答次数
	int nRightAnswerQuestion = QuestionData::instance()->get_rightAnswerQuestion();				// 已经答对的题目数目
	int nCardNum = QuestionData::instance()->get_cardNum();													// 可翻牌的次数

	// 注意:已答题数(已答题数 / 每日答题次数【总次数】)
	std::string strAnswerQuestion = "";

	char charAllQuestion[20];
	sprintf(charAllQuestion, "%d", nAllQuestion);

	char charHasAnswerQuestion[20];
	sprintf(charHasAnswerQuestion, "%d", nHasAnswerQuestion);
	
	strAnswerQuestion.append(charHasAnswerQuestion);
	strAnswerQuestion.append(" / ");
	strAnswerQuestion.append(charAllQuestion);

	m_label_hasAnswerValue->setText(strAnswerQuestion.c_str());


	// 已经答对的题目数目
	char charRightAnswerQuestion[20];
	sprintf(charRightAnswerQuestion, "%d", nRightAnswerQuestion);
	m_label_rightAnswerValue->setText(charRightAnswerQuestion);

	// 可翻牌的次数
	char charCardNum[20];
	sprintf(charCardNum, "%d", nCardNum);
	m_label_cardNumValue->setText(charCardNum);
}

void QuestionUI::HandleResult( int lastResult,int lastTrueOption )
{
	if (0 == lastResult)
	{
		setTrueOptionPos(lastTrueOption);
		CCSequence* sequence = CCSequence::create(
			CCHide::create(),
			CCDelayTime::create(0.5f),
			CCShow::create(),
			CCDelayTime::create(1.5f),
			CCCallFunc::create(this, callfunc_selector(QuestionUI::reqNextQuestion)),
			CCHide::create(),
			NULL
			);
		imageView_trueOption->runAction(sequence);
	}
	else if(1 == lastResult)
	{
		reqNextQuestion();
	}
}

void QuestionUI::reqNextQuestion()
{
	if (QuestionData::instance()->get_hasAnswerQuestion() < 20)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2802);
		QuestionData::instance()->set_isGetedResult(true);
	}
}

void QuestionUI::setTrueOptionPos(int index )
{
	switch(index)
	{
	case 0:
		{
			imageView_trueOption->setPosition(ccp(350,353));
		}
		break;
	case 1:
		{
			imageView_trueOption->setPosition(ccp(661,353));
		}
		break;
	case 2:
		{
			imageView_trueOption->setPosition(ccp(350,271));
		}
		break;
	case 3:
		{
			imageView_trueOption->setPosition(ccp(661,271));
		}
		break;
	}
}

