#ifndef _UI_QUESTION_QUESTIONDATA_H_
#define _UI_QUESTION_QUESTIONDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 答题模块（用于保存答题服务器数据）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.05.13
 */

class QuestionData
{
public:
	static QuestionData * s_questionData;
	static QuestionData * instance();

private:
	QuestionData(void);
	~QuestionData(void);

private:
	int m_nAllQuestion;															// 每日可答题次数（总次数）
	int m_nHasAnswerQuestion;												// 已回答次数
	int m_nRightAnswerQuestion;											// 已经答对的题目数目
	int m_nCardNum;																// 可翻牌的次数

	int m_nMoney;																	// 金钱
	int m_nExp;																		// 经验
	std::string m_strQuestionTitle;											// 题目标题

	int m_nResult;																	// 答题结果
	bool m_bIsGetedResult;													// 是否已经收到服务器返回的答题结果

	int m_nReward_index;														//奖励卡牌标号

public:
	std::vector<std::string>	m_vector_questionContent;		// 题目内容

public:
	void set_allQuestion(int nAllQuestion);
	int get_allQuestion();

	void set_hasAnswerQuestion(int nHasAnswerQuestion);
	int get_hasAnswerQuestion();

	void set_rightAnswerQuestion(int nRightAnswerQuestion);
	int get_rightAnswerQuestion();

	void set_cardNum(int nCardNum);
	int get_cardNum();

	void set_questionTitle(std::string strQuestionTitle);
	std::string get_questionTitle();

	void set_money(int nMoney);
	int get_money();

	void set_exp(int nExp);
	int get_exp();

	void set_result(int nResult);
	int get_result();

	void clearData();

	void set_isGetedResult(bool isGeted);
	bool get_isGetedResult();

	void set_reward_index(int idx);
	int get_reward_index();
};

#endif

