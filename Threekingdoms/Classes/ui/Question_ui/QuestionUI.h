#ifndef _UI_QUESTION_QUESTIONUI_H_
#define _UI_QUESTION_QUESTIONUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 答题UI
 * @author liuliang
 * @version 0.1.0
 * @date 2014.05.13
 */

class QuestionUI:public UIScene
{
public:
	QuestionUI(void);
	~QuestionUI(void);

public:
	static UIPanel * s_pPanel;
	static QuestionUI* create();
	bool init();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);

	virtual void onEnter();
	virtual void onExit();

private:
	UILayer * m_base_layer;																							// 基调layer

	UILabel * m_label_questionTitle;																				// 问题标题
	UILabel * m_label_expValue;																						// 经验值
	UILabel * m_label_conisValue;																					// 金钱
	UILabel * m_label_rightAnswerValue;																			// 累计答对题数
	UILabel * m_label_hasAnswerValue;																			// 已答题数(已答题数 / 每日答题次数【总次数】)
	UILabel * m_label_cardNumValue;																				// 可翻牌次数

	UIButton * m_btn_a;																								// btn1	A
	UIButton * m_btn_b;																								// btn2	B
	UIButton * m_btn_c;																								// btn3	C
	UIButton * m_btn_d;																								// btn4	D

	UILabel * m_label_btn1_text;																					// btn1 A text
	UILabel * m_label_btn2_text;																					// btn2 B text
	UILabel * m_label_btn3_text;																					// btn3 C text
	UILabel * m_label_btn4_text;																					// btn4 D text

	UIImageView * imageView_trueOption;

public:
	void callBackBtnClolse(CCObject * obj);																	// 关闭按钮的响应
	void callBackBtnQuestion(CCObject* obj);																	// 答题按钮的响应

	void requestQuestion();																								// 向服务器请求问题
	void initQuestionInfoFromServer();																			// 取得服务器数据初始化UI（题目）
	void initBaseInfoFromServer();																					// 取得服务器数据初始化UI（登录时服务器发送的数据Push2801）

	void HandleResult(int lastResult,int lastTrueOption);
	void reqNextQuestion();
	void setTrueOptionPos(int index);
private:
	void initUI();

private:
	typedef enum TagBtnQuestion
	{
		TAG_BTN_QUE_1 = 101,
		TAG_BTN_QUE_2 = 102,
		TAG_BTN_QUE_3 = 103,
		TAG_BTN_QUE_4 = 104,
	};
};

#endif

