#ifndef _UI_QUESTION_QUESTIONENDUI_H_
#define _UI_QUESTION_QUESTIONENDUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../skillscene/MusouSkillTalent.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CPropReward;

#define  kTag_Base_RewardCardItem 100

/////////////////////////////////
/**
 * 答题结束后领奖UI
 * @author yangjun
 * @version 0.1.0
 * @date 2014.05.19
 */

class QuestionEndUI : public UIScene
{
public:
	QuestionEndUI();
	~QuestionEndUI();

	static QuestionEndUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void update(float delta);

	void CloseEvent(CCObject * pSender);

	void initReward(std::vector<CPropReward*> tempList);

	//把所有牌都翻到背面
	void AllCardTurnToBack();
	//开始洗牌
	void BeginShuffle();

	void set_RemainNumber(int _value);
	int get_RemainNumber();

public:
	UILayer *u_layer;
private:
	
	std::vector<Coordinate> Coordinate_item;

	//剩余时间
	UILabel * l_remainTime;
	int remainTime;

	//剩余次数
	UILabel * l_remainNumber;
	int remainNumber;
};

#endif
