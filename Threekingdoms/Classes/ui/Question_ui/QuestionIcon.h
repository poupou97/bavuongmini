#ifndef _UI_QUESTION_QUESTIONICON_H_
#define _UI_QUESTION_QUESTIONICON_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 签到Icon
 * @author liuliang
 * @version 0.1.0
 * @date 2014.05.13
 */


class CCTutorialParticle;

class QuestionIcon:public UIScene
{
public:
	QuestionIcon(void);
	~QuestionIcon(void);

public:
	static QuestionIcon* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

public:
	UIButton * m_btn_questionIcon;					// 可点击的ICON
	UILayer * m_layer_question;							// 需要用到的layer
	UILabel * m_label_questionIcon_text;			// 	ICON下方显示的文字	
	CCScale9Sprite * m_spirte_fontBg;				// 文字显示的底图

public:
	void callBackBtnQuestion(CCObject *obj);
	
};

#endif

