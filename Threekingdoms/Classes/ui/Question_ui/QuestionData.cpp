#include "QuestionData.h"


QuestionData * QuestionData::s_questionData = NULL;

QuestionData::QuestionData(void):
m_bIsGetedResult(true),
m_nReward_index(-1)
{

}

QuestionData::~QuestionData(void)
{

}

QuestionData * QuestionData::instance()
{
	if (NULL == s_questionData)
	{
		s_questionData = new QuestionData();
	}

	return s_questionData;
}

void QuestionData::set_allQuestion( int nAllQuestion )
{
	this->m_nAllQuestion = nAllQuestion;
}

int QuestionData::get_allQuestion()
{
	return this->m_nAllQuestion;
}

void QuestionData::set_hasAnswerQuestion( int nHasAnswerQuestion )
{
	this->m_nHasAnswerQuestion = nHasAnswerQuestion;
}

int QuestionData::get_hasAnswerQuestion()
{
	return this->m_nHasAnswerQuestion;
}

void QuestionData::set_rightAnswerQuestion( int nRightAnswerQuestion )
{
	this->m_nRightAnswerQuestion = nRightAnswerQuestion;
}

int QuestionData::get_rightAnswerQuestion()
{
	return this->m_nRightAnswerQuestion;
}

void QuestionData::set_cardNum( int nCardNum )
{
	this->m_nCardNum = nCardNum;
}

int QuestionData::get_cardNum()
{
	return this->m_nCardNum;
}

void QuestionData::set_questionTitle( std::string strQuestionTitle )
{
	this->m_strQuestionTitle = strQuestionTitle;
}

std::string QuestionData::get_questionTitle()
{
	return this->m_strQuestionTitle;
}

void QuestionData::set_money( int nMoney )
{
	this->m_nMoney = nMoney;
}

int QuestionData::get_money()
{
	return this->m_nMoney;
}

void QuestionData::set_exp( int nExp )
{
	this->m_nExp = nExp;
}

int QuestionData::get_exp()
{
	return this->m_nExp;
}

void QuestionData::clearData()
{
	/*m_nAllQuestion = 0;
	m_nHasAnswerQuestion = 0;
	m_nRightAnswerQuestion = 0;
	m_nCardNum = 0;

	m_nMoney = 0;
	m_nExp = 0;
	m_strQuestionTitle = "";*/

	m_vector_questionContent.clear();
}

void QuestionData::set_result( int nResult )
{
	this->m_nResult = nResult;
}

int QuestionData::get_result()
{
	return this->m_nResult;
}

void QuestionData::set_isGetedResult( bool isGeted )
{
	m_bIsGetedResult = isGeted;
}

bool QuestionData::get_isGetedResult()
{
	return m_bIsGetedResult;
}

void QuestionData::set_reward_index( int idx )
{
	m_nReward_index = idx;
}

int QuestionData::get_reward_index()
{
	return m_nReward_index;
}




