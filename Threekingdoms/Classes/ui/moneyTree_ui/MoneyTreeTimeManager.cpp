#include "MoneyTreeTimeManager.h"
#include "MoneyTreeData.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Reward_ui/RewardUi.h"
#include "GameView.h"
#include "../../messageclient/element/CRewardBase.h"
#include "../../ui/GameUIConstant.h"
#include "../../gamescene_state/role/MyPlayer.h"

MoneyTreeTimeManager::MoneyTreeTimeManager(void)
	:m_strTimeLeft("")
	,m_nLeftTime(0)
	,m_nShowTime(0)
{

}

MoneyTreeTimeManager::~MoneyTreeTimeManager(void)
{
	
}

MoneyTreeTimeManager* MoneyTreeTimeManager::create()
{
	MoneyTreeTimeManager * pMoneyTreeTimeManager = new MoneyTreeTimeManager();
	if (pMoneyTreeTimeManager && pMoneyTreeTimeManager->init())
	{
		pMoneyTreeTimeManager->autorelease();
		return pMoneyTreeTimeManager;
	}
	CC_SAFE_DELETE(pMoneyTreeTimeManager);
	return NULL;
}

bool MoneyTreeTimeManager::init()
{
	bool bRet=false;
	do 
	{
		CC_BREAK_IF(!CCNode::init());

		this->schedule(schedule_selector(MoneyTreeTimeManager::updateTime), 1.0f);

		bRet = true;

	} while (0);

	return bRet;
}

void MoneyTreeTimeManager::updateTime( float dt )
{
	m_nLeftTime -= 1000;
	m_nShowTime = m_nLeftTime;
	if (m_nShowTime <= 0)
	{
		m_nShowTime = 0;
	}

	refreshData();
}

std::string MoneyTreeTimeManager::timeFormatToString( int nTime )
{
	if (0 == nTime)
	{
		std::string timeString = "";
		const char *str1 = StringDataManager::getString("MoneyTree_canGetInfo");
		timeString.append(str1);
		return timeString;
	}
	else
	{
		int int_m = nTime / 1000 / 60;
		int int_s = nTime / 1000 % 60;

		std::string timeString = "";
		char str_m[10];
		if (int_m < 10)
		{
			timeString.append("0");
		}
		sprintf(str_m,"%d",int_m);
		timeString.append(str_m);
		timeString.append(":");

		char str_s[10];
		if (int_s < 10)
		{
			timeString.append("0");
		}
		sprintf(str_s,"%d",int_s);
		timeString.append(str_s);

		return timeString;
	}
}

void MoneyTreeTimeManager::set_timeLeftStr( std::string strTimeLeft )
{
	this->m_strTimeLeft = strTimeLeft;
}

std::string MoneyTreeTimeManager::get_timeLeftStr()
{
	return this->m_strTimeLeft;
}

void MoneyTreeTimeManager::refreshData()
{
	std::string strShowTime = timeFormatToString(m_nShowTime);

	// 赋值给 m_str_timeLeft;
	this->set_timeLeftStr(strShowTime);

	std::string timeString = "";
	const char *str1 = StringDataManager::getString("MoneyTree_canGetInfo");
	timeString.append(str1);
	
	//判断 将要显示的是否为“可以摇钱啦！”，若是，则XXX；否则 XXX
	if (this->get_timeLeftStr() == timeString)
	{
		bool ishave = false;
		if (GameView::getInstance()->myplayer->getActiveRole()->level() >= 20)
		{
			for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
			{
				if (REWARD_LIST_ID_MONEYTREE == GameView::getInstance()->rewardvector.at(i)->getId())
				{
					ishave = true;
				}
			}

			if (!MoneyTreeData::instance()->hasAllGiftGet() && ishave == false)
			{
				RewardUi::addRewardListEvent(REWARD_LIST_ID_MONEYTREE);
			}
		}

		if (!MoneyTreeData::instance()->hasAllGiftGet())
		{
			for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
			{
				if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_MONEYTREE)
				{
					GameView::getInstance()->rewardvector.at(i)->setRewardState(1);
					RewardUi::setRewardListParticle();
				}
			}
		}
	}
}

void MoneyTreeTimeManager::initDataFromIntent()
{
	m_nLeftTime = MoneyTreeData::instance()->get_needTime();
	m_nShowTime = m_nLeftTime;

	this->refreshData();
}










