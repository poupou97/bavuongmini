#include "GeneralEquipRecover.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "EquipMentUi.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "GeneralEquipShowRecover.h"
#include "GeneralEquipMent.h"


GeneralEquipRecover::GeneralEquipRecover(void)
{
	m_useLevel =0;
	m_refineLevel =0;
	m_starLevel = 0;
	m_clazz = 0;
	m_quality = 0;
}


GeneralEquipRecover::~GeneralEquipRecover(void)
{
	delete equip_;
}

GeneralEquipRecover * GeneralEquipRecover::create( CEquipment * equipment ,bool isMainequip)
{
	GeneralEquipRecover * generalequip =new GeneralEquipRecover();
	if (generalequip && generalequip->init(equipment,isMainequip))
	{
		generalequip->autorelease();
		return generalequip;
	}
	CC_SAFE_DELETE(generalequip);
	return NULL;
}

bool GeneralEquipRecover::init( CEquipment * equipment,bool isMainequip )
{
	equip_ =new CEquipment();
	equip_->CopyFrom(*equipment);
	if (UIScene::init())
	{
		m_clazz = equip_->goods().equipmentclazz();
		m_quality = equip_->goods().quality();
		std::string frameColorPath = "res_ui/";
		
		if (isMainequip == true)
		{
			if (equip_->goods().equipmentdetail().quality() == 1)
			{
				frameColorPath.append("bdi_white");
			}
			else if (equip_->goods().equipmentdetail().quality() == 2)
			{
				frameColorPath.append("bdi_green");
			}
			else if (equip_->goods().equipmentdetail().quality() == 3)
			{
				frameColorPath.append("bdi_blue");
			}
			else if (equip_->goods().equipmentdetail().quality() == 4)
			{
				frameColorPath.append("bdi_purple");
			}
			else if (equip_->goods().equipmentdetail().quality() == 5)
			{
				frameColorPath.append("bdi_orange");
			}
			else
			{
				frameColorPath.append("bdi_none");
			}
		}else
		{
			if (equip_->goods().equipmentdetail().quality() == 1)
			{
				frameColorPath.append("sdi_white");
			}
			else if (equip_->goods().equipmentdetail().quality() == 2)
			{
				frameColorPath.append("sdi_green");
			}
			else if (equip_->goods().equipmentdetail().quality() == 3)
			{
				frameColorPath.append("sdi_bule");
			}
			else if (equip_->goods().equipmentdetail().quality() == 4)
			{
				frameColorPath.append("sdi_purple");
			}
			else if (equip_->goods().equipmentdetail().quality() == 5)
			{
				frameColorPath.append("sdi_orange");
			}
			else
			{
				frameColorPath.append("sdi_white");
			}
		}
		
		frameColorPath.append(".png");

		Btn_pacItemFrame = UIButton::create();
		Btn_pacItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnable(true);
		Btn_pacItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(ccp(25,24));
		Btn_pacItemFrame->addReleaseEvent(this,coco_releaseselector(GeneralEquipRecover::showEquipInfo));
		m_pUiLayer->addWidget(Btn_pacItemFrame);

		m_useLevel = equip_->goods().uselevel();

		std::string goodsInfoStr ="res_ui/props_icon/";
		goodsInfoStr.append(equip_->goods().icon());
		goodsInfoStr.append(".png");

		UIImageView *equipIcon_ =UIImageView::create();
		equipIcon_->setTexture(goodsInfoStr.c_str());
		equipIcon_->setAnchorPoint(ccp(0.5f,0.5f));
		equipIcon_->setPosition(ccp(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		m_starLevel =equip_->goods().equipmentdetail().starlevel();
		char starLvLabel_[5];
		sprintf(starLvLabel_,"%d",m_starLevel);
		labelStarlevel_ =UILabel::create();
		labelStarlevel_->setAnchorPoint(ccp(0,0));
		labelStarlevel_->setPosition(ccp(-25,-20));
		labelStarlevel_->setFontSize(13);
		labelStarlevel_->setText(starLvLabel_);
		Btn_pacItemFrame->addChild(labelStarlevel_);

		imageStar_ =UIImageView::create();
		imageStar_->setTexture("res_ui/star_on.png");
		imageStar_->setScale(0.5f);
		imageStar_->setAnchorPoint(ccp(0,0));
		imageStar_->setPosition(ccp(labelStarlevel_->getContentSize().width-27,labelStarlevel_->getPosition().y));
		Btn_pacItemFrame->addChild(imageStar_);

		if (m_starLevel<=0)
		{
			labelStarlevel_->setVisible(false);
			imageStar_->setVisible(false);
		}

		m_refineLevel =equip_->goods().equipmentdetail().gradelevel();
		char strengthLv[5];
		sprintf(strengthLv,"%d",m_refineLevel);
		std::string strengthStr_ = "+";
		strengthStr_.append(strengthLv);
		label_refineLv = UILabel::create();
		label_refineLv->setAnchorPoint(ccp(1.0f,0.5f));
		label_refineLv->setPosition(ccp(22,15));//22 24
		label_refineLv->setText(strengthStr_.c_str());
		label_refineLv->setFontSize(13);
		Btn_pacItemFrame->addChild(label_refineLv);

		if (m_refineLevel<=0)
		{
			label_refineLv->setVisible(false);
		}

		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(Btn_pacItemFrame->getContentSize());
		return true;
	}
	return false;
}

void GeneralEquipRecover::onEnter()
{
	UIScene::onEnter();
}

void GeneralEquipRecover::onExit()
{
	UIScene::onExit();
}

void GeneralEquipRecover::showEquipInfo( CCObject * obj )
{
	int tag_ =this->getTag();
	/*
	CCSize size= CCDirector::sharedDirector()->getVisibleSize();
	EquipMentUi * equipmentui = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	GeneralEquipShowRecover *auction =GeneralEquipShowRecover::create(equip_,equipmentui->generalEquipVector,equipmentui->mes_petid);
	auction->ignoreAnchorPointForPosition(false);
	auction->setAnchorPoint(ccp(0.5f,0.5f));
	auction->setTag(tag_);
	GameView::getInstance()->getMainUIScene()->addChild(auction);
	*/
	//
	EquipMentUi * equip=(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	equip->getBackGeneralMainEquip(tag_);

// 	std::vector<CEquipment *>::iterator iter;
// 	for (iter = equip->curMainEquipvector.begin(); iter != equip->curMainEquipvector.end();++iter)
// 	{
// 		delete * iter;
// 	}
// 	equip->curMainEquipvector.clear();
// 	for (int i = 0;i<equip->generalEquipVector.size();i++)
// 	{
// 		CEquipment * temp_ =new CEquipment();
// 		temp_->CopyFrom( *equip->generalEquipVector.at(i));
// 		equip->curMainEquipvector.push_back(temp_);
// 	}

// 	if (equip->refineMainEquip == false)
// 	{
// 		equip->generalMainId = equip->mes_petid;
// 	}
	equip->stoneOfAmountRefine = 0;
}

CEquipment * GeneralEquipRecover::getEquipment()
{
	return equip_;
}

int GeneralEquipRecover::getEquipUseLevel()
{
	return m_useLevel;
}

void GeneralEquipRecover::setStarLevel( int level_,bool isAction )
{
	m_starLevel = level_;

	if (level_ > 0)
	{
		if (level_ >=20)
		{
			level_ =20;
		}
		char starLvLabel_[5];
		sprintf(starLvLabel_,"%d",level_);
		labelStarlevel_->setText(starLvLabel_);
		labelStarlevel_->setVisible(true);
		imageStar_->setPosition(ccp(labelStarlevel_->getContentSize().width-27,labelStarlevel_->getPosition().y));
		imageStar_->setVisible(true);

		if (isAction)
		{
			CCActionInterval * seq_ =(CCActionInterval *) CCSequence::create(
				CCScaleTo::create(0.01f,10.0f),
				CCScaleTo::create(0.3f,1.0f),
				NULL);
			labelStarlevel_->runAction(seq_);

			CCSequence * seq_1 = CCSequence::create(
				CCScaleTo::create(0.01f,5.0f),
				CCScaleTo::create(0.3f,0.5f),
				NULL);
			imageStar_->runAction(seq_1);
		}
	}
}

int GeneralEquipRecover::getStarLevel()
{
	return m_starLevel;
}

void GeneralEquipRecover::setRefineLevel( int level_,bool isAction )
{
	m_refineLevel = level_;

	if (level_ > 0)
	{
		if (level_ >=20)
		{
			level_ =20;
		}
		char strengthLv[5];
		sprintf(strengthLv,"%d",level_);
		std::string strengthStr_ = "+";
		strengthStr_.append(strengthLv);

		label_refineLv->setText(strengthStr_.c_str());
		label_refineLv->setVisible(true);

		if (isAction)
		{
			CCSequence * seq_ = CCSequence::create(
				CCScaleTo::create(0.01f,10.0f),
				CCScaleTo::create(0.3f,1.0f),
				NULL);
			label_refineLv->runAction(seq_);
		}
	}else
	{
		label_refineLv->setVisible(false);
	}
}

int GeneralEquipRecover::getRefineLevel()
{
	return m_refineLevel;
}

int GeneralEquipRecover::getClazz()
{
	return m_clazz;
}

int GeneralEquipRecover::getQuality()
{
	return m_quality;
}
