#include "EquipMentGeneralList.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../generals_ui/GeneralsUI.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../backpackscene/PackageScene.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "EquipMentUi.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CActiveRole.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../AppMacros.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../utils/StaticDataManager.h"
#include "../../utils/GameUtils.h"
#include "../../ui/extensions/RecruitGeneralCard.h"

EquipMentGeneralList::EquipMentGeneralList(void)
{
}


EquipMentGeneralList::~EquipMentGeneralList(void)
{
}

EquipMentGeneralList * EquipMentGeneralList::create()
{
	EquipMentGeneralList * generalList =new EquipMentGeneralList();
	if (generalList &&generalList->init())
	{
		generalList->autorelease();
		return generalList;
	}
	CC_SAFE_DELETE(generalList);
	return NULL;
}

bool EquipMentGeneralList::init()
{
	if(GeneralsListBase::init())
	{

		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		this->generalsListStatus = HaveNext;
		//general list
		generalList_tableView = CCTableView::create(this,CCSizeMake(340,106));
		generalList_tableView->setSelectedEnable(true);
		generalList_tableView->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 26, 1, 1), ccp(0,-2));
		generalList_tableView->setDirection(kCCScrollViewDirectionHorizontal);
		generalList_tableView->setAnchorPoint(ccp(0,0));
		generalList_tableView->setPosition(ccp(0,0));
		generalList_tableView->setContentOffset(ccp(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		generalList_tableView->setPressedActionEnabled(true);
		addChild(generalList_tableView);

		//add flag
		UILayer * tempLayer_flag = UILayer::create();
// 		tempLayer_flag->ignoreAnchorPointForPosition(false);
// 		tempLayer_flag->setAnchorPoint(ccp(0.5f,0.5f));
// 		tempLayer_flag->setContentSize(CCSizeMake(800, 480));
// 		tempLayer_flag->setPosition(ccp(winsize.width/2,winsize.height/2));
		tempLayer_flag->setZOrder(5);
		addChild(tempLayer_flag);

		UIImageView * imageView_leftFlag = UIImageView::create();
		imageView_leftFlag->setTexture("res_ui/shousuo.png");
		imageView_leftFlag->setAnchorPoint(ccp(0.5f,0.5f));
		//imageView_leftFlag->setPosition(ccp(382,92));
		imageView_leftFlag->setPosition(ccp(9,55));//-16  55
		imageView_leftFlag->setRotation(-135);
		imageView_leftFlag->setScale(0.5f);
		tempLayer_flag->addWidget(imageView_leftFlag);
		UIImageView * imageView_rightFlag = UIImageView::create();
		imageView_rightFlag->setTexture("res_ui/shousuo.png");
		imageView_rightFlag->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_rightFlag->setPosition(ccp(331,55));//703 92
		imageView_rightFlag->setRotation(45);
		imageView_rightFlag->setScale(0.5f);
		tempLayer_flag->addWidget(imageView_rightFlag);

		//open level
		int openlevel = 0;
		std::map<int,int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(26);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[26];
		}

		if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
		{
			UIButton * btn_mengban = UIButton::create();
			btn_mengban->setTouchEnable(true);
			btn_mengban->setTextures("res_ui/renwubeibao/mengban_green.png","res_ui/renwubeibao/mengban_green.png","");
			btn_mengban->setScale9Enable(true);
			btn_mengban->setScale9Size(CCSizeMake(343,108));
			btn_mengban->setAnchorPoint(ccp(0.5f,0.5f));
			btn_mengban->setPosition(ccp(169,51));
			btn_mengban->setOpacity(200);
			tempLayer_flag->addWidget(btn_mengban);
			UIImageView * image_desFrame = UIImageView::create();
			image_desFrame->setTexture("res_ui/renwubeibao/zi_mengban.png");
			image_desFrame->setScale9Enable(true);
			image_desFrame->setScale9Size(CCSizeMake(240,29));
			image_desFrame->setCapInsets(CCRectMake(56,11,1,1));
			image_desFrame->setAnchorPoint(ccp(0.5f,0.5f));
			image_desFrame->setPosition(ccp(169,51));
			image_desFrame->setOpacity(200);
			tempLayer_flag->addWidget(image_desFrame);
			std::string str_des = "";
			char s_lv [10];
			sprintf(s_lv,"%d",openlevel);
			str_des.append(s_lv);
			str_des.append(StringDataManager::getString("package_openGeneralEquipment"));
			UILabel * l_des = UILabel::create();
			l_des->setFontName(APP_FONT_NAME);
			l_des->setFontSize(20);
			l_des->setText(str_des.c_str());
			l_des->setAnchorPoint(ccp(0.5f,0.5f));
			l_des->setPosition(ccp(169,51));
			tempLayer_flag->addWidget(l_des);
		}

		return true;
	}
	return false;
}

void EquipMentGeneralList::onEnter()
{
	GeneralsListBase::onEnter();
}

void EquipMentGeneralList::onExit()
{
	GeneralsListBase::onExit();
}


void EquipMentGeneralList::scrollViewDidScroll( CCScrollView* view )
{

}

void EquipMentGeneralList::scrollViewDidZoom( CCScrollView* view )
{

}

void EquipMentGeneralList::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	int index_ = cell->getIdx();

	EquipMentUi * equipui =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	equipui->tabviewSelectIndex = index_;
	if (index_ == 0)
	{
		equipui->mes_petid = 0;
		equipui->refreshRoleInfo((CActiveRole*)GameView::getInstance()->myplayer->getActiveRole(),
									GameView::getInstance()->myplayer->player->fightpoint(),
									GameView::getInstance()->EquipListItem);
	}else
	{
		GeneralsSmallHeadCell * tempCell = dynamic_cast<GeneralsSmallHeadCell*>(cell);
		if (tempCell != NULL)
		{
			curGeneralBaseMsg = tempCell->getCurGeneralBaseMsg();
			equipui->mes_petid = curGeneralBaseMsg->id();
			//请求详细信息
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *) curGeneralBaseMsg->id());
		}
		
		/*
		GeneralsSmallHeadCell * tempCell = dynamic_cast<GeneralsSmallHeadCell*>(cell);
		if (tempCell != NULL)
		{
			curGeneralBaseMsg = tempCell->getCurGeneralBaseMsg();
			equipui->mes_petid = curGeneralBaseMsg->id();
		}
		std::vector<CEquipment *> tempGeneralEquipVector;
		CActiveRole * activeRole_ = new CActiveRole();
		int generalSize = GameView::getInstance()->generalsInLineDetailList.size();
		for (int i = 0;i<generalSize;i++)
		{
			if (equipui->mes_petid ==GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
			{
				activeRole_->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->activerole());
				int fight_ = GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint();

				int equipSize = GameView::getInstance()->generalsInLineDetailList.at(i)->equipments_size();
				for (int j = 0;j<equipSize;j++)
				{
					CEquipment * equip_ =new CEquipment();
					equip_->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->equipments(j));
					tempGeneralEquipVector.push_back(equip_);
				}
				equipui->refreshRoleInfo(activeRole_,fight_,tempGeneralEquipVector);
				break;
			}
		}

		std::vector<CEquipment*>::iterator iter;
		for (iter = tempGeneralEquipVector.begin(); iter != tempGeneralEquipVector.end(); ++iter)
		{
			delete *iter;
		}
		tempGeneralEquipVector.clear();

		delete activeRole_;
		*/
	}
}

cocos2d::CCSize EquipMentGeneralList::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(81,106);
}

cocos2d::extension::CCTableViewCell* EquipMentGeneralList::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called
	if (idx == 0)
	{
		cell = new CCTableViewCell();
		cell->autorelease();
		CCSprite *  smaillFrame = CCSprite::create("res_ui/generals_white.png");
		smaillFrame->setAnchorPoint(ccp(0, 0));
		smaillFrame->setPosition(ccp(0, 0));
		smaillFrame->setScaleX(0.75f);
		smaillFrame->setScaleY(0.7f);
		cell->addChild(smaillFrame);
		//列表中的头像图标
		CCSprite * sprite_icon = CCSprite::create(BasePlayer::getBigHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str());
		sprite_icon->setAnchorPoint(ccp(0.5f, 0.5f));
		sprite_icon->setPosition(ccp(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), smaillFrame->getContentSize().height/2*smaillFrame->getScaleY()));
		sprite_icon->setScale(0.75f);
		cell->addChild(sprite_icon);
		//等级
// 		std::string _lv = "LV";
// 		char s[10];
// 		sprintf(s,"%d",GameView::getInstance()->myplayer->getActiveRole()->level());
// 		_lv.append(s);
// 		CCLabelTTF * label_lv = CCLabelTTF::create(s,APP_FONT_NAME,13);
// 		label_lv->setAnchorPoint(ccp(1.0f, 0));
// 		label_lv->setPosition(ccp(smaillFrame->getContentSize().width*smaillFrame->getScaleX()-5, 20));
// 		label_lv->enableStroke(ccc3(0, 0, 0), 2.0f);
// 		cell->addChild(label_lv);
		//武将名字黑底
		CCScale9Sprite * sprite_nameFrame = CCScale9Sprite::create("res_ui/name_di3.png");
		sprite_nameFrame->setCapInsets(CCRectMake(10,10,1,5));
		sprite_nameFrame->setAnchorPoint(ccp(0.5f, 0));
		sprite_nameFrame->setContentSize(CCSizeMake(72,13));
		sprite_nameFrame->setPosition(ccp(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 8));
		cell->addChild(sprite_nameFrame);
		//武将名字
		const char * generalRoleName_ = StringDataManager::getString("generalList_RoleName");
		CCLabelTTF * label_name = CCLabelTTF::create(generalRoleName_,APP_FONT_NAME,14);
		label_name->setAnchorPoint(ccp(0.5f, 0));
		label_name->setPosition(ccp(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 5));
		//label_name->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		label_name->setColor(ccc3(0,255,0));
		cell->addChild(label_name);
		//profession
		std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(GameView::getInstance()->myplayer->getProfession());
		CCSprite * sprite_profession = CCSprite::create(generalProfess_.c_str());
		sprite_profession->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_profession->setPosition(ccp(62 ,
			32));
		sprite_profession->setScale(0.6f);
		cell->addChild(sprite_profession);
	}
	else
	{
		cell = GeneralsSmallHeadCell::create(GameView::getInstance()->generalsInLineList.at(idx-1));
	}
	return cell;
}

unsigned int EquipMentGeneralList::numberOfCellsInTableView( CCTableView *table )
{
	return GameView::getInstance()->generalsInLineList.size()+1;
}

void EquipMentGeneralList::ReloadGeneralsTableView()
{
	CCPoint offset = this->generalList_tableView->getContentOffset();
	this->generalList_tableView->reloadData();
}

void EquipMentGeneralList::ReloadGeneralsTableViewWithoutChangeOffSet()
{
	CCPoint offset = this->generalList_tableView->getContentOffset();
	this->generalList_tableView->reloadData();
	this->generalList_tableView->setContentOffset(offset);
}

