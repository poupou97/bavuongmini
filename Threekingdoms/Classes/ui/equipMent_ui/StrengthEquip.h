#ifndef _EQUIPMENTUI_STRENGTHEQUIPMENT_H
#define _EQUIPMENTUI_STRENGTHEQUIPMENT_H

#include "../extensions/UIScene.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"

class FolderInfo;
USING_NS_CC;
USING_NS_CC_EXT;
class StrengthEquip:public UIScene
{
public:
	StrengthEquip(void);
	~StrengthEquip(void);

	static StrengthEquip *create(FolderInfo* folder,bool isMainequip);
	bool init(FolderInfo* folder,bool isMainequip);

	void onEnter();
	void onExit();

	void showEquipsInfo(CCObject * obj);
	
	void setStarLevel(int level_,bool isAction = false);
	int getStarLevel();
	void setRefineLevel(int level_,bool isAction = false);
	int getRefineLevel();

	int getClazz();
	int getQuality();

	int getRefineUseLevel();
	int packIndex;
	UIButton * Btn_pacItemFrame;
	UILabel * labelStarlevel;
	UIImageView * imageStar_;

	UILabel * label_StrengthLv;
private:
	int m_starLevel;
	int m_refineLevel;
	int m_refineUselevlel;
	int m_clazz;
	int m_quality;
};

//////////////////////////
class EquipStrengthInfo:public GoodsItemInfoBase
{
public:
	EquipStrengthInfo();
	~EquipStrengthInfo();

	static EquipStrengthInfo * create(FolderInfo* folder,std::vector<CEquipment *>equipVector,long long generalId);
	bool init(FolderInfo* folder,std::vector<CEquipment *>equipVector,long long generalId);

	virtual void onEnter();
	virtual void onExit();

	///refine main equipment
	void refineMainEquip(CCObject * obj);
	virtual bool ccTouchBegan(CCTouch * pTouch,CCEvent * pEvent);
};
#endif;