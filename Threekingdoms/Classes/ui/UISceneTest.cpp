#include "UISceneTest.h"

#include "../loadscene_state/LoadSceneState.h"

UISceneTest::UISceneTest()
{
}

UISceneTest::~UISceneTest()
{
    ActionManager::purgeActionManager();
	GUIReader::shareReader()->purgeGUIReader();

	//CCTextureCache::sharedTextureCache()->removeUnusedTextures();
}

UISceneTest * UISceneTest::create()
{
	UISceneTest * _ui = new UISceneTest();
	if(_ui && _ui->init())
	{
		_ui->autorelease();
		return _ui;
	}
	CC_SAFE_DELETE(_ui);
	return NULL;
}

bool UISceneTest::init()
{
	if (UIScene::init())
	{
		CCSize s=CCDirector::sharedDirector()->getVisibleSize();

		UILayer* uiWidgetLayer = UILayer::create();
		addChild(uiWidgetLayer);
		uiWidgetLayer->setPosition(ccp(0,0));

		//UIPanel * ppanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/jishouhang_1.json");
		UIPanel * ppanel = UIPanel::create();
		ppanel->setAnchorPoint(ccp(0,0));
		ppanel->setPosition(ccp(0,0));
		uiWidgetLayer->addWidget(ppanel);

		//UIPanel * ppanel = NULL;
		//if(LoadSceneLayer::friendPanelBg->getWidgetParent() != NULL)
		//{
		//	LoadSceneLayer::friendPanelBg->removeFromParentAndCleanup(false);
		//}
		//ppanel = LoadSceneLayer::friendPanelBg;
		//uiWidgetLayer->addWidget(ppanel);

		//UIPanel * ppanel_1 = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/haoyou_1.json");
		////UIPanel * ppanel = UIPanel::create();
		//ppanel_1->setAnchorPoint(ccp(0,0));
		//ppanel_1->setPosition(ccp(0,0));
		//uiWidgetLayer->addWidget(ppanel_1);

		for(int i = 0; i < 50; i++)
		{
		//UILabel * labelText= UILabel::create();
		//labelText->setAnchorPoint(ccp(0.5f,0.5f));
		//labelText->setPosition(ccp(ppanel->getContentSize().width/2,ppanel->getContentSize().height*2/3));
		//labelText->setFontSize(24);
		//labelText->setText("hahaha");
		//labelText->setWidgetZOrder(10);
		//ppanel->addChild(labelText);
		//m_pUiLayer->addWidget(labelText);
		}

		for(int i = 0; i < 150; i++)
		{
			UIImageView * ImageView_chat = UIImageView::create();
			ImageView_chat->setScale9Enable(true);
			ImageView_chat->setTexture("gamescene_state/ditu/ditu_di.png");
			ImageView_chat->setScale9Size(CCSizeMake(380, 80));
			ImageView_chat->setAnchorPoint(CCPointZero);
			ImageView_chat->setPosition(ccp(0,0));
			ppanel->addChild(ImageView_chat);
			//uiWidgetLayer->addWidget(ImageView_chat);
		}

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(ccp(s.width,s.height));
		
		return true;
	}
	return false;
}

void UISceneTest::onEnter()
{
	UIScene::onEnter();
}

void UISceneTest::onExit()
{
	UIScene::onExit();
}

bool UISceneTest::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	return true;
}
void UISceneTest::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void UISceneTest::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{
	this->removeFromParent();
	CCLOG("removeUISceneTest");
}
void UISceneTest::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}
