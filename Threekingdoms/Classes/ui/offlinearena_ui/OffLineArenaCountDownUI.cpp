#include "OffLineArenaCountDownUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "AppMacros.h"
#include "../../utils/GameUtils.h"
#include "OffLineArenaState.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "OffLineArenaData.h"


OffLineArenaCountDownUI::OffLineArenaCountDownUI():
m_nRemainTime(5000)
{
}


OffLineArenaCountDownUI::~OffLineArenaCountDownUI()
{
}

OffLineArenaCountDownUI * OffLineArenaCountDownUI::create()
{
	OffLineArenaCountDownUI * offLineArenaCountDownUI = new OffLineArenaCountDownUI();
	if (offLineArenaCountDownUI && offLineArenaCountDownUI->init())
	{
		offLineArenaCountDownUI->autorelease();
		return offLineArenaCountDownUI;
	}
	CC_SAFE_DELETE(offLineArenaCountDownUI);
	return NULL;
}

bool OffLineArenaCountDownUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		//����ʱ��ʾ
// 		l_countDown = UILabel::create();
// 		char s_countDown[10];
// 		sprintf(s_countDown,"%d",m_nRemainTime/1000);
// 		l_countDown->setText(s_countDown);
// 		l_countDown->setAnchorPoint(ccp(0.5f,0.5f));
// 		l_countDown->setFontName(APP_FONT_NAME);
// 		l_countDown->setFontSize(40);
// 		l_countDown->setScale(2.5f);
// 		l_countDown->setPosition(ccp(winsize.width/2,winsize.height*0.8f));
// 		m_pUiLayer->addWidget(l_countDown);
		
		UIImageView * background_ =UIImageView::create();
		background_->setTexture("res_ui/zhezhao80.png");
		background_->setAnchorPoint(ccp(0.5f,0.5f));
		background_->setScale9Enable(true);
		background_->setScale9Size(CCSizeMake(winsize.width,80));
		//background_->setCapInsets(CCRect(50,50,1,1));
		background_->setPosition(ccp(-background_->getContentSize().width/2,winsize.height*0.8f));
		m_pUiLayer->addWidget(background_);
		background_->runAction(CCMoveTo::create(0.5f,ccp(winsize.width/2,winsize.height*0.8f)));
		

		imageView_countDown = UIImageView::create();
		imageView_countDown->setTexture("res_ui/font/5.png");
		imageView_countDown->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_countDown->setPosition(ccp(winsize.width+ imageView_countDown->getContentSize().width,winsize.height*0.8f));
		m_pUiLayer->addWidget(imageView_countDown);

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCShow::create(),
			CCMoveTo::create(0.5f,ccp(winsize.width/2,winsize.height*0.8f)),
			CCDelayTime::create(0.5f),
			CCCallFunc::create(this,callfunc_selector(OffLineArenaCountDownUI::showStarCountTime)),
			NULL);
		imageView_countDown->runAction(action);

		this->setContentSize(winsize);
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void OffLineArenaCountDownUI::onEnter()
{
	UIScene::onEnter();
}

void OffLineArenaCountDownUI::onExit()
{
	UIScene::onExit();
}

bool OffLineArenaCountDownUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void OffLineArenaCountDownUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void OffLineArenaCountDownUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void OffLineArenaCountDownUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void OffLineArenaCountDownUI::update( float dt )
{
	m_nRemainTime -= 1000;
	if (m_nRemainTime <= 0)
	{
		m_nRemainTime = 0;
		OffLineArenaState::setIsCanTouch(true);

		OffLineArenaData::getInstance()->setCurStatus(OffLineArenaData::s_inBattle);

		this->removeFromParent();
	}

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	std::string str_path = "res_ui/font/";
	str_path.append(s_remainTime);
	str_path.append(".png");
	imageView_countDown->setTexture(str_path.c_str());
	//l_countDown->setText(s_remainTime);
}

void OffLineArenaCountDownUI::showStarCountTime()
{
	float space = (m_nRemainTime-(GameUtils::millisecondNow() - OffLineArenaState::getStartTime()))*1.0f/m_nRemainTime;
	this->schedule(schedule_selector(OffLineArenaCountDownUI::update),space*4.0f/5.0f);
}
