
#ifndef _OFFLINEARENA_OFFLINEARENAUI_
#define _OFFLINEARENA_OFFLINEARENAUI_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/ScriptHandlerProtocol.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CHonorCommodity;
class CPartner;
class ArenaRoleInfo;

#define KTagRankListUI 400
#define KTagFightNotesUI 410
#define KTagHonorShopUI 420
#define KTagBuyFightTimesUI 430
#define ArenaRoleInfo_BaseTag 100

class OffLineArenaUI : public UIScene
{
public:
	OffLineArenaUI();
	~OffLineArenaUI();

	struct ReqForChallenge{
		long long myRanking;
		long long targetId;
		long long targetRanking;
	};

	static OffLineArenaUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void CloseEvent(CCObject *pSender);
	void RankListEvent(CCObject *pSender);
	void HonorShopEvent(CCObject *pSender);
	void FightNotesEvent(CCObject *pSender);
	void ChallengeEvent(CCObject *pSender);
	void BuyNumEvent(CCObject *pSender);
	void GetRewardsEvent(CCObject *pSender);

	void setRemaintimes(long long  _value);
	int getAllFreeFightTimes();
	int getRemainBuyTimes();

	void setCurSelectIndex(int _value);

	//刷新我的排名
	void RefreshMyRankingConfig(int ranking);
	int getMyRoleRankValue();

	//刷新整个排名显示
	void RefreshArenaDisplay(std::vector<CPartner *> arenaList);

	//刷新荣誉值显示
	void RefreshHonorConfig(int honorValue,long long remainTime);
	//刷新战斗次数显示
	void RefreshFightConfig(int fightTimes,int allFreeTimes,int remainBuyTimes);

	//刷新是否选中
	void RefreshSelectState();


	//教学
	virtual void registerScriptCommand(int scriptId);
	//选一个对手
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	//挑战按钮
	void addCCTutorialIndicator2(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();

	ArenaRoleInfo * getTutorialRoleItem();
	int getTutorialRoleItemIdx();

private:
	virtual void update(float dt);

	std::string timeFormatToString(long long t);

public:
	UIButton * btn_challenge;
private:
	UILayer * u_layer;
	UIPanel * ppanel;

	//领取奖励
	UIButton * btn_getRewards;
	//我的排名
	UIImageView * imageView_di;
	UIImageView * imageView_ming;
	UILabelBMFont * l_roleRankingValue;
	int m_nRoleRanking;
	//我的荣誉
	UILabel * l_roleHonorValue;
	int m_nRoleHonor;
	//每一段时间多少荣誉值
	UILabel * l_honorValue;
	int m_nHonor;
	//剩余领取荣誉值时间
	UILabel * l_remainTimeDes;
	UILabel * l_remainTime;
	long long  m_nRemaintimes;

	//总免费挑战次数
	int  m_nAllFreeFighttimes;
	//剩余挑战次数
	int  m_nFighttimes;
	//剩余购买次数
	int m_nRemainBuytimes;
	UILabel * l_fightTime;
	UIImageView * imageView_remainTime;

	//当前选中的位置
	int m_nCurSelectIndex;

	
public:
	std::vector<CPartner *> curArenaList;
	//荣誉商店列表
	std::vector<CHonorCommodity*> curHonorCommodityList;
    //排行榜
	//std::vector<CPartner*> curRankingList;
	
	//教学
	int mTutorialScriptInstanceId;


};


////////////////////////////人物信息//////////////////////////////
class ArenaRoleInfo : public UIScene
{
public:
	ArenaRoleInfo();
	~ArenaRoleInfo();

	static ArenaRoleInfo * create(CPartner * partner,int index);
	bool init(CPartner * partner,int index);

	void LookOverEvent(CCObject *pSender);
	void BigFrameEvent(CCObject *pSender);

	void selected();
	void unSelected();

	int getCurIndex();

private:
	CPartner * curPartner;
	int m_nIndex;

	UIImageView * imageView_select;
	//底
	UIImageView * imageView_di;

public:
	UIButton * btn_lookOver;

};


#endif

