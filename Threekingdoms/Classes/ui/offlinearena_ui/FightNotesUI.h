
#ifndef _OFFLINEARENA_FIGHTNOTESUI_
#define _OFFLINEARENA_FIGHTNOTESUI_

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCMoveableMenu;
class CHistory;

class FightNotesUI : public UIScene,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	FightNotesUI();
	~FightNotesUI();

	static FightNotesUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject *pSender);

	void RefreshData();

	void StrikeBackEvent(CCObject *pSender);
	void LookOverEvent(CCObject *pSender);

	std::string getFightNotesDes(CHistory * history);

private:
	CCTableView * m_tableView;
    CCMoveableMenu * menu_strikeBack;
	CCMoveableMenu * menu_lookOver;

	int lastSelectCellId;
	int selectCellId;

public: 
	//历史记录
	std::vector<CHistory*> curHistoryList;
};

#endif

