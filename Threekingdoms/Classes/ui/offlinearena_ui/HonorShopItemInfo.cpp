#include "HonorShopItemInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/element/CHonorCommodity.h"


HonorShopItemInfo::HonorShopItemInfo()
{
}


HonorShopItemInfo::~HonorShopItemInfo()
{
}

HonorShopItemInfo * HonorShopItemInfo::create( CHonorCommodity * honorCommodity )
{
	HonorShopItemInfo * honorShopItemInfo = new HonorShopItemInfo();
	if (honorShopItemInfo && honorShopItemInfo->init(honorCommodity))
	{
		honorShopItemInfo->autorelease();
		return honorShopItemInfo;
	}
	CC_SAFE_DELETE(honorShopItemInfo);
	return NULL;
}

bool HonorShopItemInfo::init( CHonorCommodity * honorCommodity )
{
	GoodsInfo * goodsInfo = new GoodsInfo();
	goodsInfo->CopyFrom(honorCommodity->goods());
	if (GoodsItemInfoBase::init(goodsInfo,GameView::getInstance()->EquipListItem,0))
	{
		delete goodsInfo;
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setAnchorPoint(ccp(0,0));
		return true;
	}
	return false;
}

void HonorShopItemInfo::onEnter()
{
	GoodsItemInfoBase::onEnter();
	this->openAnim();
}

void HonorShopItemInfo::onExit()
{
	GoodsItemInfoBase::onExit();
}

bool HonorShopItemInfo::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return resignFirstResponder(pTouch,this,true);
}

void HonorShopItemInfo::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void HonorShopItemInfo::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void HonorShopItemInfo::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

