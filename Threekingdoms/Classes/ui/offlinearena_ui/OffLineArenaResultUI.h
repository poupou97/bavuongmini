

#ifndef _OFFLINEARENA_OFFLINEARENARESULTUI_
#define _OFFLINEARENA_OFFLINEARENARESULTUI_

#include "../extensions/UIScene.h"

class OffLineArenaResultUI : public UIScene
{
public:
	OffLineArenaResultUI();
	~OffLineArenaResultUI();

	static OffLineArenaResultUI * create(bool isWin,int exp);
	bool init(bool isWin,int exp);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void GetRewardEvent(CCObject *pSender);
	void FightAgainEvent(CCObject *pSender);

	virtual void update(float dt);

	void ArmatureFinishCallback(cocos2d::extension::CCArmature *armature, cocos2d::extension::MovementEventType movementType, const char *movementID);

protected:
	void exitArena();

	//胜利或失败动画
	void createAnimation();
	//显示名次变化
	void showRankingAnimation();
	void createUI();

	void showUIAnimation();
	void startSlowMotion();

	void addLightAnimation();

public:
	void endSlowMotion();

private:
	UIPanel * mainPanel;
	UILayer * u_layer;
	UILabel *l_remainTime;
	int m_nRemainTime;

	bool m_bIsWin;
	int m_nExp;

	long long countDown_startTime;
};

#endif

