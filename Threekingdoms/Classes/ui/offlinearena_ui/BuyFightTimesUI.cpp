#include "BuyFightTimesUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "OffLineArenaUI.h"
#include "GameView.h"
#include "../../gamescene_state/GameScene.h"
#include "../../gamescene_state/MainScene.h"

BuyFightTimesUI::BuyFightTimesUI()
{
}

BuyFightTimesUI::~BuyFightTimesUI()
{
}

BuyFightTimesUI * BuyFightTimesUI::create()
{
	BuyFightTimesUI * buyFightTimesUI = new BuyFightTimesUI();
	if (buyFightTimesUI && buyFightTimesUI->init())
	{
		buyFightTimesUI->autorelease();
		return buyFightTimesUI;
	}
	CC_SAFE_DELETE(buyFightTimesUI);
	return NULL;
}

bool BuyFightTimesUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate(); 

		//����UI
		UIPanel *ppanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/arena_popupo2_1.json");
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnable(true);
		btn_close->addReleaseEvent(this, coco_releaseselector(BuyFightTimesUI::CloseEvent));
		btn_close->setPressedActionEnabled(true);

		UIButton * btn_oneTimes = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_take");
		btn_oneTimes->setTouchEnable(true);
		btn_oneTimes->addReleaseEvent(this, coco_releaseselector(BuyFightTimesUI::BuyOneTimesEvent));
		btn_oneTimes->setPressedActionEnabled(true);

		UIButton * btn_fiveTimes = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_take1");
		btn_fiveTimes->setTouchEnable(true);
		btn_fiveTimes->addReleaseEvent(this, coco_releaseselector(BuyFightTimesUI::BuyFiveTimesEvent));
		btn_fiveTimes->setPressedActionEnabled(true);

		UIButton * btn_tenTimes = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_take2");
		btn_tenTimes->setTouchEnable(true);
		btn_tenTimes->addReleaseEvent(this, coco_releaseselector(BuyFightTimesUI::BuyTenTimesEvent));
		btn_tenTimes->setPressedActionEnabled(true);

		l_remainTime = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_number");

		this->setContentSize(ppanel->getContentSize());
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void BuyFightTimesUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void BuyFightTimesUI::onExit()
{
	UIScene::onExit();
}

bool BuyFightTimesUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return resignFirstResponder(touch,this,false);
}

void BuyFightTimesUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void BuyFightTimesUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void BuyFightTimesUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void BuyFightTimesUI::BuyOneTimesEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(3005,(void *)1);
}

void BuyFightTimesUI::BuyFiveTimesEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(3005,(void *)5);
}

void BuyFightTimesUI::BuyTenTimesEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(3005,(void *)10);
}

void BuyFightTimesUI::update( float dt )
{
	OffLineArenaUI * offLineArenaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
	if (!offLineArenaUI)
		return;

	char s_remainBuyTimes[10];
	sprintf(s_remainBuyTimes,"%d",offLineArenaUI->getRemainBuyTimes());
	l_remainTime->setText(s_remainBuyTimes);
}

void BuyFightTimesUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

