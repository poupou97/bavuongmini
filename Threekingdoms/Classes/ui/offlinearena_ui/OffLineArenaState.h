#ifndef _OFFLINEARENAUI_OFFLINEARENASTATE_H_
#define _OFFLINEARENAUI_OFFLINEARENASTATE_H_

class OffLineArenaState
{
public:
	enum PlayerArenaStatus {
		status_none = 0,
		status_in_arena,   
	};

public:
	OffLineArenaState();
	~OffLineArenaState();

	static void init();

	static void setStatus(int status);
	static int getStatus();

	static void setCurrentOpppnentId(int id);
	static int getCurrentOpppnentId();

	static void setStartTime(long long time);
	static int getStartTime();

	static void setIsCanTouch(bool isCan);
	static int getIsCanTouch();

private:
	static int s_status;
	static int s_currentOpponent;

	static long long m_nStartTime;

	static bool m_bIsCanTouch;
};

#endif