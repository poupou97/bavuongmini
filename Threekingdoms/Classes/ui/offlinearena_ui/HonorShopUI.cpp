#include "HonorShopUI.h"
#include "../extensions/UIScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CHonorCommodity.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/GameScene.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"
#include "GameView.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../utils/StaticDataManager.h"
#include "HonorShopItemInfo.h"
#include "HonorShopItemBuyInfo.h"


HonorShopUI::HonorShopUI()
{
}


HonorShopUI::~HonorShopUI()
{
	std::vector<CHonorCommodity*>::iterator iter;
	for (iter = curHonorCommodityList.begin(); iter != curHonorCommodityList.end(); ++iter)
	{
		delete *iter;
	}
	curHonorCommodityList.clear();
}

HonorShopUI * HonorShopUI::create(std::vector<CHonorCommodity*> honorCommodityList)
{
	HonorShopUI * honorShopUI = new HonorShopUI();
	if (honorShopUI && honorShopUI->init(honorCommodityList))
	{
		honorShopUI->autorelease();
		return honorShopUI;
	}
	CC_SAFE_DELETE(honorShopUI);
	return NULL;
}

bool HonorShopUI::init(std::vector<CHonorCommodity*> honorCommodityList)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate(); 
		//refresh data
		//delete old
		std::vector<CHonorCommodity*>::iterator iter;
		for (iter = curHonorCommodityList.begin(); iter != curHonorCommodityList.end(); ++iter)
		{
			delete *iter;
		}
		curHonorCommodityList.clear();
		//add new 
		for(int i = 0;i<(int)honorCommodityList.size();++i)
		{
			CHonorCommodity * honorCommodity = new CHonorCommodity();
			honorCommodity->CopyFrom(*honorCommodityList.at(i));
			curHonorCommodityList.push_back(honorCommodity);
		}

		//create UI
		//����UI
		if(LoadSceneLayer::RankListLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::RankListLayer->removeFromParentAndCleanup(false);
		}
		UIPanel *ppanel = LoadSceneLayer::RankListLayer;
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(608, 417));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIPanel * panel_fightNotes = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_conbatRecord");
		panel_fightNotes->setVisible(false);
		UIPanel * panel_rankList = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_ranking");
		panel_rankList->setVisible(false);
		UIPanel * panel_honorShop = (UIPanel *)UIHelper::seekWidgetByName(ppanel,"Panel_honorShop");
		panel_honorShop->setVisible(true);

		UIButton *Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(HonorShopUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		l_honorValue = (UILabel *)UIHelper::seekWidgetByName(panel_honorShop,"Label_honorValue");

		m_tableView = CCTableView::create(this,CCSizeMake(498,262));
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0,0));
		m_tableView->setPosition(ccp(52,74));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(m_tableView);

// 		UILayer * u_tableViewFrameLayer = UILayer::create();
// 		addChild(u_tableViewFrameLayer);
// 		UIImageView * tableView_Frame = UIImageView::create();
// 		tableView_Frame->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		tableView_Frame->setScale9Enable(true);
// 		tableView_Frame->setScale9Size(CCSizeMake(515,277));
// 		tableView_Frame->setCapInsets(CCRectMake(32,32,1,1));
// 		tableView_Frame->setAnchorPoint(ccp(0.5f,0.5f));
// 		tableView_Frame->setPosition(ccp(299+3,198+6));
// 		u_tableViewFrameLayer->addWidget(tableView_Frame);

		this->setContentSize(CCSizeMake(608,417));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void HonorShopUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void HonorShopUI::onExit()
{
	UIScene::onExit();
}

bool HonorShopUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void HonorShopUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void HonorShopUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void HonorShopUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void HonorShopUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void HonorShopUI::scrollViewDidScroll( CCScrollView* view )
{

}

void HonorShopUI::scrollViewDidZoom( CCScrollView* view )
{

}

void HonorShopUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{

}

cocos2d::CCSize HonorShopUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(495,70);
}

cocos2d::extension::CCTableViewCell* HonorShopUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	if (idx == curHonorCommodityList.size()/2)
	{
		if (curHonorCommodityList.size()%2 == 0)
		{
			HonorShopCellItem * cellItem1 = HonorShopCellItem::create(curHonorCommodityList.at(2*idx));
			cellItem1->setPosition(ccp(5,0));
			cell->addChild(cellItem1);

			HonorShopCellItem * cellItem2 = HonorShopCellItem::create(curHonorCommodityList.at(2*idx+1));
			cellItem2->setPosition(ccp(253,0));
			cell->addChild(cellItem2);
		}
		else
		{
			HonorShopCellItem * cellItem1 = HonorShopCellItem::create(curHonorCommodityList.at(2*idx));
			cellItem1->setPosition(ccp(5,0));
			cell->addChild(cellItem1);
		}
	}
	else
	{
		HonorShopCellItem * cellItem1 = HonorShopCellItem::create(curHonorCommodityList.at(2*idx));
		cellItem1->setPosition(ccp(5,0));
		cell->addChild(cellItem1);

		HonorShopCellItem * cellItem2 = HonorShopCellItem::create(curHonorCommodityList.at(2*idx+1));
		cellItem2->setPosition(ccp(253,0));
		cell->addChild(cellItem2);
	}

	return cell;
}

unsigned int HonorShopUI::numberOfCellsInTableView( CCTableView *table )
{
	if (curHonorCommodityList.size()%2 == 0)
	{
		return curHonorCommodityList.size()/2;
	}
	else
	{
		return curHonorCommodityList.size()/2 + 1;
	}
}

void HonorShopUI::update( float dt )
{
	char s_honor [20];
	sprintf(s_honor,"%d",GameView::getInstance()->myplayer->player->honorpoint());
	l_honorValue->setText(s_honor);
}








//////////////////////////////////////////////////////////////////
HonorShopCellItem::HonorShopCellItem()
{
	curHonorCommodity = new CHonorCommodity();
}

HonorShopCellItem::~HonorShopCellItem()
{
	delete curHonorCommodity;
}

HonorShopCellItem* HonorShopCellItem::create( CHonorCommodity * honorCommodity )
{
	HonorShopCellItem * honorShopCellItem = new HonorShopCellItem();
	if (honorShopCellItem && honorShopCellItem->init(honorCommodity))
	{
		honorShopCellItem->autorelease();
		return honorShopCellItem;
	}
	CC_SAFE_DELETE(honorShopCellItem);
	return NULL;
}

bool HonorShopCellItem::init( CHonorCommodity * honorCommodity )
{
	if (CCLayer::init())
	{
		curHonorCommodity->CopyFrom(*honorCommodity);

		//��߿�
		CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/kuang0_new.png");
		sprite_bigFrame->setAnchorPoint(ccp(0, 0));
		sprite_bigFrame->setPosition(ccp(0, 0));
		sprite_bigFrame->setPreferredSize(CCSizeMake(244,68));
		addChild(sprite_bigFrame);

// 		CCScale9Sprite * spbigFrame = CCScale9Sprite::create("res_ui/kuang0_new.png");
// 		spbigFrame->setPreferredSize(CCSizeMake(71,39));
// 		spbigFrame->setCapInsets(CCRect(18,9,2,23));
// 		spbigFrame->setPreferredSize(CCSizeMake(244,68));
// 		CCMenuItemSprite *itemSprite_bigFrame = CCMenuItemSprite::create(spbigFrame, spbigFrame, spbigFrame, this, menu_selector(HonorShopCellItem::DetailEvent));
// 		itemSprite_bigFrame->setZoomScale(1.0f);
// 		CCMoveableMenu *btn_bigFrame = CCMoveableMenu::create(itemSprite_bigFrame,NULL);
// 		btn_bigFrame->setAnchorPoint(ccp(0,0));
// 		btn_bigFrame->setPosition(ccp(122,34));
// 		addChild(btn_bigFrame);

		//��ƷIcon�׿�
		std::string iconFramePath;
		switch(curHonorCommodity->goods().quality())
		{
		case 1 :
			{
				iconFramePath = "res_ui/smdi_white.png";
			}
			break;
		case 2 :
			{
				iconFramePath = "res_ui/smdi_green.png";
			}
			break;
		case 3 :
			{
				iconFramePath = "res_ui/smdi_bule.png";
			}
			break;
		case 4 :
			{
				iconFramePath = "res_ui/smdi_purple.png";
			}
			break;
		case 5 :
			{
				iconFramePath = "res_ui/smdi_orange.png";
			}
			break;
		}
// 		CCSprite *  smaillFrame1 = CCSprite::create(iconFramePath.c_str());
// 		CCSprite *  smaillFrame2 = CCSprite::create(iconFramePath.c_str());
// 		CCSprite *  smaillFrame3 = CCSprite::create(iconFramePath.c_str());
// 		CCMenuItemSprite *itemSprite_smallFrame = CCMenuItemSprite::create(smaillFrame1, smaillFrame2, smaillFrame3, this, menu_selector(HonorShopCellItem::DetailEvent));
// 		itemSprite_smallFrame->setZoomScale(1.0f);
// 		CCMenuItemImage* itemSprite_smallFrame = CCMenuItemImage::create(iconFramePath.c_str(), iconFramePath.c_str(),this, menu_selector(HonorShopCellItem::DetailEvent) );
// 		CCMoveableMenu *btn_smallFrame = CCMoveableMenu::create(itemSprite_smallFrame,NULL);
// 		btn_smallFrame->setAnchorPoint(ccp(0,0));
// 		btn_smallFrame->setPosition(ccp(10,10));
// 		addChild(btn_smallFrame);

// 		CCMenuItemImage *item_image = CCMenuItemImage::create(
// 		 	iconFramePath.c_str(),
// 		 	iconFramePath.c_str(),
// 		 	this,
// 		 	menu_selector(HonorShopCellItem::DetailEvent));
// 		CCMenu* btn_smallFrame = CCMenu::create(item_image, NULL);
// 		btn_smallFrame->setAnchorPoint(CCPointZero);
// 		btn_smallFrame->setPosition(ccp(0,0));
// 		addChild(btn_smallFrame);

		CCScale9Sprite * s_normalImage_1 = CCScale9Sprite::create(iconFramePath.c_str());
		s_normalImage_1->setPreferredSize(CCSizeMake(47,47));
		CCMenuItemSprite *firstMenuImage = CCMenuItemSprite::create(s_normalImage_1, s_normalImage_1, s_normalImage_1, this, menu_selector(HonorShopCellItem::DetailEvent));
		firstMenuImage->setZoomScale(1.0f);
		firstMenuImage->setAnchorPoint(ccp(0.5f,0.5f));
		CCMoveableMenu *firstMenu = CCMoveableMenu::create(firstMenuImage, NULL);
		firstMenu->setContentSize(CCSizeMake(47,47));
		firstMenu->setPosition(ccp(37,sprite_bigFrame->getContentSize().height/2+1));
		addChild(firstMenu);
		
		std::string pheadPath = "res_ui/props_icon/";
		pheadPath.append(curHonorCommodity->goods().icon().c_str());
		pheadPath.append(".png");
		CCSprite * phead = CCSprite::create(pheadPath.c_str());
		phead->ignoreAnchorPointForPosition(false);
		phead->setAnchorPoint(ccp(0.5f, 0.5f));
		phead->setPosition(ccp(37, sprite_bigFrame->getContentSize().height/2+1));
		phead->setScale(0.85f);
		addChild(phead);

		CCLabelTTF * pName = CCLabelTTF::create(curHonorCommodity->goods().name().c_str(),APP_FONT_NAME,18);
		pName->setAnchorPoint(ccp(0.5f,0.5f));
		pName->setPosition(ccp(112,44));
		pName->setColor(GameView::getInstance()->getGoodsColorByQuality(curHonorCommodity->goods().quality()));
		addChild(pName);

		char s_honor[20];
		sprintf(s_honor,"%d",curHonorCommodity->honor());
		CCLabelTTF * pPrice = CCLabelTTF::create(s_honor,APP_FONT_NAME,16);
		pPrice->setAnchorPoint(ccp(0.5f,0.5f));
		pPrice->setPosition(ccp(112,25));
		addChild(pPrice);

		CCScale9Sprite * spBg = CCScale9Sprite::create("res_ui/new_button_2.png");
		spBg->setPreferredSize(CCSizeMake(71,39));
		spBg->setCapInsets(CCRect(18,9,2,23));
		CCMenuItemSprite *itemSprite = CCMenuItemSprite::create(spBg, spBg, spBg, this, menu_selector(HonorShopCellItem::BuyEvent));
		itemSprite->setZoomScale(1.3f);
		CCMoveableMenu *btn_buy = CCMoveableMenu::create(itemSprite,NULL);
		btn_buy->setAnchorPoint(ccp(0.5f,0.5f));
		btn_buy->setPosition(ccp(206,34));
		addChild(btn_buy);

		const char *str1 = StringDataManager::getString("goods_auction_buy");
		char* p1 =const_cast<char*>(str1);
		CCLabelTTF *l_buy=CCLabelTTF::create(p1,APP_FONT_NAME,18);
		l_buy->setAnchorPoint(ccp(0.5f,0.5f));
		l_buy->setPosition(ccp(71/2,39/2));
		itemSprite->addChild(l_buy);

		this->setContentSize(CCSizeMake(244,68));

		return true;
	}
	return false;
}

void HonorShopCellItem::BuyEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHonorShopItemBuyInfo) == NULL)
	{
 	 	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
 	 	HonorShopItemBuyInfo * honorShopItemBuyInfo = HonorShopItemBuyInfo::create(curHonorCommodity);
 	 	honorShopItemBuyInfo->ignoreAnchorPointForPosition(false);
 	 	honorShopItemBuyInfo->setAnchorPoint(ccp(0.5f,0.5f));
 	 	honorShopItemBuyInfo->setPosition(ccp(winSize.width/2,winSize.height/2));
		honorShopItemBuyInfo->setTag(kTagHonorShopItemBuyInfo);
 	 	GameView::getInstance()->getMainUIScene()->addChild(honorShopItemBuyInfo);
	}
}

void HonorShopCellItem::DetailEvent( CCObject * pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHonorShopItemInfo) == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		HonorShopItemInfo * honorShopItemInfo = HonorShopItemInfo::create(curHonorCommodity);
		honorShopItemInfo->ignoreAnchorPointForPosition(false);
		honorShopItemInfo->setAnchorPoint(ccp(0.5f,0.5f));
		//honorShopItemInfo->setPosition(ccp(winSize.width/2,winSize.height/2));
		honorShopItemInfo->setTag(kTagHonorShopItemInfo);
		GameView::getInstance()->getMainUIScene()->addChild(honorShopItemInfo);
	}
}
