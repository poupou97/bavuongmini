
#ifndef _OFFLINEARENA_BUYFIGHTTIMESUI_H_
#define _OFFLINEARENA_BUYFIGHTTIMESUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class BuyFightTimesUI : public UIScene
{
public:
	BuyFightTimesUI();
	~BuyFightTimesUI();

	static BuyFightTimesUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void CloseEvent(CCObject *pSender);
	void BuyOneTimesEvent(CCObject *pSender);
	void BuyFiveTimesEvent(CCObject *pSender);
	void BuyTenTimesEvent(CCObject *pSender);

	virtual void update(float dt);

	//ʣ�๺�����
	UILabel * l_remainTime;

};

#endif

