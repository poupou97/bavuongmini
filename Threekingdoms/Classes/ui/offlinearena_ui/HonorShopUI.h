
#ifndef _OFFLINEARENA_HONORSHOPUI_
#define _OFFLINEARENA_HONORSHOPUI_

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CHonorCommodity;

class HonorShopUI : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	HonorShopUI();
	~HonorShopUI();

	static HonorShopUI * create(std::vector<CHonorCommodity*> honorCommodityList);
	bool init(std::vector<CHonorCommodity*> honorCommodityList);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject *pSender);

	virtual void update(float dt);

private:
	std::vector<CHonorCommodity*> curHonorCommodityList;

	CCTableView * m_tableView;

	UILabel * l_honorValue;
};


//cell中的单项
class HonorShopCellItem : public CCLayer
{
public:
	HonorShopCellItem();
	~HonorShopCellItem();

	static HonorShopCellItem* create(CHonorCommodity * honorCommodity);
	bool init(CHonorCommodity * honorCommodity);

	void DetailEvent(CCObject * pSender);
	void BuyEvent(CCObject *pSender);

private:
	CHonorCommodity * curHonorCommodity;

public:
	CCTableView *m_tableView;
};


#endif

