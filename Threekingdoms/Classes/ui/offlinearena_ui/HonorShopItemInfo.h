
#ifndef _OFFLINEARENAUI_HONORSHOPITEMINFO_H_
#define _OFFLINEARENAUI_HONORSHOPITEMINFO_H_

#include "../backpackscene/GoodsItemInfoBase.h"

class GoodsInfo;
class CHonorCommodity;

class HonorShopItemInfo : public GoodsItemInfoBase
{
public:
	HonorShopItemInfo();
	~HonorShopItemInfo();

	static HonorShopItemInfo * create(CHonorCommodity * honorCommodity);
	bool init(CHonorCommodity * honorCommodity);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
};

#endif
