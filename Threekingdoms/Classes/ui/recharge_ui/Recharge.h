#ifndef _RECHARGE_H_
#define _RECHARGE_H_

#include "../../ui/extensions/UIScene.h"

class UITab;
//class CLableGoods;
//class GoodsInfo;

class RechargeUI : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	RechargeUI();
	~RechargeUI();

	static RechargeUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject *pSender);
	void RechargeEvent(CCObject *pSender);
	void VipEvent(CCObject *pSender);
	void IndexChangedEvent(CCObject* pSender);

	virtual void update(float dt);

private:
	UIPanel * ppanel;
	UILayer * u_layer;
	UITab * mainTab;
	cocos2d::extension::UILabel * l_ingot_value;

	cocos2d::extension::UIImageView * m_imageView_horn;

	std::string m_strMarquee;

	CCTableView * m_tableView;
public:
	int curLableId;

private:
	//  LiuLiang++
	void callBackCounter(CCObject *obj);
    
public:
    void requestPay();
    void onHttpResponsePay(CCHttpClient *sender, CCHttpResponse *response);
};

#endif

