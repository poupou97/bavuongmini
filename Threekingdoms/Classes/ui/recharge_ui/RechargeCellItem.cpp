#include "RechargeCellItem.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "../Classes/messageclient/element/CLableGoods.h"
#include "../Classes/ui/extensions/CCMoveableMenu.h"
#include "../Classes/messageclient/element/GoodsInfo.h"
#include "../Classes/loadscene_state/LoadSceneState.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/gamescene_state/GameSceneState.h"
#include "../Classes/GameView.h"
#include "AppMacros.h"
#include "../Classes/utils/GameUtils.h"
#include "../Classes/ui/backpackscene/EquipmentItem.h"
#include "../Classes/ui/backpackscene/GoodsItemInfoBase.h"
#include "../Classes/messageclient/element/CRechargeInfo.h"
#include "../Classes/utils/GameConfig.h"
#include "../Classes/ui/recharge_ui/Recharge.h"


#include "WmComPlatform.h"
#include "ExtenalClass.h"

#else

#include "../../messageclient/element/CLableGoods.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../GameView.h"
#include "AppMacros.h"
#include "../../utils/GameUtils.h"
#include "../../ui/backpackscene/EquipmentItem.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"
#include "../../messageclient/element/CRechargeInfo.h"
#include "../../utils/GameConfig.h"
#include "Recharge.h"

#endif



#define HIGHLIGHT_FIRST_TAG 100
#define HIGHLIGHT_SECOND_TAG 101

int RechargeCellItem::curOrderId = 1;

RechargeCellItem::RechargeCellItem()
{
}


RechargeCellItem::~RechargeCellItem()
{
	delete curRechargeInfo;
}

RechargeCellItem* RechargeCellItem::create( CRechargeInfo * rechargeInfo )
{
	RechargeCellItem * rechargeCellItem = new RechargeCellItem();
	if (rechargeCellItem && rechargeCellItem->init(rechargeInfo))
	{
		rechargeCellItem->autorelease();
		return rechargeCellItem;
	}
	CC_SAFE_DELETE(rechargeCellItem);
	return NULL;
}

bool RechargeCellItem::init( CRechargeInfo * rechargeInfo )
{
	if (UIScene::init())
	{
		/////////////////////////////////////////////////////////////////////////////////////

		// 该UILayer位于CCTableView之内，为了正确响应TableView的拖动事件，故设置为不吞掉触摸事件
		m_pUiLayer->setSwallowsTouches(false);

		curRechargeInfo = new CRechargeInfo(*rechargeInfo);

		orderId = curRechargeInfo->get_orderId();

		cocos2d::extension::UIButton * btn_buy  = cocos2d::extension::UIButton::create();
		btn_buy->setTextures("res_ui/new_button_10.png","res_ui/new_button_10.png","");
		btn_buy->setTouchEnable(true);
		btn_buy->setPressedActionEnabled(true, 0.9f, 1.1f);
		btn_buy->setScale9Enable(true);
		btn_buy->setScale9Size(CCSizeMake(257,71));
		//btn_buy->setCapInsets(CCRectMake(18,9,2,23));
		btn_buy->setAnchorPoint(ccp(0.5f,0.5f));
		btn_buy->setPosition(ccp(129,38));
		btn_buy->addReleaseEvent(this,coco_releaseselector(RechargeCellItem::BuyEvent));
		m_pUiLayer->addWidget(btn_buy);

		cocos2d::extension::UIImageView * imageViewLight = cocos2d::extension::UIImageView::create();
		imageViewLight->setTexture("res_ui/Prepaid/light_round.png");
		imageViewLight->setScale9Enable(true);
		imageViewLight->setScale9Size(CCSizeMake(73,74));
		imageViewLight->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewLight->setPosition(ccp(-84,-2));
		btn_buy->addChild(imageViewLight);

		cocos2d::extension::UIImageView * imageViewGold = cocos2d::extension::UIImageView::create();
		std::string strGold;
		strGold.append("res_ui/Prepaid/");
		strGold.append(curRechargeInfo->get_icon());
		strGold.append(".png");
		imageViewGold->setTexture(strGold.c_str());
		imageViewGold->setScale9Enable(true);
		imageViewGold->setScale9Size(CCSizeMake(64,55));
		imageViewGold->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewGold->setPosition(ccp(0,3));
		imageViewLight->addChild(imageViewGold);
		
		cocos2d::extension::UIImageView * imageViewFrame = cocos2d::extension::UIImageView::create();
		imageViewFrame->setTexture("res_ui/di_yy.png");
		imageViewFrame->setScale9Enable(true);
		//imageViewFrame->setScale9Size(CCSizeMake(141,27));
		imageViewFrame->setScale9Size(CCSizeMake(91,27));
		imageViewFrame->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewFrame->setPosition(ccp(-4,13));
		btn_buy->addChild(imageViewFrame);
		
		cocos2d::extension::UIImageView * imageViewGoldMini = cocos2d::extension::UIImageView::create();
		imageViewGoldMini->setTexture("res_ui/ingot.png");
		//imageViewGoldMini->setScale9Enable(true);
		//imageViewGoldMini->setScale9Size(CCSizeMake(23,15));
		imageViewGoldMini->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewGoldMini->setPosition(ccp(-32,0));
		imageViewFrame->addChild(imageViewGoldMini);
		
		cocos2d::extension::UILabel * goldSum = cocos2d::extension::UILabel::create();
        goldSum->setStrokeEnabled(true);
		char aucBuf[6];
		memset(aucBuf, 0, sizeof(aucBuf));
		sprintf(aucBuf, "%d", curRechargeInfo->get_recharge_value());
		goldSum->setText(aucBuf);
		goldSum->setAnchorPoint(ccp(0,0.5f));
		goldSum->setFontName(APP_FONT_NAME);
		goldSum->setFontSize(18);
		goldSum->setPosition(ccp(-19,0));
		goldSum->setColor(ccc3(255, 255, 255));
		imageViewFrame->addChild(goldSum);
		
		cocos2d::extension::UIImageView * imageViewFrameRMB = cocos2d::extension::UIImageView::create();
		imageViewFrameRMB->setTexture("res_ui/di_yy.png");
		imageViewFrameRMB->setScale9Enable(true);
		//imageViewFrame->setScale9Size(CCSizeMake(141,27));
		imageViewFrameRMB->setScale9Size(CCSizeMake(69,27));
		imageViewFrameRMB->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewFrameRMB->setPosition(ccp(78,13));
		btn_buy->addChild(imageViewFrameRMB);
		
		cocos2d::extension::UIImageView * imageViewRMB = cocos2d::extension::UIImageView::create();
		imageViewRMB->setTexture("res_ui/vip/qian2.png");
		//imageViewRMB->setScale9Enable(true);
		//imageViewRMB->setScale9Size(CCSizeMake(15,17));
		imageViewRMB->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewRMB->setPosition(ccp(22,0));
		imageViewFrameRMB->addChild(imageViewRMB);
		
		cocos2d::extension::UILabel * moneySum = cocos2d::extension::UILabel::create();
        moneySum->setStrokeEnabled(true);
		memset(aucBuf, 0, sizeof(aucBuf));
		int a = atoi(VipValueConfig::s_vipValue[2].c_str())*100;
		sprintf(aucBuf, "%d", curRechargeInfo->get_recharge_value()/a);
		moneySum->setText(aucBuf);
		moneySum->setAnchorPoint(ccp(1,0.5f));
		moneySum->setFontName(APP_FONT_NAME);
		moneySum->setFontSize(18);
		moneySum->setPosition(ccp(15,0));
		moneySum->setColor(ccc3(255, 255, 255));
		imageViewFrameRMB->addChild(moneySum);

		char aucLable[6];
		memset(aucLable, 0, sizeof(aucLable));
		sprintf(aucLable,"%d",curRechargeInfo->get_send_value());
		std::string strLabel;
		strLabel.append(StringDataManager::getString("RechargeUI_SendLabel_front"));
		strLabel.append(aucLable);
		strLabel.append(StringDataManager::getString("RechargeUI_SendLabel_back"));
		UILabelBMFont* sendValue = UILabelBMFont::create();
		sendValue->setText(strLabel.c_str());
		sendValue->setFntFile("res_ui/font/ziti_3.fnt");
		//sendValue->setScale(0.7f);
		sendValue->setAnchorPoint(ccp(0.5f,0.5f));
		sendValue->setPosition(ccp(34,-17));
		btn_buy->addChild(sendValue);
		if(!curRechargeInfo->get_send_value())
		{
			sendValue->setVisible(false);
		}
		
		cocos2d::extension::UIImageView * imageViewSuggest = cocos2d::extension::UIImageView::create();
		imageViewSuggest->setTexture("res_ui/Prepaid/tuijian.png");
		//imageViewSuggest->setScale9Enable(true);
		//imageViewSuggest->setScale9Size(CCSizeMake(20,49));
		imageViewSuggest->setAnchorPoint(ccp(0.5f,0.5f));
		imageViewSuggest->setPosition(ccp(12,64));
		m_pUiLayer->addWidget(imageViewSuggest);
		switch(curRechargeInfo->get_suggest())
		{
		case 0:
			imageViewSuggest->setVisible(false);
			break;
		case 1:
			imageViewSuggest->setVisible(true);
			break;
		default:
			break;
		}

		this->setContentSize(CCSizeMake(241,76));

		return true;
	}
	return false;
}

void RechargeCellItem::BuyEvent( CCObject *pSender )
{
	//remove last highlight frame
	//setHighLightFrameVisible(curOrderId, false);
	//add current highlight frame
	//setHighLightFrameVisible(orderId, true);

	curOrderId = orderId;

	// 充值控制开关
	bool bChargeEnabled = GameConfig::getBoolForKey("charge_enable");
	if (true == bChargeEnabled)
	{
		CCLOG("to SDK recharge");

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

		CRechargeInfo * pClickRechargeInfo = this->curRechargeInfo;
		// 元宝数
		int nRechargeValue = pClickRechargeInfo->get_recharge_value();
		// 价格（元）
		int nCharge = nRechargeValue / 100;

		//RechargeCellItem::s_nCharge = nCharge;
		[ExtenalClass set_Charge:nCharge];

		// 支付
		requestPay();

#endif
	}
	else
	{
		const char * charChargeEnabled = StringDataManager::getString("RechargeUI_disable");
		GameView::getInstance()->showAlertDialog(charChargeEnabled);
	}
}

void RechargeCellItem::setHighLightFrameVisible(int orderId, bool mode)
{
	CCTableViewCell* cell = m_tableView->cellAtIndex((orderId-1)/2);
	if(1 == orderId%2)
	{
		CCScale9Sprite* highLightLast = (CCScale9Sprite*)cell->getChildByTag(HIGHLIGHT_FIRST_TAG);
		highLightLast->setVisible(mode);
	}
	else
	{
		CCScale9Sprite* highLightLast = (CCScale9Sprite*)cell->getChildByTag(HIGHLIGHT_SECOND_TAG);
		highLightLast->setVisible(mode);
	}
}