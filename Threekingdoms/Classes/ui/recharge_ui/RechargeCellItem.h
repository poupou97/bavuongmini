#ifndef  _RECHARGEUI_RECHARGECELL_H_
#define _RECHARGEUI_RECHARGECELL_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CLableGoods;
class CRechargeInfo;

class RechargeCellItem : public UIScene
{
public:
	RechargeCellItem();
	~RechargeCellItem();

	static RechargeCellItem* create(CRechargeInfo * rechargeInfo);
	bool init(CRechargeInfo * rechargeInfo);

	void BuyEvent(CCObject *pSender);
	void GoodItemEvent(CCObject *pSender);
	void setTableView(CCTableView* tableView){m_tableView = tableView;}
private:
	CRechargeInfo * curRechargeInfo;
	int orderId;
	CCTableView* m_tableView;
    static int curOrderId;
	void setHighLightFrameVisible(int orderId, bool mode);
public:
    void requestPay();
    void onHttpResponsePay(CCHttpClient *sender, CCHttpResponse *response);
    
    //static int s_nCharge;           // 单位是元
};

#endif

