#include "VipDetailUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/CRewardProp.h"
#include "../backpackscene/PackageItem.h"
#include "../../AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "VipRewardCellItem.h"
#include "../../utils/GameUtils.h"
#include "../../utils/StrUtils.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"

#define  M_SCROLLVIEWTAG 213

VipDetailUI::VipDetailUI():
m_nCurSelectVipLevel(0)
{
}


VipDetailUI::~VipDetailUI()
{
}


VipDetailUI* VipDetailUI::create()
{
	VipDetailUI * vipDetailUI = new VipDetailUI();
	if (vipDetailUI && vipDetailUI->init())
	{
		vipDetailUI->autorelease();
		return vipDetailUI;
	}
	CC_SAFE_DELETE(vipDetailUI);
	return NULL;
}

bool VipDetailUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		//加载UI
		if(LoadSceneLayer::vipDetatilPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::vipDetatilPanel->removeFromParentAndCleanup(false);
		}
		UIPanel* panel_vip = LoadSceneLayer::vipDetatilPanel;
		panel_vip->setAnchorPoint(ccp(0.0f,0.0f));
		panel_vip->setPosition(CCPointZero);
		panel_vip->setTouchEnable(true);
		m_pUiLayer->addWidget(panel_vip);

		u_layer = UILayer::create();
		addChild(u_layer);

		UIButton *Btn_close = (UIButton*)UIHelper::seekWidgetByName(panel_vip,"Button_close");
		Btn_close->setTouchEnable(true);
		Btn_close->setPressedActionEnabled(true);
		Btn_close->addReleaseEvent(this, coco_releaseselector(VipDetailUI::CloseEvent));

		UIButton *Btn_buyVip = (UIButton*)UIHelper::seekWidgetByName(panel_vip,"Button_buy");
		Btn_buyVip->setTouchEnable(true);
		Btn_buyVip->setPressedActionEnabled(true);
		Btn_buyVip->addReleaseEvent(this, coco_releaseselector(VipDetailUI::VipBuyEvent));

		lbt_vipLevel = (UILabelBMFont *)UIHelper::seekWidgetByName(panel_vip,"LabelBMFont_vipLevel");
		l_goldInGotValue = (UILabel*)UIHelper::seekWidgetByName(panel_vip,"Label_goldInGotValue");

		m_tableView = CCTableView::create(this,CCSizeMake(525,96));
		m_tableView->setSelectedEnable(true);
		m_tableView->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 26, 1, 1), ccp(0,0));
		m_tableView->setDirection(kCCScrollViewDirectionHorizontal);
		m_tableView->setAnchorPoint(ccp(0,0));
		m_tableView->setPosition(ccp(57,303));
		m_tableView->setContentOffset(ccp(0,0));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		m_tableView->setPressedActionEnabled(true);
		u_layer->addChild(m_tableView);

		UILayer * u_tableViewFrameLayer = UILayer::create();
		this->addChild(u_tableViewFrameLayer);
		UIImageView * tableView_kuang = UIImageView::create();
		tableView_kuang->setTexture("res_ui/LV5_dikuang1_miaobian2.png");
		tableView_kuang->setScale9Enable(true);
		tableView_kuang->setScale9Size(CCSizeMake(545,116));
		tableView_kuang->setCapInsets(CCRectMake(30,30,1,1));
		tableView_kuang->setAnchorPoint(ccp(0,0));
		tableView_kuang->setPosition(ccp(45,294));
		u_tableViewFrameLayer->addWidget(tableView_kuang);

		UIImageView * image_titleFrame = UIImageView::create();
		image_titleFrame->setTexture("res_ui/caidai_1.png");
		image_titleFrame->setAnchorPoint(ccp(0.5f,0.5f));
		image_titleFrame->setPosition(ccp(320,436));
		image_titleFrame->setScale9Enable(true);
		image_titleFrame->setScale9Size(CCSizeMake(451,62));
		image_titleFrame->setCapInsets(CCRectMake(178,62,1,1));
		u_tableViewFrameLayer->addWidget(image_titleFrame);

		UIImageView * image_title = UIImageView::create();
		image_title->setTexture("res_ui/vip/vip_1.png");
		image_title->setAnchorPoint(ccp(0.5f,0.5f));
		image_title->setPosition(ccp(319,441));
		u_tableViewFrameLayer->addWidget(image_title);

		//set default data
		m_nCurSelectVipLevel = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5202,(void*)m_nCurSelectVipLevel);

		int scroll_height = 0;
		//
		//介绍
		//CCLabelTTF * l_des = CCLabelTTF::create(des.c_str(),APP_FONT_NAME,18,CCSizeMake(265,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
		//l_des = CCRichLabel::createWithString("",CCSizeMake(263, 0),this,NULL,0,16,5);

		m_scrollView = CCScrollView::create(CCSizeMake(265,205));
		m_scrollView->setViewSize(CCSizeMake(265, 205));
		m_scrollView->ignoreAnchorPointForPosition(false);
		m_scrollView->setTouchEnabled(true);
		m_scrollView->setDirection(kCCScrollViewDirectionVertical);
		m_scrollView->setAnchorPoint(ccp(0,0));
		m_scrollView->setPosition(ccp(75,43));
		m_scrollView->setBounceable(true);
		m_scrollView->setClippingToBounds(true);
		m_scrollView->setTag(M_SCROLLVIEWTAG);
		u_layer->addChild(m_scrollView);

		int zeroLineHeight;
		std::map<int,std::string>::iterator it = VipDescriptionConfig::s_vipDes.begin();
		for ( ; it != VipDescriptionConfig::s_vipDes.end(); ++ it )
		{
			CCRichLabel *l_vipDes = CCRichLabel::createWithString(it->second.c_str(),CCSizeMake(263, 0),this,NULL,0,16,5);
			l_vipDes->setTag(it->first);
			m_scrollView->addChild(l_vipDes);

			if (it->first == 1)
			{
				zeroLineHeight = l_vipDes->getContentSize().height+10;
			}
		}

		scroll_height = zeroLineHeight;
		if (scroll_height > 190)
		{
			m_scrollView->setContentSize(CCSizeMake(265,scroll_height));
			m_scrollView->setContentOffset(ccp(0,205-scroll_height));  
		}
		else
		{
			m_scrollView->setContentSize(ccp(265,205));
		}
		m_scrollView->setClippingToBounds(true);

		std::map<int,std::string>::iterator itr = VipDescriptionConfig::s_vipDes.begin();
		for ( ; itr != VipDescriptionConfig::s_vipDes.end(); ++ itr )
		{
			CCRichLabel *l_vipDes = dynamic_cast<CCRichLabel*>(m_scrollView->getContainer()->getChildByTag(itr->first));
			if (l_vipDes)
			{
				l_vipDes->setPosition(ccp(2,m_scrollView->getContentSize().height - zeroLineHeight));
				if (itr->first  != 1)
				{
					l_vipDes->setVisible(false);
				}
			}
		}

		//m_scrollView->addChild(l_des);
		//l_des->setPosition(ccp(2,m_scrollView->getContentSize().height - zeroLineHeight));

		RefreshDescriptionByLevel(m_nCurSelectVipLevel);
		refreshInGotValue();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(panel_vip->getContentSize());

		return true;
	}
	return false;
}

void VipDetailUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void VipDetailUI::onExit()
{
	UIScene::onExit();
}

bool VipDetailUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	return true;
}

void VipDetailUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void VipDetailUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void VipDetailUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}


void VipDetailUI::scrollViewDidScroll( CCScrollView* view )
{

}

void VipDetailUI::scrollViewDidZoom( CCScrollView* view )
{

}

void VipDetailUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	m_nCurSelectVipLevel = cell->getIdx()+1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5202,(void*)m_nCurSelectVipLevel);
	RefreshDescriptionByLevel(m_nCurSelectVipLevel);
}

cocos2d::CCSize VipDetailUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(100,89);
}

cocos2d::extension::CCTableViewCell* VipDetailUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();

	CCSprite * sprite_cion = CCSprite::create("res_ui/vip/vip_b.png");
	sprite_cion->setAnchorPoint(ccp(0.5f,0.5f));
	sprite_cion->setPosition(ccp(50,56));
	cell->addChild(sprite_cion);
	 
	char str_level[20];
	sprintf(str_level,"%d",idx+1);
	CCLabelBMFont * l_vipLevel = CCLabelBMFont::create(str_level,"res_ui/font/ziti_3.fnt");
	l_vipLevel->setScale(0.9f);
	l_vipLevel->setAnchorPoint(ccp(0.5f,0.5f));
	l_vipLevel->setPosition(ccp(43,29));
	sprite_cion->addChild(l_vipLevel);

	int m_price_value = 0;
	int m_price_type = 0;
	std::map<int, VipPriceConfig::priceValueAndType*>::const_iterator cIter;
	cIter = VipPriceConfig::s_vipPriceValue.find(idx+1);
	if (cIter == VipPriceConfig::s_vipPriceValue.end()) // 没找到就是指向END了  
	{
	}
	else
	{
		m_price_value = VipPriceConfig::s_vipPriceValue[idx+1]->priceValue;
		m_price_type = VipPriceConfig::s_vipPriceValue[idx+1]->priceType;
	}

	CCSprite * sprite_money;

	char str_moneyValue[20];
	if (m_price_type == 1) //元宝
	{
		sprite_money = CCSprite::create("res_ui/ingot.png");
		sprintf(str_moneyValue,"%d",m_price_value);
	}
	else
	{
		sprite_money = CCSprite::create("res_ui/vip/qian.png");
		sprintf(str_moneyValue,"%d",m_price_value/100);
	}

	sprite_money->setAnchorPoint(ccp(0.5f,0.5f));
	sprite_money->setPosition(ccp(36,14));
	cell->addChild(sprite_money);
	
	CCLabelBMFont * l_moneyValue = CCLabelBMFont::create(str_moneyValue,"res_ui/font/ziti_1.fnt");
	l_moneyValue->setAnchorPoint(ccp(0.5f,0.5f));
	l_moneyValue->setScale(0.7f);
	l_moneyValue->setPosition(ccp(55+sprite_money->getContentSize().width*l_moneyValue->getScale()/2,14));
	cell->addChild(l_moneyValue);

	sprite_money->setPosition(ccp(55 - l_moneyValue->getContentSize().width*l_moneyValue->getScale()/2 - 4,14));

	return cell;
}

unsigned int VipDetailUI::numberOfCellsInTableView( CCTableView *table )
{
	return VipDescriptionConfig::s_vipDes.size();
}

void VipDetailUI::CloseEvent(CCObject * pSender)
{
	this->closeAnim();
}

void VipDetailUI::RefreshGoodsRewards( std::vector<CRewardProp*> list )
{
	//delete old
	for(int i = 0;i<4;i++)
	{
		if (m_pUiLayer->getChildByTag(200+i))
		{
			m_pUiLayer->getChildByTag(200+i)->removeFromParent();
		}
	}
	//add new
	for(int i =0;i<list.size();++i)
	{
		if (i>3)
			continue;

		GoodsInfo * temp = new GoodsInfo();
		temp->CopyFrom(list.at(i)->goods());
		VipRewardCellItem * goodsItem = VipRewardCellItem::create(temp,list.at(i)->number());
		goodsItem->ignoreAnchorPointForPosition(false);
		goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		goodsItem->setPosition(ccp(0,0));
		goodsItem->setTag(200+i);
		goodsItem->setScale(0.9f);
		m_pUiLayer->addChild(goodsItem);

		switch(i)
		{
		case 0:
			{
				goodsItem->setPosition(ccp(444,220));
			}
			break;
		case 1:
			{
				goodsItem->setPosition(ccp(523,220));
			}
			break;
		case 2:
			{
				goodsItem->setPosition(ccp(444,160));
			}
			break;
		case 3:
			{
				goodsItem->setPosition(ccp(523,160));
			}
			break;
		}
	}
}

void VipDetailUI::RefreshDescriptionByLevel( int vipLevel )
{
	char str_lev[20];
	sprintf(str_lev,"%d",vipLevel);
	lbt_vipLevel->setText(str_lev);
// 	long long startTime = GameUtils::millisecondNow();
// 	std::string des = "";
// 	std::map<int,std::string>::const_iterator cIter;
// 	cIter = VipDescriptionConfig::s_vipDes.find(vipLevel);
// 	if (cIter == VipDescriptionConfig::s_vipDes.end()) // 没找到就是指向END了  
// 	{
// 		return ;
// 	}
// 	else
// 	{
// 		 des.append(VipDescriptionConfig::s_vipDes[vipLevel]);
// 	}
// 
// 	long long endTime = GameUtils::millisecondNow();
// 	CCLOG(" find time = %ld ",endTime - startTime);
// 
// 	startTime = GameUtils::millisecondNow();
// 	//l_des->setString(des.c_str());
// 	l_des->setString(StringDataManager::getString("generals_teach_willOpen_1"));
// 	endTime = GameUtils::millisecondNow();
// 	CCLOG(" setString time = %ld ",endTime - startTime);
// 
// 	int zeroLineHeight = l_des->getContentSize().height+10;
// 	int scroll_height = zeroLineHeight;
// 
// 	startTime = GameUtils::millisecondNow();
// 	if (scroll_height > 190)
// 	{
// 		m_scrollView->setContentSize(CCSizeMake(265,scroll_height));
// 		m_scrollView->setContentOffset(ccp(0,205-scroll_height));  
// 	}
// 	else
// 	{
// 		m_scrollView->setContentSize(ccp(265,205));
// 	}
// 	l_des->setPosition(ccp(2,m_scrollView->getContentSize().height - zeroLineHeight));
// 	endTime = GameUtils::millisecondNow();
// 	CCLOG(" setPostion time = %ld ",endTime - startTime);
// 	
	std::map<int,std::string>::iterator it = VipDescriptionConfig::s_vipDes.begin();
	for ( ; it != VipDescriptionConfig::s_vipDes.end(); ++ it )
	{
		CCRichLabel *l_vipDes = dynamic_cast<CCRichLabel*>(m_scrollView->getContainer()->getChildByTag(it->first));
		if (l_vipDes)
		{
			if (it->first == vipLevel)
			{
				int zeroLineHeight = l_vipDes->getContentSize().height+10;
				int scroll_height = zeroLineHeight;

				if (scroll_height > 190)
				{
					m_scrollView->setContentSize(CCSizeMake(265,scroll_height));
					m_scrollView->setContentOffset(ccp(0,205-scroll_height));  
				}
				else
				{
					m_scrollView->setContentSize(ccp(265,205));
				}
				l_vipDes->setPosition(ccp(2,m_scrollView->getContentSize().height - zeroLineHeight));
				l_vipDes->setVisible(true);
			}
			else
			{
				l_vipDes->setVisible(false);
			}
		}
	}
}

void VipDetailUI::VipBuyEvent( CCObject *pSender )
{
	if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() > 0)
	{
		//cur vip
		std::string str_curVip = "VIP";
		char s_curVip[10];
		sprintf(s_curVip,"%d",GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel());
		str_curVip.append(s_curVip);
		//next vip
		std::string str_nextVip = "VIP";
		char s_nextVip[10];
		sprintf(s_nextVip,"%d",m_nCurSelectVipLevel);
		str_nextVip.append(s_nextVip);

		std::string str_des;
		str_des.append(StringDataManager::getString("vip_detail_des_1"));
		//str_des.append(StrUtils::applyColor(str_curVip.c_str(),ccc3(255,51,51)));
		str_des.append(str_curVip.c_str());
		str_des.append(StringDataManager::getString("vip_detail_des_2"));
		//str_des.append(StrUtils::applyColor(str_nextVip.c_str(),ccc3(255,51,51)));
		str_des.append(str_nextVip.c_str());
		str_des.append(StringDataManager::getString("vip_detail_des_3"));
		//str_des.append(StrUtils::applyColor(str_nextVip.c_str(),ccc3(255,51,51)));
		str_des.append(str_nextVip.c_str());
		str_des.append(StringDataManager::getString("vip_detail_des_4"));
		//str_des.append(StrUtils::applyColor(str_curVip.c_str(),ccc3(255,51,51)));
		str_des.append(str_curVip.c_str());
		str_des.append(StringDataManager::getString("vip_detail_des_5"));
		
		GameView::getInstance()->showPopupWindow(str_des.c_str(),2,this,coco_selectselector(VipDetailUI::SureToBuyVip),NULL);
	}
	else
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5201,(void*)m_nCurSelectVipLevel);
	}
}

void VipDetailUI::setToDefault()
{
	m_nCurSelectVipLevel = 1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5202,(void*)m_nCurSelectVipLevel);
	RefreshDescriptionByLevel(m_nCurSelectVipLevel);
	m_tableView->selectCell(0);
}

void VipDetailUI::SureToBuyVip( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5201,(void*)m_nCurSelectVipLevel);
}

void VipDetailUI::refreshInGotValue()
{
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	l_goldInGotValue->setText(str_ingot);
}
