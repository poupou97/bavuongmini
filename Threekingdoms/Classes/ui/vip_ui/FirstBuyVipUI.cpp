#include "FirstBuyVipUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../backpackscene/PackageItem.h"
#include "../../AppMacros.h"
#include "../../messageclient/element/CRewardProp.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "VipDetailUI.h"
#include "VipRewardCellItem.h"
#include "../../utils/StaticDataManager.h"
#include "../recharge_ui/Recharge.h"
#include "../../utils/GameConfig.h"

FirstBuyVipUI::FirstBuyVipUI()
{
}


FirstBuyVipUI::~FirstBuyVipUI()
{
}

FirstBuyVipUI* FirstBuyVipUI::create()
{
	FirstBuyVipUI * firstBuyVipUI = new FirstBuyVipUI();
	if (firstBuyVipUI && firstBuyVipUI->init())
	{
		firstBuyVipUI->autorelease();
		return firstBuyVipUI;
	}
	CC_SAFE_DELETE(firstBuyVipUI);
	return NULL;
}

bool FirstBuyVipUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		//加载UI
		UIPanel* panel_vip =  (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/firstbuy_1.json");
		panel_vip->setAnchorPoint(ccp(0.0f,0.0f));
		panel_vip->setPosition(CCPointZero);
		panel_vip->setTouchEnable(true);
		m_pUiLayer->addWidget(panel_vip);

		u_layer = UILayer::create();
		addChild(u_layer);

		UIButton *Btn_close = (UIButton*)UIHelper::seekWidgetByName(panel_vip,"Button_close");
		Btn_close->setTouchEnable(true);
		Btn_close->setPressedActionEnabled(true);
		Btn_close->addReleaseEvent(this, coco_releaseselector(FirstBuyVipUI::CloseEvent));

		UIButton *Btn_buyVip = (UIButton*)UIHelper::seekWidgetByName(panel_vip,"Button_buy");
		Btn_buyVip->setTouchEnable(true);
		Btn_buyVip->setPressedActionEnabled(true);
		Btn_buyVip->addReleaseEvent(this, coco_releaseselector(FirstBuyVipUI::VipBuyEvent));

		UILabelBMFont * lbf_value = (UILabelBMFont*)UIHelper::seekWidgetByName(panel_vip,"LabelBMFont_aValue");
		std::map<int,std::string>::const_iterator cIter;
		cIter = VipValueConfig::s_vipValue.find(1);
		if (cIter == VipValueConfig::s_vipValue.end()) // 没找到就是指向END了  
		{
			
		}
		else
		{
			std::string value = VipValueConfig::s_vipValue[1];
			int a = atoi(value.c_str());
			char str_value[200];
			sprintf(str_value,"%d",a*100);
			lbf_value->setText(str_value);
		}


		//set default data
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5202,(void*)0);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(panel_vip->getContentSize());

		return true;
	}
	return false;
}

void FirstBuyVipUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FirstBuyVipUI::onExit()
{
	UIScene::onExit();
}

bool FirstBuyVipUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	return true;
}

void FirstBuyVipUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void FirstBuyVipUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void FirstBuyVipUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void FirstBuyVipUI::CloseEvent(CCObject * pSender)
{
	this->closeAnim();
}

void FirstBuyVipUI::RefreshGoodsRewards( std::vector<CRewardProp*> list )
{
	//delete old
	for(int i = 0;i<4;i++)
	{
// 		std::string str_name = "rewardItem_";
// 		char s_index [5];
// 		sprintf(s_index,"%d",i);
// 		str_name.append(s_index);
// 		if (m_pUiLayer->getWidgetByName(str_name.c_str()))
// 		{
// 			m_pUiLayer->getWidgetByName(str_name.c_str())->removeFromParent();
// 		}

		if (m_pUiLayer->getChildByTag(200+i))
		{
			m_pUiLayer->getChildByTag(200+i)->removeFromParent();
		}
	}
	//add new
	for(int i =0;i<list.size();++i)
	{
		if (i>3)
			continue;

		GoodsInfo * temp = new GoodsInfo();
		temp->CopyFrom(list.at(i)->goods());
		VipRewardCellItem * goodsItem = VipRewardCellItem::create(temp,list.at(i)->number());
		goodsItem->ignoreAnchorPointForPosition(false);
		goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		goodsItem->setPosition(ccp(320+69*i,145));
		goodsItem->setTag(200+i);
		m_pUiLayer->addChild(goodsItem);
// 		GoodsInfo * temp = new GoodsInfo();
// 		temp->CopyFrom(list.at(i)->goods());
// 		PackageItem * packageItem = PackageItem::create(temp);
// 		packageItem->setAnchorPoint(ccp(0,0));
//		packageItem->setPosition(ccp(268+69*i,115));
//		m_pUiLayer->addWidget(packageItem);
// 		std::string str_name = "rewardItem_";
// 		char s_index [5];
// 		sprintf(s_index,"%d",i);
// 		str_name.append(s_index);
// 		packageItem->setName(str_name.c_str());
// 
// 		char s_num[20];
// 		sprintf(s_num,"%d",list.at(i)->number());
// 		UILabel *Lable_num = UILabel::create();
// 		Lable_num->setText(s_num);
// 		Lable_num->setFontName(APP_FONT_NAME);
// 		Lable_num->setFontSize(13);
// 		Lable_num->setAnchorPoint(ccp(1.0f,0));
// 		Lable_num->setPosition(ccp(75,19));
// 		Lable_num->setName("Label_num");
// 		packageItem->addChild(Lable_num);
	}
}

void FirstBuyVipUI::VipBuyEvent( CCObject *pSender )
{
	// 充值控制开关
	bool bChargeEnabled = GameConfig::getBoolForKey("charge_enable");
	if (true == bChargeEnabled)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		CCLayer *rechargeScene = (CCLayer*)mainscene->getChildByTag(kTagRechargeUI);
		if(rechargeScene == NULL)
		{
			RechargeUI * rechargeUI = RechargeUI::create();
			mainscene->addChild(rechargeUI,0,kTagRechargeUI);
			rechargeUI->ignoreAnchorPointForPosition(false);
			rechargeUI->setAnchorPoint(ccp(0.5f,0.5f));
			rechargeUI->setPosition(ccp(winSize.width/2, winSize.height/2));
		}
		//GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillui_nothavefuction"));

		this->closeAnim();
	}
	else
	{
		const char * charChargeEnabled = StringDataManager::getString("RechargeUI_disable");
		GameView::getInstance()->showAlertDialog(charChargeEnabled);
	}
}
