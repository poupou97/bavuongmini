#include "VipRewardCellItem.h"
#include "../../messageclient/element/COneVipGift.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "AppMacros.h"
#include "../../utils/GameUtils.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"
#include "../backpackscene/EquipmentItem.h"

VipRewardCellItem::VipRewardCellItem()
{
}


VipRewardCellItem::~VipRewardCellItem()
{
	delete curGoodInfo;
}

VipRewardCellItem* VipRewardCellItem::create(GoodsInfo* goods,int num)
{
	VipRewardCellItem * vipRewardCellItem = new VipRewardCellItem();
	if (vipRewardCellItem && vipRewardCellItem->init(goods,num))
	{
		vipRewardCellItem->autorelease();
		return vipRewardCellItem;
	}
	CC_SAFE_DELETE(vipRewardCellItem);
	return NULL;
}

bool VipRewardCellItem::init(GoodsInfo* goods,int num)
{
	if (UIScene::init())
	{
		/////////////////////////////////////////////////////////////////////////////////////

		// 该UILayer位于CCTableView之内，为了正确响应TableView的拖动事件，故设置为不吞掉触摸事件
		m_pUiLayer->setSwallowsTouches(false);

		curGoodInfo = new GoodsInfo();
		curGoodInfo->CopyFrom(*goods);
		/*********************判断装备的颜色***************************/
		std::string frameColorPath;
		if (goods->quality() == 1)
		{
			frameColorPath = EquipWhiteFramePath;
		}
		else if (goods->quality() == 2)
		{
			frameColorPath = EquipGreenFramePath;
		}
		else if (goods->quality() == 3)
		{
			frameColorPath = EquipBlueFramePath;
		}
		else if (goods->quality() == 4)
		{
			frameColorPath = EquipPurpleFramePath;
		}
		else if (goods->quality() == 5)
		{
			frameColorPath = EquipOrangeFramePath;
		}
		else
		{
			frameColorPath = EquipVoidFramePath;
		}

		UIButton *Btn_goodsItemFrame = UIButton::create();
		Btn_goodsItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_goodsItemFrame->setTouchEnable(true);
		Btn_goodsItemFrame->setAnchorPoint(ccp(0.5f, 0.5f));
		Btn_goodsItemFrame->setPosition(ccp(Btn_goodsItemFrame->getContentSize().width / 2, Btn_goodsItemFrame->getContentSize().height / 2));
		Btn_goodsItemFrame->setScale(1.0f);
		Btn_goodsItemFrame->setPressedActionEnabled(true);
		Btn_goodsItemFrame->addReleaseEvent(this,coco_releaseselector(VipRewardCellItem::GoodItemEvent));
		m_pUiLayer->addWidget(Btn_goodsItemFrame);

		UIImageView * uiiImageView_goodsItem = UIImageView::create();
		if (strcmp(goods->icon().c_str(),"") == 0)
		{
			uiiImageView_goodsItem->setTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(goods->icon().c_str());
			_path.append(".png");
			uiiImageView_goodsItem->setTexture(_path.c_str());
		}
		uiiImageView_goodsItem->setScale(0.9f);
		uiiImageView_goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		uiiImageView_goodsItem->setPosition(ccp(0,0));
		Btn_goodsItemFrame->addChild(uiiImageView_goodsItem);

		char s_num[20];
		sprintf(s_num,"%d",num);
		UILabel *Lable_num = UILabel::create();
		Lable_num->setText(s_num);
		Lable_num->setFontName(APP_FONT_NAME);
		Lable_num->setFontSize(13);
		Lable_num->setAnchorPoint(ccp(1.0f,0.5f));
		Lable_num->setPosition(ccp(0 + Btn_goodsItemFrame->getContentSize().width / 2 - 7, 
			0 - Btn_goodsItemFrame->getContentSize().height / 2 + Lable_num->getContentSize().height - 2));
		Lable_num->setName("Label_num");
		Btn_goodsItemFrame->addChild(Lable_num);

		if (goods->binding() == 1)
		{
			UIImageView *ImageView_bound = UIImageView::create();
			ImageView_bound->setTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(ccp(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(ccp(8,55));
			m_pUiLayer->addWidget(ImageView_bound);
		}

		//如果是装备
		if (goods->has_equipmentdetail())
		{
			if (goods->equipmentdetail().gradelevel() > 0)  //精练等级
			{
				std::string ss_gradeLevel = "+";
				char str_gradeLevel [10];
				sprintf(str_gradeLevel,"%d",goods->equipmentdetail().gradelevel());
				ss_gradeLevel.append(str_gradeLevel);
				UILabel *Lable_gradeLevel = UILabel::create();
				Lable_gradeLevel->setText(ss_gradeLevel.c_str());
				Lable_gradeLevel->setFontName(APP_FONT_NAME);
				Lable_gradeLevel->setFontSize(13);
				Lable_gradeLevel->setAnchorPoint(ccp(1.0f,1.0f));
				Lable_gradeLevel->setPosition(ccp(Btn_goodsItemFrame->getContentSize().width-10,Btn_goodsItemFrame->getContentSize().height-7));
				Lable_gradeLevel->setName("Lable_gradeLevel");
				m_pUiLayer->addWidget(Lable_gradeLevel);
			}

			if (goods->equipmentdetail().starlevel() > 0)  //星级
			{
				char str_starLevel [10];
				sprintf(str_starLevel,"%d",goods->equipmentdetail().starlevel());
				UILabel *Lable_starLevel = UILabel::create();
				Lable_starLevel->setText(str_starLevel);
				Lable_starLevel->setFontName(APP_FONT_NAME);
				Lable_starLevel->setFontSize(13);
				Lable_starLevel->setAnchorPoint(ccp(0,0));
				Lable_starLevel->setPosition(ccp(8,6));
				Lable_starLevel->setName("Lable_starLevel");
				m_pUiLayer->addWidget(Lable_starLevel);

				UIImageView *ImageView_star = UIImageView::create();
				ImageView_star->setTexture("res_ui/star_on.png");
				ImageView_star->setAnchorPoint(ccp(0,0));
				ImageView_star->setPosition(ccp(8+Lable_starLevel->getContentSize().width,8));
				ImageView_star->setVisible(true);
				ImageView_star->setScale(0.5f);
				m_pUiLayer->addWidget(ImageView_star);
			}
		}

		this->setContentSize(CCSizeMake(63,63));

		return true;
	}
	return false;
}


void VipRewardCellItem::GoodItemEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase) == NULL)
	{
		CCSize size= CCDirector::sharedDirector()->getVisibleSize();
		GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(curGoodInfo,GameView::getInstance()->EquipListItem,0);
		goodsItemInfoBase->ignoreAnchorPointForPosition(false);
		goodsItemInfoBase->setAnchorPoint(ccp(0.5f,0.5f));
		//goodsItemInfoBase->setPosition(ccp(size.width/2,size.height/2));
		goodsItemInfoBase->setTag(kTagGoodsItemInfoBase);
		GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
	}
}
