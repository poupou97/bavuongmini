#ifndef  _VIPUI_VIPUI_H_
#define _VIPUI_VIPUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CRewardProp;
class CCRichLabel;

class VipDetailUI : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
private:
	UILayer * u_layer;
	CCTableView* m_tableView;
	UILabelBMFont * lbt_vipLevel;
	int m_nCurSelectVipLevel;

	CCRichLabel *l_des;
	CCScrollView * m_scrollView;
	UILabel * l_goldInGotValue;
public:
	VipDetailUI();
	~VipDetailUI();

	static VipDetailUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject * pSender);

	void VipBuyEvent(CCObject *pSender);

	void RefreshGoodsRewards(std::vector<CRewardProp*> list);
	void RefreshDescriptionByLevel(int vipLevel);

	void setToDefault();

	void SureToBuyVip(CCObject *pSender);

	void refreshInGotValue();
};

#endif

