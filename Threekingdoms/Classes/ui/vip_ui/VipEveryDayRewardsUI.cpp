#include "VipEveryDayRewardsUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/COneVipGift.h"
#include "../missionscene/MissionRewardGoodsItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "VipRewardCellItem.h"
#include "AppMacros.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/GameSceneState.h"
#include "VipDetailUI.h"


VipEveryDayRewardsUI::VipEveryDayRewardsUI(void)
{
}

VipEveryDayRewardsUI::~VipEveryDayRewardsUI()
{
}

VipEveryDayRewardsUI* VipEveryDayRewardsUI::create()
{
	VipEveryDayRewardsUI * vipEveryDayRewardsUI = new VipEveryDayRewardsUI();
	if (vipEveryDayRewardsUI && vipEveryDayRewardsUI->init())
	{
		vipEveryDayRewardsUI->autorelease();
		return vipEveryDayRewardsUI;
	}
	CC_SAFE_DELETE(vipEveryDayRewardsUI);
	return NULL;
}

bool VipEveryDayRewardsUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		//加载UI
		if(LoadSceneLayer::vipEveryDayRewardPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::vipEveryDayRewardPanel->removeFromParentAndCleanup(false);
		}
		UIPanel* panel_vip = LoadSceneLayer::vipEveryDayRewardPanel;
		panel_vip->setAnchorPoint(ccp(0.0f,0.0f));
		panel_vip->setPosition(CCPointZero);
		panel_vip->setTouchEnable(true);
		m_pUiLayer->addWidget(panel_vip);

		u_layer = UILayer::create();
		addChild(u_layer);

		UIButton *Btn_close = (UIButton*)UIHelper::seekWidgetByName(panel_vip,"Button_close");
		Btn_close->setTouchEnable(true);
		Btn_close->setPressedActionEnabled(true);
		Btn_close->addReleaseEvent(this, coco_releaseselector(VipEveryDayRewardsUI::CloseEvent));

		UIButton *Btn_vipBuy = (UIButton*)UIHelper::seekWidgetByName(panel_vip,"Button_buy");
		Btn_vipBuy->setTouchEnable(true);
		Btn_vipBuy->setPressedActionEnabled(true);
		Btn_vipBuy->addReleaseEvent(this, coco_releaseselector(VipEveryDayRewardsUI::VipBuyEvent));

		UILabelBMFont * lbt_vipLevel = (UILabelBMFont*)UIHelper::seekWidgetByName(panel_vip,"LabelBMFont_vipValue");
		int vipLevel = 0;
		vipLevel = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel();
		std::string str_viplevel = "VIP";
		char str_level[20];
		sprintf(str_level,"%d",vipLevel);
		str_viplevel.append(str_level);
		lbt_vipLevel->setText(str_viplevel.c_str());

		UILabelBMFont * lbt_remainTimeValue = (UILabelBMFont*)UIHelper::seekWidgetByName(panel_vip,"LabelBMFont_remainTimeValue");
		int remainTime = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().vipremaintime()/1000/3600/24;
		std::string str_remainTime = "";
		char str_rt[20];
		sprintf(str_rt,"%d",remainTime);
		str_remainTime.append(str_rt);
		str_remainTime.append(StringDataManager::getString("label_tian"));
		lbt_remainTimeValue->setText(str_remainTime.c_str());

		m_tableView = CCTableView::create(this,CCSizeMake(434,379));
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0,0));
		m_tableView->setPosition(ccp(272,36));
		m_tableView->setContentOffset(ccp(0,0));
		m_tableView->setDelegate(this);;
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		u_layer->addChild(m_tableView);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(panel_vip->getContentSize());

		return true;
	}
	return false;
}



void VipEveryDayRewardsUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void VipEveryDayRewardsUI::onExit()
{
	UIScene::onExit();
}

bool VipEveryDayRewardsUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	return true;
}

void VipEveryDayRewardsUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void VipEveryDayRewardsUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void VipEveryDayRewardsUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void VipEveryDayRewardsUI::CloseEvent(CCObject * pSender)
{
	this->closeAnim();
}

void VipEveryDayRewardsUI::VipBuyEvent( CCObject* pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	CCLayer *vipScene = (CCLayer*)mainscene->getChildByTag(kTagVipDetailUI);
 	if(vipScene == NULL)
 	{
 	 	VipDetailUI * vipDetailUI = VipDetailUI::create();
 	 	mainscene->addChild(vipDetailUI,0,kTagVipDetailUI);
 	 	vipDetailUI->ignoreAnchorPointForPosition(false);
 	 	vipDetailUI->setAnchorPoint(ccp(0.5f,0.5f));
 	 	vipDetailUI->setPosition(ccp(winSize.width/2, winSize.height/2));
 	}
}

// void VipEveryDayRewardsUI::Vip1GetEvent( CCObject* pSender )
// {
// 
// }
// 
// void VipEveryDayRewardsUI::Vip2GetEvent( CCObject* pSender )
// {
// 
// }
// 
// void VipEveryDayRewardsUI::Vip3GetEvent( CCObject* pSender )
// {
// 
// }
// 
// void VipEveryDayRewardsUI::Vip4GetEvent( CCObject* pSender )
// {
// 
// }
// 
// void VipEveryDayRewardsUI::Vip5GetEvent( CCObject* pSender )
// {
// 
// }

// void VipEveryDayRewardsUI::RefreshRewardItem()
// {
// 	for(int i = 0;i<GameView::getInstance()->m_vipEveryDayRewardsList.size();++i)
// 	{
// 		COneVipGift * oneVipGift = GameView::getInstance()->m_vipEveryDayRewardsList.at(i);
// 		for(int j = 0;j<oneVipGift->goods_size();++j)
// 		{
// 			if (j >= 2)
// 				continue;
// 
// 			GoodsInfo * goodInfo = new GoodsInfo();
// 			goodInfo->CopyFrom(oneVipGift->goods(j));
// 			int num = 0;
// 			if (j < oneVipGift->goodsnumber_size())
// 				num = oneVipGift->goodsnumber(j);
// 
// 			MissionRewardGoodsItem * mrGoodsItem = MissionRewardGoodsItem::create(goodInfo,num);
// 			mrGoodsItem->setAnchorPoint(ccp(0.5f,0.5f));
// 			mrGoodsItem->setPosition(ccp(352+79*j,57+73*(5-oneVipGift->number())));
// 			m_pUiLayer->addWidget(mrGoodsItem);
// 
// 			delete goodInfo;
// 		}
// 
// 		RefreshStatus(oneVipGift->number(),oneVipGift->status());
// 	}
// }

// void VipEveryDayRewardsUI::RefreshStatus( int vipLevel ,int status)
// {
// 	switch(status)
// 	{
// 	case 1:   //不可领取
// 		{
// 			switch(vipLevel)
// 			{
// 			case 1:
// 				{
// 					Btn_get_1->setVisible(false);
// 					imageView_canNotGet_1->setVisible(true);
// 					l_content_1->setVisible(true);
// 					l_content_1->setText(StringDataManager::getString("vip_reward_cannotGet"));
// 				}
// 				break;
// 			case 2:
// 				{
// 					Btn_get_2->setVisible(false);
// 					imageView_canNotGet_2->setVisible(true);
// 					l_content_2->setVisible(true);
// 					l_content_2->setText(StringDataManager::getString("vip_reward_cannotGet"));
// 				}
// 				break;
// 			case 3:
// 				{
// 					Btn_get_3->setVisible(false);
// 					imageView_canNotGet_3->setVisible(true);
// 					l_content_3->setVisible(true);
// 					l_content_3->setText(StringDataManager::getString("vip_reward_cannotGet"));
// 				}
// 				break;
// 			case 4:
// 				{
// 					Btn_get_4->setVisible(false);
// 					imageView_canNotGet_4->setVisible(true);
// 					l_content_4->setVisible(true);
// 					l_content_4->setText(StringDataManager::getString("vip_reward_cannotGet"));
// 				}
// 				break;
// 			case 5:
// 				{
// 					Btn_get_5->setVisible(false);
// 					imageView_canNotGet_5->setVisible(true);
// 					l_content_5->setVisible(true);
// 					l_content_5->setText(StringDataManager::getString("vip_reward_cannotGet"));
// 				}
// 				break;
// 			}
// 		}
// 		break;
// 	case 2:   //可领取
// 		{
// 			switch(vipLevel)
// 			{
// 			case 1:
// 				{
// 					Btn_get_1->setVisible(true);
// 					imageView_canNotGet_1->setVisible(false);
// 					l_content_1->setVisible(false);
// 				}
// 				break;
// 			case 2:
// 				{
// 					Btn_get_2->setVisible(true);
// 					imageView_canNotGet_2->setVisible(false);
// 					l_content_2->setVisible(false);
// 				}
// 				break;
// 			case 3:
// 				{
// 					Btn_get_3->setVisible(true);
// 					imageView_canNotGet_3->setVisible(false);
// 					l_content_3->setVisible(false);
// 				}
// 				break;
// 			case 4:
// 				{
// 					Btn_get_4->setVisible(true);
// 					imageView_canNotGet_4->setVisible(false);
// 					l_content_4->setVisible(false);
// 				}
// 				break;
// 			case 5:
// 				{
// 					Btn_get_5->setVisible(true);
// 					imageView_canNotGet_5->setVisible(false);
// 					l_content_5->setVisible(false);
// 				}
// 				break;
// 			}
// 		}
// 		break;
// 	case 3:   //已领取
// 		{
// 			switch(vipLevel)
// 			{
// 			case 1:
// 				{
// 					Btn_get_1->setVisible(false);
// 					imageView_canNotGet_1->setVisible(true);
// 					l_content_1->setVisible(true);
// 					l_content_1->setText(StringDataManager::getString("vip_reward_Geted"));
// 				}
// 				break;
// 			case 2:
// 				{
// 					Btn_get_2->setVisible(false);
// 					imageView_canNotGet_2->setVisible(true);
// 					l_content_2->setVisible(true);
// 					l_content_2->setText(StringDataManager::getString("vip_reward_Geted"));
// 				}
// 				break;
// 			case 3:
// 				{
// 					Btn_get_3->setVisible(false);
// 					imageView_canNotGet_3->setVisible(true);
// 					l_content_3->setVisible(true);
// 					l_content_3->setText(StringDataManager::getString("vip_reward_Geted"));
// 				}
// 				break;
// 			case 4:
// 				{
// 					Btn_get_4->setVisible(false);
// 					imageView_canNotGet_4->setVisible(true);
// 					l_content_4->setVisible(true);
// 					l_content_4->setText(StringDataManager::getString("vip_reward_Geted"));
// 				}
// 				break;
// 			case 5:
// 				{
// 					Btn_get_5->setVisible(false);
// 					imageView_canNotGet_5->setVisible(true);
// 					l_content_5->setVisible(true);
// 					l_content_5->setText(StringDataManager::getString("vip_reward_Geted"));
// 				}
// 				break;
// 			}
// 		}
// 		break;
// 	}
// }

void VipEveryDayRewardsUI::scrollViewDidScroll( CCScrollView* view )
{

}

void VipEveryDayRewardsUI::scrollViewDidZoom( CCScrollView* view )
{

}

void VipEveryDayRewardsUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{

}

cocos2d::CCSize VipEveryDayRewardsUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(423,75);
}

cocos2d::extension::CCTableViewCell* VipEveryDayRewardsUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();

	COneVipGift * oneVipGift = GameView::getInstance()->m_vipEveryDayRewardsList.at(idx);

	VipEveryDayRewardCell * tempCell = VipEveryDayRewardCell::create(oneVipGift);
	cell->addChild(tempCell);
// 
// 	CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/highlight.png.png");
// 	sprite_frame->setContentSize(CCSizeMake(366,71));
// 	sprite_frame->setAnchorPoint(ccp(0,0));
// 	sprite_frame->setPosition(ccp(0,0));
// 	cell->addChild(sprite_frame);
// 
// 	CCSprite * sprite_cion = CCSprite::create("res_ui/vip/vip_b.png");
// 	sprite_cion->setAnchorPoint(ccp(0,0));
// 	sprite_cion->setPosition(ccp(35,35));
// 	cell->addChild(sprite_cion);
// 
// 	char str_level[20];
// 	sprintf(str_level,"%d",oneVipGift->number());
// 	CCLabelBMFont * l_vipLevel = CCLabelBMFont::create(str_level.,"res_ui/font/ziti_3.fnt",);
// 	l_vipLevel->setScale(0.9f);
// 	l_vipLevel->setAnchorPoint(ccp(0.5f,0.5f));
// 	l_vipLevel->setPosition(ccp(12,-2));
// 	sprite_cion->addChild(l_vipLevel);
// 
// 	for(int j = 0;j<oneVipGift->goods_size();++j)
// 	{
// 		if (j >= 2)
// 			continue;
// 
// 		GoodsInfo * goodInfo = new GoodsInfo();
// 		goodInfo->CopyFrom(oneVipGift->goods(j));
// 		int num = 0;
// 		if (j < oneVipGift->goodsnumber_size())
// 			num = oneVipGift->goodsnumber(j);
// 
// 		VipRewardCellItem * goodsItem = VipRewardCellItem::create(goodInfo,num);
// 		goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
// 		goodsItem->setPosition(ccp(116+79*j,35));
// 		cell->addChild(goodsItem);
// 
// 		delete goodInfo;
// 	}
	

	return cell;
}

unsigned int VipEveryDayRewardsUI::numberOfCellsInTableView( CCTableView *table )
{
	return GameView::getInstance()->m_vipEveryDayRewardsList.size();
}

void VipEveryDayRewardsUI::refreshTableViewWithOutChangeOffSet()
{
	CCPoint _s = m_tableView->getContentOffset();
	int _h = m_tableView->getContentSize().height + _s.y;
	m_tableView->reloadData();
	CCPoint temp = ccp(_s.x,_h - m_tableView->getContentSize().height);
	m_tableView->setContentOffset(temp); 
}


///////////////////////////////////////////////////////////////////////////////////////////////////

VipEveryDayRewardCell::VipEveryDayRewardCell()
{

}

VipEveryDayRewardCell::~VipEveryDayRewardCell()
{
	delete curOneVipGift;
}

VipEveryDayRewardCell* VipEveryDayRewardCell::create( COneVipGift * oneVipGift )
{
	VipEveryDayRewardCell * vipEveryDayRewardCell = new VipEveryDayRewardCell();
	if (vipEveryDayRewardCell && vipEveryDayRewardCell->init(oneVipGift))
	{
		vipEveryDayRewardCell->autorelease();
		return vipEveryDayRewardCell;
	}
	CC_SAFE_DELETE(vipEveryDayRewardCell);
	return NULL;
}

bool VipEveryDayRewardCell::init( COneVipGift * oneVipGift )
{
	if (UIScene::init())
	{
		m_pUiLayer->setSwallowsTouches(false);

		curOneVipGift = new COneVipGift();
		curOneVipGift->CopyFrom(*oneVipGift);

		UIImageView * image_frame = UIImageView::create();
		image_frame->setTexture("res_ui/kuang02_new.png");
		image_frame->setScale9Enable(true);
		image_frame->setScale9Size(CCSizeMake(423,72));
		image_frame->setCapInsets(CCRectMake(16,33,1,1));
		image_frame->setAnchorPoint(ccp(0.5f,0.5f));
		image_frame->setPosition(ccp(423/2,72/2));
		m_pUiLayer->addWidget(image_frame);
 		
// 		UIImageView * image_line = UIImageView::create();
// 		image_line->setTexture("res_ui/henggang_red.png");
// 		image_line->setAnchorPoint(ccp(0.5f,0.5f));
// 		image_line->setPosition(ccp(423/2,2));
// 		image_line->setScaleX(2.3f);
// 		m_pUiLayer->addWidget(image_line);

		UIImageView * imageView_icon = UIImageView::create();
		imageView_icon->setTexture("res_ui/vip/vip_b.png");
		imageView_icon->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_icon->setPosition(ccp(60,36));
		m_pUiLayer->addWidget(imageView_icon);

		char str_level[20];
		sprintf(str_level,"%d",oneVipGift->number());
		UILabelBMFont * l_vipLevel = UILabelBMFont::create();
		l_vipLevel->setFntFile("res_ui/font/ziti_3.fnt");
		l_vipLevel->setText(str_level);
		l_vipLevel->setScale(0.9f);
		l_vipLevel->setAnchorPoint(ccp(0.5f,0.5f));
		l_vipLevel->setPosition(ccp(13,-2));
		imageView_icon->addChild(l_vipLevel);

		for(int j = 0;j<oneVipGift->goods_size();++j)
		{
			if (j >= 2)
				continue;

			GoodsInfo * goodInfo = new GoodsInfo();
			goodInfo->CopyFrom(oneVipGift->goods(j));
			int num = 0;
			if (j < oneVipGift->goodsnumber_size())
				num = oneVipGift->goodsnumber(j);

			VipRewardCellItem * goodsItem = VipRewardCellItem::create(goodInfo,num);
			goodsItem->ignoreAnchorPointForPosition(false);
			goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
			goodsItem->setPosition(ccp(150+79*j,36));
			m_pUiLayer->addChild(goodsItem);

			delete goodInfo;
		}

		switch(oneVipGift->status())
		{
		case 1:   //不可领取
		 	{
		 		UIImageView * imageView_di = UIImageView::create();
				imageView_di->setTexture("res_ui/zhezhao_btn.png");
				imageView_di->setAnchorPoint(ccp(0.5f,0.5f));
				imageView_di->setPosition(ccp(336,36));
		 		m_pUiLayer->addWidget(imageView_di);
				imageView_di->setScale(1.2f);

				std::string str_des = "VIP";
				char s_level[20];
				sprintf(s_level,"%d",oneVipGift->number());
				str_des.append(s_level);
				str_des.append(StringDataManager::getString("vip_reward_cannotGet"));
		 		UILabel *l_des = UILabel::create();
				l_des->setFontName(APP_FONT_NAME);
				l_des->setFontSize(18);
				l_des->setText(str_des.c_str());
				l_des->setAnchorPoint(ccp(0.5f,0.5f));
				l_des->setPosition(ccp(336,36));
				m_pUiLayer->addWidget(l_des);
			}
			break;
		case 2:   //可领取
			{
				UIButton * btn_get = UIButton::create();
				btn_get->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				btn_get->setScale9Enable(true);
				btn_get->setScale9Size(CCSizeMake(108,43));
				btn_get->setCapInsets(CCRectMake(18,9,2,23));
				btn_get->setAnchorPoint(ccp(0.5f,0.5f));
				btn_get->setPosition(ccp(336,36));
				btn_get->setTouchEnable(true);
				btn_get->setPressedActionEnabled(true);
				btn_get->addReleaseEvent(this,coco_releaseselector(VipEveryDayRewardCell::getRewardEvent));
				m_pUiLayer->addWidget(btn_get);

				UILabel *l_get = UILabel::create();
				l_get->setFontName(APP_FONT_NAME);
				l_get->setFontSize(18);
				l_get->setText(StringDataManager::getString("btn_lingqu"));
				l_get->setAnchorPoint(ccp(0.5f,0.5f));
				l_get->setPosition(ccp(0,0));
				btn_get->addChild(l_get);
			}
			break;
		case 3:   //已领取
			{
				UIImageView * imageView_di = UIImageView::create();
				imageView_di->setTexture("res_ui/zhezhao_btn.png");
				imageView_di->setAnchorPoint(ccp(0.5f,0.5f));
				imageView_di->setPosition(ccp(336,36));
				m_pUiLayer->addWidget(imageView_di);
				imageView_di->setScale(1.2f);

				UILabel *l_des = UILabel::create();
				l_des->setFontName(APP_FONT_NAME);
				l_des->setFontSize(18);
				l_des->setText(StringDataManager::getString("vip_reward_Geted"));
				l_des->setAnchorPoint(ccp(0.5f,0.5f));
				l_des->setPosition(ccp(336,36));
				m_pUiLayer->addWidget(l_des);
			}
			break;
		}

		this->setContentSize(CCSizeMake(423,72));

		return true;
	}
	return false;
}

void VipEveryDayRewardCell::getRewardEvent( CCObject *pSender )
{
	//req to get reward
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5119);
}
