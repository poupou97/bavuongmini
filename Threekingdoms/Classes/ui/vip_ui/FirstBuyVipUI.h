#ifndef _VIPUI_FIRSTBUYVIPUI_H_
#define _VIPUI_FIRSTBUYVIPUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CRewardProp;

class FirstBuyVipUI :public UIScene
{
public:
	FirstBuyVipUI();
	~FirstBuyVipUI();

	static FirstBuyVipUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void CloseEvent(CCObject * pSender);

	void VipBuyEvent(CCObject *pSender);

	void RefreshGoodsRewards( std::vector<CRewardProp*> list );
private:
	UILayer *u_layer;
};
#endif