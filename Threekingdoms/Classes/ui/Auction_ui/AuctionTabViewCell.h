#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;
class AuctionUi;
class AuctionTabViewCell:public CCTableViewCell
{
public:
	AuctionTabViewCell(void);
	~AuctionTabViewCell(void);

	static AuctionTabViewCell*create(int idx);
	bool init(int idx);

	void onEnter();
	void onExit();

	void goodsPurchase(CCObject * obj);
	void goodsConsign(CCObject * obj);

	std::string getEquipmentQualityByIndex(int quality);
	ccColor3B getEquipmentColorByQuality(int quality);
private:
	int currId;
	CCSize winsize;
	AuctionUi * auctionui;
};

