#include "AuctionGoodsInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "AuctionUi.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "AppMacros.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"

AuctionGoodsInfo::AuctionGoodsInfo(void)
{
}


AuctionGoodsInfo::~AuctionGoodsInfo(void)
{
}

AuctionGoodsInfo * AuctionGoodsInfo::create( GoodsInfo * goods,int curTab )
{
	AuctionGoodsInfo * auction_ =new AuctionGoodsInfo();
	if (auction_ && auction_->init(goods,curTab))
	{
		auction_->autorelease();
		return auction_;
	}
	CC_SAFE_DELETE(auction_);
	return NULL;
}

bool AuctionGoodsInfo::init(GoodsInfo * goods,int curTab)
{
	if (GoodsItemInfoBase::init(goods,GameView::getInstance()->EquipListItem,0))
	{
		m_goodsName = goods->name();
		switch(curTab)
		{
		case 0:
			{
				const char *str_buy = StringDataManager::getString("goods_auction_buy");
				const char *str_cancel = StringDataManager::getString("goods_auction_search");
				//���� ����
				UIButton * Button_purchase= UIButton::create();
				Button_purchase->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
				Button_purchase->setTouchEnable(true);
				Button_purchase->setPressedActionEnabled(true);
				Button_purchase->addReleaseEvent(this,coco_releaseselector(AuctionGoodsInfo::callBackPurchase));
				Button_purchase->setAnchorPoint(ccp(0.5f,0.5f));
				Button_purchase->setScale9Enable(true);
				Button_purchase->setScale9Size(CCSizeMake(80,43));
				Button_purchase->setCapInsets(CCRect(18,9,2,23));
				Button_purchase->setPosition(ccp(80,25));

				UILabel * Label_purchase = UILabel::create();
				Label_purchase->setText(str_buy);
				Label_purchase->setAnchorPoint(ccp(0.5f,0.5f));
				Label_purchase->setFontName(APP_FONT_NAME);
				Label_purchase->setFontSize(18);
				Label_purchase->setPosition(ccp(0,0));
				Button_purchase->addChild(Label_purchase);
				m_pUiLayer->addWidget(Button_purchase);

				UIButton * Button_cancel= UIButton::create();
				Button_cancel->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
				Button_cancel->setTouchEnable(true);
				Button_cancel->setPressedActionEnabled(true);
				Button_cancel->addReleaseEvent(this,coco_releaseselector(AuctionGoodsInfo::callBackSearch));
				Button_cancel->setAnchorPoint(ccp(0.5f,0.5f));
				Button_cancel->setScale9Enable(true);
				Button_cancel->setScale9Size(CCSizeMake(80,43));
				Button_cancel->setCapInsets(CCRect(18,9,2,23));
				Button_cancel->setPosition(ccp(200,25));

				UILabel * Label_cancel= UILabel::create();
				Label_cancel->setText(str_cancel);
				Label_cancel->setAnchorPoint(ccp(0.5f,0.5f));
				Label_cancel->setFontName(APP_FONT_NAME);
				Label_cancel->setFontSize(18);
				Label_cancel->setPosition(ccp(0,0));
				Button_cancel->addChild(Label_cancel);
				m_pUiLayer->addWidget(Button_cancel);
			}break;
		case 1:
			{
				const char *str_recove = StringDataManager::getString("goods_auction_recove");
				const char *str_cancel = StringDataManager::getString("goods_auction_search");
				// ���� ����
				UIButton * Button_recover= UIButton::create();
				Button_recover->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
				Button_recover->setTouchEnable(true);
				Button_recover->setPressedActionEnabled(true);
				Button_recover->addReleaseEvent(this,coco_releaseselector(AuctionGoodsInfo::callBackRecover));
				Button_recover->setAnchorPoint(ccp(0.5f,0.5f));
				Button_recover->setScale9Enable(true);
				Button_recover->setScale9Size(CCSizeMake(80,43));
				Button_recover->setCapInsets(CCRect(18,9,2,23));
				Button_recover->setPosition(ccp(80,25));

				UILabel * Label_recover = UILabel::create();
				Label_recover->setText(str_recove);
				Label_recover->setAnchorPoint(ccp(0.5f,0.5f));
				Label_recover->setFontName(APP_FONT_NAME);
				Label_recover->setFontSize(18);
				Label_recover->setPosition(ccp(0,0));
				Button_recover->addChild(Label_recover);
				m_pUiLayer->addWidget(Button_recover);

				UIButton * Button_cancel= UIButton::create();
				Button_cancel->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
				Button_cancel->setTouchEnable(true);
				Button_cancel->setPressedActionEnabled(true);
				Button_cancel->addReleaseEvent(this,coco_releaseselector(AuctionGoodsInfo::callBackSearch));
				Button_cancel->setAnchorPoint(ccp(0.5f,0.5f));
				Button_cancel->setScale9Enable(true);
				Button_cancel->setScale9Size(CCSizeMake(80,43));
				Button_cancel->setCapInsets(CCRect(18,9,2,23));
				Button_cancel->setPosition(ccp(200,25));

				UILabel * Label_cancel = UILabel::create();
				Label_cancel->setText(str_cancel);
				Label_cancel->setAnchorPoint(ccp(0.5f,0.5f));
				Label_cancel->setFontName(APP_FONT_NAME);
				Label_cancel->setFontSize(18);
				Label_cancel->setPosition(ccp(0,0));
				Button_cancel->addChild(Label_cancel);
				m_pUiLayer->addWidget(Button_cancel);
			}break;;

		}
		return true;
	}
	return false;
}

void AuctionGoodsInfo::onEnter()
{
	GoodsItemInfoBase::onEnter();

}

void AuctionGoodsInfo::onExit()
{
	GoodsItemInfoBase::onExit();
}

bool AuctionGoodsInfo::ccTouchBegan( CCTouch * pTouch,CCEvent * pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}

void AuctionGoodsInfo::callBackPurchase( CCObject * obj )
{
	//goodsid
	AuctionUi * auctionui= (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	auctionui->setopType=5;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1803,auctionui);
	GameUtils::playGameSound(PURCHASE_SUCCESS, 2, false);

	this->removeFromParentAndCleanup(true);
}

void AuctionGoodsInfo::callBackRecover( CCObject * obj )
{
	//goodsid
	AuctionUi * auctionui= (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	auctionui->setopType=3;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1803,auctionui);
	this->removeFromParentAndCleanup(true);
}

void AuctionGoodsInfo::callBackCancel( CCObject * obj )
{
	this->removeFromParentAndCleanup(true);
}

void AuctionGoodsInfo::callBackSearch( CCObject * obj )
{
	AuctionUi *auctionui = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	if(auctionui == NULL)
	{
		return;
	}

	auctionui->labelType->setText(auctionui->acutionTypevector.at(0)->typedesc().c_str());
	auctionui->setAuctiontype= auctionui->acutionTypevector.at(0)->type();
	auctionui->isSelectChat = false;
	auctionui->labelKind->setText(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->subtypedescs(0).c_str());
	auctionui->labelLevel->setText(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->leveldescs(0).c_str());
	auctionui->labelQuality->setText(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->qualitydescs(0).c_str());
	const char *strings_goldAll = StringDataManager::getString("auction_SelectMoney");
	auctionui->label_gold->setText(strings_goldAll);

	auctionui->setpageindex=1;
	auctionui->setAuctiontype = 0;
	auctionui->setAuctionsubtype = 0;
	auctionui->setlevel = 0;
	auctionui->setMoney = 0;
	auctionui->setquality = 0;
	auctionui->setorderby = 0;
	auctionui->setorderbydesc = 0;

	auctionui->setname = m_goodsName;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,auctionui);
	removeFromParentAndCleanup(true);
	auctionui->callBack_btn_back(obj);
}
