#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/element/CAuctionType.h"

USING_NS_CC;
USING_NS_CC_EXT;

class AnnexItem;
class AuctionTabView;
class PacPageView;
class RichTextInputBox;
class UITab;
typedef enum
{
	AUCTIONTABVIEW = 401,
	TAGWHEREEXLIST=402,
	AUCTIONLAYER=420,
	AUCTIONCONSIGGOODSINFONTAG = 421,
};
enum
{
	PURCHASETABS=0,
	CONSIGNTABS=1
	
};

class AuctionUi:public UIScene
{
public:
	AuctionUi(void);
	~AuctionUi(void);

	static AuctionUi * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

public:
	void callBackchangebuyOrsale(CCObject * obj); 

	UILabelBMFont * LabelBMFont_selfConsignIsNull;

	void callBack_btn_buyOfqualitySort(CCObject * obj);
	void callBack_btn_LvSort(CCObject * obj);
	void callBack_btn_remainTimeSort(CCObject * obj);
	void callBack_btn_priceSort(CCObject * obj);

	void callBack_btn_typeList(CCObject * obj);
	void callBack_btn_lv(CCObject * obj);
	void callBack_btn_kind(CCObject * obj);
	void callBack_btn_quality(CCObject * obj);
	void callBack_btn_gold(CCObject * obj);
	
	void callBAck_btn_find(CCObject *obj);

	void callBack_btn_EnterConsignMent(CCObject * obj);

	void callBack_btn_priceCounter(CCObject * obj);
	void callBack_btn_amountCounter(CCObject* obj);
	void callBack_Pricecounter(CCObject *obj);
	void callBack_amountCounter(CCObject* obj);

	UICheckBox * checkbox_gold;
	UICheckBox * checkbox_Money;
	void setGoldDeal(CCObject * obj,CheckBoxEventType type);
	void setMoneyDeal(CCObject * obj,CheckBoxEventType type);

	void setGoldDealCopyCheckbox(CCObject * obj);
	void setMoneyCopyCheckbox(CCObject * obj);

	void callBack_btn_back(CCObject * obj);
	void callBack_btn_consignment(CCObject * obj);
	
	void addAuctionPacInfo(CCObject * obj);
	
	void getbackAuction(CCObject * obj);

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	void CurrentPageViewChanged(CCObject * obj);
	void callBack_btn_colse(CCObject * obj);

	void PageScrollToDefault();

	bool isSelectChat;
	UILabel * goldlabel_;
	UILabel * goldIngot;
	void refreshPlayerMoney();
	PacPageView *pageView;
	RichTextInputBox * checkInfoBox;
	UITab *buyOrsale;
	UILayer *layer_;
	UILabel * handCharge;

	int curMaxConsignGoodsNum;
public:
	UIPanel* consignSales;
	UIPanel* purchase;
	UIPanel* puerchaseSelect;
	UIPanel* consignSelect;
	UIImageView * iamge_;
	UILabel * labelPrice;
	UILabel * labelAmount;

	UIImageView * dealMoney;
	UIImageView * changeCost;
	UILabel * changeCostLabel;
	UILabelBMFont * consignAmount;

	AuctionTabView * auctionView;
	UIImageView *currentPage;
public:
	UILabel*labelType;
	UILabel*labelLevel;
	UILabel*labelKind;
	UILabel*labelQuality;
	UILabel * label_gold;
private:
	UIImageView * buyandquality;
	UIImageView * image_level;
	UIImageView * image_remaintime;
	UIImageView * image_priceup;

	bool buyandqualityLift;
	bool image_levelLift;
	bool image_remaintimLift;
	bool image_priceupLift;

	AnnexItem *auction;
	bool isConsignGoods;
public:
	//req1801
	int setAuctiontype;
	int setAuctionsubtype;
	int setprofession;
	int setlevel;
	int setquality;
	int setMoney;
	int setcurrency;
	std::string setname;
	//const char* setname;
	int setpageindex;
	int setorderby;
	int setorderbydesc;
	////////
	bool hasMorePage;

	int successConsign;
public:
	//req 1802
	int setIndex;
	int setPrice;
	int setPropNum;
	int setPriceType;//����ж� ����
public:
	//req 1803
	int setopType;
	int goodsid;
public:
	std::vector<CAuctionType *>acutionTypevector;
	int tabsType;
};	

