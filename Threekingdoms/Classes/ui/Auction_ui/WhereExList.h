#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

enum{
	TYPELIST=0,
	KINDLIST,
	LEVELLIST,
	QUALITYLIST,
	GOLDLIST
};

class WhereExList:public UIScene
{
public:
	WhereExList(void);
	~WhereExList(void);

	static WhereExList *create(int type);
	
	bool init(int type);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	
	void selectType(CCObject * obj);
	void selectKind(CCObject * obj);
	void selectLevel(CCObject * obj);
	void selectQuality(CCObject * obj);
	void seletGold(CCObject * obj);
public:

};

