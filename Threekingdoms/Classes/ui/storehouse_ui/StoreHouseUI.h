#ifndef _STOREHOUSEUI_STOREHOUSEUI_H
#define _STOREHOUSEUI_STOREHOUSEUI_H

#include "../extensions/UIScene.h"
#include "../backpackscene/PacPageView.h"

USING_NS_CC;
USING_NS_CC_EXT;

class FolderInfo;
class PacPageView;
class StoreHousePageView;

class StoreHouseUI : public UIScene
{
public:
	StoreHouseUI();
	~StoreHouseUI();

	struct ReqData{
		int mode;
		int source;
		int num;
	};

	static StoreHouseUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

// 	std::vector<Coordinate> CoordinateVector;
// 
// 	std::string storePageViewPanelName[4];
// 	std::string storeItem_name[20];
// 
// 	int curStoreItemIndex;
// 	FolderInfo * curStoreFolder;
// 
// 	int itemNumEveryPage ;

	//点击的是背包项还是仓库项（默认为背包）
	bool storeOrPac ;

	void PutInOrOutEvent(CCObject * pSender);

	void ReloadData();
	void ReloadOneStoreHouseItem(FolderInfo * folderInfo);

	void PageScrollToDefault();

private: 
	UILayer * u_layer;
	UIPanel * ppanel;
	StoreHousePageView * m_storePageView;
	PacPageView * m_pacPageView;

	//左边pageview的指示点
	UIImageView * pointLeft;
	//右边pageview的指示点
	UIImageView * pointRight;

	UILabel *Label_gold;
	UILabel *Label_goldIngot;

	void reloadInit();

	virtual void update(float dt);

public:
	float m_remainArrangePac;
	float m_remainArrangeStore;
private:
	void CloseEvent(CCObject * pSender);
	void SortPacEvent(CCObject *pSender);
	void SortStoreHouseEvent(CCObject * pSender);
	void ShopEvent(CCObject * pSender);

	void LeftPageViewChanged(CCObject* pSender);
	void RightPageViewChanged(CCObject* pSender);

	void GetStoreItemNum(CCObject* pSender);
};

#endif