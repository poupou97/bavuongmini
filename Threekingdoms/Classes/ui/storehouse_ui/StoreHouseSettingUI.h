#ifndef _STOREHOUSEUI_STOREHOUSESETTINGUI_H
#define _STOREHOUSEUI_STOREHOUSESETTINGUI_H

#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class StoreHouseSettingUI : public UIScene
{
public:
	StoreHouseSettingUI();
	~StoreHouseSettingUI();

	static StoreHouseSettingUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	UIPanel * ppanel;
	UILayer * u_layer;

	UIPanel * panel_enterPassWord;
	UILayer * layer_enterPassWord;
	UIPanel * panel_changePassWord;
	UILayer * layer_changePassWord;
	UIPanel * panel_setPassWord;
	UILayer * layer_setPassWord;

private:
	void CloseEvent(CCObject *pSender);

};

#endif

