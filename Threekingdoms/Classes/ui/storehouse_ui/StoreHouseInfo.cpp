#include "StoreHouseInfo.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../GameView.h"
#include "StoreHouseUI.h"
#include "../extensions/Counter.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"

StoreHouseInfo::StoreHouseInfo():
m_nfolderIndex(0)
{
}


StoreHouseInfo::~StoreHouseInfo()
{
}

StoreHouseInfo * StoreHouseInfo::create( FolderInfo * folder )
{
	StoreHouseInfo * storeHosueInfo = new StoreHouseInfo();
	if (storeHosueInfo && storeHosueInfo->init(folder))
	{
		storeHosueInfo->autorelease();
		return storeHosueInfo;
	}
	CC_SAFE_DELETE(storeHosueInfo);
	return NULL;
}

bool StoreHouseInfo::init( FolderInfo * folder )
{
	GoodsInfo * goodsInfo = new GoodsInfo();
	goodsInfo->CopyFrom(folder->goods());
	m_nfolderIndex = folder->id();
	m_quantity = folder->quantity();
	if (goodsInfo != NULL)
	if (GoodsItemInfoBase::init(goodsInfo,GameView::getInstance()->EquipListItem,0))
	{
		delete goodsInfo;
		//�Ӳֿ�ȡ��
		UIButton * Button_getOut = UIButton::create();
		Button_getOut->setTextures(buttonImagePath.c_str(),
			buttonImagePath.c_str(),
			"");
		Button_getOut->setAnchorPoint(ccp(0.5f,0.5f));
		Button_getOut->setPosition(ccp(141,25));
		Button_getOut->setScale9Enable(true);
		Button_getOut->setScale9Size(CCSizeMake(91,43));
		Button_getOut->setCapInsets(CCRect(18,9,2,23));
		Button_getOut->setTouchEnable(true);
		Button_getOut->addReleaseEvent(this,coco_releaseselector(StoreHouseInfo::GetOutEvent));
		Button_getOut->setPressedActionEnabled(true);

		const char *strings_getOut = StringDataManager::getString("storeHouse_getOut");
		UILabel * Label_getOut = UILabel::create();
		Label_getOut->setText(strings_getOut);
		Label_getOut->setFontName(APP_FONT_NAME);
		Label_getOut->setFontSize(18);
		Label_getOut->setAnchorPoint(ccp(0.5f,0.5f));
		Label_getOut->setPosition(ccp(0,0));
		Button_getOut->addChild(Label_getOut);
		m_pUiLayer->addWidget(Button_getOut);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setAnchorPoint(ccp(0,0));

		return true;
	}
	return false;
}

void StoreHouseInfo::onEnter()
{
	GoodsItemInfoBase::onEnter();
	this->openAnim();
}
void StoreHouseInfo::onExit()
{
	GoodsItemInfoBase::onExit();
}

bool StoreHouseInfo::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	return this->resignFirstResponder(pTouch,this,false);
}
void StoreHouseInfo::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void StoreHouseInfo::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void StoreHouseInfo::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}


void StoreHouseInfo::GetOutEvent( CCObject *pSender )
{
	if (this->isClosing())
		return;

	if (this->m_quantity>1)
	{
		GameView::getInstance()->showCounter(this, callfuncO_selector(StoreHouseInfo::GetStoreItemNum),this->m_quantity);
	}
	else
	{
		StoreHouseUI::ReqData * temp = new StoreHouseUI::ReqData();
		temp->mode = 1;
		temp->source = this->m_nfolderIndex;
		temp->num = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1319,temp);
		delete temp;

		this->removeFromParent();
	}

	
}

void StoreHouseInfo::GetStoreItemNum( CCObject *pSender )
{
	Counter * counter =(Counter *)pSender;

	StoreHouseUI::ReqData * temp = new StoreHouseUI::ReqData();
	temp->mode = 1;
	temp->source = this->m_nfolderIndex;
	temp->num = counter->getInputNum();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1319,temp);
	delete temp;

	this->removeFromParent();
}
