#include "GeneralsListUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GeneralsUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../GameView.h"
#include "NormalGeneralsHeadItem.h"
#include "GeneralsTeachUI.h"
#include "GeneralsEvolutionUI.h"
#include "../../messageclient/protobuf/GeneralMessage.pb.h"
#include "generals_popup_ui/GeneralsFateInfoUI.h"
#include "../../gamescene_state/MainScene.h"
#include "GeneralsShortcutSlot.h"
#include "../../messageclient/element/CShortCut.h"
#include "generals_popup_ui/GeneralsSkillListUI.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "GeneralsShortcutLayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "AppMacros.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/GameUtils.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../gamescene_state/role/ShowBaseGeneral.h"

#define  SHORTCUTLAYERTAG 1000
#define  HeadItemTag  20
#define  GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG 456

//////////////////////////////////////////////////

GeneralsListUI::GeneralsListUI():
isFirstTutorial(false),
isSecondTutorial(false),
isFirstOrSecondMenu(true),
selectCellId(0),
lastSelectCellId(0),
isInFirstGeneralTutorial(false),
Btn_show_general(NULL),
Btn_hide_general(NULL)
{
// 	if (generalBaseMsgList.size()>0)
// 	{
// 		curGeneralBaseMsg->CopyFrom(*generalBaseMsgList.at(0));
// 	}
// 	else 
// 	{
// 	}

}

GeneralsListUI::~GeneralsListUI()
{
	delete curGeneralBaseMsg;
	delete curGeneralDetail;
}
GeneralsListUI* GeneralsListUI::create()
{
	GeneralsListUI * generalsListUI = new GeneralsListUI();
	if (generalsListUI && generalsListUI->init())
	{
		generalsListUI->autorelease();
		return generalsListUI;
	}
	CC_SAFE_DELETE(generalsListUI);
	return NULL;
}

bool GeneralsListUI::init()
{
	if (UIScene::init())
	{
		curGeneralDetail = new CGeneralDetail();
		curGeneralBaseMsg  = new CGeneralBaseMsg();

		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		//武将列表
		panel_generalsList = (UIPanel*)UIHelper::seekWidgetByName(GeneralsUI::ppanel,"Panel_generals_detail");
		panel_generalsList->setVisible(true);
		//  武将列表层
		Layer_generalsList = UILayer::create();
// 		Layer_generalsList->ignoreAnchorPointForPosition(false);
// 		Layer_generalsList->setAnchorPoint(ccp(0.5f,0.5f));
// 		Layer_generalsList->setContentSize(CCSizeMake(800, 480));
// 		Layer_generalsList->setPosition(ccp(winsize.width/2,winsize.height/2));

		addChild(Layer_generalsList);
// 		generalsList = GeneralsListUI::create();
// 		Layer_generalsList->addChild(generalsList);
// 		Layer_generalsList->setVisible(false);
// 		
		UIScrollView* pScrollView = (UIScrollView*)UIHelper::seekWidgetByName(panel_generalsList, "ScrollView_property");
		pScrollView->setBounceEnabled(true);

		Btn_evolution = (UIButton*)UIHelper::seekWidgetByName(panel_generalsList,"Button_evolution"); //进化
		Btn_evolution->setTouchEnable(true);
		Btn_evolution->setPressedActionEnabled(true);
		Btn_evolution->addReleaseEvent(this,coco_releaseselector(GeneralsListUI::EvolutionEvent));

		Btn_teach = (UIButton*)UIHelper::seekWidgetByName(panel_generalsList,"Button_teach"); //传功
		Btn_teach->setTouchEnable(true);
		Btn_teach->setPressedActionEnabled(true);
		Btn_teach->addReleaseEvent(this,coco_releaseselector(GeneralsListUI::TeachEvent));

		Btn_show_general = (UIButton*)UIHelper::seekWidgetByName(panel_generalsList, "Button_show");	// 展示
		Btn_show_general->setTouchEnable(true);
		Btn_show_general->setPressedActionEnabled(true);
		Btn_show_general->addReleaseEvent(this, coco_releaseselector(GeneralsListUI::ShowGeneralEvent));
		Btn_show_general->setVisible(true);

		Btn_hide_general = (UIButton*)UIHelper::seekWidgetByName(panel_generalsList, "Button_hide");	// 收回
		Btn_hide_general->setTouchEnable(true);
		Btn_hide_general->setPressedActionEnabled(true);
		Btn_hide_general->addReleaseEvent(this, coco_releaseselector(GeneralsListUI::HideGeneralEvent));
		Btn_hide_general->setVisible(false);

		l_hp = (UILabel*)UIHelper::seekWidgetByName(panel_generalsList,"Label_hp"); //血量值
        l_hp->setStrokeEnabled(true);
		Image_hp = (UIImageView* )UIHelper::seekWidgetByName(panel_generalsList,"ImageView_hp");
		l_mp = (UILabel*)UIHelper::seekWidgetByName(panel_generalsList,"Label_mp"); //法力值
        l_mp->setStrokeEnabled(true);
		Image_mp = (UIImageView* )UIHelper::seekWidgetByName(panel_generalsList,"ImageView_mp");
		//l_exp = (UILabel*)UIHelper::seekWidgetByName(panel_generalsList,"Label_exp"); //经验值
        //l_exp->setStrokeEnabled(true);
		//Image_exp = (UIImageView* )UIHelper::seekWidgetByName(panel_generalsList,"ImageView_exp");
		l_profession = (UILabel*)UIHelper::seekWidgetByName(panel_generalsList,"Label_job"); //职业
		l_combatPower = (UILabel*)UIHelper::seekWidgetByName(panel_generalsList,"Label_job_combat"); //战力
		l_inBattleTime = (UILabel*)UIHelper::seekWidgetByName(panel_generalsList,"Label_inBattleValue"); //出战时间
		l_restTime = (UILabel*)UIHelper::seekWidgetByName(panel_generalsList,"Label_restValue"); //休息时间
		l_powerValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_power");
		l_aglieValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_aglie");  //敏捷
		l_intelligenceValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_intelligence");  //智力
		l_focusValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_focus");  //专注
		l_phyAttackValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_physical_attacks");
		l_magicAttackValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_magic_attacks");
		l_phyDenValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_power_physical_defense");
		l_magicDenValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_power_magic_defense");
		l_hitValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_hit");  //命中
		l_dodgeValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_dodge"); //闪避
		l_critValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_crit"); //暴击
		l_critDamageValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_crit_damage"); //暴击伤害
		l_atkSpeedValue = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_attack_speed");

		btn_fate_1 = (UIButton*)UIHelper::seekWidgetByName(panel_generalsList,"Button_fate_1");
		btn_fate_1->setTouchEnable(true);
		btn_fate_1->setPressedActionEnabled(true);
		btn_fate_1->addReleaseEvent(this,coco_releaseselector(GeneralsListUI::FateEvent));
		btn_fate_2 = (UIButton*)UIHelper::seekWidgetByName(panel_generalsList,"Button_fate_2");
		btn_fate_2->setTouchEnable(true);
		btn_fate_2->setPressedActionEnabled(true);
		btn_fate_2->addReleaseEvent(this,coco_releaseselector(GeneralsListUI::FateEvent));
		btn_fate_3 = (UIButton*)UIHelper::seekWidgetByName(panel_generalsList,"Button_fate_3");
		btn_fate_3->setTouchEnable(true);
		btn_fate_3->setPressedActionEnabled(true);
		btn_fate_3->addReleaseEvent(this,coco_releaseselector(GeneralsListUI::FateEvent));
		btn_fate_4 = (UIButton*)UIHelper::seekWidgetByName(panel_generalsList,"Button_fate_6");
		btn_fate_4->setTouchEnable(true);
		btn_fate_4->setPressedActionEnabled(true);
		btn_fate_4->addReleaseEvent(this,coco_releaseselector(GeneralsListUI::FateEvent));
		btn_fate_5 = (UIButton*)UIHelper::seekWidgetByName(panel_generalsList,"Button_fate_5");
		btn_fate_5->setTouchEnable(true);
		btn_fate_5->setPressedActionEnabled(true);
		btn_fate_5->addReleaseEvent(this,coco_releaseselector(GeneralsListUI::FateEvent));
		btn_fate_6 = (UIButton*)UIHelper::seekWidgetByName(panel_generalsList,"Button_fate_4");
		btn_fate_6->setTouchEnable(true);
		btn_fate_6->setPressedActionEnabled(true);
		btn_fate_6->addReleaseEvent(this,coco_releaseselector(GeneralsListUI::FateEvent));

		l_fate_1 = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_fate_1");
		l_fate_1->setText("");
		l_fate_2 = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_fate_2");
		l_fate_2->setText("");
		l_fate_3 = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_fate_3");
		l_fate_3->setText("");
		l_fate_4 = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_fate_6");
		l_fate_4->setText("");
		l_fate_5 = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_fate_5");
		l_fate_5->setText("");
		l_fate_6 = (UILabel *)UIHelper::seekWidgetByName(panel_generalsList,"Label_fate_4");
		l_fate_6->setText("");

		this->generalsListStatus = HaveNext;
		//武将列表
		generalList_tableView = CCTableView::create(this,CCSizeMake(245,398));
		generalList_tableView->setDirection(kCCScrollViewDirectionVertical);
		generalList_tableView->setAnchorPoint(ccp(0,0));
		generalList_tableView->setPosition(ccp(74,42));
		generalList_tableView->setContentOffset(ccp(0,0));
		generalList_tableView->setDelegate(this);
		//generalList_tableView->setClippingToBounds(false);
		generalList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		Layer_generalsList->addChild(generalList_tableView);

		
		Layer_generalsTop = UILayer::create();
// 		Layer_generalsTop->ignoreAnchorPointForPosition(false);
// 		Layer_generalsTop->setAnchorPoint(ccp(0.5f,0.5f));
// 		Layer_generalsTop->setContentSize(CCSizeMake(800, 480));
// 		Layer_generalsTop->setPosition(ccp(winsize.width/2,winsize.height/2));

		addChild(Layer_generalsTop);

// 		UIImageView * tableView_kuang = UIImageView::create();
// 		tableView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		tableView_kuang->setScale9Enable(true);
// 		tableView_kuang->setScale9Size(CCSizeMake(260,414));
// 		tableView_kuang->setCapInsets(CCRectMake(30,30,1,1));
// 		tableView_kuang->setAnchorPoint(ccp(0,0));
// 		tableView_kuang->setPosition(ccp(64,34));
// 		Layer_generalsTop->addWidget(tableView_kuang);

		return true;
	}
	return false;
}

void GeneralsListUI::onEnter()
{
	GeneralsListBase::onEnter();
}

void GeneralsListUI::onExit()
{
	GeneralsListBase::onExit();
// 	std::vector<CGeneralBaseMsg*>::iterator iter_generalBaseMsgList;
// 	for (iter_generalBaseMsgList = generalBaseMsgList.begin(); iter_generalBaseMsgList != generalBaseMsgList.end(); ++iter_generalBaseMsgList)
// 	{
// 		delete *iter_generalBaseMsgList;
// 	}
// 	generalBaseMsgList.clear();
}

void GeneralsListUI::scrollViewDidScroll( CCScrollView* view )
{
}

void GeneralsListUI::scrollViewDidZoom( CCScrollView* view )
{
}

void GeneralsListUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	int i = cell->getIdx();
	GeneralsListCell * tempCell = dynamic_cast<GeneralsListCell*>(cell);
	if (!tempCell)
		return;
	
	curGeneralBaseMsg->CopyFrom(*tempCell->getCurGeneralBaseMsg());
	
	selectCellId = cell->getIdx();
	//取出上次选中状态，更新本次选中状态
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
		}
	}

	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(26, 26, 1, 1) , "res_ui/highlight.png");
	pHighlightSpr->setPreferredSize(CCSize(235,72));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setTag(GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG);
	cell->addChild(pHighlightSpr);

	lastSelectCellId = cell->getIdx();

//	if (generalsListStatus == HaveNone)
//	{
		//请求详细信息
		if (curGeneralBaseMsg != NULL)
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)curGeneralBaseMsg->id());
	//}
// 	else if (generalsListStatus == HaveLast)
// 	{
// 		if (i == 0)
// 		{
// 			//req for last
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage-1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 1;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//请求详细信息
// 			if (curGeneralBaseMsg != NULL)
// 				GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)curGeneralBaseMsg->id());
// 		}
// 	}
// 	else if (generalsListStatus == HaveNext)
// 	{
// 		if (i == generalBaseMsgList.size())
// 		{
// 			//req for next
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage+1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 1;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//请求详细信息
// 			if (curGeneralBaseMsg != NULL)
// 				GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)curGeneralBaseMsg->id());
// 		}
// 	}
// 	else if (generalsListStatus == HaveBoth)
// 	{
// 		if (i == 0)
// 		{
// 			//req for lase
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage-1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 1;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else if (i == generalBaseMsgList.size()+1)
// 		{
// 			//req for next
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage+1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 1;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//请求详细信息
// 			if (curGeneralBaseMsg != NULL)
// 				GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)curGeneralBaseMsg->id());
// 		}
// 	}

}

cocos2d::CCSize GeneralsListUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(235,74);
}

cocos2d::extension::CCTableViewCell* GeneralsListUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called
	if (!cell)
	{
		cell = GeneralsListCell::create(idx,GameView::getInstance()->generalBaseMsgList.at(idx)); 
	}
	else
	{
		GeneralsListCell * tempCell = dynamic_cast<GeneralsListCell*>(cell);
		if (tempCell->getChildByTag(GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG))
		{
			tempCell->getChildByTag(GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
		}
		tempCell->RefreshCell(idx,GameView::getInstance()->generalBaseMsgList.at(idx));
	}

	if (idx == 0)
	{
		firstCell = dynamic_cast<GeneralsListCell*>(cell);
	}
	else if (idx == 1)
	{
		secondCell = dynamic_cast<GeneralsListCell*>(cell);
	}

	if(idx == GameView::getInstance()->generalBaseMsgList.size()-4)
	{
		if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
		{
			if (getIsCanReq())
			{
				//req for next
				this->isReqNewly = false;
				GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
				temp->page = this->s_mCurPage+1;
				temp->pageSize = this->everyPageNum;
				temp->type = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
				setIsCanReq(false);
				delete temp;
			}
		}
	}

	if (this->selectCellId == idx)
	{
		CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(26, 26, 1, 1) , "res_ui/highlight.png");
		pHighlightSpr->setPreferredSize(CCSize(235,72));
		pHighlightSpr->setAnchorPoint(CCPointZero);
		pHighlightSpr->setTag(GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG);
		cell->addChild(pHighlightSpr);
	}
	return cell;
}

 unsigned int GeneralsListUI::numberOfCellsInTableView( CCTableView *table )
 {
	 return GameView::getInstance()->generalBaseMsgList.size();
}

void GeneralsListUI::RefreshGeneralsList()
{
	generalList_tableView->reloadData();
}


void GeneralsListUI::RefreshGeneralsListWithOutChangeOffSet()
{
	CCPoint _s = generalList_tableView->getContentOffset();
	int _h = generalList_tableView->getContentSize().height + _s.y;
	generalList_tableView->reloadData();
	CCPoint temp = ccp(_s.x,_h - generalList_tableView->getContentSize().height);
	generalList_tableView->setContentOffset(temp); 
}


CGeneralBaseMsg* GeneralsListUI::getCurGeneralBaseMsg()
{
	return this->curGeneralBaseMsg;
}

void GeneralsListUI::RefreshGeneralsLayerData()
{
	if (Layer_generalsList->getChildByTag(HeadItemTag))
	{
		Layer_generalsList->getChildByTag(HeadItemTag)->removeFromParent();
	}

	NormalGeneralsHeadItemBase * headItem = NormalGeneralsHeadItemBase::create(curGeneralDetail);
	headItem->setAnchorPoint(ccp(0,0));
	headItem->setPosition(ccp(360,285));
	headItem->setTag(HeadItemTag);
	//headItem->setCanShowBigCard(true);
	Layer_generalsList->addChild(headItem);
}

//进化
void GeneralsListUI::EvolutionEvent( CCObject * pSender )
{
	//开启等级
	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(3);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[3];
	}

	if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
	{
		if (curGeneralBaseMsg->has_id())
		{
			if (curGeneralBaseMsg->evolution() == 20)
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_evolution_maxLevel"));
			}
			else
			{
				this->setVisible(false);
				GeneralsUI::generalsEvolutionUI->setVisible(true);

				GeneralsUI::generalsEvolutionUI->isReqNewly = true;
				GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
				temp->page = 0;
				temp->pageSize = GeneralsUI::generalsListUI->everyPageNum;
				temp->type = 3;
				temp->generalId = curGeneralBaseMsg->id();
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
				delete temp;

				
				GeneralsUI::generalsEvolutionUI->createEvolutionHead(curGeneralBaseMsg);
				GeneralsUI::generalsEvolutionUI->RefreshGeneralEvolutionInfo(curGeneralDetail);
			}
		}else
		{
			//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *str1  = ((CCString*)strings->objectForKey("generals_pleaseaddgenerals"))->m_sString.c_str();
			const char *str1 = StringDataManager::getString("generals_pleaseaddgenerals");
			char* p1 =const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(p1);
		}
	}
	else
	{
		std::string str_des = StringDataManager::getString("feature_will_be_open_function");
		char str_level[20];
		sprintf(str_level,"%d",openlevel);
		str_des.append(str_level);
		str_des.append(StringDataManager::getString("feature_will_be_open_open"));
		GameView::getInstance()->showAlertDialog(str_des.c_str());
	}
	
}
//传功
void GeneralsListUI::TeachEvent( CCObject * pSender )
{

	//开启等级
	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(4);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[4];
	}

	if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
	{
		if (curGeneralBaseMsg->has_id())
		{
			if (curGeneralBaseMsg->currentquality() == 50)
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_teach_maxLevel"));
			}
			else
			{
				this->setVisible(false);
				GeneralsUI::generalsTeachUI->setVisible(true);

				GeneralsUI::generalsTeachUI->isReqNewly = true;
				GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
				temp->page = 0;
				temp->pageSize = GeneralsUI::generalsListUI->everyPageNum;
				temp->type = 2;
				temp->generalId = curGeneralBaseMsg->id();
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
				delete temp;
				
				GeneralsUI::generalsTeachUI->createTeachHeadItem(curGeneralBaseMsg);
				GeneralsUI::generalsTeachUI->setGeneralsAddedPropertyToDefault(curGeneralDetail);
				GeneralsUI::generalsTeachUI->ResetEvent(pSender);
			}
		}
		else
		{	
			//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *str1  = ((CCString*)strings->objectForKey("generals_pleaseaddgenerals"))->m_sString.c_str();
			const char *str1 = StringDataManager::getString("generals_pleaseaddgenerals");
			char* p1 =const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(p1);
		}
	}
	else
	{
		std::string str_des = StringDataManager::getString("feature_will_be_open_function");
		char str_level[20];
		sprintf(str_level,"%d",openlevel);
		str_des.append(str_level);
		str_des.append(StringDataManager::getString("feature_will_be_open_open"));
		GameView::getInstance()->showAlertDialog(str_des.c_str());
	}
}

void GeneralsListUI::RefreshGeneralsInfo( CGeneralDetail * generalDetail )
{
	curGeneralDetail->CopyFrom(*generalDetail);
	//evolution
	curGeneralBaseMsg->set_evolution(curGeneralDetail->evolution());
	curGeneralBaseMsg->set_evolutionexp(curGeneralDetail->evolutionexp());
	//teach
	curGeneralBaseMsg->set_currentquality(curGeneralDetail->currentquality());
	curGeneralBaseMsg->set_qualityexp(curGeneralDetail->qualityexp());

	this->RefreshGeneralsLayerData();

	std::string hpValue = "";
	char s_hp[30];
	sprintf(s_hp,"%d",generalDetail->activerole().hp());
	hpValue.append(s_hp);
	hpValue.append("/");
	char s_maxhp[30];
	sprintf(s_maxhp,"%d",generalDetail->activerole().maxhp());
	hpValue.append(s_maxhp);
	l_hp->setText(hpValue.c_str());//血量值

	float hpScaleValue = (generalDetail->activerole().hp()*1.0f)/(generalDetail->activerole().maxhp()*1.0f);
	if (hpScaleValue > 1.0f )
		hpScaleValue = 1.0f;

	Image_hp->setScaleX(hpScaleValue*1.03f);

// 	std::string mpValue = "";
// 	char s_mp[30];
// 	sprintf(s_mp,"%d",generalDetail->activerole().mp());
// 	mpValue.append(s_mp);
// 	mpValue.append("/");
// 	char s_maxmp[30];
// 	sprintf(s_maxmp,"%d",generalDetail->activerole().maxmp());
// 	mpValue.append(s_maxmp);
// 	l_mp->setText(mpValue.c_str());//法力值
// 
// 	float mpScaleValue = (generalDetail->activerole().mp()*1.0f)/(generalDetail->activerole().maxmp()*1.0f);
// 	if (mpScaleValue > 1.0f )
// 		mpScaleValue = 1.0f;
// 
// 	Image_mp->setScaleX(mpScaleValue*1.03f);

// 	std::string expValue = "";
// 	char s_experience[30];
// 	sprintf(s_experience,"%d",generalDetail->experience());
// 	expValue.append(s_experience);
// 	expValue.append("/");
// 	char s_nextlevelexperience[30];
// 	sprintf(s_nextlevelexperience,"%d",generalDetail->nextlevelexperience());
// 	expValue.append(s_nextlevelexperience);
// 	l_exp->setText(expValue.c_str());   //经验值
// 
// 	float expScaleValue = (generalDetail->experience()*1.0f)/(generalDetail->nextlevelexperience()*1.0f);
// 	if (expScaleValue > 1.0f )
// 		expScaleValue = 1.0f;
// 
// 	Image_exp->setScaleX(expScaleValue*1.03f);

	l_profession->setText(generalDetail->profession().c_str()); //职业

	char s_fightpoint[30];
	sprintf(s_fightpoint,"%d",generalDetail->fightpoint());
	l_combatPower->setText(s_fightpoint) ; //战力

	const char *strings_s = StringDataManager::getString("generals_strategies_s");
	char strInbattleTime[20];
	sprintf(strInbattleTime,strings_s,generalDetail->showtimebase()/1000);
	l_inBattleTime->setText(strInbattleTime) ;

	char strRestTime[20];
	sprintf(strRestTime,strings_s,generalDetail->resttimebase()/1000);
	l_restTime->setText(strRestTime) ;

	char s_strength[30];
	sprintf(s_strength,"%d",generalDetail->aptitude().strength());
	l_powerValue->setText(s_strength) ; //力量

	char s_dexterity[30];
	sprintf(s_dexterity,"%d",generalDetail->aptitude().dexterity());
	l_aglieValue->setText(s_dexterity) ;  //敏捷

	char s_intelligence [30];
	sprintf(s_intelligence,"%d",generalDetail->aptitude().intelligence());
	l_intelligenceValue->setText(s_intelligence) ;  //智力

	char s_focus[30];
	sprintf(s_focus,"%d",generalDetail->aptitude().focus());
	l_focusValue->setText(s_focus) ;  //专注

	std::string phyAttackValue = "";
	char s_minattack[30];
	sprintf(s_minattack,"%d",generalDetail->aptitude().minattack());
	phyAttackValue.append(s_minattack);
	phyAttackValue.append("-");
	char s_maxattack[30];
	sprintf(s_maxattack,"%d",generalDetail->aptitude().maxattack());
	phyAttackValue.append(s_maxattack);
	l_phyAttackValue->setText(phyAttackValue.c_str());

	std::string magicAttackValue = "";
	char s_minmagicattack[30];
	sprintf(s_minmagicattack,"%d",generalDetail->aptitude().minmagicattack());
	magicAttackValue.append(s_minmagicattack);
	magicAttackValue.append("-");
	char s_maxmagicattack[30];
	sprintf(s_maxmagicattack,"%d",generalDetail->aptitude().maxmagicattack());
	magicAttackValue.append(s_maxmagicattack);
	l_magicAttackValue->setText(magicAttackValue.c_str());

 	std::string phyDenValue = "";
 	char s_mindefend[30];
 	sprintf(s_mindefend,"%d",generalDetail->aptitude().mindefend());
 	phyDenValue.append(s_mindefend);
 	phyDenValue.append("-");
 	char s_maxdefend[30];
 	sprintf(s_maxdefend,"%d",generalDetail->aptitude().maxdefend());
 	phyDenValue.append(s_maxdefend);
 	l_phyDenValue->setText(phyDenValue.c_str());
	 
	std::string magicDenValue = "";
	char s_minmagicdefend[30];
	sprintf(s_minmagicdefend,"%d",generalDetail->aptitude().minmagicdefend());
	magicDenValue.append(s_minmagicdefend);
	magicDenValue.append("-");
	char s_maxmagicdefend[30];
	sprintf(s_maxmagicdefend,"%d",generalDetail->aptitude().maxmagicdefend());
	magicDenValue.append(s_maxmagicdefend);
	l_magicDenValue->setText(magicDenValue.c_str());

	char s_hit[30];
	sprintf(s_hit,"%d",generalDetail->aptitude().hit());
	l_hitValue->setText(s_hit); //命中
	char s_dodge[30];
	sprintf(s_dodge,"%d",generalDetail->aptitude().dodge());
	l_dodgeValue->setText(s_dodge); //闪避
	char s_crit[30];
	sprintf(s_crit,"%d",generalDetail->aptitude().crit());
	l_critValue->setText(s_crit); //暴击
	char s_critdamage[30];
	sprintf(s_critdamage,"%d",generalDetail->aptitude().critdamage());
	l_critDamageValue->setText(s_critdamage); //暴击伤害
	char s_attackspeed[30];
	sprintf(s_attackspeed,"%d",generalDetail->aptitude().attackspeed());
	l_atkSpeedValue->setText(s_attackspeed); //攻速
	
	// 刷新 展示 or 收回
	int nState = ShowMyGeneral::getInstance()->getShowState(generalDetail->generalid());
	if (1 == nState)
	{
		Btn_show_general->setVisible(true);
		Btn_hide_general->setVisible(false);
	}
	else if(0 == nState)
	{
		Btn_show_general->setVisible(false);
		Btn_hide_general->setVisible(true);
	}

	RefreshGeneralsFate(generalDetail);
// 	setSkillTypeByGeneralsDetail(generalDetail);   //先刷新职业再刷新快捷栏（快捷栏记录了当前武将的职业）
 	RefreshGeneralsSkill(generalDetail);
	
	RefreshEvolutionEnabled(generalDetail);
}

bool GeneralsListUI::isVisible()
{
	return panel_generalsList->isVisible();
}

void GeneralsListUI::setVisible( bool visible )
{
	panel_generalsList->setVisible(visible);
	Layer_generalsList->setVisible(visible);
	Layer_generalsTop->setVisible(visible);
}

void GeneralsListUI::RefreshGeneralsFate( CGeneralDetail * generalDetail )
{
	l_fate_1->setText("");
	l_fate_2->setText("");
	l_fate_3->setText("");
	l_fate_4->setText("");
	l_fate_5->setText("");
	l_fate_6->setText("");

	btn_fate_1->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_2->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_3->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_4->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_5->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_6->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");

	btn_fate_1->setVisible(true);
	btn_fate_2->setVisible(true);
	btn_fate_3->setVisible(true);
	btn_fate_4->setVisible(true);
	btn_fate_5->setVisible(true);
	btn_fate_6->setVisible(true);

	if (generalDetail->fates_size()>0)
	{
		for (int i = 0;i<generalDetail->fates_size();++i)
		{
			if (i == 0)
			{
				l_fate_1->setText(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_1->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 1)
			{
				l_fate_2->setText(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_2->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 2)
			{
				l_fate_3->setText(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_3->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 3)
			{
				l_fate_4->setText(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_4->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 4)
			{
				l_fate_5->setText(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_5->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 5)
			{
				l_fate_6->setText(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_6->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
		}
// 		for (int i = generalDetail->fates_size();i<6;++i)
// 		{
// 			switch(i)
// 			{
// 			case 0:
// 				btn_fate_1->setVisible(false);
// 				break;
// 			case 1:
// 				btn_fate_2->setVisible(false);
// 				break;
// 			case 2:
// 				btn_fate_3->setVisible(false);
// 				break;
// 			case 3:
// 				btn_fate_4->setVisible(false);
// 				break;
// 			case 4:
// 				btn_fate_5->setVisible(false);
// 				break;
// 			case 5:
// 				btn_fate_6->setVisible(false);
// 				break;
// 			}
// 		}
	}
// 	else
// 	{
// 		btn_fate_1->setVisible(false);
// 		btn_fate_2->setVisible(false);
// 		btn_fate_3->setVisible(false);
// 		btn_fate_4->setVisible(false);
// 		btn_fate_5->setVisible(false);
// 		btn_fate_6->setVisible(false);
// 	}
}

void GeneralsListUI::FateEvent( CCObject * pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsFateInfoUI) == NULL)
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		GeneralsFateInfoUI * generalsFateInfoUI = GeneralsFateInfoUI::create(curGeneralDetail);
		generalsFateInfoUI->ignoreAnchorPointForPosition(false);
		generalsFateInfoUI->setAnchorPoint(ccp(0.5f,0.5f));
		generalsFateInfoUI->setPosition(ccp(winsize.width/2,winsize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(generalsFateInfoUI,0,kTagGeneralsFateInfoUI);
	}
}

void GeneralsListUI::ShowGeneralEvent( CCObject * pSender )
{
	// 展示/收回 武将
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5171,(void *)curGeneralBaseMsg->id(),(void *)1);

	//刷新武将列表
	GeneralsUI::generalsListUI->isReqNewly = true;
	GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
	temp->page = 0;
	temp->pageSize = 20;
	temp->type = 1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
	delete temp;

	Btn_show_general->setVisible(false);
	Btn_hide_general->setVisible(true);
}

void GeneralsListUI::HideGeneralEvent( CCObject * pSender )
{
	// 展示/收回 武将
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5171,(void *)curGeneralBaseMsg->id(),(void *)0);

	//刷新武将列表
	GeneralsUI::generalsListUI->isReqNewly = true;
	GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
	temp->page = 0;
	temp->pageSize = 20;
	temp->type = 1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
	delete temp;

	Btn_show_general->setVisible(true);
	Btn_hide_general->setVisible(false);
}

void GeneralsListUI::RefreshGeneralsSkill( CGeneralDetail * generalDetail )
{
	long long generalsid = -1;
	std::string musouid = "";
	if (Layer_generalsList->getChildByTag(SHORTCUTLAYERTAG) != NULL)
	{
		GeneralsShortcutLayer * temp = (GeneralsShortcutLayer *)Layer_generalsList->getChildByTag(SHORTCUTLAYERTAG);
		generalsid = temp->curGeneralDetail->generalid();
		musouid = temp->m_sCurMumouId;
		Layer_generalsList->getChildByTag(SHORTCUTLAYERTAG)->removeFromParent();
	}

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	generalsShortcutLayer = GeneralsShortcutLayer::create(generalDetail);
	generalsShortcutLayer->ignoreAnchorPointForPosition(false);
	generalsShortcutLayer->setAnchorPoint(ccp(0.5f,0.5f));
	generalsShortcutLayer->setPosition(ccp(winSize.width/2,winSize.height/2));
	Layer_generalsList->addChild(generalsShortcutLayer,0,SHORTCUTLAYERTAG);
	generalsShortcutLayer->m_nLastGeneralId = generalsid;
	generalsShortcutLayer->m_sLastMumouId = musouid;
	//generalsShortcutLayer->InitMusouSkill(generalDetail);
	

// 	for (int i = 0;i<5;++i)
// 	{
// 		if (Layer_generalsList->getChildByTag(ShortSlotBaseTag+i))
// 		{
// 			Layer_generalsList->getChildByTag(ShortSlotBaseTag+i)->removeFromParent();
// 		}
// 	}
// 
// 	if (generalDetail->shortcuts_size()>0)
// 	{
// 		for (int i = 0;i<generalDetail->shortcuts_size();++i)
// 		{
// 			CShortCut * shortcut = new CShortCut();
// 			shortcut->CopyFrom(generalDetail->shortcuts(i));
// 			GeneralsShortcutSlot * genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->currentquality(),shortcut);
// 			genentalsShortcutSlot->ignoreAnchorPointForPosition(false);
// 			genentalsShortcutSlot->setAnchorPoint(ccp(0.5f,0.5f));
// 			genentalsShortcutSlot->setPosition(slotPos[i]);
// 			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
// 			genentalsShortcutSlot->setSkillProfession(curSkillType);
// 			Layer_generalsList->addChild(genentalsShortcutSlot);
// 
// 			if (shortcut->complexflag() == 1)
// 			{
// 
// 			}
// 			delete shortcut;
// 		}
// 		for (int i = generalDetail->shortcuts_size();i<5;++i)
// 		{
// 			GeneralsShortcutSlot * genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->currentquality(),i);
// 			genentalsShortcutSlot->ignoreAnchorPointForPosition(false);
// 			genentalsShortcutSlot->setAnchorPoint(ccp(0.5f,0.5f));
// 			genentalsShortcutSlot->setPosition(slotPos[i]);
// 			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
// 			genentalsShortcutSlot->setSkillProfession(curSkillType);
// 			Layer_generalsList->addChild(genentalsShortcutSlot);
// 		}
// 	}
// 	else
// 	{
// 		for (int i = 0;i<5;++i)
// 		{
// 			GeneralsShortcutSlot * genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->currentquality(),i);
// 			genentalsShortcutSlot->ignoreAnchorPointForPosition(false);
// 			genentalsShortcutSlot->setAnchorPoint(ccp(0.5f,0.5f));
// 			genentalsShortcutSlot->setPosition(slotPos[i]);
// 			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
// 			genentalsShortcutSlot->setSkillProfession(curSkillType);
// 			Layer_generalsList->addChild(genentalsShortcutSlot);
// 		}
// 	}
}

void GeneralsListUI::RefreshOneGeneralsSkillSlot(long long generalsId,CShortCut * shortCut)
{
	this->generalsShortcutLayer->RefreshOneShortcutSlot(generalsId,shortCut);
}

void GeneralsListUI::RefreshAllGeneralsSkillSlot(int generalsId,std::vector<CShortCut*> shortcutList)
{
	this->generalsShortcutLayer->RefreshAllShortcutSlot(generalsId,shortcutList);
}

#define BASEFIGHTER_CHATBUBBLE_OFFSET_Y 124
void GeneralsListUI::addChatBubble( const char* text, float duration )
{
	CCNode* bubble = CCNode::create();
	bubble->setPosition(ccp(0,0));
	bubble->setAnchorPoint(ccp(0.5f,0.f));

	int MAX_WIDTH = 192;
	int MARGINS = 2;
	// first, create a label which its width is less than MAX_WIDTH
	CCLabelTTF* label = CCLabelTTF::create(text, APP_FONT_NAME, 24);
	ccColor3B shadowColor = ccc3(0,0,0);   // black
	label->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	if(label->getContentSize().width > MAX_WIDTH)
	{
		label->release();
		label = CCLabelTTF::create(text, APP_FONT_NAME, 24, CCSizeMake(192, 0), kCCTextAlignmentLeft);
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		label->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	}
	label->setAnchorPoint(ccp(0.5f, 0.f));
	label->setPosition( ccp(0, MARGINS) );
	bubble->addChild(label, 1);

	// then, according the above label to set background's size 
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();
	CCSprite* tmp = CCSprite::create("extensions/buttonHighlighted.png");
	CCSize size = tmp->getContentSize();
	CCRect fullRect = CCRectMake(0,0, size.width, size.height);
	CCRect insetRect = CCRectMake(5,4,size.width-5-5, size.height-6-4);
	tmp->release();
	CCScale9Sprite* backGround = CCScale9Sprite::create("extensions/buttonHighlighted.png", fullRect, insetRect ); 
	backGround->setPreferredSize(CCSizeMake(label->getContentSize().width + MARGINS*2, label->getContentSize().height + MARGINS*2));   //  CCSizeMake(256, 50)
	//backGround->setPosition(ccp(s.width/2, s.height/2));
	backGround->setAnchorPoint(ccp(0.5f, 0.f));
	backGround->setOpacity(180);
	bubble->addChild(backGround, 0);

	CCActionInterval *action = (CCActionInterval*)CCSequence::create
		(
		CCShow::create(),
		CCDelayTime::create(duration),
		CCRemoveSelf::create(),
		NULL
		);
	bubble->runAction(action);

	Layer_generalsList->addChild(bubble,0,BASEFIGHTER_CHATBUBBLE_OFFSET_Y);
}

void GeneralsListUI::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction ,bool firstTutorialOrSecond,bool firstMenuOrSecond)
{
	if(firstTutorialOrSecond)
	{
		isFirstTutorial = true;
		isSecondTutorial = false;
	}
	else
	{
		isFirstTutorial = false;
		isSecondTutorial = true;
	}

	isFirstOrSecondMenu = firstMenuOrSecond;

// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,40,35);
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	Layer_generalsTop->addChild(tutorialIndicator);
// 	if (firstTutorialOrSecond)   //第一行
// 	{
// 		if (firstMenuOrSecond)    //上阵
// 		{
// 			tutorialIndicator->setPosition(ccp(pos.x+160,pos.y+260));//(160,280)
// 		}
// 		else                                   //出战
// 		{
// 			tutorialIndicator->setPosition(ccp(pos.x+245,pos.y+260));//(250,280)
// 		}
// 	}
// 	else                                     //第二行
// 	{
// 		if (firstMenuOrSecond)    //上阵
// 		{
// 			tutorialIndicator->setPosition(ccp(pos.x+242,pos.y+110));
// 		}
// 		else                                   //出战
// 		{
// 			tutorialIndicator->setPosition(ccp(pos.x+242,pos.y+110));
// 		}
// 	}

	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,65,26,true,true,true);
	if (firstTutorialOrSecond)   //第一行
	{
		tutorialIndicator->setDrawNodePos(ccp(pos.x+197+_w,pos.y+270+_h));
	}
	else                                     //第二行
	{
		tutorialIndicator->setDrawNodePos(ccp(pos.x+197+_w,pos.y+195+_h));
	}
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralsListUI::addCCTutorialIndicator2( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction ,bool firstOrSecond )
{
// 	isFirstOrSecondTutorial = true;
// 	isFirstOrSecondMenu = firstOrSecond;
// 
// 	m_content = content;
// 	m_pos = pos ;
// 	m_direction = direction;
// 
// 	this->generalList_tableView->reloadData();
}

void GeneralsListUI::removeCCTutorialIndicator()
{
 	isFirstTutorial = false;
 	isSecondTutorial = false;
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(Layer_generalsTop->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void GeneralsListUI::registerScriptCommand( int scriptId )
{
	 mTutorialScriptInstanceId = scriptId;
}

void GeneralsListUI::RefreshEvolutionEnabled( CGeneralDetail * generalDetail )
{
	if (generalDetail->evolutenum() >= 2)
	{
		Btn_evolution->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
	}
	else
	{
		Btn_evolution->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	}
}




