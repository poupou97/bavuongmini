#include "GeneralsEvolutionUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GeneralsUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../GameView.h"
#include "TeachAndEvolutionCell.h"
#include "NormalGeneralsHeadItem.h"
#include "GeneralsListUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGeneralsEvolution.h"
#include "AppMacros.h"
#include "../../gamescene_state/GameSceneEffects.h"

#define EvolutionHeadTag 50
#define VictimHeadTag 60

#define Action_Progress_Speed 200
#define Action_Progress_Cur_Tag 70
#define Action_Progress_PreView_Tag 80

GeneralsEvolutionUI::GeneralsEvolutionUI():
curVictimGeneralId(-1),
curEvolutionGeneralId(-1),
curEvolutionNum(-1),
allEvolutionNum(-1),
lastVictimGeneralId(-1)
{
	curEvolutionGeneralBaseMsg = new CGeneralBaseMsg();
	curEvolutionGeneralDetail = new CGeneralDetail();
	oldEvolutionGeneralDetail = new CGeneralDetail();
}

GeneralsEvolutionUI::~GeneralsEvolutionUI()
{
	delete curEvolutionGeneralBaseMsg;
	delete curEvolutionGeneralDetail;
	delete oldEvolutionGeneralDetail;
}
GeneralsEvolutionUI* GeneralsEvolutionUI::create()
{
	GeneralsEvolutionUI * generalsEvolutionUI = new GeneralsEvolutionUI();
	if (generalsEvolutionUI && generalsEvolutionUI->init())
	{
		generalsEvolutionUI->autorelease();
		return generalsEvolutionUI;
	}
	CC_SAFE_DELETE(generalsEvolutionUI);
	return NULL;
}

bool GeneralsEvolutionUI::init()
{
	if (GeneralsListBase::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		//curGeneralBaseMsg = new CGeneralBaseMsg();
		//武将进化面板
		panel_evolution = (UIPanel*)UIHelper::seekWidgetByName(GeneralsUI::ppanel,"Panel_GeneralsUpgrade");
		panel_evolution->setVisible(true);
		//武将进化层
		Layer_evolution= UILayer::create();
// 		Layer_evolution->ignoreAnchorPointForPosition(false);
// 		Layer_evolution->setAnchorPoint(ccp(0.5f,0.5f));
// 		Layer_evolution->setContentSize(CCSizeMake(800, 480));
// 		Layer_evolution->setPosition(ccp(winsize.width/2,winsize.height/2));

		addChild(Layer_evolution);

		UIButton * btn_back = (UIButton*)UIHelper::seekWidgetByName(panel_evolution,"Button_back");
		btn_back->setTouchEnable(true);
		btn_back->setPressedActionEnabled(true);
		btn_back->addReleaseEvent(this,coco_releaseselector(GeneralsEvolutionUI::BackEvent));
		UIButton * btn_reset = (UIButton*)UIHelper::seekWidgetByName(panel_evolution,"Button_reset");
		btn_reset->setTouchEnable(true);
		btn_reset->setPressedActionEnabled(true);
		btn_reset->addReleaseEvent(this,coco_releaseselector(GeneralsEvolutionUI::ResetEvent));
		UIButton * btn_evolution = (UIButton*)UIHelper::seekWidgetByName(panel_evolution,"Button_evolution");
		btn_evolution->setTouchEnable(true);
		btn_evolution->setPressedActionEnabled(true);
		btn_evolution->addReleaseEvent(this,coco_releaseselector(GeneralsEvolutionUI::EvolutionEvent));

		ImageView_MainGeneralFrame = (UIImageView*)UIHelper::seekWidgetByName(panel_evolution,"ImageView_MainGeneralFrame");

		Label_CapsExp_value = (UILabel *)UIHelper::seekWidgetByName(panel_evolution,"Label_CapsExp_value");
        Label_CapsExp_value->setStrokeEnabled(true);
		Label_CapsExp_value->setVisible(false);
		ImageView_exp_cur = (UIImageView *)UIHelper::seekWidgetByName(panel_evolution,"ImageView_CurExp");
		ImageView_exp_cur->setVisible(false);
		ImageView_exp_preView = (UIImageView *)UIHelper::seekWidgetByName(panel_evolution,"ImageView_MaxExp");
		ImageView_exp_preView->setVisible(false);

		sp_exp_preView = CCProgressTimer::create(CCSprite::create("res_ui/wujiang/jindu2.png"));
		sp_exp_preView->setType(kCCProgressTimerTypeBar);
		sp_exp_preView->setMidpoint(ccp(0,0));
		sp_exp_preView->setBarChangeRate(ccp(1, 0));
		Layer_evolution->addChild(sp_exp_preView);
		sp_exp_preView->setAnchorPoint(ccp(0,0));
		sp_exp_preView->setPosition(ccp(405, 229));   //427,238
		
		//CCFadeTo * moveToAction = CCFadeTo::create(0.5f,140);
		//CCRepeatForever * repeapAction = CCRepeatForever::create(CCSequence::create(moveToAction,moveToAction->reverse(),NULL));
		//sp_exp_preView->runAction(repeapAction);

		CCFadeTo*  action1 = CCFadeTo::create(.5f,140);
		CCFadeTo*  action2 = CCFadeTo::create(.5f,255);
		CCRepeatForever * repeapAction = CCRepeatForever::create(CCSequence::create(action1,action2,NULL));
		sp_exp_preView->runAction(repeapAction);

// 		CCFadeIn * action1 = CCFadeIn::create(0.5f);
// 		CCFadeOut* action2 = CCFadeOut::create(0.5f);
// 		CCRepeatForever* repeatAction = CCRepeatForever::create(CCSequence::create(action1,action2,NULL));
// 		sp_exp_preView->runAction(repeatAction);

		sp_exp_cur = CCProgressTimer::create(CCSprite::create("res_ui/wujiang/jindu_big.png"));
		sp_exp_cur->setType(kCCProgressTimerTypeBar);
		sp_exp_cur->setMidpoint(ccp(0,0));
		sp_exp_cur->setBarChangeRate(ccp(1, 0));
		Layer_evolution->addChild(sp_exp_cur);
		sp_exp_cur->setAnchorPoint(ccp(0,0));
		sp_exp_cur->setPosition(ccp(405, 229));

		l_CapsExp_value = CCLabelTTF::create("ABC",APP_FONT_NAME,14);
		l_CapsExp_value->setAnchorPoint(ccp(0.5f,0.5f));
		l_CapsExp_value->setPosition(ccp(548,236));
		Layer_evolution->addChild(l_CapsExp_value);

		Label_curStrenth = (UILabel *)UIHelper::seekWidgetByName(panel_evolution,"Label_CurPower");
		Label_addStrenth = (UILabel *)UIHelper::seekWidgetByName(panel_evolution,"Label_MaxPower");
		Label_curDexterity = (UILabel *)UIHelper::seekWidgetByName(panel_evolution,"Label_CurAglie");
		Label_addDexterity = (UILabel *)UIHelper::seekWidgetByName(panel_evolution,"Label_MaxAglie");
		Label_curIntelligence = (UILabel *)UIHelper::seekWidgetByName(panel_evolution,"Label_CurIntelligence");
		Label_addIntelligence = (UILabel *)UIHelper::seekWidgetByName(panel_evolution,"Label_MaxIntelligence");
		Label_curFocus = (UILabel *)UIHelper::seekWidgetByName(panel_evolution,"Label_CurFocus");
		Label_addFocus = (UILabel *)UIHelper::seekWidgetByName(panel_evolution,"Label_MaxFocus");
		L_constValue = (UILabel *)UIHelper::seekWidgetByName(panel_evolution,"Label_cost_value");
		L_constValue->setText("");

		this->generalsListStatus = HaveNext;
		//武将列表
		generalList_tableView = CCTableView::create(this,CCSizeMake(261,397));
		//generalList_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
		//generalList_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 26, 1, 1), ccp(0,5)); 
		//generalList_tableView->setPressedActionEnabled(true);
		generalList_tableView->setDirection(kCCScrollViewDirectionVertical);
		generalList_tableView->setAnchorPoint(ccp(0,0));
		generalList_tableView->setPosition(ccp(75,42));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		Layer_evolution->addChild(generalList_tableView);

		Layer_upper = UILayer::create();
		// 		Layer_upper->ignoreAnchorPointForPosition(false);
		// 		Layer_upper->setAnchorPoint(ccp(0.5f,0.5f));
		// 		Layer_upper->setContentSize(CCSizeMake(800, 480));
		// 		Layer_upper->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(Layer_upper);

// 		UIImageView * tableView_kuang = UIImageView::create();
// 		tableView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		tableView_kuang->setScale9Enable(true);
// 		tableView_kuang->setScale9Size(CCSizeMake(261,418));
// 		tableView_kuang->setCapInsets(CCRectMake(30,30,1,1));
// 		tableView_kuang->setAnchorPoint(ccp(0,0));
// 		tableView_kuang->setPosition(ccp(62,29));
// 		Layer_upper->addWidget(tableView_kuang);

		//"主"标识
		UIImageView * imageView_main = UIImageView::create();
		imageView_main->setTexture("res_ui/wujiang/zhu.png");
		imageView_main->setAnchorPoint(ccp(0,0));
		imageView_main->setPosition(ccp(401,388));//(359,274)
		Layer_upper->addWidget(imageView_main);
		//"辅"标识
		UIImageView * imageView_other = UIImageView::create();
		imageView_other->setTexture("res_ui/wujiang/fu.png");
		imageView_other->setAnchorPoint(ccp(0,0));
		imageView_other->setPosition(ccp(573,362));
		Layer_upper->addWidget(imageView_other);
		

		return true;
	}
	return false;
}

void GeneralsEvolutionUI::onEnter()
{
	GeneralsListBase::onEnter();
}

void GeneralsEvolutionUI::onExit()
{
	GeneralsListBase::onExit();
	std::vector<CGeneralBaseMsg*>::iterator iter_evolutionGeneralsList;
	for (iter_evolutionGeneralsList = evolutionGeneralsList.begin(); iter_evolutionGeneralsList != evolutionGeneralsList.end(); ++iter_evolutionGeneralsList)
	{
		delete *iter_evolutionGeneralsList;
	}
	evolutionGeneralsList.clear();
}

void GeneralsEvolutionUI::scrollViewDidScroll( CCScrollView* view )
{
}

void GeneralsEvolutionUI::scrollViewDidZoom( CCScrollView* view )
{
}

void GeneralsEvolutionUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	int i = cell->getIdx();

	TeachAndEvolutionCell * tempCell = dynamic_cast<TeachAndEvolutionCell*>(cell);
	if (!tempCell)
		return;

	if (i<0)
		return;

	if(curEvolutionGeneralId != curVictimGeneralId)
	{
		//消除上一个的选中状态
		if (lastVictimGeneralId != -1)
		{
			for(int i = 0;i<evolutionGeneralsList.size();++i)
			{
				if (lastVictimGeneralId == evolutionGeneralsList.at(i)->id())
				{
					TeachAndEvolutionCell * tempCell = dynamic_cast<TeachAndEvolutionCell*>(table->cellAtIndex(i));
					if (tempCell)
						tempCell->setGray(false);
				}
			}
		}

		if (curEvolutionGeneralBaseMsg->evolution() >= 20)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_evolution_maxLevel"));
		}
		else
		{
			//准备做牺牲品
			curVictimGeneralId = evolutionGeneralsList.at(i)->id();
			curGeneralBaseMsg = evolutionGeneralsList.at(i);
			createVictimHead();
			tempCell->setGray(true);
			//set last
			lastVictimGeneralId = curVictimGeneralId;
			//RefreshProgressPreView
			RefreshGeneralEvolutionProgressPreView(curEvolutionGeneralBaseMsg);
		}
	}

// 	int i = cell->getIdx();
// 	if (generalsListStatus == HaveNone)
// 	{
// 		//准备做牺牲品
// 		curVictimGeneralId = evolutionGeneralsList.at(i)->id();
// 		createVictimHead();
// 	}
// 	else if (generalsListStatus == HaveLast)
// 	{
// 		if (i == 0)
// 		{
// 			//req for last
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage-1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 3;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//准备做牺牲品
// 			curVictimGeneralId = evolutionGeneralsList.at(i-1)->id();
// 			createVictimHead();
// 		}
// 	}
// 	else if (generalsListStatus == HaveNext)
// 	{
// 		if (i == evolutionGeneralsList.size())
// 		{
// 			//req for next
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage+1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 3;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//准备做牺牲品
// 			curVictimGeneralId = evolutionGeneralsList.at(i)->id();
// 			createVictimHead();
// 		}
// 	}
// 	else if (generalsListStatus == HaveBoth)
// 	{
// 		if (i == 0)
// 		{
// 			//req for lase
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage-1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 3;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else if (i == evolutionGeneralsList.size()+1)
// 		{
// 			//req for next
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage+1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 3;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//准备做牺牲品
// 			curVictimGeneralId = evolutionGeneralsList.at(i-1)->id();
// 			createVictimHead();
// 		}
// 	}

}

cocos2d::CCSize GeneralsEvolutionUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(242,72);
}

cocos2d::extension::CCTableViewCell* GeneralsEvolutionUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	TeachAndEvolutionCell * tempCell = dynamic_cast<TeachAndEvolutionCell*>(cell);
	if (tempCell)
	{
		tempCell->refreshCell(evolutionGeneralsList.at(idx));
	}
	else
	{
		cell = TeachAndEvolutionCell::create(evolutionGeneralsList.at(idx)); 
	}
	
	if(idx == evolutionGeneralsList.size()-4)
	{
		if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
		{
			if (getIsCanReq())
			{
				//req for next
				this->isReqNewly = false;
				GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
				temp->page = this->s_mCurPage+1;
				temp->pageSize = this->everyPageNum;
				temp->type = 3;
				temp->generalId = curEvolutionGeneralId;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
				setIsCanReq(false);
				delete temp;
			}
		}
	}

	if(curEvolutionGeneralId != curVictimGeneralId)
	{
		if (curVictimGeneralId == evolutionGeneralsList.at(idx)->id())
		{
			TeachAndEvolutionCell * tempCell = dynamic_cast<TeachAndEvolutionCell*>(cell);
			if (tempCell)
				tempCell->setGray(true);
		}
	}

	return cell;
}

unsigned int GeneralsEvolutionUI::numberOfCellsInTableView( CCTableView *table )
{
	 return evolutionGeneralsList.size();
// 	if (generalsListStatus == HaveNone)
// 	{
// 		return evolutionGeneralsList.size();
// 	}
// 	else if (generalsListStatus == HaveLast ||generalsListStatus == HaveNext)
// 	{
// 		return evolutionGeneralsList.size()+1;
// 	}
// 	else if (generalsListStatus == HaveBoth)
// 	{
// 		return evolutionGeneralsList.size()+2;
// 	}
// 	else
// 	{
// 		return 0;
// 	}
}

void GeneralsEvolutionUI::RefreshGeneralsList()
{
	this->generalList_tableView->reloadData();
}

void GeneralsEvolutionUI::RefreshGeneralsListWithOutChangeOffSet()
{
	CCPoint _s = generalList_tableView->getContentOffset();
	int _h = generalList_tableView->getContentSize().height + _s.y;
	generalList_tableView->reloadData();
	CCPoint temp = ccp(_s.x,_h - generalList_tableView->getContentSize().height);
	generalList_tableView->setContentOffset(temp); 
}

bool GeneralsEvolutionUI::isVisible()
{
	return panel_evolution->isVisible();
}

void GeneralsEvolutionUI::setVisible( bool visible )
{
	panel_evolution->setVisible(visible);
	Layer_evolution->setVisible(visible);
	Layer_upper->setVisible(visible);
}

void GeneralsEvolutionUI::BackEvent( CCObject * pSender )
{
	this->setVisible(false);
	GeneralsUI::generalsListUI->setVisible(true);

	curEvolutionGeneralDetail->set_evolution(curEvolutionGeneralBaseMsg->evolution());
	curEvolutionGeneralDetail->set_evolutionexp(curEvolutionGeneralBaseMsg->evolutionexp());

	if (this->curEvolutionGeneralDetail->has_generalid())
	{
		if (GeneralsUI::generalsListUI->curGeneralBaseMsg->has_id())
		{
			if (GeneralsUI::generalsListUI->curGeneralBaseMsg->id() == this->curEvolutionGeneralDetail->generalid())
			{
				GeneralsUI::generalsListUI->RefreshGeneralsInfo(curEvolutionGeneralDetail);
				//刷新武将列表
				GeneralsUI::generalsListUI->isReqNewly = true;
				GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
				temp->page = 0;
				temp->pageSize = 20;
				temp->type = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
				delete temp;
			}
		}
	}
}

void GeneralsEvolutionUI::ResetEvent( CCObject * pSender )
{
	if (Layer_evolution->getChildByTag(VictimHeadTag) != NULL)
	{
		Layer_evolution->getChildByTag(VictimHeadTag)->removeFromParent();
		curVictimGeneralId = -1;

		generalList_tableView->reloadData();
		this->RefreshGeneralEvolutionProgressToDefault(curEvolutionGeneralBaseMsg);
	}
}

void GeneralsEvolutionUI::EvolutionEvent( CCObject * pSender )
{
	if (curVictimGeneralId < 0)
	{
		const char *str1  = StringDataManager::getString("generals_pleaseaddgenerals");
		GameView::getInstance()->showAlertDialog(str1);
	}
	else
	{
		if (curGeneralBaseMsg->evolution() > curEvolutionGeneralBaseMsg->evolution())
		{
			GameView::getInstance()->showPopupWindow(StringDataManager::getString("generals_evolution_highterEvolution"),2,this,coco_selectselector(GeneralsEvolutionUI::SureToEvolution),NULL);
		}
		else
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5057,(void*)curEvolutionGeneralId,(void*)curVictimGeneralId);
		}
	}
}

void GeneralsEvolutionUI::SureToEvolution( CCObject * pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5057,(void*)curEvolutionGeneralId,(void*)curVictimGeneralId);
}

void GeneralsEvolutionUI::createVictimHead()
{
	if (Layer_evolution->getChildByTag(VictimHeadTag) == NULL)
	{
		NormalGeneralsHeadItemBase * normalGeneralsHeadItem = NormalGeneralsHeadItemBase::create(curGeneralBaseMsg);
		normalGeneralsHeadItem->ignoreAnchorPointForPosition(false);
		normalGeneralsHeadItem->setAnchorPoint(ccp(0,0));
		normalGeneralsHeadItem->setPosition(ccp(590,263));
		normalGeneralsHeadItem->setScale(.85f);
		Layer_evolution->addChild(normalGeneralsHeadItem,0,VictimHeadTag);
	}
}

void GeneralsEvolutionUI::createEvolutionHead( CGeneralBaseMsg * generalsBaseMsg )
{
	if (Layer_evolution->getChildByTag(EvolutionHeadTag) != NULL)
	{
		Layer_evolution->getChildByTag(EvolutionHeadTag)->removeFromParent();
	}
	//重置准备牺牲的武将
	if (Layer_evolution->getChildByTag(VictimHeadTag) != NULL)
	{
		Layer_evolution->getChildByTag(VictimHeadTag)->removeFromParent();
		curVictimGeneralId = -1;
	}

	curEvolutionGeneralId = generalsBaseMsg->id();
	curEvolutionGeneralBaseMsg->CopyFrom(*generalsBaseMsg);
	NormalGeneralsHeadItemBase * normalGeneralsHeadItem = NormalGeneralsHeadItemBase::create(generalsBaseMsg);
	normalGeneralsHeadItem->ignoreAnchorPointForPosition(false);
	normalGeneralsHeadItem->setAnchorPoint(ccp(0,0));
	normalGeneralsHeadItem->setPosition(ccp(415,269));
	Layer_evolution->addChild(normalGeneralsHeadItem,0,EvolutionHeadTag);

	refreshEvolutionHeadFrame(generalsBaseMsg);

	this->RefreshGeneralEvolutionProgressToDefault(curEvolutionGeneralBaseMsg);	
}

void GeneralsEvolutionUI::refreshEvolutionHead( CGeneralBaseMsg * generalsBaseMsg,bool isUpgraded )
{
	if (Layer_evolution->getChildByTag(EvolutionHeadTag) != NULL)
	{
		Layer_evolution->getChildByTag(EvolutionHeadTag)->removeFromParent();
	}
	//重置准备牺牲的武将
	if (Layer_evolution->getChildByTag(VictimHeadTag) != NULL)
	{
		Layer_evolution->getChildByTag(VictimHeadTag)->removeFromParent();
		curVictimGeneralId = -1;
	}

	curEvolutionGeneralId = generalsBaseMsg->id();
	curEvolutionGeneralBaseMsg->CopyFrom(*generalsBaseMsg);
	NormalGeneralsHeadItemBase * normalGeneralsHeadItem = NormalGeneralsHeadItemBase::create(generalsBaseMsg);
	normalGeneralsHeadItem->ignoreAnchorPointForPosition(false);
	normalGeneralsHeadItem->setAnchorPoint(ccp(0,0));
	normalGeneralsHeadItem->setPosition(ccp(415,269));
	Layer_evolution->addChild(normalGeneralsHeadItem,0,EvolutionHeadTag);
	if (isUpgraded)
	{
		normalGeneralsHeadItem->playAnmOfEvolution();
	}

	refreshEvolutionHeadFrame(generalsBaseMsg);

	this->RefreshGeneralEvolutionProgress(curEvolutionGeneralBaseMsg);	
}

void GeneralsEvolutionUI::RefreshGeneralEvolutionInfo( CGeneralDetail *generalsDetail )
{
	oldEvolutionGeneralDetail->CopyFrom(*curEvolutionGeneralDetail);
	//
	curEvolutionGeneralId = generalsDetail->generalid();
	curEvolutionGeneralDetail->CopyFrom(*generalsDetail);
	
	CGeneralBaseMsg * generalsBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalsDetail->modelid()];
	bool isHeightLevel = false;
	if (generalsDetail->evolution() == 20)
	{
		isHeightLevel = true;
	}

	//当前信息
	GeneralEvolutionConfigData::ProAndRare temp;
	temp.profession = generalsBaseMsgFromDb->get_profession();
	temp.rare = generalsDetail->rare();
	if(generalsDetail->evolution() == 0)
	{
		temp.evolution = 1;
	}
	else 
	{
		temp.evolution = generalsDetail->evolution();
	}
	CGeneralsEvolution * curGeneralsEvolution = GeneralEvolutionConfigData::s_generalEvolution[temp];

	curEvolutionNum = generalsDetail->evolutionexp();

	CGeneralsEvolution * nextGeneralsEvolution;
	if (isHeightLevel)
	{
		//下一级信息
		GeneralEvolutionConfigData::ProAndRare  temp;
		temp.profession = generalsBaseMsgFromDb->get_profession();
		temp.rare = generalsDetail->rare();
		temp.evolution = 20;

		nextGeneralsEvolution = GeneralEvolutionConfigData::s_generalEvolution[temp];
	}
	else
	{
		//下一级信息
		GeneralEvolutionConfigData::ProAndRare  temp;
		temp.profession = generalsBaseMsgFromDb->get_profession();
		temp.rare = generalsDetail->rare();
		if(generalsDetail->evolution() == 0)
		{
			temp.evolution = 1;
		}
		else if (generalsDetail->evolution() == 20)
		{
			temp.evolution = 20;
		}
		else 
		{
			temp.evolution = generalsDetail->evolution()+1;
		}

		nextGeneralsEvolution = GeneralEvolutionConfigData::s_generalEvolution[temp];
	}
	allEvolutionNum = nextGeneralsEvolution->get_need_card();

	//花费金币
	char str_gold [20];
	sprintf(str_gold,"%d",nextGeneralsEvolution->get_cost());
	L_constValue->setText(str_gold);

// 	//设置进度显示（例：5/10）
// 	std::string evolutionProgress = "";
// 	char s_cur[10];
// 	sprintf(s_cur,"%d",curEvolutionNum);
// 	evolutionProgress.append(s_cur);
// 	evolutionProgress.append(" / ");
// 	char s_all[10];
// 	sprintf(s_all,"%d",allEvolutionNum);
// 	evolutionProgress.append(s_all);
// 	Label_CapsExp_value->setText(evolutionProgress.c_str());
// 	//设置进度条长度
// 	float a = curEvolutionNum*1.0f;
// 	float b = allEvolutionNum*1.0f;
// 	float sl = a/b*1.1f;
// 	ImageView_CurExp->setScaleX(sl);

	char s_strenth[20];
	sprintf(s_strenth,"%d",generalsDetail->aptitude().strength());
	Label_curStrenth->setText(s_strenth);
	char s_dexterity[20];
	sprintf(s_dexterity,"%d",generalsDetail->aptitude().dexterity());
	Label_curDexterity->setText(s_dexterity);
	char s_intelligence[20];
	sprintf(s_intelligence,"%d",generalsDetail->aptitude().intelligence());
	Label_curIntelligence->setText(s_intelligence);
	char s_focus[20];
	sprintf(s_focus,"%d",generalsDetail->aptitude().focus());
	Label_curFocus->setText(s_focus);

	if (isHeightLevel)
	{
		Label_addStrenth->setText("");
		Label_addDexterity->setText("");
		Label_addIntelligence->setText("");
		Label_addFocus->setText("");
	}
	else
	{
		if (curGeneralsEvolution->get_evolution() == nextGeneralsEvolution->get_evolution() == 1)
		{
			char s_strenth_1[20];
			sprintf(s_strenth_1,"%d",generalsDetail->aptitude().strength()+nextGeneralsEvolution->get_strenth());
			Label_addStrenth->setText(s_strenth_1);
			char s_dexterity_1[20];
			sprintf(s_dexterity_1,"%d",generalsDetail->aptitude().dexterity()+nextGeneralsEvolution->get_dexterity());
			Label_addDexterity->setText(s_dexterity_1);
			char s_intelligence_1[20];
			sprintf(s_intelligence_1,"%d",generalsDetail->aptitude().intelligence()+nextGeneralsEvolution->get_intelligence());
			Label_addIntelligence->setText(s_intelligence_1);
			char s_focus_1[20];
			sprintf(s_focus_1,"%d",generalsDetail->aptitude().focus()+nextGeneralsEvolution->get_focus());
			Label_addFocus->setText(s_focus_1);
		}
		else
		{
			char s_strenth_1[20];
			sprintf(s_strenth_1,"%d",generalsDetail->aptitude().strength()+(nextGeneralsEvolution->get_strenth()-curGeneralsEvolution->get_strenth()));
			Label_addStrenth->setText(s_strenth_1);
			char s_dexterity_1[20];
			sprintf(s_dexterity_1,"%d",generalsDetail->aptitude().dexterity()+(nextGeneralsEvolution->get_dexterity()-curGeneralsEvolution->get_dexterity()));
			Label_addDexterity->setText(s_dexterity_1);
			char s_intelligence_1[20];
			sprintf(s_intelligence_1,"%d",generalsDetail->aptitude().intelligence()+(nextGeneralsEvolution->get_intelligence()-curGeneralsEvolution->get_intelligence()));
			Label_addIntelligence->setText(s_intelligence_1);
			char s_focus_1[20];
			sprintf(s_focus_1,"%d",generalsDetail->aptitude().focus()+(nextGeneralsEvolution->get_focus()-curGeneralsEvolution->get_focus()));
			Label_addFocus->setText(s_focus_1);
		}
	}
}

void GeneralsEvolutionUI::RefreshGeneralEvolutionProgress( CGeneralBaseMsg * generalsBaseMsg )
{
	curEvolutionGeneralId = generalsBaseMsg->id();

	CGeneralBaseMsg * generalsBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalsBaseMsg->modelid()];
	bool isHeightLevel = false;
	if (generalsBaseMsg->evolution() == 20)
	{
		isHeightLevel = true;
	}

	//当前信息
	GeneralEvolutionConfigData::ProAndRare temp;
	temp.profession = generalsBaseMsgFromDb->get_profession();
	temp.rare = generalsBaseMsg->rare();
	if(generalsBaseMsg->evolution() == 0)
	{
		temp.evolution = 1;
	}
	else 
	{
		temp.evolution = generalsBaseMsg->evolution();
	}
	CGeneralsEvolution * curGeneralsEvolution = GeneralEvolutionConfigData::s_generalEvolution[temp];

	curEvolutionNum = generalsBaseMsg->evolutionexp();

	CGeneralsEvolution * nextGeneralsEvolution;
	if (isHeightLevel)
	{
		//下一级信息
		GeneralEvolutionConfigData::ProAndRare  temp;
		temp.profession = generalsBaseMsgFromDb->get_profession();
		temp.rare = generalsBaseMsg->rare();
		temp.evolution = 20;

		nextGeneralsEvolution = GeneralEvolutionConfigData::s_generalEvolution[temp];
	}
	else
	{
		//下一级信息
		GeneralEvolutionConfigData::ProAndRare  temp;
		temp.profession = generalsBaseMsgFromDb->get_profession();
		temp.rare = generalsBaseMsg->rare();
		if(generalsBaseMsg->evolution() == 0)
		{
			temp.evolution = 1;
		}
		else if (generalsBaseMsg->evolution() == 20)
		{
			temp.evolution = 20;
		}
		else 
		{
			temp.evolution = generalsBaseMsg->evolution()+1;
		}

		nextGeneralsEvolution = GeneralEvolutionConfigData::s_generalEvolution[temp];
	}
	allEvolutionNum = nextGeneralsEvolution->get_need_card();

	//设置进度显示（例：5/10）
	std::string evolutionProgress = "";
	char s_cur[10];
	sprintf(s_cur,"%d",curEvolutionNum);
	evolutionProgress.append(s_cur);
	evolutionProgress.append(" / ");
	char s_all[10];
	sprintf(s_all,"%d",allEvolutionNum);
	evolutionProgress.append(s_all);
	l_CapsExp_value->setString(evolutionProgress.c_str());
	//设置进度条长度
	float a = curEvolutionNum*1.0f;
	float b = allEvolutionNum*1.0f;
	float sl_old = (a-1)/b;
	float sl = a/b;
	//ImageView_exp_preView->setScaleX(sl);
	//ImageView_exp_cur->setScaleX(sl);
	// 

	if (sp_exp_cur->getActionByTag(Action_Progress_Cur_Tag))
	{
		sp_exp_cur->stopAllActions();
		sp_exp_cur->setPercentage(sl_old*100);
	}

	if (sl == 0.f)
	{
		CCProgressTo *to1 = CCProgressTo::create((100-sp_exp_cur->getPercentage())*1.0f/Action_Progress_Speed, 100);
		CCSequence * sq = CCSequence::create(to1,CCCallFunc::create(this,callfunc_selector(GeneralsEvolutionUI::ProgressToFinishCall)),NULL);
		sq->setTag(Action_Progress_Cur_Tag);
		sp_exp_cur->runAction(sq);
	}
	else
	{
		CCProgressTo *to1 = CCProgressTo::create((100-sp_exp_cur->getPercentage())*1.0f/Action_Progress_Speed, sl*100);
		CCSequence * sq = CCSequence::create(to1,NULL);
		sq->setTag(Action_Progress_Cur_Tag);
		sp_exp_cur->runAction(sq);
	}

}

void GeneralsEvolutionUI::RefreshGeneralEvolutionProgressPreView( CGeneralBaseMsg * generalsBaseMsg )
{
	curEvolutionGeneralId = generalsBaseMsg->id();

	CGeneralBaseMsg * generalsBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalsBaseMsg->modelid()];
	bool isHeightLevel = false;
	if (generalsBaseMsg->evolution() == 20)
	{
		isHeightLevel = true;
	}

	//当前信息
	GeneralEvolutionConfigData::ProAndRare temp;
	temp.profession = generalsBaseMsgFromDb->get_profession();
	temp.rare = generalsBaseMsg->rare();
	if(generalsBaseMsg->evolution() == 0)
	{
		temp.evolution = 1;
	}
	else 
	{
		temp.evolution = generalsBaseMsg->evolution();
	}
	CGeneralsEvolution * curGeneralsEvolution = GeneralEvolutionConfigData::s_generalEvolution[temp];

	curEvolutionNum = generalsBaseMsg->evolutionexp();

	CGeneralsEvolution * nextGeneralsEvolution;
	if (isHeightLevel)
	{
		//下一级信息
		GeneralEvolutionConfigData::ProAndRare  temp;
		temp.profession = generalsBaseMsgFromDb->get_profession();
		temp.rare = generalsBaseMsg->rare();
		temp.evolution = 20;

		nextGeneralsEvolution = GeneralEvolutionConfigData::s_generalEvolution[temp];
	}
	else
	{
		//下一级信息
		GeneralEvolutionConfigData::ProAndRare  temp;
		temp.profession = generalsBaseMsgFromDb->get_profession();
		temp.rare = generalsBaseMsg->rare();
		if(generalsBaseMsg->evolution() == 0)
		{
			temp.evolution = 1;
		}
		else if (generalsBaseMsg->evolution() == 20)
		{
			temp.evolution = 20;
		}
		else 
		{
			temp.evolution = generalsBaseMsg->evolution()+1;
		}

		nextGeneralsEvolution = GeneralEvolutionConfigData::s_generalEvolution[temp];
	}
	allEvolutionNum = nextGeneralsEvolution->get_need_card();

	//设置进度显示（例：5/10）
	std::string evolutionProgress = "";
	char s_cur[10];
	if (curEvolutionNum+1 <= allEvolutionNum )
	{
		sprintf(s_cur,"%d",curEvolutionNum+1);
	}
	else
	{
		sprintf(s_cur,"%d",curEvolutionNum);
	}
	evolutionProgress.append(s_cur);
	evolutionProgress.append(" / ");
	char s_all[10];
	sprintf(s_all,"%d",allEvolutionNum);
	evolutionProgress.append(s_all);
	l_CapsExp_value->setString(evolutionProgress.c_str());
	//设置进度条长度
	float a = curEvolutionNum*1.0f;
	float b = allEvolutionNum*1.0f;
	float sl_old = a/b;
	if (curEvolutionNum+1 <= allEvolutionNum )
	{
		a = (curEvolutionNum+1)*1.0f;
	}

	float sl = a/b;
	//ImageView_exp_preView->setScaleX(sl);
	/*
	if (sp_exp_preView->getActionByTag(Action_Progress_PreView_Tag))
	{
		sp_exp_preView->stopAllActions();
	}

	sp_exp_preView->setPercentage(sl_old*100);
	CCProgressTo *to1 = CCProgressTo::create((100-sp_exp_preView->getPercentage())*1.0f/Action_Progress_Speed, sl*100);
	CCSequence * sq = CCSequence::create(to1,NULL);
	sq->setTag(Action_Progress_PreView_Tag);
	sp_exp_preView->runAction(sq);*/
	sp_exp_preView->setPercentage(sl*100);
}

void GeneralsEvolutionUI::RefreshGeneralEvolutionProgressToDefault( CGeneralBaseMsg * generalsBaseMsg )
{
	curEvolutionGeneralId = generalsBaseMsg->id();

	CGeneralBaseMsg * generalsBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalsBaseMsg->modelid()];
	bool isHeightLevel = false;
	if (generalsBaseMsg->evolution() == 20)
	{
		isHeightLevel = true;
	}

	//当前信息
	GeneralEvolutionConfigData::ProAndRare temp;
	temp.profession = generalsBaseMsgFromDb->get_profession();
	temp.rare = generalsBaseMsg->rare();
	if(generalsBaseMsg->evolution() == 0)
	{
		temp.evolution = 1;
	}
	else 
	{
		temp.evolution = generalsBaseMsg->evolution();
	}
	CGeneralsEvolution * curGeneralsEvolution = GeneralEvolutionConfigData::s_generalEvolution[temp];

	curEvolutionNum = generalsBaseMsg->evolutionexp();

	CGeneralsEvolution * nextGeneralsEvolution;
	if (isHeightLevel)
	{
		//下一级信息
		GeneralEvolutionConfigData::ProAndRare  temp;
		temp.profession = generalsBaseMsgFromDb->get_profession();
		temp.rare = generalsBaseMsg->rare();
		temp.evolution = 20;

		nextGeneralsEvolution = GeneralEvolutionConfigData::s_generalEvolution[temp];
	}
	else
	{
		//下一级信息
		GeneralEvolutionConfigData::ProAndRare  temp;
		temp.profession = generalsBaseMsgFromDb->get_profession();
		temp.rare = generalsBaseMsg->rare();
		if(generalsBaseMsg->evolution() == 0)
		{
			temp.evolution = 1;
		}
		else if (generalsBaseMsg->evolution() == 20)
		{
			temp.evolution = 20;
		}
		else 
		{
			temp.evolution = generalsBaseMsg->evolution()+1;
		}

		nextGeneralsEvolution = GeneralEvolutionConfigData::s_generalEvolution[temp];
	}
	allEvolutionNum = nextGeneralsEvolution->get_need_card();

	//设置进度显示（例：5/10）
	std::string evolutionProgress = "";
	char s_cur[10];
	sprintf(s_cur,"%d",curEvolutionNum);
	evolutionProgress.append(s_cur);
	evolutionProgress.append(" / ");
	char s_all[10];
	sprintf(s_all,"%d",allEvolutionNum);
	evolutionProgress.append(s_all);
	l_CapsExp_value->setString(evolutionProgress.c_str());
	//设置进度条长度
	float a = curEvolutionNum*1.0f;
	float b = allEvolutionNum*1.0f;
	float sl = a/b;
	//ImageView_exp_preView->setScaleX(sl);
	//ImageView_exp_cur->setScaleX(sl);
	// 
	if (sp_exp_cur->getActionByTag(Action_Progress_Cur_Tag))
	{
		sp_exp_cur->stopAllActions();
	}

	sp_exp_cur->setPercentage(sl*100);
	
	if (sp_exp_preView->getActionByTag(Action_Progress_PreView_Tag))
	{
		sp_exp_preView->stopAllActions();
	}

	sp_exp_preView->setPercentage(sl*100);
}

void GeneralsEvolutionUI::ProgressToFinishCall()
{
	this->RefreshGeneralEvolutionProgressToDefault(curEvolutionGeneralBaseMsg);
}

void GeneralsEvolutionUI::refreshEvolutionHeadFrame( CGeneralBaseMsg * generalsBaseMsg )
{
	int quality = generalsBaseMsg->currentquality();

	std::string frameNameBase;
	if (quality < 11)                                                                //白色
	{
		frameNameBase = ImageView_MainGeneralFrame_White;
	}
	else if (quality>10 && quality < 21)                              //绿色
	{
		frameNameBase = ImageView_MainGeneralFrame_Green;
	}
	else if (quality>20 && quality < 31)                              //蓝色
	{
		frameNameBase = ImageView_MainGeneralFrame_Blue;
	}
	else if (quality>30 && quality < 41)                              //紫色
	{
		frameNameBase = ImageView_MainGeneralFrame_Purple;
	}
	else if (quality>40 && quality < 51)                              //橙色
	{
		frameNameBase = ImageView_MainGeneralFrame_Orange;
	}
	else
	{
		frameNameBase = ImageView_MainGeneralFrame_White;
	}

	ImageView_MainGeneralFrame->setTexture(frameNameBase.c_str());
}

void GeneralsEvolutionUI::AddBonusSpecialEffect()
{
	// Bonus Special Effect
	CCNodeRGBA* pNode = BonusSpecialEffect::create();
	pNode->setPosition(ccp(469, 342));
	pNode->setScale(1.0f);
	Layer_evolution->addChild(pNode);
}

void GeneralsEvolutionUI::AddAptitudePopupEffect()
{
	CCNodeRGBA* pEffect = AptitudePopupEffect::create(oldEvolutionGeneralDetail->mutable_aptitude(), curEvolutionGeneralDetail->mutable_aptitude());
	Layer_evolution->addChild(pEffect);
	pEffect->setPosition(ccp(535, 135));
}


