#include "GeneralsTeachUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GeneralsUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../GameView.h"
#include "TeachAndEvolutionCell.h"
#include "NormalGeneralsHeadItem.h"
#include "GeneralsListUI.h"
#include "../../messageclient/element/CGeneralsTeach.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../messageclient/element/CGeneralTeachAddedProperty.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "GeneralsEvolutionUI.h"
#include "../../gamescene_state/GameSceneEffects.h"

#define  EvolutionGeneralHeadItemTag 20
#define  kTagTeachAnm 231
#define  kTagAptitudePopupEffect 232

#define  TeachItem1 30
#define  TeachItem2 31
#define  TeachItem3 32
#define  TeachItem4 33

#define Action_TeachProgress_Speed 120
#define Action_TeachProgress_Cur_Tag 70
#define Action_TeachProgress_PreView_Tag 80

GeneralsTeachUI::GeneralsTeachUI():
curTeachGeneralId(-1),
curExpValue(0),
costGoldValue(0),
m_nCallProgressFunctionTimes(0),
m_nPreViewUpgradeLevel(0)
{
	headSlotInfo1 = new headSlotInfo();
	headSlotInfo1->isHave = false;
	headSlotInfo1->loc = ccp(344,107);
	headSlotInfo1->tag = TeachItem1;
	headSlotInfo2 = new headSlotInfo();
	headSlotInfo2->isHave = false;
	headSlotInfo2->loc = ccp(445,107);
	headSlotInfo2->tag = TeachItem2;
	headSlotInfo3 = new headSlotInfo();
	headSlotInfo3->isHave = false;
	headSlotInfo3->loc = ccp(547,107);
	headSlotInfo3->tag = TeachItem3;
	headSlotInfo4 = new headSlotInfo();
	headSlotInfo4->isHave = false;
	headSlotInfo4->loc = ccp(648,107);
	headSlotInfo4->tag = TeachItem4;

	curTeachGeneral = new CGeneralBaseMsg();
	oldTeachGeneral = new CGeneralBaseMsg();
	curTeachGeneralDetail = new CGeneralDetail();
	oldTeachGeneralDetail = new CGeneralDetail();
	curVictimGeneralBaseMsg = new CGeneralBaseMsg();
}

GeneralsTeachUI::~GeneralsTeachUI()
{
	victimList.erase(victimList.begin(),victimList.end());

	delete headSlotInfo1;
	delete headSlotInfo2;
	delete headSlotInfo3;
	delete headSlotInfo4;

	delete curTeachGeneral;
	delete oldTeachGeneral;
	delete curTeachGeneralDetail;
	delete oldTeachGeneralDetail;
	delete curVictimGeneralBaseMsg;
}
GeneralsTeachUI* GeneralsTeachUI::create(CGeneralBaseMsg * generalsBaseMsg)
{
	GeneralsTeachUI * generalsTeachUI = new GeneralsTeachUI();
	if (generalsTeachUI && generalsTeachUI->init(generalsBaseMsg))
	{
		generalsTeachUI->autorelease();
		return generalsTeachUI;
	}
	CC_SAFE_DELETE(generalsTeachUI);
	return NULL;
}

bool GeneralsTeachUI::init(CGeneralBaseMsg * generalsBaseMsg)
{
	if (GeneralsListBase::init())
	{
		//武将传功面板
		panel_teach = (UIPanel*)UIHelper::seekWidgetByName(GeneralsUI::ppanel,"Panel_GeneralSynthesis");
		panel_teach->setVisible(true);
		//武将传功层
		Layer_teach = UILayer::create();
		addChild(Layer_teach);

		Layer_upper = UILayer::create();
		addChild(Layer_upper);

		UIButton *btn_reset = (UIButton*)UIHelper::seekWidgetByName(panel_teach,"Button_reset");   
		btn_reset->setTouchEnable(true);
		btn_reset->setPressedActionEnabled(true);
		btn_reset->addReleaseEvent(this,coco_releaseselector(GeneralsTeachUI::ResetEvent));
		UIButton * btn_back = (UIButton*)UIHelper::seekWidgetByName(panel_teach,"Button_back");   
		btn_back->setTouchEnable(true);
		btn_back->setPressedActionEnabled(true);
		btn_back->addReleaseEvent(this,coco_releaseselector(GeneralsTeachUI::BackEvent));
		UIButton * btn_teach = (UIButton*)UIHelper::seekWidgetByName(panel_teach,"Button_teach");  
		btn_teach->setTouchEnable(true);
		btn_teach->setPressedActionEnabled(true);
		btn_teach->addReleaseEvent(this,coco_releaseselector(GeneralsTeachUI::TeachEvent));
		UIButton * btn_convenientTeach = (UIButton*)UIHelper::seekWidgetByName(panel_teach,"Button_superEnter");  
		btn_convenientTeach->setTouchEnable(true);
		btn_convenientTeach->setPressedActionEnabled(true);
		btn_convenientTeach->addReleaseEvent(this,coco_releaseselector(GeneralsTeachUI::ConvenientTeachEvent));

		ImageView_MainGeneralFrame = (UIImageView*)UIHelper::seekWidgetByName(panel_teach,"ImageView_MainGeneralFrame");

		lbf_hp = (UILabelBMFont*)UIHelper::seekWidgetByName(panel_teach,"LabelBMFont_hp");
		lbf_max_attack = (UILabelBMFont*)UIHelper::seekWidgetByName(panel_teach,"LabelBMFont_max_attack");
		lbf_max_magic_attack = (UILabelBMFont*)UIHelper::seekWidgetByName(panel_teach,"LabelBMFont_max_magic_attack");
		l_hpValue_cur = (UILabel*)UIHelper::seekWidgetByName(panel_teach,"Label_hp_cur");
		l_hpValue_next = (UILabel*)UIHelper::seekWidgetByName(panel_teach,"Label_hp_next");
		l_attackValue_cur = (UILabel*)UIHelper::seekWidgetByName(panel_teach,"Label_attack_cur");
		l_attackValue_next = (UILabel*)UIHelper::seekWidgetByName(panel_teach,"Label_attack_next");
		image_jiantou_first = (UIImageView*)UIHelper::seekWidgetByName(panel_teach,"ImageView_jiantou_0");
		image_jiantou_second = (UIImageView*)UIHelper::seekWidgetByName(panel_teach,"ImageView_jiantou_1");

		l_curTeachExp = (UILabel *)UIHelper::seekWidgetByName(panel_teach,"Label_CurClass");
		l_constValue = (UILabel *)UIHelper::seekWidgetByName(panel_teach,"Label_costValue");
		l_constValue->setText("");

		sp_exp_preView = CCProgressTimer::create(CCSprite::create("res_ui/wujiang/jindu3.png"));
		sp_exp_preView->setType(kCCProgressTimerTypeBar);
		sp_exp_preView->setMidpoint(ccp(0,0));
		sp_exp_preView->setBarChangeRate(ccp(1, 0));
		Layer_teach->addChild(sp_exp_preView);
		sp_exp_preView->setAnchorPoint(ccp(0,0));
		sp_exp_preView->setPosition(ccp(536, 367));
		sp_exp_preView->setScale(.85f);

		CCFadeTo*  action1 = CCFadeTo::create(0.5f,140);
		CCFadeTo*  action2 = CCFadeTo::create(0.5f,255);
		CCRepeatForever * repeapAction = CCRepeatForever::create(CCSequence::create(action1,action2,NULL));
		sp_exp_preView->runAction(repeapAction);

		sp_exp_cur = CCProgressTimer::create(CCSprite::create("res_ui/wujiang/jindu.png"));
		sp_exp_cur->setType(kCCProgressTimerTypeBar);
		sp_exp_cur->setMidpoint(ccp(0,0));
		sp_exp_cur->setBarChangeRate(ccp(1, 0));
		Layer_teach->addChild(sp_exp_cur);
		sp_exp_cur->setAnchorPoint(ccp(0,0));
		sp_exp_cur->setPosition(ccp(536, 367));
		sp_exp_cur->setScale(.85f);

		l_CapsExp_value = CCLabelTTF::create("ABC",APP_FONT_NAME,13);
		l_CapsExp_value->setAnchorPoint(ccp(0.5f,0.5f));
		l_CapsExp_value->setPosition(ccp(626,373));
		Layer_teach->addChild(l_CapsExp_value);

		this->generalsListStatus = HaveNext;
		//武将列表
		generalList_tableView = CCTableView::create(this,CCSizeMake(261,397));
		//generalList_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
		//generalList_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 26, 1, 1), ccp(0,5)); 
		//generalList_tableView->setPressedActionEnabled(true);
		generalList_tableView->setDirection(kCCScrollViewDirectionVertical);
		generalList_tableView->setAnchorPoint(ccp(0,0));
		generalList_tableView->setPosition(ccp(75,42));
		generalList_tableView->setContentOffset(ccp(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		Layer_teach->addChild(generalList_tableView);


		Layer_upper = UILayer::create();
		addChild(Layer_upper);

// 		UIImageView * tableView_kuang = UIImageView::create();
// 		tableView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		tableView_kuang->setScale9Enable(true);
// 		tableView_kuang->setScale9Size(CCSizeMake(261,418));
// 		tableView_kuang->setCapInsets(CCRectMake(30,30,1,1));
// 		tableView_kuang->setAnchorPoint(ccp(0,0));
// 		tableView_kuang->setPosition(ccp(63,32));
// 		Layer_upper->addWidget(tableView_kuang);

		//"主"标识
		UIImageView * imageView_main = UIImageView::create();
		imageView_main->setTexture("res_ui/wujiang/zhu.png");
		imageView_main->setAnchorPoint(ccp(.5f,.5f));
		imageView_main->setPosition(ccp(366,414));//(364,355)
		Layer_upper->addWidget(imageView_main);
		//"辅"标识1
		UIImageView * imageView_other1 = UIImageView::create();
		imageView_other1->setTexture("res_ui/wujiang/fu.png");
		imageView_other1->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_other1->setPosition(ccp(351,225));
		Layer_upper->addWidget(imageView_other1);
		//"辅"标识2
		UIImageView * imageView_other2 = UIImageView::create();
		imageView_other2->setTexture("res_ui/wujiang/fu.png");
		imageView_other2->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_other2->setPosition(ccp(452,225));
		Layer_upper->addWidget(imageView_other2);
		//"辅"标识3
		UIImageView * imageView_other3 = UIImageView::create();
		imageView_other3->setTexture("res_ui/wujiang/fu.png");
		imageView_other3->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_other3->setPosition(ccp(554,225));
		Layer_upper->addWidget(imageView_other3);
		//"辅"标识4
		UIImageView * imageView_other4 = UIImageView::create();
		imageView_other4->setTexture("res_ui/wujiang/fu.png");
		imageView_other4->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_other4->setPosition(ccp(655,225));
		Layer_upper->addWidget(imageView_other4);
	

		return true;
	}
	return false;
}

void GeneralsTeachUI::onEnter()
{
	GeneralsListBase::onEnter();
}

void GeneralsTeachUI::onExit()
{
	GeneralsListBase::onExit();
	std::vector<CGeneralBaseMsg*>::iterator iter_generalsTeachList;
	for (iter_generalsTeachList = generalsTeachList.begin(); iter_generalsTeachList != generalsTeachList.end(); ++iter_generalsTeachList)
	{
		delete *iter_generalsTeachList;
	}
	generalsTeachList.clear();
}

void GeneralsTeachUI::scrollViewDidScroll( CCScrollView* view )
{
}

void GeneralsTeachUI::scrollViewDidZoom( CCScrollView* view )
{
}

void GeneralsTeachUI::createTeachHeadItem( CGeneralBaseMsg * generalsBaseMsg )
{
	if (Layer_teach->getChildByTag(EvolutionGeneralHeadItemTag) != NULL)
	{
		Layer_teach->getChildByTag(EvolutionGeneralHeadItemTag)->removeFromParent();
	}

	oldTeachGeneral->CopyFrom(*generalsBaseMsg);
	curTeachGeneralId = generalsBaseMsg->id();
	curTeachGeneral->CopyFrom(*generalsBaseMsg);
	NormalGeneralsHeadItemBase * teachlHeadItem = NormalGeneralsHeadItemBase::create(generalsBaseMsg);
	teachlHeadItem->ignoreAnchorPointForPosition(false);
	teachlHeadItem->setAnchorPoint(ccp(0,0));
	teachlHeadItem->setPosition(ccp(365,275));
	Layer_teach->addChild(teachlHeadItem,0,EvolutionGeneralHeadItemTag);

	refreshEvolutionHeadFrame(generalsBaseMsg);

	curExpValue = generalsBaseMsg->qualityexp();
	int curExp = generalsBaseMsg->qualityexp();
	int nextExp = 0;
	if (generalsBaseMsg->currentquality() == 0)
	{
		CGeneralsTeach * temp = GeneralTeachConfigData::s_generalTeach[1];
		nextExp = temp->get_need_exp();
	}
	else if (generalsBaseMsg->currentquality() == 50)
	{
		CGeneralsTeach * temp = GeneralTeachConfigData::s_generalTeach[50];
		curExp = 0;
		nextExp = 0;
	}
	else
	{
		CGeneralsTeach * temp_next = GeneralTeachConfigData::s_generalTeach[generalsBaseMsg->currentquality() +1];
		nextExp = temp_next->get_need_exp();
	}
	l_curTeachExp->setText(GeneralsUI::getQuality(generalsBaseMsg->currentquality()).c_str());
	std::string curTeachValue = "";
	char s_curExp[10];
	sprintf(s_curExp,"%d",curExp);
	curTeachValue.append(s_curExp);
	curTeachValue.append(" / ");
	char s_nextExp[10];
	sprintf(s_nextExp,"%d",nextExp);
	curTeachValue.append(s_nextExp);
	l_CapsExp_value->setString(curTeachValue.c_str());
	float a = curExp*1.0f;
	float b = nextExp*1.0f;
	float _scale = a/b;
	sp_exp_preView->setPercentage(_scale*100);
	sp_exp_cur->setPercentage(_scale*100);
	
	l_hpValue_next->setVisible(false);
	lbf_max_attack->setVisible(false);
	lbf_max_magic_attack->setVisible(false);
	l_attackValue_cur->setVisible(true);
	l_attackValue_next->setVisible(false);
	image_jiantou_first->setVisible(false);
	image_jiantou_second->setVisible(false);
}

void GeneralsTeachUI::refreshTeachHeadItem( CGeneralBaseMsg * generalsBaseMsg )
{
	if (Layer_teach->getChildByTag(EvolutionGeneralHeadItemTag) != NULL)
	{
		Layer_teach->getChildByTag(EvolutionGeneralHeadItemTag)->removeFromParent();
	}

	oldTeachGeneral->CopyFrom(*curTeachGeneral);

	curTeachGeneralId = generalsBaseMsg->id();
	curTeachGeneral->CopyFrom(*generalsBaseMsg);

	curTeachGeneralDetail->set_currentquality(curTeachGeneral->currentquality());
	curTeachGeneralDetail->set_qualityexp(curTeachGeneral->qualityexp());

	NormalGeneralsHeadItemBase * teachlHeadItem = NormalGeneralsHeadItemBase::create(generalsBaseMsg);
	teachlHeadItem->ignoreAnchorPointForPosition(false);
	teachlHeadItem->setAnchorPoint(ccp(0,0));
	teachlHeadItem->setPosition(ccp(365,275));
	Layer_teach->addChild(teachlHeadItem,0,EvolutionGeneralHeadItemTag);

	refreshEvolutionHeadFrame(generalsBaseMsg);

	curExpValue = generalsBaseMsg->qualityexp();
	int curExp = generalsBaseMsg->qualityexp();
	int nextExp = 0;
	if (generalsBaseMsg->currentquality() == 0)
	{
		CGeneralsTeach * temp = GeneralTeachConfigData::s_generalTeach[1];
		nextExp = temp->get_need_exp();
	}
	else if (generalsBaseMsg->currentquality() == 50)
	{
		CGeneralsTeach * temp = GeneralTeachConfigData::s_generalTeach[50];
		//nextExp = temp->get_need_exp();
		curExp = 0;
		nextExp = 0;
	}
	else
	{
		CGeneralsTeach * temp_next = GeneralTeachConfigData::s_generalTeach[generalsBaseMsg->currentquality()+1];
		nextExp = temp_next->get_need_exp();
	}
	l_curTeachExp->setText(GeneralsUI::getQuality(generalsBaseMsg->currentquality()).c_str());
	//std::string curTeachValue = "";
	nextTeachValue="";
	char s_curExp[10];
	sprintf(s_curExp,"%d",curExp);
	nextTeachValue.append(s_curExp);
	nextTeachValue.append(" / ");
	char s_nextExp[10];
	sprintf(s_nextExp,"%d",nextExp);
	nextTeachValue.append(s_nextExp);
	l_CapsExp_value->setString(nextTeachValue.c_str());
	float a = curExp*1.0f;
	float b = nextExp*1.0f;
	float _scale = a/b;

	//run action (progress)
	//升了几级
	int levelSpace = curTeachGeneral->currentquality() - oldTeachGeneral->currentquality();
	//begin action
	if (sp_exp_cur->getActionByTag(Action_TeachProgress_Cur_Tag))
	{
	}
	else
	{
		//设为没有副武将的状态
		sp_exp_cur->setVisible(true);
		sp_exp_preView->setVisible(true);
		
		if (oldTeachGeneralDetail->has_generalid())
		{
			//setGeneralsAddedPropertyToDefault(oldTeachGeneralDetail);
			l_curTeachExp->setText(GeneralsUI::getQuality(oldTeachGeneral->currentquality()).c_str());
		}
		
		if (levelSpace == 1)
		{
			CCProgressTo *to1 = CCProgressTo::create((100-sp_exp_cur->getPercentage())*1.0f/Action_TeachProgress_Speed, 100);
			CCProgressTo *to2 = CCProgressTo::create((_scale*100)/Action_TeachProgress_Speed,_scale*100);

			sp_exp_preView->setPercentage(100);
			int t_scale = _scale*100000; 
			CCSequence * sq = CCSequence::create(
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::RefreshGeneralsAddedPropertyPreView)),
				to1,
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::ProgressToFinishCall)),
				CCCallFuncND::create(this,callfuncND_selector(GeneralsTeachUI::FinalProgressToFinishCall),(void *)t_scale),
				to2,
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::ResetCallFucTime)),
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::ProgressNotFinishCall)),
				NULL);

			sq->setTag(Action_TeachProgress_Cur_Tag);
			sp_exp_cur->runAction(sq);
		}
		else if (levelSpace > 1)
		{
			sp_exp_preView->setPercentage(100);
			int t_scale = _scale*100000; 
			CCProgressTo *to1 = CCProgressTo::create((100-sp_exp_cur->getPercentage())*1.0f/Action_TeachProgress_Speed, 100);
			CCRepeat * repeat1 = CCRepeat::create(CCSequence::create(
				CCProgressTo::create(100*1.0f/Action_TeachProgress_Speed, 100),
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::RefreshGeneralsAddedPropertyPreView)),
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::ProgressToFinishCall)),
				NULL),levelSpace-1);
			CCProgressTo *to2 = CCProgressTo::create((_scale*100)/Action_TeachProgress_Speed,_scale*100);

			CCSequence * sq = CCSequence::create(
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::RefreshGeneralsAddedPropertyPreView)),
				to1,
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::ProgressToFinishCall)),
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::RefreshGeneralsAddedPropertyPreView)),
				repeat1,
				CCCallFuncND::create(this,callfuncND_selector(GeneralsTeachUI::FinalProgressToFinishCall),(void *)t_scale),
				to2,
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::ResetCallFucTime)),
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::ProgressNotFinishCall)),
				NULL);

			sq->setTag(Action_TeachProgress_Cur_Tag);
			sp_exp_cur->runAction(sq);
		}
		else
		{
			CCProgressTo *to1 = CCProgressTo::create((_scale*100-sp_exp_cur->getPercentage())*1.0f/Action_TeachProgress_Speed, _scale*100);
			CCSequence * sq = CCSequence::create(to1,
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::ResetCallFucTime)),
				CCCallFunc::create(this,callfunc_selector(GeneralsTeachUI::ProgressNotFinishCall)),
				NULL);

			sq->setTag(Action_TeachProgress_Cur_Tag);
			sp_exp_cur->runAction(sq);
		}
	}
}

void GeneralsTeachUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo1->tag);
	if (headitem)
	{
		if (headitem->getActionByTag(kTagTeachAnm))
		{
			return;
		}
	}

	if (sp_exp_preView->getActionByTag(Action_TeachProgress_PreView_Tag))
	{
		return;
	}

	if (sp_exp_cur->getActionByTag(Action_TeachProgress_Cur_Tag))
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_teach_teaching"));
		return;
	}

	int i = cell->getIdx();
	TeachAndEvolutionCell * tempCell = dynamic_cast<TeachAndEvolutionCell*>(cell);

	if (curTeachGeneral->currentquality() == 50)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_teach_maxLevel"));
		return;
	}

	if (victimList.size() >= 4)
	{
		const char *str1 = StringDataManager::getString("generals_wujiangyiman");
		char* p1 =const_cast<char*>(str1);
		GameView::getInstance()->showAlertDialog(p1);
	}
	else
	{
		if (isAddedToTeach(generalsTeachList.at(i)->id()))
		{
			//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *str1  = ((CCString*)strings->objectForKey("generals_yitianjia"))->m_sString.c_str();
			const char *str1 = StringDataManager::getString("generals_yitianjia");
			char* p1 =const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(p1);
		}
		else
		{
			//准备做牺牲品
			addToTeach(generalsTeachList.at(i),getEmptyItem());
			if(tempCell)
				tempCell->setGray(true);
		}
	}
}

cocos2d::CCSize GeneralsTeachUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(260,72);
}

cocos2d::extension::CCTableViewCell* GeneralsTeachUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	TeachAndEvolutionCell * tempCell = dynamic_cast<TeachAndEvolutionCell*>(cell);
	if (tempCell)
	{
		tempCell->refreshCell(generalsTeachList.at(idx));
	}
	else
	{
		cell = TeachAndEvolutionCell::create(generalsTeachList.at(idx)); 
		if (!cell)
			return NULL;
	}

	if(idx == generalsTeachList.size()-4)
	{
		if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
		{
			if (getIsCanReq())
			{
				//req for next
				this->isReqNewly = false;
				GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
				temp->page = this->s_mCurPage+1;
				temp->pageSize = this->everyPageNum;
				temp->type = 2;
				temp->generalId = this->curTeachGeneralId;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
				setIsCanReq(false);
				delete temp;
			}
		}
	}

	for(int i = 0;i<victimList.size();++i)
	{
		if (victimList.at(i) == generalsTeachList.at(idx)->id())
		{
			TeachAndEvolutionCell * tempCell = dynamic_cast<TeachAndEvolutionCell*>(cell);
			if (tempCell)
				tempCell->setGray(true);
		}
	}

	return cell;
}

unsigned int GeneralsTeachUI::numberOfCellsInTableView( CCTableView *table )
{
	return generalsTeachList.size();
}

void GeneralsTeachUI::RefreshGeneralsList()
{
	this->generalList_tableView->reloadData();
	
}

void GeneralsTeachUI::RefreshGeneralsListWithOutChangeOffSet()
{
	CCPoint _s = generalList_tableView->getContentOffset();
	int _h = generalList_tableView->getContentSize().height + _s.y;
	generalList_tableView->reloadData();
	CCPoint temp = ccp(_s.x,_h - generalList_tableView->getContentSize().height);
	generalList_tableView->setContentOffset(temp); 
}

bool GeneralsTeachUI::isVisible()
{
	return panel_teach->isVisible();
}

void GeneralsTeachUI::setVisible( bool visible )
{
	panel_teach->setVisible(visible);
	Layer_teach->setVisible(visible);
	Layer_upper->setVisible(visible);
}

void GeneralsTeachUI::TeachEvent( CCObject *pSender )
{
	if (victimList.size()>0)
	{
		if (costGoldValue > GameView::getInstance()->getPlayerGold())
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_teach_noEnoughGold"));
		}
		else
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5058,this);
			BeginTeachAnm();
		}
	}
	else
	{
		const char *str1 = StringDataManager::getString("generals_pleaseaddgenerals");
		char* p1 =const_cast<char*>(str1);
		GameView::getInstance()->showAlertDialog(p1);
	}
}

void GeneralsTeachUI::ConvenientTeachEvent(CCObject *pSender)
{
	NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo1->tag);
	if (headitem)
	{
		if (headitem->getActionByTag(kTagTeachAnm))
		{
			return;
		}
	}

	if (sp_exp_preView->getActionByTag(Action_TeachProgress_PreView_Tag))
	{
		return;
	}

	if (sp_exp_cur->getActionByTag(Action_TeachProgress_Cur_Tag))
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_teach_teaching"));
		return;
	}

	if (curTeachGeneral->currentquality() == 50)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_teach_maxLevel"));
		return;
	}

	if (victimList.size() >= 4)   //已满
	{
		const char *str1 = StringDataManager::getString("generals_wujiangyiman");
		char* p1 =const_cast<char*>(str1);
		GameView::getInstance()->showAlertDialog(p1);
	}
	else
	{
		if(generalsTeachList.size()>4)
		{
			for(int i = 0;i<4;i++)
			{
				if (victimList.size() >= 4)   //已满
				{
					break;
				}

				if (isAddedToTeach(generalsTeachList.at(i)->id()))
				{
				}
				else
				{
					//准备做牺牲品
					addToTeach(generalsTeachList.at(i),getEmptyItem());
				}
			}
		}
		else
		{
			for(int i = 0;i<generalsTeachList.size();i++)
			{
				if (victimList.size() >= 4)   //已满
				{
					break;
				}

				if (isAddedToTeach(generalsTeachList.at(i)->id()))
				{
// 					const char *str1 = StringDataManager::getString("generals_yitianjia");
// 					char* p1 =const_cast<char*>(str1);
// 					GameView::getInstance()->showAlertDialog(p1);
				}
				else
				{
					//准备做牺牲品
					addToTeach(generalsTeachList.at(i),getEmptyItem());
				}
			}
		}

		generalList_tableView->reloadData();

	}
}

void GeneralsTeachUI::ResetEvent( CCObject *pSender )
{
	if (sp_exp_preView->getActionByTag(Action_TeachProgress_PreView_Tag))
	{
		sp_exp_preView->stopAllActions();
	}

	if (sp_exp_cur->getActionByTag(Action_TeachProgress_Cur_Tag))
	{
		sp_exp_cur->stopAllActions();
	}

	//设为没有副武将的状态
 	sp_exp_cur->setVisible(true);
 	sp_exp_preView->setVisible(true);

	victimList.erase(victimList.begin(),victimList.end());

	if (Layer_teach->getChildByTag(headSlotInfo1->tag))
	{
		NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo1->tag);
		if (!headitem->getActionByTag(kTagTeachAnm))
		{
			headitem->removeFromParent();
		};
	}

	if (Layer_teach->getChildByTag(headSlotInfo2->tag))
	{
		NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo2->tag);
		if (!headitem->getActionByTag(kTagTeachAnm))
		{
			headitem->removeFromParent();
		};
	}

	if (Layer_teach->getChildByTag(headSlotInfo3->tag))
	{
		NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo3->tag);
		if (!headitem->getActionByTag(kTagTeachAnm))
		{
			headitem->removeFromParent();
		};
	}

	if (Layer_teach->getChildByTag(headSlotInfo4->tag))
	{
		NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo4->tag);
		if (!headitem->getActionByTag(kTagTeachAnm))
		{
			headitem->removeFromParent();
		};
	}

	headSlotInfo1->isHave = false;
	headSlotInfo2->isHave = false;
	headSlotInfo3->isHave = false;
	headSlotInfo4->isHave = false;

	//curExpValue = 0;
	m_nCallProgressFunctionTimes = 0;
	m_nPreViewUpgradeLevel = 0;
	costGoldValue = 0;
	l_constValue->setText("");
	createTeachHeadItem(curTeachGeneral);
	setGeneralsAddedPropertyToDefault(curTeachGeneralDetail);

	generalList_tableView->reloadData();
}

void GeneralsTeachUI::BackEvent( CCObject *pSender )
{
	this->setVisible(false);
	GeneralsUI::generalsListUI->setVisible(true);

	//curEvolutionGeneralDetail->set_evolution(curEvolutionGeneralBaseMsg->evolution());
	//curEvolutionGeneralDetail->set_evolutionexp(curEvolutionGeneralBaseMsg->evolutionexp());

	if (this->curTeachGeneralDetail->has_generalid())
	{
		if (GeneralsUI::generalsListUI->curGeneralBaseMsg->has_id())
		{
			if (GeneralsUI::generalsListUI->curGeneralBaseMsg->id() == this->curTeachGeneralDetail->generalid())
			{
				GeneralsUI::generalsListUI->RefreshGeneralsInfo(curTeachGeneralDetail);
				//刷新武将列表
				GeneralsUI::generalsListUI->isReqNewly = true;
				GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
				temp->page = 0;
				temp->pageSize = 20;
				temp->type = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
				delete temp;
			}
		}
	}
}

GeneralsTeachUI::headSlotInfo* GeneralsTeachUI::getEmptyItem()
{
	if (headSlotInfo1->isHave)
	{
		if (headSlotInfo2->isHave)
		{
			if (headSlotInfo3->isHave)
			{
				if (headSlotInfo4->isHave)
				{
					return NULL;
				}
				else
				{
					return headSlotInfo4;
				}
			}
			else
			{
				return headSlotInfo3;
			}
		}
		else
		{
			return headSlotInfo2;
		}
	}
	else
	{
		return headSlotInfo1;
	}
}

void GeneralsTeachUI::addToTeach(CGeneralBaseMsg * generalsBaseMsg, headSlotInfo* headslotinfo )
{
	if (curTeachGeneral->currentquality() == 50)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_teach_maxLevel"));
		return;
	}

	curVictimGeneralBaseMsg->CopyFrom(*generalsBaseMsg);

	if (headslotinfo != NULL)
	{
 		NormalGeneralsHeadItemBase * headitem = NormalGeneralsHeadItemBase::create(generalsBaseMsg);
		headitem->setAnchorPoint(ccp(0,0));
		headitem->setPosition(ccp(headslotinfo->loc.x,headslotinfo->loc.y));
		headitem->setTag(headslotinfo->tag);
		headitem->setScale(.85f);
		Layer_teach->addChild(headitem);

		headslotinfo->isHave = true;

		victimList.push_back(generalsBaseMsg->id());

		//花费金币
		costGoldValue += generalsBaseMsg->fightpoint()/2 + 1;
		char str_gold [20];
		sprintf(str_gold,"%d",costGoldValue);
		l_constValue->setText(str_gold);

		int nextLevelNeedExp = 0;  //升到下一等级所需的经验
		if (curTeachGeneral->currentquality() == 0)
		{
			CGeneralsTeach * temp = GeneralTeachConfigData::s_generalTeach[1];
			nextLevelNeedExp = temp->get_need_exp();
		}
		else if (curTeachGeneral->currentquality() == 50)
		{
			CGeneralsTeach * temp = GeneralTeachConfigData::s_generalTeach[50];
			nextLevelNeedExp = temp->get_need_exp();
		}
		else
		{
			CGeneralsTeach * temp_next = GeneralTeachConfigData::s_generalTeach[curTeachGeneral->currentquality()+1];
			nextLevelNeedExp = temp_next->get_need_exp();
		}

		curExpValue += generalsBaseMsg->fightpoint()/1 + 1;

		if(curExpValue > nextLevelNeedExp)                   //将会升级
		{
			int max_qualityLevel = 0;
			int max_qualityExp = 0;
			std::map<int, CGeneralsTeach*>::iterator it = GeneralTeachConfigData::s_generalTeach.begin();
			for ( ; it != GeneralTeachConfigData::s_generalTeach.end(); ++ it )
			{
				CGeneralsTeach * tempTeach = it->second;
				if (tempTeach->get_quality_level() > curTeachGeneral->currentquality()+1)
				{
					if (tempTeach->get_quality_level() -1 - curTeachGeneral->currentquality() > 1)
					{
						int newExpValue = 0;
						for(int i = curTeachGeneral->currentquality()+1;i <= tempTeach->get_quality_level() -1;i++)
						{
							CGeneralsTeach * temp_next = GeneralTeachConfigData::s_generalTeach[i];
							if (temp_next)
							{
								newExpValue += temp_next->get_need_exp();
							}
						}

						if (curExpValue-newExpValue < tempTeach->get_need_exp())
						{
							max_qualityLevel = tempTeach->get_quality_level()-1;
							max_qualityExp = tempTeach->get_need_exp();
							break;
						}
					}
					else
					{
						if (curExpValue - nextLevelNeedExp < tempTeach->get_need_exp())
						{
							max_qualityLevel = tempTeach->get_quality_level()-1;
							max_qualityExp = tempTeach->get_need_exp();
							break;
						}
					}

				}
			}


			if (max_qualityLevel == 0)
			{
				max_qualityLevel = 50;
				max_qualityExp = 0;
			}

			//将会升几级
			m_nPreViewUpgradeLevel = max_qualityLevel - oldTeachGeneral->currentquality();
			l_curTeachExp->setText(GeneralsUI::getQuality(max_qualityLevel).c_str());

			nextTeachValue = "";
			char s_retainExpValue[10];
			int retainExpValue = 0;
			if(max_qualityLevel == 50)
			{
				retainExpValue = 0;
			}
			else
			{
				if (max_qualityLevel - curTeachGeneral->currentquality() > 1)
				{
					int newExpValue = 0;
					for(int i = curTeachGeneral->currentquality()+1;i <= max_qualityLevel;i++)
					{
						CGeneralsTeach * temp_next = GeneralTeachConfigData::s_generalTeach[i];
						if (temp_next)
						{
							newExpValue += temp_next->get_need_exp();
						}
					}
					retainExpValue = curExpValue-newExpValue;
				}
				else
				{
					retainExpValue = curExpValue-nextLevelNeedExp;
				}
			}

			std::string str_des;
			sprintf(s_retainExpValue,"%d",retainExpValue);
			str_des.append(s_retainExpValue);
			str_des.append(" / ");
			char s_max_qualityExp[10];
			sprintf(s_max_qualityExp,"%d",max_qualityExp);
			str_des.append(s_max_qualityExp);
			l_CapsExp_value->setString(str_des.c_str());

			float a = retainExpValue*1.0f;
			float b = max_qualityExp*1.0f;
			float _scale = a/b;
			sp_exp_preView->setPercentage(_scale*100);

			if (!curVictimGeneralBaseMsg->has_id())
				return;

			sp_exp_cur->setVisible(false);
			sp_exp_preView->setVisible(true);

			if (!curTeachGeneralDetail->has_generalid())
				return;

			if (!curVictimGeneralBaseMsg->has_id())
				return;

			l_hpValue_next->setVisible(true);
			l_attackValue_next->setVisible(true);
			image_jiantou_first->setVisible(true);
			image_jiantou_second->setVisible(true);

			GeneralTeachAddedPropertyConfigData::TeachAddedKey  tempKey;
			tempKey.star = curTeachGeneralDetail->rare();
			tempKey.quality = curTeachGeneralDetail->currentquality();
			tempKey.profession = BasePlayer::getProfessionIdxByName(curTeachGeneralDetail->profession());

			std::map<GeneralTeachAddedPropertyConfigData::TeachAddedKey,CGeneralTeachAddedProperty*>::const_iterator cIter;
			cIter = GeneralTeachAddedPropertyConfigData::s_generalTeachAdded.find(tempKey);
			if (cIter == GeneralTeachAddedPropertyConfigData::s_generalTeachAdded.end()) // 没找到就是指向END了  
			{

			}
			else
			{
				CGeneralTeachAddedProperty * curAddedProperty = GeneralTeachAddedPropertyConfigData::s_generalTeachAdded[tempKey];
				if (curAddedProperty)
				{
					//get next addePropery
					tempKey.quality = curTeachGeneralDetail->currentquality()+m_nPreViewUpgradeLevel;
					std::map<GeneralTeachAddedPropertyConfigData::TeachAddedKey,CGeneralTeachAddedProperty*>::const_iterator cIter;
					cIter = GeneralTeachAddedPropertyConfigData::s_generalTeachAdded.find(tempKey);
					if (cIter == GeneralTeachAddedPropertyConfigData::s_generalTeachAdded.end()) // 没找到就是指向END了  
					{

					}
					else
					{
						CGeneralTeachAddedProperty * nextAddedProperty = GeneralTeachAddedPropertyConfigData::s_generalTeachAdded[tempKey];
						if (nextAddedProperty)
						{
							char s_hp [20];
							sprintf(s_hp,"%d",curTeachGeneralDetail->aptitude().hpcapacity()+(nextAddedProperty->get_hp_capacity()-curAddedProperty->get_hp_capacity()));
							l_hpValue_next->setText(s_hp);

							if (BasePlayer::getProfessionIdxByName(curTeachGeneralDetail->profession()) == PROFESSION_MJ_INDEX || BasePlayer::getProfessionIdxByName(curTeachGeneralDetail->profession()) == PROFESSION_SS_INDEX)
							{
								char s_attack [20];
								sprintf(s_attack,"%d",curTeachGeneralDetail->aptitude().maxattack()+(nextAddedProperty->get_max_attack()-curAddedProperty->get_max_attack()));
								l_attackValue_next->setText(s_attack);
							}
							else
							{
								char s_magic_attack [20];
								sprintf(s_magic_attack,"%d",curTeachGeneralDetail->aptitude().maxmagicattack()+(nextAddedProperty->get_max_magic_attack()-curAddedProperty->get_max_magic_attack()));
								l_attackValue_next->setText(s_magic_attack);
							}
						}
					}
				}
			}
		}
		else
		{
// 			sp_exp_cur->setVisible(false);
// 			sp_exp_preView->setVisible(true);

			std::string str_des= "";
			char s_curExpValue[10];
			sprintf(s_curExpValue,"%d",curExpValue);
			str_des.append(s_curExpValue);
			str_des.append(" / ");
			char s_nextExp[10];
			sprintf(s_nextExp,"%d",nextLevelNeedExp);
			str_des.append(s_nextExp);
			l_CapsExp_value->setString(str_des.c_str());
			float a = curExpValue*1.0f;
			float b = nextLevelNeedExp*1.0f;
			float _scale = a/b;
			sp_exp_preView->setPercentage( _scale*100);
		}
	}
	else
	{
		const char *str1 = StringDataManager::getString("generals_wujiangyiman");
		char* p1 =const_cast<char*>(str1);
		GameView::getInstance()->showAlertDialog(p1);
	}
}

bool GeneralsTeachUI::isAddedToTeach( long long _id )
{
	for (int m = 0;m<victimList.size();++m)
	{
		if (_id == victimList.at(m))
		{
			return true;
		}
	}
	return false;
}

void GeneralsTeachUI::BeginTeachAnm()
{
	if (headSlotInfo1->isHave)
	{
		NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo1->tag);
		if (headitem)
		{
			CCFiniteTimeAction*  action = CCSequence::create(
				CCMoveTo::create(0.15f,ccp(365,275)),
				CCRemoveSelf::create(),
				NULL);
			action->setTag(kTagTeachAnm);
			headitem->runAction(action);
		}
	}
	if (headSlotInfo2->isHave)
	{
		NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo2->tag);
		if (headitem)
		{
			CCFiniteTimeAction*  action = CCSequence::create(
				CCMoveTo::create(0.15f,ccp(365,275)),
				CCRemoveSelf::create(),
				NULL);
			action->setTag(kTagTeachAnm);
			headitem->runAction(action);
		}
	}
	if (headSlotInfo3->isHave)
	{
		NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo3->tag);
		if (headitem)
		{
			CCFiniteTimeAction*  action = CCSequence::create(
				CCMoveTo::create(0.15f,ccp(365,275)),
				CCRemoveSelf::create(),
				NULL);
			action->setTag(kTagTeachAnm);
			headitem->runAction(action);
		}
	}
	if (headSlotInfo4->isHave)
	{
		NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo4->tag);
		if (headitem)
		{
			CCFiniteTimeAction*  action = CCSequence::create(
				CCMoveTo::create(0.15f,ccp(365,275)),
				CCRemoveSelf::create(),
				NULL);
			action->setTag(kTagTeachAnm);
			headitem->runAction(action);
		}
	}
}

void GeneralsTeachUI::ProgressToFinishCall()
{
	if (!curVictimGeneralBaseMsg->has_id())
		return;

	sp_exp_cur->setPercentage(0);

	int nextExp = 0;  //下一等级经验
	int nextLevel = 0;
	if (oldTeachGeneral->currentquality()+m_nCallProgressFunctionTimes <= 0)
	{
		CGeneralsTeach * temp = GeneralTeachConfigData::s_generalTeach[1];
		nextExp = temp->get_need_exp();
		nextLevel = 1;
	}
	else if (oldTeachGeneral->currentquality()+m_nCallProgressFunctionTimes >= 50)
	{
		CGeneralsTeach * temp = GeneralTeachConfigData::s_generalTeach[50];
		nextExp = temp->get_need_exp();
		nextLevel = 50;
	}
	else
	{
		CGeneralsTeach * temp_next = GeneralTeachConfigData::s_generalTeach[oldTeachGeneral->currentquality()+m_nCallProgressFunctionTimes+1];
		nextExp = temp_next->get_need_exp();
		nextLevel = oldTeachGeneral->currentquality()+m_nCallProgressFunctionTimes+1;
	}

	std::string curTeachValue = "";
	char s_curExpValue[10];
	sprintf(s_curExpValue,"%d",nextExp);
	curTeachValue.append(s_curExpValue);
	curTeachValue.append(" / ");
	char s_nextExp[10];
	sprintf(s_nextExp,"%d",nextExp);
	curTeachValue.append(s_nextExp);
	l_CapsExp_value->setString(curTeachValue.c_str());
	l_curTeachExp->setText(GeneralsUI::getQuality(nextLevel).c_str());


	m_nCallProgressFunctionTimes++;
}

void GeneralsTeachUI::ResetCallFucTime()
{
	m_nCallProgressFunctionTimes = 0;
	setToDefault();
}

void GeneralsTeachUI::ProgressNotFinishCall()
{
	l_CapsExp_value->setString(nextTeachValue.c_str());
	setGeneralsAddedPropertyToDefault(curTeachGeneralDetail);

	//本次升级动画完成
	oldTeachGeneral->CopyFrom(*curTeachGeneral);
	oldTeachGeneralDetail->CopyFrom(*curTeachGeneralDetail);

	setToDefault();
}

void GeneralsTeachUI::RefreshGeneralsAddedPropertyPreView()
{
	if (!curTeachGeneralDetail->has_generalid())
		return;

	if (!curVictimGeneralBaseMsg->has_id())
		return;

	l_hpValue_next->setVisible(true);
	l_attackValue_next->setVisible(true);
	image_jiantou_first->setVisible(true);
	image_jiantou_second->setVisible(true);

	GeneralTeachAddedPropertyConfigData::TeachAddedKey  tempKey;
	tempKey.star = oldTeachGeneral->rare();
	tempKey.quality = oldTeachGeneral->currentquality();
	tempKey.profession = BasePlayer::getProfessionIdxByName(curTeachGeneralDetail->profession());

	std::map<GeneralTeachAddedPropertyConfigData::TeachAddedKey,CGeneralTeachAddedProperty*>::const_iterator cIter;
	cIter = GeneralTeachAddedPropertyConfigData::s_generalTeachAdded.find(tempKey);
	if (cIter == GeneralTeachAddedPropertyConfigData::s_generalTeachAdded.end()) // 没找到就是指向END了  
	{

	}
	else
	{
		CGeneralTeachAddedProperty * curAddedProperty = GeneralTeachAddedPropertyConfigData::s_generalTeachAdded[tempKey];
		if (curAddedProperty)
		{
			//get next addePropery
			tempKey.quality = oldTeachGeneral->currentquality()+m_nCallProgressFunctionTimes+1;
			std::map<GeneralTeachAddedPropertyConfigData::TeachAddedKey,CGeneralTeachAddedProperty*>::const_iterator cIter;
			cIter = GeneralTeachAddedPropertyConfigData::s_generalTeachAdded.find(tempKey);
			if (cIter == GeneralTeachAddedPropertyConfigData::s_generalTeachAdded.end()) // 没找到就是指向END了  
			{

			}
			else
			{
				CGeneralTeachAddedProperty * nextAddedProperty = GeneralTeachAddedPropertyConfigData::s_generalTeachAdded[tempKey];
				if (nextAddedProperty)
				{
					char s_hp [20];
					sprintf(s_hp,"%d",oldTeachGeneralDetail->aptitude().hpcapacity()+(nextAddedProperty->get_hp_capacity()-curAddedProperty->get_hp_capacity()));
					l_hpValue_next->setText(s_hp);

					if (BasePlayer::getProfessionIdxByName(oldTeachGeneralDetail->profession()) == PROFESSION_MJ_INDEX || BasePlayer::getProfessionIdxByName(oldTeachGeneralDetail->profession()) == PROFESSION_SS_INDEX)
					{
						char s_attack [20];
						sprintf(s_attack,"%d",oldTeachGeneralDetail->aptitude().maxattack()+(nextAddedProperty->get_max_attack()-curAddedProperty->get_max_attack()));
						l_attackValue_next->setText(s_attack);
					}
					else
					{
						char s_magic_attack [20];
						sprintf(s_magic_attack,"%d",oldTeachGeneralDetail->aptitude().maxmagicattack()+(nextAddedProperty->get_max_magic_attack()-curAddedProperty->get_max_magic_attack()));
						l_attackValue_next->setText(s_magic_attack);
					}
				}
			}
		}
	}
}

void GeneralsTeachUI::RefreshGeneralsAddedPropertyCur( CGeneralDetail * generalDetail )
{
	oldTeachGeneralDetail->CopyFrom(*curTeachGeneralDetail);
	curTeachGeneralId = generalDetail->generalid();
	curTeachGeneralDetail->CopyFrom(*generalDetail);

	l_hpValue_next->setVisible(false);
	l_attackValue_next->setVisible(false);
	image_jiantou_first->setVisible(false);
	image_jiantou_second->setVisible(false);

	char s_hp [20];
	sprintf(s_hp,"%d",generalDetail->aptitude().hpcapacity());
	l_hpValue_cur->setText(s_hp);

	if (BasePlayer::getProfessionIdxByName(generalDetail->profession()) == PROFESSION_MJ_INDEX || BasePlayer::getProfessionIdxByName(generalDetail->profession()) == PROFESSION_SS_INDEX)
	{
		lbf_max_attack->setVisible(true);
		lbf_max_magic_attack->setVisible(false);

		char s_attack [20];
		sprintf(s_attack,"%d",generalDetail->aptitude().maxattack());
		l_attackValue_cur->setText(s_attack);
	}
	else
	{
		lbf_max_attack->setVisible(false);
		lbf_max_magic_attack->setVisible(true);

		char s_magic_attack [20];
		sprintf(s_magic_attack,"%d",generalDetail->aptitude().maxmagicattack());
		l_attackValue_cur->setText(s_magic_attack);
	}
}

void GeneralsTeachUI::setToDefault()
{
	victimList.erase(victimList.begin(),victimList.end());

	if (Layer_teach->getChildByTag(headSlotInfo1->tag))
	{
		NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo1->tag);
		if (!headitem->getActionByTag(kTagTeachAnm))
		{
			headitem->removeFromParent();
		}
	}

	if (Layer_teach->getChildByTag(headSlotInfo2->tag))
	{
		NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo2->tag);
		if (!headitem->getActionByTag(kTagTeachAnm))
		{
			headitem->removeFromParent();
		}
	}

	if (Layer_teach->getChildByTag(headSlotInfo3->tag))
	{
		NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo3->tag);
		if (!headitem->getActionByTag(kTagTeachAnm))
		{
			headitem->removeFromParent();
		}
	}

	if (Layer_teach->getChildByTag(headSlotInfo4->tag))
	{
		NormalGeneralsHeadItemBase * headitem = (NormalGeneralsHeadItemBase*)Layer_teach->getChildByTag(headSlotInfo4->tag);
		if (!headitem->getActionByTag(kTagTeachAnm))
		{
			headitem->removeFromParent();
		}
	}

	headSlotInfo1->isHave = false;
	headSlotInfo2->isHave = false;
	headSlotInfo3->isHave = false;
	headSlotInfo4->isHave = false;

	costGoldValue = 0;
	l_constValue->setText("");
	//createTeachHeadItem(curTeachGeneral);

	generalList_tableView->reloadData();
}

void GeneralsTeachUI::setGeneralsAddedPropertyToDefault( CGeneralDetail * generalDetail )
{
	curTeachGeneralId = generalDetail->generalid();
	oldTeachGeneralDetail->CopyFrom(*generalDetail);
	curTeachGeneralDetail->CopyFrom(*generalDetail);

	l_hpValue_next->setVisible(false);
	l_attackValue_next->setVisible(false);
	image_jiantou_first->setVisible(false);
	image_jiantou_second->setVisible(false);

	char s_hp [20];
	sprintf(s_hp,"%d",generalDetail->aptitude().hpcapacity());
	l_hpValue_cur->setText(s_hp);

	if (BasePlayer::getProfessionIdxByName(generalDetail->profession()) == PROFESSION_MJ_INDEX || BasePlayer::getProfessionIdxByName(generalDetail->profession()) == PROFESSION_SS_INDEX)
	{
		lbf_max_attack->setVisible(true);
		lbf_max_magic_attack->setVisible(false);

		char s_attack [20];
		sprintf(s_attack,"%d",generalDetail->aptitude().maxattack());
		l_attackValue_cur->setText(s_attack);
	}
	else
	{
		lbf_max_attack->setVisible(false);
		lbf_max_magic_attack->setVisible(true);

		char s_magic_attack [20];
		sprintf(s_magic_attack,"%d",generalDetail->aptitude().maxmagicattack());
		l_attackValue_cur->setText(s_magic_attack);
	}
}

void GeneralsTeachUI::FinalProgressToFinishCall( CCNode * pNode,void * scale )
{
	float t_scale = (int)scale*1.0f/1000.f;
	sp_exp_preView->setPercentage(t_scale);
}

void GeneralsTeachUI::refreshEvolutionHeadFrame( CGeneralBaseMsg * generalsBaseMsg )
{
	int quality = generalsBaseMsg->currentquality();

	std::string frameNameBase;
	if (quality < 11)                                                                //白色
	{
		frameNameBase = ImageView_MainGeneralFrame_White;
	}
	else if (quality>10 && quality < 21)                              //绿色
	{
		frameNameBase = ImageView_MainGeneralFrame_Green;
	}
	else if (quality>20 && quality < 31)                              //蓝色
	{
		frameNameBase = ImageView_MainGeneralFrame_Blue;
	}
	else if (quality>30 && quality < 41)                              //紫色
	{
		frameNameBase = ImageView_MainGeneralFrame_Purple;
	}
	else if (quality>40 && quality < 51)                              //橙色
	{
		frameNameBase = ImageView_MainGeneralFrame_Orange;
	}
	else
	{
		frameNameBase = ImageView_MainGeneralFrame_White;
	}

	ImageView_MainGeneralFrame->setTexture(frameNameBase.c_str());
}

void GeneralsTeachUI::AddBonusSpecialEffect()
{
	// Bonus Special Effect
	CCNodeRGBA* pNode = BonusSpecialEffect::create();
	pNode->setPosition(ccp(414, 348));
	pNode->setScale(1.0f);
	Layer_teach->addChild(pNode);
}

void GeneralsTeachUI::AddAptitudePopupEffect()
{
	if (Layer_teach->getChildByTag(kTagAptitudePopupEffect))
	{
		Layer_teach->getChildByTag(kTagAptitudePopupEffect)->removeFromParent();
	}
	CCNodeRGBA* pEffect = AptitudePopupEffect::create(oldTeachGeneralDetail->mutable_aptitude(), curTeachGeneralDetail->mutable_aptitude());
	Layer_teach->addChild(pEffect);
	pEffect->setTag(kTagAptitudePopupEffect);
	pEffect->setPosition(ccp(625, 295));
}
