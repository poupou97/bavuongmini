
#ifndef _GENERALSUI_GENERALSHEADITEM_H_
#define _GENERALSUI_GENERALSHEADITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCLegendAnimation;

/////////////////////////////////
/**
 * 武将界面下的 武将头像基类
 * 头像，名字，稀有度
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.4
 */

class CGeneralBaseMsg;
class CGeneralDetail;

class GeneralsHeadItemBase :public UIScene
{
public:
	GeneralsHeadItemBase();
	~GeneralsHeadItemBase();

	enum CreateType
	{
		CT_withGeneralBaseMsg = 0,
		CT_withModleId,
		CT_withModleIdAndQuality,
	};

	virtual void onEnter();
	virtual void onExit();

	static GeneralsHeadItemBase * create(CGeneralBaseMsg * generalBaseMsg);
	static GeneralsHeadItemBase * create(int generalModleId);
	static GeneralsHeadItemBase * create(int generalModleId,int currentquality);
	bool init(CGeneralBaseMsg * generalBaseMsg);
	bool init(int generalModleId);
	bool init(int generalModleId,int currentquality);

	//剪影效果
	void showSilhouetteEffect();
	void removeSilhouetteEffect();

	//设置是否可以通过点击展示大卡牌
	void setCanShowBigCard(bool isCan);

	CCLegendAnimation * getLegendHead();
	CCLegendAnimation * getFiveStarAnm();

public:
	UIButton * frame;
	UIImageView * black_namebg;
	UILabel * label_name;
	
	UIImageView * imageView_star;
	UIImageView * imageView_prefession;

	//除底框和头像，剩余的都加到这个层上
	UILayer * layer_up;

	int m_nCreateType;

	CGeneralBaseMsg * m_pGeneralBaseMsg;
	int m_nGeneralModleId;
	int m_nCurrentquality;

	void FrameEvent(CCObject *pSender);

private:
	CCLegendAnimation * la_head;
};

#endif