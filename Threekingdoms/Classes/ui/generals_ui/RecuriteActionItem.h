#ifndef  _GENERALS_RECURITEACTIONITEM_H_
#define _GENERALS_RECURITEACTIONITEM_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 武将界面下的“武将招募”中的普通，一般，极品招募
 * 主要用来控制状态和刷新时间显示
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.3
 */

class UITab;
class CActionDetail;


class RecuriteActionItem : public UIScene
{
public:
	RecuriteActionItem();
	~RecuriteActionItem();
	
	enum State{
		RunCD,
		Free,
		Gold
	};

// 	enum Style{
// 		RetainNum,
// 		Free,
// 		Gold
// 	};

	static RecuriteActionItem * create(CActionDetail * actionDetail);
	bool init(CActionDetail * actionDetail);

	static std::string getStarPathByNum(int num);
	void update(float dt);
	void refreshData();
    static std::string timeFormatToString(long long t);

public: 
	CActionDetail * curActionDetail;
	float m_fRecordTime;
	State state ;

	long long cdTime;
	int actionMaxNumber ;
	int playerRemainNumber ;
	double lastChangeTime;

private:
	cocos2d::extension::UIImageView * retainImage;
	cocos2d::extension::UILabel * label_num;
	cocos2d::extension::UIImageView * freeImage;
	cocos2d::extension::UILabel * label_time;
	cocos2d::extension::UIImageView* freeBefore;
	cocos2d::extension::UIImageView * image_gold;
	cocos2d::extension::UILabel * label_gold;
	cocos2d::extension::UIImageView * image_noFreeTime;
};
#endif

