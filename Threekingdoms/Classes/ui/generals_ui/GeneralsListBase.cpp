#include "GeneralsListBase.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GeneralsUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"

GeneralsListBase::GeneralsListBase():
generalsListStatus(HaveNone),
everyPageNum(20),
s_mCurPage(0),
isReqNewly(false),
isCanReq(false)
{

}

GeneralsListBase::~GeneralsListBase()
{

}
GeneralsListBase* GeneralsListBase::create()
{
	GeneralsListBase * generalsListBase = new GeneralsListBase();
	if (generalsListBase && generalsListBase->init())
	{
		generalsListBase->autorelease();
		return generalsListBase;
	}
	CC_SAFE_DELETE(generalsListBase);
	return NULL;
}

bool GeneralsListBase::init()
{
	if (UIScene::init())
	{
		return true;
	}
	return false;
}

void GeneralsListBase::onEnter()
{
	UIScene::onEnter();
}

void GeneralsListBase::onExit()
{
	UIScene::onExit();
}

bool GeneralsListBase::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	return true;
}

void GeneralsListBase::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralsListBase::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralsListBase::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralsListBase::RefreshGeneralsListStatus( int page, int allPageNum )
{
	s_mCurPage = page;
	s_mAllPageNum = allPageNum;
	if (page == 0)
	{
		if (page < allPageNum-1)
		{
			this->generalsListStatus = HaveNext;
		}
		else
		{
			this->generalsListStatus = HaveNone;
		}
	}
	else
	{
		if (page < allPageNum-1)
		{
			this->generalsListStatus = HaveBoth;
		}
		else
		{
			this->generalsListStatus = HaveLast;
		}
	}
}

bool GeneralsListBase::getIsCanReq()
{
	return isCanReq;
}

void GeneralsListBase::setIsCanReq( bool _value )
{
	isCanReq = _value;
}
