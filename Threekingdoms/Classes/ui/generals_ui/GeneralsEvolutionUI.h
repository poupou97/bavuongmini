#ifndef _GENERALSUI_GENERALSEVOTUTIONUI_H_
#define _GENERALSUI_GENERALSEVOTUTIONUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "./GeneralsListBase.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralBaseMsg;
class CGeneralDetail;

#define ImageView_MainGeneralFrame_White "res_ui/wujiang/di_white.png"
#define ImageView_MainGeneralFrame_Green "res_ui/wujiang/di_green.png"
#define ImageView_MainGeneralFrame_Blue "res_ui/wujiang/di_blue.png"
#define ImageView_MainGeneralFrame_Purple "res_ui/wujiang/di_purple.png"
#define ImageView_MainGeneralFrame_Orange "res_ui/wujiang/di_orange.png"

/////////////////////////////////
/**
 * 武将界面下的 武将进化类
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.4
 */

class GeneralsEvolutionUI : public GeneralsListBase,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	GeneralsEvolutionUI();
	~GeneralsEvolutionUI();


	static GeneralsEvolutionUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

private:
	UIPanel * panel_evolution; //进化面板
	UILayer * Layer_evolution;
	UILayer * Layer_upper;
	UIButton * btn_reset;   //重置
	UIButton * btn_levelUp; //升级
	UIButton * btn_teach;  //传功

	UIImageView * ImageView_MainGeneralFrame;

	UILabel * Label_CapsExp_value;
	UIImageView * ImageView_exp_cur;
	UIImageView * ImageView_exp_preView;

	CCLabelTTF * l_CapsExp_value;
	CCProgressTimer *sp_exp_cur;
	CCProgressTimer *sp_exp_preView;

	UILabel * Label_curStrenth;
	UILabel * Label_addStrenth;
	UILabel * Label_curDexterity;
	UILabel * Label_addDexterity;
	UILabel * Label_curIntelligence;
	UILabel * Label_addIntelligence;
	UILabel * Label_curFocus;
	UILabel * Label_addFocus;
	UILabel * L_constValue;

// 	int curStrenth;
// 	int nextStrenth;
// 	int curDexterity;
// 	int nextDexterity;
// 	int curIntelligence;
// 	int nextIntelligence;
// 	int curFocus;
// 	int nextFocus;

	CCTableView * generalList_tableView;	
	
	//当前进化牌数
	int curEvolutionNum;
	//进化总牌数（进化多少次可以升级）
	int allEvolutionNum;

public:
	std::vector<CGeneralBaseMsg *> evolutionGeneralsList;

	long long curEvolutionGeneralId ;
	CGeneralBaseMsg * curEvolutionGeneralBaseMsg;
	CGeneralDetail * curEvolutionGeneralDetail;
	CGeneralDetail * oldEvolutionGeneralDetail;

	long long curVictimGeneralId;
	CGeneralBaseMsg * curGeneralBaseMsg;

	long long lastVictimGeneralId;

	//刷新武将列表
	void RefreshGeneralsList();
	//刷新武将列表(tableView 的偏移量不变)
	void RefreshGeneralsListWithOutChangeOffSet();

	void BackEvent(CCObject * pSender);
	void ResetEvent(CCObject * pSender);
	void EvolutionEvent(CCObject * pSender);
	void SureToEvolution(CCObject *pSender);

	void createEvolutionHead(CGeneralBaseMsg * generalsBaseMsg);
	void refreshEvolutionHead(CGeneralBaseMsg * generalsBaseMsg,bool isUpgraded = false);
	void refreshEvolutionHeadFrame(CGeneralBaseMsg * generalsBaseMsg);
	void createVictimHead();
	//刷新当前武将的属性信息以及下一级的属性信息
	void RefreshGeneralEvolutionInfo(CGeneralDetail *generalsDetail);
	//进度条刷新
	void RefreshGeneralEvolutionProgress(CGeneralBaseMsg * generalsBaseMsg);
	//进度条预览
	void RefreshGeneralEvolutionProgressPreView(CGeneralBaseMsg * generalsBaseMsg);
	//进度条刷新为默认状态
	void RefreshGeneralEvolutionProgressToDefault(CGeneralBaseMsg * generalsBaseMsg);

	void ProgressToFinishCall();

	virtual bool isVisible();
	virtual void setVisible(bool visible);

	//add bonusSpecialEffect
	void AddBonusSpecialEffect();

	//add AptitudePopupEffect
	void AddAptitudePopupEffect();
};

#endif

