#ifndef  _GENERALSUI_GENERALSUI_H_
#define _GENERALSUI_GENERALSUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UITab;
class CActionDetail;
class CGeneralBaseMsg;
class CGeneralDetail;
class GeneralsListUI;
class GeneralsEvolutionUI;
class GeneralsTeachUI;
class GeneralsRecuriteUI;
class SingleRecuriteResultUI;
class TenTimesRecuriteResultUI;
class GeneralsStrategiesUI;
class GeneralsSkillsUI;
class GameFightSkill;

#define  BASERECURITEACTIONITEMTAG 50 
#define  BETTERRECURITEACTIONITEMTAG 51
#define  BESTRECURITEACTIONITEMTAG 52 

#define  TenTimeRecruitResultBaseTag 120

class GeneralsUI : public UIScene
{
public:
	GeneralsUI();
	~GeneralsUI();

	struct ReqListParameter{
		int page;
		int pageSize;
		int type ;
		long long generalId;
	};

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	static GeneralsUI * create();
	bool init();

public:
	UILayer * u_layer;
	static UIPanel * ppanel;

    cocos2d::extension::UIButton * Button_close;

	//武将招募
	static GeneralsRecuriteUI * generalsRecuriteUI;
	//武将单次招募结果阵法
	static SingleRecuriteResultUI * singleRecuriteResultUI;
	//武将十连抽结果
	static TenTimesRecuriteResultUI * tenTimesRecuriteResultUI;

	//武将列表
	static GeneralsListUI * generalsListUI;
	//武将进化
	static GeneralsEvolutionUI * generalsEvolutionUI;
	//武将传功
	static GeneralsTeachUI * generalsTeachUI;

	//阵法
	static GeneralsStrategiesUI * generalsStrategiesUI;

	//武将技能
	static GeneralsSkillsUI * generalsSkillsUI;

	UITab * mainTab;

	void CloseEvent(CCObject *pSender);
	void IndexChangedEvent(CCObject* pSender);

	//每次打开都停在招募界面
	void setToDefaultTab();

	void setToTabByIndex(int idx);

	//通过Int型的品阶数据获取string型的品阶
	static std::string getQuality(int quality);
	//通过Int型的品阶数据获取武将头像框的名字
	static std::string getBigHeadFramePath(int quality);
	static std::string getSmallHeadFramePath(int quality);

	//教学
	virtual void registerScriptCommand(int scriptId);
	//给uitab(点将台)添加
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	//给uitab(我的武将)添加
	void addCCTutorialIndicator2(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	//给关闭按钮添加
	void addCCTutorialIndicator3(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	//给uitab(阵法)添加
	void addCCTutorialIndicator4(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

public:
	//教学
	int mTutorialScriptInstanceId;

};



/////////////////////////////////
/**
 * 武将界面下的“我的武将”中的武将列表项
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.2
 */

class CGeneralBaseMsg;
class CCMoveableMenu;

class GeneralsListCell : public CCTableViewCell
{
public:
	GeneralsListCell();
	~GeneralsListCell();
	static GeneralsListCell* create(int index,CGeneralBaseMsg * generalBaseMsg);
	bool init(int index,CGeneralBaseMsg * generalBaseMsg);

	CGeneralBaseMsg* getCurGeneralBaseMsg();

	void RefreshCell(int index,CGeneralBaseMsg * generalBaseMsg);
private:
	void FirstMenuEvent(CCObject * pSender);
	void SecondMenuEvent(CCObject * pSender);

private:
	CGeneralBaseMsg * curGeneralBaseMsg; 
	int m_index;
	//教学
	int mTutorialScriptInstanceId;
	//教学第一个还是第二个按钮
	bool isFirstOrSecond;

	int m_nNowState;						// 1:展示；0：收回

private:
	CCSprite * sprite_icon;
	CCLabelTTF * label_level;
	CCLabelTTF * label_name;
	CCLabelTTF * label_rank;

	CCMoveableMenu* firstMenu;
	CCMoveableMenu* secondMenu;
	CCLabelTTF * label_firstMenu;
	CCLabelTTF * label_secondMenu;

	CCLabelTTF * m_label_generalShow;
};


#endif

