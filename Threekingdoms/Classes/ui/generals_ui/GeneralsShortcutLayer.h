
#ifndef _GENERALSUI_GENERALSSHORTCUTLAYER_H_
#define _GENERALSUI_GENERALSSHORTCUTLAYER_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "GeneralsSkillsUI.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CShortCut;
class CGeneralDetail;

class GeneralsShortcutLayer : public UIScene
{
private:
	CCPoint slotPos[5] ;

public:
	UILayer * layer_shortcut;
	UILayer * layer_musou;

	GeneralsSkillsUI::SkillTpye curSkillType;
	CGeneralDetail * curGeneralDetail;
	//记录的上一个无双技能
	std::string m_sLastMumouId;
	std::string m_sCurMumouId;
	//记录的上一个武将Id
	long long m_nLastGeneralId;

public:
	GeneralsShortcutLayer();
	~GeneralsShortcutLayer();

	static GeneralsShortcutLayer * create(CGeneralDetail * generalDetail);
	bool init(CGeneralDetail * generalDetail);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void RefreshGeneralsShortcutSlot( CGeneralDetail * generalDetail );

	void setSkillTypeByGeneralsDetail( CGeneralDetail *generalsDetail );

	//刷新武将技能(单个快捷栏)
	void RefreshOneShortcutSlot(long long generalsId,CShortCut * shortCut);

	//刷新武将技能(所有快捷栏)
	void RefreshAllShortcutSlot(long long generalsId,std::vector<CShortCut*> shortCutList);

//	//初始化无双技能
// 	void InitMusouSkill(CGeneralDetail * generalDetail);
// 
// 	//设置无双技能
// 	void AddMusouNormal(int index);
// 	void AddMusouWithAnimation(int index);


	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();
	//教学
	int mTutorialScriptInstanceId;

};

#endif

