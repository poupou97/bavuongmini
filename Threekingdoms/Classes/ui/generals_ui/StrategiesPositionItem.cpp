#include "StrategiesPositionItem.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "GeneralsListBase.h"
#include "NormalGeneralsHeadItem.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "generals_popup_ui/OnStrategiesGeneralsInfoUI.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "GeneralsUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CFightWayBase.h"
#include "../../messageclient/element/CFightWayGrowingUp.h"
#include "GeneralsStrategiesUI.h"
#include "AppMacros.h"
#include "RecuriteActionItem.h"


StrategiesPositionItem::StrategiesPositionItem():
positionIndex(1),
fightStatus(1),
isHaveGeneral(false),
genenralsId(-1)
{
}


StrategiesPositionItem::~StrategiesPositionItem()
{
}

StrategiesPositionItem * StrategiesPositionItem::create(CGeneralBaseMsg * generalBaseMsg)
{
	StrategiesPositionItem * strategiesPositionItem = new StrategiesPositionItem();
	if (strategiesPositionItem && strategiesPositionItem->init(generalBaseMsg))
	{
		strategiesPositionItem->autorelease();
		return strategiesPositionItem;
	}
	CC_SAFE_DELETE(strategiesPositionItem);
	return NULL;
}

StrategiesPositionItem * StrategiesPositionItem::create( int index )
{
	StrategiesPositionItem * strategiesPositionItem = new StrategiesPositionItem();
	if (strategiesPositionItem && strategiesPositionItem->init(index))
	{
		strategiesPositionItem->autorelease();
		return strategiesPositionItem;
	}
	CC_SAFE_DELETE(strategiesPositionItem);
	return NULL;
}

bool StrategiesPositionItem::init(CGeneralBaseMsg * generalBaseMsg)
{
	if (UIScene::init())
	{
		if (generalBaseMsg->order() == 0)
			return true;

		positionIndex = generalBaseMsg->order();
		fightStatus = generalBaseMsg->fightstatus();
		isHaveGeneral = true;
		genenralsId = generalBaseMsg->id();

		UIImageView * imageView_Frame = UIImageView::create();
		imageView_Frame->setTexture("res_ui/no4_di.png");
		imageView_Frame->setScale9Enable(true);
		imageView_Frame->setCapInsets(CCRectMake(7,7,1,1));
		imageView_Frame->setScale9Size(CCSizeMake(127,165));
		imageView_Frame->setAnchorPoint(ccp(0,0));
		imageView_Frame->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(imageView_Frame);

// 		UIImageView * imageView_miaobian = UIImageView::create();
// 		imageView_miaobian->setTexture("res_ui/miaobian1.png");
// 		imageView_miaobian->setScale9Enable(true);
// 		imageView_miaobian->setScale9Size(CCSizeMake(126,163));
// 		imageView_miaobian->setCapInsets(CCRectMake(4,4,1,1));
// 		imageView_miaobian->setAnchorPoint(ccp(0,0));
// 		imageView_miaobian->setPosition(ccp(-1,-1));
// 		m_pUiLayer->addWidget(imageView_miaobian);

		UIImageView * imageView_desFrame = UIImageView::create();
		imageView_desFrame->setTexture("res_ui/zhezhao80.png");
		imageView_desFrame->setScale9Enable(true);
		imageView_desFrame->setScale9Size(CCSizeMake(118,23));
		imageView_desFrame->setAnchorPoint(ccp(0,0));
		imageView_desFrame->setPosition(ccp(4,3));
		imageView_Frame->addChild(imageView_desFrame);

		//底部的加成显示
		l_additive = UILabel::create();
		l_additive->setText("asdf1234");
		l_additive->setFontName(APP_FONT_NAME);
		l_additive->setFontSize(16);
		l_additive->setColor(ccc3(108,255,0));
		l_additive->setAnchorPoint(ccp(0.5f,0.5f));
		l_additive->setPosition(ccp(62,13));
		m_pUiLayer->addWidget(l_additive);


		//最上方的状态按钮
// 		btn_fightStatus = UIButton::create();
// 		btn_fightStatus->setTextures("res_ui/new_button_00.png","res_ui/new_button_00.png","");
// 		btn_fightStatus->setScale9Enable(true);
// 		btn_fightStatus->setScale9Size(CCSizeMake(126,34));
// 		btn_fightStatus->setAnchorPoint(ccp(0.5f,0.5f));
// 		btn_fightStatus->setCapInsets(CCRectMake(31,0,1,0));
// 		btn_fightStatus->setPosition(ccp(62,147));
// 		btn_fightStatus->setTouchEnable(true);
// 		btn_fightStatus->addReleaseEvent(this,coco_releaseselector(StrategiesPositionItem::FightStatusEvent));
// 		m_pUiLayer->addWidget(btn_fightStatus);
		//编号
// 		UILabel * l_index = UILabel::create();
// 		char s_positionIndex[3];
// 		sprintf(s_positionIndex,"%d",positionIndex);
// 		l_index->setText(s_positionIndex);
// 		l_index->setAnchorPoint(ccp(0,0));
// 		l_index->setPosition(ccp(-29,-9));
// 		l_index->setFontName(APP_FONT_NAME);
// 		l_index->setFontSize(18);
// 		btn_fightStatus->addChild(l_index);
		//状态的显示
// 		l_fightStatus = UILabel::create();
// 		RefreshGeneralsStatus(generalBaseMsg->fightstatus());
// 		l_fightStatus->setAnchorPoint(ccp(0,0));
// 		l_fightStatus->setPosition(ccp(-12,-9));
// 		l_fightStatus->setFontName(APP_FONT_NAME);
// 		l_fightStatus->setFontSize(18);
// 		btn_fightStatus->addChild(l_fightStatus);
		//中间的头像背景框
		UIButton * btn_headFrame = UIButton::create();
		btn_headFrame->setTextures("res_ui/generals_white.png","res_ui/generals_white.png","");
		btn_headFrame->setAnchorPoint(ccp(0.5f,0.5f));
		btn_headFrame->setPosition(ccp(62,92));
		btn_headFrame->setTouchEnable(true);
		btn_headFrame->addReleaseEvent(this,coco_releaseselector(StrategiesPositionItem::HeadFrameEvent));
		imageView_Frame->addChild(btn_headFrame);
		btn_headFrame->setScale(0.93f);
		//中间的头像
		if (generalBaseMsg->fightstatus() == GeneralsListBase::NoBattle)
		{
			//中间的头像(没有时是一个加号)
// 			UIImageView * imageView_head = UIImageView::create();
// 			imageView_head->setTexture("res_ui/plus.png");
// 			imageView_head->setAnchorPoint(ccp(0.5f,0.5f));
// 			imageView_head->setPosition(ccp(62,79));
// 			m_pUiLayer->addWidget(imageView_head);
		}
		else
		{
			NormalGeneralsHeadItemBase * normalGeneralsHeadItem = NormalGeneralsHeadItemBase::create(generalBaseMsg);
			normalGeneralsHeadItem->ignoreAnchorPointForPosition(false);
			normalGeneralsHeadItem->setAnchorPoint(ccp(0.5f,0.5f));
			normalGeneralsHeadItem->setPosition(ccp(62,92));
			addChild(normalGeneralsHeadItem);
			normalGeneralsHeadItem->setScale(0.93f);
// 			CGeneralBaseMsg * generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];
// 			//边框
// 			UIImageView *frame = UIImageView::create();
// 			frame->setTexture(GeneralsUI::getBigHeadFramePath(generalBaseMsg->currentquality()).c_str());
// 			frame->setScale9Enable(true);
// 			frame->setScale9Size(CCSizeMake(108,110));
// 			frame->setAnchorPoint(CCPointZero);
// 			frame->setPosition(ccp(8,24));
// 			m_pUiLayer->addWidget(frame);
// 			//头像
// 			std::string icon_path = "res_ui/generals/";
// 			icon_path.append(generalMsgFromDb->get_half_photo());
// 			icon_path.append(".png");
// 			UIImageView *imageView_head = UIImageView::create();
// 			imageView_head->setTexture(icon_path.c_str());
// 			imageView_head->setAnchorPoint(ccp(0.5f,0));
// 			imageView_head->setPosition(ccp(frame->getPosition().x+frame->getSize().width/2,31));
// 			imageView_head->setScale(0.85f);
// 			m_pUiLayer->addWidget(imageView_head);
// 			//名字黑底
//  			UIImageView *black_namebg = UIImageView::create(); 
//  			black_namebg->setTexture("res_ui/name_di3.png");
//  			black_namebg->setAnchorPoint(ccp(0.5f,0));
// 			black_namebg->setScale9Enable(true);
// 			black_namebg->setCapInsets(CCRectMake(12,18,1,1));
// 			black_namebg->setScale9Size(CCSizeMake(96,19));
//  			black_namebg->setPosition(ccp(frame->getPosition().x+frame->getSize().width/2,31));
//  			m_pUiLayer->addWidget(black_namebg);
// 			//名字
// 			UILabel *label_name = UILabel::create();
// 			label_name->setText(generalMsgFromDb->name().c_str());
// 			label_name->setFontName(APP_FONT_NAME);
// 			label_name->setFontSize(16);
// 			label_name->setAnchorPoint(ccp(0.5f,0.5f));
// 			label_name->setPosition(ccp(frame->getPosition().x+frame->getSize().width/2,black_namebg->getPosition().y+black_namebg->getSize().height/2));
// 			label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
// 			m_pUiLayer->addWidget(label_name);
// 
// 			//稀有度
// 			if (generalBaseMsg->rare()>0)
// 			{
// 				UIImageView * star = UIImageView::create();
// 				star->setTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
// 				star->setAnchorPoint(CCPointZero);
// 				star->setScale(0.8f);
// 				star->setPosition(ccp(11,40));
// 				m_pUiLayer->addWidget(star);
// 			}
// 
// 			//等级底图
// 			UIImageView * imageView_lvFrame = UIImageView::create();
// 			imageView_lvFrame->setTexture("res_ui/zhezhao80.png");
// 			imageView_lvFrame->setScale9Enable(true);
// 			imageView_lvFrame->setScale9Size(CCSizeMake(43,15));
// 			imageView_lvFrame->setAnchorPoint(ccp(1.0f,0));
// 			imageView_lvFrame->setPosition(ccp(110,50));
// 			m_pUiLayer->addWidget(imageView_lvFrame);
// 			//等级
// 			std::string _lv = "LV";
// 			char s_level[5];
// 			sprintf(s_level,"%d",generalBaseMsg->level());
// 			_lv.append(s_level);
// 			UILabel * label_level = UILabel::create();
// 			label_level->setText(_lv.c_str());
// 			label_level->setAnchorPoint(ccp(0.5f,0.5f));
// 			label_level->setPosition(ccp(-20,8));
// 			imageView_lvFrame->addChild(label_level);
// 			//军衔
// 			std::string rankPathBase = "res_ui/rank/rank";
// 			char s_evolution[5];
// 			sprintf(s_evolution,"%d",generalBaseMsg->evolution());
// 			rankPathBase.append(s_evolution);
// 			rankPathBase.append(".png");
// 			UIImageView * imageView_rank = UIImageView::create();
// 			imageView_rank->setTexture(rankPathBase.c_str());
// 			imageView_rank->setAnchorPoint(ccp(1.0f,1.0f));
// 			imageView_rank->setPosition(ccp(107,122));
// 			m_pUiLayer->addWidget(imageView_rank);
		}

		UILayer * u_layer_pos = UILayer::create();
		addChild(u_layer_pos);

		std::string str_postionPath = "res_ui/wujiang/";
		switch(positionIndex)
		{
		case 1:
			{
				str_postionPath.append("one");
			}
			break;
		case 2:
			{
				str_postionPath.append("two");
			}
			break;
		case 3:
			{
				str_postionPath.append("three");
			}
			break;
		case 4:
			{
				str_postionPath.append("four");
			}
			break;
		case 5:
			{
				str_postionPath.append("five");
			}
			break;
		}
		str_postionPath.append(".png");
		UIImageView * imageView_pos = UIImageView::create();
		imageView_pos->setTexture(str_postionPath.c_str());
		imageView_pos->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_pos->setPosition(ccp(21,140));
		u_layer_pos->addWidget(imageView_pos);

		RefreshAddedProperty();

		this->setContentSize(CCSizeMake(127,165));

		return true;
	}
	return false;
}

bool StrategiesPositionItem::init( int index )
{
	if (UIScene::init())
	{
		positionIndex = index;
		fightStatus = GeneralsListBase::HoldTheLine;
		isHaveGeneral = false;

		UIImageView * imageView_Frame = UIImageView::create();
		imageView_Frame->setTexture("res_ui/no4_di.png");
		imageView_Frame->setScale9Enable(true);
		imageView_Frame->setCapInsets(CCRectMake(7,7,1,1));
		imageView_Frame->setScale9Size(CCSizeMake(127,165));
		imageView_Frame->setAnchorPoint(ccp(0,0));
		imageView_Frame->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(imageView_Frame);

// 		UIImageView * imageView_miaobian = UIImageView::create();
// 		imageView_miaobian->setTexture("res_ui/miaobian1.png");
// 		imageView_miaobian->setScale9Enable(true);
// 		imageView_miaobian->setScale9Size(CCSizeMake(126,163));
// 		imageView_miaobian->setCapInsets(CCRectMake(4,4,1,1));
// 		imageView_miaobian->setAnchorPoint(ccp(0,0));
// 		imageView_miaobian->setPosition(ccp(-1,-1));
// 		m_pUiLayer->addWidget(imageView_miaobian);

		UIImageView * imageView_desFrame = UIImageView::create();
		imageView_desFrame->setTexture("res_ui/zhezhao80.png");
		imageView_desFrame->setScale9Enable(true);
		imageView_desFrame->setScale9Size(CCSizeMake(118,23));
		imageView_desFrame->setAnchorPoint(ccp(0,0));
		imageView_desFrame->setPosition(ccp(4,3));
		imageView_Frame->addChild(imageView_desFrame);

		//底部的加成显示
		l_additive = UILabel::create();
		l_additive->setText("asdf1234");
		l_additive->setFontName(APP_FONT_NAME);
		l_additive->setFontSize(16);
		l_additive->setColor(ccc3(108,255,0));
		l_additive->setAnchorPoint(ccp(0.5f,0.5f));
		l_additive->setPosition(ccp(62,13));
		m_pUiLayer->addWidget(l_additive);

		//最上方的状态按钮
// 		btn_fightStatus = UIButton::create();
// 		btn_fightStatus->setTextures("res_ui/new_button_00.png","res_ui/new_button_00.png","");
// 		btn_fightStatus->setScale9Enable(true);
// 		btn_fightStatus->setScale9Size(CCSizeMake(126,34));
// 		btn_fightStatus->setAnchorPoint(ccp(0.5f,0.5f));
// 		btn_fightStatus->setPosition(ccp(62,147));
// 		btn_fightStatus->setTouchEnable(true);
// 		btn_fightStatus->addReleaseEvent(this,coco_releaseselector(StrategiesPositionItem::FightStatusEvent));
// 		m_pUiLayer->addWidget(btn_fightStatus);
		//编号
// 		UILabel * l_index = UILabel::create();
// 		char s_positionIndex[3];
// 		sprintf(s_positionIndex,"%d",positionIndex);
// 		l_index->setText(s_positionIndex);
// 		l_index->setAnchorPoint(ccp(0,0));
// 		l_index->setPosition(ccp(-29,-7));
// 		l_index->setFontName(APP_FONT_NAME);
// 		l_index->setFontSize(18);
// 		btn_fightStatus->addChild(l_index);
		//状态的显示
// 		l_fightStatus = UILabel::create();
// 		RefreshGeneralsStatus(GeneralsListBase::NoBattle);
// 		l_fightStatus->setAnchorPoint(ccp(0,0));
// 		l_fightStatus->setPosition(ccp(-12,-7));
// 		l_fightStatus->setFontName(APP_FONT_NAME);
// 		l_fightStatus->setFontSize(18);
// 		btn_fightStatus->addChild(l_fightStatus);
		//中间的头像背景框
		UIButton * btn_headFrame = UIButton::create();
		btn_headFrame->setTextures("res_ui/generals_white.png","res_ui/generals_white.png","");
		btn_headFrame->setAnchorPoint(ccp(0.5f,0.5f));
		btn_headFrame->setPosition(ccp(62,92));
		btn_headFrame->setTouchEnable(true);
		btn_headFrame->addReleaseEvent(this,coco_releaseselector(StrategiesPositionItem::HeadFrameEvent));
		imageView_Frame->addChild(btn_headFrame);
		btn_headFrame->setScale(0.93f);
		//中间的头像(没有时是一个加号)
// 		UIImageView * imageView_head = UIImageView::create();
// 		imageView_head->setTexture("res_ui/plus.png");
// 		imageView_head->setAnchorPoint(ccp(0.5f,0.5f));
// 		imageView_head->setPosition(ccp(62,79));
// 		m_pUiLayer->addWidget(imageView_head);

		UILayer * u_layer_pos = UILayer::create();
		addChild(u_layer_pos);

		std::string str_postionPath = "res_ui/wujiang/";
		switch(index)
		{
		case 1:
			{
				str_postionPath.append("one");
			}
			break;
		case 2:
			{
				str_postionPath.append("two");
			}
			break;
		case 3:
			{
				str_postionPath.append("three");
			}
			break;
		case 4:
			{
				str_postionPath.append("four");
			}
			break;
		case 5:
			{
				str_postionPath.append("five");
			}
			break;
		}
		str_postionPath.append(".png");
		UIImageView * imageView_pos = UIImageView::create();
		imageView_pos->setTexture(str_postionPath.c_str());
		imageView_pos->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_pos->setPosition(ccp(21,140));
		u_layer_pos->addWidget(imageView_pos);

		RefreshAddedProperty();

		this->setContentSize(CCSizeMake(127,165));

		return true;
	}
	return false;
}

void StrategiesPositionItem::onEnter()
{
	UIScene::onEnter();
}

void StrategiesPositionItem::onExit()
{
	UIScene::onExit();
}

bool StrategiesPositionItem::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return false;
}

void StrategiesPositionItem::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void StrategiesPositionItem::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void StrategiesPositionItem::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void StrategiesPositionItem::FightStatusEvent( CCObject *pSender )
{
	if (genenralsId > 0)
	{
		if (fightStatus == GeneralsListBase::HoldTheLine)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)genenralsId,(void *)2);
		}
		else if (fightStatus == GeneralsListBase::InBattle)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)genenralsId,(void *)1);
		}
	}
	
}

void StrategiesPositionItem::HeadFrameEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsListForFightWaysUI) == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		if (isHaveGeneral)
		{
			OnStrategiesGeneralsInfoUI * onStrategiesGeneralsInfoUI = OnStrategiesGeneralsInfoUI::create(positionIndex,genenralsId);
			onStrategiesGeneralsInfoUI->ignoreAnchorPointForPosition(false);
			onStrategiesGeneralsInfoUI->setAnchorPoint(ccp(0.5f,0.5f));
			onStrategiesGeneralsInfoUI->setPosition(ccp(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(onStrategiesGeneralsInfoUI,0,kTagGeneralsListForFightWaysUI);
		}
		else
		{
			OnStrategiesGeneralsInfoUI * onStrategiesGeneralsInfoUI = OnStrategiesGeneralsInfoUI::create(positionIndex);
			onStrategiesGeneralsInfoUI->ignoreAnchorPointForPosition(false);
			onStrategiesGeneralsInfoUI->setAnchorPoint(ccp(0.5f,0.5f));
			onStrategiesGeneralsInfoUI->setPosition(ccp(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(onStrategiesGeneralsInfoUI,0,kTagGeneralsListForFightWaysUI);
		}

	}
}

void StrategiesPositionItem::RefreshAddedProperty()
{
	int fightwayLevel = 0;
	for(int i = 0;i<GeneralsUI::generalsStrategiesUI->generalsStrategiesList.size();i++)
	{
		if (GeneralsUI::generalsStrategiesUI->getCurSelectFightWayId() == GeneralsUI::generalsStrategiesUI->generalsStrategiesList.at(i)->fightwayid())
		{
			fightwayLevel = GeneralsUI::generalsStrategiesUI->generalsStrategiesList.at(i)->level();
			break;
		}
	}
	CFightWayGrowingUp * fightWayGrowingUp;
	if (fightwayLevel == 0)
	{
		fightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[GeneralsUI::generalsStrategiesUI->getCurSelectFightWayId()]->at(1);
	}
	else if (fightwayLevel == 10)
	{
		fightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[GeneralsUI::generalsStrategiesUI->getCurSelectFightWayId()]->at(10);
	}
	else
	{
		fightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[GeneralsUI::generalsStrategiesUI->getCurSelectFightWayId()]->at(fightwayLevel);
	}

	if (fightWayGrowingUp != NULL)
		switch(positionIndex)
	{
		case 1 :
			{
				std::string str_property1 = "generals_strategies_property_";
				char s_grid1_property[10];
				sprintf(s_grid1_property,"%d",fightWayGrowingUp->get_grid1_property());
				str_property1.append(s_grid1_property);
				const char *shuxing_1 = StringDataManager::getString(str_property1.c_str());
				std::string property1_value = shuxing_1;
				property1_value.append("+");
// 				if (fightwayLevel>0)
// 				{
					char s_grid1_value[10];
					sprintf(s_grid1_value,"%.f",fightWayGrowingUp->get_grid1_value());
					property1_value.append(s_grid1_value);
// 				}
// 				else
// 				{
// 					property1_value.append("0");
// 				}
				property1_value.append("%");
				l_additive->setText(property1_value.c_str());
			}
			break;
		case 2 :
			{
				std::string str_property2 = "generals_strategies_property_";
				char s_grid2_property[10];
				sprintf(s_grid2_property,"%d",fightWayGrowingUp->get_grid2_property());
				str_property2.append(s_grid2_property);
				const char *shuxing_2 = StringDataManager::getString(str_property2.c_str());
				std::string property2_value = shuxing_2;
				property2_value.append("+");
// 				if (fightwayLevel>0)
// 				{
					char s_grid2_value[10];
					sprintf(s_grid2_value,"%.f",fightWayGrowingUp->get_grid2_value());
					property2_value.append(s_grid2_value);
// 				}
// 				else
// 				{
// 					property2_value.append("0");
// 				}
				property2_value.append("%");
				l_additive->setText(property2_value.c_str());
			}
			break;
		case 3 :
			{
				std::string str_property3 = "generals_strategies_property_";
				char s_grid3_property[10];
				sprintf(s_grid3_property,"%d",fightWayGrowingUp->get_grid3_property());
				str_property3.append(s_grid3_property);
				const char *shuxing_3 = StringDataManager::getString(str_property3.c_str());
				std::string property3_value = shuxing_3;
				property3_value.append("+");
// 				if (fightwayLevel>0)
// 				{
					char s_grid3_value[10];
					sprintf(s_grid3_value,"%.f",fightWayGrowingUp->get_grid3_value());
					property3_value.append(s_grid3_value);
// 				}
// 				else
// 				{
// 					property3_value.append("0");
// 				}
				property3_value.append("%");
				l_additive->setText(property3_value.c_str());
			}
			break;
		case 4 :
			{
				std::string str_property4 = "generals_strategies_property_";
				char s_grid4_property[10];
				sprintf(s_grid4_property,"%d",fightWayGrowingUp->get_grid4_property());
				str_property4.append(s_grid4_property);
				const char *shuxing_4 = StringDataManager::getString(str_property4.c_str());
				std::string property4_value = shuxing_4;
				property4_value.append("+");
// 				if (fightwayLevel>0)
// 				{
					char s_grid4_value[10];
					sprintf(s_grid4_value,"%.f",fightWayGrowingUp->get_grid4_value());
					property4_value.append(s_grid4_value);
// 				}
// 				else
// 				{
// 					property4_value.append("0");
// 				}
				property4_value.append("%");
				l_additive->setText(property4_value.c_str());
			}
			break;
		case 5 :
			{
				std::string str_property5 = "generals_strategies_property_";
				char s_grid5_property[10];
				sprintf(s_grid5_property,"%d",fightWayGrowingUp->get_grid5_property());
				str_property5.append(s_grid5_property);
				const char *shuxing_5 = StringDataManager::getString(str_property5.c_str());
				std::string property5_value = shuxing_5;
				property5_value.append("+");
// 				if (fightwayLevel>0)
// 				{
					char s_grid5_value[10];
					sprintf(s_grid5_value,"%.f",fightWayGrowingUp->get_grid5_value());
					property5_value.append(s_grid5_value);
// 				}
// 				else
// 				{
// 					property5_value.append("0");
// 				}
				property5_value.append("%");
				l_additive->setText(property5_value.c_str());
			}
			break;
	}
}

void StrategiesPositionItem::RefreshGeneralsStatus( int _status )
{
	fightStatus = _status;
	//CCDictionary *strings = CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
	if (fightStatus == GeneralsListBase::InBattle)
	{
		//const char *str1  = ((CCString*)strings->objectForKey("generals_fighttype_chuzhan"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("generals_fighttype_chuzhan");
		char* p1 =const_cast<char*>(str1);
		l_fightStatus->setText(p1);

		btn_fightStatus->setTextures("res_ui/new_button_000.png","res_ui/new_button_000.png","");
	}
	else if(fightStatus == GeneralsListBase::HoldTheLine ||fightStatus == GeneralsListBase::NoBattle)
	{
		//const char *str1  = ((CCString*)strings->objectForKey("generals_fighttype_luezhen"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("generals_fighttype_luezhen");
		char* p1 =const_cast<char*>(str1);
		l_fightStatus->setText(p1);

		btn_fightStatus->setTextures("res_ui/new_button_00.png","res_ui/new_button_00.png","");
	}
}
