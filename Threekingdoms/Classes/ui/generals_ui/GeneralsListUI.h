#ifndef  _GENERALSUI_GENERALSLISTUI_H_
#define _GENERALSUI_GENERALSLISTUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "GeneralsListBase.h"
#include "GeneralsSkillsUI.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralBaseMsg;
class CGeneralDetail;
class CShortCut;
class GeneralsShortcutLayer;
class GeneralsListCell;

/////////////////////////////////
/**
 * 武将界面下的“我的武将”中的武将列表类
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.3
 */

class GeneralsListUI : public GeneralsListBase,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	GeneralsListUI();
	~GeneralsListUI();
	static GeneralsListUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	//刷新武将列表
	void RefreshGeneralsList();
	//刷新武将列表(tableView 的偏移量不变)
	void RefreshGeneralsListWithOutChangeOffSet();

	void RefreshGeneralsLayerData();

	CGeneralBaseMsg* getCurGeneralBaseMsg();

	//添加聊天泡泡
	void addChatBubble(const char* text, float duration);

	//教学
	std::string m_content;
	CCPoint m_pos;
	CCTutorialIndicator::Direction m_direction;
	int mTutorialScriptInstanceId;

	//第一次教学还是第二次教学
	bool isFirstTutorial;
	bool isSecondTutorial;
	//教学里的第一个按钮（上阵）还是第二个按钮（出战）
	bool isFirstOrSecondMenu;
	//上阵/出战
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction,bool firstTutorialOrSecond,bool firstMenuOrSecond);
	void addCCTutorialIndicator2(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction,bool firstOrSecond);
	void removeCCTutorialIndicator();
	virtual void registerScriptCommand(int scriptId);
	

private:
	UIPanel * panel_generalsList; //武将列表面板
public:
	UILayer * Layer_generalsList;
	UILayer * Layer_generalsTop;  //位于武将列表层之上

	int lastSelectCellId;
	int selectCellId;

	bool isInFirstGeneralTutorial;
	/******************************************** 武将列表界面*********************************************/
private:

	cocos2d::extension::UIButton *	Btn_evolution; //进化
	cocos2d::extension::UIButton *	Btn_teach ; //传功
	cocos2d::extension::UILabel *	l_hp ; //血量值
	cocos2d::extension::UIImageView * Image_hp;
	cocos2d::extension::UILabel *	l_mp ; //法力值
	cocos2d::extension::UIImageView * Image_mp;
	cocos2d::extension::UILabel *	l_exp ; //经验值
	cocos2d::extension::UIImageView * Image_exp;
	cocos2d::extension::UILabel *	l_profession; //职业
	cocos2d::extension::UILabel *	l_combatPower ; //战力
	cocos2d::extension::UILabel *	l_inBattleTime ; //出战时间
	cocos2d::extension::UILabel *	l_restTime ; //冷却时间
	cocos2d::extension::UILabel * l_powerValue;//力量
	cocos2d::extension::UILabel * l_aglieValue;  //敏捷
	cocos2d::extension::UILabel * l_intelligenceValue;  //智力
	cocos2d::extension::UILabel * l_focusValue;  //专注
	cocos2d::extension::UILabel * l_phyAttackValue;
	cocos2d::extension::UILabel * l_magicAttackValue;
	cocos2d::extension::UILabel * l_phyDenValue;
	cocos2d::extension::UILabel * l_magicDenValue;
	cocos2d::extension::UILabel * l_hitValue;  //命中
	cocos2d::extension::UILabel * l_dodgeValue; //闪避
	cocos2d::extension::UILabel * l_critValue;  //暴击
	cocos2d::extension::UILabel * l_critDamageValue;//暴击伤害
	cocos2d::extension::UILabel * l_atkSpeedValue;
	cocos2d::extension::UIButton * Btn_show_general;	// 展示
	cocos2d::extension::UIButton * Btn_hide_general;	// 收回

	cocos2d::extension::UIButton * btn_fate_1;
	cocos2d::extension::UIButton * btn_fate_2;
	cocos2d::extension::UIButton * btn_fate_3;
	cocos2d::extension::UIButton * btn_fate_4;
	cocos2d::extension::UIButton * btn_fate_5;
	cocos2d::extension::UIButton * btn_fate_6;
	cocos2d::extension::UILabel * l_fate_1;
	cocos2d::extension::UILabel * l_fate_2;
	cocos2d::extension::UILabel * l_fate_3;
	cocos2d::extension::UILabel * l_fate_4;
	cocos2d::extension::UILabel * l_fate_5;
	cocos2d::extension::UILabel * l_fate_6;

	void EvolutionEvent(CCObject * pSender);
	void TeachEvent(CCObject * pSender);
	void FateEvent(CCObject * pSender);

	void ShowGeneralEvent(CCObject * pSender);
	void HideGeneralEvent(CCObject * pSender);

public:
	GeneralsListCell * firstCell;
	GeneralsListCell * secondCell;
	//void setSkillTypeByGeneralsDetail(CGeneralDetail *generalsDetail);
public:

	virtual bool isVisible();
	virtual void setVisible(bool visible);

	//刷新武将详细信息
	void RefreshGeneralsInfo(CGeneralDetail * generalDetail);
	//刷新武将缘分
	void RefreshGeneralsFate(CGeneralDetail * generalDetail);
	//刷新武将技能
	void RefreshGeneralsSkill(CGeneralDetail * generalDetail);
	//刷新武将技能(单个快捷栏)
	void RefreshOneGeneralsSkillSlot(long long generalsId,CShortCut * shortCut);
	//刷新武将技能(所有快捷栏)
	void RefreshAllGeneralsSkillSlot(int generalsId,std::vector<CShortCut*> shortcutList);
	//刷新武将是否可以进化
	void RefreshEvolutionEnabled(CGeneralDetail * generalDetail);

	// 武将列表信息(武将列表界面)
	//std::vector<CGeneralBaseMsg * > generalBaseMsgList;

	CCTableView * generalList_tableView;

	CGeneralBaseMsg * curGeneralBaseMsg; 
	CGeneralDetail * curGeneralDetail;

	GeneralsShortcutLayer * generalsShortcutLayer;

};

#endif

