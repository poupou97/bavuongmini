
#ifndef _GENERALSUI_NORMALGENERALSHEADITEM_H_
#define _GENERALSUI_NORMALGENERALSHEADITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "GeneralsHeadItemBase.h"

USING_NS_CC;
USING_NS_CC_EXT;
/////////////////////////////////
/**
 * 武将界面下的 普通武将头像类
 * 增加了等级和军衔
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.4
 */
class CGeneralBaseMsg;
class CGeneralDetail;

class NormalGeneralsHeadItemBase :public GeneralsHeadItemBase
{
public:
	NormalGeneralsHeadItemBase();
	~NormalGeneralsHeadItemBase();

	static NormalGeneralsHeadItemBase * create(CGeneralBaseMsg * generalBaseMsg);
	bool initWithGeneralBaseMsg(CGeneralBaseMsg * generalBaseMsg);

	static NormalGeneralsHeadItemBase * create(int generalModleId);
	bool initWithGeneralId(int generalModleId);

	static NormalGeneralsHeadItemBase * create(CGeneralDetail * generalDetail);
	bool initWithGeneralDetail(CGeneralDetail * generalDetail);

	void playAnmOfEvolution();

protected:
	UILayer * u_layer;
};

#endif