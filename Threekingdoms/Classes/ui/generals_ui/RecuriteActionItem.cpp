#include "RecuriteActionItem.h"
#include "../../messageclient/element/CActionDetail.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"
#include "../../GameView.h"


RecuriteActionItem::RecuriteActionItem():
lastChangeTime(0)
{
}


RecuriteActionItem::~RecuriteActionItem()
{
	delete curActionDetail;
}

RecuriteActionItem * RecuriteActionItem::create(CActionDetail * actionDetail)
{
	RecuriteActionItem * recuriteActionItem = new RecuriteActionItem();
	if (recuriteActionItem && recuriteActionItem->init(actionDetail))
	{
		recuriteActionItem->autorelease();
		return recuriteActionItem;
	}
	CC_SAFE_DELETE(recuriteActionItem);
	return NULL;
}

bool RecuriteActionItem::init(CActionDetail * actionDetail)
{
	if (UIScene::init())
	{
		curActionDetail = new CActionDetail();
		curActionDetail->CopyFrom(*actionDetail);
		//this->lastChangeTime = GameUtils::millisecondNow();
		cdTime =  actionDetail->cdtime();
		/*m_fRecordTime = m_fTime =   GameUtils::millisecondNow() - actionDetail->cdtime();*/
		playerRemainNumber = curActionDetail->playerremainnumber();

	    retainImage = UIImageView::create();
		retainImage->setTexture("res_ui/wujiang/jin.png");
		retainImage->setAnchorPoint(ccp(0,0));
		retainImage->setPosition(ccp(18,62));
		m_pUiLayer->addWidget(retainImage);
		label_num = UILabel::create();
		label_num->setText("2/5");
		label_num->setFontName(APP_FONT_NAME);
		label_num->setFontSize(14);
		label_num->setColor(ccc3(47,93,13));
		label_num->setAnchorPoint(ccp(0.5f,0.5f));
		label_num->setPosition(ccp(179,72));
		m_pUiLayer->addWidget(label_num);

		freeImage = UIImageView::create();
		freeImage->setTexture("res_ui/wujiang/ben.png");
		freeImage->setAnchorPoint(ccp(0,0));
		freeImage->setPosition(ccp(59,62));
		m_pUiLayer->addWidget(freeImage);

	    label_time = UILabel::create();
		label_time->setText("12:25:45");
		label_time->setFontName(APP_FONT_NAME);
		label_time->setFontSize(14);
		label_time->setColor(ccc3(47,93,13));
		label_time->setAnchorPoint(ccp(0.5f,0.5f));
		label_time->setPosition(ccp(75,84));
		m_pUiLayer->addWidget(label_time);
		freeBefore = UIImageView::create();
		freeBefore->setTexture("res_ui/wujiang/hou.png");
		freeBefore->setAnchorPoint(ccp(0,0));
		freeBefore->setPosition(ccp(125,73));
		m_pUiLayer->addWidget(freeBefore);
		image_gold = UIImageView::create();
		image_gold->setTexture("res_ui/ingot.png");
		image_gold->setAnchorPoint(ccp(0,0));
		image_gold->setPosition(ccp(82,52));
		m_pUiLayer->addWidget(image_gold);
		label_gold = UILabel::create();
		char s_gold[20];
		sprintf(s_gold,"%d",actionDetail->gold());
		label_gold->setText(s_gold);
		label_gold->setFontName(APP_FONT_NAME);
		label_gold->setFontSize(14);
		label_gold->setColor(ccc3(47,93,13));
		label_gold->setAnchorPoint(ccp(0,0));
		label_gold->setPosition(ccp(121,55));
		m_pUiLayer->addWidget(label_gold);

		image_noFreeTime = UIImageView::create();
		image_noFreeTime->setTexture("res_ui/wujiang/jina.png");
		image_noFreeTime->setAnchorPoint(ccp(0,0));
		image_noFreeTime->setPosition(ccp(40,73));
		m_pUiLayer->addWidget(image_noFreeTime);

		retainImage->setVisible(false);
		label_num->setVisible(false);
		freeImage->setVisible(false);
		label_time->setVisible(false);
		freeBefore->setVisible(false);
		image_gold->setVisible(false);
		label_gold->setVisible(false);
		image_noFreeTime->setVisible(false);

		if (actionDetail->show1rare()>0)
		{
			UIImageView * star_normal = UIImageView::create();
			star_normal->setTexture(getStarPathByNum(actionDetail->show1rare()).c_str());
			star_normal->setPosition(ccp(71,125));
			m_pUiLayer->addWidget(star_normal);
		}

		if (actionDetail->show2rare()>0)
		{
			UIImageView * star_good = UIImageView::create();
			star_good->setTexture(getStarPathByNum(actionDetail->show2rare()).c_str());
			star_good->setPosition(ccp(120,125));
			m_pUiLayer->addWidget(star_good);
		}

		if (actionDetail->show3rare()>0)
		{
			UIImageView * star_wonderful = UIImageView::create();
			star_wonderful->setTexture(getStarPathByNum(actionDetail->show3rare()).c_str());
			star_wonderful->setPosition(ccp(171,125));
			m_pUiLayer->addWidget(star_wonderful);
		}

		this->setContentSize(CCSizeMake(215,207));

		refreshData();
		this->schedule(schedule_selector(RecuriteActionItem::update),1.0f);
		return true;
	}
	return false;
}

void RecuriteActionItem::update( float dt )
{
// 	    cdTime -= 1000;
// 		m_fRecordTime = cdTime;
// 		if (m_fRecordTime < 0)
// 		{
// 			m_fRecordTime = 0;
// 		}

		refreshData();
}

void RecuriteActionItem::refreshData()
{
	MainScene * mainscene_ =(MainScene *)GameView::getInstance()->getMainUIScene();
	for(int i = 0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		CActionDetail * tempActionDetail = GameView::getInstance()->actionDetailList.at(i);
		if (tempActionDetail->type() == curActionDetail->type())
		{
			if (tempActionDetail->type() == BASE)
			{
				actionMaxNumber = tempActionDetail->actionmaxnumber();
				playerRemainNumber = tempActionDetail->playerremainnumber();
				std::string str_des = "";
				char s_playerRemainNumber[10];
				sprintf(s_playerRemainNumber,"%d",tempActionDetail->playerremainnumber());
				str_des.append(s_playerRemainNumber);
				str_des.append("/");
				char s_actionMaxNumber[10];
				sprintf(s_actionMaxNumber,"%d",actionMaxNumber);
				str_des.append(s_actionMaxNumber);
				label_num->setText(str_des.c_str());

				if (playerRemainNumber>0)   /********第N次免费抽********/
				{
					if (tempActionDetail->cdtime() > 0)   /********花费元宝抽********/
					{
						state = Gold;
						retainImage->setVisible(false);
						label_num->setVisible(false);
						freeImage->setVisible(false);
						label_time->setVisible(true);
						label_time->setText(timeFormatToString(tempActionDetail->cdtime()/1000).c_str());
						freeBefore->setVisible(true);
						image_gold->setVisible(true);
						label_gold->setVisible(true);
						image_noFreeTime->setVisible(false);
					}
					else
					{
						state = Free;
						retainImage->setVisible(true);
						label_num->setVisible(true);
						freeImage->setVisible(false);
						label_time->setVisible(false);
						freeBefore->setVisible(false);
						image_gold->setVisible(false);
						label_gold->setVisible(false);
						image_noFreeTime->setVisible(false);
					}
				}
				else/********花费元宝抽********/
				{
					state = Gold;
					retainImage->setVisible(false);
					label_num->setVisible(false);
					freeImage->setVisible(false);
					label_time->setVisible(false);
					freeBefore->setVisible(false);
					image_gold->setVisible(true);
					label_gold->setVisible(true);
					image_noFreeTime->setVisible(true);
				}
			}
			else
			{
				if (tempActionDetail->cdtime() > 0)   /********花费元宝抽********/
				{
					state = Gold;
					retainImage->setVisible(false);
					label_num->setVisible(false);
					freeImage->setVisible(false);
					label_time->setVisible(true);
					label_time->setText(timeFormatToString(tempActionDetail->cdtime()/1000).c_str());
					freeBefore->setVisible(true);
					image_gold->setVisible(true);
					label_gold->setVisible(true);
					image_noFreeTime->setVisible(false);
				}
				else
				{
					state = Free;                                                   /********免费抽1次********/
					retainImage->setVisible(false);
					label_num->setVisible(false);
					freeImage->setVisible(true);
					label_time->setVisible(false);
					freeBefore->setVisible(false);
					image_gold->setVisible(false);
					label_gold->setVisible(false);
					image_noFreeTime->setVisible(false);
				}
			}
		}
	}
 
}

std::string RecuriteActionItem::timeFormatToString( long long t )
{
	int int_h = t/60/60;
	int int_m = (t%3600)/60;
	int int_s = (t%3600)%60;

	//时
	std::string timeString = "";
	char str_h[10];
	sprintf(str_h,"%d",int_h);
	if (int_h <= 0)
	{

	}
	else if (int_h > 0 && int_h < 10)
	{
		timeString.append("0");
		timeString.append(str_h);
		timeString.append(":");
	}
	else
	{
		timeString.append(str_h);
		timeString.append(":");
	}
	
	//分
	char str_m[10];
	sprintf(str_m,"%d",int_m);
	if (int_m >= 0 && int_m < 10)
	{
		timeString.append("0");
		timeString.append(str_m);
	}
	else
	{
		timeString.append(str_m);
	}
	timeString.append(":");

	char str_s[10];
	sprintf(str_s,"%d",int_s);
	if (int_s >= 0 && int_s < 10)
	{
		timeString.append("0");
		timeString.append(str_s);
	}
	else
	{
		timeString.append(str_s);
	}

	return timeString;
}

std::string RecuriteActionItem::getStarPathByNum( int num )
{
	std::string path;
	if (num == 1)
	{
		path = "res_ui/wujiang/star_one.png";
	}
	else if (num == 2)
	{
		path = "res_ui/wujiang/star_two.png";
	}
	else if (num == 3)
	{
		path = "res_ui/wujiang/star_three.png";
	}
	else if (num == 4)
	{
		path = "res_ui/wujiang/star_four.png";
	}
	else if (num == 5)
	{
		path = "res_ui/wujiang/star_five.png";
	}
	else
	{
		path = "res_ui/wujiang/star_one.png";
	}

	return path;
}
