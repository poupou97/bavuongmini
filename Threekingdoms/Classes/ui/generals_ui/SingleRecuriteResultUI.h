#ifndef  _GENERALSUI_SINGLERECURITERESULTUI_H_
#define _GENERALSUI_SINGLERECURITERESULTUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "GeneralsListBase.h"
#include "../extensions/UIScene.h"
#include "GeneralsSkillsUI.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralBaseMsg;
class CGeneralDetail;
class CActionDetail;

/////////////////////////////////
/**
 * 武将界面下的“点将台”中的单次招募结果类
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.9
 */

class SingleRecuriteResultUI : public UIScene
{
public:
	SingleRecuriteResultUI();
	~SingleRecuriteResultUI();

	static SingleRecuriteResultUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void CloseEvent(CCObject * pSender);

	//返回（关闭招募成功界面）
	void RecruiteBackEvent(CCObject *pSender);
	//再来一次（继续招募）
	void RecruiteAgainEvent(CCObject *pSender);

	//刷新按钮的显示与否
	void RefreshBtnPresent(bool isPresent);
	//刷新招募成功界面的花费
	void RefreshRecruiteCost(int actionType);
	//刷新招募成功界面的属性信息
	void RefreshRecruiteResult(CGeneralDetail * generalDetail);
	//刷新武将缘分
	void RefreshGeneralsFate(CGeneralDetail * generalDetail);
	//刷新武将技能
	void RefreshGeneralsSkill(CGeneralDetail * generalDetail);
	//刷新武将头像
	void RefreshGeneralsHead(CGeneralDetail * generalDetail);

	void FateEvent(CCObject *pSender);

	void setSkillTypeByGeneralsDetail(CGeneralDetail *generalsDetail);

	//教学
	virtual void registerScriptCommand(int scriptId);
	//关闭当前界面
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();

	void createParticleSys();

public: 
	UIPanel * panel_generalsInfo;//武将详细信息(单次成功招募后)
	UILayer * Layer_generalsInfo;

	UIPanel * panel_btn;

	UIButton * Btn_close;
	CGeneralDetail * curGeneralDetail;
private:
	CCPoint slotPos[5] ;

	UILabel *	l_result_profession; //职业
	UILabel *	l_result_combatPower ; //战力
	UILabel *	l_result_hp ; //血量值
	UIImageView * Image_result_hp;
	UILabel *	l_result_mp ; //法力值
	UIImageView * Image_result_mp;
	UILabel * l_result_powerValue;//力量
	UILabel * l_result_aglieValue;  //敏捷
	UILabel * l_result_intelligenceValue;  //智力
	UILabel * l_result_focusValue;  //专注
	UILabel * l_result_phyAttackValue;
	UILabel * l_result_magicAttackValue;
	UILabel * l_result_phyDenValue;
	UILabel * l_result_magicDenValue;
	UILabel * l_result_hitValue;  //命中
	UILabel * l_result_dodgeValue; //闪避
	UILabel * l_result_critValue;  //暴击
	UILabel * l_result_critDamageValue;//暴击伤害
	UILabel * l_result_atkSpeedValue;

	UIButton * btn_fate_1;
	UIButton * btn_fate_2;
	UIButton * btn_fate_3;
	UIButton * btn_fate_4;
	UIButton * btn_fate_5;
	UIButton * btn_fate_6;
	UILabel * l_fate_1;
	UILabel * l_fate_2;
	UILabel * l_fate_3;
	UILabel * l_fate_4;
	UILabel * l_fate_5;
	UILabel * l_fate_6;

	UILabel * lable_singleCost_gold;

	GeneralsSkillsUI::SkillTpye curSkillType;

private:
	//教学
	int mTutorialScriptInstanceId;
};

#endif

