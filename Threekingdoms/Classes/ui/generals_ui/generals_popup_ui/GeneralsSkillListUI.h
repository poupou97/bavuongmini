
#ifndef _GENERALSPOPUPUI_GENERALSSKILLLISTUI_H_
#define _GENERALSPOPUPUI_GENERALSSKILLLISTUI_H_

class GameFightSkill;

#include "../../extensions/UIScene.h"
#include "../GeneralsSkillsUI.h"

class GeneralsSkillListUI : public UIScene,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	GeneralsSkillListUI();
	~GeneralsSkillListUI();

	struct ReqData{
		long long generalsid ;
		std::string skillid ;
		int index;
	};

	static GeneralsSkillListUI * create(GeneralsSkillsUI::SkillTpye skill_type,int index);
	bool init(GeneralsSkillsUI::SkillTpye skill_type,int index );

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject *pSender);
	//装配
	void EquipEvent(CCObject *pSender);

	void RefreshSkillInfo(GameFightSkill * gameFightSkill);

	void UpdateDataByType(GeneralsSkillsUI::SkillTpye s_t,int index);
	void showSkillDes(std::string curSkillDes,std::string nextSkillDes);

	void NullSkillDesEvent(CCObject * pSender);
public: 
	std::vector<GameFightSkill *>DataSourceList;
	CCTableView * generalsSkillsList_tableView;
	UIImageView* i_skillFrame;
	UIImageView* i_skillImage;
	UILabel* l_skillName;
	UILabel* l_skillType;
	UILabel* l_skillLvValue ;
	UIImageView *imageView_skillQuality;
	UILabel* l_skillQualityValue;
	UILabel* l_skillMpValue;
	UILabel* l_skillCDTimeValue ;
	UILabel* l_skillDistanceValue;
	//UITextArea * t_skillDescription;
	//UITextArea * t_skillNextDescription;

private:
	UILayer * Layer_skills;
	UILayer * Layer_upper;
	std::string curSkillId;
	int curSlotIndex;
};

#endif