
#ifndef _GENERALSPOPUPUI_GENERALSFATEINFO_H_
#define _GENERALSPOPUPUI_GENERALSFATEINFO_H_

#include "../../extensions/UIScene.h"
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralDetail;

class GeneralsFateInfoUI : public UIScene
{
public:
	GeneralsFateInfoUI();
	~GeneralsFateInfoUI();

	static GeneralsFateInfoUI * create(CGeneralDetail * generalsDetail);
	bool init(CGeneralDetail * generalsDetail);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void RefreshFateInfo(CGeneralDetail * generalsDetail);

private:
	UIPanel *ppanel;

	UILabel * l_fateName_1;
	UILabel * l_fateName_2;
	UILabel * l_fateName_3;
	UILabel * l_fateName_4;
	UILabel * l_fateName_5;

	CCLabelTTF * l_fateDes_1;
	CCLabelTTF * l_fateDes_2;
	CCLabelTTF * l_fateDes_3;
	CCLabelTTF * l_fateDes_4;
	CCLabelTTF * l_fateDes_5;

private:
	void CloseEvent(CCObject * pSender);
};

#endif