#ifndef _GENETALSPOPUPUI_GENERALSSKILLUI_H_
#define _GENETALSPOPUPUI_GENERALSSKILLUI_H_

#include "../../extensions/UIScene.h"
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../GeneralsSkillsUI.h"
#include "../../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class GameFightSkill;

class GeneralsSkillInfoUI : public UIScene
{
public:
	GeneralsSkillInfoUI();
	~GeneralsSkillInfoUI();

	static GeneralsSkillInfoUI * create(GameFightSkill * gameFightSkill,int slotIndex);
	bool init(GameFightSkill * gameFightSkill,int slotIndex);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void setSkillProfession(GeneralsSkillsUI::SkillTpye skill_prof);
	GeneralsSkillsUI::SkillTpye getSkillProfession();

	void RefreshSkillInfo(GameFightSkill * gameFightSkill);

private:
	std::string getProfessionBySkill(std::string skillid);

	void CloseEvent(CCObject *pSender);
	void SuperSkillEvent(CCObject *pSender); 
	void LvUpEvent(CCObject *pSender); 
	void ReplaceEvent(CCObject *pSender); 

public:
	std::string curSkillId;
	int skillSlotIndex;
	//无双按钮
	UIButton * btn_superSkill;

private:
	UIPanel * ppanel;
	UIScrollView * m_scrollView;

	//当前界面属于的武将职业技能类型
	GeneralsSkillsUI::SkillTpye skill_profession;
	
	//下一级描述
	CCLabelTTF * l_nextLv;
	//下一级
	CCLabelTTF * t_nextLvDes;
	//升级需要
	CCSprite * s_upgradeNeed;
	CCLabelTTF * l_upgradeNeed;
	CCSprite * s_second_line;
	//人物等级
	CCLabelTTF * l_playerLv;
	CCLabelTTF * l_playerLvValue;
	//技能点
	CCLabelTTF * l_skillPoint;
	CCLabelTTF * l_skillPointValue;
	//前置技能
	CCLabelTTF * l_preSkill;
	CCLabelTTF * l_preSkillValue;
	//金币
	CCLabelTTF * l_gold;
	CCLabelTTF * l_goldValue;
	//道具
	CCLabelTTF * l_prop;
	CCLabelTTF * l_propValue;

public:
	//教学
	virtual void registerScriptCommand(int scriptId);
	//龙魂按钮
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();
	//教学
	int mTutorialScriptInstanceId;
};

#endif
