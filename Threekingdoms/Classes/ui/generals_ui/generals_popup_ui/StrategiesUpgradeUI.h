
#ifndef _GENERALSPOPUPUI_STRATEGIESUPGRADEUI_H_
#define _GENERALSPOPUPUI_STRATEGIESUPGRADEUI_H_

#include "../../extensions/UIScene.h"
#include "../../../legend_script/CCTutorialIndicator.h"

class CFightWayBase;

class StrategiesUpgradeUI : public UIScene
{
public:
	StrategiesUpgradeUI();
	~StrategiesUpgradeUI();

	static StrategiesUpgradeUI * create(int selectFightWayId);
	bool init(int selectFightWayId);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void UpgradeEvent(CCObject *pSender);
	void CloseEvent(CCObject *pSender);

	void RefreshPropertyInfo(CFightWayBase * fightWayBase);
	void RefreshButtonStatus();

private:
	UILabel * l_name;
	UILabel * l_level;
	UILabel * l_upgrade;
	UILabel * l_property1_l;
	UILabel * l_property1_r;
	UILabel * l_property2_l;
	UILabel * l_property2_r;
	UILabel * l_property3_l;
	UILabel * l_property3_r;
	UILabel * l_property4_l;
	UILabel * l_property4_r;
	UILabel * l_property5_l;
	UILabel * l_property5_r;
	UILabel * l_costGoldValue;
	UILabel * l_costConsumeValue;

public:
	CFightWayBase * curFightWayBase ;

	UIButton * Button_close;
	UIButton * Button_upgrade;

	//教学
	virtual void registerScriptCommand(int scriptId);
	//学习阵法
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	//关闭界面
	void addCCTutorialIndicator2(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

private:
	//教学
	int mTutorialScriptInstanceId;
};

#endif
