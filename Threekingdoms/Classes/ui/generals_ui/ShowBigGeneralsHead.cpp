
#include "ShowBigGeneralsHead.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../utils/StaticDataManager.h"
#include "GeneralsUI.h"
#include "GameView.h"
#include "RecuriteActionItem.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "AppMacros.h"

#define M_Frame_Scale 1.0f

#define KTag_FiveStar_Anm 345
#define kTagLegendAnm_Head 10  
#define kTagBeginAnm 20

ShowBigGeneralsHead::ShowBigGeneralsHead():
m_nCreateType(-1)
{
	m_pGeneralBaseMsg = new CGeneralBaseMsg();
}

ShowBigGeneralsHead::~ShowBigGeneralsHead()
{
	delete m_pGeneralBaseMsg;
}


void ShowBigGeneralsHead::onEnter()
{
	UIScene::onEnter();
}

void ShowBigGeneralsHead::onExit()
{
	UIScene::onExit();
}


ShowBigGeneralsHead * ShowBigGeneralsHead::create(CGeneralBaseMsg * generalBaseMsg)
{
	ShowBigGeneralsHead * showBigGeneralsHead = new ShowBigGeneralsHead();
	if (showBigGeneralsHead && showBigGeneralsHead->init(generalBaseMsg))
	{
		showBigGeneralsHead->autorelease();
		return showBigGeneralsHead;
	}
	CC_SAFE_DELETE(showBigGeneralsHead);
	return NULL;
}

ShowBigGeneralsHead * ShowBigGeneralsHead::create( int generalModleId)
{
	ShowBigGeneralsHead * showBigGeneralsHead = new ShowBigGeneralsHead();
	if (showBigGeneralsHead && showBigGeneralsHead->init(generalModleId))
	{
		showBigGeneralsHead->autorelease();
		return showBigGeneralsHead;
	}
	CC_SAFE_DELETE(showBigGeneralsHead);
	return NULL;
}

ShowBigGeneralsHead * ShowBigGeneralsHead::create( int generalModleId,int currentquality )
{
	ShowBigGeneralsHead * showBigGeneralsHead = new ShowBigGeneralsHead();
	if (showBigGeneralsHead && showBigGeneralsHead->init(generalModleId,currentquality))
	{
		showBigGeneralsHead->autorelease();
		return showBigGeneralsHead;
	}
	CC_SAFE_DELETE(showBigGeneralsHead);
	return NULL;
}

bool ShowBigGeneralsHead::init(CGeneralBaseMsg * generalBaseMsg)
{
	if (UIScene::init())
	{
		m_nCreateType = CT_withGeneralBaseMsg;
		m_pGeneralBaseMsg->CopyFrom(*generalBaseMsg);

		CGeneralBaseMsg * generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];

		//边框
		frame = UIImageView::create();
		frame->setTexture(getBigHeadFramePath(generalBaseMsg->currentquality()).c_str());
		frame->setAnchorPoint(CCPointZero);
		frame->setPosition(ccp(0,0));
		frame->setScale(M_Frame_Scale);
		m_pUiLayer->addWidget(frame);

		//头像
		std::string icon_path = "res_ui/general/";
		icon_path.append(generalMsgFromDb->get_half_photo());
		icon_path.append(".png");
		imageView_head = UIImageView::create();
		imageView_head->setTexture(icon_path.c_str());
		imageView_head->setAnchorPoint(ccp(0.5f,0));
		imageView_head->setPosition(ccp(frame->getSize().width/2,64));
		m_pUiLayer->addWidget(imageView_head);

		//名字
		label_name = UILabel::create();
		label_name->setText(generalMsgFromDb->name().c_str());
		label_name->setFontSize(20);
		label_name->setAnchorPoint(ccp(0.5f,0.5f));
		label_name->setAnchorPoint(ccp(0.5f,0.5f));
		label_name->setPosition(ccp(frame->getSize().width/2,42));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		m_pUiLayer->addWidget(label_name);

		//稀有度
		if (generalBaseMsg->rare()>0)
		{
			imageView_star = UIImageView::create();
			imageView_star->setTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(CCPointZero);
			imageView_star->setScale(0.8f);
			imageView_star->setPosition(ccp(6,27));
			imageView_star->setName("ImageView_Star");
			m_pUiLayer->addWidget(imageView_star);

			if (generalBaseMsg->rare() >= 5)
			{
				// load animation
				std::string animFileName = "animation/ui/wj/wj2.anm";
				CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
				m_pAnim->setPlayLoop(true);
				m_pAnim->setReleaseWhenStop(false);
				m_pAnim->setScale(.8f);
				m_pAnim->setPosition(ccp(6,27));
				m_pAnim->setPlaySpeed(.35f);
				m_pAnim->setTag(KTag_FiveStar_Anm);
				m_pUiLayer->addChild(m_pAnim);
			}
		}

		this->setContentSize(CCSizeMake(frame->getSize().width*M_Frame_Scale,frame->getSize().height*M_Frame_Scale));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}

bool ShowBigGeneralsHead::init( int generalModleId )
{
	if (UIScene::init())
	{
		m_nCreateType = CT_withModleId;
		m_nGeneralModleId = generalModleId;

		CGeneralBaseMsg* generalBaseMsg = GeneralsConfigData::s_generalsBaseMsgData[generalModleId];
		//边框
		frame = UIImageView::create();
		frame->setTexture(getBigHeadFramePath(1).c_str());    //默认是1级（1-10为白色）
		frame->setAnchorPoint(CCPointZero);
		frame->setPosition(ccp(0,0));
		frame->setScale(M_Frame_Scale);
		m_pUiLayer->addWidget(frame);

		//名字
		label_name = UILabel::create();
		label_name->setText(generalBaseMsg->name().c_str());
		label_name->setFontSize(20);
		label_name->setAnchorPoint(ccp(0.5f,0.5f));
		label_name->setAnchorPoint(ccp(0.5f,0.5f));
		label_name->setPosition(ccp(frame->getSize().width/2,42));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(1));
		m_pUiLayer->addWidget(label_name);
		//头像
		std::string icon_path = "res_ui/general/";
		icon_path.append(generalBaseMsg->get_half_photo());
		//icon_path.append("guanyu");
		icon_path.append(".png");
		imageView_head = UIImageView::create();
		imageView_head->setTexture(icon_path.c_str());
		imageView_head->setAnchorPoint(ccp(0.5f,0));
		imageView_head->setPosition(ccp(frame->getSize().width/2,64));
		m_pUiLayer->addWidget(imageView_head);

		//稀有度
		if (generalBaseMsg->rare()>0)
		{
			imageView_star = UIImageView::create();
			imageView_star->setTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(CCPointZero);
			imageView_star->setScale(1.0f);
			imageView_star->setPosition(ccp(15,63));
			imageView_star->setName("ImageView_Star");
			m_pUiLayer->addWidget(imageView_star);

			if (generalBaseMsg->rare() >= 5)
			{
				// load animation
				std::string animFileName = "animation/ui/wj/wj2.anm";
				CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
				m_pAnim->setPlayLoop(true);
				m_pAnim->setReleaseWhenStop(true);
				m_pAnim->setScale(1.0f);
				m_pAnim->setPosition(ccp(15,63));
				m_pAnim->setPlaySpeed(.35f);
				m_pAnim->setTag(KTag_FiveStar_Anm);
				m_pUiLayer->addChild(m_pAnim);
			}
		}

		this->setContentSize(CCSizeMake(frame->getSize().width*M_Frame_Scale,frame->getSize().height*M_Frame_Scale));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}

bool ShowBigGeneralsHead::init( int generalModleId,int currentquality )
{
	if (UIScene::init())
	{
		m_nCreateType = CT_withModleIdAndQuality;
		m_nGeneralModleId = generalModleId;
		m_nCurrentquality = currentquality;

		CGeneralBaseMsg* generalBaseMsg = GeneralsConfigData::s_generalsBaseMsgData[generalModleId];
		//边框
		frame = UIImageView::create();
		frame->setTexture(getBigHeadFramePath(currentquality).c_str());    //（1-10为白色）
		frame->setAnchorPoint(CCPointZero);
		frame->setPosition(ccp(0,0));
		frame->setScale(M_Frame_Scale);
		m_pUiLayer->addWidget(frame);

		//名字
		label_name = UILabel::create();
		label_name->setText(generalBaseMsg->name().c_str());
		label_name->setFontName(APP_FONT_NAME);
		label_name->setFontSize(20);
		label_name->setAnchorPoint(ccp(0.5f,0.5f));
		label_name->setPosition(ccp(frame->getSize().width/2,42));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(1));
		m_pUiLayer->addWidget(label_name);
		//头像
 		std::string icon_path = "res_ui/general/";
 		icon_path.append(generalBaseMsg->get_half_photo());
 		//icon_path.append("guanyu");
 		icon_path.append(".png");
 		imageView_head = UIImageView::create();
 		imageView_head->setTexture(icon_path.c_str());
 		imageView_head->setAnchorPoint(ccp(0.5f,0));
 		imageView_head->setPosition(ccp(frame->getSize().width/2,64));
 		m_pUiLayer->addWidget(imageView_head);

		//稀有度
		if (generalBaseMsg->rare()>0)
		{
			imageView_star = UIImageView::create();
			imageView_star->setTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(CCPointZero);
			imageView_star->setScale(1.0f);
			imageView_star->setPosition(ccp(15,63));
			imageView_star->setName("ImageView_Star");
			m_pUiLayer->addWidget(imageView_star);

			if (generalBaseMsg->rare() >= 5)
			{
				// load animation
				std::string animFileName = "animation/ui/wj/wj2.anm";
				CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
				m_pAnim->setPlayLoop(true);
				m_pAnim->setReleaseWhenStop(true);
				m_pAnim->setScale(1.0f);
				m_pAnim->setPosition(ccp(15,63));
				m_pAnim->setPlaySpeed(.35f);
				m_pAnim->setTag(KTag_FiveStar_Anm);
				m_pUiLayer->addChild(m_pAnim);
			}
		}

		this->setContentSize(CCSizeMake(frame->getSize().width*M_Frame_Scale,frame->getSize().height*M_Frame_Scale));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}

bool ShowBigGeneralsHead::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	if (this->getActionByTag(kTagBeginAnm))
	{
		return true;
	}

	return resignFirstResponder(touch,this,false);
}

void ShowBigGeneralsHead::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void ShowBigGeneralsHead::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void ShowBigGeneralsHead::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}


std::string ShowBigGeneralsHead::getBigHeadFramePath( int quality )
{
	std::string frameNameBase ;
	if (quality < 11)                                                                //白色
	{
		frameNameBase = BigGeneralsFramePath_White;
	}
	else if (quality>10 && quality < 21)                              //绿色
	{
		frameNameBase = BigGeneralsFramePath_Green;
	}
	else if (quality>20 && quality < 31)                              //蓝色
	{
		frameNameBase = BigGeneralsFramePath_Blue;
	}
	else if (quality>30 && quality < 41)                              //紫色
	{
		frameNameBase = BigGeneralsFramePath_Purpe;
	}
	else if (quality>40 && quality < 51)                              //橙色
	{
		frameNameBase = BigGeneralsFramePath_Orange;
	}
	else
	{
		frameNameBase = BigGeneralsFramePath_White;
	}
	return frameNameBase;
}

void ShowBigGeneralsHead::showBeginAction()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	//test 1
// 	this->setPosition(ccp(-this->getContentSize().width,winSize.height/2));
// 	CCActionInterval*  actionTo = CCJumpTo::create(1.5f, ccp(winSize.width/2,winSize.height/2), 60, 3);
// 	this->runAction(actionTo);
// 	actionTo->setTag(kTagBeginAnm);
	//test 2
	CCShuffleTiles* shuffle = CCShuffleTiles::create(1.5, CCSizeMake(16,12), 15);
	CCActionInterval* shuffle_back = shuffle->reverse();
	CCDelayTime* delay = CCDelayTime::create(.001f);
	CCSequence * sequence = CCSequence::create(shuffle, delay, shuffle_back, NULL);
	this->runAction(sequence);
	sequence->setTag(kTagBeginAnm);
}
