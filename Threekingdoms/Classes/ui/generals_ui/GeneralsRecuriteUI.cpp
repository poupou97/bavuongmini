#include "GeneralsRecuriteUI.h"
#include "GeneralsUI.h"
#include "../../messageclient/element/CActionDetail.h"
#include "../../GameView.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "RecuriteActionItem.h"
#include "GeneralsHeadItemBase.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "SingleRecuriteResultUI.h"
#include "TenTimesRecuriteResultUI.h"
#include "../../legend_script/UITutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "GeneralsListUI.h"
#include "../extensions/RecruitGeneralCard.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/extensions/ShowSystemInfo.h"
#include "../../legend_script/CCTeachingGuide.h"

#define kTagGeneralRecuritMarquee 123

GeneralsRecuriteUI::GeneralsRecuriteUI():
curActionType(BASE)
{
}

GeneralsRecuriteUI::~GeneralsRecuriteUI()
{
	delete m_pCurGeneralDetail;
}

void GeneralsRecuriteUI::onEnter()
{
	UIScene::onEnter();
}

void GeneralsRecuriteUI::onExit()
{
	UIScene::onExit();
}

bool GeneralsRecuriteUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	return false;
}

void GeneralsRecuriteUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralsRecuriteUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralsRecuriteUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

GeneralsRecuriteUI* GeneralsRecuriteUI::create()
{
	GeneralsRecuriteUI * generalsRecuriteUI = new GeneralsRecuriteUI();
	if (generalsRecuriteUI && generalsRecuriteUI->init())
	{
		generalsRecuriteUI->autorelease();
		return generalsRecuriteUI;
	}
	CC_SAFE_DELETE(generalsRecuriteUI);
	return NULL;
}

bool GeneralsRecuriteUI::init()
{
	if (UIScene::init())
	{
		m_pCurGeneralDetail = new CGeneralDetail();

		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		//��ļ���
		panel_recruit = (UIPanel*)UIHelper::seekWidgetByName(GeneralsUI::ppanel,"Panel_generals");
		panel_recruit->setVisible(true);
		//  ��ļ��
		Layer_recruit = UILayer::create();
// 		Layer_recruit->ignoreAnchorPointForPosition(false);
// 		Layer_recruit->setAnchorPoint(ccp(0.5f,0.5f));
// 		Layer_recruit->setContentSize(CCSizeMake(800, 480));
// 		Layer_recruit->setPosition(ccp(winsize.width/2,winsize.height/2));

		addChild(Layer_recruit);

		Layer_upper = UILayer::create();
		addChild(Layer_upper);

		UIImageView * tableView_kuang = UIImageView::create();
		tableView_kuang->setTexture("res_ui/LV5_dikuang_001.png");
		tableView_kuang->setRotation(90);
		tableView_kuang->setScale9Enable(true);
		tableView_kuang->setScale9Size(CCSizeMake(180,31));
		tableView_kuang->setCapInsets(CCRectMake(32,31,1,1));
		tableView_kuang->setAnchorPoint(ccp(0.5f,0.5f));
		tableView_kuang->setPosition(ccp(79,358));
		Layer_upper->addWidget(tableView_kuang);

		UIButton* btn_null_good = (UIButton*)UIHelper::seekWidgetByName(panel_recruit,"Button_null_good");
		btn_null_good->setTouchEnable(true);
		btn_null_good->addReleaseEvent(this, coco_releaseselector(GeneralsRecuriteUI::NullGoodEvent));
		UIButton* btn_null_better = (UIButton*)UIHelper::seekWidgetByName(panel_recruit,"Button_null_better");
		btn_null_better->setTouchEnable(true);
		btn_null_better->addReleaseEvent(this, coco_releaseselector(GeneralsRecuriteUI::NullBetterEvent));
		UIButton* btn_null_best = (UIButton*)UIHelper::seekWidgetByName(panel_recruit,"Button_null_best");
		btn_null_best->setTouchEnable(true);
		btn_null_best->addReleaseEvent(this, coco_releaseselector(GeneralsRecuriteUI::NullBestEvent));

		//��ͨ��ļ
		Button_normalRecurit = (UIButton*)UIHelper::seekWidgetByName(panel_recruit,"Button_putongzhaomu");
		Button_normalRecurit->setTouchEnable(true);
		Button_normalRecurit->setPressedActionEnabled(true);
		Button_normalRecurit->addReleaseEvent(this, coco_releaseselector(GeneralsRecuriteUI::NormalRecuritEvent));
		//������ļ
		Button_goodRecurit = (UIButton*)UIHelper::seekWidgetByName(panel_recruit,"Button_youxiuzhaomu");
		Button_goodRecurit->setTouchEnable(true);
		Button_goodRecurit->setPressedActionEnabled(true);
		Button_goodRecurit->addReleaseEvent(this, coco_releaseselector(GeneralsRecuriteUI::GoodRecuritEvent));
		//��Ʒ��ļ
		Button_wonderfulRecurit = (UIButton*)UIHelper::seekWidgetByName(panel_recruit,"Button_jipinzhaomu");
		Button_wonderfulRecurit->setTouchEnable(true);
		Button_wonderfulRecurit->setPressedActionEnabled(true);
		Button_wonderfulRecurit->addReleaseEvent(this, coco_releaseselector(GeneralsRecuriteUI::WonderfulRecuritEvent));
		//ʮ��é®
		Button_tenTimesRecurit = (UIButton*)UIHelper::seekWidgetByName(panel_recruit,"Button_shigumaolu");
		Button_tenTimesRecurit->setVisible(true);
		Button_tenTimesRecurit->setTouchEnable(true);
		Button_tenTimesRecurit->setPressedActionEnabled(true);
		Button_tenTimesRecurit->addReleaseEvent(this, coco_releaseselector(GeneralsRecuriteUI::TenTimesRecuritEvent));
		UILabel * l_tenTimesCostGold = (UILabel*)UIHelper::seekWidgetByName(panel_recruit,"Label_1000");
		for (int i = 0;i<GameView::getInstance()->actionDetailList.size();++i)
		{
			CActionDetail * actionDetail = GameView::getInstance()->actionDetailList.at(i);
			if(actionDetail->type() == TEN_TIMES)
			{
				char s_gold[20];
				sprintf(s_gold,"%d",actionDetail->gold());
				l_tenTimesCostGold->setText(s_gold);
			}
		}

		RefreshRecruitData();

		createFiveBestGeneral();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);

		return true;
	}
	return false;
}

void GeneralsRecuriteUI::CloseEvent( CCObject *pSender )
{
	for (int i = 0;i<3;++i)
	{
		if (Layer_recruit->getChildByTag(50+i))
		{
			RecuriteActionItem * tempRecurite = ( RecuriteActionItem *)Layer_recruit->getChildByTag(50+i);
			tempRecurite->lastChangeTime = GameUtils::millisecondNow();
		}
	}
}

void GeneralsRecuriteUI::NormalRecuritEvent( CCObject* pSender )
{
	curActionType = BASE;
	ReqData* tempdata = new ReqData();
	tempdata->actionType = BASE;
	if (Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG))
	{
		RecuriteActionItem * temp = (RecuriteActionItem *)Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG);
		if (temp->state == RecuriteActionItem::Free)
		{
			tempdata->recruitType = FREE;
		}
		else
		{
			tempdata->recruitType = GOLD;
		}
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
	delete tempdata;
}

void GeneralsRecuriteUI::GoodRecuritEvent( CCObject* pSender )
{
	curActionType = BETTER;
	ReqData* tempdata = new ReqData();
	tempdata->actionType = BETTER;
	if (Layer_recruit->getChildByTag(BETTERRECURITEACTIONITEMTAG))
	{
		RecuriteActionItem * temp = (RecuriteActionItem *)Layer_recruit->getChildByTag(BETTERRECURITEACTIONITEMTAG);
		if (temp->state == RecuriteActionItem::Free)
		{
			tempdata->recruitType = FREE;
		}
		else
		{
			tempdata->recruitType = GOLD;
		}
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
	delete tempdata;

	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);
}

void GeneralsRecuriteUI::WonderfulRecuritEvent( CCObject* pSender )
{
	curActionType = BEST;
	ReqData* tempdata = new ReqData();
	tempdata->actionType = BEST;
	if (Layer_recruit->getChildByTag(BESTRECURITEACTIONITEMTAG))
	{
		RecuriteActionItem * temp = (RecuriteActionItem *)Layer_recruit->getChildByTag(BESTRECURITEACTIONITEMTAG);
		if (temp->state == RecuriteActionItem::Free)
		{
			tempdata->recruitType = FREE;
		}
		else
		{
			tempdata->recruitType = GOLD;
		}
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
	delete tempdata;

	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);
}

void GeneralsRecuriteUI::TenTimesRecuritEvent( CCObject* pSender )
{
	curActionType = TEN_TIMES;
	ReqData* tempdata = new ReqData();
	tempdata->actionType = TEN_TIMES;
	tempdata->recruitType = GOLD;

	GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
	delete tempdata;
}


void GeneralsRecuriteUI::createFiveBestGeneral()
{
// 	for (int i =0;i<GameView::getInstance()->actionDetailList.size();++i)
// 	{
// 		if (GameView::getInstance()->actionDetailList.at(i)->type() == TEN_TIMES)
// 		{
// 			for (int j = 0;j<GameView::getInstance()->actionDetailList.at(i)->generals_size();++j)
// 			{
// 				if (j < 5)
// 				{
// 					int _id = GameView::getInstance()->actionDetailList.at(i)->generals(j);
// 					GeneralsHeadItemBase * generalsHeadItemBase = GeneralsHeadItemBase::create(_id);
// 					generalsHeadItemBase->setAnchorPoint(ccp(0,0));
// 					generalsHeadItemBase->setPosition(ccp(72 + j*111,254));
// 					Layer_recruit->addChild(generalsHeadItemBase);
// 
// 					int width = generalsHeadItemBase->getContentSize().width-4;
// 					int height = generalsHeadItemBase->getContentSize().height-4;
// 					float speed = 150.f;
// 					ccColor4F color = ccc4FFromccc3B(ccc3(255, 222, 0));
// 					std::string _path = "animation/texiao/particledesigner/";
// 					_path.append("cardAround.plist");
// 
// 					CCParticleSystemQuad* particleEffect_1 = CCParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
// 					particleEffect_1->setPosition(ccp(72 + j*111+2,254+2));
// 					particleEffect_1->setStartSize(30);
// 					particleEffect_1->setPositionType(kCCPositionTypeFree);
// 					particleEffect_1->setVisible(true);
// 					particleEffect_1->setScale(1.0f);
// 					particleEffect_1->setLife(1.5f);
// 					//particleEffect_1->setStartColor(color);
// 					//particleEffect_1->setEndColor(color);
// 					Layer_recruit->addChild(particleEffect_1);
// 
// 					CCSequence * sequence_1 = CCSequence::create(
// 						CCMoveTo::create(height*1.0f/speed,ccp(72 + j*111+2,254+2+height)),
// 						CCMoveTo::create(width*1.0f/speed,ccp(72 + j*111+2+width,254+2+height)),
// 						CCMoveTo::create(height*1.0f/speed,ccp(72 + j*111+2+width,254+2)),
// 						CCMoveTo::create(width*1.0f/speed,ccp(72 + j*111+2,254+2)),
// 						NULL
// 						);
// 					CCRepeatForever * repeatForeverAnm_1 = CCRepeatForever::create(sequence_1);
// 					particleEffect_1->runAction(repeatForeverAnm_1);
// 
// 					CCParticleSystemQuad* particleEffect_2 = CCParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
// 					particleEffect_2->setPosition(ccp(72 + j*111+2+width,254+2+height));
// 					particleEffect_2->setStartSize(30);
// 					particleEffect_2->setPositionType(kCCPositionTypeFree);
// 					particleEffect_2->setVisible(true);
// 					particleEffect_2->setScale(1.0f);
// 					particleEffect_2->setLife(1.5f);
// 					//particleEffect_2->setStartColor(color);
// 					//particleEffect_2->setEndColor(color);
// 					Layer_recruit->addChild(particleEffect_2);
// 
// 					CCSequence * sequence_2 = CCSequence::create(
// 						CCMoveTo::create(height*1.0f/speed,ccp(72 + j*111+2+width,254+2)),
// 						CCMoveTo::create(width*1.0f/speed,ccp(72 + j*111+2,254+2)),
// 						CCMoveTo::create(height*1.0f/speed,ccp(72 + j*111+2,254+2+height)),
// 						CCMoveTo::create(width*1.0f/speed,ccp(72 + j*111+2+width,254+2+height)),
// 						NULL
// 						);
// 					CCRepeatForever * repeatForeverAnm_2 = CCRepeatForever::create(sequence_2);
// 					particleEffect_2->runAction(repeatForeverAnm_2);
// 
// // 					CCParticleSystemQuad* particleEffect_3 = CCParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
// // 					particleEffect_3->setPosition(ccp(72 + j*111+2,254+2+height));
// // 					particleEffect_3->setStartSize(10);
// // 					particleEffect_3->setPositionType(kCCPositionTypeFree);
// // 					particleEffect_3->setVisible(true);
// // 					particleEffect_3->setScale(1.0f);
// // 					Layer_recruit->addChild(particleEffect_3);
// // 
// // 					CCSequence * sequence_3 = CCSequence::create(
// // 						CCMoveTo::create(width*1.0f/speed,ccp(72 + j*111+2+width,254+2+height)),
// // 						CCMoveTo::create(height*1.0f/speed,ccp(72 + j*111+2+width,254+2)),
// // 						CCMoveTo::create(width*1.0f/speed,ccp(72 + j*111+2,254+2)),
// // 						CCMoveTo::create(height*1.0f/speed,ccp(72 + j*111+2,254+2+height)),
// // 						NULL
// // 						);
// // 					CCRepeatForever * repeatForeverAnm_3 = CCRepeatForever::create(sequence_3);
// // 					particleEffect_3->runAction(repeatForeverAnm_3);
// // 
// // 					CCParticleSystemQuad* particleEffect_4 = CCParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
// // 					particleEffect_4->setPosition(ccp(72 + j*111+2+width,254+2));
// // 					particleEffect_4->setStartSize(10);
// // 					particleEffect_4->setPositionType(kCCPositionTypeFree);
// // 					particleEffect_4->setVisible(true);
// // 					particleEffect_4->setScale(1.0f);
// // 					Layer_recruit->addChild(particleEffect_4);
// // 
// // 					CCSequence * sequence_4 = CCSequence::create(
// // 						CCMoveTo::create(width*1.0f/speed,ccp(72 + j*111+2,254+2)),
// // 						CCMoveTo::create(height*1.0f/speed,ccp(72 + j*111+2,254+2+height)),
// // 						CCMoveTo::create(width*1.0f/speed,ccp(72 + j*111+2+width,254+2+height)),
// // 						CCMoveTo::create(height*1.0f/speed,ccp(72 + j*111+2+width,254+2)),
// // 						NULL
// // 						);
// // 					CCRepeatForever * repeatForeverAnm_4 = CCRepeatForever::create(sequence_4);
// // 					particleEffect_4->runAction(repeatForeverAnm_4);
// 				}
// 			}
// 		}
// 	}

	//������ 
	if (Layer_recruit->getChildByTag(kTagGeneralRecuritMarquee) == NULL)
	{
		GeneralRecuritMarquee * pMarquee = GeneralRecuritMarquee::create(554,  147);
		pMarquee->setTag(kTagGeneralRecuritMarquee);
		pMarquee->setPosition(ccp(70,284));
		Layer_recruit->addChild(pMarquee);
	}
}

void GeneralsRecuriteUI::RefreshRecruitData()
{
	for (int i = 0;i<3;++i)
	{
		if (Layer_recruit->getChildByTag(50+i))
		{
			RecuriteActionItem * tempRecurite = ( RecuriteActionItem *)Layer_recruit->getChildByTag(50+i);
			tempRecurite->removeFromParent();
		}
	}

	for(int i = 0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		CActionDetail * temp = GameView::getInstance()->actionDetailList.at(i);
		if(temp->type() != TEN_TIMES)
		{
			ReloadNormalType(temp);
		}
	}
}

void GeneralsRecuriteUI::ReloadNormalType(CActionDetail * actionDetail)
{
	if (actionDetail->type() == BASE)
	{
		RecuriteActionItem * BaseRecurite = RecuriteActionItem::create(actionDetail);
		BaseRecurite->ignoreAnchorPointForPosition(false);
		BaseRecurite->setAnchorPoint(ccp(0,0));
		BaseRecurite->setPosition(ccp(62,52));
		BaseRecurite->setTag(BASERECURITEACTIONITEMTAG);
		Layer_recruit->addChild(BaseRecurite);
	}
	else if (actionDetail->type() == BETTER)
	{
		RecuriteActionItem * BetterRecurite = RecuriteActionItem::create(actionDetail);
		BetterRecurite->ignoreAnchorPointForPosition(false);
		BetterRecurite->setAnchorPoint(ccp(0,0));
		BetterRecurite->setPosition(ccp(294,52));
		BetterRecurite->setTag(BETTERRECURITEACTIONITEMTAG);
		Layer_recruit->addChild(BetterRecurite);
	}
	else if (actionDetail->type() == BEST)
	{
		RecuriteActionItem * BestRecurite = RecuriteActionItem::create(actionDetail);
		BestRecurite->ignoreAnchorPointForPosition(false);
		BestRecurite->setAnchorPoint(ccp(0,0));
		BestRecurite->setPosition(ccp(526,52));
		BestRecurite->setTag(BESTRECURITEACTIONITEMTAG);
		Layer_recruit->addChild(BestRecurite);
	}
}

void GeneralsRecuriteUI::setVisible( bool visible )
{
	panel_recruit->setVisible(visible);
	Layer_recruit->setVisible(visible);
	Layer_upper->setVisible(visible);

	if (visible)
	{
		for (int i = 0;i<3;++i)
		{
			if (Layer_recruit->getChildByTag(50+i))
			{
				RecuriteActionItem * tempRecurite = ( RecuriteActionItem *)Layer_recruit->getChildByTag(50+i);
				tempRecurite->refreshData();
			}
		}
	}
}

void GeneralsRecuriteUI::presentRecruiteResult( CGeneralDetail * generalDetail,int actionType,int recruitGeneralCardType,bool isPresentBtn )
{
	m_pCurGeneralDetail->CopyFrom(*generalDetail);
	switch(actionType)
	{
	case 1:
		curActionType = BASE;
		break;
	case 2:
		curActionType = BETTER;
		break;
	case 3:
		curActionType = BEST;
		break;
	}
	
	m_bIsPresentBtn = false;

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRecuriteGeneralCardUI))
		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRecuriteGeneralCardUI)->removeFromParent();

	RecruitGeneralCard * recuitCard_ = RecruitGeneralCard::create(recruitGeneralCardType,generalDetail->modelid());
	recuitCard_->ignoreAnchorPointForPosition(false);
	recuitCard_->setAnchorPoint(ccp(0.5f,0.5f));
	recuitCard_->setPosition(ccp(winSize.width/2,winSize.height/2));
	recuitCard_->AddcallBackFirstEvent(this,coco_selectselector(GeneralsRecuriteUI::DetailInfoEvent));
	recuitCard_->AddcallBacksecondEvent(this,NULL);
	recuitCard_->AddcallBackThirdEvent(this,coco_selectselector(GeneralsRecuriteUI::RecuriteAgainEvent));
	recuitCard_->setTag(kTagRecuriteGeneralCardUI);
	GameView::getInstance()->getMainUIScene()->addChild(recuitCard_);
	recuitCard_->RefreshCostValueByActionType(actionType);

// 	SingleRecuriteResultUI * singleRecuriteResultUI = (SingleRecuriteResultUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSingleRecuriteResultUI);
// 	if (singleRecuriteResultUI == NULL)
// 	{
// 		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
// 		SingleRecuriteResultUI * singleRecuriteResultLayer = SingleRecuriteResultUI::create();
// 		GameView::getInstance()->getMainUIScene()->addChild(singleRecuriteResultLayer,0,kTagGeneralsSingleRecuriteResultUI);
// 		singleRecuriteResultLayer->ignoreAnchorPointForPosition(false);
// 		singleRecuriteResultLayer->setAnchorPoint(ccp(0.5f, 0.5f));
// 		singleRecuriteResultLayer->setPosition(ccp(winSize.width/2, winSize.height/2));
// 		singleRecuriteResultLayer->RefreshRecruiteResult(generalDetail);
// 		singleRecuriteResultLayer->RefreshRecruiteCost(actionType);
// 		singleRecuriteResultLayer->RefreshBtnPresent(isPresentBtn);
// 	}
// 	else
// 	{
// 		singleRecuriteResultUI->RefreshRecruiteResult(generalDetail);
// 		singleRecuriteResultUI->RefreshRecruiteCost(actionType);
// 		singleRecuriteResultUI->RefreshBtnPresent(isPresentBtn);
// 	}
}



void GeneralsRecuriteUI::presentTenTimesRecruiteResult( std::vector<CGeneralDetail *> temp ,bool isPresentBtn)
{	
	TenTimesRecuriteResultUI * tenTimesRecuriteResultUI = (TenTimesRecuriteResultUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsTenTimesRecuriteResultUI);
	if (tenTimesRecuriteResultUI == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		TenTimesRecuriteResultUI * tenTimesRecuriteResultLayer = TenTimesRecuriteResultUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(tenTimesRecuriteResultLayer,0,kTagGeneralsTenTimesRecuriteResultUI);
		tenTimesRecuriteResultLayer->ignoreAnchorPointForPosition(false);
		tenTimesRecuriteResultLayer->setAnchorPoint(ccp(0.5f, 0.5f));
		tenTimesRecuriteResultLayer->setPosition(ccp(winSize.width/2, winSize.height/2));
		tenTimesRecuriteResultLayer->RefreshTenTimesRecruitData(temp);
		tenTimesRecuriteResultLayer->RefreshBtnPresent(isPresentBtn);
	}
	else
	{
		tenTimesRecuriteResultUI->RefreshTenTimesRecruitData(temp);
		tenTimesRecuriteResultUI->RefreshBtnPresent(isPresentBtn);
	}
}

GeneralsRecuriteUI::RecuriteActionType GeneralsRecuriteUI::getCurRecuriteActionType()
{
	return curActionType;
}


void GeneralsRecuriteUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralsRecuriteUI::addCCTutorialIndicator( const char* content,CCPoint pos ,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,70,50);
// 	tutorialIndicator->setPosition(ccp(pos.x-40,pos.y+90));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	Layer_recruit->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,120,40,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralsRecuriteUI::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(Layer_recruit->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();
	
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void GeneralsRecuriteUI::DetailInfoEvent( CCObject *pSender )
{
	SingleRecuriteResultUI * singleRecuriteResultUI = (SingleRecuriteResultUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSingleRecuriteResultUI);
	if (singleRecuriteResultUI == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		SingleRecuriteResultUI * singleRecuriteResultLayer = SingleRecuriteResultUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(singleRecuriteResultLayer,0,kTagGeneralsSingleRecuriteResultUI);
		singleRecuriteResultLayer->ignoreAnchorPointForPosition(false);
		singleRecuriteResultLayer->setAnchorPoint(ccp(0.5f, 0.5f));
		singleRecuriteResultLayer->setPosition(ccp(winSize.width/2, winSize.height/2));
		singleRecuriteResultLayer->RefreshRecruiteResult(m_pCurGeneralDetail);
		singleRecuriteResultLayer->RefreshRecruiteCost(m_nActionType);
		singleRecuriteResultLayer->RefreshBtnPresent(m_bIsPresentBtn);
	}
	else
	{
		singleRecuriteResultUI->RefreshRecruiteResult(m_pCurGeneralDetail);
		singleRecuriteResultUI->RefreshRecruiteCost(m_nActionType);
		singleRecuriteResultUI->RefreshBtnPresent(m_bIsPresentBtn);
	}
}


void GeneralsRecuriteUI::RecuriteAgainEvent( CCObject *pSender )
{
	if (this->getCurRecuriteActionType() == BASE)
	{
		GeneralsRecuriteUI::ReqData* tempdata = new GeneralsRecuriteUI::ReqData();
		tempdata->actionType = BASE;
		if (Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG))
		{
			RecuriteActionItem * temp = (RecuriteActionItem *)Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG);
			if (temp->state == RecuriteActionItem::Free)
			{
				tempdata->recruitType = FREE;
			}
			else
			{
				tempdata->recruitType = GOLD;
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;
	}
	else if (this->getCurRecuriteActionType() == BETTER)
	{
		GeneralsRecuriteUI::ReqData* tempdata = new GeneralsRecuriteUI::ReqData();
		tempdata->actionType = BETTER;
		if (Layer_recruit->getChildByTag(BETTERRECURITEACTIONITEMTAG))
		{
			RecuriteActionItem * temp = (RecuriteActionItem *)Layer_recruit->getChildByTag(BETTERRECURITEACTIONITEMTAG);
			if (temp->state == RecuriteActionItem::Free)
			{
				tempdata->recruitType = FREE;
			}
			else
			{
				tempdata->recruitType = GOLD;
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;
	}
	else if (this->getCurRecuriteActionType() == BEST)
	{
		GeneralsRecuriteUI::ReqData* tempdata = new GeneralsRecuriteUI::ReqData();
		tempdata->actionType = BEST;
		if (Layer_recruit->getChildByTag(BESTRECURITEACTIONITEMTAG))
		{
			RecuriteActionItem * temp = (RecuriteActionItem *)Layer_recruit->getChildByTag(BESTRECURITEACTIONITEMTAG);
			if (temp->state == RecuriteActionItem::Free)
			{
				tempdata->recruitType = FREE;
			}
			else
			{
				tempdata->recruitType = GOLD;
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;
	}
}

void GeneralsRecuriteUI::NullGoodEvent( CCObject *pSender )
{
	CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(14);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end())
	{
		return ;
	}
	else
	{
		if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		{
			ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[14].c_str());
			GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			showSystemInfo->ignoreAnchorPointForPosition(false);
			showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			showSystemInfo->setPosition(ccp(winsize.width/2, winsize.height/2));
		}
	}
}

void GeneralsRecuriteUI::NullBetterEvent( CCObject *pSender )
{
	CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(15);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end())
	{
		return ;
	}
	else
	{
		if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		{
			ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[15].c_str());
			GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			showSystemInfo->ignoreAnchorPointForPosition(false);
			showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			showSystemInfo->setPosition(ccp(winsize.width/2, winsize.height/2));
		}
	}
}

void GeneralsRecuriteUI::NullBestEvent( CCObject *pSender )
{
	CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(16);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end())
	{
		return ;
	}
	else
	{
		if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		{
			ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[16].c_str());
			GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			showSystemInfo->ignoreAnchorPointForPosition(false);
			showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			showSystemInfo->setPosition(ccp(winsize.width/2, winsize.height/2));
		}
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GeneralRecuritMarquee::GeneralRecuritMarquee()
	:m_layerRecurite1Pos(ccp(0, 0))
	,m_layerRecurite2Pos(ccp(0, 0))
	,m_nWidth(0)
	,m_nHeight(0)
{

}

GeneralRecuritMarquee::~GeneralRecuritMarquee()
{
	this->unscheduleUpdate();
}

GeneralRecuritMarquee * GeneralRecuritMarquee::create(int width, int height )
{
	GeneralRecuritMarquee * marquee = new GeneralRecuritMarquee();
	if (marquee && marquee->init(width, height))
	{
		marquee->autorelease();
		return marquee;
	}
	CC_SAFE_DELETE(marquee);
	return NULL;
}

bool GeneralRecuritMarquee::init(int width, int height )
{
	if (CCNode::init())
	{
		m_nWidth = width;
		m_nHeight = height;

		CCScrollView* m_contentScrollView = CCScrollView::create(CCSizeMake(width,height));
		m_contentScrollView->setViewSize(CCSizeMake(width, height));
		m_contentScrollView->ignoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(false);
		m_contentScrollView->setDirection(kCCScrollViewDirectionHorizontal);
		m_contentScrollView->setAnchorPoint(ccp(0,0));
		m_contentScrollView->setPosition(ccp(0,0));
		m_contentScrollView->setBounceable(false);
		addChild(m_contentScrollView);

		//create recurite 1
		l_recurite_1 = CCLayer::create();
		l_recurite_1->setContentSize(CCSizeMake(width,height));
		m_contentScrollView->addChild(l_recurite_1);

		for (int i =0;i<GameView::getInstance()->actionDetailList.size();++i)
		{
			if (GameView::getInstance()->actionDetailList.at(i)->type() == TEN_TIMES)
			{
				for (int j = 0;j<GameView::getInstance()->actionDetailList.at(i)->generals_size();++j)
				{
					if (j < 5)
					{
						int _id = GameView::getInstance()->actionDetailList.at(i)->generals(j);
						GeneralsHeadItemBase * generalsHeadItemBase = GeneralsHeadItemBase::create(_id);
						generalsHeadItemBase->setAnchorPoint(ccp(0,0));
						generalsHeadItemBase->setPosition(ccp(0 + j*111,0));
						l_recurite_1->addChild(generalsHeadItemBase);
						generalsHeadItemBase->visit();

						int width = generalsHeadItemBase->getContentSize().width-4;
						int height = generalsHeadItemBase->getContentSize().height-4;
						float speed = 150.f;
						ccColor4F color = ccc4FFromccc3B(ccc3(255, 222, 0));
						std::string _path = "animation/texiao/particledesigner/";
						_path.append("cardAround.plist");

						CCParticleSystemQuad* particleEffect_1 = CCParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
						particleEffect_1->setPosition(ccp( j*111+2,2));
						particleEffect_1->setStartSize(30);
						particleEffect_1->setPositionType(kCCPositionTypeRelative);
						particleEffect_1->setVisible(true);
						particleEffect_1->setScale(1.0f);
						particleEffect_1->setLife(1.5f);
						//particleEffect_1->setStartColor(color);
						//particleEffect_1->setEndColor(color);
						l_recurite_1->addChild(particleEffect_1);

						CCSequence * sequence_1 = CCSequence::create(
							CCMoveTo::create(height*1.0f/speed,ccp(j*111+2,2+height)),
							CCMoveTo::create(width*1.0f/speed,ccp(j*111+2+width,2+height)),
							CCMoveTo::create(height*1.0f/speed,ccp(j*111+2+width,2)),
							CCMoveTo::create(width*1.0f/speed,ccp(j*111+2,2)),
							NULL
							);
						CCRepeatForever * repeatForeverAnm_1 = CCRepeatForever::create(sequence_1);
						particleEffect_1->runAction(repeatForeverAnm_1);

						CCParticleSystemQuad* particleEffect_2 = CCParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
						particleEffect_2->setPosition(ccp(j*111+2+width,2+height));
						particleEffect_2->setStartSize(30);
						particleEffect_2->setPositionType(kCCPositionTypeRelative);
						particleEffect_2->setVisible(true);
						particleEffect_2->setScale(1.0f);
						particleEffect_2->setLife(1.5f);
						//particleEffect_2->setStartColor(color);
						//particleEffect_2->setEndColor(color);
						l_recurite_1->addChild(particleEffect_2);

						CCSequence * sequence_2 = CCSequence::create(
							CCMoveTo::create(height*1.0f/speed,ccp(j*111+2+width,2)),
							CCMoveTo::create(width*1.0f/speed,ccp(j*111+2,2)),
							CCMoveTo::create(height*1.0f/speed,ccp(j*111+2,2+height)),
							CCMoveTo::create(width*1.0f/speed,ccp(j*111+2+width,2+height)),
							NULL
							);
						CCRepeatForever * repeatForeverAnm_2 = CCRepeatForever::create(sequence_2);
						particleEffect_2->runAction(repeatForeverAnm_2);
					}
				}
			}
		}


		//create recurite 2
		l_recurite_2 = CCLayer::create();
		l_recurite_2->setContentSize(CCSizeMake(width,height));
		m_contentScrollView->addChild(l_recurite_2);

		for (int i =0;i<GameView::getInstance()->actionDetailList.size();++i)
		{
			if (GameView::getInstance()->actionDetailList.at(i)->type() == TEN_TIMES)
			{
				for (int j = 0;j<GameView::getInstance()->actionDetailList.at(i)->generals_size();++j)
				{
					if (j < 5)
					{
						int _id = GameView::getInstance()->actionDetailList.at(i)->generals(j);
						GeneralsHeadItemBase * generalsHeadItemBase = GeneralsHeadItemBase::create(_id);
						generalsHeadItemBase->setAnchorPoint(ccp(0,0));
						generalsHeadItemBase->setPosition(ccp(0 + j*111,0));
						l_recurite_2->addChild(generalsHeadItemBase);
						generalsHeadItemBase->visit();

						int width = generalsHeadItemBase->getContentSize().width-4;
						int height = generalsHeadItemBase->getContentSize().height-4;
						float speed = 150.f;
						ccColor4F color = ccc4FFromccc3B(ccc3(255, 222, 0));
						std::string _path = "animation/texiao/particledesigner/";
						_path.append("cardAround.plist");

						CCParticleSystemQuad* particleEffect_1 = CCParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
						particleEffect_1->setPosition(ccp( j*111+2,2));
						particleEffect_1->setStartSize(30);
						particleEffect_1->setPositionType(kCCPositionTypeRelative);
						particleEffect_1->setVisible(true);
						particleEffect_1->setScale(1.0f);
						particleEffect_1->setLife(1.5f);
						//particleEffect_1->setStartColor(color);
						//particleEffect_1->setEndColor(color);
						l_recurite_2->addChild(particleEffect_1);

						CCSequence * sequence_1 = CCSequence::create(
							CCMoveTo::create(height*1.0f/speed,ccp(j*111+2,2+height)),
							CCMoveTo::create(width*1.0f/speed,ccp(j*111+2+width,2+height)),
							CCMoveTo::create(height*1.0f/speed,ccp(j*111+2+width,2)),
							CCMoveTo::create(width*1.0f/speed,ccp(j*111+2,2)),
							NULL
							);
						CCRepeatForever * repeatForeverAnm_1 = CCRepeatForever::create(sequence_1);
						particleEffect_1->runAction(repeatForeverAnm_1);

						CCParticleSystemQuad* particleEffect_2 = CCParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
						particleEffect_2->setPosition(ccp(j*111+2+width,2+height));
						particleEffect_2->setStartSize(30);
						particleEffect_2->setPositionType(kCCPositionTypeRelative);
						particleEffect_2->setVisible(true);
						particleEffect_2->setScale(1.0f);
						particleEffect_2->setLife(1.5f);
						//particleEffect_2->setStartColor(color);
						//particleEffect_2->setEndColor(color);
						l_recurite_2->addChild(particleEffect_2);

						CCSequence * sequence_2 = CCSequence::create(
							CCMoveTo::create(height*1.0f/speed,ccp(j*111+2+width,2)),
							CCMoveTo::create(width*1.0f/speed,ccp(j*111+2,2)),
							CCMoveTo::create(height*1.0f/speed,ccp(j*111+2,2+height)),
							CCMoveTo::create(width*1.0f/speed,ccp(j*111+2+width,2+height)),
							NULL
							);
						CCRepeatForever * repeatForeverAnm_2 = CCRepeatForever::create(sequence_2);
						particleEffect_2->runAction(repeatForeverAnm_2);
					}
				}
			}
		}

		m_layerRecurite1Pos = ccp(0,0);
		m_layerRecurite2Pos = ccp(-m_nWidth,0);

		this->scheduleUpdate();

		return true;
	}

	return false;
}

void GeneralRecuritMarquee::update( float dt )
{
  	m_layerRecurite1Pos.y = 0;
	m_layerRecurite2Pos.y = 0;
  
  	m_layerRecurite1Pos.x = m_layerRecurite1Pos.x + dt * 20;
	m_layerRecurite2Pos.x = m_layerRecurite2Pos.x + dt * 20;
  	if (m_layerRecurite1Pos.x  > m_nWidth)
  	{
		m_layerRecurite1Pos.x = -m_nWidth;
  	}

	if (m_layerRecurite2Pos.x  > m_nWidth)
	{
		m_layerRecurite2Pos.x = -m_nWidth;
	}
  
  	l_recurite_1->setPosition(m_layerRecurite1Pos);
	l_recurite_2->setPosition(m_layerRecurite2Pos);
}