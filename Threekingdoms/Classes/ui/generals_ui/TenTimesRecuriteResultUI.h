#ifndef  _GENERALSUI_TENTIMESRECURITERESULTUI_H_
#define _GENERALSUI_TENTIMESRECURITERESULTUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "GeneralsListBase.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralBaseMsg;
class CGeneralDetail;
class CActionDetail;

/////////////////////////////////
/**
 * 武将界面下的“点将台”中的十连抽结果类
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.9
 */

class TenTimesRecuriteResultUI : public UIScene
{
public:
	TenTimesRecuriteResultUI();
	~TenTimesRecuriteResultUI();

	static TenTimesRecuriteResultUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);


	void CloseEvent(CCObject * pSender);

	//十连抽返回
	void TenTimesBackEvent(CCObject * pSender);
	//十连抽再来一次
	void TenTimesAgainEvent(CCObject * pSender);

	//刷新十连抽招募完成面板
	void RefreshTenTimesRecruitData( std::vector<CGeneralDetail *> temp );

	//刷新按钮的显示与否
	void RefreshBtnPresent(bool isPresent);

public:
	UIPanel * panel_tenTimesRecruit;  //十连抽结果面板
	UILayer * Layer_tenTimesRecruit;

	UIPanel * panel_btn;
	UIButton * btn_close;

private:
	UILabel * lable_tenTimes_gold;

};



////////////////////////////////////////////////////////////////////////////////
/*
* 十顾茅庐招募武将卡牌
*/
class CGeneralDetail;
class GeneralCardItem:public UIScene
{
public:
	GeneralCardItem(void);
	~GeneralCardItem(void);

	static GeneralCardItem * create(CGeneralDetail * generalInfo);
	bool init(CGeneralDetail * generalInfo);
	void onEnter();
	void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	std::string getGeneralIconPath(std::string generalIcon);
	void callBackShowGeneral(CCObject * obj);

	void showStarAndProfession();

	void showBonusEffect();

	void generalStarActions();

	void generalProfessionActions();

private:
	cocos2d::extension::UIImageView * imageGeneral_bg;
	//cocos2d::extension::UIImageView * imageGeneral_icon;
	cocos2d::extension::UIImageView * imageGeneral_Star;
	cocos2d::extension::UIImageView * imageGeneral_profession;
	CCSize winSize;
	UILayer * u_layer;
};

#endif

