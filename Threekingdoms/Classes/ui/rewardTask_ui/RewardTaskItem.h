#ifndef _UI_REWARDTASKUI_REWARDTASKITEM_H_
#define _UI_REWARDTASKUI_REWARDTASKITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CBoardMissionInfo;

/////////////////////////////////
/**
 * ����������
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.08.21
 */

class RewardTaskItem : public UIScene
{
public:
	RewardTaskItem();
	~RewardTaskItem();

	static RewardTaskItem * create(CBoardMissionInfo * missionInfo,int pos);
	bool init(CBoardMissionInfo * missionInfo,int pos);

	void MissionStateEvent(CCObject *pSender);
	void LookOverEvent(CCObject *pSender);

	void SureToFinishRightNow(CCObject *pSender);

	int getCurPos();

	void RefreshBtnMissionState(int state,bool isAnm = true);

private:
	CBoardMissionInfo * curBoardMission;
	int m_nPos;

	UIButton * btn_missionState;
	UILabel * l_missionState;
	UIImageView * imageView_missionState;

	UIImageView * imageView_mask;
};


#endif