#ifndef _UI_REWARDTASKUI_REWARDTASKDETAILUI_H_
#define _UI_REWARDTASKUI_REWARDTASKDETAILUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 悬赏任务的详情界面
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.08.25
 */

class CBoardMissionInfo;

class RewardTaskDetailUI : public UIScene
{
public:
	RewardTaskDetailUI(void);
	~RewardTaskDetailUI(void);

	static RewardTaskDetailUI* create(CBoardMissionInfo * boardMissionInfo);
	bool init(CBoardMissionInfo * boardMissionInfo);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void update(float dt);

	void CloseEvent( CCObject *pSender );
	void GetMissionEvent( CCObject *pSender );
	void FinishMissionEvent( CCObject *pSender );
	void FinishRightNowEvent( CCObject *pSender );
	void AutoFindPathEvent( CCObject *pSender );
	void GiveUpMissionEvent( CCObject *pSender );
	void SureToGiveUpMission( CCObject *pSender );
	void SureToFinishRightNow( CCObject *pSender );

	void refreshBtnState(int missionState);

private:
	UILayer* m_base_layer;

	UIButton * btn_getMission;
	UIButton * btn_finishMission;
	UIButton * btn_finishRightNow;
	UIButton * btn_autoFindPath;
	UIButton * btn_giveUpMission;

public:
	CBoardMissionInfo* curBoardMissionInfo;
};

#endif