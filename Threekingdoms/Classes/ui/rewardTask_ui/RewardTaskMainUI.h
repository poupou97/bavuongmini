#ifndef _UI_REWARDTASKUI_REWARDTASKMAINUI_H_
#define _UI_REWARDTASKUI_REWARDTASKMAINUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define kTag_RewardTaskItem_Base 200
#define kTag_RefreshAction_deleteOld 210
#define kTag_RefreshAction_addNew 220

#define RewardTastItem_First_Pos_x 70
#define RewardTastItem_First_Pos_y 166
#define RewardTastItem_space 133

/////////////////////////////////
/**
 * 悬赏任务UI
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.08.21
 */

class RewardTaskMainUI : public UIScene
{
public:
	RewardTaskMainUI(void);
	~RewardTaskMainUI(void);

public:
	static RewardTaskMainUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void update(float dt);

	void CloseEvent( CCObject *pSender );
	void RefreshEvent(CCObject * pSender);
	void RefreshFiveStarEvent(CCObject *pSender);

	//显示5个悬赏任务
	void RefreshFiveMissionPresent();
	void RefreshFiveMissionPresentWithAnm();

	void RefreshMissionState(int index,int state);
	//刷新今日剩余任务显示
	void RefreshRemainTaskNum();
public:
	UILayer * m_base_layer;																							// 基调layer
	UILayer * m_up_layer;
private:
	UILabel * l_remainTimeToFree;                                                                                //剩余时间
	UILabel * l_remainTaskNum;                                                                                   //剩余任务
	UILabel * l_refreshCostValue;                                                                                  //刷新消耗（元宝）
	UILabel * l_refreshFiveStarCostValue;                                                                      //一键五星消耗（元宝）
	UILabel * l_freeThisTime;                                                                                        //本次刷新免费

public:
	//教学
	int mTutorialScriptInstanceId;
	virtual void registerScriptCommand(int scriptId);
	//选技能
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();
};

#endif

