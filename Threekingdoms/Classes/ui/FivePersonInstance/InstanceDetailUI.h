#ifndef _UI_INSTANCE_INSTANCEDETAIL_H_
#define _UI_INSTANCE_INSTANCEDETAIL_H_

#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"

class UITab;
class CFivePersonInstance;

class InstanceDetailUI : public UIScene
{
public:
	InstanceDetailUI();
	~InstanceDetailUI();

	static InstanceDetailUI *create(int instanceId);
	bool init(int instanceId);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void callBackExit(CCObject * pSender);

	void signUpCallback(CCObject* pSender);
	void cancelSignCallback(CCObject* pSender);
	void enterInstanceCallback(CCObject* pSender);
	void createTeamCallback(CCObject* pSender);
	void btnRewardCallback(CCObject* pSender);

	void RefreshInstanceInfo(int instanceId);
	void RefreshRewardsInfo();
	void TabIndexChangedEvent(CCObject* pSender);

	bool isFinishAction();

	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();

private:
	//��ѧ
	int mTutorialScriptInstanceId;

public:
	UIButton * btnEnter;
	UIButton * btnSignUp;
	UIButton * btnCancelSign;
protected:
	UILayer * layer;

	UILabelBMFont * l_instanceName ;
	UILabel * l_open_lv;
	UILabel * l_consume;
	UILabel * l_open_time;
	UILabel * l_difficult;
	UILabel * l_suggest;
	UILabel * l_combat;
	UITextArea * t_instanceExplain;

	CCScrollView * m_scrollView_des;
	CCScrollView * m_scrollView_explain;
	CCLabelTTF * l_des_head;
	CCLabelTTF * l_des;
	CCLabelTTF * l_info_head;
	CCLabelTTF * l_info;

	int curInstanceId;
};

#endif