#include "InstanceFunctionPanel.h"

#include "../../messageclient/GameMessageProcessor.h"
#include "../Mail_ui/MailFriend.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../Chat_ui/AddPrivateUi.h"
#include "../../utils/StaticDataManager.h"

InstanceFunctionPanel::InstanceFunctionPanel()
{
}

InstanceFunctionPanel::~InstanceFunctionPanel()
{
}

InstanceFunctionPanel * InstanceFunctionPanel::create()
{
	InstanceFunctionPanel * _panel = new InstanceFunctionPanel();
	if (_panel && _panel->init())
	{
		_panel->autorelease();
		return _panel;
	}
	CC_SAFE_DELETE(_panel);
	return NULL;
}

bool InstanceFunctionPanel::init()
{
	if (UIScene::init())
	{
		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

		UIPanel * mainPanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/instance_function_panel.json");
		mainPanel->setAnchorPoint(ccp(0.f,0.f));
		mainPanel->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mainPanel);

		UIButton * button= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_CallFriend");
		CCAssert(button != NULL, "should not be nil");
		button->setTouchEnable(true);
		button->setPressedActionEnabled(true);
		button->addReleaseEvent(this,coco_releaseselector(InstanceFunctionPanel::callFriendCallback));

		button= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_ExitInstance");
		CCAssert(button != NULL, "should not be nil");
		button->setTouchEnable(true);
		button->setPressedActionEnabled(true);
		button->addReleaseEvent(this,coco_releaseselector(InstanceFunctionPanel::exitInstanceCallback));

		this->ignoreAnchorPointForPosition(false);
		this->setAnchorPoint(ccp(0,0));
		this->setPosition(ccp(0,0));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(mainPanel->getContentSize());
		
		return true;
	}
	return false;
}

bool InstanceFunctionPanel::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	if(UIScene::ccTouchBegan(pTouch, pEvent))
		return resignFirstResponder(pTouch,this,false);

	return false;
}

void InstanceFunctionPanel::onEnter()
{
	UIScene::onEnter();
}

void InstanceFunctionPanel::onExit()
{
	UIScene::onExit();
}

void InstanceFunctionPanel::callFriendCallback(CCObject* pSender)
{	
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagPopFriendListUI) == NULL)
	{
		this->removeFromParent();
		CCSize winsize=CCDirector::sharedDirector()->getVisibleSize();
		MailFriend * mailFriend =MailFriend::create(kTagInstanceDetailUI);
		mailFriend->ignoreAnchorPointForPosition(false);
		mailFriend->setAnchorPoint(ccp(0.5f,0.5f));
		mailFriend->setPosition(ccp(winsize.width/2,winsize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(mailFriend,5,kTagPopFriendListUI);
		AddPrivateUi::FriendList friend1={0,20,0,1};
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);
	}
}

void InstanceFunctionPanel::exitInstanceCallback(CCObject* pSender)
{
	GameView::getInstance()->showPopupWindow(StringDataManager::getString("fivePerson_areYouSureToExit"),2,this,coco_selectselector(InstanceFunctionPanel::sureToExit),NULL,PopupWindow::KTypeRemoveByTime,10);
}

void InstanceFunctionPanel::sureToExit( CCObject *pSender )
{
	this->removeFromParent();
	// request to exit the instance
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1924);
}
