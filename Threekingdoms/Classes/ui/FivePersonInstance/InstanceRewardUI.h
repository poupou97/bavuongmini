
#ifndef _FIVEPERSONINSTANCE_INSTANCEREWARD_H_
#define _FIVEPERSONINSTANCE_INSTANCEREWARD_H_

#include "../extensions/UIScene.h"
#include "../backpackscene/PacPageView.h"

class GoodsInfo;
class InstanceRewardUI : public UIScene
{
public:
	InstanceRewardUI();
	~InstanceRewardUI();

	static InstanceRewardUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void CloseEvent(CCObject *pSender);
	void ReloadData();
	void ReloadDataByIndex(int index);

	void CurrentPageViewChanged(CCObject* pSender);

public:
	//std::vector<GoodsInfo*> rewardGoods;

private:
	std::vector<Coordinate> CoordinateVector;

	std::string rewardGoodItem_name[16];

	UILayer * u_layer;
	UIPageView *m_pageView;

	int m_nElementNumEveryPage;
	int m_nPageNum;
	//点的间距
	int m_nSpace;
	//最大页数
	int m_nMaxPageNum;

	float first_point_x;
};

#endif
