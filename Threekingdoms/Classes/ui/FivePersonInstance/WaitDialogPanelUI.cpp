#include "WaitDialogPanelUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "FivePersonInstance.h"
#include "GameView.h"
#include "../../messageclient/GameMessageProcessor.h"


WaitDialogPanelUI::WaitDialogPanelUI(void)
{
}


WaitDialogPanelUI::~WaitDialogPanelUI(void)
{
}

WaitDialogPanelUI* WaitDialogPanelUI::create()
{
	WaitDialogPanelUI * _panel = new WaitDialogPanelUI();
	if (_panel && _panel->init())
	{
		_panel->autorelease();
		return _panel;
	}
	CC_SAFE_DELETE(_panel);
	return NULL;
}

bool WaitDialogPanelUI::init()
{
	if (UIScene::init())
	{
		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

		UIPanel * ppanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/tishikuang_instance_1.json");
		ppanel->setAnchorPoint(ccp(0,0));
		ppanel->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(ppanel);

		UITextArea * textArea =(UITextArea*)UIHelper::seekWidgetByName(ppanel,"TextArea_content");

		std::string str_des = StringDataManager::getString("fivePerson_waitSignUp_des_1");
		for (int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
		{
			CFivePersonInstance * fivePersonInstance = FivePersonInstanceConfigData::s_fivePersonInstance.at(i);
			if (FivePersonInstance::getSignedUpId() == fivePersonInstance->get_id())
			{
				str_des.append(fivePersonInstance->get_name().c_str());
				break;
			}
		}
		str_des.append(StringDataManager::getString("fivePerson_waitSignUp_des_2"));
		textArea->setText(str_des.c_str());

		UIButton * btn_Wait=(UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_enter");
		btn_Wait->setTouchEnable(true);
		btn_Wait->setPressedActionEnabled(true);
		btn_Wait->addReleaseEvent(this,coco_releaseselector(WaitDialogPanelUI::WaitEvent));

		UIButton * btn_CancelWait=(UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_cancel");
		btn_CancelWait->setTouchEnable(true);
		btn_CancelWait->setPressedActionEnabled(true);
		btn_CancelWait->addReleaseEvent(this,coco_releaseselector(WaitDialogPanelUI::CancelWaitEvent));

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(ppanel->getContentSize());

		return true;
	}
	return false;
}

bool WaitDialogPanelUI::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return true;
}

void WaitDialogPanelUI::onEnter()
{
	UIScene::onEnter();
}

void WaitDialogPanelUI::onExit()
{
	UIScene::onExit();
}

void WaitDialogPanelUI::WaitEvent( CCObject *pSender )
{
	FivePersonInstance::isStopUpdate = false;
	this->closeAnim();
}

void WaitDialogPanelUI::CancelWaitEvent( CCObject *pSender )
{
	if (FivePersonInstance::getSignedUpId() == -1)
	{

	}
	else
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1921,(void *)FivePersonInstance::getSignedUpId());
	}
	this->closeAnim();
}
