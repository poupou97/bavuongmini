#include "InstanceMapUI.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "../../utils/StaticDataManager.h"
#include "../extensions/UITab.h"
#include "../../GameView.h"
#include "InstanceRewardUI.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "FivePersonInstance.h"
#include "../../utils/GameUtils.h"
#include "InstanceDetailUI.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "../../messageclient/element/CLable.h"
#include "../goldstore_ui/GoldStoreUI.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../gamescene_state/GameSceneEffects.h"

bool sortByInt(int a,int b)
{
	return a<b;
}

InstanceMapUI::InstanceMapUI():
curInstanceId(-1),
curInstancePos(ccp(0,0))
{
}

InstanceMapUI::~InstanceMapUI()
{
}

InstanceMapUI * InstanceMapUI::create()
{
	InstanceMapUI * _ui=new InstanceMapUI();
	if (_ui && _ui->init())
	{
		_ui->autorelease();
		return _ui;
	}
	CC_SAFE_DELETE(_ui);
	return NULL;
}

bool InstanceMapUI::init()
{
	if (UIScene::init())
	{
		CCSize size=CCDirector::sharedDirector()->getVisibleSize();

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(size);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		layer=UILayer::create();
		layer->ignoreAnchorPointForPosition(false);
		layer->setAnchorPoint(ccp(0,0));
		layer->setContentSize(size);
		layer->setPosition(ccp(0,0));
		addChild(layer);

		UIPanel *mainPanel=(UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/fubenditu_1.json");
		mainPanel->setAnchorPoint(ccp(0,0));
		mainPanel->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mainPanel);

		UIButton * btn_Exit= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_close");
		btn_Exit->setTouchEnable(true);
		btn_Exit->setPressedActionEnabled(true);
		btn_Exit->addReleaseEvent(this,coco_releaseselector(InstanceMapUI::CloseEvent));
		btn_Exit->setPosition(ccp(size.width-40,size.height-40));

		UIImageView * imageView_CloseFrame = (UIImageView*)UIHelper::seekWidgetByName(mainPanel,"ImageView_358");
		imageView_CloseFrame->setPosition(ccp(size.width-40,size.height-40));

		UIPanel * panel_phyInfo = (UIPanel*)UIHelper::seekWidgetByName(mainPanel,"Panel_phyInfo");
		panel_phyInfo->setAnchorPoint(ccp(0,1.0f));
		panel_phyInfo->setPosition(ccp(0,size.height));

		panel_tutorial =  (UIPanel*)UIHelper::seekWidgetByName(mainPanel,"Panel_tutorial");

		btn_addPhy = (UIButton*)UIHelper::seekWidgetByName(mainPanel,"Button_addPhy");
		btn_addPhy->setTouchEnable(true);
		btn_addPhy->setPressedActionEnabled(true);
		btn_addPhy->addReleaseEvent(this,coco_releaseselector(InstanceMapUI::AddPhyEvent));

		p_MyDragPanel = (UIDragPanel*)UIHelper::seekWidgetByName(mainPanel,"DragPanel_22");
		p_MyDragPanel->setTouchEnable(true);
		p_MyDragPanel->setSize(size);
		p_MyDragPanel->scrollToBottomRight(0.001f,false);
		//ScrollInstanceToCenter(11);
		//add releaseSelector for every btn
		for(int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
		{
			//Button
			std::string commonString = "Button_";
			commonString.append(FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_keyValue());
			if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
			{
				UIButton *child = (UIButton *)p_MyDragPanel->getChildByName(commonString.c_str());
				if (child)
				{
					child->setTouchEnable(true);
					child->setPressedActionEnabled(true);
					child->addReleaseEvent(this,coco_releaseselector(InstanceMapUI::BtnInfoEvent));

					//tutorial
					if (i == 0)
					{
						btn_firstInstance = child;
					}
					else if (i == 1)
					{
						btn_secondInstance = child;
					}
					else if (i == 2)
					{
						btn_thirdInstance = child;
					}
					else if (i == 3)
					{
						btn_fouthInstance = child;
					}

					if (FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_open_level() > GameView::getInstance()->myplayer->getActiveRole()->level())
					{
						//child->setColor(ccc3(66,66,66));     //change to gray
						child->setVisible(false);
					}
					else
					{
						//child->setColor(ccc3(255,255,255));     //change to white
						child->setVisible(true);
					}
				}
			}

			//ImageView
			std::string commonString_imageView = "ImageView_";
			commonString_imageView.append(FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_keyValue());
			if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
			{	
				UIImageView *child = (UIImageView *)p_MyDragPanel->getChildByName(commonString_imageView.c_str());
			
				if (child)
				{
					if (FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_open_level() > GameView::getInstance()->myplayer->getActiveRole()->level())
					{
						//child->setColor(ccc3(66,66,66));     //change to gray
						child->setVisible(false);
					}
					else
					{
						//child->setColor(ccc3(255,255,255));     //change to white
						child->setVisible(true);
					}
				}
			}

			//ImageView_luxian
			std::string commonString_imageView_luxian = "ImageView_luxian_";
			commonString_imageView_luxian.append(FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_keyValue());
			if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
			{	
				UIImageView *child = (UIImageView *)p_MyDragPanel->getChildByName(commonString_imageView_luxian.c_str());

				if (child)
				{
					if (FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_open_level() > GameView::getInstance()->myplayer->getActiveRole()->level())
					{
						//child->setColor(ccc3(66,66,66));     //change to gray
						child->setVisible(false);
					}
					else
					{
						//child->setColor(ccc3(255,255,255));     //change to white
						child->setVisible(true);
					}
				}
			}
		}

		//刷新星级显示
		RefreshInstanceStar();

		//显示下一级的主城图标（灰色，不可响应）
		std::vector<int> gapList;
		for(int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
		{
			int gap = FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_open_level() - GameView::getInstance()->myplayer->getActiveRole()->level();
			if (gap > 0)
			{
				gapList.push_back(gap);
			}
		}
		sort(gapList.begin(),gapList.end(),sortByInt);
		int gap = -1;
		if (gapList.size()>0)
		{
			gap = gapList.at(0);
		}
		if (gap != -1)
		{
			for(int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
			{
				//Button
				std::string commonString = "Button_";
				commonString.append(FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_keyValue());
				if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
				{
					UIButton *child = (UIButton *)p_MyDragPanel->getChildByName(commonString.c_str());
					if (child)
					{
						if (FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_open_level() - GameView::getInstance()->myplayer->getActiveRole()->level() == gap)
						{
							child->setTouchEnable(false);
							child->setColor(ccc3(128,128,128));     //change to gray
							child->setVisible(true);
						}
						else
						{
							child->setTouchEnable(true);
							child->setColor(ccc3(255,255,255));     //change to white
						}
					}
				}

				//ImageView
				std::string commonString_imageView_luxian = "ImageView_luxian_";
				commonString_imageView_luxian.append(FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_keyValue());
				if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
				{	
					UIImageView *child = (UIImageView *)p_MyDragPanel->getChildByName(commonString_imageView_luxian.c_str());

					if (child)
					{
						if (FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_open_level() - GameView::getInstance()->myplayer->getActiveRole()->level() == gap)
						{
							//child->setColor(ccc3(66,66,66));     //change to gray
							child->setVisible(false);
						}
					}
				}
			}
		}

// 		ImageView_select = UIImageView::create();
// 		ImageView_select->setTexture("res_ui/fubenditu/qizi.png");
// 		ImageView_select->setAnchorPoint(ccp(.5f,.5f));
// 		panel_tutorial->addChild(ImageView_select);
// 		ImageView_select->setVisible(false);

		// 初始化 体力条（体力值）
		m_pImageView_phy = (UIImageView *)UIHelper::seekWidgetByName(mainPanel,"ImageView_phy");
		m_pLabelBMF_phyValue = (UILabelBMFont *)UIHelper::seekWidgetByName(mainPanel, "LabelBMFont_phyValue");

		int curPhysicalValue = GameView::getInstance()->myplayer->getPhysicalValue();
		int allPhysicalValue = GameView::getInstance()->myplayer->getPhysicalCapacity();

		m_pImageView_phy->setTextureRect(CCRectMake(0, 0, 
			m_pImageView_phy->getContentSize().width * 0.0f,
			m_pImageView_phy->getContentSize().height));

		PhysicalBarEffect* pPhysicalBarEffect = PhysicalBarEffect::create(curPhysicalValue, allPhysicalValue, 1.5f, 
			m_pImageView_phy, m_pLabelBMF_phyValue);
		this->addChild(pPhysicalBarEffect);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(size);
		return true;
	}
	return false;
}

void InstanceMapUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void InstanceMapUI::onExit()
{
	UIScene::onExit();
}

void InstanceMapUI::CloseEvent(CCObject * pSender)
{
	InstanceDetailUI * instanceDetailUI = (InstanceDetailUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceDetailUI);
	if(instanceDetailUI)
		instanceDetailUI->closeAnim();

	this->closeAnim();
}

void InstanceMapUI::scrollViewDidScroll( CCScrollView* view )
{

}

void InstanceMapUI::scrollViewDidZoom( CCScrollView* view )
{

}

void InstanceMapUI::BtnInfoEvent( CCObject * pSender )
{
	UIButton * selectInstance = (UIButton*)pSender;
	std::string buttonName = selectInstance->getName();
	std::string keyValue = buttonName.substr(7);
	int instanceId = -1;
	for (int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();i++)
	{
		if (keyValue == FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_keyValue())
		{
			instanceId = FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_id();

			Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
				sc->endCommand(pSender);
		}
	}

	//设置选中状态的图片位置
// 	ImageView_select->setVisible(true);
// 	ImageView_select->setPosition(ccp(selectInstance->getPosition().x,selectInstance->getPosition().y+20));

	OpenDetailUI(instanceId);
}

void InstanceMapUI::OpenDetailUI( int instanceId )
{
	if (instanceId > 0)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
		CCAssert(mainUILayer != NULL, "should not be nil");

		InstanceDetailUI * _ui = (InstanceDetailUI*)mainUILayer->getChildByTag(kTagInstanceDetailUI);
		if(_ui == NULL)
		{
			InstanceDetailUI * _ui=InstanceDetailUI::create(instanceId);
			_ui->ignoreAnchorPointForPosition(false);
			_ui->setAnchorPoint(ccp(.5f,.5f));  //(1.0f,0.5f)
			_ui->setPosition(ccp(winSize.width/2,winSize.height/2));    //(winSize.width+_ui->getContentSize().width,winSize.height/2)
			_ui->setTag(kTagInstanceDetailUI);
			mainUILayer->addChild(_ui);

// 			CCMoveTo * moveTo = CCMoveTo::create(.2f,ccp(winSize.width,winSize.height/2));
// 			CCEaseBackOut * easebackOut = CCEaseBackOut::create(moveTo);
// 			_ui->runAction(easebackOut);
		}
		else
		{
			_ui->RefreshInstanceInfo(instanceId);
		}
	}
}

void InstanceMapUI::AddPhyEvent( CCObject * pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	GoldStoreUI *goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
	if(goldStoreUI == NULL)
	{		
		goldStoreUI = GoldStoreUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI,0,kTagGoldStoreUI);
		goldStoreUI->ignoreAnchorPointForPosition(false);
		goldStoreUI->setAnchorPoint(ccp(0.5f, 0.5f));
		goldStoreUI->setPosition(ccp(winSize.width/2, winSize.height/2));

		for (int i=0;i <GameView::getInstance()->goldStoreList.size();i++)
		{
			if (GameView::getInstance()->goldStoreList.at(i)->id() == GoldStoreUI::type_phypower)
			{
				goldStoreUI->getTab_function()->setDefaultPanelByIndex(i);
				goldStoreUI->IndexChangedEvent(goldStoreUI->getTab_function());
			}
		}
	}
}

void InstanceMapUI::RefreshInstanceStar()
{
	for(int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
	{
		CFivePersonInstance * tempData = FivePersonInstanceConfigData::s_fivePersonInstance.at(i);
		std::string commonString = "Button_";
		commonString.append(tempData->get_keyValue());
		if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
		{	
			UIButton *child = (UIButton *)p_MyDragPanel->getChildByName(commonString.c_str());
			if (!child || child->isVisible() == false)
				continue;
			
			int star = 0;
			std::map<int, int>::iterator it = FivePersonInstance::getInstance()->s_m_copyStar.begin();
			for ( ; it != FivePersonInstance::getInstance()->s_m_copyStar.end(); ++ it )
			{
				if (it->first == tempData->get_id())
				{
					star = it->second;
					break;
				}
			}

			CCPoint firstPos = child->getPosition();

			std::string panel_star_name = "panel_star_";
			panel_star_name.append(tempData->get_keyValue());

			UIPanel *panel_star = (UIPanel*)p_MyDragPanel->getChildByName(panel_star_name.c_str());
			if (!panel_star)
			{
				panel_star = UIPanel::create();
				p_MyDragPanel->addChild(panel_star);
				panel_star->setWidgetZOrder(2);
				panel_star->setName("panel_star_name");
			}

			//star
			for (int i = 0;i<5;i++)
			{
			 	UIImageView * pIcon = UIImageView::create();
			 	if (i<star)
			 	{
			 		pIcon->setTexture("res_ui/star_on.png");
			 	}
			 	else
			 	{
			 		pIcon->setTexture("res_ui/star_off.png");
			 	}
			 	pIcon->setScale(.8f);
			 	pIcon->setAnchorPoint(ccp(0.5f, 0.5f));
			 	pIcon->setPosition(ccp(firstPos.x - 40 +20*i, firstPos.y+50));
			 	panel_star->addChild(pIcon);
			}
		}
	}
}


void InstanceMapUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void InstanceMapUI::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,40,60);
	tutorialIndicator->setPosition(pos);
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	tutorialIndicator->addHighLightFrameAction(80,80);
	tutorialIndicator->addCenterAnm(80,80);
	panel_tutorial->addCCNode(tutorialIndicator);

// 	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,80,80,true);
// 	tutorialIndicator->setDrawNodePos(ccp(pos.x-402,pos.y-5));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
// 	if (mainScene)
// 	{
// 		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
// 	}
}

void InstanceMapUI::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(panel_tutorial->getRenderer()->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

// 	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
// 	if (mainScene)
// 	{
// 		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
// 		if(teachingGuide != NULL)
// 			teachingGuide->removeFromParent();
// 	}
}

void InstanceMapUI::ScrollInstanceToCenter( int instanceId )
{
	for(int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
	{
		CFivePersonInstance * tempData = FivePersonInstanceConfigData::s_fivePersonInstance.at(i);
		if (tempData->get_id() == instanceId)
		{
			std::string commonString = "Button_";
			commonString.append(tempData->get_keyValue());
			if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
			{	
				UIButton *child = (UIButton *)p_MyDragPanel->getChildByName(commonString.c_str());
				if (!child || child->isVisible() == false)
					continue;

				curInstancePos = child->getPosition();
				CCAction* action = CCSequence::create(
					CCDelayTime::create(0.05f),
					CCCallFunc::create(this, callfunc_selector(InstanceMapUI::callBackScroll)),
					NULL);
				this->runAction(action);
				break;
			}
		}
	}
}

void InstanceMapUI::callBackScroll()
{
	int x = MAX(curInstancePos.x - p_MyDragPanel->getContentSize().width/2, 0);
	x = MIN(x, p_MyDragPanel->getInnerContainerSize().width - p_MyDragPanel->getContentSize().width);
	int y = p_MyDragPanel->getInnerContainerSize().height - curInstancePos.y - p_MyDragPanel->getContentSize().height/2;
	y = MIN(y, p_MyDragPanel->getInnerContainerSize().height - p_MyDragPanel->getContentSize().height);
	y = MAX(y, 0);
	float fWidth = (float)(x)/(p_MyDragPanel->getInnerContainerSize().width - p_MyDragPanel->getContentSize().width);
	float fHeight = (float)(y)/(p_MyDragPanel->getInnerContainerSize().height - p_MyDragPanel->getContentSize().height);
	p_MyDragPanel->jumpToPercentBothDirection(ccp(fWidth*100,fHeight*100));
}

