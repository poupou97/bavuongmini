#ifndef _FIVEPERSONINSTANCE_WAITDIALOGPANELUI_H_
#define _FIVEPERSONINSTANCE_WAITDIALOGPANELUI_H_

#include "../extensions/UIScene.h"

class WaitDialogPanelUI : public UIScene
{
public:
	WaitDialogPanelUI();
	~WaitDialogPanelUI();

	static WaitDialogPanelUI* create();
	bool init();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);

	virtual void onEnter();
	virtual void onExit();

	void WaitEvent(CCObject *pSender);
	void CancelWaitEvent(CCObject *pSender);
};

#endif

