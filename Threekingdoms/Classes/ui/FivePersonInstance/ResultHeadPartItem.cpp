#include "ResultHeadPartItem.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../generals_ui/GeneralsListUI.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/GameStatistics.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/role/BaseFighterConstant.h"
#include "../../ui/GameUIConstant.h"

ResultHeadPartItem::ResultHeadPartItem()
{

}

ResultHeadPartItem::~ResultHeadPartItem()
{

}

ResultHeadPartItem * ResultHeadPartItem::create( int expValue , int index ,int type)
{
	ResultHeadPartItem * resultHeadPartItem = new ResultHeadPartItem();
	if (resultHeadPartItem && resultHeadPartItem->init(expValue,index,type))
	{
		resultHeadPartItem->autorelease();
		return resultHeadPartItem;
	}
	CC_SAFE_DELETE(resultHeadPartItem);
	return NULL;
}

bool ResultHeadPartItem::init( int expValue , int index , int type)
{
	if (UIScene::init())
	{
		int getExpValue = 0;
		int getDmgValue = 0;

		std::string icon_path;
		int roleLevel = 0;

		std::string str_name;
		ccColor3B nameColor;
		//是否出战
		bool isInBattle = false;

		if (index == 0)  //myplayer
		{
			getExpValue = expValue;
			getDmgValue = DamageStatistics::getDamge(GameView::getInstance()->myplayer->getRoleId());
			//头像图标
			icon_path = BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession());
			roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();

			str_name = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
			nameColor = ccc3(0,255,0);
		}
		else
		{
			if (index > 6)
				return false;

			CGeneralBaseMsg * generalBaseMsg = GameView::getInstance()->generalsInLineList.at(index-1);
			CGeneralBaseMsg * generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];
			if (generalBaseMsg->fightstatus() == GeneralsListUI::NoBattle)
			{
				getExpValue = 0;
			}
			else
			{
				if(generalBaseMsg->fightstatus() == GeneralsListUI::InBattle)            //已出战
				{
					getExpValue = expValue*GENERAL_INBATTLE_EXPERIENCE_RATIO;
					isInBattle = true;
				}
				else if (generalBaseMsg->fightstatus() == GeneralsListUI::HoldTheLine)
				{
					getExpValue = expValue*GENERAL_HOLDTHELINE_EXPERIENCE_RATIO;
				}
			}

			getDmgValue = DamageStatistics::getDamge(generalBaseMsg->id());

			//头像图标
			icon_path = "res_ui/generals46X45/";
			icon_path.append(generalMsgFromDb->get_head_photo());
			icon_path.append(".png");

			roleLevel = generalBaseMsg->level();

			str_name = generalMsgFromDb->name();
			nameColor = GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality());
		}

		//create ui
		UIImageView * imageView_frame = UIImageView::create();
		imageView_frame->setTexture("res_ui/kuanga.png");
		imageView_frame->setAnchorPoint(ccp(.0f,.0f));
		imageView_frame->setPosition(ccp(0,50));
		m_pUiLayer->addWidget(imageView_frame);
		//head
		UIImageView * imageView_head = UIImageView::create();
		imageView_head->setTexture(icon_path.c_str());
		imageView_head->setAnchorPoint(ccp(.5f,.5f));
		imageView_head->setPosition(ccp(imageView_frame->getContentSize().width/2,imageView_frame->getContentSize().height/2));
		imageView_frame->addChild(imageView_head);
		if (index == 0)  //myplayer
		{
			imageView_head->setScale(0.8f);
		}
		//出战标识
// 		if(isInBattle)
// 		{
// 			UIImageView * imageView_inBattle = UIImageView::create();
// 			imageView_inBattle->setTexture("res_ui/wujiang/play.png");
// 			imageView_inBattle->setScale(IMAGEVIEW_INBATTLE_INRESULTUI_SCALE);
// 			imageView_inBattle->setAnchorPoint(ccp(.5f,.5f));
// 			imageView_inBattle->setPosition(ccp(imageView_frame->getPosition().x+4,imageView_frame->getContentSize().height-4));
// 			imageView_frame->addChild(imageView_inBattle);
// 		}
		//level frame
// 		UIImageView * imageView_LvFrame = UIImageView::create();
// 		imageView_LvFrame->setTexture("res_ui/lv_kuang.png");
// 		imageView_LvFrame->setAnchorPoint(ccp(1.0f,.0f));
// 		imageView_LvFrame->setPosition(ccp(imageView_frame->getContentSize().width-2,2));
// 		imageView_frame->addChild(imageView_LvFrame);
// 		//level
// 		UILabel * l_level = UILabel::create();
// 		char s_lv[10];
// 		sprintf(s_lv,"%d",roleLevel);
// 		l_level->setText(s_lv);
// 		l_level->setAnchorPoint(ccp(.5f,.5f));
// 		l_level->setPosition(ccp(-imageView_LvFrame->getContentSize().width/2,imageView_LvFrame->getContentSize().height/2));
// 		imageView_LvFrame->addChild(l_level);
		//nameFrame
		UIImageView * imageView_nameFrame = UIImageView::create();
		imageView_nameFrame->setTexture("res_ui/LV4_diaa.png");
		imageView_nameFrame->setScale9Enable(true);
		imageView_nameFrame->setScale9Size(CCSizeMake(72,22));
		imageView_nameFrame->setCapInsets(CCRectMake(11,11,1,1));
		imageView_nameFrame->setAnchorPoint(ccp(.5f,1.0f));
		imageView_nameFrame->setPosition(ccp(imageView_frame->getContentSize().width/2,50));
		m_pUiLayer->addWidget(imageView_nameFrame);
		//name
		UILabel * l_name = UILabel::create();
		l_name->setFontName(APP_FONT_NAME);
		l_name->setFontSize(12);
		l_name->setText(str_name.c_str());
		l_name->setColor(nameColor);
		l_name->setAnchorPoint(ccp(.5f,.5f));
		l_name->setPosition(ccp(0,-11));
		imageView_nameFrame->addChild(l_name);
		//Dmg
		UILabelBMFont * l_dmgName = UILabelBMFont::create();
		l_dmgName->setFntFile("res_ui/font/ziti_10.fnt");
		l_dmgName->setText(StringDataManager::getString("FamilyFightUI_dmg"));
		l_dmgName->setAnchorPoint(ccp(0,0.5f));
		l_dmgName->setPosition(ccp(0,7));
		m_pUiLayer->addWidget(l_dmgName);
		//DmgValue
		NumberRollEffect * dmgEffect = NumberRollEffect::create(getDmgValue,"res_ui/font/ziti_10.fnt",1.5f,0);
		dmgEffect->setAnchorPoint(ccp(.0f,.5f));
		dmgEffect->setPosition(ccp(l_dmgName->getContentSize().width+3,l_dmgName->getPosition().y));
		addChild(dmgEffect);
		//EXP
// 		UILabelBMFont * l_expName = UILabelBMFont::create();
// 		l_expName->setFntFile("res_ui/font/ziti_9.fnt");
// 		l_expName->setText("EXP+");
// 		l_expName->setAnchorPoint(ccp(0,0.5f));
// 		l_expName->setPosition(ccp(0,20));
// 		m_pUiLayer->addWidget(l_expName);
// 		//ExpValue
// 		NumberRollEffect * expEffect = NumberRollEffect::create(getExpValue,"res_ui/font/ziti_9.fnt",1.5f,0);
// 		expEffect->setAnchorPoint(ccp(.0f,.5f));
// 		expEffect->setPosition(ccp(l_expName->getContentSize().width,l_expName->getPosition().y));
// 		addChild(expEffect);

		imageView_frame->setPosition(ccp(0,36));
		imageView_nameFrame->setPosition(ccp(imageView_frame->getContentSize().width/2,36));
		l_dmgName->setVisible(true);
		l_dmgName->setPosition(ccp(0,0));
		dmgEffect->setVisible(true);
		dmgEffect->setPosition(ccp(l_dmgName->getContentSize().width+3,l_dmgName->getPosition().y));

		this->setContentSize(CCSizeMake(imageView_frame->getContentSize().width,imageView_frame->getContentSize().height+imageView_nameFrame->getSize().height+15));


// 		switch(type)
// 		{
// 		case type_allExist:
// 			{
// 				l_dmgName->setVisible(true);
// 				l_dmgName->setPosition(ccp(0,7));
// 				dmgEffect->setVisible(true);
// 				dmgEffect->setPosition(ccp(l_dmgName->getContentSize().width+3,l_dmgName->getPosition().y));
// // 				l_expName->setVisible(true);
// // 				l_expName->setPosition(ccp(0,20));
// // 				expEffect->setVisible(true);
// // 				expEffect->setPosition(ccp(l_expName->getContentSize().width,l_expName->getPosition().y));
// 
// 				this->setContentSize(CCSizeMake(imageView_frame->getContentSize().width,imageView_frame->getContentSize().height+imageView_nameFrame->getSize().height+28));
// 			}
// 			break;
// 		case type_dmgOnly:
// 			{
// 				imageView_frame->setPosition(ccp(0,36));
// 				imageView_nameFrame->setPosition(ccp(imageView_frame->getContentSize().width/2,36));
// 				l_dmgName->setVisible(true);
// 				l_dmgName->setPosition(ccp(0,7));
// 				dmgEffect->setVisible(true);
// 				dmgEffect->setPosition(ccp(l_dmgName->getContentSize().width+3,l_dmgName->getPosition().y));
// // 				l_expName->setVisible(false);
// // 				expEffect->setVisible(false);
// 
// 				this->setContentSize(CCSizeMake(imageView_frame->getContentSize().width,imageView_frame->getContentSize().height+imageView_nameFrame->getSize().height+15));
// 			}
// 			break;
// 		case type_expOnly:
// 			{
// 				imageView_frame->setPosition(ccp(0,36));
// 				imageView_nameFrame->setPosition(ccp(imageView_frame->getContentSize().width/2,36));
// 				l_dmgName->setVisible(false);
// 				dmgEffect->setVisible(false);
// 				
// // 				l_expName->setVisible(true);
// // 				expEffect->setVisible(true);
// // 				l_expName->setPosition(ccp(0,7));
// // 				expEffect->setPosition(ccp(l_expName->getContentSize().width,l_expName->getPosition().y));
// 
// 				this->setContentSize(CCSizeMake(imageView_frame->getContentSize().width,imageView_frame->getContentSize().height+imageView_nameFrame->getSize().height+15));
// 			}
// 			break;
// 		}

		
		return true;
	}
	return false;
}

