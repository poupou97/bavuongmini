#include "InstanceEndUI.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CFivePersonInstanceEndInfo.h"
#include "RewardCardItem.h"
#include "../../messageclient/element/CRewardProp.h"
#include "../../messageclient/element/CVipInfo.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../GameUIConstant.h"
#include "../../utils/GameUtils.h"
#include "ResultHeadPartItem.h"
#include "../../gamescene_state/GameSceneCamera.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "FivePersonInstance.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../gamescene_state/MainAnimationScene.h"

#define UISCENE_MAIN_SCENE_TAG 1000
#define TAG_IMAGEVIEW_STAR_BASE 500
#define kTagULayer 600
#define TAG_LIGHTANIMATION 700

InstanceEndUI::InstanceEndUI():
remainTime(60000),
countDown_startTime(0)
{
	//coordinate for card
	for (int m = 1;m>=0;m--)
	{
		for (int n = 5;n>=0;n--)
		{
			int _m = 1-m;
			int _n = 5-n;

			Coordinate temp;
			temp.x = 205+_n*94 ;
			temp.y = 187+m*119;

			Coordinate_card.push_back(temp);
		}
	}
}

InstanceEndUI::~InstanceEndUI()
{
	std::vector<CRewardProp*>::iterator iter;
	for (iter = curRewardPropList.begin(); iter != curRewardPropList.end(); ++iter)
	{
		delete *iter;
	}
	curRewardPropList.clear();

	delete curFivePersonInstanceEndInfo;
}

InstanceEndUI * InstanceEndUI::create(CFivePersonInstanceEndInfo * fivePersonInstanceEndInfo)
{
	InstanceEndUI * _ui=new InstanceEndUI();
	if (_ui && _ui->init(fivePersonInstanceEndInfo))
	{
		_ui->autorelease();
		return _ui;
	}
	CC_SAFE_DELETE(_ui);
	return NULL;
}

bool InstanceEndUI::init(CFivePersonInstanceEndInfo * fivePersonInstanceEndInfo)
{
	if (UIScene::init())
	{
		countDown_startTime = GameUtils::millisecondNow();
		m_nAllNum = 3;
		m_nRemainNum = 3;

		VipConfigData::TypeIdAndVipLevel typeAndLevel;
		typeAndLevel.typeName = StringDataManager::getString("vip_function_instance_reward");
		typeAndLevel.vipLevel = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel();

		std::map<VipConfigData::TypeIdAndVipLevel,CVipInfo*>::const_iterator cIter;
		cIter = VipConfigData::s_vipConfig.find(typeAndLevel);
		if (cIter == VipConfigData::s_vipConfig.end()) // 没找到就是指向END了  
		{
		}
		else
		{
			CVipInfo * tempVipInfo = cIter->second;
			m_nAllNum += tempVipInfo->get_add_number();
			m_nRemainNum += tempVipInfo->get_add_number();
		}

		CCSize size = CCDirector::sharedDirector()->getVisibleSize();
		curFivePersonInstanceEndInfo = new CFivePersonInstanceEndInfo();
		curFivePersonInstanceEndInfo->copyFrom(fivePersonInstanceEndInfo);

		MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		if (!mainscene)
			return false;

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCDelayTime::create(END_BATTLE_BEFORE_SLOWMOTION_DURATION),
			CCCallFunc::create(this, callfunc_selector(InstanceEndUI::startSlowMotion)),
			CCCallFunc::create(this,callfunc_selector(InstanceEndUI::addLightAnimation)),
			CCDelayTime::create(END_BATTLE_SLOWMOTION_DURATION),
			CCCallFunc::create(this, callfunc_selector(InstanceEndUI::endSlowMotion)),
			CCCallFunc::create(this, callfunc_selector(InstanceEndUI::showUIAnimation)),
			CCDelayTime::create(2.0f),
			CCCallFunc::create(this, callfunc_selector(InstanceEndUI::createAnimation)),
			NULL);
		this->runAction(action);	

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(size);
		return true;
	}
	return false;
}

void InstanceEndUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void InstanceEndUI::onExit()
{
	UIScene::onExit();
}

void InstanceEndUI::callBackExit(CCObject * pSender)
{
	// request to exit the instance
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1924);
	this->closeAnim();
}

void InstanceEndUI::exitInstanceCallback(CCObject* pSender)
{
	// request to exit the instance
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1924);
}

void InstanceEndUI::AllCardTurnToBack()
{
	for(int i = 0;i<12;++i)
	{
		if (u_layer->getChildByTag(RewardCardItemBaseTag +i))
		{
			RewardCardItem * rewardCardItem = (RewardCardItem*)u_layer->getChildByTag(RewardCardItemBaseTag +i);
			rewardCardItem->FlopAnimationToBack();
		}
	}
}

void InstanceEndUI::BeginShuffle()
{
 	CCPoint _pos = ccp((Coordinate_card.at(5).x+Coordinate_card.at(6).x)/2,(Coordinate_card.at(5).y+Coordinate_card.at(6).y)/2);

// 	int indexArray[12];
// 	for (int i = 0;i<12;i++)
// 	{
// 		indexArray[i] = i;
// 	}

	for(int i = 0;i<12;++i)
	{
		if (u_layer->getChildByTag(RewardCardItemBaseTag +curRewardPropList.at(i)->grid()))
		{
// 			int random_index = (int)(CCRANDOM_0_1()*(12-i)); //生成0-11的随机数，包括0和11
// 			int random_value = indexArray[random_index];
// 			CCLOG("random_index = %d",random_index);
// 			CCLOG("random_value = %d",random_value);
// 			for(int j=random_index;j<12-i-1;j++)
// 			{
// 				indexArray[j]=indexArray[j+1];
// 			}
// 			indexArray[12-i-1] = NULL;

			RewardCardItem * rewardCardItem = (RewardCardItem*)u_layer->getChildByTag(RewardCardItemBaseTag +i);
			CCFiniteTimeAction*  action1 = CCSequence::create(
				CCMoveTo::create(REWARD_CARD_MOVE_TIME,_pos),
				CCDelayTime::create(0.5f),
				//CCMoveTo::create(0.7f,ccp(Coordinate_card.at(random_value).x,Coordinate_card.at(random_value).y)),
				CCMoveTo::create(REWARD_CARD_MOVE_TIME,ccp(Coordinate_card.at(i).x,Coordinate_card.at(i).y)),
				CCCallFunc::create(rewardCardItem, callfunc_selector(RewardCardItem::setBtnTouchEnabled)),
				CCCallFunc::create(rewardCardItem, callfunc_selector(RewardCardItem::presentDesLable)),
				NULL);
			rewardCardItem->runAction(action1);
		}
	}

	CCSequence * temp = CCSequence::create(
		CCDelayTime::create(REWARD_CARD_MOVE_TIME+0.5f+REWARD_CARD_MOVE_TIME),
		CCCallFunc::create(this, callfunc_selector(InstanceEndUI::PresentBtnClose)),
		CCCallFunc::create(this, callfunc_selector(InstanceEndUI::BeginToturial)),
		NULL
		);
	this->runAction(temp);
}

void InstanceEndUI::update( float dt )
{
	if (remainTime >0)
	{
		remainTime -= 1000;

		std::string str_remainTime = "";
		char s_remainTime[10];
		sprintf(s_remainTime,"%d",remainTime/1000);
		str_remainTime.append(s_remainTime);
		l_remainTime_panel1->setText(str_remainTime.c_str());
		l_remainTime_panel2->setText(str_remainTime.c_str());

// 		char str_ingot[20];
// 		sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
// 		l_IngotValue->setText(str_ingot);
	}
	else
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1924);
		this->closeAnim();
	}
}

void InstanceEndUI::setPayForCardValue()
{
	//l_payForCardValue->setText(str.c_str());
	//翻牌费用 //剩余次数
	std::string str_payForCard = "";
	std::string path_icon = "";
	if (this->getRemainNumValue() == 0)   
	{
		str_payForCard.append("100");
		//str_payForCard.append(StringDataManager::getString("skillui_goldingold"));
		path_icon = "res_ui/ingot.png";
	}
	else if(this->getRemainNumValue()  == 1)       //100元宝  
	{
		str_payForCard.append("100");
		//str_payForCard.append(StringDataManager::getString("skillui_goldingold"));
		path_icon = "res_ui/ingot.png";
	}
	else if(this->getRemainNumValue()  == 2)     //1000金币
	{
		str_payForCard.append("1000");
		//str_payForCard.append(StringDataManager::getString("skillui_gold"));
		path_icon = "res_ui/coins.png";
	}
	else
	{
		str_payForCard.append(StringDataManager::getString("game_free"));
		path_icon = "";
	}

	for(int i = 0;i<12;++i)
	{
		RewardCardItem * rewardCardItem = (RewardCardItem*)u_layer->getChildByTag(RewardCardItemBaseTag +i);
		if (rewardCardItem)
		{
			rewardCardItem->RefreshCostValue(str_payForCard,path_icon);
		}
	}
}

void InstanceEndUI::setRemainNumValue(int _value )
{
	m_nRemainNum = _value;
	char s_remainNum [10];
	sprintf(s_remainNum,"%d",m_nRemainNum);
	l_remainNum->setText(s_remainNum);
}

int InstanceEndUI::getRemainNumValue()
{
	return m_nRemainNum;
}

void InstanceEndUI::setAllNumValue(int _value)
{
	m_nAllNum = _value;
	char s_allNum [10];
	sprintf(s_allNum,"%d",m_nAllNum);
	l_allFreeNum->setText(s_allNum);
}

int InstanceEndUI::getAllNumValue()
{
	return m_nAllNum;
}

void InstanceEndUI::createAnimation()
{
	CCAssert(curFivePersonInstanceEndInfo->rewardProps.size() >= 12, "card num < 12");

	CCSize size=CCDirector::sharedDirector()->getVisibleSize();

	for (unsigned int i =0;i<curFivePersonInstanceEndInfo->rewardProps.size();i++)
	{
		CRewardProp * rewardProp = new CRewardProp();
		rewardProp->CopyFrom(*curFivePersonInstanceEndInfo->rewardProps.at(i));
		curRewardPropList.push_back(rewardProp);
	}

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	u_layer=UILayer::create();
	u_layer->ignoreAnchorPointForPosition(false);
	u_layer->setAnchorPoint(ccp(0.5f,0.5f));
	u_layer->setPosition(ccp(winSize.width/2,winSize.height/2));
	u_layer->setContentSize(CCSizeMake(800,480));
	u_layer->setTag(kTagULayer);
	addChild(u_layer);

	layer_base  = UILayer::create();
	u_layer->addChild(layer_base);

	layer_1 = UILayer::create();
	u_layer->addChild(layer_1);

	layer_2 = UILayer::create();
	u_layer->addChild(layer_2);

	CCSize sizeMengBan = CCSizeMake(winSize.width + 20, winSize.height + 20);

	UIImageView *mengban = UIImageView::create();
	mengban->setTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enable(true);
	mengban->setScale9Size(sizeMengBan);
	mengban->setAnchorPoint(ccp(0,0));
	mengban->setPosition(ccp(-10, -10));
	m_pUiLayer->addWidget(mengban);

	//从导出文件异步加载动画
	CCArmatureDataManager::sharedArmatureDataManager()->addArmatureFileInfo("res_ui/uiflash/winLogo/winLogo0.png","res_ui/uiflash/winLogo/winLogo0.plist","res_ui/uiflash/winLogo/winLogo.ExportJson");
	//根据动画名称创建动画精灵
	CCArmature * armature = CCArmature::create("winLogo");
	//播放指定动作
	//armature->getAnimation()->playByIndex(0,-1,-1,ANIMATION_NO_LOOP);
	armature->getAnimation()->play("Animation_begin");
	//修改属性
	armature->setScale(1.0f);
	//设置动画精灵位置
	armature->setPosition(ccp(UI_DESIGN_RESOLUTION_WIDTH/2,UI_DESIGN_RESOLUTION_HEIGHT/2));

	armature->getAnimation()->setMovementEventCallFunc(this,movementEvent_selector(InstanceEndUI::ArmatureFinishCallback));  
	//添加到当前页面
	layer_base->addChild(armature);

	int space = 8;
	int width = 45;
	float firstPos_x = 400- space*0.5f*(curFivePersonInstanceEndInfo->get_star()-1) - width*0.5f*(curFivePersonInstanceEndInfo->get_star()-1);
	for (int i = 0;i<curFivePersonInstanceEndInfo->get_star();++i)
	{
	 	if (i >= 5)
	 		continue;
	 
	 	CCSprite* sprite_star = CCSprite::create("res_ui/star2.png");
	 	sprite_star->setAnchorPoint(ccp(0.5f,0.5f));
	 	sprite_star->setPosition(ccp(firstPos_x+(width+space)*i,200));
	 	layer_1->addChild(sprite_star);
	 	sprite_star->setTag(TAG_IMAGEVIEW_STAR_BASE+i);
	 	sprite_star->setVisible(false);
		sprite_star->setScale(10.0f);
		sprite_star->setOpacity(25);


	 	CCFiniteTimeAction*  win_action = CCSequence::create(
	 		CCDelayTime::create(0.5f+0.25f*i),
	 		CCShow::create(),
			CCEaseExponentialIn::create(CCSpawn::create(
				CCScaleTo::create(0.25f,1.0f),
				CCFadeTo::create(0.25f, 255),
				NULL)),
			CCCallFunc::create(this, callfunc_selector(InstanceEndUI::starActionCallBack)),
	 		NULL);
	 	sprite_star->runAction(win_action);
	 
	 	CCSequence * sequence = CCSequence::create(CCDelayTime::create(2.2f),
	 		CCEaseOut::create(CCMoveTo::create(0.2f,ccp(sprite_star->getPosition().x,335)),2.2f),
	 		NULL);
	 	sprite_star->runAction(sequence);
	}

	CCSequence * sequence = CCSequence::create(
		CCDelayTime::create(2.55f),
		CCCallFunc::create(this,callfunc_selector(InstanceEndUI::createUI)),
		CCDelayTime::create(3.0f),
		CCCallFunc::create(this,callfunc_selector(InstanceEndUI::showAllRewarsCard)),
		NULL
		);
	this->runAction(sequence);

	/************************************************************/
// 	//胜利背后的佛光
// 	UIImageView * imageView_light = UIImageView::create();
// 	imageView_light->setTexture("res_ui/lightlight.png");
// 	imageView_light->setAnchorPoint(ccp(0.5f,0.5f));
// 	imageView_light->setPosition(ccp(400,240));
// 	u_layer->addWidget(imageView_light);
// 	imageView_light->setVisible(false);
// 	//胜利图标
// 	UIImageView * imageView_result = UIImageView::create();
// 	imageView_result->setTexture("res_ui/jiazuzhan/win.png");
// 	imageView_result->setAnchorPoint(ccp(0.5f,0.5f));
// 	imageView_result->setPosition(ccp(400,240));
// 	imageView_result->setScale(RESULT_UI_RESULTFLAG_BEGINSCALE);
// 	u_layer->addWidget(imageView_result);
// 
// 	int space = 8;
// 	int width = 45;
// 	float firstPos_x = 400- space*0.5f*(curFivePersonInstanceEndInfo->get_star()-1) - width*0.5f*(curFivePersonInstanceEndInfo->get_star()-1);
// 
// 	for (int i = 0;i<curFivePersonInstanceEndInfo->get_star();++i)
// 	{
// 		if (i >= 5)
// 			continue;
// 
// 		UIImageView* imageView_star = UIImageView::create();
// 		imageView_star->setTexture("res_ui/star2.png"); 
// 		imageView_star->setAnchorPoint(ccp(0.5f,0.5f));
// 		imageView_star->setPosition(ccp(firstPos_x+(width+space)*i,195));
// 		layer_1->addWidget(imageView_star);
// 		imageView_star->setTag(TAG_IMAGEVIEW_STAR_BASE+i);
// 		imageView_star->setVisible(false);
// 		imageView_star->setScale(2.5f);
// 		CCFiniteTimeAction*  win_action = CCSequence::create(
// 			CCDelayTime::create(0.25f+0.3f*i),
// 			CCShow::create(),
// 			CCEaseExponentialIn::create(CCScaleTo::create(0.25f,1.0f)),
// 			NULL);
// 		imageView_star->runAction(win_action);
// 
// 		CCSequence * sequence = CCSequence::create(CCDelayTime::create(2.0f+RESULT_UI_RESULTFLAG_SCALECHANGE_TIME),
// 			CCMoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,ccp(imageView_star->getPosition().x,355)),
// 			NULL);
// 		imageView_star->runAction(sequence);
// 	}
// 
// 	CCSequence * sequence = CCSequence::create(CCScaleTo::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME,1.0f,1.0f),
// 		CCDelayTime::create(2.0f),
// 		CCMoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,ccp(400,403)),
// 		CCDelayTime::create(0.05f),
// 		CCCallFunc::create(this,callfunc_selector(InstanceEndUI::createUI)),
// 		CCDelayTime::create(5.0f),
// 		CCCallFunc::create(this,callfunc_selector(InstanceEndUI::showAllRewarsCard)),
// 		NULL);
// 	imageView_result->runAction(sequence);
// 
// 	CCRotateBy * rotateAnm = CCRotateBy::create(4.0f,360);
// 	CCRepeatForever * repeatAnm = CCRepeatForever::create(CCSequence::create(rotateAnm,NULL));
// 	CCSequence * sequence_light = CCSequence::create(
// 		CCDelayTime::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME),
// 		CCShow::create(),
// 		CCDelayTime::create(2.0f),
// 		CCMoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,ccp(400,403)),
// 		NULL);
// 	imageView_light->runAction(sequence_light);
// 	imageView_light->runAction(repeatAnm);
}

void InstanceEndUI::createUI()
{
	CCSize size = CCDirector::sharedDirector()->getVisibleSize();
	if(LoadSceneLayer::instanceEndUiPanel->getWidgetParent() != NULL)
	{
		LoadSceneLayer::instanceEndUiPanel->removeFromParentAndCleanup(false);
	}

	UIPanel *mainPanel=LoadSceneLayer::instanceEndUiPanel;
	mainPanel->setAnchorPoint(ccp(0.5f,0.5f));
	mainPanel->setPosition(ccp(size.width/2,size.height/2-20));
	mainPanel->setWidgetTag(UISCENE_MAIN_SCENE_TAG);
	m_pUiLayer->addWidget(mainPanel);

	panel_1= (UIPanel *)UIHelper::seekWidgetByName(mainPanel,"Panel_InstanceEnd_1");
	panel_1->setVisible(true);
	layer_1->setVisible(true);
	panel_2= (UIPanel *)UIHelper::seekWidgetByName(mainPanel,"Panel_InstanceEnd_2");
	panel_2->setVisible(false);
	layer_2->setVisible(false);

// 	const char * secondStr = StringDataManager::getString("UIName_fu");
// 	const char * thirdStr = StringDataManager::getString("UIName_ben");
// 	CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
// 	atmature->setPosition(ccp(89,215));
// 	u_layer->addChild(atmature);
// 
	btn_close= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_close");
	CCAssert(btn_close != NULL, "should not be nil");
	btn_close->setTouchEnable(true);
	btn_close->setPressedActionEnabled(true);
	btn_close->addReleaseEvent(this,coco_releaseselector(InstanceEndUI::callBackExit));
	btn_close->setVisible(false);

// 	imageView_Win = UIImageView::create();
// 	imageView_Win->setTexture("res_ui/instance_end/yesyes.png"); 
// 	imageView_Win->setAnchorPoint(ccp(0.5f,0.5f));
// 	imageView_Win->setPosition(ccp(217,382));
// 	u_layer->addWidget(imageView_Win);
// 	imageView_Win->setScale(2.5f);
// 	CCFiniteTimeAction*  win_action = CCSequence::create(
// 		CCEaseExponentialIn ::create(CCScaleTo::create(0.4f,1.0f)),
// 		NULL);
// 	imageView_Win->runAction(win_action);
// 
// 	for (int i = 0;i<curFivePersonInstanceEndInfo->get_star();++i)
// 	{
// 		if (i >= 5)
// 			continue;
// 
// 		UIImageView* imageView_star = UIImageView::create();
// 		imageView_star->setTexture("res_ui/star_on.png"); 
// 		imageView_star->setAnchorPoint(ccp(0.f,0.f));
// 		imageView_star->setPosition(ccp(414+27*i,390));
// 		u_layer->addWidget(imageView_star);
// 		imageView_star->setVisible(false);
// 		imageView_star->setScale(2.5f);
// 		CCFiniteTimeAction*  win_action = CCSequence::create(
// 			CCDelayTime::create(0.45f+0.3f*i),
// 			CCShow::create(),
// 			CCEaseExponentialIn::create(CCScaleTo::create(0.25f,1.0f)),
// 			NULL);
// 		imageView_star->runAction(win_action);
// 	}

	l_remainTime_panel1 = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_RemainingTime_value");
	l_remainTime_panel2 = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_RemainingTime_value_0");
	//翻牌次数
	l_allFreeNum = (UILabel *)UIHelper::seekWidgetByName(panel_2,"Label_retainMax");
	setAllNumValue(m_nAllNum);
	l_remainNum = (UILabel *)UIHelper::seekWidgetByName(panel_2,"Label_retainNum");
	setRemainNumValue(m_nRemainNum);
	//当前元宝
// 	l_IngotValue = (UILabel *)UIHelper::seekWidgetByName(panel_2,"Label_IngotValue");
// 	char s_playerGoldIngot[20];
// 	sprintf(s_playerGoldIngot,"%d",GameView::getInstance()->getPlayerGoldIngot());
// 	l_IngotValue->setText(s_playerGoldIngot);
	//翻牌花费
	l_payForCardValue = (UILabel *)UIHelper::seekWidgetByName(panel_2,"Label_PayForCard_value");
	//setPayForCardValue(StringDataManager::getString("game_free"));
	setPayForCardValue();
	//经验
// 	UILabel * l_exp = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_RewardExp");
// 	char s_exp[20];
// 	sprintf(s_exp,"%d",curFivePersonInstanceEndInfo->get_exp());
// 	l_exp->setText(s_exp);
 	//金币
	UILabel * l_gold = (UILabel *)UIHelper::seekWidgetByName(panel_1,"Label_RewardCoins");
	char s_gold[20];
	sprintf(s_gold,"%d",curFivePersonInstanceEndInfo->get_gold());
	l_gold->setText(s_gold);
	//龙魂精华
	UILabel * l_musouValue = (UILabel *)UIHelper::seekWidgetByName(panel_1,"Label_musouValue");
	char s_musou[20];
	sprintf(s_musou,"%d",curFivePersonInstanceEndInfo->get_dragonValue());
	l_musouValue->setText(s_musou);

	remainTime -= (GameUtils::millisecondNow() - countDown_startTime - 6);

	std::string str_remainTime = "";
	char s_remainTime[10];
	sprintf(s_remainTime,"%d",remainTime/1000);
	str_remainTime.append(s_remainTime);
	l_remainTime_panel1->setText(str_remainTime.c_str());
	l_remainTime_panel2->setText(str_remainTime.c_str());

	this->schedule(schedule_selector(InstanceEndUI::update),1.0f);

	//展示武将得经验
	int space = 19;
	int width = 66;
	int allsize = GameView::getInstance()->generalsInLineList.size()+1;
	float firstPos_x = 400- (space+width)*0.5f*(allsize-1);

	for (int i = 0;i<allsize;++i)
	{
		if (i >= 6)
			continue;

		ResultHeadPartItem * temp = ResultHeadPartItem::create(curFivePersonInstanceEndInfo->get_exp(),i);
		temp->ignoreAnchorPointForPosition(false);
		temp->setAnchorPoint(ccp(0.5f,0.5f));
		temp->setPosition(ccp(firstPos_x+(width+space)*i,148));
		layer_1->addChild(temp);
	}
}

void InstanceEndUI::showUIAnimation()
{
// 	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
// 	if (mainscene != NULL)
// 		mainscene->addInterfaceAnm("fbtg/fbtg.anm");

	MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
	if (mainAnmScene)
	{
		mainAnmScene->addInterfaceAnm("fbtg/fbtg.anm");
	}
}

void InstanceEndUI::startSlowMotion()
{
	CCDirector::sharedDirector()->getScheduler()->setTimeScale(0.2f);
}
void InstanceEndUI::endSlowMotion()
{
	CCDirector::sharedDirector()->getScheduler()->setTimeScale(1.0f);
}

void InstanceEndUI::showAllRewarsCard()
{
	for (int i = 0;i<curFivePersonInstanceEndInfo->get_star();++i)
	{
		if (layer_1->getChildByTag(TAG_IMAGEVIEW_STAR_BASE+i))
		{
			CCFadeOut *fadeoutAction = CCFadeOut::create(0.5f);
			layer_1->getChildByTag(TAG_IMAGEVIEW_STAR_BASE+i)->runAction(fadeoutAction);
		}
	}

 	panel_1->setVisible(false);
 	layer_1->setVisible(false);
 	panel_2->setVisible(true);
 	layer_2->setVisible(true);
 	
// 	panel_1->setVisible(true);
// 	layer_1->setVisible(true);
// 	panel_2->setVisible(true);
// 	layer_2->setVisible(true);
// 	CCFadeOut * action_panel_1 = CCFadeOut::create(0.5f);
// 	panel_1->runAction(action_panel_1);
// 	CCFadeOut * action_layer_1 = CCFadeOut::create(0.5f);
// 	layer_1->runAction(action_layer_1);
// 	CCFadeIn * action_panel_2 = CCFadeIn::create(0.5f);
// 	panel_2->runAction(action_panel_2);
// 	CCFadeIn * action_layer_2 = CCFadeIn::create(0.5f);
// 	layer_2->runAction(action_layer_2);

	CCSize size = CCDirector::sharedDirector()->getVisibleSize();
	for(int i = 0;i<12;++i)
	{
		RewardCardItem * rewardCardItem = RewardCardItem::create(curFivePersonInstanceEndInfo->rewardProps.at(i),curFivePersonInstanceEndInfo->get_timeDouble());
		rewardCardItem->ignoreAnchorPointForPosition(false);
		rewardCardItem->setAnchorPoint(ccp(0.5f,0.5f));
		rewardCardItem->setPosition(ccp(Coordinate_card.at(i).x+size.width+200,Coordinate_card.at(i).y));
		rewardCardItem->setTag(RewardCardItemBaseTag+curFivePersonInstanceEndInfo->rewardProps.at(i)->grid());
		u_layer->addChild(rewardCardItem);
	}

	for(int i = 0;i<12;++i)
	{
		RewardCardItem * rewardCardItem = (RewardCardItem*)u_layer->getChildByTag(RewardCardItemBaseTag+curFivePersonInstanceEndInfo->rewardProps.at(i)->grid());
		if (rewardCardItem)
		{
			CCMoveTo * moveBack = CCMoveTo::create(.7f,ccp(Coordinate_card.at(i).x,Coordinate_card.at(i).y));
			rewardCardItem->runAction(CCSequence::create(CCDelayTime::create(.3f),moveBack,NULL));
		}
	}

	CCFiniteTimeAction*  action1 = CCSequence::create(
		CCDelayTime::create(2.0f),
		CCCallFunc::create(this, callfunc_selector(InstanceEndUI::AllCardTurnToBack)),
		CCDelayTime::create(0.7f),
		CCCallFunc::create(this,callfunc_selector(InstanceEndUI::BeginShuffle)),
		NULL);

	this->stopAllActions();
	this->runAction(action1);
}

UILayer * InstanceEndUI::getULayer()
{
	UILayer * temp = (UILayer*)this->getChildByTag(kTagULayer);
	if (temp)
	{
		return temp;
	}

	return NULL;
}

void InstanceEndUI::ArmatureFinishCallback( cocos2d::extension::CCArmature *armature, cocos2d::extension::MovementEventType movementType, const char *movementID )
{
	std::string id = movementID;
	if (id.compare("Animation_begin") == 0)
	{
		armature->stopAllActions();
		armature->getAnimation()->play("Animation1_end");
	}
}

void InstanceEndUI::starActionCallBack()
{
	GameSceneCamera::shakeScreen(0.25f, 3);
}

void InstanceEndUI::PresentBtnClose()
{
	btn_close->setVisible(true);
}

void InstanceEndUI::BeginToturial()
{
	if (GameView::getInstance()->myplayer->getActiveRole()->level()>15)
		return;

	if(FivePersonInstance::getStatus() != FivePersonInstance::status_in_instance)
	{
		return;
	}

	CFivePersonInstance * fivePersonInstance;
	bool isExist = false;
	for (int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();i++)
	{
		fivePersonInstance = FivePersonInstanceConfigData::s_fivePersonInstance.at(i);
		if (FivePersonInstance::getCurrentInstanceId() == fivePersonInstance->get_id())
		{
			isExist = true;
			break;
		}
	}

	if (!isExist)
		return;

	if (fivePersonInstance->get_open_level() > 15)
		return;

	//如果该副本星级为零，开启教学，指引他点第一个卡牌进行翻牌
// 	bool isNeedOpen = true;
// 	std::map<int, int>::iterator it = FivePersonInstance::getInstance()->s_m_copyStar.begin();
// 	for ( ; it != FivePersonInstance::getInstance()->s_m_copyStar.end(); ++ it )
// 	{
// 		if (it->first == FivePersonInstance::getInstance()->getCurrentInstanceId())
// 		{
// 			int star =  it->second;
// 			if (star > 0)
// 			{
// 				isNeedOpen = false;
// 			}
// 
// 			break;
// 		}
// 	}
// 	if (!isNeedOpen)
// 		return;
	
	addToturial();
}

void InstanceEndUI::addToturial()
{
	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(StringDataManager::getString("fivePerson_pleaseGetYourAwards"),ccp(0,0),CCTutorialIndicator::Direction_LU);
	tutorialIndicator->setPosition(ccp(165,250));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	tutorialIndicator->addHighLightFrameAction(83,114);
	tutorialIndicator->addCenterAnm(83,114);
	u_layer->addChild(tutorialIndicator);
}

void InstanceEndUI::removeToturial()
{
	if (u_layer->getChildByTag(CCTUTORIALINDICATORTAG))
	{
		u_layer->getChildByTag(CCTUTORIALINDICATORTAG)->removeFromParent();
	}
}

void InstanceEndUI::addLightAnimation()
{
	CCSize size = CCDirector::sharedDirector()->getVisibleSize();
	std::string animFileName = "animation/texiao/renwutexiao/SHANGUANG/shanguang.anm";
	CCLegendAnimation *la_light = CCLegendAnimation::create(animFileName);
	if (la_light)
	{
		la_light->setPlayLoop(false);
		la_light->setReleaseWhenStop(true);
		la_light->setPlaySpeed(5.0f);
		la_light->setScale(0.85f);
		la_light->setPosition(ccp(size.width/2,size.height/2));
		la_light->setTag(TAG_LIGHTANIMATION);
		GameView::getInstance()->getMainAnimationScene()->addChild(la_light);
	}
}
