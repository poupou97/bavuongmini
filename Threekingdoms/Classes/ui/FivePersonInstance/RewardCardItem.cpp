#include "RewardCardItem.h"
#include "../../messageclient/element/CRewardProp.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "GameView.h"
#include "AppMacros.h"
#include "../GameUIConstant.h"
#include "../../utils/StaticDataManager.h"
#include "InstanceEndUI.h"
#include "../../gamescene_state/MainScene.h"

#define voidFramePath "res_ui/smdi_none.png"

#define whiteFramePath "res_ui/smdi_white.png"
#define greenFramePath "res_ui/smdi_green.png"
#define blueFramePath "res_ui/smdi_bule.png"
#define purpleFramePath "res_ui/smdi_purple.png"
#define orangeFramePath "res_ui/smdi_orange.png"


RewardCardItem::RewardCardItem():
isFloped(false),
grid(-1),
isFrontOrBack(true)
{
}


RewardCardItem::~RewardCardItem()
{
}

RewardCardItem * RewardCardItem::create( CRewardProp *rewardProp , int timeDouble)
{
	RewardCardItem * rewardCardItem = new RewardCardItem();
	if (rewardCardItem && rewardCardItem->init(rewardProp,timeDouble))
	{
		rewardCardItem->autorelease();
		return rewardCardItem;
	}
	CC_SAFE_DELETE(rewardCardItem);
	return NULL;
}

bool RewardCardItem::init( CRewardProp *rewardProp , int timeDouble)
{
	if (UIScene::init())
	{
		isFrontOrBack = true;
		grid = rewardProp->grid();

		//���Ʊ���
		btn_backCardFrame = UIButton::create();
		btn_backCardFrame->setTextures("res_ui/instance_end/card.png","res_ui/instance_end/card.png","");
		btn_backCardFrame->setAnchorPoint(ccp(0.5f,0.5f));
		btn_backCardFrame->setPosition(ccp(0,0));
		btn_backCardFrame->setTouchEnable(false);
		btn_backCardFrame->addReleaseEvent(this,coco_releaseselector(RewardCardItem::BackCradEvent));
		m_pUiLayer->addWidget(btn_backCardFrame);
		btn_backCardFrame->setVisible(true);

		UIImageView* image_desFrame = UIImageView::create();
		image_desFrame->setTexture("res_ui/zhezhao80.png");
		image_desFrame->setScale9Enable(true);
		image_desFrame->setScale9Size(CCSizeMake(72,40));
		image_desFrame->setAnchorPoint(ccp(0.5f,0.5f));
		image_desFrame->setPosition(ccp(0,0));
		btn_backCardFrame->addChild(image_desFrame);
		image_desFrame->setName("image_desFrame");
		image_desFrame->setVisible(false);

		l_des = UILabel::create();
		l_des->setText(StringDataManager::getString("fivePerson_turnCard"));
		l_des->setTextAreaSize(CCSizeMake(70,0));
		l_des->setTextVerticalAlignment(kCCVerticalTextAlignmentCenter);
		l_des->setTextHorizontalAlignment(kCCTextAlignmentCenter);
		l_des->setFontName(APP_FONT_NAME);
		l_des->setFontSize(16);
		//l_des->setColor(ccc3(0,255,0));
		l_des->setAnchorPoint(ccp(0.5f,0.5f));
		l_des->setRotationY(180);
		btn_backCardFrame->addChild(l_des);
		l_des->setPosition(ccp(0,10));
		l_des->setVisible(false);

		l_costValue = UILabel::create();
		l_costValue->setText(StringDataManager::getString("game_free"));
		l_costValue->setTextAreaSize(CCSizeMake(70,0));
		l_costValue->setTextVerticalAlignment(kCCVerticalTextAlignmentCenter);
		l_costValue->setTextHorizontalAlignment(kCCTextAlignmentCenter);
		l_costValue->setFontName(APP_FONT_NAME);
		l_costValue->setFontSize(16);
		//l_des->setColor(ccc3(0,255,0));
		l_costValue->setAnchorPoint(ccp(0.5f,0.5f));
		l_costValue->setRotationY(180);
		btn_backCardFrame->addChild(l_costValue);
		l_costValue->setPosition(ccp(0,-10));
		l_costValue->setVisible(false);

		image_unit = UIImageView::create();
		image_unit->setTexture("");
		image_unit->setAnchorPoint(ccp(0.5f,0.5f));
		image_unit->setRotationY(180);
		btn_backCardFrame->addChild(image_unit);
		image_unit->setPosition(ccp(0,-10));
		image_unit->setVisible(false);

		//��������
		btn_frontCardFrame = UIButton::create();
		btn_frontCardFrame->setTextures("res_ui/instance_end/card_get.png","res_ui/instance_end/card_get.png","");
		btn_frontCardFrame->setAnchorPoint(ccp(0.5f,0.5f));
		btn_frontCardFrame->setPosition(ccp(0,0));
		btn_backCardFrame->setTouchEnable(false);
		btn_frontCardFrame->addReleaseEvent(this,coco_releaseselector(RewardCardItem::FrontCradEvent));
		btn_frontCardFrame->setRotationY(0);
		m_pUiLayer->addWidget(btn_frontCardFrame);
		btn_frontCardFrame->setVisible(true);


		/*********************�ж���ɫ***************************/
		std::string frameColorPath;
		if (rewardProp->goods().quality() == 1)
		{
			frameColorPath = whiteFramePath;
		}
		else if (rewardProp->goods().quality() == 2)
		{
			frameColorPath = greenFramePath;
		}
		else if (rewardProp->goods().quality() == 3)
		{
			frameColorPath = blueFramePath;
		}
		else if (rewardProp->goods().quality() == 4)
		{
			frameColorPath = purpleFramePath;
		}
		else if (rewardProp->goods().quality() == 5)
		{
			frameColorPath = orangeFramePath;
		}
		else
		{
			frameColorPath = voidFramePath;
		}

		//��
		imageView_frame = UIImageView::create();
		imageView_frame->setTexture(frameColorPath.c_str());
		imageView_frame->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_frame->setPosition(ccp(0,1));
		btn_frontCardFrame->addChild(imageView_frame);
		//����ͼƬ
		imageView_icon = UIImageView::create();
		std::string iconPath = "res_ui/props_icon/";
		iconPath.append(rewardProp->goods().icon());
		iconPath.append(".png");
		imageView_icon->setTexture(iconPath.c_str());
		imageView_icon->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_icon->setPosition(ccp(0,1));
		imageView_icon->setScale(0.8f);
		btn_frontCardFrame->addChild(imageView_icon);
		//�Ƿ�˫��
		if (timeDouble == 1)
		{
			UIImageView* imageView_timeDouble = UIImageView::create();
			imageView_timeDouble->setTexture("res_ui/double.png");
			imageView_timeDouble->setAnchorPoint(ccp(0.0f,1.0f));
			imageView_timeDouble->setPosition(ccp(-imageView_frame->getContentSize().width/2+1,imageView_frame->getContentSize().height/2-1));
			imageView_timeDouble->setScale(.5f);
			btn_frontCardFrame->addChild(imageView_timeDouble);
		}
		//��������
		l_rewardName = UILabel::create();
		l_rewardName->setText(rewardProp->goods().name().c_str());
		l_rewardName->setFontName(APP_FONT_NAME);
		l_rewardName->setFontSize(14);
		l_rewardName->setAnchorPoint(ccp(0.5f,0));
		l_rewardName->setPosition(ccp(0,-btn_frontCardFrame->getContentSize().width/2-7));
		l_rewardName->setColor(GameView::getInstance()->getGoodsColorByQuality(rewardProp->goods().quality()));
		btn_frontCardFrame->addChild(l_rewardName);

		//��������
		l_roleName = UILabel::create();
		l_roleName->setText("");
		l_roleName->setFontName(APP_FONT_NAME);
		l_roleName->setFontSize(14);
		l_roleName->setAnchorPoint(ccp(0.5f,0));
		l_roleName->setPosition(ccp(0,btn_frontCardFrame->getContentSize().width/2-5));
		btn_frontCardFrame->addChild(l_roleName);

		this->setContentSize(btn_backCardFrame->getContentSize());
		return true;
	}
	return false;
}

void RewardCardItem::BackCradEvent( CCObject * pSender )
{
	if (grid == 0)
	{
		InstanceEndUI * instanceEndUI = (InstanceEndUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceEndUI);
		if (instanceEndUI)
		{
			instanceEndUI->removeToturial();
		}
	}

	if (isFloped)
		return;

// 	//���ƣ������棩
// 	this->FlopAnimationToFront();
	//������ 1925
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1925,(void *)grid);
	isFloped = true;

}

void RewardCardItem::FrontCradEvent( CCObject * pSender )
{

}

//�ѿ��Ʒ������沢���ò��ɴ���
void RewardCardItem::FlopAnimationToFront()
{
	btn_backCardFrame->setZOrder(10);
	btn_frontCardFrame->setRotationY(-180);
	CCActionInterval*  orbit1 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_90_TIME,1, 0, 0, -90, 0, 0);
	CCSequence*  action1 = CCSequence::create(
		CCShow::create(),
		orbit1,
		CCHide::create(),
		NULL);

	CCActionInterval*  orbit2 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_180_TIME,1, 0, 0, 180, 0, 0);
	CCSequence*  action2 = CCSequence::create(
		CCShow::create(),
		orbit2,
		NULL);

	CCFiniteTimeAction* action_ToFront = CCSequence::create(action1,NULL);
	btn_backCardFrame->runAction(action_ToFront);
	CCFiniteTimeAction* action_ToBack = CCSequence::create(action2,NULL);
	btn_frontCardFrame->runAction(action_ToBack);

	isFrontOrBack = true;
	btn_frontCardFrame->setTouchEnable(false);
	btn_backCardFrame->setTouchEnable(false);
}

//�ѿ��Ʒ������沢���ò��ɴ���
void RewardCardItem::FlopAnimationToBack()
{
	CCActionInterval*  orbit1 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_180_TIME,1, 0, 0, 180, 0, 0);
	CCSequence*  action1 = CCSequence::create(
		orbit1,
		NULL);

	CCActionInterval*  orbit2 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_90_TIME,1, 0, 0, 90, 0, 0);
	CCSequence*  action2 = CCSequence::create(
		orbit2,
		CCHide::create(),
		NULL);

	CCFiniteTimeAction* action_ToBack = CCSequence::create(action1,NULL);
	btn_backCardFrame->runAction(action_ToBack);
	CCFiniteTimeAction* action_ToFront = CCSequence::create(action2,NULL);
	btn_frontCardFrame->runAction(action_ToFront);

	isFrontOrBack = false;
	btn_frontCardFrame->setTouchEnable(false);
	btn_backCardFrame->setTouchEnable(false);
}

void RewardCardItem::setBtnTouchEnabled()
{
	btn_backCardFrame->setTouchEnable(true);
	btn_frontCardFrame->setTouchEnable(false);
}

void RewardCardItem::RefreshCardInfo( CRewardProp *rewardProp ,std::string roleName,long long roleId)
{
	/*********************�ж���ɫ***************************/
	std::string frameColorPath;
	if (rewardProp->goods().quality() == 1)
	{
		frameColorPath = whiteFramePath;
	}
	else if (rewardProp->goods().quality() == 2)
	{
		frameColorPath = greenFramePath;
	}
	else if (rewardProp->goods().quality() == 3)
	{
		frameColorPath = blueFramePath;
	}
	else if (rewardProp->goods().quality() == 4)
	{
		frameColorPath = purpleFramePath;
	}
	else if (rewardProp->goods().quality() == 5)
	{
		frameColorPath = orangeFramePath;
	}
	else
	{
		frameColorPath = voidFramePath;
	}

	//��
	imageView_frame->setTexture(frameColorPath.c_str());
	//����ͼƬ
	std::string iconPath = "res_ui/props_icon/";
	iconPath.append(rewardProp->goods().icon());
	iconPath.append(".png");
	imageView_icon->setTexture(iconPath.c_str());
	//��������
	l_rewardName->setText(rewardProp->goods().name().c_str());
	l_rewardName->setColor(GameView::getInstance()->getGoodsColorByQuality(rewardProp->goods().quality()));

	//���ƽ�ɫ����
	l_roleName->setText(roleName.c_str());
	if (GameView::getInstance()->isOwn(roleId))
	{
		l_roleName->setColor(ccc3(0,255,255));
	}
	else
	{
		l_roleName->setColor(ccc3(0,255,0));
	}
}

void RewardCardItem::presentDesLable()
{
	if (btn_backCardFrame->getChildByName("image_desFrame"))
	{
		btn_backCardFrame->getChildByName("image_desFrame")->setVisible(true);
	}
	l_des->setVisible(true);
	l_costValue->setVisible(true);
	image_unit->setVisible(true);
}

void RewardCardItem::RefreshCostValue(std::string costValue,std::string iconPath)
{
	l_costValue->setText(costValue.c_str());
	image_unit->setTexture(iconPath.c_str());
	int allWidth = l_costValue->getContentSize().width+image_unit->getContentSize().width;
	l_costValue->setAnchorPoint(ccp(.0f,0.5f));
	l_costValue->setPosition(ccp(allWidth/2,l_costValue->getPosition().y));
	image_unit->setAnchorPoint(ccp(1.0f,0.5f));
	image_unit->setPosition(ccp(l_costValue->getPosition().x-l_costValue->getContentSize().width-5,l_costValue->getPosition().y));
}
