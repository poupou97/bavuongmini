
#ifndef _MISSIONSCENE_HANDLEMISSION_
#define _MISSIONSCENE_HANDLEMISSION_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UITab;

class MissionInfo;

class HandleMission : public UIScene
{
public:
	enum handleType
	{
		getMission,
		submitMission
	};

	HandleMission();
	~HandleMission();

	static HandleMission* create(MissionInfo * missionInfo);
	bool init(MissionInfo * missionInfo);

	virtual void update( float dt );

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);


	std::string rewardIconName[5];
	std::string rewardLabelName[5];

	int getTargetNpcId();

	handleType getCurHandlerType();

public: 
	UIButton * Button_getMission;
	UIButton * Button_finishMission;

	//教学
	virtual void registerScriptCommand(int scriptId);
	//领取/提交按钮
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();
private:
	MissionInfo * curMissionInfo;
	int targetNpcId;
	handleType curHandleType;
	UIButton * Button_Handle;
	CCScrollView * m_scrollView;
	CCLabelTTF * m_describeLabel;
	void CloseEvent(CCObject * pSender);
	void GetMissionEvent(CCObject * pSender);
	void FinishMissionEvent(CCObject * pSender);

	UIImageView * reward_di_1;
	UIImageView * reward_di_2;
	UIImageView * reward_di_3;
	UIImageView * reward_di_4;
	UIImageView * reward_di_5;


	//教学
	int mTutorialScriptInstanceId;
};
#endif;

