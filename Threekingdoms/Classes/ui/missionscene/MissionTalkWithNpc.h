
#ifndef _MISSIONSCENE_TALKWITHNPC_
#define _MISSIONSCENE_TALKWITHNPC_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UITab;
class CNpcDialog;
class MissionInfo;
class LegendLevel;
class FunctionNPC;

class MissionTalkWithNpc : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	MissionTalkWithNpc();
	~MissionTalkWithNpc();

	static MissionTalkWithNpc* create(FunctionNPC* fcNpc);
	bool init(FunctionNPC* fcNpc);

	virtual void update( float dt );

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject * pSender);

	int getCurNpcId();

private:
	UIImageView * ImageView_headImage;
	UIImageView * ImageView_name;
	CCTableView * m_tableView;
	CCScrollView * m_scrollView;
	CCLabelTTF * m_describeLabel;

	FunctionNPC * curFunctionNPC;
	int curNpcId;
};
#endif;