
#ifndef _MISSIONSCENE_MISSIONREWARDGOODSITEM_H_
#define _MISSIONSCENE_MISSIONREWARDGOODSITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class GoodsInfo;

class MissionRewardGoodsItem :public UIWidget
{
public:
	MissionRewardGoodsItem();
	~MissionRewardGoodsItem();

	static MissionRewardGoodsItem * create(GoodsInfo* goods,int num);
	bool initWithGoods(GoodsInfo* goods,int num);

	void GoodItemEvent(CCObject *pSender);

private:
	GoodsInfo * curGoodsInfo;
};

#endif

