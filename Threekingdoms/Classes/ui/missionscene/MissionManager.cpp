#include "MissionManager.h"

#include "../../messageclient/element/MissionInfo.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "MissionActions.h"
#include "GameView.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/npcscene/NpcTalkWindow.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "HandleMission.h"
#include "MissionTalkWithNpc.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/FunctionNPC.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../legend_engine/GameWorld.h"
#include "../../utils/pathfinder/AStarPathFinder.h"

//交互距离
#define CommunicateInstance 400

static MissionManager *_missionManager = NULL;
int MissionManager::s_status = MissionManager::status_none;

MissionManager::MissionManager():
isAutoRunForMission(false),
m_nTargetNpcId(0),
m_bIsFirstCreateMissionAndTeam(true),
guideForMmission_time(0.f),
isExistGuideForMmission(false),
m_sLastMissionPackageId("")
{
	curMissionInfo = new MissionInfo();
}


MissionManager::~MissionManager()
{
	std::vector<MissionInfo*>::iterator _iter;
	for (_iter = MissionList.begin(); _iter != MissionList.end(); ++_iter)
	{
		delete *_iter;
	}
	MissionList.clear();

	for (_iter = MissionList_MainScene.begin(); _iter != MissionList_MainScene.end(); ++_iter)
	{
		delete *_iter;
	}
	MissionList_MainScene.clear();

	for (_iter = curMissionList.begin(); _iter != curMissionList.end(); ++_iter)
	{
		delete *_iter;
	}
	curMissionList.clear();

	delete curMissionInfo;
}

MissionManager* MissionManager::getInstance()
{
	if (!_missionManager)
	{
		_missionManager = new MissionManager();
	}

	return _missionManager;
}

void MissionManager::update()
{
	if (getStatus() == status_transportFinished)
	{
		addMission(curMissionInfo);
	}

	if (isAutoRunForMission)
	{
		std::vector<MissionInfo*>::iterator iter;
		for(int i = 0;i<(int)curMissionList.size();i++)
		{
			iter = curMissionList.begin()+i;
			MissionInfo* mi = *iter;
			if (mi == NULL)
			{
				curMissionList.erase(iter);
				i--;
				continue;
			}
			else
			{
				switch(curActionType)
				{
				case KTypeMMoveAction :
					{
						if(curMoveAction->isFinishedMove())
						{
							isAutoRunForMission = false;
							CC_SAFE_DELETE(curMoveAction);
						}
						break;
					}
				case KTypeMTalkWithNpcAction :
					{
						if (curTalkWithNpcAction->isFinishedMove())
						{
							isAutoRunForMission = false;
							curTalkWithNpcAction->doMTalkWithNpcAction();
							CC_SAFE_DELETE(curTalkWithNpcAction);
						}

						break;
					}
				case KTypeMMonsterKillAction :
					{
						if (curMonsterKillAction->isFinishedMove())
						{
							isAutoRunForMission = false;
							curMonsterKillAction->doMMonsterKillAction();
							CC_SAFE_DELETE(curMonsterKillAction);
						}
						break;
					}
				case KTypeMPickAction :
					{
						if (curPickAction->isFinishedMove())
						{
							isAutoRunForMission = false;
							curPickAction->doPickAction();
							CC_SAFE_DELETE(curPickAction);
						}
						break;
					}
				case KTypeMHandleViewAction :
					{
						CC_SAFE_DELETE(curHandleAction);
						break;
					}
				case KTypeMOpenViewAction :
					{
						CC_SAFE_DELETE(curOpenAction);
						break;
					}
				case KTypeMOpenNpcFunction :
					{
						if (curOpenNpcFunction->isFinishedMove())
						{
							isAutoRunForMission = false;
							curOpenNpcFunction->doMOpenNpcFunction();
							CC_SAFE_DELETE(curOpenNpcFunction);
						}
						break;
					}
				case KTypeMTalkMissionAction :
					{
						if (curTalkMission->isFinishedMove())
						{
							isAutoRunForMission = false;
							curTalkMission->doMTalkMission();
							CC_SAFE_DELETE(curTalkMission);
						}
						break;
					}
				case KTypeMoveToTargetAndOpenUI :
					{
						if (curMoveToTargetAndOpenUIAction->isFinishedMove())
						{
							isAutoRunForMission = false;
							curMoveToTargetAndOpenUIAction->doMMoveToTargetAndOpenUI();
							CC_SAFE_DELETE(curMoveToTargetAndOpenUIAction);
						}
						break;
					}
				case KTypeMFailAction :
					{
						CC_SAFE_DELETE(curFailAction);
						break;
					}
				}
				
			}
		}
	}
	
	addGuideForMainMission();
// 	//
// 	//检测是否到达目的地
// 	if (curMoveAction != nullptr)
// 	{
// 		curMoveAction->update(0.1f);
// 	}
// 	
}

void MissionManager::doMission(MissionInfo *missionInfo)
{
	switch(curActionType)
	{
	case KtypeDoNothing :
		{
			if (missionInfo->has_tip())
			{
				//GameView::getInstance()->showAlertDialog(missionInfo->tip().dialog().c_str());
			}
			
			break;
		}
	case KTypeMMoveAction :
		{
			curMoveAction = MMoveAction::create(missionInfo);
			break;
		}
	case KTypeMTalkWithNpcAction :
		{
			curTalkWithNpcAction = MTalkWithNpcAction::create(missionInfo);
			break;
		}
	case KTypeMMonsterKillAction :
		{
			curMonsterKillAction = MMonsterKillAction::create(missionInfo);
			break;
		}
	case KTypeMPickAction :
		{
			curPickAction = MPickAction::create(missionInfo);
			break;
		}
	case KTypeMHandleViewAction :
		{
			MyPlayerAIConfig::setAutomaticSkill(0);
			MyPlayerAIConfig::enableRobot(false);
			GameView::getInstance()->myplayer->getMyPlayerAI()->stop();
			if (GameView::getInstance()->getGameScene())
			{
				if (GameView::getInstance()->getMainUIScene())
				{
					GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
					if (guide_)
					{
						guide_->setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
					}
				}
			}
			curHandleAction = MHandleViewAction::create(missionInfo);
			break;
		}
	case KTypeMOpenViewAction :
		{
			MyPlayerAIConfig::setAutomaticSkill(0);
			MyPlayerAIConfig::enableRobot(false);
			GameView::getInstance()->myplayer->getMyPlayerAI()->stop();
			if (GameView::getInstance()->getGameScene())
			{
				if (GameView::getInstance()->getMainUIScene())
				{
					GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
					if (guide_)
					{
						guide_->setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
					}
				}
			}
			curOpenAction = MOpenViewAction::create(missionInfo);
			break;
		}
	case KTypeMOpenNpcFunction :
		{
			 curOpenNpcFunction = MOpenNpcFunction::create(missionInfo);
			break;
		}
	case KTypeMTalkMissionAction :
		{
			curTalkMission = MTalkMission::create(missionInfo);
			break;
		}
	case KTypeMoveToTargetAndOpenUI :
		{
			curMoveToTargetAndOpenUIAction = MMoveToTargetAndOpenUI::create(missionInfo);
			break;
		}
	case KTypeMFailAction :
		{
			curFailAction = MFailAction::create(missionInfo);
			break;
		}
	}
}

void MissionManager::addMission(MissionInfo *mInfo )
{
	//set status
	setStatus(status_none);

	MissionAction mat;
	mat.CopyFrom(mInfo->action());
	switch(mat.action())
	{
	case doNothing:
		curActionType = KtypeDoNothing;
		break;
	case moveTo:
		curActionType = KTypeMMoveAction;
		break;
	case moveToFight:
		curActionType = KTypeMMonsterKillAction;
		break;
	case moveToCollection:
		curActionType = KTypeMPickAction;
		break;
	case moveToNPC:
		curActionType = KTypeMTalkWithNpcAction;
		break;
	case openSpecifiedUI:
		curActionType = KTypeMOpenViewAction;
		break;
	case openNpcFunction:
		curActionType = KTypeMOpenNpcFunction;
		break;
	case taklToNpc:
		curActionType = KTypeMTalkMissionAction;
		break;
	case moveToNpcAndOpenUi:
		curActionType = KTypeMoveToTargetAndOpenUI;
		break;
	}

	std::vector<MissionInfo*>::iterator _iter;
	for (_iter = this->curMissionList.begin(); _iter != this->curMissionList.end(); ++_iter)
	{
		delete *_iter;
	}
	this->curMissionList.clear();

	MissionInfo * temp = new MissionInfo();
	temp->CopyFrom(*mInfo);
	curMissionList.push_back(temp);

	doMission(mInfo);
}

bool MissionManager::isOutOfMissionTalkArea(int missionNpcId)
{
	if (!GameView::getInstance()->getGameScene())
		return true;

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	if (GameView::getInstance()->getGameScene()->getActor(missionNpcId) != NULL)
	{
		CCPoint cp_target = GameView::getInstance()->getGameScene()->getActor(missionNpcId)->getWorldPosition();

		CCPoint cp_role = GameView::getInstance()->myplayer->getWorldPosition();

		if (ccpDistance(cp_role,cp_target) > winSize.width/2)
		{
			return true;
		}
		else 
		{
			return false;
		}
	}
	else
	{
		return true;
	}
}

bool SortMissionListByType(MissionInfo * missionInfo1 , MissionInfo *missionInfo2)
{
	return (missionInfo1->missionpackagetype() < missionInfo2->missionpackagetype());
}


void MissionManager::autoPopUpHandleMission()
{
// 	if (missionInfo->has_action())
// 	{
// 		if (missionInfo->action().has_moveto())
// 		{
// 			if (missionInfo->action().moveto().has_targetnpc())
// 			{

// 				FunctionNPC* fn = dynamic_cast<FunctionNPC*>(GameView::getInstance()->getGameScene()->getActor(missionInfo->action().moveto().targetnpc().npcid()));
// 				if (!fn)
// 					return;
// 
// 				fn->RefreshNpc();

// 

// 				if (curMissionList.size()>0)
// 				{
// 					MissionInfo * tempMissionInfo = curMissionList.at(0);
// 					if(tempMissionInfo->has_action())
// 					{
// 						if (tempMissionInfo->action().has_moveto())
// 						{
// 							if(tempMissionInfo->action().moveto().targetnpc().npcid() == missionInfo->action().moveto().targetnpc().npcid())
// 							{
// 								if (fn->npcMissionList.size()<=0)
// 									return;
// 
// 								//按类型排序
// 								sort(fn->npcMissionList.begin(),fn->npcMissionList.end(),SortMissionListByType);
// 
// 								GameSceneLayer* scene = GameView::getInstance()->getGameScene();
// 								for (int i = 0;i<(int)fn->npcMissionList.size();++i)
// 								{
// 									if (fn->npcMissionList.at(i)->missionstate() == dispatched)  //任务可接
// 									{
// 										curMissionInfo->CopyFrom(*(fn->npcMissionList.at(i)));
// 
// 										if (scene->getMainUIScene()->getChildByTag(kTagHandleMission))
// 										{
// 											scene->getMainUIScene()->getChildByTag(kTagHandleMission)->removeFromParent();
// 										}
// 
// 										CCNode* pNode = scene->getChildByTag(kTagMovieBaseNode);
// 										if(pNode == NULL)
// 										{
// 											if (!isOutOfMissionTalkArea(missionInfo->action().moveto().targetnpc().npcid()))
// 											{
// 												CCActionInterval * m_action =(CCActionInterval *)CCSequence::create(CCDelayTime::create(0.5f),
// 													CCCallFuncND::create(scene, callfuncND_selector(MissionManager::showHandleMission),(void *)curMissionInfo),
// 													NULL);
// 
// 												scene->runAction(m_action);
// 												break;
// 											}
// 										}
// 									}
// 								}
// 							}
// 						}
// 					}
// 				}

// 			}

// 		}

// 	}


	if (m_nTargetNpcId == 0)
		return;

	FunctionNPC* fn = dynamic_cast<FunctionNPC*>(GameView::getInstance()->getGameScene()->getActor(m_nTargetNpcId));
	if (!fn)
		return;

	fn->RefreshNpc();

	//按类型排序
	sort(fn->npcMissionList.begin(),fn->npcMissionList.end(),SortMissionListByType);

	if (isOutOfMissionTalkArea(m_nTargetNpcId))
		return;

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	for (int i = 0;i<(int)fn->npcMissionList.size();++i)
	{
		if (fn->npcMissionList.at(i)->missionstate() == dispatched)  //任务可接
		{
			curMissionInfo->CopyFrom(*(fn->npcMissionList.at(i)));

			if (scene->getMainUIScene()->getChildByTag(kTagHandleMission) != NULL)
			{
				scene->getMainUIScene()->getChildByTag(kTagHandleMission)->removeFromParent();
			}

			if (scene->getMainUIScene()->getChildByTag(kTagHandleMission) == NULL)
			{
				//scene->getMainUIScene()->getChildByTag(kTagHandleMission)->removeFromParent();
				CCNode* pNode = scene->getChildByTag(kTagMovieBaseNode);
				if(pNode == NULL)
				{
					CCActionInterval * m_action =(CCActionInterval *)CCSequence::create(CCDelayTime::create(0.5f),
						CCCallFuncND::create(scene, callfuncND_selector(MissionManager::showHandleMission),(void *)curMissionInfo),
						NULL);

					scene->runAction(m_action);
					break;
				}
			}
		}
	}
}

void MissionManager::showHandleMission( CCNode* sender, void* data )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission) == NULL)
	{
		//GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission)->removeFromParent();
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		HandleMission * getMission = HandleMission::create((MissionInfo*)data);
		getMission->ignoreAnchorPointForPosition(false);
		getMission->setAnchorPoint(ccp(0,0.5f));
		getMission->setPosition(ccp(0,winSize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(getMission,0,kTagHandleMission);	
	}
}

void MissionManager::setMissionAndTeamOffSet( CCPoint temp )
{
	cp_offSet = temp;
}

cocos2d::CCPoint MissionManager::getMissionAndTeamOffSet()
{
	return cp_offSet;
}

bool MissionManager::getIsFirstCreateMissionAndTeam()
{
	return m_bIsFirstCreateMissionAndTeam;
}

void MissionManager::setIsFirstCreateMissionAndTeam( bool _value )
{
	m_bIsFirstCreateMissionAndTeam = _value;
}

void MissionManager::setStatus( int status )
{
	s_status = status;
}

int MissionManager::getStatus()
{
	return s_status;
}

void MissionManager::AcrossMapTransport( std::string mapId,CCPoint pos )
{
	setStatus(MissionManager::status_waitForTransport);
	//req acrossMap
	CCPoint * t_point = new CCPoint(getNearestReachablePos(pos));
	//CCPoint * t_point = new CCPoint(pos);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1114,(void *)mapId.c_str(),t_point);
	delete t_point;
}


CCPoint MissionManager::getNearestReachablePos(CCPoint& targetPoint)
{
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();

	short tileX = scene->positionToTileX(targetPoint.x);
	short tileY = scene->positionToTileY(targetPoint.y);
	const int range_offset = 2;
	const int init_offset = 1;
	short initTileX = tileX - init_offset;
	short initTileY = tileY - init_offset;
	for(short x = initTileX; x < (initTileX+range_offset); x++)
	{
		for(short y = initTileY; y < (initTileY+range_offset); y++)
		{
			// out of map, ignore
			if(!scene->checkInMap(x, y))
				continue;

			if(scene->isLimitOnGround(x, y))
				continue;

			float posX = scene->tileToPositionX(x);
			float posY = scene->tileToPositionY(y);
			return ccp(posX, posY);
		}
	}

	return targetPoint;
}

void MissionManager::addGuideForMainMission()
{
	if (GameView::getInstance()->myplayer)
	{
		if(GameView::getInstance()->myplayer->getActiveRole()->level() > K_GuideForMainMission_Level)
			return;
	}

	if (GameView::getInstance()->missionManager->MissionList_MainScene.size() <= 0)
		return;

	int recoverTime = K_GuideForMainMission_Normal_RecoverTime;
	MissionInfo * mainMission = GameView::getInstance()->missionManager->MissionList_MainScene.at(0);
	if (mainMission->action().action() == moveToFight || mainMission->action().action() == moveToCollection || mainMission->action().action() == moveTo)
	{
		if (mainMission->missionstate() == accepted)
		{
			recoverTime = K_GuideForMainMission_MonsterKilll_RecoverTime;
		}
	}


	if (isExistGuideForMmission)
	{
		guideForMmission_time = 0.f; 
	}
	else
	{
		guideForMmission_time += 1.0f/60;
	}

	if (guideForMmission_time >= recoverTime)
	{
		if (GameView::getInstance())
		{
			if (GameView::getInstance()->getGameScene())
			{
				MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
				if (mainScene)
				{
		 			MissionAndTeam * missionAndTeam = (MissionAndTeam*)mainScene->getChildByTag(kTagMissionAndTeam);
		 			if (missionAndTeam)
		 			{
						missionAndTeam->addGuidePen();
					}
				}
			}
		}
	}
}

bool MissionManager::CheckIsEnableToRunToTarget( const char * mapid,CCPoint pos )
{
	//first check isEnable to ruan to target
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if (!scene)
		return false;

	std::string currentMapId = GameView::getInstance()->getMapInfo()->mapid();
	// no cross map
	if(mapid == currentMapId)
	{
		// start point
		short start[2] = {0, 0};   // tileY, tileX
		short tileX = scene->positionToTileX(GameView::getInstance()->myplayer->getWorldPosition().x);
		short tileY = scene->positionToTileY(GameView::getInstance()->myplayer->getWorldPosition().y);
		start[0] = tileY;
		start[1] = tileX;
		// out of map, ignore
		if(!scene->checkInMap(tileX, tileY))
		{
			return false;
		}

		// end point
		short end[2] = {0, 0};
		tileX = scene->positionToTileX(pos.x);
		tileY = scene->positionToTileY(pos.y);
		end[0] = tileY;
		end[1] = tileX;
		// out of map, ignore
		if(!scene->checkInMap(tileX, tileY))
		{
			return false;
		}

		std::vector<short*> *path = scene->getPathFinder()->searchPath(start, end);
		if(path != NULL)
		{
			delete path;
		}
		else
		{
			//GameView::getInstance()->showAlertDialog(StringDataManager::getString("mission_canNotAutoSearchPathNow"));
			return false;
		}
	}
	// cross map
	else if(mapid != currentMapId)
	{
		std::vector<std::string>* crossMapPaths = GameWorld::searchLevelPath(currentMapId, mapid);
		if(crossMapPaths == NULL)
		{
			//GameView::getInstance()->showAlertDialog(StringDataManager::getString("mission_canNotAutoSearchPathNow"));
			return false;
		}
		else
		{
			CC_SAFE_DELETE(crossMapPaths);
		}

	}

	return true;
}
