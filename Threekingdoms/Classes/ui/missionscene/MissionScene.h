
#ifndef _MISSIONSCENE_MISSIONSCENE_
#define _MISSIONSCENE_MISSIONSCENE_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UITab;
class MissionInfo;
class CCRichLabel;

class MissionScene :public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	MissionScene();
	~MissionScene();

	static MissionScene* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject* pSender);
	void GiveUpMissionEvent(CCObject * pSender);
	void AutoPathFindEvent(CCObject * pSender);
	void MainTabIndexChangedEvent(CCObject * pSender);

	void ReloadMissionInfoData();
	void updataCurDataSourceByMissionState(int missionState); /***1:已接  0:未接*************/

	void ReloadDataWithOutChangeOffSet();
public:
	enum MissionState
	{
		mission_didNotReceive,
		mission_received,
		mission_finished
	};

	enum MissionType
	{
		mission_mainline,
		mission_sideline,
		mission_dailyTasks
	};

	enum MissionWay
	{
		mission_killMonsters,
		mission_collect,
		mission_pick,
		mission_protect,
		mission_search,
		mission_control,
		mission_talk,
		mission_catch,
		mission_fishing
	};

	int selectCellIdx;
private:
	UILayer * u_layer;

	UIImageView * flag;

	UILayer * contentLayer;
	CCSprite * sprite_name;
	CCLabelTTF * label_name;
	CCSprite * sprite_target;
	CCRichLabel * label_target;
	CCSprite * sprite_des;
	CCRichLabel * label_des;
	CCSprite * sprite_award;
	CCLabelTTF * label_award_expName;
	CCLabelTTF * label_award_expValue;
	CCLabelTTF * label_award_goldName;
	CCLabelTTF * label_award_goldValue;
	CCScrollView * m_contentScrollView;

	UIButton * Button_giveUpMission;
	UIButton * Button_autoPathFinder;
	UILabel * l_autoPathFinder;

	UILabel * l_haveNoMissionToGet;
	UILabel * l_haveNoMissionGeted;

public:
	CCTableView * m_tableView;
	std::vector<MissionInfo*> curMissionDataSource;

	UITab * mainTab ;
	MissionInfo * curMissionInfo;
};
#endif;
