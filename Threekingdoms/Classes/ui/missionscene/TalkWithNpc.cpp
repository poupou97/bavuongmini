#include "TalkWithNpc.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"
#include "../../messageclient/protobuf/NPCMessage.pb.h"
#include "../../messageclient/element/CNpcDialog.h"
#include "../extensions/CCRichLabel.h"
#include "../extensions/UITab.h"
#include "AppMacros.h"

TalkWithNpc::TalkWithNpc()
{
}


TalkWithNpc::~TalkWithNpc()
{
}


TalkWithNpc* TalkWithNpc::create()
{
	TalkWithNpc * talkWithNpc = new TalkWithNpc();
	if(talkWithNpc && talkWithNpc->init())
	{
		talkWithNpc->autorelease();
		return talkWithNpc;
	}
	CC_SAFE_DELETE(talkWithNpc);
	return NULL;
}

TalkWithNpc* TalkWithNpc::create(CNpcDialog * npcDialog )
{
	TalkWithNpc * talkWithNpc = new TalkWithNpc();
	if (talkWithNpc && talkWithNpc->init(npcDialog))
	{
		talkWithNpc->autorelease();
		return talkWithNpc;
	}
	CC_SAFE_DELETE(talkWithNpc);
	return NULL;
}

bool TalkWithNpc::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();
		//加载UI
		if(LoadSceneLayer::TalkWithNpcLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::TalkWithNpcLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (UIPanel*)LoadSceneLayer::s_pPanel->copy();
		UIPanel *ppanel = LoadSceneLayer::TalkWithNpcLayer;
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(350, 400));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * button_close = (UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_close");
		button_close->setTouchEnable(true);
		button_close->addReleaseEvent(this,coco_releaseselector(TalkWithNpc::CloseEvent));
		ImageView_headImage = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_touxiang");;
		ImageView_name = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_mingzi");

		//richLabel_description
		CCRichLabel * label_des = CCRichLabel::createWithString("去新手村杀五只棍犬加快两个",CCSizeMake(285,50),this,NULL,0);
		CCSize desSize = label_des->getContentSize();
		int height = desSize.height;
		//ccscrollView_description
		CCScrollView *m_contentScrollView = CCScrollView::create(CCSizeMake(290,120));
		m_contentScrollView->setViewSize(CCSizeMake(290, 120));
		m_contentScrollView->ignoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(kCCScrollViewDirectionVertical);
		m_contentScrollView->setAnchorPoint(ccp(0,0));
		m_contentScrollView->setPosition(ccp(33,173));
		m_contentScrollView->setBounceable(false);
		m_pUiLayer->addChild(m_contentScrollView);
		if (height > 120)
		{
			m_contentScrollView->setContentSize(ccp(290,height));
			m_contentScrollView->setContentOffset(ccp(0,120-height));
		}
		else
		{
			m_contentScrollView->setContentSize(ccp(290,120));
		}
		m_contentScrollView->setClippingToBounds(true);

		m_contentScrollView->addChild(label_des);
		label_des->ignoreAnchorPointForPosition(false);
		label_des->setAnchorPoint(ccp(0,1));
		label_des->setPosition(ccp(3,m_contentScrollView->getContentSize().height));


		//m_tableView = CCTableView::create(this,CCSizeMake(296,132));
		//m_tableView->setDirection(kCCScrollViewDirectionVertical);
		//m_tableView->setAnchorPoint(ccp(0,0));
		//m_tableView->setPosition(ccp(27,30));
		//m_tableView->setDelegate(this);
		//m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		//this->addChild(m_tableView);
		
		//this->setTouchEnabled(true);
		//this->setTouchMode(kCCTouchesOneByOne);
		//this->setContentSize(designResolutionSize);

		return true;
	}
	return false;
}

bool TalkWithNpc::init(CNpcDialog * npcDialog)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();
		//加载UI
		if(LoadSceneLayer::TalkWithNpcLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::TalkWithNpcLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (UIPanel*)LoadSceneLayer::s_pPanel->copy();
		UIPanel *ppanel = LoadSceneLayer::TalkWithNpcLayer;
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(350, 400));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * button_close = (UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_close");
		button_close->setTouchEnable(true);
		button_close->addReleaseEvent(this,coco_releaseselector(TalkWithNpc::CloseEvent));
		ImageView_headImage = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_touxiang");;
		ImageView_name = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_mingzi");

		UILabel * label_welcom = UILabel::create();
		label_welcom->setText("hello every hello every hello every hello every hello every hello every ");
		label_welcom->setFontName(APP_FONT_NAME);
		label_welcom->setFontSize(20);
		label_welcom->setTextAreaSize(CCSizeMake(280,120));
		label_welcom->setTextHorizontalAlignment(kCCTextAlignmentLeft);
		label_welcom->setAnchorPoint(ccp(0,1));
		label_welcom->setPosition(ccp(34,367));
		m_pUiLayer->addWidget(label_welcom);

// 		if (npcDialog->options_size()>0)
// 		{
// 			m_tableView = CCTableView::create(this,CCSizeMake(296,132));
// 			m_tableView->setDirection(kCCScrollViewDirectionVertical);
// 			m_tableView->setAnchorPoint(ccp(0,0));
// 			m_tableView->setPosition(ccp(27,30));
// 			m_tableView->setDelegate(this);
// 			m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
// 			this->addChild(m_tableView);
// 		}

		//m_describeLabel = CCLabelTTF::create("123456789","Arial",16);
		//m_describeLabel->setAnchorPoint(ccp(0.5f,0.5f));

		//m_scrollView = CCScrollView::create(m_describeLabel->getContentSize(),m_describeLabel);
		//m_scrollView->setViewSize(CCSizeMake(296, 132));
		//m_scrollView->setAnchorPoint(CCPointZero);
		//m_scrollView->setContentOffset(ccp(0,0));
		//m_scrollView->setTouchEnabled(true);
		//m_scrollView->setDirection(kCCScrollViewDirectionVertical);
		//m_scrollView->setPosition(ccp(27,164));
		//m_scrollView->setDelegate(this);
		//m_scrollView->setBounceable(true);
		//m_scrollView->setClippingToBounds(true);
		//addChild(m_scrollView);

		//m_tableView = CCTableView::create(this,CCSizeMake(296,132));
		//m_tableView->setDirection(kCCScrollViewDirectionVertical);
		//m_tableView->setAnchorPoint(ccp(0,0));
		//m_tableView->setPosition(ccp(27,30));
		//m_tableView->setDelegate(this);
		//m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		//this->addChild(m_tableView);
		//
		//this->setTouchEnabled(true);
		//this->setTouchMode(kCCTouchesOneByOne);
		//this->setContentSize(designResolutionSize);

		return true;
	}
	return false;
}


void TalkWithNpc::onEnter()
{
	UIScene::onEnter();

	CCFiniteTimeAction*  action = CCSequence::create(
		CCScaleTo::create(0.15f*1/2,1.1f),
		CCScaleTo::create(0.15f*1/3,1.0f),
		NULL);

	runAction(action);

	//this->setTouchEnabled(true);
	//this->setContentSize(CCSizeMake(800,480));


}

void TalkWithNpc::onExit()
{
	UIScene::onExit();
}


bool TalkWithNpc::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	return this->resignFirstResponder(touch,this,false);
}

void TalkWithNpc::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void TalkWithNpc::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void TalkWithNpc::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}



CCSize TalkWithNpc::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
	if (idx == 0)
	{
		return CCSizeMake(289,37);
	}
	else
	{
		return CCSizeMake(289, 37);
	}

}

CCTableViewCell* TalkWithNpc::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();

	CCSprite * pCellBg = CCSprite::create("res_ui/renwua/button_0.png");
	pCellBg->setAnchorPoint(ccp(0, 0));
	pCellBg->setPosition(ccp(0, 0));
	cell->addChild(pCellBg);

	CCLabelTTF * pMissionName = CCLabelTTF::create("123456789",APP_FONT_NAME,16);
	pMissionName->setAnchorPoint(ccp(0.5f,0.5f));
	pMissionName->setPosition(ccp(pCellBg->getContentSize().width/2,pCellBg->getContentSize().height/2));
	ccColor3B shadowColor = ccc3(0,0,0);   // black
	pMissionName->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	cell->addChild(pMissionName);

	return cell;

}


unsigned int TalkWithNpc::numberOfCellsInTableView(CCTableView *table)
{
	return 2;
}

void TalkWithNpc::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{

}

void TalkWithNpc::scrollViewDidScroll(CCScrollView* view )
{
}

void TalkWithNpc::scrollViewDidZoom(CCScrollView* view )
{
}

void TalkWithNpc::CloseEvent( CCObject * pSender )
{
	this->closeAnim();
}



