#include "RewardItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "MissionRewardGoodsItem.h"
#include "AppMacros.h"

RewardItem::RewardItem()
{
}


RewardItem::~RewardItem()
{
}

RewardItem * RewardItem::create(GoodsInfo* goods,int num)
{
	RewardItem * rewardItem = new RewardItem();
	if (rewardItem && rewardItem->init(goods,num))
	{
		rewardItem->autorelease();
		return rewardItem;
	}
	CC_SAFE_DELETE(rewardItem);
	return NULL;
}

bool RewardItem::init(GoodsInfo* goods,int num)
{
	if (UIScene::init())
	{
		MissionRewardGoodsItem * mrGoodsItem = MissionRewardGoodsItem::create(goods,num);
		mrGoodsItem->setAnchorPoint( ccp( 0, 0 ) );  
		mrGoodsItem->setPosition( ccp( 0, 0) );  
		mrGoodsItem->setScale(0.8f);
		m_pUiLayer->addWidget(mrGoodsItem);

		label_rewardName = CCLabelTTF::create();
		label_rewardName->setString(goods->name().c_str());
		label_rewardName->setFontName(APP_FONT_NAME);
		label_rewardName->setFontSize(14);
		label_rewardName->ignoreAnchorPointForPosition( false );  
		label_rewardName->setAnchorPoint( ccp( 0, 0 ) );  
		label_rewardName->setPosition( ccp( 50 + 10, 28) );
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		label_rewardName->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		this->addChild(label_rewardName);
		
		std::string str_rewardNum = "  X";
		label_rewardNum = CCLabelTTF::create();
		char str_num[20];
		sprintf(str_num,"%d",num);
		str_rewardNum.append(str_num);
		label_rewardNum->setString(str_rewardNum.c_str());
		label_rewardNum->setFontName(APP_FONT_NAME);
		label_rewardNum->setFontSize(14);
		label_rewardNum->ignoreAnchorPointForPosition( false );  
		label_rewardNum->setAnchorPoint( ccp( 0, 0 ) );  
		label_rewardNum->setPosition( ccp( 50 + 10, 0) );  
		label_rewardNum->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		this->addChild(label_rewardNum);

		this->setContentSize(CCSizeMake(150,50));
		return true;
	}
	return false;
}
