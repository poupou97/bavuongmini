#include "MissionRewardGoodsItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/backpackscene/EquipmentItem.h"
#include "AppMacros.h"

MissionRewardGoodsItem::MissionRewardGoodsItem()
{
}


MissionRewardGoodsItem::~MissionRewardGoodsItem()
{
	delete curGoodsInfo;
}

MissionRewardGoodsItem * MissionRewardGoodsItem::create( GoodsInfo* goods,int num )
{
	MissionRewardGoodsItem *widget = new MissionRewardGoodsItem();
	if (widget && widget->initWithGoods(goods,num))
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}

bool MissionRewardGoodsItem::initWithGoods( GoodsInfo* goods,int num )
{
	if (UIWidget::init())
	{
		curGoodsInfo = new GoodsInfo();
		curGoodsInfo->CopyFrom(*goods);
		/*********************判断装备的颜色***************************/
		std::string frameColorPath;
		if (curGoodsInfo->quality() == 1)
		{
			frameColorPath = EquipWhiteFramePath;
		}
		else if (curGoodsInfo->quality() == 2)
		{
			frameColorPath = EquipGreenFramePath;
		}
		else if (curGoodsInfo->quality() == 3)
		{
			frameColorPath = EquipBlueFramePath;
		}
		else if (curGoodsInfo->quality() == 4)
		{
			frameColorPath = EquipPurpleFramePath;
		}
		else if (curGoodsInfo->quality() == 5)
		{
			frameColorPath = EquipOrangeFramePath;
		}
		else
		{
			frameColorPath = EquipVoidFramePath;
		}

		UIButton *Btn_goodsItemFrame = UIButton::create();
		Btn_goodsItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_goodsItemFrame->setTouchEnable(true);
		Btn_goodsItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_goodsItemFrame->setPosition(ccp(Btn_goodsItemFrame->getContentSize().width/2,Btn_goodsItemFrame->getContentSize().height/2));
		Btn_goodsItemFrame->addReleaseEvent(this,coco_releaseselector(MissionRewardGoodsItem::GoodItemEvent));
		Btn_goodsItemFrame->setPressedActionEnabled(true);
		this->addChild(Btn_goodsItemFrame);

		UIImageView * uiiImageView_goodsItem = UIImageView::create();
		if (strcmp(goods->icon().c_str(),"") == 0)
		{
			uiiImageView_goodsItem->setTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(goods->icon().c_str());
			_path.append(".png");
			uiiImageView_goodsItem->setTexture(_path.c_str());
		}
		uiiImageView_goodsItem->setScale(0.9f);
		uiiImageView_goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		uiiImageView_goodsItem->setPosition(ccp(0,0));
		Btn_goodsItemFrame->addChild(uiiImageView_goodsItem);

		UILabel * l_num = UILabel::create();
		char s_num[20];
		sprintf(s_num,"%d",num);
		l_num->setText(s_num);
		l_num->setFontName(APP_FONT_NAME);
		l_num->setFontSize(13);
		l_num->setAnchorPoint(ccp(1.0f,0));
		l_num->setPosition(ccp(Btn_goodsItemFrame->getContentSize().width/2-10,7-Btn_goodsItemFrame->getContentSize().height/2));
		Btn_goodsItemFrame->addChild(l_num);

		if (goods->binding() == 1)
		{
			UIImageView *ImageView_bound = UIImageView::create();
			ImageView_bound->setTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(ccp(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(ccp(8-Btn_goodsItemFrame->getContentSize().width/2,55-Btn_goodsItemFrame->getContentSize().height/2));
			Btn_goodsItemFrame->addChild(ImageView_bound);
		}

		//如果是装备
		if(goods->has_equipmentdetail())
		{
			if (goods->equipmentdetail().gradelevel() > 0)  //精练等级
			{
				std::string ss_gradeLevel = "+";
				char str_gradeLevel [10];
				sprintf(str_gradeLevel,"%d",goods->equipmentdetail().gradelevel());
				ss_gradeLevel.append(str_gradeLevel);
				UILabel *Lable_gradeLevel = UILabel::create();
				Lable_gradeLevel->setText(ss_gradeLevel.c_str());
				Lable_gradeLevel->setFontName(APP_FONT_NAME);
				Lable_gradeLevel->setFontSize(13);
				Lable_gradeLevel->setAnchorPoint(ccp(1.0f,1.0f));
				Lable_gradeLevel->setPosition(ccp(Btn_goodsItemFrame->getContentSize().width/2-10,Btn_goodsItemFrame->getContentSize().height/2-7));
				Lable_gradeLevel->setName("Lable_gradeLevel");
				Btn_goodsItemFrame->addChild(Lable_gradeLevel);
			}

			if (goods->equipmentdetail().starlevel() > 0)  //星级
			{
				char str_starLevel [10];
				sprintf(str_starLevel,"%d",goods->equipmentdetail().starlevel());
				UILabel *Lable_starLevel = UILabel::create();
				Lable_starLevel->setText(str_starLevel);
				Lable_starLevel->setFontName(APP_FONT_NAME);
				Lable_starLevel->setFontSize(13);
				Lable_starLevel->setAnchorPoint(ccp(0,0));
				Lable_starLevel->setPosition(ccp(8-Btn_goodsItemFrame->getContentSize().width/2,Btn_goodsItemFrame->getContentSize().height/2-6));
				Lable_starLevel->setName("Lable_starLevel");
				Btn_goodsItemFrame->addChild(Lable_starLevel);

				UIImageView *ImageView_star = UIImageView::create();
				ImageView_star->setTexture("res_ui/star_on.png");
				ImageView_star->setAnchorPoint(ccp(0,0));
				ImageView_star->setPosition(ccp(Lable_starLevel->getContentSize().width,2));
				ImageView_star->setVisible(true);
				ImageView_star->setScale(0.5f);
				Lable_starLevel->addChild(ImageView_star);
			}
		}

		this->setTouchEnable(true);
		this->setSize(CCSizeMake(63,63));

		return true;
	}
	return false;
}

void MissionRewardGoodsItem::GoodItemEvent( CCObject *pSender )
{
	CCSize size= CCDirector::sharedDirector()->getVisibleSize();
	GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(curGoodsInfo,GameView::getInstance()->EquipListItem,0);
	goodsItemInfoBase->ignoreAnchorPointForPosition(false);
	goodsItemInfoBase->setAnchorPoint(ccp(0.5f,0.5f));
	//goodsItemInfoBase->setPosition(ccp(size.width/2,size.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
}
