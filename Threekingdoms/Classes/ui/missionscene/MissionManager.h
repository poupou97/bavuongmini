
#ifndef _MISSIONSCENE_MISSIONMANAGER_H_
#define _MISSIONSCENE_MISSIONMANAGER_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "MissionActions.h"

USING_NS_CC;

class MTalkWithNpcAction;
class MMonsterKillAction;
class MPickAction;
class MHandleViewAction;
class MOpenViewAction;
class MTalkMission;
class MFailAction;
class MissionInfo;
class MissionMessage;
class MMoveToTargetAndOpenUI;

/**
 * 任务管理者,处理任务流程
 * @author yangjun
 */

class MissionInfo;
class MMoveAction;

//20级前需要有主线任务的指引
#define K_GuideForMainMission_Level 20
//主线任务的指引消失12S后再次出现
#define K_GuideForMainMission_Normal_RecoverTime 15.f
#define K_GuideForMainMission_MonsterKilll_RecoverTime 40.f

enum MissionPackageType
{
	MPT_Main = 1,                    //主线
	BRANCH,                             //支线
	GANG,                                 //宗师
	SEVEN_TIMES,                     //七进七出
	BEAN_SOLDIERS,                 //撒豆成兵
	SNEAK_ATTACK,                  //夜袭乌巢
	FAMILY,                               //家族
	TAVERN,                              //酒馆
	WAR,                                   //南征北战
};

class MissionManager
{
public:

	enum MActionType
	{
		KtypeDoNothing = 0,
		KTypeMMoveAction = 1,
		KTypeMMonsterKillAction = 2,
		KTypeMPickAction = 3,
		KTypeMTalkWithNpcAction = 4,
		KTypeMHandleViewAction = 5,
		KTypeMOpenViewAction = 6,
		KTypeMOpenNpcFunction = 7,
		KTypeMTalkMissionAction = 8,
		KTypeMoveToTargetAndOpenUI = 9,
		KTypeMFailAction = 10,
	};

	enum CurMissionStatus {
		status_none = 0,
		status_waitForTransport,   // the player is in the the waiting sequence to transport
		status_transportFinished,   // the player is finished the transport
	};

	MissionManager();
	virtual ~MissionManager();

	static MissionManager* getInstance();

	void update();

	//指引进行主线任务
	bool isExistGuideForMmission;
	float guideForMmission_time;
	void addGuideForMainMission();

public:
	MissionInfo * curMissionInfo;
	bool isAutoRunForMission;
	//任务列表
	std::vector<MissionInfo *> MissionList;
	std::vector<MissionInfo *> MissionList_MainScene;
	std::vector<MissionInfo *> curMissionList;
	void addMission(MissionInfo *missionInfo);
	void doMission(MissionInfo *missionInfo);

	bool isOutOfMissionTalkArea(int missionNpcId);

	void autoPopUpHandleMission();

	void showHandleMission(CCNode* sender, void* data);

	int m_nTargetNpcId;

	void setMissionAndTeamOffSet(CCPoint temp);
	CCPoint getMissionAndTeamOffSet();

	bool getIsFirstCreateMissionAndTeam();
	void setIsFirstCreateMissionAndTeam(bool _value);

 	static void setStatus(int status);
 	static int getStatus();

	void AcrossMapTransport(std::string mapId,CCPoint pos);
	CCPoint getNearestReachablePos(CCPoint& targetPoint);

	//上一个完成的任务的包ID
	std::string m_sLastMissionPackageId;
	//上一个完成的任务在主任务面板中的位置信息
	int m_nLastMissionIndexInMain;
	//上一个完成的任务提交前的偏移量
	CCPoint m_pLastMissionOffSet;
	CCSize m_missionTableViewSize;

	//检测是否能够寻路
	bool CheckIsEnableToRunToTarget(const char * mapid,CCPoint pos);

private:
	MActionType curActionType;

	MMoveAction * curMoveAction;
	MTalkWithNpcAction * curTalkWithNpcAction;
	MMonsterKillAction * curMonsterKillAction;
	MPickAction * curPickAction;
	MHandleViewAction * curHandleAction;
	MOpenViewAction * curOpenAction;
	MOpenNpcFunction * curOpenNpcFunction;
	MTalkMission * curTalkMission;
	MMoveToTargetAndOpenUI * curMoveToTargetAndOpenUIAction;
	MFailAction * curFailAction;

	bool m_bIsFirstCreateMissionAndTeam;
	CCPoint cp_offSet;

	static int s_status;
};
#endif;

