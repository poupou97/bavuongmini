#include "MissionScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"
#include "../extensions/UITab.h"
#include "MissionTalkWithNpc.h"
#include "HandleMission.h"
#include "GameView.h"
#include "MissionManager.h"
#include "../extensions/CCRichLabel.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../../messageclient/element/MissionInfo.h"
#include "RewardItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../ui/backpackscene/EquipmentItem.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/GameUtils.h"

#define  M_SCROLLVIEWTAG 44

using namespace CocosDenshion;

#define  MISSIONSCENE_TABALEVIEW_HIGHLIGHT_TAG 85

MissionScene::MissionScene():
selectCellIdx(0)
{
}
	


MissionScene::~MissionScene()
{
	delete curMissionInfo;
}

MissionScene* MissionScene::create()
{
	MissionScene * missionScene = new MissionScene();
	if(missionScene && missionScene->init())
	{
		missionScene->autorelease();
		return missionScene;
	}
	CC_SAFE_DELETE(missionScene);
	return NULL;
}

bool MissionScene::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		curMissionInfo = new MissionInfo();

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(800, 480));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		//加载UI
		if(LoadSceneLayer::MissionSceneLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::MissionSceneLayer->removeFromParentAndCleanup(false);
		}
		UIPanel *ppanel = LoadSceneLayer::MissionSceneLayer;
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		const char * secondStr = StringDataManager::getString("UIName_ren_1");
		const char * thirdStr = StringDataManager::getString("UIName_wu_2");
		CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(ccp(30,240));
		u_layer->addChild(atmature);

		//关闭按钮
		UIButton * Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(MissionScene::CloseEvent));
		Button_close->setPressedActionEnabled(true);
		//放弃任务
		Button_giveUpMission = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_fangqirenwu");
		Button_giveUpMission->setTouchEnable(true);
		Button_giveUpMission->setPressedActionEnabled(true);
		Button_giveUpMission->addReleaseEvent(this, coco_releaseselector(MissionScene::GiveUpMissionEvent));
		Button_giveUpMission->setVisible(false);
		//自动寻路
		Button_autoPathFinder = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_zidongxunlu");
		Button_autoPathFinder->setTouchEnable(true);
		Button_autoPathFinder->setPressedActionEnabled(true);
		Button_autoPathFinder->addReleaseEvent(this, coco_releaseselector(MissionScene::AutoPathFindEvent));
		
		l_autoPathFinder = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_zidongxunlu");

		//maintab
		const char * normalImage = "res_ui/tab_1_off.png";
		const char * selectImage = "res_ui/tab_1_on.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_1_on.png";
		char * stateNames[] ={"已接","可接"};
		mainTab = UITab::createWithText(2,normalImage,selectImage,finalImage,stateNames,HORIZONTAL,9);
		mainTab->setAnchorPoint(ccp(0,0));
		mainTab->setPosition(ccp(68,412));
		mainTab->setHighLightImage((char * )highLightImage);
		mainTab->setDefaultPanelByIndex(0);
		mainTab->addIndexChangedEvent(this,coco_indexchangedselector(MissionScene::MainTabIndexChangedEvent));
		mainTab->setPressedActionEnabled(true);
		u_layer->addWidget(mainTab);

		m_tableView = CCTableView::create(this,CCSizeMake(249,352));
		m_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
		m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 25, 1, 1), ccp(0,0));   // 设置九宫格图片
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0,0));
		m_tableView->setPosition(ccp(77,47));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		m_tableView->setPressedActionEnabled(true);
		u_layer->addChild(m_tableView);

		UILayer * Layer_tableViewFrame = UILayer::create();
		Layer_tableViewFrame->ignoreAnchorPointForPosition(false);
		Layer_tableViewFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Layer_tableViewFrame->setContentSize(CCSizeMake(800, 480));
		Layer_tableViewFrame->setPosition(CCPointZero);
		Layer_tableViewFrame->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(Layer_tableViewFrame);
// 		UIImageView * imageView_tableViewFrame = UIImageView::create();
// 		imageView_tableViewFrame->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		imageView_tableViewFrame->setScale9Enable(true);
// 		imageView_tableViewFrame->setScale9Size(CCSizeMake(261,368));
// 		imageView_tableViewFrame->setCapInsets(CCRectMake(30,30,1,1));
// 		imageView_tableViewFrame->setAnchorPoint(ccp(0,0));
// 		imageView_tableViewFrame->setPosition(ccp(65,39));
// 		Layer_tableViewFrame->addWidget(imageView_tableViewFrame);

		l_haveNoMissionToGet = UILabel::create();
		l_haveNoMissionToGet->setText(StringDataManager::getString("mission_haveNoMissionForGet"));
		l_haveNoMissionToGet->setAnchorPoint(ccp(0.5f,0.5f));
		l_haveNoMissionToGet->setPosition(ccp(196,234));
		l_haveNoMissionToGet->setFontName(APP_FONT_NAME);
		l_haveNoMissionToGet->setFontSize(18);
		Layer_tableViewFrame->addWidget(l_haveNoMissionToGet);
		l_haveNoMissionToGet->setVisible(false);

		l_haveNoMissionGeted = UILabel::create();
		l_haveNoMissionGeted->setText(StringDataManager::getString("mission_haveNoMissionGeted"));
		l_haveNoMissionGeted->setAnchorPoint(ccp(0.5f,0.5f));
		l_haveNoMissionGeted->setPosition(ccp(196,234));
		l_haveNoMissionGeted->setFontName(APP_FONT_NAME);
		l_haveNoMissionGeted->setFontSize(18);
		Layer_tableViewFrame->addWidget(l_haveNoMissionGeted);
		l_haveNoMissionGeted->setVisible(false);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);

		//已接任务
		this->updataCurDataSourceByMissionState(1);
		//reload
		m_tableView->reloadData();
		//设置默认
		if (this->curMissionDataSource.size()>0)
		{
			curMissionInfo->CopyFrom(*this->curMissionDataSource.at(0));
			//Button_giveUpMission->setVisible(true);
			m_tableView->selectCell(0);
			selectCellIdx = 0;
		}
		
		this->ReloadMissionInfoData();
		
		return true;
	}
	return false;
}

void MissionScene::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void MissionScene::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}


bool MissionScene::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	return this->resignFirstResponder(touch,this,false);
}

void MissionScene::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void MissionScene::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void MissionScene::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}


void MissionScene::CloseEvent( CCObject* pSender )
{
	this->closeAnim();
}

void MissionScene::GiveUpMissionEvent( CCObject * pSender )
{
	if (this->curMissionDataSource.size()>0)
	{
		if (curMissionInfo->missionstate()>0)
		{
			GameUtils::playGameSound(MISSION_CANCEL, 2, false);
			//放弃任务
			GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*this->curMissionInfo);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(7009, this);
		}		
	}
}

void MissionScene::AutoPathFindEvent( CCObject * pSender )
{
	if (this->curMissionDataSource.size()>0)
	{
		GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*this->curMissionInfo);
		GameView::getInstance()->missionManager->addMission(curMissionInfo);

		this->closeAnim();
	}
}

void MissionScene::MainTabIndexChangedEvent(CCObject* pSender)
{
	switch((int)mainTab->getCurrentIndex())
	{
	case 0 :
		//Button_giveUpMission->setVisible(false);
		this->updataCurDataSourceByMissionState(1);
		//reload
		m_tableView->reloadData();
		//设置默认
		if (this->curMissionDataSource.size()>0)
		{
			curMissionInfo->CopyFrom(*this->curMissionDataSource.at(0));
			m_tableView->selectCell(0);
			selectCellIdx = 0;
		}
		
		this->ReloadMissionInfoData();
		break;
	case 1 :
		//Button_giveUpMission->setVisible(true);
		this->updataCurDataSourceByMissionState(0);
		m_tableView->reloadData();
		//设置默认
		if (this->curMissionDataSource.size()>0)
		{
			curMissionInfo->CopyFrom(*(this->curMissionDataSource.at(0)));
			m_tableView->selectCell(0);
			selectCellIdx = 0;
		}
		
		this->ReloadMissionInfoData();
		break;
	}
}

CCSize MissionScene::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
	return CCSizeMake(242, 60);
}

CCTableViewCell* MissionScene::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();

	MissionInfo * missionInfo = this->curMissionDataSource.at(idx);

	CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/kuang2_new.png");
	sprite_frame->setContentSize(CCSizeMake(242,60));
	sprite_frame->setCapInsets(CCRectMake(45,45,1,1));
	sprite_frame->setAnchorPoint(ccp(0,0));
	sprite_frame->setPosition(ccp(0,0));
	cell->addChild(sprite_frame);
	
	std::string str_missionType;
	if (missionInfo->missionpackagetype() == 1)
	{
		str_missionType = StringDataManager::getString("mission_zhu");
	}
	else if(missionInfo->missionpackagetype() == 2)
	{
		str_missionType = StringDataManager::getString("mission_zhi");
	}
	else if(missionInfo->missionpackagetype() == 7)
	{
		str_missionType = StringDataManager::getString("mission_jia");
	}
	else if(missionInfo->missionpackagetype() == 12)
	{
		str_missionType = StringDataManager::getString("mission_xuan");
	}
	else
	{
		str_missionType = StringDataManager::getString("mission_ri");
	}

	str_missionType.append(missionInfo->missionname().c_str());
	//mission name
	CCLabelTTF * pMissionName = CCLabelTTF::create(str_missionType.c_str(),APP_FONT_NAME,16);
	pMissionName->setAnchorPoint(ccp(0.f,0.5f));
	pMissionName->setPosition(ccp(40,sprite_frame->getContentSize().height/2));
	ccColor3B shadowColor = ccc3(0,0,0);   // black
	pMissionName->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	sprite_frame->addChild(pMissionName);

	switch(missionInfo->missionstate())
	{
	case dispatched : //已分配  
		pMissionName->setColor(ccc3(254,254,51));                       //淡黄色
		break;
	case accepted : //已接受
		pMissionName->setColor(ccc3(0,255,240));                   //蓝色
		break;
	case done ://已完成
		pMissionName->setColor(ccc3(90,255,0));                     //绿色
		break;
		//case failed ://已失败
		//	break;
	case canceled ://已取消
		break;
	case submitted ://已提交
		break;
	}

	return cell;

}

unsigned int MissionScene::numberOfCellsInTableView(CCTableView *table)
{
	return this->curMissionDataSource.size();
}

void MissionScene::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
	selectCellIdx = cell->getIdx();
	curMissionInfo->CopyFrom(*this->curMissionDataSource.at(cell->getIdx()));
	this->ReloadMissionInfoData();

	Button_giveUpMission->setVisible(false);
	if (curMissionInfo->missionpackagetype() >= 3)
	{
		if (curMissionInfo->missionstate() > 0)
		{
			Button_giveUpMission->setVisible(true);
		}
	}

	if (curMissionInfo->action().action() == openSpecifiedUI)  //打开界面
	{
		if (curMissionInfo->action().specifiedui().id()  == kTagMissionScene)  
		{
			Button_autoPathFinder->setVisible(false);
		}
		else
		{
			Button_autoPathFinder->setVisible(true);
		}
	}
	else
	{
		Button_autoPathFinder->setVisible(true);
	}
}
void MissionScene::scrollViewDidScroll(CCScrollView* view )
{
}

void MissionScene::scrollViewDidZoom(CCScrollView* view )
{
}

void MissionScene::ReloadMissionInfoData()
{
	if (u_layer->getChildByTag(M_SCROLLVIEWTAG) != NULL)
	{
		u_layer->getChildByTag(M_SCROLLVIEWTAG)->removeFromParent();
	}

	if (curMissionDataSource.size() == 0)
		return;

	if (!curMissionInfo->has_missionid())
		return;

	int scroll_height = 0;
	int _space = 15;
	//name
	sprite_name = CCSprite::create("res_ui/renwubb/renwumingcheng.png");	
	label_name = CCLabelTTF::create(curMissionInfo->missionname().c_str(),APP_FONT_NAME,18,CCSizeMake(270, 0 ), kCCTextAlignmentLeft);
	ccColor3B shadowColor = ccc3(0,0,0);   // black
	label_name->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	int zeroLineHeight = label_name->getContentSize().height+_space;
	//target
	sprite_target = CCSprite::create("res_ui/renwubb/renwumubiao.png");	
	label_target = CCRichLabel::createWithString(curMissionInfo->summary().c_str(),CCSizeMake(270, 0 ),NULL,NULL ,0,18);
	//label_target->setScale(0.9f);
	int firstLineHeight = zeroLineHeight + label_target->getContentSize().height+_space;
	//description
	sprite_des = CCSprite::create("res_ui/renwubb/renwuxushu.png");
	label_des = CCRichLabel::createWithString(curMissionInfo->description().c_str(),CCSizeMake(270, 0 ), NULL,NULL,0,18,4);
	int secondLineHeight = firstLineHeight+label_des->getContentSize().height + _space;
	//rewardSprite
	sprite_award = CCSprite::create("res_ui/renwubb/renwujiangli.png");  
	//reward(gold,exp)
	bool isHaveExp = false;
	bool isHaveGold =  false; 
	CCSprite *sprite_expItemFrame;
	if (curMissionInfo->rewards().has_exp() && curMissionInfo->rewards().exp() > 0)
	{
		sprite_expItemFrame = CCSprite::create(EquipOrangeFramePath);

		CCSprite * sprite_goodsItem = CCSprite::create("res_ui/exp.png");
		sprite_goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_goodsItem->setPosition(ccp(sprite_expItemFrame->getContentSize().width/2,sprite_expItemFrame->getContentSize().height/2));
		sprite_expItemFrame->addChild(sprite_goodsItem);
		sprite_expItemFrame->setScale(0.8f);

		char s[20];
		sprintf(s,"%d",curMissionInfo->rewards().exp());
		CCLabelTTF * l_expValue = CCLabelTTF::create(s,APP_FONT_NAME,18);
		l_expValue->setAnchorPoint(ccp(0,0.5f));
		l_expValue->setPosition(ccp(80,28));
		sprite_expItemFrame->addChild(l_expValue);

		isHaveExp = true;
	}

	CCSprite *sprite_goldItemFrame;
	if (curMissionInfo->rewards().has_gold() && curMissionInfo->rewards().gold() > 0)
	{
		sprite_goldItemFrame = CCSprite::create(EquipOrangeFramePath);

		CCSprite * sprite_goodsItem = CCSprite::create("res_ui/coins.png");
		sprite_goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_goodsItem->setPosition(ccp(sprite_goldItemFrame->getContentSize().width/2,sprite_goldItemFrame->getContentSize().height/2));
		sprite_goldItemFrame->addChild(sprite_goodsItem);
		sprite_goldItemFrame->setScale(0.8f);

		char s[20];
		sprintf(s,"%d",curMissionInfo->rewards().gold());
		CCLabelTTF * l_goldValue = CCLabelTTF::create(s,APP_FONT_NAME,18);
		l_goldValue->setAnchorPoint(ccp(0,0.5f));
		l_goldValue->setPosition(ccp(80,25));
		sprite_goldItemFrame->addChild(l_goldValue);

		isHaveGold = true;
	}

	int thirdLineHeight;
	if (isHaveExp || isHaveGold)
	{
		 thirdLineHeight = secondLineHeight + 50 + _space;
	}
	else
	{
		thirdLineHeight = secondLineHeight + _space;
	}
	//reward(goods).
	int fourthLineHeight = thirdLineHeight;
	if (curMissionInfo->has_rewards())
	{
		for(int i = 0;i<curMissionInfo->rewards().goods_size();++i)
		{
			CCSize awardSize = CCSizeMake(150,50);
			if (i%2 == 0)
			{
				fourthLineHeight += awardSize.height;
			}
		}
	}
	
	scroll_height = fourthLineHeight+_space;

	m_contentScrollView = CCScrollView::create(CCSizeMake(389,293));
	m_contentScrollView->setViewSize(CCSizeMake(389, 293));
	m_contentScrollView->ignoreAnchorPointForPosition(false);
	m_contentScrollView->setTouchEnabled(true);
	m_contentScrollView->setDirection(kCCScrollViewDirectionVertical);
	m_contentScrollView->setAnchorPoint(ccp(0,0));
	m_contentScrollView->setPosition(ccp(345,104));
	m_contentScrollView->setBounceable(true);
	m_contentScrollView->setClippingToBounds(true);
	m_contentScrollView->setTag(M_SCROLLVIEWTAG);
	u_layer->addChild(m_contentScrollView);
	if (scroll_height > 293)
	{
		m_contentScrollView->setContentSize(ccp(389,scroll_height));
		m_contentScrollView->setContentOffset(ccp(0,293-scroll_height));  
	}
	else
	{
		m_contentScrollView->setContentSize(ccp(389,293));
	}
	m_contentScrollView->setClippingToBounds(true);

	sprite_name->setAnchorPoint(ccp(0,1.0f));
	sprite_name->setPosition( ccp( 15, m_contentScrollView->getContentSize().height -zeroLineHeight) );
	m_contentScrollView->addChild(sprite_name);
	label_name->setAnchorPoint(ccp(0,1.0f));
	label_name->setPosition( ccp( sprite_name->getContentSize().width+15, m_contentScrollView->getContentSize().height -zeroLineHeight) );
	m_contentScrollView->addChild(label_name);

	sprite_target->setAnchorPoint(ccp(0,1.0f));
	sprite_target->setPosition( ccp( 15, m_contentScrollView->getContentSize().height -firstLineHeight+label_target->getContentSize().height-sprite_target->getContentSize().height) );
	m_contentScrollView->addChild(sprite_target);
	label_target->setAnchorPoint(ccp(0,1.0f));
	label_target->setPosition( ccp( sprite_target->getContentSize().width+15, m_contentScrollView->getContentSize().height -firstLineHeight) );
	m_contentScrollView->addChild(label_target);

	sprite_des->setAnchorPoint(ccp(0,1.0f));
	sprite_des->setPosition( ccp( 15, m_contentScrollView->getContentSize().height -secondLineHeight+label_des->getContentSize().height-sprite_des->getContentSize().height) );
	m_contentScrollView->addChild(sprite_des);
	label_des->setAnchorPoint(ccp(0,1.0f));
	label_des->setPosition( ccp( sprite_des->getContentSize().width+15, m_contentScrollView->getContentSize().height -secondLineHeight) );
	m_contentScrollView->addChild(label_des);

	sprite_award->setAnchorPoint(ccp(0,1.0f));
	
	if (isHaveExp || isHaveGold)
	{
		sprite_award->setPosition( ccp( 15, m_contentScrollView->getContentSize().height -thirdLineHeight+25) );
	}
	else
	{
		sprite_award->setPosition( ccp( 15, m_contentScrollView->getContentSize().height -thirdLineHeight-25) );
	}

	m_contentScrollView->addChild(sprite_award);

	if (isHaveExp && isHaveGold)
	{
		sprite_expItemFrame->ignoreAnchorPointForPosition(false);
		sprite_expItemFrame->setAnchorPoint(ccp(0,1.0f));
		sprite_expItemFrame->setPosition( ccp( 120, m_contentScrollView->getContentSize().height -thirdLineHeight) );
		m_contentScrollView->addChild(sprite_expItemFrame);
		sprite_goldItemFrame->ignoreAnchorPointForPosition(false);
		sprite_goldItemFrame->setAnchorPoint(ccp(0,1.0f));
		sprite_goldItemFrame->setPosition( ccp( 261, m_contentScrollView->getContentSize().height -thirdLineHeight) );
		m_contentScrollView->addChild(sprite_goldItemFrame);
	}
	else if (isHaveExp && (!isHaveGold))
	{
		sprite_expItemFrame->ignoreAnchorPointForPosition(false);
		sprite_expItemFrame->setAnchorPoint(ccp(0,1.0f));
		sprite_expItemFrame->setPosition( ccp( 120, m_contentScrollView->getContentSize().height -thirdLineHeight) );
		m_contentScrollView->addChild(sprite_expItemFrame);
	}
	else if (isHaveGold && (!isHaveExp))
	{
		sprite_goldItemFrame->ignoreAnchorPointForPosition(false);
		sprite_goldItemFrame->setAnchorPoint(ccp(0,1.0f));
		sprite_goldItemFrame->setPosition( ccp( 120, m_contentScrollView->getContentSize().height -thirdLineHeight) );
		m_contentScrollView->addChild(sprite_goldItemFrame);
	}
	else
	{

	}

	if (curMissionInfo->has_rewards())
	{
		for(int i = 0;i<curMissionInfo->rewards().goods_size();++i)
		{
			GoodsInfo * goodsInfo = new GoodsInfo();
			goodsInfo->CopyFrom(curMissionInfo->rewards().goods(i).goods());
			RewardItem * rewardItem = RewardItem::create(goodsInfo,curMissionInfo->rewards().goods(i).quantity());
			rewardItem->setTag(8569+i);
			m_contentScrollView->addChild(rewardItem);
			delete goodsInfo;
			if(i%2 == 0)
			{
				rewardItem->ignoreAnchorPointForPosition(false);
				rewardItem->setAnchorPoint(ccp(0,1.0f));
				if (isHaveExp || isHaveGold)
				{
					rewardItem->setPosition(ccp(120, m_contentScrollView->getContentSize().height - thirdLineHeight -5 - 5*((int)(i/2)) -  rewardItem->getContentSize().height*((int)(i/2))));
				}
				else
				{
					rewardItem->setPosition(ccp(120, m_contentScrollView->getContentSize().height -thirdLineHeight));
				}
			}
			else
			{
				rewardItem->ignoreAnchorPointForPosition(false);
				rewardItem->setAnchorPoint(ccp(0,1.0f));
				if (isHaveExp || isHaveGold)
				{
					rewardItem->setPosition(ccp(261, m_contentScrollView->getContentSize().height - thirdLineHeight -5 - 5*((int)(i/2)) - rewardItem->getContentSize().height*((int)(i/2))));
				}
				else
				{
					rewardItem->setPosition(ccp(261, m_contentScrollView->getContentSize().height -thirdLineHeight));
				}
			}
		}
	}

	Button_giveUpMission->setVisible(false);
	if (curMissionInfo->missionpackagetype() >= 3)
	{
		if (curMissionInfo->missionstate() > 0)
		{
			Button_giveUpMission->setVisible(true);
		}
	}

	if (curMissionInfo->action().action() == openSpecifiedUI)  //打开界面
	{
		if (curMissionInfo->action().specifiedui().id()  == kTagMissionScene)  
		{
			Button_autoPathFinder->setVisible(false);
		}
		else
		{
			Button_autoPathFinder->setVisible(true);
		}
	}
	else
	{
		Button_autoPathFinder->setVisible(true);
	}
}

void MissionScene::updataCurDataSourceByMissionState( int missionState )
{
	if (missionState == 1)  //已接
	{
		l_autoPathFinder->setText(StringDataManager::getString("mission_autoFind"));
		//first delete old data
		std::vector<MissionInfo*>::iterator iter;
		for (iter = curMissionDataSource.begin(); iter != curMissionDataSource.end(); ++iter)
		{
			delete *iter;
		}
		curMissionDataSource.clear();
		for(int i = 0;i<GameView::getInstance()->missionManager->MissionList.size();i++)
		{
			if (GameView::getInstance()->missionManager->MissionList.at(i)->missionstate()>0)
			{
				MissionInfo * temp = new MissionInfo();
				temp->CopyFrom(*GameView::getInstance()->missionManager->MissionList.at(i));
				this->curMissionDataSource.push_back(temp);
			}
		}

		l_haveNoMissionToGet->setVisible(false);
		if (curMissionDataSource.size()<=0)//当前没有已接任务
		{
			l_haveNoMissionGeted->setVisible(true);
		}
		else
		{
			l_haveNoMissionGeted->setVisible(false);
		}
	}
	else if (missionState == 0)   //可接
	{
		l_autoPathFinder->setText(StringDataManager::getString("mission_getMission"));
		//first delete old data
		std::vector<MissionInfo*>::iterator iter;
		for (iter = curMissionDataSource.begin(); iter != curMissionDataSource.end(); ++iter)
		{
			delete *iter;
		}
		curMissionDataSource.clear();
		for(int i = 0;i<GameView::getInstance()->missionManager->MissionList.size();i++)
		{
			if (GameView::getInstance()->missionManager->MissionList.at(i)->missionstate() == 0)
			{
				MissionInfo * temp = new MissionInfo();
				temp->CopyFrom(*GameView::getInstance()->missionManager->MissionList.at(i));
				this->curMissionDataSource.push_back(temp);
			}
		}

		l_haveNoMissionGeted->setVisible(false);
		if (curMissionDataSource.size()<=0)//当前没有可接任务
		{
			l_haveNoMissionToGet->setVisible(true);
		}
		else
		{
			l_haveNoMissionToGet->setVisible(false);
		}
	}
	else
	{

	}
}

void MissionScene::ReloadDataWithOutChangeOffSet()
{
	CCPoint _s = m_tableView->getContentOffset();
	int _h = m_tableView->getContentSize().height + _s.y;
	m_tableView->reloadData();
	CCPoint temp = ccp(_s.x,_h - m_tableView->getContentSize().height);
	m_tableView->setContentOffset(temp); 
}
