
#ifndef _GOLDSTOREUI_GOLDSTOREITEMINFO_H_
#define _GOLDSTOREUI_GOLDSTOREITEMINFO_H_

#include "../backpackscene/GoodsItemInfoBase.h"

class GoodsInfo;

class GoldStoreItemInfo : public GoodsItemInfoBase
{
public:
	GoldStoreItemInfo();
	~GoldStoreItemInfo();

	static GoldStoreItemInfo * create(GoodsInfo * goodsInfo);
	bool init(GoodsInfo * goodsInfo);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
};

#endif
