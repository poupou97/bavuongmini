
#ifndef _GOLDSTOREUI_GOODSITEM_H_
#define _GOLDSTOREUI_GOODSITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"

USING_NS_CC;
USING_NS_CC_EXT;

class GoodsInfo;

class GoodsItem :public UIWidget
{
public:
	GoodsItem();
	~GoodsItem();

	static GoodsItem * create(GoodsInfo* goods);
	bool initWithGoods(GoodsInfo* goods);

	void GoodItemEvent(CCObject *pSender);

private:
	GoodsInfo * curGoodsInfo;
};

#endif