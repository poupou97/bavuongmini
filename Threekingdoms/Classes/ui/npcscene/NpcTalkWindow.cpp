#include "NpcTalkWindow.h"
#include "../../messageclient/element/CNpcDialog.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/CCRichLabel.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../missionscene/MissionTalkWithNpc.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../../gamescene_state/role/FunctionNPC.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../../../../cocos2dx/cocoa/CCGeometry.h"
#include "../Auction_ui/AuctionUi.h"
#include "../storehouse_ui/StoreHouseUI.h"
#include "../shop_ui/ShopUI.h"
#include "../family_ui/ApplyFamily.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../family_ui/ManageFamily.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"
#include "../../messageclient/element/CGuildBase.h"
#include "../../utils/GameUtils.h"
#include "GameAudio.h"
#include "../challengeRound/ChallengeRoundUi.h"
#include "../Active_ui/WorldBoss_ui/WorldBossUI.h"
#include "../../ui/rewardTask_ui/RewardTaskMainUI.h"
#include "../rewardTask_ui/RewardTaskData.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTeachingGuide.h"

/// 弹板功能
#define FUNCTION_INHERIT -1       // 继承
#define FUNCTION_SHANGDIAN 1       //商店
#define FUNCTION_CANGKU 6       // 打开仓库
#define FUNCTION_XIULIZHUANGBEI 10       // 修理装备
#define FUNCTION_WUJIANGFUHUO 19   //武将复活
#define FUNCTION_JISHOU 51       // 继承
#define FUNCTION_MY_JISHOU 52    // 我的寄售
#define FUNCTION_CLOSE 53        // 关闭窗体
#define FUNCTION_XIANGQIAN 55    // 镶嵌
#define FUNCTION_XILIAN 56       // 洗练
#define FUNCTION_FENGYIN 57      // 封印
#define FUNCTION_TRANSFOR 5   // 传送功能
#define FUNCTION_FAMILYCREATE 2       // 家族创建
#define FUNCTION_FAMILYAPPLY 18       // 家族申请
#define FUNCTION_DUIHUANMA 59       // 兑换码
#define FUNCTION_SHIJIEBOSS 62          //世界Boss
#define FUNCTION_XUANSHANGRENWU 63          //悬赏任务

#define FUNCTION_ANSWER 17    //答题

#define FUNCTTON_SINGCOPY 21 //过关斩将 单人副本
NpcTalkWindow::NpcTalkWindow()
{
}


NpcTalkWindow::~NpcTalkWindow()
{
}


void NpcTalkWindow::update( float dt )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow) != NULL)
	{
		if (GameView::getInstance()->missionManager->isOutOfMissionTalkArea(this->getLockedNpcId()))
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
			this->closeAnim();
		}
	}
}


NpcTalkWindow* NpcTalkWindow::create( CNpcDialog * npcDialog )
{
	NpcTalkWindow * npcTalkWindow = new NpcTalkWindow();
	if (npcTalkWindow && npcTalkWindow->init(npcDialog))
	{
		npcTalkWindow->autorelease();
		return npcTalkWindow;
	}
	CC_SAFE_DELETE(npcTalkWindow);
	return NULL;
}

bool NpcTalkWindow::init( CNpcDialog * npcDialog )
{
	if (UIScene::init())
	{
		m_nLockedId = GameView::getInstance()->myplayer->getLockedActorId();
		curNpcDialog = npcDialog;

		std::map<long long ,CNpcInfo*>::const_iterator cIter;
		cIter = GameWorld::NpcInfos.find(m_nLockedId);
		if (cIter == GameWorld::NpcInfos.end()) // 没找到就是指向END了  
		{
			return false;
		}

		CNpcInfo * npcInfo = GameWorld::NpcInfos[m_nLockedId];

		winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();
		//加载UI
// 		if(LoadSceneLayer::TalkWithNpcLayer->getWidgetParent() != NULL)
// 		{
// 			LoadSceneLayer::TalkWithNpcLayer->removeFromParentAndCleanup(false);
// 		}
// 		//ppanel = (UIPanel*)LoadSceneLayer::s_pPanel->copy();
// 		UIPanel *ppanel = LoadSceneLayer::TalkWithNpcLayer;
// 		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
// 		ppanel->getValidNode()->setContentSize(CCSizeMake(350, 420));
// 		ppanel->setPosition(CCPointZero);
// 		ppanel->setScale(1.0f);
// 		ppanel->setTouchEnable(true);
// 		m_pUiLayer->addWidget(ppanel);

		UIPanel * ppanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/talkWithNpc_1.json");
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->setPosition(CCPointZero);	
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

// 		UIPanel * panel_mission = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_renwu");
// 		panel_mission->setVisible(false);
		UIButton * button_close = (UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_close");
		button_close->setTouchEnable(true);
		button_close->addReleaseEvent(this,coco_releaseselector(NpcTalkWindow::CloseEvent));
// 		UIImageView *ImageView_headImage = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_touxiang");;
// 		std::string headPath = "res_ui/npc/";
// 		headPath.append(npcInfo->icon.c_str());
// 		headPath.append(".png");
// 		ImageView_headImage->setTexture(headPath.c_str());
		UIImageView *ImageView_name = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_mingzi");

		// load animation
		std::string animFileName = "res_ui/npc/";
		animFileName.append(npcInfo->icon.c_str());
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/npc/caiwenji.anm";
		CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
		if (m_pAnim)
		{
			m_pAnim->setPlayLoop(true);
			m_pAnim->setReleaseWhenStop(false);
			m_pAnim->setScale(1.0f);
			m_pAnim->setPosition(ccp(33,312));
			//m_pAnim->setPlaySpeed(.35f);
			addChild(m_pAnim);
		}

		CCSprite * sprite_anmFrame = CCSprite::create("res_ui/renwua/tietu2.png");
		sprite_anmFrame->setAnchorPoint(ccp(0.5,0.5f));
		sprite_anmFrame->setPosition(ccp(116,356));
		addChild(sprite_anmFrame);

		ccFontDefinition strokeShaodwTextDef;
		strokeShaodwTextDef.m_fontName = std::string(APP_FONT_NAME);
		ccColor3B tintColorGreen   =  { 54, 92, 7 };
		strokeShaodwTextDef.m_fontFillColor   = tintColorGreen;   // 字体颜色
		strokeShaodwTextDef.m_fontSize = 22;   // 字体大小
// 		strokeShaodwTextDef.m_stroke.m_strokeEnabled = true;   // 是否勾边
// 		strokeShaodwTextDef.m_stroke.m_strokeColor   = blackColor;   // 勾边颜色，黑色
// 		strokeShaodwTextDef.m_stroke.m_strokeSize    = 2.0;   // 勾边的大小
	/*	CCLabelTTF* l_nameLabel = CCLabelTTF::createWithFontDefinition(npcInfo->npcName.c_str(), strokeShaodwTextDef);
		ccColor3B tintColorBlack   =  { 0, 0, 0 };
		l_nameLabel->enableShadow(CCSizeMake(2.0, -2.0), 0.5, 0.0, tintColorBlack); 
		l_nameLabel->setAnchorPoint(ccp(0.5,0.5f));
		l_nameLabel->setPosition(ccp(184,359));
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		l_nameLabel->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		addChild(l_nameLabel);*/
		CCLabelTTF* l_nameLabel = CCLabelTTF::create(npcInfo->npcName.c_str(),APP_FONT_NAME,22);
		l_nameLabel->setAnchorPoint(ccp(0.5,0.5f));
		l_nameLabel->setPosition(ccp(207,327));
		ccColor3B shadowColor = ccc3(0,0,0);   // black
		l_nameLabel->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		addChild(l_nameLabel);

		//richLabel_description
// 		CCLabelTTF * label_des = CCLabelTTF::create(npcInfo->welcome.c_str(),APP_FONT_NAME,18,CCSizeMake(285,0),kCCTextAlignmentLeft,kCCVerticalTextAlignmentTop);
// 		label_des->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
// 		CCSize desSize = label_des->getContentSize();
		CCRichLabel *label_des = CCRichLabel::createWithString(npcInfo->welcome.c_str(),CCSizeMake(285, 0),this,NULL,0,18,5);
		CCSize desSize = label_des->getContentSize();
		int height = desSize.height;
		//ccscrollView_description
		CCScrollView *m_contentScrollView = CCScrollView::create(CCSizeMake(290,99));
		m_contentScrollView->setViewSize(CCSizeMake(290, 99));
		m_contentScrollView->ignoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(kCCScrollViewDirectionVertical);
		m_contentScrollView->setAnchorPoint(ccp(0,0));
		m_contentScrollView->setPosition(ccp(30,205));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_pUiLayer->addChild(m_contentScrollView);
		if (height > 99)
		{
			m_contentScrollView->setContentSize(ccp(290,height));
			m_contentScrollView->setContentOffset(ccp(0,99-height));
		}
		else
		{
			m_contentScrollView->setContentSize(ccp(290,99));
		}
		m_contentScrollView->setClippingToBounds(true);

		m_contentScrollView->addChild(label_des);
		label_des->ignoreAnchorPointForPosition(false);
		label_des->setAnchorPoint(ccp(0,1));
		label_des->setPosition(ccp(3,m_contentScrollView->getContentSize().height-3));

		CCTableView * tableView_function = CCTableView::create(this,CCSizeMake(290,165));
		tableView_function->setPressedActionEnabled(true);
		tableView_function->setDirection(kCCScrollViewDirectionVertical);
		tableView_function->setAnchorPoint(ccp(0,0));
		tableView_function->setPosition(ccp(35,25));
		tableView_function->setDelegate(this);
		tableView_function->setVerticalFillOrder(kCCTableViewFillTopDown);
		m_pUiLayer->addChild(tableView_function);

// 		this->ignoreAnchorPointForPosition(false);
// 		this->setAnchorPoint(ccp(0,0.5f));
// 		this->setPosition(ccp(0,winsize.height/2));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(350,420));

		return true;
	}
	return false;
}


void NpcTalkWindow::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void NpcTalkWindow::onExit()
{
	UIScene::onExit();
}

bool NpcTalkWindow::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	CCPoint location = touch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	return false;
}

void NpcTalkWindow::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void NpcTalkWindow::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void NpcTalkWindow::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void NpcTalkWindow::CloseEvent( CCObject * pSender )
{
	//CCLOG("close npc window");
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
	this->closeAnim();
}


CCSize NpcTalkWindow::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
	return CCSizeMake(270, 42);
}

CCTableViewCell* NpcTalkWindow::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();

// 	CCSprite * pCellBg = CCSprite::create("res_ui/renwua/button_0.png");
// 	pCellBg->setAnchorPoint(ccp(0, 0));
// 	pCellBg->setPosition(ccp(0, 0));
// 	cell->addChild(pCellBg);
	CCScale9Sprite * pCellBg = CCScale9Sprite::create("res_ui/new_button_0.png");
	pCellBg->setContentSize(CCSizeMake(270,39));
	pCellBg->setCapInsets(CCRectMake(38,20,1,1));
	pCellBg->setAnchorPoint(ccp(0, 0));
	pCellBg->setPosition(ccp(0, 0));
	cell->addChild(pCellBg);

	CCLabelTTF * pMissionName = CCLabelTTF::create("hello girl ",APP_FONT_NAME,16);
	pMissionName->setAnchorPoint(ccp(0.5f,0.5f));
	pMissionName->setPosition(ccp(pCellBg->getContentSize().width/2,pCellBg->getContentSize().height/2));
	ccColor3B shadowColor = ccc3(0,0,0);   // black
	pMissionName->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	cell->addChild(pMissionName);

	if (isHasMission(m_nLockedId))
	{
		if (idx == 0)
		{
			const char *missionTitleStr = StringDataManager::getString("task_name_str");
			pMissionName->setString(missionTitleStr);
		}
		else
		{
			pMissionName->setString(curNpcDialog->options(idx-1).titel().c_str());
		}
	}
	else
	{
		pMissionName->setString(curNpcDialog->options(idx).titel().c_str());
	}
	

	return cell;
}


unsigned int NpcTalkWindow::numberOfCellsInTableView(CCTableView *table)
{
 	if (isHasMission(m_nLockedId))
 	{
		return curNpcDialog->options_size()+1;
 	}
 	else
 	{
 		return curNpcDialog->options_size();
 	}
}

void NpcTalkWindow::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
	int idx = cell->getIdx();
	int para = 0;

	int selfLevel_ =GameView::getInstance()->myplayer->getActiveRole()->level();
	//if (isHasMission(m_nLockedId))
	//{

	if (isHasMission(m_nLockedId))
	{
		para = idx -1;
	}
	else
	{
		para = idx;
	}

	if(idx == 0 && isHasMission(m_nLockedId))
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		/*bool a = isHasMission(GameView::getInstance()->myplayer->getLockedActor()->getRoleId());*/
		//先关闭这个界面
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
		this->closeAnim();
		//打开与npc交互的任务面板 
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission))
		{
			GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission)->removeFromParent();
		}

		GameSceneLayer* scene = GameView::getInstance()->getGameScene();
		FunctionNPC* fn = dynamic_cast<FunctionNPC*>(scene->getActor(m_nLockedId));
		//CNpcDialog * npcDialog = new CNpcDialog();
		//npcDialog->CopyFrom(*curNpcDialog);
		MissionTalkWithNpc * talkWithNpc = MissionTalkWithNpc::create(fn);
		GameView::getInstance()->getMainUIScene()->addChild(talkWithNpc,0,kTagTalkWithNpc);
		talkWithNpc->ignoreAnchorPointForPosition(false);
		talkWithNpc->setAnchorPoint(ccp(0,0.5f));
		talkWithNpc->setPosition(ccp(0,winSize.height/2));	
		//delete npcDialog;
	}
	else
	{
		switch(curNpcDialog->options(para).clientfunction())
		{
		case FUNCTION_INHERIT :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_SHANGDIAN :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
				CCLayer *shopLayer = (CCLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShopUI);
				if(shopLayer == NULL)
				{
					shopLayer = ShopUI::create();
					GameView::getInstance()->getMainUIScene()->addChild(shopLayer,0,kTagShopUI);
					shopLayer->ignoreAnchorPointForPosition(false);
					shopLayer->setAnchorPoint(ccp(0.5f, 0.5f));
					shopLayer->setPosition(ccp(winSize.width/2, winSize.height/2));
				}
				break;
			}
		case FUNCTION_CANGKU :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
				CCLayer *storeHouseLayer = (CCLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStoreHouseUI);
				if(storeHouseLayer == NULL)
				{
					storeHouseLayer = StoreHouseUI::create();
					GameView::getInstance()->getMainUIScene()->addChild(storeHouseLayer,0,kTagStoreHouseUI);
					storeHouseLayer->ignoreAnchorPointForPosition(false);
					storeHouseLayer->setAnchorPoint(ccp(0.5f, 0.5f));
					storeHouseLayer->setPosition(ccp(winSize.width/2, winSize.height/2));
				}
				break;
			}
		case FUNCTION_XIULIZHUANGBEI :
			{
				//GameUtils::playGameSound(REPAIR_TRIGGER, 2, false);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				//GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				//this->closeAnim();
	
				break;
			}
		case FUNCTION_WUJIANGFUHUO :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				//GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				//this->closeAnim();

				break;
			}
		case FUNCTION_JISHOU :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_MY_JISHOU :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();

				int openlevel = 0;
				std::map<int,int>::const_iterator cIter;
				cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(13);
				if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[13];
				}

				if (selfLevel_ >= openlevel)
				{
					AuctionUi * auction =AuctionUi::create();
					auction->ignoreAnchorPointForPosition(false);
					auction->setAnchorPoint(ccp(0.5f,0.5f));
					auction->setPosition(ccp(winSize.width/2,winSize.height/2));
					GameView::getInstance()->getMainUIScene()->addChild(auction,0,kTagAuction);
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,auction);
				}else
				{
					char openLevelStr[100];
					const char *strings = StringDataManager::getString("auction_OfMoneyOpenLevel");
					sprintf(openLevelStr,strings,openlevel);
					GameView::getInstance()->showAlertDialog(openLevelStr);
				}

				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_CLOSE :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_XIANGQIAN :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_XILIAN :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_FENGYIN :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_TRANSFOR :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_FAMILYCREATE :
			{
				int openlevel = 0;
				std::map<int,int>::const_iterator cIter;
				cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(15);
				if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[15];
				}

				if (selfLevel_ < openlevel)
				{
					char openLevelStr[100];
					const char *strings = StringDataManager::getString("family_createFamilyEnter_level");
					sprintf(openLevelStr,strings,openlevel);
					GameView::getInstance()->showAlertDialog(openLevelStr);
				}else
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				}

				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();

				break;
			}
		case FUNCTION_FAMILYAPPLY :
			{
				int openlevel = 0;
				std::map<int,int>::const_iterator cIter;
				cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(16);
				if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[16];
				}

				if (selfLevel_ >= openlevel)
				{
					ApplyFamily * applyFamily =ApplyFamily::create();
					applyFamily->ignoreAnchorPointForPosition(false);
					applyFamily->setAnchorPoint(ccp(0.5f,0.5f));
					applyFamily->setPosition(ccp(winsize.width/2,winsize.height/2));
					applyFamily->setTag(kTagApplyFamilyUI);
					GameView::getInstance()->getMainUIScene()->addChild(applyFamily);

					std::vector<CGuildBase *>::iterator iter;
					for (iter=applyFamily->vectorGuildBase.begin();iter!=applyFamily->vectorGuildBase.end();iter++)
					{
						delete * iter;
					}
					applyFamily->vectorGuildBase.clear();
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1501,(void *)0);
				}else
				{
					char openLevelStr[100];
					const char *strings = StringDataManager::getString("family_ApplyFamilyEnter_level");
					sprintf(openLevelStr,strings,openlevel);
					GameView::getInstance()->showAlertDialog(openLevelStr);
				}

				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_DUIHUANMA :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTTON_SINGCOPY:
			{
				int openlevel = 0;
				std::map<int,int>::const_iterator cIter;
				cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(18);
				if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[18];
				}
				if (selfLevel_ >= openlevel)
				{
					if (this->mTutorialIndex == 1)
					{
						Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
						if(sc != NULL)
							sc->endCommand(this);
					}

					ChallengeRoundUi * challengeui = ChallengeRoundUi::create();
					challengeui->ignoreAnchorPointForPosition(false);
					challengeui->setAnchorPoint(ccp(0.5f,0.5f));
					challengeui->setPosition(ccp(winsize.width/2,winsize.height/2));
					challengeui->setTag(ktagChallengeRoundUi);
					GameView::getInstance()->getMainUIScene()->addChild(challengeui);
				}else
				{
					char openLevelStr[100];
					const char *strings = StringDataManager::getString("family_ApplyFamilyEnter_level");
					sprintf(openLevelStr,strings,openlevel);
					GameView::getInstance()->showAlertDialog(openLevelStr);
				}

				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
			}break;
		case FUNCTION_SHIJIEBOSS :
			{
				//开启等级
				int openlevel = 0;
				std::map<int,int>::const_iterator cIter;
				cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(19);
				if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[19];
				}

				if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
					this->closeAnim();
					CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
					CCLayer *worldBossUI = (CCLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagWorldBossUI);
					if(worldBossUI == NULL)
					{
						worldBossUI = WorldBossUI::create();
						GameView::getInstance()->getMainUIScene()->addChild(worldBossUI,0,kTagWorldBossUI);
						worldBossUI->ignoreAnchorPointForPosition(false);
						worldBossUI->setAnchorPoint(ccp(0.5f, 0.5f));
						worldBossUI->setPosition(ccp(winSize.width/2, winSize.height/2));

						GameMessageProcessor::sharedMsgProcessor()->sendReq(5301);
					}
				}
				else
				{
					std::string str_des = StringDataManager::getString("feature_will_be_open_function");
					char str_level[20];
					sprintf(str_level,"%d",openlevel);
					str_des.append(str_level);
					str_des.append(StringDataManager::getString("feature_will_be_open_open"));
					GameView::getInstance()->showAlertDialog(str_des.c_str());
				}
			}break;
		case FUNCTION_XUANSHANGRENWU:
			{
				//开启等级
				int openlevel = 0;
				std::map<int,int>::const_iterator cIter;
				cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(28);
				if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[28];
				}

				if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
				{
					MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
					RewardTaskData::getInstance()->setTargetNpcId(this->getLockedNpcId());
					//refresh ui
					RewardTaskMainUI * rewardTaskMainUI = (RewardTaskMainUI*)mainscene->getChildByTag(kTagRewardTaskMainUI);
					if (!rewardTaskMainUI)
					{
						rewardTaskMainUI = RewardTaskMainUI::create();
						rewardTaskMainUI->ignoreAnchorPointForPosition(false);
						rewardTaskMainUI->setAnchorPoint(ccp(0.5f,0.5f));
						rewardTaskMainUI->setPosition(ccp(winsize.width/2,winsize.height/2));
						rewardTaskMainUI->setTag(kTagRewardTaskMainUI);
						mainscene->addChild(rewardTaskMainUI);
						GameMessageProcessor::sharedMsgProcessor()->sendReq(7014);
					}

					GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
					this->closeAnim();
				}
				else
				{
					std::string str_des = StringDataManager::getString("feature_will_be_open_function");
					char str_level[20];
					sprintf(str_level,"%d",openlevel);
					str_des.append(str_level);
					str_des.append(StringDataManager::getString("feature_will_be_open_open"));
					GameView::getInstance()->showAlertDialog(str_des.c_str());
				}
				break;
			}
		}
	}
}

void NpcTalkWindow::scrollViewDidScroll(CCScrollView* view )
{
}

void NpcTalkWindow::scrollViewDidZoom(CCScrollView* view )
{
}

bool NpcTalkWindow::isHasMission( int npcId )
{
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if (!scene)
		return false;

	FunctionNPC* fn = dynamic_cast<FunctionNPC*>(scene->getActor(npcId));
	if (!fn)
		return false;

	if (fn->npcMissionList.size()>0)
		return true;
	else
		return false;
}

int NpcTalkWindow::getLockedNpcId()
{
	return m_nLockedId;
}

void NpcTalkWindow::addCCTutorialIndicatorNpc( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction,int tutorialIndex )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,275,45,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+171,pos.y+155+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}

	mTutorialIndex = tutorialIndex;
}

void NpcTalkWindow::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void NpcTalkWindow::removeCCTutorialIndicator()
{
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
	mTutorialIndex = -1;
}




