#include "ChessBoardUI.h"

#include "SimpleAudioEngine.h"
#include "GameAudio.h"

#include "../../loadscene_state/LoadSceneState.h"
#include "../../utils/GameUtils.h"
#include "GameView.h"
#include "ChessBoardLayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "XMLOperate.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "../../gamescene_state/role/MyPlayer.h"

using namespace CocosDenshion;

#define TAG_CHESSBOARDlAYER 100
#define TAG_PARTICLE_FINISH 200
#define TAG_SPRITE_EXIT 300
#define TAG_TEACH_ANM 400
#define TAG_TEACH_HAND_ANM 401

#define COCOSTUDIO_PANEL_WIDTH 587
#define COCOSTUDIO_PANEL_HEIGHT 478



UIPanel * ChessBoardUI::s_pPanel = NULL;

ChessBoardUI::ChessBoardUI()
	:m_layer_chessBoardLayer(NULL)
	,m_imageView_frame(NULL)
	,m_base_layer(NULL)
	,m_labelBMF_stepValue(NULL)
	,m_pBtnFinish_enable(NULL)
	,m_pBtnFinish_disable(NULL)
	,m_pBtnSkip(NULL)
	,m_pBtnReset_enable(NULL)
	,m_pBtnReset_disable(NULL)
	,m_xOff_chessBoard(0.0f)
	,m_yOff_chessBoard(0.0f)
	,m_nDifficulty(1)
	,m_particle_finish(NULL)
	,m_sprite_arrowExit(NULL)
{

}

ChessBoardUI::~ChessBoardUI()
{
	XMLOperate::clearPassNum();
}

ChessBoardUI * ChessBoardUI::create()
{
	ChessBoardUI* pChessBoardUI = new ChessBoardUI();
	if (pChessBoardUI && pChessBoardUI->init())
	{
		pChessBoardUI->autorelease();
		return pChessBoardUI;
	}
	CC_SAFE_DELETE(pChessBoardUI);
	return NULL;
}

bool ChessBoardUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		m_base_layer = UILayer::create();
		m_base_layer->ignoreAnchorPointForPosition(false);
		m_base_layer->setAnchorPoint(ccp(0, 0));
		m_base_layer->setContentSize(CCSizeMake(800, 480));
		m_base_layer->setPosition(ccp(0, 0));
		this->addChild(m_base_layer);	

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winSize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		// 即用即加载，不再提前加载
		s_pPanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/huarongdao_1.json");
		s_pPanel->setAnchorPoint(ccp(0, 0));
		s_pPanel->getValidNode()->setContentSize(CCSizeMake(COCOSTUDIO_PANEL_WIDTH, COCOSTUDIO_PANEL_HEIGHT));
		s_pPanel->setPosition(ccp((winSize.width - COCOSTUDIO_PANEL_WIDTH) / 2, (winSize.height - COCOSTUDIO_PANEL_HEIGHT) / 2));
		s_pPanel->setScale(1.0f);
		s_pPanel->setTouchEnable(true);
		m_pUiLayer->addWidget(s_pPanel);

		initUI();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winSize);

		return true;
	}

	return false;
}

void ChessBoardUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void ChessBoardUI::onExit()
{
	UIScene::onExit();

	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

bool ChessBoardUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void ChessBoardUI::initUI()
{
	UIButton* pBtnClose = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnable(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addReleaseEvent(this, coco_releaseselector(ChessBoardUI::callBackBtnClolse));

	m_pBtnReset_enable = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_reset");
	m_pBtnReset_enable->setTouchEnable(true);
	m_pBtnReset_enable->setVisible(true);
	m_pBtnReset_enable->setPressedActionEnabled(true);
	m_pBtnReset_enable->addReleaseEvent(this, coco_releaseselector(ChessBoardUI::callBackBtnRest));

	m_pBtnReset_disable = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_reset_disable");
	m_pBtnReset_disable->setTouchEnable(false);
	m_pBtnReset_disable->setVisible(false);
	m_pBtnReset_disable->setPressedActionEnabled(false);

	m_pBtnFinish_enable = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_finish");
	m_pBtnFinish_enable->setTouchEnable(true);
	m_pBtnFinish_enable->setVisible(false);
	m_pBtnFinish_enable->setPressedActionEnabled(true);
	m_pBtnFinish_enable->addReleaseEvent(this, coco_releaseselector(ChessBoardUI::callBackBtnFinish));

	m_pBtnFinish_disable = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_finish_disable");
	m_pBtnFinish_disable->setTouchEnable(false);
	m_pBtnFinish_disable->setVisible(true);
	m_pBtnFinish_disable->setPressedActionEnabled(false);

	/*m_pBtnSkip = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_skip");
	m_pBtnSkip->setTouchEnable(true);
	m_pBtnSkip->setPressedActionEnabled(true);
	m_pBtnSkip->addReleaseEvent(this, coco_releaseselector(ChessBoardUI::callBackBtnSkip));*/

	UILabel * pLabel_info = (UILabel *)UIHelper::seekWidgetByName(s_pPanel,"Label_info");
	pLabel_info->setTextAreaSize(CCSizeMake(134,0));

	//finish button action
	CCActionInterval * action1 = CCScaleTo::create(1.0f,1.1f);
	CCActionInterval * action2 = CCScaleTo::create(1.0f,0.9);

	CCSequence * pAcitionSeq = CCSequence::create(action1,action2,NULL);
	m_pBtnFinish_enable->runAction(CCRepeatForever::create(pAcitionSeq));

	m_imageView_frame = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_frame");
	// x     m_imageView_frame->getPosition().x + s_pPanel->getPosition().x + 9.0f
	// y	 m_imageView_frame->getPosition().y + s_pPanel->getPosition().y + 5.5f

	// 暂且去掉该功能
	//m_pBtnSkip->setVisible(false);

	initLocationFrame();

	// stepValue（步数）
	m_labelBMF_stepValue = (UILabelBMFont *)UIHelper::seekWidgetByName(s_pPanel, "LabelBMFont_moveValue");

	// 教学动画添加（需判断是否需要加）
	std::string strPlayerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name(); 
	std::string strKey = strPlayerName;
	strKey.append("ChessBoard_needTeach");

	bool bIsNeedTeachAnm = CCUserDefault::sharedUserDefault()->getBoolForKey(strKey.c_str(), true);
	if (bIsNeedTeachAnm)
	{
		// 箭头
		std::string animFileName = "animation/texiao/jiemiantexiao/xunlu/xunlu.anm";
		CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
		m_pAnim->setReleaseWhenStop(true);
		m_pAnim->setPlayLoop(true);
		m_pAnim->setPosition(ccp(m_imageView_frame->getPosition().x + s_pPanel->getPosition().x + 9.0f + 250.0f, 
			m_imageView_frame->getPosition().y + s_pPanel->getPosition().y + 5.5f + 85.0f));
		m_pAnim->setTag(TAG_TEACH_ANM);
		this->addChild(m_pAnim);

		// 小手
		CCSprite * sprite_hand = CCSprite::create("animation/texiao/renwutexiao/shou/shou.png");
		sprite_hand->setAnchorPoint(ccp(0, 1));
		sprite_hand->ignoreAnchorPointForPosition(false);
		sprite_hand->setPosition(ccp(m_imageView_frame->getPosition().x + s_pPanel->getPosition().x + 9.0f + 185.0f,
			m_imageView_frame->getPosition().y + s_pPanel->getPosition().y + 5.5f + 90.0f));
		sprite_hand->setTag(TAG_TEACH_HAND_ANM);

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCDelayTime::create(0.5f),
			CCMoveTo::create(0.0f, ccp(m_imageView_frame->getPosition().x + s_pPanel->getPosition().x + 9.0f + 185.0f,
			m_imageView_frame->getPosition().y + s_pPanel->getPosition().y + 5.5f + 90.0f)),
			CCMoveBy::create(1.0f, ccp(100, 0)),
			CCDelayTime::create(0.5f),
			NULL);

		CCRepeatForever * pActionRepeat = CCRepeatForever::create(action);
		sprite_hand->runAction(pActionRepeat);

		this->addChild(sprite_hand);
	}

	this->scheduleUpdate();
}

void ChessBoardUI::callBackBtnClolse( CCObject * obj )
{
	this->closeAnim();
}

void ChessBoardUI::callBackBtnRest( CCObject* obj )
{
	ChessBoardLayer* pChessBoardLayer = (ChessBoardLayer *)m_base_layer->getChildByTag(TAG_CHESSBOARDlAYER);
	if (NULL != pChessBoardLayer)
	{
		m_base_layer->removeChildByTag(TAG_CHESSBOARDlAYER);
	}

	// 重新初始化棋盘
	initChessBoard(m_xOff_chessBoard, m_yOff_chessBoard);

	// 初始化 完成按钮的状态
	m_pBtnFinish_enable->setVisible(false);
	m_pBtnFinish_disable->setVisible(true);

	// 移除“完成”按钮的粒子效果
	this->removeFinishParticle();
}

void ChessBoardUI::callBackBtnFinish( CCObject* obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(7013, (void *)this->get_difficulty(), (void *)(com::future::threekingdoms::server::transport::protocol::HUA_RONG));

	this->closeAnim();
}

void ChessBoardUI::callBackBtnSkip( CCObject* obj )
{
	
}

void ChessBoardUI::initChessBoard( float xPos, float yPos )
{
	m_layer_chessBoardLayer = ChessBoardLayer::create();
	m_layer_chessBoardLayer->ignoreAnchorPointForPosition(false);
	m_layer_chessBoardLayer->setAnchorPoint(ccp(0, 0));
	m_layer_chessBoardLayer->setPosition(ccp(xPos, yPos));
	m_layer_chessBoardLayer->setTag(TAG_CHESSBOARDlAYER);
	m_layer_chessBoardLayer->set_difficulty(this->get_difficulty());


	float width = m_imageView_frame->getSize().width;
	float height = m_imageView_frame->getSize().height;

	// 此处的widthReal 和 heightReal 是 图片的实际大小（19.0f 和 13.0f是根据计算得出）
	float widthReal = width - 19.0f;
	float heigthReal = height - 13.0f;

	m_layer_chessBoardLayer->set_xPos(xPos);
	m_layer_chessBoardLayer->set_yPos(yPos);
	m_layer_chessBoardLayer->initChessBoardUI(xPos, yPos, widthReal, heigthReal);

	m_base_layer->addChild(m_layer_chessBoardLayer);

	// 初始化出口图片
	initExitSprite();
}

void ChessBoardUI::update( float dt )
{
	int nStepMoveValue = m_layer_chessBoardLayer->get_stepMove();
	char charStep[20];
	sprintf(charStep, "%d", nStepMoveValue);

	m_labelBMF_stepValue->setText(charStep);

	// 判断是否去掉教学动画，如果有的话
	CCNode* pNode_tecah = this->getChildByTag(TAG_TEACH_ANM);
	if (NULL != pNode_tecah && nStepMoveValue != 0)
	{
		pNode_tecah->removeFromParent();

		CCNode * pNode_tecahHand = this->getChildByTag(TAG_TEACH_HAND_ANM);
		if (NULL != pNode_tecahHand)
		{
			pNode_tecahHand->removeFromParent();
		}

		std::string strPlayerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name(); 
		std::string strKey = strPlayerName;
		strKey.append("ChessBoard_needTeach");

		CCUserDefault::sharedUserDefault()->setBoolForKey(strKey.c_str(), false);
	}
}

void ChessBoardUI::setFinishBtnEnable()
{
	m_pBtnFinish_enable->setVisible(true);

	m_pBtnFinish_disable->setVisible(false);
}

void ChessBoardUI::setResetBtnDisable()
{
	m_pBtnReset_disable->setVisible(true);

	m_pBtnReset_enable->setVisible(false);
}


void ChessBoardUI::initLocationFrame()
{
	/*if (!m_pBtnSkip->isVisible())
	{
	m_pBtnReset_enable->setPosition(ccp(m_pBtnReset_enable->getPosition().x, m_pBtnReset_enable->getPosition().y + 20));
	m_pBtnReset_disable->setPosition(ccp(m_pBtnReset_disable->getPosition().x, m_pBtnReset_disable->getPosition().y + 20));

	m_pBtnFinish_enable->setPosition(ccp(m_pBtnFinish_enable->getPosition().x, m_pBtnFinish_enable->getPosition().y + 10));
	m_pBtnFinish_disable->setPosition(ccp(m_pBtnFinish_disable->getPosition().x, m_pBtnFinish_disable->getPosition().y + 10));
	}*/
}

void ChessBoardUI::set_difficulty( int nDifficulty )
{
	this->m_nDifficulty = nDifficulty;
}

int ChessBoardUI::get_difficulty()
{
	return this->m_nDifficulty;
}

void ChessBoardUI::initFinishParticle()
{
	CCTutorialParticle* pFinishParticle = (CCTutorialParticle*)this->getChildByTag(TAG_PARTICLE_FINISH);
	if (NULL == pFinishParticle)
	{
		float xPos = m_pBtnFinish_enable->getPosition().x + s_pPanel->getPosition().x;
		float yPos = m_pBtnFinish_enable->getPosition().y + s_pPanel->getPosition().y - 20;

		m_particle_finish = CCTutorialParticle::create("tuowei0.plist", 90, m_pBtnFinish_enable->getContentSize().height);
		m_particle_finish->setPosition(ccp(xPos, yPos));
		m_particle_finish->setAnchorPoint(ccp(0.5f, 0.5f));
		m_particle_finish->setTag(TAG_PARTICLE_FINISH);

		m_base_layer->addChild(m_particle_finish);
	}
}

void ChessBoardUI::removeFinishParticle()
{
	CCTutorialParticle* pFinishParticle = (CCTutorialParticle*)m_base_layer->getChildByTag(TAG_PARTICLE_FINISH);
	if (NULL != pFinishParticle)
	{
		m_base_layer->removeChild(m_particle_finish);
	}
}

void ChessBoardUI::initExitSprite()
{
	CCSprite * tmpSprite = (CCSprite*)m_base_layer->getChildByTag(TAG_SPRITE_EXIT);
	if (NULL == tmpSprite)
	{
		m_sprite_arrowExit = CCSprite::create("res_ui/jiantou_b.png");
		m_sprite_arrowExit->setAnchorPoint(ccp(0, 0));
		m_sprite_arrowExit->ignoreAnchorPointForPosition(false);
		m_sprite_arrowExit->setTag(TAG_SPRITE_EXIT);

		float xPos = m_xOff_chessBoard;
		float yPos = m_yOff_chessBoard;

		float width = m_imageView_frame->getSize().width;
		float height = m_imageView_frame->getSize().height;

		// 此处的widthReal 和 heightReal 是 图片的实际大小（19.0f 和 13.0f是根据计算得出）
		float widthReal = width - 19.0f;
		float heigthReal = height - 13.0f;

		float xFinalPos = xPos + (widthReal - m_sprite_arrowExit->getContentSize().width) / 2;
		//float yFinalPos = yPos - m_sprite_arrowExit->getContentSize().height - 5;
		float yFinalPos = yPos - m_sprite_arrowExit->getContentSize().height - 1;

		m_sprite_arrowExit->setPosition(ccp(xFinalPos, yFinalPos));

		m_base_layer->addChild(m_sprite_arrowExit);

		// 初始化 出口图片的动画效果
		//initExitSpriteAction();
	}
}

void ChessBoardUI::initExitSpriteAction()
{
	// 上下动的 效果
	CCMoveBy * pActionMoveByDown = CCMoveBy::create(1.0f, ccp(0, 8));
	CCMoveBy * pActionMoveByUp = CCMoveBy::create(1.0f, ccp(0, -8)); 
	CCEaseSineInOut * pActionExpoIn = CCEaseSineInOut::create(pActionMoveByDown);
	CCEaseSineInOut * pActionExpoOut = CCEaseSineInOut::create(pActionMoveByUp);
	CCSequence * pActionSequence = CCSequence::createWithTwoActions(pActionExpoIn, pActionExpoOut);
	CCRepeatForever * pActionRepeat = CCRepeatForever::create(pActionSequence);
	m_sprite_arrowExit->runAction(pActionRepeat);
}

void ChessBoardUI::removeExitSprite()
{
	m_sprite_arrowExit->removeFromParent();
}

void ChessBoardUI::initChessBoardData()
{
	// 相对棋盘的偏移量（8.5f 和 5.5f 是 根据cocostudio的UI尝试算出来的）
	m_xOff_chessBoard = m_imageView_frame->getPosition().x + s_pPanel->getPosition().x + 9.0f;
	m_yOff_chessBoard = m_imageView_frame->getPosition().y + s_pPanel->getPosition().y + 5.5f;

	initChessBoard(m_xOff_chessBoard, m_yOff_chessBoard);
}





