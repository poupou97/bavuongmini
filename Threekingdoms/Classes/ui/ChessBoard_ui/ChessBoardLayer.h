#ifndef _UI_CHESSBOARD_CHESSBOARDLAYER_H_
#define _UI_CHESSBOARD_CHESSBOARDLAYER_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ChessBlockItem;

class ChessBoardLayer :
	public cocos2d::CCLayer
{
public:
	ChessBoardLayer(void);
	~ChessBoardLayer(void);

	enum
	{
		DIRECTION_UP = 1,
		DIRECTION_DOWN,
		DIRECTION_LEFT,
		DIRECTION_RIGHT,
		DIRECTION_NONE,
	};
public:
	static ChessBoardLayer * s_chessBoardLayer;
	static ChessBoardLayer * getInstance();

public:
	static ChessBoardLayer* create();

	virtual bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

private:
	CCPoint m_point_begin;
	CCPoint m_point_end; 

	float m_width_cell;
	float m_height_cell;

	float m_width_chessBoard;
	float m_height_chessBoard;

	int m_state_direction;

	float m_xPos;
	float m_yPos;

	int m_nStepMove;														// 步数
	int m_nDifficulty;														// 难易程度（1：简单；2：中等；3：困难）
	
	bool m_bIsWin;

public:
	float getCellWidth();
	float getCellHeight();

	float getChessBoardWidth();
	float getChessBoardHeight();

	void set_xPos(float xPos);
	float get_xPos();

	void set_yPos(float yPos);
	float get_yPos();

	void set_stepMove(int nStep);
	int get_stepMove();

	int get_row(CCPoint point);																			// 通过点击的point获取所在的row
	int get_column(CCPoint point);																		// 通过点击的point获取所在的column

	void set_difficulty(int nDifficulty);
	int get_difficulty();

	void set_isWin(bool bIsWin);
	bool get_isWin();
	
	void initDataFormXML(std::vector<ChessBlockItem *> vector);
	void initChessBoardUI(float xPos, float yPos, float boardWidth, float boardHeight);

	
	int m_array[5][4];

private:
	
	void initChessBoard(const char * strFileName);
	void initChessBoard();

	void initArrayEmtpy();

	void selectConfigXML(int nDifficulty);
};

#endif // _UI_CHESSBOARD_CHESSBOARDUI_H_