#include "EquipmentItem.h"
#include "GameView.h"
#include "AppMacros.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "PackageScene.h"
#include "../../messageclient/element/CEquipment.h"
#include "GoodsItemInfoBase.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "EquipmentItemInfos.h"
#include "../../utils/StaticDataManager.h"

#define  MengBan_Red_Name "mengban_red"

EquipmentItem::EquipmentItem() :
index(0)
{
}


EquipmentItem::~EquipmentItem()
{
	delete curEquipment;
}

EquipmentItem * EquipmentItem::create(CEquipment* equipment)
{
	EquipmentItem *widget = new EquipmentItem();
	if (widget && widget->init(equipment))
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}

bool EquipmentItem::init(CEquipment* equipment)
{
	if (UIWidget::init())
	{
	curEquipment = new CEquipment();
	curEquipment->CopyFrom(*equipment);

	if (equipment->has_goods() == true)
	{
		/*********************判断装备的颜色***************************/
		std::string frameColorPath;
		if (equipment->goods().equipmentdetail().quality() == 1)
		{
			frameColorPath = EquipWhiteFramePath;
		}
		else if (equipment->goods().equipmentdetail().quality() == 2)
		{
			frameColorPath = EquipGreenFramePath;
		}
		else if (equipment->goods().equipmentdetail().quality() == 3)
		{
			frameColorPath = EquipBlueFramePath;
		}
		else if (equipment->goods().equipmentdetail().quality() == 4)
		{
			frameColorPath = EquipPurpleFramePath;
		}
		else if (equipment->goods().equipmentdetail().quality() == 5)
		{
			frameColorPath = EquipOrangeFramePath;
		}
		else
		{
			frameColorPath = EquipVoidFramePath;
		}

		UIButton *Btn_goodsItemFrame = UIButton::create();
		Btn_goodsItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_goodsItemFrame->setTouchEnable(true);
		Btn_goodsItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_goodsItemFrame->setPosition(ccp(Btn_goodsItemFrame->getContentSize().width/2,Btn_goodsItemFrame->getContentSize().height/2));
		Btn_goodsItemFrame->setPressedActionEnabled(true);
		Btn_goodsItemFrame->addReleaseEvent(this,coco_releaseselector(EquipmentItem::EquipmentItemEvent));
		this->addChild(Btn_goodsItemFrame);

		UIImageView * uiiImageView_goodsItem = UIImageView::create();
		std::string iconPath = "res_ui/props_icon/";
		iconPath.append(equipment->goods().icon().c_str());
		iconPath.append(".png");
		uiiImageView_goodsItem->setTexture(iconPath.c_str());
		uiiImageView_goodsItem->setScale(0.9f);
		uiiImageView_goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		uiiImageView_goodsItem->setPosition(ccp(0,0));
		Btn_goodsItemFrame->addChild(uiiImageView_goodsItem);

		if (equipment->goods().binding() == 1)
		{
			UIImageView *ImageView_bound = UIImageView::create();
			ImageView_bound->setTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(ccp(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(ccp(7-Btn_goodsItemFrame->getContentSize().width/2,56-Btn_goodsItemFrame->getContentSize().height/2));
			Btn_goodsItemFrame->addChild(ImageView_bound);
		}

		//如果是装备
		if (equipment->goods().has_equipmentdetail())
		{
			if (equipment->goods().equipmentdetail().gradelevel() > 0)  //精练等级
			{
				std::string ss_gradeLevel = "+";
				char str_gradeLevel [10];
				sprintf(str_gradeLevel,"%d",equipment->goods().equipmentdetail().gradelevel());
				ss_gradeLevel.append(str_gradeLevel);
				UILabel *Lable_gradeLevel = UILabel::create();
				Lable_gradeLevel->setText(ss_gradeLevel.c_str());
				Lable_gradeLevel->setFontName(APP_FONT_NAME);
				Lable_gradeLevel->setFontSize(13);
				Lable_gradeLevel->setAnchorPoint(ccp(1.0f,1.0f));
				Lable_gradeLevel->setPosition(ccp(Btn_goodsItemFrame->getContentSize().width/2-8,Btn_goodsItemFrame->getContentSize().height/2-6));
				Lable_gradeLevel->setName("Lable_gradeLevel");
				Btn_goodsItemFrame->addChild(Lable_gradeLevel);
			}

			if (equipment->goods().equipmentdetail().starlevel() > 0)  //星级
			{
				char str_starLevel [10];
				sprintf(str_starLevel,"%d",equipment->goods().equipmentdetail().starlevel());
				UILabel *Lable_starLevel = UILabel::create();
				Lable_starLevel->setText(str_starLevel);
				Lable_starLevel->setFontName(APP_FONT_NAME);
				Lable_starLevel->setFontSize(13);
				Lable_starLevel->setAnchorPoint(ccp(0,0));
				Lable_starLevel->setPosition(ccp(7-Btn_goodsItemFrame->getContentSize().width/2,5-Btn_goodsItemFrame->getContentSize().height/2));
				Lable_starLevel->setName("Lable_starLevel");
				Btn_goodsItemFrame->addChild(Lable_starLevel);

				UIImageView *ImageView_star = UIImageView::create();
				ImageView_star->setTexture("res_ui/star_on.png");
				ImageView_star->setAnchorPoint(ccp(0,0));
				ImageView_star->setPosition(ccp(Lable_starLevel->getContentSize().width,2));
				ImageView_star->setVisible(true);
				ImageView_star->setScale(0.5f);
				Lable_starLevel->addChild(ImageView_star);
			}
		}


		if (equipment->goods().equipmentdetail().durable()> 10)
		{
			this->setRed(false);
		}
		else
		{
			this->setRed(true);
		}

		this->setAnchorPoint(CCPointZero);
		this->setSize(CCSizeMake(63,63));
		this->setWidgetZOrder(100);
		switch(equipment->part())
		{
		case 0 :
			this->setPosition(ccp(311, 342));    //头盔
			break;
		case 1 :
			this->setPosition(ccp(284, 144));  
			break;
		case 2 :
			this->setPosition(ccp(67, 273));   //衣服
			break;
		case 3 :
			this->setPosition(ccp(311, 205));    // 腰
			break;
		case 4 :
			this->setPosition(ccp(67, 342));    //武器
			break;
		case 5 :
			this->setPosition(ccp(284, 79));
			break;
		case 6 :
			this->setPosition(ccp(29, 79));
			break;
		case 7 :
			this->setPosition(ccp(67, 205));    //戒指
			break;
		case 8 :
			this->setPosition(ccp(29, 144));   
			break;
		case 9 :
			this->setPosition(ccp(311, 273));    //鞋
			break;
		case 10 :
			this->setPosition(ccp(29, 79));
			break;
		}
	}
	else
	{
	}

	return true;
	}
	return false;
}

void EquipmentItem::EquipmentItemEvent( CCObject * pSender )
{
	CCScene* pScene = CCDirector::sharedDirector()->getRunningScene();
	UILayer * uiScene = (UILayer*)pScene->getChildren()->objectAtIndex(0);
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	PackageScene * packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	//packageScene->curEquipmentIndex = curEquipment->id();
	packageScene->curEquipment = curEquipment;
// 	GoodsInfo * goodsinfo =new GoodsInfo();
// 	goodsinfo->CopyFrom(curEquipment->goods());
	EquipmentItemInfos * equipmentItemInfos = EquipmentItemInfos::create(curEquipment,packageScene->selectActorId);
	equipmentItemInfos->ignoreAnchorPointForPosition(false);
	equipmentItemInfos->setAnchorPoint(ccp(0.5f,0.5f));
	//equipmentItemInfos->setPosition(ccp(winSize.width/2,winSize.height/2));
	equipmentItemInfos->setTag(kTagGoodsItemInfoBase);
	GameView::getInstance()->getMainUIScene()->addChild(equipmentItemInfos);
	//记录点击装备时此时的武将id
	equipmentItemInfos->pacTabviewSelectindex = packageScene->selectTabviewIndex;
/*	delete goodsinfo;*/
	
}


CCPoint EquipmentItem::autoGetPosition(CCObject * pSender)
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
	CCPoint cur_positon = ((EquipmentItem*)(((UIButton*)pSender)->getWidgetParent()))->getPosition();
	CCSize cur_size = CCSizeMake(63,63);
	if (cur_positon.x+cur_size.width+272+(winSize.width-designResolutionSize.width) < winSize.width)
	{
			//在右侧中间显示	
			int x= cur_positon.x+cur_size.width+(winSize.width-designResolutionSize.width)/2;
			int y = (winSize.height-360)/2;
			return ccp(x,y);
	}
	return ccp(winSize.width/2,winSize.height/2);
}

void EquipmentItem::setRed( bool isRed )
{
	if (this->getChildByName(MengBan_Red_Name))
	{
		this->getChildByName(MengBan_Red_Name)->removeFromParent();
	}

	if (isRed)
	{
		UIImageView * m_mengban_red = UIImageView::create();
		m_mengban_red->setTexture("res_ui/mengban_red55.png");
		m_mengban_red->setScale9Enable(true);
		m_mengban_red->setScale9Size(CCSizeMake(63,63));
		m_mengban_red->setCapInsets(CCRectMake(5,5,1,1));
		m_mengban_red->setName(MengBan_Red_Name);
		m_mengban_red->setAnchorPoint(ccp(0,0));
		m_mengban_red->setPosition(ccp(0,0));
		this->addChild(m_mengban_red);

		//add lable
		UILabel * l_des = UILabel::create();
		l_des->setText(StringDataManager::getString("packageitem_notAvailable"));
		l_des->setFontName(APP_FONT_NAME);
		l_des->setFontSize(18);
		l_des->setAnchorPoint(ccp(0.5f,0.5f));
		l_des->setPosition(ccp(m_mengban_red->getSize().width/2,m_mengban_red->getSize().height/2));
		m_mengban_red->addChild(l_des);
	}
	else
	{
	}
}
