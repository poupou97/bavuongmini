
#ifndef _BACKPACKSCENE_USEMISSIONPROPUI_H_
#define _BACKPACKSCENE_USEMISSIONPROPUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

/******
des : 使用接取任务道具后弹出的接任务面板
auto : yangjun 
date : 2014.08.22
*********/

USING_NS_CC;
USING_NS_CC_EXT;

class CMissionReward;

class UseMissionPropUI : public UIScene
{
public:

	UseMissionPropUI();
	~UseMissionPropUI();

	static UseMissionPropUI* create(int npcId,const char * des,CMissionReward * missionReward);
	bool init(int npcId,const char * des,CMissionReward * missionReward);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);


	std::string rewardIconName[5];
	std::string rewardLabelName[5];

	int getTargetNpcId();

public: 
	UIButton * Button_getMission;
	UIButton * Button_finishMission;
private:
	int targetNpcId;
	UIButton * Button_Handle;
	CCScrollView * m_scrollView;
	CCLabelTTF * m_describeLabel;
	void CloseEvent(CCObject * pSender);
	void GetMissionEvent(CCObject * pSender);
	void FinishMissionEvent(CCObject * pSender);

	UIImageView * reward_di_1;
	UIImageView * reward_di_2;
	UIImageView * reward_di_3;
	UIImageView * reward_di_4;
	UIImageView * reward_di_5;
};
#endif;

