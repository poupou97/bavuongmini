
#ifndef _BACKPACKSCENE_EQUIPMETNINFOS_H
#define _BACKPACKSCENE_EQUIPMETNINFOS_H

#include "../extensions/UIScene.h"
#include "GoodsItemInfoBase.h"

USING_NS_CC;
USING_NS_CC_EXT;

class PackageItemInfo;
class PackageScene;
class LoadSceneState;
class CEquipment;

class EquipmentItemInfos : public GoodsItemInfoBase
{
public:
	EquipmentItemInfos();
	~EquipmentItemInfos();
	
	static EquipmentItemInfos * create(CEquipment * equipment,long long generalId);
	bool init(CEquipment * equipment,long long generalId);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent); 
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void callBackRepair(CCObject * obj);
	void callBackPick(CCObject * obj);
	void callBackPickSure(CCObject * obj);
	void getOffEvent(CCObject * pSender); /*******����********/
	void lvUpEvent(CCObject * pSender);   /*******����********/
	void showEquipStrength(CCNode * pNode,void * equip);
	int pacTabviewSelectindex;
private:
	std::string buttonImagePath;
	CEquipment * generalEquip;
	//general id 
	long long m_generalId;
	struct assistEquipStruct
	{
		int source_;
		int  pob_;
		long long petid_;
		int index_;
		int playType;
	};
};
#endif;
