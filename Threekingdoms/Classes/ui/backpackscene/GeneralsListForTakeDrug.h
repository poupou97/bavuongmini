
#ifndef _BACKPACKSCENE_GENERALSLISTFORTAKEDRUG_H_
#define _BACKPACKSCENE_GENERALSLISTFORTAKEDRUG_H_

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class FolderInfo;

/////////////////////////////////
/**
 * 背包中点击武将药品的使用弹出的武将列表
 * @author yangjun
 * @version 0.1.0
 * @date 2014.1.23
 */
class GeneralsListForTakeDrug : public UIScene,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	GeneralsListForTakeDrug();
	~GeneralsListForTakeDrug();

	static GeneralsListForTakeDrug * create(FolderInfo * folderInfo);
	bool init(FolderInfo * folderInfo);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void showCountForGeneralExp(CCObject *pSender);
public: 
	int curPackageItemIndex;
	FolderInfo * curFolder;
	long long curRoleId;
};

/////////////////////////////////////////////////////////////
class CGeneralBaseMsg;

class GeneralsListForTakeDrugCell : public CCTableViewCell
{
public:
	GeneralsListForTakeDrugCell();
	~GeneralsListForTakeDrugCell();
	static GeneralsListForTakeDrugCell* create(CGeneralBaseMsg * generalBaseMsg);
	bool init(CGeneralBaseMsg * generalBaseMsg);

	CGeneralBaseMsg* getCurGeneralBaseMsg();

private:
	CGeneralBaseMsg * curGeneralBaseMsg; 
	int m_index;
};


#endif

