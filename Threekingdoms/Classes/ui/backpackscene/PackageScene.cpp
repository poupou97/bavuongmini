#include "PackageScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"
#include "GameView.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "PackageItem.h"
#include "EquipmentItem.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../extensions/UITab.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../generals_ui/GeneralsUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../../gamescene_state/role/GameActorAnimation.h"
#include "../../messageclient/element/CActiveRole.h"
#include "../goldstore_ui/GoldStoreUI.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "AppMacros.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CDrug.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"
#include "../../utils/GameUtils.h"
#include "../generals_ui/RecuriteActionItem.h"
#include "../../ui/storehouse_ui/StoreHouseUI.h"
#include "particle_nodes/CCParticleSystem.h"
#include "../../utils/GameConfig.h"
#include "../extensions/ShowSystemInfo.h"
#include "BattleAchievementUI.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "../../ui/extensions/RecruitGeneralCard.h"

using namespace CocosDenshion;

#define PROFESSION_MENGJIANG "res_ui/renwubeibao/mj.png"
#define PROFESSION_GUIMOU "res_ui/renwubeibao/gm.png"
#define PROFESSION_HAOJIE "res_ui/renwubeibao/hj.png"
#define PROFESSION_SHENSHE "res_ui/renwubeibao/ss.png"

#define  RoleAnimationTag 687
#define  kTag_generalLevelInfo 265;

enum{
	Equipment_Helmet,
	Equipment_Clothes,
	Equipment_Weapon,
	Equipment_Ornament,
	Equipment_Shoes,
	Equipment_Ring,
};

// UIPageViewTest
PackageScene::PackageScene():
selectActorId(0),
selectTabviewIndex(0)
{
	//coordinate for equipmentlist
	for (int m = 2;m>=0;m--)
	{
		for (int n = 1;n>=0;n--)
		{
			int _m = 2-m;
			int _n = 1-n;

			Coordinate temp;
			temp.x = 23+_n*243;
			temp.y = 1+m*122;

			Coordinate_equip.push_back(temp);
		}
	}
	//equipmentitem name 
	for (int i = 0; i<12; i++)
	{
		std::string equipBaseName = "equipment_";
		char num[4];
		int tmpID = i;
		sprintf(num, "0%d", tmpID);
		std::string number = num;
		equipBaseName.append(num);
		equipmentItem_name[i] = equipBaseName;
	}
}

PackageScene::~PackageScene()
{
}

PackageScene * PackageScene::create()
{
	PackageScene * widget = new PackageScene();
	if (widget && widget->init())
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}


void PackageScene::onEnter()
{
	GeneralsListBase::onEnter();

	CCFiniteTimeAction*  action = CCSequence::create(
		CCScaleTo::create(0.15f*1/2,1.1f),
		CCScaleTo::create(0.15f*1/3,1.0f),
		NULL);

	runAction(action);

	GameUtils::playGameSound(PACKAGE_OPEN, 2, false);
	//this->setTouchEnabled(true);
	//this->setContentSize(CCSizeMake(800,480));
}

void PackageScene::onExit()
{
	GeneralsListBase::onExit();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

bool PackageScene::init()
{
    if (GeneralsListBase::init())
    {
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		this->scheduleUpdate();

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(800, 480));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);
		
		//加载UI
		if(LoadSceneLayer::s_pPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::s_pPanel->removeFromParentAndCleanup(false);
		}

 		ppanel = LoadSceneLayer::s_pPanel;
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		const char * firstStr = StringDataManager::getString("UIName_ren");
		const char * secondStr = StringDataManager::getString("UIName_wu");
		const char * thirdStr = StringDataManager::getString("UIName_bei_1");
		const char * fourthStr = StringDataManager::getString("UIName_bao_1");
		CCArmature * atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,fourthStr);
		atmature->setPosition(ccp(30,240));
		u_layer->addChild(atmature);

		//关闭按钮
		Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->setPressedActionEnabled(true);
		Button_close->addPushDownEvent(this, coco_pushselector(PackageScene::closeBeganEvent));
		Button_close->addMoveEvent(this, coco_moveselector(PackageScene::closeMovedEvent));
		Button_close->addReleaseEvent(this, coco_releaseselector(PackageScene::closeEndedEvent));
		//整理按钮
		UIButton * Button_sort = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_zhengli");
		Button_sort->setTouchEnable(true);
		Button_sort->addReleaseEvent(this, coco_releaseselector(PackageScene::SortEvent));
		Button_sort->setPressedActionEnabled(true);

		UIButton * Button_goldStore = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_shangcheng");
		Button_goldStore->setTouchEnable(true);
		Button_goldStore->addReleaseEvent(this, coco_releaseselector(PackageScene::GoldStoreEvent));
		Button_goldStore->setPressedActionEnabled(true);

		UIButton * Button_remoteStoreHouse = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_remoteStoreHouse");
		Button_remoteStoreHouse->setTouchEnable(true);
		Button_remoteStoreHouse->addReleaseEvent(this, coco_releaseselector(PackageScene::RemoteStoreHouseEvent));
		Button_remoteStoreHouse->setPressedActionEnabled(true);
		//one key of all equip 
		UIButton * Button_allEquip = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_allEquip_temp");
		Button_allEquip->setTouchEnable(true);
		Button_allEquip->addReleaseEvent(this, coco_releaseselector(PackageScene::oneKeyAllEquip));
		Button_allEquip->setPressedActionEnabled(true);

		// vip开关控制
		//bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		//if (vipEnabled)
		//{
		//	Button_remoteStoreHouse->setVisible(true);
		//}
		//else
		//{
		//	Button_remoteStoreHouse->setVisible(false);
		//}

		// 战功奖励
		UIButton * Button_achievement = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_achievement");
		Button_achievement->setTouchEnable(true);
		Button_achievement->addReleaseEvent(this, coco_releaseselector(PackageScene::AchievementEvent));
		Button_achievement->setPressedActionEnabled(true);
		Button_achievement->setVisible(true);

		//称谓
		UIButton * Button_title = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_title");
		Button_title->setTouchEnable(true);
		Button_title->addReleaseEvent(this, coco_releaseselector(PackageScene::TitleEvent));
		Button_title->setPressedActionEnabled(true);
		Button_title->setVisible(false);

		//获得面板
		playerPanel = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_renwu");
		dataPanel = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_property");
		infoPanle = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_informaion");
		playerPanel->setVisible(true);
		dataPanel->setVisible(false);
		infoPanle->setVisible(false);

		ImageView_wuqi = (UIImageView*)UIHelper::seekWidgetByName(playerPanel, "LV4_wuqi");
		ImageView_toukui = (UIImageView*)UIHelper::seekWidgetByName(playerPanel, "LV4_toukui");
		ImageView_yifu = (UIImageView*)UIHelper::seekWidgetByName(playerPanel, "LV4_yifu");
		ImageView_xiezi = (UIImageView*)UIHelper::seekWidgetByName(playerPanel, "LV4_xiezi");
		ImageView_jiezhi = (UIImageView*)UIHelper::seekWidgetByName(playerPanel, "LV4_jiezhi");
		ImageView_peishi = (UIImageView*)UIHelper::seekWidgetByName(playerPanel, "LV4_peishi");
        
        UIScrollView* pScrollView_information = (UIScrollView*)UIHelper::seekWidgetByName(infoPanle, "ScrollView_information");
        pScrollView_information->setBounceEnabled(true);
        
        UIScrollView* pScrollView_property = (UIScrollView*)UIHelper::seekWidgetByName(dataPanel, "ScrollView_property");
        pScrollView_property->setBounceEnabled(true);

		UIButton * btn_firstDes = (UIButton*)UIHelper::seekWidgetByName(pScrollView_property, "Button_firstDes");
		btn_firstDes->setTouchEnable(true);
		btn_firstDes->addReleaseEvent(this, coco_releaseselector(PackageScene::FirstGradeDesEvent));
		UIButton * btn_secondtDes = (UIButton*)UIHelper::seekWidgetByName(pScrollView_property, "Button_secondDes");
		btn_secondtDes->setTouchEnable(true);
		btn_secondtDes->addReleaseEvent(this, coco_releaseselector(PackageScene::SecondGradeDesEvent));

		playerLayer = UILayer::create();
		u_layer->addChild(playerLayer);
		playerLayer->setVisible(true);

		//rola name
		Label_roleName = (UILabel *)UIHelper::seekWidgetByName(playerPanel,"Label_roleName");
        Label_roleName->setStrokeEnabled(false);
        Label_roleName->setColor(ccc3(255,255,255));   // must use white color to show the special simbol correctly, for example, sun
		Label_roleName->setText(GameView::getInstance()->myplayer->getActiveRole()->rolebase().name().c_str());
		//Label_roleName->setColor(ccc3(47,93,13));
		Label_roleName->setVisible(true);
		//general name
		l_generalName = CCRichLabel::createWithString("456456", CCSizeMake(100,20),NULL,NULL);
		l_generalName->setAnchorPoint(ccp(0.5f,0.5f));
		l_generalName->setPosition(ccp(220,387));
		l_generalName->setScale(0.9f);
		playerLayer->addChild(l_generalName);
		l_generalName->setVisible(false);

		// vip isopen
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (vipEnabled)
		{
			if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() > 0)
			{
				UIWidget *widget_VipInfo = MainScene::addVipInfoByLevelForWidget(GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel());
				playerLayer->addWidget(widget_VipInfo);
				widget_VipInfo->setName("widget_VipInfo");
				widget_VipInfo->setPosition(ccp(Label_roleName->getPosition().x+Label_roleName->getContentSize().width/2+15,Label_roleName->getPosition().y));
			}
		}

		imageView_country = (UIImageView *)UIHelper::seekWidgetByName(playerPanel,"ImageView_country");
		int countryId = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().country();
		if (countryId > 0 && countryId <6)
		{
			std::string countryIdName = "country_";
			char id[2];
			sprintf(id, "%d", countryId);
			countryIdName.append(id);
			std::string iconPathName = "res_ui/country_icon/";
			iconPathName.append(countryIdName);
			iconPathName.append(".png");
			imageView_country->setTexture(iconPathName.c_str());
			imageView_country->setPosition(ccp(Label_roleName->getPosition().x-Label_roleName->getContentSize().width/2-imageView_country->getContentSize().width/2,imageView_country->getPosition().y));
		}

		imageView_professionName = (UIImageView *)UIHelper::seekWidgetByName(playerPanel,"ImageView_jobvalue");

		l_roleLevel = (UILabel *)UIHelper::seekWidgetByName(playerPanel,"Label_roleLevel");
        l_roleLevel->setStrokeEnabled(false);
		l_fightPoint = (UILabelBMFont *)UIHelper::seekWidgetByName(playerPanel,"LabelBMFont_figntpoint_value");
        //l_fightPoint->setStrokeEnabled(false);
// 		l_allFightPoint = (UILabel *)UIHelper::seekWidgetByName(playerPanel,"LabelBMFont_allfigntpoint_value");
//         l_allFightPoint->setStrokeEnabled(false);
		//->setColor(ccc3(255,255,255));
		//l_allFightPoint->setVisible(false);
		/*
		CCParticleSystem* particleFire = CCParticleSystemQuad::create("animation/texiao/particledesigner/huo2.plist");
		particleFire->setPositionType(kCCPositionTypeFree);
		particleFire->setPosition(ccp(143,162));
		particleFire->setAnchorPoint(ccp(0.5f,0.5f));
		particleFire->setVisible(true);
		//particleFire->setLife(0.5f);
		particleFire->setLifeVar(0);
		particleFire->setScale(0.5f);
		particleFire->setScaleX(0.6f);
		playerLayer->addChild(particleFire);

		CCParticleSystem* particleFire1 = CCParticleSystemQuad::create("animation/texiao/particledesigner/huo2.plist");
		particleFire1->setPositionType(kCCPositionTypeFree);
		particleFire1->setPosition(ccp(147,165));
		particleFire1->setAnchorPoint(ccp(0.5f,0.5f));
		particleFire1->setVisible(true);
		//particleFire1->setLife(0.5f);
		particleFire1->setLifeVar(0);
		particleFire1->setScale(0.5f);
		particleFire1->setScaleX(0.6f);
		playerLayer->addChild(particleFire1);
		*/
		UILayer * u_tempLayer = UILayer::create();
		playerLayer->addChild(u_tempLayer);

		/*UILabelBMFont * l_allFPName = UILabelBMFont::create();
		l_allFPName->setText(StringDataManager::getString("rank_capacity"));
		l_allFPName->setFntFile("res_ui/font/ziti_1.fnt");
		l_allFPName->setAnchorPoint(ccp(0.5f,0.5f));
		l_allFPName->setPosition(ccp(185,167));
		l_allFPName->setScale(0.6f);
		u_tempLayer->addWidget(l_allFPName);*/
		UIImageView * Image_allFPName = UIImageView::create();
		Image_allFPName->setTexture("res_ui/renwubeibao/zi2.png");
		Image_allFPName->setAnchorPoint(ccp(0.5f,0.5f));
		Image_allFPName->setPosition(ccp(103,162));
		Image_allFPName->setScale(1.0f);
		u_tempLayer->addWidget(Image_allFPName);

		l_allFightPoint = UILabelBMFont::create();
		l_allFightPoint->setText("123456789");
		l_allFightPoint->setFntFile("res_ui/font/ziti_1.fnt");
		l_allFightPoint->setAnchorPoint(ccp(0.5f,0.5f));
		l_allFightPoint->setPosition(ccp(186,162));
		l_allFightPoint->setScale(0.6f);
		u_tempLayer->addWidget(l_allFightPoint);
// 
// 		UILabel * l_allFPName = UILabel::create();
// 		l_allFPName->setText(StringDataManager::getString("rank_capacity"));
// 		l_allFPName->setFontName(APP_FONT_NAME);
// 		l_allFPName->setFontSize(18);
// 		l_allFPName->setAnchorPoint(ccp(0.5f,0.5f));
// 		l_allFPName->setPosition(ccp(185,169));
// 		playerLayer->addWidget(l_allFPName);
// 
// 		UILabel * l_allFPValue = UILabel::create();
// 		l_allFPValue->setText("123456789");
// 		l_allFPValue->setFontName(APP_FONT_NAME);
// 		l_allFPValue->setFontSize(18);
// 		l_allFPValue->setAnchorPoint(l_allFightPoint->getAnchorPoint());
// 		l_allFPValue->setPosition(l_allFightPoint->getPosition());
// 		playerLayer->addWidget(l_allFPValue);
		
		l_nameValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_nameValue");
        l_nameValue->setColor(ccc3(255,255,255));
        l_nameValue->setStrokeEnabled(false);
		l_levelValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_lvValue");
        l_levelValue->setStrokeEnabled(false);
		l_powerValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_powerValue");
        l_powerValue->setStrokeEnabled(false);
		l_aglieValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_agileValue");  //敏捷
        l_aglieValue->setStrokeEnabled(false);
		l_intelligenceValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_intelligenceValue");  //智力
        l_intelligenceValue->setStrokeEnabled(false);
		l_focusValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_focusValue");  //专注
        l_focusValue->setStrokeEnabled(false);
		l_curHpValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_HpMin");
        l_curHpValue->setStrokeEnabled(true);
        UILabel* lGang1 = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_gang1");
        lGang1->setStrokeEnabled(true);
		l_allHpValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_HpMax");
        l_allHpValue->setStrokeEnabled(true);
		imageView_hp = (UIImageView *)UIHelper::seekWidgetByName(dataPanel,"ImageView_hp");
		l_curMpValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_MpMin");
        l_curMpValue->setStrokeEnabled(true);
        UILabel* lGang2 = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_gang2");
        lGang2->setStrokeEnabled(true);
		l_allMpValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_Mpmax");
        l_allMpValue->setStrokeEnabled(true);
		imageView_mp = (UIImageView *)UIHelper::seekWidgetByName(dataPanel,"ImageView_mp");
		l_phyAttackValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_PhysicalAttackValue");
        l_phyAttackValue->setStrokeEnabled(false);
		l_magicAttackValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_MagicAttackValue");
        l_magicAttackValue->setStrokeEnabled(false);
		l_phyDenValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_Physicaldefense_Digital");
        l_phyDenValue->setStrokeEnabled(false);
		l_magicDenValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_power_physical_defense");
        l_magicDenValue->setStrokeEnabled(false);
		l_hitValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_hitValue");  //命中
        l_hitValue->setStrokeEnabled(false);
		l_dodgeValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_dodgeValue"); //闪避
        l_dodgeValue->setStrokeEnabled(false);
		l_critValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_critValue"); //暴击
        l_critValue->setStrokeEnabled(false);
		l_critDamageValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_CritDamageValue"); //暴击伤害
        l_critDamageValue->setStrokeEnabled(false);
		l_atkSpeedValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_AttackSpeedValue");
        l_atkSpeedValue->setStrokeEnabled(false);
		l_moveSpeedValue = (UILabel *)UIHelper::seekWidgetByName(dataPanel,"Label_SpeedValue");
        l_moveSpeedValue->setStrokeEnabled(false);

		l_roleNameValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_roleNameValue"); ///
        l_roleNameValue->setColor(ccc3(255,255,255));
        l_roleNameValue->setStrokeEnabled(false);
		l_roleLvValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_roleLvValue");///
        l_roleLvValue->setStrokeEnabled(false);
		l_jobValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_jobValue");
        l_jobValue->setStrokeEnabled(false);
		l_genderValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_genderValue");
        l_genderValue->setStrokeEnabled(false);
		l_countryValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_countryValue");
        l_countryValue->setStrokeEnabled(false);
		l_titleValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_titileValue");
        l_titleValue->setStrokeEnabled(false);
		l_factionValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_factionValue");
        l_factionValue->setStrokeEnabled(false);
		l_honorValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_officialValue");
        l_honorValue->setStrokeEnabled(false);
		l_familyValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_familyValue");
        l_familyValue->setStrokeEnabled(false);
		l_battleAchValue = (UILabel *)UIHelper::seekWidgetByName(ppanel, "Label_zhangongValue");
		l_battleAchValue->setStrokeEnabled(false);
		//l_experienceValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_experienceValue"); 
		l_physicalValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_experienceValue");
        l_physicalValue->setStrokeEnabled(false);
		l_expValue = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_exp_caps");
        l_expValue->setStrokeEnabled(true);
		imageView_exp = (UIImageView * )UIHelper::seekWidgetByName(ppanel,"ImageView_exp_schedule");

		ReloadProperty();
		ReloadInfo();
			
		//jinbi
		int m_goldValue = GameView::getInstance()->getPlayerGold();
		char GoldStr_[20];
		sprintf(GoldStr_,"%d",m_goldValue);
		Label_gold = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_gold");
		Label_gold->setText(GoldStr_);
		//yuanbao
		int m_goldIngot =  GameView::getInstance()->getPlayerGoldIngot();
		char GoldIngotStr_[20];
		sprintf(GoldIngotStr_,"%d",m_goldIngot);
		Label_goldIngot = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_goldIngold");
		Label_goldIngot->setText(GoldIngotStr_);
		//bangdingyuanbao
		Label_bingGoldIngot = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_binghGoldIngold");
		int m_bingGoldIngot =  GameView::getInstance()->getPlayerBindGoldIngot();
		char bingGoldIngotStr_[20];
		sprintf(bingGoldIngotStr_,"%d",m_bingGoldIngot);
		Label_bingGoldIngot->setText(bingGoldIngotStr_);

		//当前显示的Page
		currentPage = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"dian_on");

		const char * normalImage = "res_ui/tab_1_off.png";
		const char * selectImage = "res_ui/tab_1_on.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_1_on.png";
		//左侧TAB
		const char *str1 = StringDataManager::getString("packagescene_tab_role");
		char* p1 =const_cast<char*>(str1);
		const char *str2 = StringDataManager::getString("packagescene_tab_property");
		char* p2 =const_cast<char*>(str2);
		const char *str3 = StringDataManager::getString("packagescene_tab_infomation");
		char* p3 =const_cast<char*>(str3);

		char * leftItemName[] ={p1,p2,p3};
		leftTab = UITab::createWithText(3,normalImage,selectImage,finalImage,leftItemName,HORIZONTAL,9);
		leftTab->setAnchorPoint(ccp(0,0));
		leftTab->setPosition(ccp(58,415));
		leftTab->setHighLightImage((char * )highLightImage);
		leftTab->setDefaultPanelByIndex(0);
		this->ChangeLeftPanelByIndex(0);
		leftTab->addIndexChangedEvent(this,coco_indexchangedselector(PackageScene::LeftIndexChangedEvent));
		leftTab->setPressedActionEnabled(true);
		u_layer->addWidget(leftTab);

		// Create the page view
		reCreatePageVeiw();

// 		UILayer * u_pageFrameLayer = UILayer::create();
// 		u_pageFrameLayer->ignoreAnchorPointForPosition(false);
// 		u_pageFrameLayer->setAnchorPoint(ccp(0.5f,0.5f));
// 		u_pageFrameLayer->setContentSize(CCSizeMake(800, 480));
// 		u_pageFrameLayer->setPosition(ccp(winsize.width/2,winsize.height/2));
// 		this->addChild(u_pageFrameLayer);
// 		UIImageView * pageView_kuang = UIImageView::create();
// 		pageView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setScale9Enable(true);
// 		pageView_kuang->setScale9Size(CCSizeMake(363,323));
// 		pageView_kuang->setCapInsets(CCRectMake(30,30,1,1));
// 		pageView_kuang->setAnchorPoint(ccp(0,0));
// 		pageView_kuang->setPosition(ccp(390,90));
// 		u_pageFrameLayer->addWidget(pageView_kuang);
		
		MyPlayer* my = GameView::getInstance()->myplayer;
		// show the player's equipment

// 		std::vector<CEquipment *>::iterator iter_compair;
// 		for (iter_compair =GameView::getInstance()->compairEquipVector.begin();iter_compair!=GameView::getInstance()->compairEquipVector.end();iter_compair++)
// 		{
// 			delete * iter_compair;
// 		}
// 		GameView::getInstance()->compairEquipVector.clear();
// 
// 		for (int i=0;i<GameView::getInstance()->EquipListItem.size();i++)
// 		{
// 			CEquipment * equip_ =new CEquipment();
// 			equip_->CopyFrom( * GameView::getInstance()->EquipListItem.at(i));
// 			GameView::getInstance()->compairEquipVector.push_back(equip_);
// 		}
		RefreshEquipment(GameView::getInstance()->EquipListItem);
		// show the player's animation
		RefreshRoleAnimation((CActiveRole*)my->getActiveRole());
		// show the role's(container generals) Info
		RefreshRoleInfo((CActiveRole*)GameView::getInstance()->myplayer->getActiveRole(),GameView::getInstance()->myplayer->player->fightpoint());

		UIButton * testButton = UIButton::create();
		testButton->setTouchEnable(true);
		testButton->setTextures("res_ui/button_2_off.png","res_ui/button_2_off.png","");
		testButton->setAnchorPoint(CCPointZero);
		testButton->setPosition(ccp(200,200));
		testButton->addReleaseEvent(this,coco_releaseselector(PackageScene::closeBeganEvent));
		m_pUiLayer->addWidget(testButton);
		testButton->setVisible(false);

		this->generalsListStatus = HaveNext;
		//武将列表
		generalList_tableView = CCTableView::create(this,CCSizeMake(319,109));
		generalList_tableView->setSelectedEnable(true);
		generalList_tableView->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 26, 1, 1), ccp(0,0));
		generalList_tableView->setDirection(kCCScrollViewDirectionHorizontal);
		generalList_tableView->setAnchorPoint(ccp(0,0));
		generalList_tableView->setPosition(ccp(62,26));
		generalList_tableView->setContentOffset(ccp(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		generalList_tableView->setPressedActionEnabled(true);
		playerLayer->addChild(generalList_tableView);

		tempLayer = UILayer::create();
		tempLayer->ignoreAnchorPointForPosition(false);
		tempLayer->setAnchorPoint(ccp(0.5f,0.5f));
		tempLayer->setContentSize(CCSizeMake(800, 480));
		tempLayer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(tempLayer);
// 		UIImageView * roleList_kuang = UIImageView::create();
// 		roleList_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		roleList_kuang->setScale9Enable(true);
// 		roleList_kuang->setScale9Size(CCSizeMake(321,114));
// 		roleList_kuang->setCapInsets(CCRectMake(30,30,1,1));
// 		roleList_kuang->setAnchorPoint(ccp(0,0));
// 		roleList_kuang->setPosition(ccp(61,35));
// 		tempLayer->addWidget(roleList_kuang);
		UIImageView * imageView_leftFlag = UIImageView::create();
		imageView_leftFlag->setTexture("res_ui/shousuo.png");
		imageView_leftFlag->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_leftFlag->setPosition(ccp(76,85));
		imageView_leftFlag->setRotation(-135);
		imageView_leftFlag->setScale(0.5f);
		tempLayer->addWidget(imageView_leftFlag);
		UIImageView * imageView_rightFlag = UIImageView::create();
		imageView_rightFlag->setTexture("res_ui/shousuo.png");
		imageView_rightFlag->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_rightFlag->setPosition(ccp(367,85));
		imageView_rightFlag->setRotation(45);
		imageView_rightFlag->setScale(0.5f);
		tempLayer->addWidget(imageView_rightFlag);
		
		int openlevel = 0;//shor item equip is open level
		std::map<int,int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(26);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[26];
		}

		if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
		{
			UIButton * btn_mengban = UIButton::create();
			btn_mengban->setTouchEnable(true);
			btn_mengban->setTextures("res_ui/renwubeibao/mengban_green.png","res_ui/renwubeibao/mengban_green.png","");
			btn_mengban->setScale9Enable(true);
			btn_mengban->setScale9Size(CCSizeMake(320,108));
			btn_mengban->setAnchorPoint(ccp(0.5f,0.5f));
			btn_mengban->setPosition(ccp(221,85));
			btn_mengban->setOpacity(200);
			tempLayer->addWidget(btn_mengban);
			UIImageView * image_desFrame = UIImageView::create();
			image_desFrame->setTexture("res_ui/renwubeibao/zi_mengban.png");
 			image_desFrame->setScale9Enable(true);
 			image_desFrame->setScale9Size(CCSizeMake(240,29));
			image_desFrame->setCapInsets(CCRectMake(56,11,1,1));
			image_desFrame->setAnchorPoint(ccp(0.5f,0.5f));
			image_desFrame->setPosition(ccp(221,85));
			image_desFrame->setOpacity(200);
			tempLayer->addWidget(image_desFrame);
			std::string str_des = "";
			char s_lv [10];
			sprintf(s_lv,"%d",openlevel);
			str_des.append(s_lv);
			str_des.append(StringDataManager::getString("package_openGeneralEquipment"));
			UILabel * l_des = UILabel::create();
			l_des->setFontName(APP_FONT_NAME);
			l_des->setFontSize(20);
			l_des->setText(str_des.c_str());
			l_des->setAnchorPoint(ccp(0.5f,0.5f));
			l_des->setPosition(ccp(221,85));
			tempLayer->addWidget(l_des);
		}
		

		//refresh cd
		ShortcutLayer * shortCutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortCutLayer)
		{
			for (int i = 0;i<7;++i)  // there are total 7 shortcut slots
			{
				if (i < GameView::getInstance()->shortCutList.size())
				{
					CShortCut * c_shortcut = GameView::getInstance()->shortCutList.at(i);
					if (c_shortcut->storedtype() == 2)    // this is item
					{
						if(CDrug::isDrug(c_shortcut->skillpropid()))
						{
							if (shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100))
							{
								ShortcutSlot * shortcutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100);
								this->pacPageView->RefreshCD(c_shortcut->skillpropid());
							}
						}
					}
				}
			}
		}

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);

        return true;
    }
    return false;
}

void PackageScene::RefreshEquipment(std::vector<CEquipment *>& equipmentList)
{
	//delete old data
	for(int i = 0;i<12;++i)
	{
		if (playerPanel->getChildByName(this->equipmentItem_name[i].c_str()) != NULL)
		{
			playerPanel->getChildByName(this->equipmentItem_name[i].c_str())->removeFromParent();
			this->setPresentVoidEquipItem(i,true);
		}
	}
	//add new data
	int size = equipmentList.size();
	for (unsigned int i = 0; i < equipmentList.size(); i++)
	{
		CEquipment * tempEquip = equipmentList.at(i);
		EquipmentItem * equipment = EquipmentItem::create(tempEquip);
		equipment->setAnchorPoint(ccp(0,0));
		//equipment->setSize(CCSizeMake(90,109));
		equipment->setScale(1.0f);
		equipment->setName(this->equipmentItem_name[tempEquip->part()].c_str());
		playerPanel->addChild(equipment);

		this->setPresentVoidEquipItem(tempEquip->part(),false);
	}
}


void PackageScene::RefreshRoleAnimation( CActiveRole * activeRole )
{
	if (playerLayer->getChildByTag(RoleAnimationTag))
	{
		playerLayer->getChildByTag(RoleAnimationTag)->removeFromParent();
	}

	std::string roleFigureName = ActorUtils::getActorFigureName(*activeRole);
	
	std::string roleWeaponEffectForRefine = "";
	std::string roleWeaponEffectForStar = "";
	if (selectTabviewIndex == 0)
	{
		int equipSize = GameView::getInstance()->EquipListItem.size();
		for (int i=0;i<equipSize;i++)
		{
			if (GameView::getInstance()->EquipListItem.at(i)->part() == 4)
			{
				int pression_ = GameView::getInstance()->EquipListItem.at(i)->goods().equipmentdetail().profession();
				int refineLevel =  GameView::getInstance()->EquipListItem.at(i)->goods().equipmentdetail().gradelevel();
				int starLevel =  GameView::getInstance()->EquipListItem.at(i)->goods().equipmentdetail().starlevel();

				roleWeaponEffectForRefine = ActorUtils::getWeapEffectByRefineLevel(pression_,refineLevel);
				roleWeaponEffectForStar = ActorUtils::getWeapEffectByStarLevel(pression_, starLevel);
			}
		}
	}
	GameActorAnimation* pAnim = ActorUtils::createActorAnimation(roleFigureName.c_str(), activeRole->hand().c_str(),
		roleWeaponEffectForRefine.c_str(), roleWeaponEffectForStar.c_str());
	pAnim->setPosition(ccp(220,235));
	pAnim->setTag(RoleAnimationTag);
	playerLayer->addChild(pAnim);
}

GameActorAnimation* PackageScene::getRoleAnimation()
{
	if (playerLayer->getChildByTag(RoleAnimationTag))
	{
		return (GameActorAnimation*)playerLayer->getChildByTag(RoleAnimationTag);
	}
	else
	{
		return NULL;
	}
}

void PackageScene::RefreshRoleInfo(CActiveRole * activeRole,int fightpoint)
{
	if (GameView::getInstance()->myplayer->getRoleId() == activeRole->rolebase().roleid())
	{
		Label_roleName->setVisible(true);
		l_generalName->setVisible(false);

		imageView_country->setVisible(true);
		if (playerLayer->getWidgetByName("widget_VipInfo"))
		{
			playerLayer->getWidgetByName("widget_VipInfo")->setVisible(true);
		}
	}
	else
	{
		Label_roleName->setVisible(false);
		l_generalName->setVisible(true);
		//名字
		l_generalName->setString(activeRole->rolebase().name().c_str());

		imageView_country->setVisible(false);
		if (playerLayer->getWidgetByName("widget_VipInfo"))
		{
			playerLayer->getWidgetByName("widget_VipInfo")->setVisible(false);
		}
	}

	//职业
	switch(BasePlayer::getProfessionIdxByName(activeRole->profession().c_str()))
	{
	case PROFESSION_MJ_INDEX:
		{
			imageView_professionName->setTexture(PROFESSION_MENGJIANG);
		}
		break;
	case PROFESSION_GM_INDEX:
		{
			imageView_professionName->setTexture(PROFESSION_GUIMOU);
		}
		break;
	case PROFESSION_HJ_INDEX:
		{
			imageView_professionName->setTexture(PROFESSION_HAOJIE);
		}
		break;
	case PROFESSION_SS_INDEX:
		{
			imageView_professionName->setTexture(PROFESSION_SHENSHE);
		}
		break;
	default:
		{
			imageView_professionName->setTexture(PROFESSION_MENGJIANG);
		}
		break;
	}
	//等级
	std::string str_lv = "LV.";
	char s_level[20];
	sprintf(s_level,"%d",activeRole->level());
	str_lv.append(s_level);
	l_roleLevel->setText(str_lv.c_str());

 	char s_fightpoint[20];
	sprintf(s_fightpoint,"%d",fightpoint);
    //l_fightPoint->setStrokeEnabled(false);
	l_fightPoint->setText(s_fightpoint);
	//总战斗力
	int allFightPoint = GameView::getInstance()->myplayer->player->fightpoint();
	for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();++i)
	{
		allFightPoint += GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint();
	}
	char s_allFightPoint[20];
	sprintf(s_allFightPoint,"%d",allFightPoint);
    //l_allFightPoint->setStrokeEnabled(false);
	l_allFightPoint->setText(s_allFightPoint);
}

void PackageScene::closeBeganEvent(CCObject *pSender)
{
	CCLog("close Began");
}

void PackageScene::closeMovedEvent(CCObject *pSender)
{
	CCLog("clsoe Moved");
}
void PackageScene::closeEndedEvent(CCObject *pSender)
{
	CCLog("close ended");

	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	this->closeAnim();
}

void PackageScene::SortEvent(CCObject *pSender)
{
	CCLog("Sort ended");
	if (m_remainArrangePac > 0.f)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("package_arrange_canNot"));
		return;
	}

	m_remainArrangePac = 15.0f;
	//与服务器通讯，获取数据,整理背包
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1308,(void *)1);
	GameUtils::playGameSound(PACKAGE_SORT, 2, false);
}

void PackageScene::GoldStoreEvent(CCObject *pSender)
{
	CCLog("GoldStoreEvent");
	GoldStoreUI *goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
	if(goldStoreUI == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		goldStoreUI = GoldStoreUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI,0,kTagGoldStoreUI);

		goldStoreUI->ignoreAnchorPointForPosition(false);
		goldStoreUI->setAnchorPoint(ccp(0.5f, 0.5f));
		goldStoreUI->setPosition(ccp(winSize.width/2, winSize.height/2));
	}
}

void PackageScene::TitleEvent(CCObject *pSender)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
}

void PackageScene::AchievementEvent(CCObject *pSender)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
	BattleAchievementUI *tmpBattleAchUI = (BattleAchievementUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBattleAchievementUI);
	if (NULL == tmpBattleAchUI)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		BattleAchievementUI* battleAchUI = BattleAchievementUI::create();
		battleAchUI->ignoreAnchorPointForPosition(false);
		battleAchUI->setAnchorPoint(ccp(0.5f, 0.5f));
		battleAchUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		battleAchUI->setTag(kTagBattleAchievementUI);

		GameView::getInstance()->getMainUIScene()->addChild(battleAchUI);

		// 请求服务器(请求战功日常奖励Req5101)
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5101);
	}
}

void PackageScene::update(float dt)
{
	//金币
	char str_gold[20];
	sprintf(str_gold,"%d",GameView::getInstance()->getPlayerGold());
	Label_gold->setText(str_gold);
	//元宝
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	Label_goldIngot->setText(str_ingot);
	//绑定元宝
	char bingGoldIngotStr_[20];
	sprintf(bingGoldIngotStr_,"%d",GameView::getInstance()->getPlayerBindGoldIngot());
	Label_bingGoldIngot->setText(bingGoldIngotStr_);

	m_remainArrangePac -= 1.0f/60;
	if (m_remainArrangePac < 0)
		m_remainArrangePac = 0.f;
}


bool PackageScene::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	return this->resignFirstResponder(touch,this,false);
}

void PackageScene::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void PackageScene::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void PackageScene::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

 void PackageScene::menuCloseCallback(CCObject* pSender)
 {
	 //this->removeFromParent();

	 CCFiniteTimeAction*  action = CCSequence::create(
		 CCScaleTo::create(0.15f*1/3,1.1f),
		 CCScaleTo::create(0.15f*1/2,1.0f),
		 CCRemoveSelf::create(),
		 NULL);

	 runAction(action);
 }

 void PackageScene::PlayerEvent(CCObject* pSender)
 {
	 CCLog("player player ");
	 //this->playerItemImage->setScale(1.1f);
	 //this->dataItemImage->setScale(1.0f);

	 this->playerItemImage->setColor(ccc3(0, 255, 0));
	 this->dataItemImage->setColor(ccc3(255, 255, 255));

	 //this->setindex(0)
 }
 void PackageScene::DateEvent(CCObject* pSender)
 {
	CCLog("data data ");
	//this->playerItemImage->setScale(1.0f);
	//this->dataItemImage->setScale(1.1f);

	this->playerItemImage->setColor(ccc3(255, 255, 255));
	this->dataItemImage->setColor(ccc3(0, 255, 0));
 }

 void PackageScene::ChangeLeftPanelByIndex(int index)
 {
	switch(index)
	{
	case 0 :
			CCLog("00");
			playerPanel->setVisible(true);
			dataPanel->setVisible(false);
			break;   
	case 1 :
			CCLog("01");
			playerPanel->setVisible(false);
			dataPanel->setVisible(true);
			break;
	}
 }

 void PackageScene::LeftIndexChangedEvent(CCObject* pSender)
 {
	CCLog("UIPageViewTest");
	//CCLog("this.index = %d ",leftTab->getCurrentIndex());
	
	switch((int)leftTab->getCurrentIndex())
	{
	case 0 :
			CCLog("00");
			playerPanel->setVisible(true);
			playerLayer->setVisible(true);
			tempLayer->setVisible(true);
			dataPanel->setVisible(false);
			infoPanle->setVisible(false);
			break;   
	case 1 :
			CCLog("01");
			playerPanel->setVisible(false);
			playerLayer->setVisible(false);
			tempLayer->setVisible(false);
			dataPanel->setVisible(true);
			infoPanle->setVisible(false);
			ReloadProperty();
			break;
	case 2 :
			CCLog("02");
			playerPanel->setVisible(false);
			playerLayer->setVisible(false);
			tempLayer->setVisible(false);
			dataPanel->setVisible(false);
			infoPanle->setVisible(true);
			break;
	}
 }

  void PackageScene::CurrentPageViewChanged(CCObject* pSender)
  {
	  CCLog("currentIndex ==========  %d",(int)(pacPageView->getPageView()->getCurPageIndex()));
	  switch((int)(pacPageView->getPageView()->getCurPageIndex()))
	  {
	  case 0:
		  currentPage->setPosition(ccp(530,78));
		  break;
	  case 1:
		  currentPage->setPosition(ccp(551,78));
		  break;
	  case 2:
		  currentPage->setPosition(ccp(570,78));
		  break;
	  case 3:
		  currentPage->setPosition(ccp(589,78));
		  break;
	  case 4:
		  currentPage->setPosition(ccp(608,78));
		  break;
	  }
	  
  }

  void PackageScene::reCreatePageVeiw()
  {
	  pacPageView = GameView::getInstance()->pacPageView;
	  pacPageView->getPageView()->addPageTurningEvent(this,coco_PageView_PageTurning_selector(PackageScene::CurrentPageViewChanged));
	  u_layer->addChild(pacPageView);
	  pacPageView->setPosition(ccp(397,99));
	  pacPageView->setCurUITag(kTagBackpack);
	  pacPageView->setVisible(true);

	  CCSequence* seq = CCSequence::create(CCDelayTime::create(0.05f),CCCallFunc::create(this,callfunc_selector(PackageScene::PageScrollToDefault)),NULL);
	  pacPageView->runAction(seq);
	  //pacPageView->getPageView()->scrollToPage(0);
	  //CurrentPageViewChanged(pacPageView);

	  pacPageView->checkCDOnBegan();
  }

  void PackageScene::sdEvent( CCObject *pSender )
  {
		CCLOG("sdEvent ");
  }


  void PackageScene::ReloadProperty()
  {
	  GamePlayer * _player = GameView::getInstance()->myplayer->player; 
	  ActiveRole * activeRole = GameView::getInstance()->myplayer->getActiveRole();
	  //名字
	  l_nameValue->setText(activeRole->rolebase().name().c_str());
	  //等级
	  char s_level[20];
	  sprintf(s_level,"%d",activeRole->level());
	  l_levelValue->setText(s_level);

	  char s_strength[20];
	  sprintf(s_strength,"%d",_player->aptitude().strength());
	  l_powerValue->setText(s_strength);
	  //敏捷
	  char s_dexterity[20];
	  sprintf(s_dexterity,"%d",_player->aptitude().dexterity());
	  l_aglieValue->setText(s_dexterity);
	  //智力
	  char s_intelligence[20];
	  sprintf(s_intelligence,"%d",_player->aptitude().intelligence());
	  l_intelligenceValue->setText(s_intelligence);
	  //专注
	  char s_focus[20];
	  sprintf(s_focus,"%d",_player->aptitude().focus());
	  l_focusValue->setText(s_focus);

	  char s_hp[20];
	  sprintf(s_hp,"%d",activeRole->hp());
	  l_curHpValue->setText(s_hp);
	  char s_maxhp[20];
	  sprintf(s_maxhp,"%d",activeRole->maxhp());
	  l_allHpValue->setText(s_maxhp);

	  float blood_scale = (float)((activeRole->hp()*1.0f)/(activeRole->maxhp()*1.0f));
	  if (blood_scale > 1.0f)
		  blood_scale = 1.0f;

	  imageView_hp->setTextureRect(CCRectMake(0,0,114*blood_scale,12));
	  imageView_hp->setScaleX(blood_scale);

	  char s_mp[20];
	  sprintf(s_mp,"%d",activeRole->mp());
	  l_curMpValue->setText(s_mp);
	  char s_maxmp[20];
	  sprintf(s_maxmp,"%d",activeRole->maxmp());
	  l_allMpValue->setText(s_maxmp);
	  float magic_scale = (float)((activeRole->mp()*1.0f)/(activeRole->maxmp()*1.0f));
	  if (magic_scale > 1.0f)
		  magic_scale = 1.0f;

	  imageView_mp->setTextureRect(CCRectMake(0,0,114*magic_scale,12));
	  imageView_mp->setScaleX(magic_scale);

	  std::string phyAttackValue = "";
	  char s_minattack[20];
	  sprintf(s_minattack,"%d",_player->aptitude().minattack());
	  phyAttackValue.append(s_minattack);
	  phyAttackValue.append("-");
	  char s_maxattack[20];
	  sprintf(s_maxattack,"%d",_player->aptitude().maxattack());
	  phyAttackValue.append(s_maxattack);
	   l_phyAttackValue->setText(phyAttackValue.c_str());

	   std::string magicAttackValue = "";
	   char s_minmagicattack [20];
	   sprintf(s_minmagicattack,"%d",_player->aptitude().minmagicattack());
	   magicAttackValue.append(s_minmagicattack);
	   magicAttackValue.append("-");
	   char s_maxmagicattack[20];
	   sprintf(s_maxmagicattack,"%d",_player->aptitude().maxmagicattack());
	   magicAttackValue.append(s_maxmagicattack);
	   l_magicAttackValue->setText(magicAttackValue.c_str());

	   std::string phyDenValue = "";
	   char s_mindefend [20];
	   sprintf(s_mindefend,"%d",_player->aptitude().mindefend());
	   phyDenValue.append(s_mindefend);
	   phyDenValue.append("-");
	   char s_maxdefend[20];
	   sprintf(s_maxdefend,"%d",_player->aptitude().maxdefend());
	   phyDenValue.append(s_maxdefend);
	   l_phyDenValue->setText(phyDenValue.c_str());

	   std::string magicDenValue = "";
	   char s_minmagicdefend[20];
	   sprintf(s_minmagicdefend,"%d",_player->aptitude().minmagicdefend());
	   magicDenValue.append(s_minmagicdefend);
	   magicDenValue.append("-");
	   char s_maxmagicdefend[20];
	   sprintf(s_maxmagicdefend,"%d",_player->aptitude().maxmagicdefend());
	   magicDenValue.append(s_maxmagicdefend);
	   l_magicDenValue->setText(magicDenValue.c_str());

	  char s_hit[20];
	  sprintf(s_hit,"%d",_player->aptitude().hit());
	  l_hitValue->setText(s_hit); //命中
	  char s_dodge[20];
	  sprintf(s_dodge,"%d",_player->aptitude().dodge());
	  l_dodgeValue->setText(s_dodge); //闪避
	  char s_crit[20];
	  sprintf(s_crit,"%d",_player->aptitude().crit());
	  l_critValue->setText(s_crit); //暴击
	  char s_critdamage[20];
	  sprintf(s_critdamage,"%d",_player->aptitude().critdamage());
	  l_critDamageValue->setText(s_critdamage); //暴击伤害
	  char s_attackspeed[20];
	  sprintf(s_attackspeed,"%d",_player->aptitude().attackspeed());
	  l_atkSpeedValue->setText(s_attackspeed); //
	  char s_mspeed[20];
	  sprintf(s_mspeed,"%d",_player->aptitude().movespeed());
	  l_moveSpeedValue->setText(s_mspeed); //移动速度
  }

  void PackageScene::ReloadInfo()
  {
	  GamePlayer * _player = GameView::getInstance()->myplayer->player; 
	  ActiveRole * activeRole = GameView::getInstance()->myplayer->getActiveRole();

	  l_roleNameValue->setText(activeRole->rolebase().name().c_str());

	  char s_level[20];
	  sprintf(s_level,"%d",activeRole->level());
	  l_roleLvValue->setText(s_level);

	  l_jobValue->setText(activeRole->profession().c_str());

	  if (activeRole->rolebase().gender() == 1)
	  {
		  //man
		  l_genderValue->setText(StringDataManager::getString("role_gender_nan"));
	  }
	  else
	  {
		  //woman
		  l_genderValue->setText(StringDataManager::getString("role_gender_nv"));
	  }
	 
	  l_countryValue->setText(CMapInfo::getCountryName(activeRole->playerbaseinfo().country()));
	  l_titleValue->setText(activeRole->rolebase().title().c_str());
	  l_factionValue->setText(_player->factionname().c_str());
	
	  // 荣誉
	  long long longHonorPoint = _player->honorpoint();
	  char charHonorPoint[20];
	  sprintf(charHonorPoint, "%lld", longHonorPoint);
	  l_honorValue->setText(charHonorPoint);

	  // 家族
	  l_familyValue->setText( activeRole->playerbaseinfo().factionname().c_str());

	  // 战功
	  int nBattleAchievement = _player->pkpoint();
	  char charBattleAch[20];
	  sprintf(charBattleAch, "%d", nBattleAchievement);
	  l_battleAchValue->setText(charBattleAch);

	  std::string str_physicalValue = "";
	  char s_physicalValue[20];
	  sprintf(s_physicalValue,"%d",GameView::getInstance()->myplayer->getPhysicalValue());
	  str_physicalValue.append(s_physicalValue);
	  str_physicalValue.append("/");
	  char s_physicalCapacity[20];
	  sprintf(s_physicalCapacity,"%d",GameView::getInstance()->myplayer->getPhysicalCapacity());
	  str_physicalValue.append(s_physicalCapacity);
	  l_physicalValue->setText(str_physicalValue.c_str());
	  
	  

	  std::string expValue = "";
	  char s_experience[20];
	  sprintf(s_experience,"%d",_player->experience());
	  expValue.append(s_experience);
	  expValue.append("/");
	  char s_nextlevelexperience[20];
	  sprintf(s_nextlevelexperience,"%d",_player->nextlevelexperience());
	  expValue.append(s_nextlevelexperience);
	  l_expValue->setText(expValue.c_str());

	  float scaleValue = (_player->experience()*1.0f)/(_player->nextlevelexperience()*1.0f);
	  imageView_exp->setScaleX(scaleValue);
  }

  void PackageScene::scrollViewDidScroll( CCScrollView* view )
  {

  }

  void PackageScene::scrollViewDidZoom( CCScrollView* view )
  {

  }

  void PackageScene::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
  {
	  int i = cell->getIdx();
	  selectTabviewIndex = i;
	  // the same one, ignore
	  if(table->getSelectedCellIndex() == i)
		 return;

	  if (i == 0)
	  {
		  selectActorId = 0;
		  RefreshEquipment(GameView::getInstance()->EquipListItem);
		  RefreshRoleAnimation((CActiveRole*)GameView::getInstance()->myplayer->getActiveRole());
		  RefreshRoleInfo((CActiveRole*)GameView::getInstance()->myplayer->getActiveRole(),GameView::getInstance()->myplayer->player->fightpoint());
	  }
	  else
	  {
		  GeneralsSmallHeadCell * tempCell = dynamic_cast<GeneralsSmallHeadCell*>(cell);
		  if (tempCell != NULL)
		  {
			  selectActorId = tempCell->getCurGeneralBaseMsg()->id();
			  curGeneralBaseMsg = tempCell->getCurGeneralBaseMsg();

			  bool isExist = false;
			  for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();++i)
			  {
				  if (GameView::getInstance()->generalsInLineDetailList.at(i)->generalid() == curGeneralBaseMsg->id())
				  {
					  CActiveRole * _activerole = new CActiveRole();
					  _activerole->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->activerole());
					  RefreshRoleAnimation(_activerole);
					  RefreshRoleInfo(_activerole,GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint());
					  delete _activerole;

					  std::vector<CEquipment *> temp ;
					  for (int j = 0;j<GameView::getInstance()->generalsInLineDetailList.at(i)->equipments_size();++j)
					  {
						  CEquipment * temp_equipment = new CEquipment();
						  temp_equipment->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->equipments(j));
						  temp.push_back(temp_equipment);
					  }
					  //refresh Equipment
					  RefreshEquipment(temp);
					  //delete vector
					  std::vector<CEquipment*>::iterator iter;
					  for (iter = temp.begin(); iter != temp.end(); ++iter)
					  {
						  delete *iter;
					  }
					  temp.clear();
					  isExist = true;
					  break;  
				  }
			  }
			  if(!isExist)  
			  {
				  //请求详细信息
				  GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)curGeneralBaseMsg->id());
			  }
		  }
	  }
  }

  cocos2d::CCSize PackageScene::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
  {
		return CCSizeMake(81,103);
  }

  cocos2d::extension::CCTableViewCell* PackageScene::tableCellAtIndex( CCTableView *table, unsigned int idx )
  {
	  CCTableViewCell *cell = table->dequeueCell();   // this method must be called
	  
	  if (idx == 0)
	  {
		  cell = new CCTableViewCell();
		  cell->autorelease();
		  CCSprite *  smaillFrame = CCSprite::create("res_ui/generals_white.png");
		  smaillFrame->setAnchorPoint(ccp(0, 0));
		  smaillFrame->setPosition(ccp(0, 0));
		  smaillFrame->setScaleX(0.75f);
		  smaillFrame->setScaleY(0.7f);
		  cell->addChild(smaillFrame);
		  //列表中的头像图标
		  CCSprite * sprite_icon = CCSprite::create(BasePlayer::getBigHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str());
		  sprite_icon->setAnchorPoint(ccp(0.5f, 0.5f));
		  sprite_icon->setPosition(ccp(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), smaillFrame->getContentSize().height/2*smaillFrame->getScaleY()));
		  sprite_icon->setScale(0.75f);
		  cell->addChild(sprite_icon);
		  //
// 		  CCScale9Sprite * sprite_lvFrame = CCScale9Sprite::create("res_ui/zhezhao80.png");
// 		  sprite_lvFrame->setAnchorPoint(ccp(1.0f, 0));
// 		  sprite_lvFrame->setContentSize(CCSizeMake(34,13));
// 		  sprite_lvFrame->setPosition(ccp(smaillFrame->getContentSize().width*smaillFrame->getScaleX()-5, 21));
// 		  cell->addChild(sprite_lvFrame);
// 		  //等级
// 		  std::string _lv = "LV";
// 		  char s_level[5];
// 		  sprintf(s_level,"%d",GameView::getInstance()->myplayer->getActiveRole()->level());
// 		  _lv.append(s_level);
// 		  CCLabelTTF * label_lv = CCLabelTTF::create(_lv.c_str(),APP_FONT_NAME,13);
// 		  label_lv->setAnchorPoint(ccp(1.0f, 0));
// 		  label_lv->setPosition(ccp(smaillFrame->getContentSize().width*smaillFrame->getScaleX()-5, 19));
// 		  label_lv->enableStroke(ccc3(0, 0, 0), 2.0f);
// 		  cell->addChild(label_lv);
		  //武将名字黑底
		  CCScale9Sprite * sprite_nameFrame = CCScale9Sprite::create("res_ui/name_di3.png");
		  sprite_nameFrame->setCapInsets(CCRectMake(10,10,1,5));
		  sprite_nameFrame->setAnchorPoint(ccp(0.5f, 0));
		  sprite_nameFrame->setContentSize(CCSizeMake(71,13));
		  sprite_nameFrame->setPosition(ccp(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 8));
		  cell->addChild(sprite_nameFrame);
// 		  CCSprite * sprite_nameFrame = CCSprite::create("res_ui/name_di2.png");
// 		  sprite_nameFrame->setAnchorPoint(ccp(0.5f, 0));
// 		  sprite_nameFrame->setPosition(ccp(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 6));
// 		  sprite_nameFrame->setScaleX(1.1f);
// 		  cell->addChild(sprite_nameFrame);
		  //武将名字
		  const char * generalRoleName_ = StringDataManager::getString("generalList_RoleName");
		  CCLabelTTF * label_name = CCLabelTTF::create(generalRoleName_,APP_FONT_NAME,14);
		  label_name->setAnchorPoint(ccp(0.5f, 0));
		  label_name->setPosition(ccp(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 5));
		  //label_name->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		  label_name->setColor(ccc3(0,255,0));
		  cell->addChild(label_name);
		  //profession
		  std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(GameView::getInstance()->myplayer->getProfession());
		  CCSprite * sprite_profession = CCSprite::create(generalProfess_.c_str());
		  sprite_profession->setAnchorPoint(ccp(0.5f,0.5f));
		  sprite_profession->setPosition(ccp(62 ,
			  32));
		  sprite_profession->setScale(0.6f);
		  cell->addChild(sprite_profession);
	  }
	  else
	  {
		   cell = GeneralsSmallHeadCell::create(GameView::getInstance()->generalsInLineList.at(idx-1)); 
	  }
	 
// 	  if(idx == GameView::getInstance()->generalsInLineList.size()-3)
// 	  {
// 			if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
// 			{
// 					//req for next
// 					 GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 					 temp->page = this->s_mCurPage+1;
// 					 temp->pageSize = this->everyPageNum;
// 					 temp->type = 1;
// 					 GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 					 delete temp;
// 			 }
// 	  }
	  return cell;
  }

  unsigned int PackageScene::numberOfCellsInTableView( CCTableView *table )
  {
		 // return generalBaseMsgList.size()+1;
	   return GameView::getInstance()->generalsInLineList.size()+1;
  }


  void PackageScene::ReloadGeneralsTableView()
  {
	  CCPoint offset = this->generalList_tableView->getContentOffset();
	  this->generalList_tableView->reloadData();
  }

  void PackageScene::ReloadGeneralsTableViewWithoutChangeOffSet()
  {
	  CCPoint offset = this->generalList_tableView->getContentOffset();
	  this->generalList_tableView->reloadData();
	  this->generalList_tableView->setContentOffset(offset);
  }

  void PackageScene::registerScriptCommand( int scriptId )
  {
	  mTutorialScriptInstanceId = scriptId;
  }

  void PackageScene::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
  {
	  CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	  int _w = (winSize.width-800)/2;
	  int _h = (winSize.height - 480)/2;
// 	  CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	  tutorialIndicator->setPosition(ccp(pos.x-60+_w,pos.y-75+_h));
// 	  tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	  this->addChild(tutorialIndicator);

	  CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,40,40);
	  tutorialIndicator->setDrawNodePos(ccp(pos.x+_w,pos.y+_h));
	  tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	  MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	  if (mainScene)
	  {
		  mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	  }
  }

  void PackageScene::removeCCTutorialIndicator()
  {
// 	  CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
// 	  if(tutorialIndicator != NULL)
// 		  tutorialIndicator->removeFromParent();

	  MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	  if (mainScene)
	  {
		  CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		  if(teachingGuide != NULL)
			  teachingGuide->removeFromParent();
	  }
  }

  void PackageScene::RefreshGeneralLevelInTableView( long long generalId )
  {
	  for(int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	  {
		  if (generalId == GameView::getInstance()->generalsInLineList.at(i)->id())
		  {
			  GeneralsSmallHeadCell * cell = dynamic_cast<GeneralsSmallHeadCell*>(generalList_tableView->cellAtIndex(i+1));
			  if (!cell)
				  continue;

			  if (cell->getCurGeneralBaseMsg()->id() == generalId)
			  {
				  CCLabelTTF * l_lv = dynamic_cast<CCLabelTTF*>(cell->getChildByTag(265));
				  if (l_lv)
				  {
					  //武将等级
					  std::string _lv = "LV";
					  char s[5];
					  sprintf(s,"%d",GameView::getInstance()->generalsInLineList.at(i)->level());
					  _lv.append(s);
					  l_lv->setString(_lv.c_str());
				  }
			  }
		  }
	  }
  }

  void PackageScene::RemoteStoreHouseEvent( CCObject *pSender )
  {
	  MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	  if (!mainScene)
		  return;

	  mainScene->createRemoteStoreHouseUI();
  }

  void PackageScene::PageScrollToDefault()
  {
	  pacPageView->getPageView()->scrollToPage(0);
	  CurrentPageViewChanged(pacPageView);
  }


  void PackageScene::FirstGradeDesEvent( CCObject *pSender )
  {
	  CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
	  int index = 9;

	  switch(GameView::getInstance()->myplayer->getProfession())
	  {
	  case PROFESSION_MJ_INDEX:
		  {
			  index = 9;
		  }
		  break;
	  case PROFESSION_GM_INDEX:
		  {
			  index = 10;
		  }
		  break;
	  case PROFESSION_HJ_INDEX:
		  {
			  index = 11;
		  }
		  break;
	  case PROFESSION_SS_INDEX:
		  {
			  index = 12;
		  }
		  break;
	  }

	  std::map<int,std::string>::const_iterator cIter;
	  cIter = SystemInfoConfigData::s_systemInfoList.find(index);
	  if (cIter == SystemInfoConfigData::s_systemInfoList.end())
	  {
		  return ;
	  }
	  else
	  {
		  if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		  {
			  ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[index].c_str());
			  GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			  showSystemInfo->ignoreAnchorPointForPosition(false);
			  showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			  showSystemInfo->setPosition(ccp(winsize.width/2, winsize.height/2));
		  }
	  }
  }

  void PackageScene::SecondGradeDesEvent( CCObject *pSender )
  {
	  CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
	  std::map<int,std::string>::const_iterator cIter;
	  cIter = SystemInfoConfigData::s_systemInfoList.find(13);
	  if (cIter == SystemInfoConfigData::s_systemInfoList.end())
	  {
		  return ;
	  }
	  else
	  {
		  if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		  {
			  ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[13].c_str());
			  GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			  showSystemInfo->ignoreAnchorPointForPosition(false);
			  showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			  showSystemInfo->setPosition(ccp(winsize.width/2, winsize.height/2));
		  }
	  }
  }

  void PackageScene::setPresentVoidEquipItem( int part,bool isPresent )
  {
	  switch(part)
	  {
	  case 0 :
		  ImageView_toukui->setVisible(isPresent);    //toukui
		  break;
	  case 1 :
		  ImageView_toukui->setVisible(isPresent);  
		  break;
	  case 2 :
		  ImageView_yifu->setVisible(isPresent);   //yifu
		  break;
	  case 3 :
		  ImageView_peishi->setVisible(isPresent);    //yao
		  break;
	  case 4 :
		  ImageView_wuqi->setVisible(isPresent);    //wuqi
		  break;
	  case 5 :
		  ImageView_toukui->setVisible(isPresent);
		  break;
	  case 6 :
		  ImageView_toukui->setVisible(isPresent);
		  break;
	  case 7 :
		  ImageView_jiezhi->setVisible(isPresent);   //jiezhi
		  break;
	  case 8 :
		  ImageView_toukui->setVisible(isPresent);
		  break;
	  case 9 :
		  ImageView_xiezi->setVisible(isPresent);    //xie
		  break;
	  case 10 :
		  ImageView_toukui->setVisible(isPresent);
		  break;
	  }
  }

  void PackageScene::oneKeyAllEquip( CCObject * obj )
  {
	  GameMessageProcessor::sharedMsgProcessor()->sendReq(1330,(void *)selectActorId);
  }

  ///////////////////////////////////////////////////////////

  GeneralsSmallHeadCell::GeneralsSmallHeadCell()
  {

  }

  GeneralsSmallHeadCell::~GeneralsSmallHeadCell()
  {

  }

  GeneralsSmallHeadCell* GeneralsSmallHeadCell::create(CGeneralBaseMsg * generalBaseMsg)
  {
	  GeneralsSmallHeadCell * generalsSmallHeadCell = new GeneralsSmallHeadCell();
	  if (generalsSmallHeadCell && generalsSmallHeadCell->init(generalBaseMsg))
	  {
		  generalsSmallHeadCell->autorelease();
		  return generalsSmallHeadCell;
	  }
	  CC_SAFE_DELETE(generalsSmallHeadCell);
	  return NULL;
  }

  bool GeneralsSmallHeadCell::init(CGeneralBaseMsg * generalBaseMsg)
  {
	  if (CCTableViewCell::init())
	  {
		  curGeneralBaseMsg = generalBaseMsg;
		  CGeneralBaseMsg * generalBaseMsgFromDB  = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];

		  CCSprite * sprite_frame = CCSprite::create(GeneralsUI::getBigHeadFramePath(generalBaseMsg->currentquality()).c_str());
		  sprite_frame->setAnchorPoint(ccp(0, 0));
		  sprite_frame->setPosition(ccp(0, 0));
		  sprite_frame->setScaleX(0.75f);
		  sprite_frame->setScaleY(0.7f);
		  addChild(sprite_frame);

		  //列表中的头像图标
		  std::string sprite_icon_path = "res_ui/generals/";
		  sprite_icon_path.append(generalBaseMsgFromDB->get_half_photo());
		  sprite_icon_path.append(".anm");
		  CCLegendAnimation *la_head = CCLegendAnimation::create(sprite_icon_path);
		  if (la_head)
		  {
			  la_head->setPlayLoop(true);
			  la_head->setReleaseWhenStop(false);
			  la_head->setScale(0.7f*0.75f);	
			  la_head->setPosition(ccp(4,13));
			  addChild(la_head);
		  }
// 		  //等级
// 		  CCScale9Sprite * sprite_lvFrame = CCScale9Sprite::create("res_ui/zhezhao80.png");
// 		  sprite_lvFrame->setAnchorPoint(ccp(1.0f, 0));
// 		  sprite_lvFrame->setContentSize(CCSizeMake(34,13));
// 		  sprite_lvFrame->setPosition(ccp(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, 21));
// 		  addChild(sprite_lvFrame);
// 		  //武将等级
// 		  std::string _lv = "LV";
// 		  char s[5];
// 		  sprintf(s,"%d",generalBaseMsg->level());
// 		  _lv.append(s);
// 		  CCLabelTTF * label_lv = CCLabelTTF::create(_lv.c_str(),APP_FONT_NAME,13);
// 		  label_lv->setAnchorPoint(ccp(1.0f, 0));
// 		  label_lv->setPosition(ccp(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, 19));
// 		  label_lv->enableStroke(ccc3(0, 0, 0), 2.0f);
// // 		  ccColor3B shadowColor = ccc3(0,0,0);   // black
// // 		  label_lv->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
// 		  label_lv->setTag(265);
// 		  addChild(label_lv);
		  //武将名字黑底
		  CCScale9Sprite * sprite_nameFrame = CCScale9Sprite::create("res_ui/name_di3.png");
		  sprite_nameFrame->setCapInsets(CCRectMake(10,10,1,5));
		  sprite_nameFrame->setAnchorPoint(ccp(0.5f, 0));
		  sprite_nameFrame->setContentSize(CCSizeMake(72,13));
		  sprite_nameFrame->setPosition(ccp(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 8));
		  addChild(sprite_nameFrame);
// 		  CCScale9Sprite * sprite_nameFrame = CCScale9Sprite::create("res_ui/zidi2.png");
// 		  sprite_nameFrame->setCapInsets(CCRectMake(31,8,1,1));
// 		  sprite_nameFrame->setAnchorPoint(ccp(0.5f, 0));
// 		  sprite_nameFrame->setContentSize(CCSizeMake(72,13));
// 		  sprite_nameFrame->setPosition(ccp(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 6));
		  //addChild(sprite_nameFrame);
// 		  CCSprite * sprite_nameFrame = CCSprite::create("res_ui/name_di2.png");
// 		  sprite_nameFrame->setAnchorPoint(ccp(0.5f, 0));
// 		  sprite_nameFrame->setPosition(ccp(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 6));
// 		  sprite_nameFrame->setScaleX(1.1f);
// 		  addChild(sprite_nameFrame);
		  //武将名字
		  CCLabelTTF * label_name = CCLabelTTF::create(generalBaseMsgFromDB->name().c_str(),APP_FONT_NAME,14);
		  label_name->setAnchorPoint(ccp(0.5f, 0));
          label_name->enableStroke(ccc3(0, 0, 0), 2.0f);
		  label_name->setPosition(ccp(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 5));
/*		  label_name->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);*/
		  label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		  addChild(label_name);
		  //稀有度
		  if (generalBaseMsg->rare()>0)
		  {
			  CCSprite * sprite_star = CCSprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			  sprite_star->setAnchorPoint(ccp(0,0));
			  sprite_star->setScale(0.6f);
			  sprite_star->setPosition(ccp(5, 21));
			  addChild(sprite_star);
		  }
		  //武将军衔
		  if (generalBaseMsg->evolution() > 0)
		  {
			  CCSprite * sprite_rank = ActorUtils::createGeneralRankSprite(generalBaseMsg->evolution());
			  sprite_rank->setAnchorPoint(ccp(1.0f,1.0f));
			  sprite_rank->setScale(0.7f);
			  sprite_rank->setPosition(ccp(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, sprite_frame->getContentSize().height*sprite_frame->getScaleY()-6));
			  addChild(sprite_rank);
		  }

		  if(generalBaseMsg->fightstatus() == GeneralsListUI::InBattle)
		  {
			  //出战标识
			  CCSprite * imageView_inBattle = CCSprite::create("res_ui/wujiang/play.png");
			  imageView_inBattle->setAnchorPoint(ccp(0.5,0.5));
			  imageView_inBattle->setPosition(ccp(17,85));
			  imageView_inBattle->setScale(0.8f);
			  addChild(imageView_inBattle);
		  }
		 
		  //profession
		  std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(generalBaseMsgFromDB->get_profession());
		  CCSprite * sprite_profession = CCSprite::create(generalProfess_.c_str());
		  sprite_profession->setAnchorPoint(ccp(0.5f,0.5f));
		  sprite_profession->setPosition(ccp(62 ,
			  32));
		  sprite_profession->setScale(0.6f);
		  addChild(sprite_profession);
		  return true;
	  }
	  return false;
  }

  CGeneralBaseMsg* GeneralsSmallHeadCell::getCurGeneralBaseMsg()
  {
	  return this->curGeneralBaseMsg;
  }














  