
#ifndef _PACKAGESCENE_PACPAGEVIEW_H
#define _PACKAGESCENE_PACPAGEVIEW_H

#include "../extensions/UIScene.h"
//#include "PackageItem.h"

class CEquipment;
class FolderInfo;
class PackageItem;
typedef enum
{
	kTagPageView = 10,
};

struct Coordinate
{
	float x;
	float y;
};

class PacPageView : public UIScene
{
public:
	PacPageView();
	~PacPageView();

	static PacPageView * create();
	bool init();
	void ReloadOnePacItem(FolderInfo* folderInfo);

	int getCurUITag();
	void setCurUITag(int tag);

	void reloadInit();
	void reCreatePageVeiw();
	void ReloadData();

	//设置当前背包格子颜色
	void SetCurFolderGray(bool isGray,int index,std::string isShowGoodsNUm = "");

public:
	std::vector<Coordinate> CoordinateVector;

	std::string pageViewPanelName[5];
	std::string packageItem_name[16];

	int curPackageItemIndex;
	FolderInfo * curFolder;

	int pageNum;
	int itemNumEveryPage ;

private: 
	UIPageView * m_pageView;
	int curUITag;
public: 
	UIPageView * getPageView();
	PackageItem * getPackageItem(int index);

	void RefreshCD( std::string drugId);

	//每次重新赋值时检测下
	void checkCDOnBegan();
};
#endif;

