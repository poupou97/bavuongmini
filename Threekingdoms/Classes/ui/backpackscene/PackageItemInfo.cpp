#include "PackageItemInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../Mail_ui/MailUI.h"
#include "../Auction_ui/AuctionUi.h"
#include "../Chat_ui/BackPackage.h"
#include "../Chat_ui/ChatUI.h"
#include "PackageScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../equipMent_ui/EquipMentUi.h"
#include "../storehouse_ui/StoreHouseUI.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutConfigure.h"
#include "../../gamescene_state/MainScene.h"
#include "../extensions/Counter.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CDrug.h"
#include "GeneralsListForTakeDrug.h"
#include "../../messageclient/element/CFunctionOpenLevel.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "AppMacros.h"
#include "PacPageView.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../messageclient/element/MapStarPickAmount.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"
#include "../shop_ui/ShopUI.h"
#include "../../ui/Chat_ui/PrivateChatUi.h"
#include "../../ui/extensions/UITab.h"
#include "../../legend_script/CCTeachingGuide.h"

// optional int32 additionProperty = 8;
// /** 是否可装备到快捷栏 */
// equip_shortcuts,
// 	/** 是否可直接使用*/
// 	direct_use,	
// 	/** 使用后消失*/
// 	used_dissolve,
// 	/** 可强化*/
// 	strengthen,
// 	/** 可镶嵌*/
// 	inlay,
// 
// 	/** 丢弃后显示 */
// 	discard_show,
// 	/** 丢弃后消失 */
// 	discard_dissolve,
// 
// 	/** 可交易*/
// 	tradeable,
// 	/** 使用前可交易*/
// 	before_use_tradeable,
// 
// 	/** 掉落*/	
// 	dropable,
// 	/** 百分百掉落*/
// 	absolute_dropable,
// 
// 	/** 是否可拍卖*/
// 	auction,
// 	/** 可出售*/
// 	saleable,	
// 	/** 可存入仓库*/
// 	storage,
// 	/** 可穿着*/
// 	wearable,
// 	/** 可洗炼*/
// 	baptize,
// 	/** 可继承*/
// 	inherit,
// 	/** 可做为镶嵌材料*/
// 	inlay_meterial,
// 	/** 可一次使用多个*/
// 	mutil_use
// 	;
// 
// 
// public int intValue() {
// 	switch(this) {
// 	case equip_shortcuts:
// 		return 0X00000001;
// 	case direct_use:	
// 		return 0X00000002;
// 	case used_dissolve:
// 		return 0X00000004;
// 	case strengthen:
// 		return 0X00000008;
// 	case inlay:
// 		return 0X00000010;
// 	case discard_show:
// 		return 0X00000020;
// 	case discard_dissolve:
// 		return 0X00000040;
// 	case tradeable:
// 		return 0X00000080;
// 	case before_use_tradeable:
// 		return 0X00000100;
// 	case dropable:
// 		return 0X00000200;
// 	case absolute_dropable:
// 		return 0X00000400;
// 	case auction:
// 		return 0X00000800;
// 	case saleable:	
// 		return 0X00001000;
// 	case storage:
// 		return 0X00002000;
// 	case wearable:
// 		return 0X00004000;
// 	case baptize:
// 		return 0X00008000;
// 	case inherit:
// 		return 0X00010000;
// 	case inlay_meterial:
// 		return 0X00020000;
// 	case mutil_use:
// 		return 0X00040000;



PackageItemInfo::PackageItemInfo():
deleteAmount(0)
{
	buttonImagePath = "res_ui/new_button_1.png";
	//this->setZOrder(5000);
}

PackageItemInfo::~PackageItemInfo()
{
	delete curGoods;
	delete curFolders;
}

PackageItemInfo * PackageItemInfo::create(FolderInfo * folder,std::vector<CEquipment *>equipmentVector,long long generalId)
{
	PackageItemInfo * packageItem_info = new PackageItemInfo();
	if (packageItem_info && packageItem_info->init(folder,equipmentVector,generalId))
	{
		packageItem_info->autorelease();
		return packageItem_info;
	}
	CC_SAFE_DELETE(packageItem_info);
	return NULL;
}

bool PackageItemInfo::init(FolderInfo * folder,std::vector<CEquipment *>equipmentVector,long long generalId)
{
	curGoods =new GoodsInfo();
	curGoods->CopyFrom(folder->goods());

	curFolders =new FolderInfo();
	curFolders->CopyFrom(*folder);


	if (GoodsItemInfoBase::init(curGoods,equipmentVector,generalId))
	{
		switch(GameView::getInstance()->pacPageView->getCurUITag())
		{
		case kTagBackpack :
			{	
				if (folder->goods().equipmentclazz() == 0)
				{
					if (folder->goods().canshortcut()  == 1)
					{
						UIButton * Button_delete= UIButton::create();
						Button_delete->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_delete->setTouchEnable(true);
						Button_delete->setPressedActionEnabled(true);
						Button_delete->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::deleteEvent));
						Button_delete->setAnchorPoint(ccp(0.5f,0.5f));
						Button_delete->setScale9Enable(true);
						Button_delete->setScale9Size(CCSizeMake(80,43));
						Button_delete->setCapInsets(CCRect(18,9,2,23));
						Button_delete->setPosition(ccp(55,25));

						UILabel * Label_delete = UILabel::create();
						Label_delete->setText((const char *)"丢弃");
						Label_delete->setFontName(APP_FONT_NAME);
						Label_delete->setFontSize(16);
						Label_delete->setAnchorPoint(ccp(0.5f,0.5f));
						Label_delete->setPosition(ccp(0,0));
						Button_delete->addChild(Label_delete);
						//normalInfoPanel->addChild(Button_delete);
						m_pUiLayer->addWidget(Button_delete);


						UIButton * Button_shortCut= UIButton::create();
						Button_shortCut->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_shortCut->setTouchEnable(true);
						Button_shortCut->setPressedActionEnabled(true);
						Button_shortCut->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::shortCutEvent));
						Button_shortCut->setAnchorPoint(ccp(0.5f,0.5f));
						Button_shortCut->setScale9Enable(true);
						Button_shortCut->setScale9Size(CCSizeMake(80,43));
						Button_shortCut->setCapInsets(CCRect(18,9,2,23));
						Button_shortCut->setPosition(ccp(141,25));

						UILabel * Label_shortCut = UILabel::create();
						Label_shortCut->setText((const char *)"快捷");
						Label_shortCut->setFontName(APP_FONT_NAME);
						Label_shortCut->setFontSize(16);
						Label_shortCut->setAnchorPoint(ccp(0.5f,0.5f));
						Label_shortCut->setPosition(ccp(0,0));
						Button_shortCut->addChild(Label_shortCut);
						//normalInfoPanel->addChild(Button_delete);
						m_pUiLayer->addWidget(Button_shortCut);


						UIButton * Button_use= UIButton::create();
						Button_use->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_use->setTouchEnable(true);
						Button_use->setPressedActionEnabled(true);
						Button_use->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::useEvent));
						Button_use->setAnchorPoint(ccp(0.5f,0.5f));
						Button_use->setScale9Enable(true);
						Button_use->setScale9Size(CCSizeMake(80,43));
						Button_use->setCapInsets(CCRect(18,9,2,23));
						Button_use->setPosition(ccp(225,25));
						//使用按钮

						UILabel * Label_use = UILabel::create();
						Label_use->setText((const char *)"使用");
						Label_use->setFontName(APP_FONT_NAME);
						Label_use->setFontSize(16);
						Label_use->setAnchorPoint(ccp(0.5f,0.5f));
						Label_use->setPosition(ccp(0,0));
						Button_use->addChild(Label_use);
						//normalInfoPanel->addChild(Button_delete);
						m_pUiLayer->addWidget(Button_use);

						//合成按钮
						UIButton * Button_systhesis= UIButton::create();
						Button_systhesis->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_systhesis->setTouchEnable(true);
						Button_systhesis->setPressedActionEnabled(true);
						Button_systhesis->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::systhesisEvent));
						Button_systhesis->setAnchorPoint(ccp(0.5f,0.5f));
						Button_systhesis->setScale9Enable(true);
						Button_systhesis->setScale9Size(CCSizeMake(80,43));
						Button_systhesis->setCapInsets(CCRect(18,9,2,23));
						Button_systhesis->setPosition(ccp(225,25));

						UILabel * Label_systhesis = UILabel::create();
						Label_systhesis->setText((const char *)"合成");
						Label_systhesis->setFontName(APP_FONT_NAME);
						Label_systhesis->setFontSize(16);
						Label_systhesis->setAnchorPoint(ccp(0.5f,0.5f));
						Label_systhesis->setPosition(ccp(0,0));
						Button_systhesis->addChild(Label_systhesis);
						m_pUiLayer->addWidget(Button_systhesis);

						if (((folder->goods().additionproperty() & 0X00000002)  !=0)||((folder->goods().additionproperty() & 0X00040000)  !=0) )//可以直接使用
						{
							if(!folder->goods().mergeable())  //合成不显示(只有三个)
							{
								Button_systhesis->setVisible(false);
								Button_delete->setPosition(ccp(57,25));
								Button_systhesis->setPosition(ccp(141,25));
								Button_use->setPosition(ccp(225,25));
							}
							else   //四个都显示
							{
								scrollView_info->setSize(CCSizeMake(548,260));
								scrollView_info->setPosition(ccp(0,97));
								Button_use->setPosition(ccp(80,70));
								Button_shortCut->setPosition(ccp(200,70));
								Button_delete->setPosition(ccp(80,25));
								Button_systhesis->setPosition(ccp(200,25));
							}
						}
						else
						{
							Button_use->setVisible(false);

							if(!folder->goods().mergeable())  //合成不显示(只有两个)
							{
								Button_systhesis->setVisible(false);
								Button_delete->setPosition(ccp(80,25));
								Button_shortCut->setPosition(ccp(200,25));
							}
							else   //只有使用不显示(有三个)
							{
								Button_delete->setPosition(ccp(57,25));
								Button_shortCut->setPosition(ccp(141,25));
								Button_systhesis->setPosition(ccp(225,25));
							}
						}
					}
					else
					{
						//使用按钮
						UIButton * Button_use= UIButton::create();
						Button_use->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_use->setTouchEnable(true);
						Button_use->setPressedActionEnabled(true);
						Button_use->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::useEvent));
						Button_use->setAnchorPoint(ccp(0.5f,0.5f));
						Button_use->setScale9Enable(true);
						Button_use->setScale9Size(CCSizeMake(91,43));
						Button_use->setCapInsets(CCRect(18,9,2,23));
						Button_use->setPosition(ccp(55,25));
						
						UILabel * Label_use = UILabel::create();
						Label_use->setText((const char *)"使用");
						Label_use->setFontName(APP_FONT_NAME);
						Label_use->setFontSize(16);
						Label_use->setAnchorPoint(ccp(0.5f,0.5f));
						Label_use->setPosition(ccp(0,0));
						Button_use->addChild(Label_use);
						//normalInfoPanel->addChild(Button_delete);
						m_pUiLayer->addWidget(Button_use);

						//丢弃按钮
						UIButton * Button_delete= UIButton::create();
						Button_delete->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_delete->setTouchEnable(true);
						Button_delete->setPressedActionEnabled(true);
						Button_delete->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::deleteEvent));
						Button_delete->setAnchorPoint(ccp(0.5f,0.5f));
						Button_delete->setScale9Enable(true);
						Button_delete->setScale9Size(CCSizeMake(91,43));
						Button_delete->setCapInsets(CCRect(18,9,2,23));
						Button_delete->setPosition(ccp(141,25));
 
						UILabel * Label_delete = UILabel::create();
						Label_delete->setText((const char *)"丢弃");
						Label_delete->setFontName(APP_FONT_NAME);
						Label_delete->setFontSize(16);
						Label_delete->setAnchorPoint(ccp(0.5f,0.5f));
						Label_delete->setPosition(ccp(0,0));
						Button_delete->addChild(Label_delete);
						//normalInfoPanel->addChild(Button_delete);
						m_pUiLayer->addWidget(Button_delete);

						//合成按钮
						UIButton * Button_systhesis= UIButton::create();
						Button_systhesis->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_systhesis->setTouchEnable(true);
						Button_systhesis->setPressedActionEnabled(true);
						Button_systhesis->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::systhesisEvent));
						Button_systhesis->setAnchorPoint(ccp(0.5f,0.5f));
						Button_systhesis->setScale9Enable(true);
						Button_systhesis->setScale9Size(CCSizeMake(91,43));
						Button_systhesis->setCapInsets(CCRect(18,9,2,23));
						Button_systhesis->setPosition(ccp(225,25));

						UILabel * Label_systhesis = UILabel::create();
						Label_systhesis->setText((const char *)"合成");
						Label_systhesis->setFontName(APP_FONT_NAME);
						Label_systhesis->setFontSize(16);
						Label_systhesis->setAnchorPoint(ccp(0.5f,0.5f));
						Label_systhesis->setPosition(ccp(0,0));
						Button_systhesis->addChild(Label_systhesis);
						m_pUiLayer->addWidget(Button_systhesis);

						if (((folder->goods().additionproperty() & 0X00000002)  !=0)||((folder->goods().additionproperty() & 0X00040000)  !=0)  )//可以直接使用
						{
							if(!folder->goods().mergeable())
							{
								Button_systhesis->setVisible(false);
								Button_use->setPosition(ccp(200,25));
								Button_delete->setPosition(ccp(80,25));
							}
						}
						else
						{
							Button_use->setVisible(false);
							if (!folder->goods().mergeable())
							{
								Button_systhesis->setVisible(false);
								Button_delete->setPosition(ccp(141,25));
							}
							else
							{
								Button_delete->setPosition(ccp(80,25));
								Button_systhesis->setPosition(ccp(200,25));
							}
							
						}
					}
					
				}
				else if (folder->goods().equipmentclazz()>0 && folder->goods().equipmentclazz()<10)
				{
					UIButton * Button_pickStar= UIButton::create();
					Button_pickStar->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_pickStar->setTouchEnable(true);
					Button_pickStar->setPressedActionEnabled(true);
					Button_pickStar->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::callBackPick));
					Button_pickStar->setAnchorPoint(ccp(0.5f,0.5f));
					Button_pickStar->setScale9Enable(true);
					Button_pickStar->setScale9Size(CCSizeMake(80,43));
					Button_pickStar->setCapInsets(CCRect(18,9,2,23));
					Button_pickStar->setPosition(ccp(141,25));

					UILabel * Label_pick = UILabel::create();
					Label_pick->setText((const char *)"摘星");
					Label_pick->setFontName(APP_FONT_NAME);
					Label_pick->setFontSize(18);
					Label_pick->setAnchorPoint(ccp(0.5f,0.5f));
					Label_pick->setPosition(ccp(0,0));
					Button_pickStar->addChild(Label_pick);
					m_pUiLayer->addWidget(Button_pickStar);


					UIButton * Button_delete= UIButton::create();
					Button_delete->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_delete->setTouchEnable(true);
					Button_delete->setPressedActionEnabled(true);
					Button_delete->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::deleteEvent));
					Button_delete->setAnchorPoint(ccp(0.5f,0.5f));
					Button_delete->setScale9Enable(true);
					Button_delete->setScale9Size(CCSizeMake(80,43));
					Button_delete->setCapInsets(CCRect(18,9,2,23));
					Button_delete->setPosition(ccp(55,25));
					//丢弃按钮
				
					UILabel * Label_delete = UILabel::create();
					Label_delete->setText((const char *)"丢弃");
					Label_delete->setFontName(APP_FONT_NAME);
					Label_delete->setFontSize(16);
					Label_delete->setAnchorPoint(ccp(0.5f,0.5f));
					Label_delete->setPosition(ccp(0,0));
					Button_delete->addChild(Label_delete);
					m_pUiLayer->addWidget(Button_delete);
					//强化按钮
					
					UIButton * Button_LVUp= UIButton::create();
					Button_LVUp->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_LVUp->setTouchEnable(true);
					Button_LVUp->setPressedActionEnabled(true);
					Button_LVUp->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::lvUpEvent));
					Button_LVUp->setAnchorPoint(ccp(0.5f,0.5f));
					Button_LVUp->setScale9Enable(true);
					Button_LVUp->setScale9Size(CCSizeMake(80,43));
					Button_LVUp->setCapInsets(CCRect(18,9,2,23));
					Button_LVUp->setPosition(ccp(141,25));

					UILabel * Label_LVUp = UILabel::create();
					Label_LVUp->setText((const char *)"强化");
					Label_LVUp->setFontName(APP_FONT_NAME);
					Label_LVUp->setFontSize(16);
					Label_LVUp->setAnchorPoint(ccp(0.5f,0.5f));
					Label_LVUp->setPosition(ccp(0,0));
					Button_LVUp->addChild(Label_LVUp);
					m_pUiLayer->addWidget(Button_LVUp);
					
					//装备按钮
					Button_dressOn= UIButton::create();
					Button_dressOn->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_dressOn->setTouchEnable(true);
					Button_dressOn->setPressedActionEnabled(true);
					Button_dressOn->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::dressOnEvent));
					Button_dressOn->setAnchorPoint(ccp(0.5f,0.5f));
					Button_dressOn->setScale9Enable(true);
					Button_dressOn->setScale9Size(CCSizeMake(80,43));
					Button_dressOn->setCapInsets(CCRect(18,9,2,23));
					Button_dressOn->setTag(112233);
					Button_dressOn->setPosition(ccp(224,25));

					UILabel * Label_dressOn = UILabel::create();
					Label_dressOn->setText((const char *)"装备");
					Label_dressOn->setFontName(APP_FONT_NAME);
					Label_dressOn->setFontSize(16);
					Label_dressOn->setAnchorPoint(ccp(0.5f,0.5f));
					Label_dressOn->setPosition(ccp(0,0));
					Button_dressOn->addChild(Label_dressOn);
					m_pUiLayer->addWidget(Button_dressOn);
					//强化按等级开放
					int roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
					int openLevel = 0;
					for (int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
					{
						if (strcmp("btn_strengthen",FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str()) == 0)
						{
							openLevel = FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel();
							break;
						}
					}
					if (openLevel <= roleLevel)     //可以显示
					{
						Button_LVUp->setVisible(true);
						Button_pickStar->setVisible(true);
						Button_delete->setPosition(ccp(58,25));
						Button_dressOn->setPosition(ccp(224,25));
					}
					else
					{
						Button_LVUp->setVisible(false);
						Button_pickStar->setVisible(false);
						Button_delete->setPosition(ccp(80,25));
						Button_dressOn->setPosition(ccp(200,25));
					}
					Button_pickStar->setVisible(false);

					if (folder->goods().equipmentdetail().starlevel() > 0)
					{
						scrollView_info->setSize(CCSizeMake(548,260));
						scrollView_info->setPosition(ccp(0,97));
						Button_pickStar->setVisible(true);
						Button_pickStar->setPosition(ccp(58,70));
						Button_LVUp->setPosition(ccp(224,70));
					}
				}
				break;
			}
			case kTabChat :
				{
					const char *strings_synthesize = StringDataManager::getString("goods_chat_btnAdd");
					char *str_synthesize=const_cast<char*>(strings_synthesize);

					UIButton * Button_getAnnes= UIButton::create();
					Button_getAnnes->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_getAnnes->setTouchEnable(true);
					Button_getAnnes->setPressedActionEnabled(true);
					Button_getAnnes->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::showGoodsInfo));
					Button_getAnnes->setAnchorPoint(ccp(0.5f,0.5f));
					Button_getAnnes->setScale9Enable(true);
					Button_getAnnes->setScale9Size(CCSizeMake(110,43));
					Button_getAnnes->setCapInsets(CCRect(18,9,2,23));
					Button_getAnnes->setPosition(ccp(141,25));

					UILabel * Label_getAnnes = UILabel::create();
					Label_getAnnes->setText(str_synthesize);
					Label_getAnnes->setFontName(APP_FONT_NAME);
					Label_getAnnes->setFontSize(16);
					Label_getAnnes->setAnchorPoint(ccp(0.5f,0.5f));
					Label_getAnnes->setPosition(ccp(0,0));
					Button_getAnnes->addChild(Label_getAnnes);
					m_pUiLayer->addWidget(Button_getAnnes);
					break;
				}
			case ktagPrivateUI :
				{
					const char *strings_synthesize = StringDataManager::getString("goods_chat_btnAdd");
					char *str_synthesize=const_cast<char*>(strings_synthesize);

					UIButton * Button_getAnnes= UIButton::create();
					Button_getAnnes->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_getAnnes->setTouchEnable(true);
					Button_getAnnes->setPressedActionEnabled(true);
					Button_getAnnes->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::showGoodsInfoPrivateui));
					Button_getAnnes->setAnchorPoint(ccp(0.5f,0.5f));
					Button_getAnnes->setScale9Enable(true);
					Button_getAnnes->setScale9Size(CCSizeMake(110,43));
					Button_getAnnes->setCapInsets(CCRect(18,9,2,23));
					Button_getAnnes->setPosition(ccp(141,25));

					UILabel * Label_getAnnes = UILabel::create();
					Label_getAnnes->setText(str_synthesize);
					Label_getAnnes->setFontName(APP_FONT_NAME);
					Label_getAnnes->setFontSize(16);
					Label_getAnnes->setAnchorPoint(ccp(0.5f,0.5f));
					Label_getAnnes->setPosition(ccp(0,0));
					Button_getAnnes->addChild(Label_getAnnes);
					m_pUiLayer->addWidget(Button_getAnnes);
					break;
				}
			case kTagMailUi :
				{
					const char *strings_synthesize = StringDataManager::getString("goods_mail_btnAdd");
					char *str_synthesize=const_cast<char*>(strings_synthesize);

					UIButton * Button_getAnnes= UIButton::create();
					Button_getAnnes->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_getAnnes->setTouchEnable(true);
					Button_getAnnes->setPressedActionEnabled(true);
					Button_getAnnes->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::GetAnnesEvent));
					Button_getAnnes->setAnchorPoint(ccp(0.5f,0.5f));
					Button_getAnnes->setScale9Enable(true);
					Button_getAnnes->setScale9Size(CCSizeMake(110,43));
					Button_getAnnes->setCapInsets(CCRect(18,9,2,23));
					Button_getAnnes->setPosition(ccp(141,25));

					UILabel * Label_getAnnes = UILabel::create();
					Label_getAnnes->setText(str_synthesize);
					Label_getAnnes->setFontName(APP_FONT_NAME);
					Label_getAnnes->setFontSize(16);
					Label_getAnnes->setAnchorPoint(ccp(0.5f,0.5f));
					Label_getAnnes->setPosition(ccp(0,0));
					Button_getAnnes->addChild(Label_getAnnes);
					m_pUiLayer->addWidget(Button_getAnnes);

					break;
				}
			case kTagAuction:
				{
					const char *strings_consign = StringDataManager::getString("goods_auction_consion");
					char *str_consign=const_cast<char*>(strings_consign);

					const char *strings_search = StringDataManager::getString("goods_auction_search");
					char *str_search=const_cast<char*>(strings_search);

					UIButton * Button_consign= UIButton::create();
					Button_consign->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_consign->setTouchEnable(true);
					Button_consign->setPressedActionEnabled(true);
					Button_consign->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::callBackConsign));
					Button_consign->setAnchorPoint(ccp(0.5f,0.5f));
					Button_consign->setScale9Enable(true);
					Button_consign->setScale9Size(CCSizeMake(91,43));
					Button_consign->setCapInsets(CCRect(18,9,2,23));
					Button_consign->setPosition(ccp(80,25));

					UILabel * Label_consign = UILabel::create();
					Label_consign->setText(str_consign);
					Label_consign->setFontName(APP_FONT_NAME);
					Label_consign->setFontSize(16);
					Label_consign->setAnchorPoint(ccp(0.5f,0.5f));
					Label_consign->setPosition(ccp(0,0));
					Button_consign->addChild(Label_consign);
					//normalInfoPanel->addChild(Button_delete);
					m_pUiLayer->addWidget(Button_consign);

					UIButton * Button_search= UIButton::create();
					Button_search->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_search->setTouchEnable(true);
					Button_search->setPressedActionEnabled(true);
					Button_search->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::callBackSearch));
					Button_search->setAnchorPoint(ccp(0.5f,0.5f));
					Button_search->setScale9Enable(true);
					Button_search->setScale9Size(CCSizeMake(91,43));
					Button_search->setCapInsets(CCRect(18,9,2,23));
					Button_search->setPosition(ccp(200,25));

					UILabel * Label_search = UILabel::create();
					Label_search->setText(str_search);
					Label_search->setFontName(APP_FONT_NAME);
					Label_search->setFontSize(16);
					Label_search->setAnchorPoint(ccp(0.5f,0.5f));
					Label_search->setPosition(ccp(0,0));
					Button_search->addChild(Label_search);
					//normalInfoPanel->addChild(Button_delete);
					m_pUiLayer->addWidget(Button_search);
					break;
				}
			case ktagEquipMentUI:
				{
					
					EquipMentUi * equip= (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
					//add main equip
					UIButton * btn_addMainEquip= UIButton::create();
					btn_addMainEquip->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					btn_addMainEquip->setTouchEnable(true);
					btn_addMainEquip->setPressedActionEnabled(true);
					btn_addMainEquip->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::showCEquipment));
					btn_addMainEquip->setAnchorPoint(ccp(0.5f,0.5f));
					btn_addMainEquip->setScale9Enable(true);
					btn_addMainEquip->setScale9Size(CCSizeMake(91,43));
					btn_addMainEquip->setCapInsets(CCRect(18,9,2,23));
					btn_addMainEquip->setPosition(ccp(141,25));
					const char *strings_;
					if (equip->currType == 2 && folder->goods().clazz()== GOODS_CLASS_XIANGQIANCAILIAO)
					{
						strings_ = StringDataManager::getString("equip_strength_equipLayily");
					}else
					{
						strings_ = StringDataManager::getString("goods_equipment_main");	
					}

					UILabel * Label_mainEquip = UILabel::create();
					Label_mainEquip->setText(strings_);
					Label_mainEquip->setFontName(APP_FONT_NAME);
					Label_mainEquip->setFontSize(16);
					Label_mainEquip->setAnchorPoint(ccp(0.5f,0.5f));
					Label_mainEquip->setPosition(ccp(0,0));
					btn_addMainEquip->addChild(Label_mainEquip);
					m_pUiLayer->addWidget(btn_addMainEquip);
					
					break;
				}
			case kTagStoreHouseUI:
				{
					UIButton * Button_putIn= UIButton::create();
					Button_putIn->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_putIn->setTouchEnable(true);
					Button_putIn->setPressedActionEnabled(true);
					Button_putIn->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::PutInEvent));
					Button_putIn->setAnchorPoint(ccp(0.5f,0.5f));
					Button_putIn->setScale9Enable(true);
					Button_putIn->setScale9Size(CCSizeMake(91,43));
					Button_putIn->setCapInsets(CCRect(18,9,2,23));
					Button_putIn->setPosition(ccp(141,25));

					const char *strings_putIn = StringDataManager::getString("storeHouse_putIn");
					UILabel * Label_putIn = UILabel::create();
					Label_putIn->setText(strings_putIn);
					Label_putIn->setFontName(APP_FONT_NAME);
					Label_putIn->setFontSize(16);
					Label_putIn->setAnchorPoint(ccp(0.5f,0.5f));
					Label_putIn->setPosition(ccp(0,0));
					Button_putIn->addChild(Label_putIn);
					m_pUiLayer->addWidget(Button_putIn);

					break;
				}
			case kTagShopUI :
				{

					UIButton * Button_sell= UIButton::create();
					Button_sell->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_sell->setTouchEnable(true);
					Button_sell->setPressedActionEnabled(true);
					Button_sell->addReleaseEvent(this,coco_releaseselector(PackageItemInfo::SellEvent));
					Button_sell->setAnchorPoint(ccp(0.5f,0.5f));
					Button_sell->setScale9Enable(true);
					Button_sell->setScale9Size(CCSizeMake(91,43));
					Button_sell->setCapInsets(CCRect(18,9,2,23));
					Button_sell->setPosition(ccp(141,25));

					UILabel * Label_sell = UILabel::create();
					Label_sell->setText((const char *)"出售");
					Label_sell->setFontName(APP_FONT_NAME);
					Label_sell->setFontSize(16);
					Label_sell->setAnchorPoint(ccp(0.5f,0.5f));
					Label_sell->setPosition(ccp(0,0));
					Button_sell->addChild(Label_sell);
					m_pUiLayer->addWidget(Button_sell);
					break;
				}
		}
		
		
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setAnchorPoint(ccp(0,0));
		//this->setContentSize(CCSizeMake(background->getContentSize().width,background->getContentSize().height));
		//this->setContentSize(CCSizeMake(274,362));

		return true;
	}
	return false;
}

void PackageItemInfo::onEnter()
{
	UIScene::onEnter();
	CCFiniteTimeAction*  action = CCSequence::create(
		CCScaleTo::create(0.15f*1/2,1.1f),
		CCScaleTo::create(0.15f*1/3,1.0f),
		NULL);
	runAction(action);
}
void PackageItemInfo::onExit()
{
	UIScene::onExit();
}

bool PackageItemInfo::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	return this->resignFirstResponder(pTouch,this,false);
}
void PackageItemInfo::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void PackageItemInfo::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void PackageItemInfo::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}

void PackageItemInfo::dressOnEvent(CCObject * pSender)
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	if(this->isClosing())
		return;

// 	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
// 	if(sc != NULL)
// 		sc->endCommand(pSender);

	if (GameView::getInstance()->pacPageView->curFolder->goods().binding() == 1)
	{
		PackageScene * packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1303,GameView::getInstance()->pacPageView->curFolder,(void *)packageScene->selectActorId);
		GameUtils::playGameSound(EQUIP_DRESS, 2, false);
		//关闭详细信息界面
		this->removeFromParentAndCleanup(true);
	}
	else
	{
		GameView::getInstance()->showPopupWindow(StringDataManager::getString("packageitemInfo_isSureToDressOn"),2,this,coco_selectselector(PackageItemInfo::sureToDressOn),NULL);
	}

}

void PackageItemInfo::sureToDressOn(CCObject * pSender)
{
	PackageScene * packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1303,GameView::getInstance()->pacPageView->curFolder,(void *)packageScene->selectActorId);
	GameUtils::playGameSound(EQUIP_DRESS, 2, false);
	//关闭详细信息界面
	this->removeFromParentAndCleanup(true);
}

void PackageItemInfo::deleteEvent(CCObject * pSender)
{
	if(this->isClosing())
		return;
		
	if (GameView::getInstance()->pacPageView->curFolder->quantity() == 1)
	{
		deleteAmount = 1;
		GameView::getInstance()->showPopupWindow(StringDataManager::getString("packageitem_delete"),2,this,coco_selectselector(PackageItemInfo::sureToDelete),NULL);
	}
	else
	{
		//弹出计算器
		GameView::getInstance()->showCounter(this,callfuncO_selector(PackageItemInfo::showCountForDelete),GameView::getInstance()->pacPageView->curFolder->quantity());
	}

	GameUtils::playGameSound(ITEM_DISCARD, 2, false);
	// 	if (curGoods->quality() >= 3)
// 	{
// 		GameView::getInstance()->showPopupWindow(StringDataManager::getString("packageitem_delete"),2,this,coco_selectselector(PackageItemInfo::sureToDelete),NULL);
// 	}
// 	else
// 	{
// 		GameMessageProcessor::sharedMsgProcessor()->sendReq(1313, this);
// 		//关闭详细信息界面
// 		this->removeFromParentAndCleanup(true);
// 	}
}

void PackageItemInfo::showCountForDelete(CCObject *pSender)
{
	Counter * counter_ =(Counter *)pSender;
	int amount_;
	if (counter_ != NULL)
	{
		amount_ =counter_->getInputNum();
	}else
	{
		amount_ =1;
	}

	deleteAmount = amount_;

	if (amount_ > GameView::getInstance()->pacPageView->curFolder->quantity())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("packageitemInfo_notEnough"));
	}
	else
	{
		if (GameView::getInstance()->pacPageView->curFolder->goods().quality() >= 3)  //蓝色品阶以上的需要二次确认
		{
		 	GameView::getInstance()->showPopupWindow(StringDataManager::getString("packageitem_delete"),2,this,coco_selectselector(PackageItemInfo::sureToDelete),NULL);
		}
		else
		{
		 	GameMessageProcessor::sharedMsgProcessor()->sendReq(1313, (void *)GameView::getInstance()->pacPageView->curPackageItemIndex,(void *)deleteAmount);
		 	//关闭详细信息界面
		 	this->removeFromParentAndCleanup(true);
		}
	}
}

void PackageItemInfo::sureToDelete(CCObject * pSender)
{
	if (deleteAmount <= 0)
		return;

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1313, (void *)GameView::getInstance()->pacPageView->curPackageItemIndex,(void *)deleteAmount);
	//关闭详细信息界面
	this->removeFromParentAndCleanup(true);
}

void PackageItemInfo::lvUpEvent(CCObject * pSender)
{
	if(this->isClosing())
		return;

	CCLog("lvUpEvent");
	//关闭详细信息界面
	this->setVisible(false);
	PackageScene *packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	if(packageScene != NULL)
	{
		packageScene->closeEndedEvent(NULL);
	}

	CCActionInterval * action =(CCActionInterval *)CCSequence::create(CCDelayTime::create(0.5f),
										CCCallFuncO::create(this,callfuncO_selector(PackageItemInfo::showEquipStrength),NULL),
										CCRemoveSelf::create(),
										NULL);
	runAction(action);
}

void PackageItemInfo::shortCutEvent(CCObject *pSender)
{
	if(this->isClosing())
		return;

	CCLog("shortCutEvent");
	//打开快捷面板
	CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
	ShortcutConfigure * shortcutConfigure = ShortcutConfigure::create();
	shortcutConfigure->setTag(kTagShortcutConfigure);
	GameView::getInstance()->getMainUIScene()->addChild(shortcutConfigure);
	shortcutConfigure->ignoreAnchorPointForPosition(false);
	shortcutConfigure->setAnchorPoint(ccp(0.5f,0.5f));
	shortcutConfigure->setPosition(ccp(winsize.width/2,winsize.height/2));
	//关闭详细信息界面
	this->removeFromParentAndCleanup(true);
}
void PackageItemInfo::useEvent(CCObject *pSender)
{
	if(this->isClosing())
		return;

	CCLog("useEvent");
	PackageScene * packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	if (packageScene)
	{
// 		FolderInfo * folderInfo = new FolderInfo();
// 		for (int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
// 		{
// 			if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
// 			{
// 				if (strcmp(curGoods->id().c_str(),GameView::getInstance()->AllPacItem.at(i)->goods().id().c_str()) == 0)
// 				{
// 					folderInfo->CopyFrom(*GameView::getInstance()->AllPacItem.at(i));
// 					break;
// 				}
// 			}
// 		}

		if(!curFolders->has_goods())
			return;

		if ((curFolders->goods().additionproperty() & 0X00040000)  !=0)  //可以一次性使用多个
		{
			if (curFolders->quantity() == 1)
			{
				PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
				temp->index = GameView::getInstance()->pacPageView->curPackageItemIndex;
				temp->num = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0,temp);
				delete temp;

				this->removeFromParentAndCleanup(true);
			}
			else
			{
				//弹出计算器
				GameView::getInstance()->showCounter(this,callfuncO_selector(PackageItemInfo::useAllEvent),GameView::getInstance()->pacPageView->curFolder->quantity());
			}
		}
		else
		{
			if (curGoods->clazz() == 30 || curGoods->clazz() == 28)//武将用（复活）/武将用（经验丹）
			{
				if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralListForTakeDrug) == NULL)
				{
					CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
					GeneralsListForTakeDrug * generalListForTakeDrug = GeneralsListForTakeDrug::create(curFolders);
					generalListForTakeDrug->ignoreAnchorPointForPosition(false);
					generalListForTakeDrug->setAnchorPoint(ccp(0.5f,0.5f));
					generalListForTakeDrug->setPosition(ccp(winSize.width/2,winSize.height/2));
					GameView::getInstance()->getMainUIScene()->addChild(generalListForTakeDrug,0,kTagGeneralListForTakeDrug);
				}
			}
			else if (curGoods->clazz() == 1)  //是药水
			{
				std::map<std::string,CDrug*>::const_iterator cIter;
				cIter = CDrugMsgConfigData::s_drugBaseMsg.find(curGoods->id());
				if (cIter != CDrugMsgConfigData::s_drugBaseMsg.end()) // 没找到就是指向END了  
				{
					CDrug * temp =  CDrugMsgConfigData::s_drugBaseMsg.find(curGoods->id())->second;
					if (temp->get_type_flag() == 2)//武将用
					{
						if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralListForTakeDrug) == NULL)
						{
							CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
							GeneralsListForTakeDrug * generalListForTakeDrug = GeneralsListForTakeDrug::create(curFolders);
							generalListForTakeDrug->ignoreAnchorPointForPosition(false);
							generalListForTakeDrug->setAnchorPoint(ccp(0.5f,0.5f));
							generalListForTakeDrug->setPosition(ccp(winSize.width/2,winSize.height/2));
							GameView::getInstance()->getMainUIScene()->addChild(generalListForTakeDrug,0,kTagGeneralListForTakeDrug);
						}
					}
					else  //角色用
					{
						PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
						temp->index = GameView::getInstance()->pacPageView->curPackageItemIndex;
						temp->num = 1;
						GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)packageScene->selectActorId,temp);
						delete temp;
					}
				}
			}
			else
			{
				PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
				temp->index = GameView::getInstance()->pacPageView->curPackageItemIndex;
				temp->num = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0,temp);
				delete temp;
			}
			this->removeFromParentAndCleanup(true);
		}
	}
	else
	{
	}
	GameUtils::playGameSound(PROP_USE, 2, false);
	//关闭详细信息界面
	//this->removeFromParentAndCleanup(true);
}


 void PackageItemInfo::scrollViewDidScroll(CCScrollView* view)
{
	
}
 
 void PackageItemInfo::scrollViewDidZoom(CCScrollView* view)
 {

 }

 void PackageItemInfo::GetAnnesEvent( CCObject * pSender )
 {
	 if(this->isClosing())
		 return;

	 CCLog("GetAnnesEvent");
	 //添加附件
	int isbingding =  GameView::getInstance()->AllPacItem.at(GameView::getInstance()->pacPageView->curPackageItemIndex)->goods().binding();
	 if (isbingding == 0)
	 {
		 MailUI *mail_ui = (MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
		 mail_ui->callBackAddexan(pSender);
	 }else
	 {
		 const char *string_  = StringDataManager::getString("mail_ofnotSendValue");
		 GameView::getInstance()->showAlertDialog(string_);
	 }
	removeFromParentAndCleanup(true);
 }

 void PackageItemInfo::cancelAuction( CCObject * pSender )
 {
	 if(this->isClosing())
		 return;

	 //cancel auction
	 this->removeFromParentAndCleanup(true);

 }

 void PackageItemInfo::callBackSearch( CCObject * obj )
 {
	 if(this->isClosing())
		 return;

	 AuctionUi *auctionui = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);

	 if(auctionui == NULL)
	 {
		 return;
	 }

	 auctionui->labelType->setText(auctionui->acutionTypevector.at(0)->typedesc().c_str());
	 auctionui->setAuctiontype= auctionui->acutionTypevector.at(0)->type();
	 auctionui->isSelectChat = false;
	 auctionui->labelKind->setText(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->subtypedescs(0).c_str());
	 auctionui->labelLevel->setText(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->leveldescs(0).c_str());
	 auctionui->labelQuality->setText(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->qualitydescs(0).c_str());

	 const char *strings_goldAll = StringDataManager::getString("auction_SelectMoney");
	 auctionui->label_gold->setText(strings_goldAll);

	 auctionui->setpageindex=1;
	 auctionui->setAuctiontype = 0;
	 auctionui->setAuctionsubtype = 0;
	 auctionui->setlevel = 0;
	 auctionui->setMoney = 0;
	 auctionui->setquality = 0;
	 auctionui->setorderby = 0;
	 auctionui->setorderbydesc = 0;

	 auctionui->setname = GameView::getInstance()->AllPacItem.at(auctionui->pageView->curPackageItemIndex)->goods().name();
	 GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,auctionui);
	 removeFromParentAndCleanup(true);
	 auctionui->callBack_btn_back(obj);
 }

 void PackageItemInfo::callBackConsign( CCObject * obj )
 {
	 if(this->isClosing())
		 return;

	 //add auction
	 int isbingding =  GameView::getInstance()->AllPacItem.at(GameView::getInstance()->pacPageView->curPackageItemIndex)->goods().binding();
	 if (isbingding == 0)
	 {
		 AuctionUi *auctionui = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
		 auctionui->addAuctionPacInfo(obj);
	 }else
	 {
		 const char *string_  = StringDataManager::getString("mail_ofnotSendValue");
		 GameView::getInstance()->showAlertDialog(string_);
	 }
	 removeFromParentAndCleanup(true);
 }

 void PackageItemInfo::showGoodsInfo( CCObject * obj )
 {
	 if(this->isClosing())
		 return;

	 BackPackage * package=(BackPackage *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat)->getChildByTag(PACKAGEAGE);
	 package->callBack(obj);
	 removeFromParentAndCleanup(true);
 }

 void PackageItemInfo::showGoodsInfoPrivateui( CCObject * obj )
 {
	 if(this->isClosing())
		 return;

	 PrivateChatUi * privateui_ = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);
	BackPackage * package=(BackPackage *)privateui_->layer2->getChildByTag(PACKAGEAGE);
	 package->callBackOfPrivateUi();
	 removeFromParentAndCleanup(true);
 }

 void PackageItemInfo::showCEquipment( CCObject * obj )
 {
	 if(this->isClosing())
		 return;

	 UIButton * btn =(UIButton *)obj;
	 EquipMentUi *equip_ui = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	 equip_ui->addOperationFloder(curFolders);

	 removeFromParentAndCleanup(true);	
 }

 void PackageItemInfo::showStrengthStone( CCObject * obj )
 {
	 //add assist strengthStone
	 /*
	 EquipMentUi *equip_ui = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	 equip_ui->addStrengthStone(obj);
	 removeFromParentAndCleanup(true);
	 */
 }

 void PackageItemInfo::useAllEvent( CCObject *pSender )
 {
	 Counter * counter_ =(Counter *)pSender;
	 int amount_;
	 if (counter_ != NULL)
	 {
		 amount_ =counter_->getInputNum();
	 }else
	 {
		 amount_ =1;
	 }

	 if (amount_ > GameView::getInstance()->pacPageView->curFolder->quantity())
	 {
		 GameView::getInstance()->showAlertDialog(StringDataManager::getString("packageitemInfo_notEnough"));
	 }
	 else
	 {
		 PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
		 temp->index = GameView::getInstance()->pacPageView->curPackageItemIndex;
		 temp->num = amount_;
		 GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0,temp);
		 delete temp;
	 }

	 this->removeFromParentAndCleanup(true);
 }

 void PackageItemInfo::PutInEvent( CCObject * pSender )
 {
	 if(this->isClosing())
		 return;

	 StoreHouseUI * storeHouseui = (StoreHouseUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStoreHouseUI);
	 storeHouseui->PutInOrOutEvent(pSender);
	 removeFromParentAndCleanup(true);
 }

 void PackageItemInfo::SellEvent( CCObject *pSender )
 {
	 if(this->isClosing())
		 return;

	 if (curFolders->quantity() <= 1)
	 {
		 ShopUI::sellGoodsStruct goodsStruct= {GameView::getInstance()->pacPageView->curPackageItemIndex,curFolders->quantity(),0};
		 GameMessageProcessor::sharedMsgProcessor()->sendReq(1135,&goodsStruct);
		 GameUtils::playGameSound(PROP_SALE, 2, false);
		 this->removeFromParent();
	 }
	 else
	 {
		 GameView::getInstance()->showCounter(this, callfuncO_selector(PackageItemInfo::SellEventNum),curFolders->quantity());
	 }
 }

 void PackageItemInfo::SellEventNum( CCObject *pSender )
 {
	 Counter * counter_ =(Counter *)pSender;
	 int amount_;
	 if (counter_ != NULL)
	 {
		 amount_ =counter_->getInputNum();
	 }else
	 {
		 amount_ =1;
	 }

	 if (amount_ <= 0)
	 {

	 }
	 else
	 {
		 ShopUI::sellGoodsStruct goodsStruct= {GameView::getInstance()->pacPageView->curPackageItemIndex,amount_,0};
		 GameMessageProcessor::sharedMsgProcessor()->sendReq(1135,&goodsStruct);
	 }
	 
	 GameUtils::playGameSound(PROP_SALE, 2, false);
	 this->removeFromParent();
 }

 void PackageItemInfo::showEquipStrength( CCObject * obj )
 {
	 CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	 if(GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI) == NULL)
	 {
		 EquipMentUi * equip=EquipMentUi::create();
		 equip->ignoreAnchorPointForPosition(false);
		 equip->setAnchorPoint(ccp(0.5f,0.5f));
		 equip->setPosition(ccp(winSize.width/2,winSize.height/2));
		 GameView::getInstance()->getMainUIScene()->addChild(equip,0,ktagEquipMentUI);
		 equip->refreshBackPack(0,-1);

		FolderInfo * folders = new FolderInfo(*GameView::getInstance()->pacPageView->curFolder); 
		equip->addMainEquipment(folders);
		delete folders;
	 }
 }

 //////////////////////
 void PackageItemInfo::registerScriptCommand( int scriptId )
 {
	 mTutorialScriptInstanceId = scriptId;
 }

 void PackageItemInfo::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
 {
	 CCSize winSize = CCDirector::sharedDirector()->getWinSize();
// 	 CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,35,50);
// 	 tutorialIndicator->setPosition(ccp(pos.x+55,pos.y+80));
// 	 tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	 m_pUiLayer->addChild(tutorialIndicator);

	 int _w = (winSize.width-800)/2;
	 int _h = (winSize.height - 480)/2;

	 CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,80,43);
	 tutorialIndicator->setDrawNodePos(ccp(pos.x+85.5f+_w,pos.y+59.5f+_h));
	 tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	 MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	 if (mainScene)
	 {
		 mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	 }
 }

 void PackageItemInfo::removeCCTutorialIndicator()
 {
// 	 CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(m_pUiLayer->getChildByTag(CCTUTORIALINDICATORTAG));
// 	 if(tutorialIndicator != NULL)
// 		 tutorialIndicator->removeFromParent();

	 MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	 if (mainScene)
	 {
		 CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		 if(teachingGuide != NULL)
			 teachingGuide->removeFromParent();
	 }
 }

 void PackageItemInfo::callBackPick( CCObject * obj )
 {
	 this->removeFromParentAndCleanup(true);
	 const char *strBegin  = StringDataManager::getString("packBackGoodsPickSureBegin");
	 const char *strEnd  = StringDataManager::getString("packBackGoodsPickSureEnd");

	 int starLevel =  GameView::getInstance()->AllPacItem.at(GameView::getInstance()->pacPageView->curPackageItemIndex)->goods().equipmentdetail().starlevel();

	 MapStarPickAmount * equipStarLevel = StrengthStarConfigData::s_EquipStarAmount[starLevel];
	 int starCount_ = equipStarLevel->get_count();
	 char str[20];
	 sprintf(str,"%d",starCount_);

	 std::string string_ = strBegin;
	 string_.append(str);
	 string_.append(strEnd);
	 GameView::getInstance()->showPopupWindow(string_,2,this,coco_selectselector(PackageItemInfo::callBackPickSure),NULL);
 }

 void PackageItemInfo::callBackPickSure( CCObject * obj )
 {
	 assistEquipStruct assists_={2,0,0,GameView::getInstance()->pacPageView->curPackageItemIndex,2};
	 GameMessageProcessor::sharedMsgProcessor()->sendReq(1601,&assists_);

	 GameMessageProcessor::sharedMsgProcessor()->sendReq(1608,this);
	 GameMessageProcessor::sharedMsgProcessor()->sendReq(1699,this);
 }

 void PackageItemInfo::systhesisEvent( CCObject *pSender )
 {
	 //关闭详细信息界面
	 this->setVisible(false);
	 PackageScene *packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	 if(packageScene != NULL)
	 {
		 packageScene->closeEndedEvent(NULL);
	 }

	 CCActionInterval * action =(CCActionInterval *)CCSequence::create(
		 CCDelayTime::create(0.5f),
		 CCCallFuncND::create(this,callfuncND_selector(PackageItemInfo::showEquipSysthesisUI),(void *)curFolders),
		 CCRemoveSelf::create(),
		 NULL);
	 runAction(action);
 }

 void PackageItemInfo::showEquipSysthesisUI( CCNode *pNode ,void * folderInfo)
 {
	 CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	 FolderInfo * folder = (FolderInfo *)(folderInfo);
	 if (folder)
	 {
		 if(GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI) == NULL)
		 {
			 EquipMentUi * equipui=EquipMentUi::create();
			 equipui->ignoreAnchorPointForPosition(false);
			 equipui->setAnchorPoint(ccp(0.5f,0.5f));
			 equipui->setPosition(ccp(winSize.width/2,winSize.height/2));
			 GameView::getInstance()->getMainUIScene()->addChild(equipui,0,ktagEquipMentUI);

			 equipui->equipTab->setDefaultPanelByIndex(4);
			 equipui->callBackChangeTab( equipui->equipTab);
			 equipui->addMainEquipment(folder);
		 }
	 }
 }

 void PackageItemInfo::setCurEquipIsUseAble( bool useAble )
 {
	 UIButton * btn_ = (UIButton *)m_pUiLayer->getWidgetByTag(112233);
	 if (btn_ != NULL)
	 {
		 if (useAble)
		 {
			 Button_dressOn->setTouchEnable(true);
			 Button_dressOn->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		 }else
		 {
			 Button_dressOn->setTouchEnable(false);
			 Button_dressOn->setTextures("res_ui/new_button_001.png","res_ui/new_button_001.png","");
		 }
	 }
 }

