
#ifndef _H_GETFLOWER_H_
#define _H_GETFLOWER_H_

#include "../../ui/extensions/UIScene.h"
class GetFlower:public UIScene
{
public:
	GetFlower(void);
	~GetFlower(void);

	static GetFlower * create(long long id,std::string name,std::string str);
	bool init(long long id,std::string name,std::string str);

	void onEnter();
	void onExit();

	void callbackPrivate(CCObject * obj);
	void callBackThank(CCObject * obj);

private:
	CCSize winsize;

	long long m_playerId;
	std::string m_name;
	int m_vipLv;
	int m_countryId;
	int m_lv;
	int m_pressionId;



};

#endif