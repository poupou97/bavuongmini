#include "GiveFlower.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/RichTextInput.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../GameView.h"
#include "../extensions/Counter.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../ui/goldstore_ui/GoldStoreUI.h"
#include "../../messageclient/element/CLable.h"
#include "../../ui/extensions/UITab.h"


GiveFlower::GiveFlower(void)
{
	m_giveFlowerNum = 0;
	m_giveFlowerModel = 0;
}

GiveFlower::~GiveFlower(void)
{
}

GiveFlower * GiveFlower::create(long long id,std::string name,int pression,int level,int countryId,int vipLv)
{
	GiveFlower * flower_ =  new GiveFlower();
	if (flower_ && flower_->init(id,name,pression,level,countryId,vipLv))
	{
		flower_->autorelease();
		return flower_;
	}
	CC_SAFE_DELETE(flower_);
	return NULL;
}

bool GiveFlower::init(long long id,std::string name,int pression,int level,int countryId,int vipLv)
{
	if (UIScene::init())
	{
		winsize= CCDirector::sharedDirector()->getVisibleSize();
		
		m_playerId = id;
		m_playerName = name;

		UILayer * m_Layer= UILayer::create();
		m_Layer->ignoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(ccp(0.5f,0.5f));
		m_Layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_Layer->setContentSize(CCSizeMake(800,480));
		this->addChild(m_Layer);

		UIPanel * panel_ = (UIPanel*) GUIReader::shareReader()->widgetFromJsonFile("res_ui/flower_1.json");
		panel_->setAnchorPoint(ccp(0.5f,0.5f));
		panel_->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(panel_);
		
		//flower type type1
// 		checkbox_flowerType1 =(UICheckBox *)UIHelper::seekWidgetByName(panel_,"CheckBox_flower1");
// 		checkbox_flowerType1->setTouchEnable(true);
// 		checkbox_flowerType1->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::UICheckBoxType1));
// 		checkbox_flowerType1->setSelectedState(false);

		checkbox_flowerType1 = UICheckBox::create();
		checkbox_flowerType1->setTouchEnable(true);
		checkbox_flowerType1->loadTextures("res_ui/xiaofangkuang2.png",
			"res_ui/xiaofangkuang2.png",
			"res_ui/red.png",
			"res_ui/red.png",
			"");
		checkbox_flowerType1->setAnchorPoint(ccp(0.5f,0.5f));
		checkbox_flowerType1->addEventListenerCheckBox(this, checkboxselectedeventselector(GiveFlower::UICheckBoxType1));
		checkbox_flowerType1->setPosition(ccp(66,298));
		checkbox_flowerType1->setZOrder(10);
		panel_->addChild(checkbox_flowerType1);
		checkbox_flowerType1->setSelectedState(false);

		UIImageView * image_flowerType1 = UIImageView::create();
		image_flowerType1->setTexture("res_ui/props_icon/flower_lily.png");
		image_flowerType1->setAnchorPoint(ccp(0.5f,0.5f));
		image_flowerType1->setPosition(ccp(103,300));//290
		image_flowerType1->setZOrder(10);
		panel_->addChild(image_flowerType1);
		//47 93 13
		UILabel * label_flower1 = UILabel::create();
		label_flower1->setText(StringDataManager::getString("Flower_Type_baihe"));
		label_flower1->setAnchorPoint(ccp(0.5f,0.5f));
		label_flower1->setPosition(ccp(image_flowerType1->getPosition().x,
									image_flowerType1->getPosition().y - image_flowerType1->getContentSize().height/2 - label_flower1->getContentSize().height/2));
		label_flower1->setFontSize(18);
		label_flower1->setColor(ccc3(47,93,13));
		label_flower1->setZOrder(10);
		panel_->addChild(label_flower1);

// 		UIButton * btn_flowerType1 =(UIButton *)UIHelper::seekWidgetByName(panel_,"Button_flower1");
// 		btn_flowerType1->setTouchEnable(true);
// 		btn_flowerType1->setPressedActionEnabled(true);
// 		btn_flowerType1->addReleaseEvent(this,coco_releaseselector(GiveFlower::callBackFlowerType));
		//type 2
// 		checkbox_flowerType2 =(UICheckBox *)UIHelper::seekWidgetByName(panel_,"CheckBox_flower2");
// 		checkbox_flowerType2->setTouchEnable(true);
// 		checkbox_flowerType2->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::UICheckBoxType2));
// 		checkbox_flowerType2->setSelectedState(false);

		checkbox_flowerType2 = UICheckBox::create();
		checkbox_flowerType2->setTouchEnable(true);
		checkbox_flowerType2->loadTextures("res_ui/xiaofangkuang2.png",
			"res_ui/xiaofangkuang2.png",
			"res_ui/red.png",
			"res_ui/red.png",
			"");
		checkbox_flowerType2->setAnchorPoint(ccp(0.5f,0.5f));
		checkbox_flowerType2->addEventListenerCheckBox(this, checkboxselectedeventselector(GiveFlower::UICheckBoxType2));
		checkbox_flowerType2->setPosition(ccp(156,298));
		checkbox_flowerType2->setZOrder(10);
		panel_->addChild(checkbox_flowerType2);
		checkbox_flowerType2->setSelectedState(false);

		UIImageView * image_flowerType2 = UIImageView::create();
		image_flowerType2->setTexture("res_ui/props_icon/flower_pink.png");
		image_flowerType2->setAnchorPoint(ccp(0.5f,0.5f));
		image_flowerType2->setZOrder(10);
		image_flowerType2->setPosition(ccp(193,300));
		panel_->addChild(image_flowerType2);

		UILabel * label_flower2 = UILabel::create();
		label_flower2->setText(StringDataManager::getString("Flower_Type_fenmeigui"));
		label_flower2->setAnchorPoint(ccp(0.5f,0.5f));
		label_flower2->setPosition(ccp(image_flowerType2->getPosition().x,
			image_flowerType2->getPosition().y - image_flowerType2->getContentSize().height/2 - label_flower2->getContentSize().height/2));
		label_flower2->setFontSize(18);
		label_flower2->setColor(ccc3(47,93,13));
		label_flower2->setZOrder(10);
		panel_->addChild(label_flower2);

// 		UIButton * btn_flowerType2 =(UIButton *)UIHelper::seekWidgetByName(panel_,"Button_flower2");
// 		btn_flowerType2->setTouchEnable(true);
// 		btn_flowerType2->setPressedActionEnabled(true);
// 		btn_flowerType2->addReleaseEvent(this,coco_releaseselector(GiveFlower::callBackFlowerType));
		//type3
// 		checkbox_flowerType3 =(UICheckBox *)UIHelper::seekWidgetByName(panel_,"CheckBox_flower3");
// 		checkbox_flowerType3->setTouchEnable(true);
// 		checkbox_flowerType3->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::UICheckBoxType3));
// 		checkbox_flowerType3->setSelectedState(false);
		
		checkbox_flowerType3 = UICheckBox::create();
		checkbox_flowerType3->setTouchEnable(true);
		checkbox_flowerType3->loadTextures("res_ui/xiaofangkuang2.png",
			"res_ui/xiaofangkuang2.png",
			"res_ui/red.png",
			"res_ui/red.png",
			"");
		checkbox_flowerType3->setAnchorPoint(ccp(0.5f,0.5f));
		checkbox_flowerType3->addEventListenerCheckBox(this, checkboxselectedeventselector(GiveFlower::UICheckBoxType3));
		checkbox_flowerType3->setPosition(ccp(246,298));
		checkbox_flowerType3->setZOrder(10);
		panel_->addChild(checkbox_flowerType3);
		checkbox_flowerType3->setSelectedState(false);

		UIImageView * image_flowerType3 = UIImageView::create();
		image_flowerType3->setTexture("res_ui/props_icon/flower_red.png");
		image_flowerType3->setAnchorPoint(ccp(0.5f,0.5f));
		image_flowerType3->setPosition(ccp(283,300));
		image_flowerType3->setZOrder(10);
		panel_->addChild(image_flowerType3);

		UILabel * label_flower3 = UILabel::create();
		label_flower3->setText(StringDataManager::getString("Flower_Type_hongmeigui"));
		label_flower3->setAnchorPoint(ccp(0.5f,0.5f));
		label_flower3->setPosition(ccp(image_flowerType3->getPosition().x,
										image_flowerType3->getPosition().y - image_flowerType3->getContentSize().height/2 - label_flower3->getContentSize().height/2));
		label_flower3->setFontSize(18);
		label_flower3->setColor(ccc3(47,93,13));
		label_flower3->setZOrder(10);
		panel_->addChild(label_flower3);
// 		UIButton * btn_flowerType3 =(UIButton *)UIHelper::seekWidgetByName(panel_,"Button_flower3");
// 		btn_flowerType3->setTouchEnable(true);
// 		btn_flowerType3->setPressedActionEnabled(true);
// 		btn_flowerType3->addReleaseEvent(this,coco_releaseselector(GiveFlower::callBackFlowerType));
		//Button_number_1
		checkbox_num1 =(UICheckBox *)UIHelper::seekWidgetByName(panel_,"CheckBox_select1");
		checkbox_num1->setTouchEnable(true);
		checkbox_num1->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::UICheckBoxNum1));
		checkbox_num1->setSelectedState(false);

// 		UIButton * btn_flowerNum1 =(UIButton *)UIHelper::seekWidgetByName(panel_,"Button_select1");
// 		btn_flowerNum1->setTouchEnable(true);
// 		btn_flowerNum1->setPressedActionEnabled(true);
// 		btn_flowerNum1->addReleaseEvent(this,coco_releaseselector(GiveFlower::callBackModelFlowerNum));
		// num 11
		checkbox_num11 =(UICheckBox *)UIHelper::seekWidgetByName(panel_,"CheckBox_select11");
		checkbox_num11->setTouchEnable(true);
		checkbox_num11->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::UICheckBoxNum11));
		checkbox_num11->setSelectedState(false);

// 		UIButton * btn_flowerNum11 =(UIButton *)UIHelper::seekWidgetByName(panel_,"Button_select11");
// 		btn_flowerNum11->setTouchEnable(true);
// 		btn_flowerNum11->setPressedActionEnabled(true);
// 		btn_flowerNum11->addReleaseEvent(this,coco_releaseselector(GiveFlower::callBackModelFlowerNum));
 		//num 99
		checkbox_num99 =(UICheckBox *)UIHelper::seekWidgetByName(panel_,"CheckBox_select99");
		checkbox_num99->setTouchEnable(true);
		checkbox_num99->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::UICheckBoxNum99));
		checkbox_num99->setSelectedState(false);

// 		UIButton * btn_flowerNum99 =(UIButton *)UIHelper::seekWidgetByName(panel_,"Button_select99");
// 		btn_flowerNum99->setTouchEnable(true);
// 		btn_flowerNum99->setPressedActionEnabled(true);
// 		btn_flowerNum99->addReleaseEvent(this,coco_releaseselector(GiveFlower::callBackModelFlowerNum));
		//num 365
		checkbox_num365 =(UICheckBox *)UIHelper::seekWidgetByName(panel_,"CheckBox_select365");
		checkbox_num365->setTouchEnable(true);
		checkbox_num365->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::UICheckBoxNum365));
		checkbox_num365->setSelectedState(false);

// 		UIButton * btn_flowerNum365 =(UIButton *)UIHelper::seekWidgetByName(panel_,"Button_select365");
// 		btn_flowerNum365->setTouchEnable(true);
// 		btn_flowerNum365->setPressedActionEnabled(true);
// 		btn_flowerNum365->addReleaseEvent(this,coco_releaseselector(GiveFlower::callBackModelFlowerNum));
		//num 999
		checkbox_num999 =(UICheckBox *)UIHelper::seekWidgetByName(panel_,"CheckBox_select999");
		checkbox_num999->setTouchEnable(true);
		checkbox_num999->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::UICheckBoxNum999));
		checkbox_num999->setSelectedState(false);

// 		UIButton * btn_flowerNum999 =(UIButton *)UIHelper::seekWidgetByName(panel_,"Button_select999");
// 		btn_flowerNum999->setTouchEnable(true);
// 		btn_flowerNum999->setPressedActionEnabled(true);
// 		btn_flowerNum999->addReleaseEvent(this,coco_releaseselector(GiveFlower::callBackModelFlowerNum));
		//self define//
		checkbox_Define =(UICheckBox *)UIHelper::seekWidgetByName(panel_,"CheckBox_selfDefine");
		checkbox_Define->setTouchEnable(true);
		checkbox_Define->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::UIcheckBoxDefineNum));
		checkbox_Define->setSelectedState(false);

// 		UIButton * btn_Define =(UIButton *)UIHelper::seekWidgetByName(panel_,"Button_selfDefine");
// 		btn_Define->setTouchEnable(true);
// 		btn_Define->setPressedActionEnabled(true);
// 		btn_Define->addReleaseEvent(this,coco_releaseselector(GiveFlower::callBackModelFlowerNum));

		//input flower number 
		UIButton * btn_SelectNum =(UIButton *)UIHelper::seekWidgetByName(panel_,"Button_Counter");
		btn_SelectNum->setTouchEnable(true);
		//btn_SelectNum->setPressedActionEnabled(true);
		btn_SelectNum->addReleaseEvent(this,coco_releaseselector(GiveFlower::callBackDefineFlowerNum));

		label_showFlowerNum = (UILabel *)UIHelper::seekWidgetByName(panel_,"Label_selectNum");
		label_showFlowerNum->setText("0");

		//player icon
		std::string playerIcon_;
		if (pression > 0 && pression <5)
		{
			playerIcon_ = BasePlayer::getProfessionIconByIndex(pression);

		}else
		{
			playerIcon_ = "";
		}

		UIImageView * imagePlayerIcon = (UIImageView *)UIHelper::seekWidgetByName(panel_,"ImageView_profession");
		imagePlayerIcon->setTexture(playerIcon_.c_str());

		//
		std::string playerIcon_headHero = BasePlayer::getHalfBodyPathByProfession(pression);
		UIImageView * imagePlayerHeadHeroIcon = (UIImageView *)UIHelper::seekWidgetByName(panel_,"ImageView_player");
		imagePlayerHeadHeroIcon->setTexture(playerIcon_headHero.c_str());
		

		//player name
		UILabel * label_playerName = (UILabel *)UIHelper::seekWidgetByName(panel_,"Label_name");
		label_playerName->setText(m_playerName.c_str());

		UIImageView * image_country = (UIImageView *)UIHelper::seekWidgetByName(panel_,"ImageView_country");
		if (countryId > 0 && countryId <6)
		{
			std::string countryIdName = "country_";
			char id[2];
			sprintf(id, "%d", countryId);
			countryIdName.append(id);
			std::string iconPathName = "res_ui/country_icon/";
			iconPathName.append(countryIdName);
			iconPathName.append(".png");

			image_country->setTexture(countryIdName.c_str());
			image_country->setPosition(ccp(label_playerName->getPosition().x -label_playerName->getContentSize().width/2 - image_country->getContentSize().width/2*0.9f,
							label_playerName->getPosition().y));
		}

		if (vipLv > 0)
		{
			UIImageView * image_vip = (UIImageView *)UIHelper::seekWidgetByName(panel_,"ImageView_vip");
			image_vip->setTexture("gamescene_state/zhujiemian3/zhujuetouxiang/vip1.png");
			image_vip->setPosition(ccp(label_playerName->getPosition().x +label_playerName->getContentSize().width/2,label_playerName->getPosition().y -  image_vip->getContentSize().height/2));

			UILabelBMFont *lbt_vipLevel = UILabelBMFont::create();
			char s_level[10];
			sprintf(s_level,"%d",vipLv);
			lbt_vipLevel->setText(s_level);
			lbt_vipLevel->setFntFile("res_ui/font/ziti_3.fnt");
			lbt_vipLevel->setScale(0.7f);
			lbt_vipLevel->setAnchorPoint(ccp(0.5f,0.5f));
			//lbt_vipLevel->setPosition(ccp(15,-5));
			lbt_vipLevel->setPosition(ccp(image_vip->getContentSize().width,0));
			image_vip->addChild(lbt_vipLevel);
		}

		char levelStr[20];
		sprintf(levelStr,"%d",level);
		std::string stringLevel_ = "LV.";
		stringLevel_.append(levelStr);
		
		UILabel * label_level = (UILabel *)UIHelper::seekWidgetByName(panel_,"Label_level");
		label_level->setText(stringLevel_.c_str());

		//input flowerSaid (601,209)
		inputFlowerSaid=new RichTextInputBox();
		inputFlowerSaid->setMultiLinesMode(true);
		inputFlowerSaid->setInputBoxWidth(250);
		inputFlowerSaid->setInputBoxHeight(80);
		inputFlowerSaid->setDirection(RichTextInputBox::kCCRichInputDirectionVertical);
		inputFlowerSaid->setAnchorPoint(ccp(0,0));
		inputFlowerSaid->setScale(0.9f);
		//inputFlowerSaid->setPosition(ccp(480,280));
		inputFlowerSaid->setPosition(ccp(440,157));
		inputFlowerSaid->setCharLimit(20);
		m_Layer->addChild(inputFlowerSaid);
		inputFlowerSaid->release();

		//
		checkbox_public =(UICheckBox *)UIHelper::seekWidgetByName(panel_,"CheckBox_public");
		checkbox_public->setTouchEnable(true);
		checkbox_public->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::setModelPublic));
		checkbox_public->setSelectedState(true);

		checkbox_private =(UICheckBox *)UIHelper::seekWidgetByName(panel_,"CheckBox_private");
		checkbox_private->setTouchEnable(true);
		checkbox_private->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::setModelPrivate));
		checkbox_private->setSelectedState(false);

		//give flower
		UIButton * btn_giveFlower =(UIButton *)UIHelper::seekWidgetByName(panel_,"Button_send");
		btn_giveFlower->setTouchEnable(true);
		btn_giveFlower->setPressedActionEnabled(true);
		btn_giveFlower->addReleaseEvent(this,coco_releaseselector(GiveFlower::callBackGiveFlower));

		//close ui
		UIButton * buttonExit =(UIButton *)UIHelper::seekWidgetByName(panel_,"Button_close");
		buttonExit->setTouchEnable(true);
		buttonExit->setPressedActionEnabled(true);
		buttonExit->addReleaseEvent(this,coco_releaseselector(GiveFlower::callBackExit));

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void GiveFlower::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GiveFlower::onExit()
{
	UIScene::onExit();
}

void GiveFlower::callBackExit( CCObject * obj )
{
	this->closeAnim();
}

void GiveFlower::callBackGiveFlower( CCObject * obj )
{
	if (strlen(getFlowerName().c_str()) <= 0)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("giveflowerui_selectFlowerType"));
		return;
	}

	if (getFlowerNum() <= 0)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("giveflowerui_selectFlowerNum"));
		return;
	}
	//flower num is enough
	int hasFlowerNum_ = 0;
	for (int i =0;i<GameView::getInstance()->AllPacItem.size();i++)
	{
		
		std::string goodsId_ = GameView::getInstance()->AllPacItem.at(i)->goods().id();
		if (strcmp(getFlowerName().c_str(),goodsId_.c_str()) ==0)
		{
			hasFlowerNum_+=GameView::getInstance()->AllPacItem.at(i)->quantity();
		}
	}

	if (getFlowerNum() > hasFlowerNum_)
	{
		std::string str_ = StringDataManager::getString("FlowerUi_FlowerIsNotEnough");
		GameView::getInstance()->showPopupWindow(str_,2,this,coco_selectselector(GiveFlower::callBackOpenShop),NULL);
		return;
	}
	
	const char* inputStr = inputFlowerSaid->getInputString();
	if (getFlowerNum() < 999 && inputStr != NULL)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("giveflowerui_selectFlowerAndSaid"));
		return;
	}

	std::string flowerSaid_ = "";
	if (inputStr != NULL)
	{
		flowerSaid_ = inputStr;
	}

	GiveFlowerStruct flowerInfo = {m_playerId,m_playerName,getFlowerNum(),getFlowerName(),getModelType(),flowerSaid_};
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5140,&flowerInfo);
}

void GiveFlower::callBackModelFlowerNum( CCObject * obj )
{
	UIButton * btn = (UIButton *)obj;
	//uibuttontree
	this->setFlowerNum(999);
}

void GiveFlower::setModelPublic( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * check_ = (UICheckBox *)obj;
	
	if (check_->getSelectedState())
	{
		setModelType(0);
		checkbox_private->setSelectedState(false);
	}else
	{
		setModelType(1);
		checkbox_private->setSelectedState(true);
	}
}

void GiveFlower::setModelPrivate( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * check_ = (UICheckBox *)obj;

	if (check_->getSelectedState())
	{
		setModelType(1);
		checkbox_public->setSelectedState(false);
	}else
	{
		setModelType(0);
		checkbox_public->setSelectedState(true);
	}
}

void GiveFlower::callBackFlowerType( CCObject * obj )
{
	//uibuttontree
	setFlowerName("flower type");
}

void GiveFlower::callBackDefineFlowerNum( CCObject * obj )
{
	//
	if (checkbox_Define->getSelectedState() == false)
	{
		checkbox_Define->setSelectedState(true);
		setFlowerNum(0);
		checkbox_num1->setSelectedState(false);
		checkbox_num11->setSelectedState(false);
		checkbox_num99->setSelectedState(false);
		checkbox_num365->setSelectedState(false);
		checkbox_num999->setSelectedState(false);
	}
	
	GameView::getInstance()->showCounter(this,callfuncO_selector(GiveFlower::showFlowerNum));
}

void GiveFlower::showFlowerNum( CCObject * obj )
{
	Counter * couter_ = (Counter *)obj;
	int num_ = couter_->getInputNum();
	
	this->setFlowerNum(num_);

	char str[20];
	sprintf(str,"%d",getFlowerNum());

	label_showFlowerNum->setText(str);
}

void GiveFlower::setModelType( int type )
{
	m_giveFlowerModel = type;
}

int GiveFlower::getModelType()
{
	return m_giveFlowerModel;
}

void GiveFlower::setFlowerNum( int num )
{
	m_giveFlowerNum = num;
}

int GiveFlower::getFlowerNum()
{
	return m_giveFlowerNum;
}

void GiveFlower::setFlowerName( std::string flowerName )
{
	m_flowerName = flowerName;
}

std::string GiveFlower::getFlowerName()
{
	return m_flowerName;
}

void GiveFlower::UICheckBoxNum1( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * check_ = (UICheckBox *)obj;
	if (check_->getSelectedState())
	{
		setFlowerNum(1);
		checkbox_num1->setSelectedState(true);
		checkbox_num11->setSelectedState(false);
		checkbox_num99->setSelectedState(false);
		checkbox_num365->setSelectedState(false);
		checkbox_num999->setSelectedState(false);
		checkbox_Define->setSelectedState(false);
	}else
	{
		setAllUIcheckNumBoxIsNull();
	}

	label_showFlowerNum->setText("0");
}

void GiveFlower::UICheckBoxNum11( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * check_ = (UICheckBox *)obj;
	if (check_->getSelectedState())
	{
		setFlowerNum(11);
		checkbox_num1->setSelectedState(false);
		checkbox_num11->setSelectedState(true);
		checkbox_num99->setSelectedState(false);
		checkbox_num365->setSelectedState(false);
		checkbox_num999->setSelectedState(false);
		checkbox_Define->setSelectedState(false);
	}else
	{
		setAllUIcheckNumBoxIsNull();
	}

	label_showFlowerNum->setText("0");
}

void GiveFlower::UICheckBoxNum99( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * check_ = (UICheckBox *)obj;
	if (check_->getSelectedState())
	{
		setFlowerNum(99);
		checkbox_num1->setSelectedState(false);
		checkbox_num11->setSelectedState(false);
		checkbox_num99->setSelectedState(true);
		checkbox_num365->setSelectedState(false);
		checkbox_num999->setSelectedState(false);
		checkbox_Define->setSelectedState(false);
	}else
	{
		setAllUIcheckNumBoxIsNull();
	}

	label_showFlowerNum->setText("0");
}

void GiveFlower::UICheckBoxNum365( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * check_ = (UICheckBox *)obj;
	if (check_->getSelectedState())
	{
		setFlowerNum(365);
		checkbox_num1->setSelectedState(false);
		checkbox_num11->setSelectedState(false);
		checkbox_num99->setSelectedState(false);
		checkbox_num365->setSelectedState(true);
		checkbox_num999->setSelectedState(false);
		checkbox_Define->setSelectedState(false);
	}else
	{
		setAllUIcheckNumBoxIsNull();
	}

	label_showFlowerNum->setText("0");
}

void GiveFlower::UICheckBoxNum999( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * check_ = (UICheckBox *)obj;
	if (check_->getSelectedState())
	{
		setFlowerNum(999);
		checkbox_num1->setSelectedState(false);
		checkbox_num11->setSelectedState(false);
		checkbox_num99->setSelectedState(false);
		checkbox_num365->setSelectedState(false);
		checkbox_num999->setSelectedState(true);
		checkbox_Define->setSelectedState(false);
	}else
	{
		setAllUIcheckNumBoxIsNull();
	}

	label_showFlowerNum->setText("0");
}

void GiveFlower::UIcheckBoxDefineNum( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * check_ = (UICheckBox *)obj;
	if (check_->getSelectedState())
	{
		setFlowerNum(0);
		checkbox_num1->setSelectedState(false);
		checkbox_num11->setSelectedState(false);
		checkbox_num99->setSelectedState(false);
		checkbox_num365->setSelectedState(false);
		checkbox_num999->setSelectedState(false);

		//
		callBackDefineFlowerNum(NULL);
	}else
	{
		setAllUIcheckNumBoxIsNull();
	}
}

void GiveFlower::UICheckBoxType1( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * check_ = (UICheckBox *)obj;
	if (check_->getSelectedState())
	{
		setFlowerName(FLOWERTYPE_1);
		checkbox_flowerType1->setSelectedState(true);
		checkbox_flowerType2->setSelectedState(false);
		checkbox_flowerType3->setSelectedState(false);
	}else
	{
		setAllUIcheckTypeBoxIsNull();
	}
}

void GiveFlower::UICheckBoxType2( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * check_ = (UICheckBox *)obj;
	if (check_->getSelectedState())
	{
		setFlowerName(FLOWERTYPE_2);
		checkbox_flowerType1->setSelectedState(false);
		checkbox_flowerType2->setSelectedState(true);
		checkbox_flowerType3->setSelectedState(false);
	}else
	{
		setAllUIcheckTypeBoxIsNull();
	}
}

void GiveFlower::UICheckBoxType3( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * check_ = (UICheckBox *)obj;
	if (check_->getSelectedState())
	{
		setFlowerName(FLOWERTYPE_3);
		checkbox_flowerType1->setSelectedState(false);
		checkbox_flowerType2->setSelectedState(false);
		checkbox_flowerType3->setSelectedState(true);
	}else
	{
		setAllUIcheckTypeBoxIsNull();
	}
}

void GiveFlower::setAllUIcheckTypeBoxIsNull()
{
	setFlowerName("");
	checkbox_flowerType1->setSelectedState(false);
	checkbox_flowerType2->setSelectedState(false);
	checkbox_flowerType3->setSelectedState(false);
}

void GiveFlower::setAllUIcheckNumBoxIsNull()
{
	setFlowerNum(0);

	checkbox_num1->setSelectedState(false);
	checkbox_num11->setSelectedState(false);
	checkbox_num99->setSelectedState(false);
	checkbox_num365->setSelectedState(false);
	checkbox_num999->setSelectedState(false);

	checkbox_Define->setSelectedState(false);
}

void GiveFlower::callBackOpenShop( CCObject * obj )
{
	GoldStoreUI *goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
	if(goldStoreUI == NULL)
	{
		goldStoreUI = GoldStoreUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI,0,kTagGoldStoreUI);

		goldStoreUI->ignoreAnchorPointForPosition(false);
		goldStoreUI->setAnchorPoint(ccp(0.5f, 0.5f));
		goldStoreUI->setPosition(ccp(winsize.width/2, winsize.height/2));

		for (int i=0;i <GameView::getInstance()->goldStoreList.size();i++)
		{
			if (GameView::getInstance()->goldStoreList.at(i)->id() == GoldStoreUI::type_aid)
			{
				goldStoreUI->getTab_function()->setDefaultPanelByIndex(i);
				goldStoreUI->IndexChangedEvent(goldStoreUI->getTab_function());
			}
		}
	}
}
