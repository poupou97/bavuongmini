
#ifndef CHALLENGEROUND_CHALLENGEROUND_H
#define CHALLENGEROUND_CHALLENGEROUND_H

#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
class ChallengeRoundUi:public UIScene,public cocos2d::extension::CCTableViewDataSource,public cocos2d::extension::CCTableViewDelegate
{
public:
	ChallengeRoundUi(void);
	~ChallengeRoundUi(void);

	static ChallengeRoundUi * create();
	bool init();

	void onEnter();
	void onExit();
	void callBackCloseUi(CCObject *obj);

	void tabIndexChangedEvent(CCObject * obj);

	void setCopyClazz(int value_);
	int getCopyClazz();

	void setCopyCostResume(int value_);
	int getCopyCostResume();

	static std::string getClazzName(int clazz_);
	static std::string getClazzLevel(int clazz);
public:
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	// default implements are used to call script callback if exist
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);


public:
	//��ѧ
	int mTutorialScriptInstanceId;
	int mTutorialIndex;
	//��ѧ
	virtual void registerScriptCommand(int scriptId);

	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

private:
	CCSize winsize;
	int m_copyClazz;
	int m_copyResume;
};

#endif