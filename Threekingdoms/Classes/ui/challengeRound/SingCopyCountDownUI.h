
#ifndef _CHALLOUND_SINGCOPYCOUNTDOWN_H_
#define _CHALLOUND_SINGCOPYCOUNTDOWN_H_

#include "../extensions/UIScene.h"

class SingCopyCountDownUI : public UIScene
{
public:
	SingCopyCountDownUI();
	~SingCopyCountDownUI();

	static SingCopyCountDownUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);

	void showStarCountTime();
	virtual void update(float dt);

private:
	int m_nRemainTime;
	//UILabel *l_countDown;
	UIImageView * imageView_countDown;
	CCSize winsize;
};

#endif

