#include "SingCopyCountDownUI.h"
#include "../../utils/GameUtils.h"
#include "SingCopyInstance.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../utils/StaticDataManager.h"

SingCopyCountDownUI::SingCopyCountDownUI():
m_nRemainTime(3000)
{
	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();	
	MissionAndTeam * missionAndTeam = (MissionAndTeam *)mainscene->getChildByTag(kTagMissionAndTeam);
	if (missionAndTeam)
	{
		missionAndTeam->setSingCopy_useTime(0);
	}
}


SingCopyCountDownUI::~SingCopyCountDownUI()
{
}

SingCopyCountDownUI * SingCopyCountDownUI::create()
{
	SingCopyCountDownUI * offLineArenaCountDownUI = new SingCopyCountDownUI();
	if (offLineArenaCountDownUI && offLineArenaCountDownUI->init())
	{
		offLineArenaCountDownUI->autorelease();
		return offLineArenaCountDownUI;
	}
	CC_SAFE_DELETE(offLineArenaCountDownUI);
	return NULL;
}

bool SingCopyCountDownUI::init()
{
	if (UIScene::init())
	{
		winsize = CCDirector::sharedDirector()->getVisibleSize();

		const char * str_ = StringDataManager::getString("singleCopy_curLevelLabelBmFont");
		char string_[50];
		sprintf(string_,str_,SingCopyInstance::getCurSingCopyLevel());

		UIImageView * background_ =UIImageView::create();
		background_->setTexture("res_ui/zhezhao80.png");
		background_->setAnchorPoint(ccp(0.5f,0.5f));
		background_->setScale9Enable(true);
		background_->setScale9Size(CCSizeMake(winsize.width,80));
		//background_->setCapInsets(CCRect(50,50,1,1));
		background_->setPosition(ccp(-background_->getContentSize().width/2,winsize.height*0.8f));
 		m_pUiLayer->addWidget(background_);
 		background_->runAction(CCMoveTo::create(0.5f,ccp(winsize.width/2,winsize.height*0.8f)));

		UILabelBMFont * level_BMFont = UILabelBMFont::create();
		level_BMFont->setAnchorPoint(ccp(0.5f,0.5f));
		level_BMFont->setPosition(ccp(winsize.width+level_BMFont->getContentSize().width ,winsize.height*0.8f));
		level_BMFont->setText(string_);
		level_BMFont->setScale(1.2f);
		level_BMFont->setFntFile("res_ui/font/ziti_1.fnt");
		m_pUiLayer->addWidget(level_BMFont);

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCShow::create(),
			CCMoveTo::create(0.5f,ccp(winsize.width/2,winsize.height*0.8f)),
			CCDelayTime::create(0.5f),
			CCRemoveSelf::create(),
			CCCallFunc::create(this,callfunc_selector(SingCopyCountDownUI::showStarCountTime)),
			NULL);

		level_BMFont->runAction(action);
		

		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void SingCopyCountDownUI::onEnter()
{
	UIScene::onEnter();
}

void SingCopyCountDownUI::onExit()
{
	UIScene::onExit();
}

bool SingCopyCountDownUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void SingCopyCountDownUI::update( float dt )
{
	m_nRemainTime -= 1000;
	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();	
	MissionAndTeam * missionAndTeam = (MissionAndTeam *)mainscene->getChildByTag(kTagMissionAndTeam);
	if (missionAndTeam)
	{
		missionAndTeam->setSingCopy_useTime(0);
	}
	if (m_nRemainTime <= 0)
	{
		m_nRemainTime = 0;
		missionAndTeam->starRunUpdate();
		this->removeFromParent();
	}

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	std::string str_path = "res_ui/font/";
	str_path.append(s_remainTime);
	str_path.append(".png");
	imageView_countDown->setTexture(str_path.c_str());
}

void SingCopyCountDownUI::showStarCountTime()
{
	imageView_countDown = UIImageView::create();
	imageView_countDown->setTexture("res_ui/font/3.png");
	imageView_countDown->setAnchorPoint(ccp(0.5f,0.5f));
	imageView_countDown->setPosition(ccp(winsize.width/2,winsize.height*0.8f));
	m_pUiLayer->addWidget(imageView_countDown);

	float space = (m_nRemainTime-(GameUtils::millisecondNow() - SingCopyInstance::getStartTime()))*1.0f/m_nRemainTime;
	if (space <=0)
	{
		space = 1;
	}
	this->schedule(schedule_selector(SingCopyCountDownUI::update),space*1.2f);

}
