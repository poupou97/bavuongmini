#include "ShowLevelUi.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CTowerHistory.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "SingleRankListUi.h"
#include "../../gamescene_state/MainScene.h"
#include "RankRewardUi.h"
#include "ChallengeRoundUi.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../extensions/ShowSystemInfo.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "SingCopyInstance.h"
#include "../../gamescene_state/GameStatistics.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../goldstore_ui/GoldStoreUI.h"
#include "../../messageclient/element/CLable.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "utils/StrUtils.h"


ShowLevelUi::ShowLevelUi(void)
{
	m_curPageIndex = 0;
	m_clazz =1;
	m_everyPageNum = 10;
	m_todaymaxLevel = 0;
	m_curLevel = 1;
}


ShowLevelUi::~ShowLevelUi(void)
{
}

ShowLevelUi * ShowLevelUi::create(int clazzIndex)
{
	ShowLevelUi * showLevelui = new ShowLevelUi();
	if (showLevelui && showLevelui->init(clazzIndex))
	{
		showLevelui->autorelease();
		return showLevelui;
	}
	CC_SAFE_DELETE(showLevelui);
	return NULL;
}

bool ShowLevelUi::init(int clazzIndex)
{
	if (UIScene::init())
	{
		winsize= CCDirector::sharedDirector()->getVisibleSize();
		m_clazz = clazzIndex;

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		UILayer * layer_  = UILayer::create();
		layer_->ignoreAnchorPointForPosition(false);
		layer_->setAnchorPoint(ccp(0.5f, 0.5f));
		layer_->setContentSize(CCSizeMake(720,438));
		layer_->setPosition(ccp(winsize.width/2, winsize.height/2));
		this->addChild(layer_);


		UIPanel * mainPanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/guoguanzhanjiang2_1.json");
		mainPanel->setAnchorPoint(ccp(0,0));
		mainPanel->setPosition(ccp(0,0));
		layer_->addWidget(mainPanel);

		UIButton * btn_reward = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_34");
		btn_reward->setTouchEnable(true);
		btn_reward->setPressedActionEnabled(true);
		btn_reward->addReleaseEvent(this,coco_releaseselector(ShowLevelUi::callBackReward));

		UIButton * btn_rank = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_34_0");
		btn_rank->setTouchEnable(true);
		btn_rank->setPressedActionEnabled(true);
		btn_rank->addReleaseEvent(this,coco_releaseselector(ShowLevelUi::callBackRankList));
		
		UIButton * btn_ruleDes = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_34_0_0");
		btn_ruleDes->setTouchEnable(true);
		btn_ruleDes->setPressedActionEnabled(true);
		btn_ruleDes->addReleaseEvent(this,coco_releaseselector(ShowLevelUi::callBackRuleDes));

		UIButton * btn_close = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_close");
		btn_close->setTouchEnable(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(ShowLevelUi::callBackCloseUi));

		std::string levelUI_clazzNameAndlevel = ChallengeRoundUi::getClazzName(m_clazz);
		levelUI_clazzNameAndlevel.append(ChallengeRoundUi::getClazzLevel(m_clazz).c_str());
		UILabelBMFont * labelBmFontlevelClazz = (UILabelBMFont *)UIHelper::seekWidgetByName(mainPanel,"LabelBMFont_60");		
		labelBmFontlevelClazz->setText(levelUI_clazzNameAndlevel.c_str());

		labelTodayMaxLevel = (UILabelBMFont *)UIHelper::seekWidgetByName(mainPanel,"Label_28_0");
		labelTodayMaxUsetime = (UILabelBMFont *)UIHelper::seekWidgetByName(mainPanel,"Label_28_1_0");

		UIButton * btn_phypowerAdd = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_lintili_jiahao");
		btn_phypowerAdd->setTouchEnable(true);
		btn_phypowerAdd->setPressedActionEnabled(true);
		btn_phypowerAdd->addReleaseEvent(this,coco_releaseselector(ShowLevelUi::callBackAddPhyPower));

		int curPhysicalValue = GameView::getInstance()->myplayer->getPhysicalValue();
		int allPhysicalValue = GameView::getInstance()->myplayer->getPhysicalCapacity();


		UIImageView * image_phypowerSp = (UIImageView *)UIHelper::seekWidgetByName(mainPanel,"ImageView_tilitiao_");
		UILabelBMFont* labelBMFont_phyValue = (UILabelBMFont *)UIHelper::seekWidgetByName(mainPanel, "LabelBMFont_tiliValue");

		//CCLabelBMFont* label = CCLabelBMFont::create(str, "res_ui/font/c123.fnt");   // yellow color
		//label->setAnchorPoint(ccp(0.5f,0.0f));
		//label->setPosition(ccp(defender->getPosition().x, defender->getPosition().y + BASEFIGHTER_ROLE_HEIGHT*1/3));
		////label->setPosition(label->getPosition() + SimpleEffectManager::getNumberOffset());
		//label->setVisible(false);
		
		image_phypowerSp->setTextureRect(CCRectMake(0, 0, 
			image_phypowerSp->getContentSize().width * 0.0f,
			image_phypowerSp->getContentSize().height));

		// 体力条逐渐增长的动画效果
		PhysicalBarEffect* pPhysicalBarEffect = PhysicalBarEffect::create(curPhysicalValue, allPhysicalValue, 1.5f, 
			image_phypowerSp, labelBMFont_phyValue);
		this->addChild(pPhysicalBarEffect);

		m_pageView = UIPageView::create();
		m_pageView->setTouchEnable(true);
		m_pageView->setAnchorPoint(ccp(0,0));
		m_pageView->setSize(CCSizeMake(570,257));
		m_pageView->setPosition(ccp(75,100));
		m_pageView->addPageTurningEvent(this,coco_PageView_PageTurning_selector(ShowLevelUi::showCurLevelContent));
		layer_->addWidget(m_pageView);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		//this->setContentSize(CCSizeMake(720,438));
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void ShowLevelUi::onEnter()
{
	this->openAnim();
	UIScene::onEnter();
}

void ShowLevelUi::onExit()
{
	UIScene::onExit();
}

void ShowLevelUi::callBackCloseUi( CCObject *obj )
{
	this->closeAnim();
}

void ShowLevelUi::addPageViewIndex( int pageCount )
{
	int m_levelIndex = 0;
	for (int pageIndex =0;pageIndex <pageCount;pageIndex++)
	{
		UILayout * layout = UILayout::create();
		layout->setSize(m_pageView->getContentSize());
		
		for (int i = 0; i <2; ++i)
		{
			for (int j=0;j<5;j++)
			{
				m_levelIndex++;

				UIImageView * image_sp = UIImageView::create();
				image_sp->setTexture("res_ui/guoguanzhanjiang/pic_di.png");
				image_sp->setAnchorPoint(ccp(0.5f,0.5f));
				image_sp->setPosition(ccp(image_sp->getContentSize().width/2+5 + (image_sp->getContentSize().width+10)*j,
							layout->getContentSize().height/2 + image_sp->getContentSize().height/2 + 5 - (image_sp->getContentSize().height+8)*i));
				layout->addChild(image_sp);

				ccColor3B levelColor=ccc3(255,255,255);
				int m_curLevel_ =  m_levelIndex;
				UIImageView * image_mapIcon = UIImageView::create();
				if (m_curLevel_ <= this->getTodaymaxLevel()+1)
				{
					if (m_curLevel_%5 == 0)
					{
						image_mapIcon->setTexture("res_ui/guoguanzhanjiang/pic_2.png");
					}else
					{
						image_mapIcon->setTexture("res_ui/guoguanzhanjiang/pic_1.png");
					}
					if (m_curLevel_ <= this->getTodaymaxLevel())
					{
						levelColor = ccc3(0,255,6);
					}else
					{
						levelColor = ccc3(255,246,0);
					}

				}else
				{
					if (m_curLevel_%5 == 0)
					{
						image_mapIcon->setTexture("res_ui/guoguanzhanjiang/pic_2off.png");
					}else
					{
						image_mapIcon->setTexture("res_ui/guoguanzhanjiang/pic_1off.png");
					}
				}
				image_mapIcon->setAnchorPoint(ccp(0.5f,0));
				image_mapIcon->setPosition(ccp(0,12 - image_mapIcon->getContentSize().height/2));
				image_sp->addChild(image_mapIcon);

				const char *strings_curLevelBmfont = StringDataManager::getString("singleCopy_curLevelLabelBmFont");
				char levelStr[10];
				sprintf(levelStr,strings_curLevelBmfont,m_curLevel_);
				UILabelBMFont * labellevel_ = UILabelBMFont::create();
				labellevel_->setAnchorPoint(ccp(0.5f,0));
				labellevel_->setFntFile("res_ui/font/ziti_3.fnt");
				labellevel_->setText(levelStr);
				labellevel_->setPosition(ccp(0,labellevel_->getContentSize().height+2));
				image_sp->addChild(labellevel_);

				UIButton * btn_Enter = UIButton::create();
				btn_Enter->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
				btn_Enter->setAnchorPoint(ccp(0.5f,0.5f));
				btn_Enter->setTouchEnable(true);
				btn_Enter->setScale9Enable(true);
				btn_Enter->setScale9Size(CCSizeMake(80,32));
				btn_Enter->setPressedActionEnabled(true);
				btn_Enter->addReleaseEvent(this,coco_releaseselector(ShowLevelUi::enterCopyLevel));
				btn_Enter->setTag(m_levelIndex);
				btn_Enter->setPosition(ccp(0, -image_sp->getContentSize().height/2+btn_Enter->getContentSize().height/2));
				image_sp->addChild(btn_Enter);
				UILabel * label_enter = UILabel::create();
				label_enter->setText(StringDataManager::getString("singleCopy_isEnterLevel"));
				label_enter->setAnchorPoint(ccp(0.5f,0.5f));
				label_enter->setFontSize(16);
				label_enter->setPosition(ccp(0,0));
				btn_Enter->addChild(label_enter);

				UILabel * labelTime_ = UILabel::create();
				labelTime_->setAnchorPoint(ccp(0.5f,0));
				labelTime_->setFontSize(16);
				labelTime_->setTag(m_levelIndex);
				labelTime_->setColor(levelColor);
				
				std::string useTimeStr = "";
				if (singCopyHistoryVector.size() >= m_levelIndex)
				{
					int temp_useTime  = singCopyHistoryVector.at(m_levelIndex - 1)->usetime()/1000;
					useTimeStr.append(RecuriteActionItem::timeFormatToString(temp_useTime));

					UIImageView * image_1 = UIImageView::create();
					image_1->setTexture("res_ui/guoguanzhanjiang/mengban.png");
					image_1->setScale9Enable(true);
					image_1->setScale9Size(CCSizeMake(80,20));
					//image_1->setCapInsets(CCRect(11,12,1,1));
					image_1->setPosition(ccp(0,-10));
					image_sp->addChild(image_1);

					labelTime_->setText(useTimeStr.c_str());
					labelTime_->setPosition(ccp(0,-labelTime_->getContentSize().height));
					image_sp->addChild(labelTime_);
					btn_Enter->setVisible(true);
				}else
				{
					if (m_levelIndex == singCopyHistoryVector.size() +1)
					{
						UIImageView * image_1 = UIImageView::create();
						image_1->setTexture("res_ui/guoguanzhanjiang/mengban.png");
						image_1->setScale9Enable(true);
						image_1->setScale9Size(CCSizeMake(80,20));
						image_1->setCapInsets(CCRect(11,12,1,1));
						image_1->setPosition(ccp(0,-10));//0,-10
						image_sp->addChild(image_1);

						const char *strings = StringDataManager::getString("singleCopy_isnotOpen");
						useTimeStr.append(strings);

						labelTime_->setText(useTimeStr.c_str());
						labelTime_->setPosition(ccp(0,-labelTime_->getContentSize().height));
						image_sp->addChild(labelTime_);
						btn_Enter->setVisible(true);
					}else
					{
						UIImageView * image_useTime = UIImageView::create();
						image_useTime->setTexture("res_ui/LV4_diaa.png");
						image_useTime->setScale9Enable(true);
						image_useTime->setScale9Size(CCSizeMake(80,25));
						image_useTime->setCapInsets(CCRect(11,12,1,1));
						image_useTime->setPosition(ccp(0,-image_sp->getContentSize().height/2+image_useTime->getContentSize().height/2+10));//7
						image_sp->addChild(image_useTime);

						const char *strings = StringDataManager::getString("singleCopy_isOpenUp");
						useTimeStr.append(strings);

						labelTime_->setText(useTimeStr.c_str());
						labelTime_->setPosition(ccp(0, -labelTime_->getContentSize().height/2 ));
						image_useTime->addChild(labelTime_);

						btn_Enter->setVisible(false);
					}
				}
			}
		}        
		m_pageView->addPage(layout);
	}
}

void ShowLevelUi::refreshPageDate()
{
}


void ShowLevelUi::showCurLevelContent( CCObject * obj )
{
}

void ShowLevelUi::callBackReward( CCObject * obj )
{
	//ActiveUI * activeui = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	ChallengeRoundUi * challengeui_ = (ChallengeRoundUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);
	
	RankRewardUi * ranklist_ = RankRewardUi::create(challengeui_->getCopyClazz());
	ranklist_->ignoreAnchorPointForPosition(false);
	ranklist_->setAnchorPoint(ccp(0.5f,0.5f));
	ranklist_->setPosition(ccp(winsize.width/2,winsize.height/2));
	ranklist_->setTag(ktagSingCopyReward);
	GameView::getInstance()->getMainUIScene()->addChild(ranklist_);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1905,(void *)challengeui_->getCopyClazz());
}

void ShowLevelUi::callBackRankList( CCObject * obj )
{
	SingleRankListUi * ranklist_ = SingleRankListUi::create();
	ranklist_->ignoreAnchorPointForPosition(false);
	ranklist_->setAnchorPoint(ccp(0.5f,0.5f));
	ranklist_->setPosition(ccp(winsize.width/2,winsize.height/2));
	ranklist_->setTag(ktagSingCopyRank);
	GameView::getInstance()->getMainUIScene()->addChild(ranklist_);

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1904,ranklist_);
}

void ShowLevelUi::callBackRuleDes( CCObject * obj )
{
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(8);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end())
	{
		return ;
	}
	else
	{
		if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		{
			ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[8].c_str());
			GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			showSystemInfo->ignoreAnchorPointForPosition(false);
			showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			showSystemInfo->setPosition(ccp(winsize.width/2, winsize.height/2));
		}
	}
}

void ShowLevelUi::callBackAddPhyPower( CCObject * obj )
{
	GoldStoreUI *goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
	if(goldStoreUI == NULL)
	{		
		goldStoreUI = GoldStoreUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI,0,kTagGoldStoreUI);
		goldStoreUI->ignoreAnchorPointForPosition(false);
		goldStoreUI->setAnchorPoint(ccp(0.5f, 0.5f));
		goldStoreUI->setPosition(ccp(winsize.width/2, winsize.height/2));

		for (int i=0;i <GameView::getInstance()->goldStoreList.size();i++)
		{

			if (GameView::getInstance()->goldStoreList.at(i)->id() == GoldStoreUI::type_phypower)
			{
				goldStoreUI->getTab_function()->setDefaultPanelByIndex(i);
				goldStoreUI->IndexChangedEvent(goldStoreUI->getTab_function());
			}
		}
	}
}


void ShowLevelUi::setCurPageIndex( int index )
{
	m_curPageIndex = index;
}

int ShowLevelUi::getCurpageIndex()
{
	return m_curPageIndex;
}

void ShowLevelUi::setTodaymaxlevel( int level_ )
{
	m_todaymaxLevel = level_;
}

int ShowLevelUi::getTodaymaxLevel()
{
	return m_todaymaxLevel;
}

void ShowLevelUi::enterCopyLevel( CCObject * obj )
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(this);


	UIButton * btn = (UIButton *)obj;
	this->setCurLevel(btn->getTag());

	int curPage_ = m_pageView->getCurPageIndex();
	int m_curLevel = this->getCurLevel();

	if (singCopyHistoryVector.size()+1 >= m_curLevel)
	{

		ChallengeRoundUi * challengeui_ = (ChallengeRoundUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);
		int costResume = challengeui_->getCopyCostResume();
		char resumeAndLevelStr[100];
		const char *strings = StringDataManager::getString("singleCopy_isCostResumeEnterLevel");
		sprintf(resumeAndLevelStr,strings,costResume);

		std::string temp_str = "";
		temp_str.append(resumeAndLevelStr);
		std::string temp_clazz = "";
		switch(m_clazz)
		{
		case 1:
			{
				std::string str_1 = StrUtils::applyColor(StringDataManager::getString("singCopy_firstClazzName_chushiniudao"),ccc3(243,252,2));
				temp_clazz.append(str_1.c_str());
			}break;
		case 2:
			{
				std::string str_2 = StrUtils::applyColor(StringDataManager::getString("singCopy_secondClazzName_xiaoyoumingqi"),ccc3(243,252,2));
				temp_clazz.append(str_2.c_str());
			}break;
		case 3:
			{
				std::string str_3 = StrUtils::applyColor(StringDataManager::getString("singCopy_thirdClazzName_jiangongliye"),ccc3(243,252,2));
				temp_clazz.append(str_3);
			}break;
		}
		temp_str.append(temp_clazz);

		char lev_str[100];
		const char * strings_lev_str = StringDataManager::getString("singleCopy_isCostResumeEnterLevel_append");
		sprintf(lev_str,strings_lev_str,this->getCurLevel());
		std::string strLevel_temp = StrUtils::applyColor(lev_str,ccc3(243,252,2));
		temp_str.append(strLevel_temp);
		//GameView::getInstance()->showPopupWindow(temp_str.c_str(),2,this,coco_selectselector(ShowLevelUi::enterSureSignCopyLevel),NULL);

		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
		PopupWindow * dialog =PopupWindow::create(temp_str.c_str(),2,PopupWindow::KtypeNeverRemove,0);
		dialog->ignoreAnchorPointForPosition(false);
		dialog->setAnchorPoint(ccp(0.5f,0.5f));
		dialog->setPosition(ccp(winSize.width/2,winSize.height/2));
		dialog->setTag(101011);
		dialog->AddcallBackEvent(this,coco_selectselector(ShowLevelUi::enterSureSignCopyLevel),NULL);
		GameView::getInstance()->getMainUIScene()->addChild(dialog);


		/*
		SingCopyInstance::setCurSingCopyClazz(m_clazz);
		SingCopyInstance::setCurSingCopyLevel(this->getCurLevel());
		this->callBackCloseUi(NULL);
		if ( challengeui_)
		{
			challengeui_->callBackCloseUi(NULL);
		}
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1901,(void *)m_clazz,(void *)this->getCurLevel());
		DamageStatistics::clear();
		*/
	}else
	{
		const char *strings = StringDataManager::getString("singleCopy_isOpenOnLevel");
		GameView::getInstance()->showAlertDialog(strings);
	}
}

void ShowLevelUi::enterSureSignCopyLevel( CCObject * obj )
{
	
	CCActionInterval * action =(CCActionInterval *)CCSequence::create(
		CCDelayTime::create(0.5f),
		CCCallFuncO::create(this,callfuncO_selector(ShowLevelUi::enterSingCopyAction),NULL),
		NULL);
	runAction(action);

}

void ShowLevelUi::enterSingCopyAction( CCObject * obj )
{
	SingCopyInstance::setCurSingCopyClazz(m_clazz);
	SingCopyInstance::setCurSingCopyLevel(this->getCurLevel());
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1901,(void *)m_clazz,(void *)this->getCurLevel());

	DamageStatistics::clear();

	this->callBackCloseUi(NULL);
	
	ChallengeRoundUi * challengeui_ = (ChallengeRoundUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);
	if (challengeui_)
	{
		challengeui_->callBackCloseUi(NULL);
	}
	
}

void ShowLevelUi::setCurLevel( int value_ )
{
	m_curLevel = value_;
}

int ShowLevelUi::getCurLevel()
{
	return m_curLevel;
}

void ShowLevelUi::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void ShowLevelUi::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,88,38,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+170+_w,pos.y+276+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void ShowLevelUi::removeCCTutorialIndicator()
{
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}






