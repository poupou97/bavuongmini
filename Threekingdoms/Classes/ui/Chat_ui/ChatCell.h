#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
//#include "../extensions/CCRichLabel.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCRichLabel;
struct ChatCellStruct
{
	int channelId;
	std::string playNameId;
	std::string text;
	float height;   // the height of each chat
};

class ChatCell:public CCTableViewCell
{
public:
	ChatCell(void);
	~ChatCell(void);
	static ChatCell * create(int channel_id,std::string player_country,std::string play_name,std::string describe,long long playerid,bool isMainChat,int viplevel,long long listenerId,std::string listenerName = "");
	bool init(int channel_id,std::string play_name,std::string player_country,std::string describe,long long playerid,bool isMainChat,int viplevel,long long listenerId,std::string listenerName = "");
	
	void onEnter();
	void onExit();
	
	std::string getChannelImageOfIndex(int index);
	static std::string getCountryImageByStr(std::string country);

	CCLabelTTF * getChannelNameByindex(int index);
	
	void LinkEvent(CCObject * obj);
	void NameEvent(CCObject* pSender);
	void callBackTransmit(CCObject * obj);

	int channelId;
	std::string playerCountry;
	std::string describeText;
	long long playerId;
	std::string playName;
	int playerVip_;

	std::string listenerName_;
	int richlabel_space;
	std::string chatContent_;
	CCRichLabel* labelLink;

	void setOneCellOfLine(int line);
	int getOneCellOfLine();
	int label_width;
private:
	int m_oneCellLine;
};

