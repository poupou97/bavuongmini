#pragma once
#include "cocos2d.h"
#include "../extensions/UIScene.h"


class CCMoveableMenu;
class CCRichLabel;
USING_NS_CC;
USING_NS_CC_EXT;
typedef enum{
	TABVIEW_TAG=206,
};
class Tablist:public UIScene, public cocos2d::extension::CCTableViewDataSource, public cocos2d::extension::CCTableViewDelegate
{
public:
	Tablist(void);
	~Tablist(void);

	static Tablist *create();
	bool init();
	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);


	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);
	

	// default implements are used to call script callback if exist
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	CCTableView* tableView;
	UILayer *scene_;
	static int enterTime;
private:
	CCMenuItemFont *itemImage_name;
	UIPanel* panel;
	CCSize cellSize;
	CCRichLabel * labelLink;
	UIPanel *equipInfoPanel;
	CCMoveableMenu *name_menu;
};

