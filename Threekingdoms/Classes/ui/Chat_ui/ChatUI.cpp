#include "ChatUI.h"
#include "../../logo_state/LogoState.h"
#include "BackPackage.h"
#include "../extensions/RichTextInput.h"
#include "ExpressionLayer.h"
#include "AddPrivateUi.h"
#include "../extensions/RichElement.h"
#include "../extensions/RichTextInput.h"
#include "../../messageclient/ClientNetEngine.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../GameView.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/UITab.h"
#include "Tablist.h"
#include "ChatCell.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "ChatInstance.h"

typedef enum {
	WHOLE_LABEL = 0,
	WORLD_LABEL,
	COUNTRY_LABEL,
	FAMILY_LABEL,
	TEAM_LABEL,
	CURRECT_LABEL,
	PRIVATE_LABEL,
	SYSTEM_LABEL,
	ACOUSTIC_LABEL,
	
}label;

ChatUI::ChatUI(void)
{
	whetherOfRoll=true;
	selectChannelId=1;
}


ChatUI::~ChatUI(void)
{
}

ChatUI * ChatUI::create()
{
	ChatUI * chatUi=new ChatUI();
	if (chatUi && chatUi->init())
	{
		chatUi->autorelease();
		return chatUi;
	}
	CC_SAFE_DELETE(chatUi);
	return NULL;

}

bool ChatUI::init()
{
	if (UIScene::init())
	{
		winsize=CCDirector::sharedDirector()->getVisibleSize();
		
		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		if(LoadSceneLayer::chatPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::chatPanel->removeFromParentAndCleanup(false);
		}
		UIPanel * ppanel = LoadSceneLayer::chatPanel;
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		layer_1 = UILayer::create();
		layer_1->ignoreAnchorPointForPosition(false);
		layer_1->setAnchorPoint(ccp(0.5f,0.5f));
		layer_1->setPosition(ccp(winsize.width/2,winsize.height/2));
		layer_1->setContentSize(CCSizeMake(800,480));
		this->addChild(layer_1, 0);
		//layer_1->addWidget(ppanel);

		const char * secondStr = StringDataManager::getString("chat_diaoshi_liao");
		const char * thirdStr = StringDataManager::getString("chat_diaoshi_tian");
		CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(ccp(30,240));
		layer_1->addChild(atmature);

		layer_3 = UILayer::create();
		layer_3->ignoreAnchorPointForPosition(false);
		layer_3->setAnchorPoint(ccp(0.5f,0.5f));
		layer_3->setPosition(ccp(winsize.width/2,winsize.height/2));
		layer_3->setContentSize(CCSizeMake(800,480));
		addChild(layer_3,2,CHATUILAYER3);

// 		UIImageView * pageView_kuang = UIImageView::create();
// 		pageView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setScale9Enable(true);
// 		pageView_kuang->setScale9Size(CCSizeMake(684,314));
// 		pageView_kuang->setCapInsets(CCRectMake(32,32,1,1));
// 		pageView_kuang->setAnchorPoint(ccp(0,0));
// 		pageView_kuang->setPosition(ccp(65,95));
// 		layer_3->addWidget(pageView_kuang);

		UIPanel * ppane_3 = UIPanel::create();
		ppane_3->setAnchorPoint(ccp(0,0));
		ppane_3->setPosition(ccp(0,0));
		ppane_3->setZOrder(5);
		layer_3->addWidget(ppane_3);
		
		textBox=new RichTextInputBox();
		textBox->setInputMode(RichTextInputBox::kCCInputModeAppendOnly);
		textBox->setInputBoxWidth(352);
		textBox->setAnchorPoint(ccp(0,0));
		textBox->setPosition(ccp(139,45));//68
		layer_3->addChild(textBox, 2, RICHTEXTINPUT_TAG);
        textBox->setCharLimit(50);
		textBox->autorelease();

		const char *str_synthesize = StringDataManager::getString("chatui_synthesize");
		char *synthesize_left=const_cast<char*>(str_synthesize);

		const char *str_world = StringDataManager::getString("chatui_world");
		char *world_left=const_cast<char*>(str_world);
		
		const char *str_courtny = StringDataManager::getString("chatui_country");
		char *courtny_left=const_cast<char*>(str_courtny);

		const char *str_camp = StringDataManager::getString("chatui_camp");
		char *camp_left=const_cast<char*>(str_camp);
		
		const char *str_team = StringDataManager::getString("chatui_team");
		char *team_left=const_cast<char*>(str_team);	

		const char *str_curr = StringDataManager::getString("chatui_current");
		char *curr_left=const_cast<char*>(str_curr);

		const char *str_private = StringDataManager::getString("chatui_private");
		char *private_left=const_cast<char*>(str_private);

		const char *str_system = StringDataManager::getString("chatui_system");
		char *system_left=const_cast<char*>(str_system);

		const char *str_Acoustic = StringDataManager::getString("chatui_acoustic");
		char *acoustic_left=const_cast<char*>(str_Acoustic);
		//////////CCTab  频道切换标签
		const char * normalImage = "res_ui/tab_2_off.png";
		const char * selectImage ="res_ui/tab_2_on.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_2_on.png";
		char *left[]={synthesize_left,world_left,courtny_left,camp_left,team_left,curr_left,private_left,system_left,acoustic_left};
		channelLabel= UITab::createWithText(9,normalImage,selectImage,finalImage,left,HORIZONTAL,2);
		channelLabel->setAnchorPoint(ccp(0,0));
		channelLabel->setPosition(ccp(64,410));
		channelLabel->setPressedActionEnabled(true);
		channelLabel->setHighLightImage((char * )highLightImage);
		channelLabel->setDefaultPanelByIndex(0);
		channelLabel->addIndexChangedEvent(this,coco_indexchangedselector(ChatUI::ChangeLeftPanelByIndex));
		channelLabel->setWidgetZOrder(200);
		channelLabel->setScale(0.95f);
		ppane_3->addChild(channelLabel);
		//自动滚动 
		btn_rollBg=UIButton::create();
		btn_rollBg->setTouchEnable(true);
		btn_rollBg->setPressedActionEnabled(true);
		btn_rollBg->setTextures("res_ui/liaotian/suo_off.png","res_ui/liaotian/suo_off.png","");
		btn_rollBg->setAnchorPoint(ccp(0.5f,0.5f));
		btn_rollBg->setPosition(ccp(650,375));
		btn_rollBg->setScale(0.8f);
		btn_rollBg->addReleaseEvent(this,coco_releaseselector(ChatUI::callBackRoll));
		ppane_3->addChild(btn_rollBg);

		UIButton * buttonDelete =(UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_delete");
		buttonDelete->setTouchEnable(true);
		buttonDelete->setPressedActionEnabled(true);
		buttonDelete->addReleaseEvent(this, coco_releaseselector(ChatUI::callBackDelete));

		labelchanel=(UILabel *)UIHelper::seekWidgetByName(ppanel,"labelchanel");

		buttonChannel_ =(UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_channel");
		buttonChannel_->setTouchEnable(true);
		buttonChannel_->setPressedActionEnabled(true);
		buttonChannel_->addReleaseEvent(this, coco_releaseselector(ChatUI::addCahannelSelect));
		//const char *stringsWorld = StringDataManager::getString("chatui_current");
		labelchanel->setText(world_left);

		buttonExpression_ =(UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_expression");
		buttonExpression_->setTouchEnable(true);
		buttonExpression_->setPressedActionEnabled(true);
		buttonExpression_->addReleaseEvent(this, coco_releaseselector(ChatUI::callBackExpression));

		UIButton * buttonPack_ =(UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_backpack");
		buttonPack_->setTouchEnable(true);
		buttonPack_->setPressedActionEnabled(true);
		buttonPack_->addReleaseEvent(this, coco_releaseselector(ChatUI::callbackBackPakage));

		UIButton * buttonSend_ =(UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_sendOut");
		buttonSend_->setTouchEnable(true);
		buttonSend_->setPressedActionEnabled(true);
		buttonSend_->addReleaseEvent(this, coco_releaseselector(ChatUI::callBackSend));
		//exitButton
		UIButton * button_close= (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		button_close->setPressedActionEnabled(true);
		button_close->setTouchEnable(true);
		button_close->addReleaseEvent(this, coco_releaseselector(ChatUI::callBackExit));
		
		GameView::getInstance()->selectCell_vdata.clear();
		this->refreshChatCellsByChannelId(-1);
		
		listLayer=Tablist::create();
		listLayer->ignoreAnchorPointForPosition(false);
		listLayer->setAnchorPoint(ccp(0,0));
		listLayer->setPosition(ccp(80,102));
		layer_1->addChild(listLayer,1,TABLIST_TAG);
		
		CCTableView* tableView = (CCTableView*)listLayer->getChildByTag(TABVIEW_TAG);
		tableView->reloadData();
		if(tableView->getContentSize().height > tableView->getViewSize().height)
			tableView->setContentOffset(tableView->maxContainerOffset(), false);

		setTouchEnabled(true);
		setTouchMode(kCCTouchesOneByOne);
		setContentSize(winsize);
		
		return true;
	}
	return false;
}

void ChatUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	int m_index = ChatInstance::getInstance()->getCurChannel();
	channelLabel->setDefaultPanelByIndex(m_index);
	this->ChangeLeftPanelByIndex(channelLabel);

}

void ChatUI::addCahannelSelect( CCObject * obj )
{
	AddPrivateUi * UIprivate=AddPrivateUi::create(3);
	UIprivate->ignoreAnchorPointForPosition(false);
		UIprivate->setAnchorPoint(ccp(0,0));
	UIprivate->setPosition(ccp(buttonChannel_->getPosition().x- buttonChannel_->getSize().width/2 ,
								buttonChannel_->getPosition().y+UIprivate->getContentSize().height+7));
	layer_3->addChild(UIprivate,4);

}

void ChatUI::callBackExpression(CCObject * obj)
{
	ExpressionLayer * ex=ExpressionLayer::create(kTabChat);
	ex->ignoreAnchorPointForPosition(false);
	ex->setAnchorPoint(ccp(0.5f,0));
	ex->setPosition(ccp(407,buttonExpression_->getPosition().y  + buttonExpression_->getContentSize().height));
	layer_3->addChild(ex,4);
}

void ChatUI::callbackBackPakage( CCObject * obj )
{
	if(layer_3->getChildByTag(PACKAGEAGE) != NULL)
		return;

	BackPackage * backbag=BackPackage::create(kTabChat);
	backbag->ignoreAnchorPointForPosition(false);
	backbag->setAnchorPoint(ccp(0.5f,0.5f));
	backbag->setPosition(ccp(layer_3->getContentSize().width/2,layer_3->getContentSize().height/2));
	layer_3->addChild(backbag,4,PACKAGEAGE);
}

void ChatUI::addPrivate( )
{
	AddPrivateUi * privateui_ =(AddPrivateUi *)layer_3->getChildByTag(PRIVATEUI);
	if (privateui_ == NULL)
	{
		AddPrivateUi * UIprivate=AddPrivateUi::create(1);
		UIprivate->ignoreAnchorPointForPosition(false);
		UIprivate->setAnchorPoint(ccp(0,0));
		UIprivate->setPosition(ccp(450,300));
		layer_3->addChild(UIprivate,4,PRIVATEUI);
	}
	
}

void ChatUI::callBackSend( CCObject * obj )
{
    const char* inputStr = textBox->getInputString();
    if(inputStr == NULL)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("chatui_callBackSend_null"));
		return;
	}
        
    chatContent = inputStr;
	if (selectChannelId == 1)
	{
		if (chatContent.length() > 60)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("chatui_Acoustic_LimitSize"));
			return;
		}
	}
	if (selectChannelId==0)
	{
		AddPrivateUi * privateui=(AddPrivateUi *)layer_3->getChildByTag(PRIVATEUI);
		if (privateui ==NULL)
		{
			const char *str_ = StringDataManager::getString("chatui_private_name");
			GameView::getInstance()->showAlertDialog(str_);
			this->addPrivate();
			return;
		}
		privateName = privateui->textBox_private->getInputString();
		if (privateName ==NULL)
		{
			const char *str_ = StringDataManager::getString("chatui_private_name");
			GameView::getInstance()->showAlertDialog(str_);
			return;
		}
	}else
	{
		privateName = "";
	}

	ChatInfoStruct chatInfo = {selectChannelId,chatContent,-1,privateName};
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2203, &chatInfo);
	//textBox->deleteAllInputString();
	sendSecond=0;
}

void ChatUI::callBackExit( CCObject * obj )
{
	this->closeAnim();
}

void ChatUI::refreshChatCellsByChannelId( int channelId )
{
	std::vector<ChatCell*>& allChatCells = GameView::getInstance()->chatCell_Vdata;

	for (unsigned int num=0;num<allChatCells.size();num++)
	{
		// -1 indicate: whole chanels
		if (allChatCells.at(num)->channelId == channelId || channelId == -1)
		{
			int channelid_ = allChatCells.at(num)->channelId;
			std::string playname_ = allChatCells.at(num)->playName;
			std::string descrite_ = allChatCells.at(num)->describeText;

			GameView::getInstance()->selectCell_vdata.push_back(allChatCells.at(num));
		}	
	}
}

int ChatUI::getChannelIndex()
{
	CCAssert(channelLabel != NULL, "should not be nil");
	return channelLabel->getCurrentIndex();
}

void ChatUI::refreshChatUIInfoByChanelIndex( int index )
{
	// selectCell_vdata 's data comes from chatCell_Vdata, so clear it directly without release
	GameView::getInstance()->selectCell_vdata.clear();

	const char *stringsPrivate = StringDataManager::getString("chatui_private");
	const char *stringsWorld = StringDataManager::getString("chatui_world");
	const char *stringsRoom = StringDataManager::getString("chatui_room");
	const char *stringsCurrent = StringDataManager::getString("chatui_current");
	const char *stringsSystem = StringDataManager::getString("chatui_system");
	const char *stringsTeam = StringDataManager::getString("chatui_team");
	const char *stringsCamp = StringDataManager::getString("chatui_camp");
	const char *stringsFaction = StringDataManager::getString("chatui_faction");
	const char *stringsCountry = StringDataManager::getString("chatui_country");
	const char *stringsAcoustic = StringDataManager::getString("chatui_acoustic");

	switch (index)
	{
	case SYSTEM_LABEL:
		{
			refreshChatCellsByChannelId(4);
		}break;
	case WHOLE_LABEL:
		{
			refreshChatCellsByChannelId(-1);
		}break;
	case WORLD_LABEL:
		{
			refreshChatCellsByChannelId(1);
			selectChannelId=1;
			labelchanel->setText(stringsWorld);//Label_channel
		}break;
	case COUNTRY_LABEL:
		{
			refreshChatCellsByChannelId(8);
			selectChannelId=8;
			labelchanel->setText(stringsCountry);
		}break;
	case FAMILY_LABEL:
		{
			refreshChatCellsByChannelId(6);
			selectChannelId=6;
			labelchanel->setText(stringsCamp);
		}break;
	case TEAM_LABEL:
		{
			refreshChatCellsByChannelId(5);
			selectChannelId=5;
			labelchanel->setText(stringsTeam);
		}break;
	case CURRECT_LABEL:
		{
			refreshChatCellsByChannelId(3);
			selectChannelId=3;
			labelchanel->setText(stringsCurrent);
		}break;
	case PRIVATE_LABEL:
		{
			refreshChatCellsByChannelId(0);
			selectChannelId=0;
			labelchanel->setText(stringsPrivate);
		}break;
	case ACOUSTIC_LABEL:
		{
			refreshChatCellsByChannelId(9);
			selectChannelId = 9;
			labelchanel->setText(stringsAcoustic);
		}break;
	}

	ChatInstance::getInstance()->setCurChannel(index);
}

void ChatUI::ChangeLeftPanelByIndex( CCObject * obj )
{
	int index=((UITab *)obj)->getCurrentIndex();
	if (index != 6)
	{
		AddPrivateUi * privateui_ = (AddPrivateUi *)layer_3->getChildByTag(PRIVATEUI);
		if (privateui_ != NULL)
		{
			privateui_->callBackClosed(NULL);
		}
	}

	//改变频道按钮的选项
	refreshChatUIInfoByChanelIndex(index);
	
	CCTableView* tableView = (CCTableView*)listLayer->getChildByTag(TABVIEW_TAG);
	tableView->reloadData();
	if(tableView->getContentSize().height > tableView->getViewSize().height)
		tableView->setContentOffset(tableView->maxContainerOffset(), false);
}

bool ChatUI::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return true;
}

void ChatUI::callBackDelete( CCObject * obj )
{
	textBox->onTextFieldDeleteBackward(NULL, NULL, 0);
}

void ChatUI::callBackRoll( CCObject * obj )
{
	CCTableView* tableView = (CCTableView*)listLayer->getChildByTag(TABVIEW_TAG);
	const char *str_ = NULL;
	if (whetherOfRoll==true)
	{
		whetherOfRoll=false;
		btn_rollBg->setTextures("res_ui/liaotian/suo_on.png","res_ui/liaotian/suo_on.png","");
		//curTabViewOff= tableView->getContentOffset();

		str_ = StringDataManager::getString("chatui_not_auto_roll");
	}
	else
	{
		whetherOfRoll=true;
		btn_rollBg->setTextures("res_ui/liaotian/suo_off.png","res_ui/liaotian/suo_off.png","");

		str_ = StringDataManager::getString("chatui_auto_roll");
	}
	GameView::getInstance()->showAlertDialog(str_);
}

void ChatUI::addToMainChatUiMessage( int channel_id,std::string message )
{
	ChatCell * chatCellBig = ChatCell::create(channel_id, "", "", message,NULL, true,0,0);
	chatCellBig->retain();
	GameView::getInstance()->chatCell_Vdata.push_back(chatCellBig);

	if (GameView::getInstance()->chatCell_Vdata.size() > 30)
	{
		std::vector<ChatCell *>::iterator iter=GameView::getInstance()->chatCell_Vdata.begin();
		ChatCell * cellv_=* iter;
		GameView::getInstance()->chatCell_Vdata.erase(iter);
		cellv_->release();
	}

	ChatUI * chatui_ = (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
	if (chatui_ != NULL)
	{
		chatui_->refreshChatUIInfoByChanelIndex(chatui_->getChannelIndex());
		CCTableView* tableView = (CCTableView*)chatui_->listLayer->getChildByTag(TABVIEW_TAG);
		CCPoint off_ =tableView->getContentOffset();
		int h_ = tableView->getContentSize().height + off_.y;
		tableView->reloadData();
		if(tableView->getContentSize().height > tableView->getViewSize().height)
		{
			if (chatui_->whetherOfRoll==false)
			{
				CCPoint temp = ccp(off_.x,h_- tableView->getContentSize().height);
				tableView->setContentOffset(temp);
			}else
			{
				tableView->setContentOffset(tableView->maxContainerOffset(), false);
			}			
		}
	}
}

bool ChatUI::redRight=true;
int ChatUI::sendSecond=0;



