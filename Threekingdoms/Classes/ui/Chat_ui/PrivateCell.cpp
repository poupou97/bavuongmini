#include "PrivateCell.h"
#include "ChatUI.h"
#include "../extensions/RichTextInput.h"
#include "AddPrivateUi.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../extensions/CCMoveableMenu.h"
#include "../extensions/CCRichLabel.h"
#include "../../GameView.h"
#include "../backpackscene/PacPageView.h"
#include "../../ui/extensions/RichElement.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "AppMacros.h"
#include "../../gamescene_state/sceneelement/ChatWindows.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/HeadMenu.h"
#include "PrivateChatUi.h"
#include "../../messageclient/element/CRelationPlayer.h"


PrivateCell::PrivateCell(void)
{	
	m_curCellHeight = 0;
}
PrivateCell::~PrivateCell(void)
{
}
PrivateCell * PrivateCell::create(int pressionId_,std::string play_name,std::string chatContent,long long playerid,int viplevel,std::string listenerName /*= ""*/ )
{
	PrivateCell * chatcell=new PrivateCell();
	if(chatcell && chatcell->init(pressionId_,play_name,chatContent,playerid,viplevel,listenerName))
	{
		chatcell->autorelease();
		return chatcell;
	}
	CC_SAFE_DELETE(chatcell);
	return NULL;
}

bool PrivateCell::init(int pressionId_,std::string play_name,std::string chatContent,long long playerid,int viplevel,std::string listener_Name /*= ""*/ )
{
	if (CCTableViewCell::init())
	{
		speakName = play_name;
		listenerName =listener_Name ;
		speakerId = playerid;
		int m_pression = pressionId_;
		
		if ( playerid == GameView::getInstance()->myplayer->getActiveRole()->rolebase().roleid())
		{
			CCSprite * iconBackGround =CCSprite::create("res_ui/round.png");
			iconBackGround->setAnchorPoint(ccp(0,0));
			iconBackGround->setPosition(ccp(345 - iconBackGround->getContentSize().width - 16,0));
			addChild(iconBackGround);

			m_pression  = GameView::getInstance()->myplayer->getProfession();
			std::string playerIcon_ = "";
			if (m_pression > 0 && m_pression <5)
			{
				playerIcon_ = BasePlayer::getHeadPathByProfession(m_pression);
			}else
			{	
				playerIcon_ = "res_ui/protangonis56x55/none.png";
			}

			CCSprite * friendHead =CCSprite::create(playerIcon_.c_str());
			friendHead->setAnchorPoint(ccp(0.5f,0.5f));
			//friendHead->setPosition(ccp(345 - iconBackGround->getContentSize().width - 13,3));
			friendHead->setPosition(ccp(iconBackGround->getContentSize().width/2,iconBackGround->getContentSize().height/2-1));
			friendHead->setScale(0.8f);
			iconBackGround->addChild(friendHead);

			CCRichLabel *labelLink =CCRichLabel::createWithString(chatContent.c_str(),CCSizeMake(220,50),this,menu_selector(PrivateCell::LinkEvent),0,16);
			labelLink->setAnchorPoint(ccp(0,0));
			labelLink->setPosition(ccp(0,10));
			addChild(labelLink,5);

			int dialog_hight = 42;//dialog2 min is 42, 
			if (labelLink->getContentSize().height > 30)
			{
				dialog_hight = labelLink->getContentSize().height + 20;
				iconBackGround->setPositionY(dialog_hight - 40);
				//friendHead->setPositionY(dialog_hight - 40);
			}

			CCScale9Sprite  * contentSp_ = CCScale9Sprite::create("res_ui/dialog2.png");
			contentSp_->setAnchorPoint(ccp(0,0));
			contentSp_->setPreferredSize(CCSizeMake(labelLink->getContentSize().width+30,dialog_hight));
			contentSp_->setCapInsets(CCRect(24,27,1,3));
			contentSp_->setRotationY(180);
			contentSp_->setPosition(ccp(0,0));
			addChild(contentSp_);

			if (labelLink->getContentSize().width < 220)
			{
				labelLink->setPositionX(345 - labelLink->getContentSize().width - iconBackGround->getContentSize().width - 34);
				contentSp_->setPositionX(345 - iconBackGround->getContentSize().width - 16);
			}

			CCLabelTTF *label_playerName = CCLabelTTF::create(speakName.c_str(), APP_FONT_NAME, 16);
			label_playerName->setAnchorPoint(ccp(1,0));
			label_playerName->setPosition(ccp(345 - label_playerName->getContentSize().width/2 - iconBackGround->getContentSize().width,contentSp_->getContentSize().height));
			addChild(label_playerName);
			label_playerName->setColor(ccc3(15,202,250));

			int h_ = iconBackGround->getContentSize().height + label_playerName->getContentSize().height;
			if (h_ < contentSp_->getContentSize().height + label_playerName->getContentSize().height)
			{
				h_ = contentSp_->getContentSize().height+label_playerName->getContentSize().height;
			}
			this->setCurCellHeight(h_);
		}else
		{
			CCSprite * iconBackGround =CCSprite::create("res_ui/round.png");
			iconBackGround->setAnchorPoint(ccp(0.5f,0));
			iconBackGround->setPosition(ccp(iconBackGround->getContentSize().width/2,0));
			addChild(iconBackGround);

			std::string playerIcon_;
			if (m_pression > 0 && m_pression <5)
			{
				playerIcon_ = BasePlayer::getHeadPathByProfession(m_pression);
				
			}else
			{
				playerIcon_ = "res_ui/protangonis56x55/none.png";
			}

			/*
			CCSprite * friendHead =CCSprite::create(playerIcon_.c_str());
			CCMenuItemSprite * nameIcon_item = CCMenuItemSprite::create(friendHead, friendHead, friendHead, this, menu_selector(PrivateCell::callBackNameList));
			nameIcon_item->setZoomScale(0.6f);
			CCMoveableMenu * nameIcon_menu = CCMoveableMenu::create(nameIcon_item,NULL);
			nameIcon_menu->setAnchorPoint(ccp(0.5f,0.5f));
			nameIcon_menu->setPosition(ccp(iconBackGround->getContentSize().width/2 - 80, -iconBackGround->getContentSize().height/2));//
			addChild(nameIcon_menu);
			nameIcon_menu->setScale(0.8f);
			*/

			UILayer * layer = UILayer::create();
			addChild(layer);

			UIButton * btn_icon = UIButton::create();
			btn_icon->setTextures(playerIcon_.c_str(),playerIcon_.c_str(),"");
			btn_icon->setAnchorPoint(ccp(0.5f,0.5f));
			btn_icon->addReleaseEvent(this,coco_releaseselector(PrivateCell::callBackNameList));
			btn_icon->setTouchEnable(true);
			btn_icon->setPosition(ccp(iconBackGround->getContentSize().width/2,btn_icon->getContentSize().height/2));
			btn_icon->setPressedActionEnabled(true);
			layer->addWidget(btn_icon);

			btn_icon->setScale(0.8f);

			CCRichLabel *labelLink =CCRichLabel::createWithString(chatContent.c_str(),CCSizeMake(220,50),this,menu_selector(PrivateCell::LinkEvent),0,16);
			labelLink->setAnchorPoint(ccp(0,0));
			labelLink->setPosition(ccp(iconBackGround->getContentSize().width+20,10));
			addChild(labelLink,5);

			int dialog_hight = 42;//dialog2 min is 42, 
			if (labelLink->getContentSize().height > 30)
			{
				dialog_hight = labelLink->getContentSize().height + 20;
				iconBackGround->setPositionY(dialog_hight - 40);
				btn_icon->setPosition(ccp(iconBackGround->getPositionX(),iconBackGround->getPositionY()+btn_icon->getContentSize().height/2));//dialog_hight - 40 -iconBackGround->getContentSize().height/2 
			}

			CCScale9Sprite  * contentSp_ = CCScale9Sprite::create("res_ui/dialog1.png");
			contentSp_->setAnchorPoint(ccp(0,0));
			contentSp_->setPreferredSize(CCSizeMake(labelLink->getContentSize().width+30,dialog_hight));
			contentSp_->setCapInsets(CCRect(24,27,1,3));
			contentSp_->setPosition(ccp(iconBackGround->getContentSize().width+5,0));
			addChild(contentSp_);

			CCLabelTTF *label_playerName = CCLabelTTF::create(speakName.c_str(), APP_FONT_NAME, 16);
			label_playerName->setAnchorPoint(ccp(0,0));
			label_playerName->setPosition(ccp(iconBackGround->getPositionX() + iconBackGround->getContentSize().width,contentSp_->getContentSize().height));
			addChild(label_playerName,5);
			label_playerName->setColor(ccc3(136,234,31));

			int h_ = iconBackGround->getContentSize().height + label_playerName->getContentSize().height;
			
			if (h_ < contentSp_->getContentSize().height + label_playerName->getContentSize().height)
			{
				h_ = contentSp_->getContentSize().height+label_playerName->getContentSize().height;
			}
			this->setCurCellHeight(h_);
		}

		return true;
	}
	return false;
}

void PrivateCell::onEnter()
{
	CCTableViewCell::onEnter();
}
void PrivateCell::onExit()
{
	CCTableViewCell::onExit();
}

void PrivateCell::LinkEvent( CCObject * obj )
{    
	CCMenuItemFont* button = (CCMenuItemFont*)obj;
	RichElementButton* btnElement = (RichElementButton*)button->getUserData();
	//CCLog("richlabel clicked, %s", btnElement->linkContent.c_str());
    structPropIds prop_ids = RichElementButton::parseLink(btnElement->linkContent);
    CCLOG("prop id: %s, prop instance id: %ld", prop_ids.propId.c_str(), prop_ids.propInstanceId);
    
	const char * goodsPropId = prop_ids.propId.c_str();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1322,(void *)prop_ids.propInstanceId,(void *)goodsPropId);
}

void PrivateCell::NameEvent( CCObject* pSender )
{
	/*
	CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
	PrivateChatUi * privateui= (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);

	long long myId = GameView::getInstance()->myplayer->getRoleId();
	if (myId == speakerId)
	{
		return;
	}
	AddPrivateUi * nameList=AddPrivateUi::create(4);
	nameList->ignoreAnchorPointForPosition(false);
	nameList->setAnchorPoint(ccp(0,0));

	int cellOff = this->getPosition().y;
	nameList->setPosition(ccp(100,winsize.height/2));
	privateui->addChild(nameList,4);
	*/
}

void PrivateCell::setCurCellHeight( int height )
{
	m_curCellHeight = height;
}

int PrivateCell::getCurCellHeight()
{
	return m_curCellHeight;
}

void PrivateCell::callBackNameList( CCObject * obj )
{
	PrivateChatUi * privateui_ = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);
	
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	NameList * nameList = NameList::create(speakerId,speakName);
	nameList->ignoreAnchorPointForPosition(false);
	nameList->setAnchorPoint(ccp(0,0));
	//nameList->setPosition(ccp(300,winSize.height/2 - nameList->getContentSize().height/2+20));
	nameList->setPosition(ccp(160,winSize.height/2 - nameList->getContentSize().height/2+20));
	privateui_->layer2->addChild(nameList);
}
