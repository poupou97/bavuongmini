#include "ExpressionLayer.h"
#include "../extensions/RichTextInput.h"
#include "../Chat_ui/ChatUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "PrivateChatUi.h"

ExpressionLayer::ExpressionLayer(void)
{	
}


ExpressionLayer::~ExpressionLayer(void)
{

}
ExpressionLayer * ExpressionLayer::create(int uitag)
{
	ExpressionLayer * exLayer=new ExpressionLayer();
	if (exLayer && exLayer->init(uitag))
	{
		exLayer->autorelease();
		return exLayer;
	}
	CC_SAFE_DELETE(exLayer);
	return NULL;

}

bool ExpressionLayer::init(int uitag)
{
	if (UIScene::init())
	{
		CCSize s=CCDirector::sharedDirector()->getVisibleSize();

		if(LoadSceneLayer::expressPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::expressPanel->removeFromParentAndCleanup(false);
		}

		UIPanel * panel=LoadSceneLayer::expressPanel;
		panel->setAnchorPoint(ccp(0,0));
		panel->setTouchEnable(true);
		panel->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(panel);
		uitagIndex = uitag;
		index=1;
		for (int j=0;j<3;j++)
		{
			for(int i=0;i<10;i++)
			{
			
				char exName[50];
				if (index<=9)
				{
					sprintf(exName, "res_ui/Expression/e0%d.png", index);
				}
				else
				{
					sprintf(exName, "res_ui/Expression/e%d.png", index);
				}

				UIButton * expButton=UIButton::create();
				expButton->setTouchEnable(true);
				expButton->setAnimationTextures(exName,exName,"");
				expButton->setAnchorPoint(ccp(0,0));
				expButton->setPosition(ccp(20+45* i,143-j*60));//38,45
				expButton->setWidgetTag(index);
				expButton->addReleaseEvent(this,coco_cancelselector(ExpressionLayer::callBack));
				m_pUiLayer->addWidget(expButton);
				index++;	
			}
		}

 		this->setTouchEnabled(true);
  		this->setTouchMode(kCCTouchesOneByOne);
 		this->setContentSize(CCSizeMake(480,200));

		return true;
	}
	return false;
}

void ExpressionLayer::onEnter()
{
	UIScene::onEnter();
}



bool ExpressionLayer::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return true;
}

void ExpressionLayer::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{
	removeFromParentAndCleanup(true);
}

void ExpressionLayer::callBack( CCObject * obj )
{
	UIButton* btn = (UIButton*)obj;
	int bunindx = btn->getWidgetTag();

	char str[5];
	if (bunindx<=9)
	{
		sprintf(str, "/e0%d", bunindx);
	}
	else
	{
		sprintf(str, "/e%d", bunindx);
	}

	switch(uitagIndex)
	{
	case kTabChat:
		{
			ChatUI * chatui = (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
			RichTextInputBox * text=(RichTextInputBox *)chatui->getChildByTag(CHATUILAYER3)->getChildByTag(RICHTEXTINPUT_TAG);
			text->onTextFieldInsertText(NULL, str, 4);
		}break;
	case ktagPrivateUI:
		{
			PrivateChatUi * privateui = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);
			privateui->textInputBox->onTextFieldInsertText(NULL, str, 4);
		}break;
	}
}
