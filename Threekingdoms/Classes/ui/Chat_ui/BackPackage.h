#pragma once

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../backpackscene/GoodsItemInfoBase.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ChatUI;
class PacPageView;
class CEquipment;
class UITab;
#define  ROLELAYEROFEQUIP_BACKPAC 101
class BackPackage:public UIScene, public cocos2d::extension::CCTableViewDataSource, public cocos2d::extension::CCTableViewDelegate
{
public:
	BackPackage(void);
	~BackPackage(void);

	static BackPackage * create(int uitag);
	bool init(int uitag);

	void onEnter();
	void onExit();

	void refreshCurRoleValue(int idx);
	void callBackShowEquipInfo(CCObject *obj);
	void callBackClose(CCObject * obj);
	
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	void callBackPackAndRole(CCObject * obj);
	bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	void CurrentPageViewChanged(CCObject * obj);
	void callBack(CCObject * obj);
	void callBackOfPrivateUi();
	void getCurRolequipment(int idx);

	void PageScrollToDefault();

	UITab * roleAndPack;
	int curRoleIndex;
public:
	CCSize winsize;
	UILayer * m_Layer;
	UILayer * m_LayerRole;
	UILayer * tempLayer_flag;
	int curType;
	std::string weapEffectStrForRefine;
	std::string weapEffectStrForStar;
	PacPageView *pageView;
	UIImageView* currentPage;
	std::vector<CEquipment*>playerEquipments;
	UIImageView * imageIcon_;
	UILabel * labelLevel;
	UILabelBMFont * labelFightValue_;
	UILabelBMFont * labelAllFightValue_;
	
	UIPanel * panelRole;
	UIPanel * panelPac ;
	UIImageView * pageView_kuang;

	//
	UIImageView * image_accessories;
	UIImageView * image_shoes;
	UIImageView * image_helmet;
	UIImageView * image_ring;
	UIImageView * image_clothes;
	UIImageView * image_arms;

};

///////////////////////////////// equip goodsifno 
class GoodsInfo;
class EquipmentInfoShow:public GoodsItemInfoBase
{
public:
	EquipmentInfoShow(void);
	~EquipmentInfoShow(void);

	static EquipmentInfoShow *create(GoodsInfo * goods,std::vector<CEquipment *> equipvector,long long generalId);
	bool init(GoodsInfo * goods,std::vector<CEquipment *> equipvector,long long generalId);

	void onEnter();
	void onExit();

	void callBackShowEquip(CCObject * obj);

private:
	GoodsInfo * goods_;
};


