#include "Tablist.h"
#include "ChatUI.h"
#include "../extensions/CCMoveableMenu.h"
#include "../extensions/RichTextInput.h"
#include "AddPrivateUi.h"
#include "ChatCell.h"
#include "GameView.h"
#include "../../messageclient/element/CRelationPlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"

enum
{
	WHOLE_RICHLABEL,
	WORLD_RICHLABEL,
	COUNTRY_RICHLABEL,
	CAMP_RICHLABEL,
	PRIVATE_RICHLABEL,
	TEAM_RICHLABEL,
	CURRECT_RICHLABEL,
	SYSTEN_RICHLABEL,
};

Tablist::Tablist(void)
{
	
}

Tablist::~Tablist(void)
{

}

Tablist * Tablist::create()
{
	Tablist *list=new Tablist();
	if (list && list->init())
	{
		list->autorelease();
		return list;
	}
	CC_SAFE_DELETE(list);
	return NULL;
}
bool Tablist::init()
{
	if (UIScene::init())
	{
		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();

		tableView = CCTableView::create(this, CCSizeMake(674, 298));
		tableView->setDirection(kCCScrollViewDirectionVertical);
		tableView->setAnchorPoint(ccp(0,0));
		tableView->setPosition(ccp(0,0));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(tableView, -1, TABVIEW_TAG);
		tableView->reloadData();
		
		if (enterTime == 0)
		{
			enterTime++;
		}else
		{
			if(tableView->getContentSize().height > tableView->getViewSize().height)
				tableView->setContentOffset(tableView->maxContainerOffset(), false);	
		}
		setTouchEnabled(false);
		return true;
	}
	return false;
}
void Tablist::onEnter()
{
	UIScene::onEnter();

}
void Tablist::onExit()
{
	UIScene::onExit();
}

void Tablist::tableCellTouched(CCTableView* table, CCTableViewCell* cell)
{
	CCLOG("cell touched at index: %i", cell->getIdx());
	int idx= cell->getIdx();
	ChatUI * chatui =(ChatUI *)this->getParent()->getParent();
	chatui->playerName_= GameView::getInstance()->selectCell_vdata.at(idx)->playName;
	
	if (strcmp(GameView::getInstance()->myplayer->getActorName().c_str(),chatui->playerName_.c_str()) == 0)
	{
		chatui->playerName_ = GameView::getInstance()->selectCell_vdata.at(idx)->listenerName_;
	}

	chatui->playerId_= GameView::getInstance()->selectCell_vdata.at(idx)->playerId;

	chatui->playerVipLevel = GameView::getInstance()->selectCell_vdata.at(idx)->playerVip_;

	chatui->countryId = 0;
	chatui->playerLevel = 20;
	chatui->pressionId = 0;

}

CCSize Tablist::tableCellSizeForIndex(CCTableView *table, unsigned int idx)
{
	int h_= 25;
	if (h_ <= GameView::getInstance()->selectCell_vdata.at(idx)->getContentSize().height)
	{
		h_ = GameView::getInstance()->selectCell_vdata.at(idx)->getContentSize().height;
	}
	return CCSizeMake(480,h_);
}

CCTableViewCell* Tablist::tableCellAtIndex(CCTableView *table, unsigned int idx)
{
	CCString *string = CCString::createWithFormat("%d", idx);
	ChatCell *cell = (ChatCell*)table->dequeueCell();   // this method must be called
	cell = GameView::getInstance()->selectCell_vdata.at(idx);
	return cell;
}

unsigned int Tablist::numberOfCellsInTableView(CCTableView *table)
{
	int cellLine=GameView::getInstance()->selectCell_vdata.size();
	return cellLine;	
}

void Tablist::scrollViewDidScroll( cocos2d::extension::CCScrollView* view )
{
}

void Tablist::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{
}

bool Tablist::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return true;
}

void Tablist::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void Tablist::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{
	//removeFromParentAndCleanup(true);
}

void Tablist::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

int Tablist::enterTime=0;

