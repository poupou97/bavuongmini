﻿#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
class UITab;
class Tablist;
class ExpressionUi;
class BackPackage;
class ChatCell;
class RichTextInputBox;

typedef enum{
	RICHTEXTINPUT_TAG=201,
	TABLIST_TAG=202,
	PRIVATEUI=203,
	PACKAGEAGE=204,
	CHATUILAYER3=240
	
};
class ChatUI:public UIScene
{
public:
	ChatUI(void);
	~ChatUI(void);

	static ChatUI *create();
	bool init();
	void onEnter();
	
	void callBackDelete(CCObject  * obj);
	void callBackExpression(CCObject * obj);
	void callbackBackPakage(CCObject * obj);
	void callBackSend(CCObject * obj);

	void callBackExit(CCObject * obj);
	void callBackRoll(CCObject * obj);

	void addPrivate();
	void addCahannelSelect(CCObject * obj);
	void ChangeLeftPanelByIndex(CCObject *obj);

 	bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);

	UIButton *btn_rollBg;
	bool whetherOfRoll;
	bool selectButton;
	static bool  redRight;
	int selectChannelId;
    std::string chatContent;
	const char * privateName;
	UILabel * labelchanel;

	static int sendSecond;
	CCSize curTabViewOff;
	int getChannelIndex();
	void refreshChatUIInfoByChanelIndex(int index);

	struct ChatInfoStruct
	{
		int channelId;
		std::string chatContent;
		long long listenerId;
		std::string listenerName;
	};

	static void addToMainChatUiMessage(int channel_id,std::string message);
private:
	void refreshChatCellsByChannelId( int channelId );
public:
	UILayer* layer_1;
	UILayer* layer_3;
	int operation_ ;
	int relationtype_;
	long long playerId_;
	std::string playerName_;
	Tablist* listLayer;
	RichTextInputBox * textBox;
	UITab*  channelLabel;

	int countryId;
	int playerVipLevel;
	int playerLevel;
	int pressionId;

private:
	CCSize winsize;
	UIImageView *redRight_sp;
	UIButton * buttonChannel_;
	UIButton * buttonExpression_;

};

