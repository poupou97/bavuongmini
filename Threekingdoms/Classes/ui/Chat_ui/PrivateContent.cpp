#include "PrivateContent.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../messageclient/element/CRelationPlayer.h"
#include "ChatCell.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "PrivateCell.h"


PrivateContent::PrivateContent(void)
{
}


PrivateContent::~PrivateContent(void)
{
}

PrivateContent * PrivateContent::create()
{
	PrivateContent * privateUi =  new PrivateContent();
	if (privateUi && privateUi->init())
	{
		privateUi->autorelease();
		return privateUi;
	}
	CC_SAFE_DELETE(privateUi);
	return NULL;
}

bool PrivateContent::init()
{
	if (UIScene::init())
	{
		winsize = CCDirector::sharedDirector()->getVisibleSize();

		chatContentTabview = CCTableView::create(this, CCSizeMake(345,255));
		chatContentTabview->setDirection(kCCScrollViewDirectionVertical);
		chatContentTabview->setAnchorPoint(ccp(0,0));
		chatContentTabview->setPosition(ccp(0,0));
		chatContentTabview->setDelegate(this);
		chatContentTabview->setVerticalFillOrder(kCCTableViewFillTopDown);
		m_pUiLayer->addChild(chatContentTabview);

		setTouchEnabled(false);
		return true;
	}
	return false;
}

void PrivateContent::onEnter()
{
	UIScene::onEnter();
}

void PrivateContent::onExit()
{
	UIScene::onExit();
}

void PrivateContent::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}

void PrivateContent::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void PrivateContent::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	
}

cocos2d::CCSize PrivateContent::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	//int height= GameView::getInstance()->chatPrivateContentSourceVector.at(idx)->getContentSize().height+25;
	int height= GameView::getInstance()->chatPrivateContentSourceVector.at(idx)->getCurCellHeight();
	return CCSizeMake(345,height);
}

cocos2d::extension::CCTableViewCell* PrivateContent::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	//CCTableViewCell *cell =table->dequeueCell();   // this method must be called
	PrivateCell *cell = (PrivateCell*)table->dequeueCell();   // this method must be called
	cell = GameView::getInstance()->chatPrivateContentSourceVector.at(idx);
	/*
	cell=new CCTableViewCell();
	cell->autorelease();
	
	long long speakerId = GameView::getInstance()->chatPrivateContentSourceVector.at(idx)->playerId;
	std::string str_nameend = GameView::getInstance()->chatPrivateContentSourceVector.at(idx)->playName;
	std::string str_content = GameView::getInstance()->chatPrivateContentSourceVector.at(idx)->chatContent_;

	if ( speakerId == GameView::getInstance()->myplayer->getActiveRole()->rolebase().roleid())
	{
		CCMenuItemFont * itemImage_name = CCMenuItemFont::create(str_nameend.c_str(), this, menu_selector(PrivateContent::callBackNameEvent));
		ccColor3B nameColor=ccc3(161,255,156);
		itemImage_name->setColor(nameColor);
		itemImage_name->setAnchorPoint(ccp(0.5f,0));

		CCMoveableMenu * name_menu=CCMoveableMenu::create(itemImage_name,NULL);
		name_menu->setAnchorPoint(ccp(1,0));
		name_menu->setPosition(ccp(327 - itemImage_name->getContentSize().width,0));
		cell->addChild(name_menu);

		CCRichLabel *labelLink =CCRichLabel::createWithString(str_content.c_str(),CCSizeMake(320,50),NULL,NULL,0);
		labelLink->setAnchorPoint(ccp(0,0));
		labelLink->setPosition(ccp(0, -itemImage_name->getContentSize().height));
		cell->addChild(labelLink);

		if (labelLink->getContentSize().width < 327)
		{
			labelLink->setPosition(ccp(327 - labelLink->getContentSize().width,-itemImage_name->getContentSize().height));
		}

	}else
	{
		CCMenuItemFont * itemImage_name = CCMenuItemFont::create(str_nameend.c_str(), this, menu_selector(PrivateContent::callBackNameEvent));
		ccColor3B nameColor=ccc3(161,255,156);
		itemImage_name->setColor(nameColor);
		itemImage_name->setAnchorPoint(ccp(0.5f,0));
		CCMoveableMenu * name_menu=CCMoveableMenu::create(itemImage_name,NULL);
		name_menu->setAnchorPoint(ccp(0,0));
		name_menu->setPosition(ccp(itemImage_name->getContentSize().width/2,0));
		cell->addChild(name_menu);

		CCRichLabel *labelLink =CCRichLabel::createWithString(str_content.c_str(),CCSizeMake(320,50),NULL,NULL,0);
		labelLink->setAnchorPoint(ccp(0,0));
		labelLink->setPosition(ccp(0, - itemImage_name->getContentSize().height));
		cell->addChild(labelLink);
	}
	*/
	return cell;
}

unsigned int PrivateContent::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	return GameView::getInstance()->chatPrivateContentSourceVector.size();
}

bool PrivateContent::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return true;
}




