#include "FamilyOperatorOfLeader.h"
#include "ManageFamily.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../extensions/RichTextInput.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "FamilyUI.h"
#include "../../gamescene_state/role/MyPlayer.h"

FamilyOperatorOfLeader::FamilyOperatorOfLeader(void)
{
}


FamilyOperatorOfLeader::~FamilyOperatorOfLeader(void)
{
}

FamilyOperatorOfLeader * FamilyOperatorOfLeader::create()
{
	FamilyOperatorOfLeader * operatorFamily =new FamilyOperatorOfLeader();
	if (operatorFamily && operatorFamily->init())
	{
		operatorFamily->autorelease();
		return operatorFamily;
	}
	CC_SAFE_DELETE(operatorFamily);
	return NULL;
}

bool FamilyOperatorOfLeader::init()
{
	if (UIScene::init())
	{
		winsize=CCDirector::sharedDirector()->getVisibleSize();

		UIPanel *panel_ =UIPanel::create();
		panel_->setAnchorPoint(ccp(0.5f,0.5f));
		panel_->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(panel_);

		UIImageView * background=UIImageView::create();
		background->setTexture("res_ui/zhezhao80.png");
		background->setScale9Enable(true);
		background->setScale9Size(CCSizeMake(198,120));
		background->setAnchorPoint(ccp(0,0));
		background->setPosition(ccp(99,19));
		panel_->addChild(background);

		UIImageView * backgroundFram=UIImageView::create();
		backgroundFram->setTexture("res_ui/bian_1.png");
		backgroundFram->setScale9Enable(true);
		backgroundFram->setScale9Size(CCSizeMake(200,120));
		backgroundFram->setCapInsets(CCRect(7,8,2,2));
		backgroundFram->setAnchorPoint(ccp(0,0));
		backgroundFram->setPosition(ccp(98,20));
		panel_->addChild(backgroundFram);

		UIImageView * backgroundFramRect=UIImageView::create();
		backgroundFramRect->setTexture("res_ui/LV3_dikuang_light.png");
		backgroundFramRect->setScale9Enable(true);
		backgroundFramRect->setScale9Size(CCSizeMake(196,117));
		backgroundFramRect->setCapInsets(CCRect(7.5f,13.5f,3.0f,3.0f));
		backgroundFramRect->setAnchorPoint(ccp(0,0));
		backgroundFramRect->setPosition(ccp(100,20));
		panel_->addChild(backgroundFramRect);

		const char *str_admit = StringDataManager::getString("family_zhaoshou");
		//招收 
		UIButton * button_admit= UIButton::create();
		button_admit->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		button_admit->setTouchEnable(true);
		button_admit->setPressedActionEnabled(true);
		button_admit->addReleaseEvent(this,coco_releaseselector(FamilyOperatorOfLeader::callBackConvene));
		button_admit->setAnchorPoint(ccp(0.5f,0.5f));
		button_admit->setScale9Enable(true);
		button_admit->setScale9Size(CCSizeMake(80,43));
		button_admit->setCapInsets(CCRect(18,9,2,23));
		button_admit->setPosition(ccp(155,106));
		panel_->addChild(button_admit);
// 		UIButton * button_admit = UIButton::create();
// 		button_admit->setPressedActionEnabled(true);
// 		button_admit->setTouchEnable(true);
// 		button_admit->setTextures("res_ui/button_16.png","res_ui/button_16.png","");
// 		button_admit->setFontSize(25);
// 		button_admit->setAnchorPoint(ccp(0.5f,0.5f));
// 		button_admit->setPosition(ccp(155,106));
// 		button_admit->addReleaseEvent(this, coco_releaseselector(FamilyOperatorOfLeader::callBackConvene));
// 		panel_->addChild(button_admit);

		UILabel * labelAdmit= UILabel::create();
		labelAdmit->setAnchorPoint(ccp(0.5f,0.5f));
		labelAdmit->setPosition(ccp(0,0));
		labelAdmit->setFontSize(16);
		labelAdmit->setText(str_admit);
		button_admit->addChild(labelAdmit);

		const char *str_check = StringDataManager::getString("family_shenhe");
		//、审核
		button_check= UIButton::create();
		button_check->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		button_check->setTouchEnable(true);
		button_check->setPressedActionEnabled(true);
		button_check->addReleaseEvent(this,coco_releaseselector(FamilyOperatorOfLeader::callBackCheck));
		button_check->setAnchorPoint(ccp(0.5f,0.5f));
		button_check->setScale9Enable(true);
		button_check->setScale9Size(CCSizeMake(80,43));
		button_check->setCapInsets(CCRect(18,9,2,23));
		button_check->setPosition(ccp(244,106));
		panel_->addChild(button_check);

// 		UIButton * button_check = UIButton::create();
// 		button_check->setPressedActionEnabled(true);
// 		button_check->setTouchEnable(true);
// 		button_check->setTextures("res_ui/button_16.png","res_ui/button_16.png","");
// 		button_check->setFontSize(25);
// 		button_check->setAnchorPoint(ccp(0.5f,0.5f));
// 		button_check->setPosition(ccp(244,106));
// 		button_check->addReleaseEvent(this, coco_releaseselector(FamilyOperatorOfLeader::callBackCheck));
// 		panel_->addChild(button_check);

		UILabel * labelcheck= UILabel::create();
		labelcheck->setAnchorPoint(ccp(0.5f,0.5f));
		labelcheck->setPosition(ccp(0,0));
		labelcheck->setFontSize(16);
		labelcheck->setText(str_check);
		labelcheck->setZOrder(10);
		button_check->addChild(labelcheck);
	
		const char *str_convene = StringDataManager::getString("family_zhaoji");
		// 召集
		UIButton * button_convene= UIButton::create();
		button_convene->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		button_convene->setTouchEnable(true);
		button_convene->setPressedActionEnabled(true);
		button_convene->addReleaseEvent(this,coco_releaseselector(FamilyOperatorOfLeader::callBackCallTogether));
		button_convene->setAnchorPoint(ccp(0.5f,0.5f));
		button_convene->setScale9Enable(true);
		button_convene->setScale9Size(CCSizeMake(80,43));
		button_convene->setCapInsets(CCRect(18,9,2,23));
		button_convene->setPosition(ccp(155,53));
		panel_->addChild(button_convene);

// 		UIButton * button_convene = UIButton::create();
// 		button_convene->setPressedActionEnabled(true);
// 		button_convene->setTouchEnable(true);
// 		button_convene->setTextures("res_ui/button_16.png","res_ui/button_16.png","");
// 		button_convene->setFontSize(16);
// 		button_convene->setAnchorPoint(ccp(0.5f,0.5f));
// 		button_convene->setPosition(ccp(155,53));
// 		button_convene->addReleaseEvent(this, coco_releaseselector(FamilyOperatorOfLeader::callBackCallTogether));
// 		panel_->addChild(button_convene);

		UILabel * labelConvene= UILabel::create();
		labelConvene->setAnchorPoint(ccp(0.5f,0.5f));
		labelConvene->setPosition(ccp(0,0));
		labelConvene->setFontSize(16);
		labelConvene->setText(str_convene);
		button_convene->addChild(labelConvene);
		
		const char *str_dismiss = StringDataManager::getString("family_jiesan");
		//解散
		UIButton * button_dismiss= UIButton::create();
		button_dismiss->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		button_dismiss->setTouchEnable(true);
		button_dismiss->setPressedActionEnabled(true);
		button_dismiss->addReleaseEvent(this,coco_releaseselector(FamilyOperatorOfLeader::callBackDismissSure));
		button_dismiss->setAnchorPoint(ccp(0.5f,0.5f));
		button_dismiss->setScale9Enable(true);
		button_dismiss->setScale9Size(CCSizeMake(80,43));
		button_dismiss->setCapInsets(CCRect(18,9,2,23));
		button_dismiss->setPosition(ccp(244,53));
		//button_dismiss->setPosition(ccp(155,53));
		panel_->addChild(button_dismiss);

// 		UIButton * button_dismiss = UIButton::create();
// 		button_dismiss->setPressedActionEnabled(true);
// 		button_dismiss->setTouchEnable(true);
// 		button_dismiss->setTextures("res_ui/button_16.png","res_ui/button_16.png","");
// 		button_dismiss->setFontSize(25);
// 		button_dismiss->setAnchorPoint(ccp(0.5f,0.5f));
// 		button_dismiss->setPosition(ccp(244,53));
// 		button_dismiss->addReleaseEvent(this, coco_releaseselector(FamilyOperatorOfLeader::callBackDismissSure));
// 		panel_->addChild(button_dismiss);

		UILabel * labeldismiss= UILabel::create();
		labeldismiss->setAnchorPoint(ccp(0.5f,0.5f));
		labeldismiss->setPosition(ccp(0,0));
		labeldismiss->setFontSize(16);
		labeldismiss->setText(str_dismiss);
		button_dismiss->addChild(labeldismiss);
		
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(background->getSize());
		return true;
	}
	return false;
}

void FamilyOperatorOfLeader::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FamilyOperatorOfLeader::onExit()
{
	UIScene::onExit();
}

void FamilyOperatorOfLeader::callBackConvene( CCObject * obj )
{
	//招收
	this->closeAnim();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1508,(void*)5);
}

void FamilyOperatorOfLeader::callBackCheck( CCObject * obj )
{
	this->closeAnim();
	//审核
	ManageFamily * manageFamily =ManageFamily::create();
	manageFamily->ignoreAnchorPointForPosition(false);
	manageFamily->setAnchorPoint(ccp(0.5f,0.5f));
	manageFamily->setPosition(ccp(winsize.width/2,winsize.height/2));
	manageFamily->setTag(kTagManageFamily);
	GameView::getInstance()->getMainUIScene()->addChild(manageFamily);
}

void FamilyOperatorOfLeader::callBackCallTogether( CCObject * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1530);
	/*
	const char *strings_ = StringDataManager::getString("feature_will_be_open");
	GameView::getInstance()->showAlertDialog(strings_);
	*/
	//召集
	this->closeAnim();
}

void FamilyOperatorOfLeader::callBackDismissSure( CCObject * obj )
{
	const char *strings = StringDataManager::getString("family_btnmember_DismissSure");
	GameView::getInstance()->showPopupWindow(strings,2,this,coco_selectselector(FamilyOperatorOfLeader::callBackDismiss),NULL);
	
	this->closeAnim();
}

void FamilyOperatorOfLeader::callBackDismiss( CCObject * obj )
{
	FamilyUI * familyui_ =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	//解散
	if (familyui_->selfPost_==1)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1510,this);
	}else
	{
		const char *strings = StringDataManager::getString("family_DismissOfleader");
		GameView::getInstance()->showAlertDialog(strings);
	}
}

void FamilyOperatorOfLeader::callBackClose( CCObject * obj )
{
	this->closeAnim();
}

bool FamilyOperatorOfLeader::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}



/////////////////////////////////////////
FamilyChangeNotice::FamilyChangeNotice()
{

}
FamilyChangeNotice::~FamilyChangeNotice()
{

}

FamilyChangeNotice * FamilyChangeNotice::create(int type)
{
	FamilyChangeNotice * familyNotice =new FamilyChangeNotice();
	if (familyNotice && familyNotice->init(type))
	{
		familyNotice->autorelease();
		return familyNotice;
	}
	CC_SAFE_DELETE(familyNotice);
	return NULL;
}

bool FamilyChangeNotice::init(int type)
{
	if (UIScene::init())
	{
		CCSize winsize =CCDirector::sharedDirector()->getVisibleSize();

		UIPanel *mainPanel=(UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/family_ChangePuipose_popupo_1.json");
		mainPanel->setAnchorPoint(ccp(0,0));
		mainPanel->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mainPanel);

		UILayer *loadLayer= UILayer::create();
		loadLayer->ignoreAnchorPointForPosition(false);
		loadLayer->setAnchorPoint(ccp(0.5f,0.5f));
		loadLayer->setPosition(ccp(mainPanel->getContentSize().width/2,mainPanel->getContentSize().height/2));
		loadLayer->setContentSize(mainPanel->getContentSize());
		this->addChild(loadLayer);
		////intputText
// 		UIImageView * image_ =UIImageView::create();
// 		image_->setTexture("res_ui/tietu_flower.png");
// 		image_->setAnchorPoint(ccp(0,0));
// 		image_->setPosition(ccp(45,loadLayer->getContentSize().height/2+50));
// 		loadLayer->addWidget(image_);

		UILabelBMFont * label_ = (UILabelBMFont*)UIHelper::seekWidgetByName(mainPanel,"LabelBMFont_tip");
		if (type == 1)
		{
			const char * secondStr = StringDataManager::getString("family_genggaizongzhi");
			label_->setText(secondStr);
		}else
		{
			const char * secondStr = StringDataManager::getString("family_genggaixuanyang");
			label_->setText(secondStr);
		}

		textFamilyNotice=new RichTextInputBox();
		textFamilyNotice->setMultiLinesMode(true);
		textFamilyNotice->setInputBoxWidth(265);
		textFamilyNotice->setInputBoxHeight(130);
		textFamilyNotice->setDirection(RichTextInputBox::kCCRichInputDirectionVertical);
		textFamilyNotice->setAnchorPoint(ccp(0,0));
		textFamilyNotice->setPosition(ccp(45,loadLayer->getContentSize().height/2+30));
		loadLayer->addChild(textFamilyNotice,10);
		textFamilyNotice->autorelease();

		UIButton * btnSure_ =(UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_enter");
		btnSure_->setTouchEnable(true);
		btnSure_->setPressedActionEnabled(true);
		btnSure_->setTag(type);
		btnSure_->addReleaseEvent(this,coco_releaseselector(FamilyChangeNotice::callBackSure));

		UIButton * btnCancel_ =(UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_cancel");
		btnCancel_->setTouchEnable(true);
		btnCancel_->setPressedActionEnabled(true);
		btnCancel_->addReleaseEvent(this,coco_releaseselector(FamilyChangeNotice::callBackCancel));

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(mainPanel->getContentSize());
		return true;
	}
	return false;
}

void FamilyChangeNotice::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FamilyChangeNotice::onExit()
{
	UIScene::onExit();
}

bool FamilyChangeNotice::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return true;
}

void FamilyChangeNotice::callBackSure( CCObject * obj )
{
	FamilyUI * familyui_ =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	UIButton * btn = (UIButton *)obj;
	familyui_->familyNotice =(char *)textFamilyNotice->getInputString();
	if (btn->getTag() ==1 )
	{
		if (familyui_->familyNotice != NULL)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1506,(void*)familyui_->familyNotice);
		}
	}else
	{
		if (familyui_->familyNotice != NULL)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1523,(void*)familyui_->familyNotice);
		}
	}
	
	this->closeAnim();
}

void FamilyChangeNotice::callBackCancel( CCObject * obj )
{
	this->closeAnim();
}


