#ifndef _FAMILY_CREATEFAMILY_H
#define _FAMILY_CREATEFAMILY_H
#include "../extensions/UIScene.h"
class CGuildMemberBase;
class FamilyUI;
class ManageFamily:public UIScene,public cocos2d::extension::CCTableViewDataSource,public cocos2d::extension::CCTableViewDelegate
{
public:
	ManageFamily(void);
	~ManageFamily(void);


	static ManageFamily *create();
	bool init();
	void onEnter();
	void onExit();

	void callBackExit(CCObject * obj);
	void callBackAgree(CCObject * obj);
	void callBAckRefuse(CCObject * obj);
public:
	////familyMember
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	// default implements are used to call script callback if exist
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
	CCTableView *tableviewstate;
private:
	FamilyUI * familyui_;
};


#endif