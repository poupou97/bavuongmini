#include "FamilyShopCount.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/GameMessageProcessor.h"


FamilyShopCount::FamilyShopCount(void)
{
}


FamilyShopCount::~FamilyShopCount(void)
{
}

FamilyShopCount * FamilyShopCount::create(CGuildCommodity * commodity)
{
	FamilyShopCount * count_ =new FamilyShopCount();
	if (count_ && count_->init(commodity))
	{
		count_->autorelease();
		return count_;
	}
	CC_SAFE_DELETE(count_);
	return NULL;
}

bool FamilyShopCount::init(CGuildCommodity * commodity)
{
	if (UIScene::init())
	{
		winsize = CCDirector::sharedDirector()->getVisibleSize();

		u_layer = UILayer::create();
		u_layer->ignoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(ccp(0.5f,0.5f));
		u_layer->setContentSize(CCSizeMake(298, 403));
		u_layer->setPosition(CCPointZero);
		u_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		addChild(u_layer);
		//加载UI
		if(LoadSceneLayer::BuyCalculatorsLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::BuyCalculatorsLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (UIPanel*)LoadSceneLayer::s_pPanel->copy();
		UIPanel *ppanel = LoadSceneLayer::BuyCalculatorsLayer;
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(298, 403));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		ppanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(ppanel);

		//关闭按钮
		UIButton * Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(FamilyShopCount::CloseEvent));
		Button_close->setPressedActionEnabled(true);
		//物品Icon底框
		UIImageView * imageView_dikuang = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_dikuang");
		switch(commodity->goods().quality())
		{
		case 1 :
			{
				imageView_dikuang->setTexture("res_ui/smdi_white.png");
			}
			break;
		case 2 :
			{
				imageView_dikuang->setTexture("res_ui/smdi_green.png");
			}
			break;
		case 3 :
			{
				imageView_dikuang->setTexture("res_ui/smdi_bule.png");
			}
			break;
		case 4 :
			{
				imageView_dikuang->setTexture("res_ui/smdi_purple.png");
			}
			break;
		case 5 :
			{
				imageView_dikuang->setTexture("res_ui/smdi_orange.png");
			}
			break;
		}

		std::string headImageStr ="res_ui/props_icon/";
		headImageStr.append(commodity->goods().icon());
		headImageStr.append(".png");
		UIImageView * imageView_icon = UIImageView::create();
		imageView_icon->setTexture(headImageStr.c_str());
		imageView_icon->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_icon->setPosition(ccp(62,344));
		imageView_icon->setScale(0.85f);
		u_layer->addWidget(imageView_icon);

		UILabel * l_single_name = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_single_name");
		l_single_name->setVisible(true);
		l_single_name->setText(StringDataManager::getString("measurement_contribute"));
		UILabel * l_total_name = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_total_name");
		l_total_name->setVisible(true);
		l_total_name->setText(StringDataManager::getString("measurement_contribute"));

		//单价的单位图标(元宝)
		UIImageView * ImageView_IngotOrCoins_single = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_IngotOrCoins_single");
		ImageView_IngotOrCoins_single->setVisible(false);

		//总价的单位图标(元宝)
		UIImageView * ImageView_IngotOrCoins_total = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_IngotOrCoins_total");
		ImageView_IngotOrCoins_total->setVisible(false);
		//物品名称
		UILabel * l_name = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_name");
		l_name->setText(commodity->goods().name().c_str());
		l_name->setColor(GameView::getInstance()->getGoodsColorByQuality(commodity->goods().quality()));
		//单价 
		basePrice = commodity->guildhonor();
		UILabel *l_priceOfOne = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_SingleValue");
		l_priceOfOne->setPosition(ccp(185,334));
		char s_basePrice[20];
		sprintf(s_basePrice,"%d",basePrice);
		l_priceOfOne->setText(s_basePrice);
		//购买数量  
		l_buyNum = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_amount");
		l_buyNum->setText("");
		//总价  
		l_totalPrice = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_TotalValue");
		l_totalPrice->setText("");
		l_totalPrice->setPosition(ccp(166,267));

		int index = 1;
		for (int i=0;i<3;i++)
		{
			for (int j=0;j<3;j++)
			{

				char str_index[2];
				sprintf(str_index,"%d",index);

				UIButton *extractAnnex=UIButton::create();
				extractAnnex->setTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
				extractAnnex->setTouchEnable(true);
				extractAnnex->setPressedActionEnabled(true);
				extractAnnex->setAnchorPoint(ccp(0.5f,0.5f));
				extractAnnex->setWidgetTag(index);
				//extractAnnex->setPosition(ccp(31+j*61,185- i*50));
				extractAnnex->setPosition(ccp(60+j*61,207- i*50));
				extractAnnex->addReleaseEvent(this,coco_releaseselector(FamilyShopCount::getAmount));

				UILabel *label_Num=UILabel::create();
				label_Num->setText(str_index);
				label_Num->setFontSize(20);
				label_Num->setAnchorPoint(ccp(0.5f,0.5f));
				label_Num->setPosition(ccp(0,0));
				extractAnnex->addChild(label_Num);
				u_layer->addWidget(extractAnnex);
				/*
				UITextButton *extractAnnex=UITextButton::create();
				extractAnnex->setTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
				extractAnnex->setTouchEnable(true);
				extractAnnex->setText(str_index);
				extractAnnex->setFontSize(20);
				extractAnnex->setAnchorPoint(ccp(0,0));
				extractAnnex->setWidgetTag(index);
				extractAnnex->setPosition(ccp(31+j*61,185- i*50));
				extractAnnex->addReleaseEvent(this,coco_releaseselector(FamilyShopCount::getAmount));
				u_layer->addWidget(extractAnnex);
				*/
				index++;
			}
		}

		UIButton * btn_clear=UIButton::create();
		btn_clear->setTouchEnable(true);
		btn_clear->setTextures("res_ui/enjian_y.png","res_ui/enjian_y.png","");
		btn_clear->setAnchorPoint(ccp(0,0));
		btn_clear->setPosition(ccp(216,185));
		btn_clear->setWidgetTag(index);
		btn_clear->addReleaseEvent(this,coco_releaseselector(FamilyShopCount::deleteNum));
		u_layer->addWidget(btn_clear);
		UIImageView * deleteSp=UIImageView::create();
		deleteSp->setTexture("res_ui/jisuanqi/jiantou.png");
		deleteSp->setAnchorPoint(ccp(0.5f,0.5f));
		deleteSp->setPosition(ccp(btn_clear->getContentSize().width/2,btn_clear->getContentSize().height/2));
		btn_clear->addChild(deleteSp);
		/*
		UITextButton *btn_Zero=UITextButton::create();
		btn_Zero->setText("0");
		btn_Zero->setTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		btn_Zero->setTouchEnable(true);
		btn_Zero->setFontSize(25);
		btn_Zero->setAnchorPoint(ccp(0,0));
		btn_Zero->setWidgetTag(0);
		btn_Zero->setPosition(ccp(216,135));
		btn_Zero->addReleaseEvent(this,coco_releaseselector(FamilyShopCount::getAmount));
		u_layer->addWidget(btn_Zero);
		*/
		UIButton *button_Zero=UIButton::create();
		button_Zero->setTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		button_Zero->setTouchEnable(true);
		button_Zero->setFontSize(25);
		button_Zero->setAnchorPoint(ccp(0.5f,0.5f));
		button_Zero->setWidgetTag(0);
		button_Zero->setPosition(ccp(242,157));
		button_Zero->setPressedActionEnabled(true);
		button_Zero->addReleaseEvent(this,coco_releaseselector(FamilyShopCount::getAmount));
		u_layer->addWidget(button_Zero);

		UILabel *label_Zero=UILabel::create();
		label_Zero->setText("0");
		label_Zero->setFontSize(20);
		label_Zero->setAnchorPoint(ccp(0.5f,0.5f));
		label_Zero->setPosition(ccp(0,0));
		button_Zero->addChild(label_Zero);

		/*
		UITextButton *btn_C=UITextButton::create();
		btn_C->setText("C");
		btn_C->setTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		btn_C->setTouchEnable(true);
		btn_C->setFontSize(25);
		btn_C->setAnchorPoint(ccp(0,0));
		btn_C->setPosition(ccp(216,85));
		btn_C->addReleaseEvent(this,coco_releaseselector(FamilyShopCount::clearNum));
		u_layer->addWidget(btn_C);
		*/
		UIButton *Button_C=UIButton::create();
		Button_C->setTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		Button_C->setTouchEnable(true);
		Button_C->setAnchorPoint(ccp(0.5f,0.5f));
		Button_C->setPosition(ccp(242,107));
		Button_C->setPressedActionEnabled(true);
		Button_C->addReleaseEvent(this,coco_releaseselector(FamilyShopCount::clearNum));
		u_layer->addWidget(Button_C);

		UILabel *label_C=UILabel::create();
		label_C->setText("C");
		label_C->setFontSize(20);
		label_C->setAnchorPoint(ccp(0.5f,0.5f));
		label_C->setPosition(ccp(0,0));
		Button_C->addChild(label_C);

		UIButton * btn_buy = (UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_enter");
		btn_buy->setTouchEnable(true);
		btn_buy->addReleaseEvent(this, coco_releaseselector(FamilyShopCount::BuyEvent));
		btn_buy->setTag(commodity->id());
		btn_buy->setPressedActionEnabled(true);
		UILabel * l_buy = (UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_enter");
		const char *str3 = StringDataManager::getString("goods_auction_buy");
		char* shop_buy =const_cast<char*>(str3);
		l_buy->setText(shop_buy);

		setToDefaultNum();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setAnchorPoint(ccp(0,0));
		return true;
	}
	return false;
}

void FamilyShopCount::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FamilyShopCount::onExit()
{
	UIScene::onExit();
}

bool FamilyShopCount::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return true;
}

void FamilyShopCount::BuyEvent( CCObject * pSender )
{
	if (buyNum > 0)
	{
		UIButton * btn = (UIButton *)pSender;
		int id_ =btn->getTag();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1709,(void*)id_,(void *)buyNum);
	}
	else
	{
		const char *str1 = StringDataManager::getString("goods_shop_pleaseInputNum");
		char* pleaseinputNum =const_cast<char*>(str1);
		GameView::getInstance()->showAlertDialog(pleaseinputNum);
	}
	this->closeAnim();
}

void FamilyShopCount::CloseEvent( CCObject * pSender )
{
	this->closeAnim();
}

void FamilyShopCount::getAmount( CCObject * obj )
{
	UITextButton * btn =(UITextButton *)obj;
	int id_= btn->getWidgetTag();

	if (buyNum>999)
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("goldStore_buy_inputNum"));
		return;
	}
// 	char num[3];
// 	sprintf(num,"%d",id_);
// 	str_buyNum.append(num);
	if (buyNum <= 0 && id_ == 0)
	{
		return;
	}
	char num[3];
	sprintf(num,"%d",id_);
	if (std::atoi(str_buyNum.c_str()) <=0)
	{
		str_buyNum = num;
	}else
	{
		str_buyNum.append(num);
	}
	l_buyNum->setText(str_buyNum.c_str());
	buyNum =std::atoi(str_buyNum.c_str());

	buyPrice = basePrice * buyNum;
	char str_buyPrice [20];
	sprintf(str_buyPrice,"%d",buyPrice);
	l_totalPrice->setText(str_buyPrice);
	/*
	if (buyPrice <= GameView::getInstance()->myplayer->player->honorpoint())   //钱购买
	{
		l_totalPrice->setColor(ccc3(225,255,138));   //黄色
	}
	else
	{
		l_totalPrice->setColor(ccc3(255,51,51));   //红色
	}
	*/
}

void FamilyShopCount::clearNum( CCObject * obj )
{
	str_buyNum.clear();
	l_buyNum->setText(str_buyNum.c_str());
	buyNum =std::atoi(str_buyNum.c_str());

	l_totalPrice->setText("");
}

void FamilyShopCount::deleteNum( CCObject * obj )
{
	if (str_buyNum.size()>0)
	{
		str_buyNum.erase(str_buyNum.end()-1);
		l_buyNum->setText(str_buyNum.c_str());
		buyNum =std::atoi(str_buyNum.c_str());

		buyPrice = basePrice * buyNum;
		char str_buyPrice [20];
		sprintf(str_buyPrice,"%d",buyPrice);
		l_totalPrice->setText(str_buyPrice);

		/*
		if (buyPrice <= GameView::getInstance()->myplayer->player->honorpoint())   //荣誉值足够
		{
			l_totalPrice->setColor(ccc3(225,255,138));   //黄色
		}
		else
		{
			l_totalPrice->setColor(ccc3(255,51,51));   //红色
		}
		*/
	}
}

void FamilyShopCount::setToDefaultNum()
{
	l_buyNum->setText("1");
	buyNum = 1;

	buyPrice = basePrice * buyNum;
	char str_buyPrice [20];
	sprintf(str_buyPrice,"%d",buyPrice);
	l_totalPrice->setText(str_buyPrice);
	/*
	if (buyPrice <= GameView::getInstance()->myplayer->player->honorpoint())   //钱购买
	{
		l_totalPrice->setColor(ccc3(225,255,138));   //黄色
	}
	else
	{
		l_totalPrice->setColor(ccc3(255,51,51));   //红色
	}
	*/
}
