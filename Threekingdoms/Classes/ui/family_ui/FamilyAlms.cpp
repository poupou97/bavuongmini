#include "FamilyAlms.h"
#include "../extensions/Counter.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/FolderInfo.h"
#include "FamilyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CGuildDonateOption.h"


FamilyAlms::FamilyAlms(void)
{
	contributionCount =0;
}


FamilyAlms::~FamilyAlms(void)
{
}

FamilyAlms * FamilyAlms::create()
{
	FamilyAlms * alms = new FamilyAlms ();
	if (alms && alms->init())
	{
		alms->autorelease();
		return alms;
	}
	CC_SAFE_DELETE(alms);
	return NULL;
}

bool FamilyAlms::init()
{
	if (UIScene::init())
	{
		CCSize winsize  = CCDirector::sharedDirector()->getVisibleSize();
		UIPanel * mainPanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/family_alms_1.json");
		mainPanel->setAnchorPoint(ccp(0.5f,0.5f));
		mainPanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(mainPanel);
		 
		familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		labelFamilyLevel = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"Label_familyLvValue");
		labelFamilyContrbution = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"Label_familyLvValue_0");
		labelContrbutionOfMine = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"Label_familyLvValue_0_0");

		UIButton * btn_Input = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_shurukuang");
		btn_Input->setTouchEnable(true);
		btn_Input->addReleaseEvent(this,coco_releaseselector(FamilyAlms::callBackInput));

		UIButton * btn_SurContrbution = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_enter");
		btn_SurContrbution->setTouchEnable(true);
		btn_SurContrbution->setPressedActionEnabled(true);
		btn_SurContrbution->addReleaseEvent(this,coco_releaseselector(FamilyAlms::callBackSurContrbution));

		labelCount = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"showCountGetNum");
		familyContrbutionAdd = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"familyContrbutionPersonAdd");
		familyPersonContrbutionAdd = (UILabel*)UIHelper::seekWidgetByName(mainPanel,"familyContrbutionValueAdd");

		UIButton * btnClose = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_close");
		btnClose->setTouchEnable(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addReleaseEvent(this,coco_releaseselector(FamilyAlms::callBackClose));

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void FamilyAlms::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FamilyAlms::onExit()
{
	UIScene::onExit();
}

void FamilyAlms::callBackInput( CCObject * obj )
{
	if (this->isClosing())
		return;

	GameView::getInstance()->showCounter(this, callfuncO_selector(FamilyAlms::callBackGetCountNum));
}

void FamilyAlms::callBackSurContrbution( CCObject * obj )
{
	if (this->getCurContrGoodsAmount() > 0)
	{
		if (contributionCount >0)
		{
			if (familyui->guilidDonateVector.size() > 0)
			{
				//暂定家族贡品只有一个 
				//如果以后添加 则家族贡品名字也需要变更
				std::string propid_ = familyui->guilidDonateVector.at(0)->propid();
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1509,(void *)propid_.c_str(),(void *)contributionCount);
			}
		}else
		{
			const char * string_ = StringDataManager::getString("familyContrbutionAmount");
			GameView::getInstance()->showAlertDialog(string_);
		}
	}else
	{
		const char * string_ = StringDataManager::getString("familyNotContrbutionAmout");
		GameView::getInstance()->showAlertDialog(string_);
	}
}

void FamilyAlms::callBackClose( CCObject * obj )
{
	this->closeAnim();
}

void FamilyAlms::callBackGetCountNum( CCObject * obj )
{
	Counter * counter_ =(Counter *)obj;
	int num = counter_->getInputNum();
	if (num >this->getCurContrGoodsAmount())
	{
		contributionCount = this->getCurContrGoodsAmount();
	}else
	{
		contributionCount = num;
	}
	char labelNum[10];
	sprintf(labelNum,"%d",contributionCount);
	labelCount->setText(labelNum);
}

int FamilyAlms::getCurContrGoodsAmount()
{
	int contrGoodsAmount = 0;
	
	int size = GameView::getInstance()->AllPacItem.size();
	for (int i =0;i<size;i++)
	{
		if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
		{
			std::string pacGoodsId = GameView::getInstance()->AllPacItem.at(i)->goods().id();

			for (int j = 0;j<familyui->guilidDonateVector.size();j++)
			{
				std::string contrbutionPropId = familyui->guilidDonateVector.at(j)->propid();
				if (strcmp(pacGoodsId.c_str(),contrbutionPropId.c_str())==0)
				{
					contrGoodsAmount +=GameView::getInstance()->AllPacItem.at(i)->quantity();
				}
			}
		}
	}
	return contrGoodsAmount;
}

void FamilyAlms::refreshInfo(bool isDefault)
{
	int familyLevel = familyui->familyLevel;
	char familyLevelStr[10];
	sprintf(familyLevelStr,"%d",familyLevel);
	labelFamilyLevel->setText(familyLevelStr);

	if (isDefault == true)
	{
		contributionCount = this->getCurContrGoodsAmount();
		char showStrContrbution[10];
		sprintf(showStrContrbution,"%d",this->getCurContrGoodsAmount());
		labelCount->setText(showStrContrbution);
	}else
	{
		contributionCount = 0;
		labelCount->setText("0");
	}
	
	long long familyContrbutionOfMine = familyui->curFamilyContrbution;
	long long familyContrbution = familyui->nextFamilyContrbution;
	std::string showContrbution = "";
	if (familyContrbution > 0 )
	{
		char contrbutionMine[10];
		sprintf(contrbutionMine,"%d",familyContrbutionOfMine);
		char contrbutionFamily[20];
		sprintf(contrbutionFamily,"%ld",familyContrbution);

		showContrbution.append(contrbutionMine);
		showContrbution.append("/");
		showContrbution.append(contrbutionFamily);
	}else
	{
		const char * familyContrbution_  = StringDataManager::getString("familyLveleIsFull");
		showContrbution.append(familyContrbution_);
	}
	labelFamilyContrbution->setText(showContrbution.c_str());
	//暂定家族贡品只有一个 
	//如果以后添加 则家族贡品名字也需要变更
	const char * remStr_ = StringDataManager::getString("family_jiazugongpinshengyu");
	char remContrbutionStr[20];
	sprintf(remContrbutionStr,"%d",this->getCurContrGoodsAmount());
	std::string showContrbutionOfMine= "(";
	showContrbutionOfMine.append(remStr_);
	showContrbutionOfMine.append(remContrbutionStr);
	showContrbutionOfMine.append(")");
	labelContrbutionOfMine->setText(showContrbutionOfMine.c_str());

	char famStr[20];
	sprintf(famStr,"%d",familyui->guilidDonateVector.at(0)->donate());
	familyContrbutionAdd->setText(famStr);

	char perStr[20];
	sprintf(perStr,"%d",familyui->guilidDonateVector.at(0)->personaldonate());
	familyPersonContrbutionAdd->setText(perStr);
}
