
#ifndef FAMILY_FAMILYMEMBER_OPERATOR
#define FAMILY_FAMILYMEMBER_OPERATOR

#include "../extensions/UIScene.h"

class FamilyMemberOperator:public UIScene
{
public:
	FamilyMemberOperator(void);
	~FamilyMemberOperator(void);
	
	enum
	{
		FAMILYLEADER=1,
		FAMILYVICELEADER,
		FAMILYMEMBER,
	};

	enum
	{
		operationCheck=0,
		operationFriend,
		operationprivate,
		operationAppoint,
		operationKickout,
		operationRemove,
		operationDeise,
		operationBlack,
	};

	static FamilyMemberOperator *create(int post_,int index_);
	bool init(int post_,int index_);
	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void leaderAndMember(CCObject * obj);
	void leaderAndViceLeader(CCObject * obj);
	void viceLeaderAndMember(CCObject * obj);
	void viceLeaderAndLeader(CCObject * obj);
	void pressionOfMember(CCObject * obj);

	void isSureKickOut(CCObject * obj);
	void isSureDemis(CCObject * obj);
	void isSureAppoint(CCObject * obj);
	void isSureRemove(CCObject * obj);

	void removeSelf(CCObject * obj);
private:
	CCSize winSize;
	int opherPost_;

	long long otherplayerId_;
	std::string otherPlayerName_;
	int otherCountry;
	int otherViplevel;
	int otherLevel;
	int otherPression;
};

#endif;
