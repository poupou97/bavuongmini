
#ifndef _FAMILY_APPLYFAMILY_H
#define _FAMILY_APPLYFAMILY_H
#include "../extensions/UIScene.h"

class CGuildBase;
class ApplyFamily:public UIScene,public cocos2d::extension::CCTableViewDataSource,public cocos2d::extension::CCTableViewDelegate
{
public:
	ApplyFamily(void);
	~ApplyFamily(void);

	static ApplyFamily * create();
	bool init();

	void onEnter();
	void onExit();

	void callBackExit(CCObject * obj);
	void callBackCheck(CCObject * obj);
	void callBackApply(CCObject * obj);
public:
	////familyMember
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	// default implements are used to call script callback if exist
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
public:
	CCTableView *tableviewstate;
	std::vector<CGuildBase *>vectorGuildBase;
	long long selfGuildId;
	int curPage;
	bool hasNextPage;
};

////////////////
class ApplyChackFamily:public UIScene
{
public:
	ApplyChackFamily(void);
	~ApplyChackFamily(void);

	static ApplyChackFamily * create(std::string name,std::string leaderName,long long familyId,std::string membernum,std::string familyNotice);
	bool init(std::string name,std::string leaderName,long long familyId,std::string membernum,std::string familyNotice);

	void onEnter();
	void onExit();
	void callBackApply(CCObject * obj);
	void callBack(CCObject * obj);
};



#endif