#include "ApplyFamily.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CGuildBase.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../extensions/CCRichLabel.h"
#include "../../ui/Rank_ui/RankUI.h"
#include "../../gamescene_state/role/MyPlayer.h"

ApplyFamily::ApplyFamily(void)
{
	selfGuildId =-10;
	curPage = 0;
	hasNextPage =false;
}


ApplyFamily::~ApplyFamily(void)
{
	std::vector<CGuildBase *>::iterator iter;
	for (iter=vectorGuildBase.begin();iter!=vectorGuildBase.end();iter++)
	{
		delete * iter;
	}
	vectorGuildBase.clear();
}

ApplyFamily * ApplyFamily::create()
{
	ApplyFamily * family_=new ApplyFamily();
	if (family_ && family_->init())
	{
		family_->autorelease();
		return family_;
	}
	CC_SAFE_DELETE(family_);
	return NULL;
}

bool ApplyFamily::init()
{
	if (UIScene::init())
	{
		CCSize winsize= CCDirector::sharedDirector()->getVisibleSize();
		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		if(LoadSceneLayer::familyApplyPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::familyApplyPanel->removeFromParentAndCleanup(false);
		}

		UIPanel *mainPanel=LoadSceneLayer::familyApplyPanel;
		mainPanel->setAnchorPoint(ccp(0.5f,0.5f));
		mainPanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(mainPanel);

		UILayer *loadLayer= UILayer::create();
		loadLayer->ignoreAnchorPointForPosition(false);
		loadLayer->setAnchorPoint(ccp(0.5f,0.5f));
		loadLayer->setPosition(ccp(winsize.width/2,winsize.height/2));
		loadLayer->setContentSize(CCSizeMake(800,480));
		this->addChild(loadLayer);


		const char * secondStr = StringDataManager::getString("chat_diaoshi_family_jia");
		const char * thirdStr = StringDataManager::getString("chat_diaoshi_family_zu");
		CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(ccp(30,240));
		loadLayer->addChild(atmature);

		tableviewstate = CCTableView::create(this, CCSizeMake(650,360));
		tableviewstate->setSelectedEnable(true);
		tableviewstate->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 24, 1, 1), ccp(0,0));
		//tableviewstate->setPressedActionEnabled(true);
		tableviewstate->setDirection(kCCScrollViewDirectionVertical);
		tableviewstate->setAnchorPoint(ccp(0,0));
		tableviewstate->setPosition(ccp(85,45));
		tableviewstate->setDelegate(this);
		tableviewstate->setVerticalFillOrder(kCCTableViewFillTopDown);
		loadLayer->addChild(tableviewstate);
		tableviewstate->reloadData();

		UIButton * btnClose =(UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_close");
		btnClose->setTouchEnable(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addReleaseEvent(this,coco_releaseselector(ApplyFamily::callBackExit));


		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);


		return true;
	}
	return false;
}

void ApplyFamily::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ApplyFamily::onExit()
{
	UIScene::onExit();
}

void ApplyFamily::callBackExit( CCObject * obj )
{
	this->closeAnim();
}

void ApplyFamily::callBackCheck( CCObject * obj )
{
	CCLOG("check  1504");
	CCMenuItemSprite * menu_ =(CCMenuItemSprite *)obj;
	int tag_ =menu_->getTag();

	std::string familyName = vectorGuildBase.at(tag_)->name();
	std::string familyLeaderName = vectorGuildBase.at(tag_)->leadername();
	int familyCurMember  =vectorGuildBase.at(tag_)->currmembernumber();
	int familyMaxMember  = vectorGuildBase.at(tag_)->maxmembernumber();
	long long familyId = vectorGuildBase.at(tag_)->id();
	std::string familyNotice = vectorGuildBase.at(tag_)->declaration();

	char curmemberStr[10];
	sprintf(curmemberStr,"%d",familyCurMember);
	char maxMemberStr[10];
	sprintf(maxMemberStr,"%d",familyMaxMember);
	std::string memberStr_ ="";
	memberStr_.append(curmemberStr);
	memberStr_.append("/");
	memberStr_.append(maxMemberStr);

	CCSize winsize= CCDirector::sharedDirector()->getVisibleSize();
	ApplyChackFamily * checkFamily =ApplyChackFamily::create(familyName,familyLeaderName,familyId,memberStr_.c_str(),familyNotice.c_str());
	checkFamily->ignoreAnchorPointForPosition(false);
	checkFamily->setAnchorPoint(ccp(0.5,0.5f));
	checkFamily->setPosition(ccp(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(checkFamily);
}


void ApplyFamily::callBackApply( CCObject * obj )
{
	CCMenuItemSprite * menu_ =(CCMenuItemSprite *)obj;
	int tag_ =menu_->getTag();

	if (selfGuildId == vectorGuildBase.at(tag_)->id())
	{
		return;
	}
	
	for (int i =0; i< vectorGuildBase.size();i++)
	{
		if (selfGuildId == vectorGuildBase.at(i)->id())
		{
			selfGuildId = vectorGuildBase.at(tag_)->id();
			tableviewstate->updateCellAtIndex(i);
		}
	}
	long long familyId =vectorGuildBase.at(tag_)->id();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1502,(void *)familyId);
	selfGuildId = familyId;
	tableviewstate->updateCellAtIndex(tag_);
// 	CCTableViewCell* cell = tableviewstate->cellAtIndex(tag_);
// 	tableviewstate->addSelectedFlagForCell(cell);
}

void ApplyFamily::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}


void ApplyFamily::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void ApplyFamily::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	int  cellidx = cell->getIdx();

}

cocos2d::CCSize ApplyFamily::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(645,60);
}

cocos2d::extension::CCTableViewCell* ApplyFamily::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString * string =CCString::createWithFormat("%d",idx);	
	CCTableViewCell *cell =table->dequeueCell();
	cell=new CCTableViewCell();
	cell->autorelease();
	////��Ҫ��ҳ
	CCScale9Sprite * spbg = CCScale9Sprite::create("res_ui/kuang0_new.png");
	spbg->setPreferredSize(CCSizeMake(645,60));
	spbg->setCapInsets(CCRect(15,30,1,1));
	spbg->setAnchorPoint(ccp(0,0));
	spbg->setPosition(ccp(0,0));
	cell->addChild(spbg);
	
	std::string familyName = vectorGuildBase.at(idx)->name();
	CCLabelTTF * labelName= CCLabelTTF::create(familyName.c_str(), APP_FONT_NAME,16);
	labelName->setAnchorPoint(ccp(0,0));
	labelName->setPosition(ccp(15,25));
	cell->addChild(labelName);

	std::string familyLeaderName = vectorGuildBase.at(idx)->leadername();
	CCLabelTTF * labelManage= CCLabelTTF::create(familyLeaderName.c_str(), APP_FONT_NAME,16);
	labelManage->setAnchorPoint(ccp(0,0));
	labelManage->setPosition(ccp(125,25));
	cell->addChild(labelManage);

	int familyLevel = vectorGuildBase.at(idx)->level();
	char levelStr[5];
	sprintf(levelStr,"%d",familyLevel);
	CCLabelTTF * labelLevel= CCLabelTTF::create(levelStr, APP_FONT_NAME,16);
	labelLevel->setAnchorPoint(ccp(0,0));
	labelLevel->setPosition(ccp(230,25));
	cell->addChild(labelLevel);

	int familyCurNum = vectorGuildBase.at(idx)->currmembernumber();
	int familyMaxNum = vectorGuildBase.at(idx)->maxmembernumber();

	char familyCurMember[10];
	sprintf(familyCurMember,"%d",familyCurNum);
	char familyMaxMember[10];
	sprintf(familyMaxMember,"%d",familyMaxNum);
	
	std::string showFamilyMember =familyCurMember;
	showFamilyMember.append("/");
	showFamilyMember.append(familyMaxMember);

	CCLabelTTF * labelNumber= CCLabelTTF::create(showFamilyMember.c_str(), APP_FONT_NAME,16);
	labelNumber->setAnchorPoint(ccp(0,0));
	labelNumber->setPosition(ccp(310,25));
	cell->addChild(labelNumber);
	
	CCScale9Sprite * bg_check = CCScale9Sprite::create("res_ui/new_button_1.png");
	bg_check->setPreferredSize(CCSizeMake(80,36));
	bg_check->setCapInsets(CCRect(18,9,2,23));
	CCMenuItemSprite *btn_check = CCMenuItemSprite::create(bg_check, bg_check, bg_check, this, menu_selector(ApplyFamily::callBackCheck));
	btn_check->setZoomScale(0.75f);
	btn_check->setTag(idx);
	CCMoveableMenu *menu_check=CCMoveableMenu::create(btn_check,NULL);
	menu_check->setAnchorPoint(ccp(0,0));
	menu_check->setPosition(ccp(450,33));
	cell->addChild(menu_check);
	const char *strings_check = StringDataManager::getString("family_apply_btncheck");
	CCLabelTTF * label_check= CCLabelTTF::create(strings_check,APP_FONT_NAME,16);
	label_check->setAnchorPoint(ccp(0.5f,0.5f));
	label_check->setPosition(ccp(bg_check->getContentSize().width/2,bg_check->getContentSize().height/2));
	btn_check->addChild(label_check);

	long long guildId_ = vectorGuildBase.at(idx)->id();

	const char *strings_apply;
	if (selfGuildId == guildId_)
	{
		strings_apply = StringDataManager::getString("family_apply_btnapply_already");
	}else
	{
		strings_apply = StringDataManager::getString("family_apply_btnapply");
	}

	char * str_apply=const_cast<char*>(strings_apply);
	CCScale9Sprite * bg_apply = CCScale9Sprite::create("res_ui/new_button_2.png");
	bg_apply->setPreferredSize(CCSizeMake(80,36));
	bg_apply->setCapInsets(CCRect(18,9,2,23));
	CCMenuItemSprite *btn_apply = CCMenuItemSprite::create(bg_apply, bg_apply, bg_apply, this, menu_selector(ApplyFamily::callBackApply));
	btn_apply->setTag(idx);
	btn_apply->setZoomScale(0.75f);
	CCMoveableMenu *menu_apply=CCMoveableMenu::create(btn_apply,NULL);
	menu_apply->setAnchorPoint(ccp(0,0));
	menu_apply->setPosition(ccp(550,33));
	cell->addChild(menu_apply);	
	CCLabelTTF * label_apply= CCLabelTTF::create(str_apply,APP_FONT_NAME,16);
	label_apply->setAnchorPoint(ccp(0.5f,0.5f));
	label_apply->setPosition(ccp(bg_apply->getContentSize().width/2,bg_apply->getContentSize().height/2));
	label_apply->setTag(idx);
	btn_apply->addChild(label_apply);

	if(idx == vectorGuildBase.size()-3)
	{
		if (hasNextPage == true)
		{
			//req for next
			curPage++;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1501,(void *)curPage);
		}
	}
	return cell;
}

unsigned int ApplyFamily::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	return vectorGuildBase.size();
}

void ApplyFamily::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ApplyFamily::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ApplyFamily::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

/////////////////////////////////////////////

ApplyChackFamily::ApplyChackFamily( void )
{

}

ApplyChackFamily::~ApplyChackFamily( void )
{

}

ApplyChackFamily * ApplyChackFamily::create(std::string name,std::string leaderName,long long familyId,std::string membernum,std::string familyNotice)
{
	ApplyChackFamily * checkfamily =new ApplyChackFamily();
	if (checkfamily && checkfamily->init(name,leaderName,familyId,membernum,familyNotice))
	{
		checkfamily->autorelease();
		return checkfamily;
	}
	CC_SAFE_DELETE(checkfamily);
	return NULL;
}

bool ApplyChackFamily::init(std::string name,std::string leaderName,long long familyId,std::string membernum,std::string familyNotice)
{
	if (UIScene::init())
	{
		CCSize size= CCDirector::sharedDirector()->getVisibleSize();
		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(size);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		UIPanel * ppanel = (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/seeFamily_1.json");
		ppanel->setAnchorPoint(ccp(0.5f,0.5f));
		ppanel->setPosition(ccp(size.width/2,size.height/2));
		m_pUiLayer->addWidget(ppanel);

		UILayer *loadLayer= UILayer::create();
		loadLayer->ignoreAnchorPointForPosition(false);
		loadLayer->setAnchorPoint(ccp(0.5f,0.5f));
		loadLayer->setPosition(ccp(size.width/2,size.height/2));
		loadLayer->setContentSize(CCSizeMake(600,360));
		this->addChild(loadLayer);

		UILabel * labelFamilyName =(UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_name_value");
		UILabel * labelFamilyLeaderName =(UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_sedname_value");
		UILabel * labelFamilyMemberNum =(UILabel *)UIHelper::seekWidgetByName(ppanel,"Label_NumberOfPeople_value");
		labelFamilyName->setText(name.c_str());
		labelFamilyLeaderName->setText(leaderName.c_str());
		labelFamilyMemberNum->setText(membernum.c_str());

		if (strlen(familyNotice.c_str()) <= 0)
		{
			const char * familyWriteIsEmpty  = StringDataManager::getString("familyMangerIsNotWrite");
			familyNotice.append(familyWriteIsEmpty);
		}

		CCRichLabel * familyNoticeLabel = CCRichLabel::createWithString(familyNotice.c_str(), CCSizeMake(360, 0), NULL, NULL, 0, 20.f, 1);
		familyNoticeLabel->setAnchorPoint(ccp(0,0));
		familyNoticeLabel->setPosition(ccp(0,0));

		CCScrollView * noticeScrollView = CCScrollView::create(CCSizeMake(360,115));
		noticeScrollView->setContentSize(CCSizeMake(360,115));
		noticeScrollView->ignoreAnchorPointForPosition(false);
		noticeScrollView->setTouchEnabled(true);
		noticeScrollView->setDirection(kCCScrollViewDirectionVertical);
		noticeScrollView->setAnchorPoint(ccp(0.5f,0));
		noticeScrollView->setPosition(ccp(348,110));
		noticeScrollView->setBounceable(true);
		noticeScrollView->setClippingToBounds(true);
		loadLayer->addChild(noticeScrollView);	
		noticeScrollView->addChild(familyNoticeLabel);

		int h_ =familyNoticeLabel->getContentSize().height;
		if (h_ < noticeScrollView->getContentSize().height)
		{
			h_ =noticeScrollView->getContentSize().height;
		}

		noticeScrollView->setContentSize(CCSizeMake(360,h_));
		familyNoticeLabel->setPosition(ccp(0,noticeScrollView->getContentSize().height - familyNoticeLabel->getContentSize().height));


		UIButton * applyFamily =(UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_enter");
		applyFamily->setPressedActionEnabled(true);
		applyFamily->setTouchEnable(true);
		applyFamily->setWidgetTag(familyId);
		applyFamily->addReleaseEvent(this,coco_releaseselector(ApplyChackFamily::callBackApply));

		UIButton * closeButton =(UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_close");
		closeButton->setPressedActionEnabled(true);
		closeButton->setTouchEnable(true);
		closeButton->addReleaseEvent(this,coco_releaseselector(ApplyChackFamily::callBack));

		setTouchEnabled(true);
		setTouchMode(kCCTouchesOneByOne);
		setContentSize(size);
		return true;
	}
	return false;
}

void ApplyChackFamily::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ApplyChackFamily::onExit()
{
	UIScene::onExit();
}

void ApplyChackFamily::callBack( CCObject * obj )
{
	this->closeAnim();
}

void ApplyChackFamily::callBackApply( CCObject * obj )
{
	UIButton * button = (UIButton*)obj;
	long long familyId_ = button->getWidgetTag();
	ApplyFamily * applyFamily = (ApplyFamily *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagApplyFamilyUI);
	if (applyFamily != NULL)
	{
		if (applyFamily->selfGuildId == familyId_)
		{
			return;
		}
		int cellIndex_ = -1;
		
		for (int i =0; i< applyFamily->vectorGuildBase.size();i++)
		{
			if (applyFamily->selfGuildId == applyFamily->vectorGuildBase.at(i)->id())
			{
				applyFamily->selfGuildId = familyId_;
				applyFamily->tableviewstate->updateCellAtIndex(i);
			}

			if (familyId_ == applyFamily->vectorGuildBase.at(i)->id())
			{
				cellIndex_ = i;
			}
		}
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1502,(void *)familyId_);
		if (cellIndex_ >= 0)
		{
			applyFamily->selfGuildId = familyId_;
			applyFamily->tableviewstate->updateCellAtIndex(cellIndex_);
		}
	}
	
	RankUI * rankui_ = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
	if (rankui_ != NULL)
	{
		if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionid() > 0)
		{
			std::string strings_  = StringDataManager::getString("rank_youhaveFamily");
			GameView::getInstance()->showAlertDialog(strings_.c_str());
		}else
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1502,(void *)familyId_);
			//std::string strings_  = StringDataManager::getString("rank_applyFamilyIssend");
			//GameView::getInstance()->showAlertDialog(strings_.c_str());
		}
	}
}
