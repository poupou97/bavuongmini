
#ifndef _FAMILYUIMAIN_H_
#define _FAMILYUIMAIN_H_
#include "../extensions/UIScene.h"
#include "../../messageclient/element/CGuildCommodity.h"
#include "../extensions/UITab.h"

class CGuildBase;
class CGuildMemberBase;
class CGuildDonateOption;
class CCRichLabel;
class FamilyFightUI;

#define familyShoptag 100
#define familyAlms 110
#define familyLeaderManager 120
class FamilyUI:public UIScene,public cocos2d::extension::CCTableViewDataSource,public cocos2d::extension::CCTableViewDelegate
{
public:
	FamilyUI(void);
	~FamilyUI(void);
	static FamilyUI * create();
	bool init();
	void onEnter();
	void onExit();
	void callBackExit(CCObject * obj);

	UIPanel * mainPanel;
	UIPanel * familyInfo_panel;
	UIPanel * familyMember_panel;
	UIPanel * familyState_panel;
	UIPanel * familyState_panel_infoIsNUll;
	void changeUITabIndex(CCObject * obj);

	CCRichLabel *familyNoticeLabel;
	CCScrollView * noticeScrollView;
	UIButton * btn_repairNotice;
	CCRichLabel *familyManifestoLabel;
	CCScrollView * manifestoScrollView;
	UIButton * btn_repairManifesto;

	void repairFamilyNotice(CCObject * obj);
	void repairFamilyManifesto(CCObject * obj);

	void callBackManageFamily(CCObject * obj);
	void memberQuitFamily(CCObject * obj);

	void callbackFamilyUplLevel(CCObject * obj);
	void callbackFamilyShop(CCObject * obj);
	void callbackFamilyAlms(CCObject * obj);

	void manageQuitFamily(CCObject * obj);
	void quitFamilySure(CCObject * obj);
	void downListMember(int index);
	void callBackDesFamily(CCObject * obj);
public:
	CCSize winsize;
	int currType;

	char * familyNotice;
	UILayer *loadLayer;
	bool hasNextPageOfManageMember;
	int curPageOfManageFamilyMember;
	bool hasNextPageOfMember;
	int curPageOfFamilyMember;
public:
	////familyMember
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	// default implements are used to call script callback if exist
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

public:
	CCTableView *tableviewMember;
	std::vector<CGuildMemberBase *>vectorApplyGuildMember;
	std::vector<CGuildMemberBase *>vectorGuildMemberBase;
	std::vector<CGuildBase *>vectorGuildbase;
	UILayer * layerMember;
	CCTableView *tableviewstate;
	UILayer * layerstate;
	UIButton * btn_mamage;
	UIButton * btn_familyUplevel;

	UILabel * label_familyName;
	UILabel * label_familyLevel;
	UILabel * label_familyNumber;

	UILabel * label_familyContribution;
	UILabel * label_myPost;
	//UILabel * label_myAllContribution;
	UILabel * label_myRemContribution;
	////player post
	int selfPost_ ;
	long long selfId_;
	long long remPersonContrbution;
	long long curFamilyContrbution;
	long long nextFamilyContrbution;
	//捐献每一贡品家族获取的贡献值
	int familyAddContrbution;
	//捐献每一贡品个人获取的贡献值
	int personAddContrbution;
	//family num
	int curFamilyMemberNum;
	int maxFamilyMemberNum;


	std::vector<CGuildDonateOption *> guilidDonateVector;

	int familyLevel;
	//family shop vector
	std::vector<CGuildCommodity *> guildCommodityVector;

	//add by yangjun 2014.7.15
private:
	UILayer * l_FamilyFightLayer;
	UIPanel * panel_familyFight;

	UITab * m_familytab;

	void initFamilyFightUI();

public:
	FamilyFightUI * GetFamilyFightUI();
	// LiuLiang++ open the FamilyFightUI
	void openFamilyFightUI();
};

#endif