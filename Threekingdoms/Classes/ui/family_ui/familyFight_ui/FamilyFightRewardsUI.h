
#ifndef _FAMILYFIGHTUI_FAMILYFIGHTREWARDSUI_H_
#define _FAMILYFIGHTUI_FAMILYFIGHTREWARDSUI_H_

#include "../../extensions/UIScene.h"

class FamilyFightRewardsUI : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	FamilyFightRewardsUI();
	~FamilyFightRewardsUI();

	static FamilyFightRewardsUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void ReloadTableViewWithOutChangeOffSet();
	void RefreshMyRanking();

private:
	UILayer * m_base_layer;
	CCTableView * m_tableView;
	UILabel * l_myRanking;

private:
	void CloseEvent(CCObject * pSender);
	void getRewardEvent(CCObject * pSender);
};

#endif

