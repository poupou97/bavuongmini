#include "familyFightHistoryNotesUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../../GameAudio.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../AppMacros.h"
#include "GameView.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../utils/GameUtils.h"
#include "../../../messageclient/element/CDeadHistory.h"
#include "../../../messageclient/element/CBoss.h"
#include "../../../utils/GameConfig.h"
#include "FamilyFightData.h"
#include "../../../messageclient/element/CGuildFightRecord.h"
#include "../../../messageclient/GameMessageProcessor.h"

#define  SelectImageTag 458

familyFightHistoryNotesUI::familyFightHistoryNotesUI(void):
lastSelectCellId(0),
selectCellId(0)
{

}


familyFightHistoryNotesUI::~familyFightHistoryNotesUI(void)
{
	//delete old history
	std::vector<CGuildFightRecord*>::iterator iter_history;
	for (iter_history = familyFightHistoryList.begin(); iter_history != familyFightHistoryList.end(); ++iter_history)
	{
		delete *iter_history;
	}
	familyFightHistoryList.clear();
}

familyFightHistoryNotesUI* familyFightHistoryNotesUI::create()
{
	familyFightHistoryNotesUI * historyNotesUI = new familyFightHistoryNotesUI();
	if (historyNotesUI && historyNotesUI->init())
	{
		historyNotesUI->autorelease();
		return historyNotesUI;
	}
	CC_SAFE_DELETE(historyNotesUI);
	return NULL;
}

bool familyFightHistoryNotesUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		everyPageNum = 20;

		m_base_layer = UILayer::create();
		this->addChild(m_base_layer);	

		initUI();

		return true;
	}
	return false;
}

void familyFightHistoryNotesUI::onEnter()
{
	UIScene::onEnter();
}

void familyFightHistoryNotesUI::onExit()
{
	UIScene::onExit();
}

void familyFightHistoryNotesUI::initUI()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	UIImageView *mengban = UIImageView::create();
	mengban->setTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enable(true);
	mengban->setScale9Size(winSize);
	mengban->setAnchorPoint(ccp(.5f,.5f));
	m_pUiLayer->addWidget(mengban);

	//加载UI
	UIPanel * ppanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/familyfamily_1.json");
	ppanel->setAnchorPoint(ccp(0.0f,0.0f));
	ppanel->setPosition(CCPointZero);	
	ppanel->setTouchEnable(true);
	m_pUiLayer->addWidget(ppanel);
	mengban->setPosition(ccp(ppanel->getContentSize().width/2,ppanel->getContentSize().height/2));

	UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
	btn_close->setTouchEnable( true );
	btn_close->setPressedActionEnabled(true);
	btn_close->addReleaseEvent(this,coco_releaseselector(familyFightHistoryNotesUI::CloseEvent));

	UIPanel * panel_history = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_historyNotes");
	panel_history->setVisible(true);
	UIPanel * panel_ranking = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_integrationRanking");
	panel_ranking->setVisible(false);

	// 主tableView
	m_tableView = CCTableView::create(this, CCSizeMake(665,324));
	//m_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
	//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 25, 1, 1), ccp(0,0));   // 设置九宫格图片
	m_tableView->setDirection(kCCScrollViewDirectionVertical);
	m_tableView->setAnchorPoint(ccp(0, 0));
	m_tableView->setPosition(ccp(77,39));
	m_tableView->setDelegate(this);
	m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	m_tableView->setPressedActionEnabled(false);
	m_base_layer->addChild(m_tableView);

	this->setContentSize(ppanel->getContentSize());
}


bool familyFightHistoryNotesUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return false;
}

void familyFightHistoryNotesUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void familyFightHistoryNotesUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void familyFightHistoryNotesUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void familyFightHistoryNotesUI::scrollViewDidScroll( CCScrollView* view )
{

}

void familyFightHistoryNotesUI::scrollViewDidZoom( CCScrollView* view )
{

}

void familyFightHistoryNotesUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	selectCellId = cell->getIdx();
	//取出上次选中状态，更新本次选中状态
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();
}

cocos2d::CCSize familyFightHistoryNotesUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(665,39);
}

cocos2d::extension::CCTableViewCell* familyFightHistoryNotesUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(CCSize(662,37));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setPosition(ccp(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	CGuildFightRecord * record = familyFightHistoryList.at(idx);
	ccColor3B color_green = ccc3(47,93,13);
	
	// 底图
	CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(ccp(0, 0));
	sprite_bigFrame->setPosition(ccp(1, 0));
	sprite_bigFrame->setCapInsets(CCRectMake(9,14,1,1));
	sprite_bigFrame->setPreferredSize(CCSizeMake(662,37));
	cell->addChild(sprite_bigFrame);

	//战斗结果
	std::string str_result_path;
	if (record->scorechanged() == 3)
	{
		str_result_path = "res_ui/jiazuzhan/wins.png";
	}
	else if (record->scorechanged() == 1)
	{
		str_result_path = "res_ui/jiazuzhan/draws.png";
	}
	else if (record->scorechanged() == -1)
	{
		str_result_path = "res_ui/jiazuzhan/losts.png";
	}
	else
	{
		str_result_path = "";
	}

	if (str_result_path != "")
	{
		CCSprite * sp_result = CCSprite::create(str_result_path.c_str());
		sp_result->setAnchorPoint(ccp(0.5f, 0.5f));
		sp_result->setPosition(ccp(35, 18));
		cell->addChild(sp_result);
	}

	//积分
	std::string str_integration ;
	char s_total[20];
	sprintf(s_total,"%d",record->scoretotal());
	//sprintf(s_total,"%d",200);
	str_integration.append(s_total);
	if (record->scorechanged() != 0)
	{
		str_integration.append("(");
		if (record->scorechanged() > 0)
		{
			str_integration.append("+");
		}
// 		else
// 		{
// 			str_integration.append("-");
// 		}
		char s_change[10];
		sprintf(s_change,"%d",record->scorechanged());
		//sprintf(s_change,"%d",+52);
		str_integration.append(s_change);
		str_integration.append(")");
	}
	
	CCLabelTTF * l_integration = CCLabelTTF::create(str_integration.c_str(), APP_FONT_NAME, 15);
	l_integration->setColor(color_green);
	l_integration->setAnchorPoint(ccp(0.5f, 0.5f));
	l_integration->setPosition(ccp(125, 18));
	cell->addChild(l_integration);

	//对方家族名字
	CCLabelTTF * l_targetFamilyName = CCLabelTTF::create(record->enemyguild().guildname().c_str(), APP_FONT_NAME, 15);
	//CCLabelTTF * l_targetFamilyName = CCLabelTTF::create(StringDataManager::getString("PleaseDownloadNewClient"), APP_FONT_NAME, 15);
	l_targetFamilyName->setColor(color_green);
	l_targetFamilyName->setAnchorPoint(ccp(0.5f, 0.5f));
	l_targetFamilyName->setPosition(ccp(240, 18));
	cell->addChild(l_targetFamilyName);
	//对方家族等级
	char s_lv[10];
	sprintf(s_lv,"%d",record->enemyguild().guildlevel());
	//sprintf(s_lv,"%d",5);
	CCLabelTTF * l_targetFamilyLevel = CCLabelTTF::create(s_lv, APP_FONT_NAME, 15);
	l_targetFamilyLevel->setColor(color_green);
	l_targetFamilyLevel->setAnchorPoint(ccp(0.5f, 0.5f));
	l_targetFamilyLevel->setPosition(ccp(337, 18));
	cell->addChild(l_targetFamilyLevel);
	//对方家族组长名字
	CCLabelTTF * l_targetFamilyLeaderName = CCLabelTTF::create(record->enemyguild().guildleadername().c_str(), APP_FONT_NAME, 15);
	//CCLabelTTF * l_targetFamilyLeaderName = CCLabelTTF::create(StringDataManager::getString("CheckVersion"), APP_FONT_NAME, 15);
	l_targetFamilyLeaderName->setColor(color_green);
	l_targetFamilyLeaderName->setAnchorPoint(ccp(0.0f, 0.5f));
	l_targetFamilyLeaderName->setPosition(ccp(410, 18));
	cell->addChild(l_targetFamilyLeaderName);

	int countryId = record->enemyguild().guildleadercountry();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		CCSprite * countrySp_ = CCSprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(ccp(1.0,0.5f));
		countrySp_->setPosition(ccp(l_targetFamilyLeaderName->getPositionX()-1,l_targetFamilyLeaderName->getPositionY()));
		countrySp_->setScale(0.7f);
		cell->addChild(countrySp_);
	}

	//vipInfo
	// vip开关控制
	bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
	if (vipEnabled)
	{
		if (record->enemyguild().guildleaderviplevel() > 0)
		{
			CCNode *pNode = MainScene::addVipInfoByLevelForNode(record->enemyguild().guildleaderviplevel());
			if (pNode)
			{
				cell->addChild(pNode);
				pNode->setPosition(ccp(l_targetFamilyLeaderName->getPosition().x+l_targetFamilyLeaderName->getContentSize().width+13,l_targetFamilyLeaderName->getPosition().y));
			}
		}
	}

	// 时间
	CCLabelTTF * l_date = CCLabelTTF::create(record->fightdate().c_str(), APP_FONT_NAME, 15, CCSizeMake(200, 0), kCCTextAlignmentLeft);
	//CCLabelTTF * l_date = CCLabelTTF::create("2014/05/06", APP_FONT_NAME, 15, CCSizeMake(200, 0), kCCTextAlignmentLeft);
	l_date->setColor(color_green);
	l_date->setAnchorPoint(ccp(0.5f, 0.5f));
	l_date->setPosition(ccp(660, 18));
	cell->addChild(l_date);

	if (this->selectCellId == idx)
	{
		pHighlightSpr->setVisible(true);
	}


	if (getIsCanReq())
	{
		if(idx == familyFightHistoryList.size()-4)
		{
			if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
			{
				//req for next
				this->isReqNewly = false;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1533,(void*)(this->s_mCurPage+1),(void*)everyPageNum);
				setIsCanReq(false);
			}
		}
	}

	return cell;
}

unsigned int familyFightHistoryNotesUI::numberOfCellsInTableView( CCTableView *table )
{
	return familyFightHistoryList.size();
}

void familyFightHistoryNotesUI::refreshUI()
{
	lastSelectCellId = 0;
	selectCellId = 0;
	m_tableView->reloadData();
}

void familyFightHistoryNotesUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}
