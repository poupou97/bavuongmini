#include "BattleSituationUI.h"
#include "../../extensions/UIScene.h"
#include "../../../messageclient/element/CGuildBattleInfo.h"
#include "AppMacros.h"
#include "../../../utils/GameConfig.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/role/BasePlayer.h"
#include "../../../messageclient/GameMessageProcessor.h"


#define  SelectImageTag 458

#define kTag_TableView_blue 500
#define kTag_TableView_red 510

#define Action_Progress_Speed 160

BattleSituationUI::BattleSituationUI():
lastSelectCellId(0),
selectCellId(0)
{
}


BattleSituationUI::~BattleSituationUI()
{
	delete blueInfo;
	delete redInfo;
}

BattleSituationUI * BattleSituationUI::create()
{
	BattleSituationUI * battleSituationUI = new BattleSituationUI();
	if (battleSituationUI && battleSituationUI->init())
	{
		battleSituationUI->autorelease();
		return battleSituationUI;
	}
	CC_SAFE_DELETE(battleSituationUI);
	return NULL;
}

bool BattleSituationUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate(); 

		blueInfo = new CGuildBattleInfo();
		redInfo = new CGuildBattleInfo();;

		u_layer = UILayer::create();
		addChild(u_layer);

		//加载UI
		UIPanel * ppanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/zhankuang_1.json");
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->setPosition(CCPointZero);	
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton *Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(BattleSituationUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		l_blueName = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_nameA");
		l_redName = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_nameB");
// 		ImageView_blueBlood = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_bossHpA");
// 		ImageView_redBlood = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_bossHpB");
// 		l_blueBloodValue= (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_bossHpA");
// 		l_redBloodValue = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_bossHpBValue");

		pt_blueBlood = CCProgressTimer::create(CCSprite::create("res_ui/jiazuzhan/bosshp2.png"));
		pt_blueBlood->setType(kCCProgressTimerTypeBar);
		pt_blueBlood->setMidpoint(ccp(0,0));
		pt_blueBlood->setBarChangeRate(ccp(1, 0));
		addChild(pt_blueBlood);
		pt_blueBlood->setAnchorPoint(ccp(0,0));
		pt_blueBlood->setPosition(ccp(110, 348));		pt_redBlood = CCProgressTimer::create(CCSprite::create("res_ui/jiazuzhan/bosshp2.png"));
		pt_redBlood->setType(kCCProgressTimerTypeBar);
		pt_redBlood->setMidpoint(ccp(0,0));
		pt_redBlood->setBarChangeRate(ccp(1, 0));
		addChild(pt_redBlood);
		pt_redBlood->setAnchorPoint(ccp(0,0));
		pt_redBlood->setPosition(ccp(505, 348));		l_blueBloodValue = CCLabelTTF::create("",APP_FONT_NAME,14);		l_blueBloodValue->setAnchorPoint(ccp(.5f,.5f));		l_blueBloodValue->setPosition(ccp(204,356));		addChild(l_blueBloodValue);

		l_redBloodValue = CCLabelTTF::create("",APP_FONT_NAME,14);		l_redBloodValue->setAnchorPoint(ccp(.5f,.5f));		l_redBloodValue->setPosition(ccp(599,356));		addChild(l_redBloodValue);

		m_tableView_blue = CCTableView::create(this,CCSizeMake(329,245));
		m_tableView_blue->setDirection(kCCScrollViewDirectionVertical);
		m_tableView_blue->setAnchorPoint(ccp(0,0));
		m_tableView_blue->setPosition(ccp(62,48));
		m_tableView_blue->setDelegate(this);
		m_tableView_blue->setTag(kTag_TableView_blue);
		m_tableView_blue->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(m_tableView_blue);

		m_tableView_red = CCTableView::create(this,CCSizeMake(329,245));
		m_tableView_red->setDirection(kCCScrollViewDirectionVertical);
		m_tableView_red->setAnchorPoint(ccp(0,0));
		m_tableView_red->setPosition(ccp(411,48));
		m_tableView_red->setDelegate(this);
		m_tableView_red->setTag(kTag_TableView_red);
		m_tableView_red->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(m_tableView_red);

		this->setContentSize(ppanel->getContentSize());
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void BattleSituationUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1548);
}

void BattleSituationUI::onExit()
{
	UIScene::onExit();
}

bool BattleSituationUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void BattleSituationUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void BattleSituationUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void BattleSituationUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void BattleSituationUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void BattleSituationUI::scrollViewDidScroll( CCScrollView* view )
{

}

void BattleSituationUI::scrollViewDidZoom( CCScrollView* view )
{

}

void BattleSituationUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	selectCellId = cell->getIdx();
	//取出上次选中状态，更新本次选中状态
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();
}

cocos2d::CCSize BattleSituationUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(326,38);
}

cocos2d::extension::CCTableViewCell* BattleSituationUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CGuildBattleInfo* tempInfo;
 	if (table->getTag() == kTag_TableView_blue)
	{
		tempInfo = blueInfo;
	}
	else
	{
		tempInfo = redInfo;
	}

	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(CCSize(326,37));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setPosition(ccp(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

 	ccColor3B color_green = ccc3(47,93,13);

	// 底图
	CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(ccp(0, 0));
	sprite_bigFrame->setPosition(ccp(1, 0));
	sprite_bigFrame->setCapInsets(CCRectMake(9,14,1,1));
	sprite_bigFrame->setPreferredSize(CCSizeMake(326,37));
	cell->addChild(sprite_bigFrame);

 	//名称
  	CCLabelTTF * l_name = CCLabelTTF::create(tempInfo->players(idx).rolename().c_str(), APP_FONT_NAME, 15);
  	l_name->setColor(color_green);
  	l_name->setAnchorPoint(ccp(0.0f, 0.5f));
  	l_name->setPosition(ccp(30, 18));
  	cell->addChild(l_name);
  
  	int countryId = tempInfo->players(idx).country();
  	if (countryId > 0 && countryId <6)
  	{
  		std::string countryIdName = "country_";
  		char id[2];
  		sprintf(id, "%d", countryId);
  		countryIdName.append(id);
  		std::string iconPathName = "res_ui/country_icon/";
  		iconPathName.append(countryIdName);
  		iconPathName.append(".png");
  		CCSprite * countrySp_ = CCSprite::create(iconPathName.c_str());
  		countrySp_->setAnchorPoint(ccp(1.0,0.5f));
  		countrySp_->setPosition(ccp(l_name->getPositionX()-1,l_name->getPositionY()));
  		countrySp_->setScale(0.7f);
  		cell->addChild(countrySp_);
  	}
  	//vipInfo
  	// vip开关控制
  	bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
  	if (vipEnabled)
  	{
  		if (tempInfo->players(idx).viplevel() > 0)
  		{
  			CCNode *pNode = MainScene::addVipInfoByLevelForNode(tempInfo->players(idx).viplevel());
  			if (pNode)
  			{
  				cell->addChild(pNode);
  				pNode->setPosition(ccp(l_name->getPosition().x+l_name->getContentSize().width+13,l_name->getPosition().y));
  			}
  		}
  	}

	//职业
	CCLabelTTF * l_profession = CCLabelTTF::create(BasePlayer::getProfessionNameIdxByIndex(tempInfo->players(idx).profession()).c_str(), APP_FONT_NAME, 15);
	l_profession->setColor(color_green);
	l_profession->setAnchorPoint(ccp(0.5f, 0.5f));
	l_profession->setPosition(ccp(165, 18));
	cell->addChild(l_profession);
	//等级
	char s_lv[10];
	sprintf(s_lv,"%d",tempInfo->players(idx).rolelevel());
	CCLabelTTF * l_lv = CCLabelTTF::create(s_lv, APP_FONT_NAME, 15);
	l_lv->setColor(color_green);
	l_lv->setAnchorPoint(ccp(0.5f, 0.5f));
	l_lv->setPosition(ccp(205, 18));
	cell->addChild(l_lv);
	//杀人数
	char s_killNumber[20];
	sprintf(s_killNumber,"%d",tempInfo->players(idx).killed());
	CCLabelTTF * l_killNumber = CCLabelTTF::create(s_killNumber, APP_FONT_NAME, 15);
	l_killNumber->setColor(color_green);
	l_killNumber->setAnchorPoint(ccp(0.5f, 0.5f));
	l_killNumber->setPosition(ccp(253, 18));
	cell->addChild(l_killNumber);
	//死亡数
	char s_deadNumber[20];
	sprintf(s_deadNumber,"%d",tempInfo->players(idx).death());
	CCLabelTTF * l_deadNumber = CCLabelTTF::create(s_deadNumber, APP_FONT_NAME, 15);
	l_deadNumber->setColor(color_green);
	l_deadNumber->setAnchorPoint(ccp(0.5f, 0.5f));
	l_deadNumber->setPosition(ccp(301, 18));
	cell->addChild(l_deadNumber);

	return cell;
}

unsigned int BattleSituationUI::numberOfCellsInTableView( CCTableView *table )
{
	if (table->getTag() == kTag_TableView_blue)
	{
		return blueInfo->players_size();
	}
	else if (table->getTag() == kTag_TableView_red)
	{
		return redInfo->players_size();
	}
	else
	{
		return 0;
	}
}

void BattleSituationUI::RefreshOwnData()
{
	m_tableView_blue->reloadData();
}

void BattleSituationUI::RefreshOwnDataWithOutChangeOffSet()
{
	CCPoint _s = m_tableView_blue->getContentOffset();
	int _h = m_tableView_blue->getContentSize().height + _s.y;
	m_tableView_blue->reloadData();
	CCPoint temp = ccp(_s.x,_h - m_tableView_blue->getContentSize().height);
	m_tableView_blue->setContentOffset(temp); 
}

void BattleSituationUI::RefreshEmptyData()
{
	m_tableView_red->reloadData();
}

void BattleSituationUI::RefreshEmptyDataWithOutChangeOffSet()
{
	CCPoint _s = m_tableView_red->getContentOffset();
	int _h = m_tableView_red->getContentSize().height + _s.y;
	m_tableView_red->reloadData();
	CCPoint temp = ccp(_s.x,_h - m_tableView_red->getContentSize().height);
	m_tableView_red->setContentOffset(temp); 
}

void BattleSituationUI::RefreshUI()
{
	l_blueName->setText(blueInfo->guildname().c_str());
	l_redName->setText(redInfo->guildname().c_str());;

	//ImageView_blueBlood->setTextureRect(CCRectMake(0,0,ImageView_blueBlood->getContentSize().width*blueInfo->bosshp()*1.0f/100.f,ImageView_blueBlood->getContentSize().height));
	//ImageView_redBlood->setTextureRect(CCRectMake(0,0,ImageView_redBlood->getContentSize().width*redInfo->bosshp()*1.0f/100.f,ImageView_redBlood->getContentSize().height));

	CCProgressTo * pt_anm_blue = CCProgressTo::create((100-pt_blueBlood->getPercentage())*1.0f/Action_Progress_Speed, blueInfo->bosshp()*1.0f);
	pt_blueBlood->runAction(pt_anm_blue);

	CCProgressTo * pt_anm_red= CCProgressTo::create((100-pt_redBlood->getPercentage())*1.0f/Action_Progress_Speed, redInfo->bosshp()*1.0f);
	pt_redBlood->runAction(pt_anm_red);

	std::string blueBloodValue;
	char s_blueBloodValue[20];
	sprintf(s_blueBloodValue,"%d",blueInfo->bosshp());
	blueBloodValue.append(s_blueBloodValue);
	blueBloodValue.append("%");
	l_blueBloodValue->setString(blueBloodValue.c_str());

	std::string redBloodValue;
	char s_redBloodValue[20];
	sprintf(s_redBloodValue,"%d",redInfo->bosshp());
	redBloodValue.append(s_redBloodValue);
	redBloodValue.append("%");
	l_redBloodValue->setString(redBloodValue.c_str());
}
