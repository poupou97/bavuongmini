

#ifndef _UI_FAMILYFIGHTUI_FAMILYFIGHTRESULTUI_H_
#define _UI_FAMILYFIGHTUI_FAMILYFIGHTRESULTUI_H_

#include "../../extensions/UIScene.h"
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../../messageclient/GameProtobuf.h"
#include "../../../messageclient/protobuf/GuildMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

USING_NS_CC;
USING_NS_CC_EXT;

class FamilyFightResultUI : public UIScene
{
public:
	FamilyFightResultUI();
	~FamilyFightResultUI();

	static FamilyFightResultUI * create(Push1543 fightResult);
	bool init(Push1543 fightResult);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void QuitEvent(CCObject *pSender);
	void exitFamilyFight();

	virtual void update(float dt);

	void ArmatureFinishCallback(cocos2d::extension::CCArmature *armature, cocos2d::extension::MovementEventType movementType, const char *movementID);

protected:
	void createUI();
	void createAnimation();

	void showUIAnimation();
	void startSlowMotion();

	void addLightAnimation();

public:
	void endSlowMotion();

private:
	UILayer *layer_anm;

	UILabel *l_remainTime;
	int m_nRemainTime;

	long long countDown_startTime;

	Push1543 curResultInfo;
};

#endif

