#include "FamilyFightCountDownUI.h"
#include "AppMacros.h"
#include "GameView.h"
#include "../../../utils/GameUtils.h"
#include "../../../utils/StaticDataManager.h"


FamilyFightCountDownUI::FamilyFightCountDownUI():
m_nRemainTime(5000)
{
}


FamilyFightCountDownUI::~FamilyFightCountDownUI()
{
}

FamilyFightCountDownUI * FamilyFightCountDownUI::create()
{
	FamilyFightCountDownUI * familyFightCountDownUI = new FamilyFightCountDownUI();
	if (familyFightCountDownUI && familyFightCountDownUI->init())
	{
		familyFightCountDownUI->autorelease();
		return familyFightCountDownUI;
	}
	CC_SAFE_DELETE(familyFightCountDownUI);
	return NULL;
}

bool FamilyFightCountDownUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		UIImageView * imageView_Frame = UIImageView::create();
		imageView_Frame->setTexture("res_ui/LV4_dia.png");
		imageView_Frame->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_Frame->setScale9Enable(true);
		imageView_Frame->setCapInsets(CCRectMake(19,19,1,1));
		imageView_Frame->setScale9Size(CCSizeMake(250,160));
		imageView_Frame->setPosition(ccp(winsize.width/2,winsize.height*0.8f - 80));
		m_pUiLayer->addWidget(imageView_Frame);

		UILabel * l_des = UILabel::create();
		l_des->setText(StringDataManager::getString("FamilyFightUI_alive_des"));
		l_des->setTextAreaSize(CCSizeMake(203,0));
		l_des->setTextHorizontalAlignment(kCCTextAlignmentLeft);
		l_des->setTextVerticalAlignment(kCCVerticalTextAlignmentCenter);
		l_des->setFontName(APP_FONT_NAME);
		l_des->setFontSize(20);
		l_des->setAnchorPoint(ccp(.5f,.5f));
		l_des->setPosition(ccp(0,-40));
		l_des->setColor(ccc3(243,252,2));
		imageView_Frame->addChild(l_des);

		//����ʱ��ʾ
		imageView_countDown = UIImageView::create();
		imageView_countDown->setTexture("res_ui/font/5.png");
		imageView_countDown->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_countDown->setPosition(ccp(0,30));
		imageView_Frame->addChild(imageView_countDown);

		float space = m_nRemainTime*1.0f/m_nRemainTime;

		this->schedule(schedule_selector(FamilyFightCountDownUI::update),1.0f);

		this->setContentSize(winsize);
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void FamilyFightCountDownUI::onEnter()
{
	UIScene::onEnter();
}

void FamilyFightCountDownUI::onExit()
{
	UIScene::onExit();
}

bool FamilyFightCountDownUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void FamilyFightCountDownUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightCountDownUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightCountDownUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void FamilyFightCountDownUI::update( float dt )
{
	m_nRemainTime -= 1000;
	if (m_nRemainTime <= 0)
	{
		m_nRemainTime = 0;
		//this->removeFromParent();
	}

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	std::string str_path = "res_ui/font/";
	str_path.append(s_remainTime);
	str_path.append(".png");
	imageView_countDown->setTexture(str_path.c_str());
}
