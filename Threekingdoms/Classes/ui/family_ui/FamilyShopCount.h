#ifndef _FAMILYSHOPCOUNT_
#define _FAMILYSHOPCOUNT_
#include "../extensions/UIScene.h"
#include "../../messageclient/element/CGuildCommodity.h"

class FamilyShopCount:public UIScene
{
public:
	FamilyShopCount(void);
	~FamilyShopCount(void);

	static FamilyShopCount * create(CGuildCommodity * commodity);
	bool init(CGuildCommodity * commodity);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);


	void BuyEvent(CCObject * pSender);
	void CloseEvent(CCObject * pSender);

	void getAmount(CCObject * obj);
	void clearNum(CCObject * obj);
	void deleteNum(CCObject * obj);

	void setToDefaultNum();
private:
	CCSize winsize;
	UILayer * u_layer;
	//UILabel * l_priceOfOne;
	UILabel * l_buyNum;
	UILabel * l_totalPrice;


	UITextButton * btn_inputNumValue;
	UILabel * l_constGoldValue;

	//该物品单价
	int basePrice;
	//购买的数量
	int buyNum;
	std::string str_buyNum;
	//购买的总价
	int buyPrice;

	//单价 
	UILabel * l_priceOfOne;
};
#endif;
