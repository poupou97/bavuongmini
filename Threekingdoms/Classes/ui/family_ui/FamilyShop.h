#ifndef _FAMILYUIMAINSHOP_H_
#define _FAMILYUIMAINSHOP_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

class FamilyUI;
class FamilyShop:public UIScene,cocos2d::extension::CCTableViewDataSource,public cocos2d::extension::CCTableViewDelegate
{
public:
	FamilyShop(void);
	~FamilyShop(void);

	static FamilyShop * create();
	bool init();
	void onEnter();
	void onExit();


	void callBackExit(CCObject * obj);
public:
	CCSize winsize;
	UILayer * loadLayer;
	CCTableView * tableviewMember;
	FamilyUI * familyui;
	UILabel * labelContrbution;
public:
	////familyMember
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	// default implements are used to call script callback if exist
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
	
};

//////////////////////////////////////
class goodsItemCell:public UIScene
{
public:
	goodsItemCell(void);
	~goodsItemCell(void);

	static goodsItemCell * create(int idx);
	bool init(int idx);

	void onEnter();
	void onExit();

	std::string getColorSpByQuality( int quality );
	void callBackBuy(CCObject * obj);
	void showGoodsInfo(CCObject * obj);

	CCSize winsize;
	
};

#endif
