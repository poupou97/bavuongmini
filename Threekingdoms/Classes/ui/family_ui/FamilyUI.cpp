#include "FamilyUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/UITab.h"
#include "GameView.h"
#include "ManageFamily.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGuildRecord.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CGuildMemberBase.h"
#include "FamilyMemberOperator.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../extensions/RichTextInput.h"
#include "FamilyOperatorOfLeader.h"
#include "../../gamescene_state/GameSceneState.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "FamilyShop.h"
#include "FamilyAlms.h"
#include "../../messageclient/element/CGuildDonateOption.h"
#include "../../utils/GameUtils.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../extensions/ShowSystemInfo.h"
#include "familyFight_ui/FamilyFightUI.h"
#include "../../messageclient/element/CGuildBase.h"

#define  kTagFamilyFightUI 500

using namespace CocosDenshion;

FamilyUI::FamilyUI(void)
{
	currType=0;
	hasNextPageOfManageMember =  false;
	curPageOfManageFamilyMember = 0;
	hasNextPageOfMember =false;
	curPageOfFamilyMember = 0;
	selfPost_ = 0;

	remPersonContrbution = 0;
	curFamilyContrbution = 0;
	nextFamilyContrbution = 0;
	familyAddContrbution = 0;
	personAddContrbution =0;
	familyLevel = 0;

	curFamilyMemberNum = 0;
	maxFamilyMemberNum = 0;
	m_familytab = NULL;
}


FamilyUI::~FamilyUI(void)
{
	std::vector<CGuildMemberBase *>::iterator iterbase;
	for (iterbase=vectorGuildMemberBase.begin();iterbase!=vectorGuildMemberBase.end();iterbase++)
	{
		delete * iterbase;
	}
	vectorGuildMemberBase.clear();

	std::vector<CGuildMemberBase *>::iterator itermember;
	for (itermember=vectorApplyGuildMember.begin();itermember!=vectorApplyGuildMember.end();itermember++)
	{
		delete * itermember;
	}
	vectorApplyGuildMember.clear();

	std::vector<CGuildCommodity *>::iterator iterCommodity;
	for (iterCommodity=guildCommodityVector.begin();iterCommodity!=guildCommodityVector.end();iterCommodity++)
	{
		delete * iterCommodity;
	}
	guildCommodityVector.clear();


	std::vector<CGuildDonateOption *>::iterator iterGuildDonate;
	for (iterGuildDonate=guilidDonateVector.begin();iterGuildDonate!=guilidDonateVector.end();iterGuildDonate++)
	{
		delete * iterGuildDonate;
	}
	guilidDonateVector.clear();
}

FamilyUI * FamilyUI::create()
{
	FamilyUI * family = new FamilyUI();
	if (family && family->init())
	{
		family->autorelease();
		return family;
	}
	CC_SAFE_DELETE(family);
	return NULL;
}

bool FamilyUI::init()
{
	if (UIScene::init())
	{
		winsize= CCDirector::sharedDirector()->getVisibleSize();
		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		if(LoadSceneLayer::familyMainPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::familyMainPanel->removeFromParentAndCleanup(false);
		}
		
		mainPanel=LoadSceneLayer::familyMainPanel;
		mainPanel->setAnchorPoint(ccp(0.5f,0.5f));
		mainPanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(mainPanel);

		loadLayer= UILayer::create();
		loadLayer->ignoreAnchorPointForPosition(false);
		loadLayer->setAnchorPoint(ccp(0.5f,0.5f));
		loadLayer->setPosition(ccp(winsize.width/2,winsize.height/2));
		loadLayer->setContentSize(CCSizeMake(800,480));
		this->addChild(loadLayer);

		const char * secondStr = StringDataManager::getString("UIName_family_jia");
		const char * thirdStr = StringDataManager::getString("UIName_family_zu");
		CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(ccp(30,240));
		loadLayer->addChild(atmature);

		const char *strings_info = StringDataManager::getString("family_tab_info");
		char *tab_info=const_cast<char*>(strings_info);	

		const char *strings_member = StringDataManager::getString("family_tab_member");
		char *tab_member=const_cast<char*>(strings_member);	

		const char *strings_state = StringDataManager::getString("family_tab_state");
		char *tab_state=const_cast<char*>(strings_state);	

		const char *strings_familyFight = StringDataManager::getString("family_tab_familyFight");
		char *tab_familyFight=const_cast<char*>(strings_familyFight);	

		char *labelFont[]={tab_info,tab_member,tab_state,tab_familyFight};
		//m_familytab=UITab::createWithBMFont(4,"res_ui/tab_4_off.png","res_ui/tab_4_on.png","",labelFont,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
		m_familytab=UITab::createWithText(4,"res_ui/tab_4_off.png","res_ui/tab_4_on.png","",labelFont,HORIZONTAL,5);
		m_familytab->setAnchorPoint(ccp(0.5f,0.5f));
		m_familytab->setPosition(ccp(70,413));
		m_familytab->setHighLightImage("res_ui/tab_4_on.png");
		m_familytab->setDefaultPanelByIndex(0);
		m_familytab->setPressedActionEnabled(true);
		m_familytab->addIndexChangedEvent(this,coco_indexchangedselector(FamilyUI::changeUITabIndex));
		loadLayer->addWidget(m_familytab);

		familyInfo_panel =(UIPanel *)UIHelper::seekWidgetByName(mainPanel,"Panel_information");
		familyMember_panel =(UIPanel *)UIHelper::seekWidgetByName(mainPanel,"Panel_menbers");
		familyState_panel =(UIPanel *)UIHelper::seekWidgetByName(mainPanel,"Panel_state");
		familyState_panel_infoIsNUll = (UIPanel *)UIHelper::seekWidgetByName(mainPanel,"Panel_stateInfoIsNull");
		familyInfo_panel->setVisible(true);
		familyMember_panel->setVisible(false);
		familyState_panel->setVisible(false);

		panel_familyFight = (UIPanel*)UIHelper::seekWidgetByName(mainPanel,"Panel_familyFight");
		panel_familyFight->setVisible(false);

		label_familyName =(UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_name_value");
		label_familyLevel = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_LV_value");
		label_familyNumber =(UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_NumberOfPeople_value");
		label_familyContribution = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_contribution_value");
		
		//get self id
		selfId_ = GameView::getInstance()->myplayer->getRoleId();
		
		label_myPost = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_jobValue");
		//label_myAllContribution = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_accumulativeValue");
		label_myRemContribution = (UILabel *)UIHelper::seekWidgetByName(mainPanel,"Label_rateOfSurplusValue");

		btn_repairNotice =(UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_announcement");
		btn_repairNotice->setTouchEnable(true);
		btn_repairNotice->addReleaseEvent(this,coco_releaseselector(FamilyUI::repairFamilyNotice));
		
		//const char *strings_NoticeString = StringDataManager::getString("dianjishurujiazuzhongzhi");
		
		familyNoticeLabel=CCRichLabel::createWithString("",CCSizeMake(250,0),NULL,NULL,0,18,2);
		familyNoticeLabel->setAnchorPoint(ccp(0,0));
		familyNoticeLabel->setPosition(ccp(0,0));
		
		noticeScrollView = CCScrollView::create(CCSizeMake(270,105));
		noticeScrollView->setContentSize(CCSizeMake(270,105));
		noticeScrollView->ignoreAnchorPointForPosition(false);
		noticeScrollView->setTouchEnabled(true);
		noticeScrollView->setDirection(kCCScrollViewDirectionVertical);
		noticeScrollView->setAnchorPoint(ccp(0,0));
		noticeScrollView->setPosition(ccp(485,105));
		noticeScrollView->setBounceable(true);
		noticeScrollView->setClippingToBounds(true);
		loadLayer->addChild(noticeScrollView);	
		noticeScrollView->addChild(familyNoticeLabel);
		
		btn_repairManifesto =(UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_repairgonggao");
		btn_repairManifesto->setTouchEnable(true);
		btn_repairManifesto->addReleaseEvent(this,coco_releaseselector(FamilyUI::repairFamilyManifesto));

		//const char *strings_manifestoString = StringDataManager::getString("dianjishurujiazuxuanyan");
		familyManifestoLabel=CCRichLabel::createWithString("",CCSizeMake(250,0),NULL,NULL,0,18,2);
		familyManifestoLabel->setAnchorPoint(ccp(0,0));
		//familyManifestoLabel->setPosition(ccp(143,210));
		familyManifestoLabel->setPosition(ccp(0,0));

		manifestoScrollView = CCScrollView::create(CCSizeMake(270,105));
		manifestoScrollView->setContentSize(CCSizeMake(270,105));
		manifestoScrollView->ignoreAnchorPointForPosition(false);
		manifestoScrollView->setTouchEnabled(true);
		manifestoScrollView->setDirection(kCCScrollViewDirectionVertical);
		manifestoScrollView->setAnchorPoint(ccp(0,0));
		manifestoScrollView->setPosition(ccp(143,105));
		manifestoScrollView->setBounceable(true);
		manifestoScrollView->setClippingToBounds(true);
		loadLayer->addChild(manifestoScrollView);	
		manifestoScrollView->addChild(familyManifestoLabel);

		//btn family is up level
		btn_familyUplevel = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_familyUplevel");
		btn_familyUplevel->setTouchEnable(true);
		btn_familyUplevel->setPressedActionEnabled(true);
		btn_familyUplevel->addReleaseEvent(this,coco_releaseselector(FamilyUI::callbackFamilyUplLevel));

		btn_mamage=(UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_adminstration");
		btn_mamage->setTouchEnable(true);
		btn_mamage->setPressedActionEnabled(true);
		btn_mamage->addReleaseEvent(this,coco_releaseselector(FamilyUI::callBackManageFamily));

		UIButton * btn_quit=(UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_out");
		btn_quit->setTouchEnable(true);
		btn_quit->setPressedActionEnabled(true);
		btn_quit->addReleaseEvent(this,coco_releaseselector(FamilyUI::memberQuitFamily));
		
		UIButton * button_des = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"button_des");
		button_des->setTouchEnable(true);
		button_des->setPressedActionEnabled(true);
		button_des->addReleaseEvent(this,coco_releaseselector(FamilyUI::callBackDesFamily));

		//btn shop and contrbution
		UIButton * btn_shop=(UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_shop");
		btn_shop->setTouchEnable(true);
		btn_shop->setPressedActionEnabled(true);
		btn_shop->addReleaseEvent(this,coco_releaseselector(FamilyUI::callbackFamilyShop));

		UIButton * btn_alms=(UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_alms");
		btn_alms->setTouchEnable(true);
		btn_alms->setPressedActionEnabled(true);
		btn_alms->addReleaseEvent(this,coco_releaseselector(FamilyUI::callbackFamilyAlms));

		layerMember = UILayer::create();
		layerMember->ignoreAnchorPointForPosition(false);
		layerMember->setAnchorPoint(ccp(0.5f,0.5f));
		layerMember->setPosition(ccp(winsize.width/2,winsize.height/2));
		layerMember->setZOrder(10);
		layerMember->setContentSize(CCSizeMake(800,480));
		this->addChild(layerMember);
		layerMember->setVisible(false);

		tableviewMember = CCTableView::create(this, CCSizeMake(654,317));
		tableviewMember->setSelectedEnable(true);
		tableviewMember->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 24, 1, 1), ccp(0,0));
		tableviewMember->setPressedActionEnabled(true);
		tableviewMember->setDirection(kCCScrollViewDirectionVertical);
		tableviewMember->setAnchorPoint(ccp(0,0));
		tableviewMember->setPosition(ccp(80,49));
		tableviewMember->setDelegate(this);
		tableviewMember->setVerticalFillOrder(kCCTableViewFillTopDown);
		layerMember->addChild(tableviewMember);

		layerstate = UILayer::create();
		layerstate->ignoreAnchorPointForPosition(false);
		layerstate->setAnchorPoint(ccp(0.5f,0.5f));
		layerstate->setContentSize(CCSizeMake(800,480));
		layerstate->setPosition(ccp(winsize.width/2,winsize.height/2));
		this->addChild(layerstate);
		layerstate->setVisible(false);
		tableviewstate = CCTableView::create(this, CCSizeMake(675,360));
		//tableviewstate->setSelectedEnable(true);
		//tableviewstate->setSelectedScale9Texture();
		tableviewstate->setPressedActionEnabled(true);
		tableviewstate->setDirection(kCCScrollViewDirectionVertical);
		tableviewstate->setAnchorPoint(ccp(0,0));
		tableviewstate->setPosition(ccp(70,40));
		tableviewstate->setDelegate(this);
		tableviewstate->setVerticalFillOrder(kCCTableViewFillTopDown);
		layerstate->addChild(tableviewstate);

		UIButton * btnClose =(UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_close");
		btnClose->setTouchEnable(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addReleaseEvent(this,coco_releaseselector(FamilyUI::callBackExit));

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);

		return true;
	}
	return false;
}

void FamilyUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	initFamilyFightUI();

	//GameMessageProcessor::sharedMsgProcessor()->sendReq(1708,this);
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void FamilyUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

void FamilyUI::changeUITabIndex( CCObject * obj )
{
	UITab * tab= (UITab *)obj;
	int num =tab->getCurrentIndex();
	currType=num;
	switch(num)
	{
	case 0:
		{
			familyInfo_panel->setVisible(true);
			familyMember_panel->setVisible(false);
			familyState_panel->setVisible(false);
			layerMember->setVisible(false);
			layerstate->setVisible(false);

			familyNoticeLabel->setVisible(true);
			familyManifestoLabel->setVisible(true);

			l_FamilyFightLayer->setVisible(false);
			panel_familyFight->setVisible(false);
		}break;
	case 1:
		{
			familyInfo_panel->setVisible(false);
			familyMember_panel->setVisible(true);
			familyState_panel->setVisible(false);
			layerMember->setVisible(true);
			tableviewMember->reloadData();
			layerstate->setVisible(false);

			familyNoticeLabel->setVisible(false);
			familyManifestoLabel->setVisible(false);

			l_FamilyFightLayer->setVisible(false);
			panel_familyFight->setVisible(false);

			GameMessageProcessor::sharedMsgProcessor()->sendReq(1512,(void *)0,(void *)0);
		}break;
	case 2:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1511,this);

			familyInfo_panel->setVisible(false);
			familyMember_panel->setVisible(false);
			familyState_panel->setVisible(true);
			layerMember->setVisible(false);
			layerstate->setVisible(true);
			tableviewstate->reloadData();

			familyNoticeLabel->setVisible(false);
			familyManifestoLabel->setVisible(false);

			l_FamilyFightLayer->setVisible(false);
			panel_familyFight->setVisible(false);
		}break;
	case 3:
		{
			//开启等级
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(17);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[17];
			}

			if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				familyInfo_panel->setVisible(false);
				familyMember_panel->setVisible(false);
				familyState_panel->setVisible(false);
				layerMember->setVisible(false);
				layerstate->setVisible(false);
				familyNoticeLabel->setVisible(false);
				familyManifestoLabel->setVisible(false);

				l_FamilyFightLayer->setVisible(true);
				panel_familyFight->setVisible(true);
				//请求当前家族战状态
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1544);
			}
			else
			{
				std::string str_des = StringDataManager::getString("feature_will_be_open_function");
				char str_level[20];
				sprintf(str_level,"%d",openlevel);
				str_des.append(str_level);
				str_des.append(StringDataManager::getString("feature_will_be_open_open"));
				GameView::getInstance()->showAlertDialog(str_des.c_str());
			}
		}break;
	}
}

void FamilyUI::repairFamilyNotice( CCObject * obj )
{
	FamilyChangeNotice * familyNotice_ =FamilyChangeNotice::create(1);
	familyNotice_->ignoreAnchorPointForPosition(false);
	familyNotice_->setAnchorPoint(ccp(0.5f,0.5f));
	familyNotice_->setPosition(ccp(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(familyNotice_);
}

void FamilyUI::repairFamilyManifesto( CCObject * obj )
{
	FamilyChangeNotice * familyNotice_ =FamilyChangeNotice::create(2);
	familyNotice_->ignoreAnchorPointForPosition(false);
	familyNotice_->setAnchorPoint(ccp(0.5f,0.5f));
	familyNotice_->setPosition(ccp(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(familyNotice_);
}

void FamilyUI::callbackFamilyShop( CCObject * obj )
{
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	FamilyShop *shopui_ = (FamilyShop*)mainscene->getChildByTag(familyShoptag);
	if (shopui_ == NULL)
	{
		FamilyShop * shop = FamilyShop::create();
		shop->ignoreAnchorPointForPosition(false);
		shop->setAnchorPoint(ccp(0.5f,0.5f));
		shop->setPosition(ccp(winsize.width/2,winsize.height/2));
		shop->setTag(familyShoptag);
		mainscene->addChild(shop);

		GameMessageProcessor::sharedMsgProcessor()->sendReq(1708,this);
	}
}

void FamilyUI::callbackFamilyAlms( CCObject * obj )
{
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	FamilyAlms *familalmsui_ = (FamilyAlms*)mainscene->getChildByTag(familyAlms);
	if (familalmsui_ == NULL)
	{
		FamilyAlms * alms = FamilyAlms::create();
		alms->ignoreAnchorPointForPosition(false);
		alms->setAnchorPoint(ccp(0.5f,0.5f));
		alms->setPosition(ccp(winsize.width/2,winsize.height/2));
		alms->setTag(familyAlms);
		mainscene->addChild(alms);

		GameMessageProcessor::sharedMsgProcessor()->sendReq(1518,this);
	}

}


void FamilyUI::callBackManageFamily( CCObject * obj )
{
	FamilyOperatorOfLeader * familyLeader =FamilyOperatorOfLeader::create();
	familyLeader->ignoreAnchorPointForPosition(false);
	familyLeader->setAnchorPoint(ccp(1,0));
	familyLeader->setPosition(ccp(btn_mamage->getPosition().x-familyLeader->getContentSize().width/2,btn_mamage->getPosition().y+ 20));
	familyLeader->setTag(familyLeaderManager);
	loadLayer->addChild(familyLeader);
	//if self is leader req1505,clear vector
	if (selfPost_ != 3)
	{
		std::vector<CGuildMemberBase *>::iterator iter;
		for (iter=vectorApplyGuildMember.begin();iter!=vectorApplyGuildMember.end();iter++)
		{
			delete * iter;
		}
		vectorApplyGuildMember.clear();

		GameMessageProcessor::sharedMsgProcessor()->sendReq(1505,(void*)curPageOfManageFamilyMember);
	}
}


void FamilyUI::memberQuitFamily( CCObject * obj )
{
	const char *strings = StringDataManager::getString("family_btnmember_quit");
	char *str=const_cast<char*>(strings);	
	GameView::getInstance()->showPopupWindow(str,2,this,coco_selectselector(FamilyUI::quitFamilySure),NULL);
}
void FamilyUI::manageQuitFamily( CCObject * obj )
{
	const char *strings = StringDataManager::getString("family_btnmanage_quit");
	GameView::getInstance()->showAlertDialog(strings);
}

void FamilyUI::quitFamilySure( CCObject * obj )
{
	if (selfPost_ !=1)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1507,this);
		this->closeAnim();
	}else
	{
		const char *strings = StringDataManager::getString("family_DismissOfleaderSure");
		GameView::getInstance()->showAlertDialog(strings);
	}
}

void FamilyUI::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}

void FamilyUI::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void FamilyUI::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	if (currType ==1)
	{
		int index_ = cell->getIdx();
		long long playerId_ = vectorGuildMemberBase.at(index_)->id();
		if (playerId_ != selfId_)
		{
			int pointX = cell->getPosition().x;
			int pointY = cell->getPosition().y;
			int offX = table->getContentOffset().x;
			int offY = table->getContentOffset().y;

			FamilyMemberOperator * memberOperator =FamilyMemberOperator::create(selfPost_,index_);
			memberOperator->ignoreAnchorPointForPosition(false);
			memberOperator->setAnchorPoint(ccp(0.5f,0.5f));
			memberOperator->setPosition(ccp(100,layerMember->getContentSize().height/2 - memberOperator->getContentSize().height/2-50));
			memberOperator->setZOrder(10);
			layerMember->addChild(memberOperator);
		}
	}
}

cocos2d::CCSize FamilyUI::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	if (currType==1)
	{
		return CCSizeMake(650,60);
	}else if (currType==2)
	{
		return CCSizeMake(675,40);
	}else
	{
		return CCSizeMake(0,0);
	}
}

cocos2d::extension::CCTableViewCell* FamilyUI::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString * string =CCString::createWithFormat("%d",idx);	
	CCTableViewCell *cell =table->dequeueCell();
	cell=new CCTableViewCell();
	cell->autorelease();

	switch (currType)
	{
	case 1:
		{
			CCScale9Sprite * spbg = CCScale9Sprite::create("res_ui/kuang0_new.png");
			spbg->setPreferredSize(CCSizeMake(650,60));
			spbg->setCapInsets(CCRect(15,30,1,1));
			spbg->setAnchorPoint(ccp(0,0));
			spbg->setPosition(ccp(0,0));
			cell->addChild(spbg);

			std::string familyerName_ = vectorGuildMemberBase.at(idx)->name();
			CCLabelTTF  * labelMemberName=CCLabelTTF::create(familyerName_.c_str(),APP_FONT_NAME,16);
			labelMemberName->setAnchorPoint(ccp(0,0));
			labelMemberName->setPosition(ccp(13,25));
			cell->addChild(labelMemberName);

			int vipLevel_ = vectorGuildMemberBase.at(idx)->viplevel();
			if (vipLevel_ > 0)
			{
				CCNode * vipNode = MainScene::addVipInfoByLevelForNode(vipLevel_);
				cell->addChild(vipNode);
				vipNode->setPosition(ccp(labelMemberName->getPositionX() + labelMemberName->getContentSize().width + vipNode->getContentSize().width/2,
											labelMemberName->getPositionY() + vipNode->getContentSize().height/2));
			}

			int familyerJob_ = vectorGuildMemberBase.at(idx)->post();
			const char *strings;
			switch(familyerJob_)
			{
			case 1:
				{
					strings = StringDataManager::getString("family_member_Master");
				}break;
			case 2:
				{
					strings = StringDataManager::getString("family_member_SecondMaster");
				}break;
			case 3:
				{
					strings = StringDataManager::getString("family_member_member");
				}break;
			}
			char *str=const_cast<char*>(strings);
			CCLabelTTF  * labelMemberJob=CCLabelTTF::create(str,APP_FONT_NAME,16);
			labelMemberJob->setAnchorPoint(ccp(0,0));
			labelMemberJob->setPosition(ccp(142,25));
			cell->addChild(labelMemberJob);

			int memberLevel_ =vectorGuildMemberBase.at(idx)->level();
			char levelStr_[5];
			sprintf(levelStr_,"%d",memberLevel_);
			CCLabelTTF  * labelMemberLevel=CCLabelTTF::create(levelStr_,APP_FONT_NAME,16);
			labelMemberLevel->setAnchorPoint(ccp(0,0));
			labelMemberLevel->setPosition(ccp(215,25));
			cell->addChild(labelMemberLevel);

			int memberPression_ =vectorGuildMemberBase.at(idx)->profession();
			std::string pressionStr_ = BasePlayer::getProfessionNameIdxByIndex(memberPression_);

			CCLabelTTF  * labelMemberPression=CCLabelTTF::create(pressionStr_.c_str(),APP_FONT_NAME,16);
			labelMemberPression->setAnchorPoint(ccp(0,0));
			labelMemberPression->setPosition(ccp(278,25));
			cell->addChild(labelMemberPression);

			int memberContrbution_ =vectorGuildMemberBase.at(idx)->guilddonate();
			char donateStr[10];
			sprintf(donateStr,"%d",memberContrbution_);
			CCLabelTTF  * labelMembercontrbution=CCLabelTTF::create(donateStr,APP_FONT_NAME,16);
			labelMembercontrbution->setAnchorPoint(ccp(0,0));
			labelMembercontrbution->setPosition(ccp(355,25));
			cell->addChild(labelMembercontrbution);

			//const char *stringSort_ = StringDataManager::getString("family_sortString_");
			int memberFight = vectorGuildMemberBase.at(idx)->fightpoint();
			char strFight[20];
			sprintf(strFight,"%d",memberFight);
			CCLabelTTF  * labelMemberSort=CCLabelTTF::create(strFight,APP_FONT_NAME,16);
			labelMemberSort->setAnchorPoint(ccp(0,0));
			labelMemberSort->setPosition(ccp(455,25));
			cell->addChild(labelMemberSort);

			//const char *memberState_;
			std::string memberState_;
			if (vectorGuildMemberBase.at(idx)->isonline()==1)
			{
				const char * state_  = StringDataManager::getString("family_member_state_online");
				memberState_ =state_;
			}else
			{
				long long lastTime_ =vectorGuildMemberBase.at(idx)->lastlogintime();
				if ((long long)(lastTime_/24)>0)//1 day
				{
					if ((long long)(lastTime_/(long long)(24*30)) >0)//1 moth
					{
						const char* strings = StringDataManager::getString("family_mothago");
						int online_ =int (lastTime_/(long long)(24*30));
						char onlineStr_[20];
						sprintf(onlineStr_,"%d",online_);
						memberState_ =onlineStr_;
						memberState_.append(strings);
					}else
					{
						const char* strings = StringDataManager::getString("family_dayAgo");
						int online_ =int (lastTime_/24);
						char onlineStr_[20];
						sprintf(onlineStr_,"%d",online_);
						memberState_ =onlineStr_;
						memberState_.append(strings);
					}
				}else
				{
					if (lastTime_ <= 1)
					{
						const char* strings = StringDataManager::getString("family_minAgo");
						memberState_.append(strings);
					}else
					{
						const char* strings = StringDataManager::getString("family_hourAgo");
						char onlineStr_[20];
						sprintf(onlineStr_,"%lld",lastTime_);
						memberState_ = onlineStr_;
						memberState_.append(strings);
					}
				}
			}
			CCLabelTTF  * labelMemberOnLine=CCLabelTTF::create(memberState_.c_str(),APP_FONT_NAME,16);
			labelMemberOnLine->setAnchorPoint(ccp(0,0));
			labelMemberOnLine->setPosition(ccp(580,25));
			cell->addChild(labelMemberOnLine);

			if (idx == vectorGuildMemberBase.size()-3)
			{
				if (hasNextPageOfMember ==true)
				{
					curPageOfFamilyMember++;
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1512,(void *)curPageOfFamilyMember,(void *)0);
				}
			}
		}break;
	case 2:
		{	
			CCScale9Sprite * spbg=CCScale9Sprite::create("res_ui/LV3_dikuang.png");
			spbg->setAnchorPoint(ccp(0,0));
			spbg->setPreferredSize(CCSizeMake(670,30));
			spbg->setCapInsets(CCRect(9,15,1,1));
			spbg->setPosition(ccp(2,0));
			cell->addChild(spbg);

			std::string labelMsg_ = GameView::getInstance()->guildRecordVector.at(idx)->msg();
			CCLabelTTF  * labelContent=CCLabelTTF::create(labelMsg_.c_str(),APP_FONT_NAME,16);
			labelContent->setAnchorPoint(ccp(0,0.5f));
			labelContent->setPosition(ccp(10,15));
			labelContent->setColor(ccc3(47,93,13));
			cell->addChild(labelContent,10);
		}break;
	}
	return cell;
}

unsigned int FamilyUI::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	if (currType==1)
	{
		return vectorGuildMemberBase.size();
	}else if (currType==2)
	{
		return GameView::getInstance()->guildRecordVector.size();
	}else
	{
		return 0;
	}
}

void FamilyUI::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void FamilyUI::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void FamilyUI::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

void FamilyUI::callBackExit( CCObject * obj )
{
	this->closeAnim();
}

void FamilyUI::downListMember( int index )
{
	
}

void FamilyUI::callBackDesFamily( CCObject * obj )
{
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(5);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end())
	{
		return ;
	}
	else
	{
		if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
		{
			ShowSystemInfo * showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[5].c_str());
			GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo,0,kTagStrategiesDetailInfo);
			showSystemInfo->ignoreAnchorPointForPosition(false);
			showSystemInfo->setAnchorPoint(ccp(0.5f, 0.5f));
			showSystemInfo->setPosition(ccp(winsize.width/2, winsize.height/2));
		}
	}
}

void FamilyUI::initFamilyFightUI()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	l_FamilyFightLayer=UILayer::create();
	l_FamilyFightLayer->ignoreAnchorPointForPosition(false);
	l_FamilyFightLayer->setAnchorPoint(ccp(0.5f,0.5f));
	l_FamilyFightLayer->setPosition(ccp(winSize.width/2,winSize.height/2));
	l_FamilyFightLayer->setContentSize(CCSizeMake(800,480));
	this->addChild(l_FamilyFightLayer);
	l_FamilyFightLayer->setVisible(false);

	FamilyFightUI * familyFightUI = FamilyFightUI::create();
	l_FamilyFightLayer->addChild(familyFightUI);
	familyFightUI->setTag(kTagFamilyFightUI);

	// 请求家族战场日常奖励Req1545空消息
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1545);
}

FamilyFightUI * FamilyUI::GetFamilyFightUI()
{
	FamilyFightUI * temp = (FamilyFightUI*)l_FamilyFightLayer->getChildByTag(kTagFamilyFightUI);
	if (temp)
		return temp;

	return NULL;
}

void FamilyUI::callbackFamilyUplLevel( CCObject * obj )
{
	if (selfPost_ != CGuildBase::position_master)
	{
		const char * strings_ = StringDataManager::getString("family_zuzhang_isUpfamilyLevel");
		GameView::getInstance()->showAlertDialog(strings_);
	}else
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1514);
	}
}

void FamilyUI::openFamilyFightUI()
{
	familyInfo_panel->setVisible(false);
	familyMember_panel->setVisible(false);
	familyState_panel->setVisible(false);
	layerMember->setVisible(false);
	layerstate->setVisible(false);
	familyNoticeLabel->setVisible(false);
	familyManifestoLabel->setVisible(false);

	l_FamilyFightLayer->setVisible(true);
	panel_familyFight->setVisible(true);

	// 设置UITab所在标签位置
	m_familytab->setDefaultPanelByIndex(3);


	//请求当前家族战状态
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1544);
}






