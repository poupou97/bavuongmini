#ifndef _STORY_STORYBLACKBAND_H_
#define _STORY_STORYBLACKBAND_H_

#include "cocos2d.h"
#include "cocos-ext.h"

#define STORYBLACKBAND_HEIGHT 114
#define STORYBLACKBAND_TOP_HEIGHT 60

USING_NS_CC;
USING_NS_CC_EXT;

/**
  * Display a black band at the top and bottom
  * @Author zhaogang
  * @Date 2014/1/4
  */
class StoryBlackBand : public CCLayer
{
public:
    StoryBlackBand();
	~StoryBlackBand();

	static StoryBlackBand* create();
	virtual bool init();

	virtual void update(float dt);

	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);

public:
	void endActionCallBack(CCObject * object);

private:

};

#endif
