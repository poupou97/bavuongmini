#include "GameUIUtils.h"

#include "./GameUIConstant.h"
#include "../gamescene_state/role/WeaponEffect.h"
#include "../legend_engine/CCLegendAnimation.h"

bool GameUIUtils::s_bPreloaded = false;

void GameUIUtils::preloadEffects()
{
	if(s_bPreloaded)
		return;

	CCLegendAnimation* pAnim = NULL;
	CCSprite* pSprite = NULL;

	// load something
	WeaponEffect::getParticle(INTERFACE_ANM_PARTICLE_EFFECT_1);
	WeaponEffect::getParticle(INTERFACE_ANM_PARTICLE_EFFECT_2);
	WeaponEffect::getParticle(INTERFACE_ANM_PARTICLE_EFFECT_3);
	WeaponEffect::getParticle(INTERFACE_ANM_PARTICLE_EFFECT_4);

	///////////////////////////////////////////////////

	//// mission effects
	pAnim = CCLegendAnimation::create("animation/texiao/jiemiantexiao/wcrw/wcrw.anm");
	pAnim->retain();
	//pAnim = CCLegendAnimation::create("animation/texiao/jiemiantexiao/jsrw/jsrw.anm");
	//pAnim->retain();

	// upgrade effects
	//pAnim = CCLegendAnimation::create("animation/texiao/jiemiantexiao/gxsj/gxsj.anm");
	//pAnim->retain();
	//pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/SJTX/sjtx1.anm");
	//pAnim->retain();
	pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/FUHUO/fuhuo1.anm");
	pAnim->retain();

	// the tutorial resources
	pSprite = CCSprite::create("res_ui/tutorial/yindaokuang_0.png");
	pSprite->retain();
	pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/shou1/shou.anm");
	pAnim->retain();

	// fight point resource
	pSprite = CCSprite::create("res_ui/kuang_01.png");
	pSprite->retain();
	pSprite = CCSprite::create("res_ui/font/fightup.png");
	pSprite->retain();
	pSprite = CCSprite::create("res_ui/jt2_up.png");
	pSprite->retain();
	pSprite = CCSprite::create("res_ui/jt2_down.png");
	pSprite->retain();
	CCLabelBMFont* pBMFont = CCLabelBMFont::create("1","res_ui/font/ziti_1.fnt"); 
	pBMFont->retain();

	// mini items window
	pSprite = CCSprite::create("res_ui/kuangb.png");
	pSprite->retain();
	pSprite = CCSprite::create("res_ui/new_button_2.png");
	pSprite->retain();
	pSprite = CCSprite::create("res_ui/close.png");
	pSprite->retain();
	pSprite = CCSprite::create("res_ui/generals_white.png");
	pSprite->retain();
	pSprite = CCSprite::create("res_ui/name_di3.png");
	pSprite->retain();
	pSprite = CCSprite::create("res_ui/zhezhao80.png");
	pSprite->retain();
	//mini items window of general
	pSprite = CCSprite::create("res_ui/wujiang/play.png");
	pSprite->retain();
	


	// need re-preload resource
	GameUIUtils::s_bPreloaded = true;
}
