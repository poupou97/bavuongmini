#include "RobotSetSkill.h"
#include "../extensions/UITab.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "RobotMainUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "AppMacros.h"
#include "../generals_ui/GeneralsShortcutSlot.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../ui/extensions/CCRichLabel.h"

#define  GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG 1211

RobotSetSkill::RobotSetSkill(void)
{
}


RobotSetSkill::~RobotSetSkill(void)
{
	std::vector<GameFightSkill *>::iterator iter;
	for (iter =generalSkillVector.begin();iter!=generalSkillVector.end();iter++)
	{
		delete * iter;
	}
	generalSkillVector.clear();
}

RobotSetSkill * RobotSetSkill::create(  )
{
	RobotSetSkill * robotkill =new RobotSetSkill();
	if (robotkill && robotkill->init())
	{
		robotkill->autorelease();
		return robotkill;
	}
	CC_SAFE_DELETE(robotkill);
	return NULL;
}

bool RobotSetSkill::init( )
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		
		std::map<std::string, GameFightSkill*>::iterator it;
		for (it = GameView::getInstance()->GameFightSkillList.begin(); it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
		{
			GameFightSkill * tempSkill = it->second;
			if (tempSkill->getCBaseSkill()->usemodel() == 0)
			{
				GameFightSkill * skills_ =new GameFightSkill();
				skills_->initSkill(tempSkill->getId(),tempSkill->getId(),tempSkill->getLevel(),GameActor::type_player);
				generalSkillVector.push_back(skills_);
			}
		}

		//加载UI
		if(LoadSceneLayer::GeneralsSkillListLayer->getWidgetParent() != NULL)
		{
			LoadSceneLayer::GeneralsSkillListLayer->removeFromParentAndCleanup(false);
		}
		UIPanel *ppanel = LoadSceneLayer::GeneralsSkillListLayer;
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(703, 416));
		ppanel->setPosition(CCPointZero);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		Layer_skills = UILayer::create();
		addChild(Layer_skills);

		UIButton * Button_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(RobotSetSkill::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		UIButton * btn_equip = (UIButton *)UIHelper::seekWidgetByName(ppanel,"Button_set");
		btn_equip->setTouchEnable(true);
		btn_equip->setPressedActionEnabled(true);
		btn_equip->addReleaseEvent(this,coco_releaseselector(RobotSetSkill::EquipEvent));

		i_skillFrame = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_skill_di"); 
		i_skillImage = (UIImageView*)UIHelper::seekWidgetByName(ppanel,"ImageView_skill"); 
		l_skillName = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_SkillName"); 
		l_skillType = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_SkillType"); 
		l_skillLvValue = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_LvValue"); 
		imageView_skillQuality = (UIImageView *)UIHelper::seekWidgetByName(ppanel,"ImageView_abc");
		imageView_skillQuality->setVisible(false);
		UILabel * label_skillQuality = (UILabel *)UIHelper::seekWidgetByName(ppanel,"label_skillQualityValue");
		label_skillQuality->setVisible(true);

		l_skillMpValue = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_MpValue"); 
		l_skillCDTimeValue = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_CDTimeValue"); 
		l_skillDistanceValue = (UILabel*)UIHelper::seekWidgetByName(ppanel,"Label_DistanceValue"); 
		
		generalsSkillsList_tableView = CCTableView::create(this,CCSizeMake(230,354));
		generalsSkillsList_tableView->setSelectedEnable(true);
		generalsSkillsList_tableView->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 26, 1, 1), ccp(0,-1));
		generalsSkillsList_tableView->setDirection(kCCScrollViewDirectionVertical);
		generalsSkillsList_tableView->setAnchorPoint(ccp(0,0));
		generalsSkillsList_tableView->setPosition(ccp(27,30));
		generalsSkillsList_tableView->setDelegate(this);
		generalsSkillsList_tableView->setPressedActionEnabled(true);
		generalsSkillsList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(generalsSkillsList_tableView);
		generalsSkillsList_tableView->reloadData();

		if (generalSkillVector.size()>0)
		{
			this->refreshSkillInfo(generalSkillVector.at(0));
			setSkillIndex=0;
		}
		
		this->setContentSize(CCSizeMake(704,416));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}

void RobotSetSkill::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RobotSetSkill::onExit()
{
	UIScene::onExit();
}

bool RobotSetSkill::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void RobotSetSkill::scrollViewDidScroll( CCScrollView* view )
{

}

void RobotSetSkill::scrollViewDidZoom( CCScrollView* view )
{

}

void RobotSetSkill::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	setSkillIndex = cell->getIdx();
	this->refreshSkillInfo(generalSkillVector.at(setSkillIndex));
}

cocos2d::CCSize RobotSetSkill::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(227,65);
}

cocos2d::extension::CCTableViewCell* RobotSetSkill::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/kuang01_new.png");
	sprite_bigFrame->setAnchorPoint(ccp(0, 0));
	sprite_bigFrame->setPosition(ccp(0, 0));
	sprite_bigFrame->setContentSize(CCSizeMake(225,63));
	sprite_bigFrame->setCapInsets(CCRectMake(15,31,1,1));
	cell->addChild(sprite_bigFrame);

	//小边框
// 	CCScale9Sprite *  smaillFrame = CCScale9Sprite::create("res_ui/LV4_allb.png");
// 	smaillFrame->setAnchorPoint(ccp(0, 0));
// 	smaillFrame->setPosition(ccp(18,5));
// 	smaillFrame->setCapInsets(CCRectMake(15,15,1,1));
// 	smaillFrame->setContentSize(CCSizeMake(52,52));
// 	cell->addChild(smaillFrame);
	CCSprite *  smaillFrame = CCSprite::create("res_ui/jinengdi.png");
	smaillFrame->setAnchorPoint(ccp(0, 0));
	smaillFrame->setPosition(ccp(20,5));
	smaillFrame->setScale(0.91f);
	cell->addChild(smaillFrame);

	CCScale9Sprite *sprite_nameFrame = CCScale9Sprite::create("res_ui/LV4_diaa.png");
	sprite_nameFrame->setAnchorPoint(ccp(0, 0));
	sprite_nameFrame->setPosition(ccp(83, 17));
	sprite_nameFrame->setCapInsets(CCRectMake(11,11,1,1));
	sprite_nameFrame->setContentSize(CCSizeMake(130,30));
	sprite_bigFrame->addChild(sprite_nameFrame);

	std::string skillIconPath = "res_ui/jineng_icon/";

// 	if (strcmp(generalSkillVector.at(idx)->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 		||strcmp(generalSkillVector.at(idx)->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 		||strcmp(generalSkillVector.at(idx)->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 		||strcmp(generalSkillVector.at(idx)->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 	{
// 		skillIconPath.append("jineng_2");
// 	}
// 	else
// 	{
		CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[generalSkillVector.at(idx)->getId()];
		if (baseSkill)
		{
			skillIconPath.append(baseSkill->icon());
		}
		else
		{
			skillIconPath.append("jineng_2");
		}
//	}

	skillIconPath.append(".png");
	CCSprite *  sprite_image = CCSprite::create(skillIconPath.c_str());
	sprite_image->setAnchorPoint(ccp(0.5f,0.5f));
	sprite_image->setPosition(ccp(46,31));
	cell->addChild(sprite_image);

	CCSprite *  sprite_lvFrame = CCSprite::create("res_ui/jineng/jineng_zhezhao.png");
	sprite_lvFrame->setAnchorPoint(ccp(0.5f,0.f));
	sprite_lvFrame->setPosition(ccp(45/2+1,0));
	sprite_image->addChild(sprite_lvFrame);

	int curSkillLv = generalSkillVector.at(idx)->getLevel();
	int maxSkillLv = generalSkillVector.at(idx)->getCBaseSkill()->maxlevel();
	std::string skillStr = "";
	char curStrSkill[5];
	sprintf(curStrSkill,"%d",curSkillLv);
	char maxStrSkill[5];
	sprintf(maxStrSkill,"%d",maxSkillLv);
	skillStr.append(curStrSkill);
	skillStr.append("/");
	skillStr.append(maxStrSkill);
	CCLabelTTF * skillLv = CCLabelTTF::create(skillStr.c_str(),APP_FONT_NAME,12);
	skillLv->setAnchorPoint(ccp(0.5f, 0.f));
	skillLv->setPosition(ccp(sprite_lvFrame->getPosition().x,sprite_lvFrame->getPosition().y));
// 	ccColor3B shadowColorSkill = ccc3(0,0,0); 
// 	skillLv->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColorSkill);
	sprite_image->addChild(skillLv);


// 	CCSprite * sprite_highLight = CCSprite::create("gamescene_state/zhujiemian3/jinengqu/gaoguang_1.png");
// 	sprite_highLight->setAnchorPoint(ccp(0.5f, 0.5f));
// 	sprite_highLight->setPosition(ccp(46,35));
// 	cell->addChild(sprite_highLight);


	//skill variety icon
	CCNode * sprite_skillVariety = SkillScene::GetSkillVarirtySprite(generalSkillVector.at(idx)->getId().c_str());
	if (sprite_skillVariety)
	{
		sprite_skillVariety->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_skillVariety->setPosition(ccp(60,29));
		sprite_skillVariety->setScale(.85f);
		cell->addChild(sprite_skillVariety);
	}

	CCLabelTTF * label_skillName = CCLabelTTF::create(generalSkillVector.at(idx)->getCBaseSkill()->name().c_str(),APP_FONT_NAME,18);
	label_skillName->setAnchorPoint(ccp(0, 0.5f));
	label_skillName->setPosition(ccp(90,31));
// 	ccColor3B shadowColor = ccc3(196,255,68);   // black
// 	label_skillName->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	cell->addChild(label_skillName);
	
	return cell;
}

unsigned int RobotSetSkill::numberOfCellsInTableView( CCTableView *table )
{
	return generalSkillVector.size();
}

void RobotSetSkill::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void RobotSetSkill::EquipEvent( CCObject *pSender )
{
	CCLOG("equipid = %d",setSkillIndex);
	if (generalSkillVector.size()<0)
	{
		return;
	}
	
	//if skill is exit show
	std::string skillName_ =generalSkillVector.at(setSkillIndex)->getId();
	//type  activeSkill =0    passiveSkill =2
	int skillType = generalSkillVector.at(setSkillIndex)->getCBaseSkill()->usemodel();

	for (int i=0;i<GameView::getInstance()->myplayer->getMyPlayerAI()->m_skillList.size();i++)
	{
		if (skillType == 2)
		{	
			const char *str = StringDataManager::getString("robotui_passiveSkillType");
			GameView::getInstance()->showAlertDialog(str);
			return;
		}
	}

	std::string skillIconPath ="res_ui/jineng_icon/";
// 	if (strcmp(generalSkillVector.at(setSkillIndex)->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 		||strcmp(generalSkillVector.at(setSkillIndex)->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 		||strcmp(generalSkillVector.at(setSkillIndex)->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 		||strcmp(generalSkillVector.at(setSkillIndex)->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 	{
// 		skillIconPath.append("jineng_2");
// 	}
// 	else
// 	{
		CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[generalSkillVector.at(setSkillIndex)->getId()];
		if (baseSkill)
		{
			skillIconPath.append(baseSkill->icon());
		}
		else
		{
			skillIconPath.append("jineng_2");
		}
	//}
	skillIconPath.append(".png");

	RobotMainUI *robotui =(RobotMainUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
	UIButton * btn = (UIButton *)robotui->battlePanel->getChildByTag(robotui->buttonSkillIconIndex);
	
	std::string skillImageName = "ImageView_skillbtn_";
	char imageId[3];
	sprintf(imageId, "%d", robotui->buttonSkillIconIndex);
	skillImageName.append(imageId);
	
	UIImageView *imageIcon =(UIImageView *)btn->getChildByName(skillImageName.c_str());
	imageIcon->setTexture(skillIconPath.c_str()) ;
	imageIcon->setVisible(true);
	//add skillType

	std::string imageLight_str = "ImageView_light_";
	char imageLightId[25];
	sprintf(imageLightId, "%d", robotui->buttonSkillIconIndex);
	imageLight_str.append(imageLightId);
	UIImageView * imageLight_ = (UIImageView *)btn->getChildByName(imageLight_str.c_str());
	
	std::string imageSkillType_str = "ImageView_skilType_";
	char imageSkillTypeId[25];
	sprintf(imageSkillTypeId, "%d", robotui->buttonSkillIconIndex);
	imageSkillType_str.append(imageLightId);
	UIImageView * image_skillType = (UIImageView *)imageLight_->getChildByName(imageSkillType_str.c_str());
	std::string str_skillType_string = robotui->getSkillTypeStr(baseSkill->id().c_str());
	image_skillType->setTexture(str_skillType_string.c_str());
	image_skillType->setVisible(true);


	//
	MyPlayerAIConfig::setSkillList(robotui->buttonSkillIconIndex,generalSkillVector.at(setSkillIndex)->getId());

	this->closeAnim();
}

#define GENERALSSHORTCUTSLOT_TAG 785 
void RobotSetSkill::refreshSkillInfo( GameFightSkill * gameFightSkill )
{
	if (Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG))
	{
		Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG)->removeFromParent();
	}

	GeneralsShortcutSlot * generalsShortCutSlot = GeneralsShortcutSlot::create(gameFightSkill);
	generalsShortCutSlot->ignoreAnchorPointForPosition(false);
	generalsShortCutSlot->setAnchorPoint(ccp(0.5f,0.5f));
	generalsShortCutSlot->setPosition(ccp(342,336));
	generalsShortCutSlot->setTouchEnabled(false);
	Layer_skills->addChild(generalsShortCutSlot,0,GENERALSSHORTCUTSLOT_TAG);

	l_skillName->setText(gameFightSkill->getCBaseSkill()->name().c_str());

	//主动/被动
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setText(skillinfo_zhudongjineng);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setText(skillinfo_zhudongjineng);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setText(skillinfo_zhudongjineng);
	}

	char s_level[20];
	sprintf(s_level,"%d",gameFightSkill->getCFightSkill()->level());
	l_skillLvValue->setText(s_level);
	
	char s_mp[20];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	l_skillMpValue->setText(s_mp);
	char s_intervaltime[20];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	l_skillCDTimeValue->setText(s_intervaltime);
	char s_distance[20];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	l_skillDistanceValue ->setText(s_distance);
	
	//show skill des
	this->showSkillDes(gameFightSkill->getCFightSkill()->description(),gameFightSkill->getCFightSkill()->get_next_description());


	if(Layer_skills->getWidgetByName("panel_levelInfo"))
	{
		Layer_skills->getWidgetByName("panel_levelInfo")->removeFromParent();
	}
	UIPanel * panel_levelInfo  = UIPanel::create();
	Layer_skills->addWidget(panel_levelInfo);
	panel_levelInfo->setName("panel_levelInfo");


	/////////////////////////////////////////////////////
	int m_required_level = 0;
	int m_required_point = 0;
	std::string m_str_pre_skill = "";
	std::string m_pre_skill_id = "";
	int m_pre_skill_level = 0;
	int m_required_gold= 0;
	int m_required_skillBookValue = 0;

	//等级
	if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
	{
		m_required_level = gameFightSkill->getCFightSkill()->get_next_required_level();
	}
	//技能点
	if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
	{
		m_required_point = gameFightSkill->getCFightSkill()->get_next_required_point();
	}
	//XXX技能
	if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
	{
		CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
		m_str_pre_skill.append(temp->name());
		m_pre_skill_id.append(temp->id());

		m_pre_skill_level = gameFightSkill->getCFightSkill()->get_next_pre_skill_level();
	}
	//金币
	if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
	{
		m_required_gold = gameFightSkill->getCFightSkill()->get_next_required_gold();
	}
	//金技能书
	if (gameFightSkill->getCFightSkill()->get_next_required_num()>0)
	{
		m_required_skillBookValue = gameFightSkill->getCFightSkill()->get_next_required_num();
	}

	/////////////////////////////////////////////////////
	int lineNum = 0;
	//主角等级
	if (m_required_level>0)
	{
		lineNum++;
		UILabel * l_playerLv = UILabel::create();
		l_playerLv->setText(StringDataManager::getString("skillinfo_zhujuedengji"));
		l_playerLv->setFontName(APP_FONT_NAME);
		l_playerLv->setFontSize(16);
		l_playerLv->setColor(ccc3(47,93,13));
		l_playerLv->setAnchorPoint(ccp(0,0));
		l_playerLv->setPosition(ccp(283,103-lineNum*19));
		l_playerLv->setName("l_playerLv");
		panel_levelInfo->addChild(l_playerLv);

		UILabel * l_playerLv_colon = UILabel::create();
		l_playerLv_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_playerLv_colon->setFontName(APP_FONT_NAME);
		l_playerLv_colon->setFontSize(16);
		l_playerLv_colon->setColor(ccc3(47,93,13));
		l_playerLv_colon->setAnchorPoint(ccp(0,0));
		l_playerLv_colon->setPosition(ccp(347,103-lineNum*19));
		l_playerLv_colon->setName("l_playerLv_colon");
		panel_levelInfo->addChild(l_playerLv_colon);

		char s_next_required_level[20];
		sprintf(s_next_required_level,"%d",m_required_level);
		UILabel * l_playerLvValue = UILabel::create();
		l_playerLvValue->setText(s_next_required_level);
		l_playerLvValue->setFontName(APP_FONT_NAME);
		l_playerLvValue->setFontSize(16);
		l_playerLvValue->setColor(ccc3(35,93,13));
		l_playerLvValue->setAnchorPoint(ccp(0,0));
		l_playerLvValue->setPosition(ccp(357,103-lineNum*19));
		panel_levelInfo->addChild(l_playerLvValue);
		// 
		if (GameView::getInstance()->myplayer->getActiveRole()->level()<m_required_level)  //人物等级未达到技能升级要求
		{
			l_playerLv->setColor(ccc3(212,59,59));
			l_playerLv_colon->setColor(ccc3(212,59,59));
			l_playerLvValue->setColor(ccc3(212,59,59));
		}
	}
	//技能点
	if (m_required_point>0)
	{
		lineNum++;

		UIWidget * widget_skillPoint = UIWidget::create();
		panel_levelInfo->addChild(widget_skillPoint);
		widget_skillPoint->setName("widget_skillPoint");
		widget_skillPoint->setAnchorPoint(ccp(0,0));
		widget_skillPoint->setPosition(ccp(283,103-lineNum*19));

		std::string str_skillPoint = StringDataManager::getString("skillinfo_jinengdian");
		for(int i = 0;i<3;i++)
		{
			std::string str_name = "l_skillPoint";
			char s_index[5];
			sprintf(s_index,"%d",i);
			str_name.append(s_index);
			UILabel * l_skillPoint = UILabel::create();
			l_skillPoint->setText(str_skillPoint.substr(i*3,3).c_str());
			l_skillPoint->setFontName(APP_FONT_NAME);
			l_skillPoint->setFontSize(16);
			l_skillPoint->setColor(ccc3(47,93,13));
			l_skillPoint->setAnchorPoint(ccp(0,0));
			l_skillPoint->setName(str_name.c_str());
			widget_skillPoint->addChild(l_skillPoint);
			l_skillPoint->setPosition(ccp(24*i,0));
		}

		UILabel * l_skillPoint_colon = UILabel::create();
		l_skillPoint_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_skillPoint_colon->setFontName(APP_FONT_NAME);
		l_skillPoint_colon->setFontSize(16);
		l_skillPoint_colon->setColor(ccc3(47,93,13));
		l_skillPoint_colon->setAnchorPoint(ccp(0,0));
		l_skillPoint_colon->setPosition(ccp(347,103-lineNum*19));
		l_skillPoint_colon->setName("l_skillPoint_colon");
		panel_levelInfo->addChild(l_skillPoint_colon);

		char s_required_point[20];
		sprintf(s_required_point,"%d",m_required_point);
		UILabel * l_skillPointValue = UILabel::create();
		l_skillPointValue->setText(s_required_point);
		l_skillPointValue->setFontName(APP_FONT_NAME);
		l_skillPointValue->setFontSize(16);
		l_skillPointValue->setColor(ccc3(47,93,13));
		l_skillPointValue->setAnchorPoint(ccp(0,0));
		l_skillPointValue->setPosition(ccp(357,103-lineNum*19));
		panel_levelInfo->addChild(l_skillPointValue);

		if(GameView::getInstance()->myplayer->player->skillpoint()<m_required_point)//技能点不足
		{
			for(int i = 0;i<3;i++)
			{
				std::string str_name = "l_skillPoint";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				UILabel * l_skillPoint = (UILabel*)widget_skillPoint->getChildByName(str_name.c_str());
				if (l_skillPoint)
				{
					l_skillPoint->setColor(ccc3(212,59,59));
				}
			}
			l_skillPoint_colon->setColor(ccc3(212,59,59));
			l_skillPointValue->setColor(ccc3(212,59,59));
		}
	}
	//XXX技能
	if (m_str_pre_skill != "")
	{
		lineNum++;
		UIWidget * widget_pre_skill = UIWidget::create();
		panel_levelInfo->addChild(widget_pre_skill);
		widget_pre_skill->setName("widget_pre_skill");
		widget_pre_skill->setAnchorPoint(ccp(0,0));
		widget_pre_skill->setPosition(ccp(283,103-lineNum*19));
		if (m_str_pre_skill.size()/3 < 4)
		{
			for(int i = 0;i<m_str_pre_skill.size()/3;i++)
			{
				std::string str_name = "l_preSkill";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);

				UILabel * l_preSkill = UILabel::create();
				l_preSkill->setText(m_str_pre_skill.substr(i*3,3).c_str());
				l_preSkill->setFontName(APP_FONT_NAME);
				l_preSkill->setFontSize(16);
				l_preSkill->setColor(ccc3(47,93,13));
				l_preSkill->setAnchorPoint(ccp(0,0));
				widget_pre_skill->addChild(l_preSkill);
				l_preSkill->setName(str_name.c_str());
				if (m_str_pre_skill.size()/3 == 1)
				{
					l_preSkill->setPosition(ccp(0,0));
				}
				else if (m_str_pre_skill.size()/3 == 2)
				{
					l_preSkill->setPosition(ccp(48*i,0));
				}
				else if (m_str_pre_skill.size()/3 == 3)
				{
					l_preSkill->setPosition(ccp(24*i,0));
				}
			}
		}
		else
		{
			UILabel * l_preSkill = UILabel::create();
			l_preSkill->setText(m_str_pre_skill.c_str());
			l_preSkill->setFontName(APP_FONT_NAME);
			l_preSkill->setFontSize(16);
			l_preSkill->setColor(ccc3(47,93,13));
			l_preSkill->setAnchorPoint(ccp(0,0));
			l_preSkill->setPosition(ccp(0,0));
			widget_pre_skill->addChild(l_preSkill);
			l_preSkill->setName("l_preSkill");
		}

		UILabel * l_preSkill_colon = UILabel::create();
		l_preSkill_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_preSkill_colon->setFontName(APP_FONT_NAME);
		l_preSkill_colon->setFontSize(16);
		l_preSkill_colon->setColor(ccc3(47,93,13));
		l_preSkill_colon->setAnchorPoint(ccp(0,0));
		l_preSkill_colon->setPosition(ccp(347,103-lineNum*19));
		l_preSkill_colon->setName("l_preSkill_colon");
		panel_levelInfo->addChild(l_preSkill_colon);

		char s_pre_skill_level[20];
		sprintf(s_pre_skill_level,"%d",m_pre_skill_level);
		UILabel * l_preSkillValue = UILabel::create();
		l_preSkillValue->setText(s_pre_skill_level);
		l_preSkillValue->setFontName(APP_FONT_NAME);
		l_preSkillValue->setFontSize(16);
		l_preSkillValue->setColor(ccc3(47,93,13));
		l_preSkillValue->setAnchorPoint(ccp(0,0));
		l_preSkillValue->setPosition(ccp(357,103-lineNum*19));
		l_preSkillValue->setName("l_preSkillValue");
		panel_levelInfo->addChild(l_preSkillValue);

		std::map<std::string,GameFightSkill*>::const_iterator cIter;
		cIter = GameView::getInstance()->GameFightSkillList.find(m_pre_skill_id);
		if (cIter == GameView::getInstance()->GameFightSkillList.end()) // 没找到就是指向END了  
		{
		}
		else
		{
			GameFightSkill * gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[m_pre_skill_id];
			if (gameFightSkillStuded)
			{
				if (gameFightSkillStuded->getLevel() < m_pre_skill_level)
				{
					if (m_str_pre_skill.size()/3 < 4)
					{
						for(int i = 0;i<m_str_pre_skill.size()/3;i++)
						{
							std::string str_name = "l_preSkill";
							char s_index[5];
							sprintf(s_index,"%d",i);
							str_name.append(s_index);

							UILabel * l_preSkill = (UILabel*)widget_pre_skill->getChildByName(str_name.c_str());
							if (l_preSkill)
							{
								l_preSkill->setColor(ccc3(212,59,59));
							}
						}
					}
					else
					{
						UILabel * l_preSkill = (UILabel*)widget_pre_skill->getChildByName("l_preSkill");
						if (l_preSkill)
						{
							l_preSkill->setColor(ccc3(212,59,59));
						}
					}
					l_preSkill_colon->setColor(ccc3(212,59,59));
					l_preSkillValue->setColor(ccc3(212,59,59));
				}
			}
		}
	}
	//金币
	if (m_required_gold>0)
	{
		lineNum++;

		UIWidget * widget_gold = UIWidget::create();
		panel_levelInfo->addChild(widget_gold);
		widget_gold->setPosition(ccp(283,103-lineNum*19));

		std::string str_gold = StringDataManager::getString("skillinfo_jinbi");
		for(int i = 0;i<2;i++)
		{
			std::string str_name = "l_gold";
			char s_index[5];
			sprintf(s_index,"%d",i);
			str_name.append(s_index);
			UILabel * l_gold = UILabel::create();
			l_gold->setText(str_gold.substr(i*3,3).c_str());
			l_gold->setFontName(APP_FONT_NAME);
			l_gold->setFontSize(16);
			l_gold->setColor(ccc3(47,93,13));
			l_gold->setAnchorPoint(ccp(0,0));
			l_gold->setName(str_name.c_str());
			widget_gold->addChild(l_gold);
			l_gold->setPosition(ccp(48*i,0));
		}

		UILabel * l_gold_colon = UILabel::create();
		l_gold_colon->setText(StringDataManager::getString("skillinfo_maohao"));
		l_gold_colon->setFontName(APP_FONT_NAME);
		l_gold_colon->setFontSize(16);
		l_gold_colon->setColor(ccc3(47,93,13));
		l_gold_colon->setAnchorPoint(ccp(0,0));
		l_gold_colon->setPosition(ccp(347,103-lineNum*19));
		l_gold_colon->setName("l_gold_colon");
		panel_levelInfo->addChild(l_gold_colon);

		char s_required_gold[20];
		sprintf(s_required_gold,"%d",m_required_gold);
		UILabel * l_goldValue = UILabel::create();
		l_goldValue->setText(s_required_gold);
		l_goldValue->setFontName(APP_FONT_NAME);
		l_goldValue->setFontSize(16);
		l_goldValue->setColor(ccc3(47,93,13));
		l_goldValue->setAnchorPoint(ccp(0,0));
		l_goldValue->setPosition(ccp(357,103-lineNum*19));
		panel_levelInfo->addChild(l_goldValue);

		if(GameView::getInstance()->getPlayerGold()<m_required_gold)//金币不足
		{
			for(int i = 0;i<2;i++)
			{
				std::string str_name = "l_gold";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				UILabel * l_gold = (UILabel*)widget_gold->getChildByName(str_name.c_str());
				if (l_gold)
				{
					l_gold->setColor(ccc3(212,59,59));
				}
			}
			l_gold_colon->setColor(ccc3(212,59,59));
			l_goldValue->setColor(ccc3(212,59,59));
		}
	}
}

void RobotSetSkill::showSkillDes( std::string curSkillDes,std::string nextSkillDes )
{
	//skill des text
	if (m_pUiLayer->getWidgetByTag(123456))
	{
		UIScrollView * scroll_ = (UIScrollView *)m_pUiLayer->getWidgetByTag(123456);
		scroll_->removeFromParentAndCleanup(true);
	}

	UIScrollView * scrollView_info = UIScrollView::create();
	scrollView_info->setTouchEnable(true);
	scrollView_info->setDirection(SCROLLVIEW_DIR_VERTICAL);
	scrollView_info->setSize(CCSizeMake(264,303));
	scrollView_info->setPosition(ccp(425,75));
	scrollView_info->setBounceEnabled(true);
	scrollView_info->setTag(123456);
	m_pUiLayer->addWidget(scrollView_info);

	ccColor3B color_ = ccc3(47,93,13);

	UIImageView * mo_1 = UIImageView::create();
	mo_1->setTexture("res_ui/mo_2.png");
	mo_1->setAnchorPoint(ccp(0,0));
	mo_1->setScale(0.7f);
	scrollView_info->addChild(mo_1);

	UILabelBMFont * labelBM_1 = UILabelBMFont::create();
	labelBM_1->setAnchorPoint(ccp(0,0));
	labelBM_1->setText(StringDataManager::getString("robotui_skill_des_setSkill"));
	labelBM_1->setFntFile("res_ui/font/ziti_3.fnt");
	scrollView_info->addChild(labelBM_1);

	UILabel * label_des=UILabel::create();
	label_des->setText(curSkillDes.c_str());
	label_des->setAnchorPoint(ccp(0,0));
	label_des->setFontSize(16);
	label_des->setColor(color_);
	label_des->setTextAreaSize(CCSizeMake(240,0));
	scrollView_info->addChild(label_des);

	UIImageView * mo_2 = UIImageView::create();
	mo_2->setTexture("res_ui/mo_2.png");
	mo_2->setAnchorPoint(ccp(0,0));
	mo_2->setScale(0.7f);
	scrollView_info->addChild(mo_2);

	UILabelBMFont * labelBM_2 = UILabelBMFont::create();
	labelBM_2->setAnchorPoint(ccp(0,0));
	labelBM_2->setText(StringDataManager::getString("robotui_nextSkill_des_setSkill"));
	labelBM_2->setFntFile("res_ui/font/ziti_3.fnt");
	scrollView_info->addChild(labelBM_2);

	UILabel * label_next_des=UILabel::create();
	label_next_des->setText(nextSkillDes.c_str());
	label_next_des->setAnchorPoint(ccp(0,0));
	label_next_des->setFontSize(16);
	label_next_des->setColor(color_);
	label_next_des->setTextAreaSize(CCSizeMake(240,0));
	scrollView_info->addChild(label_next_des);

	int h_ = mo_1->getContentSize().height * 2;
	int temp_min_h = 100;

	if (label_des->getContentSize().height > temp_min_h)
	{
		h_+= label_des->getContentSize().height;
	}else
	{
		h_+= temp_min_h;
	}

	if (label_next_des->getContentSize().height > temp_min_h)
	{
		h_+= label_next_des->getContentSize().height;
	}else
	{
		h_+= temp_min_h;
	}


	int innerWidth = scrollView_info->getRect().size.width;
	int innerHeight = h_;
	if (h_ < scrollView_info->getRect().size.height)
	{
		innerHeight = scrollView_info->getRect().size.height;
	}

	scrollView_info->setInnerContainerSize(CCSizeMake(innerWidth,innerHeight));

	mo_1->setPosition(ccp(0,innerHeight - mo_1->getContentSize().height ));
	labelBM_1->setPosition(ccp(10,mo_1->getPosition().y+2));
	label_des->setPosition(ccp(0,mo_1->getPosition().y - label_des->getContentSize().height));

	mo_2->setPosition(ccp(0,label_des->getPosition().y - 30));
	labelBM_2->setPosition(ccp(10,mo_2->getPosition().y+2));
	label_next_des->setPosition(ccp(0,mo_2->getPosition().y - label_next_des->getContentSize().height));
}
