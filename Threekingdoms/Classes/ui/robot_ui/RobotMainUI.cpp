#include "RobotMainUI.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../utils/StaticDataManager.h"
#include "../extensions/UITab.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "RobotSetSkill.h"
#include "RobotSetHpAndMp.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../messageclient/element/MapRobotDrug.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/element/CVipInfo.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../legend_script/CCTeachingGuide.h"

using namespace CocosDenshion;


#define UISCENE_MAIN_SCENE_TAG 1000

RobotMainUI::RobotMainUI(void)
{
}

RobotMainUI::~RobotMainUI(void)
{
// 	labelRolebuyHpId_->removeFromParentAndCleanup(true);
// 	labelRolebuyMpId_->removeFromParentAndCleanup(true);
// 	labelGeneralbuyHpId_->removeFromParentAndCleanup(true);
// 	labelGeneralbuyMpId_->removeFromParentAndCleanup(true);

	imageRoleHp->removeFromParentAndCleanup(true);
	imageRoleMp->removeFromParentAndCleanup(true);
	imageGeneralHp->removeFromParentAndCleanup(true);
	imageGeneralMp->removeFromParentAndCleanup(true);
}

RobotMainUI * RobotMainUI::create()
{
	RobotMainUI * _ui=new RobotMainUI();
	if (_ui && _ui->init())
	{
		_ui->autorelease();
		return _ui;
	}
	CC_SAFE_DELETE(_ui);
	return NULL;
}

bool RobotMainUI::init()
{
	if (UIScene::init())
	{
		mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());

		//// first, get AI config, these config has been loaded at Push1001, so ignore
		//std::string playerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
		//MyPlayerAIConfig::loadConfig(playerName);

		winsize =CCDirector::sharedDirector()->getVisibleSize();
		
		UILayer * layer=UILayer::create();
		layer->ignoreAnchorPointForPosition(false);
		layer->setAnchorPoint(ccp(0.5f,0.5f));
		layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		layer->setContentSize(CCSizeMake(800,480));
		addChild(layer);

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		mainPanel = LoadSceneLayer::onHookKillPanel;
		mainPanel->setWidgetTag(UISCENE_MAIN_SCENE_TAG);
		mainPanel->setAnchorPoint(ccp(0.5f,0.5f));
		mainPanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(mainPanel);

		const char * secondStr = StringDataManager::getString("robotui_diaoshi_gua");
		const char * thirdStr = StringDataManager::getString("robotui_diaoshi_ji");
		CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(ccp(55,230));
		layer->addChild(atmature);

		UILayer *m_Layer= UILayer::create();
		m_Layer->ignoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(ccp(0.5f,0.5f));
		m_Layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_Layer->setContentSize(CCSizeMake(800,480));
		this->addChild(m_Layer);

		const char *str_fight = StringDataManager::getString("robotui_tab_fight");
		char *fight_left=const_cast<char*>(str_fight);	

		const char *str_protect = StringDataManager::getString("robotui_tab_protect");
		char *protect_left=const_cast<char*>(str_protect);	

		const char *str_goods = StringDataManager::getString("robotui_tab_goods");
		char *goods_left=const_cast<char*>(str_goods);	

		char *labelFont[]={fight_left,protect_left,goods_left};
		//robotTab=UITab::createWithBMFont(3,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",labelFont,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
		robotTab=UITab::createWithText(3,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",labelFont,HORIZONTAL,5);
		robotTab->setAnchorPoint(ccp(0.5f,0.5f));
		robotTab->setPosition(ccp(100,405));
		robotTab->setHighLightImage("res_ui/tab_1_on.png");
		robotTab->setDefaultPanelByIndex(0);
		robotTab->setPressedActionEnabled(true);
		robotTab->addIndexChangedEvent(this,coco_indexchangedselector(RobotMainUI::changeTabEvent));
		m_Layer->addWidget(robotTab);

		battlePanel =(UIPanel *)UIHelper::seekWidgetByName(mainPanel,"Panel_Hangupthebattle4");
		battlePanel->setVisible(true);
		protectionPanel =(UIPanel *)UIHelper::seekWidgetByName(mainPanel,"Panel_Hangupprotection2");
		protectionPanel->setVisible(false);
		goodsPanel =(UIPanel *)UIHelper::seekWidgetByName(mainPanel,"Panel_Hangupthegoods");
		goodsPanel->setVisible(false);

		/////protect panel(set use hp/mp)
		this->initFightPanel();
		this->initProtectPanel();
		this->initGoodsPanel();

		btn_Exit= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_close");
		CCAssert(btn_Exit != NULL, "should not be nil");
		btn_Exit->setTouchEnable(true);
		btn_Exit->setPressedActionEnabled(true);
		btn_Exit->addReleaseEvent(this,coco_releaseselector(RobotMainUI::callBackExit));

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void RobotMainUI::initSkillButton(UIPanel* mainPanel, int index)
{
	// skill name
	std::string skillButtonName = "SkillBtn_";
	char id[3];
	sprintf(id, "%d", index);
	skillButtonName.append(id);

	buttonSkillIcon = (UIButton *)UIHelper::seekWidgetByName(mainPanel, skillButtonName.c_str());
	CCAssert(buttonSkillIcon != NULL, "should not be nil");
	buttonSkillIcon->setTouchEnable(true);
	buttonSkillIcon->setPressedActionEnabled(true);
	buttonSkillIcon->setWidgetTag(index);
	buttonSkillIcon->addReleaseEvent(this,coco_releaseselector(RobotMainUI::callBackSkillList));

	//set skill light
	std::string imageLight_str = "ImageView_light_";
	char imageLightId[25];
	sprintf(imageLightId, "%d", index);
	imageLight_str.append(imageLightId);
	UIImageView * imageLight_ = (UIImageView *)buttonSkillIcon->getChildByName(imageLight_str.c_str());
	///set skill icon 
	std::string skillImageName = "ImageView_skillbtn_";
	char imageId[25];
	sprintf(imageId, "%d", index);
	skillImageName.append(imageId);
	UIImageView *imageIcon =(UIImageView *)buttonSkillIcon->getChildByName(skillImageName.c_str());
	
	std::string playerSkill_ = "";
	if (index < GameView::getInstance()->myplayer->getMyPlayerAI()->m_skillList.size())
	{
		playerSkill_ = GameView::getInstance()->myplayer->getMyPlayerAI()->m_skillList.at(index);
	}

	std::string imageSkillType_str = "ImageView_skilType_";
	char imageSkillTypeId[25];
	sprintf(imageSkillTypeId, "%d", index);
	imageSkillType_str.append(imageLightId);

	UIImageView * image_skillType = (UIImageView *)imageLight_->getChildByName(imageSkillType_str.c_str());
	
	if (strcmp(playerSkill_.c_str(),"")==0)
	{
		//imageIcon->setTexture("res_ui/jineng/gaoguang_1.png");
		imageIcon->setVisible(false);

		image_skillType->setVisible(false);
		return;
	}

	CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[playerSkill_];
	if (baseSkill)
	{
		std::string playerSkillIcon ="res_ui/jineng_icon/";
		playerSkillIcon.append(baseSkill->icon());
		playerSkillIcon.append(".png");
		imageIcon->setTexture(playerSkillIcon.c_str());
		imageIcon->setVisible(true);
		//add skill type
		std::string str_skillType_string = getSkillTypeStr(baseSkill->id().c_str());
		image_skillType->setTexture(str_skillType_string.c_str());
		image_skillType->setVisible(true);
	}
	else
	{
		imageIcon->setTexture("res_ui/jineng_icon/jineng_2.png");
		imageIcon->setVisible(true);
	}
}

void RobotMainUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void RobotMainUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);

	MyPlayerAIConfig::setMoveRange(curSliderMove);
	MyPlayerAIConfig::setRoleLimitHp(curSliderHp);
	MyPlayerAIConfig::setRoleLimitMp(curSliderMp);
	MyPlayerAIConfig::setGeneralLimitHp(curSliderGeneralHp);
	MyPlayerAIConfig::setGeneralLimitMp(curSliderGeneralMp);
}

void RobotMainUI::callBackExit(CCObject * pSender)
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	this->closeAnim();
}

void RobotMainUI::callBackSkillList(CCObject * pSender)
{
	UIButton* skillButton = dynamic_cast<UIButton*>(pSender);
	buttonSkillIconIndex = skillButton->getWidgetTag();
	RobotSetSkill * robotkill = RobotSetSkill::create();
	robotkill->ignoreAnchorPointForPosition(false);
	robotkill->setAnchorPoint(ccp(0.5f,0.5f));
	robotkill->setPosition(ccp(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(robotkill);
}

void RobotMainUI::callBackStartRobot(CCObject * pSender)
{
	MyPlayerAIConfig::enableRobot(true);
	GameView::getInstance()->myplayer->getMyPlayerAI()->start();

	// hide current button
	UIButton* currentButton = dynamic_cast<UIButton*>(pSender);
	CCAssert(currentButton != NULL, "should not be nil");
	currentButton->setVisible(false);

	// show another button
	UIPanel*  mainPanel = dynamic_cast<UIPanel*>(m_pUiLayer->getWidgetByTag(UISCENE_MAIN_SCENE_TAG));
	UIButton * anotherButton= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_stop");
	CCAssert(anotherButton != NULL, "should not be nil");
	anotherButton->setVisible(true);

	/*设置挂机时对于新获取的物品的处理*/
	this->setGoodPickAndSellOption(STARTROBOTSTATE);
}
void RobotMainUI::callBackStopRobot(CCObject * pSender)
{
	MyPlayerAIConfig::enableRobot(false);
	GameView::getInstance()->myplayer->getMyPlayerAI()->stop();

	// hide current button
	UIButton* currentButton = dynamic_cast<UIButton*>(pSender);
	CCAssert(currentButton != NULL, "should not be nil");
	currentButton->setVisible(false);

	// show another button
	UIPanel*  mainPanel = dynamic_cast<UIPanel*>(m_pUiLayer->getWidgetByTag(UISCENE_MAIN_SCENE_TAG));
	UIButton * anotherButton= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_start");
	CCAssert(anotherButton != NULL, "should not be nil");
	anotherButton->setVisible(true);
	/*结束挂机时对于新获取的物品的处理*/
	this->setGoodPickAndSellOption(ENDROBOTSTATE);
}

void RobotMainUI::changeTabEvent( CCObject * obj )
{
	UITab * tab =(UITab *)obj;
	int num =tab->getCurrentIndex();
	switch(num)
	{
	case 0:
		{
			battlePanel->setVisible(true);
			protectionPanel->setVisible(false);
			goodsPanel->setVisible(false);
		}break;
	case 1:
		{
			UIButton * button_ =(UIButton *)tab->getObjectByIndex(1);
			Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
				sc->endCommand(button_);

			battlePanel->setVisible(false);
			protectionPanel->setVisible(true);
			goodsPanel->setVisible(false);
		}break;
	case 2:
		{
			UIButton * button_ =(UIButton *)tab->getObjectByIndex(2);
			Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
				sc->endCommand(button_);

			battlePanel->setVisible(false);
			protectionPanel->setVisible(false);
			goodsPanel->setVisible(true);
		}break;
	}
}

void RobotMainUI::setAutomaticskill( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * checkbox =(UICheckBox *)obj;
	GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (checkbox->getSelectedState() ==true)
	{
		MyPlayerAIConfig::setAutomaticSkill(1);

		//set mainscene robot button 
		guide_->isOpenSkillMonster=true;
		guide_->setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png");
	}else
	{
		MyPlayerAIConfig::setAutomaticSkill(0);
		guide_->isOpenSkillMonster=false;
		guide_->setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
	}
}

void RobotMainUI::moveRangeConfig( CCObject *obj,SliderEventType type )
{
	UISlider* slider = dynamic_cast<UISlider*>(obj);
	int percent = slider->getPercent();
	moveRangeLabel->setText(CCString::createWithFormat("%d", percent)->getCString());

	//MyPlayerAIConfig::setMoveRange(percent);

	curSliderMove = percent;
}

void RobotMainUI::setGoodsPanelAllChexkBox(CCObject * obj,CheckBoxEventType type)
{
	UICheckBox * checkBox =dynamic_cast<UICheckBox *>(obj);
	int tag_ =checkBox->getTag();
	int select_ =checkBox->getSelectedState();
	/*
	int myVip_Robot_AutoSell_limit = 0;
	VipConfigData::TypeIdAndVipLevel typeAndLevel_robot_autoSell;
	typeAndLevel_robot_autoSell.typeName = StringDataManager::getString("vip_function_instance_RobotSellEquipment");
	typeAndLevel_robot_autoSell.vipLevel = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel();

	std::map<VipConfigData::TypeIdAndVipLevel,CVipInfo*>::const_iterator cIter_Robot_autoSell;
	cIter_Robot_autoSell = VipConfigData::s_vipConfig.find(typeAndLevel_robot_autoSell);
	if (cIter_Robot_autoSell == VipConfigData::s_vipConfig.end()) // 没找到就是指向END了  
	{
	}
	else
	{
		CVipInfo * tempVipInfo = cIter_Robot_autoSell->second;
		myVip_Robot_AutoSell_limit = tempVipInfo->get_add_number();
	}
	*/
	
	int myVip_Robot_AutoSell_limit = 0;
	VipConfigData::TypeIdAndVipLevel typeAndLevel_autoSell;
	typeAndLevel_autoSell.typeName = StringDataManager::getString("vip_function_instance_RobotSellEquipment");

	for(int i = 0;i<VipDescriptionConfig::s_vipDes.size();++i)
	{
		typeAndLevel_autoSell.vipLevel = i;

		std::map<VipConfigData::TypeIdAndVipLevel,CVipInfo*>::const_iterator cIter;
		cIter = VipConfigData::s_vipConfig.find(typeAndLevel_autoSell);
		if (cIter == VipConfigData::s_vipConfig.end()) // 没找到就是指向END了  
		{
			continue;
		}
		else
		{
			myVip_Robot_AutoSell_limit = typeAndLevel_autoSell.vipLevel;
			break;
		}
	}

	//////////////////////

	switch(tag_)
	{
	case 1:
		{
			if (select_ == 1)
			{
				for (int i=1;i<5;i++)
				{
					MyPlayerAIConfig::setGoodsPickAndSell(i,1);
				}
			}else
			{
				for (int i=1;i<5;i++)
				{
					MyPlayerAIConfig::setGoodsPickAndSell(i,0);
				}
			}
		}break;
	case 5:
		{
			if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() >= myVip_Robot_AutoSell_limit)
			{
				if (select_ ==1)
				{
					for (int i=5;i<9;i++)
					{
						MyPlayerAIConfig::setGoodsPickAndSell(i,1);
					}
				}else
				{
					for (int i=5;i<9;i++)
					{
						MyPlayerAIConfig::setGoodsPickAndSell(i,0);
					}
				}
			}else
			{
				//vip level is not------------------
				std::string funName_ = StringDataManager::getString("vip_function_instance_RobotSellEquipment");
				this->showVipFunctionOpen(funName_);
			}
		}break;
	default:
		{
			if (tag_<5)
			{
				MyPlayerAIConfig::setGoodsPickAndSell(1,0);
				MyPlayerAIConfig::setGoodsPickAndSell(tag_,select_);
			}else
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() >=  myVip_Robot_AutoSell_limit)
				{
					MyPlayerAIConfig::setGoodsPickAndSell(5,0);
					MyPlayerAIConfig::setGoodsPickAndSell(tag_,select_);
				}else
				{
					//vip level----------
					std::string funName_ = StringDataManager::getString("vip_function_instance_RobotSellEquipment");
					this->showVipFunctionOpen(funName_);
				}
			}
		}break;
	}
	this->initGoodsPanel();
}

void RobotMainUI::popupBagList(CCObject * obj)
{
	buttonProtectBg =(UIButton *)obj;
	int tag_ =buttonProtectBg->getTag();
	CCSize winsize =CCDirector::sharedDirector()->getVisibleSize();
	//1 roleHP  3 roleMp  2generalHp  4generalMp
	int drugType;
	RobotSetHpAndMp * robotHpAndMp;
	switch(tag_)
	{
	case 1:
		{
			drugType = 1;
			robotHpAndMp = RobotSetHpAndMp::create(drugType);
			robotHpAndMp->ignoreAnchorPointForPosition(false);
			robotHpAndMp->setAnchorPoint(ccp(0.5f,0.5f));
			robotHpAndMp->setPosition(ccp(winsize.width/2,winsize.height/2+robotHpAndMp->getContentSize().height/2-40));
		}break;
	case 2:
		{
			drugType = 3;
			robotHpAndMp = RobotSetHpAndMp::create(drugType);
			robotHpAndMp->ignoreAnchorPointForPosition(false);
			robotHpAndMp->setAnchorPoint(ccp(0.5f,0.5f));
			robotHpAndMp->setPosition(ccp(winsize.width/2,winsize.height/2-robotHpAndMp->getContentSize().height - 10));
		}break;
	case 3:
		{
			drugType = 2;
			robotHpAndMp = RobotSetHpAndMp::create(drugType);
			robotHpAndMp->ignoreAnchorPointForPosition(false);
			robotHpAndMp->setAnchorPoint(ccp(0.5f,0.5f));
			robotHpAndMp->setPosition(ccp(winsize.width/2,winsize.height/2+robotHpAndMp->getContentSize().height/2-40));
		}break;
	case 4:
		{
			drugType = 4;
			robotHpAndMp = RobotSetHpAndMp::create(drugType);
			robotHpAndMp->ignoreAnchorPointForPosition(false);
			robotHpAndMp->setAnchorPoint(ccp(0.5f,0.5f));
			robotHpAndMp->setPosition(ccp(winsize.width/2,winsize.height/2-robotHpAndMp->getContentSize().height - 10));
		}break;

	}

// 	RobotSetHpAndMp * robotHpAndMp = RobotSetHpAndMp::create(drugType);
// 	robotHpAndMp->ignoreAnchorPointForPosition(false);
// 	robotHpAndMp->setAnchorPoint(ccp(0.5f,0.5f));
// 	robotHpAndMp->setPosition(ccp(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(robotHpAndMp);
}

void RobotMainUI::addUseGoods(std::string goodsIcon ,std::string goodsid ,std::string goodsName)
{
	std::string btnName_ =buttonProtectBg->getName();
	//pac index 
	std::string goodsIcon_ = "res_ui/props_icon/";
	goodsIcon_.append(goodsIcon);
	goodsIcon_.append(".png");

	std::string playerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
	std::string btnNameStr =playerName.append(btnName_);

	if (strcmp(btnName_.c_str(),"Button_retore_1")==0)
	{
		imageRoleHp->setTexture(goodsIcon_.c_str());
		labelRolebuyHpId_->setText(goodsName.c_str());
		label_roleHp->setText(goodsName.c_str());

		MyPlayerAIConfig::setRoleUseDrugHpName(goodsid.c_str());
	}
	if (strcmp(btnName_.c_str(),"Button_retore_3")==0)
	{
		imageRoleMp->setTexture(goodsIcon_.c_str());
		labelRolebuyMpId_->setText(goodsName.c_str());
		label_roleHp->setText(goodsName.c_str());

		MyPlayerAIConfig::setRoleUseDrugMpName(goodsid.c_str());
	}
	if (strcmp(btnName_.c_str(),"Button_retore_2")==0)
	{
		imageGeneralHp->setTexture(goodsIcon_.c_str());
		labelGeneralbuyHpId_->setText(goodsName.c_str());
		label_GeneralHp->setText(goodsName.c_str());

		MyPlayerAIConfig::setGeneralUseDrugHpName(goodsid.c_str());
	}
	if (strcmp(btnName_.c_str(),"Button_retore_4")==0)
	{
		imageGeneralMp->setTexture(goodsIcon_.c_str());
		labelGeneralbuyMpId_->setText(goodsName.c_str());
		label_GeneralMp->setText(goodsName.c_str());
		MyPlayerAIConfig::setGeneralUseDrugMpName(goodsid.c_str());
	}
}


void RobotMainUI::showAutomaticGoods( std::string goodsName )
{
	//UIButton * btn =(UIButton *)UIHelper::seekWidgetByName(protectionPanel,goodsName.c_str());
	std::map<int,MapRobotDrug *> * robotdrugMap;
	std::string useDrugId;
	if (strcmp(goodsName.c_str(),"Button_retore_1")==0)
	{
		useDrugId =MyPlayerAIConfig::getRoleUseDrugHpName();
		robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[1];
	}
	if (strcmp(goodsName.c_str(),"Button_retore_3")==0)
	{
		useDrugId =MyPlayerAIConfig::getRoleUseDrugMpName();

		robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[2];
	}

	if (strcmp(goodsName.c_str(),"Button_retore_2")==0)
	{
		useDrugId =MyPlayerAIConfig::getGeneralUseDrugHpName();
		
		robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[3];
	}

	if (strcmp(goodsName.c_str(),"Button_retore_4")==0)
	{
		useDrugId =MyPlayerAIConfig::getGeneralUseDrugMpName();
		robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[4];
	}

	std::string goodsIcon_;
	std::string goodsName_;
	std::string goodsid_;
	int mapsize =robotdrugMap->size();
	for (int i=0;i<mapsize;i++)
	{
		if (i==0)
		{
			goodsIcon_ = robotdrugMap->at(1)->get_icon();
			goodsName_ = robotdrugMap->at(1)->get_name();
			goodsid_ = robotdrugMap->at(1)->get_id();
		}else
		{
			goodsIcon_ =robotdrugMap->at(i*15)->get_icon();
			goodsName_ = robotdrugMap->at(i*15)->get_name();
			goodsid_ = robotdrugMap->at(i*15)->get_id();
		}

		std::string goodsIconStr_ ="res_ui/props_icon/";
		goodsIconStr_.append(goodsIcon_);
		goodsIconStr_.append(".png");

		if (strcmp(useDrugId.c_str(),goodsid_.c_str())==0)
		{
			if (strcmp(goodsName.c_str(),"Button_retore_1")==0)
			{
				imageRoleHp->setTexture(goodsIconStr_.c_str());
				labelRolebuyHpId_->setText(goodsName_.c_str());
				label_roleHp->setText(goodsName_.c_str());

			}
			if (strcmp(goodsName.c_str(),"Button_retore_3")==0)
			{
				imageRoleMp->setTexture(goodsIconStr_.c_str());
				labelRolebuyMpId_->setText(goodsName_.c_str());
				label_roleMp->setText(goodsName_.c_str());
			}

			if (strcmp(goodsName.c_str(),"Button_retore_2")==0)
			{
				imageGeneralHp->setTexture(goodsIconStr_.c_str());
				labelGeneralbuyHpId_->setText(goodsName_.c_str());
				label_GeneralHp->setText(goodsName_.c_str());
			}

			if (strcmp(goodsName.c_str(),"Button_retore_4")==0)
			{
				imageGeneralMp->setTexture(goodsIconStr_.c_str());
				labelGeneralbuyMpId_->setText(goodsName_.c_str());
				label_GeneralMp->setText(goodsName_.c_str());
			}	
		}
	}
}

void RobotMainUI::initFightPanel()
{
	int m_automaticSkill = MyPlayerAIConfig::getAutomaticSkill();
	UICheckBox * checkBoxKill =(UICheckBox *)UIHelper::seekWidgetByName(battlePanel,"CheckBox_honggou_9");
	checkBoxKill->setTouchEnable(true);
	if (m_automaticSkill ==1)
	{
		checkBoxKill->setSelectedState(true);
	}else
	{
		checkBoxKill->setSelectedState(false);
	}
	checkBoxKill->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setAutomaticskill));

	//int m_moveRange =MyPlayerAIConfig::getMoveRange();
	curSliderMove = MyPlayerAIConfig::getMoveRange();
	moveRangeLabel =(UILabel *)UIHelper::seekWidgetByName(battlePanel,"Label_moverange2");
	moveRangeLabel->setText(CCString::createWithFormat("%d",curSliderMove)->getCString());

	UISlider * slider_Sound = (UISlider *)UIHelper::seekWidgetByName(battlePanel,"slider_moveRange");
	slider_Sound->setTouchEnable(true);
	slider_Sound->setPercent(curSliderMove);
	slider_Sound->addEventListenerSlider(this, sliderpercentchangedselector(RobotMainUI::moveRangeConfig));

	//temp not use //autoTeamBtn
// 	UIButton * autoTeamBtn = (UIButton *)UIHelper::seekWidgetByName(battlePanel,"Button_autoteam");
// 	autoTeamBtn->setTouchEnable(true);
// 	autoTeamBtn->setPressedActionEnabled(true);
// 	autoTeamBtn->addReleaseEvent(this,coco_releaseselector());
// 	autoTeamBtn->setVisible(false);

	// init skill buttons
	for(int i = 0; i < ROBOT_SETUP_SKILL_MAX; i++) {
		initSkillButton(battlePanel, i);
	}

	UIButton* button= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_start");
	CCAssert(button != NULL, "should not be nil");
	button->setTouchEnable(true);
	button->setPressedActionEnabled(true);
	button->addReleaseEvent(this,coco_releaseselector(RobotMainUI::callBackStartRobot));
	// setup by config
	if(MyPlayerAIConfig::isEnableRobot())
		button->setVisible(false);
	else
		button->setVisible(true);

	button= (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_stop");
	CCAssert(button != NULL, "should not be nil");
	button->setTouchEnable(true);
	button->setPressedActionEnabled(true);
	button->addReleaseEvent(this,coco_releaseselector(RobotMainUI::callBackStopRobot));
	// setup by config
	if(MyPlayerAIConfig::isEnableRobot())
		button->setVisible(true);
	else
		button->setVisible(false);
}

void RobotMainUI::initProtectPanel()
{
	//role
	checkBoxSetUseHp =(UICheckBox *)UIHelper::seekWidgetByName(protectionPanel,"CheckBox_honggou_1");
	checkBoxSetUseHp->setTouchEnable(true);
	checkBoxSetUseHp->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setCheckBoxuseHp));
	int m_SelectuseHp = MyPlayerAIConfig::getRoleUseHp();
	if (m_SelectuseHp==1)
	{
		checkBoxSetUseHp->setSelectedState(true);
	}else
	{
		checkBoxSetUseHp->setSelectedState(false);
	}

	//int m_setsliderRoleHp =MyPlayerAIConfig::getRoleLimitHp();
	curSliderHp =MyPlayerAIConfig::getRoleLimitHp();
	const char *str_RoleHp = StringDataManager::getString("robotui_role_hp");
	const char *str_automatic = StringDataManager::getString("robotui_role_automatic");
	std::string siliderHpNum_ =CCString::createWithFormat("%d",curSliderHp)->getCString();
	std::string sliderHplabel_="";
	sliderHplabel_.append(str_RoleHp);
	sliderHplabel_.append("<");
	sliderHplabel_.append(siliderHpNum_);
	sliderHplabel_.append("%");
	sliderHplabel_.append(str_automatic);

	rolesliderHplabel =(UILabel *)UIHelper::seekWidgetByName(protectionPanel,"Label_writing1");
	rolesliderHplabel->setText(sliderHplabel_.c_str());

	UIButton * roleSetUseHp =(UIButton *)UIHelper::seekWidgetByName(protectionPanel,"Button_retore_1");
	roleSetUseHp->setTouchEnable(true);
	roleSetUseHp->setTag(1);
	roleSetUseHp->addReleaseEvent(this,coco_releaseselector(RobotMainUI::popupBagList));

	imageRoleHp =UIImageView::create();
	imageRoleHp->setTexture("");
	imageRoleHp->setAnchorPoint(ccp(0.5f,0.5f));
	imageRoleHp->setPosition(ccp(0,0));
	roleSetUseHp->addChild(imageRoleHp);

	label_roleHp = (UILabel *)UIHelper::seekWidgetByName(protectionPanel,"label_retore_1");
	label_roleHp->setZOrder(10);

	label_roleMp = (UILabel *)UIHelper::seekWidgetByName(protectionPanel,"label_retore_3");
	label_roleMp->setZOrder(10);

	label_GeneralHp = (UILabel *)UIHelper::seekWidgetByName(protectionPanel,"label_retore_2");
	label_GeneralHp->setZOrder(10);

	label_GeneralMp = (UILabel *)UIHelper::seekWidgetByName(protectionPanel,"label_retore_4");
	label_GeneralMp->setZOrder(10);

	UISlider * slider_setUseHP = (UISlider *)UIHelper::seekWidgetByName(protectionPanel,"silderHp_protect");
	slider_setUseHP->setTouchEnable(true);
	slider_setUseHP->setPercent(curSliderHp);
	slider_setUseHP->addEventListenerSlider(this, sliderpercentchangedselector(RobotMainUI::setsliderHp));
	//silider mp
	checkBoxSetUseMp =(UICheckBox *)UIHelper::seekWidgetByName(protectionPanel,"CheckBox_honggou_5");
	checkBoxSetUseMp->setTouchEnable(true);
	checkBoxSetUseMp->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setCheckBoxuseMp));
	int m_SelectuseMp = MyPlayerAIConfig::getRoleUseMp();
	if (m_SelectuseMp==1)
	{
		checkBoxSetUseMp->setSelectedState(true);
	}else
	{
		checkBoxSetUseMp->setSelectedState(false);
	}

	//int m_setsliderRoleMp =MyPlayerAIConfig::getRoleLimitMp();
	curSliderMp =MyPlayerAIConfig::getRoleLimitMp();
	const char *str_RoleMp = StringDataManager::getString("robotui_role_mp");
	std::string siliderMpNum_ =CCString::createWithFormat("%d",curSliderMp)->getCString();
	std::string sliderMplabel_="";
	sliderMplabel_.append(str_RoleMp);
	sliderMplabel_.append("<");
	sliderMplabel_.append(siliderMpNum_);
	sliderMplabel_.append("%");
	sliderMplabel_.append(str_automatic);

	rolesliderMplabel =(UILabel *)UIHelper::seekWidgetByName(protectionPanel,"Label_writing2");
	rolesliderMplabel->setText(sliderMplabel_.c_str());

	UIButton * roleSetUseMp =(UIButton *)UIHelper::seekWidgetByName(protectionPanel,"Button_retore_3");
	roleSetUseMp->setTouchEnable(true);
	roleSetUseMp->setTag(3);
	roleSetUseMp->addReleaseEvent(this,coco_releaseselector(RobotMainUI::popupBagList));

	imageRoleMp =UIImageView::create();
	imageRoleMp->setTexture("");
	imageRoleMp->setAnchorPoint(ccp(0.5f,0.5f));
	imageRoleMp->setPosition(ccp(0,0));
	roleSetUseMp->addChild(imageRoleMp);

	UISlider * slider_setUseMp = (UISlider *)UIHelper::seekWidgetByName(protectionPanel,"silderMp_protect");
	slider_setUseMp->setTouchEnable(true);
	slider_setUseMp->setPercent(curSliderMp);
	slider_setUseMp->addEventListenerSlider(this, sliderpercentchangedselector(RobotMainUI::setsliderMp));
	//buy  Label_writing4/Label_writing5
	checkBoxbuyHp =(UICheckBox *)UIHelper::seekWidgetByName(protectionPanel,"CheckBox_honggou_2");
	checkBoxbuyHp->setTouchEnable(true);
	checkBoxbuyHp->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setCheckBoxBuyHp));
	int m_SelectbuyHp=MyPlayerAIConfig::getRoleBuyHp();
	if (m_SelectbuyHp==1)
	{
		checkBoxbuyHp->setSelectedState(true);
	}else
	{
		checkBoxbuyHp->setSelectedState(false);
	}

	labelRolebuyHpId_ =(UILabel *)UIHelper::seekWidgetByName(protectionPanel,"Label_roleRobotHpName");
	labelRolebuyHpId_->setText("");

	//buy mp
	checkBoxbuyMp =(UICheckBox *)UIHelper::seekWidgetByName(protectionPanel,"CheckBox_honggou_6");
	checkBoxbuyMp->setTouchEnable(true);
	checkBoxbuyMp->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setCheckBoxBuyMp));
	int m_SelectbuyMp = MyPlayerAIConfig::getRoleBuyMp();

	if (m_SelectbuyMp==1)
	{
		checkBoxbuyMp->setSelectedState(true);
	}else
	{
		checkBoxbuyMp->setSelectedState(false);
	}

	labelRolebuyMpId_ =(UILabel *)UIHelper::seekWidgetByName(protectionPanel,"Label_roleRobotMpName");
	labelRolebuyMpId_->setText("");

	//general hp / mp
	checkBoxGeneralHp =(UICheckBox *)UIHelper::seekWidgetByName(protectionPanel,"CheckBox_honggou_3");
	checkBoxGeneralHp->setTouchEnable(true);
	int m_SelectgeneraluseHp = MyPlayerAIConfig::getGeneralUseHp();
	if (m_SelectgeneraluseHp==1)
	{
		checkBoxGeneralHp->setSelectedState(true);
	}else
	{
		checkBoxGeneralHp->setSelectedState(false);
	}
	checkBoxGeneralHp->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setCheckBoxGeneralHp));

	//int m_sliderGeneralHp = MyPlayerAIConfig::getGeneralLimitHp();
	curSliderGeneralHp = MyPlayerAIConfig::getGeneralLimitHp();
	const char *str_GeneralHp = StringDataManager::getString("robotui_generalHp");
	std::string siliderGeneralHpNum_ =CCString::createWithFormat("%d",curSliderGeneralHp)->getCString();
	std::string sliderGeneralHplabel_="";
	sliderGeneralHplabel_.append(str_GeneralHp);
	sliderGeneralHplabel_.append("<");
	sliderGeneralHplabel_.append(siliderGeneralHpNum_);
	sliderGeneralHplabel_.append("%");
	sliderGeneralHplabel_.append(str_automatic);

	generalSliderHplabel =(UILabel *)UIHelper::seekWidgetByName(protectionPanel,"Label_writing6");
	generalSliderHplabel->setText(sliderGeneralHplabel_.c_str());

	UIButton * generalUseHp =(UIButton *)UIHelper::seekWidgetByName(protectionPanel,"Button_retore_2");
	generalUseHp->setTouchEnable(true);
	generalUseHp->setTag(2);
	generalUseHp->addReleaseEvent(this,coco_releaseselector(RobotMainUI::popupBagList));

	imageGeneralHp =UIImageView::create();
	imageGeneralHp->setTexture("");
	imageGeneralHp->setAnchorPoint(ccp(0.5f,0.5f));
	imageGeneralHp->setPosition(ccp(0,0));
	generalUseHp->addChild(imageGeneralHp);

	UISlider * slider_setGeneralHp = (UISlider *)UIHelper::seekWidgetByName(protectionPanel,"setsliderGeneralHp");
	slider_setGeneralHp->setTouchEnable(true);
	slider_setGeneralHp->setPercent(curSliderGeneralHp);
	slider_setGeneralHp->addEventListenerSlider(this, sliderpercentchangedselector(RobotMainUI::setsliderGeneralHp));
	//////////////////
	checkBoxGeneralMp =(UICheckBox *)UIHelper::seekWidgetByName(protectionPanel,"CheckBox_honggou_7");
	checkBoxGeneralMp->setTouchEnable(true);
	int m_SelectgeneraluseMp = MyPlayerAIConfig::getGeneralUseMp();
	if (m_SelectgeneraluseMp==1)
	{
		checkBoxGeneralMp->setSelectedState(true);
	}else
	{
		checkBoxGeneralMp->setSelectedState(false);
	}
	checkBoxGeneralMp->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setCheckBoxGeneralMp));

	//int m_sliderGeneralMp =MyPlayerAIConfig::getGeneralLimitMp();
	curSliderGeneralMp = MyPlayerAIConfig::getGeneralLimitMp();
	const char *str_GeneralMp = StringDataManager::getString("robotui_generalMp");
	std::string siliderGeneralMpNum_ =CCString::createWithFormat("%d",curSliderGeneralMp)->getCString();
	std::string sliderGeneralMplabel_="";
	sliderGeneralMplabel_.append(str_GeneralMp);
	sliderGeneralMplabel_.append("<");
	sliderGeneralMplabel_.append(siliderGeneralMpNum_);
	sliderGeneralMplabel_.append("%");
	sliderGeneralMplabel_.append(str_automatic);

	generalSliderMplabel =(UILabel *)UIHelper::seekWidgetByName(protectionPanel,"Label_writing7");
	generalSliderMplabel->setText(sliderGeneralMplabel_.c_str());


	UIButton * generalUseMp =(UIButton *)UIHelper::seekWidgetByName(protectionPanel,"Button_retore_4");
	generalUseMp->setTouchEnable(true);
	generalUseMp->setTag(4);
	generalUseMp->addReleaseEvent(this,coco_releaseselector(RobotMainUI::popupBagList));

	imageGeneralMp =UIImageView::create();
	imageGeneralMp->setTexture("");
	imageGeneralMp->setAnchorPoint(ccp(0.5f,0.5f));
	imageGeneralMp->setPosition(ccp(0,0));
	generalUseMp->addChild(imageGeneralMp);

	UISlider * slider_setGeneralMp = (UISlider *)UIHelper::seekWidgetByName(protectionPanel,"setsliderGeneralMp");
	slider_setGeneralMp->setTouchEnable(true);
	slider_setGeneralMp->setPercent(curSliderGeneralMp);
	slider_setGeneralMp->addEventListenerSlider(this, sliderpercentchangedselector(RobotMainUI::setsliderGeneralMp));

	// GeneralBuyHp
	checkBoxGeneralBuyHp =(UICheckBox *)UIHelper::seekWidgetByName(protectionPanel,"CheckBox_honggou_4");
	checkBoxGeneralBuyHp->setTouchEnable(true);
	int m_SelectgeneralbuyHp = MyPlayerAIConfig::getGeneralBuyHp();
	if (m_SelectgeneralbuyHp==1)
	{
		checkBoxGeneralBuyHp->setSelectedState(true);
	}else
	{
		checkBoxGeneralBuyHp->setSelectedState(false);
	}
	checkBoxGeneralBuyHp->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setCheckBoxGeneralBuyHp));

	// GeneralBuyMp
	checkBoxGeneralBuyMp =(UICheckBox *)UIHelper::seekWidgetByName(protectionPanel,"CheckBox_honggou_9");
	checkBoxGeneralBuyMp->setTouchEnable(true);
	int m_SelectgeneralbuyMp = MyPlayerAIConfig::getGeneralBuyMp();
	if (m_SelectgeneralbuyMp==1)
	{
		checkBoxGeneralBuyMp->setSelectedState(true);
	}else
	{
		checkBoxGeneralBuyMp->setSelectedState(false);
	}
	checkBoxGeneralBuyMp->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setCheckBoxGeneralBuyMp));

	// GeneralBuyHpLabel
	labelGeneralbuyHpId_ =(UILabel *)UIHelper::seekWidgetByName(protectionPanel,"Label_GeneralRobotHpName");
	labelGeneralbuyHpId_->setText("");

	// GeneralBuyMpLabel
	labelGeneralbuyMpId_ = (UILabel *)UIHelper::seekWidgetByName(protectionPanel, "Label_GeneralRobotMpName");
	labelGeneralbuyMpId_->setText("");

	/// automatic equip
	UICheckBox * checkBoxReapair =(UICheckBox *)UIHelper::seekWidgetByName(protectionPanel,"CheckBox_honggou_8");
	checkBoxReapair->setTouchEnable(true);
	int m_Selectrepairequip= MyPlayerAIConfig::getAutoRepairEquip();
	if (m_Selectrepairequip==1)
	{
		checkBoxReapair->setSelectedState(true);
	}else
	{
		checkBoxReapair->setSelectedState(false);
	}
	checkBoxReapair->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setCheckBoxRepairEquip));

	
	//显示设置的药品
	for (int i=1;i<5;i++)
	{
		char strIndex[20];
		sprintf(strIndex,"Button_retore_%d",i);
		this->showAutomaticGoods(strIndex);
	}
	
}

void RobotMainUI::initGoodsPanel()
{
	for (int i=1;i<9;i++)
	{
		std::string checkBoxName ="CheckBox_honggou_";
		char checkIndex[3];
		sprintf(checkIndex,"%d",i);
		checkBoxName.append(checkIndex);

		UICheckBox * checkBoxSetgoods =(UICheckBox *)UIHelper::seekWidgetByName(goodsPanel,checkBoxName.c_str());
		checkBoxSetgoods->setTouchEnable(true);
		checkBoxSetgoods->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setGoodsPanelAllChexkBox));
		int vale_ = MyPlayerAIConfig::getGoodsPickAndSell(i);
		if (vale_==1)
		{
			checkBoxSetgoods->setSelectedState(true);
		}else
		{
			checkBoxSetgoods->setSelectedState(false);
		}
		checkBoxSetgoods->setWidgetTag(i);
	}
}

void RobotMainUI::setsliderHp( CCObject *obj,SliderEventType type )
{
	UISlider* slider = dynamic_cast<UISlider*>(obj);
	int percent = slider->getPercent();

	//MyPlayerAIConfig::setRoleLimitHp(percent);
	curSliderHp = percent;

	const char *str_RoleHp = StringDataManager::getString("robotui_role_hp");
	const char *str_automatic = StringDataManager::getString("robotui_role_automatic");
	std::string siliderHpNum_ =CCString::createWithFormat("%d",percent)->getCString();
	std::string sliderHplabel_="";
	sliderHplabel_.append(str_RoleHp);
	sliderHplabel_.append("<");
	sliderHplabel_.append(siliderHpNum_);
	sliderHplabel_.append("%");
	sliderHplabel_.append(str_automatic);

	rolesliderHplabel->setText(sliderHplabel_.c_str());
}

void RobotMainUI::setsliderMp( CCObject *obj,SliderEventType type )
{
	UISlider* slider = dynamic_cast<UISlider*>(obj);
	int percent = slider->getPercent();
	
	//MyPlayerAIConfig::setRoleLimitMp(percent);
	curSliderMp = percent;

	const char *str_RoleMp = StringDataManager::getString("robotui_role_mp");
	std::string siliderMpNum_ =CCString::createWithFormat("%d",percent)->getCString();
	const char *str_automatic = StringDataManager::getString("robotui_role_automatic");
	std::string sliderMplabel_="";
	sliderMplabel_.append(str_RoleMp);
	sliderMplabel_.append("<");
	sliderMplabel_.append(siliderMpNum_);
	sliderMplabel_.append("%");
	sliderMplabel_.append(str_automatic);

	rolesliderMplabel->setText(sliderMplabel_.c_str());
}

void RobotMainUI::setsliderGeneralHp( CCObject *obj,SliderEventType type )
{
	UISlider* slider = dynamic_cast<UISlider*>(obj);
	int percent = slider->getPercent();

	//MyPlayerAIConfig::setGeneralLimitHp(percent);
	curSliderGeneralHp = percent;

	const char *str_RoleMp = StringDataManager::getString("robotui_generalHp");
	std::string siliderMpNum_ =CCString::createWithFormat("%d",percent)->getCString();
	const char *str_automatic = StringDataManager::getString("robotui_role_automatic");
	std::string sliderMplabel_="";
	sliderMplabel_.append(str_RoleMp);
	sliderMplabel_.append("<");
	sliderMplabel_.append(siliderMpNum_);
	sliderMplabel_.append("%");
	sliderMplabel_.append(str_automatic);

	generalSliderHplabel->setText(sliderMplabel_.c_str());
}

void RobotMainUI::setsliderGeneralMp( CCObject *obj,SliderEventType type )
{
	UISlider* slider = dynamic_cast<UISlider*>(obj);
	int percent = slider->getPercent();
	
	//MyPlayerAIConfig::setGeneralLimitMp(percent);
	curSliderGeneralMp = percent;


	const char *str_RoleMp = StringDataManager::getString("robotui_generalMp");
	std::string siliderMpNum_ =CCString::createWithFormat("%d",percent)->getCString();
	const char *str_automatic = StringDataManager::getString("robotui_role_automatic");
	std::string sliderMplabel_="";
	sliderMplabel_.append(str_RoleMp);
	sliderMplabel_.append("<");
	sliderMplabel_.append(siliderMpNum_);
	sliderMplabel_.append("%");
	sliderMplabel_.append(str_automatic);

	generalSliderMplabel->setText(sliderMplabel_.c_str());
}

void RobotMainUI::setCheckBoxuseHp( CCObject * obj ,CheckBoxEventType type)
{
	UICheckBox * checkBox =dynamic_cast<UICheckBox *>(obj);
	int select_ =checkBox->getSelectedState();

	MyPlayerAIConfig::setRoleUseHp(select_);
}
void RobotMainUI::setCheckBoxuseMp( CCObject * obj  ,CheckBoxEventType type)
{
	UICheckBox * checkBox =dynamic_cast<UICheckBox *>(obj);
	int select_ =checkBox->getSelectedState();

	MyPlayerAIConfig::setRoleUseMp(select_);

}

void RobotMainUI::setCheckBoxBuyHp( CCObject * obj ,CheckBoxEventType type )
{
	UICheckBox * checkBox =dynamic_cast<UICheckBox *>(obj);
	int select_ =checkBox->getSelectedState();
	
	MyPlayerAIConfig::setRoleBuyHp(select_);

}

void RobotMainUI::setCheckBoxBuyMp( CCObject * obj ,CheckBoxEventType type )
{
	UICheckBox * checkBox =dynamic_cast<UICheckBox *>(obj);
	int select_ =checkBox->getSelectedState();

	MyPlayerAIConfig::setRoleBuyMp(select_);

}

void RobotMainUI::setCheckBoxGeneralHp( CCObject * obj ,CheckBoxEventType type )
{
	UICheckBox * checkBox =dynamic_cast<UICheckBox *>(obj);
	int select_ =checkBox->getSelectedState();

	MyPlayerAIConfig::setGeneralUseHp(select_);

}

void RobotMainUI::setCheckBoxGeneralMp( CCObject * obj ,CheckBoxEventType type )
{
	UICheckBox * checkBox =dynamic_cast<UICheckBox *>(obj);
	int select_ =checkBox->getSelectedState();

	MyPlayerAIConfig::setGeneralUseMp(select_);

}

void RobotMainUI::setCheckBoxGeneralBuyHp( CCObject * obj ,CheckBoxEventType type)
{
	UICheckBox * checkBox =dynamic_cast<UICheckBox *>(obj);
	int select_ =checkBox->getSelectedState();
	
	MyPlayerAIConfig::setGeneralBuyHp(select_);

}

void RobotMainUI::setCheckBoxGeneralBuyMp( CCObject * obj,CheckBoxEventType type )
{
	UICheckBox * checkBox =dynamic_cast<UICheckBox *>(obj);
	int select_ =checkBox->getSelectedState();

	MyPlayerAIConfig::setGeneralBuyMp(select_);
}


void RobotMainUI::setCheckBoxRepairEquip( CCObject * obj  ,CheckBoxEventType type)
{
	int myVip_Robot_Repair_limit = 0;
	VipConfigData::TypeIdAndVipLevel typeAndLevel_autoSell;
	typeAndLevel_autoSell.typeName = StringDataManager::getString("vip_function_instance_RobotRepairEquipment");

	for(int i = 0;i<VipDescriptionConfig::s_vipDes.size();++i)
	{
		typeAndLevel_autoSell.vipLevel = i;

		std::map<VipConfigData::TypeIdAndVipLevel,CVipInfo*>::const_iterator cIter;
		cIter = VipConfigData::s_vipConfig.find(typeAndLevel_autoSell);
		if (cIter == VipConfigData::s_vipConfig.end()) // 没找到就是指向END了  
		{
			continue;
		}
		else
		{
			myVip_Robot_Repair_limit = typeAndLevel_autoSell.vipLevel;
			break;
		}
	}

	UICheckBox * checkBox =dynamic_cast<UICheckBox *>(obj);
	if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() >= myVip_Robot_Repair_limit)
	{
		int select_ =checkBox->getSelectedState();
		MyPlayerAIConfig::setAutoRepairEquip(select_);
	}else
	{
		checkBox->setSelectedState(false);

		std::string str_des = StringDataManager::getString("vip_function_des");
		char s_vipLevel[20];
		sprintf(s_vipLevel,"%d",myVip_Robot_Repair_limit);
		str_des.append(s_vipLevel);
		str_des.append(StringDataManager::getString("vip_function_willOpen"));
		GameView::getInstance()->showAlertDialog(str_des.c_str());

		//std::string funName_ = StringDataManager::getString("vip_function_instance_RobotRepairEquipment");
		//this->showVipFunctionOpen(funName_);
	}
	/*
	checkBox->setSelectedState(false);
	const char *strings_ = StringDataManager::getString("feature_will_be_open");
	GameView::getInstance()->showAlertDialog(strings_);
	*/
}

void RobotMainUI::setGoodPickAndSellOption(int robotState)
{
	if (robotState == STARTROBOTSTATE)
	{
		/*拾取： 1全部 2绿装 3蓝装 4白装  */
		/*售出： 5全部 6绿装 7蓝装 8白装  */
		ReqSyncPickupSeting1212 * pickOption =new ReqSyncPickupSeting1212();
		for (int i=1;i<9;i++)
		{
			int checkBoxSelect = MyPlayerAIConfig::getGoodsPickAndSell(i);
			if (i==1 && checkBoxSelect ==0)
			{
				pickOption->set_type(ALL);

				GameMessageProcessor::sharedMsgProcessor()->sendReq(1212,pickOption);
				delete pickOption;
				return;
			}else
			{
				if (checkBoxSelect == 0)
				{
					pickOption->set_type(OPTION);
					switch(i)
					{
					case 2:
						{
							pickOption->add_list(GREEN_EQ);
						}break;
					case 3:
						{
							pickOption->add_list(BLUE_EQ);
						}break;
					case 4:
						{
							pickOption->add_list(WHITE_EQ);
						}break;
					case 5:
						{
							pickOption->add_list(STRENGTHEN_STORE);
						}break;
					}
				}
			}
		}
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1212,pickOption);
		delete pickOption;
	}else if (robotState==ENDROBOTSTATE)
	{
		ReqSyncPickupSeting1212 * pickOption =new ReqSyncPickupSeting1212();
		pickOption->set_type(ALL);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1212,pickOption);
		delete pickOption;
	}
}

void RobotMainUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void RobotMainUI::addCCTutorialIndicator1( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	tutorialIndicator->setPosition(ccp(pos.x+180+_w,pos.y-60+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,87,39,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+135.5f+_w,pos.y+19.5f+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void RobotMainUI::addCCTutorialIndicator2( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	tutorialIndicator->setPosition(ccp(pos.x+280+_w,pos.y-60+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,87,39,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+228+_w,pos.y+19.5f+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void RobotMainUI::addCCTutorialIndicatorClose( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	tutorialIndicator->setPosition(ccp(pos.x-20+_w,pos.y-55+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,40,40,true);
	tutorialIndicator->setDrawNodePos(ccp(pos.x+_w+45,pos.y+_h+15));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void RobotMainUI::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

// 	CCTutorialParticle* tutorialParticle = dynamic_cast<CCTutorialParticle*>(this->getChildByTag(CCTUTORIALPARTICLETAG));
// 	if(tutorialParticle != NULL)
// 		tutorialParticle->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void RobotMainUI::showVipFunctionOpen( std::string functionName )
{
	int opelLevel = -1;
	VipConfigData::TypeIdAndVipLevel typeAndLevel_autoSell;
	typeAndLevel_autoSell.typeName = functionName;

	for(int i = 0;i<=VipDescriptionConfig::s_vipDes.size();++i)
	{
		typeAndLevel_autoSell.vipLevel = i;

		std::map<VipConfigData::TypeIdAndVipLevel,CVipInfo*>::const_iterator cIter;
		cIter = VipConfigData::s_vipConfig.find(typeAndLevel_autoSell);
		if (cIter == VipConfigData::s_vipConfig.end()) // 没找到就是指向END了  
		{
			continue;
		}
		else
		{
			opelLevel = typeAndLevel_autoSell.vipLevel;
			break;
		}
	}
	if (opelLevel < 0)
		return;
	if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() < opelLevel)
	{
		std::string str_des = StringDataManager::getString("vip_function_des");
		char s_vipLevel[20];
		sprintf(s_vipLevel,"%d",opelLevel);
		str_des.append(s_vipLevel);
		str_des.append(StringDataManager::getString("vip_function_willOpen"));
		GameView::getInstance()->showAlertDialog(str_des.c_str());
		return;
	}
}

std::string RobotMainUI::getSkillTypeStr( const char * skillid )
{
	std::map<std::string,CBaseSkill*>::const_iterator cIter;
	cIter = StaticDataBaseSkill::s_baseSkillData.find(skillid);
	if (cIter == StaticDataBaseSkill::s_baseSkillData.end())
	{
		return NULL;
	}

	CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[skillid];

	std::string str_skillVariety_path = "res_ui/font/";
	switch(baseSkill->get_skill_variety())
	{
	case 1:
		{
			str_skillVariety_path.append("dan.png");
		}
		break;
	case 2:
		{
			str_skillVariety_path.append("qun.png");
		}
		break;
	case 3:
		{
			str_skillVariety_path.append("kong.png");
		}
		break;
	case 4:
		{
			str_skillVariety_path.append("liao.png");
		}
		break;
	case 5:
		{
			str_skillVariety_path.append("te.png");
		}
		break;
	case 6:
		{
			str_skillVariety_path.append("fu.png");
		}
		break;
	default:
		{
			str_skillVariety_path.append("fu.png");
		}break;
	}
	
	return str_skillVariety_path;
}








