
#ifndef _ROBOTUI_ROBOTSETHPANDMP_H
#define _ROBOTUI_ROBOTSETHPANDMP_H

#include "../extensions/UIScene.h"
class FolderInfo;
class RobotSetHpAndMp: public UIScene,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	RobotSetHpAndMp(void);
	~RobotSetHpAndMp(void);

	//1 roleHP  3 roleMp  2generalHp  4generalMp
	static RobotSetHpAndMp * create(int drugtype);
	bool init(int drugtype);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);
private:
	std::vector<FolderInfo *>goodsVector;
	int m_drugType;
	int drugLevel;
};

#endif;