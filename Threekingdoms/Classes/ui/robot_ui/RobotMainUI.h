#ifndef _UI_ROBOTMAINUI_H_
#define _UI_ROBOTMAINUI_H_

#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"

/**
  * 挂机模块UI
  * @author zhaogang
  */
class MainScene;
class UITab;
enum
{
	STARTROBOTSTATE =0,
	ENDROBOTSTATE =1,
};
class RobotMainUI : public UIScene
{
public:
	RobotMainUI(void);
	~RobotMainUI(void);

	static RobotMainUI *create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	void callBackExit(CCObject * pSender);
	////////change by liuzhenxing
	void initFightPanel();
	void initProtectPanel();
	void initGoodsPanel();

	void setGoodPickAndSellOption(int robotState);

	void changeTabEvent(CCObject * obj);
	void moveRangeConfig(CCObject *obj,SliderEventType type);
	
	void setAutomaticskill(CCObject * obj,CheckBoxEventType type);
	void popupBagList(CCObject * obj);
	void addUseGoods(std::string goodsIcon ,std::string goodsid,std::string goodsName);
	void showAutomaticGoods(std::string goodsName);

	void callBackStartRobot(CCObject * pSender);
	void callBackStopRobot(CCObject * pSender);
	void callBackSkillList(CCObject * pSender);

	//set skill type
	std::string getSkillTypeStr(const  char * skillid);

private:
	int curSliderMove;
	int curSliderHp;
	int curSliderMp;
	int curSliderGeneralHp;
	int curSliderGeneralMp;
public:
	UIPanel* battlePanel;
	UIPanel* protectionPanel;
	UIPanel * goodsPanel; 
	UIButton * buttonSkillIcon;
	int buttonSkillIconIndex;

	UIImageView * imageRoleHp;
	UIImageView * imageRoleMp;
	UIImageView * imageGeneralHp;
	UIImageView * imageGeneralMp;
	
	UILabel * label_roleHp;
	UILabel * label_roleMp;
	UILabel * label_GeneralHp;
	UILabel * label_GeneralMp;

	UILabel * labelRolebuyHpId_;
	UILabel * labelRolebuyMpId_;
	UILabel * labelGeneralbuyHpId_;
	UILabel * labelGeneralbuyMpId_;
	
	UICheckBox * checkBoxSetUseHp;
	UICheckBox * checkBoxSetUseMp;
	UICheckBox * checkBoxbuyHp;
	UICheckBox * checkBoxbuyMp;

	UICheckBox * checkBoxGeneralHp;
	UICheckBox * checkBoxGeneralMp;
	UICheckBox * checkBoxGeneralBuyHp;
	UICheckBox * checkBoxGeneralBuyMp;

	UIButton * buttonProtectBg;
private:
	void initSkillButton(UIPanel* mainPanel, int index);

	CCSize winsize;
	MainScene *mainUILayer;
	UIPanel * mainPanel;

	UILabel * moveRangeLabel;
	//int m_moveRange;
	UILabel *rolesliderHplabel;
	UILabel *rolesliderMplabel;
	UILabel * generalSliderHplabel;
	UILabel * generalSliderMplabel;

// 	UILabel * roleBuyHpLabel;
// 	UILabel * roleBuyMpLabel;
// 	UILabel * generalBuyHplabel;
// 	UILabel * generalBuyMplabel;

	void setsliderHp(CCObject *obj,SliderEventType type);
	void setsliderMp(CCObject *obj,SliderEventType type);
	void setsliderGeneralHp(CCObject *obj,SliderEventType type);
	void setsliderGeneralMp(CCObject *obj,SliderEventType type);
	
	void setCheckBoxuseHp(CCObject * obj,CheckBoxEventType type);
	void setCheckBoxuseMp(CCObject * obj,CheckBoxEventType type);
	void setCheckBoxBuyHp(CCObject * obj,CheckBoxEventType type);
	void setCheckBoxBuyMp(CCObject * obj,CheckBoxEventType type);
	void setCheckBoxGeneralHp(CCObject * obj,CheckBoxEventType type);
	void setCheckBoxGeneralMp(CCObject * obj,CheckBoxEventType type);
	void setCheckBoxGeneralBuyHp(CCObject * obj,CheckBoxEventType type);
	void setCheckBoxGeneralBuyMp(CCObject * obj,CheckBoxEventType type);
	void setCheckBoxRepairEquip(CCObject * obj,CheckBoxEventType type);
	///goods panel
	int m_checkBoxGoodsIndex;
	void setGoodsPanelAllChexkBox(CCObject * obj,CheckBoxEventType type);
public:
	//vip open
	void showVipFunctionOpen(std::string functionName);

	//study
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void addCCTutorialIndicator2(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void addCCTutorialIndicatorClose(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	int mTutorialScriptInstanceId;

	UIButton * btn_Exit;
	UITab * robotTab;
};

#endif