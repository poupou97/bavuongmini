#ifndef REWARD_REMIND_UI_H 
#define REWARD_REMIND_UI_H

#include "../../ui/extensions/uiscene.h"

USING_NS_CC;
USING_NS_CC_EXT;

//cclabelTTf is set tag of cell use get refresh time
#define REWARD_ONLINE_TIME_TAG 666
#define REWARD_MONEYTREE_TIME_TAG 667


#define CELL_NAME_SP_TAG 700
#define CELL_NAME_TAG 701
#define CELL_LAYER_TAG 777
class CRewardBase;

enum
{
	state_open = 0,
	state_visible,
	stat_show,
};

class RewardUi:public UIScene,public CCTableViewDataSource,public CCTableViewDelegate
{
public:
	RewardUi(void);
	~RewardUi(void);

	static RewardUi * create();
	bool init();
	void onEnter();
	void onExit();

	CCTableView * tablevie_reward;

	void callBackCheck(CCObject * obj);
	void callBackGetReward(CCObject * obj);

	void updateShowCountTime(float dt);

	void callBackExit(CCObject * obj);
	static void setRewardListParticle();

	static void addRewardListEvent(int index_);
	static void removeRewardListEvent(int index);

	void update(float delta);
	int m_uiState;
	int m_openUiTag;
public:
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
public:
	CCSize winsize;
};

#endif