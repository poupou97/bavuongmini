#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
class SetUI:public UIScene
{
public:
	SetUI(void);
	~SetUI(void);

	static SetUI *create();
	bool init();
	void onEnter();
	void onExit();

	void reLoading(CCObject * obj);
	void SureToReLoading(CCObject *obj);

	void callBackExit(CCObject * obj);
	void callBackError(CCObject * obj);

	 void tabIndexChangedEvent(CCObject* pSender);
	//start by:liutao
	void closeWindow(CCObject * obj);

	void sliderMusicEvent(CCObject *pSender, SliderEventType type);

	void sliderSoundEvent(CCObject *pSender, SliderEventType type);

	void getUserDefault();
	void callBackShowNearbyPlayer( CCObject * obj ,CheckBoxEventType type );
protected:
	int m_nMusic;
	int m_nSound;
	//above  by:liutao
};

