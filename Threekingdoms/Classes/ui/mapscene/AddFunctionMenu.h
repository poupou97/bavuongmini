#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class RichTextInputBox;

enum
{
	PRIVATECHAT=0,
	CHECK,
	TEAM,
	FRIEND,
	MAIL,
	MOVE
};
class AddFunctionMenu:public UIScene
{
public:
	AddFunctionMenu(void);
	~AddFunctionMenu(void);

	static AddFunctionMenu * create(long long roleId);
	bool init(long long roleId);
	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void callBackFuncList(CCObject * obj);
private:
	long long m_roleId;
	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};
};

