
#ifndef _MAPSCENE_AREAMAP_H_
#define _MAPSCENE_AREAMAP_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../../legend_engine/LegendLevel.h"
#include "../../legend_engine/CCLegendTiledMap.h"

USING_NS_CC_EXT;
USING_NS_CC;

class UIScene;
class UITab;
class DoorInfo;

class AreaMap :public CCLayer
{
public:
	AreaMap();
	~AreaMap();
	static AreaMap* create(const char* levelName);
	bool init(const char* levelName);

	virtual void update(float dt);

	 void onEnter();
	 void onExit();

	// default implements are used to call script callback if exist
	 bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	 void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	 void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	 void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	 void menuTest(CCObject * pSender);

	 CCPoint m_tBeginPos;
	 CCSprite * backgroundSprite;

	CCSize getMapSize();

	// 将游戏世界坐标转换为地图坐标
	CCPoint convertToMapSpace(const CCPoint& gameWorldPoint);
	// 将cocos2d坐标转换为游戏世界坐标
	CCPoint convertToGameWorldSpace(const CCPoint& cocos2DPoint);

	CCPoint getMyPlayerPosition();
	CCPoint getMapCenterPosition();

private:
	void addDoor(DoorInfo* info, const char* mapId, LegendLevel* pLevelData);
	void loadTeammateInfo(std::string mapId);
	void updateNpcStateIcon(int state);

private:
	CCSize m_mapSize;
	CCSize m_mapSizeAtWorldSpace;   // the size at the world space
	float m_mapScale;
	CCSprite * m_npcMissionIndicate;
};
#endif;
