#include "NearbyPlayersDetail.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "AddFunctionMenu.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"


NearbyPlayersDetail::NearbyPlayersDetail()
{
}


NearbyPlayersDetail::~NearbyPlayersDetail()
{
}

NearbyPlayersDetail* NearbyPlayersDetail::create( long long roleId )
{
	NearbyPlayersDetail * nearbyPlayerDetail = new NearbyPlayersDetail();
	if (nearbyPlayerDetail && nearbyPlayerDetail->init(roleId))
	{
		nearbyPlayerDetail->autorelease();
		return nearbyPlayerDetail;
	}
	CC_SAFE_DELETE(nearbyPlayerDetail);
	return NULL;
}

bool NearbyPlayersDetail::init( long long roleId )
{
	if (UIScene::init())
	{
		/////////////////////////////////////////////////////////////////////////////////////

		m_roleId = roleId;
		// 该UILayer位于CCTableView之内，为了正确响应TableView的拖动事件，故设置为不吞掉触摸事件
		m_pUiLayer->setSwallowsTouches(false);

		UIButton * btn_detail  = UIButton::create();
		btn_detail->setTextures("res_ui/new_button_12.png","res_ui/new_button_12.png","");
		btn_detail->setTouchEnable(true);
		//btn_detail->setPressedActionEnabled(true);
		btn_detail->setScale9Enable(true);
		btn_detail->setScale9Size(CCSizeMake(50,30));
		//btn_buy->setCapInsets(CCRectMake(18,9,2,23));
		btn_detail->setAnchorPoint(ccp(0.5f,0.5f));
		btn_detail->setPosition(ccp(0,0));
		btn_detail->addReleaseEvent(this,coco_releaseselector(NearbyPlayersDetail::callBackClicked));
		m_pUiLayer->addWidget(btn_detail);

		UILabel * label_detail = UILabel::create();
		label_detail->setStrokeEnabled(true);
		label_detail->setText(StringDataManager::getString("map_current_detail"));
		label_detail->setAnchorPoint(ccp(0.5f,0.5f));
		label_detail->setFontName(APP_FONT_NAME);
		label_detail->setFontSize(16);
		label_detail->setPosition(ccp(0, 0));
		label_detail->setColor(ccc3(255, 255, 255));
		btn_detail->addChild(label_detail);

		this->setContentSize(CCSizeMake(60,45));

		return true;
	}
	return false;
}

void NearbyPlayersDetail::callBackClicked( CCObject *obj )
{
	UIButton * btn = (UIButton *)obj;

	int selfLevel_ =GameView::getInstance()->myplayer->getActiveRole()->level();
	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(20);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[20];
	}
	
	if (selfLevel_ < openlevel)
	{
		char openLevelStr[100];
		const char *strings = StringDataManager::getString("chatAndmailIsOpenInFiveLevel");
		sprintf(openLevelStr,strings,openlevel);
		GameView::getInstance()->showAlertDialog(openLevelStr);
		return;
	}


	AddFunctionMenu * nameList=AddFunctionMenu::create(m_roleId);
	nameList->ignoreAnchorPointForPosition(false);
	nameList->setAnchorPoint(ccp(0,0));
	short rol = m_tableview->getPosition().x + 100;
	short col = m_tableview->getPosition().y + 310;
	nameList->setPosition(ccp(rol,col));
	m_layer->addChild(nameList, 4);
}
