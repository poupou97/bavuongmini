#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class NearbyPlayersDetail: public UIScene
{
public:
	NearbyPlayersDetail();
	~NearbyPlayersDetail();

	static NearbyPlayersDetail* create(long long roleId);
	bool init(long long roleId);
	void setLayer(UILayer* layer){m_layer = layer;}
	void setTableView(CCTableView* tableView){m_tableview = tableView;}
private:
	long long m_roleId;
	UILayer* m_layer;
	CCTableView* m_tableview;
private:
	void callBackClicked(CCObject * obj);

};