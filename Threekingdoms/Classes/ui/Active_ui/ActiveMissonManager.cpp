#include "ActiveMissonManager.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../../messageclient/element/MissionInfo.h"
#include "ActiveMissionActions.h"


ActiveMissonManager * ActiveMissonManager::s_missonManager = NULL;

ActiveMissonManager::ActiveMissonManager()
	:m_curActionType(kTypeActiveDoNothing)
	,m_curMoveAction(NULL)
	,m_curTalkWithNpcAction(NULL)
	,m_bIsAutoRunForMission(false)
{

}

ActiveMissonManager::~ActiveMissonManager()
{
	std::vector<MissionInfo *>::iterator iter;
	for (iter = m_vector_curMissonInfo.begin(); iter != m_vector_curMissonInfo.end(); iter++)
	{
		delete *iter;
	}
	m_vector_curMissonInfo.clear();
}

ActiveMissonManager * ActiveMissonManager::instance()
{
	if (NULL == s_missonManager)
	{
		s_missonManager = new ActiveMissonManager();
	}

	return s_missonManager;
}

void ActiveMissonManager::addAcitveMisson( MissionInfo * missionInfo )
{
	// 先是 跑路，然后 弹出任务面板

	// 跑路
	m_curActionType = kTypeActiveMoveAction;

	std::vector<MissionInfo *>::iterator iter;
	for (iter = this->m_vector_curMissonInfo.begin(); iter != this->m_vector_curMissonInfo.end(); iter++)
	{
		delete *iter;
	}
	this->m_vector_curMissonInfo.clear();

	MissionInfo * tempMissionInfo = new MissionInfo();
	tempMissionInfo->CopyFrom(*missionInfo);
	m_vector_curMissonInfo.push_back(tempMissionInfo);

	doActiveMisson(tempMissionInfo);
}

void ActiveMissonManager::doActiveMisson( MissionInfo * missionInfo )
{
	switch (m_curActionType)
	{
	case kTypeActiveDoNothing:
		{
		
		}
		break;
	case kTypeActiveMoveAction:
		{
			m_curMoveAction = ActiveMoveAction::create(missionInfo);
		}
		break;
	case kTypeActiveTalkWithNpc:
		{
			m_curTalkWithNpcAction = ActiveTalkWithNpcAction::create(missionInfo);
		}
		break;
	}
}

void ActiveMissonManager::update()
{
	if (m_bIsAutoRunForMission)
	{
		std::vector<MissionInfo*>::iterator iter;
		for(int i = 0;i < (int)m_vector_curMissonInfo.size(); i++)
		{
			iter = m_vector_curMissonInfo.begin() + i;
			MissionInfo* mi = *iter;
			if (mi == NULL)
			{
				m_vector_curMissonInfo.erase(iter);
				i--;
				continue;
			}
			else
			{
				switch(m_curActionType)
				{
				case kTypeActiveMoveAction :
					{
						if(m_curMoveAction->isFinishedMove())
						{
							m_bIsAutoRunForMission = false;
							CC_SAFE_DELETE(m_curMoveAction);
						}
						break;
					}
				case kTypeActiveTalkWithNpc :
					{
						if (m_curTalkWithNpcAction->isFinishedMove())
						{
							m_bIsAutoRunForMission = false;
							m_curTalkWithNpcAction->doMTalkWithNpcAction();
							CC_SAFE_DELETE(m_curTalkWithNpcAction);
						}

						break;
					}
				}

			}
		}
	}
}
