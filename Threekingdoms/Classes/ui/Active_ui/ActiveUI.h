#ifndef _UI_ACTIVE_ACTIVEUI_H_
#define _UI_ACTIVE_ACTIVEUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../extensions/UITab.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 活跃度UI
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.21
 */

class CCTutorialParticle;
class ActiveNoteData;
class ActiveLabelData;
class KillNotesList;
class CActiveLabelToday;
//class Active;

class ActiveUI:public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	ActiveUI(void);
	~ActiveUI(void);

public:


	UIPanel * s_panel_activeUI;
	UIPanel * s_panel_worldBossUI;
	UIPanel * s_panel_singCopyUI;

	static UIPanel * s_pPanel;
	static ActiveUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

public:
	void callBackBtnClolse(CCObject * obj);																	// 关闭按钮的响应
	void callBackBtnJoin(CCObject * obj);
	void callBackBtnAcitveShop(CCObject * obj);															// 点击 活跃度商城
	
	void callBackGoodIcon(CCObject* obj);																	// 点击icon时的响应
	void callBackActiveTodayBtn(CCObject* obj);																// 点击 今日活跃度 按钮

	void tabIndexChangedEvent(CCObject *pSender);

public:
	//过关斩将
	void initChallengeUi();
	UILayer * m_challenge_layer;	

private:
	UILayer * m_base_layer;																							// 基调layer
	UILayer * m_active_layer;																						// 活跃度layer
	UILayer * m_up_layer;
	CCTableView * m_tableView;																						// 主tableView
	CCScrollView * m_contentScrollView;																				// 活动详情ScrollView
	UILabel * m_label_activeAll;																					// 用户总的活跃度
	CCLabelTTF * m_labelTTF_activeAll;
	UILabel * m_label_activeToday;
	UILabelBMFont * m_labelBMF_activeToday;
	
	CCTableView * m_leftTableView;

	UITab * m_tab;
	UIPanel * m_panel_huoyuedu;

	int m_nTableViewSize;

	CCLabelTTF * m_label_content_name;																		// 活动详情-名称
	CCLabelTTF * m_label_content_time;																		// 活动详情-时间
	CCLabelTTF * m_label_content_gift;																			// 活动详情-奖励
	CCLabelTTF * m_label_content_limit;																			// 活动详情-限制
	CCLabelTTF * m_label_content_describe;																	// 活动详情-描述

	ActiveNoteData * m_curActiveNote;																			// 当前Note信息

	std::vector<ActiveNoteData *> m_vector_activeNote;
	std::vector<ActiveLabelData *> m_vector_activeLabel;
	std::vector<CActiveLabelToday *> m_vector_labelToday;

	UILayer * m_boss_layer;																							// 世界BOSS Layer
	CCSize winSize;

private:
	// 今日活跃度相关
	UILabel * m_label_gift_num_1;
	UILabel * m_label_gift_num_2;
	UILabel * m_label_gift_num_3;
	UILabel * m_label_gift_num_4;
	UILabel * m_label_gift_num_5;

	UIButton * m_btn_gift_frame_1;
	UIButton * m_btn_gift_frame_2;
	UIButton * m_btn_gift_frame_3;
	UIButton * m_btn_gift_frame_4;
	UIButton * m_btn_gift_frame_5;

	UIImageView * m_imageView_gift_icon_1;
	UIImageView * m_imageView_gift_icon_2;
	UIImageView * m_imageView_gift_icon_3;
	UIImageView * m_imageView_gift_icon_4;
	UIImageView * m_imageView_gift_icon_5;

	UILabelBMFont * m_labelBMF_gift_1;
	UILabelBMFont * m_labelBMF_gift_2;
	UILabelBMFont * m_labelBMF_gift_3;
	UILabelBMFont * m_labelBMF_gift_4;
	UILabelBMFont * m_labelBMF_gift_5;

	UIImageView * m_imageView_progress_1;
	UIImageView * m_imageView_progress_2;
	UIImageView * m_imageView_progress_3;
	UIImageView * m_imageView_progress_4;
	UIImageView * m_imageView_progress_5;

private:
	void initUI();
	void initTodayUI();
	
	void reloadActiveInfoData();																						// 更新 活动详情 (old)
	void reloatActiveInfoScrollView();																				// 更新 活动详情（new）

	void initCell(CCTableViewCell * cell, unsigned int nIndex);
	bool IsCanPushVector(int nId);

	void openActiveWindow(UIScene * activeScene, int tag, int nActiveType);				// 1.要打开的UI; 2. UI在MainScene中的tag；3.该活动的类型(暂时保留该方法)

	void activeToSign();																									// 活跃度 点击 签到
	void activeToEctype();																									// 活跃度 点击 副本
	void activeToMisson(int nActiveTypeId);																				// 活跃度 点击 任务
	void activeToPK();																										// 活跃度 点击 竞技场
	void activeToAnswerQuestion();																							// 活跃度 点击 答题
	void activeToOpenPresentBox();																							// 活跃度 点击 开宝箱
	void activeToFamilyFight();																								// 活跃度 点击 家族战
	void activeToSingleFight();																								// 活跃度 点击 过关斩将
	void activeToWorldBoss();																								// 活跃度 点击 世界BOSS
	 
	void activeToRewardMisson();																							// 悬赏任务

public:
	void refrehTableView();
	void initLabelFromInternet();																						// 更新 服务器数据（label）到 本地显示
	void initNoteFromInternet();																						// 更新 服务器数据（note）到 本地显示
	void initUserActiveFromInternet();																				// 更新 服务器数据（userAcitve）到 本地显示
	void initTodayActiveFromInternet();

	void refreshTodayAcitveData();

	void initActiveUIData();																								// 初始化ActiveUI中的数据（外部创建ActiveUI时，调用此方法实现 服务器数据的导入功能）

	std::string getActiveJoinAll(int nJoinCountAll);															// 获取 可参与的总数 的string
	std::string getActiveCanGetAll(int nActiveCanGetAll);												// 获取 可得到活跃度的总数 的string 

	void setTableViewSize(int nTableViewSize);																// 设置tableViewSize
	int getTableViewSize();																								// 获取tableViewSize
	
private:
	std::string getEquipmentQualityByIndex(int quality);													// 根据 品阶 获得该goods需要的底图
	void initGoodsFrame(int nIndex, std::string strFrame, std::string strGoodIconName, CActiveLabelToday* pActiveLabelToday);

	CCActionInterval * initActionForGoods();

	void addParticleForGift(int nIndex);																	// 给指定的今日活跃度奖励添加粒子效果（参数指第几个奖励，从1开始）
	void removeParticleForGift(int nIndex);																	// 删除指定活跃度奖励上的粒子效果（参数指第几个奖励，从1开始）


public:
	//教学
	int mTutorialScriptInstanceId;
	int mTutorialIndex;
	//教学
	virtual void registerScriptCommand(int scriptId);
	//选第三行
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction,int tutorialIndex);

	void addCCTutorialIndicatorSingCopy(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction,int tutorialIndex);
	void removeCCTutorialIndicator();
};

#endif

