#include "ActiveIcon.h"
#include "../../GameView.h"
#include "ActiveUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../utils/StaticDataManager.h"
#include "ActiveShopData.h"
#include "../../messageclient/GameMessageProcessor.h"

ActiveIcon::ActiveIcon(void)
	:m_btn_activeIcon(NULL)
	,m_layer_active(NULL)
	,m_label_active_text(NULL)
	,m_spirte_fontBg(NULL)
{

}


ActiveIcon::~ActiveIcon(void)
{
	
}

ActiveIcon* ActiveIcon::create()
{
	ActiveIcon * activeIcon = new ActiveIcon();
	if (activeIcon && activeIcon->init())
	{
		activeIcon->autorelease();
		return activeIcon;
	}
	CC_SAFE_DELETE(activeIcon);
	return NULL;
}

bool ActiveIcon::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();


		// icon
		m_btn_activeIcon = UIButton::create();
		m_btn_activeIcon->setTouchEnable(true);
		m_btn_activeIcon->setPressedActionEnabled(true);

		m_btn_activeIcon->setTextures("gamescene_state/zhujiemian3/tubiao/active.png", "gamescene_state/zhujiemian3/tubiao/active.png","");
		
		m_btn_activeIcon->setAnchorPoint(ccp(0.5f, 0.5f));
		m_btn_activeIcon->setPosition(ccp( winSize.width - 70 - 159 - 80, winSize.height-70 - 80));
		m_btn_activeIcon->addReleaseEvent(this, coco_cancelselector(ActiveIcon::callBackActive));

		// label
		const char * str_active = StringDataManager::getString("activy_chartInfo2");

		m_label_active_text = UILabel::create();
		m_label_active_text->setText(str_active);
		m_label_active_text->setColor(ccc3(255, 246, 0));
		m_label_active_text->setFontSize(14);
		m_label_active_text->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_active_text->setPosition(ccp(m_btn_activeIcon->getPosition().x, m_btn_activeIcon->getPosition().y - m_btn_activeIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = CCScale9Sprite::create(CCRectMake(5, 10, 1, 4) , "res_ui/zhezhao70.png");
		m_spirte_fontBg->setPreferredSize(CCSize(47, 14));
		m_spirte_fontBg->setAnchorPoint(ccp(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_active_text->getPosition());


		m_layer_active = UILayer::create();
		m_layer_active->setTouchEnabled(true);
		m_layer_active->setAnchorPoint(CCPointZero);
		m_layer_active->setPosition(CCPointZero);
		m_layer_active->setContentSize(winSize);

		//m_layer_active->addChild(m_spirte_fontBg, -10);
		m_layer_active->addWidget(m_btn_activeIcon);
		//m_layer_active->addWidget(m_label_active_text);

		addChild(m_layer_active);

		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void ActiveIcon::callBackActive( CCObject *obj )
{
	// 初始化 UI WINDOW数据
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	ActiveUI* pTmpActiveUI = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	if (NULL == pTmpActiveUI)
	{
		ActiveUI * pActiveUI = ActiveUI::create();
		pActiveUI->ignoreAnchorPointForPosition(false);
		pActiveUI->setAnchorPoint(ccp(0.5f, 0.5f));
		pActiveUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		pActiveUI->setTag(kTagActiveUI);

		GameView::getInstance()->getMainUIScene()->addChild(pActiveUI);

		// 请求 活跃度商店数据（当活跃度商店数据不为空时，说明已经请求过服务器数据，那么不再发送请求）
		if (ActiveShopData::instance()->m_vector_activeShopSource.empty())
		{
			// 请求服务器领取奖品
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5114, NULL);
		}
	}
}

void ActiveIcon::onEnter()
{
	UIScene::onEnter();
}

void ActiveIcon::onExit()
{
	UIScene::onExit();
}
