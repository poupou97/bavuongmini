#ifndef _UI_ACTIVE_ACTIVESHOPUI_H_
#define _UI_ACTIVE_ACTIVESHOPUI_H_

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CHonorCommodity;

class ActiveShopUI : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	ActiveShopUI();
	~ActiveShopUI();

	static UIPanel * s_pPanel;
	static ActiveShopUI * create(std::vector<CHonorCommodity *> vector_activeShopSource);
	bool init(std::vector<CHonorCommodity *> vector_activeShopSource);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject *pSender);

private:
	void clearVectorActiveShopSource();
	UILabel * m_label_activeAll;																				// 玩家总的活跃度


private:
	std::vector<CHonorCommodity*> m_vector_activeShopSource;						// 荣誉度的协议 与 活跃度商店数据的协议 一致，故用一个协议

	CCTableView * m_tableView;

	UILabel * l_honorValue;

public:
	void initUserActiveFromInternet();																		// 更新 服务器数据（userAcitve）到 本地显示
};


//cell中的单项
class ActiveShopCellItem : public CCLayer
{
public:
	ActiveShopCellItem();
	~ActiveShopCellItem();

	static ActiveShopCellItem* create(CHonorCommodity * honorCommodity);
	bool init(CHonorCommodity * honorCommodity);

	void DetailEvent(CCObject * pSender);
	void BuyEvent(CCObject *pSender);

private:
	CHonorCommodity * m_activeShopSource;

public:
	CCTableView *m_tableView;
};


#endif

