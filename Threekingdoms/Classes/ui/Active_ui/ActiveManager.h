#ifndef _UI_ACTIVE_ACTIVEMANAGER_H_
#define _UI_ACTIVE_ACTIVEMANAGER_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * ActiveManager（用于监测寻路是否完成）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.08.01
 */


class ActiveManager : public cocos2d::CCNode
{
public:
	static ActiveManager * s_activeManager;
	static ActiveManager * instance();

	static ActiveManager* create();

	virtual bool init();
	virtual void update(float dt);

	static int s_nStatus;

private:
	ActiveManager(void);
	~ActiveManager(void);

private:
	bool m_bIsUseful;
	bool m_bIsTransportFinish;

	std::string m_strMapId;
	CCPoint m_targetPos;
	long long m_longTargetNpcId;

	int m_activeType;

private:
	void update_searchPath();
	void update_presentBox();

public:
	void set_targetMapId(std::string strMapId);
	std::string get_targetMapId();

	void set_targetPos(CCPoint targetPos);
	CCPoint get_targetpos();

	void set_targetNpcId(long long targetNpcId);
	long long get_targetNpcId();

	void set_transportFinish(bool bFlag);
	bool get_transportFinish();

	void set_activeType(int nActiveType);
	int get_activeType();

	void setLayerUserful(bool bFlag);

	void start_update();
	void cancel_update();

public:
	enum CurActiveLayerStatus 
	{
		status_none = 0,
		status_waitForTransport,   // the player is in the the waiting sequence to transport
		status_transportFinished,   // the player is finished the transport
	};

	static void set_status(int nStatus);
	static int get_status();

	static void AcrossMapTransport(std::string mapId,CCPoint pos);

public:
	enum ActiveType
	{
		TYPE_SINGLEFIGHT = 0,
		TYPE_WORLDBOSS,
		TYPE_PRESENTBOX,
	};
};

#endif

