#ifndef _UI_ACTIVE_ACTIVEDATA_H_
#define _UI_ACTIVE_ACTIVEDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 活跃度模块（用于保存服务器数据）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.22
 */

class CActiveLabel;
class CActiveNote;
class CActiveLabelToday;
class ActiveLabelData;
class ActiveNoteData;

class ActiveData
{
public:
	static ActiveData * s_activeData;
	static ActiveData * instance();

private:
	ActiveData(void);
	~ActiveData(void);

public:
	std::vector<CActiveLabel *> m_vector_internet_label;											// 保存 服务器 中label的 vector
	std::vector<CActiveNote *> m_vector_internet_note;												// 保存 服务器 中label的 note
	std::vector<CActiveLabelToday *> m_vector_internet_labelToday;
	
	std::vector<ActiveLabelData *> m_vector_native_activeLabel;
	std::vector<ActiveNoteData *> m_vector_native_activeNote;			
	
private:
	int m_nUserActive;																								// 用户已拥有的活跃度
	int m_nTodayAcitve;																								// 今日活跃度

public:
	void setUserActive(int nUserAcitve);
	int getUserActive();

	void set_todayActive(int nTodayActive);
	int get_todayActive();

public:
	void initLabelFromInternet();																					// 从 服务器 获取 Label
	void initNoteFromInternet();																					// 从 服务器 获取 active

	void clearLabelAndNote();																						// 清空labelVector和noteVector
	void clearActiveLabel();																							// 清空labelVector
	void clearActiveNote();																							// 清空noteVector
	void clearActiveLabelToday();

	void updateTodayActive(int nActive);															// 根据给出的活跃度，更新btn
};

#endif

