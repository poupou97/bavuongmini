#ifndef _UI_ACTIVE_ACTIVEICONWIDGET_H_
#define _UI_ACTIVE_ACTIVEICONWIDGET_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 活跃度IconWidget（用于加到GuidMap）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.21
 */
class CCTutorialParticle;

class ActiveIconWidget:public UIWidget
{
public:
	ActiveIconWidget(void);
	~ActiveIconWidget(void);

public:
	static ActiveIconWidget* create();
	virtual bool init();
	
	virtual void update(float dt);
	
public:
	UIButton * m_btn_activeIcon;						// 可点击的ICON
	UILayer * m_layer_active;								// 需要用到的layer
	UILabel * m_label_active_text;						// 	ICON下方显示的文字	
	CCScale9Sprite * m_spirte_fontBg;				// 文字显示的底图

public:
	void callBackActive(CCObject *obj);

	bool IsNeedParticle();

private:
	CCTutorialParticle * m_tutorialParticle;					// 活跃度圈圈特效
	

};

#endif

