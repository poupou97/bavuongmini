#ifndef _UI_ACTIVE_ACTIVELABELDATA_H_
#define _UI_ACTIVE_ACTIVELABELDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 活跃度LabelData（服务器 与 UI 数据的结构体）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.22
 */
class CActiveLabel;

class ActiveLabelData
{
public:
	ActiveLabelData(void);
	~ActiveLabelData(void);

public:
	UILabel * m_label_name;												// 主列表框-活动名称
	UILabel * m_label_joinCount;										// 主列表框-参与次数
	UILabel * m_label_active;												// 主列表框-活跃度
	UILabel * m_label_status;												// 主列表框-状态

	UIButton * m_btn_joinIn;												// 主列表框-参加
	UILabel * m_label_joinInText;										// 主列表框-参加按钮文本

	UILabel * m_label_userActive;										// 我的活跃度
	UIButton * m_btn_activeShop;										// 活跃度商城

	CActiveLabel * m_activeLabel;
};

#endif

