#ifndef _UI_ACTIVE_ACTIVETODAYTABLEVIEW_H_
#define _UI_ACTIVE_ACTIVETODAYTABLEVIEW_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 今日活跃度模块（tableView）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.07.25
 */

class CActiveLabelToday;

class ActiveTodayTableView:public CCTableViewDataSource, public CCTableViewDelegate, public CCObject
{
public:
	static ActiveTodayTableView * s_activeToadyTableView;
	static ActiveTodayTableView * instance();

private:
	ActiveTodayTableView(void);
	~ActiveTodayTableView(void);

public:
	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

public:
	std::vector<CActiveLabelToday *> m_vector_labelToday;

private:
	void initCell(CCTableViewCell * cell, unsigned int nIndex);

	void initGoodsInfo(UILayer * ui_layer, unsigned int nIndex, CCScale9Sprite* sprite_bg);

	std::string getEquipmentQualityByIndex(int quality);													// 根据 品阶 获得该goods需要的底图

public:
	void initDataFromInternet();

	void callBackGoodIcon(CCObject* obj);																	// 点击icon时的响应
	void callBackActiveTodayBtn(CCObject* obj);																// 点击 今日活跃度 按钮
};

#endif

