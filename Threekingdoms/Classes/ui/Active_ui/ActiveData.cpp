#include "ActiveData.h"
#include "../../messageclient/element/CActiveLabel.h"
#include "../../messageclient/element/CActiveNote.h"
#include "ActiveNoteData.h"
#include "ActiveLabelData.h"
#include "../../messageclient/element/CActiveLabelToday.h"

ActiveData * ActiveData::s_activeData = NULL;

ActiveData::ActiveData(void)
	:m_nUserActive(0)
	,m_nTodayAcitve(0)
{
}


ActiveData::~ActiveData(void)
{
	clearActiveLabel();	
	clearActiveNote();

	std::vector<ActiveLabelData *>::iterator iterLabel;
	for (iterLabel = m_vector_native_activeLabel.begin(); iterLabel != m_vector_native_activeLabel.end(); iterLabel++)
	{
		delete *iterLabel;
	}
	m_vector_native_activeLabel.clear();

	std::vector<ActiveNoteData *>::iterator iterNote;
	for (iterNote = m_vector_native_activeNote.begin(); iterNote != m_vector_native_activeNote.end(); iterNote++)
	{
		delete *iterNote;
	}
	m_vector_native_activeNote.clear();

}

ActiveData * ActiveData::instance()
{
	if (NULL == s_activeData)
	{
		s_activeData = new ActiveData();
	}

	return s_activeData;
}

void ActiveData::clearLabelAndNote()
{
	clearActiveLabel();
	clearActiveNote();

	clearActiveLabelToday();
}

void ActiveData::setUserActive( int nUserAcitve )
{
	this->m_nUserActive = nUserAcitve;
}

int ActiveData::getUserActive()
{
	return m_nUserActive;
}

void ActiveData::clearActiveLabel()
{
	std::vector<CActiveLabel *>::iterator iterLabel;
	for (iterLabel = m_vector_internet_label.begin(); iterLabel != m_vector_internet_label.end(); iterLabel++)
	{
		delete *iterLabel;
	}
	m_vector_internet_label.clear();
}

void ActiveData::clearActiveNote()
{
	std::vector<CActiveNote *>::iterator iterNote;
	for (iterNote = m_vector_internet_note.begin(); iterNote != m_vector_internet_note.end(); iterNote++)
	{
		delete *iterNote;
	}
	m_vector_internet_note.clear();
}

void ActiveData::initNoteFromInternet()
{
	// 清空本地数据
	std::vector<ActiveNoteData *>::iterator iterNote;
	for (iterNote = m_vector_native_activeNote.begin(); iterNote != m_vector_native_activeNote.end(); iterNote++)
	{
		delete *iterNote;
	}
	m_vector_native_activeNote.clear();

	for (unsigned int i = 0; i < m_vector_internet_note.size(); i++)
	{
		ActiveNoteData * activeNote = new ActiveNoteData();
		activeNote->m_activeNote = new CActiveNote();
		activeNote->m_activeNote->CopyFrom(*m_vector_internet_note.at(i));

		m_vector_native_activeNote.push_back(activeNote);
	}
}

void ActiveData::initLabelFromInternet()
{
	// 清空本地数据
	std::vector<ActiveLabelData *>::iterator iterLabel;
	for (iterLabel = m_vector_native_activeLabel.begin(); iterLabel != m_vector_native_activeLabel.end(); iterLabel++)
	{
		delete *iterLabel;
	}
	m_vector_native_activeLabel.clear();

	for (unsigned int i = 0; i < m_vector_internet_label.size(); i++)
	{
		ActiveLabelData * activeLabel = new ActiveLabelData();
		activeLabel->m_activeLabel = new CActiveLabel();
		activeLabel->m_activeLabel->CopyFrom(*m_vector_internet_label.at(i));

		m_vector_native_activeLabel.push_back(activeLabel);
	}
}

void ActiveData::set_todayActive( int nTodayActive )
{
	this->m_nTodayAcitve = nTodayActive;
}

int ActiveData::get_todayActive()
{
	return this->m_nTodayAcitve;
}

void ActiveData::clearActiveLabelToday()
{
	std::vector<CActiveLabelToday *>::iterator iter;
	for (iter = m_vector_internet_labelToday.begin(); iter != m_vector_internet_labelToday.end(); iter++)
	{
		delete *iter;
	}
	m_vector_internet_labelToday.clear();
}

void ActiveData::updateTodayActive( int nActive )
{
	int nSize = m_vector_internet_labelToday.size();
	for (int i = 0; i < nSize; i++)
	{
		CActiveLabelToday* activeLabelToday = m_vector_internet_labelToday.at(i);
		if (nActive == activeLabelToday->needactive())
		{
			// 奖励物品状态 ：0.未领取奖励，1.已领取奖励完毕
			activeLabelToday->set_status(1);
		}
	}
}



