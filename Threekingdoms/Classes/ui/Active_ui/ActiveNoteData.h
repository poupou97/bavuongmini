#ifndef _UI_ACTIVE_ACTIVENOTEDATA_H_
#define _UI_ACTIVE_ACTIVENOTEDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 活跃度NoteData（服务器 与 UI 数据的结构体）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.25
 */
class CActiveNote;

class ActiveNoteData
{
public:
	ActiveNoteData(void);
	~ActiveNoteData(void);

public:
	CCLabelTTF * m_label_content_name;							// 详情-名称
	CCLabelTTF * m_label_content_time;							// 详情-时间
	CCLabelTTF * m_label_content_gift;								// 详情-奖励
	CCLabelTTF * m_label_content_limit;								// 详情-限制
	CCLabelTTF * m_label_content_describe;						// 详情-描述

	CActiveNote * m_activeNote;

};

#endif

