#include "KillNotesList.h"
#include "../ActiveUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../../GameAudio.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../AppMacros.h"
#include "GameView.h"
#include "../../extensions/CCMoveableMenu.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../utils/GameUtils.h"
#include "WorldBossData.h"
#include "../../../messageclient/element/CDeadHistory.h"
#include "../../../messageclient/element/CBoss.h"
#include "DmgRankingUI.h"
#include "../../../utils/GameConfig.h"

#define  SelectImageTag 458

KillNotesList::KillNotesList(void):
m_nCurRankIdx(-1),
lastSelectCellId(0),
selectCellId(0)
{

}


KillNotesList::~KillNotesList(void)
{
}

KillNotesList* KillNotesList::create()
{
	KillNotesList * killNotesList = new KillNotesList();
	if (killNotesList && killNotesList->init())
	{
		killNotesList->autorelease();
		return killNotesList;
	}
	CC_SAFE_DELETE(killNotesList);
	return NULL;
}

bool KillNotesList::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		m_base_layer = UILayer::create();
		this->addChild(m_base_layer);	

		initUI();

		return true;
	}
	return false;
}

void KillNotesList::onEnter()
{
	UIScene::onEnter();
}

void KillNotesList::onExit()
{
	UIScene::onExit();
}

void KillNotesList::initUI()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	// 主tableView
	m_tableView = CCTableView::create(this, CCSizeMake(413,139));
	//m_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
	//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 25, 1, 1), ccp(0,0));   // 设置九宫格图片
	m_tableView->setDirection(kCCScrollViewDirectionVertical);
	m_tableView->setAnchorPoint(ccp(0, 0));
	m_tableView->setPosition(ccp(331,40));
	m_tableView->setDelegate(this);
	m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
	m_tableView->setPressedActionEnabled(false);
	m_base_layer->addChild(m_tableView);
}


bool KillNotesList::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return false;
}

void KillNotesList::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void KillNotesList::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void KillNotesList::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void KillNotesList::scrollViewDidScroll( CCScrollView* view )
{

}

void KillNotesList::scrollViewDidZoom( CCScrollView* view )
{

}

void KillNotesList::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	selectCellId = cell->getIdx();
	//取出上次选中状态，更新本次选中状态
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();

	m_nCurRankIdx = cell->getIdx();

	if (WorldBossData::getInstance()->m_nCurSelectBossIdx < 0)
		return;

	if (m_nCurRankIdx < 0)
		return;

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagDmgRankingUI) == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		DmgRankingUI * dmgRankingUI = DmgRankingUI::create(WorldBossData::getInstance()->m_nCurSelectBossIdx,m_nCurRankIdx);
		dmgRankingUI->ignoreAnchorPointForPosition(false);
		dmgRankingUI->setAnchorPoint(ccp(.5f,.5f));
		dmgRankingUI->setPosition(ccp(winSize.width/2,winSize.height/2));
		dmgRankingUI->setTag(kTagDmgRankingUI);
		GameView::getInstance()->getMainUIScene()->addChild(dmgRankingUI);
	}
}

cocos2d::CCSize KillNotesList::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(409,47);
}

cocos2d::extension::CCTableViewCell* KillNotesList::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(CCSize(409,45));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setPosition(ccp(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	CDeadHistory * deadHistory = m_deadHistoryList.at(idx);
	ccColor3B color_green = ccc3(47,93,13);

	// 底图
	CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(ccp(0, 0));
	sprite_bigFrame->setPosition(ccp(1, 0));
	sprite_bigFrame->setCapInsets(CCRectMake(9,14,1,1));
	sprite_bigFrame->setPreferredSize(CCSizeMake(409,45));
	cell->addChild(sprite_bigFrame);

// 	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(5, 5, 1, 1) , "res_ui/mengban_green.png");
// 	pHighlightSpr->setPreferredSize(CCSize(409,45));
// 	pHighlightSpr->setAnchorPoint(CCPointZero);
// 	pHighlightSpr->setPosition(ccp(0,0));
// 	//pHighlightSpr->setTag(SelectImageTag);
// 	cell->addChild(pHighlightSpr);
// 	pHighlightSpr->setVisible(false);

	// 击杀日期
	CCLabelTTF * l_date = CCLabelTTF::create(deadHistory->data().c_str(), APP_FONT_NAME, 15, CCSizeMake(200, 0), kCCTextAlignmentLeft);
	l_date->setColor(color_green);
	l_date->setAnchorPoint(ccp(0.f, 0.5f));
	l_date->setPosition(ccp(13, 30));
	cell->addChild(l_date);
	// 击杀时间
	CCLabelTTF * l_time = CCLabelTTF::create(deadHistory->time().c_str(), APP_FONT_NAME, 15, CCSizeMake(200, 0), kCCTextAlignmentLeft);
	l_time->setColor(color_green);
	l_time->setAnchorPoint(ccp(0.f, 0.5f));
	l_time->setPosition(ccp(13, 11));
	cell->addChild(l_time);

	// 最高伤害
	CCLabelTTF * l_maxDmg = CCLabelTTF::create("", APP_FONT_NAME, 18);
	l_maxDmg->setColor(color_green);
	l_maxDmg->setAnchorPoint(ccp(0.0f, 0.5f));
	l_maxDmg->setHorizontalAlignment(kCCTextAlignmentCenter);
	l_maxDmg->setPosition(ccp(135, 23));
	cell->addChild(l_maxDmg);

	if (deadHistory->ranking_size() > 0)
	{
		l_maxDmg->setString(deadHistory->ranking(0).player().name().c_str());

		int countryId = deadHistory->ranking(0).player().countryid();
		if (countryId > 0 && countryId <6)
		{
			std::string countryIdName = "country_";
			char id[2];
			sprintf(id, "%d", countryId);
			countryIdName.append(id);
			std::string iconPathName = "res_ui/country_icon/";
			iconPathName.append(countryIdName);
			iconPathName.append(".png");
			CCSprite * countrySp_ = CCSprite::create(iconPathName.c_str());
			countrySp_->setAnchorPoint(ccp(1.0,0.5f));
			//countrySp_->setPosition(ccp(l_maxDmg->getPositionX() - l_maxDmg->getContentSize().width/2-1,l_maxDmg->getPositionY()));
			countrySp_->setPosition(ccp(l_maxDmg->getPositionX()-1,l_maxDmg->getPositionY()));
			countrySp_->setScale(0.7f);
			cell->addChild(countrySp_);
		}

		//vipInfo
		// vip开关控制
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (vipEnabled)
		{
			if (deadHistory->ranking(0).player().viplevel() > 0)
			{
				CCNode *pNode = MainScene::addVipInfoByLevelForNode(deadHistory->ranking(0).player().viplevel());
				if (pNode)
				{
					cell->addChild(pNode);
					//pNode->setPosition(ccp(l_maxDmg->getPosition().x+l_maxDmg->getContentSize().width/2+13,l_maxDmg->getPosition().y));
					pNode->setPosition(ccp(l_maxDmg->getPosition().x+l_maxDmg->getContentSize().width+13,l_maxDmg->getPosition().y));
				}
			}
		}
	}

	//最后一击
	CCLabelTTF * l_lastKill = CCLabelTTF::create(deadHistory->killer().name().c_str(), APP_FONT_NAME, 18);
	l_lastKill->setColor(color_green);
	l_lastKill->setAnchorPoint(ccp(0.0f, 0.5f));
	l_lastKill->setHorizontalAlignment(kCCTextAlignmentCenter);
	l_lastKill->setPosition(ccp(287, 23));
	cell->addChild(l_lastKill);

	int countryId = deadHistory->killer().countryid();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		CCSprite * countrySp_ = CCSprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(ccp(1.0,0.5f));
		//countrySp_->setPosition(ccp(l_lastKill->getPositionX() - l_lastKill->getContentSize().width/2-1,l_lastKill->getPositionY()));
		countrySp_->setPosition(ccp(l_lastKill->getPositionX()-1,l_lastKill->getPositionY()));
		countrySp_->setScale(0.7f);
		cell->addChild(countrySp_);
	}

	//vipInfo
	// vip开关控制
	bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
	if (vipEnabled)
	{
		if (deadHistory->killer().viplevel() > 0)
		{
			CCNode *pNode = MainScene::addVipInfoByLevelForNode(deadHistory->killer().viplevel());
			if (pNode)
			{
				cell->addChild(pNode);
				//pNode->setPosition(ccp(l_lastKill->getPosition().x+l_lastKill->getContentSize().width/2+13,l_lastKill->getPosition().y));
				pNode->setPosition(ccp(l_lastKill->getPosition().x+l_lastKill->getContentSize().width+13,l_lastKill->getPosition().y));
			}
		}
	}

	if (this->selectCellId == idx)
	{
		pHighlightSpr->setVisible(true);
	}

	return cell;
}

unsigned int KillNotesList::numberOfCellsInTableView( CCTableView *table )
{
	return m_deadHistoryList.size();
}

void KillNotesList::refreshDataSourceByIdx(int idx)
{
	//delete old data
	std::vector<CDeadHistory *>::iterator iter;
	for (iter = m_deadHistoryList.begin(); iter != m_deadHistoryList.end(); iter++)
	{
		delete *iter;
	}
	m_deadHistoryList.clear();

	//add new data

	for (int i = 0;i<WorldBossData::getInstance()->m_worldBossDataList.at(idx)->deadhistory_size();++i)
	{
		CDeadHistory * temp = new CDeadHistory();
		temp->CopyFrom(WorldBossData::getInstance()->m_worldBossDataList.at(idx)->deadhistory(i));
		m_deadHistoryList.push_back(temp);
	}
}

void KillNotesList::refreshUI()
{
	lastSelectCellId = 0;
	selectCellId = 0;
	m_tableView->reloadData();
}
