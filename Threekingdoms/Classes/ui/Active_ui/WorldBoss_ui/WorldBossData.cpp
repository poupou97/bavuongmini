#include "WorldBossData.h"
#include "../../../messageclient/element/CBoss.h"
#include "../../../messageclient/element/CUnReceivePrize.h"

WorldBossData * WorldBossData::s_bossData = NULL;

WorldBossData::WorldBossData(void):
m_nCurSelectBossIdx(-1)
{
}


WorldBossData::~WorldBossData(void)
{
}

WorldBossData * WorldBossData::getInstance()
{
	if (s_bossData == NULL)
	{
		s_bossData = new WorldBossData();
	}

	return s_bossData;
}

void WorldBossData::clearBossDataList()
{
	std::vector<CBoss *>::iterator iter;
	for (iter = m_worldBossDataList.begin(); iter != m_worldBossDataList.end(); iter++)
	{
		delete *iter;
	}
	m_worldBossDataList.clear();
}

void WorldBossData::clearUnReceiveRewardList()
{
	std::vector<CUnReceivePrize *>::iterator iter;
	for (iter = m_unReceiveRewardList.begin(); iter != m_unReceiveRewardList.end(); iter++)
	{
		delete *iter;
	}
	m_unReceiveRewardList.clear();
}
