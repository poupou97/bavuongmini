#ifndef _UI_ACTIVE_WORLDBOSSUI_KILLNOTESLIST_H_
#define _UI_ACTIVE_WORLDBOSSUI_KILLNOTESLIST_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 击杀记录
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.05.27
 */

class CDeadHistory;

class KillNotesList:public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	KillNotesList(void);
	~KillNotesList(void);

public:
	static KillNotesList* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);
private:
	UILayer * m_base_layer;																							// 基调layer
	CCTableView * m_tableView;																						// 主tableView

	std::vector<CDeadHistory*> m_deadHistoryList;
	int m_nCurRankIdx;

	int lastSelectCellId;
	int selectCellId;

private:
	void initUI();

public:
	void refreshDataSourceByIdx(int idx);
	void refreshUI();
};

#endif

