#include "GetRewardUI.h"
#include "WorldBossData.h"
#include "../../../messageclient/element/CBoss.h"
#include "../../../messageclient/element/CRankingPrize.h"
#include "AppMacros.h"
#include "../../../messageclient/element/GoodsInfo.h"
#include "../../../ui/vip_ui/VipRewardCellItem.h"
#include "../../../messageclient/element/CUnReceivePrize.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../utils/StaticDataManager.h"

GetRewardUI::GetRewardUI()
{
}


GetRewardUI::~GetRewardUI()
{
	std::vector<CUnReceivePrize*>::iterator _iter;
	for (_iter = m_unReceiveRewardList.begin(); _iter != m_unReceiveRewardList.end(); ++_iter)
	{
		delete *_iter;
	}
	m_unReceiveRewardList.clear();
}

GetRewardUI * GetRewardUI::create()
{
	GetRewardUI * getRewardUI = new GetRewardUI();
	if (getRewardUI && getRewardUI->init())
	{
		getRewardUI->autorelease();
		return getRewardUI;
	}
	CC_SAFE_DELETE(getRewardUI);
	return NULL;
}

bool GetRewardUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		UILayer * u_layer = UILayer::create();
		addChild(u_layer);

		//加载UI
		UIPanel * ppanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/worldboss_1.json");
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->getValidNode()->setContentSize(CCSizeMake(608, 417));
		ppanel->setPosition(CCPointZero);	
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		UIButton * btn_close = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnable( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(GetRewardUI::CloseEvent));

		UIPanel * panel_rewardDes = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_introduction");
		panel_rewardDes->setVisible(false);
		UIPanel * panel_getReward = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_introduction_0");
		panel_getReward->setVisible(true);
		UIPanel * panel_ranking = (UIPanel*)UIHelper::seekWidgetByName(ppanel,"Panel_ranking");
		panel_ranking->setVisible(false);

		m_tableView = CCTableView::create(this, CCSizeMake(501,290));
		//m_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
		//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 25, 1, 1), ccp(0,0));   // 设置九宫格图片
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0, 0));
		m_tableView->setPosition(ccp(54,43));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		m_tableView->setPressedActionEnabled(false);
		u_layer->addChild(m_tableView);

		UILayer * u_tableViewFrameLayer = UILayer::create();
		this->addChild(u_tableViewFrameLayer);
		UIImageView * tableView_kuang = UIImageView::create();
		tableView_kuang->setTexture("res_ui/LV5_dikuang_miaobian1.png");
		tableView_kuang->setScale9Enable(true);
		tableView_kuang->setScale9Size(CCSizeMake(515,303));
		tableView_kuang->setCapInsets(CCRectMake(32,32,1,1));
		tableView_kuang->setAnchorPoint(ccp(0.5f,0.5f));
		tableView_kuang->setPosition(ccp(303,187));
		u_tableViewFrameLayer->addWidget(tableView_kuang);

		this->setContentSize(CCSizeMake(608,417));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void GetRewardUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5302);
}

void GetRewardUI::onExit()
{
	UIScene::onExit();
}

bool GetRewardUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return true;
}

void GetRewardUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void GetRewardUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void GetRewardUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void GetRewardUI::CloseEvent( CCObject * pSender )
{
	this->closeAnim();
}

void GetRewardUI::scrollViewDidScroll( CCScrollView* view )
{

}

void GetRewardUI::scrollViewDidZoom( CCScrollView* view )
{

}

void GetRewardUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{

}

cocos2d::CCSize GetRewardUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(500,95);
}

cocos2d::extension::CCTableViewCell* GetRewardUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	CUnReceivePrize * unReceivePrize = WorldBossData::getInstance()->m_unReceiveRewardList.at(idx);
	ccColor3B color_green = ccc3(47,93,13);

	// 底图
	CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(ccp(0, 0));
	sprite_bigFrame->setPosition(ccp(0, 0));
	sprite_bigFrame->setCapInsets(CCRectMake(9,14,1,1));
	sprite_bigFrame->setPreferredSize(CCSizeMake(500,93));
	cell->addChild(sprite_bigFrame);

	CCLabelBMFont * l_boss_icon = CCLabelBMFont::create("BOSS","res_ui/font/ziti_3.fnt");
	l_boss_icon->setAnchorPoint(ccp(0.5f, 0.5f));
	l_boss_icon->setPosition(ccp(40, 79));
	cell->addChild(l_boss_icon);
	//Name
	CCLabelTTF * l_name = CCLabelTTF::create(unReceivePrize->name().c_str(),APP_FONT_NAME,18);
	l_name->setAnchorPoint(ccp(0.5f,0.5f));
	l_name->setPosition(ccp(107,79));
	l_name->setColor(color_green);
	cell->addChild(l_name);
	//Level
	std::string str_lv = "LV";
	char s_lv[20];
	sprintf(s_lv,"%d",unReceivePrize->level());
	str_lv.append(s_lv);
	CCLabelTTF * l_lv = CCLabelTTF::create(str_lv.c_str(),APP_FONT_NAME,18);
	l_lv->setAnchorPoint(ccp(0.5f,0.5f));
	l_lv->setPosition(ccp(194,79));
	l_lv->setColor(color_green);
	cell->addChild(l_lv);
	//Date
	CCLabelTTF * l_date = CCLabelTTF::create(unReceivePrize->data().c_str(),APP_FONT_NAME,18);
	l_date->setAnchorPoint(ccp(0.f,0.5f));
	l_date->setPosition(ccp(263,79));
	l_date->setColor(color_green);
	cell->addChild(l_date);
	//Time
	CCLabelTTF * l_time = CCLabelTTF::create(unReceivePrize->time().c_str(),APP_FONT_NAME,18);
	l_time->setAnchorPoint(ccp(0.f,0.5f));
	l_time->setPosition(ccp(365,79));
	l_time->setColor(color_green);
	cell->addChild(l_time);

	//EXP
	CCSprite * sp_exp_icon = CCSprite::create("res_ui/exp.png");
	sp_exp_icon->setAnchorPoint(ccp(0.5f,0.5f));
	sp_exp_icon->setPosition(ccp(40,47));
	cell->addChild(sp_exp_icon);
	char s_expValue[20];
	sprintf(s_expValue,"%d",unReceivePrize->prize().exp());
	CCLabelTTF * l_expValue = CCLabelTTF::create(s_expValue,APP_FONT_NAME,14);
	l_expValue->setAnchorPoint(ccp(0.5f,0.5f));
	l_expValue->setPosition(ccp(107,49));
	l_expValue->setColor(color_green);
	cell->addChild(l_expValue);

	//Gold
	CCSprite * sp_gold_icon = CCSprite::create("res_ui/coins.png");
	sp_gold_icon->setAnchorPoint(ccp(0.5f,0.5f));
	sp_gold_icon->setPosition(ccp(40,22));
	cell->addChild(sp_gold_icon);
	char s_goldValue[20];
	sprintf(s_goldValue,"%d",unReceivePrize->prize().gold());
	CCLabelTTF * l_goldValue = CCLabelTTF::create(s_goldValue,APP_FONT_NAME,14);
	l_goldValue->setAnchorPoint(ccp(0.5f,0.5f));
	l_goldValue->setPosition(ccp(107,22));
	l_goldValue->setColor(color_green);
	cell->addChild(l_goldValue);

	for(int j = 0;j<unReceivePrize->prize().goods_size();++j)
	{
		if (j >= 3)
			continue;

		GoodsInfo * goodInfo = new GoodsInfo();
		goodInfo->CopyFrom(unReceivePrize->prize().goods(j).goods());
		int num = 0;
		num = unReceivePrize->prize().goods(j).quantity();

		VipRewardCellItem * goodsItem = VipRewardCellItem::create(goodInfo,num);
		goodsItem->ignoreAnchorPointForPosition(false);
		goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
		goodsItem->setPosition(ccp(195+79*j,36));
		cell->addChild(goodsItem);

		delete goodInfo;
	}

	UILayer * btn_layer = UILayer::create();
	cell->addChild(btn_layer);
	btn_layer->setSwallowsTouches(false);

	UIButton * btn_getReward = UIButton::create();
	btn_getReward->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
	btn_getReward->setScale9Enabled(true);
	btn_getReward->setScale9Size(CCSizeMake(103,42));
	btn_getReward->setCapInsets(CCRectMake(18,9,2,23));
	btn_getReward->setTouchEnable(true);
	btn_getReward->setPressedActionEnabled(true);
	btn_getReward->setTag(idx);
	btn_getReward->setAnchorPoint(ccp(.5f,.5f));
	btn_getReward->setPosition(ccp(445,36));
	btn_getReward->addReleaseEvent(this,coco_releaseselector(GetRewardUI::getRewardEvent));
	btn_layer->addWidget(btn_getReward);

	UILabel * l_getReward = UILabel::create();
	l_getReward->setText(StringDataManager::getString("btn_lingqu"));
	l_getReward->setAnchorPoint(ccp(.5f,.5f));
	l_getReward->setFontName(APP_FONT_NAME);
	l_getReward->setFontSize(20);
	l_getReward->setPosition(ccp(0,0));
	btn_getReward->addChild(l_getReward);

	return cell;
}

unsigned int GetRewardUI::numberOfCellsInTableView( CCTableView *table )
{
	return WorldBossData::getInstance()->m_unReceiveRewardList.size();
}

void GetRewardUI::RefreshUI()
{
	m_tableView->reloadData();
}

void GetRewardUI::getRewardEvent( CCObject * pSender )
{
	UIButton* temp = dynamic_cast<UIButton*>(pSender);
	if (!temp)
		return;

	int idx = temp->getTag();
	if (WorldBossData::getInstance()->m_unReceiveRewardList.size() > idx)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5303,(void *)WorldBossData::getInstance()->m_unReceiveRewardList.at(idx)->id());
	}

}
