
#ifndef _UI_ACTIVE_WORLDBOSSUI_REWARDDESUI_H_
#define _UI_ACTIVE_WORLDBOSSUI_REWARDDESUI_H_

#include "../../extensions/UIScene.h"
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CRankingPrize;

class RewardDesUI : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
public:
	RewardDesUI();
	~RewardDesUI();

	static RewardDesUI* create(int idx);
	bool init(int idx);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);
private:
	void CloseEvent(CCObject * pSender);

private:
	std::vector<CRankingPrize*> m_rewardDesList;
};

#endif