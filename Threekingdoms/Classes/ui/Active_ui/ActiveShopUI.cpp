#include "ActiveShopUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../GameAudio.h"
#include "../../messageclient/element/CHonorCommodity.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../GameView.h"
#include "../../AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../offlinearena_ui/HonorShopItemBuyInfo.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "ActiveShopItemBuyInfo.h"
#include "../offlinearena_ui/HonorShopItemInfo.h"
#include "ActiveData.h"
#include "../../utils/GameUtils.h"

using namespace CocosDenshion;


UIPanel * ActiveShopUI::s_pPanel;

const CCSize CCSizeTableView = CCSizeMake(498, 262);
const CCPoint CCPointTableView = CCPointMake(52, 74);
const CCSize CCSizeCellSize = CCSizeMake(495, 70);


ActiveShopUI::ActiveShopUI()
	:m_label_activeAll(NULL)
{
}


ActiveShopUI::~ActiveShopUI()
{
	clearVectorActiveShopSource();
}

ActiveShopUI * ActiveShopUI::create(std::vector<CHonorCommodity *> vector_activeShopSource)
{
	ActiveShopUI * activeShopUI = new ActiveShopUI();
	if (activeShopUI && activeShopUI->init(vector_activeShopSource))
	{
		activeShopUI->autorelease();
		return activeShopUI;
	}
	CC_SAFE_DELETE(activeShopUI);
	return NULL;
}

bool ActiveShopUI::init(std::vector<CHonorCommodity *> vector_activeShopSource)
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		// 删除旧数据
		clearVectorActiveShopSource();

		// 将服务器数据更新到 native数据
		for (int i = 0; i < (int)vector_activeShopSource.size(); i++)
		{
			//m_vector_activeShopSource.push_back(vector_activeShopSource.at(i));

			CHonorCommodity * data_ = new CHonorCommodity();
			data_->CopyFrom(*vector_activeShopSource.at(i));
			m_vector_activeShopSource.push_back(data_);
		}

		//create UI
		//加载UI
		if(LoadSceneLayer::activeShopPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::activeShopPanel->removeFromParentAndCleanup(false);
		}
		s_pPanel = LoadSceneLayer::activeShopPanel;

		s_pPanel->setTouchEnable(true);
		s_pPanel->setAnchorPoint(ccp(0.0f,0.0f));
		s_pPanel->getValidNode()->setContentSize(CCSizeMake(608, 417));
		s_pPanel->setPosition(CCPointZero);
		s_pPanel->setScale(1.0f);

		m_pUiLayer->addWidget(s_pPanel);

		UIButton *Button_close = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_close");
		Button_close->setTouchEnable(true);
		Button_close->addReleaseEvent(this, coco_releaseselector(ActiveShopUI::CloseEvent));
		Button_close->setPressedActionEnabled(true);

		// 初始化 活跃度
		initUserActiveFromInternet();

		m_tableView = CCTableView::create(this, CCSizeTableView);
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0, 0));
		m_tableView->setPosition(CCPointTableView);
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(m_tableView);

		/*UILayer * u_tableViewFrameLayer = UILayer::create();
		addChild(u_tableViewFrameLayer);
		UIImageView * tableView_Frame = UIImageView::create();
		tableView_Frame->setTexture("res_ui/LV5_dikuang_miaobian1.png");
		tableView_Frame->setScale9Enable(true);
		tableView_Frame->setScale9Size(CCSizeMake(515,277));
		tableView_Frame->setCapInsets(CCRectMake(32,32,1,1));
		tableView_Frame->setAnchorPoint(ccp(0.5f,0.5f));
		tableView_Frame->setPosition(ccp(299+3,198+6));
		u_tableViewFrameLayer->addWidget(tableView_Frame);*/

		this->setContentSize(CCSizeMake(608,417));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		m_tableView->reloadData();

		return true;
	}
	return false;
}

void ActiveShopUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void ActiveShopUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

bool ActiveShopUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void ActiveShopUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void ActiveShopUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void ActiveShopUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void ActiveShopUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void ActiveShopUI::scrollViewDidScroll( CCScrollView* view )
{

}

void ActiveShopUI::scrollViewDidZoom( CCScrollView* view )
{

}

void ActiveShopUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{

}

cocos2d::CCSize ActiveShopUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeCellSize;
}

cocos2d::extension::CCTableViewCell* ActiveShopUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	if (idx == m_vector_activeShopSource.size() / 2)
	{
		if (m_vector_activeShopSource.size() % 2 == 0)
		{
			ActiveShopCellItem * cellItem1 = ActiveShopCellItem::create(m_vector_activeShopSource.at(2 * idx));
			cellItem1->setPosition(ccp(5, 0));
			cell->addChild(cellItem1);

			ActiveShopCellItem * cellItem2 = ActiveShopCellItem::create(m_vector_activeShopSource.at(2 * idx + 1));
			cellItem2->setPosition(ccp(253, 0));
			cell->addChild(cellItem2);
		}
		else
		{
			ActiveShopCellItem * cellItem1 = ActiveShopCellItem::create(m_vector_activeShopSource.at(2 * idx));
			cellItem1->setPosition(ccp(5, 0));
			cell->addChild(cellItem1);
		}
	}
	else
	{
		ActiveShopCellItem * cellItem1 = ActiveShopCellItem::create(m_vector_activeShopSource.at(2 * idx));
		cellItem1->setPosition(ccp(5, 0));
		cell->addChild(cellItem1);

		ActiveShopCellItem * cellItem2 = ActiveShopCellItem::create(m_vector_activeShopSource.at(2 * idx + 1));
		cellItem2->setPosition(ccp(253, 0));
		cell->addChild(cellItem2);
	}

	return cell;
}

unsigned int ActiveShopUI::numberOfCellsInTableView( CCTableView *table )
{
	if (m_vector_activeShopSource.size() % 2 == 0)
	{
		return m_vector_activeShopSource.size() / 2;
	}
	else
	{
		return m_vector_activeShopSource.size() / 2 + 1;
	}
}

void ActiveShopUI::clearVectorActiveShopSource()
{
	std::vector<CHonorCommodity *>::iterator iter;
	for (iter = m_vector_activeShopSource.begin(); iter != m_vector_activeShopSource.end(); iter++)
	{
		delete *iter;
	}
	m_vector_activeShopSource.clear();
}

void ActiveShopUI::initUserActiveFromInternet()
{
	// 用户总的活跃度
	int nUserActive = ActiveData::instance()->getUserActive();
	char str_userActive[10];
	sprintf(str_userActive, "%d", nUserActive);

	m_label_activeAll = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_activityValue");
	m_label_activeAll->setText(str_userActive);
}



////////////////////////////////////////////////////////////////
ActiveShopCellItem::ActiveShopCellItem()
{
	m_activeShopSource = new CHonorCommodity();
}

ActiveShopCellItem::~ActiveShopCellItem()
{
	delete m_activeShopSource;
}

ActiveShopCellItem* ActiveShopCellItem::create( CHonorCommodity * honorCommodity )
{
	ActiveShopCellItem * honorShopCellItem = new ActiveShopCellItem();
	if (honorShopCellItem && honorShopCellItem->init(honorCommodity))
	{
		honorShopCellItem->autorelease();
		return honorShopCellItem;
	}
	CC_SAFE_DELETE(honorShopCellItem);
	return NULL;
}

bool ActiveShopCellItem::init( CHonorCommodity * honorCommodity )
{
	if (CCLayer::init())
	{
		m_activeShopSource->CopyFrom(*honorCommodity);

		//大边框
		CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/kuang0_new.png");
		sprite_bigFrame->setAnchorPoint(ccp(0, 0));
		sprite_bigFrame->setPosition(ccp(0, 0));
		sprite_bigFrame->setPreferredSize(CCSizeMake(244,68));
		addChild(sprite_bigFrame);

// 		CCScale9Sprite * spbigFrame = CCScale9Sprite::create("res_ui/kuang0_new.png");
// 		spbigFrame->setPreferredSize(CCSizeMake(71,39));
// 		spbigFrame->setCapInsets(CCRect(18,9,2,23));
// 		spbigFrame->setPreferredSize(CCSizeMake(244,68));
// 		CCMenuItemSprite *itemSprite_bigFrame = CCMenuItemSprite::create(spbigFrame, spbigFrame, spbigFrame, this, menu_selector(HonorShopCellItem::DetailEvent));
// 		itemSprite_bigFrame->setZoomScale(1.0f);
// 		CCMoveableMenu *btn_bigFrame = CCMoveableMenu::create(itemSprite_bigFrame,NULL);
// 		btn_bigFrame->setAnchorPoint(ccp(0,0));
// 		btn_bigFrame->setPosition(ccp(122,34));
// 		addChild(btn_bigFrame);

		//物品Icon底框
		std::string iconFramePath;
		switch(m_activeShopSource->goods().quality())
		{
		case 1 :
			{
				iconFramePath = "res_ui/smdi_white.png";
			}
			break;
		case 2 :
			{
				iconFramePath = "res_ui/smdi_green.png";
			}
			break;
		case 3 :
			{
				iconFramePath = "res_ui/smdi_bule.png";
			}
			break;
		case 4 :
			{
				iconFramePath = "res_ui/smdi_purple.png";
			}
			break;
		case 5 :
			{
				iconFramePath = "res_ui/smdi_orange.png";
			}
			break;
		}
// 		CCSprite *  smaillFrame1 = CCSprite::create(iconFramePath.c_str());
// 		CCSprite *  smaillFrame2 = CCSprite::create(iconFramePath.c_str());
// 		CCSprite *  smaillFrame3 = CCSprite::create(iconFramePath.c_str());
// 		CCMenuItemSprite *itemSprite_smallFrame = CCMenuItemSprite::create(smaillFrame1, smaillFrame2, smaillFrame3, this, menu_selector(HonorShopCellItem::DetailEvent));
// 		itemSprite_smallFrame->setZoomScale(1.0f);
// 		CCMenuItemImage* itemSprite_smallFrame = CCMenuItemImage::create(iconFramePath.c_str(), iconFramePath.c_str(),this, menu_selector(HonorShopCellItem::DetailEvent) );
// 		CCMoveableMenu *btn_smallFrame = CCMoveableMenu::create(itemSprite_smallFrame,NULL);
// 		btn_smallFrame->setAnchorPoint(ccp(0,0));
// 		btn_smallFrame->setPosition(ccp(10,10));
// 		addChild(btn_smallFrame);

// 		CCMenuItemImage *item_image = CCMenuItemImage::create(
// 		 	iconFramePath.c_str(),
// 		 	iconFramePath.c_str(),
// 		 	this,
// 		 	menu_selector(HonorShopCellItem::DetailEvent));
// 		CCMenu* btn_smallFrame = CCMenu::create(item_image, NULL);
// 		btn_smallFrame->setAnchorPoint(CCPointZero);
// 		btn_smallFrame->setPosition(ccp(0,0));
// 		addChild(btn_smallFrame);

		CCScale9Sprite * s_normalImage_1 = CCScale9Sprite::create(iconFramePath.c_str());
		s_normalImage_1->setPreferredSize(CCSizeMake(47,47));
		CCMenuItemSprite *firstMenuImage = CCMenuItemSprite::create(s_normalImage_1, s_normalImage_1, s_normalImage_1, this, menu_selector(ActiveShopCellItem::DetailEvent));
		firstMenuImage->setZoomScale(1.0f);
		firstMenuImage->setAnchorPoint(ccp(0.5f,0.5f));
		CCMoveableMenu *firstMenu = CCMoveableMenu::create(firstMenuImage, NULL);
		firstMenu->setContentSize(CCSizeMake(47,47));
		firstMenu->setPosition(ccp(37,sprite_bigFrame->getContentSize().height/2+1));
		addChild(firstMenu);
		
		std::string pheadPath = "res_ui/props_icon/";
		pheadPath.append(m_activeShopSource->goods().icon().c_str());
		pheadPath.append(".png");
		CCSprite * phead = CCSprite::create(pheadPath.c_str());
		phead->ignoreAnchorPointForPosition(false);
		phead->setAnchorPoint(ccp(0.5f, 0.5f));
		phead->setPosition(ccp(37, sprite_bigFrame->getContentSize().height/2+1));
		phead->setScale(0.85f);
		addChild(phead);

		CCLabelTTF * pName = CCLabelTTF::create(m_activeShopSource->goods().name().c_str(),APP_FONT_NAME,18);
		pName->setAnchorPoint(ccp(0.5f,0.5f));
		pName->setPosition(ccp(112,44));
		pName->setColor(GameView::getInstance()->getGoodsColorByQuality(m_activeShopSource->goods().quality()));
		addChild(pName);

		char * s = new char [20];
		sprintf(s,"%d",m_activeShopSource->honor());
		CCLabelTTF * pPrice = CCLabelTTF::create(s,APP_FONT_NAME,16);
		delete [] s;
		pPrice->setAnchorPoint(ccp(0.5f,0.5f));
		pPrice->setPosition(ccp(112,25));
		addChild(pPrice);

		CCScale9Sprite * spBg = CCScale9Sprite::create("res_ui/new_button_2.png");
		spBg->setPreferredSize(CCSizeMake(71,39));
		spBg->setCapInsets(CCRect(18,9,2,23));
		CCMenuItemSprite *itemSprite = CCMenuItemSprite::create(spBg, spBg, spBg, this, menu_selector(ActiveShopCellItem::BuyEvent));
		itemSprite->setZoomScale(1.3f);
		CCMoveableMenu *btn_buy = CCMoveableMenu::create(itemSprite,NULL);
		btn_buy->setAnchorPoint(ccp(0.5f,0.5f));
		btn_buy->setPosition(ccp(206,34));
		addChild(btn_buy);

		const char *str1 = StringDataManager::getString("goods_auction_buy");
		char* p1 =const_cast<char*>(str1);
		CCLabelTTF *l_buy=CCLabelTTF::create(p1,APP_FONT_NAME,18);
		l_buy->setAnchorPoint(ccp(0.5f,0.5f));
		l_buy->setPosition(ccp(71/2,39/2));
		itemSprite->addChild(l_buy);

		this->setContentSize(CCSizeMake(244,68));

		return true;
	}
	return false;
}

void ActiveShopCellItem::BuyEvent( CCObject *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveShopItemBuyInfo) == NULL)
	{
 	 	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
 	 	ActiveShopItemBuyInfo * activeShopItemBuyInfo = ActiveShopItemBuyInfo::create(m_activeShopSource);
 	 	activeShopItemBuyInfo->ignoreAnchorPointForPosition(false);
 	 	activeShopItemBuyInfo->setAnchorPoint(ccp(0.5f,0.5f));
 	 	activeShopItemBuyInfo->setPosition(ccp(winSize.width/2,winSize.height/2));
		activeShopItemBuyInfo->setTag(kTagActiveShopItemBuyInfo);
 	 	GameView::getInstance()->getMainUIScene()->addChild(activeShopItemBuyInfo);
	}
}

void ActiveShopCellItem::DetailEvent( CCObject * pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveShopItemInfo) == NULL)
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		HonorShopItemInfo * honorShopItemInfo = HonorShopItemInfo::create(m_activeShopSource);
		honorShopItemInfo->ignoreAnchorPointForPosition(false);
		honorShopItemInfo->setAnchorPoint(ccp(0.5f,0.5f));
		//honorShopItemInfo->setPosition(ccp(winSize.width/2,winSize.height/2));
		honorShopItemInfo->setTag(kTagActiveShopItemInfo);
		GameView::getInstance()->getMainUIScene()->addChild(honorShopItemInfo);
	}
}
