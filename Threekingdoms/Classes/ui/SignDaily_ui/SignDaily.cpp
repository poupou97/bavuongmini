#include "SignDaily.h"
#include "../../messageclient/element/GoodsInfo.h"

SignDaily::SignDaily( void )
	:m_btn_sign(NULL)
	,m_label_btnText(NULL)
	,m_image_red(NULL)
	,m_btn_gift1_frame(NULL)
	,m_image_gift1_icon(NULL)
	,m_label_gift1_num(NULL)
	,m_btn_gift2_frame(NULL)
	,m_image_gift2_icon(NULL)
	,m_label_gift2_num(NULL)
	,m_btn_gift3_frame(NULL)
	,m_image_gift3_icon(NULL)
	,m_label_gift3_num(NULL)
	,m_btn_gift4_frame(NULL)
	,m_image_gift4_icon(NULL)
	,m_label_gift4_num(NULL)
	,m_nGiftIndex(0)
	,m_nStatus(0)
{

}


SignDaily::~SignDaily( void )
{
	std::vector<GoodsInfo *>::iterator iter;
	for (iter = m_vector_goods.begin(); iter != m_vector_goods.end(); iter++)
	{
		delete *iter;
	}
	m_vector_goods.clear();
}

