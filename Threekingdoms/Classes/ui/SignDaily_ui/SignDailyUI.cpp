#include "SignDailyUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "SignDailyData.h"
#include "SignDaily.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/COneSignEverydayGift.h"
#include "../../GameView.h"
#include "SignDailyIcon.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/GameUtils.h"

using namespace CocosDenshion;

UIPanel * SignDailyUI::s_pPanel;

#define  TAG_TUTORIALPARTICLE 500

SignDailyUI::SignDailyUI(void)
	:m_nGiftNum(0)
{
	m_array_tagBtnSign[0] = TAG_BTN_SIGN_1; 
	m_array_tagBtnSign[1] = TAG_BTN_SIGN_2; 
	m_array_tagBtnSign[2] = TAG_BTN_SIGN_3; 
	m_array_tagBtnSign[3] = TAG_BTN_SIGN_4; 
	m_array_tagBtnSign[4] = TAG_BTN_SIGN_5; 
	m_array_tagBtnSign[5] = TAG_BTN_SIGN_6; 
	m_array_tagBtnSign[6] = TAG_BTN_SIGN_7; 

	m_array_tagSignGift1[0] = TAG_SIGN_GIFT_1_1;
	m_array_tagSignGift1[1] = TAG_SIGN_GIFT_2_1;
	m_array_tagSignGift1[2] = TAG_SIGN_GIFT_3_1;
	m_array_tagSignGift1[3] = TAG_SIGN_GIFT_4_1;
	m_array_tagSignGift1[4] = TAG_SIGN_GIFT_5_1;
	m_array_tagSignGift1[5] = TAG_SIGN_GIFT_6_1;

	m_array_tagSignGift2[0] = TAG_SIGN_GIFT_1_2;
	m_array_tagSignGift2[1] = TAG_SIGN_GIFT_2_2;
	m_array_tagSignGift2[2] = TAG_SIGN_GIFT_3_2;
	m_array_tagSignGift2[3] = TAG_SIGN_GIFT_4_2;
	m_array_tagSignGift2[4] = TAG_SIGN_GIFT_5_2;
	m_array_tagSignGift2[5] = TAG_SIGN_GIFT_6_2;

	m_array_tagSignGfit7All[0] = TAG_SIGN_GIFT_7_1;
	m_array_tagSignGfit7All[1] = TAG_SIGN_GIFT_7_2;
	m_array_tagSignGfit7All[2] = TAG_SIGN_GIFT_7_3;
	m_array_tagSignGfit7All[3] = TAG_SIGN_GIFT_7_4;
}


SignDailyUI::~SignDailyUI(void)
{

}

SignDailyUI* SignDailyUI::create()
{
	SignDailyUI * signDailyUI = new SignDailyUI();
	if (signDailyUI && signDailyUI->init())
	{
		signDailyUI->autorelease();
		return signDailyUI;
	}
	CC_SAFE_DELETE(signDailyUI);
	return NULL;
}

bool SignDailyUI::init()
{
	if (UIScene::init())
	{
		if(LoadSceneLayer::signDailyUIPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::signDailyUIPanel->removeFromParentAndCleanup(false);
		}
		s_pPanel = LoadSceneLayer::signDailyUIPanel;

		m_pUiLayer->addWidget(s_pPanel);

		initUI();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSize(790, 440));

		//this->refreshUI();

		return true;
	}
	return false;
}

void SignDailyUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void SignDailyUI::onExit()
{
	UIScene::onExit();

	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

void SignDailyUI::callBackBtnClolse( CCObject * obj )
{
	this->closeAnim();
}

void SignDailyUI::callBackBtnSign( CCObject* obj )
{
	UIButton * pUIBtn = (UIButton *)obj;
	int nSignGiftIndex = pUIBtn->getTag() - TAG_BTN_SIGN_1;

	// 请求服务器 签到（nSignGiftIndex :第几天的签到）
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5113, (void *)nSignGiftIndex);

}

void SignDailyUI::callBackBtnSignGift( CCObject* obj )
{
	UIButton * pUIImageView = (UIButton *)obj;
	CCSize size = CCDirector::sharedDirector()->getVisibleSize();
			
	int nGift = 0;										// 第几天的奖励（从0开始）
	int nGiftNum = 0;								// 第几个奖励

	for (int i = 0; i < 7; i++)
	{
		if (m_array_tagSignGift1[i] == pUIImageView->getTag())
		{
			nGift = i;
			nGiftNum = 0;

			break;
		}

		if (m_array_tagSignGift2[i] == pUIImageView->getTag())
		{
			nGift = i;
			nGiftNum = 1;

			break;
		}

		if (i < 4 && m_array_tagSignGfit7All[i] == pUIImageView->getTag())
		{
			nGift = 6;
			nGiftNum = i;

			break;
		}
	}

	GoodsInfo * pGoodsInfo = SignDailyData::instance()->getGoosInfo(nGift, nGiftNum);
	GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(pGoodsInfo,GameView::getInstance()->EquipListItem,0);
	goodsItemInfoBase->ignoreAnchorPointForPosition(false);
	goodsItemInfoBase->setAnchorPoint(ccp(0.5f, 0.5f));
	//goodsItemInfoBase->setPosition(ccp(size.width / 2, size.height / 2));

	GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
}

void SignDailyUI::initUI()
{
	UIButton* pBtnClose = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnable(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addReleaseEvent(this, coco_releaseselector(SignDailyUI::callBackBtnClolse));
}

void SignDailyUI::refreshUI()
{
	std::vector<SignDaily *> vector_signDaily = SignDailyData::instance()->m_vector_native_sign;

	// 前6个奖励 初始化方法一致，第7个签到奖励 因为 奖励数量有变（4个），需要单独初始化
	for (unsigned int i = 0; (i + 1) < vector_signDaily.size(); i++)
	{
		char s_num_i[10];
		sprintf(s_num_i, "%d", (i + 1) * 10 + (i + 1));

		char s_num_1[10];
		sprintf(s_num_1, "%d", 1);

		char s_num_2[10];
		sprintf(s_num_2, "%d", 2);

		// m_btn_sign
		std::string str_btn_gift = "";
		str_btn_gift.append("Button_");
		str_btn_gift.append(s_num_i);

		// m_label_btnText
		std::string str_label_btnGift_text = "";
		str_label_btnGift_text.append("Label_");
		str_label_btnGift_text.append(s_num_i);
		str_label_btnGift_text.append("_text");

		// m_image_red
		std::string str_image_red = "";
		str_image_red.append("ImageView_");
		str_image_red.append(s_num_i);
		str_image_red.append("_red");

		// m_image_text
		std::string str_image_text = "";
		str_image_text.append("ImageView_");
		str_image_text.append(s_num_i);
		str_image_text.append("_notime");

		// m_label_timeNeed
		std::string str_label_timeNeed = "";
		str_label_timeNeed.append("Label_");
		str_label_timeNeed.append(s_num_i);
		str_label_timeNeed.append("_notime");

		// m_image_gift1_frame
		std::string str_image_gift1_frame = "";
		str_image_gift1_frame.append("Button_");
		str_image_gift1_frame.append(s_num_i);
		str_image_gift1_frame.append("_gift");
		str_image_gift1_frame.append(s_num_1);

		// m_image_gift1_icon
		std::string str_image_gift1_icon = "";
		str_image_gift1_icon.append("ImageView_");
		str_image_gift1_icon.append(s_num_i);
		str_image_gift1_icon.append("_gift");
		str_image_gift1_icon.append(s_num_1);
		str_image_gift1_icon.append("_icon");

		// m_label_gift1_num
		std::string str_image_gift1_num = "";
		str_image_gift1_num.append("Label_");
		str_image_gift1_num.append(s_num_i);
		str_image_gift1_num.append("_gift");
		str_image_gift1_num.append(s_num_1);
		str_image_gift1_num.append("_num");

		// m_image_gift2_frame
		std::string str_image_gift2_frame = "";
		str_image_gift2_frame.append("Button_");
		str_image_gift2_frame.append(s_num_i);
		str_image_gift2_frame.append("_gift");
		str_image_gift2_frame.append(s_num_2);

		// m_image_gift2_icon
		std::string str_image_gift2_icon = "";
		str_image_gift2_icon.append("ImageView_");
		str_image_gift2_icon.append(s_num_i);
		str_image_gift2_icon.append("_gift");
		str_image_gift2_icon.append(s_num_2);
		str_image_gift2_icon.append("_icon");

		// m_label_gift2_num
		std::string str_image_gift2_num = "";
		str_image_gift2_num.append("Label_");
		str_image_gift2_num.append(s_num_i);
		str_image_gift2_num.append("_gift");
		str_image_gift2_num.append(s_num_2);
		str_image_gift2_num.append("_num");

		//签到btn
		vector_signDaily.at(i)->m_btn_sign = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, str_btn_gift.c_str());
		vector_signDaily.at(i)->m_btn_sign->setTouchEnable(true);
		vector_signDaily.at(i)->m_btn_sign->setPressedActionEnabled(true);
		vector_signDaily.at(i)->m_btn_sign->addReleaseEvent(this, coco_releaseselector(SignDailyUI::callBackBtnSign));
		vector_signDaily.at(i)->m_btn_sign->setTag(m_array_tagBtnSign[i]);
		vector_signDaily.at(i)->m_btn_sign->setVisible(true);

		//签到btn的文本
		vector_signDaily.at(i)->m_label_btnText = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, str_label_btnGift_text.c_str());
		vector_signDaily.at(i)->m_label_btnText->setVisible(true);

		//红色 高亮显示
		vector_signDaily.at(i)->m_image_red = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, str_image_red.c_str());
		vector_signDaily.at(i)->m_image_red->setVisible(false);

		// 连续X天 的底图
		vector_signDaily.at(i)->m_image_text = (UIImageView*)UIHelper::seekWidgetByName(s_pPanel, str_image_text.c_str());
		vector_signDaily.at(i)->m_image_text->setVisible(false);

		// 连续X天 的文本
		vector_signDaily.at(i)->m_label_timeNeed = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, str_label_timeNeed.c_str());
		vector_signDaily.at(i)->m_label_timeNeed->setColor(ccc3(255, 246, 0));
		vector_signDaily.at(i)->m_label_timeNeed->setVisible(false);

		vector_signDaily.at(i)->m_btn_gift1_frame = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, str_image_gift1_frame.c_str());
		vector_signDaily.at(i)->m_image_gift1_icon = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, str_image_gift1_icon.c_str());
		vector_signDaily.at(i)->m_label_gift1_num = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, str_image_gift1_num.c_str());

		vector_signDaily.at(i)->m_btn_gift2_frame = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, str_image_gift2_frame.c_str());
		vector_signDaily.at(i)->m_image_gift2_icon = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, str_image_gift2_icon.c_str());
		vector_signDaily.at(i)->m_label_gift2_num = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, str_image_gift2_num.c_str());

		vector_signDaily.at(i)->m_btn_gift1_frame->setVisible(false);
		vector_signDaily.at(i)->m_image_gift1_icon->setVisible(false);
		vector_signDaily.at(i)->m_label_gift1_num->setVisible(false);

		vector_signDaily.at(i)->m_btn_gift2_frame->setVisible(false);
		vector_signDaily.at(i)->m_image_gift2_icon->setVisible(false);
		vector_signDaily.at(i)->m_label_gift2_num->setVisible(false);

		vector_signDaily.at(i)->m_btn_gift1_frame->setTag(m_array_tagSignGift1[i]);
		vector_signDaily.at(i)->m_btn_gift2_frame->setTag(m_array_tagSignGift2[i]);

		vector_signDaily.at(i)->m_btn_gift1_frame->setTouchEnable(true);
		vector_signDaily.at(i)->m_btn_gift2_frame->setTouchEnable(true);

		vector_signDaily.at(i)->m_btn_gift1_frame->setScale(0.8f);
		vector_signDaily.at(i)->m_btn_gift2_frame->setScale(0.8f);

		vector_signDaily.at(i)->m_btn_gift1_frame->setPressedActionEnabled(true);
		vector_signDaily.at(i)->m_btn_gift2_frame->setPressedActionEnabled(true);

		vector_signDaily.at(i)->m_btn_gift1_frame->addReleaseEvent(this, coco_releaseselector(SignDailyUI::callBackBtnSignGift));
		vector_signDaily.at(i)->m_btn_gift2_frame->addReleaseEvent(this, coco_releaseselector(SignDailyUI::callBackBtnSignGift));

		initBtnStatus(vector_signDaily.at(i));
		initGoodsInfo(vector_signDaily.at(i));
	}

	// 第7天的签到奖励单独初始化
	int nSignSize = vector_signDaily.size();
	if (7 == nSignSize)
	{
		int nIndex = nSignSize - 1;
		vector_signDaily.at(nIndex)->m_btn_sign = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_77");
		vector_signDaily.at(nIndex)->m_btn_sign->setTouchEnable(true);
		vector_signDaily.at(nIndex)->m_btn_sign->setPressedActionEnabled(true);
		vector_signDaily.at(nIndex)->m_btn_sign->addReleaseEvent(this, coco_releaseselector(SignDailyUI::callBackBtnSign));
		vector_signDaily.at(nIndex)->m_btn_sign->setTag(m_array_tagBtnSign[nSignSize - 1]);
		vector_signDaily.at(nIndex)->m_btn_sign->setVisible(true);

		vector_signDaily.at(nIndex)->m_label_btnText = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_77_text");
		vector_signDaily.at(nIndex)->m_label_btnText->setVisible(true);

		vector_signDaily.at(nIndex)->m_image_red = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_77_red");
		vector_signDaily.at(nIndex)->m_image_red->setVisible(false);

		vector_signDaily.at(nIndex)->m_image_text = (UIImageView*)UIHelper::seekWidgetByName(s_pPanel, "ImageView_77_notime");
		vector_signDaily.at(nIndex)->m_image_text->setVisible(false);

		vector_signDaily.at(nIndex)->m_label_timeNeed = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_77_notime");
		vector_signDaily.at(nIndex)->m_label_timeNeed->setColor(ccc3(255, 246, 0));
		vector_signDaily.at(nIndex)->m_label_timeNeed->setVisible(false);

		vector_signDaily.at(nIndex)->m_btn_gift1_frame = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_77_gift1");
		vector_signDaily.at(nIndex)->m_image_gift1_icon = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_77_gift1_icon");
		vector_signDaily.at(nIndex)->m_label_gift1_num = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_77_gift1_num");

		vector_signDaily.at(nIndex)->m_btn_gift2_frame = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_77_gift2");
		vector_signDaily.at(nIndex)->m_image_gift2_icon = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_77_gift2_icon");
		vector_signDaily.at(nIndex)->m_label_gift2_num = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_77_gift2_num");

		vector_signDaily.at(nIndex)->m_btn_gift3_frame = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_77_gift3");
		vector_signDaily.at(nIndex)->m_image_gift3_icon = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_77_gift3_icon");
		vector_signDaily.at(nIndex)->m_label_gift3_num = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_77_gift3_num");

		vector_signDaily.at(nIndex)->m_btn_gift4_frame = (UIButton *)UIHelper::seekWidgetByName(s_pPanel, "Button_77_gift4");
		vector_signDaily.at(nIndex)->m_image_gift4_icon = (UIImageView *)UIHelper::seekWidgetByName(s_pPanel, "ImageView_77_gift4_icon");
		vector_signDaily.at(nIndex)->m_label_gift4_num = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_77_gift4_num");

		vector_signDaily.at(nIndex)->m_btn_gift1_frame->setVisible(false);
		vector_signDaily.at(nIndex)->m_image_gift1_icon->setVisible(false);
		vector_signDaily.at(nIndex)->m_label_gift1_num->setVisible(false);

		vector_signDaily.at(nIndex)->m_btn_gift2_frame->setVisible(false);
		vector_signDaily.at(nIndex)->m_image_gift2_icon->setVisible(false);
		vector_signDaily.at(nIndex)->m_label_gift2_num->setVisible(false);

		vector_signDaily.at(nIndex)->m_btn_gift3_frame->setVisible(false);
		vector_signDaily.at(nIndex)->m_image_gift3_icon->setVisible(false);
		vector_signDaily.at(nIndex)->m_label_gift3_num->setVisible(false);

		vector_signDaily.at(nIndex)->m_btn_gift4_frame->setVisible(false);
		vector_signDaily.at(nIndex)->m_image_gift4_icon->setVisible(false);
		vector_signDaily.at(nIndex)->m_label_gift4_num->setVisible(false);

		vector_signDaily.at(nIndex)->m_btn_gift1_frame->setTag(m_array_tagSignGfit7All[0]);
		vector_signDaily.at(nIndex)->m_btn_gift2_frame->setTag(m_array_tagSignGfit7All[1]);
		vector_signDaily.at(nIndex)->m_btn_gift3_frame->setTag(m_array_tagSignGfit7All[2]);
		vector_signDaily.at(nIndex)->m_btn_gift4_frame->setTag(m_array_tagSignGfit7All[3]);

		vector_signDaily.at(nIndex)->m_btn_gift1_frame->setTouchEnable(true);
		vector_signDaily.at(nIndex)->m_btn_gift2_frame->setTouchEnable(true);
		vector_signDaily.at(nIndex)->m_btn_gift3_frame->setTouchEnable(true);
		vector_signDaily.at(nIndex)->m_btn_gift4_frame->setTouchEnable(true);

		vector_signDaily.at(nIndex)->m_btn_gift1_frame->setScale(0.8f);
		vector_signDaily.at(nIndex)->m_btn_gift2_frame->setScale(0.8f);
		vector_signDaily.at(nIndex)->m_btn_gift3_frame->setScale(0.8f);
		vector_signDaily.at(nIndex)->m_btn_gift4_frame->setScale(0.8f);

		vector_signDaily.at(nIndex)->m_btn_gift1_frame->setPressedActionEnabled(true);
		vector_signDaily.at(nIndex)->m_btn_gift2_frame->setPressedActionEnabled(true);
		vector_signDaily.at(nIndex)->m_btn_gift3_frame->setPressedActionEnabled(true);
		vector_signDaily.at(nIndex)->m_btn_gift4_frame->setPressedActionEnabled(true);

		vector_signDaily.at(nIndex)->m_btn_gift1_frame->addReleaseEvent(this, coco_releaseselector(SignDailyUI::callBackBtnSignGift));
		vector_signDaily.at(nIndex)->m_btn_gift2_frame->addReleaseEvent(this, coco_releaseselector(SignDailyUI::callBackBtnSignGift));
		vector_signDaily.at(nIndex)->m_btn_gift3_frame->addReleaseEvent(this, coco_releaseselector(SignDailyUI::callBackBtnSignGift));
		vector_signDaily.at(nIndex)->m_btn_gift4_frame->addReleaseEvent(this, coco_releaseselector(SignDailyUI::callBackBtnSignGift));

		// 签到第七天奖励的粒子环绕效果
		initParticleForGoods();

		initBtnStatus(vector_signDaily.at(nIndex));
		initGoodsInfo(vector_signDaily.at(nIndex));
	}
}

void SignDailyUI::initBtnStatus( SignDaily * signDaily )
{
	int nGiftIndex = signDaily->m_nGiftIndex;
	int nGiftStatus = signDaily->m_nStatus;

	/** 奖励物品状态 ：1.不可领取奖励，2.可领取奖励，3.已领取奖励完毕*/
	if (1 == nGiftStatus)
	{
		// 不可领取：灰色；显示“连续X天”；btn不可用
		signDaily->m_btn_sign->setTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");
		signDaily->m_btn_sign->setTouchEnable(false);
		signDaily->m_btn_sign->setVisible(false);

		const char *str_lianxu = StringDataManager::getString("label_lianxu");
		const char *str_tian = StringDataManager::getString("label_tian");

		char str_num[10];
		sprintf(str_num, "%d", nGiftIndex + 1);

		std::string str_text = "";
		str_text.append(str_lianxu);
		str_text.append(str_num);
		str_text.append(str_tian);

		signDaily->m_label_btnText->setText("");
		signDaily->m_label_btnText->setVisible(false);

		signDaily->m_image_text->setVisible(true);
		signDaily->m_label_timeNeed->setText(str_text.c_str());
		signDaily->m_label_timeNeed->setVisible(true);
	}
	else if (2 == nGiftStatus)
	{
		// 可领取：正常色；显示“签到”；btn可用；高亮显示
		signDaily->m_image_red->setVisible(true);

		signDaily->m_btn_sign->setTextures("res_ui/new_button_2.png", "res_ui/new_button_2.png", "");
		signDaily->m_btn_sign->setTouchEnable(true);

		const char *str_lingqu = StringDataManager::getString("btn_qiandao");
		signDaily->m_label_btnText->setText(str_lingqu);
		signDaily->m_label_btnText->setVisible(true);

		signDaily->m_image_text->setVisible(false);
		signDaily->m_label_timeNeed->setText("");
		signDaily->m_label_timeNeed->setVisible(false);

		// 奖励可领取的话，加上 粒子特效
		CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL == tutorialParticle)
		{
			UIButton * btn_sign = signDaily->m_btn_sign;
			m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", btn_sign->getContentSize().width + 25, btn_sign->getContentSize().height + 5);
			m_tutorialParticle->setPosition(ccp(btn_sign->getPosition().x, btn_sign->getPosition().y - 22));
			m_tutorialParticle->setAnchorPoint(ccp(0.5f, 0.5f));
			m_tutorialParticle->setTag(TAG_TUTORIALPARTICLE);

			this->addChild(m_tutorialParticle);
		}
	}
	else if (3 ==  nGiftStatus)
	{
		// 已领取：灰色；显示“已领取”；btn不可用
		signDaily->m_btn_sign->setTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");
		signDaily->m_btn_sign->setTouchEnable(false);
		signDaily->m_btn_sign->setVisible(true);

		const char *str_yilingqu = StringDataManager::getString("btn_yilingqu");
		signDaily->m_label_btnText->setText(str_yilingqu);
		signDaily->m_label_btnText->setVisible(true);

		signDaily->m_image_text->setVisible(false);
		signDaily->m_label_timeNeed->setText("");
		signDaily->m_label_timeNeed->setVisible(false);
	}
}

void SignDailyUI::initGoodsInfo( SignDaily* signDaily )
{
	// 设置 goods icon 和 frame
	std::vector<GoodsInfo *> vector_goods = signDaily->m_vector_goods;
	for (unsigned int i = 0; i < vector_goods.size(); i++)
	{
		std::string goods_icon = vector_goods.at(i)->icon();

		std::string goodsIcon_ = "res_ui/props_icon/";
		goodsIcon_.append(goods_icon);
		goodsIcon_.append(".png");

		// 根据goods品阶选择底图
		int quality_ = vector_goods.at(i)->quality();
		std::string qualityStr_ = getEquipmentQualityByIndex(quality_);

		if (0 == i)
		{
			signDaily->m_image_gift1_icon->setTexture(goodsIcon_.c_str());
			signDaily->m_image_gift1_icon->setVisible(true);
			signDaily->m_btn_gift1_frame->loadTextures(qualityStr_.c_str(), qualityStr_.c_str(), qualityStr_.c_str());
			signDaily->m_btn_gift1_frame->setVisible(true);
		}
		else if (1 == i)
		{
			signDaily->m_image_gift2_icon->setTexture(goodsIcon_.c_str());
			signDaily->m_image_gift2_icon->setVisible(true);
			signDaily->m_btn_gift2_frame->loadTextures(qualityStr_.c_str(), qualityStr_.c_str(), qualityStr_.c_str());
			signDaily->m_btn_gift2_frame->setVisible(true);
		}
		else if (2 == i)
		{
			signDaily->m_image_gift3_icon->setTexture(goodsIcon_.c_str());
			signDaily->m_image_gift3_icon->setVisible(true);
			signDaily->m_btn_gift3_frame->loadTextures(qualityStr_.c_str(), qualityStr_.c_str(), qualityStr_.c_str());
			signDaily->m_btn_gift3_frame->setVisible(true);
		}
		else if (3 == i)
		{
			signDaily->m_image_gift4_icon->setTexture(goodsIcon_.c_str());
			signDaily->m_image_gift4_icon->setVisible(true);
			signDaily->m_btn_gift4_frame->loadTextures(qualityStr_.c_str(), qualityStr_.c_str(), qualityStr_.c_str());
			signDaily->m_btn_gift4_frame->setVisible(true);
		}
	}

	// 设置 goods 数量
	std::vector<int > vector_goodsNum = signDaily->m_vector_goodsNum;
	for (unsigned int j = 0; j < vector_goodsNum.size(); j++)
	{
		int goods_num = vector_goodsNum.at(j);

		char sText[10];
		sprintf(sText, "%d", goods_num);

		if (0 == j)
		{
			signDaily->m_label_gift1_num->setText(sText);
			signDaily->m_label_gift1_num->setVisible(true);
		}
		else if (1 == j)
		{
			signDaily->m_label_gift2_num->setText(sText);
			signDaily->m_label_gift2_num->setVisible(true);
		}
		else if (2 == j)
		{
			signDaily->m_label_gift3_num->setText(sText);
			signDaily->m_label_gift3_num->setVisible(true);
		}
		else if (3 == j)
		{
			signDaily->m_label_gift4_num->setText(sText);
			signDaily->m_label_gift4_num->setVisible(true);
		}
	}
}

std::string SignDailyUI::getEquipmentQualityByIndex( int quality )
{
	std::string frameColorPath = "res_ui/";
	switch(quality)
	{
	case 1:
		{
			frameColorPath.append("sdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("sdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("sdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("sdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("sdi_orange");
		}break;
	}
	frameColorPath.append(".png");
	return frameColorPath;
}

void SignDailyUI::getGiftSuc( const int nGiftNum, const int nGiftStatus )
{
	std::vector<COneSignEverydayGift *> vector_internet	 = SignDailyData::instance()->m_vector_internt_sign;
	for (unsigned int i = 0; i < vector_internet.size(); i++)
	{
		// 前面的 奖励 全部 时间设为 0，status 设为 已领取（3）
		if (vector_internet.at(i)->number() < vector_internet.at(nGiftNum)->number())
		{
			vector_internet.at(i)->set_status(3);
		}
		else if (vector_internet.at(i)->number() == vector_internet.at(nGiftNum)->number())
		{
			vector_internet.at(i)->set_status(3);

			// 去除 环绕按钮的粒子特效效果
			CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
			if (NULL != tutorialParticle)
			{
				this->removeChild(m_tutorialParticle);
			}
		}
	}

	SignDailyData::instance()->initDataFromIntent();

	// 把已领取的数据 同步给 icon(通过 native数据同步)
	SignDailyIcon * pSignDailyIcon = (SignDailyIcon *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagSignDailyIcon);
	if (NULL != pSignDailyIcon)
	{
		pSignDailyIcon->setIsCanSign(false);
		pSignDailyIcon->initDataFromIntent();
	}

	// 更新window UI
	refreshUI();
}

int SignDailyUI::getGiftNum()
{
	std::vector<COneSignEverydayGift *> vector_internet= SignDailyData::instance()->m_vector_internt_sign;
	for (unsigned int i=0; i < vector_internet.size(); i++)
	{

		int nGiftNum =  vector_internet.at(i)->number();
		int nGiftStatus = vector_internet.at(i)->status();	

		// 如果当前状态 为 已领取，则查询下一个奖励；否则的话 当前奖励 为 即将领取的奖励
		if (3 == nGiftStatus)
		{
			continue;
		}
		else
		{
			m_nGiftNum = nGiftNum;
			return nGiftNum;
		}
	}

	return 0;
}

bool SignDailyUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void SignDailyUI::initParticleForGoods()
{
	std::vector<SignDaily *> vector_signDaily = SignDailyData::instance()->m_vector_native_sign;

	// 第7天的签到奖励
	int nSignSize = vector_signDaily.size();
	if (7 == nSignSize)
	{
		int nIndex = nSignSize - 1;

		float width = vector_signDaily.at(nIndex)->m_btn_gift1_frame->getContentSize().width * 0.8f;
		float height = vector_signDaily.at(nIndex)->m_btn_gift1_frame->getContentSize().height * 0.8f;

		float xPos = vector_signDaily.at(nIndex)->m_btn_gift1_frame->getPosition().x - width / 2.0f;
		float yPos = vector_signDaily.at(nIndex)->m_btn_gift1_frame->getPosition().y - height / 2.0f;

		// 根据Cocostudio 读取的json文件，第七天的奖励共4个
		int nGiftSize = 4;
		for (int i = 0; i < nGiftSize; i ++)
		{
			float speed = 100.f;

			float nOff = 73.0f;

			std::string _path = "animation/texiao/particledesigner/";
			_path.append("tuowei0.plist");

			CCParticleSystemQuad* particleEffect_1 = CCParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
			particleEffect_1->setPosition(ccp(xPos + i * 83, yPos));
			//particleEffect_1->setStartSize(30);
			particleEffect_1->setPositionType(kCCPositionTypeFree);
			particleEffect_1->setVisible(true);
			particleEffect_1->setScale(1.0f);
			//particleEffect_1->setLife(1.5f);
			//particleEffect_1->setStartColor(color);
			//particleEffect_1->setEndColor(color);
			this->addChild(particleEffect_1);

			CCSequence * sequence_1 = CCSequence::create(
				CCMoveTo::create(height*1.0f/speed,ccp(xPos + i * nOff, yPos + height)),
				CCMoveTo::create(width*1.0f/speed,ccp(xPos + i * nOff + width, yPos + height)),
				CCMoveTo::create(height*1.0f/speed,ccp(xPos + i * nOff + width, yPos)),
				CCMoveTo::create(width*1.0f/speed,ccp(xPos + i * nOff, yPos)),
				NULL
				);
			CCRepeatForever * repeatForeverAnm_1 = CCRepeatForever::create(sequence_1);
			particleEffect_1->runAction(repeatForeverAnm_1);


			CCParticleSystemQuad* particleEffect_2 = CCParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
			particleEffect_2->setPosition(ccp(xPos + i * nOff + 2 + width,yPos + 2 + height));
			//particleEffect_2->setStartSize(30);
			particleEffect_2->setPositionType(kCCPositionTypeFree);
			particleEffect_2->setVisible(true);
			particleEffect_2->setScale(1.0f);
			//particleEffect_2->setLife(1.5f);
			//particleEffect_2->setStartColor(color);
			//particleEffect_2->setEndColor(color);
			this->addChild(particleEffect_2);

			CCSequence * sequence_2 = CCSequence::create(
				CCMoveTo::create(height*1.0f/speed,ccp(xPos + i * nOff + width, yPos)),
				CCMoveTo::create(width*1.0f/speed,ccp(xPos + i * nOff, yPos)),
				CCMoveTo::create(height*1.0f/speed,ccp(xPos + i * nOff, yPos  + height)),
				CCMoveTo::create(width*1.0f/speed,ccp(xPos + i * nOff + width, yPos  + height)),
				NULL
				);
			CCRepeatForever * repeatForeverAnm_2 = CCRepeatForever::create(sequence_2);
			particleEffect_2->runAction(repeatForeverAnm_2);
		}
	}
}

