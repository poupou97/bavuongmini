#ifndef _UI_SIGNDAILY_SIGNDAILYUI_H_
#define _UI_SIGNDAILY_SIGNDAILYUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 签到UI
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.18
 */

class SignDaily;
class CCTutorialParticle;

class SignDailyUI:public UIScene
{
public:
	SignDailyUI(void);
	~SignDailyUI(void);

public:
	static UIPanel * s_pPanel;
	static SignDailyUI* create();
	bool init();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);

	virtual void onEnter();
	virtual void onExit();

public:
	void callBackBtnClolse(CCObject * obj);																	// 关闭按钮的响应
	void callBackBtnSign(CCObject* obj);																		// 签到按钮的响应
	void callBackBtnSignGift(CCObject* obj);																	// 点击奖励时的响应

public:
	void refreshUI();																										// 用 服务器数据 更新 UI
	void getGiftSuc(const int nGiftNum, const int nGiftStatus);										// 获取奖励成功

private:
	void initUI();
	void initBtnStatus(SignDaily * signDaily);																	// 初始化Btn的状态
	void initGoodsInfo(SignDaily* signDaily);																	// 初始化goods的信息
	int getGiftNum();																							// 用服务器 中的 数据 初始化 当前正在进行奖励 的编号

	int m_nGiftNum;																								// 当前正在进行的 GiftNum;

	CCTutorialParticle* m_tutorialParticle;																		// 环绕可领取按钮的粒子特效

private:
	std::string getEquipmentQualityByIndex(int quality);													// 根据 品阶 获得该goods需要的底图
	void initParticleForGoods();

private:
	enum TagBtnSign{
		TAG_BTN_SIGN_1 = 101,
		TAG_BTN_SIGN_2 = 102,
		TAG_BTN_SIGN_3 = 103,
		TAG_BTN_SIGN_4 = 104,
		TAG_BTN_SIGN_5 = 105,
		TAG_BTN_SIGN_6 = 106,
		TAG_BTN_SIGN_7 = 107,
	};

	enum TagSignGift1{
		TAG_SIGN_GIFT_1_1 = 1011,
		TAG_SIGN_GIFT_2_1 = 1021,
		TAG_SIGN_GIFT_3_1 = 1031,
		TAG_SIGN_GIFT_4_1 = 1041,
		TAG_SIGN_GIFT_5_1 = 1051,
		TAG_SIGN_GIFT_6_1 = 1061,
	};

	enum TagSignGift2{
		TAG_SIGN_GIFT_1_2 = 1012,
		TAG_SIGN_GIFT_2_2 = 1022,
		TAG_SIGN_GIFT_3_2 = 1032,
		TAG_SIGN_GIFT_4_2 = 1042,
		TAG_SIGN_GIFT_5_2 = 1052,
		TAG_SIGN_GIFT_6_2 = 1062,
	};

	enum TagSignGift7All{
		TAG_SIGN_GIFT_7_1 = 1071,
		TAG_SIGN_GIFT_7_2 = 1072,
		TAG_SIGN_GIFT_7_3 = 1073,
		TAG_SIGN_GIFT_7_4 = 1074,
	};

	TagBtnSign m_array_tagBtnSign[7];									// tag数组
	TagSignGift1 m_array_tagSignGift1[6];
	TagSignGift2 m_array_tagSignGift2[6];
	TagSignGift7All m_array_tagSignGfit7All[4];
};

#endif

