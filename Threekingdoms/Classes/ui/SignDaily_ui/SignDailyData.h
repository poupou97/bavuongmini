#ifndef _UI_SIGNDAILY_SIGNDAILYDATA_H_
#define _UI_SIGNDAILY_SIGNDAILYDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 签到模块（用于保存服务器数据）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.18
 */

class COneSignEverydayGift;
class SignDaily;
class GoodsInfo;

class SignDailyData
{
public:
	static SignDailyData * s_signDailyData;
	static SignDailyData * instance();

private:
	SignDailyData(void);
	~SignDailyData(void);

public:
	void initDataFromIntent();																									// 从 m_vector_internt 更新到 m_vector_native_sign
	GoodsInfo* getGoosInfo(unsigned int nGift, int nGiftNum);												// nGift编号，nGiftNum 第几个奖励（从0开始）

	void setIsLogin(int nIsLogin);																							// 设置是否为 第一次登陆
	int getIsLogin();
	bool isCanSign();																												// 是否可以签到

	void setLoginPlayerLevel(int nPlayerLevel);
	int getLoginPalyerLevel();


public:
	std::vector<COneSignEverydayGift *> m_vector_internt_sign;											// 保存 服务器 中总数据的 vector
	std::vector<SignDaily *> m_vector_native_sign;																// 保存 本地 的 总数据的 vector
	
private:
	int m_nIsLogin;																													// 是登陆还是24点推送 0推送，1登陆
	int m_nLoginPlayerLevel;																									// 登陆时角色的等级
	

};

#endif

