#ifndef _UI_SIGNDAILY_SIGNDAILYICON_H_
#define _UI_SIGNDAILY_SIGNDAILYICON_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 签到Icon
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.18
 */


class CCTutorialParticle;

class SignDailyIcon:public UIScene
{
public:
	SignDailyIcon(void);
	~SignDailyIcon(void);

public:
	static SignDailyIcon* create();
	bool init();
	void update(float dt);

	virtual void onEnter();
	virtual void onExit();

public:
	UIButton * m_btn_signDailyIcon;					// 可点击的ICON
	UILayer * m_layer_signDaily;							// 需要用到的layer
	UILabel * m_label_signDailyIcon_text;			// 	ICON下方显示的文字	
	CCScale9Sprite * m_spirte_fontBg;				// 文字显示的底图

public:
	void callBackBtnSign(CCObject *obj);

public:
	void initDataFromIntent();								// 初始化数据从intent
	void setIsCanSign(bool bIsCanSign);			// 设置 是否可签到
	void refreshIcon();										// 更新ICON ui

private:
	 CCTutorialParticle * m_tutorialParticle;			// 在线奖励特效
	 bool m_bIsCanSign;										// 是否可以签到
	

};

#endif

