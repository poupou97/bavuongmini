#ifndef _UI_UISCENETEST_H_
#define _UI_UISCENETEST_H_

#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UISceneTest : public UIScene
{
public:
	UISceneTest();
	virtual ~UISceneTest();

	static UISceneTest * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

private:

};

#endif