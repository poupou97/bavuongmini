#include "QuiryUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CEquipment.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../backpackscene/EquipmentItemInfos.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../../gamescene_state/role/GameActorAnimation.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/CActiveRole.h"
#include "AppMacros.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "CCRichLabel.h"
#include "../backpackscene/PackageScene.h"
#include "../generals_ui/RecuriteActionItem.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "RecruitGeneralCard.h"

QuiryUI::QuiryUI(void)
{
	curRoleIndex = 0;
}


QuiryUI::~QuiryUI(void)
{
	delete roleValue_;
	std::vector<CEquipment*>::iterator iterEquip;
	for (iterEquip = playerEquipments.begin(); iterEquip != playerEquipments.end(); ++iterEquip)
	{
		delete *iterEquip;
	}
	playerEquipments.clear();

	std::vector<CGeneralDetail*>::iterator iterDetail;
	for (iterDetail = generalDetails.begin(); iterDetail != generalDetails.end(); ++iterDetail)
	{
		delete *iterDetail;
	}
	generalDetails.clear();
}

QuiryUI * QuiryUI::create(CActiveRole * activerole,std::vector<CEquipment *>euipmentsVector,std::vector<CGeneralDetail *>generalDetailVector)
{
	QuiryUI * quiry= new QuiryUI();
	if (quiry && quiry->init(activerole,euipmentsVector,generalDetailVector))
	{
		quiry->autorelease();
		return quiry;
	}
	CC_SAFE_DELETE(quiry);
	return NULL;
}

bool QuiryUI::init(CActiveRole * activerole,std::vector<CEquipment *>euipmentsVector,std::vector<CGeneralDetail *>generalDetailVector)
{
	if (UIScene::init())
	{
		winsize= CCDirector::sharedDirector()->getVisibleSize();
		if(LoadSceneLayer::quiryPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::quiryPanel->removeFromParentAndCleanup(false);
		}

		m_Layer= UILayer::create();
		m_Layer->ignoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(ccp(0.5f,0.5f));
		m_Layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_Layer->setContentSize(CCSizeMake(800,480));
		this->addChild(m_Layer);

		UIPanel *quiryPanel_ = (UIPanel*) GUIReader::shareReader()->widgetFromJsonFile("res_ui/zhuangbeitanchukuang_1.json");
		quiryPanel_->setAnchorPoint(ccp(0.5f,0.5f));
		quiryPanel_->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(quiryPanel_);
		
		imagePression = (UIImageView *)UIHelper::seekWidgetByName(quiryPanel_,"image_jobvalue");
		labelLevel = (UILabel *)UIHelper::seekWidgetByName(quiryPanel_,"Label_roleLevel");
		labelFightValue_ = (UILabelBMFont *)UIHelper::seekWidgetByName(quiryPanel_,"LabelBMFont_fp_value");
		/*
		CCParticleSystem* particleFire = CCParticleSystemQuad::create("animation/texiao/particledesigner/huo2.plist");
		particleFire->setPositionType(kCCPositionTypeFree);
		particleFire->setPosition(ccp(400,181));
		particleFire->setAnchorPoint(ccp(0.5f,0.5f));
		particleFire->setVisible(true);
		//particleFire->setLife(0.5f);
		particleFire->setLifeVar(0);
		particleFire->setScale(0.5f);
		particleFire->setScaleX(0.6f);
		m_Layer->addChild(particleFire);

		CCParticleSystem* particleFire1 = CCParticleSystemQuad::create("animation/texiao/particledesigner/huo2.plist");
		particleFire1->setPositionType(kCCPositionTypeFree);
		particleFire1->setPosition(ccp(405,184));
		particleFire1->setAnchorPoint(ccp(0.5f,0.5f));
		particleFire1->setVisible(true);
		//particleFire1->setLife(0.5f);
		particleFire1->setLifeVar(0);
		particleFire1->setScale(0.5f);
		particleFire1->setScaleX(0.6f);
		m_Layer->addChild(particleFire1);
		*/
		// 
		ImageView_arms = (UIImageView *)UIHelper::seekWidgetByName(quiryPanel_,"ImageView_arms");
		ImageView_clothes = (UIImageView *)UIHelper::seekWidgetByName(quiryPanel_,"ImageView_clothes");
		ImageView_ring = (UIImageView *)UIHelper::seekWidgetByName(quiryPanel_,"ImageView_ring");
		ImageView_helmet = (UIImageView *)UIHelper::seekWidgetByName(quiryPanel_,"ImageView_helmet");
		ImageView_shoes = (UIImageView *)UIHelper::seekWidgetByName(quiryPanel_,"ImageView_shoes");
		ImageView_accessories = (UIImageView *)UIHelper::seekWidgetByName(quiryPanel_,"ImageView_accessories");
		this->setEquipImageVisible(true);

		UILayer * u_tempLayer = UILayer::create();
		m_Layer->addChild(u_tempLayer);

		/*UILabelBMFont * l_allFPName = UILabelBMFont::create();
		l_allFPName->setText(StringDataManager::getString("rank_capacity"));
		l_allFPName->setFntFile("res_ui/font/ziti_1.fnt");
		l_allFPName->setAnchorPoint(ccp(0.5f,0.5f));
		l_allFPName->setPosition(ccp(185,167));
		l_allFPName->setScale(0.6f);
		u_tempLayer->addWidget(l_allFPName);*/
		UIImageView * Image_allFPName = UIImageView::create();
		Image_allFPName->setTexture("res_ui/renwubeibao/zi2.png");
		Image_allFPName->setAnchorPoint(ccp(0.5f,0.5f));
		Image_allFPName->setPosition(ccp(355,187));//(177,168)
		Image_allFPName->setScale(1.0f);
		u_tempLayer->addWidget(Image_allFPName);

		labelAllFightValue_ = UILabelBMFont::create();
		labelAllFightValue_->setText("123456789");
		labelAllFightValue_->setFntFile("res_ui/font/ziti_1.fnt");
		labelAllFightValue_->setAnchorPoint(ccp(0.5f,0.5f));
		labelAllFightValue_->setPosition(ccp(437,186));
		labelAllFightValue_->setScale(0.6f);
		u_tempLayer->addWidget(labelAllFightValue_);

		roleValue_ = new CActiveRole();
		roleValue_->CopyFrom(*activerole);

		for (int i=0;i<euipmentsVector.size();i++)
		{
			CEquipment * equip =new CEquipment();
			equip->CopyFrom(*euipmentsVector.at(i));
			playerEquipments.push_back(equip);
		}

		for (int i=0;i<generalDetailVector.size();i++)
		{
			CGeneralDetail * detail_ =new CGeneralDetail();
			detail_->CopyFrom(*generalDetailVector.at(i));
			generalDetails.push_back(detail_);
		}

		CCTableView * generalList = CCTableView::create(this, CCSizeMake(307, 109));//280
		generalList->setSelectedEnable(true);
		generalList->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 26, 1, 1), ccp(0,0));
		generalList->setDirection(kCCScrollViewDirectionHorizontal);
		generalList->setAnchorPoint(ccp(0,0));
		generalList->setPosition(ccp(246,55));
		generalList->setDelegate(this);
		generalList->setVerticalFillOrder(kCCTableViewFillTopDown);
		generalList->setPressedActionEnabled(true);
		m_Layer->addChild(generalList);
		generalList->reloadData();

		//
		UIImageView * image_left_flag = (UIImageView *)UIHelper::seekWidgetByName(quiryPanel_,"ImageView_arrow_left");
		image_left_flag->setVisible(false);
		UIImageView * image_right_flag = (UIImageView *)UIHelper::seekWidgetByName(quiryPanel_,"ImageView_arrow_right");
		image_right_flag->setVisible(false);
		//add flag
		UILayer * tempLayer_flag = UILayer::create();
// 		tempLayer_flag->ignoreAnchorPointForPosition(false);
// 		tempLayer_flag->setAnchorPoint(ccp(0.5f,0.5f));
// 		tempLayer_flag->setContentSize(CCSizeMake(800, 480));
// 		tempLayer_flag->setPosition(ccp(winsize.width/2,winsize.height/2));
		tempLayer_flag->setZOrder(5);
		m_Layer->addChild(tempLayer_flag);

		UIImageView * imageView_leftFlag = UIImageView::create();
		imageView_leftFlag->setTexture("res_ui/shousuo.png");
		imageView_leftFlag->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_leftFlag->setPosition(ccp(256,110));//231 110
		imageView_leftFlag->setRotation(-135);
		imageView_leftFlag->setScale(0.5f);
		tempLayer_flag->addWidget(imageView_leftFlag);
		UIImageView * imageView_rightFlag = UIImageView::create();
		imageView_rightFlag->setTexture("res_ui/shousuo.png");
		imageView_rightFlag->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_rightFlag->setPosition(ccp(544,110));
		imageView_rightFlag->setRotation(45);
		imageView_rightFlag->setScale(0.5f);
		tempLayer_flag->addWidget(imageView_rightFlag);


		this->refreshCurRoleValue();

		UIButton * buttonExit =(UIButton *)UIHelper::seekWidgetByName(quiryPanel_,"Button_close");
		buttonExit->setTouchEnable(true);
		buttonExit->setPressedActionEnabled(true);
		buttonExit->addReleaseEvent(this,coco_releaseselector(QuiryUI::callBackExit));
	
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setPosition(CCPointZero);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void QuiryUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void QuiryUI::onExit()
{
	UIScene::onExit();
}

void QuiryUI::callBackExit( CCObject * obj )
{
	this->closeAnim();
}

void QuiryUI::callBackShowEquipInfo( CCObject *obj )
{
	UIButton * btn=dynamic_cast<UIButton *>(obj);
	int num =btn->getTag();
	int amount_ = 0;

	GoodsInfo * goods_ =new GoodsInfo();
	goods_->CopyFrom(playerEquipments.at(num)->goods());

	for (int i =0;i<playerEquipments.size();i++)
	{
		if (playerEquipments.at(i)->goods().equipmentdetail().suitid() == goods_->equipmentdetail().suitid())
		{
			amount_++;
		}
	}

	this->setRoleEquipAmount(amount_);
	GoodsItemInfoBase * equipmentItemInfos = GoodsItemInfoBase::create(goods_,GameView::getInstance()->EquipListItem,0);
	equipmentItemInfos->ignoreAnchorPointForPosition(false);
	equipmentItemInfos->setAnchorPoint(ccp(0.5f,0.5f));
	//equipmentItemInfos->setPosition(ccp(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(equipmentItemInfos);

	delete goods_;
}

void QuiryUI::callBackShowGeneralEquipInfo( CCObject *obj )
{
	UIButton * btn=dynamic_cast<UIButton *>(obj);
	int num =btn->getTag();

	int amount_ = 0;

	GoodsInfo * goods_ =new GoodsInfo();
	goods_->CopyFrom(generalDetails.at(curRoleIndex)->equipments(num).goods());
	for (int i=0;i<generalDetails.at(curRoleIndex)->equipments_size();i++)
	{
		if (generalDetails.at(curRoleIndex)->equipments(i).goods().equipmentdetail().suitid() == goods_->equipmentdetail().suitid())
		{
			amount_++;
		}
	}

	this->setRoleEquipAmount(amount_);

	GoodsItemInfoBase * equipmentItemInfos = GoodsItemInfoBase::create(goods_,GameView::getInstance()->EquipListItem,0);
	equipmentItemInfos->ignoreAnchorPointForPosition(false);
	equipmentItemInfos->setAnchorPoint(ccp(0.5f,0.5f));
	//equipmentItemInfos->setPosition(ccp(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(equipmentItemInfos);

	delete goods_;
}


void QuiryUI::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	int cellId = cell->getIdx();
	UILayer * roleLayer_ = (UILayer *)this->getChildByTag(ROLELAYEROFEQUIP);
	if (roleLayer_ != NULL)
	{
		roleLayer_->removeFromParentAndCleanup(true);
	}
	UILayer * generalLayer_ = (UILayer *)this->getChildByTag(GENERALLAYEROFEQUIP);
	if (generalLayer_ != NULL)
	{
		generalLayer_->removeFromParentAndCleanup(true);
	}
	if (cellId == 0)
	{
		curRoleIndex = 0;
		this->refreshCurRoleValue();
	}else
	{
		curRoleIndex = cellId - 1;
		this->refreshCurGeneralValue(cellId - 1);
	}
}

cocos2d::CCSize QuiryUI::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(81,103);
}

cocos2d::extension::CCTableViewCell* QuiryUI::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	 CCTableViewCell *cell = table->dequeueCell();
	 cell = new CCTableViewCell();
	 cell->autorelease();
	 if (idx == 0)
	 {
		 //selectActorId = 0;
		 CCSprite *  smaillFrame = CCSprite::create("res_ui/generals_white.png");
		 smaillFrame->setAnchorPoint(ccp(0, 0));
		 smaillFrame->setPosition(ccp(0, 0));
		 smaillFrame->setScaleX(0.75f);
		 smaillFrame->setScaleY(0.7f);
		 cell->addChild(smaillFrame);
		 //列表中的头像图标
		 std::string pressionStr_ =roleValue_->profession();
		 int roleIconIndex_ =  BasePlayer::getProfessionIdxByName(pressionStr_.c_str());
		 std::string roleIcon_ = BasePlayer::getBigHeadPathByProfession(roleIconIndex_);
		 
		 CCSprite * sprite_icon = CCSprite::create(roleIcon_.c_str());
		 sprite_icon->setAnchorPoint(ccp(0.5f, 0.5f));
		 sprite_icon->setPosition(ccp(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), smaillFrame->getContentSize().height/2*smaillFrame->getScaleY()));
		 sprite_icon->setScale(0.75f);
		 cell->addChild(sprite_icon);
		 //
// 		 CCScale9Sprite * sprite_lvFrame = CCScale9Sprite::create("res_ui/zhezhao80.png");
// 		 sprite_lvFrame->setAnchorPoint(ccp(1.0f, 0));
// 		 sprite_lvFrame->setContentSize(CCSizeMake(34,13));
// 		 sprite_lvFrame->setPosition(ccp(smaillFrame->getContentSize().width*smaillFrame->getScaleX()-5, 19));
// 		 cell->addChild(sprite_lvFrame);
// 		 //等级
// 		 std::string _lv = "LV";
// 		 char roleLevel[5];
// 		 sprintf(roleLevel,"%d",roleValue_->level());
// 		 _lv.append(roleLevel);
// 		 CCLabelTTF * label_lv = CCLabelTTF::create(_lv.c_str(),APP_FONT_NAME,13);
// 		 label_lv->setAnchorPoint(ccp(1.0f, 0));
// 		 label_lv->setPosition(ccp(smaillFrame->getContentSize().width*smaillFrame->getScaleX()-5, 17));
// 		 label_lv->enableStroke(ccc3(0, 0, 0), 2.0f);
// 		 cell->addChild(label_lv);

		 CCSprite * sprite_nameFrame = CCSprite::create("res_ui/name_di2.png");
		 sprite_nameFrame->setAnchorPoint(ccp(0.5f, 0));
		 sprite_nameFrame->setPosition(ccp(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 6));
		 sprite_nameFrame->setScaleX(1.1f);
		 cell->addChild(sprite_nameFrame);
		 //武将名字
		 const char * generalRoleName_ = StringDataManager::getString("generalList_RoleName");
		 CCLabelTTF * label_name = CCLabelTTF::create(generalRoleName_,APP_FONT_NAME,14);
		 label_name->setAnchorPoint(ccp(0.5f, 0));
		 label_name->setPosition(ccp(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 5));
		 label_name->setColor(ccc3(0,255,0));
		 cell->addChild(label_name);
		 //profession
		 std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(roleIconIndex_);
		 CCSprite * sprite_profession = CCSprite::create(generalProfess_.c_str());
		 sprite_profession->setAnchorPoint(ccp(0.5f,0.5f));
		 sprite_profession->setPosition(ccp(62 ,
			 32));
		 sprite_profession->setScale(0.6f);
		 cell->addChild(sprite_profession);
	 }
	 else
	 {
 		 int curGeneralQuality = generalDetails.at(idx-1)->currentquality();
 		 CCSprite * sprite_frame = CCSprite::create(GeneralsUI::getBigHeadFramePath(curGeneralQuality).c_str());
 		 sprite_frame->setAnchorPoint(ccp(0, 0));
 		 sprite_frame->setPosition(ccp(0, 0));
 		 sprite_frame->setScaleX(0.75f);
 		 sprite_frame->setScaleY(0.7f);
 		 cell->addChild(sprite_frame);
 		
 		 CGeneralBaseMsg * generalBaseMsgFromDB  = GeneralsConfigData::s_generalsBaseMsgData[generalDetails.at(idx-1)->modelid()];
 		 std::string curGeneralFace_ = generalBaseMsgFromDB->get_half_photo();
 		 //列表中的头像图标
 		 std::string sprite_icon_path = "res_ui/generals/";
 		 sprite_icon_path.append(curGeneralFace_);
 		 sprite_icon_path.append(".anm");
		 CCLegendAnimation *la_head = CCLegendAnimation::create(sprite_icon_path);
		 if (la_head)
		 {
			 la_head->setPlayLoop(true);
			 la_head->setReleaseWhenStop(false);
			 la_head->setScale(0.7f*0.75f);	
			 la_head->setPosition(ccp(4,13));
			 cell->addChild(la_head);
		 }
 		 //等级
 		 CCScale9Sprite * sprite_lvFrame = CCScale9Sprite::create("res_ui/zhezhao80.png");
 		 sprite_lvFrame->setAnchorPoint(ccp(1.0f, 0));
 		 sprite_lvFrame->setContentSize(CCSizeMake(34,13));
 		 sprite_lvFrame->setPosition(ccp(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, 19));
 		 cell->addChild(sprite_lvFrame);
 		 //武将等级
 		 int curGeneralLevel = generalDetails.at(idx-1)->activerole().level();
 		 std::string _lv = "LV";
 		 char generalLevel_[10];
 		 sprintf(generalLevel_,"%d",curGeneralLevel);
 		 _lv.append(generalLevel_);
 		 CCLabelTTF * label_lv = CCLabelTTF::create(_lv.c_str(),APP_FONT_NAME,13);
 		 label_lv->setAnchorPoint(ccp(1.0f, 0));
 		 label_lv->setPosition(ccp(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, 18));
 		 label_lv->enableStroke(ccc3(0, 0, 0), 2.0f);
 		 cell->addChild(label_lv);
 
 		 CCSprite * sprite_nameFrame = CCSprite::create("res_ui/name_di2.png");
 		 sprite_nameFrame->setAnchorPoint(ccp(0.5f, 0));
 		 sprite_nameFrame->setPosition(ccp(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 6));
 		 sprite_nameFrame->setScaleX(1.1f);
 		 cell->addChild(sprite_nameFrame);
 		 //武将名字
 		 std::string curGeneralname_ = generalDetails.at(idx-1)->activerole().rolebase().name();
 
 		 CCRichLabel *label_name=CCRichLabel::createWithString(curGeneralname_.c_str(),CCSizeMake(390,38),NULL,NULL,0);
 		 label_name->setScale(0.7f);
 		 label_name->setAnchorPoint(ccp(0.5f,0));
 		 label_name->setPosition(ccp(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 5));
 		 cell->addChild(label_name);
 
		 //稀有度
		 if (generalDetails.at(idx-1)->rare()>0)
		 {
			 CCSprite * sprite_star = CCSprite::create(RecuriteActionItem::getStarPathByNum(generalDetails.at(idx-1)->rare()).c_str());
			 sprite_star->setAnchorPoint(ccp(0,0));
			 sprite_star->setScale(0.6f);
			 sprite_star->setPosition(ccp(5, 21));
			 cell->addChild(sprite_star);
		 }
 		 //武将军衔
 		 int curGeneralEvol_ = generalDetails.at(idx-1)->evolution();
 		 if (curGeneralEvol_ > 0)
 		 {
 			 std::string rankPathBase = "res_ui/rank/rank";
 			 char s[10];
 			 sprintf(s,"%d",curGeneralEvol_);
 			 rankPathBase.append(s);
 			 rankPathBase.append(".png");
 			 CCSprite * sprite_rank = CCSprite::create(rankPathBase.c_str());
			 sprite_rank->setScale(0.7f);
 			 sprite_rank->setAnchorPoint(ccp(1.0f,1.0f));
			 sprite_rank->setPosition(ccp(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, sprite_frame->getContentSize().height*sprite_frame->getScaleY()-6));
			 cell->addChild(sprite_rank);
 		 }
		 //profession
		 std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(generalBaseMsgFromDB->get_profession());
		 CCSprite * sprite_profession = CCSprite::create(generalProfess_.c_str());
		 sprite_profession->setAnchorPoint(ccp(0.5f,0.5f));
		 sprite_profession->setPosition(ccp(62 ,
			 32));
		 sprite_profession->setScale(0.6f);
		 cell->addChild(sprite_profession);
	 }

	 return cell;
}

unsigned int QuiryUI::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	return generalDetails.size()+1;
}

void QuiryUI::scrollViewDidScroll( cocos2d::extension::CCScrollView* view )
{
}

void QuiryUI::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{
}

void QuiryUI::refreshCurGeneralValue(int idx)
{
	UILayer * generalLayer= UILayer::create();
	generalLayer->ignoreAnchorPointForPosition(false);
	generalLayer->setAnchorPoint(ccp(0.5f,0.5f));
	generalLayer->setPosition(ccp(winsize.width/2,winsize.height/2));
	generalLayer->setContentSize(CCSizeMake(800,480));
	generalLayer->setTag(GENERALLAYEROFEQUIP);
	this->addChild(generalLayer);

	std::string generalName_ = generalDetails.at(idx)->activerole().rolebase().name();
	CCRichLabel *label_name=CCRichLabel::createWithString(generalName_.c_str(),CCSizeMake(390,38),NULL,NULL,0);
	label_name->setAnchorPoint(ccp(0.5f,0));
	label_name->setPosition(ccp(400,390));
	generalLayer->addChild(label_name);
	this->setEquipImageVisible(true);
	std::string pressionStr_ = generalDetails.at(idx)->activerole().profession().c_str();
	std::string professionPath = BasePlayer::getProfessionIconByStr(pressionStr_.c_str());
	imagePression->setTexture(professionPath.c_str());

	int levelStr_ =  generalDetails.at(idx)->activerole().level();
	char lvStr_[5];
	sprintf(lvStr_,"%d",levelStr_);
	std::string roleLevelStr_ ="LV.";
	roleLevelStr_.append(lvStr_);
	labelLevel->setText(roleLevelStr_.c_str());

	int fightPoint = generalDetails.at(idx)->fightpoint();
	char fightStr[20];
	sprintf(fightStr,"%d",fightPoint);
	labelFightValue_->setText(fightStr);
	////////////
	int roleFightPoint = roleValue_->fightpoint();
	for (int i = 0;i<generalDetails.size();++i)
	{
		roleFightPoint += generalDetails.at(i)->fightpoint();
	}
	char allFightStr[20];
	sprintf(allFightStr,"%d",roleFightPoint);
	labelAllFightValue_->setText(allFightStr);

	int vectorSize = generalDetails.at(idx)->equipments_size();
	std::string weapEffectStrForRefine="";
	std::string weapEffectStrForStar="";

	for(int i=0;i<vectorSize;i++)
	{
		int equipmentquality_ = generalDetails.at(idx)->equipments(i).goods().quality();
		std::string frameColorPath = "res_ui/";
		if (equipmentquality_ == 1)
		{
			frameColorPath.append("sdi_white");
		}
		else if (equipmentquality_ == 2)
		{
			frameColorPath.append("sdi_green");
		}
		else if (equipmentquality_ == 3)
		{
			frameColorPath.append("sdi_bule");
		}
		else if (equipmentquality_ == 4)
		{
			frameColorPath.append("sdi_purple");
		}
		else if (equipmentquality_ == 5)
		{
			frameColorPath.append("sdi_orange");
		}
		else
		{
			frameColorPath.append("sdi_white");
		}
		frameColorPath.append(".png");

		UIButton * Btn_pacItemFrame = UIButton::create();
		Btn_pacItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnable(true);
		Btn_pacItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_pacItemFrame->setTag(i);
		Btn_pacItemFrame->setPressedActionEnabled(true);
		Btn_pacItemFrame->addReleaseEvent(this,coco_releaseselector(QuiryUI::callBackShowGeneralEquipInfo));
		generalLayer->addWidget(Btn_pacItemFrame);

		switch(generalDetails.at(idx)->equipments(i).part())
		{
		case part_arms:
			{
				Btn_pacItemFrame->setPosition(ccp(284.5f, 381.5f));    //武器 4
				//add weap effect
				int pression_ = generalDetails.at(idx)->equipments(i).goods().equipmentdetail().profession();
				int refineLevel =  generalDetails.at(idx)->equipments(i).goods().equipmentdetail().gradelevel();
				int starLevel =  generalDetails.at(idx)->equipments(i).goods().equipmentdetail().starlevel();
				weapEffectStrForRefine = ActorUtils::getWeapEffectByRefineLevel(pression_,refineLevel);
				weapEffectStrForStar = ActorUtils::getWeapEffectByStarLevel(pression_,starLevel);

				ImageView_arms->setVisible(false);
			}break;
		case part_clothes:
			{
				Btn_pacItemFrame->setPosition(ccp(284.5f, 301.5f));   //衣服 2
				ImageView_clothes->setVisible(false);
			}break;
		case part_helmet:
			{
				Btn_pacItemFrame->setPosition(ccp(515, 381.5f));    //头盔  0
				ImageView_helmet->setVisible(false);
			}break;
		case part_ring:
			{
				Btn_pacItemFrame->setPosition(ccp(284.5f, 220.5f));    //戒指7
				ImageView_ring->setVisible(false);
			}break;
		case part_accessories:
			{
				Btn_pacItemFrame->setPosition(ccp(515, 222));    //腰带 3
				ImageView_accessories->setVisible(false);
			}break;
		case part_shoes:
			{
				Btn_pacItemFrame->setPosition(ccp(515, 302));    //鞋 9
				ImageView_shoes->setVisible(false);
			}break;
		}

		std::string  iconPath_ ="res_ui/props_icon/";
		std::string equipicon_ = generalDetails.at(idx)->equipments(i).goods().icon();
		iconPath_.append(equipicon_);
		iconPath_.append(".png");

		UIImageView *equipIcon_ =UIImageView::create();
		equipIcon_->setTexture(iconPath_.c_str());
		equipIcon_->setAnchorPoint(ccp(0.5f,0.5f));
		equipIcon_->setPosition(ccp(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		if (generalDetails.at(idx)->equipments(i).goods().binding() == 1)
		{
			UIImageView *ImageView_bound = UIImageView::create();
			ImageView_bound->setTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(ccp(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(ccp(-25,23));
			Btn_pacItemFrame->addChild(ImageView_bound);
		}

		int starLv_ = generalDetails.at(idx)->equipments(i).goods().equipmentdetail().starlevel();
		if (starLv_>0)
		{
			char starLvLabel_[5];
			sprintf(starLvLabel_,"%d",starLv_);
			UILabel * label_ =UILabel::create();
			label_->setAnchorPoint(ccp(0,0));
			label_->setPosition(ccp(-23,-23));
			label_->setFontSize(13);
			label_->setText(starLvLabel_);
			Btn_pacItemFrame->addChild(label_);

			UIImageView * imageStar_ =UIImageView::create();
			imageStar_->setTexture("res_ui/star_on.png");
			imageStar_->setScale(0.7f);
			imageStar_->setAnchorPoint(ccp(0,0));
			imageStar_->setPosition(ccp(label_->getContentSize().width-22,label_->getPosition().y));
			Btn_pacItemFrame->addChild(imageStar_);
		}

		int strengthLv_ = generalDetails.at(idx)->equipments(i).goods().equipmentdetail().gradelevel();
		if (strengthLv_>0)
		{
			char strengthLv[5];
			sprintf(strengthLv,"%d",strengthLv_);
			std::string strengthStr_ = "+";
			strengthStr_.append(strengthLv);

			UILabel * label_StrengthLv = UILabel::create();
			label_StrengthLv->setAnchorPoint(ccp(1,1));
			label_StrengthLv->setText(strengthStr_.c_str());
			label_StrengthLv->setPosition(ccp(23,25));
			label_StrengthLv->setFontSize(13);
			Btn_pacItemFrame->addChild(label_StrengthLv);
		}

		if (generalDetails.at(idx)->equipments(i).goods().equipmentdetail().durable()<= 10)
		{
			UIImageView * image_sp = UIImageView::create();
			image_sp->setAnchorPoint(ccp(0.5,0.5f));
			image_sp->setPosition(ccp(0,0));
			image_sp->setTexture("res_ui/mengban_red55.png");
			image_sp->setScale9Enable(true);
			image_sp->setCapInsets(CCRect(5,5,1,1));
			image_sp->setScale9Size(Btn_pacItemFrame->getContentSize());
			Btn_pacItemFrame->addChild(image_sp);

			UILabel * l_des = UILabel::create();
			l_des->setText(StringDataManager::getString("packageitem_notAvailable"));
			l_des->setFontName(APP_FONT_NAME);
			l_des->setFontSize(18);
			l_des->setAnchorPoint(ccp(0.5f,0.5f));
			l_des->setPosition(ccp(0,0));
			image_sp->addChild(l_des);
		}
	}
	// show the player's animation
	CActiveRole * generalActive = new CActiveRole();
	generalActive->CopyFrom(generalDetails.at(idx)->activerole());
	std::string roleFigureName = ActorUtils::getActorFigureName(*generalActive);
	GameActorAnimation* pAnim = ActorUtils::createActorAnimation(roleFigureName.c_str(), 
																roleValue_->hand().c_str(),
																	weapEffectStrForRefine.c_str(), weapEffectStrForStar.c_str());
	pAnim->setPosition(ccp(400,250));
	generalLayer->addChild(pAnim);

	delete generalActive;
}

void QuiryUI::refreshCurRoleValue()
{
	UILayer * roleLayer= UILayer::create();
	roleLayer->ignoreAnchorPointForPosition(false);
	roleLayer->setAnchorPoint(ccp(0.5f,0.5f));
	roleLayer->setPosition(ccp(winsize.width/2,winsize.height/2));
	roleLayer->setContentSize(CCSizeMake(800,480));
	roleLayer->setTag(ROLELAYEROFEQUIP);
	this->addChild(roleLayer);

	std::string roleName_ = roleValue_->rolebase().name();
	CCRichLabel *label_name=CCRichLabel::createWithString(roleName_.c_str(),CCSizeMake(390,38),NULL,NULL,0);
	label_name->setAnchorPoint(ccp(0.5f,0));
	label_name->setPosition(ccp(400,390));
	roleLayer->addChild(label_name);
	this->setEquipImageVisible(true);
	int  vipLevel_ = roleValue_->playerbaseinfo().viplevel();
	if (vipLevel_ > 0)
	{
		UIWidget *widgetVip = MainScene::addVipInfoByLevelForWidget(vipLevel_);
		roleLayer->addWidget(widgetVip);
		widgetVip->setPosition(ccp(label_name->getPositionX() + label_name->getContentSize().width/2 +10,label_name->getPositionY() + widgetVip->getContentSize().height/2));
	}
	
	//int countryId_  = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().country();
	int countryId_ = roleValue_->playerbaseinfo().country();
	if (countryId_ > 0 && countryId_ <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId_);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		CCSprite * countrySp_ = CCSprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(ccp(1,0.5f));
		countrySp_->setPosition(ccp(label_name->getPositionX()- label_name->getContentSize().width/2*0.9f,label_name->getPositionY()+countrySp_->getContentSize().height/2*0.8f));
		countrySp_->setScale(0.65f);
		roleLayer->addChild(countrySp_);
	}

	std::string pressionStr_ = roleValue_->profession().c_str();
	std::string professionPath = BasePlayer::getProfessionIconByStr(pressionStr_.c_str());
	imagePression->setTexture(professionPath.c_str());
	
	int levelStr_ =  roleValue_->level();
	char lvStr_[5];
	sprintf(lvStr_,"%d",levelStr_);
	std::string roleLevelStr_ ="LV.";
	roleLevelStr_.append(lvStr_);
	labelLevel->setText(roleLevelStr_.c_str());
	//////////////////////////////
	int fightPoint = roleValue_->fightpoint();
	char fightStr[20];
	sprintf(fightStr,"%d",fightPoint);
	labelFightValue_->setText(fightStr);
	///////////////////////
	int allFightPoint = roleValue_->fightpoint();
	for (int i = 0;i<generalDetails.size();++i)
	{
		allFightPoint += generalDetails.at(i)->fightpoint();
	}
	char allFightStr[20];
	sprintf(allFightStr,"%d",allFightPoint);
	labelAllFightValue_->setText(allFightStr);

	int vectorSize = playerEquipments.size();
	std::string weapEffectStrForRefine="";
	std::string weapEffectStrForStar="";
	for(int i=0;i<vectorSize;i++)
	{
		int equipmentquality_ = playerEquipments.at(i)->goods().quality();
		std::string frameColorPath = "res_ui/";
		if (equipmentquality_ == 1)
		{
			frameColorPath.append("sdi_white");
		}
		else if (equipmentquality_ == 2)
		{
			frameColorPath.append("sdi_green");
		}
		else if (equipmentquality_ == 3)
		{
			frameColorPath.append("sdi_bule");
		}
		else if (equipmentquality_ == 4)
		{
			frameColorPath.append("sdi_purple");
		}
		else if (equipmentquality_ == 5)
		{
			frameColorPath.append("sdi_orange");
		}
		else
		{
			frameColorPath.append("sdi_white");
		}
		frameColorPath.append(".png");

		UIButton * Btn_pacItemFrame = UIButton::create();
		Btn_pacItemFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnable(true);
		Btn_pacItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_pacItemFrame->setPressedActionEnabled(true);
		Btn_pacItemFrame->setTag(i);
		Btn_pacItemFrame->addReleaseEvent(this,coco_releaseselector(QuiryUI::callBackShowEquipInfo));
		roleLayer->addWidget(Btn_pacItemFrame);

		switch(playerEquipments.at(i)->part())
		{
		case part_arms:
			{
				Btn_pacItemFrame->setPosition(ccp(284.5f, 381.5f));    //武器 4
				//
				int pression_ = playerEquipments.at(i)->goods().equipmentdetail().profession();
				int refineLevel =  playerEquipments.at(i)->goods().equipmentdetail().gradelevel();
				int starLevel =  playerEquipments.at(i)->goods().equipmentdetail().starlevel();

				weapEffectStrForRefine = ActorUtils::getWeapEffectByRefineLevel(pression_,refineLevel);
				weapEffectStrForStar = ActorUtils::getWeapEffectByStarLevel(pression_,starLevel);
				ImageView_arms->setVisible(false);
			}break;
		case part_clothes:
			{
				Btn_pacItemFrame->setPosition(ccp(284.5f, 301.5f));   //衣服 2
				ImageView_clothes->setVisible(false);
			}break;
		case part_helmet:
			{
				Btn_pacItemFrame->setPosition(ccp(515, 381.5f));    //头盔  0
				ImageView_helmet->setVisible(false);
			}break;
		case part_ring:
			{
				Btn_pacItemFrame->setPosition(ccp(284.5f, 220.5f));    //戒指7
				ImageView_ring->setVisible(false);
			}break;
		case part_accessories:
			{
				Btn_pacItemFrame->setPosition(ccp(515, 222));    //腰带 3
				ImageView_accessories->setVisible(false);
			}break;
		case part_shoes:
			{
				Btn_pacItemFrame->setPosition(ccp(515, 302));    //鞋 9
				ImageView_shoes->setVisible(false);
			}break;
		}

		std::string  iconPath_ ="res_ui/props_icon/";
		std::string equipicon_ = playerEquipments.at(i)->goods().icon();
		iconPath_.append(equipicon_);
		iconPath_.append(".png");

		UIImageView *equipIcon_ =UIImageView::create();
		equipIcon_->setTexture(iconPath_.c_str());
		equipIcon_->setAnchorPoint(ccp(0.5f,0.5f));
		equipIcon_->setPosition(ccp(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		if (playerEquipments.at(i)->goods().binding() == 1)
		{
			UIImageView *ImageView_bound = UIImageView::create();
			ImageView_bound->setTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(ccp(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(ccp(-25,23));
			Btn_pacItemFrame->addChild(ImageView_bound);
		}

		int starLv_ =playerEquipments.at(i)->goods().equipmentdetail().starlevel();
		if (starLv_>0)
		{
			char starLvLabel_[5];
			sprintf(starLvLabel_,"%d",starLv_);
			UILabel * label_ =UILabel::create();
			label_->setAnchorPoint(ccp(0,0));
			label_->setPosition(ccp(-23,-23));
			label_->setFontSize(13);
			label_->setText(starLvLabel_);
			Btn_pacItemFrame->addChild(label_);

			UIImageView * imageStar_ =UIImageView::create();
			imageStar_->setTexture("res_ui/star_on.png");
			imageStar_->setScale(0.7f);
			imageStar_->setAnchorPoint(ccp(0,0));
			imageStar_->setPosition(ccp(label_->getContentSize().width-22,label_->getPosition().y));
			Btn_pacItemFrame->addChild(imageStar_);
		}

		int strengthLv_ = playerEquipments.at(i)->goods().equipmentdetail().gradelevel();
		if (strengthLv_>0)
		{
			char strengthLv[5];
			sprintf(strengthLv,"%d",strengthLv_);
			std::string strengthStr_ = "+";
			strengthStr_.append(strengthLv);

			UILabel * label_StrengthLv = UILabel::create();
			label_StrengthLv->setAnchorPoint(ccp(1,1));
			label_StrengthLv->setText(strengthStr_.c_str());
			label_StrengthLv->setPosition(ccp(23,25));
			label_StrengthLv->setFontSize(13);
			Btn_pacItemFrame->addChild(label_StrengthLv);
		}

		if (playerEquipments.at(i)->goods().equipmentdetail().durable()<= 10)
		{
			UIImageView * image_sp = UIImageView::create();
			image_sp->setAnchorPoint(ccp(0.5,0.5f));
			image_sp->setPosition(ccp(0,0));
			image_sp->setTexture("res_ui/mengban_red55.png");
			image_sp->setScale9Enable(true);
			image_sp->setCapInsets(CCRect(5,5,1,1));
			image_sp->setScale9Size(Btn_pacItemFrame->getContentSize());
			Btn_pacItemFrame->addChild(image_sp);

			UILabel * l_des = UILabel::create();
			l_des->setText(StringDataManager::getString("packageitem_notAvailable"));
			l_des->setFontName(APP_FONT_NAME);
			l_des->setFontSize(18);
			l_des->setAnchorPoint(ccp(0.5f,0.5f));
			l_des->setPosition(ccp(0,0));
			image_sp->addChild(l_des);
		}
	}

	// show the player's animation
	std::string roleFigureName = ActorUtils::getActorFigureName(*roleValue_);
	GameActorAnimation* pAnim = ActorUtils::createActorAnimation(roleFigureName.c_str(), 
																		roleValue_->hand().c_str(),
																		weapEffectStrForRefine.c_str(), weapEffectStrForStar.c_str());
	pAnim->setPosition(ccp(400,250));
	roleLayer->addChild(pAnim);
}

bool QuiryUI::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return true;
}

void QuiryUI::setRoleEquipAmount(int amount)
{
	curRoleEquipQuility = amount;
}

int QuiryUI::getRoleEquipAmount()
{
	return curRoleEquipQuility;
}

void QuiryUI::setEquipImageVisible( bool value_ )
{
	ImageView_arms->setVisible(value_);
	ImageView_clothes->setVisible(value_);
	ImageView_ring->setVisible(value_);
	ImageView_helmet->setVisible(value_);
	ImageView_shoes->setVisible(value_);
	ImageView_accessories->setVisible(value_);
}
