#ifndef _UI_EXTENSIONS_ALERTDIALOG_H_
#define _UI_EXTENSIONS_ALERTDIALOG_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/*
* AlertDialog是非阻塞式弹出框
* 弹出警告消息，数秒后自动消失
*/
class AlertDialog : public UIScene
{
public:
	AlertDialog();
	virtual ~AlertDialog();

	static AlertDialog * create(std::string string);
	bool init(std::string string);

	virtual void onEnter();
	virtual void onExit();
};

#endif