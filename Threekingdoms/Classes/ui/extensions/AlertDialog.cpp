#include "AlertDialog.h"
#include "CCRichLabel.h"

AlertDialog::AlertDialog()
{
}

AlertDialog::~AlertDialog()
{
}

AlertDialog * AlertDialog::create(std::string string)
{
	AlertDialog * dialog = new AlertDialog();
	if(dialog && dialog->init(string))
	{
		dialog->autorelease();
		return dialog;
	}
	CC_SAFE_DELETE(dialog);
	return NULL;
}

bool AlertDialog::init(std::string string)
{
	if (UIScene::init())
	{
		CCSize winSize =CCDirector::sharedDirector()->getVisibleSize();

		UILayer * layer = UILayer::create();
		layer->ignoreAnchorPointForPosition(false);
		layer->setAnchorPoint(ccp(0.5f,0.5f));
		layer->setPosition(ccp(0,0));
		layer->setContentSize(winSize);
		addChild(layer);
        
#if CC_TARGET_PLATFORM==CC_PLATFORM_IOS
        const int offsetY = 2;
#else
        const int offsetY = 0;
#endif
		
		CCRichLabel *labelLink=CCRichLabel::createWithString(string.c_str(),CCSizeMake(600,50),NULL,NULL,0,20.f,5,CCRichLabel::alignment_vertical_center);
		labelLink->setAnchorPoint(ccp(0.5f,0.5f));
		labelLink->setPosition(ccp(layer->getContentSize().width/2, layer->getContentSize().height/2 + offsetY));
		layer->addChild(labelLink,1);

		CCScale9Sprite * backGroundSp = CCScale9Sprite::create("res_ui/kuang_1.png");
		backGroundSp->setCapInsets(CCRect(0,0,0,0));
		backGroundSp->setPreferredSize(CCSizeMake(labelLink->getContentSize().width+30,labelLink->getContentSize().height+8));
		backGroundSp->setAnchorPoint(ccp(0.5f,0.5f));
		backGroundSp->setPosition(ccp(winSize.width/2,winSize.height/2));
		layer->addChild(backGroundSp);

		this->setContentSize(backGroundSp->getContentSize());

		/*
		UIPanel * panel=UIPanel::create();
		panel->setAnchorPoint(ccp(0,0));
		panel->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(panel);

		UILabel * labelText= UILabel::create();
		labelText->setAnchorPoint(ccp(0.5f,0.5f));
		labelText->setPosition(ccp(panel->getContentSize().width/2,panel->getContentSize().height/2+3));
		labelText->setFontSize(20);
		labelText->setText(string.c_str());
		labelText->setWidgetZOrder(10);
		panel->addChild(labelText);

		UIImageView * imageBg=UIImageView::create();
		imageBg->setTexture("res_ui/kuang_1.png");
		imageBg->setAnchorPoint(ccp(0.5f,0.5f));
		//imageBg->setTextureRect(CCRect(11,11,1,1));
		imageBg->setScale9Enable(true);
		imageBg->setScale9Size(CCSizeMake(labelText->getContentSize().width+20,labelText->getContentSize().height+20));
		imageBg->setPosition(ccp(0,0));
		panel->addChild(imageBg);
		*/
		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCShow::create(),
			CCDelayTime::create(2.6f),
			CCRemoveSelf::create(),
			NULL);
		
		this->runAction(action);
		return true;
	}
	return false;
}

void AlertDialog::onEnter()
{
	UIScene::onEnter();
}

void AlertDialog::onExit()
{
	UIScene::onExit();
}
