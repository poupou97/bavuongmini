#ifndef _MAILUI_ANNEXITEM_H
#endif _MAILUI_ANNEXITEM_H
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../backpackscene/GoodsItemInfoBase.h"

USING_NS_CC;
USING_NS_CC_EXT;
/*
* FloderItemInstance 是创建一个folder为信息的物品，可以点击查看
* GoodsItemInstance  是创建一个goods为信息的物品，可以点击查看
* FloderItemInfo 是点击FloderItemInstance 或者GoodsItemInstance弹出的详情
*/
class FolderInfo;
class FloderItemInstance:public UIScene
{

public:
	enum typeEnum
	{
		ktype_auction = 1,
		ktype_mail_addAnnex = 2,
	}; 
public:
	FloderItemInstance();
	~FloderItemInstance();

	static FloderItemInstance * create(FolderInfo* folder,int type,int amount = 0);
	bool init(FolderInfo* folder,int type,int amount);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch * pTouch,CCEvent * pEvent);

	void callBackShowAuctionItem(CCObject * obj);

	void callBackShowMailItem(CCObject * obj);

	void setFloderIndex(int index_);
	int getFloderIndex();
private:
	FolderInfo * m_floder;
	int m_type;
	std::string goodsSopbgIcon(int quality_);
	int m_floderIndex;
};

////////////////////////////////////////
class GoodsInfo;
class GoodsItemInstance:public UIScene
{
public:
	enum typeEnumGoods
	{
		ktype_mail_getBackAnnex = 1,
	}; 

public:
	GoodsItemInstance();
	~GoodsItemInstance();

	static GoodsItemInstance * create(GoodsInfo * goods,int type,int amount = 0);
	bool init(GoodsInfo * goods,int amount,int type);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch * pTouch,CCEvent * pEvent);

	void callBackShowMailAnnexItem(CCObject * obj);
private:
	GoodsInfo * m_goods;
	int m_type;
	std::string goodsSopbgIcon(int quality_);
};

///////////////////////////////////////////
class GoodsInfo;
class FloderItemInfo:public GoodsItemInfoBase
{
public:
	enum FloderEnum
	{
		ktype_auctionItem = 1,
		ktype_mail_addAnnexItem = 2,
		ktype_mail_getBackAnnexItem = 3,
	}; 

public:
	FloderItemInfo();
	~FloderItemInfo();

	static FloderItemInfo * create(GoodsInfo * goods,int type);
	bool init(GoodsInfo * goods,int type);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch * pTouch,CCEvent * pEvent);

	void callbackAuctionItem(CCObject * obj);

	void callBackAddMailAnnex(CCObject * obj);

	void callBackShowSendMailAnnex(CCObject * obj);
};
