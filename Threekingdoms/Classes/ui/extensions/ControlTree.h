#ifndef CONTROL_TREE_
#define CONTROL_TREE_

#include "../extensions/UIScene.h"
#include "UIButtonTree.h"

class TreeStructInfo;

#define BUTTON_EXPAND_SHRINK_MARK "button_expand_shrink"  
#define BUTTON_NEWADDMARK "button_newAddMark"
#define BUTTON_SELECT_STATE "button_select_state"
#define SECOND_BUTTON_SELECT_STATE "second_btn_select_state"
#define SECOND_BUTTON_NEWADDMARK "second_button_newAddMark"

#define TREE_BUTTON_TAG 100
#define SECOND_PANEL_TAG 10000
#define SECOND_BUTTONTREE_TAG 1000000


class ControlTree:public UIScene
{
public:
	ControlTree(void);
	~ControlTree(void);

	static ControlTree * create(std::vector<TreeStructInfo *> treeVector,CCSize scrollSize,CCSize treeButtonSize,CCSize branchButtonSize);
	bool init(std::vector<TreeStructInfo *> treeVector,CCSize scrollSize,CCSize treeButtonSize,CCSize branchButtonSize);
	void onEnter();
	void onExit();

	void createSecondtree(int btnIndex,int num, bool isCreate);

	void callBackTreeCellEvent(CCObject * obj);
	void callBackSecondTreeCell(CCObject * obj);

	void setAllButtonSelectStateVisible();
	void setTreeSelectState(std::string imagePath);
	void setSecondTreeImage(std::string imagePath);

	void addcallBackTreeEvent(CCObject* pSender, SEL_CallFuncO pSelector);
	void addcallBackSecondTreeEvent(CCObject* pSender, SEL_CallFuncO pSelector);
	//get tree
	UIButton * getTreeButtonByIndex(int vectorIndex);
	void addMarkOnButton(std::string str,CCPoint p,int vectorIndex = -1);
	void removeMarkOnButton(int vectorIndex = -1);
	
	UIButtonTree * getSecondTreeButtonByIndex(int vectorIndex);
	void addMarkOnSecondButton(std::string str,CCPoint p,int vectorIndex = -1);
	void removeMarkOnSecondButton(int vectorIndex = -1);

	//
	UIButtonTree * getSecondTreeByPanelIndexOrSecondIndex(int panelIndex,int secondIndex);

	//
	void setShowSecondTreeCell(int vectorIndex);

	//get touch tree by index
	int getTreeIndex(int vectorIndex);
	//get touch tree 
	void setTouchTreeIndex(int index);
	int getTouchTreeIndex();

	void setSecondTreeIndex(int index);
	int getSecondTreeIndex();
	//
	void setSecondTreeByEveryPanelIndex(int panelIndex);
	int getSecondTreeByEveryPanelIndex();
	//
	void setButtonTreeSize(CCSize size_);
	CCSize getButtonTreeSize();

	void setBranchButtonSize(CCSize size_);
	CCSize getBranchButtonSize();
	//
	void removeSecondButton(int vectorIndex);

private:
	/*
	*记录上一次点击的父类的子类，用于判断再次点击时是否需要记录
	*如果还是上次点击的父类，当再次点击此父类时应该有记录上次点击的子类，
	*如果不是上次点击的父类，则默认到新的父类的第一个子类
	*/
	void setIsLastTree(bool isValue);
	bool getIsLastTree();
	bool m_isLastTree;
private:
	UIScrollView * scrollView_info;
	int tree_buttonNum;
	int tree_buttonHeight;

	int secondTree_buttonNum;
	int secondTree_buttonHeight;

	std::vector<TreeStructInfo * > m_treeVector; 

	std::string m_treeSelectState;//树干选中状态
	std::string m_secondTreeImage;//二级树干图片资源
	std::string m_expandImage;//扩展图片资源
	std::string m_shrink;//收缩图片资源

	int m_curSelectIndex;

	int m_secondTreeOfPanelIndex;

	int m_touchTreeIndex;
	
	CCSize treeButton_size;
	CCSize branchButton_size;
private:
	CCObject * senderListener_tree;
	SEL_PushEvent senderSelector_tree;

	CCObject * senderListener_branch;
	SEL_PushEvent senderSelector_branch;
};


///////////////////////////////
class TreeStructInfo
{
public:
	TreeStructInfo(void);
	~TreeStructInfo(void);

	void setTreeName(std::string name);
	std::string getTreeName();

	void addSecondVector(std::string secondString);
	std::vector<std::string> getSecondVector();

private:
	int m_treeIndex_;
	std::string m_treeName_;
	std::vector<std::string> m_secondTree;
};
#endif;
