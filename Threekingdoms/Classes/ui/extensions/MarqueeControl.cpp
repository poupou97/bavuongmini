#include "MarqueeControl.h"
#include "CCRichLabel.h"
#include "utils/StaticDataManager.h"
#include "AppMacros.h"
#include "gamescene_state/MainScene.h"
#include "../Chat_ui/ChatCell.h"


MarqueeControl::MarqueeControl(void)
{
	m_prioity = 0;
}


MarqueeControl::~MarqueeControl(void)
{
}

MarqueeControl * MarqueeControl::create( std::string string,int priority)
{
	MarqueeControl * marquee= new MarqueeControl();
	if (marquee && marquee->init(string,priority))
	{
		marquee->autorelease();
		return marquee;
	}
	CC_SAFE_DELETE(marquee);
	return NULL;
}

bool MarqueeControl::init( std::string string,int priority)
{
	if (UIScene::init())
	{
		CCSize winSize =CCDirector::sharedDirector()->getVisibleSize();

		CCRichLabel *labelLink=CCRichLabel::createWithString(string.c_str(),CCSizeMake(winSize.width,30),NULL,NULL,0,20,5,CCRichLabel::alignment_vertical_center);
		labelLink->setAnchorPoint(ccp(0.5f,0));
		labelLink->setPosition(ccp(0,1));
		labelLink->setScale(0.9f);
		m_pUiLayer->addChild(labelLink,1);

		CCScale9Sprite * background_ = CCScale9Sprite::create("res_ui/paoMaDeng_di.png");
		background_->setCapInsets(CCRect(0,0,0,0));
		background_->setAnchorPoint(ccp(0.5,0));
		background_->setPosition(ccp(0,0));
		background_->setPreferredSize(CCSizeMake(winSize.width,labelLink->getContentSize().height+2));
		m_pUiLayer->addChild(background_,0);
		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCScaleTo::create(1.0f,1,1),
			CCDelayTime::create(7.0f),
			CCScaleTo::create(0.3f,1,0),
			CCRemoveSelf::create(),
			NULL);

		m_prioity = priority;

		this->runAction(action);
		this->setContentSize(CCSizeMake(300,50));
		return true;
	}
	return false;
}

void MarqueeControl::onEnter()
{
	UIScene::onEnter();
}

void MarqueeControl::onExit()
{
	UIScene::onExit();
}

int MarqueeControl::getPrioity()
{
	return m_prioity;
}

////////////////////////////////////////////
FlowerMarquee::FlowerMarquee(void)
{
}


FlowerMarquee::~FlowerMarquee(void)
{
}

FlowerMarquee * FlowerMarquee::create( std::string string)
{
	FlowerMarquee * flower_ = new FlowerMarquee();
	if (flower_ && flower_->init(string))
	{
		flower_->autorelease();
		return flower_;
	}
	CC_SAFE_DELETE(flower_);
	return NULL;
}

bool FlowerMarquee::init( std::string string)
{
	if (UIScene::init())
	{
		CCSize winSize =CCDirector::sharedDirector()->getVisibleSize();

		labelLink=CCRichLabel::createWithString(string.c_str(),CCSizeMake(winSize.width - 100,30),NULL,NULL,0);
		labelLink->setAnchorPoint(ccp(0.5f,0));
		labelLink->setPosition(ccp(0,1));
		labelLink->setScale(0.9f);
		labelLink->setVisible(false);
		m_pUiLayer->addChild(labelLink,1);

		m_labelHight = labelLink->getContentSize().height;
		m_labelWidth = labelLink->getContentSize().width;

		background_ = CCScale9Sprite::create("res_ui/flower_di.png");
		//background_->setCapInsets(CCRect(128,32,1,1));
		background_->setAnchorPoint(ccp(0.5,0));
		background_->setPosition(ccp(0,0));
		//background_->setPreferredSize(CCSizeMake(labelLink->getContentSize().width+100,labelLink->getContentSize().height+2));
		m_pUiLayer->addChild(background_,0);

		flower_left = CCSprite::create("res_ui/props_icon/flower_pink2.png");
		flower_left->setAnchorPoint(ccp(0.5f,0.5f));
		flower_left->setPosition(ccp(background_->getPositionX() - background_->getContentSize().width/2+flower_left->getContentSize().width,
													background_->getContentSize().height/2));
		m_pUiLayer->addChild(flower_left,1);

		flower_right = CCSprite::create("res_ui/props_icon/flower_pink2.png");
		flower_right->setAnchorPoint(ccp(0.5f,0.5f));
		flower_right->setPosition(ccp(background_->getPositionX()+background_->getContentSize().width/2-flower_right->getContentSize().width,
									background_->getContentSize().height/2));
		flower_right->setRotationY(180);
		m_pUiLayer->addChild(flower_right,1);

		scheduleUpdate();

		this->setContentSize(CCSizeMake(300,50));
		return true;
	}
	return false;
}

void FlowerMarquee::onEnter()
{
	UIScene::onEnter();
}

void FlowerMarquee::onExit()
{
	UIScene::onExit();
}

void FlowerMarquee::update( float delta )
{
	if (m_labelWidth  < FLOWERMARQUEE_SP_WIDTH)
	{
		background_->setPreferredSize(CCSizeMake(background_->getContentSize().width+SP_SPEED*delta,m_labelHight+2));
		flower_left->setPosition(ccp(background_->getPositionX() - background_->getContentSize().width/2+flower_left->getContentSize().width,
			background_->getContentSize().height/2));

		flower_right->setPosition(ccp(background_->getPositionX()+background_->getContentSize().width/2-flower_right->getContentSize().width,
			background_->getContentSize().height/2));

		m_labelWidth = background_->getContentSize().width;
	}else
	{
		labelLink->setVisible(true);

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCScaleTo::create(1.0f,1,1),
			CCDelayTime::create(7.0f),
			CCScaleTo::create(0.3f,1,0),
			CCRemoveSelf::create(),
			NULL);

		this->runAction(action);
	}
}

////////////////////////////////////////////
ChatAcousticMarquee::ChatAcousticMarquee(void)
{
}


ChatAcousticMarquee::~ChatAcousticMarquee(void)
{
}

ChatAcousticMarquee * ChatAcousticMarquee::create(std::string country,std::string name,std::string content,int vipLv)
{
	ChatAcousticMarquee * flower_ = new ChatAcousticMarquee();
	if (flower_ && flower_->init(country,name,content,vipLv))
	{
		flower_->autorelease();
		return flower_;
	}
	CC_SAFE_DELETE(flower_);
	return NULL;
}

bool ChatAcousticMarquee::init(std::string country,std::string name,std::string content,int vipLv)
{
	if (UIScene::init())
	{
		CCSize winSize =CCDirector::sharedDirector()->getVisibleSize();
		int itemSize = 0;
		
		CCSprite * sp_country = CCSprite::create(ChatCell::getCountryImageByStr(country.c_str()).c_str());
		sp_country->setAnchorPoint(ccp(0,0));
		sp_country->setPosition(ccp(0,0));
		sp_country->setScale(0.7f);
		m_pUiLayer->addChild(sp_country,1);
		itemSize = sp_country->getContentSize().width*0.7f;

		CCLabelTTF * label_name = CCLabelTTF::create(name.c_str(),APP_FONT_NAME, 20);
		label_name->setAnchorPoint(ccp(0,0));
		label_name->setPosition(ccp(itemSize,0));
		label_name->setColor(ccc3(0,255,0));//G
		m_pUiLayer->addChild(label_name,1);
		itemSize += label_name->getContentSize().width;

		CCNode * widgetVip;
		if (vipLv > 0)
		{
			widgetVip = MainScene::addVipInfoByLevelForNode(vipLv);
			m_pUiLayer->addChild(widgetVip,1);
			widgetVip->setPosition(ccp(itemSize + widgetVip->getContentSize().width/2,widgetVip->getContentSize().height/2));

			itemSize += widgetVip->getContentSize().width;
		}

		CCLabelTTF *label_colon = CCLabelTTF::create(":",APP_FONT_NAME, 20);
		label_colon->setAnchorPoint(ccp(0,0));
		label_colon->setPosition(ccp(itemSize+5,0));
		m_pUiLayer->addChild(label_colon,1);
		itemSize += label_colon->getContentSize().width;

		RichLabelDefinition def;
		def.firstLineIndent = itemSize+5;
		def.fontSize = 20;
		def.alignment = 2;
		CCRichLabel * labelLink = CCRichLabel::create(content.c_str(),CCSizeMake(330,20),def);
		labelLink->setAnchorPoint(ccp(0,0));
		labelLink->setPosition(ccp(0,1));
		m_pUiLayer->addChild(labelLink,1);

		const std::vector<int>& linesHeight = labelLink->getLinesHeight();
		CCAssert(linesHeight.size() > 0, "out of range");
		int offsetY = labelLink->getContentSize().height - linesHeight.at(0);

		sp_country->setPositionY(offsetY);
		label_name->setPositionY(offsetY);
		if (vipLv > 0)
		{
			widgetVip->setPositionY(offsetY+widgetVip->getContentSize().height/2);
		}
		label_colon->setPositionY(offsetY);

		background_ = CCScale9Sprite::create("res_ui/zhezhao70.png");
		background_->setOpacity(200);
		background_->setPreferredSize(CCSizeMake(350,labelLink->getContentSize().height));
		background_->setAnchorPoint(ccp(0,0));
		background_->setPosition(ccp(-10,0));
		m_pUiLayer->addChild(background_,0);

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCScaleTo::create(1.0f,1,1),
			CCDelayTime::create(5.0f),
			CCScaleTo::create(0.3f,1,0),
			CCRemoveSelf::create(),
			NULL);
		this->runAction(action);

		//scheduleUpdate();

		this->setContentSize(CCSizeMake(350,labelLink->getContentSize().height));
		return true;
	}
	return false;
}

void ChatAcousticMarquee::onEnter()
{
	UIScene::onEnter();
}

void ChatAcousticMarquee::onExit()
{
	UIScene::onExit();
}

void ChatAcousticMarquee::update( float delta )
{
	/*
	if (m_labelWidth  < FLOWERMARQUEE_SP_WIDTH)
	{
		background_->setPreferredSize(CCSizeMake(background_->getContentSize().width+SP_SPEED*delta,m_labelHight+2));
		flower_left->setPosition(ccp(background_->getPositionX() - background_->getContentSize().width/2+flower_left->getContentSize().width,
			background_->getContentSize().height/2));

		flower_right->setPosition(ccp(background_->getPositionX()+background_->getContentSize().width/2-flower_right->getContentSize().width,
			background_->getContentSize().height/2));

		m_labelWidth = background_->getContentSize().width;
	}else
	{
		labelLink->setVisible(true);

		CCActionInterval * action =(CCActionInterval *)CCSequence::create(
			CCScaleTo::create(1.0f,1,1),
			CCDelayTime::create(7.0f),
			CCScaleTo::create(0.3f,1,0),
			CCRemoveSelf::create(),
			NULL);

		this->runAction(action);
	}
	*/
}