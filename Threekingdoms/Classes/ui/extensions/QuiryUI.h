
#ifndef _UI_EXTENSION_QUIRY_H
#define  _UI_EXTENSION_QUIRY_H

#include "UIScene.h"
class CEquipment;
class CActiveRole;
class CGeneralDetail;

#define  ROLELAYEROFEQUIP 500 
#define  GENERALLAYEROFEQUIP 510

class QuiryUI:public UIScene, public cocos2d::extension::CCTableViewDataSource, public cocos2d::extension::CCTableViewDelegate
{
public:

	enum
	{
		part_arms = 4,
		part_clothes = 2,
		part_ring = 7,
		part_helmet = 0,
		part_shoes = 9,
		part_accessories = 3,
	};


	QuiryUI(void);
	~QuiryUI(void);

	static QuiryUI *create(CActiveRole * activerole,std::vector<CEquipment *>euipmentsVector,std::vector<CGeneralDetail *>generalDetailVector);
	bool init(CActiveRole * activerole,std::vector<CEquipment *>euipmentsVector,std::vector<CGeneralDetail *>generalDetailVector);

	void onEnter();
	void onExit();
	void callBackExit(CCObject * obj);
	void callBackShowEquipInfo(CCObject *obj);
	void callBackShowGeneralEquipInfo(CCObject * obj);

	void refreshCurRoleValue();
	void refreshCurGeneralValue(int idx);

	void setRoleEquipAmount(int amount);
	int getRoleEquipAmount();

	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	// default implements are used to call script callback if exist
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
private:
	CCSize winsize;
	UILayer *m_Layer;
	UIImageView * imagePression;
	UILabel * labelLevel;
	UILabelBMFont * labelFightValue_;
	UILabelBMFont * labelAllFightValue_;
	
	int curRoleEquipQuility;
	int curRoleIndex;
	CActiveRole * roleValue_;
	std::vector<CEquipment*>playerEquipments;
	std::vector<CGeneralDetail *>generalDetails;

private:
	void setEquipImageVisible(bool value_);
	UIImageView * ImageView_arms;
	UIImageView * ImageView_clothes;
	UIImageView * ImageView_ring;
	UIImageView * ImageView_helmet;
	UIImageView * ImageView_shoes;
	UIImageView * ImageView_accessories;
};

#endif