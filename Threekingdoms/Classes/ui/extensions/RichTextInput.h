#ifndef _RICHTEXT_INPUT_H_
#define _RICHTEXT_INPUT_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCRichLabel;
class KeyboardNotificationLayer;

class KeyboardNotificationLayer : public CCLayer, public CCIMEDelegate
{
public:
    KeyboardNotificationLayer();

    virtual void onClickTrackNode(bool bClicked) = 0;
    virtual bool isClickTrackNode(const CCRect& trackNodeRect, const CCPoint& touchPoint) = 0;

    virtual void registerWithTouchDispatcher();
    virtual void keyboardWillShow(CCIMEKeyboardNotificationInfo& info);
    virtual void keyboardWillHide(CCIMEKeyboardNotificationInfo& info);

    // CCLayer
    virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
    virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
    
protected:
    void updateInputBoxPosition(bool bAnimated = false);
    
protected:
    static CCNode* s_currentClickingNode;

    CCNode * m_pTrackNode;
    CCPoint  m_beginPos;
    float m_inputCursorOffsetY;
    
    CCRect m_keyboardEndRect;
    
    // the vertical adjustment when keyboard appear
    float m_adjustVert;
};

//////////////////////////////////////////////////////////////////////////
// TextFieldTTFActionTest
//////////////////////////////////////////////////////////////////////////

class RichTextInputBox : public KeyboardNotificationLayer, public CCTextFieldDelegate, public CCScrollViewDelegate
{
    CCTextFieldTTF *    m_pTextField;
    int                 m_nCharLimit;       // the textfield max char limit
	int					m_nInputBoxWidth;
    int					m_nInputBoxHeight;
	CCRichLabel *		m_pRichLabel;
	CCSprite *			m_pCursorSprite;
    int                 m_direction;

public:
    typedef enum {
        kCCRichInputDirectionHorizontal = 0,
        kCCRichInputDirectionVertical = 1,
    } CCRichInputDirection;

   enum {
        kCCInputModeAppendOnly = 0,
        kCCInputModeFullText = 1,
    } ;
    
public:
	RichTextInputBox();

    virtual void onClickTrackNode(bool bClicked);
    bool isClickTrackNode(const CCRect& trackNodeRect, const CCPoint& touchPoint);

    // CCLayer
    virtual void onEnter();
    virtual void onExit();

	void scrollViewDidScroll(CCScrollView* view);
	void scrollViewDidZoom(CCScrollView* view);

    // CCTextFieldDelegate
    virtual bool onTextFieldAttachWithIME(CCTextFieldTTF * pSender);
    virtual bool onTextFieldDetachWithIME(CCTextFieldTTF * pSender);
    virtual bool onTextFieldInsertText(CCTextFieldTTF * pSender, const char * text, int nLen);
    virtual bool onTextFieldDeleteBackward(CCTextFieldTTF * pSender, const char * delText, int nLen);
    virtual bool onDraw(CCTextFieldTTF * pSender);

	void menuInsertEmotionCallback(CCObject* pSender);
	void menuInsertItemCallback(CCObject* pSender);

	const char* getInputString();
	void deleteAllInputString();

	//CCRichLabel* getTextTTF();

	void setCharLimit(int limit);
	inline void setInputBoxWidth(int width) { m_nInputBoxWidth = width; };
    inline void setInputBoxHeight(int height) { m_nInputBoxHeight = height; };
    inline int getInputBoxHeight() { return m_nInputBoxHeight; };
    
    void setDirection(int dir);
	void setMultiLinesMode(bool bMultiLins);
	void setInputMode(int mode);

private:
	/* when insert or delete char, adjust input cursor position */
	void adjustInputCursorPos();

	void sychronizeTextFieldTTF();

	int m_inputMode;
	bool m_bMultiLines;
};

#endif    // _RICHTEXT_INPUT_H_
