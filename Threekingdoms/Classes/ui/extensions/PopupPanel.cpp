#include "PopupPanel.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "RichTextInput.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "AppMacros.h"
#include "CCRichLabel.h"

PopupPanel::PopupPanel(void)
{
	reqOption_=-1;
	buttonSize = 0;
}


PopupPanel::~PopupPanel(void)
{
}

PopupPanel * PopupPanel::create(PopupBoardPara popupBoardPara,std::vector<PopuBoardOption *>optionvector)
{
	PopupPanel * panel_ =new PopupPanel();
	if (panel_ && panel_->init(popupBoardPara,optionvector))
	{
		panel_->autorelease();
		return panel_;
	}
	CC_SAFE_DELETE(panel_);
	return NULL;
}

bool PopupPanel::init(PopupBoardPara popupBoardPara,std::vector<PopuBoardOption *>optionvector)
{
	if (UIScene::init())
	{
		//CCSize winsize =CCDirector::sharedDirector()->getVisibleSize();
		CCSize winsize =CCSizeMake(800,480);
		UILayer * layer = UILayer::create();
		layer->ignoreAnchorPointForPosition(false);
		layer->setAnchorPoint(ccp(0.5f,0.5f));
		layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		layer->setContentSize(winsize);
		addChild(layer);
		m_boardType =popupBoardPara.boardType_;

		CCScale9Sprite * backGroundSp = CCScale9Sprite::create("res_ui/dikuang_new.png");
		backGroundSp->setCapInsets(CCRect(0,0,0,0));
		backGroundSp->setPreferredSize(CCSizeMake(377,201));
		backGroundSp->setAnchorPoint(ccp(0.5f,0.5f));
		backGroundSp->setPosition(ccp(winsize.width/2,winsize.height/2));
		layer->addChild(backGroundSp);

		CCScale9Sprite * backGroundSp_frame = CCScale9Sprite::create("res_ui/LV4_didia.png");
		backGroundSp->addChild(backGroundSp_frame);

		std::string showString = popupBoardPara.boardText_;
		CCRichLabel *labelLink=CCRichLabel::createWithString(showString.c_str(),CCSizeMake(290,50),NULL,NULL,0,20,2);
		labelLink->setAnchorPoint(ccp(0,1));
		labelLink->setPosition(ccp(45,backGroundSp->getContentSize().height-30));
		backGroundSp->addChild(labelLink);

		m_countDown =popupBoardPara.boardTime_;
		buttonSize = optionvector.size();
		if (m_countDown > 0)
		{
			char countDownStr[10];
			sprintf(countDownStr,"%d",m_countDown);
			std::string timeBegin = "(";
			timeBegin.append(countDownStr);
			timeBegin.append(")");

			centerTimeLabel = CCLabelTTF::create(timeBegin.c_str(), APP_FONT_NAME, 20);
			centerTimeLabel->setAnchorPoint(ccp(0.5f,0.5f));
			centerTimeLabel->setPosition(ccp(backGroundSp->getContentSize().width/2,backGroundSp->getContentSize().height/2 - 10));
			backGroundSp->addChild(centerTimeLabel);

			defaultId_ = popupBoardPara.boardDefaultid_;
			this->schedule(schedule_selector(PopupPanel::step),1.0f); 
		}

		if (m_boardType == TABLETBOARD)
		{
			CCScale9Sprite * inputboxSp = CCScale9Sprite::create("res_ui/InputBox30.png");
			inputboxSp->setPreferredSize(CCSizeMake(246,35));
			inputboxSp->setAnchorPoint(ccp(0,1));
			inputboxSp->setPosition(ccp(49,backGroundSp->getContentSize().height-labelLink->getContentSize().height - 30));
			backGroundSp->addChild(inputboxSp);

			int inputMaxsize =popupBoardPara.boardMaxsize_;
			textBox_private=new RichTextInputBox();
			textBox_private->setCharLimit(inputMaxsize);
			textBox_private->setAnchorPoint(ccp(0.5f,1));
			textBox_private->setPosition(ccp(51,backGroundSp->getContentSize().height-labelLink->getContentSize().height-33 - 30));
			backGroundSp->addChild(textBox_private,10);
			textBox_private->autorelease();
		}

		for (int i=0;i<buttonSize;i++)
		{
			int selectid_ =optionvector.at(i)->id_;
			std::string string_title =optionvector.at(i)->title_;
			std::string string_msg = optionvector.at(i)->msg_;

			UILayer * layerbutton = UILayer::create();
			layerbutton->ignoreAnchorPointForPosition(false);
			layerbutton->setAnchorPoint(ccp(0.5f,0.5f));
			layerbutton->setPosition(ccp(winsize.width/2,winsize.height/2));
			layerbutton->setContentSize(winsize);
			layer->addChild(layerbutton);

			UIButton * buttonSure= UIButton::create();
			buttonSure->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
			buttonSure->setTouchEnable(true);
			buttonSure->setPressedActionEnabled(true);
			buttonSure->addReleaseEvent(this,coco_releaseselector(PopupPanel::callBackSend));
			buttonSure->setAnchorPoint(ccp(0.5f,0.5f));
			buttonSure->setScale9Enable(true);
			buttonSure->setScale9Size(CCSizeMake(110,43));
			buttonSure->setCapInsets(CCRect(18,9,2,23));
			if (optionvector.size()==1)
			{
				buttonSure->setPosition(ccp(layer->getContentSize().width/2,layer->getContentSize().height/2 - backGroundSp->getContentSize().height/2 +48));
			}else
			{
				buttonSure->setPosition(ccp(layer->getContentSize().width/2-85+ i* 170,layer->getContentSize().height/2 - backGroundSp->getContentSize().height/2 +48));
			}
			buttonSure->setTag(selectid_);
			layerbutton->addWidget(buttonSure);

			UILabel * btn_label= UILabel::create();
			btn_label->setAnchorPoint(ccp(0.5f,0.5f));
			btn_label->setPosition(ccp(0,0));
			btn_label->setFontSize(18);
			btn_label->setText(string_title.c_str());
			buttonSure->addChild(btn_label);
		}

		
		if (m_boardType == TEXTBOARD)
		{
			UILayer * layerbutton = UILayer::create();
			layerbutton->ignoreAnchorPointForPosition(false);
			layerbutton->setAnchorPoint(ccp(0.5f,0.5f));
			layerbutton->setPosition(ccp(winsize.width/2,winsize.height/2));
			layerbutton->setContentSize(winsize);
			layer->addChild(layerbutton);

			UIButton * buttonClose_=UIButton::create();
			buttonClose_->setTouchEnable(true);
			buttonClose_->setPressedActionEnabled(true);
			buttonClose_->setTextures("res_ui/close.png","res_ui/close.png","");
			buttonClose_->setAnchorPoint(ccp(0.5f,0.5f));
			buttonClose_->setPosition(ccp(layer->getContentSize().width/2+ backGroundSp->getContentSize().width/2 - buttonClose_->getContentSize().width/2-5,
										355-buttonClose_->getContentSize().height/2 - 5));
			buttonClose_->addReleaseEvent(this,coco_releaseselector(PopupPanel::callBackClose));
			layerbutton->addWidget(buttonClose_);
		}
		
		if (buttonSize > 0)
		{
			backGroundSp_frame->setCapInsets(CCRect(13,14,1,1));
			backGroundSp_frame->setPreferredSize(CCSizeMake(300,108));
			backGroundSp_frame->setAnchorPoint(ccp(0.5f,1.0f));
			backGroundSp_frame->setPosition(ccp(backGroundSp->getContentSize().width/2-2,180));
		}else
		{
			backGroundSp_frame->setCapInsets(CCRect(13,14,1,1));
			backGroundSp_frame->setPreferredSize(CCSizeMake(304,160));
			backGroundSp_frame->setAnchorPoint(ccp(0.5f,0.5f));
			backGroundSp_frame->setPosition(ccp(backGroundSp->getContentSize().width/2-3,backGroundSp->getContentSize().height/2+5));
		}



		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}
void PopupPanel::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void PopupPanel::onExit()
{
	UIScene::onExit();
}

void PopupPanel::step( float dt )
{
	m_countDown--;
	char countDownStr_[10];
	sprintf(countDownStr_,"%d",m_countDown);

	std::string timeBegin = "(";
	timeBegin.append(countDownStr_);
	timeBegin.append(")");

	centerTimeLabel->setString(timeBegin.c_str());
	if (m_countDown==0)
	{
		this->callBackCoutDownDefault();
	}
}

void PopupPanel::selectedStateEvent( CCObject *pSender, CheckBoxEventType type )
{
	UICheckBox * checkBox_ =(UICheckBox *)pSender;
	reqOption_ =checkBox_->getSelectedState();
}

void PopupPanel::callBackCoutDownDefault()
{
	//send 1128 default
	sendContent content_ ={defaultId_,"",reqOption_};
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1128,&content_);
	this->closeAnim();
}

void PopupPanel::callBackSend( CCObject * obj )
{
	UIButton * btn_ =(UIButton *)obj;
	reqSelectId_ =btn_->getTag();
	if (m_boardType == TABLETBOARD)
	{
		const char * contentStr_ = textBox_private->getInputString();
		std::string strings_ ="";
		if (contentStr_!=NULL)
		{
			strings_ = contentStr_;
		}
		sendContent content_ ={reqSelectId_,strings_,reqOption_};
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1128,&content_);
		this->closeAnim();
	}else
	{
		sendContent content_ ={reqSelectId_,"",reqOption_};
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1128,&content_);
		this->closeAnim();
	}
}

void PopupPanel::callBackClose( CCObject * obj )
{
	this->closeAnim();
}




