#include "UITab.h"
#include <cstdio> 
#include <string> 
#include <sstream>

USING_NS_CC;
USING_NS_CC_EXT;

#define TABBUTTONBASETAG 150

UITab::UITab():
m_pIndexListener(NULL),
m_pfnIndexSelector(NULL),
m_nMaxCount(1),
m_currentIndex(0),
m_spaceWidth(20),
m_spaceHeight(50),
highLightImagePath(""),
isAutoClose(false),
currentTabMod(HORIZONTAL),
isPressedActionEnabled(false)
{
}


UITab::~UITab()
{
	if (curTabMode == VERTICAL_LIST)
	{
		CCLog("VERTICAL_LIST");
	}
}
/*
bool UITab::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
		//this->removeFromParentAndCleanup(false);
		return true;
}

void UITab::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void UITab::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void UITab::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}*/

UITab * UITab::createWithImage(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allImagePath[],TabMode mode,int space)
{
	UITab * tab = new UITab();
	if (tab && tab->initWithImage(maxcount,normalImage,selectedImage,disabledImage,allImagePath,mode,space))
	{
		tab->autorelease();
		return tab;
	}
	CC_SAFE_DELETE(tab);
	return NULL;
}

bool UITab::initWithImage(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allImagePath[],TabMode mode,int space)
{
	if (UIWidget::init())
	{
		this->normalImagePath= (char *)normalImage;
		if (mode == HORIZONTAL)
		{
			this->m_spaceWidth = space;
		}
		else if (mode == VERTICAL || mode == VERTICAL_LIST)
		{
			this->m_spaceHeight = space;
		}
		this->currentTabMod = mode;
		
		int x = 0;
		int y = 0;
		int aaa = maxcount;
		UIImageView * tempImageView = UIImageView::create();
		tempImageView->setTexture(normalImage);
		for (int i = 0;i<maxcount;++i)
		{
			UIButton * _Button = UIButton::create();
			_Button->setNormalTexture(normalImage);
			_Button->setPressedTexture(selectedImage);
			_Button->setDisabledTexture(disabledImage);
			_Button->setTag(TABBUTTONBASETAG+i);
			//_Button->setPressedActionEnabled(true);
			if (mode == HORIZONTAL)
			{
				curTabMode = HORIZONTAL;
				_Button->setAnchorPoint(ccp(0.5f,0));
				//item->setPosition(x, 0);
				_Button->setPosition(ccp(i*tempImageView->getContentSize().width+x+tempImageView->getContentSize().width/2,0));
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				
				x += m_spaceWidth;

				UIImageView * imageView = UIImageView::create();
				imageView->setTexture((const char *)allImagePath[i]);
				imageView->setAnchorPoint(ccp(0.5f,0));
				imageView->setPosition(ccp(0,(_Button->getContentSize().height-imageView->getContentSize().height)/2));
				_Button->addChild(imageView);

			}
			else if (mode == VERTICAL)
			{
				curTabMode = VERTICAL;

				UIImageView * imageView = UIImageView::create();
				imageView->setTexture((const char *)allImagePath[i]);
				_Button->addChild(imageView);

				if (_Button->getContentSize().width>_Button->getContentSize().height)
				{
					_Button->setAnchorPoint(ccp(0.5f,0.5f));
					_Button->setPosition(ccp(tempImageView->getContentSize().width/2,-(i+1)*tempImageView->getContentSize().height+tempImageView->getContentSize().height/2-y));
					imageView->setAnchorPoint(ccp(0.5f,0.5f));
					imageView->setPosition(ccp(0,0));
				}
				else
				{
					_Button->setAnchorPoint(ccp(0,0.5f));
					_Button->setPosition(ccp(0,-(i+1)*tempImageView->getContentSize().height+tempImageView->getContentSize().height/2-y));	
					imageView->setAnchorPoint(ccp(0,0.5f));
					imageView->setPosition(ccp((_Button->getContentSize().width-imageView->getContentSize().width)/2,0));
				}
				
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				y += m_spaceHeight;
			}
			else if (mode == VERTICAL_LIST)
			{
				curTabMode = VERTICAL_LIST;
				_Button->setAnchorPoint(ccp(0,1));
				_Button->setPosition(ccp(0,0));
				int aabb = -i*tempImageView->getContentSize().height-y;
				CCLog("%d",aabb);
				CCPoint	position = ccp(0,-i*tempImageView->getContentSize().height-y);
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				y += m_spaceHeight;

				UIImageView * imageView = UIImageView::create();
				imageView->setTexture((const char *)allImagePath[i]);
				imageView->setAnchorPoint(ccp(0.5f,0.5f));
				imageView->setPosition(ccp(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_Button->addChild(imageView);

				CCAction * action = CCMoveTo::create(0.12+0.05*i,position);
				_Button->runAction(action);
			}

			//_Button->setTouchPriority(0);
			_Button->addReleaseEvent(this,coco_releaseselector(UITab::touchEndedEvent));
			_Button->setTouchEnable(true);

			this->addChild(_Button);
		}

		m_nMaxCount = maxcount;

		this->setTouchEnable(true);
		CCLog("this.contenetSize = %d,%d",this->getContentSize().width,this->getContentSize().height);
		return true;
	}
	return false;
	
}




UITab * UITab::createWithText(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],TabMode mode,int space,int fontSize)
{
	UITab * tab = new UITab();
	if (tab && tab->initWithText(maxcount,normalImage,selectedImage,disabledImage,allTextNames,mode,space,fontSize))
	{
		//tab->autoreleqase();
		return tab;
	}
	CC_SAFE_DELETE(tab);
	return NULL;
}

bool UITab::initWithText(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],TabMode mode,int space,int fontSize)
{
	if (UIWidget::init())
	{
		this->normalImagePath= (char *)normalImage;
		if (mode == HORIZONTAL)
		{
			this->m_spaceWidth = space;
		}
		else if (mode == VERTICAL || mode == VERTICAL_LIST)
		{
			this->m_spaceHeight = space;
		}
		this->currentTabMod = mode;
		
		int x = 0;
		int y = 0;
		int aaa = maxcount;
		UIImageView * tempImageView = UIImageView::create();
		tempImageView->setTexture(normalImage);
		for (int i = 0;i<maxcount;++i)
		{
			//UIButton * _Button = UIButton::create();
			UITextButton * _Button = UITextButton::create();
			_Button->setNormalTexture(normalImage);
			_Button->setPressedTexture(selectedImage);
			_Button->setDisabledTexture(disabledImage);
			//_Button->setText(allTextNames[i]);
			//_Button->setFontSize(25);
			_Button->setTag(TABBUTTONBASETAG+i);
			//_Button->setPressedActionEnabled(true);

			if (mode == HORIZONTAL)
			{
				curTabMode = HORIZONTAL;
				_Button->setAnchorPoint(ccp(0.5f,0));
				//item->setPosition(x, 0);
				_Button->setPosition(ccp(i*tempImageView->getContentSize().width+x+tempImageView->getContentSize().width/2,0));
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				
				x += m_spaceWidth;

				//创建label
				UILabel * _label = UILabel::create();
				_label->setSize(CCSizeMake(tempImageView->getContentSize().width,0));
				_label->setText((const char *)allTextNames[i]);
				_label->setFontSize(fontSize);
				_label->setAnchorPoint(ccp(0.5f,0.5f));
				_label->setPosition(ccp(0,_Button->getContentSize().height/2));
				_label->setName("Label_des");
				_Button->addChild(_label);
				//创建label
				/*UILabelBMFont* labelBMFont = UILabelBMFont::create();
				labelBMFont->setFntFile("fonts/mine.fnt");
				labelBMFont->setText((const char *)allTextNames[i]);
				labelBMFont->setAnchorPoint(ccp(0.5f,0.5f));
				labelBMFont->setPosition(ccp(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_Button->addChild(labelBMFont);*/
				/*UIImageView * imageView = UIImageView::create();
				imageView->setTexture((const char *)allTextNames[i]);
				imageView->setAnchorPoint(ccp(0.5f,0.5f));
				imageView->setPosition(ccp(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_Button->addChild(imageView);*/

			}
			else if (mode == VERTICAL)
			{
				curTabMode = VERTICAL;

				//创建label
				UILabel * _label = UILabel::create();
				if (tempImageView->getContentSize().width < tempImageView->getContentSize().height)
				{
					_label->setTextAreaSize(CCSizeMake(25,0));
					_label->setTextHorizontalAlignment(kCCTextAlignmentCenter);
					_label->setTextVerticalAlignment(kCCVerticalTextAlignmentCenter);
				}
				_label->setText((const char *)allTextNames[i]);
				_label->setFontSize(fontSize);
				_label->setAnchorPoint(ccp(0.5f,0.5f));
				_label->setPosition(ccp(0,_Button->getContentSize().height/2));
				_label->setName("Label_des");
				_Button->addChild(_label);

				if (tempImageView->getContentSize().width > tempImageView->getContentSize().height)
				{
					_Button->setAnchorPoint(ccp(0.5f,0.5f));
					_Button->setPosition(ccp(tempImageView->getContentSize().width/2,-(i+1)*tempImageView->getContentSize().height+tempImageView->getContentSize().height/2-y));
					_label->setAnchorPoint(ccp(0.5f,0.5f));
					_label->setPosition(ccp(0,0));
				}
				else
				{
					_Button->setAnchorPoint(ccp(0.f,0.5f));
					_Button->setPosition(ccp(0,-(i+1)*tempImageView->getContentSize().height+tempImageView->getContentSize().height/2-y));	
					_label->setAnchorPoint(ccp(0.5f,0.5f));
					_label->setPosition(ccp(_Button->getContentSize().width/2,0));
				}
				
				//_Button->setAnchorPoint(ccp(0.5f,0));
				//_Button->setPosition(ccp(tempImageView->getContentSize().width/2,-(i+1)*tempImageView->getContentSize().height-y));

				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				y += m_spaceHeight;

			}
			else if (mode == VERTICAL_LIST)
			{
				curTabMode = VERTICAL_LIST;
				_Button->setAnchorPoint(ccp(0,1));
				_Button->setPosition(ccp(0,0));
				int aabb = -i*tempImageView->getContentSize().height-y;
				//CCLog("%d",aabb);
				const int OFFSET_Y = 10;
				const int INTERVAL_Y = 3;
				CCPoint position = ccp(0,-i * (tempImageView->getContentSize().height + INTERVAL_Y) - OFFSET_Y - y);
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				y += m_spaceHeight;

				//创建label
				UILabel * _label = UILabel::create();
				_label->setSize(CCSizeMake(tempImageView->getContentSize().width,0));
				_label->setText((const char *)allTextNames[i]);
				_label->setFontSize(fontSize);
				_label->setAnchorPoint(ccp(0.5f,0.5f));
				_label->setPosition(ccp(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_label->setName("Label_des");
				_Button->addChild(_label);
				/*
				//创建label
				UILabelBMFont* labelBMFont = UILabelBMFont::create();
				labelBMFont->setFntFile("res_ui/font/mine.fnt");
				labelBMFont->setText((const char *)a_allNames[i]);
				labelBMFont->setAnchorPoint(ccp(0.5f,0.5f));
				labelBMFont->setPosition(ccp(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_Button->addChild(labelBMFont);
				*/
				/*
				UIImageView * imageView = UIImageView::create();
				imageView->setTexture((const char *)allTextNames[i]);
				imageView->setAnchorPoint(ccp(0.5f,0.5f));
				imageView->setPosition(ccp(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_Button->addChild(imageView);*/

				// effect 1: droping
				//CCFiniteTimeAction*  action = CCSequence::create(
				//	CCDelayTime::create((maxcount-i-1)*0.03f),
				//	CCMoveTo::create(0.03*i,position),
				//	NULL);
				// effect 2: scrolling
				CCAction * action = CCMoveTo::create(0.05+0.03*i,position);
				_Button->runAction(action);
			}

			//_Button->setTouchPriority(0);
			_Button->addReleaseEvent(this,coco_releaseselector(UITab::touchEndedEvent));
			_Button->setTouchEnable(true);

			this->addChild(_Button);
		}
		
		CCLog("this.contenetSize = %d,%d",this->getContentSize().width,this->getContentSize().height);
		m_nMaxCount = maxcount;
		//this->setTouchEnable(true);
		return true;
	}
	return false;
	
}


UITab * UITab::createWithBMFont(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],char * fontName,TabMode mode,int space)
{
	UITab * tab = new UITab();
	if (tab && tab->initWithBMFont(maxcount,normalImage,selectedImage,disabledImage,allTextNames,fontName,mode,space))
	{
		tab->autorelease();
		return tab;
	}
	CC_SAFE_DELETE(tab);
	return NULL;
}

bool UITab::initWithBMFont(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],char * fontName,TabMode mode,int space)
{
	if (UIWidget::init())
	{
		this->normalImagePath= (char *)normalImage;
		if (mode == HORIZONTAL)
		{
			this->m_spaceWidth = space;
		}
		else if (mode == VERTICAL || mode == VERTICAL_LIST)
		{
			this->m_spaceHeight = space;
		}
		this->currentTabMod = mode;
		
		int x = 0;
		int y = 0;
		int aaa = maxcount;
		UIImageView * tempImageView = UIImageView::create();
		tempImageView->setTexture(normalImage);
		for (int i = 0;i<maxcount;++i)
		{
			UIButton * _Button = UIButton::create();
			_Button->setNormalTexture(normalImage);
			_Button->setPressedTexture(selectedImage);
			_Button->setDisabledTexture(disabledImage);
			_Button->setTag(TABBUTTONBASETAG+i);
			//_Button->setPressedActionEnabled(true);
			if (mode == HORIZONTAL)
			{
				curTabMode = HORIZONTAL;
				_Button->setAnchorPoint(ccp(0.5f,0));
				//item->setPosition(x, 0);
				_Button->setPosition(ccp(i*tempImageView->getContentSize().width+x+tempImageView->getContentSize().width/2,0));
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				
				x += m_spaceWidth;
				
				//创建label
				UILabelBMFont* labelBMFont = UILabelBMFont::create();
				labelBMFont->setFntFile(fontName);
				labelBMFont->setText((const char *)allTextNames[i]);
				labelBMFont->setAnchorPoint(ccp(0.5f,0));
				labelBMFont->setPosition(ccp(0,(_Button->getContentSize().height-labelBMFont->getContentSize().height)/2));
				_Button->addChild(labelBMFont);

			}
			else if (mode == VERTICAL)
			{
				curTabMode = VERTICAL;

				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				y += m_spaceHeight;
				
				//创建label
				UILabelBMFont* labelBMFont = UILabelBMFont::create();
				labelBMFont->setFntFile(fontName);
				labelBMFont->setText((const char *)allTextNames[i]);
				_Button->addChild(labelBMFont);

				if (_Button->getContentSize().width>_Button->getContentSize().height)
				{
					_Button->setAnchorPoint(ccp(0.5f,0.5f));
					_Button->setPosition(ccp(tempImageView->getContentSize().width/2,-(i+1)*tempImageView->getContentSize().height+tempImageView->getContentSize().height/2-y));
					labelBMFont->setAnchorPoint(ccp(0.5f,0.5f));
					labelBMFont->setPosition(ccp(0,0));
				}
				else
				{
					_Button->setAnchorPoint(ccp(0,0.5f));
					_Button->setPosition(ccp(0,-(i+1)*tempImageView->getContentSize().height+tempImageView->getContentSize().height/2-y));	
					labelBMFont->setAnchorPoint(ccp(0,0.5f));
					labelBMFont->setPosition(ccp((_Button->getContentSize().width-labelBMFont->getContentSize().width)/2,0));
				}

			}
			else if (mode == VERTICAL_LIST)
			{
				curTabMode = VERTICAL_LIST;
				_Button->setAnchorPoint(ccp(0,1));
				_Button->setPosition(ccp(0,0));
				int aabb = -i*tempImageView->getContentSize().height-y;
				CCLog("%d",aabb);
				CCPoint	position = ccp(0,-i*tempImageView->getContentSize().height-y);
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				y += m_spaceHeight;
				
				//创建label
				UILabelBMFont* labelBMFont = UILabelBMFont::create();
				labelBMFont->setFntFile(fontName);
				labelBMFont->setText((const char *)allTextNames[i]);
				labelBMFont->setAnchorPoint(ccp(0.5f,0.5f));
				labelBMFont->setPosition(ccp(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_Button->addChild(labelBMFont);
				
// 				UIImageView * imageView = UIImageView::create();
// 				imageView->setTexture((const char *)allImagePath[i]);
// 				imageView->setAnchorPoint(ccp(0.5f,0.5f));
// 				imageView->setPosition(ccp(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
// 				_Button->addChild(imageView);

				CCAction * action = CCMoveTo::create(0.12+0.05*i,position);
				_Button->runAction(action);
			}

			//_Button->setTouchPriority(0);
			_Button->addReleaseEvent(this,coco_releaseselector(UITab::touchEndedEvent));
			_Button->setTouchEnable(true);

			this->addChild(_Button);
		}

		m_nMaxCount = maxcount;

		this->setTouchEnable(true);
		CCLog("this.contenetSize = %d,%d",this->getContentSize().width,this->getContentSize().height);
		return true;
	}
	return false;
	
}



int UITab::getCurrentIndex()
{
	return this->m_currentIndex;
}

void UITab::setCurrentIndex(int tempIndex)
{
	this->m_currentIndex = tempIndex;
}

void UITab::setHighLightImage(char *s)
{
	this->highLightImagePath = s;
}

void UITab::setNormalImage(char *s)
{
	this->normalImagePath = s;
}

void UITab::setDefaultPanelByIndex(int index)
{
	this->m_currentIndex = index;
	changeToHeightLightByIndex(this->m_currentIndex);
}

void UITab::indexChangedEvent(CCObject* pSender)
{
	//CCLog("IndexChangedEvent");
	//CCMenuItemImage * itemImage = dynamic_cast<CCMenuItemImage*>(pSender);
	//int tagindex = itemImage->getTag();
	const char * index = ((UIButton*)pSender)->getName();
	int tagindex = atoi(index);
	if (this->m_currentIndex != tagindex)
	{
		setCurrentIndex(tagindex);
		changeToHeightLight((UIButton*)pSender);
	}

	if (m_pIndexListener && m_pfnIndexSelector)
    {
        (m_pIndexListener->*m_pfnIndexSelector)(this);
    }


}

void UITab::addIndexChangedEvent(CCObject* pSender, SEL_CallFuncO selector)
 {
	m_pIndexListener = pSender;
	m_pfnIndexSelector = selector;
 }

void UITab::changeToHeightLight(CCObject* pSender)
{
	for (int i = 0;i<this->m_nMaxCount;i++)
	{
		char s[5];
		//itoa(i,s,10);
		sprintf(s,"%d",i);
		UIButton * _tempButton = dynamic_cast<UIButton*>(this->getChildByName(s));
		_tempButton->setNormalTexture(normalImagePath);
		_tempButton->setPressedTexture(normalImagePath);
	}
	//高亮显示
	if (this->highLightImagePath)
	{
		UIButton * _tempButton = dynamic_cast<UIButton*>(pSender);
		_tempButton->setNormalTexture(highLightImagePath);
		_tempButton->setPressedTexture(highLightImagePath);

		if (this->currentTabMod ==VERTICAL_LIST)
		{
			//this->removeFromParentAndCleanup(true);
		}
	}
	else
	{
	}

	//CCMenuItemImage * tempitemImage = dynamic_cast<CCMenuItemImage*>(this->_Menu->getChildByTag(100+this->getCurrentIndex()));
	//CCLabelTTF* label = dynamic_cast<CCLabelTTF*>(tempitemImage->getChildByTag(100+this->getCurrentIndex()));
	//label->setPosition(ccp(tempitemImage->getContentSize().width/2,tempitemImage->getContentSize().height/2));
}

void UITab::changeToHeightLightByIndex(int index)
{
	for (int i = 0;i<this->m_nMaxCount;i++)
	{
		char s[5];
		//itoa(i,s,10);
		sprintf(s,"%d",i);
		UIButton * _tempButton = dynamic_cast<UIButton*>(this->getChildByName(s));
		_tempButton->setNormalTexture(normalImagePath);
		_tempButton->setPressedTexture(normalImagePath);
	}
	//高亮显示
	if (this->highLightImagePath)
	{
		char s[5];
		//itoa(index,s,10);
		sprintf(s,"%d",index);
		UIButton * _tempButton = dynamic_cast<UIButton*>(this->getChildByName(s));
		_tempButton->setNormalTexture(highLightImagePath);
		_tempButton->setPressedTexture(highLightImagePath);
	}
	else
	{
	}
}

void UITab::touchBeganEvent(CCObject *pSender)
{
	
}
void UITab::touchMovedEvent(CCObject *pSender)
{

}
void UITab::touchEndedEvent(CCObject *pSender)
{	
	this->indexChangedEvent(pSender);
}

bool UITab::onTouchBegan( const CCPoint &touchPoint )
{
	//add by yang jun 2013.10.21
	return false;
}

void UITab::onTouchMoved( const CCPoint &touchPoint )
{

}

void UITab::onTouchEnded( const CCPoint &touchPoint )
{

}

void UITab::onTouchCancelled( const CCPoint &touchPoint )
{

}

void UITab::didNotSelectSelf()
{
	if (isAutoClose)
	{
		this->removeAllChildrenAndCleanUp(false);
		//this->setVisible(false);
	}
}

void UITab::setAutoClose(bool _isAutoClose )
{
	this->isAutoClose = _isAutoClose;
}

void UITab::setPressedActionEnabled( bool visible )
{
	ccArray * childrenArray = this->getChildren()->data;
	int length = childrenArray->num;
	for (int i = 0;i<length;++i)
	{
		UIButton * child =dynamic_cast<UIButton*>(childrenArray->arr[i]);
		if (child != NULL)
		{
			child->setPressedActionEnabled(visible);
		}
	}
}

UIButton * UITab::getObjectByIndex( int index )
{
	UIButton * button = (UIButton *)this->getChildByTag(TABBUTTONBASETAG+index);
	return button;
}

void UITab::setHightLightLabelColor( ccColor3B cl )
{
	for (int i = 0;i<m_nMaxCount;++i)
	{
		UIButton* temp = (UIButton*)this->getObjectByIndex(i);
		UILabel * temp_lb = dynamic_cast<UILabel*>(temp->getChildByName("Label_des"));
		if (temp_lb)
		{
			if (i == m_currentIndex)
			{
				temp_lb->setColor(cl);
			}
		}
	}
}

void UITab::setNormalLabelColor( ccColor3B cl )
{
	for (int i = 0;i<m_nMaxCount;++i)
	{
		UIButton* temp = (UIButton*)this->getObjectByIndex(i);
		UILabel * temp_lb = dynamic_cast<UILabel*>(temp->getChildByName("Label_des"));
		if (temp_lb)
		{
			if (i != m_currentIndex)
			{
				temp_lb->setColor(cl);
			}
		}
	}
}
