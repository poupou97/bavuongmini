
#ifndef _EXTENSIONS_SHOWSYSTEMINFO_H_
#define _EXTENSIONS_SHOWSYSTEMINFO_H_

#include "UIScene.h"
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ShowSystemInfo : public UIScene
{
public:
	ShowSystemInfo();
	~ShowSystemInfo();

	static ShowSystemInfo * create(const char * str_info,float sizeScale = 1.0f);
	bool init(const char * str_info,float sizeScale);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

};

#endif

