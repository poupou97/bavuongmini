#include "PopupWindow.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "GameView.h"
#include "AppMacros.h"
#include "CCRichLabel.h"

PopupWindow::PopupWindow():
senderListener(NULL),
senderSelectorSure(NULL),
senderSelectorCancel(NULL)
{
}

PopupWindow::~PopupWindow()
{
}

PopupWindow * PopupWindow::create(std::string string,int buttonNum,PopWindowType winType, int remainTime)
{
	PopupWindow * dialog = new PopupWindow();
	if(dialog && dialog->init(string, buttonNum,winType,remainTime))
	{
		dialog->autorelease();
		return dialog;
	}
	CC_SAFE_DELETE(dialog);
	return NULL;
}

bool PopupWindow::init(std::string string,int buttonNum,PopWindowType winType, int remainTime)
{
	if (UIScene::init())
	{
		curWindowType = winType;
		m_nRemainTime = remainTime;

		CCSize winsize = CCSizeMake(800,480);

		CCScale9Sprite * backGroundSp = CCScale9Sprite::create("res_ui/dikuang_new.png");
		backGroundSp->setCapInsets(CCRect(0,0,0,0));
		backGroundSp->setPreferredSize(CCSizeMake(377,201));
		backGroundSp->setAnchorPoint(ccp(0.5f,0.5f));
		backGroundSp->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addChild(backGroundSp);

		CCScale9Sprite * backGroundSp_frame = CCScale9Sprite::create("res_ui/LV4_didia.png");
		backGroundSp_frame->setCapInsets(CCRect(13,14,1,1));
		backGroundSp_frame->setPreferredSize(CCSizeMake(300,108));
		backGroundSp_frame->setAnchorPoint(ccp(0.5f,1.0f));
		backGroundSp_frame->setPosition(ccp(backGroundSp->getContentSize().width/2 - 3 ,180));
		backGroundSp->addChild(backGroundSp_frame);

		CCRichLabel *labelLink = CCRichLabel::createWithString(string.c_str(),CCSizeMake(290,50),NULL,NULL,0,18,2);
		labelLink->setAnchorPoint(ccp(0,1));
		labelLink->setPosition(ccp(45,backGroundSp->getContentSize().height-30));
		backGroundSp->addChild(labelLink);

		UILayer * layerbutton = UILayer::create();
		layerbutton->ignoreAnchorPointForPosition(false);
		layerbutton->setAnchorPoint(ccp(0.5f,0.5f));
		layerbutton->setPosition(ccp(winsize.width/2,winsize.height/2));
		layerbutton->setContentSize(winsize);
		addChild(layerbutton);

		if (buttonNum == 2)
		{
			buttonSure = UIButton::create();
			buttonSure->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
			buttonSure->setTouchEnable(true);
			buttonSure->setPressedActionEnabled(true);
			buttonSure->addReleaseEvent(this,coco_releaseselector(PopupWindow::sureEvent));
			buttonSure->setAnchorPoint(ccp(0.5f,0.5f));
			buttonSure->setScale9Enable(true);
			buttonSure->setScale9Size(CCSizeMake(110,43));
			buttonSure->setCapInsets(CCRect(18,9,2,23));
			buttonSure->setPosition(ccp(layerbutton->getContentSize().width/2-87,layerbutton->getContentSize().height/2 - backGroundSp->getContentSize().height/2 +48));
			layerbutton->addWidget(buttonSure);

			UILabel * btn_labelSure= UILabel::create();
			btn_labelSure->setAnchorPoint(ccp(0.5f,0.5f));
			btn_labelSure->setPosition(ccp(0,0));
			btn_labelSure->setFontSize(18);
			btn_labelSure->setText(StringDataManager::getString("popupWindow_sure"));
			buttonSure->addChild(btn_labelSure);

			buttonCancel = UIButton::create();
			buttonCancel->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
			buttonCancel->setTouchEnable(true);
			buttonCancel->setPressedActionEnabled(true);
			buttonCancel->addReleaseEvent(this,coco_releaseselector(PopupWindow::cancelEvent));
			buttonCancel->setAnchorPoint(ccp(0.5f,0.5f));
			buttonCancel->setScale9Enable(true);
			buttonCancel->setScale9Size(CCSizeMake(110,43));
			buttonCancel->setCapInsets(CCRect(18,9,2,23));
			buttonCancel->setPosition(ccp(layerbutton->getContentSize().width/2+82,buttonSure->getPosition().y));
			layerbutton->addWidget(buttonCancel);

			UILabel * btn_labelCancel= UILabel::create();
			btn_labelCancel->setAnchorPoint(ccp(0.5f,0.5f));
			btn_labelCancel->setPosition(ccp(0,0));
			btn_labelCancel->setFontSize(18);
			btn_labelCancel->setText(StringDataManager::getString("popupWindow_cancel"));
			buttonCancel->addChild(btn_labelCancel);
		}else
		{
			buttonSure = UIButton::create();
			buttonSure->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
			buttonSure->setTouchEnable(true);
			buttonSure->setPressedActionEnabled(true);
			buttonSure->addReleaseEvent(this,coco_releaseselector(PopupWindow::sureEvent));
			buttonSure->setAnchorPoint(ccp(0.5f,0.5f));
			buttonSure->setScale9Enable(true);
			buttonSure->setScale9Size(CCSizeMake(110,43));
			buttonSure->setCapInsets(CCRect(18,9,2,23));
			buttonSure->setPosition(ccp(layerbutton->getContentSize().width/2,layerbutton->getContentSize().height/2 - backGroundSp->getContentSize().height/2 +48));
			layerbutton->addWidget(buttonSure);

			UILabel * btn_labelSure= UILabel::create();
			btn_labelSure->setAnchorPoint(ccp(0.5f,0.5f));
			btn_labelSure->setPosition(ccp(0,0));
			btn_labelSure->setFontSize(18);
			btn_labelSure->setText(StringDataManager::getString("con_reconnection_text"));
			buttonSure->addChild(btn_labelSure);
		}

		if (winType == KtypeNeverRemove)
		{
			
		}
		else if (winType = KTypeRemoveByTime)
		{
			std::string str_remainTime = "(";
			char s_remainTime[10];
			sprintf(s_remainTime,"%d",m_nRemainTime);
			str_remainTime.append(s_remainTime);
			str_remainTime.append(")");
			
			centerTimeLabel = CCLabelTTF::create(str_remainTime.c_str(), APP_FONT_NAME, 20);
			centerTimeLabel->setAnchorPoint(ccp(0.5f,0.5f));
			centerTimeLabel->setPosition(ccp(backGroundSp->getContentSize().width/2 - 3,backGroundSp->getContentSize().height/2 - 10));
			backGroundSp->addChild(centerTimeLabel);
			this->schedule(schedule_selector(PopupWindow::update),1.0f);
		}

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setPosition(CCPointZero);
		this->setContentSize(winsize);
		this->setTouchPriority(TPriority_PopUpWindow);
		m_pUiLayer->setTouchPriority(TPriority_PopUpWindow-1);
		layerbutton->setTouchPriority(TPriority_PopUpWindow - 2);

		return true;
	}
	return false;
}

void PopupWindow::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void PopupWindow::onExit()
{
	UIScene::onExit();
}

void PopupWindow::AddcallBackEvent( CCObject* pSender, SEL_CallFuncO pSelectorSure,SEL_CallFuncO pSelectorCancel )
{
	senderListener = pSender;
	senderSelectorSure = pSelectorSure;
	senderSelectorCancel =pSelectorCancel;
}

void PopupWindow::sureEvent( CCObject * pSender )
{
	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	if (senderListener && senderSelectorSure)
	{
		(senderListener->*senderSelectorSure)(this);
	}
	this->closeAnim();
}

void PopupWindow::cancelEvent( CCObject * obj )
{
	if (senderListener && senderSelectorCancel)
	{
		(senderListener->*senderSelectorCancel)(this);
	}
	this->closeAnim();
}

bool PopupWindow::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	return true;
}
void PopupWindow::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void PopupWindow::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void PopupWindow::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}

void PopupWindow::update( float delta )
{
	if (curWindowType == KtypeNeverRemove)
		return;

	if (curWindowType == KTypeRemoveByTime)
	{
		m_nRemainTime--;
		if (m_nRemainTime <= 0)
		{
			this->closeAnim();
		}

		std::string str_remainTime = "(";
		char s_remainTime[10];
		sprintf(s_remainTime,"%d",m_nRemainTime);
		str_remainTime.append(s_remainTime);
		str_remainTime.append(")");

		centerTimeLabel->setString(str_remainTime.c_str());
	}
}

void PopupWindow::setButtonPosition()
{
	CCPoint p1 = buttonSure->getPosition();
	CCPoint p2 = buttonCancel->getPosition();

	buttonSure->setPosition(p2);
	buttonCancel->setPosition(p1);
}

void PopupWindow::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void PopupWindow::addCCTutorialIndicator( const char* content,CCPoint pos,CCTutorialIndicator::Direction direction )
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;

	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,115,45,true);
	tutorialIndicator->setDrawNodePos(ccp(_w+400 - 85,_h+240 - 51));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void PopupWindow::removeCCTutorialIndicator()
{
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}
