#include "ShowSystemInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"

#define DEFAULT_SIZE_WIDTH 326
#define DEFAULT_SIZE_HEIGHT 276

ShowSystemInfo::ShowSystemInfo()
{
}


ShowSystemInfo::~ShowSystemInfo()
{
}

ShowSystemInfo * ShowSystemInfo::create( const char * str_info,float sizeScale)
{
	ShowSystemInfo * showSystemInfo = new ShowSystemInfo();
	if (showSystemInfo && showSystemInfo->init(str_info,sizeScale))
	{
		showSystemInfo->autorelease();
		return showSystemInfo;
	}
	CC_SAFE_DELETE(showSystemInfo);
	return NULL;
}

bool ShowSystemInfo::init( const char * str_info,float sizeScale )
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		//����UI
// 		if(LoadSceneLayer::StrategiesDetailInfoPopUpLayer->getWidgetParent() != NULL)
// 		{
// 			LoadSceneLayer::StrategiesDetailInfoPopUpLayer->removeFromParentAndCleanup(false);
// 		}
// 		UIPanel *ppanel = LoadSceneLayer::StrategiesDetailInfoPopUpLayer;
// 
// 		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
// 		ppanel->getValidNode()->setContentSize(CCSizeMake(326, 276));
// 		ppanel->setPosition(CCPointZero);
// 		ppanel->setTouchEnable(true);
// 		ppanel->setScale(1.5f);
// 		m_pUiLayer->addWidget(ppanel);
// 		
		UIImageView * imageView_frame_mo = UIImageView::create();
		imageView_frame_mo->setTexture("res_ui/shadow_4.png");
		imageView_frame_mo->setScale9Enable(true);
		imageView_frame_mo->setScale9Size(CCSizeMake(DEFAULT_SIZE_WIDTH*sizeScale,DEFAULT_SIZE_HEIGHT*sizeScale));
		imageView_frame_mo->setCapInsets(CCRectMake(30,30,1,1));
		imageView_frame_mo->setAnchorPoint(ccp(.5f,.5f));
		imageView_frame_mo->setPosition(ccp(DEFAULT_SIZE_WIDTH*sizeScale/2,DEFAULT_SIZE_HEIGHT*sizeScale/2));
		m_pUiLayer->addWidget(imageView_frame_mo);

		UIImageView * imageView_frame = UIImageView::create();
		imageView_frame->setTexture("res_ui/dibian_2.png");
		imageView_frame->setScale9Enable(true);
		imageView_frame->setScale9Size(CCSizeMake(296*sizeScale,248*sizeScale));
		imageView_frame->setCapInsets(CCRectMake(19,19,1,1));
		imageView_frame->setAnchorPoint(ccp(.5f,.5f));
		imageView_frame->setPosition(ccp(DEFAULT_SIZE_WIDTH*sizeScale/2,DEFAULT_SIZE_HEIGHT*sizeScale/2));
		m_pUiLayer->addWidget(imageView_frame);

// 		UIImageView * imageView_frame_adorn = UIImageView::create();
// 		imageView_frame_adorn->setTexture("res_ui/tietu_left.png");
// 		imageView_frame_adorn->setAnchorPoint(ccp(.5f,.5f));
// 		imageView_frame_adorn->setPosition(ccp(141,113));
// 		m_pUiLayer->addWidget(imageView_frame_adorn);

		UILayer *u_layer = UILayer::create();
		addChild(u_layer);

		//���
		CCLabelTTF * l_des = CCLabelTTF::create(str_info,APP_FONT_NAME,16,CCSizeMake(255*sizeScale, 0 ), kCCTextAlignmentLeft);
		//l_des->setColor(ccc3(47,93,13));

		int scroll_height = l_des->getContentSize().height;
		CCScrollView * m_scrollView = CCScrollView::create(CCSizeMake(300*sizeScale,216*sizeScale));
		m_scrollView->setViewSize(CCSizeMake(300*sizeScale, 216*sizeScale));
		m_scrollView->ignoreAnchorPointForPosition(false);
		m_scrollView->setTouchEnabled(true);
		m_scrollView->setDirection(kCCScrollViewDirectionVertical);
		m_scrollView->setAnchorPoint(ccp(0,0));
		m_scrollView->setPosition(ccp(12*sizeScale,32*sizeScale));
		m_scrollView->setBounceable(true);
		m_scrollView->setClippingToBounds(true);
		u_layer->addChild(m_scrollView);

		if (scroll_height > 216*sizeScale)
		{
			m_scrollView->setContentSize(CCSizeMake(300*sizeScale,scroll_height));
			m_scrollView->setContentOffset(ccp(0,216*sizeScale-scroll_height));  
		}
		else
		{
			m_scrollView->setContentSize(ccp(300*sizeScale,216*sizeScale));
		}
		m_scrollView->setClippingToBounds(true);

		m_scrollView->addChild(l_des);
		l_des->setPosition(ccp(23*sizeScale,(m_scrollView->getContentSize().height - scroll_height + 0)));

		this->setContentSize(CCSizeMake(DEFAULT_SIZE_WIDTH*sizeScale,DEFAULT_SIZE_HEIGHT*sizeScale));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}

void ShowSystemInfo::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ShowSystemInfo::onExit()
{
	UIScene::onExit();
}

bool ShowSystemInfo::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return resignFirstResponder(touch,this,false);
}

void ShowSystemInfo::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void ShowSystemInfo::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void ShowSystemInfo::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}
