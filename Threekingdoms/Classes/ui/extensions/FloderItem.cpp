#include "FloderItem.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../Auction_ui/AuctionUi.h"
#include "../Mail_ui/MailUI.h"


FloderItemInstance::FloderItemInstance()
{
}

FloderItemInstance::~FloderItemInstance()
{
	delete m_floder;
}

FloderItemInstance * FloderItemInstance::create( FolderInfo* folder,int type,int amount /*= 0*/ )
{
	FloderItemInstance * floder_ = new FloderItemInstance();
	if (floder_ && floder_->init(folder,type,amount))
	{
		floder_->autorelease();
		return floder_;
	}
	CC_SAFE_DELETE(floder_);
	return NULL;
}

bool FloderItemInstance::init( FolderInfo* folder,int type,int amount )
{
	if (UIScene::init())
	{
		m_floder = new FolderInfo();
		m_floder->CopyFrom(*folder);

		m_type = type;
		setFloderIndex(folder->id());

		std::string frameIconPath = goodsSopbgIcon(m_floder->goods().quality());
		UIButton * Btn_pacItemFrame = UIButton::create();
		Btn_pacItemFrame->setTextures(frameIconPath.c_str(),frameIconPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnable(true);
		Btn_pacItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(ccp(31,31));

		switch(type)
		{
		case  ktype_auction:
			{
				Btn_pacItemFrame->addReleaseEvent(this,coco_releaseselector(FloderItemInstance::callBackShowAuctionItem));
			}break;
		case 2:
			{
				Btn_pacItemFrame->addReleaseEvent(this,coco_releaseselector(FloderItemInstance::callBackShowMailItem));
			}break;
		case 3:
			{
				//Btn_pacItemFrame->addReleaseEvent(this,coco_releaseselector(FloderItem::showGoodsInfo));
			}break;
		}

		m_pUiLayer->addWidget(Btn_pacItemFrame);

		std::string goodsInfoStr ="res_ui/props_icon/";
		goodsInfoStr.append(m_floder->goods().icon());
		goodsInfoStr.append(".png");

		UIImageView *equipIcon_ =UIImageView::create();
		equipIcon_->setTexture(goodsInfoStr.c_str());
		equipIcon_->setAnchorPoint(ccp(0.5f,0.5f));
		equipIcon_->setPosition(ccp(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		if (m_floder->goods().equipmentclazz() > 0 && m_floder->goods().equipmentclazz() < 11 )
		{
			int starLv_ = m_floder->goods().equipmentdetail().starlevel();
			if (starLv_>0)
			{
				char starLvLabel_[5];
				sprintf(starLvLabel_,"%d",starLv_);
				UILabel * label_ =UILabel::create();
				label_->setAnchorPoint(ccp(0,0));
				label_->setPosition(ccp(-25,-20));
				label_->setFontSize(13);
				label_->setText(starLvLabel_);
				Btn_pacItemFrame->addChild(label_);

				UIImageView * imageStar_ =UIImageView::create();
				imageStar_->setTexture("res_ui/star_on.png");
				imageStar_->setScale(0.5f);
				imageStar_->setAnchorPoint(ccp(0,0));
				imageStar_->setPosition(ccp(label_->getContentSize().width-27,label_->getPosition().y));
				Btn_pacItemFrame->addChild(imageStar_);
			}

			int strengthLv_ = m_floder->goods().equipmentdetail().gradelevel();
			if (strengthLv_>0)
			{
				char strengthLv[5];
				sprintf(strengthLv,"%d",strengthLv_);
				std::string strengthStr_ = "+";
				strengthStr_.append(strengthLv);

				UILabel * label_StrengthLv = UILabel::create();
				label_StrengthLv->setAnchorPoint(ccp(1,1));
				label_StrengthLv->setPosition(ccp(23,20));
				label_StrengthLv->setText(strengthStr_.c_str());
				label_StrengthLv->setFontSize(13);
				Btn_pacItemFrame->addChild(label_StrengthLv);
			}
		}

		if (amount > 1)
		{
			char amountStr[10];
			sprintf(amountStr,"%d",amount);
			UILabel * label_amount = UILabel::create();
			label_amount->setAnchorPoint(ccp(1,1));
			label_amount->setPosition(ccp(20,-10));
			label_amount->setText(amountStr);
			label_amount->setFontSize(13);
			Btn_pacItemFrame->addChild(label_amount);
		}
		
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(Btn_pacItemFrame->getContentSize());
		return true;
	}
	return false;
}

void FloderItemInstance::onEnter()
{
	UIScene::onEnter();
}

void FloderItemInstance::onExit()
{
	UIScene::onExit();
}

std::string FloderItemInstance::goodsSopbgIcon( int quality_ )
{
	std::string frameColorPath = "res_ui/";

	switch(quality_)
	{
	case 1:
		{
			frameColorPath.append("sdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("sdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("sdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("sdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("sdi_orange");
		}break;
	default:
		{
			frameColorPath.append("sdi_white");
		}
	}
	frameColorPath.append(".png");

	return frameColorPath;
}

bool FloderItemInstance::ccTouchBegan( CCTouch * pTouch,CCEvent * pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}

void FloderItemInstance::callBackShowAuctionItem( CCObject * obj )
{
	AuctionUi * auctionui = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	if (auctionui != NULL)
	{
		auctionui->getbackAuction(NULL);
	}

	/*
	GoodsInfo * m_goods = new GoodsInfo();
	m_goods->CopyFrom(m_floder->goods());

	FloderItemInfo * floder_info =FloderItemInfo::create(m_goods,m_type);
	floder_info->ignoreAnchorPointForPosition(false);
	floder_info->setAnchorPoint(ccp(0.5f,0.5f));
	GameView::getInstance()->getMainUIScene()->addChild(floder_info);

	delete m_goods;
	*/
}

void FloderItemInstance::callBackShowMailItem( CCObject * obj )
{
	MailUI *mail_ui = (MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
	if (mail_ui != NULL)
	{
		if (this->getTag() == REDUCTIONANNEXBUTTON1)
		{
			mail_ui->reductionAnnex1(obj);
		}else
		{
			mail_ui->reductionAnnex2(obj);
		}
	}

	/*
	GoodsInfo * m_goods = new GoodsInfo();
	m_goods->CopyFrom(m_floder->goods());

	FloderItemInfo * floder_info =FloderItemInfo::create(m_goods,m_type);
	floder_info->ignoreAnchorPointForPosition(false);
	floder_info->setAnchorPoint(ccp(0.5f,0.5f));
	floder_info->setTag(this->getTag());
	GameView::getInstance()->getMainUIScene()->addChild(floder_info);

	delete m_goods;
	*/
}

int FloderItemInstance::getFloderIndex()
{
	return m_floderIndex;
}

void FloderItemInstance::setFloderIndex( int index_ )
{
	m_floderIndex = index_;
}


//////////////////////////////////////
GoodsItemInstance::GoodsItemInstance()
{
}

GoodsItemInstance::~GoodsItemInstance()
{
}

GoodsItemInstance * GoodsItemInstance::create( GoodsInfo * goods,int type,int amount /*= 0*/ )
{
	GoodsItemInstance * goods_ = new GoodsItemInstance();
	if (goods_ && goods_->init(goods,type,amount))
	{
		goods_->autorelease();
		return goods_;
	}
	CC_SAFE_DELETE(goods_);
	return NULL;
}

bool GoodsItemInstance::init( GoodsInfo * goods,int type,int amount )
{
	if (UIScene::init())
	{
		m_goods = new GoodsInfo();
		m_goods->CopyFrom(*goods);
		m_type = type;

		std::string frameIconPath = goodsSopbgIcon(goods->quality());
		UIButton * Btn_pacItemFrame = UIButton::create();
		Btn_pacItemFrame->setTextures(frameIconPath.c_str(),frameIconPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnable(true);
		Btn_pacItemFrame->setPressedActionEnabled(true);
		Btn_pacItemFrame->setAnchorPoint(ccp(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(ccp(31,31));

		switch(type)
		{
		case  ktype_mail_getBackAnnex:
			{
				Btn_pacItemFrame->addReleaseEvent(this,coco_releaseselector(GoodsItemInstance::callBackShowMailAnnexItem));
			}break;
		case 2:
			{
			}break;
		}

		m_pUiLayer->addWidget(Btn_pacItemFrame);

		std::string goodsInfoStr ="res_ui/props_icon/";
		goodsInfoStr.append(goods->icon());
		goodsInfoStr.append(".png");

		UIImageView *equipIcon_ =UIImageView::create();
		equipIcon_->setTexture(goodsInfoStr.c_str());
		equipIcon_->setAnchorPoint(ccp(0.5f,0.5f));
		equipIcon_->setPosition(ccp(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		if (goods->equipmentclazz() > 0 && goods->equipmentclazz() < 11 )
		{
			int starLv_ = goods->equipmentdetail().starlevel();
			if (starLv_>0)
			{
				char starLvLabel_[5];
				sprintf(starLvLabel_,"%d",starLv_);
				UILabel * label_ =UILabel::create();
				label_->setAnchorPoint(ccp(0,0));
				label_->setPosition(ccp(-25,-20));
				label_->setFontSize(13);
				label_->setText(starLvLabel_);
				Btn_pacItemFrame->addChild(label_);

				UIImageView * imageStar_ =UIImageView::create();
				imageStar_->setTexture("res_ui/star_on.png");
				imageStar_->setScale(0.5f);
				imageStar_->setAnchorPoint(ccp(0,0));
				imageStar_->setPosition(ccp(label_->getContentSize().width-27,label_->getPosition().y));
				Btn_pacItemFrame->addChild(imageStar_);
			}

			int strengthLv_ =  goods->equipmentdetail().gradelevel();
			if (strengthLv_>0)
			{
				char strengthLv[5];
				sprintf(strengthLv,"%d",strengthLv_);
				std::string strengthStr_ = "+";
				strengthStr_.append(strengthLv);

				UILabel * label_StrengthLv = UILabel::create();
				label_StrengthLv->setAnchorPoint(ccp(1,1));
				label_StrengthLv->setPosition(ccp(23,20));
				label_StrengthLv->setText(strengthStr_.c_str());
				label_StrengthLv->setFontSize(13);
				Btn_pacItemFrame->addChild(label_StrengthLv);
			}
		}

		if (amount > 1)
		{
			char amountStr[10];
			sprintf(amountStr,"%d",amount);
			UILabel * label_amount = UILabel::create();
			label_amount->setAnchorPoint(ccp(1,1));
			label_amount->setPosition(ccp(20,-10));
			label_amount->setText(amountStr);
			label_amount->setFontSize(13);
			Btn_pacItemFrame->addChild(label_amount);
		}
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(Btn_pacItemFrame->getContentSize());
		return true;
	}
	return false;
}

void GoodsItemInstance::onEnter()
{
	UIScene::onEnter();
}

void GoodsItemInstance::onExit()
{
	UIScene::onExit();
}

std::string GoodsItemInstance::goodsSopbgIcon( int quality_ )
{
	std::string frameColorPath = "res_ui/";

	switch(quality_)
	{
	case 1:
		{
			frameColorPath.append("sdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("sdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("sdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("sdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("sdi_orange");
		}break;
	default:
		{
			frameColorPath.append("sdi_white");
		}
	}
	frameColorPath.append(".png");

	return frameColorPath;
}

bool GoodsItemInstance::ccTouchBegan( CCTouch * pTouch,CCEvent * pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}


void GoodsItemInstance::callBackShowMailAnnexItem( CCObject * obj )
{
	CCSize winsize =CCDirector::sharedDirector()->getVisibleSize();
	GoodsItemInfoBase * goodsui =GoodsItemInfoBase::create(m_goods,GameView::getInstance()->EquipListItem,0);
	goodsui->ignoreAnchorPointForPosition(false);
	goodsui->setAnchorPoint(ccp(0.5f,0.5f));
	GameView::getInstance()->getMainUIScene()->addChild(goodsui);

// 
// 	FloderItemInfo * floder_info =FloderItemInfo::create(m_goods,m_type);
// 	floder_info->ignoreAnchorPointForPosition(false);
// 	floder_info->setAnchorPoint(ccp(0.5f,0.5f));
// 	GameView::getInstance()->getMainUIScene()->addChild(floder_info);
}

///////////////
FloderItemInfo::FloderItemInfo()
{
}

FloderItemInfo::~FloderItemInfo()
{
}

FloderItemInfo * FloderItemInfo::create( GoodsInfo * goods ,int type)
{
	FloderItemInfo * floderInfo =new FloderItemInfo();
	if (floderInfo && floderInfo->init(goods,type))
	{
		floderInfo->autorelease();
		return floderInfo;
	}
	CC_SAFE_DELETE(floderInfo);
	return NULL;
}

bool FloderItemInfo::init( GoodsInfo * goods ,int type)
{
	if (GoodsItemInfoBase::init(goods,GameView::getInstance()->EquipListItem,0))
	{
		const char *str_ = StringDataManager::getString("goods_auction_getback");
		UIButton * Button_getAnnes= UIButton::create();
		Button_getAnnes->setTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_getAnnes->setTouchEnable(true);
		Button_getAnnes->setPressedActionEnabled(true);
		Button_getAnnes->setAnchorPoint(ccp(0.5f,0.5f));
		Button_getAnnes->setScale9Enable(true);
		Button_getAnnes->setScale9Size(CCSizeMake(80,43));
		Button_getAnnes->setCapInsets(CCRect(18,9,2,23));
		Button_getAnnes->setPosition(ccp(135,25));

		switch(type)
		{
		case ktype_auctionItem:
			{
				Button_getAnnes->addReleaseEvent(this,coco_releaseselector(FloderItemInfo::callbackAuctionItem));
			}break;
		case ktype_mail_addAnnexItem:
			{
				Button_getAnnes->addReleaseEvent(this,coco_releaseselector(FloderItemInfo::callBackAddMailAnnex));
			}break;
		case ktype_mail_getBackAnnexItem:
			{
				//Button_getAnnes->addReleaseEvent(this,coco_releaseselector(FloderItemInfo::callBackAddMailAnnex));
			}break;
		}


		UILabel * Label_getAnnes = UILabel::create();
		Label_getAnnes->setText(str_);
		Label_getAnnes->setFontName(APP_FONT_NAME);
		Label_getAnnes->setFontSize(18);
		Label_getAnnes->setAnchorPoint(ccp(0.5f,0.5f));
		Label_getAnnes->setPosition(ccp(0,0));
		Button_getAnnes->addChild(Label_getAnnes);
		m_pUiLayer->addWidget(Button_getAnnes);

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setAnchorPoint(ccp(0,0));
		return true;
	}
	return false;
}

void FloderItemInfo::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FloderItemInfo::onExit()
{
	UIScene::onExit();
}

bool FloderItemInfo::ccTouchBegan( CCTouch * pTouch,CCEvent * pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}

void FloderItemInfo::callbackAuctionItem( CCObject * obj )
{
	AuctionUi * auctionui = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	if (auctionui != NULL)
	{
		auctionui->getbackAuction(NULL);
	}
	this->removeFromParentAndCleanup(true);
}

void FloderItemInfo::callBackAddMailAnnex( CCObject * obj )
{
	MailUI *mail_ui = (MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
	if (mail_ui != NULL)
	{
		if (this->getTag() == REDUCTIONANNEXBUTTON1)
		{
			FloderItemInstance * mailAnnexui =(FloderItemInstance *)mail_ui->sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON1);
			if (mailAnnexui != NULL)
			{
				mail_ui->reductionAnnex1(obj);
			}
		}

		if (this->getTag() == REDUCTIONANNEXBUTTON2)
		{
			FloderItemInstance * mailAnnexui =(FloderItemInstance *)mail_ui->sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON2);
			if (mailAnnexui != NULL)
			{
				mail_ui->reductionAnnex2(obj);
			}
		}
	}

	this->removeFromParentAndCleanup(true);
}

void FloderItemInfo::callBackShowSendMailAnnex( CCObject * obj )
{
	CCLOG("show mail goods annex info");
}
