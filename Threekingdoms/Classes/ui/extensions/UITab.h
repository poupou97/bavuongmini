
#ifndef _HELLOCPP_UITAB_H_
#define _HELLOCPP_UITAB_H_

#include "CocoStudio/GUI/BaseClasses/UIWidget.h"
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

typedef enum {
	HORIZONTAL,
	VERTICAL,
	VERTICAL_LIST
}TabMode;


typedef void (CCObject::*SEL_IndexChangedEvent)(CCObject*);
#define coco_indexchangedselector(_SELECTOR) (SEL_IndexChangedEvent)(&_SELECTOR)

class UITab :public UIWidget
{
public:
	UITab();
	~UITab();

	virtual bool onTouchBegan(const CCPoint &touchPoint);
	virtual void onTouchMoved(const CCPoint &touchPoint);
	virtual void onTouchEnded(const CCPoint &touchPoint);
	virtual void onTouchCancelled(const CCPoint &touchPoint);

	//virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	//virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	//virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	//virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	static UITab * createWithImage(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allImagePath[],TabMode mode,int space);
	bool initWithImage(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allImagePath[],TabMode mode,int space);

	static UITab * createWithText(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],TabMode mode,int space,int fontSize = 20);
	bool initWithText(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],TabMode mode,int space,int fontSize);

	static UITab * createWithBMFont(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],char * fontName,TabMode mode,int space);
	bool initWithBMFont(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],char * fontName,TabMode mode,int space);
	
	int getCurrentIndex();
	virtual void addIndexChangedEvent(CCObject* pSender, SEL_CallFuncO selector);
	virtual void setHighLightImage(char *s);
	virtual void setNormalImage(char *s);
	virtual void setDefaultPanelByIndex(int index);

	virtual void didNotSelectSelf();
	void setAutoClose(bool _isAutoClose);
	void setPressedActionEnabled(bool visible);
public:
    cocos2d::extension::UIButton * getObjectByIndex(int index);
	void setHightLightLabelColor(ccColor3B cl);
	void setNormalLabelColor(ccColor3B cl);
private:
	int m_currentIndex;
	int m_nMaxCount;
	char * highLightImagePath;
	char * normalImagePath;
	int m_spaceHeight;
	int m_spaceWidth;
	bool isPressedActionEnabled;
	TabMode curTabMode;
	void setCurrentIndex(int tempIndex);
	void indexChangedEvent(CCObject* pSender);
	void changeToHeightLight(CCObject* Psender);
	void changeToHeightLightByIndex(int index);
	TabMode currentTabMod;

	CCSize normalImageSize;

	CCObject* m_pIndexListener;
	SEL_PushEvent m_pfnIndexSelector;


 	void touchBeganEvent(CCObject *pSender);
 	void touchMovedEvent(CCObject *pSender);
	void touchEndedEvent(CCObject *pSender);

	bool isAutoClose;
};
#endif;

