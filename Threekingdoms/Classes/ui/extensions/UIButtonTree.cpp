#include "UIButtonTree.h"

NS_CC_EXT_BEGIN

cocos2d::extension::UIButtonTree::UIButtonTree()
{

}

cocos2d::extension::UIButtonTree::~UIButtonTree()
{

}

UIButtonTree * cocos2d::extension::UIButtonTree::create()
{
	UIButtonTree* widget = new UIButtonTree();
	if (widget && widget->init())
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}

bool cocos2d::extension::UIButtonTree::init()
{
	if(UIButton::init())
	{
		return true;
	}
	return false;
}

void cocos2d::extension::UIButtonTree::onEnter()
{
	UIButton::onEnter();
}

void cocos2d::extension::UIButtonTree::onExit()
{
	UIButton::onExit();
}


void cocos2d::extension::UIButtonTree::setInfo( int tag )
{
	m_parentTag = tag;
}

int cocos2d::extension::UIButtonTree::getInfo()
{
	return m_parentTag;
}

NS_CC_EXT_END