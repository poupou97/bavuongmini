
#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
USING_NS_CC;
USING_NS_CC_EXT;

class Counter:public UIScene
{
public:
	Counter(void);
	~Counter(void);

	static Counter * create();
	bool init(std::string test);
	virtual void onEnter();
	virtual void onExit();

	void getAmount(CCObject * obj);
	void clearNum(CCObject * obj);
	void deleteNum(CCObject * obj);
	void sendButton(CCObject * obj);
	void callBackExit(CCObject * obj);

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	int getInputNum();
	void setShowNumDefault(int counts);
	void callBackSendNum(CCObject* pSender, SEL_CallFuncO pSelector);
private:
	int sendNum_;
	CCObject * senderListener;
	SEL_PushEvent senderSelector;
private:
	//int m_num;
	std::string goodsNum;
	CCLabelTTF * showNum;
	int goodSendNum;
};

