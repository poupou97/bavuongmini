#ifndef _CUSTOM_CCRICHLABEL_RICHELEMENT_H_
#define _CUSTOM_CCRICHLABEL_RICHELEMENT_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCRichLabel;

struct TextFontDefinition {
	bool hasColorFlag;
	ccColor3B fontColor;
	// shadow
	// stroke
	// font size, etc.
};

class RichElement
{
public:
	enum ElementType
	{
		RichElementType_Sprite = 0,
		RichElementType_Button = 1,
		RichElementType_Text = 2,
		RichElementType_Return = 3,
	};

public:
	RichElement();
	virtual ~RichElement();

	virtual CCSize getSize() = 0;
	virtual void setPosition(CCPoint pos) = 0;
	virtual CCPoint getPosition() = 0;

	virtual CCNode* getNode() = 0;

	virtual void parse(const char* elementStr) = 0;

	virtual int getCharCount() = 0;

public:
	int x, y;
	int w, h;
	int color;
	std::string m_string;
	int line;   // line position, line 0, 1, or 2, etc.
	int type;

	CCRichLabel* _container;
};

class RichElementSprite : public RichElement
{
public:
	RichElementSprite();
	virtual ~RichElementSprite();

	virtual CCSize getSize();
	virtual void setPosition(CCPoint pos);
	virtual CCPoint getPosition();
	virtual void parse(const char* elementStr);

	virtual CCNode* getNode();

	void animate();

	virtual int getCharCount();

private:
	CCSprite* m_sprite;
	CCAnimation *m_animation;
};

////////////////////////////////////////////////////////

#define RICHELEMENT_ITEM_KEYWORD "propid"
#define RICHELEMENT_ITEM_SEPARATOR ";"

struct structPropIds {
    std::string propId;   // item id
    long propInstanceId;   // item instance id
};

#define RICHELEMENT_COLOR_KEYWORD "color"

class RichElementButton : public RichElement
{
    // some utils functions
public:
    static std::string makeButton(const char* buttonName, const char* propId, long propInstanceId, int color = 0xedf03d);
    static structPropIds parseLink(std::string& linkContent);
private:
    static std::string makeLink(const char* propId, long propInstanceId);
    
    static std::string makeColor(int color);
    static ccColor3B parseColor(std::string& colorContent);
    
public:
	RichElementButton();
	virtual ~RichElementButton();

	virtual CCSize getSize();
	virtual void setPosition(CCPoint pos);
	virtual CCPoint getPosition();
	virtual void parse(const char* elementStr);

	virtual CCNode* getNode();

	virtual int getCharCount();

public:
	std::string buttonName;
	std::string linkContent;
    std::string colorContent;

private:
	CCMenu* m_pMenu;
};

class RichElementText : public RichElement
{
public:
	RichElementText();
	RichElementText(ccColor3B& color);
	virtual ~RichElementText();

	virtual CCSize getSize();
	virtual void setPosition(CCPoint pos);
	virtual CCPoint getPosition();
	virtual void parse(const char* elementStr);

	virtual CCNode* getNode();

	virtual int getCharCount();

private:
	CCLabelTTF* m_label;

	TextFontDefinition m_fontDefinition;
};

////////////////////////////////////////////////////

class RichElementReturn : public RichElement
{
public:
	RichElementReturn();
	virtual ~RichElementReturn();

	virtual CCSize getSize();
	virtual void setPosition(CCPoint pos);
	virtual CCPoint getPosition();
	virtual void parse(const char* elementStr);

	virtual CCNode* getNode();

	virtual int getCharCount();

private:
	CCNode* m_node;
};

#endif
