
#ifndef H_GETSKILLTEACH_H
#define H_GETSKILLTEACH_H

#include "../extensions/UIScene.h"
/*
*教学获得技能                                                                     
*/
class GetSkillTeach : public UIScene
{
public:
	GetSkillTeach(void);
	~GetSkillTeach(void);

	static GetSkillTeach * create(std::string skillId,std::string skillName,std::string skillIcon);
	bool init(std::string skillId,std::string skillName,std::string skillIcon);

	void onEnter();
	void onExit();

	void callBackSkillAction(CCObject * obj);
	void callBackStudySkill(CCObject * obj);

	void ArmatureFinishCallback(cocos2d::extension::CCArmature *armature, cocos2d::extension::MovementEventType movementType, const char *movementID);
private:
	UILayer * u_layer;
	CCSize winSize;
	std::string m_skillId;
	std::string m_skillIcon;
	std::string m_skillName;
	UIImageView *mengban;
};

#endif