
#ifndef __UIBUTTONTREE_H__
#define __UIBUTTONTREE_H__
#include "CocoStudio/GUI/UIWidgets/UIButton.h"

NS_CC_EXT_BEGIN

class UIButtonTree : public UIButton
{
public:
    UIButtonTree();
    ~UIButtonTree();
    
	static UIButtonTree * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	void setInfo(int tag);
	int getInfo();

private:
	int m_parentTag;
};
NS_CC_EXT_END

#endif;
