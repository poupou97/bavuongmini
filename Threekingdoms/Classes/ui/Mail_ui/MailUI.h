#pragma once
#include "cocos-ext.h"
#include "cocos2d.h"
#include "../../ui/extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UITab;
class PacPageView;
class CCRichLabel;
class MailList;
class AnnexItem;
class RichTextInputBox;
class CMailInfo;
class CAttachedProps;


typedef enum
{
	FRIENDNAMERICHINPUT = 301,
	TITLERICHINPUT = 302,
	CONTENTRICHINPUT = 303,

	MAILADDLAYER=320,

	MAILLISTTAG=304,
	TAGMAILLAYER=305,
	TAGSENDMAILLAYER=306,
	//TAGMAILFIRND=307
};
typedef enum{
	REDUCTIONANNEXBUTTON1=311,
	REDUCTIONANNEXLAB1=312,
	REDUCTIONANNEXBUTTON2=313,
	REDUCTIONANNEXLAB2=314,

	ANNEXITEM1=315,
	ANNEXITEM2=316,

	MAILANNEXT = 319,
};

class MailUI:public UIScene,public CCScrollViewDelegate
{
public:
	MailUI(void);
	~MailUI(void);

	static MailUI * create();
	bool init();
	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);

	void callbackClose(CCObject * obj);
	void callBackChangeMail(CCObject * obj);
	void callBackChangeMailSender(CCObject * obj);

	void callBackFriendList(CCObject * obj);
	void callBackShop(CCObject * obj);

	UILabel * label_Gold;
	UILabel * label_GoldIngold;
	void refreshPlayerMoney();
public:
	void callBackExtractAnnex(CCObject * obj);
	void callBackReplyMail(CCObject * obj);
	void callBackDeleteMail(CCObject * obj);
	
	void callBackSurePayToMoney(CCObject * obj);
	void deleteMailSure(CCObject * obj);
	int curTabType;
	UILabel *mailTitle;
	UIPanel * mailContentPanel;
	UIScrollView * m_scrollView;
	void showMailInfo(int idx);

	UIImageView * image_mailItem_1;
	UIImageView * image_mailItem_2;
public:
	//write
	void counterAnnex(CCObject * obj);
	void callBackAddexan(CCObject * obj);

	void reductionAnnex1(CCObject *obj);
	void reductionAnnex2(CCObject *obj);

	void callBackClean(CCObject * obj);
	int sendMailCollectcoins;
	int sendMailCollectinIngots;
	void callBackSendMail(CCObject * obj);
public:
	void scrollViewDidScroll(CCScrollView* view);
	void scrollViewDidZoom(CCScrollView* view);
public:
	int curMailPage;
	bool isHasNextPage;
	int operationType;
	std::vector<CMailInfo *> sourceDataVector;
	std::vector<CAttachedProps *> attachePropVector;

	void collectCoins(CCObject * obj);
	void setCollectCoinsValue(CCObject * obj);
	void collectIngot(CCObject * obj);
	void setCollectIngotValue(CCObject * obj);
	UILabel * labelCoins;
	UILabel * labelIngot;
	//UIImageView * imageCostgetMail;
	UIImageView * readMailCoinsImage;
	UILabel * readMailCoinsLabel;
	UIImageView * readMailIngotImage;
	UILabel * readMailIngotLabel;
public:
	UILayer * layer_;
	// mail info
	int receiverId; 
	std::string receiverName;
	std::string mail_subject;
	std::string mail_text;
	bool mail_attactMent;
	int mail_amount;//attach amount

	MailList * mailList;
	int selectAnnexNum;// annex rame num
	bool firstIsNull;
	int mailList_Id;
public:
	int mailAttachment_amount;
	int packageIndex;
	UILayer * getMailLayer;
	UILayer * sendMailLayer;
	UITab * channelLabel;
	PacPageView * pageView;
	RichTextInputBox * friendName;

	UIPanel * readPanel;
	UIPanel * writePanel;
	UIPanel * mailNull;
	//UITab * channelLabelMail;
	UIButton * replyMail;
	UIButton * extractAnnex;
	//UILabelBMFont *labelMailNum_;
	RichTextInputBox * titleBox;
	RichTextInputBox * contentBox;
	UIImageView* currentPage;
	void CurrentPageViewChanged(CCObject *pSender);

	void PageScrollToDefault();
private:
	struct FriendStruct
	{
		int relationType;
		int pageNum;
		int pageIndex;
		int showOnline;
	};
};
