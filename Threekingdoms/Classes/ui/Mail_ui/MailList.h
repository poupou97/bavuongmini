#pragma once

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
typedef enum{
	MAILTABTAG = 333,
	TABALEVIEW_HIGHLIGHT_TAG =334,
	TABLEVIEW_HIGHLIGHT_SELECT_TAG = 335, 
};
class MailList:public UIScene,public cocos2d::extension::CCTableViewDataSource,public cocos2d::extension::CCTableViewDelegate
{
public:
	MailList(void);
	~MailList(void);

	static MailList * create();

	bool init();
	virtual void onEnter();
	virtual void onExit();

    virtual void tableCellHighlight(CCTableView* table, CCTableViewCell* cell);
    virtual void tableCellUnhighlight(CCTableView* table, CCTableViewCell* cell);
    
	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	void reSetRemind(long long mailId);
	// default implements are used to call script callback if exist
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);
public:
	CCTableView* tableView;
	int lastSelectCellId;

	//CCScale9Sprite * pageView_kuang;
};


///////////////////////////////// mail cell
#define mailStateFlag 10
#define mailAttachmentStateFlag 20
class MailListCell:public CCTableViewCell
{
public:
	MailListCell(void);
	~MailListCell(void);

	static MailListCell *create(int idx);
	bool init(int idx);

	void onEnter();
	void onExit();
};
