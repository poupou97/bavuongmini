#include "OffLineExpUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "AppMacros.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/vip_ui/VipDetailUI.h"
#include "../../messageclient/element/COfflineExpPuf.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/vip_ui/VipData.h"

#define kTagGreeFlag 444
#define kTagLabelValue_Normal 445
#define kTagLabelValue_Gold 446
#define kTagLabelValue_GoldInGot 447

OffLineExpUI::OffLineExpUI():
m_nGetExpType(1)
{
}

OffLineExpUI::~OffLineExpUI()
{
}

OffLineExpUI* OffLineExpUI::create()
{
	OffLineExpUI * offLineExpUI = new OffLineExpUI();
	if (offLineExpUI && offLineExpUI->init())
	{
		offLineExpUI->autorelease();
		return offLineExpUI;
	}
	CC_SAFE_DELETE(offLineExpUI);
	return NULL;
}

bool OffLineExpUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		this->scheduleUpdate();

		m_curRoleVipLevel = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel();

		UIPanel* panel_offLineExp =  (UIPanel*)GUIReader::shareReader()->widgetFromJsonFile("res_ui/offlineexp_1.json");
		panel_offLineExp->setAnchorPoint(ccp(0.0f,0.0f));
		panel_offLineExp->getValidNode()->setContentSize(CCSizeMake(640, 465));
		panel_offLineExp->setPosition(CCPointZero);
		panel_offLineExp->setTouchEnable(true);
		m_pUiLayer->addWidget(panel_offLineExp);

		u_layer = UILayer::create();
		addChild(u_layer);

		UIButton *Btn_close = (UIButton*)UIHelper::seekWidgetByName(panel_offLineExp,"Button_close");
		Btn_close->setTouchEnable(true);
		Btn_close->setPressedActionEnabled(true);
		Btn_close->addReleaseEvent(this, coco_releaseselector(OffLineExpUI::CloseEvent));

		UIButton *Btn_vipBuy = (UIButton*)UIHelper::seekWidgetByName(panel_offLineExp,"Button_takeVip");
		Btn_vipBuy->setTouchEnable(true);
		Btn_vipBuy->setPressedActionEnabled(true);
		Btn_vipBuy->addReleaseEvent(this, coco_releaseselector(OffLineExpUI::VipBuyEvent));

		Btn_getExp = (UIButton*)UIHelper::seekWidgetByName(panel_offLineExp,"Button_takeExp");
		Btn_getExp->setTouchEnable(true);
		Btn_getExp->setPressedActionEnabled(true);
		Btn_getExp->addReleaseEvent(this, coco_releaseselector(OffLineExpUI::GetExpEvent));

		l_btn_des = (UILabel*)UIHelper::seekWidgetByName(panel_offLineExp,"Label_964");

		UIButton *Btn_normal = (UIButton*)UIHelper::seekWidgetByName(panel_offLineExp,"Button_normal");
		Btn_normal->setTouchEnable(true);
		//Btn_normal->setPressedActionEnabled(true);
		Btn_normal->addReleaseEvent(this, coco_releaseselector(OffLineExpUI::NormalEvent));
		cb_normal = (UICheckBox*)UIHelper::seekWidgetByName(Btn_normal,"CheckBox_normal");
		cb_normal->setSelectedState(true);

		UIButton *Btn_gold = (UIButton*)UIHelper::seekWidgetByName(panel_offLineExp,"Button_gold");
		Btn_gold->setTouchEnable(true);
		//Btn_gold->setPressedActionEnabled(true);
		Btn_gold->addReleaseEvent(this, coco_releaseselector(OffLineExpUI::GoldEvent));
		cb_gold = (UICheckBox*)UIHelper::seekWidgetByName(panel_offLineExp,"CheckBox_gold");
		cb_gold->setSelectedState(false);
		l_gold = (UILabel*)UIHelper::seekWidgetByName(panel_offLineExp,"Label_coinsValue");
		char s_goldValue[20];
		sprintf(s_goldValue,"%d",GameView::getInstance()->m_nRequiredGoldToGetExp);
		l_gold->setText(s_goldValue);

		UIButton *Btn_goldInGot = (UIButton*)UIHelper::seekWidgetByName(panel_offLineExp,"Button_goldingot");
		Btn_goldInGot->setTouchEnable(true);
		//Btn_goldInGot->setPressedActionEnabled(true);
		Btn_goldInGot->addReleaseEvent(this, coco_releaseselector(OffLineExpUI::GoldInGotEvent));
		cb_goldInGot = (UICheckBox*)UIHelper::seekWidgetByName(panel_offLineExp,"CheckBox_goldingot");
		cb_goldInGot->setSelectedState(false);
		l_goldInGot = (UILabel*)UIHelper::seekWidgetByName(panel_offLineExp,"Label_goldValue");
		char s_goldInGotValue[20];
		sprintf(s_goldInGotValue,"%d",GameView::getInstance()->m_nRequiredGoldInGotToGetExp);
		l_goldInGot->setText(s_goldInGotValue);

		m_tableView = CCTableView::create(this,CCSizeMake(497,207));
		m_tableView->setDirection(kCCScrollViewDirectionVertical);
		m_tableView->setAnchorPoint(ccp(0,0));
		m_tableView->setPosition(ccp(70,89));
		m_tableView->setContentOffset(ccp(0,0));
		m_tableView->setDelegate(this);;
		m_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		u_layer->addChild(m_tableView);

		setToDefault();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(640,465));

		return true;
	}
	return false;
}



void OffLineExpUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void OffLineExpUI::onExit()
{
	UIScene::onExit();
}

bool OffLineExpUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	return true;
}

void OffLineExpUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void OffLineExpUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void OffLineExpUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void OffLineExpUI::CloseEvent(CCObject * pSender)
{
	this->closeAnim();
}

void OffLineExpUI::VipBuyEvent( CCObject* pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	CCLayer *vipScene = (CCLayer*)mainscene->getChildByTag(kTagVipDetailUI);
	if(vipScene == NULL)
	{
		if(VipData::vipDetailUI)
		{
			VipDetailUI * vipDetailUI = (VipDetailUI*)VipData::vipDetailUI;
			mainscene->addChild(vipDetailUI,0,kTagVipDetailUI);
			vipDetailUI->ignoreAnchorPointForPosition(false);
			vipDetailUI->setAnchorPoint(ccp(0.5f,0.5f));
			vipDetailUI->setPosition(ccp(winSize.width/2, winSize.height/2));
			vipDetailUI->setToDefault();
		}
	}
}

void OffLineExpUI::GetExpEvent( CCObject* pSender )
{
	COfflineExpPuf * temp = GameView::getInstance()->m_offLineExpPuf.at(m_curRoleVipLevel);
	if (!temp)
		return;

	switch(m_nGetExpType)
	{
	case 1:
		{
			if (temp->normal() <= 0)
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("vip_reward_Geted"));
			}
			else
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(2809,(void *)m_nGetExpType);
			}
		}
		break;
	case 2:
		{
			if (temp->gold() <= 0)
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("vip_reward_Geted"));
			}
			else
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(2809,(void *)m_nGetExpType);
			}
		}
		break;
	case 3:
		{
			if (temp->goldingot() <= 0)
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("vip_reward_Geted"));
			}
			else
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(2809,(void *)m_nGetExpType);
			}
		}
		break;
	}
}

void OffLineExpUI::scrollViewDidScroll( CCScrollView* view )
{

}

void OffLineExpUI::scrollViewDidZoom( CCScrollView* view )
{

}

void OffLineExpUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{

}

cocos2d::CCSize OffLineExpUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(490,34);
}

cocos2d::extension::CCTableViewCell* OffLineExpUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	COfflineExpPuf * temp = GameView::getInstance()->m_offLineExpPuf.at(idx);

	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();

	CCSprite * sprite_levelFrame;

	if (m_curRoleVipLevel == idx)
	{
		sprite_levelFrame = CCSprite::create("res_ui/new_button_12.png");
		sprite_levelFrame->setAnchorPoint(ccp(0,0));
		sprite_levelFrame->setPosition(ccp(0,0));
		sprite_levelFrame->setScaleX(1.3f);
		sprite_levelFrame->setScaleY(0.9f);
		cell->addChild(sprite_levelFrame);
	}
	else
	{
		sprite_levelFrame = CCSprite::create("res_ui/new_button_9.png");
		sprite_levelFrame->setAnchorPoint(ccp(0,0));
		sprite_levelFrame->setPosition(ccp(0,0));
		sprite_levelFrame->setScaleX(1.3f);
		sprite_levelFrame->setScaleY(0.9f);
		cell->addChild(sprite_levelFrame);
	}

	std::string str_vipLevel;
	if (idx == 0)
	{
		str_vipLevel = StringDataManager::getString("offLineExpUI_vip0");
	}
	else
	{
		str_vipLevel = "VIP";
		char s_lv[10];
		sprintf(s_lv,"%d",idx);
		str_vipLevel.append(s_lv);
	}
	CCLabelBMFont * lbt_vipLevel = CCLabelBMFont::create(str_vipLevel.c_str(),"res_ui/font/ziti_3.fnt");
	lbt_vipLevel->setAnchorPoint(ccp(0.5f,0.5f));
	lbt_vipLevel->setPosition(ccp(sprite_levelFrame->getContentSize().width/2*sprite_levelFrame->getScaleX(),sprite_levelFrame->getContentSize().height*sprite_levelFrame->getScaleY()/2));
	cell->addChild(lbt_vipLevel);

	//绿色底
	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(5, 5, 1, 1) , "res_ui/mengban_green2.png");
	pHighlightSpr->setPreferredSize(CCSize(132,29));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setTag(kTagGreeFlag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	CCScale9Sprite * sprite_di = CCScale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_di->setPreferredSize(CCSizeMake(401,31));
	sprite_di->setCapInsets(CCRect(7.5f,13.5f,1,1));
	sprite_di->setAnchorPoint(ccp(0,0));
	sprite_di->setPosition(ccp(95,0));
	cell->addChild(sprite_di);
	CCSprite * sprite_di_1 = CCSprite::create("res_ui/LV3_dikuang00.png");
	sprite_di_1->setAnchorPoint(ccp(0,0));
	sprite_di_1->setPosition(ccp(229,0));
	cell->addChild(sprite_di_1);
	CCSprite * sprite_di_2 = CCSprite::create("res_ui/LV3_dikuang00.png");
	sprite_di_2->setAnchorPoint(ccp(0,0));
	sprite_di_2->setPosition(ccp(362,0));
	cell->addChild(sprite_di_2);

	CCLabelTTF * l_normalExp = CCLabelTTF::create("",APP_FONT_NAME,18);
	l_normalExp->setAnchorPoint(ccp(0.5f,0.5f));
	l_normalExp->setPosition(ccp(159,15));
	//l_normalExp->setColor(ccc3(49,93,11));
	l_normalExp->setTag(kTagLabelValue_Normal);
	cell->addChild(l_normalExp);
	if (temp->normal()<0)
	{
		l_normalExp->setString(StringDataManager::getString("offLineExpUI_canNotGetExp"));
	}
	else
	{
		char s_normalExp [20];
		sprintf(s_normalExp,"%d",temp->normal());
		l_normalExp->setString(s_normalExp);
	}

	
	CCLabelTTF * l_goldExp = CCLabelTTF::create("",APP_FONT_NAME,18);
	l_goldExp->setAnchorPoint(ccp(0.5f,0.5f));
	l_goldExp->setPosition(ccp(295,15));
	//l_goldExp->setColor(ccc3(49,93,13));
	l_goldExp->setTag(kTagLabelValue_Gold);
	cell->addChild(l_goldExp);
	if (temp->gold()<0)
	{
		l_goldExp->setString(StringDataManager::getString("offLineExpUI_canNotGetExp"));
	}
	else
	{
		char s_goldExp [20];
		sprintf(s_goldExp,"%d",temp->gold());
		l_goldExp->setString(s_goldExp);
	}

	CCLabelTTF * l_goldInGotExp = CCLabelTTF::create("",APP_FONT_NAME,18);
	l_goldInGotExp->setAnchorPoint(ccp(0.5f,0.5f));
	l_goldInGotExp->setPosition(ccp(431,15));
	//l_goldInGotExp->setColor(ccc3(49,93,13));
	l_goldInGotExp->setTag(kTagLabelValue_GoldInGot);
	cell->addChild(l_goldInGotExp);
	if (temp->goldingot()<0)
	{
		l_goldInGotExp->setString(StringDataManager::getString("offLineExpUI_canNotGetExp"));
	}
	else
	{
		char s_goldInGotExp [20];
		sprintf(s_goldInGotExp,"%d",temp->goldingot());
		l_goldInGotExp->setString(s_goldInGotExp);
	}

	if (m_curRoleVipLevel == idx)
	{
		pHighlightSpr->setVisible(true);
		switch(m_nGetExpType)
		{
		case 1:
			{
				pHighlightSpr->setPosition(ccp(100,1));
				l_normalExp->setColor(ccc3(255,0,0));
			}
			break;
		case 2:
			{
				pHighlightSpr->setPosition(ccp(232,1));
				l_goldExp->setColor(ccc3(255,0,0));
			}
			break;
		case 3:
			{
				pHighlightSpr->setPosition(ccp(364,1));
				l_goldInGotExp->setColor(ccc3(255,0,0));
			}
			break;
		}
	}

	return cell;
}

unsigned int OffLineExpUI::numberOfCellsInTableView( CCTableView *table )
{
	return GameView::getInstance()->m_offLineExpPuf.size();
}

void OffLineExpUI::refreshTableViewWithOutChangeOffSet()
{
	CCPoint _s = m_tableView->getContentOffset();
	int _h = m_tableView->getContentSize().height + _s.y;
	m_tableView->reloadData();
	CCPoint temp = ccp(_s.x,_h - m_tableView->getContentSize().height);
	m_tableView->setContentOffset(temp); 
}

void OffLineExpUI::NormalEvent( CCObject* pSender )
{
	COfflineExpPuf * temp = GameView::getInstance()->m_offLineExpPuf.at(m_curRoleVipLevel);
	if (!temp)
		return;

	if (temp->normal() < 0)
	{
		std::string str_dialog ;
		if (m_curRoleVipLevel <= 0)
		{
			str_dialog.append(StringDataManager::getString("offLineExpUI_vip0"));
		}
		else
		{
			str_dialog.append("VIP");
			char s_viplevel[20];
			sprintf(s_viplevel,"%d",m_curRoleVipLevel);
			str_dialog.append(s_viplevel);

		}
		str_dialog.append(StringDataManager::getString("offLineExpUI_canNotSelectIt"));
		GameView::getInstance()->showAlertDialog(str_dialog.c_str());
		return;
	}

	m_nGetExpType = 1;

	cb_normal->setSelectedState(true);
	cb_gold->setSelectedState(false);
	cb_goldInGot->setSelectedState(false);
	RefreshSelectStaus();
}

void OffLineExpUI::GoldEvent( CCObject* pSender )
{
	COfflineExpPuf * temp = GameView::getInstance()->m_offLineExpPuf.at(m_curRoleVipLevel);
	if (!temp)
		return;

	if (temp->gold() < 0)
	{
		std::string str_dialog ;
		if (m_curRoleVipLevel <= 0)
		{
			str_dialog.append(StringDataManager::getString("offLineExpUI_vip0"));
		}
		else
		{
			str_dialog.append("VIP");
			char s_viplevel[20];
			sprintf(s_viplevel,"%d",m_curRoleVipLevel);
			str_dialog.append(s_viplevel);

		}
		str_dialog.append(StringDataManager::getString("offLineExpUI_canNotSelectIt"));
		GameView::getInstance()->showAlertDialog(str_dialog.c_str());
		return;
	}

	m_nGetExpType = 2;

	cb_normal->setSelectedState(false);
	cb_gold->setSelectedState(true);
	cb_goldInGot->setSelectedState(false);
	RefreshSelectStaus();
}

void OffLineExpUI::GoldInGotEvent( CCObject* pSender )
{
	COfflineExpPuf * temp = GameView::getInstance()->m_offLineExpPuf.at(m_curRoleVipLevel);
	if (!temp)
		return;
	
	if (temp->goldingot() < 0)
	{
		std::string str_dialog ;
		if (m_curRoleVipLevel <= 0)
		{
			str_dialog.append(StringDataManager::getString("offLineExpUI_vip0"));
		}
		else
		{
			str_dialog.append("VIP");
			char s_viplevel[20];
			sprintf(s_viplevel,"%d",m_curRoleVipLevel);
			str_dialog.append(s_viplevel);
			
		}
		str_dialog.append(StringDataManager::getString("offLineExpUI_canNotSelectIt"));
		GameView::getInstance()->showAlertDialog(str_dialog.c_str());
		return;
	}

	m_nGetExpType = 3;

	cb_normal->setSelectedState(false);
	cb_gold->setSelectedState(false);
	cb_goldInGot->setSelectedState(true);
	RefreshSelectStaus();
}

void OffLineExpUI::RefreshSelectStaus()
{
	CCTableViewCell * cell = m_tableView->cellAtIndex(m_curRoleVipLevel);
	if (cell)
	{
		switch(m_nGetExpType)
		{
		case 1:
			{
				cell->getChildByTag(kTagGreeFlag)->setPosition(ccp(97,1));
				CCLabelTTF * lb_normal = dynamic_cast<CCLabelTTF*>(cell->getChildByTag(kTagLabelValue_Normal));
				if (lb_normal)
				{
					lb_normal->setColor(ccc3(255,0,0));
				}
				CCLabelTTF * lb_gold = dynamic_cast<CCLabelTTF*>(cell->getChildByTag(kTagLabelValue_Gold));
				if (lb_gold)
				{
					lb_gold->setColor(ccc3(255,255,255));
				}
				CCLabelTTF * lb_goldInGot = dynamic_cast<CCLabelTTF*>(cell->getChildByTag(kTagLabelValue_GoldInGot));
				if (lb_goldInGot)
				{
					lb_goldInGot->setColor(ccc3(255,255,255));
				}
			}
			break;
		case 2:
			{
				cell->getChildByTag(kTagGreeFlag)->setPosition(ccp(231,1));
				CCLabelTTF * lb_normal = dynamic_cast<CCLabelTTF*>(cell->getChildByTag(kTagLabelValue_Normal));
				if (lb_normal)
				{
					lb_normal->setColor(ccc3(255,255,255));
				}
				CCLabelTTF * lb_gold = dynamic_cast<CCLabelTTF*>(cell->getChildByTag(kTagLabelValue_Gold));
				if (lb_gold)
				{
					lb_gold->setColor(ccc3(255,0,0));
				}
				CCLabelTTF * lb_goldInGot = dynamic_cast<CCLabelTTF*>(cell->getChildByTag(kTagLabelValue_GoldInGot));
				if (lb_goldInGot)
				{
					lb_goldInGot->setColor(ccc3(255,255,255));
				}
			}
			break;
		case 3:
			{
				cell->getChildByTag(kTagGreeFlag)->setPosition(ccp(363,1));
				CCLabelTTF * lb_normal = dynamic_cast<CCLabelTTF*>(cell->getChildByTag(kTagLabelValue_Normal));
				if (lb_normal)
				{
					lb_normal->setColor(ccc3(255,255,255));
				}
				CCLabelTTF * lb_gold = dynamic_cast<CCLabelTTF*>(cell->getChildByTag(kTagLabelValue_Gold));
				if (lb_gold)
				{
					lb_gold->setColor(ccc3(255,255,255));
				}
				CCLabelTTF * lb_goldInGot = dynamic_cast<CCLabelTTF*>(cell->getChildByTag(kTagLabelValue_GoldInGot));
				if (lb_goldInGot)
				{
					lb_goldInGot->setColor(ccc3(255,0,0));
				}
			}
			break;
		}

		COfflineExpPuf * temp = GameView::getInstance()->m_offLineExpPuf.at(m_curRoleVipLevel);
		if ( temp )
		{
			if (temp->m_bIsGeted)
			{
				Btn_getExp->setTextures("res_ui/new_button_001.png","res_ui/new_button_001.png","");
				Btn_getExp->setTouchEnable(false);
				l_btn_des->setText(StringDataManager::getString("vip_reward_Geted"));
			}
			else
			{
				Btn_getExp->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				Btn_getExp->setTouchEnable(true);
				l_btn_des->setText(StringDataManager::getString("offLineExpUI_getExp"));
			}
		}
	}
}

void OffLineExpUI::RefreshUI()
{
	refreshTableViewWithOutChangeOffSet();

	char s_goldValue[20];
	sprintf(s_goldValue,"%d",GameView::getInstance()->m_nRequiredGoldToGetExp);
	l_gold->setText(s_goldValue);

	char s_goldInGotValue[20];
	sprintf(s_goldInGotValue,"%d",GameView::getInstance()->m_nRequiredGoldInGotToGetExp);
	l_goldInGot->setText(s_goldInGotValue);

	RefreshSelectStaus();
}

void OffLineExpUI::setToDefault()
{
	COfflineExpPuf * temp = GameView::getInstance()->m_offLineExpPuf.at(m_curRoleVipLevel);
	if (!temp)
		return;

	if (temp->goldingot() >= 0)
	{
		m_nGetExpType = 3;

		cb_normal->setSelectedState(false);
		cb_gold->setSelectedState(false);
		cb_goldInGot->setSelectedState(true);
		RefreshSelectStaus();
	}
	else if (temp->gold() >= 0)
	{
		m_nGetExpType = 2;

		cb_normal->setSelectedState(false);
		cb_gold->setSelectedState(true);
		cb_goldInGot->setSelectedState(false);
		RefreshSelectStaus();
	}
	else
	{
		m_nGetExpType = 1;

		cb_normal->setSelectedState(true);
		cb_gold->setSelectedState(false);
		cb_goldInGot->setSelectedState(false);
		RefreshSelectStaus();
	}
}

void OffLineExpUI::setCurRoleVipLevel( int lv )
{
	m_curRoleVipLevel = lv;
}


///////////////////////////////////////////////////////////////////////////////////////////////////

// VipEveryDayRewardCell::VipEveryDayRewardCell()
// {
// 
// }
// 
// VipEveryDayRewardCell::~VipEveryDayRewardCell()
// {
// 	delete curOneVipGift;
// }
// 
// VipEveryDayRewardCell* VipEveryDayRewardCell::create( COneVipGift * oneVipGift )
// {
// 	VipEveryDayRewardCell * vipEveryDayRewardCell = new VipEveryDayRewardCell();
// 	if (vipEveryDayRewardCell && vipEveryDayRewardCell->init(oneVipGift))
// 	{
// 		vipEveryDayRewardCell->autorelease();
// 		return vipEveryDayRewardCell;
// 	}
// 	CC_SAFE_DELETE(vipEveryDayRewardCell);
// 	return NULL;
// }
// 
// bool VipEveryDayRewardCell::init( COneVipGift * oneVipGift )
// {
// 	if (UIScene::init())
// 	{
// 		m_pUiLayer->setSwallowsTouches(false);
// 
// 		curOneVipGift = new COneVipGift();
// 		curOneVipGift->CopyFrom(*oneVipGift);
// 
// 		// 		UIImageView * image_frame = UIImageView::create();
// 		// 		image_frame->setTexture("res_ui/highlight.png");
// 		// 		image_frame->setScale9Enable(true);
// 		// 		image_frame->setScale9Size(CCSizeMake(366,71));
// 		// 		image_frame->setAnchorPoint(ccp(0.5f,0.5f));
// 		// 		image_frame->setPosition(ccp(366/2,71/2));
// 		// 		m_pUiLayer->addWidget(image_frame);
// 		// 		
// 		UIImageView * image_line = UIImageView::create();
// 		image_line->setTexture("res_ui/henggang_red.png");
// 		image_line->setAnchorPoint(ccp(0.5f,0.5f));
// 		image_line->setPosition(ccp(366/2,3));
// 		image_line->setScaleX(2.3f);
// 		m_pUiLayer->addWidget(image_line);
// 
// 		UIImageView * imageView_icon = UIImageView::create();
// 		imageView_icon->setTexture("res_ui/vip/vip_b.png");
// 		imageView_icon->setAnchorPoint(ccp(0.5f,0.5f));
// 		imageView_icon->setPosition(ccp(40,35));
// 		m_pUiLayer->addWidget(imageView_icon);
// 
// 		char str_level[20];
// 		sprintf(str_level,"%d",oneVipGift->number());
// 		UILabelBMFont * l_vipLevel = UILabelBMFont::create();
// 		l_vipLevel->setFntFile("res_ui/font/ziti_3.fnt");
// 		l_vipLevel->setText(str_level);
// 		l_vipLevel->setScale(0.9f);
// 		l_vipLevel->setAnchorPoint(ccp(0.5f,0.5f));
// 		l_vipLevel->setPosition(ccp(12,-2));
// 		imageView_icon->addChild(l_vipLevel);
// 
// 		for(int j = 0;j<oneVipGift->goods_size();++j)
// 		{
// 			if (j >= 2)
// 				continue;
// 
// 			GoodsInfo * goodInfo = new GoodsInfo();
// 			goodInfo->CopyFrom(oneVipGift->goods(j));
// 			int num = 0;
// 			if (j < oneVipGift->goodsnumber_size())
// 				num = oneVipGift->goodsnumber(j);
// 
// 			VipRewardCellItem * goodsItem = VipRewardCellItem::create(goodInfo,num);
// 			goodsItem->ignoreAnchorPointForPosition(false);
// 			goodsItem->setAnchorPoint(ccp(0.5f,0.5f));
// 			goodsItem->setPosition(ccp(116+79*j,35));
// 			m_pUiLayer->addChild(goodsItem);
// 
// 			delete goodInfo;
// 		}
// 
// 		switch(oneVipGift->status())
// 		{
// 		case 1:   //不可领取
// 			{
// 				UIImageView * imageView_di = UIImageView::create();
// 				imageView_di->setTexture("res_ui/zhezhao_btn.png");
// 				imageView_di->setAnchorPoint(ccp(0.5f,0.5f));
// 				imageView_di->setPosition(ccp(306,35));
// 				m_pUiLayer->addWidget(imageView_di);
// 
// 				std::string str_des = "VIP";
// 				char s_level[20];
// 				sprintf(s_level,"%d",oneVipGift->number());
// 				str_des.append(s_level);
// 				str_des.append(StringDataManager::getString("vip_reward_cannotGet"));
// 				UILabel *l_des = UILabel::create();
// 				l_des->setFontName(APP_FONT_NAME);
// 				l_des->setFontSize(18);
// 				l_des->setText(str_des.c_str());
// 				l_des->setAnchorPoint(ccp(0.5f,0.5f));
// 				l_des->setPosition(ccp(0,0));
// 				imageView_di->addChild(l_des);
// 			}
// 			break;
// 		case 2:   //可领取
// 			{
// 				UIButton * btn_get = UIButton::create();
// 				btn_get->setTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
// 				btn_get->setScale9Enable(true);
// 				btn_get->setScale9Size(CCSizeMake(108,43));
// 				btn_get->setCapInsets(CCRectMake(18,9,2,23));
// 				btn_get->setAnchorPoint(ccp(0.5f,0.5f));
// 				btn_get->setPosition(ccp(306,35));
// 				btn_get->setTouchEnable(true);
// 				btn_get->setPressedActionEnabled(true);
// 				btn_get->addReleaseEvent(this,coco_releaseselector(VipEveryDayRewardCell::getRewardEvent));
// 				m_pUiLayer->addWidget(btn_get);
// 
// 				UILabel *l_get = UILabel::create();
// 				l_get->setFontName(APP_FONT_NAME);
// 				l_get->setFontSize(18);
// 				l_get->setText(StringDataManager::getString("btn_lingqu"));
// 				l_get->setAnchorPoint(ccp(0.5f,0.5f));
// 				l_get->setPosition(ccp(0,0));
// 				btn_get->addChild(l_get);
// 			}
// 			break;
// 		case 3:   //已领取
// 			{
// 				UIImageView * imageView_di = UIImageView::create();
// 				imageView_di->setTexture("res_ui/zhezhao_btn.png");
// 				imageView_di->setAnchorPoint(ccp(0.5f,0.5f));
// 				imageView_di->setPosition(ccp(306,35));
// 				m_pUiLayer->addWidget(imageView_di);
// 				UILabel *l_des = UILabel::create();
// 				l_des->setFontName(APP_FONT_NAME);
// 				l_des->setFontSize(18);
// 				l_des->setText(StringDataManager::getString("vip_reward_Geted"));
// 				l_des->setAnchorPoint(ccp(0.5f,0.5f));
// 				l_des->setPosition(ccp(0,0));
// 				imageView_di->addChild(l_des);
// 			}
// 			break;
// 		}
// 
// 		this->setContentSize(CCSizeMake(350,76));
// 
// 		return true;
// 	}
// 	return false;
// }
// 
// void VipEveryDayRewardCell::getRewardEvent( CCObject *pSender )
// {
// 	//req to get reward
// 	GameMessageProcessor::sharedMsgProcessor()->sendReq(5119);
// }
// 
