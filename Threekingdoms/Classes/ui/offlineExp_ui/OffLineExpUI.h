
#ifndef  _OFFLINEEXPUI_OFFLINEEXPUI_H_
#define _OFFLINEEXPUI_OFFLINEEXPUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class OffLineExpUI : public UIScene, public CCTableViewDataSource, public CCTableViewDelegate
{
private:
	UILayer * u_layer;
	CCTableView * m_tableView;
public:
	OffLineExpUI();
	~OffLineExpUI();

	static OffLineExpUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	void CloseEvent(CCObject * pSender);

	void refreshTableViewWithOutChangeOffSet();

 	void VipBuyEvent(CCObject* pSender);
	void GetExpEvent(CCObject* pSender);

	void NormalEvent(CCObject* pSender);
	void GoldEvent(CCObject* pSender);
	void GoldInGotEvent(CCObject* pSender);

	void RefreshSelectStaus();

	void RefreshUI();
	void setToDefault();

	void setCurRoleVipLevel(int lv);

private:
	int m_nGetExpType;

	int m_curRoleVipLevel;

private:
	UICheckBox * cb_normal;
	UICheckBox * cb_gold;
	UICheckBox * cb_goldInGot;

	UIButton * Btn_getExp;
	UILabel * l_btn_des;

	UILabel * l_gold;
	UILabel * l_goldInGot;
};



/////////////////////////////////
/**
 * vip每日领取下的cell
 * @author yangjun
 * @version 0.1.0
 * @date 2014.5.6
 */
// 
// class COneVipGift;
// 
// class VipEveryDayRewardCell : public UIScene
// {
// public:
// 	VipEveryDayRewardCell();
// 	~VipEveryDayRewardCell();
// 	static VipEveryDayRewardCell* create(COneVipGift * oneVipGift);
// 	bool init(COneVipGift * oneVipGift);
// 
// 	void getRewardEvent(CCObject *pSender);
// 
// public:
// 	COneVipGift * curOneVipGift;
// };



#endif

