#include "OnLineRewardUI.h"

#include "../../loadscene_state/LoadSceneState.h"

#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../messageclient/element/COneOnlineGift.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "OnlineGift.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "OnlineGiftIcon.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "OnlineGiftData.h"
#include "../../utils/StaticDataManager.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "OnlineGiftIconWidget.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../utils/GameUtils.h"
#include "OnlineGiftLayer.h"

using namespace CocosDenshion;

UIPanel * OnLineRewardUI::s_pPanel = NULL;

#define  TAG_TUTORIALPARTICLE 500

OnLineRewardUI::OnLineRewardUI()
	:m_nTimeShow(0)
	,m_nOnlineGiftTime(0)
	,m_nGiftNum(0)
	,m_tutorialParticle(NULL)
{
	m_array_tagBtnGet[0] = TAG_BTN_GET_1;
	m_array_tagBtnGet[1] = TAG_BTN_GET_2;
	m_array_tagBtnGet[2] = TAG_BTN_GET_3;
	m_array_tagBtnGet[3] = TAG_BTN_GET_4;
	m_array_tagBtnGet[4] = TAG_BTN_GET_5;
	m_array_tagBtnGet[5] = TAG_BTN_GET_6;
	m_array_tagBtnGet[6] = TAG_BTN_GET_7;
	m_array_tagBtnGet[7] = TAG_BTN_GET_8;

	m_array_tagBtnGift1[0] = TAG_BTN_GIFT_1_1;
	m_array_tagBtnGift1[1] = TAG_BTN_GIFT_2_1;
	m_array_tagBtnGift1[2] = TAG_BTN_GIFT_3_1;
	m_array_tagBtnGift1[3] = TAG_BTN_GIFT_4_1;
	m_array_tagBtnGift1[4] = TAG_BTN_GIFT_5_1;
	m_array_tagBtnGift1[5] = TAG_BTN_GIFT_6_1;
	m_array_tagBtnGift1[6] = TAG_BTN_GIFT_7_1;
	m_array_tagBtnGift1[7] = TAG_BTN_GIFT_8_1;

	m_array_tagBtnGift2[0] = TAG_BTN_GIFT_1_2;
	m_array_tagBtnGift2[1] = TAG_BTN_GIFT_2_2;
	m_array_tagBtnGift2[2] = TAG_BTN_GIFT_3_2;
	m_array_tagBtnGift2[3] = TAG_BTN_GIFT_4_2;
	m_array_tagBtnGift2[4] = TAG_BTN_GIFT_5_2;
	m_array_tagBtnGift2[5] = TAG_BTN_GIFT_6_2;
	m_array_tagBtnGift2[6] = TAG_BTN_GIFT_7_2;
	m_array_tagBtnGift2[7] = TAG_BTN_GIFT_8_2;

}

OnLineRewardUI::~OnLineRewardUI()
{

}

OnLineRewardUI * OnLineRewardUI::create()
{
	OnLineRewardUI* pOnLineRewardUI = new OnLineRewardUI();
	if (pOnLineRewardUI && pOnLineRewardUI->init())
	{
		pOnLineRewardUI->autorelease();
		return pOnLineRewardUI;
	}
	CC_SAFE_DELETE(pOnLineRewardUI);
	return NULL;
}

bool OnLineRewardUI::init()
{
	if (UIScene::init())
	{
		if(LoadSceneLayer::onlineAwardsUIPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::onlineAwardsUIPanel->removeFromParentAndCleanup(false);
		}
		s_pPanel = LoadSceneLayer::onlineAwardsUIPanel;

		m_pUiLayer->addWidget(s_pPanel);

		initUI();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSize(790, 440));

		this->schedule(schedule_selector(OnLineRewardUI::update),1.0f);

		return true;
	}

	return false;
}

void OnLineRewardUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void OnLineRewardUI::onExit()
{
	UIScene::onExit();

	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

bool OnLineRewardUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void OnLineRewardUI::callBackBtnClolse( CCObject * obj )
{
	this->closeAnim();
}

void OnLineRewardUI::callBackBtnGetGift( CCObject* obj )
{
	UIButton * pUIBtn = (UIButton *)obj;
	// 通过tag判断是第几个奖品的Btn被点击（即哪个奖品被点击领取）
	if (TAG_BTN_GET_1 == pUIBtn->getTag())
	{
		// 请求服务器领取奖品
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)0);
	}
	else if (TAG_BTN_GET_2 == pUIBtn->getTag())
	{
		// 请求服务器领取奖品
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)1);
	}
	else if (TAG_BTN_GET_3 == pUIBtn->getTag())
	{
		// 请求服务器领取奖品
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)2);
	}
	else if (TAG_BTN_GET_4 == pUIBtn->getTag())
	{
		// 请求服务器领取奖品
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)3);
	}
	else if (TAG_BTN_GET_5 == pUIBtn->getTag())
	{
		// 请求服务器领取奖品
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)4);
	}
	else if (TAG_BTN_GET_6 == pUIBtn->getTag())
	{
		// 请求服务器领取奖品
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)5);
	}
	else if (TAG_BTN_GET_7 == pUIBtn->getTag())
	{
		// 请求服务器领取奖品
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)6);
	}
	else if (TAG_BTN_GET_8 == pUIBtn->getTag())
	{
		// 请求服务器领取奖品
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)7);
	}
}

void OnLineRewardUI::initUI()
{
	UIButton* pBtnClose = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnable(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addReleaseEvent(this, coco_releaseselector(OnLineRewardUI::callBackBtnClolse));
}

void OnLineRewardUI::refreshData()
{
	std::vector<OnlineGift *> vector_onlineGift = OnlineGiftData::instance()->m_vector_OnlineGift;

	for (unsigned int i = 0; i < vector_onlineGift.size(); i++)
	{
		char s_num_i[10];
		sprintf(s_num_i, "%d", i + 1);

		char s_num_1[10];
		sprintf(s_num_1, "%d", 1);

		char s_num_2[10];
		sprintf(s_num_2, "%d", 2);

		// m_label_timeFix
		std::string str_label_timeFix = "";
		str_label_timeFix.append("LabelBMFont_");
		str_label_timeFix.append(s_num_i);
		str_label_timeFix.append("_time");

		// m_btn_gift
		std::string str_btn_gift = "";
		str_btn_gift.append("Button_");
		str_btn_gift.append(s_num_i);

		// m_label_btnGift_text
		std::string str_label_btnGift_text = "";
		str_label_btnGift_text.append("Label_");
		str_label_btnGift_text.append(s_num_i);
		str_label_btnGift_text.append("_text");

		// m_image_red
		std::string str_image_red = "";
		str_image_red.append("ImageView_");
		str_image_red.append(s_num_i);
		str_image_red.append("_red");

		// m_image_text
		std::string str_image_text = "";
		str_image_text.append("ImageView_");
		str_image_text.append(s_num_i);
		str_image_text.append("_notime");

		// m_label_timeLeft
		std::string str_label_timeLeft = "";
		str_label_timeLeft.append("Label_");
		str_label_timeLeft.append(s_num_i);
		str_label_timeLeft.append("_notime");

		// m_image_gift1_frame
		std::string str_image_gift1_frame = "";
		str_image_gift1_frame.append("Button_");
		str_image_gift1_frame.append(s_num_i);
		str_image_gift1_frame.append("_gift");
		str_image_gift1_frame.append(s_num_1);

		// m_image_gift1_icon
		std::string str_image_gift1_icon = "";
		str_image_gift1_icon.append("ImageView_");
		str_image_gift1_icon.append(s_num_i);
		str_image_gift1_icon.append("_gift");
		str_image_gift1_icon.append(s_num_1);
		str_image_gift1_icon.append("_icon");

		// m_label_gift1_num
		std::string str_image_gift1_num = "";
		str_image_gift1_num.append("Label_");
		str_image_gift1_num.append(s_num_i);
		str_image_gift1_num.append("_gift");
		str_image_gift1_num.append(s_num_1);
		str_image_gift1_num.append("_num");

		// m_image_gift2_frame
		std::string str_image_gift2_frame = "";
		str_image_gift2_frame.append("Button_");
		str_image_gift2_frame.append(s_num_i);
		str_image_gift2_frame.append("_gift");
		str_image_gift2_frame.append(s_num_2);

		// m_image_gift2_icon
		std::string str_image_gift2_icon = "";
		str_image_gift2_icon.append("ImageView_");
		str_image_gift2_icon.append(s_num_i);
		str_image_gift2_icon.append("_gift");
		str_image_gift2_icon.append(s_num_2);
		str_image_gift2_icon.append("_icon");

		// m_label_gift2_num
		std::string str_image_gift2_num = "";
		str_image_gift2_num.append("Label_");
		str_image_gift2_num.append(s_num_i);
		str_image_gift2_num.append("_gift");
		str_image_gift2_num.append(s_num_2);
		str_image_gift2_num.append("_num");

		// 奖励领取的固定时间
		//vector_onlineGift.at(i)->m_BMFONT_timeFix = (UILabelBMFont *)UIHelper::seekWidgetByName(s_pPanel, str_label_timeFix.c_str());
		//vector_onlineGift.at(i)->m_BMFONT_timeFix->setVisible(true);

		// 领取按钮
		vector_onlineGift.at(i)->m_btn_gift = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, str_btn_gift.c_str());
		vector_onlineGift.at(i)->m_btn_gift->setTouchEnable(true);
		vector_onlineGift.at(i)->m_btn_gift->setPressedActionEnabled(true);
		vector_onlineGift.at(i)->m_btn_gift->addReleaseEvent(this, coco_releaseselector(OnLineRewardUI::callBackBtnGetGift));
		vector_onlineGift.at(i)->m_btn_gift->setTag(m_array_tagBtnGet[i]);

		// 领取按钮 或 不可领取 按钮的 文本
		vector_onlineGift.at(i)->m_label_btnGift_text = (UILabel*)UIHelper::seekWidgetByName(s_pPanel, str_label_btnGift_text.c_str());
		vector_onlineGift.at(i)->m_label_btnGift_text->setVisible(true);

		// 高亮显示的底图（红色）
		vector_onlineGift.at(i)->m_image_red = (UIImageView*)UIHelper::seekWidgetByName(s_pPanel, str_image_red.c_str());
		vector_onlineGift.at(i)->m_image_red->setVisible(false);

		// 时间未到哦 的底图
		vector_onlineGift.at(i)->m_image_text = (UIImageView*)UIHelper::seekWidgetByName(s_pPanel, str_image_text.c_str());
		vector_onlineGift.at(i)->m_image_text->setVisible(false);

		// 时间未到哦 的文本
		vector_onlineGift.at(i)->m_label_timeLeft = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, str_label_timeLeft.c_str());
		vector_onlineGift.at(i)->m_label_timeLeft->setColor(ccc3(255, 246, 0));
		vector_onlineGift.at(i)->m_label_timeLeft->setVisible(false);

		// goods的底图框，iocn 和 数量
		vector_onlineGift.at(i)->m_btn_gift1_frame = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, str_image_gift1_frame.c_str());
		vector_onlineGift.at(i)->m_image_gift1_icon = (UIImageView*)UIHelper::seekWidgetByName(s_pPanel, str_image_gift1_icon.c_str());
		vector_onlineGift.at(i)->m_label_gift1_num = (UILabel*)UIHelper::seekWidgetByName(s_pPanel, str_image_gift1_num.c_str());

		// goods的底图框，iocn 和 数量
		vector_onlineGift.at(i)->m_btn_gift2_frame = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, str_image_gift2_frame.c_str());
		vector_onlineGift.at(i)->m_image_gift2_icon = (UIImageView*)UIHelper::seekWidgetByName(s_pPanel, str_image_gift2_icon.c_str());
		vector_onlineGift.at(i)->m_label_gift2_num = (UILabel*)UIHelper::seekWidgetByName(s_pPanel, str_image_gift2_num.c_str());

		vector_onlineGift.at(i)->m_btn_gift1_frame->setVisible(false);
		vector_onlineGift.at(i)->m_image_gift1_icon->setVisible(false);
		vector_onlineGift.at(i)->m_label_gift1_num->setVisible(false);

		vector_onlineGift.at(i)->m_btn_gift2_frame->setVisible(false);
		vector_onlineGift.at(i)->m_image_gift2_icon->setVisible(false);
		vector_onlineGift.at(i)->m_label_gift2_num->setVisible(false);

		vector_onlineGift.at(i)->m_btn_gift1_frame->setScale(0.8f);
		vector_onlineGift.at(i)->m_btn_gift2_frame->setScale(0.8f);

		vector_onlineGift.at(i)->m_btn_gift1_frame->setTouchEnable(true);
		vector_onlineGift.at(i)->m_btn_gift2_frame->setTouchEnable(true);
		vector_onlineGift.at(i)->m_btn_gift1_frame->setTag(m_array_tagBtnGift1[i]);
		vector_onlineGift.at(i)->m_btn_gift2_frame->setTag(m_array_tagBtnGift2[i]);

		vector_onlineGift.at(i)->m_btn_gift1_frame->addReleaseEvent(this, coco_releaseselector(OnLineRewardUI::callBackBtnGiftInfo));
		vector_onlineGift.at(i)->m_btn_gift2_frame->addReleaseEvent(this, coco_releaseselector(OnLineRewardUI::callBackBtnGiftInfo));

		vector_onlineGift.at(i)->m_btn_gift1_frame->setPressedActionEnabled(true);
		vector_onlineGift.at(i)->m_btn_gift2_frame->setPressedActionEnabled(true);

		//initGiftFixTime(vector_onlineGift.at(i));
		initBtnStatus(vector_onlineGift.at(i));
		initGoodsInfo(vector_onlineGift.at(i));

	}

	// 当前正在进行的 UI初始化
	//update(0);

}

void OnLineRewardUI::initBtnStatus( OnlineGift * onlineGift )
{
	/** 奖励物品状态 ：1.不可领取奖励，2.可领取奖励，3.已领取奖励完毕*/
	if (1 == onlineGift->m_nStatus)
	{
		onlineGift->m_btn_gift->setVisible(false);

		onlineGift->m_label_btnGift_text->setVisible(false);

		onlineGift->m_image_text->setVisible(true);
		onlineGift->m_label_timeLeft->setVisible(true);

		const char *str_num = StringDataManager::getString("label_minute");

		int nMillionSecond = onlineGift->m_nTimeAll;
		int nMinuteShow = nMillionSecond / 60000;
		char strTmp[20];
		sprintf(strTmp, "%d", nMinuteShow);

		std::string str_minute = "";
		str_minute.append(strTmp);
		str_minute.append(str_num);
		onlineGift->m_label_timeLeft->setText(str_minute.c_str());

	}
	else if (2 == onlineGift->m_nStatus)
	{
		onlineGift->m_btn_gift->setVisible(true);
		onlineGift->m_btn_gift->setTouchEnable(true);
		onlineGift->m_btn_gift->setTextures("res_ui/new_button_2.png", "res_ui/new_button_2.png", "");

		onlineGift->m_label_btnGift_text->setVisible(true);

		const char *str1 = StringDataManager::getString("btn_lingqu");
		onlineGift->m_label_btnGift_text->setText(str1);


		onlineGift->m_image_text->setVisible(false);
		onlineGift->m_label_timeLeft->setVisible(false);
		onlineGift->m_label_timeLeft->setText("");
	}
	else if (3 == onlineGift->m_nStatus)
	{
		onlineGift->m_btn_gift->setVisible(true);
		onlineGift->m_btn_gift->setTouchEnable(false);
		onlineGift->m_btn_gift->setTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");

		onlineGift->m_label_btnGift_text->setVisible(true);

		const char *str1 = StringDataManager::getString("btn_yilingqu");
		onlineGift->m_label_btnGift_text->setText(str1);

		onlineGift->m_image_text->setVisible(false);
		onlineGift->m_label_timeLeft->setVisible(false);
		onlineGift->m_label_timeLeft->setText("");
	}
}

int OnLineRewardUI::getGiftNeedTime()
{
	std::vector<COneOnlineGift *> vector_oneOnLineGift = OnlineGiftData::instance()->m_vector_oneOnLineGift;
	for (unsigned int i=0; i < vector_oneOnLineGift.size(); i++)
	{

		int nGiftNum =  vector_oneOnLineGift.at(i)->number();
		int nGiftNeddTime =  vector_oneOnLineGift.at(i)->needtime();
		int nGiftStatus = vector_oneOnLineGift.at(i)->status();	

		// 如果当前状态 为 已领取，则查询下一个奖励；否则的话 当前奖励 为 即将领取的奖励
		if (3 == nGiftStatus)
		{
			continue;
		}
		else
		{
			m_nOnlineGiftTime = nGiftNeddTime;
			return nGiftNeddTime;
		}
	}

	return 0;
}

int OnLineRewardUI::getGiftNum()
{
	std::vector<COneOnlineGift *> vector_oneOnLineGift = OnlineGiftData::instance()->m_vector_oneOnLineGift;
	for (unsigned int i=0; i < vector_oneOnLineGift.size(); i++)
	{

		int nGiftNum =  vector_oneOnLineGift.at(i)->number();
		int nGiftNeddTime =  vector_oneOnLineGift.at(i)->needtime();
		int nGiftStatus = vector_oneOnLineGift.at(i)->status();	

		// 如果当前状态 为 已领取，则查询下一个奖励；否则的话 当前奖励 为 即将领取的奖励
		if (3 == nGiftStatus)
		{
			continue;
		}
		else
		{
			m_nGiftNum = nGiftNum;
			return nGiftNum;
		}
	}

	return -1;
}

void OnLineRewardUI::giftGetSuc( const int nGiftNum, const int nGiftStatus )
{
	std::vector<COneOnlineGift *> vector_oneOnLineGift = OnlineGiftData::instance()->m_vector_oneOnLineGift;
	for (unsigned int i = 0; i < vector_oneOnLineGift.size(); i++)
	{
		// 前面的 奖励 全部 时间设为 0，status 设为 已领取（3）
		if (vector_oneOnLineGift.at(i)->number() < vector_oneOnLineGift.at(nGiftNum)->number())
		{
			vector_oneOnLineGift.at(i)->set_needtime(0);
			vector_oneOnLineGift.at(i)->set_status(3);
		}
		else if (vector_oneOnLineGift.at(i)->number()  ==  vector_oneOnLineGift.at(nGiftNum)->number())		// 设置 当前奖励编号为nNum的,status设为 已领取（3）
		{
			vector_oneOnLineGift.at(i)->set_needtime(0);
			vector_oneOnLineGift.at(i)->set_status(3);

			if (i < vector_oneOnLineGift.size() - 1)
			{
				// 开始nGiftNum + 1 的奖励计时(下面为 num 和 time)
				m_nGiftNum = this->getGiftNum();
				m_nOnlineGiftTime = this->getGiftNeedTime();


				// 把nGiftNum + 1的数据 同步给 icon
				GuideMap * pGuideMap = (GuideMap *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
				if (NULL != pGuideMap)
				{
					if (pGuideMap->getActionLayer())
					{
						OnlineGiftIconWidget* pOnlineGiftIconWidget = (OnlineGiftIconWidget *)pGuideMap->getActionLayer()->getWidgetByTag(kTagOnLineGiftIcon);
						if (NULL != pOnlineGiftIconWidget)
						{
							pOnlineGiftIconWidget->initGiftTime(vector_oneOnLineGift.at(i + 1)->needtime());
							pOnlineGiftIconWidget->initGiftNum(vector_oneOnLineGift.at(i + 1)->number());
						}
					}
				}

				// 把nGiftNum + 1的数据 同步给 layer
				OnlineGiftLayer * pOnlineGiftLayer = (OnlineGiftLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftLayer);
				if (NULL != pOnlineGiftLayer)
				{
					pOnlineGiftLayer->initGiftTime(vector_oneOnLineGift.at(i + 1)->needtime());
					pOnlineGiftLayer->initGiftNum(vector_oneOnLineGift.at(i + 1)->number());
				}
			}
			else
			{
				// 所有奖励领取完毕，(1)将数据同步给icon (2)icon消失
				GuideMap * pGuideMap = (GuideMap *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
				if (NULL != pGuideMap)
				{
					if (pGuideMap->getActionLayer())
					{
						OnlineGiftIconWidget* pOnlineGiftIconWidget = (OnlineGiftIconWidget *)pGuideMap->getActionLayer()->getWidgetByTag(kTagOnLineGiftIcon);
						if (NULL != pOnlineGiftIconWidget)
						{
							pOnlineGiftIconWidget->initGiftTime(0);
							pOnlineGiftIconWidget->initGiftNum(8);

							pOnlineGiftIconWidget->allGiftGetIconExit();
						}
					}	
				}

				// 把nGiftNum + 1的数据 同步给 layer
				OnlineGiftLayer * pOnlineGiftLayer = (OnlineGiftLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftLayer);
				if (NULL != pOnlineGiftLayer)
				{
					pOnlineGiftLayer->initGiftTime(0);
					pOnlineGiftLayer->initGiftNum(8);

					pOnlineGiftLayer->allGiftGetLayerExit();
				}

				m_nGiftNum = 8;
				m_nOnlineGiftTime = 0;

			}
		}
	}

	OnlineGiftData::instance()->initDataFromIntent();

	// 更新window UI
	refreshData();
}

void OnLineRewardUI::update( float dt )
{
	m_nOnlineGiftTime -= 1000;
	m_nTimeShow = m_nOnlineGiftTime;
	if (m_nTimeShow <= 0)
	{
		m_nTimeShow = 0;
	}

	refreshTime();
}

void OnLineRewardUI::refreshTime()
{
	std::vector<OnlineGift *> vector_onlineGift = OnlineGiftData::instance()->m_vector_OnlineGift;
	if (m_nGiftNum < vector_onlineGift.size())
	{
		vector_onlineGift.at(m_nGiftNum)->m_image_red->setVisible(true);

		if (0 == m_nTimeShow)
		{
			// 奖励可领取
			vector_onlineGift.at(m_nGiftNum)->m_image_text->setVisible(false);
			vector_onlineGift.at(m_nGiftNum)->m_label_timeLeft->setVisible(false);
			vector_onlineGift.at(m_nGiftNum)->m_label_timeLeft->setText("");

			vector_onlineGift.at(m_nGiftNum)->m_btn_gift->setVisible(true);
			vector_onlineGift.at(m_nGiftNum)->m_btn_gift->setEnabled(true);
			vector_onlineGift.at(m_nGiftNum)->m_btn_gift->setTextures("res_ui/new_button_2.png", "res_ui/new_button_2.png", "");

			const char *str1 = StringDataManager::getString("btn_lingqu");
			vector_onlineGift.at(m_nGiftNum)->m_label_btnGift_text->setVisible(true);
			vector_onlineGift.at(m_nGiftNum)->m_label_btnGift_text->setText(str1);

			// 奖励可领取的话，加上 粒子特效
			CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
			if (NULL == tutorialParticle)
			{
				UIButton * btn_gift = vector_onlineGift.at(m_nGiftNum)->m_btn_gift;

				m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", btn_gift->getContentSize().width + 25, btn_gift->getContentSize().height + 5);
				m_tutorialParticle->setPosition(ccp(btn_gift->getPosition().x, btn_gift->getPosition().y - 22));
				m_tutorialParticle->setAnchorPoint(ccp(0.5f, 0.5f));
				m_tutorialParticle->setTag(TAG_TUTORIALPARTICLE);

				this->addChild(m_tutorialParticle);
			}
		}
		else
		{
			// 当前奖励剩余时间
			vector_onlineGift.at(m_nGiftNum)->m_image_text->setVisible(true);
			vector_onlineGift.at(m_nGiftNum)->m_label_timeLeft->setVisible(true);
			vector_onlineGift.at(m_nGiftNum)->m_label_timeLeft->setText(timeFormatToString(m_nTimeShow).c_str());

			vector_onlineGift.at(m_nGiftNum)->m_btn_gift->setVisible(false);

			// 去除 环绕按钮的粒子特效效果
			CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
			if (NULL != tutorialParticle)
			{
				this->removeChild(m_tutorialParticle);
			}
			
		}
	}
	else
	{
		// 奖品已经全部领取

		// 设置全部的 高亮显示红色消失；设置全部的 按钮为已领取
		for (unsigned int i = 0; i < vector_onlineGift.size(); i++)
		{
			vector_onlineGift.at(i)->m_btn_gift->setVisible(true);
			vector_onlineGift.at(i)->m_btn_gift->setTouchEnable(false);
			vector_onlineGift.at(i)->m_btn_gift->setTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");

			vector_onlineGift.at(i)->m_label_btnGift_text->setVisible(true);

			const char *str1 = StringDataManager::getString("btn_yilingqu");
			vector_onlineGift.at(i)->m_label_btnGift_text->setText(str1);

			vector_onlineGift.at(i)->m_image_text->setVisible(false);
			vector_onlineGift.at(i)->m_label_timeLeft->setVisible(false);
			vector_onlineGift.at(i)->m_label_timeLeft->setText("");

			// 高亮显示红色消失
			vector_onlineGift.at(i)->m_image_red->setVisible(false);
		}

		// 去除 环绕按钮的粒子特效效果
		CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL != tutorialParticle)
		{
			this->removeChild(m_tutorialParticle);
		}

	}
}

std::string OnLineRewardUI::timeFormatToString( int nTime )
{
	int int_m = nTime / 1000 / 60;
	int int_s = nTime / 1000 % 60;

	std::string timeString = "";
	char str_m[10];
	if (int_m < 10)
	{
		timeString.append("0");
	}
	sprintf(str_m,"%d",int_m);
	timeString.append(str_m);
	timeString.append(":");

	char str_s[10];
	if (int_s < 10)
	{
		timeString.append("0");
	}
	sprintf(str_s,"%d",int_s);
	timeString.append(str_s);

	return timeString;
}

void OnLineRewardUI::refreshGiftTime()
{
	m_nTimeShow = this->getGiftNeedTime();
	m_nOnlineGiftTime =  this->getGiftNeedTime();
}

void OnLineRewardUI::refreshGiftNum()
{
	m_nGiftNum =  this->getGiftNum();
}

void OnLineRewardUI::refreshTimeAndNum()
{
	this->refreshGiftTime();
	this->refreshGiftNum();
}

void OnLineRewardUI::initGoodsInfo( OnlineGift* onlineGift )
{
	// 设置 goods icon 和 frame
	std::vector<GoodsInfo *> vector_goods = onlineGift->m_vector_goods;
	for (unsigned int i = 0; i < vector_goods.size(); i++)
	{
		std::string goods_icon = vector_goods.at(i)->icon();

		std::string goodsIcon_ = "res_ui/props_icon/";
		goodsIcon_.append(goods_icon);
		goodsIcon_.append(".png");

		// 根据goods品阶选择底图
		int quality_ = vector_goods.at(i)->quality();
		std::string qualityStr_ = getEquipmentQualityByIndex(quality_);

		if (0 == i)
		{
			onlineGift->m_image_gift1_icon->setTexture(goodsIcon_.c_str());
			onlineGift->m_image_gift1_icon->setVisible(true);
			//onlineGift->m_image_gift1_frame->setTexture(qualityStr_.c_str());
			onlineGift->m_btn_gift1_frame->loadTextures(qualityStr_.c_str(), qualityStr_.c_str(), qualityStr_.c_str());
			onlineGift->m_btn_gift1_frame->setVisible(true);
		}
		else if (1 == i)
		{
			onlineGift->m_image_gift2_icon->setTexture(goodsIcon_.c_str());
			onlineGift->m_image_gift2_icon->setVisible(true);
			//onlineGift->m_image_gift2_frame->setTexture(qualityStr_.c_str());
			onlineGift->m_btn_gift2_frame->loadTextures(qualityStr_.c_str(), qualityStr_.c_str(), qualityStr_.c_str());
			onlineGift->m_btn_gift2_frame->setVisible(true);
		}
	}

	// 设置 goods 数量
	std::vector<int > vector_goodsNum = onlineGift->m_vector_goodsNum;
	for (unsigned int j = 0; j < vector_goodsNum.size(); j++)
	{
		int goods_num = vector_goodsNum.at(j);

		char s[10];
		sprintf(s, "%d", goods_num);

		if (0 == j)
		{
			onlineGift->m_label_gift1_num->setText(s);
			onlineGift->m_label_gift1_num->setVisible(true);
		}
		else if (1 == j)
		{
			onlineGift->m_label_gift2_num->setText(s);
			onlineGift->m_label_gift2_num->setVisible(true);
		}
	}
}

void OnLineRewardUI::callBackBtnGiftInfo( CCObject* obj )
{
	UIButton * pUIImageView = (UIButton *)obj;
	CCSize size = CCDirector::sharedDirector()->getVisibleSize();

	int nGift = 0;
	int nGiftNum = 0;

	// 通过tag判断是第几个奖励被点击
	if (TAG_BTN_GIFT_1_1 == pUIImageView->getTag())
	{
		nGift = 0;
		nGiftNum = 0;
	}
	else if (TAG_BTN_GIFT_1_2 == pUIImageView->getTag())
	{
		nGift = 0;
		nGiftNum = 1;
	}
	else if (TAG_BTN_GIFT_2_1 == pUIImageView->getTag())
	{
		nGift = 1;
		nGiftNum = 0;
	}
	else if (TAG_BTN_GIFT_2_2 == pUIImageView->getTag())
	{
		nGift = 1;
		nGiftNum = 1;
	}
	else if (TAG_BTN_GIFT_3_1 == pUIImageView->getTag())
	{
		nGift = 2;
		nGiftNum = 0;
	}
	else if (TAG_BTN_GIFT_3_2 == pUIImageView->getTag())
	{
		nGift = 2;
		nGiftNum = 1;
	}
	else if (TAG_BTN_GIFT_4_1 == pUIImageView->getTag())
	{
		nGift = 3;
		nGiftNum = 0;
	}
	else if (TAG_BTN_GIFT_4_2 == pUIImageView->getTag())
	{
		nGift = 3;
		nGiftNum = 1;
	}
	else if (TAG_BTN_GIFT_5_1 == pUIImageView->getTag())
	{
		nGift = 4;
		nGiftNum = 0;
	}
	else if (TAG_BTN_GIFT_5_2 == pUIImageView->getTag())
	{
		nGift = 4;
		nGiftNum = 1;
	}
	else if (TAG_BTN_GIFT_6_1 == pUIImageView->getTag())
	{
		nGift = 5;
		nGiftNum = 0;
	}
	else if (TAG_BTN_GIFT_6_2 == pUIImageView->getTag())
	{
		nGift = 5;
		nGiftNum = 1;
	}
	else if (TAG_BTN_GIFT_7_1 == pUIImageView->getTag())
	{
		nGift = 6;
		nGiftNum = 0;
	}
	else if (TAG_BTN_GIFT_7_2 == pUIImageView->getTag())
	{
		nGift = 6;
		nGiftNum = 1;
	}
	else if (TAG_BTN_GIFT_8_1 == pUIImageView->getTag())
	{
		nGift = 7;
		nGiftNum = 0;
	}
	else if (TAG_BTN_GIFT_8_2 == pUIImageView->getTag())
	{
		nGift = 7;
		nGiftNum = 1;
	}

	GoodsInfo * pGoodsInfo = OnlineGiftData::instance()->getGoosInfo(nGift, nGiftNum);
	GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(pGoodsInfo,GameView::getInstance()->EquipListItem,0);
	goodsItemInfoBase->ignoreAnchorPointForPosition(false);
	goodsItemInfoBase->setAnchorPoint(ccp(0.5f, 0.5f));
	//goodsItemInfoBase->setPosition(ccp(size.width / 2, size.height / 2));

	GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
	
}

std::string OnLineRewardUI::getEquipmentQualityByIndex( int quality )
{
	std::string frameColorPath = "res_ui/";
	switch(quality)
	{
	case 1:
		{
			frameColorPath.append("sdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("sdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("sdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("sdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("sdi_orange");
		}break;
	}
	frameColorPath.append(".png");
	return frameColorPath;
}

void OnLineRewardUI::initGiftFixTime( OnlineGift * onlineGift )
{
	int nMillionSecond = onlineGift->m_nTimeAll;

	int nMinuteShow = nMillionSecond / 60000;
	char str[10];
	sprintf(str, "%d", nMinuteShow);
	onlineGift->m_BMFONT_timeFix->setText(str);
}



