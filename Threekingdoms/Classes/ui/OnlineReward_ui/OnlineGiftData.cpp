#include "OnlineGiftData.h"
#include "../../messageclient/element/COneOnlineGift.h"
#include "OnlineGift.h"
#include "../../messageclient/element/GoodsInfo.h"

OnlineGiftData * OnlineGiftData::s_onlineGiftData = NULL;

OnlineGiftData::OnlineGiftData(void)
{
}


OnlineGiftData::~OnlineGiftData(void)
{
	std::vector<COneOnlineGift *>::iterator iter;
	for (iter = m_vector_oneOnLineGift.begin(); iter != m_vector_oneOnLineGift.end(); iter++)
	{
		delete *iter;
	}
	m_vector_oneOnLineGift.clear();

	std::vector<OnlineGift *>::iterator iterOnlineGift;
	for (iterOnlineGift = m_vector_OnlineGift.begin(); iterOnlineGift != m_vector_OnlineGift.end(); iterOnlineGift++)
	{
		delete *iterOnlineGift;
	}
	m_vector_OnlineGift.clear();
}

OnlineGiftData * OnlineGiftData::instance()
{
	if (NULL == s_onlineGiftData)
	{
		s_onlineGiftData = new OnlineGiftData();
	}

	return s_onlineGiftData;
}

int OnlineGiftData::initGetGiftNeedTime()
{
	for (unsigned int i=0; i < m_vector_oneOnLineGift.size(); i++)
	{

		int nGiftNum =  m_vector_oneOnLineGift.at(i)->number();
		int nGiftNeddTime =  m_vector_oneOnLineGift.at(i)->needtime();
		int nGiftStatus = m_vector_oneOnLineGift.at(i)->status();	

		// 如果当前状态 为 已领取，则查询下一个奖励；否则的话 当前奖励 为 即将领取的奖励
		if (3 == nGiftStatus)
		{
			continue;
		}
		else
		{
			return nGiftNeddTime;
		}
	}

	return 0;
}

int OnlineGiftData::initGetGiftNum()
{
	for (unsigned int i=0; i < m_vector_oneOnLineGift.size(); i++)
	{

		int nGiftNum =  m_vector_oneOnLineGift.at(i)->number();
		int nGiftNeddTime =  m_vector_oneOnLineGift.at(i)->needtime();
		int nGiftStatus = m_vector_oneOnLineGift.at(i)->status();	

		// 如果当前状态 为 已领取，则查询下一个奖励；否则的话 当前奖励 为 即将领取的奖励
		if (3 == nGiftStatus)
		{
			continue;
		}
		else
		{
			return nGiftNum;
		}
	}

	// 返回8，说明所有奖励全部领取完毕
	return 8;
}

void OnlineGiftData::getGiftDataTimeFromIcon( const int nTime, const unsigned int nNum )
{
	// 如果nNum小于m_vector_oneOnLineGift.size(),说明 奖励合理；否则，奖励全部领取完毕
	if (nNum < m_vector_oneOnLineGift.size())
	{
		for (unsigned int i = 0; i < m_vector_oneOnLineGift.size(); i++)
		{
			// 前面的 奖励 全部 时间设为 0，status 设为 已领取（3）
			if (m_vector_oneOnLineGift.at(i)->number() < m_vector_oneOnLineGift.at(nNum)->number())
			{
				m_vector_oneOnLineGift.at(i)->set_needtime(0);
				m_vector_oneOnLineGift.at(i)->set_status(3);
			}
			else if (m_vector_oneOnLineGift.at(i)->number()  ==  m_vector_oneOnLineGift.at(nNum)->number())		// 设置 当前奖励编号为nNum的剩余时间为nTime,status设为 可领取（2）
			{
				m_vector_oneOnLineGift.at(i)->set_needtime(nTime);

				// 如果剩余时间为0，则为可领取（2）；否则为不可领取（1）
				if (0 == nTime)
				{
					m_vector_oneOnLineGift.at(i)->set_status(2);
				}
				else
				{
					m_vector_oneOnLineGift.at(i)->set_status(1);
				}
			}

		}
	}
	else
	{
		for (unsigned int i = 0; i < m_vector_oneOnLineGift.size(); i++)
		{
			m_vector_oneOnLineGift.at(i)->set_needtime(0);
			m_vector_oneOnLineGift.at(i)->set_status(3);
		}
	}
}

void OnlineGiftData::initDataFromIntent()
{
	// 清空数据
	std::vector<OnlineGift *>::iterator iter;
	for (iter = m_vector_OnlineGift.begin(); iter != m_vector_OnlineGift.end(); iter++)
	{
		delete *iter;
	}
	m_vector_OnlineGift.clear();

	for (unsigned int i=0; i < m_vector_oneOnLineGift.size(); i++)
	{
		OnlineGift * onlineGift = new OnlineGift();

		onlineGift->m_nGiftNum = m_vector_oneOnLineGift.at(i)->number();							// 奖品编号，从0开始
		onlineGift->m_nTimeLeft = m_vector_oneOnLineGift.at(i)->needtime();						// 需要领取奖品时间
		onlineGift->m_nStatus = m_vector_oneOnLineGift.at(i)->status();								// 奖品领取状态
		onlineGift->m_nTimeAll = m_vector_oneOnLineGift.at(i)->fixtime();							// 奖品领取需要的固定时间

		int goodsSize = m_vector_oneOnLineGift.at(i)->goods_size();
		for (int goodsIndex_ =0; goodsIndex_ < goodsSize; goodsIndex_++)
		{
			GoodsInfo* goodsInfo = new GoodsInfo();
			goodsInfo->CopyFrom(m_vector_oneOnLineGift.at(i)->goods(goodsIndex_));
			onlineGift->m_vector_goods.push_back(goodsInfo);
		}

		int goodsNumSize = m_vector_oneOnLineGift.at(i)->goodsnumber_size();
		for (int goodsNumIndex = 0; goodsNumIndex < goodsNumSize; goodsNumIndex++)
		{
			onlineGift->m_vector_goodsNum.push_back(m_vector_oneOnLineGift.at(i)->goodsnumber(goodsNumIndex));		// goods num
		}

		m_vector_OnlineGift.push_back(onlineGift);
	}
}

bool OnlineGiftData::isAllGiftHasGet()
{
	for (unsigned int i=0; i < m_vector_oneOnLineGift.size(); i++)
	{
		int nGiftStatus = m_vector_oneOnLineGift.at(i)->status();	

		// 如果遍历的 奖励状态全部为 已领取，
		if (3 == nGiftStatus)
		{
			continue;
		}
		else
		{
			return false;
		}

	}

	return true;
}

GoodsInfo* OnlineGiftData::getGoosInfo(unsigned int nGift, int nGiftNum )
{
	for (unsigned int i = 0; i < m_vector_OnlineGift.size(); i++)
	{
		if (nGift < m_vector_OnlineGift.size())
		{
			return m_vector_OnlineGift.at(nGift)->m_vector_goods.at(nGiftNum);
		}
	}

	return NULL;
}


