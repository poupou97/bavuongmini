#ifndef _UI_ONLINEREWARD_ONLINEGIFTLAYER_H_
#define _UI_ONLINEREWARD_ONLINEGIFTLAYER_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 在线奖励layer（用于监测领取状态）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.08.01
 */


class OnlineGiftLayer : public cocos2d::CCLayer
{
public:
	static OnlineGiftLayer * s_onLineGiftLayer;
	static OnlineGiftLayer * instance();

	static OnlineGiftLayer* create();

	virtual bool init();
	virtual void update(float dt);

private:
	OnlineGiftLayer(void);
	~OnlineGiftLayer(void);

private:
	bool m_bIsUseful;
	std::string m_str_timeLeft;

public:
	void setLayerUserful(bool bFlag);

public:
	void initGiftTime(const int nTime);							// 当前奖励 还需 nTime 毫秒可领取
	void initGiftNum(const int nGiftNum);					// 当前奖励的编号

	void initDataFromIntent();										// 从服务器数据初始化Icon

	void allGiftGetLayerExit();										// 所有奖励已成功领取，layer移除（或者等级达到上限，layer移除）

	int m_nTimeShow;												// 显示在面板的 该奖励可领取的时间
	int m_nOnlineGiftTime;											// 服务器所获取的 该奖励 领取 所需时间
	int m_nGiftNum;													// 当前显示奖励 的 编号

	float m_timeDelta;

public:
	int get_giftTime();
	int get_giftNum();

	std::string get_timeLeft();

private:
	std::string timeFormatToString(int nTime);
	void refreshData();
};

#endif

