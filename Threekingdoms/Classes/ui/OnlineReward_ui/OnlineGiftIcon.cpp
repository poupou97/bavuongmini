#include "OnlineGiftIcon.h"
#include "OnLineRewardUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "OnlineGiftData.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/CCTutorialParticle.h"

#define  TAG_TUTORIALPARTICLE 500

OnlineGiftIcon::OnlineGiftIcon(void)
	:m_nOnlineGiftTime(0)
	,m_nTimeShow(0)
	,m_btn_onlineGiftIcon(NULL)
	,m_layer_onlineGift(NULL)
	,m_label_onlineGift_time(NULL)
	,m_spirte_fontBg(NULL)
{

}


OnlineGiftIcon::~OnlineGiftIcon(void)
{
	OnlineGiftData::instance()->getGiftDataTimeFromIcon(m_nTimeShow, m_nGiftNum);
}

OnlineGiftIcon* OnlineGiftIcon::create()
{
	OnlineGiftIcon * onlineGiftIcon = new OnlineGiftIcon();
	if (onlineGiftIcon && onlineGiftIcon->init())
	{
		onlineGiftIcon->autorelease();
		return onlineGiftIcon;
	}
	CC_SAFE_DELETE(onlineGiftIcon);
	return NULL;
}

bool OnlineGiftIcon::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		// icon
		m_btn_onlineGiftIcon = UIButton::create();
		m_btn_onlineGiftIcon->setTouchEnable(true);
		m_btn_onlineGiftIcon->setPressedActionEnabled(true);
		m_btn_onlineGiftIcon->setTextures("gamescene_state/zhujiemian3/tubiao/OnlineAwards.png", "gamescene_state/zhujiemian3/tubiao/OnlineAwards.png","");
		m_btn_onlineGiftIcon->setAnchorPoint(ccp(0.5f, 0.5f));
		m_btn_onlineGiftIcon->setPosition(ccp( winSize.width-70-159, winSize.height-70));
		m_btn_onlineGiftIcon->addReleaseEvent(this, coco_cancelselector(OnlineGiftIcon::callBackBtnOnlineGift));

		// label
		m_label_onlineGift_time = UILabel::create();
		m_label_onlineGift_time->setText("00:00");
		m_label_onlineGift_time->setColor(ccc3(255, 246, 0));
		m_label_onlineGift_time->setFontSize(14);
		m_label_onlineGift_time->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_onlineGift_time->setPosition(ccp(m_btn_onlineGiftIcon->getPosition().x, m_btn_onlineGiftIcon->getPosition().y - m_btn_onlineGiftIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = CCScale9Sprite::create("res_ui/zhezhao70.png");//CCRectMake(11, 8, 1, 17)
		m_spirte_fontBg->setPreferredSize(CCSize(47, 14));
		m_spirte_fontBg->setAnchorPoint(ccp(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_onlineGift_time->getPosition());


		m_layer_onlineGift = UILayer::create();
		m_layer_onlineGift->setTouchEnabled(true);
		m_layer_onlineGift->setAnchorPoint(CCPointZero);
		m_layer_onlineGift->setPosition(CCPointZero);
		m_layer_onlineGift->setContentSize(winSize);

		m_layer_onlineGift->addChild(m_spirte_fontBg, -10);
		m_layer_onlineGift->addWidget(m_btn_onlineGiftIcon);
		m_layer_onlineGift->addWidget(m_label_onlineGift_time);


		addChild(m_layer_onlineGift);

		this->schedule(schedule_selector(OnlineGiftIcon::update),1.0f);

		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void OnlineGiftIcon::callBackBtnOnlineGift( CCObject *obj )
{
	// 将Icon最新的数据 同步给 服务器数据
	OnlineGiftData::instance()->getGiftDataTimeFromIcon(m_nTimeShow, m_nGiftNum);
	OnlineGiftData::instance()->initDataFromIntent();
	

	// 初始化 UI WINDOW数据
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	OnLineRewardUI* pOnLineRewardUI = OnLineRewardUI::create();
	pOnLineRewardUI->ignoreAnchorPointForPosition(false);
	pOnLineRewardUI->setAnchorPoint(ccp(0.5f, 0.5f));
	pOnLineRewardUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	pOnLineRewardUI->setTag(kTagOnLineGiftUI);

	pOnLineRewardUI->refreshData();
	pOnLineRewardUI->refreshTimeAndNum();					// 更新 奖励编号 和 时间
	pOnLineRewardUI->update(0);

	GameView::getInstance()->getMainUIScene()->addChild(pOnLineRewardUI);
}

void OnlineGiftIcon::onEnter()
{
	UIScene::onEnter();
}

void OnlineGiftIcon::onExit()
{
	UIScene::onExit();
}

void OnlineGiftIcon::initGiftTime(const int nTime )
{
	m_nTimeShow = nTime;
	m_nOnlineGiftTime = nTime;

	refreshData();
}


void OnlineGiftIcon::initGiftNum( const int nGiftNum )
{
	m_nGiftNum = nGiftNum;
}

void OnlineGiftIcon::update( float dt )
{
	m_nOnlineGiftTime -= 1000;
	m_nTimeShow = m_nOnlineGiftTime;
	if (m_nTimeShow <= 0)
	{
		m_nTimeShow = 0;
	}

	refreshData();
}

std::string OnlineGiftIcon::timeFormatToString( int nTime )
{
	if (0 == nTime)
	{
		std::string timeString = "";
		const char *str1 = StringDataManager::getString("label_timeout");
		timeString.append(str1);
		return timeString;
	}
	else
	{
		int int_m = nTime / 1000 / 60;
		int int_s = nTime / 1000 % 60;

		std::string timeString = "";
		char str_m[10];
		if (int_m < 10)
		{
			timeString.append("0");
		}
		sprintf(str_m,"%d",int_m);
		timeString.append(str_m);
		timeString.append(":");

		char str_s[10];
		if (int_s < 10)
		{
			timeString.append("0");
		}
		sprintf(str_s,"%d",int_s);
		timeString.append(str_s);

		return timeString;
	}

}

void OnlineGiftIcon::refreshData()
{
	std::string strShow = timeFormatToString(m_nTimeShow);

	std::string timeString = "";
	const char *str1 = StringDataManager::getString("label_timeout");
	timeString.append(str1);

	// 判断 将要显示的是否为“可领取”，若是，则创建特效；否则 移除特效（如果先前的特效存在的话）
	if (strShow == timeString)
	{
		CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL == tutorialParticle)
		{
			m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", m_btn_onlineGiftIcon->getContentSize().width, m_btn_onlineGiftIcon->getContentSize().height + 10);
			m_tutorialParticle->setPosition(ccp(m_btn_onlineGiftIcon->getPosition().x, m_btn_onlineGiftIcon->getPosition().y - 35));
			m_tutorialParticle->setAnchorPoint(ccp(0.5f, 0.5f));
			m_tutorialParticle->setTag(TAG_TUTORIALPARTICLE);

			this->addChild(m_tutorialParticle);
		}
	}
	else 
	{
		CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL != tutorialParticle)
		{
			this->removeChild(m_tutorialParticle);
		}
	}

	m_label_onlineGift_time->setText(strShow.c_str());
}

void OnlineGiftIcon::initDataFromIntent()
{
	// 在线奖励ICON 获取数据
	this->initGiftTime(OnlineGiftData::instance()->initGetGiftNeedTime());
	this->initGiftNum(OnlineGiftData::instance()->initGetGiftNum());
}

void OnlineGiftIcon::allGiftGetIconExit()
{
	// 所有奖励获取成功，关闭icon窗体
	this->closeAnim();
}

