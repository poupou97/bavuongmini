#ifndef _UI_ONLINEREWARD_ONLINEGIFT_H_
#define _UI_ONLINEREWARD_ONLINEGIFT_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class GoodsInfo;

class OnlineGift
{
public:
	OnlineGift(void);
	~OnlineGift(void);
public:
	UIButton * m_btn_gift;						// btn
	UILabel * m_label_btnGift_text;			// btn 文本

	UIImageView * m_image_red;			// 当前正在进行的 奖励（红色背景高亮显示）

	UIImageView * m_image_text;			// 时间未到哦 image
	UILabel * m_label_timeLeft;				// 剩余时间 或者 时间未到哦		
			
	UIButton * m_btn_gift1_frame;
	UIImageView * m_image_gift1_icon;
	UILabel * m_label_gift1_num;
	
	UIButton * m_btn_gift2_frame;
	UIImageView * m_image_gift2_icon;
	UILabel * m_label_gift2_num;

	UILabelBMFont * m_BMFONT_timeFix;				// 固定时间

	int m_nTimeLeft;								// 当前奖励 剩余时间
	int m_nTimeAll;									// 当前奖励 固定时间
	int m_nGiftNum;								// 奖励编号
	int m_nStatus;									// 1:不可领取；2：可领取；3：已领取完毕

	std::vector<GoodsInfo *> m_vector_goods;
	std::vector<int > m_vector_goodsNum;
};

#endif

