#ifndef _UI_ONLINEREWARD_ONLINEGIFTDATA_H_
#define _UI_ONLINEREWARD_ONLINEGIFTDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class COneOnlineGift;
class OnlineGift;
class GoodsInfo;

class OnlineGiftData
{
public:
	static OnlineGiftData * s_onlineGiftData;
	static OnlineGiftData * instance();
private:
	OnlineGiftData(void);
	~OnlineGiftData(void);

public:
	std::vector<COneOnlineGift *> m_vector_oneOnLineGift;											// OnlineGift的vector
	std::vector<OnlineGift *> m_vector_OnlineGift;														// 一个OnlineGift中的Gift数据的vector

	int initGetGiftNeedTime();																							// 用服务器 中的 数据 初始化 当前正在进行奖励 所需时间
	int initGetGiftNum();																									// 用服务器 中的 数据 初始化 当前正在进行奖励 的编号
	void initDataFromIntent();																							// 用internet 中的 数据 初始化 native数据

	void getGiftDataTimeFromIcon(const int nTime, const unsigned int nNum);								// 用icon的最新的数据 更新 服务器数据		
	
	bool isAllGiftHasGet();

	GoodsInfo* getGoosInfo(unsigned int nGift, int nGiftNum);													// nGift编号，nGiftNum 第几个奖励（从0开始）

};

#endif

