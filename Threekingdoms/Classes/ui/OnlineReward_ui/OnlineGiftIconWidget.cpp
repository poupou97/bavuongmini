#include "OnlineGiftIconWidget.h"
#include "OnLineRewardUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "OnlineGiftData.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/CCTutorialParticle.h"

#define  TAG_TUTORIALPARTICLE 500

OnlineGiftIconWidget::OnlineGiftIconWidget(void)
	:m_nOnlineGiftTime(0)
	,m_nTimeShow(0)
	,m_btn_onlineGiftIcon(NULL)
	,m_layer_onlineGift(NULL)
	,m_label_onlineGift_time(NULL)
	,m_spirte_fontBg(NULL)
	,m_timeDelta(0.0f)
{

}


OnlineGiftIconWidget::~OnlineGiftIconWidget(void)
{
	OnlineGiftData::instance()->getGiftDataTimeFromIcon(m_nTimeShow, m_nGiftNum);
}

OnlineGiftIconWidget* OnlineGiftIconWidget::create()
{
	OnlineGiftIconWidget * onlineGiftIconWidget = new OnlineGiftIconWidget();
	if (onlineGiftIconWidget && onlineGiftIconWidget->init())
	{
		onlineGiftIconWidget->autorelease();
		return onlineGiftIconWidget;
	}
	CC_SAFE_DELETE(onlineGiftIconWidget);
	return NULL;
}

bool OnlineGiftIconWidget::init()
{
	if (UIWidget::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		// icon
		m_btn_onlineGiftIcon = UIButton::create();
		m_btn_onlineGiftIcon->setTouchEnable(true);
		m_btn_onlineGiftIcon->setPressedActionEnabled(true);
		m_btn_onlineGiftIcon->setTextures("gamescene_state/zhujiemian3/tubiao/OnlineAwards.png", "gamescene_state/zhujiemian3/tubiao/OnlineAwards.png","");
		m_btn_onlineGiftIcon->setAnchorPoint(ccp(0.5f, 0.5f));
		/*m_btn_onlineGiftIcon->setPosition(ccp( winSize.width-70-159, winSize.height-70));*/
		m_btn_onlineGiftIcon->setPosition(ccp(0, 0));
		m_btn_onlineGiftIcon->addReleaseEvent(this, coco_cancelselector(OnlineGiftIconWidget::callBackBtnOnlineGift));

		// label
		m_label_onlineGift_time = UILabel::create();
		m_label_onlineGift_time->setText("00:00");
		m_label_onlineGift_time->setColor(ccc3(255, 246, 0));
		m_label_onlineGift_time->setFontSize(14);
		m_label_onlineGift_time->setAnchorPoint(ccp(0.5f, 0.5f));
		m_label_onlineGift_time->setPosition(ccp(m_btn_onlineGiftIcon->getPosition().x, m_btn_onlineGiftIcon->getPosition().y - m_btn_onlineGiftIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = CCScale9Sprite::create(CCRectMake(11, 8, 1, 17) , "res_ui/zhezhao70.png");
		m_spirte_fontBg->setPreferredSize(CCSize(47, 14));
		m_spirte_fontBg->setAnchorPoint(ccp(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_onlineGift_time->getPosition());


		/*m_layer_onlineGift = UILayer::create();
		m_layer_onlineGift->setTouchEnabled(true);
		m_layer_onlineGift->setAnchorPoint(CCPointZero);
		m_layer_onlineGift->setPosition(CCPointZero);
		m_layer_onlineGift->setContentSize(winSize);

		m_layer_onlineGift->addChild(m_spirte_fontBg, -10);
		m_layer_onlineGift->addWidget(m_btn_onlineGiftIcon);
		m_layer_onlineGift->addWidget(m_label_onlineGift_time);*/
		CCLayer * tmpLayer = CCLayer::create();
		tmpLayer->setTouchEnabled(true);
		tmpLayer->setPosition(ccp(0, 0));
		tmpLayer->setContentSize(winSize);
		tmpLayer->schedule(schedule_selector(OnlineGiftIconWidget::update), 1.0f);

		//this->addCCNode(m_spirte_fontBg);
		this->addCCNode(tmpLayer);
		this->addCCNode(m_spirte_fontBg);
		this->addChild(m_btn_onlineGiftIcon);
		this->addChild(m_label_onlineGift_time);
		
		this->setUpdateEnabled(true);
		this->setTouchEnable(true);
		this->setSize(m_btn_onlineGiftIcon->getContentSize());

		//this->schedule(schedule_selector(OnlineGiftIcon::update),1.0f);

		//this->setContentSize(winSize);

	

		return true;
	}
	return false;
}

void OnlineGiftIconWidget::callBackBtnOnlineGift( CCObject *obj )
{
	// 将Icon最新的数据 同步给 服务器数据
	OnlineGiftData::instance()->getGiftDataTimeFromIcon(m_nTimeShow, m_nGiftNum);
	OnlineGiftData::instance()->initDataFromIntent();
	

	// 初始化 UI WINDOW数据
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	OnLineRewardUI* pOnLineRewardUI = OnLineRewardUI::create();
	pOnLineRewardUI->ignoreAnchorPointForPosition(false);
	pOnLineRewardUI->setAnchorPoint(ccp(0.5f, 0.5f));
	pOnLineRewardUI->setPosition(ccp(winSize.width / 2, winSize.height / 2));
	pOnLineRewardUI->setTag(kTagOnLineGiftUI);

	pOnLineRewardUI->refreshData();
	pOnLineRewardUI->refreshTimeAndNum();					// 更新 奖励编号 和 时间
	pOnLineRewardUI->update(0);

	GameView::getInstance()->getMainUIScene()->addChild(pOnLineRewardUI);
}


void OnlineGiftIconWidget::initGiftTime(const int nTime )
{
	m_nTimeShow = nTime;
	m_nOnlineGiftTime = nTime;

	refreshData();
}


void OnlineGiftIconWidget::initGiftNum( const int nGiftNum )
{
	m_nGiftNum = nGiftNum;
}

void OnlineGiftIconWidget::update( float dt )
{
	m_timeDelta += dt;
	if (m_timeDelta > 1)
	{
		m_timeDelta -= 1;
	}
	else
	{
		/*char str_time[20];
		sprintf(str_time, "%f", m_timeDelta);
		GameView::getInstance()->showAlertDialog(str_time);*/

		return;
	}

	m_nOnlineGiftTime -= 1000;
	m_nTimeShow = m_nOnlineGiftTime;
	if (m_nTimeShow <= 0)
	{
		m_nTimeShow = 0;
	}

	refreshData();
}

std::string OnlineGiftIconWidget::timeFormatToString( int nTime )
{
	if (0 == nTime)
	{
		std::string timeString = "";
		const char *str1 = StringDataManager::getString("label_timeout");
		timeString.append(str1);
		return timeString;
	}
	else
	{
		int int_m = nTime / 1000 / 60;
		int int_s = nTime / 1000 % 60;

		std::string timeString = "";
		char str_m[10];
		if (int_m < 10)
		{
			timeString.append("0");
		}
		sprintf(str_m,"%d",int_m);
		timeString.append(str_m);
		timeString.append(":");

		char str_s[10];
		if (int_s < 10)
		{
			timeString.append("0");
		}
		sprintf(str_s,"%d",int_s);
		timeString.append(str_s);

		return timeString;
	}

}

void OnlineGiftIconWidget::refreshData()
{
	std::string strShow = timeFormatToString(m_nTimeShow);

	std::string timeString = "";
	const char *str1 = StringDataManager::getString("label_timeout");
	timeString.append(str1);

	//判断 将要显示的是否为“可领取”，若是，则创建特效；否则 移除特效（如果先前的特效存在的话）
	if (strShow == timeString)
	{
		CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getRenderer()->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL == tutorialParticle)
		{
			//m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", m_btn_onlineGiftIcon->getContentSize().width, m_btn_onlineGiftIcon->getContentSize().height + 10);
			//m_tutorialParticle->setPosition(ccp(m_btn_onlineGiftIcon->getPosition().x, m_btn_onlineGiftIcon->getPosition().y - 35));
			m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", 30, 46);
			m_tutorialParticle->setPosition(ccp(m_btn_onlineGiftIcon->getPosition().x, m_btn_onlineGiftIcon->getPosition().y - 25));
			m_tutorialParticle->setAnchorPoint(ccp(0.5f, 0.5f));
			m_tutorialParticle->setTag(TAG_TUTORIALPARTICLE);
			this->addCCNode(m_tutorialParticle);
		}
	}
	else 
	{
		CCTutorialParticle* tutorialParticle = (CCTutorialParticle*)this->getRenderer()->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL != tutorialParticle)
		{
			//this->getRenderer()->removeChildByTag(TAG_TUTORIALPARTICLE);
			m_tutorialParticle->removeFromParent();
		}
	}

	m_label_onlineGift_time->setText(strShow.c_str());
}

void OnlineGiftIconWidget::initDataFromIntent()
{
	// 在线奖励ICON 获取数据
	this->initGiftTime(OnlineGiftData::instance()->initGetGiftNeedTime());
	this->initGiftNum(OnlineGiftData::instance()->initGetGiftNum());
}

void OnlineGiftIconWidget::allGiftGetIconExit()
{
	// 所有奖励获取成功，关闭icon窗体
	//this->closeAnim();
	
	this->removeFromParent();
}

