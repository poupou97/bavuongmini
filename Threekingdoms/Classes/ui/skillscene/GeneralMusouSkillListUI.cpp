#include "GeneralMusouSkillListUI.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../AppMacros.h"
#include "../generals_ui/GeneralsUI.h"
#include "../generals_ui/RecuriteActionItem.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "../generals_ui/GeneralsShortcutSlot.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "SkillScene.h"
#include "../../gamescene_state/role/GameActor.h"
#include "MusouSkillScene.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/ActorUtils.h"

#define  M_ROLEMUSOUSKILL_FLAT_TAG 641
#define  M_ROLESKILLITEMBASE_TAG 726

#define  M_ROLESELECT_FLAG_TAG 671
#define  M_ROLE_SCROLLVIEW_TAG 681

GeneralMusouSkillListUI::GeneralMusouSkillListUI():
isSelectImageExist(false)
{
}


GeneralMusouSkillListUI::~GeneralMusouSkillListUI()
{
	delete m_curSelectShortCut;
}

GeneralMusouSkillListUI* GeneralMusouSkillListUI::create()
{
	GeneralMusouSkillListUI * generalMusouSkillListUI = new GeneralMusouSkillListUI();
	if (generalMusouSkillListUI && generalMusouSkillListUI->init())
	{
		generalMusouSkillListUI->autorelease();
		return generalMusouSkillListUI;
	}
	CC_SAFE_DELETE(generalMusouSkillListUI);
	return NULL;
}

bool GeneralMusouSkillListUI::init()
{
	if (UIScene::init())
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		//武将龙魂技能层
		layer_generalMusouSkillList= UILayer::create();
		addChild(layer_generalMusouSkillList);

		m_curSelectGeneralId = -1;
		m_curSelectShortCut = new CShortCut();

		//刷新主角技能列表数据
		RefreshStudedSkillList();

		//武将列表
		generalList_tableView = CCTableView::create(this,CCSizeMake(420,356));
		generalList_tableView->setDirection(kCCScrollViewDirectionVertical);
		generalList_tableView->setAnchorPoint(ccp(0,0));
		generalList_tableView->setPosition(ccp(0,0));
		generalList_tableView->setContentOffset(ccp(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		//generalList_tableView->setPressedActionEnabled(true);
		layer_generalMusouSkillList->addChild(generalList_tableView);

		return true;
	}
	return false;
}

void GeneralMusouSkillListUI::onEnter()
{
	UIScene::onEnter();
}

void GeneralMusouSkillListUI::onExit()
{
	UIScene::onExit();
}

void GeneralMusouSkillListUI::scrollViewDidScroll( CCScrollView* view )
{

}

void GeneralMusouSkillListUI::scrollViewDidZoom( CCScrollView* view )
{

}

void GeneralMusouSkillListUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	SkillScene *skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if (!skillScene)
		return;

	int index = cell->getIdx();
	if (index == 0)
	{
		//设置当前选中的是主角
		skillScene->musouSkillLayer->isRoleOrGeneral = true;
	}
	else
	{
		//设置当前选中的是武将
		skillScene->musouSkillLayer->isRoleOrGeneral = false;
		m_curSelectGeneralId = GameView::getInstance()->generalsInLineDetailList.at(index-1)->generalid();
	}
	
}

cocos2d::CCSize GeneralMusouSkillListUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeMake(410,91);
}

cocos2d::extension::CCTableViewCell* GeneralMusouSkillListUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	if(idx == 0)    //主角
	{
		//大边框
		CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/kuang0_new.png");
		sprite_bigFrame->setAnchorPoint(ccp(0, 0));
		sprite_bigFrame->setPosition(ccp(0, 0));
		sprite_bigFrame->setPreferredSize(CCSizeMake(410,91));
		cell->addChild(sprite_bigFrame);
		//头像底框
		CCSprite * sprite_headFrame = CCSprite::create("res_ui/round.png");
		sprite_headFrame->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_headFrame->setPosition(ccp(46,54));
		cell->addChild(sprite_headFrame);
		//头像
		CCSprite * sprite_head = CCSprite::create(BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str());
		sprite_head->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_head->setPosition(ccp(sprite_headFrame->getContentSize().width/2,sprite_headFrame->getContentSize().height/2));
		sprite_headFrame->addChild(sprite_head);
		//名字底框
		CCScale9Sprite * sprite_nameFrame = CCScale9Sprite::create("res_ui/zhezhao80_new.png");
		sprite_nameFrame->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_nameFrame->setPosition(ccp(46,21));
		sprite_nameFrame->setPreferredSize(CCSizeMake(75,20));
		cell->addChild(sprite_nameFrame);
		//名字
		CCLabelTTF * l_name = CCLabelTTF::create(GameView::getInstance()->myplayer->getActiveRole()->rolebase().name().c_str(),APP_FONT_NAME,16);
		l_name->setAnchorPoint(ccp(0.5f, 0.5f));
		l_name->setPosition(ccp(44, 21));
		cell->addChild(l_name);
		//等级底框
		CCScale9Sprite * sprite_lvFrame = CCScale9Sprite::create("res_ui/zhezhao80_new.png");
		sprite_lvFrame->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_lvFrame->setPosition(ccp(68,38));
		sprite_lvFrame->setPreferredSize(CCSizeMake(33,12));
		cell->addChild(sprite_lvFrame);
		//等级
		std::string str_lv = "LV";
		char str_lvValue [10];
		sprintf(str_lvValue,"%d",GameView::getInstance()->myplayer->getActiveRole()->level());
		str_lv.append(str_lvValue);
		CCLabelTTF * l_lv = CCLabelTTF::create(str_lv.c_str(),APP_FONT_NAME,12);
		l_lv->setAnchorPoint(ccp(0.5f, 0.5f));
		l_lv->setPosition(ccp(67, 38));
		cell->addChild(l_lv);

		//    test by yangjun 2014.3.11

		int scroll_width = 105*m_studedSkillList.size();
		m_scrollView = CCScrollView::create(CCSizeMake(310,90));
		m_scrollView->setViewSize(CCSizeMake(310, 90));
		m_scrollView->ignoreAnchorPointForPosition(false);
		//m_scrollView->setTouchEnabled(true);
		//m_scrollView->setDirection(kCCScrollViewDirectionHorizontal);
		m_scrollView->setAnchorPoint(ccp(0,0));
		m_scrollView->setPosition(ccp(100,0));
		m_scrollView->setBounceable(true);
		m_scrollView->setClippingToBounds(false);
		m_scrollView->setTag(M_ROLE_SCROLLVIEW_TAG);
		cell->addChild(m_scrollView);

		CCSprite * sprite_musou = CCSprite::create("res_ui/longhun.png");
		sprite_musou->setPosition(ccp(0,0));
		sprite_musou->setAnchorPoint(ccp(0.5,0.5f));
		sprite_musou->setTag(M_ROLEMUSOUSKILL_FLAT_TAG);
		sprite_musou->setVisible(false);
		m_scrollView->addChild(sprite_musou);
		sprite_musou->setZOrder(5);

		//选中状态
		CCSprite * sprite_select = CCSprite::create("res_ui/jineng/jinengdi_1_on.png");
		sprite_select->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_select->setPosition(ccp(43,61));
		sprite_select->setTag(M_ROLESELECT_FLAG_TAG);
		m_scrollView->addChild(sprite_select);
		sprite_select->setVisible(false);

		for(int i =0;i<m_studedSkillList.size();++i)
		{
			//底部技能名称的底图           
			RoleSkillItem * roleSkillItem = RoleSkillItem::create(m_studedSkillList.at(i));
			roleSkillItem->ignoreAnchorPointForPosition(false);
			roleSkillItem->setAnchorPoint(ccp(0.5f, 0.5f));
			roleSkillItem->setPosition(ccp(105*i-24, 5));
			m_scrollView->addChild(roleSkillItem);
			roleSkillItem->setTag(M_ROLESKILLITEMBASE_TAG+i);
			roleSkillItem->setIndex(i);

			if (GameView::getInstance()->myplayer->getMusouSkill())
			{
				if (strcmp(GameView::getInstance()->myplayer->getMusouSkill()->getId().c_str(),m_studedSkillList.at(i)->getId().c_str()) == 0)
				{
					sprite_musou->setAnchorPoint(ccp(0.5f,0.5f));
					sprite_musou->setPosition(ccp(43+105*i,61));
					sprite_musou->setVisible(true);
				}
			}
		}
		if (scroll_width > 310)
		{
			m_scrollView->setContentSize(CCSizeMake(scroll_width,90));
		}
		else
		{
			m_scrollView->setContentSize(ccp(310,90));
		}
		m_scrollView->setClippingToBounds(true);
		
	}
	else              //武将
	{
		CGeneralDetail * generalsDetail = GameView::getInstance()->generalsInLineDetailList.at(idx-1);
		CGeneralBaseMsg * generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalsDetail->modelid()];
		if (!generalMsgFromDb)
			return NULL;

		//m_curSelectGeneralId = generalsDetail->generalid();

		//大边框
		CCScale9Sprite * sprite_bigFrame = CCScale9Sprite::create("res_ui/kuang0_new.png");
		sprite_bigFrame->setAnchorPoint(ccp(0, 0));
		sprite_bigFrame->setPosition(ccp(0, 0));
		sprite_bigFrame->setPreferredSize(CCSizeMake(410,91));
		sprite_bigFrame->setZOrder(-2);
		cell->addChild(sprite_bigFrame);
		//武将头像底框
		CCSprite * sprite_headFrame = CCSprite::create("res_ui/round.png");
		sprite_headFrame->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_headFrame->setPosition(ccp(46,54));
		cell->addChild(sprite_headFrame);
		//武将头像
		std::string icon_path = "res_ui/generals46X45/";
		icon_path.append(generalMsgFromDb->get_head_photo());
		icon_path.append(".png");
		CCSprite * sprite_head = CCSprite::create(icon_path.c_str());
		sprite_head->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_head->setPosition(ccp(sprite_headFrame->getContentSize().width/2,sprite_headFrame->getContentSize().height/2));
		sprite_headFrame->addChild(sprite_head);
		//武将名字底框
		CCScale9Sprite * sprite_nameFrame = CCScale9Sprite::create("res_ui/zhezhao80_new.png");
		sprite_nameFrame->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_nameFrame->setPosition(ccp(46,21));
		sprite_nameFrame->setPreferredSize(CCSizeMake(75,20));
		cell->addChild(sprite_nameFrame);
		//武将名字
		CCLabelTTF * l_name = CCLabelTTF::create(generalMsgFromDb->name().c_str(),APP_FONT_NAME,16);
		l_name->setAnchorPoint(ccp(0.5f, 0.5f));
		l_name->setPosition(ccp(44, 21));
		l_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalsDetail->currentquality()));
		cell->addChild(l_name);
		//武将等级底框
		CCScale9Sprite * sprite_lvFrame = CCScale9Sprite::create("res_ui/zhezhao80_new.png");
		sprite_lvFrame->setAnchorPoint(ccp(0.5f,0.5f));
		sprite_lvFrame->setPosition(ccp(68,38));
		sprite_lvFrame->setPreferredSize(CCSizeMake(33,12));
		cell->addChild(sprite_lvFrame);
		//武将等级
		CCLabelTTF * l_lv = CCLabelTTF::create("LV88",APP_FONT_NAME,12);
		l_lv->setAnchorPoint(ccp(0.5f, 0.5f));
		l_lv->setPosition(ccp(67, 38));
		cell->addChild(l_lv);
		char s_lv[10];
		sprintf(s_lv,"%d",generalsDetail->activerole().level());
		l_lv->setString(s_lv);
		//武将军衔
		if (generalsDetail->evolution() > 0)
		{
			CCSprite * sprite_rank = ActorUtils::createGeneralRankSprite(generalsDetail->evolution());
			sprite_rank->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_rank->setPosition(ccp(77,74));
			sprite_rank->setScale(0.8f);
			cell->addChild(sprite_rank);
		}
		//武将稀有度
		if (generalsDetail->rare()>0)
		{
			CCSprite * sprite_star = CCSprite::create(RecuriteActionItem::getStarPathByNum(generalsDetail->rare()).c_str());
			sprite_star->setAnchorPoint(ccp(0.5f,0.5f));
			sprite_star->setPosition(ccp(16,71));
			sprite_star->setScale(0.8f);
			cell->addChild(sprite_star);
		}


		//创建武将技能
		for(int i = 0;i<generalsDetail->shortcuts_size();i++)
		{
			CShortCut * shortcut = new CShortCut();
			shortcut->CopyFrom(generalsDetail->shortcuts(i));
			if (shortcut->index()%2 == 0)
			{
				GeneralsMusouSkillItem * generalsMusouSkillItem = GeneralsMusouSkillItem::create(generalsDetail->generalid(),shortcut);
				generalsMusouSkillItem->ignoreAnchorPointForPosition(false);
				generalsMusouSkillItem->setAnchorPoint(ccp(0.5f,0.5f));
				generalsMusouSkillItem->setPosition(ccp(142+105*shortcut->index()/2,80));
				cell->addChild(generalsMusouSkillItem);

				if (shortcut->complexflag() == 1)
				{
					CCSprite * sprite_musou = CCSprite::create("res_ui/longhun.png");
					sprite_musou->setPosition(ccp(143+105*shortcut->index()/2,60));
					sprite_musou->setTag(M_ROLEMUSOUSKILL_FLAT_TAG);
					cell->addChild(sprite_musou);
					sprite_musou->setZOrder(5);

					//作假（设置龙魂后，龙魂标示会盖住等级）
					//等级底
					CCScale9Sprite * s_lvFrame = CCScale9Sprite::create("res_ui/zhezhao80_new.png");
					s_lvFrame->setAnchorPoint(ccp(0.5f, 0.5f));
					s_lvFrame->setPosition(ccp(166+105*shortcut->index()/2,39));
					s_lvFrame->setPreferredSize(CCSizeMake(33,12));
					cell->addChild(s_lvFrame);
					s_lvFrame->setZOrder(10);
					//等级
					std::string str_lvstr = "LV";
					char str_lv [10];
					sprintf(str_lv,"%d",shortcut->skilllevel());
					str_lvstr.append(str_lv);
					CCLabelTTF * l_lv = CCLabelTTF::create(str_lvstr.c_str(),APP_FONT_NAME,12);
					l_lv->setAnchorPoint(ccp(0.5f,0.5f));
					l_lv->setPosition(ccp(166+105*shortcut->index()/2,39));
					cell->addChild(l_lv);
					l_lv->setZOrder(10);
				}
			}
		}
	}
	
	return cell;
}

unsigned int GeneralMusouSkillListUI::numberOfCellsInTableView( CCTableView *table )
{
	return GameView::getInstance()->generalsInLineDetailList.size()+1;
}

void GeneralMusouSkillListUI::AddMusouForRole(std::string skillId)
{
	int idx = -1;
	for(int i = 0;i<m_studedSkillList.size();i++)
	{
	 	if (strcmp(skillId.c_str(),m_studedSkillList.at(i)->getId().c_str()) == 0)
	 	{
	 		idx = i;
	 		break;
	 	}
	}
	 
	if (idx == -1)
	 	return;

	if (!generalList_tableView->cellAtIndex(0))
		return;

	if(m_scrollView->getContainer()->getChildByTag(M_ROLESKILLITEMBASE_TAG+idx))
	{
		CCSprite * sprite_musou = (CCSprite*)m_scrollView->getContainer()->getChildByTag(M_ROLEMUSOUSKILL_FLAT_TAG);
		sprite_musou->setPosition(ccp(43+105*idx,61));
		sprite_musou->setAnchorPoint(ccp(0.5f,0.5));
		sprite_musou->setVisible(true);
		sprite_musou->setScale(3.0f);
		CCFiniteTimeAction*  action = CCSequence::create(
			CCEaseElasticIn::create(CCScaleTo::create(0.7f,1.05f)),
			NULL);
		sprite_musou->runAction(action);
	}
}

void GeneralMusouSkillListUI::AddMusouForGeneral( long long generalid,int index )
{
	int idx = -1;
	for(int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
	{
		if (generalid == GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
		{
			idx = i;
			break;
		}
	}

	if (idx == -1)
		return;

	for(int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
	{
		if (generalid != GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
			continue;

		CCTableViewCell * cell = generalList_tableView->cellAtIndex(i+1);
		if (!cell)
			continue;

		if (cell->getChildByTag(M_ROLEMUSOUSKILL_FLAT_TAG))
		{
			cell->getChildByTag(M_ROLEMUSOUSKILL_FLAT_TAG)->removeFromParent();
		}
	}

	CCTableViewCell * cell = generalList_tableView->cellAtIndex(idx+1);
	if (!cell)
		return;

	CCSprite * sprite_musou = CCSprite::create("res_ui/longhun.png");
	//CCSprite * sprite_musou = CCSprite::create("gamescene_state/jinengqu/wushuang_1.png");
	sprite_musou->setPosition(ccp(143+105*index/2,60));
	sprite_musou->setTag(M_ROLEMUSOUSKILL_FLAT_TAG);
	cell->addChild(sprite_musou);
	sprite_musou->setScale(3.0f);
	sprite_musou->setVisible(true);
	sprite_musou->setZOrder(5);
	CCFiniteTimeAction*  action = CCSequence::create(
		CCEaseElasticIn::create(CCScaleTo::create(0.7f,1.05f)),
		NULL);
	sprite_musou->runAction(action);

// 	//作假（设置龙魂后，龙魂标示会盖住等级）
// 	//等级底
// 	CCScale9Sprite * s_lvFrame = CCScale9Sprite::create("res_ui/zhezhao80_new.png");
// 	s_lvFrame->setAnchorPoint(ccp(0.5f, 0.5f));
// 	s_lvFrame->setPosition(ccp(142+105*index/2,60));
// 	s_lvFrame->setPreferredSize(CCSizeMake(33,12));
// 	cell->addChild(s_lvFrame);
// 	//等级
// 	std::string str_lvstr = "LV";
// 	char str_lv [10];
// 	sprintf(str_lv,"%d",5);
// 	str_lvstr.append(str_lv);
// 	CCLabelTTF * l_lv = CCLabelTTF::create(str_lvstr.c_str(),APP_FONT_NAME,12);
// 	l_lv->setAnchorPoint(ccp(0.5f,0.5f));
// 	l_lv->setPosition(ccp(142+105*index/2,60));
// 	cell->addChild(l_lv);

}

long long GeneralMusouSkillListUI::getSelectGeneralID()
{
	return m_curSelectGeneralId;
}

CShortCut * GeneralMusouSkillListUI::getSelectShortCut()
{
	return m_curSelectShortCut;
}

void GeneralMusouSkillListUI::setSelectShortCut( CShortCut * shortCut )
{
	m_curSelectShortCut->CopyFrom(*shortCut);
}



void GeneralMusouSkillListUI::RefreshStudedSkillList()
{
	//delete old
	std::vector<GameFightSkill*>::iterator iter_skill;
	for (iter_skill = m_studedSkillList.begin(); iter_skill != m_studedSkillList.end(); ++iter_skill)
	{
		delete *iter_skill;
	}
	m_studedSkillList.clear();
	//refresh
	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
	{
		GameFightSkill * tempSkill = it->second;
		if (!tempSkill)
			continue;

		if (tempSkill->getCBaseSkill()->usemodel() != 0)
			continue;

		if (strcmp(tempSkill->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
			||strcmp(tempSkill->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
			||strcmp(tempSkill->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
			||strcmp(tempSkill->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
			continue;

		GameFightSkill * newSkill = new GameFightSkill();
		newSkill->initSkill(tempSkill->getId(),tempSkill->getId(),tempSkill->getLevel(),GameActor::type_player);
		m_studedSkillList.push_back(newSkill);
	}
}


void GeneralMusouSkillListUI::RefreshRoleSkillItem()
{
	if (m_scrollView)
	{
		m_scrollView->removeAllChildren();
	}
	
	int scroll_width = 105*m_studedSkillList.size();
	
	CCSprite * sprite_musou = CCSprite::create("res_ui/longhun.png");
	sprite_musou->setPosition(ccp(0,0));
	sprite_musou->setAnchorPoint(ccp(0.5,0.5f));
	sprite_musou->setTag(M_ROLEMUSOUSKILL_FLAT_TAG);
	sprite_musou->setVisible(false);

	for(int i =0;i<m_studedSkillList.size();++i)
	{
		//底部技能名称的底图           
		RoleSkillItem * roleSkillItem = RoleSkillItem::create(m_studedSkillList.at(i));
		roleSkillItem->ignoreAnchorPointForPosition(false);
		roleSkillItem->setAnchorPoint(ccp(0.5f, 0.5f));
		roleSkillItem->setPosition(ccp(105*i-24, 5));
		m_scrollView->addChild(roleSkillItem);
		roleSkillItem->setTag(M_ROLESKILLITEMBASE_TAG+i);
		roleSkillItem->setIndex(i);

		if (GameView::getInstance()->myplayer->getMusouSkill())
		{
			if (strcmp(GameView::getInstance()->myplayer->getMusouSkill()->getId().c_str(),m_studedSkillList.at(i)->getId().c_str()) == 0)
			{
				sprite_musou->setVisible(true);
				sprite_musou->setPosition(ccp(43+105*i,61));
			}
		}
	}
	if (scroll_width > 310)
	{
		m_scrollView->setContentSize(CCSizeMake(scroll_width,90));
	}
	else
	{
		m_scrollView->setContentSize(ccp(310,90));
	}
	m_scrollView->setClippingToBounds(true);

	m_scrollView->addChild(sprite_musou);
}

void GeneralMusouSkillListUI::addSelectImage()
{
	if(isSelectImageExist)
	{
		//delete old
	}

	//add new 


	//set data 
	isSelectImageExist = true;
}

void GeneralMusouSkillListUI::addSelectImageForGeneral( long long generalid,int index )
{
	//添加武将技能的选中标示
	int idx = -1;   //当前选的是第几个Cell
	for(int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
	{
		if (generalid == GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
		{
			idx = i;
			break;
		}
	}

	if (idx == -1)
		return;

	for(int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
	{
// 		if (generalid != GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
// 			continue;

		CCTableViewCell * cell = generalList_tableView->cellAtIndex(i+1);
		if (!cell)
			continue;

		if (i == 0)
		{
			//删除人物技能的选中状态
			CCTableViewCell * roleCell = generalList_tableView->cellAtIndex(0);
			if (roleCell)
			{
				if (roleCell->getChildByTag(M_ROLE_SCROLLVIEW_TAG))
				{
					if (m_scrollView->getContainer()->getChildByTag(M_ROLESELECT_FLAG_TAG))
					{
						(m_scrollView->getContainer()->getChildByTag(M_ROLESELECT_FLAG_TAG))->setVisible(false);
					}
				}
			}
		}

		if (cell->getChildByTag(M_ROLESELECT_FLAG_TAG))
		{
			cell->getChildByTag(M_ROLESELECT_FLAG_TAG)->removeFromParent();
		}
	}

	CCTableViewCell * cell = generalList_tableView->cellAtIndex(idx+1);
	if (!cell)
		return;

	//选中状态
	CCSprite * sprite_select = CCSprite::create("res_ui/jineng/jinengdi_1_on.png");
	sprite_select->setAnchorPoint(ccp(0.5f,0.5f));
	sprite_select->setPosition(ccp(143+105*index/2,55));
	sprite_select->setTag(M_ROLESELECT_FLAG_TAG);
	cell->addChild(sprite_select);
	sprite_select->setZOrder(-1);
	sprite_select->setVisible(true);
}

void GeneralMusouSkillListUI::AddSelectImageForRole( std::string skillId )
{
	//删除武将技能的选中状态
	for(int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
	{
		CCTableViewCell * cell = generalList_tableView->cellAtIndex(i+1);
		if (!cell)
			continue;

		if (cell->getChildByTag(M_ROLESELECT_FLAG_TAG))
		{
			cell->getChildByTag(M_ROLESELECT_FLAG_TAG)->removeFromParent();
		}
	}
	//添加人物技能的选中标示
	int idx = -1;
	for(int i = 0;i<m_studedSkillList.size();i++)
	{
		if (strcmp(skillId.c_str(),m_studedSkillList.at(i)->getId().c_str()) == 0)
		{
			idx = i;
			break;
		}
	}

	if (idx == -1)
		return;

	if (!generalList_tableView->cellAtIndex(0))
		return;

	if(m_scrollView->getContainer()->getChildByTag(M_ROLESKILLITEMBASE_TAG+idx))
	{
		//选中状态
		if (m_scrollView->getContainer()->getChildByTag(M_ROLESELECT_FLAG_TAG))
		{
			(m_scrollView->getContainer()->getChildByTag(M_ROLESELECT_FLAG_TAG))->setPosition(ccp(2+105*idx,15));
			(m_scrollView->getContainer()->getChildByTag(M_ROLESELECT_FLAG_TAG))->setVisible(true);
		}
	}
}


/////////////////////////////////////////////////////////

GeneralsMusouSkillItem::GeneralsMusouSkillItem():
curGeneralId(-1)
{

}

GeneralsMusouSkillItem::~GeneralsMusouSkillItem()
{
	delete m_curShortCut;
}

GeneralsMusouSkillItem* GeneralsMusouSkillItem::create( long long genenralid,CShortCut * shortCut )
{
	GeneralsMusouSkillItem * generalsMusouSkillItem = new GeneralsMusouSkillItem();
	if (generalsMusouSkillItem && generalsMusouSkillItem->init(genenralid,shortCut))
	{
		generalsMusouSkillItem->autorelease();
		return generalsMusouSkillItem;
	}
	CC_SAFE_DELETE(generalsMusouSkillItem);
	return NULL;
}

bool GeneralsMusouSkillItem::init( long long genenralid,CShortCut * shortCut )
{
	if (UIScene::init())
	{
		curGeneralId = genenralid;
		// 该UILayer位于CCTableView之内，为了正确响应TableView的拖动事件，故设置为不吞掉触摸事件
		m_pUiLayer->setSwallowsTouches(false);

        m_curShortCut = new CShortCut();
		m_curShortCut->CopyFrom(*shortCut);

		CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[shortCut->skillpropid()];
		CCAssert(baseSkill != NULL, "baseSkill should not be null");

		CCAssert(shortCut->has_skillpropid(), "baseSkill should not be null");

		m_skillId = shortCut->skillpropid();
		//框
		std::string frameColorPath;
		if (baseSkill->get_quality() == 1)
		{
			frameColorPath = GENERALSKILLWhiteFramePath;
		}
		else if (baseSkill->get_quality()  == 2)
		{
			frameColorPath = GENERALSKILLGreenFramePath;
		}
		else if (baseSkill->get_quality()  == 3)
		{
			frameColorPath = GENERALSKILLBlueFramePath;
		}
		else if (baseSkill->get_quality()  == 4)
		{
			frameColorPath = GENERALSKILLPurpleFramePath;
		}
		else if (baseSkill->get_quality()  == 5)
		{
			frameColorPath = GENERALSKILLOrangeFramePath;
		}
		else
		{
			frameColorPath = GENERALSKILLVoidFramePath;
		}
		btn_bgFrame = UIButton::create();
		btn_bgFrame->setTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		btn_bgFrame->setAnchorPoint(ccp(0.5f,0.5f));
		btn_bgFrame->setPosition(ccp(42,45));
		btn_bgFrame->setScale(0.9f);
		btn_bgFrame->setTouchEnable(true);
		//btn_bgFrame->setPressedActionEnabled(true);
		btn_bgFrame->addReleaseEvent(this,coco_releaseselector(GeneralsMusouSkillItem::DoThing));
		m_pUiLayer->addWidget(btn_bgFrame);

		std::string skillIconPath = "res_ui/jineng_icon/";

// 		if (strcmp(shortCut->skillpropid().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 			||strcmp(shortCut->skillpropid().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 			||strcmp(shortCut->skillpropid().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 			||strcmp(shortCut->skillpropid().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 		{
// 			skillIconPath.append("jineng_2");
// 		}
// 		else
// 		{
			if (baseSkill)
			{
				skillIconPath.append(baseSkill->icon());
			}
			else
			{
				skillIconPath.append("jineng_2");
			}
//		}
		skillIconPath.append(".png");


		UIButton * imageView_skill = UIButton::create();
		imageView_skill->setTextures(skillIconPath.c_str(),skillIconPath.c_str(),"");
		imageView_skill->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_skill->setPosition(ccp(btn_bgFrame->getPosition().x,btn_bgFrame->getPosition().y));
		imageView_skill->setTouchEnable(true);
		imageView_skill->setScale(0.9f);
		imageView_skill->addReleaseEvent(this,coco_releaseselector(GeneralsMusouSkillItem::DoThing));
		m_pUiLayer->addWidget(imageView_skill);
		//名字底
		UIImageView * imageView_nameFrame = UIImageView::create();
		imageView_nameFrame->setTexture("res_ui/LV4_di80.png");
		imageView_nameFrame->setScale9Enable(true);
		imageView_nameFrame->setScale9Size(CCSizeMake(83,23));
		imageView_nameFrame->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_nameFrame->setPosition(ccp(42,12));
		m_pUiLayer->addWidget(imageView_nameFrame);
		//名字
		UILabel * l_name = UILabel::create();
		l_name->setText(baseSkill->name().c_str());
		l_name->setFontName(APP_FONT_NAME);
		l_name->setFontSize(16);
		l_name->setAnchorPoint(ccp(0.5f,0.5f));
		l_name->setPosition(ccp(42,12));
		m_pUiLayer->addWidget(l_name);
		//等级底
		UIImageView * imageView_lvFrame = UIImageView::create();
		imageView_lvFrame->setTexture("res_ui/zhezhao80_new.png");
		imageView_lvFrame->setScale9Enable(true);
		imageView_lvFrame->setScale9Size(CCSizeMake(33,12));
		imageView_lvFrame->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_lvFrame->setPosition(ccp(66,30));
		m_pUiLayer->addWidget(imageView_lvFrame);
		//等级
		std::string str_lvstr = "LV";
		char str_lv [10];
		sprintf(str_lv,"%d",shortCut->skilllevel());
		str_lvstr.append(str_lv);
		UILabel * l_lv = UILabel::create();
		l_lv->setText(str_lvstr.c_str());
		l_lv->setFontName(APP_FONT_NAME);
		l_lv->setFontSize(12);
		l_lv->setAnchorPoint(ccp(0.5f,0.5f));
		l_lv->setPosition(ccp(-1,0));
		imageView_lvFrame->addChild(l_lv);

		this->setTouchEnabled(false);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(83,143));
		return true;
	}
	return false;
}

void GeneralsMusouSkillItem::DoThing( CCObject *pSender )
{
	RefreshGeneralSkillInfo(m_skillId);
	SkillScene *skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if (!skillScene)
		return;
	
	//设置当前选中的是武将
	skillScene->musouSkillLayer->isRoleOrGeneral = false;
	//设置当前选中的技能信息
	skillScene->musouSkillLayer->generalMusouSkillListUI->setSelectShortCut(m_curShortCut);
	//设置选中状态
	skillScene->musouSkillLayer->generalMusouSkillListUI->addSelectImageForGeneral(curGeneralId,m_curShortCut->index());
}

void GeneralsMusouSkillItem::RefreshGeneralSkillInfo( std::string skillid )
{
	SkillScene * skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(!skillScene)
		return;

	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
	{
		if (strcmp(skillid.c_str(),it->second->getId().c_str()) == 0)
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(), it->second->getLevel(), GameActor::type_player);
			skillScene->musouSkillLayer->RefreshSkillInfo(it->second->getLevel(),gameFightSkill);
			delete gameFightSkill;
			break;
		}
	}	
}

void GeneralsMusouSkillItem::setFrameVisible( bool isVisible )
{
	btn_bgFrame->setVisible(isVisible);
}


//////////////////////////////////////////////////////
RoleSkillItem::RoleSkillItem():
m_index(-1)
{

}

RoleSkillItem::~RoleSkillItem()
{
	delete curGameFightSkill;
}

RoleSkillItem* RoleSkillItem::create( GameFightSkill * gameFightSkill )
{
	RoleSkillItem * roleSkillItem = new RoleSkillItem();
	if (roleSkillItem && roleSkillItem->init(gameFightSkill))
	{
		roleSkillItem->autorelease();
		return roleSkillItem;
	}
	CC_SAFE_DELETE(roleSkillItem);
	return NULL;
}

bool RoleSkillItem::init( GameFightSkill * gameFightSkill )
{
	if(UIScene::init())
	{
		if (!gameFightSkill)
		 	return NULL;

		// 该UILayer位于CCTableView之内，为了正确响应TableView的拖动事件，故设置为不吞掉触摸事件
		m_pUiLayer->setSwallowsTouches(false);

		curGameFightSkill = new GameFightSkill();
		curGameFightSkill->initSkill(gameFightSkill->getId(),gameFightSkill->getId(),gameFightSkill->getLevel(),GameActor::type_player);
		 
		//底部技能名称的底图           
		CCScale9Sprite * sprite_skillNameFrame = CCScale9Sprite::create("res_ui/LV4_di80.png");
		sprite_skillNameFrame->ignoreAnchorPointForPosition(false);
		sprite_skillNameFrame->setPreferredSize(CCSizeMake(83,23));
		sprite_skillNameFrame->setAnchorPoint(ccp(0.5f, 0.5f));
		sprite_skillNameFrame->setPosition(ccp(67, 12));
		addChild(sprite_skillNameFrame);
		//技能名称
		CCLabelTTF * label_skillName = CCLabelTTF::create(gameFightSkill->getCBaseSkill()->name().c_str(),APP_FONT_NAME,16);
		label_skillName->setAnchorPoint(ccp(0.5f,0.5f));
		label_skillName->setPosition(ccp(42,12));
		sprite_skillNameFrame->addChild(label_skillName);

// 		CCSprite * s_skillIconFrame = CCSprite::create("res_ui/jineng/jinengdi_1.png");
// 		s_skillIconFrame->setAnchorPoint(ccp(0.5f,0.5f));
// 		s_skillIconFrame->setPosition(ccp(67,50));
// 		addChild(s_skillIconFrame);
		UIButton * btn_di = UIButton::create();
		btn_di->setTextures("res_ui/jineng/jinengdi_1.png","res_ui/jineng/jinengdi_1.png","");
		btn_di->setAnchorPoint(ccp(0.5f,0.5f));
		btn_di->setPosition(ccp(67,50));
		btn_di->setTouchEnable(true);
		btn_di->addReleaseEvent(this,coco_releaseselector(RoleSkillItem::DoThing));
		m_pUiLayer->addWidget(btn_di);
		//技能图标
		std::string skillIconPath = "res_ui/jineng_icon/";
		 
		CBaseSkill * baseSkill = gameFightSkill->getCBaseSkill();
		if (baseSkill)
		{
// 		 	if (strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 		 		||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 		 		||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 		 		||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 		 	{
// 		 		skillIconPath.append("jineng_2");
// 		 	}
// 		 	else
// 		 	{
		 		skillIconPath.append(baseSkill->icon());
//		 	}
		}
		else
		{
		 	skillIconPath.append("jineng_2");
		}
		skillIconPath.append(".png");
		CCSprite * sprite_skillIcon = CCSprite::create(skillIconPath.c_str());
		sprite_skillIcon->setAnchorPoint(ccp(.5f,.5f));
		sprite_skillIcon->setPosition(ccp(67,51));
		addChild(sprite_skillIcon);
		//高光
		CCSprite * sprite_highLight = CCSprite::create("res_ui/jineng/gaoguang_1.png");
		sprite_highLight->setAnchorPoint(ccp(.5f,.5f));
		sprite_highLight->setPosition(ccp(67,52));
		addChild(sprite_highLight);
		//遮罩
		CCSprite * sprite_lvFrame = CCSprite::create("res_ui/jineng/jineng_zhezhao.png");
		sprite_lvFrame->setAnchorPoint(ccp(.5f,0));
		sprite_lvFrame->setPosition(ccp(67,28));
		addChild(sprite_lvFrame);
		//等级
		std::string str_lvValue;
		char str_lv [20];
		sprintf(str_lv,"%d",gameFightSkill->getLevel());
		str_lvValue.append(str_lv);
		str_lvValue.append("/10");
		CCLabelBMFont * label_skillLV = CCLabelBMFont::create(str_lvValue.c_str(),"res_ui/font/ziti_3.fnt");
		label_skillLV->setAnchorPoint(ccp(0.5f,0.5f));
		label_skillLV->setPosition(ccp(68,35));
		label_skillLV->setScale(0.6f);
		addChild(label_skillLV);
		

		this->setTouchEnabled(false);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(83,143));

		return true;
	}
	return false;
}

void RoleSkillItem::DoThing( CCObject *pSender )
{
	RefreshRoleSkillInfo(curGameFightSkill->getLevel(),curGameFightSkill);

	SkillScene *skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if (!skillScene)
		return;

	//设置当前选中的是主角
	skillScene->musouSkillLayer->isRoleOrGeneral = true;
	//设置当前选中的技能信息
	skillScene->musouSkillLayer->curSelectRoleSkillSkillId = curGameFightSkill->getId();
	//设置选中状态
	skillScene->musouSkillLayer->generalMusouSkillListUI->AddSelectImageForRole(curGameFightSkill->getId());
}

void RoleSkillItem::RefreshRoleSkillInfo( int curLevel,GameFightSkill * gameFightSkill )
{
	SkillScene * skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(!skillScene)
		return;

	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	skillScene->musouSkillLayer->RefreshSkillInfo(gameFightSkill->getLevel(),gameFightSkill);
}

void RoleSkillItem::setIndex( int idx )
{
	m_index = idx;
}

int RoleSkillItem::getIndex()
{
	return m_index;
}
