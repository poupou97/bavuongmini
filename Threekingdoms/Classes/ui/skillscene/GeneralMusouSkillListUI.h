#ifndef _SKILLUI_GENERALMUSOUSKILLLISTUI_H_
#define _SKILLUI_GENERALMUSOUSKILLLISTUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CShortCut;
class GameFightSkill;

class GeneralMusouSkillListUI : public UIScene,public CCTableViewDelegate,public CCTableViewDataSource
{
public:
	GeneralMusouSkillListUI();
	~GeneralMusouSkillListUI();

	static GeneralMusouSkillListUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(CCScrollView* view);
	virtual void scrollViewDidZoom(CCScrollView* view);

	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(CCTableView* table, CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(CCTableView *table);

	//添加龙魂标示（有动画）
	void AddMusouForGeneral(long long generalid,int index);
	void AddMusouForRole(std::string skillId);
	//添加选中状态
	void addSelectImage();
	void addSelectImageForGeneral(long long generalid,int index);
	void AddSelectImageForRole(std::string skillId);

	long long getSelectGeneralID();
	CShortCut * getSelectShortCut();
	void setSelectShortCut(CShortCut * shortCut);

	void RefreshStudedSkillList();
	void RefreshRoleSkillItem();

public:
	UILayer * layer_generalMusouSkillList;
	CCTableView * generalList_tableView;

	CCScrollView * m_scrollView;

	bool isSelectImageExist;

private:
	//当前选中的武将ID
	long long m_curSelectGeneralId;
	//当前选中的武将的技能信息
	CShortCut * m_curSelectShortCut;
	//角色学会的技能
	std::vector<GameFightSkill * >m_studedSkillList;
};


/////////////////////////////////
/**
 *   主角技能列表中的单项
 * @author yangjun
 * @version 0.1.0
 * @date 2014.3.11
 */

class CShortCut;

class RoleSkillItem : public UIScene
{
public:
	RoleSkillItem();
	~RoleSkillItem();
	static RoleSkillItem* create(GameFightSkill * gameFightSkill);
	bool init(GameFightSkill * gameFightSkill);

	void DoThing(CCObject *pSender);
	void RefreshRoleSkillInfo(int curLevel,GameFightSkill * gameFightSkill);/**********更新某个技能信息(等级)**********/

	void setIndex(int idx);
	int getIndex();

private:
	GameFightSkill * curGameFightSkill;
	int m_index;
};


/////////////////////////////////
/**
 *  已上阵武将技能列表中的单项
 * @author yangjun
 * @version 0.1.0
 * @date 2014.3.11
 */

class CShortCut;

class GeneralsMusouSkillItem : public UIScene
{
public:
	GeneralsMusouSkillItem();
	~GeneralsMusouSkillItem();
	static GeneralsMusouSkillItem* create(long long genenralid,CShortCut * shortCut);
	bool init(long long genenralid,CShortCut * shortCut);

	void DoThing(CCObject *pSender);
	void RefreshGeneralSkillInfo(std::string skillid);

	void setFrameVisible(bool isVisible);

private:
	long long curGeneralId;
	std::string m_skillId;
	CShortCut * m_curShortCut;

	UIButton *btn_bgFrame;
};


#endif

