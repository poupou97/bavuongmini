#include "FriendInfo.h"
#include "../extensions/CCMoveableMenu.h"
#include "GameView.h"
#include "FriendInfoList.h"
#include "FriendUi.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../Mail_ui/MailUI.h"
#include "../Mail_ui/MailFriend.h"
#include "../../messageclient/element/CRelationPlayer.h"
#include "../extensions/RichTextInput.h"
#include "../Chat_ui/ChatUI.h"
#include "../Chat_ui/AddPrivateUi.h"
#include "../FivePersonInstance/FivePersonInstance.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../extensions/RecruitGeneralCard.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/element/CPlayerGetPhypower.h"
#include "FriendGetPhyPower.h"

FriendInfo::FriendInfo(void)
{
}


FriendInfo::~FriendInfo(void)
{
}

FriendInfo * FriendInfo::create(int idx,int usedUiTag)
{
	FriendInfo * friendInfo =new FriendInfo();
	if (friendInfo && friendInfo->init(idx,usedUiTag))
	{
		friendInfo->autorelease();
		return friendInfo;
	}
	CC_SAFE_DELETE(friendInfo);
	return NULL;
}

bool FriendInfo::init(int idx,int usedUiTag)
{
	if (UIScene::init())
	{
		selectCellIndex = idx;
		uitag_ = usedUiTag;
		
		if (uitag_ == kTagGetphyPowerUi)
		{
			playerId_ = GameView::getInstance()->playerPhypowervector.at(idx)->id();
			playName_ = GameView::getInstance()->playerPhypowervector.at(idx)->name();
			level_ = GameView::getInstance()->playerPhypowervector.at(idx)->level();
			profession_ = GameView::getInstance()->playerPhypowervector.at(idx)->profession();
			vipLevel_ = GameView::getInstance()->playerPhypowervector.at(idx)->viplevel();
			countryId = GameView::getInstance()->playerPhypowervector.at(idx)->country();
			phyPowervalue_ = GameView::getInstance()->playerPhypowervector.at(idx)->value();
		}else
		{
			playerId_ = GameView::getInstance()->relationSourceVector.at(idx)->playerid();
			playName_ = GameView::getInstance()->relationSourceVector.at(idx)->playername();
			onLine_ =GameView::getInstance()->relationSourceVector.at(idx)->online();
			level_ = GameView::getInstance()->relationSourceVector.at(idx)->level();
			profession_ = GameView::getInstance()->relationSourceVector.at(idx)->profession();
			vipLevel_ = GameView::getInstance()->relationSourceVector.at(idx)->viplevel();
			countryId = GameView::getInstance()->relationSourceVector.at(idx)->country();
			phyPowerState_ = GameView::getInstance()->relationSourceVector.at(idx)->sendphypowerstatus();
		}

		CCScale9Sprite * imageBackGround = CCScale9Sprite::create("res_ui/kuang01_new.png");
		imageBackGround->setPreferredSize(CCSizeMake(320,78));
		imageBackGround->setCapInsets(CCRect(15,30,1,1));

		CCMenuItemSprite * itemFriend ; 

		switch(uitag_)
		{
		case kTagFriendUi:
			{
				itemFriend = CCMenuItemSprite::create(imageBackGround, imageBackGround, imageBackGround, this, menu_selector(FriendInfo::callBackWorkList));
			}break;
		case kTagGetphyPowerUi:
			{
				itemFriend = CCMenuItemSprite::create(imageBackGround, imageBackGround, imageBackGround, this, menu_selector(FriendInfo::callBackgetPhypower));
			}break;
		default:
			{
				itemFriend = CCMenuItemSprite::create(imageBackGround, imageBackGround, imageBackGround, this, menu_selector(FriendInfo::showMailFriendInfo));
			}break;
		}

		itemFriend->setZoomScale(1.0f);
		CCMoveableMenu *select_friendL=CCMoveableMenu::create(itemFriend,NULL);
		select_friendL->setAnchorPoint(ccp(0,0));
		select_friendL->setPosition(ccp(20+imageBackGround->getContentSize().width/2,40));
		addChild(select_friendL);

		CCScale9Sprite * iconBackGround = CCScale9Sprite::create("res_ui/LV4_all.png");
		iconBackGround->setPreferredSize(CCSizeMake(72,64));
		iconBackGround->setAnchorPoint(ccp(0.5f,0.5f));
		iconBackGround->setPosition(ccp(65,38));
		addChild(iconBackGround);

		std::string playerIcon_ = BasePlayer::getHeadPathByProfession(profession_);
		CCSprite * friendHead =CCSprite::create(playerIcon_.c_str());
		friendHead->setAnchorPoint(ccp(0.5f,0.5f));
		friendHead->setPosition(ccp(-6+iconBackGround->getContentSize().width/2,1+iconBackGround->getContentSize().height/2));
		friendHead->setScale(0.8f);
		iconBackGround->addChild(friendHead);
		
		char string_lv[5];
		sprintf(string_lv,"%d",level_);
		CCLabelTTF *label_level = CCLabelTTF::create(string_lv, APP_FONT_NAME, 14);
		label_level->setAnchorPoint(ccp(0.5f,0));
		label_level->setPosition(ccp(iconBackGround->getContentSize().width - 14,0));
		iconBackGround->addChild(label_level,10);

		std::string playerProfession_str = RecruitGeneralCard::getGeneralProfessionIconPath(profession_);
		CCSprite * friendProfession_sp =CCSprite::create(playerProfession_str.c_str());
		friendProfession_sp->setAnchorPoint(ccp(0.5f,0.5f));
		friendProfession_sp->setPosition(ccp(5,iconBackGround->getContentSize().height - 5));
		friendProfession_sp->setScale(0.6f);
		iconBackGround->addChild(friendProfession_sp);

		CCScale9Sprite * name_spbg =CCScale9Sprite::create("res_ui/LV4_diaa.png");
		name_spbg->setPreferredSize(CCSizeMake(166,27));
		name_spbg->setAnchorPoint(ccp(0.5f,0.5f));
		name_spbg->setPosition(ccp(185,58));
		addChild(name_spbg);

		CCLabelTTF *label_playName = CCLabelTTF::create(playName_.c_str(), APP_FONT_NAME, 16);
		label_playName->setAnchorPoint(ccp(0.5f,0.5f));
		label_playName->setPosition(ccp(name_spbg->getContentSize().width/2,name_spbg->getContentSize().height/2));
		label_playName->setColor(ccc3(78,255,0));
		name_spbg->addChild(label_playName,10);

		if (countryId > 0 && countryId <6)
		{
			std::string countryIdName = "country_";
			char id[2];
			sprintf(id, "%d", countryId);
			countryIdName.append(id);
			std::string iconPathName = "res_ui/country_icon/";
			iconPathName.append(countryIdName);
			iconPathName.append(".png");
			CCSprite * countrySp_ = CCSprite::create(iconPathName.c_str());
			countrySp_->setAnchorPoint(ccp(0.5f,0.5f));
			countrySp_->setPosition(ccp(label_playName->getPositionX() - label_playName->getContentSize().width/2 - countrySp_->getContentSize().width/2,label_playName->getPositionY()));
			countrySp_->setScale(0.65f);
			name_spbg->addChild(countrySp_);
		}

		if (vipLevel_ > 0)
		{
			CCNode *  widgetVip = MainScene::addVipInfoByLevelForNode(vipLevel_);
			name_spbg->addChild(widgetVip);
			widgetVip->setPosition(ccp(label_playName->getPositionX()+ label_playName->getContentSize().width/2+widgetVip->getContentSize().width/2 ,
												label_playName->getPositionY()));
		}

		if (uitag_ == kTagFriendUi)
		{
			FriendUi * friendui =(FriendUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
			if (friendui != NULL && friendui->curType == FRIENDLIST )
			{
				//when friend is show phyPower 
				if (phyPowerState_ ==0)
				{
					CCSprite * physical_sp = CCSprite::create("res_ui/haoyou/songtili.png");
					CCMenuItemSprite * physical_item = CCMenuItemSprite::create(physical_sp, physical_sp, physical_sp, this, menu_selector(FriendInfo::callBackSendPhyPower));
					physical_item->setZoomScale(0.75f);
					CCMoveableMenu *physical_menu=CCMoveableMenu::create(physical_item,NULL);
					physical_menu->setAnchorPoint(ccp(0,0));
					physical_menu->setPosition(ccp(name_spbg->getPositionX() + name_spbg->getContentSize().width/2+physical_sp->getContentSize().width/2+5,40));
					addChild(physical_menu,12);
				}else
				{
					CCSprite * physical_sp = CCSprite::create("res_ui/haoyou/songtili.png");
					physical_sp->setAnchorPoint(ccp(0.5f,0.5f));
					physical_sp->setPosition(ccp(name_spbg->getPositionX() + name_spbg->getContentSize().width/2+physical_sp->getContentSize().width/2+5,40));
					GameUtils::addGray(physical_sp);
					addChild(physical_sp,12);
				}
			}
		}
		
		if (uitag_ == kTagGetphyPowerUi)
		{
			CCSprite * physical_sp = CCSprite::create("res_ui/haoyou/lingtili.png");
			CCMenuItemSprite * physical_item = CCMenuItemSprite::create(physical_sp, physical_sp, physical_sp, this, menu_selector(FriendInfo::callBackGetPhyPower));
			physical_item->setZoomScale(0.75f);
			CCMoveableMenu *physical_menu=CCMoveableMenu::create(physical_item,NULL);
			physical_menu->setAnchorPoint(ccp(0,0));
			physical_menu->setPosition(ccp(name_spbg->getPositionX() + name_spbg->getContentSize().width/2+physical_sp->getContentSize().width/2+5,40));
			addChild(physical_menu);

			const char * phyPower = StringDataManager::getString("friend_playerOfSendPhyPowerValue");
			char string_[100];
			sprintf(string_,phyPower,phyPowervalue_);

			CCLabelTTF *label_guildName_ = CCLabelTTF::create(string_, APP_FONT_NAME, 16);
			label_guildName_->setAnchorPoint(ccp(0,0));
			label_guildName_->setPosition(ccp(125,16));
			label_guildName_->setColor(ccc3(255,231,25));
			addChild(label_guildName_,10);
		}else
		{
			const char *familystr_ = StringDataManager::getString("friend_playerOfFamilyLabel");
			std::string playerfamilyStr_ =familystr_;
			std::string guildName_ = GameView::getInstance()->relationSourceVector.at(idx)->guildname();
			int guildNameLen = strlen(guildName_.c_str());
			if (guildNameLen > 0)
			{
				playerfamilyStr_.append(guildName_);
			}else
			{
				const char *familystrIsNUll_ = StringDataManager::getString("friend_playerOfFamilyIsNull");
				playerfamilyStr_.append(familystrIsNUll_);
			}

			CCLabelTTF *label_guildName_ = CCLabelTTF::create(playerfamilyStr_.c_str(), APP_FONT_NAME, 16);
			label_guildName_->setAnchorPoint(ccp(0,0));
			label_guildName_->setPosition(ccp(105,25));
			addChild(label_guildName_,10);

			if (onLine_==0)
			{
				CCScale9Sprite * onLineSpIcon_ =CCScale9Sprite::create("res_ui/haoyouzhezhao.png");
				onLineSpIcon_->setPreferredSize(CCSizeMake(320,78));
				onLineSpIcon_->setCapInsets(CCRect(10,10,1,1));
				onLineSpIcon_->setAnchorPoint(ccp(0.5f,0.5f));
				onLineSpIcon_->setPosition(ccp(180,40));
				addChild(onLineSpIcon_,11);

				CCSprite * onLine_ = CCSprite::create("res_ui/haoyou/lixian.png");
				onLine_->setAnchorPoint(ccp(0.5f,0.5f));
				onLine_->setPosition(ccp(60+onLine_->getContentSize().width/2 ,75 - onLine_->getContentSize().height/2));
				addChild(onLine_,12);

				long long lastTime_ =  GameView::getInstance()->relationSourceVector.at(idx)->lastlogintime();
				
				const char *mapStr_ = StringDataManager::getString("friend_playerOfLastOnlineTIme");
				std::string lastOnlineTime_ = mapStr_;
				lastOnlineTime_.append(getlastTime(lastTime_));
				CCLabelTTF *label_mapName_ = CCLabelTTF::create(lastOnlineTime_.c_str(), APP_FONT_NAME, 16);
				label_mapName_->setAnchorPoint(ccp(0,0));
				label_mapName_->setPosition(ccp(105,7));
				addChild(label_mapName_,10);

			}else
			{
				std::string mapId_ = GameView::getInstance()->relationSourceVector.at(idx)->mapid();
				std::string mapNameStr_ = StaticDataMapName::s_mapname[mapId_].c_str();

				const char *mapStr_ = StringDataManager::getString("friendInfo_mapInfo");
				std::string playerMap =mapStr_;
				playerMap.append(mapNameStr_);

				CCLabelTTF *label_mapName_ = CCLabelTTF::create(playerMap.c_str(), APP_FONT_NAME, 16);
				label_mapName_->setAnchorPoint(ccp(0,0));
				label_mapName_->setPosition(ccp(105,5));
				addChild(label_mapName_,10);
			}
		}		
		return true;
	}
	return false;
}

void FriendInfo::onEnter()
{
	UIScene::onEnter();
}

void FriendInfo::onExit()
{
	UIScene::onExit();
}

void FriendInfo::callBackWorkList( CCObject * obj )
{	
	FriendUi * friendui =(FriendUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	if (friendui != NULL)
	{
		friendui->cellIdSelect_ = selectCellIndex;
		friendui->cellSelectTag = this->getTag();
	}
}

void FriendInfo::callBackgetPhypower( CCObject * obj )
{
	FriendGetPhyPower * phypower_ui =(FriendGetPhyPower *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGetphyPowerUi);
	if (phypower_ui != NULL)
	{
		phypower_ui->curVectorIndex = selectCellIndex;
		phypower_ui->curSelectCellTag = this->getTag();
	}
}

void FriendInfo::showMailFriendInfo( CCObject * obj )
{
	int index_= selectCellIndex;
	long long playerId_ =GameView::getInstance()->relationFriendVector.at(index_)->playerid();
	std::string friendName_=GameView::getInstance()->relationSourceVector.at(index_)->playername();

	switch(uitag_)
	{
	case kTabChat:
		{
			ChatUI * chatui_ =(ChatUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
			if (chatui_!=NULL)
			{
				AddPrivateUi * privateui=(AddPrivateUi *)chatui_->layer_3->getChildByTag(PRIVATEUI);
				if (privateui != NULL)
				{
					privateui->textBox_private->deleteAllInputString();
					privateui->textBox_private->onTextFieldInsertText(NULL,friendName_.c_str(),30);
				}
			}
		}break;
	case kTagMailUi:
		{
			MailUI * mail_ui=(MailUI*) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
			if (mail_ui != NULL)
			{
				mail_ui->friendName->deleteAllInputString();
				mail_ui->friendName->onTextFieldInsertText(NULL,friendName_.c_str(),30);
			}
		}break;
	case kTagInstanceDetailUI:
		{
			// request to call friend to enter instance
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1923,(void *)playerId_);
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("fivePerson_callFriend_des"));
		}break;
		// 	case kTagFriendUi:
		// 		{
		// 			FriendUi * friendui =(FriendUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
		// 			if (friendui != NULL)
		// 			{
		// 				friendui->cellIdSelect_ = selectCellIndex;
		// 				friendui->cellSelectTag = this->getTag();
		// 			}
		// 		}break;
	}
	MailFriend * mailFriend = (MailFriend *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagPopFriendListUI);
	if (mailFriend != NULL)
	{
		mailFriend->removeFromParentAndCleanup(true);
	}



	/*
	int index_= selectCellIndex;
	long long playerId_ =GameView::getInstance()->relationFriendVector.at(index_)->playerid();

	MailFriend * mailFriend = (MailFriend *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagPopFriendListUI);
	mailFriend->removeFromParentAndCleanup(true);
	std::string friendName_=GameView::getInstance()->relationSourceVector.at(index_)->playername();
	
	ChatUI * chatui_ =(ChatUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
	MailUI * mail_ui=(MailUI*) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
	if (mail_ui !=NULL)
	{
		mail_ui->friendName->deleteAllInputString();
		mail_ui->friendName->onTextFieldInsertText(NULL,friendName_.c_str(),30);
		return;
	}
	else if (chatui_!=NULL)
	{
		AddPrivateUi * privateui=(AddPrivateUi *)chatui_->layer_3->getChildByTag(PRIVATEUI);
		if (privateui != NULL)
		{
			privateui->textBox_private->deleteAllInputString();
			privateui->textBox_private->onTextFieldInsertText(NULL,friendName_.c_str(),30);
		}
		return;
	}
	else
	{
		// request to call friend to enter instance
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1923,(void *)playerId_);
	}
	*/
}

void FriendInfo::callBackSendPhyPower( CCObject * obj )
{
	FriendUi * friendui =(FriendUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	friendui->setTouchCellType(ktypeTouchCellOfMenuEvent);

	friendui->cellIdSelect_ = selectCellIndex;
	long long playerId_ =GameView::getInstance()->relationFriendVector.at(selectCellIndex)->playerid();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5094,(void *)playerId_);
}

void FriendInfo::callBackGetPhyPower( CCObject * obj )
{
	FriendGetPhyPower * phypower_ui = (FriendGetPhyPower *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGetphyPowerUi);
	if (phypower_ui != NULL)
	{
		phypower_ui->setPhypowerTouchCellType(phypower_ktypeTouchCellOfMenuEvent);
		if (phypower_ui->m_getPhypowerCount <= 0)
		{
			std::string str_ = StringDataManager::getString("curDayIsFull_lingqutili");
			GameView::getInstance()->showAlertDialog(str_);
		}else
		{
			long long playerId_ =GameView::getInstance()->playerPhypowervector.at(selectCellIndex)->id();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5133,(void *)playerId_);
		}
	}
}

std::string FriendInfo::getlastTime( long long time_ )
{
	std::string memberState_ = "";
	if ((long long)(time_/24)>0)//1 day
	{
		if ((long long)(time_/(long long)(24*30)) >0)//1 moth
		{
			const char* strings = StringDataManager::getString("family_mothago");
			int online_ =int (time_/(long long)(24*30));
			char onlineStr_[20];
			sprintf(onlineStr_,"%d",online_);

			memberState_.append(onlineStr_);
			memberState_.append(strings);
		}else
		{
			const char* strings = StringDataManager::getString("family_dayAgo");
			int online_ =int (time_/24);
			char onlineStr_[20];
			sprintf(onlineStr_,"%d",online_);
			memberState_.append(onlineStr_);
			memberState_.append(strings);
		}
	}else
	{
		if (time_ <= 1)
		{
			const char* strings = StringDataManager::getString("family_minAgo");
			memberState_.append(strings);
		}else
		{
			const char* strings = StringDataManager::getString("family_hourAgo");
			char onlineStr_[20];
			sprintf(onlineStr_,"%lld",time_);
			memberState_.append(onlineStr_);
			memberState_.append(strings);
		}
	}
	return memberState_;
}
