#include "FriendWorkList.h"
#include "../extensions/CCMoveableMenu.h"
#include "GameView.h"
#include "../Chat_ui/ChatUI.h"
#include "../Mail_ui/MailUI.h"
#include "FriendInfo.h"
#include "FriendUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/generals_ui/GeneralsListBase.h"
#include "FriendInfoList.h"


FriendWorkList::FriendWorkList(void)
{
	winSize=CCDirector::sharedDirector()->getVisibleSize();
	friendNum=0;
	lastSelectCellId =0;
	lastSelectCellTag = FRIENDINFOCELLL;
}


FriendWorkList::~FriendWorkList(void)
{
}

FriendWorkList * FriendWorkList::create()
{
	FriendWorkList *friendworklist=new FriendWorkList();
	friendworklist->autorelease();
	return friendworklist;
}

bool FriendWorkList::init()
{
	if (UIScene::init())
	{
		tableView = CCTableView::create(this, CCSizeMake(671, 305));
		tableView->setDirection(kCCScrollViewDirectionVertical);
		tableView->setAnchorPoint(ccp(0,0));
		tableView->setPosition(ccp(35,66));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(tableView,1,FRIENDWORKLISTTAB);
		tableView->reloadData();
		FriendUi * friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
		friend_ui->setTouchCellType(ktypeTouchCellEvent);

		this->setContentSize(CCSizeMake(715,340));
		return true;
	}
	return false;
}

void FriendWorkList::onEnter()
{
	UIScene::onEnter();
}

void FriendWorkList::onExit()
{
	UIScene::onExit();
}

void FriendWorkList::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}

void FriendWorkList::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void FriendWorkList::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	int cellId = cell->getIdx();
	FriendUi * friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);

	if (table->cellAtIndex(lastSelectCellId) != NULL)
	{
		FriendInfo * friendCell = (FriendInfo *)table->cellAtIndex(lastSelectCellId)->getChildByTag(lastSelectCellTag);
		if (friendCell != NULL)
		{
			if (friendCell->getChildByTag(1212) != NULL)
			{
				friendCell->getChildByTag(1212)->removeFromParent();
			}
		}
	}
	
	FriendInfo * friendCurCell = (FriendInfo *)table->cellAtIndex(cellId)->getChildByTag(friend_ui->cellSelectTag);
	if (friendCurCell == NULL)
	{
		return;
	}
	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(15, 30, 1, 1) , "res_ui/highlight.png");
	pHighlightSpr->setPreferredSize(CCSize(322,80));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setTag(1212);
	pHighlightSpr->setPosition(ccp(18,1));
	friendCurCell->addChild(pHighlightSpr,20);
	
	// type =ktypeTouchCellEvent 
	// type =ktypeTouchCellOfMenuEvent
	if (friend_ui->getTouchCellType() == ktypeTouchCellEvent)
	{
		int pointX = friendCurCell->getPosition().x;
		int pointY = cell->getPosition().y;
		int offX = table->getContentOffset().x;
		int offY = table->getContentOffset().y;

		FriendInfoList * infolist =FriendInfoList::create(friend_ui->curType);
		infolist->ignoreAnchorPointForPosition(false);
		infolist->setAnchorPoint(ccp(0,1));
		infolist->setZOrder(10);
		infolist->setPosition(ccp(pointX + offX + 100 ,friend_ui->layer_->getContentSize().height/2+infolist->getContentSize().height/2+80));
		friend_ui->layer_->addChild(infolist);
	}
	friend_ui->setTouchCellType(ktypeTouchCellEvent);
 	lastSelectCellId =cell->getIdx();
	lastSelectCellTag = friend_ui->cellSelectTag;
}

cocos2d::CCSize FriendWorkList::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(671,80);
}

cocos2d::extension::CCTableViewCell* FriendWorkList::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell =table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();
	
	int friendNum = GameView::getInstance()->relationSourceVector.size();
	FriendUi * friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);

	if (idx == friendNum/2)
	{
		if (friendNum%2 == 0)
		{
			FriendInfo * friendInfoL =FriendInfo::create(2*idx,kTagFriendUi);
			friendInfoL->setAnchorPoint(ccp(0.5f,0.5f));
			friendInfoL->setPosition(ccp(0,0));
			friendInfoL->setTag(FRIENDINFOCELLL);
			cell->addChild(friendInfoL);

			FriendInfo * friendInfoR =FriendInfo::create(2*idx+1,kTagFriendUi);
			friendInfoR->setAnchorPoint(ccp(0.5f,0.5f));
			friendInfoR->setPosition(ccp(325,0));
			friendInfoR->setTag(FRIENDINFOCELLR);
			cell->addChild(friendInfoR);
		}else
		{
			FriendInfo * friendInfoL =FriendInfo::create(2*idx,kTagFriendUi);
			friendInfoL->setAnchorPoint(ccp(0.5f,0.5f));
			friendInfoL->setPosition(ccp(0,0));
			friendInfoL->setTag(FRIENDINFOCELLL);
			cell->addChild(friendInfoL);
		}
	}
	else
	{
		FriendInfo * friendInfoL =FriendInfo::create(2*idx,kTagFriendUi);
		friendInfoL->setAnchorPoint(ccp(0.5f,0.5f));
		friendInfoL->setPosition(ccp(0,0));
		friendInfoL->setTag(FRIENDINFOCELLL);
		cell->addChild(friendInfoL);

		FriendInfo * friendInfoR =FriendInfo::create(2*idx+1,kTagFriendUi);
		friendInfoR->setAnchorPoint(ccp(0.5f,0.5f));
		friendInfoR->setPosition(ccp(325,0));
		friendInfoR->setTag(FRIENDINFOCELLR);
		cell->addChild(friendInfoR);
	}

	if(idx == friendNum/2 - 3)
	{
		FriendUi * friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
		if (friend_ui->pageIndexSelect < totalPageNum-1 )
		{
			friend_ui->pageIndexSelect++;
			switch(friend_ui->curType)
			{
			case CHECKTABLIST_ONEKEY:
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(2210,friend_ui);
				}break;
			case CHECKTABLIST_CHECKBOX:
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(2209,friend_ui);
				}break;
			default:
				{
					FriendStruct friend1={friend_ui->curType,20,friend_ui->pageIndexSelect,0};
					GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);
				}break;
			}
		}
	}
	return cell;
}

unsigned int FriendWorkList::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	int vectorSize = GameView::getInstance()->relationSourceVector.size();
	
	if (vectorSize%2 == 0)
	{
		return vectorSize/2;
	}else
	{
		return vectorSize/2 + 1;
	}
}

bool FriendWorkList::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return resignFirstResponder(pTouch,this,false);
}

void FriendWorkList::ReloadTableViewWithoutChangeOffSet()
{
	CCPoint offset = this->tableView->getContentOffset();
	this->tableView->reloadData();
	this->tableView->setContentOffset(offset);
}

