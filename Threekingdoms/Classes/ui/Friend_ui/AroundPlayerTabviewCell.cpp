#include "AroundPlayerTabviewCell.h"
#include "../extensions/CCMoveableMenu.h"
#include "GameView.h"
#include "../../messageclient/element/CAroundPlayer.h"
#include "FriendUi.h"
#include "FriendInfoList.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"

AroundPlayerTabviewCell::AroundPlayerTabviewCell(void)
{
}


AroundPlayerTabviewCell::~AroundPlayerTabviewCell(void)
{
}

AroundPlayerTabviewCell * AroundPlayerTabviewCell::create(int idx)
{
	AroundPlayerTabviewCell * aroundplayer= new AroundPlayerTabviewCell();
	if (aroundplayer && aroundplayer->init(idx))
	{
		aroundplayer->autorelease();
		return aroundplayer;
	}
	CC_SAFE_DELETE(aroundplayer);
	return NULL;
}

bool AroundPlayerTabviewCell::init(int idx)
{
	if (UIScene::init())
	{
		selectCellId =idx;

		std::string roleName_= GameView::getInstance()->aroundPlayerVector.at(idx)->rolename();
		long long roldId_= GameView::getInstance()->aroundPlayerVector.at(idx)->roleid();
		int profession_ =GameView::getInstance()->aroundPlayerVector.at(idx)->profession();
		int level_= GameView::getInstance()->aroundPlayerVector.at(idx)->level();
		CCScale9Sprite * imageBackGround = CCScale9Sprite::create("res_ui/kuang01_new.png");
		imageBackGround->setPreferredSize(CCSizeMake(320,78));
		imageBackGround->setCapInsets(CCRect(15,30,1,1));
		imageBackGround->setZOrder(0);

		FriendUi * friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
		CCMenuItemSprite * itemFriend = CCMenuItemSprite::create(imageBackGround, imageBackGround, imageBackGround, 
																	this, menu_selector(AroundPlayerTabviewCell::callBack));
		itemFriend->setZoomScale(1.0f);
		CCMoveableMenu *select_friendL=CCMoveableMenu::create(itemFriend,NULL);
		select_friendL->setAnchorPoint(ccp(0,0));
		select_friendL->setPosition(ccp(20+imageBackGround->getContentSize().width/2,35));
		addChild(select_friendL);

		CCScale9Sprite * iconBackGround =CCScale9Sprite::create("res_ui/LV4_allb.png");
		iconBackGround->setPreferredSize(CCSizeMake(60,59));//62
		iconBackGround->setAnchorPoint(ccp(0.5f,0.5f));
		iconBackGround->setPosition(ccp(59,35.5f));//60 35
		addChild(iconBackGround);

		std::string playerIcon_ = BasePlayer::getHeadPathByProfession(profession_);
		CCSprite * friendHead =CCSprite::create(playerIcon_.c_str());
		friendHead->setAnchorPoint(ccp(0.5f,0.5f));
		friendHead->setScale(0.8f);
		friendHead->setPosition(ccp(59,34));//59 35
		addChild(friendHead);

		CCLabelTTF *label_playName = CCLabelTTF::create(roleName_.c_str(), APP_FONT_NAME, 16);
		label_playName->setAnchorPoint(ccp(0,0));
		label_playName->setPosition(ccp(150,40));
		addChild(label_playName,10);

		int vipLevel = GameView::getInstance()->aroundPlayerVector.at(idx)->viplevel();
		if (vipLevel > 0)
		{
			CCNode * vipNode = MainScene::addVipInfoByLevelForNode(vipLevel);
			addChild(vipNode);
			vipNode->setPosition(ccp(label_playName->getPositionX() + label_playName->getContentSize().width + vipNode->getContentSize().width/2,
											label_playName->getPositionY()+ vipNode->getContentSize().height/2));
		}


		std::string pression_name =BasePlayer::getProfessionNameIdxByIndex(profession_);
		const char *pressStr_ = StringDataManager::getString("friend_pressionName");
		std::string pressionStr_ =pressStr_;
		pressionStr_.append(pression_name);

		CCLabelTTF *label_playprofession = CCLabelTTF::create(pressionStr_.c_str(), APP_FONT_NAME, 16);
		label_playprofession->setAnchorPoint(ccp(0,0));
		label_playprofession->setPosition(ccp(115,15));
		addChild(label_playprofession,10);

		char string_lv[5];
		sprintf(string_lv,"%d",level_);
		const char *levelstr_ = StringDataManager::getString("friend_playerLevel");
		std::string playerlevelStr_ =levelstr_;
		playerlevelStr_.append(string_lv);

		CCLabelTTF *label_level = CCLabelTTF::create(playerlevelStr_.c_str(), APP_FONT_NAME, 16);
		label_level->setAnchorPoint(ccp(0,0));
		label_level->setPosition(ccp(230,15));
		addChild(label_level,10);

		return true;
	}
	return false;
}

void AroundPlayerTabviewCell::onEnter()
{
	UIScene::onEnter();
}

void AroundPlayerTabviewCell::onExit()
{
	UIScene::onExit();
}

void AroundPlayerTabviewCell::callBack( CCObject * obj )
{
	FriendUi * friendui =(FriendUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
 	friendui->cellIdSelect_ = selectCellId;
	friendui->cellSelectTag = this->getTag();
}


