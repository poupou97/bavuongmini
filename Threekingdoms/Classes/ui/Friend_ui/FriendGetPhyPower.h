#ifndef FRIEND_GETPHYPOWER_H
#define FRIEND_GETPHYPOWER_H

#include "../extensions/UIScene.h"
class CPlayerGetPhypower;

enum
{
	phypower_ktypeTouchCellEvent = 0,
	phypower_ktypeTouchCellOfMenuEvent,
};

class FriendGetPhyPower:public UIScene,public CCTableViewDataSource,public CCTableViewDelegate
{
public:
	FriendGetPhyPower(void);
	~FriendGetPhyPower(void);

	enum
	{
		phypowerCellLeft = 10,
		phypowerCellRight = 20,
	};

	static FriendGetPhyPower * create(int count);
	bool init(int count);
	void onEnter();
	void onExit();


	void callBackClose(CCObject * obj);

	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	//一共生成多少项
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	// default implements are used to call script callback if exist
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);

	void setPhypowerTouchCellType(int type_);
	int getPhypowerTouchCellType();

	void setShowPhypowerlabel(bool isShow);
public:
	CCSize winsize;
	CCTableView * phypowerTavleView;
	UILabelBMFont * remPhyPowerCount_;

	int lastSelectCellId;
	int lastSelectCellTag;
	int curSelectCellTag;
	int m_curTouchType;
	UILayer *phypower_layer;
	int m_getPhypowerCount;
	UILabelBMFont * labelBmFont_phypowerNull;
public:
	int curVectorIndex;
	long long phypower_playerId;
	std::string phypower_playerName;

};
#endif
