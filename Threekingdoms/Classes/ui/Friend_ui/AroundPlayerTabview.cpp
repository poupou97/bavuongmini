#include "AroundPlayerTabview.h"
#include "GameView.h"
#include "../../messageclient/element/CAroundPlayer.h"
#include "../extensions/CCMoveableMenu.h"
#include "../Friend_ui/AroundPlayerTabviewCell.h"
#include "../extensions/UITab.h"
#include "FriendUi.h"
#include "FriendInfoList.h"
#include "../../gamescene_state/MainScene.h"
#include "FriendInfo.h"

AroundPlayerTabview::AroundPlayerTabview(void)
{
	lastSelectCellId =0;
	lastSelectCellTag =PLAYEROTHERFRIENDCELLL;
}

AroundPlayerTabview::~AroundPlayerTabview(void)
{
}

AroundPlayerTabview * AroundPlayerTabview::create()
{
	AroundPlayerTabview * aroundPlayer= new AroundPlayerTabview();
	aroundPlayer->autorelease();
	return aroundPlayer;
}

bool AroundPlayerTabview::init()
{
	if (UIScene::init())
	{
		CCTableView *tableView = CCTableView::create(this, CCSizeMake(670, 350));
		tableView->setDirection(kCCScrollViewDirectionVertical);
		tableView->setAnchorPoint(ccp(0,0));
		tableView->setPosition(ccp(0,0));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(tableView,1,AROUNDPLAYERTABVIEW);
		tableView->reloadData();

		this->setContentSize(CCSizeMake(670,350));

		return true;
	}
	return false;
}

void AroundPlayerTabview::onEnter()
{
	UIScene::onEnter();
}

void AroundPlayerTabview::onExit()
{
	UIScene::onExit();
}

void AroundPlayerTabview::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}

void AroundPlayerTabview::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void AroundPlayerTabview::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	int cellId= cell->getIdx();

	FriendUi * friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);

	if (table->cellAtIndex(lastSelectCellId) != NULL)
	{
		FriendInfo * friendCell = (FriendInfo *)table->cellAtIndex(lastSelectCellId)->getChildByTag(lastSelectCellTag);
		if (friendCell != NULL)
		{
			if (friendCell->getChildByTag(1212) != NULL)
			{
				friendCell->getChildByTag(1212)->removeFromParent();
			}
		}
	}
	
	FriendInfo * friendCurCell = (FriendInfo *)table->cellAtIndex(cellId)->getChildByTag(friend_ui->cellSelectTag);
	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(15, 30, 1, 1) , "res_ui/highlight.png");
	pHighlightSpr->setPreferredSize(CCSize(322,80));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setTag(1212);
	pHighlightSpr->setPosition(ccp(18,-6));
	friendCurCell->addChild(pHighlightSpr);
	
	int pointX = friendCurCell->getPosition().x;
	int pointY = cell->getPosition().y;
	int offX = table->getContentOffset().x;
	int offY = table->getContentOffset().y;

	FriendInfoList * infolist =FriendInfoList::create(friend_ui->curType);
	infolist->ignoreAnchorPointForPosition(false);
	infolist->setAnchorPoint(ccp(0,1));
	infolist->setZOrder(10);
	/*
	if (offY + pointY +infolist->getContentSize().height >= 480)
	{
		infolist->setPosition(ccp(pointX + offX + 100,offY + pointY - infolist->getContentSize().height/2));
	}else
	{
		infolist->setPosition(ccp(pointX + offX + 100,offY + pointY));
	}
	*/
	infolist->setPosition(ccp(pointX + offX + 100 ,friend_ui->layer_->getContentSize().height/2+infolist->getContentSize().height/2+50));
	friend_ui->layer_->addChild(infolist);

 	lastSelectCellId =cell->getIdx();
	lastSelectCellTag = friend_ui->cellSelectTag;
}

cocos2d::CCSize AroundPlayerTabview::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(700,80);
}

cocos2d::extension::CCTableViewCell* AroundPlayerTabview::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell =table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();
	int aroundPlayerNum= GameView::getInstance()->aroundPlayerVector.size();

	if (aroundPlayerNum/2==idx)
	{
		if (aroundPlayerNum%2 == 1)
		{
			AroundPlayerTabviewCell * aroundplayerCell= AroundPlayerTabviewCell::create(2*idx);
			aroundplayerCell->setAnchorPoint(ccp(0,0));
			aroundplayerCell->setPosition(ccp(0,0));
			aroundplayerCell->setTag(PLAYEROTHERFRIENDCELLL);
			cell->addChild(aroundplayerCell);

		}else
		{
			AroundPlayerTabviewCell * aroundplayerCellL= AroundPlayerTabviewCell::create(2*idx);
			aroundplayerCellL->setAnchorPoint(ccp(0,0));
			aroundplayerCellL->setPosition(ccp(0,0));
			aroundplayerCellL->setTag(PLAYEROTHERFRIENDCELLL);
			cell->addChild(aroundplayerCellL);

			AroundPlayerTabviewCell * aroundplayerCellR= AroundPlayerTabviewCell::create(2*idx+1);
			aroundplayerCellR->setAnchorPoint(ccp(0,0));
			aroundplayerCellR->setPosition(ccp(325,0));
			aroundplayerCellR->setTag(PLAYEROTHERFRIENDCELLR);
			cell->addChild(aroundplayerCellR);

		}
	}else
	{
		AroundPlayerTabviewCell * aroundplayerCellL= AroundPlayerTabviewCell::create(2*idx);
		aroundplayerCellL->setAnchorPoint(ccp(0,0));
		aroundplayerCellL->setPosition(ccp(0,0));
		aroundplayerCellL->setTag(PLAYEROTHERFRIENDCELLL);
		cell->addChild(aroundplayerCellL);

		AroundPlayerTabviewCell * aroundplayerCellR= AroundPlayerTabviewCell::create(2*idx+1);
		aroundplayerCellR->setAnchorPoint(ccp(0,0));
		aroundplayerCellR->setPosition(ccp(325,0));
		aroundplayerCellR->setTag(PLAYEROTHERFRIENDCELLR);
		cell->addChild(aroundplayerCellR);

	}

	return cell;
}

unsigned int AroundPlayerTabview::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	int vectorSize = GameView::getInstance()->aroundPlayerVector.size();
	int numEnd = vectorSize%2;
	int numCellNum = vectorSize/2+ numEnd;
	return numCellNum;
}

bool AroundPlayerTabview::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return resignFirstResponder(pTouch,this,false); 
}

// void AroundPlayerTabview::callBack( CCObject * obj )
// {
// 	FriendUi * friendui =(FriendUi *)this->getParent();
// 	int currSelectType=friendui->curType;
// 	FriendInfoList * infolist =FriendInfoList::create(currSelectType);
// 	infolist->ignoreAnchorPointForPosition(false);
// 	infolist->setAnchorPoint(ccp(0,0));
// 	infolist->setPosition(ccp(200,100));
// 	CCDirector::sharedDirector()->getRunningScene()->addChild(infolist);
// }

