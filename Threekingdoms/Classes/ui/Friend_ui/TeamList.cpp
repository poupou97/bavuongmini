#include "TeamList.h"
#include "GameView.h"
#include "../../messageclient/element/CMapTeam.h"
#include "../extensions/CCMoveableMenu.h"
#include "FriendUi.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/GameActor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"


TeamList::TeamList(void)
{
	selectId=0;
}


TeamList::~TeamList(void)
{
}

TeamList * TeamList::create()
{
	TeamList *team=new TeamList();
	team->autorelease();
	return team;
}

bool TeamList::init()
{
	if (UIScene::init())
	{

		CCTableView* tableView = CCTableView::create(this, CCSizeMake(325,275));
		tableView->setDirection(kCCScrollViewDirectionVertical);
		tableView->setSelectedEnable(true);
		tableView->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(25, 24, 1, 1), ccp(0,0));
		tableView->setAnchorPoint(ccp(0,0));
		tableView->setPosition(ccp(0,0));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		addChild(tableView,1,TEAMLISTTABVIEW);
		tableView->reloadData();
		
		return true;
	}
	return false;
}

void TeamList::onEnter()
{
	UIScene::onEnter();
}

void TeamList::onExit()
{
	UIScene::onExit();
}

void TeamList::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}

void TeamList::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void TeamList::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	selectId =cell->getIdx();
	FriendUi * friendui=(FriendUi *)this->getParent()->getParent();
	friendui->showTeamPlayer(selectId);
}

cocos2d::CCSize TeamList::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(323,70);
}

cocos2d::extension::CCTableViewCell* TeamList::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell =table->dequeueCell();   // this method must be called
	cell=new CCTableViewCell();
	cell->autorelease();
	FriendUi * friendui=(FriendUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	
	CCScale9Sprite * spbg=CCScale9Sprite::create("res_ui/kuang01_new.png");
	spbg->setAnchorPoint(ccp(0,0));
	spbg->setPosition(ccp(0,0));
	spbg->setPreferredSize(CCSizeMake(321,68));
	spbg->setCapInsets(CCRect(15,30,1,1));
	cell->addChild(spbg);

	int teamid_=GameView::getInstance()->mapteamVector.at(idx)->teamid();
	long long playerid_= GameView::getInstance()->mapteamVector.at(idx)->leaderid();
	long long playerSelfId =  GameView::getInstance()->myplayer->getRoleId();
	//self have team
	if (teamid_ == friendui->playerTeamId)
	{
		/////////self is leader
		if (playerid_ == playerSelfId)
		{
			const char *strings_dismiss = StringDataManager::getString("team_dismiss");
			char *dimissTeam=const_cast<char*>(strings_dismiss);	

			const char *strings_exitTeam = StringDataManager::getString("team_exit");
			char *exitTeam=const_cast<char*>(strings_exitTeam);

			CCScale9Sprite * bg_dismiss = CCScale9Sprite::create("res_ui/new_button_2.png");
			bg_dismiss->setPreferredSize(CCSizeMake(80,36));
			bg_dismiss->setCapInsets(CCRect(18,9,2,23));

			CCMenuItemSprite *btn_dismiss = CCMenuItemSprite::create(bg_dismiss, bg_dismiss, bg_dismiss, this, menu_selector(TeamList::callBackDismiss));
			btn_dismiss->setZoomScale(0.5f);
			CCMoveableMenu *menu_dismiss=CCMoveableMenu::create(btn_dismiss,NULL);
			menu_dismiss->setAnchorPoint(ccp(0,0));
			menu_dismiss->setPosition(ccp(195,38));
			cell->addChild(menu_dismiss);
			CCLabelTTF * label_dismiss= CCLabelTTF::create(dimissTeam,APP_FONT_NAME,18);
			label_dismiss->setAnchorPoint(ccp(0,0));
			label_dismiss->setPosition(ccp(175,28));
			cell->addChild(label_dismiss);

			CCScale9Sprite * bg_exit = CCScale9Sprite::create("res_ui/new_button_1.png");
			bg_exit->setPreferredSize(CCSizeMake(80,36));
			bg_exit->setCapInsets(CCRect(18,9,2,23));

			CCMenuItemSprite *btn_exit = CCMenuItemSprite::create(bg_exit, bg_exit, bg_exit, this, menu_selector(TeamList::callBackExit));
			btn_exit->setZoomScale(0.5f);
			CCMoveableMenu *menu_exit=CCMoveableMenu::create(btn_exit,NULL);
			menu_exit->setAnchorPoint(ccp(0,0));
			menu_exit->setPosition(ccp(275,38));
			cell->addChild(menu_exit);
			CCLabelTTF * label_exit= CCLabelTTF::create(exitTeam,APP_FONT_NAME,18);
			label_exit->setAnchorPoint(ccp(0,0));
			label_exit->setPosition(ccp(255,28));
			cell->addChild(label_exit);
		}else
		{
			const char *strings_appaly = StringDataManager::getString("team_exit");
			char *applyTeam=const_cast<char*>(strings_appaly);

			CCScale9Sprite * bg_exit = CCScale9Sprite::create("res_ui/new_button_1.png");
			bg_exit->setPreferredSize(CCSizeMake(80,36));
			bg_exit->setCapInsets(CCRect(18,9,2,23));
			CCMenuItemSprite *btn_exit = CCMenuItemSprite::create(bg_exit, bg_exit, bg_exit, this, menu_selector(TeamList::callBackExit));
			btn_exit->setZoomScale(0.5f);
			CCMoveableMenu *menu_exit=CCMoveableMenu::create(btn_exit,NULL);
			menu_exit->setAnchorPoint(ccp(0,0));
			menu_exit->setPosition(ccp(275,38));
			cell->addChild(menu_exit);

			CCLabelTTF * label_exit= CCLabelTTF::create(applyTeam, APP_FONT_NAME,18);
			label_exit->setAnchorPoint(ccp(0,0));
			label_exit->setPosition(ccp(255,28));
			cell->addChild(label_exit);
		}

	}else
	{
		const char *strings_appaly = StringDataManager::getString("team_apply");
		char *applyTeam=const_cast<char*>(strings_appaly);

		CCScale9Sprite * bg_apply = CCScale9Sprite::create("res_ui/new_button_2.png");
		bg_apply->setPreferredSize(CCSizeMake(80,36));
		bg_apply->setCapInsets(CCRect(18,9,2,23));
		CCMenuItemSprite *btn_apply = CCMenuItemSprite::create(bg_apply, bg_apply, bg_apply, this, menu_selector(TeamList::callBackApply));
		btn_apply->setZoomScale(0.5f);
		btn_apply->setTag(playerid_);
		CCMoveableMenu *menu_apply=CCMoveableMenu::create(btn_apply,NULL);
		menu_apply->setAnchorPoint(ccp(0,0));
		menu_apply->setPosition(ccp(275,38));
		cell->addChild(menu_apply);

		CCLabelTTF * label_exit= CCLabelTTF::create(applyTeam, APP_FONT_NAME,18);
		label_exit->setAnchorPoint(ccp(0,0));
		label_exit->setPosition(ccp(255,28));
		cell->addChild(label_exit);
	}

	CCScale9Sprite * name_spbg =CCScale9Sprite::create("res_ui/LV4_diaa.png");
	name_spbg->setPreferredSize(CCSizeMake(140,27));
	name_spbg->setAnchorPoint(ccp(0.5f,0.5f));
	name_spbg->setPosition(ccp(name_spbg->getContentSize().width/2+10,38));
	cell->addChild(name_spbg);

	std::string teamName_=GameView::getInstance()->mapteamVector.at(idx)->teamname();
	CCLabelTTF * label_teamName =CCLabelTTF::create();
	label_teamName->setAnchorPoint(ccp(0.5f,0.5f));
	label_teamName->setPosition(ccp(name_spbg->getContentSize().width/2,name_spbg->getContentSize().height/2));
	label_teamName->setString(teamName_.c_str());
	label_teamName->setFontSize(18);
	name_spbg->addChild(label_teamName,10);
	
	return cell;
}

unsigned int TeamList::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	return GameView::getInstance()->mapteamVector.size();
}

bool TeamList::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return resignFirstResponder(pTouch,this,false);
}

void TeamList::callBackApply( CCObject * obj )
{
	CCMenuItemSprite * menu_= (CCMenuItemSprite *)obj;
	long long selfId=menu_->getTag(); 
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1401,(void *)selfId);
}

void TeamList::callBackExit( CCObject * obj )
{
	int teamWork=3;
	int playerId=0;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)teamWork,(void *)playerId);
}

void TeamList::callBackDismiss( CCObject * obj )
{
	CCDictionary *strings= CCDictionary::createWithContentsOfFile("res_ui/font/strings.xml");
	const char *str = ((CCString*)strings->objectForKey("friend_dismissSure"))->m_sString.c_str();
	char * applyTeam = const_cast<char*>(str);
	GameView::getInstance()->showPopupWindow(applyTeam,2,this,coco_selectselector(TeamList::callBackDismissSure),NULL);
}

void TeamList::callBackDismissSure( CCObject * obj )
{
	int teamWork=2;
	int playerId=0;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)teamWork,(void *)playerId);
}
