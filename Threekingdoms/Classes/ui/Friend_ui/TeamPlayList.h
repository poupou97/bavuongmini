#ifndef _UI_FRIEND_TEAMPLAYERLIST_H_
#define _UI_FRIEND_TEAMPLAYERLIST_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

class TeamPlayList:public UIScene
{
public:
	TeamPlayList(void);
	~TeamPlayList(void);

	static TeamPlayList *create();
	bool init();

	void onEnter();
	void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	void callBack(CCObject * obj);
};

#endif

