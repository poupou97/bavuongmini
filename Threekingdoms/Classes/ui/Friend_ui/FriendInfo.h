#ifndef _UI_FRIEND_FRIENDINFO_H_
#define _UI_FRIEND_FRIENDINFO_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
class FriendInfo:public UIScene
{
public:
	FriendInfo(void);
	~FriendInfo(void);

	static FriendInfo * create(int idx,int usedUiTag);
	bool init(int idx,int usedUiTag);

	void onEnter();
	void onExit();

	std::string getGeneralIconPath();

	void callBackWorkList(CCObject * obj);

	void callBackgetPhypower(CCObject * obj);

	void showMailFriendInfo(CCObject * obj);

	void callBackSendPhyPower(CCObject * obj);

	void callBackGetPhyPower(CCObject * obj);

	std::string getlastTime(long long time_);
	int phyPowerState_;
private:
	int selectCellIndex;	
	int uitag_;

	int playerId_;
	std::string playName_;
	int onLine_;
	int level_;
	int profession_;
	int vipLevel_;
	int countryId;
	int phyPowervalue_;
};

#endif

