#ifndef _UI_FRIEND_TEAMLIST_H_
#define _UI_FRIEND_TEAMLIST_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;
typedef enum
{
	TEAMLISTTABVIEW = 504,
};
class TeamList:public UIScene,public CCTableViewDataSource,public CCTableViewDelegate
{
public:
	TeamList(void);
	~TeamList(void);

	static TeamList * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::CCScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::CCScrollView* view);
	virtual void tableCellTouched(cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell);
	virtual cocos2d::CCSize tableCellSizeForIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual cocos2d::extension::CCTableViewCell* tableCellAtIndex(cocos2d::extension::CCTableView *table, unsigned int idx);
	virtual unsigned int numberOfCellsInTableView(cocos2d::extension::CCTableView *table);

	// default implements are used to call script callback if exist
	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
private:
	int selectId;
	void callBackDismiss(CCObject * obj);
	void callBackApply(CCObject * obj);
	void callBackExit(CCObject * obj);

	void callBackDismissSure(CCObject * obj);
};

#endif