#include "FriendGetPhyPower.h"
#include "../../messageclient/element/CPlayerGetPhypower.h"
#include "../../GameView.h"
#include "FriendInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "FriendInfoList.h"
#include "../../ui/GameUIConstant.h"


FriendGetPhyPower::FriendGetPhyPower(void)
{
	lastSelectCellId = 0;
	curVectorIndex = 0;
}


FriendGetPhyPower::~FriendGetPhyPower(void)
{
}

FriendGetPhyPower * FriendGetPhyPower::create(int count)
{
	FriendGetPhyPower * phyPower_ = new FriendGetPhyPower();
	if (phyPower_ && phyPower_->init(count))
	{
		phyPower_->autorelease();
		return phyPower_;
	}
	CC_SAFE_DELETE(phyPower_);
	return NULL;
}

bool FriendGetPhyPower::init(int count)
{
	if (UIScene::init())
	{
		winsize= CCDirector::sharedDirector()->getVisibleSize();
		m_getPhypowerCount = count;
		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winsize);
		mengban->setAnchorPoint(ccp(0,0));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);

		UIPanel *mainPanel=(UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/lingtili_1.json");
		mainPanel->setAnchorPoint(ccp(0.5f,0.5f));
		mainPanel->setPosition(ccp(winsize.width/2,winsize.height/2));
		m_pUiLayer->addWidget(mainPanel);

		setPhypowerTouchCellType(phypower_ktypeTouchCellEvent);

		remPhyPowerCount_ = (UILabelBMFont *)UIHelper::seekWidgetByName(mainPanel,"LabelBMFont_PhyPowerCount");
		char string_count[10];
		sprintf(string_count,"%d",count);
		remPhyPowerCount_->setText(string_count);

		labelBmFont_phypowerNull = (UILabelBMFont *)UIHelper::seekWidgetByName(mainPanel,"LabelBMFont_notphyPowerCount");
		labelBmFont_phypowerNull->setVisible(false);

		phypower_layer= UILayer::create();
		phypower_layer->ignoreAnchorPointForPosition(false);
		phypower_layer->setAnchorPoint(ccp(0.5f,0.5f));
		phypower_layer->setPosition(ccp(winsize.width/2,winsize.height/2));
		phypower_layer->setContentSize(CCSizeMake(800,480));
		this->addChild(phypower_layer,10);


		UIButton * btn_close = (UIButton *)UIHelper::seekWidgetByName(mainPanel,"Button_close");
		btn_close->setTouchEnable(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addReleaseEvent(this,coco_releaseselector(FriendGetPhyPower::callBackClose));

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void FriendGetPhyPower::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	phypowerTavleView = CCTableView::create(this, CCSizeMake(682, 315));
	phypowerTavleView->setDirection(kCCScrollViewDirectionVertical);
	phypowerTavleView->setAnchorPoint(ccp(0,0));
	phypowerTavleView->setPosition(ccp(60,100));
	phypowerTavleView->setDelegate(this);
	phypowerTavleView->setVerticalFillOrder(kCCTableViewFillTopDown);
	phypower_layer->addChild(phypowerTavleView);
}

void FriendGetPhyPower::onExit()
{
	UIScene::onExit();
}

void FriendGetPhyPower::scrollViewDidScroll( cocos2d::extension::CCScrollView * view )
{

}

void FriendGetPhyPower::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}

void FriendGetPhyPower::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	int cellId = cell->getIdx();
	
	if (table->cellAtIndex(lastSelectCellId) != NULL)
	{
		FriendInfo * friendCell = (FriendInfo *)table->cellAtIndex(lastSelectCellId)->getChildByTag(lastSelectCellTag);
		if (friendCell != NULL)
		{
			if (friendCell->getChildByTag(1313) != NULL)
			{
				friendCell->getChildByTag(1313)->removeFromParent();
			}
		}
	}

	FriendInfo * curPhyower_ = (FriendInfo *)table->cellAtIndex(cellId)->getChildByTag(curSelectCellTag);
	if (curPhyower_ == NULL)
	{
		return;
	}
	CCScale9Sprite* pHighlightSpr = CCScale9Sprite::create(CCRectMake(15, 30, 1, 1) , "res_ui/highlight.png");
	pHighlightSpr->setPreferredSize(CCSize(322,80));
	pHighlightSpr->setAnchorPoint(CCPointZero);
	pHighlightSpr->setTag(1313);
	pHighlightSpr->setPosition(ccp(18,1));
	curPhyower_->addChild(pHighlightSpr,20);

	if (getPhypowerTouchCellType() == phypower_ktypeTouchCellEvent)
	{
		FriendInfoList * infolist = FriendInfoList::create(7);
		infolist->ignoreAnchorPointForPosition(false);
		infolist->setAnchorPoint(ccp(0,0.5f));
		infolist->setZOrder(10);
		infolist->setPosition(ccp(100,infolist->getContentSize().height+50));
		phypower_layer->addChild(infolist);
	}
	
	setPhypowerTouchCellType(phypower_ktypeTouchCellEvent);
	lastSelectCellTag = curSelectCellTag;
	lastSelectCellId =cell->getIdx();
}

cocos2d::CCSize FriendGetPhyPower::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	return CCSizeMake(671,80);
}

cocos2d::extension::CCTableViewCell* FriendGetPhyPower::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell =table->dequeueCell();   // this method must be called

	cell=new CCTableViewCell();
	cell->autorelease();

	int phypowerSize = GameView::getInstance()->playerPhypowervector.size();
	
	if (idx == phypowerSize/2)
	{
		if (phypowerSize%2 == 0)
		{
			FriendInfo * friendInfoL =FriendInfo::create(2*idx,kTagGetphyPowerUi);
			friendInfoL->setAnchorPoint(ccp(0.5f,0.5f));
			friendInfoL->setPosition(ccp(0,0));
			friendInfoL->setTag(phypowerCellLeft);
			cell->addChild(friendInfoL);

			FriendInfo * friendInfoR =FriendInfo::create(2*idx+1,kTagGetphyPowerUi);
			friendInfoR->setAnchorPoint(ccp(0.5f,0.5f));
			friendInfoR->setPosition(ccp(325,0));
			friendInfoR->setTag(phypowerCellRight);
			cell->addChild(friendInfoR);
		}else
		{
			FriendInfo * friendInfoL =FriendInfo::create(2*idx,kTagGetphyPowerUi);
			friendInfoL->setAnchorPoint(ccp(0.5f,0.5f));
			friendInfoL->setPosition(ccp(0,0));
			friendInfoL->setTag(phypowerCellLeft);
			cell->addChild(friendInfoL);
		}
	}
	else
	{
		FriendInfo * friendInfoL =FriendInfo::create(2*idx,kTagGetphyPowerUi);
		friendInfoL->setAnchorPoint(ccp(0.5f,0.5f));
		friendInfoL->setPosition(ccp(0,0));
		friendInfoL->setTag(phypowerCellLeft);
		cell->addChild(friendInfoL);

		FriendInfo * friendInfoR =FriendInfo::create(2*idx+1,kTagGetphyPowerUi);
		friendInfoR->setAnchorPoint(ccp(0.5f,0.5f));
		friendInfoR->setPosition(ccp(325,0));
		friendInfoR->setTag(phypowerCellRight);
		cell->addChild(friendInfoR);
	}
	return cell;
}

unsigned int FriendGetPhyPower::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	int vectorSize =  GameView::getInstance()->playerPhypowervector.size();

	if (vectorSize%2 == 0)
	{
		return vectorSize/2;
	}else
	{
		return vectorSize/2 + 1;
	}
}

bool FriendGetPhyPower::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return resignFirstResponder(pTouch,this,false);
}

void FriendGetPhyPower::callBackClose( CCObject * obj )
{
	this->closeAnim();
}

void FriendGetPhyPower::setPhypowerTouchCellType( int type_ )
{
	m_curTouchType = type_;
}

int FriendGetPhyPower::getPhypowerTouchCellType()
{
	return m_curTouchType;
}

void FriendGetPhyPower::setShowPhypowerlabel( bool isShow )
{
	labelBmFont_phypowerNull->setVisible(isShow);
}

