#ifndef _UI_FRIEND_FRIENDUI_H_
#define _UI_FRIEND_FRIENDUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class TeamList;
class UITab;
class RichTextInputBox;
class AroundPlayerTabview;
class FriendWorkList;
typedef enum
{
	FRIENDWORKLISTTAG = 501,
	TEAMLISTTAG=502,
	FRIENDADDLAYER=520,
	TEAMLISTLAYERTAG=521,
	UILAYEROFTEAMTAG = 550,
};

enum
{
	FRIENDLIST=0,
	BACKLIST=1,
	ENEMYLIST=2,
	TEAMPLAYER=3,
	AROUNDPLAYER=4,
	CHECKTABLIST_ONEKEY = 5,
	CHECKTABLIST_CHECKBOX = 6,
};

enum
{
	ktypeTouchCellEvent = 0,
	ktypeTouchCellOfMenuEvent,
};

class FriendUi:public UIScene
{
public:
	FriendUi(void);
	~FriendUi(void);

	static FriendUi *create();
	bool init();
	void onEnter();
	void onExit();

	void callBackChangeFriendType(CCObject * obj);
	void callBackFriendButton(CCObject * obj);
	void callBackChackButton(CCObject * obj);
	void callBackKeyPersonalButton(CCObject * obj);

	void callBackPopCounterLV1(CCObject * obj);
	void callBackPopCounterLV2(CCObject * obj);
	void callBackLabelLV1(CCObject * obj);
	void callBackLabelLV2(CCObject * obj);

	void callBackPhypower(CCObject * obj);

	void callBackExit(CCObject * obj);

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);

	void reloadSourceData(int playerNum);
	void showfriendOfLine(CCObject * obj);
	UILabel * labelFriendType;
	UILabel * label_Friendnum;
	UILabel * labclCheckResult;
	UILayer * layer_;
	FriendWorkList* friendWorkList;
	CCSize winsize;

	UITab* friendType;
	UIButton * btn_phypower;
	int totalPlayerNum_;//total Player num
public:
	void setTouchCellType(int type_);
	int getTouchCellType();
	int m_touchType;
public:
	//team
	TeamList * teamList;
	void showTeamPlayer(int idx);
	void selectAcceptTeam(CCObject * obj,CheckBoxEventType type);
	void selectRefuseTeam(CCObject * obj,CheckBoxEventType type);

	void callToTogether(CCObject * obj);
	void autoApplyTeam(CCObject * obj);
private:
	UITab* friendWork;

	UIPanel *friendPanel;
	UIPanel *palPanel; 
	UIPanel *checkPanel;
	UIPanel *checkResultPanel;
	
	UIPanel *panelTeam;
	UIPanel *panelOtherplayer;
	UILabel * image_friendNum;
	UICheckBox * CheckBox_pro_HaoJie;
	UICheckBox * CheckBox_pro_MengJiang;
	UICheckBox * CheckBox_pro_GuiMou;
	UICheckBox * CheckBox_pro_ShenShe;

	UICheckBox * checkBox_country_yi;
	UICheckBox * checkBox_country_yang;
	UICheckBox * checkBox_country_jing;
	//UICheckBox * checkBox_country_you;
	//UICheckBox * checkBox_country_liang;

	void refreshCheckCountryAndPression(bool isSelect);

	UICheckBox *isShowFriendCheckbox;//好友panel 是否只显示在线
	//UILabel * showFriendLabel;//label 是否只显示在线好友

	UIButton *button_backCheck;//返回 checkpanel
	UIButton *button_addFriend;//一键添加

	UICheckBox * CheckBox_accept;//接受 拒绝
	UICheckBox * CheckBox_refuse;

	UILabel * label_lv1;
	UILabel * label_lv2;

	RichTextInputBox *nameBox;
public:
	void callBackButton_backCheck(CCObject * obj);
	void callbackButton_addFriend(CCObject * obj);


public:
	int operation_;
	int relationtype_;
	long long playerId_;
	std::string playerName_;

	std::vector<int>professionVector;
	std::vector<int>countryVector;
	std::string nameString_;//name check string
	int sexSelect;
	int professionSelect;
	int minLvSelect;
	int maxLvSelect;
	int showOnlineSelect;
	int pageNumSelect;
	int pageIndexSelect;
	/////////
	int cellIdSelect_;
	int cellSelectTag;
	int playerTeamId;
public:
	int curType;
	void selectWorkFriend(CCObject * obj);
	//void teamPlayerList(CCObject * obj);
	AroundPlayerTabview * aroundplayer;
public:
	int selectIndex;
	int fuctionValue;
	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};

	struct FriendList
	{
		int relationType;
		int pageNum;
		int pageIndex;
		int showOnline;
	};
};

#endif

