#include "TeamMemberList.h"
#include "../extensions/UITab.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../Chat_ui/ChatUI.h"
#include "../Chat_ui/AddPrivateUi.h"
#include "../extensions/RichTextInput.h"
#include "../../GameView.h"
#include "../../messageclient/element/CMapTeam.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CTeamMember.h"
#include "../extensions/QuiryUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Mail_ui/MailUI.h"
#include "../../gamescene_state/MainScene.h"


TeamMemberList::TeamMemberList(void)
{
}


TeamMemberList::~TeamMemberList(void)
{
	delete m_teammenber;
}

TeamMemberList * TeamMemberList::create(CTeamMember * teammenber,long long leaderid)
{
	TeamMemberList * list=new TeamMemberList();
	if (list && list->init(teammenber,leaderid))
	{
		list->autorelease();
		return list;
	}
	CC_SAFE_DELETE(list);
	return NULL;
}

bool TeamMemberList::init(CTeamMember * teammenber,long long leaderid)
{
	if (UIScene::init())
	{
		UIButton * playerBg=UIButton::create();
		playerBg->setScale9Enable(true);
		playerBg->setScale9Size(CCSizeMake(284,55));
		playerBg->setCapInsets(CCRect(15,30,1,1));
		playerBg->setTouchEnable(true);
		playerBg->setTextures("res_ui/kuang01_new.png","res_ui/kuang01_new.png","");
		playerBg->setAnchorPoint(ccp(0,0));
		playerBg->setPosition(ccp(0,0));
		playerBg->setTag(teammenber->roleid());
		playerBg->addReleaseEvent(this,coco_cancelselector(TeamMemberList::callBackUitab));
		m_pUiLayer->addWidget(playerBg);
		
		std::string teamName_=teammenber->name();
		int pressionid_=teammenber->profession();
		int level_=teammenber->level();
		long long playerId_ =teammenber->roleid();
		int vipLevel_ =  teammenber->viplevel();
		int isOnline_ = teammenber->isonline();
		//////icon name pression lv
		UIImageView * playerIconBg_ =UIImageView::create();
		playerIconBg_->setTexture("res_ui/LV4_all.png");
		playerIconBg_->setAnchorPoint(ccp(0.5f,0.5f));
		playerIconBg_->setPosition(ccp(40,26));
		playerIconBg_->setScale(0.8f);
		playerBg->addChild(playerIconBg_);

		std::string icon_path =  BasePlayer::getHeadPathByProfession(pressionid_);
		UIImageView * playerIcon_ =UIImageView::create();
		playerIcon_->setTexture(icon_path.c_str());
		playerIcon_->setAnchorPoint(ccp(0.5f,0.5f));
		playerIcon_->setPosition(ccp(-6,1));
		playerIcon_->setScale(0.8f);
		playerIconBg_->addChild(playerIcon_);

		char memberLevelStr[10];
		sprintf(memberLevelStr,"%d",level_);
		UILabel * label_memberLv_ =UILabel::create();
		label_memberLv_->setAnchorPoint(ccp(1,0));
		label_memberLv_->setPosition(ccp(playerIconBg_->getContentSize().width/2 - 5, - playerIconBg_->getContentSize().height/2));
		label_memberLv_->setText(memberLevelStr);
		label_memberLv_->setFontSize(14);
		playerIconBg_->addChild(label_memberLv_);

		if (playerId_ == leaderid)
		{
			UIImageView * playerIcon_ =UIImageView::create();
			playerIcon_->setTexture("res_ui/dui.png");
			playerIcon_->setAnchorPoint(ccp(0,0));
			playerIcon_->setPosition(ccp(3,30));
			playerBg->addChild(playerIcon_);
		}
		
		UILabel * label_teamName =UILabel::create();
		label_teamName->setAnchorPoint(ccp(0,0));
		label_teamName->setPosition(ccp(75,20));
		label_teamName->setText(teamName_.c_str());
		label_teamName->setFontSize(16);
		playerBg->addChild(label_teamName);

		if (vipLevel_ > 0)
		{
			UIWidget * vipWidge = MainScene::addVipInfoByLevelForWidget(vipLevel_);
			playerBg->addChild(vipWidge);
			vipWidge->setPosition(ccp(label_teamName->getPosition().x+ label_teamName->getContentSize().width + vipWidge->getContentSize().width/2,
										label_teamName->getPosition().y + vipWidge->getContentSize().height/2));
		}

		std::string pressionName_ = BasePlayer::getProfessionNameIdxByIndex(pressionid_);
		UILabel * label_pression =UILabel::create();
		label_pression->setAnchorPoint(ccp(0,0));
		label_pression->setPosition(ccp(180,20));
		label_pression->setText(pressionName_.c_str());
		label_pression->setFontSize(16);
		playerBg->addChild(label_pression);
		/*
		char string_lv[5];
		sprintf(string_lv,"%d",level_);
		std::string playerlevelStr_ = "LV";
		playerlevelStr_.append(string_lv);
		*/
		std::string onlineString_ = "";
		if (isOnline_ == 1)
		{
			const char *  onLine_ = StringDataManager::getString("Arena_roleStype_online");
			onlineString_.append(onLine_);
		}else
		{
			const char *  onLine_ = StringDataManager::getString("Arena_roleStype_notOnline");
			onlineString_.append(onLine_);

			UIImageView * onLineSpIcon_ =UIImageView::create();
			onLineSpIcon_->setTexture("res_ui/haoyouzhezhao.png");
			onLineSpIcon_->setScale9Enable(true);
			onLineSpIcon_->setScale9Size(CCSizeMake(284,55));
			onLineSpIcon_->setCapInsets(CCRect(10,10,1,1));
			onLineSpIcon_->setAnchorPoint(ccp(0,0));
			onLineSpIcon_->setPosition(ccp(0,0));
			playerBg->addChild(onLineSpIcon_);
			onLineSpIcon_->setZOrder(10);
		}
		
		UILabel * label_onLine =UILabel::create();
		label_onLine->setAnchorPoint(ccp(0,0));
		label_onLine->setPosition(ccp(230,20));
		label_onLine->setText(onlineString_.c_str());
		label_onLine->setFontSize(16);
		playerBg->addChild(label_onLine);
		
		m_teammenber=new CTeamMember();
		m_teammenber->CopyFrom(*teammenber);
		m_leaderid = leaderid;
		
		return true;
	}
	return false;
}

void TeamMemberList::onEnter()
{
	UIScene::onEnter();
}

void TeamMemberList::onExit()
{
	UIScene::onExit();
	
}

void TeamMemberList::callBackUitab( CCObject * obj )
{
	//if touched is self return false
	UIButton * button_ =(UIButton *)obj;
	int numIdx_ =button_->getTag();

	long long selfId =GameView::getInstance()->myplayer->getRoleId();
	if (selfId != numIdx_)
	{
		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		TeamMemberListOperator * listOperator = TeamMemberListOperator::create(m_teammenber,m_leaderid);
		listOperator->ignoreAnchorPointForPosition(false);
		listOperator->setAnchorPoint(ccp(0.5f,0.5f));
		//listOperator->setPosition(ccp(0,0));
		GameView::getInstance()->getMainUIScene()->addChild(listOperator);
		listOperator->setPosition(ccp(winsize.width/2- 20,winsize.height/2+listOperator->getContentSize().height));
		/*
		if (selfId == m_leaderid)
		{
			//listOperator->setPosition(ccp(winsize.width/2,550));
			listOperator->setPosition(ccp(winsize.width/2- 50,winsize.height/2+listOperator->getContentSize().height));
		}else
		{
			listOperator->setPosition(ccp(435,395));
		}
		*/
	}
}

bool TeamMemberList::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	CCPoint location = pTouch->getLocation();

	CCPoint pos = this->getPosition();
	CCSize size = this->getContentSize();
	CCPoint anchorPoint = this->getAnchorPointInPoints();
	pos = ccpSub(pos, anchorPoint);
	CCRect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}
	
	return false;
}

void TeamMemberList::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void TeamMemberList::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void TeamMemberList::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}



///////////////////////////////////
TeamMemberListOperator::TeamMemberListOperator( void )
{

}

TeamMemberListOperator::~TeamMemberListOperator( void )
{
	delete mm_teammenber;
}

TeamMemberListOperator * TeamMemberListOperator::create( CTeamMember * teammenber,long long leaderid )
{
	TeamMemberListOperator * listOPerator =new TeamMemberListOperator();
	if (listOPerator && listOPerator->init(teammenber,leaderid))
	{
		listOPerator->autorelease();
		return listOPerator;
	}
	CC_SAFE_DELETE(listOPerator);
	return NULL;
}

bool TeamMemberListOperator::init( CTeamMember * teammenber,long long leaderid )
{
	if (UIScene::init())
	{
		winSize=CCDirector::sharedDirector()->getVisibleSize();

		const char *strings_addfriend = StringDataManager::getString("friend_addfriend");
		char *addfriend_left=const_cast<char*>(strings_addfriend);

		const char *strings_delete = StringDataManager::getString("friend_delete");
		char *delete_left=const_cast<char*>(strings_delete);

		const char *strings_check = StringDataManager::getString("friend_check");
		char *check_left=const_cast<char*>(strings_check);

		const char *strings_team = StringDataManager::getString("friend_team");
		char *team_left=const_cast<char*>(strings_team);

		const char *strings_mail = StringDataManager::getString("friend_mail");
		char *mail_left=const_cast<char*>(strings_mail);

		const char *strings_black = StringDataManager::getString("friend_black");
		char *black_left=const_cast<char*>(strings_black);

		const char *strings_private = StringDataManager::getString("chatui_private");
		char *private_left=const_cast<char*>(strings_private);

		const char *strings_enemy = StringDataManager::getString("friend_enemy");
		char *enemy_left=const_cast<char*>(strings_enemy);

		const char *strings_track = StringDataManager::getString("friend_track");
		char *track_left=const_cast<char*>(strings_track);

		const char *strings_kickout = StringDataManager::getString("team_leaderKick");
		char *kickout_left=const_cast<char*>(strings_kickout);

		const char *strings_leaderout = StringDataManager::getString("team_leaderOut");
		char *leaderout_left=const_cast<char*>(strings_leaderout);

		long long selfid= GameView::getInstance()->myplayer->getRoleId();	
		if (selfid == leaderid)
		{
			const char * Chaneel_normalImage="res_ui/new_button_5.png";
			const char * Channel_selectImage = "res_ui/new_button_5.png";
			const char * Channel_finalImage = "";
			const char * Chaneel_highLightImage="res_ui/new_button_5.png";

			char * label[] = {private_left,check_left,leaderout_left,kickout_left,mail_left,addfriend_left,black_left};
			UITab* friendTab=UITab::createWithText(7,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
			friendTab->setAnchorPoint(ccp(0,0));
			friendTab->setPosition(ccp(0,0));
			friendTab->setHighLightImage((char *) Chaneel_highLightImage);
			friendTab->setDefaultPanelByIndex(0);
			friendTab->setAutoClose(true);
			friendTab->addIndexChangedEvent(this,coco_indexchangedselector(TeamMemberListOperator::callBack1));
			friendTab->setPressedActionEnabled(true);
			m_pUiLayer->addWidget(friendTab);

			setContentSize(CCSizeMake(103,280));
		}else
		{
			const char * Chaneel_normalImage="res_ui/new_button_5.png";
			const char * Channel_selectImage = "res_ui/new_button_5.png";
			const char * Channel_finalImage = "";
			const char * Chaneel_highLightImage="res_ui/new_button_5.png";

			char * label[] = {private_left,check_left,mail_left,addfriend_left,black_left};
			UITab* friendTab=UITab::createWithText(5,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
			friendTab->setAnchorPoint(ccp(0,0));
			friendTab->setPosition(ccp(0,0));
			friendTab->setHighLightImage((char *) Chaneel_highLightImage);
			friendTab->setDefaultPanelByIndex(0);
			friendTab->setAutoClose(true);
			friendTab->addIndexChangedEvent(this,coco_indexchangedselector(TeamMemberListOperator::callBack2));
			friendTab->setPressedActionEnabled(true);
			m_pUiLayer->addWidget(friendTab);

			setContentSize(CCSizeMake(103,200));
		}
		mm_teammenber =new CTeamMember();
		mm_teammenber->CopyFrom(*teammenber);

		setTouchEnabled(true);
		setTouchMode(kCCTouchesOneByOne);
		return true;
	}
	return false;
}

void TeamMemberListOperator::onEnter()
{
	UIScene::onEnter();
}

void TeamMemberListOperator::onExit()
{
	UIScene::onExit();
}

bool TeamMemberListOperator::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return this->resignFirstResponder(pTouch,this,false,false);
}

void TeamMemberListOperator::callBack1( CCObject * obj )
{
	int num= ((UITab *) obj)->getCurrentIndex();
	long long memberid = mm_teammenber->roleid();
	std::string memberName = mm_teammenber->name();
	removeFromParentAndCleanup(true);
	switch(num)
	{
	case 0:
		{
			long long id_ = mm_teammenber->roleid();
			std::string name_ = mm_teammenber->name();
			int countryId = 0;
			int vipLv = mm_teammenber->viplevel();
			int lv = mm_teammenber->level();
			int pressionId = mm_teammenber->profession();

			MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(id_,name_,countryId,vipLv,lv,pressionId);

			/*
			ChatUI * chatui_ = ChatUI::create();
			chatui_->ignoreAnchorPointForPosition(false);
			chatui_->setAnchorPoint(ccp(0.5f,0.5f));
			chatui_->setPosition(ccp(winSize.width/2,winSize.height/2));
			chatui_->setTag(kTabChat);
			GameView::getInstance()->getMainUIScene()->addChild(chatui_);

			chatui_->selectChannelId =0;
			const char *str_private = StringDataManager::getString("chatui_private");
			char *private_left=const_cast<char*>(str_private);
			chatui_->labelchanel->setText(private_left);
			chatui_->channelLabel->setDefaultPanelByIndex(6);
			chatui_->addPrivate();


			std::string friendName_= memberName;
			AddPrivateUi * privateui=(AddPrivateUi *)chatui_->layer_3->getChildByTag(PRIVATEUI);
			privateui->textBox_private->onTextFieldInsertText(NULL,friendName_.c_str(),30);
			*/
		}break;
	case 1:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)memberid);
		}break;
	case 2:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)1,(void *)memberid);
		}break;
	case 3:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)0,(void *)memberid);
		}break;
	case 4:
		{
			MailUI * mail_ui=MailUI::create();
			mail_ui->ignoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(ccp(0.5f,0.5f));
			mail_ui->setPosition(ccp(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001, (void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			std::string friendName_= memberName;
			mail_ui->friendName->onTextFieldInsertText(NULL,friendName_.c_str(),30);
		}break;
	case 5:
		{
			FriendStruct friend1={0,0,memberid,memberName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	case 6:
		{
			FriendStruct friend1={0,1,memberid,memberName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	}	
}

void TeamMemberListOperator::callBack2( CCObject * obj )
{
	int num= ((UITab *) obj)->getCurrentIndex();
	long long memberid = mm_teammenber->roleid();
	std::string memberName = mm_teammenber->name();
	removeFromParentAndCleanup(true);
	switch(num)
	{
	case 0:
		{
			long long id_ = mm_teammenber->roleid();
			std::string name_ = mm_teammenber->name();
			int countryId = 0;
			int vipLv = mm_teammenber->viplevel();
			int lv = mm_teammenber->level();
			int pressionId = mm_teammenber->profession();

			MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(id_,name_,countryId,vipLv,lv,pressionId);



			/*
			ChatUI * chatui_ = ChatUI::create();
			chatui_->ignoreAnchorPointForPosition(false);
			chatui_->setAnchorPoint(ccp(0.5f,0.5f));
			chatui_->setPosition(ccp(winSize.width/2,winSize.height/2));
			chatui_->setTag(kTabChat);
			GameView::getInstance()->getMainUIScene()->addChild(chatui_);

			chatui_->selectChannelId =0;
			const char *str_private = StringDataManager::getString("chatui_private");
			char *private_left=const_cast<char*>(str_private);
			chatui_->labelchanel->setText(private_left);
			chatui_->channelLabel->setDefaultPanelByIndex(6);
			chatui_->addPrivate();


			std::string friendName_= memberName;
			AddPrivateUi * privateui=(AddPrivateUi *)chatui_->layer_3->getChildByTag(PRIVATEUI);
			privateui->textBox_private->onTextFieldInsertText(NULL,friendName_.c_str(),30);
			*/
		}break;
	case 1:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)memberid);
		}break;
	case 2:
		{
			MailUI * mail_ui=MailUI::create();
			mail_ui->ignoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(ccp(0.5f,0.5f));
			mail_ui->setPosition(ccp(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001, (void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			std::string friendName_= memberName;
			mail_ui->friendName->onTextFieldInsertText(NULL,friendName_.c_str(),30);
		}break;
	case 3:
		{
			FriendStruct friend1={0,0,memberid,memberName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	case 4:
		{
			FriendStruct friend1={0,1,memberid,memberName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	}	
}


