#include "RankDb.h"
#include <sqlite3.h>
#include "../../messageclient/element/CRoleMessage.h"
#include "../../GameView.h"
#include "../../messageclient/element/CHomeMessage.h"
#include "RankData.h"
#include "RankUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/MainScene.h"
#include "RankDataTableView.h"
#include "../../utils/StaticDataManager.h"


RankDb * RankDb::s_rankDb = NULL;

RankDb::RankDb(void)
{

}

RankDb::~RankDb(void)
{

}

RankDb * RankDb::instance()
{
	if (NULL == s_rankDb)
	{
		s_rankDb = new RankDb();
	}

	return s_rankDb;
}

long long RankDb::checkDbTimeFlag(std::string strTableName)
{
	//记录返回结果是否成功  
	int result; 

	const char * configFileName = "rank.db";

	//获取保存路径  + 保存文件名  
	std::string path = CCFileUtils::sharedFileUtils()->getWritablePath() + configFileName;

	//数据库对象  
	sqlite3 *pdb;

	result = sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result != SQLITE_OK)
	{
		CCLog("open database failed,  number%d",result); 
	}

	// 确保 每个 table 中都有 timeFlag 这个字段
	std::string str_query = "SELECT timeFlag FROM '"+ strTableName +"'";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, str_query.c_str(), strlen(str_query.c_str()), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			long long longTimeFlag = sqlite3_column_int64(statement, 0);

			sqlite3_finalize(statement);

			sqlite3_close(pdb); 

			return longTimeFlag;
		}
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 

	return 0;
}

void RankDb::insertData( std::string strTableName, std::vector<CRoleMessage *> vector_roleMessage)
{
	//记录返回结果是否成功  
	int result; 

	const char * configFileName = "rank.db";

	//获取保存路径  + 保存文件名  
	std::string path = CCFileUtils::sharedFileUtils()->getWritablePath() + configFileName;

	//数据库对象  
	sqlite3 *pdb;

	result = sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result != SQLITE_OK)
	{
		CCLog("open database failed,  number%d",result); 
	}

	int nVectorSize = vector_roleMessage.size();
	for (int i = 0; i < nVectorSize; i++)
	{
		int nRankType = vector_roleMessage.at(i)->get_rankType();
		long long longId = vector_roleMessage.at(i)->id();
		std::string strName = vector_roleMessage.at(i)->name();
		int nRank = vector_roleMessage.at(i)->index();
		int nProfession = vector_roleMessage.at(i)->profession();
		int nMount = vector_roleMessage.at(i)->mount();
		int nCountry = vector_roleMessage.at(i)->country();
		long long longTimeFlag = vector_roleMessage.at(i)->get_timeFlag();
		int nPageCur = vector_roleMessage.at(i)->get_pageCur();
		int nPageAll = vector_roleMessage.at(i)->get_pageAll();
		int nMyRank = vector_roleMessage.at(i)->get_myPlayerRank();
		long long longMyPlayerId = vector_roleMessage.at(i)->get_myPlayerId();
		long long longTimeRefreshFlag = vector_roleMessage.at(i)->get_timeRefreshFlag();
		std::string strMyPlayerName = vector_roleMessage.at(i)->get_myPlayerName();
		int vipLevel = vector_roleMessage.at(i)->viplevel();
		int nLevel = vector_roleMessage.at(i)->level();
		int nFlowerType = vector_roleMessage.at(i)->get_flowerType();

		char tmpChar_rankType[4];
		sprintf(tmpChar_rankType, "%d", nRankType);
		std::string str_rankType = tmpChar_rankType;

		char tmpChar_id[20];
		sprintf(tmpChar_id, "%lld", longId);
		std::string str_id = tmpChar_id;

		char tmpChar_rank[20];
		sprintf(tmpChar_rank, "%d", nRank);
		std::string str_rank = tmpChar_rank;

		char tmpChar_profession[20];
		sprintf(tmpChar_profession, "%d", nProfession);
		std::string str_profession = tmpChar_profession;

		char tmpChar_mount[20];
		sprintf(tmpChar_mount, "%d", nMount);
		std::string str_mount = tmpChar_mount;

		char tmpChar_country[10];
		sprintf(tmpChar_country, "%d", nCountry);
		std::string str_country = tmpChar_country;

		char tmpChar_timeFlag[20];
		sprintf(tmpChar_timeFlag, "%lld", longTimeFlag);
		std::string str_timeFlag = tmpChar_timeFlag;

		char tmpChar_pageCur[10];
		sprintf(tmpChar_pageCur, "%d", nPageCur);
		std::string str_pageCur = tmpChar_pageCur;

		char tmpChar_pageAll[10];
		sprintf(tmpChar_pageAll, "%d", nPageAll);
		std::string str_pageAll = tmpChar_pageAll;

		char tmpChar_myPlayerRank[20];
		sprintf(tmpChar_myPlayerRank, "%d", nMyRank);
		std::string str_myPlayerRank = tmpChar_myPlayerRank;

		char tmpChar_myPlayerId[20];
		sprintf(tmpChar_myPlayerId, "%lld", longMyPlayerId);
		std::string str_myPlayerId = tmpChar_myPlayerId;

		char tmpChar_longTimeRefreshFlag[20];
		sprintf(tmpChar_longTimeRefreshFlag, "%lld", longTimeRefreshFlag);
		std::string str_longTimeRefreshFlag = tmpChar_longTimeRefreshFlag;

		char tmpChar_vipLevel[20];
		sprintf(tmpChar_vipLevel, "%d", vipLevel);
		std::string str_vipLevel = tmpChar_vipLevel;

		char tmpChar_level[20];
		sprintf(tmpChar_level, "%d", nLevel);
		std::string str_level = tmpChar_level;

		char tmpChar_flowerType[20];
		sprintf(tmpChar_flowerType, "%d", nFlowerType);
		std::string str_flowerType = tmpChar_flowerType;

		std::string str_query = "INSERT INTO "+ strTableName +" VALUES('"+ str_rankType +"','"+ str_id +"','"+ strName +"', '"+ str_rank +"','"+ str_profession +"','"+ str_mount +"','"+ str_country +"','"+ str_timeFlag +"','"+ str_pageCur +"','"+ str_pageAll +"','"+ str_myPlayerRank +"','"+ str_myPlayerId +"','"+ str_longTimeRefreshFlag +"','"+ strMyPlayerName +"','"+ str_vipLevel+"','"+ str_level +"','"+ str_flowerType + "')";
		char * zErrMsg = 0;
		result = sqlite3_exec(pdb, str_query.c_str(), NULL, NULL, &zErrMsg);
		if (result == SQLITE_OK)
		{

		}
		else
		{
			//GameView::getInstance()->showAlertDialog(zErrMsg);
		}
	}

	sqlite3_close(pdb); 
}

void RankDb::insertData( std::string strTableName, std::vector<CHomeMessage *> vector_familyMessage )
{
	//记录返回结果是否成功  
	int result; 

	const char * configFileName = "rank.db";

	//获取保存路径  + 保存文件名  
	std::string path = CCFileUtils::sharedFileUtils()->getWritablePath() + configFileName;

	//数据库对象  
	sqlite3 *pdb;

	result = sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result != SQLITE_OK)
	{
		CCLog("open database failed,  number%d",result); 
	}

	int nVectorSize = vector_familyMessage.size();
	for (int i = 0; i < nVectorSize; i++)
	{
		long long longId = vector_familyMessage.at(i)->id();
		int nRank = vector_familyMessage.at(i)->index();
		std::string strFamilyName = vector_familyMessage.at(i)->name();
		int nLevel = vector_familyMessage.at(i)->level();
		int nFamilyMember = vector_familyMessage.at(i)->number();
		int nFamilyMemberMax = vector_familyMessage.at(i)->numberall();
		int nCountry = vector_familyMessage.at(i)->country();
		long long longTimeFlag = vector_familyMessage.at(i)->get_timeFlag();
		int nPageCur = vector_familyMessage.at(i)->get_pageCur();
		int nPageAll = vector_familyMessage.at(i)->get_pageAll();
		int nMyRank = vector_familyMessage.at(i)->get_myRank();
		std::string strCreaterName = vector_familyMessage.at(i)->creatername();
		std::string strAnnouncement = vector_familyMessage.at(i)->announcement();
		long long longTimeRefreshFlag = vector_familyMessage.at(i)->get_timeRefreshFlag();
		

		char tmpChar_id[20];
		sprintf(tmpChar_id, "%lld", longId);
		std::string str_id = tmpChar_id;

		char tmpChar_rank[20];
		sprintf(tmpChar_rank, "%d", nRank);
		std::string str_rank = tmpChar_rank;

		char tmpChar_levle[10];
		sprintf(tmpChar_levle, "%d", nLevel);
		std::string str_level = tmpChar_levle;

		char tmpChar_familyMember[10];
		sprintf(tmpChar_familyMember, "%d", nFamilyMember);
		std::string str_familyMember = tmpChar_familyMember;

		char tmpChar_familyMemberMax[10];
		sprintf(tmpChar_familyMemberMax, "%d", nFamilyMemberMax);
		std::string str_familyMemberMax = tmpChar_familyMemberMax;

		char tmpChar_country[10];
		sprintf(tmpChar_country, "%d", nCountry);
		std::string str_country = tmpChar_country;

		char tmpChar_timeFlag[20];
		sprintf(tmpChar_timeFlag, "%lld", longTimeFlag);
		std::string str_timeFlag = tmpChar_timeFlag;

		char tmpChar_pageCur[10];
		sprintf(tmpChar_pageCur, "%d", nPageCur);
		std::string str_pageCur = tmpChar_pageCur;

		char tmpChar_pageAll[20];
		sprintf(tmpChar_pageAll, "%d", nPageAll);
		std::string str_pageAll = tmpChar_pageAll;

		char tmpChar_myRank[20];
		sprintf(tmpChar_myRank, "%d", nMyRank);
		std::string str_myRank = tmpChar_myRank;

		char tmpChar_timeRefreshFlag[20];
		sprintf(tmpChar_timeRefreshFlag, "lld", longTimeRefreshFlag);
		std::string str_longTimeRefreshFlag = tmpChar_timeRefreshFlag;

		std::string str_query = "INSERT INTO "+ strTableName +" VALUES('"+ str_id +"','"+ str_rank +"','"+ strFamilyName +"','"+ str_level +"','"+ str_familyMember +"','"+ str_familyMemberMax +"','"+ str_country +"','"+ str_timeFlag +"','"+ str_pageCur +"','"+ str_pageAll +"','"+ str_myRank +"','"+ strCreaterName +"','"+ strAnnouncement +"','"+ str_longTimeRefreshFlag +"')";
		char * zErrMsg = 0;
		result = sqlite3_exec(pdb, str_query.c_str(), NULL, NULL, &zErrMsg);
		if (SQLITE_OK == result)
		{
			
		}
		else
		{
			//GameView::getInstance()->showAlertDialog(zErrMsg);
		}
	}
	
	sqlite3_close(pdb);
}

void RankDb::deleteTableData( std::string strTableName )
{
	//记录返回结果是否成功  
	int result; 

	const char * configFileName = "rank.db";

	//获取保存路径  + 保存文件名  
	std::string path = CCFileUtils::sharedFileUtils()->getWritablePath() + configFileName;

	//数据库对象  
	sqlite3 *pdb;

	result = sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result != SQLITE_OK)
	{
		CCLog("open database failed,  number%d",result); 
	}

	std::string str_query = "DELETE FROM'"+ strTableName +"'";
	char * zErrMsg = 0;
	result = sqlite3_exec(pdb, str_query.c_str(), NULL, NULL, &zErrMsg);
	if (result == SQLITE_OK)
	{

	}
	else
	{
		//GameView::getInstance()->showAlertDialog(zErrMsg);
	}

	sqlite3_close(pdb); 
}

void RankDb::getDataFromDb(std::vector<CRoleMessage *> &vector_roleMessage, int nRankType, int nProfession)
{
	// 清空 vector_roleMessage 中的数据
	std::vector<CRoleMessage *>::iterator iter;
	for (iter = vector_roleMessage.begin(); iter != vector_roleMessage.end(); iter++)
	{
		delete *iter;
	}
	vector_roleMessage.clear();


	//记录返回结果是否成功  
	int result; 

	const char * configFileName = "rank.db";

	//获取保存路径  + 保存文件名  
	std::string path = CCFileUtils::sharedFileUtils()->getWritablePath() + configFileName;

	//数据库对象  
	sqlite3 *pdb;

	result = sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result != SQLITE_OK)
	{
		CCLog("open database failed,  number%d",result); 
	}

	char tmpChar_rankType[10];
	sprintf(tmpChar_rankType, "%d", nRankType);
	std::string str_rankType = tmpChar_rankType;

	char tmpChar_profession[10];
	sprintf(tmpChar_profession, "%d", nProfession);
	std::string str_profession = tmpChar_profession;

	//请求类别 0等级，1战斗力，2金币数，3战功, 4家族， 5鲜花

	// nRankType 为3 表示 类型为战功
	std::string str_query = "";
	if (0 == nProfession && 3 != nRankType)
	{
		str_query = "SELECT rank_type,id,name,rank,profession,mount,country,timeFlag,pageCur,pageAll,myPlayerRank,myPlayerId,timeRefreshFlag,myPlayerName,vipLevel,level,flowerType from t_rank_player_all WHERE rank_type='"+ str_rankType +"'";
	}
	else if (0 == nProfession && 3 == nRankType)
	{
		str_query = "SELECT rank_type,id,name,rank,profession,mount,country,timeFlag,pageCur,pageAll,myPlayerRank,myPlayerId,timeRefreshFlag,myPlayerName,vipLevel,level,flowerType from t_rank_country_all WHERE rank_type='"+ str_rankType +"'";
	}
	else if (5 == nRankType)
	{
		str_query = "SELECT rank_type,id,name,rank,profession,mount,country,timeFlag,pageCur,pageAll,myPlayerRank,myPlayerId,timeRefreshFlag,myPlayerName,vipLevel,level,flowerType from t_rank_player WHERE rank_type='" + str_rankType +"' AND flowerType='"+ str_profession + "'";
	}
	else if (3 == nRankType)
	{
		str_query = "SELECT rank_type,id,name,rank,profession,mount,country,timeFlag,pageCur,pageAll,myPlayerRank,myPlayerId,timeRefreshFlag,myPlayerName,vipLevel,level,flowerType from t_rank_player WHERE rank_type='"+ str_rankType +"'AND country='"+ str_profession +"'";
	}
	else
	{
		str_query = "SELECT rank_type,id,name,rank,profession,mount,country,timeFlag,pageCur,pageAll,myPlayerRank,myPlayerId,timeRefreshFlag,myPlayerName,vipLevel,level,flowerType from t_rank_player WHERE rank_type='"+ str_rankType +"'AND profession='"+ str_profession +"'";
	}
	
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, str_query.c_str(), strlen(str_query.c_str()), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int nRankType = sqlite3_column_int(statement, 0);
			long long longId = sqlite3_column_int64(statement, 1);
			char * pStrName = (char *)sqlite3_column_text(statement, 2);
			int nRank = sqlite3_column_int(statement, 3);
			int nProfession =sqlite3_column_int(statement, 4);
			int nMount = sqlite3_column_int(statement, 5);
			int nCountry = sqlite3_column_int(statement, 6);
			long long longTimeFlag = sqlite3_column_int64(statement, 7);
			int nPageCur = sqlite3_column_int(statement, 8);
			int nPageAll = sqlite3_column_int(statement, 9);
			int nMyPlayerRank = sqlite3_column_int(statement, 10);
			long long longMyPlayerId = sqlite3_column_int64(statement, 11);
			long long longTimeRefreshFlag = sqlite3_column_int64(statement, 12);
			char * pMyPlayerName = (char *)sqlite3_column_text(statement, 13);
			int vipLevel = sqlite3_column_int(statement, 14);
			int nLevel = sqlite3_column_int(statement, 15);
			int nFlowerType = sqlite3_column_int(statement, 16);

			CRoleMessage * roleMessage = new CRoleMessage();
			roleMessage->set_rankType(nRankType);
			roleMessage->set_id(longId);
			roleMessage->set_name(pStrName);
			roleMessage->set_index(nRank);
			roleMessage->set_profession(nProfession);
			roleMessage->set_mount(nMount);
			roleMessage->set_country(nCountry);
			roleMessage->set_timeFlag(longTimeFlag);
			roleMessage->set_pageCur(nPageCur);
			roleMessage->set_pageAll(nPageAll);
			roleMessage->set_myPlayerRank(nMyPlayerRank);
			roleMessage->set_myPlayerId(longMyPlayerId);
			roleMessage->set_timeRefreshFlag(longTimeRefreshFlag);
			roleMessage->set_myPlayerName(pMyPlayerName);
			roleMessage->set_viplevel(vipLevel);
			roleMessage->set_level(nLevel);
			roleMessage->set_flowerType(nFlowerType);

			vector_roleMessage.push_back(roleMessage);
		}
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));

		//GameView::getInstance()->showAlertDialog("select is error");
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

void RankDb::getDataFromDb(std::vector<CHomeMessage *> &vector_familyMessage )
{
	// 清空 vector_familyMessage 中的数据
	std::vector<CHomeMessage *>::iterator iter;
	for (iter = vector_familyMessage.begin(); iter != vector_familyMessage.end(); iter++)
	{
		delete *iter;
	}
	vector_familyMessage.clear();

	// 记录返回结果是否成功
	int result;

	const char * configFileName = "rank.db";

	// 获取保存路径 + 保存文件名
	std::string path = CCFileUtils::sharedFileUtils()->getWritablePath() + configFileName;

	// 数据对象
	sqlite3 *pdb;

	result = sqlite3_open(path.c_str(), &pdb);
	CCLOG("db path name: %s", path.c_str());
	if (result != SQLITE_OK)
	{
		CCLOG("open database failed, number%d", result);
	}

	std::string str_query = "SELECT familyId,rank,familyName,level,familyMember,familyMemberMax,country,timeFlag,pageCur,pageAll,myRank,createrName,announcement,timeRefreshFlag from t_rank_family";

	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, str_query.c_str(), strlen(str_query.c_str()), &statement, NULL);
	if (SQLITE_OK == result)
	{
		while(SQLITE_ROW == sqlite3_step(statement))
		{
			long long longId = sqlite3_column_int64(statement, 0);
			int nRank = sqlite3_column_int(statement, 1);
			char * pFamilyName = (char *)sqlite3_column_text(statement, 2);
			int nLevel = sqlite3_column_int(statement, 3);
			int nFamilyMember = sqlite3_column_int(statement, 4);
			int nFamilyMemberMax = sqlite3_column_int(statement, 5);
			int nCountry = sqlite3_column_int(statement, 6);
			long long longTimeFlag = sqlite3_column_int64(statement, 7);
			int nPageCur = sqlite3_column_int(statement, 8);
			int nPageAll = sqlite3_column_int(statement, 9);
			int nMyRank = sqlite3_column_int(statement, 10);
			char * pCreaterName = (char *)sqlite3_column_text(statement, 11);
			char * pAnnouncement = (char *)sqlite3_column_text(statement, 12);
			long long longTimeRefreshFlag = sqlite3_column_int64(statement, 13);

			CHomeMessage * familyMessage = new CHomeMessage();
			familyMessage->set_id(longId);
			familyMessage->set_index(nRank);
			familyMessage->set_name(pFamilyName);
			familyMessage->set_level(nLevel);
			familyMessage->set_number(nFamilyMember);
			familyMessage->set_numberall(nFamilyMemberMax);
			familyMessage->set_country(nCountry);
			familyMessage->set_timeFlag(longTimeFlag);
			familyMessage->set_pageCur(nPageCur);
			familyMessage->set_pageAll(nPageAll);
			familyMessage->set_myRank(nMyRank);
			familyMessage->set_creatername(pCreaterName);
			familyMessage->set_announcement(pAnnouncement);
			familyMessage->set_timeRefreshFlag(longTimeRefreshFlag);

			vector_familyMessage.push_back(familyMessage);
		}
	}
	else
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));

		//GameView::getInstance()->showAlertDialog("select is error");
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

void RankDb::timeIsOut()
{
	// 删除所有的表
	RankDb::instance()->deleteTableData("t_rank_player_all");
	RankDb::instance()->deleteTableData("t_rank_country_all");
	RankDb::instance()->deleteTableData("t_rank_player");
	RankDb::instance()->deleteTableData("t_rank_family");

	// 删除dataTableView
	RankUI* pRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
	if (NULL != pRankUI)
	{
		if (NULL != pRankUI->getBaseUILayer()->getChildByTag(TAG_SCROLLVIEW))
		{
			 pRankUI->getBaseUILayer()->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
		}

		// 刷新数据提示信息
		const char *str_rankRefresh = StringDataManager::getString("rank_rankHasRefresh");
		char* pRankRefresh = const_cast<char*>(str_rankRefresh);
		GameView::getInstance()->showAlertDialog(pRankRefresh);
	}

	// 删除相关vector
	RankDataTableView::instance()->clearVectorRankPlayer();
	RankDataTableView::instance()->clearVectorRankFamily();

	RankData::instance()->clearVectorFamilyInternet();
	RankData::instance()->clearVectorFamilyDb();
	RankData::instance()->clearVectorPlayerInternet();
	RankData::instance()->clearVectorPlayerDb();


	// 更新用户当前所看页的数据（获取第0页数据）
	// 向服务器请求数据
	int nRankType = RankData::instance()->get_rankType();
	int nProfession = RankData::instance()->get_proOrCountry();
	
	// 更新当前timeFlag和timeRefreshFlag等于-1
	RankData::instance()->set_timeFlag(-1);
	RankData::instance()->set_timeRefreshFlag(-1);



	if (0 == nRankType || 1 == nRankType)
	{
		// 请求服务器-----发送请求，请求服务器第0页数据(当前玩家所看的页)
		RankDataStruct * rankDataStruct = new RankDataStruct();
		rankDataStruct->nType = nRankType;
		rankDataStruct->nWork = nProfession;
		rankDataStruct->nPage = 0;

		///////////////////////////////////////////////////////////////////////////
		/*	char str_page[10];
		sprintf(str_page, "%d", rankDataStruct->nPage);
		std::string str_show = "request pageNum:";
		str_show.append(str_page);

		GameView::getInstance()->showAlertDialog(str_show);*/
		///////////////////////////////////////////////////////////////////////////////

		GameMessageProcessor::sharedMsgProcessor()->sendReq(2300, (void *)rankDataStruct);
	}
	else if (2 == nRankType)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2301, (void *)0);
	}
}

long long RankDb::checkMyPlayerId( std::string strTableName )
{
	//记录返回结果是否成功  
	int result; 

	const char * configFileName = "rank.db";

	//获取保存路径  + 保存文件名  
	std::string path = CCFileUtils::sharedFileUtils()->getWritablePath() + configFileName;

	//数据库对象  
	sqlite3 *pdb;

	result = sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result != SQLITE_OK)
	{
		CCLog("open database failed,  number%d",result); 
	}

	// 确保 每个 table 中都有 timeFlag 这个字段
	std::string str_query = "SELECT myPlayerId FROM '"+ strTableName +"'";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, str_query.c_str(), strlen(str_query.c_str()), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			long long longMyPlayerId = sqlite3_column_int64(statement, 0);

			sqlite3_finalize(statement);

			sqlite3_close(pdb); 

			return longMyPlayerId;
		}
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 

	return 0;
}

long long RankDb::checkDbTimeRefreshFlag( std::string strTableName )
{
	//记录返回结果是否成功  
	int result; 

	const char * configFileName = "rank.db";

	//获取保存路径  + 保存文件名  
	std::string path = CCFileUtils::sharedFileUtils()->getWritablePath() + configFileName;

	//数据库对象  
	sqlite3 *pdb;

	result = sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result != SQLITE_OK)
	{
		CCLog("open database failed,  number%d",result); 
	}

	// 确保 每个 table 中都有 timeRefreshFlag 这个字段
	std::string str_query = "SELECT timeRefreshFlag FROM '"+ strTableName +"'";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, str_query.c_str(), strlen(str_query.c_str()), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			long long longTimeRefreshFlag = sqlite3_column_int64(statement, 0);

			sqlite3_finalize(statement);

			sqlite3_close(pdb); 

			return longTimeRefreshFlag;
		}
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 

	return 0;
}

std::string RankDb::checkMyPlayerName( std::string strTableName )
{
	std::string str_myPlayerName = "";

	//记录返回结果是否成功  
	int result; 

	const char * configFileName = "rank.db";

	//获取保存路径  + 保存文件名  
	std::string path = CCFileUtils::sharedFileUtils()->getWritablePath() + configFileName;

	//数据库对象  
	sqlite3 *pdb;

	result = sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result != SQLITE_OK)
	{
		CCLog("open database failed,  number%d",result); 
	}

	// 确保 每个 table 中都有 myPlayerName 这个字段
	std::string str_query = "SELECT myPlayerName FROM '"+ strTableName +"'";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, str_query.c_str(), strlen(str_query.c_str()), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			char * pMyPlayerName = (char *)sqlite3_column_text(statement, 0);
			str_myPlayerName = pMyPlayerName;

			sqlite3_finalize(statement);

			sqlite3_close(pdb); 

			return str_myPlayerName;
		}
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 

	return str_myPlayerName;
}














