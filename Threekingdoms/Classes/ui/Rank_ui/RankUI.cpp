#include "RankUI.h"
#include "SimpleAudioEngine.h"
#include "../../GameAudio.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../../AppMacros.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CRoleMessage.h"
#include "RankData.h"
#include "RankDataTableView.h"
#include "../../messageclient/element/CHomeMessage.h"
#include "RankDb.h"
#include "../../utils/GameUtils.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/family_ui/ApplyFamily.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../ui/extensions/ControlTree.h"

using namespace CocosDenshion;

UIPanel * RankUI::s_pPanel;

const CCSize CCSizeCellSize = CCSizeMake(128, 44);
const CCSize CCSizeTableView = CCSizeMake(135, 390);
const CCSize CCSizeSecondTree = CCSizeMake(130,38);

const CCPoint CCPointTableView = CCPointMake(77, 43);

const CCSize CCSizeDataTableViewGeneral = CCSizeMake(530, 288);
const CCPoint CCPointDataTableView = CCPointMake(222, 75);

const CCSize CCSizeDataTableViewFamily = CCSizeMake(530, 330);

const CCPoint CCPointProTypeTab = CCPointMake(208, 407);

/////////////////////////////////////////////////////////////////////////////////////////////

RankUI::RankUI(void)
	:m_base_layer(NULL)
	//,m_leftView(NULL)
	,m_dataView(NULL)
	,m_tab_pro(NULL)
	,m_panel_level(NULL)
	,m_panel_combat(NULL)
	,m_panel_coin(NULL)
	,m_panel_family(NULL)
	,m_panel_battleAch(NULL)
	,m_panel_flower(NULL)
	,m_label_myRankLabel(NULL)
	,m_label_myRankNum(NULL)
	,m_label_myRankInfo(NULL)
	,m_tab_country(NULL)
{

}


RankUI::~RankUI(void)
{
	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		m_dataView->removeFromParent();
	}

	RankDataTableView::instance()->clearVectorRankPlayer();
	RankDataTableView::instance()->clearVectorRankFamily();

	RankData::instance()->clearAllData();


}

RankUI* RankUI::create()
{
	RankUI * rankUI = new RankUI();
	if (rankUI && rankUI->init())
	{
		rankUI->autorelease();
		return rankUI;
	}
	CC_SAFE_DELETE(rankUI);
	return NULL;
}

bool RankUI::init()
{
	if (UIScene::init())
	{
		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

		m_base_layer = UILayer::create();
		m_base_layer->ignoreAnchorPointForPosition(false);
		m_base_layer->setAnchorPoint(ccp(0.5f, 0.5f));
		m_base_layer->setContentSize(CCSizeMake(800, 480));
		m_base_layer->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		this->addChild(m_base_layer);

		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winSize);
		mengban->setAnchorPoint(CCPointZero);
		mengban->setPosition(ccp(0, 0));
		m_pUiLayer->addWidget(mengban);

		if(LoadSceneLayer::rankUIPanel->getWidgetParent() != NULL)
		{
			LoadSceneLayer::rankUIPanel->removeFromParentAndCleanup(false);
		}

		s_pPanel = LoadSceneLayer::rankUIPanel;
		s_pPanel->setAnchorPoint(ccp(0.5f, 0.5f));
		s_pPanel->getValidNode()->setContentSize(CCSizeMake(800, 480));
		s_pPanel->setPosition(ccp(winSize.width / 2, winSize.height / 2));
		s_pPanel->setScale(1.0f);
		s_pPanel->setTouchEnable(true);
		m_pUiLayer->addWidget(s_pPanel);

		// 初始化稻穗(中国穗)
		const char * firstStr = StringDataManager::getString("UIName_rank_pai");
		const char * secondStr = StringDataManager::getString("UIName_rank_hang");
		const char * thirdStr = StringDataManager::getString("UIName_rank_bang");
		CCArmature * atmature = MainScene::createPendantAnm(firstStr, secondStr, thirdStr,"");
		atmature->setPosition(ccp(30,240));
		m_base_layer->addChild(atmature);

		// 初始化 vector_tableView
		const char *str1 = StringDataManager::getString("rank_level");
		char* p1 = const_cast<char*>(str1);
		m_vector_tableView.push_back(p1);

		const char *str2 = StringDataManager::getString("rank_capacity");
		char* p2 = const_cast<char*>(str2);
		m_vector_tableView.push_back(p2);

		const char *str3 = StringDataManager::getString("rank_coin");
		char* p3 = const_cast<char*>(str3);
		m_vector_tableView.push_back(p3);

		const char *str4 = StringDataManager::getString("rank_battleAchievement");
		char* p4 = const_cast<char*>(str4);
		m_vector_tableView.push_back(p4);

		const char *str5 = StringDataManager::getString("rank_family");
		char* p5 = const_cast<char*>(str5);
		m_vector_tableView.push_back(p5);

		const char *str6 = StringDataManager::getString("rank_flower");
		char* p6 = const_cast<char*>(str6);
		m_vector_tableView.push_back(p6);

		// 初始化UI
		initUI();

		// 取得时间戳
		initTimeFlag();

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void RankUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_OPEN, false);
}

void RankUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::sharedEngine()->playEffect(SCENE_CLOSE, false);
}

bool RankUI::ccTouchBegan( CCTouch *touch, CCEvent * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void RankUI::ccTouchEnded( CCTouch *touch, CCEvent * pEvent )
{

}

void RankUI::ccTouchCancelled( CCTouch *touch, CCEvent * pEvent )
{

}

void RankUI::ccTouchMoved( CCTouch *touch, CCEvent * pEvent )
{

}

void RankUI::scrollViewDidScroll( CCScrollView* view )
{

}

void RankUI::scrollViewDidZoom( CCScrollView* view )
{

}

void RankUI::tableCellTouched( CCTableView* table, CCTableViewCell* cell )
{
	RankData::instance()->set_rankType(cell->getIdx());

	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		RankDataTableView::instance()->clearVectorRankPlayer();
		RankDataTableView::instance()->clearVectorRankFamily();

		m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	// 每次点击左侧的item，需要重置所选item的index为default
	RankDataTableView::instance()->setSelectItemDefault();

	// 0:等级;1:战斗力；2:金币；3:战功；4：家族；5：鲜花
	switch (RankData::instance()->get_rankType())
	{
	case 0:
		{
			m_panel_level->setVisible(true);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_family->setVisible(false);
			m_panel_battleAch->setVisible(false);

			//m_tab_pro->setVisible(true);
			//m_tab_country->setVisible(false);

			//m_tab_pro->setDefaultPanelByIndex(0);
			RankData::instance()->set_proOrCountry(0);

			// 更新时间戳
			reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 1:
		{
			m_panel_combat->setVisible(true);
			m_panel_level->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_family->setVisible(false);
			m_panel_battleAch->setVisible(false);

			//m_tab_pro->setVisible(true);
			//m_tab_country->setVisible(false);

			//m_tab_pro->setDefaultPanelByIndex(0);
			RankData::instance()->set_proOrCountry(0);

			// 更新时间戳
			reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 2:
		{
			m_panel_coin->setVisible(true);
			m_panel_family->setVisible(false);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_battleAch->setVisible(false);

			//m_tab_pro->setVisible(false);
			//m_tab_country->setVisible(false);

			//m_tab_pro->setDefaultPanelByIndex(0);
			RankData::instance()->set_proOrCountry(0);

			// 更新时间戳
			reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 3:
		{
			m_panel_battleAch->setVisible(true);
			m_panel_family->setVisible(false);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);

			//m_tab_country->setVisible(true);
			//m_tab_pro->setVisible(false);

			//m_tab_country->setDefaultPanelByIndex(0);
			RankData::instance()->set_proOrCountry(0);

			// 更新时间戳
			reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 4:
		{
			m_panel_family->setVisible(true);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_battleAch->setVisible(false);

			//m_tab_pro->setVisible(false);
			//m_tab_country->setVisible(false);

			// 更新时间戳
			reloadTimeFlag();
		}
		break;
	}
}

cocos2d::CCSize RankUI::tableCellSizeForIndex( CCTableView *table, unsigned int idx )
{
	return CCSizeCellSize;
}

cocos2d::extension::CCTableViewCell* RankUI::tableCellAtIndex( CCTableView *table, unsigned int idx )
{
	CCString *string = CCString::createWithFormat("%d", idx);
	CCTableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new CCTableViewCell();
	cell->autorelease();

	if (idx < m_vector_tableView.size())
	{
		// 底图
		CCScale9Sprite * sprite_frame = CCScale9Sprite::create("res_ui/kuang0_new.png");
		sprite_frame->setContentSize(CCSizeMake(CCSizeCellSize.width - 10, CCSizeCellSize.height - 2));
		sprite_frame->setCapInsets(CCRectMake(15, 12, 5, 49));
		sprite_frame->setAnchorPoint(CCPointZero);
		sprite_frame->setPosition(ccp(5, 1));
		cell->addChild(sprite_frame);

		std::string str_rankName =	 m_vector_tableView.at(idx);
		CCLabelTTF * label_rankName = CCLabelTTF::create(str_rankName.c_str(), APP_FONT_NAME, 18, CCSizeMake(150, 0), kCCTextAlignmentCenter);
		label_rankName->setColor(ccc3(255, 255, 255));
		ccColor3B color = ccc3(0, 0, 0);
		label_rankName->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0f, color);
		label_rankName->setAnchorPoint(ccp(0.5f, 0.5f));
		label_rankName->setHorizontalAlignment(kCCTextAlignmentCenter);
		label_rankName->setPosition(ccp(sprite_frame->getContentSize().width / 2 + sprite_frame->getPositionX(), sprite_frame->getContentSize().height / 2 + sprite_frame->getPositionY()));

		cell->addChild(label_rankName);
	}

	return cell;
}

unsigned int RankUI::numberOfCellsInTableView( CCTableView *table )
{
	return m_vector_tableView.size();
}

void RankUI::initUI()
{
	// close btn
	UIButton* pBtnClose = (UIButton*)UIHelper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnable(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addReleaseEvent(this, coco_releaseselector(RankUI::callBackBtnClose));

	// ui panel 获取
	m_panel_level = (UIPanel *)UIHelper::seekWidgetByName(s_pPanel, "Panel_lv");
	m_panel_level->setVisible(true);

	m_panel_combat = (UIPanel *)UIHelper::seekWidgetByName(s_pPanel, "Panel_combat");
	m_panel_combat->setVisible(false);

	m_panel_coin = (UIPanel *)UIHelper::seekWidgetByName(s_pPanel, "Panel_coins");
	m_panel_coin->setVisible(false);

	m_panel_family = (UIPanel *)UIHelper::seekWidgetByName(s_pPanel, "Panel_family");
	m_panel_family->setVisible(false);

	m_panel_battleAch = (UIPanel *)UIHelper::seekWidgetByName(s_pPanel, "Panel_battleAch");
	m_panel_battleAch->setVisible(false);

	m_panel_flower = (UIPanel *)UIHelper::seekWidgetByName(s_pPanel, "Panel_flower");
	m_panel_flower->setVisible(false);

	// 我的排行label
	m_label_myRankLabel = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_myRankLabel");
	m_label_myRankLabel->setVisible(true);

	// 我的排行num
	m_label_myRankNum = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_myRankNum");
	m_label_myRankNum->setVisible(true);

	// 鲜花排行提示
	m_label_myRankInfo = (UILabel *)UIHelper::seekWidgetByName(s_pPanel, "Label_myRankInfo");
	m_label_myRankInfo->setVisible(false);

	// tab_pro
	const char * normalImage = "res_ui/tab_1_off.png";
	const char * selectImage = "res_ui/tab_1_on.png";
	const char * finalImage = "";
	const char * highLightImage = "res_ui/tab_1_on.png";

	const char *strAll = StringDataManager::getString("rank_all");
	char* pAll = const_cast<char*>(strAll);
	const char *strMengjiang  = StringDataManager::getString("rank_mengjiang");
	char* pMengjiang = const_cast<char*>(strMengjiang);
	const char *strHaojie  = StringDataManager::getString("rank_haojie");
	char* pHaojie = const_cast<char*>(strHaojie);
	const char *strShenshe  = StringDataManager::getString("rank_shenshe");
	char* pShenshe = const_cast<char*>(strShenshe);
	const char *strGuimou  = StringDataManager::getString("rank_guimou");
	char* pGuimou = const_cast<char*>(strGuimou);

	char* pHighLightImage = const_cast<char*>(highLightImage);

	char * mainNames[] ={pAll, pMengjiang, pHaojie, pShenshe, pGuimou};
	/*m_tab_pro = UITab::createWithBMFont(5, normalImage, selectImage, finalImage, mainNames,"res_ui/font/ziti_1.fnt", HORIZONTAL, 2);
	m_tab_pro->setAnchorPoint(ccp(0, 0));
	m_tab_pro->setPosition(CCPointProTypeTab);
	m_tab_pro->setHighLightImage(pHighLightImage);
	m_tab_pro->setDefaultPanelByIndex(0);
	m_tab_pro->addIndexChangedEvent(this, coco_indexchangedselector(RankUI::proTypeTabIndexChangedEvent));
	m_tab_pro->setPressedActionEnabled(true);*/
	//m_base_layer->addWidget(m_tab_pro);

	// tab_country
	const char *strWeiGuo = StringDataManager::getString("rank_weiguo");
	char* pWeiGuo = const_cast<char*>(strWeiGuo);
	const char *strShuGuo = StringDataManager::getString("rank_shuguo");
	char* pShuGuo = const_cast<char*>(strShuGuo);
	const char *strWuGuo = StringDataManager::getString("rank_wuguo");
	char* pWuGuo = const_cast<char*>(strWuGuo);

	char * countryNames[] ={pAll, pWeiGuo, pShuGuo, pWuGuo};
	/*m_tab_country = UITab::createWithBMFont(4, normalImage, selectImage, finalImage, countryNames,"res_ui/font/ziti_1.fnt", HORIZONTAL, 2);
	m_tab_country->setAnchorPoint(ccp(0, 0));
	m_tab_country->setPosition(CCPointProTypeTab);
	m_tab_country->setHighLightImage(pHighLightImage);
	m_tab_country->setDefaultPanelByIndex(0);
	m_tab_country->addIndexChangedEvent(this, coco_indexchangedselector(RankUI::countryTypeTabIndexChangedEvent));
	m_tab_country->setPressedActionEnabled(true);*/
	//m_base_layer->addWidget(m_tab_country);

	// tab_flower
	const char * strZuoRiShouHua = StringDataManager::getString("rank_zuorishouhua");
	char * pZuoRiShouHua = const_cast<char*>(strZuoRiShouHua);
	const char * strZuoRiSongHua = StringDataManager::getString("rank_zuorisonghua");
	char * pZuoRiSongHua = const_cast<char*>(strZuoRiSongHua);
	const char * strLiShiShouHua = StringDataManager::getString("rank_lishishouhua");
	char * pLiShiShouHua = const_cast<char*>(strLiShiShouHua);
	const char * strLiShiSongHua = StringDataManager::getString("rank_lishisonghua");
	char * pLiShiSongHua = const_cast<char*>(strLiShiSongHua);

	// 显示其中的一个tab
	//m_tab_pro->setVisible(true);
	//m_tab_country->setVisible(false);

	/*
	// 主tableView
	m_leftView = CCTableView::create(this, CCSizeTableView);
	m_leftView ->setSelectedEnable(true);   // 支持选中状态的显示
	m_leftView ->setSelectedScale9Texture("res_ui/highlight.png", CCRectMake(26, 24, 1, 7), ccp(0,0));   // 设置九宫格图片
	m_leftView->setDirection(kCCScrollViewDirectionVertical);
	m_leftView->setAnchorPoint(ccp(0, 0));
	m_leftView->setPosition(CCPointTableView);
	m_leftView->setDelegate(this);
	m_leftView->setVerticalFillOrder(kCCTableViewFillTopDown);
	m_leftView->setPressedActionEnabled(true);
	m_base_layer->addChild(m_leftView);
	*/
	// 更新tableView数据
	//m_leftView->reloadData();

	// 更新scrollViewData
	this->reloadDataTableView();


	//add scroll tree
	std::vector<TreeStructInfo *> tempTreeVector;
	for (int i = 0;i<m_vector_tableView.size();i++)
	{
		TreeStructInfo * treeInfo_ = new TreeStructInfo();
		treeInfo_->setTreeName(m_vector_tableView.at(i));

		const char *str_level = StringDataManager::getString("rank_level");
		if (strcmp(m_vector_tableView.at(i).c_str(),str_level)==0)
		{
			//set first treebranch is index
			treeInfo_->addSecondVector(pAll);
			treeInfo_->addSecondVector(pMengjiang);
			treeInfo_->addSecondVector(pHaojie);
			treeInfo_->addSecondVector(pShenshe);
			treeInfo_->addSecondVector(pGuimou);
		}

		const char *str_capcity = StringDataManager::getString("rank_capacity");
		if (strcmp(m_vector_tableView.at(i).c_str(),str_capcity)==0)
		{
			treeInfo_->addSecondVector(pAll);
			treeInfo_->addSecondVector(pMengjiang);
			treeInfo_->addSecondVector(pHaojie);
			treeInfo_->addSecondVector(pShenshe);
			treeInfo_->addSecondVector(pGuimou);
		}

		const char *str_coin = StringDataManager::getString("rank_coin");
		if (strcmp(m_vector_tableView.at(i).c_str(),str_coin)==0)
		{
			//branch tree is null
			treeInfo_->addSecondVector(pAll);
		}

		const char *str_battleAchievement = StringDataManager::getString("rank_battleAchievement");
		if (strcmp(m_vector_tableView.at(i).c_str(),str_battleAchievement)==0)
		{
			treeInfo_->addSecondVector(pAll);
			treeInfo_->addSecondVector(pWeiGuo);
			treeInfo_->addSecondVector(pShuGuo);
			treeInfo_->addSecondVector(pWuGuo);
		}

		const char *str_family = StringDataManager::getString("rank_family");
		if (strcmp(m_vector_tableView.at(i).c_str(),str_family)==0)
		{
			//branch tree is null
			treeInfo_->addSecondVector(pAll);
		}

		const char *str_flower = StringDataManager::getString("rank_flower");
		if (strcmp(m_vector_tableView.at(i).c_str(),str_flower)==0)
		{
			treeInfo_->addSecondVector(pZuoRiShouHua);
			treeInfo_->addSecondVector(pZuoRiSongHua);
			treeInfo_->addSecondVector(pLiShiShouHua);
			treeInfo_->addSecondVector(pLiShiSongHua);
		}

		tempTreeVector.push_back(treeInfo_);
	}
	 
	tree_ = ControlTree::create(tempTreeVector,CCSizeTableView,CCSizeCellSize,CCSizeSecondTree);
	tree_->setAnchorPoint(ccp(0.5f,0.5f));
	tree_->setPosition(ccp(61,43));
	//tree_->addcallBackTreeEvent(this,coco_selectselector(RankUI::callBackTreeEvent));
	tree_->addcallBackSecondTreeEvent(this,coco_selectselector(RankUI::callBackSecondTreeEvent));
	m_base_layer->addChild(tree_);

}

void RankUI::callBackBtnClose( CCObject * obj )
{
	this->closeAnim();
}

void RankUI::proTypeTabIndexChangedEvent( CCObject * pSender )
{
	//职业 0.全部、1.猛将、2.鬼谋、3.豪杰、4.神射、（协议定义）
	//职业 0.全部、1.猛将、2.豪杰、3.神射、4.鬼谋   （UI中顺序）

	switch(((UITab*)pSender)->getCurrentIndex())
	{
	case 0: 
		{
			RankData::instance()->set_proOrCountry(0);
		}
		break;
	case 1:
		{
			RankData::instance()->set_proOrCountry(1);
		}
		break;
	case 2:
		{
			RankData::instance()->set_proOrCountry(3);
		}
		break;
	case 3:
		{
			RankData::instance()->set_proOrCountry(4);
		}
		break;
	case 4:
		{
			RankData::instance()->set_proOrCountry(2);
		}
		break;
	}
	
	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		RankDataTableView::instance()->clearVectorRankPlayer();

		m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	// 每次点击左侧的item，需要重置所选item的index为default
	RankDataTableView::instance()->setSelectItemDefault();

	reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
}


void RankUI::countryTypeTabIndexChangedEvent( CCObject * pSender )
{
	//职业 0.全部、1.魏国、2.蜀国、3.吴国

	switch(((UITab*)pSender)->getCurrentIndex())
	{
	case 0: 
		{
			RankData::instance()->set_proOrCountry(0);
		}
		break;
	case 1:
		{
			RankData::instance()->set_proOrCountry(1);
		}
		break;
	case 2:
		{
			RankData::instance()->set_proOrCountry(2);
		}
		break;
	case 3:
		{
			RankData::instance()->set_proOrCountry(3);
		}
		break;
	}

	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		RankDataTableView::instance()->clearVectorRankPlayer();

		m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	// 每次点击左侧的item，需要重置所选item的index为default
	RankDataTableView::instance()->setSelectItemDefault();

	reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
}

void RankUI::flowerTypeTabIndexChagedEvent( CCObject * pSender )
{
	// 鲜花 1.今日收花、2.今日送花、3.历史收花、4.历史送花
	
	switch(((UITab*)pSender)->getCurrentIndex())
	{
	case 0: 
		{
			RankData::instance()->set_proOrCountry(1);
		}
		break;
	case 1:
		{
			RankData::instance()->set_proOrCountry(2);
		}
		break;
	case 2:
		{
			RankData::instance()->set_proOrCountry(3);
		}
		break;
	case 3:
		{
			RankData::instance()->set_proOrCountry(4);
		}
		break;
	}

	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		RankDataTableView::instance()->clearVectorRankPlayer();

		m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	// 每次点击左侧的item，需要重置所选item的index为default
	RankDataTableView::instance()->setSelectItemDefault();

	reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
	
}

void RankUI::reloadDataTableView()
{
	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{

	}
	else
	{
		m_dataView = CCTableView::create(RankDataTableView::instance(), CCSizeDataTableViewGeneral);
		//m_dataView->setSelectedEnable(true);
		//m_dataView->setSelectedScale9Texture("res_ui/mengban_green.png", CCRectMake(7, 7, 1, 1), ccp(0,0));   // 设置九宫格图片
		m_dataView->setDirection(kCCScrollViewDirectionVertical);
		m_dataView->setAnchorPoint(ccp(0, 0));
		m_dataView->setPosition(CCPointDataTableView);
		m_dataView->setDelegate(RankDataTableView::instance());
		m_dataView->setVerticalFillOrder(kCCTableViewFillTopDown);
		m_dataView->setPressedActionEnabled(false);
		m_dataView->setTag(TAG_SCROLLVIEW);

		int nRankType = RankData::instance()->get_rankType();
		if (0 == nRankType || 1 == nRankType)
		{
			m_dataView->setViewSize(CCSizeDataTableViewFamily);
			m_dataView->setSelectedEnable(true);
		}
		else if (2 == nRankType)
		{
			m_dataView->setViewSize(CCSizeDataTableViewFamily);
			m_dataView->setSelectedEnable(true);
		}
		else if (3 == nRankType)
		{
			m_dataView->setViewSize(CCSizeDataTableViewFamily);
			m_dataView->setSelectedEnable(true);
		}
		else if (4 == nRankType)
		{
			m_dataView->setViewSize(CCSizeDataTableViewFamily);
			m_dataView->setSelectedEnable(false);
		}
		else if (5 == nRankType)
		{
			m_dataView->setViewSize(CCSizeDataTableViewFamily);
			m_dataView->setSelectedEnable(false);
		}
		

		m_base_layer->addChild(m_dataView);

		refreshDataTableView();
	}


	if (0 == RankData::instance()->get_rankType() || 1 == RankData::instance()->get_rankType() || 2 == RankData::instance()->get_rankType())
	{
		initDataFromDb(RankData::instance()->m_vector_player_db);

		reloadMyRankLabel(RANKITEMTYPE_PROFESSION);
	}
	else if (3 == RankData::instance()->get_rankType())
	{
		initDataFromDb(RankData::instance()->m_vector_player_db);

		reloadMyRankLabel(RANKITEMTYPE_COUNTRY);
	}
	else if (4 == RankData::instance()->get_rankType())
	{
		initDataFromDb(RankData::instance()->m_vector_family_db);

		reloadMyRankLabel(RANKITEMTYPE_PROFESSION);
	}
	else if (5 == RankData::instance()->get_rankType())
	{
		initDataFromDb(RankData::instance()->m_vector_player_db);

		reloadMyRankLabel(RANKITEMTYPE_FLOWER);
	}
}

void RankUI::initDataFromDb( std::vector<CRoleMessage *> vector_roleMessage )
{
	// 清空vector 
	RankDataTableView::instance()->clearVectorRankPlayer();

	// db中数据 保存 到 m_vector_rankPlayer中
	int nVectorSize = vector_roleMessage.size();
	for (int i = 0; i < nVectorSize; i++)
	{
		long long longPlayerId = vector_roleMessage.at(i)->id();
		int nRank = vector_roleMessage.at(i)->index();
		std::string  strName = vector_roleMessage.at(i)->name();
		int nMount = vector_roleMessage.at(i)->mount();
		int nProfession = vector_roleMessage.at(i)->profession();
		int nCountry = vector_roleMessage.at(i)->country();
		int nPageCur = vector_roleMessage.at(i)->get_pageCur();
		int nPageAll = vector_roleMessage.at(i)->get_pageAll();
		int nMyRank = vector_roleMessage.at(i)->get_myPlayerRank();
		long long longMyPlayerId = vector_roleMessage.at(i)->get_myPlayerId();
		int nVipLevel = vector_roleMessage.at(i)->viplevel();
		int nLevel = vector_roleMessage.at(i)->level();
		int nFlowerType = vector_roleMessage.at(i)->get_flowerType();

		std::string strProfession = getProfession(nProfession);
		std::string strCountry = getCountry(nCountry);

		RankPlayerStruct * rankPlayerStruct = new RankPlayerStruct();
		rankPlayerStruct->longPlayerId = longPlayerId;
		rankPlayerStruct->nRank = nRank;
		rankPlayerStruct->strName = strName;
		rankPlayerStruct->nMount = nMount;
		rankPlayerStruct->strProfession = strProfession;
		rankPlayerStruct->strCountry = strCountry;
		rankPlayerStruct->nCountry = nCountry;
		rankPlayerStruct->nPageCur = nPageCur;
		rankPlayerStruct->nPageAll = nPageAll;
		rankPlayerStruct->nMyPlayerRank = nMyRank;
		rankPlayerStruct->longMyPlayerId = longMyPlayerId;
		rankPlayerStruct->nVipLevel = nVipLevel;
		rankPlayerStruct->nLevel = nLevel;
		rankPlayerStruct->nFlowerType = nFlowerType;

		RankDataTableView::instance()->m_vector_rankPlayer.push_back(rankPlayerStruct);
	}

	refreshDataTableViewWithChangeOffSet();
}

void RankUI::initDataFromDb( std::vector<CHomeMessage *> vector_familyMessage )
{
	// 清空vector(vector_family)
	RankDataTableView::instance()->clearVectorRankFamily();

	// db中数据 保存 到 m_vector_rankFamily中
	int nVectorSize = vector_familyMessage.size();
	for (int i = 0;i < nVectorSize; i++)
	{
		long long longFamilyId = vector_familyMessage.at(i)->id();
		int nRank = vector_familyMessage.at(i)->index();
		std::string strFamilyName = vector_familyMessage.at(i)->name();
		int nLevel = vector_familyMessage.at(i)->level();
		int nFamilyMember = vector_familyMessage.at(i)->number();
		int nFamilyMemberMax = vector_familyMessage.at(i)->numberall();
		int nCountry = vector_familyMessage.at(i)->country();
		int nPageCur = vector_familyMessage.at(i)->get_pageCur();
		int nPageAll = vector_familyMessage.at(i)->get_pageAll();
		int nMyRank = vector_familyMessage.at(i)->get_myRank();
		std::string strCreaterName = vector_familyMessage.at(i)->creatername();
		std::string strAnnouncement = vector_familyMessage.at(i)->announcement();

		std::string strCountry = getCountry(nCountry);

		RankFamilyStruct * rankFamilyStruct = new RankFamilyStruct();
		rankFamilyStruct->longFamilyId = longFamilyId;
		rankFamilyStruct->nRank = nRank;
		rankFamilyStruct->strFamilyName = strFamilyName;
		rankFamilyStruct->nLevel = nLevel;
		rankFamilyStruct->nFamilyMember = nFamilyMember;
		rankFamilyStruct->nFamilyMemberMax = nFamilyMemberMax;
		rankFamilyStruct->strCountry = strCountry;
		rankFamilyStruct->nPageCur = nPageCur;
		rankFamilyStruct->nPageAll = nPageAll;
		rankFamilyStruct->nMyRank = nMyRank;
		rankFamilyStruct->strCreaterName = strCreaterName;
		rankFamilyStruct->strAnnouncement = strAnnouncement;

		RankDataTableView::instance()->m_vector_rankFamily.push_back(rankFamilyStruct);
	}

	refreshDataTableViewWithChangeOffSet();
}

std::string RankUI::getProfession( int nProfession )
{
	std::string strProfession = "";

	// 0:全部;1:猛将;2:鬼谋;3:豪杰;4:神射
	switch (nProfession)
	{
	case 0:
		{
			const char *strAll = StringDataManager::getString("rank_all");
			strProfession = strAll;
		}
		break;
	case 1:
		{
			const char *strMengjiang = StringDataManager::getString("rank_mengjiang");
			strProfession = strMengjiang;
		}
		break;
	case 2:
		{
			const char *strGuimou = StringDataManager::getString("rank_guimou");
			strProfession = strGuimou;
		}
		break;
	case 3:
		{
			const char *strHaojie = StringDataManager::getString("rank_haojie");
			strProfession = strHaojie;
		}
		break;
	case 4:
		{
			const char *strShenshe = StringDataManager::getString("rank_shenshe");
			strProfession = strShenshe;
		}
		break;
	default:
		{

		}
	}

	return strProfession;
}

std::string RankUI::getCountry( int nCountry )
{
	// 0:全部 1:魏；2：蜀；3：吴
	const char * charCountry = CMapInfo::getCountryStr(nCountry);

	return charCountry;
}

void RankUI::set_hasReqInternet( bool bFlag )
{
	this->m_bHasReqInternet = bFlag;
}

bool RankUI::get_hasReqInternet()
{
	return this->m_bHasReqInternet;
}

void RankUI::refreshDataTableView()
{
	m_dataView->reloadData();
}

void RankUI::refreshDataTableViewWithChangeOffSet()
{
	CCPoint offSetPoint = m_dataView->getContentOffset();
	int offsetHeight = m_dataView->getContentSize().height + offSetPoint.y;

	m_dataView->reloadData();

	CCPoint temp = ccp(offSetPoint.x, offsetHeight - m_dataView->getContentSize().height);
	m_dataView->setContentOffset(temp); 
}

void RankUI::initTimeFlag()
{
	// 判断服务器重启时间戳
	// 查询数据库
	std::string str_tableName = "t_rank_player_all";
	long long timeRefreshFlag = RankDb::instance()->checkDbTimeRefreshFlag(str_tableName);

	if (timeRefreshFlag != RankData::instance()->get_timeRefreshFlag())
	{
		// 时间戳失效
		RankDb::instance()->timeIsOut();

		return ;
	}
	
	// 判断时间戳
	if (-1 == RankData::instance()->get_timeFlag() || -1 == RankData::instance()->get_timeRefreshFlag())
	{
		// 查询数据库
		std::string str_tableName = "t_rank_player_all";
		long long timeFlag = RankDb::instance()->checkDbTimeFlag(str_tableName);
		long long timeRefreshFlag = RankDb::instance()->checkDbTimeRefreshFlag(str_tableName);

		// 若 timeFlag等于0，说明数据库为空
		if (0 == timeFlag || 0 == timeRefreshFlag)
		{
			// 请求服务器-----发送请求，请求服务器 等级、全部、第0页数据
			RankDataStruct * rankDataStruct = new RankDataStruct();
			rankDataStruct->nType = 0;
			rankDataStruct->nWork = 0;
			rankDataStruct->nPage = 0;

			///////////////////////////////////////////////////////////////////////
			/*char str_page[10];
			sprintf(str_page, "%d", rankDataStruct->nPage);
			std::string str_show = "request pageNum:";
			str_show.append(str_page);

			GameView::getInstance()->showAlertDialog(str_show);*/
			//////////////////////////////////////////////////////////////////////

			GameMessageProcessor::sharedMsgProcessor()->sendReq(2300, (void *)rankDataStruct);
		}
		else
		{
			// 获取时间戳
			RankData::instance()->set_timeFlag(timeFlag);
			RankData::instance()->set_timeRefreshFlag(timeRefreshFlag);

			// 判断时间戳
			judgeTimeFlag();
		}
	}
	else
	{
		// 判断时间戳
		judgeTimeFlag();
	}
}

void RankUI::reloadTimeFlag( int nRankType, int nProfession )
{
	// tableName
	std::string str_tableName = "";
	if (0 == nProfession && 3 != nRankType)
	{
		// 查询数据库
		str_tableName = "t_rank_player_all";
	}
	else if(0 == nProfession && 3 == nRankType)
	{
		str_tableName = "t_rank_country_all";
	}
	else
	{
		str_tableName = "t_rank_player";
	}

	// 从db中取数据到 vector(临时的vector)
	std::vector<CRoleMessage *> tmpVector;
	RankDb::instance()->getDataFromDb(tmpVector, nRankType, nProfession);
	int nSizeVector = tmpVector.size();

	// 判断所要显示的数据是否为空
	if (nSizeVector > 0)
	{
		// 判断时间戳是否过期
		// 时间戳 与 现在的时间对比。如果timeFlag 等于 milliscondNow()，（1）清空数据库（2）关闭窗体，重新打开（3）return;否则，读取数据库数据
		if (GameUtils::millisecondNow() >= RankData::instance()->get_timeFlag())
		{
			//GameView::getInstance()->showAlertDialog("rankUI reloadTimeFlag timeFlag equals");

			// 时间戳已更新
			RankDb::instance()->timeIsOut();

			return;
		}
		else
		{
			// 读取数据库数据----从db中取数据到 vector(m_vector_player_db)
			RankDb::instance()->getDataFromDb(RankData::instance()->m_vector_player_db, nRankType, nProfession);

			this->reloadDataTableView();
		}
	}
	else
	{
		// 请求服务器-----发送请求，请求服务器 等级、全部、第0页数据
		RankDataStruct * rankDataStruct = new RankDataStruct();
		rankDataStruct->nType = nRankType;
		rankDataStruct->nWork = nProfession;
		rankDataStruct->nPage = 0;

		///////////////////////////////////////////////////////////////////////////////
		/*char str_page[10];
		sprintf(str_page, "%d", rankDataStruct->nPage);
		std::string str_show = "request pageNum:";
		str_show.append(str_page);

		GameView::getInstance()->showAlertDialog(str_show);*/
		///////////////////////////////////////////////////////////////////////////////

		GameMessageProcessor::sharedMsgProcessor()->sendReq(2300, (void *)rankDataStruct);
	}
}

void RankUI::reloadTimeFlag()
{
	std::string str_tableName= "t_rank_family";
	// 从db中取数据到 vector(临时的vector)
	std::vector<CHomeMessage *> tmpVector;
	RankDb::instance()->getDataFromDb(tmpVector);
	int nSizeVector = tmpVector.size();

	if (nSizeVector > 0)
	{
		// 判断时间戳是否过期
		// 时间戳 与 现在的时间对比。如果timeFlag 等于 milliscondNow()，（1）清空数据库（2）关闭窗体，重新打开（3）return;否则，读取数据库数据
		if (GameUtils::millisecondNow() >= RankData::instance()->get_timeFlag())
		{
			//GameView::getInstance()->showAlertDialog("reloadTimeFlag timeFlag equals");

			// 时间戳已更新
			RankDb::instance()->timeIsOut();

			return;
		}
		else
		{
			// 读取数据库数据----从db中取数据到 vector(m_vector_player_db)
			RankDb::instance()->getDataFromDb(RankData::instance()->m_vector_family_db);

			this->reloadDataTableView();
		}
	}
	else
	{
		// 请求服务器-----发送请求，请求服务器 等级、全部、第0页数据
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2301, (void *)0);
	}
}

UILayer* RankUI::getBaseUILayer()
{
	return this->m_base_layer;
}

void RankUI::reloadMyRankLabel(int nRankItemType)
{
	if (RANKITEMTYPE_PROFESSION == nRankItemType)
	{
		std::string str_PlayerProfession = GameView::getInstance()->myplayer->getActiveRole()->profession();
		std::string str_ClickProfession = getProfession(RankData::instance()->get_proOrCountry());

		const char* strAll = StringDataManager::getString("rank_all");
		char* pAll = const_cast<char*>(strAll);

		//说明所选的是"全部" 或者 对应自己职业的武将称呼
		if (str_ClickProfession == pAll || str_ClickProfession == str_PlayerProfession)
		{
			m_label_myRankLabel->setVisible(true);
			m_label_myRankNum->setVisible(true);
		}
		else
		{
			m_label_myRankLabel->setVisible(false);
			m_label_myRankNum->setVisible(false);
		}
	}
	else if (RANKITEMTYPE_COUNTRY == nRankItemType)
	{
		std::string str_PlayerCountry = getCountry(GameView::getInstance()->getMapInfo()->country());
		std::string str_ClickCountry = getCountry(RankData::instance()->get_proOrCountry());

		const char* strAll = StringDataManager::getString("rank_all");
		char* pAll = const_cast<char*>(strAll);

		//说明所选的是"全部" 或者 对应自己国家
		if (str_ClickCountry == pAll || str_ClickCountry == str_PlayerCountry)
		{
			m_label_myRankLabel->setVisible(true);
			m_label_myRankNum->setVisible(true);
		}
		else
		{
			m_label_myRankLabel->setVisible(false);
			m_label_myRankNum->setVisible(false);
		}
	}
	else if (RANKITEMTYPE_FLOWER == nRankItemType)
	{
		m_label_myRankLabel->setVisible(true);
		m_label_myRankNum->setVisible(true);
	}

	// 更新数据
	int nMyRank = 0;

	int nRankType = RankData::instance()->get_rankType();
	if (0 == nRankType || 1 == nRankType || 2 == nRankType || 3 == nRankType || 5 == nRankType)
	{
		const char* char_wodepaihang = StringDataManager::getString("rank_wodepaiming");
		m_label_myRankLabel->setText(char_wodepaihang);

		std::vector<RankPlayerStruct *> vector_rankPlayer = RankDataTableView::instance()->m_vector_rankPlayer;
		if (vector_rankPlayer.size() > 0)
		{
			nMyRank = vector_rankPlayer.at(0)->nMyPlayerRank;
		}
	}
	else if (4 == nRankType)
	{
		const char* char_wodejiazupaihang = StringDataManager::getString("rank_wodejiazupaiming");
		m_label_myRankLabel->setText(char_wodejiazupaihang);

		std::vector<RankFamilyStruct *> vector_rankFamily = RankDataTableView::instance()->m_vector_rankFamily;
		if (vector_rankFamily.size() > 0)
		{
			nMyRank = vector_rankFamily.at(0)->nMyRank;
		}

		m_label_myRankLabel->setVisible(true);
		m_label_myRankNum->setVisible(true);
	}

	std::string str_myRankNum = "";

	m_label_myRankInfo->setVisible(false);

	// 我的排名为-1，说明 新开服，排行榜未刷新(鲜花榜例外)；为-2，说明 家族（未加入任何家族）
	if (-1 == nMyRank)
	{
		// 
		if (5 == nRankType)
		{
			m_label_myRankInfo->setVisible(true);

			m_label_myRankLabel->setVisible(false);
			m_label_myRankNum->setVisible(false);
		}
		else
		{
			const char* char_notJoinInRank = StringDataManager::getString("rank_rankNoRefresh");
			str_myRankNum = char_notJoinInRank;
		}
	}
	else if(-2 == nMyRank)
	{
		const char* char_notJoinInFamily = StringDataManager::getString("rank_nojiazu");
		str_myRankNum = char_notJoinInFamily;
	}
	else 
	{
		char str_myRank[20];
		sprintf(str_myRank, "%d", nMyRank);
		str_myRankNum = str_myRank;
	}

	m_label_myRankNum->setText(str_myRankNum.c_str());
	m_label_myRankNum->setPosition(ccp(m_label_myRankLabel->getPosition().x + m_label_myRankLabel->getContentSize().width, m_label_myRankNum->getPosition().y));
}

void RankUI::judgeTimeFlag()
{
	// 判断时间戳是否过期
	// 时间戳 与 现在的时间对比。如果timeFlag 等于 milliscondNow()，（1）清空数据库（2）填充当前用户所浏览的nRankType和nProfession（3）return;否则，读取数据库数据
	if (GameUtils::millisecondNow() >= RankData::instance()->get_timeFlag())
	{
		//GameView::getInstance()->showAlertDialog("rankUI initTimeflag timeFlag equals");

		// 时间戳失效
		RankDb::instance()->timeIsOut();
	}
	else
	{
		// 判断该数据库 是否 对应 相应的玩家（id 和 name 共同确认）
		std::string str_tableName = "t_rank_player_all";
		long long longMyPlayerIdDb = RankDb::instance()->checkMyPlayerId(str_tableName);
		std::string str_myPlayerNameDb = RankDb::instance()->checkMyPlayerName(str_tableName);

		// 获取用户id
		RoleBase* base = GameView::getInstance()->myplayer->getActiveRole()->mutable_rolebase();
		long long longRoleId = base->roleid();
		std::string str_myPlayerName = base->name();

		if (longMyPlayerIdDb == longRoleId && str_myPlayerNameDb == str_myPlayerName)
		{
			// 读取数据库数据----从db中取数据到 vector(m_vector_player_db)
			RankDb::instance()->getDataFromDb(RankData::instance()->m_vector_player_db, RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());

			this->reloadDataTableView();
		}
		else 
		{
			// 用户数据库不一致，重新获取，可视为 时间戳失效
			RankDb::instance()->timeIsOut();
		}
	}
}

void RankUI::callBackBtnFamilyInfo( CCObject * pSender )
{
	CCMenuItemSprite* pMenuItem = (CCMenuItemSprite *)pSender;
	int nRank = pMenuItem->getTag();

	/*char charId[20];
	sprintf(charId, "%d", nRank);

	GameView::getInstance()->showAlertDialog(charId);*/


	std::vector<RankFamilyStruct *> vector_rankFamily = RankDataTableView::instance()->m_vector_rankFamily;
	int nIndex = nRank - 1;

	std::string str_familyName = vector_rankFamily.at(nIndex)->strFamilyName;
	std::string str_familyCreaterName = vector_rankFamily.at(nIndex)->strCreaterName;
	long long longFamilyId = vector_rankFamily.at(nIndex)->longFamilyId;
	std::string str_announcement = vector_rankFamily.at(nIndex)->strAnnouncement;

	int nFamilyMember = vector_rankFamily.at(nIndex)->nFamilyMember;
	char charFamilyMember[10];
	sprintf(charFamilyMember, "%d", nFamilyMember);

	int nFamilyMemberMax = vector_rankFamily.at(nIndex)->nFamilyMemberMax;
	char charFamilyMemberMax[10];
	sprintf(charFamilyMemberMax, "%d", nFamilyMemberMax);

	std::string str_familyNum = "";
	str_familyNum.append(charFamilyMember);
	str_familyNum.append("/");
	str_familyNum.append(charFamilyMemberMax);


	CCSize winsize= CCDirector::sharedDirector()->getVisibleSize();
	
	ApplyChackFamily * checkFamily =ApplyChackFamily::create(str_familyName, str_familyCreaterName, longFamilyId, str_familyNum, str_announcement);
	checkFamily->ignoreAnchorPointForPosition(false);
	checkFamily->setAnchorPoint(ccp(0.5, 0.5f));
	checkFamily->setPosition(ccp(winsize.width / 2, winsize.height / 2));
	GameView::getInstance()->getMainUIScene()->addChild(checkFamily);
}

void RankUI::callBackTreeEvent( CCObject * obj )
{
	ControlTree * tree_ = (ControlTree *)obj;
	//int index_ = tree_->getTouchTreeIndex();
	//RankData::instance()->set_rankType(index_);

}

void RankUI::callBackSecondTreeEvent( CCObject * obj )
{
	ControlTree * tree_ = (ControlTree *)obj;

	int index_ = tree_->getTouchTreeIndex();
	RankData::instance()->set_rankType(index_);

	int index_panel = tree_->getSecondTreeByEveryPanelIndex();
	RankData::instance()->set_proOrCountry(index_panel);

	refreshDataByTreeIndex();
	//
	const char *str_level = StringDataManager::getString("rank_level");
	if (strcmp(m_vector_tableView.at(index_).c_str(),str_level)==0)
	{
		refreshDataBySecondTreeIndex(index_panel);
	}

	const char *str_capcity = StringDataManager::getString("rank_capacity");
	if (strcmp(m_vector_tableView.at(index_).c_str(),str_capcity)==0)
	{
		refreshDataBySecondTreeIndex(index_panel);
	}
	const char *str_coin = StringDataManager::getString("rank_coin");
	if (strcmp(m_vector_tableView.at(index_).c_str(),str_coin)==0)
	{
		//branch tree is null
	}
	const char *str_battleAchievement = StringDataManager::getString("rank_battleAchievement");
	if (strcmp(m_vector_tableView.at(index_).c_str(),str_battleAchievement)==0)
	{
		//职业 0.全部、1.魏国、2.蜀国、3.吴国
		switch(index_panel)
		{
		case 0: 
			{
				RankData::instance()->set_proOrCountry(0);
			}
			break;
		case 1:
			{
				RankData::instance()->set_proOrCountry(1);
			}
			break;
		case 2:
			{
				RankData::instance()->set_proOrCountry(2);
			}
			break;
		case 3:
			{
				RankData::instance()->set_proOrCountry(3);
			}
			break;
		}

		if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
		{
			RankDataTableView::instance()->clearVectorRankPlayer();

			m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
		}

		// 每次点击左侧的item，需要重置所选item的index为default
		RankDataTableView::instance()->setSelectItemDefault();
		reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
	}
	const char *str_family = StringDataManager::getString("rank_family");
	if (strcmp(m_vector_tableView.at(index_).c_str(),str_family)==0)
	{
		//branch tree is null
	}

	const char *str_flower = StringDataManager::getString("rank_flower");
	if (strcmp(m_vector_tableView.at(index_).c_str(),str_flower)==0)
	{
		// 鲜花 1.今日收花、2.今日送花、3.历史收花、4.历史送花
		switch(index_panel)
		{
		case 0: 
			{
				RankData::instance()->set_proOrCountry(1);
			}
			break;
		case 1:
			{
				RankData::instance()->set_proOrCountry(2);
			}
			break;
		case 2:
			{
				RankData::instance()->set_proOrCountry(3);
			}
			break;
		case 3:
			{
				RankData::instance()->set_proOrCountry(4);
			}
			break;
		}

		if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
		{
			RankDataTableView::instance()->clearVectorRankPlayer();

			m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
		}

		// 每次点击左侧的item，需要重置所选item的index为default
		RankDataTableView::instance()->setSelectItemDefault();
		reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
	}
}

void RankUI::refreshDataByTreeIndex()
{
	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		RankDataTableView::instance()->clearVectorRankPlayer();
		RankDataTableView::instance()->clearVectorRankFamily();

		m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}
	// 每次点击左侧的item，需要重置所选item的index为default
	RankDataTableView::instance()->setSelectItemDefault();

	// 0:等级;1:战斗力；2:金币；3:战功；4：家族；5：鲜花
	switch (RankData::instance()->get_rankType())
	{
	case 0:
		{
			m_panel_level->setVisible(true);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_family->setVisible(false);
			m_panel_battleAch->setVisible(false);
			m_panel_flower->setVisible(false);

			//m_tab_pro->setVisible(true);
			//m_tab_country->setVisible(false);

			//m_tab_pro->setDefaultPanelByIndex(0);
			RankData::instance()->set_proOrCountry(0);

			// 更新时间戳
			//reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 1:
		{
			m_panel_combat->setVisible(true);
			m_panel_level->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_family->setVisible(false);
			m_panel_battleAch->setVisible(false);
			m_panel_flower->setVisible(false);

			RankData::instance()->set_proOrCountry(0);

			// 更新时间戳
			//reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 2:
		{
			m_panel_coin->setVisible(true);
			m_panel_family->setVisible(false);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_battleAch->setVisible(false);
			m_panel_flower->setVisible(false);

			RankData::instance()->set_proOrCountry(0);

			// 更新时间戳
			reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 3:
		{
			m_panel_battleAch->setVisible(true);
			m_panel_family->setVisible(false);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_flower->setVisible(false);

			RankData::instance()->set_proOrCountry(0);

			// 更新时间戳
			//reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 4:
		{
			m_panel_family->setVisible(true);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_battleAch->setVisible(false);
			m_panel_flower->setVisible(false);

			// 更新时间戳
			reloadTimeFlag();
		}
		break;
	case 5:
		{
			m_panel_flower->setVisible(true);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_family->setVisible(false);
			m_panel_battleAch->setVisible(false);
			
			RankData::instance()->set_proOrCountry(1);
		}
	}
}

void RankUI::refreshDataBySecondTreeIndex( int index )
{
	//职业 0.全部、1.猛将、2.鬼谋、3.豪杰、4.神射、（协议定义）
	//职业 0.全部、1.猛将、2.豪杰、3.神射、4.鬼谋   （UI中顺序）
	switch(index)
	{
	case 0: 
		{
			RankData::instance()->set_proOrCountry(0);
		}
		break;
	case 1:
		{
			RankData::instance()->set_proOrCountry(1);
		}
		break;
	case 2:
		{
			RankData::instance()->set_proOrCountry(3);
		}
		break;
	case 3:
		{
			RankData::instance()->set_proOrCountry(4);
		}
		break;
	case 4:
		{
			RankData::instance()->set_proOrCountry(2);
		}
		break;
	}

	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		RankDataTableView::instance()->clearVectorRankPlayer();

		m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	// 每次点击左侧的item，需要重置所选item的index为default
	RankDataTableView::instance()->setSelectItemDefault();
	reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
}




