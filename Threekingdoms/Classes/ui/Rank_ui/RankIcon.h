#ifndef _UI_RANK_RANKICON_H_
#define _UI_RANK_RANKICON_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 排行版Icon
 * @author liuliang
 * @version 0.1.0
 * @date 2014.04.08
 */

class RankIcon:public UIScene
{
public:
	RankIcon(void);
	~RankIcon(void);

public:
	static RankIcon* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

public:
	UIButton * m_btn_rankIcon;							// 可点击的ICON
	UILayer * m_layer_rank;								// 需要用到的layer
	UILabel * m_label_rank_text;						// 	ICON下方显示的文字	
	CCScale9Sprite * m_spirte_fontBg;				// 文字显示的底图

public:
	void callBackRank(CCObject *obj);

};

#endif

