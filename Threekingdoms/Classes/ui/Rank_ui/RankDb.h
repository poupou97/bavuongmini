#ifndef _UI_RANK_RANKDB_H_
#define _UI_RANK_RANKDB_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * 排行榜模块（用于DB数据库操作）
 * @author liuliang
 * @version 0.1.0
 * @date 2014.04.09
 */
class CRoleMessage;
class CHomeMessage;

class RankDb
{
public:
	static RankDb * s_rankDb;
	static RankDb * instance();

private:
	RankDb(void);
	~RankDb(void);

public:
	long long checkDbTimeFlag(std::string strTableName);																																//  查询 db 数据库中的时间戳（若返回值为0，说明 数据库为空）
	long long checkMyPlayerId(std::string strTableName);																																	//  查询 db 数据库中 玩家的id（确保我的排行是同一个人）
	std::string checkMyPlayerName(std::string strTableName);																															//	 查询 db 数据库中 玩家的name（id  和 name确保是同一个人---不同服务器上同一玩家玩家信息是不一样的）

	long long checkDbTimeRefreshFlag(std::string strTableName);																														//  查询 db 数据库中timeRefreshFlag

	void insertData(std::string strTableName, std::vector<CRoleMessage *> vector_roleMessage);																//  插入数据到 指定 表
	void insertData(std::string strTableName, std::vector<CHomeMessage *> vector_familyMessage);													

	void deleteTableData(std::string strTableName);																																			//  删除表中的数据

	void getDataFromDb(std::vector<CRoleMessage *> &vector_roleMessage, int nRankType, int nProfession);											//  从db中获取数据保存到vector中（用于显示db中的数据）
	void getDataFromDb(std::vector<CHomeMessage *> &vector_familyMessage);

	void timeIsOut();																																															//  timeIsOut
};

#endif

