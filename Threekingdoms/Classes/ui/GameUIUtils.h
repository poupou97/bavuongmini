#ifndef _GAME_UI_UTILS_H_
#define _GAME_UI_UTILS_H_

#include "cocos2d.h"

USING_NS_CC;

class CCLegendAnimation;

class GameUIUtils {
public:
	/** pre-load some resources **/
	static void preloadEffects();

	static bool s_bPreloaded;
};

#endif