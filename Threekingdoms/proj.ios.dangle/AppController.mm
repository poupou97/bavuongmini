#import "AppController.h"
#import "EAGLView.h"
#import "cocos2d.h"
#import "AppDelegate.h"
#import "RootViewController.h"

#import <DownjoySDK/DJPlatform.h>
#import <DownjoySDK/DJPlatformNotification.h>
#import <DownjoySDK/UserInfomation.h>

#import "ExtenalClass.h"

@implementation AppController

#pragma mark -
#pragma mark Application lifecycle

// cocos2d application instance
static AppDelegate s_sharedApplication;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {    
    
    // Override point for customization after application launch.

    // Add the view controller's view to the window and display.
    window = [[UIWindow alloc] initWithFrame: [[UIScreen mainScreen] bounds]];
    
    // Init the EAGLView
    EAGLView *__glView = [EAGLView viewWithFrame: [window bounds]
                                     pixelFormat: kEAGLColorFormatRGB565
                                     depthFormat: GL_DEPTH24_STENCIL8_OES
                              preserveBackbuffer: NO
                                      sharegroup: nil
                                   multiSampling: NO
                                 numberOfSamples: 0];

    // Use RootViewController manage EAGLView 
    viewController = [[RootViewController alloc] initWithNibName:nil bundle:nil];
    viewController.wantsFullScreenLayout = YES;
    viewController.view = __glView;

    // Set RootViewController to window
    if ( [[UIDevice currentDevice].systemVersion floatValue] < 6.0)
    {
        // warning: addSubView doesn't work on iOS6
        [window addSubview: viewController.view];
    }
    else
    {
        // use this method on ios6
        [window setRootViewController:viewController];
    }
    
    [window makeKeyAndVisible];
    
    [[UIApplication sharedApplication] setStatusBarHidden:true];
	
	// to prevent the phone goes into auto-lock and auto-dimming 
	[[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
    
    //初始化SDK, 这些数据CP从我们的后台获取
    [[DJPlatform defaultDJPlatform] setAppId:@"1758"];
    [[DJPlatform defaultDJPlatform] setAppKey:@"S8P5ZxNG"];
    [[DJPlatform defaultDJPlatform] setMerchantId:@"598"];
    [[DJPlatform defaultDJPlatform] setServerId:@"118"];
    
    //设置屏幕方向
//    [[DJPlatform defaultDJPlatform] setDJScreenOrientation:UIInterfaceOrientationMaskLandscapeLeft];
    [[DJPlatform defaultDJPlatform] setDJScreenOrientation:UIInterfaceOrientationMaskLandscape];
    
    //设置浮漂在屏幕中的位置
    [[DJPlatform defaultDJPlatform] setAssistivePosition:TopMiddlePosition];
    
    //应用Scheme，在iOS系统中，app之间通信的ID，用支付宝快捷支付的时候需要
    [[DJPlatform defaultDJPlatform] setAppScheme:@"DJPlatformSDK3.0"];
    
    //注册监听登录结果通知
    [[NSNotificationCenter defaultCenter] addObserver:[ExtenalClass sharedIntance] selector:@selector(DJLoginResult:) name:kDJPlatformLoginResultNotification object:nil];
    
    //注销（登出）回调
    [[NSNotificationCenter defaultCenter] addObserver:[ExtenalClass sharedIntance] selector:@selector(DJLogout:) name:kDJPlatformLogoutResultNotification object:nil];
    
    //注册支付结果回调
    [[NSNotificationCenter defaultCenter] addObserver:[ExtenalClass sharedIntance] selector:@selector(DJUniPayResult:) name:kDJPlatformPaymentResultNotification object:nil];
    
    //注册充值结果回调
    [[NSNotificationCenter defaultCenter] addObserver:[ExtenalClass sharedIntance] selector:@selector(DJUniRechargeResult:)name:kDJPlatformRechargeResultNotification object:nil];
    
    
    cocos2d::CCApplication::sharedApplication()->run();

    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
    cocos2d::CCDirector::sharedDirector()->pause();
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
    cocos2d::CCDirector::sharedDirector()->resume();
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
    cocos2d::CCApplication::sharedApplication()->applicationDidEnterBackground();
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
    cocos2d::CCApplication::sharedApplication()->applicationWillEnterForeground();
}

- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
}

-(NSUInteger)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window
{
    return UIInterfaceOrientationMaskAll;
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


- (void)dealloc {
    [window release];
    [super dealloc];
}


@end
