//
//  ExtenalClass.m
//  Threekingdoms
//
//  Created by 刘亮 on 14-4-30.
//
//

#import "ExtenalClass.h"
#import "SetUI.h"
#import "GoldStoreUI.h"

#import <DownjoySDK/DJPlatform.h>
#import <DownjoySDK/UserInfomation.h>

#import "../Classes/login_state/Login.h"
#import "../Classes/login_state/LoginState.h"
#import "../Classes/messageclient/GameMessageProcessor.h"

#include "../Classes/GameView.h"




static ExtenalClass *sharedObj = nil;

static NSString *s_guestAccount = nil;

static long long s_payNum = 0;

static NSString *s_nsMacStr = nil;

static int s_nMachineType = 0;

static NSString *s_nsPlatformStr = nil;

static int s_nCharge = 0;

@implementation ExtenalClass

+(ExtenalClass*) sharedIntance
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            [[self alloc] init];
        }
    }
    return sharedObj;
}

+(id) allocWithZone:(struct _NSZone *)zone
{
    @synchronized(self)
    {
        if (sharedObj == nil)
        {
            sharedObj = [super allocWithZone:zone];
            return sharedObj;
        }
    }
    
    return nil;
}

-(id) copyWithZone:(NSZone *)zone
{
    return self;
}

-(id) retain
{
    return self;
}

-(unsigned) retainCount
{
    return UINT_MAX;
}

-(oneway void) release
{
    
}

-(id) autorelease
{
    return self;
}

-(id) init
{
    @synchronized(self)
    {
        [super init];
        return self;
    }
}

+(long long) get_PayNum
{
    return s_payNum;
}

+(void) set_PayNum:(long long)tmpPayNum
{
    s_payNum = tmpPayNum;
}

+(int) get_Charge
{
    return s_nCharge;
}

+(void) set_Charge:(int)nCharge
{
    s_nCharge = nCharge;
}

+(NSString *) get_macStr
{
    return s_nsMacStr;
}

+(void) set_macStr:(NSString *)nsMac
{
    s_nsMacStr = nsMac;
}

+(int) get_MachineType
{
    return s_nMachineType;
}

+(void) set_MachineType:(int)nMachineType
{
    s_nMachineType = nMachineType;
}

+(NSString *) get_platformStr
{
    return s_nsPlatformStr;
}

+(void) set_PlatformStr:(NSString *)nsPlatform
{
    s_nsPlatformStr = nsPlatform;
}

///////////////////////////////////////////////

+(void)messageBox:(NSString*)stringTip
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:stringTip
													message:nil
												   delegate:nil
										  cancelButtonTitle:nil
										  otherButtonTitles:@"确定", nil];
	[alert show];
	[alert release];
}

-(void)DJLoginResult:(NSNotification *)notify
{
    // 测试代码
//    UserInfomation *memberInfo = [notify object]; //玩家信息
//    NSString *content=[NSString stringWithFormat:@"mid:%@,username:%@,nickname:%@,token:%@,avatar_url:%@, gender:%@", memberInfo.mid, memberInfo.userName, memberInfo.nickName, memberInfo.token, memberInfo.avatarUrl, memberInfo.gender];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示信息" message:content delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//    [alert show];
//    [alert release];
    
//    // 正式代码
    UserInfomation * memberInfo = notify.object;   //玩家信息
    
    // 用户名
    NSString * userName = memberInfo.userName;
    
    // 乐号（mid）
    NSString * mid = memberInfo.mid;
    
    // token(相当于sessionId)
    NSString * token = memberInfo.token;
    
    const char * charAccount = [userName UTF8String];
    const char * charUin = [mid UTF8String];
    const char * charSessionId = [token UTF8String];
    
    Login::getInstance()->getLoginInfo(charAccount, charUin, charSessionId);
    
    //[[DJPlatform defaultDJPlatform] appearDJMemberCenter];
    
}

-(void)DJLogout:(NSNotification *)notify
{
    // 注销账号的通知
    cocos2d::CCUserDefault::sharedUserDefault()->flush();
    GameMessageProcessor::sharedMsgProcessor()->sendReq(1002);
}

-(void)DJUniPayResult:(NSNotification *)notify                     // 支付付款返回结果通知
{
    NSDictionary *dict = notify.object;
    NSString *orderNo = [dict objectForKey:@"orderNo"];
    NSNumber *code = [dict objectForKey:@"code"];
    NSString *message = nil;
    NSString * strTip = @"";
    if (code.integerValue == PaymentResultTypeFinish)   //支付结果状态码
    {
        message = @"支付完成";
        strTip = @"支付完成";
        
        const char * charChargeTip = [strTip UTF8String];
        GameView::getInstance()->showAlertDialog(charChargeTip);
    }
    else if(code.integerValue == PaymentResultTypeUnfinish)
    {
        message = @"支付未完成";
        strTip = @"支付未完成";
        
        const char * charChargeTip = [strTip UTF8String];
        GameView::getInstance()->showAlertDialog(charChargeTip);
    }
    
    
    
    // 提示信息
//    NSString * strLoginTip = message;
//    
//    const char * charMessageTip = [strLoginTip UTF8String];
//    
//    GameView::getInstance()->showAlertDialog(charMessageTip);
    
//    NSString *content=[NSString stringWithFormat:@"%@\norderNo:%@", message, orderNo];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示信息" message:content delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//    [alert show];
//    [alert release];
}

-(void)DJUniRechargeResult:(NSNotification *)notify              // 充值返回结果通知
{
    NSDictionary *dict = notify.object;
    NSNumber *code = [dict objectForKey:@"code"];
    NSString *message = nil;
    NSString * strTip = @"";
    if (code.integerValue == RechangeResultTypeSuccess)   //支付结果状态码
    {
        message = @"充值成功";
        strTip = @"充值成功";
        
        const char * charChargeTip = [strTip UTF8String];
        GameView::getInstance()->showAlertDialog(charChargeTip);
    }
    else if(code.integerValue == RechangeResultTypeFail)
    {
        message = @"充值失败";
        strTip = @"充值失败";
        
        const char * charChargeTip = [strTip UTF8String];
        GameView::getInstance()->showAlertDialog(charChargeTip);
    }
    else if(code.integerValue == RechangeResultTypeWait)
    {
        message = @"充值等待";
        strTip = @"充值等待";
        
        const char * charChargeTip = [strTip UTF8String];
        GameView::getInstance()->showAlertDialog(charChargeTip);
    }
    
//    NSString *content=[NSString stringWithFormat:@"%@", message];
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"提示信息" message:content delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
//    [alert show];
//    [alert release];
    
    // 提示信息
//    NSString * strLoginTip = message;
//    
//    const char * charMessageTip = [strLoginTip UTF8String];
//    
//    GameView::getInstance()->showAlertDialog(charMessageTip);
}


@end

