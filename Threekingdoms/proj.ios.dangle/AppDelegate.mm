#include "AppDelegate.h"

#include <vector>
#include <string>

//#include "GameView.h"
//#include "logo_state/LogoState.h"
//#include "test_state/TestState.h"
//#include "utils/GameResourceManager.h"
//#include "AppMacros.h"
//#include "SimpleAudioEngine.h"
//#include "loadres_state/LoadResState.h"

#include "../Classes/GameView.h"
#include "../Classes/logo_state/LogoState.h"
#include "../Classes/test_state/TestState.h"
#include "../Classes/utils/GameResourceManager.h"
#include "../Classes/AppMacros.h"
#include "SimpleAudioEngine.h"
#include "../Classes/loadres_state/LoadResState.h"

#include <DownjoySDK/DJPlatform.h>
#include <DownjoySDK/DJPlatformNotification.h>

USING_NS_CC;
using namespace std;
using namespace CocosDenshion;

AppDelegate::AppDelegate() {

}

AppDelegate::~AppDelegate() 
{
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    CCDirector* pDirector = CCDirector::sharedDirector();
    CCEGLView* pEGLView = CCEGLView::sharedOpenGLView();

    pDirector->setOpenGLView(pEGLView);

    TargetPlatform target = getTargetPlatform();
	//target = kTargetIpad;
    if (target == kTargetIpad)
    {
        designResolutionSize = cocos2d::CCSizeMake(960, 720);   // (1024,768)
    }
    
    // Set the design resolution
	pEGLView->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, kResolutionFixedHeight);
	if(pEGLView->getDesignResolutionSize().width < designResolutionSize.width)
	{
		pEGLView->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, kResolutionFixedWidth);
		if(pEGLView->getDesignResolutionSize().height < designResolutionSize.height)
		{
			// in this situation, maybe we can use pEGLView->getFrameSize() for design resolution
			CCAssert(false, "no suitable design resolution can be applied");
		}
	}
	CCLOG("Design Resolution: %d * %d", (int)pEGLView->getDesignResolutionSize().width, (int)pEGLView->getDesignResolutionSize().height);

	CCSize frameSize = pEGLView->getFrameSize();
    
    vector<string> searchPath;

	pDirector->setContentScaleFactor(1.0f);
    
    // set searching path
    CCFileUtils::sharedFileUtils()->setSearchPaths(searchPath);
	CCFileUtils::sharedFileUtils()->addSearchPath(CCFileUtils::sharedFileUtils()->getWritablePath().c_str());
	
    // turn on display FPS
    //pDirector->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    pDirector->setAnimationInterval(1.0 / 60);

	// prepare resource
	GameResourceManager* mgr = new GameResourceManager();
	mgr->prepareStaticData();
	delete mgr;

	// enter the first state for the game
	GameView::getInstance()->init();
	GameState* pScene = new LoadResState();
	//GameState* pScene = new TestState();
    if (pScene)
    {
        pScene->runThisState();
        pScene->release();
    }

    return true;
}

// This function will be called when the app is inactive. When comes a phone call,it's be invoked too
void AppDelegate::applicationDidEnterBackground() {
    CCDirector::sharedDirector()->stopAnimation();

    // if you use SimpleAudioEngine, it must be pause
    SimpleAudioEngine::sharedEngine()->pauseBackgroundMusic();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    CCDirector::sharedDirector()->startAnimation();

    // if you use SimpleAudioEngine, it must resume here
    SimpleAudioEngine::sharedEngine()->resumeBackgroundMusic();
}

//// 当乐SDK需要加上
//-(bool) application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
//{
//    [[NSNotificationCenter defaultCenter] postNotificationName:kDJPlatformAlixQuickPayEnd object:url];
//    
//    return YES;
//}
