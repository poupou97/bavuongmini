#include "Login.h"
#include <string>

#include "../Classes/login_state/LoginState.h"
#include "../Classes/GameView.h"
#include "../Classes/messageclient/protobuf/LoginMessage.pb.h"
#include "../Classes/messageclient/reqsender/ReqSenderProtocol.h"
#include "../Classes/ui/extensions/RichTextInput.h"
#include "SimpleAudioEngine.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/AppMacros.h"
#include "../Classes/GameUserDefault.h"
#include "../Classes/legend_engine/CCLegendAnimation.h"
#include "GameAudio.h"
#include "../Classes/utils/GameUtils.h"

#import <DownjoySDK/DJPlatform.h>
#import "ExtenalClass.h"

#define IS_CLEAR_ENABLE 0
#define BACKGROUND_ROLL_TIME 100.0f
#define BACKGROUND_TAG 123

using namespace CocosDenshion;

USING_NS_CC;
USING_NS_CC_EXT;

ServerListRsp* Login::m_pServerListRsp = new ServerListRsp();
long long Login::userId = 1;
string Login::sessionId = "";
std::string Login::s_loginserver_ip = THREEKINGDOMS_LOGINSERVER_OFFICIAL;

Login * Login::s_login = NULL;

Login::Login() 
: m_labelStatusCode(NULL)
{
	//stop the backgroundmusic when the Socket Closed         by:liutao  2014-1-23
	//SimpleAudioEngine::sharedEngine()->stopBackgroundMusic();
	GameUtils::playGameSound(MUSIC_KING, 1, true);

    CCSize winSize = CCDirector::sharedDirector()->getWinSize();

	GameView::getInstance()->ResetData();

    const int MARGIN = 40;
    const int SPACE = 35;

	// game logo
	/*CCSprite* pSprite = CCSprite::create("res_ui/first/first_di.png");
	pSprite->setPosition(ccp(winSize.width/2, winSize.height/2));
	pSprite->setAnchorPoint(ccp(0.5f, 0.5f));
	if(((float)winSize.width/(float)854) > ((float)winSize.height/(float)480))
	{
		pSprite->setScale((float)winSize.width/(float)854);
	}
	else
	{
		pSprite->setScale((float)winSize.height/(float)480);
	}
	addChild(pSprite, 0);
	*/

	//background image
	CCSprite* pSprite = CCSprite::create("images/denglu.jpg");
	pSprite->setPosition(ccp(0, 0));
	pSprite->setAnchorPoint(ccp(0, 0));
	float scaleValue;
	if(((float)winSize.width/(float)1136) > ((float)winSize.height/(float)640))
	{
		scaleValue = (float)winSize.width/(float)1136;
	}
	else
	{
		scaleValue = (float)winSize.height/(float)640;
	}
	pSprite->setScale(scaleValue);
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;
	//pSprite->setColor(ccc3(100, 100, 100));
	addChild(pSprite, 0);

	CCSprite* pSpriteNext = CCSprite::create("images/denglu.jpg");
	pSpriteNext->setPosition(ccp(winSize.width-1, 0));
	pSpriteNext->setAnchorPoint(ccp(0, 0));
	pSpriteNext->setScale(scaleValue);
	pSpriteNext->setTag(BACKGROUND_TAG);
	addChild(pSpriteNext, 0);

	CCMoveTo * moveToAction = CCMoveTo::create(BACKGROUND_ROLL_TIME,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturn = CCMoveTo::create(0,ccp(winSize.width-1, 0));
	CCMoveTo * moveToStart = CCMoveTo::create(BACKGROUND_ROLL_TIME/widthMultiple,ccp(0, 0));
	CCRepeatForever * repeapAction = CCRepeatForever::create(CCSequence::create(moveToAction,moveToReturn,CCDelayTime::create(BACKGROUND_ROLL_TIME*(widthMultiple-1)/widthMultiple),moveToStart,NULL));
	pSprite->runAction(repeapAction);
	
	CCSequence * nextAction = CCSequence::create(CCDelayTime::create(BACKGROUND_ROLL_TIME*(widthMultiple-1)/widthMultiple), CCCallFunc::create(this, callfunc_selector(Login::repeatFunc)), NULL);
	pSpriteNext->runAction(nextAction);

	Login::addBirds(this);

	//gamelogo
	CCSprite* pSpriteLogo = CCSprite::create("res_ui/select_the_sercer/gamelogo.png");
	pSpriteLogo->setPosition(ccp(winSize.width/2+10, winSize.height/2+100));
	pSpriteLogo->setAnchorPoint(ccp(0.5f, 0.5f));
	addChild(pSpriteLogo, 1);

	// game code version
	CCLabelTTF * labelVersion = CCLabelTTF::create(GAME_CODE_VERSION, APP_FONT_NAME, 20);
	ccColor3B blackColor = ccc3(0,0,0);   // black
	labelVersion->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, blackColor);
	labelVersion->setAnchorPoint(ccp(0, 1));
	labelVersion->setPosition(ccp(3, winSize.height));
	addChild(labelVersion);

	// game res version
	int resVersion = CCUserDefault::sharedUserDefault()->getIntegerForKey(KEY_OF_RES_VERSION, GAME_RES_VERSION);
	char resVerStr[20];
	sprintf(resVerStr, "res version: %d", resVersion);
	labelVersion = CCLabelTTF::create(resVerStr, APP_FONT_NAME, 20);
	labelVersion->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, blackColor);
	labelVersion->setAnchorPoint(ccp(0, 1));
	labelVersion->setPosition(ccp(3, winSize.height - 25));
	addChild(labelVersion);

	// reset the delta time for smooth particle animation
	// because the previous scene is a loading scene, the delta time is stop when loading resource
	CCDirector::sharedDirector()->setNextDeltaTimeZero(true);

	UILayer* layer= UILayer::create();

	// Create the button
    cocos2d::extension::UIButton *button = cocos2d::extension::UIButton::create();
    button->setTouchEnable(true);
    button->loadTextures("res_ui/button_di_1.png", "res_ui/button_di_1.png", "");
    //button->setPosition(ccp(winSize.width / 2.0f, winSize.height / 2.0f));
	//button->setAnchorPoint(ccp(0.5,0.5));
	button->setPosition(ccp(-50,-150));
	button->addReleaseEvent(this, coco_releaseselector(Login::onMenuGetLoginClicked));
	button->setPressedActionEnabled(true);
	
	cocos2d::extension::UIImageView * loginText = cocos2d::extension::UIImageView::create();
	loginText->setTexture("res_ui/first/button_denglu.png");
	loginText->setVisible(true);
	//loginText->setAnchorPoint(ccp(0,0));
	//loginText->setPosition(ccp(2,3));
	button->addChild(loginText);

	cocos2d::extension::UIButton *buttonRegister = cocos2d::extension::UIButton::create();
    buttonRegister->setTouchEnable(true);
    buttonRegister->loadTextures("res_ui/button_di_1.png", "res_ui/button_di_1.png", "");
//	buttonRegister->setPosition(ccp(50,-150));
    button->setAnchorPoint(ccp(0.5f,0.5f));
	button->setPosition(ccp(0,-150));
	buttonRegister->addReleaseEvent(this, coco_releaseselector(Login::onMenuGetRegisterClicked));
	buttonRegister->setPressedActionEnabled(true);

	cocos2d::extension::UIImageView * registerImage = cocos2d::extension::UIImageView::create();
	registerImage->setTexture("res_ui/first/button_zhuce.png");
	registerImage->setVisible(true);
	buttonRegister->addChild(registerImage);

#if IS_CLEAR_ENABLE
	// clear account and password
	UIButton *buttonClear = UIButton::create();
    buttonClear->setTouchEnable(true);
    buttonClear->loadTextures("res_ui/new_button_7.png", "res_ui/new_button_7.png", "");
	buttonClear->setPosition(ccp(150,-50));
	buttonClear->addReleaseEvent(this, coco_releaseselector(Login::onMenuClear));
	buttonClear->setPressedActionEnabled(true);

	UIImageView * deleteImage = UIImageView::create();
	deleteImage->setTexture("res_ui/liaotian/delete.png");
	deleteImage->setVisible(true);
	buttonClear->addChild(deleteImage);
	layer->addWidget(buttonClear);
#endif

    layer->addWidget(button);
	//layer->addWidget(buttonRegister);
	layer->setPosition(ccp(winSize.width/2,winSize.height/2));
	addChild(layer);

	ccColor3B shadowColor = ccc3(0,0,0);   // black

	CCLabelTTF * account = CCLabelTTF::create("账号", APP_FONT_NAME, 20);
	account->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	account->setPosition(ccp(-130,-30));
	account->setColor(ccc3(255,255,255));
	account->enableShadow(CCSizeMake(2.0, -2.0), 0.5, 0.0, shadowColor);
	//layer->addChild(account);

	CCLabelTTF * password = CCLabelTTF::create("密码", APP_FONT_NAME, 20);
	password->enableShadow(CCSizeMake(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	password->setPosition(ccp(-130,-80));
	password->setColor(ccc3(255,255,255));
	password->enableShadow(CCSizeMake(2.0, -2.0), 0.5, 0.0, shadowColor);
	//layer->addChild(password);
	
	cocos2d::extension::UIImageView *inputBox=cocos2d::extension::UIImageView::create();
	inputBox->setTexture("res_ui/liaotian/shurukuang.png");
	inputBox->setAnchorPoint(ccp(0.5f,0.5f));
	inputBox->setPosition(ccp(0,-30));
	inputBox->setScale9Enable(true);
	inputBox->setScale9Size(CCSizeMake(200,34));
	//layer->addWidget(inputBox);

	cocos2d::extension::UIImageView *inputPin=cocos2d::extension::UIImageView::create();
	inputPin->setTexture("res_ui/liaotian/shurukuang.png");
	inputPin->setAnchorPoint(ccp(0.5f,0.5f));
	inputPin->setPosition(ccp(0,-80));
	inputPin->setScale9Enable(true);
	inputPin->setScale9Size(CCSizeMake(200,34));
	//layer->addWidget(inputPin);
	
	pAccountInputBox = new RichTextInputBox();
	pAccountInputBox->setCharLimit(20);
	pAccountInputBox->setInputBoxWidth(200);
	
	pAccountInputBox->setPosition(ccp(-100,-45));
	//layer->addChild(pAccountInputBox);
	pAccountInputBox->autorelease();

	pPinInputBoxr = new RichTextInputBox();
	pPinInputBoxr->setCharLimit(20);
	pPinInputBoxr->setInputBoxWidth(200);
	
	pPinInputBoxr->setPosition(ccp(-100,-95));
	//layer->addChild(pPinInputBoxr);
	pPinInputBoxr->autorelease();
}

Login::~Login()
{
    //CCHttpClient::getInstance()->destroyInstance();
}

void Login::addBirds(CCNode* parent)
{
	CCSize s = CCDirector::sharedDirector()->getVisibleSize();

	const char* birdAnimFileName = "animation/texiao/changjingtexiao/xianhe/xianhe.anm";

	// birds
	// the head bird
	CCLegendAnimation* pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(ccp(0.5f,0.5f));
	pDoorNode->setPosition(ccp(s.width/2-200, s.height-50));
	pDoorNode->setScale(0.33f);
	parent->addChild(pDoorNode);

	pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(ccp(0.5f,0.5f));
	pDoorNode->setPosition(ccp(s.width/2-213, s.height-42));
	pDoorNode->setScale(0.28f);
	parent->addChild(pDoorNode);

	pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(ccp(0.5f,0.5f));
	pDoorNode->setPosition(ccp(s.width/2-226, s.height-34));
	pDoorNode->setScale(0.26f);
	parent->addChild(pDoorNode);

	pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(ccp(0.5f,0.5f));
	pDoorNode->setPosition(ccp(s.width/2-225, s.height-60));
	pDoorNode->setScale(0.38f);
	parent->addChild(pDoorNode);

	pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(ccp(0.5f,0.5f));
	pDoorNode->setPosition(ccp(s.width/2-250, s.height-70));
	pDoorNode->setScale(0.4f);
	parent->addChild(pDoorNode);
}

Login * Login::getInstance()
{
    if (NULL == s_login)
    {
        s_login = new Login();
    }
    
    return s_login;
}

void Login::onEnter()
{
	CCLayer::onEnter();
    
    // 隐藏悬浮图标
    [[DJPlatform defaultDJPlatform] setDJAssistiveTouchHidden:YES];
    
    // LiuLiang++(登录接口)
    [[DJPlatform defaultDJPlatform] DJLogin];

//	string account = CCUserDefault::sharedUserDefault()->getStringForKey("account");
//	string password = CCUserDefault::sharedUserDefault()->getStringForKey("password");
//	if(NULL == pAccountInputBox->getInputString())
//	{
//		pAccountInputBox->onTextFieldInsertText(NULL, account.c_str(), account.size());
//	}
//	if(NULL == pPinInputBoxr->getInputString())
//	{
//		pPinInputBoxr->onTextFieldInsertText(NULL, password.c_str(), password.size());
//	}
}

void Login::repeatFunc()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();
	CCSprite* pSprite = (CCSprite*)getChildByTag(BACKGROUND_TAG);
	float scaleValue = pSprite->getScale();
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;

	CCMoveTo * moveToActionNext = CCMoveTo::create(BACKGROUND_ROLL_TIME*(widthMultiple+1)/widthMultiple,ccp(-pSprite->getContentSize().width*scaleValue, 0));
	CCMoveTo * moveToReturnNext = CCMoveTo::create(0,ccp(winSize.width-1, 0));
	CCRepeatForever * repeapActionNext = CCRepeatForever::create(CCSequence::create(moveToActionNext,moveToReturnNext,CCDelayTime::create(BACKGROUND_ROLL_TIME*(widthMultiple-1)/widthMultiple),NULL));
	pSprite->runAction(repeapActionNext);
}

void Login::onVersionCheck()
{
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = Login::s_loginserver_ip;
		//std::string url = "192.168.1.122";
        url.append(":6666/versioncheck");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(Login::onVersionCheckCompleted));
        
		VersionCheckReq httpReq;
		httpReq.set_clienttype("");
		httpReq.set_clientversion(GAME_CODE_VERSION);
		CCLOG(GAME_CODE_VERSION);
		int resVersion = CCUserDefault::sharedUserDefault()->getIntegerForKey(KEY_OF_RES_VERSION, GAME_RES_VERSION);
		httpReq.set_resversion(resVersion);
		httpReq.set_channelid(0);
		httpReq.set_batchid(0);
		httpReq.set_recommendid(0);
		httpReq.set_extensionfield("");
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
		httpReq.set_platform("");
#else
		httpReq.set_platform(OPERATION_PLATFORM);
#endif
        
		string msgData;
		httpReq.SerializeToString(&msgData);
        
		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void Login::onVersionCheckCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag()))
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed())
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        
        return;
    }
    
	VersionCheckRsp versionCheckRsp;
	if(response->getResponseData()->size() <= 0)
	{
		versionCheckRsp.set_result(1);   // server no response, so the default value is 1, indicate that there is no update
	}
	else
	{
		//CCAssert(response->getResponseData()->size() > 0, "should not be empty");
		versionCheckRsp.ParseFromString(response->getResponseData()->data());
        
		CCLOG("response.result = %d", versionCheckRsp.result());
	}
    
	int result = versionCheckRsp.result();
	if(1 == result)
	{
		onMenuGetTestClicked();
	}
	else if(2 == result)
	{
		GameView::getInstance()->showAlertDialog("请到当乐下载最新客户端");
	}
}

void Login::onMenuGetTestClicked()
{    
    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = s_loginserver_ip;
        url.append(":6666/serverlist");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestCompleted));

		ServerListReq httpReq;
		httpReq.set_userid(userId);
		httpReq.set_sessionid(sessionId);

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void Login::onHttpRequestCompleted(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	m_pServerListRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pServerListRsp->result());
	/*
	CCLOG("m_pServerListRsp->serverinfos().roleid() = %d",m_pServerListRsp->serverinfos(0).roleid());
	CCLOG("response.serverinfos_size = %d", m_pServerListRsp->serverinfos_size());
	CCLOG("m_pServerListRsp->serverinfos(0).has_roleid() = %d",m_pServerListRsp->serverinfos(0).has_roleid());
	CCLOG("m_pServerListRsp->serverinfos(0).roleid() = %lld\n",m_pServerListRsp->serverinfos(0).roleid());*/
	if(1 == m_pServerListRsp->result())
	{
		GameState* pScene = new LoginState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
	else
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_server_list"));
	}
}

void Login::onMenuGetLoginClicked(cocos2d::CCObject *sender)
{    
//	if(NULL == pAccountInputBox->getInputString())
//	{
//        GameView::getInstance()->showAlertDialog("帐号不能为空");
//		return;
//	}
//	else
//	{
//		CCUserDefault::sharedUserDefault()->setStringForKey("account", pAccountInputBox->getInputString());
//	}
//	if(NULL == pPinInputBoxr->getInputString())
//	{
//        GameView::getInstance()->showAlertDialog("密码不能为空");
//		return;
//	}
//	else
//	{
//		CCUserDefault::sharedUserDefault()->setStringForKey("password", pPinInputBoxr->getInputString());
//	}
//
//    // test 1
//    {
//        CCHttpRequest* request = new CCHttpRequest();
//        std::string url = s_loginserver_ip;
//		url.append(":6666/login");
//        request->setUrl(url.c_str());
//        request->setRequestType(CCHttpRequest::kHttpPost);
//        request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestLogin));
//
//		LoginReq httpReq;
//		httpReq.set_account(pAccountInputBox->getInputString());
//		httpReq.set_authenticid(pPinInputBoxr->getInputString());
//		//useless element
//		httpReq.set_accountnum(0);
//		httpReq.set_accountpt("test");
//		httpReq.set_imei("test");
//		httpReq.set_ua("test");
//		httpReq.set_channelid(LoginReq::QIANLIANG);
//		httpReq.set_batchid(0);
//		httpReq.set_recommendid(0);
//		httpReq.set_systemversion("test");
//		httpReq.set_nettype(0);
//		httpReq.set_apkwithres(0);
//		httpReq.set_platformuserid("test");
//
//		string msgData;
//		httpReq.SerializeToString(&msgData);
//
//		request->setRequestData(msgData.c_str(), msgData.size());
//        request->setTag("POST");
//		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
//        CCHttpClient::getInstance()->send(request);
//        request->release();
//    }
    
    // 隐藏悬浮图标
    [[DJPlatform defaultDJPlatform] setDJAssistiveTouchHidden:YES];
    
    // LiuLiang++(登录接口)
    [[DJPlatform defaultDJPlatform] DJLogin];
}

void Login::onHttpRequestLogin(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	LoginRsp* m_pRegisterRsp = new LoginRsp();
	m_pRegisterRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pRegisterRsp->result());

	if(1 == m_pRegisterRsp->result())
	{
		userId = m_pRegisterRsp->userid();
		sessionId = m_pRegisterRsp->sessionid();
		onVersionCheck();
	}
	else
	{
		GameView::getInstance()->showAlertDialog(m_pRegisterRsp->resultmessage());
	}
}


void Login::onMenuClear(cocos2d::CCObject *sender)
{
	pAccountInputBox->deleteAllInputString();
	pPinInputBoxr->deleteAllInputString();
}

void Login::onMenuGetRegisterClicked(cocos2d::CCObject *sender)
{    
	if(NULL == pAccountInputBox->getInputString())
	{
        GameView::getInstance()->showAlertDialog("帐号不能为空");
		return;
	}
	if(NULL == pPinInputBoxr->getInputString())
	{
        GameView::getInstance()->showAlertDialog("密码不能为空");
		return;
	}
    

    // test 1
    {
        CCHttpRequest* request = new CCHttpRequest();
        std::string url = s_loginserver_ip;
		url.append(":6666/auth_regist");
        request->setUrl(url.c_str());
        request->setRequestType(CCHttpRequest::kHttpPost);
        request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestRegister));

		AuthRegistReq httpReq;
		httpReq.set_account(pAccountInputBox->getInputString());
		httpReq.set_password(pPinInputBoxr->getInputString());

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
        CCHttpClient::getInstance()->send(request);
        request->release();
    }
}

void Login::onHttpRequestRegister(CCHttpClient *sender, CCHttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	AuthRegistRsp* m_pLoginRsp = new AuthRegistRsp();
	m_pLoginRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pLoginRsp->result());

	if(1 == m_pLoginRsp->result())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_register_success"));
	}
	else
	{
		GameView::getInstance()->showAlertDialog(m_pLoginRsp->resultmessage());
	}
}


void Login::getServerListInfo(ServerListRsp* httpRsp)
{
	*httpRsp = *m_pServerListRsp;
}

void runLogin()
{
    CCScene *pScene = CCScene::create();
    Login *pLayer = new Login();
    pScene->addChild(pLayer);
    
    CCDirector::sharedDirector()->replaceScene(pScene);
    pLayer->release();
}

// 此处目前是以91的为例(此处为当乐SDK)
void Login::getLoginInfo(std::string strAccount, std::string strLoginUin, std::string strSessionId)
{
    CCHttpRequest* request = new CCHttpRequest();
    std::string url = s_loginserver_ip;
    url.append(":6666/login");
    request->setUrl(url.c_str());
    request->setRequestType(CCHttpRequest::kHttpPost);
    request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestLogin));
    
    
    const char * charMid = strLoginUin.c_str();
    const char * charToken = strSessionId.c_str();
    
    OepnDLoginReq * pOepnDLoginReq = new OepnDLoginReq();
    pOepnDLoginReq->set_mid(charMid);
    pOepnDLoginReq->set_token(charToken);
    
    LoginReq httpReq;
    //httpReq.set_account(pAccountInputBox->getInputString());
    //httpReq.set_authenticid(pPinInputBoxr->getInputString());
    httpReq.set_account(strAccount);
    httpReq.set_accountnum(0);
    httpReq.set_accountpt("test");
    httpReq.set_imei("test");
    httpReq.set_ua("test");
    httpReq.set_channelid(LoginReq_Channel_OPEND);
    httpReq.set_batchid(0);
    httpReq.set_recommendid(0);
    httpReq.set_systemversion("test");
    httpReq.set_nettype(0);
    httpReq.set_apkwithres(0);
    httpReq.set_platformuserid("test");
    //httpReq.set_allocated_nd91loginreq(nd91LoginReq);
    //httpReq.set_allocated_laohuloginreq(laohuLoginReq);
    httpReq.set_allocated_oepndloginreq(pOepnDLoginReq);
    
    string msgData;
    httpReq.SerializeToString(&msgData);
    
    request->setRequestData(msgData.c_str(), msgData.size());
    request->setTag("POST");
    CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
    CCHttpClient::getInstance()->send(request);
    request->release();
}
