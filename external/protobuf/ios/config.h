/* the location of <hash_set> */
#define HASH_SET_H <ext/hash_set>
#define HASH_MAP_H <ext/hash_map>
#define HASH_NAMESPACE __gnu_cxx
 
/* define if the compiler has hash_map */
//#define HAVE_HASH_MAP 1
 
/* define if the compiler has hash_set */
//#define HAVE_HASH_SET 1
 
#define HAVE_PTHREAD
