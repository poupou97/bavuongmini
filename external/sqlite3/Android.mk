LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_ARM_MODE := arm
LOCAL_CPPFLAGS += -frtti
LOCAL_MODULE_CLASS := STATIC_LIBRARIES
LOCAL_MODULE := libsqlite3_static

LOCAL_SRC_FILES := \
src/sqlite3.c                \

LOCAL_EXPORT_C_INCLUDES := . \
$(LOCAL_PATH)/include \

LOCAL_C_INCLUDES := . \
$(LOCAL_PATH)/include \

include $(BUILD_STATIC_LIBRARY)