#include "LogoState.h"
#include "../gamescene_state/GameSceneState.h"
#include "../loadscene_state/LoadSceneState.h"
#include "../login_state/LoginState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../legend_engine/LegendLevel.h"
#include "../ui/extensions/CCRichLabel.h"
#include "../ui/extensions/RichElement.h"

#include "../utils/GameUtils.h"

#include "cocos2d.h"
#include "cocos-ext.h"

#include "../messageclient/GameMessageProcessor.h"
#include "../messageclient/ClientNetEngine.h"
#include "../ui/extensions/RichTextInput.h"
#include "../gamescene_state/role/GameActorAnimation.h"
#include "../GameView.h"
#include "../legend_engine/GameWorld.h"
#include "../messageclient/element/CMapInfo.h"
#include "../ui/UITest.h"
#include "../ui/UISceneTest.h"
#include "../ui/extensions/UIScene.h"
#include "../legend_engine/CCLegendTiledMap.h"
#include "../utils/pathfinder/AStarTiledMap.h"

#include <cstdio> 
#include "../utils/StaticDataManager.h"

#include "../login_state/Login.h"
#include "../login_state/LoginStateTest.h"
#include "SimpleAudioEngine.h"
#include "../GameUserDefault.h"
#include "../loadres_state/LoadResState.h"

#include "../utils/GameConfig.h"
#include "cocostudio\CCSGUIReader.h"

using namespace CocosDenshion;

USING_NS_CC;
USING_NS_CC_EXT;

/**
* 加载游戏中常驻内存的一些公共资源
*/
void loadCommonRes() {
	GameMetaData::load("level/world");
	CCLOG("world actor class count: %d", GameMetaData::getActorClassCount());

	GameWorld::loadWorldConfigure();

	// load generalsBaseMsg data
	GeneralsConfigData::load("gamestaticdata.db");
	// load generalsEvolution data
	GeneralEvolutionConfigData::load("gamestaticdata.db");
	// load generalsTeach data
	GeneralTeachConfigData::load("gamestaticdata.db");
	// load baseskill data
	StaticDataBaseSkill::load("gamestaticdata.db");
	// load fightskill data
	StaticDataFightSkill::load("gamestaticdata.db");
	// load monster fightskill data
	StaticDataMonsterFightSkill::load("gamestaticdata.db");
	// load fivePersonInstance data
	FivePersonInstanceConfigData::load("gamestaticdata.db");
	// load fightWayBaseData
	FightWayConfigData::load("gamestaticdata.db");
	// load fightWayGrowingUpData
	FightWayGrowingUpConfigData::load("gamestaticdata.db");
	// load fateBaseMsg
	FateBaseMsgConfigData::load("gamestaticdata.db");
	// load drugBaseMsg
	CDrugMsgConfigData::load("gamestaticdata.db");
	//load mapName
	StaticDataMapName::load("gamestaticdata.db");
	//load MusouTalentConfigData
	MusouTalentConfigData::load("gamestaticdata.db");
	//load stone add exp
	StrengthStoneConfigData::load("gamestaticdata.db");
	//load equipment uplevl exp
	StrengthEquipmentConfigData::load("gamestaticdata.db");
	StrengthStarConfigData::load("gamestaticdata.db");
	StrengthGemConfigData::load("gamestaticdata.db");
	MarqueeStringConfigData::load("gamestaticdata.db");
	EquipRefineAddValueConfigData::load("gamestaticdata.db");
	EquipRefineFactorConfigData::load("gamestaticdata.db");
	//load vipConfig data
	VipConfigData::load("gamestaticdata.db");
	//load collectInfoConfig
	CollectInfoConfig::load("gamestaticdata.db");
	//load vipConfig
	VipValueConfig::load("gamestaticdata.db");
	//load vipPriceConfig
	VipPriceConfig::load("gamestaticdata.db");
	//load RechargeConfig
	RechargeConfig::load("gamestaticdata.db");
	//load monster info
	StaticDataMonsterBaseInfo::load("gamestaticdata.db");
	//load GeneralTeachAddedPropertyConfigData
	GeneralTeachAddedPropertyConfigData::load("gamestaticdata.db");
	//load powerSendInfo
	PowerSendConfig::load("gamestaticdata.db");
	//load mergeInfo
	SysthesisItemConfig::load("gamestaticdata.db");

	//加载状态信息
	ExStatusConfigData::load("game.db");
	// 加载技能客户端配置信息
	StaticDataSkillBin::load("game.db");
	RobotPotionMilitData::load("game.db");
	EquipRefineEffectConfigData::load("game.db");
	EquipStarEffectConfigData::load("game.db");
	//load new function remind data
	NewFunctionRemindConfigData::load("game.db");
	//load systemInfo data
	SystemInfoConfigData::load("game.db");
	//load btn openLevel data
	BtnOpenLevelConfigData::load("game.db");
	//load vipDesConfig
	VipDescriptionConfig::load("game.db");
	// load robotDrugMsg
	RobotDrugConfigData::load("game.db");
	// load functionOpenLevelMsg
	FunctionOpenLevelConfigData::load("game.db");
	// load ShortcutSlotOpenLevelMsg
	ShortcutSlotOpenLevelConfigData::load("game.db");
	GeneralsDialogData::load("game.db");
	MonsterDialogData::load("game.db");
	//strength stone level
	EquipStrengthStoneLevelConfigData::load("game.db");

	StringDataManager::load();

	GameConfig::load();
	//singCopy clazz and level 
	SingleCopyClazzConfig::load("gamestaticdata.db");
	SingleCopyLevelConfig::load("gamestaticdata.db");
	//gem property 
	gempropertyConfig::load("gamestaticdata.db");
	//
	RewardListConfig::load("game.db");
	//npc info
	NpcData::load("gamestaticdata.db");
	//get propInfo 
	StaticPropInfoConfig::load("gamestaticdata.db");
}


LogoLayer::LogoLayer()
: m_state(-1)
, m_dStartTime(0)
,m_curState(State_perfect_logo)
{
	////setTouchEnabled(true);
	scheduleUpdate();
    
    Size s = Director::getInstance()->getVisibleSize();
	
	auto background = LayerColor::create(Color4B(255,255,255,255));
	background->setContentSize(s);
	addChild(background, -10);

	Size size=Director::getInstance()->getVisibleSize();

#if defined(CHANNEL_DANGLE_1)
	spLog=Sprite::create("images/wanmeilogo_dl.jpg");
#else
	spLog=Sprite::create("images/wanmeilogo.jpg");
#endif
# 
	spLog->setAnchorPoint(Vec2(0.5f,0.5f));
	spLog->setPosition(Vec2(size.width/2,size.height/2));
	addChild(spLog,1);
	
	auto action =(ActionInterval *)Sequence::create(
		Show::create(),
		DelayTime::create(0.05f),
		CallFuncN::create(CC_CALLBACK_1(LogoLayer::publisherLogoStateCallBack, this)),
		NULL);
	this->runAction(action);

	return;
}

LogoLayer::~LogoLayer()
{
}

void LogoLayer::loadUIWidgetRes()
{
	long long currentTime = 0;
	long deltaTime = 0;

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::MissionSceneLayer = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/renwubb_1.json");
	LoadSceneLayer::MissionSceneLayer->retain();
	deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::TalkWithNpcLayer = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/renwua_1.json");
	LoadSceneLayer::TalkWithNpcLayer->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::chatPanel=(Layout*) cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/liaotian_1.json");
	LoadSceneLayer::chatPanel->retain();
	deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::friendlistPanel= (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/mingan_1.json");
	LoadSceneLayer::friendlistPanel->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::expressPanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/biaoqing_popupo_1.json");
	LoadSceneLayer::expressPanel->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::backPacPanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/daoju_popupo_1.json");
	LoadSceneLayer::backPacPanel->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::mailFriendPopup = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/haoyou_popupo_1.json");
	LoadSceneLayer::mailFriendPopup->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	//currentTime = GameUtils::millisecondNow();
	//LoadSceneLayer::SkillSceneLayer= (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/jineng_1.json");
	//LoadSceneLayer::SkillSceneLayer->retain();
	//	deltaTime = GameUtils::millisecondNow() - currentTime;
	//CCLOG("load UI, cost time: %d", deltaTime);

	//currentTime = GameUtils::millisecondNow();
	//LoadSceneLayer::equipMentPanel=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/zhuangbei_1.json");
	//LoadSceneLayer::equipMentPanel->retain();
	//	deltaTime = GameUtils::millisecondNow() - currentTime;
	//CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::setLayout=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/set_1.json");
	LoadSceneLayer::setLayout->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	//currentTime = GameUtils::millisecondNow();
	//LoadSceneLayer::GeneralsLayer=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/wujiang_1.json");
	//LoadSceneLayer::GeneralsLayer->retain();
	//	deltaTime = GameUtils::millisecondNow() - currentTime;
	//CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::StoreHouseLayer=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/warehouse_1.json");
	LoadSceneLayer::StoreHouseLayer->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::quiryPanel=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/zhuangbeitanchukuang_1.json");
	LoadSceneLayer::quiryPanel->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::familyManagePanel=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/family_application_1.json");//family_1
	LoadSceneLayer::familyManagePanel->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::familyApplyPanel=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/family_1.json");
	LoadSceneLayer::familyApplyPanel->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::instanceDetailLayout=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/instance_1.json");
	LoadSceneLayer::instanceDetailLayout->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::instanceEndLayout=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/instance_end.json");
	LoadSceneLayer::instanceEndLayout->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::GeneralsFateInfoLayer=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/wujiang_detail_popupo_1.json");
	LoadSceneLayer::GeneralsFateInfoLayer->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::GeneralsSkillInfoLayer=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/wujiang_skill_popupo_1.json");
	LoadSceneLayer::GeneralsSkillInfoLayer->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::GeneralsSkillListLayer=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/wujiang_SkillPopupo_1.json");
	LoadSceneLayer::GeneralsSkillListLayer->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::StrategiesUpgradeLayer=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/wujiang_array_popupo_1.json");
	LoadSceneLayer::StrategiesUpgradeLayer->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::OnStrategiesGeneralsInfoLayer=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/wujiang_generals_popupo_1.json");
	LoadSceneLayer::OnStrategiesGeneralsInfoLayer->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::ShopLayer=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/shop_1.json");
	LoadSceneLayer::ShopLayer->retain();
		deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);
	
	//currentTime = GameUtils::millisecondNow();
	//LoadSceneLayer::onCountryPanel=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/loading_map_1.json");
	//LoadSceneLayer::onCountryPanel->retain();
		//deltaTime = GameUtils::millisecondNow() - currentTime;
	//CCLOG("load UI, cost time: %d", deltaTime);

	//currentTime = GameUtils::millisecondNow();
	//LoadSceneLayer::onlineAwardsLayout=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/OnlineAwards_1.json");
	//LoadSceneLayer::onlineAwardsLayout->retain();
	//deltaTime = GameUtils::millisecondNow() - currentTime;
	//CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::activeShopPanel=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/Activity_shop_1.json");
	LoadSceneLayer::activeShopPanel->retain();
	deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::rankLayout=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/Ranking_1.json");
	LoadSceneLayer::rankLayout->retain();
	deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::GoldStoreLayer=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/rmbshop_1.json");
	LoadSceneLayer::GoldStoreLayer->retain();
	deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::BuyCalculatorsLayer=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/BuyCalculators_1.json");
	LoadSceneLayer::BuyCalculatorsLayer->retain();
	deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::StrategiesDetailInfoPopUpLayer = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/all_popupo_1.json");
	LoadSceneLayer::StrategiesDetailInfoPopUpLayer->retain();
	deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::ExtraRewardsLayer = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/ExtraRewards_1.json");
	LoadSceneLayer::ExtraRewardsLayer->retain();
	deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::RankListLayer = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/arena_popupo_1.json");
	LoadSceneLayer::RankListLayer->retain();
	deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);
	
	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::vipDetatilPanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/vip2_1.json");
	LoadSceneLayer::vipDetatilPanel->retain();
	deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::questionLayout = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/answerandquestion_1.json");
	LoadSceneLayer::questionLayout->retain();
	deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);

	currentTime = GameUtils::millisecondNow();
	LoadSceneLayer::RechargePanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/Prepaid_1.json");
	LoadSceneLayer::RechargePanel->retain();
	deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("load UI, cost time: %d", deltaTime);
}

void LogoLayer::TouchesEnded(__Set *pTouches, Event *pEvent)
{
	if (m_curState == State_touchEnable)
	{
#if THREEKINGDOMS_SERVERLIST_MODE
		//runLogin();
#else
		GameState* pScene = new LoginTestState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
#endif
	}


	//layer->setVisible(false);
	//return;

// 	GameView::getInstance()->showPopupWindow("Attachments extracted successfully",2,callfuncO_selector(LogoLayer::menuSendCallback));
// 	return;

	// exit app
	//Director::getInstance()->end();

	//Director::getInstance()->purgeCachedData();

	// create the next scene and run it
    //GameState* pScene = new GameSceneState();
    //if (pScene)
    //{
    //    pScene->runThisState();
    //    pScene->release();
    //}

	//// create the next scene and run it
	//add by yangjun 2013.9.29 am
	//test for art
	/*GameView::getInstance()->getMapInfo()->set_mapid("gd.level");
    GameState* pScene = new LoadSceneState();
    if (pScene)
    {
        pScene->runThisState();
        pScene->release();
    }*/
	
	// create the next scene and run it

	
	//CCLegendAnimation* anim = (CCLegendAnimation*)this->getChildByTag(1);
	//static int actionId = 0;
	//actionId++;
	//if(actionId >= 7)
	//	actionId = 0;
	//anim->setAction(actionId);
}

void LogoLayer::update(float dt)
{
    if(m_curState == State_touchEnable)
    {
        
    }
    else if(m_curState == State_studio_logo)
    {
        
    }
    
	//GameMessageProcessor::sharedMsgProcessor()->socketUpdate();
	static int tick = 0;
	if(m_state == 0)
	{
		//GameMessageProcessor::sharedMsgProcessor()->sendReq(1001, this);

		//ClientNetEngine::sharedSocketEngine()->connect();

		//string data;
		//com::future::nettytest::protocol::CommonMessage comMessage;
		//comMessage.set_cmdid(1001);

		//com::future::nettytest::protocol::MessageFight message2;
		//message2.set_item("item");
		//message2.set_player("player");
		//message2.set_action(1002);
		//string msgData;
		//message2.SerializeToString(&msgData);
		//comMessage.set_data(msgData);

		//comMessage.SerializeToString(&data);
  //  
		//clent->send(comMessage);
		////Sleep(1*1000);
		//clent->send(comMessage);
		////Sleep(1*1000);
		//clent->getSendMsgs()->push(comMessage);
  //  
		////Sleep(1*1000);

		//clent->getSendMsgs()->push(comMessage);

		tick = 0;
		m_state = 1;
	}
	else if(m_state == 1)
	{
		tick++;
		if(tick < 60)
			return;
		//Sleep(5*1000);
		int i = 0;
		//while (i < 10) 
		{
			//com::future::nettytest::protocol::CommonMessage comMessage = ClientNetEngine::sharedSocketEngine()->getMsg();
			//if(comMessage.cmdid() == 0)
			//	return;

			//char name[5];
			//sprintf(name, "%d", comMessage.cmdid());   // message id
			//std::string pushhandlerClassName = "PushHandler";
			//PushHandler1001 *pVar = (PushHandler1001*)CKClassFactory::sharedClassFactory().getClassByName(pushhandlerClassName.append(name));
			//pVar->set(comMessage);
			//delete pVar;

			//cout << i << endl;
			//cout << "CommonMessage11: " << endl;
			//cout << "cmdid: " << comMessage.cmdid() << endl;
			//cout << "data: " << comMessage.data() << endl;
			//com::future::nettytest::protocol::MessagePlayer test;
			//test.ParseFromString(comMessage.data());
			//CCLOG("msg: %d, %s", comMessage.cmdid(), comMessage.data().c_str());

			//cout << "message11: " << endl;
			//cout << "from: " << test.from() << endl;
			//cout << "to: " << test.to() << endl;
			//cout << "keycode: " << test.keycode() << endl;
			//CCLOG("key: %s, %s, %d", test.from().c_str(), test.to().c_str(), test.keycode());
   //     
			////Sleep(1*1000);
			//i++;

			//char msg[100];
			//sprintf(msg, "msg: %d, %s; key: %s, %s, %d", comMessage.cmdid(), comMessage.data().c_str(), test.from().c_str(), test.to().c_str(), test.keycode());
			//Label *pLabel = Label::createWithTTF(msg, "Marker Felt", 15);
			//static int index = 0;
			//pLabel->setPosition(Vec2(10, 480 - index*30));
			//pLabel->setAnchorPoint(Vec2::ZERO);
			//addChild(pLabel);

			//index++;
		}

		m_state = 5;

    
		//sleep(100000);
	//    return 0;
	}
	else if(m_state == 5)
	{
		// do nothing
	}
}

void LogoLayer::getUserDefault()
{
	// sound
    SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(UserDefault::getInstance()->getIntegerForKey(MUSIC, 50)/100.0f);
    SimpleAudioEngine::getInstance()->setEffectsVolume(UserDefault::getInstance()->getIntegerForKey(SOUND, 50)/100.0f);
}

void LogoLayer::publisherLogoStateCallBack( Ref * obj )
{
	long long currentTime = GameUtils::millisecondNow();
	loadCommonRes();
	long deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("loadCommonRes(), cost time: %d", deltaTime);

	getUserDefault();

	spLog->removeFromParentAndCleanup(true);

	// next state is studio logo
	Size size=Director::getInstance()->getVisibleSize();
	Sprite * workRoomLogo=Sprite::create("images/quzhonglogo.jpg");
	workRoomLogo->setAnchorPoint(Vec2(0.5f,0.5f));
	workRoomLogo->setPosition(Vec2(size.width/2,size.height/2));
	addChild(workRoomLogo,1);
	//m_curState=State_touchEnable;

	auto action =(ActionInterval *)Sequence::create(
		Show::create(),
		DelayTime::create(0.05f),
		CallFuncN::create(CC_CALLBACK_1(LogoLayer::studioLogoStateCallBack, this)),
		NULL);
	this->runAction(action);
}

void LogoLayer::studioLogoStateCallBack( Ref * obj )
{
	long long currentTime = GameUtils::millisecondNow();
	loadUIWidgetRes();
	long deltaTime = GameUtils::millisecondNow() - currentTime;
	CCLOG("loadUIWidgetRes(), cost time: %d", deltaTime);
	/*
	GameState* pScene = new LoadResState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}*/
	//runLogin();
	auto pScene = new LoginGameState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}
}

/////////////////////////////////////////////////////////////////////////

LogoState::~LogoState()
{
}

void LogoState::runThisState()
{
    /*Layer* pLayer = new LogoLayer();
    addChild(pLayer, 0, 1);

	if(Director::getInstance()->getRunningScene() != NULL)
		Director::getInstance()->replaceScene(this);
	else
		Director::getInstance()->runWithScene(this);
    pLayer->release();
	*/
	auto pLayer = new LogoLayer();
    addChild(pLayer);

    Director::getInstance()->replaceScene(this);
    pLayer->release();
}







