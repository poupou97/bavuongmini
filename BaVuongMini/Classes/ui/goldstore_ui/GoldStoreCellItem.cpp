#include "GoldStoreCellItem.h"
#include "../../messageclient/element/CLableGoods.h"
#include "../extensions/CCMoveableMenu.h"
#include "GoodsItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "GoldStoreItemBuyInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "AppMacros.h"
#include "../../utils/GameUtils.h"
#include "../../ui/backpackscene/EquipmentItem.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"

GoldStoreCellItem::GoldStoreCellItem()
{
}


GoldStoreCellItem::~GoldStoreCellItem()
{
	delete curLableGoods;
}

GoldStoreCellItem* GoldStoreCellItem::create( CLableGoods * lableGoods )
{
	auto goldStoreCellItem = new GoldStoreCellItem();
	if (goldStoreCellItem && goldStoreCellItem->init(lableGoods))
	{
		goldStoreCellItem->autorelease();
		return goldStoreCellItem;
	}
	CC_SAFE_DELETE(goldStoreCellItem);
	return NULL;
}

bool GoldStoreCellItem::init( CLableGoods * lableGoods )
{
	if (UIScene::init())
	{
		/////////////////////////////////////////////////////////////////////////////////////

		// �Layer�λ�TableView�֮�ڣ�Ϊ����ȷ��ӦTableView���϶��¼������Ϊ���̵����¼
		//m_pLayer->setSwallowsTouches(false);

		curLableGoods = new CLableGoods();
		curLableGoods->CopyFrom(*lableGoods);
		auto imageView_frame = ImageView::create();
		imageView_frame->loadTexture("res_ui/kuang01_new.png");
		imageView_frame->setScale9Enabled(true);
		imageView_frame->setContentSize(Size(203,115));
		imageView_frame->setCapInsets(Rect(15,30,1,1));
		imageView_frame->setRotation(180);
		imageView_frame->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_frame->setPosition(Vec2(203/2,115/2));
		m_pLayer->addChild(imageView_frame);

		l_name = Label::createWithTTF(lableGoods->goods().name().c_str(), APP_FONT_NAME, 18);
		l_name->enableOutline(Color4B::BLACK, 2.0f);
		l_name->setAnchorPoint(Vec2(0.5f,0.5f));
		l_name->setPosition(Vec2(103,96));
		l_name->setColor(GameView::getInstance()->getGoodsColorByQuality(lableGoods->goods().quality()));
		m_pLayer->addChild(l_name);

		//�ԭ� 
		char s_oldPrice[20];
		sprintf(s_oldPrice,"%d",lableGoods->goods().price());
		l_oldPrice_value = Label::createWithTTF(s_oldPrice, APP_FONT_NAME, 16);
		l_oldPrice_value->enableOutline(Color4B::BLACK, 2.0f);
		l_oldPrice_value->setAnchorPoint(Vec2(0,0.5f));
		l_oldPrice_value->setPosition(Vec2(144,53));
		m_pLayer->addChild(l_oldPrice_value);
	
		const char *str1 = StringDataManager::getString("goldStore_oldPrice");
		l_oldPrice = Label::createWithTTF(str1, APP_FONT_NAME, 16);
		l_oldPrice->enableOutline(Color4B::BLACK, 2.0f);
		l_oldPrice->setAnchorPoint(Vec2(0.5f,0.5f));
		l_oldPrice->setPosition(Vec2(-41,0));
		l_oldPrice_value->addChild(l_oldPrice);
		imageView_oldPrice_goldIngold = ImageView::create();
		imageView_oldPrice_goldIngold->loadTexture("res_ui/ingot.png");
		imageView_oldPrice_goldIngold->setAnchorPoint(Vec2(0,0.5f));
		imageView_oldPrice_goldIngold->setPosition(Vec2(-25,0));
		l_oldPrice_value->addChild(imageView_oldPrice_goldIngold);
		//ۺ��
		imageView_redLine = ImageView::create();
		imageView_redLine->loadTexture("res_ui/redgang.png");
		imageView_redLine->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_redLine->setPosition(Vec2(130,53));
		imageView_redLine->setScaleX(1.6f);
		m_pLayer->addChild(imageView_redLine);

		//��ּ
		char s_newPrice[20];
		sprintf(s_newPrice,"%d",lableGoods->discount());
		l_newPrice_value = Label::createWithTTF(s_newPrice, APP_FONT_NAME, 16);
		l_newPrice_value->enableOutline(Color4B::BLACK, 2.0f);
		l_newPrice_value->setAnchorPoint(Vec2(0,0.5f));
		l_newPrice_value->setPosition(Vec2(144,69));
		m_pLayer->addChild(l_newPrice_value);

		const char *str2 = StringDataManager::getString("goldStore_newPrice");
		l_newPrice = Label::createWithTTF(str2, APP_FONT_NAME, 16);
		l_newPrice->enableOutline(Color4B::BLACK, 2.0f);
		l_newPrice->setAnchorPoint(Vec2(0.5f,0.5f));
		l_newPrice->setPosition(Vec2(-41,0));
		l_newPrice_value->addChild(l_newPrice);
		imageView_newPrice_goldIngold = ImageView::create();
		imageView_newPrice_goldIngold->loadTexture("res_ui/ingot.png");
		imageView_newPrice_goldIngold->setAnchorPoint(Vec2(0,0.5f));
		imageView_newPrice_goldIngold->setPosition(Vec2(-25,0));
		l_newPrice_value->addChild(imageView_newPrice_goldIngold);

		ImageView_hot = ImageView::create();
		ImageView_hot->loadTexture("res_ui/rmbshop/hot.png");
		ImageView_hot->setAnchorPoint(Vec2(0.5f,0.5f));
		ImageView_hot->setPosition(Vec2(192,100));
		m_pLayer->addChild(ImageView_hot);
		ImageView_hot->setVisible(true);

		//ۼ۸
		char s_nowPrice[20];
		sprintf(s_nowPrice,"%d",lableGoods->goods().price());
		l_price_value = Label::createWithTTF(s_nowPrice, APP_FONT_NAME, 16);
		l_price_value->enableOutline(Color4B::BLACK, 2.0f);
		l_price_value->setAnchorPoint(Vec2(0,0.5f));
		l_price_value->setPosition(Vec2(144,61));
		m_pLayer->addChild(l_price_value);

		const char *str3 = StringDataManager::getString("goldStore_price");
		l_price = Label::createWithTTF(str3, APP_FONT_NAME, 16);
		l_price->enableOutline(Color4B::BLACK, 2.0f);
		l_price->setAnchorPoint(Vec2(0.5f,0.5f));
		l_price->setPosition(Vec2(-41,0));
		l_price_value->addChild(l_price);

		imageView_price_goldIngold = ImageView::create();
		imageView_price_goldIngold->loadTexture("res_ui/ingot.png");
		imageView_price_goldIngold->setAnchorPoint(Vec2(0,0.5f));
		imageView_price_goldIngold->setPosition(Vec2(-25,0));
		l_price_value->addChild(imageView_price_goldIngold);

		if (lableGoods->isbind())  //�
		{
			imageView_oldPrice_goldIngold->loadTexture("res_ui/ingotBind.png");
			imageView_newPrice_goldIngold->loadTexture("res_ui/ingotBind.png");
			imageView_price_goldIngold->loadTexture("res_ui/ingotBind.png");
		}
		else
		{
			imageView_oldPrice_goldIngold->loadTexture("res_ui/ingot.png");
			imageView_newPrice_goldIngold->loadTexture("res_ui/ingot.png");
			imageView_price_goldIngold->loadTexture("res_ui/ingot.png");
		}

		if (lableGoods->ishot())
		{
			//�ԭ� 
			l_oldPrice_value->setVisible(true);
			l_oldPrice->setVisible(true);
			imageView_oldPrice_goldIngold->setVisible(true);
			//ۺ��
			imageView_redLine->setVisible(true);

			//��ּ
			l_newPrice_value->setVisible(true);
			l_newPrice->setVisible(true);
			imageView_newPrice_goldIngold->setVisible(true);

			ImageView_hot->setVisible(true);


			l_price_value->setVisible(false);
			l_price->setVisible(false);
			imageView_price_goldIngold->setVisible(false);
		}
		else
		{
			//�ԭ� 
			l_oldPrice_value->setVisible(false);
			l_oldPrice->setVisible(false);
			imageView_oldPrice_goldIngold->setVisible(false);
			//ۺ��
			imageView_redLine->setVisible(false);

			//��ּ
			l_newPrice_value->setVisible(false);
			l_newPrice->setVisible(false);
			imageView_newPrice_goldIngold->setVisible(false);

			ImageView_hot->setVisible(false);


			//ۼ۸
			l_price_value->setVisible(true);
			l_price->setVisible(true);
			imageView_price_goldIngold->setVisible(true);
		}

		/*********************��ж�װ������ɫ***************************/
		std::string frameColorPath;
		if (lableGoods->goods().quality() == 1)
		{
			frameColorPath = EquipWhiteFramePath;
		}
		else if (lableGoods->goods().quality() == 2)
		{
			frameColorPath = EquipGreenFramePath;
		}
		else if (lableGoods->goods().quality() == 3)
		{
			frameColorPath = EquipBlueFramePath;
		}
		else if (lableGoods->goods().quality() == 4)
		{
			frameColorPath = EquipPurpleFramePath;
		}
		else if (lableGoods->goods().quality() == 5)
		{
			frameColorPath = EquipOrangeFramePath;
		}
		else
		{
			frameColorPath = EquipVoidFramePath;
		}

		Btn_goodsItemFrame = Button::create();
		Btn_goodsItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_goodsItemFrame->setTouchEnabled(true);
		Btn_goodsItemFrame->setAnchorPoint(Vec2(.5f,.5f));
		Btn_goodsItemFrame->setPosition(Vec2(17+Btn_goodsItemFrame->getContentSize().width/2,17+Btn_goodsItemFrame->getContentSize().height/2));
		Btn_goodsItemFrame->setScale(0.9f);
		Btn_goodsItemFrame->addTouchEventListener(CC_CALLBACK_2(GoldStoreCellItem::GoodItemEvent, this));
		Btn_goodsItemFrame->setPressedActionEnabled(true);
		m_pLayer->addChild(Btn_goodsItemFrame);

		uiiImageView_goodsItem = ImageView::create();
		if (strcmp(lableGoods->goods().icon().c_str(),"") == 0)
		{
			uiiImageView_goodsItem->loadTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(lableGoods->goods().icon().c_str());
			_path.append(".png");
			uiiImageView_goodsItem->loadTexture(_path.c_str());
		}
		uiiImageView_goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		uiiImageView_goodsItem->setPosition(Vec2(0,0));
		Btn_goodsItemFrame->addChild(uiiImageView_goodsItem);

		//����ť
		auto btn_buy  = Button::create();
		btn_buy->loadTextures("res_ui/new_button_6.png","res_ui/new_button_6.png","");
		btn_buy->setTouchEnabled(true);
		btn_buy->setPressedActionEnabled(true);
		//btn_buy->setScale9Enabled(true);
		//btn_buy->setContentSize(Size(80,40));
		//btn_buy->setCapInsets(Rect(18,9,2,23));
		btn_buy->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_buy->setPosition(Vec2(131,25));
		btn_buy->addTouchEventListener(CC_CALLBACK_2(GoldStoreCellItem::BuyEvent, this));
		m_pLayer->addChild(btn_buy);

		const char *str4 = StringDataManager::getString("goods_auction_buy");
		//const char *str4 = "123";
		auto l_buy = Label::createWithTTF(str4, APP_FONT_NAME, 20);
		l_buy->setAnchorPoint(Vec2(0.5f,0.5f));
		l_buy->setPosition(Vec2(0,0));
		btn_buy->addChild(l_buy);

		this->setContentSize(Size(203,115));

		return true;
	}
	return false;
}

void GoldStoreCellItem::BuyEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreItemBuyInfo) == NULL)
		{
			auto size = Director::getInstance()->getVisibleSize();
			auto goldStoreItemBuyInfo = GoldStoreItemBuyInfo::create(curLableGoods);
			goldStoreItemBuyInfo->setIgnoreAnchorPointForPosition(false);
			goldStoreItemBuyInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
			goldStoreItemBuyInfo->setPosition(Vec2(size.width / 2, size.height / 2));
			goldStoreItemBuyInfo->setTag(kTagGoldStoreItemBuyInfo);
			GameView::getInstance()->getMainUIScene()->addChild(goldStoreItemBuyInfo);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GoldStoreCellItem::GoodItemEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase) == NULL)
		{
			auto size = Director::getInstance()->getVisibleSize();
			auto curGoods = new GoodsInfo();
			curGoods->CopyFrom(curLableGoods->goods());
			auto goodsItemInfoBase = GoodsItemInfoBase::create(curGoods, GameView::getInstance()->EquipListItem, 0);
			goodsItemInfoBase->setIgnoreAnchorPointForPosition(false);
			goodsItemInfoBase->setAnchorPoint(Vec2(0.5f, 0.5f));
			//goodsItemInfoBase->setPosition(Vec2(size.width/2,size.height/2));
			goodsItemInfoBase->setTag(kTagGoodsItemInfoBase);
			GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
			delete curGoods;
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GoldStoreCellItem::refreshUI( CLableGoods * lableGoods )
{
	curLableGoods->CopyFrom(*lableGoods);

	l_name->setString(lableGoods->goods().name().c_str());
	l_name->setColor(GameView::getInstance()->getGoodsColorByQuality(lableGoods->goods().quality()));

	if (lableGoods->isbind())  //�
	{
		imageView_oldPrice_goldIngold->loadTexture("res_ui/ingotBind.png");
		imageView_newPrice_goldIngold->loadTexture("res_ui/ingotBind.png");
		imageView_price_goldIngold->loadTexture("res_ui/ingotBind.png");
	}
	else
	{
		imageView_oldPrice_goldIngold->loadTexture("res_ui/ingot.png");
		imageView_newPrice_goldIngold->loadTexture("res_ui/ingot.png");
		imageView_price_goldIngold->loadTexture("res_ui/ingot.png");
	}

	if (lableGoods->ishot())
	{
		//�ԭ� 
		char s_oldPrice[20];
		sprintf(s_oldPrice,"%d",lableGoods->goods().price());
		l_oldPrice_value->setString(s_oldPrice);
		const char *str1 = StringDataManager::getString("goldStore_oldPrice");
		l_oldPrice->setString(str1);

		//��ּ
		char s_newPrice[20];
		sprintf(s_newPrice,"%d",lableGoods->discount());
		l_newPrice_value->setString(s_newPrice);
		const char *str2 = StringDataManager::getString("goldStore_newPrice");
		l_newPrice->setString(str2);

		//�ԭ� 
		l_oldPrice_value->setVisible(true);
		l_oldPrice->setVisible(true);
		imageView_oldPrice_goldIngold->setVisible(true);
		//ۺ��
		imageView_redLine->setVisible(true);

		//��ּ
		l_newPrice_value->setVisible(true);
		l_newPrice->setVisible(true);
		imageView_newPrice_goldIngold->setVisible(true);

		ImageView_hot->setVisible(true);


		l_price_value->setVisible(false);
		l_price->setVisible(false);
		imageView_price_goldIngold->setVisible(false);
	}
	else
	{
		//ۼ۸
		char s_nowPrice[20];
		sprintf(s_nowPrice,"%d",lableGoods->goods().price());
		l_price_value->setString(s_nowPrice);
		const char *str3 = StringDataManager::getString("goldStore_price");
		l_price->setString(str3);

		//�ԭ� 
		l_oldPrice_value->setVisible(false);
		l_oldPrice->setVisible(false);
		imageView_oldPrice_goldIngold->setVisible(false);
		//ۺ��
		imageView_redLine->setVisible(false);

		//��ּ
		l_newPrice_value->setVisible(false);
		l_newPrice->setVisible(false);
		imageView_newPrice_goldIngold->setVisible(false);

		ImageView_hot->setVisible(false);


		//ۼ۸
		l_price_value->setVisible(true);
		l_price->setVisible(true);
		imageView_price_goldIngold->setVisible(true);
	}

	/*********************��ж�װ������ɫ***************************/
	std::string frameColorPath;
	if (lableGoods->goods().quality() == 1)
	{
		frameColorPath = EquipWhiteFramePath;
	}
	else if (lableGoods->goods().quality() == 2)
	{
		frameColorPath = EquipGreenFramePath;
	}
	else if (lableGoods->goods().quality() == 3)
	{
		frameColorPath = EquipBlueFramePath;
	}
	else if (lableGoods->goods().quality() == 4)
	{
		frameColorPath = EquipPurpleFramePath;
	}
	else if (lableGoods->goods().quality() == 5)
	{
		frameColorPath = EquipOrangeFramePath;
	}
	else
	{
		frameColorPath = EquipVoidFramePath;
	}

	Btn_goodsItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");

	if (strcmp(lableGoods->goods().icon().c_str(),"") == 0)
	{
		uiiImageView_goodsItem->loadTexture("res_ui/props_icon/prop.png");
	}
	else
	{
		std::string _path = "res_ui/props_icon/";
		_path.append(lableGoods->goods().icon().c_str());
		_path.append(".png");
		uiiImageView_goodsItem->loadTexture(_path.c_str());
	}
}
