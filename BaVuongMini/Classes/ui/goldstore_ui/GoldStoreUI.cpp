#include "GoldStoreUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "../extensions/UITab.h"
#include "../../messageclient/element/CLable.h"
#include "GoldStoreCellItem.h"
#include "../../messageclient/element/CLableGoods.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GoldStoreMarquee.h"
#include "../../utils/StrUtils.h"
#include "../vip_ui/VipDetailUI.h"
#include "../vip_ui/VipEveryDayRewardsUI.h"
#include "../vip_ui/FirstBuyVipUI.h"
#include "../vip_ui/VipData.h"
#include "../recharge_ui/Recharge.h"
#include "../../utils/GameConfig.h"

#define MARQUEE_LABLE_WIDTH 150
#define MARQUEE_LABEL_HEIGHT 72
#define GOLD_STORE_MAX_FILTER 6

#define kTag_CellItem_Left 236
#define kTag_CellItem_Right 237

#define kTag_MARQUEE 300

GoldStoreUI::GoldStoreUI():
curLableId(0),
m_strMarquee("")
{
}


GoldStoreUI::~GoldStoreUI()
{
	std::vector<GoodsInfo *>::iterator iterGoodsInfo;
	for (iterGoodsInfo = m_vector_goodsInfo.begin(); iterGoodsInfo != m_vector_goodsInfo.end(); iterGoodsInfo++)
	{
		delete *iterGoodsInfo;
	}
	m_vector_goodsInfo.clear();
}

GoldStoreUI* GoldStoreUI::create()
{
	auto goldStoreUI = new GoldStoreUI();
	if (goldStoreUI && goldStoreUI->init())
	{
		goldStoreUI->autorelease();
		return goldStoreUI;
	}
	CC_SAFE_DELETE(goldStoreUI);
	return goldStoreUI;
}

bool GoldStoreUI::init()
{
	if (UIScene::init())
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(800, 480));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		//���UI
		if(LoadSceneLayer::GoldStoreLayer->getParent() != NULL)
		{
			LoadSceneLayer::GoldStoreLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::GoldStoreLayer;
		ppanel->getVirtualRenderer()->setContentSize(Size(800, 480));
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		const char * firstStr = StringDataManager::getString("UIName_yuan");
		const char * secondStr = StringDataManager::getString("UIName_bao");
		const char * thirdStr = StringDataManager::getString("UIName_shang");
		const char * fourthStr = StringDataManager::getString("UIName_cheng");
		auto atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,fourthStr);
		atmature->setPosition(Vec2(30,240));
		u_layer->addChild(atmature);

		//عرհ�ť
		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(GoldStoreUI::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);

		Button_recharge = (Button*)Helper::seekWidgetByName(ppanel,"Button_recharge");
		Button_recharge->setTouchEnabled(true);
		Button_recharge->addTouchEventListener(CC_CALLBACK_2(GoldStoreUI::RechargeEvent, this));
		Button_recharge->setPressedActionEnabled(true);

		Button_vip = (Button*)Helper::seekWidgetByName(ppanel,"Button_vip");
		Button_vip->setTouchEnabled(true);
		Button_vip->addTouchEventListener(CC_CALLBACK_2(GoldStoreUI::VipEvent, this));
		Button_vip->setPressedActionEnabled(true);

		// vip���ؿ��
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (vipEnabled)
		{
			Button_vip->setVisible(true);
		}
		else
		{
			Button_vip->setVisible(false);
		}

		ImageView_inGotPart = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_lv6di");
		ImageView_inGotPart->setVisible(true);
		ImageView_bindInGotPart = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_lv6di_1");
		ImageView_bindInGotPart->setVisible(false);

		l_ingot_value = (Text*)Helper::seekWidgetByName(ppanel,"Label_IngotValue");
		char str_ingot[20];
		sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
		l_ingot_value->setString(str_ingot);

		l_bindIngot_value = (Text*)Helper::seekWidgetByName(ppanel,"Label_BindIngotValue");
		char str_bindingot[20];
		sprintf(str_bindingot,"%d",GameView::getInstance()->getPlayerBindGoldIngot());
		l_bindIngot_value->setString(str_bindingot);

		// �С��
		m_imageView_horn = (ImageView *)Helper::seekWidgetByName(ppanel, "ImageView_horn");
		m_imageView_horn->setVisible(true);

		const char *function_type_str1 = StringDataManager::getString("goldStore_tab_type_1");
		char* function_type_p1 =const_cast<char*>(function_type_str1);
		const char *function_type_str2 = StringDataManager::getString("goldStore_tab_type_2");
		char* function_type_p2 =const_cast<char*>(function_type_str2);
		char * mainNames[] ={function_type_p2,function_type_p1};
		tab_main  = UITab::createWithText(2,"res_ui/tab_3_off.png","res_ui/tab_3_on.png","",mainNames,HORIZONTAL,5);
		tab_main->setAnchorPoint(Vec2(0,0));
		tab_main->setPosition(Vec2(66,408));
		tab_main->setHighLightImage("res_ui/tab_3_on.png");
		tab_main->setDefaultPanelByIndex(0);
		tab_main->addIndexChangedEvent(this,coco_indexchangedselector(GoldStoreUI::FunctionTypeIndexChangedEvent));
		tab_main->setPressedActionEnabled(true);
		u_layer->addChild(tab_main);

// 		int _num = GameView::getInstance()->goldStoreList.size();
// 		const char * normalImage = "res_ui/tab_s_off.png";
// 		const char * selectImage = "res_ui/tab_s.png";
// 		const char * finalImage = "";
// 		const char * highLightImage = "res_ui/tab_s.png";
// 		CCAssert(_num <= GOLD_STORE_MAX_FILTER, "out of range");
// 		char * ItemName[GOLD_STORE_MAX_FILTER];
// 		for (int i =0;i<_num;++i)
// 		{
// 			std::string imagePath = "res_ui/rmbshop/";
// 			imagePath.append(GameView::getInstance()->goldStoreList.at(i)->name().c_str());
// 
// 			ItemName[i] = new char [50];
// 			strcpy(ItemName[i],imagePath.c_str());
// 		}
// 		if (_num > 0)
// 		{
// 			tab_function_type  = UITab::createWithImage(_num,normalImage,selectImage,finalImage,ItemName,VERTICAL,-10);
// 			tab_function_type->setAnchorPoint(Vec2(0,0));
// 			tab_function_type->setPosition(Vec2(760,420));
// 			tab_function_type->setHighLightImage((char * )highLightImage);
// 			tab_function_type->setDefaultPanelByIndex(0);
// 			tab_function_type->addIndexChangedEvent(this,coco_indexchangedselector(GoldStoreUI::IndexChangedEvent));
// 			tab_function_type->setPressedActionEnabled(true);
// 			u_layer->addChild(tab_function_type);
// 		}
// 		
// 		for (int i = 0;i<_num;i++)
// 		{
// 			delete ItemName[i];
// 		}

		m_tableView = TableView::create(this,Size(449,317));
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0,0));
		m_tableView->setPosition(Vec2(284,81));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		u_layer->addChild(m_tableView);

		refreshTypeTabByIndex(0);

		//this->scheduleUpdate();

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(GoldStoreUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(GoldStoreUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(GoldStoreUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(GoldStoreUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void GoldStoreUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	//add new data
//	if (tab_main->getCurrentIndex() == 0)
//	{
// 		if (GameView::getInstance()->goldStoreList.size() > 0)
// 		{
// 			for (int i = 0;i<GameView::getInstance()->goldStoreList.at(0)->commdity_size();++i)
// 			{
// 				auto _LableGoods = new CLableGoods();
// 				_LableGoods->CopyFrom(GameView::getInstance()->goldStoreList.at(0)->commdity(i));
// 				DataSource.push_back(_LableGoods);
// 			}
// 		}
//	}
//	else
//	{
// 		if (GameView::getInstance()->bingGoldStoreList.size() > 0)
// 		{
// 			for (int i = 0;i<GameView::getInstance()->bingGoldStoreList.at(0)->commdity_size();++i)
// 			{
// 				auto _LableGoods = new CLableGoods();
// 				_LableGoods->CopyFrom(GameView::getInstance()->bingGoldStoreList.at(0)->commdity(i));
// 				DataSource.push_back(_LableGoods);
// 			}
// 		}
//	}

//	m_tableView->reloadData();
}

void GoldStoreUI::onExit()
{
	UIScene::onExit();
}

bool GoldStoreUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void GoldStoreUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void GoldStoreUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void GoldStoreUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void GoldStoreUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void GoldStoreUI::RechargeEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		// ȳ�ֵ���ƿ��
		bool bChargeEnabled = GameConfig::getBoolForKey("charge_enable");
		if (true == bChargeEnabled)
		{
			auto winSize = Director::getInstance()->getVisibleSize();
			auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
			auto rechargeScene = (Layer*)mainscene->getChildByTag(kTagRechargeUI);
			if (rechargeScene == NULL)
			{
				auto rechargeUI = RechargeUI::create();
				mainscene->addChild(rechargeUI, 0, kTagRechargeUI);
				rechargeUI->setIgnoreAnchorPointForPosition(false);
				rechargeUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				rechargeUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			}

			//GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillui_nothavefuction"));
		}
		else
		{
			const char * charChargeEnabled = StringDataManager::getString("RechargeUI_disable");
			GameView::getInstance()->showAlertDialog(charChargeEnabled);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GoldStoreUI::VipEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto winSize = Director::getInstance()->getVisibleSize();
		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		auto vipScene = (Layer*)mainscene->getChildByTag(kTagVipDetailUI);
		if (vipScene == NULL)
		{
			if (VipData::vipDetailUI)
			{
				auto vipDetailUI = (VipDetailUI*)VipData::vipDetailUI;
				mainscene->addChild(vipDetailUI, 0, kTagVipDetailUI);
				vipDetailUI->setIgnoreAnchorPointForPosition(false);
				vipDetailUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				vipDetailUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				vipDetailUI->setToDefault();
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GoldStoreUI::FunctionTypeIndexChangedEvent(Ref* pSender)
{
	if (tab_main->getCurrentIndex() == 0)
	{
		refreshTypeTabByIndex(0);
	}
	else
	{
		refreshTypeTabByIndex(1);
	}
}

void GoldStoreUI::IndexChangedEvent( Ref* pSender )
{
	tab_function_type->setHightLightLabelColor(Color3B(47,93,13));
	tab_function_type->setNormalLabelColor(Color3B(255,255,255));

	//delete old data
	DataSource.erase(DataSource.begin(),DataSource.end());
	std::vector<CLableGoods*>::iterator iter;
	for (iter = DataSource.begin(); iter != DataSource.end(); ++iter)
	{
		delete *iter;
	}
	DataSource.clear();

	if (tab_main->getCurrentIndex() == 1)
	{
		int idx = tab_function_type->getCurrentIndex();
		curLableId = GameView::getInstance()->bingGoldStoreList.at(idx)->id();

		//add new data
		for (int i = 0;i<GameView::getInstance()->bingGoldStoreList.at(idx)->commdity_size();++i)
		{
			auto _LableGoods = new CLableGoods();
			_LableGoods->CopyFrom(GameView::getInstance()->bingGoldStoreList.at(idx)->commdity(i));
			DataSource.push_back(_LableGoods);
		}
	}
	else
	{
		int idx = tab_function_type->getCurrentIndex();
		curLableId = GameView::getInstance()->goldStoreList.at(idx)->id();

		//add new data
		for (int i = 0;i<GameView::getInstance()->goldStoreList.at(idx)->commdity_size();++i)
		{
			auto _LableGoods = new CLableGoods();
			_LableGoods->CopyFrom(GameView::getInstance()->goldStoreList.at(idx)->commdity(i));
			DataSource.push_back(_LableGoods);
		}
	}

	this->m_tableView->reloadData();
}

void GoldStoreUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void GoldStoreUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void GoldStoreUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
}

cocos2d::Size GoldStoreUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(444,120);
}

cocos2d::extension::TableViewCell* GoldStoreUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called
	if (!cell)
	{
		cell = new TableViewCell();
		cell->autorelease();

		if (idx == DataSource.size()/2)
		{
			if (DataSource.size()%2 == 0)
			{
				auto cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
				cellItem1->setPosition(Vec2(15,0));
				cellItem1->setTag(kTag_CellItem_Left);
				cell->addChild(cellItem1);

				auto cellItem2 = GoldStoreCellItem::create(DataSource.at(2*idx+1));
				cellItem2->setPosition(Vec2(226,0));
				cellItem2->setTag(kTag_CellItem_Right);
				cell->addChild(cellItem2);
			}
			else
			{
				auto cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
				cellItem1->setPosition(Vec2(15,0));
				cellItem1->setTag(kTag_CellItem_Left);
				cell->addChild(cellItem1);
			}
		}
		else
		{
			auto cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
			cellItem1->setPosition(Vec2(15,0));
			cellItem1->setTag(kTag_CellItem_Left);
			cell->addChild(cellItem1);

			auto cellItem2 = GoldStoreCellItem::create(DataSource.at(2*idx+1));
			cellItem2->setPosition(Vec2(226,0));
			cellItem2->setTag(kTag_CellItem_Right);
			cell->addChild(cellItem2);
		}
	}
	else
	{
		auto cellItem1 = dynamic_cast<GoldStoreCellItem*>(cell->getChildByTag(kTag_CellItem_Left));
		auto cellItem2 = dynamic_cast<GoldStoreCellItem*>(cell->getChildByTag(kTag_CellItem_Right));
		if (idx == DataSource.size()/2)
		{
			if (DataSource.size()%2 == 0)
			{
				if (cellItem1)
				{
					cellItem1->refreshUI(DataSource.at(2*idx));
				}
				else
				{
					auto cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
					cellItem1->setPosition(Vec2(15,0));
					cellItem1->setTag(kTag_CellItem_Left);
					cell->addChild(cellItem1);
				}

				if (cellItem2)
				{
					cellItem2->refreshUI(DataSource.at(2*idx+1));
				}
				else
				{
					auto cellItem2 = GoldStoreCellItem::create(DataSource.at(2*idx+1));
					cellItem2->setPosition(Vec2(226,0));
					cellItem2->setTag(kTag_CellItem_Right);
					cell->addChild(cellItem2);
				}
			}
			else
			{
				if (cellItem1)
				{
					cellItem1->refreshUI(DataSource.at(2*idx));
				}
				else
				{
					auto cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
					cellItem1->setPosition(Vec2(15,0));
					cellItem1->setTag(kTag_CellItem_Left);
					cell->addChild(cellItem1);
				}

				if (cellItem2)
				{
					cellItem2->removeFromParent();
				}
			}
		}
		else
		{
			if (cellItem1)
			{
				cellItem1->refreshUI(DataSource.at(2*idx));
			}
			else
			{
				auto cellItem1 = GoldStoreCellItem::create(DataSource.at(2*idx));
				cellItem1->setPosition(Vec2(15,0));
				cellItem1->setTag(kTag_CellItem_Left);
				cell->addChild(cellItem1);
			}

			if (cellItem2)
			{
				cellItem2->refreshUI(DataSource.at(2*idx+1));
			}
			else
			{
				auto cellItem2 = GoldStoreCellItem::create(DataSource.at(2*idx+1));
				cellItem2->setPosition(Vec2(226,0));
				cellItem2->setTag(kTag_CellItem_Right);
				cell->addChild(cellItem2);
			}
		}
	}

	return cell;
}

ssize_t GoldStoreUI::numberOfCellsInTableView( TableView *table )
{
	if (DataSource.size()%2 == 0)
	{
		return DataSource.size()/2;
	}
	else
	{
		return DataSource.size()/2 + 1;
	}
}

void GoldStoreUI::update(float dt)
{
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	l_ingot_value->setString(str_ingot);

	char str_bindingot[20];
	sprintf(str_bindingot,"%d",GameView::getInstance()->getPlayerBindGoldIngot());
	l_bindIngot_value->setString(str_bindingot);
}

void GoldStoreUI::initMarquee()
{
	// clear vector
	std::vector<GoodsInfo*>::iterator iter;
	for (iter = m_vector_goodsInfo.begin(); iter != m_vector_goodsInfo.end(); ++iter)
	{
		delete *iter;
	}
	m_vector_goodsInfo.clear();


	// ػ�ȡ������Ʒ�б
	int nDataSourceSize = DataSource.size();
	for (int i = 0; i < nDataSourceSize; i++)
	{
		bool bIsHot = DataSource.at(i)->ishot();
		if (true == bIsHot)
		{
			auto goodsInfo = new GoodsInfo();
			goodsInfo->CopyFrom(DataSource.at(i)->goods());

			m_vector_goodsInfo.push_back(goodsInfo);
		}
	}

	// ��ж��Ƿ��д�����Ʒ�����û�д�����Ʒ������ʾ����
	if (0 == m_vector_goodsInfo.size())
	{
		return;
	}

	//GameView::getInstance()->getGoodsColorByQuality(lableGoods->goods().quality())

	// Ƽ��뵽Ҫ��ʾ���ַ��
	const char * str_tip = StringDataManager::getString("goldStore_buy_marquee");
	const char * str_tip_left = StringDataManager::getString("goldStore_buy_goodsInfoShowBegin");
	const char * str_tip_right = StringDataManager::getString("goldStore_buy_goodsInfoShowEnd");
	const char * str_tip_seperateSignal = StringDataManager::getString("goldStore_buy_dunhao");
	const char * str_tip_fullstopSignal = StringDataManager::getString("goldStore_buy_junhao");

	m_strMarquee = "";
	m_strMarquee.append(str_tip);

	int nVectorSize = m_vector_goodsInfo.size();
	for (int i = 0; i < nVectorSize; i++)
	{
		std::string strGoodsName = m_vector_goodsInfo.at(i)->name();
		auto colorFont = GameView::getInstance()->getGoodsColorByQuality(m_vector_goodsInfo.at(i)->quality());
		
		std::string strGoodsNameWithColor = StrUtils::applyColor(strGoodsName.c_str(), colorFont);
		std::string strTipLeft = StrUtils::applyColor(str_tip_left, colorFont);
		std::string strTipRight = StrUtils::applyColor(str_tip_right, colorFont);

		m_strMarquee.append(strTipLeft.c_str());
		m_strMarquee.append(strGoodsNameWithColor.c_str());
		m_strMarquee.append(strTipRight.c_str());

		if (i == nVectorSize - 1)
		{
			m_strMarquee.append(str_tip_fullstopSignal);
		}
		else 
		{
			m_strMarquee.append(str_tip_seperateSignal);
		}
	}

	int nPositionX = m_imageView_horn->getPosition().x + m_imageView_horn->getContentSize().width - 43;
	int nPositionY = m_imageView_horn->getPosition().y + m_imageView_horn->getContentSize().height - 7;

	// �����
	auto pMarqueeLabel = GoldStoreMarquee::create(nPositionX, nPositionY, MARQUEE_LABLE_WIDTH,  MARQUEE_LABEL_HEIGHT);
	pMarqueeLabel->initLabelStr(m_strMarquee.c_str(), 14);
	pMarqueeLabel->setTag(kTag_MARQUEE);
	u_layer->addChild(pMarqueeLabel);

}

void GoldStoreUI::refreshTypeTabByIndex( int idx )
{
	//refresh btn type , ingotValue
	if (idx == 0)
	{
		Button_recharge->setVisible(true);
		Button_vip->setVisible(true);
		ImageView_inGotPart->setVisible(true);
		ImageView_bindInGotPart->setVisible(false);
	}
	else
	{
		Button_recharge->setVisible(false);
		Button_vip->setVisible(true);
		ImageView_inGotPart->setVisible(false);
		ImageView_bindInGotPart->setVisible(true);
	}

	if (u_layer->getChildByName("TypeTab"))
	{
		u_layer->getChildByName("TypeTab")->removeFromParent();
	}

	const char * normalImage = "res_ui/tab_s_off.png";
	const char * selectImage = "res_ui/tab_s.png";
	const char * finalImage = "";
	const char * highLightImage = "res_ui/tab_s.png";
	int _num = 0;
	if (idx == 0)   //�Ԫ���̳
	{
		_num = GameView::getInstance()->goldStoreList.size();
		CCAssert(_num <= GOLD_STORE_MAX_FILTER, "out of range");
		char * ItemName[GOLD_STORE_MAX_FILTER];
		for (int i =0;i<_num;++i)
		{
			//std::string imagePath = "res_ui/rmbshop/";
			std::string imagePath = "";
			imagePath.append(GameView::getInstance()->goldStoreList.at(i)->name().c_str());

			ItemName[i] = new char [50];
			strcpy(ItemName[i],imagePath.c_str());
		}

		if (_num > 0)
		{
			tab_function_type  = UITab::createWithText(_num,normalImage,selectImage,finalImage,ItemName,VERTICAL,-10,18);
			tab_function_type->setAnchorPoint(Vec2(0,0));
			tab_function_type->setPosition(Vec2(760,420));
			tab_function_type->setHighLightImage((char * )highLightImage);
			tab_function_type->setHightLightLabelColor(Color3B(47,93,13));
			tab_function_type->setNormalLabelColor(Color3B(255,255,255));
			tab_function_type->setDefaultPanelByIndex(0);
			tab_function_type->addIndexChangedEvent(this,coco_indexchangedselector(GoldStoreUI::IndexChangedEvent));
			tab_function_type->setPressedActionEnabled(true);
			tab_function_type->setName("TypeTab");
			u_layer->addChild(tab_function_type);
		}

		for (int i = 0;i<_num;i++)
		{
			delete ItemName[i];
		}
	}
	else
	{
		_num = GameView::getInstance()->bingGoldStoreList.size();
		CCAssert(_num <= GOLD_STORE_MAX_FILTER, "out of range");
		char * ItemName[GOLD_STORE_MAX_FILTER];
		for (int i =0;i<_num;++i)
		{
			//std::string imagePath = "res_ui/rmbshop/";
			std::string imagePath = "";
			imagePath.append(GameView::getInstance()->bingGoldStoreList.at(i)->name().c_str());

			ItemName[i] = new char [50];
			strcpy(ItemName[i],imagePath.c_str());
		}

		if (_num > 0)
		{
			tab_function_type  = UITab::createWithText(_num,normalImage,selectImage,finalImage,ItemName,VERTICAL,-10,18);
			tab_function_type->setAnchorPoint(Vec2(0,0));
			tab_function_type->setPosition(Vec2(760,420));
			tab_function_type->setHighLightImage((char * )highLightImage);
			tab_function_type->setHightLightLabelColor(Color3B(47,93,13));
			tab_function_type->setNormalLabelColor(Color3B(255,255,255));
			tab_function_type->setDefaultPanelByIndex(0);
			tab_function_type->addIndexChangedEvent(this,coco_indexchangedselector(GoldStoreUI::IndexChangedEvent));
			tab_function_type->setPressedActionEnabled(true);
			tab_function_type->setName("TypeTab");
			u_layer->addChild(tab_function_type);
		}

		for (int i = 0;i<_num;i++)
		{
			delete ItemName[i];
		}
	}

	//refresh tableView
	//delete old data
	DataSource.erase(DataSource.begin(),DataSource.end());
	std::vector<CLableGoods*>::iterator iter;
	for (iter = DataSource.begin(); iter != DataSource.end(); ++iter)
	{
		delete *iter;
	}
	DataSource.clear();

	if (tab_main->getCurrentIndex() == 0)
	{
		if (u_layer->getChildByName("TypeTab"))
		{
			int idx = tab_function_type->getCurrentIndex();
			curLableId = GameView::getInstance()->goldStoreList.at(idx)->id();

			//add new data
			for (int i = 0;i<GameView::getInstance()->goldStoreList.at(idx)->commdity_size();++i)
			{
				auto _LableGoods = new CLableGoods();
				_LableGoods->CopyFrom(GameView::getInstance()->goldStoreList.at(idx)->commdity(i));
				DataSource.push_back(_LableGoods);
			}
		}
	}
	else
	{
		if (u_layer->getChildByName("TypeTab"))
		{
			int idx = tab_function_type->getCurrentIndex();
			curLableId = GameView::getInstance()->bingGoldStoreList.at(idx)->id();

			//add new data
			for (int i = 0;i<GameView::getInstance()->bingGoldStoreList.at(idx)->commdity_size();++i)
			{
				auto _LableGoods = new CLableGoods();
				_LableGoods->CopyFrom(GameView::getInstance()->bingGoldStoreList.at(idx)->commdity(i));
				DataSource.push_back(_LableGoods);
			}
		}
	}

	refreshMarquee();

	this->m_tableView->reloadData();
}

void GoldStoreUI::refreshMarquee()
{
	auto tmpMarquee = (GoldStoreMarquee*)u_layer->getChildByTag(kTag_MARQUEE);
	if (NULL != tmpMarquee)
	{
		u_layer->removeChildByTag(kTag_MARQUEE);
	}

	initMarquee();
}

UITab * GoldStoreUI::getTab_function()
{
	return tab_function_type;
}
