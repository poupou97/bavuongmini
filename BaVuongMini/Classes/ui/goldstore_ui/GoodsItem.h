
#ifndef _GOLDSTOREUI_GOODSITEM_H_
#define _GOLDSTOREUI_GOODSITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GoodsInfo;

class GoodsItem :public Widget
{
public:
	GoodsItem();
	~GoodsItem();

	static GoodsItem * create(GoodsInfo* goods);
	bool initWithGoods(GoodsInfo* goods);

	void GoodItemEvent(Ref *pSender, Widget::TouchEventType type);

private:
	GoodsInfo * curGoodsInfo;
};

#endif