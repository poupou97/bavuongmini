#include "GoodsItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GoldStoreItemInfo.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/backpackscene/EquipmentItem.h"


GoodsItem::GoodsItem()
{
}


GoodsItem::~GoodsItem()
{
	delete curGoodsInfo;
}

GoodsItem * GoodsItem::create( GoodsInfo* goods )
{
	auto widget = new GoodsItem();
	if (widget && widget->initWithGoods(goods))
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}

bool GoodsItem::initWithGoods( GoodsInfo* goods )
{
	if (Widget::init())
	{
		curGoodsInfo = new GoodsInfo();
		curGoodsInfo->CopyFrom(*goods);
		/*********************�ж�װ������ɫ***************************/
		/*********************�ж�װ������ɫ***************************/
		std::string frameColorPath;
		if (curGoodsInfo->quality() == 1)
		{
			frameColorPath = EquipWhiteFramePath;
		}
		else if (curGoodsInfo->quality() == 2)
		{
			frameColorPath = EquipGreenFramePath;
		}
		else if (curGoodsInfo->quality() == 3)
		{
			frameColorPath = EquipBlueFramePath;
		}
		else if (curGoodsInfo->quality() == 4)
		{
			frameColorPath = EquipPurpleFramePath;
		}
		else if (curGoodsInfo->quality() == 5)
		{
			frameColorPath = EquipOrangeFramePath;
		}
		else
		{
			frameColorPath = EquipVoidFramePath;
		}

// 		ImageView * cloud = ImageView::create();
// 		cloud->loadTexture(cloudColorPaht.c_str());
// 		cloud->setAnchorPoint(Vec2(0,0));
// 		cloud->setPosition(Vec2(0,0));
// 		cloud->setScale9Enabled(true);
// 		cloud->setContentSize(Size(56,56));
// 		cloud->setName("res_ui/imageView_cloud");
// 		this->addChild(cloud);

		auto Btn_goodsItemFrame = Button::create();
		Btn_goodsItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_goodsItemFrame->setTouchEnabled(true);
		Btn_goodsItemFrame->setAnchorPoint(Vec2(0,0));
		Btn_goodsItemFrame->setPosition(Vec2(0,0));
		Btn_goodsItemFrame->setScale(0.9f);
		Btn_goodsItemFrame->addTouchEventListener(CC_CALLBACK_2(GoodsItem::GoodItemEvent, this));
		this->addChild(Btn_goodsItemFrame);

		auto uiiImageView_goodsItem = ImageView::create();
		if (strcmp(goods->icon().c_str(),"") == 0)
		{
			uiiImageView_goodsItem->loadTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(goods->icon().c_str());
			_path.append(".png");
			uiiImageView_goodsItem->loadTexture(_path.c_str());
		}
		uiiImageView_goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		uiiImageView_goodsItem->setPosition(Vec2(28,28));
		this->addChild(uiiImageView_goodsItem);

		//this->setTouchEnabled(true);
		this->setContentSize(Size(56,56));

		return true;
	}
	return false;
}

void GoodsItem::GoodItemEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreItemInfo) == NULL)
		{
			auto size = Director::getInstance()->getVisibleSize();
			auto goldStoreItemInfo = GoldStoreItemInfo::create(curGoodsInfo);
			goldStoreItemInfo->setIgnoreAnchorPointForPosition(false);
			goldStoreItemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
			//goldStoreItemInfo->setPosition(Vec2(size.width/2,size.height/2));
			goldStoreItemInfo->setTag(kTagGoldStoreItemInfo);
			GameView::getInstance()->getMainUIScene()->addChild(goldStoreItemInfo);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}
