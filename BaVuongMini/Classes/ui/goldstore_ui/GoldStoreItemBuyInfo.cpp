#include "GoldStoreItemBuyInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../extensions/Counter.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CLableGoods.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"


GoldStoreItemBuyInfo::GoldStoreItemBuyInfo():
buyNum(0),
basePrice(0),
buyPrice(0)
{
}


GoldStoreItemBuyInfo::~GoldStoreItemBuyInfo()
{
	delete curLableGoods;
}

GoldStoreItemBuyInfo * GoldStoreItemBuyInfo::create( CLableGoods * lableGoods )
{
	auto goldStoreItemBuyInfo = new GoldStoreItemBuyInfo();
	if (goldStoreItemBuyInfo && goldStoreItemBuyInfo->init(lableGoods))
	{
		goldStoreItemBuyInfo->autorelease();
		return goldStoreItemBuyInfo;
	}
	CC_SAFE_DELETE(goldStoreItemBuyInfo);
	return NULL;
}

bool GoldStoreItemBuyInfo::init( CLableGoods * lableGoods )
{
	if (UIScene::init())
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		curLableGoods = new CLableGoods();
		curLableGoods->CopyFrom(*lableGoods);

		if (lableGoods->ishot())
		{
			basePrice = lableGoods->discount();
		}
		else
		{
			basePrice = lableGoods->goods().price();
		}


		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(298, 403));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);
		//���UI
		if(LoadSceneLayer::BuyCalculatorsLayer->getParent() != NULL)
		{
			LoadSceneLayer::BuyCalculatorsLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (Layout*)LoadSceneLayer::s_pPanel->copy();
		auto ppanel = LoadSceneLayer::BuyCalculatorsLayer;
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->getVirtualRenderer()->setContentSize(Size(298, 403));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		//عرհ�ť
		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(GoldStoreItemBuyInfo::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);
		//��ƷIcon�׿
		auto imageView_dikuang = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_dikuang");
		switch(lableGoods->goods().quality())
		{
		case 1 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_white.png");
			}
			break;
		case 2 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_green.png");
			}
			break;
		case 3 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_bule.png");
			}
			break;
		case 4 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_purple.png");
			}
			break;
		case 5 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_orange.png");
			}
			break;
		}

		std::string headImageStr ="res_ui/props_icon/";
		headImageStr.append(lableGoods->goods().icon());
		headImageStr.append(".png");
		auto imageView_icon = ImageView::create();
		imageView_icon->loadTexture(headImageStr.c_str());
		imageView_icon->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_icon->setPosition(Vec2(62,344));
		imageView_icon->setScale(0.85f);
		u_layer->addChild(imageView_icon);

		auto l_single_name = (Text*)Helper::seekWidgetByName(ppanel,"Label_single_name");
		l_single_name->setVisible(false);
		auto l_total_name = (Text*)Helper::seekWidgetByName(ppanel,"Label_total_name");
		l_total_name->setVisible(false);
		//򵥼۵ĵ�λͼ�(�Ԫ��)
		auto ImageView_IngotOrCoins_single = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_IngotOrCoins_single");
		ImageView_IngotOrCoins_single->loadTexture("res_ui/ingot.png");
		ImageView_IngotOrCoins_single->setVisible(true);
		//�ܼ۵ĵ�λͼ�(�Ԫ��)
		auto ImageView_IngotOrCoins_total = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_IngotOrCoins_total");
		ImageView_IngotOrCoins_total->loadTexture("res_ui/ingot.png");
		ImageView_IngotOrCoins_total->setVisible(true);
		if (curLableGoods->isbind())
		{
			ImageView_IngotOrCoins_single->loadTexture("res_ui/ingotBind.png");
			ImageView_IngotOrCoins_total->loadTexture("res_ui/ingotBind.png");
		}
		//��Ʒ��
		auto l_name = (Text*)Helper::seekWidgetByName(ppanel,"Label_name");
		l_name->setString(lableGoods->goods().name().c_str());
		l_name->setColor(GameView::getInstance()->getGoodsColorByQuality(lableGoods->goods().quality()));
		//Ƶ�� 
		l_priceOfOne = (Text*)Helper::seekWidgetByName(ppanel,"Label_SingleValue");
		l_priceOfOne->setPosition(Vec2(181,334));
		char s_basePrice[20];
		sprintf(s_basePrice,"%d",basePrice);
		l_priceOfOne->setString(s_basePrice);
		int moneyValue = 0;
		if (curLableGoods->isbind())
		{
			moneyValue = GameView::getInstance()->getPlayerBindGoldIngot();
		}
		else
		{
			moneyValue = GameView::getInstance()->getPlayerGoldIngot();
		}
		if (basePrice <= moneyValue)   //�Ǯ���
		{
			l_priceOfOne->setColor(Color3B(39,238,194));   //��ɫ
		}
		else
		{
			l_priceOfOne->setColor(Color3B(255,51,51));   //��ɫ
		}
		//�������  
		l_buyNum = (Text*)Helper::seekWidgetByName(ppanel,"Label_amount");
		l_buyNum->setString("");
		//�ܼ  
		l_totalPrice = (Text*)Helper::seekWidgetByName(ppanel,"Label_TotalValue");
		l_totalPrice->setString("");
		l_totalPrice->setPosition(Vec2(155,267));

// 		int index = 1;
// 		for (int i=0;i<3;i++)
// 		{
// 			for (int j=0;j<3;j++)
// 			{
// 
// 				char str_index[2]={0};
// 				sprintf(str_index,"%d",index);
// 
// 				UITextButton *extractAnnex=Button::create();
// 				extractAnnex->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
// 				extractAnnex->setTouchEnabled(true);
// 				extractAnnex->setText(str_index);
// 				extractAnnex->setFontSize(20);
// 				extractAnnex->setAnchorPoint(Vec2(0,0));
// 				extractAnnex->setTag(index);
// 				extractAnnex->setPosition(Vec2(31+j*61,185- i*50));
// 				extractAnnex->addTouchEventListener(CC_CALLBACK_2(GoldStoreItemBuyInfo::getAmount));
// 				u_layer->addChild(extractAnnex);
// 				index++;
// 			}
// 		}
		int buttonNumId = 1;
		for (int i=0;i<3;i++)
		{
			for (int j=0;j<3;j++)
			{
				char num[5];
				sprintf(num,"%d",buttonNumId);

				auto extractAnnex=Button::create();
				extractAnnex->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
				extractAnnex->setTouchEnabled(true);
				extractAnnex->setPressedActionEnabled(true);
				extractAnnex->setAnchorPoint(Vec2(0.5f,0.5f));
				extractAnnex->setTag(buttonNumId);
				extractAnnex->setPosition(Vec2(60+j*61,207- i*50));
				extractAnnex->addTouchEventListener(CC_CALLBACK_2(GoldStoreItemBuyInfo::getAmount, this));

				auto label_Num=Label::createWithTTF(num, APP_FONT_NAME, 25);
				label_Num->setAnchorPoint(Vec2(0.5f,0.5f));
				label_Num->setPosition(Vec2(0,0));
				extractAnnex->addChild(label_Num);
				u_layer->addChild(extractAnnex);

				buttonNumId++;
			}
		}
// 
// 		Button * btn_clear=Button::create();
// 		btn_clear->setTouchEnabled(true);
// 		btn_clear->loadTextures("res_ui/enjian_y.png","res_ui/enjian_y.png","");
// 		btn_clear->setAnchorPoint(Vec2(0,0));
// 		btn_clear->setPosition(Vec2(216,185));
// 		btn_clear->setTag(index);
// 		btn_clear->addTouchEventListener(CC_CALLBACK_2(GoldStoreItemBuyInfo::deleteNum));
// 		u_layer->addChild(btn_clear);
// 		ImageView * deleteSp=ImageView::create();
// 		deleteSp->loadTexture("res_ui/jisuanqi/jiantou.png");
// 		deleteSp->setAnchorPoint(Vec2(0.5f,0.5f));
// 		deleteSp->setPosition(Vec2(btn_clear->getContentSize().width/2,btn_clear->getContentSize().height/2));
// 		btn_clear->addChild(deleteSp);
// 
// 		UITextButton *btn_Zero=Button::create();
// 		btn_Zero->setText("0");
// 		btn_Zero->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
// 		btn_Zero->setTouchEnabled(true);
// 		btn_Zero->setFontSize(25);
// 		btn_Zero->setAnchorPoint(Vec2(0,0));
// 		btn_Zero->setTag(0);
// 		btn_Zero->setPosition(Vec2(216,135));
// 		btn_Zero->addTouchEventListener(CC_CALLBACK_2(GoldStoreItemBuyInfo::getAmount));
// 		u_layer->addChild(btn_Zero);
// 
// 		UITextButton *btn_C=Button::create();
// 		btn_C->setText("C");
// 		btn_C->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
// 		btn_C->setTouchEnabled(true);
// 		btn_C->setFontSize(25);
// 		btn_C->setAnchorPoint(Vec2(0,0));
// 		btn_C->setPosition(Vec2(216,85));
// 		btn_C->addTouchEventListener(CC_CALLBACK_2(GoldStoreItemBuyInfo::clearNum));
// 		u_layer->addChild(btn_C);

		auto btn_clear=Button::create();
		btn_clear->setTouchEnabled(true);
		btn_clear->loadTextures("res_ui/enjian_y.png","res_ui/enjian_y.png","");
		btn_clear->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_clear->setPosition(Vec2(216+53/2,185+22));
		btn_clear->setTag(buttonNumId);
		btn_clear->addTouchEventListener(CC_CALLBACK_2(GoldStoreItemBuyInfo::deleteNum, this));
		btn_clear->setPressedActionEnabled(true);
		u_layer->addChild(btn_clear);

		auto deleteSp=ImageView::create();
		deleteSp->loadTexture("res_ui/jisuanqi/jiantou.png");
		deleteSp->setAnchorPoint(Vec2(0.5f,0.5f));
		deleteSp->setPosition(Vec2(0,0));
		btn_clear->addChild(deleteSp);

		auto button_Zero=Button::create();
		button_Zero->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		button_Zero->setTouchEnabled(true);
		button_Zero->setTitleFontSize(25);
		button_Zero->setAnchorPoint(Vec2(0.5f,0.5f));
		button_Zero->setTag(0);
		button_Zero->setPosition(Vec2(216+53/2,135+22));
		button_Zero->setPressedActionEnabled(true);
		button_Zero->addTouchEventListener(CC_CALLBACK_2(GoldStoreItemBuyInfo::getAmount, this));
		u_layer->addChild(button_Zero);

		auto label_Zero=Label::createWithTTF("0",APP_FONT_NAME, 25);
		label_Zero->setAnchorPoint(Vec2(0.5f,0.5f));
		label_Zero->setPosition(Vec2(0,0));
		button_Zero->addChild(label_Zero);

		auto Button_C=Button::create();
		Button_C->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		Button_C->setTouchEnabled(true);
		Button_C->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_C->setPosition(Vec2(216+53/2,85+22));
		Button_C->setPressedActionEnabled(true);
		Button_C->addTouchEventListener(CC_CALLBACK_2(GoldStoreItemBuyInfo::clearNum, this));
		u_layer->addChild(Button_C);

		auto label_C=Label::createWithTTF("C", APP_FONT_NAME, 25);
		label_C->setAnchorPoint(Vec2(0.5f,0.5f));
		label_C->setPosition(Vec2(0,0));
		Button_C->addChild(label_C);

		auto btn_buy = (Button *)Helper::seekWidgetByName(ppanel,"Button_enter");
		btn_buy->setTouchEnabled(true);
		btn_buy->addTouchEventListener(CC_CALLBACK_2(GoldStoreItemBuyInfo::BuyEvent, this));
		btn_buy->setPressedActionEnabled(true);
		auto l_buy = (Text*)Helper::seekWidgetByName(ppanel,"Label_enter");
		const char *str3 = StringDataManager::getString("goods_auction_buy");
		char* shop_buy =const_cast<char*>(str3);
		l_buy->setString(shop_buy);

		setToDefaultNum();

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setAnchorPoint(Vec2(0,0));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(GoldStoreItemBuyInfo::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(GoldStoreItemBuyInfo::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(GoldStoreItemBuyInfo::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(GoldStoreItemBuyInfo::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void GoldStoreItemBuyInfo::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GoldStoreItemBuyInfo::onExit()
{
	UIScene::onExit();
}

bool GoldStoreItemBuyInfo::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	//return resignFirstResponder(pTouch,this,true);
	return true;
}

void GoldStoreItemBuyInfo::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void GoldStoreItemBuyInfo::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void GoldStoreItemBuyInfo::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}


void GoldStoreItemBuyInfo::BuyEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (buyNum > 0)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1705, curLableGoods, (void *)buyNum);
		}
		else
		{
			const char *str1 = StringDataManager::getString("goods_shop_pleaseInputNum");
			char* pleaseinputNum = const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(pleaseinputNum);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GoldStoreItemBuyInfo::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}


void GoldStoreItemBuyInfo::getAmount(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button *)pSender;
		int id_ = btn->getTag();

		if (buyNum>999)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("goldStore_buy_inputNum"));
			return;
		}
		// 	char num[3];
		// 	sprintf(num,"%d",id_);
		// 	str_buyNum.append(num);
		if (buyNum <= 0 && id_ == 0)
		{
			return;
		}
		char num[3];
		sprintf(num, "%d", id_);
		if (std::atoi(str_buyNum.c_str()) <= 0)
		{
			str_buyNum = num;
		}
		else
		{
			str_buyNum.append(num);
		}
		l_buyNum->setString(str_buyNum.c_str());
		buyNum = std::atoi(str_buyNum.c_str());

		buyPrice = basePrice * buyNum;
		char str_buyPrice[20];
		sprintf(str_buyPrice, "%d", buyPrice);
		l_totalPrice->setString(str_buyPrice);
		int moneyValue = 0;
		if (curLableGoods->isbind())
		{
			moneyValue = GameView::getInstance()->getPlayerBindGoldIngot();
		}
		else
		{
			moneyValue = GameView::getInstance()->getPlayerGoldIngot();
		}
		if (buyPrice <= moneyValue)   //�Ǯ���
		{
			l_totalPrice->setColor(Color3B(225, 255, 138));   //��ɫ
		}
		else
		{
			l_totalPrice->setColor(Color3B(255, 51, 51));   //��ɫ
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void GoldStoreItemBuyInfo::clearNum(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		str_buyNum.clear();
		l_buyNum->setString(str_buyNum.c_str());
		buyNum = std::atoi(str_buyNum.c_str());

		l_totalPrice->setString("");
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GoldStoreItemBuyInfo::deleteNum(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (str_buyNum.size()>0)
		{
			str_buyNum.erase(str_buyNum.end() - 1);
			l_buyNum->setString(str_buyNum.c_str());
			buyNum = std::atoi(str_buyNum.c_str());

			buyPrice = basePrice * buyNum;
			char str_buyPrice[20];
			sprintf(str_buyPrice, "%d", buyPrice);
			l_totalPrice->setString(str_buyPrice);

			int moneyValue = 0;
			if (curLableGoods->isbind())
			{
				moneyValue = GameView::getInstance()->getPlayerBindGoldIngot();
			}
			else
			{
				moneyValue = GameView::getInstance()->getPlayerGoldIngot();
			}

			if (buyPrice <= moneyValue)   //Ǯ���
			{
				l_totalPrice->setColor(Color3B(225, 255, 138));   //��ɫ
			}
			else
			{
				l_totalPrice->setColor(Color3B(255, 51, 51));   //��ɫ
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GoldStoreItemBuyInfo::setToDefaultNum()
{
	l_buyNum->setString("1");
	buyNum = 1;

	buyPrice = basePrice * buyNum;
	char str_buyPrice [20];
	sprintf(str_buyPrice,"%d",buyPrice);
	l_totalPrice->setString(str_buyPrice);

	int moneyValue = 0;
	if (curLableGoods->isbind())
	{
		moneyValue = GameView::getInstance()->getPlayerBindGoldIngot();
	}
	else
	{
		moneyValue = GameView::getInstance()->getPlayerGoldIngot();
	}
	if (buyPrice <= moneyValue)   //Ǯ���
	{
		l_totalPrice->setColor(Color3B(225,255,138));   //��ɫ
	}
	else
	{
		l_totalPrice->setColor(Color3B(255,51,51));   //��ɫ
	}
}
