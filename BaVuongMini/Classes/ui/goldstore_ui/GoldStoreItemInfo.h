
#ifndef _GOLDSTOREUI_GOLDSTOREITEMINFO_H_
#define _GOLDSTOREUI_GOLDSTOREITEMINFO_H_

#include "../backpackscene/GoodsItemInfoBase.h"

class GoodsInfo;

class GoldStoreItemInfo : public GoodsItemInfoBase
{
public:
	GoldStoreItemInfo();
	~GoldStoreItemInfo();

	static GoldStoreItemInfo * create(GoodsInfo * goodsInfo);
	bool init(GoodsInfo * goodsInfo);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
};

#endif
