#ifndef _GOLDSTOREUI_GOLDSTOREMARQUEE_H_
#define _GOLDSTOREUI_GOLDSTOREMARQUEE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ����ƣ�����Ԫ���̳ǣ�
 * @author liuliang
 * @version 0.1.0
 * @date 2014.04.23
 */

class CCRichLabel;

class GoldStoreMarquee :public Node
{
public:
	GoldStoreMarquee();
	~GoldStoreMarquee();

	virtual void update(float dt);

	static GoldStoreMarquee * create(int x, int y, int width, int height);
	bool init(int x, int y ,int width, int height);
	void initLabelStr(const char * pStr,int nFontSize);

	void setLabelStr(std::string strLabel);

private:
	RenderTexture * m_pRenderTexture;
	CCRichLabel * m_pRichLabelTTF;
	Vec2 m_labelPoint;
	int m_nWidth;
	int m_nHeight;
	int m_nFontSize;
	float m_timeDelta;

	std::string m_strLabel;
};

#endif