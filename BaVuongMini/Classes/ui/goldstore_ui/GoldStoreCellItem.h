#ifndef  _GOLDSTOREUI_GOLDSTORECELL_H_
#define _GOLDSTOREUI_GOLDSTORECELL_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CLableGoods;

class GoldStoreCellItem : public UIScene
{
public:
	GoldStoreCellItem();
	~GoldStoreCellItem();

	static GoldStoreCellItem* create(CLableGoods * lableGoods);
	bool init(CLableGoods * lableGoods);

	void BuyEvent(Ref *pSender, Widget::TouchEventType type);
	void GoodItemEvent(Ref *pSender, Widget::TouchEventType type);

	void refreshUI(CLableGoods * lableGoods);

private:
	CLableGoods * curLableGoods;

	Label * l_name;
	Label * l_oldPrice_value;
	Label * l_oldPrice;
	ImageView * imageView_oldPrice_goldIngold;
	ImageView * imageView_redLine;
	Label * l_newPrice_value;
	Label * l_newPrice;
	ImageView * imageView_newPrice_goldIngold;
	ImageView * ImageView_hot;
	Label * l_price_value;
	Label * l_price;
	ImageView * imageView_price_goldIngold;
	Button *Btn_goodsItemFrame;
	ImageView * uiiImageView_goodsItem;
};

#endif

