#ifndef _GOLDSTOREUI_GOLDSTOREUI_H_
#define _GOLDSTOREUI_GOLDSTOREUI_H_

#include "../../ui/extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "network\HttpClient.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
class CLableGoods;
class GoodsInfo;

class GoldStoreUI : public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	/*�̵������ҳǩ*/
	enum goldStore_tabType
	{
		type_all = 0,
		type_strength,
		type_aid,
		type_phypower,
		type_gem
	};


	GoldStoreUI();
	~GoldStoreUI();

	static GoldStoreUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void RechargeEvent(Ref *pSender, Widget::TouchEventType type);
	void VipEvent(Ref *pSender, Widget::TouchEventType type);

	void FunctionTypeIndexChangedEvent(Ref* pSender);
	void IndexChangedEvent(Ref* pSender);

	void refreshTypeTabByIndex(int idx);

	virtual void update(float dt);

	UITab * getTab_function();

private:
	Layout * ppanel;
	Layer * u_layer;
	//
	UITab * tab_main;
	UITab * tab_function_type;

    Button * Button_recharge;
	Button * Button_vip;

	ImageView *ImageView_inGotPart;
	ImageView *ImageView_bindInGotPart;
	
	Text * l_ingot_value;
	Text * l_bindIngot_value;

	ImageView * m_imageView_horn;

	std::vector<CLableGoods *> DataSource;
	std::vector<GoodsInfo *> m_vector_goodsInfo;										// ������Ʒ��������Ʒ���vector

	std::string m_strMarquee;

	TableView * m_tableView;

public:
	int curLableId;

private:
	//  LiuLiang++
	void initMarquee();																					// ��̳Ǵ�����Ʒ��������Ʒ������Ƶ�ʵ�
	void refreshMarquee();
    
public:
    void requestPay();
    void onHttpResponsePay(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
};

#endif

