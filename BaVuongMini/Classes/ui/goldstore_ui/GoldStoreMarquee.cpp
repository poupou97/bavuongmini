#include "GoldStoreMarquee.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../../GameView.h"

#define TIME_CIRCLE_SECOND 5

GoldStoreMarquee::GoldStoreMarquee()
	:m_pRenderTexture(NULL)
	,m_pRichLabelTTF(NULL)
	,m_labelPoint(Vec2(0, 0))
	,m_nWidth(0)
	,m_nHeight(0)
	,m_nFontSize(0)
	,m_timeDelta(0.0f)
	,m_strLabel("")
{

}

GoldStoreMarquee::~GoldStoreMarquee()
{
	this->unscheduleUpdate();

	if (m_pRichLabelTTF)
	{
		m_pRichLabelTTF->release();

		m_pRichLabelTTF = NULL;
	}
}

GoldStoreMarquee * GoldStoreMarquee::create( int x, int y, int width, int height )
{
	auto marquee = new GoldStoreMarquee();
	if (marquee && marquee->init(x, y, width, height))
	{
		marquee->autorelease();
		return marquee;
	}
	CC_SAFE_DELETE(marquee);
	return NULL;
}

bool GoldStoreMarquee::init( int x, int y ,int width, int height )
{
	if (Node::init())
	{
		m_nWidth = width;
		m_nHeight = height;

		/*m_pRenderTexture = CCRenderTexture::create(width, height, kTexture2DPixelFormat_RGBA8888);*/
		m_pRenderTexture = CCRenderTexture::create(width, height);
		m_pRenderTexture->getSprite()->setAnchorPoint(Vec2(0, 0));
		//m_pRenderTexture->getSprite()->setIgnoreAnchorPointForPosition(false);
		m_pRenderTexture->setAnchorPoint(Vec2(0, 0));
		m_pRenderTexture->setIgnoreAnchorPointForPosition(false);
		m_pRenderTexture->setPosition(Vec2(x, y));
		addChild(m_pRenderTexture);

		return true;
	}

	return false;
}

void GoldStoreMarquee::initLabelStr(const char * pStr,int nFontSize )
{
	m_nFontSize = nFontSize;

	m_strLabel = pStr;

	m_pRichLabelTTF = CCRichLabel::createWithString(pStr, Size(m_nWidth, 0), NULL, NULL, 0, nFontSize, 1);
	m_pRichLabelTTF->setAnchorPoint(Vec2(0,0));
	m_pRichLabelTTF->setIgnoreAnchorPointForPosition(false);
	m_pRichLabelTTF->setColor(Color3B(255, 255, 255));
	m_pRichLabelTTF->retain();

	float yPos = m_pRichLabelTTF->getContentSize().height;

	m_labelPoint = Vec2(0, -yPos);

	this->scheduleUpdate();
}

void GoldStoreMarquee::setLabelStr( std::string strLabel )
{
	m_strLabel = strLabel;
	m_pRichLabelTTF->setString(m_strLabel.c_str());
}

void GoldStoreMarquee::update( float dt )
{
	m_labelPoint.x = 0;

	m_labelPoint.y = m_labelPoint.y + dt * 20;
	if (m_labelPoint.y  > m_pRenderTexture->getSprite()->getTexture()->getContentSize().height)
	{
		m_timeDelta += dt;

		if (m_timeDelta >= TIME_CIRCLE_SECOND)
		{
			m_timeDelta = 0.0f;

			float yPos = m_pRichLabelTTF->getContentSize().height;

			m_labelPoint.y = -yPos; 
		}
		else 
		{
			return;
		}
	}

	m_pRichLabelTTF->setPosition(m_labelPoint);

	if (m_pRenderTexture)
	{
		/*m_pRenderTexture->clear(0.5f, 0.5f, 0.5f, 0.5f);*/
		m_pRenderTexture->clear(0, 0, 0, 0);

		m_pRenderTexture->begin();

		m_pRichLabelTTF->visit();

		m_pRenderTexture->end();
	}
}