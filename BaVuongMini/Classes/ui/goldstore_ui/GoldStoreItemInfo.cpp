#include "GoldStoreItemInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"


GoldStoreItemInfo::GoldStoreItemInfo()
{
}


GoldStoreItemInfo::~GoldStoreItemInfo()
{
}

GoldStoreItemInfo * GoldStoreItemInfo::create( GoodsInfo * goodsInfo )
{
	auto goldStoreItemInfo = new GoldStoreItemInfo();
	if (goldStoreItemInfo && goldStoreItemInfo->init(goodsInfo))
	{
		goldStoreItemInfo->autorelease();
		return goldStoreItemInfo;
	}
	CC_SAFE_DELETE(goldStoreItemInfo);
	return NULL;
}

bool GoldStoreItemInfo::init( GoodsInfo * goodsInfo )
{
	if (GoodsItemInfoBase::init(goodsInfo,GameView::getInstance()->EquipListItem,0))
	{

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setAnchorPoint(Vec2(0,0));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(GoldStoreItemInfo::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(GoldStoreItemInfo::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(GoldStoreItemInfo::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(GoldStoreItemInfo::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void GoldStoreItemInfo::onEnter()
{
	GoodsItemInfoBase::onEnter();
	this->openAnim();
}

void GoldStoreItemInfo::onExit()
{
	GoodsItemInfoBase::onExit();
}

bool GoldStoreItemInfo::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return resignFirstResponder(pTouch,this,true);
}

void GoldStoreItemInfo::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void GoldStoreItemInfo::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void GoldStoreItemInfo::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

