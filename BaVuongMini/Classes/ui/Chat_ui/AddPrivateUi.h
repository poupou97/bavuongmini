#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class RichTextInputBox;

typedef enum{

	RICHBOXINPUT_TAG=205
};
enum
{
	PRIVATECHAT=0,
	CHECK,
	TEAM,
	FRIEND,
	MAIL,
	BLACKLIST
};
class AddPrivateUi:public UIScene
{
public:
	AddPrivateUi(void);
	~AddPrivateUi(void);

	struct FriendList
	{
		int relationType;
		int pageNum;
		int pageIndex;
		int showOnline;
	};

	static AddPrivateUi * create(int num);
	bool init(int num);
	virtual void onEnter();
	virtual void onExit();
	void callBackFriend(Ref *pSender, Widget::TouchEventType type);

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
	
	void callBackClosed(Ref *pSender, Widget::TouchEventType type);
	void callBackFriendList(Ref * obj);

	void selectButton_channel(Ref * obj);
public:
	RichTextInputBox * textBox_private;
	int currType;
private:
	 Layout * privatePanel;
	 struct FriendStruct
	 {
		 int operation;
		 int relationType;
		 long long playerid;
		 std::string playerName;
	 };
};

