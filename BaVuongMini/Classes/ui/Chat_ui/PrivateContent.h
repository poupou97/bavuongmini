#ifndef  PRIVATECHATCONTENT_H 
#define PRIVATECHATCONTENT_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class PrivateContent:public UIScene,public cocos2d::extension::TableViewDataSource,public cocos2d::extension::TableViewDelegate
{
public:
	PrivateContent(void);
	~PrivateContent(void);

	static PrivateContent * create();
	bool init();
	void onEnter();
	void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
public:
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	TableView * chatContentTabview;
private:
	Size winsize;


};

#endif