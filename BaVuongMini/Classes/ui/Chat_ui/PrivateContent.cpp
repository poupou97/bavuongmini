#include "PrivateContent.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../messageclient/element/CRelationPlayer.h"
#include "ChatCell.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "PrivateCell.h"


PrivateContent::PrivateContent(void)
{
}


PrivateContent::~PrivateContent(void)
{
}

PrivateContent * PrivateContent::create()
{
	auto privateUi =  new PrivateContent();
	if (privateUi && privateUi->init())
	{
		privateUi->autorelease();
		return privateUi;
	}
	CC_SAFE_DELETE(privateUi);
	return NULL;
}

bool PrivateContent::init()
{
	if (UIScene::init())
	{
		winsize = Director::getInstance()->getVisibleSize();

		chatContentTabview = TableView::create(this, Size(345,255));
		chatContentTabview->setDirection(TableView::Direction::VERTICAL);
		chatContentTabview->setAnchorPoint(Vec2(0,0));
		chatContentTabview->setPosition(Vec2(0,0));
		chatContentTabview->setDelegate(this);
		chatContentTabview->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		m_pLayer->addChild(chatContentTabview);

		//setTouchEnabled(false);
		return true;
	}
	return false;
}

void PrivateContent::onEnter()
{
	UIScene::onEnter();
}

void PrivateContent::onExit()
{
	UIScene::onExit();
}

void PrivateContent::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{

}

void PrivateContent::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

void PrivateContent::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	
}

cocos2d::Size PrivateContent::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	//int height= GameView::getInstance()->chatPrivateContentSourceVector.at(idx)->getContentSize().height+25;
	int height= GameView::getInstance()->chatPrivateContentSourceVector.at(idx)->getCurCellHeight();
	return Size(345,height);
}

cocos2d::extension::TableViewCell* PrivateContent::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	//TableViewCell *cell =table->dequeueCell();   // this method must be called
	auto cell = (PrivateCell*)table->dequeueCell();   // this method must be called
	cell = GameView::getInstance()->chatPrivateContentSourceVector.at(idx);
	/*
	cell=new TableViewCell();
	cell->autorelease();
	
	long long speakerId = GameView::getInstance()->chatPrivateContentSourceVector.at(idx)->playerId;
	std::string str_nameend = GameView::getInstance()->chatPrivateContentSourceVector.at(idx)->playName;
	std::string str_content = GameView::getInstance()->chatPrivateContentSourceVector.at(idx)->chatContent_;

	if ( speakerId == GameView::getInstance()->myplayer->getActiveRole()->rolebase().roleid())
	{
		MenuItemFont * itemImage_name = MenuItemFont::create(str_nameend.c_str(), this, menu_selector(PrivateContent::callBackNameEvent));
		Color3B nameColor=Color3B(161,255,156);
		itemImage_name->setColor(nameColor);
		itemImage_name->setAnchorPoint(Vec2(0.5f,0));

		CCMoveableMenu * name_menu=CCMoveableMenu::create(itemImage_name,NULL);
		name_menu->setAnchorPoint(Vec2(1,0));
		name_menu->setPosition(Vec2(327 - itemImage_name->getContentSize().width,0));
		cell->addChild(name_menu);

		CCRichLabel *labelLink =CCRichLabel::createWithString(str_content.c_str(),Size(320,50),NULL,NULL,0);
		labelLink->setAnchorPoint(Vec2(0,0));
		labelLink->setPosition(Vec2(0, -itemImage_name->getContentSize().height));
		cell->addChild(labelLink);

		if (labelLink->getContentSize().width < 327)
		{
			labelLink->setPosition(Vec2(327 - labelLink->getContentSize().width,-itemImage_name->getContentSize().height));
		}

	}else
	{
		MenuItemFont * itemImage_name = MenuItemFont::create(str_nameend.c_str(), this, menu_selector(PrivateContent::callBackNameEvent));
		Color3B nameColor=Color3B(161,255,156);
		itemImage_name->setColor(nameColor);
		itemImage_name->setAnchorPoint(Vec2(0.5f,0));
		CCMoveableMenu * name_menu=CCMoveableMenu::create(itemImage_name,NULL);
		name_menu->setAnchorPoint(Vec2(0,0));
		name_menu->setPosition(Vec2(itemImage_name->getContentSize().width/2,0));
		cell->addChild(name_menu);

		CCRichLabel *labelLink =CCRichLabel::createWithString(str_content.c_str(),Size(320,50),NULL,NULL,0);
		labelLink->setAnchorPoint(Vec2(0,0));
		labelLink->setPosition(Vec2(0, - itemImage_name->getContentSize().height));
		cell->addChild(labelLink);
	}
	*/
	return cell;
}

ssize_t PrivateContent::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	return GameView::getInstance()->chatPrivateContentSourceVector.size();
}

bool PrivateContent::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return true;
}




