#ifndef  PRIVATECHATUI_H 
#define PRIVATECHATUI_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../extensions/RichTextInput.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class PrivateChatUi:public UIScene,public cocos2d::extension::TableViewDataSource,public cocos2d::extension::TableViewDelegate
{
public:
	PrivateChatUi(void);
	~PrivateChatUi(void);

	static PrivateChatUi * create(int index);
	bool init(int index);
	void onEnter();
	void onExit();

	void setNewmessagePlayerId(long long playerid);
	long long getNewmessagePlayerId();

	//void setIsNewMessage(bool value_);
	void refreshNewMessageFlag(long long id_);

	void setNewMessageFlag(bool isVisible_);

	void refreshPrivateContent(Ref * obj);
	
	void showPrivateContent(std::string selectName);

	RichTextInputBox * textInputBox;

	void callBackMin(Ref *pSender, Widget::TouchEventType type);
	void callBackNewMessage(Ref *pSender, Widget::TouchEventType type);
	void callBackDelete(Ref *pSender, Widget::TouchEventType type);
	void callBackBrow(Ref *pSender, Widget::TouchEventType type);
	void callBackProps(Ref *pSender, Widget::TouchEventType type);
	void callBackSend(Ref *pSender, Widget::TouchEventType type);
	void callBackClose(Ref *pSender, Widget::TouchEventType type);
public:
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	long long selectPlayerId;
	std::string selectPlayerName;
	TableView * nameTabview;
	Layer * layer2;

	void setCellSelectIndex(int index);
	int getCellSelectIndex();
private:
	Size winsize;
	Button * btnBrow;
	
	long long m_newMessagePlayerId;
	bool m_isHaveNewMessage;
	int m_tabviewSelectIndex;

	ImageView * image_newMessageFlag;
	Label * label_newMessageNum;
	struct ChatInfoStruct
	{
		int channelId;
		std::string chatContent;
		long long listenerId;
		std::string listenerName;
	};
};
////////////////////
class NameTableCell:public TableViewCell
{
public:
	NameTableCell(void);
	~NameTableCell(void);

	static NameTableCell *create(int idx);
	bool init(int idx);

	void onEnter();
	void onExit();
	void callBackNameList(Ref *pSender, Widget::TouchEventType type);
	void deleteSelectCell(Ref *pSender);
private:
	long long m_roleId;
	std::string m_roleName;
};


////
class NameList:public UIScene
{
public:
	NameList(void);
	~NameList(void);

	static NameList * create(long long id_,std::string name_);
	bool init(long long id_,std::string name_);
	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	
	void callBackFriendList(Ref * obj);
private:
	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};

	long long m_roleId;
	std::string m_roleName;
};

#endif