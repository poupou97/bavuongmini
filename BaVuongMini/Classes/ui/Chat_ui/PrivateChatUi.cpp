#include "PrivateChatUi.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../messageclient/element/CRelationPlayer.h"
#include "ChatCell.h"
#include "PrivateContent.h"
#include "ExpressionLayer.h"
#include "BackPackage.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "PrivateCell.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "ChatUI.h"
#include "ui/extensions/UITab.h"
#include "ui/Mail_ui/MailUI.h"
#include "cocostudio\CCSGUIReader.h"

#define  SelectImageTag 458

PrivateChatUi::PrivateChatUi(void)
{
	selectPlayerId = 0;
	m_tabviewSelectIndex = -1;
}


PrivateChatUi::~PrivateChatUi(void)
{
}

PrivateChatUi * PrivateChatUi::create(int index)
{
	auto privateUi =  new PrivateChatUi();
	if (privateUi && privateUi->init(index))
	{
		privateUi->autorelease();
		return privateUi;
	}
	CC_SAFE_DELETE(privateUi);
	return NULL;
}

bool PrivateChatUi::init(int index)
{
	if (UIScene::init())
	{
		winsize = Director::getInstance()->getVisibleSize();

		auto privatePanel =(Layout*) cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/PM_1.json");
		privatePanel->setAnchorPoint(Vec2(0,0));
		privatePanel->setPosition(Vec2(0,0));
		m_pLayer->addChild(privatePanel);

		nameTabview = TableView::create(this, Size(206,300));
		//nameTabview->setSelectedEnable(true);
		//nameTabview->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 24, 1, 1), Vec2(0,0));
		nameTabview->setDirection(TableView::Direction::VERTICAL);
		nameTabview->setAnchorPoint(Vec2(0,0));
		nameTabview->setPosition(Vec2(48,95));
		nameTabview->setDelegate(this);
		nameTabview->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		m_pLayer->addChild(nameTabview);

		auto contentui = PrivateContent::create();
		contentui->setIgnoreAnchorPointForPosition(false);
		contentui->setAnchorPoint(Vec2(0,0));
		contentui->setPosition(Vec2(292,135));
		contentui->setTag(ktagPrivateContentUi);
		this->addChild(contentui);

		auto layer1 = Layer::create();
		this->addChild(layer1,1);

		layer2 = Layer::create();
		layer2->setContentSize(Size(673,430));
		this->addChild(layer2,2);

		textInputBox=new RichTextInputBox();
		textInputBox->setInputMode(RichTextInputBox::kCCInputModeAppendOnly);
		textInputBox->setInputBoxWidth(335);
		textInputBox->setAnchorPoint(Vec2(0,0));
		textInputBox->setPosition(Vec2(287,85));
		layer1->addChild(textInputBox);
		textInputBox->setCharLimit(50);
		textInputBox->autorelease();

		auto btnMinMize = (Button *)Helper::seekWidgetByName(privatePanel,"Button_minimize");
		btnMinMize->setTouchEnabled(true);
		btnMinMize->setPressedActionEnabled(true);
		btnMinMize->addTouchEventListener(CC_CALLBACK_2(PrivateChatUi::callBackMin,this));

		auto btnNewMessage = (Button *)Helper::seekWidgetByName(privatePanel,"Button_31_newMessage");
		btnNewMessage->setTouchEnabled(true);
		btnNewMessage->setPressedActionEnabled(true);
		btnNewMessage->addTouchEventListener(CC_CALLBACK_2(PrivateChatUi::callBackNewMessage, this));
		
		image_newMessageFlag = ImageView::create();
		image_newMessageFlag->loadTexture("res_ui/new1.png");
		image_newMessageFlag->setAnchorPoint(Vec2(0.5f,0.5f));
		image_newMessageFlag->setPosition(Vec2(btnNewMessage->getContentSize().width - 1,btnNewMessage->getContentSize().height/2 - 3));
		btnNewMessage->addChild(image_newMessageFlag);
		image_newMessageFlag->setVisible(false);

		label_newMessageNum = Label::createWithBMFont("res_ui/font/ziti_1.fnt", "0");
		label_newMessageNum->setAnchorPoint(Vec2(0.5f,0.5f));
		label_newMessageNum->setPosition(Vec2(0.5,0));
		label_newMessageNum->setScale(0.55f);
		image_newMessageFlag->addChild(label_newMessageNum);

		int m_vectorSize = GameView::getInstance()->chatPrivateSpeakerVector.size();
		if ( m_vectorSize > 0)
		{
			if (index == 0)
			{
				selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(0)->playername();
				selectPlayerId = GameView::getInstance()->chatPrivateSpeakerVector.at(0)->playerid();
				this->showPrivateContent(selectPlayerName);
				nameTabview->cellAtIndex(0);
			}else
			{
				selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(m_vectorSize - 1)->playername();
				selectPlayerId = GameView::getInstance()->chatPrivateSpeakerVector.at(m_vectorSize - 1)->playerid();
				this->showPrivateContent(selectPlayerName);
				nameTabview->cellAtIndex(m_vectorSize - 1);
			}
		}

		
		auto btnDelete = (Button *)Helper::seekWidgetByName(privatePanel,"Button_delete");
		btnDelete->setTouchEnabled(true);
		btnDelete->setPressedActionEnabled(true);
		btnDelete->addTouchEventListener(CC_CALLBACK_2(PrivateChatUi::callBackDelete, this));

		btnBrow = (Button *)Helper::seekWidgetByName(privatePanel,"Button_brow");
		btnBrow->setTouchEnabled(true);
		btnBrow->setPressedActionEnabled(true);
		btnBrow->addTouchEventListener(CC_CALLBACK_2(PrivateChatUi::callBackBrow, this));

		auto btnProps = (Button *)Helper::seekWidgetByName(privatePanel,"Button_props");
		btnProps->setTouchEnabled(true);
		btnProps->setPressedActionEnabled(true);
		btnProps->addTouchEventListener(CC_CALLBACK_2(PrivateChatUi::callBackProps, this));

		auto btnSend = (Button *)Helper::seekWidgetByName(privatePanel,"Button_send");
		btnSend->setTouchEnabled(true);
		btnSend->setPressedActionEnabled(true);
		btnSend->addTouchEventListener(CC_CALLBACK_2(PrivateChatUi::callBackSend, this));

		auto btnClose = (Button *)Helper::seekWidgetByName(privatePanel,"Button_close");
		btnClose->setTouchEnabled(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addTouchEventListener(CC_CALLBACK_2(PrivateChatUi::callBackClose, this));

// 		ImageView * imageKuang = ImageView::create();
// 		imageKuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		imageKuang->setAnchorPoint(Vec2(0,0));
// 		imageKuang->setScale9Enabled(true);
// 		imageKuang->setContentSize(Size(226,319));
// 		imageKuang->setCapInsets(Rect(32,32,1,1));
// 		imageKuang->setPosition(Vec2(39,87));
// 		layer2->addChild(imageKuang);

		this->setContentSize(Size(673,430));
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		////this->setTouchEnabled(true);
		return true;
	}
	return false;
}

void PrivateChatUi::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void PrivateChatUi::onExit()
{
	UIScene::onExit();
}

void PrivateChatUi::scrollViewDidScroll(cocos2d::extension::ScrollView * view )
{

}

void PrivateChatUi::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void PrivateChatUi::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void PrivateChatUi::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void PrivateChatUi::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void PrivateChatUi::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	int index_ = cell->getIdx();
	
	auto m_cell = table->cellAtIndex(this->getCellSelectIndex());
	if (m_cell != NULL)
	{
		auto nameCell_1 = (NameTableCell *)m_cell->getChildByTag(1010);
		auto menu_ = (CCMoveableMenu *)nameCell_1->getChildByTag(1011);
		menu_->runAction(MoveBy::create(0.2f,Vec2(80,0)));
		nameCell_1->runAction(MoveBy::create(0.2f,Vec2(5,0)));
		menu_->setVisible(false);
	}
 	
 	this->setCellSelectIndex(index_);
	auto nameCell_ = (NameTableCell *)cell->getChildByTag(1010);
	if (nameCell_ != NULL)
	{
		auto menu_ = (CCMoveableMenu *)nameCell_->getChildByTag(1011);
		menu_->runAction(MoveBy::create(0.2f,Vec2(-80,0)));
		nameCell_->runAction(MoveBy::create(0.2f,Vec2(-5,0)));
		menu_->setVisible(true);
	}
	
	selectPlayerId =  GameView::getInstance()->chatPrivateSpeakerVector.at(this->getCellSelectIndex())->playerid();
	selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(this->getCellSelectIndex())->playername();
	this->refreshNewMessageFlag(selectPlayerId);
	this->showPrivateContent(GameView::getInstance()->chatPrivateSpeakerVector.at(this->getCellSelectIndex())->playername());
}

cocos2d::Size PrivateChatUi::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(206,61);
}

cocos2d::extension::TableViewCell* PrivateChatUi::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	__String * string =__String::createWithFormat("%d",idx);	
	auto cell =table->dequeueCell();
	cell=new TableViewCell();
	cell->autorelease();

	auto spbg = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
	spbg->setPreferredSize(Size(206,61));
	spbg->setCapInsets(Rect(15,30,1,1));
	spbg->setAnchorPoint(Vec2(0,0));
	spbg->setPosition(Vec2(0,0));
	cell->addChild(spbg);

	auto m_layer = NameTableCell::create(idx);
	m_layer->setIgnoreAnchorPointForPosition(false);
	m_layer->setAnchorPoint(Vec2(0,0));
	m_layer->setPosition(Vec2(0,0));
	m_layer->setTag(1010);
	cell->addChild(m_layer);

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	return cell;
}

ssize_t PrivateChatUi::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	return GameView::getInstance()->chatPrivateSpeakerVector.size();
}

void PrivateChatUi::refreshPrivateContent( Ref * obj )
{
	auto item = (MenuItemSprite *)obj;
	int index = item->getTag();
	
	selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(index)->playername();
	selectPlayerId = GameView::getInstance()->chatPrivateSpeakerVector.at(index)->playerid();
	this->showPrivateContent(selectPlayerName);
}

void PrivateChatUi::callBackMin(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		this->closeAnim();
		auto mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
		mainScene->btnRemin->setVisible(true);
		mainScene->btnReminOfNewMessageParticle->setVisible(false);

		GameView::getInstance()->hasPrivatePlaer = false;
		GameView::getInstance()->isShowPrivateChatBtn = true;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void PrivateChatUi::callBackNewMessage(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		int m_size = GameView::getInstance()->chatPrivateNewMessageInfo.size();
		if (m_size > 0)
		{
			long long m_id = GameView::getInstance()->chatPrivateNewMessageInfo.at(m_size - 1)->playerid();
			if (GameView::getInstance()->chatPrivateSpeakerVector.size() >0)
			{
				for (int i = 0; i<GameView::getInstance()->chatPrivateSpeakerVector.size(); i++)
				{
					if (GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playerid() == m_id)
					{
						nameTabview->cellAtIndex(i);
						showPrivateContent(GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playername());
						selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playername();
					}
				}

				this->refreshNewMessageFlag(m_id);
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void PrivateChatUi::callBackDelete(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		textInputBox->onTextFieldDeleteBackward(NULL, NULL, 0);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void PrivateChatUi::callBackBrow(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto ex = ExpressionLayer::create(ktagPrivateUI);
		ex->setIgnoreAnchorPointForPosition(false);
		ex->setAnchorPoint(Vec2(0.5f, 0));
		ex->setPosition(Vec2(layer2->getContentSize().width / 2, btnBrow->getPosition().y + btnBrow->getContentSize().height));
		layer2->addChild(ex, 4);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void PrivateChatUi::callBackProps(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto backbag = BackPackage::create(ktagPrivateUI);
		backbag->setIgnoreAnchorPointForPosition(false);
		backbag->setAnchorPoint(Vec2(0.5f, 0.5f));
		backbag->setTag(PACKAGEAGE);
		backbag->setPosition(Vec2(layer2->getContentSize().width / 2, layer2->getContentSize().height / 2));
		layer2->addChild(backbag);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void PrivateChatUi::callBackSend(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//send 2203
		const char * inputStr = textInputBox->getInputString();
		if (inputStr == NULL)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("chatui_callBackSend_null"));
			return;
		}
		if (strcmp(selectPlayerName.c_str(), "") == 0)
		{
			const char *string_ = StringDataManager::getString("privateChat_privateNameIsNull");
			GameView::getInstance()->showAlertDialog(string_);
			return;
		}

		ChatInfoStruct info = { 0,inputStr,-1,selectPlayerName };
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2203, &info);

		textInputBox->deleteAllInputString();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void PrivateChatUi::callBackClose(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
		auto mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
		mainScene->btnRemin->setVisible(false);
		mainScene->btnReminOfNewMessageParticle->setVisible(false);
		GameView::getInstance()->hasPrivatePlaer = false;
		GameView::getInstance()->isShowPrivateChatBtn = false;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void PrivateChatUi::setNewmessagePlayerId( long long playerid )
{
	m_newMessagePlayerId = playerid;
}

long long PrivateChatUi::getNewmessagePlayerId()
{
	return m_newMessagePlayerId;
}

void PrivateChatUi::refreshNewMessageFlag(long long id_)
{
	for (int i = 0; i<GameView::getInstance()->chatPrivateNewMessageInfo.size();i++)
	{
		if (id_ ==GameView::getInstance()->chatPrivateNewMessageInfo.at(i)->playerid())
		{
			std::vector<CRelationPlayer *>::iterator iter= GameView::getInstance()->chatPrivateNewMessageInfo.begin() + i;
			auto playerInfo =*iter ;
			GameView::getInstance()->chatPrivateNewMessageInfo.erase(iter);
			delete playerInfo;
			i--;
		}
	}
		
	if (GameView::getInstance()->chatPrivateNewMessageInfo.size() > 0)
	{
		this->setNewMessageFlag(true);
	}else
	{
		this->setNewMessageFlag(false);
	}
}

void PrivateChatUi::showPrivateContent(std::string selectName )
{
	GameView::getInstance()->chatPrivateContentSourceVector.clear();
	for (int i=0;i< GameView::getInstance()->chatPrivateContentVector.size();i++)
	{
		//���vector ��ҵ����Լ�˽�ĵ�����˼�����������
		if (strcmp(selectName.c_str(),GameView::getInstance()->chatPrivateContentVector.at(i)->speakName.c_str())==0 ||
			strcmp(selectName.c_str(),GameView::getInstance()->chatPrivateContentVector.at(i)->listenerName.c_str())==0)
		{
			GameView::getInstance()->chatPrivateContentSourceVector.push_back(GameView::getInstance()->chatPrivateContentVector.at(i));
		}
	}
	auto contentui = (PrivateContent *)this->getChildByTag(ktagPrivateContentUi);
	contentui->chatContentTabview->reloadData();

	if(contentui->chatContentTabview->getContentSize().height > contentui->chatContentTabview->getViewSize().height)
		contentui->chatContentTabview->setContentOffset(contentui->chatContentTabview->maxContainerOffset(), false);	
}

void PrivateChatUi::setNewMessageFlag( bool isVisible_ )
{
	int size_ = GameView::getInstance()->chatPrivateNewMessageInfo.size();
	char num[10];
	sprintf(num,"%d",size_);
	image_newMessageFlag->setVisible(isVisible_);
	label_newMessageNum->setString(num);
}

void PrivateChatUi::setCellSelectIndex( int index )
{
	m_tabviewSelectIndex = index;
}

int PrivateChatUi::getCellSelectIndex()
{
	return m_tabviewSelectIndex;
}

////////////////////////////
NameTableCell::NameTableCell( void )
{

}

NameTableCell::~NameTableCell( void )
{

}

NameTableCell * NameTableCell::create( int idx )
{
	auto cell_ = new NameTableCell();
	if (cell_ && cell_->init(idx))
	{
		cell_->autorelease();
		return cell_;
	}
	CC_SAFE_DELETE(cell_);
	return NULL;
}

bool NameTableCell::init( int idx )
{
	if (TableViewCell::init())
	{
		int playerId_ = GameView::getInstance()->chatPrivateSpeakerVector.at(idx)->playerid();
		std::string playName_ = GameView::getInstance()->chatPrivateSpeakerVector.at(idx)->playername();
		int playerLevel_ = GameView::getInstance()->chatPrivateSpeakerVector.at(idx)->level();
		int profession_ = GameView::getInstance()->chatPrivateSpeakerVector.at(idx)->profession();
		int vipLevel_ = GameView::getInstance()->chatPrivateSpeakerVector.at(idx)->viplevel();
		int countryId = GameView::getInstance()->chatPrivateSpeakerVector.at(idx)->country();

		m_roleId = playerId_;
		m_roleName = playName_;

		auto iconBackGround =Sprite::create("res_ui/round.png");
		iconBackGround->setAnchorPoint(Vec2(0.5f,0.5f));
		iconBackGround->setPosition(Vec2(40,30));
		addChild(iconBackGround);

		std::string playerIcon_ = "res_ui/protangonis56x55/none.png";
		if (profession_ >0 && profession_ <5)
		{
			playerIcon_ = BasePlayer::getHeadPathByProfession(profession_);
		}
		
		/*
		auto friendHead =Sprite::create(playerIcon_.c_str());
		auto nameIcon_item = MenuItemSprite::create(friendHead, friendHead, friendHead, this, menu_selector(NameTableCell::callBackNameList));
		nameIcon_item->setZoomScale(0.6f);
		auto nameIcon_menu = CCMoveableMenu::create(nameIcon_item,NULL);
		nameIcon_menu->setAnchorPoint(Vec2(0.5f,0.5f));
		nameIcon_menu->setPosition(Vec2(-60,-23));
		//nameIcon_menu->setPosition(Vec2(iconBackGround->getContentSize().width/2,iconBackGround->getContentSize().height/2));
		iconBackGround->addChild(nameIcon_menu);
		nameIcon_menu->setScale(0.8f);
		*/

		auto layer = Layer::create();
		addChild(layer);

		auto btn_icon = Button::create();
		btn_icon->loadTextures(playerIcon_.c_str(),playerIcon_.c_str(),"");
		btn_icon->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_icon->addTouchEventListener(CC_CALLBACK_2(NameTableCell::callBackNameList,this));
		btn_icon->setTouchEnabled(true);
		btn_icon->setPosition(Vec2(40,30));
		btn_icon->setPressedActionEnabled(true);
		layer->addChild(btn_icon);

		btn_icon->setScale(0.8f);

		auto playerLvBg = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80_new.png");
		playerLvBg->setAnchorPoint(Vec2(0,0));
		playerLvBg->setPreferredSize(Size(35,12));
		playerLvBg->setPosition(Vec2(iconBackGround->getPositionX()+playerLvBg->getContentSize().width/2 - 8,4));
		addChild(playerLvBg);

		char string_lv[5];
		sprintf(string_lv,"%d",playerLevel_);
		std::string playerlevelStr_ = "LV";
		playerlevelStr_.append(string_lv);

		auto label_level = Label::createWithTTF(playerlevelStr_.c_str(), APP_FONT_NAME, 12);
		label_level->setAnchorPoint(Vec2(0.5,0));
		label_level->setPosition(Vec2(playerLvBg->getContentSize().width/2,0));
		playerLvBg->addChild(label_level,10);

		auto label_playName = Label::createWithTTF(playName_.c_str(), APP_FONT_NAME, 16);
		label_playName->setAnchorPoint(Vec2(0.5f,0));
		label_playName->setPosition(Vec2(115,30));
		addChild(label_playName,10);

		if (countryId >0 && countryId <6)
		{
			std::string countryIdName = "country_";
			char id[2];
			sprintf(id, "%d", countryId);
			countryIdName.append(id);
			std::string iconPathName = "res_ui/country_icon/";
			iconPathName.append(countryIdName);
			iconPathName.append(".png");

			auto countrySp_ = Sprite::create(iconPathName.c_str());
			countrySp_->setAnchorPoint(Vec2(0.5f,0));
			countrySp_->setPosition(Vec2(100,5));
			countrySp_->setScale(0.8f);
			countrySp_->setTag(10);
			addChild(countrySp_);
		}

		if (vipLevel_ > 0)
		{
			auto  widgetVip = MainScene::addVipInfoByLevelForNode(vipLevel_);
			addChild(widgetVip);
			widgetVip->setTag(11);
			widgetVip->setPosition(Vec2(135,16));
		}

		auto delete_ImageBg=Sprite::create("res_ui/close.png");
		auto delete_itemBg = MenuItemSprite::create(delete_ImageBg, delete_ImageBg, delete_ImageBg, CC_CALLBACK_1(NameTableCell::deleteSelectCell, this));
		delete_itemBg->setScale(0.5f);
		auto delete_name=CCMoveableMenu::create(delete_itemBg,NULL);
		delete_name->setAnchorPoint(Vec2(1,0));
		delete_name->setPosition(Vec2(260,29));
		delete_name->setTag(1011);
		addChild(delete_name);
		
		auto privateui_ = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);
		if (privateui_ != NULL)
		{
			if (privateui_->getCellSelectIndex() == idx )
			{
				iconBackGround->setPositionX(iconBackGround->getPositionX() - 5);
				//friendHead->setPositionX(friendHead->getPositionX() - 5);
				btn_icon->setPosition(Vec2(35,30));
				playerLvBg->setPositionX(playerLvBg->getPositionX() - 5);
				label_playName->setPositionX(label_playName->getPositionX() - 5);
				if (countryId >0 && countryId <6)
				{
					auto sp_= (Sprite *)getChildByTag(10);
					sp_->setPositionX(sp_->getPositionX() - 5);
				}
				if (vipLevel_ > 0)
				{
					auto node_ = (Node *)getChildByTag(11);
					node_->setPositionX(node_->getPositionX() - 5);
				}

				delete_name->setPosition(Vec2(175,29));
			}
		}
		
		return true;
	}
	return false;
}

void NameTableCell::onEnter()
{
	TableViewCell::onEnter();
}

void NameTableCell::onExit()
{
	TableViewCell::onExit();
}

void NameTableCell::deleteSelectCell(Ref *pSender)
{
	auto privateui_ = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);
	int index = privateui_->getCellSelectIndex();
	privateui_->selectPlayerId = GameView::getInstance()->chatPrivateSpeakerVector.at(index)->playerid();
	//std::string m_name = GameView::getInstance()->chatPrivateSpeakerVector.at(index)->playername();
	for (int i=0;i< GameView::getInstance()->chatPrivateSpeakerVector.size();i++)
	{
		if (privateui_->selectPlayerId == GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playerid())
		{
			std::vector<CRelationPlayer *>::iterator iter= GameView::getInstance()->chatPrivateSpeakerVector.begin() + i;
			auto playerInfo =*iter ;
			GameView::getInstance()->chatPrivateSpeakerVector.erase(iter);
			delete playerInfo;
		}
	}
	privateui_->nameTabview->reloadData();
	/*
	//ݱ���������� ݰ�����ɾ���ɲ�ɾ�  �������ʷ�����¼��
	for (int i=0;i< GameView::getInstance()->chatPrivateContentVector.size();i++)
	{
		if (strcmp(m_name.c_str(),GameView::getInstance()->chatPrivateContentVector.at(i)->speakName.c_str())==0 ||
					strcmp(m_name.c_str(),GameView::getInstance()->chatPrivateContentVector.at(i)->listenerName.c_str())==0)
		{
			std::vector<PrivateCell *>::iterator iter= GameView::getInstance()->chatPrivateContentVector.begin() + i;
			PrivateCell * contentStr_ =*iter ;
			GameView::getInstance()->chatPrivateContentVector.erase(iter);
			delete contentStr_;
			//GameView::getInstance()->chatPrivateContentSourceVector.push_back(GameView::getInstance()->chatPrivateContentVector.at(i));
		}
	}
	*/
	if (GameView::getInstance()->chatPrivateSpeakerVector.size()<= index)
	{
		index--;
	}

	if (GameView::getInstance()->chatPrivateSpeakerVector.size() > 0)
	{
		privateui_->selectPlayerName = GameView::getInstance()->chatPrivateSpeakerVector.at(index)->playername();
		privateui_->nameTabview->cellAtIndex(index);
	}else
	{
		privateui_->selectPlayerName = "";
	}
	
	privateui_->showPrivateContent(privateui_->selectPlayerName);
}

void NameTableCell::callBackNameList(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		auto privateui_ = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);

		Size winSize = Director::getInstance()->getVisibleSize();
		auto nameList = NameList::create(m_roleId, m_roleName);
		nameList->setIgnoreAnchorPointForPosition(false);
		nameList->setAnchorPoint(Vec2(0, 0));
		//nameList->setPosition(Vec2(50,winSize.height/2 - nameList->getContentSize().height/2+20));
		nameList->setPosition(Vec2(-80, winSize.height / 2 - nameList->getContentSize().height / 2 + 20));
		privateui_->layer2->addChild(nameList);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}


////////////////////////////
NameList::NameList( void )
{

}

NameList::~NameList( void )
{

}

NameList * NameList::create(long long id_,std::string name_)
{
	auto pui = new NameList();
	if (pui && pui->init(id_,name_))
	{
		pui->autorelease();
		return pui;
	}
	CC_SAFE_DELETE(	pui);
	return NULL;
}

bool NameList::init(long long id_,std::string name_)
{
	if(UIScene::init())
	{
		Size s=Director::getInstance()->getVisibleSize();

		const char *str_addfriend = StringDataManager::getString("friend_addfriend");
		char *addfriend_left=const_cast<char*>(str_addfriend);	

		const char *str_check = StringDataManager::getString("friend_check");
		char *check_left=const_cast<char*>(str_check);

		const char *str_team = StringDataManager::getString("friend_team");
		char *team_left=const_cast<char*>(str_team);

		const char *str_mail = StringDataManager::getString("friend_mail");
		char *mail_left=const_cast<char*>(str_mail);

		const char *str_black = StringDataManager::getString("friend_black");
		char *black_left=const_cast<char*>(str_black);

		auto panel = Layout::create();
		panel->setContentSize(Size(100, 200));
		panel->setPosition(Vec2(200,150));
		const char * normalImage = "res_ui/new_button_5.png";
		const char * selectImage ="res_ui/new_button_5.png";
		const char * finalImage = "";
		char * label[] = {check_left,team_left,addfriend_left,mail_left,black_left};

		m_roleId = id_;
		m_roleName = name_;

		auto privateTab = UITab::createWithText(5,normalImage,selectImage,finalImage,label,VERTICAL,-2);
		privateTab->setAnchorPoint(Vec2(0,0));
		privateTab->setPosition(Vec2(0,0));
		privateTab->setHighLightImage((char*)finalImage);
		privateTab->addIndexChangedEvent(this,coco_indexchangedselector(NameList::callBackFriendList));
		privateTab->setPressedActionEnabled(true);
		panel->addChild(privateTab);
		m_pLayer->addChild(panel);

		setContentSize(Size(100,200));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}

void NameList::onEnter()
{
	UIScene::onEnter();
}

void NameList::onExit()
{
	UIScene::onExit();
}

bool NameList::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return this->resignFirstResponder(pTouch,this,false,false);
}

void NameList::callBackFriendList( Ref * obj )
{
	auto tab =(UITab *)obj;
	int num = tab->getCurrentIndex();
	
	Size winSize =Director::getInstance()->getWinSize();

	switch(num)
	{
	case 0:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)m_roleId);
		}break;
	case 1:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1401,(void *)m_roleId);
		}break;
	case 2:
		{
			FriendStruct friend1={0,0,m_roleId,m_roleName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	case 3:
		{
			auto mail_ui=MailUI::create();
			mail_ui->setIgnoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(Vec2(0.5f,0.5f));
			mail_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			mail_ui->friendName->onTextFieldInsertText(NULL,m_roleName.c_str(),30);
		}break;
	case 4:
		{
			FriendStruct friend1={0,1,m_roleId,m_roleName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	}
	removeFromParentAndCleanup(true);
}
