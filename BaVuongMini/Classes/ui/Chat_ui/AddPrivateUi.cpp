#include "AddPrivateUi.h"
#include "../extensions/UITab.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "ChatUI.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../Mail_ui/MailUI.h"
#include "../extensions/RichTextInput.h"
#include "../extensions/QuiryUI.h"
#include "../Mail_ui/MailFriend.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"

#define UITAB_VERTICAL_INTERVAL_SIZE (-2)

typedef enum{
	ACOUSTIC_SELECT = 0,
	WORLD_SELECT,
	COUNTRY_SELECT,
	CAMP_SELECT,
	PRIVATE_SELECT,
	TEAM_SELECT,
	CURRECT_SELECT
}selectButton;

AddPrivateUi::AddPrivateUi(void)
{
}


AddPrivateUi::~AddPrivateUi(void)
{

}

AddPrivateUi * AddPrivateUi::create(int num)
{
	auto pui=new AddPrivateUi();
	if (pui && pui->init(num))
	{
		pui->autorelease();
		return pui;
	}
	CC_SAFE_DELETE(	pui);
	return NULL;
}

bool AddPrivateUi::init(int num)
{
	if(UIScene::init())
	{
		Size s=Director::getInstance()->getVisibleSize();
		currType =num;
		if (currType==1)
		{
			auto background=ImageView::create();
			background->loadTexture("res_ui/liaotian/haoyoudi.png");
			background->setAnchorPoint(Vec2(0,0));
			background->setPosition(Vec2(0,0));
			m_pLayer->addChild(background);

			auto button_friend = Button::create();
			button_friend->setPressedActionEnabled(true);
			button_friend->setTouchEnabled(true);
			button_friend->loadTextures("res_ui/button_6_on.png","res_ui/button_6_on.png","");
			button_friend->setTitleFontSize(25);
			button_friend->setAnchorPoint(Vec2(0.5f,0.5f));
			button_friend->setPosition(Vec2(45,33));
			button_friend->addTouchEventListener(CC_CALLBACK_2(AddPrivateUi::callBackFriend, this));
			background->addChild(button_friend);

			auto labelFriend= Label::createWithTTF("好友", APP_FONT_NAME,25);
			labelFriend->setAnchorPoint(Vec2(0.5f,0.5f));
			labelFriend->setPosition(Vec2(0,0));
			button_friend->addChild(labelFriend);
			
			auto inputBox=ImageView::create();
			inputBox->loadTexture("res_ui/liaotian/haoyoukuang.png");
			inputBox->setAnchorPoint(Vec2(0.5f,0.5f));
			inputBox->setPosition(Vec2(140,35));
			inputBox->setScale9Enabled(true);
			inputBox->setContentSize(Size(120,40));//130 40
			background->addChild(inputBox);

			textBox_private=new RichTextInputBox();
			textBox_private->setInputBoxWidth(100);
			textBox_private->setAnchorPoint(Vec2(0,0));
			textBox_private->setPosition(Vec2(85,15));
			textBox_private->setCharLimit(30);
			addChild(textBox_private, 0,RICHBOXINPUT_TAG );
			textBox_private->autorelease();

			auto closeButton=Button::create();
			closeButton->setPressedActionEnabled(true);
			closeButton->setTouchEnabled(true);
			closeButton->loadTextures("res_ui/close.png","res_ui/close.png","");
			closeButton->setPosition(Vec2(222,32));
			closeButton->addTouchEventListener(CC_CALLBACK_2(AddPrivateUi::callBackClosed, this));
			background->addChild(closeButton);

		}else
		if(currType==2)
		{
			const char *str_addfriend = StringDataManager::getString("friend_addfriend");
			char *addfriend_left=const_cast<char*>(str_addfriend);	

			const char *str_check = StringDataManager::getString("friend_check");
			char *check_left=const_cast<char*>(str_check);

			const char *str_team = StringDataManager::getString("friend_team");
			char *team_left=const_cast<char*>(str_team);

			const char *str_mail = StringDataManager::getString("friend_mail");
			char *mail_left=const_cast<char*>(str_mail);

			const char *str_black = StringDataManager::getString("friend_black");
			char *black_left=const_cast<char*>(str_black);

			const char *str_private = StringDataManager::getString("chatui_private");
			char *private_left=const_cast<char*>(str_private);

			auto panel = Layout::create();
			panel->setContentSize(Size(403, 335));
			panel->setPosition(Vec2(200,150));
			const char * normalImage = "res_ui/new_button_5.png";
			const char * selectImage ="res_ui/new_button_5.png";
			const char * finalImage = "";
			char * label[] = {private_left,check_left,team_left,addfriend_left,mail_left,black_left};
			auto privateTab=UITab::createWithText(6,normalImage,selectImage,finalImage,label,VERTICAL,UITAB_VERTICAL_INTERVAL_SIZE);
			privateTab->setAnchorPoint(Vec2(0,0));
			privateTab->setPosition(Vec2(0,0));
			privateTab->setHighLightImage((char*)finalImage);
			privateTab->addIndexChangedEvent(this,coco_indexchangedselector(AddPrivateUi::callBackFriendList));
			privateTab->setPressedActionEnabled(true);
			panel->addChild(privateTab);
			m_pLayer->addChild(panel);

			setContentSize(Size(100,240));
		}else
		if (currType==3)
		{	
			const char *str_world = StringDataManager::getString("chatui_world");
			char *world_left=const_cast<char*>(str_world);	

			const char *str_courtny = StringDataManager::getString("chatui_country");
			char *courtny_left=const_cast<char*>(str_courtny);	

			const char *str_camp = StringDataManager::getString("chatui_camp");
			char *camp_left=const_cast<char*>(str_camp);	

			const char *str_team = StringDataManager::getString("chatui_team");
			char *team_left=const_cast<char*>(str_team);	

			const char *str_curr = StringDataManager::getString("chatui_current");
			char *curr_left=const_cast<char*>(str_curr);	
			
			const char *str_private = StringDataManager::getString("chatui_private");
			char *private_left=const_cast<char*>(str_private);	
			
			const char *str_acoustic = StringDataManager::getString("chatui_acoustic");
			char *acoustic_left=const_cast<char*>(str_acoustic);	
			
			const char * Chaneel_normalImage="res_ui/new_button_9.png";
			const char * Channel_selectImage = "res_ui/new_button_9.png";
			const char * Channel_finalImage = "";
			const char * Chaneel_highLightImage="res_ui/new_button_9.png";
			char *left[]={acoustic_left,world_left,courtny_left,camp_left,private_left,team_left,curr_left};
			auto tabSelectButton=UITab::createWithText(7,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,left,VERTICAL,-3);
			tabSelectButton->setAnchorPoint(Vec2(0.5f,0.5f));
			tabSelectButton->setPosition(Vec2(0,0));
			tabSelectButton->setHighLightImage((char *) Chaneel_highLightImage);
			tabSelectButton->setDefaultPanelByIndex(1);
			tabSelectButton->setAutoClose(true);
			tabSelectButton->setPressedActionEnabled(true);
			tabSelectButton->addIndexChangedEvent(this,coco_indexchangedselector(AddPrivateUi::selectButton_channel));
			m_pLayer->addChild(tabSelectButton);
  			setContentSize(Size(58,252));

		}else if (currType==3)
		{

		}
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(AddPrivateUi::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(AddPrivateUi::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(AddPrivateUi::onTouchMoved, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void AddPrivateUi::onEnter()
{
	UIScene::onEnter();
	//this->openAnim();
}
void AddPrivateUi::onExit()
{
	UIScene::onExit();
}

bool AddPrivateUi::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	if (currType==1)
	{
		return false;
	}else
	{
		return this->resignFirstResponder(pTouch,this,false,false);
	}
}

void AddPrivateUi::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}
void AddPrivateUi::onTouchEnded( Touch *pTouch, Event *pEvent )
{
	
}
void AddPrivateUi::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}
void AddPrivateUi::callBackClosed(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

	}
	break;

	case Widget::TouchEventType::CANCELED:
	{
		removeFromParentAndCleanup(true);
		//this->closeAnim();
	}
		break;

	default:
		break;
	}


}

void AddPrivateUi::callBackFriend(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add friends name
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagPopFriendListUI) == NULL)
		{
			Size winsize = Director::getInstance()->getVisibleSize();
			auto mailFriend = MailFriend::create(kTabChat);
			mailFriend->setIgnoreAnchorPointForPosition(false);
			mailFriend->setAnchorPoint(Vec2(0.5f, 0.5f));
			mailFriend->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			GameView::getInstance()->getMainUIScene()->addChild(mailFriend, 5, kTagPopFriendListUI);

			FriendList friend1 = { 0,20,0,0 };
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202, &friend1);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void AddPrivateUi::callBackFriendList( Ref * obj )
{
	auto tab =(UITab *)obj;
	int num = tab->getCurrentIndex();
	auto chatui =(ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
	Size winSize =Director::getInstance()->getWinSize();
	long long roleGuildId_ = GameView::getInstance()->myplayer->getRoleId();
	
	switch(num)
	{
	case PRIVATECHAT:
		{
			if (roleGuildId_ == chatui->playerId_)
			{
				//chatui_priveateOfSelf  chatui_mailOfSelf
				const char *strings_ = StringDataManager::getString("chatui_priveateOfSelf");
				GameView::getInstance()->showAlertDialog(strings_);
				removeFromParentAndCleanup(true);
				return;
			}

			auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene->addPrivateChatUi(chatui->playerId_,chatui->playerName_,chatui->countryId,chatui->playerVipLevel,chatui->playerLevel,chatui->pressionId);
		}break;
	case CHECK:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)chatui->playerId_);
		}break;
	case TEAM:
		{
			int selectId=chatui->playerId_;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1401,(void *)selectId);
		}break;
	case FRIEND:
		{
			FriendStruct friend1={0,0,chatui->playerId_,chatui->playerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	case MAIL:
		{
			if (roleGuildId_ == chatui->playerId_)
			{
				const char *strings_ = StringDataManager::getString("chatui_mailOfSelf");
				GameView::getInstance()->showAlertDialog(strings_);
				removeFromParentAndCleanup(true);
				return;
			}

			auto mail_ui=MailUI::create();
			mail_ui->setIgnoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(Vec2(0.5f,0.5f));
			mail_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			std::string playerName=chatui->playerName_;	
			mail_ui->friendName->onTextFieldInsertText(NULL,playerName.c_str(),30);
			
		}break;
	case BLACKLIST:
		{
			chatui->operation_ =0;
			chatui->relationtype_=1;
			FriendStruct friend1={0,1,chatui->playerId_,chatui->playerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	}
	removeFromParentAndCleanup(true);
}

void AddPrivateUi::selectButton_channel( Ref * obj )
{
	int num= ((UITab *) obj)->getCurrentIndex();
	auto chatui= (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
	//if private is have remove
	auto privateui_ =(AddPrivateUi *)chatui->layer_3->getChildByTag(PRIVATEUI);
	if (privateui_ != NULL)
	{
		privateui_->removeFromParentAndCleanup(true);
	}

	switch (num)
	{
	case ACOUSTIC_SELECT:
		{
			const char *strings = StringDataManager::getString("chatui_acoustic");
			chatui->selectChannelId = 9;//-----
			chatui->labelchanel->setString(strings);
		}break;
	case WORLD_SELECT:
		{
			const char *strings = StringDataManager::getString("chatui_world");
			chatui->selectChannelId=1;
			chatui->labelchanel->setString(strings);
		}break;
	case COUNTRY_SELECT:
		{
			const char *strings = StringDataManager::getString("chatui_country");
			chatui->selectChannelId=8;
			chatui->labelchanel->setString(strings);
		}break;
	case CAMP_SELECT:
		{
			long long roleGuildId_ = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionid();
			if (roleGuildId_==0)
			{
				const char *strings = StringDataManager::getString("chatui_familyOfchat");
				GameView::getInstance()->showAlertDialog(strings);
			}else
			{
				const char *strings = StringDataManager::getString("chatui_camp");
				chatui->selectChannelId=6;
				chatui->labelchanel->setString(strings);
			}

		}break;
	case PRIVATE_SELECT:
		{
			const char *strings = StringDataManager::getString("chatui_private");
			chatui->selectChannelId=0;
			chatui->addPrivate();
			chatui->labelchanel->setString(strings);
		}break;
	case TEAM_SELECT:
		{
			int vectorSize = GameView::getInstance()->teamMemberVector.size();
			if (vectorSize > 0)
			{
				const char *strTeam = StringDataManager::getString("chatui_team");
				chatui->selectChannelId=5;
				chatui->labelchanel->setString(strTeam);
			}else
			{
				const char *strings = StringDataManager::getString("chatui_teamOfchat");
				GameView::getInstance()->showAlertDialog(strings);
			}
				
		}break;
	case CURRECT_SELECT:
		{
			const char *strings = StringDataManager::getString("chatui_current");
			chatui->selectChannelId=3;
			chatui->labelchanel->setString(strings);
		}break;	
	}
	
	this->removeFromParentAndCleanup(true);
}



