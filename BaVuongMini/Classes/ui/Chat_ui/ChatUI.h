﻿#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
class Tablist;
class ExpressionUi;
class BackPackage;
class ChatCell;
class RichTextInputBox;

typedef enum{
	RICHTEXTINPUT_TAG=201,
	TABLIST_TAG=202,
	PRIVATEUI=203,
	PACKAGEAGE=204,
	CHATLayer3=240
	
};
class ChatUI:public UIScene
{
public:
	ChatUI(void);
	~ChatUI(void);

	static ChatUI *create();
	bool init();
	void onEnter();
	
	void callBackDelete(Ref *pSender, Widget::TouchEventType type);
	void callBackExpression(Ref *pSender, Widget::TouchEventType type);
	void callbackBackPakage(Ref *pSender, Widget::TouchEventType type);
	void callBackSend(Ref *pSender, Widget::TouchEventType type);

	void callBackExit(Ref *pSender, Widget::TouchEventType type);
	void callBackRoll(Ref *pSender, Widget::TouchEventType type);

	void addPrivate();
	void addCahannelSelect(Ref *pSender, Widget::TouchEventType type);
	void ChangeLeftPanelByIndex(Ref *obj);

 	bool onTouchBegan(Touch *pTouch, Event *pEvent);

	Button *btn_rollBg;
	bool whetherOfRoll;
	bool selectButton;
	static bool  redRight;
	int selectChannelId;
    std::string chatContent;
	const char * privateName;
	Text * labelchanel;

	static int sendSecond;
	Size curTabViewOff;
	int getChannelIndex();
	void refreshChatUIInfoByChanelIndex(int index);

	struct ChatInfoStruct
	{
		int channelId;
		std::string chatContent;
		long long listenerId;
		std::string listenerName;
	};

	static void addToMainChatUiMessage(int channel_id,std::string message);
private:
	void refreshChatCellsByChannelId( int channelId );
public:
	Layer* layer_1;
	Layer* layer_3;
	int operation_ ;
	int relationtype_;
	long long playerId_;
	std::string playerName_;
	Tablist* listLayer;
	RichTextInputBox * textBox;
	UITab*  channelLabel;

	int countryId;
	int playerVipLevel;
	int playerLevel;
	int pressionId;

private:
	Size winsize;
	ImageView *redRight_sp;
	Button * buttonChannel_;
	Button * buttonExpression_;

};

