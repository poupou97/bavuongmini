#ifndef _H_PRIVATECELL_
#define _H_PRIVATECELL_
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CCRichLabel;

class PrivateCell:public TableViewCell
{
public:
	PrivateCell(void);
	~PrivateCell(void);
	static PrivateCell * create(int pressionId_,std::string play_name,std::string chatContent,long long playerid,int viplevel,std::string listener_Name = "");
	bool init(int pressionId_,std::string play_name,std::string chatContent,long long playerid,int viplevel,std::string listener_Name = "");
	
	void onEnter();
	void onExit();
	
	void LinkEvent(Ref * obj);
	void NameEvent(Ref* pSender);

	void setCurCellHeight(int height);
	int getCurCellHeight();

	void callBackNameList(Ref *pSender, Widget::TouchEventType type);

public:
	std::string speakName;
	std::string listenerName;
	long long speakerId;

private:
	int m_curCellHeight;
};
#endif
