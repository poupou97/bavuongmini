#include "ExpressionLayer.h"
#include "../extensions/RichTextInput.h"
#include "../Chat_ui/ChatUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "PrivateChatUi.h"

ExpressionLayer::ExpressionLayer(void)
{	
}


ExpressionLayer::~ExpressionLayer(void)
{

}
ExpressionLayer * ExpressionLayer::create(int uitag)
{
	auto exLayer=new ExpressionLayer();
	if (exLayer && exLayer->init(uitag))
	{
		exLayer->autorelease();
		return exLayer;
	}
	CC_SAFE_DELETE(exLayer);
	return NULL;

}

bool ExpressionLayer::init(int uitag)
{
	if (UIScene::init())
	{
		Size s=Director::getInstance()->getVisibleSize();

		if(LoadSceneLayer::expressPanel->getParent() != NULL)
		{
			LoadSceneLayer::expressPanel->removeFromParentAndCleanup(false);
		}

		auto panel=LoadSceneLayer::expressPanel;
		panel->setAnchorPoint(Vec2(0,0));
		panel->setTouchEnabled(true);
		panel->setPosition(Vec2(0,0));
		m_pLayer->addChild(panel);
		uitagIndex = uitag;
		index=1;
		for (int j=0;j<3;j++)
		{
			for(int i=0;i<10;i++)
			{
			
				char exName[50];
				if (index<=9)
				{
					sprintf(exName, "res_ui/Expression/e0%d.png", index);
				}
				else
				{
					sprintf(exName, "res_ui/Expression/e%d.png", index);
				}

				auto expButton=Button::create();
				expButton->setTouchEnabled(true);
				expButton->loadTextures(exName,exName,"");
				expButton->setAnchorPoint(Vec2(0,0));
				expButton->setPosition(Vec2(20+45* i,143-j*60));//38,45
				expButton->setTag(index);
				expButton->addTouchEventListener(CC_CALLBACK_2(ExpressionLayer::callBack, this));
				m_pLayer->addChild(expButton);
				index++;	
			}
		}

 		////this->setTouchEnabled(true);
  		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
 		this->setContentSize(Size(480,200));

		return true;
	}
	return false;
}

void ExpressionLayer::onEnter()
{
	UIScene::onEnter();
}



bool ExpressionLayer::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return true;
}

void ExpressionLayer::onTouchEnded( Touch *pTouch, Event *pEvent )
{
	removeFromParentAndCleanup(true);
}

void ExpressionLayer::callBack(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

	}
	break;

	case Widget::TouchEventType::CANCELED:
	{
		auto btn = (Button*)pSender;
		int bunindx = btn->getTag();

		char str[5];
		if (bunindx <= 9)
		{
			sprintf(str, "/e0%d", bunindx);
		}
		else
		{
			sprintf(str, "/e%d", bunindx);
		}

		switch (uitagIndex)
		{
		case kTabChat:
		{
			auto chatui = (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
			auto text = (RichTextInputBox *)chatui->getChildByTag(CHATLayer3)->getChildByTag(RICHTEXTINPUT_TAG);
			text->onTextFieldInsertText(NULL, str, 4);
		}break;
		case ktagPrivateUI:
		{
			auto privateui = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);
			privateui->textInputBox->onTextFieldInsertText(NULL, str, 4);
		}break;
		}
	}
		break;

	default:
		break;
	}


}
