#pragma once

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class ChatUI;
class PacPageView;
class CEquipment;
class UITab;
#define  ROLELAYEROFEQUIP_BACKPAC 101
class BackPackage:public UIScene, public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate
{
public:
	BackPackage(void);
	~BackPackage(void);

	static BackPackage * create(int uitag);
	bool init(int uitag);

	void onEnter();
	void onExit();

	void refreshCurRoleValue(int idx);
	void callBackShowEquipInfo(Ref *pSender, Widget::TouchEventType type);
	void callBackClose(Ref *pSender, Widget::TouchEventType type);
	
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	void callBackPackAndRole(Ref * obj);
	bool onTouchBegan(Touch *pTouch, Event *pEvent);
	void onTouchEnded(Touch *pTouch, Event *pEvent);
	void CurrentPageViewChanged(Ref *pSender, PageView::EventType type);
	void callBack(Ref * obj);
	void callBackOfPrivateUi();
	void getCurRolequipment(int idx);

	void PageScrollToDefault();

	UITab * roleAndPack;
	int curRoleIndex;
public:
	Size winsize;
	Layer * m_Layer;
	Layer * m_LayerRole;
	Layer * tempLayer_flag;
	int curType;
	std::string weapEffectStrForRefine;
	std::string weapEffectStrForStar;
	PacPageView *pageView;
	ImageView* currentPage;
	std::vector<CEquipment*>playerEquipments;
	ImageView * imageIcon_;
	Text * labelLevel;
	Text * labelFightValue_;
	Label * labelAllFightValue_;
	
	Layout * panelRole;
	Layout * panelPac ;
	ImageView * pageView_kuang;

	//
	ImageView * image_accessories;
	ImageView * image_shoes;
	ImageView * image_helmet;
	ImageView * image_ring;
	ImageView * image_clothes;
	ImageView * image_arms;

};

///////////////////////////////// equip goodsifno 
class GoodsInfo;
class EquipmentInfoShow:public GoodsItemInfoBase
{
public:
	EquipmentInfoShow(void);
	~EquipmentInfoShow(void);

	static EquipmentInfoShow *create(GoodsInfo * goods,std::vector<CEquipment *> equipvector,long long generalId);
	bool init(GoodsInfo * goods,std::vector<CEquipment *> equipvector,long long generalId);

	void onEnter();
	void onExit();

	void callBackShowEquip(Ref *pSender, Widget::TouchEventType type);

private:
	GoodsInfo * goods_;
};


