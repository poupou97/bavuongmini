#include "ChatUI.h"
#include "../../logo_state/LogoState.h"
#include "BackPackage.h"
#include "../extensions/RichTextInput.h"
#include "ExpressionLayer.h"
#include "AddPrivateUi.h"
#include "../extensions/RichElement.h"
#include "../extensions/RichTextInput.h"
#include "../../messageclient/ClientNetEngine.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../GameView.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/UITab.h"
#include "Tablist.h"
#include "ChatCell.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "ChatInstance.h"
#include "cocostudio\CCProcessBase.h"

typedef enum {
	WHOLE_LABEL = 0,
	WORLD_LABEL,
	COUNTRY_LABEL,
	FAMILY_LABEL,
	TEAM_LABEL,
	CURRECT_LABEL,
	PRIVATE_LABEL,
	SYSTEM_LABEL,
	ACOUSTIC_LABEL,
	
}label;

ChatUI::ChatUI(void)
{
	whetherOfRoll=true;
	selectChannelId=1;
}


ChatUI::~ChatUI(void)
{
}

ChatUI * ChatUI::create()
{
	auto chatUi=new ChatUI();
	if (chatUi && chatUi->init())
	{
		chatUi->autorelease();
		return chatUi;
	}
	CC_SAFE_DELETE(chatUi);
	return NULL;

}

bool ChatUI::init()
{
	if (UIScene::init())
	{
		winsize=Director::getInstance()->getVisibleSize();
		
		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		if(LoadSceneLayer::chatPanel->getParent() != NULL)
		{
			LoadSceneLayer::chatPanel->removeFromParentAndCleanup(false);
		}
		auto ppanel = LoadSceneLayer::chatPanel;
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		layer_1 = Layer::create();
		layer_1->setIgnoreAnchorPointForPosition(false);
		layer_1->setAnchorPoint(Vec2(0.5f,0.5f));
		layer_1->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layer_1->setContentSize(Size(800,480));
		this->addChild(layer_1, 0);
		//layer_1->addChild(ppanel);

		const char * secondStr = StringDataManager::getString("chat_diaoshi_liao");
		const char * thirdStr = StringDataManager::getString("chat_diaoshi_tian");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		layer_1->addChild(atmature);

		layer_3 = Layer::create();
		layer_3->setIgnoreAnchorPointForPosition(false);
		layer_3->setAnchorPoint(Vec2(0.5f,0.5f));
		layer_3->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layer_3->setContentSize(Size(800,480));
		addChild(layer_3,2,CHATLayer3);

// 		ImageView * pageView_kuang = ImageView::create();
// 		pageView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setScale9Enabled(true);
// 		pageView_kuang->setContentSize(Size(684,314));
// 		pageView_kuang->setCapInsets(Rect(32,32,1,1));
// 		pageView_kuang->setAnchorPoint(Vec2(0,0));
// 		pageView_kuang->setPosition(Vec2(65,95));
// 		layer_3->addChild(pageView_kuang);

		auto ppane_3 = Layout::create();
		ppane_3->setAnchorPoint(Vec2(0,0));
		ppane_3->setPosition(Vec2(0,0));
		ppane_3->setLocalZOrder(5);
		layer_3->addChild(ppane_3);
		
		textBox=new RichTextInputBox();
		textBox->setInputMode(RichTextInputBox::kCCInputModeAppendOnly);
		textBox->setInputBoxWidth(352);
		textBox->setAnchorPoint(Vec2(0,0));
		textBox->setPosition(Vec2(139,45));//68
		layer_3->addChild(textBox, 2, RICHTEXTINPUT_TAG);
        textBox->setCharLimit(50);
		textBox->autorelease();

		const char *str_synthesize = StringDataManager::getString("chatui_synthesize");
		char *synthesize_left=const_cast<char*>(str_synthesize);

		const char *str_world = StringDataManager::getString("chatui_world");
		char *world_left=const_cast<char*>(str_world);
		
		const char *str_courtny = StringDataManager::getString("chatui_country");
		char *courtny_left=const_cast<char*>(str_courtny);

		const char *str_camp = StringDataManager::getString("chatui_camp");
		char *camp_left=const_cast<char*>(str_camp);
		
		const char *str_team = StringDataManager::getString("chatui_team");
		char *team_left=const_cast<char*>(str_team);	

		const char *str_curr = StringDataManager::getString("chatui_current");
		char *curr_left=const_cast<char*>(str_curr);

		const char *str_private = StringDataManager::getString("chatui_private");
		char *private_left=const_cast<char*>(str_private);

		const char *str_system = StringDataManager::getString("chatui_system");
		char *system_left=const_cast<char*>(str_system);

		const char *str_Acoustic = StringDataManager::getString("chatui_acoustic");
		char *acoustic_left=const_cast<char*>(str_Acoustic);
		//////////CCTab  频道切换标签
		const char * normalImage = "res_ui/tab_2_off.png";
		const char * selectImage ="res_ui/tab_2_on.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_2_on.png";
		char *left[]={synthesize_left,world_left,courtny_left,camp_left,team_left,curr_left,private_left,system_left,acoustic_left};
		channelLabel= UITab::createWithText(9,normalImage,selectImage,finalImage,left,HORIZONTAL,2);
		channelLabel->setAnchorPoint(Vec2(0,0));
		channelLabel->setPosition(Vec2(64,410));
		channelLabel->setPressedActionEnabled(true);
		channelLabel->setHighLightImage((char * )highLightImage);
		channelLabel->setDefaultPanelByIndex(0);
		channelLabel->addIndexChangedEvent(this,coco_indexchangedselector(ChatUI::ChangeLeftPanelByIndex));
		channelLabel->setLocalZOrder(200);
		channelLabel->setScale(0.95f);
		ppane_3->addChild(channelLabel);
		//自动滚动 
		btn_rollBg=Button::create();
		btn_rollBg->setTouchEnabled(true);
		btn_rollBg->setPressedActionEnabled(true);
		btn_rollBg->loadTextures("res_ui/liaotian/suo_off.png","res_ui/liaotian/suo_off.png","");
		btn_rollBg->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_rollBg->setPosition(Vec2(650,375));
		btn_rollBg->setScale(0.8f);
		btn_rollBg->addTouchEventListener(CC_CALLBACK_2(ChatUI::callBackRoll, this));
		ppane_3->addChild(btn_rollBg);

		auto buttonDelete =(Button *)Helper::seekWidgetByName(ppanel,"Button_delete");
		buttonDelete->setTouchEnabled(true);
		buttonDelete->setPressedActionEnabled(true);
		buttonDelete->addTouchEventListener(CC_CALLBACK_2(ChatUI::callBackDelete, this));

		labelchanel=(Text*)Helper::seekWidgetByName(ppanel,"labelchanel");

		buttonChannel_ =(Button *)Helper::seekWidgetByName(ppanel,"Button_channel");
		buttonChannel_->setTouchEnabled(true);
		buttonChannel_->setPressedActionEnabled(true);
		buttonChannel_->addTouchEventListener(CC_CALLBACK_2(ChatUI::addCahannelSelect, this));
		//const char *stringsWorld = StringDataManager::getString("chatui_current");
		labelchanel->setString(world_left);

		buttonExpression_ =(Button *)Helper::seekWidgetByName(ppanel,"Button_expression");
		buttonExpression_->setTouchEnabled(true);
		buttonExpression_->setPressedActionEnabled(true);
		buttonExpression_->addTouchEventListener(CC_CALLBACK_2(ChatUI::callBackExpression, this));

		auto buttonPack_ =(Button *)Helper::seekWidgetByName(ppanel,"Button_backpack");
		buttonPack_->setTouchEnabled(true);
		buttonPack_->setPressedActionEnabled(true);
		buttonPack_->addTouchEventListener(CC_CALLBACK_2(ChatUI::callbackBackPakage, this));

		auto buttonSend_ =(Button *)Helper::seekWidgetByName(ppanel,"Button_sendOut");
		buttonSend_->setTouchEnabled(true);
		buttonSend_->setPressedActionEnabled(true);
		buttonSend_->addTouchEventListener(CC_CALLBACK_2(ChatUI::callBackSend, this));
		//exitButton
		auto button_close= (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		button_close->setPressedActionEnabled(true);
		button_close->setTouchEnabled(true);
		button_close->addTouchEventListener(CC_CALLBACK_2(ChatUI::callBackExit, this));
		
		GameView::getInstance()->selectCell_vdata.clear();
		this->refreshChatCellsByChannelId(-1);
		
		listLayer=Tablist::create();
		listLayer->setIgnoreAnchorPointForPosition(false);
		listLayer->setAnchorPoint(Vec2(0,0));
		listLayer->setPosition(Vec2(80,102));
		layer_1->addChild(listLayer,1,TABLIST_TAG);
		
		auto tableView = (TableView*)listLayer->getChildByTag(TABVIEW_TAG);
		tableView->reloadData();
		if(tableView->getContentSize().height > tableView->getViewSize().height)
			tableView->setContentOffset(tableView->maxContainerOffset(), false);

		////setTouchEnabled(true);
		//setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		setContentSize(winsize);
		
		return true;
	}
	return false;
}

void ChatUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	int m_index = ChatInstance::getInstance()->getCurChannel();
	channelLabel->setDefaultPanelByIndex(m_index);
	this->ChangeLeftPanelByIndex(channelLabel);

}

void ChatUI::addCahannelSelect(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto UIprivate = AddPrivateUi::create(3);
		UIprivate->setIgnoreAnchorPointForPosition(false);
		UIprivate->setAnchorPoint(Vec2(0, 0));
		UIprivate->setPosition(Vec2(buttonChannel_->getPosition().x - buttonChannel_->getContentSize().width / 2,
			buttonChannel_->getPosition().y + UIprivate->getContentSize().height + 7));
		layer_3->addChild(UIprivate, 4);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}



}

void ChatUI::callBackExpression(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto ex = ExpressionLayer::create(kTabChat);
		ex->setIgnoreAnchorPointForPosition(false);
		ex->setAnchorPoint(Vec2(0.5f, 0));
		ex->setPosition(Vec2(407, buttonExpression_->getPosition().y + buttonExpression_->getContentSize().height));
		layer_3->addChild(ex, 4);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void ChatUI::callbackBackPakage(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		if (layer_3->getChildByTag(PACKAGEAGE) != NULL)
			return;

		auto backbag = BackPackage::create(kTabChat);
		backbag->setIgnoreAnchorPointForPosition(false);
		backbag->setAnchorPoint(Vec2(0.5f, 0.5f));
		backbag->setPosition(Vec2(layer_3->getContentSize().width / 2, layer_3->getContentSize().height / 2));
		layer_3->addChild(backbag, 4, PACKAGEAGE);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void ChatUI::addPrivate( )
{
	auto privateui_ =(AddPrivateUi *)layer_3->getChildByTag(PRIVATEUI);
	if (privateui_ == NULL)
	{
		auto UIprivate=AddPrivateUi::create(1);
		UIprivate->setIgnoreAnchorPointForPosition(false);
		UIprivate->setAnchorPoint(Vec2(0,0));
		UIprivate->setPosition(Vec2(450,300));
		layer_3->addChild(UIprivate,4,PRIVATEUI);
	}
	
}

void ChatUI::callBackSend(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		const char* inputStr = textBox->getInputString();
		if (inputStr == NULL)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("chatui_callBackSend_null"));
			return;
		}

		chatContent = inputStr;
		if (selectChannelId == 1)
		{
			if (chatContent.length() > 60)
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("chatui_Acoustic_LimitSize"));
				return;
			}
		}
		if (selectChannelId == 0)
		{
			auto privateui = (AddPrivateUi *)layer_3->getChildByTag(PRIVATEUI);
			if (privateui == NULL)
			{
				const char *str_ = StringDataManager::getString("chatui_private_name");
				GameView::getInstance()->showAlertDialog(str_);
				this->addPrivate();
				return;
			}
			privateName = privateui->textBox_private->getInputString();
			if (privateName == NULL)
			{
				const char *str_ = StringDataManager::getString("chatui_private_name");
				GameView::getInstance()->showAlertDialog(str_);
				return;
			}
		}
		else
		{
			privateName = "";
		}

		ChatInfoStruct chatInfo = { selectChannelId,chatContent,-1,privateName };
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2203, &chatInfo);
		//textBox->deleteAllInputString();
		sendSecond = 0;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void ChatUI::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void ChatUI::refreshChatCellsByChannelId( int channelId )
{
	std::vector<ChatCell*>& allChatCells = GameView::getInstance()->chatCell_Vdata;

	for (unsigned int num=0;num<allChatCells.size();num++)
	{
		// -1 indicate: whole chanels
		if (allChatCells.at(num)->channelId == channelId || channelId == -1)
		{
			int channelid_ = allChatCells.at(num)->channelId;
			std::string playname_ = allChatCells.at(num)->playName;
			std::string descrite_ = allChatCells.at(num)->describeText;

			GameView::getInstance()->selectCell_vdata.push_back(allChatCells.at(num));
		}	
	}
}

int ChatUI::getChannelIndex()
{
	CCAssert(channelLabel != NULL, "should not be nil");
	return channelLabel->getCurrentIndex();
}

void ChatUI::refreshChatUIInfoByChanelIndex( int index )
{
	// selectCell_vdata 's data comes from chatCell_Vdata, so clear it directly without release
	GameView::getInstance()->selectCell_vdata.clear();

	const char *stringsPrivate = StringDataManager::getString("chatui_private");
	const char *stringsWorld = StringDataManager::getString("chatui_world");
	const char *stringsRoom = StringDataManager::getString("chatui_room");
	const char *stringsCurrent = StringDataManager::getString("chatui_current");
	const char *stringsSystem = StringDataManager::getString("chatui_system");
	const char *stringsTeam = StringDataManager::getString("chatui_team");
	const char *stringsCamp = StringDataManager::getString("chatui_camp");
	const char *stringsFaction = StringDataManager::getString("chatui_faction");
	const char *stringsCountry = StringDataManager::getString("chatui_country");
	const char *stringsAcoustic = StringDataManager::getString("chatui_acoustic");

	switch (index)
	{
	case SYSTEM_LABEL:
		{
			refreshChatCellsByChannelId(4);
		}break;
	case WHOLE_LABEL:
		{
			refreshChatCellsByChannelId(-1);
		}break;
	case WORLD_LABEL:
		{
			refreshChatCellsByChannelId(1);
			selectChannelId=1;
			labelchanel->setString(stringsWorld);//Label_channel
		}break;
	case COUNTRY_LABEL:
		{
			refreshChatCellsByChannelId(8);
			selectChannelId=8;
			labelchanel->setString(stringsCountry);
		}break;
	case FAMILY_LABEL:
		{
			refreshChatCellsByChannelId(6);
			selectChannelId=6;
			labelchanel->setString(stringsCamp);
		}break;
	case TEAM_LABEL:
		{
			refreshChatCellsByChannelId(5);
			selectChannelId=5;
			labelchanel->setString(stringsTeam);
		}break;
	case CURRECT_LABEL:
		{
			refreshChatCellsByChannelId(3);
			selectChannelId=3;
			labelchanel->setString(stringsCurrent);
		}break;
	case PRIVATE_LABEL:
		{
			refreshChatCellsByChannelId(0);
			selectChannelId=0;
			labelchanel->setString(stringsPrivate);
		}break;
	case ACOUSTIC_LABEL:
		{
			refreshChatCellsByChannelId(9);
			selectChannelId = 9;
			labelchanel->setString(stringsAcoustic);
		}break;
	}

	ChatInstance::getInstance()->setCurChannel(index);
}

void ChatUI::ChangeLeftPanelByIndex( Ref * obj )
{
	int index=((UITab *)obj)->getCurrentIndex();
	if (index != 6)
	{
		auto privateui_ = (AddPrivateUi *)layer_3->getChildByTag(PRIVATEUI);
		if (privateui_ != NULL)
		{
			privateui_->callBackClosed(NULL,Widget::TouchEventType::ENDED);
		}
	}

	//改变频道按钮的选项
	refreshChatUIInfoByChanelIndex(index);
	
	auto tableView = (TableView*)listLayer->getChildByTag(TABVIEW_TAG);
	tableView->reloadData();
	if(tableView->getContentSize().height > tableView->getViewSize().height)
		tableView->setContentOffset(tableView->maxContainerOffset(), false);
}

bool ChatUI::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return true;
}

void ChatUI::callBackDelete(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		textBox->onTextFieldDeleteBackward(NULL, NULL, 0);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void ChatUI::callBackRoll(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto tableView = (TableView*)listLayer->getChildByTag(TABVIEW_TAG);
		const char *str_ = NULL;
		if (whetherOfRoll == true)
		{
			whetherOfRoll = false;
			btn_rollBg->loadTextures("res_ui/liaotian/suo_on.png", "res_ui/liaotian/suo_on.png", "");
			//curTabViewOff= tableView->getContentOffset();

			str_ = StringDataManager::getString("chatui_not_auto_roll");
		}
		else
		{
			whetherOfRoll = true;
			btn_rollBg->loadTextures("res_ui/liaotian/suo_off.png", "res_ui/liaotian/suo_off.png", "");

			str_ = StringDataManager::getString("chatui_auto_roll");
		}
		GameView::getInstance()->showAlertDialog(str_);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void ChatUI::addToMainChatUiMessage( int channel_id,std::string message )
{
	auto chatCellBig = ChatCell::create(channel_id, "", "", message,NULL, true,0,0);
	chatCellBig->retain();
	GameView::getInstance()->chatCell_Vdata.push_back(chatCellBig);

	if (GameView::getInstance()->chatCell_Vdata.size() > 30)
	{
		std::vector<ChatCell *>::iterator iter=GameView::getInstance()->chatCell_Vdata.begin();
		auto cellv_=* iter;
		GameView::getInstance()->chatCell_Vdata.erase(iter);
		cellv_->release();
	}

	auto chatui_ = (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
	if (chatui_ != NULL)
	{
		chatui_->refreshChatUIInfoByChanelIndex(chatui_->getChannelIndex());
		auto tableView = (TableView*)chatui_->listLayer->getChildByTag(TABVIEW_TAG);
		Vec2 off_ =tableView->getContentOffset();
		int h_ = tableView->getContentSize().height + off_.y;
		tableView->reloadData();
		if(tableView->getContentSize().height > tableView->getViewSize().height)
		{
			if (chatui_->whetherOfRoll==false)
			{
				Vec2 temp = Vec2(off_.x,h_- tableView->getContentSize().height);
				tableView->setContentOffset(temp);
			}else
			{
				tableView->setContentOffset(tableView->maxContainerOffset(), false);
			}			
		}
	}
}

bool ChatUI::redRight=true;
int ChatUI::sendSecond=0;



