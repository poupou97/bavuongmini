#include "ChatCell.h"
#include "ChatUI.h"
#include "../extensions/RichTextInput.h"
#include "AddPrivateUi.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../extensions/CCMoveableMenu.h"
#include "../extensions/CCRichLabel.h"
#include "../../GameView.h"
#include "../backpackscene/PacPageView.h"
#include "../../ui/extensions/RichElement.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "AppMacros.h"
#include "../../gamescene_state/sceneelement/ChatWindows.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/HeadMenu.h"
#include "../../legend_script/CCTutorialParticle.h"


ChatCell::ChatCell(void)
{	
}
ChatCell::~ChatCell(void)
{
}
ChatCell * ChatCell::create( int channel_id,std::string player_country,std::string play_name,std::string describe,long long playerid,bool isMainChat,int viplevel,long long listenerId,std::string listenerName /*= ""*/ )
{
	auto chatcell=new ChatCell();
	if(chatcell && chatcell->init(channel_id,player_country,play_name,describe,playerid,isMainChat,viplevel,listenerId,listenerName))
	{
		chatcell->autorelease();
		return chatcell;
	}
	CC_SAFE_DELETE(chatcell);
	return NULL;
}

bool ChatCell::init(int channel_id,std::string player_country,std::string play_name,std::string describe,long long playerid,bool isMainChat,int viplevel,long long listenerId,std::string listenerName /*= ""*/)
{
	if (TableViewCell::init())
	{
		channelId = channel_id;
		playerCountry = player_country;
		long long selfRoleId = GameView::getInstance()->myplayer->getRoleId();
		if ( channelId ==0 && listenerId != selfRoleId && strlen(listenerName.c_str()) >0)
		{
			playerId = listenerId;
		}else
		{
			playerId = playerid;
		}
		
		playName =play_name;
		describeText = describe;
		listenerName_ = listenerName;
		chatContent_ = describe;
		playerVip_ = viplevel;

		int nameSize2 = strlen(playName.c_str());
		std::string pathImage = getChannelImageOfIndex(channel_id);
		auto channelSp = Sprite::create(pathImage.c_str());
		channelSp->setAnchorPoint(Vec2(0,0));
		channelSp->setPosition(Vec2(0,0));
		Sprite * sp_country;
		//self name
		std::string selfName = GameView::getInstance()->myplayer->getActorName();
		//player name
		const char *countryState = StringDataManager::getString("chatui_playerNameOfcountry");
		std::string str_nameend = "[";
		//str_nameend.append(playerCountry);
		//str_nameend.append(countryState);
		str_nameend.append(playName);
		str_nameend.append("]");

        const int CHANNEL_TEXT_OFFSET = 2;
        const int COLON_TEXT_OFFSET = 2;
		if (isMainChat ==true)
		{
			int itemSize = 0;
			CCMoveableMenu * name_menu;
			MenuItemFont * itemImage_name;
			itemImage_name->setFontSize(20);
			Label * label_PrivateBegin;
			Label * label_PrivateEnd;
			Node * widgetVip;
			if (channelId == 0)
			{
				MainScene * mainscene_ =(MainScene *)GameView::getInstance()->getMainUIScene();
				if (strcmp(playName.c_str(),selfName.c_str())==0)
				{
					//self sayto other
					const char *selfToOtherSayStr = StringDataManager::getString("selfToOtherSay");
					label_PrivateBegin = Label::createWithTTF(selfToOtherSayStr, APP_FONT_NAME, 20);
					label_PrivateBegin->setAnchorPoint(Vec2(0,0));
					label_PrivateBegin->setPosition(Vec2(channelSp->getContentSize().width,0));
					addChild(label_PrivateBegin);
					itemSize = channelSp->getContentSize().width + label_PrivateBegin->getContentSize().width;

					sp_country = Sprite::create(getCountryImageByStr(playerCountry.c_str()).c_str());
					sp_country->setAnchorPoint(Vec2(0,0));
					sp_country->setPosition(Vec2(itemSize+1,0));
					sp_country->setScale(0.7f);
					addChild(sp_country);
					itemSize += sp_country->getContentSize().width*0.7f;

					std::string str_private_="[";
					//str_private_.append(playerCountry);
					//str_private_.append(countryState);
					str_private_.append(listenerName);
					str_private_.append("]");
					itemImage_name = MenuItemFont::create(str_private_.c_str(), CC_CALLBACK_1(ChatCell::NameEvent,this));
					Color3B nameColor=Color3B(161,255,156);
					itemImage_name->setColor(nameColor);
					itemImage_name->setAnchorPoint(Vec2(0.5f,0));
					name_menu=CCMoveableMenu::create(itemImage_name,NULL);
					name_menu->setAnchorPoint(Vec2(0,0));
					name_menu->setPosition(Vec2(itemSize+itemImage_name->getContentSize().width/2,0));
					this->addChild(name_menu);
					itemSize += itemImage_name->getContentSize().width;
// 					if (viplevel > 0)
// 					{
// 						widgetVip = MainScene::addVipInfoByLevelForNode(viplevel);
// 						addChild(widgetVip);
// 						widgetVip->setPosition(Vec2(itemSize+ widgetVip->getContentSize().width/2,0));
// 
// 						itemSize += widgetVip->getContentSize().width+5;
// 					}
				}else
				{
					sp_country = Sprite::create(getCountryImageByStr(playerCountry.c_str()).c_str());
					sp_country->setAnchorPoint(Vec2(0,0));
					sp_country->setPosition(Vec2(channelSp->getContentSize().width+1.5f,0));
					sp_country->setScale(0.7f);
					addChild(sp_country);
					itemSize = channelSp->getContentSize().width + sp_country->getContentSize().width*0.7f;

					//other sayto self
					itemImage_name = MenuItemFont::create(str_nameend.c_str(), CC_CALLBACK_1(ChatCell::NameEvent,this));
					Color3B nameColor=Color3B(161,255,156);
					itemImage_name->setColor(nameColor);
					itemImage_name->setAnchorPoint(Vec2(0.5f,0));
					name_menu=CCMoveableMenu::create(itemImage_name,NULL);
					name_menu->setAnchorPoint(Vec2(0,0));
					name_menu->setPosition(Vec2(itemSize+itemImage_name->getContentSize().width/2,0));
					this->addChild(name_menu);
					itemSize += itemImage_name->getContentSize().width;

					if (viplevel > 0)
					{
						widgetVip = MainScene::addVipInfoByLevelForNode(viplevel);
						addChild(widgetVip);
						widgetVip->setPosition(Vec2(itemSize+ widgetVip->getContentSize().width/2,0));

						itemSize += widgetVip->getContentSize().width+5;
					}

					const char *otherToSelfSayStr = StringDataManager::getString("otherToSelfSay");
					label_PrivateBegin = Label::createWithTTF(otherToSelfSayStr, APP_FONT_NAME, 20);
					label_PrivateBegin->setAnchorPoint(Vec2(0,0));
					label_PrivateBegin->setPosition(Vec2(itemSize,0));
					addChild(label_PrivateBegin);
					itemSize += label_PrivateBegin->getContentSize().width;
					
					GameView::getInstance()->hasPrivatePlaer = true;
					mainscene_->btnRemin->setVisible(true);
					mainscene_->btnReminOfNewMessageParticle->setVisible(true);
				}
				const char *privateChatOfPlayerStr = StringDataManager::getString("privateChatOfPlayer");
				label_PrivateEnd = Label::createWithTTF(privateChatOfPlayerStr, APP_FONT_NAME, 20);
				label_PrivateEnd->setAnchorPoint(Vec2(0,0));
				label_PrivateEnd->setPosition(Vec2(itemSize,0));
				addChild(label_PrivateEnd);
				itemSize += label_PrivateEnd->getContentSize().width;
			}else
			{				
				if (nameSize2 > 0)
				{
					sp_country = Sprite::create(getCountryImageByStr(playerCountry.c_str()).c_str());
					sp_country->setAnchorPoint(Vec2(0,0));
					sp_country->setPosition(Vec2(channelSp->getContentSize().width+1.5f,0));
					sp_country->setScale(0.7f);
					addChild(sp_country);
					itemSize = channelSp->getContentSize().width+sp_country->getContentSize().width*0.7f;

					itemImage_name = MenuItemFont::create(str_nameend.c_str(), CC_CALLBACK_1(ChatCell::NameEvent, this));
					Color3B nameColor=Color3B(161,255,156);
					itemImage_name->setColor(nameColor);
					itemImage_name->setAnchorPoint(Vec2(0.5f,0));
					name_menu=CCMoveableMenu::create(itemImage_name,NULL);
					name_menu->setAnchorPoint(Vec2(0,0));
					name_menu->setPosition(Vec2(itemSize+itemImage_name->getContentSize().width/2,0));
					this->addChild(name_menu);

					itemSize += itemImage_name->getContentSize().width;

					if (viplevel > 0)
					{
						widgetVip = MainScene::addVipInfoByLevelForNode(viplevel);
						addChild(widgetVip);
						widgetVip->setPosition(Vec2(itemSize+ widgetVip->getContentSize().width/2,0));

						itemSize += widgetVip->getContentSize().width + 5;
					}
				}else
				{
					itemSize=channelSp->getContentSize().width;
				}
			}
			
			auto label_colon = Label::createWithTTF(":", APP_FONT_NAME, 20);
			label_colon->setAnchorPoint(Vec2(0,0));
			label_colon->setPosition(Vec2(itemSize,0));
			Color3B colonColor=Color3B(161,255,156);
			label_colon->setColor(colonColor);

			int richlabel_space = itemSize+ 5;
			auto labelLink=CCRichLabel::createWithString(describe.c_str(),Size(620,50),this,menu_selector(ChatCell::LinkEvent),richlabel_space);
			labelLink->setAnchorPoint(Vec2(0,0));
			labelLink->setPosition(Vec2(0,0));

			//if system channel ignor
			if (channel_id != 4 && nameSize2 > 0)
			{
				auto spBg=Sprite::create("res_ui/liaotian/zhuan_di.png");
				auto transmit = MenuItemSprite::create(spBg, spBg, spBg, CC_CALLBACK_1(ChatCell::callBackTransmit, this));
				transmit->setScale(0.5f);
				auto transmit_menu=CCMoveableMenu::create(transmit,NULL);
				transmit_menu->setAnchorPoint(Vec2(0,0));
				transmit_menu->setPosition(Vec2(635,spBg->getContentSize().height/2));
				this->addChild(transmit_menu);
			}
			
			this->addChild(labelLink);
			this->addChild(label_colon);
			this->addChild(channelSp);

			const std::vector<int>& linesHeight = labelLink->getLinesHeight();
            CCAssert(linesHeight.size() > 0, "out of range");
			int offsetY = labelLink->getContentSize().height - linesHeight.at(0);
			
			if (nameSize2 > 0)
			{
				sp_country->setPositionY(offsetY);
				name_menu->setPositionY(offsetY);
			}
			if (channelId == 0)
			{
				label_PrivateBegin->setPositionY(offsetY);
				label_PrivateEnd->setPositionY(offsetY);
			}
			if (viplevel > 0)
			{
				if (!(channelId == 0 && strcmp(playName.c_str(),selfName.c_str())==0))
				{
					widgetVip->setPositionY(offsetY + widgetVip->getContentSize().height/2 - CHANNEL_TEXT_OFFSET );
				}
			}

			channelSp->setPositionY(offsetY + CHANNEL_TEXT_OFFSET);
			label_colon->setPositionY(offsetY + COLON_TEXT_OFFSET);
			label_width=labelLink->getContentSize().width;
			this->setContentSize(Size(480,labelLink->getContentSize().height));
		}else
		{
			int itemSize = 0;
			Label * label_PrivateBegin;
			Label *label_PrivateEnd;
			//MenuItemFont * itemImage_name;
			//CCMoveableMenu * playerNameLabel;
			Label * label_name;

			Node * widgetVip;
			if (channelId == 0)
			{
				//Color3B nameColor=Color3B(161,255,156);
				Color3B nameColor= Color3B(243,252,2);
				if (strcmp(playName.c_str(),selfName.c_str())==0)
				{
					//self sayto other
					const char *selfToOtherSayStr = StringDataManager::getString("selfToOtherSay");
					label_PrivateBegin = Label::createWithTTF(selfToOtherSayStr, APP_FONT_NAME, 20);
					label_PrivateBegin->setAnchorPoint(Vec2(0,0));
					label_PrivateBegin->setPosition(Vec2(channelSp->getContentSize().width,0));
					addChild(label_PrivateBegin);
					itemSize = channelSp->getContentSize().width + label_PrivateBegin->getContentSize().width;

					sp_country = Sprite::create(getCountryImageByStr(playerCountry.c_str()).c_str());
					sp_country->setAnchorPoint(Vec2(0,0));
					sp_country->setPosition(Vec2(itemSize,0));
					sp_country->setScale(0.7f);
					addChild(sp_country);
					itemSize += sp_country->getContentSize().width*0.7f;

					std::string str_private_="[";
					//str_private_.append(playerCountry);
					//str_private_.append(countryState);
					str_private_.append(listenerName);
					str_private_.append("]");
					
					label_name = Label::createWithTTF(str_private_.c_str(),APP_FONT_NAME, 20);
					label_name->setAnchorPoint(Vec2(0.5f,0));
					label_name->setPosition(Vec2(itemSize+label_name->getContentSize().width/2,0));
					label_name->setColor(nameColor);
					this->addChild(label_name);
					itemSize += label_name->getContentSize().width;

// 					if (viplevel > 0)
// 					{
// 						widgetVip = MainScene::addVipInfoByLevelForNode(viplevel);
// 						addChild(widgetVip);
// 						widgetVip->setPosition(Vec2(itemSize+ widgetVip->getContentSize().width/2,0));
// 
// 						itemSize += widgetVip->getContentSize().width+5;
// 					}
				}else
				{
					//other sayto self
					sp_country = Sprite::create(getCountryImageByStr(playerCountry.c_str()).c_str());
					sp_country->setAnchorPoint(Vec2(0,0));
					sp_country->setPosition(Vec2(channelSp->getContentSize().width+1,0));
					sp_country->setScale(0.7f);
					addChild(sp_country);
					itemSize = channelSp->getContentSize().width + sp_country->getContentSize().width*0.7f;

					label_name = Label::createWithTTF(str_nameend.c_str(),APP_FONT_NAME, 20);
					label_name->setAnchorPoint(Vec2(0.5f,0));
					label_name->setColor(nameColor);
					label_name->setPosition(Vec2(itemSize + label_name->getContentSize().width/2,0));
					this->addChild(label_name);
					itemSize +=  label_name->getContentSize().width;

					if (viplevel > 0)
					{
						widgetVip = MainScene::addVipInfoByLevelForNode(viplevel);
						addChild(widgetVip);
						widgetVip->setPosition(Vec2(itemSize+ widgetVip->getContentSize().width/2,0));

						itemSize += widgetVip->getContentSize().width+5;
					}
					const char *otherToSelfSayStr = StringDataManager::getString("otherToSelfSay");
					label_PrivateBegin = Label::createWithTTF(otherToSelfSayStr, APP_FONT_NAME, 20);
					label_PrivateBegin->setAnchorPoint(Vec2(0,0));
					label_PrivateBegin->setPosition(Vec2(itemSize,0));
					addChild(label_PrivateBegin);
					itemSize += label_PrivateBegin->getContentSize().width;
				}
				const char *privateChatOfPlayerStr = StringDataManager::getString("privateChatOfPlayer");
				label_PrivateEnd = Label::createWithTTF(privateChatOfPlayerStr, APP_FONT_NAME, 20);
				label_PrivateEnd->setAnchorPoint(Vec2(0,0));
				label_PrivateEnd->setPosition(Vec2(itemSize,0));
				addChild(label_PrivateEnd);
				itemSize += label_PrivateEnd->getContentSize().width;
		}else
		{
			if (nameSize2 > 0)
			{
				sp_country = Sprite::create(getCountryImageByStr(playerCountry.c_str()).c_str());
				sp_country->setAnchorPoint(Vec2(0,0));
				sp_country->setPosition(Vec2(channelSp->getContentSize().width,0));
				sp_country->setScale(0.7f);
				addChild(sp_country);
				itemSize = channelSp->getContentSize().width+sp_country->getContentSize().width*0.7f;

				label_name = Label::createWithTTF(str_nameend.c_str(),APP_FONT_NAME, 20);
				label_name->setAnchorPoint(Vec2(0.5f,0));
				label_name->setPosition(Vec2(itemSize+label_name->getContentSize().width/2,0));
				label_name->setColor(Color3B(243,252,2));
				this->addChild(label_name);
				itemSize += label_name->getContentSize().width;

				if (viplevel > 0)
				{
					widgetVip = MainScene::addVipInfoByLevelForNode(viplevel);
					addChild(widgetVip);
					widgetVip->setPosition(Vec2(itemSize+ widgetVip->getContentSize().width/2,0));

					itemSize += widgetVip->getContentSize().width+5;
				}
			}else
			{
				itemSize=channelSp->getContentSize().width;
			}
		}

		auto label_colon = Label::createWithTTF(":",APP_FONT_NAME, 20);
		label_colon->setAnchorPoint(Vec2(0,0));
		label_colon->setPosition(Vec2(itemSize,0));

		auto labelLink =CCRichLabel::createWithString(describe.c_str(),Size(430,50),NULL,NULL,itemSize+5);
		labelLink->setAnchorPoint(Vec2(0,0));
		labelLink->setPosition(Vec2(0,0));
		
		setOneCellOfLine(labelLink->getLinesHeight().size());

		this->addChild(labelLink);
		this->addChild(label_colon);
		this->addChild(channelSp);

        const std::vector<int>& linesHeight = labelLink->getLinesHeight();
        CCAssert(linesHeight.size() > 0, "out of range");
        int offsetY = labelLink->getContentSize().height - linesHeight.at(0);
		if (nameSize2 > 0)
		{
			sp_country->setPositionY(offsetY+CHANNEL_TEXT_OFFSET);
			label_name->setPositionY(offsetY+CHANNEL_TEXT_OFFSET);
			//playerNameLabel->setPositionY(offsetY+CHANNEL_TEXT_OFFSET);
		}
		if (channelId == 0)
		{
			label_PrivateBegin->setPositionY(offsetY);
			label_PrivateEnd->setPositionY(offsetY);
		}
		if (viplevel > 0)
		{
			if (!(channelId == 0 && strcmp(playName.c_str(),selfName.c_str())==0))
			{
				widgetVip->setPositionY(offsetY + widgetVip->getContentSize().height/2 - CHANNEL_TEXT_OFFSET);
			}
		}
        
        channelSp->setPositionY(offsetY + CHANNEL_TEXT_OFFSET);
		label_colon->setPositionY(offsetY + COLON_TEXT_OFFSET);
		label_width=labelLink->getContentSize().width;

		this->setContentSize(Size(480,labelLink->getContentSize().height));
		}
		return true;
	}
	return false;
}

void ChatCell::onEnter()
{
	TableViewCell::onEnter();
}
void ChatCell::onExit()
{
	TableViewCell::onExit();
}

void ChatCell::LinkEvent( Ref * obj )
{
	//add equipment
	auto chatUi= (UIScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
    if(chatUi == NULL)
        return;
    
	auto button = (MenuItemFont*)obj;
	auto btnElement = (RichElementButton*)button->getUserData();
	//CCLOG("richlabel clicked, %s", btnElement->linkContent.c_str());
    structPropIds prop_ids = RichElementButton::parseLink(btnElement->linkContent);
    CCLOG("prop id: %s, prop instance id: %ld", prop_ids.propId.c_str(), prop_ids.propInstanceId);
    
	const char * goodsPropId = prop_ids.propId.c_str();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1322,(void *)prop_ids.propInstanceId,(void *)goodsPropId);
}

void ChatCell::NameEvent( Ref* pSender )
{
	int selfLevel_ =GameView::getInstance()->myplayer->getActiveRole()->level();
	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(20);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[20];
	}

	if (selfLevel_ < openlevel)
	{
		char openLevelStr[100];
		const char *strings = StringDataManager::getString("chatAndmailIsOpenInFiveLevel");
		sprintf(openLevelStr,strings,openlevel);
		GameView::getInstance()->showAlertDialog(openLevelStr);
		return;
	}

	
	auto m_chatUi= (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
	long long selfRoleId = GameView::getInstance()->myplayer->getRoleId();
	if (selfRoleId == playerId)
	{
		return;
	}

	Size winsize = Director::getInstance()->getVisibleSize();
	auto nameList=AddPrivateUi::create(2);
	nameList->setIgnoreAnchorPointForPosition(false);
	nameList->setAnchorPoint(Vec2(0,0));

	int cellOff = this->getPosition().y;
	//nameList->setPosition(Vec2(200,cellOff+winsize.height/2-m_chatUi->getContentSize().height/2));
	nameList->setPosition(Vec2(100,winsize.height/2));
	m_chatUi->addChild(nameList,4);
}

void ChatCell::callBackTransmit( Ref * obj )
{
	auto sendId=(CCMoveableMenu *)obj;
	int tag = sendId->getTag();

	int chatSize = describeText.size();
	const char * chat_text=describeText.c_str();

	auto pScene=Director::getInstance()->getRunningScene();
	auto m_chatUi= (UIScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
	auto text = (RichTextInputBox*)m_chatUi->getChildByTag(CHATLayer3)->getChildByTag(RICHTEXTINPUT_TAG);
	
	if (ChatUI::sendSecond==0)
	{
		text->onTextFieldInsertText(NULL, chat_text, chatSize);
		ChatUI::sendSecond++;
	}else
	{
		text->deleteAllInputString();
		text->onTextFieldInsertText(NULL, chat_text, chatSize);
	}
}

Label *ChatCell::getChannelNameByindex(int index)
{
	Color3B channelColor;
	const char *strings;
	switch(index)
	{
	case 0:
		{
			strings = StringDataManager::getString("chatui_private");
			channelColor=Color3B(255,163,226);
		}break;
	case 1:
		{
			strings = StringDataManager::getString("chatui_world");
			channelColor=Color3B(142,69,252);
		}break;
	case 2:
		{
			strings = StringDataManager::getString("chatui_room");
			channelColor=Color3B(0,240,216);
		}break;
	case 3:
		{
			strings = StringDataManager::getString("chatui_current");
			channelColor=Color3B(255,163,226);
		}break;
	case 4:
		{
			strings = StringDataManager::getString("chatui_system");
			channelColor=Color3B(255,163,226);
		}break;
	case 5:
		{
			strings = StringDataManager::getString("chatui_team");
			channelColor=Color3B(249,186,46);
		}break;
	case 6:
		{
			strings = StringDataManager::getString("chatui_camp");
			channelColor=Color3B(255,255,255);
		}break;
	case 7:
		{
			strings = StringDataManager::getString("chatui_faction");
		}break;
	case 8:
		{
			strings = StringDataManager::getString("chatui_country");
		}break;
	}

	std::string strFirst="<";
	std::string strSec=strFirst.append(strings);
	std::string chat_end=strSec.append(">");

	auto channel_label = Label::createWithTTF(chat_end.c_str(), APP_FONT_NAME, 20);
	channel_label->setColor(channelColor);
	return channel_label;
}

std::string ChatCell::getChannelImageOfIndex( int index )
{
	std::string imagepath;
	switch(index)
	{
	case 0:
		{
			imagepath = "res_ui/liaotian/siliao.png";
		}break;
	case 1:
		{
			imagepath = "res_ui/liaotian/shijie.png";
		}break;
	case 2:
		{
			/*
			imagepath = "res_ui/liaotian/siliao.png";
			strings = StringDataManager::getString("chatui_room");
			channelColor=Color3B(0,240,216);
			*/
		}break;
	case 3:
		{
			imagepath = "res_ui/liaotian/dangqian.png";
		}break;
	case 4:
		{
			imagepath = "res_ui/liaotian/xitong.png";
		}break;
	case 5:
		{
			imagepath = "res_ui/liaotian/duiwu.png";
		}break;
	case 6:
		{
			imagepath = "res_ui/liaotian/jiazu.png";
		}break;
	case 7:
		{
			imagepath = "res_ui/liaotian/siliao.png";
		}break;
	case 8:
		{
			imagepath = "res_ui/liaotian/guojia.png";
		}break;
	case 9:
		{
			imagepath = "res_ui/liaotian/chuan.png";
		}break;
	}
	return imagepath;
}

int ChatCell::getOneCellOfLine()
{
	return m_oneCellLine;
}

void ChatCell::setOneCellOfLine( int line )
{
	m_oneCellLine = line;
}

std::string ChatCell::getCountryImageByStr( std::string country )
{
	const char *country_1 = StringDataManager::getString("country_1");
	const char *country_2 = StringDataManager::getString("country_2");
	const char *country_3 = StringDataManager::getString("country_3");

	std::string imagePath_ = "res_ui/Expression/";
	if (strcmp(country.c_str(),country_1)== 0)
	{
		imagePath_.append("e31");
	}

	if (strcmp(country.c_str(),country_2)== 0)
	{
		imagePath_.append("e32");
	}

	if (strcmp(country.c_str(),country_3)== 0)
	{
		imagePath_.append("e33");
	}

	imagePath_.append(".png");

	return imagePath_;
}


