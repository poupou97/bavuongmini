#include "PrivateCell.h"
#include "ChatUI.h"
#include "../extensions/RichTextInput.h"
#include "AddPrivateUi.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../extensions/CCMoveableMenu.h"
#include "../extensions/CCRichLabel.h"
#include "../../GameView.h"
#include "../backpackscene/PacPageView.h"
#include "../../ui/extensions/RichElement.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "AppMacros.h"
#include "../../gamescene_state/sceneelement/ChatWindows.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/HeadMenu.h"
#include "PrivateChatUi.h"
#include "../../messageclient/element/CRelationPlayer.h"


PrivateCell::PrivateCell(void)
{	
	m_curCellHeight = 0;
}
PrivateCell::~PrivateCell(void)
{
}
PrivateCell * PrivateCell::create(int pressionId_,std::string play_name,std::string chatContent,long long playerid,int viplevel,std::string listenerName /*= ""*/ )
{
	auto chatcell=new PrivateCell();
	if(chatcell && chatcell->init(pressionId_,play_name,chatContent,playerid,viplevel,listenerName))
	{
		chatcell->autorelease();
		return chatcell;
	}
	CC_SAFE_DELETE(chatcell);
	return NULL;
}

bool PrivateCell::init(int pressionId_,std::string play_name,std::string chatContent,long long playerid,int viplevel,std::string listener_Name /*= ""*/ )
{
	if (TableViewCell::init())
	{
		speakName = play_name;
		listenerName =listener_Name ;
		speakerId = playerid;
		int m_pression = pressionId_;
		
		if ( playerid == GameView::getInstance()->myplayer->getActiveRole()->rolebase().roleid())
		{
			auto iconBackGround =Sprite::create("res_ui/round.png");
			iconBackGround->setAnchorPoint(Vec2(0,0));
			iconBackGround->setPosition(Vec2(345 - iconBackGround->getContentSize().width - 16,0));
			addChild(iconBackGround);

			m_pression  = GameView::getInstance()->myplayer->getProfession();
			std::string playerIcon_ = "";
			if (m_pression > 0 && m_pression <5)
			{
				playerIcon_ = BasePlayer::getHeadPathByProfession(m_pression);
			}else
			{	
				playerIcon_ = "res_ui/protangonis56x55/none.png";
			}

			auto friendHead =Sprite::create(playerIcon_.c_str());
			friendHead->setAnchorPoint(Vec2(0.5f,0.5f));
			//friendHead->setPosition(Vec2(345 - iconBackGround->getContentSize().width - 13,3));
			friendHead->setPosition(Vec2(iconBackGround->getContentSize().width/2,iconBackGround->getContentSize().height/2-1));
			friendHead->setScale(0.8f);
			iconBackGround->addChild(friendHead);

			auto labelLink =CCRichLabel::createWithString(chatContent.c_str(),Size(220,50),this,menu_selector(PrivateCell::LinkEvent),0,16);
			labelLink->setAnchorPoint(Vec2(0,0));
			labelLink->setPosition(Vec2(0,10));
			addChild(labelLink,5);

			int dialog_hight = 42;//dialog2 min is 42, 
			if (labelLink->getContentSize().height > 30)
			{
				dialog_hight = labelLink->getContentSize().height + 20;
				iconBackGround->setPositionY(dialog_hight - 40);
				//friendHead->setPositionY(dialog_hight - 40);
			}

			auto contentSp_ = cocos2d::extension::Scale9Sprite::create("res_ui/dialog2.png");
			contentSp_->setAnchorPoint(Vec2(0,0));
			contentSp_->setPreferredSize(Size(labelLink->getContentSize().width+30,dialog_hight));
			contentSp_->setCapInsets(Rect(24,27,1,3));
			contentSp_->setRotationSkewY(180);
			contentSp_->setPosition(Vec2(0,0));
			addChild(contentSp_);

			if (labelLink->getContentSize().width < 220)
			{
				labelLink->setPositionX(345 - labelLink->getContentSize().width - iconBackGround->getContentSize().width - 34);
				contentSp_->setPositionX(345 - iconBackGround->getContentSize().width - 16);
			}

			auto label_playerName = Label::createWithTTF(speakName.c_str(), APP_FONT_NAME, 16);
			label_playerName->setAnchorPoint(Vec2(1,0));
			label_playerName->setPosition(Vec2(345 - label_playerName->getContentSize().width/2 - iconBackGround->getContentSize().width,contentSp_->getContentSize().height));
			addChild(label_playerName);
			label_playerName->setColor(Color3B(15,202,250));

			int h_ = iconBackGround->getContentSize().height + label_playerName->getContentSize().height;
			if (h_ < contentSp_->getContentSize().height + label_playerName->getContentSize().height)
			{
				h_ = contentSp_->getContentSize().height+label_playerName->getContentSize().height;
			}
			this->setCurCellHeight(h_);
		}else
		{
			auto iconBackGround =Sprite::create("res_ui/round.png");
			iconBackGround->setAnchorPoint(Vec2(0.5f,0));
			iconBackGround->setPosition(Vec2(iconBackGround->getContentSize().width/2,0));
			addChild(iconBackGround);

			std::string playerIcon_;
			if (m_pression > 0 && m_pression <5)
			{
				playerIcon_ = BasePlayer::getHeadPathByProfession(m_pression);
				
			}else
			{
				playerIcon_ = "res_ui/protangonis56x55/none.png";
			}

			/*
			auto friendHead =Sprite::create(playerIcon_.c_str());
			MenuItemSprite * nameIcon_item = MenuItemSprite::create(friendHead, friendHead, friendHead, this, menu_selector(PrivateCell::callBackNameList));
			nameIcon_item->setZoomScale(0.6f);
			CCMoveableMenu * nameIcon_menu = CCMoveableMenu::create(nameIcon_item,NULL);
			nameIcon_menu->setAnchorPoint(Vec2(0.5f,0.5f));
			nameIcon_menu->setPosition(Vec2(iconBackGround->getContentSize().width/2 - 80, -iconBackGround->getContentSize().height/2));//
			addChild(nameIcon_menu);
			nameIcon_menu->setScale(0.8f);
			*/

			auto layer = Layer::create();
			addChild(layer);

			auto btn_icon = Button::create();
			btn_icon->loadTextures(playerIcon_.c_str(),playerIcon_.c_str(),"");
			btn_icon->setAnchorPoint(Vec2(0.5f,0.5f));
			btn_icon->addTouchEventListener(CC_CALLBACK_2(PrivateCell::callBackNameList, this));
			btn_icon->setTouchEnabled(true);
			btn_icon->setPosition(Vec2(iconBackGround->getContentSize().width/2,btn_icon->getContentSize().height/2));
			btn_icon->setPressedActionEnabled(true);
			layer->addChild(btn_icon);

			btn_icon->setScale(0.8f);

			auto labelLink =CCRichLabel::createWithString(chatContent.c_str(),Size(220,50),this,CC_MENU_SELECTOR(PrivateCell::LinkEvent),0,16);
			labelLink->setAnchorPoint(Vec2(0,0));
			labelLink->setPosition(Vec2(iconBackGround->getContentSize().width+20,10));
			addChild(labelLink,5);

			int dialog_hight = 42;//dialog2 min is 42, 
			if (labelLink->getContentSize().height > 30)
			{
				dialog_hight = labelLink->getContentSize().height + 20;
				iconBackGround->setPositionY(dialog_hight - 40);
				btn_icon->setPosition(Vec2(iconBackGround->getPositionX(),iconBackGround->getPositionY()+btn_icon->getContentSize().height/2));//dialog_hight - 40 -iconBackGround->getContentSize().height/2 
			}

			auto contentSp_ = cocos2d::extension::Scale9Sprite::create("res_ui/dialog1.png");
			contentSp_->setAnchorPoint(Vec2(0,0));
			contentSp_->setPreferredSize(Size(labelLink->getContentSize().width+30,dialog_hight));
			contentSp_->setCapInsets(Rect(24,27,1,3));
			contentSp_->setPosition(Vec2(iconBackGround->getContentSize().width+5,0));
			addChild(contentSp_);

			auto label_playerName = Label::createWithTTF(speakName.c_str(), APP_FONT_NAME, 16);
			label_playerName->setAnchorPoint(Vec2(0,0));
			label_playerName->setPosition(Vec2(iconBackGround->getPositionX() + iconBackGround->getContentSize().width,contentSp_->getContentSize().height));
			addChild(label_playerName,5);
			label_playerName->setColor(Color3B(136,234,31));

			int h_ = iconBackGround->getContentSize().height + label_playerName->getContentSize().height;
			
			if (h_ < contentSp_->getContentSize().height + label_playerName->getContentSize().height)
			{
				h_ = contentSp_->getContentSize().height+label_playerName->getContentSize().height;
			}
			this->setCurCellHeight(h_);
		}

		return true;
	}
	return false;
}

void PrivateCell::onEnter()
{
	TableViewCell::onEnter();
}
void PrivateCell::onExit()
{
	TableViewCell::onExit();
}

void PrivateCell::LinkEvent( Ref * obj )
{    
	auto button = (MenuItemFont*)obj;
	auto btnElement = (RichElementButton*)button->getUserData();
	//CCLOG("richlabel clicked, %s", btnElement->linkContent.c_str());
    structPropIds prop_ids = RichElementButton::parseLink(btnElement->linkContent);
    CCLOG("prop id: %s, prop instance id: %ld", prop_ids.propId.c_str(), prop_ids.propInstanceId);
    
	const char * goodsPropId = prop_ids.propId.c_str();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1322,(void *)prop_ids.propInstanceId,(void *)goodsPropId);
}

void PrivateCell::NameEvent( Ref* pSender )
{
	/*
	Size winsize = Director::getInstance()->getVisibleSize();
	auto privateui= (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);

	long long myId = GameView::getInstance()->myplayer->getRoleId();
	if (myId == speakerId)
	{
		return;
	}
	AddPrivateUi * nameList=AddPrivateUi::create(4);
	nameList->setIgnoreAnchorPointForPosition(false);
	nameList->setAnchorPoint(Vec2(0,0));

	int cellOff = this->getPosition().y;
	nameList->setPosition(Vec2(100,winsize.height/2));
	privateui->addChild(nameList,4);
	*/
}

void PrivateCell::setCurCellHeight( int height )
{
	m_curCellHeight = height;
}

int PrivateCell::getCurCellHeight()
{
	return m_curCellHeight;
}

void PrivateCell::callBackNameList(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto privateui_ = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);

		Size winSize = Director::getInstance()->getVisibleSize();
		auto nameList = NameList::create(speakerId, speakName);
		nameList->setIgnoreAnchorPointForPosition(false);
		nameList->setAnchorPoint(Vec2(0, 0));
		//nameList->setPosition(Vec2(300,winSize.height/2 - nameList->getContentSize().height/2+20));
		nameList->setPosition(Vec2(160, winSize.height / 2 - nameList->getContentSize().height / 2 + 20));
		privateui_->layer2->addChild(nameList);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}
