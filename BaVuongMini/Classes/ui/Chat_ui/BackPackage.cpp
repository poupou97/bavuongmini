#include "BackPackage.h"
#include "../extensions/RichTextInput.h"
#include "../Chat_ui/ChatUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../backpackscene/PacPageView.h"
#include "../../ui/extensions/RichElement.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../messageclient/element/CDrug.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"
#include "../../utils/GameUtils.h"
#include "../extensions/CCRichLabel.h"
#include "../../gamescene_state/role/GameActorAnimation.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../generals_ui/RecuriteActionItem.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "AppMacros.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../generals_ui/GeneralsUI.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CActiveRole.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../extensions/UITab.h"
#include "../../messageclient/element/EquipmentInfo.h"
#include "PrivateChatUi.h"
#include "cocostudio\CCSGUIReader.h"

#define  SelectImageTag 458

BackPackage::BackPackage(void)
{
	curType = 0;
	weapEffectStrForRefine = "";
	weapEffectStrForStar = "";

	curRoleIndex = 0;
}

BackPackage::~BackPackage(void)
{
	std::vector<CEquipment *>::iterator iter;
	for (iter = playerEquipments.begin();iter != playerEquipments.end();++iter)
	{
		delete * iter;
	}
	playerEquipments.clear();
}

BackPackage * BackPackage::create(int uitag)
{
	auto package=new BackPackage();
	if (package && package->init(uitag))
	{
		package->autorelease();
		return package;
	}
	CC_SAFE_DELETE(package);
	return NULL;
}

bool BackPackage::init(int uitag)
{
	if (UIScene::init())
	{
		winsize=Director::getInstance()->getVisibleSize();

		m_Layer= Layer::create();
		m_Layer->setIgnoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(Vec2(0,0));
		m_Layer->setPosition(Vec2(0,0));
		m_Layer->setContentSize(Size(440,430));
		this->addChild(m_Layer);

		m_LayerRole= Layer::create();
		m_LayerRole->setIgnoreAnchorPointForPosition(false);
		m_LayerRole->setAnchorPoint(Vec2(0,0));
		m_LayerRole->setPosition(Vec2(0,0));
		m_LayerRole->setContentSize(Size(440,430));
		this->addChild(m_LayerRole);

		auto layer1= Layer::create();
		layer1->setIgnoreAnchorPointForPosition(false);
		layer1->setAnchorPoint(Vec2(0,0));
		layer1->setPosition(Vec2(0,0));
		layer1->setContentSize(Size(440,430));
		this->addChild(layer1,1);

		auto backPacPanel =(Layout*) cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/props_1.json");
		backPacPanel->setAnchorPoint(Vec2(0,0));
		backPacPanel->setPosition(Vec2(0,0));
		m_Layer->addChild(backPacPanel);

		panelRole = (Layout *)Helper::seekWidgetByName(backPacPanel,"PanelRole");
		panelRole->setVisible(true);
		panelPac = (Layout *)Helper::seekWidgetByName(backPacPanel,"PanelPac");
		panelPac->setVisible(false);
		imageIcon_ = (ImageView *)Helper::seekWidgetByName(backPacPanel,"Label_jobvalue");
		labelLevel = (Text*)Helper::seekWidgetByName(backPacPanel,"Label_roleLevel");
		labelFightValue_ = (Text*)Helper::seekWidgetByName(backPacPanel,"Label_fp_value");
		/*
		ParticleSystem* particleFire = ParticleSystemQuad::create("animation/texiao/particledesigner/huo2.plist");
		particleFire->setPositionType(ParticleSystem::PositionType::FREE);
		particleFire->setPosition(Vec2(217,160));
		particleFire->setAnchorPoint(Vec2(0.5f,0.5f));
		particleFire->setVisible(true);
		//particleFire->setLife(0.5f);
		particleFire->setLifeVar(0);
		particleFire->setScale(0.5f);
		particleFire->setScaleX(0.6f);
		m_LayerRole->addChild(particleFire);

		ParticleSystem* particleFire1 = ParticleSystemQuad::create("animation/texiao/particledesigner/huo2.plist");
		particleFire1->setPositionType(ParticleSystem::PositionType::FREE);
		particleFire1->setPosition(Vec2(222,163));
		particleFire1->setAnchorPoint(Vec2(0.5f,0.5f));
		particleFire1->setVisible(true);
		//particleFire1->setLife(0.5f);
		particleFire1->setLifeVar(0);
		particleFire1->setScale(0.5f);
		particleFire1->setScaleX(0.6f);
		m_LayerRole->addChild(particleFire1);
		*/
		auto u_tempLayer = Layer::create();
		m_LayerRole->addChild(u_tempLayer);

		/*Label * l_allFPName = Label::create();
		l_allFPName->setText(StringDataManager::getString("rank_capacity"));
		l_allFPName->setFntFile("res_ui/font/ziti_1.fnt");
		l_allFPName->setAnchorPoint(Vec2(0.5f,0.5f));
		l_allFPName->setPosition(Vec2(185,167));
		l_allFPName->setScale(0.6f);
		u_tempLayer->addChild(l_allFPName);*/
		auto Image_allFPName = ImageView::create();
		Image_allFPName->loadTexture("res_ui/renwubeibao/zi2.png");
		Image_allFPName->setAnchorPoint(Vec2(0.5f,0.5f));
		Image_allFPName->setPosition(Vec2(172,166));
		Image_allFPName->setScale(1.0f);
		u_tempLayer->addChild(Image_allFPName);

		labelAllFightValue_ = Label::createWithBMFont("res_ui/font/ziti_1.fnt", "123456789");
		labelAllFightValue_->setAnchorPoint(Vec2(0.5f,0.5f));
		labelAllFightValue_->setPosition(Vec2(256,165));
		labelAllFightValue_->setScale(0.6f);
		u_tempLayer->addChild(labelAllFightValue_);

		const char * normalImage = "res_ui/tab_b_off.png";
		const char * selectImage = "res_ui/tab_b.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_b.png";
		//char * labelImage[] ={"res_ui/zhuangbei/tab_renwu.png","res_ui/zhuangbei/tab_beibao.png"};


		const char *str1_renwu = StringDataManager::getString("equip_uitab_renwu");
		char* p1_renwu =const_cast<char*>(str1_renwu);
		const char *str2_beibao = StringDataManager::getString("equip_uitab_beibao");
		char* p2_beibao =const_cast<char*>(str2_beibao);
		char * labelImage[] ={p1_renwu,p2_beibao};

		roleAndPack = UITab::createWithText(2,normalImage,selectImage,finalImage,labelImage,VERTICAL,-5,18);
		//roleAndPack = UITab::createWithImage(2,normalImage,selectImage,"",labelImage,VERTICAL,10);
		roleAndPack->setAnchorPoint(Vec2(0,0.5f));
		roleAndPack->setPosition(Vec2(backPacPanel->getContentSize().width - 36,370));
		roleAndPack->setHighLightImage("res_ui/tab_b.png");
		roleAndPack->setHightLightLabelColor(Color3B(47,93,13));
		roleAndPack->setNormalLabelColor(Color3B(255,255,255));
		roleAndPack->setDefaultPanelByIndex(1);
		roleAndPack->setPressedActionEnabled(true);
		roleAndPack->addIndexChangedEvent(this,coco_indexchangedselector(BackPackage::callBackPackAndRole));
		m_Layer->addChild(roleAndPack);
		
		currentPage=(ImageView *)Helper::seekWidgetByName(backPacPanel,"ImageView_dian_on");
		pageView = GameView::getInstance()->pacPageView;
		pageView->getPageView()->addEventListener((PageView::ccPageViewCallback)CC_CALLBACK_2(BackPackage::CurrentPageViewChanged, this));
		pageView->setIgnoreAnchorPointForPosition(false);
		pageView->setAnchorPoint(Vec2(0,0));
		pageView->setPosition(Vec2(44,94));
		m_Layer->addChild(pageView,0);
		pageView->setVisible(false);
		//pageView->setCurUITag(kTabChat);
		pageView->setCurUITag(uitag);
		auto seq = Sequence::create(DelayTime::create(0.05f),CallFunc::create(CC_CALLBACK_0(BackPackage::PageScrollToDefault, this)),NULL);
		pageView->runAction(seq);
		//CurrentPageViewChanged(pageView);
		pageView->checkCDOnBegan();

// 		pageView_kuang = ImageView::create();
// 		pageView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setScale9Enabled(true);
// 		pageView_kuang->setContentSize(Size(361,322));
// 		pageView_kuang->setCapInsets(Rect(30,30,1,1));
// 		pageView_kuang->setAnchorPoint(Vec2(0,0));
// 		pageView_kuang->setPosition(Vec2(40,85));
// 		pageView_kuang->setLocalZOrder(10);
// 		layer1->addChild(pageView_kuang);
// 		pageView_kuang->setVisible(false);

// 		Button * buttonExit =Button::create();
// 		buttonExit->setAnchorPoint(Vec2(0.5f,0.5f));
// 		buttonExit->loadTextures("res_ui/close.png","res_ui/close.png","");
// 		buttonExit->setPosition(Vec2(backPacPanel->getContentSize().width - 30,410));
// 		buttonExit->setTouchEnabled(true);
// 		buttonExit->setPressedActionEnabled(true);
// 		buttonExit->addTouchEventListener(CC_CALLBACK_2(BackPackage::callBackClose));
// 		buttonExit->setLocalZOrder(11);
// 		layer1->addChild(buttonExit);
		//
		auto buttonExit = (Button*)Helper::seekWidgetByName(backPacPanel,"Button_close");
		buttonExit->setTouchEnabled(true);
		buttonExit->setPressedActionEnabled(true);
		buttonExit->addTouchEventListener(CC_CALLBACK_2(BackPackage::callBackClose, this));

		//refresh cd
		auto shortCutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortCutLayer)
		{
			for (int i = 0;i<7;++i)  //һ��7�����
			{
				if (i < GameView::getInstance()->shortCutList.size())
				{
					auto c_shortcut = GameView::getInstance()->shortCutList.at(i);
					if (c_shortcut->storedtype() == 2)    //����������Ʒ
					{
						if(CDrug::isDrug(c_shortcut->skillpropid()))
						{
							if (shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100))
							{
								auto shortcutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100);
								this->pageView->RefreshCD(c_shortcut->skillpropid());
							}
						}
					}
				}
			}
		}
		
		auto generalList = TableView::create(this, Size(354, 120));//312 100
		//generalList->setSelectedEnable(true);
		//generalList->setSelectedScale9Texture("res_ui/highlight.png", Rect(26, 26, 1, 1), Vec2(0,0));
		generalList->setDirection(TableView::Direction::HORIZONTAL);
		generalList->setAnchorPoint(Vec2(0,0));
		generalList->setPosition(Vec2(42,17));
		generalList->setDelegate(this);
		generalList->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//generalList->setPressedActionEnabled(true);
		m_LayerRole->addChild(generalList);
		generalList->reloadData();

		//
		image_accessories = (ImageView *)Helper::seekWidgetByName(backPacPanel,"ImageView_accessories");  
		image_shoes = (ImageView *)Helper::seekWidgetByName(backPacPanel,"ImageView_shoes");  
		image_helmet = (ImageView *)Helper::seekWidgetByName(backPacPanel,"ImageView_helmet");  
		image_ring = (ImageView *)Helper::seekWidgetByName(backPacPanel,"ImageView_ring");  
		image_clothes = (ImageView *)Helper::seekWidgetByName(backPacPanel,"ImageView_clothes");  
		image_arms = (ImageView *)Helper::seekWidgetByName(backPacPanel,"ImageView_arms");  

		//add flag
		tempLayer_flag = Layer::create();
		tempLayer_flag->setLocalZOrder(5);
		addChild(tempLayer_flag);

		auto imageView_leftFlag = ImageView::create();
		imageView_leftFlag->loadTexture("res_ui/shousuo.png");
		imageView_leftFlag->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_leftFlag->setPosition(Vec2(50,90));
		imageView_leftFlag->setRotation(-135);
		imageView_leftFlag->setScale(0.5f);
		tempLayer_flag->addChild(imageView_leftFlag);
		auto imageView_rightFlag = ImageView::create();
		imageView_rightFlag->loadTexture("res_ui/shousuo.png");
		imageView_rightFlag->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_rightFlag->setPosition(Vec2(385,90));
		imageView_rightFlag->setRotation(45);
		imageView_rightFlag->setScale(0.5f);
		tempLayer_flag->addChild(imageView_rightFlag);


		this->refreshCurRoleValue(curRoleIndex);
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(440,430));
		return true;
	}

	return false;
}

void BackPackage::onEnter()
{
	UIScene::onEnter();
	this->callBackPackAndRole(roleAndPack);
	this->openAnim();
}

void BackPackage::onExit()
{
	UIScene::onExit();
}
bool BackPackage::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return true;
}

void BackPackage::onTouchEnded( Touch *pTouch, Event *pEvent )
{
	//removeFromParentAndCleanup(true);
}

void BackPackage::callBack( Ref * obj )
{
	std::string equementStr=GameView::getInstance()->pacPageView->curFolder->goods().name();

	std::string showGoodsInfoStr = "";
	const char *goodsInfoBeginStr = StringDataManager::getString("chat_goodsInfoShowBegin");
	const char *goodsInfoEndStr = StringDataManager::getString("chat_goodsInfoShowEnd");
	showGoodsInfoStr.append(goodsInfoBeginStr);
	showGoodsInfoStr.append(equementStr);
	showGoodsInfoStr.append(goodsInfoEndStr);

	Color3B color = GameView::getInstance()->getGoodsColorByQuality(GameView::getInstance()->pacPageView->curFolder->goods().quality());
	int newColor = GameUtils::convertToColorInt(color);
	std::string propId = GameView::getInstance()->pacPageView->curFolder->goods().id();
	long long propInstanceId = GameView::getInstance()->pacPageView->curFolder->goods().instanceid();
	std::string str_=RichElementButton::makeButton(showGoodsInfoStr.c_str(), propId.c_str(), propInstanceId,newColor);   // temp code

	auto chatui = (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
	auto text=(RichTextInputBox *)chatui->getChildByTag(CHATLayer3)->getChildByTag(RICHTEXTINPUT_TAG);
	text->onTextFieldInsertText(NULL, str_.c_str(), str_.size());
}

void BackPackage::callBackOfPrivateUi()
{
	std::string equementStr=GameView::getInstance()->pacPageView->curFolder->goods().name();

	std::string showGoodsInfoStr = "";
	const char *goodsInfoBeginStr = StringDataManager::getString("chat_goodsInfoShowBegin");
	const char *goodsInfoEndStr = StringDataManager::getString("chat_goodsInfoShowEnd");
	showGoodsInfoStr.append(goodsInfoBeginStr);
	showGoodsInfoStr.append(equementStr);
	showGoodsInfoStr.append(goodsInfoEndStr);

	Color3B color = GameView::getInstance()->getGoodsColorByQuality(GameView::getInstance()->pacPageView->curFolder->goods().quality());
	int newColor = GameUtils::convertToColorInt(color);
	std::string propId = GameView::getInstance()->pacPageView->curFolder->goods().id();
	long long propInstanceId = GameView::getInstance()->pacPageView->curFolder->goods().instanceid();
	std::string str_=RichElementButton::makeButton(showGoodsInfoStr.c_str(), propId.c_str(), propInstanceId,newColor);   // temp code

	auto privateui_  = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);
	auto text=(RichTextInputBox *)privateui_->textInputBox;
	text->onTextFieldInsertText(NULL, str_.c_str(), str_.size());
}

void BackPackage::CurrentPageViewChanged(Ref *pSender, PageView::EventType type)
{
	CCLOG("currentIndex ==========  %d",(int)(pageView->getPageView()->getCurrentPageIndex()));

	if (type == PageView::EventType::TURNING) {
		auto pageView = dynamic_cast<PageView*>(pSender);
		switch (static_cast<int>(pageView->getCurrentPageIndex()))
		{
		case 0:
			currentPage->setPosition(Vec2(151, 57));
			break;
		case 1:
			currentPage->setPosition(Vec2(172, 57));
			break;
		case 2:
			currentPage->setPosition(Vec2(191, 57));
			break;
		case 3:
			currentPage->setPosition(Vec2(210, 57));
			break;
		case 4:
			currentPage->setPosition(Vec2(229, 57));
			break;

		}
	}
}

void BackPackage::refreshCurRoleValue(int idx)
{
	auto roleValue_ = new CActiveRole();
	if (idx == 0)
	{
		roleValue_->CopyFrom(* GameView::getInstance()->myplayer->getActiveRole());
	}else
	{
		roleValue_->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(idx - 1)->activerole());
	}

	auto roleLayer= Layer::create();
	roleLayer->setIgnoreAnchorPointForPosition(false);
	roleLayer->setAnchorPoint(Vec2(0.5f,0.5f));
	roleLayer->setPosition(Vec2(m_LayerRole->getContentSize().width/2,m_LayerRole->getContentSize().height/2));
	roleLayer->setContentSize(Size(800,480));
	roleLayer->setTag(ROLELAYEROFEQUIP_BACKPAC);
	m_LayerRole->addChild(roleLayer);

	std::string roleName_ = roleValue_->rolebase().name();
	auto label_name=CCRichLabel::createWithString(roleName_.c_str(),Size(390,38),NULL,NULL,0);
	label_name->setAnchorPoint(Vec2(0.5f,0));
	label_name->setPosition(Vec2(400,395));
	roleLayer->addChild(label_name);

	if (roleValue_->playerbaseinfo().viplevel() > 0)
	{
		auto widgetVip = MainScene::addVipInfoByLevelForWidget(roleValue_->playerbaseinfo().viplevel());
		roleLayer->addChild(widgetVip);
		widgetVip->setPosition(Vec2(label_name->getPositionX() + label_name->getContentSize().width/2 +10,label_name->getPositionY() + widgetVip->getContentSize().height/2));
	}

	int countryId_  = roleValue_->playerbaseinfo().country();
	if (countryId_ > 0 && countryId_ <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId_);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		auto countrySp_ = Sprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(Vec2(1,0.5f));
		countrySp_->setPosition(Vec2(label_name->getPositionX()- label_name->getContentSize().width/2*0.9f,label_name->getPositionY()+countrySp_->getContentSize().height/2*0.8f));
		countrySp_->setScale(0.65f);
		roleLayer->addChild(countrySp_);
	}

	std::string pressionStr_ = roleValue_->profession().c_str();
	std::string professionPath = BasePlayer::getProfessionIconByStr(pressionStr_.c_str());
	imageIcon_->loadTexture(professionPath.c_str());

	int levelStr_ =  roleValue_->level();
	char lvStr_[5];
	sprintf(lvStr_,"%d",levelStr_);
	std::string roleLevelStr_ ="LV.";
	roleLevelStr_.append(lvStr_);
	labelLevel->setString(roleLevelStr_.c_str());
	//////////////////////////////
	int fightPoint = roleValue_->fightpoint();
	char fightStr[20];
	sprintf(fightStr,"%d",fightPoint);
	labelFightValue_->setString(fightStr);
	///////////////////////
	int allFightPoint = GameView::getInstance()->myplayer->player->fightpoint();
	for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();++i)
	{
		allFightPoint += GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint();
	}
	char allFightStr[20];
	sprintf(allFightStr,"%d",allFightPoint);
	labelAllFightValue_->setString(allFightStr);

	this->getCurRolequipment(idx);

	// show the player's animation
	std::string roleFigureName = ActorUtils::getActorFigureName(*roleValue_);
	GameActorAnimation* pAnim = ActorUtils::createActorAnimation(roleFigureName.c_str(), roleValue_->hand().c_str(),
																	weapEffectStrForRefine.c_str(), weapEffectStrForStar.c_str());
	pAnim->setPosition(Vec2(400,255));
	roleLayer->addChild(pAnim);

	delete roleValue_;
}

void BackPackage::callBackShowEquipInfo(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn_ = (Button *)pSender;
		int num = btn_->getTag();
		auto goods_ = new GoodsInfo();
		goods_->CopyFrom(playerEquipments.at(num)->goods());

		long long generalId = 0;
		if (curRoleIndex == 0)
		{
			generalId = 0;
		}
		else
		{
			generalId = GameView::getInstance()->generalsInLineDetailList.at(curRoleIndex - 1)->generalid();
		}

		auto equipmentItemInfos = EquipmentInfoShow::create(goods_, playerEquipments, generalId);
		equipmentItemInfos->setIgnoreAnchorPointForPosition(false);
		equipmentItemInfos->setAnchorPoint(Vec2(0.5f, 0.5f));
		GameView::getInstance()->getMainUIScene()->addChild(equipmentItemInfos);

		delete goods_;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void BackPackage::callBackClose(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void BackPackage::scrollViewDidScroll( cocos2d::extension::ScrollView* view )
{

}

void BackPackage::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

void BackPackage::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void BackPackage::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void BackPackage::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void BackPackage::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	int cellId = cell->getIdx();
	curRoleIndex = cellId;
	auto roleLayer_ = (Layer *)m_LayerRole->getChildByTag(ROLELAYEROFEQUIP_BACKPAC);
	if (roleLayer_ != NULL)
	{
		roleLayer_->removeFromParentAndCleanup(true);
	}

	image_accessories->setVisible(true);
	image_shoes->setVisible(true);
	image_helmet->setVisible(true);
	image_ring->setVisible(true);
	image_clothes->setVisible(true);
	image_arms->setVisible(true);

	this->refreshCurRoleValue(curRoleIndex);
}

cocos2d::Size BackPackage::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(81,103);
}

cocos2d::extension::TableViewCell* BackPackage::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	auto cell = table->dequeueCell();
	cell = new TableViewCell();
	cell->autorelease();

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	if (idx == 0)
	{
		auto  smaillFrame = Sprite::create("res_ui/generals_white.png");
		smaillFrame->setAnchorPoint(Vec2(0, 0));
		smaillFrame->setPosition(Vec2(0, 0));
		smaillFrame->setScaleX(0.75f);
		smaillFrame->setScaleY(0.7f);
		cell->addChild(smaillFrame);
		//role icon
		std::string pressionStr_ = GameView::getInstance()->myplayer->getActiveRole()->profession();
		int roleIconIndex_ =  BasePlayer::getProfessionIdxByName(pressionStr_.c_str());
		std::string roleIcon_ = BasePlayer::getBigHeadPathByProfession(roleIconIndex_);

		auto sprite_icon = Sprite::create(roleIcon_.c_str());
		sprite_icon->setAnchorPoint(Vec2(0.5f, 0.5f));
		sprite_icon->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), smaillFrame->getContentSize().height/2*smaillFrame->getScaleY()));
		sprite_icon->setScale(0.75f);
		cell->addChild(sprite_icon);
		//
		auto sprite_lvFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80.png");
		sprite_lvFrame->setAnchorPoint(Vec2(1.0f, 0));
		sprite_lvFrame->setContentSize(Size(34,13));
		sprite_lvFrame->setPosition(Vec2(smaillFrame->getContentSize().width*smaillFrame->getScaleX()-5, 19));
		cell->addChild(sprite_lvFrame);
		//role level
		std::string _lv = "LV";
		char roleLevel[5];
		sprintf(roleLevel,"%d",GameView::getInstance()->myplayer->getActiveRole()->level());
		_lv.append(roleLevel);
		auto label_lv = Label::createWithTTF(_lv.c_str(),APP_FONT_NAME,13);
		label_lv->setAnchorPoint(Vec2(1.0f, 0));
		label_lv->setPosition(Vec2(smaillFrame->getContentSize().width*smaillFrame->getScaleX()-5, 17));
		label_lv->enableOutline(Color4B::BLACK, 2.0f);
		cell->addChild(label_lv);

		auto sprite_nameFrame = Sprite::create("res_ui/name_di2.png");
		sprite_nameFrame->setAnchorPoint(Vec2(0.5f, 0));
		sprite_nameFrame->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 6));
		sprite_nameFrame->setScaleX(1.1f);
		cell->addChild(sprite_nameFrame);
		//�佫���
		const char * generalRoleName_ = StringDataManager::getString("generalList_RoleName");
		auto label_name = Label::createWithTTF(generalRoleName_,APP_FONT_NAME,14);
		label_name->setAnchorPoint(Vec2(0.5f, 0));
		label_name->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 5));
		label_name->setColor(Color3B(47,93,13));
		cell->addChild(label_name);
	}
	else
	{
		int curGeneralQuality = GameView::getInstance()->generalsInLineDetailList.at(idx - 1)->currentquality();
		auto sprite_frame = Sprite::create(GeneralsUI::getBigHeadFramePath(curGeneralQuality).c_str());
		sprite_frame->setAnchorPoint(Vec2(0, 0));
		sprite_frame->setPosition(Vec2(0, 0));
		sprite_frame->setScaleX(0.75f);
		sprite_frame->setScaleY(0.7f);
		cell->addChild(sprite_frame);

		auto generalBaseMsgFromDB  = GeneralsConfigData::s_generalsBaseMsgData[GameView::getInstance()->generalsInLineDetailList.at(idx - 1)->modelid()];
		std::string curGeneralFace_ = generalBaseMsgFromDB->get_half_photo();
		//general icon
		std::string sprite_icon_path = "res_ui/generals/";
		sprite_icon_path.append(curGeneralFace_);
		sprite_icon_path.append(".anm");
		auto la_head = CCLegendAnimation::create(sprite_icon_path);
		if (la_head)
		{
			la_head->setPlayLoop(true);
			la_head->setReleaseWhenStop(false);
			la_head->setScale(0.7f*0.75f);	
			la_head->setPosition(Vec2(4,13));
			cell->addChild(la_head);
		}
	
		auto sprite_lvFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80.png");
		sprite_lvFrame->setAnchorPoint(Vec2(1.0f, 0));
		sprite_lvFrame->setContentSize(Size(34,13));
		sprite_lvFrame->setPosition(Vec2(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, 19));
		cell->addChild(sprite_lvFrame);
		//general level
		int curGeneralLevel = GameView::getInstance()->generalsInLineDetailList.at(idx - 1)->activerole().level();
		std::string _lv = "LV";
		char generalLevel_[10];
		sprintf(generalLevel_,"%d",curGeneralLevel);
		_lv.append(generalLevel_);
		auto label_lv = Label::createWithTTF(_lv.c_str(),APP_FONT_NAME,13);
		label_lv->setAnchorPoint(Vec2(1.0f, 0));
		label_lv->setPosition(Vec2(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, 18));
		label_lv->enableOutline(Color4B::BLACK, 2.0f);
		cell->addChild(label_lv);

		auto sprite_nameFrame = Sprite::create("res_ui/name_di2.png");
		sprite_nameFrame->setAnchorPoint(Vec2(0.5f, 0));
		sprite_nameFrame->setPosition(Vec2(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 6));
		sprite_nameFrame->setScaleX(1.1f);
		cell->addChild(sprite_nameFrame);
		//general name
		std::string curGeneralname_ = GameView::getInstance()->generalsInLineDetailList.at(idx - 1)->activerole().rolebase().name();

		auto label_name=CCRichLabel::createWithString(curGeneralname_.c_str(),Size(390,38),NULL,NULL,0);
		label_name->setScale(0.7f);
		label_name->setAnchorPoint(Vec2(0.5f,0));
		label_name->setPosition(Vec2(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 5));
		cell->addChild(label_name);
		//�ϡ�ж
		if (GameView::getInstance()->generalsInLineDetailList.at(idx - 1)->rare()>0)
		{
			auto sprite_star = Sprite::create(RecuriteActionItem::getStarPathByNum(GameView::getInstance()->generalsInLineDetailList.at(idx - 1)->rare()).c_str());
			sprite_star->setAnchorPoint(Vec2(0,0));
			sprite_star->setScale(0.6f);
			sprite_star->setPosition(Vec2(5, 21));
			cell->addChild(sprite_star);
		}
		//��佫���
		int curGeneralEvol_ =GameView::getInstance()->generalsInLineDetailList.at(idx - 1)->evolution();
		if (curGeneralEvol_ > 0)
		{
			std::string rankPathBase = "res_ui/rank/rank";
			char s[10];
			sprintf(s,"%d",curGeneralEvol_);
			rankPathBase.append(s);
			rankPathBase.append(".png");
			auto sprite_rank = Sprite::create(rankPathBase.c_str());
			sprite_rank->setScale(0.7f);
			sprite_rank->setAnchorPoint(Vec2(1.0f,1.0f));
			sprite_rank->setPosition(Vec2(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, sprite_frame->getContentSize().height*sprite_frame->getScaleY()-6));
			cell->addChild(sprite_rank);
		}
	}

	return cell;
}

ssize_t BackPackage::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	return GameView::getInstance()->generalsInLineDetailList.size()+1;
}

void BackPackage::getCurRolequipment(int idx)
{
	int vectorSize = 0;
	std::vector<CEquipment *>::iterator iter;
	for (iter = playerEquipments.begin();iter != playerEquipments.end();++iter)
	{
		delete * iter;
	}
	playerEquipments.clear();
	
	if (idx == 0)
	{
		for (int i = 0;i< GameView::getInstance()->EquipListItem.size();i++)
		{
			auto equip_ = new CEquipment();
			equip_->CopyFrom( *GameView::getInstance()->EquipListItem.at(i));
			playerEquipments.push_back(equip_);
			//delete equip_;
		}
	}else
	{
		for (int i = 0;i< GameView::getInstance()->generalsInLineDetailList.at(idx - 1)->equipments_size();i++)
		{
			auto equip_ = new CEquipment();
			equip_->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(idx - 1)->equipments(i));
			playerEquipments.push_back(equip_);
			//delete equip_;
		}
	}
	
	auto roleLayer_ = (Layer *)m_LayerRole->getChildByTag(ROLELAYEROFEQUIP_BACKPAC);
	vectorSize = playerEquipments.size();
	for(int i=0;i<vectorSize;i++)
	{
		int equipmentquality_ = playerEquipments.at(i)->goods().quality();
		std::string frameColorPath = "res_ui/";
		if (equipmentquality_ == 1)
		{
			frameColorPath.append("sdi_white");
		}
		else if (equipmentquality_ == 2)
		{
			frameColorPath.append("sdi_green");
		}
		else if (equipmentquality_ == 3)
		{
			frameColorPath.append("sdi_bule");
		}
		else if (equipmentquality_ == 4)
		{
			frameColorPath.append("sdi_purple");
		}
		else if (equipmentquality_ == 5)
		{
			frameColorPath.append("sdi_orange");
		}
		else
		{
			frameColorPath.append("sdi_white");
		}
		frameColorPath.append(".png");

		auto Btn_pacItemFrame = Button::create();
		Btn_pacItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnabled(true);
		Btn_pacItemFrame->setPressedActionEnabled(true);
		Btn_pacItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_pacItemFrame->setTag(i);
		Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(BackPackage::callBackShowEquipInfo, this));
		roleLayer_->addChild(Btn_pacItemFrame);

		switch(playerEquipments.at(i)->part())
		{
		case 4 :
			{
				Btn_pacItemFrame->setPosition(Vec2(267, 381.5f));    //���� 4
				image_arms->setVisible(false);
				//
				int pression_ = playerEquipments.at(i)->goods().equipmentdetail().profession();
				int refineLevel =  playerEquipments.at(i)->goods().equipmentdetail().gradelevel();
				int starLevel =  playerEquipments.at(i)->goods().equipmentdetail().starlevel();

				weapEffectStrForRefine = ActorUtils::getWeapEffectByRefineLevel(pression_,refineLevel);
				weapEffectStrForStar = ActorUtils::getWeapEffectByStarLevel(pression_, starLevel);
			}break;
		case 2 :
			{
				Btn_pacItemFrame->setPosition(Vec2(267, 301.5f));   //��· 2
				image_clothes->setVisible(false);
			}break;
		case 0 :
			{
				Btn_pacItemFrame->setPosition(Vec2(532, 381.5f));    //�ͷ�  0
				image_helmet->setVisible(false);
			}break;
		case 7 :
			{
				Btn_pacItemFrame->setPosition(Vec2(267, 220.5f));    //��ָ7
				image_ring->setVisible(false);
			}break;
		case 3 :
			{
				Btn_pacItemFrame->setPosition(Vec2(532, 222));    //�� 3
				image_accessories->setVisible(false);
			}break;
		case 9 :
			{
				Btn_pacItemFrame->setPosition(Vec2(532, 302));    //�Ь 9
				image_shoes->setVisible(false);
			}break;
		}

		std::string  iconPath_ ="res_ui/props_icon/";
		std::string equipicon_ = playerEquipments.at(i)->goods().icon();
		iconPath_.append(equipicon_);
		iconPath_.append(".png");

		auto equipIcon_ =ImageView::create();
		equipIcon_->loadTexture(iconPath_.c_str());
		equipIcon_->setAnchorPoint(Vec2(0.5f,0.5f));
		equipIcon_->setPosition(Vec2(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		if (playerEquipments.at(i)->goods().binding() == 1)
		{
			auto ImageView_bound = ImageView::create();
			ImageView_bound->loadTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(Vec2(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(Vec2(-25,23));
			Btn_pacItemFrame->addChild(ImageView_bound);
		}


		int starLv_ =playerEquipments.at(i)->goods().equipmentdetail().starlevel();
		if (starLv_>0)
		{
			char starLvLabel_[5];
			sprintf(starLvLabel_,"%d",starLv_);
			auto label_ =Label::createWithTTF(starLvLabel_, APP_FONT_NAME, 13);
			label_->setAnchorPoint(Vec2(0,0));
			label_->setPosition(Vec2(-23,-23));
			Btn_pacItemFrame->addChild(label_);

			auto imageStar_ =ImageView::create();
			imageStar_->loadTexture("res_ui/star_on.png");
			imageStar_->setScale(0.7f);
			imageStar_->setAnchorPoint(Vec2(0,0));
			imageStar_->setPosition(Vec2(label_->getContentSize().width-22,label_->getPosition().y));
			Btn_pacItemFrame->addChild(imageStar_);
		}

		int strengthLv_ = playerEquipments.at(i)->goods().equipmentdetail().gradelevel();
		if (strengthLv_>0)
		{
			char strengthLv[5];
			sprintf(strengthLv,"%d",strengthLv_);
			std::string strengthStr_ = "+";
			strengthStr_.append(strengthLv);

			auto label_StrengthLv = Label::createWithTTF(strengthStr_.c_str(), APP_FONT_NAME, 13);
			label_StrengthLv->setAnchorPoint(Vec2(1,1));
			label_StrengthLv->setPosition(Vec2(23,25));
			Btn_pacItemFrame->addChild(label_StrengthLv);
		}

		if (playerEquipments.at(i)->goods().equipmentdetail().durable()<=0)
		{
			auto image_sp = ImageView::create();
			image_sp->setAnchorPoint(Vec2(0.5,0.5f));
			image_sp->setPosition(Vec2(0,0));
			image_sp->loadTexture("res_ui/mengban_red55.png");
			image_sp->setScale9Enabled(true);
			image_sp->setCapInsets(Rect(5,5,1,1));
			image_sp->setContentSize(Btn_pacItemFrame->getContentSize());
			Btn_pacItemFrame->addChild(image_sp);

			auto l_des = Label::createWithTTF(StringDataManager::getString("packageitem_notAvailable"), APP_FONT_NAME, 18);
			l_des->setAnchorPoint(Vec2(0.5f,0.5f));
			l_des->setPosition(Vec2(0,0));
			image_sp->addChild(l_des);
		}
	}
}

void BackPackage::callBackPackAndRole( Ref * obj )
{
	auto tab_ = (UITab *)obj;
	tab_->setHightLightLabelColor(Color3B(47,93,13));
	tab_->setNormalLabelColor(Color3B(255,255,255));

	int index_ = tab_->getCurrentIndex();
	curType = index_;
	if(index_ == 0)
	{
		m_LayerRole->setVisible(true);
		panelRole->setVisible(true);
		pageView->setVisible(false);
		panelPac->setVisible(false);

		tempLayer_flag->setVisible(true);
		//pageView_kuang->setVisible(false);
	}else
	{
		pageView->setVisible(true);
		panelRole->setVisible(false);
		m_LayerRole->setVisible(false);
		panelPac->setVisible(true);

		tempLayer_flag->setVisible(false);
		//pageView_kuang->setVisible(true);
	}
}

void BackPackage::PageScrollToDefault()
{
	pageView->getPageView()->scrollToPage(0);
	CurrentPageViewChanged(pageView, PageView::EventType::TURNING);
}

///////////////////////////////////

EquipmentInfoShow::EquipmentInfoShow(void)
{
	
}

EquipmentInfoShow::~EquipmentInfoShow(void)
{
	delete goods_;
}


EquipmentInfoShow * EquipmentInfoShow::create( GoodsInfo * goods ,std::vector<CEquipment *> equipvector,long long generalId)
{
	auto equipInfo = new EquipmentInfoShow();
	if (equipInfo && equipInfo->init(goods,equipvector,generalId))
	{
		equipInfo->autorelease();
		return equipInfo;
	}
	CC_SAFE_DELETE(equipInfo);
	return NULL;
}

bool EquipmentInfoShow::init( GoodsInfo * goods ,std::vector<CEquipment *> equipvector,long long generalId)
{
	if (GoodsItemInfoBase::init(goods,equipvector,generalId))
	{
		goods_ =new GoodsInfo();
		goods_->CopyFrom(*goods);

		const char *strings_synthesize = StringDataManager::getString("goods_chat_btnAdd");
		char *str_synthesize=const_cast<char*>(strings_synthesize);

		auto Button_getAnnes= Button::create();
		Button_getAnnes->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_getAnnes->setTouchEnabled(true);
		Button_getAnnes->setPressedActionEnabled(true);
		Button_getAnnes->addTouchEventListener(CC_CALLBACK_2(EquipmentInfoShow::callBackShowEquip, this));
		Button_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_getAnnes->setScale9Enabled(true);
		Button_getAnnes->setContentSize(Size(110,43));
		Button_getAnnes->setCapInsets(Rect(18,9,2,23));
		Button_getAnnes->setPosition(Vec2(141,25));

		auto Label_getAnnes = Label::createWithTTF(str_synthesize, APP_FONT_NAME, 16);
		Label_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_getAnnes->setPosition(Vec2(0,0));
		Button_getAnnes->addChild(Label_getAnnes);
		m_pLayer->addChild(Button_getAnnes);
		return true;
	}
	return false;
}

void EquipmentInfoShow::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void EquipmentInfoShow::onExit()
{
	UIScene::onExit();
}

void EquipmentInfoShow::callBackShowEquip(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		std::string equementStr = goods_->name();

		std::string showGoodsInfoStr = "";
		const char *goodsInfoBeginStr = StringDataManager::getString("chat_goodsInfoShowBegin");
		const char *goodsInfoEndStr = StringDataManager::getString("chat_goodsInfoShowEnd");
		showGoodsInfoStr.append(goodsInfoBeginStr);
		showGoodsInfoStr.append(equementStr);
		showGoodsInfoStr.append(goodsInfoEndStr);

		Color3B color = GameView::getInstance()->getGoodsColorByQuality(goods_->quality());
		int newColor = GameUtils::convertToColorInt(color);
		std::string propId = goods_->id();
		long long propInstanceId = goods_->instanceid();
		std::string str_ = RichElementButton::makeButton(showGoodsInfoStr.c_str(), propId.c_str(), propInstanceId, newColor);   // temp code

		auto chatui = (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
		auto privateui = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);
		if (privateui != NULL)
		{
			if (privateui != NULL)
			{
				privateui->textInputBox->onTextFieldInsertText(NULL, str_.c_str(), str_.size());
			}

		}
		else
		{
			auto text = (RichTextInputBox *)chatui->getChildByTag(CHATLayer3)->getChildByTag(RICHTEXTINPUT_TAG);
			text->onTextFieldInsertText(NULL, str_.c_str(), str_.size());
		}

		this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}
