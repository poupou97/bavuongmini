#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class ExpressionLayer :public UIScene
{
public:
	ExpressionLayer(void);
	~ExpressionLayer(void);

	static ExpressionLayer * create(int uitag);
	bool init(int uitag);
	void onEnter();

	bool onTouchBegan(Touch *pTouch, Event *pEvent);
	void onTouchEnded(Touch *pTouch, Event *pEvent);
	void callBack(Ref *pSender, Widget::TouchEventType type);
private:
	int index;
	int uitagIndex;
};

