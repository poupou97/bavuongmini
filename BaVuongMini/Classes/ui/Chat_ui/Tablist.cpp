#include "Tablist.h"
#include "ChatUI.h"
#include "../extensions/CCMoveableMenu.h"
#include "../extensions/RichTextInput.h"
#include "AddPrivateUi.h"
#include "ChatCell.h"
#include "GameView.h"
#include "../../messageclient/element/CRelationPlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"

enum
{
	WHOLE_RICHLABEL,
	WORLD_RICHLABEL,
	COUNTRY_RICHLABEL,
	CAMP_RICHLABEL,
	PRIVATE_RICHLABEL,
	TEAM_RICHLABEL,
	CURRECT_RICHLABEL,
	SYSTEN_RICHLABEL,
};

Tablist::Tablist(void)
{
	
}

Tablist::~Tablist(void)
{

}

Tablist * Tablist::create()
{
	Tablist *list=new Tablist();
	if (list && list->init())
	{
		list->autorelease();
		return list;
	}
	CC_SAFE_DELETE(list);
	return NULL;
}
bool Tablist::init()
{
	if (UIScene::init())
	{
		Size winSize=Director::getInstance()->getVisibleSize();

		tableView = TableView::create(this, Size(674, 298));
		tableView->setDirection(TableView::Direction::VERTICAL);
		tableView->setAnchorPoint(Vec2(0,0));
		tableView->setPosition(Vec2(0,0));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(tableView, -1, TABVIEW_TAG);
		tableView->reloadData();
		
		if (enterTime == 0)
		{
			enterTime++;
		}else
		{
			if(tableView->getContentSize().height > tableView->getViewSize().height)
				tableView->setContentOffset(tableView->maxContainerOffset(), false);	
		}
		//setTouchEnabled(false);
		return true;
	}
	return false;
}
void Tablist::onEnter()
{
	UIScene::onEnter();

}
void Tablist::onExit()
{
	UIScene::onExit();
}

void Tablist::tableCellTouched(TableView* table, TableViewCell* cell)
{
	CCLOG("cell touched at index: %i", cell->getIdx());
	int idx= cell->getIdx();
	auto chatui =(ChatUI *)this->getParent()->getParent();
	chatui->playerName_= GameView::getInstance()->selectCell_vdata.at(idx)->playName;
	
	if (strcmp(GameView::getInstance()->myplayer->getActorName().c_str(),chatui->playerName_.c_str()) == 0)
	{
		chatui->playerName_ = GameView::getInstance()->selectCell_vdata.at(idx)->listenerName_;
	}

	chatui->playerId_= GameView::getInstance()->selectCell_vdata.at(idx)->playerId;

	chatui->playerVipLevel = GameView::getInstance()->selectCell_vdata.at(idx)->playerVip_;

	chatui->countryId = 0;
	chatui->playerLevel = 20;
	chatui->pressionId = 0;

}

Size Tablist::tableCellSizeForIndex(TableView *table, unsigned int idx)
{
	int h_= 25;
	if (h_ <= GameView::getInstance()->selectCell_vdata.at(idx)->getContentSize().height)
	{
		h_ = GameView::getInstance()->selectCell_vdata.at(idx)->getContentSize().height;
	}
	return Size(480,h_);
}

TableViewCell* Tablist::tableCellAtIndex(TableView *table, ssize_t  idx)
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = (ChatCell*)table->dequeueCell();   // this method must be called
	cell = GameView::getInstance()->selectCell_vdata.at(idx);
	return cell;
}

ssize_t Tablist::numberOfCellsInTableView(TableView *table)
{
	int cellLine=GameView::getInstance()->selectCell_vdata.size();
	return cellLine;	
}

void Tablist::scrollViewDidScroll( cocos2d::extension::ScrollView* view )
{
}

void Tablist::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{
}

bool Tablist::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return true;
}

void Tablist::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void Tablist::onTouchEnded( Touch *pTouch, Event *pEvent )
{
	//removeFromParentAndCleanup(true);
}

void Tablist::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

int Tablist::enterTime=0;

