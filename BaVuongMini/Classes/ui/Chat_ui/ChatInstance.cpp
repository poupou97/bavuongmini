#include "ChatInstance.h"
#include "GameView.h"
#include "../../utils/StaticDataManager.h"

ChatInstance::ChatInstance()
{
	m_curSelectChannel = 0;
}

ChatInstance::~ChatInstance()
{
}

void ChatInstance::setCurChannel( int index_ )
{
	m_curSelectChannel = index_;
}

int ChatInstance::getCurChannel()
{
	return m_curSelectChannel;
}

ChatInstance * ChatInstance::getInstance()
{
	if (s_chatInstance == NULL)
	{
		s_chatInstance = new ChatInstance();
	}

	return s_chatInstance;
}


ChatInstance * ChatInstance::s_chatInstance = NULL;
