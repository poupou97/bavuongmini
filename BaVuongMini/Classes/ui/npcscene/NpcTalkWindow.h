#ifndef _NPCSCENE_NPCTALKWINDOW_H_
#define _NPCSCENE_NPCTALKWINDOW_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CNpcDialog;

class NpcTalkWindow : public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	NpcTalkWindow();
	~NpcTalkWindow();

	virtual void update(float dt);
	static NpcTalkWindow* create(CNpcDialog * npcDialog);
	bool init(CNpcDialog * npcDialog);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);


	void CloseEvent(Ref * pSender, Widget::TouchEventType type);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	bool isHasMission(int npcId);

	int getLockedNpcId();

	//��ѧ
	int mTutorialScriptInstanceId;
	int mTutorialIndex;
	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	
	void addCCTutorialIndicatorNpc(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction,int tutorialIndex);
	void removeCCTutorialIndicator();

private:
	CNpcDialog * curNpcDialog;
	Size winsize;
	int m_nLockedId;
};
#endif;
