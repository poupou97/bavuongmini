#include "NpcTalkWindow.h"
#include "../../messageclient/element/CNpcDialog.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/CCRichLabel.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../missionscene/MissionTalkWithNpc.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../../gamescene_state/role/FunctionNPC.h"
#include "../../gamescene_state/role/MyPlayer.h"
//#include "../../../../../cocos2dx/cocoa/CCGeometry.h"
#include "../Auction_ui/AuctionUi.h"
#include "../storehouse_ui/StoreHouseUI.h"
#include "../shop_ui/ShopUI.h"
#include "../family_ui/ApplyFamily.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../family_ui/ManageFamily.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"
#include "../../messageclient/element/CGuildBase.h"
#include "../../utils/GameUtils.h"
#include "GameAudio.h"
#include "../challengeRound/ChallengeRoundUi.h"
#include "../Active_ui/WorldBoss_ui/WorldBossUI.h"
#include "../../ui/rewardTask_ui/RewardTaskMainUI.h"
#include "../rewardTask_ui/RewardTaskData.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "cocostudio/CCSGUIReader.h"

/// 弹板功能
#define FUNCTION_INHERIT -1       // 继承
#define FUNCTION_SHANGDIAN 1       //商店
#define FUNCTION_CANGKU 6       // 打开仓库
#define FUNCTION_XIULIZHUANGBEI 10       // 修理装备
#define FUNCTION_WUJIANGFUHUO 19   //武将复活
#define FUNCTION_JISHOU 51       // 继承
#define FUNCTION_MY_JISHOU 52    // 我的寄售
#define FUNCTION_CLOSE 53        // 关闭窗体
#define FUNCTION_XIANGQIAN 55    // 镶嵌
#define FUNCTION_XILIAN 56       // 洗练
#define FUNCTION_FENGYIN 57      // 封印
#define FUNCTION_TRANSFOR 5   // 传送功能
#define FUNCTION_FAMILYCREATE 2       // 家族创建
#define FUNCTION_FAMILYAPPLY 18       // 家族申请
#define FUNCTION_DUIHUANMA 59       // 兑换码
#define FUNCTION_SHIJIEBOSS 62          //世界Boss
#define FUNCTION_XUANSHANGRENWU 63          //悬赏任务

#define FUNCTION_ANSWER 17    //答题

#define FUNCTTON_SINGCOPY 21 //过关斩将 单人副本
NpcTalkWindow::NpcTalkWindow()
{
}


NpcTalkWindow::~NpcTalkWindow()
{
}


void NpcTalkWindow::update( float dt )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow) != NULL)
	{
		if (GameView::getInstance()->missionManager->isOutOfMissionTalkArea(this->getLockedNpcId()))
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
			this->closeAnim();
		}
	}
}


NpcTalkWindow* NpcTalkWindow::create( CNpcDialog * npcDialog )
{
	auto npcTalkWindow = new NpcTalkWindow();
	if (npcTalkWindow && npcTalkWindow->init(npcDialog))
	{
		npcTalkWindow->autorelease();
		return npcTalkWindow;
	}
	CC_SAFE_DELETE(npcTalkWindow);
	return NULL;
}

bool NpcTalkWindow::init( CNpcDialog * npcDialog )
{
	if (UIScene::init())
	{
		m_nLockedId = GameView::getInstance()->myplayer->getLockedActorId();
		curNpcDialog = npcDialog;

		std::map<long long ,CNpcInfo*>::const_iterator cIter;
		cIter = GameWorld::NpcInfos.find(m_nLockedId);
		if (cIter == GameWorld::NpcInfos.end()) // 没找到就是指向END了  
		{
			return false;
		}

		auto npcInfo = GameWorld::NpcInfos[m_nLockedId];

		winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();
		//加载UI
// 		if(LoadSceneLayer::TalkWithNpcLayer->getWidgetParent() != NULL)
// 		{
// 			LoadSceneLayer::TalkWithNpcLayer->removeFromParentAndCleanup(false);
// 		}
// 		//ppanel = (Layout*)LoadSceneLayer::s_pPanel->copy();
// 		Layout *ppanel = LoadSceneLayer::TalkWithNpcLayer;
// 		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
// 		ppanel->getValidNode()->setContentSize(Size(350, 420));
// 		ppanel->setPosition(Vec2::ZERO);
// 		ppanel->setScale(1.0f);
// 		ppanel->setTouchEnabled(true);
// 		m_pLayer->addChild(ppanel);

		auto ppanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/talkWithNpc_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->setPosition(Vec2::ZERO);	
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

// 		Layout * panel_mission = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_renwu");
// 		panel_mission->setVisible(false);
		auto button_close = (Button *)Helper::seekWidgetByName(ppanel,"Button_close");
		button_close->setTouchEnabled(true);
		button_close->addTouchEventListener(CC_CALLBACK_2(NpcTalkWindow::CloseEvent, this));
// 		ImageView *ImageView_headImage = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_touxiang");;
// 		std::string headPath = "res_ui/npc/";
// 		headPath.append(npcInfo->icon.c_str());
// 		headPath.append(".png");
// 		ImageView_headImage->loadTexture(headPath.c_str());
		auto ImageView_name = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_mingzi");

		// load animation
		std::string animFileName = "res_ui/npc/";
		animFileName.append(npcInfo->icon.c_str());
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/npc/caiwenji.anm";
		auto m_pAnim = CCLegendAnimation::create(animFileName);
		if (m_pAnim)
		{
			m_pAnim->setPlayLoop(true);
			m_pAnim->setReleaseWhenStop(false);
			m_pAnim->setScale(1.0f);
			m_pAnim->setPosition(Vec2(33,312));
			//m_pAnim->setPlaySpeed(.35f);
			addChild(m_pAnim);
		}

		auto sprite_anmFrame = Sprite::create("res_ui/renwua/tietu2.png");
		sprite_anmFrame->setAnchorPoint(Vec2(0.5,0.5f));
		sprite_anmFrame->setPosition(Vec2(116,356));
		addChild(sprite_anmFrame);

		FontDefinition strokeShaodwTextDef;
		strokeShaodwTextDef._fontName = std::string(APP_FONT_NAME);
		Color3B tintColorGreen   =  { 54, 92, 7 };
		strokeShaodwTextDef._fontFillColor   = tintColorGreen;   // 字体颜色
		strokeShaodwTextDef._fontSize = 22;   // 字体大小
// 		strokeShaodwTextDef.m_stroke.m_strokeEnabled = true;   // 是否勾边
// 		strokeShaodwTextDef.m_stroke.m_strokeColor   = blackColor;   // 勾边颜色，黑色
// 		strokeShaodwTextDef.m_stroke.m_strokeSize    = 2.0;   // 勾边的大小
	/*	Label* l_nameLabel = Label::createWithTTFWithFontDefinition(npcInfo->npcName.c_str(), strokeShaodwTextDef);
		Color3B tintColorBlack   =  { 0, 0, 0 };
		l_nameLabel->enableShadow(Size(2.0, -2.0), 0.5, 0.0, tintColorBlack); 
		l_nameLabel->setAnchorPoint(Vec2(0.5,0.5f));
		l_nameLabel->setPosition(Vec2(184,359));
		Color3B shadowColor = Color3B(0,0,0);   // black
		l_nameLabel->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		addChild(l_nameLabel);*/
		auto l_nameLabel = Label::createWithTTF(npcInfo->npcName.c_str(),APP_FONT_NAME,22);
		l_nameLabel->setAnchorPoint(Vec2(0.5,0.5f));
		l_nameLabel->setPosition(Vec2(207,327));
		auto shadowColor = Color4B::BLACK;   // black
		l_nameLabel->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		addChild(l_nameLabel);

		//richLabel_description
// 		Label * label_des = Label::createWithTTF(npcInfo->welcome.c_str(),APP_FONT_NAME,18,Size(285,0),TextHAlignment::LEFT,TextVAlignment::TOP);
// 		label_des->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
// 		Size desSize = label_des->getContentSize();
		auto label_des = CCRichLabel::createWithString(npcInfo->welcome.c_str(),Size(285, 0),this,NULL,0,18,5);
		Size desSize = label_des->getContentSize();
		int height = desSize.height;
		//ScrollView_description
		auto m_contentScrollView = cocos2d::extension::ScrollView::create(Size(290,99));
		m_contentScrollView->setViewSize(Size(290, 99));
		m_contentScrollView->setIgnoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		m_contentScrollView->setAnchorPoint(Vec2(0,0));
		m_contentScrollView->setPosition(Vec2(30,205));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_pLayer->addChild(m_contentScrollView);
		if (height > 99)
		{
			m_contentScrollView->setContentSize(Size(290,height));
			m_contentScrollView->setContentOffset(Vec2(0,99-height));
		}
		else
		{
			m_contentScrollView->setContentSize(Size(290,99));
		}
		m_contentScrollView->setClippingToBounds(true);

		m_contentScrollView->addChild(label_des);
		label_des->setIgnoreAnchorPointForPosition(false);
		label_des->setAnchorPoint(Vec2(0,1));
		label_des->setPosition(Vec2(3,m_contentScrollView->getContentSize().height-3));

		auto tableView_function = TableView::create(this,Size(290,165));
		//tableView_function->setPressedActionEnabled(true);
		tableView_function->setDirection(TableView::Direction::VERTICAL);
		tableView_function->setAnchorPoint(Vec2(0,0));
		tableView_function->setPosition(Vec2(35,25));
		tableView_function->setDelegate(this);
		tableView_function->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		m_pLayer->addChild(tableView_function);

// 		this->setIgnoreAnchorPointForPosition(false);
// 		this->setAnchorPoint(Vec2(0,0.5f));
// 		this->setPosition(Vec2(0,winsize.height/2));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(350,420));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(NpcTalkWindow::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(NpcTalkWindow::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(NpcTalkWindow::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(NpcTalkWindow::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}


void NpcTalkWindow::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void NpcTalkWindow::onExit()
{
	UIScene::onExit();
}

bool NpcTalkWindow::onTouchBegan(Touch *touch, Event * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	Vec2 location = touch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	return false;
}

void NpcTalkWindow::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void NpcTalkWindow::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void NpcTalkWindow::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void NpcTalkWindow::CloseEvent(Ref * pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//CCLOG("close npc window");
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}


Size NpcTalkWindow::tableCellSizeForIndex(TableView *table, unsigned int idx)
{
	return Size(270, 42);
}

TableViewCell* NpcTalkWindow::tableCellAtIndex(TableView *table, ssize_t  idx)
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell=new TableViewCell();
	cell->autorelease();

// 	Sprite * pCellBg = Sprite::create("res_ui/renwua/button_0.png");
// 	pCellBg->setAnchorPoint(Vec2(0, 0));
// 	pCellBg->setPosition(Vec2(0, 0));
// 	cell->addChild(pCellBg);
	auto pCellBg = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_0.png");
	pCellBg->setContentSize(Size(270,39));
	pCellBg->setCapInsets(Rect(38,20,1,1));
	pCellBg->setAnchorPoint(Vec2(0, 0));
	pCellBg->setPosition(Vec2(0, 0));
	cell->addChild(pCellBg);

	auto pMissionName = Label::createWithTTF("hello girl ",APP_FONT_NAME,16);
	pMissionName->setAnchorPoint(Vec2(0.5f,0.5f));
	pMissionName->setPosition(Vec2(pCellBg->getContentSize().width/2,pCellBg->getContentSize().height/2));
	auto shadowColor = Color4B::BLACK;   // black
	pMissionName->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
	cell->addChild(pMissionName);

	if (isHasMission(m_nLockedId))
	{
		if (idx == 0)
		{
			const char *missionTitleStr = StringDataManager::getString("task_name_str");
			pMissionName->setString(missionTitleStr);
		}
		else
		{
			pMissionName->setString(curNpcDialog->options(idx-1).titel().c_str());
		}
	}
	else
	{
		pMissionName->setString(curNpcDialog->options(idx).titel().c_str());
	}
	

	return cell;
}


ssize_t NpcTalkWindow::numberOfCellsInTableView(TableView *table)
{
 	if (isHasMission(m_nLockedId))
 	{
		return curNpcDialog->options_size()+1;
 	}
 	else
 	{
 		return curNpcDialog->options_size();
 	}
}

void NpcTalkWindow::tableCellTouched(TableView* table, TableViewCell* cell)
{
	int idx = cell->getIdx();
	int para = 0;

	int selfLevel_ =GameView::getInstance()->myplayer->getActiveRole()->level();
	//if (isHasMission(m_nLockedId))
	//{

	if (isHasMission(m_nLockedId))
	{
		para = idx -1;
	}
	else
	{
		para = idx;
	}

	if(idx == 0 && isHasMission(m_nLockedId))
	{
		auto winSize = Director::getInstance()->getVisibleSize();
		/*bool a = isHasMission(GameView::getInstance()->myplayer->getLockedActor()->getRoleId());*/
		//先关闭这个界面
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
		this->closeAnim();
		//打开与npc交互的任务面板 
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission))
		{
			GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission)->removeFromParent();
		}

		auto scene = GameView::getInstance()->getGameScene();
		auto fn = dynamic_cast<FunctionNPC*>(scene->getActor(m_nLockedId));
		//CNpcDialog * npcDialog = new CNpcDialog();
		//npcDialog->CopyFrom(*curNpcDialog);
		auto talkWithNpc = MissionTalkWithNpc::create(fn);
		GameView::getInstance()->getMainUIScene()->addChild(talkWithNpc,0,kTagTalkWithNpc);
		talkWithNpc->setIgnoreAnchorPointForPosition(false);
		talkWithNpc->setAnchorPoint(Vec2(0,0.5f));
		talkWithNpc->setPosition(Vec2(0,winSize.height/2));	
		//delete npcDialog;
	}
	else
	{
		switch(curNpcDialog->options(para).clientfunction())
		{
		case FUNCTION_INHERIT :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_SHANGDIAN :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				Size winSize=Director::getInstance()->getVisibleSize();
				auto shopLayer = (Layer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShopUI);
				if(shopLayer == NULL)
				{
					shopLayer = ShopUI::create();
					GameView::getInstance()->getMainUIScene()->addChild(shopLayer,0,kTagShopUI);
					shopLayer->setIgnoreAnchorPointForPosition(false);
					shopLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
					shopLayer->setPosition(Vec2(winSize.width/2, winSize.height/2));
				}
				break;
			}
		case FUNCTION_CANGKU :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				Size winSize=Director::getInstance()->getVisibleSize();
				auto storeHouseLayer = (Layer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStoreHouseUI);
				if(storeHouseLayer == NULL)
				{
					storeHouseLayer = StoreHouseUI::create();
					GameView::getInstance()->getMainUIScene()->addChild(storeHouseLayer,0,kTagStoreHouseUI);
					storeHouseLayer->setIgnoreAnchorPointForPosition(false);
					storeHouseLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
					storeHouseLayer->setPosition(Vec2(winSize.width/2, winSize.height/2));
				}
				break;
			}
		case FUNCTION_XIULIZHUANGBEI :
			{
				//GameUtils::playGameSound(REPAIR_TRIGGER, 2, false);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				//GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				//this->closeAnim();
	
				break;
			}
		case FUNCTION_WUJIANGFUHUO :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				//GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				//this->closeAnim();

				break;
			}
		case FUNCTION_JISHOU :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_MY_JISHOU :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				Size winSize=Director::getInstance()->getVisibleSize();

				int openlevel = 0;
				std::map<int,int>::const_iterator cIter;
				cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(13);
				if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[13];
				}

				if (selfLevel_ >= openlevel)
				{
					auto auction =AuctionUi::create();
					auction->setIgnoreAnchorPointForPosition(false);
					auction->setAnchorPoint(Vec2(0.5f,0.5f));
					auction->setPosition(Vec2(winSize.width/2,winSize.height/2));
					GameView::getInstance()->getMainUIScene()->addChild(auction,0,kTagAuction);
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,auction);
				}else
				{
					char openLevelStr[100];
					const char *strings = StringDataManager::getString("auction_OfMoneyOpenLevel");
					sprintf(openLevelStr,strings,openlevel);
					GameView::getInstance()->showAlertDialog(openLevelStr);
				}

				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_CLOSE :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_XIANGQIAN :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_XILIAN :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_FENGYIN :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_TRANSFOR :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_FAMILYCREATE :
			{
				int openlevel = 0;
				std::map<int,int>::const_iterator cIter;
				cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(15);
				if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[15];
				}

				if (selfLevel_ < openlevel)
				{
					char openLevelStr[100];
					const char *strings = StringDataManager::getString("family_createFamilyEnter_level");
					sprintf(openLevelStr,strings,openlevel);
					GameView::getInstance()->showAlertDialog(openLevelStr);
				}else
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				}

				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();

				break;
			}
		case FUNCTION_FAMILYAPPLY :
			{
				int openlevel = 0;
				std::map<int,int>::const_iterator cIter;
				cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(16);
				if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[16];
				}

				if (selfLevel_ >= openlevel)
				{
					auto applyFamily =ApplyFamily::create();
					applyFamily->setIgnoreAnchorPointForPosition(false);
					applyFamily->setAnchorPoint(Vec2(0.5f,0.5f));
					applyFamily->setPosition(Vec2(winsize.width/2,winsize.height/2));
					applyFamily->setTag(kTagApplyFamilyUI);
					GameView::getInstance()->getMainUIScene()->addChild(applyFamily);

					std::vector<CGuildBase *>::iterator iter;
					for (iter=applyFamily->vectorGuildBase.begin();iter!=applyFamily->vectorGuildBase.end();iter++)
					{
						delete * iter;
					}
					applyFamily->vectorGuildBase.clear();
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1501,(void *)0);
				}else
				{
					char openLevelStr[100];
					const char *strings = StringDataManager::getString("family_ApplyFamilyEnter_level");
					sprintf(openLevelStr,strings,openlevel);
					GameView::getInstance()->showAlertDialog(openLevelStr);
				}

				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTION_DUIHUANMA :
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1130, (void *)curNpcDialog->options(para).id());
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
				break;
			}
		case FUNCTTON_SINGCOPY:
			{
				int openlevel = 0;
				std::map<int,int>::const_iterator cIter;
				cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(18);
				if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[18];
				}
				if (selfLevel_ >= openlevel)
				{
					if (this->mTutorialIndex == 1)
					{
						Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
						if(sc != NULL)
							sc->endCommand(this);
					}

					auto challengeui = ChallengeRoundUi::create();
					challengeui->setIgnoreAnchorPointForPosition(false);
					challengeui->setAnchorPoint(Vec2(0.5f,0.5f));
					challengeui->setPosition(Vec2(winsize.width/2,winsize.height/2));
					challengeui->setTag(ktagChallengeRoundUi);
					GameView::getInstance()->getMainUIScene()->addChild(challengeui);
				}else
				{
					char openLevelStr[100];
					const char *strings = StringDataManager::getString("family_ApplyFamilyEnter_level");
					sprintf(openLevelStr,strings,openlevel);
					GameView::getInstance()->showAlertDialog(openLevelStr);
				}

				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				this->closeAnim();
			}break;
		case FUNCTION_SHIJIEBOSS :
			{
				//开启等级
				int openlevel = 0;
				std::map<int,int>::const_iterator cIter;
				cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(19);
				if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[19];
				}

				if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
					this->closeAnim();
					Size winSize=Director::getInstance()->getVisibleSize();
					Layer *worldBossUI = (Layer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagWorldBossUI);
					if(worldBossUI == NULL)
					{
						worldBossUI = WorldBossUI::create();
						GameView::getInstance()->getMainUIScene()->addChild(worldBossUI,0,kTagWorldBossUI);
						worldBossUI->setIgnoreAnchorPointForPosition(false);
						worldBossUI->setAnchorPoint(Vec2(0.5f, 0.5f));
						worldBossUI->setPosition(Vec2(winSize.width/2, winSize.height/2));

						GameMessageProcessor::sharedMsgProcessor()->sendReq(5301);
					}
				}
				else
				{
					std::string str_des = StringDataManager::getString("feature_will_be_open_function");
					char str_level[20];
					sprintf(str_level,"%d",openlevel);
					str_des.append(str_level);
					str_des.append(StringDataManager::getString("feature_will_be_open_open"));
					GameView::getInstance()->showAlertDialog(str_des.c_str());
				}
			}break;
		case FUNCTION_XUANSHANGRENWU:
			{
				//开启等级
				int openlevel = 0;
				std::map<int,int>::const_iterator cIter;
				cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(28);
				if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[28];
				}

				if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
				{
					MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
					RewardTaskData::getInstance()->setTargetNpcId(this->getLockedNpcId());
					//refresh ui
					RewardTaskMainUI * rewardTaskMainUI = (RewardTaskMainUI*)mainscene->getChildByTag(kTagRewardTaskMainUI);
					if (!rewardTaskMainUI)
					{
						rewardTaskMainUI = RewardTaskMainUI::create();
						rewardTaskMainUI->setIgnoreAnchorPointForPosition(false);
						rewardTaskMainUI->setAnchorPoint(Vec2(0.5f,0.5f));
						rewardTaskMainUI->setPosition(Vec2(winsize.width/2,winsize.height/2));
						rewardTaskMainUI->setTag(kTagRewardTaskMainUI);
						mainscene->addChild(rewardTaskMainUI);
						GameMessageProcessor::sharedMsgProcessor()->sendReq(7014);
					}

					GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
					this->closeAnim();
				}
				else
				{
					std::string str_des = StringDataManager::getString("feature_will_be_open_function");
					char str_level[20];
					sprintf(str_level,"%d",openlevel);
					str_des.append(str_level);
					str_des.append(StringDataManager::getString("feature_will_be_open_open"));
					GameView::getInstance()->showAlertDialog(str_des.c_str());
				}
				break;
			}
		}
	}
}

void NpcTalkWindow::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void NpcTalkWindow::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

bool NpcTalkWindow::isHasMission( int npcId )
{
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if (!scene)
		return false;

	FunctionNPC* fn = dynamic_cast<FunctionNPC*>(scene->getActor(npcId));
	if (!fn)
		return false;

	if (fn->npcMissionList.size()>0)
		return true;
	else
		return false;
}

int NpcTalkWindow::getLockedNpcId()
{
	return m_nLockedId;
}

void NpcTalkWindow::addCCTutorialIndicatorNpc( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction,int tutorialIndex )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,275,45,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+171,pos.y+155+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}

	mTutorialIndex = tutorialIndex;
}

void NpcTalkWindow::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void NpcTalkWindow::removeCCTutorialIndicator()
{
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
	mTutorialIndex = -1;
}
