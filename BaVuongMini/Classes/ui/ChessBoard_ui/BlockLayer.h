#ifndef __LAYER_BLOCKLAYER_H__
#define __LAYER_BLOCKLAYER_H__

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

#include <string>

using namespace std;

class ChessBoardLayer;

class BlockLayer :
	public cocos2d::Layer
{
public:
	BlockLayer(void);
	~BlockLayer(void);

public:
	static BlockLayer* create(ChessBoardLayer * pChessBoardLayer);

	virtual bool init(ChessBoardLayer * pChessBoardLayer);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

public:
	void initBlock(int nType, int nBlockInRow, int nBlockInColumn, float cellWidth, float cellHeight, std::string strImageName);			// ��ʼ��Block
	
	bool tryMoveUp(int &nRow, int &nColumn, int nType);															// �Ƿ����� �� ��ƶ�
	bool tryMoveDown(int &nRow, int &nColumn, int nType);															// �Ƿ����� �� ��ƶ�
	bool tryMoveLeft(int &nRow, int &nColumn, int nType);															// �Ƿ����� �� ��ƶ�
	bool tryMoveRight(int &nRow, int &nColumn, int nType);															// �Ƿ����� �� ��ƶ�

	void blockRunMoveAction(int nDirection);																		// block��ָ���ķ����ƶ�
	void blockMove(float xOff, float yOff, int nDirection);
	void blockRunMoveAction(float xOff, float yOff, int nDirection);

	void refreshChessBoard(int nRow, int nColumn, int nDirection, int nBlockType);								
	void refreshChessBoardForSmall(int nRow, int nColumn, int nDirection);
	void refreshChessBoardForVer(int nRow, int nColumn, int nDirection);
	void refreshChessBoardForHor(int nRow, int nColumn, int nDirection);
	void refreshChessBoardForBig(int nRow, int nColumn, int nDirection);

	void refreshBlockState(int nRow, int nColumn);																	// ���blcokState
	void refreshBlockState(int nRow, int nColumn, int nDirection, int nBlockType);

	void refreshBeginTouch(Vec2& pointBegin, Vec2& pointMove);

public:
	typedef enum BlockType
	{
		BLOCK_TYPE_SMALL = 1,
		BLOCK_TYPE_VER,
		BLOCK_TYPE_HOR,
		BLOCK_TYPE_BIG,
		BLOCK_TYPE_NONE
	};

	typedef enum BlockDirection
	{
		DIRECTION_UP = 1,
		DIRECTION_DOWN,
		DIRECTION_LEFT,
		DIRECTION_RIGHT,
		DIRECTION_NONE
	};

private:
	ChessBoardLayer * m_chessBoardLayer;

	int m_blockType;										// blockType
	Sprite * m_pBlockSprite;								// �ͼ�
		
private:
	Vec2 m_point_begin;
	Vec2 m_point_end; 
	Vec2 m_point_move;
	Vec2 m_point_preMove;

	int m_state_direction;						

	int m_BlockInRow;										// ����ĵ����row
	int m_BlockInColumn;									// ����ĵ����column
	
	int m_point_touchRow;
	int m_point_touchColumn;
	
	float m_xPos;
	float m_yPos;

	bool m_bIsWin;

public:
	void set_blockInRow(int nRow);
	void set_blockInColumn(int nColumn);
	void set_blockType(int nType);

	void set_xPos(float xPos);
	float get_xPos();

	void set_yPos(float yPos);
	float get_yPos();

private:
	int get_row(Vec2 point);																			// �ͨ����pointĻ�ȡ���ڵrow
	int get_column(Vec2 point);																		// �ͨ����pointĻ�ȡ���ڵcolumn
	
	bool checkIsInTouch(int nRow, int nColumn, int nBlockType);										// ļ�BeginTouch����Ƿ��block��
	bool checkIsCanMove(int &nBeginRow, int &nBeginColumn, int nDirection,int nBlockType);			// ϼ���Ƿ�����ƶ�
	bool checkIsCanMove(int &nBeginRow, int &nBeginColumn, int nDirection,int nBlockType, int nMoveRow, int nMoveColumn);			// ����Ƿ�����ƶ�

	bool checkPointIsEmpty(int nRow, int nColumn);

	void initTouchMove(Touch *pTouch);																// ��ʼ��touchMove�¼��߼�
	void initTouchMoveForSmall(Touch *pTouch);
	void initTouchMoveForVer(Touch *pTouch);
	void initTouchMoveForHor(Touch *pTouch);
	void initTouchMoveForBig(Touch *pTouch);

	void initTouchEnd(Touch *pTouch);																	// ��ʼ��touchEnd�¼��߼�
	void initTouchEndForSmall(Touch *pTouch);
	void initTouchEndForVer(Touch *pTouch);
	void initTouchEndForHor(Touch *pTouch);
	void initTouchEndForBig(Touch *pTouch);

	bool checkDirectionIsUserful(int nDirection);						// �ж��ƶ������Ƿ���ã���������ʼ����Ϊˮƽ�������TouchEnd�ǰ����ֱ������ƶ���Ч��

	void changeRowToCenter(int &nRow);																	// ��row���õ����ĵ
	void changeColumnToCenter(int &nColumn);															// 㽫column���õ����ĵ

	bool checkIsWin();																					// ��ж��Ƿ�ʤ�
	void BigBlockRunWinnerAction();

	void setNegativeDirection(int nPositiveDirection);												// �ͨ����������÷����

	int checkEndMoveDirection(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);

	void refreshEndMoveState(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void refreshEndMoveStateForSmall(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void refreshEndMoveStateForVer(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void refreshEndMoveStateForHor(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void refreshEndMoveStateForBig(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);

	void refreshStepMove(int nStepMove);
	void calculateStepMove(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void calculateStepMoveForSmall(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void calculateStepMoveForVer(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void calculateStepMoveForHor(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);
	void calculateStepMoveForBig(int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn);

	void calculateSpentTimeForHor(Touch *pTouch);
	void calculateSpentTimeForVer(Touch *pTouch);

	std::string getImagePath(std::string strImageName);												// ��ȡXML imageName
	
private:
	void callBackWinnerActionFinish( Ref * obj );

private:
	float m_xBeginPos;
	float m_yBeginPos;

	float m_xMinPos;
	float m_xMaxPos;
	float m_yMinPos;
	float m_yMaxPos;

	bool bIsMoveing;

	int m_direction_positive;
	int m_direcion_negative;

	long double m_dStartTime;
	long double m_dEndedTime;

	float m_time_runAction;
	float m_velocity;
};

#endif // __LAYER_CHESSBOARDLAYER_H__