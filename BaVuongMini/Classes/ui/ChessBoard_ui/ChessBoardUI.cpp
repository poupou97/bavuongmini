#include "ChessBoardUI.h"

#include "SimpleAudioEngine.h"
#include "GameAudio.h"

#include "../../loadscene_state/LoadSceneState.h"
#include "../../utils/GameUtils.h"
#include "GameView.h"
#include "ChessBoardLayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "XMLOperate.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "cocostudio\CCSGUIReader.h"

using namespace CocosDenshion;

#define TAG_CHESSBOARDlAYER 100
#define TAG_PARTICLE_FINISH 200
#define TAG_SPRITE_EXIT 300
#define TAG_TEACH_ANM 400
#define TAG_TEACH_HAND_ANM 401

#define COCOSTUDIO_PANEL_WIDTH 587
#define COCOSTUDIO_PANEL_HEIGHT 478



Layout * ChessBoardUI::s_pPanel = NULL;

ChessBoardUI::ChessBoardUI()
	:m_layer_chessBoardLayer(NULL)
	,m_imageView_frame(NULL)
	,m_base_layer(NULL)
	,m_labelBMF_stepValue(NULL)
	,m_pBtnFinish_enable(NULL)
	,m_pBtnFinish_disable(NULL)
	,m_pBtnSkip(NULL)
	,m_pBtnReset_enable(NULL)
	,m_pBtnReset_disable(NULL)
	,m_xOff_chessBoard(0.0f)
	,m_yOff_chessBoard(0.0f)
	,m_nDifficulty(1)
	,m_particle_finish(NULL)
	,m_sprite_arrowExit(NULL)
{

}

ChessBoardUI::~ChessBoardUI()
{
	XMLOperate::clearPassNum();
}

ChessBoardUI * ChessBoardUI::create()
{
	auto pChessBoardUI = new ChessBoardUI();
	if (pChessBoardUI && pChessBoardUI->init())
	{
		pChessBoardUI->autorelease();
		return pChessBoardUI;
	}
	CC_SAFE_DELETE(pChessBoardUI);
	return NULL;
}

bool ChessBoardUI::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		m_base_layer = Layer::create();
		m_base_layer->setIgnoreAnchorPointForPosition(false);
		m_base_layer->setAnchorPoint(Vec2(0, 0));
		m_base_layer->setContentSize(Size(800, 480));
		m_base_layer->setPosition(Vec2(0, 0));
		this->addChild(m_base_layer);	

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winSize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		// ���ü����أ�������ǰ���
		s_pPanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/huarongdao_1.json");
		s_pPanel->setAnchorPoint(Vec2(0, 0));
		s_pPanel->getVirtualRenderer()->setContentSize(Size(COCOSTUDIO_PANEL_WIDTH, COCOSTUDIO_PANEL_HEIGHT));
		s_pPanel->setPosition(Vec2((winSize.width - COCOSTUDIO_PANEL_WIDTH) / 2, (winSize.height - COCOSTUDIO_PANEL_HEIGHT) / 2));
		s_pPanel->setScale(1.0f);
		s_pPanel->setTouchEnabled(true);
		m_pLayer->addChild(s_pPanel);

		initUI();

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winSize);

		return true;
	}

	return false;
}

void ChessBoardUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void ChessBoardUI::onExit()
{
	UIScene::onExit();

	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

bool ChessBoardUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void ChessBoardUI::initUI()
{
	auto pBtnClose = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnabled(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addTouchEventListener(CC_CALLBACK_2(ChessBoardUI::callBackBtnClolse, this));

	m_pBtnReset_enable = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_reset");
	m_pBtnReset_enable->setTouchEnabled(true);
	m_pBtnReset_enable->setVisible(true);
	m_pBtnReset_enable->setPressedActionEnabled(true);
	m_pBtnReset_enable->addTouchEventListener(CC_CALLBACK_2(ChessBoardUI::callBackBtnRest, this));

	m_pBtnReset_disable = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_reset_disable");
	m_pBtnReset_disable->setTouchEnabled(false);
	m_pBtnReset_disable->setVisible(false);
	m_pBtnReset_disable->setPressedActionEnabled(false);

	m_pBtnFinish_enable = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_finish");
	m_pBtnFinish_enable->setTouchEnabled(true);
	m_pBtnFinish_enable->setVisible(false);
	m_pBtnFinish_enable->setPressedActionEnabled(true);
	m_pBtnFinish_enable->addTouchEventListener(CC_CALLBACK_2(ChessBoardUI::callBackBtnFinish, this));

	m_pBtnFinish_disable = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_finish_disable");
	m_pBtnFinish_disable->setTouchEnabled(false);
	m_pBtnFinish_disable->setVisible(true);
	m_pBtnFinish_disable->setPressedActionEnabled(false);

	/*m_pBtnSkip = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_skip");
	m_pBtnSkip->setTouchEnabled(true);
	m_pBtnSkip->setPressedActionEnabled(true);
	m_pBtnSkip->addTouchEventListener(CC_CALLBACK_2(ChessBoardUI::callBackBtnSkip));*/

	auto pLabel_info = (Text *)Helper::seekWidgetByName(s_pPanel,"Label_info");
	pLabel_info->setTextAreaSize(Size(134,0));

	//finish button action
	auto action1 = ScaleTo::create(1.0f,1.1f);
	auto action2 = ScaleTo::create(1.0f,0.9);

	auto pAcitionSeq = Sequence::create(action1,action2,NULL);
	m_pBtnFinish_enable->runAction(RepeatForever::create(pAcitionSeq));

	m_imageView_frame = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_frame");
	// x     m_imageView_frame->getPosition().x + s_pPanel->getPosition().x + 9.0f
	// y	 m_imageView_frame->getPosition().y + s_pPanel->getPosition().y + 5.5f

	// �����ȥ��ù��
	//m_pBtnSkip->setVisible(false);

	initLocationFrame();

	// stepValueܣ����
	m_labelBMF_stepValue = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_moveValue");

	// ��ѧ������ӣ����ж��Ƿ���Ҫ�ӣ�
	std::string strPlayerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name(); 
	std::string strKey = strPlayerName;
	strKey.append("ChessBoard_needTeach");

	bool bIsNeedTeachAnm = UserDefault::getInstance()->getBoolForKey(strKey.c_str(), true);
	if (bIsNeedTeachAnm)
	{
		// ��ͷ
		std::string animFileName = "animation/texiao/jiemiantexiao/xunlu/xunlu.anm";
		auto m_pAnim = CCLegendAnimation::create(animFileName);
		m_pAnim->setReleaseWhenStop(true);
		m_pAnim->setPlayLoop(true);
		m_pAnim->setPosition(Vec2(m_imageView_frame->getPosition().x + s_pPanel->getPosition().x + 9.0f + 250.0f, 
			m_imageView_frame->getPosition().y + s_pPanel->getPosition().y + 5.5f + 85.0f));
		m_pAnim->setTag(TAG_TEACH_ANM);
		this->addChild(m_pAnim);

		// С�
		auto sprite_hand = Sprite::create("animation/texiao/renwutexiao/shou/shou.png");
		sprite_hand->setAnchorPoint(Vec2(0, 1));
		sprite_hand->setIgnoreAnchorPointForPosition(false);
		sprite_hand->setPosition(Vec2(m_imageView_frame->getPosition().x + s_pPanel->getPosition().x + 9.0f + 185.0f,
			m_imageView_frame->getPosition().y + s_pPanel->getPosition().y + 5.5f + 90.0f));
		sprite_hand->setTag(TAG_TEACH_HAND_ANM);

		auto action =(ActionInterval *)Sequence::create(
			DelayTime::create(0.5f),
			MoveTo::create(0.0f, Vec2(m_imageView_frame->getPosition().x + s_pPanel->getPosition().x + 9.0f + 185.0f,
			m_imageView_frame->getPosition().y + s_pPanel->getPosition().y + 5.5f + 90.0f)),
			MoveBy::create(1.0f, Vec2(100, 0)),
			DelayTime::create(0.5f),
			NULL);

		auto pActionRepeat = RepeatForever::create(action);
		sprite_hand->runAction(pActionRepeat);

		this->addChild(sprite_hand);
	}

	this->scheduleUpdate();
}

void ChessBoardUI::callBackBtnClolse(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void ChessBoardUI::callBackBtnRest(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto pChessBoardLayer = (ChessBoardLayer *)m_base_layer->getChildByTag(TAG_CHESSBOARDlAYER);
		if (NULL != pChessBoardLayer)
		{
			m_base_layer->removeChildByTag(TAG_CHESSBOARDlAYER);
		}

		// ����³�ʼ�����
		initChessBoard(m_xOff_chessBoard, m_yOff_chessBoard);

		// ̳�ʼ�� ��ɰ�ť��״̬
		m_pBtnFinish_enable->setVisible(false);
		m_pBtnFinish_disable->setVisible(true);

		// �Ƴ���ɡ���ť�����Ч�
		this->removeFinishParticle();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void ChessBoardUI::callBackBtnFinish(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(7013, (void *)this->get_difficulty(), (void *)(com::future::threekingdoms::server::transport::protocol::HUA_RONG));

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void ChessBoardUI::callBackBtnSkip( Ref* obj )
{
	
}

void ChessBoardUI::initChessBoard( float xPos, float yPos )
{
	m_layer_chessBoardLayer = ChessBoardLayer::create();
	m_layer_chessBoardLayer->setIgnoreAnchorPointForPosition(false);
	m_layer_chessBoardLayer->setAnchorPoint(Vec2(0, 0));
	m_layer_chessBoardLayer->setPosition(Vec2(xPos, yPos));
	m_layer_chessBoardLayer->setTag(TAG_CHESSBOARDlAYER);
	m_layer_chessBoardLayer->set_difficulty(this->get_difficulty());


	float width = m_imageView_frame->getContentSize().width;
	float height = m_imageView_frame->getContentSize().height;

	// �˴��widthReal ĺ heightReal �� �ͼƬ��ʵ�ʴ�С��19.0f � 13.0f��Ǹ�ݼ���ó
	float widthReal = width - 19.0f;
	float heigthReal = height - 13.0f;

	m_layer_chessBoardLayer->set_xPos(xPos);
	m_layer_chessBoardLayer->set_yPos(yPos);
	m_layer_chessBoardLayer->initChessBoardUI(xPos, yPos, widthReal, heigthReal);

	m_base_layer->addChild(m_layer_chessBoardLayer);

	// ��ʼ�����ͼƬ
	initExitSprite();
}

void ChessBoardUI::update( float dt )
{
	int nStepMoveValue = m_layer_chessBoardLayer->get_stepMove();
	char charStep[20];
	sprintf(charStep, "%d", nStepMoveValue);

	m_labelBMF_stepValue->setString(charStep);

	// �ж��Ƿ�ȥ���ѧ����������еĻ�
	auto pNode_tecah = this->getChildByTag(TAG_TEACH_ANM);
	if (NULL != pNode_tecah && nStepMoveValue != 0)
	{
		pNode_tecah->removeFromParent();

		auto pNode_tecahHand = this->getChildByTag(TAG_TEACH_HAND_ANM);
		if (NULL != pNode_tecahHand)
		{
			pNode_tecahHand->removeFromParent();
		}

		std::string strPlayerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name(); 
		std::string strKey = strPlayerName;
		strKey.append("ChessBoard_needTeach");

		UserDefault::getInstance()->setBoolForKey(strKey.c_str(), false);
	}
}

void ChessBoardUI::setFinishBtnEnable()
{
	m_pBtnFinish_enable->setVisible(true);

	m_pBtnFinish_disable->setVisible(false);
}

void ChessBoardUI::setResetBtnDisable()
{
	m_pBtnReset_disable->setVisible(true);

	m_pBtnReset_enable->setVisible(false);
}


void ChessBoardUI::initLocationFrame()
{
	/*if (!m_pBtnSkip->isVisible())
	{
	m_pBtnReset_enable->setPosition(Vec2(m_pBtnReset_enable->getPosition().x, m_pBtnReset_enable->getPosition().y + 20));
	m_pBtnReset_disable->setPosition(Vec2(m_pBtnReset_disable->getPosition().x, m_pBtnReset_disable->getPosition().y + 20));

	m_pBtnFinish_enable->setPosition(Vec2(m_pBtnFinish_enable->getPosition().x, m_pBtnFinish_enable->getPosition().y + 10));
	m_pBtnFinish_disable->setPosition(Vec2(m_pBtnFinish_disable->getPosition().x, m_pBtnFinish_disable->getPosition().y + 10));
	}*/
}

void ChessBoardUI::set_difficulty( int nDifficulty )
{
	this->m_nDifficulty = nDifficulty;
}

int ChessBoardUI::get_difficulty()
{
	return this->m_nDifficulty;
}

void ChessBoardUI::initFinishParticle()
{
	auto pFinishParticle = (CCTutorialParticle*)this->getChildByTag(TAG_PARTICLE_FINISH);
	if (NULL == pFinishParticle)
	{
		float xPos = m_pBtnFinish_enable->getPosition().x + s_pPanel->getPosition().x;
		float yPos = m_pBtnFinish_enable->getPosition().y + s_pPanel->getPosition().y - 20;

		m_particle_finish = CCTutorialParticle::create("tuowei0.plist", 90, m_pBtnFinish_enable->getContentSize().height);
		m_particle_finish->setPosition(Vec2(xPos, yPos));
		m_particle_finish->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_particle_finish->setTag(TAG_PARTICLE_FINISH);

		m_base_layer->addChild(m_particle_finish);
	}
}

void ChessBoardUI::removeFinishParticle()
{
	auto pFinishParticle = (CCTutorialParticle*)m_base_layer->getChildByTag(TAG_PARTICLE_FINISH);
	if (NULL != pFinishParticle)
	{
		m_base_layer->removeChild(m_particle_finish);
	}
}

void ChessBoardUI::initExitSprite()
{
	auto tmpSprite = (Sprite*)m_base_layer->getChildByTag(TAG_SPRITE_EXIT);
	if (NULL == tmpSprite)
	{
		m_sprite_arrowExit = Sprite::create("res_ui/jiantou_b.png");
		m_sprite_arrowExit->setAnchorPoint(Vec2(0, 0));
		m_sprite_arrowExit->setIgnoreAnchorPointForPosition(false);
		m_sprite_arrowExit->setTag(TAG_SPRITE_EXIT);

		float xPos = m_xOff_chessBoard;
		float yPos = m_yOff_chessBoard;

		float width = m_imageView_frame->getContentSize().width;
		float height = m_imageView_frame->getContentSize().height;

		// �˴��widthReal ĺ heightReal �� �ͼƬ��ʵ�ʴ�С��19.0f � 13.0f��Ǹ�ݼ���ó
		float widthReal = width - 19.0f;
		float heigthReal = height - 13.0f;

		float xFinalPos = xPos + (widthReal - m_sprite_arrowExit->getContentSize().width) / 2;
		//float yFinalPos = yPos - m_sprite_arrowExit->getContentSize().height - 5;
		float yFinalPos = yPos - m_sprite_arrowExit->getContentSize().height - 1;

		m_sprite_arrowExit->setPosition(Vec2(xFinalPos, yFinalPos));

		m_base_layer->addChild(m_sprite_arrowExit);

		// ��ʼ�� ���ͼƬ�Ķ���Ч�
		//initExitSpriteAction();
	}
}

void ChessBoardUI::initExitSpriteAction()
{
	// ����¶�� �Ч�
	auto pActionMoveByDown = MoveBy::create(1.0f, Vec2(0, 8));
	auto pActionMoveByUp = MoveBy::create(1.0f, Vec2(0, -8)); 
	auto pActionExpoIn = EaseSineInOut::create(pActionMoveByDown);
	auto pActionExpoOut = EaseSineInOut::create(pActionMoveByUp);
	auto pActionSequence = Sequence::createWithTwoActions(pActionExpoIn, pActionExpoOut);
	auto pActionRepeat = RepeatForever::create(pActionSequence);
	m_sprite_arrowExit->runAction(pActionRepeat);
}

void ChessBoardUI::removeExitSprite()
{
	m_sprite_arrowExit->removeFromParent();
}

void ChessBoardUI::initChessBoardData()
{
	// �������̵�ƫ�����8.5f � 5.5f �� Ǹ�cocostudioݵUIĳ��������ģ�
	m_xOff_chessBoard = m_imageView_frame->getPosition().x + s_pPanel->getPosition().x + 9.0f;
	m_yOff_chessBoard = m_imageView_frame->getPosition().y + s_pPanel->getPosition().y + 5.5f;

	initChessBoard(m_xOff_chessBoard, m_yOff_chessBoard);
}





