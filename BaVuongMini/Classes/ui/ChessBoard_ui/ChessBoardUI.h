#ifndef _UI_CHESSBOARD_CHESSBOARDUI_H_
#define _UI_CHESSBOARD_CHESSBOARDUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ���ݵ ���
 * @author liuliang
 * @version 0.1.0
 * @date 2014.07.11
 */

class ChessBoardLayer;
class CCTutorialParticle;

class ChessBoardUI : public UIScene
{
public:
	ChessBoardUI();
	virtual ~ChessBoardUI();

	static Layout * s_pPanel;

	static ChessBoardUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void update(float dt);

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);

public:
	void callBackBtnClolse(Ref *pSender, Widget::TouchEventType type);																	// �رհ�ť����Ӧ
	void callBackBtnRest(Ref *pSender, Widget::TouchEventType type);																	// ��� ð�ť����Ӧ
	void callBackBtnFinish(Ref *pSender, Widget::TouchEventType type);																	// �� ɰ�ť����Ӧ
	void callBackBtnSkip(Ref* obj);

	void setFinishBtnEnable();																				// ��� ��� ɰ�ť Ϊ���
	void setResetBtnDisable();																				// ���� ���� ð�ť Ϊ�����
	
	void initFinishParticle();																				// ó�ʼ�� �� ɰ�ť����Ч
	void removeFinishParticle();																			// �Ƴ ��� ɰ�ť����Ч
	
	void initExitSpriteAction();																			// ��ʼ�� ���ͼƬ� Ķ���Ч�
	void removeExitSprite();																				// ��Ƴ ���ͼƬ��������Ч�
	
	void initChessBoardData();																				// ����ӿڣ����ڳ�ʼ�����̣����ô˽ӿ�ǰ����ص�� set_difficulty ýӿڣ�

private:
	void initUI();
	void initChessBoard(float xPos, float yPos);
	void initLocationFrame();																				// �˷������ ڵ� ���ť������ʱ�����²��
	void initExitSprite();																					// ֳ�ʼ�� ���ͼƬ


private:
	Layer * m_base_layer;																							// ��layer

	ChessBoardLayer * m_layer_chessBoardLayer;
	ImageView * m_imageView_frame;

	Button* m_pBtnFinish_enable;
	Button* m_pBtnFinish_disable;
	Button* m_pBtnSkip;
	Button* m_pBtnReset_enable;
	Button* m_pBtnReset_disable;

	Text * m_labelBMF_stepValue;

	CCTutorialParticle * m_particle_finish;																//  ���ɰ�ť ��Ч
	Sprite * m_sprite_arrowExit;

	float m_xOff_chessBoard;
	float m_yOff_chessBoard;

	int m_nDifficulty;																						// ���׳̶ȣ�1���򵥣�2���еȣ�3����ѣ�
public:
	void set_difficulty(int nDifficulty);
	int get_difficulty();
};

#endif