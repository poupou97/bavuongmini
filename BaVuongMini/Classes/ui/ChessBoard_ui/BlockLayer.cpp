#include "BlockLayer.h"
#include "ChessBoardLayer.h"
#include "GameView.h"
#include "ChessBoardUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainAnimationScene.h"

USING_NS_CC;

#define CHESSBOARD_ROW 5
#define CHESSBOARD_COLUMN 4

BlockLayer::BlockLayer( void )
	:m_blockType(BLOCK_TYPE_NONE)
	,m_pBlockSprite(NULL)
	,m_chessBoardLayer(NULL)
	,m_BlockInRow(0)
	,m_BlockInColumn(0)
	,m_point_touchRow(0)
	,m_point_touchColumn(0)
	,bIsMoveing(false)
	,m_direction_positive(DIRECTION_NONE)
	,m_direcion_negative(DIRECTION_NONE)
	,m_time_runAction(0.0f)
	,m_velocity(0.0f)
	,m_bIsWin(false)
{

}

BlockLayer::~BlockLayer( void )
{

}

BlockLayer* BlockLayer::create(ChessBoardLayer * pChessBoardLayer)
{
	auto pLoadLayer = new BlockLayer();
	if (pLoadLayer && pLoadLayer->init(pChessBoardLayer))
	{
		pLoadLayer->autorelease();
		return pLoadLayer;
	}
	CC_SAFE_DELETE(pLoadLayer);
	return NULL;
}

bool BlockLayer::init(ChessBoardLayer * pChessBoardLayer)
{
	bool bRet = false;
	do 
	{
		CC_BREAK_IF(!Layer::init());

		// "���"
		m_chessBoardLayer = pChessBoardLayer;

		this->set_xPos(m_chessBoardLayer->get_xPos());
		this->set_yPos(m_chessBoardLayer->get_yPos());

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		bRet = true;

	} while (0);

	return bRet;
}

void BlockLayer::onEnter()
{
	Layer::onEnter();
}

void BlockLayer::onExit()
{
	Layer::onExit();
}

bool BlockLayer::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	if (m_chessBoardLayer->get_isWin())
	{
		return false;
	}

	Vec2 position = pTouch->getLocationInView();
	Vec2 tmpPos = Director::getInstance()->convertToGL(position);

	m_point_begin = Vec2(tmpPos.x - this->get_xPos(), tmpPos.y - this->get_yPos());


	if (m_point_begin.x < 0 || m_point_begin.y < 0)
	{
		return false;
	}

	int nColumn = get_column(m_point_begin);
	int nRow = get_row(m_point_begin);

	if (checkIsInTouch(nRow, nColumn, this->m_blockType))
	{
		struct timeval m_sStartTime;
		gettimeofday(&m_sStartTime, NULL);
		m_dStartTime = m_sStartTime.tv_sec*1000 + m_sStartTime.tv_usec/1000;

		return true;
	}
	else
	{
		return false;
	}

}

void BlockLayer::onTouchMoved( Touch *pTouch, Event *pEvent )
{
	initTouchMove(pTouch);
}

void BlockLayer::onTouchEnded( Touch *pTouch, Event *pEvent )
{
	initTouchEnd(pTouch);
}

void BlockLayer::onTouchCancelled( Touch *pTouch, Event *pEvent )
{
	
}

void BlockLayer::initBlock( int nType, int nBlockInRow, int nBlockInColumn, float cellWidth, float cellHeight, std::string strImageName )
{
	if (BLOCK_TYPE_SMALL == nType)
	{
		m_pBlockSprite = Sprite::create("res_ui/huarongdao/zu.png");
	}
	else if (BLOCK_TYPE_HOR == nType || BLOCK_TYPE_VER == nType)
	{
		std::string tmpImageName = this->getImagePath(strImageName);
		if ("" != tmpImageName)
		{
			m_pBlockSprite = Sprite::create(tmpImageName.c_str());
		}
	}
	else if (BLOCK_TYPE_BIG == nType)
	{
		m_pBlockSprite = Sprite::create("res_ui/huarongdao/caocao.png");
	}

	if (NULL != m_pBlockSprite)
	{
		m_pBlockSprite->setIgnoreAnchorPointForPosition(false);
		m_pBlockSprite->setAnchorPoint(Vec2(0, 0));
		m_pBlockSprite->setPosition(Vec2(nBlockInColumn * cellWidth, nBlockInRow * cellHeight));

		this->set_blockType(nType);

		this->set_blockInRow(nBlockInRow);
		this->set_blockInColumn(nBlockInColumn);

		this->addChild(m_pBlockSprite);

		this->setContentSize(this->m_pBlockSprite->getContentSize());
	}
}

int BlockLayer::get_row( Vec2 point )
{
	float x_pos = point.x;
	float y_pos = point.y;

	float cellWidth = m_chessBoardLayer->getCellWidth();
	float cellHeight = m_chessBoardLayer->getCellHeight();

	int nTouchColumn = x_pos / cellWidth;
	int nTouchRow = y_pos / cellHeight;

	return nTouchRow;
}

int BlockLayer::get_column( Vec2 point )
{
	float x_pos = point.x;
	float y_pos = point.y;

	float cellWidth = m_chessBoardLayer->getCellWidth();
	float cellHeight = m_chessBoardLayer->getCellHeight();

	int nTouchColumn = x_pos / cellWidth;
	int nTouchRow = y_pos / cellHeight;

	return nTouchColumn;
}

void BlockLayer::set_blockInRow( int nRow )
{
	this->m_BlockInRow = nRow;
}

void BlockLayer::set_blockInColumn( int nColumn )
{
	this->m_BlockInColumn = nColumn;
}

bool BlockLayer::checkIsInTouch( int nRow, int nColumn, int nBlockType )
{
	bool bIsInTouch = false;

	if (BLOCK_TYPE_SMALL == nBlockType)
	{
		if (nRow == this->m_BlockInRow && nColumn == this->m_BlockInColumn)
		{
			bIsInTouch = true;
		}
	}
	else if (BLOCK_TYPE_VER == nBlockType)
	{
		if (nRow >= this->m_BlockInRow)
		{
			if ((nRow == this->m_BlockInRow || nRow == this->m_BlockInRow + 1) && nColumn == this->m_BlockInColumn)
			{
				bIsInTouch = true;
			}
		}
	}
	else if (BLOCK_TYPE_HOR == nBlockType)
	{
		if (nColumn >= this->m_BlockInColumn)
		{
			if ((nColumn == this->m_BlockInColumn || nColumn == this->m_BlockInColumn + 1) && nRow == this->m_BlockInRow)
			{
				bIsInTouch = true;
			}
		}
	}
	else if (BLOCK_TYPE_BIG == nBlockType)
	{
		if (nColumn >= this->m_BlockInColumn && nRow >= this->m_BlockInRow)
		{
			if ((nColumn == this->m_BlockInColumn || nColumn == this->m_BlockInColumn + 1) && (nRow == this->m_BlockInRow || nRow == this->m_BlockInRow + 1))
			{
				bIsInTouch = true;
			}
		}
	}

	return bIsInTouch;
}

void BlockLayer::set_blockType( int nType )
{
	this->m_blockType = nType;
}

bool BlockLayer::checkIsCanMove( int &nBeginRow, int &nBeginColumn, int nDirection, int nBlockType )
{
	bool bIsCanMove = false;

	if (DIRECTION_UP == nDirection)
	{
		bIsCanMove = tryMoveUp(nBeginRow, nBeginColumn, nBlockType);
	}
	else if (DIRECTION_DOWN == nDirection)
	{
		bIsCanMove = tryMoveDown(nBeginRow, nBeginColumn, nBlockType);
	}
	else if (DIRECTION_LEFT == nDirection)
	{
		bIsCanMove = tryMoveLeft(nBeginRow, nBeginColumn, nBlockType);
	}
	else if (DIRECTION_RIGHT == nDirection)
	{
		bIsCanMove = tryMoveRight(nBeginRow, nBeginColumn, nBlockType);
	}

	return bIsCanMove;
}

bool BlockLayer::checkIsCanMove( int &nBeginRow, int &nBeginColumn, int nDirection,int nBlockType, int nMoveRow, int nMoveColumn )
{
	bool bIsCanMove = false;

	if (DIRECTION_UP == nDirection)
	{
		bIsCanMove = tryMoveUp(nBeginRow, nBeginColumn, nBlockType);
	}
	else if (DIRECTION_DOWN == nDirection)
	{
		bIsCanMove = tryMoveDown(nBeginRow, nBeginColumn, nBlockType);
	}
	else if (DIRECTION_LEFT == nDirection)
	{
		bIsCanMove = tryMoveLeft(nBeginRow, nBeginColumn, nBlockType);
	}
	else if (DIRECTION_RIGHT == nDirection)
	{
		bIsCanMove = tryMoveRight(nBeginRow, nBeginColumn, nBlockType);
	}

	// ��жmoveTouch�����λ��״̬�Ƿ�Ϊ0
	if (bIsCanMove)
	{
		if (0 == m_chessBoardLayer->m_array[nMoveRow][nMoveColumn])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}

	return bIsCanMove;
}

bool BlockLayer::checkPointIsEmpty( int nRow, int nColumn )
{
	bool bPointIsEmpty = false;

	// ��ǰ�����Ƿ�Ϊ�
	if (0 == m_chessBoardLayer->m_array[nRow][nColumn])
	{
		bPointIsEmpty = true;
	}

	return bPointIsEmpty;
}

bool BlockLayer::tryMoveUp( int &nRow, int &nColumn, int nType )
{
	bool bIsCanMove = false;

	if (BLOCK_TYPE_SMALL == nType)
	{
		if (CHESSBOARD_ROW == nRow + 1)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow + 1][nColumn])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}
	else if (BLOCK_TYPE_VER == nType)
	{
		changeRowToCenter(nRow);

		if (CHESSBOARD_ROW == nRow + 2)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow + 2][nColumn])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}
	else if (BLOCK_TYPE_HOR == nType)
	{
		changeColumnToCenter(nColumn);

		if (CHESSBOARD_ROW == nRow + 1)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow + 1][nColumn] && 0 == m_chessBoardLayer->m_array[nRow + 1][nColumn + 1])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}
	else if (BLOCK_TYPE_BIG == nType)
	{
		changeRowToCenter(nRow);
		changeColumnToCenter(nColumn);

		if (CHESSBOARD_ROW == nRow + 2)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow + 2][nColumn] && 0 == m_chessBoardLayer->m_array[nRow + 2][nColumn + 1])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}

	return bIsCanMove;
}

bool BlockLayer::tryMoveDown( int &nRow, int &nColumn, int nType )
{
	bool bIsCanMove = false;

	if (BLOCK_TYPE_SMALL == nType)
	{
		// ������ж��Ƿ�Խ�
		if (0 == nRow)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow - 1][nColumn])
		{
			bIsCanMove = true;
		}
	}
	else if (BLOCK_TYPE_VER == nType)
	{
		changeRowToCenter(nRow);

		if (0 == nRow)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow - 1][nColumn])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}
	else if (BLOCK_TYPE_HOR == nType)
	{
		changeColumnToCenter(nColumn);

		if (0 == nRow)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow - 1][nColumn] && 0 == m_chessBoardLayer->m_array[nRow - 1][nColumn + 1])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}
	else if (BLOCK_TYPE_BIG == nType)
	{
		changeRowToCenter(nRow);
		changeColumnToCenter(nColumn);

		if (0 == nRow)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow - 1][nColumn] && 0 == m_chessBoardLayer->m_array[nRow - 1][nColumn + 1])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}

	return bIsCanMove;
}

bool BlockLayer::tryMoveLeft( int &nRow, int &nColumn, int nType )
{
	bool bIsCanMove = false;

	if (BLOCK_TYPE_SMALL == nType)
	{
		if (0 == nColumn)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow][nColumn - 1])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}
	else if (BLOCK_TYPE_VER == nType)
	{
		changeRowToCenter(nRow);

		if (0 == nColumn)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow][nColumn - 1] && 0 == m_chessBoardLayer->m_array[nRow + 1][nColumn - 1])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}
	else if (BLOCK_TYPE_HOR == nType)
	{
		changeColumnToCenter(nColumn);

		if (0 == nColumn)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow][nColumn - 1])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}
	else if (BLOCK_TYPE_BIG == nType)
	{
		changeRowToCenter(nRow);
		changeColumnToCenter(nColumn);

		if (0 == nColumn)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow][nColumn - 1] && 0 == m_chessBoardLayer->m_array[nRow + 1][nColumn - 1])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}

	return bIsCanMove;
}

bool BlockLayer::tryMoveRight( int &nRow, int &nColumn, int nType )
{
	bool bIsCanMove = false;

	if (BLOCK_TYPE_SMALL == nType)
	{
		if (CHESSBOARD_COLUMN == nColumn + 1)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow][nColumn + 1])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}
	else if (BLOCK_TYPE_VER == nType)
	{
		changeRowToCenter(nRow);

		if (CHESSBOARD_COLUMN == nColumn + 1)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow][nColumn + 1] && 0 == m_chessBoardLayer->m_array[nRow + 1][nColumn + 1])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}
	else if (BLOCK_TYPE_HOR == nType)
	{
		changeColumnToCenter(nColumn);

		if (CHESSBOARD_COLUMN == nColumn + 2)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow][nColumn + 2])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}
	else if (BLOCK_TYPE_BIG == nType)
	{
		changeRowToCenter(nRow);
		changeColumnToCenter(nColumn);

		if (CHESSBOARD_COLUMN == nColumn + 2)
		{
			bIsCanMove = false;
		}
		else if (0 == m_chessBoardLayer->m_array[nRow][nColumn + 2] && 0 == m_chessBoardLayer->m_array[nRow + 1][nColumn + 2])
		{
			bIsCanMove = true;
		}
		else
		{
			bIsCanMove = false;
		}
	}

	return bIsCanMove;
}

void BlockLayer::blockRunMoveAction( int nDirection )
{
	if (NULL != m_pBlockSprite)
	{
		if (DIRECTION_UP == nDirection)
		{
			auto action = MoveBy::create(0.1f, Vec2(0, m_chessBoardLayer->getCellHeight()));
			m_pBlockSprite->runAction(action);
		}
		else if (DIRECTION_DOWN == nDirection)
		{
			auto action = MoveBy::create(0.1f, Vec2(0, 0 - m_chessBoardLayer->getCellHeight()));
			m_pBlockSprite->runAction(action);
		}
		else if (DIRECTION_LEFT == nDirection)
		{
			auto action = MoveBy::create(0.1f, Vec2(0 - m_chessBoardLayer->getCellWidth(), 0));
			m_pBlockSprite->runAction(action);
		}
		else if (DIRECTION_RIGHT == nDirection)
		{
			auto action = MoveBy::create(0.1f, Vec2(m_chessBoardLayer->getCellWidth(), 0));
			m_pBlockSprite->runAction(action);
		}
	}
}

void BlockLayer::refreshChessBoardForSmall( int nRow, int nColumn, int nDirection )
{
	if (DIRECTION_UP == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow + 1][nColumn] = 1;

		refreshBlockState(nRow + 1, nColumn);
	}
	else if (DIRECTION_DOWN == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow - 1][nColumn] = 1;

		refreshBlockState(nRow - 1, nColumn);
	}
	else if (DIRECTION_RIGHT == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow][nColumn + 1] = 1;

		refreshBlockState(nRow, nColumn + 1);
	}
	else if (DIRECTION_LEFT == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn] =0;
		m_chessBoardLayer->m_array[nRow][nColumn - 1] = 1;

		refreshBlockState(nRow, nColumn - 1);
	}
}

void BlockLayer::refreshBlockState( int nRow, int nColumn )
{
	this->set_blockInRow(nRow);
	this->set_blockInColumn(nColumn);
}

void BlockLayer::refreshBlockState( int nRow, int nColumn, int nDirection, int nBlockType )
{
	if (BLOCK_TYPE_SMALL == nBlockType)
	{
		refreshChessBoardForSmall(nRow, nColumn, nDirection);
	}
	else if (BLOCK_TYPE_HOR == nBlockType)
	{
		refreshChessBoardForHor(nRow, nColumn, nDirection);
	}
	else if (BLOCK_TYPE_VER == nBlockType)
	{
		refreshChessBoardForVer(nRow, nColumn, nDirection);
	}
	else if (BLOCK_TYPE_BIG == nBlockType)
	{
		refreshChessBoardForBig(nRow, nColumn, nDirection);
	}
}

void BlockLayer::refreshChessBoardForVer( int nRow, int nColumn, int nDirection )
{
	if (DIRECTION_UP == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow + 2][nColumn] = 1;

		refreshBlockState(nRow + 1, nColumn);
	}
	else if (DIRECTION_DOWN == nDirection)
	{
		m_chessBoardLayer->m_array[nRow + 1][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow - 1][nColumn] = 1;

		refreshBlockState(nRow - 1, nColumn);
	}
	else if (DIRECTION_RIGHT == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow + 1][nColumn] = 0;

		m_chessBoardLayer->m_array[nRow][nColumn + 1] = 1;
		m_chessBoardLayer->m_array[nRow + 1][nColumn + 1] = 1;

		refreshBlockState(nRow, nColumn + 1);
	}
	else if (DIRECTION_LEFT == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow + 1][nColumn] = 0;

		m_chessBoardLayer->m_array[nRow][nColumn - 1] = 1;
		m_chessBoardLayer->m_array[nRow + 1][nColumn - 1] = 1;

		refreshBlockState(nRow, nColumn - 1);
	}
}

void BlockLayer::refreshChessBoardForHor( int nRow, int nColumn, int nDirection )
{
	if (DIRECTION_UP == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow][nColumn + 1] = 0;

		m_chessBoardLayer->m_array[nRow + 1][nColumn] = 1;
		m_chessBoardLayer->m_array[nRow + 1][nColumn + 1] = 1;

		refreshBlockState(nRow + 1, nColumn);
	}
	else if (DIRECTION_DOWN == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow][nColumn + 1] = 0;

		m_chessBoardLayer->m_array[nRow - 1][nColumn] = 1;
		m_chessBoardLayer->m_array[nRow - 1][nColumn + 1] = 1;

		refreshBlockState(nRow - 1, nColumn);
	}
	else if (DIRECTION_RIGHT == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow][nColumn + 2] = 1;

		refreshBlockState(nRow, nColumn + 1);
	}
	else if (DIRECTION_LEFT == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn - 1] = 1;
		m_chessBoardLayer->m_array[nRow][nColumn + 1] = 0;

		refreshBlockState(nRow, nColumn - 1);
	}
}

void BlockLayer::changeRowToCenter( int &nRow )
{
	// �˵�������ĵ
	if (nRow == this->m_BlockInRow)
	{

	}
	else
	{
		nRow = nRow - 1;
	}
}

void BlockLayer::changeColumnToCenter( int &nColumn )
{
	// �˵�������ĵ
	if (nColumn == this->m_BlockInColumn)
	{

	}
	else
	{
		nColumn = nColumn - 1;
	}
}

void BlockLayer::refreshChessBoardForBig( int nRow, int nColumn, int nDirection )
{
	if (DIRECTION_UP == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow][nColumn + 1] = 0;

		m_chessBoardLayer->m_array[nRow + 2][nColumn] = 1;
		m_chessBoardLayer->m_array[nRow + 2][nColumn + 1] = 1;

		refreshBlockState(nRow + 1, nColumn);
	}
	else if (DIRECTION_DOWN == nDirection)
	{
		m_chessBoardLayer->m_array[nRow + 1][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow + 1][nColumn + 1] = 0;

		m_chessBoardLayer->m_array[nRow - 1][nColumn] = 1;
		m_chessBoardLayer->m_array[nRow - 1][nColumn + 1] = 1;

		refreshBlockState(nRow - 1, nColumn);
	}
	else if (DIRECTION_RIGHT == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn] = 0;
		m_chessBoardLayer->m_array[nRow + 1][nColumn] = 0;

		m_chessBoardLayer->m_array[nRow][nColumn + 2] = 1;
		m_chessBoardLayer->m_array[nRow + 1][nColumn + 2] = 1;

		refreshBlockState(nRow, nColumn + 1);
	}
	else if (DIRECTION_LEFT == nDirection)
	{
		m_chessBoardLayer->m_array[nRow][nColumn + 1] = 0;
		m_chessBoardLayer->m_array[nRow + 1][nColumn + 1] = 0;

		m_chessBoardLayer->m_array[nRow][nColumn - 1] = 1;
		m_chessBoardLayer->m_array[nRow + 1][nColumn - 1] = 1;

		refreshBlockState(nRow, nColumn - 1);
	}
}

bool BlockLayer::checkIsWin()
{
	bool bIsWin = false;

	if (BLOCK_TYPE_BIG == m_blockType && 0 == this->m_BlockInRow && 1 == this->m_BlockInColumn)
	{
		bIsWin = true;
	}
	
	return bIsWin;
}

void BlockLayer::blockMove( float xOff, float yOff, int nDirection)
{
	if (NULL != m_pBlockSprite)
	{
		if (DIRECTION_UP == nDirection)
		{
			m_pBlockSprite->setPositionY(m_pBlockSprite->getPositionY() + yOff);
		}
		else if (DIRECTION_DOWN == nDirection)
		{
			m_pBlockSprite->setPositionY(m_pBlockSprite->getPositionY() + yOff);
		}
		else if (DIRECTION_LEFT == nDirection)
		{
			m_pBlockSprite->setPositionX(m_pBlockSprite->getPositionX() + xOff);
		}
		else if (DIRECTION_RIGHT == nDirection)
		{
			m_pBlockSprite->setPositionX(m_pBlockSprite->getPositionX() + xOff);
		}
	}
}

void BlockLayer::set_xPos( float xPos )
{
	this->m_xPos = xPos;
}

float BlockLayer::get_xPos()
{
	return this->m_xPos;
}

void BlockLayer::set_yPos( float yPos )
{
	this->m_yPos = yPos;
}

float BlockLayer::get_yPos()
{
	return this->m_yPos;
}

void BlockLayer::refreshBeginTouch( Vec2& pointBegin, Vec2& pointMove )
{
	pointBegin = pointMove;
}

void BlockLayer::refreshStepMove( int nStepMove )
{
	int nOldStepMove =  m_chessBoardLayer->get_stepMove();
	int nNewStepMove = nOldStepMove + nStepMove;
	m_chessBoardLayer->set_stepMove(nNewStepMove);
}

void BlockLayer::initTouchMove( Touch *pTouch )
{
	if (BLOCK_TYPE_SMALL == m_blockType)
	{
		initTouchMoveForSmall(pTouch);
	}
	else if (BLOCK_TYPE_VER == m_blockType)
	{
		initTouchMoveForVer(pTouch);
	}
	else if (BLOCK_TYPE_HOR == m_blockType)
	{
		initTouchMoveForHor(pTouch);
	}
	else if (BLOCK_TYPE_BIG == m_blockType)
	{
		initTouchMoveForBig(pTouch);
	}
}

void BlockLayer::initTouchMoveForSmall( Touch *pTouch )
{
	Vec2 postionMove = pTouch->getLocationInView();
	Vec2 tmpPosMove = Director::getInstance()->convertToGL(postionMove);
	m_point_move = Vec2(tmpPosMove.x - this->get_xPos(), tmpPosMove.y - this->get_yPos());

	Vec2 postionMovePre = pTouch->getPreviousLocationInView();
	Vec2 tmpPosMovePre = Director::getInstance()->convertToGL(postionMovePre);
	m_point_preMove = Vec2(tmpPosMovePre.x - this->get_xPos(), tmpPosMovePre.y - this->get_yPos());

	float xMovePre = m_point_preMove.x;
	float yMovePre = m_point_preMove.y;

	float xMove = m_point_move.x;
	float yMove = m_point_move.y;

	float xOff = xMove - xMovePre;
	float yOff = yMove - yMovePre;

	if (yOff > 0 && fabs(yOff) > fabs(xOff))
	{
		m_state_direction = DIRECTION_UP;
	}
	else if (yOff < 0 && fabs(yOff) > fabs(xOff))
	{
		m_state_direction = DIRECTION_DOWN;
	}
	else if (xOff > 0 && fabs(xOff) > fabs(yOff))
	{
		m_state_direction = DIRECTION_RIGHT;
	}
	else if (xOff < 0 && fabs(xOff) > fabs(yOff))
	{	
		m_state_direction = DIRECTION_LEFT;
	}

	// ��BeginTouch rowúcolumn
	int nBeginColumn = get_column(m_point_begin);
	int nBeginRow = get_row(m_point_begin);

	m_xBeginPos = nBeginColumn * m_chessBoardLayer->getCellWidth() ;
	m_yBeginPos = nBeginRow * m_chessBoardLayer->getCellHeight();

	int nRowCanReach = nBeginRow;
	int nColumnCanReach = nBeginColumn;

	if (DIRECTION_UP == m_state_direction && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginRow  + 1< CHESSBOARD_ROW && 0 == m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn])
		{
			if (nBeginRow + 2 < CHESSBOARD_ROW && 0 == m_chessBoardLayer->m_array[nBeginRow + 2][nBeginColumn])
			{
				nRowCanReach = nRowCanReach + 2;
			}
			else
			{
				nRowCanReach = nRowCanReach + 1;
			}
		}

		// �������������ƶ�
		if (nRowCanReach != nBeginRow)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_yMinPos = nBeginRow * m_chessBoardLayer->getCellHeight();
			m_yMaxPos = nBeginRow * m_chessBoardLayer->getCellHeight() + (nRowCanReach - nBeginRow) * m_chessBoardLayer->getCellHeight();

			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}	
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}

	}
	else if (DIRECTION_DOWN == m_state_direction  && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginRow - 1 >= 0 && 0 == m_chessBoardLayer->m_array[nBeginRow - 1][nBeginColumn])
		{
			if (nBeginRow - 2 >= 0 && 0 == m_chessBoardLayer->m_array[nBeginRow - 2][nBeginColumn])
			{
				nRowCanReach = nRowCanReach - 2;
			}
			else
			{
				nRowCanReach = nRowCanReach - 1;
			}
		}

		// ������������ƶ�
		if (nRowCanReach != nBeginRow)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_yMaxPos = nBeginRow * m_chessBoardLayer->getCellHeight();
			m_yMinPos = nBeginRow * m_chessBoardLayer->getCellHeight() - (nBeginRow - nRowCanReach) * m_chessBoardLayer->getCellHeight();

			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}
	}
	else if (DIRECTION_LEFT == m_state_direction  && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginColumn - 1 >= 0 && 0 == m_chessBoardLayer->m_array[nBeginRow][nBeginColumn - 1])
		{
			if (nBeginColumn - 2 >= 0 && 0 == m_chessBoardLayer->m_array[nBeginRow][nBeginColumn - 2])
			{
				nColumnCanReach = nColumnCanReach - 2;
			}
			else
			{
				nColumnCanReach = nColumnCanReach - 1;
			}
		}

		// �����:�����ƶ�
		if (nColumnCanReach != nBeginColumn)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_xMaxPos = nBeginColumn * m_chessBoardLayer->getCellWidth();
			m_xMinPos = nBeginColumn * m_chessBoardLayer->getCellWidth() - (nBeginColumn - nColumnCanReach) * m_chessBoardLayer->getCellWidth();

			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}
	}
	else if (DIRECTION_RIGHT == m_state_direction  && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginColumn + 1 < CHESSBOARD_COLUMN && 0 == m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 1])
		{
			if (nBeginColumn + 2 < CHESSBOARD_COLUMN && 0 == m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 2])
			{
				nColumnCanReach = nColumnCanReach + 2;
			}
			else 
			{
				nColumnCanReach = nColumnCanReach + 1;
			}
		}

		// ������������ƶ�
		if (nColumnCanReach != nBeginColumn)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_xMinPos = nBeginColumn * m_chessBoardLayer->getCellWidth();
			m_xMaxPos = nBeginColumn * m_chessBoardLayer->getCellWidth() + (nColumnCanReach - nBeginColumn) * m_chessBoardLayer->getCellWidth();

			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos) 
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos) 
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}

	}
}

void BlockLayer::initTouchMoveForVer( Touch *pTouch )
{
	Vec2 postionMove = pTouch->getLocationInView();
	m_point_move = Director::getInstance()->convertToGL(postionMove);

	Vec2 postionMovePre = pTouch->getPreviousLocationInView();
	m_point_preMove = Director::getInstance()->convertToGL(postionMovePre);

	float xMovePre = m_point_preMove.x;
	float yMovePre = m_point_preMove.y;

	float xMove = m_point_move.x;
	float yMove = m_point_move.y;

	float xOff = xMove - xMovePre;
	float yOff = yMove - yMovePre;

	if (yOff > 0 && fabs(yOff) > fabs(xOff))
	{
		m_state_direction = DIRECTION_UP;
	}
	else if (yOff < 0 && fabs(yOff) > fabs(xOff))
	{
		m_state_direction = DIRECTION_DOWN;
	}
	else if (xOff > 0 && fabs(xOff) > fabs(yOff))
	{
		m_state_direction = DIRECTION_RIGHT;
	}
	else if (xOff < 0 && fabs(xOff) > fabs(yOff))
	{	
		m_state_direction = DIRECTION_LEFT;
	}

	// ��BeginTouch rowúcolumn
	int nBeginColumn = get_column(m_point_begin);
	int nBeginRow = get_row(m_point_begin);

	// ͸�beginTouchĵ����ĵ
	changeRowToCenter(nBeginRow);

	m_xBeginPos = nBeginColumn * m_chessBoardLayer->getCellWidth();
	m_yBeginPos = nBeginRow * m_chessBoardLayer->getCellHeight();

	int nRowCanReach = nBeginRow;
	int nColumnCanReach = nBeginColumn;

	if (DIRECTION_UP == m_state_direction && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginRow  + 2 < CHESSBOARD_ROW && 0 == m_chessBoardLayer->m_array[nBeginRow + 2][nBeginColumn])
		{
			if (nBeginRow + 3 < CHESSBOARD_ROW && 0 == m_chessBoardLayer->m_array[nBeginRow + 3][nBeginColumn])
			{
				nRowCanReach = nRowCanReach + 2;
			}
			else
			{
				nRowCanReach = nRowCanReach + 1;
			}
		}

		// �������������ƶ�
		if (nRowCanReach != nBeginRow)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_yMinPos = nBeginRow * m_chessBoardLayer->getCellHeight();
			m_yMaxPos = nBeginRow * m_chessBoardLayer->getCellHeight() + (nRowCanReach - nBeginRow) * m_chessBoardLayer->getCellHeight();

			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}	
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}

	}
	else if (DIRECTION_DOWN == m_state_direction  && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginRow - 1 >= 0 && 0 == m_chessBoardLayer->m_array[nBeginRow - 1][nBeginColumn])
		{
			if (nBeginRow - 2 >= 0 && 0 == m_chessBoardLayer->m_array[nBeginRow - 2][nBeginColumn])
			{
				nRowCanReach = nRowCanReach - 2;
			}
			else
			{
				nRowCanReach = nRowCanReach - 1;
			}
		}

		// ������������ƶ�
		if (nRowCanReach != nBeginRow)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_yMaxPos = nBeginRow * m_chessBoardLayer->getCellHeight();
			m_yMinPos = nBeginRow * m_chessBoardLayer->getCellHeight() - (nBeginRow - nRowCanReach) * m_chessBoardLayer->getCellHeight();

			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}
	}
	else if (DIRECTION_LEFT == m_state_direction  && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginColumn - 1 >= 0 && 
			0 == m_chessBoardLayer->m_array[nBeginRow][nBeginColumn - 1] &&
			0 == m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn - 1])
		{
			nColumnCanReach = nColumnCanReach - 1;
		}

		// �����:�����ƶ�
		if (nColumnCanReach != nBeginColumn)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_xMaxPos = nBeginColumn * m_chessBoardLayer->getCellWidth();
			m_xMinPos = nBeginColumn * m_chessBoardLayer->getCellWidth() - (nBeginColumn - nColumnCanReach) * m_chessBoardLayer->getCellWidth();

			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}
	}
	else if (DIRECTION_RIGHT == m_state_direction  && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginColumn + 1 < CHESSBOARD_COLUMN && 
			0 == m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 1] &&
			0 == m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn + 1])
		{
			nColumnCanReach = nColumnCanReach + 1;
		}

		// ������������ƶ�
		if (nColumnCanReach != nBeginColumn)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_xMinPos = nBeginColumn * m_chessBoardLayer->getCellWidth();
			m_xMaxPos = nBeginColumn * m_chessBoardLayer->getCellWidth() + (nColumnCanReach - nBeginColumn) * m_chessBoardLayer->getCellWidth();

			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos) 
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos) 
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}

	}
}

void BlockLayer::initTouchMoveForHor( Touch *pTouch )
{
	Vec2 postionMove = pTouch->getLocationInView();
	m_point_move = Director::getInstance()->convertToGL(postionMove);

	Vec2 postionMovePre = pTouch->getPreviousLocationInView();
	m_point_preMove = Director::getInstance()->convertToGL(postionMovePre);

	float xMovePre = m_point_preMove.x;
	float yMovePre = m_point_preMove.y;

	float xMove = m_point_move.x;
	float yMove = m_point_move.y;

	float xOff = xMove - xMovePre;
	float yOff = yMove - yMovePre;


	if (yOff > 0 && fabs(yOff) > fabs(xOff))
	{
		m_state_direction = DIRECTION_UP;
	}
	else if (yOff < 0 && fabs(yOff) > fabs(xOff))
	{
		m_state_direction = DIRECTION_DOWN;
	}
	else if (xOff > 0 && fabs(xOff) > fabs(yOff))
	{
		m_state_direction = DIRECTION_RIGHT;
	}
	else if (xOff < 0 && fabs(xOff) > fabs(yOff))
	{	
		m_state_direction = DIRECTION_LEFT;
	}

	// ��BeginTouch rowúcolumn
	int nBeginColumn = get_column(m_point_begin);
	int nBeginRow = get_row(m_point_begin);

	// ͸�beginTouchĵ����ĵ
	changeColumnToCenter(nBeginColumn);

	m_xBeginPos = nBeginColumn * m_chessBoardLayer->getCellWidth();
	m_yBeginPos = nBeginRow * m_chessBoardLayer->getCellHeight();

	int nRowCanReach = nBeginRow;
	int nColumnCanReach = nBeginColumn;

	if (DIRECTION_UP == m_state_direction && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginRow + 1 <= CHESSBOARD_ROW && 
			0 == m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn] &&
			0 == m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn + 1])
		{
			nRowCanReach = nRowCanReach + 1;
		}

		// �������������ƶ�
		if (nRowCanReach != nBeginRow)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_yMinPos = nBeginRow * m_chessBoardLayer->getCellHeight();
			m_yMaxPos = nBeginRow * m_chessBoardLayer->getCellHeight() + (nRowCanReach - nBeginRow) * m_chessBoardLayer->getCellHeight();

			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}	
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}

	}
	else if (DIRECTION_DOWN == m_state_direction  && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginRow - 1 >= 0 && 
			0 == m_chessBoardLayer->m_array[nBeginRow - 1][nBeginColumn] &&
			0 == m_chessBoardLayer->m_array[nBeginRow - 1][nBeginColumn + 1])
		{
			nRowCanReach = nRowCanReach - 1;
		}

		// ������������ƶ�
		if (nRowCanReach != nBeginRow)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_yMaxPos = nBeginRow * m_chessBoardLayer->getCellHeight();
			m_yMinPos = nBeginRow * m_chessBoardLayer->getCellHeight() - (nBeginRow - nRowCanReach) * m_chessBoardLayer->getCellHeight();

			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}
	}
	else if (DIRECTION_LEFT == m_state_direction  && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginColumn - 1 >= 0 && 0 == m_chessBoardLayer->m_array[nBeginRow][nBeginColumn - 1])
		{
			if (nBeginColumn - 2 >= 0 && 0 == m_chessBoardLayer->m_array[nBeginRow][nBeginColumn -2])
			{
				nColumnCanReach = nColumnCanReach - 2;
			}
			else
			{
				nColumnCanReach = nColumnCanReach - 1;
			}
		}

		// �����:�����ƶ�
		if (nColumnCanReach != nBeginColumn)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_xMaxPos = nBeginColumn * m_chessBoardLayer->getCellWidth();
			m_xMinPos = nBeginColumn * m_chessBoardLayer->getCellWidth() - (nBeginColumn - nColumnCanReach) * m_chessBoardLayer->getCellWidth();

			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}
		else if (bIsMoveing)
		{

			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}
	}
	else if (DIRECTION_RIGHT == m_state_direction  && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginColumn + 2 <= CHESSBOARD_COLUMN && 0 == m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 2])
		{
			if (nBeginColumn + 3 <= CHESSBOARD_COLUMN && 0 == m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 3])
			{
				nColumnCanReach = nColumnCanReach + 2;
			}
			else
			{
				nColumnCanReach = nColumnCanReach + 1;
			}
		}

		// ������������ƶ�
		if (nColumnCanReach != nBeginColumn)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_xMinPos = nBeginColumn * m_chessBoardLayer->getCellWidth();
			m_xMaxPos = nBeginColumn * m_chessBoardLayer->getCellWidth() + (nColumnCanReach - nBeginColumn) * m_chessBoardLayer->getCellWidth();

			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos) 
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos) 
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}

	}
}

void BlockLayer::initTouchMoveForBig( Touch *pTouch )
{
	Vec2 postionMove = pTouch->getLocationInView();
	m_point_move = Director::getInstance()->convertToGL(postionMove);

	Vec2 postionMovePre = pTouch->getPreviousLocationInView();
	m_point_preMove = Director::getInstance()->convertToGL(postionMovePre);

	float xMovePre = m_point_preMove.x;
	float yMovePre = m_point_preMove.y;

	float xMove = m_point_move.x;
	float yMove = m_point_move.y;

	float xOff = xMove - xMovePre;
	float yOff = yMove - yMovePre;

	if (yOff > 0 && fabs(yOff) > fabs(xOff))
	{
		m_state_direction = DIRECTION_UP;
	}
	else if (yOff < 0 && fabs(yOff) > fabs(xOff))
	{
		m_state_direction = DIRECTION_DOWN;
	}
	else if (xOff > 0 && fabs(xOff) > fabs(yOff))
	{
		m_state_direction = DIRECTION_RIGHT;
	}
	else if (xOff < 0 && fabs(xOff) > fabs(yOff))
	{	
		m_state_direction = DIRECTION_LEFT;
	}

	// ��BeginTouch rowúcolumn
	int nBeginColumn = get_column(m_point_begin);
	int nBeginRow = get_row(m_point_begin);

	// ͸�beginTouchĵ����ĵ
	changeRowToCenter(nBeginRow);
	changeColumnToCenter(nBeginColumn);

	m_xBeginPos = nBeginColumn * m_chessBoardLayer->getCellWidth();
	m_yBeginPos = nBeginRow * m_chessBoardLayer->getCellHeight();

	int nRowCanReach = nBeginRow;
	int nColumnCanReach = nBeginColumn;

	if (DIRECTION_UP == m_state_direction && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginRow + 2 <= CHESSBOARD_ROW && 
			0 == m_chessBoardLayer->m_array[nBeginRow + 2][nBeginColumn] &&
			0 == m_chessBoardLayer->m_array[nBeginRow + 2][nBeginColumn + 1])
		{
			nRowCanReach = nRowCanReach + 1;
		}

		// �������������ƶ�
		if (nRowCanReach != nBeginRow)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_yMinPos = nBeginRow * m_chessBoardLayer->getCellHeight();
			m_yMaxPos = nBeginRow * m_chessBoardLayer->getCellHeight() + (nRowCanReach - nBeginRow) * m_chessBoardLayer->getCellHeight();

			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}	
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}

	}
	else if (DIRECTION_DOWN == m_state_direction  && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginRow - 1 >= 0 && 
			0 == m_chessBoardLayer->m_array[nBeginRow - 1][nBeginColumn] &&
			0 == m_chessBoardLayer->m_array[nBeginRow - 1][nBeginColumn + 1])
		{
			nRowCanReach = nRowCanReach - 1;
		}

		// ������������ƶ�
		if (nRowCanReach != nBeginRow)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_yMaxPos = nBeginRow * m_chessBoardLayer->getCellHeight();
			m_yMinPos = nBeginRow * m_chessBoardLayer->getCellHeight() - (nBeginRow - nRowCanReach) * m_chessBoardLayer->getCellHeight();

			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionY() + yOff >= m_yMinPos && m_pBlockSprite->getPositionY() + yOff <= m_yMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}
	}
	else if (DIRECTION_LEFT == m_state_direction  && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginColumn - 1 >= 0 && 
			0 == m_chessBoardLayer->m_array[nBeginRow][nBeginColumn - 1] &&
			0 == m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn - 1])
		{
			nColumnCanReach = nColumnCanReach - 1;
		}

		// �����:�����ƶ�
		if (nColumnCanReach != nBeginColumn)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_xMaxPos = nBeginColumn * m_chessBoardLayer->getCellWidth();
			m_xMinPos = nBeginColumn * m_chessBoardLayer->getCellWidth() - (nBeginColumn - nColumnCanReach) * m_chessBoardLayer->getCellWidth();

			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos)
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}
	}
	else if (DIRECTION_RIGHT == m_state_direction  && checkDirectionIsUserful(m_state_direction))
	{
		if (nBeginColumn + 2 < CHESSBOARD_COLUMN && 
			0 == m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 2] &&
			0 == m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn + 2])
		{
			nColumnCanReach = nColumnCanReach + 1;
		}

		// ������������ƶ�
		if (nColumnCanReach != nBeginColumn)
		{
			this->m_direction_positive = m_state_direction;
			this->setNegativeDirection(m_state_direction);

			m_xMinPos = nBeginColumn * m_chessBoardLayer->getCellWidth();
			m_xMaxPos = nBeginColumn * m_chessBoardLayer->getCellWidth() + (nColumnCanReach - nBeginColumn) * m_chessBoardLayer->getCellWidth();

			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos) 
			{
				this->blockMove(xOff, yOff, m_state_direction);

				bIsMoveing = true;
			}
		}
		else if (bIsMoveing)
		{
			if (m_pBlockSprite->getPositionX() + xOff >= m_xMinPos && m_pBlockSprite->getPositionX() + xOff <= m_xMaxPos) 
			{
				this->blockMove(xOff, yOff, m_state_direction);
			}
		}
	}
}

void BlockLayer::initTouchEnd( Touch *pTouch )
{
	if (BLOCK_TYPE_SMALL == m_blockType)
	{
		initTouchEndForSmall(pTouch);
	}
	else if (BLOCK_TYPE_VER == m_blockType)
	{
		initTouchEndForVer(pTouch);
	}
	else if (BLOCK_TYPE_HOR == m_blockType)
	{
		initTouchEndForHor(pTouch);
	}
	else if (BLOCK_TYPE_BIG == m_blockType)
	{
		initTouchEndForBig(pTouch);
	}
}

void BlockLayer::initTouchEndForSmall( Touch *pTouch )
{
	this->m_direction_positive = DIRECTION_NONE;
	this->setNegativeDirection(DIRECTION_NONE);

	float xSpriteEndPos = m_pBlockSprite->getPositionX();
	float ySpriteEndPos = m_pBlockSprite->getPositionY();

	// ��BeginTouch rowúcolumn
	int nSpriteInColumn = get_column(m_pBlockSprite->getPosition());
	int nSpriteInRow = get_row(m_pBlockSprite->getPosition());

	float xBetweenMinPos = nSpriteInColumn * m_chessBoardLayer->getCellWidth();
	float xBetweenMaxPos = (nSpriteInColumn + 1) * m_chessBoardLayer->getCellWidth();

	float yBetweenMinPos = nSpriteInRow * m_chessBoardLayer->getCellHeight();
	float yBetweenMaxPos = (nSpriteInRow + 1) * m_chessBoardLayer->getCellHeight();

	// �˵� �ֻ���ƶ�� ���ֱ���
	if (xBetweenMinPos == xSpriteEndPos)
	{
		float yOffWithMin = ySpriteEndPos - yBetweenMinPos;
		float yOffWithMax = yBetweenMaxPos - ySpriteEndPos;

		calculateSpentTimeForVer(pTouch);

		// sprite����ڵrow + 1
		if (yOffWithMax < yOffWithMin)
		{
			nSpriteInRow++;

			//this->blockMove(0, yOffWithMax, DIRECTION_UP);
			this->blockRunMoveAction(0, yOffWithMax, DIRECTION_UP);
		}
		else
		{
			//this->blockMove(0, -yOffWithMin, DIRECTION_DOWN);
			this->blockRunMoveAction(0, yOffWithMin, DIRECTION_DOWN);
		}

	}
	else if (yBetweenMinPos == ySpriteEndPos)	// �˵��ֻ���ƶ�� �ˮƽ���
	{
		float xOffWithMin = xSpriteEndPos - xBetweenMinPos;
		float xOffWithMax = xBetweenMaxPos - xSpriteEndPos;

		calculateSpentTimeForHor(pTouch);

		// sprite����ڵcolumn +1
		if (xOffWithMax < xOffWithMin)
		{
			nSpriteInColumn++;

			//this->blockMove(xOffWithMax, 0, DIRECTION_RIGHT);
			this->blockRunMoveAction(xOffWithMax, 0, DIRECTION_RIGHT);
		}
		else
		{
			//this->blockMove(-xOffWithMin, 0, DIRECTION_LEFT);
			this->blockRunMoveAction(xOffWithMin, 0, DIRECTION_LEFT);
		}
	}

	// ����Ϲ�� � sprite����ڵrow ĺ column �ȷ�����

	int nBeginInColumn = get_column(m_point_begin);
	int nBeginInRow = get_row(m_point_begin);

	int nEndInColumn = nSpriteInColumn;
	int nEndInRow = nSpriteInRow;

	refreshEndMoveState(nBeginInRow, nBeginInColumn, nEndInRow, nEndInColumn);

	//���²��
	calculateStepMove(nBeginInRow, nBeginInColumn, nEndInRow, nEndInColumn);

	bIsMoveing = false;
}

void BlockLayer::initTouchEndForVer( Touch *pTouch )
{
	this->m_direction_positive = DIRECTION_NONE;
	this->setNegativeDirection(DIRECTION_NONE);

	float xSpriteEndPos = m_pBlockSprite->getPositionX();
	float ySpriteEndPos = m_pBlockSprite->getPositionY();

	// ��BeginTouch rowúcolumn
	int nSpriteInColumn = get_column(m_pBlockSprite->getPosition());
	int nSpriteInRow = get_row(m_pBlockSprite->getPosition());

	float xBetweenMinPos = nSpriteInColumn * m_chessBoardLayer->getCellWidth();
	float xBetweenMaxPos = (nSpriteInColumn + 1) * m_chessBoardLayer->getCellWidth();

	float yBetweenMinPos = nSpriteInRow * m_chessBoardLayer->getCellHeight();
	float yBetweenMaxPos = (nSpriteInRow + 1) * m_chessBoardLayer->getCellHeight();

	// �˵� �ֻ���ƶ�� ���ֱ���
	if (xBetweenMinPos == xSpriteEndPos)
	{
		float yOffWithMin = ySpriteEndPos - yBetweenMinPos;
		float yOffWithMax = yBetweenMaxPos - ySpriteEndPos;

		calculateSpentTimeForVer(pTouch);

		// sprite����ڵrow + 1
		if (yOffWithMax < yOffWithMin)
		{
			nSpriteInRow++;

			//this->blockMove(0, yOffWithMax, DIRECTION_UP);
			this->blockRunMoveAction(0, yOffWithMax, DIRECTION_UP);
		}
		else
		{
			//this->blockMove(0, -yOffWithMin, DIRECTION_DOWN);
			this->blockRunMoveAction(0, yOffWithMin, DIRECTION_DOWN);
		}

	}
	else if (yBetweenMinPos == ySpriteEndPos)	// �˵��ֻ���ƶ�� �ˮƽ���
	{
		float xOffWithMin = xSpriteEndPos - xBetweenMinPos;
		float xOffWithMax = xBetweenMaxPos - xSpriteEndPos;

		calculateSpentTimeForHor(pTouch);

		// sprite����ڵcolumn +1
		if (xOffWithMax < xOffWithMin)
		{
			nSpriteInColumn++;

			//this->blockMove(xOffWithMax, 0, DIRECTION_RIGHT);
			this->blockRunMoveAction(xOffWithMax, 0, DIRECTION_RIGHT);
		}
		else
		{
			//this->blockMove(-xOffWithMin, 0, DIRECTION_LEFT);
			this->blockRunMoveAction(xOffWithMin, 0, DIRECTION_LEFT);
		}
	}

	// ����Ϲ�� � sprite����ڵrow ĺ column �ȷ�����

	int nBeginInColumn = get_column(m_point_begin);
	int nBeginInRow = get_row(m_point_begin);

	// ��beginTouchĵ����ĵ
	changeRowToCenter(nBeginInRow);

	int nEndInColumn = nSpriteInColumn;
	int nEndInRow = nSpriteInRow;

	refreshEndMoveState(nBeginInRow, nBeginInColumn, nEndInRow, nEndInColumn);

	//���²��
	calculateStepMove(nBeginInRow, nBeginInColumn, nEndInRow, nEndInColumn);

	bIsMoveing = false;
}

void BlockLayer::initTouchEndForHor( Touch *pTouch )
{
	this->m_direction_positive = DIRECTION_NONE;
	this->setNegativeDirection(DIRECTION_NONE);

	float xSpriteEndPos = m_pBlockSprite->getPositionX();
	float ySpriteEndPos = m_pBlockSprite->getPositionY();

	// ��BeginTouch rowúcolumn
	int nSpriteInColumn = get_column(m_pBlockSprite->getPosition());
	int nSpriteInRow = get_row(m_pBlockSprite->getPosition());

	float xBetweenMinPos = nSpriteInColumn * m_chessBoardLayer->getCellWidth();
	float xBetweenMaxPos = (nSpriteInColumn + 1) * m_chessBoardLayer->getCellWidth();

	float yBetweenMinPos = nSpriteInRow * m_chessBoardLayer->getCellHeight();
	float yBetweenMaxPos = (nSpriteInRow + 1) * m_chessBoardLayer->getCellHeight();

	// �˵� �ֻ���ƶ�� ���ֱ���
	if (xBetweenMinPos == xSpriteEndPos)
	{
		float yOffWithMin = ySpriteEndPos - yBetweenMinPos;
		float yOffWithMax = yBetweenMaxPos - ySpriteEndPos;

		calculateSpentTimeForVer(pTouch);

		// sprite����ڵrow + 1
		if (yOffWithMax < yOffWithMin)
		{
			nSpriteInRow++;

			//this->blockMove(0, yOffWithMax, DIRECTION_UP);
			this->blockRunMoveAction(0, yOffWithMax, DIRECTION_UP);
		}
		else
		{
			//this->blockMove(0, -yOffWithMin, DIRECTION_DOWN);
			this->blockRunMoveAction(0, yOffWithMin, DIRECTION_DOWN);
		}

	}
	else if (yBetweenMinPos == ySpriteEndPos)	// �˵��ֻ���ƶ�� �ˮƽ���
	{
		float xOffWithMin = xSpriteEndPos - xBetweenMinPos;
		float xOffWithMax = xBetweenMaxPos - xSpriteEndPos;

		calculateSpentTimeForHor(pTouch);

		// sprite����ڵcolumn +1
		if (xOffWithMax < xOffWithMin)
		{
			nSpriteInColumn++;

			//this->blockMove(xOffWithMax, 0, DIRECTION_RIGHT);
			this->blockRunMoveAction(xOffWithMax, 0, DIRECTION_RIGHT);
		}
		else
		{
			//this->blockMove(-xOffWithMin, 0, DIRECTION_LEFT);
			this->blockRunMoveAction(xOffWithMin, 0, DIRECTION_LEFT);
		}
	}

	// ����Ϲ�� � sprite����ڵrow ĺ column �ȷ�����

	int nBeginInColumn = get_column(m_point_begin);
	int nBeginInRow = get_row(m_point_begin);

	// ��beginTouchĵ����ĵ
	changeColumnToCenter(nBeginInColumn);

	int nEndInColumn = nSpriteInColumn;
	int nEndInRow = nSpriteInRow;

	refreshEndMoveState(nBeginInRow, nBeginInColumn, nEndInRow, nEndInColumn);

	//���²��
	calculateStepMove(nBeginInRow, nBeginInColumn, nEndInRow, nEndInColumn);

	bIsMoveing = false;
}

void BlockLayer::initTouchEndForBig( Touch *pTouch )
{
	this->m_direction_positive = DIRECTION_NONE;
	this->setNegativeDirection(DIRECTION_NONE);

	float xSpriteEndPos = m_pBlockSprite->getPositionX();
	float ySpriteEndPos = m_pBlockSprite->getPositionY();

	// ��BeginTouch rowúcolumn
	int nSpriteInColumn = get_column(m_pBlockSprite->getPosition());
	int nSpriteInRow = get_row(m_pBlockSprite->getPosition());

	float xBetweenMinPos = nSpriteInColumn * m_chessBoardLayer->getCellWidth();
	float xBetweenMaxPos = (nSpriteInColumn + 1) * m_chessBoardLayer->getCellWidth();

	float yBetweenMinPos = nSpriteInRow * m_chessBoardLayer->getCellHeight();
	float yBetweenMaxPos = (nSpriteInRow + 1) * m_chessBoardLayer->getCellHeight();

	// �˵� �ֻ���ƶ�� ���ֱ���
	if (xBetweenMinPos == xSpriteEndPos)
	{
		float yOffWithMin = ySpriteEndPos - yBetweenMinPos;
		float yOffWithMax = yBetweenMaxPos - ySpriteEndPos;

		calculateSpentTimeForVer(pTouch);

		// sprite����ڵrow + 1
		if (yOffWithMax < yOffWithMin)
		{
			nSpriteInRow++;

			//this->blockMove(0, yOffWithMax, DIRECTION_UP);
			this->blockRunMoveAction(0, yOffWithMax, DIRECTION_UP);
		}
		else
		{
			//this->blockMove(0, -yOffWithMin, DIRECTION_DOWN);
			this->blockRunMoveAction(0, yOffWithMin, DIRECTION_DOWN);
		}

	}
	else if (yBetweenMinPos == ySpriteEndPos)	// �˵��ֻ���ƶ�� �ˮƽ���
	{
		float xOffWithMin = xSpriteEndPos - xBetweenMinPos;
		float xOffWithMax = xBetweenMaxPos - xSpriteEndPos;

		calculateSpentTimeForHor(pTouch);

		// sprite����ڵcolumn +1
		if (xOffWithMax < xOffWithMin)
		{
			nSpriteInColumn++;

			//this->blockMove(xOffWithMax, 0, DIRECTION_RIGHT);
			this->blockRunMoveAction(xOffWithMax, 0, DIRECTION_RIGHT);
		}
		else
		{
			//this->blockMove(-xOffWithMin, 0, DIRECTION_LEFT);
			this->blockRunMoveAction(xOffWithMin, 0, DIRECTION_LEFT);
		}
	}

	// ����Ϲ�� � sprite����ڵrow ĺ column �ȷ�����

	int nBeginInColumn = get_column(m_point_begin);
	int nBeginInRow = get_row(m_point_begin);

	// ��beginTouchĵ����ĵ
	changeRowToCenter(nBeginInRow);
	changeColumnToCenter(nBeginInColumn);

	int nEndInColumn = nSpriteInColumn;
	int nEndInRow = nSpriteInRow;

	refreshEndMoveState(nBeginInRow, nBeginInColumn, nEndInRow, nEndInColumn);

	//���²��
	calculateStepMove(nBeginInRow, nBeginInColumn, nEndInRow, nEndInColumn);

	bIsMoveing = false;

	// ����Ƿ�ʤ�
	if (checkIsWin())
	{
		m_chessBoardLayer->set_isWin(true);

		// �ܲ runWinerAction
		BigBlockRunWinnerAction();
	}
}

bool BlockLayer::checkDirectionIsUserful( int nDirection )
{
	bool bIsUserful = false;

	if (DIRECTION_NONE == this->m_direction_positive)
	{
		bIsUserful = true;
	}
	else if (nDirection == this->m_direction_positive || nDirection == this->m_direcion_negative)
	{
		bIsUserful = true;
	}

	return bIsUserful;
}

void BlockLayer::setNegativeDirection( int nPositiveDirection )
{
	if (DIRECTION_UP == nPositiveDirection)
	{
		this->m_direcion_negative = DIRECTION_DOWN;
	}
	else if (DIRECTION_DOWN == nPositiveDirection)
	{
		this->m_direcion_negative = DIRECTION_UP;
	}
	else if (DIRECTION_LEFT == nPositiveDirection)
	{
		this->m_direcion_negative = DIRECTION_RIGHT;
	}
	else if (DIRECTION_RIGHT == nPositiveDirection)
	{
		this->m_direcion_negative = DIRECTION_LEFT;
	}
	else if (DIRECTION_NONE == nPositiveDirection)
	{
		this->m_direcion_negative = DIRECTION_NONE;
	}
}

int BlockLayer::checkEndMoveDirection( int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn )
{
	int nEndMoveDirection = DIRECTION_NONE;

	if (nBeginRow == nEndRow && nBeginColumn == nEndColumn)
	{
		nEndMoveDirection = DIRECTION_NONE;
	}
	else
	{
		if (nEndColumn > nBeginColumn)
		{
			nEndMoveDirection = DIRECTION_RIGHT;
		}
		else if (nEndColumn < nBeginColumn)
		{
			nEndMoveDirection = DIRECTION_LEFT;
		}
		else if (nEndRow > nBeginRow)
		{
			nEndMoveDirection = DIRECTION_UP;
		}
		else if (nEndRow < nBeginRow)
		{
			nEndMoveDirection = DIRECTION_DOWN;
		}
	}

	return nEndMoveDirection;
}

void BlockLayer::refreshEndMoveState( int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn )
{
	if (BLOCK_TYPE_SMALL == m_blockType)
	{
		refreshEndMoveStateForSmall(nBeginRow, nBeginColumn, nEndRow, nEndColumn);
	}
	else if (BLOCK_TYPE_VER == m_blockType)
	{
		refreshEndMoveStateForVer(nBeginRow, nBeginColumn, nEndRow, nEndColumn);
	}
	else if (BLOCK_TYPE_HOR == m_blockType)
	{
		refreshEndMoveStateForHor(nBeginRow, nBeginColumn, nEndRow, nEndColumn);
	}
	else if (BLOCK_TYPE_BIG == m_blockType)
	{
		refreshEndMoveStateForBig(nBeginRow, nBeginColumn, nEndRow, nEndColumn);
	}
}

void BlockLayer::refreshEndMoveStateForSmall( int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn )
{
	m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
	m_chessBoardLayer->m_array[nEndRow][nEndColumn] = 1;

	refreshBlockState(nEndRow, nEndColumn);
}

void BlockLayer::refreshEndMoveStateForVer( int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn )
{
	int nEndMoveDirection = checkEndMoveDirection(nBeginRow, nBeginColumn, nEndRow, nEndColumn);

	if (DIRECTION_UP == nEndMoveDirection)
	{
		if (nBeginRow + 1 == nEndRow)
		{
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
			m_chessBoardLayer->m_array[nBeginRow + 2][nBeginColumn] = 1;

			refreshBlockState(nEndRow, nEndColumn);
		}
		else if (nBeginRow + 2 == nEndRow)
		{
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
			m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn] = 0;

			m_chessBoardLayer->m_array[nBeginRow + 2][nBeginColumn] = 1;
			m_chessBoardLayer->m_array[nBeginRow + 3][nBeginColumn] = 1;

			refreshBlockState(nEndRow, nEndColumn);
		}
	}
	else if (DIRECTION_DOWN == nEndMoveDirection)
	{
		if (nBeginRow - 1 == nEndRow)
		{
			m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn] = 0;
			m_chessBoardLayer->m_array[nBeginRow - 1][nBeginColumn] = 1;

			refreshBlockState(nEndRow, nEndColumn);
		}
		else if (nBeginRow - 2 == nEndRow)
		{
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
			m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn] = 0;

			m_chessBoardLayer->m_array[nBeginRow - 2][nBeginColumn] = 1;
			m_chessBoardLayer->m_array[nBeginRow - 1][nBeginColumn] = 1;

			refreshBlockState(nEndRow, nEndColumn);
		}
	}
	else if(DIRECTION_LEFT == nEndMoveDirection)
	{
		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
		m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn] = 0;

		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn - 1] = 1;
		m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn - 1] = 1;

		refreshBlockState(nEndRow, nEndColumn);
	}
	else if (DIRECTION_RIGHT == nEndMoveDirection)
	{
		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
		m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn] = 0;

		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 1] = 1;
		m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn + 1] = 1;

		refreshBlockState(nEndRow, nEndColumn);
	}
}

void BlockLayer::refreshEndMoveStateForHor( int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn )
{
	int nEndMoveDirection = checkEndMoveDirection(nBeginRow, nBeginColumn, nEndRow, nEndColumn);

	if (DIRECTION_UP == nEndMoveDirection)
	{
		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 1] = 0;

		m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn] = 1;
		m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn + 1] = 1;

		refreshBlockState(nEndRow, nEndColumn);
	}
	else if (DIRECTION_DOWN == nEndMoveDirection)
	{
		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 1] = 0;

		m_chessBoardLayer->m_array[nBeginRow - 1][nBeginColumn] = 1;
		m_chessBoardLayer->m_array[nBeginRow - 1][nBeginColumn + 1] = 1;

		refreshBlockState(nEndRow, nEndColumn);
	}
	else if(DIRECTION_LEFT == nEndMoveDirection)
	{
		if (nBeginColumn - 1 == nEndColumn)
		{
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 1] = 0;
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn - 1] = 1;

			refreshBlockState(nEndRow, nEndColumn);
		}
		else if (nBeginColumn - 2 == nEndColumn)
		{
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 1] = 0;

			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn - 1] = 1;
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn - 2] = 1;

			refreshBlockState(nEndRow, nEndColumn);
		}
	}
	else if (DIRECTION_RIGHT == nEndMoveDirection)
	{
		if (nBeginColumn + 1 == nEndColumn)
		{
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 2] = 1;

			refreshBlockState(nEndRow, nEndColumn);
		}
		else if (nBeginColumn + 2 == nEndColumn)
		{
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 1] = 0;

			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 2] = 1;
			m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 3] = 1;

			refreshBlockState(nEndRow, nEndColumn);
		}
	}
}

void BlockLayer::refreshEndMoveStateForBig( int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn )
{
	int nEndMoveDirection = checkEndMoveDirection(nBeginRow, nBeginColumn, nEndRow, nEndColumn);

	if (DIRECTION_UP == nEndMoveDirection)
	{
		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 1] = 0;

		m_chessBoardLayer->m_array[nBeginRow + 2][nBeginColumn] = 1;
		m_chessBoardLayer->m_array[nBeginRow + 2][nBeginColumn + 1] = 1;

		refreshBlockState(nEndRow, nEndColumn);
	}
	else if (DIRECTION_DOWN == nEndMoveDirection)
	{
		m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn] = 0;
		m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn + 1] = 0;

		m_chessBoardLayer->m_array[nBeginRow - 1][nBeginColumn] = 1;
		m_chessBoardLayer->m_array[nBeginRow - 1][nBeginColumn + 1] = 1;

		refreshBlockState(nEndRow, nEndColumn);
	}
	else if(DIRECTION_LEFT == nEndMoveDirection)
	{
		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 1] = 0;
		m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn + 1] = 0;

		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn - 1] = 1;
		m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn - 1] = 1;

		refreshBlockState(nEndRow, nEndColumn);
	}
	else if (DIRECTION_RIGHT == nEndMoveDirection)
	{
		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn] = 0;
		m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn] = 0;

		m_chessBoardLayer->m_array[nBeginRow][nBeginColumn + 2] = 1;
		m_chessBoardLayer->m_array[nBeginRow + 1][nBeginColumn + 2] = 1;

		refreshBlockState(nEndRow, nEndColumn);
	}
}

void BlockLayer::calculateStepMove( int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn )
{
	if (BLOCK_TYPE_SMALL == m_blockType)
	{
		calculateStepMoveForSmall(nBeginRow, nBeginColumn, nEndRow, nEndColumn);
	}
	else if (BLOCK_TYPE_VER == m_blockType)
	{
		calculateStepMoveForVer(nBeginRow, nBeginColumn, nEndRow, nEndColumn);
	}
	else if (BLOCK_TYPE_HOR == m_blockType)
	{
		calculateStepMoveForHor(nBeginRow, nBeginColumn, nEndRow, nEndColumn);
	}
	else if (BLOCK_TYPE_BIG == m_blockType)
	{
		calculateStepMoveForBig(nBeginRow, nBeginColumn, nEndRow, nEndColumn);
	}
}

void BlockLayer::calculateStepMoveForSmall( int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn )
{
	if (nBeginRow == nEndRow)
	{
		if (1 == abs(nBeginColumn - nEndColumn))
		{
			refreshStepMove(1);
		}
		else if (2 == abs(nBeginColumn - nEndColumn))
		{
			refreshStepMove(2);
		}
	}
	else if (nBeginColumn == nEndColumn)
	{
		if (1 == abs(nBeginRow - nEndRow))
		{
			refreshStepMove(1);
		}
		else if (2 == abs(nBeginRow - nEndRow))
		{
			refreshStepMove(2);
		}
	}
}

void BlockLayer::calculateStepMoveForVer( int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn )
{
	if (nBeginColumn == nEndColumn)
	{
		if (1 == abs(nBeginRow - nEndRow))
		{
			refreshStepMove(1);
		}
		else if (2 == abs(nBeginRow - nEndRow))
		{
			refreshStepMove(2);
		}
	}
	else if (nBeginRow == nEndRow)
	{
		if (1 == abs(nBeginColumn - nEndColumn))
		{
			refreshStepMove(1);
		}
	}
}

void BlockLayer::calculateStepMoveForHor( int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn )
{
	if (nBeginRow == nEndRow)
	{
		if (1 == abs(nBeginColumn - nEndColumn))
		{
			refreshStepMove(1);
		}
		else if (2 == abs(nBeginColumn - nEndColumn))
		{
			refreshStepMove(2);
		}
	}
	else if (nBeginColumn == nEndColumn)
	{
		if (1 == abs(nBeginRow - nEndRow))
		{
			refreshStepMove(1);
		}
	}
}

void BlockLayer::calculateStepMoveForBig( int nBeginRow, int nBeginColumn, int nEndRow, int nEndColumn )
{
	if (nBeginRow == nEndRow)
	{
		if (1 == abs(nBeginColumn - nEndColumn))
		{
			refreshStepMove(1);
		}
	}
	else if (nBeginColumn == nEndColumn)
	{
		if (1 == abs(nBeginRow - nEndRow))
		{
			refreshStepMove(1);
		}
	}
}

std::string BlockLayer::getImagePath( std::string strImageName )
{
	std::string tmpImageName = "";

	if ("guanyu_h" == strImageName || "guanyu_s" == strImageName || "huangzhong_h" == strImageName ||
		"huangzhong_s" == strImageName || "machao_h" == strImageName || "machao_s" == strImageName ||
		"zhangfei_h" == strImageName || "zhangfei_s" == strImageName || "zhaoyun_h" == strImageName ||
		"zhaoyun_s" == strImageName)
	{
		tmpImageName.append("res_ui/huarongdao/");
		tmpImageName.append(strImageName);
		tmpImageName.append(".png");
	}

	return tmpImageName;
}

void BlockLayer::blockRunMoveAction( float xOff, float yOff, int nDirection )
{
	if (NULL != m_pBlockSprite)
	{
		float spentTime = 0.2f;
		if (0.0f != m_velocity)
		{
			char charSpentTime[20];

			if (0.0f != xOff)
			{
				spentTime = fabs(xOff) / fabs(m_velocity) / 1000;

			}
			else if (0.0f != yOff)
			{
				spentTime = fabs(yOff) / fabs(m_velocity) / 1000;
			}

			if (spentTime > 0.2f)
			{
				spentTime = 0.2f;
			}
		}

		if (DIRECTION_UP == nDirection)
		{
			auto action = MoveTo::create(spentTime, Vec2(m_pBlockSprite->getPositionX(), m_pBlockSprite->getPositionY() + yOff));
			m_pBlockSprite->runAction(action);
		}
		else if (DIRECTION_DOWN == nDirection)
		{
			auto action = MoveTo::create(spentTime, Vec2(m_pBlockSprite->getPositionX(), m_pBlockSprite->getPositionY() - yOff));
			m_pBlockSprite->runAction(action);
		}
		else if (DIRECTION_LEFT == nDirection)
		{
			auto action = MoveTo::create(spentTime, Vec2(m_pBlockSprite->getPositionX() - xOff, m_pBlockSprite->getPositionY()));
			m_pBlockSprite->runAction(action);
		}
		else if (DIRECTION_RIGHT == nDirection)
		{
			auto action = MoveTo::create(spentTime, Vec2(m_pBlockSprite->getPositionX() + xOff, m_pBlockSprite->getPositionY()));
			m_pBlockSprite->runAction(action);
		}

		m_velocity = 0.0f;
	}
}

void BlockLayer::calculateSpentTimeForHor(Touch *pTouch)
{
	struct timeval m_sEndedTime;
	gettimeofday(&m_sEndedTime, NULL);
	m_dEndedTime = m_sEndedTime.tv_sec * 1000 + m_sEndedTime.tv_usec / 1000;

	Vec2 position = pTouch->getLocationInView();
	Vec2 tmpPos = Director::getInstance()->convertToGL(position);

	m_point_end = Vec2(tmpPos.x - this->get_xPos(), tmpPos.y - this->get_yPos());

	m_velocity = (float)(m_point_end.x - m_point_begin.x) / (float)(m_dEndedTime - m_dStartTime);
}

void BlockLayer::calculateSpentTimeForVer(Touch *pTouch)
{
	struct timeval m_sEndedTime;
	gettimeofday(&m_sEndedTime, NULL);
	m_dEndedTime = m_sEndedTime.tv_sec * 1000 + m_sEndedTime.tv_usec / 1000;

	Vec2 position = pTouch->getLocationInView();
	Vec2 tmpPos = Director::getInstance()->convertToGL(position);

	m_point_end = Vec2(tmpPos.x - this->get_xPos(), tmpPos.y - this->get_yPos());

	m_velocity = (float)(m_point_end.y - m_point_begin.y) / (float)(m_dEndedTime - m_dStartTime);
}

void BlockLayer::BigBlockRunWinnerAction()
{
	auto action =(ActionInterval *)Sequence::create(
		Show::create(),
		DelayTime::create(0.2f),
		MoveBy::create(0.1f, Vec2(0, -25.0f)),
		CallFuncN::create(CC_CALLBACK_1(BlockLayer::callBackWinnerActionFinish, this)),
		NULL);
	m_pBlockSprite->runAction(action);
}

void BlockLayer::callBackWinnerActionFinish( Ref * obj )
{
	/*********************************ٵ��������Ч������ȥ�***************************************/
	//// �����
	//float xPos = m_pBlockSprite->getPositionX();
	//float yPos = m_pBlockSprite->getPositionY() + m_chessBoardLayer->getCellHeight() * 2;

	//cocos2d::extension::Scale9Sprite * spritePop = cocos2d::extension::Scale9Sprite::create("res_ui/dialog3.png");
	//spritePop->setPreferredSize(Size(150, 70));
	//spritePop->setCapInsets(Rect(10, 10, 1, 1));
	//spritePop->setAnchorPoint(Vec2(0, 0));
	//spritePop->setPosition(Vec2(xPos,yPos));

	//const char * strPopInfo = StringDataManager::getString("ChessBoardUI_info");
	//Label * labelTTF_popInfo = Label::createWithTTF(strPopInfo, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
	//labelTTF_popInfo->setIgnoreAnchorPointForPosition(false);
	//labelTTF_popInfo->setAnchorPoint(Vec2(0, 0));
	//labelTTF_popInfo->setPosition(Vec2(2, 20));
	//
	//spritePop->addChild(labelTTF_popInfo);
	//spritePop->setVisible(false);

	//this->addChild(spritePop);

	//// ����ݵĶ���Ч�
	//ActionInterval * actionInterval = (ActionInterval *)Sequence::create(
	//	ScaleTo::create(0.01f, 0.0f),
	//	Show::create(),
	//	ScaleTo::create(0.25f, 1.3f),
	//	ScaleTo::create(0.05f, 1),
	//	NULL);

	//spritePop->runAction(actionInterval);
	/***********************************************************************************************/

	// ��ص�Ч�
	auto mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
// 	if (NULL != mainScene)
// 	{
// 		mainScene->addInterfaceAnm("gxgg/gxgg.anm");
// 	}
	auto mainAnmScene = GameView::getInstance()->getMainAnimationScene();
	if (mainAnmScene)
	{
		mainAnmScene->addInterfaceAnm("gxgg/gxgg.anm");
	}
		
	
	// ���塰��ɡ���ť���,��ҡ����á���ť�����
	auto pChessBoradUI = (ChessBoardUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagChessBoardUI);
	if (NULL != pChessBoradUI)
	{
		pChessBoradUI->setFinishBtnEnable();
		pChessBoradUI->setResetBtnDisable();

		// õ������Ч�
		pChessBoradUI->initFinishParticle();

		// ��Ƴ��ڵ�ͼƬ����Ч�
		pChessBoradUI->removeExitSprite();
	}
}

