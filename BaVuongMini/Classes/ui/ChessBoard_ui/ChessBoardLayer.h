#ifndef _UI_CHESSBOARD_CHESSBOARDLAYER_H_
#define _UI_CHESSBOARD_CHESSBOARDLAYER_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class ChessBlockItem;

class ChessBoardLayer :
	public cocos2d::Layer
{
public:
	ChessBoardLayer(void);
	~ChessBoardLayer(void);

	enum
	{
		DIRECTION_UP = 1,
		DIRECTION_DOWN,
		DIRECTION_LEFT,
		DIRECTION_RIGHT,
		DIRECTION_NONE,
	};
public:
	static ChessBoardLayer * s_chessBoardLayer;
	static ChessBoardLayer * getInstance();

public:
	static ChessBoardLayer* create();

	virtual bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

private:
	Vec2 m_point_begin;
	Vec2 m_point_end; 

	float m_width_cell;
	float m_height_cell;

	float m_width_chessBoard;
	float m_height_chessBoard;

	int m_state_direction;

	float m_xPos;
	float m_yPos;

	int m_nStepMove;														// ���
	int m_nDifficulty;														// ����׳̶ȣ�1���򵥣�2���еȣ�3����ѣ�
	
	bool m_bIsWin;

public:
	float getCellWidth();
	float getCellHeight();

	float getChessBoardWidth();
	float getChessBoardHeight();

	void set_xPos(float xPos);
	float get_xPos();

	void set_yPos(float yPos);
	float get_yPos();

	void set_stepMove(int nStep);
	int get_stepMove();

	int get_row(Vec2 point);																			// ͨ����pointĻ�ȡ���ڵrow
	int get_column(Vec2 point);																		// �ͨ����pointĻ�ȡ���ڵcolumn

	void set_difficulty(int nDifficulty);
	int get_difficulty();

	void set_isWin(bool bIsWin);
	bool get_isWin();
	
	void initDataFormXML(std::vector<ChessBlockItem *> vector);
	void initChessBoardUI(float xPos, float yPos, float boardWidth, float boardHeight);

	
	int m_array[5][4];

private:
	
	void initChessBoard(const char * strFileName);
	void initChessBoard();

	void initArrayEmtpy();

	void selectConfigXML(int nDifficulty);
};

#endif // _UI_CHESSBOARD_CHESSBOARDUI_H_