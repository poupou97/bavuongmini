#ifndef _UI_CHESSBOARD_CHESSBLOCKITEM_H_
#define _UI_CHESSBOARD_CHESSBLOCKITEM_H_

#include <string>

using namespace std;

class ChessBlockItem
{
public:
	ChessBlockItem();
	~ChessBlockItem();

private:
	int m_nId;
	int m_nType;
	int m_nRow;
	int m_nColumn;
	std::string m_strImageName;

public:
	void set_id(int nId);
	int get_id();

	void set_type(int nType);
	int get_type();

	void set_row(int nRow);
	int get_row();

	void set_column(int nColumn);
	int get_column();

	void set_imageName(std::string strImageName);
	std::string get_imageName();
};

#endif  //_UI_CHESSBOARD_CHESSBLOCKITEM_H_