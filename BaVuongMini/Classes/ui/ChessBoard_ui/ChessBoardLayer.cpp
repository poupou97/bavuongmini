#include "ChessBoardLayer.h"
#include "BlockLayer.h"
#include "XMLOperate.h"
#include "ChessBlockItem.h"

USING_NS_CC;

#define CHESSBOARD_ROW 5
#define CHESSBOARD_COLUMN 4

ChessBoardLayer * ChessBoardLayer::s_chessBoardLayer = NULL;

ChessBoardLayer::ChessBoardLayer( void )
	:m_state_direction(DIRECTION_NONE)
	,m_width_chessBoard(0.0f)
	,m_height_chessBoard(0.0f)
	,m_nStepMove(0)
	,m_nDifficulty(1)
	,m_bIsWin(false)
{

}

ChessBoardLayer::~ChessBoardLayer( void )
{

}

ChessBoardLayer* ChessBoardLayer::create()
{
	auto pLoadLayer = new ChessBoardLayer();
	if (pLoadLayer && pLoadLayer->init())
	{
		pLoadLayer->autorelease();
		return pLoadLayer;
	}
	CC_SAFE_DELETE(pLoadLayer);
	return NULL;
}

bool ChessBoardLayer::init()
{
	bool bRet=false;
	do 
	{
		CC_BREAK_IF(!Layer::init());

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		bRet=true;

	} while (0);

	return bRet;
}

void ChessBoardLayer::onEnter()
{
	Layer::onEnter();
}

void ChessBoardLayer::onExit()
{
	Layer::onExit();
}

bool ChessBoardLayer::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return false;
}

void ChessBoardLayer::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void ChessBoardLayer::onTouchEnded( Touch *pTouch, Event *pEvent )
{
	
}

void ChessBoardLayer::onTouchCancelled( Touch *pTouch, Event *pEvent )
{
	
}

int ChessBoardLayer::get_row( Vec2 point )
{
	Size size = Director::getInstance()->getVisibleSize();

	float x_pos = point.x;
	float y_pos = point.y;

	float width = size.width;
	float height = size.height;

	float cellWidth = width / CHESSBOARD_COLUMN;
	float cellHeight = height / CHESSBOARD_ROW;

	int nTouchColumn = x_pos / cellWidth;
	int nTouchRow = y_pos / cellHeight;

	return nTouchRow;
}

int ChessBoardLayer::get_column( Vec2 point )
{
	Size size = Director::getInstance()->getVisibleSize();

	float x_pos = point.x;
	float y_pos = point.y;

	float width = size.width;
	float height = size.height;

	float cellWidth = width / CHESSBOARD_COLUMN;
	float cellHeight = height / CHESSBOARD_ROW;

	int nTouchColumn = x_pos / cellWidth;
	int nTouchRow = y_pos / cellHeight;

	return nTouchColumn;
}

void ChessBoardLayer::initChessBoard()
{
	// ������
	XMLOperate::clearVector();

	// ݶ�ȡXML����ȡ��ݵ�s_vector_data
	this->selectConfigXML(this->get_difficulty());

	// ��ʼ��ռλ��Ϣ��ȫ��Ϊ0��
	this->initArrayEmtpy();

	this->initDataFormXML(XMLOperate::s_vector_data);
}

ChessBoardLayer * ChessBoardLayer::getInstance()
{
	if (NULL == s_chessBoardLayer)
	{
		s_chessBoardLayer = new ChessBoardLayer();
		s_chessBoardLayer->init();
	}

	return s_chessBoardLayer;
}

float ChessBoardLayer::getCellWidth()
{
	return m_width_cell;
}

float ChessBoardLayer::getCellHeight()
{
	return m_height_cell;
}

float ChessBoardLayer::getChessBoardWidth()
{
	return m_width_chessBoard;
}

float ChessBoardLayer::getChessBoardHeight()
{
	return m_height_chessBoard;
}

void ChessBoardLayer::initArrayEmtpy()
{
	for (int i = 0; i < CHESSBOARD_ROW; i++)
	{
		for (int j = 0; j < CHESSBOARD_COLUMN; j++)
		{
			m_array[i][j] = 0;
		}
	}
}

void ChessBoardLayer::initDataFormXML( std::vector<ChessBlockItem *> vector )
{
	// ��ȡXML����ʼ�������̡����
	int nSize = vector.size();
	for (int i = 0; i < nSize; i++)
	{
		auto pChessBlockItem = vector.at(i);
		int nId = pChessBlockItem->get_id();
		int nType = pChessBlockItem->get_type();
		int nRow = pChessBlockItem->get_row();
		int nColumn = pChessBlockItem->get_column();
		std::string strImageName = pChessBlockItem->get_imageName();

		// ֳ�ʼ��block
		auto pBlock = BlockLayer::create(this);
		pBlock->initBlock(nType, nRow, nColumn, m_width_cell, m_height_cell, strImageName);
		this->addChild(pBlock);

		// ��ʼ��վλ��Ϣ
		if (BlockLayer::BLOCK_TYPE_SMALL == nType)
		{
			m_array[nRow][nColumn] = 1;
		}
		else if (BlockLayer::BLOCK_TYPE_VER == nType)
		{
			m_array[nRow][nColumn] = 1;
			m_array[nRow + 1][nColumn] = 1;
		}
		else if (BlockLayer::BLOCK_TYPE_HOR == nType)
		{
			m_array[nRow][nColumn] = 1;
			m_array[nRow][nColumn + 1] = 1;
		}
		else if (BlockLayer::BLOCK_TYPE_BIG == nType)
		{
			m_array[nRow][nColumn] = 1;
			m_array[nRow][nColumn + 1] = 1;
			m_array[nRow + 1][nColumn] = 1;
			m_array[nRow + 1][nColumn + 1] = 1;
		}
	}
}

void ChessBoardLayer::initChessBoardUI( float xPos, float yPos, float boardWidth, float boardHeight )
{
	this->set_xPos(xPos);
	this->set_yPos(yPos);

	m_width_chessBoard = boardWidth;
	m_height_chessBoard = boardHeight;

	this->setContentSize(Size(m_width_chessBoard, m_height_chessBoard));

	m_width_cell = boardWidth / CHESSBOARD_COLUMN;
	m_height_cell = boardHeight / CHESSBOARD_ROW;

	// xml��
	initChessBoard();
}

void ChessBoardLayer::set_xPos( float xPos )
{
	this->m_xPos = xPos;
}

float ChessBoardLayer::get_xPos()
{
	return this->m_xPos;
}

void ChessBoardLayer::set_yPos( float yPos )
{
	this->m_yPos = yPos;
}

float ChessBoardLayer::get_yPos()
{
	return this->m_yPos;
}

void ChessBoardLayer::set_stepMove( int nStep )
{
	this->m_nStepMove = nStep;
}

int ChessBoardLayer::get_stepMove()
{
	return this->m_nStepMove;
}

void ChessBoardLayer::set_difficulty( int nDifficulty )
{
	this->m_nDifficulty = nDifficulty;
}

int ChessBoardLayer::get_difficulty()
{
	return this->m_nDifficulty;
}

void ChessBoardLayer::selectConfigXML( int nDifficulty )
{
	if (1 == nDifficulty || 2== nDifficulty || 3 == nDifficulty)
	{
		char charDifficulty[20];
		sprintf(charDifficulty, "%d", nDifficulty);

		std::string strFileName = "";
		strFileName.append("res_ui/huarongdao/ChessBoard_");
		strFileName.append(charDifficulty);
		strFileName.append(".xml");

		XMLOperate::readXML(strFileName.c_str());
	}
	else
	{
		XMLOperate::readXML("res_ui/huarongdao/ChessBoard_1.xml");
	}
}

void ChessBoardLayer::set_isWin( bool bIsWin )
{
	this->m_bIsWin = bIsWin;
}

bool ChessBoardLayer::get_isWin()
{
	return this->m_bIsWin;
}



