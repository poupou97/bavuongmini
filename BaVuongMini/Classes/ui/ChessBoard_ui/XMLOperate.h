#ifndef _UI_CHESSBOARD_XMLOPERATE_H_
#define _UI_CHESSBOARD_XMLOPERATE_H_

#include <vector>

class ChessBlockItem;

class XMLOperate
{
public:
	XMLOperate();
	~XMLOperate();

public:
	static void readXML(const char * charFileName);

	static void clearVector();

	static int s_randPassNum;									// ���ѡ�����Ĺؿ�
	
	static void clearPassNum();

public:
	static std::vector<ChessBlockItem *> s_vector_data;
};

#endif