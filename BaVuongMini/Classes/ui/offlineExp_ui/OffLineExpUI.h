
#ifndef  _OFFLINEEXPUI_OFFLINEEXPUI_H_
#define _OFFLINEEXPUI_OFFLINEEXPUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class OffLineExpUI : public UIScene, public TableViewDataSource, public TableViewDelegate
{
private:
	Layer * u_layer;
	TableView * m_tableView;
public:
	OffLineExpUI();
	~OffLineExpUI();

	static OffLineExpUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	void refreshTableViewWithOutChangeOffSet();

 	void VipBuyEvent(Ref *pSender, Widget::TouchEventType type);
	void GetExpEvent(Ref *pSender, Widget::TouchEventType type);

	void NormalEvent(Ref *pSender, Widget::TouchEventType type);
	void GoldEvent(Ref *pSender, Widget::TouchEventType type);
	void GoldInGotEvent(Ref *pSender, Widget::TouchEventType type);

	void RefreshSelectStaus();

	void RefreshUI();
	void setToDefault();

	void setCurRoleVipLevel(int lv);

private:
	int m_nGetExpType;

	int m_curRoleVipLevel;

private:
	CheckBox * cb_normal;
	CheckBox * cb_gold;
	CheckBox * cb_goldInGot;

	Button * Btn_getExp;
	Text * l_btn_des;

	Text * l_gold;
	Text * l_goldInGot;
};



/////////////////////////////////
/**
 * vip�ÿ����ȡ�µcell
 * @author yangjun
 * @version 0.1.0
 * @date 2014.5.6
 */
// 
// class COneVipGift;
// 
// class VipEveryDayRewardCell : public UIScene
// {
// public:
// 	VipEveryDayRewardCell();
// 	~VipEveryDayRewardCell();
// 	static VipEveryDayRewardCell* create(COneVipGift * oneVipGift);
// 	bool init(COneVipGift * oneVipGift);
// 
// 	void getRewardEvent(Ref *pSender);
// 
// public:
// 	COneVipGift * curOneVipGift;
// };



#endif

