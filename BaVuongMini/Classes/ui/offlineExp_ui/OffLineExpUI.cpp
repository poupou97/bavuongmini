#include "OffLineExpUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "AppMacros.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/vip_ui/VipDetailUI.h"
#include "../../messageclient/element/COfflineExpPuf.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/vip_ui/VipData.h"
#include "cocostudio\CCSGUIReader.h"

#define kTagGreeFlag 444
#define kTagLabelValue_Normal 445
#define kTagLabelValue_Gold 446
#define kTagLabelValue_GoldInGot 447

OffLineExpUI::OffLineExpUI():
m_nGetExpType(1)
{
}

OffLineExpUI::~OffLineExpUI()
{
}

OffLineExpUI* OffLineExpUI::create()
{
	auto offLineExpUI = new OffLineExpUI();
	if (offLineExpUI && offLineExpUI->init())
	{
		offLineExpUI->autorelease();
		return offLineExpUI;
	}
	CC_SAFE_DELETE(offLineExpUI);
	return NULL;
}

bool OffLineExpUI::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		m_curRoleVipLevel = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel();

		auto panel_offLineExp =  (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/offlineexp_1.json");
		panel_offLineExp->setAnchorPoint(Vec2(0.0f,0.0f));
		panel_offLineExp->getVirtualRenderer()->setContentSize(Size(640, 465));
		panel_offLineExp->setPosition(Vec2::ZERO);
		panel_offLineExp->setTouchEnabled(true);
		m_pLayer->addChild(panel_offLineExp);

		u_layer = Layer::create();
		addChild(u_layer);

		auto Btn_close = (Button*)Helper::seekWidgetByName(panel_offLineExp,"Button_close");
		Btn_close->setTouchEnabled(true);
		Btn_close->setPressedActionEnabled(true);
		Btn_close->addTouchEventListener(CC_CALLBACK_2(OffLineExpUI::CloseEvent, this));

		auto Btn_vipBuy = (Button*)Helper::seekWidgetByName(panel_offLineExp,"Button_takeVip");
		Btn_vipBuy->setTouchEnabled(true);
		Btn_vipBuy->setPressedActionEnabled(true);
		Btn_vipBuy->addTouchEventListener(CC_CALLBACK_2(OffLineExpUI::VipBuyEvent, this));

		Btn_getExp = (Button*)Helper::seekWidgetByName(panel_offLineExp,"Button_takeExp");
		Btn_getExp->setTouchEnabled(true);
		Btn_getExp->setPressedActionEnabled(true);
		Btn_getExp->addTouchEventListener(CC_CALLBACK_2(OffLineExpUI::GetExpEvent, this));

		l_btn_des = (Text*)Helper::seekWidgetByName(panel_offLineExp,"Label_964");

		auto Btn_normal = (Button*)Helper::seekWidgetByName(panel_offLineExp,"Button_normal");
		Btn_normal->setTouchEnabled(true);
		//Btn_normal->setPressedActionEnabled(true);
		Btn_normal->addTouchEventListener(CC_CALLBACK_2(OffLineExpUI::NormalEvent, this));
		cb_normal = (CheckBox*)Helper::seekWidgetByName(Btn_normal,"CheckBox_normal");
		cb_normal->setSelected(true);

		auto Btn_gold = (Button*)Helper::seekWidgetByName(panel_offLineExp,"Button_gold");
		Btn_gold->setTouchEnabled(true);
		//Btn_gold->setPressedActionEnabled(true);
		Btn_gold->addTouchEventListener(CC_CALLBACK_2(OffLineExpUI::GoldEvent, this));
		cb_gold = (CheckBox*)Helper::seekWidgetByName(panel_offLineExp,"CheckBox_gold");
		cb_gold->setSelected(false);
		l_gold = (Text*)Helper::seekWidgetByName(panel_offLineExp,"Label_coinsValue");
		char s_goldValue[20];
		sprintf(s_goldValue,"%d",GameView::getInstance()->m_nRequiredGoldToGetExp);
		l_gold->setString(s_goldValue);

		auto Btn_goldInGot = (Button*)Helper::seekWidgetByName(panel_offLineExp,"Button_goldingot");
		Btn_goldInGot->setTouchEnabled(true);
		//Btn_goldInGot->setPressedActionEnabled(true);
		Btn_goldInGot->addTouchEventListener(CC_CALLBACK_2(OffLineExpUI::GoldInGotEvent, this));
		cb_goldInGot = (CheckBox*)Helper::seekWidgetByName(panel_offLineExp,"CheckBox_goldingot");
		cb_goldInGot->setSelected(false);
		l_goldInGot = (Text*)Helper::seekWidgetByName(panel_offLineExp,"Label_goldValue");
		char s_goldInGotValue[20];
		sprintf(s_goldInGotValue,"%d",GameView::getInstance()->m_nRequiredGoldInGotToGetExp);
		l_goldInGot->setString(s_goldInGotValue);

		m_tableView = TableView::create(this,Size(497,207));
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0,0));
		m_tableView->setPosition(Vec2(70,89));
		m_tableView->setContentOffset(Vec2(0,0));
		m_tableView->setDelegate(this);;
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		u_layer->addChild(m_tableView);

		setToDefault();

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(640,465));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(OffLineExpUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(OffLineExpUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(OffLineExpUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(OffLineExpUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}



void OffLineExpUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void OffLineExpUI::onExit()
{
	UIScene::onExit();
}

bool OffLineExpUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	return true;
}

void OffLineExpUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void OffLineExpUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void OffLineExpUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void OffLineExpUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void OffLineExpUI::VipBuyEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		auto vipScene = (Layer*)mainscene->getChildByTag(kTagVipDetailUI);
		if (vipScene == NULL)
		{
			if (VipData::vipDetailUI)
			{
				auto vipDetailUI = (VipDetailUI*)VipData::vipDetailUI;
				mainscene->addChild(vipDetailUI, 0, kTagVipDetailUI);
				vipDetailUI->setIgnoreAnchorPointForPosition(false);
				vipDetailUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				vipDetailUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				vipDetailUI->setToDefault();
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void OffLineExpUI::GetExpEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto temp = GameView::getInstance()->m_offLineExpPuf.at(m_curRoleVipLevel);
		if (!temp)
			return;

		switch (m_nGetExpType)
		{
		case 1:
		{
			if (temp->normal() <= 0)
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("vip_reward_Geted"));
			}
			else
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(2809, (void *)m_nGetExpType);
			}
		}
		break;
		case 2:
		{
			if (temp->gold() <= 0)
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("vip_reward_Geted"));
			}
			else
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(2809, (void *)m_nGetExpType);
			}
		}
		break;
		case 3:
		{
			if (temp->goldingot() <= 0)
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("vip_reward_Geted"));
			}
			else
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(2809, (void *)m_nGetExpType);
			}
		}
		break;
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void OffLineExpUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void OffLineExpUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void OffLineExpUI::tableCellTouched( TableView* table, TableViewCell* cell )
{

}

cocos2d::Size OffLineExpUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(490,34);
}

cocos2d::extension::TableViewCell* OffLineExpUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto temp = GameView::getInstance()->m_offLineExpPuf.at(idx);

	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell=new TableViewCell();
	cell->autorelease();

	Sprite * sprite_levelFrame;

	if (m_curRoleVipLevel == idx)
	{
		sprite_levelFrame = Sprite::create("res_ui/new_button_12.png");
		sprite_levelFrame->setAnchorPoint(Vec2(0,0));
		sprite_levelFrame->setPosition(Vec2(0,0));
		sprite_levelFrame->setScaleX(1.3f);
		sprite_levelFrame->setScaleY(0.9f);
		cell->addChild(sprite_levelFrame);
	}
	else
	{
		sprite_levelFrame = Sprite::create("res_ui/new_button_9.png");
		sprite_levelFrame->setAnchorPoint(Vec2(0,0));
		sprite_levelFrame->setPosition(Vec2(0,0));
		sprite_levelFrame->setScaleX(1.3f);
		sprite_levelFrame->setScaleY(0.9f);
		cell->addChild(sprite_levelFrame);
	}

	std::string str_vipLevel;
	if (idx == 0)
	{
		str_vipLevel = StringDataManager::getString("offLineExpUI_vip0");
	}
	else
	{
		str_vipLevel = "VIP";
		char s_lv[10];
		sprintf(s_lv,"%d",idx);
		str_vipLevel.append(s_lv);
	}
	auto lbt_vipLevel = Label::createWithBMFont("res_ui/font/ziti_3.fnt", str_vipLevel.c_str());
	lbt_vipLevel->setAnchorPoint(Vec2(0.5f,0.5f));
	lbt_vipLevel->setPosition(Vec2(sprite_levelFrame->getContentSize().width/2*sprite_levelFrame->getScaleX(),sprite_levelFrame->getContentSize().height*sprite_levelFrame->getScaleY()/2));
	cell->addChild(lbt_vipLevel);

	//��ɫ�
	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(5, 5, 1, 1) , "res_ui/mengban_green2.png");
	pHighlightSpr->setPreferredSize(Size(132,29));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setTag(kTagGreeFlag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	auto sprite_di = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_di->setPreferredSize(Size(401,31));
	sprite_di->setCapInsets(Rect(7.5f,13.5f,1,1));
	sprite_di->setAnchorPoint(Vec2(0,0));
	sprite_di->setPosition(Vec2(95,0));
	cell->addChild(sprite_di);
	auto sprite_di_1 = Sprite::create("res_ui/LV3_dikuang00.png");
	sprite_di_1->setAnchorPoint(Vec2(0,0));
	sprite_di_1->setPosition(Vec2(229,0));
	cell->addChild(sprite_di_1);
	auto sprite_di_2 = Sprite::create("res_ui/LV3_dikuang00.png");
	sprite_di_2->setAnchorPoint(Vec2(0,0));
	sprite_di_2->setPosition(Vec2(362,0));
	cell->addChild(sprite_di_2);

	auto l_normalExp = Label::createWithTTF("",APP_FONT_NAME,18);
	l_normalExp->setAnchorPoint(Vec2(0.5f,0.5f));
	l_normalExp->setPosition(Vec2(159,15));
	//l_normalExp->setColor(Color3B(49,93,11));
	l_normalExp->setTag(kTagLabelValue_Normal);
	cell->addChild(l_normalExp);
	if (temp->normal()<0)
	{
		l_normalExp->setString(StringDataManager::getString("offLineExpUI_canNotGetExp"));
	}
	else
	{
		char s_normalExp [20];
		sprintf(s_normalExp,"%d",temp->normal());
		l_normalExp->setString(s_normalExp);
	}

	
	auto l_goldExp = Label::createWithTTF("",APP_FONT_NAME,18);
	l_goldExp->setAnchorPoint(Vec2(0.5f,0.5f));
	l_goldExp->setPosition(Vec2(295,15));
	//l_goldExp->setColor(Color3B(49,93,13));
	l_goldExp->setTag(kTagLabelValue_Gold);
	cell->addChild(l_goldExp);
	if (temp->gold()<0)
	{
		l_goldExp->setString(StringDataManager::getString("offLineExpUI_canNotGetExp"));
	}
	else
	{
		char s_goldExp [20];
		sprintf(s_goldExp,"%d",temp->gold());
		l_goldExp->setString(s_goldExp);
	}

	auto l_goldInGotExp = Label::createWithTTF("",APP_FONT_NAME,18);
	l_goldInGotExp->setAnchorPoint(Vec2(0.5f,0.5f));
	l_goldInGotExp->setPosition(Vec2(431,15));
	//l_goldInGotExp->setColor(Color3B(49,93,13));
	l_goldInGotExp->setTag(kTagLabelValue_GoldInGot);
	cell->addChild(l_goldInGotExp);
	if (temp->goldingot()<0)
	{
		l_goldInGotExp->setString(StringDataManager::getString("offLineExpUI_canNotGetExp"));
	}
	else
	{
		char s_goldInGotExp [20];
		sprintf(s_goldInGotExp,"%d",temp->goldingot());
		l_goldInGotExp->setString(s_goldInGotExp);
	}

	if (m_curRoleVipLevel == idx)
	{
		pHighlightSpr->setVisible(true);
		switch(m_nGetExpType)
		{
		case 1:
			{
				pHighlightSpr->setPosition(Vec2(100,1));
				l_normalExp->setColor(Color3B(255,0,0));
			}
			break;
		case 2:
			{
				pHighlightSpr->setPosition(Vec2(232,1));
				l_goldExp->setColor(Color3B(255,0,0));
			}
			break;
		case 3:
			{
				pHighlightSpr->setPosition(Vec2(364,1));
				l_goldInGotExp->setColor(Color3B(255,0,0));
			}
			break;
		}
	}

	return cell;
}

ssize_t OffLineExpUI::numberOfCellsInTableView( TableView *table )
{
	return GameView::getInstance()->m_offLineExpPuf.size();
}

void OffLineExpUI::refreshTableViewWithOutChangeOffSet()
{
	Vec2 _s = m_tableView->getContentOffset();
	int _h = m_tableView->getContentSize().height + _s.y;
	m_tableView->reloadData();
	Vec2 temp = Vec2(_s.x,_h - m_tableView->getContentSize().height);
	m_tableView->setContentOffset(temp); 
}

void OffLineExpUI::NormalEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto temp = GameView::getInstance()->m_offLineExpPuf.at(m_curRoleVipLevel);
		if (!temp)
			return;

		if (temp->normal() < 0)
		{
			std::string str_dialog;
			if (m_curRoleVipLevel <= 0)
			{
				str_dialog.append(StringDataManager::getString("offLineExpUI_vip0"));
			}
			else
			{
				str_dialog.append("VIP");
				char s_viplevel[20];
				sprintf(s_viplevel, "%d", m_curRoleVipLevel);
				str_dialog.append(s_viplevel);

			}
			str_dialog.append(StringDataManager::getString("offLineExpUI_canNotSelectIt"));
			GameView::getInstance()->showAlertDialog(str_dialog.c_str());
			return;
		}

		m_nGetExpType = 1;

		cb_normal->setSelected(true);
		cb_gold->setSelected(false);
		cb_goldInGot->setSelected(false);
		RefreshSelectStaus();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void OffLineExpUI::GoldEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto temp = GameView::getInstance()->m_offLineExpPuf.at(m_curRoleVipLevel);
		if (!temp)
			return;

		if (temp->gold() < 0)
		{
			std::string str_dialog;
			if (m_curRoleVipLevel <= 0)
			{
				str_dialog.append(StringDataManager::getString("offLineExpUI_vip0"));
			}
			else
			{
				str_dialog.append("VIP");
				char s_viplevel[20];
				sprintf(s_viplevel, "%d", m_curRoleVipLevel);
				str_dialog.append(s_viplevel);

			}
			str_dialog.append(StringDataManager::getString("offLineExpUI_canNotSelectIt"));
			GameView::getInstance()->showAlertDialog(str_dialog.c_str());
			return;
		}

		m_nGetExpType = 2;

		cb_normal->setSelected(false);
		cb_gold->setSelected(true);
		cb_goldInGot->setSelected(false);
		RefreshSelectStaus();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void OffLineExpUI::GoldInGotEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto temp = GameView::getInstance()->m_offLineExpPuf.at(m_curRoleVipLevel);
		if (!temp)
			return;

		if (temp->goldingot() < 0)
		{
			std::string str_dialog;
			if (m_curRoleVipLevel <= 0)
			{
				str_dialog.append(StringDataManager::getString("offLineExpUI_vip0"));
			}
			else
			{
				str_dialog.append("VIP");
				char s_viplevel[20];
				sprintf(s_viplevel, "%d", m_curRoleVipLevel);
				str_dialog.append(s_viplevel);

			}
			str_dialog.append(StringDataManager::getString("offLineExpUI_canNotSelectIt"));
			GameView::getInstance()->showAlertDialog(str_dialog.c_str());
			return;
		}

		m_nGetExpType = 3;

		cb_normal->setSelected(false);
		cb_gold->setSelected(false);
		cb_goldInGot->setSelected(true);
		RefreshSelectStaus();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void OffLineExpUI::RefreshSelectStaus()
{
	auto cell = m_tableView->cellAtIndex(m_curRoleVipLevel);
	if (cell)
	{
		switch(m_nGetExpType)
		{
		case 1:
			{
				cell->getChildByTag(kTagGreeFlag)->setPosition(Vec2(97,1));
				auto lb_normal = dynamic_cast<Label*>(cell->getChildByTag(kTagLabelValue_Normal));
				if (lb_normal)
				{
					lb_normal->setColor(Color3B(255,0,0));
				}
				auto lb_gold = dynamic_cast<Label*>(cell->getChildByTag(kTagLabelValue_Gold));
				if (lb_gold)
				{
					lb_gold->setColor(Color3B(255,255,255));
				}
				auto lb_goldInGot = dynamic_cast<Label*>(cell->getChildByTag(kTagLabelValue_GoldInGot));
				if (lb_goldInGot)
				{
					lb_goldInGot->setColor(Color3B(255,255,255));
				}
			}
			break;
		case 2:
			{
				cell->getChildByTag(kTagGreeFlag)->setPosition(Vec2(231,1));
				auto lb_normal = dynamic_cast<Label*>(cell->getChildByTag(kTagLabelValue_Normal));
				if (lb_normal)
				{
					lb_normal->setColor(Color3B(255,255,255));
				}
				auto lb_gold = dynamic_cast<Label*>(cell->getChildByTag(kTagLabelValue_Gold));
				if (lb_gold)
				{
					lb_gold->setColor(Color3B(255,0,0));
				}
				auto lb_goldInGot = dynamic_cast<Label*>(cell->getChildByTag(kTagLabelValue_GoldInGot));
				if (lb_goldInGot)
				{
					lb_goldInGot->setColor(Color3B(255,255,255));
				}
			}
			break;
		case 3:
			{
				cell->getChildByTag(kTagGreeFlag)->setPosition(Vec2(363,1));
				auto lb_normal = dynamic_cast<Label*>(cell->getChildByTag(kTagLabelValue_Normal));
				if (lb_normal)
				{
					lb_normal->setColor(Color3B(255,255,255));
				}
				auto lb_gold = dynamic_cast<Label*>(cell->getChildByTag(kTagLabelValue_Gold));
				if (lb_gold)
				{
					lb_gold->setColor(Color3B(255,255,255));
				}
				auto lb_goldInGot = dynamic_cast<Label*>(cell->getChildByTag(kTagLabelValue_GoldInGot));
				if (lb_goldInGot)
				{
					lb_goldInGot->setColor(Color3B(255,0,0));
				}
			}
			break;
		}

		auto temp = GameView::getInstance()->m_offLineExpPuf.at(m_curRoleVipLevel);
		if ( temp )
		{
			if (temp->m_bIsGeted)
			{
				Btn_getExp->loadTextures("res_ui/new_button_001.png","res_ui/new_button_001.png","");
				Btn_getExp->setTouchEnabled(false);
				l_btn_des->setString(StringDataManager::getString("vip_reward_Geted"));
			}
			else
			{
				Btn_getExp->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				Btn_getExp->setTouchEnabled(true);
				l_btn_des->setString(StringDataManager::getString("offLineExpUI_getExp"));
			}
		}
	}
}

void OffLineExpUI::RefreshUI()
{
	refreshTableViewWithOutChangeOffSet();

	char s_goldValue[20];
	sprintf(s_goldValue,"%d",GameView::getInstance()->m_nRequiredGoldToGetExp);
	l_gold->setString(s_goldValue);

	char s_goldInGotValue[20];
	sprintf(s_goldInGotValue,"%d",GameView::getInstance()->m_nRequiredGoldInGotToGetExp);
	l_goldInGot->setString(s_goldInGotValue);

	RefreshSelectStaus();
}

void OffLineExpUI::setToDefault()
{
	auto temp = GameView::getInstance()->m_offLineExpPuf.at(m_curRoleVipLevel);
	if (!temp)
		return;

	if (temp->goldingot() >= 0)
	{
		m_nGetExpType = 3;

		cb_normal->setSelected(false);
		cb_gold->setSelected(false);
		cb_goldInGot->setSelected(true);
		RefreshSelectStaus();
	}
	else if (temp->gold() >= 0)
	{
		m_nGetExpType = 2;

		cb_normal->setSelected(false);
		cb_gold->setSelected(true);
		cb_goldInGot->setSelected(false);
		RefreshSelectStaus();
	}
	else
	{
		m_nGetExpType = 1;

		cb_normal->setSelected(true);
		cb_gold->setSelected(false);
		cb_goldInGot->setSelected(false);
		RefreshSelectStaus();
	}
}

void OffLineExpUI::setCurRoleVipLevel( int lv )
{
	m_curRoleVipLevel = lv;
}


///////////////////////////////////////////////////////////////////////////////////////////////////

// VipEveryDayRewardCell::VipEveryDayRewardCell()
// {
// 
// }
// 
// VipEveryDayRewardCell::~VipEveryDayRewardCell()
// {
// 	delete curOneVipGift;
// }
// 
// VipEveryDayRewardCell* VipEveryDayRewardCell::create( COneVipGift * oneVipGift )
// {
// 	VipEveryDayRewardCell * vipEveryDayRewardCell = new VipEveryDayRewardCell();
// 	if (vipEveryDayRewardCell && vipEveryDayRewardCell->init(oneVipGift))
// 	{
// 		vipEveryDayRewardCell->autorelease();
// 		return vipEveryDayRewardCell;
// 	}
// 	CC_SAFE_DELETE(vipEveryDayRewardCell);
// 	return NULL;
// }
// 
// bool VipEveryDayRewardCell::init( COneVipGift * oneVipGift )
// {
// 	if (UIScene::init())
// 	{
// 		m_pLayer->setSwallowsTouches(false);
// 
// 		curOneVipGift = new COneVipGift();
// 		curOneVipGift->CopyFrom(*oneVipGift);
// 
// 		// 		ImageView * image_frame = ImageView::create();
// 		// 		image_frame->loadTexture("res_ui/highlight.png");
// 		// 		image_frame->setScale9Enabled(true);
// 		// 		image_frame->setContentSize(Size(366,71));
// 		// 		image_frame->setAnchorPoint(Vec2(0.5f,0.5f));
// 		// 		image_frame->setPosition(Vec2(366/2,71/2));
// 		// 		m_pLayer->addChild(image_frame);
// 		// 		
// 		ImageView * image_line = ImageView::create();
// 		image_line->loadTexture("res_ui/henggang_red.png");
// 		image_line->setAnchorPoint(Vec2(0.5f,0.5f));
// 		image_line->setPosition(Vec2(366/2,3));
// 		image_line->setScaleX(2.3f);
// 		m_pLayer->addChild(image_line);
// 
// 		ImageView * imageView_icon = ImageView::create();
// 		imageView_icon->loadTexture("res_ui/vip/vip_b.png");
// 		imageView_icon->setAnchorPoint(Vec2(0.5f,0.5f));
// 		imageView_icon->setPosition(Vec2(40,35));
// 		m_pLayer->addChild(imageView_icon);
// 
// 		char str_level[20];
// 		sprintf(str_level,"%d",oneVipGift->number());
// 		Label * l_vipLevel = Label::create();
// 		l_vipLevel->setFntFile("res_ui/font/ziti_3.fnt");
// 		l_vipLevel->setText(str_level);
// 		l_vipLevel->setScale(0.9f);
// 		l_vipLevel->setAnchorPoint(Vec2(0.5f,0.5f));
// 		l_vipLevel->setPosition(Vec2(12,-2));
// 		imageView_icon->addChild(l_vipLevel);
// 
// 		for(int j = 0;j<oneVipGift->goods_size();++j)
// 		{
// 			if (j >= 2)
// 				continue;
// 
// 			GoodsInfo * goodInfo = new GoodsInfo();
// 			goodInfo->CopyFrom(oneVipGift->goods(j));
// 			int num = 0;
// 			if (j < oneVipGift->goodsnumber_size())
// 				num = oneVipGift->goodsnumber(j);
// 
// 			VipRewardCellItem * goodsItem = VipRewardCellItem::create(goodInfo,num);
// 			goodsItem->setIgnoreAnchorPointForPosition(false);
// 			goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
// 			goodsItem->setPosition(Vec2(116+79*j,35));
// 			m_pLayer->addChild(goodsItem);
// 
// 			delete goodInfo;
// 		}
// 
// 		switch(oneVipGift->status())
// 		{
// 		case 1:   //ײ�����ȡ
// 			{
// 				ImageView * imageView_di = ImageView::create();
// 				imageView_di->loadTexture("res_ui/zhezhao_btn.png");
// 				imageView_di->setAnchorPoint(Vec2(0.5f,0.5f));
// 				imageView_di->setPosition(Vec2(306,35));
// 				m_pLayer->addChild(imageView_di);
// 
// 				std::string str_des = "VIP";
// 				char s_level[20];
// 				sprintf(s_level,"%d",oneVipGift->number());
// 				str_des.append(s_level);
// 				str_des.append(StringDataManager::getString("vip_reward_cannotGet"));
// 				Label *l_des = Label::create();
// 				l_des->setFontName(APP_FONT_NAME);
// 				l_des->setFontSize(18);
// 				l_des->setText(str_des.c_str());
// 				l_des->setAnchorPoint(Vec2(0.5f,0.5f));
// 				l_des->setPosition(Vec2(0,0));
// 				imageView_di->addChild(l_des);
// 			}
// 			break;
// 		case 2:   //����ȡ
// 			{
// 				Button * btn_get = Button::create();
// 				btn_get->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
// 				btn_get->setScale9Enabled(true);
// 				btn_get->setContentSize(Size(108,43));
// 				btn_get->setCapInsets(Rect(18,9,2,23));
// 				btn_get->setAnchorPoint(Vec2(0.5f,0.5f));
// 				btn_get->setPosition(Vec2(306,35));
// 				btn_get->setTouchEnabled(true);
// 				btn_get->setPressedActionEnabled(true);
// 				btn_get->addTouchEventListener(CC_CALLBACK_2(VipEveryDayRewardCell::getRewardEvent));
// 				m_pLayer->addChild(btn_get);
// 
// 				Label *l_get = Label::create();
// 				l_get->setFontName(APP_FONT_NAME);
// 				l_get->setFontSize(18);
// 				l_get->setText(StringDataManager::getString("btn_lingqu"));
// 				l_get->setAnchorPoint(Vec2(0.5f,0.5f));
// 				l_get->setPosition(Vec2(0,0));
// 				btn_get->addChild(l_get);
// 			}
// 			break;
// 		case 3:   //����ȡ
// 			{
// 				ImageView * imageView_di = ImageView::create();
// 				imageView_di->loadTexture("res_ui/zhezhao_btn.png");
// 				imageView_di->setAnchorPoint(Vec2(0.5f,0.5f));
// 				imageView_di->setPosition(Vec2(306,35));
// 				m_pLayer->addChild(imageView_di);
// 				Label *l_des = Label::create();
// 				l_des->setFontName(APP_FONT_NAME);
// 				l_des->setFontSize(18);
// 				l_des->setText(StringDataManager::getString("vip_reward_Geted"));
// 				l_des->setAnchorPoint(Vec2(0.5f,0.5f));
// 				l_des->setPosition(Vec2(0,0));
// 				imageView_di->addChild(l_des);
// 			}
// 			break;
// 		}
// 
// 		this->setContentSize(Size(350,76));
// 
// 		return true;
// 	}
// 	return false;
// }
// 
// void VipEveryDayRewardCell::getRewardEvent( Ref *pSender )
// {
// 	//req to get reward
// 	GameMessageProcessor::sharedMsgProcessor()->sendReq(5119);
// }
// 
