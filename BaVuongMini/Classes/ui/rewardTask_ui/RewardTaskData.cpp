#include "RewardTaskData.h"
#include "../../messageclient/element/CBoardMissionInfo.h"

RewardTaskData * RewardTaskData::s_rewardTaskData = NULL;

RewardTaskData::RewardTaskData(void):
m_nRefreshType(0),
m_nTargetNpcId(0)
{
}


RewardTaskData::~RewardTaskData(void)
{
}

RewardTaskData * RewardTaskData::getInstance()
{
	if (s_rewardTaskData == NULL)
	{
		s_rewardTaskData = new RewardTaskData();
	}

	return s_rewardTaskData;
}

int RewardTaskData::getRemainTaskNum()
{
	return m_nRemainTaskNum;
}

void RewardTaskData::setRemainTaskNum( int _value )
{
	m_nRemainTaskNum = _value;
}

int RewardTaskData::getAllTTaskNum()
{
	return m_nAllTaskNum;
}

void RewardTaskData::setAllTTaskNum( int _value )
{
	m_nAllTaskNum = _value;
}

long long RewardTaskData::getRemainCDTime()
{
	return m_nRemainCDTime;
}

void RewardTaskData::setRemainCDTime( long long _value )
{
	m_nRemainCDTime = _value;
}

void RewardTaskData::clearTaskList()
{
	std::vector<CBoardMissionInfo*>::iterator iter;
	for (iter = p_taskList.begin(); iter != p_taskList.end(); ++iter)
	{
		delete *iter;
	}
	p_taskList.clear();
}

int RewardTaskData::getCurRefreshType()
{
	return m_nRefreshType;
}

void RewardTaskData::setCurRefreshType( int type )
{
	m_nRefreshType = type;
}

long long RewardTaskData::getTargetNpcId()
{
	return m_nTargetNpcId;
}

void RewardTaskData::setTargetNpcId( long long _value )
{
	m_nTargetNpcId = _value;
}
