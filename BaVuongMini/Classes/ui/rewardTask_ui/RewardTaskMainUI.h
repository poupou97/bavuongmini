#ifndef _UI_REWARDTASKUI_REWARDTASKMAINUI_H_
#define _UI_REWARDTASKUI_REWARDTASKMAINUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

#define kTag_RewardTaskItem_Base 200
#define kTag_RefreshAction_deleteOld 210
#define kTag_RefreshAction_addNew 220

#define RewardTastItem_First_Pos_x 70
#define RewardTastItem_First_Pos_y 166
#define RewardTastItem_space 133

/////////////////////////////////
/**
 * �������UI
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.08.21
 */

class RewardTaskMainUI : public UIScene
{
public:
	RewardTaskMainUI(void);
	~RewardTaskMainUI(void);

public:
	static RewardTaskMainUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void update(float dt);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void RefreshEvent(Ref *pSender, Widget::TouchEventType type);
	void RefreshFiveStarEvent(Ref *pSender, Widget::TouchEventType type);

	//���ʾ5���������
	void RefreshFiveMissionPresent();
	void RefreshFiveMissionPresentWithAnm();

	void RefreshMissionState(int index,int state);
	//�ˢ�½���ʣ��������ʾ
	void RefreshRemainTaskNum();
public:
	Layer * m_base_layer;																							// ��layer
	Layer * m_up_layer;
private:
	Text * l_remainTimeToFree;                                                                                //�ʣ��ʱ�
	Text * l_remainTaskNum;                                                                                   //�ʣ�����
	Text * l_refreshCostValue;                                                                                  //�ˢ����ģ�Ԫ����
	Text * l_refreshFiveStarCostValue;                                                                      //һ��������ģ�Ԫ����
	Text * l_freeThisTime;                                                                                        //����ˢ����

public:
	//ѽ�ѧ
	int mTutorialScriptInstanceId;
	virtual void registerScriptCommand(int scriptId);
	//ѡ���
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();
};

#endif

