#ifndef _UI_REWARDTASKUI_REWARDTASKDATAUI_H_
#define _UI_REWARDTASKUI_REWARDTASKDATAUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CBoardMissionInfo;

/////////////////////////////////
/**
 * ������������
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.08.21
 */

class RewardTaskData
{
public:
	RewardTaskData(void);
	~RewardTaskData(void);

public: 
	static RewardTaskData * s_rewardTaskData;
	static RewardTaskData * getInstance();

	void clearTaskList();

	int getRemainTaskNum();
	void setRemainTaskNum(int _value);
	int getAllTTaskNum();
	void setAllTTaskNum(int _value);
	long long getRemainCDTime();
	void setRemainCDTime(long long _value);
	int getCurRefreshType();
	void setCurRefreshType(int type);
	long long getTargetNpcId();
	void setTargetNpcId(long long _value);

private:
	//ʣ���������
	int m_nRemainTaskNum;
	//���������
	int m_nAllTaskNum;
	//CDʱ��
	long long m_nRemainCDTime;

	//ˢ������
	int m_nRefreshType;

	//����NPCid
	long long m_nTargetNpcId;

public:
	std::vector<CBoardMissionInfo*> p_taskList;

};

#endif
