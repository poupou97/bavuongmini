#include "RewardTaskItem.h"
#include "../../messageclient/element/CBoardMissionInfo.h"
#include "../../AppMacros.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "RewardTaskDetailUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../utils/StaticDataManager.h"
#include "RewardTaskMainUI.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"

#define  kTagRotateAnm 777

RewardTaskItem::RewardTaskItem():
m_nPos(-1)
{

}

RewardTaskItem::~RewardTaskItem()
{
	delete curBoardMission;
}

RewardTaskItem * RewardTaskItem::create(CBoardMissionInfo * missionInfo,int pos)
{
	auto rewardTaskItem = new RewardTaskItem();
	if (rewardTaskItem && rewardTaskItem->init(missionInfo,pos))
	{
		rewardTaskItem->autorelease();
		return rewardTaskItem;
	}
	CC_SAFE_DELETE(rewardTaskItem);
	return NULL;
}

bool RewardTaskItem::init(CBoardMissionInfo * missionInfo,int pos)
{
	if (UIScene::init())
	{
		m_nPos = pos;
		curBoardMission = new CBoardMissionInfo();
		curBoardMission->CopyFrom(*missionInfo);

		//�
		auto btn_frame = Button::create();
		btn_frame->loadTextures("res_ui/kuang01_new.png","res_ui/kuang01_new.png","");
		btn_frame->setTouchEnabled(true);
		btn_frame->setScale9Enabled(true);
		btn_frame->setContentSize(Size(131,202));
		btn_frame->setCapInsets(Rect(14,30,1,1));
		btn_frame->setAnchorPoint(Vec2(.5f,.5f));
		btn_frame->setPosition(Vec2(131/2,202/2));
		btn_frame->addTouchEventListener(CC_CALLBACK_2(RewardTaskItem::LookOverEvent, this));
		btn_frame->setPressedActionEnabled(true);
		m_pLayer->addChild(btn_frame);
		//����ֵ
		auto imageView_nameFrame = ImageView::create();
		imageView_nameFrame->loadTexture("res_ui/LV4_diaa.png");
		imageView_nameFrame->setScale9Enabled(true);
		imageView_nameFrame->setContentSize(Size(108,49));
		imageView_nameFrame->setCapInsets(Rect(11,11,1,1));
		imageView_nameFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_nameFrame->setPosition(Vec2(0,61));
		btn_frame->addChild(imageView_nameFrame);
 		//����
		auto l_name = Text::create(missionInfo->missionname().c_str(), APP_FONT_NAME, 18);
		l_name->setTextAreaSize(Size(90,0));
		l_name->setAnchorPoint(Vec2(0.5f,0.5f));
		l_name->setPosition(Vec2(0,61));
		btn_frame->addChild(l_name);
		//��Ǽ�
		for(int i=0;i<5;i++)
		{
			auto imageView_star = ImageView::create();
			imageView_star->loadTexture("");
			imageView_star->setAnchorPoint(Vec2(0.5f,0.5f));
			imageView_star->setPosition(Vec2(-131/2+20+23*i,19));
			btn_frame->addChild(imageView_star);

			if (i<missionInfo->star())
			{
				imageView_star->loadTexture("res_ui/star2.png");
				imageView_star->setScale(.6f);
			}
			else
			{
				imageView_star->loadTexture("res_ui/star_off.png");
				imageView_star->setScale(1.0f);
			}
		}
		// exp
		auto imageView_exp = ImageView::create();
		imageView_exp->loadTexture("res_ui/exp.png");
		imageView_exp->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_exp->setPosition(Vec2(28-131/2,-10));
		btn_frame->addChild(imageView_exp);
		imageView_exp->setScale(.8f);
		//exp value
		
		char s_exp [20];
		sprintf(s_exp,"%d",missionInfo->rewards().exp());
		auto l_expValue = Label::createWithTTF(s_exp, APP_FONT_NAME, 14);
		l_expValue->setAnchorPoint(Vec2(0.f,0.5f));
		l_expValue->setPosition(Vec2(imageView_exp->getPosition().x + imageView_exp->getContentSize().width/2,imageView_exp->getPosition().y));
		btn_frame->addChild(l_expValue);

		// gold
		auto imageView_gold = ImageView::create();
		imageView_gold->loadTexture("res_ui/coins.png");
		imageView_gold->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_gold->setPosition(Vec2(34-131/2,-27));
		btn_frame->addChild(imageView_gold);
		imageView_gold->setScale(.8f);
		//gold value
		
		char s_gold [20];
		sprintf(s_gold,"%d",missionInfo->rewards().gold());
		auto l_goldValue = Label::createWithTTF(s_gold, APP_FONT_NAME, 14);
		l_goldValue->setAnchorPoint(Vec2(0.f,0.5f));
		l_goldValue->setPosition(Vec2(imageView_gold->getPosition().x + imageView_gold->getContentSize().width/2+2,imageView_gold->getPosition().y));
		btn_frame->addChild(l_goldValue);

		imageView_mask = ImageView::create();
		imageView_mask->loadTexture("res_ui/zhezhao30.png");
		imageView_mask->setScale9Enabled(true);
		imageView_mask->setContentSize(Size(131,202));
		imageView_mask->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_mask->setPosition(Vec2(0,0));
		btn_frame->addChild(imageView_mask);
		imageView_mask->setVisible(false);

		//��ʾ����״̬�İ�ť
		btn_missionState = Button::create();
		btn_missionState->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		btn_missionState->setTouchEnabled(true);
		btn_missionState->setScale9Enabled(true);
		btn_missionState->setContentSize(Size(98,40));
		btn_missionState->setCapInsets(Rect(18,18,1,1));
		btn_missionState->setAnchorPoint(Vec2(.5f,.5f));
		btn_missionState->setPosition(Vec2(0,3-64));
		btn_missionState->setPressedActionEnabled(true);
		btn_missionState->addTouchEventListener(CC_CALLBACK_2(RewardTaskItem::MissionStateEvent, this));
		btn_frame->addChild(btn_missionState);

		l_missionState = Label::createWithTTF("jieshourenwu", APP_FONT_NAME, 18);
		l_missionState->setAnchorPoint(Vec2(0.5f,0.5f));
		l_missionState->setPosition(Vec2(0,0));
		btn_missionState->addChild(l_missionState);

		imageView_missionState = ImageView::create();
		imageView_missionState->loadTexture("");
		imageView_missionState->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_missionState->setPosition(Vec2(131/2,37));
		m_pLayer->addChild(imageView_missionState);
		imageView_missionState->setVisible(false);

		RefreshBtnMissionState(curBoardMission->state(),false);

		this->setContentSize(Size(131,202));
		return true;
	}
	return false;
}

void RewardTaskItem::MissionStateEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		switch (curBoardMission->state())
		{
		case dispatched:// �ѷ��
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(7015, (void *)curBoardMission->index());

			auto rewardTaskMainUI = (RewardTaskMainUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskMainUI);
			if (rewardTaskMainUI)
			{
				auto sc = ScriptManager::getInstance()->getScriptById(rewardTaskMainUI->mTutorialScriptInstanceId);
				if (sc != NULL)
					sc->endCommand(this);
			}
		}
		break;
		case accepted:// ��ѽ��
		{
			// 			for (int i = 0;i<GameView::getInstance()->missionManager->MissionList_MainScene.size();i++)
			// 			{
			// 				if (curBoardMission->packageid() == GameView::getInstance()->missionManager->MissionList_MainScene.at(i)->missionpackageid())
			// 				{
			// 					GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*GameView::getInstance()->missionManager->MissionList_MainScene.at(i));
			// 					GameView::getInstance()->missionManager->addMission(GameView::getInstance()->missionManager->curMissionInfo);
			// 					break;
			// 				}
			// 			}	

			char des[100];
			const char *strings = StringDataManager::getString("RewardTask_sureToFinishRightNow");
			sprintf(des, strings, curBoardMission->quickcompletegold());
			GameView::getInstance()->showPopupWindow(des, 2, this, SEL_CallFuncO(&RewardTaskItem::SureToFinishRightNow), NULL);
		}
		break;
		case done://�����
		{
			for (int i = 0; i<GameView::getInstance()->missionManager->MissionList_MainScene.size(); i++)
			{
				if (curBoardMission->packageid() == GameView::getInstance()->missionManager->MissionList_MainScene.at(i)->missionpackageid())
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(7007, (char *)curBoardMission->packageid().c_str());
					break;
				}
			}
		}
		break;
		case submitted:// ����ύ
		{

		}
		break;
		case abandoned:// �ѷ��
		{

		}
		break;
		case submitFailed:// ��ύʧ�
		{

		}
		break;
		case canceled://  ���ȡ�
		{

		}
		break;
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RewardTaskItem::LookOverEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void*)curPartner->role().rolebase().roleid());
		if (!GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskDetailUI))
		{
			auto winSize = Director::getInstance()->getVisibleSize();
			auto detailUI = RewardTaskDetailUI::create(curBoardMission);
			detailUI->setIgnoreAnchorPointForPosition(false);
			detailUI->setAnchorPoint(Vec2(.5f, .5f));
			detailUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			detailUI->setTag(kTagRewardTaskDetailUI);
			GameView::getInstance()->getMainUIScene()->addChild(detailUI);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

int RewardTaskItem::getCurPos()
{
	return m_nPos;
}

void RewardTaskItem::RefreshBtnMissionState(int state,bool isAnm)
{
	btn_missionState->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");

	switch(state)
	{
	case dispatched:// ��ѷ��
		{
			curBoardMission->set_state(dispatched);

			btn_missionState->setVisible(true);
			imageView_missionState->setVisible(false);
			imageView_mask->setVisible(false);
			l_missionState->setString(StringDataManager::getString("RewardTaskItem_missionstate_get"));
		}
		break;
	case accepted:// ��ѽ��
		{
			curBoardMission->set_state(accepted);

			btn_missionState->setVisible(true);
			imageView_missionState->setVisible(false);
			imageView_mask->setVisible(false);
			l_missionState->setString(StringDataManager::getString("RewardTaskItem_missionstate_finishRightNow"));
		}
		break;
	case done://�����
		{
			curBoardMission->set_state(done);

			btn_missionState->setVisible(true);
			btn_missionState->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
			imageView_missionState->setVisible(false);
			imageView_mask->setVisible(false);
			l_missionState->setString(StringDataManager::getString("RewardTaskItem_missionstate_finish"));
		}
		break;
	case submitted:// ����ύ
		{
			curBoardMission->set_state(submitted);

			btn_missionState->setVisible(false);
			imageView_missionState->setVisible(true);
			imageView_mask->setVisible(true);
			imageView_missionState->loadTexture("res_ui/offerReward/dacheng.png");
			if (isAnm)
			{
				imageView_missionState->setScale(3.0f);
				imageView_missionState->setVisible(true);
				FiniteTimeAction*  action = Sequence::create(
					CCEaseElasticIn::create(ScaleTo::create(0.7f,1.0f)),
					NULL);
				imageView_missionState->runAction(action);
			}
		}
		break;
	case abandoned:// �ѷ��
		{
			curBoardMission->set_state(abandoned);
			
			btn_missionState->setVisible(false);
			imageView_missionState->setVisible(true);
			imageView_mask->setVisible(true);
			imageView_missionState->loadTexture("res_ui/offerReward/fangqi.png");
			if (isAnm)
			{
				imageView_missionState->setScale(3.0f);
				imageView_missionState->setVisible(true);
				FiniteTimeAction*  action = Sequence::create(
					CCEaseElasticIn::create(ScaleTo::create(0.7f,1.0f)),
					NULL);
				imageView_missionState->runAction(action);
			}
		}
		break;
	case submitFailed:// ��ύʧ�
		{
			curBoardMission->set_state(submitFailed);
			//l_missionState->setText("tijiaoshibai");
		}
		break;
	case canceled://  ���ȡ�
		{
			curBoardMission->set_state(canceled);
			
			btn_missionState->setVisible(false);
			imageView_missionState->setVisible(true);
			imageView_mask->setVisible(true);
			imageView_missionState->loadTexture("res_ui/offerReward/fangqi.png");
			if (isAnm)
			{
				imageView_missionState->setScale(3.0f);
				imageView_missionState->setVisible(true);
				auto action = Sequence::create(
					CCEaseElasticIn::create(ScaleTo::create(0.7f,1.0f)),
					NULL);

				imageView_missionState->runAction(action);
			}
		}
		break;
	}
}

void RewardTaskItem::SureToFinishRightNow( Ref *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(7023);
}


