#include "RewardTaskDetailUI.h"
#include "../../messageclient/element/CBoardMissionInfo.h"
#include "../../AppMacros.h"
#include "../extensions/CCRichLabel.h"
#include "../backpackscene/EquipmentItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../missionscene/RewardItem.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../utils/GameUtils.h"
#include "GameAudio.h"
#include "RewardTaskData.h"
#include "../../utils/StaticDataManager.h"
#include "cocostudio\CCSGUIReader.h"

RewardTaskDetailUI::RewardTaskDetailUI(void)
{
}


RewardTaskDetailUI::~RewardTaskDetailUI(void)
{
	delete curBoardMissionInfo;
}

RewardTaskDetailUI* RewardTaskDetailUI::create(CBoardMissionInfo * boardMissionInfo)
{
	auto rewardTaskDetailUI = new RewardTaskDetailUI();
	if (rewardTaskDetailUI && rewardTaskDetailUI->init(boardMissionInfo))
	{
		rewardTaskDetailUI->autorelease();
		return rewardTaskDetailUI;
	}
	CC_SAFE_DELETE(rewardTaskDetailUI);
	return NULL;
}

bool RewardTaskDetailUI::init(CBoardMissionInfo * boardMissionInfo)
{
	if (UIScene::init())
	{
		curBoardMissionInfo = new CBoardMissionInfo();
		curBoardMissionInfo->CopyFrom(*boardMissionInfo);

		Size winSize = Director::getInstance()->getVisibleSize();

		m_base_layer = Layer::create();
		this->addChild(m_base_layer);	

		auto s_panel_rewardTaskInfo = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/offerReward_popupo_1.json");
		s_panel_rewardTaskInfo->setTouchEnabled(true);
		m_pLayer->addChild(s_panel_rewardTaskInfo);

		auto btn_close = (Button*)Helper::seekWidgetByName(s_panel_rewardTaskInfo,"Button_close");
		btn_close->setTouchEnabled(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(RewardTaskDetailUI::CloseEvent, this));

		btn_getMission = (Button*)Helper::seekWidgetByName(s_panel_rewardTaskInfo,"Button_getMission");
		btn_getMission->setTouchEnabled(true);
		btn_getMission->setPressedActionEnabled(true);
		btn_getMission->addTouchEventListener(CC_CALLBACK_2(RewardTaskDetailUI::GetMissionEvent, this));

		btn_finishMission = (Button*)Helper::seekWidgetByName(s_panel_rewardTaskInfo,"Button_finishMission");
		btn_finishMission->setTouchEnabled(true);
		btn_finishMission->setPressedActionEnabled(true);
		btn_finishMission->addTouchEventListener(CC_CALLBACK_2(RewardTaskDetailUI::FinishMissionEvent, this));

		btn_finishRightNow = (Button*)Helper::seekWidgetByName(s_panel_rewardTaskInfo,"Button_finishRightNow");
		btn_finishRightNow->setTouchEnabled(true);
		btn_finishRightNow->setPressedActionEnabled(true);
		btn_finishRightNow->addTouchEventListener(CC_CALLBACK_2(RewardTaskDetailUI::FinishRightNowEvent, this));

		btn_autoFindPath = (Button*)Helper::seekWidgetByName(s_panel_rewardTaskInfo,"Button_autoFindPath");
		btn_autoFindPath->setTouchEnabled(true);
		btn_autoFindPath->setPressedActionEnabled(true);
		btn_autoFindPath->addTouchEventListener(CC_CALLBACK_2(RewardTaskDetailUI::AutoFindPathEvent, this));

		btn_giveUpMission = (Button*)Helper::seekWidgetByName(s_panel_rewardTaskInfo,"Button_giveUpMission");
		btn_giveUpMission->setTouchEnabled(true);
		btn_giveUpMission->setPressedActionEnabled(true);
		btn_giveUpMission->addTouchEventListener(CC_CALLBACK_2(RewardTaskDetailUI::GiveUpMissionEvent, this));

		refreshBtnState(curBoardMissionInfo->state());

		//task info 
		int scroll_height = 0;
		int _space = 15;
		//name
		auto sprite_name = Sprite::create("res_ui/renwubb/renwumingcheng.png");
		auto label_name = Label::createWithTTF(boardMissionInfo->missionname().c_str(),APP_FONT_NAME,18,Size(270, 0 ), TextHAlignment::LEFT);
		int zeroLineHeight = label_name->getContentSize().height+_space;
		//star
		auto sprite_star= Sprite::create("res_ui/renwubb/renwuxingji.png");
		char s_star[10];
		sprintf(s_star,"%d",boardMissionInfo->star());
		//Label* label_star = Label::createWithTTF(s_star,APP_FONT_NAME,18,Size(270, 0 ), TextHAlignment::LEFT);
		int firstLineHeight = zeroLineHeight+sprite_star->getContentSize().height+_space;
		//target
		auto sprite_target = Sprite::create("res_ui/renwubb/renwumubiao.png");
		auto label_target = CCRichLabel::createWithString(boardMissionInfo->summary().c_str(),Size(325, 0 ),NULL,NULL );
		int secondLineHeight = firstLineHeight + label_target->getContentSize().height+_space;
		//description
		auto sprite_des = Sprite::create("res_ui/renwubb/renwuxushu.png");
		auto label_des = CCRichLabel::createWithString(boardMissionInfo->description().c_str(),Size(325, 0 ), NULL,NULL,0,18,4);
		int thirdLineHeight = secondLineHeight+label_des->getContentSize().height + _space;
		//rewardSprite
		auto sprite_award = Sprite::create("res_ui/renwubb/renwujiangli.png");
		//reward(gold,exp)
		bool isHaveExp = false;
		bool isHaveGold =  false; 
		Sprite *sprite_expItemFrame;
		if (boardMissionInfo->rewards().has_exp() && boardMissionInfo->rewards().exp() > 0)
		{
			sprite_expItemFrame = Sprite::create(EquipOrangeFramePath);

			auto sprite_goodsItem = Sprite::create("res_ui/exp.png");
			sprite_goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_goodsItem->setPosition(Vec2(sprite_expItemFrame->getContentSize().width/2,sprite_expItemFrame->getContentSize().height/2));
			sprite_expItemFrame->addChild(sprite_goodsItem);
			sprite_expItemFrame->setScale(0.8f);

			char s[20];
			sprintf(s,"%d",boardMissionInfo->rewards().exp());
			auto l_expValue = Label::createWithTTF(s,APP_FONT_NAME,18);
			l_expValue->setAnchorPoint(Vec2(0,0.5f));
			l_expValue->setPosition(Vec2(80,28));
			sprite_expItemFrame->addChild(l_expValue);

			isHaveExp = true;
		}

		Sprite *sprite_goldItemFrame;
		if (boardMissionInfo->rewards().has_gold() && boardMissionInfo->rewards().gold() > 0)
		{
			sprite_goldItemFrame = Sprite::create(EquipOrangeFramePath);

			auto sprite_goodsItem = Sprite::create("res_ui/coins.png");
			sprite_goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_goodsItem->setPosition(Vec2(sprite_goldItemFrame->getContentSize().width/2,sprite_goldItemFrame->getContentSize().height/2));
			sprite_goldItemFrame->addChild(sprite_goodsItem);
			sprite_goldItemFrame->setScale(0.8f);

			char s[20];
			sprintf(s,"%d",boardMissionInfo->rewards().gold());
			auto l_goldValue = Label::createWithTTF(s,APP_FONT_NAME,18);
			l_goldValue->setAnchorPoint(Vec2(0,0.5f));
			l_goldValue->setPosition(Vec2(80,25));
			sprite_goldItemFrame->addChild(l_goldValue);

			isHaveGold = true;
		}

		int fourthLineHeight;
		if (isHaveExp || isHaveGold)
		{
			fourthLineHeight = thirdLineHeight + 50 + _space;
		}
		else
		{
			fourthLineHeight = thirdLineHeight + _space;
		}
		//reward(goods).
		int fifthLineHeight = fourthLineHeight;
		if (boardMissionInfo->has_rewards())
		{
			for(int i = 0;i<boardMissionInfo->rewards().goods_size();++i)
			{
				Size awardSize = Size(150,50);
				if (i%2 == 0)
				{
					fifthLineHeight += awardSize.height;
				}
			}
		}

		scroll_height = fifthLineHeight+_space;

		auto m_contentScrollView = cocos2d::extension::ScrollView::create(Size(460,225));
		m_contentScrollView->setViewSize(Size(460, 225));
		m_contentScrollView->setIgnoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		m_contentScrollView->setAnchorPoint(Vec2(0,0));
		m_contentScrollView->setPosition(Vec2(50,87));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_base_layer->addChild(m_contentScrollView);
		if (scroll_height > 225)
		{
			m_contentScrollView->setContentSize(Size(460,scroll_height));
			m_contentScrollView->setContentOffset(Vec2(0,225-scroll_height));  
		}
		else
		{
			m_contentScrollView->setContentSize(Size(460,225));
		}
		m_contentScrollView->setClippingToBounds(true);

		sprite_name->setAnchorPoint(Vec2(0,1.0f));
		sprite_name->setPosition( Vec2( 15, m_contentScrollView->getContentSize().height -zeroLineHeight) );
		m_contentScrollView->addChild(sprite_name);
		label_name->setAnchorPoint(Vec2(0,1.0f));
		label_name->setPosition( Vec2( sprite_name->getContentSize().width+15, m_contentScrollView->getContentSize().height -zeroLineHeight) );
		m_contentScrollView->addChild(label_name);


		sprite_star->setAnchorPoint(Vec2(0,1.0f));
		sprite_star->setPosition( Vec2( 15, m_contentScrollView->getContentSize().height -firstLineHeight) );
		m_contentScrollView->addChild(sprite_star);
		//label_star->setAnchorPoint(Vec2(0,1.0f));
		//label_star->setPosition( Vec2( sprite_star->getContentSize().width+15, m_contentScrollView->getContentSize().height -firstLineHeight) );
		//m_contentScrollView->addChild(label_star);
		//�Ǽ�
		for(int i=0;i<5;i++)
		{
			if (i<curBoardMissionInfo->star())
			{
				Sprite* sprite_starIcon = Sprite::create("res_ui/star2.png");
				sprite_starIcon->setAnchorPoint(Vec2(0.5f,0.5f));
				sprite_starIcon->setPosition(Vec2(sprite_star->getContentSize().width+20+23*i,m_contentScrollView->getContentSize().height -firstLineHeight));
				sprite_starIcon->setScale(.6f);
				m_contentScrollView->addChild(sprite_starIcon);
			}
			else
			{
				Sprite* sprite_starIcon = Sprite::create("res_ui/star_off.png");
				sprite_starIcon->setAnchorPoint(Vec2(0.5f,0.5f));
				sprite_starIcon->setPosition(Vec2(sprite_star->getContentSize().width+20+23*i,m_contentScrollView->getContentSize().height -firstLineHeight));
				sprite_starIcon->setScale(1.0f);
				m_contentScrollView->addChild(sprite_starIcon);
			}
		}

		sprite_target->setAnchorPoint(Vec2(0,1.0f));
		sprite_target->setPosition( Vec2( 15, m_contentScrollView->getContentSize().height -secondLineHeight+label_target->getContentSize().height-sprite_target->getContentSize().height) );
		m_contentScrollView->addChild(sprite_target);
		label_target->setAnchorPoint(Vec2(0,1.0f));
		label_target->setPosition( Vec2( sprite_target->getContentSize().width+15, m_contentScrollView->getContentSize().height -secondLineHeight) );
		m_contentScrollView->addChild(label_target);

		sprite_des->setAnchorPoint(Vec2(0,1.0f));
		sprite_des->setPosition( Vec2( 15, m_contentScrollView->getContentSize().height -thirdLineHeight+label_des->getContentSize().height-sprite_des->getContentSize().height) );
		m_contentScrollView->addChild(sprite_des);
		label_des->setAnchorPoint(Vec2(0,1.0f));
		label_des->setPosition( Vec2( sprite_des->getContentSize().width+15, m_contentScrollView->getContentSize().height -thirdLineHeight) );
		m_contentScrollView->addChild(label_des);

		sprite_award->setAnchorPoint(Vec2(0,1.0f));

		if (isHaveExp || isHaveGold)
		{
			sprite_award->setPosition( Vec2( 15, m_contentScrollView->getContentSize().height -fourthLineHeight+25) );
		}
		else
		{
			sprite_award->setPosition( Vec2( 15, m_contentScrollView->getContentSize().height -fourthLineHeight-25) );
		}

		m_contentScrollView->addChild(sprite_award);

		if (isHaveExp && isHaveGold)
		{
			sprite_expItemFrame->setIgnoreAnchorPointForPosition(false);
			sprite_expItemFrame->setAnchorPoint(Vec2(0,1.0f));
			sprite_expItemFrame->setPosition( Vec2( 120, m_contentScrollView->getContentSize().height -fourthLineHeight) );
			m_contentScrollView->addChild(sprite_expItemFrame);
			sprite_goldItemFrame->setIgnoreAnchorPointForPosition(false);
			sprite_goldItemFrame->setAnchorPoint(Vec2(0,1.0f));
			sprite_goldItemFrame->setPosition( Vec2( 261, m_contentScrollView->getContentSize().height -fourthLineHeight) );
			m_contentScrollView->addChild(sprite_goldItemFrame);
		}
		else if (isHaveExp && (!isHaveGold))
		{
			sprite_expItemFrame->setIgnoreAnchorPointForPosition(false);
			sprite_expItemFrame->setAnchorPoint(Vec2(0,1.0f));
			sprite_expItemFrame->setPosition( Vec2( 120, m_contentScrollView->getContentSize().height -fourthLineHeight) );
			m_contentScrollView->addChild(sprite_expItemFrame);
		}
		else if (isHaveGold && (!isHaveExp))
		{
			sprite_goldItemFrame->setIgnoreAnchorPointForPosition(false);
			sprite_goldItemFrame->setAnchorPoint(Vec2(0,1.0f));
			sprite_goldItemFrame->setPosition( Vec2( 120, m_contentScrollView->getContentSize().height -fourthLineHeight) );
			m_contentScrollView->addChild(sprite_goldItemFrame);
		}
		else
		{

		}

		if (boardMissionInfo->has_rewards())
		{
			for(int i = 0;i<boardMissionInfo->rewards().goods_size();++i)
			{
				GoodsInfo * goodsInfo = new GoodsInfo();
				goodsInfo->CopyFrom(boardMissionInfo->rewards().goods(i).goods());
				RewardItem * rewardItem = RewardItem::create(goodsInfo,boardMissionInfo->rewards().goods(i).quantity());
				m_contentScrollView->addChild(rewardItem);
				delete goodsInfo;
				if(i%2 == 0)
				{
					rewardItem->setIgnoreAnchorPointForPosition(false);
					rewardItem->setAnchorPoint(Vec2(0,1.0f));
					if (isHaveExp || isHaveGold)
					{
						rewardItem->setPosition(Vec2(120, m_contentScrollView->getContentSize().height - fourthLineHeight -5 - 5*((int)(i/2)) -  rewardItem->getContentSize().height*((int)(i/2))));
					}
					else
					{
						rewardItem->setPosition(Vec2(120, m_contentScrollView->getContentSize().height -fourthLineHeight));
					}
				}
				else
				{
					rewardItem->setIgnoreAnchorPointForPosition(false);
					rewardItem->setAnchorPoint(Vec2(0,1.0f));
					if (isHaveExp || isHaveGold)
					{
						rewardItem->setPosition(Vec2(261, m_contentScrollView->getContentSize().height - fourthLineHeight -5 - 5*((int)(i/2)) - rewardItem->getContentSize().height*((int)(i/2))));
					}
					else
					{
						rewardItem->setPosition(Vec2(261, m_contentScrollView->getContentSize().height -fourthLineHeight));
					}
				}
			}
		}

		this->setContentSize(s_panel_rewardTaskInfo->getContentSize());
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(RewardTaskDetailUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(RewardTaskDetailUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(RewardTaskDetailUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(RewardTaskDetailUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void RewardTaskDetailUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RewardTaskDetailUI::onExit()
{
	UIScene::onExit();
}

bool RewardTaskDetailUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void RewardTaskDetailUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void RewardTaskDetailUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void RewardTaskDetailUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void RewardTaskDetailUI::update( float dt )
{
}

void RewardTaskDetailUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void RewardTaskDetailUI::GetMissionEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(7015, (void *)curBoardMissionInfo->index());
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void RewardTaskDetailUI::FinishMissionEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		for (int i = 0; i<GameView::getInstance()->missionManager->MissionList_MainScene.size(); i++)
		{
			if (curBoardMissionInfo->packageid() == GameView::getInstance()->missionManager->MissionList_MainScene.at(i)->missionpackageid())
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(7007, (char *)curBoardMissionInfo->packageid().c_str());
				break;
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RewardTaskDetailUI::FinishRightNowEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		char des[100];
		const char *strings = StringDataManager::getString("RewardTask_sureToFinishRightNow");
		sprintf(des, strings, curBoardMissionInfo->quickcompletegold());
		GameView::getInstance()->showPopupWindow(des, 2, this, SEL_CallFuncO(&RewardTaskDetailUI::SureToFinishRightNow), NULL);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RewardTaskDetailUI::SureToFinishRightNow( Ref *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(7023);
}

void RewardTaskDetailUI::AutoFindPathEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		for (int i = 0; i<GameView::getInstance()->missionManager->MissionList_MainScene.size(); i++)
		{
			if (curBoardMissionInfo->packageid() == GameView::getInstance()->missionManager->MissionList_MainScene.at(i)->missionpackageid())
			{
				GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*GameView::getInstance()->missionManager->MissionList_MainScene.at(i));
				GameView::getInstance()->missionManager->addMission(GameView::getInstance()->missionManager->curMissionInfo);
				break;
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RewardTaskDetailUI::GiveUpMissionEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameView::getInstance()->showPopupWindow(StringDataManager::getString("RewardTask_sureToGiveUpMission"), 2, this, SEL_CallFuncO(&RewardTaskDetailUI::SureToGiveUpMission), NULL);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void RewardTaskDetailUI::SureToGiveUpMission( Ref *pSender )
{
	for (int i = 0;i<GameView::getInstance()->missionManager->MissionList_MainScene.size();i++)
	{
		if (curBoardMissionInfo->packageid() == GameView::getInstance()->missionManager->MissionList_MainScene.at(i)->missionpackageid())
		{
			GameUtils::playGameSound(MISSION_CANCEL, 2, false);
			GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*GameView::getInstance()->missionManager->MissionList_MainScene.at(i));
			GameMessageProcessor::sharedMsgProcessor()->sendReq(7009, this);
			break;
		}
	}	
}

void RewardTaskDetailUI::refreshBtnState( int missionState )
{
	switch(missionState)
	{
	case dispatched:// �ѷ��
		{
			btn_getMission->setVisible(true);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
		break;
	case accepted:// ��ѽ��
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(true);
			btn_autoFindPath->setVisible(true);
			btn_giveUpMission->setVisible(true);
		}
		break;
	case done://�����
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(true);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
		break;
	case submitted:// ����ύ
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
		break;
	case abandoned:// �ѷ��
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
		break;
	case submitFailed:// ��ύʧ�
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
		break;
	case canceled://  ���ȡ�
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
		break;
	default:
		{
			btn_getMission->setVisible(false);
			btn_finishMission->setVisible(false);
			btn_finishRightNow->setVisible(false);
			btn_autoFindPath->setVisible(false);
			btn_giveUpMission->setVisible(false);
		}
	}
}
