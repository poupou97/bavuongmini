#ifndef _UI_REWARDTASKUI_REWARDTASKDETAILUI_H_
#define _UI_REWARDTASKUI_REWARDTASKDETAILUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ���������������
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.08.25
 */

class CBoardMissionInfo;

class RewardTaskDetailUI : public UIScene
{
public:
	RewardTaskDetailUI(void);
	~RewardTaskDetailUI(void);

	static RewardTaskDetailUI* create(CBoardMissionInfo * boardMissionInfo);
	bool init(CBoardMissionInfo * boardMissionInfo);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void update(float dt);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void GetMissionEvent(Ref *pSender, Widget::TouchEventType type);
	void FinishMissionEvent(Ref *pSender, Widget::TouchEventType type);
	void FinishRightNowEvent(Ref *pSender, Widget::TouchEventType type);
	void AutoFindPathEvent(Ref *pSender, Widget::TouchEventType type);
	void GiveUpMissionEvent(Ref *pSender, Widget::TouchEventType type);
	void SureToGiveUpMission( Ref *pSender );
	void SureToFinishRightNow( Ref *pSender );

	void refreshBtnState(int missionState);

private:
	Layer* m_base_layer;

	Button * btn_getMission;
	Button * btn_finishMission;
	Button * btn_finishRightNow;
	Button * btn_autoFindPath;
	Button * btn_giveUpMission;

public:
	CBoardMissionInfo* curBoardMissionInfo;
};

#endif