#ifndef _UI_REWARDTASKUI_REWARDTASKITEM_H_
#define _UI_REWARDTASKUI_REWARDTASKITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CBoardMissionInfo;

/////////////////////////////////
/**
 * ���������
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.08.21
 */

class RewardTaskItem : public UIScene
{
public:
	RewardTaskItem();
	~RewardTaskItem();

	static RewardTaskItem * create(CBoardMissionInfo * missionInfo,int pos);
	bool init(CBoardMissionInfo * missionInfo,int pos);

	void MissionStateEvent(Ref *pSender, Widget::TouchEventType type);
	void LookOverEvent(Ref *pSender, Widget::TouchEventType type);

	void SureToFinishRightNow(Ref *pSender);

	int getCurPos();

	void RefreshBtnMissionState(int state,bool isAnm = true);

private:
	CBoardMissionInfo * curBoardMission;
	int m_nPos;

	Button * btn_missionState;
	Label * l_missionState;
	ImageView * imageView_missionState;

	ImageView * imageView_mask;
};


#endif