#include "RewardTaskMainUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../GameAudio.h"
#include "../../utils/StaticDataManager.h"
#include "../../AppMacros.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/GameUtils.h"
#include "../extensions/CCRichLabel.h"
#include "RewardTaskData.h"
#include "RewardTaskItem.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CBoardMissionInfo.h"
#include "../missionscene/MissionManager.h"
#include "RewardTaskDetailUI.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "cocostudio\CCSGUIReader.h"

RewardTaskMainUI::RewardTaskMainUI(void)
{
}


RewardTaskMainUI::~RewardTaskMainUI(void)
{
}

RewardTaskMainUI* RewardTaskMainUI::create()
{
	auto rewardTaskMainUI = new RewardTaskMainUI();
	if (rewardTaskMainUI && rewardTaskMainUI->init())
	{
		rewardTaskMainUI->autorelease();
		return rewardTaskMainUI;
	}
	CC_SAFE_DELETE(rewardTaskMainUI);
	return NULL;
}

bool RewardTaskMainUI::init()
{
	if (UIScene::init())
	{
		auto winSize = Director::getInstance()->getVisibleSize();

		m_base_layer = Layer::create();
		m_base_layer->setIgnoreAnchorPointForPosition(false);
		m_base_layer->setAnchorPoint(Vec2(.5f,.5f));
		m_base_layer->setPosition(Vec2(winSize.width/2,winSize.height/2));
		m_base_layer->setContentSize(Size(800,480));
		this->addChild(m_base_layer);	

		m_up_layer = Layer::create();
		m_up_layer->setIgnoreAnchorPointForPosition(false);
		m_up_layer->setAnchorPoint(Vec2(.5f,.5f));
		m_up_layer->setPosition(Vec2(winSize.width/2,winSize.height/2));
		m_up_layer->setContentSize(Size(800,480));
		this->addChild(m_up_layer);	

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winSize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		auto s_panel_worldBossUI = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/offerReward_1.json");
		s_panel_worldBossUI->setAnchorPoint(Vec2(0.5f, 0.5f));
		s_panel_worldBossUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		s_panel_worldBossUI->setScale(1.0f);
		s_panel_worldBossUI->setTouchEnabled(true);
		m_pLayer->addChild(s_panel_worldBossUI);

		auto btn_close = (Button*)Helper::seekWidgetByName(s_panel_worldBossUI,"Button_close");
		btn_close->setTouchEnabled(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(RewardTaskMainUI::CloseEvent, this));
		//ˢ�
		auto btn_refresh = (Button*)Helper::seekWidgetByName(s_panel_worldBossUI,"Button_refresh");
		btn_refresh->setTouchEnabled(true);
		btn_refresh->setPressedActionEnabled(true);
		btn_refresh->addTouchEventListener(CC_CALLBACK_2(RewardTaskMainUI::RefreshEvent, this));
		//�һ�����
		auto btn_refreshFiveStar = (Button*)Helper::seekWidgetByName(s_panel_worldBossUI,"Button_refreshFiveStar");
		btn_refreshFiveStar->setTouchEnabled(true);
		btn_refreshFiveStar->setPressedActionEnabled(true);
		btn_refreshFiveStar->addTouchEventListener(CC_CALLBACK_2(RewardTaskMainUI::RefreshFiveStarEvent, this));
		
		l_remainTimeToFree = (Text*)Helper::seekWidgetByName(s_panel_worldBossUI,"Label_remainTimeToFree");
		l_remainTaskNum = (Text*)Helper::seekWidgetByName(s_panel_worldBossUI,"Label_remainTaskNum");
		l_refreshCostValue = (Text*)Helper::seekWidgetByName(s_panel_worldBossUI,"Label_refreshCost");
		l_refreshFiveStarCostValue = (Text*)Helper::seekWidgetByName(s_panel_worldBossUI,"Label_refreshFiveStarCost");
		l_freeThisTime = (Text*)Helper::seekWidgetByName(s_panel_worldBossUI,"Label_freeThisTime");

		this->schedule(schedule_selector(RewardTaskMainUI::update),1.0f);

		//this->setTouchEnabled(false);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winSize);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(RewardTaskMainUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(RewardTaskMainUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(RewardTaskMainUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(RewardTaskMainUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void RewardTaskMainUI::onEnter()
{
	UIScene::onEnter();
}

void RewardTaskMainUI::onExit()
{
	UIScene::onExit();
}

bool RewardTaskMainUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return false;
}

void RewardTaskMainUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void RewardTaskMainUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void RewardTaskMainUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void RewardTaskMainUI::update( float dt )
{
	if (RewardTaskData::getInstance()->getTargetNpcId() == 0)
		return;

	if (GameView::getInstance()->missionManager->isOutOfMissionTalkArea(RewardTaskData::getInstance()->getTargetNpcId()))
	{
		auto td = (RewardTaskDetailUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskDetailUI);
		if (td)
		{
			td->closeAnim();
		}
		this->closeAnim();
		return;
	}
	
	if (RewardTaskData::getInstance()->getRemainCDTime() > 0)
	{
		RewardTaskData::getInstance()->setRemainCDTime(RewardTaskData::getInstance()->getRemainCDTime()-1000);
		RewardTaskData::getInstance()->setCurRefreshType(NROM_GOLD);

		l_freeThisTime->setVisible(false);
		l_remainTimeToFree->setVisible(true);
		l_refreshCostValue->setVisible(true);
		
		std::string str_remainTime = RecuriteActionItem::timeFormatToString(RewardTaskData::getInstance()->getRemainCDTime()/1000);
		l_remainTimeToFree->setString(str_remainTime.c_str());
	}
	else
	{
		RewardTaskData::getInstance()->setRemainCDTime(0);
		RewardTaskData::getInstance()->setCurRefreshType(FREE_REFRESH);

		l_freeThisTime->setVisible(true);
		l_remainTimeToFree->setVisible(false);
		l_refreshCostValue->setVisible(false);
	}
}

void RewardTaskMainUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto td = (RewardTaskDetailUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardTaskDetailUI);
		if (td)
		{
			td->closeAnim();
		}

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RewardTaskMainUI::RefreshFiveMissionPresent()
{
	//delete old
	for(int i = 0;i<5;i++)
	{
		auto rewardTaskItem = dynamic_cast<RewardTaskItem *>(m_base_layer->getChildByTag(kTag_RewardTaskItem_Base+i));
		if (rewardTaskItem)
		{
			rewardTaskItem->removeFromParent();
		}
	}
	//add new
	for (int i = 0;i<RewardTaskData::getInstance()->p_taskList.size();i++)
	{
		if (i>=5)
			continue;

		auto rewardTaskItem = RewardTaskItem::create(RewardTaskData::getInstance()->p_taskList.at(i),i);
		rewardTaskItem->setAnchorPoint(Vec2(.5f,.5f));
		rewardTaskItem->setPosition(Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.at(i)->index());
		m_base_layer->addChild(rewardTaskItem);
	}
}

void RewardTaskMainUI::RefreshFiveMissionPresentWithAnm()
{
	float newItemDelayTime = 0.f;
	Size winSize = Director::getInstance()->getVisibleSize();
	//delete old
	for(int i = 0;i<5;i++)
	{
		auto rewardTaskItem = dynamic_cast<RewardTaskItem *>(m_base_layer->getChildByTag(kTag_RewardTaskItem_Base+i));
		if (rewardTaskItem)
		{
			auto sequence = Sequence::create(
				DelayTime::create(0.1f*i),
				CCEaseBackIn::create(MoveTo::create(0.2f,Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y-winSize.height))),
				RemoveSelf::create(),
			NULL);
			rewardTaskItem->runAction(sequence);
			sequence->setTag(kTag_RefreshAction_deleteOld);

			newItemDelayTime = 0.15f*i+0.15f;
		}
	}
	//add new
	for (int i = 0;i<RewardTaskData::getInstance()->p_taskList.size();i++)
	{
		if (i>=5)
			continue;

		/////////////////////////////////////fourth way////////////////////////////////////////////
		/**************************Ǵ��Ϸ����*******************************/
		auto rewardTaskItem = RewardTaskItem::create(RewardTaskData::getInstance()->p_taskList.at(i),i);
		rewardTaskItem->setAnchorPoint(Vec2(.5f,.5f));
		//rewardTaskItem->setPosition(Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setPosition(Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y+winSize.height));
		rewardTaskItem->setTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.at(i)->index());
		m_base_layer->addChild(rewardTaskItem);

		auto sequence = Sequence::create(
			DelayTime::create(newItemDelayTime+0.1f*i),
			CCEaseBackOut::create(MoveTo::create(0.2f,Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y))),
			NULL);
		rewardTaskItem->runAction(sequence);
		sequence->setTag(kTag_RefreshAction_addNew);
	}
}

void RewardTaskMainUI::RefreshMissionState( int index,int state )
{
	for (int i = 0;i<5;i++)
	{
		auto rewardTaskItem = (RewardTaskItem*)m_base_layer->getChildByTag(kTag_RewardTaskItem_Base+i);
		if (rewardTaskItem)
		{
			rewardTaskItem->setLocalZOrder(0);
			if (i == index)
			{
				rewardTaskItem->setLocalZOrder(5);
				rewardTaskItem->RefreshBtnMissionState(state,true);
			}
		}
	}
}

void RewardTaskMainUI::RefreshEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		bool isActionExist = false;
		for (int i = 0; i<5; i++)
		{
			auto rewardTaskItem = dynamic_cast<RewardTaskItem *>(m_base_layer->getChildByTag(kTag_RewardTaskItem_Base + i));
			if (rewardTaskItem)
			{
				if (rewardTaskItem->getActionByTag(kTag_RefreshAction_deleteOld) || rewardTaskItem->getActionByTag(kTag_RefreshAction_addNew))
				{
					isActionExist = true;
					break;
				}
			}
		}
		if (isActionExist)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("RewardTaskItem_missionstate_isRefreshing"));
			return;
		}

		if (RewardTaskData::getInstance()->getRemainCDTime() > 0)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(7016, (void *)NROM_GOLD);
		}
		else
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(7016, (void *)FREE_REFRESH);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RewardTaskMainUI::RefreshFiveStarEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		bool isActionExist = false;
		for (int i = 0; i<5; i++)
		{
			auto rewardTaskItem = dynamic_cast<RewardTaskItem *>(m_base_layer->getChildByTag(kTag_RewardTaskItem_Base + i));
			if (rewardTaskItem)
			{
				if (rewardTaskItem->getActionByTag(kTag_RefreshAction_deleteOld) || rewardTaskItem->getActionByTag(kTag_RefreshAction_addNew))
				{
					isActionExist = true;
					break;
				}
			}
		}
		if (isActionExist)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("RewardTaskItem_missionstate_isRefreshing"));
			return;
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(7016, (void *)FIVE_STAR);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void RewardTaskMainUI::RefreshRemainTaskNum()
{
	std::string str_remainNum ;
	char s_remain [10];
	sprintf(s_remain,"%d",RewardTaskData::getInstance()->getRemainTaskNum());
	str_remainNum.append(s_remain);
	str_remainNum.append("/");
	char s_total [10];
	sprintf(s_total,"%d",RewardTaskData::getInstance()->getAllTTaskNum());
	str_remainNum.append(s_total);
	l_remainTaskNum->setString(str_remainNum.c_str());
}

// void RewardTaskMainUI::RunAddNewAction()
// {
// 	//delete old
// 	for(int i = 0;i<5;i++)
// 	{
// 		RewardTaskItem * rewardTaskItem = dynamic_cast<RewardTaskItem *>(m_base_layer->getChildByTag(kTag_RewardTaskItem_Base+i));
// 		if (rewardTaskItem)
// 		{
// 			rewardTaskItem->removeFromParent();
// 		}
// 	}
// 	Size winSize = Director::getInstance()->getVisibleSize();
// 	//add new
// 	for (int i = 0;i<RewardTaskData::getInstance()->p_taskList.size();i++)
// 	{
// 		if (i>=5)
// 			continue;

		/////////////////////////////////////first way////////////////////////////////////////////
		/**************************�ϴ�Ƶķ�ʽ*******************************/
		/*
		RewardTaskItem * rewardTaskItem = RewardTaskItem::create(RewardTaskData::getInstance()->p_taskList.at(i),i);
		rewardTaskItem->setAnchorPoint(Vec2(.5f,.5f));
		//rewardTaskItem->setPosition(Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setPosition(Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*2,RewardTastItem_First_Pos_y-winSize.height/2));
		rewardTaskItem->setTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.at(i)->index());
		m_base_layer->addChild(rewardTaskItem);

		//begin run action
		Sequence * sequence = Sequence::create(
			MoveTo::create(0.5f,Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*2,RewardTastItem_First_Pos_y-winSize.height/4)),
			RotateTo::create(0.2f,18*(i-2)),
			DelayTime::create(0.4f),
			MoveTo::create(0.5f,Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y)),
			NULL);
		rewardTaskItem->runAction(sequence);
		*/

		/////////////////////////////////////second way////////////////////////////////////////////
		/**************************���*******************************/
		/*
		RewardTaskItem * rewardTaskItem = RewardTaskItem::create(RewardTaskData::getInstance()->p_taskList.at(i),i);
		rewardTaskItem->setAnchorPoint(Vec2(.5f,.5f));
		//rewardTaskItem->setPosition(Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setPosition(Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.at(i)->index());
		m_base_layer->addChild(rewardTaskItem);
		rewardTaskItem->setVisible(false);
		rewardTaskItem->setScale(1.5f);

		Sequence * sequence = Sequence::create(
			DelayTime::create(0.3f*i),
			Show::create(),
			CCEaseBackIn::create(ScaleTo::create(0.23f,1.0f,1.0f)),
			NULL);
		rewardTaskItem->runAction(sequence);
		*/

		/////////////////////////////////////third way////////////////////////////////////////////
		/**************************´��Ҳ����*******************************/
		/*
		RewardTaskItem * rewardTaskItem = RewardTaskItem::create(RewardTaskData::getInstance()->p_taskList.at(i),i);
		rewardTaskItem->setAnchorPoint(Vec2(.5f,.5f));
		//rewardTaskItem->setPosition(Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setPosition(Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i+winSize.width,RewardTastItem_First_Pos_y));
		rewardTaskItem->setTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.at(i)->index());
		m_base_layer->addChild(rewardTaskItem);

		Sequence * sequence = Sequence::create(
			DelayTime::create(0.4f*i),
			CCEaseBackOut::create(MoveTo::create(0.3f,Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y))),
			NULL);
		rewardTaskItem->runAction(sequence);
		*/

		/////////////////////////////////////fourth way////////////////////////////////////////////
		/**************************���Ϸ����*******************************/
/*
		RewardTaskItem * rewardTaskItem = RewardTaskItem::create(RewardTaskData::getInstance()->p_taskList.at(i),i);
		rewardTaskItem->setAnchorPoint(Vec2(.5f,.5f));
		//rewardTaskItem->setPosition(Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y));
		rewardTaskItem->setPosition(Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y+winSize.height));
		rewardTaskItem->setTag(kTag_RewardTaskItem_Base+RewardTaskData::getInstance()->p_taskList.at(i)->index());
		m_base_layer->addChild(rewardTaskItem);

		Sequence * sequence = Sequence::create(
			DelayTime::create(0.35f*i),
			CCEaseBackOut::create(MoveTo::create(0.3f,Vec2(RewardTastItem_First_Pos_x+RewardTastItem_space*i,RewardTastItem_First_Pos_y))),
			NULL);
		rewardTaskItem->runAction(sequence);
		*/
//	}
//}


void RewardTaskMainUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void RewardTaskMainUI::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	auto winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,60,40);
// 	tutorialIndicator->setPosition(Vec2(pos.x,pos.y));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	m_up_layer->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,94,43,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x-56+_w,pos.y-70+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void RewardTaskMainUI::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(m_up_layer->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}