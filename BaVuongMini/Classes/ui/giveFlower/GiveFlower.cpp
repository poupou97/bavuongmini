#include "GiveFlower.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/RichTextInput.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../GameView.h"
#include "../extensions/Counter.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../ui/goldstore_ui/GoldStoreUI.h"
#include "../../messageclient/element/CLable.h"
#include "../../ui/extensions/UITab.h"
#include "AppMacros.h"
#include "cocostudio\CCSGUIReader.h"


GiveFlower::GiveFlower(void)
{
	m_giveFlowerNum = 0;
	m_giveFlowerModel = 0;
}

GiveFlower::~GiveFlower(void)
{
}

GiveFlower * GiveFlower::create(long long id,std::string name,int pression,int level,int countryId,int vipLv)
{
	auto flower_ =  new GiveFlower();
	if (flower_ && flower_->init(id,name,pression,level,countryId,vipLv))
	{
		flower_->autorelease();
		return flower_;
	}
	CC_SAFE_DELETE(flower_);
	return NULL;
}

bool GiveFlower::init(long long id,std::string name,int pression,int level,int countryId,int vipLv)
{
	if (UIScene::init())
	{
		winsize= Director::getInstance()->getVisibleSize();
		
		m_playerId = id;
		m_playerName = name;

		auto m_Layer= Layer::create();
		m_Layer->setIgnoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(Vec2(0.5f,0.5f));
		m_Layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_Layer->setContentSize(Size(800,480));
		this->addChild(m_Layer);

		auto panel_ = (Layout*) cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/flower_1.json");
		panel_->setAnchorPoint(Vec2(0.5f,0.5f));
		panel_->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(panel_);
		
		//flower type type1
// 		checkbox_flowerType1 =(UICheckBox *)Helper::seekWidgetByName(panel_,"CheckBox_flower1");
// 		checkbox_flowerType1->setTouchEnabled(true);
// 		checkbox_flowerType1->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::UICheckBoxType1));
// 		checkbox_flowerType1->setSelected(false);

		checkbox_flowerType1 = CheckBox::create();
		checkbox_flowerType1->setTouchEnabled(true);
		checkbox_flowerType1->loadTextures("res_ui/xiaofangkuang2.png",
			"res_ui/xiaofangkuang2.png",
			"res_ui/red.png",
			"res_ui/red.png",
			"");
		checkbox_flowerType1->setAnchorPoint(Vec2(0.5f,0.5f));
		checkbox_flowerType1->addEventListener(CC_CALLBACK_2(GiveFlower::UICheckBoxType1,this));
		checkbox_flowerType1->setPosition(Vec2(66,298));
		checkbox_flowerType1->setLocalZOrder(10);
		panel_->addChild(checkbox_flowerType1);
		checkbox_flowerType1->setSelected(false);

		auto image_flowerType1 = ImageView::create();
		image_flowerType1->loadTexture("res_ui/props_icon/flower_lily.png");
		image_flowerType1->setAnchorPoint(Vec2(0.5f,0.5f));
		image_flowerType1->setPosition(Vec2(103,300));//290
		image_flowerType1->setLocalZOrder(10);
		panel_->addChild(image_flowerType1);
		//47 93 13
		auto label_flower1 = Label::createWithTTF(StringDataManager::getString("Flower_Type_baihe"), APP_FONT_NAME, 18);
		label_flower1->setAnchorPoint(Vec2(0.5f,0.5f));
		label_flower1->setPosition(Vec2(image_flowerType1->getPosition().x,
									image_flowerType1->getPosition().y - image_flowerType1->getContentSize().height/2 - label_flower1->getContentSize().height/2));
		label_flower1->setColor(Color3B(47,93,13));
		label_flower1->setLocalZOrder(10);
		panel_->addChild(label_flower1);

// 		Button * btn_flowerType1 =(Button *)Helper::seekWidgetByName(panel_,"Button_flower1");
// 		btn_flowerType1->setTouchEnabled(true);
// 		btn_flowerType1->setPressedActionEnabled(true);
// 		btn_flowerType1->addTouchEventListener(CC_CALLBACK_2(GiveFlower::callBackFlowerType));
		//type 2
// 		checkbox_flowerType2 =(UICheckBox *)Helper::seekWidgetByName(panel_,"CheckBox_flower2");
// 		checkbox_flowerType2->setTouchEnabled(true);
// 		checkbox_flowerType2->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::UICheckBoxType2));
// 		checkbox_flowerType2->setSelected(false);

		checkbox_flowerType2 = CheckBox::create();
		checkbox_flowerType2->setTouchEnabled(true);
		checkbox_flowerType2->loadTextures("res_ui/xiaofangkuang2.png",
			"res_ui/xiaofangkuang2.png",
			"res_ui/red.png",
			"res_ui/red.png",
			"");
		checkbox_flowerType2->setAnchorPoint(Vec2(0.5f,0.5f));
		checkbox_flowerType2->addEventListener(CC_CALLBACK_2(GiveFlower::UICheckBoxType2,this));
		checkbox_flowerType2->setPosition(Vec2(156,298));
		checkbox_flowerType2->setLocalZOrder(10);
		panel_->addChild(checkbox_flowerType2);
		checkbox_flowerType2->setSelected(false);

		auto image_flowerType2 = ImageView::create();
		image_flowerType2->loadTexture("res_ui/props_icon/flower_pink.png");
		image_flowerType2->setAnchorPoint(Vec2(0.5f,0.5f));
		image_flowerType2->setLocalZOrder(10);
		image_flowerType2->setPosition(Vec2(193,300));
		panel_->addChild(image_flowerType2);

		auto label_flower2 = Label::createWithTTF(StringDataManager::getString("Flower_Type_fenmeigui"), APP_FONT_NAME, 18);
		label_flower2->setAnchorPoint(Vec2(0.5f,0.5f));
		label_flower2->setPosition(Vec2(image_flowerType2->getPosition().x,
			image_flowerType2->getPosition().y - image_flowerType2->getContentSize().height/2 - label_flower2->getContentSize().height/2));
		label_flower2->setColor(Color3B(47,93,13));
		label_flower2->setLocalZOrder(10);
		panel_->addChild(label_flower2);

// 		Button * btn_flowerType2 =(Button *)Helper::seekWidgetByName(panel_,"Button_flower2");
// 		btn_flowerType2->setTouchEnabled(true);
// 		btn_flowerType2->setPressedActionEnabled(true);
// 		btn_flowerType2->addTouchEventListener(CC_CALLBACK_2(GiveFlower::callBackFlowerType));
		//type3
// 		checkbox_flowerType3 =(UICheckBox *)Helper::seekWidgetByName(panel_,"CheckBox_flower3");
// 		checkbox_flowerType3->setTouchEnabled(true);
// 		checkbox_flowerType3->addEventListenerCheckBox(this,checkboxselectedeventselector(GiveFlower::UICheckBoxType3));
// 		checkbox_flowerType3->setSelected(false);
		
		checkbox_flowerType3 = CheckBox::create();
		checkbox_flowerType3->setTouchEnabled(true);
		checkbox_flowerType3->loadTextures("res_ui/xiaofangkuang2.png",
			"res_ui/xiaofangkuang2.png",
			"res_ui/red.png",
			"res_ui/red.png",
			"");
		checkbox_flowerType3->setAnchorPoint(Vec2(0.5f,0.5f));
		checkbox_flowerType3->addEventListener(CC_CALLBACK_2(GiveFlower::UICheckBoxType3,this));
		checkbox_flowerType3->setPosition(Vec2(246,298));
		checkbox_flowerType3->setLocalZOrder(10);
		panel_->addChild(checkbox_flowerType3);
		checkbox_flowerType3->setSelected(false);

		auto image_flowerType3 = ImageView::create();
		image_flowerType3->loadTexture("res_ui/props_icon/flower_red.png");
		image_flowerType3->setAnchorPoint(Vec2(0.5f,0.5f));
		image_flowerType3->setPosition(Vec2(283,300));
		image_flowerType3->setLocalZOrder(10);
		panel_->addChild(image_flowerType3);

		auto label_flower3 = Label::createWithTTF(StringDataManager::getString("Flower_Type_hongmeigui"), APP_FONT_NAME, 18);
		label_flower3->setAnchorPoint(Vec2(0.5f,0.5f));
		label_flower3->setPosition(Vec2(image_flowerType3->getPosition().x,
										image_flowerType3->getPosition().y - image_flowerType3->getContentSize().height/2 - label_flower3->getContentSize().height/2));
		label_flower3->setColor(Color3B(47,93,13));
		label_flower3->setLocalZOrder(10);
		panel_->addChild(label_flower3);
// 		Button * btn_flowerType3 =(Button *)Helper::seekWidgetByName(panel_,"Button_flower3");
// 		btn_flowerType3->setTouchEnabled(true);
// 		btn_flowerType3->setPressedActionEnabled(true);
// 		btn_flowerType3->addTouchEventListener(CC_CALLBACK_2(GiveFlower::callBackFlowerType));
		//Button_number_1
		checkbox_num1 =(CheckBox *)Helper::seekWidgetByName(panel_,"CheckBox_select1");
		checkbox_num1->setTouchEnabled(true);
		checkbox_num1->addEventListener(CC_CALLBACK_2(GiveFlower::UICheckBoxNum1,this));
		checkbox_num1->setSelected(false);

// 		Button * btn_flowerNum1 =(Button *)Helper::seekWidgetByName(panel_,"Button_select1");
// 		btn_flowerNum1->setTouchEnabled(true);
// 		btn_flowerNum1->setPressedActionEnabled(true);
// 		btn_flowerNum1->addTouchEventListener(CC_CALLBACK_2(GiveFlower::callBackModelFlowerNum));
		// num 11
		checkbox_num11 =(CheckBox *)Helper::seekWidgetByName(panel_,"CheckBox_select11");
		checkbox_num11->setTouchEnabled(true);
		checkbox_num11->addEventListener(CC_CALLBACK_2(GiveFlower::UICheckBoxNum11,this));
		checkbox_num11->setSelected(false);

// 		Button * btn_flowerNum11 =(Button *)Helper::seekWidgetByName(panel_,"Button_select11");
// 		btn_flowerNum11->setTouchEnabled(true);
// 		btn_flowerNum11->setPressedActionEnabled(true);
// 		btn_flowerNum11->addTouchEventListener(CC_CALLBACK_2(GiveFlower::callBackModelFlowerNum));
 		//num 99
		checkbox_num99 =(CheckBox *)Helper::seekWidgetByName(panel_,"CheckBox_select99");
		checkbox_num99->setTouchEnabled(true);
		checkbox_num99->addEventListener(CC_CALLBACK_2(GiveFlower::UICheckBoxNum99,this));
		checkbox_num99->setSelected(false);

// 		Button * btn_flowerNum99 =(Button *)Helper::seekWidgetByName(panel_,"Button_select99");
// 		btn_flowerNum99->setTouchEnabled(true);
// 		btn_flowerNum99->setPressedActionEnabled(true);
// 		btn_flowerNum99->addTouchEventListener(CC_CALLBACK_2(GiveFlower::callBackModelFlowerNum));
		//num 365
		checkbox_num365 =(CheckBox *)Helper::seekWidgetByName(panel_,"CheckBox_select365");
		checkbox_num365->setTouchEnabled(true);
		checkbox_num365->addEventListener(CC_CALLBACK_2(GiveFlower::UICheckBoxNum365,this));
		checkbox_num365->setSelected(false);

// 		Button * btn_flowerNum365 =(Button *)Helper::seekWidgetByName(panel_,"Button_select365");
// 		btn_flowerNum365->setTouchEnabled(true);
// 		btn_flowerNum365->setPressedActionEnabled(true);
// 		btn_flowerNum365->addTouchEventListener(CC_CALLBACK_2(GiveFlower::callBackModelFlowerNum));
		//num 999
		checkbox_num999 =(CheckBox *)Helper::seekWidgetByName(panel_,"CheckBox_select999");
		checkbox_num999->setTouchEnabled(true);
		checkbox_num999->addEventListener(CC_CALLBACK_2(GiveFlower::UICheckBoxNum999,this));
		checkbox_num999->setSelected(false);

// 		Button * btn_flowerNum999 =(Button *)Helper::seekWidgetByName(panel_,"Button_select999");
// 		btn_flowerNum999->setTouchEnabled(true);
// 		btn_flowerNum999->setPressedActionEnabled(true);
// 		btn_flowerNum999->addTouchEventListener(CC_CALLBACK_2(GiveFlower::callBackModelFlowerNum));
		//self define//
		checkbox_Define =(CheckBox *)Helper::seekWidgetByName(panel_,"CheckBox_selfDefine");
		checkbox_Define->setTouchEnabled(true);
		checkbox_Define->addEventListener(CC_CALLBACK_2(GiveFlower::UIcheckBoxDefineNum,this));
		checkbox_Define->setSelected(false);

// 		Button * btn_Define =(Button *)Helper::seekWidgetByName(panel_,"Button_selfDefine");
// 		btn_Define->setTouchEnabled(true);
// 		btn_Define->setPressedActionEnabled(true);
// 		btn_Define->addTouchEventListener(CC_CALLBACK_2(GiveFlower::callBackModelFlowerNum));

		//input flower number 
		auto btn_SelectNum =(Button *)Helper::seekWidgetByName(panel_,"Button_Counter");
		btn_SelectNum->setTouchEnabled(true);
		//btn_SelectNum->setPressedActionEnabled(true);
		btn_SelectNum->addTouchEventListener(CC_CALLBACK_2(GiveFlower::callBackDefineFlowerNum, this));

		label_showFlowerNum = (Text*)Helper::seekWidgetByName(panel_,"Label_selectNum");
		label_showFlowerNum->setString("0");

		//player icon
		std::string playerIcon_;
		if (pression > 0 && pression <5)
		{
			playerIcon_ = BasePlayer::getProfessionIconByIndex(pression);

		}else
		{
			playerIcon_ = "";
		}

		auto imagePlayerIcon = (ImageView *)Helper::seekWidgetByName(panel_,"ImageView_profession");
		imagePlayerIcon->loadTexture(playerIcon_.c_str());

		//
		std::string playerIcon_headHero = BasePlayer::getHalfBodyPathByProfession(pression);
		auto imagePlayerHeadHeroIcon = (ImageView *)Helper::seekWidgetByName(panel_,"ImageView_player");
		imagePlayerHeadHeroIcon->loadTexture(playerIcon_headHero.c_str());
		

		//player name
		auto label_playerName = (Text*)Helper::seekWidgetByName(panel_,"Label_name");
		label_playerName->setString(m_playerName.c_str());

		auto image_country = (ImageView *)Helper::seekWidgetByName(panel_,"ImageView_country");
		if (countryId > 0 && countryId <6)
		{
			std::string countryIdName = "country_";
			char id[2];
			sprintf(id, "%d", countryId);
			countryIdName.append(id);
			std::string iconPathName = "res_ui/country_icon/";
			iconPathName.append(countryIdName);
			iconPathName.append(".png");

			image_country->loadTexture(countryIdName.c_str());
			image_country->setPosition(Vec2(label_playerName->getPosition().x -label_playerName->getContentSize().width/2 - image_country->getContentSize().width/2*0.9f,
							label_playerName->getPosition().y));
		}

		if (vipLv > 0)
		{
			auto image_vip = (ImageView *)Helper::seekWidgetByName(panel_,"ImageView_vip");
			image_vip->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/vip1.png");
			image_vip->setPosition(Vec2(label_playerName->getPosition().x +label_playerName->getContentSize().width/2,label_playerName->getPosition().y -  image_vip->getContentSize().height/2));

			
			char s_level[10];
			sprintf(s_level,"%d",vipLv);
			auto lbt_vipLevel = Label::createWithBMFont("res_ui/font/ziti_3.fnt", s_level);
			lbt_vipLevel->setScale(0.7f);
			lbt_vipLevel->setAnchorPoint(Vec2(0.5f,0.5f));
			//lbt_vipLevel->setPosition(Vec2(15,-5));
			lbt_vipLevel->setPosition(Vec2(image_vip->getContentSize().width,0));
			image_vip->addChild(lbt_vipLevel);
		}

		char levelStr[20];
		sprintf(levelStr,"%d",level);
		std::string stringLevel_ = "LV.";
		stringLevel_.append(levelStr);
		
		auto label_level = (Text*)Helper::seekWidgetByName(panel_,"Label_level");
		label_level->setString(stringLevel_.c_str());

		//input flowerSaid (601,209)
		inputFlowerSaid=new RichTextInputBox();
		inputFlowerSaid->setMultiLinesMode(true);
		inputFlowerSaid->setInputBoxWidth(250);
		inputFlowerSaid->setInputBoxHeight(80);
		inputFlowerSaid->setDirection(RichTextInputBox::kCCRichInputDirectionVertical);
		inputFlowerSaid->setAnchorPoint(Vec2(0,0));
		inputFlowerSaid->setScale(0.9f);
		//inputFlowerSaid->setPosition(Vec2(480,280));
		inputFlowerSaid->setPosition(Vec2(440,157));
		inputFlowerSaid->setCharLimit(20);
		m_Layer->addChild(inputFlowerSaid);
		inputFlowerSaid->release();

		//
		checkbox_public =(CheckBox *)Helper::seekWidgetByName(panel_,"CheckBox_public");
		checkbox_public->setTouchEnabled(true);
		checkbox_public->addEventListener(CC_CALLBACK_2(GiveFlower::setModelPublic,this));
		checkbox_public->setSelected(true);

		checkbox_private =(CheckBox *)Helper::seekWidgetByName(panel_,"CheckBox_private");
		checkbox_private->setTouchEnabled(true);
		checkbox_private->addEventListener(CC_CALLBACK_2(GiveFlower::setModelPrivate,this));
		checkbox_private->setSelected(false);

		//give flower
		auto btn_giveFlower =(Button *)Helper::seekWidgetByName(panel_,"Button_send");
		btn_giveFlower->setTouchEnabled(true);
		btn_giveFlower->setPressedActionEnabled(true);
		btn_giveFlower->addTouchEventListener(CC_CALLBACK_2(GiveFlower::callBackGiveFlower, this));

		//close ui
		auto buttonExit =(Button *)Helper::seekWidgetByName(panel_,"Button_close");
		buttonExit->setTouchEnabled(true);
		buttonExit->setPressedActionEnabled(true);
		buttonExit->addTouchEventListener(CC_CALLBACK_2(GiveFlower::callBackExit, this));

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void GiveFlower::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GiveFlower::onExit()
{
	UIScene::onExit();
}

void GiveFlower::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void GiveFlower::callBackGiveFlower(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (strlen(getFlowerName().c_str()) <= 0)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("giveflowerui_selectFlowerType"));
			return;
		}

		if (getFlowerNum() <= 0)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("giveflowerui_selectFlowerNum"));
			return;
		}
		//flower num is enough
		int hasFlowerNum_ = 0;
		for (int i = 0; i<GameView::getInstance()->AllPacItem.size(); i++)
		{

			std::string goodsId_ = GameView::getInstance()->AllPacItem.at(i)->goods().id();
			if (strcmp(getFlowerName().c_str(), goodsId_.c_str()) == 0)
			{
				hasFlowerNum_ += GameView::getInstance()->AllPacItem.at(i)->quantity();
			}
		}

		if (getFlowerNum() > hasFlowerNum_)
		{
			std::string str_ = StringDataManager::getString("FlowerUi_FlowerIsNotEnough");
			GameView::getInstance()->showPopupWindow(str_, 2, this, SEL_CallFuncO(&GiveFlower::callBackOpenShop), NULL);
			return;
		}

		const char* inputStr = inputFlowerSaid->getInputString();
		if (getFlowerNum() < 999 && inputStr != NULL)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("giveflowerui_selectFlowerAndSaid"));
			return;
		}

		std::string flowerSaid_ = "";
		if (inputStr != NULL)
		{
			flowerSaid_ = inputStr;
		}

		GiveFlowerStruct flowerInfo = { m_playerId,m_playerName,getFlowerNum(),getFlowerName(),getModelType(),flowerSaid_ };
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5140, &flowerInfo);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GiveFlower::callBackModelFlowerNum( Ref * obj )
{
	auto btn = (Button *)obj;
	//Buttontree
	this->setFlowerNum(999);
}

void GiveFlower::setModelPublic(Ref* pSender, CheckBox::EventType type)
{
	auto check_ = (CheckBox *)pSender;
	
	if (check_->isSelected())
	{
		setModelType(0);
		checkbox_private->setSelected(false);
	}else
	{
		setModelType(1);
		checkbox_private->setSelected(true);
	}
}

void GiveFlower::setModelPrivate(Ref* pSender, CheckBox::EventType type)
{
	auto check_ = (CheckBox *)pSender;

	if (check_->isSelected())
	{
		setModelType(1);
		checkbox_public->setSelected(false);
	}else
	{
		setModelType(0);
		checkbox_public->setSelected(true);
	}
}

void GiveFlower::callBackFlowerType( Ref * obj )
{
	//Buttontree
	setFlowerName("flower type");
}

void GiveFlower::callBackDefineFlowerNum(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//
		if (checkbox_Define->isSelected() == false)
		{
			checkbox_Define->setSelected(true);
			setFlowerNum(0);
			checkbox_num1->setSelected(false);
			checkbox_num11->setSelected(false);
			checkbox_num99->setSelected(false);
			checkbox_num365->setSelected(false);
			checkbox_num999->setSelected(false);
		}

		GameView::getInstance()->showCounter(this, callfuncO_selector(GiveFlower::showFlowerNum));
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GiveFlower::showFlowerNum( Ref * obj )
{
	auto couter_ = (Counter *)obj;
	int num_ = couter_->getInputNum();
	
	this->setFlowerNum(num_);

	char str[20];
	sprintf(str,"%d",getFlowerNum());

	label_showFlowerNum->setString(str);
}

void GiveFlower::setModelType( int type )
{
	m_giveFlowerModel = type;
}

int GiveFlower::getModelType()
{
	return m_giveFlowerModel;
}

void GiveFlower::setFlowerNum( int num )
{
	m_giveFlowerNum = num;
}

int GiveFlower::getFlowerNum()
{
	return m_giveFlowerNum;
}

void GiveFlower::setFlowerName( std::string flowerName )
{
	m_flowerName = flowerName;
}

std::string GiveFlower::getFlowerName()
{
	return m_flowerName;
}

void GiveFlower::UICheckBoxNum1(Ref* pSender, CheckBox::EventType type)
{
	auto check_ = (CheckBox *)pSender;
	if (check_->isSelected())
	{
		setFlowerNum(1);
		checkbox_num1->setSelected(true);
		checkbox_num11->setSelected(false);
		checkbox_num99->setSelected(false);
		checkbox_num365->setSelected(false);
		checkbox_num999->setSelected(false);
		checkbox_Define->setSelected(false);
	}else
	{
		setAllUIcheckNumBoxIsNull();
	}

	label_showFlowerNum->setString("0");
}

void GiveFlower::UICheckBoxNum11(Ref* pSender, CheckBox::EventType type)
{
	auto check_ = (CheckBox *)pSender;
	if (check_->isSelected())
	{
		setFlowerNum(11);
		checkbox_num1->setSelected(false);
		checkbox_num11->setSelected(true);
		checkbox_num99->setSelected(false);
		checkbox_num365->setSelected(false);
		checkbox_num999->setSelected(false);
		checkbox_Define->setSelected(false);
	}else
	{
		setAllUIcheckNumBoxIsNull();
	}

	label_showFlowerNum->setString("0");
}

void GiveFlower::UICheckBoxNum99(Ref* pSender, CheckBox::EventType type)
{
	auto check_ = (CheckBox *)pSender;
	if (check_->isSelected())
	{
		setFlowerNum(99);
		checkbox_num1->setSelected(false);
		checkbox_num11->setSelected(false);
		checkbox_num99->setSelected(true);
		checkbox_num365->setSelected(false);
		checkbox_num999->setSelected(false);
		checkbox_Define->setSelected(false);
	}else
	{
		setAllUIcheckNumBoxIsNull();
	}

	label_showFlowerNum->setString("0");
}

void GiveFlower::UICheckBoxNum365(Ref* pSender, CheckBox::EventType type)
{
	auto check_ = (CheckBox *)pSender;
	if (check_->isSelected())
	{
		setFlowerNum(365);
		checkbox_num1->setSelected(false);
		checkbox_num11->setSelected(false);
		checkbox_num99->setSelected(false);
		checkbox_num365->setSelected(true);
		checkbox_num999->setSelected(false);
		checkbox_Define->setSelected(false);
	}else
	{
		setAllUIcheckNumBoxIsNull();
	}

	label_showFlowerNum->setString("0");
}

void GiveFlower::UICheckBoxNum999(Ref* pSender, CheckBox::EventType type)
{
	auto check_ = (CheckBox *)pSender;
	if (check_->isSelected())
	{
		setFlowerNum(999);
		checkbox_num1->setSelected(false);
		checkbox_num11->setSelected(false);
		checkbox_num99->setSelected(false);
		checkbox_num365->setSelected(false);
		checkbox_num999->setSelected(true);
		checkbox_Define->setSelected(false);
	}else
	{
		setAllUIcheckNumBoxIsNull();
	}

	label_showFlowerNum->setString("0");
}

void GiveFlower::UIcheckBoxDefineNum(Ref* pSender, CheckBox::EventType type)
{
	auto check_ = (CheckBox *)pSender;
	if (check_->isSelected())
	{
		setFlowerNum(0);
		checkbox_num1->setSelected(false);
		checkbox_num11->setSelected(false);
		checkbox_num99->setSelected(false);
		checkbox_num365->setSelected(false);
		checkbox_num999->setSelected(false);

		//
		callBackDefineFlowerNum(NULL,Widget::TouchEventType::ENDED);
	}else
	{
		setAllUIcheckNumBoxIsNull();
	}
}

void GiveFlower::UICheckBoxType1(Ref* pSender, CheckBox::EventType type)
{
	auto check_ = (CheckBox *)pSender;
	if (check_->isSelected())
	{
		setFlowerName(FLOWERTYPE_1);
		checkbox_flowerType1->setSelected(true);
		checkbox_flowerType2->setSelected(false);
		checkbox_flowerType3->setSelected(false);
	}else
	{
		setAllUIcheckTypeBoxIsNull();
	}
}

void GiveFlower::UICheckBoxType2(Ref* pSender, CheckBox::EventType type)
{
	auto check_ = (CheckBox *)pSender;
	if (check_->isSelected())
	{
		setFlowerName(FLOWERTYPE_2);
		checkbox_flowerType1->setSelected(false);
		checkbox_flowerType2->setSelected(true);
		checkbox_flowerType3->setSelected(false);
	}else
	{
		setAllUIcheckTypeBoxIsNull();
	}
}

void GiveFlower::UICheckBoxType3(Ref* pSender, CheckBox::EventType type)
{
	auto check_ = (CheckBox *)pSender;
	if (check_->isSelected())
	{
		setFlowerName(FLOWERTYPE_3);
		checkbox_flowerType1->setSelected(false);
		checkbox_flowerType2->setSelected(false);
		checkbox_flowerType3->setSelected(true);
	}else
	{
		setAllUIcheckTypeBoxIsNull();
	}
}

void GiveFlower::setAllUIcheckTypeBoxIsNull()
{
	setFlowerName("");
	checkbox_flowerType1->setSelected(false);
	checkbox_flowerType2->setSelected(false);
	checkbox_flowerType3->setSelected(false);
}

void GiveFlower::setAllUIcheckNumBoxIsNull()
{
	setFlowerNum(0);

	checkbox_num1->setSelected(false);
	checkbox_num11->setSelected(false);
	checkbox_num99->setSelected(false);
	checkbox_num365->setSelected(false);
	checkbox_num999->setSelected(false);

	checkbox_Define->setSelected(false);
}

void GiveFlower::callBackOpenShop( Ref * obj )
{
	auto goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
	if(goldStoreUI == NULL)
	{
		goldStoreUI = GoldStoreUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI,0,kTagGoldStoreUI);

		goldStoreUI->setIgnoreAnchorPointForPosition(false);
		goldStoreUI->setAnchorPoint(Vec2(0.5f, 0.5f));
		goldStoreUI->setPosition(Vec2(winsize.width/2, winsize.height/2));

		for (int i=0;i <GameView::getInstance()->goldStoreList.size();i++)
		{
			if (GameView::getInstance()->goldStoreList.at(i)->id() == GoldStoreUI::type_aid)
			{
				goldStoreUI->getTab_function()->setDefaultPanelByIndex(i);
				goldStoreUI->IndexChangedEvent(goldStoreUI->getTab_function());
			}
		}
	}
}
