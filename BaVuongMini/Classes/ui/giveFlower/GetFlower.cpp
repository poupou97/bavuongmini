#include "GetFlower.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../extensions/CCRichLabel.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "AppMacros.h"


GetFlower::GetFlower(void)
{
}


GetFlower::~GetFlower(void)
{
}

GetFlower * GetFlower::create(long long id,std::string name,std::string str)
{
	auto flower_ = new GetFlower();
	if (flower_ && flower_->init(id,name,str))
	{
		flower_->autorelease();
		return flower_;
	}
	CC_SAFE_DELETE(flower_);
	return NULL;
}

bool GetFlower::init(long long id,std::string name,std::string str)
{
	if (UIScene::init())
	{
		m_playerId = id;
		m_name = name;
		//m_vipLv = 

		//
		winsize =Size(800,480);
		auto layer = Layer::create();
		layer->setIgnoreAnchorPointForPosition(false);
		layer->setAnchorPoint(Vec2(0.5f,0.5f));
		layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layer->setContentSize(winsize);
		addChild(layer);
		
		auto backGroundSp = cocos2d::extension::Scale9Sprite::create("res_ui/dikuang_new.png");
		backGroundSp->setCapInsets(Rect(0,0,0,0));
		backGroundSp->setPreferredSize(Size(376,198));
		backGroundSp->setAnchorPoint(Vec2(0.5f,0.5f));
		backGroundSp->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layer->addChild(backGroundSp);

		auto backGroundSp_frame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_didia.png");
		backGroundSp_frame->setCapInsets(Rect(13,14,1,1));
		backGroundSp_frame->setPreferredSize(Size(300,108));
		backGroundSp_frame->setAnchorPoint(Vec2(0.5f,1.0f));
		backGroundSp_frame->setPosition(Vec2(backGroundSp->getContentSize().width/2-2,180));
		backGroundSp->addChild(backGroundSp_frame);

		auto labelLink=CCRichLabel::createWithString(str.c_str(),Size(290,50),NULL,NULL,0,20,2);
		labelLink->setAnchorPoint(Vec2(0,1));
		labelLink->setPosition(Vec2(45,backGroundSp->getContentSize().height-30));
		backGroundSp->addChild(labelLink);

		//create button
		auto layerbutton = Layer::create();
		layerbutton->setIgnoreAnchorPointForPosition(false);
		layerbutton->setAnchorPoint(Vec2(0.5f,0.5f));
		layerbutton->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layerbutton->setContentSize(winsize);
		layer->addChild(layerbutton);

		auto buttonPrivate = Button::create();
		buttonPrivate->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		buttonPrivate->setTouchEnabled(true);
		buttonPrivate->setPressedActionEnabled(true);
		buttonPrivate->addTouchEventListener(CC_CALLBACK_2(GetFlower::callbackPrivate, this));
		buttonPrivate->setAnchorPoint(Vec2(0.5f,0.5f));
		buttonPrivate->setScale9Enabled(true);
		buttonPrivate->setContentSize(Size(110,43));
		buttonPrivate->setCapInsets(Rect(18,9,2,23));
		buttonPrivate->setPosition(Vec2(layer->getContentSize().width/2 - buttonPrivate->getContentSize().width + 30,
																layer->getContentSize().height/2 - backGroundSp->getContentSize().height/2 +48));
		layerbutton->addChild(buttonPrivate);

		auto label_private = Label::createWithTTF(StringDataManager::getString("giveFlower_private_"), APP_FONT_NAME, 18);
		label_private->setAnchorPoint(Vec2(0.5f,0.5f));
		label_private->setPosition(Vec2(0,0));
		buttonPrivate->addChild(label_private);

		auto buttonThanks = Button::create();
		buttonThanks->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		buttonThanks->setTouchEnabled(true);
		buttonThanks->setPressedActionEnabled(true);
		buttonThanks->addTouchEventListener(CC_CALLBACK_2(GetFlower::callBackThank, this));
		buttonThanks->setAnchorPoint(Vec2(0.5f,0.5f));
		buttonThanks->setScale9Enabled(true);
		buttonThanks->setContentSize(Size(110,43));
		buttonThanks->setCapInsets(Rect(18,9,2,23));
		buttonThanks->setPosition(Vec2(layer->getContentSize().width/2 + buttonPrivate->getContentSize().width - 30,buttonPrivate->getPosition().y));
		layerbutton->addChild(buttonThanks);

		auto label_thanks = Label::createWithTTF(StringDataManager::getString("giveFlower_thanks"), APP_FONT_NAME, 20);
		label_thanks->setAnchorPoint(Vec2(0.5f,0.5f));
		label_thanks->setPosition(Vec2(0,0));
		buttonThanks->addChild(label_thanks);


		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void GetFlower::onEnter()
{
	UIScene::onEnter();
}

void GetFlower::onExit()
{
	UIScene::onExit();
}

void GetFlower::callbackPrivate(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		int countryId = 0;
		int vipLv = 0;
		int lv = 0;
		int pressionId = 0;

		auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
		mainscene_->addPrivateChatUi(m_playerId, m_name, countryId, vipLv, lv, pressionId);

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GetFlower::callBackThank(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5141, (void *)m_playerId);

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}
