
#ifndef _UI_GIVEFLOWER_H_
#define _UI_GIVEFLOWER_H_

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

#define FLOWERTYPE_1 "baihe"
#define FLOWERTYPE_2 "fenmeigui"
#define FLOWERTYPE_3 "hongmeigui"

class RichTextInputBox;
class GiveFlower:public UIScene
{
public:
	GiveFlower(void);
	~GiveFlower(void);

	static GiveFlower * create(long long id,std::string name,int pression,int level,int countryId,int vipLv);
	bool init(long long id,std::string name,int pression,int level,int countryId,int vipLv);

	void onEnter();
	void onExit();

	//
	void callBackFlowerType(Ref * obj);

	//set flower type
	void setFlowerName(std::string flowerName);
	std::string getFlowerName();
	//set flower  num
	void setFlowerNum(int num);
	int getFlowerNum();

	//model flower num
	void callBackModelFlowerNum(Ref * obj);
	//use define num
	void callBackDefineFlowerNum(Ref *pSender, Widget::TouchEventType type);

	void showFlowerNum(Ref * obj);

	void setModelType(int type);
	int getModelType();
	void setModelPublic(Ref* pSender, CheckBox::EventType type);
	void setModelPrivate(Ref* pSender, CheckBox::EventType type);

	//
	void setAllUIcheckTypeBoxIsNull();
	void setAllUIcheckNumBoxIsNull();

	//uicheckbox is select flower type
	void UICheckBoxType1(Ref* pSender, CheckBox::EventType type);
	void UICheckBoxType2(Ref* pSender, CheckBox::EventType type);
	void UICheckBoxType3(Ref* pSender, CheckBox::EventType type);

	//uicheckbox select flower num
	void UICheckBoxNum1(Ref* pSender, CheckBox::EventType type);
	void UICheckBoxNum11(Ref* pSender, CheckBox::EventType type);
	void UICheckBoxNum99(Ref* pSender, CheckBox::EventType type);
	void UICheckBoxNum365(Ref* pSender, CheckBox::EventType type);
	void UICheckBoxNum999(Ref* pSender, CheckBox::EventType type);

	void UIcheckBoxDefineNum(Ref* pSender, CheckBox::EventType type);

	void callBackOpenShop(Ref * obj);

	void callBackGiveFlower(Ref *pSender, Widget::TouchEventType type);
	void callBackExit(Ref *pSender, Widget::TouchEventType type);

public:
	CheckBox * checkbox_public;
	CheckBox * checkbox_private;

	CheckBox * checkbox_flowerType1;
	CheckBox * checkbox_flowerType2;
	CheckBox * checkbox_flowerType3;


	CheckBox * checkbox_num1;
	CheckBox * checkbox_num11;
	CheckBox * checkbox_num99;
	CheckBox * checkbox_num365;
	CheckBox * checkbox_num999;

	CheckBox * checkbox_Define;
public:
	struct GiveFlowerStruct
	{
		long long playerId;
		std::string playerName;
		int flowerNumber;
		std::string flowerName;
		int isShowName;
		std::string flowerSaid;
	};
private:
	Size winsize;
	RichTextInputBox * inputFlowerNum;
	RichTextInputBox * inputFlowerSaid;

	Text * label_showFlowerNum;

	long long m_playerId;
	std::string m_playerName;

	std::string m_flowerName;
	int m_giveFlowerNum;
	int m_giveFlowerModel;
};

#endif