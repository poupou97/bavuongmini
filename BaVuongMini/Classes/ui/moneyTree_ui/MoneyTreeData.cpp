#include "MoneyTreeData.h"
#include "GameView.h"
#include "MoneyTreeTimeManager.h"
#include "../../gamescene_state/MainScene.h"

MoneyTreeData * MoneyTreeData::s_moneyTreeData = NULL;

MoneyTreeData::MoneyTreeData(void)
	:m_nMaxNum(0)
	,m_nHasUseNum(0)
	,m_nNeedTime(0)
	,m_nCurrentCount(0)
{

}

MoneyTreeData::~MoneyTreeData(void)
{

}

MoneyTreeData * MoneyTreeData::instance()
{
	if (NULL == s_moneyTreeData)
	{
		s_moneyTreeData = new MoneyTreeData();
	}

	return s_moneyTreeData;
}

void MoneyTreeData::set_maxNum( int nMaxNum )
{
	this->m_nMaxNum = nMaxNum;
}

int MoneyTreeData::get_maxNum()
{
	return this->m_nMaxNum;
}

void MoneyTreeData::set_hasUseNum( int nHasUseNum )
{
	this->m_nHasUseNum = nHasUseNum;
}

int MoneyTreeData::get_hasUserNum()
{
	return this->m_nHasUseNum;
}

void MoneyTreeData::set_needTime( int nNeedTime )
{
	this->m_nNeedTime = nNeedTime;
}

int MoneyTreeData::get_needTime()
{
	return this->m_nNeedTime;
}

void MoneyTreeData::set_currentCount( int nCurrentCount )
{
	this->m_nCurrentCount = nCurrentCount;
}

int MoneyTreeData::get_currentCount()
{
	return this->m_nCurrentCount;
}

std::string MoneyTreeData::get_numLeft()
{
	// [ʣ�����/�ܴ���]
	int nMaxCount = this->get_maxNum();
	int nCurrentCount = this->get_currentCount();

	int nLeftCount = nMaxCount - nCurrentCount + 1;

	char charLeftCount[10];
	sprintf(charLeftCount, "%d", nLeftCount);

	char charMaxCount[10];
	sprintf(charMaxCount, "%d", nMaxCount);

	std::string strLeftCount = "";
	strLeftCount.append(charLeftCount);
	strLeftCount.append("/");
	strLeftCount.append(charMaxCount);

	return strLeftCount;
}

bool MoneyTreeData::hasAllGiftGet()
{
	bool bFlag = false;

	int nMaxCount = this->get_maxNum();
	int nCurrentCount = this->get_currentCount();

	int nLeftTime = this->get_needTime();

	if (nCurrentCount > nMaxCount)
	{
		bFlag = true;
	}

	return bFlag;
}

std::string MoneyTreeData::get_strTimeLeft()
{
	std::string strTimeLeft = "";

	if(NULL != GameView::getInstance()->getMainUIScene())
	{
		// �����������ͬ��
		auto pMoneyTreeTimeManager = (MoneyTreeTimeManager *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMoneyTreeManager);
		if (NULL != pMoneyTreeTimeManager)
		{
			strTimeLeft = pMoneyTreeTimeManager->get_timeLeftStr();
		}
	}
	
	return strTimeLeft;
}
