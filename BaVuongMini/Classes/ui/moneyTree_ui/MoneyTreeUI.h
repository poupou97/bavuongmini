#ifndef _UI_MONEYTREE_MONEYTREEUI_H_
#define _UI_MONEYTREE_MONEYTREEUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ҡǮ�UI
 * @author liuliang
 * @version 0.1.0
 * @date 2014.11.20
 */

class MoneyTreeUI:public UIScene
{
public:
	MoneyTreeUI(void);
	~MoneyTreeUI(void);

public:
	static Layout * s_pPanel;
	static MoneyTreeUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void update(float dt);

private:
	Button* m_pBtn_begin;								// �ҡһҡ
	
	Button* m_pBtn_begin_disable;						// ҡһҡ������״̬
	Text* m_pLabel_time;								// ����ʱ

	ImageView* m_pImgView_bar;						// ʣ���
	Text* m_pLabel_num;								// XX/XX�ʣ����/��ܴ��
	

private:
	void initUI();
	std::string getLeftCount();							// ��ȡ [ʣ����/��ܴ��]
	float getBarValue();								// ��ʣ����ı���ֵ
	
	void initBtnAndTextStatus(std::string strTimeLeft);

private:
	void callBackBtnClolse(Ref *pSender, Widget::TouchEventType type);																	// �رհ�ť����Ӧ
	void callBackBtnBegin(Ref *pSender, Widget::TouchEventType type);																	// ��ʼ ҡǮ�
	

public:
	void initDataFromInternet();						// �ӷ������ȡ��ݣ����UI£�
	void addMoneyTreeAnmation();						// ҡǮ���ҡǮЧ�
};

#endif

