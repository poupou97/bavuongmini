#ifndef _UI_MONEYTREE_MONEYTREETIMEMANAGER_H_
#define _UI_MONEYTREE_MONEYTREETIMEMANAGER_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ҡǮ��ģ�飨���ڼ��ʱ�䣩
 * @author liuliang
 * @version 0.1.0
 * @date 2014.11.19
 */

class MoneyTreeTimeManager : public cocos2d::Node
{
public:
	MoneyTreeTimeManager(void);
	~MoneyTreeTimeManager(void);

public:
	static MoneyTreeTimeManager* create();

	virtual bool init();

private:
	std::string m_strTimeLeft;

	int m_nLeftTime;
	int m_nShowTime;

private:
	void updateTime(float dt);

	std::string timeFormatToString(int nTime);
	void refreshData();

public:
	void set_timeLeftStr(std::string strTimeLeft);
	std::string get_timeLeftStr();

	void initDataFromIntent();										// �ӷ�������ݳ�ʼ��data
};

#endif

