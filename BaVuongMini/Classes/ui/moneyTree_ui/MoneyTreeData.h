#ifndef _UI_MONEYTREE_MONEYTREEDATA_H_
#define _UI_MONEYTREE_MONEYTREEDATA_H_

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
//USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ҡǮ��ģ�飨���ڱ�����������ݣ�
 * @author liuliang
 * @version 0.1.0
 * @date 2014.11.19
 */

class MoneyTreeData
{
public:
	static MoneyTreeData * s_moneyTreeData;
	static MoneyTreeData * instance();

private:
	MoneyTreeData(void);
	~MoneyTreeData(void);

private:
	int m_nMaxNum;
	int m_nHasUseNum;
	int m_nNeedTime;
	int m_nCurrentCount;						// �ڼ���ҡһҡ����1��ʼ��

public:
	void set_maxNum(int nMaxNum);
	int get_maxNum();

	void set_hasUseNum(int nHasUseNum);
	int get_hasUserNum();

	void set_needTime(int nNeedTime);
	int get_needTime();

	void set_currentCount(int nCurrentCount);
	int get_currentCount();						

	// for LiuZhenXing use
	std::string get_numLeft();							// ������ʾ XX/XX
	bool hasAllGiftGet();								// ҡǮ�������Ƿ�ȫ������

	// ��ȡʣ��ʱ��[���ʣ��ʱ�䲻Ϊ00:00�Ļ�����ʾXX:XX��
	// ���ʣ��ʱ��Ϊ00:00�Ļ�������ʾStringDataManager::getString("MoneyTree_canGetInfo");]
	std::string get_strTimeLeft();							
};

#endif

