#include "MoneyTreeUI.h"

#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"
#include "GameView.h"
#include "MoneyTreeTimeManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "MoneyTreeData.h"
#include "cocostudio\CCArmature.h"
#include "cocostudio\CCSGUIReader.h"

using namespace CocosDenshion;

const Size SizeUIView = Size(300, 370);

#define COCOSTUDIO_PANEL_WIDTH 300
#define COCOSTUDIO_PANEL_HEIGHT 370

Layout * MoneyTreeUI::s_pPanel;

MoneyTreeUI::MoneyTreeUI(void)
{

}

MoneyTreeUI::~MoneyTreeUI(void)
{

}

MoneyTreeUI* MoneyTreeUI::create()
{
	auto pMoneyTreeUI = new MoneyTreeUI();
	if (pMoneyTreeUI && pMoneyTreeUI->init())
	{
		pMoneyTreeUI->autorelease();
		return pMoneyTreeUI;
	}
	CC_SAFE_DELETE(pMoneyTreeUI);
	return NULL;
}

bool MoneyTreeUI::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		// ���ü����أ�������ǰ���
		s_pPanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/yaoqianshu_1.json");
		s_pPanel->setAnchorPoint(Vec2(0, 0));
		s_pPanel->getVirtualRenderer()->setContentSize(Size(COCOSTUDIO_PANEL_WIDTH, COCOSTUDIO_PANEL_HEIGHT));
		s_pPanel->setPosition(Vec2((winSize.width - COCOSTUDIO_PANEL_WIDTH) / 2, (winSize.height - COCOSTUDIO_PANEL_HEIGHT) / 2));
		s_pPanel->setScale(1.0f);
		s_pPanel->setTouchEnabled(true);
		m_pLayer->addChild(s_pPanel);

		initUI();

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winSize);

		this->scheduleUpdate();

		return true;
	}
	return false;
}

void MoneyTreeUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void MoneyTreeUI::onExit()
{
	UIScene::onExit();

	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

void MoneyTreeUI::callBackBtnClolse(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void MoneyTreeUI::callBackBtnBegin(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		int nCurrentCount = MoneyTreeData::instance()->get_currentCount();

		// ����������ڼ��ε�ҡһҡ��
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5138, (void *)nCurrentCount);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MoneyTreeUI::initUI()
{
	auto pBtnClose = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnabled(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addTouchEventListener(CC_CALLBACK_2(MoneyTreeUI::callBackBtnClolse, this));

	// ��ʼҡǮ
	m_pBtn_begin = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_begin");
	m_pBtn_begin->setTouchEnabled(true);
	m_pBtn_begin->setPressedActionEnabled(true);
	m_pBtn_begin->setVisible(false);
	m_pBtn_begin->addTouchEventListener(CC_CALLBACK_2(MoneyTreeUI::callBackBtnBegin, this));

	// ҡһҡ�����btn
	m_pBtn_begin_disable = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_begin_disable");
	m_pBtn_begin_disable->setTouchEnabled(false);
	m_pBtn_begin_disable->setPressedActionEnabled(false);
	m_pBtn_begin_disable->setVisible(true);
	
	// �ҡһҡ����ʱ
	m_pLabel_time = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_timeLeft");
	m_pLabel_time->setVisible(true);

	// ҡһҡʣ���
	m_pImgView_bar = (ImageView*)Helper::seekWidgetByName(s_pPanel, "ImageView_bar");
	m_pImgView_bar->setVisible(true);

	// XX/XX�ʣ����/��ܴ��
	m_pLabel_num = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_num");
	m_pLabel_num->setVisible(true);
}

void MoneyTreeUI::initDataFromInternet()
{
	// ��ʱ��ʣ���[�ʣ����/��ܴ��]
	if (NULL != GameView::getInstance()->getMainUIScene())
	{
		auto pMoneyTreeTimeManager = (MoneyTreeTimeManager *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMoneyTreeManager);
		if (NULL != pMoneyTreeTimeManager)
		{
			// ��ʱ
			std::string strTimeLeft = pMoneyTreeTimeManager->get_timeLeftStr();
			m_pLabel_time->setString(strTimeLeft.c_str());

			// ��ť���ı�״̬
			this->initBtnAndTextStatus(strTimeLeft);

			// [ʣ����/��ܴ��]
			std::string strLeftCount = this->getLeftCount();
			m_pLabel_num->setString(strLeftCount.c_str());

			// �ʣ���
			float scale = this->getBarValue();
			m_pImgView_bar->setTextureRect(Rect(0, 0, m_pImgView_bar->getContentSize().width * scale, m_pImgView_bar->getContentSize().height));
		}
	}
}

void MoneyTreeUI::update( float dt )
{
	if (NULL != GameView::getInstance()->getMainUIScene())
	{
		// ��ʱ��ʣ���[�ʣ����/��ܴ��]
		auto pMoneyTreeTimeManager = (MoneyTreeTimeManager *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMoneyTreeManager);
		if (NULL != pMoneyTreeTimeManager)
		{
			std::string strTimeLeft = pMoneyTreeTimeManager->get_timeLeftStr();

			this->initBtnAndTextStatus(strTimeLeft);
		}	
	}
}

std::string MoneyTreeUI::getLeftCount()
{
	// [�ʣ����/��ܴ��]
	int nMaxCount = MoneyTreeData::instance()->get_maxNum();
	int nCurrentCount = MoneyTreeData::instance()->get_currentCount();

	int nLeftCount = nMaxCount - nCurrentCount + 1;

	char charLeftCount[10];
	sprintf(charLeftCount, "%d", nLeftCount);

	char charMaxCount[10];
	sprintf(charMaxCount, "%d", nMaxCount);

	std::string strLeftCount = "";
	strLeftCount.append(charLeftCount);
	strLeftCount.append("/");
	strLeftCount.append(charMaxCount);

	return strLeftCount;
}

float MoneyTreeUI::getBarValue()
{
	// [�ʣ����/��ܴ��]
	int nMaxCount = MoneyTreeData::instance()->get_maxNum();
	int nCurrentCount = MoneyTreeData::instance()->get_currentCount();

	int nLeftCount = nMaxCount - nCurrentCount + 1;

	float scale = (float)nLeftCount / (float)nMaxCount;

	return scale;
}

void MoneyTreeUI::addMoneyTreeAnmation()
{
	auto m_pImgView_tree = (ImageView*)Helper::seekWidgetByName(s_pPanel, "ImageView_tree");

	float xPos = s_pPanel->getPosition().x + m_pImgView_tree->getPosition().x + 7;
	float yPos = s_pPanel->getPosition().y + m_pImgView_tree->getPosition().y - 29;

	// �ҡǮ��ζ��Ķ���
	//�ӵ����ļ��첽���ض���
	cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("res_ui/uiflash/yaoqianshu/yaoqianshu0.png","res_ui/uiflash/yaoqianshu/yaoqianshu0.plist","res_ui/uiflash/yaoqianshu/yaoqianshu.ExportJson");
	//��ݶ�����ƴ����������
	auto armature = cocostudio::Armature::create("yaoqianshu");
	//鲥��ָ�����
	armature->getAnimation()->playWithIndex(0);
	//��޸����
	armature->setScale(1.0f);
	//����ö�������λ�
	armature->setPosition(xPos, yPos);
	//���ӵ���ǰҳ�
	this->addChild(armature);

	// ��ҵ���Ķ���������һ��һ�
	auto winSize = Director::getInstance()->getVisibleSize();

	float yPosCoin = s_pPanel->getPosition().y + m_pImgView_tree->getPosition().y + 40;
	float xPosLeft = winSize.width / 2 - 30;
	float xPosRight = winSize.width / 2 + 30;

	std::string strAnm = "animation/texiao/particledesigner/jinbi.plist";

	auto particleEffect_left = ParticleSystemQuad::create(strAnm.c_str());
	particleEffect_left->setAnchorPoint(Vec2(0.5f,0.5f));
	particleEffect_left->setPosition(Vec2(xPosLeft, yPosCoin));
	particleEffect_left->setScale(0.3f);
	particleEffect_left->setDuration(2.5f);
	particleEffect_left->setAutoRemoveOnFinish(true);

	auto particleEffect_right = ParticleSystemQuad::create(strAnm.c_str());
	particleEffect_right->setAnchorPoint(Vec2(0.5f,0.5f));
	particleEffect_right->setPosition(Vec2(xPosRight, yPosCoin));
	particleEffect_right->setScale(0.3f);
	particleEffect_right->setDuration(2.5f);
	particleEffect_right->setAutoRemoveOnFinish(true);

	this->addChild(particleEffect_left);
	this->addChild(particleEffect_right);
}

void MoneyTreeUI::initBtnAndTextStatus( std::string strTimeLeft )
{
	std::string timeString = "";
	const char *str1 = StringDataManager::getString("MoneyTree_canGetInfo");
	timeString.append(str1);

	//��ж Ͻ�Ҫ��ʾ���Ƿ�Ϊ������ȡ�������ǣ��򴴽���Ч����� ��Ƴ���Ч�������ǰ����Ч���ڵĻ���
	if (strTimeLeft == timeString)
	{
		if (MoneyTreeData::instance()->hasAllGiftGet())
		{
			strTimeLeft = "00:00";

			m_pBtn_begin->setVisible(false);
			m_pBtn_begin_disable->setVisible(true);
			m_pLabel_time->setVisible(true);

			m_pLabel_time->setString(strTimeLeft.c_str());
		}
		else
		{
			m_pBtn_begin->setVisible(true);
			m_pBtn_begin_disable->setVisible(false);
			m_pLabel_time->setVisible(false);
		}	
	}
	else
	{
		m_pBtn_begin->setVisible(false);
		m_pBtn_begin_disable->setVisible(true);
		m_pLabel_time->setVisible(true);

		m_pLabel_time->setString(strTimeLeft.c_str());
	}
}






