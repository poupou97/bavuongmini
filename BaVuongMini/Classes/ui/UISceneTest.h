#ifndef _UI_UISCENETEST_H_
#define _UI_UISCENETEST_H_

#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UISceneTest : public UIScene
{
public:
	UISceneTest();
	virtual ~UISceneTest();

	static UISceneTest * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

private:

};

#endif