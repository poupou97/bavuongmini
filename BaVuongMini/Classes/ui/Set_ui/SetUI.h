#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class SetUI:public UIScene
{
public:
	SetUI(void);
	~SetUI(void);

	static SetUI *create();
	bool init();
	void onEnter();
	void onExit();

	void reLoading(Ref *pSender, Widget::TouchEventType type);
	void SureToReLoading(Ref *obj);

	void callBackExit(Ref *pSender, Widget::TouchEventType type);
	void callBackError(Ref *pSender, Widget::TouchEventType type);

	 void tabIndexChangedEvent(Ref* pSender);
	//start by:liutao
	void closeWindow(Ref * obj);

	void sliderMusicEvent(Ref *pSender, ui::Slider::EventType type);

	void sliderSoundEvent(Ref *pSender, ui::Slider::EventType type);

	void getUserDefault();
	void callBackShowNearbyPlayer(Ref* pSender, CheckBox::EventType type);
protected:
	int m_nMusic;
	int m_nSound;
	//above  by:liutao
};

