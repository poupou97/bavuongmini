#include "SetUI.h"
#include "GameStateBasic.h"
#include "../../login_state/LoginState.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../extensions/UITab.h"
#include "SimpleAudioEngine.h"
#include "../../login_state/Login.h"
#include "GameAudio.h"
#include "../../GameUserDefault.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../utils/GameUtils.h"

using namespace CocosDenshion;

SetUI::SetUI(void)
{
}


SetUI::~SetUI(void)
{
}

SetUI * SetUI::create()
{
	auto setui=new SetUI();
	if (setui && setui->init())
	{
		setui->autorelease();
		return setui;
	}
	CC_SAFE_DELETE(setui);
	return NULL;
}

bool SetUI::init()
{
	if (UIScene::init())
	{
		auto size=Director::getInstance()->getVisibleSize();
		//get userdefault.xml by:liutao 
		SetUI::getUserDefault();

		//SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(m_nMusic);
		//SimpleAudioEngine::getInstance()->playBackgroundMusic("music0.mid", true);

		auto layer=Layer::create();
		layer->setIgnoreAnchorPointForPosition(false);
		layer->setAnchorPoint(Vec2(0.5f,0.5f));
		layer->setPosition(Vec2(size.width/2,size.height/2));
		layer->setContentSize(Size(UI_DESIGN_RESOLUTION_WIDTH,UI_DESIGN_RESOLUTION_HEIGHT));
		addChild(layer);

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(size);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		if(LoadSceneLayer::setLayout->getParent() != NULL)
		{
			LoadSceneLayer::setLayout->removeFromParentAndCleanup(false);
		}

		auto setPanel=LoadSceneLayer::setLayout;
		setPanel->setAnchorPoint(Vec2(0.5f,0.5f));
		setPanel->setPosition(Vec2(size.width/2,size.height/2));
		m_pLayer->addChild(setPanel);

		auto showNearbyPlayer =(CheckBox *)Helper::seekWidgetByName(setPanel,"CheckBox_seeOther");
		showNearbyPlayer->setTouchEnabled(true);
		showNearbyPlayer->addEventListener(CC_CALLBACK_2(SetUI::callBackShowNearbyPlayer,this));
		showNearbyPlayer->setSelected(UserDefault::getInstance()->getBoolForKey("showNearbyPlayer", true));

		auto btn_reload= (Button *)Helper::seekWidgetByName(setPanel,"Button_return_landing");
		btn_reload->setTouchEnabled(true);
		btn_reload->setPressedActionEnabled(true);
		btn_reload->addTouchEventListener(CC_CALLBACK_2(SetUI::reLoading, this));


		auto btn_Exit= (Button *)Helper::seekWidgetByName(setPanel,"Button_close");
		btn_Exit->setTouchEnabled(true);
		btn_Exit->setPressedActionEnabled(true);
		btn_Exit->addTouchEventListener(CC_CALLBACK_2(SetUI::callBackExit, this));
	
		//setting interface	by:liutao
		/*
		//close app
		Button * btn_ExitWindow = (Button *)Helper::seekWidgetByName(setPanel,"Button_exit");
		btn_ExitWindow->setTouchEnabled(true);
		btn_ExitWindow->setPressedActionEnabled(true);
		btn_ExitWindow->addTouchEventListener(CC_CALLBACK_2(SetUI::closeWindow));
		*/
		auto btn_Set = (Button *)Helper::seekWidgetByName(setPanel,"Button_set");
		btn_Set->loadTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");
		btn_Set->setTouchEnabled(true);
		btn_Set->setVisible(false);
		//btn_Set->setPressedActionEnabled(true);
		btn_Set->addTouchEventListener(CC_CALLBACK_2(SetUI::callBackError, this));

		auto btn_ContactUs = (Button *)Helper::seekWidgetByName(setPanel,"Button_ContactUs");
		btn_ContactUs->loadTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");
		btn_ContactUs->setTouchEnabled(true);
		btn_ContactUs->setVisible(false);
		//btn_ContactUs->setPressedActionEnabled(true);
		btn_ContactUs->addTouchEventListener(CC_CALLBACK_2(SetUI::callBackError, this));

		auto slider_Music = (Slider *)Helper::seekWidgetByName(setPanel,"Slider_music");
		slider_Music->setTouchEnabled(true);
		slider_Music->setPercent(m_nMusic);
		slider_Music->addEventListener(CC_CALLBACK_2(SetUI::sliderMusicEvent,this));

		auto slider_Sound = (Slider *)Helper::seekWidgetByName(setPanel,"Slider_sound");
		slider_Sound->setTouchEnabled(true);
		slider_Sound->setPercent(m_nSound);
		slider_Sound->addEventListener(CC_CALLBACK_2(SetUI::sliderSoundEvent,this));

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(size);
		return true;
	}
	return false;
}

void SetUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void SetUI::onExit()
{
	UIScene::onExit();
}

void SetUI::callBackShowNearbyPlayer(Ref* pSender, CheckBox::EventType type)
{

	/*switch (type)
	{
	case CheckBox::EventType::SELECTED:

		break;

	case CheckBox::EventType::UNSELECTED:

		break;

	default:
		break;
	}*/

	auto checkBox =dynamic_cast<CheckBox *>(pSender);
	int select_ =checkBox->isSelected();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5231, (void*)select_);
	//UserDefault::getInstance()->setBoolForKey("showNearbyPlayer", select_);
}

 void SetUI::tabIndexChangedEvent(Ref* pSender)
 {

 }

void SetUI::callBackError(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillui_nothavefuction"));
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void SetUI::callBackExit(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		float volume = (float)m_nMusic / 100.0f;
		SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(volume);
		UserDefault::getInstance()->setIntegerForKey(MUSIC, m_nMusic);

		float effectVolume = (float)m_nSound / 100.0f;
		SimpleAudioEngine::getInstance()->setEffectsVolume(effectVolume);
		UserDefault::getInstance()->setIntegerForKey(SOUND, m_nSound);

		UserDefault::getInstance()->flush();

		if (volume <= 0)
			SimpleAudioEngine::getInstance()->stopBackgroundMusic();

		this->closeAnim();
		SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void SetUI::reLoading(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameView::getInstance()->showPopupWindow(StringDataManager::getString("con_sureToChangeId"), 2, this, CC_CALLFUNCO_SELECTOR(SetUI::SureToReLoading), NULL);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}
/*
void SetUI::closeWindow(Ref * obj)
{
	UserDefault::getInstance()->flush();
	//SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	Director::getInstance()->end();
}
*/

void SetUI::sliderMusicEvent(Ref *pSender, ui::Slider::EventType type)
{
    switch (type)
    {
		case Slider::EventType::ON_PERCENTAGE_CHANGED:
        {
			auto slider = dynamic_cast<Slider*>(pSender);
            int percent = slider->getPercent();
			m_nMusic = percent;
			float volume = (float)percent/100.0f;
			SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(volume);
			//CCLOG("%d", percent);
			//UserDefault::getInstance()->setIntegerForKey(MUSIC, percent);
			//music regulate
        }
            break;
            
        default:
            break;
    }
}

void SetUI::sliderSoundEvent(Ref *pSender, ui::Slider::EventType type)
{
    switch (type)
    {
        case Slider::EventType::ON_PERCENTAGE_CHANGED:
        {
			auto slider = dynamic_cast<Slider*>(pSender);
            int percent = slider->getPercent();
			m_nSound = percent;
			float volume = (float)percent/100.0f;
			SimpleAudioEngine::getInstance()->setEffectsVolume(volume);
			//SimpleAudioEngine::getInstance()->playEffect("event.mid", true);

			//CCLOG("%d", percent);
			//UserDefault::getInstance()->setIntegerForKey(SOUND, percent);
			//sound regulate
        }
            break;
            
        default:
            break;
    }
}

void SetUI::getUserDefault()
{
	m_nMusic = UserDefault::getInstance()->getIntegerForKey(MUSIC,50);
    CCLOG("m_nMusic is %d", m_nMusic);

	m_nSound = UserDefault::getInstance()->getIntegerForKey(SOUND,70);
    CCLOG("m_nSound is %d", m_nSound);
}

void SetUI::SureToReLoading( Ref *obj )
{
	float volume = (float)m_nMusic/100.0f;
	SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(volume);
	UserDefault::getInstance()->setIntegerForKey(MUSIC, m_nMusic);

	float effectVolume = (float)m_nSound/100.0f;
	SimpleAudioEngine::getInstance()->setBackgroundMusicVolume(effectVolume);
	UserDefault::getInstance()->setIntegerForKey(SOUND, m_nSound);

	UserDefault::getInstance()->flush();
	//SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1002);   // request logout


	//GameState* pScene = new LoginState();
	//if (pScene)
	//{
	//	pScene->runThisState();
	//	pScene->release();
	//}
}
