#include "SearchGeneral.h"
#include "../../utils/GameUtils.h"
#include "../../legend_engine/LegendAAnimation.h"
#include "../../legend_engine/LegendAnimationCache.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/MainAnimationScene.h"
#include "cocostudio\CCSGUIReader.h"

#define GENERAL_PATH_NUM 53
#define QUANTITY_PATH_NUM 25
#define ROW_NUM 5
#define SUCCESS_NUM 6


#define LAYERTAG_GENERALICON 255
#define LAYERTAG_SCROLL 155

//game difficult type
#define GAME_TIME_1 (60*1000)
#define SHOWGENERALICON_TIME_1 2//1 s show generalIcon

#define GAME_TIME_2 (50*1000)
#define SHOWGENERALICON_TIME_2 6// 3 s

#define GAME_TIME_3 (30*1000)
#define SHOWGENERALICON_TIME_3 8// 4s

enum {
	kStateStart = 0,
	kStatePlay = 1,
	kStateSuccess = 2,
	kStateFail = 3,
	kStateWait = 4,
};

SearchGeneral::SearchGeneral(void)
{
	m_iState = kStateStart;
	m_iSuccessNum = 0;

	pass_timePrecfect = 100;
	m_touchTime = 0;
}


SearchGeneral::~SearchGeneral(void)
{
}

SearchGeneral * SearchGeneral::create(int diffType)
{
	auto searchGeneral=new SearchGeneral();
	if (searchGeneral && searchGeneral->init(diffType))
	{
		searchGeneral->autorelease();
		return searchGeneral;
	}
	CC_SAFE_DELETE(searchGeneral);
	return NULL;
}

bool SearchGeneral::init(int diffType)
{
	if (UIScene::init())
	{
		m_level = diffType;
		switch(m_level)
		{
		case 1:
			{
				m_showIconTime = GAME_TIME_1;
				m_gameEndTIme = SHOWGENERALICON_TIME_1;
			}break;
		case 2:
			{
				m_showIconTime = GAME_TIME_2;
				m_gameEndTIme = SHOWGENERALICON_TIME_2;
			}break;
		case 3:
			{
				m_showIconTime = GAME_TIME_3;
				m_gameEndTIme = SHOWGENERALICON_TIME_3;
			}break;
		}

		winSize=Director::getInstance()->getVisibleSize();

		auto mengban = ImageView::create();
		mengban->loadTexture("newcommerstory/zhezhao.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winSize);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		mengban->setOpacity(240);
		m_pLayer->addChild(mengban);

		auto panel_ = (Layout*) cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/takegeneral_1.json");
		panel_->setAnchorPoint(Vec2(0.5f,0.5f));
		panel_->setPosition(Vec2(winSize.width/2,winSize.height/2));
		m_pLayer->addChild(panel_);

		auto label_des = (Text *)Helper::seekWidgetByName(panel_,"Label_gameDes");
		label_des->setTextAreaSize(Size(128,0));

		m_Layer= Layer::create();
		m_Layer->setIgnoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(Vec2(0.5f,0.5f));
		m_Layer->setPosition(Vec2(winSize.width/2,winSize.height/2));
		m_Layer->setContentSize(Size(800,480));
		m_Layer->setLocalZOrder(10);
		this->addChild(m_Layer);
		//
		const char * firstStr = StringDataManager::getString("search_diaoshi_sha");
		const char * secondStr = StringDataManager::getString("search_diaoshi_chang");
		const char * thirdStr = StringDataManager::getString("search_diaoshi_dian");
		const char * fourStr = StringDataManager::getString("search_diaoshi_bing");
		auto atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,fourStr);
		atmature->setPosition(Vec2(30,240));
		m_Layer->addChild(atmature);

		//Button_start
		btn_start = (Button *)Helper::seekWidgetByName(panel_,"Button_start");
		btn_start->setTouchEnabled(true);
		btn_start->addTouchEventListener(CC_CALLBACK_2(SearchGeneral::callBackStart, this));
		btn_start->setPressedActionEnabled(true);

		//start button action
		auto action1 = ScaleTo::create(1.0f,1.1f);
		auto action2 = ScaleTo::create(1.0f,0.9);

		auto seq_ = Sequence::create(action1,action2,NULL);
		btn_start->runAction(RepeatForever::create(seq_));

		//Button_close
		auto btn_close = (Button *)Helper::seekWidgetByName(panel_,"Button_close");
		btn_close->setTouchEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(SearchGeneral::callBackClose, this));
		btn_close->setPressedActionEnabled(true);
		
		//ImageView_time
		
		//ImageView_generalIcon
		image_icon = (ImageView *)Helper::seekWidgetByName(panel_,"ImageView_generalIcon");

		//Label_searchResult
		label_result = (Text*)Helper::seekWidgetByName(panel_,"Label_searchResult");

		//label start
		label_star = (Text*)Helper::seekWidgetByName(panel_,"Label_37");
		label_star->setString(StringDataManager::getString("SearchGeneral_Start"));

		loadingTime = LoadingBar::create();
		loadingTime->loadTexture("res_ui/takegeneral/time2.png");
		loadingTime->setPosition(Vec2(751,207));
		loadingTime->setPercent(100);
		loadingTime->setRotation(270);
		m_Layer->addChild(loadingTime);

		//------------
		generalIConMengban = cocos2d::extension::Scale9Sprite::create(Rect(7, 7, 1, 1) , "res_ui/zhezhao80.png");
		generalIConMengban->setOpacity(255);
		generalIConMengban->setPreferredSize(Size(510,401));
		generalIConMengban->setAnchorPoint(Vec2(0,0));
		generalIConMengban->setPosition(Vec2(220,41));
		m_Layer->addChild(generalIConMengban,2);
		
		label_mengban_ = Label::createWithTTF(StringDataManager::getString("SearchGeneral_Start_labelShow"),APP_FONT_NAME,30);
		label_mengban_->setAnchorPoint(Vec2(0.5f,0.5f));
		label_mengban_->setPosition(Vec2(generalIConMengban->getContentSize().width/2,generalIConMengban->getContentSize().height/2));
		generalIConMengban->addChild(label_mengban_);

		/*
		progress_time = ProgressTimer::create(Sprite::create("res_ui/takegeneral/time2.png"));
		progress_time->setType(kProgressTimerTypeBar);
		progress_time->setMidpoint(Vec2(0,0));
		progress_time->setBarChangeRate(Vec2(0,1));
		progress_time->setAnchorPoint(Vec2(0,0));
		progress_time->setPosition(Vec2(100, 117));
		progress_time->setPercentage(100);
		m_pLayer->addChild(progress_time);
		*/

		clipperNode_ = ClippingNode::create();
 		clipperNode_->setAnchorPoint(Vec2(0.5f,0.5f));
 		clipperNode_->setPosition(Vec2(475,240));
		clipperNode_->setContentSize(Size(510,401));
		m_Layer->addChild(clipperNode_);

		auto stencil = DrawNode::create();
		Vec2 rectangle[4];
		rectangle[0] = Vec2(0, 0);
		rectangle[1] = Vec2(clipperNode_->getContentSize().width, 0);
		rectangle[2] = Vec2(clipperNode_->getContentSize().width, clipperNode_->getContentSize().height);
		rectangle[3] = Vec2(0, clipperNode_->getContentSize().height);

		Color4F white = {1, 1, 1, 1};
		stencil->drawPolygon(rectangle, 4, white, 1, white);
		clipperNode_->setStencil(stencil);

		updateResult();
		initHeadPortrait(NULL,0);
		this->scheduleUpdate();
		
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winSize);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(SearchGeneral::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(SearchGeneral::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(SearchGeneral::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(SearchGeneral::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void SearchGeneral::updateResult(bool isAction)
{
	char aucBuf[50];
	sprintf(aucBuf, "%d / %d %s", m_iSuccessNum, SUCCESS_NUM, StringDataManager::getString("SearchGeneral_SelectResult"));
	label_result->setString(aucBuf);

	if (isAction)
	{
		auto seq_ = Sequence::create(
			ScaleTo::create(0.2f,1.6f),
			//ScaleTo::create(0.5f,0.5f),
			ScaleTo::create(0.2f,1.0f),
			NULL);
		label_result->runAction(seq_);
	}
}

void SearchGeneral::initHeadPortrait(Node* sender, void* data)
{
	auto layer_GeneralIcon = Layer::create();
	layer_GeneralIcon->setIgnoreAnchorPointForPosition(false);
	layer_GeneralIcon->setAnchorPoint(Vec2(0,0));
	layer_GeneralIcon->setPosition(Vec2(0,0));
	layer_GeneralIcon->setTag(LAYERTAG_GENERALICON);
	clipperNode_->addChild(layer_GeneralIcon);

	char* path[GENERAL_PATH_NUM] = {"BlackMarket", "CaiWenJi", "CaoCao", "CaoPi", "Consignment", 
									"cz", "DaQiao", "DianWei", "DiaoChan", "DiaoXiaoChan", "GanNing", 
									"ChunYuQiong", "GuanYu", "GuoJia", "HuangYueYing", "HuangZhong", 
									"HuaTuo", "LiuBei", "LuSu", "LuXun", "LvBu", "MaChao", 
									"MiFuRen", "MrShuiJing", "NewbieGuide", "PangTong", "shop", 
									"SiMaYi", "SunCe", "SunQuan", "SunShangXiang", "TaiShiCi", "WangYun", 
									"WareHouse", "WenChou", "XiaHouDun", "XiaHouYuan", "XiaoQiao", "XuChu", 
									"XuShu", "XuYou", "YanLiang", "YuanShao", "YuanShu", 
									"ZhangChunHua", "ZhangFei", "ZhangHe", "ZhangJiao", "ZhangLiao", "ZhaoYun", 
									"ZhenJi", "ZhouYu", "ZhuGeLiang"};

	char* output[QUANTITY_PATH_NUM];


	struct timeval psv;
	gettimeofday(&psv, NULL);
	unsigned long long seed = psv.tv_sec*1000 + psv.tv_usec/1000;
	srand(seed);
     
	int size = GENERAL_PATH_NUM;
	for (int i = 0; i < QUANTITY_PATH_NUM; i++) {
		int num = GameUtils::getRandomNum(size);
		output[i] = path[num];
		path[num] = path[size-1];
		size--;
	}
	m_iAnswer = GameUtils::getRandomNum(QUANTITY_PATH_NUM);

	//
	if ((int)data == 1)
	{
		if (m_Layer->getChildByTag(LAYERTAG_SCROLL))
		{
			auto layer_ = (Layer *)m_Layer->getChildByTag(LAYERTAG_SCROLL);
			layer_->removeFromParentAndCleanup(true);
		}

		auto layer_scroll_ = Layer::create();
		layer_scroll_->setTag(LAYERTAG_SCROLL);
		m_Layer->addChild(layer_scroll_);

		auto scrollView_generalIcon = cocos2d::extension::ScrollView::create(Size(97,65));
		scrollView_generalIcon->setContentSize(Size(97,65));
		scrollView_generalIcon->setIgnoreAnchorPointForPosition(false);
		//scrollView_generalIcon->setTouchEnabled(true);
		scrollView_generalIcon->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		scrollView_generalIcon->setAnchorPoint(Vec2(0.5f,0.5f));
		scrollView_generalIcon->setPosition(Vec2(136,325));
		scrollView_generalIcon->setBounceable(true);
		scrollView_generalIcon->setClippingToBounds(true);
		layer_scroll_->addChild(scrollView_generalIcon);	
		
		std::string fullPath;
		fullPath.append("res_ui/npc/");
		fullPath.append(output[m_iAnswer]);
		fullPath.append(".anm");

		auto pAnswer = CCLegendAnimation::create(fullPath);
		pAnswer->setPlayLoop(true);
		pAnswer->setReleaseWhenStop(false);
		pAnswer->setAnchorPoint(Vec2(0.5f,0));
		pAnswer->setPosition(Vec2(0,73));
		pAnswer->setTag(100);
		pAnswer->setScale(0.7f);
		scrollView_generalIcon->addChild(pAnswer);

		pAnswer->runAction(MoveBy::create(m_gameEndTIme,Vec2(0,-73)));
		scrollView_generalIcon->runAction(MoveBy::create(m_gameEndTIme,Vec2(0,73)));

		std::string textureFile = pAnswer->getAnimationData()->getImagePath(0);
	}
	////
	for(int i=0; i<QUANTITY_PATH_NUM; i++)
	{
		std::string fullPath;
		fullPath.append("res_ui/npc/");
		fullPath.append(output[i]);
		fullPath.append(".anm");

		auto btn = Button::create();
		if ((int)data == 1)
		{
			btn->setTouchEnabled(true);
		}
		btn->loadTextures("res_ui/takegeneral/carddi.png", "res_ui/takegeneral/carddi.png", "");
		btn->setTag(i);
		if ((int)data == 1)
		{
			btn->setPosition(Vec2(55+(i%ROW_NUM)*100+600, 362-(i/ROW_NUM)*80));
		}else
		{
			btn->setPosition(Vec2(55+(i%ROW_NUM)*100, 362-(i/ROW_NUM)*80));
		}
		btn->addTouchEventListener(CC_CALLBACK_2(SearchGeneral::callBackSelected, this));
		btn->setPressedActionEnabled(true);
		layer_GeneralIcon->addChild(btn);
		
		//cardlight
		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/takegeneral/cardlight.png");
		mengban->setAnchorPoint(Vec2(0,1));
		mengban->setPosition(Vec2( 2-btn->getContentSize().width/2,btn->getContentSize().height/2));
		btn->addChild(mengban);

		auto pHeadPortrait = CCLegendAnimation::create(fullPath);
		pHeadPortrait->setPlayLoop(true);
		pHeadPortrait->setReleaseWhenStop(false);
		pHeadPortrait->setAnchorPoint(Vec2(0.5f,0.5f));
		pHeadPortrait->setPosition(Vec2(-45, -33));
		pHeadPortrait->setScale(0.7f);
		btn->addChild(pHeadPortrait);

		//ease out
		if ((int)data == 1)
		{
			auto moveTo_ = MoveTo::create(0.3f,Vec2(55+(i%ROW_NUM)*100, 362-(i/ROW_NUM)*80));
			
			auto sequence = Sequence::create(
				DelayTime::create(0.1f),
				CCEaseBackOut::create(moveTo_),
				NULL);

			btn->runAction(sequence);
		}
	}
}

void SearchGeneral::callBackSelected(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (m_iState == kStateSuccess)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("SearchGeneral_GameIsOver"));
			return;
		}

		if (m_iState == kStateFail)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("SearchGeneral_GameIsPlayerAgain"));
			return;
		}

		long long currentTime = GameUtils::millisecondNow();
		auto btn = (Button *)pSender;
		if (this->getFirstBtnTouchTime() != 0 && currentTime - this->getFirstBtnTouchTime() <300)
		{
			return;
		}
		this->setFirstBtnTouchTime(currentTime);

		btn->setTouchEnabled(false);

		if (btn->getTag() == m_iAnswer)
		{
			m_iSuccessNum++;
			if (m_iSuccessNum > SUCCESS_NUM)
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("SearchGeneral_GameWin"));
			}
			else
			{
				updateResult(true);

				if (m_iSuccessNum == SUCCESS_NUM)
				{
					auto sp_right = Sprite::create("res_ui/font/right.png");
					sp_right->setAnchorPoint(Vec2(0.5f, 0.5f));
					sp_right->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
					m_Layer->addChild(sp_right, 5);

					auto seq_ = Sequence::create(
						DelayTime::create(0.5f),
						RemoveSelf::create(),
						NULL);
					sp_right->runAction(seq_);

					m_iSuccessNum = 0;
					btn_start->setVisible(true);
					label_star->setString(StringDataManager::getString("SearchGeneral_Finish"));

					auto seq_1 = Sequence::create(
						DelayTime::create(1.0f),
						CallFuncN::create(CC_CALLBACK_0(SearchGeneral::setCurState, this)),
						NULL);
					this->runAction(seq_1);
					//
				}
				else
				{
					removeHeadPortrait();

					auto sp_right = Sprite::create("res_ui/font/right.png");
					sp_right->setAnchorPoint(Vec2(0.5f, 0.5f));
					sp_right->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
					m_Layer->addChild(sp_right, 5);

					auto seq_ = Sequence::create(
						DelayTime::create(0.5f),
						RemoveSelf::create(),
						NULL);
					sp_right->runAction(seq_);
				}
			}
		}
		else
		{
			//initHeadPortrait();
			removeHeadPortrait();

			auto sp_wrong = Sprite::create("res_ui/font/wrong.png");
			sp_wrong->setAnchorPoint(Vec2(0.5f, 0.5f));
			sp_wrong->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			m_Layer->addChild(sp_wrong, 5);

			auto seq_ = Sequence::create(
				DelayTime::create(0.5f),
				RemoveSelf::create(),
				NULL);

			sp_wrong->runAction(seq_);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void SearchGeneral::callBackStart(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button*)pSender;

		switch (m_iState)
		{
		case kStateStart:
		{
			m_iState = kStatePlay;

			btn->setVisible(false);
			generalIConMengban->setVisible(false);

			//initHeadPortrait();
			removeHeadPortrait();

			struct timeval m_sStartTime;
			gettimeofday(&m_sStartTime, NULL);
			m_dStartTime = m_sStartTime.tv_sec * 1000 + m_sStartTime.tv_usec / 1000;
		}break;
		case kStateSuccess:
		{
			m_iState = kStateWait;

			GameMessageProcessor::sharedMsgProcessor()->sendReq(7013, (void *)m_level, (void *)(com::future::threekingdoms::server::transport::protocol::FIND_GENERAL));
			this->closeAnim();
		}break;
		case kStateFail:
		{
			m_iState = kStatePlay;

			btn->setVisible(false);
			generalIConMengban->setVisible(false);

			updateResult();
			//initHeadPortrait();
			removeHeadPortrait();

			struct timeval m_sStartTime;
			gettimeofday(&m_sStartTime, NULL);
			m_dStartTime = m_sStartTime.tv_sec * 1000 + m_sStartTime.tv_usec / 1000;
		}break;
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

bool SearchGeneral::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return true;
}
void SearchGeneral::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void SearchGeneral::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void SearchGeneral::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void SearchGeneral::update(float dt)
{
	switch(m_iState)
	{
	case kStatePlay:
		{
			struct timeval m_sStartTime;
			gettimeofday(&m_sStartTime, NULL);
			m_dEndedTime = m_sStartTime.tv_sec*1000 + m_sStartTime.tv_usec/1000;
			long double time = m_dEndedTime - m_dStartTime;
			if(time >= m_showIconTime)
			{
				loadingTime->setPercent(0);
				//GameView::getInstance()->showAlertDialog(StringDataManager::getString("SearchGeneral_GameIsGoOnIsFail"));
				btn_start->setVisible(true);
				m_iState = kStateFail;

				m_iSuccessNum = 0;
				label_star->setString(StringDataManager::getString("SearchGeneral_SatrtAgain"));
			}
			else
			{
				long double passTime = m_showIconTime-time;
				int percent = (passTime*100)/m_showIconTime;

				//pass_timePrecfect -= (100/m_showIconTime)*dt;
				loadingTime->setPercent(percent);
			}
		}
		break;
	case kStateWait:
		{
			//I am a lazy state zzzzz
			//loadingTime->setPercent(100);
		}
		break;
	case kStateSuccess:
		{
			auto layer_ = (Layer *)clipperNode_->getChildByTag(LAYERTAG_GENERALICON);
			if (layer_)
			{
				for(int i=0; i<QUANTITY_PATH_NUM; i++)
				{
					auto btn_ = (Button *)layer_->getChildByTag(i);
					if (btn_)
					{
						btn_->setTouchEnabled(false);
					}
				}
			}

			generalIConMengban->setVisible(true);
			label_mengban_->setString(StringDataManager::getString("SearchGeneral_GameIsGoOn"));
		}break;
	case kStateFail:
		{
			auto layer_ = (Layer *)clipperNode_->getChildByTag(LAYERTAG_GENERALICON);
			if (layer_)
			{
				for(int i=0; i<QUANTITY_PATH_NUM; i++)
				{
					auto btn_ = (Button *)layer_->getChildByTag(i);
					if (btn_)
					{
						btn_->setTouchEnabled(false);
					}
				}
			}

			generalIConMengban->setVisible(true);
			label_mengban_->setString(StringDataManager::getString("SearchGeneral_GameIsPlayerAgain"));
		}break;
	default:
		break;
	}

}

void SearchGeneral::callBackClose(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		if (m_iState == kStateSuccess)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(7013, (void *)m_level, (void *)(com::future::threekingdoms::server::transport::protocol::FIND_GENERAL));
		}
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void SearchGeneral::removeHeadPortrait()
{
	auto layer_ = (Layer *)clipperNode_->getChildByTag(LAYERTAG_GENERALICON);
	if (layer_)
	{
		auto moveTo_ = MoveTo::create(0.5f,Vec2(-500,0));
		auto sequence = Sequence::create(
			DelayTime::create(0.2f),
			CCEaseBackOut::create(moveTo_),
			RemoveSelf::create(),
			NULL);
		layer_->runAction(sequence);
	}

	if (m_Layer->getChildByTag(LAYERTAG_SCROLL))
	{
		auto layer_ = (Layer *)m_Layer->getChildByTag(LAYERTAG_SCROLL);
		layer_->removeFromParentAndCleanup(true);
	}
	
	auto action1 =(ActionInterval *)Sequence::create(
		DelayTime::create(0.5f),
		CallFuncN::create(CC_CALLBACK_1(SearchGeneral::initHeadPortrait, this, (void *)1)),
		NULL);

	this->runAction(action1);
}

void SearchGeneral::setCurState()
{
	m_iState = kStateSuccess;

// 	MainScene * mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
// 	if (NULL != mainScene)
// 	{
// 		mainScene->addInterfaceAnm("gxgg/gxgg.anm");
// 	}

	auto mainAnmScene = GameView::getInstance()->getMainAnimationScene();
	if (mainAnmScene)
	{
		mainAnmScene->addInterfaceAnm("gxgg/gxgg.anm");
	}
}

void SearchGeneral::setFirstBtnTouchTime( long long time )
{
	m_touchTime = time;
}

long long SearchGeneral::getFirstBtnTouchTime()
{
	return m_touchTime;
}
