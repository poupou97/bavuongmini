#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../ui/extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class SearchGeneral:public UIScene
{
public:
	SearchGeneral(void);
	~SearchGeneral(void);

	static SearchGeneral *create(int diffType);
	bool init(int diffType);
	
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
	virtual void update(float dt);

	void callBackClose(Ref *pSender, Widget::TouchEventType type);
private:
	void callBackSelected(Ref *pSender, Widget::TouchEventType type);
	//void initHeadPortrait(bool isCreate = true);
	void initHeadPortrait(Node* sender, void* data);
	void callBackStart(Ref *pSender, Widget::TouchEventType type);
	void updateResult(bool isAction = false);

	void removeHeadPortrait();

	void setCurState();
private:
	int m_iAnswer;
	int m_iState;
	long double m_dStartTime;
	long double m_dEndedTime;
	int m_iSuccessNum;
	int pass_timePrecfect;
	//
	long long m_showIconTime;
	long long m_gameEndTIme;
	Size winSize;

	long long m_touchTime;
	void setFirstBtnTouchTime(long long time);
	long long getFirstBtnTouchTime();
private:
	Button *btn_start;

	Text * label_star;

	ImageView * image_icon;
	Text* label_result;

	LoadingBar* loadingTime;

	cocos2d::extension::Scale9Sprite * generalIConMengban;
	Label * label_mengban_;

	ClippingNode * clipperNode_;
	Layer * layer_Clip;

	Layer * m_Layer;
private:
	int m_level;
};

