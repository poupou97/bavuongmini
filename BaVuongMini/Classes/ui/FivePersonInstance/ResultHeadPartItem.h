
#ifndef _INSTANCE_RESULTHEADPARTITEM_H_
#define _INSTANCE_RESULTHEADPARTITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ResultHeadPartItem :public UIScene
{
public:
	enum
	{
		type_allExist = 0,
		type_expOnly ,
		type_dmgOnly ,
	};
public:
	ResultHeadPartItem();
	~ResultHeadPartItem();
	static ResultHeadPartItem * create(int expValue,int index,int type = type_allExist);
	bool init(int expValue,int index,int type);
};

#endif;