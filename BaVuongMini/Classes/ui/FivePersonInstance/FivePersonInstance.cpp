#include "FivePersonInstance.h"
#include "GameView.h"
#include "../../utils/StaticDataManager.h"
#include "WaitDialogPanelUI.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../utils/GameUtils.h"

FivePersonInstance * FivePersonInstance::s_fivePersonInstance = NULL;
int FivePersonInstance::s_status = FivePersonInstance::status_none;
int FivePersonInstance::s_currentInstanceId = -1;
int FivePersonInstance::s_signedUpId = -1;
float FivePersonInstance::m_nWaitTime = 0.f;
bool FivePersonInstance::isStopUpdate = true;

FivePersonInstance::FivePersonInstance():
m_nLastReqMonsterTime(0)
{
}

FivePersonInstance::~FivePersonInstance()
{
	std::vector<GoodsInfo*>::iterator iter;
	for (iter = s_m_rewardGoods.begin(); iter != s_m_rewardGoods.end(); ++iter)
	{
		delete *iter;
	}
	s_m_rewardGoods.clear();
}

FivePersonInstance * FivePersonInstance::getInstance()
{
	if (s_fivePersonInstance == NULL)
	{
		s_fivePersonInstance = new FivePersonInstance();
	}

	return s_fivePersonInstance;
}

void FivePersonInstance::init()
{
	setStatus(FivePersonInstance::status_none);
	setCurrentInstanceId(-1);
}

void FivePersonInstance::setStatus(int status)
{
	s_status = status;
	m_nWaitTime = 0.f;

	if (s_status == FivePersonInstance::status_in_sequence)
	{
		isStopUpdate = false;
	}
}
int FivePersonInstance::getStatus()
{
	return s_status;
}

void FivePersonInstance::setCurrentInstanceId(int id)
{
	s_currentInstanceId = id;
}
int FivePersonInstance::getCurrentInstanceId()
{
	return s_currentInstanceId;
}

void FivePersonInstance::setSignedUpId( int id )
{
	s_signedUpId = id;
}

int FivePersonInstance::getSignedUpId()
{
	return s_signedUpId;
}

void FivePersonInstance::update()
{
	if (s_status == FivePersonInstance::status_in_instance)
	{
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (!mainScene->getChildByTag(kTagInstanceEndUI))
			{
				autoKillAllMonster();
			}
		}
	}
	else if (s_status == FivePersonInstance::status_in_sequence)
	{
		if(!isStopUpdate)
		{
			m_nWaitTime += 1.0f/60;

			if (m_nWaitTime >= 120.f)
			{
				m_nWaitTime = 0.f;
				isStopUpdate = true;

				Size winSize=Director::getInstance()->getVisibleSize();
				auto waitDialogUI =WaitDialogPanelUI::create();
				waitDialogUI->setIgnoreAnchorPointForPosition(false);
				waitDialogUI->setAnchorPoint(Vec2(0.5f,0.5f));
				waitDialogUI->setPosition(Vec2(winSize.width/2,winSize.height/2));

				Director::getInstance()->getRunningScene()->addChild(waitDialogUI,100);
			}
		}
	}
}

void FivePersonInstance::autoKillAllMonster()
{
	if (MyPlayerAIConfig::getAutomaticSkill() != 1) 
	{
		return;
	}

	if(!GameView::getInstance()->myplayer->isAction(ACT_STAND))
	{
		return;
	}

	auto scene = GameView::getInstance()->getGameScene();
	
	if (scene)
	{
		bool isExistMonster = false;
		for (std::map<long long,GameActor*>::iterator it = scene->getActorsMap().begin(); it != scene->getActorsMap().end(); ++it)
		{
			auto pMonster = dynamic_cast<Monster*>(it->second);
			if(pMonster != NULL)
			{
				bool isCanAttack = GameView::getInstance()->myplayer->canAttackActor(pMonster->getRoleId());
				if (isCanAttack)
				{
					isExistMonster = true;
					break;
				}
			}
		}
		if(!isExistMonster)
		{
			auto missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
			if (missionAndTeam)
			{
				if (GameUtils::millisecondNow() - m_nLastReqMonsterTime > 500)
				{
					CCLOG("time time time = %ld",GameUtils::millisecondNow());
					m_nLastReqMonsterTime = GameUtils::millisecondNow();
					missionAndTeam->FiveInstanceInfoEvent(NULL);
				}	
			}
		}
	}
}
