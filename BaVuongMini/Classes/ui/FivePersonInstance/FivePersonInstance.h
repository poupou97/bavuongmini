#ifndef _INSTANCE_FIVEPERSONINSTANCE_H_
#define _INSTANCE_FIVEPERSONINSTANCE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GoodsInfo;

class FivePersonInstance
{
public:
	enum PlayerInstanceStatus {
		status_none = 0,
		status_in_sequence,   // the player is in the the waiting sequence
		status_in_instance,   // the player is in the instance
	};

public:
	FivePersonInstance();
	~FivePersonInstance();

	static FivePersonInstance * s_fivePersonInstance;
	static FivePersonInstance * getInstance();

	static void init();

	static void setStatus(int status);
	static int getStatus();

	static void setCurrentInstanceId(int id);
	static int getCurrentInstanceId();

	static void setSignedUpId(int id);
	static int getSignedUpId();

	void update();
	//�Ƿ���ͣUpdate
	static bool isStopUpdate;
	//auto kill monster in this map but not in screen
	void autoKillAllMonster();
private:
	static int s_status;
	static int s_currentInstanceId;
	static int s_signedUpId;

	//������ȴ�ʱ��
	static float m_nWaitTime;
	//�ϴ��������λ�õ�ʱ��
	long long m_nLastReqMonsterTime;
public:
	std::map<int ,int> s_m_copyStar;
	//cur instance rewardsInfo
	std::vector<GoodsInfo*> s_m_rewardGoods;
};

#endif