#include "InstanceDetailUI.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "../../utils/StaticDataManager.h"
#include "../extensions/UITab.h"
#include "../../GameView.h"
#include "InstanceRewardUI.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "FivePersonInstance.h"
#include "../../utils/GameUtils.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../ui/backpackscene/PackageItem.h"

#define UISCENE_MAIN_SCENE_TAG 1000
#define INSTANCEDETAILUI_TABALEVIEW_HIGHLIGHT_TAG 456
#define M_SCROLLVIEWTAG_DES 567
#define M_SCROLLVIEWTAG_EXPLAIN 568

#define kTag_StarItem_Base 900
#define kTag_RewardItem_Base 800

InstanceDetailUI::InstanceDetailUI():
curInstanceId(-1)
{
}

InstanceDetailUI::~InstanceDetailUI()
{
}

InstanceDetailUI * InstanceDetailUI::create(int instanceId)
{
	auto _ui=new InstanceDetailUI();
	if (_ui && _ui->init(instanceId))
	{
		_ui->autorelease();
		return _ui;
	}
	CC_SAFE_DELETE(_ui);
	return NULL;
}

bool InstanceDetailUI::init(int instanceId)
{
	if (UIScene::init())
	{
		curInstanceId = instanceId;
		Size size=Director::getInstance()->getVisibleSize();

		layer=Layer::create();
		addChild(layer);

		if(LoadSceneLayer::instanceDetailLayout->getParent() != NULL)
		{
			LoadSceneLayer::instanceDetailLayout->removeFromParentAndCleanup(false);
		}

		auto mainPanel=LoadSceneLayer::instanceDetailLayout;
		mainPanel->setAnchorPoint(Vec2(0,0));
		mainPanel->setPosition(Vec2(0,0));
		mainPanel->setTag(UISCENE_MAIN_SCENE_TAG);
		m_pLayer->addChild(mainPanel);

		btnSignUp= (Button *)Helper::seekWidgetByName(mainPanel,"Button_SignUp");
		CCAssert(btnSignUp != NULL, "should not be nil");
		btnSignUp->setTouchEnabled(true);
		btnSignUp->setPressedActionEnabled(true);
		btnSignUp->setVisible(true); 
		btnSignUp->addTouchEventListener(CC_CALLBACK_2(InstanceDetailUI::signUpCallback, this));

		btnCancelSign= (Button *)Helper::seekWidgetByName(mainPanel,"Button_CancelSign");
		CCAssert(btnCancelSign != NULL, "should not be nil");
		btnCancelSign->setTouchEnabled(true);
		btnCancelSign->setPressedActionEnabled(true);
		btnCancelSign->setVisible(false);   // default value, invisible
		btnCancelSign->addTouchEventListener(CC_CALLBACK_2(InstanceDetailUI::cancelSignCallback, this));

		if (FivePersonInstance::getStatus() == FivePersonInstance::status_in_sequence)
		{
			btnSignUp->setVisible(false);
			btnCancelSign->setVisible(true);
		}
		else
		{
			btnSignUp->setVisible(true);
			btnCancelSign->setVisible(false);
		}

// 		Button * btnCreateTeam= (Button *)Helper::seekWidgetByName(mainPanel,"Button_CreatTeam");
// 		CCAssert(btnCreateTeam != NULL, "should not be nil");
// 		btnCreateTeam->setTouchEnabled(true);
// 		btnCreateTeam->setPressedActionEnabled(true);
// 		btnCreateTeam->addTouchEventListener(CC_CALLBACK_2(InstanceDetailUI::createTeamCallback));
	
		btnEnter= (Button *)Helper::seekWidgetByName(mainPanel,"Button_enter");
		CCAssert(btnEnter != NULL, "should not be nil");
		btnEnter->setTouchEnabled(true);
		btnEnter->setPressedActionEnabled(true);
		btnEnter->addTouchEventListener(CC_CALLBACK_2(InstanceDetailUI::enterInstanceCallback, this));

		auto btn_Exit= (Button *)Helper::seekWidgetByName(mainPanel,"Button_close");
		btn_Exit->setTouchEnabled(true);
		btn_Exit->setPressedActionEnabled(true);
		btn_Exit->addTouchEventListener(CC_CALLBACK_2(InstanceDetailUI::callBackExit, this));

		auto btnReward= (Button *)Helper::seekWidgetByName(mainPanel,"Button_reward");
		CCAssert(btnReward != NULL, "should not be nil");
		btnReward->setTouchEnabled(true);
		btnReward->setPressedActionEnabled(true);
		btnReward->addTouchEventListener(CC_CALLBACK_2(InstanceDetailUI::btnRewardCallback,this));

		l_instanceName = (Text*)Helper::seekWidgetByName(mainPanel,"Label_InstanceName");
		CCAssert(l_instanceName != NULL, "should not be nil");
		l_open_lv = (Text*)Helper::seekWidgetByName(mainPanel,"Label_OpenLv_value");
		CCAssert(l_open_lv != NULL, "should not be nil");
		l_consume = (Text*)Helper::seekWidgetByName(mainPanel,"Label_consume_value");
		CCAssert(l_consume != NULL, "should not be nil");
		l_open_time = (Text*)Helper::seekWidgetByName(mainPanel,"Label_OpenTime_value");
		CCAssert(l_open_time != NULL, "should not be nil");
		l_difficult = (Text*)Helper::seekWidgetByName(mainPanel,"Label_difficult_value");
		CCAssert(l_difficult != NULL, "should not be nil");
		l_suggest = (Text*)Helper::seekWidgetByName(mainPanel,"Label_suggestValue");
		CCAssert(l_suggest != NULL, "should not be nil");
		l_combat = (Text*)Helper::seekWidgetByName(mainPanel,"Label_combat_value");
		CCAssert(l_combat != NULL, "should not be nil");
		
		//�������
		int scroll_height = 0;
		std::string instanceDes = "";
		const char *str_fubenjieshao = StringDataManager::getString("fivePerson_fubenjieshao");
		instanceDes.append(str_fubenjieshao);
		l_des_head = Label::createWithTTF(instanceDes.c_str(),APP_FONT_NAME,18,Size(290,0),TextHAlignment::LEFT,TextVAlignment::TOP);
		l_des_head->setColor(Color3B(47,93,13));
		int zeroLineHeight = l_des_head->getContentSize().height;
		scroll_height = zeroLineHeight;
		
		l_des = Label::createWithTTF("",APP_FONT_NAME,18,Size(290,0),TextHAlignment::LEFT,TextVAlignment::TOP);
		l_des->setColor(Color3B(47,93,13));
		int firstLineHeight = zeroLineHeight+l_des->getContentSize().height;
		scroll_height = firstLineHeight+10;

		//ܸ���˵�
		std::string instanceExplain = "";
		const char *str_fubenshuoming = StringDataManager::getString("fivePerson_fubenshuoiming");
		instanceExplain.append(str_fubenshuoming);
		l_info_head = Label::createWithTTF(instanceExplain.c_str(),APP_FONT_NAME,18,Size(290,0),TextHAlignment::LEFT,TextVAlignment::TOP);
		l_info_head->setColor(Color3B(47,93,13));
		int secondLineHeight =  firstLineHeight+l_info_head->getContentSize().height;
		scroll_height = secondLineHeight;

		//��˵�����
		l_info = Label::createWithTTF("",APP_FONT_NAME,18,Size(290,0),TextHAlignment::LEFT,TextVAlignment::TOP);
		l_info->setColor(Color3B(47,93,13));
		int thirdLineHeight = secondLineHeight + l_info->getContentSize().height;
		scroll_height = thirdLineHeight+10;

		m_scrollView_des = cocos2d::extension::ScrollView::create(Size(325,294));
		m_scrollView_des->setViewSize(Size(325, 294));
		m_scrollView_des->setIgnoreAnchorPointForPosition(false);
		m_scrollView_des->setTouchEnabled(true);
		m_scrollView_des->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		m_scrollView_des->setAnchorPoint(Vec2(0,0));
		m_scrollView_des->setPosition(Vec2(388,80));
		m_scrollView_des->setBounceable(true);
		m_scrollView_des->setClippingToBounds(true);
		m_scrollView_des->setTag(M_SCROLLVIEWTAG_DES);
		layer->addChild(m_scrollView_des);
		if (scroll_height > 294)
		{
			m_scrollView_des->setContentSize(Size(325,scroll_height));
			m_scrollView_des->setContentOffset(Vec2(0,294-scroll_height));  
		}
		else
		{
			m_scrollView_des->setContentSize(Size(325,294));
		}
		m_scrollView_des->setClippingToBounds(true);

		m_scrollView_des->addChild(l_des_head);
		l_des_head->setPosition(Vec2(36,m_scrollView_des->getContentSize().height - zeroLineHeight-7));

		m_scrollView_des->addChild(l_des);
		l_des->setPosition(Vec2(36,m_scrollView_des->getContentSize().height - firstLineHeight-7));

		m_scrollView_des->addChild(l_info_head);
		l_info_head->setPosition(Vec2(36,m_scrollView_des->getContentSize().height - secondLineHeight-7));

		m_scrollView_des->addChild(l_info);
		l_info->setPosition(Vec2(36,m_scrollView_des->getContentSize().height - thirdLineHeight-7));

		RefreshInstanceInfo(instanceId);

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(mainPanel->getContentSize());
		return true;
	}
	return false;
}

void InstanceDetailUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void InstanceDetailUI::onExit()
{
	UIScene::onExit();
}

bool InstanceDetailUI::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	// convert the touch point to OpenGL coordinates
// 	Vec2 location = pTouch->getLocation();
// 
// 	Vec2 pos = this->getPosition();
// 	Size size = this->getContentSize();
// 	Vec2 anchorPoint = this->getAnchorPointInPoints();
// 	pos = ccpSub(pos, anchorPoint);
// 	Rect rect(pos.x, pos.y, size.width, size.height);
// 	if(rect.containsPoint(location))
// 	{
// 		return true;
// 	}
// 
// 	return false;
	return true;
}
void InstanceDetailUI::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void InstanceDetailUI::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void InstanceDetailUI::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void InstanceDetailUI::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
// 	Size winSize = Director::getInstance()->getVisibleSize();
// 
// 	MoveTo * moveTo = MoveTo::create(.3f,Vec2(winSize.width+this->getContentSize().width,winSize.height/2));
// 	CCEaseBackIn * easeIn = CCEaseBackIn::create(moveTo);
// 	Sequence * sequence = Sequence::create(easeIn,RemoveSelf::create(),NULL);
// 	this->runAction(sequence);
}

void InstanceDetailUI::signUpCallback(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		// request to instance waiting sequence
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1920, (void *)curInstanceId);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}
void InstanceDetailUI::cancelSignCallback(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (FivePersonInstance::getSignedUpId() != -1)
		{
			// request to cancel the waiting sequence
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1922, (void*)FivePersonInstance::getSignedUpId());
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void InstanceDetailUI::enterInstanceCallback(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		// request to enter instance directly
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1921, (void *)curInstanceId);

		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void InstanceDetailUI::createTeamCallback(Ref* pSender)
{
}


void InstanceDetailUI::btnRewardCallback(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceRewardUI) == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			auto instanceRewardUI = InstanceRewardUI::create();
			GameView::getInstance()->getMainUIScene()->addChild(instanceRewardUI, 0, kTagInstanceRewardUI);
			instanceRewardUI->setIgnoreAnchorPointForPosition(false);
			instanceRewardUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			instanceRewardUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void InstanceDetailUI::RefreshInstanceInfo( int instanceId )
{
	CFivePersonInstance * fivePersonInstance;
	bool isExist = false;
	for (int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();i++)
	{
		fivePersonInstance = FivePersonInstanceConfigData::s_fivePersonInstance.at(i);
		if (instanceId == fivePersonInstance->get_id())
		{
			curInstanceId = instanceId;
			isExist = true;
			break;
		}
	}

	if (!isExist)
		return;

	//�����ǰѡ�и����Ľ�����Ϣ
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1926,(void *)instanceId); 
	
	//star info
	for (int i = 0;i<5;i++)
	{
		if (layer->getChildByTag(kTag_StarItem_Base+i))
		{
			layer->getChildByTag(kTag_StarItem_Base+i)->removeFromParent();
		}
	}
	int star = 0;
	std::map<int, int>::iterator it = FivePersonInstance::getInstance()->s_m_copyStar.begin();
	for ( ; it != FivePersonInstance::getInstance()->s_m_copyStar.end(); ++ it )
	{
		if (it->first == instanceId)
		{
			star = it->second;
			break;
		}
	}
	for (int i = 0;i<5;i++)
	{
		auto pIcon = ImageView::create();
		if (i<star)
		{
			pIcon->loadTexture("res_ui/star_on.png");
		}
		else
		{
			pIcon->loadTexture("res_ui/star_off.png");
		}
		pIcon->setScale(1.0f);
		pIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
		pIcon->setPosition(Vec2(240 +25*i, 400));
		pIcon->setTag(kTag_StarItem_Base+i);
		layer->addChild(pIcon);
	}

	//���
	l_instanceName->setString(fivePersonInstance->get_name().c_str());
	//ֿ��ŵȼ�
	std::string open_lv = "";
	char s_open_level[10];
	sprintf(s_open_level,"%d",fivePersonInstance->get_open_level());
	open_lv.append(s_open_level);
	const char *str_ji = StringDataManager::getString("fivePerson_ji");
	open_lv.append(str_ji);
	l_open_lv->setString(open_lv.c_str());
	//��ģ����5
	std::string consume = "";
	const char *str_tili = StringDataManager::getString("fivePerson_tili");
	consume.append(str_tili);
	char s_consume[10];
	sprintf(s_consume,"%d",fivePersonInstance->get_consume());
	consume.append(s_consume);
	l_consume->setString(consume.c_str());
	//����ʱ�䣺0:0:0-23:59:59
	std::string openTime = "";
	openTime.append(fivePersonInstance->get_start_time().substr(0,5));
	openTime.append("-");
	openTime.append(fivePersonInstance->get_end_time().substr(0,5));
	l_open_time->setString(openTime.c_str());
	//�����Ѷȣ���ͨ/��
	std::string str_difficult = "";
	if (fivePersonInstance->get_difficult() == 1)
	{
		str_difficult.append(StringDataManager::getString("fivePerson_difficult_normal"));
	}
	else
	{
		str_difficult.append(StringDataManager::getString("fivePerson_difficult_hard"));
	}

	l_difficult->setString(str_difficult.c_str());
	//ѽ��������2��
	std::string str_suggest = "";
	//const char *str_jianyi = StringDataManager::getString("fivePerson_jianyi");
	//str_suggest.append(str_jianyi);
	char s_suggest_num[10];
	sprintf(s_suggest_num,"%d",fivePersonInstance->get_suggest_num());
	str_suggest.append(s_suggest_num);
	const char *str_ren = StringDataManager::getString("fivePerson_ren");
	str_suggest.append(str_ren);
	l_suggest->setString(str_suggest.c_str());

	char s_combat_value[10];
	sprintf(s_combat_value,"%d",fivePersonInstance->get_fight_point());
	l_combat->setString(s_combat_value);


	int scroll_height = 0;
	//˸������
	int zeroLineHeight = l_des_head->getContentSize().height;

	std::string str_des;
	str_des.append(fivePersonInstance->get_description().c_str());
	l_des->setString(str_des.c_str());
	int firstLineHeight = zeroLineHeight+l_des->getContentSize().height;

	// 	//ܸ���˵�
	int secondLineHeight = firstLineHeight+l_info_head->getContentSize().height;

	//��˵�����
	std::string str_info ;
	std::map<int,std::string>::const_iterator cIter;
	cIter = SystemInfoConfigData::s_systemInfoList.find(4);
	if (cIter == SystemInfoConfigData::s_systemInfoList.end()) // �û�ҵ�����ָ�END��  
	{
		return ;
	}
	else
	{
		str_info.append(SystemInfoConfigData::s_systemInfoList[4].c_str());
	}
	l_info->setString(str_info.c_str());
	int thirdLineHeight = secondLineHeight + l_info->getContentSize().height;
	scroll_height = thirdLineHeight+30;

	if (scroll_height > 294)
	{
		m_scrollView_des->setContentSize(Size(325,scroll_height));
		m_scrollView_des->setContentOffset(Vec2(0,294-scroll_height));  
	}
	else
	{
		m_scrollView_des->setContentSize(Size(325,294));
	}
	m_scrollView_des->setClippingToBounds(true);
	l_des_head->setPosition(Vec2(36,m_scrollView_des->getContentSize().height - zeroLineHeight-10));
	l_des->setPosition(Vec2(36,m_scrollView_des->getContentSize().height - firstLineHeight-10));
	l_info_head->setPosition(Vec2(36,m_scrollView_des->getContentSize().height - secondLineHeight-20));
	l_info->setPosition(Vec2(36,m_scrollView_des->getContentSize().height - thirdLineHeight-20));

}

void InstanceDetailUI::RefreshRewardsInfo()
{
	for (int i = 0;i<8;i++)
	{
		if (layer->getChildByTag(kTag_RewardItem_Base+i))
		{
			layer->getChildByTag(kTag_RewardItem_Base+i)->removeFromParent();
		}
	}

	for (int i = 0;i<FivePersonInstance::getInstance()->s_m_rewardGoods.size();i++)
	{
		if (i >= 8)
			break;

		auto packageItem = PackageItem::create(FivePersonInstance::getInstance()->s_m_rewardGoods.at(i));
		packageItem->setAnchorPoint(Vec2(0,0));
		packageItem->setTag(kTag_RewardItem_Base+i);
		//packageItem->setPosition(Vec2(CoordinateVector.at(idx%m_nElementNumEveryPage).x, CoordinateVector.at(idx%m_nElementNumEveryPage).y));
		layer->addChild(packageItem);
		packageItem->setBoundVisible(true);

		if (i < 4)
		{
			packageItem->setPosition(Vec2(57+86*i,162));
		}
		else
		{
			packageItem->setPosition(Vec2(57+86*(i%4),85));
		}
	}
}

void InstanceDetailUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void InstanceDetailUI::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,60,55);
// 	tutorialIndicator->setPosition(Vec2(pos.x-45,pos.y+95));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	layer->addChild(tutorialIndicator);
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,110,49,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+21+_w,pos.y+17+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}


void InstanceDetailUI::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(layer->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

bool InstanceDetailUI::isFinishAction()
{
	Size winSize = Director::getInstance()->getVisibleSize();
	if (this->getPosition().x == winSize.width/2)
		return true;

	return false;
}

