
#ifndef _FIVEPERSONINSTANCE_INSTANCEREWARD_H_
#define _FIVEPERSONINSTANCE_INSTANCEREWARD_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../backpackscene/PacPageView.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GoodsInfo;

class InstanceRewardUI : public UIScene
{
public:
	InstanceRewardUI();
	~InstanceRewardUI();

	static InstanceRewardUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void ReloadData();
	void ReloadDataByIndex(int index);

	void CurrentPageViewChanged(Ref *pSender, PageView::EventType type);

public:
	//std::vector<GoodsInfo*> rewardGoods;

private:
	std::vector<Coordinate> CoordinateVector;

	std::string rewardGoodItem_name[16];

	Layer * u_layer;
	ui::PageView *m_pageView;

	int m_nElementNumEveryPage;
	int m_nPageNum;
	//��ļ�
	int m_nSpace;
	//����ҳ�
	int m_nMaxPageNum;

	float first_point_x;
};

#endif
