#include "RewardCardItem.h"
#include "../../messageclient/element/CRewardProp.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "GameView.h"
#include "AppMacros.h"
#include "../GameUIConstant.h"
#include "../../utils/StaticDataManager.h"
#include "InstanceEndUI.h"
#include "../../gamescene_state/MainScene.h"

#define voidFramePath "res_ui/smdi_none.png"

#define whiteFramePath "res_ui/smdi_white.png"
#define greenFramePath "res_ui/smdi_green.png"
#define blueFramePath "res_ui/smdi_bule.png"
#define purpleFramePath "res_ui/smdi_purple.png"
#define orangeFramePath "res_ui/smdi_orange.png"


RewardCardItem::RewardCardItem():
isFloped(false),
grid(-1),
isFrontOrBack(true)
{
}


RewardCardItem::~RewardCardItem()
{
}

RewardCardItem * RewardCardItem::create( CRewardProp *rewardProp , int timeDouble)
{
	auto rewardCardItem = new RewardCardItem();
	if (rewardCardItem && rewardCardItem->init(rewardProp,timeDouble))
	{
		rewardCardItem->autorelease();
		return rewardCardItem;
	}
	CC_SAFE_DELETE(rewardCardItem);
	return NULL;
}

bool RewardCardItem::init( CRewardProp *rewardProp , int timeDouble)
{
	if (UIScene::init())
	{
		isFrontOrBack = true;
		grid = rewardProp->grid();

		//���Ʊ��
		btn_backCardFrame = Button::create();
		btn_backCardFrame->loadTextures("res_ui/instance_end/card.png","res_ui/instance_end/card.png","");
		btn_backCardFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_backCardFrame->setPosition(Vec2(0,0));
		btn_backCardFrame->setTouchEnabled(false);
		btn_backCardFrame->addTouchEventListener(CC_CALLBACK_2(RewardCardItem::BackCradEvent, this));
		m_pLayer->addChild(btn_backCardFrame);
		btn_backCardFrame->setVisible(true);

		auto image_desFrame = ImageView::create();
		image_desFrame->loadTexture("res_ui/zhezhao80.png");
		image_desFrame->setScale9Enabled(true);
		image_desFrame->setContentSize(Size(72,40));
		image_desFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		image_desFrame->setPosition(Vec2(0,0));
		btn_backCardFrame->addChild(image_desFrame);
		image_desFrame->setName("image_desFrame");
		image_desFrame->setVisible(false);

		l_des = Text::create(StringDataManager::getString("fivePerson_turnCard"), APP_FONT_NAME, 16);
		l_des->setTextAreaSize(Size(70,0));
		l_des->setTextVerticalAlignment(TextVAlignment::CENTER);
		l_des->setTextHorizontalAlignment(TextHAlignment::CENTER);
		//l_des->setColor(Color3B(0,255,0));
		l_des->setAnchorPoint(Vec2(0.5f,0.5f));
		l_des->setRotationSkewY(180);
		btn_backCardFrame->addChild(l_des);
		l_des->setPosition(Vec2(0,10));
		l_des->setVisible(false);

		l_costValue = Text::create(StringDataManager::getString("game_free"), APP_FONT_NAME, 16);
		l_costValue->setTextAreaSize(Size(70,0));
		l_costValue->setTextVerticalAlignment(TextVAlignment::CENTER);
		l_costValue->setTextHorizontalAlignment(TextHAlignment::CENTER);
		//l_des->setColor(Color3B(0,255,0));
		l_costValue->setAnchorPoint(Vec2(0.5f,0.5f));
		l_costValue->setRotationSkewY(180);
		btn_backCardFrame->addChild(l_costValue);
		l_costValue->setPosition(Vec2(0,-10));
		l_costValue->setVisible(false);

		image_unit = ImageView::create();
		image_unit->loadTexture("");
		image_unit->setAnchorPoint(Vec2(0.5f,0.5f));
		image_unit->setRotationSkewY(180);
		btn_backCardFrame->addChild(image_unit);
		image_unit->setPosition(Vec2(0,-10));
		image_unit->setVisible(false);

		//濨�����
		btn_frontCardFrame = Button::create();
		btn_frontCardFrame->loadTextures("res_ui/instance_end/card_get.png","res_ui/instance_end/card_get.png","");
		btn_frontCardFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_frontCardFrame->setPosition(Vec2(0,0));
		btn_backCardFrame->setTouchEnabled(false);
		btn_frontCardFrame->addTouchEventListener(CC_CALLBACK_2(RewardCardItem::FrontCradEvent, this));
		btn_frontCardFrame->setRotationSkewY(0);
		m_pLayer->addChild(btn_frontCardFrame);
		btn_frontCardFrame->setVisible(true);


		/*********************��ж���ɫ***************************/
		std::string frameColorPath;
		if (rewardProp->goods().quality() == 1)
		{
			frameColorPath = whiteFramePath;
		}
		else if (rewardProp->goods().quality() == 2)
		{
			frameColorPath = greenFramePath;
		}
		else if (rewardProp->goods().quality() == 3)
		{
			frameColorPath = blueFramePath;
		}
		else if (rewardProp->goods().quality() == 4)
		{
			frameColorPath = purpleFramePath;
		}
		else if (rewardProp->goods().quality() == 5)
		{
			frameColorPath = orangeFramePath;
		}
		else
		{
			frameColorPath = voidFramePath;
		}

		//�
		imageView_frame = ImageView::create();
		imageView_frame->loadTexture(frameColorPath.c_str());
		imageView_frame->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_frame->setPosition(Vec2(0,1));
		btn_frontCardFrame->addChild(imageView_frame);
		//���ͼƬ
		imageView_icon = ImageView::create();
		std::string iconPath = "res_ui/props_icon/";
		iconPath.append(rewardProp->goods().icon());
		iconPath.append(".png");
		imageView_icon->loadTexture(iconPath.c_str());
		imageView_icon->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_icon->setPosition(Vec2(0,1));
		imageView_icon->setScale(0.8f);
		btn_frontCardFrame->addChild(imageView_icon);
		//�Ƿ�˫��
		if (timeDouble == 1)
		{
			auto imageView_timeDouble = ImageView::create();
			imageView_timeDouble->loadTexture("res_ui/double.png");
			imageView_timeDouble->setAnchorPoint(Vec2(0.0f,1.0f));
			imageView_timeDouble->setPosition(Vec2(-imageView_frame->getContentSize().width/2+1,imageView_frame->getContentSize().height/2-1));
			imageView_timeDouble->setScale(.5f);
			btn_frontCardFrame->addChild(imageView_timeDouble);
		}
		//������
		l_rewardName = Label::createWithTTF(rewardProp->goods().name().c_str(), APP_FONT_NAME, 14);
		l_rewardName->setAnchorPoint(Vec2(0.5f,0));
		l_rewardName->setPosition(Vec2(0,-btn_frontCardFrame->getContentSize().width/2-7));
		l_rewardName->setColor(GameView::getInstance()->getGoodsColorByQuality(rewardProp->goods().quality()));
		btn_frontCardFrame->addChild(l_rewardName);

		//�������
		l_roleName = Label::createWithTTF("", APP_FONT_NAME, 14);
		l_roleName->setAnchorPoint(Vec2(0.5f,0));
		l_roleName->setPosition(Vec2(0,btn_frontCardFrame->getContentSize().width/2-5));
		btn_frontCardFrame->addChild(l_roleName);

		this->setContentSize(btn_backCardFrame->getContentSize());
		return true;
	}
	return false;
}

void RewardCardItem::BackCradEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (grid == 0)
		{
			auto instanceEndUI = (InstanceEndUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceEndUI);
			if (instanceEndUI)
			{
				instanceEndUI->removeToturial();
			}
		}

		if (isFloped)
			return;

		// 	//Ʒ��ƣ������棩
		// 	this->FlopAnimationToFront();
		//����� 1925
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1925, (void *)grid);
		isFloped = true;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void RewardCardItem::FrontCradEvent(Ref *pSender, Widget::TouchEventType type)
{

}

//ưѿ��Ʒ������沢���ò��ɴ��
void RewardCardItem::FlopAnimationToFront()
{
	btn_backCardFrame->setLocalZOrder(10);
	btn_frontCardFrame->setRotationSkewY(-180);
	auto  orbit1 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_90_TIME,1, 0, 0, -90, 0, 0);
	auto  action1 = Sequence::create(
		Show::create(),
		orbit1,
		CCHide::create(),
		NULL);

	auto  orbit2 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_180_TIME,1, 0, 0, 180, 0, 0);
	auto  action2 = Sequence::create(
		Show::create(),
		orbit2,
		NULL);

	auto action_ToFront = Sequence::create(action1,NULL);
	btn_backCardFrame->runAction(action_ToFront);
	auto action_ToBack = Sequence::create(action2,NULL);
	btn_frontCardFrame->runAction(action_ToBack);

	isFrontOrBack = true;
	btn_frontCardFrame->setTouchEnabled(false);
	btn_backCardFrame->setTouchEnabled(false);
}

//ذѿ��Ʒ������沢���ò��ɴ��
void RewardCardItem::FlopAnimationToBack()
{
	auto  orbit1 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_180_TIME,1, 0, 0, 180, 0, 0);
	auto  action1 = Sequence::create(
		orbit1,
		NULL);

	auto  orbit2 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_90_TIME,1, 0, 0, 90, 0, 0);
	auto  action2 = Sequence::create(
		orbit2,
		CCHide::create(),
		NULL);

	auto action_ToBack = Sequence::create(action1,NULL);
	btn_backCardFrame->runAction(action_ToBack);
	auto action_ToFront = Sequence::create(action2,NULL);
	btn_frontCardFrame->runAction(action_ToFront);

	isFrontOrBack = false;
	btn_frontCardFrame->setTouchEnabled(false);
	btn_backCardFrame->setTouchEnabled(false);
}

void RewardCardItem::setBtnTouchEnabled()
{
	btn_backCardFrame->setTouchEnabled(true);
	btn_frontCardFrame->setTouchEnabled(false);
}

void RewardCardItem::RefreshCardInfo( CRewardProp *rewardProp ,std::string roleName,long long roleId)
{
	/*********************��ж���ɫ***************************/
	std::string frameColorPath;
	if (rewardProp->goods().quality() == 1)
	{
		frameColorPath = whiteFramePath;
	}
	else if (rewardProp->goods().quality() == 2)
	{
		frameColorPath = greenFramePath;
	}
	else if (rewardProp->goods().quality() == 3)
	{
		frameColorPath = blueFramePath;
	}
	else if (rewardProp->goods().quality() == 4)
	{
		frameColorPath = purpleFramePath;
	}
	else if (rewardProp->goods().quality() == 5)
	{
		frameColorPath = orangeFramePath;
	}
	else
	{
		frameColorPath = voidFramePath;
	}

	//�
	imageView_frame->loadTexture(frameColorPath.c_str());
	//���ͼƬ
	std::string iconPath = "res_ui/props_icon/";
	iconPath.append(rewardProp->goods().icon());
	iconPath.append(".png");
	imageView_icon->loadTexture(iconPath.c_str());
	//������
	l_rewardName->setString(rewardProp->goods().name().c_str());
	l_rewardName->setColor(GameView::getInstance()->getGoodsColorByQuality(rewardProp->goods().quality()));

	//Ʒ��ƽ�ɫ��
	l_roleName->setString(roleName.c_str());
	if (GameView::getInstance()->isOwn(roleId))
	{
		l_roleName->setColor(Color3B(0,255,255));
	}
	else
	{
		l_roleName->setColor(Color3B(0,255,0));
	}
}

void RewardCardItem::presentDesLable()
{
	if (btn_backCardFrame->getChildByName("image_desFrame"))
	{
		btn_backCardFrame->getChildByName("image_desFrame")->setVisible(true);
	}
	l_des->setVisible(true);
	l_costValue->setVisible(true);
	image_unit->setVisible(true);
}

void RewardCardItem::RefreshCostValue(std::string costValue,std::string iconPath)
{
	l_costValue->setString(costValue.c_str());
	image_unit->loadTexture(iconPath.c_str());
	int allWidth = l_costValue->getContentSize().width+image_unit->getContentSize().width;
	l_costValue->setAnchorPoint(Vec2(.0f,0.5f));
	l_costValue->setPosition(Vec2(allWidth/2,l_costValue->getPosition().y));
	image_unit->setAnchorPoint(Vec2(1.0f,0.5f));
	image_unit->setPosition(Vec2(l_costValue->getPosition().x-l_costValue->getContentSize().width-5,l_costValue->getPosition().y));
}
