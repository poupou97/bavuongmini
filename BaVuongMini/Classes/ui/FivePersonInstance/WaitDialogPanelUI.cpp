#include "WaitDialogPanelUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "FivePersonInstance.h"
#include "GameView.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "cocostudio\CCSGUIReader.h"

WaitDialogPanelUI::WaitDialogPanelUI(void)
{
}


WaitDialogPanelUI::~WaitDialogPanelUI(void)
{
}

WaitDialogPanelUI* WaitDialogPanelUI::create()
{
	auto _panel = new WaitDialogPanelUI();
	if (_panel && _panel->init())
	{
		_panel->autorelease();
		return _panel;
	}
	CC_SAFE_DELETE(_panel);
	return NULL;
}

bool WaitDialogPanelUI::init()
{
	if (UIScene::init())
	{
		Size visibleSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();

		auto ppanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/tishikuang_instance_1.json");
		ppanel->setAnchorPoint(Vec2(0,0));
		ppanel->setPosition(Vec2(0,0));
		m_pLayer->addChild(ppanel);

		 auto textArea =( ui::TextField*)Helper::seekWidgetByName(ppanel,"TextArea_content");

		std::string str_des = StringDataManager::getString("fivePerson_waitSignUp_des_1");
		for (int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
		{
			auto fivePersonInstance = FivePersonInstanceConfigData::s_fivePersonInstance.at(i);
			if (FivePersonInstance::getSignedUpId() == fivePersonInstance->get_id())
			{
				str_des.append(fivePersonInstance->get_name().c_str());
				break;
			}
		}
		str_des.append(StringDataManager::getString("fivePerson_waitSignUp_des_2"));
		textArea->setString(str_des.c_str());

		auto btn_Wait=(Button *)Helper::seekWidgetByName(ppanel,"Button_enter");
		btn_Wait->setTouchEnabled(true);
		btn_Wait->setPressedActionEnabled(true);
		btn_Wait->addTouchEventListener(CC_CALLBACK_2(WaitDialogPanelUI::WaitEvent, this));

		auto btn_CancelWait=(Button *)Helper::seekWidgetByName(ppanel,"Button_cancel");
		btn_CancelWait->setTouchEnabled(true);
		btn_CancelWait->setPressedActionEnabled(true);
		btn_CancelWait->addTouchEventListener(CC_CALLBACK_2(WaitDialogPanelUI::CancelWaitEvent, this));

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(ppanel->getContentSize());

		return true;
	}
	return false;
}

bool WaitDialogPanelUI::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return true;
}

void WaitDialogPanelUI::onEnter()
{
	UIScene::onEnter();
}

void WaitDialogPanelUI::onExit()
{
	UIScene::onExit();
}

void WaitDialogPanelUI::WaitEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		FivePersonInstance::isStopUpdate = false;
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void WaitDialogPanelUI::CancelWaitEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (FivePersonInstance::getSignedUpId() == -1)
		{

		}
		else
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1921, (void *)FivePersonInstance::getSignedUpId());
		}
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}
