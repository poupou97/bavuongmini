
#ifndef _FIVEPERSONINSTANCE_REWARDCARDITEM_H_
#define _FIVEPERSONINSTANCE_REWARDCARDITEM_H_

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CRewardProp;

class RewardCardItem : public UIScene
{
public:
	RewardCardItem();
	~RewardCardItem();

	static RewardCardItem * create(CRewardProp *rewardProp , int timeDouble);
	bool init(CRewardProp *rewardProp , int timeDouble);

	void BackCradEvent(Ref *pSender, Widget::TouchEventType type);
	void FrontCradEvent(Ref *pSender, Widget::TouchEventType type);

	void FlopAnimationToFront();
	void FlopAnimationToBack();

	void setBtnTouchEnabled();
	void presentDesLable();

	void RefreshCardInfo(CRewardProp *rewardProp,std::string roleName,long long roleId);
	void RefreshCostValue(std::string costValue,std::string iconPath);

private:
	Button * btn_backCardFrame;
	Text * l_des;
	Text * l_costValue;
	ImageView * image_unit;
	Button * btn_frontCardFrame;
	ImageView * imageView_frame;
	ImageView * imageView_icon;
	Label * l_rewardName;
	Label * l_roleName;

	//����λ�
	int grid;
	//õ�ǰ��ʾ�����(true)滹�Ǳ��(false)
	bool isFrontOrBack;
	//��Ƿ��Ѿ�����
	bool isFloped;
};

#endif