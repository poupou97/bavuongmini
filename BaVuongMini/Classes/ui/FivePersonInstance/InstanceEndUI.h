#ifndef _UI_INSTANCE_INSTANCEEND_H_
#define _UI_INSTANCE_INSTANCEEND_H_

#include "../extensions/UIScene.h"
#include "../backpackscene/PacPageView.h"
#include "cocostudio\CCArmature.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

#define  RewardCardItemBaseTag 720


class CFivePersonInstanceEndInfo;
class CRewardProp;

class InstanceEndUI : public UIScene
{
public:
	InstanceEndUI();
	~InstanceEndUI();

	struct Coordinate
	{
		float x;
		float y;
	};

	static InstanceEndUI *create(CFivePersonInstanceEndInfo * fivePersonInstanceEndInfo);
	bool init(CFivePersonInstanceEndInfo * fivePersonInstanceEndInfo);
	virtual void onEnter();
	virtual void onExit();

	void callBackExit(Ref *pSender, Widget::TouchEventType type);

	void exitInstanceCallback(Ref* pSender);

	void update(float dt);

	void setPayForCardValue();
	void setRemainNumValue(int _value);
	int getRemainNumValue();

	void setAllNumValue(int _value);
	int getAllNumValue();

	//��ʼչʾ�
	void showAllRewarsCard();
	//ư������ƶ��������
	void AllCardTurnToBack();
	//濪ʼϴ�
	void BeginShuffle();

	void createAnimation();
	void createUI();

	Layer * getULayer();

	void ArmatureFinishCallback(cocostudio::CCArmature *armature, cocostudio::MovementEventType movementType, const char *movementID);

protected:
	void showUIAnimation();
	void startSlowMotion();

	void addLightAnimation();
public:
	void endSlowMotion();
	
	// star end action callBack (++ by LiuLiang)
	void starActionCallBack();

private:
	std::vector<CRewardProp*>curRewardPropList;
	std::vector<Coordinate> Coordinate_card;

	Layout *panel_1;
	Layout *panel_2;
	Layer * layer_1;
	Layer * layer_2;

	Layer * layer_base;

	ImageView * imageView_Win;

	int remainTime;
	Text * l_remainTime_panel1;
	Text * l_remainTime_panel2;
	long long countDown_startTime;

	//free num
	Text * l_allFreeNum;
	int m_nAllNum;
	//remain num
	Text * l_remainNum;
	int m_nRemainNum;

	Text * l_payForCardValue;
	Text * l_IngotValue;

	CFivePersonInstanceEndInfo * curFivePersonInstanceEndInfo;

	Layer * u_layer;

	Button * btn_close;
	//���ʾ�رհ�ť
	void PresentBtnClose();
	//��ʼ��ѧ
	void BeginToturial();

public:
	void addToturial();
	void removeToturial();
};

#endif