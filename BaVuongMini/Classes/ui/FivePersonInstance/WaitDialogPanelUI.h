#ifndef _FIVEPERSONINSTANCE_WAITDIALOGPANELUI_H_
#define _FIVEPERSONINSTANCE_WAITDIALOGPANELUI_H_

#include "../extensions/UIScene.h"

class WaitDialogPanelUI : public UIScene
{
public:
	WaitDialogPanelUI();
	~WaitDialogPanelUI();

	static WaitDialogPanelUI* create();
	bool init();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);

	virtual void onEnter();
	virtual void onExit();

	void WaitEvent(Ref *pSender, Widget::TouchEventType type);
	void CancelWaitEvent(Ref *pSender, Widget::TouchEventType type);
};

#endif

