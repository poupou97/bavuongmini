#ifndef _UI_INSTANCE_INSTANCEMAPUI_H_
#define _UI_INSTANCE_INSTANCEMAPUI_H_

#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
class CFivePersonInstance;

class InstanceMapUI : public UIScene
{
public:
	InstanceMapUI();
	~InstanceMapUI();

	static InstanceMapUI *create();
	bool init();
	virtual void onEnter();
	virtual void onExit();

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	void BtnInfoEvent(Ref *pSender, Widget::TouchEventType type);
	void OpenDetailUI(int instanceId);
	void AddPhyEvent(Ref *pSender, Widget::TouchEventType type);

	void RefreshInstanceInfo(CFivePersonInstance * fivePersonInstance);
	//
	void RefreshInstanceStar();
	//present selected instanceIcon in center As much as possible
	void ScrollInstanceToCenter(int instanceId);
	void callBackScroll();

protected:
	Layer * layer;
	ui::ScrollView * p_MyDragPanel;
	ImageView *ImageView_select;

	ImageView *m_pImageView_phy;
	Text * m_pLabelBMF_phyValue;
	Button * btn_addPhy;

	int curInstanceId;
	Vec2 curInstancePos;

	Layout *panel_tutorial;

public:
	//��ѧ
	int mTutorialScriptInstanceId;
	virtual void registerScriptCommand(int scriptId);
	//ѡ��һ���
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();

	Button * btn_firstInstance;
	Button * btn_secondInstance;
	Button * btn_thirdInstance;
	Button * btn_fouthInstance;
};

#endif