#include "ResultHeadPartItem.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../generals_ui/GeneralsListUI.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/GameStatistics.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/role/BaseFighterConstant.h"
#include "../../ui/GameUIConstant.h"

ResultHeadPartItem::ResultHeadPartItem()
{

}

ResultHeadPartItem::~ResultHeadPartItem()
{

}

ResultHeadPartItem * ResultHeadPartItem::create( int expValue , int index ,int type)
{
	auto resultHeadPartItem = new ResultHeadPartItem();
	if (resultHeadPartItem && resultHeadPartItem->init(expValue,index,type))
	{
		resultHeadPartItem->autorelease();
		return resultHeadPartItem;
	}
	CC_SAFE_DELETE(resultHeadPartItem);
	return NULL;
}

bool ResultHeadPartItem::init( int expValue , int index , int type)
{
	if (UIScene::init())
	{
		int getExpValue = 0;
		int getDmgValue = 0;

		std::string icon_path;
		int roleLevel = 0;

		std::string str_name;
		Color3B nameColor;
		//�Ƿ��ս
		bool isInBattle = false;

		if (index == 0)  //myplayer
		{
			getExpValue = expValue;
			getDmgValue = DamageStatistics::getDamge(GameView::getInstance()->myplayer->getRoleId());
			//ͷ��ͼ�
			icon_path = BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession());
			roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();

			str_name = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
			nameColor = Color3B(0,255,0);
		}
		else
		{
			if (index > 6)
				return false;

			auto generalBaseMsg = GameView::getInstance()->generalsInLineList.at(index-1);
			auto generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];
			if (generalBaseMsg->fightstatus() == GeneralsListUI::NoBattle)
			{
				getExpValue = 0;
			}
			else
			{
				if(generalBaseMsg->fightstatus() == GeneralsListUI::InBattle)            //��ѳ�ս
				{
					getExpValue = expValue*GENERAL_INBATTLE_EXPERIENCE_RATIO;
					isInBattle = true;
				}
				else if (generalBaseMsg->fightstatus() == GeneralsListUI::HoldTheLine)
				{
					getExpValue = expValue*GENERAL_HOLDTHELINE_EXPERIENCE_RATIO;
				}
			}

			getDmgValue = DamageStatistics::getDamge(generalBaseMsg->id());

			//ͷ��ͼ�
			icon_path = "res_ui/generals46X45/";
			icon_path.append(generalMsgFromDb->get_head_photo());
			icon_path.append(".png");

			roleLevel = generalBaseMsg->level();

			str_name = generalMsgFromDb->name();
			nameColor = GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality());
		}

		//create ui
		auto imageView_frame = ImageView::create();
		imageView_frame->loadTexture("res_ui/kuanga.png");
		imageView_frame->setAnchorPoint(Vec2(.0f,.0f));
		imageView_frame->setPosition(Vec2(0,50));
		m_pLayer->addChild(imageView_frame);
		//head
		auto imageView_head = ImageView::create();
		imageView_head->loadTexture(icon_path.c_str());
		imageView_head->setAnchorPoint(Vec2(.5f,.5f));
		imageView_head->setPosition(Vec2(imageView_frame->getContentSize().width/2,imageView_frame->getContentSize().height/2));
		imageView_frame->addChild(imageView_head);
		if (index == 0)  //myplayer
		{
			imageView_head->setScale(0.8f);
		}
		//��ս��ʶ
// 		if(isInBattle)
// 		{
// 			ImageView * imageView_inBattle = ImageView::create();
// 			imageView_inBattle->loadTexture("res_ui/wujiang/play.png");
// 			imageView_inBattle->setScale(IMAGEVIEW_INBATTLE_INRESULTUI_SCALE);
// 			imageView_inBattle->setAnchorPoint(Vec2(.5f,.5f));
// 			imageView_inBattle->setPosition(Vec2(imageView_frame->getPosition().x+4,imageView_frame->getContentSize().height-4));
// 			imageView_frame->addChild(imageView_inBattle);
// 		}
		//level frame
// 		ImageView * imageView_LvFrame = ImageView::create();
// 		imageView_LvFrame->loadTexture("res_ui/lv_kuang.png");
// 		imageView_LvFrame->setAnchorPoint(Vec2(1.0f,.0f));
// 		imageView_LvFrame->setPosition(Vec2(imageView_frame->getContentSize().width-2,2));
// 		imageView_frame->addChild(imageView_LvFrame);
// 		//level
// 		Label * l_level = Label::create();
// 		char s_lv[10];
// 		sprintf(s_lv,"%d",roleLevel);
// 		l_level->setText(s_lv);
// 		l_level->setAnchorPoint(Vec2(.5f,.5f));
// 		l_level->setPosition(Vec2(-imageView_LvFrame->getContentSize().width/2,imageView_LvFrame->getContentSize().height/2));
// 		imageView_LvFrame->addChild(l_level);
		//nameFrame
		auto imageView_nameFrame = ImageView::create();
		imageView_nameFrame->loadTexture("res_ui/LV4_diaa.png");
		imageView_nameFrame->setScale9Enabled(true);
		imageView_nameFrame->setContentSize(Size(72,22));
		imageView_nameFrame->setCapInsets(Rect(11,11,1,1));
		imageView_nameFrame->setAnchorPoint(Vec2(.5f,1.0f));
		imageView_nameFrame->setPosition(Vec2(imageView_frame->getContentSize().width/2,50));
		m_pLayer->addChild(imageView_nameFrame);
		//name
		auto l_name = Label::createWithTTF(str_name.c_str(), APP_FONT_NAME, 12);
		l_name->setColor(nameColor);
		l_name->setAnchorPoint(Vec2(.5f,.5f));
		l_name->setPosition(Vec2(0,-11));
		imageView_nameFrame->addChild(l_name);
		//Dmg
		auto l_dmgName = Label::createWithBMFont("res_ui/font/ziti_10.fnt",StringDataManager::getString("FamilyFightUI_dmg"));
		l_dmgName->setAnchorPoint(Vec2(0,0.5f));
		l_dmgName->setPosition(Vec2(0,7));
		m_pLayer->addChild(l_dmgName);
		//DmgValue
		auto dmgEffect = NumberRollEffect::create(getDmgValue,"res_ui/font/ziti_10.fnt",1.5f,0);
		dmgEffect->setAnchorPoint(Vec2(.0f,.5f));
		dmgEffect->setPosition(Vec2(l_dmgName->getContentSize().width+3,l_dmgName->getPosition().y));
		addChild(dmgEffect);
		//EXP
// 		Label * l_expName = Label::create();
// 		l_expName->setFntFile("res_ui/font/ziti_9.fnt");
// 		l_expName->setText("EXP+");
// 		l_expName->setAnchorPoint(Vec2(0,0.5f));
// 		l_expName->setPosition(Vec2(0,20));
// 		m_pLayer->addChild(l_expName);
// 		//ExpValue
// 		NumberRollEffect * expEffect = NumberRollEffect::create(getExpValue,"res_ui/font/ziti_9.fnt",1.5f,0);
// 		expEffect->setAnchorPoint(Vec2(.0f,.5f));
// 		expEffect->setPosition(Vec2(l_expName->getContentSize().width,l_expName->getPosition().y));
// 		addChild(expEffect);

		imageView_frame->setPosition(Vec2(0,36));
		imageView_nameFrame->setPosition(Vec2(imageView_frame->getContentSize().width/2,36));
		l_dmgName->setVisible(true);
		l_dmgName->setPosition(Vec2(0,0));
		dmgEffect->setVisible(true);
		dmgEffect->setPosition(Vec2(l_dmgName->getContentSize().width+3,l_dmgName->getPosition().y));

		this->setContentSize(Size(imageView_frame->getContentSize().width,imageView_frame->getContentSize().height+imageView_nameFrame->getContentSize().height+15));


// 		switch(type)
// 		{
// 		case type_allExist:
// 			{
// 				l_dmgName->setVisible(true);
// 				l_dmgName->setPosition(Vec2(0,7));
// 				dmgEffect->setVisible(true);
// 				dmgEffect->setPosition(Vec2(l_dmgName->getContentSize().width+3,l_dmgName->getPosition().y));
// // 				l_expName->setVisible(true);
// // 				l_expName->setPosition(Vec2(0,20));
// // 				expEffect->setVisible(true);
// // 				expEffect->setPosition(Vec2(l_expName->getContentSize().width,l_expName->getPosition().y));
// 
// 				this->setContentSize(Size(imageView_frame->getContentSize().width,imageView_frame->getContentSize().height+imageView_nameFrame->getContentSize().height+28));
// 			}
// 			break;
// 		case type_dmgOnly:
// 			{
// 				imageView_frame->setPosition(Vec2(0,36));
// 				imageView_nameFrame->setPosition(Vec2(imageView_frame->getContentSize().width/2,36));
// 				l_dmgName->setVisible(true);
// 				l_dmgName->setPosition(Vec2(0,7));
// 				dmgEffect->setVisible(true);
// 				dmgEffect->setPosition(Vec2(l_dmgName->getContentSize().width+3,l_dmgName->getPosition().y));
// // 				l_expName->setVisible(false);
// // 				expEffect->setVisible(false);
// 
// 				this->setContentSize(Size(imageView_frame->getContentSize().width,imageView_frame->getContentSize().height+imageView_nameFrame->getContentSize().height+15));
// 			}
// 			break;
// 		case type_expOnly:
// 			{
// 				imageView_frame->setPosition(Vec2(0,36));
// 				imageView_nameFrame->setPosition(Vec2(imageView_frame->getContentSize().width/2,36));
// 				l_dmgName->setVisible(false);
// 				dmgEffect->setVisible(false);
// 				
// // 				l_expName->setVisible(true);
// // 				expEffect->setVisible(true);
// // 				l_expName->setPosition(Vec2(0,7));
// // 				expEffect->setPosition(Vec2(l_expName->getContentSize().width,l_expName->getPosition().y));
// 
// 				this->setContentSize(Size(imageView_frame->getContentSize().width,imageView_frame->getContentSize().height+imageView_nameFrame->getContentSize().height+15));
// 			}
// 			break;
// 		}

		
		return true;
	}
	return false;
}

