#ifndef _INSTANCE_INSTANCEFUNCTIONPANEL_H_
#define _INSTANCE_INSTANCEFUNCTIONPANEL_H_

#include "../../ui/extensions/uiscene.h"
#include "cocos-ext.h"
#include "cocos2d.h"

class InstanceFunctionPanel :public UIScene
{
public:
	InstanceFunctionPanel();
	~InstanceFunctionPanel();

	static InstanceFunctionPanel* create();
	bool init();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);

	virtual void onEnter();
	virtual void onExit();

	void callFriendCallback(Ref *pSender, Widget::TouchEventType type);
	void exitInstanceCallback(Ref *pSender, Widget::TouchEventType type);

	void sureToExit(Ref *pSender);

private:

};

#endif;

