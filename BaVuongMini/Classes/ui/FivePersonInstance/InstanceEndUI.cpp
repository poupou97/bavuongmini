#include "InstanceEndUI.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CFivePersonInstanceEndInfo.h"
#include "RewardCardItem.h"
#include "../../messageclient/element/CRewardProp.h"
#include "../../messageclient/element/CVipInfo.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../GameUIConstant.h"
#include "../../utils/GameUtils.h"
#include "ResultHeadPartItem.h"
#include "../../gamescene_state/GameSceneCamera.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "FivePersonInstance.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../gamescene_state/MainAnimationScene.h"

#define UISCENE_MAIN_SCENE_TAG 1000
#define TAG_IMAGEVIEW_STAR_BASE 500
#define kTagULayer 600
#define TAG_LIGHTANIMATION 700

InstanceEndUI::InstanceEndUI():
remainTime(60000),
countDown_startTime(0)
{
	//coordinate for card
	for (int m = 1;m>=0;m--)
	{
		for (int n = 5;n>=0;n--)
		{
			int _m = 1-m;
			int _n = 5-n;

			Coordinate temp;
			temp.x = 205+_n*94 ;
			temp.y = 187+m*119;

			Coordinate_card.push_back(temp);
		}
	}
}

InstanceEndUI::~InstanceEndUI()
{
	std::vector<CRewardProp*>::iterator iter;
	for (iter = curRewardPropList.begin(); iter != curRewardPropList.end(); ++iter)
	{
		delete *iter;
	}
	curRewardPropList.clear();

	delete curFivePersonInstanceEndInfo;
}

InstanceEndUI * InstanceEndUI::create(CFivePersonInstanceEndInfo * fivePersonInstanceEndInfo)
{
	auto _ui=new InstanceEndUI();
	if (_ui && _ui->init(fivePersonInstanceEndInfo))
	{
		_ui->autorelease();
		return _ui;
	}
	CC_SAFE_DELETE(_ui);
	return NULL;
}

bool InstanceEndUI::init(CFivePersonInstanceEndInfo * fivePersonInstanceEndInfo)
{
	if (UIScene::init())
	{
		countDown_startTime = GameUtils::millisecondNow();
		m_nAllNum = 3;
		m_nRemainNum = 3;

		VipConfigData::TypeIdAndVipLevel typeAndLevel;
		typeAndLevel.typeName = StringDataManager::getString("vip_function_instance_reward");
		typeAndLevel.vipLevel = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel();

		std::map<VipConfigData::TypeIdAndVipLevel,CVipInfo*>::const_iterator cIter;
		cIter = VipConfigData::s_vipConfig.find(typeAndLevel);
		if (cIter == VipConfigData::s_vipConfig.end()) // û�ҵ�����ָ�END��  
		{
		}
		else
		{
			auto tempVipInfo = cIter->second;
			m_nAllNum += tempVipInfo->get_add_number();
			m_nRemainNum += tempVipInfo->get_add_number();
		}

		Size size = Director::getInstance()->getVisibleSize();
		curFivePersonInstanceEndInfo = new CFivePersonInstanceEndInfo();
		curFivePersonInstanceEndInfo->copyFrom(fivePersonInstanceEndInfo);

		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		if (!mainscene)
			return false;

		auto action =(ActionInterval *)Sequence::create(
			DelayTime::create(END_BATTLE_BEFORE_SLOWMOTION_DURATION),
			CallFunc::create(CC_CALLBACK_0(InstanceEndUI::startSlowMotion,this)),
			CallFunc::create(CC_CALLBACK_0(InstanceEndUI::addLightAnimation,this)),
			DelayTime::create(END_BATTLE_SLOWMOTION_DURATION),
			CallFunc::create(CC_CALLBACK_0(InstanceEndUI::endSlowMotion,this)),
			CallFunc::create(CC_CALLBACK_0(InstanceEndUI::showUIAnimation,this)),
			DelayTime::create(2.0f),
			CallFunc::create(CC_CALLBACK_0(InstanceEndUI::createAnimation,this)),
			NULL);
		this->runAction(action);	

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(size);
		return true;
	}
	return false;
}

void InstanceEndUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void InstanceEndUI::onExit()
{
	UIScene::onExit();
}

void InstanceEndUI::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		// request to exit the instance
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1924);
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void InstanceEndUI::exitInstanceCallback(Ref* pSender)
{
	// request to exit the instance
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1924);
}

void InstanceEndUI::AllCardTurnToBack()
{
	for(int i = 0;i<12;++i)
	{
		if (u_layer->getChildByTag(RewardCardItemBaseTag +i))
		{
			auto rewardCardItem = (RewardCardItem*)u_layer->getChildByTag(RewardCardItemBaseTag +i);
			rewardCardItem->FlopAnimationToBack();
		}
	}
}

void InstanceEndUI::BeginShuffle()
{
 	Vec2 _pos = Vec2((Coordinate_card.at(5).x+Coordinate_card.at(6).x)/2,(Coordinate_card.at(5).y+Coordinate_card.at(6).y)/2);

// 	int indexArray[12];
// 	for (int i = 0;i<12;i++)
// 	{
// 		indexArray[i] = i;
// 	}

	for(int i = 0;i<12;++i)
	{
		if (u_layer->getChildByTag(RewardCardItemBaseTag +curRewardPropList.at(i)->grid()))
		{
// 			int random_index = (int)(CCRANDOM_0_1()*(12-i)); //���0-11ɵ��������0�11
// 			int random_value = indexArray[random_index];
// 			CCLOG("random_index = %d",random_index);
// 			CCLOG("random_value = %d",random_value);
// 			for(int j=random_index;j<12-i-1;j++)
// 			{
// 				indexArray[j]=indexArray[j+1];
// 			}
// 			indexArray[12-i-1] = NULL;

			auto rewardCardItem = (RewardCardItem*)u_layer->getChildByTag(RewardCardItemBaseTag +i);
			auto action1 = Sequence::create(
				MoveTo::create(REWARD_CARD_MOVE_TIME,_pos),
				DelayTime::create(0.5f),
				//MoveTo::create(0.7f,Vec2(Coordinate_card.at(random_value).x,Coordinate_card.at(random_value).y)),
				MoveTo::create(REWARD_CARD_MOVE_TIME,Vec2(Coordinate_card.at(i).x,Coordinate_card.at(i).y)),
				CallFunc::create(CC_CALLBACK_0(RewardCardItem::setBtnTouchEnabled, rewardCardItem)),
				CallFunc::create(CC_CALLBACK_0(RewardCardItem::presentDesLable, rewardCardItem)),
				NULL);
			rewardCardItem->runAction(action1);
		}
	}

	auto temp = Sequence::create(
		DelayTime::create(REWARD_CARD_MOVE_TIME+0.5f+REWARD_CARD_MOVE_TIME),
		CallFunc::create(CC_CALLBACK_0(InstanceEndUI::PresentBtnClose, this)),
		CallFunc::create(CC_CALLBACK_0(InstanceEndUI::BeginToturial, this)),
		NULL
		);
	this->runAction(temp);
}

void InstanceEndUI::update( float dt )
{
	if (remainTime >0)
	{
		remainTime -= 1000;

		std::string str_remainTime = "";
		char s_remainTime[10];
		sprintf(s_remainTime,"%d",remainTime/1000);
		str_remainTime.append(s_remainTime);
		l_remainTime_panel1->setString(str_remainTime.c_str());
		l_remainTime_panel2->setString(str_remainTime.c_str());

// 		char str_ingot[20];
// 		sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
// 		l_IngotValue->setText(str_ingot);
	}
	else
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1924);
		this->closeAnim();
	}
}

void InstanceEndUI::setPayForCardValue()
{
	//l_payForCardValue->setText(str.c_str());
	//ͷ��Ʒ�� //�ʣ����
	std::string str_payForCard = "";
	std::string path_icon = "";
	if (this->getRemainNumValue() == 0)   
	{
		str_payForCard.append("100");
		//str_payForCard.append(StringDataManager::getString("skillui_goldingold"));
		path_icon = "res_ui/ingot.png";
	}
	else if(this->getRemainNumValue()  == 1)       //100�Ԫ��  
	{
		str_payForCard.append("100");
		//str_payForCard.append(StringDataManager::getString("skillui_goldingold"));
		path_icon = "res_ui/ingot.png";
	}
	else if(this->getRemainNumValue()  == 2)     //1000��
	{
		str_payForCard.append("1000");
		//str_payForCard.append(StringDataManager::getString("skillui_gold"));
		path_icon = "res_ui/coins.png";
	}
	else
	{
		str_payForCard.append(StringDataManager::getString("game_free"));
		path_icon = "";
	}

	for(int i = 0;i<12;++i)
	{
		auto rewardCardItem = (RewardCardItem*)u_layer->getChildByTag(RewardCardItemBaseTag +i);
		if (rewardCardItem)
		{
			rewardCardItem->RefreshCostValue(str_payForCard,path_icon);
		}
	}
}

void InstanceEndUI::setRemainNumValue(int _value )
{
	m_nRemainNum = _value;
	char s_remainNum [10];
	sprintf(s_remainNum,"%d",m_nRemainNum);
	l_remainNum->setString(s_remainNum);
}

int InstanceEndUI::getRemainNumValue()
{
	return m_nRemainNum;
}

void InstanceEndUI::setAllNumValue(int _value)
{
	m_nAllNum = _value;
	char s_allNum [10];
	sprintf(s_allNum,"%d",m_nAllNum);
	l_allFreeNum->setString(s_allNum);
}

int InstanceEndUI::getAllNumValue()
{
	return m_nAllNum;
}

void InstanceEndUI::createAnimation()
{
	CCAssert(curFivePersonInstanceEndInfo->rewardProps.size() >= 12, "card num < 12");

	Size size=Director::getInstance()->getVisibleSize();

	for (unsigned int i =0;i<curFivePersonInstanceEndInfo->rewardProps.size();i++)
	{
		auto rewardProp = new CRewardProp();
		rewardProp->CopyFrom(*curFivePersonInstanceEndInfo->rewardProps.at(i));
		curRewardPropList.push_back(rewardProp);
	}

	Size winSize = Director::getInstance()->getVisibleSize();
	u_layer=Layer::create();
	u_layer->setIgnoreAnchorPointForPosition(false);
	u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
	u_layer->setPosition(Vec2(winSize.width/2,winSize.height/2));
	u_layer->setContentSize(Size(800,480));
	u_layer->setTag(kTagULayer);
	addChild(u_layer);

	layer_base  = Layer::create();
	u_layer->addChild(layer_base);

	layer_1 = Layer::create();
	u_layer->addChild(layer_1);

	layer_2 = Layer::create();
	u_layer->addChild(layer_2);

	Size sizeMengBan = Size(winSize.width + 20, winSize.height + 20);

	auto mengban = ImageView::create();
	mengban->loadTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enabled(true);
	mengban->setContentSize(sizeMengBan);
	mengban->setAnchorPoint(Vec2(0,0));
	mengban->setPosition(Vec2(-10, -10));
	m_pLayer->addChild(mengban);

	//Ҵӵ����ļ��첽���ض���
	cocostudio::CCArmatureDataManager::getInstance()->addArmatureFileInfo("res_ui/uiflash/winLogo/winLogo0.png","res_ui/uiflash/winLogo/winLogo0.plist","res_ui/uiflash/winLogo/winLogo.ExportJson");
	//��ݶ�����ƴ����������
	auto armature = cocostudio::CCArmature::create("winLogo");
	//鲥��ָ�����
	//armature->getAnimation()->playByIndex(0,-1,-1,ANIMATION_NO_LOOP);
	armature->getAnimation()->play("Animation_begin");
	//��޸����
	armature->setScale(1.0f);
	//����ö�������λ�
	armature->setPosition(Vec2(UI_DESIGN_RESOLUTION_WIDTH/2,UI_DESIGN_RESOLUTION_HEIGHT/2));

	armature->getAnimation()->setMovementEventCallFunc(this,movementEvent_selector(InstanceEndUI::ArmatureFinishCallback));  
	//���ӵ���ǰҳ�
	layer_base->addChild(armature);

	int space = 8;
	int width = 45;
	float firstPos_x = 400- space*0.5f*(curFivePersonInstanceEndInfo->get_star()-1) - width*0.5f*(curFivePersonInstanceEndInfo->get_star()-1);
	for (int i = 0;i<curFivePersonInstanceEndInfo->get_star();++i)
	{
	 	if (i >= 5)
	 		continue;
	 
		auto sprite_star = Sprite::create("res_ui/star2.png");
	 	sprite_star->setAnchorPoint(Vec2(0.5f,0.5f));
	 	sprite_star->setPosition(Vec2(firstPos_x+(width+space)*i,200));
	 	layer_1->addChild(sprite_star);
	 	sprite_star->setTag(TAG_IMAGEVIEW_STAR_BASE+i);
	 	sprite_star->setVisible(false);
		sprite_star->setScale(10.0f);
		sprite_star->setOpacity(25);


		auto win_action = Sequence::create(
	 		DelayTime::create(0.5f+0.25f*i),
	 		Show::create(),
			CCEaseExponentialIn::create(Spawn::create(
				ScaleTo::create(0.25f,1.0f),
				FadeTo::create(0.25f, 255),
				NULL)),
			CallFunc::create(CC_CALLBACK_0(InstanceEndUI::starActionCallBack, this)),
	 		NULL);
	 	sprite_star->runAction(win_action);
	 
		auto sequence = Sequence::create(DelayTime::create(2.2f),
	 		CCEaseOut::create(MoveTo::create(0.2f,Vec2(sprite_star->getPosition().x,335)),2.2f),
	 		NULL);
	 	sprite_star->runAction(sequence);
	}

	auto sequence = Sequence::create(
		DelayTime::create(2.55f),
		CallFunc::create(CC_CALLBACK_0(InstanceEndUI::createUI, this)),
		DelayTime::create(3.0f),
		CallFunc::create(CC_CALLBACK_0(InstanceEndUI::showAllRewarsCard, this)),
		NULL
		);
	this->runAction(sequence);

	/************************************************************/
// 	//�ʤ���ķ�
// 	ImageView * imageView_light = ImageView::create();
// 	imageView_light->loadTexture("res_ui/lightlight.png");
// 	imageView_light->setAnchorPoint(Vec2(0.5f,0.5f));
// 	imageView_light->setPosition(Vec2(400,240));
// 	u_layer->addChild(imageView_light);
// 	imageView_light->setVisible(false);
// 	//�ʤ��ͼ�
// 	ImageView * imageView_result = ImageView::create();
// 	imageView_result->loadTexture("res_ui/jiazuzhan/win.png");
// 	imageView_result->setAnchorPoint(Vec2(0.5f,0.5f));
// 	imageView_result->setPosition(Vec2(400,240));
// 	imageView_result->setScale(RESULT_UI_RESULTFLAG_BEGINSCALE);
// 	u_layer->addChild(imageView_result);
// 
// 	int space = 8;
// 	int width = 45;
// 	float firstPos_x = 400- space*0.5f*(curFivePersonInstanceEndInfo->get_star()-1) - width*0.5f*(curFivePersonInstanceEndInfo->get_star()-1);
// 
// 	for (int i = 0;i<curFivePersonInstanceEndInfo->get_star();++i)
// 	{
// 		if (i >= 5)
// 			continue;
// 
// 		ImageView* imageView_star = ImageView::create();
// 		imageView_star->loadTexture("res_ui/star2.png"); 
// 		imageView_star->setAnchorPoint(Vec2(0.5f,0.5f));
// 		imageView_star->setPosition(Vec2(firstPos_x+(width+space)*i,195));
// 		layer_1->addChild(imageView_star);
// 		imageView_star->setTag(TAG_IMAGEVIEW_STAR_BASE+i);
// 		imageView_star->setVisible(false);
// 		imageView_star->setScale(2.5f);
// 		FiniteTimeAction*  win_action = Sequence::create(
// 			DelayTime::create(0.25f+0.3f*i),
// 			Show::create(),
// 			CCEaseExponentialIn::create(ScaleTo::create(0.25f,1.0f)),
// 			NULL);
// 		imageView_star->runAction(win_action);
// 
// 		Sequence * sequence = Sequence::create(DelayTime::create(2.0f+RESULT_UI_RESULTFLAG_SCALECHANGE_TIME),
// 			MoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,Vec2(imageView_star->getPosition().x,355)),
// 			NULL);
// 		imageView_star->runAction(sequence);
// 	}
// 
// 	Sequence * sequence = Sequence::create(ScaleTo::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME,1.0f,1.0f),
// 		DelayTime::create(2.0f),
// 		MoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,Vec2(400,403)),
// 		DelayTime::create(0.05f),
// 		CallFunc::create(this,callfunc_selector(InstanceEndUI::createUI)),
// 		DelayTime::create(5.0f),
// 		CallFunc::create(this,callfunc_selector(InstanceEndUI::showAllRewarsCard)),
// 		NULL);
// 	imageView_result->runAction(sequence);
// 
// 	CCRotateBy * rotateAnm = RotateBy::create(4.0f,360);
// 	RepeatForever * repeatAnm = RepeatForever::create(Sequence::create(rotateAnm,NULL));
// 	Sequence * sequence_light = Sequence::create(
// 		DelayTime::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME),
// 		Show::create(),
// 		DelayTime::create(2.0f),
// 		MoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,Vec2(400,403)),
// 		NULL);
// 	imageView_light->runAction(sequence_light);
// 	imageView_light->runAction(repeatAnm);
}

void InstanceEndUI::createUI()
{
	Size size = Director::getInstance()->getVisibleSize();
	if(LoadSceneLayer::instanceEndLayout->getParent() != NULL)
	{
		LoadSceneLayer::instanceEndLayout->removeFromParentAndCleanup(false);
	}

	auto mainPanel=LoadSceneLayer::instanceEndLayout;
	mainPanel->setAnchorPoint(Vec2(0.5f,0.5f));
	mainPanel->setPosition(Vec2(size.width/2,size.height/2-20));
	mainPanel->setTag(UISCENE_MAIN_SCENE_TAG);
	m_pLayer->addChild(mainPanel);

	panel_1= (Layout *)Helper::seekWidgetByName(mainPanel,"Panel_InstanceEnd_1");
	panel_1->setVisible(true);
	layer_1->setVisible(true);
	panel_2= (Layout *)Helper::seekWidgetByName(mainPanel,"Panel_InstanceEnd_2");
	panel_2->setVisible(false);
	layer_2->setVisible(false);

// 	const char * secondStr = StringDataManager::getString("UIName_fu");
// 	const char * thirdStr = StringDataManager::getString("UIName_ben");
// 	CCArmature * atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
// 	atmature->setPosition(Vec2(89,215));
// 	u_layer->addChild(atmature);
// 
	btn_close= (Button *)Helper::seekWidgetByName(mainPanel,"Button_close");
	CCAssert(btn_close != NULL, "should not be nil");
	btn_close->setTouchEnabled(true);
	btn_close->setPressedActionEnabled(true);
	btn_close->addTouchEventListener(CC_CALLBACK_2(InstanceEndUI::callBackExit, this));
	btn_close->setVisible(false);

// 	imageView_Win = ImageView::create();
// 	imageView_Win->loadTexture("res_ui/instance_end/yesyes.png"); 
// 	imageView_Win->setAnchorPoint(Vec2(0.5f,0.5f));
// 	imageView_Win->setPosition(Vec2(217,382));
// 	u_layer->addChild(imageView_Win);
// 	imageView_Win->setScale(2.5f);
// 	FiniteTimeAction*  win_action = Sequence::create(
// 		CCEaseExponentialIn ::create(ScaleTo::create(0.4f,1.0f)),
// 		NULL);
// 	imageView_Win->runAction(win_action);
// 
// 	for (int i = 0;i<curFivePersonInstanceEndInfo->get_star();++i)
// 	{
// 		if (i >= 5)
// 			continue;
// 
// 		ImageView* imageView_star = ImageView::create();
// 		imageView_star->loadTexture("res_ui/star_on.png"); 
// 		imageView_star->setAnchorPoint(Vec2(0.f,0.f));
// 		imageView_star->setPosition(Vec2(414+27*i,390));
// 		u_layer->addChild(imageView_star);
// 		imageView_star->setVisible(false);
// 		imageView_star->setScale(2.5f);
// 		FiniteTimeAction*  win_action = Sequence::create(
// 			DelayTime::create(0.45f+0.3f*i),
// 			Show::create(),
// 			CCEaseExponentialIn::create(ScaleTo::create(0.25f,1.0f)),
// 			NULL);
// 		imageView_star->runAction(win_action);
// 	}

	l_remainTime_panel1 = (Text*)Helper::seekWidgetByName(mainPanel,"Label_RemainingTime_value");
	l_remainTime_panel2 = (Text*)Helper::seekWidgetByName(mainPanel,"Label_RemainingTime_value_0");
	//귭�ƴ��
	l_allFreeNum = (Text*)Helper::seekWidgetByName(panel_2,"Label_retainMax");
	setAllNumValue(m_nAllNum);
	l_remainNum = (Text*)Helper::seekWidgetByName(panel_2,"Label_retainNum");
	setRemainNumValue(m_nRemainNum);
	//�ǰԪ��
// 	l_IngotValue = (Text*)Helper::seekWidgetByName(panel_2,"Label_IngotValue");
// 	char s_playerGoldIngot[20];
// 	sprintf(s_playerGoldIngot,"%d",GameView::getInstance()->getPlayerGoldIngot());
// 	l_IngotValue->setText(s_playerGoldIngot);
	//���ƻ��
	l_payForCardValue = (Text*)Helper::seekWidgetByName(panel_2,"Label_PayForCard_value");
	//setPayForCardValue(StringDataManager::getString("game_free"));
	setPayForCardValue();
	//Ѿ��
// 	Label * l_exp = (Text*)Helper::seekWidgetByName(mainPanel,"Label_RewardExp");
// 	char s_exp[20];
// 	sprintf(s_exp,"%d",curFivePersonInstanceEndInfo->get_exp());
// 	l_exp->setText(s_exp);
 	//��
	auto l_gold = (Text*)Helper::seekWidgetByName(panel_1,"Label_RewardCoins");
	char s_gold[20];
	sprintf(s_gold,"%d",curFivePersonInstanceEndInfo->get_gold());
	l_gold->setString(s_gold);
	//���꾫��
	auto l_musouValue = (Text*)Helper::seekWidgetByName(panel_1,"Label_musouValue");
	char s_musou[20];
	sprintf(s_musou,"%d",curFivePersonInstanceEndInfo->get_dragonValue());
	l_musouValue->setString(s_musou);

	remainTime -= (GameUtils::millisecondNow() - countDown_startTime - 6);

	std::string str_remainTime = "";
	char s_remainTime[10];
	sprintf(s_remainTime,"%d",remainTime/1000);
	str_remainTime.append(s_remainTime);
	l_remainTime_panel1->setString(str_remainTime.c_str());
	l_remainTime_panel2->setString(str_remainTime.c_str());

	this->schedule(schedule_selector(InstanceEndUI::update),1.0f);

	//չʾ�佫�þ��
	int space = 19;
	int width = 66;
	int allsize = GameView::getInstance()->generalsInLineList.size()+1;
	float firstPos_x = 400- (space+width)*0.5f*(allsize-1);

	for (int i = 0;i<allsize;++i)
	{
		if (i >= 6)
			continue;

		auto temp = ResultHeadPartItem::create(curFivePersonInstanceEndInfo->get_exp(),i);
		temp->setIgnoreAnchorPointForPosition(false);
		temp->setAnchorPoint(Vec2(0.5f,0.5f));
		temp->setPosition(Vec2(firstPos_x+(width+space)*i,148));
		layer_1->addChild(temp);
	}
}

void InstanceEndUI::showUIAnimation()
{
// 	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
// 	if (mainscene != NULL)
// 		mainscene->addInterfaceAnm("fbtg/fbtg.anm");

	auto mainAnmScene = GameView::getInstance()->getMainAnimationScene();
	if (mainAnmScene)
	{
		mainAnmScene->addInterfaceAnm("fbtg/fbtg.anm");
	}
}

void InstanceEndUI::startSlowMotion()
{
	Director::getInstance()->getScheduler()->setTimeScale(0.2f);
}
void InstanceEndUI::endSlowMotion()
{
	Director::getInstance()->getScheduler()->setTimeScale(1.0f);
}

void InstanceEndUI::showAllRewarsCard()
{
	for (int i = 0;i<curFivePersonInstanceEndInfo->get_star();++i)
	{
		if (layer_1->getChildByTag(TAG_IMAGEVIEW_STAR_BASE+i))
		{
			auto fadeoutAction = FadeOut::create(0.5f);
			layer_1->getChildByTag(TAG_IMAGEVIEW_STAR_BASE+i)->runAction(fadeoutAction);
		}
	}

 	panel_1->setVisible(false);
 	layer_1->setVisible(false);
 	panel_2->setVisible(true);
 	layer_2->setVisible(true);
 	
// 	panel_1->setVisible(true);
// 	layer_1->setVisible(true);
// 	panel_2->setVisible(true);
// 	layer_2->setVisible(true);
// 	FadeOut * action_panel_1 = FadeOut::create(0.5f);
// 	panel_1->runAction(action_panel_1);
// 	FadeOut * action_layer_1 = FadeOut::create(0.5f);
// 	layer_1->runAction(action_layer_1);
// 	FadeIn * action_panel_2 = FadeIn::create(0.5f);
// 	panel_2->runAction(action_panel_2);
// 	FadeIn * action_layer_2 = FadeIn::create(0.5f);
// 	layer_2->runAction(action_layer_2);

	Size size = Director::getInstance()->getVisibleSize();
	for(int i = 0;i<12;++i)
	{
		auto rewardCardItem = RewardCardItem::create(curFivePersonInstanceEndInfo->rewardProps.at(i),curFivePersonInstanceEndInfo->get_timeDouble());
		rewardCardItem->setIgnoreAnchorPointForPosition(false);
		rewardCardItem->setAnchorPoint(Vec2(0.5f,0.5f));
		rewardCardItem->setPosition(Vec2(Coordinate_card.at(i).x+size.width+200,Coordinate_card.at(i).y));
		rewardCardItem->setTag(RewardCardItemBaseTag+curFivePersonInstanceEndInfo->rewardProps.at(i)->grid());
		u_layer->addChild(rewardCardItem);
	}

	for(int i = 0;i<12;++i)
	{
		auto rewardCardItem = (RewardCardItem*)u_layer->getChildByTag(RewardCardItemBaseTag+curFivePersonInstanceEndInfo->rewardProps.at(i)->grid());
		if (rewardCardItem)
		{
			auto moveBack = MoveTo::create(.7f,Vec2(Coordinate_card.at(i).x,Coordinate_card.at(i).y));
			rewardCardItem->runAction(Sequence::create(DelayTime::create(.3f),moveBack,NULL));
		}
	}

	auto  action1 = Sequence::create(
		DelayTime::create(2.0f),
		CallFunc::create(CC_CALLBACK_0(InstanceEndUI::AllCardTurnToBack, this)),
		DelayTime::create(0.7f),
		CallFunc::create(CC_CALLBACK_0(InstanceEndUI::BeginShuffle, this)),
		NULL);

	this->stopAllActions();
	this->runAction(action1);
}

Layer * InstanceEndUI::getULayer()
{
	auto temp = (Layer*)this->getChildByTag(kTagULayer);
	if (temp)
	{
		return temp;
	}

	return NULL;
}

void InstanceEndUI::ArmatureFinishCallback(cocostudio::CCArmature *armature, cocostudio::MovementEventType movementType, const char *movementID )
{
	std::string id = movementID;
	if (id.compare("Animation_begin") == 0)
	{
		armature->stopAllActions();
		armature->getAnimation()->play("Animation1_end");
	}
}

void InstanceEndUI::starActionCallBack()
{
	GameSceneCamera::shakeScreen(0.25f, 3);
}

void InstanceEndUI::PresentBtnClose()
{
	btn_close->setVisible(true);
}

void InstanceEndUI::BeginToturial()
{
	if (GameView::getInstance()->myplayer->getActiveRole()->level()>15)
		return;

	if(FivePersonInstance::getStatus() != FivePersonInstance::status_in_instance)
	{
		return;
	}

	CFivePersonInstance * fivePersonInstance;
	bool isExist = false;
	for (int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();i++)
	{
		fivePersonInstance = FivePersonInstanceConfigData::s_fivePersonInstance.at(i);
		if (FivePersonInstance::getCurrentInstanceId() == fivePersonInstance->get_id())
		{
			isExist = true;
			break;
		}
	}

	if (!isExist)
		return;

	if (fivePersonInstance->get_open_level() > 15)
		return;

	//����ø����Ǽ�Ϊ�㣬�����ѧ��ָ������һ���ƽ��з��
// 	bool isNeedOpen = true;
// 	std::map<int, int>::iterator it = FivePersonInstance::getInstance()->s_m_copyStar.begin();
// 	for ( ; it != FivePersonInstance::getInstance()->s_m_copyStar.end(); ++ it )
// 	{
// 		if (it->first == FivePersonInstance::getInstance()->getCurrentInstanceId())
// 		{
// 			int star =  it->second;
// 			if (star > 0)
// 			{
// 				isNeedOpen = false;
// 			}
// 
// 			break;
// 		}
// 	}
// 	if (!isNeedOpen)
// 		return;
	
	addToturial();
}

void InstanceEndUI::addToturial()
{
	auto tutorialIndicator = CCTutorialIndicator::create(StringDataManager::getString("fivePerson_pleaseGetYourAwards"),Vec2(0,0),CCTutorialIndicator::Direction_LU);
	tutorialIndicator->setPosition(Vec2(165,250));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	tutorialIndicator->addHighLightFrameAction(83,114);
	tutorialIndicator->addCenterAnm(83,114);
	u_layer->addChild(tutorialIndicator);
}

void InstanceEndUI::removeToturial()
{
	if (u_layer->getChildByTag(CCTUTORIALINDICATORTAG))
	{
		u_layer->getChildByTag(CCTUTORIALINDICATORTAG)->removeFromParent();
	}
}

void InstanceEndUI::addLightAnimation()
{
	Size size = Director::getInstance()->getVisibleSize();
	std::string animFileName = "animation/texiao/renwutexiao/SHANGUANG/shanguang.anm";
	auto la_light = CCLegendAnimation::create(animFileName);
	if (la_light)
	{
		la_light->setPlayLoop(false);
		la_light->setReleaseWhenStop(true);
		la_light->setPlaySpeed(5.0f);
		la_light->setScale(0.85f);
		la_light->setPosition(Vec2(size.width/2,size.height/2));
		la_light->setTag(TAG_LIGHTANIMATION);
		GameView::getInstance()->getMainAnimationScene()->addChild(la_light);
	}
}
