#include "InstanceFunctionPanel.h"

#include "../../messageclient/GameMessageProcessor.h"
#include "../Mail_ui/MailFriend.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../Chat_ui/AddPrivateUi.h"
#include "../../utils/StaticDataManager.h"
#include "cocostudio\CCSGUIReader.h"

InstanceFunctionPanel::InstanceFunctionPanel()
{
}

InstanceFunctionPanel::~InstanceFunctionPanel()
{
}

InstanceFunctionPanel * InstanceFunctionPanel::create()
{
	auto _panel = new InstanceFunctionPanel();
	if (_panel && _panel->init())
	{
		_panel->autorelease();
		return _panel;
	}
	CC_SAFE_DELETE(_panel);
	return NULL;
}

bool InstanceFunctionPanel::init()
{
	if (UIScene::init())
	{
		Size visibleSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();

		auto mainPanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/instance_function_panel.json");
		mainPanel->setAnchorPoint(Vec2(0.f,0.f));
		mainPanel->setPosition(Vec2(0,0));
		m_pLayer->addChild(mainPanel);

		auto button= (Button *)Helper::seekWidgetByName(mainPanel,"Button_CallFriend");
		CCAssert(button != NULL, "should not be nil");
		button->setTouchEnabled(true);
		button->setPressedActionEnabled(true);
		button->addTouchEventListener(CC_CALLBACK_2(InstanceFunctionPanel::callFriendCallback, this));

		button= (Button *)Helper::seekWidgetByName(mainPanel,"Button_ExitInstance");
		CCAssert(button != NULL, "should not be nil");
		button->setTouchEnabled(true);
		button->setPressedActionEnabled(true);
		button->addTouchEventListener(CC_CALLBACK_2(InstanceFunctionPanel::exitInstanceCallback, this));

		this->setIgnoreAnchorPointForPosition(false);
		this->setAnchorPoint(Vec2(0,0));
		this->setPosition(Vec2(0,0));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(mainPanel->getContentSize());
		
		return true;
	}
	return false;
}

bool InstanceFunctionPanel::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	if(UIScene::onTouchBegan(pTouch, pEvent))
		return resignFirstResponder(pTouch,this,false);

	return false;
}

void InstanceFunctionPanel::onEnter()
{
	UIScene::onEnter();
}

void InstanceFunctionPanel::onExit()
{
	UIScene::onExit();
}

void InstanceFunctionPanel::callFriendCallback(Ref *pSender, Widget::TouchEventType type)
{	

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagPopFriendListUI) == NULL)
		{
			this->removeFromParent();
			Size winsize = Director::getInstance()->getVisibleSize();
			MailFriend * mailFriend = MailFriend::create(kTagInstanceDetailUI);
			mailFriend->setIgnoreAnchorPointForPosition(false);
			mailFriend->setAnchorPoint(Vec2(0.5f, 0.5f));
			mailFriend->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			GameView::getInstance()->getMainUIScene()->addChild(mailFriend, 5, kTagPopFriendListUI);
			AddPrivateUi::FriendList friend1 = { 0,20,0,1 };
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202, &friend1);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void InstanceFunctionPanel::exitInstanceCallback(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameView::getInstance()->showPopupWindow(StringDataManager::getString("fivePerson_areYouSureToExit"), 2, this, SEL_CallFuncO(&InstanceFunctionPanel::sureToExit), NULL, PopupWindow::KTypeRemoveByTime, 10);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void InstanceFunctionPanel::sureToExit( Ref *pSender )
{
	this->removeFromParent();
	// request to exit the instance
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1924);
}
