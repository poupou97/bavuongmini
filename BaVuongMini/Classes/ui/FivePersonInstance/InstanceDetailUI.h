#ifndef _UI_INSTANCE_INSTANCEDETAIL_H_
#define _UI_INSTANCE_INSTANCEDETAIL_H_

#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
class CFivePersonInstance;

class InstanceDetailUI : public UIScene
{
public:
	InstanceDetailUI();
	~InstanceDetailUI();

	static InstanceDetailUI *create(int instanceId);
	bool init(int instanceId);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void callBackExit(Ref *pSender, Widget::TouchEventType type);

	void signUpCallback(Ref *pSender, Widget::TouchEventType type);
	void cancelSignCallback(Ref *pSender, Widget::TouchEventType type);
	void enterInstanceCallback(Ref *pSender, Widget::TouchEventType type);
	void createTeamCallback(Ref* pSender);
	void btnRewardCallback(Ref *pSender, Widget::TouchEventType type);

	void RefreshInstanceInfo(int instanceId);
	void RefreshRewardsInfo();
	void TabIndexChangedEvent(Ref* pSender);

	bool isFinishAction();

	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();

private:
	//��ѧ
	int mTutorialScriptInstanceId;

public:
	Button * btnEnter;
	Button * btnSignUp;
	Button * btnCancelSign;
protected:
	Layer * layer;

	Text * l_instanceName ;
	Text * l_open_lv;
	Text * l_consume;
	Text * l_open_time;
	Text * l_difficult;
	Text * l_suggest;
	Text * l_combat;
	ui::TextField * t_instanceExplain;

	cocos2d::extension::ScrollView * m_scrollView_des;
	cocos2d::extension::ScrollView * m_scrollView_explain;
	Label * l_des_head;
	Label * l_des;
	Label * l_info_head;
	Label * l_info;

	int curInstanceId;
};

#endif