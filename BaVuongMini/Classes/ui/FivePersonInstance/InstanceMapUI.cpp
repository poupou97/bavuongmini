#include "InstanceMapUI.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "../../utils/StaticDataManager.h"
#include "../extensions/UITab.h"
#include "../../GameView.h"
#include "InstanceRewardUI.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "FivePersonInstance.h"
#include "../../utils/GameUtils.h"
#include "InstanceDetailUI.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "../../messageclient/element/CLable.h"
#include "../goldstore_ui/GoldStoreUI.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "cocostudio\CCSGUIReader.h"

bool sortByInt(int a,int b)
{
	return a<b;
}

InstanceMapUI::InstanceMapUI():
curInstanceId(-1),
curInstancePos(Vec2(0,0))
{
}

InstanceMapUI::~InstanceMapUI()
{
}

InstanceMapUI * InstanceMapUI::create()
{
	auto _ui=new InstanceMapUI();
	if (_ui && _ui->init())
	{
		_ui->autorelease();
		return _ui;
	}
	CC_SAFE_DELETE(_ui);
	return NULL;
}

bool InstanceMapUI::init()
{
	if (UIScene::init())
	{
		Size size=Director::getInstance()->getVisibleSize();

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(size);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		layer=Layer::create();
		layer->setIgnoreAnchorPointForPosition(false);
		layer->setAnchorPoint(Vec2(0,0));
		layer->setContentSize(size);
		layer->setPosition(Vec2(0,0));
		addChild(layer);

		auto mainPanel=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/fubenditu_1.json");
		mainPanel->setAnchorPoint(Vec2(0,0));
		mainPanel->setPosition(Vec2(0,0));
		m_pLayer->addChild(mainPanel);

		auto btn_Exit= (Button *)Helper::seekWidgetByName(mainPanel,"Button_close");
		btn_Exit->setTouchEnabled(true);
		btn_Exit->setPressedActionEnabled(true);
		btn_Exit->addTouchEventListener(CC_CALLBACK_2(InstanceMapUI::CloseEvent, this));
		btn_Exit->setPosition(Vec2(size.width-40,size.height-40));

		auto imageView_CloseFrame = (ImageView*)Helper::seekWidgetByName(mainPanel,"ImageView_358");
		imageView_CloseFrame->setPosition(Vec2(size.width-40,size.height-40));

		auto panel_phyInfo = (Layout*)Helper::seekWidgetByName(mainPanel,"Panel_phyInfo");
		panel_phyInfo->setAnchorPoint(Vec2(0,1.0f));
		panel_phyInfo->setPosition(Vec2(0,size.height));

		panel_tutorial =  (Layout*)Helper::seekWidgetByName(mainPanel,"Panel_tutorial");

		btn_addPhy = (Button*)Helper::seekWidgetByName(mainPanel,"Button_addPhy");
		btn_addPhy->setTouchEnabled(true);
		btn_addPhy->setPressedActionEnabled(true);
		btn_addPhy->addTouchEventListener(CC_CALLBACK_2(InstanceMapUI::AddPhyEvent, this));

		p_MyDragPanel = (ui::ScrollView*)Helper::seekWidgetByName(mainPanel,"DragPanel_22");
		p_MyDragPanel->setTouchEnabled(true);
		p_MyDragPanel->setContentSize(size);
		p_MyDragPanel->scrollToBottomRight(0.001f,false);
		//ScrollInstanceToCenter(11);
		//add releaseSelector for every btn
		for(int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
		{
			//Button
			std::string commonString = "Button_";
			commonString.append(FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_keyValue());
			if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
			{
				auto child = (Button *)p_MyDragPanel->getChildByName(commonString.c_str());
				if (child)
				{
					child->setTouchEnabled(true);
					child->setPressedActionEnabled(true);
					child->addTouchEventListener(CC_CALLBACK_2(InstanceMapUI::BtnInfoEvent, this));

					//tutorial
					if (i == 0)
					{
						btn_firstInstance = child;
					}
					else if (i == 1)
					{
						btn_secondInstance = child;
					}
					else if (i == 2)
					{
						btn_thirdInstance = child;
					}
					else if (i == 3)
					{
						btn_fouthInstance = child;
					}

					if (FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_open_level() > GameView::getInstance()->myplayer->getActiveRole()->level())
					{
						//child->setColor(Color3B(66,66,66));     //change to gray
						child->setVisible(false);
					}
					else
					{
						//child->setColor(Color3B(255,255,255));     //change to white
						child->setVisible(true);
					}
				}
			}

			//ImageView
			std::string commonString_imageView = "ImageView_";
			commonString_imageView.append(FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_keyValue());
			if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
			{	
				auto child = (ImageView *)p_MyDragPanel->getChildByName(commonString_imageView.c_str());
			
				if (child)
				{
					if (FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_open_level() > GameView::getInstance()->myplayer->getActiveRole()->level())
					{
						//child->setColor(Color3B(66,66,66));     //change to gray
						child->setVisible(false);
					}
					else
					{
						//child->setColor(Color3B(255,255,255));     //change to white
						child->setVisible(true);
					}
				}
			}

			//ImageView_luxian
			std::string commonString_imageView_luxian = "ImageView_luxian_";
			commonString_imageView_luxian.append(FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_keyValue());
			if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
			{	
				auto child = (ImageView *)p_MyDragPanel->getChildByName(commonString_imageView_luxian.c_str());

				if (child)
				{
					if (FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_open_level() > GameView::getInstance()->myplayer->getActiveRole()->level())
					{
						//child->setColor(Color3B(66,66,66));     //change to gray
						child->setVisible(false);
					}
					else
					{
						//child->setColor(Color3B(255,255,255));     //change to white
						child->setVisible(true);
					}
				}
			}
		}

		//ˢ���Ǽ���ʾ
		RefreshInstanceStar();

		//��ʾ��һ�������ͼ�꣨��ɫ��������Ӧ��
		std::vector<int> gapList;
		for(int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
		{
			int gap = FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_open_level() - GameView::getInstance()->myplayer->getActiveRole()->level();
			if (gap > 0)
			{
				gapList.push_back(gap);
			}
		}
		sort(gapList.begin(),gapList.end(),sortByInt);
		int gap = -1;
		if (gapList.size()>0)
		{
			gap = gapList.at(0);
		}
		if (gap != -1)
		{
			for(int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
			{
				//Button
				std::string commonString = "Button_";
				commonString.append(FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_keyValue());
				if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
				{
					auto child = (Button *)p_MyDragPanel->getChildByName(commonString.c_str());
					if (child)
					{
						if (FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_open_level() - GameView::getInstance()->myplayer->getActiveRole()->level() == gap)
						{
							child->setTouchEnabled(false);
							child->setColor(Color3B(128,128,128));     //change to gray
							child->setVisible(true);
						}
						else
						{
							child->setTouchEnabled(true);
							child->setColor(Color3B(255,255,255));     //change to white
						}
					}
				}

				//ImageView
				std::string commonString_imageView_luxian = "ImageView_luxian_";
				commonString_imageView_luxian.append(FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_keyValue());
				if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
				{	
					auto child = (ImageView *)p_MyDragPanel->getChildByName(commonString_imageView_luxian.c_str());

					if (child)
					{
						if (FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_open_level() - GameView::getInstance()->myplayer->getActiveRole()->level() == gap)
						{
							//child->setColor(Color3B(66,66,66));     //change to gray
							child->setVisible(false);
						}
					}
				}
			}
		}

// 		ImageView_select = ImageView::create();
// 		ImageView_select->loadTexture("res_ui/fubenditu/qizi.png");
// 		ImageView_select->setAnchorPoint(Vec2(.5f,.5f));
// 		panel_tutorial->addChild(ImageView_select);
// 		ImageView_select->setVisible(false);

		// ��ʼ�� ��������ֵ��
		m_pImageView_phy = (ImageView *)Helper::seekWidgetByName(mainPanel,"ImageView_phy");
		m_pLabelBMF_phyValue = (Text*)Helper::seekWidgetByName(mainPanel, "Label_phyValue");

		int curPhysicalValue = GameView::getInstance()->myplayer->getPhysicalValue();
		int allPhysicalValue = GameView::getInstance()->myplayer->getPhysicalCapacity();

		m_pImageView_phy->setTextureRect(Rect(0, 0, 
			m_pImageView_phy->getContentSize().width * 0.0f,
			m_pImageView_phy->getContentSize().height));

		auto pPhysicalBarEffect = PhysicalBarEffect::create(curPhysicalValue, allPhysicalValue, 1.5f,
			m_pImageView_phy, m_pLabelBMF_phyValue);
		this->addChild(pPhysicalBarEffect);

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(size);
		return true;
	}
	return false;
}

void InstanceMapUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void InstanceMapUI::onExit()
{
	UIScene::onExit();
}

void InstanceMapUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		InstanceDetailUI * instanceDetailUI = (InstanceDetailUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceDetailUI);
		if (instanceDetailUI)
			instanceDetailUI->closeAnim();

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void InstanceMapUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void InstanceMapUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void InstanceMapUI::BtnInfoEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto selectInstance = (Button*)pSender;
		std::string buttonName = selectInstance->getName();
		std::string keyValue = buttonName.substr(7);
		int instanceId = -1;
		for (int i = 0; i<FivePersonInstanceConfigData::s_fivePersonInstance.size(); i++)
		{
			if (keyValue == FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_keyValue())
			{
				instanceId = FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_id();

				Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
				if (sc != NULL)
					sc->endCommand(pSender);
			}
		}

		//����ѡ��״̬��ͼƬλ�
		// 	ImageView_select->setVisible(true);
		// 	ImageView_select->setPosition(Vec2(selectInstance->getPosition().x,selectInstance->getPosition().y+20));

		OpenDetailUI(instanceId);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void InstanceMapUI::OpenDetailUI( int instanceId )
{
	if (instanceId > 0)
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		auto mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
		CCAssert(mainLayer != NULL, "should not be nil");

		auto _ui = (InstanceDetailUI*)mainLayer->getChildByTag(kTagInstanceDetailUI);
		if(_ui == NULL)
		{
			auto _ui=InstanceDetailUI::create(instanceId);
			_ui->setIgnoreAnchorPointForPosition(false);
			_ui->setAnchorPoint(Vec2(.5f,.5f));  //(1.0f,0.5f)
			_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));    //(winSize.width+_ui->getContentSize().width,winSize.height/2)
			_ui->setTag(kTagInstanceDetailUI);
			mainLayer->addChild(_ui);

// 			MoveTo * moveTo = MoveTo::create(.2f,Vec2(winSize.width,winSize.height/2));
// 			CCEaseBackOut * easebackOut = CCEaseBackOut::create(moveTo);
// 			_ui->runAction(easebackOut);
		}
		else
		{
			_ui->RefreshInstanceInfo(instanceId);
		}
	}
}

void InstanceMapUI::AddPhyEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		auto goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
		if (goldStoreUI == NULL)
		{
			goldStoreUI = GoldStoreUI::create();
			GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI, 0, kTagGoldStoreUI);
			goldStoreUI->setIgnoreAnchorPointForPosition(false);
			goldStoreUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			goldStoreUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));

			for (int i = 0; i <GameView::getInstance()->goldStoreList.size(); i++)
			{
				if (GameView::getInstance()->goldStoreList.at(i)->id() == GoldStoreUI::type_phypower)
				{
					goldStoreUI->getTab_function()->setDefaultPanelByIndex(i);
					goldStoreUI->IndexChangedEvent(goldStoreUI->getTab_function());
				}
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void InstanceMapUI::RefreshInstanceStar()
{
	for(int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
	{
		auto tempData = FivePersonInstanceConfigData::s_fivePersonInstance.at(i);
		std::string commonString = "Button_";
		commonString.append(tempData->get_keyValue());
		if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
		{	
			auto child = (Button *)p_MyDragPanel->getChildByName(commonString.c_str());
			if (!child || child->isVisible() == false)
				continue;
			
			int star = 0;
			std::map<int, int>::iterator it = FivePersonInstance::getInstance()->s_m_copyStar.begin();
			for ( ; it != FivePersonInstance::getInstance()->s_m_copyStar.end(); ++ it )
			{
				if (it->first == tempData->get_id())
				{
					star = it->second;
					break;
				}
			}

			Vec2 firstPos = child->getPosition();

			std::string panel_star_name = "panel_star_";
			panel_star_name.append(tempData->get_keyValue());

			auto panel_star = (Layout*)p_MyDragPanel->getChildByName(panel_star_name.c_str());
			if (!panel_star)
			{
				panel_star = Layout::create();
				p_MyDragPanel->addChild(panel_star);
				panel_star->setLocalZOrder(2);
				panel_star->setName("panel_star_name");
			}

			//star
			for (int i = 0;i<5;i++)
			{
				auto pIcon = ImageView::create();
			 	if (i<star)
			 	{
			 		pIcon->loadTexture("res_ui/star_on.png");
			 	}
			 	else
			 	{
			 		pIcon->loadTexture("res_ui/star_off.png");
			 	}
			 	pIcon->setScale(.8f);
			 	pIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
			 	pIcon->setPosition(Vec2(firstPos.x - 40 +20*i, firstPos.y+50));
			 	panel_star->addChild(pIcon);
			}
		}
	}
}


void InstanceMapUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void InstanceMapUI::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	auto tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,40,60);
	tutorialIndicator->setPosition(pos);
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	tutorialIndicator->addHighLightFrameAction(80,80);
	tutorialIndicator->addCenterAnm(80,80);
	panel_tutorial->addChild(tutorialIndicator);

// 	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,80,80,true);
// 	tutorialIndicator->setDrawNodePos(Vec2(pos.x-402,pos.y-5));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
// 	if (mainScene)
// 	{
// 		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
// 	}
}

void InstanceMapUI::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(panel_tutorial->getVirtualRenderer()->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

// 	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
// 	if (mainScene)
// 	{
// 		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
// 		if(teachingGuide != NULL)
// 			teachingGuide->removeFromParent();
// 	}
}

void InstanceMapUI::ScrollInstanceToCenter( int instanceId )
{
	for(int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
	{
		auto tempData = FivePersonInstanceConfigData::s_fivePersonInstance.at(i);
		if (tempData->get_id() == instanceId)
		{
			std::string commonString = "Button_";
			commonString.append(tempData->get_keyValue());
			if (p_MyDragPanel->getChildByName(commonString.c_str()) != NULL)
			{	
				auto child = (Button *)p_MyDragPanel->getChildByName(commonString.c_str());
				if (!child || child->isVisible() == false)
					continue;

				curInstancePos = child->getPosition();
				auto action = Sequence::create(
					DelayTime::create(0.05f),
					CallFunc::create(CC_CALLBACK_0(InstanceMapUI::callBackScroll,this)),
					NULL);
				this->runAction(action);
				break;
			}
		}
	}
}

void InstanceMapUI::callBackScroll()
{
	int x = MAX(curInstancePos.x - p_MyDragPanel->getContentSize().width/2, 0);
	x = MIN(x, p_MyDragPanel->getInnerContainerSize().width - p_MyDragPanel->getContentSize().width);
	int y = p_MyDragPanel->getInnerContainerSize().height - curInstancePos.y - p_MyDragPanel->getContentSize().height/2;
	y = MIN(y, p_MyDragPanel->getInnerContainerSize().height - p_MyDragPanel->getContentSize().height);
	y = MAX(y, 0);
	float fWidth = (float)(x)/(p_MyDragPanel->getInnerContainerSize().width - p_MyDragPanel->getContentSize().width);
	float fHeight = (float)(y)/(p_MyDragPanel->getInnerContainerSize().height - p_MyDragPanel->getContentSize().height);
	p_MyDragPanel->jumpToPercentBothDirection(Vec2(fWidth*100,fHeight*100));
}

