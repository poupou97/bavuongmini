#ifndef _UI_VIPUI_VIPDATA_H_
#define _UI_VIPUI_VIPDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "VipDetailUI.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class VipDetailUI;

class VipData
{
public:
	VipData(void);
	~VipData(void);

public: 
	static VipData * s_vipData;
	static VipData * getInstance();

	static VipDetailUI * vipDetailUI;
};

#endif
