#ifndef _VIPUI_FIRSTBUYVIPUI_H_
#define _VIPUI_FIRSTBUYVIPUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CRewardProp;

class FirstBuyVipUI :public UIScene
{
public:
	FirstBuyVipUI();
	~FirstBuyVipUI();

	static FirstBuyVipUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	void VipBuyEvent(Ref *pSender, Widget::TouchEventType type);

	void RefreshGoodsRewards( std::vector<CRewardProp*> list );
private:
	Layer *u_layer;
};
#endif