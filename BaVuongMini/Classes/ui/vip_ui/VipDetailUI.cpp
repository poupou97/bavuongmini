#include "VipDetailUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/CRewardProp.h"
#include "../backpackscene/PackageItem.h"
#include "../../AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "VipRewardCellItem.h"
#include "../../utils/GameUtils.h"
#include "../../utils/StrUtils.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"

#define  M_SCROLLVIEWTAG 213
#define  SelectImageTag 458

VipDetailUI::VipDetailUI():
m_nCurSelectVipLevel(0)
{
}


VipDetailUI::~VipDetailUI()
{
}


VipDetailUI* VipDetailUI::create()
{
	auto vipDetailUI = new VipDetailUI();
	if (vipDetailUI && vipDetailUI->init())
	{
		vipDetailUI->autorelease();
		return vipDetailUI;
	}
	CC_SAFE_DELETE(vipDetailUI);
	return NULL;
}

bool VipDetailUI::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		//���UI
		if(LoadSceneLayer::vipDetatilPanel->getParent() != NULL)
		{
			LoadSceneLayer::vipDetatilPanel->removeFromParentAndCleanup(false);
		}
		auto panel_vip = LoadSceneLayer::vipDetatilPanel;
		panel_vip->setAnchorPoint(Vec2(0.0f,0.0f));
		panel_vip->setPosition(Vec2::ZERO);
		panel_vip->setTouchEnabled(true);
		m_pLayer->addChild(panel_vip);

		u_layer = Layer::create();
		addChild(u_layer);

		auto Btn_close = (Button*)Helper::seekWidgetByName(panel_vip,"Button_close");
		Btn_close->setTouchEnabled(true);
		Btn_close->setPressedActionEnabled(true);
		Btn_close->addTouchEventListener(CC_CALLBACK_2(VipDetailUI::CloseEvent, this));

		auto Btn_buyVip = (Button*)Helper::seekWidgetByName(panel_vip,"Button_buy");
		Btn_buyVip->setTouchEnabled(true);
		Btn_buyVip->setPressedActionEnabled(true);
		Btn_buyVip->addTouchEventListener(CC_CALLBACK_2(VipDetailUI::VipBuyEvent, this));

		lbt_vipLevel = (TextBMFont *)Helper::seekWidgetByName(panel_vip,"LabelBMFont_vipLevel");
		l_goldInGotValue = (Text *)Helper::seekWidgetByName(panel_vip,"Label_goldInGotValue");

		m_tableView = TableView::create(this,Size(525,96));
		m_tableView->setTouchEnabled(true);
		//m_tableView->setSelectedScale9Texture("res_ui/highlight.png", Rect(26, 26, 1, 1), Vec2(0,0));
		m_tableView->setDirection(TableView::Direction::HORIZONTAL);
		m_tableView->setAnchorPoint(Vec2(0,0));
		m_tableView->setPosition(Vec2(57,303));
		m_tableView->setContentOffset(Vec2(0,0));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//m_tableView->setPressedActionEnabled(true);
		u_layer->addChild(m_tableView);

		auto u_tableViewFrameLayer = Layer::create();
		this->addChild(u_tableViewFrameLayer);
		auto tableView_kuang = ImageView::create();
		tableView_kuang->loadTexture("res_ui/LV5_dikuang1_miaobian2.png");
		tableView_kuang->setScale9Enabled(true);
		tableView_kuang->setContentSize(Size(545,116));
		tableView_kuang->setCapInsets(Rect(30,30,1,1));
		tableView_kuang->setAnchorPoint(Vec2(0,0));
		tableView_kuang->setPosition(Vec2(45,294));
		u_tableViewFrameLayer->addChild(tableView_kuang);

		auto image_titleFrame = ImageView::create();
		image_titleFrame->loadTexture("res_ui/caidai_1.png");
		image_titleFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		image_titleFrame->setPosition(Vec2(320,436));
		image_titleFrame->setScale9Enabled(true);
		image_titleFrame->setContentSize(Size(451,62));
		image_titleFrame->setCapInsets(Rect(178,62,1,1));
		u_tableViewFrameLayer->addChild(image_titleFrame);

		auto image_title = ImageView::create();
		image_title->loadTexture("res_ui/vip/vip_1.png");
		image_title->setAnchorPoint(Vec2(0.5f,0.5f));
		image_title->setPosition(Vec2(319,441));
		u_tableViewFrameLayer->addChild(image_title);

		//set default data
		m_nCurSelectVipLevel = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5202,(void*)m_nCurSelectVipLevel);

		int scroll_height = 0;
		//
		//ؽ��
		//Label * l_des = Label::createWithTTF(des.c_str(),APP_FONT_NAME,18,Size(265,0),TextHAlignment::LEFT,TextVAlignment::TOP);
		//l_des = CCRichLabel::createWithString("",Size(263, 0),this,NULL,0,16,5);

		m_scrollView = cocos2d::extension::ScrollView::create(Size(265,205));
		m_scrollView->setViewSize(Size(265, 205));
		m_scrollView->setIgnoreAnchorPointForPosition(false);
		m_scrollView->setTouchEnabled(true);
		m_scrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		m_scrollView->setAnchorPoint(Vec2(0,0));
		m_scrollView->setPosition(Vec2(75,43));
		m_scrollView->setBounceable(true);
		m_scrollView->setClippingToBounds(true);
		m_scrollView->setTag(M_SCROLLVIEWTAG);
		u_layer->addChild(m_scrollView);

		int zeroLineHeight;
		std::map<int,std::string>::iterator it = VipDescriptionConfig::s_vipDes.begin();
		for ( ; it != VipDescriptionConfig::s_vipDes.end(); ++ it )
		{
			auto l_vipDes = CCRichLabel::createWithString(it->second.c_str(),Size(263, 0),this,NULL,0,16,5);
			l_vipDes->setTag(it->first);
			m_scrollView->addChild(l_vipDes);

			if (it->first == 1)
			{
				zeroLineHeight = l_vipDes->getContentSize().height+10;
			}
		}

		scroll_height = zeroLineHeight;
		if (scroll_height > 190)
		{
			m_scrollView->setContentSize(Size(265,scroll_height));
			m_scrollView->setContentOffset(Vec2(0,205-scroll_height));  
		}
		else
		{
			m_scrollView->setContentSize(Size(265,205));
		}
		m_scrollView->setClippingToBounds(true);

		std::map<int,std::string>::iterator itr = VipDescriptionConfig::s_vipDes.begin();
		for ( ; itr != VipDescriptionConfig::s_vipDes.end(); ++ itr )
		{
			auto l_vipDes = dynamic_cast<CCRichLabel*>(m_scrollView->getContainer()->getChildByTag(itr->first));
			if (l_vipDes)
			{
				l_vipDes->setPosition(Vec2(2,m_scrollView->getContentSize().height - zeroLineHeight));
				if (itr->first  != 1)
				{
					l_vipDes->setVisible(false);
				}
			}
		}

		//m_scrollView->addChild(l_des);
		//l_des->setPosition(Vec2(2,m_scrollView->getContentSize().height - zeroLineHeight));

		RefreshDescriptionByLevel(m_nCurSelectVipLevel);
		refreshInGotValue();

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(panel_vip->getContentSize());

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(VipDetailUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(VipDetailUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(VipDetailUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(VipDetailUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void VipDetailUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void VipDetailUI::onExit()
{
	UIScene::onExit();
}

bool VipDetailUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	return true;
}

void VipDetailUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void VipDetailUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void VipDetailUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}


void VipDetailUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void VipDetailUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void VipDetailUI::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void VipDetailUI::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void VipDetailUI::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void VipDetailUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	m_nCurSelectVipLevel = cell->getIdx()+1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5202,(void*)m_nCurSelectVipLevel);
	RefreshDescriptionByLevel(m_nCurSelectVipLevel);
}

cocos2d::Size VipDetailUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(100,89);
}

cocos2d::extension::TableViewCell* VipDetailUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell=new TableViewCell();
	cell->autorelease();

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	auto sprite_cion = Sprite::create("res_ui/vip/vip_b.png");
	sprite_cion->setAnchorPoint(Vec2(0.5f,0.5f));
	sprite_cion->setPosition(Vec2(50,56));
	cell->addChild(sprite_cion);
	 
	char str_level[20];
	sprintf(str_level,"%d",idx+1);
	auto l_vipLevel = Label::createWithBMFont("res_ui/font/ziti_3.fnt", str_level);
	l_vipLevel->setScale(0.9f);
	l_vipLevel->setAnchorPoint(Vec2(0.5f,0.5f));
	l_vipLevel->setPosition(Vec2(43,29));
	sprite_cion->addChild(l_vipLevel);

	int m_price_value = 0;
	int m_price_type = 0;
	std::map<int, VipPriceConfig::priceValueAndType*>::const_iterator cIter;
	cIter = VipPriceConfig::s_vipPriceValue.find(idx+1);
	if (cIter == VipPriceConfig::s_vipPriceValue.end()) // �û�ҵ�����ָ�END��  
	{
	}
	else
	{
		m_price_value = VipPriceConfig::s_vipPriceValue[idx+1]->priceValue;
		m_price_type = VipPriceConfig::s_vipPriceValue[idx+1]->priceType;
	}

	Sprite * sprite_money;

	char str_moneyValue[20];
	if (m_price_type == 1) //�Ԫ��
	{
		sprite_money = Sprite::create("res_ui/ingot.png");
		sprintf(str_moneyValue,"%d",m_price_value);
	}
	else
	{
		sprite_money = Sprite::create("res_ui/vip/qian.png");
		sprintf(str_moneyValue,"%d",m_price_value/100);
	}

	sprite_money->setAnchorPoint(Vec2(0.5f,0.5f));
	sprite_money->setPosition(Vec2(36,14));
	cell->addChild(sprite_money);
	
	auto l_moneyValue = Label::createWithBMFont("res_ui/font/ziti_1.fnt", str_moneyValue);
	l_moneyValue->setAnchorPoint(Vec2(0.5f,0.5f));
	l_moneyValue->setScale(0.7f);
	l_moneyValue->setPosition(Vec2(55+sprite_money->getContentSize().width*l_moneyValue->getScale()/2,14));
	cell->addChild(l_moneyValue);

	sprite_money->setPosition(Vec2(55 - l_moneyValue->getContentSize().width*l_moneyValue->getScale()/2 - 4,14));

	return cell;
}

ssize_t VipDetailUI::numberOfCellsInTableView( TableView *table )
{
	return VipDescriptionConfig::s_vipDes.size();
}

void VipDetailUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void VipDetailUI::RefreshGoodsRewards( std::vector<CRewardProp*> list )
{
	//delete old
	for(int i = 0;i<4;i++)
	{
		if (m_pLayer->getChildByTag(200+i))
		{
			m_pLayer->getChildByTag(200+i)->removeFromParent();
		}
	}
	//add new
	for(int i =0;i<list.size();++i)
	{
		if (i>3)
			continue;

		auto temp = new GoodsInfo();
		temp->CopyFrom(list.at(i)->goods());
		auto goodsItem = VipRewardCellItem::create(temp,list.at(i)->number());
		goodsItem->setIgnoreAnchorPointForPosition(false);
		goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		goodsItem->setPosition(Vec2(0,0));
		goodsItem->setTag(200+i);
		goodsItem->setScale(0.9f);
		m_pLayer->addChild(goodsItem);

		switch(i)
		{
		case 0:
			{
				goodsItem->setPosition(Vec2(444,220));
			}
			break;
		case 1:
			{
				goodsItem->setPosition(Vec2(523,220));
			}
			break;
		case 2:
			{
				goodsItem->setPosition(Vec2(444,160));
			}
			break;
		case 3:
			{
				goodsItem->setPosition(Vec2(523,160));
			}
			break;
		}
	}
	
}

void VipDetailUI::RefreshDescriptionByLevel( int vipLevel )
{
	char str_lev[20];
	sprintf(str_lev,"%d",vipLevel);
	lbt_vipLevel->setString(str_lev);
// 	long long startTime = GameUtils::millisecondNow();
// 	std::string des = "";
// 	std::map<int,std::string>::const_iterator cIter;
// 	cIter = VipDescriptionConfig::s_vipDes.find(vipLevel);
// 	if (cIter == VipDescriptionConfig::s_vipDes.end()) // û�ҵ�����ָ�END��  
// 	{
// 		return ;
// 	}
// 	else
// 	{
// 		 des.append(VipDescriptionConfig::s_vipDes[vipLevel]);
// 	}
// 
// 	long long endTime = GameUtils::millisecondNow();
// 	CCLOG(" find time = %ld ",endTime - startTime);
// 
// 	startTime = GameUtils::millisecondNow();
// 	//l_des->setString(des.c_str());
// 	l_des->setString(StringDataManager::getString("generals_teach_willOpen_1"));
// 	endTime = GameUtils::millisecondNow();
// 	CCLOG(" setString time = %ld ",endTime - startTime);
// 
// 	int zeroLineHeight = l_des->getContentSize().height+10;
// 	int scroll_height = zeroLineHeight;
// 
// 	startTime = GameUtils::millisecondNow();
// 	if (scroll_height > 190)
// 	{
// 		m_scrollView->setContentSize(Size(265,scroll_height));
// 		m_scrollView->setContentOffset(Vec2(0,205-scroll_height));  
// 	}
// 	else
// 	{
// 		m_scrollView->setContentSize(Vec2(265,205));
// 	}
// 	l_des->setPosition(Vec2(2,m_scrollView->getContentSize().height - zeroLineHeight));
// 	endTime = GameUtils::millisecondNow();
// 	CCLOG(" setPostion time = %ld ",endTime - startTime);
// 	
	std::map<int,std::string>::iterator it = VipDescriptionConfig::s_vipDes.begin();
	for ( ; it != VipDescriptionConfig::s_vipDes.end(); ++ it )
	{
		auto l_vipDes = dynamic_cast<CCRichLabel*>(m_scrollView->getContainer()->getChildByTag(it->first));
		if (l_vipDes)
		{
			if (it->first == vipLevel)
			{
				int zeroLineHeight = l_vipDes->getContentSize().height+10;
				int scroll_height = zeroLineHeight;

				if (scroll_height > 190)
				{
					m_scrollView->setContentSize(Size(265,scroll_height));
					m_scrollView->setContentOffset(Vec2(0,205-scroll_height));  
				}
				else
				{
					m_scrollView->setContentSize(Size(265,205));
				}
				l_vipDes->setPosition(Vec2(2,m_scrollView->getContentSize().height - zeroLineHeight));
				l_vipDes->setVisible(true);
			}
			else
			{
				l_vipDes->setVisible(false);
			}
		}
	}
}

void VipDetailUI::VipBuyEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() > 0)
		{
			//cur vip
			std::string str_curVip = "VIP";
			char s_curVip[10];
			sprintf(s_curVip, "%d", GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel());
			str_curVip.append(s_curVip);
			//next vip
			std::string str_nextVip = "VIP";
			char s_nextVip[10];
			sprintf(s_nextVip, "%d", m_nCurSelectVipLevel);
			str_nextVip.append(s_nextVip);

			std::string str_des;
			str_des.append(StringDataManager::getString("vip_detail_des_1"));
			//str_des.append(StrUtils::applyColor(str_curVip.c_str(),Color3B(255,51,51)));
			str_des.append(str_curVip.c_str());
			str_des.append(StringDataManager::getString("vip_detail_des_2"));
			//str_des.append(StrUtils::applyColor(str_nextVip.c_str(),Color3B(255,51,51)));
			str_des.append(str_nextVip.c_str());
			str_des.append(StringDataManager::getString("vip_detail_des_3"));
			//str_des.append(StrUtils::applyColor(str_nextVip.c_str(),Color3B(255,51,51)));
			str_des.append(str_nextVip.c_str());
			str_des.append(StringDataManager::getString("vip_detail_des_4"));
			//str_des.append(StrUtils::applyColor(str_curVip.c_str(),Color3B(255,51,51)));
			str_des.append(str_curVip.c_str());
			str_des.append(StringDataManager::getString("vip_detail_des_5"));

			GameView::getInstance()->showPopupWindow(str_des.c_str(), 2, this, SEL_CallFuncO(&VipDetailUI::SureToBuyVip), NULL);
		}
		else
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5201, (void*)m_nCurSelectVipLevel);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void VipDetailUI::setToDefault()
{
	m_nCurSelectVipLevel = 1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5202,(void*)m_nCurSelectVipLevel);
	RefreshDescriptionByLevel(m_nCurSelectVipLevel);
	m_tableView->cellAtIndex(0);
}

void VipDetailUI::SureToBuyVip( Ref *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5201,(void*)m_nCurSelectVipLevel);
}

void VipDetailUI::refreshInGotValue()
{
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	l_goldInGotValue->setString(str_ingot);
}
