#include "FirstBuyVipUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../backpackscene/PackageItem.h"
#include "../../AppMacros.h"
#include "../../messageclient/element/CRewardProp.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "VipDetailUI.h"
#include "VipRewardCellItem.h"
#include "../../utils/StaticDataManager.h"
#include "../recharge_ui/Recharge.h"
#include "../../utils/GameConfig.h"
#include "cocostudio\CCSGUIReader.h"

FirstBuyVipUI::FirstBuyVipUI()
{
}


FirstBuyVipUI::~FirstBuyVipUI()
{
}

FirstBuyVipUI* FirstBuyVipUI::create()
{
	auto firstBuyVipUI = new FirstBuyVipUI();
	if (firstBuyVipUI && firstBuyVipUI->init())
	{
		firstBuyVipUI->autorelease();
		return firstBuyVipUI;
	}
	CC_SAFE_DELETE(firstBuyVipUI);
	return NULL;
}

bool FirstBuyVipUI::init()
{
	if (UIScene::init())
	{
		auto winSize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		//���UI
		auto panel_vip =  (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/firstbuy_1.json");
		panel_vip->setAnchorPoint(Vec2(0.0f,0.0f));
		panel_vip->setPosition(Vec2::ZERO);
		panel_vip->setTouchEnabled(true);
		m_pLayer->addChild(panel_vip);

		u_layer = Layer::create();
		addChild(u_layer);

		auto Btn_close = (Button*)Helper::seekWidgetByName(panel_vip,"Button_close");
		Btn_close->setTouchEnabled(true);
		Btn_close->setPressedActionEnabled(true);
		Btn_close->addTouchEventListener(CC_CALLBACK_2(FirstBuyVipUI::CloseEvent, this));

		auto Btn_buyVip = (Button*)Helper::seekWidgetByName(panel_vip,"Button_buy");
		Btn_buyVip->setTouchEnabled(true);
		Btn_buyVip->setPressedActionEnabled(true);
		Btn_buyVip->addTouchEventListener(CC_CALLBACK_2(FirstBuyVipUI::VipBuyEvent, this));

		auto lbf_value = (Text*)Helper::seekWidgetByName(panel_vip,"Label_aValue");
		std::map<int,std::string>::const_iterator cIter;
		cIter = VipValueConfig::s_vipValue.find(1);
		if (cIter == VipValueConfig::s_vipValue.end()) // �û�ҵ�����ָ�END��  
		{
			
		}
		else
		{
			std::string value = VipValueConfig::s_vipValue[1];
			int a = atoi(value.c_str());
			char str_value[200];
			sprintf(str_value,"%d",a*100);
			lbf_value->setString(str_value);
		}


		//set default data
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5202,(void*)0);

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(panel_vip->getContentSize());

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(FirstBuyVipUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(FirstBuyVipUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(FirstBuyVipUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(FirstBuyVipUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void FirstBuyVipUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FirstBuyVipUI::onExit()
{
	UIScene::onExit();
}

bool FirstBuyVipUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	return true;
}

void FirstBuyVipUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void FirstBuyVipUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void FirstBuyVipUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void FirstBuyVipUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void FirstBuyVipUI::RefreshGoodsRewards( std::vector<CRewardProp*> list )
{
	//delete old
	for(int i = 0;i<4;i++)
	{
// 		std::string str_name = "rewardItem_";
// 		char s_index [5];
// 		sprintf(s_index,"%d",i);
// 		str_name.append(s_index);
// 		if (m_pLayer->getChildByName(str_name.c_str()))
// 		{
// 			m_pLayer->getChildByName(str_name.c_str())->removeFromParent();
// 		}

		if (m_pLayer->getChildByTag(200+i))
		{
			m_pLayer->getChildByTag(200+i)->removeFromParent();
		}
	}
	//add new
	for(int i =0;i<list.size();++i)
	{
		if (i>3)
			continue;

		auto temp = new GoodsInfo();
		temp->CopyFrom(list.at(i)->goods());
		auto goodsItem = VipRewardCellItem::create(temp,list.at(i)->number());
		goodsItem->setIgnoreAnchorPointForPosition(false);
		goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		goodsItem->setPosition(Vec2(320+69*i,145));
		goodsItem->setTag(200+i);
		m_pLayer->addChild(goodsItem);
// 		GoodsInfo * temp = new GoodsInfo();
// 		temp->CopyFrom(list.at(i)->goods());
// 		PackageItem * packageItem = PackageItem::create(temp);
// 		packageItem->setAnchorPoint(Vec2(0,0));
//		packageItem->setPosition(Vec2(268+69*i,115));
//		m_pLayer->addChild(packageItem);
// 		std::string str_name = "rewardItem_";
// 		char s_index [5];
// 		sprintf(s_index,"%d",i);
// 		str_name.append(s_index);
// 		packageItem->setName(str_name.c_str());
// 
// 		char s_num[20];
// 		sprintf(s_num,"%d",list.at(i)->number());
// 		Label *Lable_num = Label::create();
// 		Lable_num->setText(s_num);
// 		Lable_num->setFontName(APP_FONT_NAME);
// 		Lable_num->setFontSize(13);
// 		Lable_num->setAnchorPoint(Vec2(1.0f,0));
// 		Lable_num->setPosition(Vec2(75,19));
// 		Lable_num->setName("Label_num");
// 		packageItem->addChild(Lable_num);
	}
}

void FirstBuyVipUI::VipBuyEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		// ˳�ֵ���ƿ��
		bool bChargeEnabled = GameConfig::getBoolForKey("charge_enable");
		if (true == bChargeEnabled)
		{
			auto winSize = Director::getInstance()->getVisibleSize();
			auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
			auto rechargeScene = (Layer*)mainscene->getChildByTag(kTagRechargeUI);
			if (rechargeScene == NULL)
			{
				auto rechargeUI = RechargeUI::create();
				mainscene->addChild(rechargeUI, 0, kTagRechargeUI);
				rechargeUI->setIgnoreAnchorPointForPosition(false);
				rechargeUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				rechargeUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			}
			//GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillui_nothavefuction"));

			this->closeAnim();
		}
		else
		{
			const char * charChargeEnabled = StringDataManager::getString("RechargeUI_disable");
			GameView::getInstance()->showAlertDialog(charChargeEnabled);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}
