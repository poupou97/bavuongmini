
#ifndef  _VIPUI_VIPEVERYDATREWARDSUI_H_
#define _VIPUI_VIPEVERYDATREWARDSUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class VipEveryDayRewardsUI : public UIScene, public TableViewDataSource, public TableViewDelegate
{
private:
	Layer * u_layer;
	TableView * m_tableView;
public:
	VipEveryDayRewardsUI();
	~VipEveryDayRewardsUI();

	static VipEveryDayRewardsUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	void refreshTableViewWithOutChangeOffSet();
 	void VipBuyEvent(Ref *pSender, Widget::TouchEventType type);
// 	void Vip1GetEvent(Ref* pSender);
// 	void Vip2GetEvent(Ref* pSender);
// 	void Vip3GetEvent(Ref* pSender);
// 	void Vip4GetEvent(Ref* pSender);
// 	void Vip5GetEvent(Ref* pSender);

//	void RefreshRewardItem();
//	void RefreshStatus(int vipLevel,int status);

private:
// 	Button *Btn_get_1;
// 	Button *Btn_get_2;
// 	Button *Btn_get_3;
// 	Button *Btn_get_4;
// 	Button *Btn_get_5;
// 
// 	ImageView * imageView_canNotGet_1;
// 	ImageView * imageView_canNotGet_2;
// 	ImageView * imageView_canNotGet_3;
// 	ImageView * imageView_canNotGet_4;
// 	ImageView * imageView_canNotGet_5;
// 
// 	Label * l_content_1;
// 	Label * l_content_2;
// 	Label * l_content_3;
// 	Label * l_content_4;
// 	Label * l_content_5;
};



/////////////////////////////////
/**
 * vip�ÿ����ȡ�µcell
 * @author yangjun
 * @version 0.1.0
 * @date 2014.5.6
 */

class COneVipGift;

class VipEveryDayRewardCell : public UIScene
{
public:
	VipEveryDayRewardCell();
	~VipEveryDayRewardCell();
	static VipEveryDayRewardCell* create(COneVipGift * oneVipGift);
	bool init(COneVipGift * oneVipGift);

	void getRewardEvent(Ref *pSender, Widget::TouchEventType type);

public:
	COneVipGift * curOneVipGift;
};



#endif

