#ifndef  _VIPUI_VIPUI_H_
#define _VIPUI_VIPUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CRewardProp;
class CCRichLabel;

class VipDetailUI : public UIScene, public TableViewDataSource, public TableViewDelegate
{
private:
	Layer * u_layer;
	TableView* m_tableView;
	TextBMFont * lbt_vipLevel;
	int m_nCurSelectVipLevel;

	CCRichLabel *l_des;
	cocos2d::extension::ScrollView * m_scrollView;
	Text * l_goldInGotValue;
public:
	VipDetailUI();
	~VipDetailUI();

	static VipDetailUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	void VipBuyEvent(Ref *pSender, Widget::TouchEventType type);

	void RefreshGoodsRewards(std::vector<CRewardProp*> list);
	void RefreshDescriptionByLevel(int vipLevel);

	void setToDefault();

	void SureToBuyVip(Ref *pSender);

	void refreshInGotValue();
};

#endif

