#include "VipRewardCellItem.h"
#include "../../messageclient/element/COneVipGift.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "AppMacros.h"
#include "../../utils/GameUtils.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"
#include "../backpackscene/EquipmentItem.h"

VipRewardCellItem::VipRewardCellItem()
{
}


VipRewardCellItem::~VipRewardCellItem()
{
	delete curGoodInfo;
}

VipRewardCellItem* VipRewardCellItem::create(GoodsInfo* goods,int num)
{
	auto vipRewardCellItem = new VipRewardCellItem();
	if (vipRewardCellItem && vipRewardCellItem->init(goods,num))
	{
		vipRewardCellItem->autorelease();
		return vipRewardCellItem;
	}
	CC_SAFE_DELETE(vipRewardCellItem);
	return NULL;
}

bool VipRewardCellItem::init(GoodsInfo* goods,int num)
{
	if (UIScene::init())
	{
		/////////////////////////////////////////////////////////////////////////////////////

		// �Layer�λ�TableView�֮�ڣ�Ϊ����ȷ��ӦTableView���϶��¼������Ϊ���̵����¼
		//m_pLayer->setSwallowsTouches(false);

		curGoodInfo = new GoodsInfo();
		curGoodInfo->CopyFrom(*goods);
		/*********************��ж�װ������ɫ***************************/
		std::string frameColorPath;
		if (goods->quality() == 1)
		{
			frameColorPath = EquipWhiteFramePath;
		}
		else if (goods->quality() == 2)
		{
			frameColorPath = EquipGreenFramePath;
		}
		else if (goods->quality() == 3)
		{
			frameColorPath = EquipBlueFramePath;
		}
		else if (goods->quality() == 4)
		{
			frameColorPath = EquipPurpleFramePath;
		}
		else if (goods->quality() == 5)
		{
			frameColorPath = EquipOrangeFramePath;
		}
		else
		{
			frameColorPath = EquipVoidFramePath;
		}

		auto Btn_goodsItemFrame = Button::create();
		Btn_goodsItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_goodsItemFrame->setTouchEnabled(true);
		Btn_goodsItemFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
		Btn_goodsItemFrame->setPosition(Vec2(Btn_goodsItemFrame->getContentSize().width / 2, Btn_goodsItemFrame->getContentSize().height / 2));
		Btn_goodsItemFrame->setScale(1.0f);
		Btn_goodsItemFrame->setPressedActionEnabled(true);
		Btn_goodsItemFrame->addTouchEventListener(CC_CALLBACK_2(VipRewardCellItem::GoodItemEvent, this));
		m_pLayer->addChild(Btn_goodsItemFrame);

		auto uiiImageView_goodsItem = ImageView::create();
		if (strcmp(goods->icon().c_str(),"") == 0)
		{
			uiiImageView_goodsItem->loadTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(goods->icon().c_str());
			_path.append(".png");
			uiiImageView_goodsItem->loadTexture(_path.c_str());
		}
		uiiImageView_goodsItem->setScale(0.9f);
		uiiImageView_goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		uiiImageView_goodsItem->setPosition(Vec2(0,0));
		Btn_goodsItemFrame->addChild(uiiImageView_goodsItem);

		char s_num[20];
		sprintf(s_num,"%d",num);
		auto Lable_num = Label::createWithTTF(s_num, APP_FONT_NAME, 13);
		Lable_num->setAnchorPoint(Vec2(1.0f,0.5f));
		Lable_num->setPosition(Vec2(0 + Btn_goodsItemFrame->getContentSize().width / 2 - 7, 
			0 - Btn_goodsItemFrame->getContentSize().height / 2 + Lable_num->getContentSize().height - 2));
		Lable_num->setName("Label_num");
		Btn_goodsItemFrame->addChild(Lable_num);

		if (goods->binding() == 1)
		{
			auto ImageView_bound = ImageView::create();
			ImageView_bound->loadTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(Vec2(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(Vec2(8,55));
			m_pLayer->addChild(ImageView_bound);
		}

		//�����װ��
		if (goods->has_equipmentdetail())
		{
			if (goods->equipmentdetail().gradelevel() > 0)  //����ȼ�
			{
				std::string ss_gradeLevel = "+";
				char str_gradeLevel [10];
				sprintf(str_gradeLevel,"%d",goods->equipmentdetail().gradelevel());
				ss_gradeLevel.append(str_gradeLevel);

				auto Lable_gradeLevel = Label::createWithTTF(ss_gradeLevel.c_str(), APP_FONT_NAME, 13);
				Lable_gradeLevel->setAnchorPoint(Vec2(1.0f,1.0f));
				Lable_gradeLevel->setPosition(Vec2(Btn_goodsItemFrame->getContentSize().width-10,Btn_goodsItemFrame->getContentSize().height-7));
				Lable_gradeLevel->setName("Lable_gradeLevel");
				m_pLayer->addChild(Lable_gradeLevel);
			}

			if (goods->equipmentdetail().starlevel() > 0)  //�Ǽ�
			{
				char str_starLevel [10];
				sprintf(str_starLevel,"%d",goods->equipmentdetail().starlevel());
				auto Lable_starLevel = Label::createWithTTF(str_starLevel, APP_FONT_NAME, 13);
				Lable_starLevel->setAnchorPoint(Vec2(0,0));
				Lable_starLevel->setPosition(Vec2(8,6));
				Lable_starLevel->setName("Lable_starLevel");
				m_pLayer->addChild(Lable_starLevel);

				auto ImageView_star = ImageView::create();
				ImageView_star->loadTexture("res_ui/star_on.png");
				ImageView_star->setAnchorPoint(Vec2(0,0));
				ImageView_star->setPosition(Vec2(8+Lable_starLevel->getContentSize().width,8));
				ImageView_star->setVisible(true);
				ImageView_star->setScale(0.5f);
				m_pLayer->addChild(ImageView_star);
			}
		}

		this->setContentSize(Size(63,63));

		return true;
	}
	return false;
}


void VipRewardCellItem::GoodItemEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase) == NULL)
		{
			Size size = Director::getInstance()->getVisibleSize();
			auto goodsItemInfoBase = GoodsItemInfoBase::create(curGoodInfo, GameView::getInstance()->EquipListItem, 0);
			goodsItemInfoBase->setIgnoreAnchorPointForPosition(false);
			goodsItemInfoBase->setAnchorPoint(Vec2(0.5f, 0.5f));
			//goodsItemInfoBase->setPosition(Vec2(size.width/2,size.height/2));
			goodsItemInfoBase->setTag(kTagGoodsItemInfoBase);
			GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}
