#include "VipEveryDayRewardsUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/COneVipGift.h"
#include "../missionscene/MissionRewardGoodsItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "VipRewardCellItem.h"
#include "AppMacros.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/GameSceneState.h"
#include "VipDetailUI.h"


VipEveryDayRewardsUI::VipEveryDayRewardsUI(void)
{
}

VipEveryDayRewardsUI::~VipEveryDayRewardsUI()
{
}

VipEveryDayRewardsUI* VipEveryDayRewardsUI::create()
{
	auto vipEveryDayRewardsUI = new VipEveryDayRewardsUI();
	if (vipEveryDayRewardsUI && vipEveryDayRewardsUI->init())
	{
		vipEveryDayRewardsUI->autorelease();
		return vipEveryDayRewardsUI;
	}
	CC_SAFE_DELETE(vipEveryDayRewardsUI);
	return NULL;
}

bool VipEveryDayRewardsUI::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		//���UI
		if(LoadSceneLayer::vipEveryDayRewardPanel->getParent() != NULL)
		{
			LoadSceneLayer::vipEveryDayRewardPanel->removeFromParentAndCleanup(false);
		}
		auto panel_vip = LoadSceneLayer::vipEveryDayRewardPanel;
		panel_vip->setAnchorPoint(Vec2(0.0f,0.0f));
		panel_vip->setPosition(Vec2::ZERO);
		panel_vip->setTouchEnabled(true);
		m_pLayer->addChild(panel_vip);

		u_layer = Layer::create();
		addChild(u_layer);

		auto Btn_close = (Button*)Helper::seekWidgetByName(panel_vip,"Button_close");
		Btn_close->setTouchEnabled(true);
		Btn_close->setPressedActionEnabled(true);
		Btn_close->addTouchEventListener(CC_CALLBACK_2(VipEveryDayRewardsUI::CloseEvent, this));

		auto Btn_vipBuy = (Button*)Helper::seekWidgetByName(panel_vip,"Button_buy");
		Btn_vipBuy->setTouchEnabled(true);
		Btn_vipBuy->setPressedActionEnabled(true);
		Btn_vipBuy->addTouchEventListener(CC_CALLBACK_2(VipEveryDayRewardsUI::VipBuyEvent, this));

		auto lbt_vipLevel = (Text*)Helper::seekWidgetByName(panel_vip,"Label_vipValue");
		int vipLevel = 0;
		vipLevel = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel();
		std::string str_viplevel = "VIP";
		char str_level[20];
		sprintf(str_level,"%d",vipLevel);
		str_viplevel.append(str_level);
		lbt_vipLevel->setString(str_viplevel.c_str());

		auto lbt_remainTimeValue = (Text*)Helper::seekWidgetByName(panel_vip,"Label_remainTimeValue");
		int remainTime = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().vipremaintime()/1000/3600/24;
		std::string str_remainTime = "";
		char str_rt[20];
		sprintf(str_rt,"%d",remainTime);
		str_remainTime.append(str_rt);
		str_remainTime.append(StringDataManager::getString("label_tian"));
		lbt_remainTimeValue->setString(str_remainTime.c_str());

		m_tableView = TableView::create(this,Size(434,379));
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0,0));
		m_tableView->setPosition(Vec2(272,36));
		m_tableView->setContentOffset(Vec2(0,0));
		m_tableView->setDelegate(this);;
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		u_layer->addChild(m_tableView);

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(panel_vip->getContentSize());

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(VipEveryDayRewardsUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(VipEveryDayRewardsUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(VipEveryDayRewardsUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(VipEveryDayRewardsUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}



void VipEveryDayRewardsUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void VipEveryDayRewardsUI::onExit()
{
	UIScene::onExit();
}

bool VipEveryDayRewardsUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	return true;
}

void VipEveryDayRewardsUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void VipEveryDayRewardsUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void VipEveryDayRewardsUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void VipEveryDayRewardsUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void VipEveryDayRewardsUI::VipBuyEvent(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto winSize = Director::getInstance()->getVisibleSize();
		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		auto vipScene = (Layer*)mainscene->getChildByTag(kTagVipDetailUI);
		if (vipScene == NULL)
		{
			auto vipDetailUI = VipDetailUI::create();
			mainscene->addChild(vipDetailUI, 0, kTagVipDetailUI);
			vipDetailUI->setIgnoreAnchorPointForPosition(false);
			vipDetailUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			vipDetailUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

// void VipEveryDayRewardsUI::Vip1GetEvent( Ref* pSender )
// {
// 
// }
// 
// void VipEveryDayRewardsUI::Vip2GetEvent( Ref* pSender )
// {
// 
// }
// 
// void VipEveryDayRewardsUI::Vip3GetEvent( Ref* pSender )
// {
// 
// }
// 
// void VipEveryDayRewardsUI::Vip4GetEvent( Ref* pSender )
// {
// 
// }
// 
// void VipEveryDayRewardsUI::Vip5GetEvent( Ref* pSender )
// {
// 
// }

// void VipEveryDayRewardsUI::RefreshRewardItem()
// {
// 	for(int i = 0;i<GameView::getInstance()->m_vipEveryDayRewardsList.size();++i)
// 	{
// 		auto oneVipGift = GameView::getInstance()->m_vipEveryDayRewardsList.at(i);
// 		for(int j = 0;j<oneVipGift->goods_size();++j)
// 		{
// 			if (j >= 2)
// 				continue;
// 
// 			GoodsInfo * goodInfo = new GoodsInfo();
// 			goodInfo->CopyFrom(oneVipGift->goods(j));
// 			int num = 0;
// 			if (j < oneVipGift->goodsnumber_size())
// 				num = oneVipGift->goodsnumber(j);
// 
// 			MissionRewardGoodsItem * mrGoodsItem = MissionRewardGoodsItem::create(goodInfo,num);
// 			mrGoodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
// 			mrGoodsItem->setPosition(Vec2(352+79*j,57+73*(5-oneVipGift->number())));
// 			m_pLayer->addChild(mrGoodsItem);
// 
// 			delete goodInfo;
// 		}
// 
// 		RefreshStatus(oneVipGift->number(),oneVipGift->status());
// 	}
// }

// void VipEveryDayRewardsUI::RefreshStatus( int vipLevel ,int status)
// {
// 	switch(status)
// 	{
// 	case 1:   //ز�����ȡ
// 		{
// 			switch(vipLevel)
// 			{
// 			case 1:
// 				{
// 					Btn_get_1->setVisible(false);
// 					imageView_canNotGet_1->setVisible(true);
// 					l_content_1->setVisible(true);
// 					l_content_1->setText(StringDataManager::getString("vip_reward_cannotGet"));
// 				}
// 				break;
// 			case 2:
// 				{
// 					Btn_get_2->setVisible(false);
// 					imageView_canNotGet_2->setVisible(true);
// 					l_content_2->setVisible(true);
// 					l_content_2->setText(StringDataManager::getString("vip_reward_cannotGet"));
// 				}
// 				break;
// 			case 3:
// 				{
// 					Btn_get_3->setVisible(false);
// 					imageView_canNotGet_3->setVisible(true);
// 					l_content_3->setVisible(true);
// 					l_content_3->setText(StringDataManager::getString("vip_reward_cannotGet"));
// 				}
// 				break;
// 			case 4:
// 				{
// 					Btn_get_4->setVisible(false);
// 					imageView_canNotGet_4->setVisible(true);
// 					l_content_4->setVisible(true);
// 					l_content_4->setText(StringDataManager::getString("vip_reward_cannotGet"));
// 				}
// 				break;
// 			case 5:
// 				{
// 					Btn_get_5->setVisible(false);
// 					imageView_canNotGet_5->setVisible(true);
// 					l_content_5->setVisible(true);
// 					l_content_5->setText(StringDataManager::getString("vip_reward_cannotGet"));
// 				}
// 				break;
// 			}
// 		}
// 		break;
// 	case 2:   //����ȡ
// 		{
// 			switch(vipLevel)
// 			{
// 			case 1:
// 				{
// 					Btn_get_1->setVisible(true);
// 					imageView_canNotGet_1->setVisible(false);
// 					l_content_1->setVisible(false);
// 				}
// 				break;
// 			case 2:
// 				{
// 					Btn_get_2->setVisible(true);
// 					imageView_canNotGet_2->setVisible(false);
// 					l_content_2->setVisible(false);
// 				}
// 				break;
// 			case 3:
// 				{
// 					Btn_get_3->setVisible(true);
// 					imageView_canNotGet_3->setVisible(false);
// 					l_content_3->setVisible(false);
// 				}
// 				break;
// 			case 4:
// 				{
// 					Btn_get_4->setVisible(true);
// 					imageView_canNotGet_4->setVisible(false);
// 					l_content_4->setVisible(false);
// 				}
// 				break;
// 			case 5:
// 				{
// 					Btn_get_5->setVisible(true);
// 					imageView_canNotGet_5->setVisible(false);
// 					l_content_5->setVisible(false);
// 				}
// 				break;
// 			}
// 		}
// 		break;
// 	case 3:   //����ȡ
// 		{
// 			switch(vipLevel)
// 			{
// 			case 1:
// 				{
// 					Btn_get_1->setVisible(false);
// 					imageView_canNotGet_1->setVisible(true);
// 					l_content_1->setVisible(true);
// 					l_content_1->setText(StringDataManager::getString("vip_reward_Geted"));
// 				}
// 				break;
// 			case 2:
// 				{
// 					Btn_get_2->setVisible(false);
// 					imageView_canNotGet_2->setVisible(true);
// 					l_content_2->setVisible(true);
// 					l_content_2->setText(StringDataManager::getString("vip_reward_Geted"));
// 				}
// 				break;
// 			case 3:
// 				{
// 					Btn_get_3->setVisible(false);
// 					imageView_canNotGet_3->setVisible(true);
// 					l_content_3->setVisible(true);
// 					l_content_3->setText(StringDataManager::getString("vip_reward_Geted"));
// 				}
// 				break;
// 			case 4:
// 				{
// 					Btn_get_4->setVisible(false);
// 					imageView_canNotGet_4->setVisible(true);
// 					l_content_4->setVisible(true);
// 					l_content_4->setText(StringDataManager::getString("vip_reward_Geted"));
// 				}
// 				break;
// 			case 5:
// 				{
// 					Btn_get_5->setVisible(false);
// 					imageView_canNotGet_5->setVisible(true);
// 					l_content_5->setVisible(true);
// 					l_content_5->setText(StringDataManager::getString("vip_reward_Geted"));
// 				}
// 				break;
// 			}
// 		}
// 		break;
// 	}
// }

void VipEveryDayRewardsUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void VipEveryDayRewardsUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void VipEveryDayRewardsUI::tableCellTouched( TableView* table, TableViewCell* cell )
{

}

cocos2d::Size VipEveryDayRewardsUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(423,75);
}

cocos2d::extension::TableViewCell* VipEveryDayRewardsUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell=new TableViewCell();
	cell->autorelease();

	auto oneVipGift = GameView::getInstance()->m_vipEveryDayRewardsList.at(idx);

	auto tempCell = VipEveryDayRewardCell::create(oneVipGift);
	cell->addChild(tempCell);
// 
// 	cocos2d::extension::Scale9Sprite * sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/highlight.png.png");
// 	sprite_frame->setContentSize(Size(366,71));
// 	sprite_frame->setAnchorPoint(Vec2(0,0));
// 	sprite_frame->setPosition(Vec2(0,0));
// 	cell->addChild(sprite_frame);
// 
// 	Sprite * sprite_cion = Sprite::create("res_ui/vip/vip_b.png");
// 	sprite_cion->setAnchorPoint(Vec2(0,0));
// 	sprite_cion->setPosition(Vec2(35,35));
// 	cell->addChild(sprite_cion);
// 
// 	char str_level[20];
// 	sprintf(str_level,"%d",oneVipGift->number());
// 	CCLabel * l_vipLevel = CCLabel::create(str_level.,"res_ui/font/ziti_3.fnt",);
// 	l_vipLevel->setScale(0.9f);
// 	l_vipLevel->setAnchorPoint(Vec2(0.5f,0.5f));
// 	l_vipLevel->setPosition(Vec2(12,-2));
// 	sprite_cion->addChild(l_vipLevel);
// 
// 	for(int j = 0;j<oneVipGift->goods_size();++j)
// 	{
// 		if (j >= 2)
// 			continue;
// 
// 		GoodsInfo * goodInfo = new GoodsInfo();
// 		goodInfo->CopyFrom(oneVipGift->goods(j));
// 		int num = 0;
// 		if (j < oneVipGift->goodsnumber_size())
// 			num = oneVipGift->goodsnumber(j);
// 
// 		VipRewardCellItem * goodsItem = VipRewardCellItem::create(goodInfo,num);
// 		goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
// 		goodsItem->setPosition(Vec2(116+79*j,35));
// 		cell->addChild(goodsItem);
// 
// 		delete goodInfo;
// 	}
	

	return cell;
}

ssize_t VipEveryDayRewardsUI::numberOfCellsInTableView( TableView *table )
{
	return GameView::getInstance()->m_vipEveryDayRewardsList.size();
}

void VipEveryDayRewardsUI::refreshTableViewWithOutChangeOffSet()
{
	Vec2 _s = m_tableView->getContentOffset();
	int _h = m_tableView->getContentSize().height + _s.y;
	m_tableView->reloadData();
	Vec2 temp = Vec2(_s.x,_h - m_tableView->getContentSize().height);
	m_tableView->setContentOffset(temp); 
}


///////////////////////////////////////////////////////////////////////////////////////////////////

VipEveryDayRewardCell::VipEveryDayRewardCell()
{

}

VipEveryDayRewardCell::~VipEveryDayRewardCell()
{
	delete curOneVipGift;
}

VipEveryDayRewardCell* VipEveryDayRewardCell::create( COneVipGift * oneVipGift )
{
	auto vipEveryDayRewardCell = new VipEveryDayRewardCell();
	if (vipEveryDayRewardCell && vipEveryDayRewardCell->init(oneVipGift))
	{
		vipEveryDayRewardCell->autorelease();
		return vipEveryDayRewardCell;
	}
	CC_SAFE_DELETE(vipEveryDayRewardCell);
	return NULL;
}

bool VipEveryDayRewardCell::init( COneVipGift * oneVipGift )
{
	if (UIScene::init())
	{
		//m_pLayer->setSwallowsTouches(false);

		curOneVipGift = new COneVipGift();
		curOneVipGift->CopyFrom(*oneVipGift);

		auto image_frame = ImageView::create();
		image_frame->loadTexture("res_ui/kuang02_new.png");
		image_frame->setScale9Enabled(true);
		image_frame->setContentSize(Size(423,72));
		image_frame->setCapInsets(Rect(16,33,1,1));
		image_frame->setAnchorPoint(Vec2(0.5f,0.5f));
		image_frame->setPosition(Vec2(423/2,72/2));
		m_pLayer->addChild(image_frame);
 		
// 		ImageView * image_line = ImageView::create();
// 		image_line->loadTexture("res_ui/henggang_red.png");
// 		image_line->setAnchorPoint(Vec2(0.5f,0.5f));
// 		image_line->setPosition(Vec2(423/2,2));
// 		image_line->setScaleX(2.3f);
// 		m_pLayer->addChild(image_line);

		auto imageView_icon = ImageView::create();
		imageView_icon->loadTexture("res_ui/vip/vip_b.png");
		imageView_icon->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_icon->setPosition(Vec2(60,36));
		m_pLayer->addChild(imageView_icon);

		char str_level[20];
		sprintf(str_level,"%d",oneVipGift->number());
		auto l_vipLevel = Label::createWithBMFont("res_ui/font/ziti_3.fnt", str_level);
		l_vipLevel->setScale(0.9f);
		l_vipLevel->setAnchorPoint(Vec2(0.5f,0.5f));
		l_vipLevel->setPosition(Vec2(13,-2));
		imageView_icon->addChild(l_vipLevel);

		for(int j = 0;j<oneVipGift->goods_size();++j)
		{
			if (j >= 2)
				continue;

			auto goodInfo = new GoodsInfo();
			goodInfo->CopyFrom(oneVipGift->goods(j));
			int num = 0;
			if (j < oneVipGift->goodsnumber_size())
				num = oneVipGift->goodsnumber(j);

			auto goodsItem = VipRewardCellItem::create(goodInfo,num);
			goodsItem->setIgnoreAnchorPointForPosition(false);
			goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
			goodsItem->setPosition(Vec2(150+79*j,36));
			m_pLayer->addChild(goodsItem);

			delete goodInfo;
		}

		switch(oneVipGift->status())
		{
		case 1:   //������ȡ
		 	{
				auto imageView_di = ImageView::create();
				imageView_di->loadTexture("res_ui/zhezhao_btn.png");
				imageView_di->setAnchorPoint(Vec2(0.5f,0.5f));
				imageView_di->setPosition(Vec2(336,36));
		 		m_pLayer->addChild(imageView_di);
				imageView_di->setScale(1.2f);

				std::string str_des = "VIP";
				char s_level[20];
				sprintf(s_level,"%d",oneVipGift->number());
				str_des.append(s_level);
				str_des.append(StringDataManager::getString("vip_reward_cannotGet"));

				auto l_des = Label::createWithTTF(str_des.c_str(), APP_FONT_NAME, 18);
				l_des->setAnchorPoint(Vec2(0.5f,0.5f));
				l_des->setPosition(Vec2(336,36));
				m_pLayer->addChild(l_des);
			}
			break;
		case 2:   //����ȡ
			{
				auto btn_get = Button::create();
				btn_get->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				btn_get->setScale9Enabled(true);
				btn_get->setContentSize(Size(108,43));
				btn_get->setCapInsets(Rect(18,9,2,23));
				btn_get->setAnchorPoint(Vec2(0.5f,0.5f));
				btn_get->setPosition(Vec2(336,36));
				btn_get->setTouchEnabled(true);
				btn_get->setPressedActionEnabled(true);
				btn_get->addTouchEventListener(CC_CALLBACK_2(VipEveryDayRewardCell::getRewardEvent, this));
				m_pLayer->addChild(btn_get);

				auto l_get = Label::createWithTTF(StringDataManager::getString("btn_lingqu"), APP_FONT_NAME, 18);
				l_get->setAnchorPoint(Vec2(0.5f,0.5f));
				l_get->setPosition(Vec2(0,0));
				btn_get->addChild(l_get);
			}
			break;
		case 3:   //����ȡ
			{
				auto imageView_di = ImageView::create();
				imageView_di->loadTexture("res_ui/zhezhao_btn.png");
				imageView_di->setAnchorPoint(Vec2(0.5f,0.5f));
				imageView_di->setPosition(Vec2(336,36));
				m_pLayer->addChild(imageView_di);
				imageView_di->setScale(1.2f);

				auto l_des = Label::createWithTTF(StringDataManager::getString("vip_reward_Geted"), APP_FONT_NAME, 18);
				l_des->setAnchorPoint(Vec2(0.5f,0.5f));
				l_des->setPosition(Vec2(336,36));
				m_pLayer->addChild(l_des);
			}
			break;
		}

		this->setContentSize(Size(423,72));

		return true;
	}
	return false;
}

void VipEveryDayRewardCell::getRewardEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//req to get reward
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5119);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}
