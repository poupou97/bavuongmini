
#ifndef  _VIPUI_VIPREWARDCELLITEM_H_
#define _VIPUI_VIPREWARDCELLITEM_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GoodsInfo;

class VipRewardCellItem : public UIScene
{
public:
	VipRewardCellItem();
	~VipRewardCellItem();

	static VipRewardCellItem* create(GoodsInfo* goods,int num);
	bool init(GoodsInfo* goods,int num);

	void BuyEvent(Ref *pSender);
	void GoodItemEvent(Ref *pSender, Widget::TouchEventType type);

private:
	GoodsInfo * curGoodInfo;
};

#endif


