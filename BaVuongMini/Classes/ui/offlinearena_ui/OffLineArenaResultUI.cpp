#include "OffLineArenaResultUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "OffLineArenaState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/GameSceneState.h"
#include "GameView.h"
#include "../../utils/GameUtils.h"
#include "../../ui/FivePersonInstance/ResultHeadPartItem.h"
#include "../../ui/GameUIConstant.h"
#include "OffLineArenaData.h"
#include "../../gamescene_state/MainAnimationScene.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "cocostudio/CCSGUIReader.h"


OffLineArenaResultUI::OffLineArenaResultUI():
m_nRemainTime(30000),
m_bIsWin(true),
m_nExp(0),
countDown_startTime(0)
{
}


OffLineArenaResultUI::~OffLineArenaResultUI()
{
}

OffLineArenaResultUI * OffLineArenaResultUI::create( bool isWin,int exp )
{
	auto offLineArenaResultUI = new OffLineArenaResultUI();
	if (offLineArenaResultUI && offLineArenaResultUI->init(isWin,exp))
	{
		offLineArenaResultUI->autorelease();
		return offLineArenaResultUI;
	}
	CC_SAFE_DELETE(offLineArenaResultUI);
	return NULL;
}

bool OffLineArenaResultUI::init(bool isWin, int exp )
{
	if (UIScene::init())
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		m_bIsWin = isWin;
		m_nExp = exp;
		countDown_startTime = GameUtils::millisecondNow();

		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		if (!mainscene)
			return false;

		auto action =(ActionInterval *)Sequence::create(
			DelayTime::create(END_BATTLE_BEFORE_SLOWMOTION_DURATION),
			CallFunc::create(CC_CALLBACK_0(OffLineArenaResultUI::startSlowMotion,this)),
			CallFunc::create(CC_CALLBACK_0(OffLineArenaResultUI::addLightAnimation,this)),
			DelayTime::create(END_BATTLE_SLOWMOTION_DURATION),
			CallFunc::create(CC_CALLBACK_0(OffLineArenaResultUI::endSlowMotion,this)),
			CallFunc::create(CC_CALLBACK_0(OffLineArenaResultUI::showUIAnimation,this)),
			DelayTime::create(1.8f),
			CallFunc::create(CC_CALLBACK_0(OffLineArenaResultUI::createAnimation,this)),
			NULL);
		this->runAction(action);

		this->setContentSize(winsize);
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(OffLineArenaResultUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(OffLineArenaResultUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(OffLineArenaResultUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(OffLineArenaResultUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void OffLineArenaResultUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void OffLineArenaResultUI::onExit()
{
	UIScene::onExit();
}

bool OffLineArenaResultUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void OffLineArenaResultUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void OffLineArenaResultUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void OffLineArenaResultUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void OffLineArenaResultUI::GetRewardEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		exitArena();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void OffLineArenaResultUI::update( float dt )
{
	m_nRemainTime -= 1000;
	if (m_nRemainTime <= 0)
	{
		m_nRemainTime = 0;
		exitArena();
	}

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	l_remainTime->setString(s_remainTime);
}

void OffLineArenaResultUI::FightAgainEvent( Ref *pSender )
{
	if (OffLineArenaState::getStatus() == OffLineArenaState::status_in_arena)
	{
		if (OffLineArenaState::getCurrentOpppnentId() != -1)
		{
			//�����ٴ���ս

		}
	}
}

void OffLineArenaResultUI::exitArena()
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(3021);
	this->removeFromParent();
}

void OffLineArenaResultUI::showRankingAnimation()
{
	auto winSize = Director::getInstance()->getVisibleSize();
	if (m_bIsWin)  //ʤ�
	{
		mainPanel->setVisible(true);

		auto panel_ranking = (Layout*)Helper::seekWidgetByName(mainPanel,"Panel_ranking");
		panel_ranking->setVisible(true);

		auto panel_reward = (Layout*)Helper::seekWidgetByName(mainPanel,"Panel_reward");
		panel_reward->setVisible(false);

		auto widget = Widget::create();
		widget->setAnchorPoint(Vec2(5.f,.5f));
		widget->setPosition(Vec2(345,131));
		widget->setScale(3.0f);
		panel_ranking->addChild(widget);

		
		char s_rank [20];
		sprintf(s_rank,"%d",OffLineArenaData::getInstance()->m_nTargetRanking);
		auto l_curRank = Label::createWithBMFont("res_ui/font/ziti_13.fnt", s_rank);
		l_curRank->setAnchorPoint(Vec2(.5f,.5f));
		l_curRank->setPosition(Vec2(0,0));
		widget->addChild(l_curRank);

		auto imageView_di = ImageView::create();
		imageView_di->loadTexture("res_ui/arena/di.png");
		imageView_di->setAnchorPoint(Vec2(1.0f,.5f));
		imageView_di->setPosition(Vec2(l_curRank->getPosition().y - l_curRank->getContentSize().width/2,l_curRank->getPosition().y));
		widget->addChild(imageView_di);

		auto imageView_ming = ImageView::create();
		imageView_ming->loadTexture("res_ui/arena/ming.png");
		imageView_ming->setAnchorPoint(Vec2(.0f,.5f));
		imageView_ming->setPosition(Vec2(l_curRank->getPosition().y + l_curRank->getContentSize().width/2,l_curRank->getPosition().y));
		widget->addChild(imageView_ming);

		auto sequence = Sequence::create(
			CCEaseExponentialIn::create(ScaleTo::create(0.35f,1.0f)),
			DelayTime::create(2.0f),
			CallFunc::create(CC_CALLBACK_0(OffLineArenaResultUI::createUI,this)),
			NULL);
		widget->runAction(sequence);
	}
	else
	{
		this->createUI();
	}
}

void OffLineArenaResultUI::createUI()
{
	auto winSize = Director::getInstance()->getVisibleSize();

	mainPanel->setVisible(true);

	auto panel_ranking = (Layout*)Helper::seekWidgetByName(mainPanel,"Panel_ranking");
	panel_ranking->setVisible(false);

	auto panel_reward = (Layout*)Helper::seekWidgetByName(mainPanel,"Panel_reward");
	panel_reward->setVisible(true);

	auto btn_getReward= (Button *)Helper::seekWidgetByName(mainPanel,"Button_enter");
	CCAssert(btn_getReward != NULL, "should not be nil");
	btn_getReward->setTouchEnabled(true);
	btn_getReward->setPressedActionEnabled(true);
	btn_getReward->addTouchEventListener(CC_CALLBACK_2(OffLineArenaResultUI::GetRewardEvent,this));

	//��ʱ
	l_remainTime = (Text*)Helper::seekWidgetByName(mainPanel,"Label_time");	

	m_nRemainTime -= (GameUtils::millisecondNow() - countDown_startTime);
	m_nRemainTime -= 1000;

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	l_remainTime->setString(s_remainTime);

	this->schedule(schedule_selector(OffLineArenaResultUI::update),1.0f);

	//չʾ�佫�þ��
	int space = 19;
	int width = 66;
	int allsize = GameView::getInstance()->generalsInLineList.size()+1;
	float firstPos_x = 400- (space+width)*0.5f*(allsize-1);

	for (int i = 0;i<allsize;++i)
	{
		if (i >= 6)
			continue;

		auto temp = ResultHeadPartItem::create(m_nExp,i);
		temp->setIgnoreAnchorPointForPosition(false);
		temp->setAnchorPoint(Vec2(0.5f,0.5f));
		temp->setPosition(Vec2(firstPos_x+(width+space)*i,245));
		u_layer->addChild(temp);
	}
}

void OffLineArenaResultUI::showUIAnimation()
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainscene != NULL)
	{
		if (m_bIsWin)
		{
			//mainscene->addInterfaceAnm("tzcg/tzcg.anm");
			auto mainAnmScene = GameView::getInstance()->getMainAnimationScene();
			if (mainAnmScene)
			{
				mainAnmScene->addInterfaceAnm("tzcg/tzcg.anm");
			}
		}
		else
		{
			//mainscene->addInterfaceAnm("tzsb/tzsb.anm");
			auto mainAnmScene = GameView::getInstance()->getMainAnimationScene();
			if (mainAnmScene)
			{
				mainAnmScene->addInterfaceAnm("tzsb/tzsb.anm");
			}
		}
	}
}
void OffLineArenaResultUI::startSlowMotion()
{
	Director::getInstance()->getScheduler()->setTimeScale(0.2f);
}
void OffLineArenaResultUI::endSlowMotion()
{
	Director::getInstance()->getScheduler()->setTimeScale(1.0f);
}

void OffLineArenaResultUI::addLightAnimation()
{
	Size size = Director::getInstance()->getVisibleSize();
	std::string animFileName = "animation/texiao/renwutexiao/SHANGUANG/shanguang.anm";
	auto la_light = CCLegendAnimation::create(animFileName);
	if (la_light)
	{
		la_light->setPlayLoop(false);
		la_light->setReleaseWhenStop(true);
		la_light->setPlaySpeed(5.0f);
		la_light->setScale(0.85f);
		la_light->setPosition(Vec2(size.width/2,size.height/2));
		GameView::getInstance()->getMainAnimationScene()->addChild(la_light);
	}
}

void OffLineArenaResultUI::createAnimation()
{
	Size winSize = Director::getInstance()->getVisibleSize();
	u_layer=Layer::create();
	u_layer->setIgnoreAnchorPointForPosition(false);
	u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
	u_layer->setPosition(Vec2(winSize.width/2,winSize.height/2));
	u_layer->setContentSize(Size(UI_DESIGN_RESOLUTION_WIDTH,UI_DESIGN_RESOLUTION_HEIGHT));
	addChild(u_layer);

	auto mengban = ImageView::create();
	mengban->loadTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enabled(true);
	mengban->setContentSize(winSize);
	mengban->setAnchorPoint(Vec2(0,0));
	mengban->setPosition(Vec2(0,0));
	m_pLayer->addChild(mengban);

	mainPanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/arena_new_1.json");
	mainPanel->setAnchorPoint(Vec2(0.5f,0.5f));
	mainPanel->setPosition(Vec2(winSize.width/2,winSize.height/2-20));
	m_pLayer->addChild(mainPanel);
	mainPanel->setVisible(false);

	//��ݶ�����ƴ����������
	cocostudio::Armature *armature ;
	if (m_bIsWin)
	{
		//�ӵ����ļ��첽���ض���
		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("res_ui/uiflash/winLogo/winLogo0.png","res_ui/uiflash/winLogo/winLogo0.plist","res_ui/uiflash/winLogo/winLogo.ExportJson");
		armature = cocostudio::Armature::create("winLogo");
		armature->getAnimation()->play("Animation_begin");
		armature->getAnimation()->setMovementEventCallFunc(this,movementEvent_selector(OffLineArenaResultUI::ArmatureFinishCallback));  
	}
	else
	{	
		//�ӵ����ļ��첽���ض���
		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("res_ui/uiflash/lostLogo/lostLogo0.png","res_ui/uiflash/lostLogo/lostLogo0.plist","res_ui/uiflash/lostLogo/lostLogo.ExportJson");
		armature = cocostudio::Armature::create("lostLogo");
		//����ָ�����
		armature->getAnimation()->playWithIndex(0,-1,cocostudio::ANIMATION_NO_LOOP);
	}
	//��޸����
	armature->setScale(1.0f);
	//����ö�������λ�
	armature->setPosition(Vec2(UI_DESIGN_RESOLUTION_WIDTH/2,UI_DESIGN_RESOLUTION_HEIGHT/2));
	//���ӵ���ǰҳ�
	u_layer->addChild(armature);

	auto sequence = Sequence::create(
		DelayTime::create(2.3f),
		CallFunc::create(CC_CALLBACK_0(OffLineArenaResultUI::showRankingAnimation,this)),
		NULL
		);
	this->runAction(sequence);

	/*************************************************************/
	//�ʤ���ķ�
// 	ImageView * imageView_light = ImageView::create();
// 	imageView_light->loadTexture("res_ui/lightlight.png");
// 	imageView_light->setAnchorPoint(Vec2(0.5f,0.5f));
// 	imageView_light->setPosition(Vec2(UI_DESIGN_RESOLUTION_WIDTH/2,UI_DESIGN_RESOLUTION_HEIGHT/2));
// 	u_layer->addChild(imageView_light);
// 	imageView_light->setVisible(false);

	//��
// 	ImageView * imageView_result = ImageView::create();
// 	if (m_bIsWin)
// 	{
// 		imageView_result->loadTexture("res_ui/jiazuzhan/win.png");
// 	}
// 	else
// 	{
// 		imageView_result->loadTexture("res_ui/jiazuzhan/lost.png");
// 	}
// 	imageView_result->setAnchorPoint(Vec2(0.5f,0.5f));
// 	imageView_result->setPosition(Vec2(UI_DESIGN_RESOLUTION_WIDTH/2,UI_DESIGN_RESOLUTION_HEIGHT/2));
// 	imageView_result->setScale(RESULT_UI_RESULTFLAG_BEGINSCALE);
// 	u_layer->addChild(imageView_result);

// 	Sequence * sequence = Sequence::create(ScaleTo::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME,1.0f,1.0f),
// 		DelayTime::create(RESULT_UI_RESULTFLAG_DELAY_TIME),
// 		MoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,Vec2(UI_DESIGN_RESOLUTION_WIDTH/2,391)),
// 		DelayTime::create(0.05f),
// 		CallFunc::create(this,callfunc_selector(OffLineArenaResultUI::showRankingAnimation)),
// 		NULL);
// 	imageView_result->runAction(sequence);

// 	if (m_bIsWin)
// 	{
// 		CCRotateBy * rotateAnm = RotateBy::create(4.0f,360);
// 		RepeatForever * repeatAnm = RepeatForever::create(Sequence::create(rotateAnm,NULL));
// 		Sequence * sequence_light = Sequence::create(
// 			DelayTime::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME),
// 			Show::create(),
// 			DelayTime::create(RESULT_UI_RESULTFLAG_DELAY_TIME),
// 			MoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,Vec2(400,391)),
// 			NULL);
// 		imageView_light->runAction(sequence_light);
// 		imageView_light->runAction(repeatAnm);
// 	}
}

void OffLineArenaResultUI::ArmatureFinishCallback(cocostudio::CCArmature *armature, cocostudio::MovementEventType movementType, const char *movementID )
{
	std::string id = movementID;
	if (id.compare("Animation_begin") == 0)
	{
		armature->stopAllActions();
		armature->getAnimation()->play("Animation1_end");
	}
}


