#ifndef _OFFLINEARENA_OFFLINEARENADATA_H_
#define _OFFLINEARENA_OFFLINEARENADATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ������������
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.05.30
 */

class OffLineArenaData
{
public:
	OffLineArenaData(void);
	~OffLineArenaData(void);

	enum OLA_Status
	{
		s_none = 0,
		s_inCountDown,
		s_inBattle
	};

public: 
	static OffLineArenaData * s_arenaData;
	static OffLineArenaData * getInstance();

	int getCurStatus();
	void setCurStatus(int _value);

	//�ҵ�����
	int m_nMyRoleRanking;
	int m_nCurStatus;

	//Ҫ��ս���������
	int m_nTargetRanking;
};

#endif
