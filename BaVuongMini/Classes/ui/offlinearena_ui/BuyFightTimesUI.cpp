#include "BuyFightTimesUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "OffLineArenaUI.h"
#include "GameView.h"
#include "../../gamescene_state/GameScene.h"
#include "../../gamescene_state/MainScene.h"
#include "cocostudio\CCSGUIReader.h"

BuyFightTimesUI::BuyFightTimesUI()
{
}

BuyFightTimesUI::~BuyFightTimesUI()
{
}

BuyFightTimesUI * BuyFightTimesUI::create()
{
	auto buyFightTimesUI = new BuyFightTimesUI();
	if (buyFightTimesUI && buyFightTimesUI->init())
	{
		buyFightTimesUI->autorelease();
		return buyFightTimesUI;
	}
	CC_SAFE_DELETE(buyFightTimesUI);
	return NULL;
}

bool BuyFightTimesUI::init()
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate(); 

		//���UI
		auto ppanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/arena_popupo2_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto btn_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(BuyFightTimesUI::CloseEvent, this));
		btn_close->setPressedActionEnabled(true);

		auto btn_oneTimes = (Button*)Helper::seekWidgetByName(ppanel,"Button_take");
		btn_oneTimes->setTouchEnabled(true);
		btn_oneTimes->addTouchEventListener(CC_CALLBACK_2(BuyFightTimesUI::BuyOneTimesEvent, this));
		btn_oneTimes->setPressedActionEnabled(true);

		auto btn_fiveTimes = (Button*)Helper::seekWidgetByName(ppanel,"Button_take1");
		btn_fiveTimes->setTouchEnabled(true);
		btn_fiveTimes->addTouchEventListener(CC_CALLBACK_2(BuyFightTimesUI::BuyFiveTimesEvent, this));
		btn_fiveTimes->setPressedActionEnabled(true);

		auto btn_tenTimes = (Button*)Helper::seekWidgetByName(ppanel,"Button_take2");
		btn_tenTimes->setTouchEnabled(true);
		btn_tenTimes->addTouchEventListener(CC_CALLBACK_2(BuyFightTimesUI::BuyTenTimesEvent, this));
		btn_tenTimes->setPressedActionEnabled(true);

		l_remainTime = (Text*)Helper::seekWidgetByName(ppanel,"Label_number");

		this->setContentSize(ppanel->getContentSize());
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(BuyFightTimesUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(BuyFightTimesUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(BuyFightTimesUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(BuyFightTimesUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void BuyFightTimesUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void BuyFightTimesUI::onExit()
{
	UIScene::onExit();
}

bool BuyFightTimesUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return resignFirstResponder(touch,this,false);
}

void BuyFightTimesUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void BuyFightTimesUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void BuyFightTimesUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void BuyFightTimesUI::BuyOneTimesEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(3005, (void *)1);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void BuyFightTimesUI::BuyFiveTimesEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(3005, (void *)5);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	GameMessageProcessor::sharedMsgProcessor()->sendReq(3005,(void *)5);
}

void BuyFightTimesUI::BuyTenTimesEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(3005, (void *)10);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void BuyFightTimesUI::update( float dt )
{
	auto offLineArenaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
	if (!offLineArenaUI)
		return;

	char s_remainBuyTimes[10];
	sprintf(s_remainBuyTimes,"%d",offLineArenaUI->getRemainBuyTimes());
	l_remainTime->setString(s_remainBuyTimes);
}

void BuyFightTimesUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

