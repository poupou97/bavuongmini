#include "RankListUI.h"
#include "../extensions/UIScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "OffLineArenaUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CPartner.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../gamescene_state/sceneelement/TargetInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/GameConfig.h"
#include "../../messageclient/element/CSimplePartner.h"

#define  SelectImageTag 458

RankListUI::RankListUI():
lastSelectCellId(0),
selectCellId(0)
{
}


RankListUI::~RankListUI()
{
	std::vector<CSimplePartner*>::iterator iter_ranking;
	for (iter_ranking = curRankingList.begin(); iter_ranking != curRankingList.end(); ++iter_ranking)
	{
		delete *iter_ranking;
	}
	curRankingList.clear();
}

RankListUI * RankListUI::create(int myRanking)
{
	auto rankListUI = new RankListUI();
	if (rankListUI && rankListUI->init(myRanking))
	{
		rankListUI->autorelease();
		return rankListUI;
	}
	CC_SAFE_DELETE(rankListUI);
	return NULL;
}

bool RankListUI::init(int myRanking)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate(); 

		//���UI
		if(LoadSceneLayer::RankListLayer->getParent() != NULL)
		{
			LoadSceneLayer::RankListLayer->removeFromParentAndCleanup(false);
		}
		auto ppanel = LoadSceneLayer::RankListLayer;
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(608, 417));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);
		
		auto panel_fightNotes = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_conbatRecord");
		panel_fightNotes->setVisible(false);
		auto panel_rankList = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_ranking");
		panel_rankList->setVisible(true);
		auto panel_honorShop = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_honorShop");
		panel_honorShop->setVisible(false);

		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(RankListUI::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);

		auto btn_makeFriend = (Button*)Helper::seekWidgetByName(panel_rankList,"Button_makeFriend");
		btn_makeFriend->setTouchEnabled(true);
		btn_makeFriend->addTouchEventListener(CC_CALLBACK_2(RankListUI::MakeFriendEvent, this));
		btn_makeFriend->setPressedActionEnabled(true);
		btn_makeFriend->setVisible(false);

		auto btn_lookOver = (Button*)Helper::seekWidgetByName(panel_rankList,"Button_view");
		btn_lookOver->setTouchEnabled(true);
		btn_lookOver->addTouchEventListener(CC_CALLBACK_2(RankListUI::LookOverEvent, this));
		btn_lookOver->setPressedActionEnabled(true);
		btn_lookOver->setVisible(false);

		l_selfRanking = (Text*)Helper::seekWidgetByName(panel_rankList,"Label_roleRank");
		//��ҵ����
		char s_ranking [20];
		sprintf(s_ranking,"%d",myRanking);
		l_selfRanking->setString(s_ranking);
// 		for(int i = 0;i<curRankingList.size();++i)
// 		{
// 			if (GameView::getInstance()->myplayer->getRoleId() == curRankingList.at(i)->role().rolebase().roleid())
// 			{
// 				char s_ranking [20];
// 				sprintf(s_ranking,"%d",curRankingList.at(i)->ranking());
// 				l_selfRanking->setText(s_ranking);
// 				break;
// 			}
// 		}

		m_tableView = TableView::create(this,Size(517,227));
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0,0));
		m_tableView->setPosition(Vec2(45,77));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(m_tableView);

		this->setContentSize(Size(608,417));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(RankListUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(RankListUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(RankListUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(RankListUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void RankListUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RankListUI::onExit()
{
	UIScene::onExit();
}

bool RankListUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void RankListUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void RankListUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void RankListUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void RankListUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void RankListUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void RankListUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void RankListUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	selectCellId = cell->getIdx();
	//�ȡ���ϴ�ѡ��״̬�����±���ѡ��״̬
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();

	//
	Size winSize_ = Director::getInstance()->getVisibleSize();
	long long roleId = curRankingList.at(selectCellId)->role().roleid();
	std::string name_ = curRankingList.at(selectCellId)->role().name();
	int countryId_ = curRankingList.at(selectCellId)->role().countryid();
	int vipLv_ = curRankingList.at(selectCellId)->role().viplevel();
	int level_ = curRankingList.at(selectCellId)->role().level();
	int pressionId_ = curRankingList.at(selectCellId)->role().profession();

	auto tarlist = TargetInfoList::create(roleId, name_,countryId_,vipLv_,level_,pressionId_);
	tarlist->setIgnoreAnchorPointForPosition(false);
	tarlist->setAnchorPoint(Vec2(0, 0));
	tarlist->setTag(ktagMainSceneTargetInfo);
	tarlist->setPosition(Vec2(winSize_.width/2+ this->getContentSize().width/2 - tarlist->getContentSize().width - 50,
								winSize_.height/2+tarlist->getContentSize().height/2 - 50));
	GameView::getInstance()->getMainUIScene()->addChild(tarlist);
}

cocos2d::Size RankListUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(510,34);
}

cocos2d::extension::TableViewCell* RankListUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto curPartner = curRankingList.at(idx);

	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(Size(515,32));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setPosition(Vec2(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	auto colorFont = getFontColor(curPartner->ranking());

	//��߿
	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(0, 0));
	sprite_bigFrame->setCapInsets(Rect(9,14,1,1));
	sprite_bigFrame->setPreferredSize(Size(515,32));
	cell->addChild(sprite_bigFrame);
	//����
	char s_ranking[20];
	sprintf(s_ranking,"%d",curPartner->ranking());
	if (curPartner->ranking()>0 && curPartner->ranking() <= 3)
	{
		std::string icon_path = "res_ui/rank";
		icon_path.append(s_ranking);
		icon_path.append(".png");
		auto sp_ranking = Sprite::create(icon_path.c_str());
		sp_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		sp_ranking->setPosition(Vec2(31,15));
		sp_ranking->setScale(0.95f);
		cell->addChild(sp_ranking);
	}
	else if(curPartner->ranking() <= 10)
	{
		auto l_ranking = Label::createWithBMFont("res_ui/font/ziti_3.fnt",s_ranking);
		l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		l_ranking->setPosition(Vec2(31,15));
		cell->addChild(l_ranking);
	}
	else
	{
		auto l_ranking = Label::createWithTTF(s_ranking,APP_FONT_NAME,18);
		l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		l_ranking->setPosition(Vec2(31,15));
		l_ranking->setColor(colorFont);
		cell->addChild(l_ranking);
	}

	//����
	auto l_name = Label::createWithTTF(curPartner->role().name().c_str(),APP_FONT_NAME,18);
	l_name->setAnchorPoint(Vec2(0.0f,0.5f));
	l_name->setPosition(Vec2(90,15));
	l_name->setColor(colorFont);
	cell->addChild(l_name);

	int countryId = curPartner->role().countryid();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		auto countrySp_ = Sprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(Vec2(1.0,0.5f));
		//countrySp_->setPosition(Vec2(l_name->getPositionX() - l_name->getContentSize().width/2-1,l_name->getPositionY()));
		countrySp_->setPosition(Vec2(l_name->getPositionX()-1,l_name->getPositionY()));
		countrySp_->setScale(0.7f);
		cell->addChild(countrySp_);
	}

	//vipInfo
	// vipֿ��ؿ��
	bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
	if (vipEnabled)
	{
		if (curPartner->role().viplevel() > 0)
		{
			auto pNode = MainScene::addVipInfoByLevelForNode(curPartner->role().viplevel());
			if (pNode)
			{
				cell->addChild(pNode);
				//pNode->setPosition(Vec2(l_name->getPosition().x+l_name->getContentSize().width/2+13,l_name->getPosition().y));
				pNode->setPosition(Vec2(l_name->getPosition().x+l_name->getContentSize().width+13,l_name->getPosition().y));
			}
		}
	}

	//�ְҵ
	auto l_profession = Label::createWithTTF(BasePlayer::getProfessionNameIdxByIndex(curPartner->role().profession()).c_str(),APP_FONT_NAME,18);
	l_profession->setAnchorPoint(Vec2(0.5f,0.5f));
	l_profession->setPosition(Vec2(225,15));
	l_profession->setColor(colorFont);
	cell->addChild(l_profession);
	//�ȼ�
	char s_lv[20];
	sprintf(s_lv,"%d",curPartner->role().level());
	auto l_lv = Label::createWithTTF(s_lv,APP_FONT_NAME,18);
	l_lv->setAnchorPoint(Vec2(0.5f,0.5f));
	l_lv->setPosition(Vec2(291,15));
	l_lv->setColor(colorFont);
	cell->addChild(l_lv);
	//ս���
	char s_fightPoint[20];
	sprintf(s_fightPoint,"%d",curPartner->role().fightpoint());
	auto l_fightPoint = Label::createWithTTF(s_fightPoint,APP_FONT_NAME,18);
	l_fightPoint->setAnchorPoint(Vec2(0.5f,0.5f));
	l_fightPoint->setPosition(Vec2(375,15));
	l_fightPoint->setColor(colorFont);
	cell->addChild(l_fightPoint);
	//״̬
	std::string str_state;
	if (curPartner->online())
	{
		str_state.append(StringDataManager::getString("Arena_roleStype_online"));
	}
	else
	{
		str_state.append(StringDataManager::getString("Arena_roleStype_notOnline"));
	}
	auto l_state = Label::createWithTTF(str_state.c_str(),APP_FONT_NAME,18);
	l_state->setAnchorPoint(Vec2(0.5f,0.5f));
	l_state->setPosition(Vec2(470,15));
	l_state->setColor(colorFont);
	cell->addChild(l_state);


	if (this->selectCellId == idx)
	{
		pHighlightSpr->setVisible(true);
	}

	if (getIsCanReq())
	{
		if(idx == curRankingList.size()-4)
		{
			if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
			{
				//req for next
				this->isReqNewly = false;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(3002,(void*)(this->s_mCurPage+1),(void*)20);
				setIsCanReq(false);
			}
		}
	}


	return cell;
}

ssize_t RankListUI::numberOfCellsInTableView( TableView *table )
{
	return curRankingList.size();
}

void RankListUI::RefreshData()
{
	m_tableView->reloadData();
}

void RankListUI::MakeFriendEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (curRankingList.size()>0)
		{
			long long roleId = curRankingList.at(selectCellId)->role().roleid();
			if (GameView::getInstance()->isOwn(roleId))
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("OffLineArenaUI_RankUI_canNotMakeFriendSelf"));
			}
			else
			{
				std::string roleName = curRankingList.at(selectCellId)->role().name();
				TargetInfo::FriendStruct friend1 = { 0,0,roleId,roleName };
				GameMessageProcessor::sharedMsgProcessor()->sendReq(2200, &friend1);
			}

		}
		else
		{

		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RankListUI::LookOverEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (curRankingList.size()>0)
		{
			long long roleId = curRankingList.at(selectCellId)->role().roleid();
			if (GameView::getInstance()->isOwn(roleId))
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("OffLineArenaUI_RankUI_canNotLookOverSelf"));
			}
			else
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1142, (void *)roleId);
			}

		}
		else
		{

		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RankListUI::RefreshDataWithOutChangeOffSet()
{
	Vec2 _s = m_tableView->getContentOffset();
	int _h = m_tableView->getContentSize().height + _s.y;
	m_tableView->reloadData();
	Vec2 temp = Vec2(_s.x,_h - m_tableView->getContentSize().height);
	m_tableView->setContentOffset(temp); 
}

cocos2d::Color3B RankListUI::getFontColor( int nRank )
{
	// Ĭ�
	Color3B colorFont = Color3B(47, 93, 13);
	if (1 == nRank)
	{
		colorFont = Color3B(155, 84, 0);
	}
	else if (2 == nRank)
	{
		colorFont = Color3B(169, 2, 155);
	}
	else if(3 == nRank)
	{
		colorFont = Color3B(0, 64, 227);
	}

	return colorFont;
}
