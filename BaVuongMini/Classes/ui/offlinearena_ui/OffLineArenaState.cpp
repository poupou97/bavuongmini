#include "OffLineArenaState.h"

int OffLineArenaState::s_status = OffLineArenaState::status_none;
int OffLineArenaState::s_currentOpponent = -1;
long long OffLineArenaState::m_nStartTime = 0.0f;
bool OffLineArenaState::m_bIsCanTouch = false;

OffLineArenaState::OffLineArenaState()
{
}

OffLineArenaState::~OffLineArenaState()
{
}

void OffLineArenaState::init()
{
	setStatus(OffLineArenaState::status_none);
	setCurrentOpppnentId(-1);
}

void OffLineArenaState::setStatus(int status)
{
	s_status = status;
}
int OffLineArenaState::getStatus()
{
	return s_status;
}

void OffLineArenaState::setCurrentOpppnentId(int id)
{
	s_currentOpponent = id;
}
int OffLineArenaState::getCurrentOpppnentId()
{
	return s_currentOpponent;
}

void OffLineArenaState::setStartTime( long long time )
{
	m_nStartTime = time;
}

int OffLineArenaState::getStartTime()
{
	return m_nStartTime;
}

void OffLineArenaState::setIsCanTouch( bool isCan )
{
	m_bIsCanTouch = isCan;
}

int OffLineArenaState::getIsCanTouch()
{
	return m_bIsCanTouch;
}
