
#ifndef _OFFLINEARENAUI_HONORSHOPITEMINFO_H_
#define _OFFLINEARENAUI_HONORSHOPITEMINFO_H_

#include "../backpackscene/GoodsItemInfoBase.h"

class GoodsInfo;
class CHonorCommodity;

class HonorShopItemInfo : public GoodsItemInfoBase
{
public:
	HonorShopItemInfo();
	~HonorShopItemInfo();

	static HonorShopItemInfo * create(CHonorCommodity * honorCommodity);
	bool init(CHonorCommodity * honorCommodity);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
};

#endif
