

#ifndef _OFFLINEARENA_OFFLINEARENARESULTUI_
#define _OFFLINEARENA_OFFLINEARENARESULTUI_

#include "../extensions/UIScene.h"
#include "cocostudio\CCArmature.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class OffLineArenaResultUI : public UIScene
{
public:
	OffLineArenaResultUI();
	~OffLineArenaResultUI();

	static OffLineArenaResultUI * create(bool isWin,int exp);
	bool init(bool isWin,int exp);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void GetRewardEvent(Ref *pSender, Widget::TouchEventType type);
	void FightAgainEvent(Ref *pSender);

	virtual void update(float dt);

	void ArmatureFinishCallback(cocostudio::Armature *armature, cocostudio::MovementEventType movementType, const char *movementID);

protected:
	void exitArena();

	//ʤ���ʧ�ܶ���
	void createAnimation();
	//��ʾ��α仯
	void showRankingAnimation();
	void createUI();

	void showUIAnimation();
	void startSlowMotion();

	void addLightAnimation();

public:
	void endSlowMotion();

private:
	Layout * mainPanel;
	Layer * u_layer;
	Text *l_remainTime;
	int m_nRemainTime;

	bool m_bIsWin;
	int m_nExp;

	long long countDown_startTime;
};

#endif

