
#ifndef _OFFLINEARENA_OFFLINEARENAUI_
#define _OFFLINEARENA_OFFLINEARENAUI_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/ScriptHandlerProtocol.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CHonorCommodity;
class CPartner;
class ArenaRoleInfo;

#define KTagRankListUI 400
#define KTagFightNotesUI 410
#define KTagHonorShopUI 420
#define KTagBuyFightTimesUI 430
#define ArenaRoleInfo_BaseTag 100

class OffLineArenaUI : public UIScene
{
public:
	OffLineArenaUI();
	~OffLineArenaUI();

	struct ReqForChallenge{
		long long myRanking;
		long long targetId;
		long long targetRanking;
	};

	static OffLineArenaUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void RankListEvent(Ref *pSender, Widget::TouchEventType type);
	void HonorShopEvent(Ref *pSender, Widget::TouchEventType type);
	void FightNotesEvent(Ref *pSender, Widget::TouchEventType type);
	void ChallengeEvent(Ref *pSender, Widget::TouchEventType type);
	void BuyNumEvent(Ref *pSender, Widget::TouchEventType type);
	void GetRewardsEvent(Ref *pSender, Widget::TouchEventType type);

	void setRemaintimes(long long  _value);
	int getAllFreeFightTimes();
	int getRemainBuyTimes();

	void setCurSelectIndex(int _value);

	//ˢ���ҵ����
	void RefreshMyRankingConfig(int ranking);
	int getMyRoleRankValue();

	//�ˢ�����������ʾ
	void RefreshArenaDisplay(std::vector<CPartner *> arenaList);

	//ˢ������ֵ��ʾ
	void RefreshHonorConfig(int honorValue,long long remainTime);
	//ˢ��ս��������ʾ
	void RefreshFightConfig(int fightTimes,int allFreeTimes,int remainBuyTimes);

	//ˢ���Ƿ�ѡ�
	void RefreshSelectState();


	//н�ѧ
	virtual void registerScriptCommand(int scriptId);
	//ѡһ����
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	//���ս��ť
	void addCCTutorialIndicator2(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();

	ArenaRoleInfo * getTutorialRoleItem();
	int getTutorialRoleItemIdx();

private:
	virtual void update(float dt);

	std::string timeFormatToString(long long t);

public:
	Button * btn_challenge;
private:
	Layer * u_layer;
	Layout * ppanel;

	//��ȡ���
	Button * btn_getRewards;
	//��ҵ����
	ImageView * imageView_di;
	ImageView * imageView_ming;
	Text * l_roleRankingValue;
	int m_nRoleRanking;
	//��ҵ����
	Text * l_roleHonorValue;
	int m_nRoleHonor;
	//�ÿһ��ʱ���������ֵ
	Text * l_honorValue;
	int m_nHonor;
	//ʣ����ȡ����ֵʱ�
	Text * l_remainTimeDes;
	Text * l_remainTime;
	long long  m_nRemaintimes;

	//��������ս���
	int  m_nAllFreeFighttimes;
	//�ʣ����ս���
	int  m_nFighttimes;
	//�ʣ�๺����
	int m_nRemainBuytimes;
	Text * l_fightTime;
	ImageView * imageView_remainTime;

	//�ǰѡ�е�λ�
	int m_nCurSelectIndex;

	
public:
	std::vector<CPartner *> curArenaList;
	//������̵��б
	std::vector<CHonorCommodity*> curHonorCommodityList;
    //����а
	//std::vector<CPartner*> curRankingList;
	
	//��ѧ
	int mTutorialScriptInstanceId;


};


////////////////////////////������Ϣ//////////////////////////////
class ArenaRoleInfo : public UIScene
{
public:
	ArenaRoleInfo();
	~ArenaRoleInfo();

	static ArenaRoleInfo * create(CPartner * partner,int index);
	bool init(CPartner * partner,int index);

	void LookOverEvent(Ref *pSender, Widget::TouchEventType type);
	void BigFrameEvent(Ref *pSender, Widget::TouchEventType type);

	void selected();
	void unSelected();

	int getCurIndex();

private:
	CPartner * curPartner;
	int m_nIndex;

	ImageView * imageView_select;
	//�
	ImageView * imageView_di;

public:
	Button * btn_lookOver;

};


#endif

