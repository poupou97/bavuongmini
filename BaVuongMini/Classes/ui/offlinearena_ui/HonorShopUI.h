
#ifndef _OFFLINEARENA_HONORSHOPUI_
#define _OFFLINEARENA_HONORSHOPUI_

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CHonorCommodity;

class HonorShopUI : public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	HonorShopUI();
	~HonorShopUI();

	static HonorShopUI * create(std::vector<CHonorCommodity*> honorCommodityList);
	bool init(std::vector<CHonorCommodity*> honorCommodityList);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	virtual void update(float dt);

private:
	std::vector<CHonorCommodity*> curHonorCommodityList;

	TableView * m_tableView;

	Text * l_honorValue;
};


//cell��еĵ��
class HonorShopCellItem : public Layer
{
public:
	HonorShopCellItem();
	~HonorShopCellItem();

	static HonorShopCellItem* create(CHonorCommodity * honorCommodity);
	bool init(CHonorCommodity * honorCommodity);

	void DetailEvent(Ref * pSender);
	void BuyEvent(Ref *pSender);

private:
	CHonorCommodity * curHonorCommodity;

public:
	TableView *m_tableView;
};


#endif

