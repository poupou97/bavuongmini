#include "HonorShopItemInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/element/CHonorCommodity.h"


HonorShopItemInfo::HonorShopItemInfo()
{
}


HonorShopItemInfo::~HonorShopItemInfo()
{
}

HonorShopItemInfo * HonorShopItemInfo::create( CHonorCommodity * honorCommodity )
{
	auto honorShopItemInfo = new HonorShopItemInfo();
	if (honorShopItemInfo && honorShopItemInfo->init(honorCommodity))
	{
		honorShopItemInfo->autorelease();
		return honorShopItemInfo;
	}
	CC_SAFE_DELETE(honorShopItemInfo);
	return NULL;
}

bool HonorShopItemInfo::init( CHonorCommodity * honorCommodity )
{
	auto goodsInfo = new GoodsInfo();
	goodsInfo->CopyFrom(honorCommodity->goods());
	if (GoodsItemInfoBase::init(goodsInfo,GameView::getInstance()->EquipListItem,0))
	{
		delete goodsInfo;
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setAnchorPoint(Vec2(0,0));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(HonorShopItemInfo::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(HonorShopItemInfo::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(HonorShopItemInfo::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(HonorShopItemInfo::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void HonorShopItemInfo::onEnter()
{
	GoodsItemInfoBase::onEnter();
	this->openAnim();
}

void HonorShopItemInfo::onExit()
{
	GoodsItemInfoBase::onExit();
}

bool HonorShopItemInfo::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return resignFirstResponder(pTouch,this,true);
}

void HonorShopItemInfo::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void HonorShopItemInfo::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void HonorShopItemInfo::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

