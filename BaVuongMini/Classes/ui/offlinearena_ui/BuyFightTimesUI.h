
#ifndef _OFFLINEARENA_BUYFIGHTTIMESUI_H_
#define _OFFLINEARENA_BUYFIGHTTIMESUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class BuyFightTimesUI : public UIScene
{
public:
	BuyFightTimesUI();
	~BuyFightTimesUI();

	static BuyFightTimesUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void BuyOneTimesEvent(Ref *pSender, Widget::TouchEventType type);
	void BuyFiveTimesEvent(Ref *pSender, Widget::TouchEventType type);
	void BuyTenTimesEvent(Ref *pSender, Widget::TouchEventType type);

	virtual void update(float dt);

	//ʣ�๺����
	Text * l_remainTime;

};

#endif

