#include "OffLineArenaUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameScene.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "RankListUI.h"
#include "HonorShopUI.h"
#include "FightNotesUI.h"
#include "../../messageclient/element/CHonorCommodity.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CPartner.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../generals_ui/GeneralsUI.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../messageclient/element/CPartner.h"
#include "BuyFightTimesUI.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../../gamescene_state/role/GameActorAnimation.h"
#include "../../messageclient/element/CActiveRole.h"
#include "../../utils/GameConfig.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTeachingGuide.h"

OffLineArenaUI::OffLineArenaUI():
m_nCurSelectIndex(-1),
m_nRemainBuytimes(0)
{
}


OffLineArenaUI::~OffLineArenaUI()
{
	std::vector<CPartner*>::iterator iter_parther;
	for (iter_parther = curArenaList.begin(); iter_parther != curArenaList.end(); ++iter_parther)
	{
		delete *iter_parther;
	}
	curArenaList.clear();

	std::vector<CHonorCommodity*>::iterator iter;
	for (iter = curHonorCommodityList.begin(); iter != curHonorCommodityList.end(); ++iter)
	{
		delete *iter;
	}
	curHonorCommodityList.clear();
}

OffLineArenaUI * OffLineArenaUI::create()
{
	auto offLineArenaUI = new OffLineArenaUI();
	if(offLineArenaUI && offLineArenaUI->init())
	{
		offLineArenaUI->autorelease();
		return offLineArenaUI;
	}
	CC_SAFE_DELETE(offLineArenaUI);
	return NULL;
}

bool OffLineArenaUI::init()
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(800, 480));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		//���UI
		if(LoadSceneLayer::OffLineArenaLayer->getParent() != NULL)
		{
			LoadSceneLayer::OffLineArenaLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::OffLineArenaLayer;
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->getVirtualRenderer()->setContentSize(Size(800, 480));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		const char * firstStr = StringDataManager::getString("UIName_jing");
		const char * secondStr = StringDataManager::getString("UIName_ji");
		const char * thirdStr = StringDataManager::getString("UIName_chang");
		auto atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		u_layer->addChild(atmature);

		//عرհ�ť
		auto btn_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnabled(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(OffLineArenaUI::CloseEvent, this));
		//���а
		auto btn_rankList = (Button*)Helper::seekWidgetByName(ppanel,"Button_ranking");
		btn_rankList->setTouchEnabled(true);
		btn_rankList->setPressedActionEnabled(true);
		btn_rankList->addTouchEventListener(CC_CALLBACK_2(OffLineArenaUI::RankListEvent, this));
		//������̵
		auto btn_honorShop = (Button*)Helper::seekWidgetByName(ppanel,"Button_honorShop");
		btn_honorShop->setTouchEnabled(true);
		btn_honorShop->setPressedActionEnabled(true);
		btn_honorShop->addTouchEventListener(CC_CALLBACK_2(OffLineArenaUI::HonorShopEvent, this));
		//�ս����¼
		auto btn_fightNotes = (Button*)Helper::seekWidgetByName(ppanel,"Button_fightNotes");
		btn_fightNotes->setTouchEnabled(true);
		btn_fightNotes->setPressedActionEnabled(true);
		btn_fightNotes->addTouchEventListener(CC_CALLBACK_2(OffLineArenaUI::FightNotesEvent, this));
		//��ս
		btn_challenge = (Button*)Helper::seekWidgetByName(ppanel,"Button_dare");
		btn_challenge->setTouchEnabled(true);
		btn_challenge->setPressedActionEnabled(true);
		btn_challenge->addTouchEventListener(CC_CALLBACK_2(OffLineArenaUI::ChallengeEvent, this));
		//������
		auto btn_buyNum = (Button*)Helper::seekWidgetByName(ppanel,"Button_buyNumber");
		btn_buyNum->setTouchEnabled(true);
		btn_buyNum->setPressedActionEnabled(true);
		btn_buyNum->addTouchEventListener(CC_CALLBACK_2(OffLineArenaUI::BuyNumEvent, this));
		//���ȡ���
		btn_getRewards = (Button*)Helper::seekWidgetByName(ppanel,"Button_takeReward");
		btn_getRewards->setTouchEnabled(true);
		btn_getRewards->setPressedActionEnabled(true);
		btn_getRewards->addTouchEventListener(CC_CALLBACK_2(OffLineArenaUI::GetRewardsEvent, this));
		btn_getRewards->setVisible(false);

		imageView_di = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_di");
		imageView_ming = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_ming");
        l_roleRankingValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_roleRankValue");
		l_roleHonorValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_roleHonorValue");
		m_nRoleHonor = GameView::getInstance()->myplayer->player->honorpoint();
		l_honorValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_honor");
		//�ʣ����ȡ����ֵʱ�
		l_remainTimeDes = (Text*)Helper::seekWidgetByName(ppanel,"Label_93");
		l_remainTime = (Text*)Helper::seekWidgetByName(ppanel,"Label_time");

		l_fightTime = (Text*)Helper::seekWidgetByName(ppanel,"Label_fightTimesValue");
		imageView_remainTime = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_remainTimes");

		//send req for arenaList
		GameMessageProcessor::sharedMsgProcessor()->sendReq(3000);
		//send req for honorShopList
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1706);

		this->schedule(schedule_selector(OffLineArenaUI::update),1.0f);

		this->setContentSize(winsize);
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(OffLineArenaUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(OffLineArenaUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(OffLineArenaUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(OffLineArenaUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void OffLineArenaUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void OffLineArenaUI::onExit()
{
	UIScene::onExit();
}

bool OffLineArenaUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void OffLineArenaUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void OffLineArenaUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void OffLineArenaUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void OffLineArenaUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void OffLineArenaUI::update( float dt )
{
	m_nRemaintimes -= 1000;
	//�ʣ����ȡ����ֵʱ�
	l_remainTime->setString(timeFormatToString(m_nRemaintimes/1000).c_str());

	if (m_nRemaintimes < 0)
	{
		//������ȡ�
		btn_getRewards->setVisible(true);
		l_remainTimeDes->setVisible(false);
		l_remainTime->setVisible(false);
	}
	else
	{
		btn_getRewards->setVisible(false);
		l_remainTimeDes->setVisible(true);
		l_remainTime->setVisible(true);
	}


	m_nRoleHonor = GameView::getInstance()->myplayer->player->honorpoint();
	char s_roleHonor[20];
	sprintf(s_roleHonor,"%d",m_nRoleHonor);
	l_roleHonorValue->setString(s_roleHonor);
}

void OffLineArenaUI::RankListEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagRankListUI) == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			auto rankListUI = RankListUI::create(m_nRoleRanking);
			rankListUI->setIgnoreAnchorPointForPosition(false);
			rankListUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			rankListUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			rankListUI->setTag(KTagRankListUI);
			GameView::getInstance()->getMainUIScene()->addChild(rankListUI);

			rankListUI->isReqNewly = true;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(3002, (void*)0, (void*)20);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void OffLineArenaUI::HonorShopEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagHonorShopUI) == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			auto honorShopUI = HonorShopUI::create(curHonorCommodityList);
			honorShopUI->setIgnoreAnchorPointForPosition(false);
			honorShopUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			honorShopUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			honorShopUI->setTag(KTagHonorShopUI);
			GameView::getInstance()->getMainUIScene()->addChild(honorShopUI);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void OffLineArenaUI::FightNotesEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagFightNotesUI) == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			auto fightNotesUI = FightNotesUI::create();
			fightNotesUI->setIgnoreAnchorPointForPosition(false);
			fightNotesUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			fightNotesUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			fightNotesUI->setTag(KTagFightNotesUI);
			GameView::getInstance()->getMainUIScene()->addChild(fightNotesUI);

			GameMessageProcessor::sharedMsgProcessor()->sendReq(3004);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void OffLineArenaUI::ChallengeEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (m_nCurSelectIndex == -1)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("OffLineArenaUI_PleaseSelectOne"));
		}
		else
		{
			auto curPartner = curArenaList.at(m_nCurSelectIndex);
			if (!curPartner)
				return;

			auto temp = new ReqForChallenge();
			temp->myRanking = m_nRoleRanking;
			temp->targetId = curPartner->role().rolebase().roleid();
			temp->targetRanking = curPartner->ranking();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(3003, temp);

			auto sc = ScriptManager::getInstance()->getScriptById(this->mTutorialScriptInstanceId);
			if (sc != NULL)
				sc->endCommand(pSender);

			this->closeAnim();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void OffLineArenaUI::BuyNumEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagBuyFightTimesUI) == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			auto buyFightTimesUI = BuyFightTimesUI::create();
			buyFightTimesUI->setIgnoreAnchorPointForPosition(false);
			buyFightTimesUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			buyFightTimesUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			buyFightTimesUI->setTag(KTagBuyFightTimesUI);
			GameView::getInstance()->getMainUIScene()->addChild(buyFightTimesUI);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void OffLineArenaUI::GetRewardsEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(3001);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void OffLineArenaUI::setRemaintimes( long long _value )
{
	m_nRemaintimes = _value;
}

int OffLineArenaUI::getAllFreeFightTimes()
{
	return m_nAllFreeFighttimes;
}

int OffLineArenaUI::getRemainBuyTimes()
{
	return m_nRemainBuytimes;
}

void OffLineArenaUI::setCurSelectIndex(int _value)
{
	m_nCurSelectIndex = _value;
}

void OffLineArenaUI::RefreshMyRankingConfig(int ranking)
{
	m_nRoleRanking = ranking;
	char s_roleRanking [20];
	sprintf(s_roleRanking,"%d",m_nRoleRanking);
	l_roleRankingValue->setString(s_roleRanking);

	l_roleRankingValue->setPosition(Vec2(imageView_di->getPosition().x+imageView_di->getContentSize().width/2*imageView_di->getScale()+l_roleRankingValue->getContentSize().width/2+2,imageView_di->getPosition().y));
	imageView_ming->setPosition(Vec2(l_roleRankingValue->getPosition().x+l_roleRankingValue->getContentSize().width/2+imageView_ming->getContentSize().width/2*imageView_ming->getScale()-1,l_roleRankingValue->getPosition().y));
}

int OffLineArenaUI::getMyRoleRankValue()
{
	return m_nRoleRanking;
}

void OffLineArenaUI::RefreshArenaDisplay( std::vector<CPartner *> arenaList )
{
	//refresh data
	//delete old data
	std::vector<CPartner*>::iterator iter_parther;
	for (iter_parther = curArenaList.begin(); iter_parther != curArenaList.end(); ++iter_parther)
	{
		delete *iter_parther;
	}
	curArenaList.clear();
	//add new data
	for(int i =0;i<arenaList.size();++i)
	{
		auto partner = new CPartner();
		partner->CopyFrom(*arenaList.at(i));
		curArenaList.push_back(partner);
	}

	int max = 0;
	if(arenaList.size() <= 5)
	{
		max = arenaList.size();
	}
	else
	{
		max = 5;
	}
	for (int i = 0;i<max;++i)
	{
		if (u_layer->getChildByTag(ArenaRoleInfo_BaseTag+i))
		{
			(u_layer->getChildByTag(ArenaRoleInfo_BaseTag+i))->removeFromParent();
		}

		auto arenaRoleInfo = ArenaRoleInfo::create(arenaList.at(i),i);
		arenaRoleInfo->setAnchorPoint(Vec2(0,0));
		arenaRoleInfo->setPosition(Vec2(617-139*i,96));//(61+139*i,96)
		arenaRoleInfo->setTag(ArenaRoleInfo_BaseTag+i);
		u_layer->addChild(arenaRoleInfo);

		if (GameView::getInstance()->isOwn(arenaList.at(i)->role().rolebase().roleid()))
		{
			RefreshMyRankingConfig(arenaList.at(i)->ranking());
		}
	}
}

void OffLineArenaUI::RefreshHonorConfig( int honorValue,long long remainTime )
{
	m_nHonor = honorValue;
	m_nRemaintimes = remainTime;
	//�ÿһ��ʱ���������ֵ
	char s_honorValue[20];
	sprintf(s_honorValue,"%d",m_nHonor);
	l_honorValue->setString(s_honorValue);
	//ʣ����ȡ����ֵʱ�
	l_remainTime->setString(timeFormatToString(m_nRemaintimes/1000).c_str());

}

void OffLineArenaUI::RefreshFightConfig( int fightTimes,int allFreeTimes,int remainBuyTimes )
{
	m_nAllFreeFighttimes = allFreeTimes;
	m_nRemainBuytimes = remainBuyTimes;
	m_nFighttimes = allFreeTimes-fightTimes;
	//�ʣ����ս���
	std::string str_fightTimes;
	char s_remainFightTimesValue[5];
	sprintf(s_remainFightTimesValue,"%d",m_nFighttimes);
	str_fightTimes.append(s_remainFightTimesValue);
	str_fightTimes.append("/");
	char s_allFightTimesValue[5];
	sprintf(s_allFightTimesValue,"%d",m_nAllFreeFighttimes);
	str_fightTimes.append(s_allFightTimesValue);
	l_fightTime->setString(str_fightTimes.c_str());

	float scale_remain = m_nFighttimes*1.0f/allFreeTimes;
	if (scale_remain > 1.0f)
		scale_remain = 1.0f;

	imageView_remainTime->setTextureRect(Rect(0,0,imageView_remainTime->getContentSize().width*scale_remain,imageView_remainTime->getContentSize().height));


	if (m_nAllFreeFighttimes - m_nFighttimes <= 0)
	{

	}
}

void OffLineArenaUI::RefreshSelectState()
{
	for (int i = 0;i<5;++i)
	{
		if (u_layer->getChildByTag(ArenaRoleInfo_BaseTag+i))
		{
			auto arenaRoleInfo = (ArenaRoleInfo*)u_layer->getChildByTag(ArenaRoleInfo_BaseTag+i);
			if (arenaRoleInfo->getCurIndex() != m_nCurSelectIndex)
			{
				arenaRoleInfo->unSelected();
			}
		}
	}

	for (int i = 0;i<5;++i)
	{
		if (u_layer->getChildByTag(ArenaRoleInfo_BaseTag+i))
		{
			auto arenaRoleInfo = (ArenaRoleInfo*)u_layer->getChildByTag(ArenaRoleInfo_BaseTag+i);
			if (arenaRoleInfo->getCurIndex() == m_nCurSelectIndex)
			{
				arenaRoleInfo->selected();
			}
		}
	}
}

std::string OffLineArenaUI::timeFormatToString( long long t )
{
	int int_h = t/60/60;
	int int_m = (t%3600)/60;
	int int_s = (t%3600)%60;

	std::string timeString = "";
	char str_h[10];
	sprintf(str_h,"%d",int_h);
	//timeString = str_h;
	//timeString.append(":");
	if (int_h <= 0)
	{

	}
	else if (int_h > 0 && int_h < 10)
	{
		timeString.append("0");
		timeString.append(str_h);
		timeString.append(":");
	}
	else
	{
		timeString.append(str_h);
		timeString.append(":");
	}

	char str_m[10];
	sprintf(str_m,"%d",int_m);
	//timeString.append(str_m);
	//timeString.append(":");
	if (int_m >= 0 && int_m < 10)
	{
		timeString.append("0");
		timeString.append(str_m);
	}
	else
	{
		timeString.append(str_m);
	}
	timeString.append(":");

	char str_s[10];
	sprintf(str_s,"%d",int_s);
	//timeString.append(str_s);
	if (int_s >= 0 && int_s < 10)
	{
		timeString.append("0");
		timeString.append(str_s);
	}
	else
	{
		timeString.append(str_s);
	}

	return timeString;
}

void OffLineArenaUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void OffLineArenaUI::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,80,60);
// 	if (direction == CCTutorialIndicator::Direction_LD)
// 	{
// 		tutorialIndicator->setPosition(Vec2(pos.x+130+_w,pos.y+200+_h));
// 	}
// 	else if (direction == CCTutorialIndicator::Direction_RD)
// 	{
// 		tutorialIndicator->setPosition(Vec2(pos.x+30+_w,pos.y+200+_h));
// 	}
// 	
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,136,243,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+136/2+_w,pos.y+243/2+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void OffLineArenaUI::addCCTutorialIndicator2( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,70,50);
// 	tutorialIndicator->setPosition(Vec2(pos.x+60+_w,pos.y+80+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,135,51,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void OffLineArenaUI::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

ArenaRoleInfo * OffLineArenaUI::getTutorialRoleItem()
{
	//�������0-4󣨣�
	if (m_nRoleRanking == 1)
		return NULL;

	int willSelectIdx = -1;
	for(int i =0;i<curArenaList.size();++i)
	{
		auto partner = curArenaList.at(i);
		if (GameView::getInstance()->isOwn(partner->role().rolebase().roleid()))
		{
			willSelectIdx = i-1;
			break;
		}
	}

	if (willSelectIdx < 0)
		return NULL;

	auto arenaRoleInfo = (ArenaRoleInfo*)(u_layer->getChildByTag(ArenaRoleInfo_BaseTag+willSelectIdx));
	if (arenaRoleInfo)
	{
		return arenaRoleInfo;		
	}
	else
	{
		return NULL;
	}
}

int OffLineArenaUI::getTutorialRoleItemIdx()
{
	//�������0-4󣨣�
	if (m_nRoleRanking == 1)
		return -1;

	int willSelectIdx = -1;
	for(int i =0;i<curArenaList.size();++i)
	{
		auto partner = curArenaList.at(i);
		if (GameView::getInstance()->isOwn(partner->role().rolebase().roleid()))
		{
			willSelectIdx = i-1;
			break;
		}
	}

	if (willSelectIdx < 0)
		return -1;

	return willSelectIdx;
}




//////////////////////////////////////////////////////////////////

#define  kTagRotateAnm 777

ArenaRoleInfo::ArenaRoleInfo():
m_nIndex(-1)
{

}

ArenaRoleInfo::~ArenaRoleInfo()
{
	delete curPartner;
}

ArenaRoleInfo * ArenaRoleInfo::create(CPartner * partner,int index)
{
	auto arenaRoleInfo = new ArenaRoleInfo();
	if (arenaRoleInfo && arenaRoleInfo->init(partner,index))
	{
		arenaRoleInfo->autorelease();
		return arenaRoleInfo;
	}
	CC_SAFE_DELETE(arenaRoleInfo);
	return NULL;
}

bool ArenaRoleInfo::init(CPartner * partner,int index)
{
	if (UIScene::init())
	{
		m_nIndex = index;
		curPartner = new CPartner();
		curPartner->CopyFrom(*partner);

		//�߿
		auto btn_bigFrame = Button::create();
		btn_bigFrame->loadTextures("res_ui/no3_di.png","res_ui/no3_di.png","");
		btn_bigFrame->setScale9Enabled(true);
		btn_bigFrame->setContentSize(Size(136,243));
		btn_bigFrame->setCapInsets(Rect(13,13,1,1));
		btn_bigFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_bigFrame->setPosition(Vec2(136/2,245/2));
		btn_bigFrame->addTouchEventListener(CC_CALLBACK_2(ArenaRoleInfo::BigFrameEvent, this));
		btn_bigFrame->setTouchEnabled(true);
		m_pLayer->addChild(btn_bigFrame);

		//�
		imageView_di = ImageView::create();
		imageView_di->loadTexture("res_ui/arena/light_long.png");
		imageView_di->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_di->setPosition(Vec2(69,155));
		m_pLayer->addChild(imageView_di);
		//����
		auto l_name = Label::createWithTTF(partner->role().rolebase().name().c_str(), APP_FONT_NAME, 18);
		l_name->setAnchorPoint(Vec2(0.5f,0.5f));
		l_name->setPosition(Vec2(69,225));
		m_pLayer->addChild(l_name);

		//ֽ�ɫ����
		if (partner->has_role())
		{
			auto activeRole = new CActiveRole();
			activeRole->CopyFrom(partner->role());

			//vipInfo
			// vip���ؿ��
			bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
			if (vipEnabled)
			{
				if (activeRole->playerbaseinfo().viplevel() > 0)
				{
					l_name->setPosition(Vec2(57,225));

					auto widget = MainScene::addVipInfoByLevelForWidget(activeRole->playerbaseinfo().viplevel());
					m_pLayer->addChild(widget);
					widget->setPosition(Vec2(l_name->getPosition().x+l_name->getContentSize().width/2+10,l_name->getPosition().y));
				}
			}

			std::string roleFigureName = ActorUtils::getActorFigureName(*activeRole);
			std::string roleWeaponEffectForRefine = "";
			std::string roleWeaponEffectForStar = "";
			for (int i = 0;i<activeRole->equipmentshowinfo().size();++i)
			{
				if (activeRole->equipmentshowinfo(i).clazz() == 1)
				{
					int profession_ = BasePlayer::getProfessionIdxByName(activeRole->profession());
					int equipRefineLevel_ = activeRole->equipmentshowinfo(i).gradelevel();
					int equipStarLevel_ = activeRole->equipmentshowinfo(i).starlevel();

					roleWeaponEffectForRefine.append(ActorUtils::getWeapEffectByRefineLevel(profession_,equipRefineLevel_));
					roleWeaponEffectForStar.append(ActorUtils::getWeapEffectByStarLevel(profession_,equipStarLevel_));
				}
			}

			auto pAnim = ActorUtils::createActorAnimation(roleFigureName.c_str(), activeRole->hand().c_str(),
				roleWeaponEffectForRefine.c_str(), roleWeaponEffectForStar.c_str());
			pAnim->setPosition(Vec2(69,105));
			addChild(pAnim);
			delete activeRole;
		}
		
		auto u_layer = Layer::create();
		addChild(u_layer);
		//����
		std::string str_ranking = StringDataManager::getString("Arena_ranking");
		char s_ranking[20];
		sprintf(s_ranking,"%d",partner->ranking());
		str_ranking.append(s_ranking);
		auto l_ranking = Label::createWithTTF(str_ranking.c_str(), APP_FONT_NAME, 16);
		l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		l_ranking->setPosition(Vec2(69,90));
		u_layer->addChild(l_ranking);
		//�ս���
		std::string str_fightPoint = StringDataManager::getString("Arena_fightPoint");
		char s_fightPointValue[20];
		sprintf(s_fightPointValue,"%d",partner->role().fightpoint());
		str_fightPoint.append(s_fightPointValue);

		auto l_fightPoint = Label::createWithTTF(str_fightPoint.c_str(), APP_FONT_NAME, 16);
		l_fightPoint->setAnchorPoint(Vec2(0.5f,0.5f));
		l_fightPoint->setPosition(Vec2(69,72));
		u_layer->addChild(l_fightPoint);
		
		//����ֵ
		std::string str_honorValue_everyTenMinutes;
		char s_honorValue[20];
		sprintf(s_honorValue,"%d",partner->honorconfig());
		str_honorValue_everyTenMinutes.append(s_honorValue);
		str_honorValue_everyTenMinutes.append(StringDataManager::getString("Arena_honorValue_everyTenMinutes"));

		auto l_honorValue = Label::createWithTTF(str_honorValue_everyTenMinutes.c_str(), APP_FONT_NAME, 16);
		l_honorValue->setAnchorPoint(Vec2(0.5f,0.5f));
		l_honorValue->setPosition(Vec2(69,54));
		u_layer->addChild(l_honorValue);

		//�鿴��ť
		btn_lookOver = Button::create();
		btn_lookOver->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		btn_lookOver->setScale9Enabled(true);
		btn_lookOver->setContentSize(Size(90,37));
		btn_lookOver->setCapInsets(Rect(18,9,2,23));
		btn_lookOver->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_lookOver->setPosition(Vec2(69,26));
		btn_lookOver->addTouchEventListener(CC_CALLBACK_2(ArenaRoleInfo::LookOverEvent, this));
		btn_lookOver->setTouchEnabled(true);
		btn_lookOver->setPressedActionEnabled(true);
		u_layer->addChild(btn_lookOver);
		btn_lookOver->setVisible(false);

		auto lookOver = Label::createWithTTF(StringDataManager::getString("family_apply_btncheck"), APP_FONT_NAME, 18);
		lookOver->setAnchorPoint(Vec2(0.5f,0.5f));
		lookOver->setPosition(Vec2(0,0));
		btn_lookOver->addChild(lookOver);

		imageView_select = ImageView::create();
		imageView_select->loadTexture("res_ui/highlight.png");
		imageView_select->setScale9Enabled(true);
		imageView_select->setContentSize(Size(136,243));
		imageView_select->setCapInsets(Rect(25,25,1,1));
		imageView_select->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_select->setPosition(Vec2(69,245/2));
		m_pLayer->addChild(imageView_select);
		imageView_select->setVisible(false);

		if (GameView::getInstance()->isOwn(partner->role().rolebase().roleid()))
		{
			imageView_di->loadTexture("res_ui/arena/light_long_blue.png");
			l_name->setColor(Color3B(0,255,255));
			l_ranking->setColor(Color3B(0,255,255));
			l_fightPoint->setColor(Color3B(0,255,255));
			l_honorValue->setColor(Color3B(0,255,255));
		}
		else
		{
			imageView_di->loadTexture("res_ui/arena/light_long.png");
			l_name->setColor(Color3B(0,255,0));
			l_ranking->setColor(Color3B(0,255,0));
			l_fightPoint->setColor(Color3B(0,255,0));
			l_honorValue->setColor(Color3B(0,255,0));
		}

		this->setContentSize(Size(138,245));
		return true;
	}
	return false;
}

void ArenaRoleInfo::LookOverEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1142, (void*)curPartner->role().rolebase().roleid());
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void ArenaRoleInfo::BigFrameEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto offLineAreaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
		if (!offLineAreaUI)
			return;

		//���õ�ǰѡ�е�λ�
		offLineAreaUI->setCurSelectIndex(m_nIndex);

		offLineAreaUI->RefreshSelectState();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void ArenaRoleInfo::selected()
{
	if (GameView::getInstance()->isOwn(curPartner->role().rolebase().roleid()))
	{
		btn_lookOver->setVisible(false);
	}
	else
	{
		btn_lookOver->setVisible(true);
	}
	
	imageView_select->setVisible(true);

	//test by yangjun 2014.3.29
	auto rotateAnm = RotateBy::create(6.0f,360);
	auto repeatAnm = RepeatForever::create(Sequence::create(rotateAnm,NULL));
	repeatAnm->setTag(kTagRotateAnm);
	imageView_di->runAction(repeatAnm);

	auto offLineArenaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
	auto sc = ScriptManager::getInstance()->getScriptById(offLineArenaUI->mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(this);
}

void ArenaRoleInfo::unSelected()
{
	btn_lookOver->setVisible(false);
	imageView_select->setVisible(false);

	//test by yangjun 2014.3.29
	if (imageView_di->getActionByTag(kTagRotateAnm) != NULL)
	{
		imageView_di->stopAllActions();
		//imageView_di->getActionManager()->removeAllActions();
		imageView_di->setRotation(0);
	}
}

int ArenaRoleInfo::getCurIndex()
{
	return m_nIndex;
}
