#include "HonorShopUI.h"
#include "../extensions/UIScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CHonorCommodity.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/GameScene.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"
#include "GameView.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../utils/StaticDataManager.h"
#include "HonorShopItemInfo.h"
#include "HonorShopItemBuyInfo.h"


HonorShopUI::HonorShopUI()
{
}


HonorShopUI::~HonorShopUI()
{
	std::vector<CHonorCommodity*>::iterator iter;
	for (iter = curHonorCommodityList.begin(); iter != curHonorCommodityList.end(); ++iter)
	{
		delete *iter;
	}
	curHonorCommodityList.clear();
}

HonorShopUI * HonorShopUI::create(std::vector<CHonorCommodity*> honorCommodityList)
{
	auto honorShopUI = new HonorShopUI();
	if (honorShopUI && honorShopUI->init(honorCommodityList))
	{
		honorShopUI->autorelease();
		return honorShopUI;
	}
	CC_SAFE_DELETE(honorShopUI);
	return NULL;
}

bool HonorShopUI::init(std::vector<CHonorCommodity*> honorCommodityList)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate(); 
		//refresh data
		//delete old
		std::vector<CHonorCommodity*>::iterator iter;
		for (iter = curHonorCommodityList.begin(); iter != curHonorCommodityList.end(); ++iter)
		{
			delete *iter;
		}
		curHonorCommodityList.clear();
		//add new 
		for(int i = 0;i<(int)honorCommodityList.size();++i)
		{
			CHonorCommodity * honorCommodity = new CHonorCommodity();
			honorCommodity->CopyFrom(*honorCommodityList.at(i));
			curHonorCommodityList.push_back(honorCommodity);
		}

		//create UI
		//���UI
		if(LoadSceneLayer::RankListLayer->getParent() != NULL)
		{
			LoadSceneLayer::RankListLayer->removeFromParentAndCleanup(false);
		}
		auto ppanel = LoadSceneLayer::RankListLayer;
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(608, 417));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto panel_fightNotes = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_conbatRecord");
		panel_fightNotes->setVisible(false);
		auto panel_rankList = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_ranking");
		panel_rankList->setVisible(false);
		auto panel_honorShop = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_honorShop");
		panel_honorShop->setVisible(true);

		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(HonorShopUI::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);

		l_honorValue = (Text*)Helper::seekWidgetByName(panel_honorShop,"Label_honorValue");

		m_tableView = TableView::create(this,Size(498,262));
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0,0));
		m_tableView->setPosition(Vec2(52,74));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(m_tableView);

// 		Layer * u_tableViewFrameLayer = Layer::create();
// 		addChild(u_tableViewFrameLayer);
// 		ImageView * tableView_Frame = ImageView::create();
// 		tableView_Frame->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		tableView_Frame->setScale9Enabled(true);
// 		tableView_Frame->setContentSize(Size(515,277));
// 		tableView_Frame->setCapInsets(Rect(32,32,1,1));
// 		tableView_Frame->setAnchorPoint(Vec2(0.5f,0.5f));
// 		tableView_Frame->setPosition(Vec2(299+3,198+6));
// 		u_tableViewFrameLayer->addChild(tableView_Frame);

		this->setContentSize(Size(608,417));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(HonorShopUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(HonorShopUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(HonorShopUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(HonorShopUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void HonorShopUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void HonorShopUI::onExit()
{
	UIScene::onExit();
}

bool HonorShopUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void HonorShopUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void HonorShopUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void HonorShopUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void HonorShopUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void HonorShopUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void HonorShopUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void HonorShopUI::tableCellTouched( TableView* table, TableViewCell* cell )
{

}

cocos2d::Size HonorShopUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(495,70);
}

cocos2d::extension::TableViewCell* HonorShopUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	if (idx == curHonorCommodityList.size()/2)
	{
		if (curHonorCommodityList.size()%2 == 0)
		{
			auto cellItem1 = HonorShopCellItem::create(curHonorCommodityList.at(2*idx));
			cellItem1->setPosition(Vec2(5,0));
			cell->addChild(cellItem1);

			auto cellItem2 = HonorShopCellItem::create(curHonorCommodityList.at(2*idx+1));
			cellItem2->setPosition(Vec2(253,0));
			cell->addChild(cellItem2);
		}
		else
		{
			auto cellItem1 = HonorShopCellItem::create(curHonorCommodityList.at(2*idx));
			cellItem1->setPosition(Vec2(5,0));
			cell->addChild(cellItem1);
		}
	}
	else
	{
		auto cellItem1 = HonorShopCellItem::create(curHonorCommodityList.at(2*idx));
		cellItem1->setPosition(Vec2(5,0));
		cell->addChild(cellItem1);

		auto cellItem2 = HonorShopCellItem::create(curHonorCommodityList.at(2*idx+1));
		cellItem2->setPosition(Vec2(253,0));
		cell->addChild(cellItem2);
	}

	return cell;
}

ssize_t HonorShopUI::numberOfCellsInTableView( TableView *table )
{
	if (curHonorCommodityList.size()%2 == 0)
	{
		return curHonorCommodityList.size()/2;
	}
	else
	{
		return curHonorCommodityList.size()/2 + 1;
	}
}

void HonorShopUI::update( float dt )
{
	char s_honor [20];
	sprintf(s_honor,"%d",GameView::getInstance()->myplayer->player->honorpoint());
	l_honorValue->setString(s_honor);
}








//////////////////////////////////////////////////////////////////
HonorShopCellItem::HonorShopCellItem()
{
	curHonorCommodity = new CHonorCommodity();
}

HonorShopCellItem::~HonorShopCellItem()
{
	delete curHonorCommodity;
}

HonorShopCellItem* HonorShopCellItem::create( CHonorCommodity * honorCommodity )
{
	auto honorShopCellItem = new HonorShopCellItem();
	if (honorShopCellItem && honorShopCellItem->init(honorCommodity))
	{
		honorShopCellItem->autorelease();
		return honorShopCellItem;
	}
	CC_SAFE_DELETE(honorShopCellItem);
	return NULL;
}

bool HonorShopCellItem::init( CHonorCommodity * honorCommodity )
{
	if (Layer::init())
	{
		curHonorCommodity->CopyFrom(*honorCommodity);

		//ش�߿
		auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
		sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
		sprite_bigFrame->setPosition(Vec2(0, 0));
		sprite_bigFrame->setPreferredSize(Size(244,68));
		addChild(sprite_bigFrame);

// 		cocos2d::extension::Scale9Sprite * spbigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
// 		spbigFrame->setPreferredSize(Size(71,39));
// 		spbigFrame->setCapInsets(Rect(18,9,2,23));
// 		spbigFrame->setPreferredSize(Size(244,68));
// 		MenuItemSprite *itemSprite_bigFrame = MenuItemSprite::create(spbigFrame, spbigFrame, spbigFrame, this, menu_selector(HonorShopCellItem::DetailEvent));
// 		itemSprite_bigFrame->setZoomScale(1.0f);
// 		CCMoveableMenu *btn_bigFrame = CCMoveableMenu::create(itemSprite_bigFrame,NULL);
// 		btn_bigFrame->setAnchorPoint(Vec2(0,0));
// 		btn_bigFrame->setPosition(Vec2(122,34));
// 		addChild(btn_bigFrame);

		//���ƷIcon�׿
		std::string iconFramePath;
		switch(curHonorCommodity->goods().quality())
		{
		case 1 :
			{
				iconFramePath = "res_ui/smdi_white.png";
			}
			break;
		case 2 :
			{
				iconFramePath = "res_ui/smdi_green.png";
			}
			break;
		case 3 :
			{
				iconFramePath = "res_ui/smdi_bule.png";
			}
			break;
		case 4 :
			{
				iconFramePath = "res_ui/smdi_purple.png";
			}
			break;
		case 5 :
			{
				iconFramePath = "res_ui/smdi_orange.png";
			}
			break;
		}
// 		Sprite *  smaillFrame1 = Sprite::create(iconFramePath.c_str());
// 		Sprite *  smaillFrame2 = Sprite::create(iconFramePath.c_str());
// 		Sprite *  smaillFrame3 = Sprite::create(iconFramePath.c_str());
// 		MenuItemSprite *itemSprite_smallFrame = MenuItemSprite::create(smaillFrame1, smaillFrame2, smaillFrame3, this, menu_selector(HonorShopCellItem::DetailEvent));
// 		itemSprite_smallFrame->setZoomScale(1.0f);
// 		MenuItemImage* itemSprite_smallFrame = MenuItemImage::create(iconFramePath.c_str(), iconFramePath.c_str(),this, menu_selector(HonorShopCellItem::DetailEvent) );
// 		CCMoveableMenu *btn_smallFrame = CCMoveableMenu::create(itemSprite_smallFrame,NULL);
// 		btn_smallFrame->setAnchorPoint(Vec2(0,0));
// 		btn_smallFrame->setPosition(Vec2(10,10));
// 		addChild(btn_smallFrame);

// 		MenuItemImage *item_image = MenuItemImage::create(
// 		 	iconFramePath.c_str(),
// 		 	iconFramePath.c_str(),
// 		 	this,
// 		 	menu_selector(HonorShopCellItem::DetailEvent));
// 		Menu* btn_smallFrame = Menu::create(item_image, NULL);
// 		btn_smallFrame->setAnchorPoint(Vec2::ZERO);
// 		btn_smallFrame->setPosition(Vec2(0,0));
// 		addChild(btn_smallFrame);

		auto s_normalImage_1 = cocos2d::extension::Scale9Sprite::create(iconFramePath.c_str());
		s_normalImage_1->setPreferredSize(Size(47,47));
		auto firstMenuImage = MenuItemSprite::create(s_normalImage_1, s_normalImage_1, s_normalImage_1, CC_CALLBACK_1(HonorShopCellItem::DetailEvent,this));
		firstMenuImage->setScale(1.0f);
		firstMenuImage->setAnchorPoint(Vec2(0.5f,0.5f));
		auto firstMenu = CCMoveableMenu::create(firstMenuImage, NULL);
		firstMenu->setContentSize(Size(47,47));
		firstMenu->setPosition(Vec2(37,sprite_bigFrame->getContentSize().height/2+1));
		addChild(firstMenu);
		
		std::string pheadPath = "res_ui/props_icon/";
		pheadPath.append(curHonorCommodity->goods().icon().c_str());
		pheadPath.append(".png");
		auto phead = Sprite::create(pheadPath.c_str());
		phead->setIgnoreAnchorPointForPosition(false);
		phead->setAnchorPoint(Vec2(0.5f, 0.5f));
		phead->setPosition(Vec2(37, sprite_bigFrame->getContentSize().height/2+1));
		phead->setScale(0.85f);
		addChild(phead);

		auto pName = Label::createWithTTF(curHonorCommodity->goods().name().c_str(),APP_FONT_NAME,18);
		pName->setAnchorPoint(Vec2(0.5f,0.5f));
		pName->setPosition(Vec2(112,44));
		pName->setColor(GameView::getInstance()->getGoodsColorByQuality(curHonorCommodity->goods().quality()));
		addChild(pName);

		char s_honor[20];
		sprintf(s_honor,"%d",curHonorCommodity->honor());
		auto pPrice = Label::createWithTTF(s_honor,APP_FONT_NAME,16);
		pPrice->setAnchorPoint(Vec2(0.5f,0.5f));
		pPrice->setPosition(Vec2(112,25));
		addChild(pPrice);

		auto spBg = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_2.png");
		spBg->setPreferredSize(Size(71,39));
		spBg->setCapInsets(Rect(18,9,2,23));
		auto itemSprite = MenuItemSprite::create(spBg, spBg, spBg, CC_CALLBACK_1(HonorShopCellItem::BuyEvent,this));
		itemSprite->setScale(1.3f);
		auto btn_buy = CCMoveableMenu::create(itemSprite,NULL);
		btn_buy->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_buy->setPosition(Vec2(206,34));
		addChild(btn_buy);

		const char *str1 = StringDataManager::getString("goods_auction_buy");
		char* p1 =const_cast<char*>(str1);
		auto l_buy=Label::createWithTTF(p1,APP_FONT_NAME,18);
		l_buy->setAnchorPoint(Vec2(0.5f,0.5f));
		l_buy->setPosition(Vec2(71/2,39/2));
		itemSprite->addChild(l_buy);

		this->setContentSize(Size(244,68));

		return true;
	}
	return false;
}

void HonorShopCellItem::BuyEvent( Ref *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHonorShopItemBuyInfo) == NULL)
	{
 	 	Size winSize = Director::getInstance()->getVisibleSize();
		auto honorShopItemBuyInfo = HonorShopItemBuyInfo::create(curHonorCommodity);
 	 	honorShopItemBuyInfo->setIgnoreAnchorPointForPosition(false);
 	 	honorShopItemBuyInfo->setAnchorPoint(Vec2(0.5f,0.5f));
 	 	honorShopItemBuyInfo->setPosition(Vec2(winSize.width/2,winSize.height/2));
		honorShopItemBuyInfo->setTag(kTagHonorShopItemBuyInfo);
 	 	GameView::getInstance()->getMainUIScene()->addChild(honorShopItemBuyInfo);
	}
}

void HonorShopCellItem::DetailEvent( Ref * pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHonorShopItemInfo) == NULL)
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		auto honorShopItemInfo = HonorShopItemInfo::create(curHonorCommodity);
		honorShopItemInfo->setIgnoreAnchorPointForPosition(false);
		honorShopItemInfo->setAnchorPoint(Vec2(0.5f,0.5f));
		//honorShopItemInfo->setPosition(Vec2(winSize.width/2,winSize.height/2));
		honorShopItemInfo->setTag(kTagHonorShopItemInfo);
		GameView::getInstance()->getMainUIScene()->addChild(honorShopItemInfo);
	}
}
