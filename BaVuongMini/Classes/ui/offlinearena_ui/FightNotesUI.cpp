#include "FightNotesUI.h"
#include "../extensions/UIScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CHistory.h"
#include "../extensions/CCRichLabel.h"
#include "../extensions/CCMoveableMenu.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../../utils/StrUtils.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "OffLineArenaUI.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"

#define  SelectImageTag 458

FightNotesUI::FightNotesUI():
lastSelectCellId(0),
selectCellId(0)
{
}


FightNotesUI::~FightNotesUI()
{
	std::vector<CHistory*>::iterator iter;
	for (iter = curHistoryList.begin(); iter != curHistoryList.end(); ++iter)
	{
		delete *iter;
	}
	curHistoryList.clear();
}

FightNotesUI * FightNotesUI::create()
{
	auto fightNotesUI = new FightNotesUI();
	if (fightNotesUI && fightNotesUI->init())
	{
		fightNotesUI->autorelease();
		return fightNotesUI;
	}
	CC_SAFE_DELETE(fightNotesUI);
	return NULL;
}

bool FightNotesUI::init()
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate(); 

		//���UI
		if(LoadSceneLayer::RankListLayer->getParent() != NULL)
		{
			LoadSceneLayer::RankListLayer->removeFromParentAndCleanup(false);
		}
		auto ppanel = LoadSceneLayer::RankListLayer;
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(608, 417));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto panel_fightNotes = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_conbatRecord");
		panel_fightNotes->setVisible(true);
		auto panel_rankList = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_ranking");
		panel_rankList->setVisible(false);
		auto panel_honorShop = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_honorShop");
		panel_honorShop->setVisible(false);

		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(FightNotesUI::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);

		m_tableView = TableView::create(this,Size(517,290));
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0,0));
		m_tableView->setPosition(Vec2(45,40));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(m_tableView);

		this->setContentSize(Size(608,417));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(FightNotesUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(FightNotesUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(FightNotesUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(FightNotesUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void FightNotesUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FightNotesUI::onExit()
{
	UIScene::onExit();
}

bool FightNotesUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void FightNotesUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void FightNotesUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void FightNotesUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void FightNotesUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void FightNotesUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void FightNotesUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void FightNotesUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	selectCellId = cell->getIdx();
	//�ȡ���ϴ�ѡ��״̬�����±���ѡ��״̬
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();
}

cocos2d::Size FightNotesUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(511,45);
}

cocos2d::extension::TableViewCell* FightNotesUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto history = curHistoryList.at(idx);

	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(Size(515,43));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setPosition(Vec2(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	//��߿
	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(0, 0));
	sprite_bigFrame->setCapInsets(Rect(9,14,1,1));
	sprite_bigFrame->setPreferredSize(Size(515,43));
	cell->addChild(sprite_bigFrame);
	//�ս����¼��Ϣ
	RichLabelDefinition def;
	def.strokeEnabled = false;
	auto rl_des = CCRichLabel::create(this->getFightNotesDes(history).c_str(),Size(350,43),def);
	rl_des->setAnchorPoint(Vec2(0,0));
	rl_des->setPosition(Vec2(12,2));
	rl_des->setScale(0.8f);
	cell->addChild(rl_des);
	//���
	auto image_strikeBack = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_2.png");
	image_strikeBack->setPreferredSize(Size(80,36));
	image_strikeBack->setCapInsets(Rect(18,9,2,23));
	auto menuSprite_strikeBack = MenuItemSprite::create(image_strikeBack, image_strikeBack, image_strikeBack, CC_CALLBACK_1(FightNotesUI::StrikeBackEvent,this));
	menuSprite_strikeBack->setScale(1.3f);
	menuSprite_strikeBack->setAnchorPoint(Vec2(0.5f,0.5f));
	menuSprite_strikeBack->setTag(idx);
	menu_strikeBack = CCMoveableMenu::create(menuSprite_strikeBack, NULL);
	menu_strikeBack->setContentSize(Size(80,36));
	menu_strikeBack->setPosition(Vec2(382,22));
	sprite_bigFrame->addChild(menu_strikeBack);
	menu_strikeBack->setVisible(false);
	auto l_strikeBack = Label::createWithTTF(StringDataManager::getString("Arena_fightNotes_strikeBack"),APP_FONT_NAME,16);
	l_strikeBack->setAnchorPoint(Vec2(.5f,.5f));
	l_strikeBack->setPosition(Vec2(40,18));
	menuSprite_strikeBack->addChild(l_strikeBack);

	//�鿴
	auto image_lookOver = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_1.png");
	image_lookOver->setPreferredSize(Size(80,36));
	image_lookOver->setCapInsets(Rect(18,9,2,23));
	auto menuSprite_lookOver = MenuItemSprite::create(image_lookOver, image_lookOver, image_lookOver, CC_CALLBACK_1(FightNotesUI::LookOverEvent,this));
	menuSprite_lookOver->setScale(1.3f);
	menuSprite_lookOver->setAnchorPoint(Vec2(0.5f,0.5f));
	menuSprite_lookOver->setTag(idx);
	menu_lookOver = CCMoveableMenu::create(menuSprite_lookOver, NULL);
	menu_lookOver->setContentSize(Size(80,36));
	menu_lookOver->setPosition(Vec2(467,22));
	sprite_bigFrame->addChild(menu_lookOver);
	auto l_lookOver = Label::createWithTTF(StringDataManager::getString("friend_check"),APP_FONT_NAME,16);
	l_lookOver->setAnchorPoint(Vec2(.5f,.5f));
	l_lookOver->setPosition(Vec2(40,18));
	menuSprite_lookOver->addChild(l_lookOver);

	if(history->type() == PASSIVE)
	{
		if (!history->win())
		{
			menu_strikeBack->setVisible(true);
		}
		else
		{
			menu_strikeBack->setVisible(false);
		}
	}
	else
	{
		menu_strikeBack->setVisible(false);
	}

	if (this->selectCellId == idx)
	{
		pHighlightSpr->setVisible(true);
	}

	return cell;
}

ssize_t FightNotesUI::numberOfCellsInTableView( TableView *table )
{
	return curHistoryList.size();
}

void FightNotesUI::RefreshData()
{
	m_tableView->reloadData();
}

void FightNotesUI::StrikeBackEvent( Ref *pSender )
{
	auto menuSprite_lookOver = dynamic_cast<MenuItemSprite*>(pSender);
	if (!menuSprite_lookOver)
		return;

	selectCellId = menuSprite_lookOver->getTag();

	if (curHistoryList.size()>0)
	{
		long long roleId = curHistoryList.at(selectCellId)->partnerid();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(3006,(void *)roleId);

		auto offLineAreaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
		if (offLineAreaUI)
		{
			offLineAreaUI->closeAnim();
		}
		this->closeAnim();
	}
	else
	{

	}
}

void FightNotesUI::LookOverEvent( Ref *pSender )
{
	auto menuSprite_lookOver = dynamic_cast<MenuItemSprite*>(pSender);
	if (!menuSprite_lookOver)
		return;

	selectCellId = menuSprite_lookOver->getTag();

	if (curHistoryList.size()>0)
	{
		long long roleId = curHistoryList.at(selectCellId)->partnerid();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)roleId);
	}
	else
	{

	}
}

std::string FightNotesUI::getFightNotesDes( CHistory * history )
{
	std::string str_des = "";
	//���
	//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par1"));
	str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par1"),Color3B(47,93,13)).c_str());
	//2014.1.2
	//str_des.append(history->timestamp().c_str());
	str_des.append(StrUtils::applyColor(history->timestamp().c_str(),Color3B(47,93,13)).c_str());
	//���ս���Ǳ���ս
	if (history->type() == PASSIVE)
	{
		//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par2_1"));
		str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par2_1"),Color3B(47,93,13)).c_str());

		std::string str_name;
		str_name.append(StringDataManager::getString("Arena_fightNotes_des_par_char1"));
		str_name.append(history->partnername());
		str_name.append(StringDataManager::getString("Arena_fightNotes_des_par_char2"));

		str_des.append(StrUtils::applyColor(str_name.c_str(),Color3B(0,151,39)));
		//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par2_2"));
		str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par2_2"),Color3B(47,93,13)).c_str());
	}
	else
	{
		//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par2_3"));
		str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par2_3"),Color3B(47,93,13)).c_str());
		std::string str_name;
		str_name.append(StringDataManager::getString("Arena_fightNotes_des_par_char1"));
		str_name.append(history->partnername());
		str_name.append(StringDataManager::getString("Arena_fightNotes_des_par_char2"));

		str_des.append(StrUtils::applyColor(str_name.c_str(),Color3B(0,151,39)));
	}
	//��
	//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par3"));
	str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par3"),Color3B(47,93,13)).c_str());
	if (history->win())
	{
		std::string str_result;
		str_result.append(StringDataManager::getString("Arena_fightNotes_des_par_char1"));
		str_result.append(StringDataManager::getString("Arena_fightNotes_des_par3_1"));
		str_result.append(StringDataManager::getString("Arena_fightNotes_des_par_char2"));
		
		str_des.append(StrUtils::applyColor(str_result.c_str(),Color3B(184,0,0)).c_str());
	}
	else
	{
		std::string str_result;
		str_result.append(StringDataManager::getString("Arena_fightNotes_des_par_char1"));
		str_result.append(StringDataManager::getString("Arena_fightNotes_des_par3_2"));
		str_result.append(StringDataManager::getString("Arena_fightNotes_des_par_char2"));

		str_des.append(StrUtils::applyColor(str_result.c_str(),Color3B(60,65,56)).c_str());
	}
	//����
	//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par4"));
	str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par4"),Color3B(47,93,13)).c_str());
	if (history->type() == PASSIVE)   //���ս���ɹ�����䣬ʧ�������½���
	{
		if(history->win())
		{
			//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par4_3"));
			str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par4_3"),Color3B(47,93,13)).c_str());
		}
		else
		{
			//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par4_2"));
			str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par4_2"),Color3B(47,93,13)).c_str());
			char s_ranking [20];
			sprintf(s_ranking,"%d",history->ranking());
			//str_des.append(s_ranking);
			str_des.append(StrUtils::applyColor(s_ranking,Color3B(47,93,13)).c_str());
		}
	}
	else            //����ս���ɹ���������ʧ������䣩
	{
		if(history->win())
		{
			//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par4_1"));
			str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par4_1"),Color3B(47,93,13)).c_str());
			char s_ranking [20];
			sprintf(s_ranking,"%d",history->ranking());
			//str_des.append(s_ranking);
			str_des.append(StrUtils::applyColor(s_ranking,Color3B(47,93,13)).c_str());
		}
		else
		{
			//str_des.append(StringDataManager::getString("Arena_fightNotes_des_par4_3"));
			str_des.append(StrUtils::applyColor(StringDataManager::getString("Arena_fightNotes_des_par4_3"),Color3B(47,93,13)).c_str());
		}
	}
	return str_des;
}
