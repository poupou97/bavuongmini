#include "OffLineArenaCountDownUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "AppMacros.h"
#include "../../utils/GameUtils.h"
#include "OffLineArenaState.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "OffLineArenaData.h"


OffLineArenaCountDownUI::OffLineArenaCountDownUI():
m_nRemainTime(5000)
{
}


OffLineArenaCountDownUI::~OffLineArenaCountDownUI()
{
}

OffLineArenaCountDownUI * OffLineArenaCountDownUI::create()
{
	auto offLineArenaCountDownUI = new OffLineArenaCountDownUI();
	if (offLineArenaCountDownUI && offLineArenaCountDownUI->init())
	{
		offLineArenaCountDownUI->autorelease();
		return offLineArenaCountDownUI;
	}
	CC_SAFE_DELETE(offLineArenaCountDownUI);
	return NULL;
}

bool OffLineArenaCountDownUI::init()
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		//����ʱ��ʾ
// 		l_countDown = Label::create();
// 		char s_countDown[10];
// 		sprintf(s_countDown,"%d",m_nRemainTime/1000);
// 		l_countDown->setText(s_countDown);
// 		l_countDown->setAnchorPoint(Vec2(0.5f,0.5f));
// 		l_countDown->setFontName(APP_FONT_NAME);
// 		l_countDown->setFontSize(40);
// 		l_countDown->setScale(2.5f);
// 		l_countDown->setPosition(Vec2(winsize.width/2,winsize.height*0.8f));
// 		m_pLayer->addChild(l_countDown);
		
		auto background_ =ImageView::create();
		background_->loadTexture("res_ui/zhezhao80.png");
		background_->setAnchorPoint(Vec2(0.5f,0.5f));
		background_->setScale9Enabled(true);
		background_->setContentSize(Size(winsize.width,80));
		//background_->setCapInsets(Rect(50,50,1,1));
		background_->setPosition(Vec2(-background_->getContentSize().width/2,winsize.height*0.8f));
		m_pLayer->addChild(background_);
		background_->runAction(MoveTo::create(0.5f,Vec2(winsize.width/2,winsize.height*0.8f)));
		

		imageView_countDown = ImageView::create();
		imageView_countDown->loadTexture("res_ui/font/5.png");
		imageView_countDown->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_countDown->setPosition(Vec2(winsize.width+ imageView_countDown->getContentSize().width,winsize.height*0.8f));
		m_pLayer->addChild(imageView_countDown);

		auto action =(ActionInterval *)Sequence::create(
			Show::create(),
			MoveTo::create(0.5f,Vec2(winsize.width/2,winsize.height*0.8f)),
			DelayTime::create(0.5f),
			CallFunc::create(CC_CALLBACK_0(OffLineArenaCountDownUI::showStarCountTime,this)),
			NULL);
		imageView_countDown->runAction(action);

		this->setContentSize(winsize);
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(OffLineArenaCountDownUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(OffLineArenaCountDownUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(OffLineArenaCountDownUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(OffLineArenaCountDownUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void OffLineArenaCountDownUI::onEnter()
{
	UIScene::onEnter();
}

void OffLineArenaCountDownUI::onExit()
{
	UIScene::onExit();
}

bool OffLineArenaCountDownUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void OffLineArenaCountDownUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void OffLineArenaCountDownUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void OffLineArenaCountDownUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void OffLineArenaCountDownUI::update( float dt )
{
	m_nRemainTime -= 1000;
	if (m_nRemainTime <= 0)
	{
		m_nRemainTime = 0;
		OffLineArenaState::setIsCanTouch(true);

		OffLineArenaData::getInstance()->setCurStatus(OffLineArenaData::s_inBattle);

		this->removeFromParent();
	}

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	std::string str_path = "res_ui/font/";
	str_path.append(s_remainTime);
	str_path.append(".png");
	imageView_countDown->loadTexture(str_path.c_str());
	//l_countDown->setText(s_remainTime);
}

void OffLineArenaCountDownUI::showStarCountTime()
{
	float space = (m_nRemainTime-(GameUtils::millisecondNow() - OffLineArenaState::getStartTime()))*1.0f/m_nRemainTime;
	this->schedule(schedule_selector(OffLineArenaCountDownUI::update),space*4.0f/5.0f);
}
