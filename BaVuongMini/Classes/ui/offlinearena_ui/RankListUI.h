
#ifndef _OFFLINEARENA_RANKLISTUI_
#define _OFFLINEARENA_RANKLISTUI_

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "../generals_ui/GeneralsListBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CSimplePartner;

class RankListUI : public GeneralsListBase,public TableViewDelegate,public TableViewDataSource
{
public:
	RankListUI();
	~RankListUI();

	static RankListUI * create(int myRanking);
	bool init(int myRanking);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void MakeFriendEvent(Ref *pSender, Widget::TouchEventType type);
	void LookOverEvent(Ref *pSender, Widget::TouchEventType type);

	void RefreshData();
	void RefreshDataWithOutChangeOffSet();
	// LiuLiang++(����ڷ����������ɫֵ 1��2��3��������)
	Color3B getFontColor(int nRank);					
	

private:
	TableView* m_tableView;
	//��ҵ����
	Text * l_selfRanking;

	int lastSelectCellId;
	int selectCellId;

public:
	//����а
	std::vector<CSimplePartner*> curRankingList;
};

#endif

