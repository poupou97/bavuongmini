

#ifndef _OFFLINEARENA_OFFLINEARENCOUNTDOWNUI_
#define _OFFLINEARENA_OFFLINEARENCOUNTDOWNUI_

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class OffLineArenaCountDownUI : public UIScene
{
public:
	OffLineArenaCountDownUI();
	~OffLineArenaCountDownUI();

	static OffLineArenaCountDownUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void update(float dt);

	void showStarCountTime();
private:
	int m_nRemainTime;
	//Label *l_countDown;
	ImageView * imageView_countDown;
};

#endif

