
#ifndef _OFFLINEARENA_FIGHTNOTESUI_
#define _OFFLINEARENA_FIGHTNOTESUI_

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CCMoveableMenu;
class CHistory;

class FightNotesUI : public UIScene,public TableViewDelegate,public TableViewDataSource
{
public:
	FightNotesUI();
	~FightNotesUI();

	static FightNotesUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	void RefreshData();

	void StrikeBackEvent(Ref *pSender);
	void LookOverEvent(Ref *pSender);

	std::string getFightNotesDes(CHistory * history);

private:
	TableView * m_tableView;
    CCMoveableMenu * menu_strikeBack;
	CCMoveableMenu * menu_lookOver;

	int lastSelectCellId;
	int selectCellId;

public: 
	//���ʷ��¼
	std::vector<CHistory*> curHistoryList;
};

#endif

