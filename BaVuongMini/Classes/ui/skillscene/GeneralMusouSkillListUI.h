#ifndef _SKILLUI_GENERALMUSOUSKILLLISTUI_H_
#define _SKILLUI_GENERALMUSOUSKILLLISTUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CShortCut;
class GameFightSkill;

class GeneralMusouSkillListUI : public UIScene,public TableViewDelegate,public TableViewDataSource
{
public:
	GeneralMusouSkillListUI();
	~GeneralMusouSkillListUI();

	static GeneralMusouSkillListUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	//��������ʾ���ж�����
	void AddMusouForGeneral(long long generalid,int index);
	void AddMusouForRole(std::string skillId);
	//���ѡ��״̬
	void addSelectImage();
	void addSelectImageForGeneral(long long generalid,int index);
	void AddSelectImageForRole(std::string skillId);

	long long getSelectGeneralID();
	CShortCut * getSelectShortCut();
	void setSelectShortCut(CShortCut * shortCut);

	void RefreshStudedSkillList();
	void RefreshRoleSkillItem();

public:
	Layer * layer_generalMusouSkillList;
	TableView * generalList_tableView;

	cocos2d::extension::ScrollView * m_scrollView;

	bool isSelectImageExist;

private:
	//��ǰѡ�е��佫ID
	long long m_curSelectGeneralId;
	//��ǰѡ�е��佫�ļ�����Ϣ
	CShortCut * m_curSelectShortCut;
	//��ɫѧ��ļ��
	std::vector<GameFightSkill * >m_studedSkillList;
};


/////////////////////////////////
/**
 *   ���Ǽ����б��еĵ��
 * @author yangjun
 * @version 0.1.0
 * @date 2014.3.11
 */

class CShortCut;

class RoleSkillItem : public UIScene
{
public:
	RoleSkillItem();
	~RoleSkillItem();
	static RoleSkillItem* create(GameFightSkill * gameFightSkill);
	bool init(GameFightSkill * gameFightSkill);

	void DoThing(Ref *pSender, Widget::TouchEventType type);
	void RefreshRoleSkillInfo(int curLevel,GameFightSkill * gameFightSkill);/**********����ĳ������Ϣ(�ȼ�)**********/

	void setIndex(int idx);
	int getIndex();

private:
	GameFightSkill * curGameFightSkill;
	int m_index;
};


/////////////////////////////////
/**
 *  �������佫�����б��еĵ��
 * @author yangjun
 * @version 0.1.0
 * @date 2014.3.11
 */

class CShortCut;

class GeneralsMusouSkillItem : public UIScene
{
public:
	GeneralsMusouSkillItem();
	~GeneralsMusouSkillItem();
	static GeneralsMusouSkillItem* create(long long genenralid,CShortCut * shortCut);
	bool init(long long genenralid,CShortCut * shortCut);

	void DoThing(Ref *pSender, Widget::TouchEventType type);
	void RefreshGeneralSkillInfo(std::string skillid);

	void setFrameVisible(bool isVisible);

private:
	long long curGeneralId;
	std::string m_skillId;
	CShortCut * m_curShortCut;

	Button *btn_bgFrame;
};


#endif

