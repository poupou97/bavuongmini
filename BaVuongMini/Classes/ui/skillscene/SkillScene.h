
#ifndef _SKILLSCENE_SKILLSCENE_H_
#define _SKILLSCENE_SKILLSCENE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/ScriptHandlerProtocol.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GameFightSkill;
class CShortCut;
class MusouSkillScene;
class UITab;
class MusouSkillTalent;

class SkillScene : public UIScene, public ScriptHandlerProtocol
{
public:
	SkillScene();
	~SkillScene();

	struct ReqForPreeless{
		std::string skillid;
		int grid;
		long long roleId;
		int roleType;
	};

	static SkillScene* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void SkillTypeTabIndexChangedEvent(Ref *pSender);

	void createTalentAndMusou();

// 	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
// 	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	void SkillInfoEvent(Ref *pSender, Widget::TouchEventType type);
	void ResetEvent(Ref *pSender, Widget::TouchEventType type); /*******���*******/
	void ConvenientEvent(Ref *pSender, Widget::TouchEventType type); /*******ñ�*******/
	void SureResetEvent(Ref *pSender);/*******�ȷ�����*******/

	void StudyEvent(Ref *pSender, Widget::TouchEventType type);     /********�ѧϰ*******/
	void SureToStudyEvent(Ref *pSender);

	void LevelUpEvent(Ref *pSender, Widget::TouchEventType type);   /********�*******/
	void LevelDownEvent(Ref *pSender, Widget::TouchEventType type);  /********��*******/
	void SureToLevUpSkill(Ref *pSender);   
	void SureToLevDownSkill(Ref *pSender);

	void PreelessEvent(Ref *pSender);

	void RefreshToDefault();

	void RefreshSkillPoint();
	void RefreshFucBtn(std::string skillid);/**********ĳ���ܸ��º�ˢ�°�ť״̬*********/
	void RefreshOneItem(int curLevel,GameFightSkill * gameFightSkill);/**********����ĳ������Ϣ(�ȼ�)**********/
	void RefreshOneItemInfo(int curLevel,GameFightSkill * gameFightSkill);/**********����ĳ������Ϣ(�ȼ�)**********/
	void RefreshData();

	//������˫��ʶ��λ�ã�Ӧ���и��,��Ϊ�ս����ҳ��ʱ�RefreshData����Ѿ����ú���λ�ã�֮��ÿ�ζ��Ǳ������Ӧ���ж�����
	void RefreshMumouPos(CShortCut *shortcut);

	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	//ѡ���
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	//����װ�䰴ť
	void addCCTutorialIndicator2(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	//�رհ�ť
	void addCCTutorialIndicator3(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	//����ǩ
	void addCCTutorialIndicator4(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();

	static void autoEquipSkill(std::string skillId);

	void AddBonusSpecialEffect(std::string skillId);
	void RemoveBonumSpecialEffect();

	Text * labelBmfont;

	static Node * GetSkillVarirtySprite(const char * skillid);
	static Widget * GetSkillVarirtyImageView(const char * skillid);

	static bool IsExistSkillEnableToLevelUp();
	static bool IsEnableLevelUp(int curLevel,GameFightSkill * gameFightSkill);
public:
	Layout * roleSkillPanel;
	Layout * skillpanel;

	Layer * u_layer;
	Layer * AllSkillLayer;

	MusouSkillScene * musouSkillLayer;

	MusouSkillTalent * musouSkillTalent;

	UITab *SkillTypeTab;

	ui::ScrollView * u_scrollView;
	//��һ���
	Button * m_firstAtkButton;
	std::string m_firstSkillName ;
	//����ü��ܵ
	Text * label_usedSkillPoint;
	//�ʣ�༼�ܵ
	Text * label_notUsedSkillPoint;
	//�ʣ�༼�ܵ�׿򣨷����СN��
	ImageView * image_remainFightpPointFrame;
	//����Ҫ
	 ui::TextField * textArea_lvUp;
	//�رհ�ť
	Button * Button_close;
	//��ť
	Button * Button_levUp;
	//������ť
	Button * Button_levDown;
	//��ݰ�ť
	Button * Button_convenient;
	//ѧϰ��ť
	Button * Button_study;
	//��˫��ť
	Button * Button_preeless;
	//ѡ��ͼ�
	ImageView * ImageView_select;
	//���˫��ʶ
	ImageView * ImageView_musou;
	
	Text * Label_skillName;
	// ui::TextField * textArea_lv;
	// ui::TextField * textArea_des;

	//��һ�����
	Label * l_nextLv;
	//���һ��
	Label * t_nextLvDes;
	//����Ҫ
	Sprite * s_upgradeNeed;
	Label * l_upgradeNeed;
	Sprite * s_second_line;
	//����ȼ�
	Label * l_playerLv;
	Label * l_playerLvValue;
	//���ܵ
	Label * l_skillPoint;
	Label * l_skillPointValue;
	//�ǰ�ü��
	Label * l_preSkill;
	Label * l_preSkillValue;
	//ܽ�
	Label * l_gold;
	Label * l_goldValue;
	//ҵ�
	Label * l_prop;
	Label * l_propValue;

	
	int curSkillLevel;

	//߽�ѧ
	int mTutorialScriptInstanceId;


public: 
	std::string defaultAtkSkill ;
	std::string curSkillId;
	Layout *ppanel;

	Layout * panel_mengjiang;
	Layout * panel_guimou;
	Layout * panel_shenshe;
	Layout * panel_haojie;

	GameFightSkill * curFightSkill;
	int updown;
};
#endif;

