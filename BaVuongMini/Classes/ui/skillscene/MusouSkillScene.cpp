#include "MusouSkillScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "GameView.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "AppMacros.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../utils/StaticDataManager.h"
#include "GeneralMusouSkillListUI.h"
#include "SkillScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../utils/StrUtils.h"

#define  M_SCROLLVIEW_TAG 621

#define  M_ROLEMUSOUSKILL_FLAT_TAG 631

MusouSkillScene::MusouSkillScene():
isRoleOrGeneral(true)
{
}


MusouSkillScene::~MusouSkillScene()
{
}

MusouSkillScene* MusouSkillScene::create()
{
	auto musouSkillScene = new MusouSkillScene();
	if (musouSkillScene && musouSkillScene->init())
	{
		musouSkillScene->autorelease();
		return musouSkillScene;
	}
	CC_SAFE_DELETE(musouSkillScene);
	return NULL;
}

bool MusouSkillScene::init()
{
	if (UIScene::init())
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		curSelectRoleSkillSkillId = "";

		//��꼼����
		panel_musouSkill = (Layout*)Helper::seekWidgetByName(LoadSceneLayer::SkillSceneLayer,"Panel_generals_jineng");
		panel_musouSkill->setVisible(true);
		//���꼼�
		layer_musouSkill= Layer::create();
		addChild(layer_musouSkill);
		layer_musouSkill->setVisible(true);

		btn_set = (Button *)Helper::seekWidgetByName(panel_musouSkill,"Button_set");
		btn_set->setTouchEnabled(true);
		btn_set->addTouchEventListener(CC_CALLBACK_2(MusouSkillScene::SetEvent, this));
		btn_set->setPressedActionEnabled(true);

		btn_suggestSet = (Button *)Helper::seekWidgetByName(panel_musouSkill,"Button_suggestSet");
		btn_suggestSet->setTouchEnabled(true);
		btn_suggestSet->addTouchEventListener(CC_CALLBACK_2(MusouSkillScene::SuggestSetEvent, this));
		btn_suggestSet->setPressedActionEnabled(true);

		//��佫�����б���
	    generalMusouSkillListUI = GeneralMusouSkillListUI::create();
		layer_musouSkill->addChild(generalMusouSkillListUI);
		generalMusouSkillListUI->setPosition(70,41);

		//��
		std::string str_info;
		std::map<int,std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(2);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end()) // �û�ҵ�����ָ�END��  
		{
			str_info.append("");
		}
		else
		{
			str_info.append(SystemInfoConfigData::s_systemInfoList[2].c_str());
		}
		auto l_des = Label::createWithTTF(str_info.c_str(),APP_FONT_NAME,16,Size(230, 0 ), TextHAlignment::LEFT);
		//l_des->setColor(Color3B(47,93,13));
		//StrUtils::applyColor(str_info.c_str(),Color3B(47,93,13));
		//CCRichLabel * l_des = CCRichLabel::createWithString(str_info.c_str(),Size(290, 0 ),NULL,NULL);
		//l_des->setScale(0.8f);
		
		int scroll_height = l_des->getContentSize().height*l_des->getScale();
		auto m_scrollView = cocos2d::extension::ScrollView::create(Size(250,120));
		m_scrollView->setViewSize(Size(250, 120));
		m_scrollView->setIgnoreAnchorPointForPosition(false);
		m_scrollView->setTouchEnabled(true);
		m_scrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		m_scrollView->setAnchorPoint(Vec2(0,0));
		m_scrollView->setPosition(Vec2(504,97));
		m_scrollView->setBounceable(true);
		m_scrollView->setClippingToBounds(true);
		layer_musouSkill->addChild(m_scrollView);
		if (scroll_height > 120)
		{
			m_scrollView->setContentSize(Size(250,scroll_height));
			m_scrollView->setContentOffset(Vec2(0,120-scroll_height));  
		}
		else
		{
			m_scrollView->setContentSize(Size(250,130));
		}
		m_scrollView->setClippingToBounds(true);

		m_scrollView->addChild(l_des);
		l_des->setPosition(Vec2(8,m_scrollView->getContentSize().height - scroll_height-5));

		layer_upper= Layer::create();
		addChild(layer_upper);
		layer_upper->setVisible(true);

		auto tableView_kuang = ImageView::create();
		tableView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
		tableView_kuang->setScale9Enabled(true);
		tableView_kuang->setContentSize(Size(429,373));
		tableView_kuang->setCapInsets(Rect(30,30,1,1));
		tableView_kuang->setAnchorPoint(Vec2(0,0));
		tableView_kuang->setPosition(Vec2(61,34));
		layer_upper->addChild(tableView_kuang);

		return true;
	}
	return false;
}

void MusouSkillScene::onEnter()
{
	UIScene::onEnter();
}

void MusouSkillScene::onExit()
{
	UIScene::onExit();
}

void MusouSkillScene::setVisible( bool visible )
{
	panel_musouSkill->setVisible(visible);
	layer_musouSkill->setVisible(visible);
	layer_upper->setVisible(visible);
}

void MusouSkillScene::SetEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (isRoleOrGeneral)
		{
			if (strcmp(curSelectRoleSkillSkillId.c_str(), "") == 0)
				return;

			auto temp = new SkillScene::ReqForPreeless();
			temp->skillid = curSelectRoleSkillSkillId;
			temp->grid = 4;
			temp->roleId = GameView::getInstance()->myplayer->getRoleId();
			temp->roleType = GameActor::type_player;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5075, temp);
			delete temp;
		}
		else
		{
			if (generalMusouSkillListUI->getSelectGeneralID() == -1)
				return;

			if (!generalMusouSkillListUI->getSelectShortCut()->has_skillpropid())
				return;

			auto temp = new SkillScene::ReqForPreeless();
			temp->skillid = generalMusouSkillListUI->getSelectShortCut()->skillpropid();
			temp->grid = generalMusouSkillListUI->getSelectShortCut()->index();
			temp->roleId = generalMusouSkillListUI->getSelectGeneralID();
			temp->roleType = GameActor::type_pet;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5075, temp);
			delete temp;
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MusouSkillScene::SuggestSetEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5076);

		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}


void MusouSkillScene::RefreshSkillInfo( int curLevel,GameFightSkill * gameFightSkill )
{
	if (layer_musouSkill->getChildByTag(M_SCROLLVIEW_TAG) != NULL)
	{
		layer_musouSkill->getChildByTag(M_SCROLLVIEW_TAG)->removeFromParent();
	}

	if (curLevel == 0 || gameFightSkill == NULL)
		return;

	int scroll_height = 0;
	//˼�����
	auto s_nameFrame = Sprite::create("res_ui/moji_0.png");
	s_nameFrame->setScaleY(0.6f);
	auto l_name = Label::createWithTTF(gameFightSkill->getCBaseSkill()->name().c_str(),APP_FONT_NAME,18);
	l_name->setColor(Color3B(196,255,68));
	int zeroLineHeight = s_nameFrame->getContentSize().height;
	scroll_height = zeroLineHeight;

	//
	auto s_first_line = Sprite::create("res_ui/henggang.png");
	s_first_line->setScaleX(2.0f);
	int zeroLineHeight_1 = zeroLineHeight + s_first_line->getContentSize().height+6;
	scroll_height = zeroLineHeight_1;

	//��/����
	Label * l_type;
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_type = Label::createWithTTF(skillinfo_zhudongjineng,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_type = Label::createWithTTF(skillinfo_zhudongjineng,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_type = Label::createWithTTF(skillinfo_zhudongjineng,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	}
	l_type->setColor(Color3B(212,255,151));

	//�ȼ�
	//const char *dengji  = ((__String*)strings->objectForKey("skillinfo_dengji"))->m_sString.c_str();
	const char *dengji = StringDataManager::getString("skillinfo_dengji");
	char* skillinfo_dengji =const_cast<char*>(dengji);
	auto l_lv = Label::createWithTTF(skillinfo_dengji,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_lv->setColor(Color3B(212,255,151));
	char s_lv[5];
	if (curLevel == 0)
	{
		sprintf(s_lv,"%d",0);
	}
	else
	{
		sprintf(s_lv,"%d",gameFightSkill->getCFightSkill()->level());
	}
	auto l_lvValue = Label::createWithTTF(s_lv,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_lvValue->setColor(Color3B(212,255,151));

	int firstLineHeight = zeroLineHeight_1 + l_type->getContentSize().height;
	scroll_height = firstLineHeight;
	///////////////////////2////////////////////////////
	//MP��
	//const char *mofaxiaohao  = ((__String*)strings->objectForKey("skillinfo_mofaxiaohao"))->m_sString.c_str();
	const char *mofaxiaohao = StringDataManager::getString("skillinfo_mofaxiaohao");
	char* skillinfo_mofaxiaohao =const_cast<char*>(mofaxiaohao);
	auto l_mp = Label::createWithTTF(skillinfo_mofaxiaohao,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_mp->setColor(Color3B(212,255,151));
	char s_mp[5];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	auto l_mpValue = Label::createWithTTF(s_mp,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_mpValue->setColor(Color3B(212,255,151));
	//CD�ʱ� 
	//const char *lengqueshijian  = ((__String*)strings->objectForKey("skillinfo_lengqueshijian"))->m_sString.c_str();
	const char *lengqueshijian = StringDataManager::getString("skillinfo_lengqueshijian");
	char* skillinfo_lengqueshijian =const_cast<char*>(lengqueshijian);
	auto l_cd = Label::createWithTTF(skillinfo_lengqueshijian,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_cd->setColor(Color3B(212,255,151));
	char s_intervaltime[5];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	auto l_cdValue = Label::createWithTTF(s_intervaltime,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_cdValue->setColor(Color3B(212,255,151));

	int secondLineHeight = firstLineHeight + l_mp->getContentSize().height;
	scroll_height = secondLineHeight;
	///////////////////////3////////////////////////////
	//�ʩ�����
	//const char *shifajuli  = ((__String*)strings->objectForKey("skillinfo_shifajuli"))->m_sString.c_str();
	const char *shifajuli = StringDataManager::getString("skillinfo_shifajuli");
	char* skillinfo_shifajuli =const_cast<char*>(shifajuli);
	auto l_dis = Label::createWithTTF(skillinfo_shifajuli,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_dis->setColor(Color3B(212,255,151));
	char s_distance[5];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	auto l_disValue = Label::createWithTTF(s_distance,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_disValue->setColor(Color3B(212,255,151));
	//�ְҵ
	//const char *zhiye  = ((__String*)strings->objectForKey("skillinfo_zhiye"))->m_sString.c_str();
	const char *zhiye = StringDataManager::getString("skillinfo_zhiye");
	char* skillinfo_zhiye =const_cast<char*>(zhiye);
	auto l_pro = Label::createWithTTF(skillinfo_zhiye,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_pro->setColor(Color3B(212,255,151));
	auto l_proValue = Label::createWithTTF(GameView::getInstance()->myplayer->getActiveRole()->profession().c_str(),APP_FONT_NAME,16,Size(100, 0 ), TextHAlignment::LEFT);
	l_proValue->setColor(Color3B(212,255,151));

	int thirdLineHeight = secondLineHeight + l_dis->getContentSize().height;
	scroll_height = thirdLineHeight;
	////////////////////4///////////////////////////////
	//��
	auto t_des = Label::createWithTTF(gameFightSkill->getCFightSkill()->description().c_str(),APP_FONT_NAME,16,Size(230, 0 ), TextHAlignment::LEFT);
	t_des->setColor(Color3B(30,255,0));

	int fourthLineHeight = thirdLineHeight+t_des->getContentSize().height;
	scroll_height = fourthLineHeight;
	/////////////////////////////////////////////////////
	int fifthLineHeight = 0;
	int sixthLineHeight = 0;
	int seventhLineHeight = 0;
	int seventhLineHeight_1 = 0;
	int eighthLineHeight = 0;
	int ninthLineHeight = 0;
	int tenthLineHeight = 0;
	int eleventhLineHeight = 0;
	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		//���һ��
			const char *xiayiji = StringDataManager::getString("skillinfo_xiayiji");
			char* skillinfo_xiayiji =const_cast<char*>(xiayiji);
			l_nextLv = Label::createWithTTF(skillinfo_xiayiji,APP_FONT_NAME,16,Size(100, 0 ), TextHAlignment::LEFT);
			l_nextLv->setColor(Color3B(0,255,255));

			fifthLineHeight = fourthLineHeight+l_nextLv->getContentSize().height*2;
			scroll_height = fifthLineHeight;
			/////////////////////////////////////////////////////
			t_nextLvDes = Label::createWithTTF(gameFightSkill->getCFightSkill()->get_next_description().c_str(),APP_FONT_NAME,16,Size(230, 0 ), TextHAlignment::LEFT);
			t_nextLvDes->setColor(Color3B(0,255,255));

			sixthLineHeight = fifthLineHeight+t_nextLvDes->getContentSize().height;
			scroll_height = sixthLineHeight;
			/////////////////////////////////////////////////////
			//�����
			s_upgradeNeed = Sprite::create("res_ui/moji_0.png");
			s_upgradeNeed->setScaleY(0.6f);
			//const char *shenjixuqiu  = ((__String*)strings->objectForKey("skillinfo_shengjixuqiu"))->m_sString.c_str();
			const char *shenjixuqiu = StringDataManager::getString("skillinfo_shengjixuqiu");
			char* skillinfo_shenjixuqiu =const_cast<char*>(shenjixuqiu);
			l_upgradeNeed = Label::createWithTTF(skillinfo_shenjixuqiu,APP_FONT_NAME,18);
			l_upgradeNeed->setColor(Color3B(196,255,68));
			seventhLineHeight = sixthLineHeight + s_upgradeNeed->getContentSize().height;
			scroll_height = seventhLineHeight;

			//////////////////////////////////////////////////////
			//
			s_second_line = Sprite::create("res_ui/henggang.png");
			s_second_line->setScaleX(2.0f);
			seventhLineHeight_1 = seventhLineHeight + s_first_line->getContentSize().height+7;
			scroll_height = seventhLineHeight_1;

			/////////////////////////////////////////////////////
			//���ǵȼ�
			if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
			{
				//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
				//const char *zhujuedengji  = ((__String*)strings->objectForKey("skillinfo_zhujuedengji"))->m_sString.c_str();
				const char *zhujuedengji = StringDataManager::getString("skillinfo_zhujuedengji");
				char* skillinfo_zhujuedengji =const_cast<char*>(zhujuedengji);
				l_playerLv = Label::createWithTTF(skillinfo_zhujuedengji,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
				l_playerLv->setColor(Color3B(212,255,151));
				char s_next_required_level[5];
				sprintf(s_next_required_level,"%d",gameFightSkill->getCFightSkill()->get_next_required_level());
				l_playerLvValue = Label::createWithTTF(s_next_required_level,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
				l_playerLvValue->setColor(Color3B(212,255,151));

				eighthLineHeight = seventhLineHeight_1 + l_playerLv->getContentSize().height;
				scroll_height = eighthLineHeight;

				if (GameView::getInstance()->myplayer->getActiveRole()->level()<gameFightSkill->getCFightSkill()->get_next_required_level())  //����ȼ�δ�ﵽ������Ҫ�
				{
					l_playerLv->setColor(Color3B(255,51,51));
					l_playerLvValue->setColor(Color3B(255,51,51));
				}
			}
			else
			{
				eighthLineHeight = seventhLineHeight_1;
				scroll_height = eighthLineHeight;
			}
			//��ܵ
			if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
			{
				//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
				//const char *jinengdian  = ((__String*)strings->objectForKey("skillinfo_jinengdian"))->m_sString.c_str();
				const char *jinengdian = StringDataManager::getString("skillinfo_jinengdian");
				char* skillinfo_jinengdian =const_cast<char*>(jinengdian);
				l_skillPoint = Label::createWithTTF(skillinfo_jinengdian,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
				l_skillPoint->setColor(Color3B(212,255,151));
				char s_next_required_point[5];
				sprintf(s_next_required_point,"%d",gameFightSkill->getCFightSkill()->get_next_required_point());
				l_skillPointValue = Label::createWithTTF(s_next_required_point,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
				l_skillPointValue->setColor(Color3B(212,255,151));

				ninthLineHeight = eighthLineHeight + l_skillPoint->getContentSize().height;
				scroll_height = ninthLineHeight;

				if(GameView::getInstance()->myplayer->player->skillpoint()<gameFightSkill->getCFightSkill()->get_next_required_point())//㼼�ܵ㲻�
				{
					l_skillPoint->setColor(Color3B(255,51,51));
					l_skillPointValue->setColor(Color3B(255,51,51));
				}
			}
			else
			{
				ninthLineHeight = eighthLineHeight;
				scroll_height = ninthLineHeight;
			}
			//XXX㼼�
			if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
			{
				std::string skillname = "";
				auto temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
				skillname.append(temp->name().c_str());
				skillname.append(": ");
				l_preSkill = Label::createWithTTF(skillname.c_str(),APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
				l_preSkill->setColor(Color3B(212,255,151));
				char s_next_pre_skill_level[5];
				sprintf(s_next_pre_skill_level,"%d",gameFightSkill->getCFightSkill()->get_next_pre_skill_level());
				l_preSkillValue = Label::createWithTTF(s_next_pre_skill_level,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
				l_preSkillValue->setColor(Color3B(212,255,151));

				tenthLineHeight = ninthLineHeight + l_preSkill->getContentSize().height;
				scroll_height = tenthLineHeight;

				std::map<std::string,GameFightSkill*>::const_iterator cIter;
				cIter = GameView::getInstance()->GameFightSkillList.find(gameFightSkill->getCFightSkill()->get_next_pre_skill());
				if (cIter == GameView::getInstance()->GameFightSkillList.end()) // �û�ҵ�����ָ�END��  
				{
				}
				else
				{
					auto gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
					if (gameFightSkillStuded)
					{
						if (gameFightSkillStuded->getLevel() < gameFightSkill->getCFightSkill()->get_next_pre_skill_level())
						{
							l_preSkill->setColor(Color3B(255,51,51));
							l_preSkillValue->setColor(Color3B(255,51,51));
						}
					}
				}
			}
			else
			{
				tenthLineHeight = ninthLineHeight;
				scroll_height = tenthLineHeight;
			}
			//˽�
			if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
			{
				//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
				//const char *jinbi  = ((__String*)strings->objectForKey("skillinfo_jinbi"))->m_sString.c_str();
				const char *jinbi = StringDataManager::getString("skillinfo_jinbi");
				char* skillinfo_jinbi =const_cast<char*>(jinbi);
				l_gold = Label::createWithTTF(skillinfo_jinbi,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
				l_gold->setColor(Color3B(212,255,151));
				char s_next_required_gold[20];
				sprintf(s_next_required_gold,"%d",gameFightSkill->getCFightSkill()->get_next_required_gold());
				l_goldValue = Label::createWithTTF(s_next_required_gold,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
				l_goldValue->setColor(Color3B(212,255,151));

				eleventhLineHeight = tenthLineHeight + l_gold->getContentSize().height;
				scroll_height = eleventhLineHeight;

				if(GameView::getInstance()->getPlayerGold()<gameFightSkill->getCFightSkill()->get_next_required_gold())//ҽ�Ҳ��
				{
					l_gold->setColor(Color3B(255,51,51));
					l_goldValue->setColor(Color3B(255,51,51));
				}
			}
			else
			{
				eleventhLineHeight = tenthLineHeight;
				scroll_height = eleventhLineHeight;
			}
	}

	auto m_scrollView = cocos2d::extension::ScrollView::create(Size(243,162));
	m_scrollView->setViewSize(Size(243, 162));
	m_scrollView->setIgnoreAnchorPointForPosition(false);
	m_scrollView->setTouchEnabled(true);
	m_scrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
	m_scrollView->setAnchorPoint(Vec2(0,0));
	m_scrollView->setPosition(Vec2(504,237));
	m_scrollView->setBounceable(true);
	m_scrollView->setClippingToBounds(true);
	m_scrollView->setTag(M_SCROLLVIEW_TAG);
	layer_musouSkill->addChild(m_scrollView);
	if (scroll_height > 162)
	{
		m_scrollView->setContentSize(Size(243,scroll_height));
		m_scrollView->setContentOffset(Vec2(0,162-scroll_height));  
	}
	else
	{
		m_scrollView->setContentSize(Size(243,162));
	}
	m_scrollView->setClippingToBounds(true);

	s_nameFrame->setIgnoreAnchorPointForPosition(false);
	s_nameFrame->setAnchorPoint(Vec2(0,0));
	m_scrollView->addChild(s_nameFrame);
	s_nameFrame->setPosition(Vec2(8,m_scrollView->getContentSize().height - zeroLineHeight));

	m_scrollView->addChild(l_name);
	l_name->setPosition(Vec2(15,m_scrollView->getContentSize().height - zeroLineHeight));

	m_scrollView->addChild(s_first_line);
	s_first_line->setPosition(Vec2(5,m_scrollView->getContentSize().height - zeroLineHeight_1+5));

	l_type->setIgnoreAnchorPointForPosition( false );  
	l_type->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_type);
	l_type->setPosition(Vec2(8,m_scrollView->getContentSize().height - firstLineHeight));

	l_mp->setIgnoreAnchorPointForPosition( false );  
	l_mp->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_mp);
	l_mp->setPosition(Vec2(105,m_scrollView->getContentSize().height - firstLineHeight));

	l_mpValue->setIgnoreAnchorPointForPosition( false );  
	l_mpValue->setAnchorPoint( Vec2( 0.5f, 0.5f ) );  
	m_scrollView->addChild(l_mpValue);
	l_mpValue->setPosition(Vec2(105+l_mp->getContentSize().width+2,m_scrollView->getContentSize().height - firstLineHeight));

	l_lv->setIgnoreAnchorPointForPosition( false );  
	l_lv->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_lv);
	l_lv->setPosition(Vec2(8,m_scrollView->getContentSize().height - secondLineHeight));

	l_lvValue->setIgnoreAnchorPointForPosition( false );  
	l_lvValue->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_lvValue);
	l_lvValue->setPosition(Vec2(8+l_lv->getContentSize().width+2,m_scrollView->getContentSize().height - secondLineHeight));

	l_cd->setIgnoreAnchorPointForPosition( false );  
	l_cd->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_cd);
	l_cd->setPosition(Vec2(105,m_scrollView->getContentSize().height - secondLineHeight));

	l_cdValue->setIgnoreAnchorPointForPosition( false );  
	l_cdValue->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_cdValue);
	l_cdValue->setPosition(Vec2(105+l_cd ->getContentSize().width+2,m_scrollView->getContentSize().height - secondLineHeight));

	l_pro->setIgnoreAnchorPointForPosition( false );  
	l_pro->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_pro);
	l_pro->setPosition(Vec2(8,m_scrollView->getContentSize().height - thirdLineHeight));

	l_proValue->setIgnoreAnchorPointForPosition( false );  
	l_proValue->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_proValue);
	l_proValue->setPosition(Vec2(8+l_pro->getContentSize().width+2,m_scrollView->getContentSize().height - thirdLineHeight));

	l_dis->setIgnoreAnchorPointForPosition( false );  
	l_dis->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_dis);
	l_dis->setPosition(Vec2(105,m_scrollView->getContentSize().height - thirdLineHeight));

	l_disValue->setIgnoreAnchorPointForPosition( false );  
	l_disValue->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_disValue);
	l_disValue->setPosition(Vec2(105+l_dis->getContentSize().width+2,m_scrollView->getContentSize().height - thirdLineHeight));

	t_des->setIgnoreAnchorPointForPosition( false );  
	t_des->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(t_des);
	t_des->setPosition(Vec2(8,m_scrollView->getContentSize().height - fourthLineHeight));

	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
			l_nextLv->setIgnoreAnchorPointForPosition( false );  
			l_nextLv->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(l_nextLv);
			l_nextLv->setPosition(Vec2(8,m_scrollView->getContentSize().height - fifthLineHeight));

			t_nextLvDes->setIgnoreAnchorPointForPosition( false );  
			t_nextLvDes->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(t_nextLvDes);
			t_nextLvDes->setPosition(Vec2(8,m_scrollView->getContentSize().height - sixthLineHeight));

			s_upgradeNeed->setIgnoreAnchorPointForPosition( false );  
			s_upgradeNeed->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(s_upgradeNeed);
			s_upgradeNeed->setPosition(Vec2(8,m_scrollView->getContentSize().height - seventhLineHeight));

			m_scrollView->addChild(l_upgradeNeed);
			l_upgradeNeed->setPosition(Vec2(15,m_scrollView->getContentSize().height - seventhLineHeight));

			m_scrollView->addChild(s_second_line);
			s_second_line->setPosition(Vec2(5,m_scrollView->getContentSize().height - seventhLineHeight_1+5));

			//���ǵȼ�
			if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
			{
				l_playerLv->setIgnoreAnchorPointForPosition( false );  
				l_playerLv->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_playerLv);
				l_playerLv->setPosition(Vec2(8,m_scrollView->getContentSize().height - eighthLineHeight));

				l_playerLvValue->setIgnoreAnchorPointForPosition( false );  
				l_playerLvValue->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_playerLvValue);
				l_playerLvValue->setPosition(Vec2(8+l_playerLv->getContentSize().width+5,m_scrollView->getContentSize().height - eighthLineHeight));
			}
			//���ܵ
			if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
			{
				l_skillPoint->setIgnoreAnchorPointForPosition( false );  
				l_skillPoint->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_skillPoint);
				l_skillPoint->setPosition(Vec2(8,m_scrollView->getContentSize().height - ninthLineHeight));

				l_skillPointValue->setIgnoreAnchorPointForPosition( false );  
				l_skillPointValue->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_skillPointValue);
				l_skillPointValue->setPosition(Vec2(8+l_skillPoint->getContentSize().width+5,m_scrollView->getContentSize().height - ninthLineHeight));
			}
			//XXX㼼�
			if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
			{
				l_preSkill->setIgnoreAnchorPointForPosition( false );  
				l_preSkill->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_preSkill);
				l_preSkill->setPosition(Vec2(8,m_scrollView->getContentSize().height - tenthLineHeight));

				l_preSkillValue->setIgnoreAnchorPointForPosition( false );  
				l_preSkillValue->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_preSkillValue);
				l_preSkillValue->setPosition(Vec2(8+l_preSkill->getContentSize().width+5,m_scrollView->getContentSize().height - tenthLineHeight));
			}
			//ܽ�
			if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
			{
				l_gold->setIgnoreAnchorPointForPosition( false );  
				l_gold->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_gold);
				l_gold->setPosition(Vec2(8,m_scrollView->getContentSize().height - eleventhLineHeight));

				l_goldValue->setIgnoreAnchorPointForPosition( false );  
				l_goldValue->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_goldValue);
				l_goldValue->setPosition(Vec2(8+l_gold->getContentSize().width+5,m_scrollView->getContentSize().height - eleventhLineHeight));
			}
	}
}

void MusouSkillScene::AddMusouFlagWithAnm( std::string skillId )
{
// 
// 	int idx = -1;
// 	for(int i = 0;i<m_studedSkillList.size();i++)
// 	{
// 		if (strcmp(skillId.c_str(),m_studedSkillList.at(i)->getId().c_str()) == 0)
// 		{
// 			idx = i;
// 			break;
// 		}
// 	}
// 
// 	if (idx == -1)
// 		return;

// 	for(int i = 0;i<m_studedSkillList.size();i++)
// 	{
// 		TableViewCell * cell = generalList_tableView->cellAtIndex(i);
// 		if(!cell)
// 			continue;
// 
// 		if (cell->getChildByTag(M_ROLEMUSOUSKILL_FLAT_TAG))
// 		{
// 			cell->getChildByTag(M_ROLEMUSOUSKILL_FLAT_TAG)->removeFromParent();
// 		}
// 	}
// 
// 	TableViewCell * cell = generalList_tableView->cellAtIndex(idx);
// 	Sprite * sprite_musou = Sprite::create("res_ui/wujiang/longhun.png");
// 	sprite_musou->setPosition(Vec2(65,65));
// 	sprite_musou->setTag(M_ROLEMUSOUSKILL_FLAT_TAG);
// 	cell->addChild(sprite_musou);
// 	sprite_musou->setScale(3.0f);
// 	sprite_musou->setVisible(true);
// 	FiniteTimeAction*  action = Sequence::create(
// 		CCEaseElasticIn::create(ScaleTo::create(0.7f,1.0f)),
// 		NULL);
// 	sprite_musou->runAction(action);
}

void MusouSkillScene::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void MusouSkillScene::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	auto winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
	auto tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,60,50);
	tutorialIndicator->setPosition(Vec2(pos.x-45,pos.y+95));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->addChild(tutorialIndicator);
}

void MusouSkillScene::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();
}
