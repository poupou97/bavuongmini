#include "SkillScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"
#include "../extensions/UITab.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutConfigure.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../generals_ui/generals_popup_ui/GeneralsSkillInfoUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"
#include "AppMacros.h"
#include "MusouSkillScene.h"
#include "GeneralMusouSkillListUI.h"
#include "MusouSkillTalent.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"
#include "../equipMent_ui/SystheSisUI.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutUIConstant.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../utils/StrUtils.h"

using namespace CocosDenshion;

#define  M_SCROLLVIEWTAG 44
#define  BUTTON_NORMAL_PATH "res_ui/new_button_1.png"
#define  BUTTON_DISABLED_PATH "res_ui/new_button_001.png"

#define kTag_BonusSpecialEffect 55

SkillScene::SkillScene():
curFightSkill(NULL),
updown(1),
curSkillLevel(0)
{
}


SkillScene::~SkillScene()
{
	CC_SAFE_DELETE(curFightSkill);
}

SkillScene* SkillScene::create()
{
	auto skillScene = new SkillScene();
	if (skillScene && skillScene->init())
	{
		skillScene->autorelease();
		return skillScene;
	}
	CC_SAFE_DELETE(skillScene);
	return NULL;
}

bool SkillScene::init()
{
	if(UIScene::init())
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		curFightSkill = new GameFightSkill();

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		//���UI
		if(LoadSceneLayer::SkillSceneLayer->getParent() != NULL)
		{
			LoadSceneLayer::SkillSceneLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::SkillSceneLayer;
		ppanel->getVirtualRenderer()->setContentSize(Size(800, 480));
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(800, 480));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		const char * secondStr = StringDataManager::getString("UIName_ji");
		const char * thirdStr = StringDataManager::getString("UIName_neng");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		u_layer->addChild(atmature);

		roleSkillPanel = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_role_jineng");
		roleSkillPanel->setVisible(true);
		//
		labelBmfont = (Text*)Helper::seekWidgetByName(ppanel,"Label_shengyu");
// 		//���꼼�
// 		long long time = GameUtils::millisecondNow();
// 		musouSkillLayer = MusouSkillScene::create();
// 		u_layer->addChild(musouSkillLayer);
// 		musouSkillLayer->setVisible(false);
// 		CCLOG(" MusouSkillScene::create() TIME = %ld",GameUtils::millisecondNow()-time);
// 
// 		//�����츳
// 		time = GameUtils::millisecondNow();
// 		musouSkillTalent = MusouSkillTalent::create();
// 		u_layer->addChild(musouSkillTalent);
// 		musouSkillTalent->setVisible(false);
// 		CCLOG(" MusouSkillTalent::create() TIME = %ld",GameUtils::millisecondNow()-time);
		u_scrollView = (ui::ScrollView*)Helper::seekWidgetByName(ppanel,"ScrollView_skill");
		u_scrollView->setBounceEnabled(true);
		//�رհ�ť
		Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(SkillScene::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);
		//���ð�ť
		auto Button_reset = (Button*)Helper::seekWidgetByName(ppanel,"Button_chongzhi");
		Button_reset->setTouchEnabled(true);
		Button_reset->addTouchEventListener(CC_CALLBACK_2(SkillScene::ResetEvent, this));
		Button_reset->setPressedActionEnabled(true);
		//��ť
		Button_levUp = (Button*)Helper::seekWidgetByName(ppanel,"Button_shengji");
		Button_levUp->setTouchEnabled(true);
		Button_levUp->addTouchEventListener(CC_CALLBACK_2(SkillScene::LevelUpEvent, this));
		Button_levUp->setPressedActionEnabled(true);
		//������ť
		Button_levDown = (Button*)Helper::seekWidgetByName(ppanel,"Button_jiangji");
		Button_levDown->setTouchEnabled(true);
		Button_levDown->addTouchEventListener(CC_CALLBACK_2(SkillScene::LevelDownEvent, this));
		Button_levDown->setPressedActionEnabled(true);
		//��ݰ�ť
		Button_convenient = (Button*)Helper::seekWidgetByName(ppanel,"Button_kuaijie");
		Button_convenient->setTouchEnabled(true);
		Button_convenient->addTouchEventListener(CC_CALLBACK_2(SkillScene::ConvenientEvent, this));
		Button_convenient->setPressedActionEnabled(true);
		//ѧϰ��ť
		Button_study = (Button*)Helper::seekWidgetByName(ppanel,"Button_xuexi");
		Button_study->setTouchEnabled(true);
		Button_study->addTouchEventListener(CC_CALLBACK_2(SkillScene::StudyEvent, this));
		Button_study->setPressedActionEnabled(true);
		//���ü��ܵ
		label_usedSkillPoint = (Text*)Helper::seekWidgetByName(ppanel,"Label_yiyongjinengdian");
		label_usedSkillPoint->setString("45");
		//�ʣ�༼�ܵ
		label_notUsedSkillPoint = (Text*)Helper::seekWidgetByName(ppanel,"Label_shengyujinengdian");
		label_notUsedSkillPoint->setString("55");
		//
		image_remainFightpPointFrame = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_5184_0");

		//�����Ҫ
// 		textArea_lvUp = ( ui::TextField *)Helper::seekWidgetByName(ppanel,"TextArea_jinengxiangqiang_c");
// 		textArea_lvUp->setText("hello everyone !");

		ImageView_select = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_select");
		ImageView_select->setVisible(false);

// 		ImageView_musou = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_musou");
// 		ImageView_musou->loadTexture("res_ui/longhun.png");
// 		ImageView_musou->setScale(1.05f);
// 		ImageView_musou->setVisible(false);

		Label_skillName =  (Text*)Helper::seekWidgetByName(ppanel,"Label_jinengmingcheng");
		//textArea_lv = ( ui::TextField *)Helper::seekWidgetByName(ppanel,"TextArea_jinengxiangqiang_a");
		//textArea_des = ( ui::TextField *)Helper::seekWidgetByName(ppanel,"TextArea_jinengxiangqiang_b");

		//��ȡ���ְҵ�ļ����
		panel_mengjiang = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_mengjiang");
		panel_guimou = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_guimou");
		panel_shenshe = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_shenshe");
		panel_haojie = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_haojie");
		panel_mengjiang->setVisible(false);
		panel_guimou->setVisible(false);
		panel_shenshe->setVisible(false);
		panel_haojie->setVisible(false);
		defaultAtkSkill = "";
		if(GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_MJ_NAME)
		{
			defaultAtkSkill = "WarriorDefaultAttack";
			curSkillId = "AZ1ltnh";
			panel_mengjiang->setVisible(true);
		}
		else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_GM_NAME)
		{
			defaultAtkSkill = "MagicDefaultAttack";
			curSkillId = "BZ1ssb";
			panel_guimou->setVisible(true);
		}
		else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_HJ_NAME)
		{
			defaultAtkSkill = "WarlockDefaultAttack";
			curSkillId = "CX1ycww";
			panel_haojie->setVisible(true);
		}
		else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_SS_NAME)
		{
			defaultAtkSkill = "RangerDefaultAttack";
			curSkillId = "DX1lys";
			panel_shenshe->setVisible(true);
		}
		
		//curSkillId = defaultAtkSkill;

		AllSkillLayer = Layer::create();
		u_layer->addChild(AllSkillLayer);
		AllSkillLayer->setContentSize(designResolutionSize);
		AllSkillLayer->setAnchorPoint(Vec2::ZERO);
		AllSkillLayer->setVisible(true);

		//maintab
		const char * normalImage = "res_ui/tab_4_off.png";
		const char * selectImage = "res_ui/tab_4_on.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_4_on.png";
		const char *str1 = StringDataManager::getString("skillui_tab_roleSkill");
		char* p1 =const_cast<char*>(str1);
//  		const char *str2  = StringDataManager::getString("skillui_tab_musouSkill");
//  		char* p2 =const_cast<char*>(str2);
		const char *str3  = StringDataManager::getString("skillui_tab_musouTalent");
		char* p3 =const_cast<char*>(str3);
		char * mainNames[] ={p1,p3};
		SkillTypeTab = UITab::createWithText(2,normalImage,selectImage,finalImage,mainNames,HORIZONTAL,5);
		SkillTypeTab->setAnchorPoint(Vec2(0,0));
		SkillTypeTab->setPosition(Vec2(66,410));
		SkillTypeTab->setHighLightImage((char * )highLightImage);
		SkillTypeTab->setDefaultPanelByIndex(0);
		SkillTypeTab->addIndexChangedEvent(this,coco_indexchangedselector(SkillScene::SkillTypeTabIndexChangedEvent));
		SkillTypeTab->setPressedActionEnabled(true);
		u_layer->addChild(SkillTypeTab);
		this->RefreshData();
		this->RefreshToDefault();

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(SkillScene::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(SkillScene::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(SkillScene::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(SkillScene::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}


void SkillScene::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);

	auto action = Sequence::create(
		DelayTime::create(0.3f),
		CallFunc::create(CC_CALLBACK_0(SkillScene::createTalentAndMusou,this)),
		NULL);
	this->runAction(action);
}

void SkillScene::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}


bool SkillScene::onTouchBegan(Touch *touch, Event * pEvent)
{
	return this->resignFirstResponder(touch,this,false);
}

void SkillScene::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void SkillScene::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void SkillScene::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void SkillScene::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		roleSkillPanel->setVisible(true);
		AllSkillLayer->setVisible(true);
		musouSkillLayer->setVisible(false);
		musouSkillTalent->setVisible(false);

		RemoveBonumSpecialEffect();

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void SkillScene::SkillTypeTabIndexChangedEvent( Ref *pSender )
{
	switch(((UITab*)pSender)->getCurrentIndex())
	{
	case 0:
	//	auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	//	if(sc != NULL)
	//		sc->endCommand(this);
		{
			roleSkillPanel->setVisible(true);
			AllSkillLayer->setVisible(true);
			musouSkillLayer->setVisible(false);
			musouSkillTalent->setVisible(false);
			//this->RefreshData();
			//this->RefreshToDefault();
		}
		break;
// 	case 1:
// 		{
// 			//���ȼ�
// 			int openlevel = 0;
// 			std::map<int,int>::const_iterator cIter;
// 			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(2);
// 			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
// 			{
// 			}
// 			else
// 			{
// 				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[2];
// 			}
// 
// 			if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
// 			{
// 				auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
// 				if(sc != NULL)
// 					sc->endCommand(SkillTypeTab);
// 
// 				roleSkillPanel->setVisible(false);
// 				AllSkillLayer->setVisible(false);
// 				musouSkillLayer->setVisible(true);
// 				musouSkillTalent->setVisible(false);
// 				musouSkillLayer->generalMusouSkillListUI->RefreshStudedSkillList();
// 				musouSkillLayer->generalMusouSkillListUI->generalList_tableView->reloadData();
// 				musouSkillLayer->RefreshSkillInfo(0,NULL);
// 			}
// 			else
// 			{
// 				std::string str_des = StringDataManager::getString("feature_will_be_open_function");
// 				char str_level[20];
// 				sprintf(str_level,"%d",openlevel);
// 				str_des.append(str_level);
// 				str_des.append(StringDataManager::getString("feature_will_be_open_open"));
// 				GameView::getInstance()->showAlertDialog(str_des.c_str());
// 			}
// 		}
// 		break;
	case 1:
		{
			//˿���ȼ�
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(7);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[7];
			}

			if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				roleSkillPanel->setVisible(false);
				AllSkillLayer->setVisible(false);
				musouSkillLayer->setVisible(false);
				musouSkillTalent->setVisible(true);
			}
			else
			{
				std::string str_des = StringDataManager::getString("feature_will_be_open_function");
				char str_level[20];
				std::sprintf(str_level,"%d",openlevel);
				str_des.append(str_level);
				str_des.append(StringDataManager::getString("feature_will_be_open_open"));
				GameView::getInstance()->showAlertDialog(str_des.c_str());
			}
		}
		break;
	}

}

// void SkillScene::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
// {
// }
// 
// void SkillScene::scrollViewDidZoom(cocos2d::extension::ScrollView* view)
// {
// }

void SkillScene::ResetEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		const char *str1 = StringDataManager::getString("skillui_reset_1");
		char* p1 = const_cast<char*>(str1);
		GameView::getInstance()->showPopupWindow(p1, 2, this, callfuncO_selector(SkillScene::SureResetEvent), NULL);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void SkillScene::ConvenientEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (ImageView_select->isVisible())
		{
			if (curSkillLevel > 0)
			{
				auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
				if (sc != NULL)
					sc->endCommand(pSender);
				//˴򿪿����
				if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutConfigure) == NULL)
				{
					Size winsize = Director::getInstance()->getVisibleSize();
					auto shortcutConfigure = ShortcutConfigure::create();
					shortcutConfigure->setTag(kTagShortcutConfigure);
					GameView::getInstance()->getMainUIScene()->addChild(shortcutConfigure);
					shortcutConfigure->setIgnoreAnchorPointForPosition(false);
					shortcutConfigure->setAnchorPoint(Vec2(0.5f, 0.5f));
					shortcutConfigure->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				}
			}
			else
			{
				//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
				//const char *str1  = ((__String*)strings->objectForKey("skillui_didnotstudy"))->m_sString.c_str();
				const char *str1 = StringDataManager::getString("skillui_didnotstudy");
				char* p1 = const_cast<char*>(str1);
				GameView::getInstance()->showAlertDialog(p1);
			}
		}
		else
		{
			//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *str1  = ((__String*)strings->objectForKey("skillui_peleaseselcetskill"))->m_sString.c_str();
			const char *str1 = StringDataManager::getString("skillui_peleaseselcetskill");
			char* p1 = const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(p1);
		}

	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void SkillScene::SureResetEvent( Ref *pSender )
{
	//�ȷ�����ü��ܵ
	updown = 3;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1126, (void *)updown,(void *)curSkillId.c_str());
}

void SkillScene::SkillInfoEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		CCLOG("SkillInfoEvent");
		//�����ѡ��״̬��ͼƬλ�
		auto selectSkill = (Button*)pSender;
		std::string buttonName = selectSkill->getName();
		std::string skillid = buttonName.substr(9);
		curSkillId = skillid;
		std::string temp = buttonName.substr(9, 1);

		if (strcmp(skillid.c_str(), m_firstSkillName.c_str()) == 0)
		{
			auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if (sc != NULL)
				sc->endCommand(pSender);
		}

		ImageView_select->setVisible(true);
		if (strcmp(temp.c_str(), "P") == 0) //ñ������
		{
			ImageView_select->loadTexture("res_ui/jineng/light_name2.png");
			//ImageView_select->setPosition(Vec2(selectSkill->getPosition().x-46,selectSkill->getPosition().y+159));
			ImageView_select->setPosition(Vec2(selectSkill->getPosition().x - 48, selectSkill->getPosition().y + (u_scrollView->getInnerContainerSize().height - 82 - u_scrollView->getContentSize().height) - 15));
		}
		else
		{
			ImageView_select->loadTexture("res_ui/jineng/light_name.png");
			//ImageView_select->setPosition(Vec2(selectSkill->getPosition().x-46,selectSkill->getPosition().y+165));
			ImageView_select->setPosition(Vec2(selectSkill->getPosition().x - 40, selectSkill->getPosition().y + (u_scrollView->getInnerContainerSize().height - 77 - u_scrollView->getContentSize().height) - 14));
		}

		auto gameFightSkill = new GameFightSkill();
		bool isHave = false;
		int curLevel;
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
		for (; it != GameView::getInstance()->GameFightSkillList.end(); ++it)
		{
			auto tempSkill = it->second;
			if (strcmp(tempSkill->getId().c_str(), skillid.c_str()) == 0)
			{
				curLevel = tempSkill->getLevel();
				curFightSkill->initSkill(skillid, skillid, tempSkill->getLevel(), GameActor::type_player);
				gameFightSkill->initSkill(skillid, skillid, tempSkill->getLevel(), GameActor::type_player);
				isHave = true;
				break;
			}
		}

		if (isHave == false)    //�ûѧ�
		{
			curLevel = 0;
			curFightSkill->initSkill(skillid, skillid, 1, GameActor::type_player);
			gameFightSkill->initSkill(skillid, skillid, 1, GameActor::type_player);
		}

		if (isHave == false)    //�ûѧ�
		{
			Button_study->setVisible(true);
			Button_study->setTouchEnabled(true);
			Button_levUp->setVisible(false);
			Button_levUp->setTouchEnabled(false);
			//Button_levDown->setBright(false);
			Button_levDown->loadTextureNormal(BUTTON_DISABLED_PATH);
			Button_levDown->setTouchEnabled(false);
			//Button_convenient->setBright(false);
			Button_convenient->loadTextureNormal(BUTTON_DISABLED_PATH);
			Button_convenient->setTouchEnabled(false);
		}
		else                //���ѧ�
		{
			Button_study->setVisible(false);
			Button_study->setTouchEnabled(false);
			Button_levUp->setVisible(true);
			Button_levUp->setTouchEnabled(true);
			Button_levUp->loadTextureNormal(BUTTON_NORMAL_PATH);
			//Button_levDown->setBright(true);
			Button_levDown->loadTextureNormal(BUTTON_NORMAL_PATH);
			Button_levDown->setTouchEnabled(true);
			if (strcmp(temp.c_str(), "P") == 0) //ᱻ�����
			{
				//Button_convenient->setBright(false);
				Button_convenient->loadTextureNormal(BUTTON_DISABLED_PATH);
				Button_convenient->setTouchEnabled(false);
			}
			else
			{
				//Button_convenient->setBright(true);
				Button_convenient->loadTextureNormal(BUTTON_NORMAL_PATH);
				Button_convenient->setTouchEnabled(true);
			}
		}

		if (strcmp(defaultAtkSkill.c_str(), skillid.c_str()) == 0)
		{
			Button_study->setVisible(false);
			Button_study->setTouchEnabled(false);
			Button_levUp->setVisible(true);
			Button_levUp->loadTextureNormal(BUTTON_DISABLED_PATH);
			Button_levUp->setTouchEnabled(false);
			//Button_levDown->setBright(false);
			Button_levDown->loadTextureNormal(BUTTON_DISABLED_PATH);
			Button_levDown->setTouchEnabled(false);
			//Button_convenient->setBright(false);
			Button_convenient->loadTextureNormal(BUTTON_NORMAL_PATH);
			Button_convenient->setTouchEnabled(true);
		}


		RefreshOneItem(curLevel, gameFightSkill);
		RefreshOneItemInfo(curLevel, gameFightSkill);

		delete gameFightSkill;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void SkillScene::StudyEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->myplayer->getActiveRole()->level()<curFightSkill->getCFightSkill()->get_required_level())  //�����ȼ�δ�ﵽ������Ҫ�
		{
			const char *str_roleLvNotEhouth = StringDataManager::getString("skillui_roleLvNotEhouth");
			GameView::getInstance()->showAlertDialog(str_roleLvNotEhouth);
		}
		else if (GameView::getInstance()->myplayer->player->skillpoint()<curFightSkill->getCFightSkill()->get_required_point())//��ܵ㲻�
		{
			const char *str_skillPointNotEnough = StringDataManager::getString("skillui_skillPointNotEnough");
			GameView::getInstance()->showAlertDialog(str_skillPointNotEnough);
		}
		else
		{
			updown = 1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1126, (void *)updown, (void *)curSkillId.c_str());

			GameUtils::playGameSound(SKILL_LEARN, 2, false);

			auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if (sc != NULL)
				sc->endCommand(pSender);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void SkillScene::SureToStudyEvent( Ref *pSender )
{

}

void SkillScene::LevelUpEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (curFightSkill->getLevel()<curFightSkill->getCBaseSkill()->maxlevel())
		{
			//���һ��
			if (curFightSkill->getLevel() == 0)
			{
				//ǰ�ü��ܵȼ�
				int preSkillLevel = 0;
				std::map<std::string, GameFightSkill*>::const_iterator cIter;
				cIter = GameView::getInstance()->GameFightSkillList.find(curFightSkill->getCFightSkill()->get_pre_skill());
				if (cIter == GameView::getInstance()->GameFightSkillList.end()) // û�ҵ�����ָ�END��  
				{
				}
				else
				{
					auto gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[curFightSkill->getCFightSkill()->get_pre_skill()];
					if (gameFightSkillStuded)
					{
						preSkillLevel = gameFightSkillStuded->getLevel();
					}
				}

				if (GameView::getInstance()->myplayer->getActiveRole()->level()<curFightSkill->getCFightSkill()->get_required_level())  //�����ȼ�δ�ﵽ������Ҫ�
				{
					const char *str_roleLvNotEhouth = StringDataManager::getString("skillui_roleLvNotEhouth");
					GameView::getInstance()->showAlertDialog(str_roleLvNotEhouth);
				}
				else if (GameView::getInstance()->myplayer->player->skillpoint()<curFightSkill->getCFightSkill()->get_required_point())//��ܵ㲻�
				{
					const char *str_skillPointNotEnough = StringDataManager::getString("skillui_skillPointNotEnough");
					GameView::getInstance()->showAlertDialog(str_skillPointNotEnough);
				}
				else if (preSkillLevel < curFightSkill->getCFightSkill()->get_pre_skill_level())//�ǰ�ü��ܵȼ����
				{
					std::string preSkillname = "";
					auto temp = StaticDataBaseSkill::s_baseSkillData[curFightSkill->getCFightSkill()->get_pre_skill()];
					preSkillname.append(temp->name().c_str());

					char des[200];
					const char *str_skillPointNotEnough = StringDataManager::getString("skillui_skillPreSkillLevelNotEnough");
					std::sprintf(des, str_skillPointNotEnough, preSkillname.c_str(), curFightSkill->getCFightSkill()->get_pre_skill_level());
					GameView::getInstance()->showAlertDialog(des);
				}
				else if (GameView::getInstance()->getPlayerGold()<curFightSkill->getCFightSkill()->get_required_gold())//��Ҳ��
				{
					const char *str_goldNotEnough = StringDataManager::getString("generals_teach_noEnoughGold");
					GameView::getInstance()->showAlertDialog(str_goldNotEnough);
				}
				else
				{
					std::string preSkillname = "";
					auto temp = StaticDataBaseSkill::s_baseSkillData[curFightSkill->getId()];
					//preSkillname.append(temp->name().c_str());
					preSkillname = StrUtils::applyColor(temp->name().c_str(), Color3B(243, 252, 2));

					char des[300];
					const char *str_skillPointNotEnough = StringDataManager::getString("skillui_areYouSureToLevelUp");
					std::sprintf(des, str_skillPointNotEnough, preSkillname.c_str(),
						curFightSkill->getCFightSkill()->level() + 1,
						curFightSkill->getCFightSkill()->get_required_point(),
						curFightSkill->getCFightSkill()->get_required_gold(),
						GameView::getInstance()->getPlayerGold());

					GameView::getInstance()->showPopupWindow(des, 2, this, SEL_CallFuncO(&SkillScene::SureToLevUpSkill), NULL);

					auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
					if (sc != NULL)
						sc->endCommand(Button_study);
				}
			}
			else
			{
				//�ǰ�ü��ܵȼ�
				int preSkillLevel = 0;
				std::map<std::string, GameFightSkill*>::const_iterator cIter;
				cIter = GameView::getInstance()->GameFightSkillList.find(curFightSkill->getCFightSkill()->get_next_pre_skill());
				if (cIter == GameView::getInstance()->GameFightSkillList.end()) // û�ҵ�����ָ�END��  
				{
				}
				else
				{
					auto gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[curFightSkill->getCFightSkill()->get_next_pre_skill()];
					if (gameFightSkillStuded)
					{
						preSkillLevel = gameFightSkillStuded->getLevel();
					}
				}

				if (GameView::getInstance()->myplayer->getActiveRole()->level()<curFightSkill->getCFightSkill()->get_next_required_level())  //�����ȼ�δ�ﵽ������Ҫ�
				{
					const char *str_roleLvNotEhouth = StringDataManager::getString("skillui_roleLvNotEhouth");
					GameView::getInstance()->showAlertDialog(str_roleLvNotEhouth);
				}
				else if (GameView::getInstance()->myplayer->player->skillpoint()<curFightSkill->getCFightSkill()->get_next_required_point())//��ܵ㲻�
				{
					const char *str_skillPointNotEnough = StringDataManager::getString("skillui_skillPointNotEnough");
					GameView::getInstance()->showAlertDialog(str_skillPointNotEnough);
				}
				else if (preSkillLevel < curFightSkill->getCFightSkill()->get_next_pre_skill_level())//�ǰ�ü��ܵȼ����
				{
					std::string preSkillname = "";
					auto temp = StaticDataBaseSkill::s_baseSkillData[curFightSkill->getCFightSkill()->get_next_pre_skill()];
					preSkillname.append(temp->name().c_str());

					char des[200];
					const char *str_skillPointNotEnough = StringDataManager::getString("skillui_skillPreSkillLevelNotEnough");
					std::sprintf(des, str_skillPointNotEnough, preSkillname.c_str(), curFightSkill->getCFightSkill()->get_next_pre_skill_level());
					GameView::getInstance()->showAlertDialog(des);
				}
				else if (GameView::getInstance()->getPlayerGold()<curFightSkill->getCFightSkill()->get_next_required_gold())//��Ҳ��
				{
					const char *str_goldNotEnough = StringDataManager::getString("generals_teach_noEnoughGold");
					GameView::getInstance()->showAlertDialog(str_goldNotEnough);
				}
				else
				{
					std::string preSkillname = "";
					auto temp = StaticDataBaseSkill::s_baseSkillData[curFightSkill->getId()];
					//preSkillname.append(temp->name().c_str());
					preSkillname = StrUtils::applyColor(temp->name().c_str(), Color3B(243, 252, 2));

					char des[300];
					const char *str_skillPointNotEnough = StringDataManager::getString("skillui_areYouSureToLevelUp");
					std::sprintf(des, str_skillPointNotEnough, preSkillname.c_str(),
						curFightSkill->getCFightSkill()->level() + 1,
						curFightSkill->getCFightSkill()->get_next_required_point(),
						curFightSkill->getCFightSkill()->get_next_required_gold(),
						GameView::getInstance()->getPlayerGold());

					GameView::getInstance()->showPopupWindow(des, 2, this, SEL_CallFuncO(&SkillScene::SureToLevUpSkill), NULL);

					auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
					if (sc != NULL)
						sc->endCommand(Button_study);
				}
			}
		}
		else
		{
			const char *str_isMaxLevel = StringDataManager::getString("skillui_maxLevel");
			GameView::getInstance()->showAlertDialog(str_isMaxLevel);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void SkillScene::LevelDownEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//updown = 2;
		//GameMessageProcessor::sharedMsgProcessor()->sendReq(1126, (void *)updown,(void *)curSkillId.c_str());
		auto temp = GameView::getInstance()->myplayer->getGameFightSkill(curSkillId.c_str(), curSkillId.c_str());
		if (!temp)
			return;

		if (temp->getCFightSkill()->get_back_num() <= 0)
		{
			updown = 2;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1126, (void *)updown, (void *)curSkillId.c_str());
			return;
		}

		std::string str_des;
		str_des.append(StringDataManager::getString("skillui_levelDown_1"));
		char s_num[10];
		std::sprintf(s_num, "%d", temp->getCFightSkill()->get_back_num());
		str_des.append(s_num);
		str_des.append(StringDataManager::getString("skillui_levelDown_2"));
		PropInfo propInfo = SysthesisUI::getPropInfoFromDBByPropId(temp->getCFightSkill()->get_back_prop().c_str());
		str_des.append(propInfo.name);
		str_des.append(StringDataManager::getString("skillui_levelDown_3"));
		str_des.append("1");
		str_des.append(StringDataManager::getString("skillui_levelDown_4"));
		if (ImageView_select->isVisible())
		{
			GameView::getInstance()->showPopupWindow(str_des.c_str(), 2, this, SEL_CallFuncO(&SkillScene::SureToLevDownSkill), NULL);
		}
		else
		{
			GameView::getInstance()->showAlertDialog("�δѡ�����");
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void SkillScene::SureToLevUpSkill(Ref *pSender)
{
	updown = 1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1126, (void *)updown,(void *)curSkillId.c_str());
}
void SkillScene::SureToLevDownSkill(Ref *pSender)
{
	updown = 2;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1126, (void *)updown,(void *)curSkillId.c_str());
}


//  ���˫��ť��Ӧ�¼
void SkillScene::PreelessEvent( Ref *pSender )
{
	auto temp = new ReqForPreeless();
	temp->skillid = curSkillId;
	temp->grid = 4;
	temp->roleId = GameView::getInstance()->myplayer->getRoleId();
	temp->roleType = GameActor::type_player;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5075,temp);
	delete temp;

	auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);
}

void SkillScene::RefreshSkillPoint()
{
	int allpoint = 0;
	if (GameView::getInstance()->myplayer->getActiveRole()->level()>=5)
	{
		int changeLevel = GameView::getInstance()->myplayer->getActiveRole()->level() - 4;
		if (changeLevel%2 == 0)
		{
			int temp = changeLevel/2;
			allpoint = temp*3;
		}
		else
		{
			int temp = changeLevel/2;
			allpoint = temp*3+1;
		}
	}
	else{}

	int usedPoint = allpoint - GameView::getInstance()->myplayer->player->skillpoint();
	if (usedPoint<0)
	{
		usedPoint = 0;
	}
	else{}

	int notUsedPoint = GameView::getInstance()->myplayer->player->skillpoint();
	if (notUsedPoint < 0)
	{
		notUsedPoint = 0;
	}
	else
	{

	}

	char s_usedPoint[10];
	std::sprintf(s_usedPoint,"%d",usedPoint);
	label_usedSkillPoint->setString(s_usedPoint);
	char s_notUsedPoint[10];
	std::sprintf(s_notUsedPoint,"%d",notUsedPoint);
	label_notUsedSkillPoint->setString(s_notUsedPoint);
}


void SkillScene::RefreshData()
{
	std::string tempStr = "SkillBtn_";
	if (panel_mengjiang->isVisible() == true)
	{
		skillpanel = panel_mengjiang;
		m_firstSkillName.append("AY1hxcq");
		tempStr.append("AY1hxcq");
	}
	else if (panel_guimou->isVisible() == true)
	{
		skillpanel = panel_guimou;
		m_firstSkillName.append("BY1tqms");
		tempStr.append("BY1tqms");
	}
	else if (panel_shenshe->isVisible() == true)
	{
		skillpanel = panel_shenshe;
		m_firstSkillName.append("DY1srpz");
		tempStr.append("DY1srpz");
	}
	else if (panel_haojie->isVisible() == true)
	{
		skillpanel = panel_haojie;
		m_firstSkillName.append("CY1tdwj");
		tempStr.append("CY1tdwj");
	}
	
	//__Array * childrenArray = skillpanel->getChildren()->data;
	cocos2d::Vector<Node*> childrenArray = skillpanel->getChildren();

	int length = childrenArray.size();
	std::string commonString = "SkillBtn_";

	for (int i = 0;i<length;++i)
	{
		auto child = (Widget*)childrenArray.at(i);
		std::string childName = child->getName();
		std::string skillid = childName.substr(9);

		if (strcmp(childName.c_str(),tempStr.c_str()) == 0)
		{
			m_firstAtkButton = (Button *)child;
		}

		if (childName.find(commonString)<childName.length())
		{
			auto gameFightSkill = new GameFightSkill();
			bool isHave = false;
			int curLevel;
			if (GameView::getInstance()->GameFightSkillList.size()>0)
			{
 				std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
				for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
				{
					//auto tempSkill = it->second;
					if (strcmp(it->second->getId().c_str(),skillid.c_str()) == 0)
					{
						curLevel = it->second->getLevel();
						curFightSkill->initSkill(skillid,skillid,it->second->getLevel(),GameActor::type_player);
						gameFightSkill->initSkill(skillid,skillid,it->second->getLevel(),GameActor::type_player);
						isHave = true;

// 						if (GameView::getInstance()->myplayer->getMusouSkill())
// 						{
// 							CCLOG("%d",GameView::getInstance()->GameFightSkillList.size());
// 							if (strcmp(GameView::getInstance()->myplayer->getMusouSkill()->getId().c_str(),it->second->getId().c_str()) == 0)
// 							{
// 								ImageView_musou->setVisible(true);
// 								ImageView_musou->setScale(1.05f);
// 								//ImageView_musou->setPosition(Vec2(child->getPosition().x-41,child->getPosition().y+170));
// 								ImageView_musou->setPosition(Vec2(child->getPosition().x-41,child->getPosition().y+(u_scrollView->getInnerContainerSize().height-70-u_scrollView->getContentSize().height)));
// 							}
// 						}

						break;
					}
				}

				if (isHave == false)
				{
					curLevel = 0;
					curFightSkill->initSkill(skillid,skillid,1,GameActor::type_player);
					gameFightSkill->initSkill(skillid,skillid,1,GameActor::type_player);
				}
			}
			else
			{
				curLevel = 0;
				gameFightSkill->initSkill(defaultAtkSkill,defaultAtkSkill,0,GameActor::type_player);
			}
			

			RefreshOneItem(curLevel,gameFightSkill);

			delete gameFightSkill;
		}	
	}
}

void SkillScene::RefreshFucBtn(std::string skillid)
{
	if (strcmp(defaultAtkSkill.c_str(),skillid.c_str()) == 0)
	{
		Button_study->setVisible(false);
		Button_study->setTouchEnabled(false);
		Button_levUp->setVisible(true);
		Button_levUp->loadTextureNormal(BUTTON_DISABLED_PATH);
		Button_levUp->setTouchEnabled(false);
		//Button_levDown->setBright(false);
		Button_levDown->loadTextureNormal(BUTTON_DISABLED_PATH);
		Button_levDown->setTouchEnabled(false);
		//Button_convenient->setBright(false);
		Button_convenient->loadTextureNormal(BUTTON_NORMAL_PATH);
		Button_convenient->setTouchEnabled(true);
		return;
	}

	std::string temp = skillid.substr(0,1);
	auto gameFightSkill = new GameFightSkill();
	bool isHave = false;
	int curLevel;
	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
	{
		auto tempSkill = it->second;
		if (strcmp(tempSkill->getId().c_str(),skillid.c_str()) == 0)
		{
			curLevel = tempSkill->getLevel();
			//curFightSkill = tempSkill;
			curFightSkill->initSkill(skillid,skillid,tempSkill->getLevel(),GameActor::type_player);
			gameFightSkill->initSkill(skillid,skillid,tempSkill->getLevel(),GameActor::type_player);
			isHave = true;
			break;
		}
	}

	if (isHave == false)    //�ûѧ�
	{
		curLevel = 0;
		gameFightSkill->initSkill(skillid,skillid,1,GameActor::type_player);
	}

	if (isHave == false)    //�ûѧ�
	{
		Button_study->setVisible(true);
		Button_study->setTouchEnabled(true);
		Button_levUp->setVisible(false);
		Button_levUp->setTouchEnabled(false);
		//Button_levDown->setBright(false);
		Button_levDown->loadTextureNormal(BUTTON_DISABLED_PATH);
		Button_levDown->setTouchEnabled(false);
		//Button_convenient->setBright(false);
		Button_convenient->loadTextureNormal(BUTTON_DISABLED_PATH);
		Button_convenient->setTouchEnabled(false);
	}
	else                //���ѧ�
	{
		Button_study->setVisible(false);
		Button_study->setTouchEnabled(false);
		Button_levUp->setVisible(true);
		Button_levUp->setTouchEnabled(true);
		Button_levUp->loadTextureNormal(BUTTON_NORMAL_PATH);
		//Button_levDown->setBright(true);
		Button_levDown->loadTextureNormal(BUTTON_NORMAL_PATH);
		Button_levDown->setTouchEnabled(true);
		if (strcmp(temp.c_str(),"P") == 0) //ᱻ�����
		{
			//Button_convenient->setBright(false);
			Button_convenient->loadTextureNormal(BUTTON_DISABLED_PATH);
			Button_convenient->setTouchEnabled(false);
		}
		else
		{
			//Button_convenient->setBright(true);
			Button_convenient->loadTextureNormal(BUTTON_NORMAL_PATH);
			Button_convenient->setTouchEnabled(true);
		}
	}
}

void SkillScene::RefreshOneItem(int curLevel,GameFightSkill * gameFightSkill)
{
	std::string commonString = "SkillBtn_";
	commonString.append(gameFightSkill->getId());
	if (skillpanel->getChildByName(commonString.c_str()) != NULL)
	{
		auto child = (Button *)skillpanel->getChildByName(commonString.c_str());
		child->setTouchEnabled(true);
		child->addTouchEventListener(CC_CALLBACK_2(SkillScene::SkillInfoEvent, this));
		
		if(((Label*)(child->getChildByName("Label_dengji"))))
		{
			std::string lv;
			curSkillLevel = curLevel;
			char curlv[3];
			std::sprintf(curlv,"%d",curSkillLevel);
			lv = curlv;
			lv.append("/");
			char maxLV[3];
			std::sprintf(maxLV,"%d",gameFightSkill->getCBaseSkill()->maxlevel());
			lv.append(maxLV);

			((Label*)(child->getChildByName("Label_dengji")))->setString(lv.c_str());
		}

		//skill variety icon
		if (gameFightSkill->getCBaseSkill()->usemodel() == 0) //��
		{
			auto image_skillVariety = SkillScene::GetSkillVarirtyImageView(gameFightSkill->getId().c_str());
			if (image_skillVariety)
			{
				if (child->getChildByName("image_skillVariety"))
				{
					child->getChildByName("image_skillVariety")->removeFromParent();
				}

				image_skillVariety->setAnchorPoint(Vec2(0.5f,0.5f));
				image_skillVariety->setPosition(Vec2(41,27));
				image_skillVariety->setScale(0.9f);
				child->addChild(image_skillVariety);
				image_skillVariety->setName("image_skillVariety");
			}
		}

		//__Array* pArray = child->getChildren();
		cocos2d::Vector<Node*> pArray = child->getChildren();
		//Ref* pObj = NULL;
		for(auto pObj : pArray)
		{
			auto pWidget = (Widget*)pObj;
			if(pWidget != NULL)
			{
				std::string name = pWidget->getName();
				int index = name.find("Label_");   // �����������
				if(index >= 0)
					continue;

				index = name.find("ImageView_yuanquan");   // ұ������ܣ����Ͻǵı�����ʶicon
				if(index >= 0)
					continue;

				if (curLevel == 0 && gameFightSkill->getCFightSkill()->get_required_level()>GameView::getInstance()->myplayer->getActiveRole()->level())
				{
					pWidget->setColor(Color3B(66,66,66));         //gray
				}
				else
				{
					pWidget->setColor(Color3B(255,255,255));   //white
				}
			}
		}
	}
}

void SkillScene::RefreshOneItemInfo(int curLevel,GameFightSkill * gameFightSkill)
{
	//create scrollerView
	auto scrollView_skillInfo = (ui::ScrollView*)AllSkillLayer->getChildByTag(M_SCROLLVIEWTAG);
	if (scrollView_skillInfo == NULL)
	{
		scrollView_skillInfo = ui::ScrollView::create();
		scrollView_skillInfo->setTouchEnabled(true);
		scrollView_skillInfo->setDirection(ui::ScrollView::Direction::VERTICAL);
		scrollView_skillInfo->setContentSize(Size(243,253));
		scrollView_skillInfo->setPosition(Vec2(501,142));
		scrollView_skillInfo->setBounceEnabled(true);
		scrollView_skillInfo->setTag(M_SCROLLVIEWTAG);
		AllSkillLayer->addChild(scrollView_skillInfo);
	}
	//create elements
	int scroll_height = 0;
	//������
	auto image_nameFrame = (ImageView *)scrollView_skillInfo->getChildByName("image_nameFrame");
	if (image_nameFrame == NULL)
	{
		image_nameFrame = ImageView::create();
		image_nameFrame->loadTexture("res_ui/moji_0.png");
		image_nameFrame->setScale(0.6f);
		image_nameFrame->setAnchorPoint(Vec2(0,0));
		image_nameFrame->setName("image_nameFrame");
		scrollView_skillInfo->addChild(image_nameFrame);
	}
	
	auto l_name = (Label *)scrollView_skillInfo->getChildByName("l_name");
	if (l_name == NULL)
	{
		l_name = Label::createWithTTF(gameFightSkill->getCBaseSkill()->name().c_str(), APP_FONT_NAME, 18);
		l_name->setColor(Color3B(196,255,68));
		l_name->setAnchorPoint(Vec2(0,0));
		l_name->setName("l_name");
		scrollView_skillInfo->addChild(l_name);
	}
	else
	{
		l_name->setString(gameFightSkill->getCBaseSkill()->name().c_str());
	}
	
	int zeroLineHeight = image_nameFrame->getContentSize().height;
	scroll_height = zeroLineHeight;

	//first line
	auto image_first_line = (ImageView *)scrollView_skillInfo->getChildByName("image_first_line");
	if (image_first_line == NULL)
	{
		image_first_line = ImageView::create();
		image_first_line->loadTexture("res_ui/henggang.png");
		image_first_line->setScaleX(0.9f);
		image_first_line->setAnchorPoint(Vec2(0,0));
		image_first_line->setName("image_first_line");
		scrollView_skillInfo->addChild(image_first_line);
	}
	int zeroLineHeight_1 = zeroLineHeight + image_first_line->getContentSize().height+6;
	scroll_height = zeroLineHeight_1;
	//��/����
	std::string str_type = "";
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		str_type.append(skillinfo_zhudongjineng);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *beidongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_beidongjineng =const_cast<char*>(beidongjineng);
		str_type.append(skillinfo_beidongjineng);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		str_type.append(skillinfo_zhudongjineng);
	}
	auto l_type = (Label *)scrollView_skillInfo->getChildByName("l_type");
	if (l_type == NULL)
	{
		l_type = Label::createWithTTF(str_type.c_str(), APP_FONT_NAME, 16);
		l_type->setColor(Color3B(212,255,151));
		l_type->setAnchorPoint(Vec2(0,0));
		l_type->setName("l_type");
		scrollView_skillInfo->addChild(l_type);
	}
	else
	{
		l_type->setString(str_type.c_str());
	}
	//MP��
	auto l_mp = (Label *)scrollView_skillInfo->getChildByName("l_mp");
	if (l_mp == NULL)
	{
		l_mp = Label::createWithTTF(StringDataManager::getString("skillinfo_mofaxiaohao"), APP_FONT_NAME, 16);
		l_mp->setColor(Color3B(212,255,151));
		l_mp->setAnchorPoint(Vec2(0,0));
		l_mp->setName("l_mp");
		scrollView_skillInfo->addChild(l_mp);
	}
	char s_mp[5];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	auto l_mpValue = (Label *)scrollView_skillInfo->getChildByName("l_mpValue");
	if (l_mpValue == NULL)
	{
		l_mpValue = Label::createWithTTF(s_mp, APP_FONT_NAME, 16);
		l_mpValue->setColor(Color3B(212,255,151));
		l_mpValue->setAnchorPoint(Vec2(0,0));
		l_mpValue->setName("l_mpValue");
		scrollView_skillInfo->addChild(l_mpValue);
	}
	else
	{
		l_mpValue->setString(s_mp);
	}
	int firstLineHeight = zeroLineHeight_1 + l_type->getContentSize().height;
	scroll_height = firstLineHeight;
	//ĵȼ�
	auto l_lv = (Label *)scrollView_skillInfo->getChildByName("l_lv");
	if (l_lv == NULL)
	{
		l_lv = Label::createWithTTF(StringDataManager::getString("skillinfo_dengji"), APP_FONT_NAME, 16);
		l_lv->setColor(Color3B(212,255,151));
		l_lv->setAnchorPoint(Vec2(0,0));
		l_lv->setName("l_lv");
		scrollView_skillInfo->addChild(l_lv);
	}

	char s_curLevel[5];
	if (curLevel == 0)
	{
		sprintf(s_curLevel,"%d",0);
	}
	else
	{
		sprintf(s_curLevel,"%d",gameFightSkill->getCFightSkill()->level());
	}
	auto l_lvValue = (Label *)scrollView_skillInfo->getChildByName("l_lvValue");
	if (l_lvValue == NULL)
	{
		l_lvValue = Label::createWithTTF(s_curLevel, APP_FONT_NAME, 16);
		l_lvValue->setColor(Color3B(212,255,151));
		l_lvValue->setAnchorPoint(Vec2(0,0));
		l_lvValue->setName("l_lvValue");
		scrollView_skillInfo->addChild(l_lvValue);
	}
	else
	{
		l_lvValue->setString(s_curLevel);
	}
	//CDʱ� 
	auto l_cd = (Label *)scrollView_skillInfo->getChildByName("l_cd");
	if (l_cd == NULL)
	{
		l_cd = Label::createWithTTF(StringDataManager::getString("skillinfo_lengqueshijian"), APP_FONT_NAME, 16);
		l_cd->setColor(Color3B(212,255,151));
		l_cd->setAnchorPoint(Vec2(0,0));
		l_cd->setName("l_cd");
		scrollView_skillInfo->addChild(l_cd);
	}
	char s_intervaltime[5];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	auto l_cdValue = (Label *)scrollView_skillInfo->getChildByName("l_cdValue");
	if (l_cdValue == NULL)
	{
		l_cdValue = Label::createWithTTF(s_intervaltime, APP_FONT_NAME, 16);
		l_cdValue->setColor(Color3B(212,255,151));
		l_cdValue->setAnchorPoint(Vec2(0,0));
		l_cdValue->setName("l_cdValue");
		scrollView_skillInfo->addChild(l_cdValue);
	}
	else
	{
		l_cdValue->setString(s_intervaltime);
	}
	int secondLineHeight = firstLineHeight + l_lv->getContentSize().height;
	scroll_height = secondLineHeight;
	//�ְҵ
	auto l_pro = (Label *)scrollView_skillInfo->getChildByName("l_pro");
	if (l_pro == NULL)
	{
		l_pro = Label::createWithTTF(StringDataManager::getString("skillinfo_zhiye"), APP_FONT_NAME, 16);
		l_pro->setColor(Color3B(212,255,151));
		l_pro->setAnchorPoint(Vec2(0,0));
		l_pro->setName("l_pro");
		scrollView_skillInfo->addChild(l_pro);
	}
	auto l_proValue = (Label *)scrollView_skillInfo->getChildByName("l_proValue");
	if (l_proValue == NULL)
	{
		l_proValue = Label::createWithTTF(GameView::getInstance()->myplayer->getActiveRole()->profession().c_str(), APP_FONT_NAME, 16);
		l_proValue->setColor(Color3B(212,255,151));
		l_proValue->setAnchorPoint(Vec2(0,0));
		l_proValue->setName("l_proValue");
		scrollView_skillInfo->addChild(l_proValue);
	}
	else
	{
		l_proValue->setString(GameView::getInstance()->myplayer->getActiveRole()->profession().c_str());
	}
	//ʩ�����
	auto l_dis = (Label *)scrollView_skillInfo->getChildByName("l_dis");
	if (l_dis == NULL)
	{
		l_dis = Label::createWithTTF(StringDataManager::getString("skillinfo_shifajuli"), APP_FONT_NAME, 16);
		l_dis->setColor(Color3B(212,255,151));
		l_dis->setAnchorPoint(Vec2(0,0));
		l_dis->setName("l_dis");
		scrollView_skillInfo->addChild(l_dis);
	}
	char s_distance[5];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	auto l_disValue = (Label *)scrollView_skillInfo->getChildByName("l_disValue");
	if (l_disValue == NULL)
	{
		l_disValue = Label::createWithTTF(s_distance, APP_FONT_NAME, 16);
		l_disValue->setColor(Color3B(212,255,151));
		l_disValue->setAnchorPoint(Vec2(0,0));
		l_disValue->setName("l_disValue");
		scrollView_skillInfo->addChild(l_disValue);
	}
	else
	{
		l_disValue->setString(s_distance);
	}
	int thirdLineHeight = secondLineHeight + l_pro->getContentSize().height;
	scroll_height = thirdLineHeight;
	//��
	auto l_des = (Text *)scrollView_skillInfo->getChildByName("l_des");
	if (l_des == NULL)
	{
		l_des = Text::create(gameFightSkill->getCFightSkill()->description().c_str(), APP_FONT_NAME, 16);
		l_des->setTextAreaSize(Size(230, 0 ));
		l_des->setColor(Color3B(30,255,0));
		l_des->setAnchorPoint(Vec2(0,0));
		l_des->setName("l_des");
		scrollView_skillInfo->addChild(l_des);
	}
	else
	{
		l_des->setString(gameFightSkill->getCFightSkill()->description().c_str());
	}
	int fourthLineHeight = thirdLineHeight+l_des->getContentSize().height;
	scroll_height = fourthLineHeight;

	//next info
	if (scrollView_skillInfo->getChildByName("panel_nextInfo"))
	{
		scrollView_skillInfo->getChildByName("panel_nextInfo")->removeFromParent();
	}
	auto panel_nextInfo = Layout::create();
	panel_nextInfo->setName("panel_nextInfo");
	scrollView_skillInfo->addChild(panel_nextInfo);

	int fifthLineHeight = 0;
	int sixthLineHeight = 0;
	int seventhLineHeight = 0;
	int seventhLineHeight_1 = 0;
	int eighthLineHeight = 0;
	int ninthLineHeight = 0;
	int tenthLineHeight = 0;
	int eleventhLineHeight = 0;

	std::string m_str_next_des = "";
	int m_required_level = 0;
	int m_required_point = 0;
	std::string m_str_pre_skill = "";
	std::string m_pre_skill_id = "";
	int m_required_pre_skil_level = 0;
	int m_required_gold = 0;

	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		//get data
		//���һ��
		if (curLevel == 0)
		{
			//��ǵȼ�
			m_required_level = gameFightSkill->getCFightSkill()->get_required_level();
			//���ܵ
			m_required_point = gameFightSkill->getCFightSkill()->get_required_point();
			//XXX㼼�
			if (gameFightSkill->getCFightSkill()->get_pre_skill() != "")
			{
				auto temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_pre_skill()];
				m_str_pre_skill.append(temp->name().c_str());
				m_pre_skill_id.append(temp->id());

				m_required_pre_skil_level = gameFightSkill->getCFightSkill()->get_pre_skill_level();
			}
			//ܽ�
			m_required_gold = gameFightSkill->getCFightSkill()->get_required_gold();
		}
		else
		{
			m_str_next_des.append(gameFightSkill->getCFightSkill()->get_next_description());
			//���ǵȼ�
			m_required_level = gameFightSkill->getCFightSkill()->get_next_required_level();
			//���ܵ
			m_required_point = gameFightSkill->getCFightSkill()->get_next_required_point();
			//XXX㼼�
			if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
			{
				auto temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
				m_str_pre_skill.append(temp->name().c_str());
				m_pre_skill_id.append(temp->id());

				m_required_pre_skil_level = gameFightSkill->getCFightSkill()->get_next_pre_skill_level();
			}
			//ܽ�
			m_required_gold = gameFightSkill->getCFightSkill()->get_next_required_gold();
		}

		//create ui
		//���һ��
		if (curLevel == 0)
		{
			fifthLineHeight = fourthLineHeight;
			scroll_height = fifthLineHeight;

			sixthLineHeight = fifthLineHeight;
			scroll_height = sixthLineHeight;
		}
		else
		{
			auto l_nextLv = Label::createWithTTF(StringDataManager::getString("skillinfo_xiayiji"), APP_FONT_NAME, 16);
			l_nextLv->setColor(Color3B(0,255,255));
			l_nextLv->setAnchorPoint(Vec2(0,0));
			l_nextLv->setName("l_nextLv");
			panel_nextInfo->addChild(l_nextLv);

			fifthLineHeight = fourthLineHeight+l_nextLv->getContentSize().height*2;
			scroll_height = fifthLineHeight;
			/////////////////////////////////////////////////////
			auto l_nextLvDes = Text::create(gameFightSkill->getCFightSkill()->get_next_description().c_str(), APP_FONT_NAME, 16);
			l_nextLvDes->setTextAreaSize(Size(230, 0 ));
			l_nextLvDes->setColor(Color3B(0,255,255));
			l_nextLvDes->setAnchorPoint(Vec2(0,0));
			l_nextLvDes->setName("l_nextLvDes");
			panel_nextInfo->addChild(l_nextLvDes);

			sixthLineHeight = fifthLineHeight+l_nextLvDes->getContentSize().height;
			scroll_height = sixthLineHeight;
		}

		//�����
		auto image_upgradeNeed = ImageView::create();
		image_upgradeNeed->loadTexture("res_ui/moji_0.png");
		image_upgradeNeed->setScale(0.6f);
		image_upgradeNeed->setAnchorPoint(Vec2(0,0));
		image_upgradeNeed->setName("image_upgradeNeed");
		panel_nextInfo->addChild(image_upgradeNeed);

		auto l_upgradeNeed = Label::createWithTTF(StringDataManager::getString("skillinfo_xuexixuqiu"), APP_FONT_NAME, 18);
		l_upgradeNeed->setColor(Color3B(196,255,68));
		l_upgradeNeed->setAnchorPoint(Vec2(0,0));
		l_upgradeNeed->setName("l_upgradeNeed");
		panel_nextInfo->addChild(l_upgradeNeed);

		seventhLineHeight = sixthLineHeight + image_upgradeNeed->getContentSize().height;
		scroll_height = seventhLineHeight;


		//////////////////////////////////////////////////////
		//
		auto image_second_line = ImageView::create();
		image_second_line->loadTexture("res_ui/henggang.png");
		image_second_line->setScale(0.9f);
		image_second_line->setAnchorPoint(Vec2(0,0));
		image_second_line->setName("image_second_line");
		panel_nextInfo->addChild(image_second_line);

		seventhLineHeight_1 = seventhLineHeight + image_second_line->getContentSize().height+7;
		scroll_height = seventhLineHeight_1;

		//���ǵȼ�
		if (m_required_level > 0)
		{
			auto l_playerLv = Label::createWithTTF(StringDataManager::getString("skillinfo_zhujuedengji"), APP_FONT_NAME, 16);
			l_playerLv->setColor(Color3B(212,255,151));
			l_playerLv->setAnchorPoint(Vec2(0,0));
			l_playerLv->setName("l_playerLv");
			panel_nextInfo->addChild(l_playerLv);

			auto l_playerLv_colon = Label::createWithTTF(":", APP_FONT_NAME, 16);
			l_playerLv_colon->setColor(Color3B(212,255,151));
			l_playerLv_colon->setAnchorPoint(Vec2(0,0));
			l_playerLv_colon->setName("l_playerLv_colon");
			panel_nextInfo->addChild(l_playerLv_colon);

			char s_required_level[5];
			sprintf(s_required_level,"%d",m_required_level);
			auto l_playerLvValue = Text::create(s_required_level, APP_FONT_NAME, 16);
			l_playerLvValue->setTextAreaSize(Size(230, 0 ));
			l_playerLvValue->setColor(Color3B(212,255,151));
			l_playerLvValue->setAnchorPoint(Vec2(0,0));
			l_playerLvValue->setName("l_playerLvValue");
			panel_nextInfo->addChild(l_playerLvValue);

			eighthLineHeight = seventhLineHeight_1 + l_playerLv->getContentSize().height;
			scroll_height = eighthLineHeight;

			if (GameView::getInstance()->myplayer->getActiveRole()->level()<m_required_level)  //����ȼ�δ�ﵽ������Ҫ�
			{
				l_playerLv->setColor(Color3B(255,51,51));
				l_playerLv_colon->setColor(Color3B(255,51,51));
				l_playerLvValue->setColor(Color3B(255,51,51));
			}
		}
		else
		{
			eighthLineHeight = seventhLineHeight_1;
			scroll_height = eighthLineHeight;
		}
		//��ܵ
		if (m_required_point > 0)
		{
			auto widget_skillPoint = Widget::create();
			panel_nextInfo->addChild(widget_skillPoint);
			widget_skillPoint->setName("widget_skillPoint");

			std::string str_skillPoint = StringDataManager::getString("skillinfo_jinengdian");
			for(int i = 0;i<3;i++)
			{
				std::string str_name = "l_skillPoint";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				auto l_skillPoint = Label::createWithTTF(str_skillPoint.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
				l_skillPoint->setColor(Color3B(212,255,151));
				l_skillPoint->setAnchorPoint(Vec2(0,0));
				l_skillPoint->setName(str_name.c_str());
				widget_skillPoint->addChild(l_skillPoint);
				l_skillPoint->setPosition(Vec2(24*i,0));
			}

			auto l_skillPoint_colon = Label::createWithTTF(":", APP_FONT_NAME, 16);
			l_skillPoint_colon->setColor(Color3B(212,255,151));
			l_skillPoint_colon->setAnchorPoint(Vec2(0,0));
			l_skillPoint_colon->setName("l_skillPoint_colon");
			panel_nextInfo->addChild(l_skillPoint_colon);

			char s_required_point[10];
			sprintf(s_required_point,"%d",m_required_point);
			auto l_skillPointValue = Text::create(s_required_point, APP_FONT_NAME, 16);
			l_skillPointValue->setTextAreaSize(Size(230, 0 ));
			l_skillPointValue->setColor(Color3B(212,255,151));
			l_skillPointValue->setAnchorPoint(Vec2(0,0));
			l_skillPointValue->setName("l_skillPointValue");
			panel_nextInfo->addChild(l_skillPointValue);

			ninthLineHeight = eighthLineHeight + l_skillPointValue->getContentSize().height;
			scroll_height = ninthLineHeight;

			if (GameView::getInstance()->myplayer->player->skillpoint()<m_required_point)  //����＼�ܵ㲻�
			{
				for(int i = 0;i<3;i++)
				{
					std::string str_name = "l_skillPoint";
					char s_index[5];
					sprintf(s_index,"%d",i);
					str_name.append(s_index);
					auto l_skillPoint = (Label*)widget_skillPoint->getChildByName(str_name.c_str());
					if (l_skillPoint)
					{
						l_skillPoint->setColor(Color3B(255,51,51));
					}
				}
				l_skillPoint_colon->setColor(Color3B(255,51,51));
				l_skillPointValue->setColor(Color3B(255,51,51));
			}
		}
		else
		{
			ninthLineHeight = eighthLineHeight;
			scroll_height = ninthLineHeight;
		}
		//XXX㼼�
		if (m_str_pre_skill != "")
		{
			auto widget_pre_skill = Widget::create();
			panel_nextInfo->addChild(widget_pre_skill);
			widget_pre_skill->setName("widget_pre_skill");
			if (m_str_pre_skill.size()/3 < 4)
			{
				for(int i = 0;i<m_str_pre_skill.size()/3;i++)
				{
					std::string str_name = "l_preSkill";
					char s_index[5];
					sprintf(s_index,"%d",i);
					str_name.append(s_index);

					auto l_preSkill = Label::createWithTTF(m_str_pre_skill.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
					l_preSkill->setColor(Color3B(212,255,151));
					l_preSkill->setAnchorPoint(Vec2(0,0));
					widget_pre_skill->addChild(l_preSkill);
					l_preSkill->setName(str_name.c_str());
					if (m_str_pre_skill.size()/3 == 1)
					{
						l_preSkill->setPosition(Vec2(0,0));
					}
					else if (m_str_pre_skill.size()/3 == 2)
					{
						l_preSkill->setPosition(Vec2(48*i,0));
					}
					else if (m_str_pre_skill.size()/3 == 3)
					{
						l_preSkill->setPosition(Vec2(24*i,0));
					}
				}
			}
			else
			{
				auto l_preSkill = Label::createWithTTF(m_str_pre_skill.c_str(), APP_FONT_NAME, 16);
				l_preSkill->setColor(Color3B(212,255,151));
				l_preSkill->setAnchorPoint(Vec2(0,0));
				l_preSkill->setPosition(Vec2(0,0));
				widget_pre_skill->addChild(l_preSkill);
				l_preSkill->setName("l_preSkill");
			}

			auto l_preSkill_colon = Label::createWithTTF(":", APP_FONT_NAME, 16);
			l_preSkill_colon->setColor(Color3B(212,255,151));
			l_preSkill_colon->setAnchorPoint(Vec2(0,0));
			l_preSkill_colon->setName("l_preSkill_colon");
			panel_nextInfo->addChild(l_preSkill_colon);

			char s_pre_skill_level[5];
			sprintf(s_pre_skill_level,"%d",m_required_pre_skil_level);
			auto l_preSkillValue = Text::create(s_pre_skill_level, APP_FONT_NAME, 16);
			l_preSkillValue->setTextAreaSize(Size(230, 0 ));
			l_preSkillValue->setColor(Color3B(212,255,151));
			l_preSkillValue->setAnchorPoint(Vec2(0,0));
			l_preSkillValue->setName("l_preSkillValue");
			panel_nextInfo->addChild(l_preSkillValue);

			tenthLineHeight = ninthLineHeight + l_preSkillValue->getContentSize().height;
			scroll_height = tenthLineHeight;

			std::map<std::string,GameFightSkill*>::const_iterator cIter;
			cIter = GameView::getInstance()->GameFightSkillList.find(m_pre_skill_id);
			if (cIter != GameView::getInstance()->GameFightSkillList.end()) // �û�ҵ�����ָ�END��  
			{
				auto gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[m_pre_skill_id];
				if (gameFightSkillStuded)
				{
					if (gameFightSkillStuded->getLevel() < m_required_pre_skil_level)
					{
						if (m_str_pre_skill.size()/3 < 4)
						{
							for(int i = 0;i<m_str_pre_skill.size()/3;i++)
							{
								std::string str_name = "l_preSkill";
								char s_index[5];
								sprintf(s_index,"%d",i);
								str_name.append(s_index);

								auto l_preSkill = (Label*)widget_pre_skill->getChildByName(str_name.c_str());
								if (l_preSkill)
								{
									l_preSkill->setColor(Color3B(255,51,51));
								}
							}
						}
						else
						{
							auto l_preSkill = (Label*)widget_pre_skill->getChildByName("l_preSkill");
							if (l_preSkill)
							{
								l_preSkill->setColor(Color3B(255,51,51));
							}
						}
						l_preSkill_colon->setColor(Color3B(255,51,51));
						l_preSkillValue->setColor(Color3B(255,51,51));
					}
				}
			}
		}
		else
		{
			tenthLineHeight = ninthLineHeight;
			scroll_height = tenthLineHeight;
		}
		//˽�
		if (m_required_gold > 0)
		{
			auto widget_gold = Widget::create();
			panel_nextInfo->addChild(widget_gold);
			widget_gold->setName("widget_gold");

			std::string str_gold = StringDataManager::getString("skillinfo_jinbi");
			for(int i = 0;i<2;i++)
			{
				std::string str_name = "l_gold";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				auto l_gold = Label::createWithTTF(str_gold.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
				l_gold->setColor(Color3B(212,255,151));
				l_gold->setAnchorPoint(Vec2(0,0));
				l_gold->setName(str_name.c_str());
				widget_gold->addChild(l_gold);
				l_gold->setPosition(Vec2(48*i,0));
			}

			auto l_gold_colon = Label::createWithTTF(":", APP_FONT_NAME, 16);
			l_gold_colon->setColor(Color3B(212,255,151));
			l_gold_colon->setAnchorPoint(Vec2(0,0));
			l_gold_colon->setName("l_gold_colon");
			panel_nextInfo->addChild(l_gold_colon);

			char s_required_gold[20];
			sprintf(s_required_gold,"%d",m_required_gold);

			auto l_goldValue = Label::createWithTTF(s_required_gold, APP_FONT_NAME, 16);
			l_goldValue->setColor(Color3B(212,255,151));
			l_goldValue->setAnchorPoint(Vec2(0,0));
			l_goldValue->setName("l_goldValue");
			panel_nextInfo->addChild(l_goldValue);

			auto image_gold = ImageView::create();
			image_gold->loadTexture("res_ui/coins.png");
			image_gold->setAnchorPoint(Vec2(0,0));
			image_gold->setName("image_gold");
			image_gold->setScale(0.85f);
			panel_nextInfo->addChild(image_gold);

			eleventhLineHeight = tenthLineHeight + l_goldValue->getContentSize().height;
			scroll_height = eleventhLineHeight;

			if(GameView::getInstance()->getPlayerGold()<m_required_gold)//ҽ�Ҳ��
			{
				for(int i = 0;i<2;i++)
				{
					std::string str_name = "l_gold";
					char s_index[5];
					sprintf(s_index,"%d",i);
					str_name.append(s_index);
					auto l_gold = (Label*)widget_gold->getChildByName(str_name.c_str());
					if (l_gold)
					{
						l_gold->setColor(Color3B(255,51,51));
					}
				}
				l_gold_colon->setColor(Color3B(255,51,51));
				l_goldValue->setColor(Color3B(255,51,51));
			}
		}
		else
		{
			eleventhLineHeight = tenthLineHeight;
			scroll_height = eleventhLineHeight;
		}
	}

	int innerWidth = scrollView_skillInfo->getBoundingBox().size.width;
	int innerHeight = scroll_height;
	if (scroll_height < scrollView_skillInfo->getBoundingBox().size.height)
	{
		innerHeight = scrollView_skillInfo->getBoundingBox().size.height;
	}
	scrollView_skillInfo->setInnerContainerSize(Size(innerWidth,innerHeight));

	image_nameFrame->setPosition(Vec2(8,innerHeight - zeroLineHeight));
	l_name->setPosition(Vec2(15,innerHeight - zeroLineHeight));
	image_first_line->setPosition(Vec2(5,innerHeight - zeroLineHeight_1+5));
	l_type->setPosition(Vec2(8,innerHeight - firstLineHeight));
	l_mp->setPosition(Vec2(105,innerHeight - firstLineHeight));
	l_mpValue->setPosition(Vec2(105+l_mp->getContentSize().width+2,innerHeight - firstLineHeight));
	l_lv->setPosition(Vec2(8,innerHeight - secondLineHeight));
	l_lvValue->setPosition(Vec2(8+l_lv->getContentSize().width+2,innerHeight - secondLineHeight));
	l_cd->setPosition(Vec2(105,innerHeight - secondLineHeight));
	l_cdValue->setPosition(Vec2(105+l_cd ->getContentSize().width+2,innerHeight - secondLineHeight));
	l_pro->setPosition(Vec2(8,innerHeight - thirdLineHeight));
	l_proValue->setPosition(Vec2(8+l_pro->getContentSize().width+2,innerHeight - thirdLineHeight));
	l_dis->setPosition(Vec2(105,innerHeight - thirdLineHeight));
	l_disValue->setPosition(Vec2(105+l_dis->getContentSize().width+2,innerHeight - thirdLineHeight));

	if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		l_mp->setVisible(false);
		l_mpValue->setVisible(false);
		l_cd->setVisible(false);
		l_cdValue->setVisible(false);
		l_dis->setVisible(false);
		l_disValue->setVisible(false);
	}
	else
	{
		l_mp->setVisible(true);
		l_mpValue->setVisible(true);
		l_cd->setVisible(true);
		l_cdValue->setVisible(true);
		l_dis->setVisible(true);
		l_disValue->setVisible(true);
	}

	l_des->setPosition(Vec2(8,innerHeight - fourthLineHeight));
	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		if (panel_nextInfo->getChildByName("l_nextLv"))
		{
			panel_nextInfo->getChildByName("l_nextLv")->setPosition(Vec2(8,innerHeight - fifthLineHeight));
		}

		if (panel_nextInfo->getChildByName("l_nextLvDes"))
		{
			panel_nextInfo->getChildByName("l_nextLvDes")->setPosition(Vec2(8,innerHeight - sixthLineHeight));
		}

		if (panel_nextInfo->getChildByName("image_upgradeNeed"))
		{
			panel_nextInfo->getChildByName("image_upgradeNeed")->setPosition(Vec2(8,innerHeight - seventhLineHeight));
		}

		if (panel_nextInfo->getChildByName("l_upgradeNeed"))
		{
			panel_nextInfo->getChildByName("l_upgradeNeed")->setPosition(Vec2(15,innerHeight - seventhLineHeight));
		}

		if (panel_nextInfo->getChildByName("image_second_line"))
		{
			panel_nextInfo->getChildByName("image_second_line")->setPosition(Vec2(5,innerHeight - seventhLineHeight_1+5));
		}

		int temp_width = 64;

		//���ǵȼ�
		if (m_required_level>0)
		{
			if (panel_nextInfo->getChildByName("l_playerLv"))
			{
				panel_nextInfo->getChildByName("l_playerLv")->setPosition(Vec2(8,innerHeight - eighthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_playerLv_colon"))
			{
				panel_nextInfo->getChildByName("l_playerLv_colon")->setPosition(Vec2(76,innerHeight - eighthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_playerLvValue"))
			{
				panel_nextInfo->getChildByName("l_playerLvValue")->setPosition(Vec2(85,innerHeight - eighthLineHeight));
			}
		}
		//���ܵ
		if (m_required_point>0)
		{
			if (panel_nextInfo->getChildByName("widget_skillPoint"))
			{
				panel_nextInfo->getChildByName("widget_skillPoint")->setPosition(Vec2(8,innerHeight - ninthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_skillPoint_colon"))
			{
				panel_nextInfo->getChildByName("l_skillPoint_colon")->setPosition(Vec2(76,innerHeight - ninthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_skillPointValue"))
			{
				panel_nextInfo->getChildByName("l_skillPointValue")->setPosition(Vec2(85,innerHeight - ninthLineHeight));
			}
		}
		//XXX㼼�
		if (m_str_pre_skill != "")
		{
			if (panel_nextInfo->getChildByName("widget_pre_skill"))
			{
				panel_nextInfo->getChildByName("widget_pre_skill")->setPosition(Vec2(8,innerHeight - tenthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_preSkill_colon"))
			{
				panel_nextInfo->getChildByName("l_preSkill_colon")->setPosition(Vec2(76,innerHeight - tenthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_preSkillValue"))
			{
				panel_nextInfo->getChildByName("l_preSkillValue")->setPosition(Vec2(85,innerHeight - tenthLineHeight));
			}
		}
		//ܽ�
		if (m_required_gold > 0)
		{
			if (panel_nextInfo->getChildByName("widget_gold"))
			{
				panel_nextInfo->getChildByName("widget_gold")->setPosition(Vec2(8,innerHeight - eleventhLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_gold_colon"))
			{
				panel_nextInfo->getChildByName("l_gold_colon")->setPosition(Vec2(76,innerHeight - eleventhLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_goldValue"))
			{
				panel_nextInfo->getChildByName("l_goldValue")->setPosition(Vec2(85,innerHeight - eleventhLineHeight));
			}
			if (panel_nextInfo->getChildByName("image_gold"))
			{
				panel_nextInfo->getChildByName("image_gold")->setPosition(Vec2(88+panel_nextInfo->getChildByName("l_goldValue")->getContentSize().width,innerHeight - eleventhLineHeight));
			}
		}
	}
	scrollView_skillInfo->jumpToTop();
}

void SkillScene::RefreshToDefault()
{
	this->RefreshSkillPoint();
	//////////////////////////////////////////////////////////////////
	//�����ѡ��״̬��ͼƬλ�
	//�Ĭ��ѡ����ͨ�����
	ImageView_select->loadTexture("res_ui/jineng/light_name.png");
	ImageView_select->setPosition(Vec2(202,403));  //(62,490)
	ImageView_select->setVisible(true);
	auto gameFightSkill = new GameFightSkill();
	bool isHave = false;
	int curLevel;
	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
	{
		auto tempSkill = it->second;
		if (strcmp(tempSkill->getId().c_str(),curSkillId.c_str()) == 0)
		{
			curLevel = tempSkill->getLevel();
			curFightSkill->initSkill(curSkillId,curSkillId,tempSkill->getLevel(),GameActor::type_player);
			gameFightSkill->initSkill(curSkillId,curSkillId,tempSkill->getLevel(),GameActor::type_player);
			isHave = true;
			break;
		}
	}

	if (isHave == false)    //�ûѧ�
	{
		curLevel = 0;
		curFightSkill->initSkill(curSkillId,curSkillId,1,GameActor::type_player);
		gameFightSkill->initSkill(curSkillId,curSkillId,1,GameActor::type_player);
	}

	//�Ĭ�ϼ��ܵĹ��ܰ�ť״̬
// 		Button_study->setVisible(false);
// 		Button_study->setTouchEnabled(false);
// 		Button_levUp->setVisible(true);
// 		Button_levUp->setNormalTexture(BUTTON_DISABLED_PATH);
// 		Button_levUp->setTouchEnabled(false);
// 		//Button_levDown->setBright(false);
// 		Button_levDown->setNormalTexture(BUTTON_DISABLED_PATH);
// 		Button_levDown->setTouchEnabled(false);
// 		//Button_convenient->setBright(false);
// 		Button_convenient->setNormalTexture(BUTTON_NORMAL_PATH);
// 		Button_convenient->setTouchEnabled(true);

	if (isHave == false)    //ûѧ�
	{
		Button_study->setVisible(true);
		Button_study->setTouchEnabled(true);
		Button_levUp->setVisible(false);
		Button_levUp->setTouchEnabled(false);
		//Button_levDown->setBright(false);
		Button_levDown->loadTextureNormal(BUTTON_DISABLED_PATH);
		Button_levDown->setTouchEnabled(false);
		//Button_convenient->setBright(false);
		Button_convenient->loadTextureNormal(BUTTON_DISABLED_PATH);
		Button_convenient->setTouchEnabled(false);
	}
	else                //���ѧ�
	{
		Button_study->setVisible(false);
		Button_study->setTouchEnabled(false);
		Button_levUp->setVisible(true);
		Button_levUp->setTouchEnabled(true);
		Button_levUp->loadTextureNormal(BUTTON_NORMAL_PATH);
		//Button_levDown->setBright(true);
		Button_levDown->loadTextureNormal(BUTTON_NORMAL_PATH);
		Button_levDown->setTouchEnabled(true);
		Button_convenient->loadTextureNormal(BUTTON_NORMAL_PATH);
		Button_convenient->setTouchEnabled(true);
	}

	RefreshOneItem(curLevel,gameFightSkill);
	RefreshOneItemInfo(curLevel,gameFightSkill);

	delete gameFightSkill;
	//////////////////////////////////////////////////////////////////
}

void SkillScene::RefreshMumouPos( CShortCut *shortcut )
{
	//__Array * childrenArray = skillpanel->getChildren()->data;
	cocos2d::Vector<Node*> childrenArray = skillpanel->getChildren();
	int length = childrenArray.size();
	std::string commonString = "SkillBtn_";

	for (int i = 0;i<length;++i)
	{
		auto child = (Widget*)childrenArray.at(i);
		std::string childName = child->getName();
		std::string skillid = childName.substr(9);

		if (childName.find(commonString)<childName.length())
		{
			if (strcmp(shortcut->skillpropid().c_str(),skillid.c_str()) == 0)
			{
// 				//ImageView_musou->setPosition(Vec2(child->getPosition().x-41,child->getPosition().y+170));
// 				ImageView_musou->setPosition(Vec2(child->getPosition().x-41,child->getPosition().y+(u_scrollView->getInnerContainerSize().height-70-u_scrollView->getContentSize().height)));
// 				ImageView_musou->setScale(3.0f);
// 				ImageView_musou->setVisible(true);
// 				FiniteTimeAction*  action = Sequence::create(
// 					CCEaseElasticIn::create(ScaleTo::create(0.7f,1.05f)),
// 					NULL);
// 				ImageView_musou->runAction(action);
			}
		}	
	}
}

void SkillScene::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void SkillScene::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	auto winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,30,60);
// 	tutorialIndicator->setPosition(Vec2(pos.x+86+_w,pos.y+100+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,52,52,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+27+_w,pos.y+10+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void SkillScene::addCCTutorialIndicator2( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	auto winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,60,65);
// 	tutorialIndicator->setPosition(Vec2(pos.x-45+_w,pos.y+100+_h));
// 	//tutorialIndicator->setPosition(Vec2(pos.x+60,pos.y+65));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,78,44,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void SkillScene::addCCTutorialIndicator3( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	auto winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	tutorialIndicator->setPosition(Vec2(pos.x-60+_w,pos.y-75+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,40,40,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void SkillScene::addCCTutorialIndicator4( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	auto winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
	auto tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,75,50);
	tutorialIndicator->setPosition(Vec2(pos.x+240+_w,pos.y-70+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->addChild(tutorialIndicator);
}


void SkillScene::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void SkillScene::autoEquipSkill(std::string skillId)
{
	auto shortCutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	if (!shortCutLayer)
		return;

	int index = -1;
	for (int i =0;i<MUSOUSKILL_INDEX;++i)
	{
		auto shortCutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(i+100);
		if (!shortCutSlot)
			continue;

		if (!shortCutSlot->isVisible())
			continue;

		if (shortCutSlot->slotType != ShortcutSlot::T_Void)
			continue;

		index = i;
		break;
	}

	if (index == -1)
		return;

	bool isExist = false;
	for (int i =0;i<MUSOUSKILL_INDEX;++i)
	{
		auto shortCutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(i+100);
		if (!shortCutSlot)
			continue;
			
		if (shortCutSlot->curShortCut->has_skillpropid())
		{
			if(shortCutSlot->curShortCut->skillpropid() == skillId)
			{
				isExist = true;
				break;
			}
		}
	}

	if (isExist)
		return;

	auto tempShortCut = new CShortCut();
	tempShortCut->set_index(index+100);
	tempShortCut->set_storedtype(1);
	tempShortCut->set_skillpropid(skillId);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1122, tempShortCut);
	delete tempShortCut;
}

void SkillScene::createTalentAndMusou()
{
	//���꼼�
	musouSkillLayer = MusouSkillScene::create();
	u_layer->addChild(musouSkillLayer);
	musouSkillLayer->setVisible(false);
	 
	//�����츳
	musouSkillTalent = MusouSkillTalent::create();
	u_layer->addChild(musouSkillTalent);
	musouSkillTalent->setVisible(false);
}

void SkillScene::AddBonusSpecialEffect( std::string skillId )
{
	std::string commonString = "SkillBtn_";
	commonString.append(skillId);
	if (skillpanel->getChildByName(commonString.c_str()) != NULL)
	{
		auto child = (Button *)skillpanel->getChildByName(commonString.c_str());
		if (child->getVirtualRenderer())
		{
			if (child->getVirtualRenderer()->getChildByTag(kTag_BonusSpecialEffect))
			{
				child->getVirtualRenderer()->getChildByTag(kTag_BonusSpecialEffect)->removeFromParent();
			}
		}

		// Bonus Special Effect
		auto pNode = BonusSpecialEffect::create();
		pNode->setScale(0.8f);
		pNode->setPosition(Vec2(30,30));
		pNode->setTag(kTag_BonusSpecialEffect);
		child->addChild(pNode);
		pNode->setLocalZOrder(5);
	}
}

void SkillScene::RemoveBonumSpecialEffect()
{
	//__Array * childrenArray = skillpanel->getChildren()->data;
	cocos2d::Vector<Node*> childrenArray = skillpanel->getChildren();

	int length = childrenArray.size();
	std::string commonString = "SkillBtn_";

	for (int i = 0;i<length;++i)
	{
		auto child = (Widget*)childrenArray.at(i);
		if (!child->getVirtualRenderer())
			continue;

		if (child->getVirtualRenderer()->getChildByTag(kTag_BonusSpecialEffect))
		{
			child->getVirtualRenderer()->getChildByTag(kTag_BonusSpecialEffect)->removeFromParent();
		}
	}
}

Node * SkillScene::GetSkillVarirtySprite( const  char * skillid )
{
	std::map<std::string,CBaseSkill*>::const_iterator cIter;
	cIter = StaticDataBaseSkill::s_baseSkillData.find(skillid);
	if (cIter == StaticDataBaseSkill::s_baseSkillData.end()) // û�ҵ�����ָ�END��  
	{
		return NULL;
	}

	auto baseSkill = StaticDataBaseSkill::s_baseSkillData[skillid];

	std::string str_skillVariety_path = "res_ui/font/";
	switch(baseSkill->get_skill_variety())
	{
	case 0:
		{
		}
		break;
	case 1:
		{
			str_skillVariety_path.append("dan.png");
		}
		break;
	case 2:
		{
			str_skillVariety_path.append("qun.png");
		}
		break;
	case 3:
		{
			str_skillVariety_path.append("kong.png");
		}
		break;
	case 4:
		{
			str_skillVariety_path.append("liao.png");
		}
		break;
	case 5:
		{
			str_skillVariety_path.append("te.png");
		}
		break;
	case 6:
		{
			str_skillVariety_path.append("fu.png");
		}
		break;
	}
	if (str_skillVariety_path.find(".png")!=string::npos)
	{
		auto sprite_skillVariety = Sprite::create(str_skillVariety_path.c_str());
		return sprite_skillVariety;
	}
	return NULL;
}

Widget * SkillScene::GetSkillVarirtyImageView( const  char * skillid )
{
	std::map<std::string,CBaseSkill*>::const_iterator cIter;
	cIter = StaticDataBaseSkill::s_baseSkillData.find(skillid);
	if (cIter == StaticDataBaseSkill::s_baseSkillData.end()) // �û�ҵ�����ָ�END��  
	{
		return NULL;
	}

	auto baseSkill = StaticDataBaseSkill::s_baseSkillData[skillid];

	std::string str_skillVariety_path = "res_ui/font/";
	switch(baseSkill->get_skill_variety())
	{
	case 0:
		{
		}
		break;
	case 1:
		{
			str_skillVariety_path.append("dan.png");
		}
		break;
	case 2:
		{
			str_skillVariety_path.append("qun.png");
		}
		break;
	case 3:
		{
			str_skillVariety_path.append("kong.png");
		}
		break;
	case 4:
		{
			str_skillVariety_path.append("liao.png");
		}
		break;
	case 5:
		{
			str_skillVariety_path.append("te.png");
		}
		break;
	case 6:
		{
			str_skillVariety_path.append("fu.png");
		}
		break;
	}
	if (str_skillVariety_path.find(".png")!=string::npos)
	{
		auto image_skillVariety = ImageView::create();
		image_skillVariety->loadTexture(str_skillVariety_path.c_str());
		return image_skillVariety;
	}
	return NULL;
}

bool SkillScene::IsExistSkillEnableToLevelUp()
{
	for(int i = 0;i<GameView::getInstance()->CBaseSkillList.size();i++)
	{
		bool isStuded = false;
		auto skillBase = GameView::getInstance()->CBaseSkillList.at(i);
		if(skillBase->get_owner() != 1)
			continue;

		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
		{
			auto fightSkill = it->second;
			if (strcmp(fightSkill->getId().c_str(),skillBase->id().c_str()) == 0)
			{
				isStuded = true;
				if (IsEnableLevelUp(fightSkill->getLevel(),fightSkill))
				{
					return true;
				}
			}
		}
		if (!isStuded)
		{
			auto fightSkill = new GameFightSkill();
			fightSkill->initSkill(skillBase->id(),skillBase->id(),1,GameActor::type_player);
			if (IsEnableLevelUp(1,fightSkill))
			{
				return true;
			}
		}
	}

	return false;
}

bool SkillScene::IsEnableLevelUp( int curLevel,GameFightSkill * gameFightSkill )
{
	if (curLevel<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		//���һ��
		if (curLevel == 0)
		{
			//��ǵȼ�
			if (gameFightSkill->getCFightSkill()->get_required_level()>0)
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->level()<gameFightSkill->getCFightSkill()->get_required_level())  //����ȼ�δ�ﵽ������Ҫ�
				{
					return false;
				}
			}
			//��ܵ
			if (gameFightSkill->getCFightSkill()->get_required_point()>0)
			{
				if(GameView::getInstance()->myplayer->player->skillpoint()<gameFightSkill->getCFightSkill()->get_required_point())//㼼�ܵ㲻�
				{
					return false;
				}
			}
			//XXX㼼�
			if (gameFightSkill->getCFightSkill()->get_pre_skill() != "")
			{
				std::map<std::string,GameFightSkill*>::const_iterator cIter;
				cIter = GameView::getInstance()->GameFightSkillList.find(gameFightSkill->getCFightSkill()->get_pre_skill());
				if (cIter == GameView::getInstance()->GameFightSkillList.end()) // �û�ҵ�����ָ�END��  
				{
					return false;
				}
				else
				{
					auto gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[gameFightSkill->getCFightSkill()->get_pre_skill()];
					if (gameFightSkillStuded)
					{
						if (gameFightSkillStuded->getLevel() < gameFightSkill->getCFightSkill()->get_pre_skill_level())
						{
							return false;
						}
					}
				}
			}
			//˽�
			if (gameFightSkill->getCFightSkill()->get_required_gold()>0)
			{
				if(GameView::getInstance()->getPlayerGold()<gameFightSkill->getCFightSkill()->get_required_gold())//ҽ�Ҳ��
				{
					return false;
				}
			}
		}
		else
		{
			/////////////////////////////////////////////////////
			//���ǵȼ�
			if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->level()<gameFightSkill->getCFightSkill()->get_next_required_level())  //����ȼ�δ�ﵽ������Ҫ�
				{
					return false;
				}
			}
			//��ܵ
			if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
			{
				if(GameView::getInstance()->myplayer->player->skillpoint()<gameFightSkill->getCFightSkill()->get_next_required_point())//㼼�ܵ㲻�
				{
					return false;
				}
			}
			//XXX㼼�
			if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
			{
				std::map<std::string,GameFightSkill*>::const_iterator cIter;
				cIter = GameView::getInstance()->GameFightSkillList.find(gameFightSkill->getCFightSkill()->get_next_pre_skill());
				if (cIter == GameView::getInstance()->GameFightSkillList.end()) // �û�ҵ�����ָ�END��  
				{
					return false;
				}
				else
				{
					auto gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
					if (gameFightSkillStuded)
					{
						if (gameFightSkillStuded->getLevel() < gameFightSkill->getCFightSkill()->get_next_pre_skill_level())
						{
							return false;
						}
					}
				}
			}
			//˽�
			if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
			{
				if(GameView::getInstance()->getPlayerGold()<gameFightSkill->getCFightSkill()->get_next_required_gold())//ҽ�Ҳ��
				{
					return false;
				}
			}
		}

		return true;
	}

	return false;
}




