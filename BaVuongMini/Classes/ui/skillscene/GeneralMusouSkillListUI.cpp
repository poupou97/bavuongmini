#include "GeneralMusouSkillListUI.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../AppMacros.h"
#include "../generals_ui/GeneralsUI.h"
#include "../generals_ui/RecuriteActionItem.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "../generals_ui/GeneralsShortcutSlot.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "SkillScene.h"
#include "../../gamescene_state/role/GameActor.h"
#include "MusouSkillScene.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/ActorUtils.h"

#define  M_ROLEMUSOUSKILL_FLAT_TAG 641
#define  M_ROLESKILLITEMBASE_TAG 726

#define  M_ROLESELECT_FLAG_TAG 671
#define  M_ROLE_SCROLLVIEW_TAG 681

GeneralMusouSkillListUI::GeneralMusouSkillListUI():
isSelectImageExist(false)
{
}


GeneralMusouSkillListUI::~GeneralMusouSkillListUI()
{
	delete m_curSelectShortCut;
}

GeneralMusouSkillListUI* GeneralMusouSkillListUI::create()
{
	auto generalMusouSkillListUI = new GeneralMusouSkillListUI();
	if (generalMusouSkillListUI && generalMusouSkillListUI->init())
	{
		generalMusouSkillListUI->autorelease();
		return generalMusouSkillListUI;
	}
	CC_SAFE_DELETE(generalMusouSkillListUI);
	return NULL;
}

bool GeneralMusouSkillListUI::init()
{
	if (UIScene::init())
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		//�佫��꼼�ܲ
		layer_generalMusouSkillList= Layer::create();
		addChild(layer_generalMusouSkillList);

		m_curSelectGeneralId = -1;
		m_curSelectShortCut = new CShortCut();

		//�ˢ����Ǽ����б���
		RefreshStudedSkillList();

		//��佫�б
		generalList_tableView = TableView::create(this,Size(420,356));
		generalList_tableView->setDirection(TableView::Direction::VERTICAL);
		generalList_tableView->setAnchorPoint(Vec2(0,0));
		generalList_tableView->setPosition(Vec2(0,0));
		generalList_tableView->setContentOffset(Vec2(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//generalList_tableView->setPressedActionEnabled(true);
		layer_generalMusouSkillList->addChild(generalList_tableView);

		return true;
	}
	return false;
}

void GeneralMusouSkillListUI::onEnter()
{
	UIScene::onEnter();
}

void GeneralMusouSkillListUI::onExit()
{
	UIScene::onExit();
}

void GeneralMusouSkillListUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void GeneralMusouSkillListUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void GeneralMusouSkillListUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	auto skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if (!skillScene)
		return;

	int index = cell->getIdx();
	if (index == 0)
	{
		//����õ�ǰѡ�е�����
		skillScene->musouSkillLayer->isRoleOrGeneral = true;
	}
	else
	{
		//����õ�ǰѡ�е����佫
		skillScene->musouSkillLayer->isRoleOrGeneral = false;
		m_curSelectGeneralId = GameView::getInstance()->generalsInLineDetailList.at(index-1)->generalid();
	}
	
}

cocos2d::Size GeneralMusouSkillListUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(410,91);
}

cocos2d::extension::TableViewCell* GeneralMusouSkillListUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	if(idx == 0)    //��
	{
		//Ǵ�߿
		auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
		sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
		sprite_bigFrame->setPosition(Vec2(0, 0));
		sprite_bigFrame->setPreferredSize(Size(410,91));
		cell->addChild(sprite_bigFrame);
		//�ͷ��׿
		auto sprite_headFrame = Sprite::create("res_ui/round.png");
		sprite_headFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_headFrame->setPosition(Vec2(46,54));
		cell->addChild(sprite_headFrame);
		//�ͷ�
		auto sprite_head = Sprite::create(BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str());
		sprite_head->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_head->setPosition(Vec2(sprite_headFrame->getContentSize().width/2,sprite_headFrame->getContentSize().height/2));
		sprite_headFrame->addChild(sprite_head);
		//����ֵ׿
		auto sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80_new.png");
		sprite_nameFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_nameFrame->setPosition(Vec2(46,21));
		sprite_nameFrame->setPreferredSize(Size(75,20));
		cell->addChild(sprite_nameFrame);
		//����
		auto l_name = Label::createWithTTF(GameView::getInstance()->myplayer->getActiveRole()->rolebase().name().c_str(),APP_FONT_NAME,16);
		l_name->setAnchorPoint(Vec2(0.5f, 0.5f));
		l_name->setPosition(Vec2(44, 21));
		cell->addChild(l_name);
		//ֵȼ��׿
		auto sprite_lvFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80_new.png");
		sprite_lvFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_lvFrame->setPosition(Vec2(68,38));
		sprite_lvFrame->setPreferredSize(Size(33,12));
		cell->addChild(sprite_lvFrame);
		//�ȼ�
		std::string str_lv = "LV";
		char str_lvValue [10];
		sprintf(str_lvValue,"%d",GameView::getInstance()->myplayer->getActiveRole()->level());
		str_lv.append(str_lvValue);
		auto l_lv = Label::createWithTTF(str_lv.c_str(),APP_FONT_NAME,12);
		l_lv->setAnchorPoint(Vec2(0.5f, 0.5f));
		l_lv->setPosition(Vec2(67, 38));
		cell->addChild(l_lv);

		//    test by yangjun 2014.3.11

		int scroll_width = 105*m_studedSkillList.size();
		m_scrollView = cocos2d::extension::ScrollView::create(Size(310,90));
		m_scrollView->setViewSize(Size(310, 90));
		m_scrollView->setIgnoreAnchorPointForPosition(false);
		//m_scrollView->setTouchEnabled(true);
		//m_scrollView->setDirection(kScrollViewDirectionHorizontal);
		m_scrollView->setAnchorPoint(Vec2(0,0));
		m_scrollView->setPosition(Vec2(100,0));
		m_scrollView->setBounceable(true);
		m_scrollView->setClippingToBounds(false);
		m_scrollView->setTag(M_ROLE_SCROLLVIEW_TAG);
		cell->addChild(m_scrollView);

		auto sprite_musou = Sprite::create("res_ui/longhun.png");
		sprite_musou->setPosition(Vec2(0,0));
		sprite_musou->setAnchorPoint(Vec2(0.5,0.5f));
		sprite_musou->setTag(M_ROLEMUSOUSKILL_FLAT_TAG);
		sprite_musou->setVisible(false);
		m_scrollView->addChild(sprite_musou);
		sprite_musou->setLocalZOrder(5);

		//ѡ��״̬
		auto sprite_select = Sprite::create("res_ui/jineng/jinengdi_1_on.png");
		sprite_select->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_select->setPosition(Vec2(43,61));
		sprite_select->setTag(M_ROLESELECT_FLAG_TAG);
		m_scrollView->addChild(sprite_select);
		sprite_select->setVisible(false);

		for(int i =0;i<m_studedSkillList.size();++i)
		{
			//�ײ�������Ƶĵ�ͼ           
			auto roleSkillItem = RoleSkillItem::create(m_studedSkillList.at(i));
			roleSkillItem->setIgnoreAnchorPointForPosition(false);
			roleSkillItem->setAnchorPoint(Vec2(0.5f, 0.5f));
			roleSkillItem->setPosition(Vec2(105*i-24, 5));
			m_scrollView->addChild(roleSkillItem);
			roleSkillItem->setTag(M_ROLESKILLITEMBASE_TAG+i);
			roleSkillItem->setIndex(i);

			if (GameView::getInstance()->myplayer->getMusouSkill())
			{
				if (strcmp(GameView::getInstance()->myplayer->getMusouSkill()->getId().c_str(),m_studedSkillList.at(i)->getId().c_str()) == 0)
				{
					sprite_musou->setAnchorPoint(Vec2(0.5f,0.5f));
					sprite_musou->setPosition(Vec2(43+105*i,61));
					sprite_musou->setVisible(true);
				}
			}
		}
		if (scroll_width > 310)
		{
			m_scrollView->setContentSize(Size(scroll_width,90));
		}
		else
		{
			m_scrollView->setContentSize(Size(310,90));
		}
		m_scrollView->setClippingToBounds(true);
		
	}
	else              //�佫
	{
		auto generalsDetail = GameView::getInstance()->generalsInLineDetailList.at(idx-1);
		auto generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalsDetail->modelid()];
		if (!generalMsgFromDb)
			return NULL;

		//m_curSelectGeneralId = generalsDetail->generalid();

		//��߿
		auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
		sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
		sprite_bigFrame->setPosition(Vec2(0, 0));
		sprite_bigFrame->setPreferredSize(Size(410,91));
		sprite_bigFrame->setLocalZOrder(-2);
		cell->addChild(sprite_bigFrame);
		//��佫ͷ��׿
		auto sprite_headFrame = Sprite::create("res_ui/round.png");
		sprite_headFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_headFrame->setPosition(Vec2(46,54));
		cell->addChild(sprite_headFrame);
		//��佫ͷ�
		std::string icon_path = "res_ui/generals46X45/";
		icon_path.append(generalMsgFromDb->get_head_photo());
		icon_path.append(".png");
		auto sprite_head = Sprite::create(icon_path.c_str());
		sprite_head->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_head->setPosition(Vec2(sprite_headFrame->getContentSize().width/2,sprite_headFrame->getContentSize().height/2));
		sprite_headFrame->addChild(sprite_head);
		//��佫���ֵ׿
		auto sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80_new.png");
		sprite_nameFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_nameFrame->setPosition(Vec2(46,21));
		sprite_nameFrame->setPreferredSize(Size(75,20));
		cell->addChild(sprite_nameFrame);
		//��佫���
		auto l_name = Label::createWithTTF(generalMsgFromDb->name().c_str(),APP_FONT_NAME,16);
		l_name->setAnchorPoint(Vec2(0.5f, 0.5f));
		l_name->setPosition(Vec2(44, 21));
		l_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalsDetail->currentquality()));
		cell->addChild(l_name);
		//��佫�ȼ��׿
		auto sprite_lvFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80_new.png");
		sprite_lvFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_lvFrame->setPosition(Vec2(68,38));
		sprite_lvFrame->setPreferredSize(Size(33,12));
		cell->addChild(sprite_lvFrame);
		//��佫�ȼ�
		auto l_lv = Label::createWithTTF("LV88",APP_FONT_NAME,12);
		l_lv->setAnchorPoint(Vec2(0.5f, 0.5f));
		l_lv->setPosition(Vec2(67, 38));
		cell->addChild(l_lv);
		char s_lv[10];
		sprintf(s_lv,"%d",generalsDetail->activerole().level());
		l_lv->setString(s_lv);
		//�佫���
		if (generalsDetail->evolution() > 0)
		{
			auto sprite_rank = ActorUtils::createGeneralRankSprite(generalsDetail->evolution());
			sprite_rank->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_rank->setPosition(Vec2(77,74));
			sprite_rank->setScale(0.8f);
			cell->addChild(sprite_rank);
		}
		//��佫ϡ�ж
		if (generalsDetail->rare()>0)
		{
			auto sprite_star = Sprite::create(RecuriteActionItem::getStarPathByNum(generalsDetail->rare()).c_str());
			sprite_star->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_star->setPosition(Vec2(16,71));
			sprite_star->setScale(0.8f);
			cell->addChild(sprite_star);
		}


		//ȴ����佫���
		for(int i = 0;i<generalsDetail->shortcuts_size();i++)
		{
			auto shortcut = new CShortCut();
			shortcut->CopyFrom(generalsDetail->shortcuts(i));
			if (shortcut->index()%2 == 0)
			{
				auto generalsMusouSkillItem = GeneralsMusouSkillItem::create(generalsDetail->generalid(),shortcut);
				generalsMusouSkillItem->setIgnoreAnchorPointForPosition(false);
				generalsMusouSkillItem->setAnchorPoint(Vec2(0.5f,0.5f));
				generalsMusouSkillItem->setPosition(Vec2(142+105*shortcut->index()/2,80));
				cell->addChild(generalsMusouSkillItem);

				if (shortcut->complexflag() == 1)
				{
					auto sprite_musou = Sprite::create("res_ui/longhun.png");
					sprite_musou->setPosition(Vec2(143+105*shortcut->index()/2,60));
					sprite_musou->setTag(M_ROLEMUSOUSKILL_FLAT_TAG);
					cell->addChild(sprite_musou);
					sprite_musou->setLocalZOrder(5);

					//���٣�������������ʾ���ס�ȼ���
					//�ȼ��
					auto s_lvFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80_new.png");
					s_lvFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
					s_lvFrame->setPosition(Vec2(166+105*shortcut->index()/2,39));
					s_lvFrame->setPreferredSize(Size(33,12));
					cell->addChild(s_lvFrame);
					s_lvFrame->setLocalZOrder(10);
					//׵ȼ�
					std::string str_lvstr = "LV";
					char str_lv [10];
					sprintf(str_lv,"%d",shortcut->skilllevel());
					str_lvstr.append(str_lv);
					auto l_lv = Label::createWithTTF(str_lvstr.c_str(),APP_FONT_NAME,12);
					l_lv->setAnchorPoint(Vec2(0.5f,0.5f));
					l_lv->setPosition(Vec2(166+105*shortcut->index()/2,39));
					cell->addChild(l_lv);
					l_lv->setLocalZOrder(10);
				}
			}
		}
	}
	
	return cell;
}

ssize_t GeneralMusouSkillListUI::numberOfCellsInTableView( TableView *table )
{
	return GameView::getInstance()->generalsInLineDetailList.size()+1;
}

void GeneralMusouSkillListUI::AddMusouForRole(std::string skillId)
{
	int idx = -1;
	for(int i = 0;i<m_studedSkillList.size();i++)
	{
	 	if (strcmp(skillId.c_str(),m_studedSkillList.at(i)->getId().c_str()) == 0)
	 	{
	 		idx = i;
	 		break;
	 	}
	}
	 
	if (idx == -1)
	 	return;

	if (!generalList_tableView->cellAtIndex(0))
		return;

	if(m_scrollView->getContainer()->getChildByTag(M_ROLESKILLITEMBASE_TAG+idx))
	{
		auto sprite_musou = (Sprite*)m_scrollView->getContainer()->getChildByTag(M_ROLEMUSOUSKILL_FLAT_TAG);
		sprite_musou->setPosition(Vec2(43+105*idx,61));
		sprite_musou->setAnchorPoint(Vec2(0.5f,0.5));
		sprite_musou->setVisible(true);
		sprite_musou->setScale(3.0f);
		auto  action = Sequence::create(
			CCEaseElasticIn::create(ScaleTo::create(0.7f,1.05f)),
			NULL);
		sprite_musou->runAction(action);
	}
}

void GeneralMusouSkillListUI::AddMusouForGeneral( long long generalid,int index )
{
	int idx = -1;
	for(int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
	{
		if (generalid == GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
		{
			idx = i;
			break;
		}
	}

	if (idx == -1)
		return;

	for(int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
	{
		if (generalid != GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
			continue;

		auto cell = generalList_tableView->cellAtIndex(i+1);
		if (!cell)
			continue;

		if (cell->getChildByTag(M_ROLEMUSOUSKILL_FLAT_TAG))
		{
			cell->getChildByTag(M_ROLEMUSOUSKILL_FLAT_TAG)->removeFromParent();
		}
	}

	auto cell = generalList_tableView->cellAtIndex(idx+1);
	if (!cell)
		return;

	auto sprite_musou = Sprite::create("res_ui/longhun.png");
	//Sprite * sprite_musou = Sprite::create("gamescene_state/jinengqu/wushuang_1.png");
	sprite_musou->setPosition(Vec2(143+105*index/2,60));
	sprite_musou->setTag(M_ROLEMUSOUSKILL_FLAT_TAG);
	cell->addChild(sprite_musou);
	sprite_musou->setScale(3.0f);
	sprite_musou->setVisible(true);
	sprite_musou->setLocalZOrder(5);
	auto  action = Sequence::create(
		CCEaseElasticIn::create(ScaleTo::create(0.7f,1.05f)),
		NULL);
	sprite_musou->runAction(action);

// 	//��٣�������������ʾ���ס�ȼ���
// 	//�ȼ��
// 	cocos2d::extension::Scale9Sprite * s_lvFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80_new.png");
// 	s_lvFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
// 	s_lvFrame->setPosition(Vec2(142+105*index/2,60));
// 	s_lvFrame->setPreferredSize(Size(33,12));
// 	cell->addChild(s_lvFrame);
// 	//׵ȼ�
// 	std::string str_lvstr = "LV";
// 	char str_lv [10];
// 	sprintf(str_lv,"%d",5);
// 	str_lvstr.append(str_lv);
// 	Label * l_lv = Label::createWithTTF(str_lvstr.c_str(),APP_FONT_NAME,12);
// 	l_lv->setAnchorPoint(Vec2(0.5f,0.5f));
// 	l_lv->setPosition(Vec2(142+105*index/2,60));
// 	cell->addChild(l_lv);

}

long long GeneralMusouSkillListUI::getSelectGeneralID()
{
	return m_curSelectGeneralId;
}

CShortCut * GeneralMusouSkillListUI::getSelectShortCut()
{
	return m_curSelectShortCut;
}

void GeneralMusouSkillListUI::setSelectShortCut( CShortCut * shortCut )
{
	m_curSelectShortCut->CopyFrom(*shortCut);
}



void GeneralMusouSkillListUI::RefreshStudedSkillList()
{
	//delete old
	std::vector<GameFightSkill*>::iterator iter_skill;
	for (iter_skill = m_studedSkillList.begin(); iter_skill != m_studedSkillList.end(); ++iter_skill)
	{
		delete *iter_skill;
	}
	m_studedSkillList.clear();
	//refresh
	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
	{
		auto tempSkill = it->second;
		if (!tempSkill)
			continue;

		if (tempSkill->getCBaseSkill()->usemodel() != 0)
			continue;

		if (strcmp(tempSkill->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
			||strcmp(tempSkill->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
			||strcmp(tempSkill->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
			||strcmp(tempSkill->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
			continue;

		auto newSkill = new GameFightSkill();
		newSkill->initSkill(tempSkill->getId(),tempSkill->getId(),tempSkill->getLevel(),GameActor::type_player);
		m_studedSkillList.push_back(newSkill);
	}
}


void GeneralMusouSkillListUI::RefreshRoleSkillItem()
{
	if (m_scrollView)
	{
		m_scrollView->removeAllChildren();
	}
	
	int scroll_width = 105*m_studedSkillList.size();
	
	auto sprite_musou = Sprite::create("res_ui/longhun.png");
	sprite_musou->setPosition(Vec2(0,0));
	sprite_musou->setAnchorPoint(Vec2(0.5,0.5f));
	sprite_musou->setTag(M_ROLEMUSOUSKILL_FLAT_TAG);
	sprite_musou->setVisible(false);

	for(int i =0;i<m_studedSkillList.size();++i)
	{
		//�ײ�������Ƶĵ�ͼ           
		auto roleSkillItem = RoleSkillItem::create(m_studedSkillList.at(i));
		roleSkillItem->setIgnoreAnchorPointForPosition(false);
		roleSkillItem->setAnchorPoint(Vec2(0.5f, 0.5f));
		roleSkillItem->setPosition(Vec2(105*i-24, 5));
		m_scrollView->addChild(roleSkillItem);
		roleSkillItem->setTag(M_ROLESKILLITEMBASE_TAG+i);
		roleSkillItem->setIndex(i);

		if (GameView::getInstance()->myplayer->getMusouSkill())
		{
			if (strcmp(GameView::getInstance()->myplayer->getMusouSkill()->getId().c_str(),m_studedSkillList.at(i)->getId().c_str()) == 0)
			{
				sprite_musou->setVisible(true);
				sprite_musou->setPosition(Vec2(43+105*i,61));
			}
		}
	}
	if (scroll_width > 310)
	{
		m_scrollView->setContentSize(Size(scroll_width,90));
	}
	else
	{
		m_scrollView->setContentSize(Size(310,90));
	}
	m_scrollView->setClippingToBounds(true);

	m_scrollView->addChild(sprite_musou);
}

void GeneralMusouSkillListUI::addSelectImage()
{
	if(isSelectImageExist)
	{
		//delete old
	}

	//add new 


	//set data 
	isSelectImageExist = true;
}

void GeneralMusouSkillListUI::addSelectImageForGeneral( long long generalid,int index )
{
	//����佫���ܵ�ѡ�б�ʾ
	int idx = -1;   //��ǰѡ���ǵڼ��Cell
	for(int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
	{
		if (generalid == GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
		{
			idx = i;
			break;
		}
	}

	if (idx == -1)
		return;

	for(int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
	{
// 		if (generalid != GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
// 			continue;

		auto cell = generalList_tableView->cellAtIndex(i+1);
		if (!cell)
			continue;

		if (i == 0)
		{
			//�ɾ�����＼�ܵ�ѡ��״̬
			auto roleCell = generalList_tableView->cellAtIndex(0);
			if (roleCell)
			{
				if (roleCell->getChildByTag(M_ROLE_SCROLLVIEW_TAG))
				{
					if (m_scrollView->getContainer()->getChildByTag(M_ROLESELECT_FLAG_TAG))
					{
						(m_scrollView->getContainer()->getChildByTag(M_ROLESELECT_FLAG_TAG))->setVisible(false);
					}
				}
			}
		}

		if (cell->getChildByTag(M_ROLESELECT_FLAG_TAG))
		{
			cell->getChildByTag(M_ROLESELECT_FLAG_TAG)->removeFromParent();
		}
	}

	auto cell = generalList_tableView->cellAtIndex(idx+1);
	if (!cell)
		return;

	//ѡ��״̬
	auto sprite_select = Sprite::create("res_ui/jineng/jinengdi_1_on.png");
	sprite_select->setAnchorPoint(Vec2(0.5f,0.5f));
	sprite_select->setPosition(Vec2(143+105*index/2,55));
	sprite_select->setTag(M_ROLESELECT_FLAG_TAG);
	cell->addChild(sprite_select);
	sprite_select->setLocalZOrder(-1);
	sprite_select->setVisible(true);
}

void GeneralMusouSkillListUI::AddSelectImageForRole( std::string skillId )
{
	//ɾ���佫���ܵ�ѡ��״̬
	for(int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
	{
		auto cell = generalList_tableView->cellAtIndex(i+1);
		if (!cell)
			continue;

		if (cell->getChildByTag(M_ROLESELECT_FLAG_TAG))
		{
			cell->getChildByTag(M_ROLESELECT_FLAG_TAG)->removeFromParent();
		}
	}
	//������＼�ܵ�ѡ�б�ʾ
	int idx = -1;
	for(int i = 0;i<m_studedSkillList.size();i++)
	{
		if (strcmp(skillId.c_str(),m_studedSkillList.at(i)->getId().c_str()) == 0)
		{
			idx = i;
			break;
		}
	}

	if (idx == -1)
		return;

	if (!generalList_tableView->cellAtIndex(0))
		return;

	if(m_scrollView->getContainer()->getChildByTag(M_ROLESKILLITEMBASE_TAG+idx))
	{
		//ѡ��״̬
		if (m_scrollView->getContainer()->getChildByTag(M_ROLESELECT_FLAG_TAG))
		{
			(m_scrollView->getContainer()->getChildByTag(M_ROLESELECT_FLAG_TAG))->setPosition(Vec2(2+105*idx,15));
			(m_scrollView->getContainer()->getChildByTag(M_ROLESELECT_FLAG_TAG))->setVisible(true);
		}
	}
}


/////////////////////////////////////////////////////////

GeneralsMusouSkillItem::GeneralsMusouSkillItem():
curGeneralId(-1)
{

}

GeneralsMusouSkillItem::~GeneralsMusouSkillItem()
{
	delete m_curShortCut;
}

GeneralsMusouSkillItem* GeneralsMusouSkillItem::create( long long genenralid,CShortCut * shortCut )
{
	auto generalsMusouSkillItem = new GeneralsMusouSkillItem();
	if (generalsMusouSkillItem && generalsMusouSkillItem->init(genenralid,shortCut))
	{
		generalsMusouSkillItem->autorelease();
		return generalsMusouSkillItem;
	}
	CC_SAFE_DELETE(generalsMusouSkillItem);
	return NULL;
}

bool GeneralsMusouSkillItem::init( long long genenralid,CShortCut * shortCut )
{
	if (UIScene::init())
	{
		curGeneralId = genenralid;
		// �Layer�λ�TableView�֮�ڣ�Ϊ����ȷ��ӦTableView���϶��¼������Ϊ���̵����¼
		//m_pLayer->setSwallowsTouches(false);

        m_curShortCut = new CShortCut();
		m_curShortCut->CopyFrom(*shortCut);

		auto baseSkill = StaticDataBaseSkill::s_baseSkillData[shortCut->skillpropid()];
		CCAssert(baseSkill != NULL, "baseSkill should not be null");

		CCAssert(shortCut->has_skillpropid(), "baseSkill should not be null");

		m_skillId = shortCut->skillpropid();
		//�
		std::string frameColorPath;
		if (baseSkill->get_quality() == 1)
		{
			frameColorPath = GENERALSKILLWhiteFramePath;
		}
		else if (baseSkill->get_quality()  == 2)
		{
			frameColorPath = GENERALSKILLGreenFramePath;
		}
		else if (baseSkill->get_quality()  == 3)
		{
			frameColorPath = GENERALSKILLBlueFramePath;
		}
		else if (baseSkill->get_quality()  == 4)
		{
			frameColorPath = GENERALSKILLPurpleFramePath;
		}
		else if (baseSkill->get_quality()  == 5)
		{
			frameColorPath = GENERALSKILLOrangeFramePath;
		}
		else
		{
			frameColorPath = GENERALSKILLVoidFramePath;
		}
		btn_bgFrame = Button::create();
		btn_bgFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		btn_bgFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_bgFrame->setPosition(Vec2(42,45));
		btn_bgFrame->setScale(0.9f);
		btn_bgFrame->setTouchEnabled(true);
		//btn_bgFrame->setPressedActionEnabled(true);
		btn_bgFrame->addTouchEventListener(CC_CALLBACK_2(GeneralsMusouSkillItem::DoThing, this));
		m_pLayer->addChild(btn_bgFrame);

		std::string skillIconPath = "res_ui/jineng_icon/";

// 		if (strcmp(shortCut->skillpropid().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 			||strcmp(shortCut->skillpropid().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 			||strcmp(shortCut->skillpropid().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 			||strcmp(shortCut->skillpropid().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 		{
// 			skillIconPath.append("jineng_2");
// 		}
// 		else
// 		{
			if (baseSkill)
			{
				skillIconPath.append(baseSkill->icon());
			}
			else
			{
				skillIconPath.append("jineng_2");
			}
//		}
		skillIconPath.append(".png");


		auto imageView_skill = Button::create();
		imageView_skill->loadTextures(skillIconPath.c_str(),skillIconPath.c_str(),"");
		imageView_skill->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_skill->setPosition(Vec2(btn_bgFrame->getPosition().x,btn_bgFrame->getPosition().y));
		imageView_skill->setTouchEnabled(true);
		imageView_skill->setScale(0.9f);
		imageView_skill->addTouchEventListener(CC_CALLBACK_2(GeneralsMusouSkillItem::DoThing, this));
		m_pLayer->addChild(imageView_skill);
		//����ֵ
		auto imageView_nameFrame = ImageView::create();
		imageView_nameFrame->loadTexture("res_ui/LV4_di80.png");
		imageView_nameFrame->setScale9Enabled(true);
		imageView_nameFrame->setContentSize(Size(83,23));
		imageView_nameFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_nameFrame->setPosition(Vec2(42,12));
		m_pLayer->addChild(imageView_nameFrame);
		//����
		auto l_name = Label::createWithTTF(baseSkill->name().c_str(), APP_FONT_NAME, 16);
		l_name->setAnchorPoint(Vec2(0.5f,0.5f));
		l_name->setPosition(Vec2(42,12));
		m_pLayer->addChild(l_name);
		//ֵȼ��
		auto imageView_lvFrame = ImageView::create();
		imageView_lvFrame->loadTexture("res_ui/zhezhao80_new.png");
		imageView_lvFrame->setScale9Enabled(true);
		imageView_lvFrame->setContentSize(Size(33,12));
		imageView_lvFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_lvFrame->setPosition(Vec2(66,30));
		m_pLayer->addChild(imageView_lvFrame);
		//׵ȼ�
		std::string str_lvstr = "LV";
		char str_lv [10];
		sprintf(str_lv,"%d",shortCut->skilllevel());
		str_lvstr.append(str_lv);
		auto l_lv = Label::createWithTTF(str_lvstr.c_str(), APP_FONT_NAME, 12);
		l_lv->setAnchorPoint(Vec2(0.5f,0.5f));
		l_lv->setPosition(Vec2(-1,0));
		imageView_lvFrame->addChild(l_lv);

		//this->setTouchEnabled(false);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(83,143));
		return true;
	}
	return false;
}

void GeneralsMusouSkillItem::DoThing(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		RefreshGeneralSkillInfo(m_skillId);
		auto skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
		if (!skillScene)
			return;

		//���õ�ǰѡ�е����佫
		skillScene->musouSkillLayer->isRoleOrGeneral = false;
		//���õ�ǰѡ�еļ�����Ϣ
		skillScene->musouSkillLayer->generalMusouSkillListUI->setSelectShortCut(m_curShortCut);
		//����ѡ��״̬
		skillScene->musouSkillLayer->generalMusouSkillListUI->addSelectImageForGeneral(curGeneralId, m_curShortCut->index());
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsMusouSkillItem::RefreshGeneralSkillInfo( std::string skillid )
{
	auto skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(!skillScene)
		return;

	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
	{
		if (strcmp(skillid.c_str(),it->second->getId().c_str()) == 0)
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(), it->second->getLevel(), GameActor::type_player);
			skillScene->musouSkillLayer->RefreshSkillInfo(it->second->getLevel(),gameFightSkill);
			delete gameFightSkill;
			break;
		}
	}	
}

void GeneralsMusouSkillItem::setFrameVisible( bool isVisible )
{
	btn_bgFrame->setVisible(isVisible);
}


//////////////////////////////////////////////////////
RoleSkillItem::RoleSkillItem():
m_index(-1)
{

}

RoleSkillItem::~RoleSkillItem()
{
	delete curGameFightSkill;
}

RoleSkillItem* RoleSkillItem::create( GameFightSkill * gameFightSkill )
{
	auto roleSkillItem = new RoleSkillItem();
	if (roleSkillItem && roleSkillItem->init(gameFightSkill))
	{
		roleSkillItem->autorelease();
		return roleSkillItem;
	}
	CC_SAFE_DELETE(roleSkillItem);
	return NULL;
}

bool RoleSkillItem::init( GameFightSkill * gameFightSkill )
{
	if(UIScene::init())
	{
		if (!gameFightSkill)
		 	return NULL;

		// �Layer�λ�TableView�֮�ڣ�Ϊ����ȷ��ӦTableView���϶��¼������Ϊ���̵����¼
		//m_pLayer->setSwallowsTouches(false);

		curGameFightSkill = new GameFightSkill();
		curGameFightSkill->initSkill(gameFightSkill->getId(),gameFightSkill->getId(),gameFightSkill->getLevel(),GameActor::type_player);
		 
		//�ײ�������Ƶĵ�ͼ           
		auto sprite_skillNameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_di80.png");
		sprite_skillNameFrame->setIgnoreAnchorPointForPosition(false);
		sprite_skillNameFrame->setPreferredSize(Size(83,23));
		sprite_skillNameFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
		sprite_skillNameFrame->setPosition(Vec2(67, 12));
		addChild(sprite_skillNameFrame);
		//������
		auto label_skillName = Label::createWithTTF(gameFightSkill->getCBaseSkill()->name().c_str(),APP_FONT_NAME,16);
		label_skillName->setAnchorPoint(Vec2(0.5f,0.5f));
		label_skillName->setPosition(Vec2(42,12));
		sprite_skillNameFrame->addChild(label_skillName);

// 		Sprite * s_skillIconFrame = Sprite::create("res_ui/jineng/jinengdi_1.png");
// 		s_skillIconFrame->setAnchorPoint(Vec2(0.5f,0.5f));
// 		s_skillIconFrame->setPosition(Vec2(67,50));
// 		addChild(s_skillIconFrame);
		auto btn_di = Button::create();
		btn_di->loadTextures("res_ui/jineng/jinengdi_1.png","res_ui/jineng/jinengdi_1.png","");
		btn_di->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_di->setPosition(Vec2(67,50));
		btn_di->setTouchEnabled(true);
		btn_di->addTouchEventListener(CC_CALLBACK_2(RoleSkillItem::DoThing, this));
		m_pLayer->addChild(btn_di);
		//Ƽ���ͼ�
		std::string skillIconPath = "res_ui/jineng_icon/";
		 
		auto baseSkill = gameFightSkill->getCBaseSkill();
		if (baseSkill)
		{
// 		 	if (strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 		 		||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 		 		||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 		 		||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 		 	{
// 		 		skillIconPath.append("jineng_2");
// 		 	}
// 		 	else
// 		 	{
		 		skillIconPath.append(baseSkill->icon());
//		 	}
		}
		else
		{
		 	skillIconPath.append("jineng_2");
		}
		skillIconPath.append(".png");
		auto sprite_skillIcon = Sprite::create(skillIconPath.c_str());
		sprite_skillIcon->setAnchorPoint(Vec2(.5f,.5f));
		sprite_skillIcon->setPosition(Vec2(67,51));
		addChild(sprite_skillIcon);
		//�߹
		auto sprite_highLight = Sprite::create("res_ui/jineng/gaoguang_1.png");
		sprite_highLight->setAnchorPoint(Vec2(.5f,.5f));
		sprite_highLight->setPosition(Vec2(67,52));
		addChild(sprite_highLight);
		//����
		auto sprite_lvFrame = Sprite::create("res_ui/jineng/jineng_zhezhao.png");
		sprite_lvFrame->setAnchorPoint(Vec2(.5f,0));
		sprite_lvFrame->setPosition(Vec2(67,28));
		addChild(sprite_lvFrame);
		//ֵȼ�
		std::string str_lvValue;
		char str_lv [20];
		sprintf(str_lv,"%d",gameFightSkill->getLevel());
		str_lvValue.append(str_lv);
		str_lvValue.append("/10");
		auto label_skillLV = Label::createWithBMFont("res_ui/font/ziti_3.fnt", str_lvValue.c_str());
		label_skillLV->setAnchorPoint(Vec2(0.5f,0.5f));
		label_skillLV->setPosition(Vec2(68,35));
		label_skillLV->setScale(0.6f);
		addChild(label_skillLV);
		

		//this->setTouchEnabled(false);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(83,143));

		return true;
	}
	return false;
}

void RoleSkillItem::DoThing(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		RefreshRoleSkillInfo(curGameFightSkill->getLevel(), curGameFightSkill);

		auto skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
		if (!skillScene)
			return;

		//���õ�ǰѡ�е�����
		skillScene->musouSkillLayer->isRoleOrGeneral = true;
		//����õ�ǰѡ�еļ�����Ϣ
		skillScene->musouSkillLayer->curSelectRoleSkillSkillId = curGameFightSkill->getId();
		//����ѡ��״̬
		skillScene->musouSkillLayer->generalMusouSkillListUI->AddSelectImageForRole(curGameFightSkill->getId());
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RoleSkillItem::RefreshRoleSkillInfo( int curLevel,GameFightSkill * gameFightSkill )
{
	auto skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(!skillScene)
		return;

	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	skillScene->musouSkillLayer->RefreshSkillInfo(gameFightSkill->getLevel(),gameFightSkill);
}

void RoleSkillItem::setIndex( int idx )
{
	m_index = idx;
}

int RoleSkillItem::getIndex()
{
	return m_index;
}
