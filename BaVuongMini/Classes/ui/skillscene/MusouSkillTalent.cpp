#include "MusouSkillTalent.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CMusouTalent.h"
#include "../../messageclient/protobuf/PlayerMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../AppMacros.h"
#include "../../GameView.h"
#include "SkillScene.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../extensions/ShowSystemInfo.h"
#include "../../gamescene_state/GameSceneEffects.h"

#define  MusouSkillTalentItemBaseTag 234
#define  M_SCROLLVIEWTAG 587

MusouSkillTalent::MusouSkillTalent():
curSelectIndex(1)
{
	for (int m = 2;m>=0;m--)
	{
		for (int n = 3;n>=0;n--)
		{
			int _m = 2-m;
			int _n = 3-n;

			Coordinate temp ;
			temp.x = 132+_n*88;
			temp.y = 65+m*120;

			CoordinateVector.push_back(temp);
		}
	}
}


MusouSkillTalent::~MusouSkillTalent()
{
	std::vector<CMusouTalent*>::iterator iter;
	for (iter = curMusouTalentList.begin(); iter != curMusouTalentList.end(); ++iter)
	{
		delete *iter;
	}
	curMusouTalentList.clear();
}

MusouSkillTalent* MusouSkillTalent::create()
{
	auto musouSkillTalent = new MusouSkillTalent();
	if (musouSkillTalent && musouSkillTalent->init())
	{
		musouSkillTalent->autorelease();
		return musouSkillTalent;
	}
	CC_SAFE_DELETE(musouSkillTalent);
	return NULL;
}

bool MusouSkillTalent::init()
{
	if (UIScene::init())
	{
		auto winsize = Director::getInstance()->getVisibleSize();

		//����츳��
		panel_musouTalent = (Layout*)Helper::seekWidgetByName(LoadSceneLayer::SkillSceneLayer,"Panel_inborn");
		panel_musouTalent->setVisible(true);
		
		layer_musouTalent= Layer::create();
		addChild(layer_musouTalent);
		layer_musouTalent->setVisible(true);
		//����˵�
		auto btn_info = (Button *)Helper::seekWidgetByName(panel_musouTalent,"Button_explanation");
		btn_info->setTouchEnabled(true);
		btn_info->addTouchEventListener(CC_CALLBACK_2(MusouSkillTalent::InfoEvent, this));
		btn_info->setPressedActionEnabled(true);
		//��
		auto btn_upgrade = (Button *)Helper::seekWidgetByName(panel_musouTalent,"Button_upgrade");
		btn_upgrade->setTouchEnabled(true);
		btn_upgrade->addTouchEventListener(CC_CALLBACK_2(MusouSkillTalent::UpgradeEvent, this));
		btn_upgrade->setPressedActionEnabled(true);
		
		l_essenceValue = (Text*)Helper::seekWidgetByName(panel_musouTalent,"Label_3703");
		
		ImageView_select = (ImageView *)Helper::seekWidgetByName(panel_musouTalent,"ImageView_talentSelect");
		ImageView_select->setAnchorPoint(Vec2(0.5f,0.5f));
		ImageView_select->setScale(0.9f);
		ImageView_select->setVisible(true);

		//refresh data
		std::map<int,int>::iterator iter_musouTalent;
		for (iter_musouTalent = GameView::getInstance()->m_musouTalentList.begin(); iter_musouTalent != GameView::getInstance()->m_musouTalentList.end(); ++iter_musouTalent)
		{
			MusouTalentConfigData::IdAndLevel temp;
			temp.idx = iter_musouTalent->first;
			if (iter_musouTalent->second == 0)
			{
				temp.level = 1;
			}
			else
			{
				temp.level = iter_musouTalent->second;
			}

			std::map<MusouTalentConfigData::IdAndLevel,CMusouTalent*>::const_iterator cIter;
			cIter = MusouTalentConfigData::s_musouTalentMsg.find(temp);
			if (cIter == MusouTalentConfigData::s_musouTalentMsg.end()) // �û�ҵ�����ָ�END��  
			{
			}
			else
			{
				auto temp = new CMusouTalent;
				temp->copyFrom(cIter->second);
				temp->set_level(iter_musouTalent->second);
				curMusouTalentList.push_back(temp);
			}
		}
		//test by yangjun 2014.3.25
// 		for (int i = 0;i<13;++i)
// 		{
// 			MusouTalentConfigData::IdAndLevel temp;
// 			temp.id = i+1;
// 			temp.level = 1;
// 			 
// 			std::map<MusouTalentConfigData::IdAndLevel,CMusouTalent*>::const_iterator cIter;
// 			cIter = MusouTalentConfigData::s_musouTalentMsg.find(temp);
// 			if (cIter == MusouTalentConfigData::s_musouTalentMsg.end()) // �û�ҵ�����ָ�END��  
// 			{
// 			}
// 			else
// 			{
// 			 	auto temp = new CMusouTalent;
// 			 	temp->copyFrom(cIter->second);
// 			 	curMusouTalentList.push_back(temp);
// 			}
// 		}
		//refresh ui
		for (int i =0;i<curMusouTalentList.size();++i)
		//for (int i =0;i<8;++i)
		{
			if (i >= 12)
				continue;

			auto musouTalentItem = MusouTalentItem::create(curMusouTalentList.at(i));
			musouTalentItem->setPosition(Vec2(CoordinateVector.at(curMusouTalentList.at(i)->get_idx()-1).x,CoordinateVector.at(curMusouTalentList.at(i)->get_idx()-1).y));
			musouTalentItem->setTag(MusouSkillTalentItemBaseTag+curMusouTalentList.at(i)->get_idx());
			layer_musouTalent->addChild(musouTalentItem);
		}

		if (curMusouTalentList.size()>0)
		{
			//�Ĭ��ѡ�е�һ�
			setSelectImagePos(curSelectIndex);
			RefreshTalentInfo(curMusouTalentList.at(0));
			RefreshTalentItemLevel(curMusouTalentList.at(0));
			RefreshEssenceValue();

		}

		return true;
	}
	return false;
}

void MusouSkillTalent::onEnter()
{
	UIScene::onEnter();
}

void MusouSkillTalent::onExit()
{
	UIScene::onExit();
}


void MusouSkillTalent::setVisible( bool visible )
{
	panel_musouTalent->setVisible(visible);
	layer_musouTalent->setVisible(visible);
}

void MusouSkillTalent::InfoEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//�����츳��Ϣ���
		Size winSize = Director::getInstance()->getVisibleSize();
		std::map<int, std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(3);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end()) // �û�ҵ�����ָ�END��  
		{
			return;
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			{
				auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[3].c_str());
				GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				showSystemInfo->setIgnoreAnchorPointForPosition(false);
				showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				showSystemInfo->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void MusouSkillTalent::UpgradeEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		for (int i = 0; i < curMusouTalentList.size(); ++i)
		{
			if (curSelectIndex == curMusouTalentList.at(i)->get_idx())
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5121, (void *)curMusouTalentList.at(i)->get_idx());
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MusouSkillTalent::setSelectImagePos( int index )
{
	ImageView_select->setVisible(true);
	ImageView_select->setPosition(Vec2(CoordinateVector.at(index-1).x+42,CoordinateVector.at(index-1).y+50));
}

void MusouSkillTalent::RefreshTalentItemLevel( CMusouTalent * musouTalent )
{
	auto temp  = (MusouTalentItem*)layer_musouTalent->getChildByTag(MusouSkillTalentItemBaseTag+musouTalent->get_idx());
	if (!temp)
		return;

	temp->RefreshLevel(musouTalent->get_level());
	temp->RefreshCurMusouTalent(musouTalent);
	
}

void MusouSkillTalent::RefreshTalentInfo( CMusouTalent * musouTalent )
{
	if (layer_musouTalent->getChildByTag(M_SCROLLVIEWTAG) != NULL)
	{
		layer_musouTalent->getChildByTag(M_SCROLLVIEWTAG)->removeFromParent();
	}

	int scroll_height = 0;
	//˼�����
	auto s_nameFrame = Sprite::create("res_ui/moji_0.png");
	s_nameFrame->setScaleY(0.6f);
	auto l_name = Label::createWithTTF(musouTalent->get_name().c_str(),APP_FONT_NAME,18);
	l_name->setColor(Color3B(196,255,68));
	int zeroLineHeight = s_nameFrame->getContentSize().height;
	scroll_height = zeroLineHeight;
	//
	auto s_first_line = Sprite::create("res_ui/henggang.png");
	s_first_line->setScaleX(0.9f);
	int zeroLineHeight_1 = zeroLineHeight + s_first_line->getContentSize().height+6;
	scroll_height = zeroLineHeight_1;

	//Ƽ�
	auto t_des = Label::createWithTTF(musouTalent->get_description().c_str(),APP_FONT_NAME,16,Size(230, 0 ), TextHAlignment::LEFT);
	t_des->setColor(Color3B(30,255,0));
	int firstLineHeight = zeroLineHeight_1+t_des->getContentSize().height;
	scroll_height = firstLineHeight;

	/////////////////////////////////////////////////////
	//int fifthLineHeight = 0;
	int secondLineHeight = 0;
	//int sixthLineHeight = 0;
	int thirdLineHeight = 0;
	//int seventhLineHeight = 0;
	int fouthLineHeight = 0;
	//int seventhLineHeight_1 = 0;
	int fouthLineHeight_1 = 0;
	//int eighthLineHeight = 0;
	int fifthLineHeight = 0;
	//int ninthLineHeight = 0;
	int sixthLineHeight = 0;
	if (musouTalent->get_level()<10)
	{
		//���һ��
		if (musouTalent->get_level() == 0)
		{
			secondLineHeight = firstLineHeight;
			scroll_height = secondLineHeight;

			thirdLineHeight = secondLineHeight;
			scroll_height = thirdLineHeight;
			/////////////////////////////////////////////////////
			//�����
			s_upgradeNeed = Sprite::create("res_ui/moji_0.png");
			s_upgradeNeed->setScaleY(0.6f);
			//const char *shenjixuqiu  = ((__String*)strings->objectForKey("skillinfo_shengjixuqiu"))->m_sString.c_str();
			const char *xuexixuqiu = StringDataManager::getString("skillinfo_xuexixuqiu");
			char* skillinfo_xuexixuqiu =const_cast<char*>(xuexixuqiu);
			l_upgradeNeed = Label::createWithTTF(skillinfo_xuexixuqiu,APP_FONT_NAME,18);
			l_upgradeNeed->setColor(Color3B(196,255,68));
			fouthLineHeight = thirdLineHeight + s_upgradeNeed->getContentSize().height;
			scroll_height = fouthLineHeight;

			//////////////////////////////////////////////////////
			//
			s_second_line = Sprite::create("res_ui/henggang.png");
			s_second_line->setScaleX(0.9f);
			fouthLineHeight_1 = fouthLineHeight + s_first_line->getContentSize().height+7;
			scroll_height = fouthLineHeight_1;

			/////////////////////////////////////////////////////
			//���ǵȼ�
			if (musouTalent->get_require_level()>0)
			{
				std::string str_lv_des = StringDataManager::getString("skillinfo_zhujuedengji");
				str_lv_des.append(StringDataManager::getString("skillinfo_maohao"));
				l_playerLv = Label::createWithTTF(str_lv_des.c_str(),APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
				l_playerLv->setColor(Color3B(212,255,151));
				char s_required_level[5];
				sprintf(s_required_level,"%d",musouTalent->get_require_level());
				l_playerLvValue = Label::createWithTTF(s_required_level,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
				l_playerLvValue->setColor(Color3B(212,255,151));

				fifthLineHeight = fouthLineHeight_1 + l_playerLv->getContentSize().height;
				scroll_height = fifthLineHeight;

				if (GameView::getInstance()->myplayer->getActiveRole()->level()<musouTalent->get_require_level())  //����ȼ�δ�ﵽ������Ҫ�
				{
					l_playerLv->setColor(Color3B(255,51,51));
					l_playerLvValue->setColor(Color3B(255,51,51));
				}
			}
			else
			{
				fifthLineHeight = fouthLineHeight_1;
				scroll_height = fifthLineHeight;
			}
			//���꾫��
			if (musouTalent->get_require_essence()>0)
			{
				const char *longhunjinghua = StringDataManager::getString("musouTalentSkillinfo_musou");
				char* skillinfo_longhunjinghua =const_cast<char*>(longhunjinghua);
				l_skillPoint = Label::createWithTTF(skillinfo_longhunjinghua,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
				l_skillPoint->setColor(Color3B(212,255,151));
				char s_required_point[10];
				sprintf(s_required_point,"%d",musouTalent->get_require_essence());
				l_skillPointValue = Label::createWithTTF(s_required_point,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
				l_skillPointValue->setColor(Color3B(212,255,151));

				sixthLineHeight = fifthLineHeight + l_skillPoint->getContentSize().height;
				scroll_height = sixthLineHeight;

// 				if(GameView::getInstance()->myplayer->player->skillpoint()<curFightSkill->getCFightSkill()->get_required_point())//���ܵ㲻�
// 				{
// 					l_skillPoint->setColor(Color3B(255,51,51));
// 					l_skillPointValue->setColor(Color3B(255,51,51));
// 				}
			}
			else
			{
				sixthLineHeight = fifthLineHeight;
				scroll_height = sixthLineHeight;
			}
		}
		else
		{

			MusouTalentConfigData::IdAndLevel temp;
			temp.idx = musouTalent->get_idx();
			temp.level = musouTalent->get_level()+1;
			std::map<MusouTalentConfigData::IdAndLevel,CMusouTalent*>::const_iterator cIter;
			cIter = MusouTalentConfigData::s_musouTalentMsg.find(temp);
			if (cIter == MusouTalentConfigData::s_musouTalentMsg.end()) // �û�ҵ�����ָ�END��  
			{
				
			}
			else
			{
				auto nextMusouTalent = new CMusouTalent();
				nextMusouTalent->copyFrom(cIter->second);

				const char *xiayiji = StringDataManager::getString("skillinfo_xiayiji");
				char* skillinfo_xiayiji =const_cast<char*>(xiayiji);
				l_nextLv = Label::createWithTTF(skillinfo_xiayiji,APP_FONT_NAME,16,Size(100, 0 ), TextHAlignment::LEFT);
				l_nextLv->setColor(Color3B(0,255,255));

				secondLineHeight = firstLineHeight+l_nextLv->getContentSize().height*2;
				scroll_height = secondLineHeight;
				/////////////////////////////////////////////////////
				t_nextLvDes = Label::createWithTTF(nextMusouTalent->get_description().c_str(),APP_FONT_NAME,16,Size(230, 0 ), TextHAlignment::LEFT);
				t_nextLvDes->setColor(Color3B(0,255,255));

				thirdLineHeight = secondLineHeight+t_nextLvDes->getContentSize().height;
				scroll_height = thirdLineHeight;
				/////////////////////////////////////////////////////
				//������
				s_upgradeNeed = Sprite::create("res_ui/moji_0.png");
				s_upgradeNeed->setScaleY(0.6f);
				//const char *shenjixuqiu  = ((__String*)strings->objectForKey("skillinfo_shengjixuqiu"))->m_sString.c_str();
				const char *shenjixuqiu = StringDataManager::getString("skillinfo_shengjixuqiu");
				char* skillinfo_shenjixuqiu =const_cast<char*>(shenjixuqiu);
				l_upgradeNeed = Label::createWithTTF(skillinfo_shenjixuqiu,APP_FONT_NAME,18);
				l_upgradeNeed->setColor(Color3B(196,255,68));
				fouthLineHeight = thirdLineHeight + s_upgradeNeed->getContentSize().height;
				scroll_height = fouthLineHeight;

				//////////////////////////////////////////////////////
				//
				s_second_line = Sprite::create("res_ui/henggang.png");
				s_second_line->setScaleX(0.9f);
				fouthLineHeight_1 = fouthLineHeight + s_first_line->getContentSize().height+7;
				scroll_height = fouthLineHeight_1;

				/////////////////////////////////////////////////////
				//���ǵȼ�
				if (nextMusouTalent->get_require_level()>0)
				{
					std::string str_lv_des = StringDataManager::getString("skillinfo_zhujuedengji");
					str_lv_des.append(StringDataManager::getString("skillinfo_maohao"));
					l_playerLv = Label::createWithTTF(str_lv_des.c_str(),APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
					l_playerLv->setColor(Color3B(212,255,151));
					char s_next_required_level[5];
					sprintf(s_next_required_level,"%d",nextMusouTalent->get_require_level());
					l_playerLvValue = Label::createWithTTF(s_next_required_level,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
					l_playerLvValue->setColor(Color3B(212,255,151));

					fifthLineHeight = fouthLineHeight_1 + l_playerLv->getContentSize().height;
					scroll_height = fifthLineHeight;

					if (GameView::getInstance()->myplayer->getActiveRole()->level()<nextMusouTalent->get_require_level())  //����ȼ�δ�ﵽ������Ҫ�
					{
						l_playerLv->setColor(Color3B(255,51,51));
						l_playerLvValue->setColor(Color3B(255,51,51));
					}
				}
				else
				{
					fifthLineHeight = fouthLineHeight_1;
					scroll_height = fifthLineHeight;
				}
				//���꾫��
				if (nextMusouTalent->get_require_essence()>0)
				{
					const char *longhunjinghua = StringDataManager::getString("musouTalentSkillinfo_musou");
					char* skillinfo_longhunjinghua =const_cast<char*>(longhunjinghua);
					l_skillPoint = Label::createWithTTF(skillinfo_longhunjinghua,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
					l_skillPoint->setColor(Color3B(212,255,151));
					char s_next_required_musou[10];
					sprintf(s_next_required_musou,"%d",nextMusouTalent->get_require_essence());
					l_skillPointValue = Label::createWithTTF(s_next_required_musou,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
					l_skillPointValue->setColor(Color3B(212,255,151));

					sixthLineHeight = fifthLineHeight + l_skillPoint->getContentSize().height;
					scroll_height = sixthLineHeight;

// 					if(GameView::getInstance()->myplayer->player->skillpoint()<curFightSkill->getCFightSkill()->get_next_required_point())//���ܵ㲻�
// 					{
// 						l_skillPoint->setColor(Color3B(255,51,51));
// 						l_skillPointValue->setColor(Color3B(255,51,51));
// 					}
				}
				else
				{
					sixthLineHeight = fifthLineHeight;
					scroll_height = sixthLineHeight;
				}
				
			}
		}
	}

	auto m_scrollView = cocos2d::extension::ScrollView::create(Size(243,240));
	m_scrollView->setViewSize(Size(243, 240));
	m_scrollView->setIgnoreAnchorPointForPosition(false);
	m_scrollView->setTouchEnabled(true);
	m_scrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
	m_scrollView->setAnchorPoint(Vec2(0,0));
	m_scrollView->setPosition(Vec2(504,159));
	m_scrollView->setBounceable(true);
	m_scrollView->setClippingToBounds(true);
	m_scrollView->setTag(M_SCROLLVIEWTAG);
	layer_musouTalent->addChild(m_scrollView);

	if (scroll_height > 212)
	{
		m_scrollView->setContentSize(Size(243,scroll_height));
		m_scrollView->setContentOffset(Vec2(0,240-scroll_height));  
	}
	else
	{
		m_scrollView->setContentSize(Size(243,240));
	}
	m_scrollView->setClippingToBounds(true);

	s_nameFrame->setIgnoreAnchorPointForPosition(false);
	s_nameFrame->setAnchorPoint(Vec2(0,0));
	m_scrollView->addChild(s_nameFrame);
	s_nameFrame->setPosition(Vec2(8,m_scrollView->getContentSize().height - zeroLineHeight));

	m_scrollView->addChild(l_name);
	l_name->setPosition(Vec2(15,m_scrollView->getContentSize().height - zeroLineHeight));

	m_scrollView->addChild(s_first_line);
	s_first_line->setPosition(Vec2(5,m_scrollView->getContentSize().height - zeroLineHeight_1+5));

	t_des->setIgnoreAnchorPointForPosition( false );  
	t_des->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(t_des);
	t_des->setPosition(Vec2(8,m_scrollView->getContentSize().height - firstLineHeight));


	if (musouTalent->get_level()<10)
	{
		//���һ��
		if (musouTalent->get_level() == 0)
		{
			s_upgradeNeed->setIgnoreAnchorPointForPosition( false );  
			s_upgradeNeed->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(s_upgradeNeed);
			s_upgradeNeed->setPosition(Vec2(8,m_scrollView->getContentSize().height - fouthLineHeight));

			m_scrollView->addChild(l_upgradeNeed);
			l_upgradeNeed->setPosition(Vec2(15,m_scrollView->getContentSize().height - fouthLineHeight));

			m_scrollView->addChild(s_second_line);
			s_second_line->setPosition(Vec2(5,m_scrollView->getContentSize().height - fouthLineHeight_1+5));

			//��ǵȼ�
			if (musouTalent->get_require_level()>0)
			{
				l_playerLv->setIgnoreAnchorPointForPosition( false );  
				l_playerLv->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_playerLv);
				l_playerLv->setPosition(Vec2(8,m_scrollView->getContentSize().height - fifthLineHeight));

				l_playerLvValue->setIgnoreAnchorPointForPosition( false );  
				l_playerLvValue->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_playerLvValue);
				l_playerLvValue->setPosition(Vec2(8+l_playerLv->getContentSize().width,m_scrollView->getContentSize().height - fifthLineHeight));
			}
			//��꾫��
			if (musouTalent->get_require_essence()>0)
			{
				l_skillPoint->setIgnoreAnchorPointForPosition( false );  
				l_skillPoint->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_skillPoint);
				l_skillPoint->setPosition(Vec2(8,m_scrollView->getContentSize().height - sixthLineHeight));

				l_skillPointValue->setIgnoreAnchorPointForPosition( false );  
				l_skillPointValue->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_skillPointValue);
				l_skillPointValue->setPosition(Vec2(8+l_skillPoint->getContentSize().width,m_scrollView->getContentSize().height - sixthLineHeight));
			}
		}
		else
		{
			l_nextLv->setIgnoreAnchorPointForPosition( false );  
			l_nextLv->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(l_nextLv);
			l_nextLv->setPosition(Vec2(8,m_scrollView->getContentSize().height - secondLineHeight));

			t_nextLvDes->setIgnoreAnchorPointForPosition( false );  
			t_nextLvDes->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(t_nextLvDes);
			t_nextLvDes->setPosition(Vec2(8,m_scrollView->getContentSize().height - thirdLineHeight));

			s_upgradeNeed->setIgnoreAnchorPointForPosition( false );  
			s_upgradeNeed->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(s_upgradeNeed);
			s_upgradeNeed->setPosition(Vec2(8,m_scrollView->getContentSize().height - fouthLineHeight));

			m_scrollView->addChild(l_upgradeNeed);
			l_upgradeNeed->setPosition(Vec2(15,m_scrollView->getContentSize().height - fouthLineHeight));

			m_scrollView->addChild(s_second_line);
			s_second_line->setPosition(Vec2(5,m_scrollView->getContentSize().height - fouthLineHeight_1+5));

			//��ǵȼ�
			if (musouTalent->get_require_level()>0)
			{
				l_playerLv->setIgnoreAnchorPointForPosition( false );  
				l_playerLv->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_playerLv);
				l_playerLv->setPosition(Vec2(8,m_scrollView->getContentSize().height - fifthLineHeight));

				l_playerLvValue->setIgnoreAnchorPointForPosition( false );  
				l_playerLvValue->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_playerLvValue);
				l_playerLvValue->setPosition(Vec2(8+l_playerLv->getContentSize().width,m_scrollView->getContentSize().height - fifthLineHeight));
			}
			//��꾫��
			if (musouTalent->get_require_essence()>0)
			{
				l_skillPoint->setIgnoreAnchorPointForPosition( false );  
				l_skillPoint->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_skillPoint);
				l_skillPoint->setPosition(Vec2(8,m_scrollView->getContentSize().height - sixthLineHeight));

				l_skillPointValue->setIgnoreAnchorPointForPosition( false );  
				l_skillPointValue->setAnchorPoint( Vec2( 0, 0 ) );  
				m_scrollView->addChild(l_skillPointValue);
				l_skillPointValue->setPosition(Vec2(8+l_skillPoint->getContentSize().width,m_scrollView->getContentSize().height - sixthLineHeight));
			}
		}
	}
}

void MusouSkillTalent::RefreshEssenceValue()
{
	char s_essence[20];
	sprintf(s_essence,"%d",GameView::getInstance()->myplayer->getMusouEssence());
	l_essenceValue->setString(s_essence);
}

void MusouSkillTalent::AddBonusSpecialEffect( CMusouTalent * musouTalent )
{
	auto temp  = (MusouTalentItem*)layer_musouTalent->getChildByTag(MusouSkillTalentItemBaseTag+musouTalent->get_idx());
	if (!temp)
		return;

	// Bonus Special Effect
	auto pNode = BonusSpecialEffect::create();
	pNode->setScale(0.7f);
	pNode->setPosition(Vec2(temp->getPosition().x+45,temp->getPosition().y+50));
	layer_musouTalent->addChild(pNode);
}


/////////////////////////////////////////////////////////

MusouTalentItem::MusouTalentItem()
{
}

MusouTalentItem::~MusouTalentItem()
{
	delete m_curMusouTalent;
}

MusouTalentItem* MusouTalentItem::create( CMusouTalent * musouTalent )
{
	auto musouTalentItem = new MusouTalentItem();
	if (musouTalentItem && musouTalentItem->init(musouTalent))
	{
		musouTalentItem->autorelease();
		return musouTalentItem;
	}
	CC_SAFE_DELETE(musouTalentItem);
	return NULL;
}

bool MusouTalentItem::init( CMusouTalent * musouTalent )
{
	if (UIScene::init())
	{
		// �Layer�λ�TableView�֮�ڣ�Ϊ����ȷ��ӦTableView���϶��¼������Ϊ���̵����¼
		//m_pLayer->setSwallowsTouches(false);

		m_curMusouTalent = new CMusouTalent();
		m_curMusouTalent->copyFrom(musouTalent);

		//�
		std::string frameColorPath = "res_ui/jineng/jinengdi_1.png";
		btn_bgFrame = Button::create();
		btn_bgFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		btn_bgFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_bgFrame->setPosition(Vec2(42,50));
		btn_bgFrame->setTouchEnabled(true);
		btn_bgFrame->addTouchEventListener(CC_CALLBACK_2(MusouTalentItem::DoThing, this));
		m_pLayer->addChild(btn_bgFrame);

		std::string skillIconPath = "res_ui/jineng_icon/";
		if (strcmp(musouTalent->get_icon().c_str(),"") == 0)
		{
			skillIconPath.append("jineng_2");
		}
		else
		{
			skillIconPath.append(musouTalent->get_icon().c_str());
		}
		skillIconPath.append(".png");

		auto imageView_skill = Button::create();
		imageView_skill->loadTextures(skillIconPath.c_str(),skillIconPath.c_str(),"");
		imageView_skill->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_skill->setPosition(Vec2(btn_bgFrame->getPosition().x,btn_bgFrame->getPosition().y));
		imageView_skill->setTouchEnabled(true);
		//imageView_skill->setScale(0.9f);
		imageView_skill->addTouchEventListener(CC_CALLBACK_2(MusouTalentItem::DoThing, this));
		m_pLayer->addChild(imageView_skill);

		//�߹
		auto image_highLight = ImageView::create();
		image_highLight->loadTexture("res_ui/jineng/gaoguang_1.png");
		image_highLight->setAnchorPoint(Vec2(.5f,.5f));
		image_highLight->setPosition(Vec2(btn_bgFrame->getPosition().x,btn_bgFrame->getPosition().y+2));
		m_pLayer->addChild(image_highLight);

		//����ֵ
		auto imageView_nameFrame = ImageView::create();
		imageView_nameFrame->loadTexture("res_ui/LV4_di80.png");
		imageView_nameFrame->setScale9Enabled(true);
		imageView_nameFrame->setContentSize(Size(75,23));
		imageView_nameFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_nameFrame->setPosition(Vec2(42,9));
		m_pLayer->addChild(imageView_nameFrame);
		//����
		auto l_name = Label::createWithTTF(musouTalent->get_name().c_str(), APP_FONT_NAME, 16);
		l_name->setAnchorPoint(Vec2(0.5f,0.5f));
		l_name->setPosition(Vec2(42,9));
		m_pLayer->addChild(l_name);
		//ֵȼ��
		auto imageView_lvFrame = ImageView::create();
		imageView_lvFrame->loadTexture("res_ui/jineng/jineng_zhezhao.png");
		imageView_lvFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_lvFrame->setPosition(Vec2(42,35));
		m_pLayer->addChild(imageView_lvFrame);
		//׵ȼ�
		std::string str_lvstr = "";
		char str_lv [10];
		sprintf(str_lv,"%d",musouTalent->get_level());
		str_lvstr.append(str_lv);
		str_lvstr.append("/10");
		l_lv = Label::createWithBMFont("res_ui/font/ziti_3.fnt", str_lvstr.c_str());
		l_lv->setScale(0.6f);
		l_lv->setAnchorPoint(Vec2(0.5f,0.5f));
		l_lv->setPosition(Vec2(0,0));
		imageView_lvFrame->addChild(l_lv);

		//this->setTouchEnabled(false);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(83,143));
		return true;
	}
	return false;
}

void MusouTalentItem::DoThing(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
		if (!skillScene)
			return;

		skillScene->musouSkillTalent->RefreshTalentInfo(m_curMusouTalent);
		skillScene->musouSkillTalent->curSelectIndex = m_curMusouTalent->get_idx();
		skillScene->musouSkillTalent->setSelectImagePos(skillScene->musouSkillTalent->curSelectIndex);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

//void MusouTalentItem::RefreshGeneralSkillInfo( std::string skillid )
//{
// 	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
// 	for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
// 	{
// 		if (strcmp(skillid.c_str(),it->second->getId().c_str()) == 0)
// 		{
// 			GameFightSkill * gameFightSkill = new GameFightSkill();
// 			gameFightSkill->initSkill(it->second->getId(), it->second->getId(), it->second->getLevel(), GameActor::type_player);
// 			skillScene->musouSkillLayer->RefreshSkillInfo(it->second->getLevel(),gameFightSkill);
// 			delete gameFightSkill;
// 			break;
// 		}
// 	}	
//}

void MusouTalentItem::setFrameVisible( bool isVisible )
{
	btn_bgFrame->setVisible(isVisible);
}

void MusouTalentItem::RefreshLevel( int _value )
{
	m_curMusouTalent->set_level(_value);

	std::string str_lvstr = "";
	char str_lv [10];
	sprintf(str_lv,"%d",_value);
	str_lvstr.append(str_lv);
	str_lvstr.append("/10");
	l_lv->setString(str_lvstr.c_str());
}

void MusouTalentItem::RefreshCurMusouTalent( CMusouTalent * temp )
{
	m_curMusouTalent->copyFrom(temp);
}

