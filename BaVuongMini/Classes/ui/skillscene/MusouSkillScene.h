#ifndef _SKILLUI_MUSOUSKILLSCENE_H_
#define _SKILLUI_MUSOUSKILLSCENE_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GameFightSkill;
class GeneralMusouSkillListUI;

class MusouSkillScene : public UIScene
{
public:
	MusouSkillScene();
	~MusouSkillScene();

	static MusouSkillScene* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void setVisible(bool visible);

	void SetEvent(Ref *pSender, Widget::TouchEventType type);
	void SuggestSetEvent(Ref *pSender, Widget::TouchEventType type);

	void RefreshSkillInfo(int curLevel,GameFightSkill * gameFightSkill);/**********����ĳ������Ϣ(�ȼ�)**********/
	//�������ʾ
	void AddMusouFlagWithAnm(std::string skillId);

	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	//ѡ���
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();

public: 
	GeneralMusouSkillListUI *generalMusouSkillListUI;
	//�ѡ��Ľ�ɫ���
	std::string curSelectRoleSkillSkillId;
	//ܱ���ѡ�������ǻ����佫
	bool isRoleOrGeneral;

	Button * btn_set;
	Button * btn_suggestSet;
private:
	Layout * panel_musouSkill;
	Layer * layer_musouSkill;
	Layer * layer_upper;


	//���ͷ�
	ImageView * ImageView_roleheat;
	//�������
	Label * L_roleName;

	//���һ�����
	Label * l_nextLv;
	//���һ��
	Label * t_nextLvDes;
	//����Ҫ
	Sprite * s_upgradeNeed;
	Label * l_upgradeNeed;
	Sprite * s_second_line;
	//����ȼ�
	Label * l_playerLv;
	Label * l_playerLvValue;
	//���ܵ
	Label * l_skillPoint;
	Label * l_skillPointValue;
	//�ǰ�ü��
	Label * l_preSkill;
	Label * l_preSkillValue;
	//ܽ�
	Label * l_gold;
	Label * l_goldValue;
	//ҵ�
	Label * l_prop;
	Label * l_propValue;

//߽�ѧ
	int mTutorialScriptInstanceId;
};

#endif

