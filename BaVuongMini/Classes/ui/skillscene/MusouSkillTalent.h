#ifndef _SKILLUI_MUSOUSKILLTALENT_H_
#define _SKILLUI_MUSOUSKILLTALENT_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CMusouTalent;

struct Coordinate
{
	float x;
	float y;
};


class MusouSkillTalent : public UIScene
{
public:
	MusouSkillTalent();
	~MusouSkillTalent();

	static MusouSkillTalent* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void setVisible(bool visible);

	void InfoEvent(Ref *pSender, Widget::TouchEventType type);
	void UpgradeEvent(Ref *pSender, Widget::TouchEventType type);

	void setSelectImagePos(int index);

	void RefreshTalentItemLevel(CMusouTalent * musouTalent);
	void RefreshTalentInfo(CMusouTalent * musouTalent);
	void RefreshEssenceValue();

	void AddBonusSpecialEffect(CMusouTalent * musouTalent);

public: 
	int curSelectIndex;
	std::vector<CMusouTalent *> curMusouTalentList;
private:
	Layout * panel_musouTalent;
	Layer * layer_musouTalent;

	Text * l_essenceValue;
	ImageView * ImageView_select;
	
	//��
	std::vector<Coordinate> CoordinateVector;


	//���һ�����
	Label * l_nextLv;
	//���һ��
	Label * t_nextLvDes;
	//����Ҫ
	Sprite * s_upgradeNeed;
	Label * l_upgradeNeed;
	Sprite * s_second_line;
	//����ȼ�
	Label * l_playerLv;
	Label * l_playerLvValue;
	//���ܵ
	Label * l_skillPoint;
	Label * l_skillPointValue;
};


//////////////////////////////////////////////////////////
class MusouTalentItem : public UIScene
{
public:
	MusouTalentItem();
	~MusouTalentItem();
	static MusouTalentItem* create(CMusouTalent * musouTalent);
	bool init(CMusouTalent * musouTalent);

	void DoThing(Ref *pSender, Widget::TouchEventType type);
	//void RefreshGeneralSkillInfo(std::string skillid);

	void setFrameVisible(bool isVisible);

	void RefreshLevel(int _value);

	void RefreshCurMusouTalent(CMusouTalent * temp);

private:
	//std::string m_skillId;
	CMusouTalent * m_curMusouTalent;

	Button *btn_bgFrame;
	Label * l_lv;
};

#endif

