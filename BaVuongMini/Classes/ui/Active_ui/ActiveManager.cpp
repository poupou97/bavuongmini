#include "ActiveManager.h"
#include "GameView.h"

#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../utils/StaticDataManager.h"


ActiveManager * ActiveManager::s_activeManager = NULL;
int ActiveManager::s_nStatus = status_none;

ActiveManager::ActiveManager(void)
	:m_bIsUseful(false)
	,m_strMapId("")
	,m_targetPos(Vec2(0,0))
	,m_longTargetNpcId(0)
	,m_bIsTransportFinish(true)
	,m_activeType(-1)
{

}

ActiveManager::~ActiveManager(void)
{

}

ActiveManager * ActiveManager::instance()
{
	if (NULL == s_activeManager)
	{
		s_activeManager = ActiveManager::create();
	}

	return s_activeManager;
}

ActiveManager* ActiveManager::create()
{
	auto pActiveLayer = new ActiveManager();
	if (pActiveLayer && pActiveLayer->init())
	{
		pActiveLayer->autorelease();
		return pActiveLayer;
	}
	CC_SAFE_DELETE(pActiveLayer);
	return NULL;
}

bool ActiveManager::init()
{
	bool bRet=false;
	do 
	{
		CC_BREAK_IF(!Node::init());

		this->scheduleUpdate();

		//GameView::getInstance()->getMainUIScene()->addChild(this);

		bRet=true;

	} while (0);

	return bRet;
}

void ActiveManager::update(float dt)
{
	if (TYPE_PRESENTBOX == this->get_activeType())
	{
		update_presentBox();
	}
	else
	{
		update_searchPath();
	}
}

void ActiveManager::set_targetMapId( std::string strMapId )
{
	this->m_strMapId = strMapId;
}

std::string ActiveManager::get_targetMapId()
{
	return this->m_strMapId;
}

void ActiveManager::set_targetPos( Vec2 targetPos )
{
	this->m_targetPos = targetPos;
}

cocos2d::Vec2 ActiveManager::get_targetpos()
{
	return this->m_targetPos;
}

void ActiveManager::setLayerUserful( bool bFlag )
{
	this->m_bIsUseful = bFlag;
}

void ActiveManager::set_targetNpcId( long long targetNpcId )
{
	this->m_longTargetNpcId = targetNpcId;
}

long long ActiveManager::get_targetNpcId()
{
	return this->m_longTargetNpcId;
}

void ActiveManager::start_update()
{
	this->scheduleUpdate();
}

void ActiveManager::cancel_update()
{
	 this->unscheduleUpdate();
}

void ActiveManager::set_transportFinish( bool bFlag )
{
	this->m_bIsTransportFinish = bFlag;
}

bool ActiveManager::get_transportFinish()
{
	return this->m_bIsTransportFinish;
}

void ActiveManager::set_status( int nStatus )
{
	s_nStatus = nStatus;
}

int ActiveManager::get_status()
{
	return s_nStatus;
}

void ActiveManager::AcrossMapTransport( std::string mapId,Vec2 pos )
{
	set_status(ActiveManager::status_waitForTransport);
	//req acrossMap
	auto t_point = new Vec2(MissionManager::getInstance()->getNearestReachablePos(pos));
	//Vec2 * t_point = new Vec2(pos);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1114,(void *)mapId.c_str(),t_point);
	delete t_point;
}

void ActiveManager::set_activeType( int nActiveType )
{
	this->m_activeType = nActiveType;
}

int ActiveManager::get_activeType()
{
	return this->m_activeType;
}

void ActiveManager::update_searchPath()
{
	if (m_bIsUseful)
	{
		while(ActiveManager::get_status() == ActiveManager::status_transportFinished && !m_bIsTransportFinish)
		{
			//same map
			Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
			Vec2 cp_target = m_targetPos;
			if (cp_role.getDistance(cp_target) < 64)
			{
				m_bIsTransportFinish = true;

				break;
			}
			else
			{
				if (m_targetPos.x == 0 && m_targetPos.y == 0)
				{
					m_bIsTransportFinish = true;

					break;
				}

				auto cmd = new MyPlayerCommandMove();
				// NOT reachable, so find the near one

				cmd->targetPosition = m_targetPos;
				cmd->method = MyPlayerCommandMove::method_searchpath;
				bool bSwitchCommandImmediately = true;
				if(GameView::getInstance()->myplayer->isAttacking())
					bSwitchCommandImmediately = false;
				GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	

				m_bIsTransportFinish = true;

				break;
			}
		}


		if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),m_strMapId.c_str()) == 0)
		{
			Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
			Vec2 cp_target = m_targetPos;
			//CCLOG("%f",sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)));
			if (cp_role.getDistance(cp_target) < 96)
			{
				//GameView::getInstance()->showAlertDialog("activeLayer");

				// ���ߵ�NPC����ʱ �������վ�״̬
				GameView::getInstance()->myplayer->changeAction(ACT_STAND);

				if (GameView::getInstance()->getGameScene()->getActor(m_longTargetNpcId) != NULL)
				{
					GameView::getInstance()->myplayer->setLockedActorId(m_longTargetNpcId);
				}

				// ���������NPC���1129
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1129, (void *)m_longTargetNpcId);

				m_bIsUseful = false;
			}
		}

	}
}

void ActiveManager::update_presentBox()
{
	if (m_bIsUseful)
	{
		if (ActiveManager::get_status() == ActiveManager::status_transportFinished && !m_bIsTransportFinish)
		{
			auto scene = GameView::getInstance()->getGameScene();
			if (NULL != scene)
			{

				auto pSceneState = dynamic_cast<GameSceneState*>(scene->getParent());
				if(pSceneState->isInLoadState())
				{
					return;
				}
					
				// ���ʾ������ͼ�б��䣬��ע����ҡ�
				const char * charInfo = StringDataManager::getString("PresentBox_tip_1");
				GameView::getInstance()->showAlertDialog(charInfo);

				m_bIsTransportFinish = true;
				m_bIsUseful = false;

				this->m_activeType = -1;
			}
		}
	}
}




