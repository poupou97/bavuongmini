#ifndef _UI_ACTIVE_ACTIVELABELDATA_H_
#define _UI_ACTIVE_ACTIVELABELDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ��Ծ�LabelDataȣ������ �� UI ���ݵĽṹ�壩
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.22
 */
class CActiveLabel;

class ActiveLabelData
{
public:
	ActiveLabelData(void);
	~ActiveLabelData(void);

public:
	Label * m_label_name;												// ���б�-���
	Label * m_label_joinCount;										// ����б�-������
	Label * m_label_active;												// ����б�-��Ծ�
	Label * m_label_status;												// ����б�-�״̬

	Button * m_btn_joinIn;												// ���б�-�μ
	Label * m_label_joinInText;										// ����б�-�μӰ�ť�ı�

	Label * m_label_userActive;										// �ҵĻ�Ծ�
	Button * m_btn_activeShop;										// Ȼ�Ծ���̳

	CActiveLabel * m_activeLabel;
};

#endif

