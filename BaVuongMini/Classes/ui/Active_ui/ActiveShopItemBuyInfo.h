#ifndef _UI_ACTIVE_ACTIVESHOPITEMBUYINFO_H_
#define _UI_ACTIVE_ACTIVESHOPITEMBUYINFO_H_

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GoodsInfo;
class CHonorCommodity;

class ActiveShopItemBuyInfo : public UIScene
{
public:
	ActiveShopItemBuyInfo();
	~ActiveShopItemBuyInfo();

	struct ReqForActiveBuyInfo
	{
		std::string strId;			//������id
		int nNum;						//�����������
	};

	static ActiveShopItemBuyInfo * create(CHonorCommodity * honorCommodity);
	bool init(CHonorCommodity * honorCommodity);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void BuyEvent(Ref *pSender, Widget::TouchEventType type);
	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	void getAmount(Ref *pSender, Widget::TouchEventType type);
	void clearNum(Ref *pSender, Widget::TouchEventType type);
	void deleteNum(Ref *pSender, Widget::TouchEventType type);

	void setToDefaultNum();
private:
	Layer * u_layer;
	CHonorCommodity * curHonorCommodity;
	Button * btn_inputNumValue;
	Label * l_constGoldValue;

	//����Ʒ���
	int basePrice;
	//۹�������
	int buyNum;
	std::string str_buyNum;
	//������ܼ
	int buyPrice;

	//۵�� 
	Text * l_priceOfOne;
	//۹������  
	Text * l_buyNum;
	//�������  
	Text * l_totalPrice;
};

#endif

