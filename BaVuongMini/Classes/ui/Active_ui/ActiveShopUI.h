#ifndef _UI_ACTIVE_ACTIVESHOPUI_H_
#define _UI_ACTIVE_ACTIVESHOPUI_H_

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CHonorCommodity;

class ActiveShopUI : public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	ActiveShopUI();
	~ActiveShopUI();

	static Layout * s_pPanel;
	static ActiveShopUI * create(std::vector<CHonorCommodity *> vector_activeShopSource);
	bool init(std::vector<CHonorCommodity *> vector_activeShopSource);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

private:
	void clearVectorActiveShopSource();
	Text * m_label_activeAll;																				// �����ܵĻ�Ծ�


private:
	std::vector<CHonorCommodity*> m_vector_activeShopSource;						// �����ȵ�Э� �� ��Ծ���̵���ݵ�Э� �һ�£�����һ��Э�

	TableView * m_tableView;

	Label * l_honorValue;

public:
	void initUserActiveFromInternet();																		// ��� ·�������ݣ�userAcitve���� ������ʾ
};


//cell�еĵ��
class ActiveShopCellItem : public Layer
{
public:
	ActiveShopCellItem();
	~ActiveShopCellItem();

	static ActiveShopCellItem* create(CHonorCommodity * honorCommodity);
	bool init(CHonorCommodity * honorCommodity);

	void DetailEvent(Ref * pSender);
	void BuyEvent(Ref *pSender);

private:
	CHonorCommodity * m_activeShopSource;

public:
	TableView *m_tableView;
};


#endif

