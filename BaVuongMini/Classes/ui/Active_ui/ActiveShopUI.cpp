#include "ActiveShopUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../GameAudio.h"
#include "../../messageclient/element/CHonorCommodity.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../GameView.h"
#include "../../AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../offlinearena_ui/HonorShopItemBuyInfo.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "ActiveShopItemBuyInfo.h"
#include "../offlinearena_ui/HonorShopItemInfo.h"
#include "ActiveData.h"
#include "../../utils/GameUtils.h"

using namespace CocosDenshion;


Layout * ActiveShopUI::s_pPanel;

const Size SizeTableView = Size(498, 262);
const Vec2 Vec2TableView = Vec2(52, 74);
const Size SizeCellSize = Size(495, 70);


ActiveShopUI::ActiveShopUI()
	:m_label_activeAll(NULL)
{
}


ActiveShopUI::~ActiveShopUI()
{
	clearVectorActiveShopSource();
}

ActiveShopUI * ActiveShopUI::create(std::vector<CHonorCommodity *> vector_activeShopSource)
{
	auto activeShopUI = new ActiveShopUI();
	if (activeShopUI && activeShopUI->init(vector_activeShopSource))
	{
		activeShopUI->autorelease();
		return activeShopUI;
	}
	CC_SAFE_DELETE(activeShopUI);
	return NULL;
}

bool ActiveShopUI::init(std::vector<CHonorCommodity *> vector_activeShopSource)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();

		// 删除旧数据
		clearVectorActiveShopSource();

		// 将服务器数据更新到 native数据
		for (int i = 0; i < (int)vector_activeShopSource.size(); i++)
		{
			//m_vector_activeShopSource.push_back(vector_activeShopSource.at(i));

			CHonorCommodity * data_ = new CHonorCommodity();
			data_->CopyFrom(*vector_activeShopSource.at(i));
			m_vector_activeShopSource.push_back(data_);
		}

		//create UI
		//加载UI
		if(LoadSceneLayer::activeShopPanel->getParent() != NULL)
		{
			LoadSceneLayer::activeShopPanel->removeFromParentAndCleanup(false);
		}
		s_pPanel = LoadSceneLayer::activeShopPanel;

		s_pPanel->setTouchEnabled(true);
		s_pPanel->setAnchorPoint(Vec2(0.0f,0.0f));
		s_pPanel->setContentSize(Size(608, 417));
		s_pPanel->setPosition(Vec2::ZERO);
		s_pPanel->setScale(1.0f);

		m_pLayer->addChild(s_pPanel);

		auto Button_close = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(ActiveShopUI::CloseEvent,this));
		Button_close->setPressedActionEnabled(true);

		// 初始化 活跃度
		initUserActiveFromInternet();

		m_tableView = TableView::create(this, SizeTableView);
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0, 0));
		m_tableView->setPosition(Vec2TableView);
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(m_tableView);

		/*Layer * u_tableViewFrameLayer = Layer::create();
		addChild(u_tableViewFrameLayer);
		ImageView * tableView_Frame = ImageView::create();
		tableView_Frame->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
		tableView_Frame->setScale9Enabled(true);
		tableView_Frame->setContentSize(Size(515,277));
		tableView_Frame->setCapInsets(Rect(32,32,1,1));
		tableView_Frame->setAnchorPoint(Vec2(0.5f,0.5f));
		tableView_Frame->setPosition(Vec2(299+3,198+6));
		u_tableViewFrameLayer->addChild(tableView_Frame);*/

		this->setContentSize(Size(608,417));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		m_tableView->reloadData();

		return true;
	}
	return false;
}

void ActiveShopUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void ActiveShopUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

bool ActiveShopUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void ActiveShopUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void ActiveShopUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void ActiveShopUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void ActiveShopUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void ActiveShopUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void ActiveShopUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void ActiveShopUI::tableCellTouched( TableView* table, TableViewCell* cell )
{

}

cocos2d::Size ActiveShopUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return SizeCellSize;
}

cocos2d::extension::TableViewCell* ActiveShopUI::tableCellAtIndex( TableView *table, ssize_t idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	if (idx == m_vector_activeShopSource.size() / 2)
	{
		if (m_vector_activeShopSource.size() % 2 == 0)
		{
			auto cellItem1 = ActiveShopCellItem::create(m_vector_activeShopSource.at(2 * idx));
			cellItem1->setPosition(Vec2(5, 0));
			cell->addChild(cellItem1);

			auto cellItem2 = ActiveShopCellItem::create(m_vector_activeShopSource.at(2 * idx + 1));
			cellItem2->setPosition(Vec2(253, 0));
			cell->addChild(cellItem2);
		}
		else
		{
			auto cellItem1 = ActiveShopCellItem::create(m_vector_activeShopSource.at(2 * idx));
			cellItem1->setPosition(Vec2(5, 0));
			cell->addChild(cellItem1);
		}
	}
	else
	{
		auto cellItem1 = ActiveShopCellItem::create(m_vector_activeShopSource.at(2 * idx));
		cellItem1->setPosition(Vec2(5, 0));
		cell->addChild(cellItem1);

		auto cellItem2 = ActiveShopCellItem::create(m_vector_activeShopSource.at(2 * idx + 1));
		cellItem2->setPosition(Vec2(253, 0));
		cell->addChild(cellItem2);
	}

	return cell;
}

ssize_t ActiveShopUI::numberOfCellsInTableView( TableView *table )
{
	if (m_vector_activeShopSource.size() % 2 == 0)
	{
		return m_vector_activeShopSource.size() / 2;
	}
	else
	{
		return m_vector_activeShopSource.size() / 2 + 1;
	}
}

void ActiveShopUI::clearVectorActiveShopSource()
{
	std::vector<CHonorCommodity *>::iterator iter;
	for (iter = m_vector_activeShopSource.begin(); iter != m_vector_activeShopSource.end(); iter++)
	{
		delete *iter;
	}
	m_vector_activeShopSource.clear();
}

void ActiveShopUI::initUserActiveFromInternet()
{
	// 用户总的活跃度
	int nUserActive = ActiveData::instance()->getUserActive();
	char str_userActive[10];
	sprintf(str_userActive, "%d", nUserActive);

	m_label_activeAll = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_activityValue");
	m_label_activeAll->setString(str_userActive);
}



////////////////////////////////////////////////////////////////
ActiveShopCellItem::ActiveShopCellItem()
{
	m_activeShopSource = new CHonorCommodity();
}

ActiveShopCellItem::~ActiveShopCellItem()
{
	delete m_activeShopSource;
}

ActiveShopCellItem* ActiveShopCellItem::create( CHonorCommodity * honorCommodity )
{
	auto honorShopCellItem = new ActiveShopCellItem();
	if (honorShopCellItem && honorShopCellItem->init(honorCommodity))
	{
		honorShopCellItem->autorelease();
		return honorShopCellItem;
	}
	CC_SAFE_DELETE(honorShopCellItem);
	return NULL;
}

bool ActiveShopCellItem::init( CHonorCommodity * honorCommodity )
{
	if (Layer::init())
	{
		m_activeShopSource->CopyFrom(*honorCommodity);

		//大边框
		auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
		sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
		sprite_bigFrame->setPosition(Vec2(0, 0));
		sprite_bigFrame->setPreferredSize(Size(244,68));
		addChild(sprite_bigFrame);

// 		auto spbigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
// 		spbigFrame->setPreferredSize(Size(71,39));
// 		spbigFrame->setCapInsets(Rect(18,9,2,23));
// 		spbigFrame->setPreferredSize(Size(244,68));
// 		MenuItemSprite *itemSprite_bigFrame = MenuItemSprite::create(spbigFrame, spbigFrame, spbigFrame, this, menu_selector(HonorShopCellItem::DetailEvent));
// 		itemSprite_bigFrame->setZoomScale(1.0f);
// 		CCMoveableMenu *btn_bigFrame = CCMoveableMenu::create(itemSprite_bigFrame,NULL);
// 		btn_bigFrame->setAnchorPoint(Vec2(0,0));
// 		btn_bigFrame->setPosition(Vec2(122,34));
// 		addChild(btn_bigFrame);

		//物品Icon底框
		std::string iconFramePath;
		switch(m_activeShopSource->goods().quality())
		{
		case 1 :
			{
				iconFramePath = "res_ui/smdi_white.png";
			}
			break;
		case 2 :
			{
				iconFramePath = "res_ui/smdi_green.png";
			}
			break;
		case 3 :
			{
				iconFramePath = "res_ui/smdi_bule.png";
			}
			break;
		case 4 :
			{
				iconFramePath = "res_ui/smdi_purple.png";
			}
			break;
		case 5 :
			{
				iconFramePath = "res_ui/smdi_orange.png";
			}
			break;
		}
// 		Sprite *  smaillFrame1 = Sprite::create(iconFramePath.c_str());
// 		Sprite *  smaillFrame2 = Sprite::create(iconFramePath.c_str());
// 		Sprite *  smaillFrame3 = Sprite::create(iconFramePath.c_str());
// 		MenuItemSprite *itemSprite_smallFrame = MenuItemSprite::create(smaillFrame1, smaillFrame2, smaillFrame3, this, menu_selector(HonorShopCellItem::DetailEvent));
// 		itemSprite_smallFrame->setZoomScale(1.0f);
// 		MenuItemImage* itemSprite_smallFrame = MenuItemImage::create(iconFramePath.c_str(), iconFramePath.c_str(),this, menu_selector(HonorShopCellItem::DetailEvent) );
// 		CCMoveableMenu *btn_smallFrame = CCMoveableMenu::create(itemSprite_smallFrame,NULL);
// 		btn_smallFrame->setAnchorPoint(Vec2(0,0));
// 		btn_smallFrame->setPosition(Vec2(10,10));
// 		addChild(btn_smallFrame);

// 		MenuItemImage *item_image = MenuItemImage::create(
// 		 	iconFramePath.c_str(),
// 		 	iconFramePath.c_str(),
// 		 	this,
// 		 	menu_selector(HonorShopCellItem::DetailEvent));
// 		Menu* btn_smallFrame = Menu::create(item_image, NULL);
// 		btn_smallFrame->setAnchorPoint(Vec2::ZERO);
// 		btn_smallFrame->setPosition(Vec2(0,0));
// 		addChild(btn_smallFrame);

		auto s_normalImage_1 = cocos2d::extension::Scale9Sprite::create(iconFramePath.c_str());
		s_normalImage_1->setPreferredSize(Size(47,47));
		auto firstMenuImage = MenuItemSprite::create(s_normalImage_1, s_normalImage_1, s_normalImage_1, CC_CALLBACK_1(ActiveShopCellItem::DetailEvent,this));
		firstMenuImage->setScale(1.0f);
		firstMenuImage->setAnchorPoint(Vec2(0.5f,0.5f));
		auto firstMenu = CCMoveableMenu::create(firstMenuImage, NULL);
		firstMenu->setContentSize(Size(47,47));
		firstMenu->setPosition(Vec2(37,sprite_bigFrame->getContentSize().height/2+1));
		addChild(firstMenu);
		
		std::string pheadPath = "res_ui/props_icon/";
		pheadPath.append(m_activeShopSource->goods().icon().c_str());
		pheadPath.append(".png");
		auto phead = Sprite::create(pheadPath.c_str());
		phead->setIgnoreAnchorPointForPosition(false);
		phead->setAnchorPoint(Vec2(0.5f, 0.5f));
		phead->setPosition(Vec2(37, sprite_bigFrame->getContentSize().height/2+1));
		phead->setScale(0.85f);
		addChild(phead);

		auto pName = Label::createWithTTF(m_activeShopSource->goods().name().c_str(),APP_FONT_NAME,18);
		pName->setAnchorPoint(Vec2(0.5f,0.5f));
		pName->setPosition(Vec2(112,44));
		pName->setColor(GameView::getInstance()->getGoodsColorByQuality(m_activeShopSource->goods().quality()));
		addChild(pName);

		char * s = new char [20];
		sprintf(s,"%d",m_activeShopSource->honor());
		auto pPrice = Label::createWithTTF(s,APP_FONT_NAME,16);
		delete [] s;
		pPrice->setAnchorPoint(Vec2(0.5f,0.5f));
		pPrice->setPosition(Vec2(112,25));
		addChild(pPrice);

		auto spBg = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_2.png");
		spBg->setPreferredSize(Size(71,39));
		spBg->setCapInsets(Rect(18,9,2,23));
		auto itemSprite = MenuItemSprite::create(spBg, spBg, spBg, CC_CALLBACK_1(ActiveShopCellItem::BuyEvent,this));
		itemSprite->setScale(1.3f);
		auto btn_buy = CCMoveableMenu::create(itemSprite,NULL);
		btn_buy->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_buy->setPosition(Vec2(206,34));
		addChild(btn_buy);

		const char *str1 = StringDataManager::getString("goods_auction_buy");
		char* p1 =const_cast<char*>(str1);
		auto l_buy=Label::createWithTTF(p1,APP_FONT_NAME,18);
		l_buy->setAnchorPoint(Vec2(0.5f,0.5f));
		l_buy->setPosition(Vec2(71/2,39/2));
		itemSprite->addChild(l_buy);

		this->setContentSize(Size(244,68));

		return true;
	}
	return false;
}

void ActiveShopCellItem::BuyEvent( Ref *pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveShopItemBuyInfo) == NULL)
	{
 	 	Size winSize = Director::getInstance()->getVisibleSize();
 	 	auto activeShopItemBuyInfo = ActiveShopItemBuyInfo::create(m_activeShopSource);
 	 	activeShopItemBuyInfo->setIgnoreAnchorPointForPosition(false);
 	 	activeShopItemBuyInfo->setAnchorPoint(Vec2(0.5f,0.5f));
 	 	activeShopItemBuyInfo->setPosition(Vec2(winSize.width/2,winSize.height/2));
		activeShopItemBuyInfo->setTag(kTagActiveShopItemBuyInfo);
 	 	GameView::getInstance()->getMainUIScene()->addChild(activeShopItemBuyInfo);
	}
}

void ActiveShopCellItem::DetailEvent( Ref * pSender )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveShopItemInfo) == NULL)
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		auto honorShopItemInfo = HonorShopItemInfo::create(m_activeShopSource);
		honorShopItemInfo->setIgnoreAnchorPointForPosition(false);
		honorShopItemInfo->setAnchorPoint(Vec2(0.5f,0.5f));
		//honorShopItemInfo->setPosition(Vec2(winSize.width/2,winSize.height/2));
		honorShopItemInfo->setTag(kTagActiveShopItemInfo);
		GameView::getInstance()->getMainUIScene()->addChild(honorShopItemInfo);
	}
}
