#include "ActiveShopData.h"
#include "../../messageclient/element/CHonorCommodity.h"

ActiveShopData * ActiveShopData::s_activeShopData = NULL;

ActiveShopData::ActiveShopData(void)
{
	
}


ActiveShopData::~ActiveShopData(void)
{
	std::vector<CHonorCommodity *>::iterator iter;
	for (iter = m_vector_activeShopSource.begin(); iter != m_vector_activeShopSource.end(); iter++)
	{
		delete *iter;
	}
	m_vector_activeShopSource.clear();
}

ActiveShopData * ActiveShopData::instance()
{
	if (NULL == s_activeShopData)
	{
		s_activeShopData = new ActiveShopData();
	}

	return s_activeShopData;
}



