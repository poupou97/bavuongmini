#ifndef _UI_ACTIVE_ACTIVEMISSONMANAGER_H_
#define _UI_ACTIVE_ACTIVEMISSONMANAGER_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class ActiveMoveAction;
class ActiveTalkWithNpcAction;
class MissionInfo;

class ActiveMissonManager
{
public:
	ActiveMissonManager();
	~ActiveMissonManager();

public:
	static ActiveMissonManager * s_missonManager;
	static ActiveMissonManager * instance();

public:
	enum AciveActionType
	{
		kTypeActiveDoNothing = 0,
		kTypeActiveMoveAction = 1,
		kTypeActiveTalkWithNpc = 4,
	};

	void update();

public:
	MissionInfo * m_curMissionInfo;
	bool m_bIsAutoRunForMission;

	std::vector<MissionInfo *> m_vector_curMissonInfo;

	void addAcitveMisson(MissionInfo * missionInfo);
	void doActiveMisson(MissionInfo * missionInfo);

private:
	AciveActionType m_curActionType;

	ActiveMoveAction * m_curMoveAction;
	ActiveTalkWithNpcAction * m_curTalkWithNpcAction;
	
};

#endif