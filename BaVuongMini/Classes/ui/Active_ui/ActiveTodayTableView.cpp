#include "ActiveTodayTableView.h"
#include "../../messageclient/element/CActiveLabelToday.h"
#include "ActiveData.h"
#include "AppMacros.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "ActiveUI.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"

#define  SelectImageTag 458
const Size SizeDataCellSize = Size(510, 37);

const Size SizeCellSize = Size(183, 57);

#define PAGE_NUM 10

ActiveTodayTableView * ActiveTodayTableView::s_activeToadyTableView = NULL;

ActiveTodayTableView * ActiveTodayTableView::instance()
{
	if (NULL == s_activeToadyTableView)
	{
		s_activeToadyTableView = new ActiveTodayTableView();
	}

	return s_activeToadyTableView;
}

ActiveTodayTableView::ActiveTodayTableView( void )
{

}

ActiveTodayTableView::~ActiveTodayTableView( void )
{

}

void ActiveTodayTableView::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void ActiveTodayTableView::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void ActiveTodayTableView::tableCellTouched( TableView* table, TableViewCell* cell )
{

}

cocos2d::Size ActiveTodayTableView::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return SizeCellSize;
}

cocos2d::extension::TableViewCell* ActiveTodayTableView::tableCellAtIndex(TableView *table, ssize_t idx)
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	initCell(cell, idx);

	return cell;
}

ssize_t ActiveTodayTableView::numberOfCellsInTableView( TableView *table )
{
	return m_vector_labelToday.size();
}

void ActiveTodayTableView::initDataFromInternet()
{
	// �����
	std::vector<CActiveLabelToday *>::iterator iter;
	for (iter = m_vector_labelToday.begin(); iter != m_vector_labelToday.end(); iter++)
	{
		delete *iter;
	}
	m_vector_labelToday.clear();

	for (unsigned int i = 0; i < ActiveData::instance()->m_vector_internet_labelToday.size(); i ++)
	{
		auto activeLabelToday = new CActiveLabelToday();
		activeLabelToday->CopyFrom(*ActiveData::instance()->m_vector_internet_labelToday.at(i));

		m_vector_labelToday.push_back(activeLabelToday);
	}
}

void ActiveTodayTableView::initCell( TableViewCell * cell, unsigned int nIndex )
{
	// ݵ�ͼ
	auto sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
	sprite_frame->setContentSize(Size(SizeCellSize.width - 6, SizeCellSize.height - 2));
	sprite_frame->setCapInsets(Rect(18, 9, 2, 23));
	sprite_frame->setAnchorPoint(Vec2::ZERO);
	sprite_frame->setPosition(Vec2(3, 1));
	cell->addChild(sprite_frame);

	auto sprite_activeIcon = Sprite::create("res_ui/huoyuedu.png");
	sprite_activeIcon->setAnchorPoint(Vec2(0, 0));
	sprite_activeIcon->setIgnoreAnchorPointForPosition(false);
	sprite_activeIcon->setPosition(Vec2(5, (sprite_frame->getContentSize().height - sprite_activeIcon->getContentSize().height) / 2));
	cell->addChild(sprite_activeIcon);


	// �����Ծ�
	int nActiveNeed = m_vector_labelToday.at(nIndex)->needactive();

	char charActiveNeed[20];
	sprintf(charActiveNeed, "%d", nActiveNeed);

	/*Label * pLabel_active = Label::createWithTTF(charActiveNeed, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
	pLabel_active->setColor(Color3B(255, 255, 255));
	pLabel_active->setAnchorPoint(Vec2(0.5f, 0.5f));
	pLabel_active->setHorizontalAlignment(TextHAlignment::CENTER);
	pLabel_active->setPosition(Vec2(20, sprite_frame->getContentSize().height / 2));
	cell->addChild(pLabel_active);*/

	auto pLabel_active = Label::createWithBMFont("res_ui/font/ziti_12.fnt", charActiveNeed);
	pLabel_active->setAnchorPoint(Vec2(0.5f, 0.5f));
	pLabel_active->setPosition(Vec2(sprite_activeIcon->getPositionX() + sprite_activeIcon->getContentSize().width / 2,
		sprite_activeIcon->getPositionY() + sprite_activeIcon->getContentSize().height / 2 - 10));
	
	auto layer = Layer::create();
	cell->addChild(layer);

	layer->addChild(pLabel_active);

	// ���Ʒ�׿
	initGoodsInfo(layer, nIndex, sprite_frame);
}

void ActiveTodayTableView::initGoodsInfo(Layer * ui_layer, unsigned int nIndex, cocos2d::extension::Scale9Sprite* sprite_bg)
{
	std::vector<GoodsInfo *> vector_goods;
	int nGoodSize = m_vector_labelToday.at(nIndex)->goods_size();	
	for(int i = 0; i < nGoodSize; i ++)
	{
		auto pGoodsInfo = new GoodsInfo();
		pGoodsInfo->CopyFrom(m_vector_labelToday.at(nIndex)->goods(i));

		vector_goods.push_back(pGoodsInfo);
	}

	// icon_frame
	if (0 == nGoodSize)
	{
		return ;
	}

	auto tmpGoodsInfo = vector_goods.at(0);

	std::string goods_icon = vector_goods.at(0)->icon();

	std::string goodsIconName = "res_ui/props_icon/";
	goodsIconName.append(goods_icon);
	goodsIconName.append(".png");

	// ��goods�Ʒ��ѡ���ͼ
	int nQuality = vector_goods.at(0)->quality();
	std::string strQuality = getEquipmentQualityByIndex(nQuality);

	ActiveUI * activeUI = (ActiveUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	if (NULL == activeUI)
	{
		return;
	}

	// frame
	auto pImageView_frame = ImageView::create();
	pImageView_frame->loadTexture(strQuality.c_str());
	pImageView_frame->setAnchorPoint(Vec2(0.5f, 0.5f));
	pImageView_frame->setPosition(Vec2(70, sprite_bg->getContentSize().height / 2));
	pImageView_frame->setTouchEnabled(true);
	pImageView_frame->setScale(0.8f);
	ui_layer->addChild(pImageView_frame);

	// icon
	auto pImageView_icon = Button::create();
	pImageView_icon->loadTextures(goodsIconName.c_str(), goodsIconName.c_str(), goodsIconName.c_str());
	pImageView_icon->setAnchorPoint(Vec2(0.5f, 0.5f));
	pImageView_icon->setPosition(Vec2(70, sprite_bg->getContentSize().height / 2));
	pImageView_icon->addTouchEventListener(CC_CALLBACK_2(ActiveUI::callBackGoodIcon, activeUI));
	pImageView_icon->setTouchEnabled(true);
	pImageView_icon->setScale(0.8f);
	pImageView_icon->setTag(m_vector_labelToday.at(nIndex)->needactive());
	ui_layer->addChild(pImageView_icon);

	// good ���
	std::vector<int > vector_goodsNum;
	int nGoodNumSize = m_vector_labelToday.at(nIndex)->goodsnumber_size();
	for(int i = 0; i < nGoodNumSize; i ++)
	{
		vector_goodsNum.push_back(m_vector_labelToday.at(nIndex)->goodsnumber(i));
	}

	if (0 == nGoodNumSize)
	{
		return ;
	}

	int nNum = vector_goodsNum.at(0);

	char charGoodNum[20];
	sprintf(charGoodNum, "%d", nNum);

	auto pLabel_goodNum = Label::createWithTTF(charGoodNum, APP_FONT_NAME, 12, Size(150, 0), TextHAlignment::LEFT);
	pLabel_goodNum->setColor(Color3B(255, 255, 255));
	pLabel_goodNum->setAnchorPoint(Vec2(0.5f, 0.5f));
	pLabel_goodNum->setHorizontalAlignment(TextHAlignment::CENTER);
	//pLabel_goodNum->setPosition(Vec2(110, sprite_bg->getContentSize().height / 2));
	pLabel_goodNum->setPosition(Vec2(pImageView_frame->getPosition().x + 15, sprite_bg->getContentSize().height / 2 - 15));
	ui_layer->addChild(pLabel_goodNum);

	// btn
	auto sprite_btn_normal = Button::create();
	sprite_btn_normal->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","res_ui/new_button_001.png");
	sprite_btn_normal->setAnchorPoint(Vec2(0.5f,0.5f));
	sprite_btn_normal->setScale9Enabled(true);
	sprite_btn_normal->setCapInsets(Rect(18,9,2,23));
	sprite_btn_normal->setContentSize(Size(75,36));
	sprite_btn_normal->setTouchEnabled(true);
	sprite_btn_normal->addTouchEventListener(CC_CALLBACK_2(ActiveTodayTableView::callBackActiveTodayBtn, this));
	sprite_btn_normal->setPosition(Vec2(135, sprite_bg->getContentSize().height / 2));
	

	// �����Ծ�
	int nActiveNeed = m_vector_labelToday.at(nIndex)->needactive();
	sprite_btn_normal->setTag(nActiveNeed);


	// ��ж btn�״̬ 0.δ��ȡ���1.�����ȡ������
	std::string strBtnText = "";
	int nBtnStatus = m_vector_labelToday.at(nIndex)->status();
	if (0 == nBtnStatus)
	{
		// ������Ծ�
		int nActiveNeed = m_vector_labelToday.at(nIndex)->needactive();
		int nTodayActive = ActiveData::instance()->get_todayActive();

		if (nTodayActive >= nActiveNeed)
		{
			const char * char_get = StringDataManager::getString("activy_lingqu");
			char * str_get = const_cast<char *>(char_get);
			strBtnText = str_get;

			sprite_btn_normal->setPressedActionEnabled(true);

		}
		else
		{
			const char * char_disable = StringDataManager::getString("activy_weidacheng");
			char * str_disable = const_cast<char *>(char_disable);
			strBtnText = str_disable;
			
			sprite_btn_normal->loadTextures("res_ui/zhezhao2_btn.png","res_ui/zhezhao2_btn.png","res_ui/zhezhao2_btn.png");
			sprite_btn_normal->setScale9Enabled(false);
			sprite_btn_normal->setTouchEnabled(false);
			sprite_btn_normal->setPressedActionEnabled(false);
		}
	}
	else if (1 == nBtnStatus)
	{
		const char * char_hasGet = StringDataManager::getString("activy_yilingqu");
		char * str_hasGet = const_cast<char *>(char_hasGet);
		strBtnText = str_hasGet;

		sprite_btn_normal->loadTextures("res_ui/new_button_001.png","res_ui/new_button_001.png","res_ui/new_button_001.png");
		sprite_btn_normal->setScale9Enabled(true);
		sprite_btn_normal->setCapInsets(Rect(18,9,2,23));
		sprite_btn_normal->setContentSize(Size(75,36));
		sprite_btn_normal->setTouchEnabled(false);
		sprite_btn_normal->setPressedActionEnabled(false);
	}
	ui_layer->addChild(sprite_btn_normal);

	// btn label
	auto label_btn = Label::createWithTTF(strBtnText.c_str(), APP_FONT_NAME, 16);
	label_btn->setAnchorPoint(Vec2(0.5f, 0.5f));
	label_btn->setPosition(Vec2(0, 0));
	sprite_btn_normal->addChild(label_btn);
}

std::string ActiveTodayTableView::getEquipmentQualityByIndex( int quality )
{
	std::string frameColorPath = "res_ui/";
	switch(quality)
	{
	case 1:
		{
			frameColorPath.append("sdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("sdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("sdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("sdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("sdi_orange");
		}break;
	}
	frameColorPath.append(".png");
	return frameColorPath;
}

void ActiveTodayTableView::callBackGoodIcon(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto pBtn = (Button *)pSender;
		// ȴ˴�� �����Ļ�Ծ����Ϊ tag
		int nTag = pBtn->getTag();

		//initDataFromInternet();

		int nSize = m_vector_labelToday.size();
		for (int i = 0; i < nSize; i++)
		{
			auto activeLabelToday = m_vector_labelToday.at(i);
			if (nTag == activeLabelToday->needactive())
			{
				auto pGoodsInfo = new GoodsInfo();
				pGoodsInfo->CopyFrom(activeLabelToday->goods(0));

				auto goodsItemInfoBase = GoodsItemInfoBase::create(pGoodsInfo, GameView::getInstance()->EquipListItem, 0);
				goodsItemInfoBase->setIgnoreAnchorPointForPosition(false);
				goodsItemInfoBase->setAnchorPoint(Vec2(0.5f, 0.5f));
				//goodsItemInfoBase->setPosition(Vec2(size.width / 2, size.height / 2));

				GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void ActiveTodayTableView::callBackActiveTodayBtn(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		/*MenuItemSprite* pMenuItem = (MenuItemSprite *)obj;*/
		auto pUIBtn = (Button*)pSender;
		int nId = pUIBtn->getTag();

		//char charTmp[20];
		//sprintf(charTmp, "%d", nId);

		//GameView::getInstance()->showAlertDialog(charTmp);

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5130, (void *)nId);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}






