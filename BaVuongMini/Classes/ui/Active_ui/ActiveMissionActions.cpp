#include "ActiveMissionActions.h"
#include "../../GameView.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../missionscene/HandleMission.h"
#include "ActiveMissonManager.h"
#include "../../gamescene_state/role/FunctionNPC.h"
#include "../missionscene/MissionTalkWithNpc.h"

////////////////////////////////////////////////////////////////////

ActiveMoveAction::ActiveMoveAction():
m_pListener(NULL),
m_pfnSelector(NULL),
distance(200)
{
	targetPos = Vec2(0,0);
	targetMapId = "xsc.level";
}

ActiveMoveAction::~ActiveMoveAction()
{
}


ActiveMoveAction * ActiveMoveAction::create(MissionInfo *missionInfo)
{
	auto missionMoveBase = new ActiveMoveAction();
	if(missionMoveBase)
	{
		missionMoveBase->init(missionInfo);
		return missionMoveBase;
	}
	CC_SAFE_DELETE(missionMoveBase);
	return NULL;
}

void ActiveMoveAction::init(MissionInfo *missionInfo)
{
	Vec2 pos = Vec2(missionInfo->action().moveto().x(),missionInfo->action().moveto().y());
	targetMapId = missionInfo->action().moveto().mapid();
	targetActorId = missionInfo->action().moveto().targetnpc().npcid();
	if (pos.x == 0 && pos.y == 0)
	{
		targetPos = Vec2(0,0);
	}
	else
	{
		float x = GameView::getInstance()->getGameScene()->tileToPositionX(pos.x);
		float y = GameView::getInstance()->getGameScene()->tileToPositionY(pos.y);
		targetPos = Vec2(x,y);
	}
	runToTarget();
	//GameView::getInstance()->missionManager->isAutoRunForMission = true;
	ActiveMissonManager::instance()->m_bIsAutoRunForMission = true;
}


// void MMoveAction::update( float dt )
// {
// 	//检测是否寻路完成
// 	//如果完成,继续做任务
// 	if (isFinishedMove())
// 	{
// 		doFinishedMove();
// 	}
// }
// 
// void MMoveAction::doFinishedMove()
// {
// 	if (m_pListener && m_pfnSelector)
// 	{
// 		(m_pListener->*m_pfnSelector)();
// 	}
// }

// void MMoveAction::addFinishedMove( Ref * pSender,SEL_FinishedMove selector )
// {
// 	m_pListener = pSender;
// 	m_pfnSelector = selector;
// }

bool ActiveMoveAction::isFinishedMove()
{
	if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	{
		Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
		Vec2 cp_target = targetPos;
		//CCLOG("%f",sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)));
		if (cp_role.getDistance(cp_target) < 64)
		{
			GameView::getInstance()->myplayer->changeAction(ACT_STAND);
			GameView::getInstance()->myplayer->setLockedActorId(targetActorId);

			// 移动结束后，直接打开 任务 面板

			//打开与npc交互的任务面板 
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission))
			{
				GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission)->removeFromParent();
			}

			Size winSize = Director::getInstance()->getVisibleSize();

			if (NULL == GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc))
			{
				auto scene = GameView::getInstance()->getGameScene();
				auto fn = dynamic_cast<FunctionNPC*>(scene->getActor(targetActorId));
				auto talkWithNpc = MissionTalkWithNpc::create(fn);
				talkWithNpc->setIgnoreAnchorPointForPosition(false);
				talkWithNpc->setAnchorPoint(Vec2(0,0.5f));
				talkWithNpc->setPosition(Vec2(0,winSize.height/2));	

				GameView::getInstance()->getMainUIScene()->addChild(talkWithNpc,0,kTagTalkWithNpc);
			}

			return true;
		}
		else 
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	
}

//移动到目的地
void ActiveMoveAction::runToTarget()
{
	if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	{
		//same map
		Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
		Vec2 cp_target = targetPos;
		if (cp_role.getDistance(cp_target) < 1)
		{
			return;
		}
		else
		{
			if (targetPos.x == 0 && targetPos.y == 0)
			{
				return;
			}

			auto cmd = new MyPlayerCommandMove();
			cmd->targetPosition = targetPos;
			cmd->method = MyPlayerCommandMove::method_searchpath;
			bool bSwitchCommandImmediately = true;
			if(GameView::getInstance()->myplayer->isAttacking())
				bSwitchCommandImmediately = false;
			GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
		}
	}
	else
	{
		auto cmd = new MyPlayerCommandMove();
		cmd->targetMapId = targetMapId;
		if (targetPos.x == 0 && targetPos.y == 0)
		{
		}
		else
		{
			cmd->targetPosition = targetPos;
		}
		cmd->method = MyPlayerCommandMove::method_searchpath;
		bool bSwitchCommandImmediately = true;
		if(GameView::getInstance()->myplayer->isAttacking())
			bSwitchCommandImmediately = false;
		GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
	}
}

float ActiveMoveAction::getDistance()
{
	return distance;
}

////////////////////////////////////////////////////////////////////

ActiveTalkWithNpcAction::ActiveTalkWithNpcAction():
isfirst(true)
{
}


ActiveTalkWithNpcAction::~ActiveTalkWithNpcAction()
{
}


ActiveTalkWithNpcAction * ActiveTalkWithNpcAction::create(MissionInfo *missionInfo)
{
	auto mTalkWithNpcAction = new ActiveTalkWithNpcAction();
	if (mTalkWithNpcAction)
	{
		mTalkWithNpcAction->init(missionInfo);
		return mTalkWithNpcAction;
	}
	CC_SAFE_DELETE(mTalkWithNpcAction);
	return false;
}

void ActiveTalkWithNpcAction::init(MissionInfo *missionInfo)
{
	curMissionInfo = missionInfo;
// 	std::string mapid = missionInfo->action().moveto().mapid();
// 	Vec2 pos = Vec2(missionInfo->action().moveto().x(),missionInfo->action().moveto().y());
// 	moveAction = MMoveAction::create(mapid.c_str(),pos);
	moveAction = ActiveMoveAction::create(missionInfo);
}


void ActiveTalkWithNpcAction::doMTalkWithNpcAction()
{
	CCLOG("doMTalkWithNpcAction");
	//弹出任务人对话窗口.
	if (isfirst)
	{
		auto mainScene = (UIScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene->getChildByTag(kTagHandleMission) == NULL)
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow))
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
				GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow)->removeFromParent();
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc))
			{
				GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc)->removeFromParent();
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI))
			{
				GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI)->removeFromParent();
			}

			Size winSize = Director::getInstance()->getVisibleSize();
			HandleMission * handleMission = HandleMission::create(curMissionInfo);
			mainScene->addChild(handleMission,0,kTagHandleMission);
			handleMission->setIgnoreAnchorPointForPosition(false);
			handleMission->setAnchorPoint(Vec2(0,0.5f));
			handleMission->setPosition(Vec2(0,winSize.height/2));
			isfirst = false;
		}
	}
}

bool ActiveTalkWithNpcAction::isFinishedMove()
{
	return moveAction->isFinishedMove();
}



