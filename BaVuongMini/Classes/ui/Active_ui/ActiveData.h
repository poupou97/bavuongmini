#ifndef _UI_ACTIVE_ACTIVEDATA_H_
#define _UI_ACTIVE_ACTIVEDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ��Ծ��ģ�飨���ڱ�����������ݣ�
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.22
 */

class CActiveLabel;
class CActiveNote;
class CActiveLabelToday;
class ActiveLabelData;
class ActiveNoteData;

class ActiveData
{
public:
	static ActiveData * s_activeData;
	static ActiveData * instance();

private:
	ActiveData(void);
	~ActiveData(void);

public:
	std::vector<CActiveLabel *> m_vector_internet_label;											// ���� ������ ��label�� vector
	std::vector<CActiveNote *> m_vector_internet_note;												// ���� ������ ��label�� note
	std::vector<CActiveLabelToday *> m_vector_internet_labelToday;
	
	std::vector<ActiveLabelData *> m_vector_native_activeLabel;
	std::vector<ActiveNoteData *> m_vector_native_activeNote;			
	
private:
	int m_nUserActive;																								// �û���ӵ�еĻ�Ծ��
	int m_nTodayAcitve;																								// ���ջ�Ծ��

public:
	void setUserActive(int nUserAcitve);
	int getUserActive();

	void set_todayActive(int nTodayActive);
	int get_todayActive();

public:
	void initLabelFromInternet();																					// �� ������ ��ȡ Label
	void initNoteFromInternet();																					// �� ������ ��ȡ active

	void clearLabelAndNote();																						// ���labelVector��noteVector
	void clearActiveLabel();																							// ���labelVector
	void clearActiveNote();																							// ���noteVector
	void clearActiveLabelToday();

	void updateTodayActive(int nActive);															// ���ݸ����Ļ�Ծ�ȣ�����btn
};

#endif

