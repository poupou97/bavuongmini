#ifndef _UI_ACTIVE_ACTIVESHOPDATA_H_
#define _UI_ACTIVE_ACTIVESHOPDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ��Ծ���̵�ģ�飨���ڱ�����������ݣ�
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.28
 */
class CHonorCommodity;

class ActiveShopData
{
public:
	static ActiveShopData * s_activeShopData;
	static ActiveShopData * instance();

private:
	ActiveShopData(void);
	~ActiveShopData(void);

public:
	std::vector<CHonorCommodity *> m_vector_activeShopSource;
};

#endif

