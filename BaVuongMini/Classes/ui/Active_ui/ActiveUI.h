#ifndef _UI_ACTIVE_ACTIVEUI_H_
#define _UI_ACTIVE_ACTIVEUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../extensions/UITab.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ��Ծ�UI
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.21
 */

class CCTutorialParticle;
class ActiveNoteData;
class ActiveLabelData;
class KillNotesList;
class CActiveLabelToday;
//class Active;

class ActiveUI:public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	ActiveUI(void);
	~ActiveUI(void);

public:


	Layout * s_panel_activeUI;
	Layout * s_panel_worldBossUI;
	Layout * s_panel_singCopyUI;

	static Layout * s_pPanel;
	static ActiveUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//ȴ�������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

public:
	void callBackBtnClolse(Ref *pSender, Widget::TouchEventType type);																	// �رհ�ť����Ӧ
	void callBackBtnJoin(Ref * obj);
	void callBackBtnAcitveShop(Ref *pSender, Widget::TouchEventType type);															// �� ��Ծ���̳
	
	void callBackGoodIcon(Ref *pSender, Widget::TouchEventType type);																	// ǵ�icon�ʱ����Ӧ
	void callBackActiveTodayBtn(Ref* obj);																// �� ���ջ�Ծ� Ȱ�ť

	void tabIndexChangedEvent(Ref *pSender);

public:
	//���ն��
	void initChallengeUi();
	Layer * m_challenge_layer;	

private:
	Layer * m_base_layer;																							// ��layer
	Layer * m_active_layer;																						// ��Ծ�layer
	Layer * m_up_layer;
	TableView * m_tableView;																						// ��tableView
	cocos2d::extension::ScrollView * m_contentScrollView;																				// ����ScrollView
	Text * m_label_activeAll;																					// ��û��ܵĻ�Ծ�
	Label * m_labelTTF_activeAll;
	Text * m_label_activeToday;
	Text * m_labelBMF_activeToday;
	
	TableView * m_leftTableView;

	UITab * m_tab;
	Layout * m_panel_huoyuedu;

	int m_nTableViewSize;

	Label * m_label_content_name;																		// Ȼ���-���
	Label * m_label_content_time;																		// ƻ���-�ʱ�
	Label * m_label_content_gift;																			// ����-齱�
	Label * m_label_content_limit;																			// ����-����
	Label * m_label_content_describe;																	// ƻ���-����

	ActiveNoteData * m_curActiveNote;																			// �ǰNote��Ϣ

	std::vector<ActiveNoteData *> m_vector_activeNote;
	std::vector<ActiveLabelData *> m_vector_activeLabel;
	std::vector<CActiveLabelToday *> m_vector_labelToday;

	Layer * m_boss_layer;																							// ��BOSS Layer
	Size winSize;

private:
	// ���ջ�Ծ����
	Text * m_label_gift_num_1;
	Text * m_label_gift_num_2;
	Text * m_label_gift_num_3;
	Text * m_label_gift_num_4;
	Text * m_label_gift_num_5;

	Button * m_btn_gift_frame_1;
	Button * m_btn_gift_frame_2;
	Button * m_btn_gift_frame_3;
	Button * m_btn_gift_frame_4;
	Button * m_btn_gift_frame_5;

	ImageView * m_imageView_gift_icon_1;
	ImageView * m_imageView_gift_icon_2;
	ImageView * m_imageView_gift_icon_3;
	ImageView * m_imageView_gift_icon_4;
	ImageView * m_imageView_gift_icon_5;

	Text * m_labelBMF_gift_1;
	Text * m_labelBMF_gift_2;
	Text * m_labelBMF_gift_3;
	Text * m_labelBMF_gift_4;
	Text * m_labelBMF_gift_5;

	ImageView * m_imageView_progress_1;
	ImageView * m_imageView_progress_2;
	ImageView * m_imageView_progress_3;
	ImageView * m_imageView_progress_4;
	ImageView * m_imageView_progress_5;

private:
	void initUI();
	void initTodayUI();
	
	void reloadActiveInfoData();																						// ظ�� »��� (old)
	void reloatActiveInfoScrollView();																				// ��� »���飨new��

	void initCell(TableViewCell * cell, unsigned int nIndex);
	bool IsCanPushVector(int nId);

	void openActiveWindow(UIScene * activeScene, int tag, int nActiveType);				// 1.Ҫ�򿪵UI; 2. UI��MainScene��еtagģ�3.�û�����(���ʱ����÷���)

	void activeToSign();																									// ��Ծ� ȵ� �ǩ��
	void activeToEctype();																									// ��Ծ� ȵ� ��
	void activeToMisson(int nActiveTypeId);																				// ��Ծ� ȵ� ����
	void activeToPK();																										// ��Ծ� ȵ� ����
	void activeToAnswerQuestion();																							// ��Ծ� ȵ� ���
	void activeToOpenPresentBox();																							// ��Ծ� ȵ� ���
	void activeToFamilyFight();																								// ��Ծ� ȵ� ����ս
	void activeToSingleFight();																								// ��Ծ� ȵ� ���ն��
	void activeToWorldBoss();																								// ��Ծ� ȵ� ���BOSS
	 
	void activeToRewardMisson();																							// ��������

public:
	void refrehTableView();
	void initLabelFromInternet();																						// ��� ·�������ݣ�label���� ������ʾ
	void initNoteFromInternet();																						// ��� ·�������ݣ�note���� ������ʾ
	void initUserActiveFromInternet();																				// ��� ·�������ݣ�userAcitve���� ������ʾ
	void initTodayActiveFromInternet();

	void refreshTodayAcitveData();

	void initActiveUIData();																								// ��ʼ��ActiveUI�е���ݣ��ⲿ����ActiveUIʱ�����ô˷���ʵ� ַ�������ݵĵ��빦�ܣ�

	std::string getActiveJoinAll(int nJoinCountAll);															// ��ȡ �ɲ������� �string
	std::string getActiveCanGetAll(int nActiveCanGetAll);												// Ļ�ȡ �ɵõ���Ծ�ȵ���� �string 

	void setTableViewSize(int nTableViewSize);																// ����tableViewSize
	int getTableViewSize();																								// û�ȡtableViewSize
	
private:
	std::string getEquipmentQualityByIndex(int quality);													// �� �Ʒ� ׻�øgoods���Ҫ�ĵ�ͼ
	void initGoodsFrame(int nIndex, std::string strFrame, std::string strGoodIconName, CActiveLabelToday* pActiveLabelToday);

	ActionInterval * initActionForGoods();

	void addParticleForGift(int nIndex);																	// ��ָ���Ľ��ջ�Ծ�Ƚ���������Ч�����ָ�ڼ�����1ӿ�ʼ��
	void removeParticleForGift(int nIndex);																	// ɾ��ָ����Ծ�Ƚ����ϵ����Ч�����ָ�ڼ�����1ӿ�ʼ��


public:
	//��ѧ
	int mTutorialScriptInstanceId;
	int mTutorialIndex;
	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	//ѡ�����
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction,int tutorialIndex);

	void addCCTutorialIndicatorSingCopy(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction,int tutorialIndex);
	void removeCCTutorialIndicator();
};

#endif

