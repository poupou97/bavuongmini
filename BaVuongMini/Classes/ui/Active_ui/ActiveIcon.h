#ifndef _UI_ACTIVE_ACTIVEICON_H_
#define _UI_ACTIVE_ACTIVEICON_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ��Ծ�Icon
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.21
 */

class ActiveIcon:public UIScene
{
public:
	ActiveIcon(void);
	~ActiveIcon(void);

public:
	static ActiveIcon* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

public:
	Button * m_btn_activeIcon;						// ȿɵ��ICON
	Layer * m_layer_active;								// ���Ҫ�õ��layer
	Label * m_label_active_text;						// 	ICON��·���ʾ�����	
	cocos2d::extension::Scale9Sprite * m_spirte_fontBg;				// �������ʾ�ĵ�ͼ

public:
	void callBackActive(Ref *pSender, Widget::TouchEventType type);

};

#endif

