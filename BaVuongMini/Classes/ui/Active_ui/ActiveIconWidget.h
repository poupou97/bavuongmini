#ifndef _UI_ACTIVE_ACTIVEICONWIDGET_H_
#define _UI_ACTIVE_ACTIVEICONWIDGET_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;
/////////////////////////////////
/**
 * ��Ծ�IconWidgetȣ����ڼӵ�GuidMap��
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.21
 */
class CCTutorialParticle;

class ActiveIconWidget:public Widget
{
public:
	ActiveIconWidget(void);
	~ActiveIconWidget(void);

public:
	static ActiveIconWidget* create();
	virtual bool init();
	
	virtual void update(float dt);
	
public:
	Button * m_btn_activeIcon;						// �ɵ��ICON
	Layer * m_layer_active;								// ���Ҫ�õ��layer
	Label * m_label_active_text;						// 	ICON��·���ʾ�����	
	cocos2d::extension::Scale9Sprite * m_spirte_fontBg;				// �������ʾ�ĵ�ͼ

public:
	void callBackActive(Ref *pSender, Widget::TouchEventType type);

	bool IsNeedParticle();

private:
	CCTutorialParticle * m_tutorialParticle;					// ��Ծ��ȦȦ��Ч
	

};

#endif

