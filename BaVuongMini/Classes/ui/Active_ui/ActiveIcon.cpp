#include "ActiveIcon.h"
#include "../../GameView.h"
#include "ActiveUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../utils/StaticDataManager.h"
#include "ActiveShopData.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "AppMacros.h"

ActiveIcon::ActiveIcon(void)
	:m_btn_activeIcon(NULL)
	,m_layer_active(NULL)
	,m_label_active_text(NULL)
	,m_spirte_fontBg(NULL)
{

}


ActiveIcon::~ActiveIcon(void)
{
	
}

ActiveIcon* ActiveIcon::create()
{
	ActiveIcon * activeIcon = new ActiveIcon();
	if (activeIcon && activeIcon->init())
	{
		activeIcon->autorelease();
		return activeIcon;
	}
	CC_SAFE_DELETE(activeIcon);
	return NULL;
}

bool ActiveIcon::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();


		// icon
		m_btn_activeIcon = Button::create();
		m_btn_activeIcon->setTouchEnabled(true);
		m_btn_activeIcon->setPressedActionEnabled(true);

		m_btn_activeIcon->loadTextures("gamescene_state/zhujiemian3/tubiao/active.png", "gamescene_state/zhujiemian3/tubiao/active.png","");
		
		m_btn_activeIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_btn_activeIcon->setPosition(Vec2( winSize.width - 70 - 159 - 80, winSize.height-70 - 80));
		m_btn_activeIcon->addTouchEventListener(CC_CALLBACK_2(ActiveIcon::callBackActive,this));

		// label
		const char * str_active = StringDataManager::getString("activy_chartInfo2");

		m_label_active_text = Label::createWithTTF(str_active, APP_FONT_NAME, 14);
		m_label_active_text->setColor(Color3B(255, 246, 0));
		m_label_active_text->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_active_text->setPosition(Vec2(m_btn_activeIcon->getPosition().x, m_btn_activeIcon->getPosition().y - m_btn_activeIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = cocos2d::extension::Scale9Sprite::create(Rect(5, 10, 1, 4) , "res_ui/zhezhao70.png");
		m_spirte_fontBg->setPreferredSize(Size(47, 14));
		m_spirte_fontBg->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_active_text->getPosition());


		m_layer_active = Layer::create();
		//m_layer_active->setTouchEnabled(true);
		m_layer_active->setAnchorPoint(Vec2::ZERO);
		m_layer_active->setPosition(Vec2::ZERO);
		m_layer_active->setContentSize(winSize);

		//m_layer_active->addChild(m_spirte_fontBg, -10);
		m_layer_active->addChild(m_btn_activeIcon);
		//m_layer_active->addChild(m_label_active_text);

		addChild(m_layer_active);

		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void ActiveIcon::callBackActive(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

	}
	break;

	case Widget::TouchEventType::CANCELED:
	{
		// ��ʼ�� UI WINDOW��
		Size winSize = Director::getInstance()->getVisibleSize();

		auto pTmpActiveUI = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
		if (NULL == pTmpActiveUI)
		{
			auto pActiveUI = ActiveUI::create();
			pActiveUI->setIgnoreAnchorPointForPosition(false);
			pActiveUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			pActiveUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			pActiveUI->setTag(kTagActiveUI);

			GameView::getInstance()->getMainUIScene()->addChild(pActiveUI);

			// ���� ��Ծ���̵���ݣ�����Ծ���̵���ݲ�Ϊ��ʱ��˵���Ѿ�������������ݣ���ô���ٷ������
			if (ActiveShopData::instance()->m_vector_activeShopSource.empty())
			{
				// ������������ȡ��Ʒ
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5114, NULL);
			}
		}
	}
		break;

	default:
		break;
	}
}

void ActiveIcon::onEnter()
{
	UIScene::onEnter();
}

void ActiveIcon::onExit()
{
	UIScene::onExit();
}
