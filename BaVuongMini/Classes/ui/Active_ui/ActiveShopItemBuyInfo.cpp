#include "ActiveShopItemBuyInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../extensions/Counter.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CHonorCommodity.h"
#include "../../AppMacros.h"
#include "ActiveData.h"


ActiveShopItemBuyInfo::ActiveShopItemBuyInfo()
	:buyNum(0)
	,basePrice(0)
	,buyPrice(0)
{
}


ActiveShopItemBuyInfo::~ActiveShopItemBuyInfo()
{
	delete curHonorCommodity;
}

ActiveShopItemBuyInfo * ActiveShopItemBuyInfo::create( CHonorCommodity * honorCommodity )
{
	auto honorShopItemBuyInfo = new ActiveShopItemBuyInfo();
	if (honorShopItemBuyInfo && honorShopItemBuyInfo->init(honorCommodity))
	{
		honorShopItemBuyInfo->autorelease();
		return honorShopItemBuyInfo;
	}
	CC_SAFE_DELETE(honorShopItemBuyInfo);
	return NULL;
}

bool ActiveShopItemBuyInfo::init( CHonorCommodity * honorCommodity )
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		curHonorCommodity = new CHonorCommodity();
		curHonorCommodity->CopyFrom(*honorCommodity);

		basePrice = curHonorCommodity->honor();

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(298, 403));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);
		//���UI
		if(LoadSceneLayer::BuyCalculatorsLayer->getParent() != NULL)
		{
			LoadSceneLayer::BuyCalculatorsLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (Layout*)LoadSceneLayer::s_pPanel->copy();
		auto ppanel = LoadSceneLayer::BuyCalculatorsLayer;
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->setContentSize(Size(298, 403));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		//عرհ�ť
		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(ActiveShopItemBuyInfo::CloseEvent,this));
		Button_close->setPressedActionEnabled(true);
		//��ƷIcon�׿
		auto imageView_dikuang = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_dikuang");
		switch(curHonorCommodity->goods().quality())
		{
		case 1 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_white.png");
			}
			break;
		case 2 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_green.png");
			}
			break;
		case 3 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_bule.png");
			}
			break;
		case 4 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_purple.png");
			}
			break;
		case 5 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_orange.png");
			}
			break;
		}

		std::string headImageStr ="res_ui/props_icon/";
		headImageStr.append(curHonorCommodity->goods().icon());
		headImageStr.append(".png");
		auto imageView_icon = ImageView::create();
		imageView_icon->loadTexture(headImageStr.c_str());
		imageView_icon->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_icon->setPosition(Vec2(62,344));
		imageView_icon->setScale(0.85f);
		u_layer->addChild(imageView_icon);

		auto l_single_name = (Text*)Helper::seekWidgetByName(ppanel,"Label_single_name");
		l_single_name->setVisible(true);
		l_single_name->setString(StringDataManager::getString("measurement_activePoint"));
		auto l_total_name = (Text*)Helper::seekWidgetByName(ppanel,"Label_total_name");
		l_total_name->setVisible(true);
		l_total_name->setString(StringDataManager::getString("measurement_activePoint"));

		//򵥼۵ĵ�λͼ�(�Ԫ��)
		auto ImageView_IngotOrCoins_single = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_IngotOrCoins_single");
 		ImageView_IngotOrCoins_single->setVisible(false);
		//�ܼ۵ĵ�λͼ�(�Ԫ��)
		auto ImageView_IngotOrCoins_total = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_IngotOrCoins_total");
		ImageView_IngotOrCoins_total->setVisible(false);
		//��Ʒ��
		auto l_name = (Text*)Helper::seekWidgetByName(ppanel,"Label_name");
		l_name->setString(curHonorCommodity->goods().name().c_str());
		l_name->setColor(GameView::getInstance()->getGoodsColorByQuality(curHonorCommodity->goods().quality()));
		//Ƶ�� 
		l_priceOfOne = (Text*)Helper::seekWidgetByName(ppanel,"Label_SingleValue");
		l_priceOfOne->setPosition(Vec2(199,334));
		char * s = new char[20];
		sprintf(s,"%d",basePrice);
		l_priceOfOne->setString(s);
		delete [] s;
		if (basePrice <= ActiveData::instance()->getUserActive())   // ۻ�Ծ�ȹ��
		{
			l_priceOfOne->setColor(Color3B(39,238,194));   //��ɫ
		}
		else
		{
			l_priceOfOne->setColor(Color3B(255,51,51));   //��ɫ
		}
		//�������  
		l_buyNum = (Text*)Helper::seekWidgetByName(ppanel,"Label_amount");
		l_buyNum->setString("");
		//�ܼ  
		l_totalPrice = (Text*)Helper::seekWidgetByName(ppanel,"Label_TotalValue");
		l_totalPrice->setString("");
		l_totalPrice->setPosition(Vec2(181,267));

		int index = 1;
		for (int i=0;i<3;i++)
		{
			for (int j=0;j<3;j++)
			{

				char str_index[2]={0};
				sprintf(str_index,"%d",index);

				auto extractAnnex=Button::create();
				extractAnnex->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
				extractAnnex->setTouchEnabled(true);
				extractAnnex->setPressedActionEnabled(true);
				extractAnnex->setTitleText(str_index);
				extractAnnex->setTitleFontSize(20);
				extractAnnex->setAnchorPoint(Vec2(0.5f,0.5f));
				extractAnnex->setTag(index);
				//extractAnnex->setPosition(Vec2(31+j*61,185- i*50));
				extractAnnex->setPosition(Vec2(60+j*61,207- i*50));
				extractAnnex->addTouchEventListener(CC_CALLBACK_2(ActiveShopItemBuyInfo::getAmount, this));
				u_layer->addChild(extractAnnex);
				index++;
			}
		}

		auto btn_clear=Button::create();
		btn_clear->setTouchEnabled(true);
		btn_clear->loadTextures("res_ui/enjian_y.png","res_ui/enjian_y.png","");
// 		btn_clear->setAnchorPoint(Vec2(0,0));
// 		btn_clear->setPosition(Vec2(216,185));
		btn_clear->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_clear->setPosition(Vec2(216+53/2,185+22));
		btn_clear->setTag(index);
		btn_clear->addTouchEventListener(CC_CALLBACK_2(ActiveShopItemBuyInfo::deleteNum, this));
		btn_clear->setPressedActionEnabled(true);
		u_layer->addChild(btn_clear);
		auto deleteSp=ImageView::create();
		deleteSp->loadTexture("res_ui/jisuanqi/jiantou.png");
		deleteSp->setAnchorPoint(Vec2(0.5f,0.5f));
		//deleteSp->setPosition(Vec2(btn_clear->getContentSize().width/2,btn_clear->getContentSize().height/2));
		deleteSp->setPosition(Vec2(0,0));
		btn_clear->addChild(deleteSp);

		auto btn_Zero= Button::create();
		btn_Zero->setTitleText("0");
		btn_Zero->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		btn_Zero->setTouchEnabled(true);
		btn_Zero->setTitleFontSize(25);
		btn_Zero->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_Zero->setTag(0);
		//btn_Zero->setPosition(Vec2(216,135));
		btn_Zero->setPosition(Vec2(216+53/2,135+22));
		btn_Zero->setPressedActionEnabled(true);
		btn_Zero->addTouchEventListener(CC_CALLBACK_2(ActiveShopItemBuyInfo::getAmount, this));
		u_layer->addChild(btn_Zero);

		auto btn_C= Button::create();
		btn_C->setTitleText("C");
		btn_C->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		btn_C->setTouchEnabled(true);
		btn_C->setTitleFontSize(25);
		btn_C->setAnchorPoint(Vec2(0.5f,0.5f));
		//btn_C->setPosition(Vec2(216,85));
		btn_C->setPosition(Vec2(216+53/2,85+22));
		btn_C->setPressedActionEnabled(true);
		btn_C->addTouchEventListener(CC_CALLBACK_2(ActiveShopItemBuyInfo::clearNum,this));
		u_layer->addChild(btn_C);

		auto btn_buy = (Button *)Helper::seekWidgetByName(ppanel,"Button_enter");
		btn_buy->setTouchEnabled(true);
		btn_buy->addTouchEventListener(CC_CALLBACK_2(ActiveShopItemBuyInfo::BuyEvent,this));
		btn_buy->setPressedActionEnabled(true);
		auto l_buy = (Text*)Helper::seekWidgetByName(ppanel,"Label_enter");
		const char *str3 = StringDataManager::getString("goods_auction_buy");
		char* shop_buy =const_cast<char*>(str3);
		l_buy->setString(shop_buy);

		setToDefaultNum();

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setAnchorPoint(Vec2(0,0));
		return true;
	}
	return false;
}

void ActiveShopItemBuyInfo::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ActiveShopItemBuyInfo::onExit()
{
	UIScene::onExit();
}

bool ActiveShopItemBuyInfo::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	//return resignFirstResponder(pTouch,this,true);
	return true;
}

void ActiveShopItemBuyInfo::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void ActiveShopItemBuyInfo::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void ActiveShopItemBuyInfo::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}


void ActiveShopItemBuyInfo::BuyEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (buyNum > 0)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5115, (void*)curHonorCommodity->id(), (void *)buyNum);
		}
		else
		{
			const char *str1 = StringDataManager::getString("goods_shop_pleaseInputNum");
			char* pleaseinputNum = const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(pleaseinputNum);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void ActiveShopItemBuyInfo::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}


void ActiveShopItemBuyInfo::getAmount(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button *)pSender;
		int id_ = btn->getTag();

		if (buyNum>999)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("goldStore_buy_inputNum"));
			return;
		}

		if (buyNum <= 0 && id_ == 0)
		{
			return;
		}
		char num[3];
		sprintf(num, "%d", id_);
		if (std::atoi(str_buyNum.c_str()) <= 0)
		{
			str_buyNum = num;
		}
		else
		{
			str_buyNum.append(num);
		}
		l_buyNum->setString(str_buyNum.c_str());
		buyNum = std::atoi(str_buyNum.c_str());

		buyPrice = basePrice * buyNum;
		char str_buyPrice[20];
		sprintf(str_buyPrice, "%d", buyPrice);
		l_totalPrice->setString(str_buyPrice);
		if (buyPrice <= ActiveData::instance()->getUserActive())   //ۻ�Ծ�ȹ��
		{
			l_totalPrice->setColor(Color3B(225, 255, 138));   //��ɫ
		}
		else
		{
			l_totalPrice->setColor(Color3B(255, 51, 51));   //��ɫ
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void ActiveShopItemBuyInfo::clearNum(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		str_buyNum.clear();
		l_buyNum->setString(str_buyNum.c_str());
		buyNum = std::atoi(str_buyNum.c_str());

		l_totalPrice->setString("");
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void ActiveShopItemBuyInfo::deleteNum(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (str_buyNum.size()>0)
		{
			str_buyNum.erase(str_buyNum.end() - 1);
			l_buyNum->setString(str_buyNum.c_str());
			buyNum = std::atoi(str_buyNum.c_str());

			buyPrice = basePrice * buyNum;
			char str_buyPrice[20];
			sprintf(str_buyPrice, "%d", buyPrice);
			l_totalPrice->setString(str_buyPrice);

			if (buyPrice <= ActiveData::instance()->getUserActive())   //��Ծ���㹻
			{
				l_totalPrice->setColor(Color3B(225, 255, 138));   //��ɫ
			}
			else
			{
				l_totalPrice->setColor(Color3B(255, 51, 51));   //��ɫ
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}



}

void ActiveShopItemBuyInfo::setToDefaultNum()
{
	l_buyNum->setString("1");
	buyNum = 1;

	buyPrice = basePrice * buyNum;
	char str_buyPrice [20];
	sprintf(str_buyPrice,"%d",buyPrice);
	l_totalPrice->setString(str_buyPrice);
	if (buyPrice <= ActiveData::instance()->getUserActive())   //��Ծ�ȹ��
	{
		l_totalPrice->setColor(Color3B(225,255,138));   //��ɫ
	}
	else
	{
		l_totalPrice->setColor(Color3B(255,51,51));   //��ɫ
	}
}
