#ifndef _UI_ACTIVE_ACTIVEMISSIONACTIONS_H_
#define _UI_ACTIVE_ACTIVEMISSIONACTIONS_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class MissionInfo;

typedef void (Ref::*SEL_FinishedMove)();
#define coco_finishedMove(_SELECTOR) (SEL_FinishedMove)(&_SELECTOR)


class ActiveMoveAction
{
public:
	ActiveMoveAction();
	~ActiveMoveAction();

	static ActiveMoveAction * create(MissionInfo *missionInfo);
	void init(MissionInfo *missionInfo);

	//void addFinishedMove(Ref * pSender,SEL_FinishedMove selector);

	//void update(float dt);

	float getDistance();
	bool isFinishedMove();
private:
	void runToTarget();
	//void doFinishedMove();
private:
	float distance;
	std::string targetMapId;
	long long targetActorId;
	Vec2 targetPos;
	Ref* m_pListener;
	SEL_FinishedMove m_pfnSelector;
};

////////////////////////////////////////////////////////////////////

class ActiveTalkWithNpcAction
{
public:
	ActiveTalkWithNpcAction();
	~ActiveTalkWithNpcAction();

	static ActiveTalkWithNpcAction * create(MissionInfo *missionInfo);
	void init(MissionInfo *missionInfo);
	bool isFinishedMove();
	void doMTalkWithNpcAction();
private:
	MissionInfo * curMissionInfo;
	ActiveMoveAction * moveAction;
	bool isfirst;

};

#endif
