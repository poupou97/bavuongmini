#ifndef _UI_ACTIVE_ACTIVETODAYTABLEVIEW_H_
#define _UI_ACTIVE_ACTIVETODAYTABLEVIEW_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ���ջ�Ծ��ģ�飨tableView��
 * @author liuliang
 * @version 0.1.0
 * @date 2014.07.25
 */

class CActiveLabelToday;

class ActiveTodayTableView:public TableViewDataSource, public TableViewDelegate, public Ref
{
public:
	static ActiveTodayTableView * s_activeToadyTableView;
	static ActiveTodayTableView * instance();

private:
	ActiveTodayTableView(void);
	~ActiveTodayTableView(void);

public:
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

public:
	std::vector<CActiveLabelToday *> m_vector_labelToday;

private:
	void initCell(TableViewCell * cell, unsigned int nIndex);

	void initGoodsInfo(Layer * ui_layer, unsigned int nIndex, cocos2d::extension::Scale9Sprite* sprite_bg);

	std::string getEquipmentQualityByIndex(int quality);													// �� �Ʒ� ׻�øgoods���Ҫ�ĵ�ͼ

public:
	void initDataFromInternet();

	void callBackGoodIcon(Ref *pSender, Widget::TouchEventType type);																	// ��icon�ʱ����Ӧ
	void callBackActiveTodayBtn(Ref *pSender, Widget::TouchEventType type);																// �� ���ջ�Ծ� Ȱ�ť
};

#endif

