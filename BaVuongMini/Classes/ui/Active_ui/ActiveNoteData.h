#ifndef _UI_ACTIVE_ACTIVENOTEDATA_H_
#define _UI_ACTIVE_ACTIVENOTEDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ��Ծ��NoteData�������� �� UI ���ݵĽṹ�壩
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.25
 */
class CActiveNote;

class ActiveNoteData
{
public:
	ActiveNoteData(void);
	~ActiveNoteData(void);

public:
	Label * m_label_content_name;							// ����-����
	Label * m_label_content_time;							// ����-ʱ��
	Label * m_label_content_gift;								// ����-����
	Label * m_label_content_limit;								// ����-����
	Label * m_label_content_describe;						// ����-����

	CActiveNote * m_activeNote;

};

#endif

