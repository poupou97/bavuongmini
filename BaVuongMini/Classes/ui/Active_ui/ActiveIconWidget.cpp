#include "ActiveIconWidget.h"
#include "../../GameView.h"
#include "ActiveUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../utils/StaticDataManager.h"
#include "ActiveShopData.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "ActiveData.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/element/CActiveLabel.h"
#include "ActiveLabelData.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "AppMacros.h"

#define  TAG_TUTORIALPARTICLE_ACTIVEICON 500

ActiveIconWidget::ActiveIconWidget(void)
	:m_btn_activeIcon(NULL)
	,m_layer_active(NULL)
	,m_label_active_text(NULL)
	,m_spirte_fontBg(NULL)
	,m_tutorialParticle(NULL)
{

}


ActiveIconWidget::~ActiveIconWidget(void)
{
	
}

ActiveIconWidget* ActiveIconWidget::create()
{
	auto activeIcon = new ActiveIconWidget();
	if (activeIcon && activeIcon->init())
	{
		activeIcon->autorelease();
		return activeIcon;
	}
	CC_SAFE_DELETE(activeIcon);
	return NULL;
}

bool ActiveIconWidget::init()
{
	if (Widget::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();


		// icon
		m_btn_activeIcon = Button::create();
		m_btn_activeIcon->setTouchEnabled(true);
		m_btn_activeIcon->setPressedActionEnabled(true);

		m_btn_activeIcon->loadTextures("gamescene_state/zhujiemian3/tubiao/active.png", "gamescene_state/zhujiemian3/tubiao/active.png","");
		
		m_btn_activeIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
		//m_btn_activeIcon->setPosition(Vec2( winSize.width - 70 - 159 - 80, winSize.height-70 - 80));
		m_btn_activeIcon->setPosition(Vec2(0, 0));
		m_btn_activeIcon->addTouchEventListener(CC_CALLBACK_2(ActiveIconWidget::callBackActive, this));

		// label
		const char * str_active = StringDataManager::getString("activy_chartInfo2");

		m_label_active_text = Label::createWithTTF(str_active, APP_FONT_NAME, 14);
		m_label_active_text->setColor(Color3B(255, 246, 0));
		m_label_active_text->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_active_text->setPosition(Vec2(m_btn_activeIcon->getPosition().x, m_btn_activeIcon->getPosition().y - m_btn_activeIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = cocos2d::extension::Scale9Sprite::create(Rect(5, 10, 1, 4) , "res_ui/zhezhao70.png");
		m_spirte_fontBg->setPreferredSize(Size(47, 14));
		m_spirte_fontBg->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_active_text->getPosition());


		// ��ʱLayer������ʹ���䶨ʱ�
		auto tmpLayer = Layer::create();
		//tmpLayer->setTouchEnabled(true);
		tmpLayer->setPosition(Vec2(0, 0));
		tmpLayer->setContentSize(winSize);
		tmpLayer->schedule(schedule_selector(ActiveIconWidget::update), 1.0f);

		this->addChild(tmpLayer);
		this->addChild(m_btn_activeIcon);

		this->scheduleUpdate();
		//this->setTouchEnabled(true);
		this->setContentSize(m_btn_activeIcon->getContentSize());

		

		return true;
	}
	return false;
}

void ActiveIconWidget::callBackActive(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

	}
	break;

	case Widget::TouchEventType::CANCELED:
	{
		//add by yangjun 2014.10.13
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		// ��ʼ�� UI WINDOW��
		Size winSize = Director::getInstance()->getVisibleSize();

		auto pTmpActiveUI = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
		if (NULL == pTmpActiveUI)
		{
			auto pActiveUI = ActiveUI::create();
			pActiveUI->setIgnoreAnchorPointForPosition(false);
			pActiveUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			pActiveUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			pActiveUI->setTag(kTagActiveUI);

			GameView::getInstance()->getMainUIScene()->addChild(pActiveUI);

			pActiveUI->initActiveUIData();

			// ���� ��Ծ���̵���ݣ�����Ծ���̵���ݲ�Ϊ��ʱ��˵���Ѿ�������������ݣ���ô���ٷ������
			if (ActiveShopData::instance()->m_vector_activeShopSource.empty())
			{
				// ������������ȡ��Ʒ
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5114, NULL);
			}
		}

		auto guideMap = (GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
		if (guideMap)
		{
			Script* sc = ScriptManager::getInstance()->getScriptById(guideMap->mTutorialScriptInstanceId);
			if (sc != NULL)
				sc->endCommand(this);
		}
	}
		break;

	default:
		break;
	}

}

bool ActiveIconWidget::IsNeedParticle()
{
	bool bFlag = false;

	for (unsigned int i = 0; i < ActiveData::instance()->m_vector_internet_label.size(); i++)
	{
		// ��ǰ�û��ȼ�
		int nPlayLevel = GameView::getInstance()->myplayer->getActiveRole()->level();

		if (nPlayLevel >= ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->levelforeshow())
		{
			auto activeLabel = ActiveData::instance()->m_vector_internet_label.at(i);

			int nHasGetActivity = activeLabel->partakeactivealready();
			int nAllActivity = activeLabel->partakeactiveall();

			int nColor = activeLabel->color();
			//��ɫ �0�1
			if (1 == nColor && nHasGetActivity != nAllActivity)
			{
				bFlag = true;

				return bFlag;
			}
		}
	}

	return bFlag;
}

void ActiveIconWidget::update( float dt )
{
	if (this->IsNeedParticle())
	{
		auto tutorialParticle = (CCTutorialParticle*)this->getVirtualRenderer()->getChildByTag(TAG_TUTORIALPARTICLE_ACTIVEICON);
		if (NULL == tutorialParticle)
		{
			//m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", m_btn_onlineGiftIcon->getContentSize().width, m_btn_onlineGiftIcon->getContentSize().height + 10);
			//m_tutorialParticle->setPosition(Vec2(m_btn_onlineGiftIcon->getPosition().x, m_btn_onlineGiftIcon->getPosition().y - 35));
			m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", 30, 46);
			m_tutorialParticle->setPosition(Vec2(m_btn_activeIcon->getPosition().x, m_btn_activeIcon->getPosition().y - 25));
			m_tutorialParticle->setAnchorPoint(Vec2(0.5f, 0.5f));
			m_tutorialParticle->setTag(TAG_TUTORIALPARTICLE_ACTIVEICON);
			this->addChild(m_tutorialParticle);
		}
	}
	else
	{
		auto tutorialParticle = (CCTutorialParticle*)this->getVirtualRenderer()->getChildByTag(TAG_TUTORIALPARTICLE_ACTIVEICON);
		if (NULL != tutorialParticle)
		{
			m_tutorialParticle->removeFromParent();
		}
	}
}
