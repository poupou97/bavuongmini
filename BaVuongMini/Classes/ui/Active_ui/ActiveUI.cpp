#include "ActiveUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../GameAudio.h"
#include "../../utils/StaticDataManager.h"
#include "ActiveData.h"
#include "../../messageclient/element/CActiveLabel.h"
#include "../../messageclient/element/CActiveNote.h"
#include "../../AppMacros.h"
#include "GameView.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../SignDaily_ui/SignDailyUI.h"
#include "../FivePersonInstance/InstanceDetailUI.h"
#include "../FivePersonInstance/FivePersonInstance.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../missionscene/MissionManager.h"
#include "ActiveNoteData.h"
#include "ActiveLabelData.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "ActiveShopData.h"
#include "ActiveShopUI.h"
#include "../offlinearena_ui/OffLineArenaUI.h"
#include "ActiveMissonManager.h"
#include "../../utils/GameUtils.h"
#include "../../ui/Question_ui/QuestionUI.h"
#include "../../ui/Question_ui/QuestionData.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/challengeRound/ChallengeRoundUi.h"
#include "ActiveTodayTableView.h"
#include "../../messageclient/element/CActiveLabelToday.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"
#include "../family_ui/FamilyUI.h"
#include "../../messageclient/element/CTargetPosition.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CMapInfo.h"
#include "ActiveManager.h"
#include "../../messageclient/element/CTargetNpc.h"
#include "math/CCGeometry.h"
#include "../FivePersonInstance/InstanceMapUI.h"
#include "WorldBoss_ui/WorldBossUI.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../gamescene_state/GameSceneEffects.h"

using namespace CocosDenshion;

Layout * ActiveUI::s_pPanel;

#define  TAG_TUTORIALPARTICLE 500
#define  TAG_SCROLLVIEW 50

#define  SelectImageTag 458

const Size SizeCellSize = Size(466, 55);

const Size SizeTableView = Size(466, 275);
const Vec2 Vec2TableView = Vec2(71, 138);

const Size SizeContentScrollView = Size(180, 255);
const Vec2 Vec2ContentScrollView = Vec2(553, 179);

const Size SizeLeftTableView = Size(198, 243);

using namespace cocos2d::ui;


ActiveUI::ActiveUI(void)
	:m_tableView(NULL)
	,m_contentScrollView(NULL)
	,m_label_content_name(NULL)
	,m_label_content_time(NULL)
	,m_label_content_gift(NULL)
	,m_label_content_describe(NULL)
	,m_label_activeAll(NULL)
	,m_labelTTF_activeAll(NULL)
	,m_nTableViewSize(0)
	,m_base_layer(NULL)
	,m_tab(NULL)
	,m_panel_huoyuedu(NULL)
	,m_leftTableView(NULL)
	,m_label_activeToday(NULL)
	,m_labelBMF_activeToday(NULL)
	,m_label_gift_num_1(NULL)
	,m_label_gift_num_2(NULL)
	,m_label_gift_num_3(NULL)
	,m_label_gift_num_4(NULL)
	,m_label_gift_num_5(NULL)
	,m_btn_gift_frame_1(NULL)
	,m_btn_gift_frame_2(NULL)
	,m_btn_gift_frame_3(NULL)
	,m_btn_gift_frame_4(NULL)
	,m_btn_gift_frame_5(NULL)
	,m_imageView_gift_icon_1(NULL)
	,m_imageView_gift_icon_2(NULL)
	,m_imageView_gift_icon_3(NULL)
	,m_imageView_gift_icon_4(NULL)
	,m_imageView_gift_icon_5(NULL)
	,m_labelBMF_gift_1(NULL)
	,m_labelBMF_gift_2(NULL)
	,m_labelBMF_gift_3(NULL)
	,m_labelBMF_gift_4(NULL)
	,m_labelBMF_gift_5(NULL)
	,m_imageView_progress_1(NULL)
	,m_imageView_progress_2(NULL)
	,m_imageView_progress_3(NULL)
	,m_imageView_progress_4(NULL)
	,m_imageView_progress_5(NULL)
{

}


ActiveUI::~ActiveUI(void)
{
	std::vector<ActiveNoteData *>::iterator iterActiveNote;
	for (iterActiveNote = m_vector_activeNote.begin(); iterActiveNote != m_vector_activeNote.end(); iterActiveNote++)
	{
		delete *iterActiveNote;
	}
	m_vector_activeNote.clear();

	std::vector<ActiveLabelData *>::iterator iterActiveLabel;
	for (iterActiveLabel = m_vector_activeLabel.begin(); iterActiveLabel != m_vector_activeLabel.end(); iterActiveLabel++)
	{
		delete *iterActiveLabel;
	}
	m_vector_activeLabel.clear();
}

ActiveUI* ActiveUI::create()
{
	auto activeUI = new ActiveUI();
	if (activeUI && activeUI->init())
	{
		activeUI->autorelease();
		return activeUI;
	}
	CC_SAFE_DELETE(activeUI);
	return NULL;
}

bool ActiveUI::init()
{
	if (UIScene::init())
	{
		winSize = Director::getInstance()->getVisibleSize();

		m_base_layer = Layer::create();
		m_base_layer->setIgnoreAnchorPointForPosition(false);
		m_base_layer->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_base_layer->setContentSize(Size(800, 480));
		m_base_layer->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		this->addChild(m_base_layer);	

		m_active_layer = Layer::create();
		m_active_layer->setIgnoreAnchorPointForPosition(false);
		m_active_layer->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_active_layer->setContentSize(Size(800, 480));
		m_active_layer->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		this->addChild(m_active_layer);
		m_active_layer->setVisible(true);

		m_boss_layer = Layer::create();
		m_boss_layer->setIgnoreAnchorPointForPosition(false);
		m_boss_layer->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_boss_layer->setContentSize(Size(800, 480));
		m_boss_layer->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		this->addChild(m_boss_layer);
		m_boss_layer->setVisible(false);

		m_challenge_layer  = Layer::create();
		m_challenge_layer->setIgnoreAnchorPointForPosition(false);
		m_challenge_layer->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_challenge_layer->setContentSize(Size(800, 480));
		m_challenge_layer->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		this->addChild(m_challenge_layer);
		this->initChallengeUi();
		m_challenge_layer->setVisible(false);
		
		m_up_layer = Layer::create();
		m_up_layer->setIgnoreAnchorPointForPosition(false);
		m_up_layer->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_up_layer->setContentSize(Size(800, 480));
		m_up_layer->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		this->addChild(m_up_layer);	

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winSize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		if(LoadSceneLayer::activeLayout->getParent() != NULL)
		{
			LoadSceneLayer::activeLayout->removeFromParentAndCleanup(false);
		}

		s_pPanel = LoadSceneLayer::activeLayout;
		s_pPanel->setAnchorPoint(Vec2(0.5f, 0.5f));
		s_pPanel->setContentSize(Size(800, 480));
		s_pPanel->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		s_pPanel->setScale(1.0f);
		s_pPanel->setTouchEnabled(true);
		m_pLayer->addChild(s_pPanel);

		s_panel_activeUI = (Layout*)Helper::seekWidgetByName(s_pPanel,"Panel_huoyuedu");
		s_panel_activeUI->setVisible(true);
		s_panel_worldBossUI = (Layout*)Helper::seekWidgetByName(s_pPanel,"Panel_worldBoss");
		s_panel_worldBossUI->setVisible(false);
		s_panel_singCopyUI =  (Layout*)Helper::seekWidgetByName(s_pPanel,"Panel_guoguanzhanjiang");
		s_panel_singCopyUI->setVisible(false);

		// ��ʼ�����
		const char * firstStr = StringDataManager::getString("UIName_active_huo");
		const char * secondStr = StringDataManager::getString("UIName_active_yue");
		const char * thirdStr = StringDataManager::getString("UIName_active_du");
		auto atmature = MainScene::createPendantAnm(firstStr, secondStr, thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		m_base_layer->addChild(atmature);

		initUI();

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void ActiveUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void ActiveUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

void ActiveUI::callBackBtnClolse(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void ActiveUI::initUI()
{
	// close btn
	auto pBtnClose = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnabled(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addTouchEventListener(CC_CALLBACK_2(ActiveUI::callBackBtnClolse,this));

	// shop btn
	auto pShopBtn = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_shop");
	pShopBtn->setTouchEnabled(true);
	pShopBtn->setPressedActionEnabled(true);
	pShopBtn->addTouchEventListener(CC_CALLBACK_2(ActiveUI::callBackBtnAcitveShop, this));


	// ��ҵĻ�Ծ� label(Ȳ��ñ༭���� ��ҵĻ�Ծ� ȼ����֣�Ҫ����� ������������һ�)
	auto pLabel_myActive = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_myActive"); 
	pLabel_myActive->setVisible(false);

	auto pImageView_frame_details = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_1073_0");
	
	const char * str_myActive = StringDataManager::getString("activy_myActive");
	auto pLabelTTF_myActive = Label::createWithTTF(str_myActive, APP_FONT_NAME, 15, Size(120, 0), TextHAlignment::LEFT);
	pLabelTTF_myActive->setColor(Color3B(255, 246, 0));

	pLabelTTF_myActive->setAnchorPoint(Vec2(0, 0));
	pLabelTTF_myActive->setIgnoreAnchorPointForPosition(false);
	pLabelTTF_myActive->setPosition(Vec2(pImageView_frame_details->getPosition().x + 36, pImageView_frame_details->getPosition().y + 15));

	this->m_active_layer->addChild(pLabelTTF_myActive);
	

	// ��û��ܵĻ�Ծ�
	int nUserActive = ActiveData::instance()->getUserActive();
	char str_userActive[20];
	sprintf(str_userActive, "%d", nUserActive);

	m_label_activeAll = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_activityValue");
	m_label_activeAll->setString(str_userActive);
	m_label_activeAll->setVisible(false);


	m_labelTTF_activeAll = Label::createWithTTF(str_userActive, APP_FONT_NAME, 15, Size(120, 0), TextHAlignment::LEFT);
	m_labelTTF_activeAll->setAnchorPoint(Vec2(0, 0));
	m_labelTTF_activeAll->setIgnoreAnchorPointForPosition(false);
	m_labelTTF_activeAll->setPosition(Vec2(pImageView_frame_details->getPosition().x + 125, pImageView_frame_details->getPosition().y + 15));

	this->m_active_layer->addChild(m_labelTTF_activeAll);


	// ȳ�ʼ�� ���ջ�Ծ����
	this->initTodayUI();

	// tab(ش�Ϊ֮ǰ�UITabģ����ڲ���Ҫ�ˣ����ұ��)
	//const char * normalImage = "res_ui/tab_3_off.png";
	//const char * selectImage = "res_ui/tab_3_on.png";
	//const char * finalImage = "";
	//const char * highLightImage = "res_ui/tab_3_on.png";
	//const char *str1 = StringDataManager::getString("activy_chartInfo2");
	//const char *str2 = StringDataManager::getString("WorldBossUI_uitab_boss");
	////const char *str3 = StringDataManager::getString("ActiveUi_ChallengeRoundTab");
	//char* p1 = const_cast<char*>(str1);
	//char* p2 = const_cast<char*>(str2);
	////char* p3 = const_cast<char*>(str3);
	//char* pHightLightImage = const_cast<char*>(highLightImage);
	//char * mainNames[] = {p1,p2};
	//m_tab = UITab::createWithBMFont(2, normalImage, selectImage, finalImage, mainNames,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
	//m_tab->setAnchorPoint(Vec2(0,0));
	//m_tab->setPosition(Vec2(74, 410));
	//m_tab->setHighLightImage(pHightLightImage);
	//m_tab->setDefaultPanelByIndex(0);
	//m_tab->addIndexChangedEvent(this, coco_indexchangedselector(ActiveUI::tabIndexChangedEvent));
	//m_tab->setPressedActionEnabled(true);
	//m_base_layer->addChild(m_tab);
	

	Size winSize = Director::getInstance()->getVisibleSize();

	// ��tableView
	m_tableView = TableView::create(this, SizeTableView);
	m_tableView ->setTouchEnabled(true);   // �֧��ѡ��״̬����ʾ
	//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 25, 1, 1), Vec2(0,0));   // ���þŹ���ͼƬ
	m_tableView->setDirection(TableView::Direction::VERTICAL);
	m_tableView->setAnchorPoint(Vec2(0, 0));
	m_tableView->setPosition(Vec2TableView);
	m_tableView->setDelegate(this);
	m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//m_tableView->setPressedActionEnabled(false);

	m_active_layer->addChild(m_tableView);


	//// ���Ľ��ջ�Ծ� tabelView
	//m_leftTableView = TableView::create(ActiveTodayTableView::instance(), SizeLeftTableView);
	//m_leftTableView->setSelectedEnable(false);
	//m_leftTableView->setSelectedScale9Texture("res_ui/mengban_green.png", Rect(7, 7, 1, 1), Vec2(0,0));   // ����þŹ���ͼƬ
	//m_leftTableView->setDirection(TableView::Direction::VERTICAL);
	//m_leftTableView->setAnchorPoint(Vec2(0, 0));
	//m_leftTableView->setPosition(Vec2Make(73, 130));
	//m_leftTableView->setDelegate(ActiveTodayTableView::instance());
	//m_leftTableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//m_leftTableView->setPressedActionEnabled(false);

	//m_active_layer->addChild(m_leftTableView);

	// ��Ӳ�����tableView���ɰ�
	//ImageView* pImageView_frame_main = (ImageView*)Helper::seekWidgetByName(s_pPanel, "");
	// 

	/*ImageView * imageViewLight = ImageView::create();
	imageViewLight->loadTexture("res_ui/Prepaid/light_round.png");
	imageViewLight->setScale9Enabled(true);
	imageViewLight->setContentSize(Size(73,74));
	imageViewLight->setAnchorPoint(Vec2(0.5f,0.5f));
	imageViewLight->setPosition(Vec2(-84,-2));
	btn_buy->addChild(imageViewLight);*/

	/*ImageView* pImageView_frame_main_copy = ImageView::create();
	pImageView_frame_main_copy->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
	pImageView_frame_main_copy->setAnchorPoint(Vec2(0, 0));
	pImageView_frame_main_copy->setPosition(Vec2(64, 138));
	pImageView_frame_main_copy->setScale9Enabled(true);
	pImageView_frame_main_copy->setCapInsets(Rect(32, 32, 1, 1));
	pImageView_frame_main_copy->setContentSize(Size(481, 300));
	m_active_layer->addChild(pImageView_frame_main_copy);*/
	
}


bool ActiveUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void ActiveUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void ActiveUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void ActiveUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void ActiveUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void ActiveUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void ActiveUI::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void ActiveUI::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void ActiveUI::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void ActiveUI::tableCellTouched( TableView* table, TableViewCell* cell )
{

	m_curActiveNote = m_vector_activeNote.at(cell->getIdx());

	this->reloatActiveInfoScrollView();
}

cocos2d::Size ActiveUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return SizeCellSize;
}

cocos2d::extension::TableViewCell* ActiveUI::tableCellAtIndex(TableView *table, ssize_t idx)
{
	__String *string = __String::createWithFormat("%d", idx);
	TableViewCell * cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	if (idx < m_vector_activeLabel.size())
	{
		initCell(cell, idx);
	}

	return cell;
}

ssize_t ActiveUI::numberOfCellsInTableView( TableView *table )
{
	return this->getTableViewSize();
}

void ActiveUI::reloadActiveInfoData()
{
	if (m_vector_activeNote.empty())
	{
		return;
	}

	if (m_active_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		m_active_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	m_contentScrollView = cocos2d::extension::ScrollView::create();
	m_contentScrollView->setContentSize(SizeContentScrollView);
	m_contentScrollView->setIgnoreAnchorPointForPosition(false);
	m_contentScrollView->setTouchEnabled(true);
	m_contentScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
	m_contentScrollView->setAnchorPoint(Vec2(0,0));
	m_contentScrollView->setPosition(Vec2ContentScrollView);
	m_contentScrollView->setBounceable(true);
	m_contentScrollView->setClippingToBounds(true);
	m_contentScrollView->setTag(TAG_SCROLLVIEW);

	m_active_layer->addChild(m_contentScrollView);


	int nScrollHeight = 0;
	int nSpace = 5;

	// ����-���
	const char * str_name = StringDataManager::getString("activy_name");
	m_label_content_name = Label::createWithTTF(str_name, APP_FONT_NAME, 18, Size(60, 0), TextHAlignment::LEFT);
	m_label_content_name->setColor(Color3B(255, 246, 0));
    Color4B color = Color4B::BLACK;
	m_label_content_name->enableShadow(color,Size(1.0f, -1.0f), 1.0f);

	// ����-���-text
	std::string str_contentName = m_curActiveNote->m_activeNote->notename();
	m_curActiveNote->m_label_content_name = Label::createWithTTF(str_contentName.c_str(), APP_FONT_NAME, 18, Size(396, 0), TextHAlignment::LEFT);
	m_curActiveNote->m_label_content_name->setColor(Color3B(255, 255, 255));
	m_curActiveNote->m_label_content_name->enableShadow(color,Size(1.0f, -1.0f), 1.0f);

	int nZeroLineHeight = m_curActiveNote->m_label_content_name->getContentSize().height + nSpace;
	int nZeroLabelHeight = m_label_content_name->getContentSize().height + nSpace;

	// ����-�ʱ�
	const char * str_time = StringDataManager::getString("activy_time");
	m_label_content_time = Label::createWithTTF(str_time, APP_FONT_NAME, 18, Size(60, 0), TextHAlignment::LEFT);
	m_label_content_time->setColor(Color3B(255, 246, 0));
	m_label_content_time->enableShadow(color, Size(1.0f, -1.0f), 1.0f);


	// ����-�ʱ�-text
	std::string str_contentTime = m_curActiveNote->m_activeNote->notetime();
	m_curActiveNote->m_label_content_time = Label::createWithTTF(str_contentTime.c_str(), APP_FONT_NAME, 18, Size(396, 0), TextHAlignment::LEFT);
	m_curActiveNote->m_label_content_time->setColor(Color3B(255, 255, 255));
	m_curActiveNote->m_label_content_time->enableShadow(color, Size(1.0f, -1.0f), 1.0f);

	int nFirstLineHeight = nZeroLineHeight + m_curActiveNote->m_label_content_time->getContentSize().height + nSpace;
	int nFirstLabelHeight = nZeroLineHeight + m_label_content_time->getContentSize().height + nSpace;

	// ����-齱�
	const char * str_gift = StringDataManager::getString("activy_gift");
	m_label_content_gift = Label::createWithTTF(str_gift, APP_FONT_NAME, 18, Size(60, 0), TextHAlignment::LEFT);
	m_label_content_gift->setColor(Color3B(255, 246, 0));
	m_label_content_gift->enableShadow(color,Size(1.0f, -1.0f));

	// ����-齱�-text
	std::string str_contentGift = m_curActiveNote->m_activeNote->notereward();
	m_curActiveNote->m_label_content_gift = Label::createWithTTF(str_contentGift.c_str(), APP_FONT_NAME, 18, Size(396, 0), TextHAlignment::LEFT);
	m_curActiveNote->m_label_content_gift->setColor(Color3B(255, 255, 255));
	m_curActiveNote->m_label_content_gift->enableShadow(color,Size(1.0f, -1.0f), 1.0f);

	int nSecondLineHeight = nFirstLineHeight + m_curActiveNote->m_label_content_gift->getContentSize().height + nSpace;
	int nSecondLabelHeight = nFirstLineHeight + m_label_content_gift->getContentSize().height + nSpace;

	// ����-����
	const char * str_limit = StringDataManager::getString("activy_limit");
	m_label_content_limit = Label::createWithTTF(str_limit, APP_FONT_NAME, 18, Size(60, 0), TextHAlignment::LEFT);
	m_label_content_limit->setColor(Color3B(255, 246, 0));
	m_label_content_limit->enableShadow(color,Size(1.0f, -1.0f));

	// ����-����-text
	std::string str_contentLimit = m_curActiveNote->m_activeNote->notelimit();
	m_curActiveNote->m_label_content_limit = Label::createWithTTF(str_contentLimit.c_str(), APP_FONT_NAME, 18, Size(396, 0), TextHAlignment::LEFT);
	m_curActiveNote->m_label_content_limit->setColor(Color3B(255, 255, 255));
	m_curActiveNote->m_label_content_limit->enableShadow(color,Size(1.0f, -1.0f), 1.0f);

	int nThirdLineHeight = nSecondLineHeight + m_curActiveNote->m_label_content_limit->getContentSize().height + nSpace;
	int nThirdLabelHeight = nSecondLineHeight + m_label_content_limit->getContentSize().height + nSpace;

	// ����-����
	const char * str_describe = StringDataManager::getString("activy_describe");
	m_label_content_describe = Label::createWithTTF(str_describe, APP_FONT_NAME, 18, Size(60, 0), TextHAlignment::LEFT);
	m_label_content_describe->setColor(Color3B(255, 246, 0));
	m_label_content_describe->enableShadow(color,Size(1.0f, -1.0f));

	// ����-����-text
	std::string str_contentDescribe = m_curActiveNote->m_activeNote->notecontent();
	m_curActiveNote->m_label_content_describe= Label::createWithTTF(str_contentDescribe.c_str(), APP_FONT_NAME, 18, Size(100, 0), TextHAlignment::LEFT);
	m_curActiveNote->m_label_content_describe->setColor(Color3B(255, 255, 255));
	m_curActiveNote->m_label_content_describe->enableShadow(color,Size(1.0f, -1.0f), 1.0f);

	int nFourthLineHeight = nThirdLineHeight + m_curActiveNote->m_label_content_describe->getContentSize().height + nSpace;
	int nFourthLabelHeight = nThirdLineHeight + m_label_content_describe->getContentSize().height + nSpace;

	nScrollHeight = nFourthLineHeight + nSpace;
	if (nScrollHeight > SizeContentScrollView.height)
	{
		m_contentScrollView->setContentSize(Size(SizeContentScrollView.width, nScrollHeight));
		m_contentScrollView->setContentOffset(Vec2(0, SizeContentScrollView.height - nScrollHeight));
	}
	else 
	{
		m_contentScrollView->setContentSize(SizeContentScrollView);
	}

	// name
	m_label_content_name->setAnchorPoint(Vec2(0, 1.0f));
	m_label_content_name->setPosition(Vec2(15, m_contentScrollView->getContentSize().height - nZeroLabelHeight));

	m_curActiveNote->m_label_content_name->setAnchorPoint(Vec2(0, 1.0f));
	m_curActiveNote->m_label_content_name->setPosition(Vec2(m_label_content_name->getPosition().x + m_label_content_name->getContentSize().width - 10, m_contentScrollView->getContentSize().height - nZeroLineHeight));

	m_contentScrollView->addChild(m_label_content_name);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_name);

	// time
	m_label_content_time->setAnchorPoint(Vec2(0, 1.0f));
	m_label_content_time->setPosition(Vec2(15, m_contentScrollView->getContentSize().height - nFirstLabelHeight));

	m_curActiveNote->m_label_content_time->setAnchorPoint(Vec2(0, 1.0f));
	m_curActiveNote->m_label_content_time->setPosition(Vec2(m_label_content_time->getPosition().x + m_label_content_time->getContentSize().width - 10, m_contentScrollView->getContentSize().height - nFirstLineHeight));

	m_contentScrollView->addChild(m_label_content_time);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_time);

	// gift
	m_label_content_gift->setAnchorPoint(Vec2(0, 1.0f));
	m_label_content_gift->setPosition(Vec2(15, m_contentScrollView->getContentSize().height - nSecondLabelHeight));

	m_curActiveNote->m_label_content_gift->setAnchorPoint(Vec2(0, 1.0f));
	m_curActiveNote->m_label_content_gift->setPosition(Vec2(m_label_content_gift->getPosition().x + m_label_content_gift->getContentSize().width - 10, m_contentScrollView->getContentSize().height - nSecondLineHeight));

	m_contentScrollView->addChild(m_label_content_gift);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_gift);

	// limit
	m_label_content_limit->setAnchorPoint(Vec2(0, 1.0f));
	m_label_content_limit->setPosition(Vec2(15, m_contentScrollView->getContentSize().height - nThirdLabelHeight));

	m_curActiveNote->m_label_content_limit->setAnchorPoint(Vec2(0, 1.0f));
	m_curActiveNote->m_label_content_limit->setPosition(Vec2(m_label_content_limit->getPosition().x + m_label_content_limit->getContentSize().width - 10, m_contentScrollView->getContentSize().height - nThirdLineHeight));

	m_contentScrollView->addChild(m_label_content_limit);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_limit);

	// describe
	m_label_content_describe->setAnchorPoint(Vec2(0, 1.0f));
	m_label_content_describe->setPosition(Vec2(15, m_contentScrollView->getContentSize().height - nFourthLabelHeight));

	m_curActiveNote->m_label_content_describe->setAnchorPoint(Vec2(0, 1.0f));
	m_curActiveNote->m_label_content_describe->setPosition(Vec2(m_label_content_describe->getPosition().x + m_label_content_describe->getContentSize().width - 10, m_contentScrollView->getContentSize().height - nFourthLineHeight));

	m_contentScrollView->addChild(m_label_content_describe);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_describe);

}

void ActiveUI::initCell( TableViewCell * cell , unsigned int nIndex)
{
	// ��ͼ��normal��
	auto sprite_frame_normal = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
	sprite_frame_normal->setContentSize(Size(SizeCellSize.width - 6, SizeCellSize.height - 2));
	sprite_frame_normal->setCapInsets(Rect(18, 9, 2, 23));
	sprite_frame_normal->setAnchorPoint(Vec2::ZERO);
	sprite_frame_normal->setPosition(Vec2(3, 1));
	//cell->addChild(sprite_frame_normal);

	// ��ͼ�����⣩
	auto sprite_frame_special = cocos2d::extension::Scale9Sprite::create("res_ui/kuang000_new.png");
	sprite_frame_special->setContentSize(Size(SizeCellSize.width - 6, SizeCellSize.height - 2));
	sprite_frame_special->setCapInsets(Rect(18, 9, 2, 23));
	sprite_frame_special->setAnchorPoint(Vec2::ZERO);
	sprite_frame_special->setPosition(Vec2(3, 1));

	// ��ͼ��ɫ �0�1
	int nColorBg =  m_vector_activeLabel.at(nIndex)->m_activeLabel->color();
	if (0 == nColorBg)
	{
		cell->addChild(sprite_frame_normal);
	}
	else if (1 == nColorBg)
	{
		cell->addChild(sprite_frame_special);
	}


	// �������
	std::string str_name = m_vector_activeLabel.at(nIndex)->m_activeLabel->name();
	auto m_label_name = Label::createWithTTF(str_name.c_str(), APP_FONT_NAME, 18, Size(200, 0), TextHAlignment::LEFT);
	m_label_name->setColor(Color3B(255, 255, 255));
    auto color = Color4B::BLACK;
	m_label_name->enableShadow(color,Size(1.0f, -1.0f), 1.0f);
	m_label_name->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_label_name->setHorizontalAlignment(TextHAlignment::CENTER);
	m_label_name->setPosition(Vec2(65, sprite_frame_normal->getContentSize().height / 2));
	cell->addChild(m_label_name);

	// Ʋ��������ݣ�
	int m_nJoinCountAlready = m_vector_activeLabel.at(nIndex)->m_activeLabel->partakenumberalready();
	int m_nJoinCountAll = m_vector_activeLabel.at(nIndex)->m_activeLabel->partakenumberall();

	char str_joinCountAlready[10];
	sprintf(str_joinCountAlready, "%d", m_nJoinCountAlready);

	std::string str_joinCount = "";
	str_joinCount.append(str_joinCountAlready);
	str_joinCount.append("/");
	str_joinCount.append(getActiveJoinAll(m_nJoinCountAll).c_str());

	auto m_label_joinCountShow = Label::createWithTTF(str_joinCount.c_str(), APP_FONT_NAME, 18, Size(200, 0), TextHAlignment::LEFT);
	m_label_joinCountShow->setColor(Color3B(255, 255, 255));
	m_label_joinCountShow->enableShadow(color, Size(1.0f, -1.0f), 1.0f);
	m_label_joinCountShow->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_label_joinCountShow->setHorizontalAlignment(TextHAlignment::CENTER);
	m_label_joinCountShow->setPosition(Vec2(175, sprite_frame_normal->getContentSize().height / 2));
	cell->addChild(m_label_joinCountShow);

	// ��Ծ�(���)
	int m_nActiveAlready = m_vector_activeLabel.at(nIndex)->m_activeLabel->partakeactivealready();
	int m_nActiveAll = m_vector_activeLabel.at(nIndex)->m_activeLabel->partakeactiveall();

	char str_activeAlready[10];
	sprintf(str_activeAlready, "%d", m_nActiveAlready);

	std::string str_active = "";
	str_active.append(str_activeAlready);
	str_active.append("/");
	str_active.append(getActiveCanGetAll(m_nActiveAll).c_str());
	
	auto  m_label_active = Label::createWithTTF(str_active.c_str(), APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
	m_label_active->setColor(Color3B(255, 255, 255));
	m_label_active->enableShadow(color, Size(1.0f, -1.0f), 1.0f);
	m_label_active->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_label_active->setHorizontalAlignment(TextHAlignment::CENTER);
	m_label_active->setPosition(Vec2(260, sprite_frame_normal->getContentSize().height / 2));
	cell->addChild(m_label_active);

	// ��ܷ�μӻ(����Ҫ��ʾ)
	int m_nIsCanJoinActivity = m_vector_activeLabel.at(nIndex)->m_activeLabel->canpartake();

	char str_status[10];
	sprintf(str_status, "%d", m_nIsCanJoinActivity);

	auto m_label_status = Label::createWithTTF(str_status, APP_FONT_NAME, 18, Size(200, 0), TextHAlignment::LEFT);
	m_label_status->setColor(Color3B(255, 255, 255));
	m_label_status->enableShadow(color, Size(1.0f, -1.0f), 1.0f);
	m_label_status->setAnchorPoint(Vec2(0, 0.5f));
	m_label_status->setPosition(Vec2(300, sprite_frame_normal->getContentSize().height / 2));
	//cell->addChild(m_label_status);

	// �μӻ��ʱ�
	/*std::string str_joinActivityTime = m_vector_activeLabel.at(nIndex)->m_activeLabel->notetime();*/
	std::string str_disableReason = m_vector_activeLabel.at(nIndex)->m_activeLabel->notcanpartakeshow();
	auto m_label_joinActivityTime = Label::createWithTTF(str_disableReason.c_str(), APP_FONT_NAME, 18, Size(200, 0), TextHAlignment::LEFT);
	m_label_joinActivityTime->setColor(Color3B(255, 255, 255));
	m_label_joinActivityTime->enableShadow(color, Size(1.0f, -1.0f), 1.0f);
	m_label_joinActivityTime->setAnchorPoint(Vec2(0.5f, 0.5f));
	m_label_joinActivityTime->setHorizontalAlignment(TextHAlignment::CENTER);
	m_label_joinActivityTime->setPosition(Vec2(375, sprite_frame_normal->getContentSize().height / 2));
	//cell->addChild(m_label_joinActivityTime);

	// MenuBtn
	int m_nId = m_vector_activeLabel.at(nIndex)->m_activeLabel->id();
	auto sprite_btn = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_2.png");
	sprite_btn->setCapInsets(Rect(18, 9, 2, 23));
	sprite_btn->setContentSize(Size(103, 38));

	auto item = MenuItemSprite::create(sprite_btn, sprite_btn, sprite_btn, CC_CALLBACK_1(ActiveUI::callBackBtnJoin,this));
	item->setScale(1.2f);
	item->setTag(m_nId);

	auto selectMenu = CCMoveableMenu::create(item, NULL);
	selectMenu->setAnchorPoint(Vec2(0,0));
	selectMenu->setPosition(Vec2(380, sprite_frame_normal->getContentSize().height / 2));
	//cell->addChild(selectMenu);

	// ����text
	const char* str1 = StringDataManager::getString("activy_canyu");
	char* str_btnText = const_cast<char*>(str1);
	auto m_label_btnText = Label::createWithTTF(str_btnText, APP_FONT_NAME, 18, Size(200, 0), TextHAlignment::LEFT);
	m_label_btnText->setColor(Color3B(255, 255, 255));
	m_label_btnText->enableShadow(color, Size(1.0f, -1.0f), 1.0f);
	m_label_btnText->setAnchorPoint(Vec2(0, 0));
	/*m_label_btnText->setPosition(Vec2(365, sprite_frame->getContentSize().height / 2));*/
	m_label_btnText->setPosition(Vec2((m_label_btnText->getContentSize().width - sprite_btn->getContentSize().width) / 2 - 14, (sprite_btn->getContentSize().height - m_label_btnText->getContentSize().height) / 2));

	// Ĳ鿴�text
	const char* strLook = StringDataManager::getString("activy_chakan");
	char * str_btnLook = const_cast<char*>(strLook);
	auto m_label_btnTextLook = Label::createWithTTF(str_btnLook, APP_FONT_NAME, 18, Size(200, 0), TextHAlignment::LEFT);
	m_label_btnTextLook->setColor(Color3B(255, 255, 255));
	m_label_btnTextLook->enableShadow(color, Size(1.0f, -1.0f), 1.0f);
	m_label_btnTextLook->setAnchorPoint(Vec2(0, 0));
	/*m_label_btnText->setPosition(Vec2(365, sprite_frame->getContentSize().height / 2));*/
	m_label_btnTextLook->setPosition(Vec2((m_label_btnTextLook->getContentSize().width - sprite_btn->getContentSize().width) / 2 - 14, (sprite_btn->getContentSize().height - m_label_btnTextLook->getContentSize().height) / 2));

	

	//��ܷ�μӻ 0���ܡ�1�
	if (1 == m_nIsCanJoinActivity)
	{
		// ��� �ɲ�� � ��Ѿ���� ���ȣ�� ��� 밴ť����ʾ�������жϣ�����û��ȼ����ڵ�� �Ԥ��ȼ���� ���ʾ������ʾ������ ���ڵ��ڵȼ����ƣ�� ���ʾ ��ť
		if (m_nActiveAll == m_nActiveAlready)
		{
			//��Ծ�����ͣ�1.���2.�ǩ����3.������4.���뾺������5.���⡢6.���䡢7.����ս��8.���ն����9.��BOSS
			//��������(activeType:1,activeTypeId:12)
			int nActiveType = m_vector_activeLabel.at(nIndex)->m_activeLabel->activetype();
			int nActiveTypeId = m_vector_activeLabel.at(nIndex)->m_activeLabel->activetypeid();

			if (2 == nActiveType || 3 == nActiveType || 4 == nActiveType || 7 == nActiveType || 8 == nActiveType || 9 == nActiveType || (1 == nActiveType && 12 == nActiveTypeId))
			{
				// ���ʾ�鿴��ť
				item->addChild(m_label_btnTextLook);
				cell->addChild(selectMenu);
			}
			else
			{
				// ��ʾ�Ѵ�
				const char * str_show = StringDataManager::getString("activy_yidacheng");
				m_label_joinActivityTime->setString(str_show);
				cell->addChild(m_label_joinActivityTime);
			}
		}
		else
		{
			// ɵ�ǰ�û��ȼ�
			int nPlayLevel = GameView::getInstance()->myplayer->getActiveRole()->level();

			// ���ڵȼ�Ԥ��ȼ�
			if (nPlayLevel >= m_vector_activeLabel.at(nIndex)->m_activeLabel->levelforeshow())
			{
				if (nPlayLevel >= m_vector_activeLabel.at(nIndex)->m_activeLabel->levellimit())
				{
					// ��Ӱ�ť
					item->addChild(m_label_btnText);
					cell->addChild(selectMenu);
				}
				else
				{
					//// ��ʾ �������
					//const char * str_comeSoon = StringDataManager::getString("activy_comesoon");
					//m_label_joinActivityTime->setString(str_comeSoon);
					//cell->addChild(m_label_joinActivityTime);
					
					const char * str_level = StringDataManager::getString("activy_open");

					// XXż����
					int nLevelLimit = m_vector_activeLabel.at(nIndex)->m_activeLabel->levellimit();

					char charLevelLimit[20];
					sprintf(charLevelLimit, "%d", nLevelLimit);

					std::string str_openLevel = "";
					str_openLevel.append(charLevelLimit);
					str_openLevel.append(str_level);
					m_label_joinActivityTime->setString(str_openLevel.c_str());
					cell->addChild(m_label_joinActivityTime);
				}
			}
		}
	}
	else if(0 == m_nIsCanJoinActivity)
	{
		// Ų��ܲ��
		cell->addChild(m_label_joinActivityTime);
	}

}

void ActiveUI::callBackBtnJoin( Ref * obj )
{
	MenuItemSprite* pMenuItem = (MenuItemSprite *)obj;
	int nId = pMenuItem->getTag();
	for (unsigned int i = 0; i < m_vector_activeLabel.size(); i++)
	{
		if (nId == m_vector_activeLabel.at(i)->m_activeLabel->id())
		{
			// ��ҵ� �����ĸ��İ�ť
			// ��Ծ�����ͣ�1.���2.�ǩ����3.������4.���뾺������5.���⡢6.�����䡢7.����ս��8.���ն����9.��BOSS
			int nActiveType = m_vector_activeLabel.at(i)->m_activeLabel->activetype();
			switch (nActiveType)
			{
			case 1:
				{
					int nActiveTypeId = m_vector_activeLabel.at(i)->m_activeLabel->activetypeid();

					// 12 �� ��������
					if (12 == nActiveTypeId)
					{
						activeToRewardMisson();
					}
					else
					{
						activeToMisson(nActiveTypeId);
					}

					this->closeAnim();

					//GameView::getInstance()->showAlertDialog("renwu");
				}
				break;
			case 2:
				{
					activeToSign();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("qiandao");
				}
				break;
			case 3:
				{
					activeToEctype();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("fuben");
				}
				break;
			case 4:
				{
					activeToPK();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("jingjichang");
				}
				break;
			case 5:
				{
					activeToAnswerQuestion();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("dati");
				}
				break;
			case 6:
				{
					activeToOpenPresentBox();
					//this->closeAnim();
					//GameView::getInstance()->showAlertDialog("kaibaoxiang");
				}
				break;
			case 7:
				{
					activeToFamilyFight();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("kaibaoxiang");
				}
				break;
			case 8:
				{
					activeToSingleFight();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("kaibaoxiang");
				}
				break;
			case 9:
				{
					// ���ն�� � ���BOSS���һ���߼�
					activeToWorldBoss();
					this->closeAnim();
					//GameView::getInstance()->showAlertDialog("kaibaoxiang");
				}
				break;

			}
		}
	}
}

void ActiveUI::openActiveWindow(UIScene * activeScene, int tag, int nActiveType)
{
	MainScene* mainScene = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	if(mainScene->getChildByTag(tag) != NULL)
	{
		const char * str_tishi1 = StringDataManager::getString("activy_tishi1");
		const char * str_tishi2 = StringDataManager::getString("activy_tishi2");

		switch (nActiveType)
		{
			case 1:
			{
				const char *str_fuben = StringDataManager::getString("activy_fuben");

				std::string str_info = "";
				str_info.append(str_tishi1);
				str_info.append(str_fuben);
				str_info.append(str_tishi2);

				GameView::getInstance()->showAlertDialog(str_info);
			}
			break;
		}

		return;
	}

	Size winSize = Director::getInstance()->getVisibleSize();
	activeScene->setIgnoreAnchorPointForPosition(false);
	activeScene->setAnchorPoint(Vec2(0.5f, 0.5f));
	activeScene->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
	activeScene->setTag(tag);

	mainScene->addChild(activeScene);

}

void ActiveUI::activeToSign()
{
	MainScene* mainScene = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	if(mainScene->getChildByTag(kTagSignDailyUI) != NULL)
	{
		mainScene->getChildByTag(kTagSignDailyUI)->removeFromParent();
	}

	Size winSize = Director::getInstance()->getVisibleSize();
	SignDailyUI * pSignDailyUI = SignDailyUI::create();
	pSignDailyUI->setIgnoreAnchorPointForPosition(false);
	pSignDailyUI->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSignDailyUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
	pSignDailyUI->setTag(kTagSignDailyUI);

	mainScene->addChild(pSignDailyUI);
	pSignDailyUI->refreshUI();
}

void ActiveUI::activeToEctype()
{
	// the player has already been in the instance map(��ɫ�Ѿ�� ڸ��� �)
	if(FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
	{
		const char * str_tishi1 = StringDataManager::getString("activy_tishi1");
		const char * str_tishi2 = StringDataManager::getString("activy_tishi2");
		const char *str_fuben = StringDataManager::getString("activy_fuben");

		std::string str_info = "";
		str_info.append(str_tishi1);
		str_info.append(str_fuben);
		str_info.append(str_tishi2);

		GameView::getInstance()->showAlertDialog(str_info);
	}
	else
	{
// 		MainScene* mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
// 		if(mainLayer->getChildByTag(kTagInstanceMainUI) != NULL)
// 		{
// 			mainLayer->getChildByTag(kTagInstanceMainUI)->removeFromParent();
// 		}
// 
// 		Size winSize = Director::getInstance()->getVisibleSize();
// 
// 		InstanceDetailUI * pDetailUI = InstanceDetailUI::create();
// 		pDetailUI->setIgnoreAnchorPointForPosition(false);
// 		pDetailUI->setAnchorPoint(Vec2(0.5f, 0.5f));
// 		pDetailUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
// 		pDetailUI->setTag(kTagSignDailyUI);
// 
// 		mainLayer->addChild(pDetailUI);

		MainScene* mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
		if(mainLayer->getChildByTag(kTagInstanceMapUI) != NULL)
		{
		 	mainLayer->getChildByTag(kTagInstanceMapUI)->removeFromParent();
		}
		 
		Size winSize = Director::getInstance()->getVisibleSize();
		 
		InstanceMapUI * iMapUI = InstanceMapUI::create();
		iMapUI->setIgnoreAnchorPointForPosition(false);
		iMapUI->setAnchorPoint(Vec2(0.5f, 0.5f));
		iMapUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		iMapUI->setTag(kTagInstanceMapUI);
		 
		mainLayer->addChild(iMapUI);
	}
}

void ActiveUI::activeToMisson(int nActiveTypeId)
{
	int nMissionCount = 0;
	int nIndex = -1;

	unsigned int nMissonSize = MissionManager::getInstance()->MissionList.size();
	std::vector<MissionInfo *> missonList = MissionManager::getInstance()->MissionList;
	for (unsigned int i = 0; i < nMissonSize; i++)
	{
		char str_tmpTypeId[4];
		sprintf(str_tmpTypeId, "%d", nActiveTypeId);
		std::string str_activeTypeId = "";
		str_activeTypeId.append(str_tmpTypeId);

		if (nActiveTypeId == missonList.at(i)->missionpackagetype())
		{
			nMissionCount++;
			nIndex = i;

			// ��� ���1ڣ�˵� �������ж���ѡ����򣬰�� �� �addMissionĽӿڣ�������ϵͳ��
			// ����ע�͵�˴����� �ž� ������д���ٴ
			/*if (nMissionCount > 1)
			{
			ActiveMissonManager::instance()->addAcitveMisson(missonList.at(i));

			return;
			}*/
		}
	}

	if (-1 != nIndex)
	{
		MissionManager::getInstance()->curMissionInfo->CopyFrom(*missonList.at(nIndex));
		MissionManager::getInstance()->addMission(missonList.at(nIndex));

		return;
	}
}

void ActiveUI::initLabelFromInternet()
{
	// ������
	std::vector<ActiveLabelData *>::iterator iterActiveLabel;
	for (iterActiveLabel = m_vector_activeLabel.begin(); iterActiveLabel != m_vector_activeLabel.end(); iterActiveLabel++)
	{
		delete *iterActiveLabel;
	}
	m_vector_activeLabel.clear();

	// �ʹm_nTableViewSize��СΪ0
	this->setTableViewSize(0);

	// ��ǰ�û��ȼ�
	int nPlayLevel = GameView::getInstance()->myplayer->getActiveRole()->level();

	ActiveRole * activeRole = GameView::getInstance()->myplayer->getActiveRole();
	long long nFactionId = activeRole->playerbaseinfo().factionid();

	unsigned int nActiveLabelSize = ActiveData::instance()->m_vector_native_activeLabel.size();
	for (unsigned int i = 0; i < nActiveLabelSize; i++)
	{
		// �ж������û�û�м��壬�򲻰 Ѽ����������ӵ�vector�
		if (!IsCanPushVector(i))
		{
			continue;
		}


		// �ֻ� ��û��ȼ� ���ڵ�� �Ԥ��ȼ�ʱ������ݲ�װ�vector
		if (nPlayLevel >= ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->levelforeshow())
		{
			this->setTableViewSize(this->getTableViewSize() + 1);

			auto data_ = new ActiveLabelData();
			data_->m_activeLabel = new CActiveLabel();
			data_->m_activeLabel->CopyFrom(*(ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel));
			m_vector_activeLabel.push_back(data_);

		}
	}
}

void ActiveUI::initNoteFromInternet()
{
	// ������
	std::vector<ActiveNoteData *>::iterator iterActiveNote;
	for (iterActiveNote = m_vector_activeNote.begin(); iterActiveNote != m_vector_activeNote.end(); iterActiveNote++)
	{

		delete *iterActiveNote;
	}
	m_vector_activeNote.clear();

	// ݵ�ǰ�û��ȼ�
	int nPlayLevel = GameView::getInstance()->myplayer->getActiveRole()->level();

	// ע������ȷ�� labelSize � noteSizeʹ�Сһ�
	unsigned int nActiveLabelSize = ActiveData::instance()->m_vector_native_activeLabel.size();
	unsigned int nActiveNoteSize = ActiveData::instance()->m_vector_native_activeNote.size();

	if (nActiveNoteSize == nActiveLabelSize)
	{
		for (unsigned int i = 0; i < nActiveNoteSize; i++)
		{
			// ��ж������û�û�м��壬�򲻰 Ѽ����������ӵ�vector�
			if (!IsCanPushVector(i))
			{
				continue;
			}

			// �ֻ� ��û��ȼ� ���ڵ�� �Ԥ��ȼ�ʱ������ݲ�װ�vector
			if (nPlayLevel >= ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->levelforeshow())
			{
				ActiveNoteData * data_ = new ActiveNoteData();
				data_->m_activeNote = new CActiveNote();
				data_->m_activeNote->CopyFrom(*ActiveData::instance()->m_vector_native_activeNote.at(i)->m_activeNote);
				m_vector_activeNote.push_back(data_);
			}
		}
	}
}

void ActiveUI::refrehTableView()
{
	m_tableView->reloadData();
}

void ActiveUI::initUserActiveFromInternet()
{
	// ��û��ܵĻ�Ծ�
	int nUserActive = ActiveData::instance()->getUserActive();
	char str_userActive[10];
	sprintf(str_userActive, "%d", nUserActive);

	m_label_activeAll = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_activityValue");
	m_label_activeAll->setString(str_userActive);
	m_label_activeAll->setVisible(false);

	if (NULL == m_labelTTF_activeAll)
	{
		auto pImageView_frame_details = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_details");

		m_labelTTF_activeAll = Label::createWithTTF(str_userActive, APP_FONT_NAME, 15, Size(120, 0), TextHAlignment::LEFT);
		m_labelTTF_activeAll->setAnchorPoint(Vec2(0, 0));
		m_labelTTF_activeAll->setIgnoreAnchorPointForPosition(false);
		m_labelTTF_activeAll->setPosition(Vec2(pImageView_frame_details->getPosition().x + 125, pImageView_frame_details->getPosition().y + 15));

		this->m_active_layer->addChild(m_labelTTF_activeAll);
	}
	else
	{
		m_labelTTF_activeAll->setString(str_userActive);
	}
}

std::string ActiveUI::getActiveJoinAll( int nJoinCountAll )
{
	std::string str_count = "";

	// ���nJoinCountAll�С�0ڣ�˵��Ϊ������������ƣ�����Ϊ��������ƴ��
	if (nJoinCountAll < 0)
	{
		const char * str_tmp = StringDataManager::getString("activy_none");
		str_count = str_tmp;
	}
	else
	{
		char str_joinCountAll[10];
		sprintf(str_joinCountAll, "%d", nJoinCountAll);
		str_count = str_joinCountAll;
	}

	return str_count;
}

std::string ActiveUI::getActiveCanGetAll( int nActiveCanGetAll )
{
	std::string str_active = "";

	// ���nActiveCanGetAll�С�0ڣ�˵��ɻ�õĻ�Ծ�������ƣ�����Ϊ��������ƴ��
	if (nActiveCanGetAll < 0)
	{
		const char * str_tmp = StringDataManager::getString("activy_none");
		str_active = str_tmp;
	}
	else
	{
		char str_activeCanGet[10];
		sprintf(str_activeCanGet, "%d", nActiveCanGetAll);
		str_active = str_activeCanGet;
	}

	return str_active;
}

void ActiveUI::setTableViewSize( int nTableViewSize )
{
	this->m_nTableViewSize = nTableViewSize;
}

int ActiveUI::getTableViewSize()
{
	return m_nTableViewSize;
}

void ActiveUI::callBackBtnAcitveShop(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveShop) == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			auto activeShopUI = ActiveShopUI::create(ActiveShopData::instance()->m_vector_activeShopSource);
			activeShopUI->setIgnoreAnchorPointForPosition(false);
			activeShopUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			activeShopUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			activeShopUI->setTag(kTagActiveShop);
			GameView::getInstance()->getMainUIScene()->addChild(activeShopUI);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void ActiveUI::activeToPK()
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI) == NULL)
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		auto offLineArenaUI = OffLineArenaUI::create();
		offLineArenaUI->setIgnoreAnchorPointForPosition(false);
		offLineArenaUI->setAnchorPoint(Vec2(0.5f, 0.5f));
		offLineArenaUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		offLineArenaUI->setTag(kTagOffLineArenaUI);
		GameView::getInstance()->getMainUIScene()->addChild(offLineArenaUI);
	}
}

void ActiveUI::activeToAnswerQuestion()
{
	if (this->mTutorialIndex == 1)
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(this);
	}

	if (QuestionData::instance()->get_hasAnswerQuestion() >= QuestionData::instance()->get_allQuestion())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("QuestionUI_hasNoNumber"));
	}
	else
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		auto pTmpQuestionUI = (QuestionUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
		if (NULL == pTmpQuestionUI)
		{
			auto pQuestionUI = QuestionUI::create();
			pQuestionUI->setIgnoreAnchorPointForPosition(false);
			pQuestionUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			pQuestionUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			pQuestionUI->setTag(kTagQuestionUI);
			pQuestionUI->requestQuestion();				// ��������������Ŀ

			GameView::getInstance()->getMainUIScene()->addChild(pQuestionUI);
		}
	}
}

void ActiveUI::tabIndexChangedEvent( Ref *pSender )
{
	switch(((UITab*)pSender)->getCurrentIndex())
	{
	case 0:
		{
			m_active_layer->setVisible(true);
			m_boss_layer->setVisible(false);
			m_challenge_layer->setVisible(false);

			s_panel_activeUI->setVisible(true);
			s_panel_worldBossUI->setVisible(false);
			s_panel_singCopyUI->setVisible(false);
		}
		break;
	case 1:
		{
			m_active_layer->setVisible(false);
			m_boss_layer->setVisible(true);
			m_challenge_layer->setVisible(false);

			s_panel_activeUI->setVisible(false);
			s_panel_worldBossUI->setVisible(true);
			s_panel_singCopyUI->setVisible(false);
		}
		break;
	case 2:
		{
// 			m_active_layer->setVisible(false);
// 			m_boss_layer->setVisible(false);
// 			m_challenge_layer->setVisible(true);
// 			
// 			s_panel_singCopyUI->setVisible(true);
// 			s_panel_activeUI->setVisible(false);
// 			s_panel_worldBossUI->setVisible(false);
			
		}break;
	}
}

void ActiveUI::reloatActiveInfoScrollView()
{
	if (m_vector_activeNote.empty())
	{
		return;
	}

	if (m_active_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		m_active_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	m_contentScrollView = cocos2d::extension::ScrollView::create();
	m_contentScrollView->setContentSize(SizeContentScrollView);
	m_contentScrollView->setIgnoreAnchorPointForPosition(false);
	m_contentScrollView->setTouchEnabled(true);
	m_contentScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
	m_contentScrollView->setAnchorPoint(Vec2(0,0));
	m_contentScrollView->setPosition(Vec2ContentScrollView);
	m_contentScrollView->setBounceable(true);
	m_contentScrollView->setClippingToBounds(true);
	m_contentScrollView->setTag(TAG_SCROLLVIEW);

	m_active_layer->addChild(m_contentScrollView);

	int nScrollHeight = 0;
	int nSpace = 2;

	auto color = Color4B::BLACK;

	// ���-齱�
	const char * str_gift = StringDataManager::getString("activy_gift");
	m_label_content_gift = Label::createWithTTF(str_gift, APP_FONT_NAME, 15, Size(60, 0), TextHAlignment::LEFT);
	m_label_content_gift->setColor(Color3B(255, 246, 0));
	m_label_content_gift->enableShadow(color, Size(1.0f, -1.0f), 1.0f);

	// ����-齱�-text
	std::string str_contentGift = m_curActiveNote->m_activeNote->notereward();
	m_curActiveNote->m_label_content_gift = Label::createWithTTF(str_contentGift.c_str(), APP_FONT_NAME, 15, Size(120, 0), TextHAlignment::LEFT);
	m_curActiveNote->m_label_content_gift->setColor(Color3B(255, 255, 255));
	m_curActiveNote->m_label_content_gift->enableShadow(color, Size(1.0f, -1.0f), 1.0f);

	int nZeroLineHeight = m_curActiveNote->m_label_content_gift->getContentSize().height + nSpace;
	int nZeroLabelHeight = m_label_content_gift->getContentSize().height + nSpace;

	// ����-����
	const char * str_limit = StringDataManager::getString("activy_limit");
	m_label_content_limit = Label::createWithTTF(str_limit, APP_FONT_NAME, 15, Size(60, 0), TextHAlignment::LEFT);
	m_label_content_limit->setColor(Color3B(255, 246, 0));
	m_label_content_limit->enableShadow(color, Size(1.0f, -1.0f), 1.0f);

	// ����-����-text
	std::string str_contentLimit = m_curActiveNote->m_activeNote->notelimit();
	m_curActiveNote->m_label_content_limit = Label::createWithTTF(str_contentLimit.c_str(), APP_FONT_NAME, 15, Size(120, 0), TextHAlignment::LEFT);
	m_curActiveNote->m_label_content_limit->setColor(Color3B(255, 255, 255));
	m_curActiveNote->m_label_content_limit->enableShadow(color, Size(1.0f, -1.0f), 1.0f);

	int nFirstLineHeight = nZeroLineHeight + m_curActiveNote->m_label_content_limit->getContentSize().height + nSpace;
	int nFirstLabelHeight = nZeroLineHeight + m_label_content_limit->getContentSize().height + nSpace;

	// ����-����
	const char * str_describe = StringDataManager::getString("activy_describe");
	m_label_content_describe = Label::createWithTTF(str_describe, APP_FONT_NAME, 15, Size(60, 0), TextHAlignment::LEFT);
	m_label_content_describe->setColor(Color3B(255, 246, 0));
	m_label_content_describe->enableShadow(color, Size(1.0f, -1.0f), 1.0f);

	// ����-����-text
	std::string str_contentDescribe = m_curActiveNote->m_activeNote->notecontent();
	m_curActiveNote->m_label_content_describe= Label::createWithTTF(str_contentDescribe.c_str(), APP_FONT_NAME, 15, Size(120, 0), TextHAlignment::LEFT);
	m_curActiveNote->m_label_content_describe->setColor(Color3B(255, 255, 255));
	m_curActiveNote->m_label_content_describe->enableShadow(color, Size(1.0f, -1.0f), 1.0f);

	int nSecondLineHeight = nFirstLineHeight + m_curActiveNote->m_label_content_describe->getContentSize().height + nSpace;
	int nSecondLabelHeight = nFirstLineHeight + m_label_content_describe->getContentSize().height + nSpace;

	nScrollHeight = nSecondLineHeight + nSpace;
	if (nScrollHeight > SizeContentScrollView.height)
	{
		m_contentScrollView->setContentSize(Size(SizeContentScrollView.width, nScrollHeight));
		m_contentScrollView->setContentOffset(Vec2(0, SizeContentScrollView.height - nScrollHeight));
	}
	else 
	{
		m_contentScrollView->setContentSize(SizeContentScrollView);
	}


	// gift
	m_label_content_gift->setAnchorPoint(Vec2(0, 1.0f));
	m_label_content_gift->setPosition(Vec2(12, m_contentScrollView->getContentSize().height - nZeroLabelHeight));

	m_curActiveNote->m_label_content_gift->setAnchorPoint(Vec2(0, 1.0f));
	m_curActiveNote->m_label_content_gift->setPosition(Vec2(m_label_content_gift->getPosition().x + m_label_content_gift->getContentSize().width - 15, m_contentScrollView->getContentSize().height - nZeroLineHeight));

	m_contentScrollView->addChild(m_label_content_gift);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_gift);

	// limit
	m_label_content_limit->setAnchorPoint(Vec2(0, 1.0f));
	m_label_content_limit->setPosition(Vec2(12, m_contentScrollView->getContentSize().height - nFirstLabelHeight));

	m_curActiveNote->m_label_content_limit->setAnchorPoint(Vec2(0, 1.0f));
	m_curActiveNote->m_label_content_limit->setPosition(Vec2(m_label_content_limit->getPosition().x + m_label_content_limit->getContentSize().width - 15, m_contentScrollView->getContentSize().height - nFirstLineHeight));

	m_contentScrollView->addChild(m_label_content_limit);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_limit);

	// describe
	m_label_content_describe->setAnchorPoint(Vec2(0, 1.0f));
	m_label_content_describe->setPosition(Vec2(12, m_contentScrollView->getContentSize().height - nSecondLabelHeight));

	m_curActiveNote->m_label_content_describe->setAnchorPoint(Vec2(0, 1.0f));
	m_curActiveNote->m_label_content_describe->setPosition(Vec2(m_label_content_describe->getPosition().x + m_label_content_describe->getContentSize().width - 15, m_contentScrollView->getContentSize().height - nSecondLineHeight));

	m_contentScrollView->addChild(m_label_content_describe);
	m_contentScrollView->addChild(m_curActiveNote->m_label_content_describe);

}

void ActiveUI::initActiveUIData()
{
	initLabelFromInternet();
	initNoteFromInternet();
	initUserActiveFromInternet();
	initTodayActiveFromInternet();


	// ���������ջ�Ծ����
	//ActiveTodayTableView::instance()->initDataFromInternet();

	if (!m_vector_activeNote.empty())
	{
		m_curActiveNote = m_vector_activeNote.at(0);
	}

	// ݸ��tableView���
	m_tableView->reloadData();
	// ����scrollView���
	this->reloatActiveInfoScrollView();
	// ¸��LeftTableView���
	//m_leftTableView->reloadData();
}

void ActiveUI::initChallengeUi()
{
// 	ChallengeRoundUi * challengeui = ChallengeRoundUi::create();
// 	challengeui->setIgnoreAnchorPointForPosition(false);
// 	challengeui->setAnchorPoint(Vec2(0,0));
// 	challengeui->setPosition(Vec2(75,40));
// 	challengeui->setTag(ktagChallengeRoundUi);
// 	m_challenge_layer->addChild(challengeui);
}

void ActiveUI::callBackGoodIcon(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		ImageView* pBtn = (ImageView *)pSender;
		// ݴ˴�� �����Ļ�Ծ����Ϊ tag
		int nTag = pBtn->getTag();

		int nSize = m_vector_labelToday.size();
		for (int i = 0; i < nSize; i++)
		{
			auto activeLabelToday = m_vector_labelToday.at(i);
			if (nTag == activeLabelToday->needactive())
			{

				// �жϵ�ǰ�����Ƿ����ȡ

				// ���ջ�Ծ�
				int nTodayActive = ActiveData::instance()->get_todayActive();
				if (nTodayActive >= nTag)
				{
					/** Ƚ�����Ʒ״̬ ��0.δ��ȡ���1.�����ȡ������*/
					int nStatus = activeLabelToday->status();
					if (0 == nStatus)
					{
						// ��������������ȡ���
						GameMessageProcessor::sharedMsgProcessor()->sendReq(5130, (void *)nTag);
					}
					else
					{
						GoodsInfo * pGoodsInfo = new GoodsInfo();
						pGoodsInfo->CopyFrom(activeLabelToday->goods(0));

						GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(pGoodsInfo, GameView::getInstance()->EquipListItem, 0);
						goodsItemInfoBase->setIgnoreAnchorPointForPosition(false);
						goodsItemInfoBase->setAnchorPoint(Vec2(0.5f, 0.5f));
						//goodsItemInfoBase->setPosition(Vec2(size.width / 2, size.height / 2));

						GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
					}
				}
				else
				{
					GoodsInfo * pGoodsInfo = new GoodsInfo();
					pGoodsInfo->CopyFrom(activeLabelToday->goods(0));

					GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(pGoodsInfo, GameView::getInstance()->EquipListItem, 0);
					goodsItemInfoBase->setIgnoreAnchorPointForPosition(false);
					goodsItemInfoBase->setAnchorPoint(Vec2(0.5f, 0.5f));
					//goodsItemInfoBase->setPosition(Vec2(size.width / 2, size.height / 2));

					GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
				}
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void ActiveUI::callBackActiveTodayBtn( Ref* obj )
{
	GameView::getInstance()->showAlertDialog("btn");
}

void ActiveUI::initTodayActiveFromInternet()
{
	// ���ջ�Ծ�
	int nTodayActive = ActiveData::instance()->get_todayActive();
	char str_todayActive[10];
	sprintf(str_todayActive, "%d", nTodayActive);

	/*m_labelBMF_activeToday->setText(str_todayActive);*/

	// Ƚ��ջ�Ծ�value����һ����������
	auto pLabelPhyBarEffect = PhysicalBarEffect::create(nTodayActive, 700, 3.5f, m_labelBMF_activeToday);
	this->addChild(pLabelPhyBarEffect);

	// �����
	std::vector<CActiveLabelToday *>::iterator iter;
	for (iter = m_vector_labelToday.begin(); iter != m_vector_labelToday.end(); iter++)
	{
		delete *iter;
	}
	m_vector_labelToday.clear();

	for (unsigned int i = 0; i < ActiveData::instance()->m_vector_internet_labelToday.size(); i++)
	{
		auto activeLabelToday = new CActiveLabelToday();
		activeLabelToday->CopyFrom(*ActiveData::instance()->m_vector_internet_labelToday.at(i));

		m_vector_labelToday.push_back(activeLabelToday);
	}

	refreshTodayAcitveData();
}

void ActiveUI::activeToOpenPresentBox()
{
	// ������������ȡ��Ʒ
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5015);
}

void ActiveUI::activeToFamilyFight()
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	auto familyLayer = (Layer*)mainscene->getChildByTag(kTagFamilyUI);
	if(familyLayer == NULL)
	{
		auto familyui =FamilyUI::create();
		familyui->setIgnoreAnchorPointForPosition(false);
		familyui->setAnchorPoint(Vec2(0.5f,0.5f));
		familyui->setPosition(Vec2(winSize.width/2,winSize.height/2));
		familyui->setTag(kTagFamilyUI);
		GameView::getInstance()->getMainUIScene()->addChild(familyui);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1500,NULL);

		mainscene->remindFamilyApply();

		// �򿪼���սUI
		familyui->openFamilyFightUI();
	}
}

void ActiveUI::activeToSingleFight()
{

	if (this->mTutorialIndex == 2)
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(this);
	}

	// ���ն���activeType�Ϊ8
	unsigned int nActiveLabelSize = ActiveData::instance()->m_vector_native_activeLabel.size();
	for (unsigned int i = 0; i < nActiveLabelSize; i++)
	{
		int nActiveType = ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->activetype();
		if (8 == nActiveType)
		{
			auto pTargetPosition = new CTargetPosition();
			pTargetPosition->CopyFrom(ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->npcpostion());

			auto pTargetNpc = new CTargetNpc();
			pTargetNpc->CopyFrom(pTargetPosition->targetnpc());

			long long targetNpcId = pTargetNpc->npcid();

			std::string targetMapId = pTargetPosition->mapid();
			int xPos = pTargetPosition->x();
			int yPos = pTargetPosition->y();

			int posX = GameView::getInstance()->getGameScene()->tileToPositionX(xPos);
			int posY = GameView::getInstance()->getGameScene()->tileToPositionY(yPos);

			Vec2 targetPos = Vec2(posX, posY);

			// �����Զ�Ѱ·
			// �ж��Ƿ�ӵ�mainUIScene
			ActiveManager * pActiveManager = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
			if (NULL == pActiveManager)
			{
				ActiveManager* activeManager = ActiveManager::create();
				activeManager->set_targetMapId(targetMapId);
				activeManager->set_targetPos(targetPos);
				activeManager->set_targetNpcId(targetNpcId);
				activeManager->setLayerUserful(true);
				activeManager->setTag(kTagActiveLayer);

				GameView::getInstance()->getMainUIScene()->addChild(activeManager);
			}
			else 
			{
				pActiveManager->set_targetMapId(targetMapId);
				pActiveManager->set_targetPos(targetPos);
				pActiveManager->set_targetNpcId(targetNpcId);
				pActiveManager->setLayerUserful(true);
			}

			// Ѱ·�߼�
			if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
			{
				//same map
				Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
				Vec2 cp_target = targetPos;
				if (cp_role.getDistance(cp_target) < 96)
				{
					return;
				}
				else
				{
					if (targetPos.x == 0 && targetPos.y == 0)
					{
						return;
					}

					MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
					// NOT reachable, so find the near one

					cmd->targetPosition = targetPos;
					cmd->method = MyPlayerCommandMove::method_searchpath;
					bool bSwitchCommandImmediately = true;
					if(GameView::getInstance()->myplayer->isAttacking())
						bSwitchCommandImmediately = false;
					GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
				}
			}
			else
			{
				// ����ͬһ��ͼ��ֱ�ӷɵ�Ŀ�ĵ
				ActiveManager::AcrossMapTransport(targetMapId, targetPos);

				// ��ж��Ƿ�ӵ�mainUIScene
				ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
				if (NULL == pActiveLayer)
				{
					ActiveManager* activeLayer = ActiveManager::create();
					activeLayer->set_transportFinish(false);
					activeLayer->setTag(kTagActiveLayer);

					GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
				}
				else 
				{
					pActiveLayer->set_transportFinish(false);
				}
			}

			CC_SAFE_DELETE(pTargetPosition);
			CC_SAFE_DELETE(pTargetNpc);
		}
		else 
		{
			continue;
		}
	}
}

bool ActiveUI::IsCanPushVector(int nId)
{
	bool bFlag = true;

	int nActiveTypeId = ActiveData::instance()->m_vector_native_activeLabel.at(nId)->m_activeLabel->activetypeid();
	int nActiveType = ActiveData::instance()->m_vector_native_activeLabel.at(nId)->m_activeLabel->activetype();
	if ((1 == nActiveType && 7 == nActiveTypeId) || (7 == nActiveType))
	{
		ActiveRole * activeRole = GameView::getInstance()->myplayer->getActiveRole();
		long long nFactionId = activeRole->playerbaseinfo().factionid();
		// ���0�˵��û�м��
		if (0 == nFactionId)
		{
			bFlag = false;
		}
		else
		{
			bFlag = true;
		}
	}

	return bFlag;
}

void ActiveUI::initTodayUI()
{
	// ���ջ�Ծ�
	m_labelBMF_activeToday = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_todayActive");

	m_btn_gift_frame_1 = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_1");
	m_btn_gift_frame_2 = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_2");
	m_btn_gift_frame_3 = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_3");
	m_btn_gift_frame_4 = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_4");
	m_btn_gift_frame_5 = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_5");

	m_imageView_gift_icon_1 = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_gift_icon_1");
	m_imageView_gift_icon_2 = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_gift_icon_2");
	m_imageView_gift_icon_3 = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_gift_icon_3");
	m_imageView_gift_icon_4 = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_gift_icon_4");
	m_imageView_gift_icon_5 = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_gift_icon_5");

	m_btn_gift_frame_1->setTouchEnabled(true);
	m_btn_gift_frame_2->setTouchEnabled(true);
	m_btn_gift_frame_3->setTouchEnabled(true);
	m_btn_gift_frame_4->setTouchEnabled(true);
	m_btn_gift_frame_5->setTouchEnabled(true);

	m_btn_gift_frame_1->setPressedActionEnabled(true);
	m_btn_gift_frame_2->setPressedActionEnabled(true);
	m_btn_gift_frame_3->setPressedActionEnabled(true);
	m_btn_gift_frame_4->setPressedActionEnabled(true);
	m_btn_gift_frame_5->setPressedActionEnabled(true);

	m_btn_gift_frame_1->setScale(0.8f);
	m_btn_gift_frame_2->setScale(0.8f);
	m_btn_gift_frame_3->setScale(0.8f);
	m_btn_gift_frame_4->setScale(0.8f);
	m_btn_gift_frame_5->setScale(0.8f);


	m_label_gift_num_1 = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_gift_num_1");
	m_label_gift_num_2 = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_gift_num_2");
	m_label_gift_num_3 = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_gift_num_3");
	m_label_gift_num_4 = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_gift_num_4");
	m_label_gift_num_5 = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_gift_num_5");

	m_labelBMF_gift_1 = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_gift_value_1");
	m_labelBMF_gift_2 = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_gift_value_2");
	m_labelBMF_gift_3 = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_gift_value_3");
	m_labelBMF_gift_4 = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_gift_value_4");
	m_labelBMF_gift_5 = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_gift_value_5");

	m_imageView_progress_1 = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_process_1");
	m_imageView_progress_2 = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_process_2");
	m_imageView_progress_3 = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_process_3");
	m_imageView_progress_4 = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_process_4");
	m_imageView_progress_5 = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_process_5");


}

void ActiveUI::refreshTodayAcitveData()
{
	int nSize = m_vector_labelToday.size();
	if (5 == nSize)
	{
		for (int i = 0; i < nSize; i++)
		{
			auto activeLabelToday = m_vector_labelToday.at(i);

			std::vector<GoodsInfo *> vector_goods;
			int nGoodSize = m_vector_labelToday.at(i)->goods_size();	
			for(int j = 0; j < nGoodSize; j ++)
			{
				auto pGoodsInfo = new GoodsInfo();
				pGoodsInfo->CopyFrom(m_vector_labelToday.at(i)->goods(j));

				vector_goods.push_back(pGoodsInfo);
			}

			// icon_frame
			if (0 == nGoodSize)
			{
				return ;
			}

			auto tmpGoodsInfo = vector_goods.at(0);

			std::string goods_icon = vector_goods.at(0)->icon();

			std::string goodsIconName = "res_ui/props_icon/";
			goodsIconName.append(goods_icon);
			goodsIconName.append(".png");

			// ȸ�goods�Ʒ��ѡ���ͼ
			int nQuality = vector_goods.at(0)->quality();
			std::string strQuality = getEquipmentQualityByIndex(nQuality);

			initGoodsFrame(i, strQuality, goodsIconName, activeLabelToday);
		}
	}
}

std::string ActiveUI::getEquipmentQualityByIndex( int quality )
{
	std::string frameColorPath = "res_ui/";
	switch(quality)
	{
	case 1:
		{
			frameColorPath.append("sdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("sdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("sdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("sdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("sdi_orange");
		}break;
	}
	frameColorPath.append(".png");
	return frameColorPath;
}

void ActiveUI::initGoodsFrame( int nIndex, std::string strFrame, std::string strGoodIconName, CActiveLabelToday* pActiveLabelToday)
{
	// good ���
	std::vector<int > vector_goodsNum;
	int nGoodNumSize = m_vector_labelToday.at(nIndex)->goodsnumber_size();
	for(int i = 0; i < nGoodNumSize; i ++)
	{
		vector_goodsNum.push_back(m_vector_labelToday.at(nIndex)->goodsnumber(i));
	}

	if (0 == nGoodNumSize)
	{
		return ;
	}

	// good's num
	int nNum = vector_goodsNum.at(0);

	char charGoodNum[20];
	sprintf(charGoodNum, "%d", nNum);

	// ���item�Ļ�Ծ�
	int nActiveItem = pActiveLabelToday->needactive();
	
	char charActiveItem[20];
	sprintf(charActiveItem, "%d", nActiveItem);

	// Ƚ��ջ�Ծ�
	int nTodayActive = ActiveData::instance()->get_todayActive();

	// ȼ������
	float progress_scale = (nTodayActive * 1.0f) / (nActiveItem * 1.0f);
	if (progress_scale > 1.0f)
	{
		progress_scale = 1.0f;
	}

	const float fActionTime = 0.5f;
	const float fActionTimeDelay = 0.55f;

	if (0 == nIndex)
	{
		m_btn_gift_frame_1->loadTextures(strFrame.c_str(), strFrame.c_str(), strFrame.c_str());
		m_btn_gift_frame_1->setTouchEnabled(true);
		m_btn_gift_frame_1->addTouchEventListener(CC_CALLBACK_2(ActiveUI::callBackGoodIcon,this));
		m_btn_gift_frame_1->setTag(pActiveLabelToday->needactive());

		m_imageView_gift_icon_1->loadTexture(strGoodIconName.c_str());
		m_imageView_gift_icon_1->setTouchEnabled(false);

		m_label_gift_num_1->setString(charGoodNum);

		m_labelBMF_gift_1->setString(charActiveItem);

		if (nTodayActive >= nActiveItem)
		{
			// ��жϽ����Ƿ���ȡ����û��ȡ����Ӷ���Ч�
			/** �����Ʒ״̬ ��0.δ��ȡ���1.�����ȡ������*/
			int nStatus = pActiveLabelToday->status();
			if (0 == nStatus)
			{
				m_imageView_gift_icon_1->runAction(initActionForGoods());
				this->addParticleForGift(nIndex + 1);
			}
			else
			{
				m_imageView_gift_icon_1->stopAllActions();
				m_imageView_gift_icon_1->setScale(0.8f);
				this->removeParticleForGift(nIndex + 1);
			}
		}

		// ϼ������
		float progress_scale = (nTodayActive * 1.0f) / (nActiveItem * 1.0f);
		if (progress_scale > 1.0f)
		{
			progress_scale = 1.0f;
		}

		//m_imageView_progress_1->setTextureRect(Rect(0, 0, m_imageView_progress_1->getContentSize().width * progress_scale, m_imageView_progress_1->getContentSize().height));

		m_imageView_progress_1->setTextureRect(Rect(0, 0, 
			m_imageView_progress_1->getContentSize().width * 0.0f,
			m_imageView_progress_1->getContentSize().height));

		auto pPhyBarEffect = PhysicalBarEffect::create(nTodayActive, nActiveItem, fActionTime, m_imageView_progress_1, NULL);
		this->addChild(pPhyBarEffect);
	}
	else if (1 == nIndex)
	{
		m_btn_gift_frame_2->loadTextures(strFrame.c_str(), strFrame.c_str(), strFrame.c_str());
		m_btn_gift_frame_2->setTouchEnabled(true);
		m_btn_gift_frame_2->addTouchEventListener(CC_CALLBACK_2(ActiveUI::callBackGoodIcon,this));
		m_btn_gift_frame_2->setTag(pActiveLabelToday->needactive());

		m_imageView_gift_icon_2->loadTexture(strGoodIconName.c_str());
		m_imageView_gift_icon_2->setTouchEnabled(false);

		m_label_gift_num_2->setString(charGoodNum);

		m_labelBMF_gift_2->setString(charActiveItem);

		int nPreActive = m_vector_labelToday.at(nIndex - 1)->needactive();

		if (nTodayActive >= nPreActive)
		{
			int nTrueActive = nActiveItem - nPreActive;

			int nTrueActiveOff = nTodayActive - nPreActive;

			// �������
			float progress_scale = (nTrueActiveOff * 1.0f) / (nTrueActive * 1.0f);
			if (progress_scale > 1.0f)
			{
				progress_scale = 1.0f;
			}

			//m_imageView_progress_2->setTextureRect(Rect(0, 0, m_imageView_progress_2->getContentSize().width * progress_scale, m_imageView_progress_2->getContentSize().height));

			m_imageView_progress_2->setTextureRect(Rect(0, 0, 
				m_imageView_progress_2->getContentSize().width * 0.0f,
				m_imageView_progress_2->getContentSize().height));

			auto pPhyBarEffect = PhysicalBarEffect::create(nTrueActiveOff, nTrueActive, fActionTime, m_imageView_progress_2, NULL, fActionTimeDelay);
			this->addChild(pPhyBarEffect);
		}
		else
		{
			m_imageView_progress_2->setTextureRect(Rect(0, 0, 0, m_imageView_progress_2->getContentSize().height));
		}

		if (nTodayActive >= nActiveItem)
		{
			// ��жϽ����Ƿ���ȡ����û��ȡ����Ӷ���Ч�
			/** �����Ʒ״̬ ��0.δ��ȡ���1.�����ȡ������*/
			int nStatus = pActiveLabelToday->status();
			if (0 == nStatus)
			{
				m_imageView_gift_icon_2->runAction(initActionForGoods());
				this->addParticleForGift(nIndex + 1);
			}
			else
			{
				m_imageView_gift_icon_2->stopAllActions();
				m_imageView_gift_icon_2->setScale(0.8f);
				this->removeParticleForGift(nIndex + 1);
			}
		}
	}
	else if (2 == nIndex)
	{
		m_btn_gift_frame_3->loadTextures(strFrame.c_str(), strFrame.c_str(), strFrame.c_str());
		m_btn_gift_frame_3->setTouchEnabled(true);
		m_btn_gift_frame_3->addTouchEventListener(CC_CALLBACK_2(ActiveUI::callBackGoodIcon,this));
		m_btn_gift_frame_3->setTag(pActiveLabelToday->needactive());

		m_imageView_gift_icon_3->loadTexture(strGoodIconName.c_str());
		m_imageView_gift_icon_3->setTouchEnabled(false);

		m_label_gift_num_3->setString(charGoodNum);

		m_labelBMF_gift_3->setString(charActiveItem);

		int nPreActive = m_vector_labelToday.at(nIndex - 1)->needactive();

		if (nTodayActive >= nPreActive)
		{
			int nTrueActive = nActiveItem - nPreActive;

			int nTrueActiveOff = nTodayActive - nPreActive;

			// ϼ������
			float progress_scale = (nTrueActiveOff * 1.0f) / (nTrueActive * 1.0f);
			if (progress_scale > 1.0f)
			{
				progress_scale = 1.0f;
			}

			//m_imageView_progress_3->setTextureRect(Rect(0, 0, m_imageView_progress_3->getContentSize().width * progress_scale, m_imageView_progress_3->getContentSize().height));

			m_imageView_progress_3->setTextureRect(Rect(0, 0, 
				m_imageView_progress_3->getContentSize().width * 0.0f,
				m_imageView_progress_3->getContentSize().height));

			auto pPhyBarEffect = PhysicalBarEffect::create(nTrueActiveOff, nTrueActive, fActionTime, m_imageView_progress_3, NULL, fActionTimeDelay * 2);
			this->addChild(pPhyBarEffect);
		}
		else
		{
			m_imageView_progress_3->setTextureRect(Rect(0, 0, 0, m_imageView_progress_3->getContentSize().height));
		}

		if (nTodayActive >= nActiveItem)
		{
			// ��жϽ����Ƿ���ȡ����û��ȡ����Ӷ���Ч�
			/** �����Ʒ״̬ ��0.δ��ȡ���1.�����ȡ������*/
			int nStatus = pActiveLabelToday->status();
			if (0 == nStatus)
			{
				m_imageView_gift_icon_3->runAction(initActionForGoods());
				this->addParticleForGift(nIndex + 1);
			}
			else
			{
				m_imageView_gift_icon_3->stopAllActions();
				m_imageView_gift_icon_3->setScale(0.8f);
				this->removeParticleForGift(nIndex + 1);
			}
		}
	}
	else if (3 == nIndex)
	{
		m_btn_gift_frame_4->loadTextures(strFrame.c_str(), strFrame.c_str(), strFrame.c_str());
		m_btn_gift_frame_4->setTouchEnabled(true);
		m_btn_gift_frame_4->addTouchEventListener(CC_CALLBACK_2(ActiveUI::callBackGoodIcon, this));
		m_btn_gift_frame_4->setTag(pActiveLabelToday->needactive());

		m_imageView_gift_icon_4->loadTexture(strGoodIconName.c_str());
		m_imageView_gift_icon_4->setTouchEnabled(false);


		m_label_gift_num_4->setString(charGoodNum);

		m_labelBMF_gift_4->setString(charActiveItem);

		int nPreActive = m_vector_labelToday.at(nIndex - 1)->needactive();

		if (nTodayActive >= nPreActive)
		{
			int nTrueActive = nActiveItem - nPreActive;

			int nTrueActiveOff = nTodayActive - nPreActive;

			// ϼ������
			float progress_scale = (nTrueActiveOff * 1.0f) / (nTrueActive * 1.0f);
			if (progress_scale > 1.0f)
			{
				progress_scale = 1.0f;
			}

			//m_imageView_progress_4->setTextureRect(Rect(0, 0, m_imageView_progress_4->getContentSize().width * progress_scale, m_imageView_progress_4->getContentSize().height));

			m_imageView_progress_4->setTextureRect(Rect(0, 0, 
				m_imageView_progress_4->getContentSize().width * 0.0f,
				m_imageView_progress_4->getContentSize().height));

			auto pPhyBarEffect = PhysicalBarEffect::create(nTrueActiveOff, nTrueActive, fActionTime, m_imageView_progress_4, NULL, fActionTimeDelay * 3);
			this->addChild(pPhyBarEffect);
		}
		else
		{
			m_imageView_progress_4->setTextureRect(Rect(0, 0, 0, m_imageView_progress_4->getContentSize().height));
		}

		if (nTodayActive >= nActiveItem)
		{
			// ��жϽ����Ƿ���ȡ����û��ȡ����Ӷ���Ч�
			/** �����Ʒ״̬ ��0.δ��ȡ���1.�����ȡ������*/
			int nStatus = pActiveLabelToday->status();
			if (0 == nStatus)
			{
				m_imageView_gift_icon_4->runAction(initActionForGoods());
				this->addParticleForGift(nIndex + 1);
			}
			else
			{
				m_imageView_gift_icon_4->stopAllActions();
				m_imageView_gift_icon_4->setScale(0.8f);
				this->removeParticleForGift(nIndex + 1);
			}
		}
	}
	else if (4 == nIndex)
	{
		m_btn_gift_frame_5->loadTextures(strFrame.c_str(), strFrame.c_str(), strFrame.c_str());
		m_btn_gift_frame_5->setTouchEnabled(true);
		m_btn_gift_frame_5->addTouchEventListener(CC_CALLBACK_2(ActiveUI::callBackGoodIcon,this));
		m_btn_gift_frame_5->setTag(pActiveLabelToday->needactive());

		m_imageView_gift_icon_5->loadTexture(strGoodIconName.c_str());
		m_imageView_gift_icon_5->setTouchEnabled(false);

		m_label_gift_num_5->setString(charGoodNum);

		m_labelBMF_gift_5->setString(charActiveItem);

		int nPreActive = m_vector_labelToday.at(nIndex - 1)->needactive();

		if (nTodayActive >= nPreActive)
		{
			int nTrueActive = nActiveItem - nPreActive;

			int nTrueActiveOff = nTodayActive - nPreActive;

			// ϼ������
			float progress_scale = (nTrueActiveOff * 1.0f) / (nTrueActive * 1.0f);
			if (progress_scale > 1.0f)
			{
				progress_scale = 1.0f;
			}

			//m_imageView_progress_5->setTextureRect(Rect(0, 0, m_imageView_progress_5->getContentSize().width * progress_scale, m_imageView_progress_5->getContentSize().height));

			m_imageView_progress_5->setTextureRect(Rect(0, 0, 
				m_imageView_progress_5->getContentSize().width * 0.0f,
				m_imageView_progress_5->getContentSize().height));

			auto pPhyBarEffect = PhysicalBarEffect::create(nTrueActiveOff, nTrueActive, fActionTime, m_imageView_progress_5, NULL, fActionTimeDelay * 4);
			this->addChild(pPhyBarEffect);
		}
		else
		{
			m_imageView_progress_5->setTextureRect(Rect(0, 0, 0, m_imageView_progress_5->getContentSize().height));
		}

		if (nTodayActive >= nActiveItem)
		{
			// ��жϽ����Ƿ���ȡ����û��ȡ����Ӷ���Ч�
			/** �����Ʒ״̬ ��0.δ��ȡ���1.�����ȡ������*/
			int nStatus = pActiveLabelToday->status();
			if (0 == nStatus)
			{
				m_imageView_gift_icon_5->runAction(initActionForGoods());
				this->addParticleForGift(nIndex + 1);
			}
			else
			{
				m_imageView_gift_icon_5->stopAllActions();
				m_imageView_gift_icon_5->setScale(0.8f);
				this->removeParticleForGift(nIndex + 1);
			}
		}
	}
}

ActionInterval * ActiveUI::initActionForGoods()
{
	// ϷŴ���С� �Ч�
	ScaleTo * pActionScaleToSmall = ScaleTo::create(0.75f, 0.8f);
	ScaleTo * pActionScaleToBig = ScaleTo::create(0.75f, 1.2f);
	Sequence * pActionSequence = Sequence::createWithTwoActions(pActionScaleToSmall, pActionScaleToBig);
	RepeatForever *pActionRepeat = RepeatForever::create(pActionSequence);

	return (ActionInterval*)pActionRepeat;
}

void ActiveUI::activeToWorldBoss()
{
	//// ���BOSS�activeType�Ϊ9
	//unsigned int nActiveLabelSize = ActiveData::instance()->m_vector_native_activeLabel.size();
	//for (unsigned int i = 0; i < nActiveLabelSize; i++)
	//{
	//	int nActiveType = ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->activetype();
	//	if (9 == nActiveType)
	//	{
	//		auto pTargetPosition = new CTargetPosition();
	//		pTargetPosition->CopyFrom(ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->npcpostion());

	//		auto pTargetNpc = new CTargetNpc();
	//		pTargetNpc->CopyFrom(pTargetPosition->targetnpc());

	//		long long targetNpcId = pTargetNpc->npcid();

	//		std::string targetMapId = pTargetPosition->mapid();
	//		int xPos = pTargetPosition->x();
	//		int yPos = pTargetPosition->y();

	//		int posX = GameView::getInstance()->getGameScene()->tileToPositionX(xPos);
	//		int posY = GameView::getInstance()->getGameScene()->tileToPositionY(yPos);

	//		Vec2 targetPos = Vec2Make(posX, posY);

	//		// �����Զ�Ѱ·
	//		// �ж��Ƿ�ӵ�mainUIScene
	//		ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
	//		if (NULL == pActiveLayer)
	//		{
	//			ActiveManager* activeLayer = ActiveManager::create();
	//			activeLayer->set_targetMapId(targetMapId);
	//			activeLayer->set_targetPos(targetPos);
	//			activeLayer->set_targetNpcId(targetNpcId);
	//			activeLayer->setLayerUserful(true);
	//			activeLayer->setTag(kTagActiveLayer);

	//			GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
	//		}
	//		else 
	//		{
	//			pActiveLayer->set_targetMapId(targetMapId);
	//			pActiveLayer->set_targetPos(targetPos);
	//			pActiveLayer->set_targetNpcId(targetNpcId);
	//			pActiveLayer->setLayerUserful(true);
	//		}

	//		// Ѱ·�߼�
	//		if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	//		{
	//			//same map
	//			Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
	//			Vec2 cp_target = targetPos;
	//			if (cp_role.getDistance(cp_target) < 64)
	//			{
	//				return;
	//			}
	//			else
	//			{
	//				if (targetPos.x == 0 && targetPos.y == 0)
	//				{
	//					return;
	//				}

	//				MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
	//				// NOT reachable, so find the near one

	//				cmd->targetPosition = targetPos;
	//				cmd->method = MyPlayerCommandMove::method_searchpath;
	//				bool bSwitchCommandImmediately = true;
	//				if(GameView::getInstance()->myplayer->isAttacking())
	//					bSwitchCommandImmediately = false;
	//				GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
	//			}
	//		}
	//		else
	//		{
	//			// ����ͬһ��ͼ��ֱ�ӷɵ�Ŀ�ĵ
	//			ActiveManager::AcrossMapTransport(targetMapId, targetPos);

	//			// ��ж��Ƿ�ӵ�mainUIScene
	//			ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
	//			if (NULL == pActiveLayer)
	//			{
	//				ActiveManager* activeLayer = ActiveManager::create();
	//				activeLayer->set_transportFinish(false);
	//				activeLayer->setTag(kTagActiveLayer);

	//				GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
	//			}
	//			else 
	//			{
	//				pActiveLayer->set_transportFinish(false);
	//			}
	//		}

	//		CC_SAFE_DELETE(pTargetPosition);
	//		CC_SAFE_DELETE(pTargetNpc);
	//	}
	//	else 
	//	{
	//		continue;
	//	}
	//}


	// �����߼�� �Ѱ·����BOSS npc紦��npc������BOSS�����
	// 񣩴˴��߼�� �ֱ�Ӵ���BOSS����
	Size winSize = Director::getInstance()->getVisibleSize();
	auto worldBossUI = (Layer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagWorldBossUI);
	if(worldBossUI == NULL)
	{
		worldBossUI = WorldBossUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(worldBossUI,0,kTagWorldBossUI);
		worldBossUI->setIgnoreAnchorPointForPosition(false);
		worldBossUI->setAnchorPoint(Vec2(0.5f, 0.5f));
		worldBossUI->setPosition(Vec2(winSize.width/2, winSize.height/2));

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5301);
	}
}

void ActiveUI::activeToRewardMisson()
{
	unsigned int nActiveLabelSize = ActiveData::instance()->m_vector_native_activeLabel.size();
	for (unsigned int i = 0; i < nActiveLabelSize; i++)
	{
		int nActiveType = ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->activetype();
		int nActiveTypeId = ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->activetypeid();
		if (1 == nActiveType && 12 == nActiveTypeId)
		{
			auto pTargetPosition = new CTargetPosition();
			pTargetPosition->CopyFrom(ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel->npcpostion());

			auto pTargetNpc = new CTargetNpc();
			pTargetNpc->CopyFrom(pTargetPosition->targetnpc());

			long long targetNpcId = pTargetNpc->npcid();

			std::string targetMapId = pTargetPosition->mapid();
			int xPos = pTargetPosition->x();
			int yPos = pTargetPosition->y();

			int posX = GameView::getInstance()->getGameScene()->tileToPositionX(xPos);
			int posY = GameView::getInstance()->getGameScene()->tileToPositionY(yPos);

			Vec2 targetPos = Vec2(posX, posY);

			// �����Զ�Ѱ·
			// �ж��Ƿ�ӵ�mainUIScene
			ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
			if (NULL == pActiveLayer)
			{
				ActiveManager* activeLayer = ActiveManager::create();
				activeLayer->set_targetMapId(targetMapId);
				activeLayer->set_targetPos(targetPos);
				activeLayer->set_targetNpcId(targetNpcId);
				activeLayer->setLayerUserful(true);
				activeLayer->setTag(kTagActiveLayer);

				GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
			}
			else 
			{
				pActiveLayer->set_targetMapId(targetMapId);
				pActiveLayer->set_targetPos(targetPos);
				pActiveLayer->set_targetNpcId(targetNpcId);
				pActiveLayer->setLayerUserful(true);
			}

			// Ѱ·�߼�
			if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
			{
				//same map
				Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
				Vec2 cp_target = targetPos;
				if (cp_role.getDistance(cp_target) < 64)
				{
					return;
				}
				else
				{
					if (targetPos.x == 0 && targetPos.y == 0)
					{
						return;
					}

					MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
					// NOT reachable, so find the near one

					cmd->targetPosition = targetPos;
					cmd->method = MyPlayerCommandMove::method_searchpath;
					bool bSwitchCommandImmediately = true;
					if(GameView::getInstance()->myplayer->isAttacking())
						bSwitchCommandImmediately = false;
					GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
				}
			}
			else
			{
				// ����ͬһ��ͼ��ֱ�ӷɵ�Ŀ�ĵ
				ActiveManager::AcrossMapTransport(targetMapId, targetPos);

				// ��ж��Ƿ�ӵ�mainUIScene
				ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
				if (NULL == pActiveLayer)
				{
					ActiveManager* activeLayer = ActiveManager::create();
					activeLayer->set_transportFinish(false);
					activeLayer->setTag(kTagActiveLayer);

					GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
				}
				else 
				{
					pActiveLayer->set_transportFinish(false);
				}
			}

			CC_SAFE_DELETE(pTargetPosition);
			CC_SAFE_DELETE(pTargetNpc);
		}
		else 
		{
			continue;
		}
	}
}

void ActiveUI::addParticleForGift( int nIndex )
{
	float width = m_btn_gift_frame_1->getContentSize().width * 0.8f;
	float height = m_btn_gift_frame_1->getContentSize().height * 0.8f;

	float xPos = m_btn_gift_frame_1->getPosition().x - width / 2.0f;
	float yPos = m_btn_gift_frame_1->getPosition().y - height / 2.0f;

	// ��Cocostudio ݶ�ȡ�json��ļ�һ���5и��
	int nGiftSize = 5;
	for (int i = 0; i < nGiftSize; i ++)
	{
		if(nIndex == i + 1)
		{
			// ����������Чtagֵ������ɾ��ʱʹ�
			int nParticleTag_1 = nIndex * 100 + 1;
			int nParticleTag_2 = nIndex * 100 + 2;

			// ��ж����Ч���Ƿ���ڣ����ڵĻ��򲻴���
			Node * pNode_1 = (Node*)this->m_active_layer->getChildByTag(nParticleTag_1);
			Node * pNode_2 = (Node*)this->m_active_layer->getChildByTag(nParticleTag_2);
			if (NULL != pNode_1 || NULL != pNode_2)
			{
				return;
			}

			float speed = 100.f;

			float nOff = 93.0f;

			std::string _path = "animation/texiao/particledesigner/";
			_path.append("tuowei0.plist");

			ParticleSystemQuad* particleEffect_1 = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
			particleEffect_1->setPosition(Vec2(xPos + i * nOff, yPos));
			//particleEffect_1->setStartSize(30);
			particleEffect_1->setPositionType(ParticleSystem::PositionType::FREE);
			particleEffect_1->setVisible(true);
			particleEffect_1->setScale(1.0f);
			//particleEffect_1->setLife(1.5f);
			particleEffect_1->setTag(nParticleTag_1);
			//particleEffect_1->setStartColor(color);
			//particleEffect_1->setEndColor(color);
			this->m_active_layer->addChild(particleEffect_1);

			Sequence * sequence_1 = Sequence::create(
				MoveTo::create(height*1.0f/speed,Vec2(xPos + i * nOff, yPos + height)),
				MoveTo::create(width*1.0f/speed,Vec2(xPos + i * nOff + width, yPos + height)),
				MoveTo::create(height*1.0f/speed,Vec2(xPos + i * nOff + width, yPos)),
				MoveTo::create(width*1.0f/speed,Vec2(xPos + i * nOff, yPos)),
				NULL
				);
			RepeatForever * repeatForeverAnm_1 = RepeatForever::create(sequence_1);
			particleEffect_1->runAction(repeatForeverAnm_1);


			ParticleSystemQuad* particleEffect_2 = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
			particleEffect_2->setPosition(Vec2(xPos + i * nOff + 2 + width,yPos + 2 + height));
			//particleEffect_2->setStartSize(30);
			particleEffect_2->setPositionType(ParticleSystem::PositionType::FREE);
			particleEffect_2->setVisible(true);
			particleEffect_2->setScale(1.0f);
			//particleEffect_2->setLife(1.5f);
			particleEffect_2->setTag(nParticleTag_2);
			//particleEffect_2->setStartColor(color);
			//particleEffect_2->setEndColor(color);
			this->m_active_layer->addChild(particleEffect_2);

			Sequence * sequence_2 = Sequence::create(
				MoveTo::create(height*1.0f/speed,Vec2(xPos + i * nOff + width, yPos)),
				MoveTo::create(width*1.0f/speed,Vec2(xPos + i * nOff, yPos)),
				MoveTo::create(height*1.0f/speed,Vec2(xPos + i * nOff, yPos  + height)),
				MoveTo::create(width*1.0f/speed,Vec2(xPos + i * nOff + width, yPos  + height)),
				NULL
				);
			RepeatForever * repeatForeverAnm_2 = RepeatForever::create(sequence_2);
			particleEffect_2->runAction(repeatForeverAnm_2);

			break;
		}
	}
}

void ActiveUI::removeParticleForGift( int nIndex )
{
	// ȡ�������Чtagֵ��ɾ��ʹ�
	int nParticleTag_1 = nIndex * 100 + 1;
	int nParticleTag_2 = nIndex * 100 + 2;

	Node * pNode_1 = (Node*)this->m_active_layer->getChildByTag(nParticleTag_1);
	if (NULL != pNode_1)
	{
		this->m_active_layer->removeChildByTag(nParticleTag_1);
	}

	Node * pNode_2 = (Node*)this->m_active_layer->getChildByTag(nParticleTag_2);
	if (NULL != pNode_2)
	{
		this->m_active_layer->removeChildByTag(nParticleTag_2);
	}
}




//void ActiveUI::AcrossMapTransport( std::string mapId,Vec2 pos )
//{
//	setStatus(ActiveUI::status_waitForTransport);
//	//req acrossMap
//	Vec2 * t_point = new Vec2(MissionManager::getInstance()->getNearestReachablePos(pos));
//	//Vec2 * t_point = new Vec2(pos);
//	GameMessageProcessor::sharedMsgProcessor()->sendReq(1114,(void *)mapId.c_str(),t_point);
//	delete t_point;
//}



void ActiveUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void ActiveUI::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction,int tutorialIndex )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,65,50);
// 	tutorialIndicator->setPosition(Vec2(pos.x+505,pos.y+350));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	m_up_layer->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,115,50,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+452+_w,pos.y+219+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}

	mTutorialIndex = tutorialIndex;
}

void ActiveUI::addCCTutorialIndicatorSingCopy( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction,int tutorialIndex )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,115,50,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+452+_w,pos.y+330+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}

	mTutorialIndex = tutorialIndex;
}


void ActiveUI::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(m_up_layer->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}

	mTutorialIndex = -1;
}
