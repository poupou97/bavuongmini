#include "GetRewardUI.h"
#include "WorldBossData.h"
#include "../../../messageclient/element/CBoss.h"
#include "../../../messageclient/element/CRankingPrize.h"
#include "AppMacros.h"
#include "../../../messageclient/element/GoodsInfo.h"
#include "../../../ui/vip_ui/VipRewardCellItem.h"
#include "../../../messageclient/element/CUnReceivePrize.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../utils/StaticDataManager.h"
#include "cocostudio\CCSGUIReader.h"

#define  SelectImageTag 458

GetRewardUI::GetRewardUI()
{
}


GetRewardUI::~GetRewardUI()
{
	std::vector<CUnReceivePrize*>::iterator _iter;
	for (_iter = m_unReceiveRewardList.begin(); _iter != m_unReceiveRewardList.end(); ++_iter)
	{
		delete *_iter;
	}
	m_unReceiveRewardList.clear();
}

GetRewardUI * GetRewardUI::create()
{
	auto getRewardUI = new GetRewardUI();
	if (getRewardUI && getRewardUI->init())
	{
		getRewardUI->autorelease();
		return getRewardUI;
	}
	CC_SAFE_DELETE(getRewardUI);
	return NULL;
}

bool GetRewardUI::init()
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		auto u_layer = Layer::create();
		addChild(u_layer);

		//���UI
		auto ppanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/worldboss_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(608, 417));
		ppanel->setPosition(Vec2::ZERO);	
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto btn_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnabled( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(GetRewardUI::CloseEvent, this));

		auto panel_rewardDes = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_introduction");
		panel_rewardDes->setVisible(false);
		auto panel_getReward = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_introduction_0");
		panel_getReward->setVisible(true);
		auto panel_ranking = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_ranking");
		panel_ranking->setVisible(false);

		m_tableView = TableView::create(this, Size(501,290));
		//m_tableView ->setSelectedEnable(true);   // �֧��ѡ��״̬����ʾ
		//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 25, 1, 1), Vec2(0,0));   // ���þŹ���ͼƬ
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0, 0));
		m_tableView->setPosition(Vec2(54,43));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//m_tableView->setPressedActionEnabled(false);
		u_layer->addChild(m_tableView);

		auto u_tableViewFrameLayer = Layer::create();
		this->addChild(u_tableViewFrameLayer);
		auto tableView_kuang = ImageView::create();
		tableView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
		tableView_kuang->setScale9Enabled(true);
		tableView_kuang->setContentSize(Size(515,303));
		tableView_kuang->setCapInsets(Rect(32,32,1,1));
		tableView_kuang->setAnchorPoint(Vec2(0.5f,0.5f));
		tableView_kuang->setPosition(Vec2(303,187));
		u_tableViewFrameLayer->addChild(tableView_kuang);

		this->setContentSize(Size(608,417));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		return true;
	}
	return false;
}

void GetRewardUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5302);
}

void GetRewardUI::onExit()
{
	UIScene::onExit();
}

bool GetRewardUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void GetRewardUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void GetRewardUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void GetRewardUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void GetRewardUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void GetRewardUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void GetRewardUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void GetRewardUI::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void GetRewardUI::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void GetRewardUI::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void GetRewardUI::tableCellTouched( TableView* table, TableViewCell* cell )
{

}

cocos2d::Size GetRewardUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(500,95);
}

cocos2d::extension::TableViewCell* GetRewardUI::tableCellAtIndex( TableView *table, ssize_t idx )
{
	TableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto unReceivePrize = WorldBossData::getInstance()->m_unReceiveRewardList.at(idx);
	Color3B color_green = Color3B(47,93,13);

	// ��ͼ
	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(0, 0));
	sprite_bigFrame->setTag(SelectImageTag);
	sprite_bigFrame->setCapInsets(Rect(9,14,1,1));
	sprite_bigFrame->setPreferredSize(Size(500,93));
	cell->addChild(sprite_bigFrame);

	auto l_boss_icon = Label::createWithBMFont("res_ui/font/ziti_3.fnt", "BOSS");
	l_boss_icon->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_boss_icon->setPosition(Vec2(40, 79));
	cell->addChild(l_boss_icon);
	//Name
	auto l_name = Label::createWithTTF(unReceivePrize->name().c_str(),APP_FONT_NAME,18);
	l_name->setAnchorPoint(Vec2(0.5f,0.5f));
	l_name->setPosition(Vec2(107,79));
	l_name->setColor(color_green);
	cell->addChild(l_name);
	//Level
	std::string str_lv = "LV";
	char s_lv[20];
	sprintf(s_lv,"%d",unReceivePrize->level());
	str_lv.append(s_lv);
	auto l_lv = Label::createWithTTF(str_lv.c_str(),APP_FONT_NAME,18);
	l_lv->setAnchorPoint(Vec2(0.5f,0.5f));
	l_lv->setPosition(Vec2(194,79));
	l_lv->setColor(color_green);
	cell->addChild(l_lv);
	//Date
	auto l_date = Label::createWithTTF(unReceivePrize->data().c_str(),APP_FONT_NAME,18);
	l_date->setAnchorPoint(Vec2(0.f,0.5f));
	l_date->setPosition(Vec2(263,79));
	l_date->setColor(color_green);
	cell->addChild(l_date);
	//Time
	auto l_time = Label::createWithTTF(unReceivePrize->time().c_str(),APP_FONT_NAME,18);
	l_time->setAnchorPoint(Vec2(0.f,0.5f));
	l_time->setPosition(Vec2(365,79));
	l_time->setColor(color_green);
	cell->addChild(l_time);

	//EXP
	auto sp_exp_icon = Sprite::create("res_ui/exp.png");
	sp_exp_icon->setAnchorPoint(Vec2(0.5f,0.5f));
	sp_exp_icon->setPosition(Vec2(40,47));
	cell->addChild(sp_exp_icon);
	char s_expValue[20];
	sprintf(s_expValue,"%d",unReceivePrize->prize().exp());
	auto l_expValue = Label::createWithTTF(s_expValue,APP_FONT_NAME,14);
	l_expValue->setAnchorPoint(Vec2(0.5f,0.5f));
	l_expValue->setPosition(Vec2(107,49));
	l_expValue->setColor(color_green);
	cell->addChild(l_expValue);

	//Gold
	auto sp_gold_icon = Sprite::create("res_ui/coins.png");
	sp_gold_icon->setAnchorPoint(Vec2(0.5f,0.5f));
	sp_gold_icon->setPosition(Vec2(40,22));
	cell->addChild(sp_gold_icon);
	char s_goldValue[20];
	sprintf(s_goldValue,"%d",unReceivePrize->prize().gold());
	auto l_goldValue = Label::createWithTTF(s_goldValue,APP_FONT_NAME,14);
	l_goldValue->setAnchorPoint(Vec2(0.5f,0.5f));
	l_goldValue->setPosition(Vec2(107,22));
	l_goldValue->setColor(color_green);
	cell->addChild(l_goldValue);

	for(int j = 0;j<unReceivePrize->prize().goods_size();++j)
	{
		if (j >= 3)
			continue;

		auto goodInfo = new GoodsInfo();
		goodInfo->CopyFrom(unReceivePrize->prize().goods(j).goods());
		int num = 0;
		num = unReceivePrize->prize().goods(j).quantity();

		auto goodsItem = VipRewardCellItem::create(goodInfo,num);
		goodsItem->setIgnoreAnchorPointForPosition(false);
		goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		goodsItem->setPosition(Vec2(195+79*j,36));
		cell->addChild(goodsItem);

		delete goodInfo;
	}

	auto btn_layer = Layer::create();
	cell->addChild(btn_layer);
	//btn_layer->setSwallowsTouches(false);

	auto btn_getReward = Button::create();
	btn_getReward->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
	btn_getReward->setScale9Enabled(true);
	btn_getReward->setContentSize(Size(103,42));
	btn_getReward->setCapInsets(Rect(18,9,2,23));
	btn_getReward->setTouchEnabled(true);
	btn_getReward->setPressedActionEnabled(true);
	btn_getReward->setTag(idx);
	btn_getReward->setAnchorPoint(Vec2(.5f,.5f));
	btn_getReward->setPosition(Vec2(445,36));
	btn_getReward->addTouchEventListener(CC_CALLBACK_2(GetRewardUI::getRewardEvent, this));
	btn_layer->addChild(btn_getReward);

	auto l_getReward = Label::createWithTTF(StringDataManager::getString("btn_lingqu"), APP_FONT_NAME, 20);
	l_getReward->setAnchorPoint(Vec2(.5f,.5f));
	l_getReward->setPosition(Vec2(0,0));
	btn_getReward->addChild(l_getReward);

	return cell;
}

ssize_t GetRewardUI::numberOfCellsInTableView( TableView *table )
{
	return WorldBossData::getInstance()->m_unReceiveRewardList.size();
}

void GetRewardUI::RefreshUI()
{
	m_tableView->reloadData();
}

void GetRewardUI::getRewardEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto temp = dynamic_cast<Button*>(pSender);
		if (!temp)
			return;

		int idx = temp->getTag();
		if (WorldBossData::getInstance()->m_unReceiveRewardList.size() > idx)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5303, (void *)WorldBossData::getInstance()->m_unReceiveRewardList.at(idx)->id());
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}



}
