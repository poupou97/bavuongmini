#ifndef _UI_ACTIVE_WORLDBOSSUI_WORLDBOSSDATA_H_
#define _UI_ACTIVE_WORLDBOSSUI_WORLDBOSSDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ����BosssUI
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.05.26
 */

class CBoss;
class CUnReceivePrize;

class WorldBossData
{
public:
	WorldBossData(void);
	~WorldBossData(void);

public: 
	static WorldBossData * s_bossData;
	static WorldBossData * getInstance();

	std::vector<CBoss*> m_worldBossDataList;

	//δ��ȡ�Ľ�����Ϣ
	std::vector<CUnReceivePrize*> m_unReceiveRewardList;

	//��ǰѡ�е�BosssIdx
	int m_nCurSelectBossIdx;

public: 
	void clearBossDataList();
	void clearUnReceiveRewardList();
};

#endif
