
#ifndef _UI_ACTIVE_WORLDBOSSUI_DMGRANKINGUI_H_
#define _UI_ACTIVE_WORLDBOSSUI_DMGRANKINGUI_H_

#include "../../extensions/UIScene.h"
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class DmgRankingUI : public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	DmgRankingUI();
	~DmgRankingUI();

	static DmgRankingUI* create(int bossIdx,int rankIdx);
	bool init(int bossIdx,int rankIdx);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);
private:
	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

private:
	int curBossIdx;
	int curRankIdx;

	int lastSelectCellId;
	int selectCellId;
};

#endif