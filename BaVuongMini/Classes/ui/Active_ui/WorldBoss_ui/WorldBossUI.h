#ifndef _UI_ACTIVE_WORLDBOSSUI_WORLDBOSSUI_H_
#define _UI_ACTIVE_WORLDBOSSUI_WORLDBOSSUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../extensions/UIScene.h"
#include "KillNotesList.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ��BosssUI
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.05.26
 */

class KillNotesList;
class CCRichLabel;

class WorldBossUI:public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	WorldBossUI(void);
	~WorldBossUI(void);

public:
	static WorldBossUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//紦������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);
private:
	Layer * m_base_layer;																							// ��layer
	TableView * m_tableView;																						// ��tableView
	cocos2d::extension::ScrollView * m_contentScrollView;																		// ����ScrollView
	CCRichLabel * l_des;

	int m_nTableViewSize;

	Text * l_name;
	Text * l_lv;
	Text * l_state;
	Text * l_pos;

	Button * btn_challenge;
	ImageView * imageView_wait;

private:
	//int curSelectBossIdx;
	KillNotesList * s_killNotesList;
private:
	void initUI();

public:
	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void getRewardEvent(Ref *pSender, Widget::TouchEventType type);
	void rewardDesEvent(Ref *pSender, Widget::TouchEventType type);
	void challengeEvent(Ref *pSender, Widget::TouchEventType type);

	void tabIndexChangedEvent(Ref *pSender);

	void RefreshTableView();
	void RefreshBossInfo(int idx);

	void RefreshDesScrollView(int idx);

	void setToDefault();
};

#endif

