#include "WorldBossUI.h"
#include "../ActiveUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../../GameAudio.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../AppMacros.h"
#include "GameView.h"
#include "../../extensions/CCMoveableMenu.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../utils/GameUtils.h"
#include "../../../messageclient/element/CBoss.h"
#include "WorldBossData.h"
#include "RewardDesUI.h"
#include "GetRewardUI.h"
#include "../../extensions/CCRichLabel.h"
#include "KillNotesList.h"
#include "../../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../../messageclient/element/CMapInfo.h"
#include "../../extensions/UITab.h"
#include "cocostudio\CCSGUIReader.h"

#define  TAG_CONTENTSSCROLLVIEW 50
#define  SelectImageTag 458

WorldBossUI::WorldBossUI(void)
{
	WorldBossData::getInstance()->m_nCurSelectBossIdx = -1;
}


WorldBossUI::~WorldBossUI(void)
{
}

WorldBossUI* WorldBossUI::create()
{
	auto worldBossUI = new WorldBossUI();
	if (worldBossUI && worldBossUI->init())
	{
		worldBossUI->autorelease();
		return worldBossUI;
	}
	CC_SAFE_DELETE(worldBossUI);
	return NULL;
}

bool WorldBossUI::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		m_base_layer = Layer::create();
		m_base_layer->setIgnoreAnchorPointForPosition(false);
		m_base_layer->setAnchorPoint(Vec2(.5f,.5f));
		m_base_layer->setPosition(Vec2(winSize.width/2,winSize.height/2));
		m_base_layer->setContentSize(Size(800,480));
		this->addChild(m_base_layer);	

		initUI();

// 		//this->setTouchEnabled(false);
// 		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
// 		this->setContentSize(winSize);
		return true;
	}
	return false;
}

void WorldBossUI::onEnter()
{
	UIScene::onEnter();
}

void WorldBossUI::onExit()
{
	UIScene::onExit();
}

void WorldBossUI::initUI()
{
	Size winSize = Director::getInstance()->getVisibleSize();

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto mengban = ImageView::create();
	mengban->loadTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enabled(true);
	mengban->setContentSize(winSize);
	mengban->setAnchorPoint(Vec2::ZERO);
	mengban->setPosition(Vec2(0,0));
	m_pLayer->addChild(mengban);

	auto s_panel_worldBossUI = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/boss_1.json");
	s_panel_worldBossUI->setAnchorPoint(Vec2(0.5f, 0.5f));
	s_panel_worldBossUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
	s_panel_worldBossUI->setScale(1.0f);
	s_panel_worldBossUI->setTouchEnabled(true);
	m_pLayer->addChild(s_panel_worldBossUI);

	auto btn_close = (Button*)Helper::seekWidgetByName(s_panel_worldBossUI,"Button_close");
	btn_close->setTouchEnabled(true);
	btn_close->setPressedActionEnabled(true);
	btn_close->addTouchEventListener(CC_CALLBACK_2(WorldBossUI::CloseEvent,this));

	auto btn_getReward = (Button*)Helper::seekWidgetByName(s_panel_worldBossUI,"Button_getReward");
	btn_getReward->setTouchEnabled(true);
	btn_getReward->setPressedActionEnabled(true);
	btn_getReward->addTouchEventListener(CC_CALLBACK_2(WorldBossUI::getRewardEvent, this));

	auto btn_rewardDes = (Button*)Helper::seekWidgetByName(s_panel_worldBossUI,"Button_introduction");
	btn_rewardDes->setTouchEnabled(true);
	btn_rewardDes->setPressedActionEnabled(true);
	btn_rewardDes->addTouchEventListener(CC_CALLBACK_2(WorldBossUI::rewardDesEvent,this));

	btn_challenge = (Button*)Helper::seekWidgetByName(s_panel_worldBossUI,"Button_challenge");
	btn_challenge->setTouchEnabled(true);
	btn_challenge->setPressedActionEnabled(true);
	btn_challenge->addTouchEventListener(CC_CALLBACK_2(WorldBossUI::challengeEvent,this));
	btn_challenge->setVisible(false);

	imageView_wait = (ImageView*)Helper::seekWidgetByName(s_panel_worldBossUI,"ImageView_waitForRefresh");
	imageView_wait->setVisible(false);

	l_name = (Text*)Helper::seekWidgetByName(s_panel_worldBossUI,"Label_bossNameValue");
	l_lv = (Text*)Helper::seekWidgetByName(s_panel_worldBossUI,"Label_bossLvValue");
	l_state = (Text*)Helper::seekWidgetByName(s_panel_worldBossUI,"Label_bossValue");
	l_pos = (Text*)Helper::seekWidgetByName(s_panel_worldBossUI,"Label_bossPlaceValue");

	// tab
// 	const char * normalImage = "res_ui/tab_3_off.png";
// 	const char * selectImage = "res_ui/tab_3_on.png";
// 	const char * finalImage = "";
// 	const char * highLightImage = "res_ui/tab_3_on.png";
// 	const char *str1 = StringDataManager::getString("WorldBossUI_uitab_boss");
// 	char* p1 = const_cast<char*>(str1);
// 	char* pHightLightImage = const_cast<char*>(highLightImage);
// 	char * mainNames[] = {p1};
// 	UITab *m_tab = UITab::createWithBMFont(1, normalImage, selectImage, finalImage, mainNames,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
// 	m_tab->setAnchorPoint(Vec2(0,0));
// 	m_tab->setPosition(Vec2(74, 410));
// 	m_tab->setHighLightImage(pHightLightImage);
// 	m_tab->setDefaultPanelByIndex(0);
// 	m_tab->addIndexChangedEvent(this, coco_indexchangedselector(WorldBossUI::tabIndexChangedEvent));
// 	m_tab->setPressedActionEnabled(true);
// 	m_base_layer->addChild(m_tab);
	
	// �tableView
	m_tableView = TableView::create(this, Size(245,289));
	//m_tableView ->setSelectedEnable(true);   // �֧��ѡ��״̬����ʾ
	//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 25, 1, 1), Vec2(0,0));   // ���þŹ���ͼƬ
	m_tableView->setDirection(TableView::Direction::VERTICAL);
	m_tableView->setAnchorPoint(Vec2(0, 0));
	m_tableView->setPosition(Vec2(71,118));
	m_tableView->setDelegate(this);
	m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//m_tableView->setPressedActionEnabled(false);
	m_base_layer->addChild(m_tableView);

	int scroll_height = 0;
	//���
	l_des = CCRichLabel::createWithString("",Size(387,0),this,0,0,18);
	m_contentScrollView = cocos2d::extension::ScrollView::create();
	m_contentScrollView->setContentSize(Size(389, 114));
	m_contentScrollView->setIgnoreAnchorPointForPosition(false);
	m_contentScrollView->setTouchEnabled(true);
	m_contentScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
	m_contentScrollView->setAnchorPoint(Vec2(0,0));
	m_contentScrollView->setPosition(Vec2(350,223));
	m_contentScrollView->setBounceable(true);
	m_contentScrollView->setClippingToBounds(true);
	m_base_layer->addChild(m_contentScrollView);
	m_contentScrollView->addChild(l_des);

	s_killNotesList = KillNotesList::create();
	m_base_layer->addChild(s_killNotesList);

	if (WorldBossData::getInstance()->m_worldBossDataList.size()>0)
	{
		WorldBossData::getInstance()->m_nCurSelectBossIdx = 0;
		RefreshBossInfo(0);
		RefreshDesScrollView(0);
		s_killNotesList->refreshDataSourceByIdx(0);
		s_killNotesList->refreshUI();
	}

	////this->setTouchEnabled(false);
	////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
	this->setContentSize(winSize);
}


bool WorldBossUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return false;
}

void WorldBossUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void WorldBossUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void WorldBossUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void WorldBossUI::scrollViewDidScroll( cocos2d::extension::ScrollView* view )
{

}

void WorldBossUI::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

void WorldBossUI::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void WorldBossUI::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void WorldBossUI::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void WorldBossUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	WorldBossData::getInstance()->m_nCurSelectBossIdx = cell->getIdx();
	RefreshBossInfo(WorldBossData::getInstance()->m_nCurSelectBossIdx);
	RefreshDesScrollView(WorldBossData::getInstance()->m_nCurSelectBossIdx);
	s_killNotesList->refreshDataSourceByIdx(WorldBossData::getInstance()->m_nCurSelectBossIdx);
	s_killNotesList->refreshUI();
}

cocos2d::Size WorldBossUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(238,53);
}

cocos2d::extension::TableViewCell* WorldBossUI::tableCellAtIndex( TableView *table, ssize_t idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	auto bossData =	WorldBossData::getInstance()->m_worldBossDataList.at(idx);

	// ܵ�ͼ
	auto sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
	sprite_frame->setContentSize(Size(233, 51));
	sprite_frame->setCapInsets(Rect(18, 9, 2, 23));
	sprite_frame->setAnchorPoint(Vec2::ZERO);
	sprite_frame->setPosition(Vec2(2, 0));
	cell->addChild(sprite_frame);

	// ��
	auto l_name = Label::createWithTTF(bossData->name().c_str(), APP_FONT_NAME, 18, Size(200, 0), TextHAlignment::LEFT);
	l_name->setColor(Color3B(255, 255, 255));
	auto color = Color4B::BLACK;
	l_name->enableShadow(color,Size(1.0f, -1.0f), 1.0f);
	l_name->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_name->setHorizontalAlignment(TextHAlignment::CENTER);
	l_name->setPosition(Vec2(61, 26));
	cell->addChild(l_name);

	// Ƶȼ�
	char str_lv[10];
	//test
	//sprintf(str_lv, "%d", 20);
	sprintf(str_lv, "%d", bossData->level());
	auto l_lv = Label::createWithTTF(str_lv, APP_FONT_NAME, 18, Size(200, 0), TextHAlignment::LEFT);
	l_lv->setColor(Color3B(255, 255, 255));
	l_lv->enableShadow(color, Size(1.0f, -1.0f), 1.0f);
	l_lv->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_lv->setHorizontalAlignment(TextHAlignment::CENTER);
	l_lv->setPosition(Vec2(140, 26));
	cell->addChild(l_lv);

	//״̬
	auto l_status = Label::createWithTTF("", APP_FONT_NAME, 18, Size(200,0), TextHAlignment::LEFT);
	l_status->setColor(Color3B(255, 255, 255));
	l_status->enableShadow(color, Size(1.0f, -1.0f), 1.0f);
	l_status->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_status->setHorizontalAlignment(TextHAlignment::CENTER);
	l_status->setPosition(Vec2(200, 26));
	cell->addChild(l_status);

	switch(bossData->state())
	{
	case DEAD:
		{
			l_status->setString(StringDataManager::getString("WorldBossUI_State_dead"));
		}
		break;
	case ALIVE:
		{
			l_status->setString(StringDataManager::getString("WorldBossUI_State_alive"));
		}
		break;
	}

	return cell;
}

ssize_t WorldBossUI::numberOfCellsInTableView( TableView *table )
{
	return WorldBossData::getInstance()->m_worldBossDataList.size();
}

void WorldBossUI::getRewardEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGetRewardUI) == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			//GetRewardUI * getRewardUI = GetRewardUI::create(WorldBossData::getInstance()->m_nCurSelectBossIdx);
			GetRewardUI * getRewardUI = GetRewardUI::create();
			getRewardUI->setIgnoreAnchorPointForPosition(false);
			getRewardUI->setAnchorPoint(Vec2(.5f, .5f));
			getRewardUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			getRewardUI->setTag(kTagGetRewardUI);
			GameView::getInstance()->getMainUIScene()->addChild(getRewardUI);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void WorldBossUI::rewardDesEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (WorldBossData::getInstance()->m_nCurSelectBossIdx < 0)
			return;

		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardDesUI) == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			auto rewardDesUI = RewardDesUI::create(WorldBossData::getInstance()->m_nCurSelectBossIdx);
			rewardDesUI->setIgnoreAnchorPointForPosition(false);
			rewardDesUI->setAnchorPoint(Vec2(.5f, .5f));
			rewardDesUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			rewardDesUI->setTag(kTagRewardDesUI);
			GameView::getInstance()->getMainUIScene()->addChild(rewardDesUI);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void WorldBossUI::challengeEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (WorldBossData::getInstance()->m_nCurSelectBossIdx < 0)
			return;

		auto tempData = WorldBossData::getInstance()->m_worldBossDataList.at(WorldBossData::getInstance()->m_nCurSelectBossIdx);

		Vec2 targetPos = Vec2(tempData->pos().x(), tempData->pos().y());
		std::string targetMapId = tempData->pos().mapid();
		// 	Vec2 targetPos;
		// 	if (pos.x == 0 && pos.y == 0)
		// 	{
		// 		targetPos = Vec2(0,0);
		// 	}
		// 	else
		// 	{
		// 		float x = GameView::getInstance()->getGameScene()->tileToPositionX(pos.x);
		// 		float y = GameView::getInstance()->getGameScene()->tileToPositionY(pos.y);
		// 		targetPos = Vec2(x,y);
		// 	}


		if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(), targetMapId.c_str()) == 0)
		{
			//same map
			Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
			Vec2 cp_target = targetPos;
			if (cp_role.getDistance(cp_target)< 64)
			{
				return;
			}
			else
			{
				if (targetPos.x == 0 && targetPos.y == 0)
				{
					return;
				}

				auto cmd = new MyPlayerCommandMove();
				// NOT reachable, so find the near one
				short tileX = GameView::getInstance()->getGameScene()->positionToTileX(targetPos.x);
				short tileY = GameView::getInstance()->getGameScene()->positionToTileY(targetPos.y);
				cmd->targetPosition = targetPos;
				cmd->method = MyPlayerCommandMove::method_searchpath;
				bool bSwitchCommandImmediately = true;
				if (GameView::getInstance()->myplayer->isAttacking())
					bSwitchCommandImmediately = false;
				GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);
			}
		}
		else
		{
			auto cmd = new MyPlayerCommandMove();
			cmd->targetMapId = targetMapId;
			if (targetPos.x == 0 && targetPos.y == 0)
			{
			}
			else
			{
				cmd->targetPosition = targetPos;
			}
			cmd->method = MyPlayerCommandMove::method_searchpath;
			bool bSwitchCommandImmediately = true;
			if (GameView::getInstance()->myplayer->isAttacking())
				bSwitchCommandImmediately = false;
			GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);
		}

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void WorldBossUI::RefreshTableView()
{
	this->m_tableView->reloadData();
	this->setToDefault();
}

void WorldBossUI::RefreshBossInfo( int idx )
{
	auto tempData = WorldBossData::getInstance()->m_worldBossDataList.at(idx);

	l_name->setString(tempData->name().c_str());
	std::string str_lv = "LV";
	char s_lv[10];
	sprintf(s_lv,"%d",tempData->level());
	str_lv.append(s_lv);
	l_lv->setString(str_lv.c_str());

	switch(tempData->state())
	{
		case ALIVE:
			{
				l_state->setString(StringDataManager::getString("WorldBossUI_State_alive"));
				
				int countryId = tempData->country();
				if(countryId == 0)   // common map
				{
					l_pos->setString(tempData->pos().mapname().c_str());
				}
				else
				{
					std::string full_map_name = CMapInfo::getCountryName(countryId);
					full_map_name.append(StringDataManager::getString("country_separator"));
					full_map_name.append(tempData->pos().mapname().c_str());
					l_pos->setString(full_map_name.c_str());
				}

				btn_challenge->setVisible(true);
				imageView_wait->setVisible(false);
			}
			break;
		case DEAD:
			{
				l_state->setString(StringDataManager::getString("WorldBossUI_State_dead"));
				l_pos->setString(StringDataManager::getString("activy_none"));
				btn_challenge->setVisible(false);
				imageView_wait->setVisible(true);
			}
			break;
	}
	
	
}

void WorldBossUI::RefreshDesScrollView( int idx )
{
	if (idx >= WorldBossData::getInstance()->m_worldBossDataList.size())
		return;

	auto tempData = WorldBossData::getInstance()->m_worldBossDataList.at(idx);
	
	l_des->setString(tempData->descr().c_str());
	int zeroLineHeight = l_des->getContentSize().height+10;
	int scroll_height = zeroLineHeight;
	 
	if (scroll_height > 114)
	{
	 	m_contentScrollView->setContentSize(Size(389,scroll_height));
	 	m_contentScrollView->setContentOffset(Vec2(0,114-scroll_height));  
	}
	else
	{
	 	m_contentScrollView->setContentSize(Size(389,114));
	}
	l_des->setPosition(Vec2(2,m_contentScrollView->getContentSize().height - zeroLineHeight)); 	
}

void WorldBossUI::setToDefault()
{
	if (WorldBossData::getInstance()->m_worldBossDataList.size() > 0)
	{
		m_tableView->cellAtIndex(0);
		RefreshBossInfo(0);
		RefreshDesScrollView(0);
	}
}

void WorldBossUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void WorldBossUI::tabIndexChangedEvent( Ref *pSender )
{
/*	this->setToDefault();*/
}
