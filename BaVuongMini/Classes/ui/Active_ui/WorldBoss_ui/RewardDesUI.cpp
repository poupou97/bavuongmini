#include "RewardDesUI.h"
#include "WorldBossData.h"
#include "../../../messageclient/element/CBoss.h"
#include "../../../messageclient/element/CRankingPrize.h"
#include "AppMacros.h"
#include "../../../messageclient/element/GoodsInfo.h"
#include "../../../ui/vip_ui/VipRewardCellItem.h"
#include "cocostudio\CCSGUIReader.h"

RewardDesUI::RewardDesUI()
{
}


RewardDesUI::~RewardDesUI()
{
	std::vector<CRankingPrize*>::iterator _iter;
	for (_iter = m_rewardDesList.begin(); _iter != m_rewardDesList.end(); ++_iter)
	{
		delete *_iter;
	}
	m_rewardDesList.clear();
}

RewardDesUI * RewardDesUI::create(int idx)
{
	auto rewardDesUI = new RewardDesUI();
	if (rewardDesUI && rewardDesUI->init(idx))
	{
		rewardDesUI->autorelease();
		return rewardDesUI;
	}
	CC_SAFE_DELETE(rewardDesUI);
	return NULL;
}

bool RewardDesUI::init(int idx)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		auto u_layer = Layer::create();
		addChild(u_layer);

		//���UI
		auto ppanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/worldboss_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(608, 417));
		ppanel->setPosition(Vec2::ZERO);	
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto btn_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnabled( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(RewardDesUI::CloseEvent, this));

		auto panel_rewardDes = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_introduction");
		panel_rewardDes->setVisible(true);
		auto panel_getReward = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_introduction_0");
		panel_getReward->setVisible(false);
		auto panel_ranking = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_ranking");
		panel_ranking->setVisible(false);

		auto l_name = (Text*)Helper::seekWidgetByName(panel_rewardDes,"Label_bossNameValue_1");
		l_name->setString(WorldBossData::getInstance()->m_worldBossDataList.at(idx)->name().c_str());
		auto l_lv = (Text*)Helper::seekWidgetByName(panel_rewardDes,"Label_bossLvValue_1");
		std::string str_lv = "LV";
		char s_lv[10];
		sprintf(s_lv,"%d",WorldBossData::getInstance()->m_worldBossDataList.at(idx)->level());
		str_lv.append(s_lv);
		l_lv->setString(str_lv.c_str());

		for (int i = 0;i<WorldBossData::getInstance()->m_worldBossDataList.at(idx)->prizes_size();++i)
		{
			auto temp = new CRankingPrize();
			temp->CopyFrom(WorldBossData::getInstance()->m_worldBossDataList.at(idx)->prizes(i));
			m_rewardDesList.push_back(temp);
		}

		auto m_tableView = TableView::create(this, Size(501,225));
		//m_tableView ->setSelectedEnable(true);   // �֧��ѡ��״̬����ʾ
		//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 25, 1, 1), Vec2(0,0));   // ���þŹ���ͼƬ
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0, 0));
		m_tableView->setPosition(Vec2(53,42));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//m_tableView->setPressedActionEnabled(false);
		u_layer->addChild(m_tableView);

		this->setContentSize(Size(608,417));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		return true;
	}
	return false;
}

void RewardDesUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RewardDesUI::onExit()
{
	UIScene::onExit();
}

bool RewardDesUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void RewardDesUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void RewardDesUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void RewardDesUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void RewardDesUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void RewardDesUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void RewardDesUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void RewardDesUI::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(120);
	spriteBg->setOpacity(100);
}
void RewardDesUI::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(120);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void RewardDesUI::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void RewardDesUI::tableCellTouched( TableView* table, TableViewCell* cell )
{

}

cocos2d::Size RewardDesUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(499,76);
}

cocos2d::extension::TableViewCell* RewardDesUI::tableCellAtIndex( TableView *table, ssize_t idx )
{
	TableViewCell *cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto spriteBg = Sprite::create("res_ui/highlight.png");
	spriteBg->setTextureRect(Rect(25, 25, 1, 1));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(120);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	auto rankingPrize = m_rewardDesList.at(idx);
	Color3B color_green = Color3B(47,93,13);

	// ��ͼ
	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(0, 0));
	sprite_bigFrame->setCapInsets(Rect(9,14,1,1));
	sprite_bigFrame->setPreferredSize(Size(499,74));
	cell->addChild(sprite_bigFrame);

	if (rankingPrize->end() - rankingPrize->start() == 0)
	{
		if (rankingPrize->start() <= 3 )
		{
			std::string icon_path = "res_ui/rank";
			char s_ranking[10];
			sprintf(s_ranking,"%d",rankingPrize->start());
			icon_path.append(s_ranking);
			icon_path.append(".png");
			auto sp_ranking = Sprite::create(icon_path.c_str());
			sp_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
			sp_ranking->setPosition(Vec2(55,38));
			sp_ranking->setScale(0.95f);
			cell->addChild(sp_ranking);
		}
		else
		{
			char s_ranking[10];
			sprintf(s_ranking,"%d",rankingPrize->start());
			auto l_ranking = Label::createWithTTF(s_ranking,APP_FONT_NAME,18);
			l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
			l_ranking->setPosition(Vec2(55,38));
			l_ranking->setColor(color_green);
			cell->addChild(l_ranking);
		}
	}
	else
	{
		std::string str_ranking_des ;
		char s_ranking_start[10];
		sprintf(s_ranking_start,"%d",rankingPrize->start());
		char s_ranking_end[10];
		sprintf(s_ranking_end,"%d",rankingPrize->end());
		str_ranking_des.append(s_ranking_start);
		str_ranking_des.append("-");
		str_ranking_des.append(s_ranking_end);
		auto l_ranking = Label::createWithTTF(str_ranking_des.c_str(),APP_FONT_NAME,18);
		l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		l_ranking->setPosition(Vec2(55,38));
		l_ranking->setColor(color_green);
		cell->addChild(l_ranking);
	}

	//EXP
	auto sp_exp_icon = Sprite::create("res_ui/exp.png");
	sp_exp_icon->setAnchorPoint(Vec2(0.5f,0.5f));
	sp_exp_icon->setPosition(Vec2(140,49));
	cell->addChild(sp_exp_icon);
	char s_expValue[20];
	sprintf(s_expValue,"%d",rankingPrize->prize().exp());
	auto l_expValue = Label::createWithTTF(s_expValue,APP_FONT_NAME,14);
	l_expValue->setAnchorPoint(Vec2(0.5f,0.5f));
	l_expValue->setPosition(Vec2(203,49));
	l_expValue->setColor(color_green);
	cell->addChild(l_expValue);

	//Gold
	auto sp_gold_icon = Sprite::create("res_ui/coins.png");
	sp_gold_icon->setAnchorPoint(Vec2(0.5f,0.5f));
	sp_gold_icon->setPosition(Vec2(140,22));
	cell->addChild(sp_gold_icon);
	char s_goldValue[20];
	sprintf(s_goldValue,"%d",rankingPrize->prize().gold());
	auto l_goldValue = Label::createWithTTF(s_goldValue,APP_FONT_NAME,14);
	l_goldValue->setAnchorPoint(Vec2(0.5f,0.5f));
	l_goldValue->setPosition(Vec2(203,22));
	l_goldValue->setColor(color_green);
	cell->addChild(l_goldValue);

	for(int j = 0;j<rankingPrize->prize().goods_size();++j)
	{
		if (j >= 3)
			continue;

		auto goodInfo = new GoodsInfo();
		goodInfo->CopyFrom(rankingPrize->prize().goods(j).goods());
		int num = 0;
		num = rankingPrize->prize().goods(j).quantity();

		auto goodsItem = VipRewardCellItem::create(goodInfo,num);
		goodsItem->setIgnoreAnchorPointForPosition(false);
		goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		goodsItem->setPosition(Vec2(290+85*j,38));
		cell->addChild(goodsItem);

		delete goodInfo;
	}

	return cell;
}

ssize_t RewardDesUI::numberOfCellsInTableView( TableView *table )
{
	return m_rewardDesList.size();
}
