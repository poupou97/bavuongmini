#include "DmgRankingUI.h"
#include "WorldBossData.h"
#include "../../../messageclient/element/CBoss.h"
#include "../../../messageclient/element/CRankingPrize.h"
#include "AppMacros.h"
#include "../../../messageclient/element/GoodsInfo.h"
#include "../../../ui/vip_ui/VipRewardCellItem.h"
#include "../../../messageclient/element/CDeadHistory.h"
#include "../../../gamescene_state/sceneelement/TargetInfo.h"
#include "../../../GameView.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../utils/GameConfig.h"
#include "../../../gamescene_state/role/BasePlayer.h"
#include "cocostudio/CCSGUIReader.h"

#define  SelectImageTag 458

DmgRankingUI::DmgRankingUI():
curBossIdx(0),
curRankIdx(0),
lastSelectCellId(0),
selectCellId(0)
{
}


DmgRankingUI::~DmgRankingUI()
{
}

DmgRankingUI * DmgRankingUI::create(int bossIdx,int rankIdx)
{
	auto dmgRankingUI = new DmgRankingUI();
	if (dmgRankingUI && dmgRankingUI->init(bossIdx,rankIdx))
	{
		dmgRankingUI->autorelease();
		return dmgRankingUI;
	}
	CC_SAFE_DELETE(dmgRankingUI);
	return NULL;
}

bool DmgRankingUI::init(int bossIdx,int rankIdx)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		curBossIdx = bossIdx;
		curRankIdx = rankIdx;

		auto u_layer = Layer::create();
		addChild(u_layer);

		//���UI
		auto ppanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/worldboss_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(608, 417));
		ppanel->setPosition(Vec2::ZERO);	
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto btn_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnabled( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(DmgRankingUI::CloseEvent,this));

		auto panel_rewardDes = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_introduction");
		panel_rewardDes->setVisible(false);
		auto panel_getReward = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_introduction_0");
		panel_getReward->setVisible(false);
		auto panel_ranking = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_ranking");
		panel_ranking->setVisible(true);

		auto l_name = (Text*)Helper::seekWidgetByName(panel_ranking,"Label_bossNameValue");
		l_name->setString(WorldBossData::getInstance()->m_worldBossDataList.at(bossIdx)->name().c_str());
		auto l_lv = (Text*)Helper::seekWidgetByName(panel_ranking,"Label_bossLvValue");
		std::string str_lv = "LV";
		char s_lv[10];
		sprintf(s_lv,"%d",WorldBossData::getInstance()->m_worldBossDataList.at(bossIdx)->level());
		str_lv.append(s_lv);
		l_lv->setString(str_lv.c_str());
		auto l_date = (Text*)Helper::seekWidgetByName(panel_ranking,"Label_dateValue");
		l_date->setString(WorldBossData::getInstance()->m_worldBossDataList.at(bossIdx)->deadhistory(rankIdx).data().c_str());
		auto l_time = (Text*)Helper::seekWidgetByName(panel_ranking,"Label_dateValue_0");
		l_time->setString(WorldBossData::getInstance()->m_worldBossDataList.at(bossIdx)->deadhistory(rankIdx).time().c_str());

		auto l_roleRanking = (Text*)Helper::seekWidgetByName(panel_ranking,"Label_roleRank");
		if (WorldBossData::getInstance()->m_worldBossDataList.at(bossIdx)->deadhistory(rankIdx).playerranking() > 0)
		{
			char s_roleRanking[10];
			sprintf(s_roleRanking,"%d",WorldBossData::getInstance()->m_worldBossDataList.at(bossIdx)->deadhistory(rankIdx).playerranking());
			l_roleRanking->setString(s_roleRanking);
		}
		else
		{
			l_roleRanking->setString(StringDataManager::getString("WorldBossUI_ranking_none"));
		}

		auto m_tableView = TableView::create(this, Size(501,205));
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0, 0));
		m_tableView->setPosition(Vec2(52,69));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		u_layer->addChild(m_tableView);

		this->setContentSize(Size(608,417));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		return true;
	}
	return false;
}

void DmgRankingUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void DmgRankingUI::onExit()
{
	UIScene::onExit();
}

bool DmgRankingUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void DmgRankingUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void DmgRankingUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void DmgRankingUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void DmgRankingUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void DmgRankingUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void DmgRankingUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void DmgRankingUI::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void DmgRankingUI::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void DmgRankingUI::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void DmgRankingUI::tableCellTouched(TableView* table, TableViewCell* cell )
{
	Size winSize = Director::getInstance()->getVisibleSize();

	selectCellId = cell->getIdx();
	//ȡ���ϴ�ѡ��״̬�����±���ѡ��״̬
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();

	auto deadHistory = new CDeadHistory();
	deadHistory->CopyFrom(WorldBossData::getInstance()->m_worldBossDataList.at(curBossIdx)->deadhistory(curRankIdx));

	auto list = (TargetInfoList*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagMainSceneTargetInfo);
	if (list != NULL)
	{
		list->removeFromParentAndCleanup(true);
	}

	std::string pressionStr = BasePlayer::getProfessionNameIdxByIndex(deadHistory->ranking(cell->getIdx()).player().profession());
	int pressionId = BasePlayer::getProfessionIdxByName(pressionStr.c_str());

	auto tarlist = TargetInfoList::create(deadHistory->ranking(cell->getIdx()).player().roleid(), 
										deadHistory->ranking(cell->getIdx()).player().name().c_str(),
										deadHistory->ranking(cell->getIdx()).player().countryid(),
										deadHistory->ranking(cell->getIdx()).player().viplevel(),
										deadHistory->ranking(cell->getIdx()).player().level(),
										pressionId);
	tarlist->setIgnoreAnchorPointForPosition(false);
	tarlist->setAnchorPoint(Vec2(0, 0));
	tarlist->setTag(ktagMainSceneTargetInfo);
	//tarlist->setPosition(Vec2(finalPositionX, finalPositionY));
	tarlist->setPosition(Vec2(winSize.width / 2 - tarlist->getContentSize().width/2, winSize.height / 2 + tarlist->getContentSize().height/2));
	GameView::getInstance()->getMainUIScene()->addChild(tarlist);

	delete deadHistory;
}

cocos2d::Size DmgRankingUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(499,47);
}

cocos2d::extension::TableViewCell* DmgRankingUI::tableCellAtIndex( TableView *table, ssize_t idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(Size(499,45));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setPosition(Vec2(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	auto deadHistory = new CDeadHistory();
	deadHistory->CopyFrom(WorldBossData::getInstance()->m_worldBossDataList.at(curBossIdx)->deadhistory(curRankIdx));
	Color3B color_green = Color3B(47,93,13);

	// ��ͼ
	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(0, 0));
	sprite_bigFrame->setCapInsets(Rect(9,14,1,1));
	sprite_bigFrame->setPreferredSize(Size(499,45));
	cell->addChild(sprite_bigFrame);

	if (deadHistory->ranking(idx).ranking() <= 3 )
	{
		std::string icon_path = "res_ui/rank";
		char s_ranking[10];
		if (deadHistory->ranking(idx).ranking() <= 0)
		{
			sprintf(s_ranking,"%d",1);
		}
		else
		{
			sprintf(s_ranking,"%d",deadHistory->ranking(idx).ranking());
		}
		icon_path.append(s_ranking);
		icon_path.append(".png");
		auto sp_ranking = Sprite::create(icon_path.c_str());
		sp_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		sp_ranking->setPosition(Vec2(41,24));
		sp_ranking->setScale(0.95f);
		cell->addChild(sp_ranking);
	}
	else if (deadHistory->ranking(idx).ranking() > 3  && deadHistory->ranking(idx).ranking() <= 10)
	{
		char s_ranking[10];
		sprintf(s_ranking,"%d",deadHistory->ranking(idx).ranking());
		auto l_ranking = Label::createWithBMFont("res_ui/font/ziti_3.fnt", s_ranking);
		l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		l_ranking->setPosition(Vec2(41,24));
		cell->addChild(l_ranking);
	}
	else
	{
		char s_ranking[10];
		sprintf(s_ranking,"%d",deadHistory->ranking(idx).ranking());
		auto l_ranking = Label::createWithTTF(s_ranking,APP_FONT_NAME,18);
		l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		l_ranking->setPosition(Vec2(41,24));
		l_ranking->setColor(color_green);
		cell->addChild(l_ranking);
	}

	//Name
	auto l_name = Label::createWithTTF(deadHistory->ranking(idx).player().name().c_str(),APP_FONT_NAME,18);
	l_name->setAnchorPoint(Vec2(0.0f,0.5f));
	l_name->setPosition(Vec2(116,24));
	l_name->setColor(color_green);
	cell->addChild(l_name);

	int countryId = deadHistory->ranking(idx).player().countryid();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		auto countrySp_ = Sprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(Vec2(1.0,0.5f));
		//countrySp_->setPosition(Vec2(l_name->getPositionX() - l_name->getContentSize().width/2-1,l_name->getPositionY()));
		countrySp_->setPosition(Vec2(l_name->getPositionX()-1,l_name->getPositionY()));
		countrySp_->setScale(0.7f);
		cell->addChild(countrySp_);
	}

	//vipInfo
	// vip���ؿ��
	bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
	if (vipEnabled)
	{
		if (deadHistory->ranking(idx).player().viplevel() > 0)
		{
			auto pNode = MainScene::addVipInfoByLevelForNode(deadHistory->ranking(idx).player().viplevel());
			if (pNode)
			{
				cell->addChild(pNode);
				//pNode->setPosition(Vec2(l_name->getPosition().x+l_name->getContentSize().width/2+13,l_name->getPosition().y));
				pNode->setPosition(Vec2(l_name->getPosition().x+l_name->getContentSize().width+13,l_name->getPosition().y));
			}
		}
	}

	//Level
	char s_lv[20];
	sprintf(s_lv,"%d",deadHistory->ranking(idx).player().level());
	auto l_level = Label::createWithTTF(s_lv,APP_FONT_NAME,18);
	l_level->setAnchorPoint(Vec2(0.5f,0.5f));
	l_level->setPosition(Vec2(267,24));
	l_level->setColor(color_green);
	cell->addChild(l_level);
	//profession
	auto l_profession = Label::createWithTTF(BasePlayer::getProfessionNameIdxByIndex(deadHistory->ranking(idx).player().profession()).c_str(),APP_FONT_NAME,18);
	l_profession->setAnchorPoint(Vec2(0.5f,0.5f));
	l_profession->setPosition(Vec2(345,24));
	l_profession->setColor(color_green);
	cell->addChild(l_profession);
	//Dmg
	char s_dmg[20];
	sprintf(s_dmg,"%d",deadHistory->ranking(idx).hurt());
	auto l_dmg = Label::createWithTTF(s_dmg,APP_FONT_NAME,18);
	l_dmg->setAnchorPoint(Vec2(0.5f,0.5f));
	l_dmg->setPosition(Vec2(440,24));
	l_dmg->setColor(color_green);
	cell->addChild(l_dmg);

	if (this->selectCellId == idx)
	{
		pHighlightSpr->setVisible(true);
	}

	delete deadHistory;
	return cell;
}

ssize_t DmgRankingUI::numberOfCellsInTableView( TableView *table )
{
	if (WorldBossData::getInstance()->m_worldBossDataList.at(curBossIdx)->deadhistory(curRankIdx).ranking_size() <= 10)
	{
		return WorldBossData::getInstance()->m_worldBossDataList.at(curBossIdx)->deadhistory(curRankIdx).ranking_size();
	}
	else
	{
		return 10;
	}
}
