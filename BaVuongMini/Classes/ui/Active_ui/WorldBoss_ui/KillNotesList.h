#ifndef _UI_ACTIVE_WORLDBOSSUI_KILLNOTESLIST_H_
#define _UI_ACTIVE_WORLDBOSSUI_KILLNOTESLIST_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ��ɱ��¼
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.05.27
 */

class CDeadHistory;

class KillNotesList:public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	KillNotesList(void);
	~KillNotesList(void);

public:
	static KillNotesList* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);
private:
	Layer * m_base_layer;																							// ��layer
	TableView * m_tableView;																						// ��tableView

	std::vector<CDeadHistory*> m_deadHistoryList;
	int m_nCurRankIdx;

	int lastSelectCellId;
	int selectCellId;

private:
	void initUI();

public:
	void refreshDataSourceByIdx(int idx);
	void refreshUI();
};

#endif

