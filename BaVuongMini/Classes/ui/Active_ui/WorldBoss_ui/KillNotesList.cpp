#include "KillNotesList.h"
#include "../ActiveUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../../GameAudio.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../AppMacros.h"
#include "GameView.h"
#include "../../extensions/CCMoveableMenu.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../utils/GameUtils.h"
#include "WorldBossData.h"
#include "../../../messageclient/element/CDeadHistory.h"
#include "../../../messageclient/element/CBoss.h"
#include "DmgRankingUI.h"
#include "../../../utils/GameConfig.h"

#define  SelectImageTag 458

KillNotesList::KillNotesList(void):
m_nCurRankIdx(-1),
lastSelectCellId(0),
selectCellId(0)
{

}


KillNotesList::~KillNotesList(void)
{
}

KillNotesList* KillNotesList::create()
{
	auto killNotesList = new KillNotesList();
	if (killNotesList && killNotesList->init())
	{
		killNotesList->autorelease();
		return killNotesList;
	}
	CC_SAFE_DELETE(killNotesList);
	return NULL;
}

bool KillNotesList::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		m_base_layer = Layer::create();
		this->addChild(m_base_layer);	

		initUI();

		return true;
	}
	return false;
}

void KillNotesList::onEnter()
{
	UIScene::onEnter();
}

void KillNotesList::onExit()
{
	UIScene::onExit();
}

void KillNotesList::initUI()
{
	Size winSize = Director::getInstance()->getVisibleSize();
	// �tableView
	m_tableView = TableView::create(this, Size(413,139));
	//m_tableView ->setSelectedEnable(true);   // �֧��ѡ��״̬����ʾ
	//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 25, 1, 1), Vec2(0,0));   // ���þŹ���ͼƬ
	m_tableView->setDirection(TableView::Direction::VERTICAL);
	m_tableView->setAnchorPoint(Vec2(0, 0));
	m_tableView->setPosition(Vec2(331,40));
	m_tableView->setDelegate(this);
	m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//m_tableView->setPressedActionEnabled(false);
	m_base_layer->addChild(m_tableView);
}


bool KillNotesList::onTouchBegan( Touch *touch, Event * pEvent )
{
	return false;
}

void KillNotesList::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void KillNotesList::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void KillNotesList::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void KillNotesList::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void KillNotesList::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void KillNotesList::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void KillNotesList::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void KillNotesList::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void KillNotesList::tableCellTouched( TableView* table, TableViewCell* cell )
{
	selectCellId = cell->getIdx();
	//ȡ���ϴ�ѡ��״̬�����±���ѡ��״̬
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();

	m_nCurRankIdx = cell->getIdx();

	if (WorldBossData::getInstance()->m_nCurSelectBossIdx < 0)
		return;

	if (m_nCurRankIdx < 0)
		return;

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagDmgRankingUI) == NULL)
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		auto dmgRankingUI = DmgRankingUI::create(WorldBossData::getInstance()->m_nCurSelectBossIdx,m_nCurRankIdx);
		dmgRankingUI->setIgnoreAnchorPointForPosition(false);
		dmgRankingUI->setAnchorPoint(Vec2(.5f,.5f));
		dmgRankingUI->setPosition(Vec2(winSize.width/2,winSize.height/2));
		dmgRankingUI->setTag(kTagDmgRankingUI);
		GameView::getInstance()->getMainUIScene()->addChild(dmgRankingUI);
	}
}

cocos2d::Size KillNotesList::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(409,47);
}

cocos2d::extension::TableViewCell* KillNotesList::tableCellAtIndex( TableView *table, ssize_t idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();


	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(Size(409,45));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setPosition(Vec2(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	auto deadHistory = m_deadHistoryList.at(idx);
	Color3B color_green = Color3B(47,93,13);

	// ��ͼ
	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(1, 0));
	sprite_bigFrame->setCapInsets(Rect(9,14,1,1));
	sprite_bigFrame->setPreferredSize(Size(409,45));
	cell->addChild(sprite_bigFrame);

// 	cocos2d::extension::Scale9Sprite* pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(5, 5, 1, 1) , "res_ui/mengban_green.png");
// 	pHighlightSpr->setPreferredSize(Size(409,45));
// 	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
// 	pHighlightSpr->setPosition(Vec2(0,0));
// 	//pHighlightSpr->setTag(SelectImageTag);
// 	cell->addChild(pHighlightSpr);
// 	pHighlightSpr->setVisible(false);

	// ��ɱ���
	auto l_date = Label::createWithTTF(deadHistory->data().c_str(), APP_FONT_NAME, 15, Size(200, 0), TextHAlignment::LEFT);
	l_date->setColor(color_green);
	l_date->setAnchorPoint(Vec2(0.f, 0.5f));
	l_date->setPosition(Vec2(13, 30));
	cell->addChild(l_date);
	// ڻ�ɱʱ�
	auto l_time = Label::createWithTTF(deadHistory->time().c_str(), APP_FONT_NAME, 15, Size(200, 0), TextHAlignment::LEFT);
	l_time->setColor(color_green);
	l_time->setAnchorPoint(Vec2(0.f, 0.5f));
	l_time->setPosition(Vec2(13, 11));
	cell->addChild(l_time);

	// �����˺�
	auto l_maxDmg = Label::createWithTTF("", APP_FONT_NAME, 18);
	l_maxDmg->setColor(color_green);
	l_maxDmg->setAnchorPoint(Vec2(0.0f, 0.5f));
	l_maxDmg->setHorizontalAlignment(TextHAlignment::CENTER);
	l_maxDmg->setPosition(Vec2(135, 23));
	cell->addChild(l_maxDmg);

	if (deadHistory->ranking_size() > 0)
	{
		l_maxDmg->setString(deadHistory->ranking(0).player().name().c_str());

		int countryId = deadHistory->ranking(0).player().countryid();
		if (countryId > 0 && countryId <6)
		{
			std::string countryIdName = "country_";
			char id[2];
			sprintf(id, "%d", countryId);
			countryIdName.append(id);
			std::string iconPathName = "res_ui/country_icon/";
			iconPathName.append(countryIdName);
			iconPathName.append(".png");
			auto countrySp_ = Sprite::create(iconPathName.c_str());
			countrySp_->setAnchorPoint(Vec2(1.0,0.5f));
			//countrySp_->setPosition(Vec2(l_maxDmg->getPositionX() - l_maxDmg->getContentSize().width/2-1,l_maxDmg->getPositionY()));
			countrySp_->setPosition(Vec2(l_maxDmg->getPositionX()-1,l_maxDmg->getPositionY()));
			countrySp_->setScale(0.7f);
			cell->addChild(countrySp_);
		}

		//vipInfo
		// vip���ؿ��
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (vipEnabled)
		{
			if (deadHistory->ranking(0).player().viplevel() > 0)
			{
				auto pNode = MainScene::addVipInfoByLevelForNode(deadHistory->ranking(0).player().viplevel());
				if (pNode)
				{
					cell->addChild(pNode);
					//pNode->setPosition(Vec2(l_maxDmg->getPosition().x+l_maxDmg->getContentSize().width/2+13,l_maxDmg->getPosition().y));
					pNode->setPosition(Vec2(l_maxDmg->getPosition().x+l_maxDmg->getContentSize().width+13,l_maxDmg->getPosition().y));
				}
			}
		}
	}

	//����һ�
	auto l_lastKill = Label::createWithTTF(deadHistory->killer().name().c_str(), APP_FONT_NAME, 18);
	l_lastKill->setColor(color_green);
	l_lastKill->setAnchorPoint(Vec2(0.0f, 0.5f));
	l_lastKill->setHorizontalAlignment(TextHAlignment::CENTER);
	l_lastKill->setPosition(Vec2(287, 23));
	cell->addChild(l_lastKill);

	int countryId = deadHistory->killer().countryid();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		auto countrySp_ = Sprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(Vec2(1.0,0.5f));
		//countrySp_->setPosition(Vec2(l_lastKill->getPositionX() - l_lastKill->getContentSize().width/2-1,l_lastKill->getPositionY()));
		countrySp_->setPosition(Vec2(l_lastKill->getPositionX()-1,l_lastKill->getPositionY()));
		countrySp_->setScale(0.7f);
		cell->addChild(countrySp_);
	}

	//vipInfo
	// vip�ؿ��
	bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
	if (vipEnabled)
	{
		if (deadHistory->killer().viplevel() > 0)
		{
			auto pNode = MainScene::addVipInfoByLevelForNode(deadHistory->killer().viplevel());
			if (pNode)
			{
				cell->addChild(pNode);
				//pNode->setPosition(Vec2(l_lastKill->getPosition().x+l_lastKill->getContentSize().width/2+13,l_lastKill->getPosition().y));
				pNode->setPosition(Vec2(l_lastKill->getPosition().x+l_lastKill->getContentSize().width+13,l_lastKill->getPosition().y));
			}
		}
	}

	if (this->selectCellId == idx)
	{
		pHighlightSpr->setVisible(true);
	}

	return cell;
}

ssize_t KillNotesList::numberOfCellsInTableView( TableView *table )
{
	return m_deadHistoryList.size();
}

void KillNotesList::refreshDataSourceByIdx(int idx)
{
	//delete old data
	std::vector<CDeadHistory *>::iterator iter;
	for (iter = m_deadHistoryList.begin(); iter != m_deadHistoryList.end(); iter++)
	{
		delete *iter;
	}
	m_deadHistoryList.clear();

	//add new data

	for (int i = 0;i<WorldBossData::getInstance()->m_worldBossDataList.at(idx)->deadhistory_size();++i)
	{
		auto temp = new CDeadHistory();
		temp->CopyFrom(WorldBossData::getInstance()->m_worldBossDataList.at(idx)->deadhistory(i));
		m_deadHistoryList.push_back(temp);
	}
}

void KillNotesList::refreshUI()
{
	lastSelectCellId = 0;
	selectCellId = 0;
	m_tableView->reloadData();
}
