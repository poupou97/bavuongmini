#include "UISceneTest.h"

#include "../loadscene_state/LoadSceneState.h"
#include "cocostudio\CCSGUIReader.h"

UISceneTest::UISceneTest()
{
}

UISceneTest::~UISceneTest()
{
    //ActionManager::purgeActionManager();
	Director::getInstance()->getActionManager()->removeAllActions();
	cocostudio::GUIReader::getInstance()->destroyInstance();

	//Director::getInstance()->getTextureCache()->removeUnusedTextures();
}

UISceneTest * UISceneTest::create()
{
	UISceneTest * _ui = new UISceneTest();
	if(_ui && _ui->init())
	{
		_ui->autorelease();
		return _ui;
	}
	CC_SAFE_DELETE(_ui);
	return NULL;
}

bool UISceneTest::init()
{
	if (UIScene::init())
	{
		Size s=Director::getInstance()->getVisibleSize();

		auto uiWidgetLayer = Layer::create();
		addChild(uiWidgetLayer);
		uiWidgetLayer->setPosition(Vec2(0,0));

		//Layout * ppanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/jishouhang_1.json");
		auto ppanel = ui::Layout::create();
		ppanel->setAnchorPoint(Vec2(0,0));
		ppanel->setPosition(Vec2(0,0));
		uiWidgetLayer->addChild(ppanel);

		//Layout * ppanel = NULL;
		//if(LoadSceneLayer::friendPanelBg->getWidgetParent() != NULL)
		//{
		//	LoadSceneLayer::friendPanelBg->removeFromParentAndCleanup(false);
		//}
		//ppanel = LoadSceneLayer::friendPanelBg;
		//uiWidgetLayer->addChild(ppanel);

		//Layout * ppanel_1 = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/haoyou_1.json");
		////Layout * ppanel = Layout::create();
		//ppanel_1->setAnchorPoint(Vec2(0,0));
		//ppanel_1->setPosition(Vec2(0,0));
		//uiWidgetLayer->addChild(ppanel_1);

		for(int i = 0; i < 50; i++)
		{
		//Label * labelText= Label::create();
		//labelText->setAnchorPoint(Vec2(0.5f,0.5f));
		//labelText->setPosition(Vec2(ppanel->getContentSize().width/2,ppanel->getContentSize().height*2/3));
		//labelText->setFontSize(24);
		//labelText->setText("hahaha");
		//labelText->setLocalZOrder(10);
		//ppanel->addChild(labelText);
		//m_pLayer->addChild(labelText);
		}

		for(int i = 0; i < 150; i++)
		{
			auto ImageView_chat = ui::ImageView::create();
			ImageView_chat->setScale9Enabled(true);
			ImageView_chat->loadTexture("gamescene_state/ditu/ditu_di.png");
			ImageView_chat->setContentSize(Size(380, 80));
			ImageView_chat->setAnchorPoint(Vec2::ZERO);
			ImageView_chat->setPosition(Vec2(0,0));
			ppanel->addChild(ImageView_chat);
			//uiWidgetLayer->addChild(ImageView_chat);
		}

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(s.width,s.height));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(UISceneTest::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(UISceneTest::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(UISceneTest::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(UISceneTest::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
		
		return true;
	}
	return false;
}

void UISceneTest::onEnter()
{
	UIScene::onEnter();
}

void UISceneTest::onExit()
{
	UIScene::onExit();
}

bool UISceneTest::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return true;
}
void UISceneTest::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void UISceneTest::onTouchEnded(Touch *pTouch, Event *pEvent)
{
	this->removeFromParent();
	CCLOG("removeUISceneTest");
}
void UISceneTest::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}
