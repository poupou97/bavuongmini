#ifndef _SIGNMONTHLYUI_SIGNMONTHLYUI_H_
#define _SIGNMONTHLYUI_SIGNMONTHLYUI_H_

#include "../../ui/extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
class GoodsInfo;

class SignMonthlyUI : public UIScene
{
public:
	SignMonthlyUI();
	~SignMonthlyUI();

	static SignMonthlyUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	virtual void update(float dt);

	void GetGeneralEvent(Ref *pSender, Widget::TouchEventType type);

	void createGenralHead();

	void RefreshByDays(int curDay);
private:
	Layer * u_layer;
	cocos2d::extension::ScrollView * scrollView_info ;
};

///////////////////////////////////////////////////////////////

class GoodsInfo;
class COneMouthSignGift;

class SignMonthlyItem : public UIScene
{
public:
	SignMonthlyItem();
	~SignMonthlyItem();

	static SignMonthlyItem* create(COneMouthSignGift * oneMoushSignGift);
	bool init(COneMouthSignGift * oneMoushSignGift);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void GoodItemEvent(Ref *pSender, Widget::TouchEventType type);

	void RefreshUIByStatus(int status);

public:
	COneMouthSignGift * curOneMonthSighGift;
private:
	
	GoodsInfo * curGoods;
	ImageView* image_Frame;
};

#endif

