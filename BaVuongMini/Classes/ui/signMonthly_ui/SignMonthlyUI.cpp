#include "SignMonthlyUI.h"
#include "SignMonthlyData.h"
#include "messageclient\element\GoodsInfo.h"
#include "ui\backpackscene\EquipmentItem.h"
#include "AppMacros.h"
#include "gamescene_state\MainScene.h"
#include "ui\backpackscene\GoodsItemInfoBase.h"
#include "GameView.h"
#include "messageclient\element\COneMouthSignGift.h"
#include "ui\generals_ui\NormalGeneralsHeadItem.h"
#include "ui\generals_ui\ShowBigGeneralsHead.h"
#include "utils\StaticDataManager.h"
#include "messageclient\element\CGeneralBaseMsg.h"
#include "ui\generals_ui\RecuriteActionItem.h"
#include "legend_engine\CCLegendAnimation.h"
#include "messageclient\element\FolderInfo.h"
#include "..\..\messageclient\GameMessageProcessor.h"
#include "cocostudio\CCSGUIReader.h"

#define kTag_SignMonthlyItem_Base 300

SignMonthlyUI::SignMonthlyUI(void)
{
}


SignMonthlyUI::~SignMonthlyUI(void)
{
}

SignMonthlyUI* SignMonthlyUI::create()
{
	auto signMonthlyUI = new SignMonthlyUI();
	if (signMonthlyUI && signMonthlyUI->init())
	{
		signMonthlyUI->autorelease();
		return signMonthlyUI;
	}
	CC_SAFE_DELETE(signMonthlyUI);
	return NULL;
}

bool SignMonthlyUI::init()
{
	if (UIScene::init())
	{
		auto winSize = Director::getInstance()->getVisibleSize();

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(800, 480));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winSize.width/2,winSize.height/2));
		addChild(u_layer);

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winSize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		//���UI
		auto  ppanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/monthLogon_1.json");
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winSize.width/2,winSize.height/2));
		m_pLayer->addChild(ppanel);

		auto btn_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnabled(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(SignMonthlyUI::CloseEvent, this));

		auto btn_getGeneral = (Button*)Helper::seekWidgetByName(ppanel,"Button_getGeneral");
		btn_getGeneral->setTouchEnabled(true);
		btn_getGeneral->setPressedActionEnabled(true);
		btn_getGeneral->addTouchEventListener(CC_CALLBACK_2(SignMonthlyUI::GetGeneralEvent, this));

		auto lbf_curMonth = (Text*)Helper::seekWidgetByName(ppanel,"Label_curMonth");
		auto lbf_curMonth1 = (Text*)Helper::seekWidgetByName(ppanel,"Label_curMonth1");
		auto lbf_signedDays = (Text*)Helper::seekWidgetByName(ppanel,"Label_SignedDays");

		char s_curMonth [5];
		sprintf(s_curMonth,"%d",SignMonthlyData::getInstance()->m_nCurMonth);
		lbf_curMonth->setString(s_curMonth);
		lbf_curMonth1->setString(s_curMonth);

		char s_signedDays [5];
		sprintf(s_signedDays,"%d",SignMonthlyData::getInstance()->m_nSignedNum);
		lbf_signedDays->setString(s_signedDays);

		//refresh GeneralHeadItemInfo
		//NormalGeneralsHeadItemBase * generalHead = NormalGeneralsHeadItemBase::create(50); //SignMonthlyData::getInstance()->m_strGeneralId
// 		GeneralsHeadItemBase * generalHead = GeneralsHeadItemBase::create(50);
// 		generalHead->setAnchorPoint(Vec2(0,0));
// 		generalHead->setPosition(Vec2(500,100));
// 		u_layer->addChild(generalHead);
		createGenralHead();

		//create scrollerView
		scrollView_info = cocos2d::extension::ScrollView::create();
		scrollView_info->setViewSize(Size(397, 286));
		scrollView_info->setTouchEnabled(true);
		scrollView_info->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		scrollView_info->setPosition(Vec2(100,70));
		scrollView_info->setBounceable(true);
		scrollView_info->setClippingToBounds(true);
		u_layer->addChild(scrollView_info);

		//create elements
		int scroll_height = 0;
		int height_everyLine = 90;
		if (SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size() > 0)
		{
			int lineNum = 0;
			if (SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size()%5 == 0)
			{
				lineNum = SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size()/5;
			}
			else
			{
				lineNum = SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size()/5+1;
			}

			scroll_height = lineNum * height_everyLine;

			if (scroll_height > scrollView_info->getViewSize().height)
			{
				scrollView_info->setContentSize(Size(scrollView_info->getViewSize().width,scroll_height));
				scrollView_info->setContentOffset(Vec2(0,scrollView_info->getViewSize().height-scroll_height));  
			}
			else
			{
				scrollView_info->setContentSize(scrollView_info->getViewSize());
			}

			for (int i = 0;i<SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size();i++)
			{
				auto goodsInfo = new GoodsInfo();
				goodsInfo->CopyFrom(SignMonthlyData::getInstance()->m_oneMonthSighGiftData.at(i)->propid());
				
				auto item = SignMonthlyItem::create(SignMonthlyData::getInstance()->m_oneMonthSighGiftData.at(i));
				item->setAnchorPoint(Vec2(0,0));
				item->setPosition(Vec2(17+(i%5)*73,scroll_height - (i/5+1)*height_everyLine));
				scrollView_info->addChild(item);
				item->setTag(kTag_SignMonthlyItem_Base+i);

				delete goodsInfo;
			}
		}	

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(SignMonthlyUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(SignMonthlyUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(SignMonthlyUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(SignMonthlyUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void SignMonthlyUI::onEnter()
{
	UIScene::onEnter();
}

void SignMonthlyUI::onExit()
{
	UIScene::onExit();
}

bool SignMonthlyUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void SignMonthlyUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void SignMonthlyUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void SignMonthlyUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void SignMonthlyUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void SignMonthlyUI::update( float dt )
{

}

void SignMonthlyUI::GetGeneralEvent(Ref *pSender, Widget::TouchEventType type)
{

}

void SignMonthlyUI::createGenralHead()
{
	int generalModelId =  std::atoi(SignMonthlyData::getInstance()->m_strGeneralId.c_str());

	std::map<int ,CGeneralBaseMsg *>::const_iterator cIter;
	cIter = GeneralsConfigData::s_generalsBaseMsgData.find(generalModelId);
	if (cIter == GeneralsConfigData::s_generalsBaseMsgData.end()) // �û�ҵ�����ָ�END��  
	{
		return;
	}

	auto generalBaseMsg = GeneralsConfigData::s_generalsBaseMsgData[generalModelId];
	if (generalBaseMsg == NULL)
		return;

	//˱߿
	auto frame = ImageView::create();
	frame->loadTexture(BigGeneralsFramePath_White);    //�Ĭ���1Ǽ���1-10Ϊ��ɫ��
	frame->setAnchorPoint(Vec2::ZERO);
	frame->setPosition(Vec2(520,158));
	frame->setScale(0.75f);
	u_layer->addChild(frame);

	//���
	auto label_name = Label::createWithTTF(generalBaseMsg->name().c_str(), APP_FONT_NAME, 20);
	label_name->setAnchorPoint(Vec2(0.5f,0.5f));
	label_name->setAnchorPoint(Vec2(0.5f,0.5f));
	label_name->setPosition(Vec2(frame->getContentSize().width/2,42));
	label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(1));
	frame->addChild(label_name);
	//�ͷ�
	std::string icon_path = "res_ui/general/";
	icon_path.append(generalBaseMsg->get_half_photo());
	//icon_path.append("guanyu");
	icon_path.append(".png");
	auto imageView_head = ImageView::create();
	imageView_head->loadTexture(icon_path.c_str());
	imageView_head->setAnchorPoint(Vec2(0.5f,0));
	imageView_head->setPosition(Vec2(frame->getContentSize().width/2,64));
	frame->addChild(imageView_head);

	//�ϡ�ж
	if (generalBaseMsg->rare()>0)
	{
// 		ImageView* imageView_star = ImageView::create();
// 		imageView_star->loadTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
// 		imageView_star->setAnchorPoint(Vec2::ZERO);
// 		imageView_star->setScale(1.0f);
// 		imageView_star->setPosition(Vec2(15,63));
// 		imageView_star->setName("ImageView_Star");
// 		frame->addChild(imageView_star);

		if (generalBaseMsg->rare() >= 5)
		{
			// load animation
			std::string animFileName = "animation/ui/wj/wj2.anm";
			auto m_pAnim = CCLegendAnimation::create(animFileName);
			m_pAnim->setPlayLoop(true);
			m_pAnim->setReleaseWhenStop(true);
			m_pAnim->setScale(0.75f);
			m_pAnim->setPosition(Vec2(531,205));
			m_pAnim->setPlaySpeed(.35f);
			u_layer->addChild(m_pAnim);
		}
	}
}

void SignMonthlyUI::RefreshByDays( int curDay )
{
	for (int i = 0;i<SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size();i++)
	{
		if (curDay != SignMonthlyData::getInstance()->m_oneMonthSighGiftData.at(i)->day())
			continue;

		auto item = (SignMonthlyItem*)scrollView_info->getChildByTag(kTag_SignMonthlyItem_Base+i);
		if (item)
		{
			item->curOneMonthSighGift->CopyFrom(*SignMonthlyData::getInstance()->m_oneMonthSighGiftData.at(i));
			item->RefreshUIByStatus(item->curOneMonthSighGift->status());
		}
	}
}

//////////////////////////////////////////////////////////////////////////////////////
SignMonthlyItem::SignMonthlyItem()
{

}

SignMonthlyItem::~SignMonthlyItem()
{
	CC_SAFE_DELETE(curOneMonthSighGift);
	CC_SAFE_DELETE(curGoods);
}

SignMonthlyItem* SignMonthlyItem::create(COneMouthSignGift * oneMoushSignGift)
{
	auto signMonthlyItem = new SignMonthlyItem();
	if (signMonthlyItem && signMonthlyItem->init(oneMoushSignGift))
	{
		signMonthlyItem->autorelease();
		return signMonthlyItem;
	}
	CC_SAFE_DELETE(signMonthlyItem);
	return NULL;
}

bool SignMonthlyItem::init(COneMouthSignGift * oneMoushSignGift)
{
	if (UIScene::init())
	{
		/////////////////////////////////////////////////////////////////////////////////////

		// ȸLayer�λ�TableView�֮�ڣ�Ϊ����ȷ��ӦTableView���϶��¼������Ϊ���̵����¼
		//m_pLayer->setSwallowsTouches(false);

		curOneMonthSighGift = new COneMouthSignGift();
		curOneMonthSighGift->CopyFrom(*oneMoushSignGift);

		curGoods = new GoodsInfo();
		curGoods->CopyFrom(oneMoushSignGift->propid());

		image_Frame = ImageView::create();
		image_Frame->loadTexture("res_ui/didd.png");
		image_Frame->setScale9Enabled(true);
		image_Frame->setContentSize(Size(70,88));
		image_Frame->setCapInsets(Rect(9,9,1,1));
		image_Frame->setAnchorPoint(Vec2(0,0));
		image_Frame->setPosition(Vec2(0,0));
		m_pLayer->addChild(image_Frame);

		/*********************��ж�װ������ɫ***************************/
		std::string frameColorPath;
		if (curGoods->quality() == 1)
		{
			frameColorPath = EquipWhiteFramePath;
		}
		else if (curGoods->quality() == 2)
		{
			frameColorPath = EquipGreenFramePath;
		}
		else if (curGoods->quality() == 3)
		{
			frameColorPath = EquipBlueFramePath;
		}
		else if (curGoods->quality() == 4)
		{
			frameColorPath = EquipPurpleFramePath;
		}
		else if (curGoods->quality() == 5)
		{
			frameColorPath = EquipOrangeFramePath;
		}
		else
		{
			frameColorPath = EquipVoidFramePath;
		}

		auto Btn_goodsItemFrame = Button::create();
		Btn_goodsItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_goodsItemFrame->setTouchEnabled(true);
		Btn_goodsItemFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
		Btn_goodsItemFrame->setPosition(Vec2(35, 55));
		Btn_goodsItemFrame->setScale(0.85f);
		Btn_goodsItemFrame->setPressedActionEnabled(true);
		Btn_goodsItemFrame->addTouchEventListener(CC_CALLBACK_2(SignMonthlyItem::GoodItemEvent, this));
		m_pLayer->addChild(Btn_goodsItemFrame);

		auto uiiImageView_goodsItem = ImageView::create();
		if (strcmp(curGoods->icon().c_str(),"") == 0)
		{
			uiiImageView_goodsItem->loadTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(curGoods->icon().c_str());
			_path.append(".png");
			uiiImageView_goodsItem->loadTexture(_path.c_str());
		}
		uiiImageView_goodsItem->setScale(0.9f);
		uiiImageView_goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		uiiImageView_goodsItem->setPosition(Vec2(0,0));
		Btn_goodsItemFrame->addChild(uiiImageView_goodsItem);

		char s_num[20];
		sprintf(s_num,"%d",curOneMonthSighGift->propnumber());
		auto Lable_num = Label::createWithTTF(s_num, APP_FONT_NAME, 13);
		Lable_num->setAnchorPoint(Vec2(1.0f,0.5f));
		Lable_num->setPosition(Vec2(0 + Btn_goodsItemFrame->getContentSize().width / 2 - 7, 
			0 - Btn_goodsItemFrame->getContentSize().height / 2 + Lable_num->getContentSize().height - 2));
		Lable_num->setName("Label_num");
		Btn_goodsItemFrame->addChild(Lable_num);

// 		if (goods->binding() == 1)
// 		{
// 			ImageView *ImageView_bound = ImageView::create();
// 			ImageView_bound->loadTexture("res_ui/binding.png");
// 			ImageView_bound->setAnchorPoint(Vec2(0,1.0f));
// 			ImageView_bound->setScale(0.85f);
// 			ImageView_bound->setPosition(Vec2(8,55));
// 			m_pLayer->addChild(ImageView_bound);
// 		}
// 
// 		//�����װ��
// 		if (goods->has_equipmentdetail())
// 		{
// 			if (goods->equipmentdetail().gradelevel() > 0)  //����ȼ�
// 			{
// 				std::string ss_gradeLevel = "+";
// 				char str_gradeLevel [10];
// 				sprintf(str_gradeLevel,"%d",goods->equipmentdetail().gradelevel());
// 				ss_gradeLevel.append(str_gradeLevel);
// 				Label *Lable_gradeLevel = Label::create();
// 				Lable_gradeLevel->setText(ss_gradeLevel.c_str());
// 				Lable_gradeLevel->setFontName(APP_FONT_NAME);
// 				Lable_gradeLevel->setFontSize(13);
// 				Lable_gradeLevel->setAnchorPoint(Vec2(1.0f,1.0f));
// 				Lable_gradeLevel->setPosition(Vec2(Btn_goodsItemFrame->getContentSize().width-10,Btn_goodsItemFrame->getContentSize().height-7));
// 				Lable_gradeLevel->setName("Lable_gradeLevel");
// 				m_pLayer->addChild(Lable_gradeLevel);
// 			}
// 
// 			if (goods->equipmentdetail().starlevel() > 0)  //�Ǽ�
// 			{
// 				char str_starLevel [10];
// 				sprintf(str_starLevel,"%d",goods->equipmentdetail().starlevel());
// 				Label *Lable_starLevel = Label::create();
// 				Lable_starLevel->setText(str_starLevel);
// 				Lable_starLevel->setFontName(APP_FONT_NAME);
// 				Lable_starLevel->setFontSize(13);
// 				Lable_starLevel->setAnchorPoint(Vec2(0,0));
// 				Lable_starLevel->setPosition(Vec2(8,6));
// 				Lable_starLevel->setName("Lable_starLevel");
// 				m_pLayer->addChild(Lable_starLevel);
// 
// 				ImageView *ImageView_star = ImageView::create();
// 				ImageView_star->loadTexture("res_ui/star_on.png");
// 				ImageView_star->setAnchorPoint(Vec2(0,0));
// 				ImageView_star->setPosition(Vec2(8+Lable_starLevel->getContentSize().width,8));
// 				ImageView_star->setVisible(true);
// 				ImageView_star->setScale(0.5f);
// 				m_pLayer->addChild(ImageView_star);
// 			}
// 		}

		const char * str_days = StringDataManager::getString("SignMonthly_curdays");
		char s_days [30];
		sprintf(s_days,str_days,curOneMonthSighGift->day());

		auto l_dayIndex = Label::createWithTTF(s_days, APP_FONT_NAME, 16);
		l_dayIndex->setAnchorPoint(Vec2(.5f,.5f));
		l_dayIndex->setPosition(Vec2(35,13));
		m_pLayer->addChild(l_dayIndex);

		if (curOneMonthSighGift->viplevel()>0)
		{
			auto image_vipLevel = ImageView::create();
			image_vipLevel->setAnchorPoint(Vec2(0.5f,0.5f));
			image_vipLevel->setPosition(Vec2(21,66));
			m_pLayer->addChild(image_vipLevel);
			switch(curOneMonthSighGift->viplevel())
			{
			case 1:
				{
					image_vipLevel->loadTexture("res_ui/monthLogon/vip1double.png");
				}
				break;
			case 2:
				{
					image_vipLevel->loadTexture("res_ui/monthLogon/vip2double.png");
				}
				break;
			case 3:
				{
					image_vipLevel->loadTexture("res_ui/monthLogon/vip3double.png");
				}
				break;
			case 4:
				{
					image_vipLevel->loadTexture("res_ui/monthLogon/vip4double.png");
				}
				break;
			case 5:
				{
					image_vipLevel->loadTexture("res_ui/monthLogon/vip5double.png");
				}
				break;
			}
		}

		RefreshUIByStatus(curOneMonthSighGift->status());
		this->setContentSize(image_Frame->getContentSize());

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(SignMonthlyItem::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(SignMonthlyItem::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(SignMonthlyItem::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(SignMonthlyItem::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void SignMonthlyItem::onEnter()
{
	UIScene::onEnter();
}

void SignMonthlyItem::onExit()
{
	UIScene::onExit();
}

bool SignMonthlyItem::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void SignMonthlyItem::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void SignMonthlyItem::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void SignMonthlyItem::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void SignMonthlyItem::GoodItemEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (curOneMonthSighGift->status() == 2)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5149, (void *)curOneMonthSighGift->day());
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase) == NULL)
			{
				auto size = Director::getInstance()->getVisibleSize();
				auto goodsItemInfoBase = GoodsItemInfoBase::create(curGoods, GameView::getInstance()->EquipListItem, 0);
				goodsItemInfoBase->setIgnoreAnchorPointForPosition(false);
				goodsItemInfoBase->setAnchorPoint(Vec2(0.5f, 0.5f));
				//goodsItemInfoBase->setPosition(Vec2(size.width/2,size.height/2));
				goodsItemInfoBase->setTag(kTagGoodsItemInfoBase);
				GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void SignMonthlyItem::RefreshUIByStatus( int status )
{
	if (m_pLayer->getChildByName("image_got"))
	{
		m_pLayer->getChildByName("image_got")->removeFromParent();
	}

	switch(status)
	{
	case 1:     //������ȡ���
		{
			image_Frame->loadTexture("res_ui/didd.png");
		}
		break;
	case 2:   //����ȡ
		{
			image_Frame->loadTexture("res_ui/didd.png");
		}
		break;
	case 3:  //����ȡ������
		{
			image_Frame->loadTexture("res_ui/diddd.png");

			auto image_got= ImageView::create();
			image_got->loadTexture("res_ui/yilingqu.png");
			image_got->setAnchorPoint(Vec2(0.5f,0.5f));
			image_got->setPosition(Vec2(49,19));
			image_got->setName("image_got");
			m_pLayer->addChild(image_got);
		}
		break;
	}
		
}
