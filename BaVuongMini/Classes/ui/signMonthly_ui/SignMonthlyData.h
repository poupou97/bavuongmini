#ifndef _SIGNMONTHLYUI_SIGNMONTHLYDATA_H_
#define _SIGNMONTHLYUI_SIGNMONTHLYDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;
/////////////////////////////////
/**
 * ��ǩ������
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.12.15
 */

class COneMouthSignGift;

class SignMonthlyData
{
public:
	SignMonthlyData(void);
	~SignMonthlyData(void);

public: 
	static SignMonthlyData * s_signMonthlyData;
	static SignMonthlyData * getInstance();

	void clearSignGiftData();

public:
	std::vector<COneMouthSignGift*> m_oneMonthSighGiftData;

	int m_nCurMonth;
	int m_nSignedNum;
	std::string m_strGeneralId;
	bool m_bCanGetGift;
};

#endif
