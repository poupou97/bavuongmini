#ifndef _RECHARGE_H_
#define _RECHARGE_H_

#include "../../ui/extensions/UIScene.h"
#include "network\HttpClient.h"

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
//class CLableGoods;
//class GoodsInfo;

class RechargeUI : public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	RechargeUI();
	~RechargeUI();

	static RechargeUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void RechargeEvent(Ref *pSender, Widget::TouchEventType type);
	void VipEvent(Ref *pSender, Widget::TouchEventType type);
	void IndexChangedEvent(Ref* pSender);

	virtual void update(float dt);

private:
	Layout * ppanel;
	Layer * u_layer;
	UITab * mainTab;
	Text * l_ingot_value;

	ui::ImageView * m_imageView_horn;

	std::string m_strMarquee;

	TableView * m_tableView;
public:
	int curLableId;

private:
	//  LiuLiang++
	void callBackCounter(Ref *obj);
    
public:
    void requestPay();
    void onHttpResponsePay(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
};

#endif

