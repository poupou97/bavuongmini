#include "Recharge.h"


#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "../Classes/loadscene_state/LoadSceneState.h"
#include "../Classes/GameView.h"
#include "../Classes/ui/extensions/UITab.h"
#include "../Classes/messageclient/element/CLable.h"
#include "../Classes/messageclient/element/CLableGoods.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/gamescene_state/MainScene.h"
#include "../Classes/messageclient/element/GoodsInfo.h"
#include "../Classes/utils/StrUtils.h"
#include "../Classes/ui/vip_ui/VipDetailUI.h"
#include "../Classes/ui/vip_ui/VipEveryDayRewardsUI.h"
#include "../Classes/ui/vip_ui/FirstBuyVipUI.h"
#include "../Classes/ui/vip_ui/VipData.h"
#include "RechargeCellItem.h"
#include "../Classes/ui/extensions/Counter.h"
#include "../Classes/utils/GameConfig.h"

#include "WmComPlatform.h"
#include "ExtenalClass.h"
#include "../Classes/login_state/Login.h"
#include "../Classes/login_state/LoginState.h"
#include "../Classes/utils/GameConfig.h"

#else

#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "../extensions/UITab.h"
#include "../../messageclient/element/CLable.h"
#include "../../messageclient/element/CLableGoods.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StrUtils.h"
#include "../vip_ui/VipDetailUI.h"
#include "../vip_ui/VipEveryDayRewardsUI.h"
#include "../vip_ui/FirstBuyVipUI.h"
#include "../vip_ui/VipData.h"
#include "RechargeCellItem.h"
#include "../extensions/Counter.h"
#include "../../utils/GameConfig.h"

#endif



#define MARQUEE_LABLE_WIDTH 596
#define GOLD_STORE_MAX_FILTER 6
#define HIGHLIGHT_FIRST_TAG 100
#define HIGHLIGHT_SECOND_TAG 101

RechargeUI::RechargeUI():
curLableId(0),
m_strMarquee("")
{
}


RechargeUI::~RechargeUI()
{

}

RechargeUI* RechargeUI::create()
{
	auto rechargeUI = new RechargeUI();
	if (rechargeUI && rechargeUI->init())
	{
		rechargeUI->autorelease();
		return rechargeUI;
	}
	CC_SAFE_DELETE(rechargeUI);
	return rechargeUI;
}

bool RechargeUI::init()
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		auto mengban = ui::ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(800, 480));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		//���UI
		if(LoadSceneLayer::RechargePanel->getParent() != NULL)
		{
			LoadSceneLayer::RechargePanel->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::RechargePanel;
		ppanel->getVirtualRenderer()->setContentSize(Size(800, 480));
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		//عرհ�ť
		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(RechargeUI::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);

		auto Button_recharge = (Button*)Helper::seekWidgetByName(ppanel,"Button_buy_0");
		Button_recharge->setTouchEnabled(true);
		Button_recharge->addTouchEventListener(CC_CALLBACK_2(RechargeUI::RechargeEvent, this));
		Button_recharge->setPressedActionEnabled(true);
		bool anyEnabled = GameConfig::getBoolForKey("recharge_any_enable");
		if(anyEnabled)
		{
			Button_recharge->setVisible(true);
		}
		else
		{
			Button_recharge->setVisible(false);
		}

		auto Button_vip = (Button*)Helper::seekWidgetByName(ppanel,"Button_buy");
		Button_vip->setTouchEnabled(true);
		Button_vip->addTouchEventListener(CC_CALLBACK_2(RechargeUI::VipEvent, this));
		Button_vip->setPressedActionEnabled(true);
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if(vipEnabled)
		{
			Button_vip->setVisible(true);
		}
		else
		{
			Button_vip->setVisible(false);
		}

		l_ingot_value = (Text*)Helper::seekWidgetByName(ppanel,"Label_myIngotValue");
		char str_ingot[20];
		sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
		l_ingot_value->setString(str_ingot);
		
		auto label_first = (Text*)Helper::seekWidgetByName(ppanel,"Label_1293");
		std::string strLabel;
		strLabel.append(StringDataManager::getString("RechargeUI_First_front"));
		memset(str_ingot, 0, sizeof(str_ingot));
		int a = atoi(VipValueConfig::s_vipValue[1].c_str());
		sprintf(str_ingot,"%d",a*100);
		strLabel.append(str_ingot);
		strLabel.append(StringDataManager::getString("RechargeUI_First_back"));
		label_first->setString(strLabel.c_str());
		
		auto label_second = (Text*)Helper::seekWidgetByName(ppanel,"Label_1293_0");
		label_second->setString(StringDataManager::getString("RechargeUI_Second"));

		if(vipEnabled)
		{
			label_first->setVisible(true);
			label_second->setVisible(true);
		}
		else
		{
			label_first->setVisible(false);
			label_second->setVisible(false);
		}

		m_tableView = TableView::create(this,Size(520,236));
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0.5f,0.5f));
		m_tableView->setPosition(Vec2(140,80));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		u_layer->addChild(m_tableView);
		m_tableView->reloadData();
		
		auto pFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV5_dikuang1_miaobian2.png");
		pFrame->setAnchorPoint(Vec2(0,0));
		pFrame->setPosition(Vec2(128,80));
		pFrame->setPreferredSize(Size(543,243));
		pFrame->setCapInsets(Rect(32,32,1,1));
		u_layer->addChild(pFrame, 2);
	
		//this->scheduleUpdate();

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(RechargeUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(RechargeUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(RechargeUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(RechargeUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void RechargeUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RechargeUI::onExit()
{
	UIScene::onExit();
}

bool RechargeUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void RechargeUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void RechargeUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void RechargeUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void RechargeUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void RechargeUI::callBackCounter(Ref *obj)
{
	auto priceCounter =(Counter *)obj;
	int price = priceCounter->getInputNum();
	CCLOG("to SDK recharge");

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	if(price > 100000)
	{
		NSString * strLoginTip = @"����ʽ�����100000ޣ��������";
		const char * charLoginTip = [strLoginTip UTF8String];
		GameView::getInstance()->showAlertDialog(charLoginTip);

		return ;
	}

	//RechargeCellItem::s_nCharge = nCharge;
	[ExtenalClass set_Charge:price];

	requestPay();

#endif

}

void RechargeUI::RechargeEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		// Գ�ֵ���ƿ��
		bool bChargeEnabled = GameConfig::getBoolForKey("charge_enable");
		if (true == bChargeEnabled)
		{
			GameView::getInstance()->showCounter(this, callfuncO_selector(RechargeUI::callBackCounter));
		}
		else
		{
			const char * charChargeEnabled = StringDataManager::getString("RechargeUI_disable");
			GameView::getInstance()->showAlertDialog(charChargeEnabled);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RechargeUI::VipEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto winSize = Director::getInstance()->getVisibleSize();
		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		auto vipScene = (Layer*)mainscene->getChildByTag(kTagVipDetailUI);
		if (vipScene == NULL)
		{
			auto vipDetailUI = (VipDetailUI*)VipData::vipDetailUI;
			mainscene->addChild(vipDetailUI, 0, kTagVipDetailUI);
			vipDetailUI->setIgnoreAnchorPointForPosition(false);
			vipDetailUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			vipDetailUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RechargeUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void RechargeUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void RechargeUI::tableCellTouched( TableView* table, TableViewCell* cell )
{

}

cocos2d::Size RechargeUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	//return Size(444,120);
	return Size(510, 76);
}

cocos2d::extension::TableViewCell* RechargeUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();
	
	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create("res_ui/kuang_0_on.png");
	pHighlightSpr->setCapInsets(Rect(7, 7, 1, 1));
	pHighlightSpr->setPreferredSize(Size(257,71));
	pHighlightSpr->setAnchorPoint(Vec2(0, 0));
	pHighlightSpr->setVisible(false);
	pHighlightSpr->setTag(HIGHLIGHT_FIRST_TAG);
	cell->addChild(pHighlightSpr);
		
	auto pHighlightSprSecond = cocos2d::extension::Scale9Sprite::create("res_ui/kuang_0_on.png");
	pHighlightSprSecond->setCapInsets(Rect(7, 7, 1, 1));
	pHighlightSprSecond->setPreferredSize(Size(257,71));
	pHighlightSprSecond->setAnchorPoint(Vec2(0, 0));
	pHighlightSprSecond->setVisible(false);
	pHighlightSprSecond->setPosition(Vec2(262, 0));
	pHighlightSprSecond->setTag(HIGHLIGHT_SECOND_TAG);
	cell->addChild(pHighlightSprSecond);
		
	if (idx == RechargeConfig::s_rechargeValue.size()/2)
	{
		if (RechargeConfig::s_rechargeValue.size()%2 == 0)
		{
			auto cellItem1 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+1]);
			cellItem1->setPosition(Vec2(0,0));
			cellItem1->setTableView(m_tableView);
			cell->addChild(cellItem1);

			auto cellItem2 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+2]);
			cellItem2->setPosition(Vec2(262,0));
			cellItem2->setTableView(m_tableView);
			cell->addChild(cellItem2);
		}
		else
		{
			auto cellItem1 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+1]);
			cellItem1->setPosition(Vec2(0,0));
			cellItem1->setTableView(m_tableView);
			cell->addChild(cellItem1);
		}
	}
	else
	{
		auto cellItem1 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+1]);
		cellItem1->setPosition(Vec2(0,0));
		cellItem1->setTableView(m_tableView);
		cell->addChild(cellItem1);

		auto cellItem2 = RechargeCellItem::create(RechargeConfig::s_rechargeValue[2*idx+2]);
		cellItem2->setPosition(Vec2(262,0));
		cellItem2->setTableView(m_tableView);
		cell->addChild(cellItem2);
	}
	
	return cell;
}

ssize_t RechargeUI::numberOfCellsInTableView( TableView *table )
{
	if (RechargeConfig::s_rechargeValue.size()%2 == 0)
	{
		return RechargeConfig::s_rechargeValue.size()/2;
	}
	else
	{
		return RechargeConfig::s_rechargeValue.size()/2 + 1;
	}
}

void RechargeUI::update(float dt)
{
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	l_ingot_value->setString(str_ingot);
}
