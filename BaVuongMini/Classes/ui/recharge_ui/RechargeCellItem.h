#ifndef  _RECHARGEUI_RECHARGECELL_H_
#define _RECHARGEUI_RECHARGECELL_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "network\HttpClient.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CLableGoods;
class CRechargeInfo;

class RechargeCellItem : public UIScene
{
public:
	RechargeCellItem();
	~RechargeCellItem();

	static RechargeCellItem* create(CRechargeInfo * rechargeInfo);
	bool init(CRechargeInfo * rechargeInfo);

	void BuyEvent(Ref *pSender, Widget::TouchEventType type);
	void GoodItemEvent(Ref *pSender);
	void setTableView(TableView* tableView){m_tableView = tableView;}
private:
	CRechargeInfo * curRechargeInfo;
	int orderId;
	TableView* m_tableView;
    static int curOrderId;
	void setHighLightFrameVisible(int orderId, bool mode);
public:
    void requestPay();
    void onHttpResponsePay(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
    
    //static int s_nCharge;           // 单位是元
};

#endif

