#include "RechargeCellItem.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "../Classes/messageclient/element/CLableGoods.h"
#include "../Classes/ui/extensions/CCMoveableMenu.h"
#include "../Classes/messageclient/element/GoodsInfo.h"
#include "../Classes/loadscene_state/LoadSceneState.h"
#include "../Classes/utils/StaticDataManager.h"
#include "../Classes/gamescene_state/GameSceneState.h"
#include "../Classes/GameView.h"
#include "AppMacros.h"
#include "../Classes/utils/GameUtils.h"
#include "../Classes/ui/backpackscene/EquipmentItem.h"
#include "../Classes/ui/backpackscene/GoodsItemInfoBase.h"
#include "../Classes/messageclient/element/CRechargeInfo.h"
#include "../Classes/utils/GameConfig.h"
#include "../Classes/ui/recharge_ui/Recharge.h"


#include "WmComPlatform.h"
#include "ExtenalClass.h"

#else

#include "../../messageclient/element/CLableGoods.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../GameView.h"
#include "AppMacros.h"
#include "../../utils/GameUtils.h"
#include "../../ui/backpackscene/EquipmentItem.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"
#include "../../messageclient/element/CRechargeInfo.h"
#include "../../utils/GameConfig.h"
#include "Recharge.h"

#endif



#define HIGHLIGHT_FIRST_TAG 100
#define HIGHLIGHT_SECOND_TAG 101

int RechargeCellItem::curOrderId = 1;

RechargeCellItem::RechargeCellItem()
{
}


RechargeCellItem::~RechargeCellItem()
{
	delete curRechargeInfo;
}

RechargeCellItem* RechargeCellItem::create( CRechargeInfo * rechargeInfo )
{
	auto rechargeCellItem = new RechargeCellItem();
	if (rechargeCellItem && rechargeCellItem->init(rechargeInfo))
	{
		rechargeCellItem->autorelease();
		return rechargeCellItem;
	}
	CC_SAFE_DELETE(rechargeCellItem);
	return NULL;
}

bool RechargeCellItem::init( CRechargeInfo * rechargeInfo )
{
	if (UIScene::init())
	{
		/////////////////////////////////////////////////////////////////////////////////////

		// �Layer�λ�TableView�֮�ڣ�Ϊ����ȷ��ӦTableView���϶��¼������Ϊ���̵����¼
		//m_pLayer->setSwallowsTouches(false);

		curRechargeInfo = new CRechargeInfo(*rechargeInfo);

		orderId = curRechargeInfo->get_orderId();

		auto btn_buy  = Button::create();
		btn_buy->loadTextures("res_ui/new_button_10.png","res_ui/new_button_10.png","");
		btn_buy->setTouchEnabled(true);
		btn_buy->setPressedActionEnabled(true);
		btn_buy->setScale9Enabled(true);
		btn_buy->setContentSize(Size(257,71));
		//btn_buy->setCapInsets(Rect(18,9,2,23));
		btn_buy->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_buy->setPosition(Vec2(129,38));
		btn_buy->addTouchEventListener(CC_CALLBACK_2(RechargeCellItem::BuyEvent, this));
		m_pLayer->addChild(btn_buy);

		auto imageViewLight = ui::ImageView::create();
		imageViewLight->loadTexture("res_ui/Prepaid/light_round.png");
		imageViewLight->setScale9Enabled(true);
		imageViewLight->setContentSize(Size(73,74));
		imageViewLight->setAnchorPoint(Vec2(0.5f,0.5f));
		imageViewLight->setPosition(Vec2(-84,-2));
		btn_buy->addChild(imageViewLight);

		auto imageViewGold = ui::ImageView::create();
		std::string strGold;
		strGold.append("res_ui/Prepaid/");
		strGold.append(curRechargeInfo->get_icon());
		strGold.append(".png");
		imageViewGold->loadTexture(strGold.c_str());
		imageViewGold->setScale9Enabled(true);
		imageViewGold->setContentSize(Size(64,55));
		imageViewGold->setAnchorPoint(Vec2(0.5f,0.5f));
		imageViewGold->setPosition(Vec2(0,3));
		imageViewLight->addChild(imageViewGold);
		
		auto imageViewFrame = ui::ImageView::create();
		imageViewFrame->loadTexture("res_ui/di_yy.png");
		imageViewFrame->setScale9Enabled(true);
		//imageViewFrame->setContentSize(Size(141,27));
		imageViewFrame->setContentSize(Size(91,27));
		imageViewFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		imageViewFrame->setPosition(Vec2(-4,13));
		btn_buy->addChild(imageViewFrame);
		
		auto imageViewGoldMini = ui::ImageView::create();
		imageViewGoldMini->loadTexture("res_ui/ingot.png");
		//imageViewGoldMini->setScale9Enabled(true);
		//imageViewGoldMini->setContentSize(Size(23,15));
		imageViewGoldMini->setAnchorPoint(Vec2(0.5f,0.5f));
		imageViewGoldMini->setPosition(Vec2(-32,0));
		imageViewFrame->addChild(imageViewGoldMini);
		
		
		char aucBuf[6];
		memset(aucBuf, 0, sizeof(aucBuf));
		sprintf(aucBuf, "%d", curRechargeInfo->get_recharge_value());
		auto goldSum = Label::createWithTTF(aucBuf, APP_FONT_NAME, 18);
		goldSum->setAnchorPoint(Vec2(0,0.5f));
		goldSum->setPosition(Vec2(-19,0));
		goldSum->setColor(Color3B(255, 255, 255));
		imageViewFrame->addChild(goldSum);
		
		auto imageViewFrameRMB = ui::ImageView::create();
		imageViewFrameRMB->loadTexture("res_ui/di_yy.png");
		imageViewFrameRMB->setScale9Enabled(true);
		//imageViewFrame->setContentSize(Size(141,27));
		imageViewFrameRMB->setContentSize(Size(69,27));
		imageViewFrameRMB->setAnchorPoint(Vec2(0.5f,0.5f));
		imageViewFrameRMB->setPosition(Vec2(78,13));
		btn_buy->addChild(imageViewFrameRMB);
		
		auto imageViewRMB = ui::ImageView::create();
		imageViewRMB->loadTexture("res_ui/vip/qian2.png");
		//imageViewRMB->setScale9Enabled(true);
		//imageViewRMB->setContentSize(Size(15,17));
		imageViewRMB->setAnchorPoint(Vec2(0.5f,0.5f));
		imageViewRMB->setPosition(Vec2(22,0));
		imageViewFrameRMB->addChild(imageViewRMB);
		
		memset(aucBuf, 0, sizeof(aucBuf));
		int a = atoi(VipValueConfig::s_vipValue[2].c_str())*100;
		sprintf(aucBuf, "%d", curRechargeInfo->get_recharge_value()/a);
		auto moneySum = Label::createWithTTF(aucBuf, APP_FONT_NAME, 18);
		moneySum->enableOutline(Color4B::BLACK, 2.0f);

		moneySum->setAnchorPoint(Vec2(1,0.5f));
		moneySum->setPosition(Vec2(15,0));
		moneySum->setColor(Color3B(255, 255, 255));
		imageViewFrameRMB->addChild(moneySum);

		char aucLable[6];
		memset(aucLable, 0, sizeof(aucLable));
		sprintf(aucLable,"%d",curRechargeInfo->get_send_value());
		std::string strLabel;
		strLabel.append(StringDataManager::getString("RechargeUI_SendLabel_front"));
		strLabel.append(aucLable);
		strLabel.append(StringDataManager::getString("RechargeUI_SendLabel_back"));

		auto sendValue = Label::createWithBMFont("res_ui/font/ziti_3.fnt",strLabel.c_str());
		//sendValue->setScale(0.7f);
		sendValue->setAnchorPoint(Vec2(0.5f,0.5f));
		sendValue->setPosition(Vec2(34,-17));
		btn_buy->addChild(sendValue);
		if(!curRechargeInfo->get_send_value())
		{
			sendValue->setVisible(false);
		}
		
		auto imageViewSuggest = ui::ImageView::create();
		imageViewSuggest->loadTexture("res_ui/Prepaid/tuijian.png");
		//imageViewSuggest->setScale9Enabled(true);
		//imageViewSuggest->setContentSize(Size(20,49));
		imageViewSuggest->setAnchorPoint(Vec2(0.5f,0.5f));
		imageViewSuggest->setPosition(Vec2(12,64));
		m_pLayer->addChild(imageViewSuggest);
		switch(curRechargeInfo->get_suggest())
		{
		case 0:
			imageViewSuggest->setVisible(false);
			break;
		case 1:
			imageViewSuggest->setVisible(true);
			break;
		default:
			break;
		}

		this->setContentSize(Size(241,76));

		return true;
	}
	return false;
}

void RechargeCellItem::BuyEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//remove last highlight frame
		//setHighLightFrameVisible(curOrderId, false);
		//add current highlight frame
		//setHighLightFrameVisible(orderId, true);

		curOrderId = orderId;

		// ��ֵ���ƿ��
		bool bChargeEnabled = GameConfig::getBoolForKey("charge_enable");
		if (true == bChargeEnabled)
		{
			CCLOG("to SDK recharge");

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

			CRechargeInfo * pClickRechargeInfo = this->curRechargeInfo;
			// �Ԫ���
			int nRechargeValue = pClickRechargeInfo->get_recharge_value();
			// �۸�Ԫ��
			int nCharge = nRechargeValue / 100;

			//RechargeCellItem::s_nCharge = nCharge;
			[ExtenalClass set_Charge : nCharge];

			// ֧��
			requestPay();

#endif
		}
		else
		{
			const char * charChargeEnabled = StringDataManager::getString("RechargeUI_disable");
			GameView::getInstance()->showAlertDialog(charChargeEnabled);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RechargeCellItem::setHighLightFrameVisible(int orderId, bool mode)
{
	auto cell = m_tableView->cellAtIndex((orderId-1)/2);
	if(1 == orderId%2)
	{
		auto highLightLast = (cocos2d::extension::Scale9Sprite*)cell->getChildByTag(HIGHLIGHT_FIRST_TAG);
		highLightLast->setVisible(mode);
	}
	else
	{
		auto highLightLast = (cocos2d::extension::Scale9Sprite*)cell->getChildByTag(HIGHLIGHT_SECOND_TAG);
		highLightLast->setVisible(mode);
	}
}