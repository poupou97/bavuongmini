#include "PickUI.h"
#include "../../messageclient/element/CPickingInfo.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CPushCollectInfo.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"

PickUI::PickUI():
m_nCount(0)
{
}


PickUI::~PickUI()
{
}

PickUI* PickUI::create(CPickingInfo* pickingInfo)
{
	auto pickUI = new PickUI();
	if(pickUI && pickUI->init(pickingInfo))
	{
		pickUI->autorelease();
		return pickUI;
	}
	CC_SAFE_DELETE(pickUI);
	return NULL;
}

PickUI* PickUI::create( float time )
{
	auto pickUI = new PickUI();
	if(pickUI && pickUI->init(time))
	{
		pickUI->autorelease();
		return pickUI;
	}
	CC_SAFE_DELETE(pickUI);
	return NULL;
}

bool PickUI::init(CPickingInfo* pickingInfo)
{
	if (UIScene::init())
	{
// 		Sprite * bg = Sprite::create("gamescene_state/caiji/caiji_di.png");
// 		bg->setIgnoreAnchorPointForPosition(false);
// 		bg->setAnchorPoint(Vec2(0,0));
// 		bg->setPosition(Vec2::ZERO);
// 		addChild(bg);
		auto bg = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/caiji/caiji_di.png");
		bg->setPreferredSize(Size(187,32));
		bg->setCapInsets(Rect(11,11,1,1));
		bg->setIgnoreAnchorPointForPosition(false);
		bg->setAnchorPoint(Vec2(0,0));
		bg->setPosition(Vec2::ZERO);
		addChild(bg);

		auto spriteIcon = Sprite::create("gamescene_state/zhujiemian3/caiji/caiji_jindu.png");
		spriteIcon->setIgnoreAnchorPointForPosition(false);
		spriteIcon->setAnchorPoint(Vec2(0,0));
		auto progressTimer = ProgressTimer::create(spriteIcon);
		progressTimer->setAnchorPoint(Vec2(0.5f,0.5f));
		progressTimer->setPosition(Vec2(bg->getContentSize().width/2,bg->getContentSize().height/2));
		//���ý����������� 
		progressTimer->setMidpoint(Vec2(0,0)); 
		//����ý�ȶ�������Ϊ����������������֡� 
		progressTimer->setBarChangeRate(Vec2(1, 0)); 
		this->addChild(progressTimer);  
		ActionInterval* action_progress_from_to;
		CallFunc* action_callback;

		progressTimer->setType(ProgressTimer::Type::BAR);

		action_progress_from_to = ProgressFromTo::create(pickingInfo->actionTime/1000, 0, 100);     
		action_callback = CallFuncN::create(CC_CALLBACK_1(PickUI::FinishedCallBack,this));
		progressTimer->runAction(Sequence::create(action_progress_from_to, action_callback, NULL));

		auto pickingActor = dynamic_cast<PickingActor*>(GameView::getInstance()->myplayer->getLockedActor());
		if (pickingActor)
		{
			//Label * label_description = Label::createWithTTF(pickingInfo->description.c_str(),APP_FONT_NAME,14);
			auto label_description = Label::createWithTTF(pickingActor->p_collectInfo->actiondescription().c_str(),APP_FONT_NAME,14);
			label_description->setAnchorPoint(Vec2(0.5f,0.5f));
			label_description->setPosition(Vec2(bg->getContentSize().width/2,bg->getContentSize().height/2));
			auto shadowColor = Color4B::BLACK;   // black
			label_description->enableShadow(shadowColor,  Size(1.0f, -1.0f), 1.0f);

			auto repeapAction = RepeatForever::create(
				Sequence::create(
					ScaleTo::create(0.9f, 1.10f),
					ScaleTo::create(0.7f, 1.0f),
					NULL));
			label_description->runAction(repeapAction);

			this->addChild(label_description);
		}

		auto adorn_left = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/aa.png");
		adorn_left->setAnchorPoint(Vec2(0.5f,0.5f));
		adorn_left->setPosition(Vec2(4,6));
		adorn_left->setFlippedX(true);
		addChild(adorn_left);

		auto adorn_right = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/aa.png");
		adorn_right->setAnchorPoint(Vec2(0.5f,0.5f));
		adorn_right->setPosition(Vec2(182,6));
		addChild(adorn_right);
		

		this->setContentSize(progressTimer->getContentSize());

		return true;
	}
	return false;
}

bool PickUI::init( float time )
{
	if (UIScene::init())
	{
		// 		Sprite * bg = Sprite::create("gamescene_state/caiji/caiji_di.png");
		// 		bg->setIgnoreAnchorPointForPosition(false);
		// 		bg->setAnchorPoint(Vec2(0,0));
		// 		bg->setPosition(Vec2::ZERO);
		// 		addChild(bg);
		auto bg = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/caiji/caiji_di.png");
		bg->setPreferredSize(Size(187,32));
		bg->setCapInsets(Rect(11,11,1,1));
		bg->setIgnoreAnchorPointForPosition(false);
		bg->setAnchorPoint(Vec2(0,0));
		bg->setPosition(Vec2::ZERO);
		addChild(bg);

		auto spriteIcon = Sprite::create("gamescene_state/zhujiemian3/caiji/caiji_jindu.png");
		spriteIcon->setIgnoreAnchorPointForPosition(false);
		spriteIcon->setAnchorPoint(Vec2(0,0));
		auto progressTimer = ProgressTimer::create(spriteIcon);
		progressTimer->setAnchorPoint(Vec2(0.5f,0.5f));
		progressTimer->setPosition(Vec2(bg->getContentSize().width/2,bg->getContentSize().height/2));
		//���ý����������� 
		progressTimer->setMidpoint(Vec2(0,0)); 
		//����ý�ȶ�������Ϊ����������������֡� 
		progressTimer->setBarChangeRate(Vec2(1, 0)); 
		this->addChild(progressTimer);  
		ActionInterval* action_progress_from_to;
		CallFunc* action_callback;

		progressTimer->setType(ProgressTimer::Type::BAR);

		action_progress_from_to = ProgressFromTo::create(time / 1000, 0, 100);     
		action_callback = CallFuncN::create(CC_CALLBACK_1(PickUI::FinishedCallBack,this));
		progressTimer->runAction(Sequence::create(action_progress_from_to, action_callback, NULL));

		//PickingActor * pickingActor = dynamic_cast<PickingActor*>(GameView::getInstance()->myplayer->getLockedActor());
		//if (pickingActor)
		{
			//Label * label_description = Label::createWithTTF(pickingInfo->description.c_str(),APP_FONT_NAME,14);
			//const char * charDescription = "the presentBox is collecting!";
			const char * charDescription = StringDataManager::getString("PresentBox_openPresentBox");
			auto label_description = Label::createWithTTF(charDescription,APP_FONT_NAME,14);
			label_description->setAnchorPoint(Vec2(0.5f,0.5f));
			label_description->setPosition(Vec2(bg->getContentSize().width/2,bg->getContentSize().height/2));
			auto shadowColor = Color4B::BLACK;   // black
			label_description->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
			this->addChild(label_description);
		}

		auto adorn_left = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/aa.png");
		adorn_left->setAnchorPoint(Vec2(0.5f,0.5f));
		adorn_left->setPosition(Vec2(4,6));
		adorn_left->setFlippedX(true);
		addChild(adorn_left);

		auto adorn_right = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/aa.png");
		adorn_right->setAnchorPoint(Vec2(0.5f,0.5f));
		adorn_right->setPosition(Vec2(182,6));
		addChild(adorn_right);


		this->setContentSize(progressTimer->getContentSize());

		return true;
	}
	return false;
}

void PickUI::update( float dt )
 {
	 
 }
// void PickUI::update( float dt )
// {
// 	m_nCount++;
// 	if (m_nCount == 100)
// 	{
// 		this->removeFromParent();
// 	}
// 	else if (m_nCount > 100)
// 	{
// 		m_nCount = 0;
// 	}
// 
// 	UILoadingBar* loadingBar = dynamic_cast<UILoadingBar*>(m_pLayer->getChildByName("LoadingBar"));
// 	loadingBar->setPercent(m_nCount);

// }

void PickUI::FinishedCallBack(Node* node)
{
	CCLOG("loading finished");
	this->removeFromParent();
}
