#include "UITest.h"
#include "AppMacros.h"

UITest::UITest()
{
	//setTouchEnabled(true);
	//setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

	Size s = Director::getInstance()->getVisibleSize();

	// 只是简单获取一下图形大小
	auto  tmp = Sprite::create("zsg_ui/diban1.png");
	auto size = tmp->getContentSize();
    Rect fullRect = Rect(0,0, size.width, size.height);
    //Rect insetRect = Rect(2,19,268,29);
	Rect insetRect = Rect(103,42,107,29);
    CCLOG("wh:%f,%f", size.width, size.height);
    tmp->release();
	// 调用cocos2d::extension::Scale9Sprite
	auto backGround = cocos2d::extension::Scale9Sprite::create("zsg_ui/diban1.png", fullRect, insetRect );
	backGround->setPreferredSize(Size(designResolutionSize.width, designResolutionSize.height));
    //backGround->setPosition(Vec2(10, 230));
	//backGround->setPosition(Vec2(s.width/2, s.height/2));
	backGround->setPosition(Vec2(0, 0));
	//backGround->setPosition(Vec2(0, s.height));
    backGround->setAnchorPoint(Vec2::ZERO);
	//backGround->setAnchorPoint(Vec2(0.5f, 0.5f));
	//backGround->setAnchorPoint(Vec2(0, 1));
    addChild(backGround);

	//this->setAnchorPoint(Vec2(0.f, 0.f));
	setContentSize(Size(designResolutionSize.width, designResolutionSize.height));

	// content
	auto pSprite = Sprite::create("zsg_ui/huoyuedu.png");
	pSprite->setAnchorPoint(Vec2(0, 0));
    pSprite->setPosition(Vec2(5, 5));
    addChild(pSprite);

	pSprite = Sprite::create("zsg_ui/tab.png");
	pSprite->setAnchorPoint(Vec2(0, 1));
	pSprite->setPosition(Vec2(25, designResolutionSize.height - 5));
    addChild(pSprite);

	// close button
	auto pButtonItem = MenuItemImage::create(
                                "zsg_ui/close.png",
                                "zsg_ui/close.png",
		CC_CALLBACK_1(UITest::menuCloseCallback,this));
    // create menu, it's an autorelease object
	auto pMenu = Menu::create(pButtonItem, NULL);
	//pMenu->setIgnoreAnchorPointForPosition(false);
	//pMenu->setAnchorPoint(Vec2(0, 0));
	pMenu->setPosition(Vec2(designResolutionSize.width - 28, designResolutionSize.height - 28));
    this->addChild(pMenu);

	auto touchListener = EventListenerTouchOneByOne::create();
	touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(UITest::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(UITest::onTouchEnded, this);
	touchListener->onTouchMoved = CC_CALLBACK_2(UITest::onTouchMoved, this);
	touchListener->onTouchCancelled = CC_CALLBACK_2(UITest::onTouchCancelled, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
}

UITest::~UITest()
{

}

void UITest::onEnter()
{
    Layer::onEnter();

	// Spawn
	auto action = Sequence::create(
		ScaleTo::create(0.15f*1/2,1.1f),
		ScaleTo::create(0.15f*1/3,1.0f),
		NULL);

	runAction(action);
}

void UITest::onExit()
{
    Layer::onExit();
}

bool UITest::onTouchBegan(Touch *touch, Event * pEvent)
{
	// convert the touch point to OpenGL coordinates
	Vec2 location = touch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}
	//this->removeFromParent();
	menuCloseCallback(NULL);
    return false;
}

void UITest::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void UITest::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void UITest::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void UITest::menuCloseCallback(Ref* pSender)
{
	//this->removeFromParent();

	auto  action = Sequence::create(
		ScaleTo::create(0.15f*1/3,1.1f),
		ScaleTo::create(0.15f*1/2,1.0f),
		RemoveSelf::create(),
		NULL);

	runAction(action);
}

void UITest::menuButtonCallback(Ref* pSender)
{

}

void UITest::menuSkillCallback(Ref* pSender)
{

}
