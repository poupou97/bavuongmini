#ifndef REWARD_REMIND_UI_H 
#define REWARD_REMIND_UI_H

#include "../../ui/extensions/uiscene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

//Label is set tag of cell use get refresh time
#define REWARD_ONLINE_TIME_TAG 666
#define REWARD_MONEYTREE_TIME_TAG 667


#define CELL_NAME_SP_TAG 700
#define CELL_NAME_TAG 701
#define CELL_LAYER_TAG 777
class CRewardBase;

enum
{
	state_open = 0,
	state_visible,
	stat_show,
};

class RewardUi:public UIScene,public TableViewDataSource,public TableViewDelegate
{
public:
	RewardUi(void);
	~RewardUi(void);

	static RewardUi * create();
	bool init();
	void onEnter();
	void onExit();

	TableView * tablevie_reward;

	void callBackCheck(Ref * obj);
	void callBackGetReward(Ref *pSender, Widget::TouchEventType type);

	void updateShowCountTime(float dt);

	void callBackExit(Ref *pSender, Widget::TouchEventType type);
	static void setRewardListParticle();

	static void addRewardListEvent(int index_);
	static void removeRewardListEvent(int index);

	void update(float delta);
	int m_uiState;
	int m_openUiTag;
public:
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
public:
	Size winsize;
};

#endif