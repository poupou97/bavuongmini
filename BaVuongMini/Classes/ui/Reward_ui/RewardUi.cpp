#include "RewardUi.h"
#include "GameView.h"
#include "../../messageclient/element/CRewardBase.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../ui/GameUIConstant.h"
#include "../../ui/SignDaily_ui/SignDailyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/vip_ui/VipEveryDayRewardsUI.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../../ui/offlineExp_ui/OffLineExpUI.h"
#include "../../ui/challengeRound/RankRewardUi.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/backpackscene/BattleAchievementUI.h"
#include "../family_ui/familyFight_ui/FamilyFightRewardsUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../ui/SignDaily_ui/SignDailyData.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../ui/OnlineReward_ui/OnLineRewardUI.h"
#include "../../ui/OnlineReward_ui/OnlineGiftData.h"
#include "../../ui/OnlineReward_ui/OnlineGiftLayer.h"
#include "../../ui/Active_ui/WorldBoss_ui/GetRewardUI.h"
#include "../moneyTree_ui/MoneyTreeUI.h"
#include "../moneyTree_ui/MoneyTreeData.h"
#include "ui/signMonthly_ui/SignMonthlyUI.h"
#include "ui/signMonthly_ui/SignMonthlyData.h"
#include "cocostudio\CCSGUIReader.h"

RewardUi::RewardUi(void)
{
	m_uiState = state_open;
	m_openUiTag = 0;
}


RewardUi::~RewardUi(void)
{
}

RewardUi * RewardUi::create()
{
	auto reward_ui = new RewardUi();
	if (reward_ui && reward_ui->init())
	{
		reward_ui->autorelease();
		return reward_ui;
	}
	CC_SAFE_DELETE(reward_ui);
	return NULL;
}

bool compareCellId(CRewardBase * reward1,CRewardBase * reward2)
{
	return reward1->getClose() < reward2->getClose();
}


bool RewardUi::init()
{
	if (UIScene::init())
	{
		winsize= Director::getInstance()->getVisibleSize();

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		auto mainPanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/reward_1.json");
		mainPanel->setAnchorPoint(Vec2(0.5f,0.5f));
		mainPanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(mainPanel);

		//sort vector
		sort(GameView::getInstance()->rewardvector.begin(),GameView::getInstance()->rewardvector.end(),compareCellId);

		auto loadLayer= Layer::create();
		loadLayer->setIgnoreAnchorPointForPosition(false);
		loadLayer->setAnchorPoint(Vec2(0.5f,0.5f));
		loadLayer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		loadLayer->setContentSize(Size(600,440));
		this->addChild(loadLayer);

		tablevie_reward = TableView::create(this, Size(485,318));
		tablevie_reward->setTouchEnabled(true);
		tablevie_reward->setDirection(TableView::Direction::VERTICAL);
		tablevie_reward->setAnchorPoint(Vec2(0,0));
		tablevie_reward->setPosition(Vec2(61,40));
		tablevie_reward->setDelegate(this);
		tablevie_reward->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		loadLayer->addChild(tablevie_reward);
		tablevie_reward->reloadData();

		auto sp_ = cocos2d::extension::Scale9Sprite::create("res_ui/LV5_dikuang1_miaobian1.png");
		sp_->setPreferredSize(Size(510,330));
		sp_->setCapInsets(Rect(32,32,1,1));
		sp_->setAnchorPoint(Vec2(0,0));
		sp_->setPosition(Vec2(46,33));
		loadLayer->addChild(sp_);

		auto btnClose =(Button *)Helper::seekWidgetByName(mainPanel,"Button_close");
		btnClose->setTouchEnabled(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addTouchEventListener(CC_CALLBACK_2(RewardUi::callBackExit, this));

		this->schedule(schedule_selector(RewardUi::updateShowCountTime),1.0f);
		this->scheduleUpdate();
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchEnded = CC_CALLBACK_2(RewardUi::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(RewardUi::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(RewardUi::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void RewardUi::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RewardUi::onExit()
{
	UIScene::onExit();
}

void RewardUi::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{

}

void RewardUi::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

void RewardUi::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	
}

cocos2d::Size RewardUi::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(480,72);
}

cocos2d::extension::TableViewCell* RewardUi::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	auto cell = table->dequeueCell();
	cell = new TableViewCell();
	cell->autorelease();

	auto spbg = cocos2d::extension::Scale9Sprite::create("res_ui/kuang02_new.png");
	spbg->setPreferredSize(Size(480,69));
	spbg->setCapInsets(Rect(15,30,1,1));
	spbg->setAnchorPoint(Vec2(0,0));
	spbg->setPosition(Vec2(0,0));
	cell->addChild(spbg);
	//
	auto rewardIcon_sp = Sprite::create("res_ui/mo_5.png");
	rewardIcon_sp->setAnchorPoint(Vec2(0,0.5f));
	rewardIcon_sp->setPosition(Vec2(17,36));
	rewardIcon_sp->setScale(1.3f);
	cell->addChild(rewardIcon_sp);
	//icon
	std::string icon_ = GameView::getInstance()->rewardvector.at(idx)->getIcon();
	//std::string temp_icon_path = "res_ui/gamescene_state/zhujiemian3/tubiao";
	std::string temp_icon_path = "res_ui/reward/";
	temp_icon_path.append(icon_);
	temp_icon_path.append(".png");

	auto rewardIcon_ = Sprite::create(temp_icon_path.c_str());
	rewardIcon_->setAnchorPoint(Vec2(0,0.5f));
	rewardIcon_->setPosition(Vec2(17,36));
	cell->addChild(rewardIcon_);

	auto name_spbg =cocos2d::extension::Scale9Sprite::create("res_ui/LV4_diaa.png");
	name_spbg->setPreferredSize(Size(166,27));
	name_spbg->setAnchorPoint(Vec2(0.5f,0.5f));
	name_spbg->setPosition(Vec2(165,46));
	name_spbg->setTag(CELL_NAME_SP_TAG);
	cell->addChild(name_spbg);
	//name
	std::string temp_name = GameView::getInstance()->rewardvector.at(idx)->getName();
	auto rewardName_= Label::createWithTTF(temp_name.c_str(), APP_FONT_NAME,16);
	rewardName_->setAnchorPoint(Vec2(0.5f,0.5f));
	rewardName_->setPosition(Vec2(82,13));
	rewardName_->setTag(CELL_NAME_TAG);
	name_spbg->addChild(rewardName_);

	//add count time 
	switch(GameView::getInstance()->rewardvector.at(idx)->getId())
	{
	case REWARD_LIST_ID_ONLINEREWARD:
		{
			OnlineGiftLayer * pOnlineGiftLayer = (OnlineGiftLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftLayer);
			if (pOnlineGiftLayer != NULL)
			{
				std::string strings_ = pOnlineGiftLayer->get_timeLeft();
				
				auto rewardCount_= Label::createWithTTF(strings_.c_str(), APP_FONT_NAME,16);
				rewardCount_->setAnchorPoint(Vec2(0,0.5f));
				rewardCount_->setPosition(Vec2(name_spbg->getPositionX()+ name_spbg->getContentSize().width/2+20,46));//rewardCount_->getContentSize().width/2
				rewardCount_->setTag(REWARD_ONLINE_TIME_TAG);
				cell->addChild(rewardCount_);

				const char *str1 = StringDataManager::getString("label_timeout");
				if (strcmp(strings_.c_str(),str1) == 0)
				{
					rewardCount_->setVisible(false);
				}else
				{
					rewardCount_->setVisible(true);
				}
			}
		}break;
	case REWARD_LIST_ID_MONEYTREE:
		{
			std::string temp_moneyTree_name = GameView::getInstance()->rewardvector.at(idx)->getName();
			std::string temp_count = MoneyTreeData::instance()->get_numLeft();
			temp_moneyTree_name.append("(");
			temp_moneyTree_name.append(temp_count.c_str());
			temp_moneyTree_name.append(")");
			rewardName_->setString(temp_moneyTree_name.c_str());
			
			std::string strings_ = MoneyTreeData::instance()->get_strTimeLeft();
			auto rewardCount_= Label::createWithTTF(strings_.c_str(), APP_FONT_NAME,16);
			rewardCount_->setAnchorPoint(Vec2(0,0.5f));
			rewardCount_->setPosition(Vec2(name_spbg->getPositionX()+name_spbg->getContentSize().width/2+20,46));
			rewardCount_->setTag(REWARD_MONEYTREE_TIME_TAG);
			cell->addChild(rewardCount_);

			const char *str1 = StringDataManager::getString("MoneyTree_canGetInfo");
			if (strcmp(strings_.c_str(),str1) == 0)
			{
				GameView::getInstance()->rewardvector.at(idx)->setRewardState(1);
				rewardCount_->setVisible(false);
			}else
			{
				GameView::getInstance()->rewardvector.at(idx)->setRewardState(0);
				rewardCount_->setVisible(true);
			}
			
		}break;
	}

	//des
	std::string temp_des = GameView::getInstance()->rewardvector.at(idx)->getDes();
	auto rewardDes_= Label::createWithTTF(temp_des.c_str(), APP_FONT_NAME,14);
	rewardDes_->setAnchorPoint(Vec2(0,1));
	rewardDes_->setPosition(Vec2(80,30));
	cell->addChild(rewardDes_);

	auto layer_ = Layer::create();
	layer_->setTag(CELL_LAYER_TAG);
	cell->addChild(layer_);

	auto image_checkReward_sp = ImageView::create();
	image_checkReward_sp->loadTexture("res_ui/reward/btn_di.png");
	image_checkReward_sp->setAnchorPoint(Vec2(0.5f,0.5f));
	image_checkReward_sp->setPosition(Vec2(410,33));
	image_checkReward_sp->setName("image_checkReward_sp");
	layer_->addChild(image_checkReward_sp);

	//menu 
	auto btn_check = Button::create();
	btn_check->loadTextures("res_ui/new_button_4.png","res_ui/new_button_4.png","");
	btn_check->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_check->setScale9Enabled(true);
	btn_check->setContentSize(Size(81,40));
	btn_check->setCapInsets(Rect(18,9,2,23));
	btn_check->setTouchEnabled(true);
	btn_check->setPressedActionEnabled(true);
	btn_check->addTouchEventListener(CC_CALLBACK_2(RewardUi::callBackGetReward, this));
	btn_check->setTag(idx);
	btn_check->setName("btn_onlineCheck_Reward"); 
	btn_check->setVisible(false);
	btn_check->setPosition(Vec2(410,33));
	layer_->addChild(btn_check);
	
	const char *strings_check = StringDataManager::getString("family_apply_btncheck");
	auto label_ = Label::createWithTTF(strings_check, APP_FONT_NAME, 16);
	label_->setAnchorPoint(Vec2(0.5f,0.5f));
	label_->setPosition(Vec2(0,0));
	btn_check->addChild(label_);

	auto btn_get = Button::create();
	btn_get->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
	btn_get->setScale9Enabled(true);
	btn_get->setContentSize(Size(81,40));
	btn_get->setCapInsets(Rect(18,9,2,23));
	btn_get->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_get->setTouchEnabled(true);
	btn_get->setPressedActionEnabled(true);
	btn_get->addTouchEventListener(CC_CALLBACK_2(RewardUi::callBackGetReward, this));
	btn_get->setTag(idx);
	btn_get->setName("btn_onlineget_Reward"); 
	btn_get->setVisible(false);
	btn_get->setPosition(Vec2(410,33));
	layer_->addChild(btn_get);

	const char *strings_getReward = StringDataManager::getString("label_timeout");
	auto label_getReward = Label::createWithTTF(strings_getReward, APP_FONT_NAME, 16);
	label_getReward->setAnchorPoint(Vec2(0.5f,0.5f));
	label_getReward->setPosition(Vec2(0,0));
	btn_get->addChild(label_getReward);


	int temp_close = GameView::getInstance()->rewardvector.at(idx)->getClose();
	int temp_state = GameView::getInstance()->rewardvector.at(idx)->getRewardState();
	if (temp_close == 1 && temp_state == 0)
	{
		btn_check->setVisible(true);
		btn_get->setVisible(false);
		image_checkReward_sp->setVisible(false);
	}else
	{
		btn_check->setVisible(false);
		btn_get->setVisible(true);
		image_checkReward_sp->setVisible(true);
	}
	return cell;
}

ssize_t RewardUi::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	return GameView::getInstance()->rewardvector.size();
}

void RewardUi::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void RewardUi::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void RewardUi::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void RewardUi::callBackCheck( Ref * obj )
{

}

void RewardUi::callBackGetReward(Ref *pSender, Widget::TouchEventType type)
{	


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{


		auto btn_ = (Button *)pSender;
		int index_ = btn_->getTag();
		int id_ = GameView::getInstance()->rewardvector.at(index_)->getId();
		auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
		switch (id_)
		{
		case REWARD_LIST_ID_SIGNDAILY:
		{
			auto pSignDailyUI = (SignDailyUI  *)mainscene_->getChildByTag(kTagSignDailyUI);
			if (pSignDailyUI == NULL)
			{
				pSignDailyUI = SignDailyUI::create();
				pSignDailyUI->setIgnoreAnchorPointForPosition(false);
				pSignDailyUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				pSignDailyUI->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				pSignDailyUI->setTag(kTagSignDailyUI);
				GameView::getInstance()->getMainUIScene()->addChild(pSignDailyUI);
				pSignDailyUI->refreshUI();

				m_openUiTag = kTagSignDailyUI;
			}
		}break;
		case REWARD_LIST_ID_ONLINEREWARD:
		{
			auto pOnLineRewardUI = (OnLineRewardUI *)mainscene_->getChildByTag(kTagOnLineGiftUI);
			if (pOnLineRewardUI == NULL)
			{
				// �nGiftNum + 1ѵ��� �ͬ��� layer
				auto pOnlineGiftLayer = (OnlineGiftLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftLayer);
				if (NULL != pOnlineGiftLayer)
				{
					OnlineGiftData::instance()->getGiftDataTimeFromIcon(pOnlineGiftLayer->get_giftTime(), pOnlineGiftLayer->get_giftNum());
					OnlineGiftData::instance()->initDataFromIntent();
				}

				pOnLineRewardUI = OnLineRewardUI::create();
				pOnLineRewardUI->setIgnoreAnchorPointForPosition(false);
				pOnLineRewardUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				pOnLineRewardUI->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				pOnLineRewardUI->setTag(kTagOnLineGiftUI);

				pOnLineRewardUI->refreshData();
				pOnLineRewardUI->refreshTimeAndNum();					// ��� ½���� ź �ʱ�
				pOnLineRewardUI->update(0);

				GameView::getInstance()->getMainUIScene()->addChild(pOnLineRewardUI);

				m_openUiTag = kTagOnLineGiftUI;
			}

		}break;
		case REWARD_LIST_ID_VIPEVERYDAYREWARD:
		{
			auto vipEveryDayRewardScene = (Layer*)mainscene_->getChildByTag(kTagVipEveryDayRewardsUI);
			if (vipEveryDayRewardScene == NULL)
			{
				auto vpEveryDayRewardsUI = VipEveryDayRewardsUI::create();
				vpEveryDayRewardsUI->setIgnoreAnchorPointForPosition(false);
				vpEveryDayRewardsUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				vpEveryDayRewardsUI->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				mainscene_->addChild(vpEveryDayRewardsUI, 0, kTagVipEveryDayRewardsUI);

				m_openUiTag = kTagVipEveryDayRewardsUI;
			}
		}break;
		case REWARD_LIST_ID_OFFLINEEXP:
		{
			auto offLineExpUI = (OffLineExpUI*)mainscene_->getChildByTag(kTagOffLineExpUI);
			if (offLineExpUI == NULL)
			{
				offLineExpUI = OffLineExpUI::create();
				offLineExpUI->setIgnoreAnchorPointForPosition(false);
				offLineExpUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				offLineExpUI->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				offLineExpUI->setTag(kTagOffLineExpUI);
				GameView::getInstance()->getMainUIScene()->addChild(offLineExpUI);

				m_openUiTag = kTagOffLineExpUI;
			}
		}break;
		case REWARD_LIST_ID_SINGCOPYGETREWARD_CHUSHINIUDAO:
		{
			auto ranklist_ = (RankRewardUi *)mainscene_->getChildByTag(ktagSingCopyReward);
			if (ranklist_ == NULL)
			{
				ranklist_ = RankRewardUi::create(1);
				ranklist_->setIgnoreAnchorPointForPosition(false);
				ranklist_->setAnchorPoint(Vec2(0.5f, 0.5f));
				ranklist_->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				ranklist_->setTag(ktagSingCopyReward);
				GameView::getInstance()->getMainUIScene()->addChild(ranklist_);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1905, (void *)1);

				m_openUiTag = ktagSingCopyReward;
			}
		}break;
		case REWARD_LIST_ID_SINGCOPYGETREWARD_XIAOYOUMINGQI:
		{
			auto ranklist_ = (RankRewardUi*)mainscene_->getChildByTag(ktagSingCopyReward);
			if (ranklist_ == NULL)
			{
				ranklist_ = RankRewardUi::create(2);
				ranklist_->setIgnoreAnchorPointForPosition(false);
				ranklist_->setAnchorPoint(Vec2(0.5f, 0.5f));
				ranklist_->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				ranklist_->setTag(ktagSingCopyReward);
				GameView::getInstance()->getMainUIScene()->addChild(ranklist_);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1905, (void *)2);

				m_openUiTag = ktagSingCopyReward;
			}
		}break;
		case REWARD_LIST_ID_SINGCOPYGETREWARD_JIANGONGLEYE:
		{
			auto ranklist_ = (RankRewardUi *)mainscene_->getChildByTag(ktagSingCopyReward);
			if (ranklist_ == NULL)
			{
				ranklist_ = RankRewardUi::create(3);
				ranklist_->setIgnoreAnchorPointForPosition(false);
				ranklist_->setAnchorPoint(Vec2(0.5f, 0.5f));
				ranklist_->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				ranklist_->setTag(ktagSingCopyReward);
				GameView::getInstance()->getMainUIScene()->addChild(ranklist_);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1905, (void *)3);

				m_openUiTag = ktagSingCopyReward;
			}
		}break;
		case REWARD_LIST_ID_OFFARENA:
		{
			auto offLineArenaUI = (OffLineArenaUI *)mainscene_->getChildByTag(kTagOffLineArenaUI);
			if (offLineArenaUI == NULL)
			{
				offLineArenaUI = OffLineArenaUI::create();
				offLineArenaUI->setIgnoreAnchorPointForPosition(false);
				offLineArenaUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				offLineArenaUI->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				offLineArenaUI->setTag(kTagOffLineArenaUI);
				GameView::getInstance()->getMainUIScene()->addChild(offLineArenaUI);

				m_openUiTag = kTagOffLineArenaUI;
			}
		}break;
		case REWARD_LIST_ID_PHYSICAL_DAY:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5093);
		}break;
		case REWARD_LIST_ID_PHYSICAL_FRIEND:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5132);
			m_openUiTag = kTagGetphyPowerUi;
		}break;
		case REWARD_LIST_ID_FAMILYFIGHT:
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightRewardUI) == NULL)
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1544);

				auto familyFightRewardUI = FamilyFightRewardsUI::create();
				familyFightRewardUI->setIgnoreAnchorPointForPosition(false);
				familyFightRewardUI->setAnchorPoint(Vec2(.5f, .5f));
				familyFightRewardUI->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				familyFightRewardUI->setTag(kTagFamilyFightRewardUI);
				GameView::getInstance()->getMainUIScene()->addChild(familyFightRewardUI);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1545);

				m_openUiTag = kTagFamilyFightRewardUI;
			}
		}break;
		case REWARD_LIST_ID_BATTLEACHIEVEMENT:
		{
			auto tmpBattleAchUI = (BattleAchievementUI*)mainscene_->getChildByTag(kTagBattleAchievementUI);
			if (NULL == tmpBattleAchUI)
			{
				auto battleAchUI = BattleAchievementUI::create();
				battleAchUI->setIgnoreAnchorPointForPosition(false);
				battleAchUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				battleAchUI->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				battleAchUI->setTag(kTagBattleAchievementUI);

				GameView::getInstance()->getMainUIScene()->addChild(battleAchUI);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5101);

				m_openUiTag = kTagBattleAchievementUI;
			}
		}break;
		case REWARD_LIST_ID_WORLDBOSS:
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGetRewardUI) == NULL)
			{
				auto winSize = Director::getInstance()->getVisibleSize();
				auto getRewardUI = GetRewardUI::create();
				getRewardUI->setIgnoreAnchorPointForPosition(false);
				getRewardUI->setAnchorPoint(Vec2(.5f, .5f));
				getRewardUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				getRewardUI->setTag(kTagGetRewardUI);
				GameView::getInstance()->getMainUIScene()->addChild(getRewardUI);

				m_openUiTag = kTagGetRewardUI;
			}
		}break;
		case REWARD_LIST_ID_MONEYTREE:
		{
			MoneyTreeUI * pMoneyTreeUI = (MoneyTreeUI *)this->getChildByTag(kTagMoneyTreeUI);
			if (NULL == pMoneyTreeUI)
			{
				MoneyTreeUI* pTmpMoneyTreeUI = MoneyTreeUI::create();
				pTmpMoneyTreeUI->setIgnoreAnchorPointForPosition(false);
				pTmpMoneyTreeUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				pTmpMoneyTreeUI->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				pTmpMoneyTreeUI->initDataFromInternet();
				pTmpMoneyTreeUI->setTag(kTagMoneyTreeUI);
				GameView::getInstance()->getMainUIScene()->addChild(pTmpMoneyTreeUI);

				m_openUiTag = kTagMoneyTreeUI;
			}
		}break;
		case REWARD_LIST_ID_SIGNMONTHLY:
		{
			//open ui
			SignMonthlyUI * signMonthUI = (SignMonthlyUI *)this->getChildByTag(kTagSignMonthlyUI);
			if (NULL == signMonthUI)
			{
				signMonthUI = SignMonthlyUI::create();
				signMonthUI->setIgnoreAnchorPointForPosition(false);
				signMonthUI->setAnchorPoint(Vec2(.5f, .5f));
				signMonthUI->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				GameView::getInstance()->getMainUIScene()->addChild(signMonthUI, 0, kTagSignMonthlyUI);

				m_openUiTag = kTagSignMonthlyUI;
			}
		}break;
		}

		if (id_ != REWARD_LIST_ID_PHYSICAL_DAY)
		{
			m_uiState = state_visible;
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RewardUi::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void RewardUi::addRewardListEvent( int index_ )
{
	for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
	{
		if (index_ == GameView::getInstance()->rewardvector.at(i)->getId())
		{
			//if reward is have,not add again
			return;
		}
	}

	auto temp_reward = new CRewardBase();
	temp_reward->setId(RewardListConfig::s_RewardListMsgData[index_]->getId());
	temp_reward->setName(RewardListConfig::s_RewardListMsgData[index_]->getName());
	temp_reward->setIcon(RewardListConfig::s_RewardListMsgData[index_]->getIcon());
	temp_reward->setDes(RewardListConfig::s_RewardListMsgData[index_]->getDes());
	temp_reward->setClose(RewardListConfig::s_RewardListMsgData[index_]->getClose());

	switch(index_)
	{
	case REWARD_LIST_ID_SIGNDAILY:
		{
			if (SignDailyData::instance()->isCanSign())
			{
				temp_reward->setRewardState(1);
			}else
			{
				temp_reward->setRewardState(0);
			}
		}break;
	case REWARD_LIST_ID_ONLINEREWARD:
		{
			temp_reward->setRewardState(0);
		}break;
	case REWARD_LIST_ID_MONEYTREE:
		{
			std::string strings_ = MoneyTreeData::instance()->get_strTimeLeft();
			const char *str1 = StringDataManager::getString("MoneyTree_canGetInfo");
			if (strcmp(strings_.c_str(),str1) == 0)
			{
				temp_reward->setRewardState(1);
			}else
			{
				temp_reward->setRewardState(0);
			}
		}break;
	default:
		{
			temp_reward->setRewardState(1);
		}break;
	case REWARD_LIST_ID_SIGNMONTHLY:
		{
			//��Ƿ������ȡ
			if (SignMonthlyData::getInstance()->m_bCanGetGift)
			{
				temp_reward->setRewardState(1);
			}else
			{
				temp_reward->setRewardState(0);
			}
		}break;
	}
	GameView::getInstance()->rewardvector.push_back(temp_reward);

	if (GameView::getInstance()->getMainUIScene())
	{
		auto reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
		if (reward_ui != NULL)
		{
			reward_ui->tablevie_reward->reloadData();
		}
	}

	RewardUi::setRewardListParticle();
}

void RewardUi::removeRewardListEvent( int index )
{
	for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
	{
		if (GameView::getInstance()->rewardvector.at(i)->getId() == index)
		{
			switch(index)
			{
			case REWARD_LIST_ID_SIGNDAILY:
				{
					GameView::getInstance()->rewardvector.at(i)->setRewardState(0);

					auto reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
					if (reward_ui != NULL)
					{
						reward_ui->tablevie_reward->updateCellAtIndex(i);
					}
				}break;
			case REWARD_LIST_ID_ONLINEREWARD:
				{
					GameView::getInstance()->rewardvector.at(i)->setRewardState(0);

					auto reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
					if (reward_ui != NULL)
					{
						reward_ui->tablevie_reward->updateCellAtIndex(i);
					}
				}break;
			default:
				{
					std::vector<CRewardBase *>::iterator iter= GameView::getInstance()->rewardvector.begin()+i;
					auto rewardBase_ = *iter ;
					GameView::getInstance()->rewardvector.erase(iter);
					delete rewardBase_;	

					auto reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
					if (reward_ui != NULL)
					{
						reward_ui->tablevie_reward->reloadData();
					}
				}break;
			}
		}
	}

	RewardUi::setRewardListParticle();
}

void RewardUi::setRewardListParticle()
{
	bool isHave = false;
	for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
	{
		if (GameView::getInstance()->rewardvector.at(i)->getRewardState() == 1)
		{
			isHave =  true;
			break;
		}
	}
	if (GameView::getInstance()->getMainUIScene() == NULL)
		return;

	auto guideMap = (GuideMap*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (isHave== true)
	{
		if (guideMap &&  guideMap->getActionLayer())
		{
			if (guideMap->getActionLayer()->getChildByName("btn_onLineGift"))
			{
				guideMap->btn_RewardParticle->setVisible(true);
			}
		}
	}else
	{
		if (guideMap &&  guideMap->getActionLayer())
		{
			if (guideMap->getActionLayer()->getChildByName("btn_onLineGift"))
			{
				guideMap->btn_RewardParticle->setVisible(false);
			}
		}
	}
}

void RewardUi::updateShowCountTime( float dt )
{
	for (int i =0;i<GameView::getInstance()->rewardvector.size();i++)
	{
		switch(GameView::getInstance()->rewardvector.at(i)->getId())
		{
		case REWARD_LIST_ID_ONLINEREWARD:
			{
			auto cell_ = tablevie_reward->cellAtIndex(i);
				if (cell_)
				{
					auto label_time = (Label *)cell_->getChildByTag(REWARD_ONLINE_TIME_TAG);
					if (label_time)
					{
						auto pOnlineGiftLayer = (OnlineGiftLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftLayer);
						if (pOnlineGiftLayer != NULL)
						{
							auto  layer_ = (Layer *)cell_->getChildByTag(CELL_LAYER_TAG);

							std::string strings_ = pOnlineGiftLayer->get_timeLeft();
							const char *str1 = StringDataManager::getString("label_timeout");
							if (strcmp(strings_.c_str(),str1) == 0 && pOnlineGiftLayer->m_nGiftNum > -1 && pOnlineGiftLayer->m_nGiftNum <8)
							{
								label_time->setVisible(false);

								if (layer_ != NULL)
								{
									auto btn_check = (Button *)layer_->getChildByName("btn_onlineCheck_Reward");
									auto btn_get = (Button *)layer_->getChildByName("btn_onlineget_Reward");
									auto image_sp_ = (ImageView *)layer_->getChildByName("image_checkReward_sp");
									if (btn_check!= NULL && btn_get!= NULL)
									{
										btn_check->setVisible(false);
										btn_get->setVisible(true);
										image_sp_->setVisible(true);
									}
								}
							}else
							{
								label_time->setString(strings_.c_str());
								if (pOnlineGiftLayer->m_nGiftNum > -1 && pOnlineGiftLayer->m_nGiftNum <8)
								{
									label_time->setVisible(true);
								}else
								{
									label_time->setVisible(false);
								}
								auto btn_check = (Button *)layer_->getChildByName("btn_onlineCheck_Reward");
								auto btn_get = (Button *)layer_->getChildByName("btn_onlineget_Reward");
								auto image_sp_ = (ImageView *)layer_->getChildByName("image_checkReward_sp");
								if (btn_check!= NULL && btn_get!= NULL)
								{
									btn_check->setVisible(true);
									btn_get->setVisible(false);
									image_sp_->setVisible(false);
								}
							}
						}
					}
				}
			}break;
		case REWARD_LIST_ID_MONEYTREE:
			{
				auto cell_ = tablevie_reward->cellAtIndex(i);
				if (cell_)
				{
					auto label_time = (Label *)cell_->getChildByTag(REWARD_MONEYTREE_TIME_TAG);
					if (label_time)
					{
						auto  layer_ = (Layer *)cell_->getChildByTag(CELL_LAYER_TAG);

						std::string strings_ = MoneyTreeData::instance()->get_strTimeLeft();
						const char *str1 = StringDataManager::getString("MoneyTree_canGetInfo");
						label_time->setString(strings_.c_str());

						if (strcmp(strings_.c_str(),str1) == 0)
						{
							label_time->setVisible(false);
							if (layer_ != NULL)
							{
								auto btn_check = (Button *)layer_->getChildByName("btn_onlineCheck_Reward");
								auto btn_get = (Button *)layer_->getChildByName("btn_onlineget_Reward");
								auto image_sp_ = (ImageView *)layer_->getChildByName("image_checkReward_sp");
								if (btn_check!= NULL && btn_get!= NULL)
								{
									btn_check->setVisible(false);
									image_sp_->setVisible(true);

									btn_get->setVisible(true);
								}
							}
						}else
						{
							label_time->setVisible(true);
							auto btn_check = (Button *)layer_->getChildByName("btn_onlineCheck_Reward");
							auto btn_get = (Button *)layer_->getChildByName("btn_onlineget_Reward");
							auto image_sp_ = (ImageView *)layer_->getChildByName("image_checkReward_sp");
							if (btn_check!= NULL && btn_get!= NULL)
							{
								btn_check->setVisible(true);
								image_sp_->setVisible(false);

								btn_get->setVisible(false);
							}
						}
					}
				}
			}break;
		}
	}
}

void RewardUi::update( float delta )
{
	/*�ڵ�cell���µui�ʱ����شuiˣ����ر��µui�ʱ���ٴ���ʾ�ui*/

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(m_openUiTag) == NULL)
	{
		m_uiState = stat_show;
	}

	switch(m_uiState)
	{
	case state_open:
		{

		}break;
	case state_visible:
		{
			this->setVisible(false);
		}break;
	case stat_show:
		{
			this->setVisible(true);
		}break;
	}
}
