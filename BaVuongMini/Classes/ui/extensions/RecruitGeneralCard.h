
#ifndef _H_RECRUITGENERALCARD_H 
#define _H_RECRUITGENERALCARD_H

#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;
/*
* ��һ����ļ�佫
* ͨ�õ���ļ�佫���
*/
class CGeneralDetail;

class RecruitGeneralCard:public UIScene
{
public:
	enum RecruitGeneralCardType
	{
		kTypeFirstRecurite = 0,
		kTypeCommonRecuriteGeneral,
		kTypeUsePropRecuriteGeneral,
	};
public:
	RecruitGeneralCard(void);
	~RecruitGeneralCard(void);

	static RecruitGeneralCard * create(int curType = kTypeFirstRecurite,int generalModelId = 0);
	bool init(int curType,int generalModelId);
	void onEnter();
	void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	std::string getGeneralIconPath(std::string generalIcon);
	void callBackShowGeneral(Ref *pSender, Widget::TouchEventType type);

	ImageView * image_light_a;
	ImageView * image_light_b;
	void lightActions();

	void createParticleSys();

	ImageView * imageGeneral_Star;
	void generalStarActions();

	ImageView * imageGeneral_profession;
	void generalProfessionActions();

	ImageView * image_up_;
	ImageView * image_down_;

	void AddcallBackFirstEvent(Ref* pSender, SEL_CallFuncO pEvent);
	void AddcallBacksecondEvent(Ref* pSender, SEL_CallFuncO pEvent);
	void AddcallBackThirdEvent(Ref* pSender, SEL_CallFuncO pEvent);

	Button * getFirstButton();
	Button * getSecondButton();
	Button * getThirdButton();

	Label * getFirstLabel();
	Label * getSecondLabel();
	Label * getThirdLabel();

	//��Ƿ����ڽ�ѧ
	bool isRunTutorials;
	bool isFinishedAction();

	void RefreshCostValueByActionType(int actionType);

private:
	ImageView * imageGeneral_bg;
	Button * button_first;
	Button * button_second;
	Button * button_third;

	Label * label_first;
	Label * label_second;
	Label * label_third;
	void callbackFirstButton(Ref *pSender, Widget::TouchEventType type);
	void callBackSecondButton(Ref *pSender, Widget::TouchEventType type);
	void callBackThirdButton(Ref *pSender, Widget::TouchEventType type);
	
	Ref * senderListener;
	SEL_CallFuncO senderSelectorFirstButton;
	SEL_CallFuncO senderSelectorSecondButton;
	SEL_CallFuncO senderSelectorThirdButton;
public:
	static std::string getGeneralProfessionIconPath(int prfofessionId);
	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	//��ѧ�����kTypeCommonRecuiiteGeneral�
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	int mTutorialScriptInstanceId;
private:
	Size winsize;
	Layer * u_layer;
	//�״���ļ���佫ID
	long long m_generalId;
	int m_type;
};
#endif;
