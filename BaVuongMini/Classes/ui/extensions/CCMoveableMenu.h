#ifndef _MENUNODE_CCMOVEABLEMENU_H_
#define _MENUNODE_CCMOVEABLEMENU_H_

#include "cocos2d.h"
USING_NS_CC;

/*
* when you want put a Menu into the ScrollView or CCTabelView,
* please use this CCMoveableMenu instead of Menu
*/
class CCMoveableMenu : public Menu
{
public:
	CCMoveableMenu(void);
	~CCMoveableMenu(void);

	static CCMoveableMenu * create();
	static CCMoveableMenu* create(MenuItem* item, ...);
	bool init();
	//virtual void registerWithTouchDispatcher();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

private:
	bool moved_;
	Vec2 mBeginPoint;   // record the point when onTouchBegan()
};

#endif;