#ifndef EXTENSIONS_POPUPPANEL_H
#define EXTENSIONS_POPUPPANEL_H

#include "UIScene.h"
enum
{
	OPTIONBOARD=1,
	TABLETBOARD,
	TEXTBOARD,
};
class RichTextInputBox;
class PopupPanel:public UIScene
{
public:
	PopupPanel(void);
	~PopupPanel(void);

	struct PopupBoardPara
	{
		int boardType_;

		std::string boardText_;
		int boardAlign_;

		int boardTime_;

		int boardDefaultid_;

		int boardinputType_; 
		int boardMaxsize_;
		int hasOptios_;
		int trigger_;
	};
	struct PopuBoardOption
	{
		int clientFunction_;
		int id_;
		std::string title_;
		std::string msg_;
	};
	

	static PopupPanel *create(PopupBoardPara popupBoardPara,std::vector<PopuBoardOption *>optionvector);
	bool init(PopupBoardPara popupBoardPara,std::vector<PopuBoardOption *>optionvector);
	void onEnter();
	void onExit();
	void callBackClose(Ref *pSender, Widget::TouchEventType type);
	struct sendContent
	{
		int id_;
		std::string inputContent_;
		int option_ ;
	};
private:
	int m_boardType;

	void step(float dt);
	//void update(float dt);
	void callBackSend(Ref *pSender, Widget::TouchEventType type);
	void selectedStateEvent(Ref *pSender, CheckBoxEventType type);
	void callBackCoutDownDefault();
	RichTextInputBox *textBox_private;
	
	int buttonSize;
	Label * centerTimeLabel;
	int m_countDown;

	int defaultId_;
	//1128 send content
	int reqSelectId_;
	//const char* reqContent_;
	int reqOption_;
};
#endif;
