#include "RecruitGeneralCard.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../generals_ui/RecuriteActionItem.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../messageclient/element/CActionDetail.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "AppMacros.h"

#define kTag_head_anm 10
RecruitGeneralCard::RecruitGeneralCard(void):
isRunTutorials(false)
{
	m_generalId = 0;
}


RecruitGeneralCard::~RecruitGeneralCard(void)
{
}

RecruitGeneralCard * RecruitGeneralCard::create(int curType,int generalModelId)
{
	auto recruitCard = new RecruitGeneralCard();
	if (recruitCard && recruitCard->init(curType,generalModelId))
	{
		recruitCard->autorelease();
		return recruitCard;
	}
	CC_SAFE_DELETE(recruitCard);
	return NULL;
}

bool RecruitGeneralCard::init(int curType,int generalModelId)
{
	if (UIScene::init())
	{
		winsize =Director::getInstance()->getVisibleSize();
		
		m_type = curType;

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2(0.5f,0.5f));
		mengban->setPosition(Vec2(383/2,379/2));
		m_pLayer->addChild(mengban);
		
		u_layer = Layer::create();
		u_layer->setAnchorPoint(Vec2(0.0f,0.0f));
		u_layer->setPosition(Vec2(0,0));
		u_layer->setContentSize(Size(383,379));
		m_pLayer->addChild(u_layer);
		//
		image_light_a = ImageView::create();
		image_light_a->loadTexture("res_ui/lightlighta.png");
		image_light_a->setAnchorPoint(Vec2(0.5f,0.5f));
		image_light_a->setPosition(Vec2(192,198));
		image_light_a->setScale(0);
		u_layer->addChild(image_light_a);
		
		image_light_b = ImageView::create();
		image_light_b->loadTexture("res_ui/lightlightb.png");
		image_light_b->setAnchorPoint(Vec2(0.5f,0.5f));
		image_light_b->setPosition(Vec2(192,198));
		image_light_b->setScale(0);
		u_layer->addChild(image_light_b);

		imageGeneral_bg = ImageView::create();
		imageGeneral_bg->loadTexture("res_ui/generals_dia.png");
		imageGeneral_bg->setAnchorPoint(Vec2(0.5f,0.5f));
		imageGeneral_bg->setPosition(Vec2(192,198));
		imageGeneral_bg->setScale(4.0f);
		u_layer->addChild(imageGeneral_bg);

		image_up_ = ImageView::create();
		image_up_->loadTexture("res_ui/wujiang/nihuode.png");
		image_up_->setAnchorPoint(Vec2(0.5f,0.5f));
		image_up_->setPosition(Vec2(192,imageGeneral_bg->getPosition().y+imageGeneral_bg->getContentSize().height/2+image_up_->getContentSize().height -10));
		u_layer->addChild(image_up_);
		image_up_->setVisible(false);

		//generalInfo
		CGeneralBaseMsg * generalBaseInfo;
		switch(curType)
		{
		case kTypeFirstRecurite:
			{
				 for (int i =0;i<GameView::getInstance()->generalBaseMsgList.size();i++)
				 {
					 if (GameView::getInstance()->generalBaseMsgList.at(i)->fightstatus() == 0)
					 {
						 m_generalId = GameView::getInstance()->generalBaseMsgList.at(i)->id();
						 int generalModle_ = GameView::getInstance()->generalBaseMsgList.at(i)->modelid();
						 generalBaseInfo  = GeneralsConfigData::s_generalsBaseMsgData[generalModle_];
						 break;
					 }
				 }
			}
			break;
		case kTypeCommonRecuriteGeneral:
			{
				generalBaseInfo = GeneralsConfigData::s_generalsBaseMsgData[generalModelId];
			}
			break;
		case kTypeUsePropRecuriteGeneral:
			{
				generalBaseInfo = GeneralsConfigData::s_generalsBaseMsgData[generalModelId];
			}
			break;
		}

		CCAssert(generalBaseInfo != NULL, "generalBaseInfo should not be null, please check db");

		std::string generalIconPath_ =  getGeneralIconPath(generalBaseInfo->get_half_photo());
		auto imageGeneral_Icon = ImageView::create();
		imageGeneral_Icon->loadTexture(generalIconPath_.c_str());
		imageGeneral_Icon->setAnchorPoint(Vec2(0.5f,0));
		imageGeneral_Icon->setPosition(Vec2(0,63-imageGeneral_bg->getContentSize().height/2));
		imageGeneral_bg->addChild(imageGeneral_Icon);

		std::string generalName_ = generalBaseInfo->name();
		auto Label_generalName=Label::createWithTTF(generalName_.c_str(), APP_FONT_NAME, 20);
		Label_generalName->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_generalName->setPosition(Vec2(0,-imageGeneral_bg->getContentSize().height/2+Label_generalName->getContentSize().height*3 - 5));
		imageGeneral_bg->addChild(Label_generalName);

		//rare
		std::string generalRare_ = RecuriteActionItem::getStarPathByNum(generalBaseInfo->rare());
		imageGeneral_Star = ImageView::create();
		imageGeneral_Star->loadTexture(generalRare_.c_str());
		imageGeneral_Star->setAnchorPoint(Vec2(0.5f,0.5f));
		imageGeneral_Star->setPosition(Vec2(imageGeneral_Star->getContentSize().width/2 - imageGeneral_bg->getContentSize().width/2+13,
			Label_generalName->getPosition().y+imageGeneral_Star->getContentSize().height));
		imageGeneral_Star->setScale(2.0f);
		imageGeneral_bg->addChild(imageGeneral_Star);
		imageGeneral_Star->setVisible(false);

		//profession
		std::string generalProfess_ = getGeneralProfessionIconPath(generalBaseInfo->get_profession());
		imageGeneral_profession = ImageView::create();
		imageGeneral_profession->loadTexture(generalProfess_.c_str());
		imageGeneral_profession->setAnchorPoint(Vec2(0.5f,0.5f));
		imageGeneral_profession->setPosition(Vec2(imageGeneral_bg->getContentSize().width/2 - imageGeneral_profession->getContentSize().width/2 - 14,
			imageGeneral_Star->getPosition().y));
		imageGeneral_profession->setScale(2.0f);
		imageGeneral_bg->addChild(imageGeneral_profession);
		imageGeneral_profession->setVisible(false);

		switch(curType)
		{
		case kTypeFirstRecurite:
			{
				image_down_ = ImageView::create();
				image_down_->loadTexture("res_ui/wujiang/daizi.png");
				image_down_->setAnchorPoint(Vec2(0.5f,0.5f));
				image_down_->setPosition(Vec2(u_layer->getContentSize().width/2,
										imageGeneral_bg->getPosition().y - imageGeneral_bg->getContentSize().height/2 - image_down_->getContentSize().height/2+5));
				u_layer->addChild(image_down_);
				image_down_->setVisible(false);

				auto button_getGeneral = Button::create();
				button_getGeneral->loadTextures("res_ui/wujiang/woyuanzhuisuini.png","res_ui/wujiang/woyuanzhuisuini.png","");
				button_getGeneral->setTouchEnabled(true);
				button_getGeneral->setPressedActionEnabled(true);
				button_getGeneral->addTouchEventListener(CC_CALLBACK_2(RecruitGeneralCard::callBackShowGeneral,this));
				button_getGeneral->setAnchorPoint(Vec2(0.5f,0.5f));
				button_getGeneral->setPosition(Vec2(0,0));
				image_down_->addChild(button_getGeneral);
			}break;
		case kTypeCommonRecuriteGeneral:
			{
				button_first = Button::create();
				button_first->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				button_first->setScale9Enabled(true);
				button_first->setContentSize(Size(110,46));
				button_first->setCapInsets(Rect(18,9,2,23));
				button_first->setTouchEnabled(true);
				button_first->setPressedActionEnabled(true);
				button_first->addTouchEventListener(CC_CALLBACK_2(RecruitGeneralCard::callbackFirstButton,this));
				button_first->setAnchorPoint(Vec2(0.5f,0.5f));
				button_first->setPosition(Vec2(70,27));
				u_layer->addChild(button_first);
				button_first->setVisible(false);

				const char * firstString_ = StringDataManager::getString("generals_Recurite_button_des");
				label_first = Label::createWithTTF(firstString_, APP_FONT_NAME, 20);
				label_first->setAnchorPoint(Vec2(0.5f,0.5f));
				label_first->setPosition(Vec2(0,0));
				button_first->addChild(label_first);

				button_second = Button::create();
				button_second->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				button_second->setScale9Enabled(true);
				button_second->setContentSize(Size(110,46));
				button_second->setCapInsets(Rect(18,9,2,23));
				button_second->setTouchEnabled(true);
				button_second->setPressedActionEnabled(true);
				button_second->addTouchEventListener(CC_CALLBACK_2(RecruitGeneralCard::callBackSecondButton, this));
				button_second->setAnchorPoint(Vec2(0.5f,0.5f));
				button_second->setPosition(Vec2(192,27));
				u_layer->addChild(button_second);
				button_second->setVisible(false);

				const char * secondString_ = StringDataManager::getString("generals_Recurite_button_close");
				label_second = Label::createWithTTF(secondString_, APP_FONT_NAME, 20);
				label_second->setAnchorPoint(Vec2(0.5f,0.5f));
				label_second->setPosition(Vec2(0,0));
				button_second->addChild(label_second);

				button_third = Button::create();
				button_third->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				button_third->setScale9Enabled(true);
				button_third->setContentSize(Size(110,46));
				button_third->setCapInsets(Rect(18,9,2,23));
				button_third->setTouchEnabled(true);
				button_third->setPressedActionEnabled(true);
				button_third->addTouchEventListener(CC_CALLBACK_2(RecruitGeneralCard::callBackThirdButton, this));
				button_third->setAnchorPoint(Vec2(0.5f,0.5f));
				button_third->setPosition(Vec2(314,27));
				u_layer->addChild(button_third);
				button_third->setVisible(false);

				const char * thirdString_ = StringDataManager::getString("generals_Recurite_button_recuriteAgain");
				label_third = Label::createWithTTF(thirdString_, APP_FONT_NAME, 20);
				label_third->setAnchorPoint(Vec2(0.5f,0.5f));
				label_third->setPosition(Vec2(0,0));
				button_third->addChild(label_third);

				//cost 
				auto imageView_goldInGot = ImageView::create();
				imageView_goldInGot->loadTexture("res_ui/ingot.png");
				imageView_goldInGot->setAnchorPoint(Vec2(0.5f,0.5f));
				imageView_goldInGot->setPosition(Vec2(290,65));
				imageView_goldInGot->setName("imageView_goldInGot");
				u_layer->addChild(imageView_goldInGot);
				imageView_goldInGot->setVisible(false);
				auto label_goldInGotValue = Label::createWithTTF("900", APP_FONT_NAME, 16);
				label_goldInGotValue->setAnchorPoint(Vec2(0,0.5f));
				label_goldInGotValue->setPosition(Vec2(imageView_goldInGot->getContentSize().width/2+10,0));
				label_goldInGotValue->setName("label_goldInGotValue");
				imageView_goldInGot->addChild(label_goldInGotValue);
			}
			break;
		case kTypeUsePropRecuriteGeneral:
			{
				button_first = Button::create();
				button_first->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				button_first->setScale9Enabled(true);
				button_first->setContentSize(Size(120,46));
				button_first->setCapInsets(Rect(18,9,2,23));
				button_first->setTouchEnabled(true);
				button_first->setPressedActionEnabled(true);
				button_first->addTouchEventListener(CC_CALLBACK_2(RecruitGeneralCard::callbackFirstButton,this));
				button_first->setAnchorPoint(Vec2(0.5f,0.5f));
				button_first->setPosition(Vec2(113,27));
				u_layer->addChild(button_first);
				button_first->setVisible(false);

				const char * firstString_ = StringDataManager::getString("generals_Recurite_button_des");
				label_first = Label::createWithTTF(firstString_, APP_FONT_NAME,20);
				label_first->setAnchorPoint(Vec2(0.5f,0.5f));
				label_first->setPosition(Vec2(0,0));
				button_first->addChild(label_first);

				button_second = Button::create();
				button_second->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				button_second->setScale9Enabled(true);
				button_second->setContentSize(Size(120,46));
				button_second->setCapInsets(Rect(18,9,2,23));
				button_second->setTouchEnabled(true);
				button_second->setPressedActionEnabled(true);
				button_second->addTouchEventListener(CC_CALLBACK_2(RecruitGeneralCard::callBackSecondButton, this));
				button_second->setAnchorPoint(Vec2(0.5f,0.5f));
				button_second->setPosition(Vec2(278,27));
				u_layer->addChild(button_second);
				button_second->setVisible(false);

				const char * secondString_ = StringDataManager::getString("generals_Recurite_button_close");
				label_second = Label::createWithTTF(secondString_,APP_FONT_NAME,20);
				label_second->setAnchorPoint(Vec2(0.5f,0.5f));
				label_second->setPosition(Vec2(0,0));

				button_second->addChild(label_second);
			}
			break;
		}

// 		auto action =(ActionInterval *)Sequence::create(
// 			ScaleTo::create(0.4f,2.0f,2.0f),
// 			ScaleTo::create(0.15f,0.8f,0.8f),
// 			ScaleTo::create(0.1f,1.0f,1.0f),
// 			CallFunc::create(this, callfunc_selector(RecruitGeneralCard::lightActions)),
// 			NULL);
// 		imageGeneral_bg->runAction(action);

		imageGeneral_bg->setScale(1.0f);
		imageGeneral_bg->setColor(Color3B(0,0,0));

		auto action =(ActionInterval *)Sequence::create(
			CCTintTo::create(0.5f, 255,255,255),
			CallFunc::create(CC_CALLBACK_0(RecruitGeneralCard::createParticleSys,this)),
			DelayTime::create(0.5f),
			CallFunc::create(CC_CALLBACK_0(RecruitGeneralCard::lightActions,this)),
			DelayTime::create(0.5f),
			NULL);
		imageGeneral_bg->runAction(action);
		action->setTag(kTag_head_anm);
		//head imageView action
		imageGeneral_Icon->setColor(Color3B(0,0,0));
		auto head_action = Sequence::create(
			DelayTime::create(1.0f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		imageGeneral_Icon->runAction(head_action);
		//name label action
		Label_generalName->setColor(Color3B(0,0,0));
		auto nameLabel_action = Sequence::create(
			DelayTime::create(1.0f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		Label_generalName->runAction(nameLabel_action);
		

// 		RepeatForever *repeat1 = RepeatForever::create(Sequence::create(MoveBy::create(0.5f,Vec2(0,10)),MoveBy::create(0.5f,Vec2(0,-10)),NULL));
// 		imageGeneral_bg->runAction(repeat1);

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(u_layer->getContentSize());
		return true;
	}
	return false;
}

void RecruitGeneralCard::onEnter()
{
	UIScene::onEnter();
}

void RecruitGeneralCard::onExit()
{
	UIScene::onExit();
}

bool RecruitGeneralCard::onTouchBegan(Touch *touch, Event * pEvent)
{
	if (m_type == kTypeFirstRecurite)
	{
		return true;
	}
	else if (m_type == kTypeCommonRecuriteGeneral || m_type == kTypeUsePropRecuriteGeneral )
	{
		if (isRunTutorials)
		{
			return true;
		}

		if (isFinishedAction())
		{
			return resignFirstResponder(touch,this,false);
		}
		else
		{
			return true;
		}
	}
	else
	{
		return true;
	}
}

void RecruitGeneralCard::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void RecruitGeneralCard::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void RecruitGeneralCard::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void RecruitGeneralCard::callBackShowGeneral(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->removeCCTutorialIndicator();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5056, (void *)m_generalId, (void *)1);
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RecruitGeneralCard::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-this->getContentSize().width)/2;
	int _h = (winSize.height - this->getContentSize().height)/2;
	auto tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
	tutorialIndicator->setPosition(Vec2(pos.x,pos.y));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	tutorialIndicator->addHighLightFrameAction(150,50,Vec2(0,5));
	tutorialIndicator->addCenterAnm(150,50,Vec2(0,5));
	this->addChild(tutorialIndicator);
}

void RecruitGeneralCard::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getVisibleSize();
	int _w = (winSize.width-this->getContentSize().width)/2;
	int _h = (winSize.height - this->getContentSize().height)/2;

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,100,40,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void RecruitGeneralCard::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(u_layer->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void RecruitGeneralCard::lightActions()
{
	auto action =(ActionInterval *)Sequence::create(
		ScaleTo::create(0.4f,1.5f,1.5f),
		CallFunc::create(CC_CALLBACK_0(RecruitGeneralCard::generalStarActions,this)),
		NULL);
	image_light_a->runAction((ActionInterval *)action->clone()->autorelease());

	auto repeat = RepeatForever::create( RotateBy::create(6.0f,360) );
	image_light_a->runAction((ActionInterval *)repeat->clone()->autorelease());

	image_light_b->runAction((ActionInterval *)repeat->clone()->autorelease());

	auto action_b = (ActionInterval *)Sequence::create(
					ScaleTo::create(0.7f,3.5f,3.5f),
					ScaleTo::create(0.7f,3.0f,3.0f),
					NULL);
	auto repeat_b = RepeatForever::create(action_b);
	image_light_b->runAction(repeat_b);
	
	image_up_->setVisible(true);
	image_up_->runAction(MoveBy::create(0.4f,Vec2(0,10)));
	
	switch(m_type)
	{
	case kTypeFirstRecurite:
		{
			image_down_->setVisible(true);	
			image_down_->runAction(MoveBy::create(0.4f,Vec2(0,-10)));
		}break;
	case kTypeCommonRecuriteGeneral:
		{
			button_first->setVisible(true);
			button_first->runAction(MoveBy::create(0.4f,Vec2(0,-10)));
			button_second->setVisible(true);
			button_second->runAction(MoveBy::create(0.4f,Vec2(0,-10)));
			button_third->setVisible(true);
			button_third->runAction(MoveBy::create(0.4f,Vec2(0,-10)));

			if (u_layer->getChildByName("imageView_goldInGot"))
			{
				u_layer->getChildByName("imageView_goldInGot")->setVisible(true);
				u_layer->getChildByName("imageView_goldInGot")->runAction(MoveBy::create(0.4f,Vec2(0,-10)));
			}
			
		}break;
	case kTypeUsePropRecuriteGeneral:
		{
			button_first->setVisible(true);
			button_first->runAction(MoveBy::create(0.4f,Vec2(0,-10)));
			button_second->setVisible(true);
			button_second->runAction(MoveBy::create(0.4f,Vec2(0,-10)));
		}break;
	}
}

void RecruitGeneralCard::generalStarActions()
{
	imageGeneral_Star->setVisible(true);
	auto action =(ActionInterval *)Sequence::create(
		ScaleTo::create(0.2f,1.0,1.0f),
		CallFunc::create(CC_CALLBACK_0(RecruitGeneralCard::generalProfessionActions,this)),
		NULL);

	imageGeneral_Star->runAction(action);
}

void RecruitGeneralCard::generalProfessionActions()
{
	imageGeneral_profession->setVisible(true);
	imageGeneral_profession->runAction(ScaleTo::create(0.2f,1.0f,1.0f));

	if (m_type == kTypeFirstRecurite)
	{
		const char *firstGeneralstring = StringDataManager::getString("general_fightfirstGetNeweGeneral");
		int x_ = image_down_->getPosition().x;
		int y_ = image_down_->getPosition().y - 5;
		this->addCCTutorialIndicator(firstGeneralstring, Vec2(x_,y_),CCTutorialIndicator::Direction_LU);
	}
}

std::string RecruitGeneralCard::getGeneralProfessionIconPath( int prfofessionId )
{
	std::string iconPath = "res_ui/";
	switch(prfofessionId)
	{
	case PROFESSION_MJ_INDEX:
		{
			iconPath.append("mengjiang");
		}break;
	case PROFESSION_GM_INDEX:
		{
			iconPath.append("guimou");
		}break;
	case PROFESSION_HJ_INDEX:
		{
			iconPath.append("haojie");
		}break;
	case PROFESSION_SS_INDEX:
		{
			iconPath.append("shenshe");
		}break;
	}
	iconPath.append(".png");
	return iconPath;
}

std::string RecruitGeneralCard::getGeneralIconPath( std::string generalIcon )
{
	std::string path_ = "res_ui/general/";
	path_.append(generalIcon);
	path_.append(".png");

	return path_;
}

void RecruitGeneralCard::callbackFirstButton(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (senderListener && senderSelectorFirstButton)
		{
			(senderListener->*senderSelectorFirstButton)(this);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RecruitGeneralCard::callBackSecondButton(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		if (senderListener && senderSelectorSecondButton)
		{
			(senderListener->*senderSelectorSecondButton)(this);
		}
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RecruitGeneralCard::callBackThirdButton(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (senderListener && senderSelectorThirdButton)
		{
			(senderListener->*senderSelectorThirdButton)(this);
		}
		//this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RecruitGeneralCard::AddcallBackFirstEvent( Ref* pSender, SEL_CallFuncO pEvent )
{
	senderListener = pSender;
	senderSelectorFirstButton = pEvent;
}

void RecruitGeneralCard::AddcallBacksecondEvent( Ref* pSender, SEL_CallFuncO pEvent )
{
	senderListener = pSender;
	senderSelectorSecondButton = pEvent;
}

void RecruitGeneralCard::AddcallBackThirdEvent( Ref* pSender, SEL_CallFuncO pEvent )
{
	senderListener = pSender;
	senderSelectorThirdButton = pEvent;
}

Button * RecruitGeneralCard::getFirstButton()
{
	return button_first;
}

Button * RecruitGeneralCard::getSecondButton()
{
	return button_second;
}

Button * RecruitGeneralCard::getThirdButton()
{
	return button_third;
}

Label * RecruitGeneralCard::getFirstLabel()
{
	return label_first;
}

Label * RecruitGeneralCard::getSecondLabel()
{
	return label_second;
}

Label * RecruitGeneralCard::getThirdLabel()
{
	return label_third;
}

void RecruitGeneralCard::createParticleSys()
{
	auto pNode = BonusSpecialEffect::create();
	pNode->setPosition(Vec2(192, 218));
	pNode->setScale(1.2f);
	addChild(pNode);
}

bool RecruitGeneralCard::isFinishedAction()
{
	if (imageGeneral_bg->getActionByTag(kTag_head_anm))
	{
		return false;
	}
	
	return true;
}

void RecruitGeneralCard::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void RecruitGeneralCard::RefreshCostValueByActionType( int actionType )
{
	for (int i =0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		if (GameView::getInstance()->actionDetailList.at(i)->type() == actionType)
		{
			char s_gold[20];
			sprintf(s_gold,"%d",GameView::getInstance()->actionDetailList.at(i)->gold());
			if (u_layer->getChildByName("imageView_goldInGot"))
			{
				auto l_goldValue = (Label*)u_layer->getChildByName("imageView_goldInGot")->getChildByName("label_goldInGotValue");
				if (l_goldValue)
				{
					l_goldValue->setString(s_gold);
				}
			}
		}
	}
}


