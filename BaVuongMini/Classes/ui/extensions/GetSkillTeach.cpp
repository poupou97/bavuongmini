#include "GetSkillTeach.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../gamescene_state/sceneelement/HeadMenu.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutUIConstant.h"
#include "AppMacros.h"

using namespace cocos2d::ui;

GetSkillTeach::GetSkillTeach(void)
{
}


GetSkillTeach::~GetSkillTeach(void)
{
}

GetSkillTeach * GetSkillTeach::create(std::string skillId,std::string skillName,std::string skillIcon)
{
	auto skill_ =new GetSkillTeach();
	if (skill_ && skill_->init(skillId,skillName,skillIcon))
	{
		skill_->autorelease();
		return skill_;
	}
	CC_SAFE_DELETE(skill_);
	return NULL;
}

bool GetSkillTeach::init(std::string skillId,std::string skillName,std::string skillIcon)
{
	if ( UIScene::init())
	{
		m_skillId = skillId;
		m_skillName = skillName;
		m_skillIcon = skillIcon;
		
		winSize = Director::getInstance()->getVisibleSize();

		mengban = ui::ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winSize);
		mengban->setAnchorPoint(Vec2(0.5f,0.5f));
		mengban->setPosition(Vec2(winSize.width/2,winSize.height/2));
		m_pLayer->addChild(mengban);

		u_layer=Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setPosition(Vec2(winSize.width/2,winSize.height/2));
		u_layer->setContentSize(Size(800,480));
		addChild(u_layer);

		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("res_ui/uiflash/get_jineng/get_jineng0.png","res_ui/uiflash/get_jineng/get_jineng0.plist","res_ui/uiflash/get_jineng/get_jineng.ExportJson");
		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("res_ui/uiflash/get_jineng/get_jineng1.png","res_ui/uiflash/get_jineng/get_jineng1.plist","res_ui/uiflash/get_jineng/get_jineng.ExportJson");
		cocostudio::Armature * armature = cocostudio::Armature::create("get_jineng");
		armature->getAnimation()->play("Animation1");
		armature->getAnimation()->setMovementEventCallFunc(this,movementEvent_selector(GetSkillTeach::ArmatureFinishCallback));  
		armature->setPosition(Vec2(400,240));
		u_layer->addChild(armature);

		std::string iconPath = "";
		iconPath.append(m_skillIcon);
		iconPath.append(".png");
		
		cocostudio::SpriteDisplayData displayData_icon;
		displayData_icon.displayName = iconPath.c_str();
		auto bone_skillIcon = (cocostudio::Bone *)armature->getBone("skill_icon");
		bone_skillIcon->addDisplay(&displayData_icon,1);
		bone_skillIcon->changeDisplayWithIndex(1,true);
		
		std::string namePath = "";
		namePath.append(m_skillIcon);
		namePath.append("_name.png");

		cocostudio::SpriteDisplayData displayData_name;
		displayData_name.displayName = namePath.c_str();
		auto bone_skillName = (cocostudio::Bone *)armature->getBone("skill_name");
		bone_skillName->addDisplay(&displayData_name,1);
		bone_skillName->changeDisplayWithIndex(1,true);

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winSize);
		return true;
	}
	return false;
}

void GetSkillTeach::onEnter()
{
	UIScene::onEnter();
}

void GetSkillTeach::onExit()
{
	UIScene::onExit();
}

void GetSkillTeach::callBackStudySkill( Ref * obj )
{
	this->closeAnim();
	int skillUpLevel = 1;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1126,(void *)skillUpLevel,(void *)m_skillId.c_str());
}

void GetSkillTeach::ArmatureFinishCallback(cocostudio::Armature *armature, cocostudio::MovementEventType movementType, const char *movementID)
{
	std::string id = movementID;
	if (id.compare("Animation1") == 0)
	{
		armature->stopAllActions();
		armature->getAnimation()->play("Animation1_end");

		auto Btn_study = Button::create();
		Btn_study->loadTextures("res_ui/creating_a_role/button_di2.png", "res_ui/creating_a_role/button_di2.png","");
		Btn_study->setTouchEnabled(true);
		Btn_study->setPressedActionEnabled(true);
		Btn_study->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_study->setPosition(Vec2(400,75));
		Btn_study->addTouchEventListener(CC_CALLBACK_2(GetSkillTeach::callBackSkillAction,this));
		u_layer->addChild(Btn_study);

		const char * str = StringDataManager::getString("popupWindow_sure");
		auto label_ = Label::createWithTTF(str, APP_FONT_NAME, 20);
		label_->setAnchorPoint(Vec2(0.5f,0.5f));
		label_->setPosition(Vec2(0,0));

		Btn_study->addChild(label_);
	}
}

void GetSkillTeach::callBackSkillAction(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Size winSize_ = Director::getInstance()->getVisibleSize();

		mengban->runAction(FadeOut::create(0.8f));


		std::string skillIconPath = "res_ui/jineng_icon/";
		skillIconPath.append(m_skillIcon);
		skillIconPath.append(".png");

		auto pSpriteIcon = Sprite::create(skillIconPath.c_str());
		pSpriteIcon->setPosition(Vec2(winSize_.width / 2, winSize_.height / 2 + pSpriteIcon->getContentSize().height*1.2f));
		pSpriteIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
		pSpriteIcon->setScale(1.3f);
		GameView::getInstance()->getMainUIScene()->addChild(pSpriteIcon);

		auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
		auto shortLayey_ = (ShortcutLayer *)mainscene_->getChildByTag(kTagShortcutLayer);
		if (shortLayey_ != NULL)
		{
			Vec2 shortPoint = shortLayey_->getChildByTag(shortLayey_->shortcurTag[1] + 100)->getPosition();
			int offx = shortPoint.x;
			int offy = shortPoint.y;
			if (mainscene_->isHeadMenuOn)
			{
				offy = offy + mainscene_->headMenu->getContentSize().height + 11;
			}

			auto action = (ActionInterval *)Sequence::create(
				DelayTime::create(0.5f),
				Spawn::create(
					MoveTo::create(0.8f, Vec2(offx, offy)),
					RotateBy::create(0.8f, 360),
					ScaleTo::create(0.8f, 1.1f),
					NULL),
				CallFuncN::create(CC_CALLBACK_1(GetSkillTeach::callBackStudySkill,this)),
				RemoveSelf::create(),
				NULL);
			pSpriteIcon->runAction(action);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}
