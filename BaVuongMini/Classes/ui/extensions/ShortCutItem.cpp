#include "ShortCutItem.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "../backpackscene/PackageScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../generals_ui/GeneralsUI.h"
#include "../generals_ui/RecuriteActionItem.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../generals_ui/GeneralsListUI.h"
#include "../../AppMacros.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "gamescene_state/EffectDispatch.h"

#define btn_label_des_tag 1100

ShortCutItem::ShortCutItem(void)
{
}


ShortCutItem::~ShortCutItem(void)
{
	CC_SAFE_DELETE(m_folder);
}

ShortCutItem * ShortCutItem::create(FolderInfo * foler,long long roleId,bool setPriority )
{
	auto item_ = new ShortCutItem();
	if (item_ && item_->init(foler,roleId,setPriority))
	{
		item_->autorelease();
		return item_;
	}
	CC_SAFE_DELETE(item_);
	return NULL;
}

bool ShortCutItem::init(FolderInfo * foler,long long roleId,bool setPriority )
{
	if (UIScene::init())
	{
		winsize =Director::getInstance()->getVisibleSize();

		m_folder =  new FolderInfo();
		m_folder->CopyFrom(*foler);

		panel_ =Layout::create();
		panel_->setAnchorPoint(Vec2(0.5f,0.5f));
		panel_->setPosition(Vec2(0,0));
		m_pLayer->addChild(panel_);

		auto imageMainFrame = ImageView::create();
		imageMainFrame->loadTexture("res_ui/kuangb.png");
		imageMainFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		imageMainFrame->setPosition(Vec2(0,0));
		panel_->addChild(imageMainFrame);

		auto buttonEqip= Button::create();
		buttonEqip->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
		buttonEqip->setTouchEnabled(true);
		buttonEqip->setPressedActionEnabled(true);
		buttonEqip->setAnchorPoint(Vec2(0.5f,0.5f));
		buttonEqip->setScale9Enabled(true);
		buttonEqip->setContentSize(Size(85,35));
		buttonEqip->setCapInsets(Rect(18,9,2,23));
		buttonEqip->setPosition(Vec2(0,buttonEqip->getContentSize().height/2-imageMainFrame->getContentSize().height/2+17));
		imageMainFrame->addChild(buttonEqip);

		auto btnParticle = CCTutorialParticle::create("tuowei0.plist",65,40);
		btnParticle->setPosition(Vec2(buttonEqip->getPosition().x,buttonEqip->getPosition().y+buttonEqip->getContentSize().height/2+6));
		btnParticle->setTag(CCTUTORIALPARTICLETAG);
		buttonEqip->addChild(btnParticle);

		auto btn_des=Label::createWithTTF("0",APP_FONT_NAME, 18);
		btn_des->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_des->setPosition(Vec2(0,0));
		btn_des->setTag(btn_label_des_tag);
		buttonEqip->addChild(btn_des);

		auto buttonClose = Button::create();
		buttonClose->loadTextures("res_ui/close.png","res_ui/close.png","");
		buttonClose->setTouchEnabled(true);
		buttonClose->setPressedActionEnabled(true);
		buttonClose->setAnchorPoint(Vec2(0.5f,0.5f));
		buttonClose->setPosition(Vec2(imageMainFrame->getContentSize().width/2 -10,imageMainFrame->getContentSize().height/2 - 10));
		buttonClose->addTouchEventListener(CC_CALLBACK_2(ShortCutItem::callBackCloseUi, this));
		imageMainFrame->addChild(buttonClose);

		int equipmentquality_ = m_folder->goods().quality();
		m_curGoodsRemAmount = m_folder->quantity();
		Color3B colonColor = GameView::getInstance()->getGoodsColorByQuality(equipmentquality_);
		auto goodsName=Label::createWithTTF(foler->goods().name().c_str(), APP_FONT_NAME, 12);
		goodsName->setAnchorPoint(Vec2(0.5f,0.5f));
		goodsName->setPosition(Vec2(0, buttonEqip->getPosition().y + buttonEqip->getContentSize().height/2 + goodsName->getContentSize().height/2+2));
		goodsName->setColor(colonColor);
		panel_->addChild(goodsName);

		std::string frameColorPath =  getGoodsIconByQuality(equipmentquality_);
		auto Btn_pacItemFrame = Button::create();
		Btn_pacItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnabled(true);
		Btn_pacItemFrame->setScale9Enabled(true);
		Btn_pacItemFrame->setCapInsets(Rect(9.5f,9.5f,6.0f,6.0f));
		Btn_pacItemFrame->setContentSize(Size(65,65));
		Btn_pacItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(Vec2(0,goodsName->getPosition().y + goodsName->getContentSize().height/2+ Btn_pacItemFrame->getContentSize().height/2+5));
		Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(ShortCutItem::callBackShowEquipment,this));
		Btn_pacItemFrame->setPressedActionEnabled(true);
		panel_->addChild(Btn_pacItemFrame);
		Btn_pacItemFrame->setScale(0.85f);

		std::string  iconPath_ ="res_ui/props_icon/";
		std::string equipicon_ = m_folder->goods().icon();
		iconPath_.append(equipicon_);
		iconPath_.append(".png");

		auto goodsIcon_ =ImageView::create();
		goodsIcon_->loadTexture(iconPath_.c_str());
		goodsIcon_->setAnchorPoint(Vec2(0.5f,0.5f));
		goodsIcon_->setPosition(Vec2(0,0));
		Btn_pacItemFrame->addChild(goodsIcon_);

		if (m_folder->quantity() >1)
		{
			char goodsAmountStr[10];
			sprintf(goodsAmountStr,"%d",m_folder->quantity());
			goodsAmount =Label::createWithTTF(goodsAmountStr, APP_FONT_NAME, 14);
			goodsAmount->setAnchorPoint(Vec2(1,1));
			goodsAmount->setPosition(Vec2(21,-10));
			Btn_pacItemFrame->addChild(goodsAmount);
		}

		if (m_folder->goods().equipmentclazz() >0  && m_folder->goods().equipmentclazz() <11)
		{
			const char *strings = StringDataManager::getString("equip_getNewequip");
			btn_des->setString(strings);
			buttonEqip->addTouchEventListener(CC_CALLBACK_2(ShortCutItem::callBackDressOn, this));


			this->setFightcapacity(m_folder->goods().equipmentdetail().fightcapacity());
			this->setFolderType(type_equip);
			this->setEquipClazz(m_folder->goods().equipmentclazz());
			this->setProfession(m_folder->goods().equipmentdetail().profession());
			//show general info for equip of dresson
			createGeneralForDressOnEquip(roleId);

		}else
		{
			this->setFolderType(type_else);

			const char *strings = StringDataManager::getString("teachRemind_useGoodsOnce");
			btn_des->setString(strings);
			buttonEqip->addTouchEventListener(CC_CALLBACK_2(ShortCutItem::callBackUseGoodsEvent,this));
		}

		this->setItemIndex(m_folder->id());
		this->setItemId(m_folder->goods().id());

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(imageMainFrame->getContentSize());
		//this->setTouchPriority(11);
		//m_pLayer->setTouchPriority(10);

		return true;
	}
	return false;
}

void ShortCutItem::onEnter()
{
	UIScene::onEnter();
}

void ShortCutItem::onExit()
{
	UIScene::onExit();
}

void ShortCutItem::callBackCloseUi(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->removeFromParentAndCleanup(true);
		//erase first  folder_vector,if it size >0
		if (EffectEquipItem::folderItemVector.size() > 0)
		{
			std::vector<FolderInfo *>::iterator iter_ = EffectEquipItem::folderItemVector.begin();
			auto iterFoler = *iter_;
			EffectEquipItem::folderItemVector.erase(iter_);
			delete iterFoler;

			EffectEquipItem::reloadShortItem();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void ShortCutItem::callBackDressOn(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn_ = (Button *)pSender;
		btn_->loadTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");
		btn_->setTouchEnabled(false);
		auto label_des = (Label *)btn_->getChildByTag(btn_label_des_tag);
		const char *strings = StringDataManager::getString("equip_getNewequip_dressOn");
		label_des->setString(strings);

		GameMessageProcessor::sharedMsgProcessor()->sendReq(1303, m_folder, (void *)m_curGeneralId);
		//
		auto scene = GameView::getInstance()->getGameScene();
		auto pSprite = Sprite::create("res_ui/font/ableUp.png");
		pSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		pSprite->setPosition(Vec2(0, 100));
		pSprite->setScale(0.75f);

		auto effect = CCLegendAnimation::create("animation/texiao/renwutexiao/SJTX/sjtx1.anm");

		if (m_curGeneralId == 0)
		{
			//add role
			GameView::getInstance()->myplayer->addChild(pSprite);
			GameView::getInstance()->myplayer->addEffect(effect, false, 0);
		}
		else
		{
			//add general
			auto target = dynamic_cast<BaseFighter*>(scene->getActor(m_curGeneralId));
			if (target)
			{
				target->addChild(pSprite);
				target->addEffect(effect, false, 0);
			}
		}

		auto  action = Sequence::create(
			Show::create(),
			DelayTime::create(0.3f),
			MoveBy::create(0.5f, Vec2(0, 40)),
			RemoveSelf::create(),
			NULL);

		pSprite->runAction(action);

		//add general card icon is runaction
		if (this->getChildByTag(CARD_LAYERTAG))
		{
			auto layer_ = (Layer *)this->getChildByTag(CARD_LAYERTAG);
			if (m_curGeneralId == 0)
			{
				//role
				if (layer_->getChildByTag(CARD_ROLE_FRAMETAG))
				{
					auto imageFrame = (ImageView *)layer_->getChildByTag(CARD_ROLE_FRAMETAG);
					if (imageFrame->getChildByTag(CARD_ROLE_ICONTAG))
					{
						auto imagerIcon = (ImageView *)imageFrame->getChildByTag(CARD_ROLE_ICONTAG);

						auto seq_ = Sequence::create(
							ScaleTo::create(0.1f, 1.2f),
							ScaleTo::create(0.1f, 1.0f),
							DelayTime::create(0.3f),
							CallFuncN::create(CC_CALLBACK_1(ShortCutItem::callBackCloseUi,this, Widget::TouchEventType::ENDED)),
							NULL);

						imagerIcon->runAction(seq_);
					}
				}
			}
			else
			{
				//general
				if (layer_->getChildByTag(CARD_GENERAL_FRAMETAG))
				{
					auto sp_frame = (Sprite *)layer_->getChildByTag(CARD_GENERAL_FRAMETAG);
					if (sp_frame->getChildByTag(CARD_GENERAL_ICONTAG))
					{
						auto node_ = (Node *)sp_frame->getChildByTag(CARD_GENERAL_ICONTAG);
						auto seq_ = Sequence::create(
							ScaleTo::create(0.1f, 1.3f),//0.85f
							ScaleTo::create(0.1f, 1.0f),//0.7f
							DelayTime::create(0.3f),
							CallFuncN::create(CC_CALLBACK_1(ShortCutItem::callBackCloseUi, this, Widget::TouchEventType::ENDED)),
							NULL);
						node_->runAction(seq_);
					}
				}
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void ShortCutItem::createGeneralForDressOnEquip( long long roleId )
{
	if (this->getChildByTag(CARD_LAYERTAG))
	{
		auto layer_ = (Layer *)this->getChildByTag(CARD_LAYERTAG);
		layer_->removeFromParentAndCleanup(true);
	}

	auto temp_layer = Layer::create();
	temp_layer->setTag(CARD_LAYERTAG);
	addChild(temp_layer);

	m_curGeneralId = roleId;

	if (roleId == 0)
	{
		auto smaillFrame = ImageView::create();
		smaillFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		smaillFrame->setPosition(Vec2(0, 0));
		smaillFrame->loadTexture("res_ui/generals_white.png");
		smaillFrame->setTag(CARD_ROLE_FRAMETAG);
		smaillFrame->setScaleY(0.98f);
		temp_layer->addChild(smaillFrame);
		//icon
		auto sprite_icon = ImageView::create();
		sprite_icon->loadTexture(BasePlayer::getBigHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str());
		sprite_icon->setScale(0.98f);
		sprite_icon->setAnchorPoint(Vec2(0.5f, 0));
		sprite_icon->setPosition(Vec2(0,-sprite_icon->getContentSize().width/2));
		sprite_icon->setTag(CARD_ROLE_ICONTAG);
		smaillFrame->addChild(sprite_icon);
		//level
		auto sprite_lvFrame = ImageView::create();
		sprite_lvFrame->setScale9Enabled(true);
		sprite_lvFrame->loadTexture("res_ui/zhezhao80.png");
		sprite_lvFrame->setContentSize(Size(34,13));
		sprite_lvFrame->setAnchorPoint(Vec2(1.0f, 0));
		sprite_lvFrame->setPosition(Vec2(smaillFrame->getContentSize().width/2-7, -smaillFrame->getContentSize().height/2+25));
		//sprite_lvFrame->setPosition(Vec2(0,0));
		smaillFrame->addChild(sprite_lvFrame);

		std::string _lv = "LV";
		char s[10];
		sprintf(s,"%d",GameView::getInstance()->myplayer->getActiveRole()->level());
		_lv.append(s);
		auto label_lv = Label::createWithTTF(_lv.c_str(), APP_FONT_NAME, 13);
		label_lv->setAnchorPoint(Vec2(1.0f, 0));
		label_lv->setPosition(Vec2(0,0));
		sprite_lvFrame->addChild(label_lv);
		//name backGround
		auto sprite_nameFrame = ImageView::create();
		sprite_nameFrame->setCapInsets(Rect(10,10,1,5));
		sprite_nameFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
		sprite_nameFrame->loadTexture("res_ui/name_di3.png");
		sprite_nameFrame->setContentSize(Size(72,13));
		sprite_nameFrame->setPosition(Vec2(0,-smaillFrame->getContentSize().height/2+sprite_nameFrame->getContentSize().height/2+8));
		smaillFrame->addChild(sprite_nameFrame);
		//name
		const char * generalRoleName_ = StringDataManager::getString("generalList_RoleName");
		auto label_name = Label::createWithTTF(generalRoleName_, APP_FONT_NAME, 14);
		label_name->setAnchorPoint(Vec2(0.5f, 0.5f));
		label_name->setPosition(Vec2(0,0));
		label_name->setColor(Color3B(0,255,0));
		sprite_nameFrame->addChild(label_name);
	}else
	{
		auto generalBaseMsg = new CGeneralBaseMsg();
		for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();i++)
		{
			if (m_curGeneralId == GameView::getInstance()->generalsInLineList.at(i)->id())
			{
				generalBaseMsg->CopyFrom(*GameView::getInstance()->generalsInLineList.at(i));
			}
		}

		auto generalBaseMsgFromDB  = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];

		auto sprite_frame = Sprite::create(GeneralsUI::getBigHeadFramePath(generalBaseMsg->currentquality()).c_str());
		sprite_frame->setAnchorPoint(Vec2(0.5f, 0.5f));
		sprite_frame->setPosition(Vec2(0,0));
		sprite_frame->setTag(CARD_GENERAL_FRAMETAG);
		sprite_frame->setScaleY(0.98f);
		temp_layer->addChild(sprite_frame);

		//general icon
		auto temp_node = Sprite::create("res_ui/di.png");
		temp_node->setAnchorPoint(Vec2(0.5f,0));
		temp_node->setPosition(Vec2(sprite_frame->getContentSize().width/2,sprite_frame->getContentSize().height/2));
		temp_node->setTag(CARD_GENERAL_ICONTAG);
		sprite_frame->addChild(temp_node);

		std::string sprite_icon_path = "res_ui/generals/";
		sprite_icon_path.append(generalBaseMsgFromDB->get_half_photo());
		sprite_icon_path.append(".anm");
		auto la_head = CCLegendAnimation::create(sprite_icon_path);
		if (la_head)
		{
			la_head->setPlayLoop(true);
			la_head->setReleaseWhenStop(false);
			//la_head->setPosition(Vec2(6,25));
			la_head->setPosition(Vec2(6-46,25 - 72));
			la_head->setScale(0.7f);
			temp_node->addChild(la_head);
		}
		//level
		auto sprite_lvFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80.png");
		sprite_lvFrame->setAnchorPoint(Vec2(1.0f, 0));
		sprite_lvFrame->setContentSize(Size(34,13));
		sprite_lvFrame->setPosition(Vec2(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-8, 25));
		sprite_frame->addChild(sprite_lvFrame);
		//general level
		std::string _lv = "LV";
		char s[5];
		sprintf(s,"%d",generalBaseMsg->level());
		_lv.append(s);
		auto label_lv = Label::createWithTTF(_lv.c_str(),APP_FONT_NAME,13);
		label_lv->setAnchorPoint(Vec2(0.5f,0.5f));
		label_lv->setPosition(Vec2(sprite_lvFrame->getContentSize().width/2,label_lv->getContentSize().height/2));
		label_lv->enableOutline(Color4B::BLACK, 2.0f);
		label_lv->setTag(265);
		sprite_lvFrame->addChild(label_lv);
		//general name
		auto label_name = Label::createWithTTF(generalBaseMsgFromDB->name().c_str(),APP_FONT_NAME,14);
		label_name->setAnchorPoint(Vec2(0.5f, 0));
		label_name->enableOutline(Color4B::BLACK, 2.0f);
		label_name->setPosition(Vec2(sprite_frame->getContentSize().width/2,7));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		sprite_frame->addChild(label_name);
		//rare
		if (generalBaseMsg->rare()>0)
		{
			auto sprite_star = Sprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			sprite_star->setAnchorPoint(Vec2(0,0));
			sprite_star->setScale(0.6f);
			sprite_star->setPosition(Vec2(5, 25));
			sprite_frame->addChild(sprite_star);
		}
		//general post
		if (generalBaseMsg->evolution() > 0)
		{
			auto sprite_rank = ActorUtils::createGeneralRankSprite(generalBaseMsg->evolution());
			sprite_rank->setAnchorPoint(Vec2(1.0f,1.0f));
			sprite_rank->setScale(0.7f);
			sprite_rank->setPosition(Vec2(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, sprite_frame->getContentSize().height*sprite_frame->getScaleY()-9));
			sprite_frame->addChild(sprite_rank);
		}

		if(generalBaseMsg->fightstatus() == GeneralsListUI::InBattle)
		{
			//fight flag
			auto imageView_inBattle = Sprite::create("res_ui/wujiang/play.png");
			imageView_inBattle->setAnchorPoint(Vec2(0.5,0.5));
			imageView_inBattle->setPosition(Vec2(17,127));
			imageView_inBattle->setScale(0.8f);
			sprite_frame->addChild(imageView_inBattle);
		}
	}
	//
	temp_layer->setPosition(Vec2(-108,2));
}

void ShortCutItem::callBackShowEquipment(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto goods_ = new GoodsInfo();
		goods_->CopyFrom(m_folder->goods());

		auto goodsbase_ = GoodsItemInfoBase::create(goods_, GameView::getInstance()->EquipListItem, 0);
		goodsbase_->setIgnoreAnchorPointForPosition(false);
		goodsbase_->setAnchorPoint(Vec2(0.5f, 0.5f));
		GameView::getInstance()->getMainUIScene()->addChild(goodsbase_);
		delete goods_;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void ShortCutItem::callBackUseGoodsEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->AllPacItem.at(m_folder->id())->has_goods())
		{
			if (m_folder->goods().id() == GameView::getInstance()->AllPacItem.at(m_folder->id())->goods().id())
			{
				PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
				temp->index = m_folder->id();
				temp->num = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0, temp);
				delete temp;
				m_curGoodsRemAmount--;
				if (m_curGoodsRemAmount <= 0)
				{
					this->removeFromParentAndCleanup(true);
				}
				else
				{
					char str[10];
					sprintf(str, "%d", m_curGoodsRemAmount);
					//goodsAmount->setText(str);
				}
			}
			else
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("teachRemind_useGoodsFail_dealog"));
				this->closeAnim();
			}
		}
		else
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("teachRemind_useGoodsFail_dealog"));
			this->closeAnim();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

bool ShortCutItem::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	Vec2 location = pTouch->getLocation();
	Vec2 location_temp = Vec2(location.x+54,location.y+73);

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos -anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location_temp))
	{
		return true;
	}
	return false;
}

std::string ShortCutItem::getGoodsIconByQuality( int quality_ )
{
	std::string frameColorPath = "res_ui/";
	if (quality_ == 1)
	{
		frameColorPath.append("sdi_white");
	}
	else if (quality_ == 2)
	{
		frameColorPath.append("sdi_green");
	}
	else if (quality_ == 3)
	{
		frameColorPath.append("sdi_bule");
	}
	else if (quality_ == 4)
	{
		frameColorPath.append("sdi_purple");
	}
	else if (quality_ == 5)
	{
		frameColorPath.append("sdi_orange");
	}
	else
	{
		frameColorPath.append("sdi_white");
	}
	frameColorPath.append(".png");

	return frameColorPath;
}

void ShortCutItem::setFightcapacity( int fightValue )
{
	m_fightcapacity = fightValue;
}

int ShortCutItem::getFightcapacity()
{
	return m_fightcapacity;
}

void ShortCutItem::setFolderType( int typeValue )
{
	m_typeValue = typeValue;
}

int ShortCutItem::getFolderType()
{
	return m_typeValue;
}

void ShortCutItem::setProfession( int profession_ )
{
	m_profession = profession_;
}

int ShortCutItem::getProfession()
{
	return m_profession;
}

void ShortCutItem::setLevel( int level_ )
{
	m_level = level_;
}

int ShortCutItem::getLevel()
{
	return m_level;
}

void ShortCutItem::setItemId( std::string id_ )
{
	m_itemId = id_;
}

std::string ShortCutItem::getItemId()
{
	return m_itemId;
}

void ShortCutItem::setItemIndex( int index_ )
{
	m_itemIndex = index_;
}

int ShortCutItem::getItemIndex()
{
	return m_itemIndex;
}

void ShortCutItem::setEquipClazz( int clazz_ )
{
	m_clazz = clazz_;
}

int ShortCutItem::getEquipClazz()
{
	return m_clazz;
}
