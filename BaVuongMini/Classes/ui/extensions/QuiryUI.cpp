#include "QuiryUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CEquipment.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../backpackscene/EquipmentItemInfos.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../../gamescene_state/role/GameActorAnimation.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/CActiveRole.h"
#include "AppMacros.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "CCRichLabel.h"
#include "../backpackscene/PackageScene.h"
#include "../generals_ui/RecuriteActionItem.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "RecruitGeneralCard.h"
#include "cocostudio\CCSGUIReader.h"

#define  SelectImageTag 458

using namespace cocos2d::ui;
USING_NS_CC_EXT;

QuiryUI::QuiryUI(void)
{
	curRoleIndex = 0;
}


QuiryUI::~QuiryUI(void)
{
	delete roleValue_;
	std::vector<CEquipment*>::iterator iterEquip;
	for (iterEquip = playerEquipments.begin(); iterEquip != playerEquipments.end(); ++iterEquip)
	{
		delete *iterEquip;
	}
	playerEquipments.clear();

	std::vector<CGeneralDetail*>::iterator iterDetail;
	for (iterDetail = generalDetails.begin(); iterDetail != generalDetails.end(); ++iterDetail)
	{
		delete *iterDetail;
	}
	generalDetails.clear();
}

QuiryUI * QuiryUI::create(CActiveRole * activerole,std::vector<CEquipment *>euipmentsVector,std::vector<CGeneralDetail *>generalDetailVector)
{
	auto quiry= new QuiryUI();
	if (quiry && quiry->init(activerole,euipmentsVector,generalDetailVector))
	{
		quiry->autorelease();
		return quiry;
	}
	CC_SAFE_DELETE(quiry);
	return NULL;
}

bool QuiryUI::init(CActiveRole * activerole,std::vector<CEquipment *>euipmentsVector,std::vector<CGeneralDetail *>generalDetailVector)
{
	if (UIScene::init())
	{
		winsize= Director::getInstance()->getVisibleSize();
		if(LoadSceneLayer::quiryPanel->getParent() != NULL)
		{
			LoadSceneLayer::quiryPanel->removeFromParentAndCleanup(false);
		}

		m_Layer= Layer::create();
		m_Layer->setIgnoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(Vec2(0.5f,0.5f));
		m_Layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_Layer->setContentSize(Size(800,480));
		this->addChild(m_Layer);

		auto quiryPanel_ = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/zhuangbeitanchukuang_1.json");
		quiryPanel_->setAnchorPoint(Vec2(0.5f,0.5f));
		quiryPanel_->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_Layer->addChild(quiryPanel_);
		
		imagePression = (ui::ImageView *)ui::Helper::seekWidgetByName(quiryPanel_,"image_jobvalue");
		labelLevel = (Label *)ui::Helper::seekWidgetByName(quiryPanel_,"Label_roleLevel");
		labelFightValue_ = (Label *)ui::Helper::seekWidgetByName(quiryPanel_,"Label_fp_value");
		/*
		ParticleSystem* particleFire = ParticleSystemQuad::create("animation/texiao/particledesigner/huo2.plist");
		particleFire->setPositionType(ParticleSystem::PositionType::FREE);
		particleFire->setPosition(Vec2(400,181));
		particleFire->setAnchorPoint(Vec2(0.5f,0.5f));
		particleFire->setVisible(true);
		//particleFire->setLife(0.5f);
		particleFire->setLifeVar(0);
		particleFire->setScale(0.5f);
		particleFire->setScaleX(0.6f);
		m_Layer->addChild(particleFire);

		ParticleSystem* particleFire1 = ParticleSystemQuad::create("animation/texiao/particledesigner/huo2.plist");
		particleFire1->setPositionType(ParticleSystem::PositionType::FREE);
		particleFire1->setPosition(Vec2(405,184));
		particleFire1->setAnchorPoint(Vec2(0.5f,0.5f));
		particleFire1->setVisible(true);
		//particleFire1->setLife(0.5f);
		particleFire1->setLifeVar(0);
		particleFire1->setScale(0.5f);
		particleFire1->setScaleX(0.6f);
		m_Layer->addChild(particleFire1);
		*/
		// 
		ImageView_arms = (ImageView *)Helper::seekWidgetByName(quiryPanel_,"ImageView_arms");
		ImageView_clothes = (ImageView *)Helper::seekWidgetByName(quiryPanel_,"ImageView_clothes");
		ImageView_ring = (ImageView *)Helper::seekWidgetByName(quiryPanel_,"ImageView_ring");
		ImageView_helmet = (ImageView *)Helper::seekWidgetByName(quiryPanel_,"ImageView_helmet");
		ImageView_shoes = (ImageView *)Helper::seekWidgetByName(quiryPanel_,"ImageView_shoes");
		ImageView_accessories = (ImageView *)Helper::seekWidgetByName(quiryPanel_,"ImageView_accessories");
		this->setEquipImageVisible(true);

		auto u_tempLayer = Layer::create();
		m_Layer->addChild(u_tempLayer);

		/*Label * l_allFPName = Label::create();
		l_allFPName->setText(StringDataManager::getString("rank_capacity"));
		l_allFPName->setFntFile("res_ui/font/ziti_1.fnt");
		l_allFPName->setAnchorPoint(Vec2(0.5f,0.5f));
		l_allFPName->setPosition(Vec2(185,167));
		l_allFPName->setScale(0.6f);
		u_tempLayer->addChild(l_allFPName);*/
		auto Image_allFPName = ImageView::create();
		Image_allFPName->loadTexture("res_ui/renwubeibao/zi2.png");
		Image_allFPName->setAnchorPoint(Vec2(0.5f,0.5f));
		Image_allFPName->setPosition(Vec2(355,187));//(177,168)
		Image_allFPName->setScale(1.0f);
		u_tempLayer->addChild(Image_allFPName);

		labelAllFightValue_ = Label::createWithBMFont("res_ui/font/ziti_1.fnt", "123456789");
		labelAllFightValue_->setAnchorPoint(Vec2(0.5f,0.5f));
		labelAllFightValue_->setPosition(Vec2(437,186));
		labelAllFightValue_->setScale(0.6f);
		u_tempLayer->addChild(labelAllFightValue_);

		roleValue_ = new CActiveRole();
		roleValue_->CopyFrom(*activerole);

		for (int i=0;i<euipmentsVector.size();i++)
		{
			auto equip =new CEquipment();
			equip->CopyFrom(*euipmentsVector.at(i));
			playerEquipments.push_back(equip);
		}

		for (int i=0;i<generalDetailVector.size();i++)
		{
			auto detail_ =new CGeneralDetail();
			detail_->CopyFrom(*generalDetailVector.at(i));
			generalDetails.push_back(detail_);
		}

		auto generalList = TableView::create(this, Size(307, 109));//280
		generalList->setTouchEnabled(true);
		//generalList->setSelectedScale9Texture("res_ui/highlight.png", Rect(26, 26, 1, 1), Vec2(0,0));
		generalList->setDirection(TableView::Direction::HORIZONTAL);
		generalList->setAnchorPoint(Vec2(0,0));
		generalList->setPosition(Vec2(246,55));
		generalList->setDelegate(this);
		generalList->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//generalList->setPressedActionEnabled(true);
		m_Layer->addChild(generalList);
		generalList->reloadData();

		//
		auto image_left_flag = (ImageView *)Helper::seekWidgetByName(quiryPanel_,"ImageView_arrow_left");
		image_left_flag->setVisible(false);
		auto image_right_flag = (ImageView *)Helper::seekWidgetByName(quiryPanel_,"ImageView_arrow_right");
		image_right_flag->setVisible(false);
		//add flag
		auto tempLayer_flag = Layer::create();
// 		tempLayer_flag->setIgnoreAnchorPointForPosition(false);
// 		tempLayer_flag->setAnchorPoint(Vec2(0.5f,0.5f));
// 		tempLayer_flag->setContentSize(Size(800, 480));
// 		tempLayer_flag->setPosition(Vec2(winsize.width/2,winsize.height/2));
		tempLayer_flag->setLocalZOrder(5);
		m_Layer->addChild(tempLayer_flag);

		auto imageView_leftFlag = ImageView::create();
		imageView_leftFlag->loadTexture("res_ui/shousuo.png");
		imageView_leftFlag->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_leftFlag->setPosition(Vec2(256,110));//231 110
		imageView_leftFlag->setRotation(-135);
		imageView_leftFlag->setScale(0.5f);
		tempLayer_flag->addChild(imageView_leftFlag);
		auto imageView_rightFlag = ImageView::create();
		imageView_rightFlag->loadTexture("res_ui/shousuo.png");
		imageView_rightFlag->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_rightFlag->setPosition(Vec2(544,110));
		imageView_rightFlag->setRotation(45);
		imageView_rightFlag->setScale(0.5f);
		tempLayer_flag->addChild(imageView_rightFlag);


		this->refreshCurRoleValue();

		auto buttonExit =(Button *)Helper::seekWidgetByName(quiryPanel_,"Button_close");
		buttonExit->setTouchEnabled(true);
		buttonExit->setPressedActionEnabled(true);
		buttonExit->addTouchEventListener(CC_CALLBACK_2(QuiryUI::callBackExit, this));
	
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setPosition(Vec2::ZERO);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void QuiryUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void QuiryUI::onExit()
{
	UIScene::onExit();
}

void QuiryUI::callBackExit(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void QuiryUI::callBackShowEquipInfo(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = dynamic_cast<Button *>(pSender);
		int num = btn->getTag();
		int amount_ = 0;

		auto goods_ = new GoodsInfo();
		goods_->CopyFrom(playerEquipments.at(num)->goods());

		for (int i = 0; i<playerEquipments.size(); i++)
		{
			if (playerEquipments.at(i)->goods().equipmentdetail().suitid() == goods_->equipmentdetail().suitid())
			{
				amount_++;
			}
		}

		this->setRoleEquipAmount(amount_);
		auto equipmentItemInfos = GoodsItemInfoBase::create(goods_, GameView::getInstance()->EquipListItem, 0);
		equipmentItemInfos->setIgnoreAnchorPointForPosition(false);
		equipmentItemInfos->setAnchorPoint(Vec2(0.5f, 0.5f));
		//equipmentItemInfos->setPosition(Vec2(winsize.width/2,winsize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(equipmentItemInfos);

		delete goods_;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void QuiryUI::callBackShowGeneralEquipInfo(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = dynamic_cast<Button *>(pSender);
		int num = btn->getTag();

		int amount_ = 0;

		auto goods_ = new GoodsInfo();
		goods_->CopyFrom(generalDetails.at(curRoleIndex)->equipments(num).goods());
		for (int i = 0; i<generalDetails.at(curRoleIndex)->equipments_size(); i++)
		{
			if (generalDetails.at(curRoleIndex)->equipments(i).goods().equipmentdetail().suitid() == goods_->equipmentdetail().suitid())
			{
				amount_++;
			}
		}

		this->setRoleEquipAmount(amount_);

		auto equipmentItemInfos = GoodsItemInfoBase::create(goods_, GameView::getInstance()->EquipListItem, 0);
		equipmentItemInfos->setIgnoreAnchorPointForPosition(false);
		equipmentItemInfos->setAnchorPoint(Vec2(0.5f, 0.5f));
		//equipmentItemInfos->setPosition(Vec2(winsize.width/2,winsize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(equipmentItemInfos);

		delete goods_;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void QuiryUI::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void QuiryUI::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void QuiryUI::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void QuiryUI::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	int cellId = cell->getIdx();
	auto roleLayer_ = (Layer *)this->getChildByTag(ROLELAYEROFEQUIP);
	if (roleLayer_ != NULL)
	{
		roleLayer_->removeFromParentAndCleanup(true);
	}
	auto generalLayer_ = (Layer *)this->getChildByTag(GENERALLAYEROFEQUIP);
	if (generalLayer_ != NULL)
	{
		generalLayer_->removeFromParentAndCleanup(true);
	}
	if (cellId == 0)
	{
		curRoleIndex = 0;
		this->refreshCurRoleValue();
	}else
	{
		curRoleIndex = cellId - 1;
		this->refreshCurGeneralValue(cellId - 1);
	}
}

cocos2d::Size QuiryUI::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(81,103);
}

cocos2d::extension::TableViewCell* QuiryUI::tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx)
{
	 auto cell = table->dequeueCell();
	 cell = new TableViewCell();
	 cell->autorelease();

	 auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	 spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	 spriteBg->setAnchorPoint(Vec2::ZERO);
	 spriteBg->setPosition(Vec2(0, 0));
	 //spriteBg->setColor(Color3B(0, 0, 0));
	 spriteBg->setTag(SelectImageTag);
	 spriteBg->setOpacity(0);
	 cell->addChild(spriteBg);

	 if (idx == 0)
	 {
		 //selectActorId = 0;
		 auto smaillFrame = Sprite::create("res_ui/generals_white.png");
		 smaillFrame->setAnchorPoint(Vec2(0, 0));
		 smaillFrame->setPosition(Vec2(0, 0));
		 smaillFrame->setScaleX(0.75f);
		 smaillFrame->setScaleY(0.7f);
		 cell->addChild(smaillFrame);
		 //�б��е�ͷ��ͼ�
		 std::string pressionStr_ =roleValue_->profession();
		 int roleIconIndex_ =  BasePlayer::getProfessionIdxByName(pressionStr_.c_str());
		 std::string roleIcon_ = BasePlayer::getBigHeadPathByProfession(roleIconIndex_);
		 
		 auto sprite_icon = Sprite::create(roleIcon_.c_str());
		 sprite_icon->setAnchorPoint(Vec2(0.5f, 0.5f));
		 sprite_icon->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), smaillFrame->getContentSize().height/2*smaillFrame->getScaleY()));
		 sprite_icon->setScale(0.75f);
		 cell->addChild(sprite_icon);
		 //
// 		 cocos2d::extension::Scale9Sprite * sprite_lvFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80.png");
// 		 sprite_lvFrame->setAnchorPoint(Vec2(1.0f, 0));
// 		 sprite_lvFrame->setContentSize(Size(34,13));
// 		 sprite_lvFrame->setPosition(Vec2(smaillFrame->getContentSize().width*smaillFrame->getScaleX()-5, 19));
// 		 cell->addChild(sprite_lvFrame);
// 		 //�ȼ�
// 		 std::string _lv = "LV";
// 		 char roleLevel[5];
// 		 sprintf(roleLevel,"%d",roleValue_->level());
// 		 _lv.append(roleLevel);
// 		 auto label_lv = Label::createWithTTF(_lv.c_str(),APP_FONT_NAME,13);
// 		 label_lv->setAnchorPoint(Vec2(1.0f, 0));
// 		 label_lv->setPosition(Vec2(smaillFrame->getContentSize().width*smaillFrame->getScaleX()-5, 17));
// 		 label_lv->enableStroke(Color3B(0, 0, 0), 2.0f);
// 		 cell->addChild(label_lv);

		 auto sprite_nameFrame = Sprite::create("res_ui/name_di2.png");
		 sprite_nameFrame->setAnchorPoint(Vec2(0.5f, 0));
		 sprite_nameFrame->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 6));
		 sprite_nameFrame->setScaleX(1.1f);
		 cell->addChild(sprite_nameFrame);
		 //�佫���
		 const char * generalRoleName_ = StringDataManager::getString("generalList_RoleName");
		 auto label_name = Label::createWithTTF(generalRoleName_,APP_FONT_NAME,14);
		 label_name->setAnchorPoint(Vec2(0.5f, 0));
		 label_name->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 5));
		 label_name->setColor(Color3B(0,255,0));
		 cell->addChild(label_name);
		 //profession
		 std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(roleIconIndex_);
		 auto sprite_profession = Sprite::create(generalProfess_.c_str());
		 sprite_profession->setAnchorPoint(Vec2(0.5f,0.5f));
		 sprite_profession->setPosition(Vec2(62 ,
			 32));
		 sprite_profession->setScale(0.6f);
		 cell->addChild(sprite_profession);
	 }
	 else
	 {
 		 int curGeneralQuality = generalDetails.at(idx-1)->currentquality();
 		 auto sprite_frame = Sprite::create(GeneralsUI::getBigHeadFramePath(curGeneralQuality).c_str());
 		 sprite_frame->setAnchorPoint(Vec2(0, 0));
 		 sprite_frame->setPosition(Vec2(0, 0));
 		 sprite_frame->setScaleX(0.75f);
 		 sprite_frame->setScaleY(0.7f);
 		 cell->addChild(sprite_frame);
 		
 		 auto generalBaseMsgFromDB  = GeneralsConfigData::s_generalsBaseMsgData[generalDetails.at(idx-1)->modelid()];
 		 std::string curGeneralFace_ = generalBaseMsgFromDB->get_half_photo();
 		 //��б��е�ͷ��ͼ�
 		 std::string sprite_icon_path = "res_ui/generals/";
 		 sprite_icon_path.append(curGeneralFace_);
 		 sprite_icon_path.append(".anm");
		 auto la_head = CCLegendAnimation::create(sprite_icon_path);
		 if (la_head)
		 {
			 la_head->setPlayLoop(true);
			 la_head->setReleaseWhenStop(false);
			 la_head->setScale(0.7f*0.75f);	
			 la_head->setPosition(Vec2(4,13));
			 cell->addChild(la_head);
		 }
 		 //�ȼ�
		 auto sprite_lvFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80.png");
 		 sprite_lvFrame->setAnchorPoint(Vec2(1.0f, 0));
 		 sprite_lvFrame->setContentSize(Size(34,13));
 		 sprite_lvFrame->setPosition(Vec2(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, 19));
 		 cell->addChild(sprite_lvFrame);
 		 //�佫�ȼ�
 		 int curGeneralLevel = generalDetails.at(idx-1)->activerole().level();
 		 std::string _lv = "LV";
 		 char generalLevel_[10];
 		 sprintf(generalLevel_,"%d",curGeneralLevel);
 		 _lv.append(generalLevel_);
 		 auto label_lv = Label::createWithTTF(_lv.c_str(),APP_FONT_NAME,13);
 		 label_lv->setAnchorPoint(Vec2(1.0f, 0));
 		 label_lv->setPosition(Vec2(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, 18));
 		 label_lv->enableOutline(Color4B::BLACK, 2.0f);
 		 cell->addChild(label_lv);
 
		 auto sprite_nameFrame = Sprite::create("res_ui/name_di2.png");
 		 sprite_nameFrame->setAnchorPoint(Vec2(0.5f, 0));
 		 sprite_nameFrame->setPosition(Vec2(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 6));
 		 sprite_nameFrame->setScaleX(1.1f);
 		 cell->addChild(sprite_nameFrame);
 		 //�佫���
 		 std::string curGeneralname_ = generalDetails.at(idx-1)->activerole().rolebase().name();
 
		 auto label_name=CCRichLabel::createWithString(curGeneralname_.c_str(),Size(390,38),NULL,NULL,0);
 		 label_name->setScale(0.7f);
 		 label_name->setAnchorPoint(Vec2(0.5f,0));
 		 label_name->setPosition(Vec2(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 5));
 		 cell->addChild(label_name);
 
		 //�ϡ�ж
		 if (generalDetails.at(idx-1)->rare()>0)
		 {
			 auto sprite_star = Sprite::create(RecuriteActionItem::getStarPathByNum(generalDetails.at(idx-1)->rare()).c_str());
			 sprite_star->setAnchorPoint(Vec2(0,0));
			 sprite_star->setScale(0.6f);
			 sprite_star->setPosition(Vec2(5, 21));
			 cell->addChild(sprite_star);
		 }
 		 //��佫���
 		 int curGeneralEvol_ = generalDetails.at(idx-1)->evolution();
 		 if (curGeneralEvol_ > 0)
 		 {
 			 std::string rankPathBase = "res_ui/rank/rank";
 			 char s[10];
 			 sprintf(s,"%d",curGeneralEvol_);
 			 rankPathBase.append(s);
 			 rankPathBase.append(".png");
			 auto sprite_rank = Sprite::create(rankPathBase.c_str());
			 sprite_rank->setScale(0.7f);
 			 sprite_rank->setAnchorPoint(Vec2(1.0f,1.0f));
			 sprite_rank->setPosition(Vec2(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, sprite_frame->getContentSize().height*sprite_frame->getScaleY()-6));
			 cell->addChild(sprite_rank);
 		 }
		 //profession
		 std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(generalBaseMsgFromDB->get_profession());
		 auto sprite_profession = Sprite::create(generalProfess_.c_str());
		 sprite_profession->setAnchorPoint(Vec2(0.5f,0.5f));
		 sprite_profession->setPosition(Vec2(62 ,
			 32));
		 sprite_profession->setScale(0.6f);
		 cell->addChild(sprite_profession);
	 }

	 return cell;
}

ssize_t QuiryUI::numberOfCellsInTableView(TableView *table )
{
	return generalDetails.size()+1;
}


void QuiryUI::scrollViewDidScroll( cocos2d::extension::ScrollView* view )
{
}

void QuiryUI::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{
}

void QuiryUI::refreshCurGeneralValue(int idx)
{
	auto generalLayer= Layer::create();
	generalLayer->setIgnoreAnchorPointForPosition(false);
	generalLayer->setAnchorPoint(Vec2(0.5f,0.5f));
	generalLayer->setPosition(Vec2(winsize.width/2,winsize.height/2));
	generalLayer->setContentSize(Size(800,480));
	generalLayer->setTag(GENERALLAYEROFEQUIP);
	this->addChild(generalLayer);

	std::string generalName_ = generalDetails.at(idx)->activerole().rolebase().name();
	auto label_name=CCRichLabel::createWithString(generalName_.c_str(),Size(390,38),NULL,NULL,0);
	label_name->setAnchorPoint(Vec2(0.5f,0));
	label_name->setPosition(Vec2(400,390));
	generalLayer->addChild(label_name);
	this->setEquipImageVisible(true);
	std::string pressionStr_ = generalDetails.at(idx)->activerole().profession().c_str();
	std::string professionPath = BasePlayer::getProfessionIconByStr(pressionStr_.c_str());
	imagePression->loadTexture(professionPath.c_str());

	int levelStr_ =  generalDetails.at(idx)->activerole().level();
	char lvStr_[5];
	sprintf(lvStr_,"%d",levelStr_);
	std::string roleLevelStr_ ="LV.";
	roleLevelStr_.append(lvStr_);
	labelLevel->setString(roleLevelStr_.c_str());

	int fightPoint = generalDetails.at(idx)->fightpoint();
	char fightStr[20];
	sprintf(fightStr,"%d",fightPoint);
	labelFightValue_->setString(fightStr);
	////////////
	int roleFightPoint = roleValue_->fightpoint();
	for (int i = 0;i<generalDetails.size();++i)
	{
		roleFightPoint += generalDetails.at(i)->fightpoint();
	}
	char allFightStr[20];
	sprintf(allFightStr,"%d",roleFightPoint);
	labelAllFightValue_->setString(allFightStr);

	int vectorSize = generalDetails.at(idx)->equipments_size();
	std::string weapEffectStrForRefine="";
	std::string weapEffectStrForStar="";

	for(int i=0;i<vectorSize;i++)
	{
		int equipmentquality_ = generalDetails.at(idx)->equipments(i).goods().quality();
		std::string frameColorPath = "res_ui/";
		if (equipmentquality_ == 1)
		{
			frameColorPath.append("sdi_white");
		}
		else if (equipmentquality_ == 2)
		{
			frameColorPath.append("sdi_green");
		}
		else if (equipmentquality_ == 3)
		{
			frameColorPath.append("sdi_bule");
		}
		else if (equipmentquality_ == 4)
		{
			frameColorPath.append("sdi_purple");
		}
		else if (equipmentquality_ == 5)
		{
			frameColorPath.append("sdi_orange");
		}
		else
		{
			frameColorPath.append("sdi_white");
		}
		frameColorPath.append(".png");

		auto Btn_pacItemFrame = Button::create();
		Btn_pacItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnabled(true);
		Btn_pacItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_pacItemFrame->setTag(i);
		Btn_pacItemFrame->setPressedActionEnabled(true);
		Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(QuiryUI::callBackShowGeneralEquipInfo, this));
		generalLayer->addChild(Btn_pacItemFrame);

		switch(generalDetails.at(idx)->equipments(i).part())
		{
		case part_arms:
			{
				Btn_pacItemFrame->setPosition(Vec2(284.5f, 381.5f));    //���� 4
				//add weap effect
				int pression_ = generalDetails.at(idx)->equipments(i).goods().equipmentdetail().profession();
				int refineLevel =  generalDetails.at(idx)->equipments(i).goods().equipmentdetail().gradelevel();
				int starLevel =  generalDetails.at(idx)->equipments(i).goods().equipmentdetail().starlevel();
				weapEffectStrForRefine = ActorUtils::getWeapEffectByRefineLevel(pression_,refineLevel);
				weapEffectStrForStar = ActorUtils::getWeapEffectByStarLevel(pression_,starLevel);

				ImageView_arms->setVisible(false);
			}break;
		case part_clothes:
			{
				Btn_pacItemFrame->setPosition(Vec2(284.5f, 301.5f));   //��· 2
				ImageView_clothes->setVisible(false);
			}break;
		case part_helmet:
			{
				Btn_pacItemFrame->setPosition(Vec2(515, 381.5f));    //�ͷ�  0
				ImageView_helmet->setVisible(false);
			}break;
		case part_ring:
			{
				Btn_pacItemFrame->setPosition(Vec2(284.5f, 220.5f));    //��ָ7
				ImageView_ring->setVisible(false);
			}break;
		case part_accessories:
			{
				Btn_pacItemFrame->setPosition(Vec2(515, 222));    //�� 3
				ImageView_accessories->setVisible(false);
			}break;
		case part_shoes:
			{
				Btn_pacItemFrame->setPosition(Vec2(515, 302));    //�Ь 9
				ImageView_shoes->setVisible(false);
			}break;
		}

		std::string  iconPath_ ="res_ui/props_icon/";
		std::string equipicon_ = generalDetails.at(idx)->equipments(i).goods().icon();
		iconPath_.append(equipicon_);
		iconPath_.append(".png");

		auto equipIcon_ =ImageView::create();
		equipIcon_->loadTexture(iconPath_.c_str());
		equipIcon_->setAnchorPoint(Vec2(0.5f,0.5f));
		equipIcon_->setPosition(Vec2(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		if (generalDetails.at(idx)->equipments(i).goods().binding() == 1)
		{
			auto ImageView_bound = ImageView::create();
			ImageView_bound->loadTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(Vec2(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(Vec2(-25,23));
			Btn_pacItemFrame->addChild(ImageView_bound);
		}

		int starLv_ = generalDetails.at(idx)->equipments(i).goods().equipmentdetail().starlevel();
		if (starLv_>0)
		{
			char starLvLabel_[5];
			sprintf(starLvLabel_,"%d",starLv_);
			auto label_ =Label::createWithTTF(starLvLabel_, APP_FONT_NAME, 13);
			label_->setAnchorPoint(Vec2(0,0));
			label_->setPosition(Vec2(-23,-23));
			Btn_pacItemFrame->addChild(label_);

			auto imageStar_ =ImageView::create();
			imageStar_->loadTexture("res_ui/star_on.png");
			imageStar_->setScale(0.7f);
			imageStar_->setAnchorPoint(Vec2(0,0));
			imageStar_->setPosition(Vec2(label_->getContentSize().width-22,label_->getPosition().y));
			Btn_pacItemFrame->addChild(imageStar_);
		}

		int strengthLv_ = generalDetails.at(idx)->equipments(i).goods().equipmentdetail().gradelevel();
		if (strengthLv_>0)
		{
			char strengthLv[5];
			sprintf(strengthLv,"%d",strengthLv_);
			std::string strengthStr_ = "+";
			strengthStr_.append(strengthLv);

			auto label_StrengthLv = Label::createWithTTF(strengthStr_.c_str(),APP_FONT_NAME,13);
			label_StrengthLv->setAnchorPoint(Vec2(1,1));
			label_StrengthLv->setPosition(Vec2(23,25));
			Btn_pacItemFrame->addChild(label_StrengthLv);
		}

		if (generalDetails.at(idx)->equipments(i).goods().equipmentdetail().durable()<= 10)
		{
			auto image_sp = ImageView::create();
			image_sp->setAnchorPoint(Vec2(0.5,0.5f));
			image_sp->setPosition(Vec2(0,0));
			image_sp->loadTexture("res_ui/mengban_red55.png");
			image_sp->setScale9Enabled(true);
			image_sp->setCapInsets(Rect(5,5,1,1));
			image_sp->setContentSize(Btn_pacItemFrame->getContentSize());
			Btn_pacItemFrame->addChild(image_sp);

			auto l_des = Label::createWithTTF(StringDataManager::getString("packageitem_notAvailable"), APP_FONT_NAME,18);
			l_des->setAnchorPoint(Vec2(0.5f,0.5f));
			l_des->setPosition(Vec2(0,0));
			image_sp->addChild(l_des);
		}
	}
	// show the player's animation
	CActiveRole * generalActive = new CActiveRole();
	generalActive->CopyFrom(generalDetails.at(idx)->activerole());
	std::string roleFigureName = ActorUtils::getActorFigureName(*generalActive);
	GameActorAnimation* pAnim = ActorUtils::createActorAnimation(roleFigureName.c_str(), 
																roleValue_->hand().c_str(),
																	weapEffectStrForRefine.c_str(), weapEffectStrForStar.c_str());
	pAnim->setPosition(Vec2(400,250));
	generalLayer->addChild(pAnim);

	delete generalActive;
}

void QuiryUI::refreshCurRoleValue()
{
	auto roleLayer= Layer::create();
	roleLayer->setIgnoreAnchorPointForPosition(false);
	roleLayer->setAnchorPoint(Vec2(0.5f,0.5f));
	roleLayer->setPosition(Vec2(winsize.width/2,winsize.height/2));
	roleLayer->setContentSize(Size(800,480));
	roleLayer->setTag(ROLELAYEROFEQUIP);
	this->addChild(roleLayer);

	std::string roleName_ = roleValue_->rolebase().name();
	auto label_name=CCRichLabel::createWithString(roleName_.c_str(),Size(390,38),NULL,NULL,0);
	label_name->setAnchorPoint(Vec2(0.5f,0));
	label_name->setPosition(Vec2(400,390));
	roleLayer->addChild(label_name);
	this->setEquipImageVisible(true);
	int  vipLevel_ = roleValue_->playerbaseinfo().viplevel();
	if (vipLevel_ > 0)
	{
		auto widgetVip = MainScene::addVipInfoByLevelForWidget(vipLevel_);
		roleLayer->addChild(widgetVip);
		widgetVip->setPosition(Vec2(label_name->getPositionX() + label_name->getContentSize().width/2 +10,label_name->getPositionY() + widgetVip->getContentSize().height/2));
	}
	
	//int countryId_  = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().country();
	int countryId_ = roleValue_->playerbaseinfo().country();
	if (countryId_ > 0 && countryId_ <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId_);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		auto countrySp_ = Sprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(Vec2(1,0.5f));
		countrySp_->setPosition(Vec2(label_name->getPositionX()- label_name->getContentSize().width/2*0.9f,label_name->getPositionY()+countrySp_->getContentSize().height/2*0.8f));
		countrySp_->setScale(0.65f);
		roleLayer->addChild(countrySp_);
	}

	std::string pressionStr_ = roleValue_->profession().c_str();
	std::string professionPath = BasePlayer::getProfessionIconByStr(pressionStr_.c_str());
	imagePression->loadTexture(professionPath.c_str());
	
	int levelStr_ =  roleValue_->level();
	char lvStr_[5];
	sprintf(lvStr_,"%d",levelStr_);
	std::string roleLevelStr_ ="LV.";
	roleLevelStr_.append(lvStr_);
	labelLevel->setString(roleLevelStr_.c_str());
	//////////////////////////////
	int fightPoint = roleValue_->fightpoint();
	char fightStr[20];
	sprintf(fightStr,"%d",fightPoint);
	labelFightValue_->setString(fightStr);
	///////////////////////
	int allFightPoint = roleValue_->fightpoint();
	for (int i = 0;i<generalDetails.size();++i)
	{
		allFightPoint += generalDetails.at(i)->fightpoint();
	}
	char allFightStr[20];
	sprintf(allFightStr,"%d",allFightPoint);
	labelAllFightValue_->setString(allFightStr);

	int vectorSize = playerEquipments.size();
	std::string weapEffectStrForRefine="";
	std::string weapEffectStrForStar="";
	for(int i=0;i<vectorSize;i++)
	{
		int equipmentquality_ = playerEquipments.at(i)->goods().quality();
		std::string frameColorPath = "res_ui/";
		if (equipmentquality_ == 1)
		{
			frameColorPath.append("sdi_white");
		}
		else if (equipmentquality_ == 2)
		{
			frameColorPath.append("sdi_green");
		}
		else if (equipmentquality_ == 3)
		{
			frameColorPath.append("sdi_bule");
		}
		else if (equipmentquality_ == 4)
		{
			frameColorPath.append("sdi_purple");
		}
		else if (equipmentquality_ == 5)
		{
			frameColorPath.append("sdi_orange");
		}
		else
		{
			frameColorPath.append("sdi_white");
		}
		frameColorPath.append(".png");

		auto Btn_pacItemFrame = Button::create();
		Btn_pacItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnabled(true);
		Btn_pacItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_pacItemFrame->setPressedActionEnabled(true);
		Btn_pacItemFrame->setTag(i);
		Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(QuiryUI::callBackShowEquipInfo, this));
		roleLayer->addChild(Btn_pacItemFrame);

		switch(playerEquipments.at(i)->part())
		{
		case part_arms:
			{
				Btn_pacItemFrame->setPosition(Vec2(284.5f, 381.5f));    //��� 4
				//
				int pression_ = playerEquipments.at(i)->goods().equipmentdetail().profession();
				int refineLevel =  playerEquipments.at(i)->goods().equipmentdetail().gradelevel();
				int starLevel =  playerEquipments.at(i)->goods().equipmentdetail().starlevel();

				weapEffectStrForRefine = ActorUtils::getWeapEffectByRefineLevel(pression_,refineLevel);
				weapEffectStrForStar = ActorUtils::getWeapEffectByStarLevel(pression_,starLevel);
				ImageView_arms->setVisible(false);
			}break;
		case part_clothes:
			{
				Btn_pacItemFrame->setPosition(Vec2(284.5f, 301.5f));   //��· 2
				ImageView_clothes->setVisible(false);
			}break;
		case part_helmet:
			{
				Btn_pacItemFrame->setPosition(Vec2(515, 381.5f));    //�ͷ�  0
				ImageView_helmet->setVisible(false);
			}break;
		case part_ring:
			{
				Btn_pacItemFrame->setPosition(Vec2(284.5f, 220.5f));    //��ָ7
				ImageView_ring->setVisible(false);
			}break;
		case part_accessories:
			{
				Btn_pacItemFrame->setPosition(Vec2(515, 222));    //�� 3
				ImageView_accessories->setVisible(false);
			}break;
		case part_shoes:
			{
				Btn_pacItemFrame->setPosition(Vec2(515, 302));    //�Ь 9
				ImageView_shoes->setVisible(false);
			}break;
		}

		std::string  iconPath_ ="res_ui/props_icon/";
		std::string equipicon_ = playerEquipments.at(i)->goods().icon();
		iconPath_.append(equipicon_);
		iconPath_.append(".png");

		auto equipIcon_ =ImageView::create();
		equipIcon_->loadTexture(iconPath_.c_str());
		equipIcon_->setAnchorPoint(Vec2(0.5f,0.5f));
		equipIcon_->setPosition(Vec2(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		if (playerEquipments.at(i)->goods().binding() == 1)
		{
			auto ImageView_bound = ImageView::create();
			ImageView_bound->loadTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(Vec2(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(Vec2(-25,23));
			Btn_pacItemFrame->addChild(ImageView_bound);
		}

		int starLv_ =playerEquipments.at(i)->goods().equipmentdetail().starlevel();
		if (starLv_>0)
		{
			char starLvLabel_[5];
			sprintf(starLvLabel_,"%d",starLv_);
			auto label_ =Label::createWithTTF(starLvLabel_, APP_FONT_NAME, 13);
			label_->setAnchorPoint(Vec2(0,0));
			label_->setPosition(Vec2(-23,-23));
			Btn_pacItemFrame->addChild(label_);

			auto imageStar_ =ImageView::create();
			imageStar_->loadTexture("res_ui/star_on.png");
			imageStar_->setScale(0.7f);
			imageStar_->setAnchorPoint(Vec2(0,0));
			imageStar_->setPosition(Vec2(label_->getContentSize().width-22,label_->getPosition().y));
			Btn_pacItemFrame->addChild(imageStar_);
		}

		int strengthLv_ = playerEquipments.at(i)->goods().equipmentdetail().gradelevel();
		if (strengthLv_>0)
		{
			char strengthLv[5];
			sprintf(strengthLv,"%d",strengthLv_);
			std::string strengthStr_ = "+";
			strengthStr_.append(strengthLv);

			auto label_StrengthLv = Label::createWithTTF(strengthStr_.c_str(), APP_FONT_NAME, 13);
			label_StrengthLv->setAnchorPoint(Vec2(1,1));
			label_StrengthLv->setPosition(Vec2(23,25));
			Btn_pacItemFrame->addChild(label_StrengthLv);
		}

		if (playerEquipments.at(i)->goods().equipmentdetail().durable()<= 10)
		{
			auto image_sp = ImageView::create();
			image_sp->setAnchorPoint(Vec2(0.5,0.5f));
			image_sp->setPosition(Vec2(0,0));
			image_sp->loadTexture("res_ui/mengban_red55.png");
			image_sp->setScale9Enabled(true);
			image_sp->setCapInsets(Rect(5,5,1,1));
			image_sp->setContentSize(Btn_pacItemFrame->getContentSize());
			Btn_pacItemFrame->addChild(image_sp);

			auto l_des = Label::createWithTTF(StringDataManager::getString("packageitem_notAvailable"), APP_FONT_NAME, 18);
			l_des->setAnchorPoint(Vec2(0.5f,0.5f));
			l_des->setPosition(Vec2(0,0));
			image_sp->addChild(l_des);
		}
	}

	// show the player's animation
	std::string roleFigureName = ActorUtils::getActorFigureName(*roleValue_);
	GameActorAnimation* pAnim = ActorUtils::createActorAnimation(roleFigureName.c_str(), 
																		roleValue_->hand().c_str(),
																		weapEffectStrForRefine.c_str(), weapEffectStrForStar.c_str());
	pAnim->setPosition(Vec2(400,250));
	roleLayer->addChild(pAnim);
}

bool QuiryUI::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return true;
}

void QuiryUI::setRoleEquipAmount(int amount)
{
	curRoleEquipQuility = amount;
}

int QuiryUI::getRoleEquipAmount()
{
	return curRoleEquipQuility;
}

void QuiryUI::setEquipImageVisible( bool value_ )
{
	ImageView_arms->setVisible(value_);
	ImageView_clothes->setVisible(value_);
	ImageView_ring->setVisible(value_);
	ImageView_helmet->setVisible(value_);
	ImageView_shoes->setVisible(value_);
	ImageView_accessories->setVisible(value_);
}
