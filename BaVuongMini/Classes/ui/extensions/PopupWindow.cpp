#include "PopupWindow.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "GameView.h"
#include "AppMacros.h"
#include "CCRichLabel.h"

using namespace cocos2d::ui;

PopupWindow::PopupWindow():
senderListener(NULL),
senderSelectorSure(NULL),
senderSelectorCancel(NULL)
{
}

PopupWindow::~PopupWindow()
{
}

PopupWindow * PopupWindow::create(std::string string,int buttonNum,PopWindowType winType, int remainTime)
{
	auto dialog = new PopupWindow();
	if(dialog && dialog->init(string, buttonNum,winType,remainTime))
	{
		dialog->autorelease();
		return dialog;
	}
	CC_SAFE_DELETE(dialog);
	return NULL;
}

bool PopupWindow::init(std::string string,int buttonNum,PopWindowType winType, int remainTime)
{
	if (UIScene::init())
	{
		curWindowType = winType;
		m_nRemainTime = remainTime;

		Size winsize = Size(800,480);

		auto backGroundSp = cocos2d::extension::Scale9Sprite::create("res_ui/dikuang_new.png");
		backGroundSp->setCapInsets(Rect(0,0,0,0));
		backGroundSp->setPreferredSize(Size(377,201));
		backGroundSp->setAnchorPoint(Vec2(0.5f,0.5f));
		backGroundSp->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(backGroundSp);

		auto backGroundSp_frame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_didia.png");
		backGroundSp_frame->setCapInsets(Rect(13,14,1,1));
		backGroundSp_frame->setPreferredSize(Size(300,108));
		backGroundSp_frame->setAnchorPoint(Vec2(0.5f,1.0f));
		backGroundSp_frame->setPosition(Vec2(backGroundSp->getContentSize().width/2 - 3 ,180));
		backGroundSp->addChild(backGroundSp_frame);

		auto labelLink = CCRichLabel::createWithString(string.c_str(),Size(290,50),NULL,NULL,0,18,2);
		labelLink->setAnchorPoint(Vec2(0,1));
		labelLink->setPosition(Vec2(45,backGroundSp->getContentSize().height-30));
		backGroundSp->addChild(labelLink);

		auto layerbutton = Layer::create();
		layerbutton->setIgnoreAnchorPointForPosition(false);
		layerbutton->setAnchorPoint(Vec2(0.5f,0.5f));
		layerbutton->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layerbutton->setContentSize(winsize);
		addChild(layerbutton);

		if (buttonNum == 2)
		{
			buttonSure = Button::create();
			buttonSure->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
			buttonSure->setTouchEnabled(true);
			buttonSure->setPressedActionEnabled(true);
			buttonSure->addTouchEventListener(CC_CALLBACK_2(PopupWindow::sureEvent, this));
			buttonSure->setAnchorPoint(Vec2(0.5f,0.5f));
			buttonSure->setScale9Enabled(true);
			buttonSure->setContentSize(Size(110,43));
			buttonSure->setCapInsets(Rect(18,9,2,23));
			buttonSure->setPosition(Vec2(layerbutton->getContentSize().width/2-87,layerbutton->getContentSize().height/2 - backGroundSp->getContentSize().height/2 +48));
			layerbutton->addChild(buttonSure);

			auto btn_labelSure= Label::createWithTTF(StringDataManager::getString("popupWindow_sure"), APP_FONT_NAME,18);
			btn_labelSure->setAnchorPoint(Vec2(0.5f,0.5f));
			btn_labelSure->setPosition(Vec2(0,0));
			buttonSure->addChild(btn_labelSure);

			buttonCancel = Button::create();
			buttonCancel->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
			buttonCancel->setTouchEnabled(true);
			buttonCancel->setPressedActionEnabled(true);
			buttonCancel->addTouchEventListener(CC_CALLBACK_2(PopupWindow::cancelEvent, this));
			buttonCancel->setAnchorPoint(Vec2(0.5f,0.5f));
			buttonCancel->setScale9Enabled(true);
			buttonCancel->setContentSize(Size(110,43));
			buttonCancel->setCapInsets(Rect(18,9,2,23));
			buttonCancel->setPosition(Vec2(layerbutton->getContentSize().width/2+82,buttonSure->getPosition().y));
			layerbutton->addChild(buttonCancel);

			auto btn_labelCancel= Label::createWithTTF(StringDataManager::getString("popupWindow_cancel"), APP_FONT_NAME, 18);
			btn_labelCancel->setAnchorPoint(Vec2(0.5f,0.5f));
			btn_labelCancel->setPosition(Vec2(0,0));
			buttonCancel->addChild(btn_labelCancel);
		}else
		{
			buttonSure = Button::create();
			buttonSure->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
			buttonSure->setTouchEnabled(true);
			buttonSure->setPressedActionEnabled(true);
			buttonSure->addTouchEventListener(CC_CALLBACK_2(PopupWindow::sureEvent,this));
			buttonSure->setAnchorPoint(Vec2(0.5f,0.5f));
			buttonSure->setScale9Enabled(true);
			buttonSure->setContentSize(Size(110,43));
			buttonSure->setCapInsets(Rect(18,9,2,23));
			buttonSure->setPosition(Vec2(layerbutton->getContentSize().width/2,layerbutton->getContentSize().height/2 - backGroundSp->getContentSize().height/2 +48));
			layerbutton->addChild(buttonSure);

			auto btn_labelSure= Label::createWithTTF(StringDataManager::getString("con_reconnection_text"), APP_FONT_NAME, 18);
			btn_labelSure->setAnchorPoint(Vec2(0.5f,0.5f));
			btn_labelSure->setPosition(Vec2(0,0));
			buttonSure->addChild(btn_labelSure);
		}

		if (winType == KtypeNeverRemove)
		{
			
		}
		else if (winType = KTypeRemoveByTime)
		{
			std::string str_remainTime = "(";
			char s_remainTime[10];
			sprintf(s_remainTime,"%d",m_nRemainTime);
			str_remainTime.append(s_remainTime);
			str_remainTime.append(")");
			
			centerTimeLabel = Label::createWithTTF(str_remainTime.c_str(), APP_FONT_NAME, 20);
			centerTimeLabel->setAnchorPoint(Vec2(0.5f,0.5f));
			centerTimeLabel->setPosition(Vec2(backGroundSp->getContentSize().width/2 - 3,backGroundSp->getContentSize().height/2 - 10));
			backGroundSp->addChild(centerTimeLabel);
			this->schedule(schedule_selector(PopupWindow::update),1.0f);
		}

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setPosition(Vec2::ZERO);
		this->setContentSize(winsize);

		//this->setTouchPriority(TPriority_PopUpWindow);
		//m_pLayer->setTouchPriority(TPriority_PopUpWindow-1);
		//layerbutton->setTouchPriority(TPriority_PopUpWindow - 2);

		return true;
	}
	return false;
}

void PopupWindow::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void PopupWindow::onExit()
{
	UIScene::onExit();
}

void PopupWindow::AddcallBackEvent( Ref* pSender, SEL_CallFuncO pSelectorSure, SEL_CallFuncO pSelectorCancel )
{
	senderListener = pSender;
	senderSelectorSure = pSelectorSure;
	senderSelectorCancel =pSelectorCancel;
}

void PopupWindow::sureEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		if (senderListener)
		{
			(senderListener->*senderSelectorSure)(this);
		}
		/*if (senderListener)
		{
		auto callback = CCCallFuncO::create(senderListener, senderSelectorSure, NULL);
		auto seq = Sequence::create(callback);
		this->runAction(seq);
		}*/

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void PopupWindow::cancelEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (senderListener)
		{
			(senderListener->*senderSelectorCancel)(this);
		}
		/*if (senderListener)
		{
		auto callback = CCCallFuncO::create(senderListener, senderSelectorCancel, NULL);
		auto seq = Sequence::create(callback);
		this->runAction(seq);
		}*/
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

bool PopupWindow::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return true;
}
void PopupWindow::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void PopupWindow::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void PopupWindow::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void PopupWindow::update( float delta )
{
	if (curWindowType == KtypeNeverRemove)
		return;

	if (curWindowType == KTypeRemoveByTime)
	{
		m_nRemainTime--;
		if (m_nRemainTime <= 0)
		{
			this->closeAnim();
		}

		std::string str_remainTime = "(";
		char s_remainTime[10];
		sprintf(s_remainTime,"%d",m_nRemainTime);
		str_remainTime.append(s_remainTime);
		str_remainTime.append(")");

		centerTimeLabel->setString(str_remainTime.c_str());
	}
}

void PopupWindow::setButtonPosition()
{
	Vec2 p1 = buttonSure->getPosition();
	Vec2 p2 = buttonCancel->getPosition();

	buttonSure->setPosition(p2);
	buttonCancel->setPosition(p1);
}

void PopupWindow::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void PopupWindow::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,115,45,true);
	tutorialIndicator->setDrawNodePos(Vec2(_w+400 - 85,_h+240 - 51));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void PopupWindow::removeCCTutorialIndicator()
{
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}
