#include "UIButtonTree.h"

USING_NS_CC;
using namespace cocos2d::ui;
NS_CC_BEGIN

ButtonTree::ButtonTree()
{

}

ButtonTree::~ButtonTree()
{

}

ButtonTree * ButtonTree::create()
{
	auto widget = new ButtonTree();
	if (widget && widget->init())
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}

bool ButtonTree::init()
{
	if(Button::init())
	{
		return true;
	}
	return false;
}

void ButtonTree::onEnter()
{
	Button::onEnter();
}

void ButtonTree::onExit()
{
	Button::onExit();
}


void ButtonTree::setInfo( int tag )
{
	m_parentTag = tag;
}

int ButtonTree::getInfo()
{
	return m_parentTag;
}

NS_CC_END