#ifndef _MAILUI_ANNEXITEM_H
#endif _MAILUI_ANNEXITEM_H
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../backpackscene/GoodsItemInfoBase.h"

USING_NS_CC;
USING_NS_CC_EXT;
/*
* FloderItemInstance �Ǵ���һ�folder�Ϊ��Ϣ����Ʒ�����Ե��鿴
* GoodsItemInstance  �Ǵ���һ�goods�Ϊ��Ϣ����Ʒ�����Ե��鿴
* FloderItemInfo �ǵ�FloderItemInstance ���GoodsItemInstanceߵ�������
*/
class FolderInfo;
class FloderItemInstance:public UIScene
{

public:
	enum typeEnum
	{
		ktype_auction = 1,
		ktype_mail_addAnnex = 2,
	}; 
public:
	FloderItemInstance();
	~FloderItemInstance();

	static FloderItemInstance * create(FolderInfo* folder,int type,int amount = 0);
	bool init(FolderInfo* folder,int type,int amount);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch * pTouch,Event * pEvent);

	void callBackShowAuctionItem(Ref *pSender, Widget::TouchEventType type);

	void callBackShowMailItem(Ref *pSender, Widget::TouchEventType type);

	void setFloderIndex(int index_);
	int getFloderIndex();
private:
	FolderInfo * m_floder;
	int m_type;
	std::string goodsSopbgIcon(int quality_);
	int m_floderIndex;
};

////////////////////////////////////////
class GoodsInfo;
class GoodsItemInstance:public UIScene
{
public:
	enum typeEnumGoods
	{
		ktype_mail_getBackAnnex = 1,
	}; 

public:
	GoodsItemInstance();
	~GoodsItemInstance();

	static GoodsItemInstance * create(GoodsInfo * goods,int type,int amount = 0);
	bool init(GoodsInfo * goods,int amount,int type);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch * pTouch,Event * pEvent);

	void callBackShowMailAnnexItem(Ref *pSender, Widget::TouchEventType type);
private:
	GoodsInfo * m_goods;
	int m_type;
	std::string goodsSopbgIcon(int quality_);
};

///////////////////////////////////////////
class GoodsInfo;
class FloderItemInfo:public GoodsItemInfoBase
{
public:
	enum FloderEnum
	{
		ktype_auctionItem = 1,
		ktype_mail_addAnnexItem = 2,
		ktype_mail_getBackAnnexItem = 3,
	}; 

public:
	FloderItemInfo();
	~FloderItemInfo();

	static FloderItemInfo * create(GoodsInfo * goods,int type);
	bool init(GoodsInfo * goods,int type);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch * pTouch,Event * pEvent);

	void callbackAuctionItem(Ref *pSender, Widget::TouchEventType type);

	void callBackAddMailAnnex(Ref *pSender, Widget::TouchEventType type);

	void callBackShowSendMailAnnex(Ref * obj);
};
