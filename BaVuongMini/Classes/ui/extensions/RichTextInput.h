#ifndef _RICHTEXT_INPUT_H_
#define _RICHTEXT_INPUT_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace cocos2d::ui;

class CCRichLabel;
class KeyboardNotificationLayer;

class KeyboardNotificationLayer : public Layer, public IMEDelegate
{
public:
    KeyboardNotificationLayer();

	

	virtual void onClickTrackNode(bool bClicked, const cocos2d::Vec2& touchPos) = 0;
    virtual bool isClickTrackNode(const Rect& trackNodeRect, const Vec2& touchPoint) = 0;

    //virtual void registerWithTouchDispatcher();
    virtual void keyboardWillShow(IMEKeyboardNotificationInfo& info);
    virtual void keyboardWillHide(IMEKeyboardNotificationInfo& info);

    // Layer
    virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
    virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
    
protected:
    void updateInputBoxPosition(bool bAnimated = false);
    
protected:
    static Node* s_currentClickingNode;

    Node * m_pTrackNode;
    Vec2  m_beginPos;
    float m_inputCursorOffsetY;
    
    Rect m_keyboardEndRect;
    
    // the vertical adjustment when keyboard appear
    float m_adjustVert;
};

//////////////////////////////////////////////////////////////////////////
// TextFieldTTFActionTest
//////////////////////////////////////////////////////////////////////////

class RichTextInputBox : public KeyboardNotificationLayer, public TextFieldDelegate, public ScrollViewDelegate
{
    TextFieldTTF *		m_pTextField;
    int                 m_nCharLimit;       // the textfield max char limit
	int					m_nInputBoxWidth;
    int					m_nInputBoxHeight;
	CCRichLabel *		m_pRichLabel;
	Sprite *			m_pCursorSprite;
    int                 m_direction;

public:
    typedef enum {
        kCCRichInputDirectionHorizontal = 0,
        kCCRichInputDirectionVertical = 1,
    } CCRichInputDirection;

   enum {
        kCCInputModeAppendOnly = 0,
        kCCInputModeFullText = 1,
    } ;
    
public:
	RichTextInputBox();

	virtual void onClickTrackNode(bool bClicked, const cocos2d::Vec2& touchPos) override;
    bool isClickTrackNode(const Rect& trackNodeRect, const Vec2& touchPoint);

    // Layer
    virtual void onEnter();
    virtual void onExit();

	void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	virtual bool onTextFieldAttachWithIME(cocos2d::TextFieldTTF*  sender) override;
	virtual bool onTextFieldDetachWithIME(cocos2d::TextFieldTTF*  sender) override;
	virtual bool onTextFieldInsertText(cocos2d::TextFieldTTF*  sender, const char * text, size_t nLen) override;
	virtual bool onTextFieldDeleteBackward(cocos2d::TextFieldTTF*  sender, const char * delText, size_t nLen) override;
	virtual bool onDraw(cocos2d::TextFieldTTF*  sender);

	void menuInsertEmotionCallback(Ref* pSender);
	void menuInsertItemCallback(Ref* pSender);

	const char* getInputString();
	void deleteAllInputString();

	//CCRichLabel* getTextTTF();

	void setCharLimit(int limit);
	inline void setInputBoxWidth(int width) { m_nInputBoxWidth = width; };
    inline void setInputBoxHeight(int height) { m_nInputBoxHeight = height; };
    inline int getInputBoxHeight() { return m_nInputBoxHeight; };
    
    void setDirection(int dir);
	void setMultiLinesMode(bool bMultiLins);
	void setInputMode(int mode);

private:
	/* when insert or delete char, adjust input cursor position */
	void adjustInputCursorPos();

	void sychronizeTextFieldTTF();

	int m_inputMode;
	bool m_bMultiLines;
};

#endif    // _RICHTEXT_INPUT_H_
