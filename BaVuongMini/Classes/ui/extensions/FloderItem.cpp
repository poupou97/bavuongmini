#include "FloderItem.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../Auction_ui/AuctionUi.h"
#include "../Mail_ui/MailUI.h"


FloderItemInstance::FloderItemInstance()
{
}

FloderItemInstance::~FloderItemInstance()
{
	delete m_floder;
}

FloderItemInstance * FloderItemInstance::create( FolderInfo* folder,int type,int amount /*= 0*/ )
{
	auto floder_ = new FloderItemInstance();
	if (floder_ && floder_->init(folder,type,amount))
	{
		floder_->autorelease();
		return floder_;
	}
	CC_SAFE_DELETE(floder_);
	return NULL;
}

bool FloderItemInstance::init( FolderInfo* folder,int type,int amount )
{
	if (UIScene::init())
	{
		m_floder = new FolderInfo();
		m_floder->CopyFrom(*folder);

		m_type = type;
		setFloderIndex(folder->id());

		std::string frameIconPath = goodsSopbgIcon(m_floder->goods().quality());
		auto Btn_pacItemFrame = Button::create();
		Btn_pacItemFrame->loadTextures(frameIconPath.c_str(),frameIconPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnabled(true);
		Btn_pacItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(Vec2(31,31));

		switch(type)
		{
		case  ktype_auction:
			{
				Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(FloderItemInstance::callBackShowAuctionItem, this));
			}break;
		case 2:
			{
				Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(FloderItemInstance::callBackShowMailItem, this));
			}break;
		case 3:
			{
				//Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(FloderItem::showGoodsInfo));
			}break;
		}

		m_pLayer->addChild(Btn_pacItemFrame);

		std::string goodsInfoStr ="res_ui/props_icon/";
		goodsInfoStr.append(m_floder->goods().icon());
		goodsInfoStr.append(".png");

		auto equipIcon_ = ui::ImageView::create();
		equipIcon_->loadTexture(goodsInfoStr.c_str());
		equipIcon_->setAnchorPoint(Vec2(0.5f,0.5f));
		equipIcon_->setPosition(Vec2(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		if (m_floder->goods().equipmentclazz() > 0 && m_floder->goods().equipmentclazz() < 11 )
		{
			int starLv_ = m_floder->goods().equipmentdetail().starlevel();
			if (starLv_>0)
			{
				char starLvLabel_[5];
				sprintf(starLvLabel_,"%d",starLv_);
				auto label_ =Label::createWithTTF(starLvLabel_, APP_FONT_NAME, 13);
				label_->setAnchorPoint(Vec2(0,0));
				label_->setPosition(Vec2(-25,-20));
				Btn_pacItemFrame->addChild(label_);

				auto imageStar_ =ui::ImageView::create();
				imageStar_->loadTexture("res_ui/star_on.png");
				imageStar_->setScale(0.5f);
				imageStar_->setAnchorPoint(Vec2(0,0));
				imageStar_->setPosition(Vec2(label_->getContentSize().width-27,label_->getPosition().y));
				Btn_pacItemFrame->addChild(imageStar_);
			}

			int strengthLv_ = m_floder->goods().equipmentdetail().gradelevel();
			if (strengthLv_>0)
			{
				char strengthLv[5];
				sprintf(strengthLv,"%d",strengthLv_);
				std::string strengthStr_ = "+";
				strengthStr_.append(strengthLv);

				auto label_StrengthLv = Label::createWithTTF(strengthStr_.c_str(), APP_FONT_NAME, 13);
				label_StrengthLv->setAnchorPoint(Vec2(1,1));
				label_StrengthLv->setPosition(Vec2(23,20));
				Btn_pacItemFrame->addChild(label_StrengthLv);
			}
		}

		if (amount > 1)
		{
			char amountStr[10];
			sprintf(amountStr,"%d",amount);
			auto label_amount = Label::createWithTTF(amountStr, APP_FONT_NAME, 13);
			label_amount->setAnchorPoint(Vec2(1,1));
			label_amount->setPosition(Vec2(20,-10));
			Btn_pacItemFrame->addChild(label_amount);
		}
		
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Btn_pacItemFrame->getContentSize());

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(FloderItemInstance::onTouchBegan, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
		return true;
	}
	return false;
}

void FloderItemInstance::onEnter()
{
	UIScene::onEnter();
}

void FloderItemInstance::onExit()
{
	UIScene::onExit();
}

std::string FloderItemInstance::goodsSopbgIcon( int quality_ )
{
	std::string frameColorPath = "res_ui/";

	switch(quality_)
	{
	case 1:
		{
			frameColorPath.append("sdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("sdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("sdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("sdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("sdi_orange");
		}break;
	default:
		{
			frameColorPath.append("sdi_white");
		}
	}
	frameColorPath.append(".png");

	return frameColorPath;
}

bool FloderItemInstance::onTouchBegan( Touch * pTouch,Event * pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}

void FloderItemInstance::callBackShowAuctionItem(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto auctionui = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
		if (auctionui != NULL)
		{
			auctionui->getbackAuction(NULL);
		}

		/*
		GoodsInfo * m_goods = new GoodsInfo();
		m_goods->CopyFrom(m_floder->goods());

		FloderItemInfo * floder_info =FloderItemInfo::create(m_goods,m_type);
		floder_info->setIgnoreAnchorPointForPosition(false);
		floder_info->setAnchorPoint(Vec2(0.5f,0.5f));
		GameView::getInstance()->getMainUIScene()->addChild(floder_info);

		delete m_goods;
		*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FloderItemInstance::callBackShowMailItem(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		MailUI *mail_ui = (MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
		if (mail_ui != NULL)
		{
			if (this->getTag() == REDUCTIONANNEXBUTTON1)
			{
				mail_ui->reductionAnnex1(pSender);
			}
			else
			{
				mail_ui->reductionAnnex2(pSender);
			}
		}

		/*
		GoodsInfo * m_goods = new GoodsInfo();
		m_goods->CopyFrom(m_floder->goods());

		FloderItemInfo * floder_info =FloderItemInfo::create(m_goods,m_type);
		floder_info->setIgnoreAnchorPointForPosition(false);
		floder_info->setAnchorPoint(Vec2(0.5f,0.5f));
		floder_info->setTag(this->getTag());
		GameView::getInstance()->getMainUIScene()->addChild(floder_info);

		delete m_goods;
		*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

int FloderItemInstance::getFloderIndex()
{
	return m_floderIndex;
}

void FloderItemInstance::setFloderIndex( int index_ )
{
	m_floderIndex = index_;
}


//////////////////////////////////////
GoodsItemInstance::GoodsItemInstance()
{
}

GoodsItemInstance::~GoodsItemInstance()
{
}

GoodsItemInstance * GoodsItemInstance::create( GoodsInfo * goods,int type,int amount /*= 0*/ )
{
	GoodsItemInstance * goods_ = new GoodsItemInstance();
	if (goods_ && goods_->init(goods,type,amount))
	{
		goods_->autorelease();
		return goods_;
	}
	CC_SAFE_DELETE(goods_);
	return NULL;
}

bool GoodsItemInstance::init( GoodsInfo * goods,int type,int amount )
{
	if (UIScene::init())
	{
		m_goods = new GoodsInfo();
		m_goods->CopyFrom(*goods);
		m_type = type;

		std::string frameIconPath = goodsSopbgIcon(goods->quality());
		auto Btn_pacItemFrame = Button::create();
		Btn_pacItemFrame->loadTextures(frameIconPath.c_str(),frameIconPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnabled(true);
		Btn_pacItemFrame->setPressedActionEnabled(true);
		Btn_pacItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(Vec2(31,31));

		switch(type)
		{
		case  ktype_mail_getBackAnnex:
			{
				Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(GoodsItemInstance::callBackShowMailAnnexItem, this));
			}break;
		case 2:
			{
			}break;
		}

		m_pLayer->addChild(Btn_pacItemFrame);

		std::string goodsInfoStr ="res_ui/props_icon/";
		goodsInfoStr.append(goods->icon());
		goodsInfoStr.append(".png");

		auto equipIcon_ =ui::ImageView::create();
		equipIcon_->loadTexture(goodsInfoStr.c_str());
		equipIcon_->setAnchorPoint(Vec2(0.5f,0.5f));
		equipIcon_->setPosition(Vec2(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		if (goods->equipmentclazz() > 0 && goods->equipmentclazz() < 11 )
		{
			int starLv_ = goods->equipmentdetail().starlevel();
			if (starLv_>0)
			{
				char starLvLabel_[5];
				sprintf(starLvLabel_,"%d",starLv_);
				auto label_ =Label::createWithTTF(starLvLabel_, APP_FONT_NAME, 13);
				label_->setAnchorPoint(Vec2(0,0));
				label_->setPosition(Vec2(-25,-20));
				Btn_pacItemFrame->addChild(label_);

				auto imageStar_ = ui::ImageView::create();
				imageStar_->loadTexture("res_ui/star_on.png");
				imageStar_->setScale(0.5f);
				imageStar_->setAnchorPoint(Vec2(0,0));
				imageStar_->setPosition(Vec2(label_->getContentSize().width-27,label_->getPosition().y));
				Btn_pacItemFrame->addChild(imageStar_);
			}

			int strengthLv_ =  goods->equipmentdetail().gradelevel();
			if (strengthLv_>0)
			{
				char strengthLv[5];
				sprintf(strengthLv,"%d",strengthLv_);
				std::string strengthStr_ = "+";
				strengthStr_.append(strengthLv);

				auto label_StrengthLv = Label::createWithTTF(strengthStr_.c_str(), APP_FONT_NAME,13);
				label_StrengthLv->setAnchorPoint(Vec2(1,1));
				label_StrengthLv->setPosition(Vec2(23,20));
				Btn_pacItemFrame->addChild(label_StrengthLv);
			}
		}

		if (amount > 1)
		{
			char amountStr[10];
			sprintf(amountStr,"%d",amount);
			auto label_amount = Label::createWithTTF(amountStr, APP_FONT_NAME,13);
			label_amount->setAnchorPoint(Vec2(1,1));
			label_amount->setPosition(Vec2(20,-10));
			Btn_pacItemFrame->addChild(label_amount);
		}
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Btn_pacItemFrame->getContentSize());
		return true;
	}
	return false;
}

void GoodsItemInstance::onEnter()
{
	UIScene::onEnter();
}

void GoodsItemInstance::onExit()
{
	UIScene::onExit();
}

std::string GoodsItemInstance::goodsSopbgIcon( int quality_ )
{
	std::string frameColorPath = "res_ui/";

	switch(quality_)
	{
	case 1:
		{
			frameColorPath.append("sdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("sdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("sdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("sdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("sdi_orange");
		}break;
	default:
		{
			frameColorPath.append("sdi_white");
		}
	}
	frameColorPath.append(".png");

	return frameColorPath;
}

bool GoodsItemInstance::onTouchBegan( Touch * pTouch,Event * pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}


void GoodsItemInstance::callBackShowMailAnnexItem(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		Size winsize = Director::getInstance()->getVisibleSize();
		GoodsItemInfoBase * goodsui = GoodsItemInfoBase::create(m_goods, GameView::getInstance()->EquipListItem, 0);
		goodsui->setIgnoreAnchorPointForPosition(false);
		goodsui->setAnchorPoint(Vec2(0.5f, 0.5f));
		GameView::getInstance()->getMainUIScene()->addChild(goodsui);

		// 
		// 	FloderItemInfo * floder_info =FloderItemInfo::create(m_goods,m_type);
		// 	floder_info->setIgnoreAnchorPointForPosition(false);
		// 	floder_info->setAnchorPoint(Vec2(0.5f,0.5f));
		// 	GameView::getInstance()->getMainUIScene()->addChild(floder_info);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

///////////////
FloderItemInfo::FloderItemInfo()
{
}

FloderItemInfo::~FloderItemInfo()
{
}

FloderItemInfo * FloderItemInfo::create( GoodsInfo * goods ,int type)
{
	FloderItemInfo * floderInfo =new FloderItemInfo();
	if (floderInfo && floderInfo->init(goods,type))
	{
		floderInfo->autorelease();
		return floderInfo;
	}
	CC_SAFE_DELETE(floderInfo);
	return NULL;
}

bool FloderItemInfo::init( GoodsInfo * goods ,int type)
{
	if (GoodsItemInfoBase::init(goods,GameView::getInstance()->EquipListItem,0))
	{
		const char *str_ = StringDataManager::getString("goods_auction_getback");
		auto Button_getAnnes= Button::create();
		Button_getAnnes->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_getAnnes->setTouchEnabled(true);
		Button_getAnnes->setPressedActionEnabled(true);
		Button_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_getAnnes->setScale9Enabled(true);
		Button_getAnnes->setContentSize(Size(80,43));
		Button_getAnnes->setCapInsets(Rect(18,9,2,23));
		Button_getAnnes->setPosition(Vec2(135,25));

		switch(type)
		{
		case ktype_auctionItem:
			{
				Button_getAnnes->addTouchEventListener(CC_CALLBACK_2(FloderItemInfo::callbackAuctionItem, this));
			}break;
		case ktype_mail_addAnnexItem:
			{
				Button_getAnnes->addTouchEventListener(CC_CALLBACK_2(FloderItemInfo::callBackAddMailAnnex, this));
			}break;
		case ktype_mail_getBackAnnexItem:
			{
				//Button_getAnnes->addTouchEventListener(CC_CALLBACK_2(FloderItemInfo::callBackAddMailAnnex));
			}break;
		}


		auto Label_getAnnes = Label::createWithTTF(str_,APP_FONT_NAME,18);
		Label_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_getAnnes->setPosition(Vec2(0,0));
		Button_getAnnes->addChild(Label_getAnnes);
		m_pLayer->addChild(Button_getAnnes);

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setAnchorPoint(Vec2(0,0));
		return true;
	}
	return false;
}

void FloderItemInfo::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FloderItemInfo::onExit()
{
	UIScene::onExit();
}

bool FloderItemInfo::onTouchBegan( Touch * pTouch,Event * pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}

void FloderItemInfo::callbackAuctionItem(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto auctionui = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
		if (auctionui != NULL)
		{
			auctionui->getbackAuction(NULL);
		}
		this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FloderItemInfo::callBackAddMailAnnex(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		MailUI * mail_ui = (MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
		if (mail_ui != NULL)
		{
			if (this->getTag() == REDUCTIONANNEXBUTTON1)
			{
				auto mailAnnexui = (FloderItemInstance *)mail_ui->sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON1);
				if (mailAnnexui != NULL)
				{
					mail_ui->reductionAnnex1(pSender);
				}
			}

			if (this->getTag() == REDUCTIONANNEXBUTTON2)
			{
				auto mailAnnexui = (FloderItemInstance *)mail_ui->sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON2);
				if (mailAnnexui != NULL)
				{
					mail_ui->reductionAnnex2(pSender);
				}
			}
		}

		this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FloderItemInfo::callBackShowSendMailAnnex( Ref * obj )
{
	CCLOG("show mail goods annex info");
}
