
#ifndef __ButtonTREE_H__
#define __ButtonTREE_H__
#include "ui/CocosGUI.h"

USING_NS_CC;
//USING_NS_CC_EXT;
using namespace cocos2d::ui;

NS_CC_BEGIN

class ButtonTree : public Button
{
public:
    ButtonTree();
    ~ButtonTree();
    
	static ButtonTree * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	void setInfo(int tag);
	int getInfo();

private:
	int m_parentTag;
};
NS_CC_END

#endif;
