
#include "UIScene.h"
#include "../../utils/GameUtils.h"
#include "../../GameAudio.h"

UIScene::UIScene(): 
m_pLayer(NULL)
{
}

UIScene::~UIScene()
{
	//cocos2d::extension::CCJsonReader::purgeJsonReader();
	//cocos2d::extension::UIActionManager::purgeUIActionManager();
	//cocos2d::extension::Helper::purgeHelper();
}

bool UIScene::init()
{
	if (Layer::init())
	{
		m_pLayer = Layer::create();
		//m_pLayer->scheduleUpdate();
		addChild(m_pLayer, 0, TAG_UISCENE_Layer);

		return true;
	}
	return false;
}

void UIScene::openAnim()
{
	auto  action = Sequence::create(
		ScaleTo::create(0.15f*1/2,1.1f),
		ScaleTo::create(0.15f*1/3,1.0f),
		NULL);
	runAction(action);
}

void UIScene::closeAnim()
{
	GameUtils::playGameSound(SCENE_CLOSE, 2, false);
	auto  action = Sequence::create(
		ScaleTo::create(0.15f*1/3,1.1f),
		ScaleTo::create(0.15f*1/2,1.0f),
		RemoveSelf::create(),
		NULL);
	action->setTag(111);
	runAction(action);
}

bool UIScene::isClosing()
{
	if(this->getActionByTag(111))
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool UIScene::resignFirstResponder(Touch *touch,Node* pNode,bool isTouchContinue, bool animated)
{
	Vec2 location = touch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	// remove it
	auto pUIScene = dynamic_cast<UIScene*>(pNode);
	if(pUIScene == NULL)
		pNode->removeFromParentAndCleanup(true);
	else
	{
		if(animated)
			pUIScene->closeAnim();
		else
			pUIScene->removeFromParent();
	}
	
	if (isTouchContinue)
	{
		return false;
	}
	else
	{
		return true;
	}

}

bool UIScene::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	if (!isVisible())
	{
		return false;
	}

	for (Node *c = this->getParent(); c != NULL; c = c->getParent())
	{
		if (c->isVisible() == false)
		{
			return false;
		}
	}
	
	return true;
}

void UIScene::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}