
#ifndef _UI_EXTENSIONS_UISCENE_H_
#define _UI_EXTENSIONS_UISCENE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

#define TAG_UISCENE_Layer 0

// this size is the same as the design resolution in the UIEditor
#define UI_DESIGN_RESOLUTION_WIDTH 800
#define UI_DESIGN_RESOLUTION_HEIGHT 480

class UIScene : public Layer
{
public:
	UIScene();
	virtual ~UIScene();
	bool init();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

	void openAnim();
	void closeAnim();
	/**
	   * @param animated �رոý���ʱ���Ƿ��ж���Ч�
	   */
	bool resignFirstResponder(Touch *touch,Node* pNode,bool isTouchContinue, bool animated = true);

	bool isClosing();

protected:
	Layer *m_pLayer;   // default Layer ( ZOrder = 0 )
};

#endif