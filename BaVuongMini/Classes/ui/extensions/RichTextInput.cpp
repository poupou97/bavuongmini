// #define COCOS2D_DEBUG   1

#include "RichTextInput.h"

//#include "CCRichLabel.h"
#include "../extensions/CCRichLabel.h"
#include "GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../utils/StrUtils.h"

//////////////////////////////////////////////////////////////////////////
// local function
//////////////////////////////////////////////////////////////////////////

#define RICHTEXT_INPUT_DEBUG 0

#define RICHTEXT_INPUT_INIT_WIDTH 200
#define RICHTEXT_INPUT_INIT_HEIGHT 50

#define FONT_NAME                       "Thonburi"
#define FONT_SIZE                       36

using namespace cocos2d::ui;

static Rect getRect(Node * pNode)
{
    Rect rc;
    rc.origin = pNode->getPosition();
    rc.size = pNode->getContentSize();
    //rc.origin.x -= rc.size.width / 2;
    //rc.origin.y -= rc.size.height / 2;
    return rc;
}

enum {
	kTagRichLabelScrollView = 100,
};

//////////////////////////////////////////////////////////////////////////
// implement KeyboardNotificationLayer
//////////////////////////////////////////////////////////////////////////

Node* KeyboardNotificationLayer::s_currentClickingNode = NULL;

KeyboardNotificationLayer::KeyboardNotificationLayer()
: m_pTrackNode(0)
, m_adjustVert(0)
{
    ////setTouchEnabled(true);
	auto touchListener = EventListenerTouchOneByOne::create();
	//touchListener->setSwallowTouches(true);
	touchListener->onTouchBegan = CC_CALLBACK_2(KeyboardNotificationLayer::onTouchBegan, this);
	touchListener->onTouchEnded = CC_CALLBACK_2(KeyboardNotificationLayer::onTouchEnded, this);
	_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
}

/*void KeyboardNotificationLayer::registerWithTouchDispatcher()
{
    auto pDirector = Director::getInstance();
    pDirector->getTouchDispatcher()->addTargetedDelegate(this, 0, false);
}*/

#define KEYBOARD_SHOW_HIDE_TIME 0.23f
void KeyboardNotificationLayer::updateInputBoxPosition(bool bAnimated)
{
    if (! m_pTrackNode)
    {
        return;
    }
    
    //
    // modify the whole screen when the keyboard will show
    //
    if(s_currentClickingNode != m_pTrackNode)
        return;
    
    //Rect rectTracked = getRect(m_pTrackNode);
    
    // get the rect according to world coordinate
    Size contentSize = m_pTrackNode->getContentSize();
    Rect rect = Rect(0, 0, contentSize.width, contentSize.height);
	Rect rectTracked = getRect(m_pTrackNode); //RectApplyAffineTransform(rect, m_pTrackNode->getNodeToWorldAffineTransform());
    
    // if the input box has multi-lines content
    rectTracked.origin.y += m_inputCursorOffsetY;
    
    //CCLOG("TextInputTest:trackingNodeAt(origin:%f,%f, size:%f,%f)",
    //      rectTracked.origin.x, rectTracked.origin.y, rectTracked.size.width, rectTracked.size.height);
    
    // if the keyboard area doesn't intersect with the tracking node area, nothing need to do.
    if (! rectTracked.intersectsRect(m_keyboardEndRect) && m_adjustVert == 0)
    {
        return;
    }
    
    // assume keyboard at the bottom of screen, calculate the vertical adjustment.
    float adjustVert = m_keyboardEndRect.getMaxY() - rectTracked.getMinY();
    m_adjustVert += adjustVert;
    //CCLOG("TextInputTest:needAdjustVerticalPosition(%f)", m_adjustVert);
    
    auto pDirector = Director::getInstance();
    if(bAnimated)
    {
        auto action = MoveBy::create(KEYBOARD_SHOW_HIDE_TIME, Vec2(0, adjustVert));
        pDirector->getRunningScene()->runAction(action);
    }
    else
    {
        pDirector->getRunningScene()->setPositionY(m_adjustVert);
    }
    
    /*
     // move all the children node of KeyboardNotificationLayer
     __Array * children = getChildren();
     Node * node = 0;
     int count = children->count();
     Vec2 pos;
     for (int i = 0; i < count; ++i)
     {
     node = (Node*)children->objectAtIndex(i);
     pos = node->getPosition();
     pos.y += adjustVert;
     node->setPosition(pos);
     }
     */
}

void KeyboardNotificationLayer::keyboardWillShow(IMEKeyboardNotificationInfo& info)
{
    CCLOG("TextInputTest:keyboardWillShowAt(origin:%f,%f, size:%f,%f)",
        info.end.origin.x, info.end.origin.y, info.end.size.width, info.end.size.height);
    
    m_keyboardEndRect = info.end;
    
    updateInputBoxPosition(true);
	/*CCLOG("TextInputTest:keyboardWillShowAt(origin:%f,%f, size:%f,%f)",
		info.end.origin.x, info.end.origin.y, info.end.size.width, info.end.size.height);

	if (!m_pTrackNode)
	{
		return;
	}

	auto rectTracked = getRect(m_pTrackNode);
	CCLOG("TextInputTest:trackingNodeAt(origin:%f,%f, size:%f,%f)",
		rectTracked.origin.x, rectTracked.origin.y, rectTracked.size.width, rectTracked.size.height);

	// if the keyboard area doesn't intersect with the tracking node area, nothing need to do.
	if (!rectTracked.intersectsRect(info.end))
	{
		return;
	}

	// assume keyboard at the bottom of screen, calculate the vertical adjustment.
	float adjustVert = info.end.getMaxY() - rectTracked.getMinY();
	CCLOG("TextInputTest:needAdjustVerticalPosition(%f)", adjustVert);

	// move all the children node of KeyboardNotificationLayer
	auto& children = getChildren();
	Node * node = 0;
	ssize_t count = children.size();
	Vec2 pos;
	for (int i = 0; i < count; ++i)
	{
		node = children.at(i);
		pos = node->getPosition();
		pos.y += adjustVert;
		node->setPosition(pos);
	}*/
}

void KeyboardNotificationLayer::keyboardWillHide(IMEKeyboardNotificationInfo& info)
{
    if(m_adjustVert != 0)
    {
        auto pDirector = Director::getInstance();
        //pDirector->getRunningScene()->setPositionY(0);
        auto action = MoveBy::create(KEYBOARD_SHOW_HIDE_TIME, Vec2(0, -m_adjustVert));
        pDirector->getRunningScene()->runAction(action);
    }
    
    /*
    if(m_adjustVert != 0)
    {
        // move all the children node of KeyboardNotificationLayer
        __Array * children = getChildren();
        Node * node = 0;
        int count = children->count();
        Vec2 pos;
        for (int i = 0; i < count; ++i)
        {
            node = (Node*)children->objectAtIndex(i);
            pos = node->getPosition();
            pos.y -= m_adjustVert;
            node->setPosition(pos);
        }
    }
     */
    
    m_adjustVert = 0;
    
    m_keyboardEndRect = Rect(0, 0, 0, 0);
}

// Layer function

bool KeyboardNotificationLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	if (!isVisible())
	{
		return false;
	}

	for (Node *c = this->getParent(); c != NULL; c = c->getParent())
	{
		if (c->isVisible() == false)
		{
			return false;
		}
	}

    CCLOG("++++++++++++++++++++++++++++++++++++++++++++");
    m_beginPos = pTouch->getLocation();    
    return true;
}

void KeyboardNotificationLayer::onTouchEnded(Touch *pTouch, Event *pEvent)
{
    if (! m_pTrackNode)
    {
        return;
    }
    
    Vec2 endPos = pTouch->getLocation();    

    float delta = 5.0f;
    if (::abs(endPos.x - m_beginPos.x) > delta
        || ::abs(endPos.y - m_beginPos.y) > delta)
    {
        // not click
        m_beginPos.x = m_beginPos.y = -1;
        return;
    }

    // decide the trackNode is clicked.
	//--------old
   /* Rect rect;
    Vec2 point = convertTouchToNodeSpaceAR(pTouch);
    //CCLOG("KeyboardNotificationLayer:clickedAt(%f,%f)", point.x, point.y);

    rect = getRect(m_pTrackNode);
    //CCLOG("KeyboardNotificationLayer:TrackNode at(origin:%f,%f, size:%f,%f)",
    //    rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
    bool bClicked = rect.containsPoint(point);
    if(!bClicked)
    {
        bClicked = isClickTrackNode(rect, point);
    }*/

	Rect rect;
	rect.size = m_pTrackNode->getContentSize();
	auto bClicked = isScreenPointInRect(endPos, Camera::getVisitingCamera(), m_pTrackNode->getWorldToNodeTransform(), rect, nullptr);
	if (!bClicked)
	{
		bClicked = isClickTrackNode(rect, endPos);
	}
	//auto drawNode = DrawNode::create();
	//drawNode->drawRect(rect.origin, rect.size, Color4F(1, 0, 0, 1));
	//addChild(drawNode);

    this->onClickTrackNode(bClicked, endPos);
}

//////////////////////////////////////////////////////////////////////////
// implement RichTextInputBox
//////////////////////////////////////////////////////////////////////////

RichTextInputBox::RichTextInputBox()
: m_nCharLimit(100)
, m_nInputBoxWidth(RICHTEXT_INPUT_INIT_WIDTH)
, m_nInputBoxHeight(RICHTEXT_INPUT_INIT_HEIGHT)
, m_direction(kCCRichInputDirectionHorizontal)
, m_inputMode(kCCInputModeFullText)
, m_bMultiLines(false)
{
}

void RichTextInputBox::onClickTrackNode(bool bClicked, const cocos2d::Vec2& touchPos)
{
    auto pTextField = (TextFieldTTF*)m_pTrackNode;
    if (bClicked)
    {
        s_currentClickingNode = pTextField;   // record the current clicking input box
        
        // TextFieldTTFTest be clicked
        CCLOG("TextFieldTTFActionTest:TextFieldTTF attachWithIME");
		pTextField->attachWithIME();
		m_pCursorSprite->setVisible(true);
		// blink the cursor
		auto repeat = RepeatForever::create( Blink::create(2, 2) );
		m_pCursorSprite->runAction( repeat);
		pTextField->setCursorFromPoint(touchPos, Camera::getVisitingCamera());
    }
    else
    {
        // TextFieldTTFTest not be clicked
        CCLOG("TextFieldTTFActionTest:TextFieldTTF detachWithIME");
        pTextField->detachWithIME();
		m_pCursorSprite->setVisible(false);
		m_pCursorSprite->stopAllActions();
		
    }
}

bool RichTextInputBox::isClickTrackNode(const Rect& trackNodeRect, const Vec2& touchPoint)
{
    Rect rc = Rect(trackNodeRect.origin.x, trackNodeRect.origin.y - (this->getInputBoxHeight() - trackNodeRect.size.height), trackNodeRect.size.width, this->getInputBoxHeight());
    
    return rc.containsPoint(touchPoint);
}

void RichTextInputBox::onEnter()
{
    KeyboardNotificationLayer::onEnter();

    // add TextFieldTTF
    Size s = Director::getInstance()->getVisibleSize();

    m_pTextField = TextFieldTTF::textFieldWithPlaceHolder("<click here for input>",
        FONT_NAME,
        23);
    

    m_pTextField->setDelegate(this);
    
	m_pTextField->setPosition(Vec2(0,0));
	m_pTextField->setAnchorPoint(Vec2(0,0));

	//m_pTextField->setString("abcdefghijklmnopqrstuvwxyz");   // m_pTextField must have some chars, otherwise, onTextFieldDeleteBackward will not be called
	m_pTextField->setString(std::string());   // m_pTextField must have some chars, otherwise, onTextFieldDeleteBackward will not be called
	m_pTextField->setContentSize(Size(m_nInputBoxWidth, RICHTEXT_INPUT_INIT_HEIGHT));
    m_pTrackNode = m_pTextField;

	// input content
    Size richLabelSize = Size(2000, RICHTEXT_INPUT_INIT_HEIGHT);   // one line style
	if(m_bMultiLines)   // inputbox allows multilines
	{
		richLabelSize.setSize(m_nInputBoxWidth, m_nInputBoxHeight);
	}
	m_pRichLabel = CCRichLabel::createWithString(std::string().c_str(), richLabelSize, NULL, NULL);
	m_pRichLabel->setCharNumInOneLabel(1);
	m_pRichLabel->setContentSize(m_pTextField->getContentSize());
	m_pRichLabel->setPosition(Vec2(0,0));
	m_pRichLabel->setAnchorPoint(Vec2(0,0));
	//addChild(m_pRichLabel);

	// input cursor
	m_pCursorSprite = Sprite::create("images/input_cursor.png");
    m_pCursorSprite->setScaleY(0.9f);
	m_pCursorSprite->setAnchorPoint(Vec2::ZERO);
	m_pCursorSprite->setVisible(false);
	//m_pTextField->addChild(m_pCursorSprite);

	auto scrollView_RichLabel = cocos2d::extension::ScrollView::create(Size(m_nInputBoxWidth, m_pTextField->getContentSize().height));
	scrollView_RichLabel->setAnchorPoint(Vec2::ZERO);
	scrollView_RichLabel->setContentOffset(Vec2(0,0));
	scrollView_RichLabel->setTouchEnabled(true);
    if(m_direction == kCCRichInputDirectionHorizontal)
        scrollView_RichLabel->setDirection(cocos2d::extension::ScrollView::Direction::HORIZONTAL);
    else
        scrollView_RichLabel->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
	scrollView_RichLabel->setPosition(Vec2(0,0));
	scrollView_RichLabel->setDelegate(this);
	scrollView_RichLabel->setBounceable(true);
	scrollView_RichLabel->setClippingToBounds(true);
	// if do not setContainer(), must setContentSize() manully
	// if setContainer(), the scrolling behavior of the ScrollView is affected by this container
	//scrollView_RichLabel->setContainer(m_pRichLabel);
	//scrollView_RichLabel->setPosition(Vec2(0,0));
	scrollView_RichLabel->setLocalZOrder(-1);
	addChild(scrollView_RichLabel);
	//scrollView_RichLabel->setVisible(false);
	scrollView_RichLabel->setTag(kTagRichLabelScrollView);
	scrollView_RichLabel->addChild(m_pRichLabel);   // add to ScrollView
	scrollView_RichLabel->addChild(m_pCursorSprite);   // add to ScrollView

	m_pTextField->setVisible(false);
	m_pTextField->setLocalZOrder(10);
	addChild(m_pTextField);

#if RICHTEXT_INPUT_DEBUG
	// insert button
	MenuItemFont::setFontSize(30);
	MenuItemFont::setFontName("Marker Felt");
	MenuItemFont *link = MenuItemFont::create("insert emote", this, menu_selector(RichTextInputBox::menuInsertEmotionCallback));	
	link->setZoomScale(1.05f);
	link->setPositionX(link->getContentSize().width/2);
	link->setColor(Color3B(255, 0, 0));
	link->setAnchorPoint(Vec2(0.5f,0));
	Menu* menu = Menu::create(link, NULL);
	menu->setPosition(Vec2(600, 100));
	m_pTextField->addChild(menu);
	
	link = MenuItemFont::create("insert item", this, menu_selector(RichTextInputBox::menuInsertItemCallback));
	link->setZoomScale(1.05f);
	link->setPositionX(link->getContentSize().width/2);
	link->setColor(Color3B(255, 0, 0));
	link->setAnchorPoint(Vec2(0.5f,0));
	menu = Menu::create(link, NULL);
	menu->setPosition(Vec2(600, 140));
	m_pTextField->addChild(menu);
#endif
}

void RichTextInputBox::onExit()
{
    KeyboardNotificationLayer::onExit();
}

void RichTextInputBox::setInputMode(int mode)
{
	m_inputMode = mode;
}

// CCTextFieldDelegate protocol
bool RichTextInputBox::onTextFieldAttachWithIME(TextFieldTTF * pSender)
{
    return false;
}

bool RichTextInputBox::onTextFieldDetachWithIME(TextFieldTTF * pSender)
{
    return false;
}

void RichTextInputBox::sychronizeTextFieldTTF()
{
	// sychronize the TextFieldTTF's content
	const char* wholeString = m_pRichLabel->getString().c_str();
	if(wholeString != NULL)
		m_pTextField->setString(wholeString);
	else
		m_pTextField->setString("");
	// this TextFieldTTF is a delegate, so always use the init content size 
	m_pTextField->setContentSize(Size(m_nInputBoxWidth, RICHTEXT_INPUT_INIT_HEIGHT));

}

bool RichTextInputBox::onTextFieldInsertText(cocos2d::TextFieldTTF*  sender, const char * text, size_t nLen)
{
    // if insert enter, treat as default to detach with ime
    if ('\n' == *text)
    {
        return false;
    }

	std::string sInsert(text, nLen);

	//const char* _str = m_pRichLabel->getString();
	//int charCount = _str != NULL ? StrUtils::calcCharCount(_str) : 0;
	//int _count = StrUtils::calcCharCount(text);
	int charCount = m_pRichLabel->getCharCount();
	auto tmpRichLabel = CCRichLabel::createWithString(sInsert.c_str(), Size(2000, RICHTEXT_INPUT_INIT_HEIGHT), NULL, NULL);
	int _count = tmpRichLabel->getCharCount();
	if(charCount + _count > m_nCharLimit)
	{
		//add limit input 
		const char * secondStr = StringDataManager::getString("limitInputmax");
		GameView::getInstance()->showAlertDialog(secondStr);
		return true;
	}

	//std::string currentString(this->m_pTextField->getString());
	//std::string currentString(m_pRichLabel->getString());
	//currentString.append(sInsert);
    //m_pRichLabel->setString(currentString.c_str());

	m_pRichLabel->appendString(sInsert.c_str());
	sychronizeTextFieldTTF();

	adjustInputCursorPos();

    // if the textfield's char count more than m_nCharLimit, doesn't insert text anymore.
    if (sender != NULL && sender->getCharCount() >= m_nCharLimit)
    {
        return true;
    }

    return true;
}

bool RichTextInputBox::onTextFieldDeleteBackward(cocos2d::TextFieldTTF*  sender, const char * delText, size_t nLen){

	int size = m_pRichLabel->getElementSize();
	if(size > 0)
	{
		m_pRichLabel->deleteElementAtIdx(size-1);

		sychronizeTextFieldTTF();

		adjustInputCursorPos();
	}
    return true;
}

bool RichTextInputBox::onDraw(TextFieldTTF * sender)
{
#if RICHTEXT_INPUT_DEBUG
	ccDrawLine(m_pTextField->getPosition(), 
		Vec2(m_pTextField->getPositionX()+m_pTextField->getContentSize().width, m_pTextField->getPositionY()));
	//ccDrawLine(Vec2(0, 30), Vec2(800, 30));
#endif

    //return true;   // don't draw m_pTextField
	return false;
}

void RichTextInputBox::setDirection(int dir)
{
    m_direction = dir;
}
void RichTextInputBox::setMultiLinesMode(bool bMultiLins)
{
	m_bMultiLines = bMultiLins;
}

void RichTextInputBox::menuInsertEmotionCallback(Ref* pSender)
{
	//m_pRichLabel->appendString("/e02");
	onTextFieldInsertText(NULL, "/e02", 4);
}
void RichTextInputBox::menuInsertItemCallback(Ref* pSender)
{
	//m_pRichLabel->appendString("/t[Sword]/");
	onTextFieldInsertText(NULL, "/t[Sword]/", 10);
}

const char* RichTextInputBox::getInputString()
{
	return m_pRichLabel->getString().c_str();
}

void RichTextInputBox::deleteAllInputString()
{
	int size = m_pRichLabel->getElementSize();
	if(size>0)
	{
		for(int i = size;i>0;i--)
		{
			m_pRichLabel->deleteElementAtIdx(i-1);
		}
	}
	//set default point Vec2(0,0)
	m_pCursorSprite->setPosition(Vec2(0,0));

}

//auto RichTextInputBox::getTextTTF()
//{
//	return m_pRichLabel;
//}

void RichTextInputBox::setCharLimit(int limit)
{
	m_nCharLimit = limit;
}

void RichTextInputBox::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void RichTextInputBox::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void RichTextInputBox::adjustInputCursorPos()
{
	auto view = (cocos2d::extension::ScrollView*)this->getChildByTag(kTagRichLabelScrollView);

	//Sprite* cursor = (Sprite*)view->getChildByTag(kTagInputCursorSprite);
	auto cursor = m_pCursorSprite;
	int cursorWidth = cursor->getContentSize().width;   // the width of the cursor

	// 1. adjust the scroll view's contentsize
	int firstLineHeight = 0;
	if(m_pRichLabel->getLinesHeight().size() > 0)
	{
		firstLineHeight = m_pRichLabel->getLinesHeight().at(0);
		
	}
	int maxViewHeight = getInputBoxHeight() - (RICHTEXT_INPUT_INIT_HEIGHT - firstLineHeight);
	float _height = m_pRichLabel->getContentSize().height;
	if(_height > maxViewHeight)
 		_height = maxViewHeight;
	view->setContentSize(Size(m_pRichLabel->getContentSize().width + cursorWidth, _height));
    Size scrollViewSize = view->getContentSize();
    view->setContentSize(Size(scrollViewSize.width, _height));
    
	// 2. according to the cursor position, adjust scrollview's offset
	Vec2 endPos = m_pRichLabel->getEndPos();
	Size inputboxSize = m_pTextField->getContentSize();
	if(endPos.x > inputboxSize.width)
	{
		//m_pRichLabel->setPositionX(m_pTextField->getPositionX() - (endPos.x - inputboxSize.width));
		view->setContentOffset(Vec2((m_pTextField->getPositionX()) -(endPos.x + cursorWidth - inputboxSize.width), 0));
	}
	else
	{
		//m_pRichLabel->setPositionX(m_pTextField->getPositionX());
		view->setContentOffset(Vec2(0,0));
	}

	// 3. adjust the input cursor's position
	cursor->setPositionX(m_pRichLabel->getPositionX() + endPos.x);
 
    // for multi-lines inputbox
	if(m_pRichLabel->getLinesHeight().size() > 0)
	{
		m_inputCursorOffsetY = firstLineHeight-m_pRichLabel->getContentSize().height;
	}
	else
	{
		m_inputCursorOffsetY = 0;
	}
	float minCursorOffsetY = RICHTEXT_INPUT_INIT_HEIGHT - this->getInputBoxHeight();
	if(m_inputCursorOffsetY < minCursorOffsetY)
		view->setPositionY(minCursorOffsetY);
	else
		view->setPositionY(m_inputCursorOffsetY);
    
    updateInputBoxPosition(false);   // now, must use false
}
