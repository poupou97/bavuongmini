#ifndef _CUSTOM_CCRICHLABEL_RICHELEMENT_H_
#define _CUSTOM_CCRICHLABEL_RICHELEMENT_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
//using namespace cocos2d::ui;

#define RICHELEMENT_ITEM_KEYWORD "propid"
#define RICHELEMENT_ITEM_SEPARATOR ";"
#define RICHELEMENT_COLOR_KEYWORD "color"

class CCRichLabel;

	
	struct TextFontDefinition {
		bool hasColorFlag;
		Color3B fontColor;
		// shadow
		// stroke
		// font size, etc.
	};
	class BVRichElement
	{
	public:
		enum ElementType
		{
			RichElementType_Sprite = 0,
			RichElementType_Button = 1,
			RichElementType_Text = 2,
			RichElementType_Return = 3,
		};

	public:
		BVRichElement();
		virtual ~BVRichElement();

		virtual Size getContentSize() = 0;
		virtual void setPosition(Vec2 pos) = 0;
		virtual Vec2 getPosition() = 0;

		virtual Node* getNode() = 0;

		virtual void parse(const char* elementStr) = 0;

		virtual int getCharCount() = 0;

	public:
		int x, y;
		int w, h;
		int color;
		std::string m_string;
		int line;   // line position, line 0, 1, or 2, etc.
		int type;

		CCRichLabel* _container;
	};

	class RichElementSprite : public BVRichElement
	{
	public:
		RichElementSprite();
		virtual ~RichElementSprite();

		virtual Size getContentSize();
		virtual void setPosition(Vec2 pos);
		virtual Vec2 getPosition();
		virtual void parse(const char* elementStr);

		virtual Node* getNode();

		void animate();

		virtual int getCharCount();

	private:
		Sprite* m_sprite;
		Animation *m_animation;
	};


////////////////////////////////////////////////////////



	struct structPropIds {
		std::string propId;   // item id
		long propInstanceId;   // item instance id
	};
	class RichElementButton : public  BVRichElement
	{
		// some utils functions
	public:
		static std::string makeButton(const char* buttonName, const char* propId, long propInstanceId, int color = 0xedf03d);
		static structPropIds parseLink(std::string& linkContent);
	private:
		static std::string makeLink(const char* propId, long propInstanceId);

		static std::string makeColor(int color);
		static Color3B parseColor(std::string& colorContent);

	public:
		RichElementButton();
		virtual ~RichElementButton();

		virtual Size getContentSize();
		virtual void setPosition(Vec2 pos);
		virtual Vec2 getPosition();
		virtual void parse(const char* elementStr);

		virtual Node* getNode();

		virtual int getCharCount();

	public:
		std::string buttonName;
		std::string linkContent;
		std::string colorContent;

	private:
		Menu* m_pMenu;
	};

	class CCRichElementText : public  BVRichElement
	{
	public:
		CCRichElementText();
		CCRichElementText(Color3B& color);
		virtual ~CCRichElementText();

		virtual Size getContentSize();
		virtual void setPosition(Vec2 pos);
		virtual Vec2 getPosition();
		virtual void parse(const char* elementStr);

		virtual Node* getNode();

		virtual int getCharCount();

	private:
		Label* m_label;

		TextFontDefinition m_fontDefinition;
	};


////////////////////////////////////////////////////


	class RichElementReturn : public BVRichElement
	{
	public:
		RichElementReturn();
		virtual ~RichElementReturn();

		virtual Size getContentSize();
		virtual void setPosition(Vec2 pos);
		virtual Vec2 getPosition();
		virtual void parse(const char* elementStr);

		virtual Node* getNode();

		virtual int getCharCount();

	private:
		Node* m_node;
	};



#endif
