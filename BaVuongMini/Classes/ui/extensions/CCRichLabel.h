#ifndef _CUSTOM_CCRICHLABEL_H_
#define _CUSTOM_CCRICHLABEL_H_

#include "cocos2d.h"
#include "cocos-ext.h"

#define MAX_RICHLABEL_EMOTE 42

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

static const char* formatFlagChar			= { "/" };
static const int formatFlagCharLen			= 1;

static const char* chatExpression			= { "/e" };
static const int chatExpressionLen			= 2;
static const int chatExpressionContentLen	= 2;

static const char* chatItem				= { "/t" };
static const int chatItemLen			= 2;
static const int chatItemContentLen		= 4;

static const char* textDefinitionMarkup		= { "/#" };
static const int textDefinitionMarkupLen		= 2;

static const char* returnMarkup		= { "/n" };
static const int returnMarkupLen		= 2;

class BVRichElement;

class RichLabelDefinition {
public:
	RichLabelDefinition();

public:
	int firstLineIndent;
	int alignment;
	int charLimit;
	float fontSize;
	Color3B fontColor;   // this color will be applied to the text which has not color flag ( /#ff0000 xxx/ )
	bool strokeEnabled;
	bool shadowEnabled;
};

////////////////////////////////////////////////////////////

/**
 * parse a rich text to a Node
 * the format of the rich text is below:
 *     "hello world, /tSword,1012/, /e01 hi /e02, etc."
 *
 * button: /tButtonName,propid(propid,propinstanceid)/,
 *     for example: "/tSword,propid(jian01,1012)/", ButtonName = Sword, LinkContent = propid(jian01,1012)
 *
 * sprite: /eSpriteName
 *     for example: "/e01", SpriteName = 01
 * ( in the future, this can be extended by using url format )
 * 
 * text with definition
 *     for example: "/#ff0000 hello world/", this will show the text by using red color ( ff0000 )
 * ( in the future, this can be extended by using more font definition ( shadow, stroke etc.) )
 * 
 * ������ʵ�֣���Ҫ����Դ�: https://github.com/happykevins/cocos2dx-ext ����еHTML Widget
 * @author zhaogang
 * @version 0.1.0
 */
class CCRichLabel : public Node, public LabelProtocol
{
// these classes are friends, they can access the private members
friend class RichElementSprite;
friend class RichElementButton;
friend class CCRichElementText;
friend class RichElementReturn;

public:
	enum alignment {
		alignment_vertical_top = 0,
		alignment_vertical_center = 1,
		alignment_vertical_bottom = 2,
	};

public:
	CCRichLabel();
	virtual ~CCRichLabel();

	static CCRichLabel* create();
	/**
	   * utf8_str, the content
	   * preferred_size, the content size
	   * target, the listener
	   * selector, the event handler
	   * firstLineIndent, the first line indent
	   * fontSize, the rich label's font size
	   * charLimit, 
	   * alignment, the alignment, now mainly for the RichElementSprite
	   */
	static CCRichLabel* createWithString(const char* utf8_str, const Size& preferred_size, Ref* target, SEL_MenuHandler selector, int firstLineIndent = 0, float fontSize = 20,int charLimit = 5, int alignment = alignment_vertical_bottom);
	static CCRichLabel* create(const char* utf8_str, const Size& preferred_size, RichLabelDefinition def, Ref* target = NULL, SEL_MenuHandler selector = NULL);
	bool initWithString(const char* utf8_str, const Size& preferred_size, Ref* target, SEL_MenuHandler selector);

	virtual void onEnter();

	// from CCLabelProtocol
	virtual void setString(const std::string &label);
	virtual const std::string& getString() const;
	/* if just append string, use this optimized function */
	void appendString(const char *string);

	virtual void parseString(const char* str, std::vector<BVRichElement*>* outputList);
	virtual int generateRichElementText(const char* str, int nLen, std::vector<BVRichElement*>* elementList);
	virtual int generateRichElementTextWithDefinition(const char* str, int nLen, std::vector<BVRichElement*>* elementList);
	virtual int generateRichElementSprite(const char* str, int nLen, std::vector<BVRichElement*>* elementList);
	virtual int generateRichElementButton(const char* str, int nLen, std::vector<BVRichElement*>* elementList);
	virtual int generateRichElementReturn(const char* str, int nLen, std::vector<BVRichElement*>* elementList);

	// from Layer
	//virtual void draw();

	/* when delete one element succeefully, m_string will be modified at the same time */
	void deleteElementAtIdx(int index);
	int getElementSize();

	/* when display RichElementText, the char number in one CCLabel. the min value is 1 */
	void setCharNumInOneLabel(int num) { m_nCharNumInOneLabel = num; };

	inline void setFirstLineIndent(int value) { m_nFirstLineIndent = value; };

	/* used for the input cursor's postion */
	Vec2 getEndPos();

	/** get the total char count of all elements */
	int getCharCount();
    inline const std::vector<int>& getLinesHeight() { return m_linesHeight; };

	inline float getFontSize()  { return m_fFontSize; };

	RichLabelDefinition& getLabelDefinition();

protected:
    Ref*       m_pListener;
    SEL_MenuHandler    m_pfnSelector;

    /** Dimensions of the label in Points */
    Size m_tDimensions;

	 float m_fFontSize;

	 int m_alignment;

	 RichLabelDefinition m_labelDef;

private:
	// ��Ű��㷨
	// ���Ԫ���Ѿ������Parse���������ʼ�����Ű
	//     ����ı��У����Image Elementˣ���Ҫ����ÿ�еy���
    bool updateAll(std::vector<BVRichElement*>* elementList);

private:
    /** label's string */
    std::string m_string;

	std::vector<BVRichElement*>* m_elementList;

	int m_nCharNumInOneLabel;

	// default value is 0, it means there is no indent for the first line
	int m_nFirstLineIndent;
    
    // the height of each lines in the rich label
    std::vector<int> m_linesHeight;
};

#endif
