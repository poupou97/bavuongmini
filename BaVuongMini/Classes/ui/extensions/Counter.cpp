#include "Counter.h"
#include "RichTextInput.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "cocos\editor-support\cocostudio\CCSGUIReader.h"
#include "AppMacros.h"


Counter::Counter(void):
senderListener(NULL),
senderSelector(NULL)
{
	sendNum_=0;
	goodSendNum=0;
}

Counter::~Counter(void)
{
}

Counter * Counter::create()
{
	auto box =new Counter();
	if (box && box->init(""))
	{
		box->autorelease();
		return box;
	}
	
	CC_SAFE_DELETE(box);
	return NULL;
	
}

bool Counter::init(std::string test)
{
	if (UIScene::init())
	{
		Size s=Director::getInstance()->getVisibleSize();
		auto ppanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/jisuanqi_1.json");
		ppanel->setAnchorPoint(Vec2(0,0));
		ppanel->setPosition(Vec2(0,0));
		m_pLayer->addChild(ppanel);

		int buttonNumId = 1;
		for (int i=0;i<3;i++)
		{
			for (int j=0;j<3;j++)
			{
				char num[5];
				sprintf(num,"%d",buttonNumId);

				auto extractAnnex=Button::create();
				extractAnnex->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
				extractAnnex->setTouchEnabled(true);
				extractAnnex->setPressedActionEnabled(true);
				extractAnnex->setAnchorPoint(Vec2(0.5f,0.5f));
				extractAnnex->setTag(buttonNumId);
				extractAnnex->setPosition(Vec2(43+j*61,153- i*50));
				extractAnnex->addTouchEventListener(CC_CALLBACK_2(Counter::getAmount, this));

				auto label_Num = Label::createWithTTF(num,APP_FONT_NAME,25);
				label_Num->setAnchorPoint(Vec2(0.5f,0.5f));
				label_Num->setPosition(Vec2(0,0));
				extractAnnex->addChild(label_Num);
				m_pLayer->addChild(extractAnnex);
				
				buttonNumId++;
			}
		}
		auto button_clear=Button::create();
		button_clear->setTouchEnabled(true);
		button_clear->loadTextures("res_ui/enjian_y.png","res_ui/enjian_y.png","");
		button_clear->setAnchorPoint(Vec2(0.5f,0.5f));
		button_clear->setPosition(Vec2(230,213));
		button_clear->setTag(buttonNumId);
		button_clear->setPressedActionEnabled(true);
		button_clear->addTouchEventListener(CC_CALLBACK_2(Counter::deleteNum, this));
		ppanel->addChild(button_clear);

		auto deleteSp=ui::ImageView::create();
		deleteSp->loadTexture("res_ui/jisuanqi/jiantou.png");
		deleteSp->setAnchorPoint(Vec2(0.5f,0.5f));
		deleteSp->setPosition(Vec2(0,0));
		button_clear->addChild(deleteSp);

		auto Button_C=Button::create();
		Button_C->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		Button_C->setTouchEnabled(true);
		Button_C->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_C->setPosition(Vec2(230,153));
		Button_C->setPressedActionEnabled(true);
		Button_C->addTouchEventListener(CC_CALLBACK_2(Counter::clearNum, this));
		m_pLayer->addChild(Button_C);

		auto label_C= Label::createWithTTF("C",APP_FONT_NAME,25);
		label_C->setAnchorPoint(Vec2(0.5f,0.5f));
		label_C->setPosition(Vec2(0,0));
		Button_C->addChild(label_C);

		auto button_Zero=Button::create();
		button_Zero->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		button_Zero->setTouchEnabled(true);
		button_Zero->setTitleFontSize(25);
		button_Zero->setAnchorPoint(Vec2(0.5f,0.5f));
		button_Zero->setTag(0);
		button_Zero->setPosition(Vec2(230,103));
		button_Zero->setPressedActionEnabled(true);
		button_Zero->addTouchEventListener(CC_CALLBACK_2(Counter::getAmount, this));
		m_pLayer->addChild(button_Zero);

		auto label_Zero= Label::createWithTTF("0", APP_FONT_NAME,25);
		label_Zero->setAnchorPoint(Vec2(0.5f,0.5f));
		label_Zero->setPosition(Vec2(0,0));
		button_Zero->addChild(label_Zero);

		auto button_Ok=Button::create();
		button_Ok->loadTextures("res_ui/enjian_y.png","res_ui/enjian_y.png","");
		button_Ok->setTouchEnabled(true);
		button_Ok->setAnchorPoint(Vec2(0.5f,0.5f));
		button_Ok->setPosition(Vec2(230,53));
		button_Ok->setPressedActionEnabled(true);
		button_Ok->addTouchEventListener(CC_CALLBACK_2(Counter::sendButton, this));
		m_pLayer->addChild(button_Ok);

		auto label_Ok=Label::createWithTTF("OK", APP_FONT_NAME,25);
		label_Ok->setAnchorPoint(Vec2(0.5f,0.5f));
		label_Ok->setPosition(Vec2(0,0));
		button_Ok->addChild(label_Ok);

		showNum=Label::createWithTTF(goodsNum.c_str(),APP_FONT_NAME,25);//
		showNum->setAnchorPoint(Vec2(0,0));//20 195
		showNum->setPosition(Vec2(25,195));
		m_pLayer->addChild(showNum);

		auto button_close=(Button * )ui::Helper::seekWidgetByName(ppanel,"Button_close");
		button_close->setTouchEnabled(true);
		button_close->setPressedActionEnabled(true);
		button_close->addTouchEventListener(CC_CALLBACK_2(Counter::callBackExit, this));

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setPosition(Vec2::ZERO);
		this->setContentSize(ppanel->getContentSize());
		return true;
	}	
	return false;
}

void Counter::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void Counter::onExit()
{
	UIScene::onExit();
}


void Counter::getAmount(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button *)pSender;
		int id_ = btn->getTag();

		if (goodsNum.size() >= 9)
		{
			return;
		}

		if (std::atoi(goodsNum.c_str()) <= 0 && id_ == 0)
		{
			return;
		}
		char num[3];
		sprintf(num, "%d", id_);
		goodsNum.append(num);
		showNum->setString(goodsNum.c_str());
		goodSendNum = std::atoi(goodsNum.c_str());
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void Counter::clearNum(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		goodsNum.clear();
		showNum->setString(goodsNum.c_str());
		goodSendNum = std::atoi(goodsNum.c_str());
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void Counter::deleteNum(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (goodsNum.size()>0)
		{
			goodsNum.erase(goodsNum.end() - 1);
			showNum->setString(goodsNum.c_str());

			goodSendNum = std::atoi(goodsNum.c_str());
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}



}

void Counter::sendButton(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		sendNum_ = goodSendNum;
		if (senderListener && senderSelector)
		{
			(senderListener->*senderSelector)(this);

			/*auto callback = CCCallFuncO::create(senderListener, senderSelector, NULL);
			auto seq = Sequence::create(callback);
			this->runAction(seq);*/
		}

		/*
		if (sendNum_>0)
		{
		if (senderListener && senderSelector)
		{
		(senderListener->*senderSelector)(this);
		}
		}
		*/

		this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}
int Counter::getInputNum()
{
	return sendNum_;
}

void Counter::callBackSendNum( Ref* pSender, SEL_CallFuncO pSelector )
{
	senderListener = pSender;
	senderSelector = pSelector;
}
void Counter::callBackExit(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
		//removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

bool Counter::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return true;
}

void Counter::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void Counter::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void Counter::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void Counter::setShowNumDefault( int counts )
{
	goodSendNum = counts;
	char num[20];
	sprintf(num,"%d",counts);
	showNum->setString(num);
}
