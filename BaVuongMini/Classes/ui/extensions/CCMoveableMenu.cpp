#include "CCMoveableMenu.h"


CCMoveableMenu::CCMoveableMenu(void)
{
	moved_=false;
}


CCMoveableMenu::~CCMoveableMenu(void)
{
}

CCMoveableMenu * CCMoveableMenu::create()
{
	auto moveMenu=new CCMoveableMenu();
	moveMenu->init();
	moveMenu->autorelease();
	return moveMenu;
}

CCMoveableMenu* CCMoveableMenu::create( MenuItem* item, ... )
{
	va_list args;
	va_start(args,item);

	auto pRet= new CCMoveableMenu();
	cocos2d::Vector<cocos2d::MenuItem*> pArray;
	//__Array* pArray = NULL;
	if( item )
	{
		//pArray = __Array::create(item, NULL);
		pArray.pushBack(item);
		MenuItem * i = va_arg(args, MenuItem*);
		while(i)
		{
			//pArray->addObject(i);
			pArray.pushBack(i);
			i = va_arg(args, MenuItem*);
		}
	}

	if(pRet)
	{
		pRet->initWithArray(pArray);
		pRet->autorelease();
	}



	return pRet;
}


bool CCMoveableMenu::init()
{
	if (Menu::init())
	{
		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(CCMoveableMenu::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(CCMoveableMenu::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(CCMoveableMenu::onTouchMoved, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}


	return false;
}

/*void CCMoveableMenu::registerWithTouchDispatcher()
{
	Director::getInstance()->getTouchDispatcher()->addTargetedDelegate(this,0,false);
}*/
bool CCMoveableMenu::onTouchBegan( Touch *pTouch, Event *pEvent )
{
// 	if(visibleRect_.size.width&&visibleRect_.size.height)
// 	{
// 		if(!visibleRect_.containsPoint(pTouch->getLocation()))
// 			return false;
// 
// 	}
	moved_=false;
	mBeginPoint = pTouch->getLocation();
	return Menu::onTouchBegan(pTouch,pEvent);
}

void CCMoveableMenu::onTouchMoved(Touch *pTouch, Event *pEvent )
{
	// must move enough distance
	if(mBeginPoint.getDistance(pTouch->getLocation()) > 10.0f)
		moved_=true;
	Menu::onTouchMoved(pTouch,pEvent);
}

void CCMoveableMenu::onTouchEnded(Touch *pTouch, Event *pEvent )
{
	if(!moved_)
	{
		Menu::onTouchEnded(pTouch,pEvent);
	}
	else
	{
		_state = State::WAITING;
		if (_selectedItem)
		{
			_selectedItem->unselected();
		}

	}
}
