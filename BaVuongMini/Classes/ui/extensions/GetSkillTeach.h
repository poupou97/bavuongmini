
#ifndef H_GETSKILLTEACH_H
#define H_GETSKILLTEACH_H

#include "../extensions/UIScene.h"
#include "ui\CocosGUI.h"
#include "cocostudio\CCArmature.h"

using namespace cocos2d::ui;
/*
*��ѧ��ü��                                                                     
*/
class GetSkillTeach : public UIScene
{
public:
	GetSkillTeach(void);
	~GetSkillTeach(void);

	static GetSkillTeach * create(std::string skillId,std::string skillName,std::string skillIcon);
	bool init(std::string skillId,std::string skillName,std::string skillIcon);

	void onEnter();
	void onExit();

	void callBackSkillAction(Ref *pSender, Widget::TouchEventType type);
	void callBackStudySkill(Ref * obj);

	void ArmatureFinishCallback(cocostudio::Armature *armature, cocostudio::MovementEventType movementType, const char *movementID);
private:
	Layer * u_layer;
	Size winSize;
	std::string m_skillId;
	std::string m_skillIcon;
	std::string m_skillName;
	ImageView *mengban;
};

#endif