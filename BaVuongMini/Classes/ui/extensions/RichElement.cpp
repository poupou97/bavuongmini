#include "RichElement.h"
#include "CCMoveableMenu.h"

#include "CCRichLabel.h"
#include "AppMacros.h"
#include "../../utils/StrUtils.h"
#include "../../utils/GameUtils.h"


	enum {
		kTagButoon = 0,
	};

	BVRichElement::BVRichElement()
	{

	}

	BVRichElement::~BVRichElement()
	{

	}

	/////////////////////////////////////////////////////////

	RichElementSprite::RichElementSprite()
		: m_sprite(NULL)
	{
		type = RichElementType_Sprite;
	}

	RichElementSprite::~RichElementSprite()
	{
		CC_SAFE_RELEASE(m_sprite);
		CC_SAFE_RELEASE(m_animation);
	}

	Size RichElementSprite::getContentSize()
	{
		CCAssert(m_sprite != NULL, "sprite should not be NULL");

		return this->m_sprite->getContentSize();
	}

	void RichElementSprite::setPosition(Vec2 pos)
	{
		CCAssert(m_sprite != NULL, "sprite should not be NULL");

		m_sprite->setPosition(pos);
	}

	Vec2 RichElementSprite::getPosition()
	{
		return m_sprite->getPosition();
	}

	Node* RichElementSprite::getNode()
	{
		return m_sprite;
	}

	void RichElementSprite::parse(const char* elementStr)
	{
		m_string = elementStr;

		std::string emotionImageName = "res_ui/Expression";
		emotionImageName.append(elementStr);
		emotionImageName.append(".png");
		auto texture = Director::getInstance()->getTextureCache()->addImage(emotionImageName.c_str());
		Size emotionSize = texture->getContentSize();

		// emotion image is organized by sequence
		// each frame's width is same as the height
		int oneFrameSize = emotionSize.height;
		int nFrames = emotionSize.width / oneFrameSize;

		//
		// Animation using Sprite BatchNode
		//
		auto frame0 = SpriteFrame::createWithTexture(texture, Rect(0, 0, oneFrameSize, oneFrameSize));
		m_sprite = Sprite::createWithSpriteFrame(frame0);
		m_sprite->setAnchorPoint(Vec2::ZERO);
		m_sprite->retain();

		// manually add frames to the frame cache
		//__Array* animFrames = __Array::createWithCapacity(3);
		cocos2d::Vector<cocos2d::SpriteFrame*> animFrames;
		animFrames.pushBack(frame0);
		for (int i = 1; i < nFrames; i++)
		{
			auto frame = SpriteFrame::createWithTexture(texture, Rect(oneFrameSize*i, 0, oneFrameSize, oneFrameSize));
			animFrames.pushBack(frame);
		}
		m_animation = Animation::createWithSpriteFrames(animFrames, 0.2f);
		m_animation->retain();

		animate();

		_container->addChild(m_sprite, 0);
	}

	void RichElementSprite::animate()
	{
		m_sprite->stopAllActions();

		auto animate = Animate::create(m_animation);
		auto seq = Sequence::create(animate, NULL);
		m_sprite->runAction(RepeatForever::create(seq));
	}

	int RichElementSprite::getCharCount()
	{
		return 1;
	}

	/////////////////////////////////////////////////////////

	structPropIds RichElementButton::parseLink(std::string& linkContent)
	{
		structPropIds ids;

		int index = linkContent.find_first_of(RICHELEMENT_ITEM_KEYWORD);   // "propid"
		CCAssert(index != -1, "wrong format");

		int beginIndex = linkContent.find_first_of("(");
		CCAssert(beginIndex != -1, "wrong format: '(' is missing");

		int separatorIndex = linkContent.find(RICHELEMENT_ITEM_SEPARATOR, beginIndex);
		ids.propId = linkContent.substr(beginIndex + 1, separatorIndex - beginIndex - 1);
		CCLOG("propId: %s", ids.propId.c_str());

		int endIndex = linkContent.find_last_of(")");
		CCAssert(endIndex != -1, "wrong format: ')' is missing");

		std::string propinstanceid = linkContent.substr(separatorIndex + 1, endIndex - separatorIndex - 1);
		//CCLOG("propinstanceid str: %s", propinstanceid.c_str());
		ids.propInstanceId = atol(propinstanceid.c_str());
		CCLOG("propinstanceid: %ld", ids.propInstanceId);

		return ids;
	}

	std::string RichElementButton::makeLink(const char* propId, long propInstanceId)
	{
		// propid(potion01,1001)
		std::string str = RICHELEMENT_ITEM_KEYWORD;
		str.append("(");
		str.append(propId);
		str.append(RICHELEMENT_ITEM_SEPARATOR);
		char longStr[15];
		sprintf(longStr, "%ld", propInstanceId);
		str.append(longStr);
		str.append(")");

		return str;
	}

	std::string RichElementButton::makeColor(int color)
	{
		// color(ff00ff)
		std::string str = RICHELEMENT_COLOR_KEYWORD;
		str.append("(");

		char colorChars[7];
		Color3B newColor = GameUtils::convertToColor3B(color);
		sprintf(colorChars, "%02x%02x%02x", newColor.r, newColor.g, newColor.b);

		str.append(colorChars);
		str.append(")");

		return str;
	}

	Color3B RichElementButton::parseColor(std::string& colorContent)
	{
		int index = colorContent.find_first_of(RICHELEMENT_COLOR_KEYWORD);   // "color"
		CCAssert(index != -1, "wrong format");

		int beginIndex = colorContent.find_first_of("(");
		CCAssert(beginIndex != -1, "wrong format: '(' is missing");

		// color string, for example "ff0000"
		std::string colorStr = colorContent.substr(beginIndex + 1, 6);
		CCAssert(colorStr.size() == 6, "wront format, not a color string");
		std::string red = colorStr.substr(0, 2);
		int r = strtol(red.c_str(), NULL, 16);
		std::string green = colorStr.substr(2, 2);
		int g = strtol(green.c_str(), NULL, 16);
		std::string blue = colorStr.substr(4, 2);
		int b = strtol(blue.c_str(), NULL, 16);

		int endIndex = colorContent.find_last_of(")");
		CCAssert(endIndex != -1, "wrong format: ')' is missing");

		Color3B color = Color3B(r, g, b);
		return color;
	}

	std::string RichElementButton::makeButton(const char* buttonName, const char* propId, long propInstanceId, int color)
	{
		// /tbuttonname,propid(potion01,1001)/
		std::string str = "/t";
		str.append(buttonName);
		str.append(",");
		str.append(makeLink(propId, propInstanceId));
		str.append(",");
		str.append(makeColor(color));
		str.append("/");

		return str;
	}

	RichElementButton::RichElementButton()
	{
		type = RichElementType_Button;
	}

	RichElementButton::~RichElementButton()
	{
		CC_SAFE_RELEASE(m_pMenu);
	}

	Size RichElementButton::getContentSize()
	{
		CCAssert(m_pMenu != NULL, "menu should not be NULL");


		return m_pMenu->getChildByTag(kTagButoon)->getContentSize();
	}

	void RichElementButton::setPosition(Vec2 pos)
	{
		CCAssert(m_pMenu != NULL, "menu should not be NULL");

		m_pMenu->setPosition(pos);
	}

	Vec2 RichElementButton::getPosition()
	{

		return m_pMenu->getPosition();

	}

	Node* RichElementButton::getNode()
	{
		return m_pMenu;
	}

	void RichElementButton::parse(const char* elementStr)
	{
		m_string = elementStr;

		// full format: /tButtonName,LinkContent/
		// parse the string to get ButtonName and LinkContent
		int beginIndex = m_string.find_first_of(",");
		buttonName = "";
		linkContent = "";
		colorContent = "edf03d";   // (237, 240, 61)

								   // first, remove "/t" ( start ) and "/" ( end )
		std::string wholeStr = m_string.substr(2, m_string.size() - chatItemLen - formatFlagCharLen);
		CCLOG("wholeStr: %s", wholeStr.c_str());

		std::vector<std::string> parameters = StrUtils::split(wholeStr, ",");

		// generate button name
		if (parameters.size() >= 1)
		{
			buttonName = parameters.at(0);
			CCLOG("buttonName: %s", buttonName.c_str());
		}

		// generate link content
		if (parameters.size() >= 2)
		{
			linkContent = parameters.at(1);
			CCLOG("linkContent: %s", linkContent.c_str());
		}

		// generate button color
		Color3B buttonColor = Color3B(237, 240, 61);
		if (parameters.size() >= 3)
		{
			colorContent = parameters.at(2);
			CCLOG("colorContent: %s", colorContent.c_str());

			buttonColor = parseColor((colorContent));
		}

		if (buttonName == "")
		{
			m_pMenu = Menu::create(NULL);
			return;
		}

		MenuItemFont::setFontSize(20);
		MenuItemFont::setFontName("Marker Felt");
		auto link = MenuItemFont::create(buttonName.c_str(), _container->m_pListener, _container->m_pfnSelector);
		link->setScale(1.1f);
		link->setPositionX(link->getContentSize().width / 2);
		link->setColor(buttonColor);
		link->setAnchorPoint(Vec2(0.5f, 0));
		link->setTag(kTagButoon);
		link->setUserData(this);   // record RichElementButton instance, used for button callback
		m_pMenu = CCMoveableMenu::create(link, NULL);
		//m_menu->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_pMenu->retain();
		m_pMenu->setEnabled(false);

		_container->addChild(m_pMenu, 10);

		if (_container->m_pListener == NULL && _container->m_pfnSelector == NULL)
		{
			m_pMenu->setEnabled(false);   // the butoon can note be pressed
		}
		else
		{
			m_pMenu->setEnabled(true);
		}
	}

	int RichElementButton::getCharCount()
	{
		return StrUtils::calcCharCount(buttonName.c_str());
	}

	/////////////////////////////////////////////////////////

	CCRichElementText::CCRichElementText()
	{
		type = RichElementType_Text;
		m_fontDefinition.hasColorFlag = false;   // default false
		m_fontDefinition.fontColor = Color3B(255, 255, 255);   // default color is white
	}

	CCRichElementText::CCRichElementText(Color3B& color)
	{
		type = RichElementType_Text;
		m_fontDefinition.hasColorFlag = true;
		m_fontDefinition.fontColor = color;
	}

	CCRichElementText::~CCRichElementText()
	{
		CC_SAFE_RELEASE(m_label);
	}

	Size CCRichElementText::getContentSize()
	{
		CCAssert(m_label != NULL, "label should not be NULL");

		return this->m_label->getContentSize();
	}

	void CCRichElementText::setPosition(Vec2 pos)
	{
		CCAssert(m_label != NULL, "label should not be NULL");

		m_label->setPosition(pos);
	}

	Vec2 CCRichElementText::getPosition()
	{
		return m_label->getPosition();
	}

	Node* CCRichElementText::getNode()
	{
		return m_label;
	}

	void CCRichElementText::parse(const char* elementStr)
	{
		m_string = elementStr;
		// Marker Felt, Helvetica, Arial
		// res_ui/font/simhei.ttf
		float fontSize = _container->getFontSize();
		m_label = Label::createWithTTF(elementStr, APP_FONT_NAME, fontSize, Size::ZERO, TextHAlignment::LEFT);
		if (_container->getLabelDefinition().strokeEnabled)
		{
			auto blackColor = Color4B::BLACK;   // black
			m_label->enableOutline(blackColor, 2.0f);
		}
		if (_container->getLabelDefinition().shadowEnabled)
		{
			auto blackColor = Color4B::BLACK;    // black
			m_label->enableShadow(blackColor, Size(1.0f, -1.0f), 1.0f);
		}
		m_label->setAnchorPoint(Vec2::ZERO);
		if (!m_fontDefinition.hasColorFlag)
			m_fontDefinition.fontColor = _container->getLabelDefinition().fontColor;
		m_label->setColor(m_fontDefinition.fontColor);
		m_label->retain();

		_container->addChild(m_label, 0);
	}

	int CCRichElementText::getCharCount()
	{
		return StrUtils::calcCharCount(m_string.c_str());
	}

	////////////////////////////////////////////////////

	RichElementReturn::RichElementReturn()
	{
		type = RichElementType_Return;
	}

	RichElementReturn::~RichElementReturn()
	{
		CC_SAFE_RELEASE(m_node);
	}

	Size RichElementReturn::getContentSize()
	{
		return Size(0, 30);   // is it ok?
	}

	void RichElementReturn::setPosition(Vec2 pos)
	{
		CCAssert(m_node != NULL, "node should not be NULL");

		m_node->setPosition(pos);
	}

	Vec2 RichElementReturn::getPosition()
	{
		return m_node->getPosition();
	}

	Node* RichElementReturn::getNode()
	{
		return m_node;
	}

	void RichElementReturn::parse(const char* elementStr)
	{
		m_string = elementStr;

		m_node = Node::create();
		m_node->retain();

		_container->addChild(m_node, 0);
	}

	int RichElementReturn::getCharCount()
	{
		return 0;
	}


