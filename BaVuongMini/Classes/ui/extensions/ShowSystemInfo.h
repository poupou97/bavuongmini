
#ifndef _EXTENSIONS_SHOWSYSTEMINFO_H_
#define _EXTENSIONS_SHOWSYSTEMINFO_H_

#include "UIScene.h"
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class ShowSystemInfo : public UIScene
{
public:
	ShowSystemInfo();
	~ShowSystemInfo();

	static ShowSystemInfo * create(const char * str_info,float sizeScale = 1.0f);
	bool init(const char * str_info,float sizeScale);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

};

#endif

