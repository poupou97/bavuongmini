#include "AlertDialog.h"
#include "CCRichLabel.h"

AlertDialog::AlertDialog()
{
}

AlertDialog::~AlertDialog()
{
}

AlertDialog * AlertDialog::create(std::string string)
{
	auto dialog = new AlertDialog();
	if(dialog && dialog->init(string))
	{
		dialog->autorelease();
		return dialog;
	}
	CC_SAFE_DELETE(dialog);
	return NULL;
}

bool AlertDialog::init(std::string string)
{
	if (UIScene::init())
	{
		Size winSize =Director::getInstance()->getVisibleSize();

		auto layer = Layer::create();
		layer->setIgnoreAnchorPointForPosition(false);
		layer->setAnchorPoint(Vec2(0.5f,0.5f));
		layer->setPosition(Vec2(0,0));
		layer->setContentSize(winSize);
		addChild(layer);
        
#if CC_TARGET_PLATFORM==CC_PLATFORM_IOS
        const int offsetY = 2;
#else
        const int offsetY = 0;
#endif
		
		auto labelLink=CCRichLabel::createWithString(string.c_str(),Size(600,50),NULL,NULL,0,20.f,5,CCRichLabel::alignment_vertical_center);
		labelLink->setAnchorPoint(Vec2(0.5f,0.5f));
		labelLink->setPosition(Vec2(layer->getContentSize().width/2, layer->getContentSize().height/2 + offsetY));
		layer->addChild(labelLink,1);

		auto backGroundSp = cocos2d::extension::Scale9Sprite::create("res_ui/kuang_1.png");
		backGroundSp->setCapInsets(Rect(0,0,0,0));
		backGroundSp->setPreferredSize(Size(labelLink->getContentSize().width+30,labelLink->getContentSize().height+8));
		backGroundSp->setAnchorPoint(Vec2(0.5f,0.5f));
		backGroundSp->setPosition(Vec2(winSize.width/2,winSize.height/2));
		layer->addChild(backGroundSp);

		this->setContentSize(backGroundSp->getContentSize());

		/*
		Layout * panel=Layout::create();
		panel->setAnchorPoint(Vec2(0,0));
		panel->setPosition(Vec2(0,0));
		m_pLayer->addChild(panel);

		Label * labelText= Label::create();
		labelText->setAnchorPoint(Vec2(0.5f,0.5f));
		labelText->setPosition(Vec2(panel->getContentSize().width/2,panel->getContentSize().height/2+3));
		labelText->setFontSize(20);
		labelText->setText(string.c_str());
		labelText->setLocalZOrder(10);
		panel->addChild(labelText);

		ImageView * imageBg=ImageView::create();
		imageBg->loadTexture("res_ui/kuang_1.png");
		imageBg->setAnchorPoint(Vec2(0.5f,0.5f));
		//imageBg->setTextureRect(Rect(11,11,1,1));
		imageBg->setScale9Enabled(true);
		imageBg->setContentSize(Size(labelText->getContentSize().width+20,labelText->getContentSize().height+20));
		imageBg->setPosition(Vec2(0,0));
		panel->addChild(imageBg);
		*/
		auto action =(ActionInterval *)Sequence::create(
			Show::create(),
			DelayTime::create(2.6f),
			RemoveSelf::create(),
			NULL);
		
		this->runAction(action);
		return true;
	}
	return false;
}

void AlertDialog::onEnter()
{
	UIScene::onEnter();
}

void AlertDialog::onExit()
{
	UIScene::onExit();
}
