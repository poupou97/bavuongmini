
#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class Counter:public UIScene
{
public:
	Counter(void);
	~Counter(void);

	static Counter * create();
	bool init(std::string test);
	virtual void onEnter();
	virtual void onExit();

	void getAmount(Ref *pSender, Widget::TouchEventType type);
	void clearNum(Ref *pSender, Widget::TouchEventType type);
	void deleteNum(Ref *pSender, Widget::TouchEventType type);
	void sendButton(Ref *pSender, Widget::TouchEventType type);
	void callBackExit(Ref *pSender, Widget::TouchEventType type);

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	int getInputNum();
	void setShowNumDefault(int counts);
	void callBackSendNum(Ref* pSender, SEL_CallFuncO pSelector);
private:
	int sendNum_;
	Ref * senderListener;
	SEL_CallFuncO senderSelector;
private:
	//int m_num;
	std::string goodsNum;
	Label * showNum;
	int goodSendNum;
};

