#include "ControlTree.h"
#include "GameView.h"
#include "../extensions/UIButtonTree.h"
#include "../../utils/GameUtils.h"
#include "AppMacros.h"

using namespace cocos2d::ui;

ControlTree::ControlTree(void)
{
	m_treeSelectState = "res_ui/highlight.png";
	m_secondTreeImage = "res_ui/no6_di.png";

	m_expandImage = "res_ui/plus2.png";
	m_shrink = "res_ui/jianhao.png";

	senderListener_tree = NULL;
	senderListener_branch = NULL;

	m_touchTreeIndex = 0;
	m_secondTreeOfPanelIndex = 0;

	m_isLastTree = true;
}

ControlTree::~ControlTree(void)
{
}

ControlTree * ControlTree::create(std::vector<TreeStructInfo *> treeVector,Size scrollSize,Size treeButtonSize,Size branchButtonSize)
{
	auto tree_ = new ControlTree();
	if (tree_ && tree_->init(treeVector,scrollSize,treeButtonSize,branchButtonSize))
	{
		tree_->autorelease();
		return tree_;
	}
	CC_SAFE_DELETE(tree_);
	return NULL;
}

bool ControlTree::init(std::vector<TreeStructInfo *> treeVector,Size scrollSize,Size treeButtonSize,Size branchButtonSize)
{
	if (UIScene::init())
	{
		for (int i = 0;i< treeVector.size();i++)
		{
			m_treeVector.push_back(treeVector.at(i));
		}
		
// 		ImageView * imageFrame =ImageView::create();
// 		imageFrame->loadTexture("res_ui/zhezhao01.png");
// 		imageFrame->setAnchorPoint(Vec2(0,0));
// 		imageFrame->setScale9Enabled(true);
// 		imageFrame->setContentSize(scrollSize);
// 		imageFrame->setCapInsets(Rect(50,50,1,1));
// 		imageFrame->setPosition(Vec2(0,0));
// 		m_pLayer->addChild(imageFrame);

		scrollView_info = ui::ScrollView::create();
		scrollView_info->setTouchEnabled(true);
		scrollView_info->setDirection(ui::ScrollView::Direction::VERTICAL);
		scrollView_info->setContentSize(scrollSize);
		scrollView_info->setPosition(Vec2(0,0));
		scrollView_info->setBounceEnabled(true);
		m_pLayer->addChild(scrollView_info);
		int innerHeight = 0;
		for (int i = 1;i<m_treeVector.size()+1;i++)
		{
			auto panel_ = Layout::create();
			panel_->setAnchorPoint(Vec2(0,0));
			panel_->setTag(i);
			scrollView_info->addChild(panel_);
			
			auto button_= Button::create();
			button_->setTouchEnabled(true);
			button_->setPressedActionEnabled(true);
			button_->addTouchEventListener(CC_CALLBACK_2(ControlTree::callBackTreeCellEvent, this));
			button_->setAnchorPoint(Vec2(0.5f,0.5f));
			button_->setScale9Enabled(true);
			button_->setContentSize(treeButtonSize);
			button_->setCapInsets(Rect(15,30,1,1));
			button_->setPosition(Vec2(0,0));
			button_->loadTextures("res_ui/kuang01_new.png","res_ui/kuang01_new.png","");
			button_->setTag(i*TREE_BUTTON_TAG);
			panel_->addChild(button_);

			auto buttonSelectState_ = ui::ImageView::create();
			buttonSelectState_->loadTexture(m_treeSelectState.c_str());			
			buttonSelectState_->setScale9Enabled(true);
			buttonSelectState_->setContentSize(button_->getContentSize());
			buttonSelectState_->setAnchorPoint(Vec2(0.5f,0.5f));
			buttonSelectState_->setPosition(Vec2(0,0));
			buttonSelectState_->setVisible(false);
			buttonSelectState_->setName(BUTTON_SELECT_STATE);
			button_->addChild(buttonSelectState_);
			
			//tree label 
			std::string label_ = m_treeVector.at(i-1)->getTreeName();
			auto button_label = Label::createWithTTF(label_.c_str(), APP_FONT_NAME,18);
			button_label->setAnchorPoint(Vec2(0.5f,0.5f));
			button_label->setPosition(Vec2(0,0 ));
			button_label->setLocalZOrder(10);
			button_->addChild(button_label);

			auto buttonState_ =ui::ImageView::create();
			buttonState_->loadTexture(m_expandImage.c_str());
			buttonState_->setAnchorPoint(Vec2(0.5f,0.5f));
			buttonState_->setPosition(Vec2(-button_->getContentSize().width/2+buttonState_->getContentSize().width/2,0));
			buttonState_->setScale(0.5f);
			buttonState_->setName(BUTTON_EXPAND_SHRINK_MARK);
			buttonState_->setLocalZOrder(10);
			button_->addChild(buttonState_);

			if (i == 1)
			{
				//set scrollView_info size
				tree_buttonHeight = button_->getContentSize().height;
				innerHeight = m_treeVector.size() * button_->getContentSize().height;
				if (innerHeight < scrollSize.height)
				{
					innerHeight = scrollSize.height;
				}

				buttonSelectState_->setVisible(true);
				scrollView_info->setInnerContainerSize(Size(scrollView_info->getContentSize().width,innerHeight));
			}

			this->setButtonTreeSize(treeButtonSize);
			this->setBranchButtonSize(branchButtonSize);

			panel_->setContentSize(Size(scrollView_info->getContentSize().width,button_->getContentSize().height));
			panel_->setPosition(Vec2(0,innerHeight - button_->getContentSize().height*i));
			button_->setPosition(Vec2(panel_->getContentSize().width/2,panel_->getContentSize().height - button_->getContentSize().height/2));
		}
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(scrollView_info->getContentSize());
		return true;
	}
	return false;
}

void ControlTree::onEnter()
{
	UIScene::onEnter();
}

void ControlTree::onExit()
{
	UIScene::onExit();
}

void ControlTree::callBackTreeCellEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn_ = (Button *)pSender;
		int btnIndex_ = btn_->getTag();

		if (this->getTouchTreeIndex() == btnIndex_ / TREE_BUTTON_TAG - 1)
		{
			this->setIsLastTree(true);
		}
		else
		{
			this->setIsLastTree(false);
		}

		this->setTouchTreeIndex(btnIndex_ / TREE_BUTTON_TAG - 1);

		auto panel_index = (Layout*)scrollView_info->getChildByTag(btnIndex_ / TREE_BUTTON_TAG);
		auto panel_secondTree = (Layout*)panel_index->getChildByTag(btnIndex_*TREE_BUTTON_TAG);

		if (btn_->getChildByName(BUTTON_EXPAND_SHRINK_MARK) != NULL)
		{
			auto image_ = (ui::ImageView *)btn_->getChildByName(BUTTON_EXPAND_SHRINK_MARK);
			int vectorindex_ = btnIndex_ / TREE_BUTTON_TAG - 1;
			if (panel_secondTree == NULL)
			{
				image_->loadTexture(m_shrink.c_str());

				//setvisible else expand secondtree,keep only one show
				for (int i = 1; i<m_treeVector.size() + 1; i++)
				{
					auto panel_index = (Layout *)scrollView_info->getChildByTag(i);
					auto panel_secondTree = (Layout *)panel_index->getChildByTag(i*SECOND_PANEL_TAG);
					if (panel_secondTree != NULL)
					{
						auto temp_button = (Button *)panel_index->getChildByTag(i*TREE_BUTTON_TAG);
						if (temp_button != NULL && temp_button->getChildByName(BUTTON_EXPAND_SHRINK_MARK))
						{
							auto image_ = (ui::ImageView *)temp_button->getChildByName(BUTTON_EXPAND_SHRINK_MARK);
							image_->loadTexture(m_expandImage.c_str());
						}
						this->createSecondtree(i*TREE_BUTTON_TAG, m_treeVector.at(i - 1)->getSecondVector().size(), false);
					}
				}

				this->createSecondtree(btnIndex_, m_treeVector.at(vectorindex_)->getSecondVector().size(), true);

				//callBack addEvent
				/*if (senderListener_tree && senderSelector_tree)
				{
				(senderListener_tree->*senderSelector_tree)(this);
				}*/
			}
			else
			{
				image_->loadTexture(m_expandImage.c_str());
				this->createSecondtree(btnIndex_, m_treeVector.at(vectorindex_)->getSecondVector().size(), false);

				//first setvisible all select state
				this->setAllButtonSelectStateVisible();
				if (btn_->getChildByName(BUTTON_SELECT_STATE) != NULL)
				{
					auto selectState_ = (ui::ImageView *)btn_->getChildByName(BUTTON_SELECT_STATE);
					selectState_->setVisible(true);
				}
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void ControlTree::createSecondtree(int btnIndex,int num,bool isCreate)
{
	auto panel_index = (Layout *)scrollView_info->getChildByTag(btnIndex/TREE_BUTTON_TAG);
	auto button_index = (Button *)panel_index->getChildByTag(btnIndex);
	
	int w_ = 0;
	int h_ = 0;
	int off_h = 0;
	int scroll_h = scrollView_info->getInnerContainer()->getContentSize().height;

	if (isCreate)
	{
		int temp_index_root = btnIndex/TREE_BUTTON_TAG;
		auto panel_second = Layout::create();
		panel_second->setAnchorPoint(Vec2(0,0));
		panel_second->setTouchEnabled(true);
		panel_second->setTag(temp_index_root*SECOND_PANEL_TAG);
		panel_index->addChild(panel_second);
		long long startTime = GameUtils::millisecondNow();//-----
		int secondTree_size = m_treeVector.at(btnIndex/TREE_BUTTON_TAG - 1)->getSecondVector().size();
		for (int i = 1;i<secondTree_size+1;i++)
		{
			auto button_branch= ButtonTree::create();
			button_branch->setTouchEnabled(true);
			//button_branch->setPressedActionEnabled(true);
			button_branch->addTouchEventListener(CC_CALLBACK_2(ControlTree::callBackSecondTreeCell, this));
			button_branch->setAnchorPoint(Vec2(0.5f,0.5f));
			button_branch->setScale9Enabled(true);
			button_branch->setContentSize(getBranchButtonSize());
			//button_branch->setCapInsets(Rect(32,12,1,1));//39,7
			button_branch->setPosition(Vec2(0,0));
			button_branch->setInfo(btnIndex/TREE_BUTTON_TAG);
			button_branch->loadTextures(m_secondTreeImage.c_str(),m_secondTreeImage.c_str(),"");
			button_branch->setTag(i*SECOND_BUTTONTREE_TAG);
			panel_second->addChild(button_branch);
			
			auto second_btnSelect = ui::ImageView::create();
			//second_btnSelect->loadTexture(m_treeSelectState.c_str());"res_ui/no1_light.png"
			second_btnSelect->loadTexture("res_ui/no1_light.png");
			second_btnSelect->setScale9Enabled(true);
			second_btnSelect->setContentSize(button_branch->getContentSize());
			//second_btnSelect->setCapInsets(Rect(39,7,1,1));//32,12
			second_btnSelect->setAnchorPoint(Vec2(0.5f,0.5f));
			second_btnSelect->setPosition(Vec2(0,0));
			second_btnSelect->setVisible(false);
			second_btnSelect->setName(SECOND_BUTTON_SELECT_STATE);
			button_branch->addChild(second_btnSelect);

			std::string labelStr_  =  (m_treeVector.at(btnIndex/100 - 1)->getSecondVector()).at(i -1 );
			auto button_label = Label::createWithTTF(labelStr_.c_str(), APP_FONT_NAME,18);
			button_label->setAnchorPoint(Vec2(0.5f,0.5f));
			button_label->setPosition(Vec2(0,0 ));
			button_label->setLocalZOrder(10);
			button_branch->addChild(button_label);

			if (i == 1)
			{
				secondTree_buttonHeight = button_branch->getContentSize().height;
				panel_second->setContentSize(Size(scrollView_info->getContentSize().width,button_branch->getContentSize().height*num));
			}
			button_branch->setPosition(Vec2(0,panel_second->getContentSize().height - button_branch->getContentSize().height *(i - 1)));
		}

		h_ = panel_index->getContentSize().height + num*secondTree_buttonHeight;
		panel_index->setContentSize(Size(w_,h_));
		
		button_index->setPosition(Vec2(scrollView_info->getContentSize().width/2,h_ - button_index->getContentSize().height/2));
		panel_second->setPosition(Vec2(scrollView_info->getContentSize().width/2,-secondTree_buttonHeight/2));
		off_h = num *secondTree_buttonHeight;

		//default first select
		int tempIndex_select = 0;
		int curBtnIndex_ = btnIndex/TREE_BUTTON_TAG - 1;
		if (this->getIsLastTree())
		{
			tempIndex_select = this->getSecondTreeByEveryPanelIndex()+1; 
		}else
		{
			tempIndex_select++;
		}
		
		auto btn_tree = (ButtonTree *)panel_second->getChildByTag(tempIndex_select*SECOND_BUTTONTREE_TAG);
		callBackSecondTreeCell(btn_tree,Widget::TouchEventType::ENDED);


		long long endTime = GameUtils::millisecondNow();
		CCLOG("cost time = %ld",endTime - startTime);
	}else
	{
		h_ = panel_index->getContentSize().height - num * secondTree_buttonHeight;
		panel_index->setContentSize(Size(w_,h_));
		button_index->setPosition(Vec2(scrollView_info->getContentSize().width/2,h_ - button_index->getContentSize().height/2));

		off_h = -num*secondTree_buttonHeight;
		auto panel_secondTree = (Layout *)panel_index->getChildByTag(btnIndex*TREE_BUTTON_TAG);
		if (panel_secondTree != NULL)
		{
			panel_secondTree->removeFromParentAndCleanup(true);
		}
	}
	scrollView_info->setInnerContainerSize(Size(scrollView_info->getContentSize().width,scroll_h + off_h));
	int scroll_inner = scroll_h + off_h;
	//
	for (int j= 1;j<=m_treeVector.size();j++)
	{
		auto panel_temp = (Layout *)scrollView_info->getChildByTag(j);
		scroll_inner -= panel_temp->getContentSize().height;
		panel_temp->setPosition(Vec2(0,scroll_inner));
	}
}

void ControlTree::callBackSecondTreeCell(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->setAllButtonSelectStateVisible();

		auto btn_ = (ButtonTree *)pSender;
		auto image_second = (ui::ImageView *)btn_->getChildByName(SECOND_BUTTON_SELECT_STATE);
		if (image_second != NULL)
		{
			image_second->setVisible(true);
		}

		//set secondButton of panelIndex
		this->setSecondTreeByEveryPanelIndex(btn_->getTag() / SECOND_BUTTONTREE_TAG - 1);

		//get secondButton by vector index
		int vectorIndex_ = 0;
		for (int i = 0; i<m_treeVector.size(); i++)
		{
			if (i< btn_->getInfo() - 1)
			{
				vectorIndex_ += m_treeVector.at(i)->getSecondVector().size();
			}
		}
		vectorIndex_ += btn_->getTag() / SECOND_BUTTONTREE_TAG;
		int secondTreeIndex_ = vectorIndex_ - 1;
		this->setSecondTreeIndex(secondTreeIndex_);

		//callBack addEvent
		/*if (senderListener_branch && senderSelector_branch)
		{
		(senderListener_branch->*senderSelector_branch)(this);
		}*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void ControlTree::setAllButtonSelectStateVisible()
{
	for (int i= 1;i<=m_treeVector.size();i++)
	{
		auto panel_temp = (Layout *)scrollView_info->getChildByTag(i);
		auto button_ = (Button *)panel_temp->getChildByTag(i*TREE_BUTTON_TAG);
		if (button_->getChildByName(BUTTON_SELECT_STATE))
		{
			auto image_ = (ui::ImageView *)button_->getChildByName(BUTTON_SELECT_STATE);
			image_->setVisible(false);
		}
		
		auto panel_selecttree = (Layout *)panel_temp->getChildByTag(i*SECOND_PANEL_TAG);
		if (panel_selecttree != NULL)
		{
			cocos2d::Vector<cocos2d::Node*> obj = panel_selecttree->getChildren();
			for (int j =0;j<obj.size();j++)
			{
				auto btn_select = (Button *)obj.at(j);
				if (btn_select != NULL)
				{
					auto image_second = (ui::ImageView *)btn_select->getChildByName(SECOND_BUTTON_SELECT_STATE);
					if (image_second != NULL)
					{
						image_second->setVisible(false);
					}
				}
			}
		}
	}
}

void ControlTree::setTreeSelectState( std::string imagePath )
{
	m_treeSelectState = imagePath;
}

void ControlTree::setSecondTreeImage( std::string imagePath )
{
	m_secondTreeImage = imagePath;
}

void ControlTree::addcallBackTreeEvent( Ref* pSender, SEL_CallFuncO pSelector )
{
	senderListener_tree = pSender;
	senderSelector_tree = pSelector;
}

void ControlTree::addcallBackSecondTreeEvent( Ref* pSender, SEL_CallFuncO pSelector )
{
	senderListener_branch = pSender;
	senderSelector_branch = pSelector;
}


Button * ControlTree::getTreeButtonByIndex( int vectorIndex )
{
	//get cur panel
	int panelIndex_ = this->getTreeIndex(vectorIndex);
	auto panel_ = (Layout *)scrollView_info->getChildByTag(panelIndex_);
	Button * btn_;
	if (panel_ != NULL)
	{
		//get cur button
		btn_ = (Button *)panel_->getChildByTag(panelIndex_*TREE_BUTTON_TAG);
	}
	return btn_;
}

void ControlTree::addMarkOnButton( std::string str,Vec2 p,int vectorIndex /* = -1*/ )
{
	if (vectorIndex <0)
	{
		for (int i = 1;i<m_treeVector.size()+1;i++)
		{
			auto panel_ = (Layout *)scrollView_info->getChildByTag(i);
			auto btn_ = (Button *)panel_->getChildByTag(i*TREE_BUTTON_TAG);
			if (btn_ != NULL && btn_->getChildByName(BUTTON_NEWADDMARK) == NULL)
			{
				auto mark_ =ui::ImageView::create();
				mark_->loadTexture(str.c_str());
				mark_->setAnchorPoint(Vec2(0.5f,0.5f));
				mark_->setPosition(p);
				mark_->setName(BUTTON_NEWADDMARK);
				btn_->addChild(mark_);
			}
		}
	}else
	{
		auto btn_ = this->getTreeButtonByIndex(vectorIndex);
		if (btn_ != NULL && btn_->getChildByName(BUTTON_NEWADDMARK) == NULL)
		{
			auto mark_ = ui::ImageView::create();
			mark_->loadTexture(str.c_str());
			mark_->setAnchorPoint(Vec2(0.5f,0.5f));
			mark_->setPosition(p);
			mark_->setName(BUTTON_NEWADDMARK);
			btn_->addChild(mark_);
		}
	}
}

void ControlTree::removeMarkOnButton( int vectorIndex /*= -1*/ )
{
	if (vectorIndex <0)
	{
		for (int i = 1;i<m_treeVector.size()+1;i++)
		{
			auto panel_ = (Layout *)scrollView_info->getChildByTag(i);
			auto btn_ = (Button *)panel_->getChildByTag(i*TREE_BUTTON_TAG);
			if (btn_ != NULL && btn_->getChildByName(BUTTON_NEWADDMARK) != NULL)
			{
				auto mark_ = (ui::ImageView *)btn_->getChildByName(BUTTON_NEWADDMARK);
				mark_->removeFromParentAndCleanup(true);
			}
		}
	}else
	{
		auto btn_ = this->getTreeButtonByIndex(vectorIndex);
		if (btn_ != NULL)
		{
			auto mark_ = (ui::ImageView *)btn_->getChildByName(BUTTON_NEWADDMARK);
			if (mark_)
			{
				mark_->removeFromParentAndCleanup(true);
			}						
		}
	}
}

int ControlTree::getTreeIndex( int vectorIndex)
{
	int temp_index = vectorIndex+1;
	for (int i =0;i<m_treeVector.size();i++)
	{
		temp_index -=m_treeVector.at(i)->getSecondVector().size();
		if (temp_index <= 0)
		{
			return i+1;
		}
	}
}

ButtonTree * ControlTree::getSecondTreeButtonByIndex( int vectorIndex)
{
	ButtonTree * btn_ = NULL;
	int temp_index = vectorIndex+1;
	for (int i = 1;i<m_treeVector.size()+1;i++)
	{
		temp_index -=m_treeVector.at(i - 1)->getSecondVector().size();
		if (temp_index <= 0)
		{
			auto panel_ = (Layout *)scrollView_info->getChildByTag(i);
			auto second_panel = (Layout *)panel_->getChildByTag(i*SECOND_PANEL_TAG);
			int btnindex_ =  temp_index+m_treeVector.at(i-1)->getSecondVector().size();
			btn_ = (ButtonTree *)second_panel->getChildByTag(btnindex_*SECOND_BUTTONTREE_TAG);
			break;
		}
	}
	return btn_;
}

ButtonTree * ControlTree::getSecondTreeByPanelIndexOrSecondIndex( int panelIndex,int secondIndex )
{
	ButtonTree * btn_ = NULL;
	auto panel_ = (Layout *)scrollView_info->getChildByTag(panelIndex);
	auto second_panel = (Layout *)panel_->getChildByTag(panelIndex*SECOND_PANEL_TAG);
	btn_ = (ButtonTree *)second_panel->getChildByTag(secondIndex*SECOND_BUTTONTREE_TAG);
	return btn_;
}


void ControlTree::addMarkOnSecondButton( std::string str,Vec2 p,int vectorIndex /*= -1*/ )
{
	auto btn_ = (ButtonTree *)this->getSecondTreeButtonByIndex(vectorIndex);
	if (btn_ && btn_->getChildByName(SECOND_BUTTON_NEWADDMARK)== NULL)
	{
		auto mark_ =ui::ImageView::create();
		mark_->loadTexture(str.c_str());
		mark_->setAnchorPoint(Vec2(0.5f,0.5f));
		mark_->setPosition(p);
		mark_->setName(SECOND_BUTTON_NEWADDMARK);
		btn_->addChild(mark_);
	}
}

void ControlTree::removeMarkOnSecondButton( int vectorIndex /*= -1*/ )
{
	if (vectorIndex <0)
	{
		for (int i = 1;i<m_treeVector.size()+1;i++)
		{
			auto panel_ = (Layout *)scrollView_info->getChildByTag(i);
			auto panel_second = (Layout *)panel_->getChildByTag(i*SECOND_PANEL_TAG);

			for (int j = 1;j<m_treeVector.at(i - 1)->getSecondVector().size()+1;j++)
			{
				auto treeButton_ = (ButtonTree *)panel_second->getChildByTag(j*SECOND_BUTTONTREE_TAG);
				if (treeButton_ != NULL && treeButton_->getChildByName(SECOND_BUTTON_NEWADDMARK) != NULL)
				{
					auto mark_ = (ui::ImageView *)treeButton_->getChildByName(SECOND_BUTTON_NEWADDMARK);
					mark_->removeFromParentAndCleanup(true);
				}
			}
		}
	}else
	{
		auto btn_ =(ButtonTree *) this->getSecondTreeButtonByIndex(vectorIndex);
		if (btn_ && btn_->getChildByName(SECOND_BUTTON_NEWADDMARK) != NULL)
		{
			auto mark_ = (ui::ImageView *)btn_->getChildByName(SECOND_BUTTON_NEWADDMARK);
			mark_->removeFromParentAndCleanup(true);
		}
	}
}

void ControlTree::setSecondTreeIndex( int index )
{
	m_curSelectIndex = index;
}

int ControlTree::getSecondTreeIndex()
{
	return m_curSelectIndex;
}

void ControlTree::setSecondTreeByEveryPanelIndex( int panelIndex )
{
	m_secondTreeOfPanelIndex = panelIndex;
}

int ControlTree::getSecondTreeByEveryPanelIndex()
{
	return m_secondTreeOfPanelIndex;
}


Size ControlTree::getBranchButtonSize()
{
	return branchButton_size;
}

void ControlTree::setBranchButtonSize( Size size_ )
{
	branchButton_size = size_;
}

void ControlTree::setButtonTreeSize( Size size_ )
{
	treeButton_size = size_;
}

Size ControlTree::getButtonTreeSize()
{
	return treeButton_size;
}

void ControlTree::removeSecondButton( int vectorIndex )
{
	/*
	//delete second button
	ButtonTree * btn_ =(ButtonTree *)this->getSecondTreeButtonByIndex(vectorIndex);
	if (btn_ )
	{
		btn_->removeFromParentAndCleanup(true);
	}

	int btnindex_ = 0;
	int vectorIndex_ = 0;
	//refresh tree data
	int temp_index = vectorIndex+1;
	for (int i = 1;i<m_treeVector.size()+1;i++)
	{
		temp_index -=m_treeVector.at(i - 1)->getSecondVector().size();
		if (temp_index <= 0)
		{
			btnindex_ =  temp_index+m_treeVector.at(i-1)->getSecondVector().size() - 1;
			vectorIndex_ = i - 1;

			TreeStructInfo * temp = m_treeVector.at(i - 1);
			std::vector<std::string> tempVector = temp->getSecondVector();
			std::vector<std::string>::iterator iter = tempVector.begin()+btnindex_;
			tempVector.erase(iter);
			
			int a = m_treeVector.at(vectorIndex_)->getSecondVector().size();			
			(m_treeVector.at(vectorIndex_)->getSecondVector()).clear();
			a = m_treeVector.at(vectorIndex_)->getSecondVector().size();
			break;
		}
	}
	
	//reSet every panel and button position
	//int btnIndex_temp = btnindex_+1;
	int btnIndex_temp = this->getTreeIndex(vectorIndex);
	int size_ = 3;//m_treeVector.at(vectorIndex_)->getSecondVector().size();
	Layout * panel_index = (Layout *)scrollView_info->getChildByTag(btnIndex_temp);
	Layout * branchPanel_ = (Layout *)panel_index->getChildByTag(btnIndex_temp*SECOND_PANEL_TAG);
	branchPanel_->setContentSize(Size(scrollView_info->getContentSize().width,secondTree_buttonHeight*size_));
	
	for (int i = 1;i< size_+1;i++)
	{
		ButtonTree * secondTree_ = (ButtonTree *)branchPanel_->getChildByTag(i*SECOND_BUTTONTREE_TAG);
		if (secondTree_ != NULL)
		{
			secondTree_->setPosition(Vec2(0,branchPanel_->getContentSize().height - secondTree_->getContentSize().height *(i - 1)));
		}
	}


	int scroll_inner = scrollView_info->getInnerContainerSize().height;
	for (int j= 1;j<=m_treeVector.size();j++)
	{
		Layout * panel_temp = (Layout *)scrollView_info->getChildByTag(j);
		scroll_inner -= panel_temp->getContentSize().height;
		panel_temp->setPosition(Vec2(0,scroll_inner));
	}
	*/
}

void ControlTree::setTouchTreeIndex( int index )
{
	m_touchTreeIndex = index;
}

int ControlTree::getTouchTreeIndex()
{
	return m_touchTreeIndex;
}

void ControlTree::setShowSecondTreeCell( int vectorIndex )
{
	if (vectorIndex<0)
	{
		return;
	}
	auto btn_ = (Button *)getTreeButtonByIndex(vectorIndex);
	this->callBackTreeCellEvent(btn_, Widget::TouchEventType::ENDED);

	auto btn_cell = (ButtonTree *)getSecondTreeButtonByIndex(vectorIndex);
	this->callBackSecondTreeCell(btn_cell, Widget::TouchEventType::ENDED);

	//set position of can see
	int offset_H = btn_->getPosition().y + btn_cell->getPosition().y;

	float h_1 = btn_cell->getPosition().y/scrollView_info->getInnerContainerSize().height;
	if (scrollView_info->getContentSize().height < offset_H)
	{
		float prect_ = 100 - h_1*100;
		scrollView_info->jumpToPercentVertical(prect_);
	}
}

void ControlTree::setIsLastTree( bool isValue )
{
	m_isLastTree = isValue;
}

bool ControlTree::getIsLastTree()
{
	return m_isLastTree;
}




///////////////////////////////////
TreeStructInfo::TreeStructInfo( void )
{

}

TreeStructInfo::~TreeStructInfo( void )
{

}

void TreeStructInfo::setTreeName( std::string name )
{
	m_treeName_ = name;
}

std::string TreeStructInfo::getTreeName()
{
	return m_treeName_;
}

void TreeStructInfo::addSecondVector(std::string secondString)
{
	m_secondTree.push_back(secondString);
}

std::vector<std::string> TreeStructInfo::getSecondVector()
{
	return m_secondTree;
}
