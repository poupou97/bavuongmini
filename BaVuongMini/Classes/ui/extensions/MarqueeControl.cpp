#include "MarqueeControl.h"
#include "CCRichLabel.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../Chat_ui/ChatCell.h"


MarqueeControl::MarqueeControl(void)
{
	m_prioity = 0;
}


MarqueeControl::~MarqueeControl(void)
{
}

MarqueeControl * MarqueeControl::create( std::string string,int priority)
{
	auto marquee= new MarqueeControl();
	if (marquee && marquee->init(string,priority))
	{
		marquee->autorelease();
		return marquee;
	}
	CC_SAFE_DELETE(marquee);
	return NULL;
}

bool MarqueeControl::init( std::string string,int priority)
{
	if (UIScene::init())
	{
		Size winSize =Director::getInstance()->getVisibleSize();

		auto labelLink=CCRichLabel::createWithString(string.c_str(),Size(winSize.width,30),NULL,NULL,0,20,5,CCRichLabel::alignment_vertical_center);
		labelLink->setAnchorPoint(Vec2(0.5f,0));
		labelLink->setPosition(Vec2(0,1));
		labelLink->setScale(0.9f);
		m_pLayer->addChild(labelLink,1);

		auto background_ = cocos2d::extension::Scale9Sprite::create("res_ui/paoMaDeng_di.png");
		background_->setCapInsets(Rect(0,0,0,0));
		background_->setAnchorPoint(Vec2(0.5,0));
		background_->setPosition(Vec2(0,0));
		background_->setPreferredSize(Size(winSize.width,labelLink->getContentSize().height+2));
		m_pLayer->addChild(background_,0);
		auto action =(ActionInterval *)Sequence::create(
			ScaleTo::create(1.0f,1,1),
			DelayTime::create(7.0f),
			ScaleTo::create(0.3f,1,0),
			RemoveSelf::create(),
			NULL);

		m_prioity = priority;

		this->runAction(action);
		this->setContentSize(Size(300,50));
		return true;
	}
	return false;
}

void MarqueeControl::onEnter()
{
	UIScene::onEnter();
}

void MarqueeControl::onExit()
{
	UIScene::onExit();
}

int MarqueeControl::getPrioity()
{
	return m_prioity;
}

////////////////////////////////////////////
FlowerMarquee::FlowerMarquee(void)
{
}


FlowerMarquee::~FlowerMarquee(void)
{
}

FlowerMarquee * FlowerMarquee::create( std::string string)
{
	auto flower_ = new FlowerMarquee();
	if (flower_ && flower_->init(string))
	{
		flower_->autorelease();
		return flower_;
	}
	CC_SAFE_DELETE(flower_);
	return NULL;
}

bool FlowerMarquee::init( std::string string)
{
	if (UIScene::init())
	{
		Size winSize =Director::getInstance()->getVisibleSize();

		labelLink=CCRichLabel::createWithString(string.c_str(),Size(winSize.width - 100,30),NULL,NULL,0);
		labelLink->setAnchorPoint(Vec2(0.5f,0));
		labelLink->setPosition(Vec2(0,1));
		labelLink->setScale(0.9f);
		labelLink->setVisible(false);
		m_pLayer->addChild(labelLink,1);

		m_labelHight = labelLink->getContentSize().height;
		m_labelWidth = labelLink->getContentSize().width;

		background_ = cocos2d::extension::Scale9Sprite::create("res_ui/flower_di.png");
		//background_->setCapInsets(Rect(128,32,1,1));
		background_->setAnchorPoint(Vec2(0.5,0));
		background_->setPosition(Vec2(0,0));
		//background_->setPreferredSize(Size(labelLink->getContentSize().width+100,labelLink->getContentSize().height+2));
		m_pLayer->addChild(background_,0);

		flower_left = Sprite::create("res_ui/props_icon/flower_pink2.png");
		flower_left->setAnchorPoint(Vec2(0.5f,0.5f));
		flower_left->setPosition(Vec2(background_->getPositionX() - background_->getContentSize().width/2+flower_left->getContentSize().width,
													background_->getContentSize().height/2));
		m_pLayer->addChild(flower_left,1);

		flower_right = Sprite::create("res_ui/props_icon/flower_pink2.png");
		flower_right->setAnchorPoint(Vec2(0.5f,0.5f));
		flower_right->setPosition(Vec2(background_->getPositionX()+background_->getContentSize().width/2-flower_right->getContentSize().width,
									background_->getContentSize().height/2));
		flower_right->setRotationSkewY(180);
		m_pLayer->addChild(flower_right,1);

		scheduleUpdate();

		this->setContentSize(Size(300,50));
		return true;
	}
	return false;
}

void FlowerMarquee::onEnter()
{
	UIScene::onEnter();
}

void FlowerMarquee::onExit()
{
	UIScene::onExit();
}

void FlowerMarquee::update( float delta )
{
	if (m_labelWidth  < FLOWERMARQUEE_SP_WIDTH)
	{
		background_->setPreferredSize(Size(background_->getContentSize().width+SP_SPEED*delta,m_labelHight+2));
		flower_left->setPosition(Vec2(background_->getPositionX() - background_->getContentSize().width/2+flower_left->getContentSize().width,
			background_->getContentSize().height/2));

		flower_right->setPosition(Vec2(background_->getPositionX()+background_->getContentSize().width/2-flower_right->getContentSize().width,
			background_->getContentSize().height/2));

		m_labelWidth = background_->getContentSize().width;
	}else
	{
		labelLink->setVisible(true);

		auto action =(ActionInterval *)Sequence::create(
			ScaleTo::create(1.0f,1,1),
			DelayTime::create(7.0f),
			ScaleTo::create(0.3f,1,0),
			RemoveSelf::create(),
			NULL);

		this->runAction(action);
	}
}

////////////////////////////////////////////
ChatAcousticMarquee::ChatAcousticMarquee(void)
{
}


ChatAcousticMarquee::~ChatAcousticMarquee(void)
{
}

ChatAcousticMarquee * ChatAcousticMarquee::create(std::string country,std::string name,std::string content,int vipLv)
{
	auto flower_ = new ChatAcousticMarquee();
	if (flower_ && flower_->init(country,name,content,vipLv))
	{
		flower_->autorelease();
		return flower_;
	}
	CC_SAFE_DELETE(flower_);
	return NULL;
}

bool ChatAcousticMarquee::init(std::string country,std::string name,std::string content,int vipLv)
{
	if (UIScene::init())
	{
		Size winSize =Director::getInstance()->getVisibleSize();
		int itemSize = 0;
		
		auto sp_country = Sprite::create(ChatCell::getCountryImageByStr(country.c_str()).c_str());
		sp_country->setAnchorPoint(Vec2(0,0));
		sp_country->setPosition(Vec2(0,0));
		sp_country->setScale(0.7f);
		m_pLayer->addChild(sp_country,1);
		itemSize = sp_country->getContentSize().width*0.7f;

		auto label_name = Label::createWithTTF(name.c_str(),APP_FONT_NAME, 20);
		label_name->setAnchorPoint(Vec2(0,0));
		label_name->setPosition(Vec2(itemSize,0));
		label_name->setColor(Color3B(0,255,0));//G
		m_pLayer->addChild(label_name,1);
		itemSize += label_name->getContentSize().width;

		Node * widgetVip;
		if (vipLv > 0)
		{
			widgetVip = MainScene::addVipInfoByLevelForNode(vipLv);
			m_pLayer->addChild(widgetVip,1);
			widgetVip->setPosition(Vec2(itemSize + widgetVip->getContentSize().width/2,widgetVip->getContentSize().height/2));

			itemSize += widgetVip->getContentSize().width;
		}

		auto label_colon = Label::createWithTTF(":",APP_FONT_NAME, 20);
		label_colon->setAnchorPoint(Vec2(0,0));
		label_colon->setPosition(Vec2(itemSize+5,0));
		m_pLayer->addChild(label_colon,1);
		itemSize += label_colon->getContentSize().width;

		RichLabelDefinition def;
		def.firstLineIndent = itemSize+5;
		def.fontSize = 20;
		def.alignment = 2;
		auto labelLink = CCRichLabel::create(content.c_str(),Size(330,20),def);
		labelLink->setAnchorPoint(Vec2(0,0));
		labelLink->setPosition(Vec2(0,1));
		m_pLayer->addChild(labelLink,1);

		const std::vector<int>& linesHeight = labelLink->getLinesHeight();
		CCAssert(linesHeight.size() > 0, "out of range");
		int offsetY = labelLink->getContentSize().height - linesHeight.at(0);

		sp_country->setPositionY(offsetY);
		label_name->setPositionY(offsetY);
		if (vipLv > 0)
		{
			widgetVip->setPositionY(offsetY+widgetVip->getContentSize().height/2);
		}
		label_colon->setPositionY(offsetY);

		background_ = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao70.png");
		background_->setOpacity(200);
		background_->setPreferredSize(Size(350,labelLink->getContentSize().height));
		background_->setAnchorPoint(Vec2(0,0));
		background_->setPosition(Vec2(-10,0));
		m_pLayer->addChild(background_,0);

		auto action =(ActionInterval *)Sequence::create(
			ScaleTo::create(1.0f,1,1),
			DelayTime::create(5.0f),
			ScaleTo::create(0.3f,1,0),
			RemoveSelf::create(),
			NULL);
		this->runAction(action);

		//scheduleUpdate();

		this->setContentSize(Size(350,labelLink->getContentSize().height));
		return true;
	}
	return false;
}

void ChatAcousticMarquee::onEnter()
{
	UIScene::onEnter();
}

void ChatAcousticMarquee::onExit()
{
	UIScene::onExit();
}

void ChatAcousticMarquee::update( float delta )
{
	/*
	if (m_labelWidth  < FLOWERMARQUEE_SP_WIDTH)
	{
		background_->setPreferredSize(Size(background_->getContentSize().width+SP_SPEED*delta,m_labelHight+2));
		flower_left->setPosition(Vec2(background_->getPositionX() - background_->getContentSize().width/2+flower_left->getContentSize().width,
			background_->getContentSize().height/2));

		flower_right->setPosition(Vec2(background_->getPositionX()+background_->getContentSize().width/2-flower_right->getContentSize().width,
			background_->getContentSize().height/2));

		m_labelWidth = background_->getContentSize().width;
	}else
	{
		labelLink->setVisible(true);

		auto action =(ActionInterval *)Sequence::create(
			ScaleTo::create(1.0f,1,1),
			DelayTime::create(7.0f),
			ScaleTo::create(0.3f,1,0),
			RemoveSelf::create(),
			NULL);

		this->runAction(action);
	}
	*/
}
