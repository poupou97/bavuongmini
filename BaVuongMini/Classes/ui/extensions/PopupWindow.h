#ifndef _UI_EXTENSIONS_POPUPWINDOW_H_
#define _UI_EXTENSIONS_POPUPWINDOW_H_

#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "UIScene.h"
#include "ui/CocosGUI.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace cocos2d::ui;
/*
* PopupWindow是一个阻塞式的弹出框
* 除了该窗体，其他区域都被屏蔽，而不可交互
*/
class PopupWindow : public UIScene
{
public:
	enum PopWindowType
	{
		KtypeNeverRemove = 0,
		KTypeRemoveByTime = 1,
	}; 
public:
	PopupWindow();
	virtual ~PopupWindow();

	static PopupWindow * create(std::string string,int buttonNum,PopWindowType winType, int remainTime);
	bool init(std::string string,int buttonNum,PopWindowType winType, int remainTime);

	virtual void AddcallBackEvent(Ref* pSender, SEL_CallFuncO pSelectorSure, SEL_CallFuncO pSelectorCancel);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

    //Button * buttonSure;
	//Button * buttonCancel;
	Button * buttonSure;
	Button * buttonCancel;
	
	Label * centerTimeLabel;
	void setButtonPosition();

	virtual void update(float delta);

public:
	//教学
	int mTutorialScriptInstanceId;
	//教学
	virtual void registerScriptCommand(int scriptId);

	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();
private:
	Ref * senderListener;
	SEL_CallFuncO senderSelectorSure;
	SEL_CallFuncO senderSelectorCancel;
	PopWindowType curWindowType;
	int m_nRemainTime;
	void sureEvent(Ref *pSender, Widget::TouchEventType type);
	void cancelEvent(Ref *pSender, Widget::TouchEventType type);
	Label * timeCenter;
};

#endif