
#ifndef _UI_EXTENSION_QUIRY_H
#define  _UI_EXTENSION_QUIRY_H

#include "UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CEquipment;
class CActiveRole;
class CGeneralDetail;

#define  ROLELAYEROFEQUIP 500 
#define  GENERALLAYEROFEQUIP 510

using namespace cocos2d::ui;

class QuiryUI:public UIScene, public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate
{
public:

	enum
	{
		part_arms = 4,
		part_clothes = 2,
		part_ring = 7,
		part_helmet = 0,
		part_shoes = 9,
		part_accessories = 3,
	};


	QuiryUI(void);
	~QuiryUI(void);

	static QuiryUI *create(CActiveRole * activerole,std::vector<CEquipment *>euipmentsVector,std::vector<CGeneralDetail *>generalDetailVector);
	bool init(CActiveRole * activerole,std::vector<CEquipment *>euipmentsVector,std::vector<CGeneralDetail *>generalDetailVector);

	void onEnter();
	void onExit();
	void callBackExit(Ref *pSender, Widget::TouchEventType type);
	void callBackShowEquipInfo(Ref *pSender, Widget::TouchEventType type);
	void callBackShowGeneralEquipInfo(Ref *pSender, Widget::TouchEventType type);

	void refreshCurRoleValue();
	void refreshCurGeneralValue(int idx);

	void setRoleEquipAmount(int amount);
	int getRoleEquipAmount();

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);


	// default implements are used to call script callback if exist
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
private:
	Size winsize;
	Layer *m_Layer;
	ImageView * imagePression;
	Label * labelLevel;
	Label * labelFightValue_;
	Label * labelAllFightValue_;
	
	int curRoleEquipQuility;
	int curRoleIndex;
	CActiveRole * roleValue_;
	std::vector<CEquipment*>playerEquipments;
	std::vector<CGeneralDetail *>generalDetails;

private:
	void setEquipImageVisible(bool value_);
	ImageView * ImageView_arms;
	ImageView * ImageView_clothes;
	ImageView * ImageView_ring;
	ImageView * ImageView_helmet;
	ImageView * ImageView_shoes;
	ImageView * ImageView_accessories;
};

#endif