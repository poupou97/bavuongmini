
#ifndef _HELLOCPP_UITAB_H_
#define _HELLOCPP_UITAB_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace cocos2d::ui;

typedef enum {
	HORIZONTAL,
	VERTICAL,
	VERTICAL_LIST
}TabMode;


typedef void (Ref::*SEL_IndexChangedEvent)(Ref*);
#define coco_indexchangedselector(_SELECTOR) (SEL_IndexChangedEvent)(&_SELECTOR)

class UITab :public Widget
{
public:
	UITab();
	~UITab();

	virtual bool onTouchBegan(const Vec2 &touchPoint);
	virtual void onTouchMoved(const Vec2 &touchPoint);
	virtual void onTouchEnded(const Vec2 &touchPoint);
	virtual void onTouchCancelled(const Vec2 &touchPoint);

	//virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	//virtual void onTouchEnded(Touch *touch, Event * pEvent);
	//virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	//virtual void onTouchMoved(Touch *touch, Event * pEvent);

	static UITab * createWithImage(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allImagePath[],TabMode mode,int space);
	bool initWithImage(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allImagePath[],TabMode mode,int space);

	static UITab * createWithText(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],TabMode mode,int space,int fontSize = 20);
	bool initWithText(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],TabMode mode,int space,int fontSize);

	static UITab * createWithBMFont(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],char * fontName,TabMode mode,int space);
	bool initWithBMFont(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],char * fontName,TabMode mode,int space);
	
	int getCurrentIndex();
	virtual void addIndexChangedEvent(Ref* pSender, SEL_IndexChangedEvent selector);
	virtual void setHighLightImage(char *s);
	virtual void setNormalImage(char *s);
	virtual void setDefaultPanelByIndex(int index);

	virtual void didNotSelectSelf();
	void setAutoClose(bool _isAutoClose);
	void setPressedActionEnabled(bool visible);
public:
    Button * getObjectByIndex(int index);
	void setHightLightLabelColor(Color3B cl);
	void setNormalLabelColor(Color3B cl);
private:
	int m_currentIndex;
	int m_nMaxCount;
	char * highLightImagePath;
	char * normalImagePath;
	int m_spaceHeight;
	int m_spaceWidth;
	bool isPressedActionEnabled;
	TabMode curTabMode;
	void setCurrentIndex(int tempIndex);
	void indexChangedEvent(Ref* pSender);
	void changeToHeightLight(Ref* Psender);
	void changeToHeightLightByIndex(int index);
	TabMode currentTabMod;

	Size normalImageSize;

	Ref* m_pIndexListener;
	SEL_IndexChangedEvent m_pfnIndexSelector;


 	void onTouchBeganEvent(Ref *pSender);
 	void onTouchMovedEvent(Ref *pSender);
	void onTouchEndedEvent(Ref *pSender, Widget::TouchEventType type);

	bool isAutoClose;
};
#endif;

