#ifndef H_MARQUEECONTROL_H
#define H_MARQUEECONTROL_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "./UIScene.h"
class CCRichLabel;
USING_NS_CC;
USING_NS_CC_EXT;

//system marquee
class MarqueeControl:public UIScene
{
public:
	MarqueeControl(void);
	~MarqueeControl(void);
	static MarqueeControl * create(std::string string,int priority);
	bool init(std::string string,int priority);
	
	void onEnter();
	void onExit();

	int getPrioity();
private:
	int m_prioity;
};


//flower marquee
#define FLOWERMARQUEE_SP_WIDTH 740
#define SP_SPEED 500
class FlowerMarquee:public UIScene
{
public:
	FlowerMarquee(void);
	~FlowerMarquee(void);
	static FlowerMarquee * create(std::string string);
	bool init(std::string string);

	void onEnter();
	void onExit();

	void update(float delta);

private:
	cocos2d::extension::Scale9Sprite * background_;
	Sprite * flower_left;
	Sprite * flower_right;
	int m_labelHight;
	int m_labelWidth;
	CCRichLabel *labelLink;
};

//chat acoustic marquee
class ChatAcousticMarquee:public UIScene
{
public:
	ChatAcousticMarquee(void);
	~ChatAcousticMarquee(void);
	static ChatAcousticMarquee * create(std::string country,std::string name,std::string content,int vipLv);
	bool init(std::string country,std::string name,std::string content,int vipLv);

	void onEnter();
	void onExit();
	void update(float delta);
private:
	cocos2d::extension::Scale9Sprite * background_;
	Sprite * flower_left;
	Sprite * flower_right;
	int m_labelHight;
	int m_labelWidth;
	CCRichLabel *labelLink;
};


#endif