#include "ShowSystemInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "ui/CocosGUI.h"

#define DEFAULT_SIZE_WIDTH 326
#define DEFAULT_SIZE_HEIGHT 276

using namespace cocos2d::ui;

ShowSystemInfo::ShowSystemInfo()
{
}


ShowSystemInfo::~ShowSystemInfo()
{
}

ShowSystemInfo * ShowSystemInfo::create( const char * str_info,float sizeScale)
{
	ShowSystemInfo * showSystemInfo = new ShowSystemInfo();
	if (showSystemInfo && showSystemInfo->init(str_info,sizeScale))
	{
		showSystemInfo->autorelease();
		return showSystemInfo;
	}
	CC_SAFE_DELETE(showSystemInfo);
	return NULL;
}

bool ShowSystemInfo::init( const char * str_info,float sizeScale )
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		//���UI
// 		if(LoadSceneLayer::StrategiesDetailInfoPopUpLayer->getWidgetParent() != NULL)
// 		{
// 			LoadSceneLayer::StrategiesDetailInfoPopUpLayer->removeFromParentAndCleanup(false);
// 		}
// 		Layout *ppanel = LoadSceneLayer::StrategiesDetailInfoPopUpLayer;
// 
// 		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
// 		ppanel->getValidNode()->setContentSize(Size(326, 276));
// 		ppanel->setPosition(Vec2::ZERO);
// 		ppanel->setTouchEnabled(true);
// 		ppanel->setScale(1.5f);
// 		m_pLayer->addChild(ppanel);
// 		
		auto imageView_frame_mo = ImageView::create();
		imageView_frame_mo->loadTexture("res_ui/shadow_4.png");
		imageView_frame_mo->setScale9Enabled(true);
		imageView_frame_mo->setContentSize(Size(DEFAULT_SIZE_WIDTH*sizeScale,DEFAULT_SIZE_HEIGHT*sizeScale));
		imageView_frame_mo->setCapInsets(Rect(30,30,1,1));
		imageView_frame_mo->setAnchorPoint(Vec2(.5f,.5f));
		imageView_frame_mo->setPosition(Vec2(DEFAULT_SIZE_WIDTH*sizeScale/2,DEFAULT_SIZE_HEIGHT*sizeScale/2));
		m_pLayer->addChild(imageView_frame_mo);

		auto imageView_frame = ImageView::create();
		imageView_frame->loadTexture("res_ui/dibian_2.png");
		imageView_frame->setScale9Enabled(true);
		imageView_frame->setContentSize(Size(296*sizeScale,248*sizeScale));
		imageView_frame->setCapInsets(Rect(19,19,1,1));
		imageView_frame->setAnchorPoint(Vec2(.5f,.5f));
		imageView_frame->setPosition(Vec2(DEFAULT_SIZE_WIDTH*sizeScale/2,DEFAULT_SIZE_HEIGHT*sizeScale/2));
		m_pLayer->addChild(imageView_frame);

// 		ImageView * imageView_frame_adorn = ImageView::create();
// 		imageView_frame_adorn->loadTexture("res_ui/tietu_left.png");
// 		imageView_frame_adorn->setAnchorPoint(Vec2(.5f,.5f));
// 		imageView_frame_adorn->setPosition(Vec2(141,113));
// 		m_pLayer->addChild(imageView_frame_adorn);

		auto u_layer = Layer::create();
		addChild(u_layer);

		//ؼ�
		auto l_des = Label::createWithTTF(str_info,APP_FONT_NAME,16,Size(255*sizeScale, 0 ), TextHAlignment::LEFT);
		//l_des->setColor(Color3B(47,93,13));

		int scroll_height = l_des->getContentSize().height;
		auto m_scrollView = cocos2d::extension::ScrollView::create();
		m_scrollView->setContentSize(Size(300*sizeScale, 216*sizeScale));
		m_scrollView->setIgnoreAnchorPointForPosition(false);
		m_scrollView->setTouchEnabled(true);
		m_scrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		m_scrollView->setAnchorPoint(Vec2(0,0));
		m_scrollView->setPosition(Vec2(12*sizeScale,32*sizeScale));
		m_scrollView->setBounceable(true);
		m_scrollView->setClippingToBounds(true);
		u_layer->addChild(m_scrollView);

		if (scroll_height > 216*sizeScale)
		{
			m_scrollView->setContentSize(Size(300*sizeScale,scroll_height));
			m_scrollView->setContentOffset(Vec2(0,216*sizeScale-scroll_height));  
		}
		else
		{
			m_scrollView->setContentSize(Size(300*sizeScale,216*sizeScale));
		}
		m_scrollView->setBounceable(true);

		m_scrollView->addChild(l_des);
		l_des->setPosition(Vec2(23*sizeScale,(m_scrollView->getContentSize().height - scroll_height + 0)));

		this->setContentSize(Size(DEFAULT_SIZE_WIDTH*sizeScale,DEFAULT_SIZE_HEIGHT*sizeScale));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(ShowSystemInfo::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(ShowSystemInfo::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(ShowSystemInfo::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(ShowSystemInfo::onTouchCancelled, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void ShowSystemInfo::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ShowSystemInfo::onExit()
{
	UIScene::onExit();
}

bool ShowSystemInfo::onTouchBegan( Touch *touch, Event * pEvent )
{
	return resignFirstResponder(touch,this,false);
}

void ShowSystemInfo::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void ShowSystemInfo::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void ShowSystemInfo::onTouchMoved( Touch *touch, Event * pEvent )
{

}
