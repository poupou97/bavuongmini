#include "UITab.h"
#include <cstdio> 
#include <string> 
#include <sstream>
#include "AppMacros.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace cocos2d::ui;

#define TABBUTTONBASETAG 150

UITab::UITab():
m_pIndexListener(NULL),
m_pfnIndexSelector(NULL),
m_nMaxCount(1),
m_currentIndex(0),
m_spaceWidth(20),
m_spaceHeight(50),
highLightImagePath(""),
isAutoClose(false),
currentTabMod(HORIZONTAL),
isPressedActionEnabled(false)
{
}


UITab::~UITab()
{
	if (curTabMode == VERTICAL_LIST)
	{
		CCLOG("VERTICAL_LIST");
	}
}
/*
bool UITab::onTouchBegan(Touch *touch, Event * pEvent)
{
		//this->removeFromParentAndCleanup(false);
		return true;
}

void UITab::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void UITab::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void UITab::onTouchMoved(Touch *touch, Event * pEvent)
{
}*/

UITab * UITab::createWithImage(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allImagePath[],TabMode mode,int space)
{
	auto tab = new UITab();
	if (tab && tab->initWithImage(maxcount,normalImage,selectedImage,disabledImage,allImagePath,mode,space))
	{
		tab->autorelease();
		return tab;
	}
	CC_SAFE_DELETE(tab);
	return NULL;
}

bool UITab::initWithImage(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allImagePath[],TabMode mode,int space)
{
	if (Widget::init())
	{
		this->normalImagePath= (char *)normalImage;
		if (mode == HORIZONTAL)
		{
			this->m_spaceWidth = space;
		}
		else if (mode == VERTICAL || mode == VERTICAL_LIST)
		{
			this->m_spaceHeight = space;
		}
		this->currentTabMod = mode;
		
		int x = 0;
		int y = 0;
		int aaa = maxcount;
		auto tempImageView = ImageView::create();
		tempImageView->loadTexture(normalImage);
		for (int i = 0;i<maxcount;++i)
		{
			auto _Button = Button::create();
			_Button->loadTextures(normalImage, selectedImage, disabledImage);
			_Button->setTag(TABBUTTONBASETAG+i);
			//_Button->setPressedActionEnabled(true);
			if (mode == HORIZONTAL)
			{
				curTabMode = HORIZONTAL;
				_Button->setAnchorPoint(Vec2(0.5f,0));
				//item->setPosition(x, 0);
				_Button->setPosition(Vec2(i*tempImageView->getContentSize().width+x+tempImageView->getContentSize().width/2,0));
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				
				x += m_spaceWidth;

				auto imageView = ImageView::create();
				imageView->loadTexture((const char *)allImagePath[i]);
				imageView->setAnchorPoint(Vec2(0.5f,0));
				imageView->setPosition(Vec2(0,(_Button->getContentSize().height-imageView->getContentSize().height)/2));
				_Button->addChild(imageView);

			}
			else if (mode == VERTICAL)
			{
				curTabMode = VERTICAL;

				auto imageView = ImageView::create();
				imageView->loadTexture((const char *)allImagePath[i]);
				_Button->addChild(imageView);

				if (_Button->getContentSize().width>_Button->getContentSize().height)
				{
					_Button->setAnchorPoint(Vec2(0.5f,0.5f));
					_Button->setPosition(Vec2(tempImageView->getContentSize().width/2,-(i+1)*tempImageView->getContentSize().height+tempImageView->getContentSize().height/2-y));
					imageView->setAnchorPoint(Vec2(0.5f,0.5f));
					imageView->setPosition(Vec2(0,0));
				}
				else
				{
					_Button->setAnchorPoint(Vec2(0,0.5f));
					_Button->setPosition(Vec2(0,-(i+1)*tempImageView->getContentSize().height+tempImageView->getContentSize().height/2-y));	
					imageView->setAnchorPoint(Vec2(0,0.5f));
					imageView->setPosition(Vec2((_Button->getContentSize().width-imageView->getContentSize().width)/2,0));
				}
				
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				y += m_spaceHeight;
			}
			else if (mode == VERTICAL_LIST)
			{
				curTabMode = VERTICAL_LIST;
				_Button->setAnchorPoint(Vec2(0,1));
				_Button->setPosition(Vec2(0,0));
				int aabb = -i*tempImageView->getContentSize().height-y;
				CCLOG("%d",aabb);
				Vec2	position = Vec2(0,-i*tempImageView->getContentSize().height-y);
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				y += m_spaceHeight;

				auto imageView = ImageView::create();
				imageView->loadTexture((const char *)allImagePath[i]);
				imageView->setAnchorPoint(Vec2(0.5f,0.5f));
				imageView->setPosition(Vec2(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_Button->addChild(imageView);

				auto action = MoveTo::create(0.12+0.05*i,position);
				_Button->runAction(action);
			}

			//_Button->setTouchPriority(0);
			_Button->addTouchEventListener(CC_CALLBACK_2(UITab::onTouchEndedEvent,this));
			_Button->setTouchEnabled(true);

			this->addChild(_Button);
		}

		m_nMaxCount = maxcount;

		//this->setTouchEnabled(true);
		CCLOG("this.contenetSize = %d,%d",this->getContentSize().width,this->getContentSize().height);
		return true;
	}
	return false;
	
}




UITab * UITab::createWithText(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],TabMode mode,int space,int fontSize)
{
	auto tab = new UITab();
	if (tab && tab->initWithText(maxcount,normalImage,selectedImage,disabledImage,allTextNames,mode,space,fontSize))
	{
		//tab->autoreleqase();
		return tab;
	}
	CC_SAFE_DELETE(tab);
	return NULL;
}

bool UITab::initWithText(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],TabMode mode,int space,int fontSize)
{
	if (Widget::init())
	{
		this->normalImagePath= (char *)normalImage;
		if (mode == HORIZONTAL)
		{
			this->m_spaceWidth = space;
		}
		else if (mode == VERTICAL || mode == VERTICAL_LIST)
		{
			this->m_spaceHeight = space;
		}
		this->currentTabMod = mode;
		
		int x = 0;
		int y = 0;
		int aaa = maxcount;
		auto tempImageView = ImageView::create();
		tempImageView->loadTexture(normalImage);
		for (int i = 0;i<maxcount;++i)
		{
			//auto _Button = Button::create();
			auto _Button = Button::create();
			_Button->loadTextureNormal(normalImage);
			_Button->loadTexturePressed(selectedImage);
			_Button->loadTextureDisabled(disabledImage);
			//_Button->setText(allTextNames[i]);
			//_Button->setFontSize(25);
			_Button->setTag(TABBUTTONBASETAG+i);
			//_Button->setPressedActionEnabled(true);

			if (mode == HORIZONTAL)
			{
				curTabMode = HORIZONTAL;
				_Button->setAnchorPoint(Vec2(0.5f,0));
				//item->setPosition(x, 0);
				_Button->setPosition(Vec2(i*tempImageView->getContentSize().width+x+tempImageView->getContentSize().width/2,0));
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				
				x += m_spaceWidth;

				//创建label
				auto _label = Label::createWithTTF((const char *)allTextNames[i],APP_FONT_NAME, fontSize);
				_label->setContentSize(Size(tempImageView->getContentSize().width,0));
				_label->setAnchorPoint(Vec2(0.5f,0.5f));
				_label->setPosition(Vec2(0,_Button->getContentSize().height/2));
				_label->setName("Label_des");
				_Button->addChild(_label);
				//创建label
				/*Label* Label = Label::create();
				Label->setFntFile("fonts/mine.fnt");
				Label->setText((const char *)allTextNames[i]);
				Label->setAnchorPoint(Vec2(0.5f,0.5f));
				Label->setPosition(Vec2(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_Button->addChild(Label);*/
				/*auto imageView = ImageView::create();
				imageView->loadTexture((const char *)allTextNames[i]);
				imageView->setAnchorPoint(Vec2(0.5f,0.5f));
				imageView->setPosition(Vec2(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_Button->addChild(imageView);*/

			}
			else if (mode == VERTICAL)
			{
				curTabMode = VERTICAL;

				//创建label
				auto _label = Label::createWithTTF((const char *)allTextNames[i], APP_FONT_NAME, fontSize);
				if (tempImageView->getContentSize().width < tempImageView->getContentSize().height)
				{
					_label->setContentSize(Size(25,0));
					_label->setHorizontalAlignment(TextHAlignment::CENTER);
					_label->setVerticalAlignment(TextVAlignment::CENTER);
				}
				_label->setAnchorPoint(Vec2(0.5f,0.5f));
				_label->setPosition(Vec2(0,_Button->getContentSize().height/2));
				_label->setName("Label_des");
				_Button->addChild(_label);

				if (tempImageView->getContentSize().width > tempImageView->getContentSize().height)
				{
					_Button->setAnchorPoint(Vec2(0.5f,0.5f));
					_Button->setPosition(Vec2(tempImageView->getContentSize().width/2,-(i+1)*tempImageView->getContentSize().height+tempImageView->getContentSize().height/2-y));
					_label->setAnchorPoint(Vec2(0.5f,0.5f));
					_label->setPosition(Vec2(0,0));
				}
				else
				{
					_Button->setAnchorPoint(Vec2(0.f,0.5f));
					_Button->setPosition(Vec2(0,-(i+1)*tempImageView->getContentSize().height+tempImageView->getContentSize().height/2-y));	
					_label->setAnchorPoint(Vec2(0.5f,0.5f));
					_label->setPosition(Vec2(_Button->getContentSize().width/2,0));
				}
				
				//_Button->setAnchorPoint(Vec2(0.5f,0));
				//_Button->setPosition(Vec2(tempImageView->getContentSize().width/2,-(i+1)*tempImageView->getContentSize().height-y));

				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				y += m_spaceHeight;

			}
			else if (mode == VERTICAL_LIST)
			{
				curTabMode = VERTICAL_LIST;
				_Button->setAnchorPoint(Vec2(0,1));
				_Button->setPosition(Vec2(0,0));
				int aabb = -i*tempImageView->getContentSize().height-y;
				//CCLOG("%d",aabb);
				const int OFFSET_Y = 10;
				const int INTERVAL_Y = 3;
				Vec2 position = Vec2(0,-i * (tempImageView->getContentSize().height + INTERVAL_Y) - OFFSET_Y - y);
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				y += m_spaceHeight;

				//创建label
				auto _label = Label::createWithTTF((const char *)allTextNames[i], APP_FONT_NAME, fontSize);
				_label->setContentSize(Size(tempImageView->getContentSize().width,0));
				_label->setAnchorPoint(Vec2(0.5f,0.5f));
				_label->setPosition(Vec2(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_label->setName("Label_des");
				_Button->addChild(_label);
				/*
				//创建label
				Label* Label = Label::create();
				Label->setFntFile("res_ui/font/mine.fnt");
				Label->setText((const char *)a_allNames[i]);
				Label->setAnchorPoint(Vec2(0.5f,0.5f));
				Label->setPosition(Vec2(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_Button->addChild(Label);
				*/
				/*
				auto imageView = ImageView::create();
				imageView->loadTexture((const char *)allTextNames[i]);
				imageView->setAnchorPoint(Vec2(0.5f,0.5f));
				imageView->setPosition(Vec2(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_Button->addChild(imageView);*/

				// effect 1: droping
				//FiniteTimeAction*  action = Sequence::create(
				//	DelayTime::create((maxcount-i-1)*0.03f),
				//	MoveTo::create(0.03*i,position),
				//	NULL);
				// effect 2: scrolling
				auto action = MoveTo::create(0.05+0.03*i,position);
				_Button->runAction(action);
			}

			//_Button->setTouchPriority(0);
			_Button->addTouchEventListener(CC_CALLBACK_2(UITab::onTouchEndedEvent, this));
			_Button->setTouchEnabled(true);

			this->addChild(_Button);
		}
		
		CCLOG("this.contenetSize = %d,%d",this->getContentSize().width,this->getContentSize().height);
		m_nMaxCount = maxcount;
		////this->setTouchEnabled(true);
		return true;
	}
	return false;
	
}


UITab * UITab::createWithBMFont(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],char * fontName,TabMode mode,int space)
{
	auto tab = new UITab();
	if (tab && tab->initWithBMFont(maxcount,normalImage,selectedImage,disabledImage,allTextNames,fontName,mode,space))
	{
		tab->autorelease();
		return tab;
	}
	CC_SAFE_DELETE(tab);
	return NULL;
}

bool UITab::initWithBMFont(int maxcount,const char *normalImage, const char *selectedImage, const char *disabledImage,char * allTextNames[],char * fontName,TabMode mode,int space)
{
	if (Widget::init())
	{
		this->normalImagePath= (char *)normalImage;
		if (mode == HORIZONTAL)
		{
			this->m_spaceWidth = space;
		}
		else if (mode == VERTICAL || mode == VERTICAL_LIST)
		{
			this->m_spaceHeight = space;
		}
		this->currentTabMod = mode;
		
		int x = 0;
		int y = 0;
		int aaa = maxcount;
		auto tempImageView = ImageView::create();
		tempImageView->loadTexture(normalImage);
		for (int i = 0;i<maxcount;++i)
		{
			auto _Button = Button::create();
			_Button->loadTextureNormal(normalImage);
			_Button->loadTexturePressed(selectedImage);
			_Button->loadTextureDisabled(disabledImage);
			_Button->setTag(TABBUTTONBASETAG+i);
			//_Button->setPressedActionEnabled(true);
			if (mode == HORIZONTAL)
			{
				curTabMode = HORIZONTAL;
				_Button->setAnchorPoint(Vec2(0.5f,0));
				//item->setPosition(x, 0);
				_Button->setPosition(Vec2(i*tempImageView->getContentSize().width+x+tempImageView->getContentSize().width/2,0));
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				
				x += m_spaceWidth;
				
				//创建label
				auto Label = Label::createWithBMFont(fontName, (const char *)allTextNames[i]);
				Label->setAnchorPoint(Vec2(0.5f,0));
				Label->setPosition(Vec2(0,(_Button->getContentSize().height-Label->getContentSize().height)/2));
				_Button->addChild(Label);

			}
			else if (mode == VERTICAL)
			{
				curTabMode = VERTICAL;

				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				y += m_spaceHeight;
				
				//创建label
				auto Label = Label::createWithBMFont(fontName, (const char *)allTextNames[i]);
				_Button->addChild(Label);

				if (_Button->getContentSize().width>_Button->getContentSize().height)
				{
					_Button->setAnchorPoint(Vec2(0.5f,0.5f));
					_Button->setPosition(Vec2(tempImageView->getContentSize().width/2,-(i+1)*tempImageView->getContentSize().height+tempImageView->getContentSize().height/2-y));
					Label->setAnchorPoint(Vec2(0.5f,0.5f));
					Label->setPosition(Vec2(0,0));
				}
				else
				{
					_Button->setAnchorPoint(Vec2(0,0.5f));
					_Button->setPosition(Vec2(0,-(i+1)*tempImageView->getContentSize().height+tempImageView->getContentSize().height/2-y));	
					Label->setAnchorPoint(Vec2(0,0.5f));
					Label->setPosition(Vec2((_Button->getContentSize().width-Label->getContentSize().width)/2,0));
				}

			}
			else if (mode == VERTICAL_LIST)
			{
				curTabMode = VERTICAL_LIST;
				_Button->setAnchorPoint(Vec2(0,1));
				_Button->setPosition(Vec2(0,0));
				int aabb = -i*tempImageView->getContentSize().height-y;
				CCLOG("%d",aabb);
				Vec2	position = Vec2(0,-i*tempImageView->getContentSize().height-y);
				char s[5];
				//itoa(i,s,10);
				sprintf(s,"%d",i);
				_Button->setName(s);
				y += m_spaceHeight;
				
				//创建label
				auto Label = Label::createWithBMFont(fontName, (const char *)allTextNames[i]);
				Label->setAnchorPoint(Vec2(0.5f,0.5f));
				Label->setPosition(Vec2(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
				_Button->addChild(Label);
				
// 				auto imageView = ImageView::create();
// 				imageView->loadTexture((const char *)allImagePath[i]);
// 				imageView->setAnchorPoint(Vec2(0.5f,0.5f));
// 				imageView->setPosition(Vec2(_Button->getContentSize().width/2,-_Button->getContentSize().height/2));
// 				_Button->addChild(imageView);

				auto action = MoveTo::create(0.12+0.05*i,position);
				_Button->runAction(action);
			}

			//_Button->setTouchPriority(0);
			_Button->addTouchEventListener(CC_CALLBACK_2(UITab::onTouchEndedEvent, this));
			_Button->setTouchEnabled(true);

			this->addChild(_Button);
		}

		m_nMaxCount = maxcount;

		//this->setTouchEnabled(true);
		CCLOG("this.contenetSize = %d,%d",this->getContentSize().width,this->getContentSize().height);
		return true;
	}
	return false;
	
}



int UITab::getCurrentIndex()
{
	return this->m_currentIndex;
}

void UITab::setCurrentIndex(int tempIndex)
{
	this->m_currentIndex = tempIndex;
}

void UITab::setHighLightImage(char *s)
{
	this->highLightImagePath = s;
}

void UITab::setNormalImage(char *s)
{
	this->normalImagePath = s;
}

void UITab::setDefaultPanelByIndex(int index)
{
	this->m_currentIndex = index;
	changeToHeightLightByIndex(this->m_currentIndex);
}

void UITab::indexChangedEvent(Ref* pSender)
{
	//CCLOG("IndexChangedEvent");
	//MenuItemImage * itemImage = dynamic_cast<MenuItemImage*>(pSender);
	//int tagindex = itemImage->getTag();
	const char * index = ((Button*)pSender)->getName().c_str();
	int tagindex = atoi(index);
	if (this->m_currentIndex != tagindex)
	{
		setCurrentIndex(tagindex);
		changeToHeightLight((Button*)pSender);
	}

	if (m_pIndexListener && m_pfnIndexSelector)
    {
        (m_pIndexListener->*m_pfnIndexSelector)(this);
    }


}

void UITab::addIndexChangedEvent(Ref* pSender, SEL_IndexChangedEvent selector)
 {
	m_pIndexListener = pSender;
	m_pfnIndexSelector = selector;
 }

void UITab::changeToHeightLight(Ref* pSender)
{
	for (int i = 0;i<this->m_nMaxCount;i++)
	{
		char s[5];
		//itoa(i,s,10);
		sprintf(s,"%d",i);
		auto _tempButton = dynamic_cast<Button*>(this->getChildByName(s));
		_tempButton->loadTextureNormal(normalImagePath);
		_tempButton->loadTexturePressed(normalImagePath);
	}
	//高亮显示
	if (this->highLightImagePath)
	{
		auto _tempButton = dynamic_cast<Button*>(pSender);
		_tempButton->loadTextureNormal(highLightImagePath);
		_tempButton->loadTexturePressed(normalImagePath);
		(highLightImagePath);

		if (this->currentTabMod ==VERTICAL_LIST)
		{
			//this->removeFromParentAndCleanup(true);
		}
	}
	else
	{
	}

	//MenuItemImage * tempitemImage = dynamic_cast<MenuItemImage*>(this->_Menu->getChildByTag(100+this->getCurrentIndex()));
	//Label* label = dynamic_cast<Label*>(tempitemImage->getChildByTag(100+this->getCurrentIndex()));
	//label->setPosition(Vec2(tempitemImage->getContentSize().width/2,tempitemImage->getContentSize().height/2));
}

void UITab::changeToHeightLightByIndex(int index)
{
	for (int i = 0;i<this->m_nMaxCount;i++)
	{
		char s[5];
		//itoa(i,s,10);
		sprintf(s,"%d",i);
		auto _tempButton = dynamic_cast<Button*>(this->getChildByName(s));
		_tempButton->loadTextureNormal(normalImagePath);
		_tempButton->loadTexturePressed(normalImagePath);
	}
	//高亮显示
	if (this->highLightImagePath)
	{
		char s[5];
		//itoa(index,s,10);
		sprintf(s,"%d",index);
		auto _tempButton = dynamic_cast<Button*>(this->getChildByName(s));
		_tempButton->loadTextureNormal(highLightImagePath);
		_tempButton->loadTexturePressed(highLightImagePath);
	}
	else
	{
	}
}

void UITab::onTouchBeganEvent(Ref *pSender)
{
	
}
void UITab::onTouchMovedEvent(Ref *pSender)
{

}
void UITab::onTouchEndedEvent(Ref *pSender, Widget::TouchEventType type)
{	

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->indexChangedEvent(pSender);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

bool UITab::onTouchBegan( const Vec2 &touchPoint )
{
	//add by yang jun 2013.10.21
	return false;
}

void UITab::onTouchMoved( const Vec2 &touchPoint )
{

}

void UITab::onTouchEnded( const Vec2 &touchPoint )
{

}

void UITab::onTouchCancelled( const Vec2 &touchPoint )
{

}

void UITab::didNotSelectSelf()
{
	if (isAutoClose)
	{
		this->removeAllChildrenWithCleanup(false);
		//this->setVisible(false);
	}
}

void UITab::setAutoClose(bool _isAutoClose )
{
	this->isAutoClose = _isAutoClose;
}

void UITab::setPressedActionEnabled( bool visible )
{
	//__Array * childrenArray = this->getChildren()->data;
	cocos2d::Vector<Node*> childrenArray = this->getChildren();
	int length = childrenArray.size();
	for (int i = 0;i<length;++i)
	{
		auto child =dynamic_cast<Button*>(childrenArray.at(i));
		if (child != NULL)
		{
			child->setPressedActionEnabled(visible);
		}
	}
}

Button * UITab::getObjectByIndex( int index )
{
	auto button = (Button *)this->getChildByTag(TABBUTTONBASETAG+index);
	return button;
}

void UITab::setHightLightLabelColor( Color3B cl )
{
	for (int i = 0;i<m_nMaxCount;++i)
	{
		auto temp = (Button*)this->getObjectByIndex(i);
		auto temp_lb = dynamic_cast<Label*>(temp->getChildByName("Label_des"));
		if (temp_lb)
		{
			if (i == m_currentIndex)
			{
				temp_lb->setColor(cl);
			}
		}
	}
}

void UITab::setNormalLabelColor( Color3B cl )
{
	for (int i = 0;i<m_nMaxCount;++i)
	{
		auto temp = (Button*)this->getObjectByIndex(i);
		auto temp_lb = dynamic_cast<Label*>(temp->getChildByName("Label_des"));
		if (temp_lb)
		{
			if (i != m_currentIndex)
			{
				temp_lb->setColor(cl);
			}
		}
	}
}
