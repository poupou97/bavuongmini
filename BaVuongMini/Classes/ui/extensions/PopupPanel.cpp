#include "PopupPanel.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "RichTextInput.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "ui/CocosGUI.h"
#include "AppMacros.h"
#include "CCRichLabel.h"

using namespace cocos2d::ui;

PopupPanel::PopupPanel(void)
{
	reqOption_=-1;
	buttonSize = 0;
}


PopupPanel::~PopupPanel(void)
{
}

PopupPanel * PopupPanel::create(PopupBoardPara popupBoardPara,std::vector<PopuBoardOption *>optionvector)
{
	auto panel_ =new PopupPanel();
	if (panel_ && panel_->init(popupBoardPara,optionvector))
	{
		panel_->autorelease();
		return panel_;
	}
	CC_SAFE_DELETE(panel_);
	return NULL;
}

bool PopupPanel::init(PopupBoardPara popupBoardPara,std::vector<PopuBoardOption *>optionvector)
{
	if (UIScene::init())
	{
		//Size winsize =Director::getInstance()->getVisibleSize();
		Size winsize =Size(800,480);
		auto layer = Layer::create();
		layer->setIgnoreAnchorPointForPosition(false);
		layer->setAnchorPoint(Vec2(0.5f,0.5f));
		layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layer->setContentSize(winsize);
		addChild(layer);
		m_boardType =popupBoardPara.boardType_;

		auto backGroundSp = cocos2d::extension::Scale9Sprite::create("res_ui/dikuang_new.png");
		backGroundSp->setCapInsets(Rect(0,0,0,0));
		backGroundSp->setPreferredSize(Size(377,201));
		backGroundSp->setAnchorPoint(Vec2(0.5f,0.5f));
		backGroundSp->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layer->addChild(backGroundSp);

		auto backGroundSp_frame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_didia.png");
		backGroundSp->addChild(backGroundSp_frame);

		std::string showString = popupBoardPara.boardText_;
		auto labelLink=CCRichLabel::createWithString(showString.c_str(),Size(290,50),NULL,NULL,0,20,2);
		labelLink->setAnchorPoint(Vec2(0,1));
		labelLink->setPosition(Vec2(45,backGroundSp->getContentSize().height-30));
		backGroundSp->addChild(labelLink);

		m_countDown =popupBoardPara.boardTime_;
		buttonSize = optionvector.size();
		if (m_countDown > 0)
		{
			char countDownStr[10];
			sprintf(countDownStr,"%d",m_countDown);
			std::string timeBegin = "(";
			timeBegin.append(countDownStr);
			timeBegin.append(")");

			centerTimeLabel = Label::createWithTTF(timeBegin.c_str(), APP_FONT_NAME, 20);
			centerTimeLabel->setAnchorPoint(Vec2(0.5f,0.5f));
			centerTimeLabel->setPosition(Vec2(backGroundSp->getContentSize().width/2,backGroundSp->getContentSize().height/2 - 10));
			backGroundSp->addChild(centerTimeLabel);

			defaultId_ = popupBoardPara.boardDefaultid_;
			this->schedule(schedule_selector(PopupPanel::step),1.0f); 
		}

		if (m_boardType == TABLETBOARD)
		{
			auto inputboxSp = cocos2d::extension::Scale9Sprite::create("res_ui/InputBox30.png");
			inputboxSp->setPreferredSize(Size(246,35));
			inputboxSp->setAnchorPoint(Vec2(0,1));
			inputboxSp->setPosition(Vec2(49,backGroundSp->getContentSize().height-labelLink->getContentSize().height - 30));
			backGroundSp->addChild(inputboxSp);

			int inputMaxsize =popupBoardPara.boardMaxsize_;
			textBox_private=new RichTextInputBox();
			textBox_private->setCharLimit(inputMaxsize);
			textBox_private->setAnchorPoint(Vec2(0.5f,1));
			textBox_private->setPosition(Vec2(51,backGroundSp->getContentSize().height-labelLink->getContentSize().height-33 - 30));
			backGroundSp->addChild(textBox_private,10);
			textBox_private->autorelease();
		}

		for (int i=0;i<buttonSize;i++)
		{
			int selectid_ =optionvector.at(i)->id_;
			std::string string_title =optionvector.at(i)->title_;
			std::string string_msg = optionvector.at(i)->msg_;

			auto layerbutton = Layer::create();
			layerbutton->setIgnoreAnchorPointForPosition(false);
			layerbutton->setAnchorPoint(Vec2(0.5f,0.5f));
			layerbutton->setPosition(Vec2(winsize.width/2,winsize.height/2));
			layerbutton->setContentSize(winsize);
			layer->addChild(layerbutton);

			auto buttonSure= Button::create();
			buttonSure->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
			buttonSure->setTouchEnabled(true);
			buttonSure->setPressedActionEnabled(true);
			buttonSure->addTouchEventListener(CC_CALLBACK_2(PopupPanel::callBackSend, this));
			buttonSure->setAnchorPoint(Vec2(0.5f,0.5f));
			buttonSure->setScale9Enabled(true);
			buttonSure->setContentSize(Size(110,43));
			buttonSure->setCapInsets(Rect(18,9,2,23));
			if (optionvector.size()==1)
			{
				buttonSure->setPosition(Vec2(layer->getContentSize().width/2,layer->getContentSize().height/2 - backGroundSp->getContentSize().height/2 +48));
			}else
			{
				buttonSure->setPosition(Vec2(layer->getContentSize().width/2-85+ i* 170,layer->getContentSize().height/2 - backGroundSp->getContentSize().height/2 +48));
			}
			buttonSure->setTag(selectid_);
			layerbutton->addChild(buttonSure);

			auto btn_label= Label::createWithTTF(string_title.c_str(), APP_FONT_NAME,18);
			btn_label->setAnchorPoint(Vec2(0.5f,0.5f));
			btn_label->setPosition(Vec2(0,0));
			buttonSure->addChild(btn_label);
		}

		
		if (m_boardType == TEXTBOARD)
		{
			auto layerbutton = Layer::create();
			layerbutton->setIgnoreAnchorPointForPosition(false);
			layerbutton->setAnchorPoint(Vec2(0.5f,0.5f));
			layerbutton->setPosition(Vec2(winsize.width/2,winsize.height/2));
			layerbutton->setContentSize(winsize);
			layer->addChild(layerbutton);

			auto buttonClose_= Button::create();
			buttonClose_->setTouchEnabled(true);
			buttonClose_->setPressedActionEnabled(true);
			buttonClose_->loadTextures("res_ui/close.png","res_ui/close.png","");
			buttonClose_->setAnchorPoint(Vec2(0.5f,0.5f));
			buttonClose_->setPosition(Vec2(layer->getContentSize().width/2+ backGroundSp->getContentSize().width/2 - buttonClose_->getContentSize().width/2-5,
										355-buttonClose_->getContentSize().height/2 - 5));
			buttonClose_->addTouchEventListener(CC_CALLBACK_2(PopupPanel::callBackClose,this));
			layerbutton->addChild(buttonClose_);
		}
		
		if (buttonSize > 0)
		{
			backGroundSp_frame->setCapInsets(Rect(13,14,1,1));
			backGroundSp_frame->setPreferredSize(Size(300,108));
			backGroundSp_frame->setAnchorPoint(Vec2(0.5f,1.0f));
			backGroundSp_frame->setPosition(Vec2(backGroundSp->getContentSize().width/2-2,180));
		}else
		{
			backGroundSp_frame->setCapInsets(Rect(13,14,1,1));
			backGroundSp_frame->setPreferredSize(Size(304,160));
			backGroundSp_frame->setAnchorPoint(Vec2(0.5f,0.5f));
			backGroundSp_frame->setPosition(Vec2(backGroundSp->getContentSize().width/2-3,backGroundSp->getContentSize().height/2+5));
		}



		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}
void PopupPanel::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void PopupPanel::onExit()
{
	UIScene::onExit();
}

void PopupPanel::step( float dt )
{
	m_countDown--;
	char countDownStr_[10];
	sprintf(countDownStr_,"%d",m_countDown);

	std::string timeBegin = "(";
	timeBegin.append(countDownStr_);
	timeBegin.append(")");

	centerTimeLabel->setString(timeBegin.c_str());
	if (m_countDown==0)
	{
		this->callBackCoutDownDefault();
	}
}

void PopupPanel::selectedStateEvent( Ref *pSender, CheckBoxEventType type )
{
	auto checkBox_ =(CheckBox *)pSender;
	reqOption_ =checkBox_->isSelected();
}

void PopupPanel::callBackCoutDownDefault()
{
	//send 1128 default
	sendContent content_ ={defaultId_,"",reqOption_};
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1128,&content_);
	this->closeAnim();
}

void PopupPanel::callBackSend(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn_ = (Button *)pSender;
		reqSelectId_ = btn_->getTag();
		if (m_boardType == TABLETBOARD)
		{
			const char * contentStr_ = textBox_private->getInputString();
			std::string strings_ = "";
			if (contentStr_ != NULL)
			{
				strings_ = contentStr_;
			}
			sendContent content_ = { reqSelectId_,strings_,reqOption_ };
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1128, &content_);
			this->closeAnim();
		}
		else
		{
			sendContent content_ = { reqSelectId_,"",reqOption_ };
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1128, &content_);
			this->closeAnim();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void PopupPanel::callBackClose(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}




