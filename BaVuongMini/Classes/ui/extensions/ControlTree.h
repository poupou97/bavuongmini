#ifndef CONTROL_TREE_
#define CONTROL_TREE_

#include "../extensions/UIScene.h"
#include "ui/UIWidget.h"
#include "../extensions/UIButtonTree.h"

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class TreeStructInfo;

#define BUTTON_EXPAND_SHRINK_MARK "button_expand_shrink"  
#define BUTTON_NEWADDMARK "button_newAddMark"
#define BUTTON_SELECT_STATE "button_select_state"
#define SECOND_BUTTON_SELECT_STATE "second_btn_select_state"
#define SECOND_BUTTON_NEWADDMARK "second_button_newAddMark"

#define TREE_BUTTON_TAG 100
#define SECOND_PANEL_TAG 10000
#define SECOND_BUTTONTREE_TAG 1000000

using namespace cocos2d::ui;


class ControlTree:public UIScene
{
public:
	ControlTree(void);
	~ControlTree(void);

	static ControlTree * create(std::vector<TreeStructInfo *> treeVector,Size scrollSize,Size treeButtonSize,Size branchButtonSize);
	bool init(std::vector<TreeStructInfo *> treeVector,Size scrollSize,Size treeButtonSize,Size branchButtonSize);
	void onEnter();
	void onExit();

	void createSecondtree(int btnIndex,int num, bool isCreate);

	void callBackTreeCellEvent(Ref *pSender, Widget::TouchEventType type);
	void callBackSecondTreeCell(Ref *pSender, Widget::TouchEventType type);

	void setAllButtonSelectStateVisible();
	void setTreeSelectState(std::string imagePath);
	void setSecondTreeImage(std::string imagePath);

	void addcallBackTreeEvent(Ref* pSender, SEL_CallFuncO pSelector);
	void addcallBackSecondTreeEvent(Ref* pSender, SEL_CallFuncO pSelector);
	//get tree
	Button * getTreeButtonByIndex(int vectorIndex);
	void addMarkOnButton(std::string str,Vec2 p,int vectorIndex = -1);
	void removeMarkOnButton(int vectorIndex = -1);
	
	ButtonTree * getSecondTreeButtonByIndex(int vectorIndex);
	void addMarkOnSecondButton(std::string str,Vec2 p,int vectorIndex = -1);
	void removeMarkOnSecondButton(int vectorIndex = -1);

	//
	ButtonTree * getSecondTreeByPanelIndexOrSecondIndex(int panelIndex,int secondIndex);

	//
	void setShowSecondTreeCell(int vectorIndex);

	//get touch tree by index
	int getTreeIndex(int vectorIndex);
	//get touch tree 
	void setTouchTreeIndex(int index);
	int getTouchTreeIndex();

	void setSecondTreeIndex(int index);
	int getSecondTreeIndex();
	//
	void setSecondTreeByEveryPanelIndex(int panelIndex);
	int getSecondTreeByEveryPanelIndex();
	//
	void setButtonTreeSize(Size size_);
	Size getButtonTreeSize();

	void setBranchButtonSize(Size size_);
	Size getBranchButtonSize();
	//
	void removeSecondButton(int vectorIndex);

private:
	/*
	*��¼��һ�ε��ĸ�������࣬�����ж��ٴε��ʱ�Ƿ���Ҫ��¼
	*������ϴε��ĸ��࣬���ٴε��˸���ʱӦ���м�¼�ϴε������࣬
	*������ϴε��ĸ��࣬��Ĭ�ϵ��µĸ���ĵ�һ�����
	*/
	void setIsLastTree(bool isValue);
	bool getIsLastTree();
	bool m_isLastTree;
private:
	ui::ScrollView * scrollView_info;
	int tree_buttonNum;
	int tree_buttonHeight;

	int secondTree_buttonNum;
	int secondTree_buttonHeight;

	std::vector<TreeStructInfo * > m_treeVector; 

	std::string m_treeSelectState;//����ѡ��״̬
	std::string m_secondTreeImage;//�����ͼƬ��Դ
	std::string m_expandImage;//�չͼƬ��Դ
	std::string m_shrink;//����ͼƬ��Դ

	int m_curSelectIndex;

	int m_secondTreeOfPanelIndex;

	int m_touchTreeIndex;
	
	Size treeButton_size;
	Size branchButton_size;
private:
	Ref * senderListener_tree;
	SEL_CallFuncO senderSelector_tree;

	Ref * senderListener_branch;
	SEL_CallFuncO senderSelector_branch;
};


///////////////////////////////
class TreeStructInfo
{
public:
	TreeStructInfo(void);
	~TreeStructInfo(void);

	void setTreeName(std::string name);
	std::string getTreeName();

	void addSecondVector(std::string secondString);
	std::vector<std::string> getSecondVector();

private:
	int m_treeIndex_;
	std::string m_treeName_;
	std::vector<std::string> m_secondTree;
};
#endif;
