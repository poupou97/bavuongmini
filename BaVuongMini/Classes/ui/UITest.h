#ifndef _UIRICHANG_H_
#define _UIRICHANG_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class UITest : public Layer
{
public:
	UITest();
	~UITest();

public:
	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
    virtual void onTouchEnded(Touch *touch, Event * pEvent);
    virtual void onTouchCancelled(Touch *touch, Event * pEvent);
    virtual void onTouchMoved(Touch *touch, Event * pEvent);

    // a selector callback
    void menuCloseCallback(Ref* pSender);
	void menuButtonCallback(Ref* pSender);
	void menuSkillCallback(Ref* pSender);

};

#endif // _UIRICHANG_H_
