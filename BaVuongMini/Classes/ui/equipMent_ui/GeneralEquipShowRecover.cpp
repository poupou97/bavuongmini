#include "GeneralEquipShowRecover.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "EquipMentUi.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CEquipment.h"
#include "AppMacros.h"

GeneralEquipShowRecover::GeneralEquipShowRecover(void)
{
}


GeneralEquipShowRecover::~GeneralEquipShowRecover(void)
{
}

GeneralEquipShowRecover * GeneralEquipShowRecover::create( CEquipment * equipment,std::vector<CEquipment *>equipVector,long long generalId )
{
	auto generalEquip =new GeneralEquipShowRecover();
	if(generalEquip && generalEquip->init(equipment,equipVector,generalId))
	{
		generalEquip->autorelease();
		return generalEquip;
	}
	CC_SAFE_DELETE(generalEquip);
	return NULL;
}

bool GeneralEquipShowRecover::init( CEquipment * equipment ,std::vector<CEquipment *>equipVector,long long generalId)
{
	auto goods_ =new GoodsInfo();
	goods_->CopyFrom(equipment->goods());
	if (GoodsItemInfoBase::init(goods_,equipVector,generalId))
	{
		const char *str_recove = StringDataManager::getString("goods_auction_getback");
		//ȡ�ظ���ť
		auto Button_getAnnes= Button::create();
		Button_getAnnes->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_getAnnes->setTouchEnabled(true);
		Button_getAnnes->setPressedActionEnabled(true);
		Button_getAnnes->addTouchEventListener(CC_CALLBACK_2(GeneralEquipShowRecover::recoverGeneralEquipment, this));
		Button_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_getAnnes->setScale9Enabled(true);
		Button_getAnnes->setContentSize(Size(91,43));
		Button_getAnnes->setCapInsets(Rect(18,9,2,23));
		Button_getAnnes->setPosition(Vec2(135,30));

		auto Label_getAnnes = Label::createWithTTF(str_recove, APP_FONT_NAME, 18);
		Label_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_getAnnes->setPosition(Vec2(0,0));
		Button_getAnnes->addChild(Label_getAnnes);
		m_pLayer->addChild(Button_getAnnes);

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}

void GeneralEquipShowRecover::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GeneralEquipShowRecover::onExit()
{
	UIScene::onExit();
}

void GeneralEquipShowRecover::recoverGeneralEquipment(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button*)pSender;
		int tag = this->getTag();
		auto equip = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		equip->getBackGeneralMainEquip(tag);

		std::vector<CEquipment *>::iterator iter;
		for (iter = equip->curMainEquipvector.begin(); iter != equip->curMainEquipvector.end(); ++iter)
		{
			delete * iter;
		}
		equip->curMainEquipvector.clear();
		for (int i = 0; i<equip->generalEquipVector.size(); i++)
		{
			auto temp_ = new CEquipment();
			temp_->CopyFrom(*equip->generalEquipVector.at(i));
			equip->curMainEquipvector.push_back(temp_);
		}

		if (equip->refineMainEquip == false)
		{
			equip->generalMainId = equip->mes_petid;
		}

		equip->stoneOfAmountRefine = 0;
		this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}
