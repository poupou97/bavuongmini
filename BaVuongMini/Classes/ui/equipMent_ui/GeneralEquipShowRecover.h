#ifndef EQUIP_GENERALEQUIPRECOVERSHOW_H
#define EQUIP_GENERALEQUIPRECOVERSHOW_H
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CEquipment;

class GeneralEquipShowRecover:public GoodsItemInfoBase
{
public:
	GeneralEquipShowRecover(void);
	~GeneralEquipShowRecover(void);

	static GeneralEquipShowRecover *create(CEquipment * equipment,std::vector<CEquipment *>equipVector,long long generalId);
	bool init(CEquipment * equipment,std::vector<CEquipment *>equipVector,long long generalId);

	virtual void onEnter();
	virtual void onExit();

	void recoverGeneralEquipment(Ref *pSender, Widget::TouchEventType type);
};

#endif;