#ifndef EQUIP_GENERALEQUIPRECOVER_H
#define EQUIP_GENERALEQUIPRECOVER_H

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CEquipment;
class GeneralEquipRecover:public UIScene
{
public:
	GeneralEquipRecover(void);
	~GeneralEquipRecover(void);

	static GeneralEquipRecover *create(CEquipment * equipment,bool isMainequip);
	bool init(CEquipment * equipment,bool isMainequip);

	virtual void onEnter();
	virtual void onExit();

	void showEquipInfo(Ref *pSender, Widget::TouchEventType type);
	CEquipment * equip_;
	Button *Btn_pacItemFrame;
	ImageView * m_mengban_black;
	CEquipment * getEquipment();
public:
	int getEquipUseLevel();

	Label * labelStarlevel_;
	ImageView * imageStar_;

	Label * label_refineLv;
	void setStarLevel(int level_,bool isAction = false);
	int getStarLevel();
	void setRefineLevel(int level_,bool isAction = false);
	int getRefineLevel();
	int getQuality();
	int getClazz();
private:
	int m_useLevel;

	int m_starLevel;
	int m_refineLevel;

	int m_clazz;
	int m_quality;
};

#endif;