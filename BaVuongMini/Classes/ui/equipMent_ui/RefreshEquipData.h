#ifndef _EQUIPVALUEDATA_
#define _EQUIPVALUEDATA_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class FolderInfo;
class RefreshEquipData:public UIScene
{
public:
	RefreshEquipData(void);
	~RefreshEquipData(void);
	
	static RefreshEquipData * s_equipData;
	static RefreshEquipData * instance();

	void refreshRefineEquip();
	void setRefineEndValue(int refineLevel,bool isAction);

	void refreshStarEquip();
	void setStarEndValue(int starLevel, bool isAction);

	void refreshGem();

	void refeshBaptize();

	void getOperationGeneral();

	void assistRunAction(Vec2 p1,Vec2 p2,FolderInfo * floders,bool isAction);

	void addExpLabelAction(Node* sender, void* data);

	void assisAddExpAction(Node* sender, void* data);

	std::string getCurEquipSp(int quality_ );	
};
#endif;

