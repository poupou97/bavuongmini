
#ifndef _EQUIPMENT_GENERALEQUIPMENT_H
#define _EQUIPMENT_GENERALEQUIPMENT_H

#include "../extensions/UIScene.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GoodsInfo;
class EquipMentUi;
class CEquipment;
class GeneralEquipMent:public UIScene
{
public:
	GeneralEquipMent(void);
	~GeneralEquipMent(void);

	static GeneralEquipMent *create(CEquipment * equipment);
	bool init(CEquipment * equipment);
	
	void onEnter();
	void onExit();

	void SetCurgeneralEquipGray(bool ptouched);
	void showEquipMentInfo(Ref *pSender, Widget::TouchEventType type);


	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);
	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	//��ѧ
	int mTutorialScriptInstanceId;
	Button *Btn_pacItemFrame;
	ImageView *m_mengban_black;
	ImageView * m_select_state;
	CEquipment * generalEquipment;
};
///////////////////////////////////////////////////////////
class GeneralEquipMentInfo:public GoodsItemInfoBase
{
public:
	GeneralEquipMentInfo();
	~GeneralEquipMentInfo();

	static GeneralEquipMentInfo *create(CEquipment * equipment);
	bool init(CEquipment * equipment);

	virtual void onEnter();
	virtual void onExit();

	EquipMentUi * equipui;
	virtual bool onTouchBegan(Touch * pTouch,Event * pEvent);

	void mainEquipment(Ref *pSender, Widget::TouchEventType type);
	GoodsInfo * goods_;
	CEquipment * curEquip_;
	int equipPob;
public:
	//Ϊ�˽�ѧ��� ��ȡ��װ���İ�ť��λ�
	Button * btn_addMainEquip;

	//ý�ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	//��ѧ
	int mTutorialScriptInstanceId;

};

#endif;