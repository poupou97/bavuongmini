#include "EquipMentUi.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/UITab.h"
#include "GameView.h"
#include "../backpackscene/PacPageView.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/protobuf/EquipmentMessage.pb.h"
#include "StrengthEquip.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/CAdditionProperty.h"
#include "../../messageclient/element/CEquipHole.h"
#include "../extensions/Counter.h"
#include "EquipMentGeneralList.h"
#include "GeneralEquipMent.h"
#include "../../utils/StaticDataManager.h"
#include "GeneralEquipRecover.h"
#include "../goldstore_ui/GoldStoreUI.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../messageclient/element/CActiveRole.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../gamescene_state/role/GameActorAnimation.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "../../messageclient/element/MapStrngthStone.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../backpackscene/PackageItem.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/MapStrengthGem.h"
#include "../../messageclient/element/MapEquipAddExp.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../messageclient/element/CDrug.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"
#include "../../messageclient/element/MapRefineValueFactor.h"
#include "RefreshEquipData.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "SystheSisUI.h"
#include "../../messageclient/element/CSysthesisInfo.h"
#include "../../messageclient/element/MapStrengthUseStone.h"
#include "AppMacros.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../ui/extensions/ShowSystemInfo.h"
#include "cocostudio\CCProcessBase.h"


using namespace CocosDenshion;

EquipMentUi::EquipMentUi(void)
{
	currType=0;
	refineMainEquip=false;
	starMainEquip =false;
	gemMainEquip =false;
	baptizeMainEquip =false;
	
	mainEquipIsPac = false;
	generalEquipOfPart = -1;
	 
	firstAssist =false;
	secondtAssist=false;
	thirdAssist=false;
	fourAssist=false;
	fiveAssist =false;
	refineCost=0;
	mes_petid=0;
	curExValue=0;
	allExValue=0;
	
	stoneOfAmountStar =0;

	StrengthStone=false;
	baptizeCost=0;

	isRefreshPacGoods =false;
	tabviewSelectIndex = 0;

	stoneOfAmountRefine = 0;
	equipmentInstanceid = 0;
	generalMainId = 0;
	curSelectGeneralId = 0;

	m_isFirstOpenBackpac = true;
}


EquipMentUi::~EquipMentUi(void)
{
	int size = GameView::getInstance()->AllPacItem.size();
	this->removeAssistAndStone(operatorGetBack);

	for (int i =0;i<size;i++)
	{
		int curClazz = GameView::getInstance()->AllPacItem.at(i)->goods().clazz();
		if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
		{
			//设置背包 遍历所有格子恢复原始状态 
			GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(true);
			GameView::getInstance()->pacPageView->getPackageItem(i)->setBoundJewelPropertyVisible(false);
		}
	}
	assistVector.clear();
	assStoneVector.clear();
}

EquipMentUi * EquipMentUi::create()
{
	auto equip=new EquipMentUi();
	if (equip && equip->init())
	{
		equip->autorelease();
		return equip;
	}
	CC_SAFE_DELETE(equip);
	return NULL;
}

bool EquipMentUi::init()
{
	if (UIScene::init())
	{
		size= Director::getInstance()->getVisibleSize();
		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(size);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		equipPanel=LoadSceneLayer::equipMentPanel;
		equipPanel->setAnchorPoint(Vec2(0.5f,0.5f));
		equipPanel->setPosition(Vec2(size.width/2,size.height/2));
		m_pLayer->addChild(equipPanel);

		std::vector<CEquipment*>::iterator iter;
		for (iter=generalEquipVector.begin();iter!=generalEquipVector.end();iter++)
		{
			delete * iter;
		}
		generalEquipVector.clear();

		for (int i=0;i<GameView::getInstance()->EquipListItem.size();i++)
		{
			auto equip_ =new CEquipment();
			equip_->CopyFrom(*GameView::getInstance()->EquipListItem.at(i));
			generalEquipVector.push_back(equip_);
		}
		
		m_Layer= Layer::create();
		m_Layer->setIgnoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(Vec2(0.5f,0.5f));
		m_Layer->setPosition(Vec2(size.width/2,size.height/2));
		m_Layer->setContentSize(Size(800,480));
		this->addChild(m_Layer);
		
		const char * firstStr = StringDataManager::getString("UIName_zhuang");
		const char * secondStr = StringDataManager::getString("UIName_bei");
		const char * thirdStr = StringDataManager::getString("UIName_qiang");
		const char * fourthStr = StringDataManager::getString("UIName_hua");
		auto atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,fourthStr);
		atmature->setPosition(Vec2(30,240));
		m_Layer->addChild(atmature);
		
		roleLayer =Layer::create();
		roleLayer->setIgnoreAnchorPointForPosition(false);
		roleLayer->setAnchorPoint(Vec2(0.5f,0.5f));
		roleLayer->setContentSize(Size(800,480));
		roleLayer->setPosition(Vec2(size.width/2,size.height/2));
		roleLayer->setTag(EQUIPMENTROLEANIMATIONTAG);
		this->addChild(roleLayer,0,0);

		const char *str_refine = StringDataManager::getString("equip_refine");
		char *refine_left=const_cast<char*>(str_refine);	
		const char *str_star = StringDataManager::getString("equip_star");
		char *star_left=const_cast<char*>(str_star);	
		const char *str_gem = StringDataManager::getString("equip_gem");
		char *gem_left=const_cast<char*>(str_gem);	
		const char *str_silver = StringDataManager::getString("equip_silver");
		char *silver_left=const_cast<char*>(str_silver);	
		const char *str_systhesis = StringDataManager::getString("equip_systhesis");
		char *systhesis_left=const_cast<char*>(str_systhesis);	
		
		char *labelFont[]={refine_left,gem_left,star_left,silver_left,systhesis_left};
		//equipTab=UITab::createWithBMFont(5,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",labelFont,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
		equipTab=UITab::createWithText(5,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",labelFont,HORIZONTAL,5);
		equipTab->setAnchorPoint(Vec2(0.5f,0.5f));
		equipTab->setPosition(Vec2(70,413));
		equipTab->setHighLightImage("res_ui/tab_1_on.png");
		equipTab->setDefaultPanelByIndex(0);
		equipTab->setPressedActionEnabled(true);
		equipTab->addIndexChangedEvent(this,coco_indexchangedselector(EquipMentUi::callBackChangeTab));
		m_Layer->addChild(equipTab);
		//tab_beibao  zhuangbei/tab_renwu
		const char * normalImage = "res_ui/tab_b_off.png";
		const char * selectImage = "res_ui/tab_b.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_b.png";
		
		const char *str1_renwu = StringDataManager::getString("equip_uitab_renwu");
		char* p1_renwu =const_cast<char*>(str1_renwu);
		const char *str2_beibao = StringDataManager::getString("equip_uitab_beibao");
		char* p2_beibao =const_cast<char*>(str2_beibao);

		char * labelImage[] ={p1_renwu,p2_beibao};
		//char * labelImage[] ={"res_ui/zhuangbei/tab_renwu.png","res_ui/zhuangbei/tab_beibao.png"};
		//roleAndPack=UITab::createWithImage(2,normalImage,selectImage,"",labelImage,VERTICAL,5);
		roleAndPack = UITab::createWithText(2,normalImage,selectImage,finalImage,labelImage,VERTICAL,-5,18);
		roleAndPack->setAnchorPoint(Vec2(0.5f,0.5f));
		roleAndPack->setPosition(Vec2(757,400));
		roleAndPack->setHighLightImage("res_ui/tab_b.png");
		roleAndPack->setHightLightLabelColor(Color3B(47,93,13));
		roleAndPack->setNormalLabelColor(Color3B(255,255,255));
		roleAndPack->setDefaultPanelByIndex(0);
		roleAndPack->setPressedActionEnabled(true);
		roleAndPack->addIndexChangedEvent(this,coco_indexchangedselector(EquipMentUi::callBackPackAndRole));
		m_Layer->addChild(roleAndPack);

		refiningPanel=(Layout *)Helper::seekWidgetByName(equipPanel,"Panel_refining");
		starPanel=(Layout *)Helper::seekWidgetByName(equipPanel,"Panel_star");
		gemPanel=(Layout *)Helper::seekWidgetByName(equipPanel,"Panel_gem");
		baptizePanel=(Layout *)Helper::seekWidgetByName(equipPanel,"Panel_purification");
		synthesisPanel = (Layout*)Helper::seekWidgetByName(equipPanel,"Panel_systhesis");

		rolePanel =(Layout *)Helper::seekWidgetByName(equipPanel,"Panel_renwu");
		packPanel =(Layout *)Helper::seekWidgetByName(equipPanel,"Panel_beibao");

		refiningPanel->setVisible(true);
		starPanel->setVisible(false);
		gemPanel->setVisible(false);
		baptizePanel->setVisible(false);
		rolePanel->setVisible(true);
		packPanel->setVisible(false);
		synthesisPanel->setVisible(false);

		//pack / role
		image_rolePression = (ImageView *)Helper::seekWidgetByName(rolePanel,"Label_jobvalue");
		//ImageView * image_roleCountry = (ImageView *)Helper::seekWidgetByName(rolePanel,"ImageView_country");

		label_roleLevel = (Text*)Helper::seekWidgetByName(rolePanel,"Label_rolelv");
		label_roleFight = (Text*)Helper::seekWidgetByName(rolePanel,"Label_fp_value");
		//label_roleTotalFight = (Text*)Helper::seekWidgetByName(rolePanel,"Label_allfightpoint_value");

// 		this->refreshRoleInfo((CActiveRole*)GameView::getInstance()->myplayer->getActiveRole(),
// 								GameView::getInstance()->myplayer->player->fightpoint(),
// 								GameView::getInstance()->EquipListItem);

		//generalList
// 		generalList =EquipMentGeneralList::create();
// 		generalList->setIgnoreAnchorPointForPosition(false);
// 		generalList->setAnchorPoint(Vec2(0,0));
// 		generalList->setPosition(Vec2(375,36));
// 		generalList->setTag(20);
// 		m_Layer->addChild(generalList);
		
		long long currentTimePacpageView = GameUtils::millisecondNow();
		////backpac
		currentPage = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_dian_on");
		pageView = GameView::getInstance()->pacPageView;
		pageView->getPageView()->addEventListener((PageView::ccPageViewCallback)CC_CALLBACK_2(EquipMentUi::CurrentPageViewChanged, this));
		pageView->setPosition(Vec2(398,99));
		m_Layer->addChild(pageView,20);
		pageView->setCurUITag(ktagEquipMentUI);
		pageView->setVisible(false);
		auto seq = Sequence::create(DelayTime::create(0.05f),CallFunc::create(CC_CALLBACK_0(EquipMentUi::PageScrollToDefault,this)),NULL);
		pageView->runAction(seq);
		pageView->checkCDOnBegan();

		CCLOG("backPac Cd  cost time: %d", GameUtils::millisecondNow() - currentTimePacpageView);
		//refresh cd
		long long currentTimeBeginShort = GameUtils::millisecondNow();
		auto shortCutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortCutLayer)
		{
			for (int i = 0;i<7;++i)  //一共7个快捷栏
			{
				if (i < GameView::getInstance()->shortCutList.size())
				{
					auto c_shortcut = GameView::getInstance()->shortCutList.at(i);
					if (c_shortcut->storedtype() == 2)    //快捷栏中是物品
					{
						if(CDrug::isDrug(c_shortcut->skillpropid()))
						{
							if (shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100))
							{
								auto shortcutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100);
								this->pageView->RefreshCD(c_shortcut->skillpropid());
							}
						}
					}
				}
			}
		}
		CCLOG("refresh shurtlayer cost time: %d", GameUtils::millisecondNow() - currentTimeBeginShort);

		layerPackageFrame = Layer::create();
		layerPackageFrame->setIgnoreAnchorPointForPosition(false);
		layerPackageFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		layerPackageFrame->setPosition(Vec2(size.width/2,size.height/2));
		layerPackageFrame->setContentSize(Size(800,480));
		this->addChild(layerPackageFrame,25);
		layerPackageFrame->setVisible(false);

// 		ImageView * pageView_kuang = ImageView::create();
// 		pageView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setScale9Enabled(true);
// 		pageView_kuang->setContentSize(Size(363,321));
// 		pageView_kuang->setCapInsets(Rect(30,30,1,1));
// 		pageView_kuang->setAnchorPoint(Vec2(0,0));
// 		pageView_kuang->setLocalZOrder(10);
// 		pageView_kuang->setPosition(Vec2(390,90));
// 		layerPackageFrame->addChild(pageView_kuang);
		
		//refine
		long long currentTimeOfInitCreateUi = GameUtils::millisecondNow();
		this->initRefine();
		this->initStar();
		this->initGem();
		this->initBaptize();
		this->initSynthesis();
		CCLOG("initUi cost time: %d", GameUtils::millisecondNow() - currentTimeOfInitCreateUi);


		long long currentTimeOfRefreshState = GameUtils::millisecondNow();
		this->refreshCurState();
		CCLOG("refreshState cost time: %d", GameUtils::millisecondNow() - currentTimeOfRefreshState);
		//pac gold 
		label_ingot=(Text*)Helper::seekWidgetByName(equipPanel,"Label_ingot");
		label_ingot->setString("");

		label_gold=(Text*)Helper::seekWidgetByName(equipPanel,"Label_ingot_Clone");
		label_gold->setString("");

		label_goldBingding=(Text*)Helper::seekWidgetByName(equipPanel,"Label_bingdingYuanbao_num");
		label_goldBingding->setString("");

		this->refreshPlayerMoney();
		//
		image_arms = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_arms");
		image_clohes = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_clohes");
		image_ring = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_ring");
		image_helmet = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_helmet");
		image_shoes = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_shoes");
		image_accessories = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_accessories");
		/////
		btn_close=(Button *)Helper::seekWidgetByName(equipPanel,"Button_close");
		btn_close->setPressedActionEnabled(true);
		btn_close->setTouchEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackExit, this));

		auto btnShop=(Button *)Helper::seekWidgetByName(equipPanel,"Button_shop");
		btnShop->setPressedActionEnabled(true);
		btnShop->setScale(0.9f);
		btnShop->setTouchEnabled(true);
		btnShop->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackShop, this));

		////setTouchEnabled(true);
		//setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		setContentSize(size);
		return true;
	}
	return false;
}

void EquipMentUi::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	long long currentTimeBegin = GameUtils::millisecondNow();
	
	this->refreshRoleInfo((CActiveRole*)GameView::getInstance()->myplayer->getActiveRole(),
		GameView::getInstance()->myplayer->player->fightpoint(),
		GameView::getInstance()->EquipListItem);

	CCLOG("create roleInfo cost time: %d", GameUtils::millisecondNow() - currentTimeBegin);

	long long currentTimegeneralList = GameUtils::millisecondNow();
	generalList =EquipMentGeneralList::create();
	generalList->setIgnoreAnchorPointForPosition(false);
	generalList->setAnchorPoint(Vec2(0,0));
	generalList->setPosition(Vec2(400,42));
	generalList->setTag(20);
	m_Layer->addChild(generalList);

	CCLOG("create generalList cost time: %d", GameUtils::millisecondNow() - currentTimegeneralList);
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void EquipMentUi::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}
void EquipMentUi::callBackShop(Ref *pSender, Widget::TouchEventType type)
{	

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
		if (goldStoreUI == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			goldStoreUI = GoldStoreUI::create();
			GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI, 0, kTagGoldStoreUI);

			goldStoreUI->setIgnoreAnchorPointForPosition(false);
			goldStoreUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			goldStoreUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipMentUi::callBackChangeTab( Ref * obj )
{
	auto tab=(UITab *)obj;
	int num= tab->getCurrentIndex();
	if (num == currType)
	{
		return;
	}

	//open level
	int index_ = -1;
	switch(num)
	{
	case REFININGTYPE:
		{
			
		}break;
	case GEMTYPE:
		{
			index_ = 5;
		}break;
	case STARTYPE:
		{
			index_ = 25;
		}break;
	case BAPTIZETYPE:
		{
			index_ = 6;
		}break;
	case SYSTHESISTYPE:
		{
			
		}break;
	}

	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(index_);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[index_];
	}

	if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
	{
		std::string str_des = StringDataManager::getString("feature_will_be_open_function");
		char str_level[20];
		sprintf(str_level,"%d",openlevel);
		str_des.append(str_level);
		str_des.append(StringDataManager::getString("feature_will_be_open_open"));
		GameView::getInstance()->showAlertDialog(str_des.c_str());

		return;
	}
	
	currType = num;

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1699,this);
	if (refineMainEquip == true ||starMainEquip ==true || gemMainEquip ==true || baptizeMainEquip ==true)
	{
		if (mainEquipIsPac == true)
		{
			if (refineMainEquip==true)
			{
				this->getBackMainEquip(REFININGMAINEQUIP,mainEquipIndexRefine);
			}

			if (starMainEquip==true)
			{
				this->getBackMainEquip(STAREQUIP,mainEquipIndexRefine);
			}

			if (gemMainEquip==true)
			{
				this->getBackMainEquip(GEMEQUIP,mainEquipIndexRefine);
			}

			if (baptizeMainEquip ==true)
			{
				this->getBackMainEquip(BAPTIZEEQUIP,mainEquipIndexRefine);
			}
			this->addMainEquipment(GameView::getInstance()->AllPacItem.at(mainEquipIndexRefine));

		}else
		{
			if (refineMainEquip ==true)
			{
				this->getBackGeneralMainEquip(GENERALREFINEEQUIP);
			}
			if (starMainEquip ==true)
			{
				this->getBackGeneralMainEquip(GENERALSTAREQUIP);
			}
			if (gemMainEquip ==true)
			{
				this->getBackGeneralMainEquip(GENERALGEMEQUIP);
			}
			if (baptizeMainEquip ==true)
			{
				this->getBackGeneralMainEquip(GENERALBAPTIZEEQUIP);
			}
			//equip instanceid
			this->addMainEquipOfGeneral(getCurRoleEquipment(equipmentInstanceid));
		}
	}else
	{
		this->refreshBackPack(num,-1);
	}

	if (roleAndPack->getCurrentIndex() == 0 )
	{
		rolePanel->setVisible(true);
		packPanel->setVisible(false);
		pageView->setVisible(false);
		generalList->setVisible(true);
		roleLayer->setVisible(true);
		layerPackageFrame->setVisible(false);
		roleAndPack->setVisible(true);
	}else
	{
		rolePanel->setVisible(false);
		packPanel->setVisible(true);
		pageView->setVisible(true);
		generalList->setVisible(false);
		roleLayer->setVisible(false);
		layerPackageFrame->setVisible(true);
		roleAndPack->setVisible(true);
	}
	
	if (exPercent_refine->getActionByTag(1501))
	{
		auto action_ =(Action *)exPercent_refine->getActionByTag(1501);
		exPercent_refine->stopAllActions();
	}
	refreshCurState();
	switch (num)
	{
	case REFININGTYPE:
		{
			refiningPanel->setVisible(true);
			starPanel->setVisible(false);
			gemPanel->setVisible(false);
			baptizePanel->setVisible(false);
			synthesisPanel->setVisible(false);
			
			refineLayer->setVisible(true);
			exLabel_refine->setVisible(true);
			starPropertyLayer->setVisible(false);
			gemLayer->setVisible(false);
			baptizePropertylayer->setVisible(false);
			synthesisLayer->setVisible(false);
		}break;
	case STARTYPE:
		{
			auto button_ =(Button *)tab->getObjectByIndex(1);
			Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
				sc->endCommand(button_);

			refiningPanel->setVisible(false);
			starPanel->setVisible(true);
			gemPanel->setVisible(false);
			baptizePanel->setVisible(false);
			synthesisPanel->setVisible(false);

			refineLayer->setVisible(false);
			exLabel_refine->setVisible(false);
			starPropertyLayer->setVisible(true);
			gemLayer->setVisible(false);
			baptizePropertylayer->setVisible(false);
			synthesisLayer->setVisible(false);
		}break;
	case GEMTYPE:
		{
			refiningPanel->setVisible(false);
			starPanel->setVisible(false);
			gemPanel->setVisible(true);
			baptizePanel->setVisible(false);
			synthesisPanel->setVisible(false);

			refineLayer->setVisible(false);
			exLabel_refine->setVisible(false);
			starPropertyLayer->setVisible(false);
			gemLayer->setVisible(true);
			baptizePropertylayer->setVisible(false);
			synthesisLayer->setVisible(false);
		}break;
	case BAPTIZETYPE:
		{
			refiningPanel->setVisible(false);
			starPanel->setVisible(false);
			gemPanel->setVisible(false);
			baptizePanel->setVisible(true);
			synthesisPanel->setVisible(false);

			refineLayer->setVisible(false);
			exLabel_refine->setVisible(false);
			starPropertyLayer->setVisible(false);
			gemLayer->setVisible(false);
			baptizePropertylayer->setVisible(true);
			synthesisLayer->setVisible(false);
		}break;
	case SYSTHESISTYPE:
		{
			refiningPanel->setVisible(false);
			starPanel->setVisible(false);
			gemPanel->setVisible(false);
			baptizePanel->setVisible(false);
			synthesisPanel->setVisible(true);

			refineLayer->setVisible(false);
			exLabel_refine->setVisible(false);
			starPropertyLayer->setVisible(false);
			gemLayer->setVisible(false);
			baptizePropertylayer->setVisible(false);
			synthesisLayer->setVisible(true);

			rolePanel->setVisible(false);
			packPanel->setVisible(false);
			pageView->setVisible(false);
			generalList->setVisible(false);
			roleLayer->setVisible(false);
			layerPackageFrame->setVisible(false);
			roleAndPack->setVisible(false);

			auto systhesisiUI = (SysthesisUI*)synthesisLayer->getChildByTag(CLASS_SYSTHESIS_TAG);
			if (systhesisiUI)
			{
				systhesisiUI->refreshContrelTree();
			}
		}break;
	}
}

void EquipMentUi::callBackPackAndRole( Ref * obj )
{
	auto tab=(UITab *)obj;

	tab->setHightLightLabelColor(Color3B(47,93,13));
	tab->setNormalLabelColor(Color3B(255,255,255));

	int num= tab->getCurrentIndex();
	switch (num)
	{
	case 0:
		{
			rolePanel->setVisible(true);
			packPanel->setVisible(false);
			pageView->setVisible(false);
			generalList->setVisible(true);
			roleLayer->setVisible(true);
			layerPackageFrame->setVisible(false);
		}break;
	case 1:
		{
			rolePanel->setVisible(false);
			packPanel->setVisible(true);
			pageView->setVisible(true);
			generalList->setVisible(false);
			roleLayer->setVisible(false);
			layerPackageFrame->setVisible(true);
		}break;
	}
}

void EquipMentUi::refreshRoleInfo( CActiveRole * activeRole,int fight,std::vector<CEquipment *>equipmentsVector)
{
	roleLayer->removeAllChildren();
	std::vector<CEquipment*>::iterator iter;
	for (iter=generalEquipVector.begin();iter!=generalEquipVector.end();iter++)
	{
		delete * iter;
	}
	generalEquipVector.clear();

	image_arms->setVisible(true);
	image_clohes->setVisible(true);
	image_ring->setVisible(true);
	image_helmet->setVisible(true);
	image_shoes->setVisible(true);
	image_accessories->setVisible(true);

	std::string weapEffectStrForRefine="";
	std::string weapEffectStrForStar="";
	int equipWeapId = 0;
	bool isWeapEquip =false;
	int generalEquipNum = equipmentsVector.size();
	long long currentTimeBegin = GameUtils::millisecondNow();
	for (int i=0;i<generalEquipNum;i++)
	{
		int partIndex = equipmentsVector.at(i)->part();
		auto equip_ =new CEquipment();
		equip_->CopyFrom(*equipmentsVector.at(i));

		auto equip =GeneralEquipMent::create(equip_);
		equip->setIgnoreAnchorPointForPosition(false);
		equip->setAnchorPoint(Vec2(0,0));
		equip->setTag(GENERALEQUIPMENTLIST+i);
		roleLayer->addChild(equip);
		generalEquipVector.push_back(equip_);

		long long generalId_ = activeRole->rolebase().roleid();
		if (GameView::getInstance()->isOwn(generalId_))
		{
			generalId_ = 0;
		}
		
		if (refineMainEquip == true || starMainEquip == true || gemMainEquip == true ||baptizeMainEquip== true)
		{
			if (generalMainId == generalId_ && generalEquipOfPart == equip_->part())
			{
				equip->SetCurgeneralEquipGray(true);
			}
		}
		
		if (equip_->part()==4)
		{
			//获取当前武器的属性用于展示特效
			int pression_ = equip_->goods().equipmentdetail().profession();
			int refineLevel =  equip_->goods().equipmentdetail().gradelevel();
			int starLevel =  equip_->goods().equipmentdetail().starlevel();

			weapEffectStrForRefine = ActorUtils::getWeapEffectByRefineLevel(pression_,refineLevel);
			weapEffectStrForStar = ActorUtils::getWeapEffectByStarLevel(pression_, starLevel);

			GameView::getInstance()->myplayer->removeAllWeaponEffects();
			GameView::getInstance()->myplayer->addWeaponEffect(weapEffectStrForRefine.c_str());
			GameView::getInstance()->myplayer->addWeaponEffect(weapEffectStrForStar.c_str());
			generalWeapTag = equip->getTag();
			isWeapEquip =true;
		}
	}

	if (isWeapEquip ==false)
	{
		generalWeapTag = equipWeapId;
	}
	/*
	if (refineMainEquip ==false &&starMainEquip==false && gemMainEquip ==false &&baptizeMainEquip ==false )
	{
		std::vector<CEquipment *>::iterator iter;
		for (iter = curMainEquipvector.begin(); iter != curMainEquipvector.end();++iter)
		{
			delete * iter;
		}
		curMainEquipvector.clear();

		for (int i = 0;i<generalEquipVector.size();i++)
		{
			CEquipment * temp_ =new CEquipment();
			temp_->CopyFrom( *generalEquipVector.at(i));
			curMainEquipvector.push_back(temp_);
		}
		//generalMainId = mes_petid;
	}
	*/
	/*
	ParticleSystem* particleFire = ParticleSystemQuad::create("animation/texiao/particledesigner/huo2.plist");
	particleFire->setPositionType(ParticleSystem::PositionType::FREE);
	particleFire->setPosition(Vec2(561,160));
	particleFire->setAnchorPoint(Vec2(0.5f,0.5f));
	particleFire->setVisible(true);
	particleFire->setLifeVar(0);
	particleFire->setScale(0.5f);
	particleFire->setScaleX(0.6f);
	roleLayer->addChild(particleFire);

	ParticleSystem* particleFire1 = ParticleSystemQuad::create("animation/texiao/particledesigner/huo2.plist");
	particleFire1->setPositionType(ParticleSystem::PositionType::FREE);
	particleFire1->setPosition(Vec2(566,163));
	particleFire1->setAnchorPoint(Vec2(0.5f,0.5f));
	particleFire1->setVisible(true);
	//particleFire1->setLife(0.5f);
	particleFire1->setLifeVar(0);
	particleFire1->setScale(0.5f);
	particleFire1->setScaleX(0.6f);
	roleLayer->addChild(particleFire1);
	*/
	auto u_tempLayer = Layer::create();
	roleLayer->addChild(u_tempLayer);

	/*Label * l_allFPName = Label::create();
	l_allFPName->setText(StringDataManager::getString("rank_capacity"));
	l_allFPName->setFntFile("res_ui/font/ziti_1.fnt");
	l_allFPName->setAnchorPoint(Vec2(0.5f,0.5f));
	l_allFPName->setPosition(Vec2(185,167));
	l_allFPName->setScale(0.6f);
	u_tempLayer->addChild(l_allFPName);*/
	auto Image_allFPName = ImageView::create();
	Image_allFPName->loadTexture("res_ui/renwubeibao/zi2.png");
	Image_allFPName->setAnchorPoint(Vec2(0.5f,0.5f));
	Image_allFPName->setPosition(Vec2(516,166));
	Image_allFPName->setScale(1.0f);
	u_tempLayer->addChild(Image_allFPName);

	label_roleTotalFight = Label::createWithBMFont("res_ui/font/ziti_1.fnt", "123456789");
	label_roleTotalFight->setAnchorPoint(Vec2(0.5f,0.5f));
	label_roleTotalFight->setPosition(Vec2(605,165));
	label_roleTotalFight->setScale(0.6f);
	u_tempLayer->addChild(label_roleTotalFight);

	auto l_generalName = CCRichLabel::createWithString(activeRole->rolebase().name().c_str(), Size(390,38),NULL,NULL);
	l_generalName->setAnchorPoint(Vec2(0.5f,0.5f));
	l_generalName->setPosition(Vec2(570,383));
	roleLayer->addChild(l_generalName);

	int bipLevel_ = activeRole->playerbaseinfo().viplevel();
	if (bipLevel_ > 0)
	{
		auto vipNode_ = MainScene::addVipInfoByLevelForNode(bipLevel_);
		roleLayer->addChild(vipNode_);
		vipNode_->setPosition(Vec2(l_generalName->getPositionX()+l_generalName->getContentSize().width/2+ vipNode_->getContentSize().width/2,
									l_generalName->getPositionY()));
	}

	int countryId_  = activeRole->playerbaseinfo().country();
	if (countryId_ > 0 && countryId_ <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId_);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		auto countrySp_ = Sprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(Vec2(1,0.5f));
		countrySp_->setPosition(Vec2(l_generalName->getPositionX()- l_generalName->getContentSize().width/2,l_generalName->getPositionY()));
		countrySp_->setScale(0.7f);
		roleLayer->addChild(countrySp_);
	}

	std::string professionPath_ = BasePlayer::getProfessionIconByStr(activeRole->profession().c_str());
	image_rolePression->loadTexture(professionPath_.c_str());
	
	char levelStr[20];
	sprintf(levelStr,"%d",activeRole->level());

	std::string stringLevel_ = "LV.";
	stringLevel_.append(levelStr);
	label_roleLevel->setString(stringLevel_.c_str());
	
	int fightVale = fight;
	char fightStr[20];
	sprintf(fightStr,"%d",fightVale);
	label_roleFight->setString(fightStr);

	int allFightPoint = GameView::getInstance()->myplayer->player->fightpoint();
	for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();++i)
	{
		allFightPoint += GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint();
	}
	char allFightStr[20];
	sprintf(allFightStr,"%d",allFightPoint);
	label_roleTotalFight->setString(allFightStr);

	std::string roleFigureName = ActorUtils::getActorFigureName(*activeRole);
	auto pAnim = ActorUtils::createActorAnimation(roleFigureName.c_str(), activeRole->hand().c_str(),
		weapEffectStrForRefine.c_str(), weapEffectStrForStar.c_str());
	pAnim->setPosition(Vec2(570,225));
	pAnim->setTag(ROLEANMITIONTAG);
	roleLayer->addChild(pAnim);
}

void EquipMentUi::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void EquipMentUi::refreshPlayerMoney()
{
	int m_goldValue = GameView::getInstance()->getPlayerGold();
	char GoldStr_[20];
	sprintf(GoldStr_,"%d",m_goldValue);
	label_gold->setString(GoldStr_);

	int m_goldIngot =  GameView::getInstance()->getPlayerGoldIngot();
	char GoldIngotStr_[20];
	sprintf(GoldIngotStr_,"%d",m_goldIngot);
	label_ingot->setString(GoldIngotStr_);

	int m_BingdinggoldIngot =  GameView::getInstance()->getPlayerBindGoldIngot();
	char bingdingGoldIngotStr_[20];
	sprintf(bingdingGoldIngotStr_,"%d",m_BingdinggoldIngot);
	label_goldBingding->setString(bingdingGoldIngotStr_);
}

void EquipMentUi::callBackClearUp( Ref * obj )
{
	if (refineMainEquip==true||starMainEquip ==true || gemMainEquip == true||baptizeMainEquip==true )
	{
		const char *str = StringDataManager::getString("equip_lockState");
		GameView::getInstance()->showAlertDialog(str);
	}else
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1308, (void *)1);
		isRefreshPacGoods = true;
	}
}

void EquipMentUi::addMainEquipment( FolderInfo * floders )
{
	switch(currType)
	{
	case REFININGTYPE:
		{
			if (refineMainEquip==true)
			{
				if (mainEquipIsPac == true)
				{
					this->getBackMainEquip(REFININGMAINEQUIP,mainEquipIndexRefine);
				}else
				{
					this->getBackGeneralMainEquip(GENERALREFINEEQUIP);
				}
			}

			if (floders->goods().equipmentdetail().gradelevel()<20)
			{
				mainEquipIsPac =true;
				mainEquipIndexRefine = floders->id();
				mes_source = 2;
				mes_index = mainEquipIndexRefine;
				GameView::getInstance()->pacPageView->SetCurFolderGray(true,mainEquipIndexRefine);
				assistEquipStruct assists_={2,mes_assistEquipPob,mes_petid,mainEquipIndexRefine,0};
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1601,&assists_);
			}else
			{
				const char *string_ = StringDataManager::getString("refineLevelIsFull");
				GameView::getInstance()->showAlertDialog(string_);
			}
		}break;
	case STARTYPE:
		{
			if (starMainEquip==true)
			{
				if (mainEquipIsPac == true)
				{
					this->getBackMainEquip(STAREQUIP,mainEquipIndexRefine);
				}else
				{
					this->getBackGeneralMainEquip(GENERALSTAREQUIP);
				}
			}

			if (floders->goods().equipmentdetail().starlevel()<20)
			{
				mainEquipIsPac =true;

				mainEquipIndexRefine = floders->id();
				mes_source = 2;
				mes_index = mainEquipIndexRefine;
				GameView::getInstance()->pacPageView->SetCurFolderGray(true,mainEquipIndexRefine);
				assistEquipStruct assists_={2,mes_assistEquipPob,mes_petid,mainEquipIndexRefine,1};
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1601,&assists_);
			}else
			{
				const char *string_ = StringDataManager::getString("starLevelIsFull");
				GameView::getInstance()->showAlertDialog(string_);
			}
		}break;
	case GEMTYPE:
		{
			if (gemMainEquip==true)
			{
				if (mainEquipIsPac == true)
				{
					this->getBackMainEquip(GEMEQUIP,mainEquipIndexRefine);
				}else
				{
					this->getBackGeneralMainEquip(GENERALGEMEQUIP);
				}
			}
			mainEquipIsPac =true;
			mainEquipIndexRefine = floders->id();
			mes_source = 2;
			mes_index = mainEquipIndexRefine;
			GameView::getInstance()->pacPageView->SetCurFolderGray(true,mainEquipIndexRefine);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1621,this);
		}break;
	case BAPTIZETYPE:
		{	
			auto goods = new GoodsInfo();
			goods->CopyFrom(floders->goods());
			if (this->isCanBaptize(goods))
			{
				if (baptizeMainEquip ==true)
				{

					if (gemMainEquip==true)
					{
						if (mainEquipIsPac == true)
						{
							this->getBackMainEquip(BAPTIZEEQUIP,mainEquipIndexRefine);
						}else
						{
							this->getBackGeneralMainEquip(GENERALBAPTIZEEQUIP);
						}
					}
				}
				//标记当前选中的背包的物品
				mainEquipIsPac =true;
				
				mainEquipIndexRefine = floders->id();
				mes_source = 2;
				mes_index = mainEquipIndexRefine;
				GameView::getInstance()->pacPageView->SetCurFolderGray(true,mainEquipIndexRefine);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1610,this);
			}else
			{
				const char *string_ = StringDataManager::getString("baptizeIsNotActive");
				GameView::getInstance()->showAlertDialog(string_);
			}
			delete goods;
		}break;
	case SYSTHESISTYPE:
		{	
			if (floders->has_goods())
			{
				auto systhesisiUI = (SysthesisUI*)synthesisLayer->getChildByTag(CLASS_SYSTHESIS_TAG);
				if (systhesisiUI)
				{
					for (int i = 0;i<SysthesisItemConfig::s_systhesisInfo.size();++i)
					{
						CSysthesisInfo * temp = SysthesisItemConfig::s_systhesisInfo.at(i);
						if (floders->goods().id() == temp->get_merge_prop_id())
						{
							systhesisiUI->setCurSysthesisInfo(temp);
							systhesisiUI->selectCellBySysthesisInfo(temp);
							systhesisiUI->refreshUI(temp);
						}
					}
				}
			}
		}break;
	}
}


void EquipMentUi::addMainEquipOfGeneral(CEquipment * equips)
{
	switch(currType)
	{
	case REFININGTYPE:
		{
			if (refineMainEquip ==true)
			{
				if (mainEquipIsPac == true)
				{
					this->getBackMainEquip(REFININGMAINEQUIP,mainEquipIndexRefine);
				}else
				{
					this->getBackGeneralMainEquip(GENERALREFINEEQUIP);
				}
			}
			if (equips->goods().equipmentdetail().gradelevel() < 20)
			{	
				//标记当前主装备是否是背包
				mainEquipIsPac = false;
				mes_source=1;
				generalEquipOfPart = equips->part();
				assistEquipStruct assists_={1,generalEquipOfPart,generalMainId,mes_index,0};
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1601,&assists_);
			}else
			{
				const char *string_ = StringDataManager::getString("refineLevelIsFull");
				GameView::getInstance()->showAlertDialog(string_);
			}
		}break;
	case STARTYPE:
		{
			if (starMainEquip ==true)
			{
				if (mainEquipIsPac == true)
				{
					this->getBackMainEquip(STAREQUIP,mainEquipIndexRefine);
				}else
				{
					this->getBackGeneralMainEquip(GENERALSTAREQUIP);
				}
			}
			if (equips->goods().equipmentdetail().starlevel() < 20)
			{
				//标记当前主装备是否是背包
				mainEquipIsPac = false;
				mes_source=1;
				generalEquipOfPart = equips->part();
				assistEquipStruct assists_={1,generalEquipOfPart,generalMainId,mes_index,1};
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1601,&assists_);
			}else
			{
				const char *string_ = StringDataManager::getString("starLevelIsFull");
				GameView::getInstance()->showAlertDialog(string_);
			}
		}break;
	case GEMTYPE:
		{
			if (gemMainEquip ==true)
			{
				if (mainEquipIsPac == true)
				{
					this->getBackMainEquip(GEMEQUIP,mainEquipIndexRefine);
				}else
				{
					this->getBackGeneralMainEquip(GENERALGEMEQUIP);
				}
			}
			
			//标记当前主装备是否是背包
			mainEquipIsPac = false;
			mes_source=1;
			generalEquipOfPart = equips->part();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1621,this);
		}break;
	case BAPTIZETYPE:
		{
			GoodsInfo * goods = new GoodsInfo();
			goods->CopyFrom(equips->goods());
			if (this->isCanBaptize(goods))
			{
				if (baptizeMainEquip ==true)
				{
					if (mainEquipIsPac == true)
					{
						this->getBackMainEquip(BAPTIZEEQUIP,mainEquipIndexRefine);
					}else
					{
						this->getBackGeneralMainEquip(GENERALBAPTIZEEQUIP);
					}
				}
				
				//标记当前主装备是否是背包
				mainEquipIsPac = false;
				mes_source=1;
				generalEquipOfPart = equips->part();
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1610,this);
			}else
			{
				const char *string_ = StringDataManager::getString("equip_isBaptizeOffalse");
				GameView::getInstance()->showAlertDialog(string_);
			}
		}break;
	}
}

void EquipMentUi::getBackGeneralMainEquip(int layerTag )
{
	GeneralEquipRecover * generalEquip;
	switch(layerTag)
	{
	case GENERALREFINEEQUIP:
		{
			generalEquip = (GeneralEquipRecover *)refineLayer->getChildByTag(layerTag);
		}break;
	case GENERALSTAREQUIP:
		{
			generalEquip = (GeneralEquipRecover *)starPropertyLayer->getChildByTag(layerTag);
		}break;
	case GENERALGEMEQUIP:
		{
			generalEquip = (GeneralEquipRecover *)gemLayer->getChildByTag(layerTag);
		}break;
	case GENERALBAPTIZEEQUIP:
		{
			generalEquip = (GeneralEquipRecover *)baptizePropertylayer->getChildByTag(layerTag);
		}break;
	}


	if (layerTag ==GENERALREFINEEQUIP || layerTag ==GENERALSTAREQUIP ||layerTag == GENERALGEMEQUIP || layerTag == GENERALBAPTIZEEQUIP )
	{
		refineMainEquip =false;
		starMainEquip =false;
		gemMainEquip =false;
		baptizeMainEquip =false;

		refreshCurState();

		for (int j=0;j<generalEquipVector.size();j++)
		{
			auto generalEquip_ = (GeneralEquipMent*)roleLayer->getChildByTag(GENERALEQUIPMENTLIST + j);
			if (generalEquip->getEquipment()->part() == generalEquip_->Btn_pacItemFrame->getTag())
			{
				generalEquip_->SetCurgeneralEquipGray(false);
			}
		}
	
		for (int i=0;i<assistVector.size();i++)
		{
			if (assistVector.at(i).source_ == 2)
			{
				GameView::getInstance()->pacPageView->SetCurFolderGray(false,assistVector.at(i).index_);
			}
		}

		for (int i=0;i<assStoneVector.size();i++)
		{
			if (assStoneVector.at(i).source_ == 2)
			{
				GameView::getInstance()->pacPageView->SetCurFolderGray(false,assStoneVector.at(i).index_);
			}
		}

		for (int i=21;i<26;i++)
		{
			auto btn_refining_stone= (StrengthEquip *)refineLayer->getChildByTag(i);
			if (btn_refining_stone != NULL)
			{
				this->getBackMainEquip(i,btn_refining_stone->packIndex);
			}
		}

		generalEquip->removeFromParentAndCleanup(true);

		this->refreshEquipProperty(0,0,false);
		this->refreshEquipProperty_star(1,0,0);
		this->refreshGemValue(true);
		this->refreshEquipProperty_baptize(0);

		this->refreshBackPack(currType,-1);

		//start
		StrengthStone = false;

		firstAssist=false;
		secondtAssist =false;
		thirdAssist=false;
		fourAssist =false;
		fiveAssist = false;
		assistVector.clear();
		assStoneVector.clear();
		for (int j=0;j<generalEquipVector.size();j++)
		{
			auto generalEquip = (GeneralEquipMent*)roleLayer->getChildByTag(GENERALEQUIPMENTLIST + j);
			generalEquip->SetCurgeneralEquipGray(false);
		}
		return;
	}
	
	generalEquip= (GeneralEquipRecover *)refineLayer->getChildByTag(layerTag);
	for (int i=0;i<assistVector.size();i++)
	{
		if (generalEquip->getEquipment()->part() == assistVector.at(i).pob_)
		{
			assistVector.erase(assistVector.begin()+i);
		}
	}

	for (int j=0;j<generalEquipVector.size();j++)
	{
		auto generalEquips = (GeneralEquipMent*)roleLayer->getChildByTag(GENERALEQUIPMENTLIST + j);
		if (generalEquip->getEquipment()->part() == generalEquips->Btn_pacItemFrame->getTag())
		{
			generalEquips->SetCurgeneralEquipGray(false);
		}
	}

	int fightCapacity = generalEquip->getEquipment()->goods().equipmentdetail().fightcapacity();
	refineCost-= fightCapacity/50+1;
	this->refreshExpValue(-(fightCapacity/5+1),false);
	generalEquip->removeFromParentAndCleanup(true);
	switch (layerTag)
	{
	case GENERALFIRSTASSIST:
		{
			firstAssist=false;
		}break;
	case GENERALSECONDASSIST:
		{
			secondtAssist=false;
		}break;
	case GENERALTHIRDASSIST:
		{
			thirdAssist=false;
		}break;
	case GENERALFOURASSIST:
		{
			fourAssist=false;
		}break;
	case GENERALFIVEASSIST:
		{
			fiveAssist =false;
		}break;
	}
}


void EquipMentUi::getBackMainEquip( int tag,int index)
{
	if (tag == REFININGMAINEQUIP || tag ==STAREQUIP || tag==GEMEQUIP || tag== BAPTIZEEQUIP)
	{
		refineMainEquip=false;
		starMainEquip =false;
		gemMainEquip =false;
		baptizeMainEquip =false;

		refreshCurState();

		refineLayer->removeAllChildren();
		this->refreshEquipProperty(0,0,false);

		starPropertyLayer->removeAllChildren();
		this->refreshEquipProperty_star(1,0,0);

		gemLayer->removeAllChildren();
		this->refreshGemValue(true);

		baptizePropertylayer->removeAllChildren();
		this->refreshEquipProperty_baptize(0);

		if (GameView::getInstance()->AllPacItem.at(index)->has_goods())
		{
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,index);
		}

		for (int i=0;i<assistVector.size();i++)
		{
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,assistVector.at(i).index_);
		}
		for (int i=0;i<assStoneVector.size();i++)
		{
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,assStoneVector.at(i).index_);
		}
		firstAssist =false;
		secondtAssist =false;
		thirdAssist =false;
		fourAssist =false;
		fiveAssist =false;
		assistVector.clear();
		assStoneVector.clear();

		StrengthStone = false;

		this->refreshBackPack(currType,-1);
		return;
	}
	int temp_amount = 0;
	for (int i=0;i<assStoneVector.size();i++)
	{
		if (index == assStoneVector.at(i).index_)
		{
			temp_amount = assStoneVector.at(i).amount_;
		}
	}

	switch(tag)
	{
	case FRISTASSIST:
		{
			firstAssist=false;
			auto strongFirst = (StrengthEquip *)refineLayer->getChildByTag(FRISTASSIST);
			strongFirst->removeFromParentAndCleanup(true);
			
			if (!GameView::getInstance()->AllPacItem.at(index)->has_goods())
			{
				return;
			}
			if (GameView::getInstance()->AllPacItem.at(index)->goods().equipmentclazz()>0 &&GameView::getInstance()->AllPacItem.at(index)->goods().equipmentclazz()<10 )
			{
				for (int i=0;i<assistVector.size();i++)
				{
					if (index == assistVector.at(i).index_)
					{
						assistVector.erase(assistVector.begin()+i);
					}
				}
				int fightCapacity = GameView::getInstance()->AllPacItem.at(index)->goods().equipmentdetail().fightcapacity();
				refineCost-= fightCapacity/50+1;
				this->refreshExpValue(-(fightCapacity/5+1),false);
			}else
			{
				auto mapStone = StrengthStoneConfigData::s_StrengthStrone[GameView::getInstance()->AllPacItem.at(index)->goods().id()];
				int addEx =  mapStone->get_exp();
				this->refreshExpValue(-addEx*temp_amount,false);

				for (int i=0;i<assStoneVector.size();i++)
				{
					if (index == assStoneVector.at(i).index_)
					{
						assStoneVector.erase(assStoneVector.begin()+i);
					}
				}
			}
			
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,index);
		}break;
	case SECONDASSIST:
		{
			secondtAssist=false;
			auto strongSecond = (StrengthEquip *)refineLayer->getChildByTag(SECONDASSIST);
			strongSecond->removeFromParentAndCleanup(true);

			if (!GameView::getInstance()->AllPacItem.at(index)->has_goods())
			{
				return;
			}
			if (GameView::getInstance()->AllPacItem.at(index)->goods().equipmentclazz()>0 &&GameView::getInstance()->AllPacItem.at(index)->goods().equipmentclazz()<10 )
			{
				for (int i=0;i<assistVector.size();i++)
				{
					if (index == assistVector.at(i).index_)
					{
						assistVector.erase(assistVector.begin()+i);
					}
				}

				int fightCapacity = GameView::getInstance()->AllPacItem.at(index)->goods().equipmentdetail().fightcapacity();
				refineCost-= fightCapacity/50+1;
				this->refreshExpValue(-(fightCapacity/5+1),false);
			}else
			{
			
				auto mapStone = StrengthStoneConfigData::s_StrengthStrone[GameView::getInstance()->AllPacItem.at(index)->goods().id()];
				int addEx =  mapStone->get_exp();
				this->refreshExpValue(-addEx*temp_amount,false);

				for (int i=0;i<assStoneVector.size();i++)
				{
					if (index == assStoneVector.at(i).index_)
					{
						assStoneVector.erase(assStoneVector.begin()+i);
					}
				}
			}
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,index);
		}break;
	case THIRDASSIST:
		{
			thirdAssist=false;
			auto strongThird = (StrengthEquip *)refineLayer->getChildByTag(THIRDASSIST);
			strongThird->removeFromParentAndCleanup(true);
			if (!GameView::getInstance()->AllPacItem.at(index)->has_goods())
			{
				return;
			}
			if (GameView::getInstance()->AllPacItem.at(index)->goods().equipmentclazz()>0 &&GameView::getInstance()->AllPacItem.at(index)->goods().equipmentclazz()<10 )
			{
				for (int i=0;i<assistVector.size();i++)
				{
					if (index == assistVector.at(i).index_)
					{
						assistVector.erase(assistVector.begin()+i);
					}
				}

				int fightCapacity =GameView::getInstance()->AllPacItem.at(index)->goods().equipmentdetail().fightcapacity();
				refineCost-= fightCapacity/50+1;
				this->refreshExpValue(-(fightCapacity/5+1),false);
			}else
			{
				for (int i=0;i<assStoneVector.size();i++)
				{
					if (index == assStoneVector.at(i).index_)
					{
						assStoneVector.erase(assStoneVector.begin()+i);
					}
				}

				auto mapStone = StrengthStoneConfigData::s_StrengthStrone[GameView::getInstance()->AllPacItem.at(index)->goods().id()];
				int addEx =  mapStone->get_exp();
				this->refreshExpValue(-addEx*temp_amount,false);
			}
			
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,index);
		}break;
	case FOURASSIST:
		{
			fourAssist=false;
			auto strongFour = (StrengthEquip *)refineLayer->getChildByTag(FOURASSIST);
			strongFour->removeFromParentAndCleanup(true);
			if (!GameView::getInstance()->AllPacItem.at(index)->has_goods())
			{
				return;
			}
			if (GameView::getInstance()->AllPacItem.at(index)->goods().equipmentclazz()>0 &&GameView::getInstance()->AllPacItem.at(index)->goods().equipmentclazz()<10 )
			{
				for (int i=0;i<assistVector.size();i++)
				{
					if (index == assistVector.at(i).index_)
					{
						assistVector.erase(assistVector.begin()+i);
					}
				}
				int fightCapacity =GameView::getInstance()->AllPacItem.at(index)->goods().equipmentdetail().fightcapacity();
				refineCost-= fightCapacity/50+1;
				this->refreshExpValue(-(fightCapacity/5+1),false);
			}else
			{
				for (int i=0;i<assStoneVector.size();i++)
				{
					if (index == assStoneVector.at(i).index_)
					{
						assStoneVector.erase(assStoneVector.begin()+i);
					}
				}

				auto mapStone = StrengthStoneConfigData::s_StrengthStrone[GameView::getInstance()->AllPacItem.at(index)->goods().id()];
				int addEx =  mapStone->get_exp();
				this->refreshExpValue(-addEx*temp_amount,false);
			}
			
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,index);
		}break;
	case FIVESTRONGTH:
		{
			fiveAssist=false;
			auto strongFive = (StrengthEquip *)refineLayer->getChildByTag(FIVESTRONGTH);
			strongFive->removeFromParentAndCleanup(true);
			if (!GameView::getInstance()->AllPacItem.at(index)->has_goods())
			{
				return;
			}
			if (GameView::getInstance()->AllPacItem.at(index)->goods().equipmentclazz()>0 &&GameView::getInstance()->AllPacItem.at(index)->goods().equipmentclazz()<10 )
			{
				for (int i=0;i<assistVector.size();i++)
				{
					if (index == assistVector.at(i).index_)
					{
						assistVector.erase(assistVector.begin()+i);
					}
				}
				int fightCapacity =GameView::getInstance()->AllPacItem.at(index)->goods().equipmentdetail().fightcapacity();
				refineCost-= fightCapacity/50+1;
				this->refreshExpValue(-(fightCapacity/5+1),false);
			}else
			{
				auto mapStone = StrengthStoneConfigData::s_StrengthStrone[GameView::getInstance()->AllPacItem.at(index)->goods().id()];
				int addEx =  mapStone->get_exp();
				this->refreshExpValue(-addEx*temp_amount,false);

				for (int i=0;i<assStoneVector.size();i++)
				{
					if (index == assStoneVector.at(i).index_)
					{
						assStoneVector.erase(assStoneVector.begin()+i);
					}
				}
			}
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,index);
		}break;
	case STONELAYERTAG:
		{
			//exLabel_star->setText("0/0");
			//exPercent_star->setScaleX(0);
			starCost_label->setString("0");
			successRate->setString("0%");
			StrengthStone = false;
			auto strongRefine = (StrengthEquip *)starPropertyLayer->getChildByTag(STONELAYERTAG);
			strongRefine->removeFromParentAndCleanup(true);
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,index);
		}
	}
}

void EquipMentUi::callBackBtn_Refine(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		//GameUtils::playGameSound(REFINE_CLICK, 2, false);
		if (refineMainEquip == true)
		{
			if (firstAssist == true || secondtAssist == true || thirdAssist == true || fourAssist == true || fiveAssist == true)
			{
				isSureEquip(REFININGTYPE);

				//GameMessageProcessor::sharedMsgProcessor()->sendReq(1602,this);
			}
			else
			{
				const char *str = StringDataManager::getString("equip_refine_AssistIsNull_2");
				GameView::getInstance()->showAlertDialog(str);
			}
		}
		else
		{
			const char *str = StringDataManager::getString("equip_refine_refineIsNull");
			GameView::getInstance()->showAlertDialog(str);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

bool compareQuality(FolderInfo * folder1,FolderInfo *folder2)
{
	return folder1->goods().equipmentdetail().fightcapacity()<folder2->goods().equipmentdetail().fightcapacity();
}
bool compareRefineStoneUseLevel(FolderInfo * folder1,FolderInfo *folder2)
{
	auto mapStone1 = StrengthStoneConfigData::s_StrengthStrone[folder1->goods().id()];
	int addEx1 =  mapStone1->get_exp();

	auto mapStone2 = StrengthStoneConfigData::s_StrengthStrone[folder2->goods().id()];
	int addEx2 =  mapStone2->get_exp();

	return addEx1<addEx2;
}
/*
bool comparelevel(FolderInfo * folder1,FolderInfo *folder2)
{
	return folder1->goods().equipmentdetail().usergrade()<folder2->goods().equipmentdetail().usergrade();
}
*/
void EquipMentUi::callBackBtn_addAll(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		if (refineMainEquip == true)
		{
			int canEnterAssist = 0;
			if (firstAssist == false)
			{
				canEnterAssist++;
			}
			if (secondtAssist == false)
			{
				canEnterAssist++;
			}
			if (thirdAssist == false)
			{
				canEnterAssist++;
			}
			if (fourAssist == false)
			{
				canEnterAssist++;
			}
			if (fiveAssist == false)
			{
				canEnterAssist++;
			}
			if (canEnterAssist <= 0)
			{
				const char *str = StringDataManager::getString("equip_assist_enough");
				GameView::getInstance()->showAlertDialog(str);
				return;
			}
			stoneOfAmountRefine = 0;
			std::vector<FolderInfo *>::iterator iter;
			for (iter = GameView::getInstance()->AllEquipItem.begin(); iter != GameView::getInstance()->AllEquipItem.end(); iter++)
			{
				delete * iter;
			}
			GameView::getInstance()->AllEquipItem.clear();

			int useLv = 0;
			int refineLv = 0;

			if (mainEquipIsPac == true)
			{
				auto equipRefine = (StrengthEquip *)refineLayer->getChildByTag(REFININGMAINEQUIP);
				useLv = equipRefine->getRefineUseLevel();
				refineLv = equipRefine->getRefineLevel();
			}
			else
			{
				auto generalEquipBegin = (GeneralEquipRecover *)refineLayer->getChildByTag(GENERALREFINEEQUIPLOOK);
				useLv = generalEquipBegin->getEquipUseLevel();
				refineLv = generalEquipBegin->getRefineLevel();
			}

			if (refineLv >= 20)
			{
				const char *string_ = StringDataManager::getString("refineLevelIsFull");
				GameView::getInstance()->showAlertDialog(string_);
				return;
			}
			if (equipLookRefineLevel_ > this->getEquipBeginLevel())
			{
				return;
			}
			auto equipExp = StrengthEquipmentConfigData::s_EquipUpLevelExp[equipLookRefineLevel_ + 1];
			int nextExTemp = equipExp->get_exp()*(useLv / 1 + 1);

			int curExTemp = curExValue;

			//添加辅助 （1获取可以添加的数量）
			for (int pacindex = 0; pacindex<GameView::getInstance()->AllPacItem.size(); pacindex++)
			{
				if (GameView::getInstance()->AllPacItem.at(pacindex)->has_goods())
				{
					auto _folder = new FolderInfo();
					_folder->CopyFrom(*GameView::getInstance()->AllPacItem.at(pacindex));
					//1.辅助装备不可为已经添加的主装备 
					if ((_folder->goods().equipmentclazz() > 0 && _folder->goods().equipmentclazz()<10)
						&& _folder->goods().equipmentdetail().quality()<4
						&& _folder->goods().equipmentdetail().gradelevel() <= 0
						&& _folder->goods().equipmentdetail().starlevel() <= 0
						&& _folder->id() != mainEquipIndexRefine)
					{
						bool isexitAssment = false;
						//2.辅助装备不可为已经添加的辅助装备
						for (int assistEquipSize = 0; assistEquipSize <assistVector.size(); assistEquipSize++)
						{
							if (_folder->id() == assistVector.at(assistEquipSize).index_)
							{
								isexitAssment = true;
							}
						}

						if (isexitAssment == false)
						{
							GameView::getInstance()->AllEquipItem.push_back(_folder);
						}
					}
				}
			}

			sort(GameView::getInstance()->AllEquipItem.begin(), GameView::getInstance()->AllEquipItem.end(), compareQuality);
			//sort(GameView::getInstance()->AllEquipItem.begin(),GameView::getInstance()->AllEquipItem.end(),comparelevel);
			for (int addindex = 0; addindex<GameView::getInstance()->AllEquipItem.size(); addindex++)
			{
				//一键添加 可以升一级就不在添加
				if (curExTemp < nextExTemp && canEnterAssist>0)
				{
					pageView->curPackageItemIndex = GameView::getInstance()->AllEquipItem.at(addindex)->id();
					mes_source = 2;
					mes_index = GameView::getInstance()->AllEquipItem.at(addindex)->id();

					auto temp_folder_ = new FolderInfo();
					temp_folder_->CopyFrom(*GameView::getInstance()->AllEquipItem.at(addindex));
					this->addAssistEquipment(temp_folder_, false);

					int fightCapacity = GameView::getInstance()->AllEquipItem.at(addindex)->goods().equipmentdetail().fightcapacity();
					int addEx = fightCapacity / 5 + 1;
					curExTemp += addEx;
				}
				else
				{
					break;
				}
				canEnterAssist--;
			}

			//如果装备不够 则允许添加精炼石
			std::string m_stoneId = "";
			std::string m_stoneName = "";
			std::string m_limitLevel = "";
			EquipStrengthStoneLevelConfigData::typeAndLevle temp_typeAndlevel;
			temp_typeAndlevel.type_ = 1;
			temp_typeAndlevel.level_ = equipLookRefineLevel_ + 1;
			auto mapStrength_ = EquipStrengthStoneLevelConfigData::s_EquipStrengthUseStone[temp_typeAndlevel];
			if (mapStrength_ != NULL)
			{
				m_stoneId = mapStrength_->get_stoneId();
				m_stoneName = mapStrength_->get_stoneName();
				m_limitLevel = mapStrength_->get_limitLevel();
			}

			std::vector<FolderInfo * > folderRefineStoneVector;
			if (curExTemp < nextExTemp &&canEnterAssist >0)
			{
				for (int pacindex = 0; pacindex<GameView::getInstance()->AllPacItem.size(); pacindex++)
				{
					if (GameView::getInstance()->AllPacItem.at(pacindex)->has_goods())
					{
						if (GameView::getInstance()->AllPacItem.at(pacindex)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO &&
							strcmp(GameView::getInstance()->AllPacItem.at(pacindex)->goods().id().c_str(), m_stoneId.c_str()) == 0)
						{
							auto fol = new FolderInfo();
							fol->CopyFrom(*GameView::getInstance()->AllPacItem.at(pacindex));
							folderRefineStoneVector.push_back(fol);
						}
					}
				}

				sort(folderRefineStoneVector.begin(), folderRefineStoneVector.end(), compareRefineStoneUseLevel);

				for (int addindex = 0; addindex<folderRefineStoneVector.size(); addindex++)
				{
					//一键添加 可以升一级就不在添加
					if (curExTemp < nextExTemp && canEnterAssist >0)
					{
						bool isExit = false;
						for (int assistEquipSize = 0; assistEquipSize <assStoneVector.size(); assistEquipSize++)
						{
							if (folderRefineStoneVector.at(addindex)->id() == assStoneVector.at(assistEquipSize).index_)
							{
								isExit = true;
							}
						}

						if (isExit == false)
						{
							pageView->curPackageItemIndex = folderRefineStoneVector.at(addindex)->id();
							mes_source = 2;
							mes_index = folderRefineStoneVector.at(addindex)->id();

							auto mapStone = StrengthStoneConfigData::s_StrengthStrone[folderRefineStoneVector.at(addindex)->goods().id()];
							int addEx = mapStone->get_exp();
							int tempAmout = (nextExTemp - curExTemp) / addEx;
							if ((nextExTemp - curExTemp) % addEx > 0)
							{
								tempAmout += 1;
							}
							stoneOfAmountRefine += tempAmout;

							if (stoneOfAmountRefine > folderRefineStoneVector.at(addindex)->quantity())
							{
								stoneOfAmountRefine = folderRefineStoneVector.at(addindex)->quantity();
							}

							//stoneOfAmountRefine += tempAmout;
							curExTemp += addEx*stoneOfAmountRefine;

							auto temp_folder = new FolderInfo();
							temp_folder->CopyFrom(*folderRefineStoneVector.at(addindex));
							this->addAssistEquipment(temp_folder, false);
						}
					}
					else
					{
						break;
					}
					canEnterAssist--;
				}
			}

			if (folderRefineStoneVector.size() <= 0 && GameView::getInstance()->AllEquipItem.size() <= 0)
			{
				const char *strings_1 = StringDataManager::getString("equip_refineStone_isNotEnough_first");
				const char *strings_2 = StringDataManager::getString("equip_refineStone_isNotEnough_second");
				const char *strings_3 = StringDataManager::getString("equip_refineStone_isNotEnough_third");

				std::string stoneNotEnough = strings_1;
				stoneNotEnough.append(m_limitLevel);
				stoneNotEnough.append(strings_2);
				stoneNotEnough.append(m_stoneName);
				stoneNotEnough.append(strings_3);
				GameView::getInstance()->showAlertDialog(stoneNotEnough);
			}

			std::vector<FolderInfo *>::iterator itertemp;
			for (itertemp = folderRefineStoneVector.begin(); itertemp != folderRefineStoneVector.end(); itertemp++)
			{
				delete * itertemp;
			}
			folderRefineStoneVector.clear();
		}
		else
		{
			const char *str = StringDataManager::getString("equip_refine_refineIsNull");
			GameView::getInstance()->showAlertDialog(str);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void EquipMentUi::callBackUpstar(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		if (starMainEquip == true)
		{
			if (StrengthStone == true)
			{
				isSureEquip(STARTYPE);
			}
			else
			{
				const char *str = StringDataManager::getString("equip_Star_AssistIsNull_2");
				GameView::getInstance()->showAlertDialog(str);
			}
		}
		else
		{
			const char *str = StringDataManager::getString("equip_Star_StarIsNull");
			GameView::getInstance()->showAlertDialog(str);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}
bool compareStarLevel(FolderInfo * folder1,FolderInfo *folder2)
{
	return folder1->goods().uselevel() < folder2->goods().uselevel();
}
void EquipMentUi::callBackAllStar(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		//if equip star is full return
		if (curMainEquipStarLevel >= 20)
		{
			const char *str = StringDataManager::getString("starLevelIsFull");
			GameView::getInstance()->showAlertDialog(str);
			return;
		}

		std::string m_stoneId = "";
		std::string m_stoneName = "";
		std::string m_limitLevel = "";

		EquipStrengthStoneLevelConfigData::typeAndLevle temp_typeAndlevel;
		temp_typeAndlevel.type_ = 2;
		temp_typeAndlevel.level_ = curMainEquipStarLevel + 1;
		auto mapStrength_ = EquipStrengthStoneLevelConfigData::s_EquipStrengthUseStone[temp_typeAndlevel];
		if (mapStrength_ != NULL)
		{
			m_stoneId = mapStrength_->get_stoneId();
			m_stoneName = mapStrength_->get_stoneName();
			m_limitLevel = mapStrength_->get_limitLevel();
		}

		std::vector<FolderInfo* > starVector;
		if (starMainEquip == true)
		{
			for (int pacindex = 0; pacindex<GameView::getInstance()->AllPacItem.size(); pacindex++)
			{
				if (GameView::getInstance()->AllPacItem.at(pacindex)->has_goods())
				{
					auto _folder = new FolderInfo();
					_folder->CopyFrom(*GameView::getInstance()->AllPacItem.at(pacindex));

					if (_folder->goods().clazz() == GOODS_CLASS_SHENGXINGSHI && strcmp(_folder->goods().id().c_str(), m_stoneId.c_str()) == 0)
					{
						starVector.push_back(_folder);
					}
				}
			}

			if (starVector.size() <= 0)
			{
				const char *strings_1 = StringDataManager::getString("equip_starStone_isNotEnough_first");
				const char *strings_2 = StringDataManager::getString("equip_starStone_isNotEnough_second");
				const char *strings_3 = StringDataManager::getString("equip_starStone_isNotEnough_third");

				std::string stoneNotEnough = strings_1;
				stoneNotEnough.append(m_limitLevel);
				stoneNotEnough.append(strings_2);
				stoneNotEnough.append(m_stoneName);
				stoneNotEnough.append(strings_3);
				GameView::getInstance()->showAlertDialog(stoneNotEnough);

				return;
			}
			//if not sort 
			sort(starVector.begin(), starVector.end(), compareStarLevel);

			if (StrengthStone == true)
			{
				auto strengthequip = (StrengthEquip *)starPropertyLayer->getChildByTag(STONELAYERTAG);
				if (strengthequip != NULL)
				{
					this->getBackMainEquip(STONELAYERTAG, strengthequip->packIndex);
				}
			}

			int starUseLevel_ = starVector.at(0)->goods().uselevel();
			stoneOfAmountStar = (curMainEquipStarLevel*curMainEquipStarLevel*curMainEquipStarLevel*curMainEquipStarLevel + 1) / (starUseLevel_ * 70);
			if ((curMainEquipStarLevel*curMainEquipStarLevel*curMainEquipStarLevel*curMainEquipStarLevel + 1) % (starUseLevel_ * 70)>0)
			{
				stoneOfAmountStar += 1;
			}

			if (stoneOfAmountStar >starVector.at(0)->quantity())
			{
				stoneOfAmountStar = starVector.at(0)->quantity();
			}
			stoneOfIndexStar = starVector.at(0)->id();

			StrengthStone = true;
			auto strengthequip = StrengthEquip::create(GameView::getInstance()->AllPacItem.at(stoneOfIndexStar), false);
			strengthequip->setIgnoreAnchorPointForPosition(false);
			strengthequip->setAnchorPoint(Vec2(0, 0));
			strengthequip->setPosition(Vec2(80, 108.5f));
			strengthequip->setScale(0.8f);
			strengthequip->setTag(STONELAYERTAG);
			starPropertyLayer->addChild(strengthequip);

			GameMessageProcessor::sharedMsgProcessor()->sendReq(1606, (void *)stoneOfIndexStar, (void *)stoneOfAmountStar);

			int goodsCount_ = GameView::getInstance()->AllPacItem.at(stoneOfIndexStar)->quantity();
			char goodsUseNum[50];
			sprintf(goodsUseNum, "%d", stoneOfAmountStar);
			char goodsAllNum[50];
			sprintf(goodsAllNum, "%d", goodsCount_);
			std::string goodsUseAndCount = goodsUseNum;
			goodsUseAndCount.append("/");
			goodsUseAndCount.append(goodsAllNum);

			GameView::getInstance()->pacPageView->SetCurFolderGray(true, stoneOfIndexStar, goodsUseAndCount);
			std::vector<FolderInfo *>::iterator iter;
			for (iter = starVector.begin(); iter != starVector.end(); iter++)
			{
				delete * iter;
			}
			starVector.clear();
		}
		else
		{
			const char *str = StringDataManager::getString("equip_Star_StarIsNull");
			GameView::getInstance()->showAlertDialog(str);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipMentUi::callBackBaptize(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (baptizeMainEquip == true)
		{
			//this->callBackBaptizeSure(obj);

			int equipBaptizeValueCount = 0;
			auto temp_goods_ = new GoodsInfo();
			if (mainEquipIsPac == true)
			{
				StrengthEquip * equipBaptize_ = (StrengthEquip *)baptizePropertylayer->getChildByTag(BAPTIZEEQUIP);
				if (equipBaptize_)
				{
					temp_goods_->CopyFrom(GameView::getInstance()->AllPacItem.at(equipBaptize_->packIndex)->goods());
				}
			}
			else
			{
				GeneralEquipRecover * generalEquip = (GeneralEquipRecover *)baptizePropertylayer->getChildByTag(GENERALBAPTIZEEQUIP);
				if (generalEquip)
				{
					temp_goods_->CopyFrom(generalEquip->equip_->goods());
				}
			}

			int baptizeSize_ = temp_goods_->equipmentdetail().properties_size();
			for (int i = 0; i<baptizeSize_; i++)
			{
				if (temp_goods_->equipmentdetail().properties(i).type() == 3)
				{
					equipBaptizeValueCount++;
				}
			}

			if (propertyLock.size() >= equipBaptizeValueCount)
			{
				const char *string_ = StringDataManager::getString("equip_baptizeClockOfAllvalue_equip");
				GameView::getInstance()->showAlertDialog(string_);
				delete temp_goods_;
				return;
			}

			if (propertyLock.size() >0)
			{
				this->equipMentSureBaptize(NULL);
			}
			else
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1611, this, (void *)0);
			}

		}
		else
		{
			const char *str = StringDataManager::getString("equip_Baptize_BaptizeIsNull");
			GameView::getInstance()->showAlertDialog(str);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipMentUi::callBackBaptizeSure( Ref * obj )
{
	/*
	propertyLock.clear();
	if (firstPropertyLock->isSelected()==1)
	{
		//lock value id
		int powerLockIndex_ = additionProperty_baptize.at(0)->property(); 
		propertyLock.push_back(powerLockIndex_);
	}
	
	if (secondPropertyLock->isSelected()==1)
	{
		int powerLockIndex_ = additionProperty_baptize.at(1)->property(); 
		propertyLock.push_back(powerLockIndex_);
	}
	
	if (thirdPropertyLock->isSelected()==1)
	{
		int powerLockIndex_ = additionProperty_baptize.at(2)->property(); 
		propertyLock.push_back(powerLockIndex_);
	}
	
	if (fourPropertyLock->isSelected()==1)
	{
		int powerLockIndex_ = additionProperty_baptize.at(3)->property(); 
		propertyLock.push_back(powerLockIndex_);
	}
	
	int amount_ =propertyLock.size();
	if (amount_ >0)
	{
		const char *stringsBegin_  = StringDataManager::getString("equipment_sureOfBaptizeNum");
		char baptizeNum[5];
		sprintf(baptizeNum,"%d",amount_);
		const char *stringsEnd_  = StringDataManager::getString("equipment_sureOfBaptizeValue");

		std::string stringBaptize = stringsBegin_;
		stringBaptize.append(baptizeNum);
		stringBaptize.append(stringsEnd_);
		GameView::getInstance()->showPopupWindow(stringBaptize,2,this,SEL_CallFuncO(EquipMentUi::equipMentSureBaptize),NULL);
	}else
	{
		this->equipMentSureBaptize(NULL);
	}
	*/
}

void EquipMentUi::thirdButtonEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameUtils::playGameSound(GEM_CLICK, 2, false);
		if (gemMainEquip == true)
		{
			auto btn = dynamic_cast<Button *>(pSender);
			const char *str_cost = StringDataManager::getString("equip_cost_pick_");
			const char *str_goldNum = StringDataManager::getString("equip_cost_gold_pick");

			int gemLevel = equipmentHoleV.at(2)->gemlevel();
			int costPick = gemLevel * gemLevel * 1000;
			char costStr[20];
			sprintf(costStr, "%d", costPick);

			std::string str_ = str_cost;
			str_.append(costStr);
			str_.append(str_goldNum);
			GameView::getInstance()->showPopupWindow(str_, 2, this, CC_CALLFUNCO_SELECTOR(EquipMentUi::equipThirdpickSure), NULL);
		}
		else
		{
			const char *str = StringDataManager::getString("equip_gem_pick");
			GameView::getInstance()->showAlertDialog(str);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipMentUi::secondButtonEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameUtils::playGameSound(GEM_CLICK, 2, false);
		if (gemMainEquip == true)
		{
			auto btn = dynamic_cast<Button *>(pSender);
			const char *str_cost = StringDataManager::getString("equip_cost_pick_");
			const char *str_goldNum = StringDataManager::getString("equip_cost_gold_pick");

			int gemLevel = equipmentHoleV.at(1)->gemlevel();
			int costPick = gemLevel * gemLevel * 1000;
			char costStr[20];
			sprintf(costStr, "%d", costPick);

			std::string str_ = str_cost;
			str_.append(costStr);
			str_.append(str_goldNum);
			GameView::getInstance()->showPopupWindow(str_, 2, this, CC_CALLFUNCO_SELECTOR(EquipMentUi::equipSecondpickSure), NULL);
		}
		else
		{
			const char *str = StringDataManager::getString("equip_gem_inlay");
			GameView::getInstance()->showAlertDialog(str);
		}
	}

	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipMentUi::firstButtonEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameUtils::playGameSound(GEM_CLICK, 2, false);
		if (gemMainEquip == true)
		{
			auto btn = dynamic_cast<Button *>(pSender);

			const char *str_cost = StringDataManager::getString("equip_cost_pick_");
			const char *str_goldNum = StringDataManager::getString("equip_cost_gold_pick");

			int gemLevel = equipmentHoleV.at(0)->gemlevel();
			int costPick = gemLevel * gemLevel * 1000;
			char costStr[20];
			sprintf(costStr, "%d", costPick);

			std::string str_ = str_cost;
			str_.append(costStr);
			str_.append(str_goldNum);
			GameView::getInstance()->showPopupWindow(str_, 2, this, CC_CALLFUNCO_SELECTOR(EquipMentUi::equipFirstPickSure), NULL);
		}
		else
		{
			const char *str = StringDataManager::getString("equip_gem_tapping");
			GameView::getInstance()->showAlertDialog(str);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipMentUi::callBackHole(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameUtils::playGameSound(GEM_CLICK, 2, false);
		if (gemMainEquip == true)
		{
			int select_ = 0;
			if (useGemBingding->isSelected() == true)
			{
				//not bingding
				select_ = 1;
				if (this->getCurHoleStoneAmount(0) > (holeFalg + 1)*(holeFalg + 1)*(holeFalg + 1))
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1622, (void *)select_);
				}
				else
				{
					if (this->getCurHoleStoneAmount(1) > 0)
					{
						const char *string_ = StringDataManager::getString("equip_holeOfBingding_BingdingEquip");
						GameView::getInstance()->showPopupWindow(string_, 2, this, CC_CALLFUNCO_SELECTOR(EquipMentUi::isSureNotHoleBingding), NULL);
					}
					else
					{
						GameMessageProcessor::sharedMsgProcessor()->sendReq(1622, (void *)select_);
					}
				}
			}
			else
			{
				//bingding
				select_ = 0;

				int mainEquip = 0;
				if (mainEquipIsPac == true)
				{
					mainEquip = GameView::getInstance()->AllPacItem.at(mainEquipIndexRefine)->goods().binding();
				}
				else
				{
					for (int j = 0; j< curMainEquipvector.size(); j++)
					{
						if (generalEquipOfPart == curMainEquipvector.at(j)->part())
						{
							mainEquip = curMainEquipvector.at(j)->goods().binding();
						}
					}
				}
				if (mainEquip == 0 && this->getCurHoleStoneAmount(0) >0)//main equip is not binging
				{
					const char *string_ = StringDataManager::getString("equip_holeOfBingding_BingdingEquip_isSure");
					GameView::getInstance()->showPopupWindow(string_, 2, this, CC_CALLFUNCO_SELECTOR(EquipMentUi::isSureBingdingHoleBingding), NULL);
				}
				else
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1622, (void *)select_);
				}
			}
		}
		else
		{
			const char *str = StringDataManager::getString("equip_gem_tapping");
			GameView::getInstance()->showAlertDialog(str);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipMentUi::CurrentPageViewChanged(Ref *pSender, PageView::EventType type)
{
	if (type == PageView::EventType::TURNING) {
		auto pageView = dynamic_cast<PageView*>(pSender);
		switch (static_cast<int>(pageView->getCurrentPageIndex()))
		{
		case 0:
			currentPage->setPosition(Vec2(522, 73));
			break;
		case 1:
			currentPage->setPosition(Vec2(543, 73));
			break;
		case 2:
			currentPage->setPosition(Vec2(562, 73));
			break;
		case 3:
			currentPage->setPosition(Vec2(581, 73));
			break;
		case 4:
			currentPage->setPosition(Vec2(600, 73));
			break;
		}
	}
}

void EquipMentUi::addAssistGeneralOfEquip(CEquipment * equips)
{
	if (refineMainEquip == false)
	{
		return;
	}

	if (firstAssist==true && secondtAssist==true && thirdAssist==true && fourAssist==true&& fiveAssist ==true )
	{
		const char *str = StringDataManager::getString("equip_assist_enough");
		GameView::getInstance()->showAlertDialog(str);
		return ;
	}

	for (int j=0;j<generalEquipVector.size();j++)
	{
		auto starGeneralEquip = (GeneralEquipMent*)roleLayer->getChildByTag(GENERALEQUIPMENTLIST + j);
		if (equips->part() == starGeneralEquip->Btn_pacItemFrame->getTag())
		{
			starGeneralEquip->SetCurgeneralEquipGray(true);
		}
	}

	if (firstAssist==false)
	{
		auto strengthequip= GeneralEquipRecover::create(equips,false);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(90,147));
		strengthequip->setScale(0.8f);
		strengthequip->setTag(GENERALFIRSTASSIST);
		refineLayer->addChild(strengthequip);

		firstAssist=true;	
	}else if (secondtAssist==false)
	{
		auto strengthequip= GeneralEquipRecover::create(equips,false);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(146,147));
		strengthequip->setScale(0.8f);
		strengthequip->setTag(GENERALSECONDASSIST);
		refineLayer->addChild(strengthequip);

		secondtAssist=true;
	}else if (thirdAssist==false)
	{
		auto strengthequip= GeneralEquipRecover::create(equips,false);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(203,147));
		strengthequip->setScale(0.8f);
		strengthequip->setTag(GENERALTHIRDASSIST);
		refineLayer->addChild(strengthequip);

		thirdAssist=true;
	}else if (fourAssist==false)
	{
		auto strengthequip= GeneralEquipRecover::create(equips,false);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(259,147));
		strengthequip->setScale(0.8f);
		strengthequip->setTag(GENERALFOURASSIST);
		refineLayer->addChild(strengthequip);

		fourAssist=true;
	}else if (fiveAssist==false)
	{
		auto strengthequip= GeneralEquipRecover::create(equips,false);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(315,147));
		strengthequip->setScale(0.8f);
		strengthequip->setTag(GENERALFIVEASSIST);
		refineLayer->addChild(strengthequip);

		fiveAssist=true;
	}
	mes_source = 1;
	mes_assistEquipPob = equips->part();

	int equipOfBinging = equips->goods().binding();
	int equipOfStarLv = equips->goods().equipmentdetail().starlevel();
	auto goodsInfo_ = new GoodsInfo();
	goodsInfo_->CopyFrom(equips->goods());
	bool equipOfGem = isHaveGemValue(goodsInfo_);
	delete goodsInfo_;
	assistEquipStruct assists_={mes_source,mes_assistEquipPob,mes_petid,pageView->curPackageItemIndex,0,equipOfBinging,equipOfStarLv,equipOfGem};
	assistVector.push_back(assists_);

	int fightCapacity = equips->goods().equipmentdetail().fightcapacity();
	refineCost+= fightCapacity/50+1;
	this->refreshExpValue((fightCapacity/5+1),false);
}

void EquipMentUi::addAssistEquipment( FolderInfo * floders,bool isAction)
{
	if (refineMainEquip == false)
	{
		return;
	}

	for (int assvectorIndex =0;assvectorIndex<assStoneVector.size();assvectorIndex++)
	{
		if (floders->id() == assStoneVector.at(assvectorIndex).index_)
		{
			this->getBackMainEquip(assStoneVector.at(assvectorIndex).tag_,floders->id());
		}
	}
	
	if (firstAssist==true && secondtAssist==true && thirdAssist==true && fourAssist==true && fiveAssist ==true)
	{
		const char *str = StringDataManager::getString("equip_assist_enough");
		GameView::getInstance()->showAlertDialog(str);
		return ;
	}
	//
	Vec2 p2;
	int mes_tag;
	if (firstAssist==false)
	{
		p2 = Vec2(110.2f,167);
		mes_tag = FRISTASSIST;
		firstAssist = true;

		auto strengthequip= StrengthEquip::create(floders,false);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(90.1f,147));
		strengthequip->setScale(0.8f);
		strengthequip->setTag(FRISTASSIST);
		refineLayer->addChild(strengthequip);

		if (isAction)
		{
			strengthequip->setVisible(false);
		}
		

	}else if (secondtAssist==false)
	{
		p2 = Vec2(166,167);
		mes_tag = SECONDASSIST;
		secondtAssist = true;

		auto strengthequip= StrengthEquip::create(floders,false);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(146,147));
		strengthequip->setScale(0.8f);
		strengthequip->setTag(SECONDASSIST);
		refineLayer->addChild(strengthequip);
		if (isAction)
		{
			strengthequip->setVisible(false);
		}
	}else if (thirdAssist==false)
	{
		p2 = Vec2(221.5f,167);
		mes_tag = THIRDASSIST;
		thirdAssist = true;
		auto strengthequip= StrengthEquip::create(floders,false);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(201.5f,147));
		strengthequip->setScale(0.8f);
		strengthequip->setTag(THIRDASSIST);
		refineLayer->addChild(strengthequip);
		if (isAction)
		{
			strengthequip->setVisible(false);
		}
		
	}else if (fourAssist == false)
	{
		p2 = Vec2(277,167);
		mes_tag = FOURASSIST;
		fourAssist = true;
		auto strengthequip= StrengthEquip::create(floders,false);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(257.5f,147));
		strengthequip->setScale(0.8f);
		strengthequip->setTag(FOURASSIST);
		refineLayer->addChild(strengthequip);
		if (isAction)
		{
			strengthequip->setVisible(false);
		}
		
	}else if (fiveAssist ==false)
	{
		p2 = Vec2(332.5f,167);
		mes_tag = FIVESTRONGTH;
		fiveAssist = true;
		auto strengthequip= StrengthEquip::create(floders,false);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(313.2f,147));
		strengthequip->setScale(0.8f);
		strengthequip->setTag(FIVESTRONGTH);
		refineLayer->addChild(strengthequip);
		if (isAction)
		{
			strengthequip->setVisible(false);
		}
	}
	mes_source = 2;
	//先运行数据， 把数据存起来， 在runAction
	if (floders->goods().equipmentclazz()> 0 && floders->goods().equipmentclazz()<10 )
	{
		int equipOfBinging = floders->goods().binding();
		int equipOfStarlv = floders->goods().equipmentdetail().starlevel();
		auto info_ =new GoodsInfo();
		info_->CopyFrom(floders->goods());
		bool equipOfGem = isHaveGemValue(info_);
		delete info_;
		assistEquipStruct assists_={mes_source,mes_assistEquipPob,mes_petid,pageView->curPackageItemIndex,0,equipOfBinging,equipOfStarlv,equipOfGem};
		assistVector.push_back(assists_);
		int fightCapacity =floders->goods().equipmentdetail().fightcapacity();
		refineCost+= fightCapacity/50+1;

		if (!isAction)
		{
			refreshExpValue(fightCapacity/5+1,false);
		}

		GameView::getInstance()->pacPageView->SetCurFolderGray(true,floders->id());
	}else
	{
		EquipMentUi::assistStoneStruct assistStone = {mes_source,pageView->curPackageItemIndex,stoneOfAmountRefine,0,mes_tag,floders->goods().binding()};
		assStoneVector.push_back(assistStone);

		auto mapStone = StrengthStoneConfigData::s_StrengthStrone[floders->goods().id()];
		int addEx =  mapStone->get_exp();

		if (!isAction )
		{
			refreshExpValue(addEx*stoneOfAmountRefine,false);
		}
	
		int goodsCount_ = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity();
		char goodsUseNum[50];
		sprintf(goodsUseNum,"%d",stoneOfAmountRefine);
		char goodsAllNum[50];
		sprintf(goodsAllNum,"%d",goodsCount_);

		std::string goodsUseAndCount = goodsUseNum;
		goodsUseAndCount.append("/");
		goodsUseAndCount.append(goodsAllNum);

		GameView::getInstance()->pacPageView->SetCurFolderGray(true,floders->id(),goodsUseAndCount);
	}

	if (isAction)
	{
		Vec2 p1 = GameView::getInstance()->pacPageView->getPackageItem(floders->id())->getPosition();
		RefreshEquipData::instance()->assistRunAction(p1,p2,floders,isAction);
	}
}

void EquipMentUi::removeAssistAndStone(int operateType)
{
	//remove all assist equip and stone
	StrengthEquip *btn_refining_first= (StrengthEquip *)refineLayer->getChildByTag(FRISTASSIST);
	if (btn_refining_first!= NULL)
	{
		if (operateType == operatorGetBack)
		{
			this->getBackMainEquip(FRISTASSIST,btn_refining_first->packIndex);
		}else
		{
			if (GameView::getInstance()->AllPacItem.at(btn_refining_first->packIndex)->has_goods())
			{
				GameView::getInstance()->pacPageView->SetCurFolderGray(false,btn_refining_first->packIndex);
			}
			btn_refining_first->removeFromParentAndCleanup(true);
		}
	}

	StrengthEquip *btn_refining_second= (StrengthEquip *)refineLayer->getChildByTag(SECONDASSIST);
	if (btn_refining_second != NULL)
	{
		if (operateType == operatorGetBack)
		{
			this->getBackMainEquip(SECONDASSIST,btn_refining_second->packIndex);
		}else
		{
			if (GameView::getInstance()->AllPacItem.at(btn_refining_second->packIndex)->has_goods())
			{
				GameView::getInstance()->pacPageView->SetCurFolderGray(false,btn_refining_second->packIndex);
			}
			btn_refining_second->removeFromParentAndCleanup(true);
		}
		
	}
	auto btn_refining_third= (StrengthEquip *)refineLayer->getChildByTag(THIRDASSIST);
	if (btn_refining_third != NULL)
	{
		if (operateType == operatorGetBack)
		{
			this->getBackMainEquip(THIRDASSIST,btn_refining_third->packIndex);
		}else
		{
			if (GameView::getInstance()->AllPacItem.at(btn_refining_third->packIndex)->has_goods())
			{
				GameView::getInstance()->pacPageView->SetCurFolderGray(false,btn_refining_third->packIndex);
			}
			btn_refining_third->removeFromParentAndCleanup(true);
		}
		
	}
	StrengthEquip *btn_refining_four= (StrengthEquip *)refineLayer->getChildByTag(FOURASSIST);
	if (btn_refining_four != NULL)
	{
		if (operateType == operatorGetBack)
		{
			this->getBackMainEquip(FOURASSIST,btn_refining_four->packIndex);
		}else
		{
			if (GameView::getInstance()->AllPacItem.at(btn_refining_four->packIndex)->has_goods())
			{
				GameView::getInstance()->pacPageView->SetCurFolderGray(false,btn_refining_four->packIndex);
			}
			btn_refining_four->removeFromParentAndCleanup(true);
		}
	}
	auto btn_refining_five= (StrengthEquip *)refineLayer->getChildByTag(FIVESTRONGTH);
	if (btn_refining_five != NULL)
	{
		if (operateType == operatorGetBack)
		{
			this->getBackMainEquip(FIVESTRONGTH,btn_refining_five->packIndex);
		}else
		{
			if (GameView::getInstance()->AllPacItem.at(btn_refining_five->packIndex)->has_goods())
			{
				GameView::getInstance()->pacPageView->SetCurFolderGray(false,btn_refining_five->packIndex);
			}
			btn_refining_five->removeFromParentAndCleanup(true);
		}
	}

	auto generalequip_First =(GeneralEquipMent *)refineLayer->getChildByTag(GENERALFIRSTASSIST);
	if (generalequip_First != NULL)
	{
		if (operateType == operatorGetBack)
		{
			this->getBackGeneralMainEquip(GENERALFIRSTASSIST);
		}else
		{
			generalequip_First->removeFromParentAndCleanup(true);
		}
	}
	auto generalequip_second =(GeneralEquipMent *)refineLayer->getChildByTag(GENERALSECONDASSIST);
	if (generalequip_second != NULL)
	{
		if (operateType == operatorGetBack)
		{
			this->getBackGeneralMainEquip(GENERALSECONDASSIST);
		}else
		{
			generalequip_second->removeFromParentAndCleanup(true);
		}
	}
	auto generalequip_third =(GeneralEquipMent *)refineLayer->getChildByTag(GENERALTHIRDASSIST);
	if (generalequip_third != NULL)
	{
		if (operateType == operatorGetBack)
		{
			this->getBackGeneralMainEquip(GENERALTHIRDASSIST);
		}else
		{
			generalequip_third->removeFromParentAndCleanup(true);
		}
	}
	auto generalequip_four =(GeneralEquipMent *)refineLayer->getChildByTag(GENERALFOURASSIST);
	if (generalequip_four != NULL)
	{
		if (operateType == operatorGetBack)
		{
			this->getBackGeneralMainEquip(GENERALFOURASSIST);
		}else
		{
			generalequip_four->removeFromParentAndCleanup(true);
		}
	}
	auto generalequip_five =(GeneralEquipMent *)refineLayer->getChildByTag(GENERALFIVEASSIST);
	if (generalequip_five != NULL)
	{
		if (operateType == operatorGetBack)
		{
			this->getBackGeneralMainEquip(GENERALFIVEASSIST);
		}else
		{
			generalequip_five->removeFromParentAndCleanup(true);
		}
	}
	assistVector.clear();
	auto stoneOfSatr= (StrengthEquip *)starPropertyLayer->getChildByTag(STONELAYERTAG);
	if (stoneOfSatr != NULL)
	{
		StrengthStone=false;
		this->getBackMainEquip(STONELAYERTAG,stoneOfIndexStar);
	}
	
	//if change tab 
	if (operateType == operatorGetBack)
	{
		auto strengthEquip=(StrengthEquip *)refineLayer->getChildByTag(REFININGMAINEQUIP);
		if (strengthEquip != NULL)
		{
			refineMainEquip =false;
			this->getBackMainEquip(REFININGMAINEQUIP,mainEquipIndexRefine);
		}

		auto generalequip_refine =(GeneralEquipRecover *)refineLayer->getChildByTag(GENERALREFINEEQUIP);
		if (generalequip_refine != NULL)
		{
			refineMainEquip =false;
			this->getBackGeneralMainEquip(GENERALREFINEEQUIP);
		}

		auto starEquip=(StrengthEquip *)starPropertyLayer->getChildByTag(STAREQUIP);
		if (starEquip != NULL)
		{
			starMainEquip=false;
			this->getBackMainEquip(STAREQUIP,mainEquipIndexRefine);
		}

		auto generalequip_star =(GeneralEquipRecover *)starPropertyLayer->getChildByTag(GENERALSTAREQUIP);
		if (generalequip_star != NULL)
		{
			starMainEquip=false;
			this->getBackGeneralMainEquip(GENERALSTAREQUIP);
		}

		auto btn_gem=(StrengthEquip *)gemLayer->getChildByTag(GEMEQUIP);
		if (btn_gem!=NULL)
		{
			gemMainEquip=false;
			this->getBackMainEquip(GEMEQUIP,mainEquipIndexRefine);
		}

		auto generalequip_gem =(GeneralEquipRecover *)gemLayer->getChildByTag(GENERALGEMEQUIP);
		if (generalequip_gem !=NULL)
		{
			gemMainEquip=false;
			this->getBackGeneralMainEquip(GENERALGEMEQUIP);
		}
		
		auto btn_baptize=(StrengthEquip *)baptizePropertylayer->getChildByTag(BAPTIZEEQUIP);
		if (btn_baptize != NULL)
		{
			baptizeMainEquip=false;
			this->getBackMainEquip(BAPTIZEEQUIP,mainEquipIndexRefine);
		}

		auto generalequip_baptize =(GeneralEquipRecover *)baptizePropertylayer->getChildByTag(GENERALBAPTIZEEQUIP);
		if (generalequip_baptize !=NULL)
		{
			baptizeMainEquip=false;
			this->getBackGeneralMainEquip(GENERALBAPTIZEEQUIP);
		}

		refreshCurState();
		
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1699,this);
	}
}


void EquipMentUi::refreshEquipProperty(int curValue,int allValue,bool isAction)
{
	refine_textAreaPutEquipment->setVisible(false);
	refineLayer->removeAllChildren();
	if (refineMainEquip == false) 
	{
		refine_textAreaPutEquipment->setVisible(true);
		//labelFont_curRefinelevel->setText("0");
		//labelFont_curRefinelevel->setVisible(false);
		exLabel_refine->setString("0/0");
		exPercent_refine->setPercentage(0);
		exPercent_refineAdd->setScaleX(0);
		exPercent_refineAdd->setScaleX(0);
		labelCost->setString("0");

		refineBeginFirstLabel->setVisible(false);
		refineBeginSecondLabel->setVisible(false);
		refineBeginFirstvalue->setVisible(false);
		refineBeginSecondvalue->setVisible(false);
		refineEndFirstLabel->setVisible(false);
		refineEndSecondLabel->setVisible(false);
		refineEndFirstvalue->setVisible(false);
		refineEndSecondvalue->setVisible(false);
		refineEndFirstvalueAdd->setVisible(false);
		refineEndSecondvalueAdd->setVisible(false);
		return;
	}
	RefreshEquipData::instance()->refreshRefineEquip();	

	std::string str_BeginFirstLabel = additionProperty.at(0)->description();
	refineBeginFirstLabel->setString(str_BeginFirstLabel.c_str());
	refineBeginFirstLabel->setVisible(true);
	std::string str_BeginSecondLabel = additionProperty.at(1)->description();
	refineBeginSecondLabel->setString(str_BeginSecondLabel.c_str());
	refineBeginSecondLabel->setVisible(true);
	char beginFirstValue[10];
	sprintf(beginFirstValue,"%d",additionProperty.at(0)->value(0));	
	refineBeginFirstvalue->setString(beginFirstValue);
	refineBeginFirstvalue->setVisible(true);
	char beginSecondValue[10];
	sprintf(beginSecondValue,"%d",additionProperty.at(1)->value(0));	
	refineBeginSecondvalue->setString(beginSecondValue);
	refineBeginSecondvalue->setVisible(true);

	std::string str_EndFirstLabel = additionProperty.at(0)->description();
	refineEndFirstLabel->setString(str_EndFirstLabel.c_str());
	refineEndFirstLabel->setVisible(true);
	std::string str_EndSecondLabel = additionProperty.at(1)->description();
	refineEndSecondLabel->setString(str_EndSecondLabel.c_str());
	refineEndSecondLabel->setVisible(true);
	
	char endFirstValue[10];
	sprintf(endFirstValue,"%d",additionProperty.at(0)->value(0));	
	refineEndFirstvalue->setString(beginFirstValue);
	refineEndFirstvalue->setVisible(true);
	char endSecondValue[10];
	sprintf(endSecondValue,"%d",additionProperty.at(1)->value(0));	
	refineEndSecondvalue->setString(beginSecondValue);
	refineEndSecondvalue->setVisible(true);

	refineEndFirstvalueAdd->setVisible(false);
	refineEndSecondvalueAdd->setVisible(false);
	
	curExValue =curValue;
	allExValue = allValue;
	this->refreshExpValue(0,true,isAction);
}

void EquipMentUi::refreshEquipProperty_star(int curluckval,int allluckval,int starCostVale,bool isAction /*= false*/)
{
	star_textArea_putEquipment->setVisible(false);
	starPropertyLayer->removeAllChildren();
	exLabel_star->setString("0/0");
	if (isAction == false)
	{
		exPercent_star->setScaleX(0);
	}
	starCost_label->setString("0");
	successRate->setString("0%");
	if (starMainEquip==false)
	{
		star_textArea_putEquipment->setVisible(true);

// 		char starstr[30];
// 		for (int i=1;i<=10;i++)
// 		{
// 			sprintf(starstr, "ImageView_star_off_%d", i);
// 			starlv_= (ImageView *)Helper::seekWidgetByName(equipPanel,starstr);
// 			starlv_->loadTexture("res_ui/star_off.png");
// 			starlv_->setVisible(false);
// 		}

		starBeginFirstLabel->setVisible(false);
		starBeginSecondLabel->setVisible(false);
		starBeginThirdLabel->setVisible(false);
		starBeginFourLabel->setVisible(false);

		starBeginFirstvalue->setVisible(false);
		starBeginSecondvalue->setVisible(false);
		starBeginThirdvalue->setVisible(false);
		starBeginFourvalue->setVisible(false);
		
		starEndFirstLabel->setVisible(false);
		starEndSecondLabel->setVisible(false);
		starEndThirdLabel->setVisible(false);
		starFourFourLabel->setVisible(false);

		starEndFirstvalue->setVisible(false);
		starEndSecondvalue->setVisible(false);
		starEndThirdvalue->setVisible(false);
		starEndFourvalue->setVisible(false);

		starEndFirstvalueAdd->setVisible(false);
		starEndSecondvalueAdd->setVisible(false);
		starEndThirdvalueAdd->setVisible(false);
		starEndFourvalueAdd->setVisible(false);
		return;
	}
	/*	
	char str[30];
	for (int i=1;i<=10;i++)
	{
		sprintf(str, "ImageView_star_off_%d", i);
		starlv_= (ImageView *)Helper::seekWidgetByName(equipPanel,str);
		starlv_->loadTexture("res_ui/star_off.png");
		starlv_->setVisible(true);
	}

	int starlv_int = starLevelValue/2;
	int starlv_half =starLevelValue%2;

	char strStar_[30];
	for (int i=1;i<=starlv_int;i++)
	{
		sprintf(strStar_, "ImageView_star_off_%d", i);
		starlv_= (ImageView *)Helper::seekWidgetByName(equipPanel,strStar_);
		starlv_->loadTexture("res_ui/star_on.png");
		starlv_->setVisible(true);
	}

	if (starlv_half >0)
	{
		sprintf(strStar_, "ImageView_star_off_%d", starlv_int+1);
		starlv_= (ImageView *)Helper::seekWidgetByName(equipPanel,strStar_);
		starlv_->loadTexture("res_ui/star_half.png");
		starlv_->setVisible(true);
	}
	*/
	m_curLuckValue =curluckval;
	m_allLuckValue =allluckval;
	this->refreshStarLuckValue(m_curLuckValue,starCostVale,isAction);
	RefreshEquipData::instance()->refreshStarEquip();

	starBeginFirstLabel->setString(additionProperty_star.at(0)->description().c_str());
	starBeginFirstLabel->setVisible(true);
	starBeginSecondLabel->setString(additionProperty_star.at(1)->description().c_str());
	starBeginSecondLabel->setVisible(true);
	starBeginThirdLabel->setString(additionProperty_star.at(2)->description().c_str());
	starBeginThirdLabel->setVisible(true);
	starBeginFourLabel->setString(additionProperty_star.at(3)->description().c_str());
	starBeginFourLabel->setVisible(true);
	char starFirstBeginValue[20];
	sprintf(starFirstBeginValue,"%d",additionProperty_star.at(0)->value(0));
	starBeginFirstvalue->setString(starFirstBeginValue);
	starBeginFirstvalue->setVisible(true);
	char starSecondBeginValue[20];
	sprintf(starSecondBeginValue,"%d",additionProperty_star.at(1)->value(0));
	starBeginSecondvalue->setString(starSecondBeginValue);
	starBeginSecondvalue->setVisible(true);
	char starThirdBeginValue[20];
	sprintf(starThirdBeginValue,"%d",additionProperty_star.at(2)->value(0));
	starBeginThirdvalue->setString(starThirdBeginValue);
	starBeginThirdvalue->setVisible(true);
	char starFourBeginValue[20];
	sprintf(starFourBeginValue,"%d",additionProperty_star.at(3)->value(0));
	starBeginFourvalue->setString(starFourBeginValue);
	starBeginFourvalue->setVisible(true);

	starEndFirstLabel->setString(additionProperty_star.at(0)->description().c_str());
	starEndFirstLabel->setVisible(true);
	starEndSecondLabel->setString(additionProperty_star.at(1)->description().c_str());
	starEndSecondLabel->setVisible(true);
	starEndThirdLabel->setString(additionProperty_star.at(2)->description().c_str());
	starEndThirdLabel->setVisible(true);
	starFourFourLabel->setString(additionProperty_star.at(3)->description().c_str());
	starFourFourLabel->setVisible(true);
	starEndFirstvalue->setString(starFirstBeginValue);
	starEndFirstvalue->setVisible(true);
	starEndSecondvalue->setString(starSecondBeginValue);
	starEndSecondvalue->setVisible(true);
	starEndThirdvalue->setString(starThirdBeginValue);
	starEndThirdvalue->setVisible(true);
	starEndFourvalue->setString(starFourBeginValue);
	starEndFourvalue->setVisible(true);
	
	if (curMainEquipStarLevel<20)
	{
		for (int aa=0; aa< 4;aa++)
		{
			if (additionProperty_star.at(aa)->value_size() <2)
			{
				return;
			}
		}

		char starFirstEndValueadd[20];
		sprintf(starFirstEndValueadd,"%d",additionProperty_star.at(0)->value(1));
		std::string strFirst_ ="(+";
		strFirst_.append(starFirstEndValueadd);
		strFirst_.append(")");
		starEndFirstvalueAdd->setString(strFirst_.c_str());
		starEndFirstvalueAdd->setVisible(true);

		char starSecondEndValueAdd[20];
		sprintf(starSecondEndValueAdd,"%d",additionProperty_star.at(1)->value(1));
		std::string strSecond_ ="(+";
		strSecond_.append(starSecondEndValueAdd);
		strSecond_.append(")");
		starEndSecondvalueAdd->setString(strSecond_.c_str());
		starEndSecondvalueAdd->setVisible(true);

		char starThirdEndValueAdd[20];
		sprintf(starThirdEndValueAdd,"%d",additionProperty_star.at(2)->value(1));
		std::string strThird_ ="(+";
		strThird_.append(starThirdEndValueAdd);
		strThird_.append(")");
		starEndThirdvalueAdd->setString(strThird_.c_str());
		starEndThirdvalueAdd->setVisible(true);

		char starFourendValueAdd[20];
		sprintf(starFourendValueAdd,"%d",additionProperty_star.at(3)->value(1));
		std::string strFour ="(+";
		strFour.append(starFourendValueAdd);
		strFour.append(")");
		starEndFourvalueAdd->setString(strFour.c_str());
		starEndFourvalueAdd->setVisible(true);
	}
}


void EquipMentUi::refreshEquipProperty_baptize(int cost)
{
	baptize_textAre_putEquipment->setVisible(false);
	if (baptizeMainEquip!=true)
	{
		baptize_textAre_putEquipment->setVisible(true);

		baptizePropertylayer->removeAllChildren();
		labelBaptizeCost->setString("0");

		propertyLock.clear();
		labelBaptizeCostStrone->setString("0");

		baptizenotBaptize_5->setVisible(false);
		baptizenotBaptize_10->setVisible(false);
		baptizenotBaptize_15->setVisible(false);
		baptizenotBaptize_20->setVisible(false);

		firstPropertyLock->setSelected(false);
		secondPropertyLock->setSelected(false);
		thirdPropertyLock->setSelected(false);
		fourPropertyLock->setSelected(false);
		fivePropertyLock->setSelected(false);

		firstPropertyLock->setVisible(false);
		secondPropertyLock->setVisible(false);
		thirdPropertyLock->setVisible(false);
		fourPropertyLock->setVisible(false);
		fivePropertyLock->setVisible(false);

		baptizeFirstLabel->setVisible(false);
		baptizeFirstvalue->setVisible(false);

		baptizesSecondLabel->setVisible(false);
		baptizeSecondvalue->setVisible(false);

		baptizeThirdLabel->setVisible(false);
		baptizeThirdvalue->setVisible(false);

		baptizeFourLabel->setVisible(false);
		baptizeFourvalue->setVisible(false);

		baptizeFiveLabel->setVisible(false);
		baptizeFivevalue->setVisible(false);
		return;
	}
	RefreshEquipData::instance()->refeshBaptize();

	int selectStateIndex = 0;
	for (int valueIndex = 0;valueIndex<additionProperty_baptize.size();valueIndex++)
	{
		baptizenotBaptize_5->setVisible(true);
		baptizenotBaptize_10->setVisible(true);
		baptizenotBaptize_15->setVisible(true);
		baptizenotBaptize_20->setVisible(true);

		selectStateIndex++;
		if (selectStateIndex>0)
		{
			firstPropertyLock->setVisible(true);
			baptizeFirstLabel->setString(additionProperty_baptize.at(0)->description().c_str());
			baptizeFirstLabel->setVisible(true);
			char baptizeFirstValue[10];
			sprintf(baptizeFirstValue,"%d",additionProperty_baptize.at(0)->value(0));
			char baptizeBeginValue[10];
			sprintf(baptizeBeginValue,"%d",additionProperty_baptize.at(0)->value(1));
			char baptizeEndValue[10];
			sprintf(baptizeEndValue,"%d",additionProperty_baptize.at(0)->value(2));
			std::string showValue = "";
			showValue.append(baptizeFirstValue);
			showValue.append("(");
			showValue.append(baptizeBeginValue);
			showValue.append("-");
			showValue.append(baptizeEndValue);
			showValue.append(")");
			baptizeFirstvalue->setString(showValue.c_str());
			baptizeFirstvalue->setVisible(true);
		}
		if (selectStateIndex>1)
		{
			secondPropertyLock->setVisible(true);
			baptizesSecondLabel->setString(additionProperty_baptize.at(1)->description().c_str());
			baptizesSecondLabel->setVisible(true);
			
			char baptizeSecondValue[10];
			sprintf(baptizeSecondValue,"%d",additionProperty_baptize.at(1)->value(0));
			char baptizeBeginValue[10];
			sprintf(baptizeBeginValue,"%d",additionProperty_baptize.at(1)->value(1));
			char baptizeEndValue[10];
			sprintf(baptizeEndValue,"%d",additionProperty_baptize.at(1)->value(2));
			std::string showValue = "";
			showValue.append(baptizeSecondValue);
			showValue.append("(");
			showValue.append(baptizeBeginValue);
			showValue.append("-");
			showValue.append(baptizeEndValue);
			showValue.append(")");
			baptizeSecondvalue->setString(showValue.c_str());
			baptizeSecondvalue->setVisible(true);

			baptizenotBaptize_5->setVisible(false);
		}
		if (selectStateIndex >2)
		{
			thirdPropertyLock->setVisible(true);

			baptizeThirdLabel->setString(additionProperty_baptize.at(2)->description().c_str());
			baptizeThirdLabel->setVisible(true);
			
			char baptizeThirdValue[10];
			sprintf(baptizeThirdValue,"%d",additionProperty_baptize.at(2)->value(0));
			char baptizeBeginValue[10];
			sprintf(baptizeBeginValue,"%d",additionProperty_baptize.at(2)->value(1));
			char baptizeEndValue[10];
			sprintf(baptizeEndValue,"%d",additionProperty_baptize.at(2)->value(2));
			std::string showValue = "";
			showValue.append(baptizeThirdValue);
			showValue.append("(");
			showValue.append(baptizeBeginValue);
			showValue.append("-");
			showValue.append(baptizeEndValue);
			showValue.append(")");
			baptizeThirdvalue->setString(showValue.c_str());
			baptizeThirdvalue->setVisible(true);

			baptizenotBaptize_10->setVisible(false);
		}
		if (selectStateIndex >3)
		{
			fourPropertyLock->setVisible(true);
			baptizeFourLabel->setString(additionProperty_baptize.at(3)->description().c_str());
			baptizeFourLabel->setVisible(true);

			char baptizeFourValue[10];
			sprintf(baptizeFourValue,"%d",additionProperty_baptize.at(3)->value(0));
			char baptizeBeginValue[10];
			sprintf(baptizeBeginValue,"%d",additionProperty_baptize.at(3)->value(1));
			char baptizeEndValue[10];
			sprintf(baptizeEndValue,"%d",additionProperty_baptize.at(3)->value(2));
			std::string showValue = "";
			showValue.append(baptizeFourValue);
			showValue.append("(");
			showValue.append(baptizeBeginValue);
			showValue.append("-");
			showValue.append(baptizeEndValue);
			showValue.append(")");

			baptizeFourvalue->setString(showValue.c_str());
			baptizeFourvalue->setVisible(true);

			baptizenotBaptize_15->setVisible(false);
		}
		if (selectStateIndex >4)
		{
			fivePropertyLock->setVisible(true);
			baptizeFiveLabel->setString(additionProperty_baptize.at(4)->description().c_str());
			baptizeFiveLabel->setVisible(true);

			char baptizeFourValue[10];
			sprintf(baptizeFourValue,"%d",additionProperty_baptize.at(4)->value(0));
			char baptizeBeginValue[10];
			sprintf(baptizeBeginValue,"%d",additionProperty_baptize.at(4)->value(1));
			char baptizeEndValue[10];
			sprintf(baptizeEndValue,"%d",additionProperty_baptize.at(4)->value(2));
			std::string showValue = "";
			showValue.append(baptizeFourValue);
			showValue.append("(");
			showValue.append(baptizeBeginValue);
			showValue.append("-");
			showValue.append(baptizeEndValue);
			showValue.append(")");

			baptizeFivevalue->setString(showValue.c_str());
			baptizeFivevalue->setVisible(true);
			
			baptizenotBaptize_20->setVisible(false);
		}
	}
	baptizeCost = cost;
	char baptizecostStr[10];
	sprintf(baptizecostStr,"%d",baptizeCost);
	labelBaptizeCost->setString(baptizecostStr);
}

void EquipMentUi::addStrengthStone()
{
	if (this->isClosing())
		return;

	switch (currType)
	{
		case REFININGTYPE:
			{
				if (refineMainEquip == true)
				{
					if (firstAssist==false || secondtAssist ==false || thirdAssist==false || fourAssist==false ||fiveAssist == false)
					{
						if (GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity()==1)
						{
							this->strengthStoneCounter(NULL);
						}else
						{
							int count_ = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity();
							GameView::getInstance()->showCounter(this,callfuncO_selector(EquipMentUi::strengthStoneCounter),count_);
						}
					}else
					{
						const char *str = StringDataManager::getString("equip_assist_enough");
						GameView::getInstance()->showAlertDialog(str);
						return;
					}
				}else
				{
					const char *str = StringDataManager::getString("equip_refine_refine");
					GameView::getInstance()->showAlertDialog(str);
				}
			}break;
		case STARTYPE :
			{
				if (GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity()==1)
				{
					this->strengthStoneCounter(NULL);
				}else
				{
					int count_ = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity();
					GameView::getInstance()->showCounter(this,callfuncO_selector(EquipMentUi::strengthStoneCounter),count_);
				}
			}break;
		case GEMTYPE:
			{
				bool isopenhole =false;
				for (int i=0;i<equipmentHoleV.size();i++)
				{
					if (equipmentHoleV.at(i)->state()==2)
					{
						isopenhole =true;
						holeIndex_ = equipmentHoleV.at(i)->id();

						isSureEquip(GEMTYPE);
						//GameMessageProcessor::sharedMsgProcessor()->sendReq(1623,(void *)holeIndex_,(void *)pageView->curPackageItemIndex);
						break;
					}
				}
				if (isopenhole == false)
				{
					const char *strings_ = StringDataManager::getString("equip_HoleNotGem");
					GameView::getInstance()->showAlertDialog(strings_);
					return;
				}
			}break;
		case BAPTIZETYPE:
			{
			}break;
	}
}

void EquipMentUi::refreshExpValue( int refineExvalue,bool isScale,bool isAction /*=true*/ )
{
	char refineCostStr[10];
	sprintf(refineCostStr,"%d",refineCost);
	labelCost->setString(refineCostStr);

	curExValue+=refineExvalue;
	if (isScale == false)
	{
		//this->setEquipBeginLevel(equipLookRefineLevel_);
	}

	if (curExValue <0)
	{
		isAction = false;
		int equipUseLevel;
		if (mainEquipIsPac == true)
		{
			auto equipRefine =(StrengthEquip *)refineLayer->getChildByTag(REFININGMAINEQUIP);
			equipUseLevel = equipRefine->getRefineUseLevel();
		}else
		{
			auto generalEquipBegin = (GeneralEquipRecover *)refineLayer->getChildByTag(GENERALREFINEEQUIP);
			equipUseLevel = generalEquipBegin->getEquipUseLevel();
		}
		exPercent_refine->setVisible(true);
		for (int i=equipLookRefineLevel_;i>0;i--)
		{
			if (curExValue <0)
			{
				auto equipCurExp_ = StrengthEquipmentConfigData::s_EquipUpLevelExp[i];
				curExValue += (equipCurExp_->get_exp()* (equipUseLevel+1));
				allExValue = equipCurExp_->get_exp()*(equipUseLevel+1);

				equipLookRefineLevel_--;
			}
		}
		if (mainEquipIsPac == true)
		{
			auto equipRefine =(StrengthEquip *)refineLayer->getChildByTag(REFININGMAINEQUIP);//REFINELOOKEQUIP/
			
			if (equipLookRefineLevel_ >equipRefine->getRefineLevel())
			{
				refineEndFirstvalueAdd->setVisible(true);
				refineEndSecondvalueAdd->setVisible(true);
			}else
			{
				refineEndFirstvalueAdd->setVisible(false);
				refineEndSecondvalueAdd->setVisible(false);
			}
			equipRefine->setRefineLevel(equipLookRefineLevel_);
		}else
		{
			auto generalEquipBegin = (GeneralEquipRecover *)refineLayer->getChildByTag(GENERALREFINEEQUIPLOOK);
			generalEquipBegin->setRefineLevel(equipLookRefineLevel_);

			if (equipLookRefineLevel_ >generalEquipBegin->getRefineLevel())
			{
				refineEndFirstvalueAdd->setVisible(true);
				refineEndSecondvalueAdd->setVisible(true);
			}else
			{
				refineEndFirstvalueAdd->setVisible(false);
				refineEndSecondvalueAdd->setVisible(false);
			}
		}
	}

	if (curExValue >= allExValue)
	{
		int equipUseLevel;
		int equipCurRefineLevel_;
		int equipNextRefineLevel_;
		if (mainEquipIsPac == true)
		{
			auto equipRefine =(StrengthEquip *)refineLayer->getChildByTag(REFININGMAINEQUIP);
			equipUseLevel = equipRefine->getRefineUseLevel();
			equipCurRefineLevel_ = equipLookRefineLevel_;
			equipNextRefineLevel_ = equipCurRefineLevel_+1;
		}else
		{
			auto generalEquipBegin = (GeneralEquipRecover *)refineLayer->getChildByTag(GENERALREFINEEQUIP);
			equipUseLevel = generalEquipBegin->getEquipUseLevel();
			equipCurRefineLevel_ = equipLookRefineLevel_;
			equipNextRefineLevel_ = equipCurRefineLevel_+1;
		}

		for (int i = equipNextRefineLevel_;i<=20 ;i++)
		{
			if (curExValue >= allExValue)
			{
				equipLookRefineLevel_++;
				if (i+1>20)
				{
					break;
				}
				auto equipExp = StrengthEquipmentConfigData::s_EquipUpLevelExp[equipLookRefineLevel_+1];
				allExValue += equipExp->get_exp() *(equipUseLevel+1);
			}else
			{
				break;
			}
		}
		exPercent_refine->setVisible(false);

		for (int i=equipLookRefineLevel_ ;i>equipCurRefineLevel_;i--)
		{
			auto equipCurExp_ = StrengthEquipmentConfigData::s_EquipUpLevelExp[i];
			curExValue  -= equipCurExp_->get_exp()*(equipUseLevel/1+1);
		}
		if (equipLookRefineLevel_+1 >= 21)
		{
			auto equipExp = StrengthEquipmentConfigData::s_EquipUpLevelExp[20];
			allExValue= equipExp->get_exp()*(equipUseLevel/1+1);
		}else
		{
			auto equipExp = StrengthEquipmentConfigData::s_EquipUpLevelExp[equipLookRefineLevel_+1];
			allExValue= equipExp->get_exp()*(equipUseLevel/1+1);
		}
	}

	if (equipLookRefineLevel_ > 0)
	{
		getRefineAdditionPropertValue(equipLookRefineLevel_);
	}
	
	
	if (mainEquipIsPac == true)
	{
		auto equipRefine =(StrengthEquip *)refineLayer->getChildByTag(REFINELOOKEQUIP);
		if (equipLookRefineLevel_ >getEquipBeginLevel())
		{
			refineEndFirstvalueAdd->setVisible(true);
			refineEndSecondvalueAdd->setVisible(true);
		}else
		{
			refineEndFirstvalueAdd->setVisible(false);
			refineEndSecondvalueAdd->setVisible(false);
		}
		equipRefine->setRefineLevel(equipLookRefineLevel_);
		
	}else
	{
		auto generalEquipBegin = (GeneralEquipRecover *)refineLayer->getChildByTag(GENERALREFINEEQUIPLOOK);
		if (equipLookRefineLevel_ > generalEquipBegin->getRefineLevel())
		{
			refineEndFirstvalueAdd->setVisible(true);
			refineEndSecondvalueAdd->setVisible(true);
		}else
		{
			refineEndFirstvalueAdd->setVisible(false);
			refineEndSecondvalueAdd->setVisible(false);
		}
		generalEquipBegin->setRefineLevel(equipLookRefineLevel_);
	}
	
	char exBegin_[10];
	sprintf(exBegin_,"%d",curExValue);
	char exEnd_[10];
	sprintf(exEnd_,"%d",allExValue);

	if (equipLookRefineLevel_ >= 20)
	{
		std::string experScroll="";
		experScroll.append(exEnd_);
		experScroll.append("/");
		experScroll.append(exEnd_);
		exLabel_refine->setString(experScroll.c_str());
	}else
	{
		std::string experScroll="";
		experScroll.append(exBegin_);
		experScroll.append("/");
		experScroll.append(exEnd_);
		exLabel_refine->setString(experScroll.c_str());
	}

	float scaleBegin_ = curExValue*1.0f;
	float scaleEnd_ = allExValue*1.0f;
	float scaleEx =scaleBegin_/scaleEnd_;
	exPercent_refineAdd->setVisible(true);
	if (isScale == true)
	{
		exPercent_refine->setVisible(true);
		if (isAction == true)
		{
			int m_speed= 80;
			int m_times = equipLookRefineLevel_ - this->getEquipBeginLevel();
			if (m_times > 0)
			{
				auto to1 = CCProgressTo::create((100 - exPercent_refine->getPercentage())*1.0f/m_speed,100);
				auto to2 = CCProgressTo::create(100/m_speed,100);
				auto repeat1 = Repeat::create(to2,m_times - 1);
				auto to3 = CCProgressTo::create(100*scaleEx/m_speed,100*scaleEx);
				Sequence * sequence_;
				if (m_times >1)
				{
					sequence_ = Sequence::create(
						CallFunc::create(CC_CALLBACK_0(EquipMentUi::addExPercentSetPrecentIsFull,this)),
						to1,
						repeat1,
						CallFunc::create(CC_CALLBACK_0(EquipMentUi::addExPercentSetPrecentIsCur,this)),
						to3,
						NULL);
				}else
				{
					sequence_ = Sequence::create(to1,to3,NULL);
				}
				sequence_->setTag(1501);
				exPercent_refine->runAction(sequence_);
			}else
			{
				auto to2 = CCProgressTo::create((100*scaleEx - exPercent_refine->getPercentage())*1.0f/m_speed,100*scaleEx);
				exPercent_refine->runAction(to2);
			}
			//this->setEquipBeginLevel(equipLookRefineLevel_);
		}else
		{
			exPercent_refine->setPercentage(scaleEx*100);
			exPercent_refineAdd->setScaleX(scaleEx);
		}

	}else
	{
		if (equipLookRefineLevel_ >= 20)
		{
			exPercent_refineAdd->setScaleX(0.98f);
		}else
		{
			exPercent_refineAdd->setScaleX(scaleEx);
		}
	}
}

void EquipMentUi::getRefineAdditionPropertValue( int equipLookLevel )
{
	int firstLookAddValue = 0;
	int secondLookAddValue = 0;

	for (int i=getEquipBeginLevel();i<equipLookLevel;i++)
	{
		float factor = 0.0;
		int addValueFirst =0;
		int addValueSecond = 0;
		std::vector<CAdditionProperty *> AddPropertyVector;
		if (mainEquipIsPac == true)
		{
			auto equipRefine =(StrengthEquip *)refineLayer->getChildByTag(REFININGMAINEQUIP);
			AddPropertyVector =EquipRefineAddValueConfigData::getAddvalueByDiffClazzAndLevel(equipRefine->getClazz(),i+1);

			EquipRefineFactorConfigData::useLevelAndQuality temp;
			temp.useLevel = equipRefine->getRefineUseLevel();
			temp.quality = equipRefine->getQuality();
			auto valueFactor = EquipRefineFactorConfigData::s_EquipRefineValueFactor[temp];
			factor = valueFactor->get_factor();
		}else
		{
			auto generalEquipBegin = (GeneralEquipRecover *)refineLayer->getChildByTag(GENERALREFINEEQUIP);
			AddPropertyVector =EquipRefineAddValueConfigData::getAddvalueByDiffClazzAndLevel(generalEquipBegin->getClazz(),i+1);

			EquipRefineFactorConfigData::useLevelAndQuality temp;
			temp.useLevel = generalEquipBegin->getEquipUseLevel();
			temp.quality = generalEquipBegin->getQuality();
			auto valueFactor = EquipRefineFactorConfigData::s_EquipRefineValueFactor[temp];
			factor = valueFactor->get_factor();
		}

		for (int i =0;i<AddPropertyVector.size();i++)
		{
			if (additionProperty.at(0)->property() == AddPropertyVector.at(i)->property())
			{
				addValueFirst = AddPropertyVector.at(i)->value(1);
			}

			if (additionProperty.at(1)->property() == AddPropertyVector.at(i)->property())
			{
				addValueSecond = AddPropertyVector.at(i)->value(1);
			}
		}

		firstLookAddValue =firstLookAddValue+ addValueFirst*factor +1;
		secondLookAddValue = secondLookAddValue+ addValueSecond*factor +1;
	}

	char endFirstValueAdd[10];
	sprintf(endFirstValueAdd,"%d",firstLookAddValue);
	std::string strFirst_ = "(+";
	strFirst_.append(endFirstValueAdd);
	strFirst_.append(")");
	refineEndFirstvalueAdd->setString(strFirst_.c_str());

	char endSecondValueAdd[10];
	sprintf(endSecondValueAdd,"%d",secondLookAddValue);
	std::string strSecond_ ="(+";
	strSecond_.append(endSecondValueAdd);
	strSecond_.append(")");
	refineEndSecondvalueAdd->setString(strSecond_.c_str());
}

void EquipMentUi::refreshStarLuckValue( int curluckval,int starCostvale,bool isAction )
{
	char curluckStr[20];
	sprintf(curluckStr,"%d",curluckval);

	int maxLuck_ =10000;
	char *maxluckStr = new char[20] ;
	sprintf(maxluckStr,"%d",maxLuck_);

	std::string luckScroll="";
	luckScroll.append(curluckStr);
	luckScroll.append("/");
	luckScroll.append(maxluckStr);
	exLabel_star->setString(luckScroll.c_str());

	float cur_ =curluckval;
	float all_ = maxLuck_;
	if (cur_/all_ >= 1)
	{
		if (isAction == true)
		{
			exPercent_star->runAction(ScaleTo::create(0.2f,1,1));
		}else
		{
			exPercent_star->setScaleX(1);
		}
	}else
	{
		if (isAction == true)
		{
			if (cur_ ==0 && this->getCurEquipExStar() >0)
			{
				Sequence * exAction = Sequence::create(ScaleTo::create(0.2f,1.0f,1.0f),
															ScaleTo::create(0.01f,0,1.0f),
															NULL);

				exPercent_star->runAction(exAction);
			}else
			{
				exPercent_star->runAction(ScaleTo::create(0.2f,cur_/all_,1.0f));
			}
			
		}else
		{
			exPercent_star->setScaleX(cur_/all_);
		}
	}
	
	starCost =starCostvale;
	char costStr[20];
	sprintf(costStr,"%d",starCost);
	starCost_label->setString(costStr);
}

void EquipMentUi::strengthStoneCounter(Ref * obj)
{
	auto counter_ =(Counter *)obj;
	int amount_;
	if (counter_ != NULL)
	{
		amount_ =counter_->getInputNum();
	}else
	{
		amount_ =1;
	}
	
	if(amount_ <= 0)
	{
		return;
	}


	if (amount_ >GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity())
	{
		const char *string_ = StringDataManager::getString("mail_country");
		GameView::getInstance()->showAlertDialog(string_);
		return;
	}
	
	switch(currType)
	{
	case REFININGTYPE:
		{
			//如果超过最大精炼等级 则默认为最大个数
			int tempMaxEx = 0;
			auto mapStone = StrengthStoneConfigData::s_StrengthStrone[GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->goods().id()];
			int addEx =  mapStone->get_exp();

			int  equipUseLevel = 1;
			if (mainEquipIsPac == true)
			{
				auto equipRefine =(StrengthEquip *)refineLayer->getChildByTag(REFININGMAINEQUIP);
				equipUseLevel = equipRefine->getRefineUseLevel();
			}else
			{
				auto generalEquipBegin = (GeneralEquipRecover *)refineLayer->getChildByTag(GENERALREFINEEQUIP);
				equipUseLevel = generalEquipBegin->getEquipUseLevel();
			}

			int floderId_ = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->id();
			for (int assvectorIndex =0;assvectorIndex<assStoneVector.size();assvectorIndex++)
			{
				if (floderId_ == assStoneVector.at(assvectorIndex).index_)
				{
					this->getBackMainEquip(assStoneVector.at(assvectorIndex).tag_,floderId_);
				}
			}

			///
			int limitEquipmentLevel = 20;
			std::string goodsId = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->goods().id();
			for (int i=equipLookRefineLevel_;i<20 ;i++ )
			{
				std::string m_stoneId = "";
				EquipStrengthStoneLevelConfigData::typeAndLevle temp_typeAndlevel;
				temp_typeAndlevel.type_ = 1;
				temp_typeAndlevel.level_ = i+1;
				auto mapStrength_ = EquipStrengthStoneLevelConfigData::s_EquipStrengthUseStone[temp_typeAndlevel];
				if(mapStrength_ != NULL)
				{
					m_stoneId = mapStrength_->get_stoneId();
				}

				if(!(strcmp(m_stoneId.c_str(),goodsId.c_str())==0))
				{
					limitEquipmentLevel = i;
					break;
				}
			}
			for (int i = equipLookRefineLevel_;i<limitEquipmentLevel;i++ )
			{
				auto equipCurExp_ = StrengthEquipmentConfigData::s_EquipUpLevelExp[i+1];
				tempMaxEx += equipCurExp_->get_exp()*(equipUseLevel/1+1);
			}
			tempMaxEx -= curExValue;
			if (addEx*amount_ > tempMaxEx)
			{
				int tempAmount =  tempMaxEx/addEx;
				if (tempMaxEx%addEx > 0)
				{
					tempAmount+=1;
				}
				stoneOfAmountRefine=tempAmount;
			}else
			{
				stoneOfAmountRefine=amount_;
			}

			if (stoneOfAmountRefine > 0)
			{
				stoneOfIndexRefine = pageView->curPackageItemIndex;
				this->addAssistEquipment(GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex));


				int goodsCount_ = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity();

				char goodsUseNum[50];
				sprintf(goodsUseNum,"%d",stoneOfAmountRefine);

				char goodsAllNum[50];
				sprintf(goodsAllNum,"%d",goodsCount_);

				std::string goodsUseAndCount = goodsUseNum;
				goodsUseAndCount.append("/");
				goodsUseAndCount.append(goodsAllNum);

				GameView::getInstance()->pacPageView->SetCurFolderGray(true,pageView->curPackageItemIndex,goodsUseAndCount);	
			}else
			{
				if (equipLookRefineLevel_ >=20)
				{
					const char *string_ = StringDataManager::getString("refineLevelIsFull");
					GameView::getInstance()->showAlertDialog(string_);
				}else
				{
					std::string m_stoneId = "";
					std::string m_stoneName = "";
					std::string m_limitLevel = "";
					EquipStrengthStoneLevelConfigData::typeAndLevle temp_typeAndlevel;
					temp_typeAndlevel.type_ = 1;
					temp_typeAndlevel.level_ = equipLookRefineLevel_+1;
					auto mapStrength_ = EquipStrengthStoneLevelConfigData::s_EquipStrengthUseStone[temp_typeAndlevel];
					if(mapStrength_ != NULL)
					{
						m_stoneId = mapStrength_->get_stoneId();
						m_stoneName = mapStrength_->get_stoneName();
						m_limitLevel = mapStrength_->get_limitLevel();
					}

					const char *strings_1= StringDataManager::getString("equip_refineStone_isNotEnough_first");
					const char *strings_2= StringDataManager::getString("equip_refineStone_isNotEnough_second");
					const char *strings_3= StringDataManager::getString("equip_refineStone_isNotEnough_third");

					std::string stoneNotEnough = strings_1;
					stoneNotEnough.append(m_limitLevel);
					stoneNotEnough.append(strings_2);
					stoneNotEnough.append(m_stoneName);
					stoneNotEnough.append(strings_3);
					GameView::getInstance()->showAlertDialog(stoneNotEnough.c_str());
				}
			}
			
		}break;
	case STARTYPE:
		{
			if (StrengthStone == true)
			{
				auto strengthequip = (StrengthEquip *)starPropertyLayer->getChildByTag(STONELAYERTAG);
				if (strengthequip != NULL)
				{
					this->getBackMainEquip(STONELAYERTAG,strengthequip->packIndex);
				}
			}
			StrengthStone=true;

			int starUseLevel = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->goods().uselevel();
			if ((70*amount_* starUseLevel)/(curMainEquipStarLevel*curMainEquipStarLevel*curMainEquipStarLevel*curMainEquipStarLevel+1)>=1)
			{
				stoneOfAmountStar = (curMainEquipStarLevel*curMainEquipStarLevel*curMainEquipStarLevel*curMainEquipStarLevel+1)/(starUseLevel*70);
				if ((curMainEquipStarLevel*curMainEquipStarLevel*curMainEquipStarLevel*curMainEquipStarLevel+1)%(starUseLevel*70)>0)
				{
					stoneOfAmountStar+=1;
				}
			}else
			{
				stoneOfAmountStar=amount_;
			}
			stoneOfIndexStar = pageView->curPackageItemIndex;
			auto strengthequip= StrengthEquip::create(GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex),false);
			strengthequip->setIgnoreAnchorPointForPosition(false);
			strengthequip->setAnchorPoint(Vec2(0,0));
			strengthequip->setPosition(Vec2(80,108.5f));
			strengthequip->setScale(0.8f);
			strengthequip->setTag(STONELAYERTAG);
			starPropertyLayer->addChild(strengthequip);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1606,(void *)stoneOfIndexStar,(void *)stoneOfAmountStar);
			
			int goodsCount_ = GameView::getInstance()->AllPacItem.at(stoneOfIndexStar)->quantity();
			char goodsUseNum[50];
			sprintf(goodsUseNum,"%d",stoneOfAmountStar);
			char goodsAllNum[50];
			sprintf(goodsAllNum,"%d",goodsCount_);
			std::string goodsUseAndCount = goodsUseNum;
			goodsUseAndCount.append("/");
			goodsUseAndCount.append(goodsAllNum);
			GameView::getInstance()->pacPageView->SetCurFolderGray(true,pageView->curPackageItemIndex,goodsUseAndCount);	
		}break;
	case GEMTYPE:
		{
			
		}break;
	case BAPTIZETYPE:
		{

		}break;
	}
}

void EquipMentUi::refreshGeneralEquipment(std::vector<CEquipment *>equipmentsVector)
{
	/*
	roleLayer->removeAllChildren();
	std::vector<CEquipment*>::iterator iter;
	for (iter=generalEquipVector.begin();iter!=generalEquipVector.end();iter++)
	{
		delete * iter;
	}
	generalEquipVector.clear();
	int equipWeapId = 0;
	bool isWeapEquip =false;
	int generalEquipNum = equipmentsVector.size();
	for (int i=0;i<generalEquipNum;i++)
	{
		int partIndex = equipmentsVector.at(i)->part();
		CEquipment * equip_ =new CEquipment();
		equip_->CopyFrom(*equipmentsVector.at(i));
		
		GeneralEquipMent * equip =GeneralEquipMent::create(equip_);
		equip->setIgnoreAnchorPointForPosition(false);
		equip->setAnchorPoint(Vec2(0,0));
		equip->setTag(GENERALEQUIPMENTLIST+i);
		roleLayer->addChild(equip);
		generalEquipVector.push_back(equip_);
		
		if (equip_->part()==4)
		{
			generalWeapTag = equip->getTag();
			isWeapEquip =true;
		}
	}

	if (isWeapEquip ==false)
	{
		generalWeapTag = equipWeapId;
	}
	*/
}

void EquipMentUi::refreshGemValue(bool backSame)
{
	gem_textArea_putEquipment->setVisible(false);
	gemLayer->removeAllChildren();
	auto layerGem_ = (Layer *)gemLayer->getChildByTag(SHOWGEMVALUELAYER);
	if (layerGem_)
	{
		layerGem_->removeFromParentAndCleanup(true);
	}
	//是否返回初始化
	if (backSame ==true)
	{
		gem_textArea_putEquipment->setVisible(true);
		//const char *str_ = StringDataManager::getString("equip_gem_hole");
		const char *strings_ = StringDataManager::getString("equip_HoleFirstInputEquip");

		holeLabelFirst->setString(strings_);
		holeLabelFirst->setVisible(true);
		imageLock1->loadTexture("res_ui/suo.png");
		imageLock1->setVisible(true);
		//gemBackGround_1->setVisible(false);
		gem_buttonHole_first->setVisible(false);
		gem_buttonPick1->setVisible(false);

		holeLabelSecond->setString(strings_);
		holeLabelSecond->setVisible(true);
		imageLock2->loadTexture("res_ui/suo.png");
		imageLock2->setVisible(true);
		//gemBackGround_2->setVisible(false);
		gem_buttonHole_second->setVisible(false);
		gem_buttonPick2->setVisible(false);

		holeLabelThird->setString(strings_);
		holeLabelThird->setVisible(true);
		imageLock3->loadTexture("res_ui/suo.png");
		imageLock3->setVisible(true);
		//gemBackGround_3->setVisible(false);
		gem_buttonHole_third->setVisible(false);
		gem_buttonPick3->setVisible(false);
		/*
		holeStoneCostLabel->setString("0");

		char holeAmountBingStr[20];
		sprintf(holeAmountBingStr,"%d",this->getCurHoleStoneAmount(1));
		holeStoneAmoumtBingdingLabel->setString(holeAmountBingStr);

		char holeAmountNotBingStr[20];
		sprintf(holeAmountNotBingStr,"%d",this->getCurHoleStoneAmount(0));
		holeStoneAmountNotBingdingLabel->setString(holeAmountNotBingStr);

		holeCostLabelGem->setString("0");
		*/
		return;
	}
	RefreshEquipData::instance()->refreshGem();

	holeFalg=0;

	auto gemShowLayer =Layer::create();
	gemShowLayer->setIgnoreAnchorPointForPosition(false);
	gemShowLayer->setAnchorPoint(Vec2(0.5f,0.5f));
	gemShowLayer->setPosition(Vec2(gemLayer->getContentSize().width/2,gemLayer->getContentSize().height/2));
	gemShowLayer->setTag(SHOWGEMVALUELAYER);
	gemShowLayer->setContentSize(Size(800,480));
	gemLayer->addChild(gemShowLayer);

	for (int i=0;i<equipmentHoleV.size();i++)
	{
		//get already  hole num
		int punchIndex_ = equipmentHoleV.at(i)->id();
		int equipState_ = equipmentHoleV.at(i)->state();

		if (equipState_!=1)
		{
			holeFalg++;
		}

		switch(punchIndex_)
		{
		case 0:
			{
				//hole_id
				switch(equipState_)
				{
				case 1:
					{
						imageLock1->setVisible(true);
						gem_buttonPick1->setVisible(false);
						gem_buttonHole_first->setVisible(true);
// 						const char *strings_ = StringDataManager::getString("equip_gemGoodsInfoHole");
// 						holeLabelFirst->setString(strings_);
						holeLabelFirst->setVisible(false);
						//gemBackGround_1->setVisible(false);
						this->addGemEquipCurHole(0);
					}break;
				case 2:
					{
						imageLock1->setVisible(false);
						gem_buttonPick1->setVisible(false);
						gem_buttonHole_first->setVisible(false);
						const char *strings_ = StringDataManager::getString("equip_gemGoodsInfoStar");
						holeLabelFirst->setString(strings_);
						holeLabelFirst->setVisible(true);
						//gemBackGround_1->setVisible(false);
					}break;
				case 3:
					{
						holeLabelFirst->setVisible(false);
						gem_buttonHole_first->setVisible(false);
						gem_buttonPick1->setVisible(true);
						//gemBackGround_1->setVisible(true);
						this->addGemEquipment(0);
					}break;
				}
			}break;
		case 1:
			{
				switch(equipState_)
				{
				case 1:
					{
						imageLock2->setVisible(true);
						gem_buttonPick2->setVisible(false);
						//gemBackGround_2->setVisible(false);
						if (holeFalg > 0)
						{
							holeLabelSecond->setVisible(false);
							gem_buttonHole_second->setVisible(true);
							this->addGemEquipCurHole(1);
						}else
						{
							const char *strings_ = StringDataManager::getString("equip_gemGoodsInfoHole");
							holeLabelSecond->setString(strings_);
							holeLabelSecond->setVisible(true);
							gem_buttonHole_second->setVisible(false);
						}
					}break;
				case 2:
					{
						imageLock2->setVisible(false);
						gem_buttonPick2->setVisible(false);
						gem_buttonHole_second->setVisible(false);
						const char *strings_ = StringDataManager::getString("equip_gemGoodsInfoStar");
						holeLabelSecond->setString(strings_);
						holeLabelSecond->setVisible(true);
						//gemBackGround_2->setVisible(false);
					}break;
				case 3:
					{
						gem_buttonPick2->setVisible(true);
						gem_buttonHole_second->setVisible(false);
						holeLabelSecond->setVisible(false);
						//gemBackGround_2->setVisible(true);
						this->addGemEquipment(1);
					}break;
				}
			}break;
		case 2:
			{
				switch(equipState_)
				{
				case 1:
					{
						imageLock3->setVisible(true);
						gem_buttonPick3->setVisible(false);
						//gemBackGround_3->setVisible(false);
						if (holeFalg >1)
						{
							this->addGemEquipCurHole(2);
							holeLabelThird->setVisible(false);
							gem_buttonHole_third->setVisible(true);
						}else
						{
							const char *strings_ = StringDataManager::getString("equip_gemGoodsInfoHole");
							holeLabelThird->setString(strings_);
							holeLabelThird->setVisible(true);
							gem_buttonHole_third->setVisible(false);
						}
					}break;
				case 2:
					{
						imageLock3->setVisible(false);;
						gem_buttonPick3->setVisible(false);
						gem_buttonHole_third->setVisible(false);
						const char *strings_ = StringDataManager::getString("equip_gemGoodsInfoStar");
						holeLabelThird->setString(strings_);
						holeLabelThird->setVisible(true);
						//gemBackGround_3->setVisible(false);
					}break;
				case 3:
					{
						gem_buttonPick3->setVisible(true);
						gem_buttonHole_third->setVisible(false);
						holeLabelThird->setVisible(false);
						//gemBackGround_3->setVisible(true);
						this->addGemEquipment(2);
					}break;
				}
			}break;
		}
	}
	/*
	int holeCost;
	if (holeFalg>2)
	{
		holeCost = 0;
	}else
	{
		holeCost = (holeFalg+1)*(holeFalg+1)*(holeFalg+1);
	}
	
	char holeStr[20];
	sprintf(holeStr,"%d",holeCost);
	holeStoneCostLabel->setString(holeStr);

	char holeAmountBingStr[20];
	sprintf(holeAmountBingStr,"%d",this->getCurHoleStoneAmount(1));
	holeStoneAmoumtBingdingLabel->setString(holeAmountBingStr);

	char holeAmountNotBingStr[20];
	sprintf(holeAmountNotBingStr,"%d",this->getCurHoleStoneAmount(0));
	holeStoneAmountNotBingdingLabel->setString(holeAmountNotBingStr);

	char holeCostStr[20];
	sprintf(holeCostStr,"%d",holeCost*1000);
	holeCostLabelGem->setString(holeCostStr);
	*/
}
/*
void EquipMentUi::equipMentInlaySure( Ref * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1623,(void *)holeIndex_,(void *)pageView->curPackageItemIndex);
}
*/
void EquipMentUi::equipFirstPickSure(Ref * obj)
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1624,(void *)0);
}
void EquipMentUi::equipSecondpickSure(Ref * obj)
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1624,(void *)1);
}

void EquipMentUi::equipThirdpickSure(Ref * obj)
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1624,(void *)2);
}

//////////teach 
void EquipMentUi::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void EquipMentUi::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,60,50);
// 	tutorialIndicator->setPosition(Vec2(pos.x+60+_w,pos.y+80+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator,50);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,104,43);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+_w,pos.y+_h+1));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void EquipMentUi::addCCTutorialIndicator2( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,87,39);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+227+_w,pos.y+19.5f+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void EquipMentUi::addCCTutorialIndicatorClose( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	tutorialIndicator->setPosition(Vec2(pos.x-60+_w,pos.y-75+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator,50);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,40,40,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void EquipMentUi::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void EquipMentUi::equipMentSureBaptize( Ref * obj )
{
	int select_ = 0;
	if (useBaptizeBingding->isSelected() == true)
	{
		select_ = 1;
		if (getBaptizeClock(0) >propertyLock.size())
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1611,this,(void *)select_);
		}else
		{
			if (getBaptizeClock(1) > 0)
			{
				const char *string_ = StringDataManager::getString("equip_baptizeClockOfBingding_BingdingEquip");
				GameView::getInstance()->showPopupWindow(string_,2,this, CC_CALLFUNCO_SELECTOR(EquipMentUi::isSureNotBingingBaptize),NULL);
			}else
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1611,this,(void *)select_);
			}
		}
	}else
	{
		select_ = 0;
		int mainEquip = 0;
		if (mainEquipIsPac ==true)
		{
			mainEquip = GameView::getInstance()->AllPacItem.at(mainEquipIndexRefine)->goods().binding();
		}else
		{
			for (int j =0;j< curMainEquipvector.size();j++)
			{
				if (generalEquipOfPart == curMainEquipvector.at(j)->part())
				{
					mainEquip = curMainEquipvector.at(j)->goods().binding();
				}
			}
		}

		if (getBaptizeClock(1) > 0 && mainEquip == 0)
		{
			const char *string_ = StringDataManager::getString("equip_baptizeClockOfBingding_BingdingEquip_isSure");
			GameView::getInstance()->showPopupWindow(string_,2,this,CC_CALLFUNCO_SELECTOR(EquipMentUi::isSureBingingBaptize),NULL);
		}else
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1611,this,(void *)select_);
		}
	}
}

void EquipMentUi::addOperationFloder(FolderInfo * floders)
{
	auto goods_ = new GoodsInfo();
	goods_->CopyFrom(floders->goods());
	switch(currType)
	{
	case REFININGTYPE:
		{
			if (refineMainEquip ==true)
			{
				if (floders->goods().clazz() ==GOODS_CLASS_JINGLIANCAILIAO )
				{
					if (equipLookRefineLevel_ < 20)
					{
						this->addStrengthStone();
					}else
					{
						const char *string_ = StringDataManager::getString("refineLevelIsFull");
						GameView::getInstance()->showAlertDialog(string_);
					}
					
				}else
				{
					if (equipLookRefineLevel_ < 20)
					{
						this->addAssistEquipment(floders);
					}else
					{
						const char *string_ = StringDataManager::getString("refineLevelIsFull");
						GameView::getInstance()->showAlertDialog(string_);
					}
				}
			}else
			{
				if (floders->goods().equipmentclazz() > 0 &&floders->goods().equipmentclazz()<10 )
				{
					this->addMainEquipment(floders);
				}else
				{
					const char *str = StringDataManager::getString("equip_refine_refine");
					GameView::getInstance()->showAlertDialog(str);
				}
			}
		}break;
	case STARTYPE:
		{
			if (starMainEquip ==true)
			{
				if (floders->goods().clazz() == GOODS_CLASS_SHENGXINGSHI )
				{
					this->addStrengthStone();
				}else
				{
					if (mainEquipIsPac == true)
					{
						this->getBackMainEquip(STAREQUIP,mainEquipIndexRefine);
					}else
					{
						this->getBackGeneralMainEquip(GENERALSTAREQUIP);
					}

					if (floders->goods().equipmentclazz() > 0 &&floders->goods().equipmentclazz()<10)
					{
						this->addMainEquipment(floders);
					}
				}
			}else
			{
				if (floders->goods().equipmentclazz() > 0 &&floders->goods().equipmentclazz()<10)
				{
					this->addMainEquipment(floders);
				}else
				{
					const char *str = StringDataManager::getString("equip_star_pick");
					GameView::getInstance()->showAlertDialog(str);
				}
			}





			/*
			if (starMainEquip ==true)
			{
				if (floders->goods().clazz() == 25 )
				{
					this->addStrengthStone();
				}else
				{
					const char *strings_ = StringDataManager::getString("equip_getBackMainPlease");
					GameView::getInstance()->showAlertDialog(strings_);
				}
			}else
			{
				if (floders->goods().equipmentclazz() > 0 &&floders->goods().equipmentclazz()<10)
				{
					this->addMainEquipment(floders);
				}else
				{
					const char *str = StringDataManager::getString("equip_star_pick");
					GameView::getInstance()->showAlertDialog(str);
				}
			}
			*/
		}break;
	case GEMTYPE:
		{
			if (gemMainEquip ==true)
			{
				if (floders->goods().clazz() == GOODS_CLASS_XIANGQIANCAILIAO )
				{
					this->addStrengthStone();
					return;
				}else
				{
					if (mainEquipIsPac == true)
					{
						this->getBackMainEquip(GEMEQUIP,mainEquipIndexRefine);
					}else
					{
						this->getBackGeneralMainEquip(GENERALGEMEQUIP);
					}
				}
			}

			if (floders->goods().equipmentclazz() > 0 &&floders->goods().equipmentclazz()<10)
			{
				this->addMainEquipment(floders);
			}else
			{
				const char *strings_ = StringDataManager::getString("equip_gem_gem");
				GameView::getInstance()->showAlertDialog(strings_);
			}
			

			/*
			if (gemMainEquip ==true)
			{
				if (floders->goods().clazz() == 3 )
				{
					this->addStrengthStone();
				}else
				{
					const char *strings_ = StringDataManager::getString("equip_getBackMainPlease");
					GameView::getInstance()->showAlertDialog(strings_);
				}
			}else
			{
				if (floders->goods().equipmentclazz() > 0 &&floders->goods().equipmentclazz()<10)
				{
					this->addMainEquipment(floders);
				}else
				{
					const char *strings_ = StringDataManager::getString("equip_gem_gem");
					GameView::getInstance()->showAlertDialog(strings_);
				}
			}
			*/
		}break;
	case BAPTIZETYPE:
		{
			if (baptizeMainEquip ==true)
			{
				if (mainEquipIsPac == true)
				{
					this->getBackMainEquip(BAPTIZEEQUIP,mainEquipIndexRefine);
				}else
				{
					this->getBackGeneralMainEquip(GENERALBAPTIZEEQUIP);
				}
			}
			
			if (this->isCanBaptize(goods_) == true)
			{
				this->addMainEquipment(floders);
			}else
			{
				const char *strings_ = StringDataManager::getString("equip_isBaptizeOffalse");
				GameView::getInstance()->showAlertDialog(strings_);
			}

			/*
			if (baptizeMainEquip ==true)
			{
				const char *strings_ = StringDataManager::getString("equip_getBackMainPlease");
				GameView::getInstance()->showAlertDialog(strings_);
			}else
			{
				if (this->isCanBaptize(goods_) == true)
				{
					this->addMainEquipment(floders);
				}else
				{
					const char *strings_ = StringDataManager::getString("equip_isBaptizeOffalse");
					GameView::getInstance()->showAlertDialog(strings_);
				}
			}
			*/
		}break;
	}
	delete goods_;
}

void EquipMentUi::addRoleEquipment( CEquipment * equips )
{
	auto goos_ =new GoodsInfo();
	goos_->CopyFrom(equips->goods());
	switch(currType)
	{
	case REFININGTYPE:
		{
			if (refineMainEquip ==true)
			{
				/*
				if (equipLookRefineLevel_ < 20)
				{
					this->addAssistGeneralOfEquip(equips);
				}else
				{
					const char *string_ = StringDataManager::getString("refineLevelIsFull");
					GameView::getInstance()->showAlertDialog(string_);
				}
				*/

				if (mainEquipIsPac == true)
				{
					this->getBackMainEquip(REFININGMAINEQUIP,mainEquipIndexRefine);
				}else
				{
					this->getBackGeneralMainEquip(GENERALREFINEEQUIP);
				}
			}

			equipmentInstanceid = equips->goods().instanceid();
			if (goos_->equipmentdetail().gradelevel()<20)
			{
				this->addMainEquipOfGeneral(equips);
			}else
			{
				const char *string_ = StringDataManager::getString("refineLevelIsFull");
				GameView::getInstance()->showAlertDialog(string_);
			}

		}break;
	case STARTYPE:
		{
			if (starMainEquip ==true)
			{
				if (mainEquipIsPac == true)
				{
					this->getBackMainEquip(STAREQUIP,mainEquipIndexRefine);
				}else
				{
					this->getBackGeneralMainEquip(GENERALSTAREQUIP);
				}
			}
			equipmentInstanceid = equips->goods().instanceid();
			if (goos_->equipmentdetail().starlevel()<20)
			{
				this->addMainEquipOfGeneral(equips);
			}else
			{
				const char *string_ = StringDataManager::getString("starLevelIsFull");
				GameView::getInstance()->showAlertDialog(string_);
			}


			/*
			if (starMainEquip ==true)
			{
				const char *strings_ = StringDataManager::getString("equip_getBackMainPlease");
				GameView::getInstance()->showAlertDialog(strings_);	
			}else
			{
				if (goos_->equipmentdetail().starlevel()<20)
				{
					this->addMainEquipOfGeneral(equips);
				}else
				{
					const char *string_ = StringDataManager::getString("starLevelIsFull");
					GameView::getInstance()->showAlertDialog(string_);
				}
			}
			*/
		}break;
	case GEMTYPE:
		{
			if (gemMainEquip ==true)
			{
				if (mainEquipIsPac == true)
				{
					this->getBackMainEquip(GEMEQUIP,mainEquipIndexRefine);
				}else
				{
					this->getBackGeneralMainEquip(GENERALGEMEQUIP);
				}
			}

			equipmentInstanceid = equips->goods().instanceid();
			this->addMainEquipOfGeneral(equips);

			/*
			if (gemMainEquip ==true)
			{
				const char *strings_ = StringDataManager::getString("equip_getBackMainPlease");
				GameView::getInstance()->showAlertDialog(strings_);
			}else
			{
				this->addMainEquipOfGeneral(equips);
			}
			*/
		}break;
	case BAPTIZETYPE:
		{
			if (baptizeMainEquip ==true)
			{
				if (mainEquipIsPac == true)
				{
					this->getBackMainEquip(BAPTIZEEQUIP,mainEquipIndexRefine);
				}else
				{
					this->getBackGeneralMainEquip(GENERALBAPTIZEEQUIP);
				}
			}
			equipmentInstanceid = equips->goods().instanceid();
			if (this->isCanBaptize(goos_) == true)
			{
				this->addMainEquipOfGeneral(equips);
			}else
			{
				const char *strings_ = StringDataManager::getString("equip_isBaptizeOffalse");
				GameView::getInstance()->showAlertDialog(strings_);
			}
			
			/*
			if (baptizeMainEquip ==true)
			{
				const char *strings_ = StringDataManager::getString("equip_getBackMainPlease");
				GameView::getInstance()->showAlertDialog(strings_);
			}else
			{
				if (this->isCanBaptize(goos_) == true)
				{
					this->addMainEquipOfGeneral(equips);
				}else
				{
					const char *strings_ = StringDataManager::getString("equip_isBaptizeOffalse");
					GameView::getInstance()->showAlertDialog(strings_);
				}
			}
			*/
		}break;
	}
	delete goos_;
}

void EquipMentUi::refreshBackPack( int typeUi,int equipStoneLevel)
{
	long long currentTimeBegin = GameUtils::millisecondNow();
	int size = GameView::getInstance()->AllPacItem.size();

	switch(typeUi)
	{
	case REFININGTYPE:
		{
			std::string m_stoneId = "";
			EquipStrengthStoneLevelConfigData::typeAndLevle temp_typeAndlevel;
			temp_typeAndlevel.type_ = 1;
			temp_typeAndlevel.level_ = equipStoneLevel+1;
			MapStrengthUseStone * mapStrength_ = EquipStrengthStoneLevelConfigData::s_EquipStrengthUseStone[temp_typeAndlevel];
			if(mapStrength_ != NULL)
			{
				m_stoneId = mapStrength_->get_stoneId();
			}
			for (int i =0;i<size;i++)
			{
				if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
				{
					int curClazz = GameView::getInstance()->AllPacItem.at(i)->goods().clazz();
					int equipsIndex = GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz();
					std::string goodsId = GameView::getInstance()->AllPacItem.at(i)->goods().id();

					if (refineMainEquip == false)
					{
						if (equipsIndex >0 &&equipsIndex < 10)
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(true);
						}else
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(false);
						}
					}else
					{
						if (curClazz == GOODS_CLASS_JINGLIANCAILIAO || (equipsIndex >0 &&equipsIndex < 10))
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(true);
							if (curClazz == 24 && !(strcmp(m_stoneId.c_str(),goodsId.c_str())==0))
							{
								GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(false);
							}
						}else
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(false);
						}
					}
				}
			}
		}break;
	case STARTYPE:
		{
			std::string m_stoneId = "";
			EquipStrengthStoneLevelConfigData::typeAndLevle temp_typeAndlevel;
			temp_typeAndlevel.type_ = 2;
			temp_typeAndlevel.level_ = equipStoneLevel+1;
			MapStrengthUseStone * mapStrength_ = EquipStrengthStoneLevelConfigData::s_EquipStrengthUseStone[temp_typeAndlevel];
			if(mapStrength_ != NULL)
			{
				m_stoneId = mapStrength_->get_stoneId();
			}

			for (int i =0;i<size;i++)
			{
				if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
				{
					int curClazz = GameView::getInstance()->AllPacItem.at(i)->goods().clazz();
					int equipsIndex = GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz();
					std::string goodsId = GameView::getInstance()->AllPacItem.at(i)->goods().id();

					if (starMainEquip == false)
					{
						if ((equipsIndex >0 &&equipsIndex < 10) )
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(true);
						}else
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(false);
						}
					}else
					{
						if ( curClazz == GOODS_CLASS_SHENGXINGSHI && (strcmp(m_stoneId.c_str(),goodsId.c_str())==0))
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(true);
						}else
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(false);
						}
					}
				}
			}
		}break;
	case GEMTYPE:
		{
			for (int i =0;i<size;i++)
			{
				if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
				{
					int curClazz = GameView::getInstance()->AllPacItem.at(i)->goods().clazz();
					int equipsIndex = GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz();

					if (gemMainEquip == false)
					{
						if (!(equipsIndex >0 &&equipsIndex < 10))
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(false);
						}else
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(true);
						}
					}else
					{
						if ( curClazz != GOODS_CLASS_XIANGQIANCAILIAO)
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(false);
						}else
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(true);
							GameView::getInstance()->pacPageView->getPackageItem(i)->setBoundJewelPropertyVisible(true);
						}
					}
				}
			}
		}break;
	case BAPTIZETYPE:
		{
			for (int i =0;i<size;i++)
			{
				if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
				{
					int equipsIndex = GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz();
					auto goods = new GoodsInfo();
					goods->CopyFrom(GameView::getInstance()->AllPacItem.at(i)->goods());

					if (baptizeMainEquip == false)
					{
						if (!(equipsIndex >0 &&equipsIndex < 10) )
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(false);
						}else
						{
							GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(true);
						}
					}else
					{
						GameView::getInstance()->pacPageView->getPackageItem(i)->setAvailable(false);
					}
					delete goods;
				}
			}
		}break;
	}

	if (refineMainEquip == true ||starMainEquip ==true || gemMainEquip ==true || baptizeMainEquip ==true)
	{
		if (mainEquipIsPac)
		{
			GameView::getInstance()->pacPageView->SetCurFolderGray(true,mainEquipIndexRefine);
		}
	}

	CCLOG("refreshBackage cost time: %d", GameUtils::millisecondNow() - currentTimeBegin);
}

bool EquipMentUi::isCanBaptize(GoodsInfo * goods )
{
	bool isBaptize =false;
	int size = goods->equipmentdetail().properties_size();
	for (int i=0;i<size;i++)
	{
		if (goods->equipmentdetail().properties(i).type()==3)
		{
			isBaptize=true;
		}
	}
	if (isBaptize==true)
	{
		return true;
	}else
	{
		return false;
	}
}

CEquipment * EquipMentUi::getCurRoleEquipment( int index )
{
	for (int i =0; i< GameView::getInstance()->EquipListItem.size();i++)
	{
		long long tempInstaceid = GameView::getInstance()->EquipListItem.at(i)->goods().instanceid();
		if (index == tempInstaceid)
		{
			return GameView::getInstance()->EquipListItem.at(i);
		}
	}
	
	for (int j = 0;j< GameView::getInstance()->generalsInLineDetailList.size();j++)
	{
		for (int tempIndex =0;tempIndex < GameView::getInstance()->generalsInLineDetailList.at(j)->equipments_size();tempIndex++)
		{
			long long tempInstaceid = GameView::getInstance()->generalsInLineDetailList.at(j)->equipments(tempIndex).goods().instanceid();
			if (index == tempInstaceid)
			{
				auto equipment_ =new CEquipment();
				equipment_->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(j)->equipments(tempIndex));
				return equipment_;
			}
		}
	}

	/*
	//equipmentInstanceid
	for (int j=0;j<generalEquipVector.size();j++)
	{
		GeneralEquipMent * starGeneralEquip = (GeneralEquipMent*)roleLayer->getChildByTag(GENERALEQUIPMENTLIST + j);
		if (index == starGeneralEquip->Btn_pacItemFrame->getTag())
		{
			return starGeneralEquip->generalEquipment;
		}
	}
	*/
}

int EquipMentUi::getCurHoleStoneAmount(int typeBing)
{
	int size = GameView::getInstance()->AllPacItem.size();
	int holeNum = 0;
	for (int i =0;i<size;i++)
	{
		if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
		{
			int curClazz = GameView::getInstance()->AllPacItem.at(i)->goods().clazz();
			int bingding = GameView::getInstance()->AllPacItem.at(i)->goods().binding();
			if ( curClazz == 27 && bingding == typeBing)
			{
				holeNum += GameView::getInstance()->AllPacItem.at(i)->quantity();
			}
		}
	}
	return holeNum;
}


void EquipMentUi::initRefine()
{
	refineLayer=Layer::create();
	refineLayer->setIgnoreAnchorPointForPosition(false);
	refineLayer->setAnchorPoint(Vec2(0.5f,0.5f));
	refineLayer->setPosition(Vec2(size.width/2,size.height/2));
	refineLayer->setTag(REFINEPROPERTYLAYER);
	refineLayer->setContentSize(Size(800,480));
	this->addChild(refineLayer);
	//touch is des 
	auto btn_mainBefore = (Button *)Helper::seekWidgetByName(equipPanel,"Button_main_refine_before");
	btn_mainBefore->setTouchEnabled(true);
	//btn_mainBefore->setPressedActionEnabled(true);
	btn_mainBefore->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackRefineMainDes, this));

	auto btn_Assist_1 = (Button *)Helper::seekWidgetByName(equipPanel,"Button_refine_minor_1");
	btn_Assist_1->setTouchEnabled(true);
	//btn_Assist_1->setPressedActionEnabled(true);
	btn_Assist_1->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackAssistEquipDes, this));

	auto btn_Assist_2 = (Button *)Helper::seekWidgetByName(equipPanel,"Button_refine_minor_2");
	btn_Assist_2->setTouchEnabled(true);
	btn_Assist_2->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackAssistEquipDes, this));

	auto btn_Assist_3 = (Button *)Helper::seekWidgetByName(equipPanel,"Button_refine_minor_3");
	btn_Assist_3->setTouchEnabled(true);
	btn_Assist_3->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackAssistEquipDes, this));

	auto btn_Assist_4 = (Button *)Helper::seekWidgetByName(equipPanel,"Button_refine_minor_4");
	btn_Assist_4->setTouchEnabled(true);
	btn_Assist_4->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackAssistEquipDes, this));

	auto btn_Assist_5 = (Button *)Helper::seekWidgetByName(equipPanel,"Button_refine_minor_5");
	btn_Assist_5->setTouchEnabled(true);
	btn_Assist_5->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackAssistEquipDes, this));
	//------------
	refine_textAreaPutEquipment = (ImageView *)Helper::seekWidgetByName(equipPanel,"refine_Image_putEquipment");
	refine_textAreaPutEquipment->setVisible(true);

	//labelFont_curRefinelevel = (Text*)Helper::seekWidgetByName(equipPanel,"Label_exp_0");
	//labelFont_curRefinelevel->setString("0");
	

	ImageView * exPercent_refine1=(ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_exp_refine");
	exPercent_refine1->setScaleX(0);
	//refine  state 
	image_state_refine_mainEquip = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_refineMainEquip");
	image_state_first_assist = (ImageView *)Helper::seekWidgetByName(equipPanel,"refine_first_cailiao");
	image_state_second_assist = (ImageView *)Helper::seekWidgetByName(equipPanel,"refine_second_cailiao");
	image_state_third_assist = (ImageView *)Helper::seekWidgetByName(equipPanel,"refine_third_cailiao");
	image_state_four_assist = (ImageView *)Helper::seekWidgetByName(equipPanel,"refine_four_cailiao");
	image_state_five_assist = (ImageView *)Helper::seekWidgetByName(equipPanel,"refine_five_cailiao");
	///
	exPercent_refine = ProgressTimer::create(Sprite::create("res_ui/jingyantiao2.png"));
	exPercent_refine->setType(ProgressTimer::Type::BAR);
	exPercent_refine->setMidpoint(Vec2(0,0));
	exPercent_refine->setBarChangeRate(Vec2(1, 0));
	m_Layer->addChild(exPercent_refine);
	exPercent_refine->setAnchorPoint(Vec2(0,0));
	exPercent_refine->setPosition(Vec2(171, 117));
	exPercent_refine->setPercentage(0);

	exPercent_refineAdd = (ImageView *)Helper::seekWidgetByName(equipPanel,"image_exp_refineAdd");
	auto  action1 = FadeTo::create(0.5f,140);
	auto  action2 = FadeTo::create(0.5f,255);
	auto repeapAction = RepeatForever::create(Sequence::create(action1,action2,NULL));
	exPercent_refineAdd->runAction(repeapAction);
	exPercent_refineAdd->setScaleX(0);
	//refine label
	auto exLabel_refine1=(Text*)Helper::seekWidgetByName(equipPanel,"Label_exp_refine");
	exLabel_refine1->setVisible(false);
	
	exLabel_refine= Label::createWithTTF("0/0", APP_FONT_NAME,16);
	exLabel_refine->setAnchorPoint(Vec2(0.5f,0.5f));
	exLabel_refine->setPosition(Vec2(265,124.5f));
	m_Layer->addChild(exLabel_refine);

	labelCost =(Text*)Helper::seekWidgetByName(equipPanel,"Label_refineCost");
	labelCost->setString("0");

	refineBeginFirstLabel = (Text*)Helper::seekWidgetByName(equipPanel,"refineLabelBeginFirst");
	refineBeginFirstLabel->setVisible(false);
	refineBeginSecondLabel = (Text*)Helper::seekWidgetByName(equipPanel,"refinelabelBeginSecond");
	refineBeginSecondLabel->setVisible(false);
	refineBeginFirstvalue = (Text*)Helper::seekWidgetByName(equipPanel,"refineBeginFirstValue");
	refineBeginFirstvalue->setVisible(false);
	refineBeginSecondvalue = (Text*)Helper::seekWidgetByName(equipPanel,"refineBeginSecondValue");
	refineBeginSecondvalue->setVisible(false);
	refineEndFirstLabel = (Text*)Helper::seekWidgetByName(equipPanel,"refineEndFirstLabel");
	refineEndFirstLabel->setVisible(false);
	refineEndSecondLabel = (Text*)Helper::seekWidgetByName(equipPanel,"refineEndSecondLabel");
	refineEndSecondLabel->setVisible(false);
	refineEndFirstvalue = (Text*)Helper::seekWidgetByName(equipPanel,"refineEndFirstValue");
	refineEndFirstvalue->setVisible(false);
	refineEndSecondvalue= (Text*)Helper::seekWidgetByName(equipPanel,"refineEndSecondValue");
	refineEndSecondvalue->setVisible(false);
	refineEndFirstvalueAdd = (Text*)Helper::seekWidgetByName(equipPanel,"refineEndFirstValueAdd");
	refineEndFirstvalueAdd->setVisible(false);
	refineEndSecondvalueAdd = (Text*)Helper::seekWidgetByName(equipPanel,"refineEndSecondValueAdd");
	refineEndSecondvalueAdd->setVisible(false);
	btn_Refine=(Button *)Helper::seekWidgetByName(equipPanel,"Button_refining");
	btn_Refine->setPressedActionEnabled(true);
	btn_Refine->setTouchEnabled(true);
	btn_Refine->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackBtn_Refine, this));
	btn_addAll=(Button *)Helper::seekWidgetByName(equipPanel,"Button_all");
	btn_addAll->setPressedActionEnabled(true);
	btn_addAll->setTouchEnabled(true);
	btn_addAll->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackBtn_addAll, this));
}

void EquipMentUi::initStar()
{
	starPropertyLayer=Layer::create();
	starPropertyLayer->setIgnoreAnchorPointForPosition(false);
	starPropertyLayer->setAnchorPoint(Vec2(0.5f,0.5f));
	starPropertyLayer->setPosition(Vec2(size.width/2,size.height/2));
	starPropertyLayer->setTag(STARPROPERTYLAYER);
	starPropertyLayer->setContentSize(Size(800,480));
	this->addChild(starPropertyLayer);
	starPropertyLayer->setVisible(false);

	star_textArea_putEquipment = (ImageView *)Helper::seekWidgetByName(equipPanel,"star_Image_putEquip");

	//touch is des 
	auto btn_star_main_Before = (Button *)Helper::seekWidgetByName(equipPanel,"Button_star_main_before");
	btn_star_main_Before->setTouchEnabled(true);
	//btn_star_main_Before->setPressedActionEnabled(true);
	btn_star_main_Before->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackStarMainDes, this));

	auto btn_Assist_star = (Button *)Helper::seekWidgetByName(equipPanel,"Button_star_shengxingshi");
	btn_Assist_star->setTouchEnabled(true);
	btn_Assist_star->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackStarAssistDes, this));

	//cur state
	image_state_star_MainEquip = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_star_mainEquip");
	image_state_star_stone = (ImageView *)Helper::seekWidgetByName(equipPanel,"star_stone_cailiao");
	
	starBeginFirstLabel = (Text*)Helper::seekWidgetByName(equipPanel,"starBeginFirstLabel");
	starBeginFirstLabel->setVisible(false);
	starBeginSecondLabel = (Text*)Helper::seekWidgetByName(equipPanel,"starBeginSecondLabel");
	starBeginSecondLabel->setVisible(false);
	starBeginThirdLabel = (Text*)Helper::seekWidgetByName(equipPanel,"starBeginThirdLabel");
	starBeginThirdLabel->setVisible(false);
	starBeginFourLabel = (Text*)Helper::seekWidgetByName(equipPanel,"starBeginFourLabel");
	starBeginFourLabel->setVisible(false);

	starBeginFirstvalue = (Text*)Helper::seekWidgetByName(equipPanel,"starBeginFirstVale");
	starBeginFirstvalue->setVisible(false);
	starBeginSecondvalue = (Text*)Helper::seekWidgetByName(equipPanel,"starBeginSecondVale");
	starBeginSecondvalue->setVisible(false);
	starBeginThirdvalue = (Text*)Helper::seekWidgetByName(equipPanel,"starBeginThirdVale");
	starBeginThirdvalue->setVisible(false);
	starBeginFourvalue = (Text*)Helper::seekWidgetByName(equipPanel,"starBeginFourVale");
	starBeginFourvalue->setVisible(false);

	starEndFirstLabel = (Text*)Helper::seekWidgetByName(equipPanel,"starEndFirstlabel");
	starEndFirstLabel->setVisible(false);
	starEndSecondLabel = (Text*)Helper::seekWidgetByName(equipPanel,"starEndSecondlabel");
	starEndSecondLabel->setVisible(false);
	starEndThirdLabel = (Text*)Helper::seekWidgetByName(equipPanel,"starEndThirdlabel");
	starEndThirdLabel->setVisible(false);
	starFourFourLabel = (Text*)Helper::seekWidgetByName(equipPanel,"starEndFourlabel");
	starFourFourLabel->setVisible(false);

	starEndFirstvalue = (Text*)Helper::seekWidgetByName(equipPanel,"starEndFirstValue");
	starEndFirstvalue->setVisible(false);
	starEndSecondvalue= (Text*)Helper::seekWidgetByName(equipPanel,"starEndSecondValue");
	starEndSecondvalue->setVisible(false);
	starEndThirdvalue= (Text*)Helper::seekWidgetByName(equipPanel,"starEndThirdValue");
	starEndThirdvalue->setVisible(false);
	starEndFourvalue= (Text*)Helper::seekWidgetByName(equipPanel,"starEndFourValue");
	starEndFourvalue->setVisible(false);

	starEndFirstvalueAdd= (Text*)Helper::seekWidgetByName(equipPanel,"starEndFirstvalueAdd");
	starEndFirstvalueAdd->setVisible(false);
	starEndSecondvalueAdd= (Text*)Helper::seekWidgetByName(equipPanel,"starEndSecondvalueAdd");
	starEndSecondvalueAdd->setVisible(false);
	starEndThirdvalueAdd= (Text*)Helper::seekWidgetByName(equipPanel,"starEndThirdvalueAdd");
	starEndThirdvalueAdd->setVisible(false);
	starEndFourvalueAdd= (Text*)Helper::seekWidgetByName(equipPanel,"starEndFourvalueAdd");
	starEndFourvalueAdd->setVisible(false);

	exPercent_star=(ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_exp_star");
	exPercent_star->setScaleX(0);
	exLabel_star=(Text*)Helper::seekWidgetByName(equipPanel,"Label_exp_star");
	exLabel_star->enableOutline(Color4B::BLACK, 2.0f);
	exLabel_star->setString("0/0");

	starCost_label =(Text*)Helper::seekWidgetByName(equipPanel,"Label_starConst");
	starCost_label->setString("0");

	successRate =(Text*)Helper::seekWidgetByName(equipPanel,"Label_success_rate_date");
	successRate->setString("0");

	auto btn_des = (Button *)Helper::seekWidgetByName(equipPanel,"Button_star_des_temp");
	btn_des->setTouchEnabled(true);
	btn_des->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackStarDes, this));

	btn_upStar=(Button *)Helper::seekWidgetByName(equipPanel,"Button_up_star");
	btn_upStar->setPressedActionEnabled(true);
	btn_upStar->setTouchEnabled(true);
	btn_upStar->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackUpstar, this));

	btn_allStar=(Button *)Helper::seekWidgetByName(equipPanel,"Button_pick_star");
	btn_allStar->setPressedActionEnabled(true);
	btn_allStar->setTouchEnabled(true);
	btn_allStar->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackAllStar,this));
}

void EquipMentUi::initGem()
{
	gemLayer=Layer::create();
	gemLayer->setIgnoreAnchorPointForPosition(false);
	gemLayer->setAnchorPoint(Vec2(0.5f,0.5f));
	gemLayer->setPosition(Vec2(size.width/2,size.height/2));
	gemLayer->setTag(GEMPROPERTYLAYER);
	gemLayer->setContentSize(Size(800,480));
	this->addChild(gemLayer);
	gemLayer->setVisible(false);

	//cur state remind of label
	gem_textArea_putEquipment = (ImageView * )Helper::seekWidgetByName(equipPanel,"gem_Image_putEquip");
	//cur state remind of image
	image_state_gem_mainEquip= (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_gem_mainEquip");

	//touch is des 
	auto btn_gem_main_Before = (Button *)Helper::seekWidgetByName(equipPanel,"Button_gem_main_equip");
	btn_gem_main_Before->setTouchEnabled(true);
	//btn_gem_main_Before->setPressedActionEnabled(true);
	btn_gem_main_Before->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackGemMainDes, this));

	auto btn_Assist_gem1 = (Button *)Helper::seekWidgetByName(equipPanel,"Button_gem_1");
	btn_Assist_gem1->setTouchEnabled(true);
	btn_Assist_gem1->setTag(0);
	btn_Assist_gem1->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackGemAssistDes, this));

	auto btn_Assist_gem2 = (Button *)Helper::seekWidgetByName(equipPanel,"Button_gem_2");
	btn_Assist_gem2->setTouchEnabled(true);
	btn_Assist_gem2->setTag(1);
	btn_Assist_gem2->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackGemAssistDes,this));

	auto btn_Assist_gem3 = (Button *)Helper::seekWidgetByName(equipPanel,"Button_gem_3");
	btn_Assist_gem3->setTouchEnabled(true);
	btn_Assist_gem3->setTag(2);
	btn_Assist_gem3->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackGemAssistDes,this));
	//-----------

	imageLock1 =(ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_gemLock_1");
	imageLock2 =(ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_gemLock2");
	imageLock3 =(ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_gemLock3");

	holeLabelFirst = (Text*)Helper::seekWidgetByName(equipPanel,"holeLabelFirst");
	//holeLabelFirst->setTextAreaSize(Size(100,0));
	holeLabelSecond = (Text*)Helper::seekWidgetByName(equipPanel,"holeLabelSecond");
	//holeLabelSecond->setTextAreaSize(Size(100,0));
	holeLabelThird = (Text*)Helper::seekWidgetByName(equipPanel,"holeLabelThird");
	//holeLabelThird->setTextAreaSize(Size(100,0));
	/*
	//cost hole of stone
	holeStoneCostLabel = (Text*)Helper::seekWidgetByName(equipPanel,"Label_depletionValue");
	holeStoneCostLabel->setString("0");
	//pay money 
	holeCostLabelGem = (Text*)Helper::seekWidgetByName(equipPanel,"Label_payCoinsValue");
	holeCostLabelGem->setString("0");
	*/

	/*-----------------------------
	gemBackGround_1 = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_first_gemSp");
	gemBackGround_1->setVisible(true);
	gemBackGround_1->setPosition(Vec2(100,100));

	gemBackGround_2 = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_secondGemSp");
	gemBackGround_2->setVisible(false);
	gemBackGround_3 = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_thirdGemSp");
	gemBackGround_3->setVisible(false);
	*/--------------------------------
	/*
	//rem hole stone
	char holeAmountBingStr[20];
	sprintf(holeAmountBingStr,"%d",this->getCurHoleStoneAmount(1));
	holeStoneAmoumtBingdingLabel = (Text*)Helper::seekWidgetByName(equipPanel,"Label_mineValue");
	holeStoneAmoumtBingdingLabel->setString(holeAmountBingStr);

	char holeAmountNotBingStr[20];
	sprintf(holeAmountNotBingStr,"%d",this->getCurHoleStoneAmount(0));
	holeStoneAmountNotBingdingLabel = (Text*)Helper::seekWidgetByName(equipPanel,"Label_mineOffValue");
	holeStoneAmountNotBingdingLabel->setString(holeAmountNotBingStr);

	useGemBingding = (UICheckBox *)Helper::seekWidgetByName(equipPanel,"CheckBox88");
	useGemBingding->setTouchEnabled(true);
	useGemBingding->addEventListenerCheckBox(this,checkboxselectedeventselector(EquipMentUi::setGemBingding));
	useGemBingding->setSelected(false);
	*/
	gem_buttonHole_first=(Button *)Helper::seekWidgetByName(equipPanel,"Button_gemHole_first");
	gem_buttonHole_first->setPressedActionEnabled(true);
	gem_buttonHole_first->setTouchEnabled(true);
	gem_buttonHole_first->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackHole,this));
	gem_buttonHole_first->setVisible(false);

	gem_buttonHole_second=(Button *)Helper::seekWidgetByName(equipPanel,"Button_gemHole_second");
	gem_buttonHole_second->setPressedActionEnabled(true);
	gem_buttonHole_second->setTouchEnabled(true);
	gem_buttonHole_second->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackHole,this));
	gem_buttonHole_second->setVisible(false);

	gem_buttonHole_third=(Button *)Helper::seekWidgetByName(equipPanel,"Button_gemHole_third");
	gem_buttonHole_third->setPressedActionEnabled(true);
	gem_buttonHole_third->setTouchEnabled(true);
	gem_buttonHole_third->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackHole, this));
	gem_buttonHole_third->setVisible(false);

	gem_buttonPick1=(Button *)Helper::seekWidgetByName(equipPanel,"Button_pickGem_first");
	gem_buttonPick1->setPressedActionEnabled(true);
	gem_buttonPick1->setTouchEnabled(true);
	gem_buttonPick1->setVisible(false);
	gem_buttonPick1->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::firstButtonEvent, this));

	gem_buttonPick2=(Button *)Helper::seekWidgetByName(equipPanel,"Button_pickGem_second");
	gem_buttonPick2->setPressedActionEnabled(true);
	gem_buttonPick2->setTouchEnabled(true);
	gem_buttonPick2->setVisible(false);
	gem_buttonPick2->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::secondButtonEvent, this));

	gem_buttonPick3=(Button *)Helper::seekWidgetByName(equipPanel,"Button_pickGemThird");
	gem_buttonPick3->setPressedActionEnabled(true);
	gem_buttonPick3->setTouchEnabled(true);
	gem_buttonPick3->setVisible(false);
	gem_buttonPick3->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::thirdButtonEvent,this));
}

void EquipMentUi::initBaptize()
{
	baptizePropertylayer=Layer::create();
	baptizePropertylayer->setIgnoreAnchorPointForPosition(false);
	baptizePropertylayer->setAnchorPoint(Vec2(0.5f,0.5f));
	baptizePropertylayer->setPosition(Vec2(size.width/2,size.height/2));
	baptizePropertylayer->setTag(BAPTIZEPROPERTYLAYER);
	baptizePropertylayer->setContentSize(Size(800,480));
	this->addChild(baptizePropertylayer);
	baptizePropertylayer->setVisible(false);

	baptize_textAre_putEquipment = (ImageView *)Helper::seekWidgetByName(equipPanel,"baptize_Image_putEquip");

	//cur state
	image_state_baptize_mainEquip = (ImageView *)Helper::seekWidgetByName(equipPanel,"ImageView_baptize_mainequip");

	//touch is des 
	auto btn_gem_main_Before = (Button *)Helper::seekWidgetByName(equipPanel,"Button_purification_main_equip");
	btn_gem_main_Before->setTouchEnabled(true);
	//btn_gem_main_Before->setPressedActionEnabled(true);
	btn_gem_main_Before->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackBaptizeMainDes, this));


	auto btn_baptize= (Button *)Helper::seekWidgetByName(equipPanel,"Button_purification");
	btn_baptize->setPressedActionEnabled(true);
	btn_baptize->setTouchEnabled(true);
	btn_baptize->addTouchEventListener(CC_CALLBACK_2(EquipMentUi::callBackBaptize, this));

	///not baptize
	baptizenotBaptize_5= (Text*)Helper::seekWidgetByName(equipPanel,"Label_refineLevel_open5");
	baptizenotBaptize_5->setVisible(false);
	baptizenotBaptize_10= (Text*)Helper::seekWidgetByName(equipPanel,"Label_refineLevel_open10");
	baptizenotBaptize_10->setVisible(false);
	baptizenotBaptize_15= (Text*)Helper::seekWidgetByName(equipPanel,"Label_refineLevel_open15");
	baptizenotBaptize_15->setVisible(false);
	baptizenotBaptize_20= (Text*)Helper::seekWidgetByName(equipPanel,"Label_refineLevel_open20");
	baptizenotBaptize_20->setVisible(false);

	firstPropertyLock =(CheckBox *)Helper::seekWidgetByName(equipPanel,"CheckBox1");
	firstPropertyLock->setTouchEnabled(true);
	firstPropertyLock->setSelected(false);
	firstPropertyLock->addEventListener(CC_CALLBACK_2(EquipMentUi::setBaptizeFirstCheckBox,this));
	firstPropertyLock->setVisible(false);

	secondPropertyLock =(CheckBox *)Helper::seekWidgetByName(equipPanel,"CheckBox2");
	secondPropertyLock->setTouchEnabled(true);
	secondPropertyLock->setSelected(false);
	secondPropertyLock->addEventListener(CC_CALLBACK_2(EquipMentUi::setBaptizeSecondCheckBox,this));
	secondPropertyLock->setVisible(false);

	thirdPropertyLock =(CheckBox *)Helper::seekWidgetByName(equipPanel,"CheckBox3");
	thirdPropertyLock->setTouchEnabled(true);
	thirdPropertyLock->setSelected(false);
	thirdPropertyLock->addEventListener(CC_CALLBACK_2(EquipMentUi::setBaptizeThirdCheckBox,this));
	thirdPropertyLock->setVisible(false);

	fourPropertyLock =(CheckBox *)Helper::seekWidgetByName(equipPanel,"CheckBox");
	fourPropertyLock->setTouchEnabled(true);
	fourPropertyLock->setSelected(false);
	fourPropertyLock->addEventListener(CC_CALLBACK_2(EquipMentUi::setBaptizeFourCheckBox,this));
	fourPropertyLock->setVisible(false);

	fivePropertyLock =(CheckBox *)Helper::seekWidgetByName(equipPanel,"CheckBox_4959");
	fivePropertyLock->setTouchEnabled(true);
	fivePropertyLock->setSelected(false);
	fivePropertyLock->addEventListener(CC_CALLBACK_2(EquipMentUi::setBaptizeFiveCheckBox,this));
	fivePropertyLock->setVisible(false);
	
	baptizeFirstLabel = (Text*)Helper::seekWidgetByName(equipPanel,"baptizeFirstLabel");
	baptizeFirstLabel->setVisible(false);
	baptizesSecondLabel= (Text*)Helper::seekWidgetByName(equipPanel,"baptizeSecondLabel");
	baptizesSecondLabel->setVisible(false);
	baptizeThirdLabel= (Text*)Helper::seekWidgetByName(equipPanel,"baptizeThirdLabel");
	baptizeThirdLabel->setVisible(false);
	baptizeFourLabel = (Text*)Helper::seekWidgetByName(equipPanel,"baptizeFourLabel");
	baptizeFourLabel->setVisible(false);
	baptizeFiveLabel = (Text*)Helper::seekWidgetByName(equipPanel,"baptizeFourLabel_0");
	baptizeFiveLabel->setVisible(false);
	
	baptizeFirstvalue= (Text*)Helper::seekWidgetByName(equipPanel,"baptizeFirstVale");
	baptizeFirstvalue->setVisible(false);
	baptizeSecondvalue= (Text*)Helper::seekWidgetByName(equipPanel,"baptizeSecondVale");
	baptizeSecondvalue->setVisible(false);
	baptizeThirdvalue= (Text*)Helper::seekWidgetByName(equipPanel,"baptizeThirdVale");
	baptizeThirdvalue->setVisible(false);
	baptizeFourvalue= (Text*)Helper::seekWidgetByName(equipPanel,"baptizeFourVale");
	baptizeFourvalue->setVisible(false);
	baptizeFivevalue= (Text*)Helper::seekWidgetByName(equipPanel,"baptizeFourVale_0");
	baptizeFivevalue->setVisible(false);
	
	labelBaptizeCost =(Text*)Helper::seekWidgetByName(equipPanel,"Label_PurificationConst");
	labelBaptizeCost->setString("0");

	labelBaptizeCostStrone =(Text*)Helper::seekWidgetByName(equipPanel,"Label_PurificationConst_0");
	labelBaptizeCostStrone->setString("0");
	
	useBaptizeBingding = (CheckBox *)Helper::seekWidgetByName(equipPanel,"CheckBox_0");
	useBaptizeBingding->setTouchEnabled(true);
	useBaptizeBingding->addEventListener(CC_CALLBACK_2(EquipMentUi::setBaptizeDingding,this));
	useBaptizeBingding->setSelected(false);

}

void EquipMentUi::initSynthesis()
{
	synthesisLayer=Layer::create();
	synthesisLayer->setIgnoreAnchorPointForPosition(false);
	synthesisLayer->setAnchorPoint(Vec2(0.5f,0.5f));
	synthesisLayer->setPosition(Vec2(size.width/2,size.height/2));
	synthesisLayer->setContentSize(Size(800,480));
	this->addChild(synthesisLayer);
	synthesisLayer->setVisible(false);

	auto systhesisiUI = SysthesisUI::create();
	synthesisLayer->addChild(systhesisiUI);
	systhesisiUI->setTag(CLASS_SYSTHESIS_TAG);
}

void EquipMentUi::addGemEquipment(int holeIndex)
{
	Layer * gemShowLayer =(Layer *)gemLayer->getChildByTag(SHOWGEMVALUELAYER);
	//show gem 
	std::string goodsIcon_ = equipmentHoleV.at(holeIndex)->propicon();
	std::string goodsName_ = equipmentHoleV.at(holeIndex)->propname();
	std::string goodsEffect_ = equipmentHoleV.at(holeIndex)->effect();

	int quality_ = equipmentHoleV.at(holeIndex)->quality();
	std::string bgPath_ = getBackGroundPathByGoodsQulity(quality_);

	auto imageBg = ImageView::create();
	imageBg->loadTexture(bgPath_.c_str());
	imageBg->setAnchorPoint(Vec2(0.5f,0.5f));
	imageBg->setScale(0.85f);
	gemShowLayer->addChild(imageBg);

	std::string goodsInfoStr ="res_ui/props_icon/";
	goodsInfoStr.append(goodsIcon_);
	goodsInfoStr.append(".png");

	auto imageStarIcon = ImageView::create();
	imageStarIcon->loadTexture(goodsInfoStr.c_str());
	imageStarIcon->setAnchorPoint(Vec2(0.5f,0.5f));
	imageStarIcon->setPosition(Vec2(0,0));
	imageBg->addChild(imageStarIcon);

	//Color3B color_ = GameView::getInstance()->getGoodsColorByQuality(quality_);
	Color3B color_ = Color3B(47,95,15);

	auto labelGemName = Label::createWithTTF(goodsName_.c_str(), APP_FONT_NAME, 16);
	labelGemName->setAnchorPoint(Vec2(0.5f,0.5f));
	labelGemName->setColor(color_);
	gemShowLayer->addChild(labelGemName);

	auto labelGemEffecct = Label::createWithTTF(goodsEffect_.c_str(), APP_FONT_NAME, 16);
	labelGemEffecct->setAnchorPoint(Vec2(0.5f,0.5f));
	labelGemEffecct->setColor(color_);
	gemShowLayer->addChild(labelGemEffecct);

	switch(holeIndex)
	{
	case 0:
		{
			//gemBackGround_1->setVisible(true);
			imageBg->setPosition(Vec2(101,239));
			labelGemName->setPosition(Vec2(200,250));
			labelGemEffecct->setPosition(Vec2(200,225));
		}break;
	case 1:
		{
			//gemBackGround_2->setVisible(true);
			imageBg->setPosition(Vec2(101,157));
			labelGemName->setPosition(Vec2(200,166));
			labelGemEffecct->setPosition(Vec2(200,140));
		}break;
	case 2:
		{
			//gemBackGround_3->setVisible(true);
			imageBg->setPosition(Vec2(101,74.5f));
			labelGemName->setPosition(Vec2(200,80));;
			labelGemEffecct->setPosition(Vec2(200,55));
		}break;
	}
}

void EquipMentUi::addGemEquipCurHole( int holeIndex )
{
	auto gemShowLayer =(Layer *)gemLayer->getChildByTag(SHOWGEMVALUELAYER);

	auto labelCost = Label::createWithTTF(StringDataManager::getString("equip_gem_costStoneAndgold"), APP_FONT_NAME, 12);
	labelCost->setAnchorPoint(Vec2(0.5f,0.5f));
	labelCost->setColor(Color3B(47,93,13));
	gemShowLayer->addChild(labelCost);

	auto image_goldIcon = ImageView::create();
	image_goldIcon->loadTexture("res_ui/coins.png");
	image_goldIcon->setAnchorPoint(Vec2(0.5f,0.5f));
	image_goldIcon->setPosition(Vec2(0,0));
	gemShowLayer->addChild(image_goldIcon);

	//
	int holeCost;
	if (holeFalg>2)
	{
		holeCost = 0;
	}else
	{
		holeCost = (holeFalg+1)*(holeFalg+1)*(holeFalg+1);
	}

	char costHoleStr[20];
	sprintf(costHoleStr,"%d",holeCost);
	
	char costGoldStr[20];
	sprintf(costGoldStr,"%d",holeCost*1000);
	//
	int allAmountStoneHole = this->getCurHoleStoneAmount(1)+ this->getCurHoleStoneAmount(0);
		
	auto labelCost_gold = Label::createWithTTF(costGoldStr, APP_FONT_NAME, 12);
	labelCost_gold->setAnchorPoint(Vec2(0.5f,0.5f));
	gemShowLayer->addChild(labelCost_gold);
	
	if (GameView::getInstance()->getPlayerGold() >= holeCost*1000)
	{
		labelCost_gold->setColor(Color3B(47,93,13));
	}else
	{
		labelCost_gold->setColor(Color3B(255,0,0));
	}

	auto labelStone_ = Label::createWithTTF(StringDataManager::getString("equip_gem_hole_goodsName"),APP_FONT_NAME, 12);
	labelStone_->setAnchorPoint(Vec2(0,0.5f));
	gemShowLayer->addChild(labelStone_);
	labelStone_->setColor(Color3B(47,93,13));

	auto labelStone_num = Label::createWithTTF(costHoleStr, APP_FONT_NAME, 12);
	labelStone_num->setAnchorPoint(Vec2(0.5f,0.5f));
	gemShowLayer->addChild(labelStone_num);

	if (allAmountStoneHole >= holeCost)
	{
		labelStone_num->setColor(Color3B(47,93,13));
	}else
	{
		labelStone_num->setColor(Color3B(255,0,0));
	}
	
	useGemBingding = ui::CheckBox::create();
	useGemBingding->setTouchEnabled(true);
	useGemBingding->loadTextures("res_ui/xiaofangkuang.png",
		"res_ui/xiaofangkuang.png",
		"res_ui/redgou.png",
		"res_ui/redgou.png",
		"");
	useGemBingding->setAnchorPoint(Vec2(0.5f,0.5f));
	useGemBingding->addEventListener(CC_CALLBACK_2(EquipMentUi::setGemBingding,this));
	gemShowLayer->addChild(useGemBingding);
	useGemBingding->setSelected(false);

	auto label_useStoneNotBinding = Label::createWithTTF(StringDataManager::getString("equip_gem_useBingding_stone_hole"), APP_FONT_NAME, 12);
	label_useStoneNotBinding->setAnchorPoint(Vec2(0,0.5f));
	gemShowLayer->addChild(label_useStoneNotBinding);
	label_useStoneNotBinding->setColor(Color3B(47,93,13));

	switch(holeIndex)
	{
	case 0:
		{
			labelCost->setPosition(Vec2(150,250));
			image_goldIcon->setPosition(Vec2(labelCost->getPosition().x+labelCost->getContentSize().width,labelCost->getPosition().y +labelCost->getContentSize().height - 5));
			labelCost_gold->setPosition(Vec2(image_goldIcon->getPosition().x + image_goldIcon->getContentSize().width+10,image_goldIcon->getPosition().y));

			labelStone_->setPosition(Vec2(image_goldIcon->getPosition().x - image_goldIcon->getContentSize().width/2,image_goldIcon->getPosition().y - labelStone_->getContentSize().height-10));
			labelStone_num->setPosition(Vec2(labelStone_->getPosition().x + labelStone_->getContentSize().width+10,labelStone_->getPosition().y));

			useGemBingding->setPosition(Vec2(140,labelStone_->getPosition().y - labelStone_->getContentSize().height - 10));
			label_useStoneNotBinding->setPosition(Vec2(useGemBingding->getPosition().x+useGemBingding->getContentSize().width/2 ,useGemBingding->getPosition().y));
		}break;
	case 1:
		{
			labelCost->setPosition(Vec2(150,170));
			image_goldIcon->setPosition(Vec2(labelCost->getPosition().x+labelCost->getContentSize().width,labelCost->getPosition().y +labelCost->getContentSize().height - 5));
			labelCost_gold->setPosition(Vec2(image_goldIcon->getPosition().x + image_goldIcon->getContentSize().width+10,image_goldIcon->getPosition().y));

			labelStone_->setPosition(Vec2(image_goldIcon->getPosition().x - image_goldIcon->getContentSize().width/2,image_goldIcon->getPosition().y - labelStone_->getContentSize().height-10));
			labelStone_num->setPosition(Vec2(labelStone_->getPosition().x + labelStone_->getContentSize().width+10,labelStone_->getPosition().y));

			useGemBingding->setPosition(Vec2(140,labelStone_->getPosition().y - labelStone_->getContentSize().height - 10));
			label_useStoneNotBinding->setPosition(Vec2(useGemBingding->getPosition().x+useGemBingding->getContentSize().width/2 ,useGemBingding->getPosition().y));
		}break;
	case 2:
		{
			labelCost->setPosition(Vec2(150,90));
			image_goldIcon->setPosition(Vec2(labelCost->getPosition().x+labelCost->getContentSize().width,labelCost->getPosition().y +labelCost->getContentSize().height - 5));
			labelCost_gold->setPosition(Vec2(image_goldIcon->getPosition().x + image_goldIcon->getContentSize().width+10,image_goldIcon->getPosition().y));

			labelStone_->setPosition(Vec2(image_goldIcon->getPosition().x - image_goldIcon->getContentSize().width/2,image_goldIcon->getPosition().y - labelStone_->getContentSize().height-10));
			labelStone_num->setPosition(Vec2(labelStone_->getPosition().x + labelStone_->getContentSize().width+10,labelStone_->getPosition().y));

			useGemBingding->setPosition(Vec2(145,labelStone_->getPosition().y - labelStone_->getContentSize().height - 10));
			label_useStoneNotBinding->setPosition(Vec2(useGemBingding->getPosition().x+useGemBingding->getContentSize().width/2 - 5,useGemBingding->getPosition().y));
		}break;
	}
}

std::string EquipMentUi::getBackGroundPathByGoodsQulity( int quality )
{
	std::string frameColorPath = "res_ui/";
	if (quality == 1)
	{
		frameColorPath.append("sdi_white");
	}
	else if (quality == 2)
	{
		frameColorPath.append("sdi_green");
	}
	else if (quality == 3)
	{
		frameColorPath.append("sdi_bule");
	}
	else if (quality == 4)
	{
		frameColorPath.append("sdi_purple");
	}
	else if (quality == 5)
	{
		frameColorPath.append("sdi_orange");
	}
	else
	{
		frameColorPath.append("sdi_none");
	}
	frameColorPath.append(".png");

	return frameColorPath;
}

void EquipMentUi::isSureEquip( int index )
{
	//if mainEquip is not binding
	int mainEquip = 0;
	if (mainEquipIsPac ==true)
	{
		mainEquip = GameView::getInstance()->AllPacItem.at(mainEquipIndexRefine)->goods().binding();
	}else
	{
		for (int j =0;j< curMainEquipvector.size();j++)
		{
			if (generalEquipOfPart == curMainEquipvector.at(j)->part())
			{
				mainEquip = curMainEquipvector.at(j)->goods().binding();
			}
		}
	}
	switch (index)
	{
	case REFININGTYPE:
		{
			bool isBinging = false;
			for (int i=0;i< assistVector.size();i++)
			{
				if (assistVector.at(i).binging == 1)
				{
					isBinging = true;
					break;
				}
			}
			for (int j = 0;j<assStoneVector.size();j++)
			{
				if (assStoneVector.at(j).binging == 1)
				{
					isBinging = true;
					break;
				}
			}

			if (mainEquip == 0 && isBinging == true)
			{
				const char * strings_  = StringDataManager::getString("equip_isRefineIsBingingOfassist");
				GameView::getInstance()->showPopupWindow(strings_,2,this,SEL_CallFuncO(&EquipMentUi::isSureBingdingRefine),NULL);
			}else
			{
				isSureBingdingRefine(NULL);
			}
		}break;
	case STARTYPE:
		{
			int starBingding = GameView::getInstance()->AllPacItem.at(stoneOfIndexStar)->goods().binding();
			if (mainEquip == 0 && starBingding == 1)
			{
				const char * strings_  = StringDataManager::getString("equip_isStarIsBingingOfassist");
				GameView::getInstance()->showPopupWindow(strings_,2,this,CC_CALLFUNCO_SELECTOR(EquipMentUi::isSureBingdingStar),NULL);
			}else
			{
				isSureBingdingStar(NULL);
			}
		}break;
	case GEMTYPE:
		{
			int starBingding = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->goods().binding();
			if (mainEquip == 0 && starBingding == 1)
			{
				const char * strings_  = StringDataManager::getString("equip_isGemIsBingingOfassist");
				GameView::getInstance()->showPopupWindow(strings_,2,this, CC_CALLFUNCO_SELECTOR(EquipMentUi::isSureBingdingGem),NULL);
			}else
			{
				isSureBingdingGem(NULL);
			}
		}break;
	case BAPTIZETYPE:
		{

		}break;
	}
}

void EquipMentUi::isSureBingdingRefine( Ref * obj )
{
	bool isHaveStarLv = false;
	bool isHaveGemValue = false;
	for (int i=0;i< assistVector.size();i++)
	{
		if (assistVector.at(i).equipStarLv > 0)
		{
			isHaveStarLv = true;
		}

		if (assistVector.at(i).equipGemLv == true)
		{
			isHaveGemValue = true;
		}
	}

	if (isHaveStarLv == true || isHaveGemValue == true) 
	{
		const char * strings_  = StringDataManager::getString("equip_isRefineIsStarLevelOfassist");
		GameView::getInstance()->showPopupWindow(strings_,2,this, CC_CALLFUNCO_SELECTOR(EquipMentUi::isSureStarLevelOfRefine),NULL);
	}else
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1602,this);
	}
}

void EquipMentUi::isSureBingdingStar( Ref * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1607,this);
}

void EquipMentUi::isSureBingdingGem( Ref * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1623,(void *)holeIndex_,(void *)pageView->curPackageItemIndex);
}

void EquipMentUi::isSureBingdingBaptize( Ref * obj )
{

}

void EquipMentUi::isSureStarLevelOfRefine( Ref * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1602,this);
}

bool EquipMentUi::isHaveGemValue( GoodsInfo * goods )
{
	bool isHaveGemvalue = false;
	for (int i =0; i< goods->equipmentdetail().properties_size();i++)
	{
		if (goods->equipmentdetail().properties(i).type() == 4 )
		{
			if (goods->equipmentdetail().properties(i).value(0) > 0)
			{
				isHaveGemvalue = true;
			}
		}
	}

	return isHaveGemvalue;
}

void EquipMentUi::setGemBingding(Ref* pSender, CheckBox::EventType type)
{

}

void EquipMentUi::setBaptizeDingding(Ref* pSender, CheckBox::EventType type)
{

}

void EquipMentUi::isSureNotHoleBingding( Ref * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1622,(void *)1);
}

void EquipMentUi::isSureBingdingHoleBingding( Ref * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1622,(void *)0);
}

int EquipMentUi::getBaptizeClock( int bingdingType )
{
	int size = GameView::getInstance()->AllPacItem.size();
	int clockAmount = 0;
	for (int i =0;i<size;i++)
	{
		if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
		{
			int curClazz = GameView::getInstance()->AllPacItem.at(i)->goods().clazz();
			int bingding = GameView::getInstance()->AllPacItem.at(i)->goods().binding();
			if ( curClazz == 26 && bingding == bingdingType)
			{
				clockAmount += GameView::getInstance()->AllPacItem.at(i)->quantity();
			}
		}
	}
	return clockAmount;
}

void EquipMentUi::isSureNotBingingBaptize( Ref * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1611,this,(void *)1);
}

void EquipMentUi::isSureBingingBaptize( Ref * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1611,this,(void *)0);
}

void EquipMentUi::setCurEquipExStar( int value_ )
{
	m_curEquipStarEx = value_;
}

int EquipMentUi::getCurEquipExStar()
{
	return m_curEquipStarEx;
}

void EquipMentUi::setEquipBeginLevel( int curLv_ )
{
	m_equipBeginLevel = curLv_;
}

int EquipMentUi::getEquipBeginLevel()
{
	return m_equipBeginLevel;
}

void EquipMentUi::PageScrollToDefault()
{
	pageView->getPageView()->scrollToPage(0);
	CurrentPageViewChanged(pageView, PageView::EventType::TURNING);
}

void EquipMentUi::addExPercentSetPrecentIsFull( )
{
	exPercent_refineAdd->setScaleX(0.98f);
}

void EquipMentUi::addExPercentSetPrecentIsCur( )
{
	float scaleBegin_ = curExValue*1.0f;
	float scaleEnd_ = allExValue*1.0f;
	float scaleEx =scaleBegin_/scaleEnd_;
	exPercent_refineAdd->setScaleX(scaleEx);
}

void EquipMentUi::refreshCurState()
{
	auto  action1 = FadeTo::create(0.75f,0);
	auto  action2 = FadeTo::create(0.75f,255);
	auto repeapAction = RepeatForever::create(Sequence::create(action1,action2,NULL));
	switch(currType)
	{
	case REFININGTYPE:
		{
			if (refineMainEquip == true)
			{
				image_state_refine_mainEquip->stopAllActions();
				image_state_first_assist->runAction((FiniteTimeAction *)(repeapAction->clone()->autorelease()));
				image_state_second_assist->runAction((FiniteTimeAction *)(repeapAction->clone()->autorelease()));
				image_state_third_assist->runAction((FiniteTimeAction *)(repeapAction->clone()->autorelease()));
				image_state_four_assist->runAction((FiniteTimeAction *)(repeapAction->clone()->autorelease()));
				image_state_five_assist->runAction((FiniteTimeAction *)(repeapAction->clone()->autorelease()));
			}else
			{
				image_state_refine_mainEquip->runAction((FiniteTimeAction *)(repeapAction->clone()->autorelease()));

				image_state_first_assist->stopAllActions();
				image_state_first_assist->runAction((FiniteTimeAction *)(action2->clone()->autorelease()));

				image_state_second_assist->stopAllActions();
				image_state_second_assist->runAction((FiniteTimeAction *)(action2->clone()->autorelease()));
				
				image_state_third_assist->stopAllActions();
				image_state_third_assist->runAction((FiniteTimeAction *)(action2->clone()->autorelease()));

				image_state_four_assist->stopAllActions();
				image_state_four_assist->runAction((FiniteTimeAction *)(action2->clone()->autorelease()));

				image_state_five_assist->stopAllActions();
				image_state_five_assist->runAction((FiniteTimeAction *)(action2->clone()->autorelease()));
			}
		}break;
	case STARTYPE:
		{
			if (starMainEquip == true)
			{
				image_state_star_MainEquip->stopAllActions();
				image_state_star_stone->runAction((FiniteTimeAction *)(repeapAction->clone()->autorelease()));
			}else
			{
				image_state_star_MainEquip->runAction((FiniteTimeAction *)(repeapAction->clone()->autorelease()));
				image_state_star_stone->stopAllActions();
				image_state_star_stone->runAction((FiniteTimeAction *)(action2->clone()->autorelease()));
			}
		}break;
	case GEMTYPE:
		{
			if (gemMainEquip == true)
			{
				image_state_gem_mainEquip->runAction((FiniteTimeAction *)(action2->clone()->autorelease()));
			}else
			{
				image_state_gem_mainEquip->runAction((FiniteTimeAction *)(repeapAction->clone()->autorelease()));
			}
		}break;
	case BAPTIZETYPE:
		{
			if (baptizeMainEquip == true)
			{
				image_state_baptize_mainEquip->runAction((FiniteTimeAction *)(action2->clone()->autorelease()));
			}else
			{
				image_state_baptize_mainEquip->runAction((FiniteTimeAction *)(repeapAction->clone()->autorelease()));
			}
		}break;

	}
}

void EquipMentUi::setBaptizeFirstCheckBox(Ref* pSender, CheckBox::EventType type)
{
	auto checkbox =(CheckBox *)pSender;
	int powerLockIndex_ = additionProperty_baptize.at(0)->property(); 
	if (checkbox->isSelected() ==true)
	{
		propertyLock.push_back(powerLockIndex_);
	}else
	{
		for (int i = 0;i<propertyLock.size();i++)
		{
			if (powerLockIndex_ == propertyLock.at(i))
			{
				propertyLock.erase(propertyLock.begin()+i);
			}
		}
	}

	char baptizeSize[10];
	sprintf(baptizeSize,"%d",propertyLock.size());
	labelBaptizeCostStrone->setString(baptizeSize);
}

void EquipMentUi::setBaptizeSecondCheckBox(Ref* pSender, CheckBox::EventType type)
{
	auto checkbox =(CheckBox *)pSender;
	int powerLockIndex_ = additionProperty_baptize.at(1)->property(); 
	if (checkbox->isSelected() ==true)
	{
		propertyLock.push_back(powerLockIndex_);
	}else
	{
		for (int i = 0;i<propertyLock.size();i++)
		{
			if (powerLockIndex_ == propertyLock.at(i))
			{
				propertyLock.erase(propertyLock.begin()+i);
			}
		}
	}
	char baptizeSize[10];
	sprintf(baptizeSize,"%d",propertyLock.size());
	labelBaptizeCostStrone->setString(baptizeSize);
}

void EquipMentUi::setBaptizeThirdCheckBox(Ref* pSender, CheckBox::EventType type)
{
	auto checkbox =(CheckBox *)pSender;
	int powerLockIndex_ = additionProperty_baptize.at(2)->property(); 
	if (checkbox->isSelected() ==true)
	{
		propertyLock.push_back(powerLockIndex_);
	}else
	{
		for (int i = 0;i<propertyLock.size();i++)
		{
			if (powerLockIndex_ == propertyLock.at(i))
			{
				propertyLock.erase(propertyLock.begin()+i);
			}
		}
	}
	char baptizeSize[10];
	sprintf(baptizeSize,"%d",propertyLock.size());
	labelBaptizeCostStrone->setString(baptizeSize);
}

void EquipMentUi::setBaptizeFourCheckBox(Ref* pSender, CheckBox::EventType type)
{
	auto checkbox =(CheckBox *)pSender;
	int powerLockIndex_ = additionProperty_baptize.at(3)->property(); 
	if (checkbox->isSelected() ==true)
	{
		propertyLock.push_back(powerLockIndex_);
	}else
	{
		for (int i = 0;i<propertyLock.size();i++)
		{
			if (powerLockIndex_ == propertyLock.at(i))
			{
				propertyLock.erase(propertyLock.begin()+i);
			}
		}
	}
	char baptizeSize[10];
	sprintf(baptizeSize,"%d",propertyLock.size());
	labelBaptizeCostStrone->setString(baptizeSize);
}

void EquipMentUi::setBaptizeFiveCheckBox(Ref* pSender, CheckBox::EventType type)
{
	auto checkbox =(CheckBox *)pSender;
	int powerLockIndex_ = additionProperty_baptize.at(4)->property(); 
	if (checkbox->isSelected() ==true)
	{
		propertyLock.push_back(powerLockIndex_);
	}else
	{
		for (int i = 0;i<propertyLock.size();i++)
		{
			if (powerLockIndex_ == propertyLock.at(i))
			{
				propertyLock.erase(propertyLock.begin()+i);
			}
		}
	}
	char baptizeSize[10];
	sprintf(baptizeSize,"%d",propertyLock.size());
	labelBaptizeCostStrone->setString(baptizeSize);
}

void EquipMentUi::callBackRefineMainDes(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (refineMainEquip == false)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("equip_refine_refineIsNull"));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipMentUi::callBackAssistEquipDes(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (refineMainEquip == false)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("equip_refine_AssistIsNull"));
		}
		else
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("equip_refine_AssistIsNull_2"));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipMentUi::callBackStarMainDes(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (starMainEquip == false)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("equip_Star_StarIsNull"));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipMentUi::callBackStarAssistDes(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (starMainEquip == false)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("equip_Star_AssistIsNull"));
		}
		else
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("equip_Star_AssistIsNull_2"));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}


void EquipMentUi::callBackGemMainDes(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (gemMainEquip == false)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("equip_Gem_GemIsNull"));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipMentUi::callBackGemAssistDes(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (gemMainEquip == false)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("equip_Gem_AssistIsNull"));
		}
		else
		{
			auto btn_ = (Button *)pSender;
			int btnIndex_ = btn_->getTag();
			if (equipmentHoleV.size() <= btnIndex_)
			{
				return;
			}
			int holeState_ = equipmentHoleV.at(btnIndex_)->state();
			switch (holeState_)
			{
			case 1:
			{
				//not open
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("equip_gem_gemState_notOpen"));
			}break;
			case 2:
			{
				//open
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("equip_gem_gemState_Open"));
			}break;
			case 3:
			{
				//gem
				//GameView::getInstance()->showAlertDialog(StringDataManager::getString("equip_strength_equipHoleIsAll"));
			}break;
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipMentUi::callBackBaptizeMainDes(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (baptizeMainEquip == false)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("equip_Baptize_BaptizeIsNull"));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void EquipMentUi::callBackStarDes(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		std::map<int, std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(18);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end())
		{
			return;
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			{
				Size winSize = Director::getInstance()->getVisibleSize();
				auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[18].c_str());
				GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				showSystemInfo->setIgnoreAnchorPointForPosition(false);
				showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				showSystemInfo->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}



