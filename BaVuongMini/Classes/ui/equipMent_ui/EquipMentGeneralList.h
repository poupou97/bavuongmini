
#ifndef EQUIPMENTUI_EQUIPMENTGENERALLIST_H
#define  EQUIPMENTUI_EQUIPMENTGENERALLIST_H

#include "../extensions/UIScene.h"
#include "../generals_ui/GeneralsListBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class EquipMentGeneralList : public GeneralsListBase,public TableViewDelegate,public TableViewDataSource
{
public:
	EquipMentGeneralList(void);
	~EquipMentGeneralList(void);

	static EquipMentGeneralList *create();
	bool init();

	virtual void onEnter();
	virtual void onExit();


	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void ReloadGeneralsTableView();
	void ReloadGeneralsTableViewWithoutChangeOffSet();


public:
	//std::vector<CGeneralBaseMsg *>generalBaseMsgList;
	TableView * generalList_tableView;
	CGeneralBaseMsg * curGeneralBaseMsg;
};

#endif;