#include "RefreshEquipData.h"
#include "EquipMentUi.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "StrengthEquip.h"
#include "../../messageclient/element/CEquipment.h"
#include "GeneralEquipMent.h"
#include "GeneralEquipRecover.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/CEquipProperty.h"
#include "../../messageclient/element/CAdditionProperty.h"
#include "../../ui/backpackscene/PacPageView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/MapStrngthStone.h"

RefreshEquipData::RefreshEquipData(void)
{
}


RefreshEquipData::~RefreshEquipData(void)
{
}
RefreshEquipData * RefreshEquipData::s_equipData = NULL;
RefreshEquipData * RefreshEquipData::instance()
{
	if (NULL == s_equipData)
	{
		s_equipData = new RefreshEquipData();
	}

	return s_equipData;
}

void RefreshEquipData::refreshRefineEquip()
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	auto equipui = (EquipMentUi *)mainscene->getChildByTag(ktagEquipMentUI);

	if (equipui->mainEquipIsPac == true)
	{
		auto strengthBegin= StrengthEquip::create(GameView::getInstance()->AllPacItem.at(equipui->mainEquipIndexRefine),true);
		strengthBegin->setIgnoreAnchorPointForPosition(false);
		strengthBegin->setAnchorPoint(Vec2(0,0));
		strengthBegin->setPosition(Vec2(113,309.2f));
		strengthBegin->setTag(REFININGMAINEQUIP);
		equipui->refineLayer->addChild(strengthBegin);

		//equip end
		auto strengthEnd= StrengthEquip::create(GameView::getInstance()->AllPacItem.at(equipui->mainEquipIndexRefine),true);
		strengthEnd->setIgnoreAnchorPointForPosition(false);
		strengthEnd->setAnchorPoint(Vec2(0,0));
		strengthEnd->setPosition(Vec2(275.5f,309.2f));
		strengthEnd->setTag(REFINELOOKEQUIP);
		strengthEnd->Btn_pacItemFrame->setTouchEnabled(false);
		equipui->refineLayer->addChild(strengthEnd);

		//equipui->equipLookRefineLevel_ =strengthEnd->getRefineLevel();
	}else
	{
		auto equipCopy = new CEquipment();
		for (int i = 0;i<equipui->curMainEquipvector.size();i++)
		{
			if (equipui->generalEquipOfPart == equipui->curMainEquipvector.at(i)->part())
			{
				equipCopy->CopyFrom(*equipui->curMainEquipvector.at(i));
			}

			auto starGeneralEquip = (GeneralEquipMent*)equipui->roleLayer->getChildByTag(GENERALEQUIPMENTLIST + i);
			if (starGeneralEquip != NULL)
			{
				//if (equipui->generalEquipOfPart == starGeneralEquip->Btn_pacItemFrame->getTag())
				if (equipui->generalEquipOfPart == starGeneralEquip->generalEquipment->part() && equipui->generalMainId == equipui->mes_petid)
				{
					starGeneralEquip->SetCurgeneralEquipGray(true);
				}
			}
		}
		//general equip begin
		auto generalEquipBegin = GeneralEquipRecover::create(equipCopy,true);
		generalEquipBegin->setIgnoreAnchorPointForPosition(false);
		generalEquipBegin->setAnchorPoint(Vec2(0,0));
		generalEquipBegin->setPosition(Vec2(113,309.2f));
		generalEquipBegin->setTag(GENERALREFINEEQUIP);
		equipui->refineLayer->addChild(generalEquipBegin);
		//general equip end
		auto generalEquipEnd = GeneralEquipRecover::create(equipCopy,true);
		generalEquipEnd->setIgnoreAnchorPointForPosition(false);
		generalEquipEnd->setAnchorPoint(Vec2(0,0));
		generalEquipEnd->setPosition(Vec2(275.7f,309.8f));
		generalEquipEnd->Btn_pacItemFrame->setTouchEnabled(false);
		generalEquipEnd->setTag(GENERALREFINEEQUIPLOOK);
		equipui->refineLayer->addChild(generalEquipEnd);

		//equipui->equipLookRefineLevel_ = generalEquipEnd->getRefineLevel();
		delete equipCopy;
	}
}

void RefreshEquipData::refreshStarEquip()
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	auto equipui = (EquipMentUi *)mainscene->getChildByTag(ktagEquipMentUI);
	if (equipui->mainEquipIsPac == true)
	{
		//equip begin
		auto strengthequip= StrengthEquip::create(GameView::getInstance()->AllPacItem.at(equipui->mainEquipIndexRefine),true);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(113,309.2f));
		strengthequip->setTag(STAREQUIP);
		equipui->starPropertyLayer->addChild(strengthequip);

		//equip end
		auto strengthequipEnd = StrengthEquip::create(GameView::getInstance()->AllPacItem.at(equipui->mainEquipIndexRefine),true);
		strengthequipEnd->setIgnoreAnchorPointForPosition(false);
		strengthequipEnd->setAnchorPoint(Vec2(0,0));
		strengthequipEnd->setPosition(Vec2(275.5f,309.2f));
		strengthequipEnd->Btn_pacItemFrame->setTouchEnabled(false);
		strengthequipEnd->setTag(STARLOOKEQUIP);
		strengthequipEnd->setStarLevel(strengthequipEnd->getStarLevel()+1);
		equipui->starPropertyLayer->addChild(strengthequipEnd);	

		//equipui->curMainEquipStarLevel= strengthequip->getStarLevel();
	}else
	{
		auto equipCopy = new CEquipment();
		for (int i = 0;i<equipui->curMainEquipvector.size();i++)
		{
			if (equipui->generalEquipOfPart == equipui->curMainEquipvector.at(i)->part())
			{
				equipCopy->CopyFrom(*equipui->curMainEquipvector.at(i));
			}

			auto starGeneralEquip = (GeneralEquipMent*)equipui->roleLayer->getChildByTag(GENERALEQUIPMENTLIST + i);
			if (starGeneralEquip != NULL)
			{
				if (equipui->generalEquipOfPart == starGeneralEquip->Btn_pacItemFrame->getTag() && 
								equipui->generalMainId == equipui->mes_petid)
				{
					starGeneralEquip->SetCurgeneralEquipGray(true);
				}
			}
		}
		/*
		for (int j=0;j<equipui->generalEquipVector.size();j++)
		{
			GeneralEquipMent * starGeneralEquip = (GeneralEquipMent*)equipui->roleLayer->getChildByTag(GENERALEQUIPMENTLIST + j);
			if (equipui->generalEquipOfPart == starGeneralEquip->Btn_pacItemFrame->getTag())
			{
				starGeneralEquip->SetCurgeneralEquipGray(true);
				equipCopy->CopyFrom(*starGeneralEquip->generalEquipment);
			}
		}
		*/
		//general begin equip
		auto generalEquip =GeneralEquipRecover::create(equipCopy,true);
		generalEquip->setIgnoreAnchorPointForPosition(false);
		generalEquip->setAnchorPoint(Vec2(0,0));
		generalEquip->setPosition(Vec2(113,309.2f));
		generalEquip->setTag(GENERALSTAREQUIP);
		equipui->starPropertyLayer->addChild(generalEquip);
		//general end equip
		auto generalEquipEnd =GeneralEquipRecover::create(equipCopy,true);
		generalEquipEnd->setIgnoreAnchorPointForPosition(false);
		generalEquipEnd->setAnchorPoint(Vec2(0,0));
		generalEquipEnd->setPosition(Vec2(275.5f,309.8f));
		generalEquipEnd->Btn_pacItemFrame->setTouchEnabled(false);
		generalEquipEnd->setTag(GENERALSTARQUIPLOOK);
		generalEquipEnd->setStarLevel(generalEquipEnd->getStarLevel()+1);
		equipui->starPropertyLayer->addChild(generalEquipEnd);
		delete equipCopy;

		//equipui->curMainEquipStarLevel= generalEquip->getStarLevel();
	}
}

void RefreshEquipData::refreshGem()
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	auto equipui = (EquipMentUi *)mainscene->getChildByTag(ktagEquipMentUI);

	if (equipui->mainEquipIsPac == true)
	{
		auto strengthequip= StrengthEquip::create(GameView::getInstance()->AllPacItem.at(equipui->mainEquipIndexRefine),true);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(196.8f,319));
		strengthequip->setTag(GEMEQUIP);
		equipui->gemLayer->addChild(strengthequip);
	}else
	{
		auto equipCopy = new CEquipment();
		for (int i = 0;i<equipui->curMainEquipvector.size();i++)
		{
			if (equipui->generalEquipOfPart == equipui->curMainEquipvector.at(i)->part())
			{
				equipCopy->CopyFrom(*equipui->curMainEquipvector.at(i));
			}

			auto starGeneralEquip = (GeneralEquipMent*)equipui->roleLayer->getChildByTag(GENERALEQUIPMENTLIST + i);
			if (starGeneralEquip != NULL)
			{
				if (equipui->generalEquipOfPart == starGeneralEquip->Btn_pacItemFrame->getTag()&& equipui->generalMainId == equipui->mes_petid)
				{
					starGeneralEquip->SetCurgeneralEquipGray(true);
				}
			}
		}
		
		/*
		for (int j=0;j<equipui->generalEquipVector.size();j++)
		{
			GeneralEquipMent * starGeneralEquip = (GeneralEquipMent*)equipui->roleLayer->getChildByTag(GENERALEQUIPMENTLIST + j);
			if (equipui->generalEquipOfPart == starGeneralEquip->Btn_pacItemFrame->getTag())
			{
				starGeneralEquip->SetCurgeneralEquipGray(true);
				equipCopy->CopyFrom(*starGeneralEquip->generalEquipment);
			}
		}
		*/
		auto generalEquipBegin = GeneralEquipRecover::create(equipCopy,true);
		generalEquipBegin->setIgnoreAnchorPointForPosition(false);
		generalEquipBegin->setAnchorPoint(Vec2(0,0));
		generalEquipBegin->setPosition(Vec2(196.8f,319));
		generalEquipBegin->setTag(GENERALGEMEQUIP);
		equipui->gemLayer->addChild(generalEquipBegin);

		delete equipCopy;
	}
}

void RefreshEquipData::refeshBaptize()
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	auto equipui = (EquipMentUi *)mainscene->getChildByTag(ktagEquipMentUI);

	if (equipui->mainEquipIsPac == true)
	{
		//strength equip begin
		auto strengthequip= StrengthEquip::create(GameView::getInstance()->AllPacItem.at(equipui->mainEquipIndexRefine),true);
		strengthequip->setIgnoreAnchorPointForPosition(false);
		strengthequip->setAnchorPoint(Vec2(0,0));
		strengthequip->setPosition(Vec2(196.8f,319));
		strengthequip->setTag(BAPTIZEEQUIP);
		equipui->baptizePropertylayer->addChild(strengthequip);
	}else
	{
		auto equipCopy = new CEquipment();
		for (int i = 0;i<equipui->curMainEquipvector.size();i++)
		{
			if (equipui->generalEquipOfPart == equipui->curMainEquipvector.at(i)->part())
			{
				equipCopy->CopyFrom(*equipui->curMainEquipvector.at(i));
			}
			auto starGeneralEquip = (GeneralEquipMent*)equipui->roleLayer->getChildByTag(GENERALEQUIPMENTLIST + i);
			if (starGeneralEquip != NULL)
			{
				if (equipui->generalEquipOfPart == starGeneralEquip->Btn_pacItemFrame->getTag()&& equipui->generalMainId == equipui->mes_petid)
				{
					starGeneralEquip->SetCurgeneralEquipGray(true);
				}
			}
		}
		
		/*
		for (int j=0;j<equipui->generalEquipVector.size();j++)
		{
			GeneralEquipMent * starGeneralEquip = (GeneralEquipMent*)equipui->roleLayer->getChildByTag(GENERALEQUIPMENTLIST + j);
			if (equipui->generalEquipOfPart == starGeneralEquip->Btn_pacItemFrame->getTag())
			{
				starGeneralEquip->SetCurgeneralEquipGray(true);
				equipCopy->CopyFrom(*starGeneralEquip->generalEquipment);
			}
		}
		*/
		auto generalEquip =GeneralEquipRecover::create(equipCopy,true);
		generalEquip->setIgnoreAnchorPointForPosition(false);
		generalEquip->setAnchorPoint(Vec2(0,0));
		generalEquip->setPosition(Vec2(196.8f,319));
		generalEquip->setTag(GENERALBAPTIZEEQUIP);
		equipui->baptizePropertylayer->addChild(generalEquip);
	}
}

void RefreshEquipData::setRefineEndValue(int refineLevel,bool isAction)
{
	////�޸�װ���ľ����� 
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	auto equipui = (EquipMentUi *)mainscene->getChildByTag(ktagEquipMentUI);
	if (equipui->mainEquipIsPac == true)
	{
		auto equipRefine =(StrengthEquip *)equipui->refineLayer->getChildByTag(REFININGMAINEQUIP);
		equipRefine->setRefineLevel(refineLevel,isAction);
		auto strengthEnd= (StrengthEquip *)equipui->refineLayer->getChildByTag(REFINELOOKEQUIP);
		strengthEnd->setRefineLevel(refineLevel);

	}else
	{
		auto generalEquipBegin = (GeneralEquipRecover *)equipui->refineLayer->getChildByTag(GENERALREFINEEQUIP);
		generalEquipBegin->setRefineLevel(refineLevel,isAction);
		auto generalEquipEnd = (GeneralEquipRecover *)equipui->refineLayer->getChildByTag(GENERALREFINEEQUIPLOOK);
		generalEquipEnd->setRefineLevel(refineLevel);
	}
}

void RefreshEquipData::setStarEndValue( int starLevel , bool isAction)
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	auto equipui = (EquipMentUi *)mainscene->getChildByTag(ktagEquipMentUI);
	if (equipui->mainEquipIsPac == true)
	{
		//equip begin
		auto strengthequipBegin = (StrengthEquip *)equipui->starPropertyLayer->getChildByTag(STAREQUIP);
		strengthequipBegin->setStarLevel(starLevel,isAction);

		//equip end
		auto strengthequipEnd =  (StrengthEquip *)equipui->starPropertyLayer->getChildByTag(STARLOOKEQUIP);
		strengthequipEnd->setStarLevel(starLevel+1);
	}else
	{
		//general begin equip
		auto generalEquipBegin =(GeneralEquipRecover*)equipui->starPropertyLayer->getChildByTag(GENERALSTAREQUIP);
		generalEquipBegin->setStarLevel(starLevel,isAction);
		//general end equip
		auto generalEquipEnd =(GeneralEquipRecover*)equipui->starPropertyLayer->getChildByTag(GENERALSTARQUIPLOOK);
		generalEquipEnd->setStarLevel(starLevel+1);
	}
}

void RefreshEquipData::getOperationGeneral()
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	auto equipui = (EquipMentUi *)mainscene->getChildByTag(ktagEquipMentUI);

	if (equipui->generalMainId == equipui->mes_petid)
	{
		std::vector<CEquipment *>::iterator iter;
		for (iter = equipui->curMainEquipvector.begin(); iter != equipui->curMainEquipvector.end();++iter)
		{
			delete * iter;
		}
		equipui->curMainEquipvector.clear();

		for (int i = 0;i<equipui->generalEquipVector.size();i++)
		{
			auto temp_ =new CEquipment();
			temp_->CopyFrom( *equipui->generalEquipVector.at(i));
			equipui->curMainEquipvector.push_back(temp_);
		}
	}
}

void RefreshEquipData::assistRunAction(Vec2 p1,Vec2 p2,FolderInfo * floders,bool isAction)
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	auto equipui = (EquipMentUi *)mainscene->getChildByTag(ktagEquipMentUI);
	if (isAction)
	{
		int p_x = equipui->pageView->getPosition().x + p1.x;
		int p_y = equipui->pageView->getPosition().y + p1.y+50;

		auto sp_ = Sprite::create(getCurEquipSp(floders->goods().quality()).c_str());
		sp_->setAnchorPoint(Vec2(0.5f,0.5f));
		sp_->setPosition(Vec2(p_x,p_y));
		equipui->refineLayer->addChild(sp_,30);

		std::string  iconPath_ ="res_ui/props_icon/";
		std::string equipicon_ = floders->goods().icon();
		iconPath_.append(equipicon_);
		iconPath_.append(".png");

		auto sp_icon = Sprite::create(iconPath_.c_str());
		sp_icon->setAnchorPoint(Vec2(0.5f,0.5f));
		sp_icon->setPosition(Vec2(31,31));
		sp_->addChild(sp_icon);

		int addExp = 0;
		auto m_folder = new FolderInfo();
		m_folder->CopyFrom(*floders);

		int temp_addExp = 0;
		if (m_folder->goods().equipmentclazz()> 0 && m_folder->goods().equipmentclazz()<10 )
		{
			addExp+= (floders->goods().equipmentdetail().fightcapacity())/5+1;
		}else
		{
			auto mapStone = StrengthStoneConfigData::s_StrengthStrone[floders->goods().id()];
			int addEx =  mapStone->get_exp();
			int goodsCount_ = equipui->stoneOfAmountRefine;
			addExp+= addEx*goodsCount_;
		}

		int temp_l = (p_x - p2.x)*(p_x - p2.x) + (p_y - p2.y)*(p_y - p2.y);
		int temp_v = 1000;
		int temp_s = sqrt((float)temp_l);
		float temp_t = (float)temp_s/temp_v;

		auto action = Spawn::create(
			ScaleTo::create(temp_t,0.8f),
			NULL);

		ActionInterval * action1 =(ActionInterval *)Sequence::create(
			MoveTo::create(temp_t,p2),
			CallFuncN::create(CC_CALLBACK_1(RefreshEquipData::assisAddExpAction, this, (void *)m_folder)),
			CallFuncN::create(CC_CALLBACK_1(RefreshEquipData::addExpLabelAction, this, (void *)addExp)),
			RemoveSelf::create(),
			NULL);

		sp_->runAction(action);
		sp_->runAction(action1);
	}else
	{
		assisAddExpAction(NULL,floders);
	}
}

void RefreshEquipData::assisAddExpAction(Node* sender, void* data)
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	auto equipui = (EquipMentUi *)mainscene->getChildByTag(ktagEquipMentUI);

	auto floders = (FolderInfo *)data;

	if (equipui->firstAssist==true)
	{
// 		StrengthEquip *strengthequip= StrengthEquip::create(floders,false);
// 		strengthequip->setIgnoreAnchorPointForPosition(false);
// 		strengthequip->setAnchorPoint(Vec2(0,0));
// 		strengthequip->setPosition(Vec2(90,147));
// 		strengthequip->setScale(0.8f);
// 		strengthequip->setTag(FRISTASSIST);
// 		equipui->refineLayer->addChild(strengthequip);
// 		equipui->firstAssist=true;	

		auto strengthequip = (StrengthEquip *)equipui->refineLayer->getChildByTag(FRISTASSIST);
		if (strengthequip)
		{
			strengthequip->setVisible(true);
		}
	}
	if (equipui->secondtAssist==true)
	{
// 		StrengthEquip *strengthequip= StrengthEquip::create(floders,false);
// 		strengthequip->setIgnoreAnchorPointForPosition(false);
// 		strengthequip->setAnchorPoint(Vec2(0,0));
// 		strengthequip->setPosition(Vec2(146,147));
// 		strengthequip->setScale(0.8f);
// 		strengthequip->setTag(SECONDASSIST);
// 		equipui->refineLayer->addChild(strengthequip);
// 		equipui->secondtAssist=true;

		auto strengthequip = (StrengthEquip *)equipui->refineLayer->getChildByTag(SECONDASSIST);
		if (strengthequip)
		{
			strengthequip->setVisible(true);
		}

	}
	if (equipui->thirdAssist==true)
	{
// 		StrengthEquip *strengthequip= StrengthEquip::create(floders,false);
// 		strengthequip->setIgnoreAnchorPointForPosition(false);
// 		strengthequip->setAnchorPoint(Vec2(0,0));
// 		strengthequip->setPosition(Vec2(202.5f,147));
// 		strengthequip->setScale(0.8f);
// 		strengthequip->setTag(THIRDASSIST);
// 		equipui->refineLayer->addChild(strengthequip);
//  		equipui->thirdAssist=true;

		auto strengthequip = (StrengthEquip *)equipui->refineLayer->getChildByTag(THIRDASSIST);
		if (strengthequip)
		{
			strengthequip->setVisible(true);
		}
	}
	if (equipui->fourAssist==true)
	{
// 		StrengthEquip *strengthequip= StrengthEquip::create(floders,false);
// 		strengthequip->setIgnoreAnchorPointForPosition(false);
// 		strengthequip->setAnchorPoint(Vec2(0,0));
// 		strengthequip->setPosition(Vec2(258.5f,147));
// 		strengthequip->setScale(0.8f);
// 		strengthequip->setTag(FOURASSIST);
// 		equipui->refineLayer->addChild(strengthequip);
//  		equipui->fourAssist=true;

		auto strengthequip = (StrengthEquip *)equipui->refineLayer->getChildByTag(FOURASSIST);
		if (strengthequip)
		{
			strengthequip->setVisible(true);
		}
	}
	if (equipui->fiveAssist ==true)
	{
// 		StrengthEquip *strengthequip= StrengthEquip::create(floders,false);
// 		strengthequip->setIgnoreAnchorPointForPosition(false);
// 		strengthequip->setAnchorPoint(Vec2(0,0));
// 		strengthequip->setPosition(Vec2(315,147));
// 		strengthequip->setScale(0.8f);
// 		strengthequip->setTag(FIVESTRONGTH);
// 		equipui->refineLayer->addChild(strengthequip);
//  		equipui->fiveAssist =true;

		auto strengthequip = (StrengthEquip *)equipui->refineLayer->getChildByTag(FIVESTRONGTH);
		if (strengthequip)
		{
			strengthequip->setVisible(true);
		}
	}
	/*
	if (floders->goods().equipmentclazz()> 0 && floders->goods().equipmentclazz()<10 )
	{
		int equipOfBinging = floders->goods().binding();
		int equipOfStarlv = floders->goods().equipmentdetail().starlevel();
		GoodsInfo * info_ =new GoodsInfo();
		info_->CopyFrom(floders->goods());
		bool equipOfGem = equipui->isHaveGemValue(info_);
		delete info_;
		EquipMentUi::assistEquipStruct assists_={equipui->mes_source,equipui->mes_assistEquipPob,equipui->mes_petid,equipui->pageView->curPackageItemIndex,0,equipOfBinging,equipOfStarlv,equipOfGem};
		equipui->assistVector.push_back(assists_);
		int fightCapacity =floders->goods().equipmentdetail().fightcapacity();
		equipui->refineCost+= fightCapacity/5+1;
		equipui->refreshExpValue(fightCapacity/5+1,false);
		GameView::getInstance()->pacPageView->SetCurFolderGray(true,floders->id());
	}else
	{
		EquipMentUi::assistStoneStruct assistStone = {equipui->mes_source,equipui->pageView->curPackageItemIndex,equipui->stoneOfAmountRefine,0,mes_tag,floders->goods().binding()};
		equipui->assStoneVector.push_back(assistStone);

		MapStrngthStone * mapStone = StrengthStoneConfigData::s_StrengthStrone[floders->goods().id()];
		int addEx =  mapStone->get_exp();
		equipui->refreshExpValue(addEx*(equipui->stoneOfAmountRefine),false);

		int goodsCount_ = GameView::getInstance()->AllPacItem.at(equipui->pageView->curPackageItemIndex)->quantity();
		char goodsUseNum[50];
		sprintf(goodsUseNum,"%d",equipui->stoneOfAmountRefine);
		char goodsAllNum[50];
		sprintf(goodsAllNum,"%d",goodsCount_);

		std::string goodsUseAndCount = goodsUseNum;
		goodsUseAndCount.append("/");
		goodsUseAndCount.append(goodsAllNum);

		GameView::getInstance()->pacPageView->SetCurFolderGray(true,floders->id(),goodsUseAndCount);
	}
	*/
	delete floders;
}

void RefreshEquipData::addExpLabelAction(Node* sender, void* data)
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	auto equipui = (EquipMentUi *)mainscene->getChildByTag(ktagEquipMentUI);

	int addExp_ = (int)data;
	std::string string_ ="+";
	char tempStr[10];
	sprintf(tempStr,"%d",addExp_);
	string_.append(tempStr);
	auto label_ = Label::createWithBMFont("res_ui/font/ziti_3.fnt", string_.c_str());
	label_->setAnchorPoint(Vec2(0.5f,0.5f));
	label_->setPosition(Vec2(280,150));
	equipui->addChild(label_,30);

	auto action =(ActionInterval *)Sequence::create(
		Show::create(),
		MoveBy::create(0.5f,Vec2(0,20)),
		DelayTime::create(1.0f),
		RemoveSelf::create(),
		NULL);

	label_->runAction(action);


	equipui->refreshExpValue(addExp_,false);

}

std::string RefreshEquipData::getCurEquipSp(int quality_ )
{
	int equipmentquality_ = quality_;
	std::string frameColorPath = "res_ui/";
	if (equipmentquality_ == 1)
	{
		frameColorPath.append("sdi_white");
	}
	else if (equipmentquality_ == 2)
	{
		frameColorPath.append("sdi_green");
	}
	else if (equipmentquality_ == 3)
	{
		frameColorPath.append("sdi_bule");
	}
	else if (equipmentquality_ == 4)
	{
		frameColorPath.append("sdi_purple");
	}
	else if (equipmentquality_ == 5)
	{
		frameColorPath.append("sdi_orange");
	}
	else
	{
		frameColorPath.append("sdi_white");
	}
	frameColorPath.append(".png");

	return frameColorPath;
}
