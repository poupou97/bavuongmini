#include "GeneralEquipMent.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "EquipMentUi.h"
#include "../backpackscene/PackageItemInfo.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "AppMacros.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../gamescene_state/role/MyPlayer.h"

GeneralEquipMent::GeneralEquipMent(void)
{
}

GeneralEquipMent::~GeneralEquipMent(void)
{
	delete generalEquipment;
}

GeneralEquipMent * GeneralEquipMent::create(CEquipment * equipment)
{
	auto generalEquip =new GeneralEquipMent();
	if (generalEquip &&generalEquip->init(equipment))
	{
		generalEquip->autorelease();
		return generalEquip;
	}
	CC_SAFE_DELETE(generalEquip);
	return NULL;
}

bool GeneralEquipMent::init(CEquipment * equipment)
{
	if (UIScene::init())
	{

		auto equipui =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);

		std::string frameColorPath = "res_ui/";
		if (equipment->goods().equipmentdetail().quality() == 1)
		{
			frameColorPath.append("sdi_white");
		}
		else if (equipment->goods().equipmentdetail().quality() == 2)
		{
			frameColorPath.append("sdi_green");
		}
		else if (equipment->goods().equipmentdetail().quality() == 3)
		{
			frameColorPath.append("sdi_bule");
		}
		else if (equipment->goods().equipmentdetail().quality() == 4)
		{
			frameColorPath.append("sdi_purple");
		}
		else if (equipment->goods().equipmentdetail().quality() == 5)
		{
			frameColorPath.append("sdi_orange");
		}
		else
		{
			frameColorPath.append("sdi_white");
		}
		frameColorPath.append(".png");

		Btn_pacItemFrame = Button::create();
		Btn_pacItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnabled(true);
		Btn_pacItemFrame->setPressedActionEnabled(true);
		Btn_pacItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(Vec2(0,0));
		Btn_pacItemFrame->setTag(equipment->part());
		Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(GeneralEquipMent::showEquipMentInfo, this));
		m_pLayer->addChild(Btn_pacItemFrame);

		std::string goodsInfoStr ="res_ui/props_icon/";
		goodsInfoStr.append(equipment->goods().icon());
		goodsInfoStr.append(".png");

		auto equipIcon_ =ImageView::create();
		equipIcon_->loadTexture(goodsInfoStr.c_str());
		equipIcon_->setAnchorPoint(Vec2(0.5f,0.5f));
		equipIcon_->setPosition(Vec2(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		if (equipment->goods().binding() == 1)
		{
			auto ImageView_bound = ImageView::create();
			ImageView_bound->loadTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(Vec2(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(Vec2(-25,23));
			Btn_pacItemFrame->addChild(ImageView_bound);
		}


		int starLv_ =equipment->goods().equipmentdetail().starlevel();
		if (starLv_>0)
		{
			char starLvLabel_[5];
			sprintf(starLvLabel_,"%d",starLv_);
			auto label_ =Label::createWithTTF(starLvLabel_, APP_FONT_NAME, 13);
			label_->setAnchorPoint(Vec2(0,0));
			label_->setPosition(Vec2(-23,-23));
			Btn_pacItemFrame->addChild(label_);

			auto imageStar_ =ImageView::create();
			imageStar_->loadTexture("res_ui/star_on.png");
			imageStar_->setScale(0.7f);
			imageStar_->setAnchorPoint(Vec2(0,0));
			imageStar_->setPosition(Vec2(label_->getContentSize().width-22,label_->getPosition().y));
			Btn_pacItemFrame->addChild(imageStar_);
		}

		int strengthLv_ = equipment->goods().equipmentdetail().gradelevel();
		if (strengthLv_>0)
		{
			char strengthLv[5];
			sprintf(strengthLv,"%d",strengthLv_);
			std::string strengthStr_ = "+";
			strengthStr_.append(strengthLv);

			auto label_StrengthLv = Label::createWithTTF(strengthStr_.c_str(), APP_FONT_NAME, 13);
			label_StrengthLv->setAnchorPoint(Vec2(1,1));
			label_StrengthLv->setPosition(Vec2(23,25));
			Btn_pacItemFrame->addChild(label_StrengthLv);
		}
	
		if (equipment->goods().equipmentdetail().durable()<=10)
		{
			auto image_sp = ImageView::create();
			image_sp->setAnchorPoint(Vec2(0.5,0.5f));
			image_sp->setPosition(Vec2(0,0));
			image_sp->loadTexture("res_ui/mengban_red55.png");
			image_sp->setScale9Enabled(true);
			image_sp->setCapInsets(Rect(5,5,1,1));
			image_sp->setContentSize(Btn_pacItemFrame->getContentSize());
			Btn_pacItemFrame->addChild(image_sp);

			auto l_des = Label::createWithTTF(StringDataManager::getString("packageitem_notAvailable"), APP_FONT_NAME, 18);
			l_des->setAnchorPoint(Vec2(0.5f,0.5f));
			l_des->setPosition(Vec2(0,0));
			Btn_pacItemFrame->addChild(l_des);
		}

		switch(equipment->part())
		{
		case 4 :
			{
				this->setPosition(Vec2(438.5f, 358.5f));    //Œ‰∆˜ 4
				equipui->image_arms->setVisible(false);

			}break;
		case 2 :
			{
				this->setPosition(Vec2(438.5f, 277.5f));   //“¬∑˛ 2
				equipui->image_clohes->setVisible(false);
			}break;
		case 0 :
			{
				this->setPosition(Vec2(701, 358.5f));    //Õ∑ø¯  0
				equipui->image_helmet->setVisible(false);
			}break;
		case 7 :
			{
				this->setPosition(Vec2(438.5f, 195));    //Ω‰÷∏7
				equipui->image_ring->setVisible(false);
			}break;
		case 3 :
			{
				this->setPosition(Vec2(701, 195));    //—¸¥¯ 3
				equipui->image_accessories->setVisible(false);
			}break;
		case 9 :
			{
				this->setPosition(Vec2(701, 277.5f));    //–¨ 9
				equipui->image_shoes->setVisible(false);
			}break;
		}
		generalEquipment =new CEquipment();
		generalEquipment->CopyFrom(*equipment);

		m_mengban_black = ImageView::create();
		m_mengban_black->loadTexture("res_ui/mengban_black65.png");
		m_mengban_black->setScale9Enabled(true);
		m_mengban_black->setContentSize(Size(63,63));
		m_mengban_black->setCapInsets(Rect(5,5,1,1));
		m_mengban_black->setAnchorPoint(Vec2(0.5f,0.5f));
		m_pLayer->addChild(m_mengban_black);
		m_mengban_black->setVisible(false);

		m_select_state = ImageView::create();
		m_select_state->loadTexture("res_ui/greengou.png");
		m_select_state->setAnchorPoint(Vec2(0.5f,0.5f));
		m_select_state->setPosition(Vec2(0,m_mengban_black->getContentSize().height/2 - 2));
		m_mengban_black->addChild(m_select_state);
		m_select_state->setVisible(false);


		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Btn_pacItemFrame->getContentSize());
		return true;
	}
	return false;
}


void GeneralEquipMent::onEnter()
{
	UIScene::onEnter();
}

void GeneralEquipMent::onExit()
{
	UIScene::onExit();
}

void GeneralEquipMent::showEquipMentInfo(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);
		Size winsize = Director::getInstance()->getVisibleSize();
		auto btn = (Button *)pSender;
		/*
		EquipMentUi * equipui =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		equipui->addRoleEquipment(generalEquipment);
		*/

		auto equipInfo = GeneralEquipMentInfo::create(generalEquipment);
		equipInfo->setIgnoreAnchorPointForPosition(false);
		equipInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
		equipInfo->setTag(kTagGoodsItemInfoBase);
		//equipInfo->setLocalZOrder(100);
		GameView::getInstance()->getMainUIScene()->addChild(equipInfo);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GeneralEquipMent::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralEquipMent::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,40,70);
// 	tutorialIndicator->setPosition(Vec2(pos.x-40,pos.y+100));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,63,63,true);
	//tutorialIndicator->setDrawNodePos(Vec2(pos.x+27+_w,pos.y+10+_h));//438.5f, 358.5f
	tutorialIndicator->setDrawNodePos(Vec2(438.5f+_w, 358.5f+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralEquipMent::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void GeneralEquipMent::SetCurgeneralEquipGray(bool ptouched)
{
	if (ptouched)
	{
		Btn_pacItemFrame->setTouchEnabled(false);
		m_mengban_black->setVisible(true);
		m_select_state->setVisible(true);
	}else
	{
		Btn_pacItemFrame->setTouchEnabled(true);
		m_mengban_black->setVisible(false);
		m_select_state->setVisible(false);
	}
}

bool GeneralEquipMent::onTouchBegan( Touch *touch, Event * pEvent )
{
	return false;
	//return this->resignFirstResponder(touch,this,false);
}

void GeneralEquipMent::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void GeneralEquipMent::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void GeneralEquipMent::onTouchMoved( Touch *touch, Event * pEvent )
{

}

///////////////////////////////////
GeneralEquipMentInfo::GeneralEquipMentInfo()
{

}

GeneralEquipMentInfo::~GeneralEquipMentInfo()
{
	delete goods_;
	delete curEquip_;
}

GeneralEquipMentInfo * GeneralEquipMentInfo::create( CEquipment * equipment )
{
	auto generalEquip =new GeneralEquipMentInfo();
	if (generalEquip && generalEquip->init(equipment))
	{
		generalEquip->autorelease();
		return generalEquip;
	}
	CC_SAFE_DELETE(generalEquip);
	return NULL;

}
bool GeneralEquipMentInfo::init( CEquipment * equipment)
{
	goods_ =new GoodsInfo();
	goods_->CopyFrom(equipment->goods());

	curEquip_ =new CEquipment();
	curEquip_->CopyFrom(*equipment);
	
	if (GoodsItemInfoBase::init(goods_,GameView::getInstance()->EquipListItem,0))
	{
		equipui =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		const char *strings_ = StringDataManager::getString("goods_equipment_main");
		//add main equip
		btn_addMainEquip= Button::create();
		btn_addMainEquip->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		btn_addMainEquip->setTouchEnabled(true);
		btn_addMainEquip->setPressedActionEnabled(true);
		btn_addMainEquip->addTouchEventListener(CC_CALLBACK_2(GeneralEquipMentInfo::mainEquipment, this));
		btn_addMainEquip->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_addMainEquip->setScale9Enabled(true);
		btn_addMainEquip->setContentSize(Size(91,43));
		btn_addMainEquip->setCapInsets(Rect(18,9,2,23));
		btn_addMainEquip->setPosition(Vec2(141,33));

		auto Label_mainEquip = Label::createWithTTF(strings_, APP_FONT_NAME, 18);
		Label_mainEquip->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_mainEquip->setPosition(Vec2(0,0));
		btn_addMainEquip->addChild(Label_mainEquip);
		m_pLayer->addChild(btn_addMainEquip);
		
		return true;
	}
	return false;
}

void GeneralEquipMentInfo::onEnter()
{
	GoodsItemInfoBase::onEnter();
}

void GeneralEquipMentInfo::onExit()
{
	GoodsItemInfoBase::onExit();
}

bool GeneralEquipMentInfo::onTouchBegan( Touch * pTouch,Event * pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}

void GeneralEquipMentInfo::mainEquipment(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		equipui->generalMainId = equipui->mes_petid;

		equipui->addRoleEquipment(curEquip_);
		this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void GeneralEquipMentInfo::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralEquipMentInfo::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();

	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;

	int temp_x = pos.x+85+_w;
	int temp_y = pos.y+60+_h;

	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(26);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[26];
	}
	if (GameView::getInstance()->myplayer->getActiveRole()->level() < openlevel)
	{
		temp_x +=41;
	}
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,91,43,true);
	tutorialIndicator->setDrawNodePos(Vec2(temp_x,temp_y));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralEquipMentInfo::removeCCTutorialIndicator()
{
// 	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(m_pLayer->getChildByTag(CCTUTORIALINDICATORTAG));
// 	if(tutorialIndicator != NULL)
// 		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}




