#include "GeneralEquipRecover.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "EquipMentUi.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "GeneralEquipShowRecover.h"
#include "GeneralEquipMent.h"
#include "AppMacros.h"


GeneralEquipRecover::GeneralEquipRecover(void)
{
	m_useLevel =0;
	m_refineLevel =0;
	m_starLevel = 0;
	m_clazz = 0;
	m_quality = 0;
}


GeneralEquipRecover::~GeneralEquipRecover(void)
{
	delete equip_;
}

GeneralEquipRecover * GeneralEquipRecover::create( CEquipment * equipment ,bool isMainequip)
{
	auto generalequip =new GeneralEquipRecover();
	if (generalequip && generalequip->init(equipment,isMainequip))
	{
		generalequip->autorelease();
		return generalequip;
	}
	CC_SAFE_DELETE(generalequip);
	return NULL;
}

bool GeneralEquipRecover::init( CEquipment * equipment,bool isMainequip )
{
	equip_ =new CEquipment();
	equip_->CopyFrom(*equipment);
	if (UIScene::init())
	{
		m_clazz = equip_->goods().equipmentclazz();
		m_quality = equip_->goods().quality();
		std::string frameColorPath = "res_ui/";
		
		if (isMainequip == true)
		{
			if (equip_->goods().equipmentdetail().quality() == 1)
			{
				frameColorPath.append("bdi_white");
			}
			else if (equip_->goods().equipmentdetail().quality() == 2)
			{
				frameColorPath.append("bdi_green");
			}
			else if (equip_->goods().equipmentdetail().quality() == 3)
			{
				frameColorPath.append("bdi_blue");
			}
			else if (equip_->goods().equipmentdetail().quality() == 4)
			{
				frameColorPath.append("bdi_purple");
			}
			else if (equip_->goods().equipmentdetail().quality() == 5)
			{
				frameColorPath.append("bdi_orange");
			}
			else
			{
				frameColorPath.append("bdi_none");
			}
		}else
		{
			if (equip_->goods().equipmentdetail().quality() == 1)
			{
				frameColorPath.append("sdi_white");
			}
			else if (equip_->goods().equipmentdetail().quality() == 2)
			{
				frameColorPath.append("sdi_green");
			}
			else if (equip_->goods().equipmentdetail().quality() == 3)
			{
				frameColorPath.append("sdi_bule");
			}
			else if (equip_->goods().equipmentdetail().quality() == 4)
			{
				frameColorPath.append("sdi_purple");
			}
			else if (equip_->goods().equipmentdetail().quality() == 5)
			{
				frameColorPath.append("sdi_orange");
			}
			else
			{
				frameColorPath.append("sdi_white");
			}
		}
		
		frameColorPath.append(".png");

		Btn_pacItemFrame = Button::create();
		Btn_pacItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnabled(true);
		Btn_pacItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(Vec2(25,24));
		Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(GeneralEquipRecover::showEquipInfo, this));
		m_pLayer->addChild(Btn_pacItemFrame);

		m_useLevel = equip_->goods().uselevel();

		std::string goodsInfoStr ="res_ui/props_icon/";
		goodsInfoStr.append(equip_->goods().icon());
		goodsInfoStr.append(".png");

		auto equipIcon_ =ImageView::create();
		equipIcon_->loadTexture(goodsInfoStr.c_str());
		equipIcon_->setAnchorPoint(Vec2(0.5f,0.5f));
		equipIcon_->setPosition(Vec2(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		m_starLevel =equip_->goods().equipmentdetail().starlevel();
		char starLvLabel_[5];
		sprintf(starLvLabel_,"%d",m_starLevel);
		labelStarlevel_ =Label::createWithTTF(starLvLabel_, APP_FONT_NAME, 13);
		labelStarlevel_->setAnchorPoint(Vec2(0,0));
		labelStarlevel_->setPosition(Vec2(-25,-20));
		Btn_pacItemFrame->addChild(labelStarlevel_);

		imageStar_ =ImageView::create();
		imageStar_->loadTexture("res_ui/star_on.png");
		imageStar_->setScale(0.5f);
		imageStar_->setAnchorPoint(Vec2(0,0));
		imageStar_->setPosition(Vec2(labelStarlevel_->getContentSize().width-27,labelStarlevel_->getPosition().y));
		Btn_pacItemFrame->addChild(imageStar_);

		if (m_starLevel<=0)
		{
			labelStarlevel_->setVisible(false);
			imageStar_->setVisible(false);
		}

		m_refineLevel =equip_->goods().equipmentdetail().gradelevel();
		char strengthLv[5];
		sprintf(strengthLv,"%d",m_refineLevel);
		std::string strengthStr_ = "+";
		strengthStr_.append(strengthLv);
		label_refineLv = Label::createWithTTF(strengthStr_.c_str(), APP_FONT_NAME, 13);
		label_refineLv->setAnchorPoint(Vec2(1.0f,0.5f));
		label_refineLv->setPosition(Vec2(22,15));//22 24
		Btn_pacItemFrame->addChild(label_refineLv);

		if (m_refineLevel<=0)
		{
			label_refineLv->setVisible(false);
		}

		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Btn_pacItemFrame->getContentSize());
		return true;
	}
	return false;
}

void GeneralEquipRecover::onEnter()
{
	UIScene::onEnter();
}

void GeneralEquipRecover::onExit()
{
	UIScene::onExit();
}

void GeneralEquipRecover::showEquipInfo(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		int tag_ = this->getTag();
		/*
		Size size= Director::getInstance()->getVisibleSize();
		EquipMentUi * equipmentui = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		GeneralEquipShowRecover *auction =GeneralEquipShowRecover::create(equip_,equipmentui->generalEquipVector,equipmentui->mes_petid);
		auction->setIgnoreAnchorPointForPosition(false);
		auction->setAnchorPoint(Vec2(0.5f,0.5f));
		auction->setTag(tag_);
		GameView::getInstance()->getMainUIScene()->addChild(auction);
		*/
		//
		auto equip = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		equip->getBackGeneralMainEquip(tag_);

		// 	std::vector<CEquipment *>::iterator iter;
		// 	for (iter = equip->curMainEquipvector.begin(); iter != equip->curMainEquipvector.end();++iter)
		// 	{
		// 		delete * iter;
		// 	}
		// 	equip->curMainEquipvector.clear();
		// 	for (int i = 0;i<equip->generalEquipVector.size();i++)
		// 	{
		// 		CEquipment * temp_ =new CEquipment();
		// 		temp_->CopyFrom( *equip->generalEquipVector.at(i));
		// 		equip->curMainEquipvector.push_back(temp_);
		// 	}

		// 	if (equip->refineMainEquip == false)
		// 	{
		// 		equip->generalMainId = equip->mes_petid;
		// 	}
		equip->stoneOfAmountRefine = 0;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

CEquipment * GeneralEquipRecover::getEquipment()
{
	return equip_;
}

int GeneralEquipRecover::getEquipUseLevel()
{
	return m_useLevel;
}

void GeneralEquipRecover::setStarLevel( int level_,bool isAction )
{
	m_starLevel = level_;

	if (level_ > 0)
	{
		if (level_ >=20)
		{
			level_ =20;
		}
		char starLvLabel_[5];
		sprintf(starLvLabel_,"%d",level_);
		labelStarlevel_->setString(starLvLabel_);
		labelStarlevel_->setVisible(true);
		imageStar_->setPosition(Vec2(labelStarlevel_->getContentSize().width-27,labelStarlevel_->getPosition().y));
		imageStar_->setVisible(true);

		if (isAction)
		{
			auto seq_ =(ActionInterval *) Sequence::create(
				ScaleTo::create(0.01f,10.0f),
				ScaleTo::create(0.3f,1.0f),
				NULL);
			labelStarlevel_->runAction(seq_);

			auto seq_1 = Sequence::create(
				ScaleTo::create(0.01f,5.0f),
				ScaleTo::create(0.3f,0.5f),
				NULL);
			imageStar_->runAction(seq_1);
		}
	}
}

int GeneralEquipRecover::getStarLevel()
{
	return m_starLevel;
}

void GeneralEquipRecover::setRefineLevel( int level_,bool isAction )
{
	m_refineLevel = level_;

	if (level_ > 0)
	{
		if (level_ >=20)
		{
			level_ =20;
		}
		char strengthLv[5];
		sprintf(strengthLv,"%d",level_);
		std::string strengthStr_ = "+";
		strengthStr_.append(strengthLv);

		label_refineLv->setString(strengthStr_.c_str());
		label_refineLv->setVisible(true);

		if (isAction)
		{
			auto seq_ = Sequence::create(
				ScaleTo::create(0.01f,10.0f),
				ScaleTo::create(0.3f,1.0f),
				NULL);
			label_refineLv->runAction(seq_);
		}
	}else
	{
		label_refineLv->setVisible(false);
	}
}

int GeneralEquipRecover::getRefineLevel()
{
	return m_refineLevel;
}

int GeneralEquipRecover::getClazz()
{
	return m_clazz;
}

int GeneralEquipRecover::getQuality()
{
	return m_quality;
}
