#include "EquipMentGeneralList.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../generals_ui/GeneralsUI.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../backpackscene/PackageScene.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "EquipMentUi.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CActiveRole.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../AppMacros.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../utils/StaticDataManager.h"
#include "../../utils/GameUtils.h"
#include "../../ui/extensions/RecruitGeneralCard.h"

#define  SelectImageTag 458

EquipMentGeneralList::EquipMentGeneralList(void)
{
}


EquipMentGeneralList::~EquipMentGeneralList(void)
{
}

EquipMentGeneralList * EquipMentGeneralList::create()
{
	EquipMentGeneralList * generalList =new EquipMentGeneralList();
	if (generalList &&generalList->init())
	{
		generalList->autorelease();
		return generalList;
	}
	CC_SAFE_DELETE(generalList);
	return NULL;
}

bool EquipMentGeneralList::init()
{
	if(GeneralsListBase::init())
	{

		Size winsize = Director::getInstance()->getVisibleSize();

		this->generalsListStatus = HaveNext;
		//general list
		generalList_tableView = TableView::create(this,Size(340,106));
		//generalList_tableView->setSelectedEnable(true);
		//generalList_tableView->setSelectedScale9Texture("res_ui/highlight.png", Rect(26, 26, 1, 1), Vec2(0,-2));
		generalList_tableView->setDirection(TableView::Direction::HORIZONTAL);
		generalList_tableView->setAnchorPoint(Vec2(0,0));
		generalList_tableView->setPosition(Vec2(0,0));
		generalList_tableView->setContentOffset(Vec2(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//generalList_tableView->setPressedActionEnabled(true);
		addChild(generalList_tableView);

		//add flag
		auto tempLayer_flag = Layer::create();
// 		tempLayer_flag->setIgnoreAnchorPointForPosition(false);
// 		tempLayer_flag->setAnchorPoint(Vec2(0.5f,0.5f));
// 		tempLayer_flag->setContentSize(Size(800, 480));
// 		tempLayer_flag->setPosition(Vec2(winsize.width/2,winsize.height/2));
		tempLayer_flag->setLocalZOrder(5);
		addChild(tempLayer_flag);

		auto imageView_leftFlag = ImageView::create();
		imageView_leftFlag->loadTexture("res_ui/shousuo.png");
		imageView_leftFlag->setAnchorPoint(Vec2(0.5f,0.5f));
		//imageView_leftFlag->setPosition(Vec2(382,92));
		imageView_leftFlag->setPosition(Vec2(9,55));//-16  55
		imageView_leftFlag->setRotation(-135);
		imageView_leftFlag->setScale(0.5f);
		tempLayer_flag->addChild(imageView_leftFlag);

		auto imageView_rightFlag = ImageView::create();
		imageView_rightFlag->loadTexture("res_ui/shousuo.png");
		imageView_rightFlag->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_rightFlag->setPosition(Vec2(331,55));//703 92
		imageView_rightFlag->setRotation(45);
		imageView_rightFlag->setScale(0.5f);
		tempLayer_flag->addChild(imageView_rightFlag);

		//open level
		int openlevel = 0;
		std::map<int,int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(26);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[26];
		}

		if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
		{
			auto btn_mengban = Button::create();
			btn_mengban->setTouchEnabled(true);
			btn_mengban->loadTextures("res_ui/renwubeibao/mengban_green.png","res_ui/renwubeibao/mengban_green.png","");
			btn_mengban->setScale9Enabled(true);
			btn_mengban->setContentSize(Size(343,108));
			btn_mengban->setAnchorPoint(Vec2(0.5f,0.5f));
			btn_mengban->setPosition(Vec2(169,51));
			btn_mengban->setOpacity(200);
			tempLayer_flag->addChild(btn_mengban);

			auto image_desFrame = ImageView::create();
			image_desFrame->loadTexture("res_ui/renwubeibao/zi_mengban.png");
			image_desFrame->setScale9Enabled(true);
			image_desFrame->setContentSize(Size(240,29));
			image_desFrame->setCapInsets(Rect(56,11,1,1));
			image_desFrame->setAnchorPoint(Vec2(0.5f,0.5f));
			image_desFrame->setPosition(Vec2(169,51));
			image_desFrame->setOpacity(200);
			tempLayer_flag->addChild(image_desFrame);
			std::string str_des = "";
			char s_lv [10];
			sprintf(s_lv,"%d",openlevel);
			str_des.append(s_lv);
			str_des.append(StringDataManager::getString("package_openGeneralEquipment"));

			auto l_des = Label::createWithTTF(str_des.c_str(), APP_FONT_NAME, 20);
			l_des->setAnchorPoint(Vec2(0.5f,0.5f));
			l_des->setPosition(Vec2(169,51));
			tempLayer_flag->addChild(l_des);
		}

		return true;
	}
	return false;
}

void EquipMentGeneralList::onEnter()
{
	GeneralsListBase::onEnter();
}

void EquipMentGeneralList::onExit()
{
	GeneralsListBase::onExit();
}


void EquipMentGeneralList::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void EquipMentGeneralList::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void EquipMentGeneralList::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void EquipMentGeneralList::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void EquipMentGeneralList::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void EquipMentGeneralList::tableCellTouched( TableView* table, TableViewCell* cell )
{
	int index_ = cell->getIdx();

	auto equipui =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	equipui->tabviewSelectIndex = index_;
	if (index_ == 0)
	{
		equipui->mes_petid = 0;
		equipui->refreshRoleInfo((CActiveRole*)GameView::getInstance()->myplayer->getActiveRole(),
									GameView::getInstance()->myplayer->player->fightpoint(),
									GameView::getInstance()->EquipListItem);
	}else
	{
		auto tempCell = dynamic_cast<GeneralsSmallHeadCell*>(cell);
		if (tempCell != NULL)
		{
			curGeneralBaseMsg = tempCell->getCurGeneralBaseMsg();
			equipui->mes_petid = curGeneralBaseMsg->id();
			//请求详细信息
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *) curGeneralBaseMsg->id());
		}
		
		/*
		GeneralsSmallHeadCell * tempCell = dynamic_cast<GeneralsSmallHeadCell*>(cell);
		if (tempCell != NULL)
		{
			curGeneralBaseMsg = tempCell->getCurGeneralBaseMsg();
			equipui->mes_petid = curGeneralBaseMsg->id();
		}
		std::vector<CEquipment *> tempGeneralEquipVector;
		CActiveRole * activeRole_ = new CActiveRole();
		int generalSize = GameView::getInstance()->generalsInLineDetailList.size();
		for (int i = 0;i<generalSize;i++)
		{
			if (equipui->mes_petid ==GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
			{
				activeRole_->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->activerole());
				int fight_ = GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint();

				int equipSize = GameView::getInstance()->generalsInLineDetailList.at(i)->equipments_size();
				for (int j = 0;j<equipSize;j++)
				{
					CEquipment * equip_ =new CEquipment();
					equip_->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->equipments(j));
					tempGeneralEquipVector.push_back(equip_);
				}
				equipui->refreshRoleInfo(activeRole_,fight_,tempGeneralEquipVector);
				break;
			}
		}

		std::vector<CEquipment*>::iterator iter;
		for (iter = tempGeneralEquipVector.begin(); iter != tempGeneralEquipVector.end(); ++iter)
		{
			delete *iter;
		}
		tempGeneralEquipVector.clear();

		delete activeRole_;
		*/
	}
}

cocos2d::Size EquipMentGeneralList::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(81,106);
}

cocos2d::extension::TableViewCell* EquipMentGeneralList::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	if (idx == 0)
	{
		cell = new TableViewCell();
		cell->autorelease();

		auto smaillFrame = Sprite::create("res_ui/generals_white.png");
		smaillFrame->setAnchorPoint(Vec2(0, 0));
		smaillFrame->setPosition(Vec2(0, 0));
		smaillFrame->setScaleX(0.75f);
		smaillFrame->setScaleY(0.7f);
		cell->addChild(smaillFrame);
		//列表中的头像图标
		auto sprite_icon = Sprite::create(BasePlayer::getBigHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str());
		sprite_icon->setAnchorPoint(Vec2(0.5f, 0.5f));
		sprite_icon->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), smaillFrame->getContentSize().height/2*smaillFrame->getScaleY()));
		sprite_icon->setScale(0.75f);
		cell->addChild(sprite_icon);
		//等级
// 		std::string _lv = "LV";
// 		char s[10];
// 		sprintf(s,"%d",GameView::getInstance()->myplayer->getActiveRole()->level());
// 		_lv.append(s);
// 		Label * label_lv = Label::createWithTTF(s,APP_FONT_NAME,13);
// 		label_lv->setAnchorPoint(Vec2(1.0f, 0));
// 		label_lv->setPosition(Vec2(smaillFrame->getContentSize().width*smaillFrame->getScaleX()-5, 20));
// 		label_lv->enableStroke(Color3B(0, 0, 0), 2.0f);
// 		cell->addChild(label_lv);
		//武将名字黑底
		auto sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/name_di3.png");
		sprite_nameFrame->setCapInsets(Rect(10,10,1,5));
		sprite_nameFrame->setAnchorPoint(Vec2(0.5f, 0));
		sprite_nameFrame->setContentSize(Size(72,13));
		sprite_nameFrame->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 8));
		cell->addChild(sprite_nameFrame);
		//武将名字
		const char * generalRoleName_ = StringDataManager::getString("generalList_RoleName");
		auto label_name = Label::createWithTTF(generalRoleName_,APP_FONT_NAME,14);
		label_name->setAnchorPoint(Vec2(0.5f, 0));
		label_name->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 5));
		//label_name->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		label_name->setColor(Color3B(0,255,0));
		cell->addChild(label_name);
		//profession
		std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(GameView::getInstance()->myplayer->getProfession());
		auto sprite_profession = Sprite::create(generalProfess_.c_str());
		sprite_profession->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_profession->setPosition(Vec2(62 ,
			32));
		sprite_profession->setScale(0.6f);
		cell->addChild(sprite_profession);
	}
	else
	{
		cell = GeneralsSmallHeadCell::create(GameView::getInstance()->generalsInLineList.at(idx-1));
	}
	return cell;
}

ssize_t EquipMentGeneralList::numberOfCellsInTableView( TableView *table )
{
	return GameView::getInstance()->generalsInLineList.size()+1;
}

void EquipMentGeneralList::ReloadGeneralsTableView()
{
	Vec2 offset = this->generalList_tableView->getContentOffset();
	this->generalList_tableView->reloadData();
}

void EquipMentGeneralList::ReloadGeneralsTableViewWithoutChangeOffSet()
{
	Vec2 offset = this->generalList_tableView->getContentOffset();
	this->generalList_tableView->reloadData();
	this->generalList_tableView->setContentOffset(offset);
}

