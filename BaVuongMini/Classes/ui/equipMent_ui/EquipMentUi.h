#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/EquipmentMessage.pb.h"
#include "EquipMentGeneralList.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../messageclient/element/CActiveRole.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class PacPageView;
class CAdditionProperty;
class EquipmentMessage;
class CEquipHole;
class CEquipment;
class FolderInfo;
class GoodsInfo;
class UITab;
enum
{
	//change  tab type 
	REFININGTYPE = 0,
	STARTYPE = 2,
	GEMTYPE = 1,
	BAPTIZETYPE = 3,
	SYSTHESISTYPE = 4,
	///add main equip tag 
	REFININGMAINEQUIP=11,
	STAREQUIP=12,
	GEMEQUIP=13,
	BAPTIZEEQUIP=14,

	//add refine assisment
	FRISTASSIST=21,
	SECONDASSIST=22,
	THIRDASSIST=23,
	FOURASSIST=24,
	FIVESTRONGTH=25,

	SHOWGEMVALUELAYER = 50,
	
	GENERALREFINEEQUIP =110,
	GENERALSTAREQUIP =120,
	GENERALGEMEQUIP=130,
	GENERALBAPTIZEEQUIP =140,

	GENERALFIRSTASSIST =210,
	GENERALSECONDASSIST =220,
	GENERALTHIRDASSIST =230,
	GENERALFOURASSIST =240,
	GENERALFIVEASSIST = 250,
	//add property to layer
	REFINEPROPERTYLAYER= 800,
	STARPROPERTYLAYER= 810,
	GEMPROPERTYLAYER = 820,
	BAPTIZEPROPERTYLAYER= 830,
	//add stone tag
	STONELAYERTAG =89,
	//add general equipment 
	GENERALEQUIPMENTLIST =90,
	ROLEANMITIONTAG = 299,
	EQUIPMENTROLEANIMATIONTAG = 300,
	CLASS_SYSTHESIS_TAG = 301,
	//end look equip
	REFINELOOKEQUIP = 801,
	GENERALREFINEEQUIPLOOK = 802,

	//end look equip
	STARLOOKEQUIP = 803,
	GENERALSTARQUIPLOOK = 804,
};

enum operatorType {
	// delete  101, getback 102
	operatorGetBack = 101,    
	operatorDelete  = 102,
};

class EquipMentUi:public UIScene
{
public:
	EquipMentUi(void);
	~EquipMentUi(void);

	static EquipMentUi *create();
	bool init();
	void onEnter();
	void onExit();
	int currType;
	Size size;
	Layer * refineLayer;
	Layer * starPropertyLayer;
	Layer * gemLayer;
	Layer * baptizePropertylayer;
	Layer * synthesisLayer;

	UITab * equipTab;
	UITab * roleAndPack;

	bool isRefreshPacGoods;
	void refreshBackPack(int curtype,int equipStoneLevel);
	void callBackChangeTab(Ref * obj);
	void callBackPackAndRole(Ref * obj);

	void refreshCurState();

	void callBackClearUp(Ref * obj);
	void callBackShop(Ref *pSender, Widget::TouchEventType type);
	void callBackExit(Ref *pSender, Widget::TouchEventType type);

	Layer * roleLayer;
	ImageView * image_rolePression;
	Text * label_roleLevel;
	Text * label_roleFight;
	Label * label_roleTotalFight;
	EquipMentGeneralList * generalList;
	void refreshRoleInfo(CActiveRole * activeRole,int fight,std::vector<CEquipment *>equipmentsVector);

	Text * label_ingot;
	Text * label_gold;
	Text * label_goldBingding;
	void refreshPlayerMoney();
	void removeAssistAndStone(int operateType);

	Layout* equipPanel;
	bool mainEquipIsPac;
	int mainEquipIndexRefine;
	int generalEquipOfPart;
	
	bool refineMainEquip;
	bool starMainEquip;
	bool gemMainEquip;
	bool baptizeMainEquip;

	bool StrengthStone;
	
	void initRefine();
	void initStar();
	void initGem();
	void initBaptize();
	void initSynthesis();
	
	//add all button is des
	void callBackRefineMainDes(Ref *pSender, Widget::TouchEventType type);
	void callBackAssistEquipDes(Ref *pSender, Widget::TouchEventType type);

	void callBackStarMainDes(Ref *pSender, Widget::TouchEventType type);
	void callBackStarAssistDes(Ref *pSender, Widget::TouchEventType type);

	void callBackGemMainDes(Ref *pSender, Widget::TouchEventType type);
	void callBackGemAssistDes(Ref *pSender, Widget::TouchEventType type);


	void callBackBaptizeMainDes(Ref *pSender, Widget::TouchEventType type);

	void isSureEquip(int index);
	//refine
	bool firstAssist;
	bool secondtAssist;
	bool thirdAssist;
	bool fourAssist;
	bool fiveAssist;
	int addAllEx;
	int curExValue;
	int allExValue;
	int refineCost;
	//role equip image 
	ImageView * image_arms;
	ImageView * image_clohes;
	ImageView * image_ring;
	ImageView * image_helmet;
	ImageView * image_shoes;
	ImageView * image_accessories;

	ImageView * refine_textAreaPutEquipment;
	Label * labelFont_curRefinelevel;
	ProgressTimer * exPercent_refine;
	ImageView * exPercent_refineAdd;
	//Label * exLabel_refine;
	Label * exLabel_refine;
	Text * labelCost;

	Text * refineBeginFirstLabel;
	Text * refineBeginSecondLabel;
	Text * refineBeginFirstvalue;
	Text * refineBeginSecondvalue;

	Text * refineEndFirstLabel;
	Text * refineEndSecondLabel;

	Text * refineEndFirstvalue;
	Text * refineEndSecondvalue;

	Text * refineEndFirstvalueAdd;
	Text * refineEndSecondvalueAdd;

	int equipLookRefineLevel_;//��ǰԤ���װ���ľ���ȼ�
	int m_equipBeginLevel;
	void setEquipBeginLevel(int curLv_);
	int getEquipBeginLevel();
	int curBeginExp;
	void addRoleEquipment(CEquipment * equips);
	void addMainEquipOfGeneral(CEquipment * equips);
	void getBackGeneralMainEquip(int layerTag);
	void addAssistGeneralOfEquip(CEquipment * equips);
	
	long long generalMainId;
	long long curSelectGeneralId;
	long long equipmentInstanceid;
	std::vector<CEquipment *>curMainEquipvector;
	CEquipment * getCurRoleEquipment(int index);

	void addOperationFloder(FolderInfo * floders);
	void addMainEquipment( FolderInfo * floders );
	void addAssistEquipment(FolderInfo * floders,bool isAction = true);
	void getBackMainEquip(int tag,int index);
	bool isCanBaptize(GoodsInfo * goods);

	void refreshExpValue(int refineExvalue,bool isScale,bool isAction = true);
	void getRefineAdditionPropertValue(int equipLookLevel);

	void addExPercentSetPrecentIsFull();
	void addExPercentSetPrecentIsCur();

	void callBackBtn_Refine(Ref *pSender, Widget::TouchEventType type);
	void callBackBtn_addAll(Ref *pSender, Widget::TouchEventType type);
	
	//star
	ImageView * star_textArea_putEquipment;

	Text * starBeginFirstLabel;
	Text * starBeginSecondLabel;
	Text * starBeginThirdLabel;
	Text * starBeginFourLabel;
	
	Text * starBeginFirstvalue;
	Text * starBeginSecondvalue;
	Text * starBeginThirdvalue;
	Text * starBeginFourvalue;

	Text * starEndFirstLabel;
	Text * starEndSecondLabel;
	Text * starEndThirdLabel;
	Text * starFourFourLabel;

	Text * starEndFirstvalue;
	Text * starEndSecondvalue;
	Text * starEndThirdvalue;
	Text * starEndFourvalue;

	Text * starEndFirstvalueAdd;
	Text * starEndSecondvalueAdd;
	Text * starEndThirdvalueAdd;
	Text * starEndFourvalueAdd;

	std::vector<CAdditionProperty *> additionProperty;
	void refreshEquipProperty(int curValue,int allValue,bool isAction);
	int m_curLuckValue;
	int m_allLuckValue;
	int starCost;
	int starLevelValue;
	Text * exLabel_star;
	
	ImageView * exPercent_star;
	
	int m_curEquipStarEx;
	void setCurEquipExStar(int value_);
	int getCurEquipExStar();

	Text * successRate;
	Text * starCost_label;
	void refreshStarLuckValue(int curluckval,int starCostvale,bool isAction);

	Button * btn_upStar;
	Button *  btn_allStar;

	int stoneOfIndexRefine;
	int stoneOfAmountRefine;

	int curMainEquipStarLevel;

	int stoneOfIndexStar;
	int stoneOfAmountStar;
	std::vector<CAdditionProperty *> additionProperty_star;
	void refreshEquipProperty_star(int curluckval,int allluckval,int starCostVale,bool isAction = false);

	void callBackStarDes(Ref *pSender, Widget::TouchEventType type);

	void callBackUpstar(Ref *pSender, Widget::TouchEventType type);
	void callBackAllStar(Ref *pSender, Widget::TouchEventType type);
	//� ������ϵ�һ�������ʱû�)
	void addStrengthStone();
	void strengthStoneCounter(Ref * obj);
	////gem
	ImageView * gem_textArea_putEquipment;
	ImageView * imageLock1;
	ImageView * imageLock2;
	ImageView * imageLock3;

	Text * holeLabelFirst;
	Text * holeLabelSecond;
	Text * holeLabelThird;

	ImageView * gemBackGround_1;
	ImageView * gemBackGround_2;
	ImageView * gemBackGround_3;

	Button * gem_buttonPick1;
	Button * gem_buttonPick2;
	Button * gem_buttonPick3;

	//Label * holeStoneCostLabel;
	//Label * holeCostLabelGem;

	Label * holeStoneAmoumtBingdingLabel;
	Label * holeStoneAmountNotBingdingLabel;

	ui::CheckBox * useGemBingding;

	Button * gem_buttonHole_first;
	Button * gem_buttonHole_second;
	Button * gem_buttonHole_third;

	int holeFalg;//get already hole id flag
	void callBackHole(Ref *pSender, Widget::TouchEventType type);

	void isSureNotHoleBingding(Ref * obj);
	void isSureBingdingHoleBingding(Ref * obj);
	int holeIndex_;
	//void addGemEquipment(std::string icon, std::string name, std::string effert);
	void addGemEquipment(int holeIndex);
	void addGemEquipCurHole(int holeIndex);
	//void equipMentInlaySure(Ref * obj);

	void equipFirstPickSure(Ref * obj);
	void equipSecondpickSure(Ref * obj);
	void equipThirdpickSure(Ref * obj);

	int getCurHoleStoneAmount(int typeBing);

	std::vector<CEquipHole *>equipmentHoleV;
	void firstButtonEvent(Ref *pSender, Widget::TouchEventType type);
	void secondButtonEvent(Ref *pSender, Widget::TouchEventType type);
	void thirdButtonEvent(Ref *pSender, Widget::TouchEventType type);
	void refreshGemValue(bool backSame);

	void callBackBaptize(Ref *pSender, Widget::TouchEventType type);
	void callBackBaptizeSure(Ref * obj);
	//////////////////////////baptize
	int aglieLock;
	int intelligenceLock;
	int focusLock;

	int baptizeCost;
	std::vector<int> propertyLock;

	ImageView * baptize_textAre_putEquipment;
	Text * labelBaptizeCost; 
	Text * labelBaptizeCostStrone;
	CheckBox * firstPropertyLock;
	CheckBox * secondPropertyLock;
	CheckBox * thirdPropertyLock;
	CheckBox * fourPropertyLock;
	CheckBox * fivePropertyLock;

	void setBaptizeFirstCheckBox(Ref* pSender, CheckBox::EventType type);
	void setBaptizeSecondCheckBox(Ref* pSender, CheckBox::EventType type);
	void setBaptizeThirdCheckBox(Ref* pSender, CheckBox::EventType type);
	void setBaptizeFourCheckBox(Ref* pSender, CheckBox::EventType type);
	void setBaptizeFiveCheckBox(Ref* pSender, CheckBox::EventType type);

	Text * baptizenotBaptize_5;
	Text * baptizenotBaptize_10;
	Text * baptizenotBaptize_15;
	Text * baptizenotBaptize_20;


	Text * baptizeFirstLabel;
	Text * baptizesSecondLabel;
	Text * baptizeThirdLabel;
	Text * baptizeFourLabel;
	Text * baptizeFiveLabel;

	Text * baptizeFirstvalue;
	Text * baptizeSecondvalue;
	Text * baptizeThirdvalue;
	Text * baptizeFourvalue;
	Text * baptizeFivevalue;

	std::vector<CAdditionProperty *> additionProperty_baptize;
	void refreshEquipProperty_baptize(int cost);
	void equipMentSureBaptize(Ref * obj);

	CheckBox * useBaptizeBingding;

	void isSureNotBingingBaptize(Ref * obj);
	void isSureBingingBaptize(Ref * obj);
	int getBaptizeClock(int bingdingType);

	void PageScrollToDefault();

public:
	Layer * m_Layer;
	Layout * refiningPanel;
	Layout * starPanel;
	Layout * gemPanel;
	Layout * baptizePanel;
	Layout * synthesisPanel;

	Layout * rolePanel;
	Layout * packPanel;
public:
	Layer *layerPackageFrame;
	PacPageView * pageView;
	ImageView* currentPage;
	void CurrentPageViewChanged(Ref *pSender, PageView::EventType type);
	void refreshGeneralEquipment(std::vector<CEquipment *>equipmentsVector);
	std::vector<CEquipment *> generalEquipVector;

	std::string getBackGroundPathByGoodsQulity(int quality);
private:
	//state 
	ImageView *image_state_refine_mainEquip;
	ImageView *image_state_first_assist;
	ImageView *image_state_second_assist;
	ImageView *image_state_third_assist;
	ImageView *image_state_four_assist;
	ImageView *image_state_five_assist;

	ImageView *image_state_star_MainEquip;
	ImageView *image_state_star_stone;

	ImageView * image_state_gem_mainEquip;

	ImageView * image_state_baptize_mainEquip;
public:
	//tabview select id
	int tabviewSelectIndex;
	//1601
	int mes_source;
	int mes_pob;
	int mes_assistEquipPob;
	int mes_index;
	int mes_petid;
	//int mes_playType;

	struct assistEquipStruct
	{
		int source_;
		int  pob_;
		long long petid_;
		int index_;
		int playType;
		int binging;
		int equipStarLv;
		bool equipGemLv;
	};
	std::vector<assistEquipStruct> assistVector;

	struct assistStoneStruct
	{
		int source_;
		int index_;
		int amount_;
		int playType_;
		int tag_;
		int binging;
	};
	std::vector<assistStoneStruct> assStoneVector;

	void isSureBingdingRefine(Ref * obj);
	void isSureBingdingStar(Ref * obj);
	void isSureBingdingGem(Ref * obj);
	void isSureBingdingBaptize(Ref * obj);

	void isSureStarLevelOfRefine(Ref * obj);
	bool isHaveGemValue(GoodsInfo * goods);

	void setGemBingding(Ref* pSender, CheckBox::EventType type);
	void setBaptizeDingding(Ref* pSender, CheckBox::EventType type);
private:
	bool m_isFirstOpenBackpac;
public:
	//н�ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void addCCTutorialIndicator2(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void addCCTutorialIndicatorClose(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	//��ѧ
	int mTutorialScriptInstanceId;

	int generalWeapTag;
	Button * btn_addAll;
	Button * btn_Refine;
	Button * btn_close;
};

