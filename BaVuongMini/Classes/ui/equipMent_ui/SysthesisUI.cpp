#include "SysthesisUI.h"
#include "external\sqlite3\include\sqlite3.h"
#include "../../utils/GameUtils.h"
#include "../../utils/StaticDataManager.h"
#include "../../AppMacros.h"
#include "../../messageclient/element/CSysthesisInfo.h"
#include "../../GameView.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../GameUIConstant.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../ui/extensions/ControlTree.h"

#define voidFramePath "res_ui/bdi_none.png"

#define whiteFramePath "res_ui/bdi_white.png"
#define greenFramePath "res_ui/bdi_green.png"
#define blueFramePath "res_ui/bdi_blue.png"
#define purpleFramePath "res_ui/bdi_purple.png"
#define orangeFramePath "res_ui/bdi_orange.png"


SysthesisUI::SysthesisUI()
{
}


SysthesisUI::~SysthesisUI()
{
	delete curSysthesisInfo;
}

SysthesisUI * SysthesisUI::create()
{
	auto systhesisui = new SysthesisUI();
	if (systhesisui && systhesisui->init())
	{
		systhesisui->autorelease();
		return systhesisui;
	}
	CC_SAFE_DELETE(systhesisui);
	return NULL;
}

bool SysthesisUI::init()
{
	if (UIScene::init())
	{
		u_layer = Layer::create();
		this->addChild(u_layer);

		curSysthesisInfo = new CSysthesisInfo();
		
		l_mergeName = (Text*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"Label_merge_name");
		l_outputName = (Text*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"Label_output_name");
		image_merge_frame = (ImageView*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"ImageView_merge_frame");
		image_merge_icon = (ImageView*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"ImageView_merge_icon");
		image_output_frame = (ImageView*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"ImageView_output_frame");
		image_output_icon = (ImageView*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"ImageView_output_icon");
		l_mergeCurNum = (Text*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"Label_curNumValue");
		l_sprit = (Text*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"Label_gangang");
		l_mergeNeedNum = (Text*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"Label_needValue");
		l_outputNum = (Text*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"Label_outputValue");
		cb_useBound = (CheckBox*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"CheckBox_useBound");
		cb_useBound->setTouchEnabled(true);
		//cb_useBound->addEventListenerCheckBox(this,checkboxselectedeventselector(SysthesisUI::checkBoxEvent));
		cb_useBound->setSelected(false);
		l_coin_mergeOneNeed = (Text*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"Label_coin_oneNeed");
		l_coin_mergeAllNeed = (Text*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"Label_coin_allNeed");
		btn_mergeOne = (Button*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"Button_takeOne");
		btn_mergeOne->setTouchEnabled(true);
		btn_mergeOne->setPressedActionEnabled(true);
		btn_mergeOne->addTouchEventListener(CC_CALLBACK_2(SysthesisUI::mergeOneEvent, this));
		btn_mergeAll = (Button*)Helper::seekWidgetByName(LoadSceneLayer::equipMentPanel,"Button_takeAll");
		btn_mergeAll->setTouchEnabled(true);
		btn_mergeAll->setPressedActionEnabled(true);
		btn_mergeAll->addTouchEventListener(CC_CALLBACK_2(SysthesisUI::mergeAllEvent,this));

		/*
		long long startTime = GameUtils::millisecondNow();
		m_tableView = TableView::create(this,Size(195,362));
		m_tableView ->setSelectedEnable(true);   // ֧��ѡ��״̬����ʾ
		m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(26, 26, 1, 1), Vec2(0,-1)); 
		m_tableView->setPressedActionEnabled(true);
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0,0));
		m_tableView->setPosition(Vec2(72,42));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		u_layer->addChild(m_tableView);

		Layer * u_TableViewFrameLayer = Layer::create();
		this->addChild(u_TableViewFrameLayer);
		ImageView * pageView_kuang = ImageView::create();
		pageView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
		pageView_kuang->setScale9Enabled(true);
		pageView_kuang->setContentSize(Size(210,377));
		pageView_kuang->setCapInsets(Rect(30,30,1,1));
		pageView_kuang->setAnchorPoint(Vec2(0,0));
		pageView_kuang->setPosition(Vec2(61,33));
		u_TableViewFrameLayer->addChild(pageView_kuang);

		long long endTime = GameUtils::millisecondNow();
		CCLOG("cost time = %ld",endTime - startTime);
		*/
		setToDefault();
		//add tree scroll
		std::vector<std::string > goodsNameVector;
		for (int i=0;i<SysthesisItemConfig::s_systhesisInfo.size();i++)
		{
			bool isHave = false;
			for (int j =0;j<goodsNameVector.size();j++)
			{
				if (strcmp(goodsNameVector.at(j).c_str(),SysthesisItemConfig::s_systhesisInfo.at(i)->get_typename().c_str())==0)
				{
					isHave = true;
				}
			}
			if (isHave == false)
			{
				goodsNameVector.push_back(SysthesisItemConfig::s_systhesisInfo.at(i)->get_typename());
			}
		}

		//add test
		std::vector<TreeStructInfo *> tempTreeVector;
		for (int i = 0;i<goodsNameVector.size();i++)
		{
			auto treeInfo_ = new TreeStructInfo();
			treeInfo_->setTreeName(goodsNameVector.at(i));

			//treeInfo_->setTreeIndex();

			for (int j =0;j<SysthesisItemConfig::s_systhesisInfo.size();j++)
			{
				if (strcmp(goodsNameVector.at(i).c_str(),SysthesisItemConfig::s_systhesisInfo.at(j)->get_typename().c_str())==0)
				{
					//set first treebranch is index
					std::string second_ = SysthesisItemConfig::s_systhesisInfo.at(j)->get_output_prop_name();

					treeInfo_->addSecondVector(second_);
				}
			}
			tempTreeVector.push_back(treeInfo_);
		}

		tree_ = ControlTree::create(tempTreeVector,Size(195,362),Size(186,52),Size(175,40));
		tree_->setAnchorPoint(Vec2(0.5f,0.5f));
		tree_->setPosition(Vec2(69,40));
		tree_->addcallBackTreeEvent(this,CC_CALLFUNCO_SELECTOR(SysthesisUI::callBackTreeEvent));
		tree_->addcallBackSecondTreeEvent(this,CC_CALLFUNCO_SELECTOR(SysthesisUI::callBackSecondTreeEvent));
		u_layer->addChild(tree_);

// 		Layer * u_scrollViewFrameLayer = Layer::create();
// 		this->addChild(u_scrollViewFrameLayer);
// 		ImageView * pageView_kuang = ImageView::create();
// 		pageView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setScale9Enabled(true);
// 		pageView_kuang->setContentSize(Size(210,377));
// 		pageView_kuang->setCapInsets(Rect(30,30,1,1));
// 		pageView_kuang->setAnchorPoint(Vec2(0,0));
// 		pageView_kuang->setPosition(Vec2(61,33));
// 		u_scrollViewFrameLayer->addChild(pageView_kuang);


		return true;
	}
	return false;
}

void SysthesisUI::onEnter()
{
	UIScene::onEnter();
}

void SysthesisUI::onExit()
{
	UIScene::onExit();
}

void SysthesisUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void SysthesisUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

PropInfo SysthesisUI::getPropInfoFromDBByPropId( const char * id )
{
	/*********************************************************/
	//��¼���ؽ���Ƿ�ɹ�  
	//long long startTime = GameUtils::millisecondNow();
	int result; 

	//��ȡ����·��  + �����ļ��  
	std::string path;
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
	path = FileUtils::getInstance()->fullPathForFilename("gamestaticdata.db");
#else
	path =  FileUtils::getInstance()->getWritablePath() + "gamestaticdata.db";
#endif


	//���ݿ���  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	char query_stmt[512];
	sprintf(query_stmt, "SELECT name,icon,quality from t_prop where id = '%s'",id);
	//char * query_stmt = "SELECT icon from t_prop";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	PropInfo temp;
	temp.quality = -1;
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char* s_name =(char*)sqlite3_column_text(statement, 0);
			char* s_icon =(char*)sqlite3_column_text(statement, 1);
			int quality = sqlite3_column_int(statement, 2);
			temp.name = s_name;
			temp.icon = s_icon;
			temp.quality = quality;
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 

	return temp;
	//long long endTime = GameUtils::millisecondNow();
	//CCLOG("cost time = %ld",endTime - startTime);
	/*********************************************************/
}

bool SysthesisUI::isEnableSysthesis( CSysthesisInfo * systhesisInfo )
{
	int num = 0;
	for (int i = 0;i<GameView::getInstance()->AllPacItem.size();++i)
	{
		auto folder = GameView::getInstance()->AllPacItem.at(i);
		if (!folder->has_goods())
			continue;

		if (folder->goods().id() == systhesisInfo->get_merge_prop_id())
		{
			num += folder->quantity();
		}
	}

	if (num >= systhesisInfo->get_need_num())
		return true;

	return false;
}


int SysthesisUI::getNumEnableSysthesis( CSysthesisInfo * systhesisInfo )
{
	int num = 0;
	for (int i = 0;i<GameView::getInstance()->AllPacItem.size();++i)
	{
		auto folder = GameView::getInstance()->AllPacItem.at(i);
		if (!folder->has_goods())
			continue;

		if (folder->goods().id() == systhesisInfo->get_merge_prop_id())
		{
			num += folder->quantity();
		}
	}

	return num;
}

bool SysthesisUI::isSysthesisBindingItem(CSysthesisInfo * systhesisInfo)
{
	bool visible = false;
	for (int i = 0;i<GameView::getInstance()->AllPacItem.size();++i)
	{
		auto folder = GameView::getInstance()->AllPacItem.at(i);
		if (!folder->has_goods())
			continue;

		if (folder->goods().id() == systhesisInfo->get_merge_prop_id())
		{
			if (folder->goods().binding())
			{
				visible = true;
				break;
			}
		}
	}

	return visible;
}

void SysthesisUI::refreshUI( CSysthesisInfo * systhesisInfo )
{
	l_mergeName->setString(systhesisInfo->get_merge_prop_name().c_str());
	l_mergeName->setColor(GameView::getInstance()->getGoodsColorByQuality(systhesisInfo->get_merge_prop_quality()));
	l_outputName->setString(systhesisInfo->get_output_prop_name().c_str());
	l_outputName->setColor(GameView::getInstance()->getGoodsColorByQuality(systhesisInfo->get_output_prop_quality()));

	char s_curNum [20];
	sprintf(s_curNum,"%d",this->getNumEnableSysthesis(systhesisInfo));
	l_mergeCurNum->setString(s_curNum);
	char s_needNum [20];
	sprintf(s_needNum,"%d",systhesisInfo->get_need_num());
	l_mergeNeedNum->setString(s_needNum);

	float _space = l_mergeCurNum->getContentSize().width - l_mergeNeedNum->getContentSize().width;
	l_sprit->setPosition(Vec2(115/2+_space/2,l_sprit->getPosition().y));
	l_mergeCurNum->setPosition(Vec2(l_sprit->getPosition().x-l_sprit->getContentSize().width/2,l_mergeCurNum->getPosition().y));
	l_mergeNeedNum->setPosition(Vec2(l_sprit->getPosition().x+l_sprit->getContentSize().width/2,l_mergeNeedNum->getPosition().y));

	if (this->getNumEnableSysthesis(systhesisInfo) < systhesisInfo->get_need_num())
	{
		l_mergeCurNum->setColor(Color3B(255,51,51)); //��ɫ
	}
	else
	{
		l_mergeCurNum->setColor(Color3B(255,255,255));  //��ɫ
	}

	//������
	int m_nWillOutPutNum = this->getNumEnableSysthesis(systhesisInfo)/systhesisInfo->get_need_num()*systhesisInfo->get_output_num();
	char s_outputNum [20];
	sprintf(s_outputNum,"%d",m_nWillOutPutNum);
	l_outputNum->setString(s_outputNum);

	char s_oneNeedCoin [20];
	sprintf(s_oneNeedCoin,"%d",systhesisInfo->get_need_price());
	l_coin_mergeOneNeed->setString(s_oneNeedCoin);
	char s_allNeedCoin [20];
	sprintf(s_allNeedCoin,"%d",systhesisInfo->get_need_price()*m_nWillOutPutNum);
	l_coin_mergeAllNeed->setString(s_allNeedCoin);

	if (systhesisInfo->get_merge_prop_quality() != -1)
	{
		/*********************�ж���ɫ***************************/
		std::string frameColorPath;
		if (systhesisInfo->get_merge_prop_quality() == 1)
		{
			frameColorPath = whiteFramePath;
		}
		else if (systhesisInfo->get_merge_prop_quality() == 2)
		{
			frameColorPath = greenFramePath;
		}
		else if (systhesisInfo->get_merge_prop_quality() == 3)
		{
			frameColorPath = blueFramePath;
		}
		else if (systhesisInfo->get_merge_prop_quality() == 4)
		{
			frameColorPath = purpleFramePath;
		}
		else if (systhesisInfo->get_merge_prop_quality() == 5)
		{
			frameColorPath = orangeFramePath;
		}
		else
		{
			frameColorPath = voidFramePath;
		}
		image_merge_frame->loadTexture(frameColorPath.c_str());

		if (strcmp(systhesisInfo->get_merge_prop_icon().c_str(),"") == 0)
		{
			image_merge_icon->loadTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(systhesisInfo->get_merge_prop_icon().c_str());
			_path.append(".png");
			image_merge_icon->loadTexture(_path.c_str());
		}
	}

	if (systhesisInfo->get_output_prop_quality() != -1)
	{
		/*********************�ж���ɫ***************************/
		std::string frameColorPath;
		if (systhesisInfo->get_output_prop_quality() == 1)
		{
			frameColorPath = whiteFramePath;
		}
		else if (systhesisInfo->get_output_prop_quality() == 2)
		{
			frameColorPath = greenFramePath;
		}
		else if (systhesisInfo->get_output_prop_quality() == 3)
		{
			frameColorPath = blueFramePath;
		}
		else if (systhesisInfo->get_output_prop_quality() == 4)
		{
			frameColorPath = purpleFramePath;
		}
		else if (systhesisInfo->get_output_prop_quality() == 5)
		{
			frameColorPath = orangeFramePath;
		}
		else
		{
			frameColorPath = voidFramePath;
		}
		image_output_frame->loadTexture(frameColorPath.c_str());

		if (strcmp(systhesisInfo->get_output_prop_icon().c_str(),"") == 0)
		{
			image_output_icon->loadTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(systhesisInfo->get_output_prop_icon().c_str());
			_path.append(".png");
			image_output_icon->loadTexture(_path.c_str());
		}
	}
}

void SysthesisUI::refreshContrelTree()
{
	/*
	//refresh tableView
	Vec2 _s = m_tableView->getContentOffset();
	int _h = m_tableView->getContentSize().height + _s.y;
	m_tableView->reloadData();
	Vec2 temp = Vec2(_s.x,_h - m_tableView->getContentSize().height);
	m_tableView->setContentOffset(temp);
	*/

	tree_->removeMarkOnButton();
	tree_->removeMarkOnSecondButton();
	for (int i = 0;i<SysthesisItemConfig::s_systhesisInfo.size();i++)
	{
		auto systhesisInfo = SysthesisItemConfig::s_systhesisInfo.at(i);
		if (this->isEnableSysthesis(systhesisInfo))
		{
			int button_size_ =  tree_->getButtonTreeSize().width;
			int second_button_size = tree_->getBranchButtonSize().width;
			tree_->addMarkOnButton("res_ui/new.png",Vec2(button_size_/2 - 15,0),i);
			tree_->addMarkOnSecondButton("res_ui/new.png",Vec2(second_button_size/2 - 15,0),i);
		}
	}
};

void SysthesisUI::checkBoxEvent( Ref* pSender )
{

}

void SysthesisUI::mergeOneEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->getNumEnableSysthesis(curSysthesisInfo) < curSysthesisInfo->get_need_num())
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("SysthesisUI_hasNotEnoughMaterial"));//����������
			return;
		}

		if (curSysthesisInfo->get_need_price() > GameView::getInstance()->getPlayerGold())
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_teach_noEnoughGold"));//��Ҳ��
			return;
		}

		if (this->isSysthesisBindingItem(curSysthesisInfo))
		{
			GameView::getInstance()->showPopupWindow(StringDataManager::getString("SysthesisUI_areYouSureToMerge"), 2, this, CC_CALLFUNCO_SELECTOR(SysthesisUI::sureToMergeOne), NULL);
		}
		else
		{
			ReqData temp = { curSysthesisInfo->get_id(),1,cb_useBound->isSelected() };
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1627, &temp);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void SysthesisUI::mergeAllEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->getNumEnableSysthesis(curSysthesisInfo) < curSysthesisInfo->get_need_num())
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("SysthesisUI_hasNotEnoughMaterial"));//����������
			return;
		}

		int m_nWillOutPutNum = this->getNumEnableSysthesis(curSysthesisInfo) / curSysthesisInfo->get_need_num()*curSysthesisInfo->get_output_num();
		if (curSysthesisInfo->get_need_price()*m_nWillOutPutNum > GameView::getInstance()->getPlayerGold())
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_teach_noEnoughGold"));//��Ҳ��
			return;
		}

		if (this->isSysthesisBindingItem(curSysthesisInfo))
		{
			GameView::getInstance()->showPopupWindow(StringDataManager::getString("SysthesisUI_areYouSureToMerge"), 2, this, CC_CALLFUNCO_SELECTOR(SysthesisUI::sureToMergeAll), NULL);
		}
		else
		{
			ReqData temp = { curSysthesisInfo->get_id(),this->getNumEnableSysthesis(curSysthesisInfo) / curSysthesisInfo->get_need_num(),cb_useBound->isSelected() };
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1627, &temp);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void SysthesisUI::setToDefault()
{
	//setToDefault
	if (SysthesisItemConfig::s_systhesisInfo.size()>0)
	{
		curSysthesisInfo->CopyFrom(SysthesisItemConfig::s_systhesisInfo.at(0));
		refreshUI(curSysthesisInfo);
	}
}

CSysthesisInfo * SysthesisUI::getCurSysthesisInfo()
{
	if (curSysthesisInfo->get_output_prop_id()!="")
	{
		return curSysthesisInfo;
	}

	return NULL;
}

void SysthesisUI::setCurSysthesisInfo( CSysthesisInfo* sysInfo )
{
	curSysthesisInfo->CopyFrom(sysInfo);
}

void SysthesisUI::selectCellBySysthesisInfo( CSysthesisInfo * systhesisInfo )
{
	int selectIndex = -1;
	for(int i = 0;i<SysthesisItemConfig::s_systhesisInfo.size();++i)
	{
		if (SysthesisItemConfig::s_systhesisInfo.at(i)->get_id() == systhesisInfo->get_id())
		{
			selectIndex = i;
			break;
		}
	}

	if (selectIndex == -1)
		return;

	//set select cell
	tree_->setShowSecondTreeCell(selectIndex);
}

void SysthesisUI::addBonusSpecialEffect()
{
	// Bonus Special Effect
	auto pNode = BonusSpecialEffect::create();
	pNode->setPosition(Vec2(622, 317));
	pNode->setScale(EQUIPMENT_UI_BONUSSPECIALEFFECT_SCALE);
	u_layer->addChild(pNode);
}

void SysthesisUI::sureToMergeOne( Ref *pSender )
{
	ReqData temp = {curSysthesisInfo->get_id(),1,cb_useBound->isSelected()};
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1627,&temp);
}

void SysthesisUI::sureToMergeAll( Ref *pSender )
{
	ReqData temp = {curSysthesisInfo->get_id(),this->getNumEnableSysthesis(curSysthesisInfo)/curSysthesisInfo->get_need_num(),cb_useBound->isSelected()};
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1627,&temp);
}

void SysthesisUI::callBackTreeEvent( Ref * obj )
{
	auto tree_ = (ControlTree *)obj;
	
	for (int i = 0;i<SysthesisItemConfig::s_systhesisInfo.size();i++)
	{
		auto systhesisInfo = SysthesisItemConfig::s_systhesisInfo.at(i);
		if (this->isEnableSysthesis(systhesisInfo))
		{
			int second_button_size = tree_->getBranchButtonSize().width;
			tree_->addMarkOnSecondButton("res_ui/new.png",Vec2(second_button_size/2 - 15,0),i);
		}
	}
}

void SysthesisUI::callBackSecondTreeEvent( Ref * obj )
{
	auto tree_ = (ControlTree *)obj;
	int index_ = tree_->getSecondTreeIndex();
	
	//tree_->removeSecondButton(index_);

	auto systhesisInfo = SysthesisItemConfig::s_systhesisInfo.at(index_);
	curSysthesisInfo->CopyFrom(systhesisInfo);
	refreshUI(systhesisInfo);
}
