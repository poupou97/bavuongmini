#include "StrengthEquip.h"
#include "GameView.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "EquipMentUi.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"


StrengthEquip::StrengthEquip(void)
{
	m_starLevel = 0;
	m_refineLevel =0;
	m_refineUselevlel =0;
	m_clazz =0;
	m_quality =0;
}


StrengthEquip::~StrengthEquip(void)
{
	
}

StrengthEquip * StrengthEquip::create(FolderInfo* folder,bool isMainequip)
{
	auto equip=new StrengthEquip();
	if (equip && equip->init(folder,isMainequip))
	{
		equip->autorelease();
		return equip;
	}
	CC_SAFE_DELETE(equip);
	return NULL;
}

bool StrengthEquip::init(FolderInfo* folder,bool isMainequip)
{
	if (UIScene::init())
	{
		m_clazz = folder->goods().equipmentclazz();
		m_quality = folder->goods().quality();
		std::string frameColorPath = "res_ui/";
		if (isMainequip == true)
		{
			if (folder->goods().quality() == 1)
			{
				frameColorPath.append("bdi_white");
			}
			else if (folder->goods().quality() == 2)
			{
				frameColorPath.append("bdi_green");
			}
			else if (folder->goods().quality() == 3)
			{
				frameColorPath.append("bdi_blue");
			}
			else if (folder->goods().quality() == 4)
			{
				frameColorPath.append("bdi_purple");
			}
			else if (folder->goods().quality() == 5)
			{
				frameColorPath.append("bdi_orange");
			}
			else
			{
				frameColorPath.append("bdi_white");
			}
		}else
		{
			if (folder->goods().quality() == 1)
			{
				frameColorPath.append("sdi_white");
			}
			else if (folder->goods().quality() == 2)
			{
				frameColorPath.append("sdi_green");
			}
			else if (folder->goods().quality() == 3)
			{
				frameColorPath.append("sdi_bule");
			}
			else if (folder->goods().quality() == 4)
			{
				frameColorPath.append("sdi_purple");
			}
			else if (folder->goods().quality() == 5)
			{
				frameColorPath.append("sdi_orange");
			}
			else
			{
				frameColorPath.append("sdi_white");
			}
		}
		
		frameColorPath.append(".png");

		Btn_pacItemFrame = Button::create();
		Btn_pacItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnabled(true);
		Btn_pacItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(Vec2(25,24));
		Btn_pacItemFrame->setTag(folder->id());
		Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(StrengthEquip::showEquipsInfo,this));
		m_pLayer->addChild(Btn_pacItemFrame);

		std::string goodsInfoStr ="res_ui/props_icon/";
		goodsInfoStr.append(folder->goods().icon());
		goodsInfoStr.append(".png");

		auto equipIcon_ =ImageView::create();
		equipIcon_->loadTexture(goodsInfoStr.c_str());
		equipIcon_->setAnchorPoint(Vec2(0.5f,0.5f));
		equipIcon_->setPosition(Vec2(0,0));
		Btn_pacItemFrame->addChild(equipIcon_);

		m_refineUselevlel = folder->goods().uselevel();

		if (folder->goods().equipmentclazz()>0 && folder->goods().equipmentclazz()<11)
		{
			m_starLevel = folder->goods().equipmentdetail().starlevel();
			char starLvLabel_[5];
			sprintf(starLvLabel_,"%d",m_starLevel);
			labelStarlevel=Label::createWithTTF(starLvLabel_, APP_FONT_NAME, 13);
			labelStarlevel->setAnchorPoint(Vec2(0,0));
			labelStarlevel->setPosition(Vec2(-25,-20));
			Btn_pacItemFrame->addChild(labelStarlevel);

			imageStar_ =ImageView::create();
			imageStar_->loadTexture("res_ui/star_on.png");
			imageStar_->setScale(0.5f);
			imageStar_->setAnchorPoint(Vec2(0,0));
			imageStar_->setPosition(Vec2(labelStarlevel->getPosition().x+ labelStarlevel->getContentSize().width,labelStarlevel->getPosition().y));
			Btn_pacItemFrame->addChild(imageStar_);

			if (this->getStarLevel() <= 0)
			{
				labelStarlevel->setVisible(false);
				imageStar_->setVisible(false);
			}

			m_refineLevel = folder->goods().equipmentdetail().gradelevel();
			char strengthLv[5];
			sprintf(strengthLv,"%d",m_refineLevel);
			std::string strengthStr_ = "+";
			strengthStr_.append(strengthLv);

			label_StrengthLv = Label::createWithTTF(strengthStr_.c_str(), APP_FONT_NAME, 13);
			label_StrengthLv->setAnchorPoint(Vec2(1.0f,0.5f));
			label_StrengthLv->setPosition(Vec2(22,15));
			Btn_pacItemFrame->addChild(label_StrengthLv);
			
			if (this->getRefineLevel()<=0)
			{
				label_StrengthLv->setVisible(false);
			}
		}else
		{
			auto equip_ =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
			char stoneAmountStr[20];
			if (equip_->currType == 0)
			{
				sprintf(stoneAmountStr,"%d",equip_->stoneOfAmountRefine);
			}else
			{
				sprintf(stoneAmountStr,"%d",equip_->stoneOfAmountStar);
			}
			
			auto label_amount = Label::createWithTTF(stoneAmountStr, APP_FONT_NAME, 13);
			label_amount->setAnchorPoint(Vec2(1,1));
			label_amount->setPosition(Vec2(21,-10));
			Btn_pacItemFrame->addChild(label_amount);
		}
		packIndex=folder->id();
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Btn_pacItemFrame->getContentSize());
		return true;
	}
	return false;
}

void StrengthEquip::onEnter()
{
	UIScene::onEnter();
}

void StrengthEquip::onExit()
{
	UIScene::onExit();
}

void StrengthEquip::showEquipsInfo(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button *)pSender;
		int index = btn->getTag();

		auto equip = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		equip->getBackMainEquip(this->getTag(), index);

		/*
		Size size= Director::getInstance()->getVisibleSize();
		EquipMentUi * equipUi = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		if (equipUi != NULL)
		{
		EquipStrengthInfo * equipInfo = EquipStrengthInfo::create(GameView::getInstance()->AllPacItem.at(index),equipUi->generalEquipVector,equipUi->mes_petid);
		equipInfo->setIgnoreAnchorPointForPosition(false);
		equipInfo->setAnchorPoint(Vec2(0.5f,0.5f));
		equipInfo->setTag(this->getTag());
		GameView::getInstance()->getMainUIScene()->addChild(equipInfo);
		}
		*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void StrengthEquip::setStarLevel( int level_ , bool isAction)
{
	m_starLevel = level_;

	if (level_ >0)
	{
		if (level_ >=20)
		{
			level_ =20;
		}
		char starLvLabel_[10];
		sprintf(starLvLabel_,"%d",level_);
		labelStarlevel->setString(starLvLabel_);
		imageStar_->setPosition(Vec2(labelStarlevel->getPosition().x+ labelStarlevel->getContentSize().width,labelStarlevel->getPosition().y));
		labelStarlevel->setVisible(true);
		imageStar_->setVisible(true);

		if (isAction)
		{
			auto seq_ = Sequence::create(
				ScaleTo::create(0.01f,10.0f),
				ScaleTo::create(0.3f,1.0f),
				NULL);
			labelStarlevel->runAction(seq_);
			
			auto seq_1 = Sequence::create(
				ScaleTo::create(0.01f,5.0f),
				ScaleTo::create(0.3f,0.5f),
				NULL);
			imageStar_->runAction(seq_1);
		}

	}else
	{
		labelStarlevel->setVisible(false);
		imageStar_->setVisible(false);
	}
}

int StrengthEquip::getStarLevel()
{
	return m_starLevel;
}

void StrengthEquip::setRefineLevel( int level_,bool isAction )
{
	m_refineLevel = level_;
	if (level_ > 0)
	{
		if (level_>=20)
		{
			level_ =20;
		}
		char strengthLv[10];
		sprintf(strengthLv,"%d",level_);
		std::string strengthStr_ = "+";
		strengthStr_.append(strengthLv);
		label_StrengthLv->setString(strengthStr_.c_str());
		label_StrengthLv->setVisible(true);

		if (isAction)
		{
			auto seq_ = Sequence::create(
				ScaleTo::create(0.01f,10.0f),
				ScaleTo::create(0.3f,1.0f),
				NULL);
			label_StrengthLv->runAction(seq_);
		}
	}else
	{
		label_StrengthLv->setVisible(false);
	}
}

int StrengthEquip::getRefineLevel()
{
	return m_refineLevel;
}

int StrengthEquip::getRefineUseLevel()
{
	return m_refineUselevlel;
}

int StrengthEquip::getClazz()
{
	return m_clazz;
}

int StrengthEquip::getQuality()
{
	return m_quality;
}
////////////////
EquipStrengthInfo::EquipStrengthInfo()
{

}

EquipStrengthInfo::~EquipStrengthInfo()
{

}

EquipStrengthInfo * EquipStrengthInfo::create( FolderInfo* folder,std::vector<CEquipment *>equipVector,long long generalId )
{
	auto equip = new EquipStrengthInfo();
	if (equip && equip->init(folder,equipVector,generalId))
	{
		equip->autorelease();
		return equip;
	}
	CC_SAFE_DELETE(equip);
	return NULL;
}

bool EquipStrengthInfo::init( FolderInfo* folder,std::vector<CEquipment *>equipVector,long long generalId )
{
	auto goods_ =new GoodsInfo();
	goods_->CopyFrom(folder->goods());
	if (GoodsItemInfoBase::init(goods_,equipVector,generalId))
	{
		const char *str_recove = StringDataManager::getString("goods_equip_getBack");
		auto Button_getAnnes= Button::create();
		Button_getAnnes->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_getAnnes->setTouchEnabled(true);
		Button_getAnnes->setPressedActionEnabled(true);
		Button_getAnnes->addTouchEventListener(CC_CALLBACK_2(EquipStrengthInfo::refineMainEquip, this));
		Button_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_getAnnes->setScale9Enabled(true);
		Button_getAnnes->setContentSize(Size(80,43));
		Button_getAnnes->setCapInsets(Rect(18,9,2,23));
		Button_getAnnes->setTag(folder->id());
		Button_getAnnes->setPosition(Vec2(135,25));

		auto Label_getAnnes = Label::createWithTTF(str_recove, APP_FONT_NAME, 18);
		Label_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_getAnnes->setPosition(Vec2(0,0));
		Button_getAnnes->addChild(Label_getAnnes);
		m_pLayer->addChild(Button_getAnnes);

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setAnchorPoint(Vec2(0,0));
		delete goods_;
		return true;
	}
	return false;
}

void EquipStrengthInfo::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void EquipStrengthInfo::onExit()
{
	UIScene::onExit();
}
void EquipStrengthInfo::refineMainEquip(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button*)pSender;
		int index = btn->getTag();
		auto equip = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		equip->getBackMainEquip(this->getTag(), index);
		this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

bool EquipStrengthInfo::onTouchBegan( Touch * pTouch,Event * pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}
