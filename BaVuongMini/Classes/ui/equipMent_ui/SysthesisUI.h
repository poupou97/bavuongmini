#ifndef _EQUIPMENTUI_SYSTEHSISUI_H_
#define _EQUIPMENTUI_SYSTEHSISUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../extensions/ControlTree.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CSysthesisInfo;

struct PropInfo 
{
	std::string name;
	std::string icon;
	int quality;
};

class SysthesisUI : public UIScene
{
public:
	SysthesisUI();
	~SysthesisUI();

	struct ReqData
	{
		int mergeid;
		int num;
		bool unbindfirst;
	};

	static SysthesisUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��ѯ��Ʒ��Ϣ
	static PropInfo getPropInfoFromDBByPropId(const char * id);

	//�Ƿ���Ժϳ
	bool isEnableSysthesis(CSysthesisInfo * systhesisInfo);
	//ɷ��ؿ�������ϳɵ����
	int getNumEnableSysthesis(CSysthesisInfo * systhesisInfo);
	//�Ƿ��ϳɰ󶨵
	bool isSysthesisBindingItem(CSysthesisInfo * systhesisInfo);

	void refreshUI(CSysthesisInfo * systhesisInfo);
	void refreshContrelTree();
	//select oneCell by systhesisInfo
	void selectCellBySysthesisInfo(CSysthesisInfo * systhesisInfo);

	void checkBoxEvent(Ref* pSender);
	void mergeOneEvent(Ref *pSender, Widget::TouchEventType type);
	void mergeAllEvent(Ref *pSender, Widget::TouchEventType type);

	//�Ĭ��ѡ���һ�
	void setToDefault();

	CSysthesisInfo * getCurSysthesisInfo();
	void setCurSysthesisInfo(CSysthesisInfo* sysInfo);
	//add BonusSpecialEffect
	void addBonusSpecialEffect();

	void sureToMergeOne(Ref *pSender);
	void sureToMergeAll(Ref *pSender);

	//add test by liuzhenxing for tree
	void callBackTreeEvent(Ref * obj);
	void callBackSecondTreeEvent(Ref * obj);

	ControlTree * tree_;
private:
	Layer * u_layer;
	//TableView * m_tableView;

	Text * l_mergeName;
	Text * l_outputName;
	ImageView * image_merge_frame;
	ImageView * image_merge_icon;
	ImageView * image_output_frame;
	ImageView * image_output_icon;

	Text * l_mergeCurNum;
	Text * l_sprit;
	Text * l_mergeNeedNum;
	Text * l_outputNum;

	CheckBox * cb_useBound;

	Text * l_coin_mergeOneNeed;
	Text * l_coin_mergeAllNeed;

	Button * btn_mergeOne;
	Button * btn_mergeAll;

private:
	CSysthesisInfo * curSysthesisInfo;

	std::map<std::string,PropInfo*>curDataSource;
};

#endif
