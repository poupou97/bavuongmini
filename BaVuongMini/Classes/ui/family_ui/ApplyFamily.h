
#ifndef _FAMILY_APPLYFAMILY_H
#define _FAMILY_APPLYFAMILY_H
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGuildBase;
class ApplyFamily:public UIScene,public cocos2d::extension::TableViewDataSource,public cocos2d::extension::TableViewDelegate
{
public:
	ApplyFamily(void);
	~ApplyFamily(void);

	static ApplyFamily * create();
	bool init();

	void onEnter();
	void onExit();

	void callBackExit(Ref *pSender, Widget::TouchEventType type);
	void callBackCheck(Ref * obj);
	void callBackApply(Ref * obj);
public:
	////familyMember
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	// default implements are used to call script callback if exist
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
public:
	TableView *tableviewstate;
	std::vector<CGuildBase *>vectorGuildBase;
	long long selfGuildId;
	int curPage;
	bool hasNextPage;
};

////////////////
class ApplyChackFamily:public UIScene
{
public:
	ApplyChackFamily(void);
	~ApplyChackFamily(void);

	static ApplyChackFamily * create(std::string name,std::string leaderName,long long familyId,std::string membernum,std::string familyNotice);
	bool init(std::string name,std::string leaderName,long long familyId,std::string membernum,std::string familyNotice);

	void onEnter();
	void onExit();
	void callBackApply(Ref *pSender, Widget::TouchEventType type);
	void callBack(Ref *pSender, Widget::TouchEventType type);
};



#endif