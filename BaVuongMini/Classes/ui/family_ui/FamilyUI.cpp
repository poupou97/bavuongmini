#include "FamilyUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/UITab.h"
#include "GameView.h"
#include "ManageFamily.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGuildRecord.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CGuildMemberBase.h"
#include "FamilyMemberOperator.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../extensions/RichTextInput.h"
#include "FamilyOperatorOfLeader.h"
#include "../../gamescene_state/GameSceneState.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "FamilyShop.h"
#include "FamilyAlms.h"
#include "../../messageclient/element/CGuildDonateOption.h"
#include "../../utils/GameUtils.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../extensions/ShowSystemInfo.h"
#include "familyFight_ui/FamilyFightUI.h"
#include "../../messageclient/element/CGuildBase.h"

#define  kTagFamilyFightUI 500
#define  SelectImageTag 458

using namespace CocosDenshion;

FamilyUI::FamilyUI(void)
{
	currType=0;
	hasNextPageOfManageMember =  false;
	curPageOfManageFamilyMember = 0;
	hasNextPageOfMember =false;
	curPageOfFamilyMember = 0;
	selfPost_ = 0;

	remPersonContrbution = 0;
	curFamilyContrbution = 0;
	nextFamilyContrbution = 0;
	familyAddContrbution = 0;
	personAddContrbution =0;
	familyLevel = 0;

	curFamilyMemberNum = 0;
	maxFamilyMemberNum = 0;
	m_familytab = NULL;
}


FamilyUI::~FamilyUI(void)
{
	std::vector<CGuildMemberBase *>::iterator iterbase;
	for (iterbase=vectorGuildMemberBase.begin();iterbase!=vectorGuildMemberBase.end();iterbase++)
	{
		delete * iterbase;
	}
	vectorGuildMemberBase.clear();

	std::vector<CGuildMemberBase *>::iterator itermember;
	for (itermember=vectorApplyGuildMember.begin();itermember!=vectorApplyGuildMember.end();itermember++)
	{
		delete * itermember;
	}
	vectorApplyGuildMember.clear();

	std::vector<CGuildCommodity *>::iterator iterCommodity;
	for (iterCommodity=guildCommodityVector.begin();iterCommodity!=guildCommodityVector.end();iterCommodity++)
	{
		delete * iterCommodity;
	}
	guildCommodityVector.clear();


	std::vector<CGuildDonateOption *>::iterator iterGuildDonate;
	for (iterGuildDonate=guilidDonateVector.begin();iterGuildDonate!=guilidDonateVector.end();iterGuildDonate++)
	{
		delete * iterGuildDonate;
	}
	guilidDonateVector.clear();
}

FamilyUI * FamilyUI::create()
{
	auto family = new FamilyUI();
	if (family && family->init())
	{
		family->autorelease();
		return family;
	}
	CC_SAFE_DELETE(family);
	return NULL;
}

bool FamilyUI::init()
{
	if (UIScene::init())
	{
		winsize= Director::getInstance()->getVisibleSize();
		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		if(LoadSceneLayer::familyMainPanel->getParent() != NULL)
		{
			LoadSceneLayer::familyMainPanel->removeFromParentAndCleanup(false);
		}
		
		mainPanel=LoadSceneLayer::familyMainPanel;
		mainPanel->setAnchorPoint(Vec2(0.5f,0.5f));
		mainPanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(mainPanel);

		loadLayer= Layer::create();
		loadLayer->setIgnoreAnchorPointForPosition(false);
		loadLayer->setAnchorPoint(Vec2(0.5f,0.5f));
		loadLayer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		loadLayer->setContentSize(Size(800,480));
		this->addChild(loadLayer);

		const char * secondStr = StringDataManager::getString("UIName_family_jia");
		const char * thirdStr = StringDataManager::getString("UIName_family_zu");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		loadLayer->addChild(atmature);

		const char *strings_info = StringDataManager::getString("family_tab_info");
		char *tab_info=const_cast<char*>(strings_info);	

		const char *strings_member = StringDataManager::getString("family_tab_member");
		char *tab_member=const_cast<char*>(strings_member);	

		const char *strings_state = StringDataManager::getString("family_tab_state");
		char *tab_state=const_cast<char*>(strings_state);	

		const char *strings_familyFight = StringDataManager::getString("family_tab_familyFight");
		char *tab_familyFight=const_cast<char*>(strings_familyFight);	

		char *labelFont[]={tab_info,tab_member,tab_state,tab_familyFight};
		//m_familytab=UITab::createWithBMFont(4,"res_ui/tab_4_off.png","res_ui/tab_4_on.png","",labelFont,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
		m_familytab=UITab::createWithText(4,"res_ui/tab_4_off.png","res_ui/tab_4_on.png","",labelFont,HORIZONTAL,5);
		m_familytab->setAnchorPoint(Vec2(0.5f,0.5f));
		m_familytab->setPosition(Vec2(70,413));
		m_familytab->setHighLightImage("res_ui/tab_4_on.png");
		m_familytab->setDefaultPanelByIndex(0);
		m_familytab->setPressedActionEnabled(true);
		m_familytab->addIndexChangedEvent(this,coco_indexchangedselector(FamilyUI::changeUITabIndex));
		loadLayer->addChild(m_familytab);

		familyInfo_panel =(Layout *)Helper::seekWidgetByName(mainPanel,"Panel_information");
		familyMember_panel =(Layout *)Helper::seekWidgetByName(mainPanel,"Panel_menbers");
		familyState_panel =(Layout *)Helper::seekWidgetByName(mainPanel,"Panel_state");
		familyState_panel_infoIsNUll = (Layout *)Helper::seekWidgetByName(mainPanel,"Panel_stateInfoIsNull");
		familyInfo_panel->setVisible(true);
		familyMember_panel->setVisible(false);
		familyState_panel->setVisible(false);

		panel_familyFight = (Layout*)Helper::seekWidgetByName(mainPanel,"Panel_familyFight");
		panel_familyFight->setVisible(false);

		label_familyName =(Text*)Helper::seekWidgetByName(mainPanel,"Label_name_value");
		label_familyLevel = (Text*)Helper::seekWidgetByName(mainPanel,"Label_LV_value");
		label_familyNumber =(Text*)Helper::seekWidgetByName(mainPanel,"Label_NumberOfPeople_value");
		label_familyContribution = (Text*)Helper::seekWidgetByName(mainPanel,"Label_contribution_value");
		
		//get self id
		selfId_ = GameView::getInstance()->myplayer->getRoleId();
		
		label_myPost = (Text*)Helper::seekWidgetByName(mainPanel,"Label_jobValue");
		//label_myAllContribution = (Text*)Helper::seekWidgetByName(mainPanel,"Label_accumulativeValue");
		label_myRemContribution = (Text*)Helper::seekWidgetByName(mainPanel,"Label_rateOfSurplusValue");

		btn_repairNotice =(Button *)Helper::seekWidgetByName(mainPanel,"Button_announcement");
		btn_repairNotice->setTouchEnabled(true);
		btn_repairNotice->addTouchEventListener(CC_CALLBACK_2(FamilyUI::repairFamilyNotice, this));
		
		//const char *strings_NoticeString = StringDataManager::getString("dianjishurujiazuzhongzhi");
		
		familyNoticeLabel=CCRichLabel::createWithString("",Size(250,0),NULL,NULL,0,18,2);
		familyNoticeLabel->setAnchorPoint(Vec2(0,0));
		familyNoticeLabel->setPosition(Vec2(0,0));
		
		noticeScrollView = cocos2d::extension::ScrollView::create(Size(270,105));
		noticeScrollView->setContentSize(Size(270,105));
		noticeScrollView->setIgnoreAnchorPointForPosition(false);
		noticeScrollView->setTouchEnabled(true);
		noticeScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		noticeScrollView->setAnchorPoint(Vec2(0,0));
		noticeScrollView->setPosition(Vec2(485,105));
		noticeScrollView->setBounceable(true);
		noticeScrollView->setClippingToBounds(true);
		loadLayer->addChild(noticeScrollView);	
		noticeScrollView->addChild(familyNoticeLabel);
		
		btn_repairManifesto =(Button *)Helper::seekWidgetByName(mainPanel,"Button_repairgonggao");
		btn_repairManifesto->setTouchEnabled(true);
		btn_repairManifesto->addTouchEventListener(CC_CALLBACK_2(FamilyUI::repairFamilyManifesto,this));

		//const char *strings_manifestoString = StringDataManager::getString("dianjishurujiazuxuanyan");
		familyManifestoLabel=CCRichLabel::createWithString("",Size(250,0),NULL,NULL,0,18,2);
		familyManifestoLabel->setAnchorPoint(Vec2(0,0));
		//familyManifestoLabel->setPosition(Vec2(143,210));
		familyManifestoLabel->setPosition(Vec2(0,0));

		manifestoScrollView = cocos2d::extension::ScrollView::create(Size(270,105));
		manifestoScrollView->setContentSize(Size(270,105));
		manifestoScrollView->setIgnoreAnchorPointForPosition(false);
		manifestoScrollView->setTouchEnabled(true);
		manifestoScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		manifestoScrollView->setAnchorPoint(Vec2(0,0));
		manifestoScrollView->setPosition(Vec2(143,105));
		manifestoScrollView->setBounceable(true);
		manifestoScrollView->setClippingToBounds(true);
		loadLayer->addChild(manifestoScrollView);	
		manifestoScrollView->addChild(familyManifestoLabel);

		//btn family is up level
		btn_familyUplevel = (Button *)Helper::seekWidgetByName(mainPanel,"Button_familyUplevel");
		btn_familyUplevel->setTouchEnabled(true);
		btn_familyUplevel->setPressedActionEnabled(true);
		btn_familyUplevel->addTouchEventListener(CC_CALLBACK_2(FamilyUI::callbackFamilyUplLevel, this));

		btn_mamage=(Button *)Helper::seekWidgetByName(mainPanel,"Button_adminstration");
		btn_mamage->setTouchEnabled(true);
		btn_mamage->setPressedActionEnabled(true);
		btn_mamage->addTouchEventListener(CC_CALLBACK_2(FamilyUI::callBackManageFamily, this));

		auto btn_quit=(Button *)Helper::seekWidgetByName(mainPanel,"Button_out");
		btn_quit->setTouchEnabled(true);
		btn_quit->setPressedActionEnabled(true);
		btn_quit->addTouchEventListener(CC_CALLBACK_2(FamilyUI::memberQuitFamily, this));
		
		auto button_des = (Button *)Helper::seekWidgetByName(mainPanel,"button_des");
		button_des->setTouchEnabled(true);
		button_des->setPressedActionEnabled(true);
		button_des->addTouchEventListener(CC_CALLBACK_2(FamilyUI::callBackDesFamily,this));

		//btn shop and contrbution
		auto btn_shop=(Button *)Helper::seekWidgetByName(mainPanel,"Button_shop");
		btn_shop->setTouchEnabled(true);
		btn_shop->setPressedActionEnabled(true);
		btn_shop->addTouchEventListener(CC_CALLBACK_2(FamilyUI::callbackFamilyShop, this));

		auto btn_alms=(Button *)Helper::seekWidgetByName(mainPanel,"Button_alms");
		btn_alms->setTouchEnabled(true);
		btn_alms->setPressedActionEnabled(true);
		btn_alms->addTouchEventListener(CC_CALLBACK_2(FamilyUI::callbackFamilyAlms, this));

		layerMember = Layer::create();
		layerMember->setIgnoreAnchorPointForPosition(false);
		layerMember->setAnchorPoint(Vec2(0.5f,0.5f));
		layerMember->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layerMember->setLocalZOrder(10);
		layerMember->setContentSize(Size(800,480));
		this->addChild(layerMember);
		layerMember->setVisible(false);

		tableviewMember = TableView::create(this, Size(654,317));
		//tableviewMember->setSelectedEnable(true);
		//tableviewMember->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 24, 1, 1), Vec2(0,0));
		//tableviewMember->setPressedActionEnabled(true);
		tableviewMember->setDirection(TableView::Direction::VERTICAL);
		tableviewMember->setAnchorPoint(Vec2(0,0));
		tableviewMember->setPosition(Vec2(80,49));
		tableviewMember->setDelegate(this);
		tableviewMember->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		layerMember->addChild(tableviewMember);

		layerstate = Layer::create();
		layerstate->setIgnoreAnchorPointForPosition(false);
		layerstate->setAnchorPoint(Vec2(0.5f,0.5f));
		layerstate->setContentSize(Size(800,480));
		layerstate->setPosition(Vec2(winsize.width/2,winsize.height/2));
		this->addChild(layerstate);
		layerstate->setVisible(false);
		tableviewstate = TableView::create(this, Size(675,360));
		//tableviewstate->setSelectedEnable(true);
		//tableviewstate->setSelectedScale9Texture();
		//tableviewstate->setPressedActionEnabled(true);
		tableviewstate->setDirection(TableView::Direction::VERTICAL);
		tableviewstate->setAnchorPoint(Vec2(0,0));
		tableviewstate->setPosition(Vec2(70,40));
		tableviewstate->setDelegate(this);
		tableviewstate->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		layerstate->addChild(tableviewstate);

		auto btnClose =(Button *)Helper::seekWidgetByName(mainPanel,"Button_close");
		btnClose->setTouchEnabled(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addTouchEventListener(CC_CALLBACK_2(FamilyUI::callBackExit, this));

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		//touchListener->onTouchBegan = CC_CALLBACK_2(FamilyUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(FamilyUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(FamilyUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(FamilyUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void FamilyUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	initFamilyFightUI();

	//GameMessageProcessor::sharedMsgProcessor()->sendReq(1708,this);
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void FamilyUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

void FamilyUI::changeUITabIndex( Ref * obj )
{
	auto tab= (UITab *)obj;
	int num =tab->getCurrentIndex();
	currType=num;
	switch(num)
	{
	case 0:
		{
			familyInfo_panel->setVisible(true);
			familyMember_panel->setVisible(false);
			familyState_panel->setVisible(false);
			layerMember->setVisible(false);
			layerstate->setVisible(false);

			familyNoticeLabel->setVisible(true);
			familyManifestoLabel->setVisible(true);

			l_FamilyFightLayer->setVisible(false);
			panel_familyFight->setVisible(false);
		}break;
	case 1:
		{
			familyInfo_panel->setVisible(false);
			familyMember_panel->setVisible(true);
			familyState_panel->setVisible(false);
			layerMember->setVisible(true);
			tableviewMember->reloadData();
			layerstate->setVisible(false);

			familyNoticeLabel->setVisible(false);
			familyManifestoLabel->setVisible(false);

			l_FamilyFightLayer->setVisible(false);
			panel_familyFight->setVisible(false);

			GameMessageProcessor::sharedMsgProcessor()->sendReq(1512,(void *)0,(void *)0);
		}break;
	case 2:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1511,this);

			familyInfo_panel->setVisible(false);
			familyMember_panel->setVisible(false);
			familyState_panel->setVisible(true);
			layerMember->setVisible(false);
			layerstate->setVisible(true);
			tableviewstate->reloadData();

			familyNoticeLabel->setVisible(false);
			familyManifestoLabel->setVisible(false);

			l_FamilyFightLayer->setVisible(false);
			panel_familyFight->setVisible(false);
		}break;
	case 3:
		{
			//����ȼ�
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(17);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[17];
			}

			if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				familyInfo_panel->setVisible(false);
				familyMember_panel->setVisible(false);
				familyState_panel->setVisible(false);
				layerMember->setVisible(false);
				layerstate->setVisible(false);
				familyNoticeLabel->setVisible(false);
				familyManifestoLabel->setVisible(false);

				l_FamilyFightLayer->setVisible(true);
				panel_familyFight->setVisible(true);
				//�����ǰ����ս״̬
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1544);
			}
			else
			{
				std::string str_des = StringDataManager::getString("feature_will_be_open_function");
				char str_level[20];
				sprintf(str_level,"%d",openlevel);
				str_des.append(str_level);
				str_des.append(StringDataManager::getString("feature_will_be_open_open"));
				GameView::getInstance()->showAlertDialog(str_des.c_str());
			}
		}break;
	}
}

void FamilyUI::repairFamilyNotice(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto familyNotice_ = FamilyChangeNotice::create(1);
		familyNotice_->setIgnoreAnchorPointForPosition(false);
		familyNotice_->setAnchorPoint(Vec2(0.5f, 0.5f));
		familyNotice_->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
		GameView::getInstance()->getMainUIScene()->addChild(familyNotice_);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyUI::repairFamilyManifesto(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto familyNotice_ = FamilyChangeNotice::create(2);
		familyNotice_->setIgnoreAnchorPointForPosition(false);
		familyNotice_->setAnchorPoint(Vec2(0.5f, 0.5f));
		familyNotice_->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
		GameView::getInstance()->getMainUIScene()->addChild(familyNotice_);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyUI::callbackFamilyShop(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		auto shopui_ = (FamilyShop*)mainscene->getChildByTag(familyShoptag);
		if (shopui_ == NULL)
		{
			auto shop = FamilyShop::create();
			shop->setIgnoreAnchorPointForPosition(false);
			shop->setAnchorPoint(Vec2(0.5f, 0.5f));
			shop->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			shop->setTag(familyShoptag);
			mainscene->addChild(shop);

			GameMessageProcessor::sharedMsgProcessor()->sendReq(1708, this);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyUI::callbackFamilyAlms(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		auto familalmsui_ = (FamilyAlms*)mainscene->getChildByTag(familyAlms);
		if (familalmsui_ == NULL)
		{
			auto alms = FamilyAlms::create();
			alms->setIgnoreAnchorPointForPosition(false);
			alms->setAnchorPoint(Vec2(0.5f, 0.5f));
			alms->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			alms->setTag(familyAlms);
			mainscene->addChild(alms);

			GameMessageProcessor::sharedMsgProcessor()->sendReq(1518, this);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}


void FamilyUI::callBackManageFamily(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto familyLeader = FamilyOperatorOfLeader::create();
		familyLeader->setIgnoreAnchorPointForPosition(false);
		familyLeader->setAnchorPoint(Vec2(1, 0));
		familyLeader->setPosition(Vec2(btn_mamage->getPosition().x - familyLeader->getContentSize().width / 2, btn_mamage->getPosition().y + 20));
		familyLeader->setTag(familyLeaderManager);
		loadLayer->addChild(familyLeader);
		//if self is leader req1505,clear vector
		if (selfPost_ != 3)
		{
			std::vector<CGuildMemberBase *>::iterator iter;
			for (iter = vectorApplyGuildMember.begin(); iter != vectorApplyGuildMember.end(); iter++)
			{
				delete * iter;
			}
			vectorApplyGuildMember.clear();

			GameMessageProcessor::sharedMsgProcessor()->sendReq(1505, (void*)curPageOfManageFamilyMember);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}


void FamilyUI::memberQuitFamily(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		const char *strings = StringDataManager::getString("family_btnmember_quit");
		char *str = const_cast<char*>(strings);
		GameView::getInstance()->showPopupWindow(str, 2, this, CC_CALLFUNCO_SELECTOR(FamilyUI::quitFamilySure), NULL);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}
void FamilyUI::manageQuitFamily( Ref * obj )
{
	const char *strings = StringDataManager::getString("family_btnmanage_quit");
	GameView::getInstance()->showAlertDialog(strings);
}

void FamilyUI::quitFamilySure( Ref * obj )
{
	if (selfPost_ !=1)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1507,this);
		this->closeAnim();
	}else
	{
		const char *strings = StringDataManager::getString("family_DismissOfleaderSure");
		GameView::getInstance()->showAlertDialog(strings);
	}
}

void FamilyUI::scrollViewDidScroll(cocos2d::extension::ScrollView * view )
{

}

void FamilyUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void FamilyUI::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void FamilyUI::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void FamilyUI::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void FamilyUI::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	if (currType ==1)
	{
		int index_ = cell->getIdx();
		long long playerId_ = vectorGuildMemberBase.at(index_)->id();
		if (playerId_ != selfId_)
		{
			int pointX = cell->getPosition().x;
			int pointY = cell->getPosition().y;
			int offX = table->getContentOffset().x;
			int offY = table->getContentOffset().y;

			auto memberOperator =FamilyMemberOperator::create(selfPost_,index_);
			memberOperator->setIgnoreAnchorPointForPosition(false);
			memberOperator->setAnchorPoint(Vec2(0.5f,0.5f));
			memberOperator->setPosition(Vec2(100,layerMember->getContentSize().height/2 - memberOperator->getContentSize().height/2-50));
			memberOperator->setLocalZOrder(10);
			layerMember->addChild(memberOperator);
		}
	}
}

cocos2d::Size FamilyUI::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	if (currType==1)
	{
		return Size(650,60);
	}else if (currType==2)
	{
		return Size(675,40);
	}else
	{
		return Size(0,0);
	}
}

cocos2d::extension::TableViewCell* FamilyUI::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	__String * string =__String::createWithFormat("%d",idx);	
	auto cell =table->dequeueCell();
	cell=new TableViewCell();
	cell->autorelease();

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	switch (currType)
	{
	case 1:
		{
			auto spbg = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
			spbg->setPreferredSize(Size(650,60));
			spbg->setCapInsets(Rect(15,30,1,1));
			spbg->setAnchorPoint(Vec2(0,0));
			spbg->setPosition(Vec2(0,0));
			cell->addChild(spbg);

			std::string familyerName_ = vectorGuildMemberBase.at(idx)->name();
			auto labelMemberName=Label::createWithTTF(familyerName_.c_str(),APP_FONT_NAME,16);
			labelMemberName->setAnchorPoint(Vec2(0,0));
			labelMemberName->setPosition(Vec2(13,25));
			cell->addChild(labelMemberName);

			int vipLevel_ = vectorGuildMemberBase.at(idx)->viplevel();
			if (vipLevel_ > 0)
			{
				auto vipNode = MainScene::addVipInfoByLevelForNode(vipLevel_);
				cell->addChild(vipNode);
				vipNode->setPosition(Vec2(labelMemberName->getPositionX() + labelMemberName->getContentSize().width + vipNode->getContentSize().width/2,
											labelMemberName->getPositionY() + vipNode->getContentSize().height/2));
			}

			int familyerJob_ = vectorGuildMemberBase.at(idx)->post();
			const char *strings;
			switch(familyerJob_)
			{
			case 1:
				{
					strings = StringDataManager::getString("family_member_Master");
				}break;
			case 2:
				{
					strings = StringDataManager::getString("family_member_SecondMaster");
				}break;
			case 3:
				{
					strings = StringDataManager::getString("family_member_member");
				}break;
			}
			char *str=const_cast<char*>(strings);
			auto labelMemberJob=Label::createWithTTF(str,APP_FONT_NAME,16);
			labelMemberJob->setAnchorPoint(Vec2(0,0));
			labelMemberJob->setPosition(Vec2(142,25));
			cell->addChild(labelMemberJob);

			int memberLevel_ =vectorGuildMemberBase.at(idx)->level();
			char levelStr_[5];
			sprintf(levelStr_,"%d",memberLevel_);
			auto labelMemberLevel=Label::createWithTTF(levelStr_,APP_FONT_NAME,16);
			labelMemberLevel->setAnchorPoint(Vec2(0,0));
			labelMemberLevel->setPosition(Vec2(215,25));
			cell->addChild(labelMemberLevel);

			int memberPression_ =vectorGuildMemberBase.at(idx)->profession();
			std::string pressionStr_ = BasePlayer::getProfessionNameIdxByIndex(memberPression_);

			auto labelMemberPression=Label::createWithTTF(pressionStr_.c_str(),APP_FONT_NAME,16);
			labelMemberPression->setAnchorPoint(Vec2(0,0));
			labelMemberPression->setPosition(Vec2(278,25));
			cell->addChild(labelMemberPression);

			int memberContrbution_ =vectorGuildMemberBase.at(idx)->guilddonate();
			char donateStr[10];
			sprintf(donateStr,"%d",memberContrbution_);
			auto labelMembercontrbution=Label::createWithTTF(donateStr,APP_FONT_NAME,16);
			labelMembercontrbution->setAnchorPoint(Vec2(0,0));
			labelMembercontrbution->setPosition(Vec2(355,25));
			cell->addChild(labelMembercontrbution);

			//const char *stringSort_ = StringDataManager::getString("family_sortString_");
			int memberFight = vectorGuildMemberBase.at(idx)->fightpoint();
			char strFight[20];
			sprintf(strFight,"%d",memberFight);
			auto labelMemberSort=Label::createWithTTF(strFight,APP_FONT_NAME,16);
			labelMemberSort->setAnchorPoint(Vec2(0,0));
			labelMemberSort->setPosition(Vec2(455,25));
			cell->addChild(labelMemberSort);

			//const char *memberState_;
			std::string memberState_;
			if (vectorGuildMemberBase.at(idx)->isonline()==1)
			{
				const char * state_  = StringDataManager::getString("family_member_state_online");
				memberState_ =state_;
			}else
			{
				long long lastTime_ =vectorGuildMemberBase.at(idx)->lastlogintime();
				if ((long long)(lastTime_/24)>0)//1 day
				{
					if ((long long)(lastTime_/(long long)(24*30)) >0)//1 moth
					{
						const char* strings = StringDataManager::getString("family_mothago");
						int online_ =int (lastTime_/(long long)(24*30));
						char onlineStr_[20];
						sprintf(onlineStr_,"%d",online_);
						memberState_ =onlineStr_;
						memberState_.append(strings);
					}else
					{
						const char* strings = StringDataManager::getString("family_dayAgo");
						int online_ =int (lastTime_/24);
						char onlineStr_[20];
						sprintf(onlineStr_,"%d",online_);
						memberState_ =onlineStr_;
						memberState_.append(strings);
					}
				}else
				{
					if (lastTime_ <= 1)
					{
						const char* strings = StringDataManager::getString("family_minAgo");
						memberState_.append(strings);
					}else
					{
						const char* strings = StringDataManager::getString("family_hourAgo");
						char onlineStr_[20];
						sprintf(onlineStr_,"%lld",lastTime_);
						memberState_ = onlineStr_;
						memberState_.append(strings);
					}
				}
			}
			auto labelMemberOnLine=Label::createWithTTF(memberState_.c_str(),APP_FONT_NAME,16);
			labelMemberOnLine->setAnchorPoint(Vec2(0,0));
			labelMemberOnLine->setPosition(Vec2(580,25));
			cell->addChild(labelMemberOnLine);

			if (idx == vectorGuildMemberBase.size()-3)
			{
				if (hasNextPageOfMember ==true)
				{
					curPageOfFamilyMember++;
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1512,(void *)curPageOfFamilyMember,(void *)0);
				}
			}
		}break;
	case 2:
		{	
			auto spbg = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
			spbg->setAnchorPoint(Vec2(0,0));
			spbg->setPreferredSize(Size(670,30));
			spbg->setCapInsets(Rect(9,15,1,1));
			spbg->setPosition(Vec2(2,0));
			cell->addChild(spbg);

			std::string labelMsg_ = GameView::getInstance()->guildRecordVector.at(idx)->msg();
			auto labelContent=Label::createWithTTF(labelMsg_.c_str(),APP_FONT_NAME,16);
			labelContent->setAnchorPoint(Vec2(0,0.5f));
			labelContent->setPosition(Vec2(10,15));
			labelContent->setColor(Color3B(47,93,13));
			cell->addChild(labelContent,10);
		}break;
	}
	return cell;
}

ssize_t FamilyUI::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	if (currType==1)
	{
		return vectorGuildMemberBase.size();
	}else if (currType==2)
	{
		return GameView::getInstance()->guildRecordVector.size();
	}else
	{
		return 0;
	}
}

void FamilyUI::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void FamilyUI::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void FamilyUI::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void FamilyUI::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void FamilyUI::downListMember( int index )
{
	
}

void FamilyUI::callBackDesFamily(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		std::map<int, std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(5);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end())
		{
			return;
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			{
				auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[5].c_str());
				GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				showSystemInfo->setIgnoreAnchorPointForPosition(false);
				showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				showSystemInfo->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyUI::initFamilyFightUI()
{
	Size winSize = Director::getInstance()->getVisibleSize();
	l_FamilyFightLayer=Layer::create();
	l_FamilyFightLayer->setIgnoreAnchorPointForPosition(false);
	l_FamilyFightLayer->setAnchorPoint(Vec2(0.5f,0.5f));
	l_FamilyFightLayer->setPosition(Vec2(winSize.width/2,winSize.height/2));
	l_FamilyFightLayer->setContentSize(Size(800,480));
	this->addChild(l_FamilyFightLayer);
	l_FamilyFightLayer->setVisible(false);

	auto familyFightUI = FamilyFightUI::create();
	l_FamilyFightLayer->addChild(familyFightUI);
	familyFightUI->setTag(kTagFamilyFightUI);

	// �������ս���ճ����Req1545����Ϣ
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1545);
}

FamilyFightUI * FamilyUI::GetFamilyFightUI()
{
	auto temp = (FamilyFightUI*)l_FamilyFightLayer->getChildByTag(kTagFamilyFightUI);
	if (temp)
		return temp;

	return NULL;
}

void FamilyUI::callbackFamilyUplLevel(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (selfPost_ != CGuildBase::position_master)
		{
			const char * strings_ = StringDataManager::getString("family_zuzhang_isUpfamilyLevel");
			GameView::getInstance()->showAlertDialog(strings_);
		}
		else
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1514);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyUI::openFamilyFightUI()
{
	familyInfo_panel->setVisible(false);
	familyMember_panel->setVisible(false);
	familyState_panel->setVisible(false);
	layerMember->setVisible(false);
	layerstate->setVisible(false);
	familyNoticeLabel->setVisible(false);
	familyManifestoLabel->setVisible(false);

	l_FamilyFightLayer->setVisible(true);
	panel_familyFight->setVisible(true);

	// ���UITab����ڱ�ǩλ�
	m_familytab->setDefaultPanelByIndex(3);


	//�����ǰ����ս״̬
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1544);
}






