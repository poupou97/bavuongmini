#ifndef _FAMILYAMLS_ 
#define _FAMILYAMLS_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "FamilyUI.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class FamilyAlms:public UIScene
{
public:
	FamilyAlms(void);
	~FamilyAlms(void);

	static FamilyAlms * create();
	bool init();
	void onEnter();
	void onExit();

	void callBackInput(Ref *pSender, Widget::TouchEventType type);
	void callBackSurContrbution(Ref *pSender, Widget::TouchEventType type);
	void callBackGetCountNum(Ref * obj);
	void callBackClose(Ref *pSender, Widget::TouchEventType type);
	int getCurContrGoodsAmount();

	void refreshInfo(bool isDefault);
private:
	FamilyUI * familyui;

	Text * labelFamilyLevel;
	Text * labelFamilyContrbution;

	int contributionCount;
	Text * labelCount;
	Text * labelContrbutionOfMine;
	Text * familyContrbutionAdd;
	Text * familyPersonContrbutionAdd;
};
#endif;

