#include "FamilyShopCount.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "AppMacros.h"

FamilyShopCount::FamilyShopCount(void)
{
}


FamilyShopCount::~FamilyShopCount(void)
{
}

FamilyShopCount * FamilyShopCount::create(CGuildCommodity * commodity)
{
	auto count_ =new FamilyShopCount();
	if (count_ && count_->init(commodity))
	{
		count_->autorelease();
		return count_;
	}
	CC_SAFE_DELETE(count_);
	return NULL;
}

bool FamilyShopCount::init(CGuildCommodity * commodity)
{
	if (UIScene::init())
	{
		winsize = Director::getInstance()->getVisibleSize();

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(298, 403));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);
		//���UI
		if(LoadSceneLayer::BuyCalculatorsLayer->getParent() != NULL)
		{
			LoadSceneLayer::BuyCalculatorsLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (Layout*)LoadSceneLayer::s_pPanel->copy();
		auto ppanel = LoadSceneLayer::BuyCalculatorsLayer;
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->getVirtualRenderer()->setContentSize(Size(298, 403));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		//عرհ�ť
		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(FamilyShopCount::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);
		//��ƷIcon�׿
		auto imageView_dikuang = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_dikuang");
		switch(commodity->goods().quality())
		{
		case 1 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_white.png");
			}
			break;
		case 2 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_green.png");
			}
			break;
		case 3 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_bule.png");
			}
			break;
		case 4 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_purple.png");
			}
			break;
		case 5 :
			{
				imageView_dikuang->loadTexture("res_ui/smdi_orange.png");
			}
			break;
		}

		std::string headImageStr ="res_ui/props_icon/";
		headImageStr.append(commodity->goods().icon());
		headImageStr.append(".png");
		auto imageView_icon = ImageView::create();
		imageView_icon->loadTexture(headImageStr.c_str());
		imageView_icon->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_icon->setPosition(Vec2(62,344));
		imageView_icon->setScale(0.85f);
		u_layer->addChild(imageView_icon);

		auto l_single_name = (Text*)Helper::seekWidgetByName(ppanel,"Label_single_name");
		l_single_name->setVisible(true);
		l_single_name->setString(StringDataManager::getString("measurement_contribute"));
		auto l_total_name = (Text*)Helper::seekWidgetByName(ppanel,"Label_total_name");
		l_total_name->setVisible(true);
		l_total_name->setString(StringDataManager::getString("measurement_contribute"));

		//򵥼۵ĵ�λͼ�(�Ԫ��)
		auto ImageView_IngotOrCoins_single = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_IngotOrCoins_single");
		ImageView_IngotOrCoins_single->setVisible(false);

		//�ܼ۵ĵ�λͼ�(�Ԫ��)
		auto ImageView_IngotOrCoins_total = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_IngotOrCoins_total");
		ImageView_IngotOrCoins_total->setVisible(false);
		//��Ʒ��
		auto l_name = (Text*)Helper::seekWidgetByName(ppanel,"Label_name");
		l_name->setString(commodity->goods().name().c_str());
		l_name->setColor(GameView::getInstance()->getGoodsColorByQuality(commodity->goods().quality()));
		//Ƶ�� 
		basePrice = commodity->guildhonor();
		auto l_priceOfOne = (Text*)Helper::seekWidgetByName(ppanel,"Label_SingleValue");
		l_priceOfOne->setPosition(Vec2(185,334));
		char s_basePrice[20];
		sprintf(s_basePrice,"%d",basePrice);
		l_priceOfOne->setString(s_basePrice);
		//۹������  
		l_buyNum = (Text*)Helper::seekWidgetByName(ppanel,"Label_amount");
		l_buyNum->setString("");
		//�ܼ  
		l_totalPrice = (Text*)Helper::seekWidgetByName(ppanel,"Label_TotalValue");
		l_totalPrice->setString("");
		l_totalPrice->setPosition(Vec2(166,267));

		int index = 1;
		for (int i=0;i<3;i++)
		{
			for (int j=0;j<3;j++)
			{

				char str_index[2];
				sprintf(str_index,"%d",index);

				auto extractAnnex=Button::create();
				extractAnnex->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
				extractAnnex->setTouchEnabled(true);
				extractAnnex->setPressedActionEnabled(true);
				extractAnnex->setAnchorPoint(Vec2(0.5f,0.5f));
				extractAnnex->setTag(index);
				//extractAnnex->setPosition(Vec2(31+j*61,185- i*50));
				extractAnnex->setPosition(Vec2(60+j*61,207- i*50));
				extractAnnex->addTouchEventListener(CC_CALLBACK_2(FamilyShopCount::getAmount, this));

				auto label_Num=Label::createWithTTF(str_index, APP_FONT_NAME, 20);
				label_Num->setAnchorPoint(Vec2(0.5f,0.5f));
				label_Num->setPosition(Vec2(0,0));
				extractAnnex->addChild(label_Num);
				u_layer->addChild(extractAnnex);
				/*
				UITextButton *extractAnnex=Button::create();
				extractAnnex->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
				extractAnnex->setTouchEnabled(true);
				extractAnnex->setText(str_index);
				extractAnnex->setFontSize(20);
				extractAnnex->setAnchorPoint(Vec2(0,0));
				extractAnnex->setTag(index);
				extractAnnex->setPosition(Vec2(31+j*61,185- i*50));
				extractAnnex->addTouchEventListener(CC_CALLBACK_2(FamilyShopCount::getAmount));
				u_layer->addChild(extractAnnex);
				*/
				index++;
			}
		}

		auto btn_clear=Button::create();
		btn_clear->setTouchEnabled(true);
		btn_clear->loadTextures("res_ui/enjian_y.png","res_ui/enjian_y.png","");
		btn_clear->setAnchorPoint(Vec2(0,0));
		btn_clear->setPosition(Vec2(216,185));
		btn_clear->setTag(index);
		btn_clear->addTouchEventListener(CC_CALLBACK_2(FamilyShopCount::deleteNum, this));
		u_layer->addChild(btn_clear);
		auto deleteSp=ImageView::create();
		deleteSp->loadTexture("res_ui/jisuanqi/jiantou.png");
		deleteSp->setAnchorPoint(Vec2(0.5f,0.5f));
		deleteSp->setPosition(Vec2(btn_clear->getContentSize().width/2,btn_clear->getContentSize().height/2));
		btn_clear->addChild(deleteSp);
		/*
		UITextButton *btn_Zero=Button::create();
		btn_Zero->setText("0");
		btn_Zero->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		btn_Zero->setTouchEnabled(true);
		btn_Zero->setFontSize(25);
		btn_Zero->setAnchorPoint(Vec2(0,0));
		btn_Zero->setTag(0);
		btn_Zero->setPosition(Vec2(216,135));
		btn_Zero->addTouchEventListener(CC_CALLBACK_2(FamilyShopCount::getAmount));
		u_layer->addChild(btn_Zero);
		*/
		auto button_Zero=Button::create();
		button_Zero->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		button_Zero->setTouchEnabled(true);
		button_Zero->setTitleFontSize(25);
		button_Zero->setAnchorPoint(Vec2(0.5f,0.5f));
		button_Zero->setTag(0);
		button_Zero->setPosition(Vec2(242,157));
		button_Zero->setPressedActionEnabled(true);
		button_Zero->addTouchEventListener(CC_CALLBACK_2(FamilyShopCount::getAmount, this));
		u_layer->addChild(button_Zero);

		auto label_Zero=Label::createWithTTF("0", APP_FONT_NAME, 20);
		label_Zero->setAnchorPoint(Vec2(0.5f,0.5f));
		label_Zero->setPosition(Vec2(0,0));
		button_Zero->addChild(label_Zero);

		/*
		UITextButton *btn_C=Button::create();
		btn_C->setText("C");
		btn_C->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		btn_C->setTouchEnabled(true);
		btn_C->setFontSize(25);
		btn_C->setAnchorPoint(Vec2(0,0));
		btn_C->setPosition(Vec2(216,85));
		btn_C->addTouchEventListener(CC_CALLBACK_2(FamilyShopCount::clearNum));
		u_layer->addChild(btn_C);
		*/
		auto Button_C=Button::create();
		Button_C->loadTextures("res_ui/enjian_g.png","res_ui/enjian_g.png","");
		Button_C->setTouchEnabled(true);
		Button_C->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_C->setPosition(Vec2(242,107));
		Button_C->setPressedActionEnabled(true);
		Button_C->addTouchEventListener(CC_CALLBACK_2(FamilyShopCount::clearNum, this));
		u_layer->addChild(Button_C);

		auto label_C=Label::createWithSystemFont("C", APP_FONT_NAME, 20);
		label_C->setAnchorPoint(Vec2(0.5f,0.5f));
		label_C->setPosition(Vec2(0,0));
		Button_C->addChild(label_C);

		auto btn_buy = (Button *)Helper::seekWidgetByName(ppanel,"Button_enter");
		btn_buy->setTouchEnabled(true);
		btn_buy->addTouchEventListener(CC_CALLBACK_2(FamilyShopCount::BuyEvent, this));
		btn_buy->setTag(commodity->id());
		btn_buy->setPressedActionEnabled(true);
		auto l_buy = (Text*)Helper::seekWidgetByName(ppanel,"Label_enter");
		const char *str3 = StringDataManager::getString("goods_auction_buy");
		char* shop_buy =const_cast<char*>(str3);
		l_buy->setString(shop_buy);

		setToDefaultNum();

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setAnchorPoint(Vec2(0,0));
		return true;
	}
	return false;
}

void FamilyShopCount::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FamilyShopCount::onExit()
{
	UIScene::onExit();
}

bool FamilyShopCount::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return true;
}

void FamilyShopCount::BuyEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (buyNum > 0)
		{
			auto btn = (Button *)pSender;
			int id_ = btn->getTag();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1709, (void*)id_, (void *)buyNum);
		}
		else
		{
			const char *str1 = StringDataManager::getString("goods_shop_pleaseInputNum");
			char* pleaseinputNum = const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(pleaseinputNum);
		}
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyShopCount::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void FamilyShopCount::getAmount(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button *)pSender;
		int id_ = btn->getTag();

		if (buyNum>999)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("goldStore_buy_inputNum"));
			return;
		}
		// 	char num[3];
		// 	sprintf(num,"%d",id_);
		// 	str_buyNum.append(num);
		if (buyNum <= 0 && id_ == 0)
		{
			return;
		}
		char num[3];
		sprintf(num, "%d", id_);
		if (std::atoi(str_buyNum.c_str()) <= 0)
		{
			str_buyNum = num;
		}
		else
		{
			str_buyNum.append(num);
		}
		l_buyNum->setString(str_buyNum.c_str());
		buyNum = std::atoi(str_buyNum.c_str());

		buyPrice = basePrice * buyNum;
		char str_buyPrice[20];
		sprintf(str_buyPrice, "%d", buyPrice);
		l_totalPrice->setString(str_buyPrice);
		/*
		if (buyPrice <= GameView::getInstance()->myplayer->player->honorpoint())   //�Ǯ���
		{
		l_totalPrice->setColor(Color3B(225,255,138));   //��ɫ
		}
		else
		{
		l_totalPrice->setColor(Color3B(255,51,51));   //��ɫ
		}
		*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyShopCount::clearNum(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		str_buyNum.clear();
		l_buyNum->setString(str_buyNum.c_str());
		buyNum = std::atoi(str_buyNum.c_str());

		l_totalPrice->setString("");
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyShopCount::deleteNum(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (str_buyNum.size()>0)
		{
			str_buyNum.erase(str_buyNum.end() - 1);
			l_buyNum->setString(str_buyNum.c_str());
			buyNum = std::atoi(str_buyNum.c_str());

			buyPrice = basePrice * buyNum;
			char str_buyPrice[20];
			sprintf(str_buyPrice, "%d", buyPrice);
			l_totalPrice->setString(str_buyPrice);

			/*
			if (buyPrice <= GameView::getInstance()->myplayer->player->honorpoint())   //����ֵ�㹻
			{
			l_totalPrice->setColor(Color3B(225,255,138));   //��ɫ
			}
			else
			{
			l_totalPrice->setColor(Color3B(255,51,51));   //��ɫ
			}
			*/
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyShopCount::setToDefaultNum()
{
	l_buyNum->setString("1");
	buyNum = 1;

	buyPrice = basePrice * buyNum;
	char str_buyPrice [20];
	sprintf(str_buyPrice,"%d",buyPrice);
	l_totalPrice->setString(str_buyPrice);
	/*
	if (buyPrice <= GameView::getInstance()->myplayer->player->honorpoint())   //Ǯ���
	{
		l_totalPrice->setColor(Color3B(225,255,138));   //��ɫ
	}
	else
	{
		l_totalPrice->setColor(Color3B(255,51,51));   //��ɫ
	}
	*/
}
