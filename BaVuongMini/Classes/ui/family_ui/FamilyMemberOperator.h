
#ifndef FAMILY_FAMILYMEMBER_OPERATOR
#define FAMILY_FAMILYMEMBER_OPERATOR

#include "../extensions/UIScene.h"

class FamilyMemberOperator:public UIScene
{
public:
	FamilyMemberOperator(void);
	~FamilyMemberOperator(void);
	
	enum
	{
		FAMILYLEADER=1,
		FAMILYVICELEADER,
		FAMILYMEMBER,
	};

	enum
	{
		operationCheck=0,
		operationFriend,
		operationprivate,
		operationAppoint,
		operationKickout,
		operationRemove,
		operationDeise,
		operationBlack,
	};

	static FamilyMemberOperator *create(int post_,int index_);
	bool init(int post_,int index_);
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void leaderAndMember(Ref * obj);
	void leaderAndViceLeader(Ref * obj);
	void viceLeaderAndMember(Ref * obj);
	void viceLeaderAndLeader(Ref * obj);
	void pressionOfMember(Ref * obj);

	void isSureKickOut(Ref * obj);
	void isSureDemis(Ref * obj);
	void isSureAppoint(Ref * obj);
	void isSureRemove(Ref * obj);

	void removeSelf(Ref * obj);
private:
	Size winSize;
	int opherPost_;

	long long otherplayerId_;
	std::string otherPlayerName_;
	int otherCountry;
	int otherViplevel;
	int otherLevel;
	int otherPression;
};

#endif;
