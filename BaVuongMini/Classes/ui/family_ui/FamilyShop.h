#ifndef _FAMILYUIMAINSHOP_H_
#define _FAMILYUIMAINSHOP_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class FamilyUI;
class FamilyShop:public UIScene,cocos2d::extension::TableViewDataSource,public cocos2d::extension::TableViewDelegate
{
public:
	FamilyShop(void);
	~FamilyShop(void);

	static FamilyShop * create();
	bool init();
	void onEnter();
	void onExit();


	void callBackExit(Ref *pSender, Widget::TouchEventType type);
public:
	Size winsize;
	Layer * loadLayer;
	TableView * tableviewMember;
	FamilyUI * familyui;
	Text * labelContrbution;
public:
	////familyMember
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	// default implements are used to call script callback if exist
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
	
};

//////////////////////////////////////
class goodsItemCell:public UIScene
{
public:
	goodsItemCell(void);
	~goodsItemCell(void);

	static goodsItemCell * create(int idx);
	bool init(int idx);

	void onEnter();
	void onExit();

	std::string getColorSpByQuality( int quality );
	void callBackBuy(Ref *pSender, Widget::TouchEventType type);
	void showGoodsInfo(Ref *pSender, Widget::TouchEventType type);

	Size winsize;
	
};

#endif
