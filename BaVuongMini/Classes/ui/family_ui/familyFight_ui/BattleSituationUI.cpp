#include "BattleSituationUI.h"
#include "../../extensions/UIScene.h"
#include "../../../messageclient/element/CGuildBattleInfo.h"
#include "AppMacros.h"
#include "../../../utils/GameConfig.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/role/BasePlayer.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "cocostudio\CCSGUIReader.h"

#define  SelectImageTag 458

#define kTag_TableView_blue 500
#define kTag_TableView_red 510

#define Action_Progress_Speed 160

BattleSituationUI::BattleSituationUI():
lastSelectCellId(0),
selectCellId(0)
{
}


BattleSituationUI::~BattleSituationUI()
{
	delete blueInfo;
	delete redInfo;
}

BattleSituationUI * BattleSituationUI::create()
{
	auto battleSituationUI = new BattleSituationUI();
	if (battleSituationUI && battleSituationUI->init())
	{
		battleSituationUI->autorelease();
		return battleSituationUI;
	}
	CC_SAFE_DELETE(battleSituationUI);
	return NULL;
}

bool BattleSituationUI::init()
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate(); 

		blueInfo = new CGuildBattleInfo();
		redInfo = new CGuildBattleInfo();;

		u_layer = Layer::create();
		addChild(u_layer);

		//���UI
		auto ppanel = (ui::Layout *) cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/zhankuang_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->setPosition(Vec2::ZERO);	
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto Button_close = (ui::Button*)ui::Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(BattleSituationUI::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);

		l_blueName = (Label*)ui::Helper::seekWidgetByName(ppanel,"Label_nameA");
		l_redName = (Label*)ui::Helper::seekWidgetByName(ppanel,"Label_nameB");
// 		ImageView_blueBlood = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_bossHpA");
// 		ImageView_redBlood = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_bossHpB");
// 		l_blueBloodValue= (Text*)Helper::seekWidgetByName(ppanel,"Label_bossHpA");
// 		l_redBloodValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_bossHpBValue");

		pt_blueBlood = ProgressTimer::create(Sprite::create("res_ui/jiazuzhan/bosshp2.png"));
		pt_blueBlood->setType(ProgressTimer::Type::BAR);
		pt_blueBlood->setMidpoint(Vec2(0,0));
		pt_blueBlood->setBarChangeRate(Vec2(1, 0));
		addChild(pt_blueBlood);
		pt_blueBlood->setAnchorPoint(Vec2(0,0));
		pt_blueBlood->setPosition(Vec2(110, 348));

		pt_redBlood = ProgressTimer::create(Sprite::create("res_ui/jiazuzhan/bosshp2.png"));
		pt_redBlood->setType(ProgressTimer::Type::BAR);
		pt_redBlood->setMidpoint(Vec2(0,0));
		pt_redBlood->setBarChangeRate(Vec2(1, 0));
		addChild(pt_redBlood);
		pt_redBlood->setAnchorPoint(Vec2(0,0));
		pt_redBlood->setPosition(Vec2(505, 348));

		l_blueBloodValue = Label::createWithTTF("",APP_FONT_NAME,14);
		l_blueBloodValue->setAnchorPoint(Vec2(.5f,.5f));
		l_blueBloodValue->setPosition(Vec2(204,356));
		addChild(l_blueBloodValue);

		l_redBloodValue = Label::createWithTTF("",APP_FONT_NAME,14);
		l_redBloodValue->setAnchorPoint(Vec2(.5f,.5f));
		l_redBloodValue->setPosition(Vec2(599,356));
		addChild(l_redBloodValue);

		m_tableView_blue = TableView::create(this,Size(329,245));
		m_tableView_blue->setDirection(TableView::Direction::VERTICAL);
		m_tableView_blue->setAnchorPoint(Vec2(0,0));
		m_tableView_blue->setPosition(Vec2(62,48));
		m_tableView_blue->setDelegate(this);
		m_tableView_blue->setTag(kTag_TableView_blue);
		m_tableView_blue->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(m_tableView_blue);

		m_tableView_red = TableView::create(this,Size(329,245));
		m_tableView_red->setDirection(TableView::Direction::VERTICAL);
		m_tableView_red->setAnchorPoint(Vec2(0,0));
		m_tableView_red->setPosition(Vec2(411,48));
		m_tableView_red->setDelegate(this);
		m_tableView_red->setTag(kTag_TableView_red);
		m_tableView_red->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(m_tableView_red);

		this->setContentSize(ppanel->getContentSize());
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		return true;
	}
	return false;
}

void BattleSituationUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1548);
}

void BattleSituationUI::onExit()
{
	UIScene::onExit();
}

bool BattleSituationUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void BattleSituationUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void BattleSituationUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void BattleSituationUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void BattleSituationUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void BattleSituationUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void BattleSituationUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void BattleSituationUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	selectCellId = cell->getIdx();
	//�ȡ���ϴ�ѡ��״̬�����±���ѡ��״̬
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();
}

cocos2d::Size BattleSituationUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(326,38);
}

cocos2d::extension::TableViewCell* BattleSituationUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	CGuildBattleInfo* tempInfo;
 	if (table->getTag() == kTag_TableView_blue)
	{
		tempInfo = blueInfo;
	}
	else
	{
		tempInfo = redInfo;
	}

	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(Size(326,37));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setPosition(Vec2(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

 	Color3B color_green = Color3B(47,93,13);

	// ��ͼ
	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(1, 0));
	sprite_bigFrame->setCapInsets(Rect(9,14,1,1));
	sprite_bigFrame->setPreferredSize(Size(326,37));
	cell->addChild(sprite_bigFrame);

 	//��
	auto l_name = Label::createWithTTF(tempInfo->players(idx).rolename().c_str(), APP_FONT_NAME, 15);
  	l_name->setColor(color_green);
  	l_name->setAnchorPoint(Vec2(0.0f, 0.5f));
  	l_name->setPosition(Vec2(30, 18));
  	cell->addChild(l_name);
  
  	int countryId = tempInfo->players(idx).country();
  	if (countryId > 0 && countryId <6)
  	{
  		std::string countryIdName = "country_";
  		char id[2];
  		sprintf(id, "%d", countryId);
  		countryIdName.append(id);
  		std::string iconPathName = "res_ui/country_icon/";
  		iconPathName.append(countryIdName);
  		iconPathName.append(".png");
  		auto countrySp_ = Sprite::create(iconPathName.c_str());
  		countrySp_->setAnchorPoint(Vec2(1.0,0.5f));
  		countrySp_->setPosition(Vec2(l_name->getPositionX()-1,l_name->getPositionY()));
  		countrySp_->setScale(0.7f);
  		cell->addChild(countrySp_);
  	}
  	//vipInfo
  	// vipƿ��ؿ��
  	bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
  	if (vipEnabled)
  	{
  		if (tempInfo->players(idx).viplevel() > 0)
  		{
  			auto pNode = MainScene::addVipInfoByLevelForNode(tempInfo->players(idx).viplevel());
  			if (pNode)
  			{
  				cell->addChild(pNode);
  				pNode->setPosition(Vec2(l_name->getPosition().x+l_name->getContentSize().width+13,l_name->getPosition().y));
  			}
  		}
  	}

	//�ְҵ
	auto l_profession = Label::createWithTTF(BasePlayer::getProfessionNameIdxByIndex(tempInfo->players(idx).profession()).c_str(), APP_FONT_NAME, 15);
	l_profession->setColor(color_green);
	l_profession->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_profession->setPosition(Vec2(165, 18));
	cell->addChild(l_profession);
	//�ȼ�
	char s_lv[10];
	sprintf(s_lv,"%d",tempInfo->players(idx).rolelevel());
	auto l_lv = Label::createWithTTF(s_lv, APP_FONT_NAME, 15);
	l_lv->setColor(color_green);
	l_lv->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_lv->setPosition(Vec2(205, 18));
	cell->addChild(l_lv);
	//ɱ���
	char s_killNumber[20];
	sprintf(s_killNumber,"%d",tempInfo->players(idx).killed());
	auto l_killNumber = Label::createWithTTF(s_killNumber, APP_FONT_NAME, 15);
	l_killNumber->setColor(color_green);
	l_killNumber->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_killNumber->setPosition(Vec2(253, 18));
	cell->addChild(l_killNumber);
	//������
	char s_deadNumber[20];
	sprintf(s_deadNumber,"%d",tempInfo->players(idx).death());
	auto l_deadNumber = Label::createWithTTF(s_deadNumber, APP_FONT_NAME, 15);
	l_deadNumber->setColor(color_green);
	l_deadNumber->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_deadNumber->setPosition(Vec2(301, 18));
	cell->addChild(l_deadNumber);

	return cell;
}

ssize_t BattleSituationUI::numberOfCellsInTableView( TableView *table )
{
	if (table->getTag() == kTag_TableView_blue)
	{
		return blueInfo->players_size();
	}
	else if (table->getTag() == kTag_TableView_red)
	{
		return redInfo->players_size();
	}
	else
	{
		return 0;
	}
}

void BattleSituationUI::RefreshOwnData()
{
	m_tableView_blue->reloadData();
}

void BattleSituationUI::RefreshOwnDataWithOutChangeOffSet()
{
	Vec2 _s = m_tableView_blue->getContentOffset();
	int _h = m_tableView_blue->getContentSize().height + _s.y;
	m_tableView_blue->reloadData();
	Vec2 temp = Vec2(_s.x,_h - m_tableView_blue->getContentSize().height);
	m_tableView_blue->setContentOffset(temp); 
}

void BattleSituationUI::RefreshEmptyData()
{
	m_tableView_red->reloadData();
}

void BattleSituationUI::RefreshEmptyDataWithOutChangeOffSet()
{
	Vec2 _s = m_tableView_red->getContentOffset();
	int _h = m_tableView_red->getContentSize().height + _s.y;
	m_tableView_red->reloadData();
	Vec2 temp = Vec2(_s.x,_h - m_tableView_red->getContentSize().height);
	m_tableView_red->setContentOffset(temp); 
}

void BattleSituationUI::RefreshUI()
{
	l_blueName->setString(blueInfo->guildname().c_str());
	l_redName->setString(redInfo->guildname().c_str());;

	//ImageView_blueBlood->setTextureRect(Rect(0,0,ImageView_blueBlood->getContentSize().width*blueInfo->bosshp()*1.0f/100.f,ImageView_blueBlood->getContentSize().height));
	//ImageView_redBlood->setTextureRect(Rect(0,0,ImageView_redBlood->getContentSize().width*redInfo->bosshp()*1.0f/100.f,ImageView_redBlood->getContentSize().height));

	auto pt_anm_blue = CCProgressTo::create((100-pt_blueBlood->getPercentage())*1.0f/Action_Progress_Speed, blueInfo->bosshp()*1.0f);
	pt_blueBlood->runAction(pt_anm_blue);

	auto pt_anm_red= CCProgressTo::create((100-pt_redBlood->getPercentage())*1.0f/Action_Progress_Speed, redInfo->bosshp()*1.0f);
	pt_redBlood->runAction(pt_anm_red);

	std::string blueBloodValue;
	char s_blueBloodValue[20];
	sprintf(s_blueBloodValue,"%d",blueInfo->bosshp());
	blueBloodValue.append(s_blueBloodValue);
	blueBloodValue.append("%");
	l_blueBloodValue->setString(blueBloodValue.c_str());

	std::string redBloodValue;
	char s_redBloodValue[20];
	sprintf(s_redBloodValue,"%d",redInfo->bosshp());
	redBloodValue.append(s_redBloodValue);
	redBloodValue.append("%");
	l_redBloodValue->setString(redBloodValue.c_str());
}
