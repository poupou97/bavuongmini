

#ifndef _UI_FAMILYFIGHTUI_FAMILYFIGHTRESULTUI_H_
#define _UI_FAMILYFIGHTUI_FAMILYFIGHTRESULTUI_H_

#include "../../extensions/UIScene.h"
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../../messageclient/GameProtobuf.h"
#include "../../../messageclient/protobuf/GuildMessage.pb.h"
#include "cocostudio\CCProcessBase.h"
#include "cocostudio\CCArmature.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class FamilyFightResultUI : public UIScene
{
public:
	FamilyFightResultUI();
	~FamilyFightResultUI();

	static FamilyFightResultUI * create(Push1543 fightResult);
	bool init(Push1543 fightResult);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void QuitEvent(Ref *pSender, Widget::TouchEventType type);
	void exitFamilyFight();

	virtual void update(float dt);

	void ArmatureFinishCallback(cocostudio::Armature *armature, cocostudio::MovementEventType movementType, const char *movementID);

protected:
	void createUI();
	void createAnimation();

	void showUIAnimation();
	void startSlowMotion();

	void addLightAnimation();

public:
	void endSlowMotion();

private:
	Layer *layer_anm;

	Text *l_remainTime;
	int m_nRemainTime;

	long long countDown_startTime;

	Push1543 curResultInfo;
};

#endif

