#include "FamilyFightRewardsUI.h"
#include "GameView.h"
#include "../FamilyUI.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../ui/extensions/ShowSystemInfo.h"
#include "FamilyFightData.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../messageclient/element/CGuildBase.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../messageclient/element/CGuildDailyReward.h"
#include "../../../AppMacros.h"
#include "../../../messageclient/element/GoodsInfo.h"
#include "../../vip_ui/VipRewardCellItem.h"
#include "cocostudio\CCSGUIReader.h"


FamilyFightRewardsUI::FamilyFightRewardsUI()
{
}


FamilyFightRewardsUI::~FamilyFightRewardsUI()
{
}

FamilyFightRewardsUI* FamilyFightRewardsUI::create()
{
	auto familyFightRewardsUI = new FamilyFightRewardsUI();
	if (familyFightRewardsUI && familyFightRewardsUI->init())
	{
		familyFightRewardsUI->autorelease();
		return familyFightRewardsUI;
	}
	CC_SAFE_DELETE(familyFightRewardsUI);
	return NULL;
}

bool FamilyFightRewardsUI::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		m_base_layer = Layer::create();
		this->addChild(m_base_layer);	

		//���UI
		auto ppanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/familyRankReward_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->setPosition(Vec2::ZERO);	
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto btn_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnabled( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(FamilyFightRewardsUI::CloseEvent, this));

		auto panel_singCopy =  (Layout*)Helper::seekWidgetByName(ppanel,"Panel_singCopy");
		panel_singCopy->setVisible(false);
		auto panel_familyFight =  (Layout*)Helper::seekWidgetByName(ppanel,"Panel_familyFight");
		panel_familyFight->setVisible(true);

		l_myRanking = (Text*)Helper::seekWidgetByName(ppanel,"Label_FRanking");
		char s_ranking [20];
		sprintf(s_ranking,"%d",FamilyFightData::getFamilyFightRanking());
		l_myRanking->setString(s_ranking);

		m_tableView = TableView::create(this, Size(275,250));
		//m_tableView ->setSelectedEnable(true);   // �֧��ѡ��״̬����ʾ
		//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 25, 1, 1), Vec2(0,0));   // ���þŹ���ͼƬ
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0, 0));
		m_tableView->setPosition(Vec2(49,35));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//m_tableView->setPressedActionEnabled(false);
		m_base_layer->addChild(m_tableView);

		this->setContentSize(ppanel->getContentSize());
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(FamilyFightRewardsUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(FamilyFightRewardsUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(FamilyFightRewardsUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(FamilyFightRewardsUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);


		return true;
	}
	return false;
}

void FamilyFightRewardsUI::onEnter()
{
	UIScene::onEnter();
}

void FamilyFightRewardsUI::onExit()
{
	UIScene::onExit();
}

bool FamilyFightRewardsUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void FamilyFightRewardsUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void FamilyFightRewardsUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void FamilyFightRewardsUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void FamilyFightRewardsUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void FamilyFightRewardsUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void FamilyFightRewardsUI::tableCellTouched( TableView* table, TableViewCell* cell )
{

}

cocos2d::Size FamilyFightRewardsUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(275,64);
}

cocos2d::extension::TableViewCell* FamilyFightRewardsUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto guideReward = FamilyFightData::getInstance()->familyFightRewardList.at(idx);
	Color3B color_green = Color3B(47,93,13);

	// ��ͼ
	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(0, 0));
	sprite_bigFrame->setCapInsets(Rect(9,14,1,1));
	sprite_bigFrame->setPreferredSize(Size(273,62));
	cell->addChild(sprite_bigFrame);

	if (guideReward->endrank() - guideReward->startrank() == 0)
	{
		if (guideReward->startrank() <= 3 )
		{
			std::string icon_path = "res_ui/rank";
			char s_ranking[10];
			sprintf(s_ranking,"%d",guideReward->startrank());
			icon_path.append(s_ranking);
			icon_path.append(".png");
			auto sp_ranking = Sprite::create(icon_path.c_str());
			sp_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
			sp_ranking->setPosition(Vec2(36,32));
			sp_ranking->setScale(0.95f);
			cell->addChild(sp_ranking);
		}
		else
		{
			char s_ranking[10];
			sprintf(s_ranking,"%d",guideReward->startrank());
			auto l_ranking = Label::createWithTTF(s_ranking,APP_FONT_NAME,18);
			l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
			l_ranking->setPosition(Vec2(36,32));
			l_ranking->setColor(color_green);
			cell->addChild(l_ranking);
		}
	}
	else
	{
		std::string str_ranking_des ;
		char s_ranking_start[10];
		sprintf(s_ranking_start,"%d",guideReward->startrank());
		char s_ranking_end[10];
		sprintf(s_ranking_end,"%d",guideReward->endrank());
		str_ranking_des.append(s_ranking_start);
		str_ranking_des.append("-");
		str_ranking_des.append(s_ranking_end);
		auto l_ranking = Label::createWithTTF(str_ranking_des.c_str(),APP_FONT_NAME,18);
		l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		l_ranking->setPosition(Vec2(36,32));
		l_ranking->setColor(color_green);
		cell->addChild(l_ranking);
	}

	if (guideReward->has_goods())
	{
		auto goodInfo = new GoodsInfo();
		goodInfo->CopyFrom(guideReward->goods());
		int num = 0;
		num = guideReward->number();

		auto goodsItem = VipRewardCellItem::create(goodInfo,num);
		goodsItem->setIgnoreAnchorPointForPosition(false);
		goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		goodsItem->setPosition(Vec2(112,31));
		goodsItem->setScale(0.8f);
		cell->addChild(goodsItem);

		delete goodInfo;
	}

	if (FamilyFightData::getInstance()->getFamilyFightRanking() >= guideReward->startrank() && FamilyFightData::getInstance()->getFamilyFightRanking() <= guideReward->endrank())
	{
		if (!FamilyFightData::getInstance()->getFamilyRewardStatus())
		{
			auto btn_layer = Layer::create();
			cell->addChild(btn_layer);
			//btn_layer->setSwallowsTouches(false);

			auto btn_getReward = Button::create();
			btn_getReward->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
			btn_getReward->setScale9Enabled(true);
			btn_getReward->setContentSize(Size(103,42));
			btn_getReward->setCapInsets(Rect(18,9,2,23));
			btn_getReward->setTouchEnabled(true);
			btn_getReward->setPressedActionEnabled(true);
			btn_getReward->setTag(idx);
			btn_getReward->setAnchorPoint(Vec2(.5f,.5f));
			btn_getReward->setPosition(Vec2(215,31));
			btn_getReward->addTouchEventListener(CC_CALLBACK_2(FamilyFightRewardsUI::getRewardEvent, this));
			btn_layer->addChild(btn_getReward);

			auto l_getReward = Label::createWithTTF(StringDataManager::getString("btn_lingqu"), APP_FONT_NAME, 20);
			l_getReward->setAnchorPoint(Vec2(.5f,.5f));
			l_getReward->setPosition(Vec2(0,0));
			btn_getReward->addChild(l_getReward);
		}
		else
		{
			auto sp_di = Sprite::create("res_ui/zhezhao_btn.png");
			sp_di->setAnchorPoint(Vec2(0.5f,0.5f));
			sp_di->setPosition(Vec2(215,31));
			cell->addChild(sp_di);
			auto l_des = Label::createWithTTF(StringDataManager::getString("vip_reward_Geted"),APP_FONT_NAME,18);
			l_des->setAnchorPoint(Vec2(0.5f,0.5f));
			l_des->setPosition(Vec2(sp_di->getContentSize().width/2,sp_di->getContentSize().height/2));
			sp_di->addChild(l_des);
		}
	}

	return cell;
}

ssize_t FamilyFightRewardsUI::numberOfCellsInTableView( TableView *table )
{
	return FamilyFightData::getInstance()->familyFightRewardList.size();
}

void FamilyFightRewardsUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void FamilyFightRewardsUI::getRewardEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1546);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void FamilyFightRewardsUI::ReloadTableViewWithOutChangeOffSet()
{
	Vec2 _s = m_tableView->getContentOffset();
	int _h = m_tableView->getContentSize().height + _s.y;
	m_tableView->reloadData();
	Vec2 temp = Vec2(_s.x,_h - m_tableView->getContentSize().height);
	m_tableView->setContentOffset(temp); 
}

void FamilyFightRewardsUI::RefreshMyRanking()
{
	char s_ranking [20];
	sprintf(s_ranking,"%d",FamilyFightData::getFamilyFightRanking());
	l_myRanking->setString(s_ranking);
}
