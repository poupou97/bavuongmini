
#ifndef _UI_FAMILYFIGHTUI_BATTLESITUATIONUI_H_
#define _UI_FAMILYFIGHTUI_BATTLESITUATIONUI_H_

#include "../../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGuildBattleInfo;

class BattleSituationUI : public UIScene,public TableViewDelegate,public TableViewDataSource
{
public:
	BattleSituationUI();
	~BattleSituationUI();

	static BattleSituationUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	void RefreshOwnData();
	void RefreshOwnDataWithOutChangeOffSet();
	void RefreshEmptyData();
	void RefreshEmptyDataWithOutChangeOffSet();

	void RefreshUI();

private:
	TableView* m_tableView_blue;
	TableView* m_tableView_red;

	int lastSelectCellId;
	int selectCellId;

	Layer * u_layer;

	Label * l_blueName;
	Label * l_redName;
	//ImageView * ImageView_blueBlood;
	//ImageView * ImageView_redBlood;
	//Label * l_blueBloodValue;
	//Label * l_redBloodValue;

	ProgressTimer * pt_blueBlood;
	ProgressTimer * pt_redBlood;
	Label * l_blueBloodValue;
	Label * l_redBloodValue;

public:
	CGuildBattleInfo* blueInfo;
	CGuildBattleInfo* redInfo;
};

#endif

