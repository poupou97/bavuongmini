#include "FamilyFightResultUI.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "GameView.h"
#include "../../../utils/GameUtils.h"
#include "../../../messageclient/protobuf/GuildMessage.pb.h"
#include "../../../utils/StaticDataManager.h"
#include "../../generals_ui/RecuriteActionItem.h"
#include "FamilyFightData.h"
#include "../../../ui/GameUIConstant.h"
#include "../../../gamescene_state/MainAnimationScene.h"
#include "../../../legend_engine/CCLegendAnimation.h"
#include "cocostudio\CCSGUIReader.h"


FamilyFightResultUI::FamilyFightResultUI():
m_nRemainTime(30000),
countDown_startTime(0)
{
}


FamilyFightResultUI::~FamilyFightResultUI()
{
}

FamilyFightResultUI * FamilyFightResultUI::create(Push1543 fightResult)
{
	auto familyFightResultUI = new FamilyFightResultUI();
	if (familyFightResultUI && familyFightResultUI->init(fightResult))
	{
		familyFightResultUI->autorelease();
		return familyFightResultUI;
	}
	CC_SAFE_DELETE(familyFightResultUI);
	return NULL;
}

bool FamilyFightResultUI::init(Push1543 fightResult)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		countDown_startTime = GameUtils::millisecondNow();
		curResultInfo = fightResult;

		layer_anm = Layer::create();
		addChild(layer_anm);

		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		if (!mainscene)
			return false;

		auto action =(ActionInterval *)Sequence::create(
			DelayTime::create(END_BATTLE_BEFORE_SLOWMOTION_DURATION),
			CallFunc::create(CC_CALLBACK_0(FamilyFightResultUI::startSlowMotion,this)),
			CallFunc::create(CC_CALLBACK_0(FamilyFightResultUI::addLightAnimation,this)),
			DelayTime::create(END_BATTLE_SLOWMOTION_DURATION),
			CallFunc::create(CC_CALLBACK_0(FamilyFightResultUI::endSlowMotion,this)),
			//CallFunc::create(this, callfunc_selector(FamilyFightResultUI::showUIAnimation)),
			//DelayTime::create(1.0f),
			CallFunc::create(CC_CALLBACK_0(FamilyFightResultUI::createAnimation,this)),
			NULL);
		this->runAction(action);

		this->setContentSize(Size(652,346));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(FamilyFightResultUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(FamilyFightResultUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(FamilyFightResultUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(FamilyFightResultUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void FamilyFightResultUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FamilyFightResultUI::onExit()
{
	UIScene::onExit();
}

bool FamilyFightResultUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void FamilyFightResultUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void FamilyFightResultUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void FamilyFightResultUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void FamilyFightResultUI::QuitEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1541, (void *)FamilyFightData::getCurrentBattleId());
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void FamilyFightResultUI::update( float dt )
{
	m_nRemainTime -= 1000;
	if (m_nRemainTime <= 0)
	{
		m_nRemainTime = 0;
		exitFamilyFight();
	}

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	l_remainTime->setString(s_remainTime);
}

void FamilyFightResultUI::exitFamilyFight()
{
	//GameMessageProcessor::sharedMsgProcessor()->sendReq(3021);
	this->removeFromParent();
}

void FamilyFightResultUI::createUI()
{
	auto mainPanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/familyAccounts_1.json");
	mainPanel->setAnchorPoint(Vec2(0.f,0.f));
	mainPanel->setPosition(Vec2(0,0));
	m_pLayer->addChild(mainPanel);

	auto btn_exit= (Button *)Helper::seekWidgetByName(mainPanel,"Button_exit");
	CCAssert(btn_exit != NULL, "should not be nil");
	btn_exit->setTouchEnabled(true);
	btn_exit->setPressedActionEnabled(true);
	btn_exit->addTouchEventListener(CC_CALLBACK_2(FamilyFightResultUI::QuitEvent, this));

// 	//��
// 	Label *l_Result = (Text*)Helper::seekWidgetByName(mainPanel,"Label_ResultValue");
// 	switch(curResultInfo.result())
// 	{
// 	case 0:
// 		{
// 			l_Result->setText(StringDataManager::getString("FamilyFightUI_result_lose"));
// 		}
// 		break;
// 	case 1:
// 		{
// 			l_Result->setText(StringDataManager::getString("FamilyFightUI_result_tie"));
// 		}
// 		break;
// 	case 2:
// 		{
// 			l_Result->setText(StringDataManager::getString("FamilyFightUI_result_win"));
// 		}
// 		break;
// 	}
	//a���
	auto l_AName = (Text*)Helper::seekWidgetByName(mainPanel,"Label_aNameValue");
	l_AName->setString(curResultInfo.blueguildname().c_str());
	//aBoss HP
	auto l_ABossHp = (Text*)Helper::seekWidgetByName(mainPanel,"Label_aBossHpValue");
	std::string str_abosshp;
	char s_abossHp[20];
	sprintf(s_abossHp,"%d",curResultInfo.bluebosshppercent());
	str_abosshp.append(s_abossHp);
	str_abosshp.append("%");
	l_ABossHp->setString(str_abosshp.c_str());
	//aBoss HP
	auto ImageView_aHp = (ImageView *)Helper::seekWidgetByName(mainPanel,"ImageView_aHp");
	ImageView_aHp->setTextureRect(Rect(0,0,ImageView_aHp->getContentSize().width*curResultInfo.bluebosshppercent()*1.0f/100.f,ImageView_aHp->getContentSize().height));
	//aƻ�
	auto l_AIntegration = (Text*)Helper::seekWidgetByName(mainPanel,"Label_aIntegralValue");
	char s_aIntegration[20];
	sprintf(s_aIntegration,"%d",curResultInfo.bluefightpoint());
	l_AIntegration->setString(s_aIntegration);
	//�����
	auto l_AIntegrationBrace_left = (Text*)Helper::seekWidgetByName(mainPanel,"Label_aIntegralBrace_left");
	//ű仯�
	auto l_AIntegrationChangeValue = (Text*)Helper::seekWidgetByName(mainPanel,"Label_aIntegralChangeValue");
	l_AIntegrationChangeValue->setString("+3");
	//����
	auto l_AIntegrationBrace_right = (Text*)Helper::seekWidgetByName(mainPanel,"Label_aIntegralBrace_right");
	//ż�ͷ
	auto ImageView_aarrow = (ImageView*)Helper::seekWidgetByName(mainPanel,"ImageView_arrowA");
	int allWidth = l_AIntegration->getContentSize().width+l_AIntegrationBrace_left->getContentSize().width+l_AIntegrationChangeValue->getContentSize().width+l_AIntegrationBrace_right->getContentSize().width+ImageView_aarrow->getContentSize().width;
	int centerWidth = allWidth/2;
	if (l_AIntegration->getContentSize().width >= centerWidth)
	{
		l_AIntegration->setPosition(Vec2(206-(l_AIntegration->getContentSize().width - centerWidth),204));	
	}
	else
	{
		if (l_AIntegration->getContentSize().width+l_AIntegrationBrace_left->getContentSize().width >= centerWidth)
		{
			l_AIntegration->setPosition(Vec2(206-l_AIntegrationBrace_left->getContentSize().width,204));
		}
		else if (l_AIntegration->getContentSize().width+l_AIntegrationBrace_left->getContentSize().width+l_AIntegrationChangeValue->getContentSize().width >= centerWidth)
		{
			l_AIntegration->setPosition(Vec2(206-l_AIntegrationBrace_left->getContentSize().width-l_AIntegrationChangeValue->getContentSize().width,204));
		}
		else
		{
			l_AIntegration->setPosition(Vec2(206,204));
		}
	}
	l_AIntegrationBrace_left->setPosition(Vec2(l_AIntegration->getPosition().x,l_AIntegration->getPosition().y));
	l_AIntegrationChangeValue->setPosition(Vec2(l_AIntegrationBrace_left->getPosition().x+l_AIntegrationBrace_left->getContentSize().width,l_AIntegrationBrace_left->getPosition().y));
	l_AIntegrationBrace_right->setPosition(Vec2(l_AIntegrationChangeValue->getPosition().x+l_AIntegrationChangeValue->getContentSize().width,l_AIntegrationChangeValue->getPosition().y));
	ImageView_aarrow->setPosition(Vec2(l_AIntegrationBrace_right->getPosition().x+l_AIntegrationBrace_right->getContentSize().width,l_AIntegrationBrace_right->getPosition().y));
	//a��
	auto l_AResult = (Text*)Helper::seekWidgetByName(mainPanel,"Label_aResultValue");
	switch(curResultInfo.blueresult())
	{
	case 0:
		{
			l_AResult->setString(StringDataManager::getString("FamilyFightUI_result_lose"));
			l_AResult->setColor(Color3B(255,0,6));
			l_AIntegrationChangeValue->setString("-1");
			l_AIntegrationChangeValue->setColor(Color3B(255,0,6));

			ImageView_aarrow->loadTexture("res_ui/sarrow_down.png");
		}
		break;
	case 1:
		{
			l_AResult->setString(StringDataManager::getString("FamilyFightUI_result_tie"));
			l_AResult->setColor(Color3B(255,246,0));
			l_AIntegrationChangeValue->setString("+1");
			l_AIntegrationChangeValue->setColor(Color3B(255,246,0));

			ImageView_aarrow->loadTexture("res_ui/sarrow_up.png");
		}
		break;
	case 2:
		{
			l_AResult->setString(StringDataManager::getString("FamilyFightUI_result_win"));
			l_AResult->setColor(Color3B(109,255,0));
			l_AIntegrationChangeValue->setString("+3");
			l_AIntegrationChangeValue->setColor(Color3B(109,255,0));

			ImageView_aarrow->loadTexture("res_ui/sarrow_up.png");
		}
		break;
	}
	//b���
	auto l_BName = (Text*)Helper::seekWidgetByName(mainPanel,"Label_bNameValue");
	l_BName->setString(curResultInfo.redguildname().c_str());
	//bBoss HP
	auto l_BBossHp = (Text*)Helper::seekWidgetByName(mainPanel,"Label_bBossHpValue");
	std::string str_bbosshp;
	char s_bbossHp[20];
	sprintf(s_bbossHp,"%d",curResultInfo.redbosshppercent());
	str_bbosshp.append(s_bbossHp);
	str_bbosshp.append("%");
	l_BBossHp->setString(str_bbosshp.c_str());
	//aBoss HP
	auto ImageView_bHp = (ImageView *)Helper::seekWidgetByName(mainPanel,"ImageView_bHp");
	ImageView_bHp->setTextureRect(Rect(0,0,ImageView_bHp->getContentSize().width*curResultInfo.redbosshppercent()*1.0f/100.f,ImageView_bHp->getContentSize().height));
	//bƻ�
	auto l_BIntegration = (Text*)Helper::seekWidgetByName(mainPanel,"Label_bIntegralValue");
	char s_bIntegration[20];
	sprintf(s_bIntegration,"%d",curResultInfo.redfightpoint());
	l_BIntegration->setString(s_bIntegration);
	//�����
	auto l_BIntegrationBrace_left = (Text*)Helper::seekWidgetByName(mainPanel,"Label_bIntegralBrace_left");
	//ű仯�
	auto l_BIntegrationChangeValue = (Text*)Helper::seekWidgetByName(mainPanel,"Label_bIntegralChangeValue");
	l_BIntegrationChangeValue->setString("+3");
	//����
	auto l_BIntegrationBrace_right = (Text*)Helper::seekWidgetByName(mainPanel,"Label_bIntegralBrace_right");
	//ż�ͷ
	auto ImageView_barrow = (ImageView*)Helper::seekWidgetByName(mainPanel,"ImageView_arrowB");
	allWidth = l_BIntegration->getContentSize().width+l_BIntegrationBrace_left->getContentSize().width+l_BIntegrationChangeValue->getContentSize().width+l_BIntegrationBrace_right->getContentSize().width+ImageView_barrow->getContentSize().width;
	centerWidth = allWidth/2;
	if (l_BIntegration->getContentSize().width >= centerWidth)
	{
		l_BIntegration->setPosition(Vec2(512-(l_BIntegration->getContentSize().width - centerWidth),204));	
	}
	else
	{
		if (l_BIntegration->getContentSize().width+l_BIntegrationBrace_left->getContentSize().width >= centerWidth)
		{
			l_BIntegration->setPosition(Vec2(512-l_BIntegrationBrace_left->getContentSize().width,204));
		}
		else if (l_BIntegration->getContentSize().width+l_BIntegrationBrace_left->getContentSize().width+l_BIntegrationChangeValue->getContentSize().width >= centerWidth)
		{
			l_BIntegration->setPosition(Vec2(512-l_BIntegrationBrace_left->getContentSize().width-l_BIntegrationChangeValue->getContentSize().width,204));
		}
		else
		{
			l_BIntegration->setPosition(Vec2(512,204));
		}
	}
	l_BIntegrationBrace_left->setPosition(Vec2(l_BIntegration->getPosition().x,l_BIntegration->getPosition().y));
	l_BIntegrationChangeValue->setPosition(Vec2(l_BIntegrationBrace_left->getPosition().x+l_BIntegrationBrace_left->getContentSize().width,l_BIntegrationBrace_left->getPosition().y));
	l_BIntegrationBrace_right->setPosition(Vec2(l_BIntegrationChangeValue->getPosition().x+l_BIntegrationChangeValue->getContentSize().width,l_BIntegrationChangeValue->getPosition().y));
	ImageView_barrow->setPosition(Vec2(l_BIntegrationBrace_right->getPosition().x+l_BIntegrationBrace_right->getContentSize().width,l_BIntegrationBrace_right->getPosition().y));

	//b��
	auto l_BResult = (Text*)Helper::seekWidgetByName(mainPanel,"Label_bResultValue");
	switch(curResultInfo.redresult())
	{
	case 0:
		{
			l_BResult->setString(StringDataManager::getString("FamilyFightUI_result_lose"));
			l_BResult->setColor(Color3B(255,0,6));
			l_BIntegrationChangeValue->setString("-1");
			l_BIntegrationChangeValue->setColor(Color3B(255,0,6));

			ImageView_barrow->loadTexture("res_ui/sarrow_down.png");
		}
		break;
	case 1:
		{
			l_BResult->setString(StringDataManager::getString("FamilyFightUI_result_tie"));
			l_BResult->setColor(Color3B(255,246,0));
			l_BIntegrationChangeValue->setString("+1");
			l_BIntegrationChangeValue->setColor(Color3B(255,246,0));

			ImageView_barrow->loadTexture("res_ui/sarrow_up.png");
		}
		break;
	case 2:
		{
			l_BResult->setString(StringDataManager::getString("FamilyFightUI_result_win"));
			l_BResult->setColor(Color3B(109,255,0));
			l_BIntegrationChangeValue->setString("+3");
			l_BIntegrationChangeValue->setColor(Color3B(109,255,0));

			ImageView_barrow->loadTexture("res_ui/sarrow_up.png");
		}
		break;
	}

	//�ս����ʱ
	auto l_costTime = (Text*)Helper::seekWidgetByName(mainPanel,"Label_flutterTimeValue");
	l_costTime->setString(RecuriteActionItem::timeFormatToString(curResultInfo.fighttime()/1000).c_str());
	//���齱�
	auto l_rewardExp = (Text*)Helper::seekWidgetByName(mainPanel,"Label_awardEXPValue");
	char s_expValue[20];
	sprintf(s_expValue,"%d",curResultInfo.rewardexp());
	l_rewardExp->setString(s_expValue);
	//���˹��
	auto l_rewardContribution = (Text*)Helper::seekWidgetByName(mainPanel,"Label_awardContributionValue");
	char s_contribution[20];
	sprintf(s_contribution,"%d",curResultInfo.rewardhonor());
	l_rewardContribution->setString(s_contribution);
	//׵���ʱ
	l_remainTime = (Text*)Helper::seekWidgetByName(mainPanel,"Label_restTimeValue");
	m_nRemainTime -= (GameUtils::millisecondNow() - countDown_startTime);
	m_nRemainTime -= 1000;

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	l_remainTime->setString(s_remainTime);

	this->schedule(schedule_selector(FamilyFightResultUI::update),1.0f);
}

void FamilyFightResultUI::showUIAnimation()
{
//	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
//	if (mainscene != NULL)
//	{
// 		if (m_bIsWin)
// 		{
// 			mainscene->addInterfaceAnm("tzcg/tzcg.anm");
// 		}
// 		else
// 		{
// 			mainscene->addInterfaceAnm("tzsb/tzsb.anm");
// 		}
//	}
//	
//	
//	
}
void FamilyFightResultUI::startSlowMotion()
{
	Director::getInstance()->getScheduler()->setTimeScale(0.2f);
}
void FamilyFightResultUI::endSlowMotion()
{
	Director::getInstance()->getScheduler()->setTimeScale(1.0f);
}

void FamilyFightResultUI::addLightAnimation()
{
	Size size = Director::getInstance()->getVisibleSize();
	std::string animFileName = "animation/texiao/renwutexiao/SHANGUANG/shanguang.anm";
	auto la_light = CCLegendAnimation::create(animFileName);
	if (la_light)
	{
		la_light->setPlayLoop(false);
		la_light->setReleaseWhenStop(true);
		la_light->setPlaySpeed(5.0f);
		la_light->setScale(0.85f);
		la_light->setPosition(Vec2(size.width/2,size.height/2));
		GameView::getInstance()->getMainAnimationScene()->addChild(la_light);
	}
}

void FamilyFightResultUI::createAnimation()
{
	Size winSize = Director::getInstance()->getVisibleSize();
	auto mengban = ImageView::create();
	mengban->loadTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enabled(true);
	mengban->setContentSize(winSize);
	mengban->setAnchorPoint(Vec2(.5f,.5f));
	m_pLayer->addChild(mengban);
	mengban->setPosition(Vec2(652/2,346/2+20));

	//��ݶ�����ƴ����������
	cocostudio::Armature *armature ;
	if (curResultInfo.result() == 0)
	{
		//�ӵ����ļ��첽���ض���
		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("res_ui/uiflash/lostLogo/lostLogo0.png","res_ui/uiflash/lostLogo/lostLogo0.plist","res_ui/uiflash/lostLogo/lostLogo.ExportJson");
		armature = cocostudio::Armature::create("lostLogo");
		//����ָ�����
		armature->getAnimation()->playWithIndex(0,-1, cocostudio::ANIMATION_NO_LOOP);
	}
	else if (curResultInfo.result() == 1)
	{
		//�ӵ����ļ��첽���ض���
		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("res_ui/uiflash/drawLogo/drawLogo0.png","res_ui/uiflash/drawLogo/drawLogo0.plist","res_ui/uiflash/drawLogo/drawLogo.ExportJson");
		armature = cocostudio::Armature::create("drawLogo");
		//����ָ�����
		armature->getAnimation()->playWithIndex(0,-1, cocostudio::ANIMATION_NO_LOOP);
	}
	else
	{	
		//�ӵ����ļ��첽���ض���
		cocostudio::ArmatureDataManager::getInstance()->addArmatureFileInfo("res_ui/uiflash/winLogo/winLogo0.png","res_ui/uiflash/winLogo/winLogo0.plist","res_ui/uiflash/winLogo/winLogo.ExportJson");
		armature = cocostudio::Armature::create("winLogo");
		armature->getAnimation()->play("Animation_begin");
		armature->getAnimation()->setMovementEventCallFunc(this,movementEvent_selector(FamilyFightResultUI::ArmatureFinishCallback));  
	}
	//�޸����
	armature->setScale(1.0f);
	//����ö�������λ�
	armature->setPosition(652/2,346/2+35);
	//���ӵ���ǰҳ�
	this->addChild(armature);

	auto sequence = Sequence::create(
		DelayTime::create(2.3f),
		CallFunc::create(CC_CALLBACK_0(FamilyFightResultUI::createUI,this)),
		NULL
		);
	this->runAction(sequence);


	/*******************************************************/
	//�ʤ���ķ�
// 	ImageView * imageView_light = ImageView::create();
// 	imageView_light->loadTexture("res_ui/lightlight.png");
// 	imageView_light->setAnchorPoint(Vec2(0.5f,0.5f));
// 	imageView_light->setPosition(Vec2(652/2,346/2));
// 	layer_anm->addChild(imageView_light);
// 	imageView_light->setVisible(false);
	//��
// 	ImageView * imageView_result = ImageView::create();
// 	if (curResultInfo.result() == 0)
// 	{
// 		imageView_result->loadTexture("res_ui/jiazuzhan/lost.png");
// 	}
// 	else if (curResultInfo.result() == 1)
// 	{
// 		imageView_result->loadTexture("res_ui/jiazuzhan/draw.png");
// 	}
// 	else
// 	{
// 		imageView_result->loadTexture("res_ui/jiazuzhan/win.png");
// 	}
// 	imageView_result->setAnchorPoint(Vec2(0.5f,0.5f));
// 	imageView_result->setPosition(Vec2(652/2,346/2));
// 	imageView_result->setScale(RESULT_UI_RESULTFLAG_BEGINSCALE);
// 	layer_anm->addChild(imageView_result);

// 	Sequence * sequence = Sequence::create(ScaleTo::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME,1.0f,1.0f),
// 		DelayTime::create(RESULT_UI_RESULTFLAG_DELAY_TIME),
// 		MoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,Vec2(652/2,346)),
// 		DelayTime::create(0.05f),
// 		CallFunc::create(this,callfunc_selector(FamilyFightResultUI::createUI)),
// 		NULL);
// 	imageView_result->runAction(sequence);

// 	if (curResultInfo.result() == 2)
// 	{
// 		CCRotateBy * rotateAnm = RotateBy::create(4.0f,360);
// 		RepeatForever * repeatAnm = RepeatForever::create(Sequence::create(rotateAnm,NULL));
// 		Sequence * sequence_light = Sequence::create(
// 			DelayTime::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME),
// 			Show::create(),
// 			DelayTime::create(RESULT_UI_RESULTFLAG_DELAY_TIME),
// 			MoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,Vec2(652/2,346)),
// 			NULL);
// 		imageView_light->runAction(sequence_light);
// 		imageView_light->runAction(repeatAnm);
// 	}
}

void FamilyFightResultUI::ArmatureFinishCallback(cocostudio::Armature *armature, cocostudio::MovementEventType movementType, const char *movementID )
{
	std::string id = movementID;
	if (id.compare("Animation_begin") == 0)
	{
		armature->stopAllActions();
		armature->getAnimation()->play("Animation1_end");
	}
}

