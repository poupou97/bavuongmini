#include "FamilyFightCountDownUI.h"
#include "AppMacros.h"
#include "GameView.h"
#include "../../../utils/GameUtils.h"
#include "../../../utils/StaticDataManager.h"


FamilyFightCountDownUI::FamilyFightCountDownUI():
m_nRemainTime(5000)
{
}


FamilyFightCountDownUI::~FamilyFightCountDownUI()
{
}

FamilyFightCountDownUI * FamilyFightCountDownUI::create()
{
	auto familyFightCountDownUI = new FamilyFightCountDownUI();
	if (familyFightCountDownUI && familyFightCountDownUI->init())
	{
		familyFightCountDownUI->autorelease();
		return familyFightCountDownUI;
	}
	CC_SAFE_DELETE(familyFightCountDownUI);
	return NULL;
}

bool FamilyFightCountDownUI::init()
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();

		auto imageView_Frame = ImageView::create();
		imageView_Frame->loadTexture("res_ui/LV4_dia.png");
		imageView_Frame->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_Frame->setScale9Enabled(true);
		imageView_Frame->setCapInsets(Rect(19,19,1,1));
		imageView_Frame->setContentSize(Size(250,160));
		imageView_Frame->setPosition(Vec2(winsize.width/2,winsize.height*0.8f - 80));
		m_pLayer->addChild(imageView_Frame);

		auto l_des = Text::create(StringDataManager::getString("FamilyFightUI_alive_des"), APP_FONT_NAME, 20);
		l_des->setTextAreaSize(Size(203,0));
		l_des->setTextHorizontalAlignment(TextHAlignment::LEFT);
		l_des->setTextVerticalAlignment(TextVAlignment::CENTER);
		l_des->setAnchorPoint(Vec2(.5f,.5f));
		l_des->setPosition(Vec2(0,-40));
		l_des->setColor(Color3B(243,252,2));
		imageView_Frame->addChild(l_des);

		//����ʱ��ʾ
		imageView_countDown = ImageView::create();
		imageView_countDown->loadTexture("res_ui/font/5.png");
		imageView_countDown->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_countDown->setPosition(Vec2(0,30));
		imageView_Frame->addChild(imageView_countDown);

		float space = m_nRemainTime*1.0f/m_nRemainTime;

		this->schedule(schedule_selector(FamilyFightCountDownUI::update),1.0f);

		this->setContentSize(winsize);
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		return true;
	}
	return false;
}

void FamilyFightCountDownUI::onEnter()
{
	UIScene::onEnter();
}

void FamilyFightCountDownUI::onExit()
{
	UIScene::onExit();
}

bool FamilyFightCountDownUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void FamilyFightCountDownUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void FamilyFightCountDownUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void FamilyFightCountDownUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void FamilyFightCountDownUI::update( float dt )
{
	m_nRemainTime -= 1000;
	if (m_nRemainTime <= 0)
	{
		m_nRemainTime = 0;
		//this->removeFromParent();
	}

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	std::string str_path = "res_ui/font/";
	str_path.append(s_remainTime);
	str_path.append(".png");
	imageView_countDown->loadTexture(str_path.c_str());
}
