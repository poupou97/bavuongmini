#ifndef _UI_FAMILYFIGHTUI_FAMILYFIGHTHISTORYNOTESUI_H_
#define _UI_FAMILYFIGHTUI_FAMILYFIGHTHISTORYNOTESUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../generals_ui/GeneralsListBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGuildFightRecord;

/////////////////////////////////
/**
 * ��ʷ��¼
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.07.17
 */

class familyFightHistoryNotesUI:public GeneralsListBase, public TableViewDataSource, public TableViewDelegate
{
public:
	familyFightHistoryNotesUI(void);
	~familyFightHistoryNotesUI(void);

public:
	static familyFightHistoryNotesUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

private:
	Layer * m_base_layer;																							// ��layer
	TableView * m_tableView;																						// ��tableView

	int lastSelectCellId;
	int selectCellId;

private:
	void initUI();

public:
	// ����ս��ʷ��¼
	std::vector<CGuildFightRecord*> familyFightHistoryList;

	void refreshUI();

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
};

#endif

