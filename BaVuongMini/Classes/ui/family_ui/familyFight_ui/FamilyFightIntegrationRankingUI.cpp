#include "FamilyFightIntegrationRankingUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../../GameAudio.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../AppMacros.h"
#include "GameView.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../utils/GameUtils.h"
#include "../../../messageclient/element/CDeadHistory.h"
#include "../../../messageclient/element/CBoss.h"
#include "../../../utils/GameConfig.h"
#include "FamilyFightData.h"
#include "../../../messageclient/element/CGuildFightRecord.h"
#include "../../../messageclient/element/CGuildFightRanking.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "cocostudio\CCSGUIReader.h"

#define  SelectImageTag 458

FamilyFightIntegrationRankingUI::FamilyFightIntegrationRankingUI(void):
lastSelectCellId(0),
selectCellId(0)
{
	//delete old ranking
	std::vector<CGuildFightRanking*>::iterator iter_ranking;
	for (iter_ranking = familyFightRankingList.begin(); iter_ranking != familyFightRankingList.end(); ++iter_ranking)
	{
		delete *iter_ranking;
	}
	familyFightRankingList.clear();
}


FamilyFightIntegrationRankingUI::~FamilyFightIntegrationRankingUI(void)
{
}

FamilyFightIntegrationRankingUI* FamilyFightIntegrationRankingUI::create()
{
	auto rankingUI = new FamilyFightIntegrationRankingUI();
	if (rankingUI && rankingUI->init())
	{
		rankingUI->autorelease();
		return rankingUI;
	}
	CC_SAFE_DELETE(rankingUI);
	return NULL;
}

bool FamilyFightIntegrationRankingUI::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		everyPageNum = 20;

		m_base_layer = Layer::create();
		this->addChild(m_base_layer);	

		initUI();

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(FamilyFightIntegrationRankingUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(FamilyFightIntegrationRankingUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(FamilyFightIntegrationRankingUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(FamilyFightIntegrationRankingUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void FamilyFightIntegrationRankingUI::onEnter()
{
	UIScene::onEnter();
}

void FamilyFightIntegrationRankingUI::onExit()
{
	UIScene::onExit();
}

void FamilyFightIntegrationRankingUI::initUI()
{
	Size winSize = Director::getInstance()->getVisibleSize();

	auto mengban = ImageView::create();
	mengban->loadTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enabled(true);
	mengban->setContentSize(winSize);
	mengban->setAnchorPoint(Vec2(.5f,.5f));
	m_pLayer->addChild(mengban);

	//���UI
	auto ppanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/familyfamily_1.json");
	ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
	ppanel->setPosition(Vec2::ZERO);	
	ppanel->setTouchEnabled(true);
	m_pLayer->addChild(ppanel);
	mengban->setPosition(Vec2(ppanel->getContentSize().width/2,ppanel->getContentSize().height/2));

	auto btn_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
	btn_close->setTouchEnabled( true );
	btn_close->setPressedActionEnabled(true);
	btn_close->addTouchEventListener(CC_CALLBACK_2(FamilyFightIntegrationRankingUI::CloseEvent, this));

	auto panel_history = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_historyNotes");
	panel_history->setVisible(false);
	auto panel_ranking = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_integrationRanking");
	panel_ranking->setVisible(true);

	lbf_myRanking = (Text*)Helper::seekWidgetByName(ppanel,"Label_rankingValue");
	if (FamilyFightData::getInstance()->getFamilyFightRanking() <= 0)
	{
		lbf_myRanking->setString(StringDataManager::getString("activy_none"));
	}
	else
	{
		char s_myRanking [20];
		sprintf(s_myRanking,"%d",FamilyFightData::getInstance()->getFamilyFightRanking());
		lbf_myRanking->setString(s_myRanking);
	}

	// ��tableView
	m_tableView = TableView::create(this, Size(666,299));
	//m_tableView ->setSelectedEnable(true);   // �֧��ѡ��״̬����ʾ
	//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 25, 1, 1), Vec2(0,0));   // ���þŹ���ͼƬ
	m_tableView->setDirection(TableView::Direction::VERTICAL);
	m_tableView->setAnchorPoint(Vec2(0, 0));
	m_tableView->setPosition(Vec2(75,64));
	m_tableView->setDelegate(this);
	m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//m_tableView->setPressedActionEnabled(false);
	m_base_layer->addChild(m_tableView);

	this->setContentSize(ppanel->getContentSize());
}


bool FamilyFightIntegrationRankingUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return false;
}

void FamilyFightIntegrationRankingUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void FamilyFightIntegrationRankingUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void FamilyFightIntegrationRankingUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void FamilyFightIntegrationRankingUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void FamilyFightIntegrationRankingUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void FamilyFightIntegrationRankingUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	selectCellId = cell->getIdx();
	//ȡ���ϴ�ѡ��״̬�����±���ѡ��״̬
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();
}

cocos2d::Size FamilyFightIntegrationRankingUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(665,39);
}

cocos2d::extension::TableViewCell* FamilyFightIntegrationRankingUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(Size(664,37));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setPosition(Vec2(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	auto rankingInfo = familyFightRankingList.at(idx);
	Color3B color_green = Color3B(47,93,13);

	// ��ͼ
	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(1, 0));
	sprite_bigFrame->setCapInsets(Rect(9,14,1,1));
	sprite_bigFrame->setPreferredSize(Size(664,37));
	cell->addChild(sprite_bigFrame);

	//���
	char s_ranking[10];
	sprintf(s_ranking,"%d",rankingInfo->ranking());
	if (rankingInfo->ranking()>0 && rankingInfo->ranking() <= 3)
	{
		std::string icon_path = "res_ui/rank";
		icon_path.append(s_ranking);
		icon_path.append(".png");
		auto sp_ranking = Sprite::create(icon_path.c_str());
		sp_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		sp_ranking->setPosition(Vec2(43,18));
		sp_ranking->setScale(0.9f);
		cell->addChild(sp_ranking);
	}
	else if(rankingInfo->ranking() <= 10)
	{
		auto l_ranking = Label::createWithBMFont("res_ui/font/ziti_3.fnt",s_ranking);
		l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		l_ranking->setPosition(Vec2(43,18));
		cell->addChild(l_ranking);
	}
	else
	{
		auto l_ranking = Label::createWithTTF(s_ranking,APP_FONT_NAME,16);
		l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		l_ranking->setPosition(Vec2(43,18));
		l_ranking->setColor(color_green);
		cell->addChild(l_ranking);
	}
	
	//�������
	auto l_familyName = Label::createWithTTF(rankingInfo->guild().guildname().c_str(), APP_FONT_NAME, 15);
	//Label * l_familyName = Label::createWithTTF(StringDataManager::getString("PleaseDownloadNewClient"), APP_FONT_NAME, 15);
	l_familyName->setColor(color_green);
	l_familyName->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_familyName->setPosition(Vec2(160, 18));
	cell->addChild(l_familyName);
	//ּ���ȼ�
	char s_lv[10];
	sprintf(s_lv,"%d",rankingInfo->guild().guildlevel());
	//sprintf(s_lv,"%d",10);
	auto l_familyLevel = Label::createWithTTF(s_lv, APP_FONT_NAME, 15);
	l_familyLevel->setColor(color_green);
	l_familyLevel->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_familyLevel->setPosition(Vec2(246, 18));
	cell->addChild(l_familyLevel);
	//�����鳤���
	auto l_familyLeaderName = Label::createWithTTF(rankingInfo->guild().guildleadername().c_str(), APP_FONT_NAME, 15);
	//Label * l_familyLeaderName = Label::createWithTTF(StringDataManager::getString("CheckVersion"), APP_FONT_NAME, 15);
	l_familyLeaderName->setColor(color_green);
	l_familyLeaderName->setAnchorPoint(Vec2(0.0f, 0.5f));
	l_familyLeaderName->setPosition(Vec2(320, 18));
	cell->addChild(l_familyLeaderName);

	int countryId = rankingInfo->guild().guildleadercountry();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		auto countrySp_ = Sprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(Vec2(1.0,0.5f));
		countrySp_->setPosition(Vec2(l_familyLeaderName->getPositionX()-1,l_familyLeaderName->getPositionY()));
		countrySp_->setScale(0.7f);
		cell->addChild(countrySp_);
	}

	//vipInfo
	// vipֿ��ؿ��
	bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
	if (vipEnabled)
	{
		if (rankingInfo->guild().guildleaderviplevel() > 0)
		{
			auto pNode = MainScene::addVipInfoByLevelForNode(rankingInfo->guild().guildleaderviplevel());
			if (pNode)
			{
				cell->addChild(pNode);
				pNode->setPosition(Vec2(l_familyLeaderName->getPosition().x+l_familyLeaderName->getContentSize().width+13,l_familyLeaderName->getPosition().y));
			}
		}
	}

	//Ƽ����
	char s_total[20];
	sprintf(s_total,"%d",rankingInfo->score());
	//sprintf(s_total,"%d",52014);
	auto l_integration = Label::createWithTTF(s_total, APP_FONT_NAME, 15);
	l_integration->setColor(color_green);
	l_integration->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_integration->setPosition(Vec2(478, 18));
	cell->addChild(l_integration);
	//�ʤ�
// 	std::string str_winningpPercentage;
// 	char s_winningPercentage[10];
// 	sprintf(s_winningPercentage,"%d",rankingInfo->winrate());
// 	//sprintf(s_winningPercentage,"%d",85);
// 	str_winningpPercentage.append(s_winningPercentage);
// 	str_winningpPercentage.append("%");
	auto l_winningpPercentage = Label::createWithTTF(rankingInfo->winrate().c_str(), APP_FONT_NAME, 15);
	l_winningpPercentage->setColor(color_green);
	l_winningpPercentage->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_winningpPercentage->setPosition(Vec2(552, 18));
	cell->addChild(l_winningpPercentage);
	//���ʤ���
	char s_winNum[10];
	sprintf(s_winNum,"%d",rankingInfo->winnum());
	//sprintf(s_winNum,"%d",32);
	auto l_winNum = Label::createWithTTF(s_winNum, APP_FONT_NAME, 15);
	l_winNum->setColor(color_green);
	l_winNum->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_winNum->setPosition(Vec2(619, 18));
	cell->addChild(l_winNum);
	
	if (this->selectCellId == idx)
	{
		pHighlightSpr->setVisible(true);
	}

	if (getIsCanReq())
	{
		if(idx == familyFightRankingList.size()-4)
		{
			if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
			{
				//req for next
				this->isReqNewly = false;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1534,(void*)(this->s_mCurPage+1),(void*)everyPageNum);
				setIsCanReq(false);
			}
		}
	}

	return cell;
}

ssize_t FamilyFightIntegrationRankingUI::numberOfCellsInTableView( TableView *table )
{
	return familyFightRankingList.size();
	//return 3;
}

void FamilyFightIntegrationRankingUI::refreshUI()
{
	lastSelectCellId = 0;
	selectCellId = 0;
	m_tableView->reloadData();
}

void FamilyFightIntegrationRankingUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}
