#include "familyFightHistoryNotesUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "SimpleAudioEngine.h"
#include "../../../GameAudio.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../AppMacros.h"
#include "GameView.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../utils/GameUtils.h"
#include "../../../messageclient/element/CDeadHistory.h"
#include "../../../messageclient/element/CBoss.h"
#include "../../../utils/GameConfig.h"
#include "FamilyFightData.h"
#include "../../../messageclient/element/CGuildFightRecord.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "cocostudio\CCSGUIReader.h"

#define  SelectImageTag 458

familyFightHistoryNotesUI::familyFightHistoryNotesUI(void):
lastSelectCellId(0),
selectCellId(0)
{

}


familyFightHistoryNotesUI::~familyFightHistoryNotesUI(void)
{
	//delete old history
	std::vector<CGuildFightRecord*>::iterator iter_history;
	for (iter_history = familyFightHistoryList.begin(); iter_history != familyFightHistoryList.end(); ++iter_history)
	{
		delete *iter_history;
	}
	familyFightHistoryList.clear();
}

familyFightHistoryNotesUI* familyFightHistoryNotesUI::create()
{
	auto historyNotesUI = new familyFightHistoryNotesUI();
	if (historyNotesUI && historyNotesUI->init())
	{
		historyNotesUI->autorelease();
		return historyNotesUI;
	}
	CC_SAFE_DELETE(historyNotesUI);
	return NULL;
}

bool familyFightHistoryNotesUI::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		everyPageNum = 20;

		m_base_layer = Layer::create();
		this->addChild(m_base_layer);	

		initUI();

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(familyFightHistoryNotesUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(familyFightHistoryNotesUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(familyFightHistoryNotesUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(familyFightHistoryNotesUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void familyFightHistoryNotesUI::onEnter()
{
	UIScene::onEnter();
}

void familyFightHistoryNotesUI::onExit()
{
	UIScene::onExit();
}

void familyFightHistoryNotesUI::initUI()
{
	Size winSize = Director::getInstance()->getVisibleSize();

	auto mengban = ImageView::create();
	mengban->loadTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enabled(true);
	mengban->setContentSize(winSize);
	mengban->setAnchorPoint(Vec2(.5f,.5f));
	m_pLayer->addChild(mengban);

	//���UI
	auto ppanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/familyfamily_1.json");
	ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
	ppanel->setPosition(Vec2::ZERO);	
	ppanel->setTouchEnabled(true);
	m_pLayer->addChild(ppanel);
	mengban->setPosition(Vec2(ppanel->getContentSize().width/2,ppanel->getContentSize().height/2));

	auto btn_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
	btn_close->setTouchEnabled( true );
	btn_close->setPressedActionEnabled(true);
	btn_close->addTouchEventListener(CC_CALLBACK_2(familyFightHistoryNotesUI::CloseEvent, this));

	auto panel_history = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_historyNotes");
	panel_history->setVisible(true);
	auto panel_ranking = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_integrationRanking");
	panel_ranking->setVisible(false);

	// ��tableView
	m_tableView = TableView::create(this, Size(665,324));
	//m_tableView ->setSelectedEnable(true);   // �֧��ѡ��״̬����ʾ
	//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 25, 1, 1), Vec2(0,0));   // ���þŹ���ͼƬ
	m_tableView->setDirection(TableView::Direction::VERTICAL);
	m_tableView->setAnchorPoint(Vec2(0, 0));
	m_tableView->setPosition(Vec2(77,39));
	m_tableView->setDelegate(this);
	m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//m_tableView->setPressedActionEnabled(false);
	m_base_layer->addChild(m_tableView);

	this->setContentSize(ppanel->getContentSize());
}


bool familyFightHistoryNotesUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return false;
}

void familyFightHistoryNotesUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void familyFightHistoryNotesUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void familyFightHistoryNotesUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void familyFightHistoryNotesUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void familyFightHistoryNotesUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void familyFightHistoryNotesUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	selectCellId = cell->getIdx();
	//ȡ���ϴ�ѡ��״̬�����±���ѡ��״̬
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();
}

cocos2d::Size familyFightHistoryNotesUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(665,39);
}

cocos2d::extension::TableViewCell* familyFightHistoryNotesUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(Size(662,37));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setPosition(Vec2(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	auto record = familyFightHistoryList.at(idx);
	Color3B color_green = Color3B(47,93,13);
	
	// ��ͼ
	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(1, 0));
	sprite_bigFrame->setCapInsets(Rect(9,14,1,1));
	sprite_bigFrame->setPreferredSize(Size(662,37));
	cell->addChild(sprite_bigFrame);

	//ս����
	std::string str_result_path;
	if (record->scorechanged() == 3)
	{
		str_result_path = "res_ui/jiazuzhan/wins.png";
	}
	else if (record->scorechanged() == 1)
	{
		str_result_path = "res_ui/jiazuzhan/draws.png";
	}
	else if (record->scorechanged() == -1)
	{
		str_result_path = "res_ui/jiazuzhan/losts.png";
	}
	else
	{
		str_result_path = "";
	}

	if (str_result_path != "")
	{
		auto sp_result = Sprite::create(str_result_path.c_str());
		sp_result->setAnchorPoint(Vec2(0.5f, 0.5f));
		sp_result->setPosition(Vec2(35, 18));
		cell->addChild(sp_result);
	}

	//��
	std::string str_integration ;
	char s_total[20];
	sprintf(s_total,"%d",record->scoretotal());
	//sprintf(s_total,"%d",200);
	str_integration.append(s_total);
	if (record->scorechanged() != 0)
	{
		str_integration.append("(");
		if (record->scorechanged() > 0)
		{
			str_integration.append("+");
		}
// 		else
// 		{
// 			str_integration.append("-");
// 		}
		char s_change[10];
		sprintf(s_change,"%d",record->scorechanged());
		//sprintf(s_change,"%d",+52);
		str_integration.append(s_change);
		str_integration.append(")");
	}
	
	auto l_integration = Label::createWithTTF(str_integration.c_str(), APP_FONT_NAME, 15);
	l_integration->setColor(color_green);
	l_integration->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_integration->setPosition(Vec2(125, 18));
	cell->addChild(l_integration);

	//ֶԷ��������
	auto l_targetFamilyName = Label::createWithTTF(record->enemyguild().guildname().c_str(), APP_FONT_NAME, 15);
	//Label * l_targetFamilyName = Label::createWithTTF(StringDataManager::getString("PleaseDownloadNewClient"), APP_FONT_NAME, 15);
	l_targetFamilyName->setColor(color_green);
	l_targetFamilyName->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_targetFamilyName->setPosition(Vec2(240, 18));
	cell->addChild(l_targetFamilyName);
	//ֶԷ�����ȼ�
	char s_lv[10];
	sprintf(s_lv,"%d",record->enemyguild().guildlevel());
	//sprintf(s_lv,"%d",5);
	auto l_targetFamilyLevel = Label::createWithTTF(s_lv, APP_FONT_NAME, 15);
	l_targetFamilyLevel->setColor(color_green);
	l_targetFamilyLevel->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_targetFamilyLevel->setPosition(Vec2(337, 18));
	cell->addChild(l_targetFamilyLevel);
	//�Է������鳤���
	auto l_targetFamilyLeaderName = Label::createWithTTF(record->enemyguild().guildleadername().c_str(), APP_FONT_NAME, 15);
	//Label * l_targetFamilyLeaderName = Label::createWithTTF(StringDataManager::getString("CheckVersion"), APP_FONT_NAME, 15);
	l_targetFamilyLeaderName->setColor(color_green);
	l_targetFamilyLeaderName->setAnchorPoint(Vec2(0.0f, 0.5f));
	l_targetFamilyLeaderName->setPosition(Vec2(410, 18));
	cell->addChild(l_targetFamilyLeaderName);

	int countryId = record->enemyguild().guildleadercountry();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		auto countrySp_ = Sprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(Vec2(1.0,0.5f));
		countrySp_->setPosition(Vec2(l_targetFamilyLeaderName->getPositionX()-1,l_targetFamilyLeaderName->getPositionY()));
		countrySp_->setScale(0.7f);
		cell->addChild(countrySp_);
	}

	//vipInfo
	// vipֿ��ؿ��
	bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
	if (vipEnabled)
	{
		if (record->enemyguild().guildleaderviplevel() > 0)
		{
			Node *pNode = MainScene::addVipInfoByLevelForNode(record->enemyguild().guildleaderviplevel());
			if (pNode)
			{
				cell->addChild(pNode);
				pNode->setPosition(Vec2(l_targetFamilyLeaderName->getPosition().x+l_targetFamilyLeaderName->getContentSize().width+13,l_targetFamilyLeaderName->getPosition().y));
			}
		}
	}

	// �ʱ�
	auto l_date = Label::createWithTTF(record->fightdate().c_str(), APP_FONT_NAME, 15, Size(200, 0), TextHAlignment::LEFT);
	//Label * l_date = Label::createWithTTF("2014/05/06", APP_FONT_NAME, 15, Size(200, 0), TextHAlignment::LEFT);
	l_date->setColor(color_green);
	l_date->setAnchorPoint(Vec2(0.5f, 0.5f));
	l_date->setPosition(Vec2(660, 18));
	cell->addChild(l_date);

	if (this->selectCellId == idx)
	{
		pHighlightSpr->setVisible(true);
	}


	if (getIsCanReq())
	{
		if(idx == familyFightHistoryList.size()-4)
		{
			if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
			{
				//req for next
				this->isReqNewly = false;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1533,(void*)(this->s_mCurPage+1),(void*)everyPageNum);
				setIsCanReq(false);
			}
		}
	}

	return cell;
}

ssize_t familyFightHistoryNotesUI::numberOfCellsInTableView( TableView *table )
{
	return familyFightHistoryList.size();
}

void familyFightHistoryNotesUI::refreshUI()
{
	lastSelectCellId = 0;
	selectCellId = 0;
	m_tableView->reloadData();
}

void familyFightHistoryNotesUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}
