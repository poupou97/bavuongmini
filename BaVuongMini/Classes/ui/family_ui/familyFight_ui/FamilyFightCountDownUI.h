

#ifndef _UI_FAMILYFIGHTUI_FAMILYFIGHTCOUNTDOWNUI_H_
#define _UI_FAMILYFIGHTUI_FAMILYFIGHTCOUNTDOWNUI_H_

#include "../../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class FamilyFightCountDownUI : public UIScene
{
public:
	FamilyFightCountDownUI();
	~FamilyFightCountDownUI();

	static FamilyFightCountDownUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void update(float dt);

private:
	int m_nRemainTime;
	//Label *l_countDown;
	ImageView * imageView_countDown;
};

#endif

