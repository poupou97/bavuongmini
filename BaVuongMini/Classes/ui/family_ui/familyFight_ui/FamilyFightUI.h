
#ifndef _FAMILYFIGHTUI_FAMILYFIGHTUI_H_
#define _FAMILYFIGHTUI_FAMILYFIGHTUI_H_

#include "../../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class FamilyFightUI : public UIScene
{
public:
	FamilyFightUI();
	~FamilyFightUI();

	static FamilyFightUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void update(float dt);

	void RecordEvent(Ref *pSender, Widget::TouchEventType type);
	void ScoreBoardEvent(Ref *pSender, Widget::TouchEventType type);
	void SignUpEvent(Ref *pSender, Widget::TouchEventType type);
	void RuleEvent(Ref *pSender, Widget::TouchEventType type);
	void GetRewardEvent(Ref *pSender, Widget::TouchEventType type);

	//ˢ�±���ť��״̬
	void RefreshBtnSignUpStatus();
	//ˢ���ҵļ�������ͻ�
	void RefreshFamilyRankingAndScore();
	//�ˢ�±���ʱ�����һ��ʱ����Ϣ
	void RefreshTimeInfo();

	void AddParticleForBtnGetReward();
	void RemoveParticleForBtnGetReward();

private:
	Layer * m_base_layer;									// ��layer
	//�ť
	Button * btn_signUp;
	ImageView * ImageView_btn_signUpState;
	Button * btn_getReward;
	//�ı�
	ImageView * ImageView_signUp;
	ImageView * ImageView_img_signUpState;
	//���뵱ǰս��ʣ��ʱ�
	Text * l_entryLeftTime;
	//��Ҳ���ʾʱ����Ϣ
	ImageView * imageView_timeInfo;
	Text * l_time_cur;
	Text * l_time_curValue;
	Text * l_time_next;
	Text * l_time_nextValue;
	Text * l_canceledDes;

	Text * l_familyName;
	Text * l_familyRanking;
	Text * l_familyScore;

	void initUI();
};

#endif

