#include "FamilyFightUI.h"
#include "GameView.h"
#include "../FamilyUI.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../ui/extensions/ShowSystemInfo.h"
#include "FamilyFightData.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../messageclient/element/CGuildBase.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../messageclient/protobuf/ModelMessage.pb.h"
#include "FamilyFightRewardsUI.h"
#include "../../../messageclient/protobuf/GuildMessage.pb.h"
#include "familyFightHistoryNotesUI.h"
#include "FamilyFightIntegrationRankingUI.h"
#include "../../../ui/generals_ui/RecuriteActionItem.h"
#include "../../../legend_script/CCTutorialParticle.h"
#include "../../../messageclient/element/CGuildDailyReward.h"

#define ICON_PATH_BAOMING "res_ui/jiazuzhan/bangming.png"
#define ICON_PATH_QUXIAOBAOMING "res_ui/jiazuzhan/quxiaobaoming.png"
#define ICON_PATH_WEIBAOMING "res_ui/jiazuzhan/weibaoming.png"
#define ICON_PATH_YIBAOMING "res_ui/jiazuzhan/yibaoming.png"
#define ICON_PATH_ZHANQIANZHUNBEI "res_ui/jiazuzhan/zhanqianzhunbei.png"
#define ICON_PATH_JINRUZHANCHANG "res_ui/jiazuzhan/jinruzhanchang.png"
#define ICON_PATH_XIUZHENGQI "res_ui/jiazuzhan/xiuzhengqi.png"
#define ICON_PATH_ZHANDOUQUXIAO "res_ui/jiazuzhan/zhandouquxiao.png"
#define ICON_PATH_BENCHANGJIESHU "res_ui/jiazuzhan/benchangjieshu.png"


FamilyFightUI::FamilyFightUI()
{
}


FamilyFightUI::~FamilyFightUI()
{
}

FamilyFightUI* FamilyFightUI::create()
{
	auto familyFightUI = new FamilyFightUI();
	if (familyFightUI && familyFightUI->init())
	{
		familyFightUI->autorelease();
		return familyFightUI;
	}
	CC_SAFE_DELETE(familyFightUI);
	return NULL;
}

bool FamilyFightUI::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		m_base_layer = Layer::create();
		this->addChild(m_base_layer);	

		initUI();

		this->schedule( schedule_selector(FamilyFightUI::update), 1.0f); 

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(FamilyFightUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(FamilyFightUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(FamilyFightUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(FamilyFightUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void FamilyFightUI::onEnter()
{
	UIScene::onEnter();
}

void FamilyFightUI::onExit()
{
	UIScene::onExit();
}

bool FamilyFightUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return false;
}

void FamilyFightUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void FamilyFightUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void FamilyFightUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void FamilyFightUI::initUI()
{
	Size winSize = Director::getInstance()->getVisibleSize();

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto familyUI =  (FamilyUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	if (!familyUI)
		return;
 
	auto s_pl_familyFight= (Layout*)Helper::seekWidgetByName(familyUI->mainPanel,"Panel_familyFight");

	//��¼
	auto btn_record = (Button*)Helper::seekWidgetByName(s_pl_familyFight,"Button_record");
	btn_record->setTouchEnabled(true);
	btn_record->setPressedActionEnabled(true);
	btn_record->addTouchEventListener(CC_CALLBACK_2(FamilyFightUI::RecordEvent, this));
	//��ְ
	auto btn_scoreBoard = (Button*)Helper::seekWidgetByName(s_pl_familyFight,"Button_leagoe");
	btn_scoreBoard->setTouchEnabled(true);
	btn_scoreBoard->setPressedActionEnabled(true);
	btn_scoreBoard->addTouchEventListener(CC_CALLBACK_2(FamilyFightUI::ScoreBoardEvent, this));
	//��
	btn_signUp = (Button*)Helper::seekWidgetByName(s_pl_familyFight,"Button_signUp");
	btn_signUp->setTouchEnabled(true);
	btn_signUp->setPressedActionEnabled(true);
	btn_signUp->addTouchEventListener(CC_CALLBACK_2(FamilyFightUI::SignUpEvent, this));
	//l_signUp = (Text*)Helper::seekWidgetByName(s_pl_familyFight,"Label_signUp");
	ImageView_btn_signUpState = (ImageView*)Helper::seekWidgetByName(s_pl_familyFight,"ImageView_btn_curState");

	ImageView_signUp = (ImageView*)Helper::seekWidgetByName(s_pl_familyFight,"ImageView_signUpFrame");
	ImageView_img_signUpState = (ImageView*)Helper::seekWidgetByName(s_pl_familyFight,"ImageView_img_curState");

	l_entryLeftTime = (Text*)Helper::seekWidgetByName(s_pl_familyFight,"Label_entryLeftTimeValue");

	btn_signUp->setVisible(false);
	ImageView_signUp->setVisible(false);

	//���
	auto btn_rule = (Button*)Helper::seekWidgetByName(s_pl_familyFight,"Button_rule");
	btn_rule->setTouchEnabled(true);
	btn_rule->setPressedActionEnabled(true);
	btn_rule->addTouchEventListener(CC_CALLBACK_2(FamilyFightUI::RuleEvent, this));
	//��콱
	btn_getReward = (Button*)Helper::seekWidgetByName(s_pl_familyFight,"Button_accept");
	btn_getReward->setTouchEnabled(true);
	btn_getReward->setPressedActionEnabled(true);
	btn_getReward->addTouchEventListener(CC_CALLBACK_2(FamilyFightUI::GetRewardEvent,this));
	//
	//�Ƿ��н��
	bool isExistReward = false;
	if (FamilyFightData::getInstance()->familyFightRewardList.size()>0)
	{
		auto guideReward = FamilyFightData::getInstance()->familyFightRewardList.at(FamilyFightData::getInstance()->familyFightRewardList.size()-1);
		if (FamilyFightData::getInstance()->getFamilyFightRanking()>0 && FamilyFightData::getInstance()->getFamilyFightRanking() <= guideReward->endrank())
		{
			isExistReward = true;
		}
	}
	if(isExistReward && (!FamilyFightData::getInstance()->getFamilyRewardStatus()))
	{
		AddParticleForBtnGetReward();
	}
	
	l_familyName = (Text*)Helper::seekWidgetByName(s_pl_familyFight,"Label_familyName");
	l_familyName->setString(GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionname().c_str());

	l_familyRanking = (Text*)Helper::seekWidgetByName(s_pl_familyFight,"Label_oldFamilyRankingValue");
	l_familyScore = (Text*)Helper::seekWidgetByName(s_pl_familyFight,"Label_integralValue");
	
	imageView_timeInfo = (ImageView*)Helper::seekWidgetByName(s_pl_familyFight,"ImageView_timeInfo");
	l_time_cur = (Text*)Helper::seekWidgetByName(s_pl_familyFight,"Label_cur");
	l_time_curValue = (Text*)Helper::seekWidgetByName(s_pl_familyFight,"Label_curTimeValue");
	l_time_next = (Text*)Helper::seekWidgetByName(s_pl_familyFight,"Label_next");
	l_time_nextValue = (Text*)Helper::seekWidgetByName(s_pl_familyFight,"Label_nextTimeValue");
	l_canceledDes = (Text*)Helper::seekWidgetByName(s_pl_familyFight,"Label_canceledDes");
	l_canceledDes->setTextAreaSize(Size(145,0));
	l_canceledDes->setTextHorizontalAlignment(TextHAlignment::LEFT);
	l_canceledDes->setTextVerticalAlignment(TextVAlignment::CENTER);
}

void FamilyFightUI::RecordEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto historyNotesUI = (familyFightHistoryNotesUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightHistoryNotesUI);
		if (!historyNotesUI)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			historyNotesUI = familyFightHistoryNotesUI::create();
			historyNotesUI->setIgnoreAnchorPointForPosition(false);
			historyNotesUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			historyNotesUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			historyNotesUI->setTag(kTagFamilyFightHistoryNotesUI);
			GameView::getInstance()->getMainUIScene()->addChild(historyNotesUI);

			historyNotesUI->isReqNewly = true;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1533, (void*)0, (void*)historyNotesUI->everyPageNum);
		}

	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyFightUI::ScoreBoardEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		auto rankingUI = (FamilyFightIntegrationRankingUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightIntegrationRankingUI);
		if (!rankingUI)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			rankingUI = FamilyFightIntegrationRankingUI::create();
			rankingUI->setIgnoreAnchorPointForPosition(false);
			rankingUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			rankingUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			rankingUI->setTag(kTagFamilyFightIntegrationRankingUI);
			GameView::getInstance()->getMainUIScene()->addChild(rankingUI);

			rankingUI->isReqNewly = true;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1534, (void*)0, (void*)rankingUI->everyPageNum);
		}

	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyFightUI::SignUpEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		switch (FamilyFightData::getFamilyFightTimeClass())
		{
		case FamilyFightData::timeClass_none:
		{
		}
		break;
		case FamilyFightData::timeClass_signUp:
		{
			if (FamilyFightData::isRegisted)   //��ѱ��
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_master ||
					GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_second_master)  //��峤���
				{
					//�ȡ���
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1532);
				}
				else
				{
					//��ѱ��
				}
			}
			else                                           //�δ���
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_master ||
					GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_second_master)  //��峤���
				{
					//ϱ��
					int needDevote = 0;
					std::map<int, std::string>::const_iterator cIter;
					cIter = VipValueConfig::s_vipValue.find(8);
					if (cIter == VipValueConfig::s_vipValue.end()) // �û�ҵ�����ָ�END��  
					{

					}
					else
					{
						std::string value = VipValueConfig::s_vipValue[8];
						needDevote = atoi(value.c_str());
					}

					auto familyUI = (FamilyUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
					if (familyUI)
					{
						if (familyUI->curFamilyContrbution >= needDevote)
						{
							GameMessageProcessor::sharedMsgProcessor()->sendReq(1531);
						}
						else
						{
							char des[500];
							const char *strings = StringDataManager::getString("FamilyFightUI_notEnoughHonor");
							sprintf(des, strings, needDevote, familyUI->curFamilyContrbution);
							GameView::getInstance()->showPopupWindow(des, 2, this, NULL, NULL);
						}
					}
				}
				else
				{
					//�δ���
				}
			}
		}
		break;
		case FamilyFightData::timeClass_fight:
		{
			if (FamilyFightData::isRegisted)   //��ѱ��
			{
				if (FamilyFightData::isCancel)  //�ƥ��ʧ�
				{
					//���ʾ��ս��ȡ�
				}
				else
				{
					if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_none)
					{
						//�սǰ׼��
					}
					else if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_in_sequence)
					{
						//սǰ׼��
					}
					else if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_startFight)
					{
						//����ս��
						GameMessageProcessor::sharedMsgProcessor()->sendReq(1540, (void *)FamilyFightData::getCurrentBattleId());
					}
					else if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_endFight)
					{
						//�������
					}
				}
			}
			else
			{
				//�δ���
			}
		}
		break;
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void FamilyFightUI::RuleEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//����ս�������
		Size winSize = Director::getInstance()->getVisibleSize();
		std::map<int, std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(7);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end()) // �û�ҵ�����ָ�END��  
		{
			return;
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			{
				auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[7].c_str(), 1.5f);
				GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				showSystemInfo->setIgnoreAnchorPointForPosition(false);
				showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				showSystemInfo->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyFightUI::GetRewardEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightRewardUI) == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			auto familyFightRewardUI = FamilyFightRewardsUI::create();
			familyFightRewardUI->setIgnoreAnchorPointForPosition(false);
			familyFightRewardUI->setAnchorPoint(Vec2(.5f, .5f));
			familyFightRewardUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			familyFightRewardUI->setTag(kTagFamilyFightRewardUI);
			GameView::getInstance()->getMainUIScene()->addChild(familyFightRewardUI);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void FamilyFightUI::RefreshBtnSignUpStatus()
{
	switch(FamilyFightData::getFamilyFightTimeClass())
	{
	case FamilyFightData::timeClass_none:
		{
			//���ʾ�������ڡ�
			btn_signUp->setVisible(false);
			ImageView_signUp->setVisible(true);
			ImageView_img_signUpState->loadTexture(ICON_PATH_XIUZHENGQI);
		}
		break;
	case FamilyFightData::timeClass_signUp:
		{
			if(FamilyFightData::isRegisted)   //�ѱ��
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_master  ||
					GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_second_master)  //��峤���
				{
					//���ʾ��ȡ���
					btn_signUp->setVisible(true);
					ImageView_btn_signUpState->loadTexture(ICON_PATH_QUXIAOBAOMING);
					ImageView_signUp->setVisible(false);
				}
				else
				{
					//���ʾ���ѱ��
					btn_signUp->setVisible(false);
					ImageView_signUp->setVisible(true);
					ImageView_img_signUpState->loadTexture(ICON_PATH_YIBAOMING);
				}
			}
			else                                           //�δ���
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_master  ||
					GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionposition() == CGuildBase::position_second_master)  //��峤���
				{
					//���ʾ�����
					btn_signUp->setVisible(true);
					ImageView_btn_signUpState->loadTexture(ICON_PATH_BAOMING);
					ImageView_signUp->setVisible(false);
				}
				else
				{
					//���ʾ��δ���
					btn_signUp->setVisible(false);
					ImageView_signUp->setVisible(true);
					ImageView_img_signUpState->loadTexture(ICON_PATH_WEIBAOMING);
				}
			}
		}
		break;
	case FamilyFightData::timeClass_fight:
		{
			if(FamilyFightData::isRegisted)   //��ѱ��
			{
				if (FamilyFightData::isCancel)  //�ƥ��ʧ�
				{
					//���ʾ��ս��ȡ�
					btn_signUp->setVisible(false);
					ImageView_signUp->setVisible(true);
					ImageView_img_signUpState->loadTexture(ICON_PATH_ZHANDOUQUXIAO);
				}
				else
				{
					if(FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_none)
					{
						//���ʾ��սǰ׼����
						btn_signUp->setVisible(false);
						ImageView_signUp->setVisible(true);
						ImageView_img_signUpState->loadTexture(ICON_PATH_ZHANQIANZHUNBEI);
					}
					else if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_in_sequence)
					{
						//��ʾ��սǰ׼����
						btn_signUp->setVisible(false);
						ImageView_signUp->setVisible(true);
						ImageView_img_signUpState->loadTexture(ICON_PATH_ZHANQIANZHUNBEI);
					}
					else if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_startFight)
					{
						//��ʾ������ս����
						btn_signUp->setVisible(true);
						ImageView_btn_signUpState->loadTexture(ICON_PATH_JINRUZHANCHANG);
						ImageView_signUp->setVisible(false);	
					}
					else if (FamilyFightData::getFamilyFightFightClasss() == FamilyFightData::fightClass_endFight)
					{
						//��ʾ���������
						btn_signUp->setVisible(false);
						ImageView_signUp->setVisible(true);
						ImageView_img_signUpState->loadTexture(ICON_PATH_BENCHANGJIESHU);
					}
				}
			}
			else
			{
				//���ʾ��δ���
				btn_signUp->setVisible(false);
				ImageView_signUp->setVisible(true);
				ImageView_img_signUpState->loadTexture(ICON_PATH_WEIBAOMING);
			}
		}
		break;
	}

}

void FamilyFightUI::RefreshFamilyRankingAndScore()
{
	char s_myRanking [20];
	sprintf(s_myRanking,"%d",FamilyFightData::getFamilyFightRanking());
	l_familyRanking->setString(s_myRanking);

	char s_myScore [20];
	sprintf(s_myScore,"%d",FamilyFightData::getFamilyFightScore());
	l_familyScore->setString(s_myScore);
}

void FamilyFightUI::RefreshTimeInfo()
{
	if (FamilyFightData::getFamilyFightTimeClass() != FamilyFightData::timeClass_fight)
	{
		imageView_timeInfo->setVisible(false);
	}
	else
	{
		imageView_timeInfo->setVisible(true);

		if (FamilyFightData::isCancel)              //�ƥ��ʧ�
		{
			l_time_cur->setVisible(false);
			l_time_curValue->setVisible(false);
			l_time_next->setVisible(false);
			l_time_nextValue->setVisible(false);
			l_canceledDes->setVisible(true);
			l_canceledDes->setString(StringDataManager::getString("FamilyFightUI_cancel_today"));
		}
		else
		{
			l_time_cur->setVisible(true);
			l_time_curValue->setVisible(true);
			l_time_curValue->setString(FamilyFightData::getCurTime().c_str());
			l_time_next->setVisible(true);
			l_time_nextValue->setVisible(true);
			l_time_nextValue->setString(FamilyFightData::getNextTime().c_str());

			l_canceledDes->setVisible(false);
		}

	}

	l_time_curValue->setString(FamilyFightData::getCurTime().c_str());
	l_time_nextValue->setString(FamilyFightData::getNextTime().c_str());
	l_canceledDes->setString(StringDataManager::getString("FamilyFightUI_cancel_today"));
}

void FamilyFightUI::update( float dt )
{
	//ܱ�������ʣ��ʱ�
	if (FamilyFightData::getFamilyFightTimeClass() != FamilyFightData::timeClass_fight)
	{
		l_entryLeftTime->setVisible(false);
	}
	else
	{
		if (FamilyFightData::getEntryLeftTime() > 0)
		{
			long long remainTime = FamilyFightData::getEntryLeftTime();
			remainTime -= 1000;
			FamilyFightData::setEntryLeftTime(remainTime);
		}

		if (FamilyFightData::getEntryLeftTime() <= 0)
		{
			l_entryLeftTime->setVisible(false);
		}
		else
		{
			l_entryLeftTime->setVisible(true);
			l_entryLeftTime->setString(RecuriteActionItem::timeFormatToString(FamilyFightData::getEntryLeftTime()/1000).c_str());
		}
	}
}

void FamilyFightUI::AddParticleForBtnGetReward()
{
	auto tutorialParticle = CCTutorialParticle::create("tuowei0.plist",38,57);
	tutorialParticle->setPosition(Vec2(btn_getReward->getPosition().x,btn_getReward->getPosition().y-btn_getReward->getContentSize().height/2));
	tutorialParticle->setTag(CCTUTORIALPARTICLETAG);
	m_base_layer->addChild(tutorialParticle);
}

void FamilyFightUI::RemoveParticleForBtnGetReward()
{
	if (m_base_layer->getChildByTag(CCTUTORIALPARTICLETAG))
	{
		m_base_layer->getChildByTag(CCTUTORIALPARTICLETAG)->removeFromParent();
	}
}
