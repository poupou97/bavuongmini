#include "FamilyFightData.h"
#include "../../../messageclient/element/CGuildDailyReward.h"
#include "../../../messageclient/element/CMapInfo.h"
#include "../../../GameView.h"
#include "../../../messageclient/protobuf/PlayerMessage.pb.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../gamescene_state/MainScene.h"
#include "FamilyFightCountDownUI.h"

FamilyFightData * FamilyFightData::s_familyFightData = NULL;
int FamilyFightData::s_status = FamilyFightData::status_none;
int FamilyFightData::s_currentBattleId = 0;
int FamilyFightData::s_familyFightFightClasss = -1;
int FamilyFightData::s_familyFightTimeClass = -1;
bool FamilyFightData::isRegisted = false;
bool FamilyFightData::isCancel = false;
bool FamilyFightData::s_familyRewardStatus = false;
int FamilyFightData::s_familyFightRanking = -1;
int FamilyFightData::s_familyFightScore = -1;
std::string FamilyFightData::s_sCurTime = "";
std::string FamilyFightData::s_sNextTime = "";
long long FamilyFightData::s_nRemainTime = 0;
long long FamilyFightData::s_nEntryLeftTime = 0;
FamilyFightData::BossInfo FamilyFightData::s_Boss_enemy_pos;
FamilyFightData::BossInfo FamilyFightData::s_Boss_Common_pos;
FamilyFightData::BossInfo FamilyFightData::s_Boss_Owner_pos;
FamilyFightData::BossHpInfo FamilyFightData::s_Boss_Owner_hpInfo;
FamilyFightData::BossHpInfo FamilyFightData::s_Boss_Common_hpInfo;
FamilyFightData::BossHpInfo FamilyFightData::s_Boss_enemy_hpInfo;

FamilyFightData::FamilyFightData(void)
{
}


FamilyFightData::~FamilyFightData(void)
{
}

FamilyFightData * FamilyFightData::getInstance()
{
	if (s_familyFightData == NULL)
	{
		s_familyFightData = new FamilyFightData();
	}

	return s_familyFightData;
}

void FamilyFightData::update()
{
	if (!GameView::getInstance())
		return;

	//����󵯳��ʱ���
	if (GameView::getInstance()->getMapInfo()->maptype() != com::future::threekingdoms::server::transport::protocol::guildFight)
		return;

	if (!GameView::getInstance()->myplayer->isDead())
	{
		auto countDownUI = (FamilyFightCountDownUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightCountDownUI);
		if (countDownUI)
			countDownUI->removeFromParent();
		
		return;
	}

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto countDownUI = (FamilyFightCountDownUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightCountDownUI);
	if (countDownUI)
		return;

	Size winSize=Director::getInstance()->getVisibleSize();
	countDownUI = FamilyFightCountDownUI::create();
	countDownUI->setIgnoreAnchorPointForPosition(false);
	countDownUI->setAnchorPoint(Vec2(0.5f,0.5f));
	countDownUI->setPosition(Vec2(winSize.width/2,winSize.height/2));
	countDownUI->setTag(kTagFamilyFightCountDownUI);
	GameView::getInstance()->getMainUIScene()->addChild(countDownUI);
}

void FamilyFightData::setStatus( int status )
{
	s_status = status;
}

int FamilyFightData::getStatus()
{
	return s_status;
}

void FamilyFightData::setCurrentBattleId( int battleId )
{
	s_currentBattleId = battleId;
}

int FamilyFightData::getCurrentBattleId()
{
	return s_currentBattleId;
}

void FamilyFightData::setFamilyFightTimeClass( int status )
{
	s_familyFightTimeClass = status;
}

int FamilyFightData::getFamilyFightTimeClass()
{
	return s_familyFightTimeClass;
}

void FamilyFightData::setFamilyFightFightClasss( int status )
{
	s_familyFightFightClasss = status;
}

int FamilyFightData::getFamilyFightFightClasss()
{
	return s_familyFightFightClasss;
}

void FamilyFightData::setFamilyFightRanking( int ranking )
{
	s_familyFightRanking = ranking;
}

int FamilyFightData::getFamilyFightRanking()
{
	return s_familyFightRanking;
}

void FamilyFightData::setFamilyFightScore( int score )
{
	s_familyFightScore = score;
}

int FamilyFightData::getFamilyFightScore()
{
	return s_familyFightScore;
}

void FamilyFightData::setFamilyRewardStatus( bool isGet )
{
	s_familyRewardStatus = isGet;
}

int FamilyFightData::getFamilyRewardStatus()
{
	return s_familyRewardStatus;
}

void FamilyFightData::setCurTime( std::string curTime )
{
	s_sCurTime = curTime;
}

std::string FamilyFightData::getCurTime()
{
	return s_sCurTime;
}

void FamilyFightData::setNextTime( std::string nextTime )
{
	s_sNextTime = nextTime;
}

std::string FamilyFightData::getNextTime()
{
	return s_sNextTime;
}

void FamilyFightData::setRemainTime( long long remaintime )
{
	s_nRemainTime = remaintime;
}

long long FamilyFightData::getRemainTime()
{
	return s_nRemainTime;
}

void FamilyFightData::setEntryLeftTime( long long remaintime )
{
	s_nEntryLeftTime = remaintime;
}

long long FamilyFightData::getEntryLeftTime()
{
	return s_nEntryLeftTime;
}

void FamilyFightData::setBossOwnerPos( int bossid,int x,int y )
{
	s_Boss_Owner_pos.bossid = bossid;
	s_Boss_Owner_pos.pos_x = x;
	s_Boss_Owner_pos.pos_y = y;
}

FamilyFightData::BossInfo FamilyFightData::getBossOwnerPos()
{
	return s_Boss_Owner_pos;
}

void FamilyFightData::setBossCommonPos( int bossid,int x,int y )
{
	s_Boss_Common_pos.bossid = bossid;
	s_Boss_Common_pos.pos_x = x;
	s_Boss_Common_pos.pos_y = y;
}

FamilyFightData::BossInfo FamilyFightData::getBossCommonPos()
{
	return s_Boss_Common_pos;
}

void FamilyFightData::setBossEnemyPos( int bossid,int x,int y )
{
	s_Boss_enemy_pos.bossid = bossid;
	s_Boss_enemy_pos.pos_x = x;
	s_Boss_enemy_pos.pos_y = y;
}

FamilyFightData::BossInfo FamilyFightData::getBossEnemyPos()
{
	return s_Boss_enemy_pos;
}

void FamilyFightData::setBossOwnerHpInfo( int curhp,int maxhp )
{
	s_Boss_Owner_hpInfo.curHp = curhp;
	s_Boss_Owner_hpInfo.maxHp = maxhp;
}

FamilyFightData::BossHpInfo FamilyFightData::getBossOwnerHpInfo()
{
	return s_Boss_Owner_hpInfo;
}

void FamilyFightData::setBossCommonHpInfo( int curhp,int maxhp )
{
	s_Boss_Common_hpInfo.curHp = curhp;
	s_Boss_Common_hpInfo.maxHp = maxhp;
}

FamilyFightData::BossHpInfo FamilyFightData::getBossCommonHpInfo()
{
	return s_Boss_Common_hpInfo;
}

void FamilyFightData::setBossEnemyHpInfo( int curhp,int maxhp )
{
	s_Boss_enemy_hpInfo.curHp = curhp;
	s_Boss_enemy_hpInfo.maxHp = maxhp;
}

FamilyFightData::BossHpInfo FamilyFightData::getBossEnemyHpInfo()
{
	return s_Boss_enemy_hpInfo;
}

void FamilyFightData::setAllToDefault()
{
	setStatus(FamilyFightData::status_none);
	setCurrentBattleId(0);
	setFamilyFightFightClasss(-1);
	setFamilyFightTimeClass(-1);
	isRegisted = false;
	isCancel = false;
	setFamilyFightRanking(-1);
	setFamilyFightScore(-1);
	setFamilyRewardStatus(false);
	setCurTime("");
	setNextTime("");
	setRemainTime(0);
	setEntryLeftTime(0);
	setBossOwnerPos(0,0,0);
	setBossCommonPos(0,0,0);
	setBossEnemyPos(0,0,0);
	setBossOwnerHpInfo(0,0);
	setBossCommonHpInfo(0,0);
	setBossEnemyHpInfo(0,0);
	//delete old rewardInfo
	std::vector<CGuildDailyReward*>::iterator iter;
	for (iter = FamilyFightData::getInstance()->familyFightRewardList.begin(); iter != FamilyFightData::getInstance()->familyFightRewardList.end(); ++iter)
	{
		delete *iter;
	}
	FamilyFightData::getInstance()->familyFightRewardList.clear();
}







