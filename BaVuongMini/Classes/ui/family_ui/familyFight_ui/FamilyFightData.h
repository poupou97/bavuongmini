#ifndef _UI_FAMILYFIGHTUI_FAMILYFIGHTDATA_H_
#define _UI_FAMILYFIGHTUI_FAMILYFIGHTDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

/////////////////////////////////
/**
 * ����ս�߼���
 * @author yangjun 
 * @version 0.1.0
 * @date 2014.07.15
 */

class CGuildDailyReward;
class CGuildFightRecord;
class CGuildFightRanking;

class FamilyFightData
{
public:
	FamilyFightData(void);
	~FamilyFightData(void);

	// ֪ͨ���� 0:δ��ʼ 1��ƥ���� 2��׼��ս�� 3��ս����ʼ 4��ս������
	enum FamilyFightStatus {
		status_none = 0,
		status_in_sequence,   //in the the waiting sequence
		status_prepareForFight,
		status_in_fight,
		status_fightEnded
	};


	////////////////////////// ���ʹ�� /////////////////////////
	// ս��ʱ��״̬ 
	enum FamilyFightTimeClass{
		timeClass_none = 0,  //������
		timeClass_signUp,      //������
		timeClass_fight          //ս����
	};
	// ���屨��״̬  true ������ false δ����
	static bool isRegisted;
	 // �����Ƿ�ȡ��ս�� 1:ȡ�� 0��δȡ��
	static bool isCancel;
	// ս��״̬
	enum FamilyFightFightClasss {
		fightClass_none = 0,
		fightClass_in_sequence,   //ƥ����
		fightClass_startFight,    //ս����ʼ
		fightClass_endFight   //ս������
	};
	///////////////////////////////////////////////////

	struct BossInfo{
		int bossid;
		int pos_x;
		int pos_y;
	};

	struct BossHpInfo{
		int curHp;
		int maxHp;
	};

public: 
	static FamilyFightData * s_familyFightData;
	static FamilyFightData * getInstance();

	void update();

	void setAllToDefault();

	static void setStatus(int status);
	static int getStatus();

	static void setCurrentBattleId(int battleId);
	static int getCurrentBattleId();

	static void setFamilyFightTimeClass(int status);
	static int getFamilyFightTimeClass();

	static void setFamilyFightFightClasss(int status);
	static int getFamilyFightFightClasss();

	static void setFamilyFightRanking(int ranking);
	static int getFamilyFightRanking();

	static void setFamilyFightScore(int score);
	static int getFamilyFightScore();

	static void setFamilyRewardStatus(bool isGet);
	static int getFamilyRewardStatus();

	static void setCurTime(std::string curTime);
	static std::string getCurTime();
	static void setNextTime(std::string nextTime);
	static std::string getNextTime();

	static void setRemainTime(long long remaintime);
	static long long getRemainTime();
	static void setEntryLeftTime(long long remaintime);
	static long long getEntryLeftTime();

	static void setBossOwnerPos(int bossid,int x,int y);
	static BossInfo getBossOwnerPos();
	static void setBossCommonPos(int bossid,int x,int y);
	static BossInfo getBossCommonPos();
	static void setBossEnemyPos(int bossid,int x,int y);
	static BossInfo getBossEnemyPos();

	static void setBossOwnerHpInfo(int curhp,int maxhp);
	static BossHpInfo getBossOwnerHpInfo();
	static void setBossCommonHpInfo(int curhp,int maxhp);
	static BossHpInfo getBossCommonHpInfo();
	static void setBossEnemyHpInfo(int curhp,int maxhp);
	static BossHpInfo getBossEnemyHpInfo();

private:
	static int s_status;
	static int s_currentBattleId;


	// ս��ʱ��״̬ 
	static int s_familyFightTimeClass;
	// ս��״̬
	static int s_familyFightFightClasss;

	// ��������
	static int s_familyFightRanking;
	// �������
	static int s_familyFightScore;	
	//������ȡ״̬
	static bool s_familyRewardStatus;

	// ��ǰս��ʱ��
	static std::string s_sCurTime;
	// �³�ս��ʱ��
	static std::string s_sNextTime;
	//��ǰ����սʣ��ʱ�䣨����ս���У��൱ǰ����ս������ʱ�䣩
	static long long s_nRemainTime;
	//��������ʣ��ʱ��
	static long long s_nEntryLeftTime;

	//�ҷ�BOSSλ����Ϣ
	static BossInfo s_Boss_Owner_pos;
	//����BOSSλ����Ϣ
	static BossInfo s_Boss_Common_pos;
	//�з�BOSSλ����Ϣ
	static BossInfo s_Boss_enemy_pos;

	//�ҷ�BOSSѪ����Ϣ
	static BossHpInfo s_Boss_Owner_hpInfo;
	//����BOSSѪ����Ϣ
	static BossHpInfo s_Boss_Common_hpInfo;
	//�з�BOSSѪ����Ϣ
	static BossHpInfo s_Boss_enemy_hpInfo;


public:
	// ������Ʒ
	std::vector<CGuildDailyReward*> familyFightRewardList;
};

#endif
