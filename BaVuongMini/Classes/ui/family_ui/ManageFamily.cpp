#include "ManageFamily.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGuildMemberBase.h"
#include "GameView.h"
#include "FamilyUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "AppMacros.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/MainScene.h"



ManageFamily::ManageFamily(void)
{
}


ManageFamily::~ManageFamily(void)
{
}

ManageFamily * ManageFamily::create()
{
	auto family_=new ManageFamily();
	if (family_ && family_->init())
	{
		family_->autorelease();
		return family_;
	}
	CC_SAFE_DELETE(family_);
	return NULL;
}

bool ManageFamily::init()
{
	if (UIScene::init())
	{
		Size winsize= Director::getInstance()->getVisibleSize();
		familyui_ =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		if(LoadSceneLayer::familyManagePanel->getParent() != NULL)
		{
			LoadSceneLayer::familyManagePanel->removeFromParentAndCleanup(false);
		}

		auto familypanel=LoadSceneLayer::familyManagePanel;
		familypanel->setAnchorPoint(Vec2(0.5f,0.5f));
		familypanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(familypanel);

		auto layer= Layer::create();
		layer->setIgnoreAnchorPointForPosition(false);
		layer->setAnchorPoint(Vec2(0.5f,0.5f));
		layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layer->setContentSize(Size(800,480));
		this->addChild(layer);

		const char * secondStr = StringDataManager::getString("chat_diaoshi_family_jia");
		const char * thirdStr = StringDataManager::getString("chat_diaoshi_family_zu");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		layer->addChild(atmature);

		tableviewstate = TableView::create(this, Size(654,360));
		tableviewstate->setDirection(TableView::Direction::VERTICAL);
		tableviewstate->setAnchorPoint(Vec2(0,0));
		tableviewstate->setPosition(Vec2(85,45));
		tableviewstate->setDelegate(this);
		tableviewstate->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		layer->addChild(tableviewstate);
		tableviewstate->reloadData();

		auto btnClose =(Button *)Helper::seekWidgetByName(familypanel,"Button_close");
		btnClose->setTouchEnabled(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addTouchEventListener(CC_CALLBACK_2(ManageFamily::callBackExit, this));

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void ManageFamily::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ManageFamily::onExit()
{
	UIScene::onExit();
}

void ManageFamily::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void ManageFamily::callBackAgree( Ref * obj )
{
	auto menu_ =(MenuItemSprite *)obj;
	int tag_ =menu_->getTag();

	long long roleId =familyui_->vectorApplyGuildMember.at(tag_)->id() ;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1503,(void*)1,(void *)roleId);

	/*
	for (int i=0;i<familyui_->vectorApplyGuildMember.size();i++)
	{
		if (familyui_->vectorApplyGuildMember.at(i)->id()==roleId)
		{
			std::vector<CGuildMemberBase *>::iterator iter= familyui_->vectorApplyGuildMember.begin() + i;
			CGuildMemberBase * applyPlayer=*iter ;
			familyui_->vectorApplyGuildMember.erase(iter);
			delete applyPlayer;
		}
	}
	tableviewstate->reloadData();
	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	GameView::getInstance()->applyPlayersize = familyui_->vectorApplyGuildMember.size();
	//mainscene_->checkIsNewRemind();
	mainscene_->remindFamilyApply();
	*/
}

void ManageFamily::callBAckRefuse( Ref * obj )
{
	auto menu_ =(MenuItemSprite *)obj;
	int tag_ =menu_->getTag();
	long long roleId =familyui_->vectorApplyGuildMember.at(tag_)->id() ;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1503,(void*)2,(void *)roleId);
	/*
	for (int i=0;i<familyui_->vectorApplyGuildMember.size();i++)
	{
		if (familyui_->vectorApplyGuildMember.at(i)->id()==roleId)
		{
			std::vector<CGuildMemberBase *>::iterator iter= familyui_->vectorApplyGuildMember.begin() + i;
			CGuildMemberBase * applyPlayer=*iter ;
			familyui_->vectorApplyGuildMember.erase(iter);
			delete applyPlayer;
		}
	}
	tableviewstate->reloadData();
	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	GameView::getInstance()->applyPlayersize = familyui_->vectorApplyGuildMember.size();
	//mainscene_->checkIsNewRemind();
	mainscene_->remindFamilyApply();
	*/
}

void ManageFamily::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{

}

void ManageFamily::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

void ManageFamily::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{

}

cocos2d::Size ManageFamily::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(650,63);
}

cocos2d::extension::TableViewCell* ManageFamily::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	__String * string =__String::createWithFormat("%d",idx);	
	auto cell =table->dequeueCell();
	cell=new TableViewCell();
	cell->autorelease();
	////////需要分页
	auto spbg = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
	spbg->setPreferredSize(Size(645,60));
	//spbg->setCapInsets(Rect(15,30,1,1));
	spbg->setCapInsets(Rect(18,9,2,23));
	spbg->setAnchorPoint(Vec2(0,0));
	spbg->setPosition(Vec2(0,0));
	cell->addChild(spbg);

	std::string applyerName = familyui_->vectorApplyGuildMember.at(idx)->name();
	auto labelName= Label::createWithTTF(applyerName.c_str(),APP_FONT_NAME,16);
	labelName->setAnchorPoint(Vec2(0,0));
	labelName->setPosition(Vec2(10,25));
	cell->addChild(labelName);

	int vipLevel_ = familyui_->vectorApplyGuildMember.at(idx)->viplevel();
	if (vipLevel_ > 0)
	{
		auto vipNode = MainScene::addVipInfoByLevelForNode(vipLevel_);
		cell->addChild(vipNode);
		vipNode->setPosition(Vec2(labelName->getPositionX() + labelName->getContentSize().width + vipNode->getContentSize().width/2,
											labelName->getPositionY()+vipNode->getContentSize().height/2));
	}

	int guiildLevel = familyui_->vectorApplyGuildMember.at(idx)->level();
	char levelStr[5];
	sprintf(levelStr,"%d",guiildLevel);
	auto labelManage= Label::createWithTTF(levelStr, APP_FONT_NAME,16);
	labelManage->setAnchorPoint(Vec2(0,0));
	labelManage->setPosition(Vec2(150,25));
	cell->addChild(labelManage);

 	int pression_ =familyui_->vectorApplyGuildMember.at(idx)->profession();
	std::string professionName = BasePlayer::getProfessionNameIdxByIndex(pression_);
	auto labelPression= Label::createWithTTF(professionName.c_str(), APP_FONT_NAME,16);
	labelPression->setAnchorPoint(Vec2(0,0));
	labelPression->setPosition(Vec2(212,25));
	cell->addChild(labelPression);

	//const char *stringSort_ = StringDataManager::getString("family_sortString_");
	int fight_ = familyui_->vectorApplyGuildMember.at(idx)->fightpoint();
	char fightStr_[20];
	sprintf(fightStr_,"%d",fight_);
	auto labelRank= Label::createWithTTF(fightStr_, APP_FONT_NAME,16);
	labelRank->setAnchorPoint(Vec2(0,0));
	labelRank->setPosition(Vec2(310,25));
	cell->addChild(labelRank);

	const char *strings_check = StringDataManager::getString("family_mamager_agree");
	char * str_check=const_cast<char*>(strings_check);	
	auto bg_check = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_1.png");
	bg_check->setPreferredSize(Size(80,36));
	bg_check->setCapInsets(Rect(15,30,1,1));
	auto btn_check = MenuItemSprite::create(bg_check, bg_check, bg_check, CC_CALLBACK_1(ManageFamily::callBackAgree,this));
	btn_check->setTag(idx);
	btn_check->setScale(0.5f);
	auto menu_check=CCMoveableMenu::create(btn_check,NULL);
	menu_check->setAnchorPoint(Vec2(0,0));
	menu_check->setPosition(Vec2(490,33));
	cell->addChild(menu_check);
	auto label_check= Label::createWithTTF(str_check,APP_FONT_NAME,16);
	label_check->setAnchorPoint(Vec2(0.5f,0.5f));
	label_check->setPosition(Vec2(bg_check->getContentSize().width/2,bg_check->getContentSize().height/2));
	btn_check->addChild(label_check);
	
	const char *strings_apply = StringDataManager::getString("family_manager_result");
	char * str_apply=const_cast<char*>(strings_apply);	
	auto bg_apply = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_2.png");
	bg_apply->setPreferredSize(Size(80,36));
	bg_apply->setCapInsets(Rect(15,30,1,1));
	auto btn_apply = MenuItemSprite::create(bg_apply, bg_apply, bg_apply, CC_CALLBACK_1(ManageFamily::callBAckRefuse,this));
	btn_apply->setTag(idx);
	btn_apply->setScale(0.5f);
	auto menu_apply=CCMoveableMenu::create(btn_apply,NULL);
	menu_apply->setAnchorPoint(Vec2(0,0));
	menu_apply->setPosition(Vec2(590,33));
	cell->addChild(menu_apply);	
	auto label_apply= Label::createWithTTF(str_apply,APP_FONT_NAME,16);
	label_apply->setAnchorPoint(Vec2(0.5f,0.5f));
	label_apply->setPosition(Vec2(bg_apply->getContentSize().width/2,bg_apply->getContentSize().height/2));
	btn_apply->addChild(label_apply);

	if (idx==familyui_->vectorApplyGuildMember.size()-3)
	{
		if (familyui_->hasNextPageOfManageMember == true)
		{
			familyui_->curPageOfManageFamilyMember ++;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1512,(void *)familyui_->curPageOfManageFamilyMember);
		}
	}

	return cell;
}

ssize_t ManageFamily::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	return familyui_->vectorApplyGuildMember.size();
}

void ManageFamily::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void ManageFamily::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void ManageFamily::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}


