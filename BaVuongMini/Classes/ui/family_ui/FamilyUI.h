
#ifndef _FAMILYUIMAIN_H_
#define _FAMILYUIMAIN_H_
#include "../extensions/UIScene.h"
#include "../../messageclient/element/CGuildCommodity.h"
#include "../extensions/UITab.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGuildBase;
class CGuildMemberBase;
class CGuildDonateOption;
class CCRichLabel;
class FamilyFightUI;

#define familyShoptag 100
#define familyAlms 110
#define familyLeaderManager 120
class FamilyUI:public UIScene,public cocos2d::extension::TableViewDataSource,public cocos2d::extension::TableViewDelegate
{
public:
	FamilyUI(void);
	~FamilyUI(void);
	static FamilyUI * create();
	bool init();
	void onEnter();
	void onExit();
	void callBackExit(Ref *pSender, Widget::TouchEventType type);

	Layout * mainPanel;
	Layout * familyInfo_panel;
	Layout * familyMember_panel;
	Layout * familyState_panel;
	Layout * familyState_panel_infoIsNUll;
	void changeUITabIndex(Ref * obj);

	CCRichLabel *familyNoticeLabel;
	cocos2d::extension::ScrollView * noticeScrollView;
	Button * btn_repairNotice;
	CCRichLabel *familyManifestoLabel;
	cocos2d::extension::ScrollView * manifestoScrollView;
	Button * btn_repairManifesto;

	void repairFamilyNotice(Ref *pSender, Widget::TouchEventType type);
	void repairFamilyManifesto(Ref *pSender, Widget::TouchEventType type);

	void callBackManageFamily(Ref *pSender, Widget::TouchEventType type);
	void memberQuitFamily(Ref *pSender, Widget::TouchEventType type);

	void callbackFamilyUplLevel(Ref *pSender, Widget::TouchEventType type);
	void callbackFamilyShop(Ref *pSender, Widget::TouchEventType type);
	void callbackFamilyAlms(Ref *pSender, Widget::TouchEventType type);

	void manageQuitFamily(Ref * obj);
	void quitFamilySure(Ref * obj);
	void downListMember(int index);
	void callBackDesFamily(Ref *pSender, Widget::TouchEventType type);
public:
	Size winsize;
	int currType;

	char * familyNotice;
	Layer *loadLayer;
	bool hasNextPageOfManageMember;
	int curPageOfManageFamilyMember;
	bool hasNextPageOfMember;
	int curPageOfFamilyMember;
public:
	////familyMember
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	// default implements are used to call script callback if exist
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

public:
	TableView *tableviewMember;
	std::vector<CGuildMemberBase *>vectorApplyGuildMember;
	std::vector<CGuildMemberBase *>vectorGuildMemberBase;
	std::vector<CGuildBase *>vectorGuildbase;
	Layer * layerMember;
	TableView *tableviewstate;
	Layer * layerstate;
	Button * btn_mamage;
	Button * btn_familyUplevel;

	Text * label_familyName;
	Text * label_familyLevel;
	Text * label_familyNumber;

	Text * label_familyContribution;
	Text * label_myPost;
	//Label * label_myAllContribution;
	Text * label_myRemContribution;
	////player post
	int selfPost_ ;
	long long selfId_;
	long long remPersonContrbution;
	long long curFamilyContrbution;
	long long nextFamilyContrbution;
	//����ÿһ��Ʒ�����ȡ�Ĺ���ֵ
	int familyAddContrbution;
	//����ÿһ��Ʒ���˻�ȡ�Ĺ���ֵ
	int personAddContrbution;
	//family num
	int curFamilyMemberNum;
	int maxFamilyMemberNum;


	std::vector<CGuildDonateOption *> guilidDonateVector;

	int familyLevel;
	//family shop vector
	std::vector<CGuildCommodity *> guildCommodityVector;

	//add by yangjun 2014.7.15
private:
	Layer * l_FamilyFightLayer;
	Layout * panel_familyFight;

	UITab * m_familytab;

	void initFamilyFightUI();

public:
	FamilyFightUI * GetFamilyFightUI();
	// LiuLiang++ open the FamilyFightUI
	void openFamilyFightUI();
};

#endif