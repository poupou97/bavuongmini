#include "FamilyAlms.h"
#include "../extensions/Counter.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/FolderInfo.h"
#include "FamilyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CGuildDonateOption.h"
#include"cocostudio\CCSGUIReader.h"

FamilyAlms::FamilyAlms(void)
{
	contributionCount =0;
}


FamilyAlms::~FamilyAlms(void)
{
}

FamilyAlms * FamilyAlms::create()
{
	auto alms = new FamilyAlms ();
	if (alms && alms->init())
	{
		alms->autorelease();
		return alms;
	}
	CC_SAFE_DELETE(alms);
	return NULL;
}

bool FamilyAlms::init()
{
	if (UIScene::init())
	{
		Size winsize  = Director::getInstance()->getVisibleSize();
		auto mainPanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/family_alms_1.json");
		mainPanel->setAnchorPoint(Vec2(0.5f,0.5f));
		mainPanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(mainPanel);
		 
		familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		labelFamilyLevel = (Text*)Helper::seekWidgetByName(mainPanel,"Label_familyLvValue");
		labelFamilyContrbution = (Text*)Helper::seekWidgetByName(mainPanel,"Label_familyLvValue_0");
		labelContrbutionOfMine = (Text*)Helper::seekWidgetByName(mainPanel,"Label_familyLvValue_0_0");

		auto btn_Input = (Button *)Helper::seekWidgetByName(mainPanel,"Button_shurukuang");
		btn_Input->setTouchEnabled(true);
		btn_Input->addTouchEventListener(CC_CALLBACK_2(FamilyAlms::callBackInput, this));

		auto btn_SurContrbution = (Button *)Helper::seekWidgetByName(mainPanel,"Button_enter");
		btn_SurContrbution->setTouchEnabled(true);
		btn_SurContrbution->setPressedActionEnabled(true);
		btn_SurContrbution->addTouchEventListener(CC_CALLBACK_2(FamilyAlms::callBackSurContrbution, this));

		labelCount = (Text*)Helper::seekWidgetByName(mainPanel,"showCountGetNum");
		familyContrbutionAdd = (Text*)Helper::seekWidgetByName(mainPanel,"familyContrbutionPersonAdd");
		familyPersonContrbutionAdd = (Text*)Helper::seekWidgetByName(mainPanel,"familyContrbutionValueAdd");

		auto btnClose = (Button *)Helper::seekWidgetByName(mainPanel,"Button_close");
		btnClose->setTouchEnabled(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addTouchEventListener(CC_CALLBACK_2(FamilyAlms::callBackClose, this));

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void FamilyAlms::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FamilyAlms::onExit()
{
	UIScene::onExit();
}

void FamilyAlms::callBackInput(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->isClosing())
			return;

		GameView::getInstance()->showCounter(this, CC_CALLFUNCO_SELECTOR(FamilyAlms::callBackGetCountNum));
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyAlms::callBackSurContrbution(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->getCurContrGoodsAmount() > 0)
		{
			if (contributionCount >0)
			{
				if (familyui->guilidDonateVector.size() > 0)
				{
					//�ݶ����幱Ʒֻ��һ� 
					//�����Ժ��� �����幱Ʒ����Ҳ��Ҫ��
					std::string propid_ = familyui->guilidDonateVector.at(0)->propid();
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1509, (void *)propid_.c_str(), (void *)contributionCount);
				}
			}
			else
			{
				const char * string_ = StringDataManager::getString("familyContrbutionAmount");
				GameView::getInstance()->showAlertDialog(string_);
			}
		}
		else
		{
			const char * string_ = StringDataManager::getString("familyNotContrbutionAmout");
			GameView::getInstance()->showAlertDialog(string_);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyAlms::callBackClose(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void FamilyAlms::callBackGetCountNum( Ref * obj )
{
	auto counter_ =(Counter *)obj;
	int num = counter_->getInputNum();
	if (num >this->getCurContrGoodsAmount())
	{
		contributionCount = this->getCurContrGoodsAmount();
	}else
	{
		contributionCount = num;
	}
	char labelNum[10];
	sprintf(labelNum,"%d",contributionCount);
	labelCount->setString(labelNum);
}

int FamilyAlms::getCurContrGoodsAmount()
{
	int contrGoodsAmount = 0;
	
	int size = GameView::getInstance()->AllPacItem.size();
	for (int i =0;i<size;i++)
	{
		if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
		{
			std::string pacGoodsId = GameView::getInstance()->AllPacItem.at(i)->goods().id();

			for (int j = 0;j<familyui->guilidDonateVector.size();j++)
			{
				std::string contrbutionPropId = familyui->guilidDonateVector.at(j)->propid();
				if (strcmp(pacGoodsId.c_str(),contrbutionPropId.c_str())==0)
				{
					contrGoodsAmount +=GameView::getInstance()->AllPacItem.at(i)->quantity();
				}
			}
		}
	}
	return contrGoodsAmount;
}

void FamilyAlms::refreshInfo(bool isDefault)
{
	int familyLevel = familyui->familyLevel;
	char familyLevelStr[10];
	sprintf(familyLevelStr,"%d",familyLevel);
	labelFamilyLevel->setString(familyLevelStr);

	if (isDefault == true)
	{
		contributionCount = this->getCurContrGoodsAmount();
		char showStrContrbution[10];
		sprintf(showStrContrbution,"%d",this->getCurContrGoodsAmount());
		labelCount->setString(showStrContrbution);
	}else
	{
		contributionCount = 0;
		labelCount->setString("0");
	}
	
	long long familyContrbutionOfMine = familyui->curFamilyContrbution;
	long long familyContrbution = familyui->nextFamilyContrbution;
	std::string showContrbution = "";
	if (familyContrbution > 0 )
	{
		char contrbutionMine[10];
		sprintf(contrbutionMine,"%d",familyContrbutionOfMine);
		char contrbutionFamily[20];
		sprintf(contrbutionFamily,"%ld",familyContrbution);

		showContrbution.append(contrbutionMine);
		showContrbution.append("/");
		showContrbution.append(contrbutionFamily);
	}else
	{
		const char * familyContrbution_  = StringDataManager::getString("familyLveleIsFull");
		showContrbution.append(familyContrbution_);
	}
	labelFamilyContrbution->setString(showContrbution.c_str());
	//��ݶ����幱Ʒֻ��һ� 
	//�����Ժ��� �����幱Ʒ����Ҳ��Ҫ��
	const char * remStr_ = StringDataManager::getString("family_jiazugongpinshengyu");
	char remContrbutionStr[20];
	sprintf(remContrbutionStr,"%d",this->getCurContrGoodsAmount());
	std::string showContrbutionOfMine= "(";
	showContrbutionOfMine.append(remStr_);
	showContrbutionOfMine.append(remContrbutionStr);
	showContrbutionOfMine.append(")");
	labelContrbutionOfMine->setString(showContrbutionOfMine.c_str());

	char famStr[20];
	sprintf(famStr,"%d",familyui->guilidDonateVector.at(0)->donate());
	familyContrbutionAdd->setString(famStr);

	char perStr[20];
	sprintf(perStr,"%d",familyui->guilidDonateVector.at(0)->personaldonate());
	familyPersonContrbutionAdd->setString(perStr);
}
