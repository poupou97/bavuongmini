#ifndef _FAMILYSHOPCOUNT_
#define _FAMILYSHOPCOUNT_
#include "../extensions/UIScene.h"
#include "../../messageclient/element/CGuildCommodity.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class FamilyShopCount:public UIScene
{
public:
	FamilyShopCount(void);
	~FamilyShopCount(void);

	static FamilyShopCount * create(CGuildCommodity * commodity);
	bool init(CGuildCommodity * commodity);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);


	void BuyEvent(Ref *pSender, Widget::TouchEventType type);
	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	void getAmount(Ref *pSender, Widget::TouchEventType type);
	void clearNum(Ref *pSender, Widget::TouchEventType type);
	void deleteNum(Ref *pSender, Widget::TouchEventType type);

	void setToDefaultNum();
private:
	Size winsize;
	Layer * u_layer;
	//Label * l_priceOfOne;
	Text * l_buyNum;
	Text * l_totalPrice;


	Button * btn_inputNumValue;
	Text * l_constGoldValue;

	//����Ʒ���
	int basePrice;
	//۹�������
	int buyNum;
	std::string str_buyNum;
	//������ܼ
	int buyPrice;

	//۵�� 
	Text * l_priceOfOne;
};
#endif;
