#include "FamilyShop.h"
#include "FamilyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "FamilyShopCount.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../utils/StaticDataManager.h"
#include "cocostudio\CCSGUIReader.h"
#include "AppMacros.h"


FamilyShop::FamilyShop(void)
{
}


FamilyShop::~FamilyShop(void)
{
}

FamilyShop * FamilyShop::create()
{
	auto shop =new FamilyShop();
	if (shop && shop->init())
	{
		shop->autorelease();
		return shop;
	}
	CC_SAFE_DELETE(shop);
	return NULL;
}

bool FamilyShop::init()
{
	if (UIScene::init())
	{
		winsize= Director::getInstance()->getVisibleSize();
		familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		auto mainPanel=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/family_shop_1.json");
		mainPanel->setAnchorPoint(Vec2(0.5f,0.5f));
		mainPanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(mainPanel);

		labelContrbution = (Text*)Helper::seekWidgetByName(mainPanel,"Label_activityValue");
		char contrbutionRem[20];
		sprintf(contrbutionRem,"%d",familyui->remPersonContrbution);
		labelContrbution->setString(contrbutionRem);

		loadLayer= Layer::create();
		loadLayer->setIgnoreAnchorPointForPosition(false);
		loadLayer->setAnchorPoint(Vec2(0.5f,0.5f));
		loadLayer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		loadLayer->setContentSize(Size(800,480));
		this->addChild(loadLayer);
		
		auto btnClose = (Button *)Helper::seekWidgetByName(mainPanel,"Button_close");
		btnClose->setTouchEnabled(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addTouchEventListener(CC_CALLBACK_2(FamilyShop::callBackExit, this));
		
		tableviewMember = TableView::create(this, Size(504,260));
		tableviewMember->setDirection(TableView::Direction::VERTICAL);
		tableviewMember->setAnchorPoint(Vec2(0,0));
		tableviewMember->setPosition(Vec2(153,109));
		tableviewMember->setDelegate(this);
		tableviewMember->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		loadLayer->addChild(tableviewMember);
		
// 		Layer * layer_= Layer::create();
// 		layer_->setIgnoreAnchorPointForPosition(false);
// 		layer_->setAnchorPoint(Vec2(0.5f,0.5f));
// 		layer_->setPosition(Vec2(winsize.width/2,winsize.height/2));
// 		layer_->setContentSize(Size(800,480));
// 		this->addChild(layer_,10);
// 
// 		ImageView * imageBg = ImageView::create();
// 		imageBg->setScale9Enabled(true);
// 		imageBg->setContentSize(Size(515,277));
// 		imageBg->setCapInsets(Rect(32,32,1,1));
// 		imageBg->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		imageBg->setAnchorPoint(Vec2(0.5f,0.5f));
// 		imageBg->setPosition(Vec2(400,240));
// 		layer_->addChild(imageBg);

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void FamilyShop::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FamilyShop::onExit()
{
	UIScene::onExit();
}

void FamilyShop::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void FamilyShop::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{

}

void FamilyShop::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

void FamilyShop::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{

}

cocos2d::Size FamilyShop::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(200,65);
}

cocos2d::extension::TableViewCell* FamilyShop::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{

	__String *string = __String::createWithFormat("%d", idx);
	auto cell =table->dequeueCell();   // this method must be called

	cell=new TableViewCell();
	cell->autorelease();

	int commoditySize = familyui->guildCommodityVector.size();
	if (idx == commoditySize/2)
	{
		if (commoditySize%2 == 0)
		{
			auto goodsItemL =goodsItemCell::create(2*idx);
			goodsItemL->setAnchorPoint(Vec2(0.5f,0.5f));
			goodsItemL->setPosition(Vec2(0,0));
			cell->addChild(goodsItemL);

			auto goodsItemR =goodsItemCell::create(2*idx+1);
			goodsItemR->setAnchorPoint(Vec2(0.5f,0.5f));
			goodsItemR->setPosition(Vec2(250,0));
			cell->addChild(goodsItemR);
		}else
		{
			auto goodsItemL =goodsItemCell::create(2*idx);
			goodsItemL->setAnchorPoint(Vec2(0.5f,0.5f));
			goodsItemL->setPosition(Vec2(0,0));
			cell->addChild(goodsItemL);
		}
	}
	else
	{
		auto goodsItemL =goodsItemCell::create(2*idx);
		goodsItemL->setAnchorPoint(Vec2(0.5f,0.5f));
		goodsItemL->setPosition(Vec2(0,0));
		cell->addChild(goodsItemL);

		auto goodsItemR =goodsItemCell::create(2*idx+1);
		goodsItemR->setAnchorPoint(Vec2(0.5f,0.5f));
		goodsItemR->setPosition(Vec2(250,0));
		cell->addChild(goodsItemR);
	}

	return cell;
}

ssize_t FamilyShop::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	int vectorSize = familyui->guildCommodityVector.size();
	if (vectorSize%2 == 0)
	{
		return vectorSize/2;
	}else
	{
		return vectorSize/2 + 1;
	}
}

void FamilyShop::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void FamilyShop::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void FamilyShop::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}
//////////////////////////////////

goodsItemCell::goodsItemCell( void )
{

}

goodsItemCell::~goodsItemCell( void )
{

}

goodsItemCell * goodsItemCell::create( int idx )
{
	auto goods_ =new goodsItemCell();
	if (goods_ && goods_->init(idx))
	{
		goods_->autorelease();
		return goods_;
	}
	CC_SAFE_DELETE(goods_);
	return NULL;
}

bool goodsItemCell::init( int idx )
{
	if (UIScene::init())
	{
		winsize= Director::getInstance()->getVisibleSize();
		auto familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		auto imageBackground = ImageView::create();
		imageBackground->loadTexture("res_ui/kuang0_new.png");
		imageBackground->setScale9Enabled(true);
		imageBackground->setContentSize(Size(240,60));
		imageBackground->setAnchorPoint(Vec2(0,0));
		imageBackground->setPosition(Vec2(0,0));
		m_pLayer->addChild(imageBackground);
		
		std::string bgSp = getColorSpByQuality(familyui->guildCommodityVector.at(idx)->goods().quality());
		auto goodsSpbtn= Button::create();
		goodsSpbtn->setTouchEnabled(true);
		//goodsSpbtn->setPressedActionEnabled(true);
		goodsSpbtn->addTouchEventListener(CC_CALLBACK_2(goodsItemCell::showGoodsInfo, this));
		goodsSpbtn->setAnchorPoint(Vec2(0.5f,0.5f));
		goodsSpbtn->setScale9Enabled(true);
		goodsSpbtn->setContentSize(Size(50,50));
		goodsSpbtn->setTag(idx);
		goodsSpbtn->setCapInsets(Rect(15,30,1,1));
		goodsSpbtn->setPosition(Vec2(goodsSpbtn->getContentSize().width/2+10,imageBackground->getContentSize().height/2));
		goodsSpbtn->loadTextures(bgSp.c_str(),bgSp.c_str(),"");
		imageBackground->addChild(goodsSpbtn);

		std::string iconPath = "res_ui/props_icon/";
		iconPath.append(familyui->guildCommodityVector.at(idx)->goods().icon());
		iconPath.append(".png");

		auto imageGoods = ImageView::create();
		imageGoods->loadTexture(iconPath.c_str());
		imageGoods->setAnchorPoint(Vec2(0.5f,0.5f));
		imageGoods->setPosition(Vec2(0,0));
		goodsSpbtn->addChild(imageGoods);

		auto labelName = Label::createWithTTF(familyui->guildCommodityVector.at(idx)->goods().name().c_str(), APP_FONT_NAME, 18);
		labelName->setAnchorPoint(Vec2(0,0));
		labelName->setPosition(Vec2(goodsSpbtn->getPosition().x+goodsSpbtn->getContentSize().width/2,goodsSpbtn->getPosition().y+5));
		labelName->setColor(GameView::getInstance()->getGoodsColorByQuality(familyui->guildCommodityVector.at(idx)->goods().quality()));
		imageBackground->addChild(labelName);

		int goodsPrice_ = familyui->guildCommodityVector.at(idx)->guildhonor();
		char goodsPriceStr[5];
		sprintf(goodsPriceStr,"%d",goodsPrice_);
		auto labelPrice = Label::createWithTTF(goodsPriceStr, APP_FONT_NAME, 18);
		labelPrice->setAnchorPoint(Vec2(0,0));
		labelPrice->setPosition(Vec2(goodsSpbtn->getPosition().x+goodsSpbtn->getContentSize().width,goodsSpbtn->getPosition().y-20));
		imageBackground->addChild(labelPrice);
		
		auto buyButton= Button::create();
		buyButton->setTouchEnabled(true);
		buyButton->setPressedActionEnabled(true);
		buyButton->addTouchEventListener(CC_CALLBACK_2(goodsItemCell::callBackBuy, this));
		buyButton->setAnchorPoint(Vec2(0.5f,0.5f));
		buyButton->setScale9Enabled(true);
		buyButton->setContentSize(Size(80,40));
		buyButton->setCapInsets(Rect(15,30,1,1));
		buyButton->setTag(idx);
		buyButton->setPosition(Vec2(imageBackground->getContentSize().width -buyButton->getContentSize().width/2-10 ,imageBackground->getContentSize().height/2));
		buyButton->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
		imageBackground->addChild(buyButton);
		
		const char *stringBuy = StringDataManager::getString("goods_auction_buy");
		auto buyLabel = Label::createWithTTF(stringBuy, APP_FONT_NAME, 18);
		buyLabel->setAnchorPoint(Vec2(0.5f,0.5f));
		buyLabel->setPosition(Vec2(0,0));
		buyButton->addChild(buyLabel);

		return true;
	}
	return false;
}

void goodsItemCell::onEnter()
{
	UIScene::onEnter();
}

void goodsItemCell::onExit()
{
	UIScene::onExit();
}

std::string goodsItemCell::getColorSpByQuality( int quality )
{
	std::string iconFramePath;
	switch(quality)
	{
	case 1 :
		{
			iconFramePath = "res_ui/smdi_white.png";
		}
		break;
	case 2 :
		{
			iconFramePath = "res_ui/smdi_green.png";
		}
		break;
	case 3 :
		{
			iconFramePath = "res_ui/smdi_bule.png";
		}
		break;
	case 4 :
		{
			iconFramePath = "res_ui/smdi_purple.png";
		}
		break;
	case 5 :
		{
			iconFramePath = "res_ui/smdi_orange.png";
		}
		break;
	}

	return iconFramePath;
}

void goodsItemCell::callBackBuy(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button *)pSender;
		int selectId = btn->getTag();
		auto familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);

		auto commodity_ = new CGuildCommodity();
		commodity_->CopyFrom(*familyui->guildCommodityVector.at(selectId));

		if (GameView::getInstance()->getMainUIScene()->getChildByTag(4562) == NULL)
		{
			auto count_ = FamilyShopCount::create(commodity_);
			count_->setIgnoreAnchorPointForPosition(false);
			count_->setAnchorPoint(Vec2(0.5f, 0.5f));
			count_->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			count_->setTag(4562);
			GameView::getInstance()->getMainUIScene()->addChild(count_);
		}

		delete commodity_;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void goodsItemCell::showGoodsInfo(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button *)pSender;
		int selectId = btn->getTag();
		auto familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		auto goods_ = new GoodsInfo();
		goods_->CopyFrom(familyui->guildCommodityVector.at(selectId)->goods());

		auto goodsInfo = GoodsItemInfoBase::create(goods_, GameView::getInstance()->EquipListItem, 0);
		goodsInfo->setIgnoreAnchorPointForPosition(false);
		goodsInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
		GameView::getInstance()->getMainUIScene()->addChild(goodsInfo);
		delete goods_;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}


