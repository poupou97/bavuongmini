#include "FamilyOperatorOfLeader.h"
#include "ManageFamily.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../extensions/RichTextInput.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "FamilyUI.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "cocostudio\CCSGUIReader.h"
#include "AppMacros.h"

FamilyOperatorOfLeader::FamilyOperatorOfLeader(void)
{
}


FamilyOperatorOfLeader::~FamilyOperatorOfLeader(void)
{
}

FamilyOperatorOfLeader * FamilyOperatorOfLeader::create()
{
	auto operatorFamily =new FamilyOperatorOfLeader();
	if (operatorFamily && operatorFamily->init())
	{
		operatorFamily->autorelease();
		return operatorFamily;
	}
	CC_SAFE_DELETE(operatorFamily);
	return NULL;
}

bool FamilyOperatorOfLeader::init()
{
	if (UIScene::init())
	{
		winsize=Director::getInstance()->getVisibleSize();

		auto panel_ =Layout::create();
		panel_->setAnchorPoint(Vec2(0.5f,0.5f));
		panel_->setPosition(Vec2(0,0));
		m_pLayer->addChild(panel_);

		auto background=ImageView::create();
		background->loadTexture("res_ui/zhezhao80.png");
		background->setScale9Enabled(true);
		background->setContentSize(Size(198,120));
		background->setAnchorPoint(Vec2(0,0));
		background->setPosition(Vec2(99,19));
		panel_->addChild(background);

		auto backgroundFram=ImageView::create();
		backgroundFram->loadTexture("res_ui/bian_1.png");
		backgroundFram->setScale9Enabled(true);
		backgroundFram->setContentSize(Size(200,120));
		backgroundFram->setCapInsets(Rect(7,8,2,2));
		backgroundFram->setAnchorPoint(Vec2(0,0));
		backgroundFram->setPosition(Vec2(98,20));
		panel_->addChild(backgroundFram);

		auto backgroundFramRect=ImageView::create();
		backgroundFramRect->loadTexture("res_ui/LV3_dikuang_light.png");
		backgroundFramRect->setScale9Enabled(true);
		backgroundFramRect->setContentSize(Size(196,117));
		backgroundFramRect->setCapInsets(Rect(7.5f,13.5f,3.0f,3.0f));
		backgroundFramRect->setAnchorPoint(Vec2(0,0));
		backgroundFramRect->setPosition(Vec2(100,20));
		panel_->addChild(backgroundFramRect);

		const char *str_admit = StringDataManager::getString("family_zhaoshou");
		//招收 
		auto button_admit= Button::create();
		button_admit->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		button_admit->setTouchEnabled(true);
		button_admit->setPressedActionEnabled(true);
		button_admit->addTouchEventListener(CC_CALLBACK_2(FamilyOperatorOfLeader::callBackConvene, this));
		button_admit->setAnchorPoint(Vec2(0.5f,0.5f));
		button_admit->setScale9Enabled(true);
		button_admit->setContentSize(Size(80,43));
		button_admit->setCapInsets(Rect(18,9,2,23));
		button_admit->setPosition(Vec2(155,106));
		panel_->addChild(button_admit);
// 		Button * button_admit = Button::create();
// 		button_admit->setPressedActionEnabled(true);
// 		button_admit->setTouchEnabled(true);
// 		button_admit->loadTextures("res_ui/button_16.png","res_ui/button_16.png","");
// 		button_admit->setFontSize(25);
// 		button_admit->setAnchorPoint(Vec2(0.5f,0.5f));
// 		button_admit->setPosition(Vec2(155,106));
// 		button_admit->addTouchEventListener(CC_CALLBACK_2(FamilyOperatorOfLeader::callBackConvene));
// 		panel_->addChild(button_admit);

		auto labelAdmit= Label::createWithTTF(str_admit, APP_FONT_NAME, 16);
		labelAdmit->setAnchorPoint(Vec2(0.5f,0.5f));
		labelAdmit->setPosition(Vec2(0,0));
		button_admit->addChild(labelAdmit);

		const char *str_check = StringDataManager::getString("family_shenhe");
		//、审核
		button_check= Button::create();
		button_check->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		button_check->setTouchEnabled(true);
		button_check->setPressedActionEnabled(true);
		button_check->addTouchEventListener(CC_CALLBACK_2(FamilyOperatorOfLeader::callBackCheck, this));
		button_check->setAnchorPoint(Vec2(0.5f,0.5f));
		button_check->setScale9Enabled(true);
		button_check->setContentSize(Size(80,43));
		button_check->setCapInsets(Rect(18,9,2,23));
		button_check->setPosition(Vec2(244,106));
		panel_->addChild(button_check);

// 		Button * button_check = Button::create();
// 		button_check->setPressedActionEnabled(true);
// 		button_check->setTouchEnabled(true);
// 		button_check->loadTextures("res_ui/button_16.png","res_ui/button_16.png","");
// 		button_check->setFontSize(25);
// 		button_check->setAnchorPoint(Vec2(0.5f,0.5f));
// 		button_check->setPosition(Vec2(244,106));
// 		button_check->addTouchEventListener(CC_CALLBACK_2(FamilyOperatorOfLeader::callBackCheck));
// 		panel_->addChild(button_check);

		auto labelcheck= Label::createWithTTF(str_check, APP_FONT_NAME, 16);
		labelcheck->setAnchorPoint(Vec2(0.5f,0.5f));
		labelcheck->setPosition(Vec2(0,0));
		labelcheck->setLocalZOrder(10);
		button_check->addChild(labelcheck);
	
		const char *str_convene = StringDataManager::getString("family_zhaoji");
		// 召集
		auto button_convene= Button::create();
		button_convene->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		button_convene->setTouchEnabled(true);
		button_convene->setPressedActionEnabled(true);
		button_convene->addTouchEventListener(CC_CALLBACK_2(FamilyOperatorOfLeader::callBackCallTogether,this));
		button_convene->setAnchorPoint(Vec2(0.5f,0.5f));
		button_convene->setScale9Enabled(true);
		button_convene->setContentSize(Size(80,43));
		button_convene->setCapInsets(Rect(18,9,2,23));
		button_convene->setPosition(Vec2(155,53));
		panel_->addChild(button_convene);

// 		Button * button_convene = Button::create();
// 		button_convene->setPressedActionEnabled(true);
// 		button_convene->setTouchEnabled(true);
// 		button_convene->loadTextures("res_ui/button_16.png","res_ui/button_16.png","");
// 		button_convene->setFontSize(16);
// 		button_convene->setAnchorPoint(Vec2(0.5f,0.5f));
// 		button_convene->setPosition(Vec2(155,53));
// 		button_convene->addTouchEventListener(CC_CALLBACK_2(FamilyOperatorOfLeader::callBackCallTogether));
// 		panel_->addChild(button_convene);

		auto labelConvene= Label::createWithTTF(str_convene, APP_FONT_NAME, 16);
		labelConvene->setAnchorPoint(Vec2(0.5f,0.5f));
		labelConvene->setPosition(Vec2(0,0));
		button_convene->addChild(labelConvene);
		
		const char *str_dismiss = StringDataManager::getString("family_jiesan");
		//解散
		auto button_dismiss= Button::create();
		button_dismiss->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		button_dismiss->setTouchEnabled(true);
		button_dismiss->setPressedActionEnabled(true);
		button_dismiss->addTouchEventListener(CC_CALLBACK_2(FamilyOperatorOfLeader::callBackDismissSure, this));
		button_dismiss->setAnchorPoint(Vec2(0.5f,0.5f));
		button_dismiss->setScale9Enabled(true);
		button_dismiss->setContentSize(Size(80,43));
		button_dismiss->setCapInsets(Rect(18,9,2,23));
		button_dismiss->setPosition(Vec2(244,53));
		//button_dismiss->setPosition(Vec2(155,53));
		panel_->addChild(button_dismiss);

// 		Button * button_dismiss = Button::create();
// 		button_dismiss->setPressedActionEnabled(true);
// 		button_dismiss->setTouchEnabled(true);
// 		button_dismiss->loadTextures("res_ui/button_16.png","res_ui/button_16.png","");
// 		button_dismiss->setFontSize(25);
// 		button_dismiss->setAnchorPoint(Vec2(0.5f,0.5f));
// 		button_dismiss->setPosition(Vec2(244,53));
// 		button_dismiss->addTouchEventListener(CC_CALLBACK_2(FamilyOperatorOfLeader::callBackDismissSure));
// 		panel_->addChild(button_dismiss);

		auto labeldismiss= Label::createWithTTF(str_dismiss, APP_FONT_NAME, 16);
		labeldismiss->setAnchorPoint(Vec2(0.5f,0.5f));
		labeldismiss->setPosition(Vec2(0,0));
		button_dismiss->addChild(labeldismiss);
		
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(background->getContentSize());

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(FamilyUI::onTouchBegan, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void FamilyOperatorOfLeader::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FamilyOperatorOfLeader::onExit()
{
	UIScene::onExit();
}

void FamilyOperatorOfLeader::callBackConvene(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//招收
		this->closeAnim();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1508, (void*)5);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void FamilyOperatorOfLeader::callBackCheck(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
		//审核
		auto manageFamily = ManageFamily::create();
		manageFamily->setIgnoreAnchorPointForPosition(false);
		manageFamily->setAnchorPoint(Vec2(0.5f, 0.5f));
		manageFamily->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
		manageFamily->setTag(kTagManageFamily);
		GameView::getInstance()->getMainUIScene()->addChild(manageFamily);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void FamilyOperatorOfLeader::callBackCallTogether(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1530);
		/*
		const char *strings_ = StringDataManager::getString("feature_will_be_open");
		GameView::getInstance()->showAlertDialog(strings_);
		*/
		//召集
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyOperatorOfLeader::callBackDismissSure(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		const char *strings = StringDataManager::getString("family_btnmember_DismissSure");
		GameView::getInstance()->showPopupWindow(strings, 2, this, CC_CALLFUNCO_SELECTOR(FamilyOperatorOfLeader::callBackDismiss), NULL);

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FamilyOperatorOfLeader::callBackDismiss( Ref * obj )
{
	auto familyui_ =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	//解散
	if (familyui_->selfPost_==1)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1510,this);
	}else
	{
		const char *strings = StringDataManager::getString("family_DismissOfleader");
		GameView::getInstance()->showAlertDialog(strings);
	}
}

void FamilyOperatorOfLeader::callBackClose( Ref * obj )
{
	this->closeAnim();
}

bool FamilyOperatorOfLeader::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}



/////////////////////////////////////////
FamilyChangeNotice::FamilyChangeNotice()
{

}
FamilyChangeNotice::~FamilyChangeNotice()
{

}

FamilyChangeNotice * FamilyChangeNotice::create(int type)
{
	auto familyNotice =new FamilyChangeNotice();
	if (familyNotice && familyNotice->init(type))
	{
		familyNotice->autorelease();
		return familyNotice;
	}
	CC_SAFE_DELETE(familyNotice);
	return NULL;
}

bool FamilyChangeNotice::init(int type)
{
	if (UIScene::init())
	{
		Size winsize =Director::getInstance()->getVisibleSize();

		auto mainPanel=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/family_ChangePuipose_popupo_1.json");
		mainPanel->setAnchorPoint(Vec2(0,0));
		mainPanel->setPosition(Vec2(0,0));
		m_pLayer->addChild(mainPanel);

		auto loadLayer= Layer::create();
		loadLayer->setIgnoreAnchorPointForPosition(false);
		loadLayer->setAnchorPoint(Vec2(0.5f,0.5f));
		loadLayer->setPosition(Vec2(mainPanel->getContentSize().width/2,mainPanel->getContentSize().height/2));
		loadLayer->setContentSize(mainPanel->getContentSize());
		this->addChild(loadLayer);
		////intputText
// 		ImageView * image_ =ImageView::create();
// 		image_->loadTexture("res_ui/tietu_flower.png");
// 		image_->setAnchorPoint(Vec2(0,0));
// 		image_->setPosition(Vec2(45,loadLayer->getContentSize().height/2+50));
// 		loadLayer->addChild(image_);

		auto label_ = (Text*)Helper::seekWidgetByName(mainPanel,"Label_tip");
		if (type == 1)
		{
			const char * secondStr = StringDataManager::getString("family_genggaizongzhi");
			label_->setString(secondStr);
		}else
		{
			const char * secondStr = StringDataManager::getString("family_genggaixuanyang");
			label_->setString(secondStr);
		}

		textFamilyNotice=new RichTextInputBox();
		textFamilyNotice->setMultiLinesMode(true);
		textFamilyNotice->setInputBoxWidth(265);
		textFamilyNotice->setInputBoxHeight(130);
		textFamilyNotice->setDirection(RichTextInputBox::kCCRichInputDirectionVertical);
		textFamilyNotice->setAnchorPoint(Vec2(0,0));
		textFamilyNotice->setPosition(Vec2(45,loadLayer->getContentSize().height/2+30));
		loadLayer->addChild(textFamilyNotice,10);
		textFamilyNotice->autorelease();

		auto btnSure_ =(Button *)Helper::seekWidgetByName(mainPanel,"Button_enter");
		btnSure_->setTouchEnabled(true);
		btnSure_->setPressedActionEnabled(true);
		btnSure_->setTag(type);
		btnSure_->addTouchEventListener(CC_CALLBACK_2(FamilyChangeNotice::callBackSure, this));

		auto btnCancel_ =(Button *)Helper::seekWidgetByName(mainPanel,"Button_cancel");
		btnCancel_->setTouchEnabled(true);
		btnCancel_->setPressedActionEnabled(true);
		btnCancel_->addTouchEventListener(CC_CALLBACK_2(FamilyChangeNotice::callBackCancel, this));

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(mainPanel->getContentSize());
		return true;
	}
	return false;
}

void FamilyChangeNotice::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void FamilyChangeNotice::onExit()
{
	UIScene::onExit();
}

bool FamilyChangeNotice::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return true;
}

void FamilyChangeNotice::callBackSure(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto familyui_ = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		auto btn = (Button *)pSender;
		familyui_->familyNotice = (char *)textFamilyNotice->getInputString();
		if (btn->getTag() == 1)
		{
			if (familyui_->familyNotice != NULL)
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1506, (void*)familyui_->familyNotice);
			}
		}
		else
		{
			if (familyui_->familyNotice != NULL)
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1523, (void*)familyui_->familyNotice);
			}
		}

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void FamilyChangeNotice::callBackCancel(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}


