#ifndef _FAMILY_CREATEFAMILY_H
#define _FAMILY_CREATEFAMILY_H
#include "../extensions/UIScene.h"

class CGuildMemberBase;
class FamilyUI;
class ManageFamily:public UIScene,public cocos2d::extension::TableViewDataSource,public cocos2d::extension::TableViewDelegate
{
public:
	ManageFamily(void);
	~ManageFamily(void);


	static ManageFamily *create();
	bool init();
	void onEnter();
	void onExit();

	void callBackExit(Ref *pSender, Widget::TouchEventType type);
	void callBackAgree(Ref * obj);
	void callBAckRefuse(Ref * obj);
public:
	////familyMember
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	// default implements are used to call script callback if exist
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
	TableView *tableviewstate;
private:
	FamilyUI * familyui_;
};


#endif