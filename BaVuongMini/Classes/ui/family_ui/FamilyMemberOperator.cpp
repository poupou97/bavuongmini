#include "FamilyMemberOperator.h"
#include "../../utils/StaticDataManager.h"
#include "../extensions/UITab.h"
#include "FamilyUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/element/CGuildMemberBase.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../Friend_ui/FriendUi.h"
#include "../Chat_ui/ChatUI.h"
#include "../Chat_ui/AddPrivateUi.h"
#include "../extensions/RichTextInput.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/MainScene.h"


FamilyMemberOperator::FamilyMemberOperator(void)
{
}


FamilyMemberOperator::~FamilyMemberOperator(void)
{
}

FamilyMemberOperator * FamilyMemberOperator::create(int post_,int index_)
{
	auto familyMember =new FamilyMemberOperator();
	if (familyMember && familyMember->init(post_,index_))
	{
		familyMember->autorelease();
		return familyMember;
	}
	CC_SAFE_DELETE(familyMember);
	return NULL;
}

bool FamilyMemberOperator::init(int post_,int index_)
{
	if (UIScene::init())
	{
		winSize=Director::getInstance()->getVisibleSize();
		const char *strings_check = StringDataManager::getString("friend_check");
		char *check_left=const_cast<char*>(strings_check);

		const char *strings_addfriend = StringDataManager::getString("friend_friend");
		char *addfriend_left=const_cast<char*>(strings_addfriend);	

		const char *strings_private = StringDataManager::getString("chatui_private");
		char *private_left=const_cast<char*>(strings_private);	

		const char *strings_appoint = StringDataManager::getString("family_ListAppoint");
		char *appoint_left=const_cast<char*>(strings_appoint);

		const char *strings_kickOut = StringDataManager::getString("team_leaderKick");
		char *kickOut_left=const_cast<char*>(strings_kickOut);

		const char *strings_black = StringDataManager::getString("friend_black");
		char *black_left=const_cast<char*>(strings_black);

		const char *strings_remove = StringDataManager::getString("family_ListRemove");
		char *remove_left=const_cast<char*>(strings_remove);

		const char *strings_demise = StringDataManager::getString("family_ListDemise");
		char *demise_left=const_cast<char*>(strings_demise);

		const char * Chaneel_normalImage="res_ui/new_button_5.png";
		const char * Channel_selectImage = "res_ui/new_button_5.png";
		const char * Channel_finalImage = "";
		const char * Chaneel_highLightImage="res_ui/new_button_5.png";

		auto familyui =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		opherPost_ =familyui->vectorGuildMemberBase.at(index_)->post();
		otherplayerId_ =familyui->vectorGuildMemberBase.at(index_)->id();
		otherPlayerName_ = familyui->vectorGuildMemberBase.at(index_)->name();
		
		otherCountry = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().country();
		otherViplevel = familyui->vectorGuildMemberBase.at(index_)->viplevel();
		otherLevel = familyui->vectorGuildMemberBase.at(index_)->level();
		otherPression = familyui->vectorGuildMemberBase.at(index_)->profession();

		switch(post_)
		{
		case FAMILYLEADER:
			{
				if (opherPost_ == FAMILYMEMBER)
				{
					/// �鿴 ��� �˽� ���� ��߳ ���ڣ��峤����Ա��
					char * label[] = {check_left,addfriend_left,private_left,appoint_left,kickOut_left,black_left};
					auto friendTab=UITab::createWithText(6,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
					friendTab->setAnchorPoint(Vec2(0,0));
					friendTab->setPosition(Vec2(100,400));
					friendTab->setHighLightImage((char *) Chaneel_highLightImage);
					friendTab->setDefaultPanelByIndex(0);
					friendTab->setAutoClose(true);
					friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FamilyMemberOperator::leaderAndMember));
					friendTab->setPressedActionEnabled(true);
					m_pLayer->addChild(friendTab);

					this->setContentSize(Size(110,240));
				}else
					if (opherPost_ == FAMILYVICELEADER)
					{
						//�鿴 ��� �˽� Ľ� �λ �߳ ���ڣ��峤����峤��
						char * label[] = {check_left,addfriend_left,private_left,remove_left,demise_left,kickOut_left,black_left};
						auto friendTab=UITab::createWithText(7,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
						friendTab->setAnchorPoint(Vec2(0,0));
						friendTab->setPosition(Vec2(100,400));
						friendTab->setHighLightImage((char *) Chaneel_highLightImage);
						friendTab->setDefaultPanelByIndex(0);
						friendTab->setAutoClose(true);
						friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FamilyMemberOperator::leaderAndViceLeader));
						friendTab->setPressedActionEnabled(true);
						m_pLayer->addChild(friendTab);
						this->setContentSize(Size(110,280));
					}
			}break;
		case FAMILYVICELEADER:
			{
				if (opherPost_ == FAMILYMEMBER)
				{
					//�鿴 ��� �˽� ��߳ ���ڣ����峤����Ա��
					char * label[] = {check_left,addfriend_left,private_left,kickOut_left,black_left};
					auto friendTab=UITab::createWithText(5,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
					friendTab->setAnchorPoint(Vec2(0,0));
					friendTab->setPosition(Vec2(100,300));
					friendTab->setHighLightImage((char *) Chaneel_highLightImage);
					friendTab->setDefaultPanelByIndex(0);
					friendTab->setAutoClose(true);
					friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FamilyMemberOperator::viceLeaderAndMember));
					friendTab->setPressedActionEnabled(true);
					m_pLayer->addChild(friendTab);

					this->setContentSize(Size(110,200));
				}else
				if (opherPost_ == FAMILYLEADER)
				{
					//�鿴 ��� �˽� ���ڣ����峤����峤��
					char * label[] = {check_left,addfriend_left,private_left,black_left};
					auto friendTab=UITab::createWithText(4,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
					friendTab->setAnchorPoint(Vec2(0,0));
					friendTab->setPosition(Vec2(100,300));
					friendTab->setHighLightImage((char *) Chaneel_highLightImage);
					friendTab->setDefaultPanelByIndex(0);
					friendTab->setAutoClose(true);
					friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FamilyMemberOperator::viceLeaderAndLeader));
					friendTab->setPressedActionEnabled(true);
					m_pLayer->addChild(friendTab);

					this->setContentSize(Size(110,160));
				}else
					if (opherPost_ == FAMILYVICELEADER)
					{
						//�鿴 ��� �˽� ���ڣ����ϵ���ϣ�
						char * label[] = {check_left,addfriend_left,private_left,black_left};
						auto friendTab=UITab::createWithText(4,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
						friendTab->setAnchorPoint(Vec2(0,0));
						friendTab->setPosition(Vec2(100,300));
						friendTab->setHighLightImage((char *) Chaneel_highLightImage);
						friendTab->setDefaultPanelByIndex(0);
						friendTab->setAutoClose(true);
						friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FamilyMemberOperator::pressionOfMember));
						friendTab->setPressedActionEnabled(true);
						m_pLayer->addChild(friendTab);

						this->setContentSize(Size(110,160));
					}
			}break;
		case FAMILYMEMBER:
			{
				///�鿴 ��� �˽� ���ڣ��峤����ϣ�
				char * label[] = {check_left,addfriend_left,private_left,black_left};
				auto friendTab=UITab::createWithText(4,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
				friendTab->setAnchorPoint(Vec2(0,0));
				friendTab->setPosition(Vec2(100,300));
				friendTab->setHighLightImage((char *) Chaneel_highLightImage);
				friendTab->setDefaultPanelByIndex(0);
				friendTab->setAutoClose(true);
				friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FamilyMemberOperator::pressionOfMember));
				friendTab->setPressedActionEnabled(true);
				m_pLayer->addChild(friendTab);

				this->setContentSize(Size(110,160));
			}break;
		}
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}

void FamilyMemberOperator::onEnter()
{
	UIScene::onEnter();
}

void FamilyMemberOperator::onExit()
{
	UIScene::onExit();
}

void FamilyMemberOperator::leaderAndMember( Ref * obj )
{
	auto tab_ =(UITab *)obj;
	int num_ =tab_->getCurrentIndex();
	//�鿴 ��� �˽� ���� ��߳ ���ڣ��峤����Ա��
	switch(num_)
	{
	case 0:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)otherplayerId_);
			this->removeFromParentAndCleanup(false);
		}break;
	case 1:
		{
			FriendUi::FriendStruct friend_={0,0,otherplayerId_,otherPlayerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend_);
			this->removeFromParentAndCleanup(false);
		}break;
	case 2:
		{
			auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(otherplayerId_,otherPlayerName_,otherCountry,otherViplevel,otherLevel,otherPression);

			this->removeFromParentAndCleanup(false);
		}break;
	case 3:
		{
			const char *strings1_ = StringDataManager::getString("family_isSur");
			const char *strings2_ = StringDataManager::getString("family_renming_is");
			const char *strings3_ = StringDataManager::getString("family_zhanglao_isrenming");
			std::string str_ = strings1_;
			str_.append(strings2_);
			str_.append(otherPlayerName_);
			str_.append(strings3_);
			GameView::getInstance()->showPopupWindow(str_.c_str(),2,this, CC_CALLFUNCO_SELECTOR(FamilyMemberOperator::isSureAppoint), CC_CALLFUNCO_SELECTOR(FamilyMemberOperator::removeSelf));
			this->setVisible(false);
		}break;
	case 4:
		{
			const char *strings1_ = StringDataManager::getString("family_isSur");
			const char *strings2_ = StringDataManager::getString("family_tichu_is");
			const char *strings3_ = StringDataManager::getString("family_isSurLeader_Wenhao_is");
			std::string str_ = strings1_;
			str_.append(strings2_);
			str_.append(otherPlayerName_);
			str_.append(strings3_);
			GameView::getInstance()->showPopupWindow(str_.c_str(),2,this, CC_CALLFUNCO_SELECTOR(FamilyMemberOperator::isSureKickOut), CC_CALLFUNCO_SELECTOR(FamilyMemberOperator::removeSelf));
			this->setVisible(false);
		}break;
	case 5:
		{
			FriendUi::FriendStruct friend_={0,1,otherplayerId_,otherPlayerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend_);
			this->removeFromParentAndCleanup(false);
		}break;
	}
	
}

void FamilyMemberOperator::leaderAndViceLeader( Ref * obj )
{
	auto tab_ =(UITab *)obj;
	int num_ =tab_->getCurrentIndex();
	//�鿴 ��� �˽� Ľ� �λ �߳ ���ڣ��峤����峤��
	switch(num_)
	{
	case 0:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)otherplayerId_);
			this->removeFromParentAndCleanup(false);
		}break;
	case 1:
		{
			FriendUi::FriendStruct friend_={0,0,otherplayerId_,otherPlayerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend_);
			this->removeFromParentAndCleanup(false);
		}break;
	case 2:
		{
			auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(otherplayerId_,otherPlayerName_,otherCountry,otherViplevel,otherLevel,otherPression);
		
			this->removeFromParentAndCleanup(false);
		}break;
	case 3:
		{
			const char *strings1_ = StringDataManager::getString("family_isSur");
			const char *strings2_ = StringDataManager::getString("family_jiechu_is");
			const char *strings3_ = StringDataManager::getString("family_zhanglao_isjiechu");
			std::string str_ = strings1_;
			str_.append(strings2_);
			str_.append(otherPlayerName_);
			str_.append(strings3_);
			GameView::getInstance()->showPopupWindow(str_.c_str(),2,this, CC_CALLFUNCO_SELECTOR(FamilyMemberOperator::isSureRemove), CC_CALLFUNCO_SELECTOR(FamilyMemberOperator::removeSelf));
			this->setVisible(false);
		}break;
	case 4:
		{
			const char *strings1_ = StringDataManager::getString("family_isSur");
			const char *strings2_ = StringDataManager::getString("family_chuanwei_is");
			const char *strings3_ = StringDataManager::getString("family_zuzhang_ischuanwei");
			std::string str_ = strings1_;
			str_.append(strings2_);
			str_.append(otherPlayerName_);
			str_.append(strings3_);
			GameView::getInstance()->showPopupWindow(str_.c_str(),2,this, CC_CALLFUNCO_SELECTOR(FamilyMemberOperator::isSureDemis), CC_CALLFUNCO_SELECTOR(FamilyMemberOperator::removeSelf));
			this->setVisible(false);
		}break;
	case 5:
		{
			const char *strings1_ = StringDataManager::getString("family_isSur");
			const char *strings2_ = StringDataManager::getString("family_tichu_is");
			const char *strings3_ = StringDataManager::getString("family_isSurLeader_Wenhao_is");
			std::string str_ = strings1_;
			str_.append(strings2_);
			str_.append(otherPlayerName_);
			str_.append(strings3_);
			GameView::getInstance()->showPopupWindow(str_.c_str(),2,this, CC_CALLFUNCO_SELECTOR(FamilyMemberOperator::isSureKickOut), CC_CALLFUNCO_SELECTOR(FamilyMemberOperator::removeSelf));
			this->setVisible(false);
		}break;
	case 6:
		{
			FriendUi::FriendStruct friend_={0,1,otherplayerId_,otherPlayerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend_);
			this->removeFromParentAndCleanup(false);
		}break;
	}
}

void FamilyMemberOperator::viceLeaderAndMember( Ref * obj )
{
	auto tab_ =(UITab *)obj;
	int num_ =tab_->getCurrentIndex();
	//�鿴 ��� �˽� ��߳ ���ڣ����峤����Ա��
	switch(num_)
	{
	case 0:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)otherplayerId_);
			this->removeSelf(NULL);
		}break;
	case 1:
		{
			FriendUi::FriendStruct friend_={0,0,otherplayerId_,otherPlayerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend_);
			this->removeSelf(NULL);
		}break;
	case 2:
		{
			auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(otherplayerId_,otherPlayerName_,otherCountry,otherViplevel,otherLevel,otherPression);
			
			this->removeSelf(NULL);
			
		}break;
	case 3:
		{
			const char *strings1_ = StringDataManager::getString("family_isSur");
			const char *strings2_ = StringDataManager::getString("family_tichu_is");
			const char *strings3_ = StringDataManager::getString("family_isSurLeader_Wenhao_is");
			std::string str_ = strings1_;
			str_.append(strings2_);
			str_.append(otherPlayerName_);
			str_.append(strings3_);
			GameView::getInstance()->showPopupWindow(str_.c_str(),2,this, CC_CALLFUNCO_SELECTOR(FamilyMemberOperator::isSureKickOut), CC_CALLFUNCO_SELECTOR(FamilyMemberOperator::removeSelf));
			this->setVisible(false);
		}break;
	case 4:
		{
			FriendUi::FriendStruct friend_={0,1,otherplayerId_,otherPlayerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend_);
			this->removeFromParentAndCleanup(false);
		}break;
	}
}

void FamilyMemberOperator::viceLeaderAndLeader( Ref * obj )
{
	auto tab_ =(UITab *)obj;
	int num_ =tab_->getCurrentIndex();
	//�鿴 ��� �˽� ���ڣ����峤����峤��
	switch(num_)
	{
	case 0:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)otherplayerId_);
		}break;
	case 1:
		{
			FriendUi::FriendStruct friend_={0,0,otherplayerId_,otherPlayerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend_);
		}break;
	case 2:
		{
			auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(otherplayerId_,otherPlayerName_,otherCountry,otherViplevel,otherLevel,otherPression);
		}break;
	case 3:
		{
			FriendUi::FriendStruct friend_={0,1,otherplayerId_,otherPlayerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend_);
		}break;
	}
	this->removeSelf(NULL);
}

void FamilyMemberOperator::pressionOfMember( Ref * obj )
{
	auto tab_ =(UITab *)obj;
	int num_ =tab_->getCurrentIndex();
	///�鿴 ��� �˽� ���ڣ��峤����ϣ�
	switch(num_)
	{
	case 0:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)otherplayerId_);
		}break;
	case 1:
		{
			FriendUi::FriendStruct friend_={0,0,otherplayerId_,otherPlayerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend_);
		}break;
	case 2:
		{
			auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(otherplayerId_,otherPlayerName_,otherCountry,otherViplevel,otherLevel,otherPression);
			
		}break;
	case 3:
		{
			FriendUi::FriendStruct friend_={0,1,otherplayerId_,otherPlayerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend_);
		}break;
	}

	this->removeFromParentAndCleanup(false);
}

bool FamilyMemberOperator::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return this->resignFirstResponder(pTouch,this,false,false);
}

void FamilyMemberOperator::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void FamilyMemberOperator::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void FamilyMemberOperator::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void FamilyMemberOperator::isSureKickOut( Ref * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1508,(void*)1,(void *)otherplayerId_);
	this->removeFromParentAndCleanup(false);
}

void FamilyMemberOperator::isSureDemis( Ref * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1508,(void*)4,(void *)otherplayerId_);
	this->removeFromParentAndCleanup(false);
}

void FamilyMemberOperator::isSureAppoint( Ref * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1508,(void*)2,(void *)otherplayerId_);
	this->removeFromParentAndCleanup(false);
}

void FamilyMemberOperator::isSureRemove( Ref * obj )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1508,(void*)3,(void *)otherplayerId_);
	this->removeFromParentAndCleanup(false);
}

void FamilyMemberOperator::removeSelf( Ref * obj )
{
	this->removeFromParentAndCleanup(true);
}
