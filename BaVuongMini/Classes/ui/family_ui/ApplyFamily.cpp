#include "ApplyFamily.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CGuildBase.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../extensions/CCRichLabel.h"
#include "../../ui/Rank_ui/RankUI.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "cocostudio/CCSGUIReader.h"

#define  SelectImageTag 458

ApplyFamily::ApplyFamily(void)
{
	selfGuildId =-10;
	curPage = 0;
	hasNextPage =false;
}


ApplyFamily::~ApplyFamily(void)
{
	std::vector<CGuildBase *>::iterator iter;
	for (iter=vectorGuildBase.begin();iter!=vectorGuildBase.end();iter++)
	{
		delete * iter;
	}
	vectorGuildBase.clear();
}

ApplyFamily * ApplyFamily::create()
{
	auto family_=new ApplyFamily();
	if (family_ && family_->init())
	{
		family_->autorelease();
		return family_;
	}
	CC_SAFE_DELETE(family_);
	return NULL;
}

bool ApplyFamily::init()
{
	if (UIScene::init())
	{
		Size winsize= Director::getInstance()->getVisibleSize();
		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		if(LoadSceneLayer::familyApplyPanel->getParent() != NULL)
		{
			LoadSceneLayer::familyApplyPanel->removeFromParentAndCleanup(false);
		}

		auto mainPanel=LoadSceneLayer::familyApplyPanel;
		mainPanel->setAnchorPoint(Vec2(0.5f,0.5f));
		mainPanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(mainPanel);

		auto loadLayer= Layer::create();
		loadLayer->setIgnoreAnchorPointForPosition(false);
		loadLayer->setAnchorPoint(Vec2(0.5f,0.5f));
		loadLayer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		loadLayer->setContentSize(Size(800,480));
		this->addChild(loadLayer);


		const char * secondStr = StringDataManager::getString("chat_diaoshi_family_jia");
		const char * thirdStr = StringDataManager::getString("chat_diaoshi_family_zu");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		loadLayer->addChild(atmature);

		tableviewstate = TableView::create(this, Size(650,360));
		//tableviewstate->setSelectedEnable(true);
		//tableviewstate->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 24, 1, 1), Vec2(0,0));
		//tableviewstate->setPressedActionEnabled(true);
		tableviewstate->setDirection(TableView::Direction::VERTICAL);
		tableviewstate->setAnchorPoint(Vec2(0,0));
		tableviewstate->setPosition(Vec2(85,45));
		tableviewstate->setDelegate(this);
		tableviewstate->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		loadLayer->addChild(tableviewstate);
		tableviewstate->reloadData();

		auto btnClose =(Button *)Helper::seekWidgetByName(mainPanel,"Button_close");
		btnClose->setTouchEnabled(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addTouchEventListener(CC_CALLBACK_2(ApplyFamily::callBackExit, this));


		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);


		return true;
	}
	return false;
}

void ApplyFamily::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ApplyFamily::onExit()
{
	UIScene::onExit();
}

void ApplyFamily::callBackExit(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void ApplyFamily::callBackCheck( Ref * obj )
{
	CCLOG("check  1504");
	auto menu_ =(MenuItemSprite *)obj;
	int tag_ =menu_->getTag();

	std::string familyName = vectorGuildBase.at(tag_)->name();
	std::string familyLeaderName = vectorGuildBase.at(tag_)->leadername();
	int familyCurMember  =vectorGuildBase.at(tag_)->currmembernumber();
	int familyMaxMember  = vectorGuildBase.at(tag_)->maxmembernumber();
	long long familyId = vectorGuildBase.at(tag_)->id();
	std::string familyNotice = vectorGuildBase.at(tag_)->declaration();

	char curmemberStr[10];
	sprintf(curmemberStr,"%d",familyCurMember);
	char maxMemberStr[10];
	sprintf(maxMemberStr,"%d",familyMaxMember);
	std::string memberStr_ ="";
	memberStr_.append(curmemberStr);
	memberStr_.append("/");
	memberStr_.append(maxMemberStr);

	Size winsize= Director::getInstance()->getVisibleSize();
	auto checkFamily =ApplyChackFamily::create(familyName,familyLeaderName,familyId,memberStr_.c_str(),familyNotice.c_str());
	checkFamily->setIgnoreAnchorPointForPosition(false);
	checkFamily->setAnchorPoint(Vec2(0.5,0.5f));
	checkFamily->setPosition(Vec2(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(checkFamily);
}


void ApplyFamily::callBackApply( Ref * obj )
{
	auto menu_ =(MenuItemSprite *)obj;
	int tag_ =menu_->getTag();

	if (selfGuildId == vectorGuildBase.at(tag_)->id())
	{
		return;
	}
	
	for (int i =0; i< vectorGuildBase.size();i++)
	{
		if (selfGuildId == vectorGuildBase.at(i)->id())
		{
			selfGuildId = vectorGuildBase.at(tag_)->id();
			tableviewstate->updateCellAtIndex(i);
		}
	}
	long long familyId =vectorGuildBase.at(tag_)->id();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1502,(void *)familyId);
	selfGuildId = familyId;
	tableviewstate->updateCellAtIndex(tag_);
// 	TableViewCell* cell = tableviewstate->cellAtIndex(tag_);
// 	tableviewstate->addSelectedFlagForCell(cell);
}

void ApplyFamily::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{

}


void ApplyFamily::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

void ApplyFamily::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void ApplyFamily::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void ApplyFamily::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void ApplyFamily::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	int  cellidx = cell->getIdx();

}

cocos2d::Size ApplyFamily::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(645,60);
}

cocos2d::extension::TableViewCell* ApplyFamily::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	__String * string =__String::createWithFormat("%d",idx);	
	auto cell =table->dequeueCell();
	cell=new TableViewCell();
	cell->autorelease();

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	////��Ҫ��ҳ
	auto spbg = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
	spbg->setPreferredSize(Size(645,60));
	spbg->setCapInsets(Rect(15,30,1,1));
	spbg->setAnchorPoint(Vec2(0,0));
	spbg->setPosition(Vec2(0,0));
	cell->addChild(spbg);
	
	std::string familyName = vectorGuildBase.at(idx)->name();
	auto labelName= Label::createWithTTF(familyName.c_str(), APP_FONT_NAME,16);
	labelName->setAnchorPoint(Vec2(0,0));
	labelName->setPosition(Vec2(15,25));
	cell->addChild(labelName);

	std::string familyLeaderName = vectorGuildBase.at(idx)->leadername();
	auto labelManage= Label::createWithTTF(familyLeaderName.c_str(), APP_FONT_NAME,16);
	labelManage->setAnchorPoint(Vec2(0,0));
	labelManage->setPosition(Vec2(125,25));
	cell->addChild(labelManage);

	int familyLevel = vectorGuildBase.at(idx)->level();
	char levelStr[5];
	sprintf(levelStr,"%d",familyLevel);
	auto labelLevel= Label::createWithTTF(levelStr, APP_FONT_NAME,16);
	labelLevel->setAnchorPoint(Vec2(0,0));
	labelLevel->setPosition(Vec2(230,25));
	cell->addChild(labelLevel);

	int familyCurNum = vectorGuildBase.at(idx)->currmembernumber();
	int familyMaxNum = vectorGuildBase.at(idx)->maxmembernumber();

	char familyCurMember[10];
	sprintf(familyCurMember,"%d",familyCurNum);
	char familyMaxMember[10];
	sprintf(familyMaxMember,"%d",familyMaxNum);
	
	std::string showFamilyMember =familyCurMember;
	showFamilyMember.append("/");
	showFamilyMember.append(familyMaxMember);

	auto labelNumber= Label::createWithTTF(showFamilyMember.c_str(), APP_FONT_NAME,16);
	labelNumber->setAnchorPoint(Vec2(0,0));
	labelNumber->setPosition(Vec2(310,25));
	cell->addChild(labelNumber);
	
	auto bg_check = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_1.png");
	bg_check->setPreferredSize(Size(80,36));
	bg_check->setCapInsets(Rect(18,9,2,23));
	auto btn_check = MenuItemSprite::create(bg_check, bg_check, bg_check, CC_CALLBACK_1(ApplyFamily::callBackCheck,this));
	btn_check->setScale(0.75f);
	btn_check->setTag(idx);
	auto menu_check=CCMoveableMenu::create(btn_check,NULL);
	menu_check->setAnchorPoint(Vec2(0,0));
	menu_check->setPosition(Vec2(450,33));
	cell->addChild(menu_check);
	const char *strings_check = StringDataManager::getString("family_apply_btncheck");
	auto label_check= Label::createWithTTF(strings_check,APP_FONT_NAME,16);
	label_check->setAnchorPoint(Vec2(0.5f,0.5f));
	label_check->setPosition(Vec2(bg_check->getContentSize().width/2,bg_check->getContentSize().height/2));
	btn_check->addChild(label_check);

	long long guildId_ = vectorGuildBase.at(idx)->id();

	const char *strings_apply;
	if (selfGuildId == guildId_)
	{
		strings_apply = StringDataManager::getString("family_apply_btnapply_already");
	}else
	{
		strings_apply = StringDataManager::getString("family_apply_btnapply");
	}

	char * str_apply=const_cast<char*>(strings_apply);
	auto bg_apply = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_2.png");
	bg_apply->setPreferredSize(Size(80,36));
	bg_apply->setCapInsets(Rect(18,9,2,23));
	auto btn_apply = MenuItemSprite::create(bg_apply, bg_apply, bg_apply, CC_CALLBACK_1(ApplyFamily::callBackApply,this));
	btn_apply->setTag(idx);
	btn_apply->setScale(0.75f);
	auto menu_apply=CCMoveableMenu::create(btn_apply,NULL);
	menu_apply->setAnchorPoint(Vec2(0,0));
	menu_apply->setPosition(Vec2(550,33));
	cell->addChild(menu_apply);	
	auto label_apply= Label::createWithTTF(str_apply,APP_FONT_NAME,16);
	label_apply->setAnchorPoint(Vec2(0.5f,0.5f));
	label_apply->setPosition(Vec2(bg_apply->getContentSize().width/2,bg_apply->getContentSize().height/2));
	label_apply->setTag(idx);
	btn_apply->addChild(label_apply);

	if(idx == vectorGuildBase.size()-3)
	{
		if (hasNextPage == true)
		{
			//req for next
			curPage++;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1501,(void *)curPage);
		}
	}
	return cell;
}

ssize_t ApplyFamily::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	return vectorGuildBase.size();
}

void ApplyFamily::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void ApplyFamily::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void ApplyFamily::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

/////////////////////////////////////////////

ApplyChackFamily::ApplyChackFamily( void )
{

}

ApplyChackFamily::~ApplyChackFamily( void )
{

}

ApplyChackFamily * ApplyChackFamily::create(std::string name,std::string leaderName,long long familyId,std::string membernum,std::string familyNotice)
{
	auto checkfamily =new ApplyChackFamily();
	if (checkfamily && checkfamily->init(name,leaderName,familyId,membernum,familyNotice))
	{
		checkfamily->autorelease();
		return checkfamily;
	}
	CC_SAFE_DELETE(checkfamily);
	return NULL;
}

bool ApplyChackFamily::init(std::string name,std::string leaderName,long long familyId,std::string membernum,std::string familyNotice)
{
	if (UIScene::init())
	{
		Size size= Director::getInstance()->getVisibleSize();
		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(size);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		auto ppanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/seeFamily_1.json");
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->setPosition(Vec2(size.width/2,size.height/2));
		m_pLayer->addChild(ppanel);

		auto loadLayer= Layer::create();
		loadLayer->setIgnoreAnchorPointForPosition(false);
		loadLayer->setAnchorPoint(Vec2(0.5f,0.5f));
		loadLayer->setPosition(Vec2(size.width/2,size.height/2));
		loadLayer->setContentSize(Size(600,360));
		this->addChild(loadLayer);

		auto labelFamilyName =(Text*)Helper::seekWidgetByName(ppanel,"Label_name_value");
		auto labelFamilyLeaderName =(Text*)Helper::seekWidgetByName(ppanel,"Label_sedname_value");
		auto labelFamilyMemberNum =(Text*)Helper::seekWidgetByName(ppanel,"Label_NumberOfPeople_value");
		labelFamilyName->setString(name.c_str());
		labelFamilyLeaderName->setString(leaderName.c_str());
		labelFamilyMemberNum->setString(membernum.c_str());

		if (strlen(familyNotice.c_str()) <= 0)
		{
			const char * familyWriteIsEmpty  = StringDataManager::getString("familyMangerIsNotWrite");
			familyNotice.append(familyWriteIsEmpty);
		}

		auto familyNoticeLabel = CCRichLabel::createWithString(familyNotice.c_str(), Size(360, 0), NULL, NULL, 0, 20.f, 1);
		familyNoticeLabel->setAnchorPoint(Vec2(0,0));
		familyNoticeLabel->setPosition(Vec2(0,0));

		auto noticeScrollView = cocos2d::extension::ScrollView::create(Size(360,115));
		noticeScrollView->setContentSize(Size(360,115));
		noticeScrollView->setIgnoreAnchorPointForPosition(false);
		noticeScrollView->setTouchEnabled(true);
		noticeScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		noticeScrollView->setAnchorPoint(Vec2(0.5f,0));
		noticeScrollView->setPosition(Vec2(348,110));
		noticeScrollView->setBounceable(true);
		noticeScrollView->setClippingToBounds(true);
		loadLayer->addChild(noticeScrollView);	
		noticeScrollView->addChild(familyNoticeLabel);

		int h_ =familyNoticeLabel->getContentSize().height;
		if (h_ < noticeScrollView->getContentSize().height)
		{
			h_ =noticeScrollView->getContentSize().height;
		}

		noticeScrollView->setContentSize(Size(360,h_));
		familyNoticeLabel->setPosition(Vec2(0,noticeScrollView->getContentSize().height - familyNoticeLabel->getContentSize().height));


		auto applyFamily =(Button *)Helper::seekWidgetByName(ppanel,"Button_enter");
		applyFamily->setPressedActionEnabled(true);
		applyFamily->setTouchEnabled(true);
		applyFamily->setTag(familyId);
		applyFamily->addTouchEventListener(CC_CALLBACK_2(ApplyChackFamily::callBackApply, this));

		auto closeButton =(Button *)Helper::seekWidgetByName(ppanel,"Button_close");
		closeButton->setPressedActionEnabled(true);
		closeButton->setTouchEnabled(true);
		closeButton->addTouchEventListener(CC_CALLBACK_2(ApplyChackFamily::callBack, this));

		////setTouchEnabled(true);
		//setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		setContentSize(size);
		return true;
	}
	return false;
}

void ApplyChackFamily::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ApplyChackFamily::onExit()
{
	UIScene::onExit();
}

void ApplyChackFamily::callBack(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void ApplyChackFamily::callBackApply(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto button = (Button*)pSender;
		long long familyId_ = button->getTag();
		auto applyFamily = (ApplyFamily *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagApplyFamilyUI);
		if (applyFamily != NULL)
		{
			if (applyFamily->selfGuildId == familyId_)
			{
				return;
			}
			int cellIndex_ = -1;

			for (int i = 0; i< applyFamily->vectorGuildBase.size(); i++)
			{
				if (applyFamily->selfGuildId == applyFamily->vectorGuildBase.at(i)->id())
				{
					applyFamily->selfGuildId = familyId_;
					applyFamily->tableviewstate->updateCellAtIndex(i);
				}

				if (familyId_ == applyFamily->vectorGuildBase.at(i)->id())
				{
					cellIndex_ = i;
				}
			}
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1502, (void *)familyId_);
			if (cellIndex_ >= 0)
			{
				applyFamily->selfGuildId = familyId_;
				applyFamily->tableviewstate->updateCellAtIndex(cellIndex_);
			}
		}

		auto rankui_ = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
		if (rankui_ != NULL)
		{
			if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionid() > 0)
			{
				std::string strings_ = StringDataManager::getString("rank_youhaveFamily");
				GameView::getInstance()->showAlertDialog(strings_.c_str());
			}
			else
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1502, (void *)familyId_);
				//std::string strings_  = StringDataManager::getString("rank_applyFamilyIssend");
				//GameView::getInstance()->showAlertDialog(strings_.c_str());
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}
