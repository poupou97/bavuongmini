#ifndef FAMILY_FAMILYMANAGE_OPERATORLEADER_H 
#define FAMILY_FAMILYMANAGE_OPERATORLEADER_H

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class FamilyOperatorOfLeader:public UIScene
{
public:
	FamilyOperatorOfLeader(void);
	~FamilyOperatorOfLeader(void);

	static FamilyOperatorOfLeader *create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
// 	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
// 	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
// 	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	//��� ��� ��ټ� ��ɢ
	void callBackConvene(Ref *pSender, Widget::TouchEventType type);
	void callBackCheck(Ref *pSender, Widget::TouchEventType type);
	void callBackCallTogether(Ref *pSender, Widget::TouchEventType type);
	void callBackDismiss(Ref * obj);
	void callBackDismissSure(Ref *pSender, Widget::TouchEventType type);
	void callBackClose(Ref * obj);

	Size winsize;
	Button * button_check;
};

#include "../extensions/RichTextInput.h"
class FamilyChangeNotice:public UIScene
{
public:
	FamilyChangeNotice();
	~FamilyChangeNotice();

	static FamilyChangeNotice *create(int type);
	bool init(int type);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);

	void callBackSure(Ref *pSender, Widget::TouchEventType type);
	void callBackCancel(Ref *pSender, Widget::TouchEventType type);
public:
	RichTextInputBox * textFamilyNotice;
};

#endif;