#ifndef _UI_QUESTION_QUESTIONDATA_H_
#define _UI_QUESTION_QUESTIONDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ����ģ�飨���ڱ��������������ݣ�
 * @author liuliang
 * @version 0.1.0
 * @date 2014.05.13
 */

class QuestionData
{
public:
	static QuestionData * s_questionData;
	static QuestionData * instance();

private:
	QuestionData(void);
	~QuestionData(void);

private:
	int m_nAllQuestion;															// ÿ�տɴ���������ܴ�����
	int m_nHasAnswerQuestion;												// �ѻش����
	int m_nRightAnswerQuestion;											// �Ѿ���Ե���Ŀ��Ŀ
	int m_nCardNum;																// �ɷ��ƵĴ���

	int m_nMoney;																	// ��Ǯ
	int m_nExp;																		// ����
	std::string m_strQuestionTitle;											// ��Ŀ����

	int m_nResult;																	// ������
	bool m_bIsGetedResult;													// �Ƿ��Ѿ��յ����������صĴ�����

	int m_nReward_index;														//�������Ʊ��

public:
	std::vector<std::string>	m_vector_questionContent;		// ��Ŀ����

public:
	void set_allQuestion(int nAllQuestion);
	int get_allQuestion();

	void set_hasAnswerQuestion(int nHasAnswerQuestion);
	int get_hasAnswerQuestion();

	void set_rightAnswerQuestion(int nRightAnswerQuestion);
	int get_rightAnswerQuestion();

	void set_cardNum(int nCardNum);
	int get_cardNum();

	void set_questionTitle(std::string strQuestionTitle);
	std::string get_questionTitle();

	void set_money(int nMoney);
	int get_money();

	void set_exp(int nExp);
	int get_exp();

	void set_result(int nResult);
	int get_result();

	void clearData();

	void set_isGetedResult(bool isGeted);
	bool get_isGetedResult();

	void set_reward_index(int idx);
	int get_reward_index();
};

#endif

