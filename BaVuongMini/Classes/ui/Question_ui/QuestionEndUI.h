#ifndef _UI_QUESTION_QUESTIONENDUI_H_
#define _UI_QUESTION_QUESTIONENDUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../skillscene/MusouSkillTalent.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CPropReward;

#define  kTag_Base_RewardCardItem 100

/////////////////////////////////
/**
 * ���������콱UI
 * @author yangjun
 * @version 0.1.0
 * @date 2014.05.19
 */

class QuestionEndUI : public UIScene
{
public:
	QuestionEndUI();
	~QuestionEndUI();

	static QuestionEndUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void update(float delta);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	void initReward(std::vector<CPropReward*> tempList);

	//�������ƶ��������
	void AllCardTurnToBack();
	//濪ʼϴ�
	void BeginShuffle();

	void set_RemainNumber(int _value);
	int get_RemainNumber();

public:
	Layer *u_layer;
private:
	
	std::vector<Coordinate> Coordinate_item;

	//�ʣ��ʱ�
	Text * l_remainTime;
	int remainTime;

	//�ʣ����
	Text * l_remainNumber;
	int remainNumber;
};

#endif
