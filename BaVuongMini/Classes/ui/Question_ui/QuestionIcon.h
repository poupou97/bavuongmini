#ifndef _UI_QUESTION_QUESTIONICON_H_
#define _UI_QUESTION_QUESTIONICON_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;
/////////////////////////////////
/**
 * ǩ��Icon
 * @author liuliang
 * @version 0.1.0
 * @date 2014.05.13
 */


class CCTutorialParticle;

class QuestionIcon:public UIScene
{
public:
	QuestionIcon(void);
	~QuestionIcon(void);

public:
	static QuestionIcon* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

public:
	Button * m_btn_questionIcon;					// �ɵ��ICON
	Layer * m_layer_question;							// ���Ҫ�õ��layer
	Label * m_label_questionIcon_text;			// 	ICON��·���ʾ�����	
	cocos2d::extension::Scale9Sprite * m_spirte_fontBg;				// �������ʾ�ĵ�ͼ

public:
	void callBackBtnQuestion(Ref *pSender, Widget::TouchEventType type);
	
};

#endif

