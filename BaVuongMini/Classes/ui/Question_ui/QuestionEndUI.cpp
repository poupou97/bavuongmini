#include "QuestionEndUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CPropReward.h"
#include "QuestionData.h"
#include "QuestionRewardCardItem.h"
#include "QuestionUI.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../GameUIConstant.h"
#include "cocostudio\CCSGUIReader.h"

QuestionEndUI::QuestionEndUI():
remainTime(30)
{
	//coordinate for card
	for (int m = 1;m>=0;m--)
	{
		for (int n = 4;n>=0;n--)
		{
			int _m = 1-m;
			int _n = 4-n;

			Coordinate temp;
			temp.x = 145+_n*94 ;
			temp.y = 185+m*119;

			Coordinate_item.push_back(temp);
		}
	}
}


QuestionEndUI::~QuestionEndUI()
{
}

QuestionEndUI * QuestionEndUI::create()
{
	auto questionEndUI = new QuestionEndUI();
	if (questionEndUI && questionEndUI->init())
	{
		return questionEndUI;
	}
	CC_SAFE_DELETE(questionEndUI);
	return NULL;
}

bool QuestionEndUI::init()
{
	if(UIScene::init())
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		
		remainNumber = QuestionData::instance()->get_cardNum();
		//���UI
// 		if(LoadSceneLayer::GeneralsFateInfoLayer->getWidgetParent() != NULL)
// 		{
// 			LoadSceneLayer::GeneralsFateInfoLayer->removeFromParentAndCleanup(false);
// 		}
// 		ppanel = LoadSceneLayer::GeneralsFateInfoLayer;
		auto ppanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/takeCard_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(588, 405));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto btn_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnabled( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(QuestionEndUI::CloseEvent, this));

		l_remainTime = (Text*)Helper::seekWidgetByName(ppanel,"Label_timeValue");
		std::string str_remainTime = "";
		char s_remainTime[20];
		sprintf(s_remainTime,"%d",remainTime);
		str_remainTime.append(s_remainTime);
		str_remainTime.append("s");
		l_remainTime->setString(str_remainTime.c_str());

		l_remainNumber = (Text*)Helper::seekWidgetByName(ppanel,"Label_numberValue");
		char s_remainNumber[20];
		sprintf(s_remainNumber,"%d",remainNumber);
		l_remainNumber->setString(s_remainNumber);

		u_layer = Layer::create();
		addChild(u_layer);

		this->schedule(schedule_selector(QuestionEndUI::update),1.0f);

		this->setContentSize(Size(588,405));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(QuestionEndUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(QuestionEndUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(QuestionEndUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(QuestionEndUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void QuestionEndUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void QuestionEndUI::onExit()
{
	UIScene::onExit();
}

bool QuestionEndUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void QuestionEndUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void QuestionEndUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void QuestionEndUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void QuestionEndUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto questionUI = (QuestionUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
		if (questionUI)
			questionUI->closeAnim();

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}



void QuestionEndUI::AllCardTurnToBack()
{
	for(int i = 0;i<12;++i)
	{
		if (u_layer->getChildByTag(kTag_Base_RewardCardItem +i))
		{
			auto rewardCardItem = (QuestionRewardCardItem*)u_layer->getChildByTag(kTag_Base_RewardCardItem +i);
			rewardCardItem->FlopAnimationToBack();
		}
	}
}

void QuestionEndUI::BeginShuffle()
{
	Vec2 _pos = Vec2(333,245);

	for(int i = 0;i<12;++i)
	{
		if (u_layer->getChildByTag(kTag_Base_RewardCardItem +i))
		{
			auto rewardCardItem = (QuestionRewardCardItem*)u_layer->getChildByTag(kTag_Base_RewardCardItem +i);
			auto  action1 = Sequence::create(
				MoveTo::create(REWARD_CARD_MOVE_TIME,_pos),
				DelayTime::create(0.5f),
				//MoveTo::create(0.7f,Vec2(Coordinate_card.at(random_value).x,Coordinate_card.at(random_value).y)),
				MoveTo::create(REWARD_CARD_MOVE_TIME,Vec2(Coordinate_item.at(i).x,Coordinate_item.at(i).y)),
				CallFunc::create(CC_CALLBACK_0(QuestionRewardCardItem::setBtnTouchEnabled, rewardCardItem)),
				NULL);
			rewardCardItem->runAction(action1);
		}
	}
}

void QuestionEndUI::initReward(std::vector<CPropReward*> tempList)
{
	for(int i = 0;i<tempList.size();++i)
	{
		if (i >= 10)
			continue;

		auto rewardCardItem = QuestionRewardCardItem::create(tempList.at(i),i);
		rewardCardItem->setIgnoreAnchorPointForPosition(false);
		rewardCardItem->setAnchorPoint(Vec2(0.5f,0.5f));
		rewardCardItem->setPosition(Vec2(Coordinate_item.at(i).x,Coordinate_item.at(i).y));
		rewardCardItem->setTag(kTag_Base_RewardCardItem+i);
		u_layer->addChild(rewardCardItem);
	}

	auto action1 = Sequence::create(
		DelayTime::create(2.0f),
		CallFunc::create(CC_CALLBACK_0(QuestionEndUI::AllCardTurnToBack,this)),
		DelayTime::create(0.7f),
		CallFunc::create(CC_CALLBACK_0(QuestionEndUI::BeginShuffle,this)),
		NULL);

	this->stopAllActions();
	this->runAction(action1);
}

void QuestionEndUI::update( float delta )
{
	if (remainTime >0)
	{
		--remainTime;

		std::string str_remainTime = "";
		char s_remainTime[20];
		sprintf(s_remainTime,"%d",remainTime);
		str_remainTime.append(s_remainTime);
		str_remainTime.append("s");
		l_remainTime->setString(str_remainTime.c_str());

		char s_remainNumber[20];
		sprintf(s_remainNumber,"%d",remainNumber);
		l_remainNumber->setString(s_remainNumber);
	}
	else
	{
		//GameMessageProcessor::sharedMsgProcessor()->sendReq(1924);
		auto questionUI = (QuestionUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
		if (questionUI)
			questionUI->closeAnim();

		this->closeAnim();
		
	}
}

void QuestionEndUI::set_RemainNumber( int _value )
{
	remainNumber = _value;
}

int QuestionEndUI::get_RemainNumber()
{
	return remainNumber;
}
