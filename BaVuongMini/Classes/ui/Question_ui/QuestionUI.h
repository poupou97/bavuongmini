#ifndef _UI_QUESTION_QUESTIONUI_H_
#define _UI_QUESTION_QUESTIONUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ���UI
 * @author liuliang
 * @version 0.1.0
 * @date 2014.05.13
 */

class QuestionUI:public UIScene
{
public:
	QuestionUI(void);
	~QuestionUI(void);

public:
	static Layout * s_pPanel;
	static QuestionUI* create();
	bool init();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);

	virtual void onEnter();
	virtual void onExit();

private:
	Layer * m_base_layer;																							// ��layer

	Text * m_label_questionTitle;																				// �������
	Text * m_label_expValue;																						// ⾭��ֵ
	Text * m_label_conisValue;																					// ��Ǯ
	Text * m_label_rightAnswerValue;																			// �ۼƴ�����
	Text * m_label_hasAnswerValue;																			// ��Ѵ����(��Ѵ���� / �ÿ�մ�������ܴ��)
	Text * m_label_cardNumValue;																				// �ɷ��ƴ��

	Button * m_btn_a;																								// btn1	A
	Button * m_btn_b;																								// btn2	B
	Button * m_btn_c;																								// btn3	C
	Button * m_btn_d;																								// btn4	D

	Text * m_label_btn1_text;																					// btn1 A text
	Text * m_label_btn2_text;																					// btn2 B text
	Text * m_label_btn3_text;																					// btn3 C text
	Text * m_label_btn4_text;																					// btn4 D text

	ImageView * imageView_trueOption;

public:
	void callBackBtnClolse(Ref *pSender, Widget::TouchEventType type);																	// �رհ�ť����Ӧ
	void callBackBtnQuestion(Ref *pSender, Widget::TouchEventType type);																	// ���ⰴť����Ӧ

	void requestQuestion();																								// ��������������
	void initQuestionInfoFromServer();																			// �ȡ�÷�������ݳ�ʼ��UI����Ŀ��
	void initBaseInfoFromServer();																					// ȡ�÷�������ݳ�ʼ��UI����¼ʱ�������͵���Push2801ݣ�

	void HandleResult(int lastResult,int lastTrueOption);
	void reqNextQuestion();
	void setTrueOptionPos(int index);
private:
	void initUI();

private:
	typedef enum TagBtnQuestion
	{
		TAG_BTN_QUE_1 = 101,
		TAG_BTN_QUE_2 = 102,
		TAG_BTN_QUE_3 = 103,
		TAG_BTN_QUE_4 = 104
	};
};

#endif

