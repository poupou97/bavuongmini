
#ifndef _QUESTIONUI_REWARDCARDITEM_H_
#define _QUESTIONUI_REWARDCARDITEM_H_

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CPropReward;
class GoodsInfo;

/////////////////////////////////
/**
 * ���������콱UI�еĿ����ֻ���ڿ����콱��
 * @author yangjun
 * @version 0.1.0
 * @date 2014.05.19
 */

class QuestionRewardCardItem : public UIScene
{
public:
	QuestionRewardCardItem();
	~QuestionRewardCardItem();

	static QuestionRewardCardItem * create(CPropReward *rewardProp,int index);
	bool init(CPropReward *rewardProp,int index);

	void BackCradEvent(Ref *pSender, Widget::TouchEventType type);
	void FrontCradEvent(Ref *pSender, Widget::TouchEventType type);

	void FlopAnimationToFront();
	void FlopAnimationToBack();

	void setBtnTouchEnabled();

	void RefreshCardInfo(GoodsInfo *goodsInfo,int amount);

private:
	Button * btn_backCardFrame;
	Button * btn_frontCardFrame;
	ImageView * imageView_frame;
	ImageView * imageView_icon;
	Label * l_rewardName;
	Label * l_roleName;

	//����λ�
	int grid;
	//õ�ǰ��ʾ�����(true)滹�Ǳ��(false)
	bool isFrontOrBack;
	//��Ƿ��Ѿ�����
	bool isFloped;
};

#endif