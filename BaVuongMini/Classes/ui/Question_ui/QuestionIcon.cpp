#include "QuestionIcon.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "QuestionUI.h"
#include "AppMacros.h"

#define  TAG_TUTORIALPARTICLE 600

QuestionIcon::QuestionIcon(void):
m_btn_questionIcon(NULL)
,m_layer_question(NULL)
,m_label_questionIcon_text(NULL)
,m_spirte_fontBg(NULL)
{

}


QuestionIcon::~QuestionIcon(void)
{
	
}

QuestionIcon* QuestionIcon::create()
{
	auto questionIcon = new QuestionIcon();
	if (questionIcon && questionIcon->init())
	{
		questionIcon->autorelease();
		return questionIcon;
	}
	CC_SAFE_DELETE(questionIcon);
	return NULL;
}

bool QuestionIcon::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		// icon
		m_btn_questionIcon = Button::create();
		m_btn_questionIcon->setTouchEnabled(true);
		m_btn_questionIcon->setPressedActionEnabled(true);
		//m_btn_signDailyIcon->loadTextures("gamescene_state/zhujiemian2/tubiao/OnlineAwards.png", "gamescene_state/zhujiemian2/tubiao/OnlineAwards.png","");
		m_btn_questionIcon->loadTextures("res_ui/meiriqiandao.png", "res_ui/meiriqiandao.png","");
		m_btn_questionIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_btn_questionIcon->setPosition(Vec2( winSize.width - 70 - 159 - 80 - 100, winSize.height-70));
		m_btn_questionIcon->addTouchEventListener(CC_CALLBACK_2(QuestionIcon::callBackBtnQuestion, this));

		// label
		m_label_questionIcon_text = Label::createWithTTF("bu ke qian dao", APP_FONT_NAME, 14);
		m_label_questionIcon_text->setColor(Color3B(255, 246, 0));
		m_label_questionIcon_text->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_questionIcon_text->setPosition(Vec2(m_btn_questionIcon->getPosition().x, m_btn_questionIcon->getPosition().y - m_btn_questionIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = cocos2d::extension::Scale9Sprite::create(Rect(5, 10, 1, 4) , "res_ui/zhezhao70.png");
		m_spirte_fontBg->setPreferredSize(Size(47, 14));
		m_spirte_fontBg->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_questionIcon_text->getPosition());


		m_layer_question = Layer::create();
		//m_layer_question->setTouchEnabled(true);
		m_layer_question->setAnchorPoint(Vec2::ZERO);
		m_layer_question->setPosition(Vec2::ZERO);
		m_layer_question->setContentSize(winSize);

		m_layer_question->addChild(m_spirte_fontBg, -10);
		m_layer_question->addChild(m_btn_questionIcon);
		m_layer_question->addChild(m_label_questionIcon_text);

		addChild(m_layer_question);

		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void QuestionIcon::callBackBtnQuestion(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
	}
	break;

	case Widget::TouchEventType::CANCELED:
	{
		// ��ʼ�� UI WINDOW��
		Size winSize = Director::getInstance()->getVisibleSize();

		//SignDailyUI* pTmpSignDailyUI = (SignDailyUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagSignDailyUI);
		//if (NULL == pTmpSignDailyUI)
		//{
		//	SignDailyUI * pSignDailyUI = SignDailyUI::create();
		//	pSignDailyUI->setIgnoreAnchorPointForPosition(false);
		//	pSignDailyUI->setAnchorPoint(Vec2(0.5f, 0.5f));
		//	pSignDailyUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		//	pSignDailyUI->setTag(kTagSignDailyUI);
		//	//pSignDailyUI->refreshUI();

		//	GameView::getInstance()->getMainUIScene()->addChild(pSignDailyUI);
		//}

		auto pTmpQuestionUI = (QuestionUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
		if (NULL == pTmpQuestionUI)
		{
			auto pQuestionUI = QuestionUI::create();
			pQuestionUI->setIgnoreAnchorPointForPosition(false);
			pQuestionUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			pQuestionUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			pQuestionUI->setTag(kTagQuestionUI);
			pQuestionUI->requestQuestion();				// ��������������Ŀ

			GameView::getInstance()->getMainUIScene()->addChild(pQuestionUI);
		}
	}
		break;

	default:
		break;
	}
}

void QuestionIcon::onEnter()
{
	UIScene::onEnter();
}

void QuestionIcon::onExit()
{
	UIScene::onExit();
}






