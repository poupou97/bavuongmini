#include "QuestionUI.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "QuestionData.h"
#include "../../utils/StaticDataManager.h"

using namespace CocosDenshion;

Layout * QuestionUI::s_pPanel;

QuestionUI::QuestionUI(void)
	:m_base_layer(NULL)
{

}

QuestionUI::~QuestionUI(void)
{

}

QuestionUI* QuestionUI::create()
{
	auto questionUI = new QuestionUI();
	if (questionUI && questionUI->init())
	{
		questionUI->autorelease();
		return questionUI;
	}
	CC_SAFE_DELETE(questionUI);
	return NULL;
}

bool QuestionUI::init()
{
	if (UIScene::init())
	{
		auto winSize = Director::getInstance()->getVisibleSize();

		m_base_layer = Layer::create();
		m_base_layer->setIgnoreAnchorPointForPosition(false);
		m_base_layer->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_base_layer->setContentSize(Size(800, 480));
		m_base_layer->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		this->addChild(m_base_layer);

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winSize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0, 0));
		m_pLayer->addChild(mengban);

		if(LoadSceneLayer::questionLayout->getParent() != NULL)
		{
			LoadSceneLayer::questionLayout->removeFromParentAndCleanup(false);
		}

		s_pPanel = LoadSceneLayer::questionLayout;
		s_pPanel->setAnchorPoint(Vec2(0.5f, 0.5f));
		s_pPanel->getVirtualRenderer()->setContentSize(Size(800, 480));
		s_pPanel->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		s_pPanel->setScale(1.0f);
		s_pPanel->setTouchEnabled(true);
		m_pLayer->addChild(s_pPanel);


		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		//this->setContentSize(Size(800, 480));

		initUI();

		return true;
	}
	return false;
}

void QuestionUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void QuestionUI::onExit()
{
	UIScene::onExit();

	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

void QuestionUI::callBackBtnClolse(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void QuestionUI::initUI()
{
	// �رհ�ť
	auto pBtnClose = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnabled(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addTouchEventListener(CC_CALLBACK_2(QuestionUI::callBackBtnClolse, this));

	// ������
	m_label_questionTitle = (Text *)Helper::seekWidgetByName(s_pPanel, "Label_question_text");
	m_label_questionTitle->setVisible(true);
	m_label_questionTitle->setTextAreaSize(Size(633,0));

	// ⾭��ֵ
	m_label_expValue =(Text*)Helper::seekWidgetByName(s_pPanel, "Label_exp_value");
	m_label_expValue->setVisible(true);

	// ��Ǯ
	m_label_conisValue = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_coins_value");
	m_label_conisValue->setVisible(true);

	// �ۼƴ�����
	m_label_rightAnswerValue = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_rightAnswer_value");
	m_label_rightAnswerValue->setVisible(true);

	// ��Ѵ����
	m_label_hasAnswerValue = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_hasAnswer_value");
	m_label_hasAnswerValue->setVisible(true);

	// �ɷ��ƴ��
	m_label_cardNumValue = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_cardNum_value");
	m_label_cardNumValue->setVisible(true);

	// btn1 A
	m_btn_a = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_a");
	m_btn_a->setTouchEnabled(true);
	m_btn_a->setPressedActionEnabled(true);
	m_btn_a->addTouchEventListener(CC_CALLBACK_2(QuestionUI::callBackBtnQuestion, this));
	m_btn_a->setTag(TAG_BTN_QUE_1);
	m_btn_a->setVisible(true);

	// btn2 B
	m_btn_b = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_b");
	m_btn_b->setTouchEnabled(true);
	m_btn_b->setPressedActionEnabled(true);
	m_btn_b->addTouchEventListener(CC_CALLBACK_2(QuestionUI::callBackBtnQuestion, this));
	m_btn_b->setTag(TAG_BTN_QUE_2);
	m_btn_b->setVisible(true);

	// btn3 C
	m_btn_c = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_c");
	m_btn_c->setTouchEnabled(true);
	m_btn_c->setPressedActionEnabled(true);
	m_btn_c->addTouchEventListener(CC_CALLBACK_2(QuestionUI::callBackBtnQuestion, this));
	m_btn_c->setTag(TAG_BTN_QUE_3);
	m_btn_c->setVisible(true);

	// btn4 D
	m_btn_d = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_d");
	m_btn_d->setTouchEnabled(true);
	m_btn_d->setPressedActionEnabled(true);
	m_btn_d->addTouchEventListener(CC_CALLBACK_2(QuestionUI::callBackBtnQuestion, this));
	m_btn_d->setTag(TAG_BTN_QUE_4);
	m_btn_d->setVisible(true);

	// btn1 A text
	m_label_btn1_text = (Text *)Helper::seekWidgetByName(s_pPanel, "Label_answerA");
	m_label_btn1_text->setVisible(true);
	m_label_btn1_text->setTextAreaSize(Size(240,0));

	// btn2 B text
	m_label_btn2_text = (Text *)Helper::seekWidgetByName(s_pPanel, "Label_answerB");
	m_label_btn2_text->setVisible(true);
	m_label_btn2_text->setTextAreaSize(Size(240,0));

	// btn3 C text
	m_label_btn3_text = (Text *)Helper::seekWidgetByName(s_pPanel, "Label_answerC");
	m_label_btn3_text->setVisible(true);
	m_label_btn3_text->setTextAreaSize(Size(240,0));

	// btn4 D text
	m_label_btn4_text = (Text *)Helper::seekWidgetByName(s_pPanel, "Label_answerD");
	m_label_btn4_text->setVisible(true);
	m_label_btn4_text->setTextAreaSize(Size(240,0));

	imageView_trueOption = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_trueOption");
	imageView_trueOption->setVisible(false);

	initBaseInfoFromServer();

	//set to default
	QuestionData::instance()->set_isGetedResult(true);
}

bool QuestionUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void QuestionUI::requestQuestion()
{
	// ��������� �������Ŀ
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2802);
}

void QuestionUI::callBackBtnQuestion(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (!QuestionData::instance()->get_isGetedResult())
			return;

		auto pUIBtn = (Button *)pSender;
		int nTag = pUIBtn->getTag();
		if (TAG_BTN_QUE_1 == nTag)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2803, (void *)0);
		}
		else if (TAG_BTN_QUE_2 == nTag)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2803, (void *)1);
		}
		else if (TAG_BTN_QUE_3 == nTag)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2803, (void *)2);
		}
		else if (TAG_BTN_QUE_4 == nTag)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2803, (void *)3);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void QuestionUI::initQuestionInfoFromServer()
{
	// ��Ǯ
	int nMoney = QuestionData::instance()->get_money();
	char charMoney[20];
	sprintf(charMoney, "%d", nMoney);
	m_label_conisValue->setString(charMoney);

	// ���
	int nExp = QuestionData::instance()->get_exp();
	char charExp[20];
	sprintf(charExp, "%d", nExp);
	m_label_expValue->setString(charExp);

	// ��Ѵ����(��Ѵ���� / �ÿ�մ�������ܴ��)
	int nHasAnswerQuestion = QuestionData::instance()->get_hasAnswerQuestion();
	char charHasAnswerQuestion[20];
	sprintf(charHasAnswerQuestion, "%d", nHasAnswerQuestion);

	int nAllQuestion = QuestionData::instance()->get_allQuestion();											// �ÿ�մ�����
	char charAllQuestion[20];
	sprintf(charAllQuestion, "%d", nAllQuestion);
	
	std::string strAnswerQuestion = "";
	strAnswerQuestion.append(charHasAnswerQuestion);
	strAnswerQuestion.append(" / ");
	strAnswerQuestion.append(charAllQuestion);

	m_label_hasAnswerValue->setString(strAnswerQuestion.c_str());

	// �������
	std::string strTmpQuestionTitle = "";
	std::string strQuestionTitle = QuestionData::instance()->get_questionTitle();

	int m_idx = nHasAnswerQuestion + 1;
	if (m_idx >= QuestionData::instance()->get_allQuestion())
		m_idx = QuestionData::instance()->get_allQuestion();

	char charQuestionTitle[20];
	sprintf(charQuestionTitle, "%d",m_idx);

	strTmpQuestionTitle.append(charQuestionTitle);
	strTmpQuestionTitle.append(". ");
	strTmpQuestionTitle.append(strQuestionTitle);

	m_label_questionTitle->setString(strTmpQuestionTitle.c_str());

	m_btn_a->setVisible(false);																								
	m_btn_b->setVisible(false);																								
	m_btn_c->setVisible(false);																								
	m_btn_d->setVisible(false);			

	// ���Ŀ��Ϣ
	int nSizeQuestion = QuestionData::instance()->m_vector_questionContent.size();
	std::vector<std::string > vector_questionContent = QuestionData::instance()->m_vector_questionContent;

	switch(nSizeQuestion)
	{
	case 1:
		{
			m_btn_a->setVisible(true);			
			m_label_btn1_text->setString(vector_questionContent.at(0).c_str());
		}
		break;
	case 2:
		{
			m_btn_a->setVisible(true);			
			m_label_btn1_text->setString(vector_questionContent.at(0).c_str());
			m_btn_b->setVisible(true);			
			m_label_btn2_text->setString(vector_questionContent.at(1).c_str());
		}
		break;
	case 3:
		{
			m_btn_a->setVisible(true);			
			m_label_btn1_text->setString(vector_questionContent.at(0).c_str());
			m_btn_b->setVisible(true);			
			m_label_btn2_text->setString(vector_questionContent.at(1).c_str());
			m_btn_c->setVisible(true);			
			m_label_btn3_text->setString(vector_questionContent.at(2).c_str());
		}
		break;
	case 4:
		{
			m_btn_a->setVisible(true);			
			m_label_btn1_text->setString(vector_questionContent.at(0).c_str());
			m_btn_b->setVisible(true);			
			m_label_btn2_text->setString(vector_questionContent.at(1).c_str());
			m_btn_c->setVisible(true);			
			m_label_btn3_text->setString(vector_questionContent.at(2).c_str());
			m_btn_d->setVisible(true);			
			m_label_btn4_text->setString(vector_questionContent.at(3).c_str());
		}
		break;
	}
	
}

void QuestionUI::initBaseInfoFromServer()
{
	int nAllQuestion = QuestionData::instance()->get_allQuestion();											// ÿ�մ�����
	int nHasAnswerQuestion = QuestionData::instance()->get_hasAnswerQuestion();					// ��ѻش���
	int nRightAnswerQuestion = QuestionData::instance()->get_rightAnswerQuestion();				// ��Ѿ���Ե���Ŀ��Ŀ
	int nCardNum = QuestionData::instance()->get_cardNum();													// �ɷ��ƵĴ��

	// �ע�:��Ѵ����(��Ѵ���� / �ÿ�մ�������ܴ��)
	std::string strAnswerQuestion = "";

	char charAllQuestion[20];
	sprintf(charAllQuestion, "%d", nAllQuestion);

	char charHasAnswerQuestion[20];
	sprintf(charHasAnswerQuestion, "%d", nHasAnswerQuestion);
	
	strAnswerQuestion.append(charHasAnswerQuestion);
	strAnswerQuestion.append(" / ");
	strAnswerQuestion.append(charAllQuestion);

	m_label_hasAnswerValue->setString(strAnswerQuestion.c_str());


	// ��Ѿ���Ե���Ŀ��Ŀ
	char charRightAnswerQuestion[20];
	sprintf(charRightAnswerQuestion, "%d", nRightAnswerQuestion);
	m_label_rightAnswerValue->setString(charRightAnswerQuestion);

	// �ɷ��ƵĴ��
	char charCardNum[20];
	sprintf(charCardNum, "%d", nCardNum);
	m_label_cardNumValue->setString(charCardNum);
}

void QuestionUI::HandleResult( int lastResult,int lastTrueOption )
{
	if (0 == lastResult)
	{
		setTrueOptionPos(lastTrueOption);
		auto sequence = Sequence::create(
			CCHide::create(),
			DelayTime::create(0.5f),
			Show::create(),
			DelayTime::create(1.5f),
			CallFunc::create(CC_CALLBACK_0(QuestionUI::reqNextQuestion,this)),
			CCHide::create(),
			NULL
			);
		imageView_trueOption->runAction(sequence);
	}
	else if(1 == lastResult)
	{
		reqNextQuestion();
	}
}

void QuestionUI::reqNextQuestion()
{
	if (QuestionData::instance()->get_hasAnswerQuestion() < 20)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2802);
		QuestionData::instance()->set_isGetedResult(true);
	}
}

void QuestionUI::setTrueOptionPos(int index )
{
	switch(index)
	{
	case 0:
		{
			imageView_trueOption->setPosition(Vec2(350,353));
		}
		break;
	case 1:
		{
			imageView_trueOption->setPosition(Vec2(661,353));
		}
		break;
	case 2:
		{
			imageView_trueOption->setPosition(Vec2(350,271));
		}
		break;
	case 3:
		{
			imageView_trueOption->setPosition(Vec2(661,271));
		}
		break;
	}
}

