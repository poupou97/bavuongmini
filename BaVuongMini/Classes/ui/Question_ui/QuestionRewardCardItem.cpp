#include "QuestionRewardCardItem.h"
#include "../../messageclient/element/CRewardProp.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "GameView.h"
#include "AppMacros.h"
#include "../../messageclient/element/CPropReward.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "QuestionData.h"
#include "../../utils/StaticDataManager.h"
#include "../GameUIConstant.h"

#define voidFramePath "res_ui/smdi_none.png"
#define whiteFramePath "res_ui/smdi_white.png"
#define greenFramePath "res_ui/smdi_green.png"
#define blueFramePath "res_ui/smdi_bule.png"
#define purpleFramePath "res_ui/smdi_purple.png"
#define orangeFramePath "res_ui/smdi_orange.png"


QuestionRewardCardItem::QuestionRewardCardItem():
isFloped(false),
grid(-1),
isFrontOrBack(true)
{
}


QuestionRewardCardItem::~QuestionRewardCardItem()
{
}

QuestionRewardCardItem * QuestionRewardCardItem::create( CPropReward *rewardProp,int index )
{
	auto questionRewardCardItem = new QuestionRewardCardItem();
	if (questionRewardCardItem && questionRewardCardItem->init(rewardProp,index))
	{
		questionRewardCardItem->autorelease();
		return questionRewardCardItem;
	}
	CC_SAFE_DELETE(questionRewardCardItem);
	return NULL;
}

bool QuestionRewardCardItem::init( CPropReward *rewardProp,int index )
{
	if (UIScene::init())
	{
		isFrontOrBack = true;
		grid = index;

		//���Ʊ��
		btn_backCardFrame = Button::create();
		btn_backCardFrame->loadTextures("res_ui/instance_end/card.png","res_ui/instance_end/card.png","");
		btn_backCardFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_backCardFrame->setPosition(Vec2(0,0));
		btn_backCardFrame->setTouchEnabled(false);
		btn_backCardFrame->addTouchEventListener(CC_CALLBACK_2(QuestionRewardCardItem::BackCradEvent, this));
		m_pLayer->addChild(btn_backCardFrame);
		btn_backCardFrame->setVisible(true);

		//濨�����
		btn_frontCardFrame = Button::create();
		btn_frontCardFrame->loadTextures("res_ui/instance_end/card_get.png","res_ui/instance_end/card_get.png","");
		btn_frontCardFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_frontCardFrame->setPosition(Vec2(0,0));
		btn_backCardFrame->setTouchEnabled(false);
		btn_frontCardFrame->addTouchEventListener(CC_CALLBACK_2(QuestionRewardCardItem::FrontCradEvent, this));
		btn_frontCardFrame->setRotationSkewY(0);
		m_pLayer->addChild(btn_frontCardFrame);
		btn_frontCardFrame->setVisible(true);


		/*********************��ж���ɫ***************************/
		std::string frameColorPath;
		if (rewardProp->good().quality() == 1)
		{
			frameColorPath = whiteFramePath;
		}
		else if (rewardProp->good().quality() == 2)
		{
			frameColorPath = greenFramePath;
		}
		else if (rewardProp->good().quality() == 3)
		{
			frameColorPath = blueFramePath;
		}
		else if (rewardProp->good().quality() == 4)
		{
			frameColorPath = purpleFramePath;
		}
		else if (rewardProp->good().quality() == 5)
		{
			frameColorPath = orangeFramePath;
		}
		else
		{
			frameColorPath = voidFramePath;
		}

		//�
		imageView_frame = ImageView::create();
		imageView_frame->loadTexture(frameColorPath.c_str());
		imageView_frame->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_frame->setPosition(Vec2(0,1));
		btn_frontCardFrame->addChild(imageView_frame);
		//���ͼƬ
		imageView_icon = ImageView::create();
		std::string iconPath = "res_ui/props_icon/";
		iconPath.append(rewardProp->good().icon());
		iconPath.append(".png");
		imageView_icon->loadTexture(iconPath.c_str());
		imageView_icon->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_icon->setPosition(Vec2(0,1));
		imageView_icon->setScale(0.8f);
		btn_frontCardFrame->addChild(imageView_icon);
		//������
		l_rewardName = Label::createWithTTF(rewardProp->good().name().c_str(), APP_FONT_NAME, 14);
		l_rewardName->setAnchorPoint(Vec2(0.5f,0));
		l_rewardName->setPosition(Vec2(0,-btn_frontCardFrame->getContentSize().width/2-7));
		l_rewardName->setColor(GameView::getInstance()->getGoodsColorByQuality(rewardProp->good().quality()));
		btn_frontCardFrame->addChild(l_rewardName);

		//�������
		l_roleName = Label::createWithTTF("", APP_FONT_NAME, 14);
		l_roleName->setAnchorPoint(Vec2(0.5f,0));
		l_roleName->setPosition(Vec2(0,btn_frontCardFrame->getContentSize().width/2-5));
		btn_frontCardFrame->addChild(l_roleName);

		this->setContentSize(btn_backCardFrame->getContentSize());
		return true;
	}
	return false;
}

void QuestionRewardCardItem::BackCradEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (isFloped)
			return;

		// 	//Ʒ��ƣ������棩
		// 	this->FlopAnimationToFront();
		//����� 2805
		// 
		// 
		if (QuestionData::instance()->get_cardNum() <= 0)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("QuestionUI_hasNoCardNumber"));
		}
		else
		{
			QuestionData::instance()->set_reward_index(grid);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2805);
			isFloped = true;
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void QuestionRewardCardItem::FrontCradEvent(Ref *pSender, Widget::TouchEventType type)
{

}

//ưѿ��Ʒ������沢���ò��ɴ��
void QuestionRewardCardItem::FlopAnimationToFront()
{
	btn_backCardFrame->setLocalZOrder(10);
	btn_frontCardFrame->setRotationSkewY(-180);
	auto  orbit1 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_90_TIME,1, 0, 0, -90, 0, 0);
	auto  action1 = Sequence::create(
		Show::create(),
		orbit1,
		CCHide::create(),
		NULL);

	auto  orbit2 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_180_TIME,1, 0, 0, 180, 0, 0);
	auto  action2 = Sequence::create(
		Show::create(),
		orbit2,
		NULL);

	auto action_ToFront = Sequence::create(action1,NULL);
	btn_backCardFrame->runAction(action_ToFront);
	auto action_ToBack = Sequence::create(action2,NULL);
	btn_frontCardFrame->runAction(action_ToBack);

	isFrontOrBack = true;
	btn_frontCardFrame->setTouchEnabled(false);
	btn_backCardFrame->setTouchEnabled(false);
}

//ذѿ��Ʒ������沢���ò��ɴ��
void QuestionRewardCardItem::FlopAnimationToBack()
{
	auto orbit1 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_180_TIME,1, 0, 0, 180, 0, 0);
	auto action1 = Sequence::create(
		orbit1,
		NULL);

	auto orbit2 = CCOrbitCamera::create(REWARD_CARD_FLOP_DEGREE_90_TIME,1, 0, 0, 90, 0, 0);
	auto action2 = Sequence::create(
		orbit2,
		CCHide::create(),
		NULL);

	auto action_ToBack = Sequence::create(action1,NULL);
	btn_backCardFrame->runAction(action_ToBack);
	auto action_ToFront = Sequence::create(action2,NULL);
	btn_frontCardFrame->runAction(action_ToFront);

	isFrontOrBack = false;
	btn_frontCardFrame->setTouchEnabled(false);
	btn_backCardFrame->setTouchEnabled(false);
}

void QuestionRewardCardItem::setBtnTouchEnabled()
{
	btn_backCardFrame->setTouchEnabled(true);
	btn_frontCardFrame->setTouchEnabled(false);
}

void QuestionRewardCardItem::RefreshCardInfo(GoodsInfo *goodsInfo,int amount)
{
	/*********************��ж���ɫ***************************/
	std::string frameColorPath;
	if (goodsInfo->quality() == 1)
	{
		frameColorPath = whiteFramePath;
	}
	else if (goodsInfo->quality() == 2)
	{
		frameColorPath = greenFramePath;
	}
	else if (goodsInfo->quality() == 3)
	{
		frameColorPath = blueFramePath;
	}
	else if (goodsInfo->quality() == 4)
	{
		frameColorPath = purpleFramePath;
	}
	else if (goodsInfo->quality() == 5)
	{
		frameColorPath = orangeFramePath;
	}
	else
	{
		frameColorPath = voidFramePath;
	}

	//�
	imageView_frame->loadTexture(frameColorPath.c_str());
	//���ͼƬ
	std::string iconPath = "res_ui/props_icon/";
	iconPath.append(goodsInfo->icon());
	iconPath.append(".png");
	imageView_icon->loadTexture(iconPath.c_str());
	//������
	l_rewardName->setString(goodsInfo->name().c_str());
	l_rewardName->setColor(GameView::getInstance()->getGoodsColorByQuality(goodsInfo->quality()));

}