#include "SingleRankListUi.h"
#include "../../AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CRankingPlayer.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../extensions/UITab.h"
#include "../../GameView.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../Mail_ui/MailUI.h"
#include "../extensions/RichTextInput.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "ChallengeRoundUi.h"
#include "cocostudio\CCSGUIReader.h"

SingleRankListUi::SingleRankListUi(void)
{
	curRanklistPage = 0;
	everyPageNum = 20;

	booIsHaveNext = false;
	lastSelectCellId = 0;
}


SingleRankListUi::~SingleRankListUi(void)
{
	std::vector<CRankingPlayer*>::iterator iter;
	for (iter = rangkPlayervector.begin(); iter != rangkPlayervector.end(); ++iter)
	{
		delete *iter;
	}
	rangkPlayervector.clear();
}

SingleRankListUi * SingleRankListUi::create()
{
	auto ranklist = new SingleRankListUi();
	if (ranklist && ranklist->init())
	{
		ranklist->autorelease();
		return ranklist;
	}
	CC_SAFE_DELETE(ranklist);
	return NULL;
}

bool SingleRankListUi::init()
{
	if ( UIScene::init())
	{
		winsize = Director::getInstance()->getVisibleSize();

		if(LoadSceneLayer::RankListLayer->getParent() != NULL)
		{
			LoadSceneLayer::RankListLayer->removeFromParentAndCleanup(false);
		}
		auto ppanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/guoguanzhanjiang_ranking_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(640, 430));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		//ActiveUI * activeui = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
		auto challengeui_ = (ChallengeRoundUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);
		auto LabelClazzName = (Text*)Helper::seekWidgetByName(ppanel,"Label_43_0");
		LabelClazzName->setString(ChallengeRoundUi::getClazzName(challengeui_->getCopyClazz()).c_str());

		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(SingleRankListUi::callBackCloseUi, this));
		Button_close->setPressedActionEnabled(true);
		//self rank
		labelSelfRank = (Text*)Helper::seekWidgetByName(ppanel,"Label_rankingValue");

		tableView_ranking = TableView::create(this,Size(556,260));
		tableView_ranking->setDirection(TableView::Direction::VERTICAL);
		tableView_ranking->setAnchorPoint(Vec2(0,0));
		tableView_ranking->setPosition(Vec2(52,63));
		tableView_ranking->setDelegate(this);
		tableView_ranking->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(tableView_ranking);

		this->setContentSize(Size(640,430));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}

void SingleRankListUi::onEnter()
{
	this->openAnim();
	UIScene::onEnter();
}

void SingleRankListUi::onExit()
{
	UIScene::onExit();
}

void SingleRankListUi::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void SingleRankListUi::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void SingleRankListUi::tableCellTouched( TableView* table, TableViewCell* cell )
{
	int index_ = cell->getIdx();
	
	if (table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(SelectImageTag)->setVisible(false);
		}
	}

	if (cell->getChildByTag(SelectImageTag))
	{
		cell->getChildByTag(SelectImageTag)->setVisible(true);
	}

	lastSelectCellId = cell->getIdx();
	
	long long selfId_ = GameView::getInstance()->myplayer->getRoleId();
	long long selectId_ = rangkPlayervector.at(index_)->role().roleid();
	if (selfId_ != selectId_)
	{
		auto listMenu = RankListMenu::create(index_);
		listMenu->setIgnoreAnchorPointForPosition(false);
		listMenu->setAnchorPoint(Vec2(0.5f,0.5f));
		listMenu->setPosition(Vec2(this->getContentSize().width - listMenu->getContentSize().width/2,listMenu->getContentSize().height+listMenu->getContentSize().height));
		GameView::getInstance()->getMainUIScene()->addChild(listMenu);
	}

}

cocos2d::Size SingleRankListUi::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(550,34);
}

cocos2d::extension::TableViewCell* SingleRankListUi::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(Size(540,32));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setPosition(Vec2(0,0));
	pHighlightSpr->setTag(SelectImageTag);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);
	
	Color3B color_ = Color3B(47,93,13);

	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(0, 0));
	sprite_bigFrame->setCapInsets(Rect(9,14,1,1));
	sprite_bigFrame->setPreferredSize(Size(540,32));
	cell->addChild(sprite_bigFrame);
	//rank
	int selfRank = idx+1;
	char s_ranking[20];
	sprintf(s_ranking,"%d",selfRank);
	if (selfRank<= 3)
	{
		std::string icon_path = "res_ui/rank";
		icon_path.append(s_ranking);
		icon_path.append(".png");
		auto sp_ranking = Sprite::create(icon_path.c_str());
		sp_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		sp_ranking->setPosition(Vec2(31,15));
		sp_ranking->setScale(0.95f);
		cell->addChild(sp_ranking);
	}
	else if(selfRank <= 10)
	{
		auto l_ranking = Label::createWithBMFont("res_ui/font/ziti_3.fnt", s_ranking);
		l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		l_ranking->setPosition(Vec2(31,15));
		cell->addChild(l_ranking);
	}
	else
	{
		auto l_ranking = Label::createWithTTF(s_ranking,APP_FONT_NAME,18);
		l_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
		l_ranking->setPosition(Vec2(31,15));
		l_ranking->setColor(color_);
		cell->addChild(l_ranking);
	}

	//name
	std::string name_ = rangkPlayervector.at(idx)->role().name();
	auto l_name = Label::createWithTTF(name_.c_str(),APP_FONT_NAME,18);
	l_name->setAnchorPoint(Vec2(0.0f,0.5f));
	l_name->setPosition(Vec2(120,15));
	l_name->setColor(color_);
	cell->addChild(l_name);

	int countryId =rangkPlayervector.at(idx)->role().countryid();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		auto countrySp_ = Sprite::create(iconPathName.c_str());
		countrySp_->setAnchorPoint(Vec2(1.0,0.5f));
		countrySp_->setPosition(Vec2(l_name->getPositionX() - 1,15));
		countrySp_->setScale(0.7f);
		cell->addChild(countrySp_);
	}
	//vipInfo
	int vipLevel_ = rangkPlayervector.at(idx)->role().viplevel();
	if (vipLevel_ > 0)
	{
		Node *pNode = MainScene::addVipInfoByLevelForNode(vipLevel_);
		if (pNode)
		{
			cell->addChild(pNode);
			pNode->setPosition(Vec2(l_name->getPosition().x+l_name->getContentSize().width+13,l_name->getPosition().y));
		}
	}
	//profession
	int professionId_ = rangkPlayervector.at(idx)->role().profession();
	std::string professionStr_ = BasePlayer::getProfessionNameIdxByIndex(professionId_);
	auto l_profession = Label::createWithTTF(professionStr_.c_str(),APP_FONT_NAME,18);
	l_profession->setAnchorPoint(Vec2(0.5f,0.5f));
	l_profession->setPosition(Vec2(265,15));
	l_profession->setColor(color_);
	cell->addChild(l_profession);
	//level
	char s_lv[20];
	sprintf(s_lv,"%d",rangkPlayervector.at(idx)->role().level());
	auto l_lv = Label::createWithTTF(s_lv,APP_FONT_NAME,18);
	l_lv->setAnchorPoint(Vec2(0.5f,0.5f));
	l_lv->setPosition(Vec2(320,15));
	l_lv->setColor(color_);
	cell->addChild(l_lv);
	//today max level
	char todaymaxLevel[20];
	sprintf(todaymaxLevel,"%d",rangkPlayervector.at(idx)->maxlevle());
	auto label_maxLevel = Label::createWithTTF(todaymaxLevel,APP_FONT_NAME,18);
	label_maxLevel->setAnchorPoint(Vec2(0.5f,0.5f));
	label_maxLevel->setPosition(Vec2(400,15));
	label_maxLevel->setColor(color_);
	cell->addChild(label_maxLevel);
	//max level is use time

	auto l_state = Label::createWithTTF(RecuriteActionItem::timeFormatToString(rangkPlayervector.at(idx)->usetime()/1000).c_str(),APP_FONT_NAME,18);
	l_state->setAnchorPoint(Vec2(0.5f,0.5f));
	l_state->setPosition(Vec2(480,15));
	l_state->setColor(color_);
	cell->addChild(l_state);

	if(idx == rangkPlayervector.size() - 3)
	{
		if (booIsHaveNext == true)
		{
			//req for next
			curRanklistPage++;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1904,this);
		}
	}

	return cell;
}

ssize_t SingleRankListUi::numberOfCellsInTableView( TableView *table )
{
	return rangkPlayervector.size();
}

bool SingleRankListUi::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void SingleRankListUi::callBackCloseUi(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}
////////////////////////////
RankListMenu::RankListMenu( void )
{
	m_index = 0;
}

RankListMenu::~RankListMenu( void )
{

}

RankListMenu * RankListMenu::create( int idx )
{
	auto listmenu = new RankListMenu();
	if (listmenu && listmenu->init(idx))
	{
		listmenu->autorelease();
		return listmenu;
	}
	CC_SAFE_DELETE(listmenu);
	return NULL;
}

bool RankListMenu::init( int idx )
{
	if (UIScene::init())
	{
		auto panel = Layout::create();
		panel->setContentSize(Size(100,240));
		panel->setPosition(Vec2(0,0));

		m_index = idx;

		const char *str_addfriend = StringDataManager::getString("friend_addfriend");
		char *addfriend_left=const_cast<char*>(str_addfriend);	

		const char *str_check = StringDataManager::getString("friend_check");
		char *check_left=const_cast<char*>(str_check);

		const char *str_mail = StringDataManager::getString("friend_mail");
		char *mail_left=const_cast<char*>(str_mail);

		const char *str_black = StringDataManager::getString("friend_black");
		char *black_left=const_cast<char*>(str_black);

		const char *str_private = StringDataManager::getString("chatui_private");
		char *private_left=const_cast<char*>(str_private);

		const char * normalImage = "res_ui/new_button_5.png";
		const char * selectImage ="res_ui/new_button_5.png";
		const char * finalImage = "";
		char * label[] = {private_left,check_left,addfriend_left,mail_left,black_left};
		auto listMenu_ =UITab::createWithText(5,normalImage,selectImage,finalImage,label,VERTICAL,-2);
		listMenu_->setAnchorPoint(Vec2(0,0));
		listMenu_->setPosition(Vec2(0,0));
		listMenu_->setHighLightImage((char*)finalImage);
		listMenu_->addIndexChangedEvent(this,coco_indexchangedselector(RankListMenu::callBackList));
		panel->addChild(listMenu_);
		m_pLayer->addChild(panel);

		setContentSize(Size(100,240));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}

	return false;
}

void RankListMenu::onEnter()
{
	UIScene::onEnter();
}

void RankListMenu::onExit()
{
	UIScene::onExit();
}

void RankListMenu::callBackList( Ref * obj )
{
	auto tab =(UITab *)obj;
	int num = tab->getCurrentIndex();
 	
	Size winSize =Director::getInstance()->getWinSize();
	
	auto ranklist_ = (SingleRankListUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyRank);
	long long playerId_ = ranklist_->rangkPlayervector.at(m_index)->role().roleid();
	std::string playerName_ = ranklist_->rangkPlayervector.at(m_index)->role().name();
 	int countryId_ = ranklist_->rangkPlayervector.at(m_index)->role().countryid();
	int vipLevel_ = ranklist_->rangkPlayervector.at(m_index)->role().viplevel();
	int level_ = ranklist_->rangkPlayervector.at(m_index)->role().level();
	int professionId_ = ranklist_->rangkPlayervector.at(m_index)->role().profession();

	switch(num)
	{
	case 0:
		{
			auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene->addPrivateChatUi(playerId_,playerName_,countryId_,vipLevel_,level_,professionId_);
		}break;
	case 1:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)playerId_);
		}break;
	case 2:
		{
			FriendStruct friend1={0,0,playerId_,playerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	case 3:
		{
			auto mail_ui=MailUI::create();
			mail_ui->setIgnoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(Vec2(0.5f,0.5f));
			mail_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			mail_ui->friendName->onTextFieldInsertText(NULL,playerName_.c_str(),30);
		}break;
	case 4:
		{
			FriendStruct friend1={0,1,playerId_,playerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	}
	removeFromParentAndCleanup(true);
}

bool RankListMenu::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return this->resignFirstResponder(pTouch,this,false,false);
}
