#include "ChallengeRoundUi.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "ShowLevelUi.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CSingCopyClazz.h"
#include "../../utils/StaticDataManager.h"
#include "../../AppMacros.h"
#include "../extensions/UITab.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "cocostudio/CCSGUIReader.h"


ChallengeRoundUi::ChallengeRoundUi(void)
{
	m_copyClazz = 1;
	m_copyResume = 0;
}


ChallengeRoundUi::~ChallengeRoundUi(void)
{
}

ChallengeRoundUi * ChallengeRoundUi::create()
{
	auto challengeui = new ChallengeRoundUi();
	if (challengeui && challengeui->init())
	{
		challengeui->autorelease();
		return challengeui;
	}
	CC_SAFE_DELETE(challengeui);
	return NULL;
}

bool ChallengeRoundUi::init()
{
	if (UIScene::init())
	{
		winsize= Director::getInstance()->getVisibleSize();

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		auto m_Layer= Layer::create();
		m_Layer->setIgnoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(Vec2(0.5f,0.5f));
		m_Layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_Layer->setContentSize(Size(800,480));
		m_Layer->setLocalZOrder(10);
		this->addChild(m_Layer);

		const char * firstStr = StringDataManager::getString("ChalllengeRoundUi_diaoshi_guo");
		const char * secondStr = StringDataManager::getString("ChalllengeRoundUi_diaoshi_guan");
		const char * thirdStr = StringDataManager::getString("ChalllengeRoundUi_diaoshi_zhan");
		const char * fourStr = StringDataManager::getString("ChalllengeRoundUi_diaoshi_jiang");

		auto atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,fourStr);
		atmature->setPosition(Vec2(30,240));
		m_Layer->addChild(atmature);

		auto mainPanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/guoguanzhanjiang_1.json");
		mainPanel->setAnchorPoint(Vec2(0.5f,0.5f));
		mainPanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(mainPanel);

		auto btn_close = (Button *)Helper::seekWidgetByName(mainPanel,"Button_close");
		btn_close->setTouchEnabled(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(ChallengeRoundUi::callBackCloseUi, this));

		auto layerClazz = Layer::create();
		layerClazz->setIgnoreAnchorPointForPosition(false);
		layerClazz->setAnchorPoint(Vec2(0.5f, 0.5f));
		layerClazz->setContentSize(Size(800, 480));
		layerClazz->setPosition(Vec2(winsize.width/2, winsize.height/2));
		this->addChild(layerClazz);

// 		const char * normalImage = "res_ui/tab_3_off.png";
// 		const char * selectImage = "res_ui/tab_3_on.png";
// 		const char * finalImage = "";
// 		const char * highLightImage = "res_ui/tab_3_on.png";
// 		const char *str1 = StringDataManager::getString("ActiveUi_ChallengeRoundTab");
// 		char* p1 = const_cast<char*>(str1);
// 		char* pHightLightImage = const_cast<char*>(highLightImage);
// 		char * mainNames[] = {p1};
// 		UITab * m_tab = UITab::createWithBMFont(1, normalImage, selectImage, finalImage, mainNames,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
// 		m_tab->setAnchorPoint(Vec2(0,0));
// 		m_tab->setPosition(Vec2(74, 408));
// 		m_tab->setHighLightImage(pHightLightImage);
// 		m_tab->setDefaultPanelByIndex(0);
// 		m_tab->addIndexChangedEvent(this, coco_indexchangedselector(ChallengeRoundUi::tabIndexChangedEvent));
// 		m_tab->setPressedActionEnabled(true);
// 		layerClazz->addChild(m_tab);

		auto copyClazzTableView = TableView::create(this, Size(665,367));
		//copyClazzTableView->setPressedActionEnabled(true);
		copyClazzTableView->setDirection(TableView::Direction::HORIZONTAL);
		copyClazzTableView->setAnchorPoint(Vec2(0,0));
		copyClazzTableView->setPosition(Vec2(75,60));
		copyClazzTableView->setDelegate(this);
		copyClazzTableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		layerClazz->addChild(copyClazzTableView);

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void ChallengeRoundUi::onEnter()
{
	this->openAnim();
	UIScene::onEnter();
}

void ChallengeRoundUi::onExit()
{
	UIScene::onExit();
}

void ChallengeRoundUi::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{

}

void ChallengeRoundUi::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

void ChallengeRoundUi::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	int cellIndex = cell->getIdx()+1;
	this->setCopyClazz(cellIndex);


	if (cell->getIdx() == 0)
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(this);
	}
	
	auto copyMsg  = SingleCopyClazzConfig::s_SingCopyClazzMsgData[this->getCopyClazz()];
	this->setCopyCostResume(copyMsg->get_resume());

	auto levelui_ = ShowLevelUi::create(cellIndex);
	levelui_->setIgnoreAnchorPointForPosition(false);
	levelui_->setAnchorPoint(Vec2(0.5f,0.5f));
	levelui_->setPosition(Vec2(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(levelui_);
	levelui_->setTag(ktagSingCopylevel);

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1903,levelui_);
}

cocos2d::Size ChallengeRoundUi::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(220,360);
}

cocos2d::extension::TableViewCell* ChallengeRoundUi::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	auto string =__String::createWithFormat("%d",idx);	
	auto cell =table->dequeueCell();
	cell=new TableViewCell();
	cell->autorelease();

	auto copyMsg  = SingleCopyClazzConfig::s_SingCopyClazzMsgData[idx+1];
	if (copyMsg)
	{
		int clazz_ = idx+1;
		int starLevel = copyMsg->get_starLevel();
		int endLevel = copyMsg->get_endLevel();
		std::string name_ =copyMsg->get_name();
		std::string icon_ = copyMsg->get_icon();
		int resume_ = copyMsg->get_resume();
		std::string rewardInfo_= copyMsg->get_reward();

		std::string clazzNamePath_ = "res_ui/guoguanzhanjiang/";
		std::string mapNamepath = "res_ui/guoguanzhanjiang/";
		switch(clazz_)
		{
		case 1:
			{
				clazzNamePath_.append("chushiniudao.png");
				mapNamepath.append("pic1.png");
			}break;
		case 2:
			{
				clazzNamePath_.append("xiaoyoumingqi.png");
				mapNamepath.append("pic2.png");
			}break;
		case 3:
			{
				clazzNamePath_.append("jiangongliye.png");
				mapNamepath.append("pic3.png");
			}break;
		}

		auto spbg = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_dia.png");
		spbg->setPreferredSize(Size(208,344));
		spbg->setCapInsets(Rect(19,20,1,1));
		spbg->setAnchorPoint(Vec2(0,0));
		spbg->setPosition(Vec2(10,0));
		cell->addChild(spbg);
		/*
		const char* resume_str = StringDataManager::getString("singleCopy_clazz_Costpower");
		CCLabel * labelresume_ = CCLabel::create(resume_str,"res_ui/font/ziti_3.fnt");
		labelresume_->setAnchorPoint(Vec2(0,0));
		labelresume_->setPosition(Vec2(20,10));
		spbg->addChild(labelresume_);

		char resumeValue[10];
		sprintf(resumeValue,"%d",resume_);
		Label  * labelresume_value=Label::createWithTTF(resumeValue,APP_FONT_NAME,18);
		labelresume_value->setAnchorPoint(Vec2(0,0));
		labelresume_value->setPosition(Vec2(labelresume_->getPositionX()+labelresume_->getContentSize().width,labelresume_->getPositionY()+2));
		spbg->addChild(labelresume_value);
		*/
		const char* reward_str = StringDataManager::getString("singleCopy_clazz_rewardInfo");
		auto labeleReward_ = Label::createWithBMFont("res_ui/font/ziti_3.fnt", reward_str);
		labeleReward_->setAnchorPoint(Vec2(0,0));
		labeleReward_->setPosition(Vec2(20,labeleReward_->getContentSize().height));
		spbg->addChild(labeleReward_);

		auto labeleReward_value=Label::createWithTTF(rewardInfo_.c_str(),APP_FONT_NAME,18);
		labeleReward_value->setAnchorPoint(Vec2(0,0));
		labeleReward_value->setPosition(Vec2(labeleReward_->getPositionX() + labeleReward_->getContentSize().width,labeleReward_->getPositionY()+2));
		spbg->addChild(labeleReward_value);
		//
		const char* recommendLevel_str = StringDataManager::getString("singleCopy_clazz_recommend");
		auto labeleRecommendLevel_ = Label::createWithBMFont("res_ui/font/ziti_3.fnt",recommendLevel_str);
		labeleRecommendLevel_->setAnchorPoint(Vec2(0,0));
		labeleRecommendLevel_->setPosition(Vec2(labeleReward_->getPositionX(),labeleReward_->getPositionY()+labeleReward_->getContentSize().height));
		spbg->addChild(labeleRecommendLevel_);

		std::string recommendLevelStrint = "";
		char recommendStarLevel[10];
		sprintf(recommendStarLevel,"%d",starLevel);
		recommendLevelStrint.append(recommendStarLevel);
		recommendLevelStrint.append("-");
		char recommendEndLevel[10];
		sprintf(recommendEndLevel,"%d",endLevel);
		recommendLevelStrint.append(recommendEndLevel);

		auto labeleRecommendLevel_value=Label::createWithTTF(recommendLevelStrint.c_str(),APP_FONT_NAME,18);
		labeleRecommendLevel_value->setAnchorPoint(Vec2(0,0));
		labeleRecommendLevel_value->setPosition(Vec2(labeleRecommendLevel_->getPositionX()+labeleRecommendLevel_->getContentSize().width,labeleRecommendLevel_->getPositionY()+2));
		spbg->addChild(labeleRecommendLevel_value);
		//
		auto spbg_firstBoundary = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_dibian.png");
		spbg_firstBoundary->setPreferredSize(Size(208,35));
		spbg_firstBoundary->setCapInsets(Rect(19,35,1,1));
		spbg_firstBoundary->setAnchorPoint(Vec2(0,0));
		spbg_firstBoundary->setPosition(Vec2(0,labeleRecommendLevel_->getPositionY()+ labeleRecommendLevel_->getContentSize().height));
		spbg->addChild(spbg_firstBoundary,1);

		auto spbg_firstMapIcon = cocos2d::extension::Scale9Sprite::create(mapNamepath.c_str());
		spbg_firstMapIcon->setAnchorPoint(Vec2(0,0));
		spbg_firstMapIcon->setPosition(Vec2(3,spbg_firstBoundary->getPositionY()+ spbg_firstBoundary->getContentSize().height/2));
		spbg_firstMapIcon->setScaleX(201/spbg_firstMapIcon->getContentSize().width);
		spbg_firstMapIcon->setScaleY(200/spbg_firstMapIcon->getContentSize().height);
		spbg->addChild(spbg_firstMapIcon);

		auto spbg_secondBoundary = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_dibian.png");
		spbg_secondBoundary->setPreferredSize(Size(208,35));
		spbg_secondBoundary->setCapInsets(Rect(19,35,1,1));
		spbg_secondBoundary->setAnchorPoint(Vec2(0,0));
		spbg_secondBoundary->setPosition(Vec2(0,spbg_firstMapIcon->getPositionY()+180));
		spbg->addChild(spbg_secondBoundary,1);

		auto spbg_firstClazzName = cocos2d::extension::Scale9Sprite::create(clazzNamePath_.c_str());
		spbg_firstClazzName->setAnchorPoint(Vec2(0,0));
		spbg_firstClazzName->setPosition(Vec2(45,spbg_secondBoundary->getPositionY()+ spbg_secondBoundary->getContentSize().height/2+spbg_firstClazzName->getContentSize().height/2 - 5));
		spbg->addChild(spbg_firstClazzName);


		/*
		Label  * labeleClazzName=Label::createWithTTF(name_.c_str(),APP_FONT_NAME,16);
		labeleClazzName->setAnchorPoint(Vec2(0.5f,0.5f));
		labeleClazzName->setPosition(Vec2(spbg->getContentSize().width/2,spbg->getContentSize().height/2));
		spbg->addChild(labeleClazzName);
		*/
	}
	return cell;
}

ssize_t ChallengeRoundUi::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	return SingleCopyClazzConfig::s_SingCopyClazzMsgData.size();
}

void ChallengeRoundUi::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void ChallengeRoundUi::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void ChallengeRoundUi::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void ChallengeRoundUi::callBackCloseUi(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void ChallengeRoundUi::setCopyClazz( int value_ )
{
	m_copyClazz = value_;
}

int ChallengeRoundUi::getCopyClazz()
{
	return m_copyClazz;
}

void ChallengeRoundUi::setCopyCostResume( int value_ )
{
	m_copyResume = value_;
}

int ChallengeRoundUi::getCopyCostResume()
{
	return m_copyResume;
}

std::string ChallengeRoundUi::getClazzName( int clazz_ )
{
	std::string clazzName ="";
	switch(clazz_)
	{
	case 1:
		{
			const char* string_ = StringDataManager::getString("singCopy_firstClazzName_chushiniudao");
			clazzName.append(string_);
		}break;
	case 2:
		{
			const char* string_ = StringDataManager::getString("singCopy_secondClazzName_xiaoyoumingqi");
			clazzName.append(string_);
		}break;
	case 3:
		{
			const char* string_ = StringDataManager::getString("singCopy_thirdClazzName_jiangongliye");
			clazzName.append(string_);
		}break;
	}
	return clazzName;
}

std::string ChallengeRoundUi::getClazzLevel( int clazz )
{
	std::string clazzLevel ="";
	switch(clazz)
	{
	case 1:
		{
			const char* string_ = StringDataManager::getString("singCopy_firstClazz_level_");
			clazzLevel.append(string_);
		}break;
	case 2:
		{
			const char* string_ = StringDataManager::getString("singCopy_secondClazz_level_");
			clazzLevel.append(string_);
		}break;
	case 3:
		{
			const char* string_ = StringDataManager::getString("singCopy_thirdClazz_level_");
			clazzLevel.append(string_);
		}break;
	}
	return clazzLevel;
}

void ChallengeRoundUi::tabIndexChangedEvent( Ref * obj )
{

}

void ChallengeRoundUi::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void ChallengeRoundUi::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction)
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,215,360,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+190+_w,pos.y+235+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void ChallengeRoundUi::removeCCTutorialIndicator()
{
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}
