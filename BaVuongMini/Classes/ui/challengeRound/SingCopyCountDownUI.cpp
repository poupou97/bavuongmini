#include "SingCopyCountDownUI.h"
#include "../../utils/GameUtils.h"
#include "SingCopyInstance.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../utils/StaticDataManager.h"

SingCopyCountDownUI::SingCopyCountDownUI():
m_nRemainTime(3000)
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();	
	auto missionAndTeam = (MissionAndTeam *)mainscene->getChildByTag(kTagMissionAndTeam);
	if (missionAndTeam)
	{
		missionAndTeam->setSingCopy_useTime(0);
	}
}


SingCopyCountDownUI::~SingCopyCountDownUI()
{
}

SingCopyCountDownUI * SingCopyCountDownUI::create()
{
	auto offLineArenaCountDownUI = new SingCopyCountDownUI();
	if (offLineArenaCountDownUI && offLineArenaCountDownUI->init())
	{
		offLineArenaCountDownUI->autorelease();
		return offLineArenaCountDownUI;
	}
	CC_SAFE_DELETE(offLineArenaCountDownUI);
	return NULL;
}

bool SingCopyCountDownUI::init()
{
	if (UIScene::init())
	{
		winsize = Director::getInstance()->getVisibleSize();

		const char * str_ = StringDataManager::getString("singleCopy_curLevelLabel");
		char string_[50];
		sprintf(string_,str_,SingCopyInstance::getCurSingCopyLevel());

		auto background_ =ImageView::create();
		background_->loadTexture("res_ui/zhezhao80.png");
		background_->setAnchorPoint(Vec2(0.5f,0.5f));
		background_->setScale9Enabled(true);
		background_->setContentSize(Size(winsize.width,80));
		//background_->setCapInsets(Rect(50,50,1,1));
		background_->setPosition(Vec2(-background_->getContentSize().width/2,winsize.height*0.8f));
 		m_pLayer->addChild(background_);
 		background_->runAction(MoveTo::create(0.5f,Vec2(winsize.width/2,winsize.height*0.8f)));

		auto level_BMFont = Label::createWithBMFont("res_ui/font/ziti_1.fnt",string_);
		level_BMFont->setAnchorPoint(Vec2(0.5f,0.5f));
		level_BMFont->setPosition(Vec2(winsize.width+level_BMFont->getContentSize().width ,winsize.height*0.8f));
		level_BMFont->setScale(1.2f);
		m_pLayer->addChild(level_BMFont);

		auto action =(ActionInterval *)Sequence::create(
			Show::create(),
			MoveTo::create(0.5f,Vec2(winsize.width/2,winsize.height*0.8f)),
			DelayTime::create(0.5f),
			RemoveSelf::create(),
			CallFunc::create(CC_CALLBACK_0(SingCopyCountDownUI::showStarCountTime,this)),
			NULL);

		level_BMFont->runAction(action);
		

		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void SingCopyCountDownUI::onEnter()
{
	UIScene::onEnter();
}

void SingCopyCountDownUI::onExit()
{
	UIScene::onExit();
}

bool SingCopyCountDownUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void SingCopyCountDownUI::update( float dt )
{
	m_nRemainTime -= 1000;
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	auto missionAndTeam = (MissionAndTeam *)mainscene->getChildByTag(kTagMissionAndTeam);
	if (missionAndTeam)
	{
		missionAndTeam->setSingCopy_useTime(0);
	}
	if (m_nRemainTime <= 0)
	{
		m_nRemainTime = 0;
		missionAndTeam->starRunUpdate();
		this->removeFromParent();
	}

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	std::string str_path = "res_ui/font/";
	str_path.append(s_remainTime);
	str_path.append(".png");
	imageView_countDown->loadTexture(str_path.c_str());
}

void SingCopyCountDownUI::showStarCountTime()
{
	imageView_countDown = ImageView::create();
	imageView_countDown->loadTexture("res_ui/font/3.png");
	imageView_countDown->setAnchorPoint(Vec2(0.5f,0.5f));
	imageView_countDown->setPosition(Vec2(winsize.width/2,winsize.height*0.8f));
	m_pLayer->addChild(imageView_countDown);

	float space = (m_nRemainTime-(GameUtils::millisecondNow() - SingCopyInstance::getStartTime()))*1.0f/m_nRemainTime;
	if (space <=0)
	{
		space = 1;
	}
	this->schedule(schedule_selector(SingCopyCountDownUI::update),space*1.2f);

}
