#include "SingleCopyResult.h"
#include "../../messageclient/element/CSingCopyClazz.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/GameStatistics.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../FivePersonInstance/ResultHeadPartItem.h"
#include "../../ui/GameUIConstant.h"
#include "SingCopyInstance.h"
#include "../../gamescene_state/MainAnimationScene.h"
#include "gamescene_state/sceneelement/MissionAndTeam.h"
#include "gamescene_state/MainScene.h"
#include "cocostudio\CCSGUIReader.h"


SingleCopyResult::SingleCopyResult(void):
m_nRemainTime(30000)
{
}


SingleCopyResult::~SingleCopyResult(void)
{
}

SingleCopyResult * SingleCopyResult::create(int clazz,int level,int curTime,int bestTime,int money,int result)
{
	auto resultui_ = new SingleCopyResult();
	if (resultui_ && resultui_->init(clazz,level,curTime,bestTime,money,result))
	{
		resultui_->autorelease();
		return resultui_;
	}
	CC_SAFE_DELETE(resultui_);
	return NULL;
}

bool SingleCopyResult::init(int clazz,int level,int curTime,int bestTime,int money,int result)
{
	if (UIScene::init())
	{
		winsize = Director::getInstance()->getVisibleSize();

		m_clazz = clazz;
		m_level = level;
		m_curTime = curTime;
		m_bestTime = bestTime;;
		m_money = money;
		m_result = result;

		layer_anm = Layer::create();
		addChild(layer_anm);

		auto action =(ActionInterval *)Sequence::create(
					DelayTime::create(END_BATTLE_BEFORE_SLOWMOTION_DURATION),
					CallFunc::create(CC_CALLBACK_0(SingleCopyResult::startSlowMotion,this)),
					CallFunc::create(CC_CALLBACK_0(SingleCopyResult::addLightAnimation,this)),
					DelayTime::create(END_BATTLE_SLOWMOTION_DURATION),
					CallFunc::create(CC_CALLBACK_0(SingleCopyResult::endSlowMotion,this)),
					DelayTime::create(2.0f),
					CallFunc::create(CC_CALLBACK_0(SingleCopyResult::showUIAnimation,this)),
					NULL);
		this->runAction(action);

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(670,351));
		return true;
	}
	return false;
}

void SingleCopyResult::onEnter()
{
	this->openAnim();
	UIScene::onEnter();
}

void SingleCopyResult::onExit()
{
	UIScene::onExit();
}

void SingleCopyResult::callBackCloseUi(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();

		auto missionAndTeam = (MissionAndTeam *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeam)
		{
			missionAndTeam->setStateIsRobot(false);
			missionAndTeam->unschedule(schedule_selector(MissionAndTeam::updateSingCopyCheckMonst));
		}
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1902, (void *)m_clazz, (void *)m_level);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void SingleCopyResult::callBackEnterNextLevel(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto missionAndTeam = (MissionAndTeam *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeam)
		{
			if (missionAndTeam->getStateIsRobot())
			{
				missionAndTeam->schedule(schedule_selector(MissionAndTeam::updateSingCopyCheckMonst), 1.0f);
			}
		}

		int temp_level = m_level + 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1901, (void *)m_clazz, (void *)temp_level);

		if (m_result == 1)
		{
			SingCopyInstance::setCurSingCopyLevel(temp_level);
		}
		else
		{
			SingCopyInstance::setCurSingCopyLevel(m_level);
		}

		DamageStatistics::clear();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void SingleCopyResult::callBackEnterCurLevel(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		auto missionAndTeam = (MissionAndTeam *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeam)
		{
			if (missionAndTeam->getStateIsRobot())
			{
				missionAndTeam->schedule(schedule_selector(MissionAndTeam::updateSingCopyCheckMonst), 1.0f);
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(1901, (void *)m_clazz, (void *)m_level);
		SingCopyInstance::setCurSingCopyLevel(m_level);
		DamageStatistics::clear();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}


void SingleCopyResult::update( float dt )
{
	m_nRemainTime -= 1000;
	if (m_nRemainTime <= 0)
	{
		m_nRemainTime = 0;
		this->callBackCloseUi(NULL,Widget::TouchEventType::ENDED);
	}
	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	labelCOuntTime->setString(s_remainTime);
}

void SingleCopyResult::showUIAnimation()
{
	Size winSize = Director::getInstance()->getVisibleSize();
	auto u_layer=Layer::create();
	u_layer->setIgnoreAnchorPointForPosition(false);
	u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
	u_layer->setPosition(Vec2(670/2,351/2));
	u_layer->setContentSize(Size(670,351));
	addChild(u_layer);

	auto mengban = ImageView::create();
	mengban->loadTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enabled(true);
	mengban->setContentSize(winSize);
	mengban->setAnchorPoint(Vec2(0.5f,0.5f));
	mengban->setPosition(Vec2(670/2,351/2));
	m_pLayer->addChild(mengban);
	
	//��ݶ�����ƴ����������
	cocostudio::CCArmature *armature ;
	if (m_result == 1)
	{
		//�ӵ����ļ��첽���ض���
		cocostudio::CCArmatureDataManager::getInstance()->addArmatureFileInfo("res_ui/uiflash/winLogo/winLogo0.png","res_ui/uiflash/winLogo/winLogo0.plist","res_ui/uiflash/winLogo/winLogo.ExportJson");
		armature = cocostudio::CCArmature::create("winLogo");
		armature->getAnimation()->play("Animation_begin");
		armature->getAnimation()->setMovementEventCallFunc(this,movementEvent_selector(SingleCopyResult::ArmatureFinishCallback));  
	}
	else
	{	
		//�ӵ����ļ��첽���ض���
		cocostudio::CCArmatureDataManager::getInstance()->addArmatureFileInfo("res_ui/uiflash/lostLogo/lostLogo0.png",
																"res_ui/uiflash/lostLogo/lostLogo0.plist",
															"res_ui/uiflash/lostLogo/lostLogo.ExportJson");
		armature = cocostudio::CCArmature::create("lostLogo");
		//����ָ�����
		armature->getAnimation()->playWithIndex(0,-1,cocostudio::AnimationType::ANIMATION_NO_LOOP);
	}
	//��޸����
	armature->setScale(1.0f);
	//����ö�������λ�
	armature->setPosition(Vec2(670/2,351/2));
	//���ӵ���ǰҳ�
	u_layer->addChild(armature);

	auto sequence = Sequence::create(ScaleTo::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME,1.0f,1.0f),
		DelayTime::create(2.3f),
		CallFunc::create(CC_CALLBACK_0(SingleCopyResult::createUI,this)),
		NULL);
	
	this->runAction(sequence);
	/*
	Size winSize = Director::getInstance()->getVisibleSize();
	ImageView *mengban = ImageView::create();
	mengban->loadTexture("res_ui/zhezhao80.png");
	mengban->setScale9Enabled(true);
	mengban->setContentSize(winSize);
	mengban->setAnchorPoint(Vec2(.5f,.5f));
	m_pLayer->addChild(mengban);
	mengban->setPosition(Vec2(670/2,351/2));

	if (m_result == 1)
	{
		ImageView * imageView_light = ImageView::create();
		imageView_light->loadTexture("res_ui/lightlight.png");
		imageView_light->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_light->setPosition(Vec2(670/2,351/2));
		layer_anm->addChild(imageView_light); 

		CCRotateBy * rotateAnm = RotateBy::create(4.0f,360);
		RepeatForever * repeatAnm = RepeatForever::create(Sequence::create(rotateAnm,NULL));
		imageView_light->runAction(repeatAnm);

		
		Sequence * sequence_light = Sequence::create(
			DelayTime::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME),
			Show::create(),
			DelayTime::create(RESULT_UI_RESULTFLAG_DELAY_TIME),
			MoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,Vec2(670/2,351)),
			NULL);
		imageView_light->runAction(sequence_light);
	}

	ImageView * imageView_result = ImageView::create();
	if (m_result == 0)
	{
		imageView_result->loadTexture("res_ui/jiazuzhan/lost.png");
	}
	else
	{
		imageView_result->loadTexture("res_ui/jiazuzhan/win.png");
	}
	imageView_result->setAnchorPoint(Vec2(0.5f,0.5f));
	imageView_result->setPosition(Vec2(670/2,351/2));
	imageView_result->setScale(RESULT_UI_RESULTFLAG_BEGINSCALE);
	layer_anm->addChild(imageView_result);

	Sequence * sequence = Sequence::create(ScaleTo::create(RESULT_UI_RESULTFLAG_SCALECHANGE_TIME,1.0f,1.0f),
		DelayTime::create(RESULT_UI_RESULTFLAG_DELAY_TIME),
		MoveTo::create(RESULT_UI_RESULTFLAG_MOVE_TIME,Vec2(670/2,351)),
		DelayTime::create(0.05f),
		CallFunc::create(this,callfunc_selector(SingleCopyResult::createUI)),
		NULL);
	imageView_result->runAction(sequence);
	*/
}

void SingleCopyResult::startSlowMotion()
{
	Director::getInstance()->getScheduler()->setTimeScale(0.2f);
}

void SingleCopyResult::endSlowMotion()
{
	Director::getInstance()->getScheduler()->setTimeScale(1.0f);
}

void SingleCopyResult::createUI()
{
	auto mainPanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/guoguanzhanjiangAccounts_1.json");
	mainPanel->setAnchorPoint(Vec2(0,0));
	mainPanel->setPosition(Vec2(0,-35));
	m_pLayer->addChild(mainPanel);

	auto clazzAndLevel_ = (Text*)Helper::seekWidgetByName(mainPanel,"Label_24");
	auto labelCurCostTime = (Text*)Helper::seekWidgetByName(mainPanel,"cueTime");
	auto labelCurbestTime = (Text*)Helper::seekWidgetByName(mainPanel,"cueBestTime");
	auto labelReward = (Text*)Helper::seekWidgetByName(mainPanel,"prize_money");
	labelCOuntTime = (Text*)Helper::seekWidgetByName(mainPanel,"Label_35");

	const char *strings_curLevelBmfont = StringDataManager::getString("singleCopy_curLevelLabel");
	char levelStr[50];
	sprintf(levelStr,strings_curLevelBmfont,m_level);
	auto copyMsg  = SingleCopyClazzConfig::s_SingCopyClazzMsgData[m_clazz];
	std::string clazz_level = "";
	clazz_level.append(copyMsg->get_name().c_str());
	clazz_level.append(levelStr);
	clazzAndLevel_->setString(clazz_level.c_str());

	labelCurCostTime->setString(RecuriteActionItem::timeFormatToString(m_curTime).c_str());
	labelCurbestTime->setString(RecuriteActionItem::timeFormatToString(m_bestTime).c_str());

	char rewardMoneyStr[10];
	sprintf(rewardMoneyStr,"%d",m_money);
	labelReward->setString(rewardMoneyStr);

	char s_remainTime[20];
	sprintf(s_remainTime,"%d",m_nRemainTime/1000);
	labelCOuntTime->setString(s_remainTime);

	//show general 
	int space = 25;
	int width = 66;
	int allsize = GameView::getInstance()->generalsInLineList.size()+1;
	float firstPos_x = 335- (space+width)*0.5f*(allsize-1);

	for (int i = 0;i<allsize;++i)
	{
		if (i >= 6)
			continue;

		auto temp = ResultHeadPartItem::create(0,i,ResultHeadPartItem::type_dmgOnly);
		temp->setIgnoreAnchorPointForPosition(false);
		temp->setAnchorPoint(Vec2(0.5f,0.5f));
		temp->setPosition(Vec2(firstPos_x+(width+space)*i,80));
		layer_anm->addChild(temp);
	}

	auto btn_again = (Button *)Helper::seekWidgetByName(mainPanel,"Button_37");
	btn_again->setTouchEnabled(true);
	btn_again->setPressedActionEnabled(true);
	btn_again->addTouchEventListener(CC_CALLBACK_2(SingleCopyResult::callBackEnterCurLevel, this));

	auto btn_next = (Button *)Helper::seekWidgetByName(mainPanel,"Button_37_0");
	btn_next->setTouchEnabled(true);
	btn_next->setPressedActionEnabled(true);
	btn_next->addTouchEventListener(CC_CALLBACK_2(SingleCopyResult::callBackEnterNextLevel, this));

	auto btn_close = (Button *)Helper::seekWidgetByName(mainPanel,"Button_37_0_0");
	btn_close->setTouchEnabled(true);
	btn_close->setPressedActionEnabled(true);
	btn_close->addTouchEventListener(CC_CALLBACK_2(SingleCopyResult::callBackCloseUi, this));

	if (m_level >= SingCopyInstance::getMaxlevel())
	{
		btn_next->setVisible(false);
	}

	this->schedule(schedule_selector(SingleCopyResult::update),1.0f);
}

void SingleCopyResult::ArmatureFinishCallback(cocostudio::Armature *armature, cocostudio::MovementEventType movementType, const char *movementID )
{
	std::string id = movementID;
	if (id.compare("Animation_begin") == 0)
	{
		armature->stopAllActions();
		armature->getAnimation()->play("Animation1_end");
	}
}

void SingleCopyResult::addLightAnimation()
{
	Size size = Director::getInstance()->getVisibleSize();
	std::string animFileName = "animation/texiao/renwutexiao/SHANGUANG/shanguang.anm";
	auto la_light = CCLegendAnimation::create(animFileName);
	if (la_light)
	{
		la_light->setPlayLoop(false);
		la_light->setReleaseWhenStop(true);
		la_light->setPlaySpeed(5.0f);
		la_light->setScale(0.85f);
		la_light->setPosition(Vec2(size.width/2,size.height/2));
		GameView::getInstance()->getMainAnimationScene()->addChild(la_light);
	}
}
