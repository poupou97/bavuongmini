
#ifndef CHALLENGEROUND_CHALLENGEROUND_H
#define CHALLENGEROUND_CHALLENGEROUND_H

#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"

class ChallengeRoundUi:public UIScene,public cocos2d::extension::TableViewDataSource,public cocos2d::extension::TableViewDelegate
{
public:
	ChallengeRoundUi(void);
	~ChallengeRoundUi(void);

	static ChallengeRoundUi * create();
	bool init();

	void onEnter();
	void onExit();
	void callBackCloseUi(Ref *pSender, Widget::TouchEventType type);

	void tabIndexChangedEvent(Ref * obj);

	void setCopyClazz(int value_);
	int getCopyClazz();

	void setCopyCostResume(int value_);
	int getCopyCostResume();

	static std::string getClazzName(int clazz_);
	static std::string getClazzLevel(int clazz);
public:
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	// default implements are used to call script callback if exist
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);


public:
	//��ѧ
	int mTutorialScriptInstanceId;
	int mTutorialIndex;
	//��ѧ
	virtual void registerScriptCommand(int scriptId);

	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

private:
	Size winsize;
	int m_copyClazz;
	int m_copyResume;
};

#endif