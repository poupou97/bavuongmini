#ifndef _INSTANCE_SINGCOPYINSTANCE_H_
#define _INSTANCE_SINGCOPYINSTANCE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class SingCopyInstance
{
public:
	SingCopyInstance();
	~SingCopyInstance();

	enum SingCopy_Status
	{
		s_none = 0,
		s_inCountDown,
		s_inBattle
	};

	static SingCopyInstance * s_singCopyInstance;
	static SingCopyInstance * getInstance();

	static void setCurSingCopyClazz(int clazz_);
	static int getCurSingCopyClazz();

	static void setCurSingCopyLevel(int level_);
	static int getCurSingCopyLevel();

	static void setCurState(int state_);
	static void getCurState();

	static void setStartTime(long long time);
	static int getStartTime();

	static void setMaxLevel(int maxLevel);
	static int getMaxlevel();
private:
	static int s_singCopy_clazz;
	static int s_singCopy_level;

	static int m_nCurStatus;
	static long long m_nStartTime;

	static int m_maxLevel;
};

#endif