
#ifndef _SINGLERANKLISTUI_H
#define _SINGLERANKLISTUI_H

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CRankingPlayer;
#define  SelectImageTag 1011
class SingleRankListUi:public UIScene,public TableViewDelegate,public TableViewDataSource
{
public:
	SingleRankListUi(void);
	~SingleRankListUi(void);

	static SingleRankListUi * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	
	void callBackCloseUi(Ref *pSender, Widget::TouchEventType type);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
public:
	Text * labelSelfRank;
	TableView * tableView_ranking;
	std::vector<CRankingPlayer *> rangkPlayervector;
	int curRanklistPage;
	int everyPageNum;
	bool booIsHaveNext;
private:
	Size winsize;
	int lastSelectCellId;
};


///////////////ranklist
class RankListMenu:public UIScene
{
public:
	RankListMenu(void);
	~RankListMenu(void);

	static RankListMenu *create(int idx);
	bool init(int idx);

	void onEnter();
	void onExit();

	void callBackList(Ref * obj);
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
private:
	int m_index;

	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};
};




#endif