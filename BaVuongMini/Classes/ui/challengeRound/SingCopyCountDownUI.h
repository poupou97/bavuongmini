
#ifndef _CHALLOUND_SINGCOPYCOUNTDOWN_H_
#define _CHALLOUND_SINGCOPYCOUNTDOWN_H_

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class SingCopyCountDownUI : public UIScene
{
public:
	SingCopyCountDownUI();
	~SingCopyCountDownUI();

	static SingCopyCountDownUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);

	void showStarCountTime();
	virtual void update(float dt);

private:
	int m_nRemainTime;
	//Label *l_countDown;
	ImageView * imageView_countDown;
	Size winsize;
};

#endif

