#include "SingCopyInstance.h"
#include "GameView.h"
#include "../../utils/StaticDataManager.h"

SingCopyInstance::SingCopyInstance()
{
}

SingCopyInstance::~SingCopyInstance()
{
}

SingCopyInstance * SingCopyInstance::getInstance()
{
	if (s_singCopyInstance == NULL)
	{
		s_singCopyInstance = new SingCopyInstance();
	}

	return s_singCopyInstance;
}

void SingCopyInstance::setCurSingCopyClazz( int clazz_ )
{
	s_singCopy_clazz = clazz_;
}

int SingCopyInstance::getCurSingCopyClazz()
{
	return s_singCopy_clazz;
}

void SingCopyInstance::setCurSingCopyLevel( int level_ )
{
	s_singCopy_level = level_;
}

int SingCopyInstance::getCurSingCopyLevel()
{
	return s_singCopy_level;
}

void SingCopyInstance::setStartTime( long long time )
{
	m_nStartTime = time;
}

int SingCopyInstance::getStartTime()
{
	return m_nStartTime;
}

void SingCopyInstance::setMaxLevel( int maxLevel )
{
	m_maxLevel = maxLevel;
}

int SingCopyInstance::getMaxlevel()
{
	return m_maxLevel;
}

int SingCopyInstance::m_maxLevel = 0;

SingCopyInstance * SingCopyInstance::s_singCopyInstance = NULL;

long long SingCopyInstance::m_nStartTime = 0;

int SingCopyInstance::s_singCopy_level = 1;

int SingCopyInstance::s_singCopy_clazz = 1;

