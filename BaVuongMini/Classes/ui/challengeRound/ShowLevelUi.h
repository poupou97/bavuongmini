#ifndef CHALLENGE_SHOWLEVELUI_H 
#define CHALLENGE_SHOWLEVELUI_H

#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CTowerHistory;
class ShowLevelUi:public UIScene
{
public:
	ShowLevelUi(void);
	~ShowLevelUi(void);

	static ShowLevelUi * create(int clazzIndex);
	bool init(int clazzIndex);
	void onEnter();
	void onExit();

	void showCurLevelContent(Ref *pSender, PageView::EventType type);
	void addPageViewIndex(int pageCount);
	void refreshPageDate();

	void callBackReward(Ref *pSender, Widget::TouchEventType type);
	void callBackRankList(Ref *pSender, Widget::TouchEventType type);
	void callBackRuleDes(Ref *pSender, Widget::TouchEventType type);
	
	void callBackAddPhyPower(Ref *pSender, Widget::TouchEventType type);

	void callBackCloseUi(Ref *pSender, Widget::TouchEventType type);

	void enterCopyLevel(Ref *pSender, Widget::TouchEventType type);
	void enterSureSignCopyLevel(Ref * obj);
	void enterSingCopyAction(Ref * obj);

	int m_clazz;
	int m_everyPageNum;
	void setCurPageIndex(int index);
	int getCurpageIndex();

	std::vector<CTowerHistory *> singCopyHistoryVector;
	void setTodaymaxlevel(int level_);
	int getTodaymaxLevel();

	void setCurLevel(int value_);
	int getCurLevel();

public:
	//��ѧ
	int mTutorialScriptInstanceId;
	int mTutorialIndex;
	//��ѧ
	virtual void registerScriptCommand(int scriptId);

	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();
public:
	Text * labelTodayMaxLevel;
	Text * labelTodayMaxUsetime;
	//Label * labelpageIndex;
private:
	Size winsize;
	PageView * m_pageView;
	int m_curPageIndex;
	int m_todaymaxLevel;
	int m_curLevel;
};

#endif