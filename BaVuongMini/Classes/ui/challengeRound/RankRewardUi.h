
#ifndef CHALLENGEROUDUI_RANREWARDUI_H
#define CHALLENGEROUDUI_RANREWARDUI_H

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CTRankingPrize;

class RankRewardUi:public UIScene,public TableViewDelegate,public TableViewDataSource
{
public:
	RankRewardUi(void);
	~RankRewardUi(void);

	static RankRewardUi * create(int clazz);
	
	bool init(int clazz);

	void onEnter();
	void onExit();

	void callBackClose(Ref *pSender, Widget::TouchEventType type);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);

	void setClazz(int clazz_);
	int getClazz();
public: 
	std::vector<CTRankingPrize *> rankPrizevector;
	Text * label_selfRank;
	Text * Label_clazzName;
	TableView * tableView_reward;
private:
	Size winsize;
	int m_clazz;
};
/////////
class PrizeCell:public TableViewCell
{
public:
	PrizeCell(void);
	~PrizeCell(void);

	static PrizeCell *create(int idx);
	bool init(int idx);

	void onEnter();
	void onExit();

	void callBackShowGoodsInfo(Ref * obj);
	void callBackGetPrize(Ref * obj);

private:
	Size winsize;
};

#endif