#include "ShowLevelUi.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CTowerHistory.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "SingleRankListUi.h"
#include "../../gamescene_state/MainScene.h"
#include "RankRewardUi.h"
#include "ChallengeRoundUi.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../extensions/ShowSystemInfo.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "SingCopyInstance.h"
#include "../../gamescene_state/GameStatistics.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../goldstore_ui/GoldStoreUI.h"
#include "../../messageclient/element/CLable.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "utils/StrUtils.h"
#include "cocostudio\CCSGUIReader.h"
#include "AppMacros.h"

ShowLevelUi::ShowLevelUi(void)
{
	m_curPageIndex = 0;
	m_clazz =1;
	m_everyPageNum = 10;
	m_todaymaxLevel = 0;
	m_curLevel = 1;
}


ShowLevelUi::~ShowLevelUi(void)
{
}

ShowLevelUi * ShowLevelUi::create(int clazzIndex)
{
	auto showLevelui = new ShowLevelUi();
	if (showLevelui && showLevelui->init(clazzIndex))
	{
		showLevelui->autorelease();
		return showLevelui;
	}
	CC_SAFE_DELETE(showLevelui);
	return NULL;
}

bool ShowLevelUi::init(int clazzIndex)
{
	if (UIScene::init())
	{
		winsize= Director::getInstance()->getVisibleSize();
		m_clazz = clazzIndex;

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		auto layer_  = Layer::create();
		layer_->setIgnoreAnchorPointForPosition(false);
		layer_->setAnchorPoint(Vec2(0.5f, 0.5f));
		layer_->setContentSize(Size(720,438));
		layer_->setPosition(Vec2(winsize.width/2, winsize.height/2));
		this->addChild(layer_);


		auto mainPanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/guoguanzhanjiang2_1.json");
		mainPanel->setAnchorPoint(Vec2(0,0));
		mainPanel->setPosition(Vec2(0,0));
		layer_->addChild(mainPanel);

		auto btn_reward = (Button *)Helper::seekWidgetByName(mainPanel,"Button_34");
		btn_reward->setTouchEnabled(true);
		btn_reward->setPressedActionEnabled(true);
		btn_reward->addTouchEventListener(CC_CALLBACK_2(ShowLevelUi::callBackReward, this));

		auto btn_rank = (Button *)Helper::seekWidgetByName(mainPanel,"Button_34_0");
		btn_rank->setTouchEnabled(true);
		btn_rank->setPressedActionEnabled(true);
		btn_rank->addTouchEventListener(CC_CALLBACK_2(ShowLevelUi::callBackRankList, this));
		
		auto btn_ruleDes = (Button *)Helper::seekWidgetByName(mainPanel,"Button_34_0_0");
		btn_ruleDes->setTouchEnabled(true);
		btn_ruleDes->setPressedActionEnabled(true);
		btn_ruleDes->addTouchEventListener(CC_CALLBACK_2(ShowLevelUi::callBackRuleDes, this));

		auto btn_close = (Button *)Helper::seekWidgetByName(mainPanel,"Button_close");
		btn_close->setTouchEnabled(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(ShowLevelUi::callBackCloseUi, this));

		std::string levelUI_clazzNameAndlevel = ChallengeRoundUi::getClazzName(m_clazz);
		levelUI_clazzNameAndlevel.append(ChallengeRoundUi::getClazzLevel(m_clazz).c_str());
		auto LabellevelClazz = (Text*)Helper::seekWidgetByName(mainPanel,"Label_60");
		LabellevelClazz->setString(levelUI_clazzNameAndlevel.c_str());

		labelTodayMaxLevel = (Text*)Helper::seekWidgetByName(mainPanel,"Label_28_0");
		labelTodayMaxUsetime = (Text*)Helper::seekWidgetByName(mainPanel,"Label_28_1_0");

		auto btn_phypowerAdd = (Button *)Helper::seekWidgetByName(mainPanel,"Button_lintili_jiahao");
		btn_phypowerAdd->setTouchEnabled(true);
		btn_phypowerAdd->setPressedActionEnabled(true);
		btn_phypowerAdd->addTouchEventListener(CC_CALLBACK_2(ShowLevelUi::callBackAddPhyPower, this));

		int curPhysicalValue = GameView::getInstance()->myplayer->getPhysicalValue();
		int allPhysicalValue = GameView::getInstance()->myplayer->getPhysicalCapacity();


		auto image_phypowerSp = (ImageView *)Helper::seekWidgetByName(mainPanel,"ImageView_tilitiao_");
		auto Label_phyValue = (Text*)Helper::seekWidgetByName(mainPanel, "Label_tiliValue");

		//CCLabel* label = CCLabel::create(str, "res_ui/font/c123.fnt");   // yellow color
		//label->setAnchorPoint(Vec2(0.5f,0.0f));
		//label->setPosition(Vec2(defender->getPosition().x, defender->getPosition().y + BASEFIGHTER_ROLE_HEIGHT*1/3));
		////label->setPosition(label->getPosition() + SimpleEffectManager::getNumberOffset());
		//label->setVisible(false);
		
		image_phypowerSp->setTextureRect(Rect(0, 0, 
			image_phypowerSp->getContentSize().width * 0.0f,
			image_phypowerSp->getContentSize().height));

		// ���������Ķ���Ч�
		auto pPhysicalBarEffect = PhysicalBarEffect::create(curPhysicalValue, allPhysicalValue, 1.5f,
			image_phypowerSp, Label_phyValue);
		this->addChild(pPhysicalBarEffect);

		m_pageView = ui::PageView::create();
		m_pageView->setTouchEnabled(true);
		m_pageView->setAnchorPoint(Vec2(0,0));
		m_pageView->setContentSize(Size(570,257));
		m_pageView->setPosition(Vec2(75,100));
		m_pageView->addEventListener((PageView::ccPageViewCallback)CC_CALLBACK_2(ShowLevelUi::showCurLevelContent, this));
		layer_->addChild(m_pageView);

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		//this->setContentSize(Size(720,438));
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void ShowLevelUi::onEnter()
{
	this->openAnim();
	UIScene::onEnter();
}

void ShowLevelUi::onExit()
{
	UIScene::onExit();
}

void ShowLevelUi::callBackCloseUi(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void ShowLevelUi::addPageViewIndex( int pageCount )
{
	int m_levelIndex = 0;
	for (int pageIndex =0;pageIndex <pageCount;pageIndex++)
	{
		auto layout = Layout::create();
		layout->setContentSize(m_pageView->getContentSize());
		
		for (int i = 0; i <2; ++i)
		{
			for (int j=0;j<5;j++)
			{
				m_levelIndex++;

				auto image_sp = ImageView::create();
				image_sp->loadTexture("res_ui/guoguanzhanjiang/pic_di.png");
				image_sp->setAnchorPoint(Vec2(0.5f,0.5f));
				image_sp->setPosition(Vec2(image_sp->getContentSize().width/2+5 + (image_sp->getContentSize().width+10)*j,
							layout->getContentSize().height/2 + image_sp->getContentSize().height/2 + 5 - (image_sp->getContentSize().height+8)*i));
				layout->addChild(image_sp);

				Color3B levelColor=Color3B(255,255,255);
				int m_curLevel_ =  m_levelIndex;
				auto image_mapIcon = ImageView::create();
				if (m_curLevel_ <= this->getTodaymaxLevel()+1)
				{
					if (m_curLevel_%5 == 0)
					{
						image_mapIcon->loadTexture("res_ui/guoguanzhanjiang/pic_2.png");
					}else
					{
						image_mapIcon->loadTexture("res_ui/guoguanzhanjiang/pic_1.png");
					}
					if (m_curLevel_ <= this->getTodaymaxLevel())
					{
						levelColor = Color3B(0,255,6);
					}else
					{
						levelColor = Color3B(255,246,0);
					}

				}else
				{
					if (m_curLevel_%5 == 0)
					{
						image_mapIcon->loadTexture("res_ui/guoguanzhanjiang/pic_2off.png");
					}else
					{
						image_mapIcon->loadTexture("res_ui/guoguanzhanjiang/pic_1off.png");
					}
				}
				image_mapIcon->setAnchorPoint(Vec2(0.5f,0));
				image_mapIcon->setPosition(Vec2(0,12 - image_mapIcon->getContentSize().height/2));
				image_sp->addChild(image_mapIcon);

				const char *strings_curLevelBmfont = StringDataManager::getString("singleCopy_curLevelLabel");
				char levelStr[10];
				sprintf(levelStr,strings_curLevelBmfont,m_curLevel_);
				auto labellevel_ = Label::createWithBMFont("res_ui/font/ziti_3.fnt", levelStr);
				labellevel_->setAnchorPoint(Vec2(0.5f,0));
				labellevel_->setPosition(Vec2(0,labellevel_->getContentSize().height+2));
				image_sp->addChild(labellevel_);

				auto btn_Enter = Button::create();
				btn_Enter->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
				btn_Enter->setAnchorPoint(Vec2(0.5f,0.5f));
				btn_Enter->setTouchEnabled(true);
				btn_Enter->setScale9Enabled(true);
				btn_Enter->setContentSize(Size(80,32));
				btn_Enter->setPressedActionEnabled(true);
				btn_Enter->addTouchEventListener(CC_CALLBACK_2(ShowLevelUi::enterCopyLevel,this));
				btn_Enter->setTag(m_levelIndex);
				btn_Enter->setPosition(Vec2(0, -image_sp->getContentSize().height/2+btn_Enter->getContentSize().height/2));
				image_sp->addChild(btn_Enter);
				auto label_enter = Label::createWithTTF(StringDataManager::getString("singleCopy_isEnterLevel"), APP_FONT_NAME, 16);
				label_enter->setAnchorPoint(Vec2(0.5f,0.5f));
				label_enter->setPosition(Vec2(0,0));
				btn_Enter->addChild(label_enter);

				auto labelTime_ = Label::createWithTTF("", APP_FONT_NAME, 16);
				labelTime_->setAnchorPoint(Vec2(0.5f,0));
				labelTime_->setTag(m_levelIndex);
				labelTime_->setColor(levelColor);
				
				std::string useTimeStr = "";
				if (singCopyHistoryVector.size() >= m_levelIndex)
				{
					int temp_useTime  = singCopyHistoryVector.at(m_levelIndex - 1)->usetime()/1000;
					useTimeStr.append(RecuriteActionItem::timeFormatToString(temp_useTime));

					auto image_1 = ImageView::create();
					image_1->loadTexture("res_ui/guoguanzhanjiang/mengban.png");
					image_1->setScale9Enabled(true);
					image_1->setContentSize(Size(80,20));
					//image_1->setCapInsets(Rect(11,12,1,1));
					image_1->setPosition(Vec2(0,-10));
					image_sp->addChild(image_1);

					labelTime_->setString(useTimeStr.c_str());
					labelTime_->setPosition(Vec2(0,-labelTime_->getContentSize().height));
					image_sp->addChild(labelTime_);
					btn_Enter->setVisible(true);
				}else
				{
					if (m_levelIndex == singCopyHistoryVector.size() +1)
					{
						auto image_1 = ImageView::create();
						image_1->loadTexture("res_ui/guoguanzhanjiang/mengban.png");
						image_1->setScale9Enabled(true);
						image_1->setContentSize(Size(80,20));
						image_1->setCapInsets(Rect(11,12,1,1));
						image_1->setPosition(Vec2(0,-10));//0,-10
						image_sp->addChild(image_1);

						const char *strings = StringDataManager::getString("singleCopy_isnotOpen");
						useTimeStr.append(strings);

						labelTime_->setString(useTimeStr.c_str());
						labelTime_->setPosition(Vec2(0,-labelTime_->getContentSize().height));
						image_sp->addChild(labelTime_);
						btn_Enter->setVisible(true);
					}else
					{
						auto image_useTime = ImageView::create();
						image_useTime->loadTexture("res_ui/LV4_diaa.png");
						image_useTime->setScale9Enabled(true);
						image_useTime->setContentSize(Size(80,25));
						image_useTime->setCapInsets(Rect(11,12,1,1));
						image_useTime->setPosition(Vec2(0,-image_sp->getContentSize().height/2+image_useTime->getContentSize().height/2+10));//7
						image_sp->addChild(image_useTime);

						const char *strings = StringDataManager::getString("singleCopy_isOpenUp");
						useTimeStr.append(strings);

						labelTime_->setString(useTimeStr.c_str());
						labelTime_->setPosition(Vec2(0, -labelTime_->getContentSize().height/2 ));
						image_useTime->addChild(labelTime_);

						btn_Enter->setVisible(false);
					}
				}
			}
		}        
		m_pageView->addPage(layout);
	}
}

void ShowLevelUi::refreshPageDate()
{
}


void ShowLevelUi::showCurLevelContent(Ref *pSender, PageView::EventType type)
{
}

void ShowLevelUi::callBackReward(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//ActiveUI * activeui = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
		auto challengeui_ = (ChallengeRoundUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);

		auto ranklist_ = RankRewardUi::create(challengeui_->getCopyClazz());
		ranklist_->setIgnoreAnchorPointForPosition(false);
		ranklist_->setAnchorPoint(Vec2(0.5f, 0.5f));
		ranklist_->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
		ranklist_->setTag(ktagSingCopyReward);
		GameView::getInstance()->getMainUIScene()->addChild(ranklist_);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1905, (void *)challengeui_->getCopyClazz());
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void ShowLevelUi::callBackRankList(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto ranklist_ = SingleRankListUi::create();
		ranklist_->setIgnoreAnchorPointForPosition(false);
		ranklist_->setAnchorPoint(Vec2(0.5f, 0.5f));
		ranklist_->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
		ranklist_->setTag(ktagSingCopyRank);
		GameView::getInstance()->getMainUIScene()->addChild(ranklist_);

		GameMessageProcessor::sharedMsgProcessor()->sendReq(1904, ranklist_);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void ShowLevelUi::callBackRuleDes(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		std::map<int, std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(8);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end())
		{
			return;
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			{
				auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[8].c_str());
				GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				showSystemInfo->setIgnoreAnchorPointForPosition(false);
				showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				showSystemInfo->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void ShowLevelUi::callBackAddPhyPower(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
		if (goldStoreUI == NULL)
		{
			goldStoreUI = GoldStoreUI::create();
			GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI, 0, kTagGoldStoreUI);
			goldStoreUI->setIgnoreAnchorPointForPosition(false);
			goldStoreUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			goldStoreUI->setPosition(Vec2(winsize.width / 2, winsize.height / 2));

			for (int i = 0; i <GameView::getInstance()->goldStoreList.size(); i++)
			{

				if (GameView::getInstance()->goldStoreList.at(i)->id() == GoldStoreUI::type_phypower)
				{
					goldStoreUI->getTab_function()->setDefaultPanelByIndex(i);
					goldStoreUI->IndexChangedEvent(goldStoreUI->getTab_function());
				}
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}


void ShowLevelUi::setCurPageIndex( int index )
{
	m_curPageIndex = index;
}

int ShowLevelUi::getCurpageIndex()
{
	return m_curPageIndex;
}

void ShowLevelUi::setTodaymaxlevel( int level_ )
{
	m_todaymaxLevel = level_;
}

int ShowLevelUi::getTodaymaxLevel()
{
	return m_todaymaxLevel;
}

void ShowLevelUi::enterCopyLevel(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(this);


		auto btn = (Button *)pSender;
		this->setCurLevel(btn->getTag());

		int curPage_ = m_pageView->getCurrentPageIndex();
		int m_curLevel = this->getCurLevel();

		if (singCopyHistoryVector.size() + 1 >= m_curLevel)
		{

			auto challengeui_ = (ChallengeRoundUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);
			int costResume = challengeui_->getCopyCostResume();
			char resumeAndLevelStr[100];
			const char *strings = StringDataManager::getString("singleCopy_isCostResumeEnterLevel");
			sprintf(resumeAndLevelStr, strings, costResume);

			std::string temp_str = "";
			temp_str.append(resumeAndLevelStr);
			std::string temp_clazz = "";
			switch (m_clazz)
			{
			case 1:
			{
				std::string str_1 = StrUtils::applyColor(StringDataManager::getString("singCopy_firstClazzName_chushiniudao"), Color3B(243, 252, 2));
				temp_clazz.append(str_1.c_str());
			}break;
			case 2:
			{
				std::string str_2 = StrUtils::applyColor(StringDataManager::getString("singCopy_secondClazzName_xiaoyoumingqi"), Color3B(243, 252, 2));
				temp_clazz.append(str_2.c_str());
			}break;
			case 3:
			{
				std::string str_3 = StrUtils::applyColor(StringDataManager::getString("singCopy_thirdClazzName_jiangongliye"), Color3B(243, 252, 2));
				temp_clazz.append(str_3);
			}break;
			}
			temp_str.append(temp_clazz);

			char lev_str[100];
			const char * strings_lev_str = StringDataManager::getString("singleCopy_isCostResumeEnterLevel_append");
			sprintf(lev_str, strings_lev_str, this->getCurLevel());
			std::string strLevel_temp = StrUtils::applyColor(lev_str, Color3B(243, 252, 2));
			temp_str.append(strLevel_temp);
			//GameView::getInstance()->showPopupWindow(temp_str.c_str(),2,this,SEL_CallFuncO(ShowLevelUi::enterSureSignCopyLevel),NULL);

			Size winSize = Director::getInstance()->getVisibleSize();
			auto dialog = PopupWindow::create(temp_str.c_str(), 2, PopupWindow::KtypeNeverRemove, 0);
			dialog->setIgnoreAnchorPointForPosition(false);
			dialog->setAnchorPoint(Vec2(0.5f, 0.5f));
			dialog->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			dialog->setTag(101011);
			dialog->AddcallBackEvent(this, CC_CALLFUNCO_SELECTOR(ShowLevelUi::enterSureSignCopyLevel), NULL);
			GameView::getInstance()->getMainUIScene()->addChild(dialog);


			/*
			SingCopyInstance::setCurSingCopyClazz(m_clazz);
			SingCopyInstance::setCurSingCopyLevel(this->getCurLevel());
			this->callBackCloseUi(NULL);
			if ( challengeui_)
			{
			challengeui_->callBackCloseUi(NULL);
			}
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1901,(void *)m_clazz,(void *)this->getCurLevel());
			DamageStatistics::clear();
			*/
		}
		else
		{
			const char *strings = StringDataManager::getString("singleCopy_isOpenOnLevel");
			GameView::getInstance()->showAlertDialog(strings);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void ShowLevelUi::enterSureSignCopyLevel( Ref * obj )
{
	
	auto action =(ActionInterval *)Sequence::create(
		DelayTime::create(0.5f),
		CallFuncN::create(CC_CALLBACK_1(ShowLevelUi::enterSingCopyAction, this)),
		NULL);
	runAction(action);

}

void ShowLevelUi::enterSingCopyAction( Ref * obj )
{
	SingCopyInstance::setCurSingCopyClazz(m_clazz);
	SingCopyInstance::setCurSingCopyLevel(this->getCurLevel());
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1901,(void *)m_clazz,(void *)this->getCurLevel());

	DamageStatistics::clear();

	this->callBackCloseUi(NULL, Widget::TouchEventType::ENDED);
	
	auto challengeui_ = (ChallengeRoundUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagChallengeRoundUi);
	if (challengeui_)
	{
		challengeui_->callBackCloseUi(NULL, Widget::TouchEventType::ENDED);
	}
	
}

void ShowLevelUi::setCurLevel( int value_ )
{
	m_curLevel = value_;
}

int ShowLevelUi::getCurLevel()
{
	return m_curLevel;
}

void ShowLevelUi::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void ShowLevelUi::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,88,38,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+170+_w,pos.y+276+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void ShowLevelUi::removeCCTutorialIndicator()
{
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}






