
#ifndef SINGLECOPYRESULT_H_
#define SINGLECOPYRESULT_H_

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocostudio\CCArmature.h"
#include "cocostudio\CCProcessBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class SingleCopyResult:public UIScene
{
public:
	SingleCopyResult(void);
	~SingleCopyResult(void);

	static SingleCopyResult * create(int clazz,int level,int curTime,int bestTime,int money,int result);
	bool init(int clazz,int level,int curTime,int bestTime,int money,int result);

	void onEnter();
	void onExit();

	void showUIAnimation();
	void startSlowMotion();
	void endSlowMotion();
	void addLightAnimation();
	void createUI();
	virtual void update(float dt);

	void callBackCloseUi(Ref *pSender, Widget::TouchEventType type);

	void callBackEnterNextLevel(Ref *pSender, Widget::TouchEventType type);
	void callBackEnterCurLevel(Ref *pSender, Widget::TouchEventType type);
	
	void ArmatureFinishCallback(cocostudio::Armature *armature, cocostudio::MovementEventType movementType, const char *movementID);

private:
	Size winsize;
	Layer *layer_anm;
	int m_clazz;
	int m_level;
	int m_curTime;
	int m_bestTime;
	int m_money;
	int m_result;
	
	int m_nRemainTime;
	Text * labelCOuntTime;



};

#endif