#include "RankRewardUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../messageclient/element/CTRankingPrize.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "ChallengeRoundUi.h"
#include "cocostudio\CCSGUIReader.h"
#include "AppMacros.h"

RankRewardUi::RankRewardUi(void)
{
	m_clazz = 1;
}


RankRewardUi::~RankRewardUi(void)
{
}

RankRewardUi * RankRewardUi::create(int clazz)
{
	auto rewardui = new RankRewardUi();
	if (rewardui && rewardui->init(clazz))
	{
		rewardui->autorelease();
		return rewardui;
	}
	CC_SAFE_DELETE(rewardui);
	return NULL;
}

bool RankRewardUi::init(int clazz)
{
	if (UIScene::init())
	{
		winsize = Director::getInstance()->getVisibleSize();

// 		ImageView * imageSp = ImageView::create();
// 		imageSp->loadTexture("res_ui/LV4_red.png");
// 		imageSp->setScale9Enabled(true);
// 		imageSp->setContentSize(Size(200,300));
// 		imageSp->setAnchorPoint(Vec2(0,0));
// 		imageSp->setPosition(Vec2(0,0));
// 		m_pLayer->addChild(imageSp);
		
		auto ppanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/familyRankReward_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->setPosition(Vec2::ZERO);	
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		label_selfRank = (Text*)Helper::seekWidgetByName(ppanel,"label_singCopyRankValue");
		Label_clazzName = (Text*)Helper::seekWidgetByName(ppanel,"Label_Clazzname");

		this->setClazz(clazz);
		Label_clazzName->setString(ChallengeRoundUi::getClazzName(this->getClazz()).c_str());

		tableView_reward = TableView::create(this,Size(275,250));
		tableView_reward->setDirection(TableView::Direction::VERTICAL);
		tableView_reward->setAnchorPoint(Vec2(0,0));
		tableView_reward->setPosition(Vec2(49,35));
		tableView_reward->setDelegate(this);
		tableView_reward->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(tableView_reward);

		auto btnClose = (Button *)Helper::seekWidgetByName(ppanel,"Button_close");
		btnClose->setTouchEnabled(true);
		btnClose->setPressedActionEnabled(true);
		btnClose->addTouchEventListener(CC_CALLBACK_2(RankRewardUi::callBackClose,this));

		this->setContentSize(ppanel->getContentSize());
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}

void RankRewardUi::onEnter()
{
	UIScene::onEnter();
}

void RankRewardUi::onExit()
{
	this->openAnim();
	UIScene::onExit();
}

void RankRewardUi::callBackClose(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void RankRewardUi::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void RankRewardUi::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void RankRewardUi::tableCellTouched( TableView* table, TableViewCell* cell )
{
	
}

cocos2d::Size RankRewardUi::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(275,64);
}

cocos2d::extension::TableViewCell* RankRewardUi::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto prizecell_ = PrizeCell::create(idx);
	prizecell_->setIgnoreAnchorPointForPosition(false);
	prizecell_->setAnchorPoint(Vec2(0,0));
	prizecell_->setPosition(Vec2(0,0));
	cell->addChild(prizecell_);

	return cell;
}

ssize_t RankRewardUi::numberOfCellsInTableView( TableView *table )
{
	return rankPrizevector.size();
}

bool RankRewardUi::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void RankRewardUi::setClazz( int clazz_ )
{
	m_clazz = clazz_;
}

int RankRewardUi::getClazz()
{
	return m_clazz;
}


/////////////////////////////
PrizeCell::PrizeCell( void )
{

}

PrizeCell::~PrizeCell( void )
{

}

PrizeCell * PrizeCell::create( int idx )
{
	auto cell_ = new PrizeCell();
	if (cell_ && cell_->init(idx))
	{
		cell_->autorelease();
		return cell_;
	}
	CC_SAFE_DELETE(cell_);
	return NULL;
}

bool PrizeCell::init( int idx )
{
	if (TableViewCell::init())
	{
		auto rankui_ = (RankRewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyReward);
		
		auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
		sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
		sprite_bigFrame->setPosition(Vec2(0, 0));
		sprite_bigFrame->setCapInsets(Rect(9,14,1,1));
		sprite_bigFrame->setPreferredSize(Size(273,62));
		addChild(sprite_bigFrame);

		int starlevel_ = rankui_->rankPrizevector.at(idx)->startranking();
		int endlevel_ = rankui_->rankPrizevector.at(idx)->endranking();
		int offx_ = 0;
		char starStr[10];
		sprintf(starStr,"%d",starlevel_);
		char endStr[10];
		sprintf(endStr,"%d",endlevel_);

		if (starlevel_ == endlevel_)
		{
			std::string icon_path = "res_ui/rank";
			icon_path.append(starStr);
			icon_path.append(".png");

			auto sp_ranking = Sprite::create(icon_path.c_str());
			sp_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
			sp_ranking->setPosition(Vec2(sp_ranking->getContentSize().width,32));
			addChild(sp_ranking);

			offx_ = sp_ranking->getPosition().x + sp_ranking->getContentSize().width;
		}else
		{
			const char * string_ = StringDataManager::getString("singCopyReward_sortName");
			const char * string_levelAndlevel = StringDataManager::getString("singCopy_levelAndLevel_");
			
			std::string rankStirng_ = starStr;
			rankStirng_.append(string_levelAndlevel);
			rankStirng_.append(endStr);
			rankStirng_.append(string_);

			auto label_ranking = Label::createWithTTF(rankStirng_.c_str(), APP_FONT_NAME, 16);
			label_ranking->setAnchorPoint(Vec2(0.5f,0.5f));
			label_ranking->setColor(Color3B(47, 93, 13));
			label_ranking->setPosition(Vec2(34,32));			
			addChild(label_ranking);
			offx_ = label_ranking->getPosition().x + label_ranking->getContentSize().width;
		}
		offx_ = 80;
		auto goodsBg_ =Sprite::create("res_ui/sdi_orange.png");
		auto item_goodsBg = MenuItemSprite::create(goodsBg_, goodsBg_, goodsBg_, CC_CALLBACK_1(PrizeCell::callBackShowGoodsInfo,this));
		item_goodsBg->setTag(idx);
		item_goodsBg->setScale(1.0f);
		auto menu_goods =CCMoveableMenu::create(item_goodsBg,NULL);
		menu_goods->setAnchorPoint(Vec2(0,0));
		menu_goods->setPosition(Vec2(offx_ + goodsBg_->getContentSize().width/2,32));
		menu_goods->setScale(0.8f);
		//menu_goods->setTag(idx);
		addChild(menu_goods);
		
		std::string goodsInfoStr_ = "res_ui/props_icon/";
		goodsInfoStr_.append(rankui_->rankPrizevector.at(idx)->goods().icon());
		goodsInfoStr_.append(".png");

		auto sp_goodsIcon = Sprite::create(goodsInfoStr_.c_str());
		sp_goodsIcon->setAnchorPoint(Vec2(0.5f,0.5f));
		sp_goodsIcon->setPosition(Vec2(menu_goods->getPositionX(),32));
		addChild(sp_goodsIcon);
		
		if (rankui_->rankPrizevector.at(idx)->canreceive())
		{
			auto prize_bgsp = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_2.png");
			prize_bgsp->setPreferredSize(Size(80,42));
			prize_bgsp->setCapInsets(Rect(18,9,2,23));
			auto item_prize = MenuItemSprite::create(prize_bgsp, prize_bgsp, prize_bgsp, CC_CALLBACK_1(PrizeCell::callBackGetPrize,this));
			item_prize->setScale(0.5f);
			auto menu_prize = CCMoveableMenu::create(item_prize,NULL);
			menu_prize->setAnchorPoint(Vec2(0,0));
			menu_prize->setPosition(Vec2(menu_goods->getPosition().x + goodsBg_->getContentSize().width + prize_bgsp->getContentSize().width/2,32));
			menu_prize->setTag(1010);
			addChild(menu_prize);

			const char * string_prize = StringDataManager::getString("singCopyReward_getPrize");
			auto label_Prize = Label::createWithTTF(string_prize, APP_FONT_NAME, 16);
			label_Prize->setAnchorPoint(Vec2(0.5f,0.5f));
			label_Prize->setPosition(Vec2(menu_prize->getPositionX(),32));
			label_Prize->setString(string_prize);
			addChild(label_Prize);
		}
		
		return true;
	}
	return false;
}

void PrizeCell::onEnter()
{
	TableViewCell::onEnter();
}

void PrizeCell::onExit()
{
	TableViewCell::onExit();
}

void PrizeCell::callBackShowGoodsInfo( Ref * obj )
{
	auto btn_ = (MenuItemSprite *)obj;
	int index_ = btn_->getTag();
	
	auto rankui_ = (RankRewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyReward);
	auto goodsInfo_ = new GoodsInfo();
	goodsInfo_->CopyFrom(rankui_->rankPrizevector.at(index_)->goods());

	auto goodsInfo = GoodsItemInfoBase::create(goodsInfo_,GameView::getInstance()->EquipListItem,0);
	goodsInfo->setIgnoreAnchorPointForPosition(false);
	goodsInfo->setAnchorPoint(Vec2(0.5f,0.5f));
	GameView::getInstance()->getMainUIScene()->addChild(goodsInfo);
	delete goodsInfo_;
}

void PrizeCell::callBackGetPrize( Ref * obj )
{
	auto rankui_ = (RankRewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyReward);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1906,(void *)rankui_->getClazz());
}
