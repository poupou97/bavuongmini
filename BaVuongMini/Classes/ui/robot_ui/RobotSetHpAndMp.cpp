#include "RobotSetHpAndMp.h"
#include "../../messageclient/element/FolderInfo.h"
#include "RobotMainUI.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/MapRobotDrug.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"


RobotSetHpAndMp::RobotSetHpAndMp(void)
{
}


RobotSetHpAndMp::~RobotSetHpAndMp(void)
{
}

RobotSetHpAndMp * RobotSetHpAndMp::create(int drugtype)
{
	auto setHpMp =new RobotSetHpAndMp();
	if (setHpMp && setHpMp->init(drugtype))
	{
		setHpMp->autorelease();
		return setHpMp;
	}
	CC_SAFE_DELETE(setHpMp);
	return NULL;

}

bool RobotSetHpAndMp::init(int drugtype)
{
	if (UIScene::init())
	{

		m_drugType = drugtype;

		auto background = cocos2d::extension::Scale9Sprite::create("res_ui/dikuang_1.png");
		background->setCapInsets(Rect(10,10,1,1));
		background->setPreferredSize(Size(320,105));
		background->setAnchorPoint(Vec2(0,0));
		background->setPosition(Vec2(0,0));
		this->addChild(background);

		auto robotSetHpAdnMp = TableView::create(this,Size(300,110));
		robotSetHpAdnMp->setDirection(TableView::Direction::HORIZONTAL);
		robotSetHpAdnMp->setAnchorPoint(Vec2(0,0));
		robotSetHpAdnMp->setPosition(Vec2(10,5));
		robotSetHpAdnMp->setDelegate(this);
		robotSetHpAdnMp->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(robotSetHpAdnMp);

		robotSetHpAdnMp->reloadData();

		this->setContentSize(Size(300,110));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(RobotSetHpAndMp::onTouchBegan, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void RobotSetHpAndMp::onEnter()
{
	UIScene::onEnter();
}

void RobotSetHpAndMp::onExit()
{
	UIScene::onExit();
}

bool RobotSetHpAndMp::onTouchBegan( Touch *touch, Event * pEvent )
{
	return resignFirstResponder(touch,this,false);
}

void RobotSetHpAndMp::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void RobotSetHpAndMp::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void RobotSetHpAndMp::tableCellTouched( TableView* table, TableViewCell* cell )
{
 	int index =cell->getIdx();
 	//add select goods
	auto robotui = (RobotMainUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
 
	std::map<int,MapRobotDrug *> * robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[m_drugType];
	
	std::string goodsIcon_;
	std::string goodsId_;
	std::string  goodsName_ ;
	if (index ==0)
	{
		goodsIcon_ = robotdrugMap->at(1)->get_icon();
		goodsId_ = robotdrugMap->at(1)->get_id();
		goodsName_ = robotdrugMap->at(1)->get_name();
	}else
	{
		goodsIcon_ = robotdrugMap->at(index*15)->get_icon();
		goodsId_ = robotdrugMap->at(index*15)->get_id();
		goodsName_ = robotdrugMap->at(index*15)->get_name();
	}

 	robotui->addUseGoods(goodsIcon_,goodsId_,goodsName_);
	this->removeFromParentAndCleanup(true);
}

cocos2d::Size RobotSetHpAndMp::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(83,110);
}

cocos2d::extension::TableViewCell* RobotSetHpAndMp::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto lvSp = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_dididi.png");
	lvSp->setCapInsets(Rect(9,10,1,1));
	lvSp->setPreferredSize(Size(77,21));
	lvSp->setAnchorPoint(Vec2(0,0));
	lvSp->setPosition(Vec2(3,0));
	cell->addChild(lvSp);

	auto spBg = Sprite::create("res_ui/di_white.png");
	spBg->setAnchorPoint(Vec2(0, 0));
	spBg->setPosition(Vec2(0,lvSp->getContentSize().height));
	cell->addChild(spBg);

	//map data
	std::map<int,MapRobotDrug *> * robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[m_drugType];
	std::string goodsIcon_;
	std::string  goodsName_ ;
	int goodsUseLevel;
	if (idx ==0)
	{
		goodsIcon_ = robotdrugMap->at(1)->get_icon();
		goodsName_ = robotdrugMap->at(1)->get_name();
		goodsUseLevel = robotdrugMap->at(1)->get_level();
	}else
	{
		goodsIcon_ = robotdrugMap->at(idx*15)->get_icon();
		goodsName_ = robotdrugMap->at(idx*15)->get_name();
		goodsUseLevel = robotdrugMap->at(idx * 15)->get_level();
	}

	std::string headImageStr ="res_ui/props_icon/";
	headImageStr.append(goodsIcon_);
	headImageStr.append(".png");

	auto spGoods=Sprite::create(headImageStr.c_str());
	spGoods->setAnchorPoint(Vec2(0,0));
	spGoods->setPosition(Vec2((spBg->getContentSize().width - spGoods->getContentSize().width) / 2,
								(spBg->getContentSize().height - spGoods->getContentSize().height) / 2 + lvSp->getContentSize().height));
	cell->addChild(spGoods);

	auto label_name =Label::createWithTTF(goodsName_.c_str(), APP_FONT_NAME, 12);
	label_name->setAnchorPoint(Vec2(0,0));
	label_name->setPosition(Vec2((spBg->getContentSize().width - label_name->getContentSize(). width) / 2,5+lvSp->getContentSize().height));
	cell->addChild(label_name);
	
	char levelStr[10];
	sprintf(levelStr,"%d",goodsUseLevel);
	std::string levelString_ ="LV";
	levelString_.append(levelStr);
	auto label_useLevel =Label::createWithTTF(levelString_.c_str(), APP_FONT_NAME, 14);
	label_useLevel->setAnchorPoint(Vec2(0.5f,0));
	label_useLevel->setPosition(Vec2(lvSp->getContentSize().width/2 +3 ,2));
	cell->addChild(label_useLevel);

	return cell;
}

ssize_t RobotSetHpAndMp::numberOfCellsInTableView( TableView *table )
{
	std::map<int,MapRobotDrug *> * robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[m_drugType];
	int mapsiez = robotdrugMap->size();
	return mapsiez;
}
