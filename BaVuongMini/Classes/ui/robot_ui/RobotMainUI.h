#ifndef _UI_ROBOTMAINUI_H_
#define _UI_ROBOTMAINUI_H_

#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;
/**
  * 挂机模块UI
  * @author zhaogang
  */
class MainScene;
class UITab;
enum
{
	STARTROBOTSTATE =0,
	ENDROBOTSTATE =1,
};
class RobotMainUI : public UIScene
{
public:
	RobotMainUI(void);
	~RobotMainUI(void);

	static RobotMainUI *create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	void callBackExit(Ref *pSender, Widget::TouchEventType type);
	////////change by liuzhenxing
	void initFightPanel();
	void initProtectPanel();
	void initGoodsPanel();

	void setGoodPickAndSellOption(int robotState);

	void changeTabEvent(Ref * obj);
	void moveRangeConfig(Ref *pSender, ui::Slider::EventType type);
	
	void setAutomaticskill(Ref* pSender, CheckBox::EventType type);
	void popupBagList(Ref *pSender, Widget::TouchEventType type);
	void addUseGoods(std::string goodsIcon ,std::string goodsid,std::string goodsName);
	void showAutomaticGoods(std::string goodsName);

	void callBackStartRobot(Ref *pSender, Widget::TouchEventType type);
	void callBackStopRobot(Ref *pSender, Widget::TouchEventType type);
	void callBackSkillList(Ref *pSender, Widget::TouchEventType type);

	//set skill type
	std::string getSkillTypeStr(const  char * skillid);

private:
	int curSliderMove;
	int curSliderHp;
	int curSliderMp;
	int curSliderGeneralHp;
	int curSliderGeneralMp;
public:
	Layout* battlePanel;
	Layout* protectionPanel;
	Layout * goodsPanel; 
	Button * buttonSkillIcon;
	int buttonSkillIconIndex;

	ImageView * imageRoleHp;
	ImageView * imageRoleMp;
	ImageView * imageGeneralHp;
	ImageView * imageGeneralMp;
	
	Text * label_roleHp;
	Text * label_roleMp;
	Text * label_GeneralHp;
	Text * label_GeneralMp;

	Text * labelRolebuyHpId_;
	Text * labelRolebuyMpId_;
	Text * labelGeneralbuyHpId_;
	Text * labelGeneralbuyMpId_;
	
	CheckBox * checkBoxSetUseHp;
	CheckBox * checkBoxSetUseMp;
	CheckBox * checkBoxbuyHp;
	CheckBox * checkBoxbuyMp;

	CheckBox * checkBoxGeneralHp;
	CheckBox * checkBoxGeneralMp;
	CheckBox * checkBoxGeneralBuyHp;
	CheckBox * checkBoxGeneralBuyMp;

	Button * buttonProtectBg;
private:
	void initSkillButton(Layout* mainPanel, int index);

	Size winsize;
	MainScene *mainLayer;
	Layout * mainPanel;

	Text * moveRangeLabel;
	//int m_moveRange;
	Text *rolesliderHplabel;
	Text *rolesliderMplabel;
	Text * generalSliderHplabel;
	Text * generalSliderMplabel;

// 	Label * roleBuyHpLabel;
// 	Label * roleBuyMpLabel;
// 	Label * generalBuyHplabel;
// 	Label * generalBuyMplabel;

	void setsliderHp(Ref *pSender, ui::Slider::EventType type);
	void setsliderMp(Ref *pSender, ui::Slider::EventType type);
	void setsliderGeneralHp(Ref *pSender, ui::Slider::EventType type);
	void setsliderGeneralMp(Ref *pSender, ui::Slider::EventType type);
	
	void setCheckBoxuseHp(Ref* pSender, CheckBox::EventType type);
	void setCheckBoxuseMp(Ref* pSender, CheckBox::EventType type);
	void setCheckBoxBuyHp(Ref* pSender, CheckBox::EventType type);
	void setCheckBoxBuyMp(Ref* pSender, CheckBox::EventType type);
	void setCheckBoxGeneralHp(Ref* pSender, CheckBox::EventType type);
	void setCheckBoxGeneralMp(Ref* pSender, CheckBox::EventType type);
	void setCheckBoxGeneralBuyHp(Ref* pSender, CheckBox::EventType type);
	void setCheckBoxGeneralBuyMp(Ref* pSender, CheckBox::EventType type);
	void setCheckBoxRepairEquip(Ref* pSender, CheckBox::EventType type);
	///goods panel
	int m_checkBoxGoodsIndex;
	void setGoodsPanelAllChexkBox(Ref* pSender, CheckBox::EventType type);
public:
	//vip open
	void showVipFunctionOpen(std::string functionName);

	//study
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void addCCTutorialIndicator2(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void addCCTutorialIndicatorClose(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	int mTutorialScriptInstanceId;

	Button * btn_Exit;
	UITab * robotTab;
};

#endif