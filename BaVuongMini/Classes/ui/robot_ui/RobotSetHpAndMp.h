
#ifndef _ROBOTUI_ROBOTSETHPANDMP_H
#define _ROBOTUI_ROBOTSETHPANDMP_H

#include "../extensions/UIScene.h"
class FolderInfo;
class RobotSetHpAndMp: public UIScene,public TableViewDelegate,public TableViewDataSource
{
public:
	RobotSetHpAndMp(void);
	~RobotSetHpAndMp(void);

	//1 roleHP  3 roleMp  2generalHp  4generalMp
	static RobotSetHpAndMp * create(int drugtype);
	bool init(int drugtype);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);
private:
	std::vector<FolderInfo *>goodsVector;
	int m_drugType;
	int drugLevel;
};

#endif;