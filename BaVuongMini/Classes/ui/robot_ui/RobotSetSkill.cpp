#include "RobotSetSkill.h"
#include "../extensions/UITab.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../GameView.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "RobotMainUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "AppMacros.h"
#include "../generals_ui/GeneralsShortcutSlot.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../ui/extensions/CCRichLabel.h"

#define  GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG 1211
#define  SelectImageTag 458

RobotSetSkill::RobotSetSkill(void)
{
}


RobotSetSkill::~RobotSetSkill(void)
{
	std::vector<GameFightSkill *>::iterator iter;
	for (iter =generalSkillVector.begin();iter!=generalSkillVector.end();iter++)
	{
		delete * iter;
	}
	generalSkillVector.clear();
}

RobotSetSkill * RobotSetSkill::create(  )
{
	auto robotkill =new RobotSetSkill();
	if (robotkill && robotkill->init())
	{
		robotkill->autorelease();
		return robotkill;
	}
	CC_SAFE_DELETE(robotkill);
	return NULL;
}

bool RobotSetSkill::init( )
{
	if (UIScene::init())
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		
		std::map<std::string, GameFightSkill*>::iterator it;
		for (it = GameView::getInstance()->GameFightSkillList.begin(); it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
		{
			GameFightSkill * tempSkill = it->second;
			if (tempSkill->getCBaseSkill()->usemodel() == 0)
			{
				GameFightSkill * skills_ =new GameFightSkill();
				skills_->initSkill(tempSkill->getId(),tempSkill->getId(),tempSkill->getLevel(),GameActor::type_player);
				generalSkillVector.push_back(skills_);
			}
		}

		//���UI
		if(LoadSceneLayer::GeneralsSkillListLayer->getParent() != NULL)
		{
			LoadSceneLayer::GeneralsSkillListLayer->removeFromParentAndCleanup(false);
		}
		auto ppanel = LoadSceneLayer::GeneralsSkillListLayer;
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(703, 416));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		Layer_skills = Layer::create();
		addChild(Layer_skills);

		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(RobotSetSkill::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);

		auto btn_equip = (Button *)Helper::seekWidgetByName(ppanel,"Button_set");
		btn_equip->setTouchEnabled(true);
		btn_equip->setPressedActionEnabled(true);
		btn_equip->addTouchEventListener(CC_CALLBACK_2(RobotSetSkill::EquipEvent, this));

		i_skillFrame = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_skill_di"); 
		i_skillImage = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_skill"); 
		l_skillName = (Text*)Helper::seekWidgetByName(ppanel,"Label_SkillName"); 
		l_skillType = (Text*)Helper::seekWidgetByName(ppanel,"Label_SkillType"); 
		l_skillLvValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_LvValue"); 
		imageView_skillQuality = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_abc");
		imageView_skillQuality->setVisible(false);
		auto label_skillQuality = (Text*)Helper::seekWidgetByName(ppanel,"label_skillQualityValue");
		label_skillQuality->setVisible(true);

		l_skillMpValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_MpValue"); 
		l_skillCDTimeValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_CDTimeValue"); 
		l_skillDistanceValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_DistanceValue"); 
		
		generalsSkillsList_tableView = TableView::create(this,Size(230,354));
		generalsSkillsList_tableView->setTouchEnabled(true);
		//generalsSkillsList_tableView->setSelectedScale9Texture("res_ui/highlight.png", Rect(26, 26, 1, 1), Vec2(0,-1));
		generalsSkillsList_tableView->setDirection(TableView::Direction::VERTICAL);
		generalsSkillsList_tableView->setAnchorPoint(Vec2(0,0));
		generalsSkillsList_tableView->setPosition(Vec2(27,30));
		generalsSkillsList_tableView->setDelegate(this);
		//generalsSkillsList_tableView->setPressedActionEnabled(true);
		generalsSkillsList_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(generalsSkillsList_tableView);
		generalsSkillsList_tableView->reloadData();

		if (generalSkillVector.size()>0)
		{
			this->refreshSkillInfo(generalSkillVector.at(0));
			setSkillIndex=0;
		}
		
		this->setContentSize(Size(704,416));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}

void RobotSetSkill::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void RobotSetSkill::onExit()
{
	UIScene::onExit();
}

bool RobotSetSkill::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void RobotSetSkill::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void RobotSetSkill::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void RobotSetSkill::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void RobotSetSkill::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void RobotSetSkill::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void RobotSetSkill::tableCellTouched( TableView* table, TableViewCell* cell )
{
	setSkillIndex = cell->getIdx();
	this->refreshSkillInfo(generalSkillVector.at(setSkillIndex));
}

cocos2d::Size RobotSetSkill::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(227,65);
}

cocos2d::extension::TableViewCell* RobotSetSkill::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang01_new.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(0, 0));
	sprite_bigFrame->setContentSize(Size(225,63));
	sprite_bigFrame->setCapInsets(Rect(15,31,1,1));
	cell->addChild(sprite_bigFrame);

	//�С�߿
// 	cocos2d::extension::Scale9Sprite *  smaillFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_allb.png");
// 	smaillFrame->setAnchorPoint(Vec2(0, 0));
// 	smaillFrame->setPosition(Vec2(18,5));
// 	smaillFrame->setCapInsets(Rect(15,15,1,1));
// 	smaillFrame->setContentSize(Size(52,52));
// 	cell->addChild(smaillFrame);
	auto  smaillFrame = Sprite::create("res_ui/jinengdi.png");
	smaillFrame->setAnchorPoint(Vec2(0, 0));
	smaillFrame->setPosition(Vec2(20,5));
	smaillFrame->setScale(0.91f);
	cell->addChild(smaillFrame);

	auto sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_diaa.png");
	sprite_nameFrame->setAnchorPoint(Vec2(0, 0));
	sprite_nameFrame->setPosition(Vec2(83, 17));
	sprite_nameFrame->setCapInsets(Rect(11,11,1,1));
	sprite_nameFrame->setContentSize(Size(130,30));
	sprite_bigFrame->addChild(sprite_nameFrame);

	std::string skillIconPath = "res_ui/jineng_icon/";

// 	if (strcmp(generalSkillVector.at(idx)->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 		||strcmp(generalSkillVector.at(idx)->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 		||strcmp(generalSkillVector.at(idx)->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 		||strcmp(generalSkillVector.at(idx)->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 	{
// 		skillIconPath.append("jineng_2");
// 	}
// 	else
// 	{
		auto baseSkill = StaticDataBaseSkill::s_baseSkillData[generalSkillVector.at(idx)->getId()];
		if (baseSkill)
		{
			skillIconPath.append(baseSkill->icon());
		}
		else
		{
			skillIconPath.append("jineng_2");
		}
//	}

	skillIconPath.append(".png");
	auto  sprite_image = Sprite::create(skillIconPath.c_str());
	sprite_image->setAnchorPoint(Vec2(0.5f,0.5f));
	sprite_image->setPosition(Vec2(46,31));
	cell->addChild(sprite_image);

	auto  sprite_lvFrame = Sprite::create("res_ui/jineng/jineng_zhezhao.png");
	sprite_lvFrame->setAnchorPoint(Vec2(0.5f,0.f));
	sprite_lvFrame->setPosition(Vec2(45/2+1,0));
	sprite_image->addChild(sprite_lvFrame);

	int curSkillLv = generalSkillVector.at(idx)->getLevel();
	int maxSkillLv = generalSkillVector.at(idx)->getCBaseSkill()->maxlevel();
	std::string skillStr = "";
	char curStrSkill[5];
	sprintf(curStrSkill,"%d",curSkillLv);
	char maxStrSkill[5];
	sprintf(maxStrSkill,"%d",maxSkillLv);
	skillStr.append(curStrSkill);
	skillStr.append("/");
	skillStr.append(maxStrSkill);
	auto skillLv = Label::createWithTTF(skillStr.c_str(),APP_FONT_NAME,12);
	skillLv->setAnchorPoint(Vec2(0.5f, 0.f));
	skillLv->setPosition(Vec2(sprite_lvFrame->getPosition().x,sprite_lvFrame->getPosition().y));
// 	Color3B shadowColorSkill = Color3B(0,0,0); 
// 	skillLv->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColorSkill);
	sprite_image->addChild(skillLv);


// 	Sprite * sprite_highLight = Sprite::create("gamescene_state/zhujiemian3/jinengqu/gaoguang_1.png");
// 	sprite_highLight->setAnchorPoint(Vec2(0.5f, 0.5f));
// 	sprite_highLight->setPosition(Vec2(46,35));
// 	cell->addChild(sprite_highLight);


	//skill variety icon
	auto sprite_skillVariety = SkillScene::GetSkillVarirtySprite(generalSkillVector.at(idx)->getId().c_str());
	if (sprite_skillVariety)
	{
		sprite_skillVariety->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_skillVariety->setPosition(Vec2(60,29));
		sprite_skillVariety->setScale(.85f);
		cell->addChild(sprite_skillVariety);
	}

	auto label_skillName = Label::createWithTTF(generalSkillVector.at(idx)->getCBaseSkill()->name().c_str(),APP_FONT_NAME,18);
	label_skillName->setAnchorPoint(Vec2(0, 0.5f));
	label_skillName->setPosition(Vec2(90,31));
// 	Color3B shadowColor = Color3B(196,255,68);   // black
// 	label_skillName->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	cell->addChild(label_skillName);
	
	return cell;
}

ssize_t RobotSetSkill::numberOfCellsInTableView( TableView *table )
{
	return generalSkillVector.size();
}

void RobotSetSkill::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void RobotSetSkill::EquipEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		CCLOG("equipid = %d", setSkillIndex);
		if (generalSkillVector.size()<0)
		{
			return;
		}

		//if skill is exit show
		std::string skillName_ = generalSkillVector.at(setSkillIndex)->getId();
		//type  activeSkill =0    passiveSkill =2
		int skillType = generalSkillVector.at(setSkillIndex)->getCBaseSkill()->usemodel();

		for (int i = 0; i<GameView::getInstance()->myplayer->getMyPlayerAI()->m_skillList.size(); i++)
		{
			if (skillType == 2)
			{
				const char *str = StringDataManager::getString("robotui_passiveSkillType");
				GameView::getInstance()->showAlertDialog(str);
				return;
			}
		}

		std::string skillIconPath = "res_ui/jineng_icon/";
		// 	if (strcmp(generalSkillVector.at(setSkillIndex)->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
		// 		||strcmp(generalSkillVector.at(setSkillIndex)->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
		// 		||strcmp(generalSkillVector.at(setSkillIndex)->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
		// 		||strcmp(generalSkillVector.at(setSkillIndex)->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
		// 	{
		// 		skillIconPath.append("jineng_2");
		// 	}
		// 	else
		// 	{
		auto baseSkill = StaticDataBaseSkill::s_baseSkillData[generalSkillVector.at(setSkillIndex)->getId()];
		if (baseSkill)
		{
			skillIconPath.append(baseSkill->icon());
		}
		else
		{
			skillIconPath.append("jineng_2");
		}
		//}
		skillIconPath.append(".png");

		auto robotui = (RobotMainUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
		auto btn = (Button *)robotui->battlePanel->getChildByTag(robotui->buttonSkillIconIndex);

		std::string skillImageName = "ImageView_skillbtn_";
		char imageId[3];
		sprintf(imageId, "%d", robotui->buttonSkillIconIndex);
		skillImageName.append(imageId);

		auto imageIcon = (ImageView *)btn->getChildByName(skillImageName.c_str());
		imageIcon->loadTexture(skillIconPath.c_str());
		imageIcon->setVisible(true);
		//add skillType

		std::string imageLight_str = "ImageView_light_";
		char imageLightId[25];
		sprintf(imageLightId, "%d", robotui->buttonSkillIconIndex);
		imageLight_str.append(imageLightId);
		auto imageLight_ = (ImageView *)btn->getChildByName(imageLight_str.c_str());

		std::string imageSkillType_str = "ImageView_skilType_";
		char imageSkillTypeId[25];
		sprintf(imageSkillTypeId, "%d", robotui->buttonSkillIconIndex);
		imageSkillType_str.append(imageLightId);
		auto image_skillType = (ImageView *)imageLight_->getChildByName(imageSkillType_str.c_str());
		std::string str_skillType_string = robotui->getSkillTypeStr(baseSkill->id().c_str());
		image_skillType->loadTexture(str_skillType_string.c_str());
		image_skillType->setVisible(true);


		//
		MyPlayerAIConfig::setSkillList(robotui->buttonSkillIconIndex, generalSkillVector.at(setSkillIndex)->getId());

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

#define GENERALSSHORTCUTSLOT_TAG 785 
void RobotSetSkill::refreshSkillInfo( GameFightSkill * gameFightSkill )
{
	if (Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG))
	{
		Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG)->removeFromParent();
	}

	auto generalsShortCutSlot = GeneralsShortcutSlot::create(gameFightSkill);
	generalsShortCutSlot->setIgnoreAnchorPointForPosition(false);
	generalsShortCutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
	generalsShortCutSlot->setPosition(Vec2(342,336));
	generalsShortCutSlot->setTouchEnabled(false);
	Layer_skills->addChild(generalsShortCutSlot,0,GENERALSSHORTCUTSLOT_TAG);

	l_skillName->setString(gameFightSkill->getCBaseSkill()->name().c_str());

	//��/����
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setString(skillinfo_zhudongjineng);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setString(skillinfo_zhudongjineng);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setString(skillinfo_zhudongjineng);
	}

	char s_level[20];
	sprintf(s_level,"%d",gameFightSkill->getCFightSkill()->level());
	l_skillLvValue->setString(s_level);
	
	char s_mp[20];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	l_skillMpValue->setString(s_mp);
	char s_intervaltime[20];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	l_skillCDTimeValue->setString(s_intervaltime);
	char s_distance[20];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	l_skillDistanceValue ->setString(s_distance);
	
	//show skill des
	this->showSkillDes(gameFightSkill->getCFightSkill()->description(),gameFightSkill->getCFightSkill()->get_next_description());


	if(Layer_skills->getChildByName("panel_levelInfo"))
	{
		Layer_skills->getChildByName("panel_levelInfo")->removeFromParent();
	}
	auto panel_levelInfo  = Layout::create();
	Layer_skills->addChild(panel_levelInfo);
	panel_levelInfo->setName("panel_levelInfo");


	/////////////////////////////////////////////////////
	int m_required_level = 0;
	int m_required_point = 0;
	std::string m_str_pre_skill = "";
	std::string m_pre_skill_id = "";
	int m_pre_skill_level = 0;
	int m_required_gold= 0;
	int m_required_skillBookValue = 0;

	//�ȼ�
	if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
	{
		m_required_level = gameFightSkill->getCFightSkill()->get_next_required_level();
	}
	//���ܵ
	if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
	{
		m_required_point = gameFightSkill->getCFightSkill()->get_next_required_point();
	}
	//XXX㼼�
	if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
	{
		auto temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
		m_str_pre_skill.append(temp->name());
		m_pre_skill_id.append(temp->id());

		m_pre_skill_level = gameFightSkill->getCFightSkill()->get_next_pre_skill_level();
	}
	//ܽ�
	if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
	{
		m_required_gold = gameFightSkill->getCFightSkill()->get_next_required_gold();
	}
	//ҽ����
	if (gameFightSkill->getCFightSkill()->get_next_required_num()>0)
	{
		m_required_skillBookValue = gameFightSkill->getCFightSkill()->get_next_required_num();
	}

	/////////////////////////////////////////////////////
	int lineNum = 0;
	//���ǵȼ�
	if (m_required_level>0)
	{
		lineNum++;
		auto l_playerLv = Label::createWithTTF(StringDataManager::getString("skillinfo_zhujuedengji"), APP_FONT_NAME, 16);
		l_playerLv->setColor(Color3B(47,93,13));
		l_playerLv->setAnchorPoint(Vec2(0,0));
		l_playerLv->setPosition(Vec2(283,103-lineNum*19));
		l_playerLv->setName("l_playerLv");
		panel_levelInfo->addChild(l_playerLv);

		auto l_playerLv_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_playerLv_colon->setColor(Color3B(47,93,13));
		l_playerLv_colon->setAnchorPoint(Vec2(0,0));
		l_playerLv_colon->setPosition(Vec2(347,103-lineNum*19));
		l_playerLv_colon->setName("l_playerLv_colon");
		panel_levelInfo->addChild(l_playerLv_colon);

		char s_next_required_level[20];
		sprintf(s_next_required_level,"%d",m_required_level);

		auto l_playerLvValue = Label::createWithTTF(s_next_required_level, APP_FONT_NAME, 16);
		l_playerLvValue->setColor(Color3B(35,93,13));
		l_playerLvValue->setAnchorPoint(Vec2(0,0));
		l_playerLvValue->setPosition(Vec2(357,103-lineNum*19));
		panel_levelInfo->addChild(l_playerLvValue);
		// 
		if (GameView::getInstance()->myplayer->getActiveRole()->level()<m_required_level)  //����ȼ�δ�ﵽ������Ҫ�
		{
			l_playerLv->setColor(Color3B(212,59,59));
			l_playerLv_colon->setColor(Color3B(212,59,59));
			l_playerLvValue->setColor(Color3B(212,59,59));
		}
	}
	//��ܵ
	if (m_required_point>0)
	{
		lineNum++;

		auto widget_skillPoint = Widget::create();
		panel_levelInfo->addChild(widget_skillPoint);
		widget_skillPoint->setName("widget_skillPoint");
		widget_skillPoint->setAnchorPoint(Vec2(0,0));
		widget_skillPoint->setPosition(Vec2(283,103-lineNum*19));

		std::string str_skillPoint = StringDataManager::getString("skillinfo_jinengdian");
		for(int i = 0;i<3;i++)
		{
			std::string str_name = "l_skillPoint";
			char s_index[5];
			sprintf(s_index,"%d",i);
			str_name.append(s_index);
			auto l_skillPoint = Label::createWithTTF(str_skillPoint.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
			l_skillPoint->setColor(Color3B(47,93,13));
			l_skillPoint->setAnchorPoint(Vec2(0,0));
			l_skillPoint->setName(str_name.c_str());
			widget_skillPoint->addChild(l_skillPoint);
			l_skillPoint->setPosition(Vec2(24*i,0));
		}

		auto l_skillPoint_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_skillPoint_colon->setColor(Color3B(47,93,13));
		l_skillPoint_colon->setAnchorPoint(Vec2(0,0));
		l_skillPoint_colon->setPosition(Vec2(347,103-lineNum*19));
		l_skillPoint_colon->setName("l_skillPoint_colon");
		panel_levelInfo->addChild(l_skillPoint_colon);

		char s_required_point[20];
		sprintf(s_required_point,"%d",m_required_point);
		auto l_skillPointValue = Label::createWithTTF(s_required_point, APP_FONT_NAME, 16);
		l_skillPointValue->setColor(Color3B(47,93,13));
		l_skillPointValue->setAnchorPoint(Vec2(0,0));
		l_skillPointValue->setPosition(Vec2(357,103-lineNum*19));
		panel_levelInfo->addChild(l_skillPointValue);

		if(GameView::getInstance()->myplayer->player->skillpoint()<m_required_point)//㼼�ܵ㲻�
		{
			for(int i = 0;i<3;i++)
			{
				std::string str_name = "l_skillPoint";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				auto l_skillPoint = (Label*)widget_skillPoint->getChildByName(str_name.c_str());
				if (l_skillPoint)
				{
					l_skillPoint->setColor(Color3B(212,59,59));
				}
			}
			l_skillPoint_colon->setColor(Color3B(212,59,59));
			l_skillPointValue->setColor(Color3B(212,59,59));
		}
	}
	//XXX㼼�
	if (m_str_pre_skill != "")
	{
		lineNum++;
		auto widget_pre_skill = Widget::create();
		panel_levelInfo->addChild(widget_pre_skill);
		widget_pre_skill->setName("widget_pre_skill");
		widget_pre_skill->setAnchorPoint(Vec2(0,0));
		widget_pre_skill->setPosition(Vec2(283,103-lineNum*19));
		if (m_str_pre_skill.size()/3 < 4)
		{
			for(int i = 0;i<m_str_pre_skill.size()/3;i++)
			{
				std::string str_name = "l_preSkill";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);

				auto l_preSkill = Label::createWithTTF(m_str_pre_skill.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
				l_preSkill->setColor(Color3B(47,93,13));
				l_preSkill->setAnchorPoint(Vec2(0,0));
				widget_pre_skill->addChild(l_preSkill);
				l_preSkill->setName(str_name.c_str());
				if (m_str_pre_skill.size()/3 == 1)
				{
					l_preSkill->setPosition(Vec2(0,0));
				}
				else if (m_str_pre_skill.size()/3 == 2)
				{
					l_preSkill->setPosition(Vec2(48*i,0));
				}
				else if (m_str_pre_skill.size()/3 == 3)
				{
					l_preSkill->setPosition(Vec2(24*i,0));
				}
			}
		}
		else
		{
			auto l_preSkill = Label::createWithTTF(m_str_pre_skill.c_str(), APP_FONT_NAME, 16);
			l_preSkill->setColor(Color3B(47,93,13));
			l_preSkill->setAnchorPoint(Vec2(0,0));
			l_preSkill->setPosition(Vec2(0,0));
			widget_pre_skill->addChild(l_preSkill);
			l_preSkill->setName("l_preSkill");
		}

		auto l_preSkill_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_preSkill_colon->setColor(Color3B(47,93,13));
		l_preSkill_colon->setAnchorPoint(Vec2(0,0));
		l_preSkill_colon->setPosition(Vec2(347,103-lineNum*19));
		l_preSkill_colon->setName("l_preSkill_colon");
		panel_levelInfo->addChild(l_preSkill_colon);

		char s_pre_skill_level[20];
		sprintf(s_pre_skill_level,"%d",m_pre_skill_level);

		auto l_preSkillValue = Label::createWithTTF(s_pre_skill_level, APP_FONT_NAME, 16);
		l_preSkillValue->setColor(Color3B(47,93,13));
		l_preSkillValue->setAnchorPoint(Vec2(0,0));
		l_preSkillValue->setPosition(Vec2(357,103-lineNum*19));
		l_preSkillValue->setName("l_preSkillValue");
		panel_levelInfo->addChild(l_preSkillValue);

		std::map<std::string,GameFightSkill*>::const_iterator cIter;
		cIter = GameView::getInstance()->GameFightSkillList.find(m_pre_skill_id);
		if (cIter == GameView::getInstance()->GameFightSkillList.end()) // �û�ҵ�����ָ�END��  
		{
		}
		else
		{
			auto gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[m_pre_skill_id];
			if (gameFightSkillStuded)
			{
				if (gameFightSkillStuded->getLevel() < m_pre_skill_level)
				{
					if (m_str_pre_skill.size()/3 < 4)
					{
						for(int i = 0;i<m_str_pre_skill.size()/3;i++)
						{
							std::string str_name = "l_preSkill";
							char s_index[5];
							sprintf(s_index,"%d",i);
							str_name.append(s_index);

							Label * l_preSkill = (Label*)widget_pre_skill->getChildByName(str_name.c_str());
							if (l_preSkill)
							{
								l_preSkill->setColor(Color3B(212,59,59));
							}
						}
					}
					else
					{
						auto l_preSkill = (Label*)widget_pre_skill->getChildByName("l_preSkill");
						if (l_preSkill)
						{
							l_preSkill->setColor(Color3B(212,59,59));
						}
					}
					l_preSkill_colon->setColor(Color3B(212,59,59));
					l_preSkillValue->setColor(Color3B(212,59,59));
				}
			}
		}
	}
	//˽�
	if (m_required_gold>0)
	{
		lineNum++;

		auto widget_gold = Widget::create();
		panel_levelInfo->addChild(widget_gold);
		widget_gold->setPosition(Vec2(283,103-lineNum*19));

		std::string str_gold = StringDataManager::getString("skillinfo_jinbi");
		for(int i = 0;i<2;i++)
		{
			std::string str_name = "l_gold";
			char s_index[5];
			sprintf(s_index,"%d",i);
			str_name.append(s_index);
			auto l_gold = Label::createWithTTF(str_gold.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
			l_gold->setColor(Color3B(47,93,13));
			l_gold->setAnchorPoint(Vec2(0,0));
			l_gold->setName(str_name.c_str());
			widget_gold->addChild(l_gold);
			l_gold->setPosition(Vec2(48*i,0));
		}

		auto l_gold_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_gold_colon->setColor(Color3B(47,93,13));
		l_gold_colon->setAnchorPoint(Vec2(0,0));
		l_gold_colon->setPosition(Vec2(347,103-lineNum*19));
		l_gold_colon->setName("l_gold_colon");
		panel_levelInfo->addChild(l_gold_colon);

		char s_required_gold[20];
		sprintf(s_required_gold,"%d",m_required_gold);

		auto l_goldValue = Label::createWithTTF(s_required_gold, APP_FONT_NAME, 16);
		l_goldValue->setColor(Color3B(47,93,13));
		l_goldValue->setAnchorPoint(Vec2(0,0));
		l_goldValue->setPosition(Vec2(357,103-lineNum*19));
		panel_levelInfo->addChild(l_goldValue);

		if(GameView::getInstance()->getPlayerGold()<m_required_gold)//ҽ�Ҳ��
		{
			for(int i = 0;i<2;i++)
			{
				std::string str_name = "l_gold";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				auto l_gold = (Label*)widget_gold->getChildByName(str_name.c_str());
				if (l_gold)
				{
					l_gold->setColor(Color3B(212,59,59));
				}
			}
			l_gold_colon->setColor(Color3B(212,59,59));
			l_goldValue->setColor(Color3B(212,59,59));
		}
	}
}

void RobotSetSkill::showSkillDes( std::string curSkillDes,std::string nextSkillDes )
{
	//skill des text
	if (m_pLayer->getChildByTag(123456))
	{
		auto scroll_ = (cocos2d::extension::ScrollView *)m_pLayer->getChildByTag(123456);
		scroll_->removeFromParentAndCleanup(true);
	}

	auto scrollView_info = ui::ScrollView::create();
	scrollView_info->setTouchEnabled(true);
	scrollView_info->setDirection(ui::ScrollView::Direction::VERTICAL);
	scrollView_info->setContentSize(Size(264,303));
	scrollView_info->setPosition(Vec2(425,75));
	scrollView_info->setBounceEnabled(true);
	scrollView_info->setTag(123456);
	m_pLayer->addChild(scrollView_info);

	auto color_ = Color3B(47,93,13);

	auto mo_1 = ImageView::create();
	mo_1->loadTexture("res_ui/mo_2.png");
	mo_1->setAnchorPoint(Vec2(0,0));
	mo_1->setScale(0.7f);
	scrollView_info->addChild(mo_1);

	auto labelBM_1 = Label::createWithBMFont("res_ui/font/ziti_3.fnt", StringDataManager::getString("robotui_skill_des_setSkill"));
	labelBM_1->setAnchorPoint(Vec2(0,0));
	scrollView_info->addChild(labelBM_1);

	auto label_des=Text::create(curSkillDes.c_str(), APP_FONT_NAME, 16);
	label_des->setAnchorPoint(Vec2(0,0));
	label_des->setColor(color_);
	label_des->setTextAreaSize(Size(240,0));
	scrollView_info->addChild(label_des);

	auto mo_2 = ImageView::create();
	mo_2->loadTexture("res_ui/mo_2.png");
	mo_2->setAnchorPoint(Vec2(0,0));
	mo_2->setScale(0.7f);
	scrollView_info->addChild(mo_2);

	auto labelBM_2 = Label::createWithBMFont("res_ui/font/ziti_3.fnt", StringDataManager::getString("robotui_nextSkill_des_setSkill"));
	labelBM_2->setAnchorPoint(Vec2(0,0));
	scrollView_info->addChild(labelBM_2);

	auto label_next_des= Text::create(nextSkillDes.c_str(), APP_FONT_NAME, 16);
	label_next_des->setAnchorPoint(Vec2(0,0));
	label_next_des->setColor(color_);
	label_next_des->setTextAreaSize(Size(240,0));
	scrollView_info->addChild(label_next_des);

	int h_ = mo_1->getContentSize().height * 2;
	int temp_min_h = 100;

	if (label_des->getContentSize().height > temp_min_h)
	{
		h_+= label_des->getContentSize().height;
	}else
	{
		h_+= temp_min_h;
	}

	if (label_next_des->getContentSize().height > temp_min_h)
	{
		h_+= label_next_des->getContentSize().height;
	}else
	{
		h_+= temp_min_h;
	}


	int innerWidth = scrollView_info->getBoundingBox().size.width;
	int innerHeight = h_;
	if (h_ < scrollView_info->getBoundingBox().size.height)
	{
		innerHeight = scrollView_info->getBoundingBox().size.height;
	}

	scrollView_info->setInnerContainerSize(Size(innerWidth,innerHeight));


	mo_1->setPosition(Vec2(0,innerHeight - mo_1->getContentSize().height ));
	labelBM_1->setPosition(Vec2(10,mo_1->getPosition().y+2));
	label_des->setPosition(Vec2(0,mo_1->getPosition().y - label_des->getContentSize().height));

	mo_2->setPosition(Vec2(0,label_des->getPosition().y - 30));
	labelBM_2->setPosition(Vec2(10,mo_2->getPosition().y+2));
	label_next_des->setPosition(Vec2(0,mo_2->getPosition().y - label_next_des->getContentSize().height));
}
