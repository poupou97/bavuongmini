#include "RobotMainUI.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../utils/StaticDataManager.h"
#include "../extensions/UITab.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "RobotSetSkill.h"
#include "RobotSetHpAndMp.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../messageclient/element/MapRobotDrug.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/element/CVipInfo.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../legend_script/CCTeachingGuide.h"

using namespace CocosDenshion;


#define UISCENE_MAIN_SCENE_TAG 1000

RobotMainUI::RobotMainUI(void)
{
}

RobotMainUI::~RobotMainUI(void)
{
// 	labelRolebuyHpId_->removeFromParentAndCleanup(true);
// 	labelRolebuyMpId_->removeFromParentAndCleanup(true);
// 	labelGeneralbuyHpId_->removeFromParentAndCleanup(true);
// 	labelGeneralbuyMpId_->removeFromParentAndCleanup(true);

	imageRoleHp->removeFromParentAndCleanup(true);
	imageRoleMp->removeFromParentAndCleanup(true);
	imageGeneralHp->removeFromParentAndCleanup(true);
	imageGeneralMp->removeFromParentAndCleanup(true);
}

RobotMainUI * RobotMainUI::create()
{
	auto _ui=new RobotMainUI();
	if (_ui && _ui->init())
	{
		_ui->autorelease();
		return _ui;
	}
	CC_SAFE_DELETE(_ui);
	return NULL;
}

bool RobotMainUI::init()
{
	if (UIScene::init())
	{
		mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());

		//// first, get AI config, these config has been loaded at Push1001, so ignore
		//std::string playerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
		//MyPlayerAIConfig::loadConfig(playerName);

		winsize =Director::getInstance()->getVisibleSize();
		
		auto layer=Layer::create();
		layer->setIgnoreAnchorPointForPosition(false);
		layer->setAnchorPoint(Vec2(0.5f,0.5f));
		layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layer->setContentSize(Size(800,480));
		addChild(layer);

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		mainPanel = LoadSceneLayer::onHookKillPanel;
		mainPanel->setTag(UISCENE_MAIN_SCENE_TAG);
		mainPanel->setAnchorPoint(Vec2(0.5f,0.5f));
		mainPanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(mainPanel);

		const char * secondStr = StringDataManager::getString("robotui_diaoshi_gua");
		const char * thirdStr = StringDataManager::getString("robotui_diaoshi_ji");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(55,230));
		layer->addChild(atmature);

		auto m_Layer= Layer::create();
		m_Layer->setIgnoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(Vec2(0.5f,0.5f));
		m_Layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_Layer->setContentSize(Size(800,480));
		this->addChild(m_Layer);

		const char *str_fight = StringDataManager::getString("robotui_tab_fight");
		char *fight_left=const_cast<char*>(str_fight);	

		const char *str_protect = StringDataManager::getString("robotui_tab_protect");
		char *protect_left=const_cast<char*>(str_protect);	

		const char *str_goods = StringDataManager::getString("robotui_tab_goods");
		char *goods_left=const_cast<char*>(str_goods);	

		char *labelFont[]={fight_left,protect_left,goods_left};
		//robotTab=UITab::createWithBMFont(3,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",labelFont,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
		robotTab=UITab::createWithText(3,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",labelFont,HORIZONTAL,5);
		robotTab->setAnchorPoint(Vec2(0.5f,0.5f));
		robotTab->setPosition(Vec2(100,405));
		robotTab->setHighLightImage("res_ui/tab_1_on.png");
		robotTab->setDefaultPanelByIndex(0);
		robotTab->setPressedActionEnabled(true);
		robotTab->addIndexChangedEvent(this,coco_indexchangedselector(RobotMainUI::changeTabEvent));
		m_Layer->addChild(robotTab);

		battlePanel =(Layout *)Helper::seekWidgetByName(mainPanel,"Panel_Hangupthebattle4");
		battlePanel->setVisible(true);
		protectionPanel =(Layout *)Helper::seekWidgetByName(mainPanel,"Panel_Hangupprotection2");
		protectionPanel->setVisible(false);
		goodsPanel =(Layout *)Helper::seekWidgetByName(mainPanel,"Panel_Hangupthegoods");
		goodsPanel->setVisible(false);

		/////protect panel(set use hp/mp)
		this->initFightPanel();
		this->initProtectPanel();
		this->initGoodsPanel();

		btn_Exit= (Button *)Helper::seekWidgetByName(mainPanel,"Button_close");
		CCAssert(btn_Exit != NULL, "should not be nil");
		btn_Exit->setTouchEnabled(true);
		btn_Exit->setPressedActionEnabled(true);
		btn_Exit->addTouchEventListener(CC_CALLBACK_2(RobotMainUI::callBackExit, this));

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void RobotMainUI::initSkillButton(Layout* mainPanel, int index)
{
	// skill name
	std::string skillButtonName = "SkillBtn_";
	char id[3];
	sprintf(id, "%d", index);
	skillButtonName.append(id);

	buttonSkillIcon = (Button *)Helper::seekWidgetByName(mainPanel, skillButtonName.c_str());
	CCAssert(buttonSkillIcon != NULL, "should not be nil");
	buttonSkillIcon->setTouchEnabled(true);
	buttonSkillIcon->setPressedActionEnabled(true);
	buttonSkillIcon->setTag(index);
	buttonSkillIcon->addTouchEventListener(CC_CALLBACK_2(RobotMainUI::callBackSkillList, this));

	//set skill light
	std::string imageLight_str = "ImageView_light_";
	char imageLightId[25];
	sprintf(imageLightId, "%d", index);
	imageLight_str.append(imageLightId);
	auto imageLight_ = (ImageView *)buttonSkillIcon->getChildByName(imageLight_str.c_str());
	///set skill icon 
	std::string skillImageName = "ImageView_skillbtn_";
	char imageId[25];
	sprintf(imageId, "%d", index);
	skillImageName.append(imageId);
	auto imageIcon =(ImageView *)buttonSkillIcon->getChildByName(skillImageName.c_str());
	
	std::string playerSkill_ = "";
	if (index < GameView::getInstance()->myplayer->getMyPlayerAI()->m_skillList.size())
	{
		playerSkill_ = GameView::getInstance()->myplayer->getMyPlayerAI()->m_skillList.at(index);
	}

	std::string imageSkillType_str = "ImageView_skilType_";
	char imageSkillTypeId[25];
	sprintf(imageSkillTypeId, "%d", index);
	imageSkillType_str.append(imageLightId);

	auto image_skillType = (ImageView *)imageLight_->getChildByName(imageSkillType_str.c_str());
	
	if (strcmp(playerSkill_.c_str(),"")==0)
	{
		//imageIcon->loadTexture("res_ui/jineng/gaoguang_1.png");
		imageIcon->setVisible(false);

		image_skillType->setVisible(false);
		return;
	}

	auto baseSkill = StaticDataBaseSkill::s_baseSkillData[playerSkill_];
	if (baseSkill)
	{
		std::string playerSkillIcon ="res_ui/jineng_icon/";
		playerSkillIcon.append(baseSkill->icon());
		playerSkillIcon.append(".png");
		imageIcon->loadTexture(playerSkillIcon.c_str());
		imageIcon->setVisible(true);
		//add skill type
		std::string str_skillType_string = getSkillTypeStr(baseSkill->id().c_str());
		image_skillType->loadTexture(str_skillType_string.c_str());
		image_skillType->setVisible(true);
	}
	else
	{
		imageIcon->loadTexture("res_ui/jineng_icon/jineng_2.png");
		imageIcon->setVisible(true);
	}
}

void RobotMainUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void RobotMainUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);

	MyPlayerAIConfig::setMoveRange(curSliderMove);
	MyPlayerAIConfig::setRoleLimitHp(curSliderHp);
	MyPlayerAIConfig::setRoleLimitMp(curSliderMp);
	MyPlayerAIConfig::setGeneralLimitHp(curSliderGeneralHp);
	MyPlayerAIConfig::setGeneralLimitMp(curSliderGeneralMp);
}

void RobotMainUI::callBackExit(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RobotMainUI::callBackSkillList(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto skillButton = dynamic_cast<Button*>(pSender);
		buttonSkillIconIndex = skillButton->getTag();
		auto robotkill = RobotSetSkill::create();
		robotkill->setIgnoreAnchorPointForPosition(false);
		robotkill->setAnchorPoint(Vec2(0.5f, 0.5f));
		robotkill->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
		GameView::getInstance()->getMainUIScene()->addChild(robotkill);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RobotMainUI::callBackStartRobot(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		MyPlayerAIConfig::enableRobot(true);
		GameView::getInstance()->myplayer->getMyPlayerAI()->start();

		// hide current button
		auto currentButton = dynamic_cast<Button*>(pSender);
		CCAssert(currentButton != NULL, "should not be nil");
		currentButton->setVisible(false);

		// show another button
		Layout*  mainPanel = dynamic_cast<Layout*>(m_pLayer->getChildByTag(UISCENE_MAIN_SCENE_TAG));
		Button * anotherButton = (Button *)Helper::seekWidgetByName(mainPanel, "Button_stop");
		CCAssert(anotherButton != NULL, "should not be nil");
		anotherButton->setVisible(true);

		/*���ùһ�ʱ�����»�ȡ����Ʒ�Ĵ��*/
		this->setGoodPickAndSellOption(STARTROBOTSTATE);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}
void RobotMainUI::callBackStopRobot(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		MyPlayerAIConfig::enableRobot(false);
		GameView::getInstance()->myplayer->getMyPlayerAI()->stop();

		// hide current button
		auto currentButton = dynamic_cast<Button*>(pSender);
		CCAssert(currentButton != NULL, "should not be nil");
		currentButton->setVisible(false);

		// show another button
		Layout*  mainPanel = dynamic_cast<Layout*>(m_pLayer->getChildByTag(UISCENE_MAIN_SCENE_TAG));
		Button * anotherButton = (Button *)Helper::seekWidgetByName(mainPanel, "Button_start");
		CCAssert(anotherButton != NULL, "should not be nil");
		anotherButton->setVisible(true);
		/*����һ�ʱ�����»�ȡ����Ʒ�Ĵ��*/
		this->setGoodPickAndSellOption(ENDROBOTSTATE);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void RobotMainUI::changeTabEvent( Ref * obj )
{
	UITab * tab =(UITab *)obj;
	int num =tab->getCurrentIndex();
	switch(num)
	{
	case 0:
		{
			battlePanel->setVisible(true);
			protectionPanel->setVisible(false);
			goodsPanel->setVisible(false);
		}break;
	case 1:
		{
			Button * button_ =(Button *)tab->getObjectByIndex(1);
			Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
				sc->endCommand(button_);

			battlePanel->setVisible(false);
			protectionPanel->setVisible(true);
			goodsPanel->setVisible(false);
		}break;
	case 2:
		{
			Button * button_ =(Button *)tab->getObjectByIndex(2);
			Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
				sc->endCommand(button_);

			battlePanel->setVisible(false);
			protectionPanel->setVisible(false);
			goodsPanel->setVisible(true);
		}break;
	}
}

void RobotMainUI::setAutomaticskill(Ref* pSender, CheckBox::EventType type)
{
	auto checkbox =(CheckBox *)pSender;
	GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (checkbox->isSelected() ==true)
	{
		MyPlayerAIConfig::setAutomaticSkill(1);

		//set mainscene robot button 
		guide_->isOpenSkillMonster=true;
		guide_->setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png");
	}else
	{
		MyPlayerAIConfig::setAutomaticSkill(0);
		guide_->isOpenSkillMonster=false;
		guide_->setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
	}
}

void RobotMainUI::moveRangeConfig(Ref *pSender, ui::Slider::EventType type)
{
	auto slider = dynamic_cast<Slider*>(pSender);
	int percent = slider->getPercent();
	moveRangeLabel->setString(__String::createWithFormat("%d", percent)->getCString());

	//MyPlayerAIConfig::setMoveRange(percent);

	curSliderMove = percent;
}

void RobotMainUI::setGoodsPanelAllChexkBox(Ref* pSender, CheckBox::EventType type)
{
	auto checkBox =dynamic_cast<CheckBox *>(pSender);
	int tag_ =checkBox->getTag();
	int select_ =checkBox->isSelected();
	/*
	int myVip_Robot_AutoSell_limit = 0;
	VipConfigData::TypeIdAndVipLevel typeAndLevel_robot_autoSell;
	typeAndLevel_robot_autoSell.typeName = StringDataManager::getString("vip_function_instance_RobotSellEquipment");
	typeAndLevel_robot_autoSell.vipLevel = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel();

	std::map<VipConfigData::TypeIdAndVipLevel,CVipInfo*>::const_iterator cIter_Robot_autoSell;
	cIter_Robot_autoSell = VipConfigData::s_vipConfig.find(typeAndLevel_robot_autoSell);
	if (cIter_Robot_autoSell == VipConfigData::s_vipConfig.end()) // �û�ҵ�����ָ�END��  
	{
	}
	else
	{
		CVipInfo * tempVipInfo = cIter_Robot_autoSell->second;
		myVip_Robot_AutoSell_limit = tempVipInfo->get_add_number();
	}
	*/
	
	int myVip_Robot_AutoSell_limit = 0;
	VipConfigData::TypeIdAndVipLevel typeAndLevel_autoSell;
	typeAndLevel_autoSell.typeName = StringDataManager::getString("vip_function_instance_RobotSellEquipment");

	for(int i = 0;i<VipDescriptionConfig::s_vipDes.size();++i)
	{
		typeAndLevel_autoSell.vipLevel = i;

		std::map<VipConfigData::TypeIdAndVipLevel,CVipInfo*>::const_iterator cIter;
		cIter = VipConfigData::s_vipConfig.find(typeAndLevel_autoSell);
		if (cIter == VipConfigData::s_vipConfig.end()) // �û�ҵ�����ָ�END��  
		{
			continue;
		}
		else
		{
			myVip_Robot_AutoSell_limit = typeAndLevel_autoSell.vipLevel;
			break;
		}
	}

	//////////////////////

	switch(tag_)
	{
	case 1:
		{
			if (select_ == 1)
			{
				for (int i=1;i<5;i++)
				{
					MyPlayerAIConfig::setGoodsPickAndSell(i,1);
				}
			}else
			{
				for (int i=1;i<5;i++)
				{
					MyPlayerAIConfig::setGoodsPickAndSell(i,0);
				}
			}
		}break;
	case 5:
		{
			if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() >= myVip_Robot_AutoSell_limit)
			{
				if (select_ ==1)
				{
					for (int i=5;i<9;i++)
					{
						MyPlayerAIConfig::setGoodsPickAndSell(i,1);
					}
				}else
				{
					for (int i=5;i<9;i++)
					{
						MyPlayerAIConfig::setGoodsPickAndSell(i,0);
					}
				}
			}else
			{
				//vip level is not------------------
				std::string funName_ = StringDataManager::getString("vip_function_instance_RobotSellEquipment");
				this->showVipFunctionOpen(funName_);
			}
		}break;
	default:
		{
			if (tag_<5)
			{
				MyPlayerAIConfig::setGoodsPickAndSell(1,0);
				MyPlayerAIConfig::setGoodsPickAndSell(tag_,select_);
			}else
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() >=  myVip_Robot_AutoSell_limit)
				{
					MyPlayerAIConfig::setGoodsPickAndSell(5,0);
					MyPlayerAIConfig::setGoodsPickAndSell(tag_,select_);
				}else
				{
					//vip level----------
					std::string funName_ = StringDataManager::getString("vip_function_instance_RobotSellEquipment");
					this->showVipFunctionOpen(funName_);
				}
			}
		}break;
	}
	this->initGoodsPanel();
}

void RobotMainUI::popupBagList(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		buttonProtectBg = (Button *)pSender;
		int tag_ = buttonProtectBg->getTag();
		Size winsize = Director::getInstance()->getVisibleSize();
		//1 roleHP  3 roleMp  2generalHp  4generalMp
		int drugType;
		RobotSetHpAndMp * robotHpAndMp;
		switch (tag_)
		{
		case 1:
		{
			drugType = 1;
			robotHpAndMp = RobotSetHpAndMp::create(drugType);
			robotHpAndMp->setIgnoreAnchorPointForPosition(false);
			robotHpAndMp->setAnchorPoint(Vec2(0.5f, 0.5f));
			robotHpAndMp->setPosition(Vec2(winsize.width / 2, winsize.height / 2 + robotHpAndMp->getContentSize().height / 2 - 40));
		}break;
		case 2:
		{
			drugType = 3;
			robotHpAndMp = RobotSetHpAndMp::create(drugType);
			robotHpAndMp->setIgnoreAnchorPointForPosition(false);
			robotHpAndMp->setAnchorPoint(Vec2(0.5f, 0.5f));
			robotHpAndMp->setPosition(Vec2(winsize.width / 2, winsize.height / 2 - robotHpAndMp->getContentSize().height - 10));
		}break;
		case 3:
		{
			drugType = 2;
			robotHpAndMp = RobotSetHpAndMp::create(drugType);
			robotHpAndMp->setIgnoreAnchorPointForPosition(false);
			robotHpAndMp->setAnchorPoint(Vec2(0.5f, 0.5f));
			robotHpAndMp->setPosition(Vec2(winsize.width / 2, winsize.height / 2 + robotHpAndMp->getContentSize().height / 2 - 40));
		}break;
		case 4:
		{
			drugType = 4;
			robotHpAndMp = RobotSetHpAndMp::create(drugType);
			robotHpAndMp->setIgnoreAnchorPointForPosition(false);
			robotHpAndMp->setAnchorPoint(Vec2(0.5f, 0.5f));
			robotHpAndMp->setPosition(Vec2(winsize.width / 2, winsize.height / 2 - robotHpAndMp->getContentSize().height - 10));
		}break;

		}

		// 	RobotSetHpAndMp * robotHpAndMp = RobotSetHpAndMp::create(drugType);
		// 	robotHpAndMp->setIgnoreAnchorPointForPosition(false);
		// 	robotHpAndMp->setAnchorPoint(Vec2(0.5f,0.5f));
		// 	robotHpAndMp->setPosition(Vec2(winsize.width/2,winsize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(robotHpAndMp);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void RobotMainUI::addUseGoods(std::string goodsIcon ,std::string goodsid ,std::string goodsName)
{
	std::string btnName_ =buttonProtectBg->getName();
	//pac index 
	std::string goodsIcon_ = "res_ui/props_icon/";
	goodsIcon_.append(goodsIcon);
	goodsIcon_.append(".png");

	std::string playerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
	std::string btnNameStr =playerName.append(btnName_);

	if (strcmp(btnName_.c_str(),"Button_retore_1")==0)
	{
		imageRoleHp->loadTexture(goodsIcon_.c_str());
		labelRolebuyHpId_->setString(goodsName.c_str());
		label_roleHp->setString(goodsName.c_str());

		MyPlayerAIConfig::setRoleUseDrugHpName(goodsid.c_str());
	}
	if (strcmp(btnName_.c_str(),"Button_retore_3")==0)
	{
		imageRoleMp->loadTexture(goodsIcon_.c_str());
		labelRolebuyMpId_->setString(goodsName.c_str());
		label_roleHp->setString(goodsName.c_str());

		MyPlayerAIConfig::setRoleUseDrugMpName(goodsid.c_str());
	}
	if (strcmp(btnName_.c_str(),"Button_retore_2")==0)
	{
		imageGeneralHp->loadTexture(goodsIcon_.c_str());
		labelGeneralbuyHpId_->setString(goodsName.c_str());
		label_GeneralHp->setString(goodsName.c_str());

		MyPlayerAIConfig::setGeneralUseDrugHpName(goodsid.c_str());
	}
	if (strcmp(btnName_.c_str(),"Button_retore_4")==0)
	{
		imageGeneralMp->loadTexture(goodsIcon_.c_str());
		labelGeneralbuyMpId_->setString(goodsName.c_str());
		label_GeneralMp->setString(goodsName.c_str());
		MyPlayerAIConfig::setGeneralUseDrugMpName(goodsid.c_str());
	}
}


void RobotMainUI::showAutomaticGoods( std::string goodsName )
{
	//Button * btn =(Button *)Helper::seekWidgetByName(protectionPanel,goodsName.c_str());
	std::map<int,MapRobotDrug *> * robotdrugMap;
	std::string useDrugId;
	if (strcmp(goodsName.c_str(),"Button_retore_1")==0)
	{
		useDrugId =MyPlayerAIConfig::getRoleUseDrugHpName();
		robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[1];
	}
	if (strcmp(goodsName.c_str(),"Button_retore_3")==0)
	{
		useDrugId =MyPlayerAIConfig::getRoleUseDrugMpName();

		robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[2];
	}

	if (strcmp(goodsName.c_str(),"Button_retore_2")==0)
	{
		useDrugId =MyPlayerAIConfig::getGeneralUseDrugHpName();
		
		robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[3];
	}

	if (strcmp(goodsName.c_str(),"Button_retore_4")==0)
	{
		useDrugId =MyPlayerAIConfig::getGeneralUseDrugMpName();
		robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[4];
	}

	std::string goodsIcon_;
	std::string goodsName_;
	std::string goodsid_;
	int mapsize =robotdrugMap->size();
	for (int i=0;i<mapsize;i++)
	{
		if (i==0)
		{
			goodsIcon_ = robotdrugMap->at(1)->get_icon();
			goodsName_ = robotdrugMap->at(1)->get_name();
			goodsid_ = robotdrugMap->at(1)->get_id();
		}else
		{
			goodsIcon_ =robotdrugMap->at(i*15)->get_icon();
			goodsName_ = robotdrugMap->at(i*15)->get_name();
			goodsid_ = robotdrugMap->at(i*15)->get_id();
		}

		std::string goodsIconStr_ ="res_ui/props_icon/";
		goodsIconStr_.append(goodsIcon_);
		goodsIconStr_.append(".png");

		if (strcmp(useDrugId.c_str(),goodsid_.c_str())==0)
		{
			if (strcmp(goodsName.c_str(),"Button_retore_1")==0)
			{
				imageRoleHp->loadTexture(goodsIconStr_.c_str());
				labelRolebuyHpId_->setString(goodsName_.c_str());
				label_roleHp->setString(goodsName_.c_str());

			}
			if (strcmp(goodsName.c_str(),"Button_retore_3")==0)
			{
				imageRoleMp->loadTexture(goodsIconStr_.c_str());
				labelRolebuyMpId_->setString(goodsName_.c_str());
				label_roleMp->setString(goodsName_.c_str());
			}

			if (strcmp(goodsName.c_str(),"Button_retore_2")==0)
			{
				imageGeneralHp->loadTexture(goodsIconStr_.c_str());
				labelGeneralbuyHpId_->setString(goodsName_.c_str());
				label_GeneralHp->setString(goodsName_.c_str());
			}

			if (strcmp(goodsName.c_str(),"Button_retore_4")==0)
			{
				imageGeneralMp->loadTexture(goodsIconStr_.c_str());
				labelGeneralbuyMpId_->setString(goodsName_.c_str());
				label_GeneralMp->setString(goodsName_.c_str());
			}	
		}
	}
}

void RobotMainUI::initFightPanel()
{
	int m_automaticSkill = MyPlayerAIConfig::getAutomaticSkill();
	auto checkBoxKill =(CheckBox *)Helper::seekWidgetByName(battlePanel,"CheckBox_honggou_9");
	checkBoxKill->setTouchEnabled(true);
	if (m_automaticSkill ==1)
	{
		checkBoxKill->setSelected(true);
	}else
	{
		checkBoxKill->setSelected(false);
	}
	checkBoxKill->addEventListener(CC_CALLBACK_2(RobotMainUI::setAutomaticskill,this));

	//int m_moveRange =MyPlayerAIConfig::getMoveRange();
	curSliderMove = MyPlayerAIConfig::getMoveRange();
	moveRangeLabel =(Text*)Helper::seekWidgetByName(battlePanel,"Label_moverange2");
	moveRangeLabel->setString(__String::createWithFormat("%d",curSliderMove)->getCString());

	auto slider_Sound = (Slider *)Helper::seekWidgetByName(battlePanel,"slider_moveRange");
	slider_Sound->setTouchEnabled(true);
	slider_Sound->setPercent(curSliderMove);
	slider_Sound->addEventListener(CC_CALLBACK_2(RobotMainUI::moveRangeConfig,this));

	//temp not use //autoTeamBtn
// 	Button * autoTeamBtn = (Button *)Helper::seekWidgetByName(battlePanel,"Button_autoteam");
// 	autoTeamBtn->setTouchEnabled(true);
// 	autoTeamBtn->setPressedActionEnabled(true);
// 	autoTeamBtn->addTouchEventListener(CC_CALLBACK_2());
// 	autoTeamBtn->setVisible(false);

	// init skill buttons
	for(int i = 0; i < ROBOT_SETUP_SKILL_MAX; i++) {
		initSkillButton(battlePanel, i);
	}

	auto button= (Button *)Helper::seekWidgetByName(mainPanel,"Button_start");
	CCAssert(button != NULL, "should not be nil");
	button->setTouchEnabled(true);
	button->setPressedActionEnabled(true);
	button->addTouchEventListener(CC_CALLBACK_2(RobotMainUI::callBackStartRobot, this));
	// setup by config
	if(MyPlayerAIConfig::isEnableRobot())
		button->setVisible(false);
	else
		button->setVisible(true);

	button= (Button *)Helper::seekWidgetByName(mainPanel,"Button_stop");
	CCAssert(button != NULL, "should not be nil");
	button->setTouchEnabled(true);
	button->setPressedActionEnabled(true);
	button->addTouchEventListener(CC_CALLBACK_2(RobotMainUI::callBackStopRobot, this));
	// setup by config
	if(MyPlayerAIConfig::isEnableRobot())
		button->setVisible(true);
	else
		button->setVisible(false);
}

void RobotMainUI::initProtectPanel()
{
	//role
	checkBoxSetUseHp =(CheckBox *)Helper::seekWidgetByName(protectionPanel,"CheckBox_honggou_1");
	checkBoxSetUseHp->setTouchEnabled(true);
	checkBoxSetUseHp->addEventListener(CC_CALLBACK_2(RobotMainUI::setCheckBoxuseHp,this));
	int m_SelectuseHp = MyPlayerAIConfig::getRoleUseHp();
	if (m_SelectuseHp==1)
	{
		checkBoxSetUseHp->setSelected(true);
	}else
	{
		checkBoxSetUseHp->setSelected(false);
	}

	//int m_setsliderRoleHp =MyPlayerAIConfig::getRoleLimitHp();
	curSliderHp =MyPlayerAIConfig::getRoleLimitHp();
	const char *str_RoleHp = StringDataManager::getString("robotui_role_hp");
	const char *str_automatic = StringDataManager::getString("robotui_role_automatic");
	std::string siliderHpNum_ =__String::createWithFormat("%d",curSliderHp)->getCString();
	std::string sliderHplabel_="";
	sliderHplabel_.append(str_RoleHp);
	sliderHplabel_.append("<");
	sliderHplabel_.append(siliderHpNum_);
	sliderHplabel_.append("%");
	sliderHplabel_.append(str_automatic);

	rolesliderHplabel =(Text*)Helper::seekWidgetByName(protectionPanel,"Label_writing1");
	rolesliderHplabel->setString(sliderHplabel_.c_str());

	Button * roleSetUseHp =(Button *)Helper::seekWidgetByName(protectionPanel,"Button_retore_1");
	roleSetUseHp->setTouchEnabled(true);
	roleSetUseHp->setTag(1);
	roleSetUseHp->addTouchEventListener(CC_CALLBACK_2(RobotMainUI::popupBagList,this));

	imageRoleHp =ImageView::create();
	imageRoleHp->loadTexture("");
	imageRoleHp->setAnchorPoint(Vec2(0.5f,0.5f));
	imageRoleHp->setPosition(Vec2(0,0));
	roleSetUseHp->addChild(imageRoleHp);

	label_roleHp = (Text*)Helper::seekWidgetByName(protectionPanel,"label_retore_1");
	label_roleHp->setLocalZOrder(10);

	label_roleMp = (Text*)Helper::seekWidgetByName(protectionPanel,"label_retore_3");
	label_roleMp->setLocalZOrder(10);

	label_GeneralHp = (Text*)Helper::seekWidgetByName(protectionPanel,"label_retore_2");
	label_GeneralHp->setLocalZOrder(10);

	label_GeneralMp = (Text*)Helper::seekWidgetByName(protectionPanel,"label_retore_4");
	label_GeneralMp->setLocalZOrder(10);

	auto slider_setUseHP = (Slider *)Helper::seekWidgetByName(protectionPanel,"silderHp_protect");
	slider_setUseHP->setTouchEnabled(true);
	slider_setUseHP->setPercent(curSliderHp);
	slider_setUseHP->addEventListener(CC_CALLBACK_2(RobotMainUI::setsliderHp,this));
	//silider mp
	checkBoxSetUseMp =(CheckBox *)Helper::seekWidgetByName(protectionPanel,"CheckBox_honggou_5");
	checkBoxSetUseMp->setTouchEnabled(true);
	checkBoxSetUseMp->addEventListener(CC_CALLBACK_2(RobotMainUI::setCheckBoxuseMp,this));
	int m_SelectuseMp = MyPlayerAIConfig::getRoleUseMp();
	if (m_SelectuseMp==1)
	{
		checkBoxSetUseMp->setSelected(true);
	}else
	{
		checkBoxSetUseMp->setSelected(false);
	}

	//int m_setsliderRoleMp =MyPlayerAIConfig::getRoleLimitMp();
	curSliderMp =MyPlayerAIConfig::getRoleLimitMp();
	const char *str_RoleMp = StringDataManager::getString("robotui_role_mp");
	std::string siliderMpNum_ =__String::createWithFormat("%d",curSliderMp)->getCString();
	std::string sliderMplabel_="";
	sliderMplabel_.append(str_RoleMp);
	sliderMplabel_.append("<");
	sliderMplabel_.append(siliderMpNum_);
	sliderMplabel_.append("%");
	sliderMplabel_.append(str_automatic);

	rolesliderMplabel =(Text*)Helper::seekWidgetByName(protectionPanel,"Label_writing2");
	rolesliderMplabel->setString(sliderMplabel_.c_str());

	Button * roleSetUseMp =(Button *)Helper::seekWidgetByName(protectionPanel,"Button_retore_3");
	roleSetUseMp->setTouchEnabled(true);
	roleSetUseMp->setTag(3);
	roleSetUseMp->addTouchEventListener(CC_CALLBACK_2(RobotMainUI::popupBagList, this));

	imageRoleMp =ImageView::create();
	imageRoleMp->loadTexture("");
	imageRoleMp->setAnchorPoint(Vec2(0.5f,0.5f));
	imageRoleMp->setPosition(Vec2(0,0));
	roleSetUseMp->addChild(imageRoleMp);

	auto slider_setUseMp = (Slider *)Helper::seekWidgetByName(protectionPanel,"silderMp_protect");
	slider_setUseMp->setTouchEnabled(true);
	slider_setUseMp->setPercent(curSliderMp);
	slider_setUseMp->addEventListener(CC_CALLBACK_2(RobotMainUI::setsliderMp,this));
	//buy  Label_writing4/Label_writing5
	checkBoxbuyHp =(CheckBox *)Helper::seekWidgetByName(protectionPanel,"CheckBox_honggou_2");
	checkBoxbuyHp->setTouchEnabled(true);
	checkBoxbuyHp->addEventListener(CC_CALLBACK_2(RobotMainUI::setCheckBoxBuyHp,this));
	int m_SelectbuyHp=MyPlayerAIConfig::getRoleBuyHp();
	if (m_SelectbuyHp==1)
	{
		checkBoxbuyHp->setSelected(true);
	}else
	{
		checkBoxbuyHp->setSelected(false);
	}

	labelRolebuyHpId_ =(Text*)Helper::seekWidgetByName(protectionPanel,"Label_roleRobotHpName");
	labelRolebuyHpId_->setString("");

	//buy mp
	checkBoxbuyMp =(CheckBox *)Helper::seekWidgetByName(protectionPanel,"CheckBox_honggou_6");
	checkBoxbuyMp->setTouchEnabled(true);
	checkBoxbuyMp->addEventListener(CC_CALLBACK_2(RobotMainUI::setCheckBoxBuyMp,this));
	int m_SelectbuyMp = MyPlayerAIConfig::getRoleBuyMp();

	if (m_SelectbuyMp==1)
	{
		checkBoxbuyMp->setSelected(true);
	}else
	{
		checkBoxbuyMp->setSelected(false);
	}

	labelRolebuyMpId_ =(Text*)Helper::seekWidgetByName(protectionPanel,"Label_roleRobotMpName");
	labelRolebuyMpId_->setString("");

	//general hp / mp
	checkBoxGeneralHp =(CheckBox *)Helper::seekWidgetByName(protectionPanel,"CheckBox_honggou_3");
	checkBoxGeneralHp->setTouchEnabled(true);
	int m_SelectgeneraluseHp = MyPlayerAIConfig::getGeneralUseHp();
	if (m_SelectgeneraluseHp==1)
	{
		checkBoxGeneralHp->setSelected(true);
	}else
	{
		checkBoxGeneralHp->setSelected(false);
	}
	checkBoxGeneralHp->addEventListener(CC_CALLBACK_2(RobotMainUI::setCheckBoxGeneralHp,this));

	//int m_sliderGeneralHp = MyPlayerAIConfig::getGeneralLimitHp();
	curSliderGeneralHp = MyPlayerAIConfig::getGeneralLimitHp();
	const char *str_GeneralHp = StringDataManager::getString("robotui_generalHp");
	std::string siliderGeneralHpNum_ =__String::createWithFormat("%d",curSliderGeneralHp)->getCString();
	std::string sliderGeneralHplabel_="";
	sliderGeneralHplabel_.append(str_GeneralHp);
	sliderGeneralHplabel_.append("<");
	sliderGeneralHplabel_.append(siliderGeneralHpNum_);
	sliderGeneralHplabel_.append("%");
	sliderGeneralHplabel_.append(str_automatic);

	generalSliderHplabel =(Text*)Helper::seekWidgetByName(protectionPanel,"Label_writing6");
	generalSliderHplabel->setString(sliderGeneralHplabel_.c_str());

	Button * generalUseHp =(Button *)Helper::seekWidgetByName(protectionPanel,"Button_retore_2");
	generalUseHp->setTouchEnabled(true);
	generalUseHp->setTag(2);
	generalUseHp->addTouchEventListener(CC_CALLBACK_2(RobotMainUI::popupBagList, this));

	imageGeneralHp =ImageView::create();
	imageGeneralHp->loadTexture("");
	imageGeneralHp->setAnchorPoint(Vec2(0.5f,0.5f));
	imageGeneralHp->setPosition(Vec2(0,0));
	generalUseHp->addChild(imageGeneralHp);

	auto slider_setGeneralHp = (Slider *)Helper::seekWidgetByName(protectionPanel,"setsliderGeneralHp");
	slider_setGeneralHp->setTouchEnabled(true);
	slider_setGeneralHp->setPercent(curSliderGeneralHp);
	slider_setGeneralHp->addEventListener(CC_CALLBACK_2(RobotMainUI::setsliderGeneralHp,this));
	//////////////////
	checkBoxGeneralMp =(CheckBox *)Helper::seekWidgetByName(protectionPanel,"CheckBox_honggou_7");
	checkBoxGeneralMp->setTouchEnabled(true);
	int m_SelectgeneraluseMp = MyPlayerAIConfig::getGeneralUseMp();
	if (m_SelectgeneraluseMp==1)
	{
		checkBoxGeneralMp->setSelected(true);
	}else
	{
		checkBoxGeneralMp->setSelected(false);
	}
	checkBoxGeneralMp->addEventListener(CC_CALLBACK_2(RobotMainUI::setCheckBoxGeneralMp,this));

	//int m_sliderGeneralMp =MyPlayerAIConfig::getGeneralLimitMp();
	curSliderGeneralMp = MyPlayerAIConfig::getGeneralLimitMp();
	const char *str_GeneralMp = StringDataManager::getString("robotui_generalMp");
	std::string siliderGeneralMpNum_ =__String::createWithFormat("%d",curSliderGeneralMp)->getCString();
	std::string sliderGeneralMplabel_="";
	sliderGeneralMplabel_.append(str_GeneralMp);
	sliderGeneralMplabel_.append("<");
	sliderGeneralMplabel_.append(siliderGeneralMpNum_);
	sliderGeneralMplabel_.append("%");
	sliderGeneralMplabel_.append(str_automatic);

	generalSliderMplabel =(Text*)Helper::seekWidgetByName(protectionPanel,"Label_writing7");
	generalSliderMplabel->setString(sliderGeneralMplabel_.c_str());


	auto generalUseMp =(Button *)Helper::seekWidgetByName(protectionPanel,"Button_retore_4");
	generalUseMp->setTouchEnabled(true);
	generalUseMp->setTag(4);
	generalUseMp->addTouchEventListener(CC_CALLBACK_2(RobotMainUI::popupBagList, this));

	imageGeneralMp =ImageView::create();
	imageGeneralMp->loadTexture("");
	imageGeneralMp->setAnchorPoint(Vec2(0.5f,0.5f));
	imageGeneralMp->setPosition(Vec2(0,0));
	generalUseMp->addChild(imageGeneralMp);

	auto slider_setGeneralMp = (Slider *)Helper::seekWidgetByName(protectionPanel,"setsliderGeneralMp");
	slider_setGeneralMp->setTouchEnabled(true);
	slider_setGeneralMp->setPercent(curSliderGeneralMp);
	slider_setGeneralMp->addEventListener(CC_CALLBACK_2(RobotMainUI::setsliderGeneralMp,this));

	// GeneralBuyHp
	checkBoxGeneralBuyHp =(CheckBox *)Helper::seekWidgetByName(protectionPanel,"CheckBox_honggou_4");
	checkBoxGeneralBuyHp->setTouchEnabled(true);
	int m_SelectgeneralbuyHp = MyPlayerAIConfig::getGeneralBuyHp();
	if (m_SelectgeneralbuyHp==1)
	{
		checkBoxGeneralBuyHp->setSelected(true);
	}else
	{
		checkBoxGeneralBuyHp->setSelected(false);
	}
	checkBoxGeneralBuyHp->addEventListener(CC_CALLBACK_2(RobotMainUI::setCheckBoxGeneralBuyHp,this));

	// GeneralBuyMp
	checkBoxGeneralBuyMp =(CheckBox *)Helper::seekWidgetByName(protectionPanel,"CheckBox_honggou_9");
	checkBoxGeneralBuyMp->setTouchEnabled(true);
	int m_SelectgeneralbuyMp = MyPlayerAIConfig::getGeneralBuyMp();
	if (m_SelectgeneralbuyMp==1)
	{
		checkBoxGeneralBuyMp->setSelected(true);
	}else
	{
		checkBoxGeneralBuyMp->setSelected(false);
	}
	checkBoxGeneralBuyMp->addEventListener(CC_CALLBACK_2(RobotMainUI::setCheckBoxGeneralBuyMp,this));

	// GeneralBuyHpLabel
	labelGeneralbuyHpId_ =(Text*)Helper::seekWidgetByName(protectionPanel,"Label_GeneralRobotHpName");
	labelGeneralbuyHpId_->setString("");

	// GeneralBuyMpLabel
	labelGeneralbuyMpId_ = (Text*)Helper::seekWidgetByName(protectionPanel, "Label_GeneralRobotMpName");
	labelGeneralbuyMpId_->setString("");

	/// automatic equip
	auto checkBoxReapair =(CheckBox *)Helper::seekWidgetByName(protectionPanel,"CheckBox_honggou_8");
	checkBoxReapair->setTouchEnabled(true);
	int m_Selectrepairequip= MyPlayerAIConfig::getAutoRepairEquip();
	if (m_Selectrepairequip==1)
	{
		checkBoxReapair->setSelected(true);
	}else
	{
		checkBoxReapair->setSelected(false);
	}
	checkBoxReapair->addEventListener(CC_CALLBACK_2(RobotMainUI::setCheckBoxRepairEquip,this));

	
	//���ʾ���õ�ҩƷ
	for (int i=1;i<5;i++)
	{
		char strIndex[20];
		sprintf(strIndex,"Button_retore_%d",i);
		this->showAutomaticGoods(strIndex);
	}
	
}

void RobotMainUI::initGoodsPanel()
{
	for (int i=1;i<9;i++)
	{
		std::string checkBoxName ="CheckBox_honggou_";
		char checkIndex[3];
		sprintf(checkIndex,"%d",i);
		checkBoxName.append(checkIndex);

		auto checkBoxSetgoods =(CheckBox *)Helper::seekWidgetByName(goodsPanel,checkBoxName.c_str());
		checkBoxSetgoods->setTouchEnabled(true);
		checkBoxSetgoods->addEventListener(CC_CALLBACK_2(RobotMainUI::setGoodsPanelAllChexkBox,this));
		int vale_ = MyPlayerAIConfig::getGoodsPickAndSell(i);
		if (vale_==1)
		{
			checkBoxSetgoods->setSelected(true);
		}else
		{
			checkBoxSetgoods->setSelected(false);
		}
		checkBoxSetgoods->setTag(i);
	}
}

void RobotMainUI::setsliderHp(Ref *pSender, ui::Slider::EventType type)
{
	auto slider = dynamic_cast<Slider*>(pSender);
	int percent = slider->getPercent();

	//MyPlayerAIConfig::setRoleLimitHp(percent);
	curSliderHp = percent;

	const char *str_RoleHp = StringDataManager::getString("robotui_role_hp");
	const char *str_automatic = StringDataManager::getString("robotui_role_automatic");
	std::string siliderHpNum_ =__String::createWithFormat("%d",percent)->getCString();
	std::string sliderHplabel_="";
	sliderHplabel_.append(str_RoleHp);
	sliderHplabel_.append("<");
	sliderHplabel_.append(siliderHpNum_);
	sliderHplabel_.append("%");
	sliderHplabel_.append(str_automatic);

	rolesliderHplabel->setString(sliderHplabel_.c_str());
}

void RobotMainUI::setsliderMp(Ref *pSender, ui::Slider::EventType type)
{
	auto slider = dynamic_cast<Slider*>(pSender);
	int percent = slider->getPercent();
	
	//MyPlayerAIConfig::setRoleLimitMp(percent);
	curSliderMp = percent;

	const char *str_RoleMp = StringDataManager::getString("robotui_role_mp");
	std::string siliderMpNum_ =__String::createWithFormat("%d",percent)->getCString();
	const char *str_automatic = StringDataManager::getString("robotui_role_automatic");
	std::string sliderMplabel_="";
	sliderMplabel_.append(str_RoleMp);
	sliderMplabel_.append("<");
	sliderMplabel_.append(siliderMpNum_);
	sliderMplabel_.append("%");
	sliderMplabel_.append(str_automatic);

	rolesliderMplabel->setString(sliderMplabel_.c_str());
}

void RobotMainUI::setsliderGeneralHp(Ref *pSender, ui::Slider::EventType type)
{
	auto slider = dynamic_cast<Slider*>(pSender);
	int percent = slider->getPercent();

	//MyPlayerAIConfig::setGeneralLimitHp(percent);
	curSliderGeneralHp = percent;

	const char *str_RoleMp = StringDataManager::getString("robotui_generalHp");
	std::string siliderMpNum_ =__String::createWithFormat("%d",percent)->getCString();
	const char *str_automatic = StringDataManager::getString("robotui_role_automatic");
	std::string sliderMplabel_="";
	sliderMplabel_.append(str_RoleMp);
	sliderMplabel_.append("<");
	sliderMplabel_.append(siliderMpNum_);
	sliderMplabel_.append("%");
	sliderMplabel_.append(str_automatic);

	generalSliderHplabel->setString(sliderMplabel_.c_str());
}

void RobotMainUI::setsliderGeneralMp(Ref *pSender, ui::Slider::EventType type)
{
	auto slider = dynamic_cast<Slider*>(pSender);
	int percent = slider->getPercent();
	
	//MyPlayerAIConfig::setGeneralLimitMp(percent);
	curSliderGeneralMp = percent;


	const char *str_RoleMp = StringDataManager::getString("robotui_generalMp");
	std::string siliderMpNum_ =__String::createWithFormat("%d",percent)->getCString();
	const char *str_automatic = StringDataManager::getString("robotui_role_automatic");
	std::string sliderMplabel_="";
	sliderMplabel_.append(str_RoleMp);
	sliderMplabel_.append("<");
	sliderMplabel_.append(siliderMpNum_);
	sliderMplabel_.append("%");
	sliderMplabel_.append(str_automatic);

	generalSliderMplabel->setString(sliderMplabel_.c_str());
}

void RobotMainUI::setCheckBoxuseHp(Ref* pSender, CheckBox::EventType type)
{
	auto checkBox =dynamic_cast<CheckBox *>(pSender);
	int select_ =checkBox->isSelected();

	MyPlayerAIConfig::setRoleUseHp(select_);
}
void RobotMainUI::setCheckBoxuseMp(Ref* pSender, CheckBox::EventType type)
{
	auto checkBox =dynamic_cast<CheckBox *>(pSender);
	int select_ =checkBox->isSelected();

	MyPlayerAIConfig::setRoleUseMp(select_);

}

void RobotMainUI::setCheckBoxBuyHp(Ref* pSender, CheckBox::EventType type)
{
	auto checkBox =dynamic_cast<CheckBox *>(pSender);
	int select_ =checkBox->isSelected();
	
	MyPlayerAIConfig::setRoleBuyHp(select_);

}

void RobotMainUI::setCheckBoxBuyMp(Ref* pSender, CheckBox::EventType type)
{
	auto checkBox =dynamic_cast<CheckBox *>(pSender);
	int select_ =checkBox->isSelected();

	MyPlayerAIConfig::setRoleBuyMp(select_);

}

void RobotMainUI::setCheckBoxGeneralHp(Ref* pSender, CheckBox::EventType type)
{
	auto checkBox =dynamic_cast<CheckBox *>(pSender);
	int select_ =checkBox->isSelected();

	MyPlayerAIConfig::setGeneralUseHp(select_);

}

void RobotMainUI::setCheckBoxGeneralMp(Ref* pSender, CheckBox::EventType type)
{
	auto checkBox =dynamic_cast<CheckBox *>(pSender);
	int select_ =checkBox->isSelected();

	MyPlayerAIConfig::setGeneralUseMp(select_);

}

void RobotMainUI::setCheckBoxGeneralBuyHp(Ref* pSender, CheckBox::EventType type)
{
	auto checkBox =dynamic_cast<CheckBox *>(pSender);
	int select_ =checkBox->isSelected();
	
	MyPlayerAIConfig::setGeneralBuyHp(select_);

}

void RobotMainUI::setCheckBoxGeneralBuyMp(Ref* pSender, CheckBox::EventType type)
{
	auto checkBox =dynamic_cast<CheckBox *>(pSender);
	int select_ =checkBox->isSelected();

	MyPlayerAIConfig::setGeneralBuyMp(select_);
}


void RobotMainUI::setCheckBoxRepairEquip(Ref* pSender, CheckBox::EventType type)
{
	int myVip_Robot_Repair_limit = 0;
	VipConfigData::TypeIdAndVipLevel typeAndLevel_autoSell;
	typeAndLevel_autoSell.typeName = StringDataManager::getString("vip_function_instance_RobotRepairEquipment");

	for(int i = 0;i<VipDescriptionConfig::s_vipDes.size();++i)
	{
		typeAndLevel_autoSell.vipLevel = i;

		std::map<VipConfigData::TypeIdAndVipLevel,CVipInfo*>::const_iterator cIter;
		cIter = VipConfigData::s_vipConfig.find(typeAndLevel_autoSell);
		if (cIter == VipConfigData::s_vipConfig.end()) // û�ҵ�����ָ�END��  
		{
			continue;
		}
		else
		{
			myVip_Robot_Repair_limit = typeAndLevel_autoSell.vipLevel;
			break;
		}
	}

	auto checkBox =dynamic_cast<CheckBox *>(pSender);
	if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() >= myVip_Robot_Repair_limit)
	{
		int select_ =checkBox->isSelected();
		MyPlayerAIConfig::setAutoRepairEquip(select_);
	}else
	{
		checkBox->setSelected(false);

		std::string str_des = StringDataManager::getString("vip_function_des");
		char s_vipLevel[20];
		sprintf(s_vipLevel,"%d",myVip_Robot_Repair_limit);
		str_des.append(s_vipLevel);
		str_des.append(StringDataManager::getString("vip_function_willOpen"));
		GameView::getInstance()->showAlertDialog(str_des.c_str());

		//std::string funName_ = StringDataManager::getString("vip_function_instance_RobotRepairEquipment");
		//this->showVipFunctionOpen(funName_);
	}
	/*
	checkBox->setSelected(false);
	const char *strings_ = StringDataManager::getString("feature_will_be_open");
	GameView::getInstance()->showAlertDialog(strings_);
	*/
}

void RobotMainUI::setGoodPickAndSellOption(int robotState)
{
	if (robotState == STARTROBOTSTATE)
	{
		/*�ʰȡ�� 1ȫ�� 2��װ 3�װ 4��װ  */
		/*�۳ 5�ȫ�� 6��װ 7�װ 8��װ  */
		auto pickOption =new ReqSyncPickupSeting1212();
		for (int i=1;i<9;i++)
		{
			int checkBoxSelect = MyPlayerAIConfig::getGoodsPickAndSell(i);
			if (i==1 && checkBoxSelect ==0)
			{
				pickOption->set_type(ALL);

				GameMessageProcessor::sharedMsgProcessor()->sendReq(1212,pickOption);
				delete pickOption;
				return;
			}else
			{
				if (checkBoxSelect == 0)
				{
					pickOption->set_type(OPTION);
					switch(i)
					{
					case 2:
						{
							pickOption->add_list(GREEN_EQ);
						}break;
					case 3:
						{
							pickOption->add_list(BLUE_EQ);
						}break;
					case 4:
						{
							pickOption->add_list(WHITE_EQ);
						}break;
					case 5:
						{
							pickOption->add_list(STRENGTHEN_STORE);
						}break;
					}
				}
			}
		}
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1212,pickOption);
		delete pickOption;
	}else if (robotState==ENDROBOTSTATE)
	{
		auto pickOption =new ReqSyncPickupSeting1212();
		pickOption->set_type(ALL);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1212,pickOption);
		delete pickOption;
	}
}

void RobotMainUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void RobotMainUI::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	tutorialIndicator->setPosition(Vec2(pos.x+180+_w,pos.y-60+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,87,39,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+135.5f+_w,pos.y+19.5f+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void RobotMainUI::addCCTutorialIndicator2( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	tutorialIndicator->setPosition(Vec2(pos.x+280+_w,pos.y-60+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,87,39,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+228+_w,pos.y+19.5f+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void RobotMainUI::addCCTutorialIndicatorClose( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	tutorialIndicator->setPosition(Vec2(pos.x-20+_w,pos.y-55+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,40,40,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+_w+45,pos.y+_h+15));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void RobotMainUI::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

// 	CCTutorialParticle* tutorialParticle = dynamic_cast<CCTutorialParticle*>(this->getChildByTag(CCTUTORIALPARTICLETAG));
// 	if(tutorialParticle != NULL)
// 		tutorialParticle->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void RobotMainUI::showVipFunctionOpen( std::string functionName )
{
	int opelLevel = -1;
	VipConfigData::TypeIdAndVipLevel typeAndLevel_autoSell;
	typeAndLevel_autoSell.typeName = functionName;

	for(int i = 0;i<=VipDescriptionConfig::s_vipDes.size();++i)
	{
		typeAndLevel_autoSell.vipLevel = i;

		std::map<VipConfigData::TypeIdAndVipLevel,CVipInfo*>::const_iterator cIter;
		cIter = VipConfigData::s_vipConfig.find(typeAndLevel_autoSell);
		if (cIter == VipConfigData::s_vipConfig.end()) // û�ҵ�����ָ�END��  
		{
			continue;
		}
		else
		{
			opelLevel = typeAndLevel_autoSell.vipLevel;
			break;
		}
	}
	if (opelLevel < 0)
		return;
	if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() < opelLevel)
	{
		std::string str_des = StringDataManager::getString("vip_function_des");
		char s_vipLevel[20];
		sprintf(s_vipLevel,"%d",opelLevel);
		str_des.append(s_vipLevel);
		str_des.append(StringDataManager::getString("vip_function_willOpen"));
		GameView::getInstance()->showAlertDialog(str_des.c_str());
		return;
	}
}

std::string RobotMainUI::getSkillTypeStr( const char * skillid )
{
	std::map<std::string,CBaseSkill*>::const_iterator cIter;
	cIter = StaticDataBaseSkill::s_baseSkillData.find(skillid);
	if (cIter == StaticDataBaseSkill::s_baseSkillData.end())
	{
		return NULL;
	}

	auto baseSkill = StaticDataBaseSkill::s_baseSkillData[skillid];

	std::string str_skillVariety_path = "res_ui/font/";
	switch(baseSkill->get_skill_variety())
	{
	case 1:
		{
			str_skillVariety_path.append("dan.png");
		}
		break;
	case 2:
		{
			str_skillVariety_path.append("qun.png");
		}
		break;
	case 3:
		{
			str_skillVariety_path.append("kong.png");
		}
		break;
	case 4:
		{
			str_skillVariety_path.append("liao.png");
		}
		break;
	case 5:
		{
			str_skillVariety_path.append("te.png");
		}
		break;
	case 6:
		{
			str_skillVariety_path.append("fu.png");
		}
		break;
	default:
		{
			str_skillVariety_path.append("fu.png");
		}break;
	}
	
	return str_skillVariety_path;
}








