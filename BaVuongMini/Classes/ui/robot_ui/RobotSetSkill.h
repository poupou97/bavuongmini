
#ifndef _ROBOTUI_ROBOTSETSKILL_H
#define _ROBOTUI_ROBOTSETSKILL_H

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GameFightSkill;
class RobotSetSkill: public UIScene,public TableViewDelegate,public TableViewDataSource
{
public:
	RobotSetSkill(void);
	~RobotSetSkill(void);

	struct ReqData{
		long long generalsid ;
		std::string skillid ;
		int index;
	};

	static RobotSetSkill * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//处理触摸事件，可以计算点击的是哪一个子项
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//每一项的宽度和高度
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//生成列表每一项的内容
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//一共生成多少项
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	
	void refreshSkillInfo(GameFightSkill * gameFightSkill);
	//装配
	void EquipEvent(Ref *pSender, Widget::TouchEventType type);


	void showSkillDes(std::string curSkillDes,std::string nextSkillDes);
public: 
	//std::vector<GameFightSkill *>DataSourceList;
	TableView * generalsSkillsList_tableView;

	ImageView* i_skillFrame;
	ImageView* i_skillImage;
	Text* l_skillName;
	Text* l_skillType;
	Text* l_skillLvValue ;
	ImageView *imageView_skillQuality;
	Label* l_skillQualityValue;
	Text* l_skillMpValue;
	Text* l_skillCDTimeValue ;
	Text* l_skillDistanceValue;

	int setEquipSkillIndex;
	std::vector<GameFightSkill *> generalSkillVector;

	Layer * Layer_skills;
private:
	int setSkillIndex;
};

#endif