#include "OnlineGiftData.h"
#include "../../messageclient/element/COneOnlineGift.h"
#include "OnlineGift.h"
#include "../../messageclient/element/GoodsInfo.h"

OnlineGiftData * OnlineGiftData::s_onlineGiftData = NULL;

OnlineGiftData::OnlineGiftData(void)
{
}


OnlineGiftData::~OnlineGiftData(void)
{
	std::vector<COneOnlineGift *>::iterator iter;
	for (iter = m_vector_oneOnLineGift.begin(); iter != m_vector_oneOnLineGift.end(); iter++)
	{
		delete *iter;
	}
	m_vector_oneOnLineGift.clear();

	std::vector<OnlineGift *>::iterator iterOnlineGift;
	for (iterOnlineGift = m_vector_OnlineGift.begin(); iterOnlineGift != m_vector_OnlineGift.end(); iterOnlineGift++)
	{
		delete *iterOnlineGift;
	}
	m_vector_OnlineGift.clear();
}

OnlineGiftData * OnlineGiftData::instance()
{
	if (NULL == s_onlineGiftData)
	{
		s_onlineGiftData = new OnlineGiftData();
	}

	return s_onlineGiftData;
}

int OnlineGiftData::initGetGiftNeedTime()
{
	for (unsigned int i=0; i < m_vector_oneOnLineGift.size(); i++)
	{

		int nGiftNum =  m_vector_oneOnLineGift.at(i)->number();
		int nGiftNeddTime =  m_vector_oneOnLineGift.at(i)->needtime();
		int nGiftStatus = m_vector_oneOnLineGift.at(i)->status();	

		// �����ǰ״̬ Ϊ ����ȡ�����ѯ��һ������������Ļ� ��ǰ���� Ϊ ������ȡ�Ľ���
		if (3 == nGiftStatus)
		{
			continue;
		}
		else
		{
			return nGiftNeddTime;
		}
	}

	return 0;
}

int OnlineGiftData::initGetGiftNum()
{
	for (unsigned int i=0; i < m_vector_oneOnLineGift.size(); i++)
	{

		int nGiftNum =  m_vector_oneOnLineGift.at(i)->number();
		int nGiftNeddTime =  m_vector_oneOnLineGift.at(i)->needtime();
		int nGiftStatus = m_vector_oneOnLineGift.at(i)->status();	

		// �����ǰ״̬ Ϊ ����ȡ�����ѯ��һ������������Ļ� ��ǰ���� Ϊ ������ȡ�Ľ���
		if (3 == nGiftStatus)
		{
			continue;
		}
		else
		{
			return nGiftNum;
		}
	}

	// ����8��˵�����н���ȫ����ȡ���
	return 8;
}

void OnlineGiftData::getGiftDataTimeFromIcon( const int nTime, const unsigned int nNum )
{
	// ���nNumС��m_vector_oneOnLineGift.size(),˵�� ������������򣬽���ȫ����ȡ���
	if (nNum < m_vector_oneOnLineGift.size())
	{
		for (unsigned int i = 0; i < m_vector_oneOnLineGift.size(); i++)
		{
			// ǰ��� ���� ȫ�� ʱ����Ϊ 0��status ��Ϊ ����ȡ��3��
			if (m_vector_oneOnLineGift.at(i)->number() < m_vector_oneOnLineGift.at(nNum)->number())
			{
				m_vector_oneOnLineGift.at(i)->set_needtime(0);
				m_vector_oneOnLineGift.at(i)->set_status(3);
			}
			else if (m_vector_oneOnLineGift.at(i)->number()  ==  m_vector_oneOnLineGift.at(nNum)->number())		// ���� ��ǰ�������ΪnNum��ʣ��ʱ��ΪnTime,status��Ϊ ����ȡ��2��
			{
				m_vector_oneOnLineGift.at(i)->set_needtime(nTime);

				// ���ʣ��ʱ��Ϊ0����Ϊ����ȡ��2��������Ϊ������ȡ��1��
				if (0 == nTime)
				{
					m_vector_oneOnLineGift.at(i)->set_status(2);
				}
				else
				{
					m_vector_oneOnLineGift.at(i)->set_status(1);
				}
			}

		}
	}
	else
	{
		for (unsigned int i = 0; i < m_vector_oneOnLineGift.size(); i++)
		{
			m_vector_oneOnLineGift.at(i)->set_needtime(0);
			m_vector_oneOnLineGift.at(i)->set_status(3);
		}
	}
}

void OnlineGiftData::initDataFromIntent()
{
	// �������
	std::vector<OnlineGift *>::iterator iter;
	for (iter = m_vector_OnlineGift.begin(); iter != m_vector_OnlineGift.end(); iter++)
	{
		delete *iter;
	}
	m_vector_OnlineGift.clear();

	for (unsigned int i=0; i < m_vector_oneOnLineGift.size(); i++)
	{
		auto onlineGift = new OnlineGift();

		onlineGift->m_nGiftNum = m_vector_oneOnLineGift.at(i)->number();							// ��Ʒ��ţ���0��ʼ
		onlineGift->m_nTimeLeft = m_vector_oneOnLineGift.at(i)->needtime();						// ��Ҫ��ȡ��Ʒʱ��
		onlineGift->m_nStatus = m_vector_oneOnLineGift.at(i)->status();								// ��Ʒ��ȡ״̬
		onlineGift->m_nTimeAll = m_vector_oneOnLineGift.at(i)->fixtime();							// ��Ʒ��ȡ��Ҫ�Ĺ̶�ʱ��

		int goodsSize = m_vector_oneOnLineGift.at(i)->goods_size();
		for (int goodsIndex_ =0; goodsIndex_ < goodsSize; goodsIndex_++)
		{
			auto goodsInfo = new GoodsInfo();
			goodsInfo->CopyFrom(m_vector_oneOnLineGift.at(i)->goods(goodsIndex_));
			onlineGift->m_vector_goods.push_back(goodsInfo);
		}

		int goodsNumSize = m_vector_oneOnLineGift.at(i)->goodsnumber_size();
		for (int goodsNumIndex = 0; goodsNumIndex < goodsNumSize; goodsNumIndex++)
		{
			onlineGift->m_vector_goodsNum.push_back(m_vector_oneOnLineGift.at(i)->goodsnumber(goodsNumIndex));		// goods num
		}

		m_vector_OnlineGift.push_back(onlineGift);
	}
}

bool OnlineGiftData::isAllGiftHasGet()
{
	for (unsigned int i=0; i < m_vector_oneOnLineGift.size(); i++)
	{
		int nGiftStatus = m_vector_oneOnLineGift.at(i)->status();	

		// ��������� ����״̬ȫ��Ϊ ����ȡ��
		if (3 == nGiftStatus)
		{
			continue;
		}
		else
		{
			return false;
		}

	}

	return true;
}

GoodsInfo* OnlineGiftData::getGoosInfo(unsigned int nGift, int nGiftNum )
{
	for (unsigned int i = 0; i < m_vector_OnlineGift.size(); i++)
	{
		if (nGift < m_vector_OnlineGift.size())
		{
			return m_vector_OnlineGift.at(nGift)->m_vector_goods.at(nGiftNum);
		}
	}

	return NULL;
}


