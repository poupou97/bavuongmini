#ifndef _UI_ONLINEREWARD_ONLINEGIFTICONWIDGET_H_
#define _UI_ONLINEREWARD_ONLINEGIFTICONWIDGET_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CCTutorialParticle;

class OnlineGiftIconWidget:public Widget
{
public:
	OnlineGiftIconWidget(void);
	~OnlineGiftIconWidget(void);

public:
	static OnlineGiftIconWidget* create();
	bool init();

	virtual void update(float dt);

public:
	Button * m_btn_onlineGiftIcon;					// �ɵ��ICON
	Layer * m_layer_onlineGift;						// ���Ҫ�Layer
	Label * m_label_onlineGift_time;				// �ʣ��ʱ��label		
	cocos2d::extension::Scale9Sprite * m_spirte_fontBg;				// �������ʾ�ĵ�ͼ

public:
	void callBackBtnOnlineGift(Ref *pSender, Widget::TouchEventType type);

public:
	void initGiftTime(const int nTime);							// ��ǰ��� �� nTime �������ȡ
	void initGiftNum(const int nGiftNum);					// ��ǰ����ı�

	void initDataFromIntent();										// Ŵӷ�������ݳ�ʼ��Icon

	void allGiftGetIconExit();										// ���н����ѳɹ���ȡ��icon��ʧ�����ߵȼ��ﵽ���ޣ�icon��ʧ��

	int m_nTimeShow;												// ��ʾ����� ĸý������ȡ��ʱ�
	int m_nOnlineGiftTime;											// ���������ȡ� ĸý�� ���ȡ ����ʱ�
	int m_nGiftNum;													// 䵱ǰ��ʾ��� � ı�

	float m_timeDelta;

private:
	 std::string timeFormatToString(int nTime);
	 void refreshData();

	 CCTutorialParticle * m_tutorialParticle;					// ����߽�����Ч
	

};

#endif

