#include "OnlineGiftIcon.h"
#include "OnLineRewardUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "OnlineGiftData.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "AppMacros.h"

#define  TAG_TUTORIALPARTICLE 500

OnlineGiftIcon::OnlineGiftIcon(void)
	:m_nOnlineGiftTime(0)
	,m_nTimeShow(0)
	,m_btn_onlineGiftIcon(NULL)
	,m_layer_onlineGift(NULL)
	,m_label_onlineGift_time(NULL)
	,m_spirte_fontBg(NULL)
{

}


OnlineGiftIcon::~OnlineGiftIcon(void)
{
	OnlineGiftData::instance()->getGiftDataTimeFromIcon(m_nTimeShow, m_nGiftNum);
}

OnlineGiftIcon* OnlineGiftIcon::create()
{
	auto onlineGiftIcon = new OnlineGiftIcon();
	if (onlineGiftIcon && onlineGiftIcon->init())
	{
		onlineGiftIcon->autorelease();
		return onlineGiftIcon;
	}
	CC_SAFE_DELETE(onlineGiftIcon);
	return NULL;
}

bool OnlineGiftIcon::init()
{
	if (UIScene::init())
	{
		auto winSize = Director::getInstance()->getVisibleSize();

		// icon
		m_btn_onlineGiftIcon = Button::create();
		m_btn_onlineGiftIcon->setTouchEnabled(true);
		m_btn_onlineGiftIcon->setPressedActionEnabled(true);
		m_btn_onlineGiftIcon->loadTextures("gamescene_state/zhujiemian3/tubiao/OnlineAwards.png", "gamescene_state/zhujiemian3/tubiao/OnlineAwards.png","");
		m_btn_onlineGiftIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_btn_onlineGiftIcon->setPosition(Vec2( winSize.width-70-159, winSize.height-70));
		m_btn_onlineGiftIcon->addTouchEventListener(CC_CALLBACK_2(OnlineGiftIcon::callBackBtnOnlineGift, this));

		// label
		m_label_onlineGift_time = Label::createWithTTF("00:00", APP_FONT_NAME, 14);
		m_label_onlineGift_time->setColor(Color3B(255, 246, 0));
		m_label_onlineGift_time->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_onlineGift_time->setPosition(Vec2(m_btn_onlineGiftIcon->getPosition().x, m_btn_onlineGiftIcon->getPosition().y - m_btn_onlineGiftIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao70.png");//Rect(11, 8, 1, 17)
		m_spirte_fontBg->setPreferredSize(Size(47, 14));
		m_spirte_fontBg->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_onlineGift_time->getPosition());


		m_layer_onlineGift = Layer::create();
		//m_layer_onlineGift->setTouchEnabled(true);
		m_layer_onlineGift->setAnchorPoint(Vec2::ZERO);
		m_layer_onlineGift->setPosition(Vec2::ZERO);
		m_layer_onlineGift->setContentSize(winSize);

		m_layer_onlineGift->addChild(m_spirte_fontBg, -10);
		m_layer_onlineGift->addChild(m_btn_onlineGiftIcon);
		m_layer_onlineGift->addChild(m_label_onlineGift_time);


		addChild(m_layer_onlineGift);

		this->schedule(schedule_selector(OnlineGiftIcon::update),1.0f);

		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void OnlineGiftIcon::callBackBtnOnlineGift(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

	}
	break;

	case Widget::TouchEventType::CANCELED:
	{
		// ��Icon���µ��� �ͬ��� ��������
		OnlineGiftData::instance()->getGiftDataTimeFromIcon(m_nTimeShow, m_nGiftNum);
		OnlineGiftData::instance()->initDataFromIntent();


		// ݳ�ʼ�� UI WINDOW��
		Size winSize = Director::getInstance()->getVisibleSize();

		auto pOnLineRewardUI = OnLineRewardUI::create();
		pOnLineRewardUI->setIgnoreAnchorPointForPosition(false);
		pOnLineRewardUI->setAnchorPoint(Vec2(0.5f, 0.5f));
		pOnLineRewardUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		pOnLineRewardUI->setTag(kTagOnLineGiftUI);

		pOnLineRewardUI->refreshData();
		pOnLineRewardUI->refreshTimeAndNum();					// ݸ�� ½���� ź �ʱ�
		pOnLineRewardUI->update(0);

		GameView::getInstance()->getMainUIScene()->addChild(pOnLineRewardUI);
	}
		break;

	default:
		break;
	}
}

void OnlineGiftIcon::onEnter()
{
	UIScene::onEnter();
}

void OnlineGiftIcon::onExit()
{
	UIScene::onExit();
}

void OnlineGiftIcon::initGiftTime(const int nTime )
{
	m_nTimeShow = nTime;
	m_nOnlineGiftTime = nTime;

	refreshData();
}


void OnlineGiftIcon::initGiftNum( const int nGiftNum )
{
	m_nGiftNum = nGiftNum;
}

void OnlineGiftIcon::update( float dt )
{
	m_nOnlineGiftTime -= 1000;
	m_nTimeShow = m_nOnlineGiftTime;
	if (m_nTimeShow <= 0)
	{
		m_nTimeShow = 0;
	}

	refreshData();
}

std::string OnlineGiftIcon::timeFormatToString( int nTime )
{
	if (0 == nTime)
	{
		std::string timeString = "";
		const char *str1 = StringDataManager::getString("label_timeout");
		timeString.append(str1);
		return timeString;
	}
	else
	{
		int int_m = nTime / 1000 / 60;
		int int_s = nTime / 1000 % 60;

		std::string timeString = "";
		char str_m[10];
		if (int_m < 10)
		{
			timeString.append("0");
		}
		sprintf(str_m,"%d",int_m);
		timeString.append(str_m);
		timeString.append(":");

		char str_s[10];
		if (int_s < 10)
		{
			timeString.append("0");
		}
		sprintf(str_s,"%d",int_s);
		timeString.append(str_s);

		return timeString;
	}

}

void OnlineGiftIcon::refreshData()
{
	std::string strShow = timeFormatToString(m_nTimeShow);

	std::string timeString = "";
	const char *str1 = StringDataManager::getString("label_timeout");
	timeString.append(str1);

	// ��ж Ͻ�Ҫ��ʾ���Ƿ�Ϊ������ȡ�������ǣ��򴴽���Ч����� ��Ƴ���Ч�������ǰ����Ч���ڵĻ���
	if (strShow == timeString)
	{
		auto tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL == tutorialParticle)
		{
			m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", m_btn_onlineGiftIcon->getContentSize().width, m_btn_onlineGiftIcon->getContentSize().height + 10);
			m_tutorialParticle->setPosition(Vec2(m_btn_onlineGiftIcon->getPosition().x, m_btn_onlineGiftIcon->getPosition().y - 35));
			m_tutorialParticle->setAnchorPoint(Vec2(0.5f, 0.5f));
			m_tutorialParticle->setTag(TAG_TUTORIALPARTICLE);

			this->addChild(m_tutorialParticle);
		}
	}
	else 
	{
		auto tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL != tutorialParticle)
		{
			this->removeChild(m_tutorialParticle);
		}
	}

	m_label_onlineGift_time->setString(strShow.c_str());
}

void OnlineGiftIcon::initDataFromIntent()
{
	// ���߽��ICON ��ȡ��
	this->initGiftTime(OnlineGiftData::instance()->initGetGiftNeedTime());
	this->initGiftNum(OnlineGiftData::instance()->initGetGiftNum());
}

void OnlineGiftIcon::allGiftGetIconExit()
{
	// ����н����ȡ�ɹ����رiconմ��
	this->closeAnim();
}

