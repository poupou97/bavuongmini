#ifndef _UI_ONLINEREWARD_ONLINEGIFTDATA_H_
#define _UI_ONLINEREWARD_ONLINEGIFTDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class COneOnlineGift;
class OnlineGift;
class GoodsInfo;

class OnlineGiftData
{
public:
	static OnlineGiftData * s_onlineGiftData;
	static OnlineGiftData * instance();
private:
	OnlineGiftData(void);
	~OnlineGiftData(void);

public:
	std::vector<COneOnlineGift *> m_vector_oneOnLineGift;											// OnlineGift��vector
	std::vector<OnlineGift *> m_vector_OnlineGift;														// һ��OnlineGift�е�Gift���ݵ�vector

	int initGetGiftNeedTime();																							// �÷����� �е� ���� ��ʼ�� ��ǰ���ڽ��н��� ����ʱ��
	int initGetGiftNum();																									// �÷����� �е� ���� ��ʼ�� ��ǰ���ڽ��н��� �ı��
	void initDataFromIntent();																							// ��internet �е� ���� ��ʼ�� native����

	void getGiftDataTimeFromIcon(const int nTime, const unsigned int nNum);								// ��icon�����µ����� ���� ����������		
	
	bool isAllGiftHasGet();

	GoodsInfo* getGoosInfo(unsigned int nGift, int nGiftNum);													// nGift��ţ�nGiftNum �ڼ�����������0��ʼ��

};

#endif

