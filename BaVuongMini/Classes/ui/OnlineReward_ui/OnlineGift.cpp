#include "OnlineGift.h"
#include "../../messageclient/element/GoodsInfo.h"


OnlineGift::OnlineGift(void)
	:m_btn_gift(NULL)
	,m_image_red(NULL)
	,m_image_text(NULL)
	,m_label_timeLeft(NULL)
	,m_btn_gift1_frame(NULL)
	,m_image_gift1_icon(NULL)
	,m_label_gift1_num(NULL)
	,m_btn_gift2_frame(NULL)
	,m_image_gift2_icon(NULL)
	,m_label_gift2_num(NULL)
	,m_BMFONT_timeFix(NULL)
	,m_nTimeLeft(0)
	,m_nTimeAll(0)
	,m_nGiftNum(0)
	,m_nStatus(0)
{
					
}


OnlineGift::~OnlineGift(void)
{
	std::vector<GoodsInfo *>::iterator iter;
	for (iter = m_vector_goods.begin(); iter != m_vector_goods.end(); iter++)
	{
		delete *iter;
	}
	m_vector_goods.clear();
}
