#ifndef _UI_ONLINEREWARD_ONLINEGIFT_H_
#define _UI_ONLINEREWARD_ONLINEGIFT_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GoodsInfo;

class OnlineGift
{
public:
	OnlineGift(void);
	~OnlineGift(void);
public:
	Button * m_btn_gift;						// btn
	Text * m_label_btnGift_text;			// btn �ı�

	ImageView * m_image_red;			// ��ǰ���ڽ��е Ľ����ɫ����������ʾ��

	ImageView * m_image_text;			// ʱ��δ��Ŷ image
	Text * m_label_timeLeft;				// ʣ��ʱ� ��� �ʱ��δ��Ŷ		
			
	Button * m_btn_gift1_frame;
	ImageView * m_image_gift1_icon;
	Text * m_label_gift1_num;
	
	Button * m_btn_gift2_frame;
	ImageView * m_image_gift2_icon;
	Text * m_label_gift2_num;

	Label * m_BMFONT_timeFix;				// �̶�ʱ�

	int m_nTimeLeft;								// 䵱ǰ��� �ʣ��ʱ�
	int m_nTimeAll;									// 䵱ǰ��� �̶�ʱ�
	int m_nGiftNum;								// 佱���
	int m_nStatus;									// 1:Ų�����ȡ��2������ȡ��3������ȡ��

	std::vector<GoodsInfo *> m_vector_goods;
	std::vector<int > m_vector_goodsNum;
};

#endif

