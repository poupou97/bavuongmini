#ifndef _UI_ONLINEREWARD_ONLINEREWARDUI_H_
#define _UI_ONLINEREWARD_ONLINEREWARDUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ���߽�� ���
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.11
 */

class COneOnlineGift;
class OnlineGift;
class CCTutorialParticle;

class OnLineRewardUI : public UIScene
{
public:
	OnLineRewardUI();
	virtual ~OnLineRewardUI();

	static Layout * s_pPanel;

	static OnLineRewardUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);

	void update(float dt);

public:
	void callBackBtnClolse(Ref *pSender, Widget::TouchEventType type);																	// �رհ�ť����Ӧ
	void callBackBtnGetGift(Ref *pSender, Widget::TouchEventType type);																	// ��ȡ��� �ť����Ӧ
	void callBackBtnGiftInfo(Ref *pSender, Widget::TouchEventType type);																	// �����ʱ����Ӧ

public:

	void refreshData();																									// � ÷���� ��е���ݳ�ʼ�� UI
	int getGiftNeedTime();																								// �÷���� ��е ��� ݳ�ʼ�� ��ǰ���ڽ��н�� �����ʱ�
	int getGiftNum();																										// ��÷���� ��е ��� ݳ�ʼ�� ��ǰ���ڽ��н�� �ı�

	void giftGetSuc(const int nGiftNum, const int nGiftStatus);										// UIŽ�� ��ȡ ���ΪnNum�Ľ���ɹ�

	void refreshTimeAndNum();

private:
	void initUI();

	void initBtnStatus(OnlineGift * onlineGift);						// ��ʼ��Btn��״̬
	void initGoodsInfo(OnlineGift* onlineGift);						// ��ʼ��goods����Ϣ
	void initGiftFixTime(OnlineGift * onlineGift);						// ��ʼ������Ĺ̶���ȡʱ�

	int m_nTimeShow;															// ���ʾ����� ĸý������ȡ��ʱ�
	int m_nOnlineGiftTime;														// ���������ȡ� ĸý�� ���ȡ ����ʱ�

	int m_nGiftNum;																// 䵱ǰ���ڽ��е GiftNum;

	void refreshTime();															// �ÿ���� ����ڽ��н��� �ʣ��ʱ�

	std::string timeFormatToString(int nTime);						// �ʱ��ת�����

	void refreshGiftTime();														// �ǰ��� �� nTime �������ȡ
	void refreshGiftNum();														// ��ǰ����ı�

	std::string getEquipmentQualityByIndex(int quality);			// Ÿ� �Ʒ� ׻�øgoods���Ҫ�ĵ�ͼ

	CCTutorialParticle* m_tutorialParticle;								// ���ƿ���ȡ��ť�������Ч


	typedef enum TagBtnGet{
		TAG_BTN_GET_1 = 101,
		TAG_BTN_GET_2 = 102,
		TAG_BTN_GET_3 = 103,
		TAG_BTN_GET_4 = 104,
		TAG_BTN_GET_5 = 105,
		TAG_BTN_GET_6 = 106,
		TAG_BTN_GET_7 = 107,
		TAG_BTN_GET_8 = 108
	};

	typedef enum TagBtnGift1{
		TAG_BTN_GIFT_1_1 = 1011,
		TAG_BTN_GIFT_2_1 = 1021,
		TAG_BTN_GIFT_3_1 = 1031,
		TAG_BTN_GIFT_4_1 = 1041,
		TAG_BTN_GIFT_5_1 = 1051,
		TAG_BTN_GIFT_6_1 = 1061,
		TAG_BTN_GIFT_7_1 = 1071,
		TAG_BTN_GIFT_8_1 = 1081
	};

	typedef enum TagBtnGift2{
		TAG_BTN_GIFT_1_2 = 1012,
		TAG_BTN_GIFT_2_2 = 1022,
		TAG_BTN_GIFT_3_2 = 1032,
		TAG_BTN_GIFT_4_2 = 1042,
		TAG_BTN_GIFT_5_2 = 1052,
		TAG_BTN_GIFT_6_2 = 1062,
		TAG_BTN_GIFT_7_2 = 1072,
		TAG_BTN_GIFT_8_2 = 1082
	};

	TagBtnGet m_array_tagBtnGet[8];									// tag���
	TagBtnGift1 m_array_tagBtnGift1[8];
	TagBtnGift2 m_array_tagBtnGift2[8];


};

#endif