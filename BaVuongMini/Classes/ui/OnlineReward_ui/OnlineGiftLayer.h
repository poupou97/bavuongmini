#ifndef _UI_ONLINEREWARD_ONLINEGIFTLAYER_H_
#define _UI_ONLINEREWARD_ONLINEGIFTLAYER_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ���߽��layer����ڼ����ȡ״̬��
 * @author liuliang
 * @version 0.1.0
 * @date 2014.08.01
 */


class OnlineGiftLayer : public cocos2d::Layer
{
public:
	static OnlineGiftLayer * s_onLineGiftLayer;
	static OnlineGiftLayer * instance();

	static OnlineGiftLayer* create();

	virtual bool init();
	virtual void update(float dt);

private:
	OnlineGiftLayer(void);
	~OnlineGiftLayer(void);

private:
	bool m_bIsUseful;
	std::string m_str_timeLeft;

public:
	void setLayerUserful(bool bFlag);

public:
	void initGiftTime(const int nTime);							// ��ǰ��� �� nTime �������ȡ
	void initGiftNum(const int nGiftNum);					// ��ǰ����ı�

	void initDataFromIntent();										// Ŵӷ�������ݳ�ʼ��Icon

	void allGiftGetLayerExit();										// ���н����ѳɹ���ȡ��layer�Ƴ���ߵȼ��ﵽ���ޣ�layer�Ƴ

	int m_nTimeShow;												// ���ʾ����� ĸý������ȡ��ʱ�
	int m_nOnlineGiftTime;											// ���������ȡ� ĸý�� ���ȡ ����ʱ�
	int m_nGiftNum;													// 䵱ǰ��ʾ��� � ı�

	float m_timeDelta;

public:
	int get_giftTime();
	int get_giftNum();

	std::string get_timeLeft();

private:
	std::string timeFormatToString(int nTime);
	void refreshData();
};

#endif

