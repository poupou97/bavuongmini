#include "OnLineRewardUI.h"

#include "../../loadscene_state/LoadSceneState.h"

#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../messageclient/element/COneOnlineGift.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "OnlineGift.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "OnlineGiftIcon.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "OnlineGiftData.h"
#include "../../utils/StaticDataManager.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "OnlineGiftIconWidget.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../utils/GameUtils.h"
#include "OnlineGiftLayer.h"

using namespace CocosDenshion;

Layout * OnLineRewardUI::s_pPanel = NULL;

#define  TAG_TUTORIALPARTICLE 500

OnLineRewardUI::OnLineRewardUI()
	:m_nTimeShow(0)
	,m_nOnlineGiftTime(0)
	,m_nGiftNum(0)
	,m_tutorialParticle(NULL)
{
	m_array_tagBtnGet[0] = TAG_BTN_GET_1;
	m_array_tagBtnGet[1] = TAG_BTN_GET_2;
	m_array_tagBtnGet[2] = TAG_BTN_GET_3;
	m_array_tagBtnGet[3] = TAG_BTN_GET_4;
	m_array_tagBtnGet[4] = TAG_BTN_GET_5;
	m_array_tagBtnGet[5] = TAG_BTN_GET_6;
	m_array_tagBtnGet[6] = TAG_BTN_GET_7;
	m_array_tagBtnGet[7] = TAG_BTN_GET_8;

	m_array_tagBtnGift1[0] = TAG_BTN_GIFT_1_1;
	m_array_tagBtnGift1[1] = TAG_BTN_GIFT_2_1;
	m_array_tagBtnGift1[2] = TAG_BTN_GIFT_3_1;
	m_array_tagBtnGift1[3] = TAG_BTN_GIFT_4_1;
	m_array_tagBtnGift1[4] = TAG_BTN_GIFT_5_1;
	m_array_tagBtnGift1[5] = TAG_BTN_GIFT_6_1;
	m_array_tagBtnGift1[6] = TAG_BTN_GIFT_7_1;
	m_array_tagBtnGift1[7] = TAG_BTN_GIFT_8_1;

	m_array_tagBtnGift2[0] = TAG_BTN_GIFT_1_2;
	m_array_tagBtnGift2[1] = TAG_BTN_GIFT_2_2;
	m_array_tagBtnGift2[2] = TAG_BTN_GIFT_3_2;
	m_array_tagBtnGift2[3] = TAG_BTN_GIFT_4_2;
	m_array_tagBtnGift2[4] = TAG_BTN_GIFT_5_2;
	m_array_tagBtnGift2[5] = TAG_BTN_GIFT_6_2;
	m_array_tagBtnGift2[6] = TAG_BTN_GIFT_7_2;
	m_array_tagBtnGift2[7] = TAG_BTN_GIFT_8_2;

}

OnLineRewardUI::~OnLineRewardUI()
{

}

OnLineRewardUI * OnLineRewardUI::create()
{
	auto pOnLineRewardUI = new OnLineRewardUI();
	if (pOnLineRewardUI && pOnLineRewardUI->init())
	{
		pOnLineRewardUI->autorelease();
		return pOnLineRewardUI;
	}
	CC_SAFE_DELETE(pOnLineRewardUI);
	return NULL;
}

bool OnLineRewardUI::init()
{
	if (UIScene::init())
	{
		if(LoadSceneLayer::onlineAwardsLayout->getParent() != NULL)
		{
			LoadSceneLayer::onlineAwardsLayout->removeFromParentAndCleanup(false);
		}
		s_pPanel = LoadSceneLayer::onlineAwardsLayout;

		m_pLayer->addChild(s_pPanel);

		initUI();

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(790, 440));

		this->schedule(schedule_selector(OnLineRewardUI::update),1.0f);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(OnLineRewardUI::onTouchBegan, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}

	return false;
}

void OnLineRewardUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void OnLineRewardUI::onExit()
{
	UIScene::onExit();

	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

bool OnLineRewardUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void OnLineRewardUI::callBackBtnClolse(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void OnLineRewardUI::callBackBtnGetGift(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto pUIBtn = (Button *)pSender;
		// ͨ�tag��ж��ǵڼ���Ʒ�Btnı������ĸ�Ʒ�������ȡ��
		if (TAG_BTN_GET_1 == pUIBtn->getTag())
		{
			// �����������ȡ��Ʒ
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)0);
		}
		else if (TAG_BTN_GET_2 == pUIBtn->getTag())
		{
			// �����������ȡ��Ʒ
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)1);
		}
		else if (TAG_BTN_GET_3 == pUIBtn->getTag())
		{
			// �����������ȡ��Ʒ
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)2);
		}
		else if (TAG_BTN_GET_4 == pUIBtn->getTag())
		{
			// �����������ȡ��Ʒ
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)3);
		}
		else if (TAG_BTN_GET_5 == pUIBtn->getTag())
		{
			// �����������ȡ��Ʒ
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)4);
		}
		else if (TAG_BTN_GET_6 == pUIBtn->getTag())
		{
			// �����������ȡ��Ʒ
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)5);
		}
		else if (TAG_BTN_GET_7 == pUIBtn->getTag())
		{
			// �����������ȡ��Ʒ
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)6);
		}
		else if (TAG_BTN_GET_8 == pUIBtn->getTag())
		{
			// �����������ȡ��Ʒ
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5111, (void *)7);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void OnLineRewardUI::initUI()
{
	auto pBtnClose = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnabled(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addTouchEventListener(CC_CALLBACK_2(OnLineRewardUI::callBackBtnClolse, this));
}

void OnLineRewardUI::refreshData()
{
	std::vector<OnlineGift *> vector_onlineGift = OnlineGiftData::instance()->m_vector_OnlineGift;

	for (unsigned int i = 0; i < vector_onlineGift.size(); i++)
	{
		char s_num_i[10];
		sprintf(s_num_i, "%d", i + 1);

		char s_num_1[10];
		sprintf(s_num_1, "%d", 1);

		char s_num_2[10];
		sprintf(s_num_2, "%d", 2);

		// m_label_timeFix
		std::string str_label_timeFix = "";
		str_label_timeFix.append("Label_");
		str_label_timeFix.append(s_num_i);
		str_label_timeFix.append("_time");

		// m_btn_gift
		std::string str_btn_gift = "";
		str_btn_gift.append("Button_");
		str_btn_gift.append(s_num_i);

		// m_label_btnGift_text
		std::string str_label_btnGift_text = "";
		str_label_btnGift_text.append("Label_");
		str_label_btnGift_text.append(s_num_i);
		str_label_btnGift_text.append("_text");

		// m_image_red
		std::string str_image_red = "";
		str_image_red.append("ImageView_");
		str_image_red.append(s_num_i);
		str_image_red.append("_red");

		// m_image_text
		std::string str_image_text = "";
		str_image_text.append("ImageView_");
		str_image_text.append(s_num_i);
		str_image_text.append("_notime");

		// m_label_timeLeft
		std::string str_label_timeLeft = "";
		str_label_timeLeft.append("Label_");
		str_label_timeLeft.append(s_num_i);
		str_label_timeLeft.append("_notime");

		// m_image_gift1_frame
		std::string str_image_gift1_frame = "";
		str_image_gift1_frame.append("Button_");
		str_image_gift1_frame.append(s_num_i);
		str_image_gift1_frame.append("_gift");
		str_image_gift1_frame.append(s_num_1);

		// m_image_gift1_icon
		std::string str_image_gift1_icon = "";
		str_image_gift1_icon.append("ImageView_");
		str_image_gift1_icon.append(s_num_i);
		str_image_gift1_icon.append("_gift");
		str_image_gift1_icon.append(s_num_1);
		str_image_gift1_icon.append("_icon");

		// m_label_gift1_num
		std::string str_image_gift1_num = "";
		str_image_gift1_num.append("Label_");
		str_image_gift1_num.append(s_num_i);
		str_image_gift1_num.append("_gift");
		str_image_gift1_num.append(s_num_1);
		str_image_gift1_num.append("_num");

		// m_image_gift2_frame
		std::string str_image_gift2_frame = "";
		str_image_gift2_frame.append("Button_");
		str_image_gift2_frame.append(s_num_i);
		str_image_gift2_frame.append("_gift");
		str_image_gift2_frame.append(s_num_2);

		// m_image_gift2_icon
		std::string str_image_gift2_icon = "";
		str_image_gift2_icon.append("ImageView_");
		str_image_gift2_icon.append(s_num_i);
		str_image_gift2_icon.append("_gift");
		str_image_gift2_icon.append(s_num_2);
		str_image_gift2_icon.append("_icon");

		// m_label_gift2_num
		std::string str_image_gift2_num = "";
		str_image_gift2_num.append("Label_");
		str_image_gift2_num.append(s_num_i);
		str_image_gift2_num.append("_gift");
		str_image_gift2_num.append(s_num_2);
		str_image_gift2_num.append("_num");

		// ������ȡ�Ĺ̶�ʱ�
		//vector_onlineGift.at(i)->m_BMFONT_timeFix = (Text*)Helper::seekWidgetByName(s_pPanel, str_label_timeFix.c_str());
		//vector_onlineGift.at(i)->m_BMFONT_timeFix->setVisible(true);

		// ���ȡ��ť
		vector_onlineGift.at(i)->m_btn_gift = (Button*)Helper::seekWidgetByName(s_pPanel, str_btn_gift.c_str());
		vector_onlineGift.at(i)->m_btn_gift->setTouchEnabled(true);
		vector_onlineGift.at(i)->m_btn_gift->setPressedActionEnabled(true);
		vector_onlineGift.at(i)->m_btn_gift->addTouchEventListener(CC_CALLBACK_2(OnLineRewardUI::callBackBtnGetGift, this));
		vector_onlineGift.at(i)->m_btn_gift->setTag(m_array_tagBtnGet[i]);

		// ��ȡ��ť � 򲻿���ȡ ��ť� ��ı�
		vector_onlineGift.at(i)->m_label_btnGift_text = (Text*)Helper::seekWidgetByName(s_pPanel, str_label_btnGift_text.c_str());
		vector_onlineGift.at(i)->m_label_btnGift_text->setVisible(true);

		// ������ʾ�ĵ�ͼ����ɫ��
		vector_onlineGift.at(i)->m_image_red = (ImageView*)Helper::seekWidgetByName(s_pPanel, str_image_red.c_str());
		vector_onlineGift.at(i)->m_image_red->setVisible(false);

		// ʱ��δ��Ŷ �ĵ�ͼ
		vector_onlineGift.at(i)->m_image_text = (ImageView*)Helper::seekWidgetByName(s_pPanel, str_image_text.c_str());
		vector_onlineGift.at(i)->m_image_text->setVisible(false);

		// ʱ��δ��Ŷ ���ı�
		vector_onlineGift.at(i)->m_label_timeLeft = (Text*)Helper::seekWidgetByName(s_pPanel, str_label_timeLeft.c_str());
		vector_onlineGift.at(i)->m_label_timeLeft->setColor(Color3B(255, 246, 0));
		vector_onlineGift.at(i)->m_label_timeLeft->setVisible(false);

		// goods�ĵ�ͼ�iocn 򣬺 ����
		vector_onlineGift.at(i)->m_btn_gift1_frame = (Button*)Helper::seekWidgetByName(s_pPanel, str_image_gift1_frame.c_str());
		vector_onlineGift.at(i)->m_image_gift1_icon = (ImageView*)Helper::seekWidgetByName(s_pPanel, str_image_gift1_icon.c_str());
		vector_onlineGift.at(i)->m_label_gift1_num = (Text*)Helper::seekWidgetByName(s_pPanel, str_image_gift1_num.c_str());

		// goods�ĵ�ͼ�iocn 򣬺 ����
		vector_onlineGift.at(i)->m_btn_gift2_frame = (Button*)Helper::seekWidgetByName(s_pPanel, str_image_gift2_frame.c_str());
		vector_onlineGift.at(i)->m_image_gift2_icon = (ImageView*)Helper::seekWidgetByName(s_pPanel, str_image_gift2_icon.c_str());
		vector_onlineGift.at(i)->m_label_gift2_num = (Text*)Helper::seekWidgetByName(s_pPanel, str_image_gift2_num.c_str());

		vector_onlineGift.at(i)->m_btn_gift1_frame->setVisible(false);
		vector_onlineGift.at(i)->m_image_gift1_icon->setVisible(false);
		vector_onlineGift.at(i)->m_label_gift1_num->setVisible(false);

		vector_onlineGift.at(i)->m_btn_gift2_frame->setVisible(false);
		vector_onlineGift.at(i)->m_image_gift2_icon->setVisible(false);
		vector_onlineGift.at(i)->m_label_gift2_num->setVisible(false);

		vector_onlineGift.at(i)->m_btn_gift1_frame->setScale(0.8f);
		vector_onlineGift.at(i)->m_btn_gift2_frame->setScale(0.8f);

		vector_onlineGift.at(i)->m_btn_gift1_frame->setTouchEnabled(true);
		vector_onlineGift.at(i)->m_btn_gift2_frame->setTouchEnabled(true);
		vector_onlineGift.at(i)->m_btn_gift1_frame->setTag(m_array_tagBtnGift1[i]);
		vector_onlineGift.at(i)->m_btn_gift2_frame->setTag(m_array_tagBtnGift2[i]);

		vector_onlineGift.at(i)->m_btn_gift1_frame->addTouchEventListener(CC_CALLBACK_2(OnLineRewardUI::callBackBtnGiftInfo, this));
		vector_onlineGift.at(i)->m_btn_gift2_frame->addTouchEventListener(CC_CALLBACK_2(OnLineRewardUI::callBackBtnGiftInfo, this));

		vector_onlineGift.at(i)->m_btn_gift1_frame->setPressedActionEnabled(true);
		vector_onlineGift.at(i)->m_btn_gift2_frame->setPressedActionEnabled(true);

		//initGiftFixTime(vector_onlineGift.at(i));
		initBtnStatus(vector_onlineGift.at(i));
		initGoodsInfo(vector_onlineGift.at(i));

	}

	// ��ǰ���ڽ��е UIĳ�ʼ��
	//update(0);

}

void OnLineRewardUI::initBtnStatus( OnlineGift * onlineGift )
{
	/** ������Ʒ״̬ ��1.������ȡ���2.����ȡ���3.�����ȡ������*/
	if (1 == onlineGift->m_nStatus)
	{
		onlineGift->m_btn_gift->setVisible(false);

		onlineGift->m_label_btnGift_text->setVisible(false);

		onlineGift->m_image_text->setVisible(true);
		onlineGift->m_label_timeLeft->setVisible(true);

		const char *str_num = StringDataManager::getString("label_minute");

		int nMillionSecond = onlineGift->m_nTimeAll;
		int nMinuteShow = nMillionSecond / 60000;
		char strTmp[20];
		sprintf(strTmp, "%d", nMinuteShow);

		std::string str_minute = "";
		str_minute.append(strTmp);
		str_minute.append(str_num);
		onlineGift->m_label_timeLeft->setString(str_minute.c_str());

	}
	else if (2 == onlineGift->m_nStatus)
	{
		onlineGift->m_btn_gift->setVisible(true);
		onlineGift->m_btn_gift->setTouchEnabled(true);
		onlineGift->m_btn_gift->loadTextures("res_ui/new_button_2.png", "res_ui/new_button_2.png", "");

		onlineGift->m_label_btnGift_text->setVisible(true);

		const char *str1 = StringDataManager::getString("btn_lingqu");
		onlineGift->m_label_btnGift_text->setString(str1);


		onlineGift->m_image_text->setVisible(false);
		onlineGift->m_label_timeLeft->setVisible(false);
		onlineGift->m_label_timeLeft->setString("");
	}
	else if (3 == onlineGift->m_nStatus)
	{
		onlineGift->m_btn_gift->setVisible(true);
		onlineGift->m_btn_gift->setTouchEnabled(false);
		onlineGift->m_btn_gift->loadTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");

		onlineGift->m_label_btnGift_text->setVisible(true);

		const char *str1 = StringDataManager::getString("btn_yilingqu");
		onlineGift->m_label_btnGift_text->setString(str1);

		onlineGift->m_image_text->setVisible(false);
		onlineGift->m_label_timeLeft->setVisible(false);
		onlineGift->m_label_timeLeft->setString("");
	}
}

int OnLineRewardUI::getGiftNeedTime()
{
	std::vector<COneOnlineGift *> vector_oneOnLineGift = OnlineGiftData::instance()->m_vector_oneOnLineGift;
	for (unsigned int i=0; i < vector_oneOnLineGift.size(); i++)
	{

		int nGiftNum =  vector_oneOnLineGift.at(i)->number();
		int nGiftNeddTime =  vector_oneOnLineGift.at(i)->needtime();
		int nGiftStatus = vector_oneOnLineGift.at(i)->status();	

		// ����ǰ״̬ Ϊ ����ȡ�����ѯ��һ�������Ļ� ��ǰ��� �Ϊ ������ȡ�Ľ��
		if (3 == nGiftStatus)
		{
			continue;
		}
		else
		{
			m_nOnlineGiftTime = nGiftNeddTime;
			return nGiftNeddTime;
		}
	}

	return 0;
}

int OnLineRewardUI::getGiftNum()
{
	std::vector<COneOnlineGift *> vector_oneOnLineGift = OnlineGiftData::instance()->m_vector_oneOnLineGift;
	for (unsigned int i=0; i < vector_oneOnLineGift.size(); i++)
	{

		int nGiftNum =  vector_oneOnLineGift.at(i)->number();
		int nGiftNeddTime =  vector_oneOnLineGift.at(i)->needtime();
		int nGiftStatus = vector_oneOnLineGift.at(i)->status();	

		// ����ǰ״̬ Ϊ ����ȡ�����ѯ��һ�������Ļ� ��ǰ��� �Ϊ ������ȡ�Ľ��
		if (3 == nGiftStatus)
		{
			continue;
		}
		else
		{
			m_nGiftNum = nGiftNum;
			return nGiftNum;
		}
	}

	return -1;
}

void OnLineRewardUI::giftGetSuc( const int nGiftNum, const int nGiftStatus )
{
	std::vector<COneOnlineGift *> vector_oneOnLineGift = OnlineGiftData::instance()->m_vector_oneOnLineGift;
	for (unsigned int i = 0; i < vector_oneOnLineGift.size(); i++)
	{
		// �ǰ�� Ľ�� �ȫ�� ʱ����Ϊ 0��status ��Ϊ ����ȡ��3��
		if (vector_oneOnLineGift.at(i)->number() < vector_oneOnLineGift.at(nGiftNum)->number())
		{
			vector_oneOnLineGift.at(i)->set_needtime(0);
			vector_oneOnLineGift.at(i)->set_status(3);
		}
		else if (vector_oneOnLineGift.at(i)->number()  ==  vector_oneOnLineGift.at(nGiftNum)->number())		// ��� õ�ǰ������ΪnNum�,status���Ϊ ����ȡ��3��
		{
			vector_oneOnLineGift.at(i)->set_needtime(0);
			vector_oneOnLineGift.at(i)->set_status(3);

			if (i < vector_oneOnLineGift.size() - 1)
			{
				// ��ʼnGiftNum + 1 �Ľ����ʱ(����Ϊ num � time)
				m_nGiftNum = this->getGiftNum();
				m_nOnlineGiftTime = this->getGiftNeedTime();


				// ͰnGiftNum + 1ѵ��� �ͬ��� icon
				auto pGuideMap = (GuideMap *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
				if (NULL != pGuideMap)
				{
					if (pGuideMap->getActionLayer())
					{
						auto pOnlineGiftIconWidget = (OnlineGiftIconWidget *)pGuideMap->getActionLayer()->getChildByTag(kTagOnLineGiftIcon);
						if (NULL != pOnlineGiftIconWidget)
						{
							pOnlineGiftIconWidget->initGiftTime(vector_oneOnLineGift.at(i + 1)->needtime());
							pOnlineGiftIconWidget->initGiftNum(vector_oneOnLineGift.at(i + 1)->number());
						}
					}
				}

				// �nGiftNum + 1ѵ��� �ͬ��� layer
				auto pOnlineGiftLayer = (OnlineGiftLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftLayer);
				if (NULL != pOnlineGiftLayer)
				{
					pOnlineGiftLayer->initGiftTime(vector_oneOnLineGift.at(i + 1)->needtime());
					pOnlineGiftLayer->initGiftNum(vector_oneOnLineGift.at(i + 1)->number());
				}
			}
			else
			{
				// ����н�����ȡ��ϣ�(1)�����ͬ���icon (2)icon���ʧ
				auto pGuideMap = (GuideMap *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
				if (NULL != pGuideMap)
				{
					if (pGuideMap->getActionLayer())
					{
						auto pOnlineGiftIconWidget = (OnlineGiftIconWidget *)pGuideMap->getActionLayer()->getChildByTag(kTagOnLineGiftIcon);
						if (NULL != pOnlineGiftIconWidget)
						{
							pOnlineGiftIconWidget->initGiftTime(0);
							pOnlineGiftIconWidget->initGiftNum(8);

							pOnlineGiftIconWidget->allGiftGetIconExit();
						}
					}	
				}

				// �nGiftNum + 1ѵ��� �ͬ��� layer
				auto pOnlineGiftLayer = (OnlineGiftLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftLayer);
				if (NULL != pOnlineGiftLayer)
				{
					pOnlineGiftLayer->initGiftTime(0);
					pOnlineGiftLayer->initGiftNum(8);

					pOnlineGiftLayer->allGiftGetLayerExit();
				}

				m_nGiftNum = 8;
				m_nOnlineGiftTime = 0;

			}
		}
	}

	OnlineGiftData::instance()->initDataFromIntent();

	// ���window UI
	refreshData();
}

void OnLineRewardUI::update( float dt )
{
	m_nOnlineGiftTime -= 1000;
	m_nTimeShow = m_nOnlineGiftTime;
	if (m_nTimeShow <= 0)
	{
		m_nTimeShow = 0;
	}

	refreshTime();
}

void OnLineRewardUI::refreshTime()
{
	std::vector<OnlineGift *> vector_onlineGift = OnlineGiftData::instance()->m_vector_OnlineGift;
	if (m_nGiftNum < vector_onlineGift.size())
	{
		vector_onlineGift.at(m_nGiftNum)->m_image_red->setVisible(true);

		if (0 == m_nTimeShow)
		{
			// ½������ȡ
			vector_onlineGift.at(m_nGiftNum)->m_image_text->setVisible(false);
			vector_onlineGift.at(m_nGiftNum)->m_label_timeLeft->setVisible(false);
			vector_onlineGift.at(m_nGiftNum)->m_label_timeLeft->setString("");

			vector_onlineGift.at(m_nGiftNum)->m_btn_gift->setVisible(true);
			vector_onlineGift.at(m_nGiftNum)->m_btn_gift->setEnabled(true);
			vector_onlineGift.at(m_nGiftNum)->m_btn_gift->loadTextures("res_ui/new_button_2.png", "res_ui/new_button_2.png", "");

			const char *str1 = StringDataManager::getString("btn_lingqu");
			vector_onlineGift.at(m_nGiftNum)->m_label_btnGift_text->setVisible(true);
			vector_onlineGift.at(m_nGiftNum)->m_label_btnGift_text->setString(str1);

			// �������ȡ�Ļ������ ������Ч
			auto tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
			if (NULL == tutorialParticle)
			{
				auto btn_gift = vector_onlineGift.at(m_nGiftNum)->m_btn_gift;

				m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", btn_gift->getContentSize().width + 25, btn_gift->getContentSize().height + 5);
				m_tutorialParticle->setPosition(Vec2(btn_gift->getPosition().x, btn_gift->getPosition().y - 22));
				m_tutorialParticle->setAnchorPoint(Vec2(0.5f, 0.5f));
				m_tutorialParticle->setTag(TAG_TUTORIALPARTICLE);

				this->addChild(m_tutorialParticle);
			}
		}
		else
		{
			// ��ǰ����ʣ��ʱ�
			vector_onlineGift.at(m_nGiftNum)->m_image_text->setVisible(true);
			vector_onlineGift.at(m_nGiftNum)->m_label_timeLeft->setVisible(true);
			vector_onlineGift.at(m_nGiftNum)->m_label_timeLeft->setString(timeFormatToString(m_nTimeShow).c_str());

			vector_onlineGift.at(m_nGiftNum)->m_btn_gift->setVisible(false);

			// �ȥ� ��ư�ť�������ЧЧ�
			auto tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
			if (NULL != tutorialParticle)
			{
				this->removeChild(m_tutorialParticle);
			}
			
		}
	}
	else
	{
		// �Ʒ�Ѿ�ȫ����ȡ

		// ����ȫ��� ĸ�����ʾ��ɫ��ʧ������ȫ��� İ�ťΪ����ȡ
		for (unsigned int i = 0; i < vector_onlineGift.size(); i++)
		{
			vector_onlineGift.at(i)->m_btn_gift->setVisible(true);
			vector_onlineGift.at(i)->m_btn_gift->setTouchEnabled(false);
			vector_onlineGift.at(i)->m_btn_gift->loadTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");

			vector_onlineGift.at(i)->m_label_btnGift_text->setVisible(true);

			const char *str1 = StringDataManager::getString("btn_yilingqu");
			vector_onlineGift.at(i)->m_label_btnGift_text->setString(str1);

			vector_onlineGift.at(i)->m_image_text->setVisible(false);
			vector_onlineGift.at(i)->m_label_timeLeft->setVisible(false);
			vector_onlineGift.at(i)->m_label_timeLeft->setString("");

			// ������ʾ��ɫ��ʧ
			vector_onlineGift.at(i)->m_image_red->setVisible(false);
		}

		// ȥ� ��ư�ť�������ЧЧ�
		auto tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL != tutorialParticle)
		{
			this->removeChild(m_tutorialParticle);
		}

	}
}

std::string OnLineRewardUI::timeFormatToString( int nTime )
{
	int int_m = nTime / 1000 / 60;
	int int_s = nTime / 1000 % 60;

	std::string timeString = "";
	char str_m[10];
	if (int_m < 10)
	{
		timeString.append("0");
	}
	sprintf(str_m,"%d",int_m);
	timeString.append(str_m);
	timeString.append(":");

	char str_s[10];
	if (int_s < 10)
	{
		timeString.append("0");
	}
	sprintf(str_s,"%d",int_s);
	timeString.append(str_s);

	return timeString;
}

void OnLineRewardUI::refreshGiftTime()
{
	m_nTimeShow = this->getGiftNeedTime();
	m_nOnlineGiftTime =  this->getGiftNeedTime();
}

void OnLineRewardUI::refreshGiftNum()
{
	m_nGiftNum =  this->getGiftNum();
}

void OnLineRewardUI::refreshTimeAndNum()
{
	this->refreshGiftTime();
	this->refreshGiftNum();
}

void OnLineRewardUI::initGoodsInfo( OnlineGift* onlineGift )
{
	// ���� goods icon ú frame
	std::vector<GoodsInfo *> vector_goods = onlineGift->m_vector_goods;
	for (unsigned int i = 0; i < vector_goods.size(); i++)
	{
		std::string goods_icon = vector_goods.at(i)->icon();

		std::string goodsIcon_ = "res_ui/props_icon/";
		goodsIcon_.append(goods_icon);
		goodsIcon_.append(".png");

		// ͸�goods�Ʒ��ѡ���ͼ
		int quality_ = vector_goods.at(i)->quality();
		std::string qualityStr_ = getEquipmentQualityByIndex(quality_);

		if (0 == i)
		{
			onlineGift->m_image_gift1_icon->loadTexture(goodsIcon_.c_str());
			onlineGift->m_image_gift1_icon->setVisible(true);
			//onlineGift->m_image_gift1_frame->loadTexture(qualityStr_.c_str());
			onlineGift->m_btn_gift1_frame->loadTextures(qualityStr_.c_str(), qualityStr_.c_str(), qualityStr_.c_str());
			onlineGift->m_btn_gift1_frame->setVisible(true);
		}
		else if (1 == i)
		{
			onlineGift->m_image_gift2_icon->loadTexture(goodsIcon_.c_str());
			onlineGift->m_image_gift2_icon->setVisible(true);
			//onlineGift->m_image_gift2_frame->loadTexture(qualityStr_.c_str());
			onlineGift->m_btn_gift2_frame->loadTextures(qualityStr_.c_str(), qualityStr_.c_str(), qualityStr_.c_str());
			onlineGift->m_btn_gift2_frame->setVisible(true);
		}
	}

	// ��� goods ����
	std::vector<int > vector_goodsNum = onlineGift->m_vector_goodsNum;
	for (unsigned int j = 0; j < vector_goodsNum.size(); j++)
	{
		int goods_num = vector_goodsNum.at(j);

		char s[10];
		sprintf(s, "%d", goods_num);

		if (0 == j)
		{
			onlineGift->m_label_gift1_num->setString(s);
			onlineGift->m_label_gift1_num->setVisible(true);
		}
		else if (1 == j)
		{
			onlineGift->m_label_gift2_num->setString(s);
			onlineGift->m_label_gift2_num->setVisible(true);
		}
	}
}

void OnLineRewardUI::callBackBtnGiftInfo(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto pImageView = (Button *)pSender;
		Size size = Director::getInstance()->getVisibleSize();

		int nGift = 0;
		int nGiftNum = 0;

		// ͨ�tag��ж��ǵڼ������
		if (TAG_BTN_GIFT_1_1 == pImageView->getTag())
		{
			nGift = 0;
			nGiftNum = 0;
		}
		else if (TAG_BTN_GIFT_1_2 == pImageView->getTag())
		{
			nGift = 0;
			nGiftNum = 1;
		}
		else if (TAG_BTN_GIFT_2_1 == pImageView->getTag())
		{
			nGift = 1;
			nGiftNum = 0;
		}
		else if (TAG_BTN_GIFT_2_2 == pImageView->getTag())
		{
			nGift = 1;
			nGiftNum = 1;
		}
		else if (TAG_BTN_GIFT_3_1 == pImageView->getTag())
		{
			nGift = 2;
			nGiftNum = 0;
		}
		else if (TAG_BTN_GIFT_3_2 == pImageView->getTag())
		{
			nGift = 2;
			nGiftNum = 1;
		}
		else if (TAG_BTN_GIFT_4_1 == pImageView->getTag())
		{
			nGift = 3;
			nGiftNum = 0;
		}
		else if (TAG_BTN_GIFT_4_2 == pImageView->getTag())
		{
			nGift = 3;
			nGiftNum = 1;
		}
		else if (TAG_BTN_GIFT_5_1 == pImageView->getTag())
		{
			nGift = 4;
			nGiftNum = 0;
		}
		else if (TAG_BTN_GIFT_5_2 == pImageView->getTag())
		{
			nGift = 4;
			nGiftNum = 1;
		}
		else if (TAG_BTN_GIFT_6_1 == pImageView->getTag())
		{
			nGift = 5;
			nGiftNum = 0;
		}
		else if (TAG_BTN_GIFT_6_2 == pImageView->getTag())
		{
			nGift = 5;
			nGiftNum = 1;
		}
		else if (TAG_BTN_GIFT_7_1 == pImageView->getTag())
		{
			nGift = 6;
			nGiftNum = 0;
		}
		else if (TAG_BTN_GIFT_7_2 == pImageView->getTag())
		{
			nGift = 6;
			nGiftNum = 1;
		}
		else if (TAG_BTN_GIFT_8_1 == pImageView->getTag())
		{
			nGift = 7;
			nGiftNum = 0;
		}
		else if (TAG_BTN_GIFT_8_2 == pImageView->getTag())
		{
			nGift = 7;
			nGiftNum = 1;
		}

		auto pGoodsInfo = OnlineGiftData::instance()->getGoosInfo(nGift, nGiftNum);
		auto goodsItemInfoBase = GoodsItemInfoBase::create(pGoodsInfo, GameView::getInstance()->EquipListItem, 0);
		goodsItemInfoBase->setIgnoreAnchorPointForPosition(false);
		goodsItemInfoBase->setAnchorPoint(Vec2(0.5f, 0.5f));
		//goodsItemInfoBase->setPosition(Vec2(size.width / 2, size.height / 2));

		GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

std::string OnLineRewardUI::getEquipmentQualityByIndex( int quality )
{
	std::string frameColorPath = "res_ui/";
	switch(quality)
	{
	case 1:
		{
			frameColorPath.append("sdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("sdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("sdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("sdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("sdi_orange");
		}break;
	}
	frameColorPath.append(".png");
	return frameColorPath;
}

void OnLineRewardUI::initGiftFixTime( OnlineGift * onlineGift )
{
	int nMillionSecond = onlineGift->m_nTimeAll;

	int nMinuteShow = nMillionSecond / 60000;
	char str[10];
	sprintf(str, "%d", nMinuteShow);
	onlineGift->m_BMFONT_timeFix->setString(str);
}



