#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class RichTextInputBox;

enum
{
	PRIVATECHAT=0,
	CHECK,
	TEAM,
	FRIEND,
	MAIL,
	MOVE
};
class AddFunctionMenu:public UIScene
{
public:
	AddFunctionMenu(void);
	~AddFunctionMenu(void);

	static AddFunctionMenu * create(long long roleId);
	bool init(long long roleId);
	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void callBackFuncList(Ref * obj);
private:
	long long m_roleId;
	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};
};

