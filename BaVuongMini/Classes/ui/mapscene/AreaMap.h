
#ifndef _MAPSCENE_AREAMAP_H_
#define _MAPSCENE_AREAMAP_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../../legend_engine/LegendLevel.h"
#include "../../legend_engine/CCLegendTiledMap.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UIScene;
class UITab;
class DoorInfo;

class AreaMap :public Layer
{
public:
	AreaMap();
	~AreaMap();
	static AreaMap* create(const char* levelName);
	bool init(const char* levelName);

	virtual void update(float dt);

	 void onEnter();
	 void onExit();

	// default implements are used to call script callback if exist
	 bool onTouchBegan(Touch *pTouch, Event *pEvent);
	 void onTouchMoved(Touch *pTouch, Event *pEvent);
	 void onTouchEnded(Touch *pTouch, Event *pEvent);
	 void onTouchCancelled(Touch *pTouch, Event *pEvent);

	 void menuTest(Ref * pSender);

	 Vec2 m_tBeginPos;
	 Sprite * backgroundSprite;

	Size getMapSize();

	// ����Ϸ������ת��Ϊ��ͼ��
	Vec2 convertToMapSpace(const Vec2& gameWorldPoint);
	// 꽫cocos2d���ת��Ϊ��Ϸ�����
	Vec2 convertToGameWorldSpace(const Vec2& cocos2DPoint);

	Vec2 getMyPlayerPosition();
	Vec2 getMapCenterPosition();

private:
	void addDoor(DoorInfo* info, const char* mapId, LegendLevel* pLevelData);
	void loadTeammateInfo(std::string mapId);
	void updateNpcStateIcon(int state);

private:
	Size m_mapSize;
	Size m_mapSizeAtWorldSpace;   // the size at the world space
	float m_mapScale;
	Sprite * m_npcMissionIndicate;
};
#endif;
