#include "ChannelUI.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "AppMacros.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"

ChannelUI::ChannelUI()
{
}


ChannelUI::~ChannelUI()
{
}


ChannelUI * ChannelUI::create()
{
	auto channelUI = new ChannelUI();
	if (channelUI && channelUI->init())
	{
		channelUI->autorelease();
		return channelUI;
	}
	CC_SAFE_DELETE(channelUI);
	return NULL;
}

bool ChannelUI::init()
{
	if (UIScene::init())
	{
		auto visibleSize = Director::getInstance()->getVisibleSize();
		auto origin = Director::getInstance()->getVisibleOrigin();

		auto bg = ImageView::create();
		bg->loadTexture("gamescene_state/zhujiemian3/renwuduiwu/renwu_di.png");
		bg->setAnchorPoint(Vec2::ZERO);
		bg->setPosition(Vec2::ZERO);
		bg->setScale9Enabled(true);
		bg->setCapInsets(Rect(12,12,1,1));
		bg->setContentSize(Size(217,240));
		m_pLayer->addChild(bg);

		auto btn_close = Button::create();
		btn_close->loadTextures("res_ui/close.png","res_ui/close.png","");
		btn_close->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_close->setPosition(Vec2(212,232));
		btn_close->setTouchEnabled(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(ChannelUI::CloseEvent, this));
		m_pLayer->addChild(btn_close);

		m_tableView = TableView::create(this,Size(217,216));
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0,0));
		m_tableView->setPosition(Vec2(0,12));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		this->addChild(m_tableView);

		this->setIgnoreAnchorPointForPosition(false);
		this->setAnchorPoint(Vec2(0,0));
		this->setPosition(Vec2(0,0));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(217,240));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(ChannelUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(ChannelUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(ChannelUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(ChannelUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}



void ChannelUI::onEnter()
{
	UIScene::onEnter();
}
void ChannelUI::onExit()
{
	UIScene::onExit();
}

bool ChannelUI::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return this->resignFirstResponder(pTouch,this,false);
}
void ChannelUI::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void ChannelUI::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void ChannelUI::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}


Size ChannelUI::tableCellSizeForIndex(TableView *table, unsigned int idx)
{
	return Size(210,49);
}

TableViewCell* ChannelUI::tableCellAtIndex(TableView *table, ssize_t  idx)
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	auto u_layer = Layer::create();
	cell->addChild(u_layer);
	//u_layer->setSwallowsTouches(false);

	auto btn_channel = Button::create();
	btn_channel->loadTextures("res_ui/new_button_5.png","res_ui/new_button_5.png","");
	btn_channel->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_channel->setPosition(Vec2(110,25));
	btn_channel->setTouchEnabled(true);
	btn_channel->setPressedActionEnabled(true);
	btn_channel->addTouchEventListener(CC_CALLBACK_2(ChannelUI::ChannelEvent, this));
	btn_channel->setTag(idx);
	u_layer->addChild(btn_channel);

	if (idx == GameView::getInstance()->getMapInfo()->channel())
	{
		btn_channel->loadTextures("res_ui/new_button_6.png","res_ui/new_button_6.png","");
	}
	else
	{
		btn_channel->loadTextures("res_ui/new_button_5.png","res_ui/new_button_5.png","");
	}

	
	std::string str_channelid;
	char s_id[5];
	sprintf(s_id,"%d",idx+1);
	str_channelid.append(s_id);
	str_channelid.append(StringDataManager::getString("GuideMap_Channel_Name"));
	auto l_channelId = Label::createWithTTF(str_channelid.c_str(), APP_FONT_NAME, 20);
	l_channelId->setAnchorPoint(Vec2(0.5f,0.5f));
	l_channelId->setPosition(Vec2(0.f,0.f));
	btn_channel->addChild(l_channelId);

	return cell;

}

ssize_t ChannelUI::numberOfCellsInTableView(TableView *table)
{
	return GameView::getInstance()->getMapInfo()->channelsize();
}

void ChannelUI::tableCellTouched(TableView* table, TableViewCell* cell)
{
}

void ChannelUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void ChannelUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

void ChannelUI::ChannelEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn_channel = dynamic_cast<Button*>(pSender);
		if (!btn_channel)
			return;

		//���PK�ʱ��������ͣ�ֻ����·
		if (GameView::getInstance()->myplayer->isPKStatus())
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("PKMode_canNoChangeChannel"));
		}
		else
		{
			int id = btn_channel->getTag();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1110, (void*)id);

			this->closeAnim();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void ChannelUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}








