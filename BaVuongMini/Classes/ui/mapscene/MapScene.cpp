#include "MapScene.h"
#include "GameView.h"
#include "AppMacros.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/element/DoorInfo.h"
#include "../../messageclient/element/MapInfo.h"
#include "../../messageclient/protobuf/PlayerMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CMonsterBaseInfo.h"
#include "../../GameUserDefault.h"
#include "../../messageclient/element/CTeamMember.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "ChannelUI.h"
#include "AddFunctionMenu.h"
#include "NearbyPlayersDetail.h"
#include "../../gamescene_state/role/MyPlayerSimpleAI.h"
#include "MapAction.h"
#include "../../gamescene_state/role/BaseFighterConstant.h"

#define MAPSCENE_MAP_LAYER_TAG 800
#define ScrollView_MAP_TAG 900
#define AREA_MAP_TAG 901
#define MAPNAME_FRAME_TAG 911
#define MAPNAME_LABEL_TAG 912
#define MAP_NUM 38
#define MYPLAYER_ICON_TAG 100
#define BTN_MAP_TAG 70
#define LAYER_TAG 101
#define COUNTRYNAME_TAG 102
#define COUNTRY_INK_TAG 103
#define CHANGELINE_TAG 104

enum MapUpdateState{
	state_idle = 1,
	state_moveto_npc = 2,
	state_moveto_monster = 3,
};

MapScene::MapScene()
:m_iState(state_idle)
{
	m_mapScrollViewDraged = false;
	pLevelData = NULL;
	m_iMapFindInfo = 0;
	scheduleUpdate();
}

MapScene::~MapScene()
{
	CC_SAFE_DELETE(pLevelData);
	image_countryNameFrame->removeAllChildrenWithCleanup(true);
	auto countryMap_dragPanel = (cocos2d::extension::ScrollView*)Helper::seekWidgetByName(panelCountry,"DragPanel_469");
	auto icon = (ImageView*)countryMap_dragPanel->getChildByTag(MYPLAYER_ICON_TAG);
	if(icon != NULL)
	{
		icon->removeFromParentAndCleanup(true);
	}
}

MapScene* MapScene::create()
{
	auto mapScene = new MapScene();
	if (mapScene && mapScene->init())
	{
		mapScene->autorelease();
		return mapScene;
	}
	CC_SAFE_DELETE(mapScene);
	return NULL;
}


 void MapScene::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

 void MapScene::onExit()
{
	UIScene::onExit();
}

bool MapScene::init()
{
	if (UIScene::init())
	{
		auto winSize = Director::getInstance()->getVisibleSize();

		m_iMapCountry = GameView::getInstance()->getMapInfo()->country();
		if(!m_iMapCountry)
		{
			m_iMapCountry = UserDefault::getInstance()->getIntegerForKey(CURRENT_COUNTRY,1);
		}

		m_currentMapId = GameView::getInstance()->getMapInfo()->mapid();
		
		///change by liuzhenxing
		auto size= Director::getInstance()->getVisibleSize();
		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(size);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		if(LoadSceneLayer::mapPanel->getParent() != NULL)
		{
			LoadSceneLayer::mapPanel->removeFromParentAndCleanup(false);
		}

		auto map_Panel=LoadSceneLayer::mapPanel;
		map_Panel->setAnchorPoint(Vec2(0.5f,0.5f));
		map_Panel->setPosition(Vec2(size.width/2,size.height/2));
		m_pLayer->addChild(map_Panel);
		
		auto layer= Layer::create();
		layer->setIgnoreAnchorPointForPosition(false);
		layer->setAnchorPoint(Vec2(0.5f,0.5f));
		layer->setPosition(Vec2(winSize.width/2,winSize.height/2));
		layer->setContentSize(Size(UI_DESIGN_RESOLUTION_WIDTH,UI_DESIGN_RESOLUTION_HEIGHT));
		layer->setTag(LAYER_TAG);
		addChild(layer, 1);

		// MyPlayer icon
		auto myplayer = GameView::getInstance()->myplayer;
		std::string iconFilePath = BasePlayer::getSmallHeadPathByProfession(myplayer->getProfession());
		myIcon = Sprite::create(iconFilePath.c_str());
		myIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
		//myIcon->setTag(MYPLAYER_ICON_TAG);
		//myIcon->setVisible(false);
		myIcon->setOpacity(0);
		layer->addChild(myIcon, 5);

		// blink the icon
		auto repeat = RepeatForever::create( CCBlink::create(1.0f, 2) );
		myIcon->runAction( repeat);

		infoTabview =TableView::create(this,Size(200,355));
		infoTabview->setDirection(TableView::Direction::VERTICAL);
		infoTabview->setAnchorPoint(Vec2(0,0));
		infoTabview->setPosition(Vec2(550,45));
		infoTabview->setDelegate(this);
		infoTabview->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		layer->addChild(infoTabview);
		
		for(int i = 0; i < 3; i++)
		{
			auto ink = ImageView::create();
			ink->loadTexture("res_ui/mo_5.png");
			ink->setAnchorPoint(Vec2(0.5f, 0.5f));
			ink->setPosition(Vec2(103+i*65, 403));
			ink->setScale(1.28f);
			ink->setOpacity(200);
			layer->addChild(ink);
		}
		/*
		char* current = (char*)StringDataManager::getString("map_current");
		char* country = (char*)StringDataManager::getString("map_country");
		char* world = (char*)StringDataManager::getString("map_world");
		char *labelFont[]={current, country, world};*/
		char *image_label[]={"res_ui/map/dangqian.png","res_ui/map/guojia.png","res_ui/map/shijie.png"};
		equipTab=UITab::createWithImage(3,"res_ui/tab1_off.png","res_ui/tab1_on.png","",image_label,HORIZONTAL,21);
		equipTab->setAnchorPoint(Vec2(0.5f,0.5f));
		equipTab->setPressedActionEnabled(true);
		equipTab->setPosition(Vec2(79,380));
		equipTab->setHighLightImage("res_ui/tab1_on.png");
		equipTab->setVisible(true);
		equipTab->setScale(0.8f);
		m_iMapType = 0;
		equipTab->setDefaultPanelByIndex(m_iMapType);
		equipTab->addIndexChangedEvent(this,coco_indexchangedselector(MapScene::callBackScene));
		layer->addChild(equipTab);

		char* npc = " N\n P\n C";
		char* monster = (char*)StringDataManager::getString("map_monster");
		char* door = (char*)StringDataManager::getString("map_door");
		char* player = (char*)StringDataManager::getString("map_player");
		char *label[]={player,door,monster,npc};
		//char *image_[]={"res_ui/map/npc.png","res_ui/map/guaiwu.png","res_ui/map/chuansongkou.png","res_ui/map/fujinwanjia.png"};
		//char *image_[]={"res_ui/map/fujinwanjia.png","res_ui/map/chuansongkou.png","res_ui/map/guaiwu.png","res_ui/map/npc.png"};
		findTab=UITab::createWithText(4,"res_ui/tab_b_off.png","res_ui/tab_b.png","",label,VERTICAL,-5, 18);
		//findTab=UITab::createWithImage(4,"res_ui/tab_b_off.png","res_ui/tab_b.png","",image_,VERTICAL,-5);
		findTab->setAnchorPoint(Vec2(0.5f,0.5f));
		findTab->setPressedActionEnabled(true);
		findTab->setPosition(Vec2(759,410));
		findTab->setHighLightImage("res_ui/tab_b.png");
		findTab->setHightLightLabelColor(Color3B(47,93,13));
		findTab->setNormalLabelColor(Color3B(255,255,255));
		findTab->setDefaultPanelByIndex(m_iMapFindInfo);
		findTab->addIndexChangedEvent(this,coco_indexchangedselector(MapScene::callBackPerson));
		layer->addChild(findTab);
		findTab->setVisible(true);

		//换线按钮
		auto btn_changeLine = Button::create();
		btn_changeLine->loadTextures("res_ui/new_button_12.png","res_ui/new_button_12.png","");
		btn_changeLine->setTouchEnabled(true);
		btn_changeLine->setPressedActionEnabled(true);
		btn_changeLine->addTouchEventListener(CC_CALLBACK_2(MapScene::ChangeLineEvent, this));
		btn_changeLine->setScale9Enabled(true);
		btn_changeLine->setContentSize(Size(85,36));
		btn_changeLine->setCapInsets(Rect(32,18,1,1));
		btn_changeLine->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_changeLine->setPosition(Vec2(475,405));
		btn_changeLine->setTag(CHANGELINE_TAG);
		layer->addChild(btn_changeLine);

		auto l_changeLine = Label::createWithTTF(StringDataManager::getString("GuideMap_Channel_ChangeLine"), APP_FONT_NAME, 18);
		l_changeLine->setAnchorPoint(Vec2(0.5f,0.5f));
		l_changeLine->setPosition(Vec2(0.f,0.f));
		btn_changeLine->addChild(l_changeLine);

		panelWorld=(Layout *)Helper::seekWidgetByName(map_Panel,"Panel_world");
		panelCountry=(Layout *)Helper::seekWidgetByName(map_Panel,"Panel_country");
		panelcurrent=(Layout *)Helper::seekWidgetByName(map_Panel,"Panel_current");

		auto pScrollView = (cocos2d::extension::ScrollView *)Helper::seekWidgetByName(panelcurrent,"DragPanel_map");
		pScrollView->setVisible(false);

		const char * secondStr = StringDataManager::getString("UIName_di");
		const char * thirdStr = StringDataManager::getString("UIName_tu");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		layer->addChild(atmature);

		auto btn_yizhou=(Button *)Helper::seekWidgetByName(panelWorld,"Button_wei");
		btn_yizhou->setTouchEnabled(true);
		btn_yizhou->setTag(1);
		btn_yizhou->setPressedActionEnabled(true);
		btn_yizhou->addTouchEventListener(CC_CALLBACK_2(MapScene::showCountryMap, this));

		auto btn_jinzhou=(Button *)Helper::seekWidgetByName(panelWorld,"Button_wu");
		btn_jinzhou->setTouchEnabled(true);
		btn_jinzhou->setTag(3);
		btn_jinzhou->setPressedActionEnabled(true);
		btn_jinzhou->addTouchEventListener(CC_CALLBACK_2(MapScene::showCountryMap, this));

		auto btn_yangzhou=(Button *)Helper::seekWidgetByName(panelWorld,"Button_shu");
		btn_yangzhou->setTouchEnabled(true);
		btn_yangzhou->setTag(2);
		btn_yangzhou->setPressedActionEnabled(true);
		btn_yangzhou->addTouchEventListener(CC_CALLBACK_2(MapScene::showCountryMap, this));

		panelcurrent->setVisible(true);
		panelCountry->setVisible(false);
		panelWorld->setVisible(false);

		char* mapId[MAP_NUM] = {"xl", "cac", "hrd1", "jzxg", "xldy2", "ty2", "ymz2", "ss1", "dh1", "dh2", "wg", "bgz2", "bgz1", "yhd2", "xy", "cbp1", "dyq2", "wl1", "hyl1", "hj1", "ymz1", "wyg", "slwc", "sc", "hl", "ysy", "dk1", "bmzc", "wcmx", "ty1", "ss2", "ltx", "fyt1", "xsc", "jyc", "qhh", "jzgy1", "hrd2"};
		mapIdList.assign(mapId, mapId+MAP_NUM);
		initMapButton();

		auto btn_exit=(Button *)Helper::seekWidgetByName(map_Panel,"Button_close");
		btn_exit->setTouchEnabled(true);
		btn_exit->setPressedActionEnabled(true);
		btn_exit->addTouchEventListener(CC_CALLBACK_2(MapScene::callBackExit, this));

		image_countryNameFrame = (ImageView *)Helper::seekWidgetByName(panelCountry,"ImageView_country_name");
		image_countryNameFrame->loadTexture("res_ui/map/world_map/country.png");
		
		auto countryMap_dragPanel = (cocos2d::extension::ScrollView*)Helper::seekWidgetByName(panelCountry,"DragPanel_469");
		//countryMap_dragPanel->jumpToBottomLeft();
		
		// MyPlayer icon
		auto myIconImage = ImageView::create();
		myIconImage->loadTexture(iconFilePath.c_str());
		myIconImage->setAnchorPoint(Vec2(0.5f, 0.5f));
		myIconImage->setTag(MYPLAYER_ICON_TAG);
		//myIcon->setVisible(false);
		myIconImage->setVisible(false);
		myIconImage->setLocalZOrder(100);
		countryMap_dragPanel->addChild(myIconImage);

		// blink the icon
		auto repeatAction = RepeatForever::create( CCBlink::create(1.0f, 2) );
		myIconImage->runAction(repeatAction);

		auto image_countryName = ImageView::create();
		char* countryName[] = {"res_ui/map/world_map/weiguo.png", "res_ui/map/world_map/shuguo.png", "res_ui/map/world_map/wuguo.png"};
		image_countryName->loadTexture(countryName[m_iMapCountry-1]);
		image_countryName->setVisible(true);
		image_countryName->setTag(COUNTRYNAME_TAG);
		image_countryNameFrame->addChild(image_countryName);

		auto image_ink = ImageView::create();
		char* ink[] = {"res_ui/map/world_map/blue.png", "res_ui/map/world_map/green.png", "res_ui/map/world_map/red.png"};
		image_ink->loadTexture(ink[m_iMapCountry-1]);
		image_ink->setVisible(true);
		image_ink->setTag(COUNTRY_INK_TAG);
		image_ink->setLocalZOrder(-1);
		image_countryNameFrame->addChild(image_ink);

		initAreaMapLayer();

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winSize);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(MapScene::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(MapScene::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(MapScene::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(MapScene::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void MapScene::callBackFunc()
{
	auto countryMap_dragPanel = (ui::ScrollView*)Helper::seekWidgetByName(panelCountry,"DragPanel_469");
	int x = MAX(countryCenterPos.x - countryMap_dragPanel->getContentSize().width/2, 0);
	x = MIN(x, countryMap_dragPanel->getInnerContainerSize().width - countryMap_dragPanel->getContentSize().width);
	int y = countryMap_dragPanel->getInnerContainerSize().height - countryCenterPos.y - countryMap_dragPanel->getContentSize().height/2;
	y = MIN(y, countryMap_dragPanel->getInnerContainerSize().height - countryMap_dragPanel->getContentSize().height);
	y = MAX(y, 0);
	float fWidth = (float)(x)/(countryMap_dragPanel->getInnerContainerSize().width - countryMap_dragPanel->getContentSize().width);
	float fHeight = (float)(y)/(countryMap_dragPanel->getInnerContainerSize().height - countryMap_dragPanel->getContentSize().height);
	//countryMap_dragPanel->scrollToPercentBothDirection(Vec2(0, 50), 0.001f, false);
	//countryMap_dragPanel->jumpToTopRight();
	//countryMap_dragPanel->jumpToBottomLeft();
	countryMap_dragPanel->jumpToPercentBothDirection(Vec2(fWidth*100,fHeight*100));
}

void MapScene::initMapButton()
{
	for(int i = 0; i < MAP_NUM; i++)
	{
		string pName("Button_");
		pName.append(mapIdList.at(i));
		auto btn =(Button *)Helper::seekWidgetByName(panelCountry,pName.c_str());
		auto image = (ImageView *)btn->getChildByTag(BTN_MAP_TAG);

		pName = pName.substr(7, pName.size());
		if((strcmp(mapIdList.at(i), "xl")!=0)&&(strcmp(mapIdList.at(i), "cac")!=0))
		{
			char pTemp[10];
			sprintf(pTemp, "_%d.level", m_iMapCountry);
			pName.append(pTemp);
		}
		else
		{
			pName.append(".level");
		}
		if(GameView::getInstance()->myplayer->isMapReached(m_iMapCountry, pName.c_str()))
		{
			btn->setVisible(true);
			btn->setTouchEnabled(true);
			btn->setPressedActionEnabled(true);
			//btn->setColor(Color3B(255, 255, 255));
			if(!memcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(), pName.c_str(), GameView::getInstance()->getMapInfo()->mapid().size()))
			{
				image->setVisible(true);
				/*UIDragPanel* countryMap_dragPanel = (UIDragPanel*)Helper::seekWidgetByName(panelCountry,"DragPanel_469");
				float fWidth = (float)btn->getPosition().x/countryMap_dragPanel->getInnerContainerSize().width;
				float fHeight = (float)btn->getPosition().y/countryMap_dragPanel->getInnerContainerSize().height;
				countryMap_dragPanel->scrollToPercentBothDirection(Vec2(fWidth*100, fHeight*100), 0.001f, false);
				*/
				countryCenterPos = btn->getPosition();
				Action* action = Sequence::create(
										DelayTime::create(0.1f),
										CallFunc::create(CC_CALLBACK_0(MapScene::callBackFunc,this)),
										NULL);
				this->runAction(action);
				//countryMap_dragPanel->jumpToBottomRight();
			}
			else
			{
				image->setVisible(false);
			}
			//btn_child->setTouchEnabled(true);
			//btn_child->setPressedActionEnabled(true);
			/*
			if((!strcmp(mapIdList.at(i), "xl"))
				||(!strcmp(mapIdList.at(i), "cac"))
				||(!strcmp(mapIdList.at(i), "xsc"))
				||(!strcmp(mapIdList.at(i), "ty1"))
				||(!strcmp(mapIdList.at(i), "jyc"))
				||(!strcmp(mapIdList.at(i), "xldy2")))
			{
				btn_child->loadTextures("res_ui/map/country_map/gjdt_an01.png", "res_ui/map/country_map/gjdt_an01.png", "");
			}
			else
			{
				btn_child->loadTextures("res_ui/map/country_map/gjdt_an02.png", "res_ui/map/country_map/gjdt_an02.png", "");
			}
			btn_child->addTouchEventListener(CC_CALLBACK_2(MapScene::showCurrMap_child));*/
			btn->addTouchEventListener(CC_CALLBACK_2(MapScene::showCurrMap, this));
		}
		else
		{
			//btn->setTouchEnabled(true);
			//btn->setPressedActionEnabled(false);
			//btn->setColor(Color3B(66, 66, 66));
			btn->setVisible(false);

			//btn_child->setTouchEnabled(true);
			//btn_child->setPressedActionEnabled(false);
			/*
			if((!strcmp(mapIdList.at(i), "xl"))
				||(!strcmp(mapIdList.at(i), "cac"))
				||(!strcmp(mapIdList.at(i), "xsc"))
				||(!strcmp(mapIdList.at(i), "ty1"))
				||(!strcmp(mapIdList.at(i), "jyc"))
				||(!strcmp(mapIdList.at(i), "xldy2")))
			{
				btn_child->loadTextures("res_ui/map/country_map/gjdt_an1.png", "res_ui/map/country_map/gjdt_an1.png", "");
			}
			else
			{
				btn_child->loadTextures("res_ui/map/country_map/gjdt_an2.png", "res_ui/map/country_map/gjdt_an2.png", "");
			}
			btn_child->addTouchEventListener(CC_CALLBACK_2(MapScene::callbackClickedNotReached));*/
			//btn->addTouchEventListener(CC_CALLBACK_2(MapScene::callbackClickedNotReached));
		}
	}
}

void MapScene::initAreaMapLayer()
{
	auto winSize = Director::getInstance()->getVisibleSize();

	// area map layer
	auto pMapLayer= Layer::create();
	pMapLayer->setIgnoreAnchorPointForPosition(false);
	pMapLayer->setAnchorPoint(Vec2(0.5f,0.5f));
	pMapLayer->setContentSize(Size(UI_DESIGN_RESOLUTION_WIDTH, UI_DESIGN_RESOLUTION_HEIGHT));
	pMapLayer->setPosition(Vec2(winSize.width/2,winSize.height/2));
	pMapLayer->setTag(MAPSCENE_MAP_LAYER_TAG);
	addChild(pMapLayer);

	//new ScrollView 
	auto scrollView_areaMap = cocos2d::extension::ScrollView::create(Size(460, 395));
	//scrollView_areaMap->setContentSize(Size(512,512));
	//scrollView_areaMap->setViewSize(Size(600, 350));
	scrollView_areaMap->setAnchorPoint(Vec2::ZERO);
	scrollView_areaMap->setContentOffset(Vec2::ZERO);
	scrollView_areaMap->setTouchEnabled(true);
	scrollView_areaMap->setDirection(cocos2d::extension::ScrollView::Direction::BOTH);
	scrollView_areaMap->setPosition(Vec2(70,45));
	//can not use setContainer,will cause error 
	//scrollView_areaMap->setContainer(areaMap);
	scrollView_areaMap->setDelegate(this);
	scrollView_areaMap->setBounceable(false);
	scrollView_areaMap->setClippingToBounds(true);
	scrollView_areaMap->setTag(ScrollView_MAP_TAG);
	pMapLayer->addChild(scrollView_areaMap, 0);

	auto mengban = cocos2d::extension::Scale9Sprite::create("res_ui/mapk.png");
	mengban->setAnchorPoint(Vec2(0.5f,0.5f));
	mengban->setCapInsets(Rect(11,8,1,1));
	mengban->setPreferredSize(Size(147,36));
	mengban->setPosition(Vec2(650, 432));
	mengban->setTag(MAPNAME_FRAME_TAG);
	pMapLayer->addChild(mengban, 1);

	auto mapName = Label::createWithTTF("", APP_FONT_NAME, 17);
	mapName->setColor(Color3B(255,216,61));  //(247,151,30) // (218,105,6)
	//int countryId = GameView::getInstance()->getMapInfo()->country();
	
	mapName->setAnchorPoint(Vec2(0.5f,0.5f));
	mapName->setPosition(Vec2(74,18));
	mapName->setTag(MAPNAME_LABEL_TAG);
	mengban->addChild(mapName);

	auto pFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV5_dikuang_miaobian1.png");
	pFrame->setAnchorPoint(Vec2(0,0));
	pFrame->setPosition(Vec2(63,39));
	pFrame->setPreferredSize(Size(472,405));
	pFrame->setCapInsets(Rect(32,32,1,1));
	pMapLayer->addChild(pFrame, 2);
	
	m_mapId = GameView::getInstance()->getMapInfo()->mapid();
	reloadAreaMap(m_mapId);
}

void MapScene::reloadAreaMap(std::string mapIdName)
{
	initMapInfo(mapIdName);
	infoTabview->reloadData();

	auto pMapLayer = (Layer*)this->getChildByTag(MAPSCENE_MAP_LAYER_TAG);
	auto pScrollView = (cocos2d::extension::ScrollView*)pMapLayer->getChildByTag(ScrollView_MAP_TAG);
	auto pFrame = (cocos2d::extension::Scale9Sprite *)pMapLayer->getChildByTag(MAPNAME_FRAME_TAG);
	auto pMapName = (Label *)pFrame->getChildByTag(MAPNAME_LABEL_TAG);
	auto pContainer = pScrollView->getContainer();
	auto pAreaMap = (AreaMap*)pContainer->getChildByTag(AREA_MAP_TAG);
	if(pAreaMap != NULL)
		pAreaMap->removeFromParent();

	pAreaMap = AreaMap::create(mapIdName.c_str());
	pAreaMap->setPosition(Vec2(0,0));
	pAreaMap->setTag(AREA_MAP_TAG);
	pScrollView->addChild(pAreaMap);

	// adjust the scroll view's contentsize
	pScrollView->setContentSize(pAreaMap->getMapSize());

	Vec2 centerPoint;
	std::string curGameMap = GameView::getInstance()->getMapInfo()->mapid();
	if(curGameMap == mapIdName)   // MyPlayer is in this map
	{
		// 将地图定位到以主角所在位置为中心
		centerPoint = pAreaMap->getMyPlayerPosition();
	}
	else
	{
		// 定位到地图的中心
		centerPoint = pAreaMap->getMapCenterPosition();
	}
	centerPoint = centerPoint * -1.f;
	centerPoint.x += pScrollView->getViewSize().width/2;
	centerPoint.y += pScrollView->getViewSize().height/2;
    const Vec2 minOffset = pScrollView->minContainerOffset();
    const Vec2 maxOffset = pScrollView->maxContainerOffset();
	Vec2 offset;
    offset.x = MAX(minOffset.x, MIN(maxOffset.x, centerPoint.x));
    offset.y = MAX(minOffset.y, MIN(maxOffset.y, centerPoint.y));
	
	pScrollView->setContentOffset(offset);

	if((strcmp(mapIdName.c_str(), "xl.level")==0)
		||(strcmp(mapIdName.c_str(), "cac.level")==0))
	{
		pMapName->setString(StaticDataMapName::s_mapname[mapIdName.c_str()].c_str());
	}
	else
	{
		std::string full_map_name = CMapInfo::getCountryName(m_iMapCountry);
		full_map_name.append(StringDataManager::getString("country_separator"));
		full_map_name.append(StaticDataMapName::s_mapname[mapIdName.c_str()].c_str());
		pMapName->setString(full_map_name.c_str());
	}
	
}

void MapScene::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
}
void MapScene::scrollViewDidZoom(cocos2d::extension::ScrollView* view)
{
}

bool MapScene::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	bool result = this->resignFirstResponder(pTouch,this,false);
	if(result)
	{
		m_mapDragStartPoint = pTouch->getLocation();   // record the start point
	}

	return result;
}
void MapScene::onTouchMoved(Touch *pTouch, Event *pEvent)
{
	// check if the drag distance is long enough
	if(pTouch->getLocation().getDistance(m_mapDragStartPoint) > 20)
	{
		m_mapScrollViewDraged = true;
	}
}
void MapScene::onTouchEnded(Touch *pTouch, Event *pEvent)
{
	// 点击地图，主角移动
	auto pMapLayer = (Layer*)this->getChildByTag(MAPSCENE_MAP_LAYER_TAG);
	auto pScrollView = (cocos2d::extension::ScrollView*)pMapLayer->getChildByTag(ScrollView_MAP_TAG);
	//只有当前地图相应主角点击移动  
	if(pScrollView != NULL&&!equipTab->getCurrentIndex())
	{
		if(m_mapScrollViewDraged)
		{
			m_mapScrollViewDraged = false;
			return;
		}

		// check if the touch point is in the scroll view rect
		auto scrollviewRect = Rect(0, 0, pScrollView->getViewSize().width, pScrollView->getViewSize().height);
		auto targetPos = pScrollView->convertTouchToNodeSpace(pTouch);
		if(!scrollviewRect.containsPoint(targetPos))
			return;

		auto contentOffset = pScrollView->getContentOffset();
		auto mapTargetPos = targetPos - contentOffset;

		auto pContainer = pScrollView->getContainer();
		auto pAreaMap = (AreaMap*)pContainer->getChildByTag(AREA_MAP_TAG);
		auto worldPosition = pAreaMap->convertToGameWorldSpace(mapTargetPos);

		auto cmd = new MyPlayerCommandMove();
		if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),m_mapId.c_str()) != 0)
		{
			cmd->targetMapId = m_mapId;
		}
		cmd->targetPosition = worldPosition;
		GameView::getInstance()->myplayer->setNextCommand(cmd, true);

		// target anim
		auto pTargetAnim = CCLegendAnimation::create("animation/texiao/changjingtexiao/SB/sb.anm");
		pTargetAnim->setPosition(pAreaMap->convertToMapSpace(worldPosition));
		pTargetAnim->setScale(1.2f);
		pAreaMap->addChild(pTargetAnim);

		// 去掉 “自动战斗”的动画（如果有的话）
		auto pMyPlayer = GameView::getInstance()->myplayer;
		auto pNode = pMyPlayer->getChildByTag(PLAYER_AUTO_FIGHT_ANIMATION_TAG);
		if (NULL != pNode)
		{
			pMyPlayer->removeAutoCombatFlag();
		}
	}
}
void MapScene::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void MapScene::tableCellHighlight(TableView* table, TableViewCell* cell)
{
    // please set cell's content size
	cell->setContentSize(tableCellSizeForIndex(table, cell->getIdx()));

    // if anchor point is Vec2::ZERO, then set its anchor point to (0.5f, 0.5f) for scale action
    if(cell->getAnchorPoint().equals(Vec2::ZERO))
    {
        cell->setPosition(cell->getPositionX()+cell->getContentSize().width/2, cell->getPositionY()+cell->getContentSize().height/2);
        cell->setAnchorPoint(Vec2(0.5f,0.5f));
    }
	auto zoomAction = ScaleTo::create(0.05f, 0.9f);
    cell->runAction(zoomAction);
}

void MapScene::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto  zoomAction = Sequence::create(
                                                         ScaleTo::create(0.1f,1.1f),
                                                         ScaleTo::create(0.1f,1.0f),
                                                         NULL);
    cell->runAction(zoomAction);
}


void MapScene::tableCellTouched(TableView* table, TableViewCell* cell)
{
	CCLOG("cell touched at index: %i", cell->getIdx());
	short rol,col;
	unsigned int index;
	auto cmd = new MyPlayerCommandMove();

	switch(m_iMapFindInfo)
	{
		case 0:
			{
				auto pActor = GameView::getInstance()->getGameScene()->getActor(playerActor.at(cell->getIdx()));
				if(pActor == NULL)
				{
					return;
				}
				//Layer* layer = (Layer*)getChildByTag(LAYER_TAG);
				short rol,col;
				rol = pActor->getWorldPosition().x;
				col = pActor->getWorldPosition().y;
				cmd->targetPosition = Vec2(rol, col);
				GameView::getInstance()->myplayer->setNextCommand(cmd, true);
				/*AddFunctionMenu * nameList=AddFunctionMenu::create(playerActor.at(cell->getIdx()));
				nameList->setIgnoreAnchorPointForPosition(false);
				nameList->setAnchorPoint(Vec2(0,0));
				rol = table->getPosition().x - 20;
				col = table->getPosition().y + 310;
				nameList->setPosition(Vec2(rol,col));
				layer->addChild(nameList,4);*/
				m_iState = state_idle;
				MapAction::getInstance()->setIsUpdate(false);
			}
			break;
		case 1:
			{
				std::vector<DoorInfo*> doors = GameWorld::MapInfos[m_mapId.c_str()]->doors;
				if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),m_mapId.c_str()) != 0)
				{
					cmd->targetMapId = m_mapId;
				}
				for(index = 0; index < doors[cell->getIdx()]->doorXY.size(); index++)
				{
					short tileX = doors[cell->getIdx()]->doorXY.at(index)[0];
					short tileY = doors[cell->getIdx()]->doorXY.at(index)[1];
					if(!GameSceneLayer::isLimitOnOtherMapGround(tileX, tileY, pLevelData))
					{
						rol = tileY;
						col = tileX;
						cmd->targetPosition = Vec2(GameSceneLayer::tileToPositionX(col), GameSceneLayer::tileToPositionY(rol));
						break;
					}
				}
				GameView::getInstance()->myplayer->setNextCommand(cmd, true);
				m_iState = state_idle;
				MapAction::getInstance()->setIsUpdate(false);
			}
			break;
		case 2:
            {
                targetMapId = GameView::getInstance()->getMapInfo()->mapid();
                targetActorId = pLevelData->npcActorsInfo[cell->getIdx()]->templateId;
                if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),m_mapId.c_str()) != 0)
                {
                    cmd->targetMapId = m_mapId;
                    targetMapId = cmd->targetMapId;
                }
                rol = pLevelData->monsterActorsInfo[monsterIndex.at(cell->getIdx())]->rol;
                col = pLevelData->monsterActorsInfo[monsterIndex.at(cell->getIdx())]->col;
                cmd->targetPosition = Vec2(GameSceneLayer::tileToPositionX(col), GameSceneLayer::tileToPositionY(rol));
                targetPos = cmd->targetPosition;
                GameView::getInstance()->myplayer->setNextCommand(cmd, true);
                m_iState = state_moveto_monster;
                
                MapAction::getInstance()->setData(m_iState, targetPos, targetMapId, targetActorId);
            }
			break;
		case 3:
            {
                targetMapId = GameView::getInstance()->getMapInfo()->mapid();
                targetActorId = pLevelData->npcActorsInfo[cell->getIdx()]->templateId;
                rol = pLevelData->npcActorsInfo[cell->getIdx()]->rol;
                col = pLevelData->npcActorsInfo[cell->getIdx()]->col;
				auto pointTmp = Vec2(GameSceneLayer::tileToPositionX(col), GameSceneLayer::tileToPositionY(rol));
                if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),m_mapId.c_str()) != 0)
                {
                    cmd->targetPosition = getNearReachablePoint(pointTmp, false, pLevelData);
                    cmd->targetMapId = m_mapId;
                    targetMapId = cmd->targetMapId;
                    targetPos = cmd->targetPosition;
                }
                else
                {
                    cmd->targetPosition = getNearReachablePoint(pointTmp, true, NULL);
                    targetPos = cmd->targetPosition;
                }
                //cmd->targetPosition = Vec2(GameSceneLayer::tileToPositionX(col), GameSceneLayer::tileToPositionY(rol));
                GameView::getInstance()->myplayer->setNextCommand(cmd, true);
                m_iState = state_moveto_npc;
                
                MapAction::getInstance()->setData(m_iState, targetPos, targetMapId, targetActorId);
            }
			break;
		default:
			break;
	}	
}

Size MapScene::tableCellSizeForIndex(TableView *table, unsigned int idx)
{
	return Size(100, 50);
}

TableViewCell* MapScene::tableCellAtIndex(TableView *table, ssize_t  idx)
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();
	cell= new TableViewCell;
	cell->autorelease();

	auto sprite = cocos2d::extension::Scale9Sprite::create("res_ui/kuang3_new.png");
    sprite->setAnchorPoint(Vec2(0, 0));
	sprite->setPreferredSize(Size(190,45));
    sprite->setPosition(Vec2(0, 0));
	sprite->setTag(123);
    cell->addChild(sprite);

	long templateId;
	CNpcInfo * npcInfo;
	std::string mapId, doorName;
	std::string monsterName;
	char pTemp[10];
	GameActor* pActor;

	auto npcName=Label::createWithTTF("",APP_FONT_NAME,18);
	auto color = Color4B::BLACK;
	npcName->enableShadow(color, Size(1.0, -1.0), 1.0);
	npcName->setAnchorPoint(Vec2(0.5f,0.5f));
	cell->addChild(npcName);
	switch(m_iMapFindInfo)
	{
		case 0:
			{
				npcName->setPosition(Vec2(sprite->getContentSize().width/2-30,sprite->getContentSize().height/2));

				pActor = GameView::getInstance()->getGameScene()->getActor(playerActor.at(idx));
				if(pActor == NULL)
					break;
				npcName->setString(pActor->getActorName().c_str());

				if(GameView::getInstance()->myplayer->getActiveRole()->level() >= 5)
				{
					auto layer = (Layer*)getChildByTag(LAYER_TAG);
					auto btn_detail = NearbyPlayersDetail::create(playerActor.at(idx));
					btn_detail->setPosition(Vec2(160,22));
					btn_detail->setLayer(layer);
					btn_detail->setTableView(table);
					cell->addChild(btn_detail);
				}
			}
			break;
		case 1:
			npcName->setPosition(Vec2(sprite->getContentSize().width/2,sprite->getContentSize().height/2));

			mapId = GameWorld::MapInfos[m_mapId.c_str()]->doors[idx]->mapId.c_str();
			doorName = StaticDataMapName::s_mapname[mapId];
			if(!strcmp(m_mapId.c_str(), "xl.level")&&!memcmp("xldy2", mapId.c_str(), 5))
			{
				doorName.append(getCountryInfo(mapId.at(6)));
			}
			npcName->setString(doorName.c_str());
			break;
		case 2:
			npcName->setPosition(Vec2(sprite->getContentSize().width/2,sprite->getContentSize().height/2));

			templateId = monsterTemplateId.at(idx);
			monsterName = StaticDataMonsterBaseInfo::s_monsterBase[templateId]->name;
			//monsterName.append(" LV_");
			sprintf(pTemp, "(%d%s)", StaticDataMonsterBaseInfo::s_monsterBase[templateId]->level, StringDataManager::getString("fivePerson_ji"));
			monsterName.append(pTemp);
			npcName->setString(monsterName.c_str());
			break;
		case 3:
			npcName->setPosition(Vec2(sprite->getContentSize().width/2,sprite->getContentSize().height/2));

			templateId = pLevelData->npcActorsInfo[idx]->templateId;
			npcInfo = GameWorld::NpcInfos[templateId];
			npcName->setString(npcInfo->npcName.c_str());
			break;
		default:
			break;
	}
	// delete level data at last
	//delete pLevelData;

	return cell;
}

ssize_t MapScene::numberOfCellsInTableView(TableView *table)
{
	if(pLevelData == NULL)
		return 0;

	switch(m_iMapFindInfo)
	{
		case 0:
			return playerActor.size();
			break;
		case 1:
			if(GameWorld::MapInfos[m_mapId.c_str()] != NULL)
			{
				return GameWorld::MapInfos[m_mapId.c_str()]->doors.size();
			}
			else
			{
				return 0;
			}
			break;
		case 2:
			return monsterTemplateId.size();
			break;
		case 3:
			return pLevelData->npcActorsNumber;
			break;
		default:
			break;
	}

	//CCAssert(false, "should not run to here");
	return 0;
}

Vec2 MapScene::getNearReachablePoint(Vec2& targetPoint, bool isCurrentMap, LegendLevel* pLevelData)
{
	if(isCurrentMap)
	{
		auto scene = GameView::getInstance()->getGameScene();
	
		short tileX = scene->positionToTileX(targetPoint.x);
		short tileY = scene->positionToTileY(targetPoint.y);
		const int range_offset = 2;
		const int init_offset = 1;
		short initTileX = tileX - init_offset;
		short initTileY = tileY - init_offset;
		for(short x = initTileX; x < (initTileX+range_offset); x++)
		{
			for(short y = initTileY; y < (initTileY+range_offset); y++)
			{
				// out of map, ignore
				if(!scene->checkInMap(x, y))
					continue;

				if(scene->isLimitOnGround(x, y))
					continue;

				float posX = scene->tileToPositionX(x);
				float posY = scene->tileToPositionY(y);
				return Vec2(posX, posY);
			}
		}
	}
	else
	{
		short tileX = GameSceneLayer::positionToTileX(targetPoint.x);
		short tileY = GameSceneLayer::positionToTileY(targetPoint.y);
		const int range_offset = 2;
		const int init_offset = 1;
		short initTileX = tileX - init_offset;
		short initTileY = tileY - init_offset;
		for(short x = initTileX; x < (initTileX+range_offset); x++)
		{
			for(short y = initTileY; y < (initTileY+range_offset); y++)
			{
				if(GameSceneLayer::isLimitOnOtherMapGround(x, y, pLevelData))
					continue;

				float posX = GameSceneLayer::tileToPositionX(x);
				float posY = GameSceneLayer::tileToPositionY(y);
				return Vec2(posX, posY);
			}
		}
	}

	return targetPoint;
}

std::string MapScene::getCountryInfo(char countryId)
{
	std::string string;
	string.append("(");

	std::string countryIdName = "country_simple_";
	char id[4];
	sprintf(id, "%c", countryId);
	countryIdName.append(id);
	string.append(StringDataManager::getString(countryIdName.c_str()));
	string.append(")");

	return string;
}

void MapScene::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void MapScene::callBackScene( Ref * obj )
{
	auto tab= (UITab *)obj;
	int num= tab->getCurrentIndex();
	auto pLayer = (Layer*)this->getChildByTag(LAYER_TAG);
	auto btn = (Button*)pLayer->getChildByTag(CHANGELINE_TAG);
	switch(num)
	{
	case 0:
		{
			m_iMapType = 0;
			panelcurrent->setVisible(true);
			findTab->setVisible(true);
			infoTabview->setVisible(true);
			panelCountry->setVisible(false);
			panelWorld->setVisible(false);
			myIcon->setOpacity(0);
			auto countryMap_dragPanel = (cocos2d::extension::ScrollView*)Helper::seekWidgetByName(panelCountry,"DragPanel_469");
			auto icon = (ImageView*)countryMap_dragPanel->getChildByTag(MYPLAYER_ICON_TAG);
			if(icon != NULL)
			{
				icon->removeFromParentAndCleanup(true);
			}
			
			btn->setVisible(true);
			//btn->setPosition(Vec2(480,410));

			auto pMapLayer = (Layer*)this->getChildByTag(MAPSCENE_MAP_LAYER_TAG);
			pMapLayer->setVisible(true);
			m_mapId = GameView::getInstance()->getMapInfo()->mapid();
			this->reloadAreaMap(m_mapId);
		}break;
	case 1:
		{
			m_iMapType = 1;
			panelcurrent->setVisible(false);
			findTab->setVisible(false);
			infoTabview->setVisible(false);
			panelCountry->setVisible(true);
			panelWorld->setVisible(false);
			auto pMapLayer = (Layer*)this->getChildByTag(MAPSCENE_MAP_LAYER_TAG);
			pMapLayer->setVisible(false);
			myIcon->setOpacity(0);
			
			btn->setVisible(false);

			if(m_iMapCountry == GameView::getInstance()->getMapInfo()->country()
				||(GameView::getInstance()->getMapInfo()->country()==0 && strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(), "jzz.level")!=0))
			{
				initMyplayerIcon(1);
			}
			else
			{
				auto countryMap_dragPanel = (cocos2d::extension::ScrollView*)Helper::seekWidgetByName(panelCountry,"DragPanel_469");
				auto icon = (ImageView*)countryMap_dragPanel->getChildByTag(MYPLAYER_ICON_TAG);
				if(icon != NULL)
				{
					icon->removeFromParentAndCleanup(true);
				}
			}
			initMapButton();
		}break;
	case 2:
		{
			m_iMapType = 2;
			panelcurrent->setVisible(false);
			findTab->setVisible(false);
			infoTabview->setVisible(false);
			panelCountry->setVisible(false);
			panelWorld->setVisible(true);
			auto pMapLayer = (Layer*)this->getChildByTag(MAPSCENE_MAP_LAYER_TAG);
			pMapLayer->setVisible(false);
			auto countryMap_dragPanel = (cocos2d::extension::ScrollView*)Helper::seekWidgetByName(panelCountry,"DragPanel_469");
			auto icon = (ImageView*)countryMap_dragPanel->getChildByTag(MYPLAYER_ICON_TAG);
			if(icon != NULL)
			{
				icon->removeFromParentAndCleanup(true);
			}
			
			btn->setVisible(false);

			if(GameView::getInstance()->getMapInfo()->country())
			{
				initMyplayerIcon(2);
			}
			else
			{
				myIcon->setOpacity(0);
			}
		}break;

	}
}

void MapScene::initMyplayerIcon(int mode)
{
	Button * btn;
	//Button * btn_child;
	string pName("Button_");
	string pMapId;
	int index, x, y;

	switch(mode)
	{
	case 1:
		{
		pMapId = GameView::getInstance()->getMapInfo()->mapid();
		if(GameView::getInstance()->getMapInfo()->country())
		{
			index = pMapId.find("_");
		}
		else
		{
			index = pMapId.find(".");
		}
		pMapId = pMapId.substr(0, index);
		if(!memcmp(pMapId.c_str(), "jzgy", 4))
		{
			pMapId.clear();
			pMapId.append("jzgy1");
		}
		pName.append(pMapId);
		btn = (Button *)panelCountry->getChildByName(pName.c_str());
		//btn_child = (Button*)btn->getChildByTag(BTN_MAP_TAG);
		//x = btn->getPosition().x+btn_child->getPosition().x;
		//y = btn->getPosition().y+btn_child->getPosition().y;
		auto countryMap_dragPanel = (cocos2d::extension::ScrollView*)Helper::seekWidgetByName(panelCountry,"DragPanel_469");
		auto icon = (ImageView*)countryMap_dragPanel->getChildByTag(MYPLAYER_ICON_TAG);
		if(icon == NULL)
		{
			auto myplayer = GameView::getInstance()->myplayer;
			std::string iconFilePath = BasePlayer::getSmallHeadPathByProfession(myplayer->getProfession());
			// MyPlayer icon
			icon = ImageView::create();
			icon->loadTexture(iconFilePath.c_str());
			icon->setAnchorPoint(Vec2(0.5f, 0.5f));
			icon->setTag(MYPLAYER_ICON_TAG);
			//myIcon->setVisible(false);
			icon->setVisible(false);
			icon->setLocalZOrder(100);
			countryMap_dragPanel->addChild(icon);

			// blink the icon
			auto repeatAction = RepeatForever::create( CCBlink::create(1.0f, 2) );
			icon->runAction(repeatAction);
		}
		x = btn->getPosition().x;
		y = btn->getPosition().y + 30;
		icon->setPosition(Vec2(x, y));
		//myIcon->setVisible(true);
		icon->setVisible(true);
		}
		break;
	case 2:
		btn = (Button *)panelWorld->getChildByTag(GameView::getInstance()->getMapInfo()->country());
		myIcon->setPosition(Vec2(btn->getPosition().x+10, btn->getPosition().y+40));
		//myIcon->setVisible(true);
		myIcon->setOpacity(255);
		break;
	default:
		break;
	}
}

void MapScene::checkActorValidity()
{
	for (std::vector<long long>::iterator it = playerActor.begin(); it != playerActor.end(); ) 
	{
		auto pActor = GameView::getInstance()->getGameScene()->getActor(*it);
		if(pActor == NULL)
		{
			it = playerActor.erase(it);
			continue;
		}
		++it;
	}
}

void MapScene::callBackPerson( Ref * obj )
{
	auto tab=(UITab *)obj;
	tab->setHightLightLabelColor(Color3B(47,93,13));
	tab->setNormalLabelColor(Color3B(255,255,255));
	m_iMapFindInfo = tab->getCurrentIndex();

	checkActorValidity();
	
	infoTabview->reloadData();
}

void MapScene::showCountryMap(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button *)pSender;
		int num = btn->getTag();

		m_iMapCountry = num;

		auto image = (ImageView *)image_countryNameFrame->getChildByTag(COUNTRYNAME_TAG);
		auto image_ink = (ImageView *)image_countryNameFrame->getChildByTag(COUNTRY_INK_TAG);

		char* countryName[] = { "res_ui/map/world_map/weiguo.png", "res_ui/map/world_map/shuguo.png", "res_ui/map/world_map/wuguo.png" };
		char* ink[] = { "res_ui/map/world_map/blue.png", "res_ui/map/world_map/green.png", "res_ui/map/world_map/red.png" };
		image->loadTexture(countryName[m_iMapCountry - 1]);
		image_ink->loadTexture(ink[m_iMapCountry - 1]);

		//m_countryName->setFontSize(50);
		showCountryMapScale();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MapScene::callbackClickedNotReached(Ref* obj)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("map_country_notquest"));
}

void MapScene::showCurrMap_child( Ref * obj )
{
	auto btn_child =(Button *)obj;
	auto btn = (Button *)btn_child->getParent();

	panelcurrent->setVisible(true);
	findTab->setVisible(true);
	panelCountry->setVisible(false);
	panelWorld->setVisible(false);
	equipTab->setDefaultPanelByIndex(0);
	string pMapId(btn->getName());
	if((strcmp(pMapId.c_str(), "Button_xl")!=0)&&(strcmp(pMapId.c_str(), "Button_cac")!=0))
	{
		pMapId = pMapId.substr(7, pMapId.size());
		char pTemp[10];
		sprintf(pTemp, "_%d.level", m_iMapCountry);
		pMapId.append(pTemp);
	}
	else
	{
		if(strcmp(pMapId.c_str(), "Button_xl")==0)
		{
			pMapId = "xl.level";
		}
		else
		{
			pMapId = "cac.level";
		}
	}
	/*if(pMapId == m_mapId)
		return;
	else*/
	if(pMapId != m_mapId)
		m_mapId = pMapId;

	infoTabview->setVisible(true);
	auto pMapLayer = (Layer*)this->getChildByTag(MAPSCENE_MAP_LAYER_TAG);
	pMapLayer->setVisible(true);
	myIcon->setOpacity(0);

	this->reloadAreaMap(m_mapId);
}

void MapScene::showCurrMap(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button *)pSender;

		panelcurrent->setVisible(true);
		findTab->setVisible(true);
		panelCountry->setVisible(false);
		panelWorld->setVisible(false);
		equipTab->setDefaultPanelByIndex(0);
		string pMapId(btn->getName());
		if ((strcmp(pMapId.c_str(), "Button_xl") != 0) && (strcmp(pMapId.c_str(), "Button_cac") != 0))
		{
			pMapId = pMapId.substr(7, pMapId.size());
			char pTemp[10];
			sprintf(pTemp, "_%d.level", m_iMapCountry);
			pMapId.append(pTemp);
		}
		else
		{
			if (strcmp(pMapId.c_str(), "Button_xl") == 0)
			{
				pMapId = "xl.level";
			}
			else
			{
				pMapId = "cac.level";
			}
		}
		/*if(pMapId == m_mapId)
		return;
		else*/
		if (pMapId != m_mapId)
			m_mapId = pMapId;

		infoTabview->setVisible(true);
		auto pMapLayer = (Layer*)this->getChildByTag(MAPSCENE_MAP_LAYER_TAG);
		pMapLayer->setVisible(true);
		myIcon->setOpacity(0);

		auto pLayer = (Layer*)this->getChildByTag(LAYER_TAG);
		auto btn_change = (Button*)pLayer->getChildByTag(CHANGELINE_TAG);
		btn_change->setVisible(true);
		//btn_change->setPosition(Vec2(480,410));

		this->reloadAreaMap(m_mapId);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MapScene::showCountryMapScale()
{
	panelcurrent->setVisible(false);
	findTab->setVisible(false);
	panelCountry->setVisible(true);
	panelWorld->setVisible(false);
	equipTab->setDefaultPanelByIndex(1);
	myIcon->setOpacity(0);
	//add by liutao    
	initMapButton();
	if(m_iMapCountry == GameView::getInstance()->getMapInfo()->country()
		||GameView::getInstance()->getMapInfo()->country()==0)
	{
		initMyplayerIcon(1);
		//myIcon->setOpacity(255);
	}
	else
	{
		myIcon->setOpacity(0);
	}
}

bool MapScene::isTemplateIdExist(int monsterIndex)
{
	for (unsigned int i = 0; i < monsterTemplateId.size(); i++)
	{
		if(pLevelData->monsterActorsInfo[monsterIndex]->templateId == monsterTemplateId.at(i))
		{
			return true;
		}
	}
	return false;
}

void MapScene::initMapInfo(std::string mapId)
{
	// delete the old one
	CC_SAFE_DELETE(pLevelData);

	// load level
	pLevelData = new LegendLevel();

	std::string levelFile = "level/";
	levelFile.append(mapId);
	pLevelData->load(levelFile.c_str());   // rom_huanggong.level

	monsterTemplateId.clear();

	if(pLevelData->monsterActorsNumber)
	{
		monsterTemplateId.push_back(pLevelData->monsterActorsInfo[0]->templateId);
		monsterIndex.push_back(0);
		for(int i=1; i < pLevelData->monsterActorsNumber; i++)
		{
			if(!MapScene::isTemplateIdExist(i))
			{
				monsterTemplateId.push_back(pLevelData->monsterActorsInfo[i]->templateId);
				monsterIndex.push_back(i);
			}
		}
	}

	//GameView::getInstance()->getGameScene()->getTypeActor(monsterActor, GameActor::type_monster);
	playerActor.clear();
	GameView::getInstance()->getGameScene()->getTypeActor(playerActor, GameActor::type_player);
}

void MapScene::ChangeLineEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto channelUI = (ChannelUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagChannelUI);
		if (channelUI == NULL)
		{
			channelUI = ChannelUI::create();
			auto winSize = Director::getInstance()->getVisibleSize();
			GameView::getInstance()->getMainUIScene()->addChild(channelUI, 0, kTagChannelUI);
			channelUI->setIgnoreAnchorPointForPosition(false);
			channelUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			channelUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

bool MapScene::isFinishedMove()
{
	if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	{
		auto cp_role = GameView::getInstance()->myplayer->getWorldPosition();
		auto cp_target = targetPos;
		//CCLOG("%f",sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)));
		if (cp_role.getDistance(cp_target) < 96)
		{
			GameView::getInstance()->myplayer->changeAction(ACT_STAND);
			if (GameView::getInstance()->getGameScene()->getActor(targetActorId) != NULL)
			{
				GameView::getInstance()->myplayer->setLockedActorId(targetActorId);
			}
			
			return true;
		}
		else 
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	
}
/*
void MapScene::update(float dt)
{
	switch(m_iState)
	{
	case state_idle:
		break;
	case state_moveto_npc:
		if(isFinishedMove())
		{
			UIScene *mainScene = (UIScene*)GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1129, (void *)targetActorId);
			}
			m_iState = state_idle;
		}
		break;
	case state_moveto_monster:
		if(isFinishedMove())
		{
			if(GameView::getInstance()->myplayer->selectEnemy(380))
 			{
 				BaseFighter* bf = dynamic_cast<BaseFighter*>(GameView::getInstance()->myplayer->getLockedActor());
 				CCAssert(bf != NULL, "basefighter should not be null");
 
 				MyPlayerCommandAttack* cmd = new MyPlayerCommandAttack();
 				cmd->skillId = GameView::getInstance()->myplayer->getDefaultSkill();
 				cmd->skillProcessorId = cmd->skillId;
 				cmd->sendAttackRequest = true;
 				cmd->targetId = bf->getRoleId();
 
 				GameView::getInstance()->myplayer->getSimpleAI()->startKill(bf->getRoleId());
 	
 				GameView::getInstance()->myplayer->setNextCommand(cmd, true);
			}
			m_iState = state_idle;
		}
		break;
	default:
		break;
	}
}*/