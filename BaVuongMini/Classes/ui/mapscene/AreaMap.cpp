#include "AreaMap.h"
#include "AppMacros.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/MapInfo.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CMonsterBaseInfo.h"
#include "../../messageclient/element/CTeamMember.h"
#include "../../gamescene_state/role/FunctionNPC.h"
#include "../../utils/GameUtils.h"

#define MAP_SCALE_CONST 0.4f

#define MYPLAYER_ICON_TAG 100
#define MYPLAYER_POSITION_TAG 101

AreaMap::AreaMap()
{
	scheduleUpdate();
}

AreaMap::~AreaMap()
{
}

void AreaMap::onEnter()
{
	Layer::onEnter();
}
void AreaMap::onExit()
{
	Layer::onExit();
}

AreaMap* AreaMap::create(const char* levelName)
{
	auto areaMap = new AreaMap();
	if (areaMap && areaMap->init(levelName))
	{
		areaMap->autorelease();
		return areaMap;
	}
	CC_SAFE_DELETE(areaMap);
	return NULL;
}

bool AreaMap::init(const char* levelName)
{
	if (Layer::init())
	{
		// load level
		auto pLevelData = new LegendLevel();

		std::string levelFile = "level/";
		levelFile.append(levelName);
		pLevelData->load(levelFile.c_str());   // rom_huanggong.level

		auto pTiledmap = new CCLegendTiledMap();
		pTiledmap->setStatic(true);
		pTiledmap->initWithTiledmapFilename(pLevelData->tiledmapName.c_str());

		auto mapSize = pTiledmap->getTiledMapData()->getMapSize();

		m_mapSizeAtWorldSpace.width = mapSize.width;
		m_mapSizeAtWorldSpace.height = mapSize.height*2;   // 转换为游戏世界坐标下的尺寸

		pTiledmap->setPosition(Vec2::ZERO);

		//m_mapScale = MAP_SCALE_CONST;
		float widthScale = 460.f / mapSize.width;
		float heightScale = 395.f / mapSize.height;
		m_mapScale = MAX(widthScale, heightScale);
		pTiledmap->setScale(m_mapScale);
		m_mapSize = mapSize * m_mapScale;

		addChild(pTiledmap);
		pTiledmap->release();

	////////////////////////////////////////////////////////////////

		// icon: myself
		if(GameView::getInstance()->getMapInfo()->mapid() == levelName)
		{
			// MyPlayer icon
			auto myplayer = GameView::getInstance()->myplayer;
			
			std::string iconFilePath = BasePlayer::getSmallHeadPathByProfession(myplayer->getProfession());
			auto myIcon = Sprite::create(iconFilePath.c_str());
			myIcon->setPosition(convertToMapSpace(myplayer->getWorldPosition()));
			myIcon->setTag(MYPLAYER_ICON_TAG);
			addChild(myIcon, 3);

			int x = GameSceneLayer::positionToTileX(myplayer->getWorldPosition().x);
			int y = GameSceneLayer::positionToTileY(myplayer->getWorldPosition().y);
			char aucBuf[12];
			memset(aucBuf, 0, sizeof(aucBuf));
			sprintf(aucBuf, "(%d,%d)", x, y);
			//Label *pLabelMyplayer=Label::createWithTTF(aucBuf,APP_FONT_NAME,18);
			auto pLabelMyplayer = Label::createWithBMFont("res_ui/font/ziti_3.fnt", aucBuf);
			//Color3B color = Color3B(0, 0, 0);
			//pLabelMyplayer->enableShadow(Size(2.0, -2.0), 0.5, 0.0, color);
			pLabelMyplayer->setAnchorPoint(Vec2(0.5f,0.5f));
			pLabelMyplayer->setScale(0.8f);
			pLabelMyplayer->setColor(Color3B(255, 255, 255));
			pLabelMyplayer->setPosition(Vec2(x, y-30));
			pLabelMyplayer->setTag(MYPLAYER_POSITION_TAG);
			addChild(pLabelMyplayer, 1);

			// blink the icon
			auto repeat = RepeatForever::create( CCBlink::create(1.0f, 2) );
			myIcon->runAction( repeat);
		}

		//icon:teammate
		loadTeammateInfo(levelName);

		// icon: teleport door
		if(GameWorld::MapInfos[levelName] != NULL)
		{
			std::vector<DoorInfo*>& allDoors = GameWorld::MapInfos[levelName]->doors;
			for(unsigned int i = 0; i < allDoors.size(); i++)
			{
				addDoor(allDoors.at(i), levelName, pLevelData);
			}
		}

		// icon: NPC
		for(int i = 0; i < pLevelData->npcActorsNumber; i++)
		{
			auto actorInfo = (NPCActorInfo*)pLevelData->npcActorsInfo[i];

			short rol = actorInfo->rol;
			short col = actorInfo->col;
			long templateId = actorInfo->templateId;

			auto npcInfo = GameWorld::NpcInfos[templateId];
			std::string mapName = npcInfo->npcName;
			std::string mapIcon = npcInfo->mapIcon;
			Vec2 mapPosition = convertToMapSpace(Vec2(GameSceneLayer::tileToPositionX(col), GameSceneLayer::tileToPositionY(rol)));
			
			auto pLabel=Label::createWithTTF(mapName.c_str(),APP_FONT_NAME,12);
			auto color = Color4B::BLACK;
			pLabel->enableShadow(color, Size(1.0, -1.0), 1.0);
			pLabel->setAnchorPoint(Vec2(0.5f,0.5f));
			pLabel->setColor(Color3B(3, 247, 245));
			pLabel->setPosition(Vec2(mapPosition.x, mapPosition.y+10));
			addChild(pLabel, 1);

			auto pSprite = Sprite::create("res_ui/select_the_sercer/blue.png");
			pSprite->setAnchorPoint(Vec2(0.5f,0.5f));
			pSprite->setPosition(Vec2(mapPosition.x, mapPosition.y));
			pSprite->setScale(0.6f);
			addChild(pSprite, 1);

			m_npcMissionIndicate = Sprite::create("gamescene_state/zhujiemian3/other/jingtan.png");
			m_npcMissionIndicate->setAnchorPoint(Vec2(0.5f,0.5f));
			m_npcMissionIndicate->setPosition(Vec2(mapPosition.x, mapPosition.y+30));
			m_npcMissionIndicate->setScale(0.4f);
			addChild(m_npcMissionIndicate, 1);
			/*MoveBy * moveToAction = MoveBy::create(0.5f,Vec2(0,-10));
			RepeatForever * repeapAction = RepeatForever::create(Sequence::create(moveToAction,moveToAction->reverse(),NULL));
			m_npcMissionIndicate->runAction(repeapAction);*/
			m_npcMissionIndicate->setVisible(false);

			auto fn = dynamic_cast<FunctionNPC*>(GameView::getInstance()->getGameScene()->getActor(templateId));
			if(fn!=NULL)
			{
				CCLOG("THE FUNCTION IS EXIST");
				if(strlen(NpcData::s_npcFunctionIcon[templateId].c_str()))
				{
					m_npcMissionIndicate->setPosition(Vec2(mapPosition.x, mapPosition.y+40));

					auto pIconSprite = Sprite::create(fn->m_sFunctionIcon.c_str());
					pIconSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
					pIconSprite->setPosition(Vec2(mapPosition.x, mapPosition.y+25));
					pIconSprite->setScale(0.5f);
					addChild(pIconSprite, 1);
				}
				
				fn->RefreshNpc();
				updateNpcStateIcon(fn->function);
			}
		}

		// icon: monster
		if(pLevelData->monsterActorsNumber)
		{
			std::vector<long> monsterTemplateId;
			std::vector<int> monsterIndex;
			int index;
			monsterTemplateId.push_back(pLevelData->monsterActorsInfo[0]->templateId);
			monsterIndex.push_back(0);
			for(int i=1; i < pLevelData->monsterActorsNumber; i++)
			{
				for (index = 0; index < monsterTemplateId.size(); index++)
				{
					if(pLevelData->monsterActorsInfo[i]->templateId == monsterTemplateId.at(index))
					{
						break;
					}
				}
				if(index == monsterTemplateId.size())
				{
					monsterTemplateId.push_back(pLevelData->monsterActorsInfo[i]->templateId);
					monsterIndex.push_back(i);
				}
			}
			for(int i = 0; i < monsterTemplateId.size(); i++)
			{
				short rol,col;
				rol = pLevelData->monsterActorsInfo[monsterIndex.at(i)]->rol;
				col = pLevelData->monsterActorsInfo[monsterIndex.at(i)]->col;
				auto mapPosition = convertToMapSpace(Vec2(GameSceneLayer::tileToPositionX(col), GameSceneLayer::tileToPositionY(rol)));

				char pTemp[10];
				std::string monsterName = StaticDataMonsterBaseInfo::s_monsterBase[monsterTemplateId.at(i)]->name;
				sprintf(pTemp, "(%d%s)", StaticDataMonsterBaseInfo::s_monsterBase[monsterTemplateId.at(i)]->level, StringDataManager::getString("fivePerson_ji"));
				monsterName.append(pTemp);
				auto pLabelMonster=Label::createWithTTF(monsterName.c_str(),APP_FONT_NAME,12);
				auto color = Color4B::BLACK;
				pLabelMonster->enableShadow(color, Size(1.0, -1.0), 1.0);
				pLabelMonster->setAnchorPoint(Vec2(0.5f,0.5f));
				pLabelMonster->setColor(Color3B(255, 14, 14));
				pLabelMonster->setPosition(Vec2(mapPosition.x, mapPosition.y+10));
				addChild(pLabelMonster, 1);

				auto pSpriteMonster = Sprite::create("res_ui/select_the_sercer/red.png");
				pSpriteMonster->setAnchorPoint(Vec2(0.5f,0.5f));
				pSpriteMonster->setPosition(Vec2(mapPosition.x, mapPosition.y));
				pSpriteMonster->setScale(0.6f);
				addChild(pSpriteMonster, 1);
			}
		}

		// do not forget to delete the level data
		delete pLevelData;
		
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(mapSize);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(AreaMap::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(AreaMap::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(AreaMap::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(AreaMap::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void AreaMap::updateNpcStateIcon(int state)
{
	switch(state)
	{
	case 0 :
		{
			//CCLOG("npc state: 0");
			m_npcMissionIndicate->setVisible(false);
		}
		break;
	case 1 : ///可接
		{
			//CCLOG("npc state: 1");
			auto texture = Director::getInstance()->getTextureCache()->addImage("gamescene_state/zhujiemian3/other/jingtan.png");
			m_npcMissionIndicate->setTexture(texture);
			m_npcMissionIndicate->setVisible(true);
			GameUtils::removeGray(m_npcMissionIndicate);
		}
		break;
	case 2 :///可提交
		{
			//CCLOG("npc state: 2");
			auto texture = Director::getInstance()->getTextureCache()->addImage("gamescene_state/zhujiemian3/other/yiwen.png");
			m_npcMissionIndicate->setTexture(texture);
			m_npcMissionIndicate->setVisible(true);
			GameUtils::removeGray(m_npcMissionIndicate);
		}
		break;
	case 3 : ///任务中
		{
			//CCLOG("npc state: 3");
			auto texture = Director::getInstance()->getTextureCache()->addImage("gamescene_state/zhujiemian3/other/yiwen.png");
			m_npcMissionIndicate->setTexture(texture);
			m_npcMissionIndicate->setVisible(true);
			GameUtils::addGray(m_npcMissionIndicate);
		}
		break;
	case 4 :
		{
			//CCLOG("npc state: 4");
			m_npcMissionIndicate->setVisible(false);
		}
		break;
	}
}

void AreaMap::loadTeammateInfo(std::string mapId)
{
	for ( int i=0;i<GameView::getInstance()->teamMemberVector.size();i++)
	{
		auto teamMember = GameView::getInstance()->teamMemberVector.at(i);
		if(teamMember->roleid() == GameView::getInstance()->myplayer->getRoleId())
		{
			continue;
		}
		auto actor = GameView::getInstance()->getGameScene()->getActor(teamMember->roleid());

		//std::string _mapId = teamMember->mapid();
		//if (_mapId == mapId)
		if(actor!=NULL)
		{
			CCLOG("*************  %d   ************", i);
			//long rol = teamMember->x();
			//long col = teamMember->y();
			//Vec2 mapPosition = convertToMapSpace(Vec2(GameSceneLayer::tileToPositionX(col), GameSceneLayer::tileToPositionY(rol)));
			auto mapPosition = convertToMapSpace(actor->getWorldPosition());

			auto pLabel=Label::createWithTTF(actor->getActorName().c_str(),APP_FONT_NAME,18);
			auto color = Color4B::BLACK;
			pLabel->enableShadow(color, Size(1.0, -1.0), 1.0);
			pLabel->setAnchorPoint(Vec2(0.5f,0.5f));
			pLabel->setColor(Color3B(72, 255, 0));
			//pLabel->setPosition(Vec2(0, 30));
			//addChild(pLabel, 1);

			auto pSpriteMonster = Sprite::create("res_ui/select_the_sercer/green.png");
			pSpriteMonster->setAnchorPoint(Vec2(0.5f,0.5f));
			pSpriteMonster->setPosition(Vec2(mapPosition.x, mapPosition.y));
			pSpriteMonster->setTag(i);
			addChild(pSpriteMonster, 1);

			pLabel->setPosition(Vec2(pSpriteMonster->getContentSize().width/2, pSpriteMonster->getContentSize().height/2+20));
			pSpriteMonster->addChild(pLabel);
		}
	}
}


void AreaMap::addDoor(DoorInfo* info, const char* mapId, LegendLevel* pLevelData)
{
	short rol,col;
	
	auto pDoorNode = CCLegendAnimation::create("animation/texiao/changjingtexiao/CSM/csm.anm");
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	//Sprite *doorSprite=Sprite::create("res_ui/map/chuansongmen.png");
	pDoorNode->setAnchorPoint(Vec2(0.5f,0.5f));
	//get door's center position
	auto centerPos = GameView::getInstance()->getGameScene()->getDoorPosition(mapId, info->mapId, pLevelData);
	col = GameView::getInstance()->getGameScene()->positionToTileX(centerPos.x);
	rol = GameView::getInstance()->getGameScene()->positionToTileY(centerPos.y);
	//rol = info->doorXY.at(0)[1];
	//col = info->doorXY.at(0)[0];
	auto mapPosition = convertToMapSpace(Vec2(GameSceneLayer::tileToPositionX(col), GameSceneLayer::tileToPositionY(rol)));
	pDoorNode->setPosition(Vec2(mapPosition.x, mapPosition.y));
	pDoorNode->setScale(0.2f);
	addChild(pDoorNode, 1);

	auto doorLabel=Label::createWithTTF("",APP_FONT_NAME,12);
	doorLabel->setString(StaticDataMapName::s_mapname[info->mapId.c_str()].c_str());
	auto color = Color4B::BLACK;
    doorLabel->enableShadow(color,Size(1.0, -1.0), 1.0);
	doorLabel->setAnchorPoint(Vec2(0.5f,0.5f));
	doorLabel->setColor(Color3B(255, 255, 255));
	doorLabel->setPosition(Vec2(mapPosition.x, mapPosition.y+10));
	addChild(doorLabel, 1);
}

Vec2 AreaMap::convertToMapSpace(const Vec2& gameWorldPoint)
{
	auto cocos2dWorldPosition = Vec2(gameWorldPoint.x, m_mapSizeAtWorldSpace.height - gameWorldPoint.y);
	cocos2dWorldPosition.y /= 2;
	return cocos2dWorldPosition * m_mapScale;
}

Vec2 AreaMap::convertToGameWorldSpace(const Vec2& cocos2DPoint)
{
	auto point = cocos2DPoint / m_mapScale;
	return Vec2(point.x, m_mapSizeAtWorldSpace.height - point.y * 2);
}

Size AreaMap::getMapSize()
{
	return m_mapSize;
}

bool AreaMap::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return false;
	//get begin point
	m_tBeginPos = pTouch->getLocation();
	return true;
}
void AreaMap::onTouchMoved(Touch *pTouch, Event *pEvent)
{
	auto size = this->backgroundSprite->getContentSize();

	auto touchLocation = pTouch->getLocation();
	float nMoveY = touchLocation.y - m_tBeginPos.y;
	float nMoveX = touchLocation.x - m_tBeginPos.x;

	//current position
	Vec2 curPos = this->backgroundSprite->getPosition();
	//next position 
	Vec2 nextPos = Vec2(curPos.x+nMoveX,curPos.y+nMoveY);

	//max distance
	float maxMoveY = this->backgroundSprite->getContentSize().height/2+size.height/2;
	float maxMoveX = this->backgroundSprite->getContentSize().width/2+size.width/2;

	this->backgroundSprite->setPosition(nextPos);
	m_tBeginPos = touchLocation;
}
void AreaMap::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void AreaMap::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void AreaMap::menuTest(Ref * pSender)
{
	CCLOG("menuTest  !!!!!  !");
}

Vec2 AreaMap::getMyPlayerPosition()
{
	auto myIcon = this->getChildByTag(MYPLAYER_ICON_TAG);
	if(myIcon != NULL)
	{
		return myIcon->getPosition();
	}

	return Vec2(0,0);
}

Vec2 AreaMap::getMapCenterPosition()
{
	return convertToMapSpace(Vec2(m_mapSizeAtWorldSpace.width/2, m_mapSizeAtWorldSpace.height/2));
}

void AreaMap::update(float dt)
{
	// set MyPlayer's position on the map
	auto myIcon = this->getChildByTag(MYPLAYER_ICON_TAG);
	if(myIcon != NULL)
	{
		auto myplayer = GameView::getInstance()->myplayer;
		myIcon->setPosition(convertToMapSpace(myplayer->getWorldPosition()));

		int x = convertToMapSpace(myplayer->getWorldPosition()).x;
		int y = convertToMapSpace(myplayer->getWorldPosition()).y;
		auto myPosition = this->getChildByTag(MYPLAYER_POSITION_TAG);
		myPosition->setPosition(Vec2(x, y-30));

		// update the position info
		x = GameSceneLayer::positionToTileX(myplayer->getWorldPosition().x);
		y = GameSceneLayer::positionToTileY(myplayer->getWorldPosition().y);
		char aucBuf[12];
		sprintf(aucBuf, "(%d,%d)", x, y);
		auto pLabelMyplayer = (Label*)myPosition;
		pLabelMyplayer->setString(aucBuf);
	}

	for ( int i=0;i<GameView::getInstance()->teamMemberVector.size();i++)
	{
		auto teamMember = GameView::getInstance()->teamMemberVector.at(i);
		if(teamMember->roleid() == GameView::getInstance()->myplayer->getRoleId())
		{
			continue;
		}
		auto scene = GameView::getInstance()->getGameScene();
		if(scene == NULL)
		{
			CCLOG("SCENE IS NOT EXSIT");
			return;
		}
		auto actor = scene->getActor(teamMember->roleid());
		if(actor!=NULL)
		{

			auto myIcon = this->getChildByTag(i);
			if(myIcon == NULL)
			{
				Vec2 mapPosition = convertToMapSpace(actor->getWorldPosition());

				auto pLabel=Label::createWithTTF(actor->getActorName().c_str(),APP_FONT_NAME,18);
				auto color = Color4B::BLACK;
				pLabel->enableShadow(color, Size(1.0, -1.0), 1.0);
				pLabel->setAnchorPoint(Vec2(0.5f,0.5f));
				pLabel->setColor(Color3B(72, 255, 0));
				//pLabel->setPosition(Vec2(0, 30));
				//addChild(pLabel, 1);

				myIcon = Sprite::create("res_ui/select_the_sercer/green.png");
				myIcon->setAnchorPoint(Vec2(0.5f,0.5f));
				myIcon->setPosition(Vec2(mapPosition.x, mapPosition.y));
				myIcon->setTag(i);
				addChild(myIcon, 1);

				pLabel->setPosition(Vec2(myIcon->getContentSize().width/2, myIcon->getContentSize().height/2+20));
				myIcon->addChild(pLabel);
				//continue;
			}
			myIcon->setPosition(convertToMapSpace(actor->getWorldPosition()));
		}
	}
}