#include "NearbyPlayersDetail.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "AddFunctionMenu.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"


NearbyPlayersDetail::NearbyPlayersDetail()
{
}


NearbyPlayersDetail::~NearbyPlayersDetail()
{
}

NearbyPlayersDetail* NearbyPlayersDetail::create( long long roleId )
{
	auto nearbyPlayerDetail = new NearbyPlayersDetail();
	if (nearbyPlayerDetail && nearbyPlayerDetail->init(roleId))
	{
		nearbyPlayerDetail->autorelease();
		return nearbyPlayerDetail;
	}
	CC_SAFE_DELETE(nearbyPlayerDetail);
	return NULL;
}

bool NearbyPlayersDetail::init( long long roleId )
{
	if (UIScene::init())
	{
		/////////////////////////////////////////////////////////////////////////////////////

		m_roleId = roleId;
		// �Layer�λ�TableView�֮�ڣ�Ϊ����ȷ��ӦTableView���϶��¼������Ϊ���̵����¼
		//m_pLayer->setSwallowsTouches(false);

		auto btn_detail  = Button::create();
		btn_detail->loadTextures("res_ui/new_button_12.png","res_ui/new_button_12.png","");
		btn_detail->setTouchEnabled(true);
		//btn_detail->setPressedActionEnabled(true);
		btn_detail->setScale9Enabled(true);
		btn_detail->setContentSize(Size(50,30));
		//btn_buy->setCapInsets(Rect(18,9,2,23));
		btn_detail->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_detail->setPosition(Vec2(0,0));
		btn_detail->addTouchEventListener(CC_CALLBACK_2(NearbyPlayersDetail::callBackClicked, this));
		m_pLayer->addChild(btn_detail);

		auto label_detail = Label::createWithTTF(StringDataManager::getString("map_current_detail"), APP_FONT_NAME, 16);
		label_detail->enableOutline(Color4B::BLACK, 2.0f);
		label_detail->setAnchorPoint(Vec2(0.5f,0.5f));
		label_detail->setPosition(Vec2(0, 0));
		label_detail->setColor(Color3B(255, 255, 255));
		btn_detail->addChild(label_detail);

		this->setContentSize(Size(60,45));

		return true;
	}
	return false;
}

void NearbyPlayersDetail::callBackClicked(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button *)pSender;

		int selfLevel_ = GameView::getInstance()->myplayer->getActiveRole()->level();
		int openlevel = 0;
		std::map<int, int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(20);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[20];
		}

		if (selfLevel_ < openlevel)
		{
			char openLevelStr[100];
			const char *strings = StringDataManager::getString("chatAndmailIsOpenInFiveLevel");
			sprintf(openLevelStr, strings, openlevel);
			GameView::getInstance()->showAlertDialog(openLevelStr);
			return;
		}


		auto nameList = AddFunctionMenu::create(m_roleId);
		nameList->setIgnoreAnchorPointForPosition(false);
		nameList->setAnchorPoint(Vec2(0, 0));
		short rol = m_tableview->getPosition().x + 100;
		short col = m_tableview->getPosition().y + 310;
		nameList->setPosition(Vec2(rol, col));
		m_layer->addChild(nameList, 4);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}
