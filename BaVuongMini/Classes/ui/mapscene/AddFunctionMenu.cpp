#include "AddFunctionMenu.h"
#include "../extensions/UITab.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../Mail_ui/MailUI.h"
#include "../extensions/RichTextInput.h"
#include "../extensions/QuiryUI.h"
#include "../Mail_ui/MailFriend.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"

#define UITAB_VERTICAL_INTERVAL_SIZE (-2)

AddFunctionMenu::AddFunctionMenu(void)
{
}


AddFunctionMenu::~AddFunctionMenu(void)
{

}

AddFunctionMenu * AddFunctionMenu::create(long long roleId)
{
	auto pui=new AddFunctionMenu();
	if (pui && pui->init(roleId))
	{
		pui->autorelease();
		return pui;
	}
	CC_SAFE_DELETE(	pui);
	return pui;
}

bool AddFunctionMenu::init(long long roleId)
{
	if(UIScene::init())
	{
		m_roleId = roleId;
		Size s=Director::getInstance()->getVisibleSize();

		auto panel = Layout::create();
		panel->setContentSize(Size(403, 335));
		panel->setPosition(Vec2(0,0));
		const char * normalImage = "res_ui/new_button_5.png";
		const char * selectImage ="res_ui/new_button_5.png";
		const char * finalImage = "";
		char * label[] = {"私聊","查看","组队","好友","邮件"/*,"寻路"*/};//加好友
		auto privateTab=UITab::createWithText(5,normalImage,selectImage,finalImage,label,VERTICAL,UITAB_VERTICAL_INTERVAL_SIZE);
		privateTab->setAnchorPoint(Vec2(0,0));
		privateTab->setPosition(Vec2(0,0));
		privateTab->setHighLightImage((char*)finalImage);
		privateTab->addIndexChangedEvent(this,coco_indexchangedselector(AddFunctionMenu::callBackFuncList));
		privateTab->setPressedActionEnabled(true);
		panel->addChild(privateTab);
		m_pLayer->addChild(panel);

		setContentSize(Size(100,240));
			
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(AddFunctionMenu::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(AddFunctionMenu::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(AddFunctionMenu::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(AddFunctionMenu::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void AddFunctionMenu::onEnter()
{
	UIScene::onEnter();
	//this->openAnim();
}
void AddFunctionMenu::onExit()
{
	UIScene::onExit();
}

bool AddFunctionMenu::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return this->resignFirstResponder(pTouch,this,false,false);
}

void AddFunctionMenu::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}
void AddFunctionMenu::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}
void AddFunctionMenu::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void AddFunctionMenu::callBackFuncList( Ref * obj )
{
	auto tab =(UITab *)obj;
	int num = tab->getCurrentIndex();
	auto cmd = new MyPlayerCommandMove();
	short rol,col;
	auto pActor = GameView::getInstance()->getGameScene()->getActor(m_roleId);
	if(pActor == NULL)
	{
		return;
	}
	Size winSize =Director::getInstance()->getWinSize();
	long long roleGuildId_ = GameView::getInstance()->myplayer->getRoleId();

	switch(num)
	{
	case PRIVATECHAT:
		{
			if (roleGuildId_ == pActor->getRoleId())
			{
				//chatui_priveateOfSelf  chatui_mailOfSelf
				const char *strings_ = StringDataManager::getString("chatui_priveateOfSelf");
				GameView::getInstance()->showAlertDialog(strings_);
				removeFromParentAndCleanup(true);
				return;
			}

			auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene->addPrivateChatUi(pActor->getRoleId(),pActor->getActorName(),0,0,0,0);
		}break;
	case CHECK:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)pActor->getRoleId());
		}break;
	case TEAM:
		{
			int selectId=pActor->getRoleId();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1401,(void *)selectId);
		}break;
	case FRIEND:
		{
			FriendStruct friend1={0,0,pActor->getRoleId(),pActor->getActorName()};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	case MAIL:
		{
			if (roleGuildId_ == pActor->getRoleId())
			{
				const char *strings_ = StringDataManager::getString("chatui_mailOfSelf");
				GameView::getInstance()->showAlertDialog(strings_);
				removeFromParentAndCleanup(true);
				return;
			}

			auto mail_ui=MailUI::create();
			mail_ui->setIgnoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(Vec2(0.5f,0.5f));
			mail_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			std::string playerName=pActor->getActorName();	
			mail_ui->friendName->onTextFieldInsertText(NULL,playerName.c_str(),30);

		}break;
	case MOVE:
		{
			rol = pActor->getWorldPosition().x;
			col = pActor->getWorldPosition().y;
			cmd->targetPosition = Vec2(rol, col);
			GameView::getInstance()->myplayer->setNextCommand(cmd, true);
		}break;
	}
	removeFromParentAndCleanup(true);
}
