#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class NearbyPlayersDetail: public UIScene
{
public:
	NearbyPlayersDetail();
	~NearbyPlayersDetail();

	static NearbyPlayersDetail* create(long long roleId);
	bool init(long long roleId);
	void setLayer(Layer* layer){m_layer = layer;}
	void setTableView(TableView* tableView){m_tableview = tableView;}
private:
	long long m_roleId;
	Layer* m_layer;
	TableView* m_tableview;
private:
	void callBackClicked(Ref *pSender, Widget::TouchEventType type);

};