
#ifndef _UI_MAPSCENE_MAPSCENE_H_
#define _UI_MAPSCENE_MAPSCENE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "AreaMap.h"
#include "../extensions/UIScene.h"
#include "../extensions/UITab.h"
//#include "../../messageclient/pushhandler/PushHandler5080.h"
#include "../../messageclient/GameProtobuf.h"
#include "../../messageclient/protobuf/PlayerMessage.pb.h"
#include "../../gamescene_state/role/GameActor.h"

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

USING_NS_THREEKINGDOMS_PROTOCOL;

class UIScene;
class UITab;

class MapScene :public UIScene,public TableViewDataSource,public TableViewDelegate
{
public:
	MapScene();
	~MapScene();
	static MapScene* create();
	bool init();

	//scrollview滚动的时候会调用
	void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	//scrollview缩放的时候会调用
	void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
    virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);

	void showCountryMapScale();
	void showCountryMap(Ref *pSender, Widget::TouchEventType type);
	
	void showCurrMap(Ref *pSender, Widget::TouchEventType type);
	////////add by liuzhenxing

	bool  isTemplateIdExist(int monsterIndex);

	void initMapInfo(std::string mapId);

	void initMapButton();
	void initMyplayerIcon(int mode);

	void reloadAreaMap(std::string mapIdName);
	void setCurrentMapId(std::string mapId){m_mapId = mapId;}

	Vec2 getNearReachablePoint(Vec2& targetPoint, bool isCurrentMap, LegendLevel* pLevelData);

	//virtual void update(float dt);
	bool isFinishedMove();
public:
	Layout * panelWorld;
	Layout * panelCountry;
	Layout * panelcurrent;
	void callBackScene(Ref * obj);
	void callBackPerson(Ref * obj);
	void callBackExit(Ref *pSender, Widget::TouchEventType type);
	void ChangeLineEvent(Ref *pSender, Widget::TouchEventType type);
	void showCurrMap_child( Ref * obj );
	void callBackFunc();
private:
	void initAreaMapLayer();
	std::string getCountryInfo(char countryId);
	void checkActorValidity();
	void callbackClickedNotReached(Ref * obj);
private:
	//Layout * worldMapPanel;
	//Layout * areaMapPanel;
	Layout * npcListPanel;
	ImageView *image_countryNameFrame;

	Layer * worldMapLayer;
	Layer * areaMapLayer;
	Layer * npcListLayer;

	TableView *infoTabview;
	UITab * equipTab;
	UITab * findTab;

	ImageView * lianzhouImage;
	ImageView * youzhouImage;
	ImageView * yizhouImage;
	ImageView * jingzhouImage;
	ImageView * yangzhouImage;

	ImageView * ImageView_biankuang_right;

	int m_iMapCountry;
	int m_iMapFindInfo;
	int m_iMapType;
	std::string m_mapId;
	std::string m_currentMapId;

	std::vector<long> monsterTemplateId;
	std::vector<int> monsterIndex;
	std::vector<long long> playerActor;

	std::vector<std::string> m_reachedMap;

	LegendLevel* pLevelData;

	Vec2 m_mapDragStartPoint;
	bool m_mapScrollViewDraged;

	std::vector<char*> mapIdList;
	//myplayer icon
	Sprite* myIcon;
	//update state
	int m_iState;
	Vec2 targetPos;
	std::string targetMapId;
	long long targetActorId;

	Vec2 countryCenterPos;
};
#endif;
