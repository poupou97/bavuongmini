#include "MapAction.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/element/MapInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../gamescene_state/role/MyPlayerSimpleAI.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"

enum MapUpdateState{
	state_idle = 1,
	state_moveto_npc = 2,
	state_moveto_monster = 3,
};

MapAction * MapAction::s_mapAction = NULL;

MapAction::MapAction():
isUpdate(false),
m_iState(state_idle)
{
}

MapAction::~MapAction()
{
}

MapAction * MapAction::getInstance()
{
	if (s_mapAction == NULL)
	{
		s_mapAction = new MapAction();
	}

	return s_mapAction;
}

void MapAction::setIsUpdate(bool value)
{
	isUpdate = value;
}

void MapAction::setData(int state, Vec2 pos, std::string mapId, long long actorId)
{
	m_iState = state;
	targetPos = pos;
	targetMapId = mapId;
	targetActorId = actorId;
	isUpdate = true;
}

bool MapAction::isFinishedMove()
{
	if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	{
		Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
		Vec2 cp_target = targetPos;
		//CCLOG("%f",sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)));
		if (cp_role.getDistance(cp_target) < 96)
		{
			GameView::getInstance()->myplayer->changeAction(ACT_STAND);
			if (GameView::getInstance()->getGameScene()->getActor(targetActorId) != NULL)
			{
				GameView::getInstance()->myplayer->setLockedActorId(targetActorId);
			}
			
			return true;
		}
		else 
		{
			return false;
		}
	}
	else
	{
		return false;
	}
	
}

void MapAction::update()
{
	if(!isUpdate)
	{
		return;
	}
	switch(m_iState)
	{
	case state_idle:
		break;
	case state_moveto_npc:
		if(isFinishedMove())
		{
			auto mainScene = (UIScene*)GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1129, (void *)targetActorId);
			}
			m_iState = state_idle;
			isUpdate = false;
		}
		break;
	case state_moveto_monster:
		if(isFinishedMove())
		{
			/*if(GameView::getInstance()->myplayer->selectEnemy(380))
 			{
 				BaseFighter* bf = dynamic_cast<BaseFighter*>(GameView::getInstance()->myplayer->getLockedActor());
 				CCAssert(bf != NULL, "basefighter should not be null");
 
 				MyPlayerCommandAttack* cmd = new MyPlayerCommandAttack();
 				cmd->skillId = GameView::getInstance()->myplayer->getDefaultSkill();
 				cmd->skillProcessorId = cmd->skillId;
 				cmd->sendAttackRequest = true;
 				cmd->targetId = bf->getRoleId();
 
 				GameView::getInstance()->myplayer->getSimpleAI()->startKill(bf->getRoleId());
 	
 				GameView::getInstance()->myplayer->setNextCommand(cmd, true);
			}*/
			MyPlayerAIConfig::setAutomaticSkill(1);
			MyPlayerAIConfig::enableRobot(true);
			GameView::getInstance()->myplayer->getMyPlayerAI()->start();

			if (GameView::getInstance()->getGameScene())
			{
				if (GameView::getInstance()->getMainUIScene())
				{
					auto guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
					if (guide_)
					{
						if (guide_->getBtnHangUp())
						{
							guide_->getBtnHangUp()->loadTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
						}
					}
				}
			}
			m_iState = state_idle;
			isUpdate = false;
		}
		break;
	default:
		break;
	}
}