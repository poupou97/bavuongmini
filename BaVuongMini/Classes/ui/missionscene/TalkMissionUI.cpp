#include "TalkMissionUI.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../../legend_engine/GameWorld.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "MissionManager.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "cocostudio\CCSGUIReader.h"


TalkMissionUI::TalkMissionUI()
:targetNpcId(0)
{
}


TalkMissionUI::~TalkMissionUI()
{
	delete curMissionInfo;
}


void TalkMissionUI::update( float dt )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI) != NULL)
	{
		if(targetNpcId == 0)
			return;

		if (GameView::getInstance()->missionManager->isOutOfMissionTalkArea(this->getTargetNpcId()))
		{
			this->closeAnim();
		}
	}
}

int TalkMissionUI::getTargetNpcId()
{
	return targetNpcId;
}

TalkMissionUI* TalkMissionUI::create(MissionInfo * missionInfo)
{
	auto talkMissionUI = new TalkMissionUI();
	if (talkMissionUI && talkMissionUI->init(missionInfo))
	{
		talkMissionUI->autorelease();
		return talkMissionUI;
	}
	CC_SAFE_DELETE(talkMissionUI);
	return NULL;

}

bool TalkMissionUI::init(MissionInfo * missionInfo)
{
	if (UIScene::init())
	{
		curMissionInfo = new MissionInfo();
		curMissionInfo->CopyFrom(*missionInfo);

		if (missionInfo->action().has_moveto())
		{
			if (missionInfo->action().moveto().has_targetnpc())
			{
				targetNpcId = missionInfo->action().moveto().targetnpc().npcid();
			}
		}

		auto winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();
		//���UI
		auto ppanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/renwub_1.json");
		ppanel->setAnchorPoint(Vec2(0,0));
		ppanel->getVirtualRenderer()->setContentSize(Size(350, 400));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(TalkMissionUI::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);

		auto Button_enter = (Button*)Helper::seekWidgetByName(ppanel,"Button_lingqu");
		Button_enter->setTouchEnabled(true);
		Button_enter->addTouchEventListener(CC_CALLBACK_2(TalkMissionUI::EnterEvent, this));
		Button_enter->setPressedActionEnabled(true);

		// load animation
		auto npcInfo = GameWorld::NpcInfos[targetNpcId];
		std::string animFileName = "res_ui/npc/";
		if (targetNpcId == 0)//�û�����NPC
		{
			animFileName.append("cz");
		}
		else
		{
			if (npcInfo)
			{
				animFileName.append(npcInfo->icon.c_str());
			}
			else
			{
				animFileName.append("cz");
			}
		}
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/npc/caiwenji.anm";
		auto m_pAnim = CCLegendAnimation::create(animFileName);
		if (m_pAnim)
		{
			m_pAnim->setPlayLoop(true);
			m_pAnim->setReleaseWhenStop(false);
			m_pAnim->setScale(1.0f);
			m_pAnim->setPosition(Vec2(33,291));
			//m_pAnim->setPlaySpeed(.35f);
			addChild(m_pAnim);
		}

		auto sprite_anmFrame = Sprite::create("res_ui/renwua/tietu2.png");
		sprite_anmFrame->setAnchorPoint(Vec2(0.5,0.5f));
		sprite_anmFrame->setPosition(Vec2(115,335));
		addChild(sprite_anmFrame);
		auto ImageView_name = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_mingzi");

		FontDefinition strokeShaodwTextDef;
		strokeShaodwTextDef._fontName = std::string("res_ui/font/simhei.ttf");
		Color3B tintColorGreen   =  { 54, 92, 7 };
		strokeShaodwTextDef._fontFillColor   = tintColorGreen;   // �������ɫ
		strokeShaodwTextDef._fontSize = 22;   // �����С
		// 		strokeShaodwTextDef.m_stroke.m_strokeEnabled = true;   // �Ƿ񹴱
		// 		strokeShaodwTextDef.m_stroke.m_strokeColor   = blackColor;   // ߹�����ɫ����ɫ
		// 		strokeShaodwTextDef.m_stroke.m_strokeSize    = 2.0;   // ���ߵĴ�С
		/*Label* l_nameLabel = Label::createWithTTFWithFontDefinition(npcInfo->npcName.c_str(), strokeShaodwTextDef);
		Color3B shadowColor = Color3B(0,0,0);   // black
		l_nameLabel->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		l_nameLabel->setAnchorPoint(Vec2(0.5,0.5f));
		l_nameLabel->setPosition(Vec2(184,359));
		addChild(l_nameLabel);*/
		Label* l_nameLabel;
		if (targetNpcId == 0) //û�����NPC
		{
			l_nameLabel = Label::createWithTTF(StringDataManager::getString("mission_teachNpcName"),"res_ui/font/simhei.ttf",18);
		}
		else
		{
			if (npcInfo)
			{
				l_nameLabel = Label::createWithTTF(npcInfo->npcName.c_str(),"res_ui/font/simhei.ttf",18);
			}
			else
			{
				l_nameLabel = Label::createWithTTF(StringDataManager::getString("mission_teachNpcName"),"res_ui/font/simhei.ttf",18);
			}
		}
		l_nameLabel->setAnchorPoint(Vec2(0.5,0.5f));
		l_nameLabel->setPosition(Vec2(207,307));
		auto shadowColor = Color4B::BLACK;   // black
		l_nameLabel->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		addChild(l_nameLabel);

		//description
		std::string mission_description = "";
// 		if (missionInfo->has_tip())
// 		{
// 			mission_description.append(missionInfo->tip().dialog());
// 		}
// 		else
// 		{
// 			mission_description.append(missionInfo->dialog());
// 		}
		mission_description.append(missionInfo->action().dialog());
// 		Label * label_des = Label::createWithTTF(mission_description.c_str(),"res_ui/font/simhei.ttf",18,Size(285,0),TextHAlignment::LEFT,TextVAlignment::TOP);
// 		label_des->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
// 		Size desSize = label_des->getContentSize();
// 		int height = desSize.height;
		auto label_des = CCRichLabel::createWithString(mission_description.c_str(),Size(285, 0),this,NULL,0,18,5);
		auto desSize = label_des->getContentSize();
		int height = desSize.height;
		//ScrollView_description
		auto m_contentScrollView = cocos2d::extension::ScrollView::create(Size(290,99));
		m_contentScrollView->setViewSize(Size(290, 99));
		m_contentScrollView->setIgnoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(TableView::Direction::VERTICAL);
		m_contentScrollView->setAnchorPoint(Vec2(0,0));
		m_contentScrollView->setPosition(Vec2(30,185));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_pLayer->addChild(m_contentScrollView);
		if (height > 99)
		{
			m_contentScrollView->setContentSize(Size(290,height));
			m_contentScrollView->setContentOffset(Vec2(0,99-height));
		}
		else
		{
			m_contentScrollView->setContentSize(Size(290,99));
		}
		m_contentScrollView->setClippingToBounds(true);

		m_contentScrollView->addChild(label_des);
		label_des->setIgnoreAnchorPointForPosition(false);
		label_des->setAnchorPoint(Vec2(0,1));
		label_des->setPosition(Vec2(3,m_contentScrollView->getContentSize().height-3));


		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(350,400));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(TalkMissionUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(TalkMissionUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(TalkMissionUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(TalkMissionUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;

}


void TalkMissionUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	////this->setTouchEnabled(true);
	//this->setContentSize(Size(800,480));


}

void TalkMissionUI::onExit()
{
	UIScene::onExit();
}


bool TalkMissionUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	Vec2 location = touch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	return false;
}

void TalkMissionUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void TalkMissionUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void TalkMissionUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void TalkMissionUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void TalkMissionUI::EnterEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(7012, (void *)getTargetNpcId());
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}
