#include "MissionRewardGoodsItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/backpackscene/EquipmentItem.h"
#include "AppMacros.h"

MissionRewardGoodsItem::MissionRewardGoodsItem()
{
}


MissionRewardGoodsItem::~MissionRewardGoodsItem()
{
	delete curGoodsInfo;
}

MissionRewardGoodsItem * MissionRewardGoodsItem::create( GoodsInfo* goods,int num )
{
	auto widget = new MissionRewardGoodsItem();
	if (widget && widget->initWithGoods(goods,num))
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}

bool MissionRewardGoodsItem::initWithGoods( GoodsInfo* goods,int num )
{
	if (Widget::init())
	{
		curGoodsInfo = new GoodsInfo();
		curGoodsInfo->CopyFrom(*goods);
		/*********************�ж�װ������ɫ***************************/
		std::string frameColorPath;
		if (curGoodsInfo->quality() == 1)
		{
			frameColorPath = EquipWhiteFramePath;
		}
		else if (curGoodsInfo->quality() == 2)
		{
			frameColorPath = EquipGreenFramePath;
		}
		else if (curGoodsInfo->quality() == 3)
		{
			frameColorPath = EquipBlueFramePath;
		}
		else if (curGoodsInfo->quality() == 4)
		{
			frameColorPath = EquipPurpleFramePath;
		}
		else if (curGoodsInfo->quality() == 5)
		{
			frameColorPath = EquipOrangeFramePath;
		}
		else
		{
			frameColorPath = EquipVoidFramePath;
		}

		auto Btn_goodsItemFrame = Button::create();
		Btn_goodsItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_goodsItemFrame->setTouchEnabled(true);
		Btn_goodsItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_goodsItemFrame->setPosition(Vec2(Btn_goodsItemFrame->getContentSize().width/2,Btn_goodsItemFrame->getContentSize().height/2));
		Btn_goodsItemFrame->addTouchEventListener(CC_CALLBACK_2(MissionRewardGoodsItem::GoodItemEvent, this));
		Btn_goodsItemFrame->setPressedActionEnabled(true);
		this->addChild(Btn_goodsItemFrame);

		auto uiiImageView_goodsItem = ImageView::create();
		if (strcmp(goods->icon().c_str(),"") == 0)
		{
			uiiImageView_goodsItem->loadTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(goods->icon().c_str());
			_path.append(".png");
			uiiImageView_goodsItem->loadTexture(_path.c_str());
		}
		uiiImageView_goodsItem->setScale(0.9f);
		uiiImageView_goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		uiiImageView_goodsItem->setPosition(Vec2(0,0));
		Btn_goodsItemFrame->addChild(uiiImageView_goodsItem);

		
		char s_num[20];
		sprintf(s_num,"%d",num);
		auto l_num = Label::createWithTTF(s_num, APP_FONT_NAME, 13);
		l_num->setAnchorPoint(Vec2(1.0f,0));
		l_num->setPosition(Vec2(Btn_goodsItemFrame->getContentSize().width/2-10,7-Btn_goodsItemFrame->getContentSize().height/2));
		Btn_goodsItemFrame->addChild(l_num);

		if (goods->binding() == 1)
		{
			auto ImageView_bound = ImageView::create();
			ImageView_bound->loadTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(Vec2(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(Vec2(8-Btn_goodsItemFrame->getContentSize().width/2,55-Btn_goodsItemFrame->getContentSize().height/2));
			Btn_goodsItemFrame->addChild(ImageView_bound);
		}

		//�����װ��
		if(goods->has_equipmentdetail())
		{
			if (goods->equipmentdetail().gradelevel() > 0)  //����ȼ�
			{
				std::string ss_gradeLevel = "+";
				char str_gradeLevel [10];
				sprintf(str_gradeLevel,"%d",goods->equipmentdetail().gradelevel());
				ss_gradeLevel.append(str_gradeLevel);
				auto Lable_gradeLevel = Label::createWithTTF(ss_gradeLevel.c_str(), APP_FONT_NAME, 13);
				Lable_gradeLevel->setAnchorPoint(Vec2(1.0f,1.0f));
				Lable_gradeLevel->setPosition(Vec2(Btn_goodsItemFrame->getContentSize().width/2-10,Btn_goodsItemFrame->getContentSize().height/2-7));
				Lable_gradeLevel->setName("Lable_gradeLevel");
				Btn_goodsItemFrame->addChild(Lable_gradeLevel);
			}

			if (goods->equipmentdetail().starlevel() > 0)  //�Ǽ�
			{
				char str_starLevel [10];
				sprintf(str_starLevel,"%d",goods->equipmentdetail().starlevel());
				auto Lable_starLevel = Label::createWithTTF(str_starLevel, APP_FONT_NAME, 13);
				Lable_starLevel->setAnchorPoint(Vec2(0,0));
				Lable_starLevel->setPosition(Vec2(8-Btn_goodsItemFrame->getContentSize().width/2,Btn_goodsItemFrame->getContentSize().height/2-6));
				Lable_starLevel->setName("Lable_starLevel");
				Btn_goodsItemFrame->addChild(Lable_starLevel);

				auto ImageView_star = ImageView::create();
				ImageView_star->loadTexture("res_ui/star_on.png");
				ImageView_star->setAnchorPoint(Vec2(0,0));
				ImageView_star->setPosition(Vec2(Lable_starLevel->getContentSize().width,2));
				ImageView_star->setVisible(true);
				ImageView_star->setScale(0.5f);
				Lable_starLevel->addChild(ImageView_star);
			}
		}

		//this->setTouchEnabled(true);
		this->setContentSize(Size(63,63));

		return true;
	}
	return false;
}

void MissionRewardGoodsItem::GoodItemEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto size = Director::getInstance()->getVisibleSize();
		auto goodsItemInfoBase = GoodsItemInfoBase::create(curGoodsInfo, GameView::getInstance()->EquipListItem, 0);
		goodsItemInfoBase->setIgnoreAnchorPointForPosition(false);
		goodsItemInfoBase->setAnchorPoint(Vec2(0.5f, 0.5f));
		//goodsItemInfoBase->setPosition(Vec2(size.width/2,size.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}
