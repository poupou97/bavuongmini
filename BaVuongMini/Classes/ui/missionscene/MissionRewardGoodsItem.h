
#ifndef _MISSIONSCENE_MISSIONREWARDGOODSITEM_H_
#define _MISSIONSCENE_MISSIONREWARDGOODSITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GoodsInfo;

class MissionRewardGoodsItem :public ui::Widget
{
public:
	MissionRewardGoodsItem();
	~MissionRewardGoodsItem();

	static MissionRewardGoodsItem * create(GoodsInfo* goods,int num);
	bool initWithGoods(GoodsInfo* goods,int num);

	void GoodItemEvent(Ref *pSender, Widget::TouchEventType type);

private:
	GoodsInfo * curGoodsInfo;
};

#endif

