#include "MissionTalkWithNpc.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../AppMacros.h"
#include "../../messageclient/protobuf/NPCMessage.pb.h"
#include "../../messageclient/element/CNpcDialog.h"
#include "../extensions/CCRichLabel.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../legend_engine/LegendLevel.h"
#include "../../gamescene_state/role/FunctionNPC.h"
#include "../../GameView.h"
#include "../../ui/missionscene/MissionManager.h"
#include "HandleMission.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../extensions/UITab.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../../legend_engine/GameWorld.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "cocostudio\CCSGUIReader.h"


MissionTalkWithNpc::MissionTalkWithNpc()
{
}


MissionTalkWithNpc::~MissionTalkWithNpc()
{
}


void MissionTalkWithNpc::update( float dt )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc) != NULL)
	{
		if (GameView::getInstance()->missionManager->isOutOfMissionTalkArea(this->getCurNpcId()))
		{
			this->closeAnim();
		}
	}
}


MissionTalkWithNpc* MissionTalkWithNpc::create(FunctionNPC* fcNpc)
{
	auto talkWithNpc = new MissionTalkWithNpc();
	if(talkWithNpc && talkWithNpc->init(fcNpc))
	{
		talkWithNpc->autorelease();
		return talkWithNpc;
	}
	CC_SAFE_DELETE(talkWithNpc);
	return NULL;
}

bool MissionTalkWithNpc::init(FunctionNPC* fcNpc)
{
	if (UIScene::init())
	{
		curNpcId = fcNpc->getRoleId();
		curFunctionNPC = fcNpc;
		auto winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();
		//加载UI
// 		if(LoadSceneLayer::TalkWithNpcLayer->getWidgetParent() != NULL)
// 		{
// 			LoadSceneLayer::TalkWithNpcLayer->removeFromParentAndCleanup(false);
// 		}
// 		Layout *ppanel = LoadSceneLayer::TalkWithNpcLayer;
// 		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
// 		ppanel->getValidNode()->setContentSize(Size(350, 420));
// 		ppanel->setPosition(Vec2::ZERO);
// 		ppanel->setScale(1.0f);
// 		ppanel->setTouchEnabled(true);
// 		m_pLayer->addChild(ppanel);
// 
// 		Layout * panel_mission = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_renwu");
// 		panel_mission->setVisible(false);

		auto ppanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/talkWithNpc_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->setPosition(Vec2::ZERO);	
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto npcInfo = GameWorld::NpcInfos[fcNpc->getRoleId()];

		auto button_close = (Button *)Helper::seekWidgetByName(ppanel,"Button_close");
		button_close->setTouchEnabled(true);
		button_close->addTouchEventListener(CC_CALLBACK_2(MissionTalkWithNpc::CloseEvent, this));
		ImageView_headImage = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_touxiang");;
		ImageView_name = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_mingzi");

		// head anm
		std::string animFileName = "res_ui/npc/";
		animFileName.append(npcInfo->icon.c_str());
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/npc/caiwenji.anm";
		auto m_pAnim = CCLegendAnimation::create(animFileName);
		if (m_pAnim)
		{
			m_pAnim->setPlayLoop(true);
			m_pAnim->setReleaseWhenStop(false);
			m_pAnim->setScale(1.0f);
			m_pAnim->setPosition(Vec2(33,312));
			//m_pAnim->setPlaySpeed(.35f);
			addChild(m_pAnim);
		}

		auto sprite_anmFrame = Sprite::create("res_ui/renwua/tietu2.png");
		sprite_anmFrame->setAnchorPoint(Vec2(0.5,0.5f));
		sprite_anmFrame->setPosition(Vec2(116,356));
		addChild(sprite_anmFrame);


		auto ImageView_name = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_mingzi");

		FontDefinition strokeShaodwTextDef;
		strokeShaodwTextDef._fontName = std::string(APP_FONT_NAME);
		Color3B tintColorGreen   =  { 54, 92, 7 };
		strokeShaodwTextDef._fontFillColor   = tintColorGreen;   // 字体颜色
		strokeShaodwTextDef._fontSize = 22;   // 字体大小
		// 		strokeShaodwTextDef.m_stroke.m_strokeEnabled = true;   // 是否勾边
		// 		strokeShaodwTextDef.m_stroke.m_strokeColor   = blackColor;   // 勾边颜色，黑色
		// 		strokeShaodwTextDef.m_stroke.m_strokeSize    = 2.0;   // 勾边的大小
		/*Label* l_nameLabel = Label::createWithTTFWithFontDefinition(npcInfo->npcName.c_str(),strokeShaodwTextDef);
		Color3B shadowColor = Color3B(0,0,0);   // black
		l_nameLabel->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		l_nameLabel->setAnchorPoint(Vec2(0.5,0.5f));
		l_nameLabel->setPosition(Vec2(184,359));
		addChild(l_nameLabel);*/
		auto l_nameLabel = Label::createWithTTF(npcInfo->npcName.c_str(),APP_FONT_NAME,22);
		l_nameLabel->setAnchorPoint(Vec2(0.5,0.5f));
		l_nameLabel->setPosition(Vec2(207,327));
		auto shadowColor = Color4B::BLACK;   // black
		l_nameLabel->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		addChild(l_nameLabel);

		//richLabel_description
// 		Label * label_des = Label::createWithTTF(npcInfo->welcome.c_str(),APP_FONT_NAME,18,Size(285,0),TextHAlignment::LEFT,TextVAlignment::TOP);
// 		label_des->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
// 		Size desSize = label_des->getContentSize();
// 		int height = desSize.height;
		auto label_des = CCRichLabel::createWithString(npcInfo->welcome.c_str(),Size(285, 0),this,NULL,0,18,5);
		auto desSize = label_des->getContentSize();
		int height = desSize.height;
		//ScrollView_description
		auto m_contentScrollView = cocos2d::extension::ScrollView::create(Size(290,99));
		m_contentScrollView->setViewSize(Size(290, 99));
		m_contentScrollView->setIgnoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		m_contentScrollView->setAnchorPoint(Vec2(0,0));
		m_contentScrollView->setPosition(Vec2(30,205));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_pLayer->addChild(m_contentScrollView);
		if (height > 99)
		{
			m_contentScrollView->setContentSize(Size(290,height));
			m_contentScrollView->setContentOffset(Vec2(0,99-height));
		}
		else
		{
			m_contentScrollView->setContentSize(Size(290,99));
		}
		m_contentScrollView->setClippingToBounds(true);

		m_contentScrollView->addChild(label_des);
		label_des->setIgnoreAnchorPointForPosition(false);
		label_des->setAnchorPoint(Vec2(0,1));
		label_des->setPosition(Vec2(3,m_contentScrollView->getContentSize().height-3));


		m_tableView = TableView::create(this,Size(290,165));
		//m_tableView->setPressedActionEnabled(true);
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0,0));
		m_tableView->setPosition(Vec2(35,25));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		this->addChild(m_tableView);
		
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(350,420));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(MissionTalkWithNpc::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(MissionTalkWithNpc::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(MissionTalkWithNpc::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(MissionTalkWithNpc::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}


void MissionTalkWithNpc::onEnter()
{
	UIScene::onEnter();

	auto action = Sequence::create(
		ScaleTo::create(0.15f*1/2,1.1f),
		ScaleTo::create(0.15f*1/3,1.0f),
		NULL);

	runAction(action);
}

void MissionTalkWithNpc::onExit()
{
	UIScene::onExit();
}


bool MissionTalkWithNpc::onTouchBegan(Touch *touch, Event * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	Vec2 location = touch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	return false;
}

void MissionTalkWithNpc::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void MissionTalkWithNpc::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void MissionTalkWithNpc::onTouchMoved(Touch *touch, Event * pEvent)
{
}



Size MissionTalkWithNpc::tableCellSizeForIndex(TableView *table, unsigned int idx)
{
	return Size(270,42);
}

TableViewCell* MissionTalkWithNpc::tableCellAtIndex(TableView *table, ssize_t  idx)
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell=new TableViewCell();
	cell->autorelease();

	auto pCellBg = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_0.png");
	pCellBg->setContentSize(Size(270,39));
	pCellBg->setCapInsets(Rect(38,20,1,1));
	pCellBg->setAnchorPoint(Vec2(0, 0));
	pCellBg->setPosition(Vec2(0, 0));
	cell->addChild(pCellBg);

	//mission type
	std::string str_missionType;
	if (curFunctionNPC->npcMissionList.at(idx)->missionpackagetype() == 1)
	{
		str_missionType.append(StringDataManager::getString("mission_zhu"));
	}
	else if(curFunctionNPC->npcMissionList.at(idx)->missionpackagetype() == 2)
	{
		str_missionType.append(StringDataManager::getString("mission_zhi"));
	}
	else if(curFunctionNPC->npcMissionList.at(idx)->missionpackagetype() == 7)
	{
		str_missionType.append(StringDataManager::getString("mission_jia"));
	}
	else if(curFunctionNPC->npcMissionList.at(idx)->missionpackagetype() == 12)
	{
		str_missionType.append(StringDataManager::getString("mission_xuan"));
	}
	else
	{
		str_missionType.append(StringDataManager::getString("mission_ri"));
	}

	str_missionType.append(curFunctionNPC->npcMissionList.at(idx)->missionname());
	//Label * pMissionName = Label::createWithTTF(curMissionInfo->description().c_str(),"Arial",16);
	auto pMissionName = Label::createWithTTF(str_missionType.c_str(),APP_FONT_NAME,16);
	pMissionName->setAnchorPoint(Vec2(0.5f,0.5f));
	pMissionName->setPosition(Vec2(pCellBg->getContentSize().width/2,pCellBg->getContentSize().height/2));
	auto shadowColor = Color4B::BLACK;   // black
	pMissionName->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
	cell->addChild(pMissionName);

	return cell;

}


ssize_t MissionTalkWithNpc::numberOfCellsInTableView(TableView *table)
{
	return curFunctionNPC->npcMissionList.size();
}

void MissionTalkWithNpc::tableCellTouched(TableView* table, TableViewCell* cell)
{
	//关闭自己
	this->closeAnim();
	//弹出HandelMission
	GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*curFunctionNPC->npcMissionList.at(cell->getIdx()));
	//GameView::getInstance()->missionManager->addMission(curFunctionNPC->npcMissionList.at(cell->getIdx()));
	auto mainScene = (UIScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene->getChildByTag(kTagHandleMission) == NULL)
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow))
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1131, this);
			GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow)->removeFromParent();
		}
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc))
		{
			GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc)->removeFromParent();
		}
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI))
		{
			GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI)->removeFromParent();
		}

		auto winSize = Director::getInstance()->getVisibleSize();
		auto getMission = HandleMission::create(curFunctionNPC->npcMissionList.at(cell->getIdx()));
		mainScene->addChild(getMission,0,kTagHandleMission);
		getMission->setIgnoreAnchorPointForPosition(false);
		getMission->setAnchorPoint(Vec2(0,0.5f));
		getMission->setPosition(Vec2(0,winSize.height/2));
	}
}

void MissionTalkWithNpc::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void MissionTalkWithNpc::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

void MissionTalkWithNpc::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

int MissionTalkWithNpc::getCurNpcId()
{
	return curNpcId;
}



