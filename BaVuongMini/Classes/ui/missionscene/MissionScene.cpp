#include "MissionScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"
#include "../extensions/UITab.h"
#include "MissionTalkWithNpc.h"
#include "HandleMission.h"
#include "GameView.h"
#include "MissionManager.h"
#include "../extensions/CCRichLabel.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../../messageclient/element/MissionInfo.h"
#include "RewardItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../ui/backpackscene/EquipmentItem.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/GameUtils.h"

#define  M_SCROLLVIEWTAG 44
#define  SelectImageTag 458

using namespace CocosDenshion;

#define  MISSIONSCENE_TABALEVIEW_HIGHLIGHT_TAG 85

MissionScene::MissionScene():
selectCellIdx(0)
{
}
	


MissionScene::~MissionScene()
{
	delete curMissionInfo;
}

MissionScene* MissionScene::create()
{
	auto missionScene = new MissionScene();
	if(missionScene && missionScene->init())
	{
		missionScene->autorelease();
		return missionScene;
	}
	CC_SAFE_DELETE(missionScene);
	return NULL;
}

bool MissionScene::init()
{
	if (UIScene::init())
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		curMissionInfo = new MissionInfo();

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(800, 480));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		//加载UI
		if(LoadSceneLayer::MissionSceneLayer->getParent() != NULL)
		{
			LoadSceneLayer::MissionSceneLayer->removeFromParentAndCleanup(false);
		}
		auto ppanel = LoadSceneLayer::MissionSceneLayer;
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->getVirtualRenderer()->setContentSize(Size(800, 480));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		const char * secondStr = StringDataManager::getString("UIName_ren_1");
		const char * thirdStr = StringDataManager::getString("UIName_wu_2");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		u_layer->addChild(atmature);

		//关闭按钮
		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(MissionScene::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);
		//放弃任务
		Button_giveUpMission = (Button*)Helper::seekWidgetByName(ppanel,"Button_fangqirenwu");
		Button_giveUpMission->setTouchEnabled(true);
		Button_giveUpMission->setPressedActionEnabled(true);
		Button_giveUpMission->addTouchEventListener(CC_CALLBACK_2(MissionScene::GiveUpMissionEvent, this));
		Button_giveUpMission->setVisible(false);
		//自动寻路
		Button_autoPathFinder = (Button*)Helper::seekWidgetByName(ppanel,"Button_zidongxunlu");
		Button_autoPathFinder->setTouchEnabled(true);
		Button_autoPathFinder->setPressedActionEnabled(true);
		Button_autoPathFinder->addTouchEventListener(CC_CALLBACK_2(MissionScene::AutoPathFindEvent, this));
		
		l_autoPathFinder = (Text*)Helper::seekWidgetByName(ppanel,"Label_zidongxunlu");

		//maintab
		const char * normalImage = "res_ui/tab_1_off.png";
		const char * selectImage = "res_ui/tab_1_on.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_1_on.png";
		char * stateNames[] ={"已接","可接"};
		mainTab = UITab::createWithText(2,normalImage,selectImage,finalImage,stateNames,HORIZONTAL,9);
		mainTab->setAnchorPoint(Vec2(0,0));
		mainTab->setPosition(Vec2(68,412));
		mainTab->setHighLightImage((char * )highLightImage);
		mainTab->setDefaultPanelByIndex(0);
		mainTab->addIndexChangedEvent(this,coco_indexchangedselector(MissionScene::MainTabIndexChangedEvent));
		mainTab->setPressedActionEnabled(true);
		u_layer->addChild(mainTab);

		m_tableView = TableView::create(this,Size(249,352));
		//m_tableView ->setSelectedEnable(true);   // 支持选中状态的显示
		//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 25, 1, 1), Vec2(0,0));   // 设置九宫格图片
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0,0));
		m_tableView->setPosition(Vec2(77,47));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//m_tableView->setPressedActionEnabled(true);
		u_layer->addChild(m_tableView);

		auto Layer_tableViewFrame = Layer::create();
		Layer_tableViewFrame->setIgnoreAnchorPointForPosition(false);
		Layer_tableViewFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Layer_tableViewFrame->setContentSize(Size(800, 480));
		Layer_tableViewFrame->setPosition(Vec2::ZERO);
		Layer_tableViewFrame->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(Layer_tableViewFrame);
// 		ImageView * imageView_tableViewFrame = ImageView::create();
// 		imageView_tableViewFrame->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		imageView_tableViewFrame->setScale9Enabled(true);
// 		imageView_tableViewFrame->setContentSize(Size(261,368));
// 		imageView_tableViewFrame->setCapInsets(Rect(30,30,1,1));
// 		imageView_tableViewFrame->setAnchorPoint(Vec2(0,0));
// 		imageView_tableViewFrame->setPosition(Vec2(65,39));
// 		Layer_tableViewFrame->addChild(imageView_tableViewFrame);

		l_haveNoMissionToGet = Label::createWithTTF(StringDataManager::getString("mission_haveNoMissionForGet"), APP_FONT_NAME, 18);
		l_haveNoMissionToGet->setAnchorPoint(Vec2(0.5f,0.5f));
		l_haveNoMissionToGet->setPosition(Vec2(196,234));
		Layer_tableViewFrame->addChild(l_haveNoMissionToGet);
		l_haveNoMissionToGet->setVisible(false);

		l_haveNoMissionGeted = Label::createWithTTF(StringDataManager::getString("mission_haveNoMissionGeted"), APP_FONT_NAME, 18);
		l_haveNoMissionGeted->setAnchorPoint(Vec2(0.5f,0.5f));
		l_haveNoMissionGeted->setPosition(Vec2(196,234));
		Layer_tableViewFrame->addChild(l_haveNoMissionGeted);
		l_haveNoMissionGeted->setVisible(false);

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);

		//已接任务
		this->updataCurDataSourceByMissionState(1);
		//reload
		m_tableView->reloadData();
		//设置默认
		if (this->curMissionDataSource.size()>0)
		{
			curMissionInfo->CopyFrom(*this->curMissionDataSource.at(0));
			//Button_giveUpMission->setVisible(true);
			m_tableView->cellAtIndex(0);
			selectCellIdx = 0;
		}
		
		this->ReloadMissionInfoData();

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(MissionScene::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(MissionScene::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(MissionScene::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(MissionScene::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
		
		return true;
	}
	return false;
}

void MissionScene::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void MissionScene::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}


bool MissionScene::onTouchBegan(Touch *touch, Event * pEvent)
{
	return this->resignFirstResponder(touch,this,false);
}

void MissionScene::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void MissionScene::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void MissionScene::onTouchMoved(Touch *touch, Event * pEvent)
{
}


void MissionScene::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void MissionScene::GiveUpMissionEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->curMissionDataSource.size()>0)
		{
			if (curMissionInfo->missionstate()>0)
			{
				GameUtils::playGameSound(MISSION_CANCEL, 2, false);
				//放弃任务
				GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*this->curMissionInfo);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(7009, this);
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MissionScene::AutoPathFindEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->curMissionDataSource.size()>0)
		{
			GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*this->curMissionInfo);
			GameView::getInstance()->missionManager->addMission(curMissionInfo);

			this->closeAnim();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MissionScene::MainTabIndexChangedEvent(Ref* pSender)
{
	switch((int)mainTab->getCurrentIndex())
	{
	case 0 :
		//Button_giveUpMission->setVisible(false);
		this->updataCurDataSourceByMissionState(1);
		//reload
		m_tableView->reloadData();
		//设置默认
		if (this->curMissionDataSource.size()>0)
		{
			curMissionInfo->CopyFrom(*this->curMissionDataSource.at(0));
			m_tableView->cellAtIndex(0);
			selectCellIdx = 0;
		}
		
		this->ReloadMissionInfoData();
		break;
	case 1 :
		//Button_giveUpMission->setVisible(true);
		this->updataCurDataSourceByMissionState(0);
		m_tableView->reloadData();
		//设置默认
		if (this->curMissionDataSource.size()>0)
		{
			curMissionInfo->CopyFrom(*(this->curMissionDataSource.at(0)));
			m_tableView->cellAtIndex(0);
			selectCellIdx = 0;
		}
		
		this->ReloadMissionInfoData();
		break;
	}
}

Size MissionScene::tableCellSizeForIndex(TableView *table, unsigned int idx)
{
	return Size(242, 60);
}

TableViewCell* MissionScene::tableCellAtIndex(TableView *table, ssize_t  idx)
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell=new TableViewCell();
	cell->autorelease();

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	auto missionInfo = this->curMissionDataSource.at(idx);

	auto sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang2_new.png");
	sprite_frame->setContentSize(Size(242,60));
	sprite_frame->setCapInsets(Rect(45,45,1,1));
	sprite_frame->setAnchorPoint(Vec2(0,0));
	sprite_frame->setPosition(Vec2(0,0));
	cell->addChild(sprite_frame);
	
	std::string str_missionType;
	if (missionInfo->missionpackagetype() == 1)
	{
		str_missionType = StringDataManager::getString("mission_zhu");
	}
	else if(missionInfo->missionpackagetype() == 2)
	{
		str_missionType = StringDataManager::getString("mission_zhi");
	}
	else if(missionInfo->missionpackagetype() == 7)
	{
		str_missionType = StringDataManager::getString("mission_jia");
	}
	else if(missionInfo->missionpackagetype() == 12)
	{
		str_missionType = StringDataManager::getString("mission_xuan");
	}
	else
	{
		str_missionType = StringDataManager::getString("mission_ri");
	}

	str_missionType.append(missionInfo->missionname().c_str());
	//mission name
	auto pMissionName = Label::createWithTTF(str_missionType.c_str(),APP_FONT_NAME,16);
	pMissionName->setAnchorPoint(Vec2(0.f,0.5f));
	pMissionName->setPosition(Vec2(40,sprite_frame->getContentSize().height/2));
	auto shadowColor = Color4B::BLACK;   // black
	pMissionName->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
	sprite_frame->addChild(pMissionName);

	switch(missionInfo->missionstate())
	{
	case dispatched : //已分配  
		pMissionName->setColor(Color3B(254,254,51));                       //淡黄色
		break;
	case accepted : //已接受
		pMissionName->setColor(Color3B(0,255,240));                   //蓝色
		break;
	case done ://已完成
		pMissionName->setColor(Color3B(90,255,0));                     //绿色
		break;
		//case failed ://已失败
		//	break;
	case canceled ://已取消
		break;
	case submitted ://已提交
		break;
	}

	return cell;

}

ssize_t MissionScene::numberOfCellsInTableView(TableView *table)
{
	return this->curMissionDataSource.size();
}

void MissionScene::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void MissionScene::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void MissionScene::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void MissionScene::tableCellTouched(TableView* table, TableViewCell* cell)
{
	selectCellIdx = cell->getIdx();
	curMissionInfo->CopyFrom(*this->curMissionDataSource.at(cell->getIdx()));
	this->ReloadMissionInfoData();

	Button_giveUpMission->setVisible(false);
	if (curMissionInfo->missionpackagetype() >= 3)
	{
		if (curMissionInfo->missionstate() > 0)
		{
			Button_giveUpMission->setVisible(true);
		}
	}

	if (curMissionInfo->action().action() == openSpecifiedUI)  //打开界面
	{
		if (curMissionInfo->action().specifiedui().id()  == kTagMissionScene)  
		{
			Button_autoPathFinder->setVisible(false);
		}
		else
		{
			Button_autoPathFinder->setVisible(true);
		}
	}
	else
	{
		Button_autoPathFinder->setVisible(true);
	}
}
void MissionScene::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void MissionScene::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

void MissionScene::ReloadMissionInfoData()
{
	if (u_layer->getChildByTag(M_SCROLLVIEWTAG) != NULL)
	{
		u_layer->getChildByTag(M_SCROLLVIEWTAG)->removeFromParent();
	}

	if (curMissionDataSource.size() == 0)
		return;

	if (!curMissionInfo->has_missionid())
		return;

	int scroll_height = 0;
	int _space = 15;
	//name
	sprite_name = Sprite::create("res_ui/renwubb/renwumingcheng.png");	
	label_name = Label::createWithTTF(curMissionInfo->missionname().c_str(),APP_FONT_NAME,18,Size(270, 0 ), TextHAlignment::LEFT);
	auto shadowColor = Color4B::BLACK;   // black
	label_name->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
	int zeroLineHeight = label_name->getContentSize().height+_space;
	//target
	sprite_target = Sprite::create("res_ui/renwubb/renwumubiao.png");	
	label_target = CCRichLabel::createWithString(curMissionInfo->summary().c_str(),Size(270, 0 ),NULL,NULL ,0,18);
	//label_target->setScale(0.9f);
	int firstLineHeight = zeroLineHeight + label_target->getContentSize().height+_space;
	//description
	sprite_des = Sprite::create("res_ui/renwubb/renwuxushu.png");
	label_des = CCRichLabel::createWithString(curMissionInfo->description().c_str(),Size(270, 0 ), NULL,NULL,0,18,4);
	int secondLineHeight = firstLineHeight+label_des->getContentSize().height + _space;
	//rewardSprite
	sprite_award = Sprite::create("res_ui/renwubb/renwujiangli.png");  
	//reward(gold,exp)
	bool isHaveExp = false;
	bool isHaveGold =  false; 
	Sprite * sprite_expItemFrame;
	if (curMissionInfo->rewards().has_exp() && curMissionInfo->rewards().exp() > 0)
	{
		sprite_expItemFrame = Sprite::create(EquipOrangeFramePath);

		auto sprite_goodsItem = Sprite::create("res_ui/exp.png");
		sprite_goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_goodsItem->setPosition(Vec2(sprite_expItemFrame->getContentSize().width/2,sprite_expItemFrame->getContentSize().height/2));
		sprite_expItemFrame->addChild(sprite_goodsItem);
		sprite_expItemFrame->setScale(0.8f);

		char s[20];
		sprintf(s,"%d",curMissionInfo->rewards().exp());
		auto l_expValue = Label::createWithTTF(s,APP_FONT_NAME,18);
		l_expValue->setAnchorPoint(Vec2(0,0.5f));
		l_expValue->setPosition(Vec2(80,28));
		sprite_expItemFrame->addChild(l_expValue);

		isHaveExp = true;
	}

	Sprite *sprite_goldItemFrame;
	if (curMissionInfo->rewards().has_gold() && curMissionInfo->rewards().gold() > 0)
	{
		sprite_goldItemFrame = Sprite::create(EquipOrangeFramePath);

		auto sprite_goodsItem = Sprite::create("res_ui/coins.png");
		sprite_goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_goodsItem->setPosition(Vec2(sprite_goldItemFrame->getContentSize().width/2,sprite_goldItemFrame->getContentSize().height/2));
		sprite_goldItemFrame->addChild(sprite_goodsItem);
		sprite_goldItemFrame->setScale(0.8f);

		char s[20];
		sprintf(s,"%d",curMissionInfo->rewards().gold());
		auto l_goldValue = Label::createWithTTF(s,APP_FONT_NAME,18);
		l_goldValue->setAnchorPoint(Vec2(0,0.5f));
		l_goldValue->setPosition(Vec2(80,25));
		sprite_goldItemFrame->addChild(l_goldValue);

		isHaveGold = true;
	}

	int thirdLineHeight;
	if (isHaveExp || isHaveGold)
	{
		 thirdLineHeight = secondLineHeight + 50 + _space;
	}
	else
	{
		thirdLineHeight = secondLineHeight + _space;
	}
	//reward(goods).
	int fourthLineHeight = thirdLineHeight;
	if (curMissionInfo->has_rewards())
	{
		for(int i = 0;i<curMissionInfo->rewards().goods_size();++i)
		{
			Size awardSize = Size(150,50);
			if (i%2 == 0)
			{
				fourthLineHeight += awardSize.height;
			}
		}
	}
	
	scroll_height = fourthLineHeight+_space;

	m_contentScrollView = cocos2d::extension::ScrollView::create(Size(389,293));
	m_contentScrollView->setViewSize(Size(389, 293));
	m_contentScrollView->setIgnoreAnchorPointForPosition(false);
	m_contentScrollView->setTouchEnabled(true);
	m_contentScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
	m_contentScrollView->setAnchorPoint(Vec2(0,0));
	m_contentScrollView->setPosition(Vec2(345,104));
	m_contentScrollView->setBounceable(true);
	m_contentScrollView->setClippingToBounds(true);
	m_contentScrollView->setTag(M_SCROLLVIEWTAG);
	u_layer->addChild(m_contentScrollView);
	if (scroll_height > 293)
	{
		m_contentScrollView->setContentSize(Size(389,scroll_height));
		m_contentScrollView->setContentOffset(Vec2(0,293-scroll_height));  
	}
	else
	{
		m_contentScrollView->setContentSize(Size(389,293));
	}
	m_contentScrollView->setClippingToBounds(true);

	sprite_name->setAnchorPoint(Vec2(0,1.0f));
	sprite_name->setPosition( Vec2( 15, m_contentScrollView->getContentSize().height -zeroLineHeight) );
	m_contentScrollView->addChild(sprite_name);
	label_name->setAnchorPoint(Vec2(0,1.0f));
	label_name->setPosition( Vec2( sprite_name->getContentSize().width+15, m_contentScrollView->getContentSize().height -zeroLineHeight) );
	m_contentScrollView->addChild(label_name);

	sprite_target->setAnchorPoint(Vec2(0,1.0f));
	sprite_target->setPosition( Vec2( 15, m_contentScrollView->getContentSize().height -firstLineHeight+label_target->getContentSize().height-sprite_target->getContentSize().height) );
	m_contentScrollView->addChild(sprite_target);
	label_target->setAnchorPoint(Vec2(0,1.0f));
	label_target->setPosition( Vec2( sprite_target->getContentSize().width+15, m_contentScrollView->getContentSize().height -firstLineHeight) );
	m_contentScrollView->addChild(label_target);

	sprite_des->setAnchorPoint(Vec2(0,1.0f));
	sprite_des->setPosition( Vec2( 15, m_contentScrollView->getContentSize().height -secondLineHeight+label_des->getContentSize().height-sprite_des->getContentSize().height) );
	m_contentScrollView->addChild(sprite_des);
	label_des->setAnchorPoint(Vec2(0,1.0f));
	label_des->setPosition( Vec2( sprite_des->getContentSize().width+15, m_contentScrollView->getContentSize().height -secondLineHeight) );
	m_contentScrollView->addChild(label_des);

	sprite_award->setAnchorPoint(Vec2(0,1.0f));
	
	if (isHaveExp || isHaveGold)
	{
		sprite_award->setPosition( Vec2( 15, m_contentScrollView->getContentSize().height -thirdLineHeight+25) );
	}
	else
	{
		sprite_award->setPosition( Vec2( 15, m_contentScrollView->getContentSize().height -thirdLineHeight-25) );
	}

	m_contentScrollView->addChild(sprite_award);

	if (isHaveExp && isHaveGold)
	{
		sprite_expItemFrame->setIgnoreAnchorPointForPosition(false);
		sprite_expItemFrame->setAnchorPoint(Vec2(0,1.0f));
		sprite_expItemFrame->setPosition( Vec2( 120, m_contentScrollView->getContentSize().height -thirdLineHeight) );
		m_contentScrollView->addChild(sprite_expItemFrame);
		sprite_goldItemFrame->setIgnoreAnchorPointForPosition(false);
		sprite_goldItemFrame->setAnchorPoint(Vec2(0,1.0f));
		sprite_goldItemFrame->setPosition( Vec2( 261, m_contentScrollView->getContentSize().height -thirdLineHeight) );
		m_contentScrollView->addChild(sprite_goldItemFrame);
	}
	else if (isHaveExp && (!isHaveGold))
	{
		sprite_expItemFrame->setIgnoreAnchorPointForPosition(false);
		sprite_expItemFrame->setAnchorPoint(Vec2(0,1.0f));
		sprite_expItemFrame->setPosition( Vec2( 120, m_contentScrollView->getContentSize().height -thirdLineHeight) );
		m_contentScrollView->addChild(sprite_expItemFrame);
	}
	else if (isHaveGold && (!isHaveExp))
	{
		sprite_goldItemFrame->setIgnoreAnchorPointForPosition(false);
		sprite_goldItemFrame->setAnchorPoint(Vec2(0,1.0f));
		sprite_goldItemFrame->setPosition( Vec2( 120, m_contentScrollView->getContentSize().height -thirdLineHeight) );
		m_contentScrollView->addChild(sprite_goldItemFrame);
	}
	else
	{

	}

	if (curMissionInfo->has_rewards())
	{
		for(int i = 0;i<curMissionInfo->rewards().goods_size();++i)
		{
			auto goodsInfo = new GoodsInfo();
			goodsInfo->CopyFrom(curMissionInfo->rewards().goods(i).goods());
			auto rewardItem = RewardItem::create(goodsInfo,curMissionInfo->rewards().goods(i).quantity());
			rewardItem->setTag(8569+i);
			m_contentScrollView->addChild(rewardItem);
			delete goodsInfo;
			if(i%2 == 0)
			{
				rewardItem->setIgnoreAnchorPointForPosition(false);
				rewardItem->setAnchorPoint(Vec2(0,1.0f));
				if (isHaveExp || isHaveGold)
				{
					rewardItem->setPosition(Vec2(120, m_contentScrollView->getContentSize().height - thirdLineHeight -5 - 5*((int)(i/2)) -  rewardItem->getContentSize().height*((int)(i/2))));
				}
				else
				{
					rewardItem->setPosition(Vec2(120, m_contentScrollView->getContentSize().height -thirdLineHeight));
				}
			}
			else
			{
				rewardItem->setIgnoreAnchorPointForPosition(false);
				rewardItem->setAnchorPoint(Vec2(0,1.0f));
				if (isHaveExp || isHaveGold)
				{
					rewardItem->setPosition(Vec2(261, m_contentScrollView->getContentSize().height - thirdLineHeight -5 - 5*((int)(i/2)) - rewardItem->getContentSize().height*((int)(i/2))));
				}
				else
				{
					rewardItem->setPosition(Vec2(261, m_contentScrollView->getContentSize().height -thirdLineHeight));
				}
			}
		}
	}

	Button_giveUpMission->setVisible(false);
	if (curMissionInfo->missionpackagetype() >= 3)
	{
		if (curMissionInfo->missionstate() > 0)
		{
			Button_giveUpMission->setVisible(true);
		}
	}

	if (curMissionInfo->action().action() == openSpecifiedUI)  //打开界面
	{
		if (curMissionInfo->action().specifiedui().id()  == kTagMissionScene)  
		{
			Button_autoPathFinder->setVisible(false);
		}
		else
		{
			Button_autoPathFinder->setVisible(true);
		}
	}
	else
	{
		Button_autoPathFinder->setVisible(true);
	}
}

void MissionScene::updataCurDataSourceByMissionState( int missionState )
{
	if (missionState == 1)  //已接
	{
		l_autoPathFinder->setString(StringDataManager::getString("mission_autoFind"));
		//first delete old data
		std::vector<MissionInfo*>::iterator iter;
		for (iter = curMissionDataSource.begin(); iter != curMissionDataSource.end(); ++iter)
		{
			delete *iter;
		}
		curMissionDataSource.clear();
		for(int i = 0;i<GameView::getInstance()->missionManager->MissionList.size();i++)
		{
			if (GameView::getInstance()->missionManager->MissionList.at(i)->missionstate()>0)
			{
				auto temp = new MissionInfo();
				temp->CopyFrom(*GameView::getInstance()->missionManager->MissionList.at(i));
				this->curMissionDataSource.push_back(temp);
			}
		}

		l_haveNoMissionToGet->setVisible(false);
		if (curMissionDataSource.size()<=0)//当前没有已接任务
		{
			l_haveNoMissionGeted->setVisible(true);
		}
		else
		{
			l_haveNoMissionGeted->setVisible(false);
		}
	}
	else if (missionState == 0)   //可接
	{
		l_autoPathFinder->setString(StringDataManager::getString("mission_getMission"));
		//first delete old data
		std::vector<MissionInfo*>::iterator iter;
		for (iter = curMissionDataSource.begin(); iter != curMissionDataSource.end(); ++iter)
		{
			delete *iter;
		}
		curMissionDataSource.clear();
		for(int i = 0;i<GameView::getInstance()->missionManager->MissionList.size();i++)
		{
			if (GameView::getInstance()->missionManager->MissionList.at(i)->missionstate() == 0)
			{
				auto temp = new MissionInfo();
				temp->CopyFrom(*GameView::getInstance()->missionManager->MissionList.at(i));
				this->curMissionDataSource.push_back(temp);
			}
		}

		l_haveNoMissionGeted->setVisible(false);
		if (curMissionDataSource.size()<=0)//当前没有可接任务
		{
			l_haveNoMissionToGet->setVisible(true);
		}
		else
		{
			l_haveNoMissionToGet->setVisible(false);
		}
	}
	else
	{

	}
}

void MissionScene::ReloadDataWithOutChangeOffSet()
{
	auto _s = m_tableView->getContentOffset();
	int _h = m_tableView->getContentSize().height + _s.y;
	m_tableView->reloadData();
	auto temp = Vec2(_s.x,_h - m_tableView->getContentSize().height);
	m_tableView->setContentOffset(temp); 
}
