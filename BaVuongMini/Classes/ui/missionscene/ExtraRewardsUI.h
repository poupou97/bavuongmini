#ifndef _MISSIONSCENE_EXTRAREWARDSUI_
#define _MISSIONSCENE_EXTRAREWARDSUI_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CMissionReward;
class CMissionGoods;

class ExtraRewardsUI : public UIScene
{
public:
	ExtraRewardsUI();
	~ExtraRewardsUI();

	static ExtraRewardsUI* create(CMissionReward * missionReward,std::string des);
	bool init(CMissionReward * missionReward,std::string des);

	virtual void update( float dt );

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

private:
};

#endif