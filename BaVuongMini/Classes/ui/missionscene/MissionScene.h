
#ifndef _MISSIONSCENE_MISSIONSCENE_
#define _MISSIONSCENE_MISSIONSCENE_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
class MissionInfo;
class CCRichLabel;

class MissionScene :public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	MissionScene();
	~MissionScene();

	static MissionScene* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void GiveUpMissionEvent(Ref *pSender, Widget::TouchEventType type);
	void AutoPathFindEvent(Ref *pSender, Widget::TouchEventType type);
	void MainTabIndexChangedEvent(Ref * pSender);

	void ReloadMissionInfoData();
	void updataCurDataSourceByMissionState(int missionState); /***1:��ѽ  0:�δ�*************/

	void ReloadDataWithOutChangeOffSet();
public:
	enum MissionState
	{
		mission_didNotReceive,
		mission_received,
		mission_finished
	};

	enum MissionType
	{
		mission_mainline,
		mission_sideline,
		mission_dailyTasks
	};

	enum MissionWay
	{
		mission_killMonsters,
		mission_collect,
		mission_pick,
		mission_protect,
		mission_search,
		mission_control,
		mission_talk,
		mission_catch,
		mission_fishing
	};

	int selectCellIdx;
private:
	Layer * u_layer;

	ImageView * flag;

	Layer * contentLayer;
	Sprite * sprite_name;
	Label * label_name;
	Sprite * sprite_target;
	CCRichLabel * label_target;
	Sprite * sprite_des;
	CCRichLabel * label_des;
	Sprite * sprite_award;
	Label * label_award_expName;
	Label * label_award_expValue;
	Label * label_award_goldName;
	Label * label_award_goldValue;
	cocos2d::extension::ScrollView * m_contentScrollView;

	Button * Button_giveUpMission;
	Button * Button_autoPathFinder;
	Text * l_autoPathFinder;

	Label * l_haveNoMissionToGet;
	Label * l_haveNoMissionGeted;

public:
	TableView * m_tableView;
	std::vector<MissionInfo*> curMissionDataSource;

	UITab * mainTab ;
	MissionInfo * curMissionInfo;
};
#endif;
