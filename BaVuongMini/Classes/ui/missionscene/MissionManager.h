
#ifndef _MISSIONSCENE_MISSIONMANAGER_H_
#define _MISSIONSCENE_MISSIONMANAGER_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "MissionActions.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class MTalkWithNpcAction;
class MMonsterKillAction;
class MPickAction;
class MHandleViewAction;
class MOpenViewAction;
class MTalkMission;
class MFailAction;
class MissionInfo;
class MissionMessage;
class MMoveToTargetAndOpenUI;

/**
 * ��������,ߴ���������
 * @author yangjun
 */

class MissionInfo;
class MMoveAction;

//20̼�ǰ��Ҫ�����������ָ�
#define K_GuideForMainMission_Level 20
//����������ָ����ʧ12S���ٴγ��
#define K_GuideForMainMission_Normal_RecoverTime 15.f
#define K_GuideForMainMission_MonsterKilll_RecoverTime 40.f

enum MissionPackageType
{
	MPT_Main = 1,                    //����
	BRANCH,                             //�֧�
	GANG,                                 //���ʦ
	SEVEN_TIMES,                     //�߽��߳
	BEAN_SOLDIERS,                 //���ɱ
	SNEAK_ATTACK,                  //�ҹϮ�ڳ�
	FAMILY,                               //���
	TAVERN,                              //�ƹ
	WAR,                                   //�����ս
};

class MissionManager
{
public:

	enum MActionType
	{
		KtypeDoNothing = 0,
		KTypeMMoveAction = 1,
		KTypeMMonsterKillAction = 2,
		KTypeMPickAction = 3,
		KTypeMTalkWithNpcAction = 4,
		KTypeMHandleViewAction = 5,
		KTypeMOpenViewAction = 6,
		KTypeMOpenNpcFunction = 7,
		KTypeMTalkMissionAction = 8,
		KTypeMoveToTargetAndOpenUI = 9,
		KTypeMFailAction = 10,
	};

	enum CurMissionStatus {
		status_none = 0,
		status_waitForTransport,   // the player is in the the waiting sequence to transport
		status_transportFinished,   // the player is finished the transport
	};

	MissionManager();
	virtual ~MissionManager();

	static MissionManager* getInstance();

	void update();

	//ָ������������
	bool isExistGuideForMmission;
	float guideForMmission_time;
	void addGuideForMainMission();

public:
	MissionInfo * curMissionInfo;
	bool isAutoRunForMission;
	//������б
	std::vector<MissionInfo *> MissionList;
	std::vector<MissionInfo *> MissionList_MainScene;
	std::vector<MissionInfo *> curMissionList;
	void addMission(MissionInfo *missionInfo);
	void doMission(MissionInfo *missionInfo);

	bool isOutOfMissionTalkArea(int missionNpcId);

	void autoPopUpHandleMission();

	void showHandleMission(Node* sender, void* data);

	int m_nTargetNpcId;

	void setMissionAndTeamOffSet(Vec2 temp);
	Vec2 getMissionAndTeamOffSet();

	bool getIsFirstCreateMissionAndTeam();
	void setIsFirstCreateMissionAndTeam(bool _value);

 	static void setStatus(int status);
 	static int getStatus();

	void AcrossMapTransport(std::string mapId,Vec2 pos);
	Vec2 getNearestReachablePos(Vec2& targetPoint);

	//���һ����ɵ�����İID
	std::string m_sLastMissionPackageId;
	//���һ����ɵ�����������������е�λ����Ϣ
	int m_nLastMissionIndexInMain;
	//��һ����ɵ������ύǰ��ƫ���
	Vec2 m_pLastMissionOffSet;
	Size m_missionTableViewSize;

	//����Ƿ��ܹ�Ѱ·
	bool CheckIsEnableToRunToTarget(const char * mapid,Vec2 pos);

private:
	MActionType curActionType;

	MMoveAction * curMoveAction;
	MTalkWithNpcAction * curTalkWithNpcAction;
	MMonsterKillAction * curMonsterKillAction;
	MPickAction * curPickAction;
	MHandleViewAction * curHandleAction;
	MOpenViewAction * curOpenAction;
	MOpenNpcFunction * curOpenNpcFunction;
	MTalkMission * curTalkMission;
	MMoveToTargetAndOpenUI * curMoveToTargetAndOpenUIAction;
	MFailAction * curFailAction;

	bool m_bIsFirstCreateMissionAndTeam;
	Vec2 cp_offSet;

	static int s_status;
};
#endif;

