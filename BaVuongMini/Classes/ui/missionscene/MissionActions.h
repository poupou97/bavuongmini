
#ifndef _MISSIONMODE_MISSIONACTIONS_H_
#define _MISSIONMODE_MISSIONACTIONS_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class MissionInfo;

typedef void (Ref::*SEL_FinishedMove)();
#define coco_finishedMove(_SELECTOR) (SEL_FinishedMove)(&_SELECTOR)

class MFailAction
{
public:
	MFailAction();
	~MFailAction();

	static MFailAction * create(MissionInfo *missionInfo);
	void init(MissionInfo *missionInfo);
};

////////////////////////////////////////////////////////////////////

class MHandleViewAction
{
public:
	MHandleViewAction();
	~MHandleViewAction();

	static MHandleViewAction * create(MissionInfo * missionInfo);
	void init(MissionInfo * missionInfo);
};

////////////////////////////////////////////////////////////////////

class MMoveAction
{
public:
	MMoveAction();
	~MMoveAction();

	static MMoveAction * create(MissionInfo *missionInfo);
	void init(MissionInfo *missionInfo);

	//void addFinishedMove(Ref * pSender,SEL_FinishedMove selector);

	//void update(float dt);

	float getDistance();
	bool isFinishedMove();
private:
	bool isEnableFindPath();
	void runToTarget();
	//void doFinishedMove();

private:
	float distance;
	std::string targetMapId;
	long long targetActorId;
	Vec2 targetPos;
	Ref* m_pListener;
	SEL_FinishedMove m_pfnSelector;

	MissionInfo * curMissionInfo;
};

////////////////////////////////////////////////////////////////////

class MMonsterKillAction
{
public:
	MMonsterKillAction();
	~MMonsterKillAction();

	static MMonsterKillAction * create(MissionInfo *missionInfo);
	void init(MissionInfo *missionInfo);
	bool isFinishedMove();
	void doMMonsterKillAction();
private:
	MMoveAction * moveAction;
};

////////////////////////////////////////////////////////////////////

class MOpenViewAction
{
public:
	MOpenViewAction();
	~MOpenViewAction();

	static MOpenViewAction * create(MissionInfo * missionInfo);
	void init(MissionInfo * missionInfo);
};

////////////////////////////////////////////////////////////////////

class MPickAction
{
public:
	MPickAction();
	~MPickAction();

	static MPickAction * create(MissionInfo * missionInfo);
	void init(MissionInfo * missionInfo);
	bool isFinishedMove();
	void doPickAction();
private:
	MMoveAction * moveAction;
};

////////////////////////////////////////////////////////////////////

class MTalkWithNpcAction
{
public:
	MTalkWithNpcAction();
	~MTalkWithNpcAction();

	static MTalkWithNpcAction * create(MissionInfo *missionInfo);
	void init(MissionInfo *missionInfo);
	bool isFinishedMove();
	void doMTalkWithNpcAction();
private:
	MissionInfo * curMissionInfo;
	MMoveAction * moveAction;
	bool isfirst;

};

////////////////////////////////////////////////////////////////////

class MOpenNpcFunction
{
public:
	MOpenNpcFunction();
	~MOpenNpcFunction();

	static MOpenNpcFunction * create(MissionInfo *missionInfo);
	void init(MissionInfo *missionInfo);
	bool isFinishedMove();
	void doMOpenNpcFunction();
private:
	MissionInfo * curMissionInfo;
	MMoveAction * moveAction;
	bool isfirst;

};

////////////////////////////////////////////////////////////////////

class MTalkMission
{
public:
	MTalkMission();
	~MTalkMission();

	static MTalkMission * create(MissionInfo *missionInfo);
	void init(MissionInfo *missionInfo);
	bool isFinishedMove();
	void doMTalkMission();
private:
	MissionInfo * curMissionInfo;
	MMoveAction * moveAction;
	bool isfirst;

};


////////////////////////////////////////////////////////////////////

class MMoveToTargetAndOpenUI
{
public:
	MMoveToTargetAndOpenUI();
	~MMoveToTargetAndOpenUI();

	static MMoveToTargetAndOpenUI * create(MissionInfo *missionInfo);
	void init(MissionInfo *missionInfo);
	bool isFinishedMove();
	void doMMoveToTargetAndOpenUI();
private:
	MissionInfo * curMissionInfo;
	MMoveAction * moveAction;
	bool isfirst;

};
#endif
