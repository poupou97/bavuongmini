#include "ExtraRewardsUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../backpackscene/PackageItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/CMissionGoods.h"
#include "../../messageclient/element/CMissionReward.h"
#include "../extensions/CCRichLabel.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"


ExtraRewardsUI::ExtraRewardsUI()
{
}


ExtraRewardsUI::~ExtraRewardsUI()
{
}

ExtraRewardsUI* ExtraRewardsUI::create(CMissionReward * missionReward,std::string des)
{
	auto extraRewardsUI = new ExtraRewardsUI();
	if (extraRewardsUI && extraRewardsUI->init(missionReward,des))
	{
		extraRewardsUI->autorelease();
		return extraRewardsUI;
	}
	CC_SAFE_DELETE(extraRewardsUI);
	return NULL;
}

bool ExtraRewardsUI::init(CMissionReward * missionReward,std::string des)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate(); 
		//���UI
		if(LoadSceneLayer::ExtraRewardsLayer->getParent() != NULL)
		{
			LoadSceneLayer::ExtraRewardsLayer->removeFromParentAndCleanup(false);
		}
		auto ppanel = LoadSceneLayer::ExtraRewardsLayer;
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(354, 226));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto btn_close = (ui::Button*)ui::Helper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(ExtraRewardsUI::CloseEvent, this));
		btn_close->setPressedActionEnabled(true);

		auto btn_enter = (ui::Button*)ui::Helper::seekWidgetByName(ppanel,"Button_enter");
		btn_enter->setTouchEnabled(true);
		btn_enter->addTouchEventListener(CC_CALLBACK_2(ExtraRewardsUI::CloseEvent, this));
		btn_enter->setPressedActionEnabled(true);

		RichLabelDefinition def;
		def.strokeEnabled = false;
		def.fontSize = 22;
		auto l_des = CCRichLabel::create(des.c_str(),Size(300,20),def);
		l_des->setAnchorPoint(Vec2(0.5f,0.5f));
		l_des->setPosition(Vec2(180,178));
		addChild(l_des);

		auto imageView_exp = (ui::ImageView *)ui::Helper::seekWidgetByName(ppanel,"ImageView_exp");
		auto imageView_gold = (ui::ImageView *)ui::Helper::seekWidgetByName(ppanel,"ImageView_gold");
		auto l_exp = (Label *)ui::Helper::seekWidgetByName(ppanel,"Label_expValue");
		auto l_gold = (Label *)ui::Helper::seekWidgetByName(ppanel,"Label_goldValue");

		if (missionReward->goods_size() <= 0)
		{
			imageView_exp->setVisible(true);
			imageView_gold->setVisible(true);
			l_exp->setVisible(true);
			l_gold->setVisible(true);

			char s_exp[20];
			sprintf(s_exp,"%d",missionReward->exp());
			l_exp->setString(s_exp);
			char s_gold[20];
			sprintf(s_gold,"%d",missionReward->gold());
			l_gold->setString(s_gold);
		}
		else
		{
			imageView_exp->setVisible(false);
			imageView_gold->setVisible(false);
			l_exp->setVisible(false);
			l_gold->setVisible(false);

			for(int i = 0;i<missionReward->goods_size();++i)
			{
				auto temp = new GoodsInfo();
				temp->CopyFrom(missionReward->goods(i).goods());
				auto packageItem = PackageItem::create(temp);
				packageItem->setAnchorPoint(Vec2(0,0));
				packageItem->setPosition(Vec2(200,80));
				m_pLayer->addChild(packageItem);
				packageItem->setLocalZOrder(20);
				std::string str_name = "rewardItem_";
				char s_index [5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				packageItem->setName(str_name.c_str());

				char s_num[20];
				sprintf(s_num,"%d",missionReward->goods(i).quantity());
				auto Lable_num = Label::createWithTTF(s_num, APP_FONT_NAME, 13);
				Lable_num->setAnchorPoint(Vec2(1.0f,0));
				Lable_num->setPosition(Vec2(75,19));
				Lable_num->setName("Label_num");
				packageItem->addChild(Lable_num);
			}

			switch(missionReward->goods_size())
			{
			case 0 :
				{

				}
				break;
			case 1 :
				{
					std::string str_name = "rewardItem_1";
					auto packageItem = dynamic_cast<PackageItem*>(m_pLayer->getChildByName(str_name.c_str()));
					if (packageItem)
					{
						packageItem->setPosition(Vec2(160,80));
					}
				}
				break;
			case 2 :
				{
					for(int i = 0;i<missionReward->goods_size();++i)
					{
						std::string str_name = "rewardItem_";
						char s_index [5];
						sprintf(s_index,"%d",i);
						str_name.append(s_index);
						auto packageItem = dynamic_cast<PackageItem*>(m_pLayer->getChildByName(str_name.c_str()));
						if (packageItem)
						{
							packageItem->setPosition(Vec2(75+115*i,80));
						}
					}
				}
				break;
			case 3 :
				{
					for(int i = 0;i<missionReward->goods_size();++i)
					{
						std::string str_name = "rewardItem_";
						char s_index [5];
						sprintf(s_index,"%d",i);
						str_name.append(s_index);
						auto packageItem = dynamic_cast<PackageItem*>(m_pLayer->getChildByName(str_name.c_str()));
						if (packageItem)
						{
							packageItem->setPosition(Vec2(45+90*i,80));
						}
					}
				}
				break;
			default:
				{

				}
			}
		}

		this->setContentSize(Size(354,226));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(ExtraRewardsUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(ExtraRewardsUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(ExtraRewardsUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(ExtraRewardsUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void ExtraRewardsUI::update( float dt )
{

}

void ExtraRewardsUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ExtraRewardsUI::onExit()
{
	UIScene::onExit();
}

bool ExtraRewardsUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void ExtraRewardsUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void ExtraRewardsUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void ExtraRewardsUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void ExtraRewardsUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}
