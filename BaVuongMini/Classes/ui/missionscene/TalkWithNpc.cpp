#include "TalkWithNpc.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"
#include "../../messageclient/protobuf/NPCMessage.pb.h"
#include "../../messageclient/element/CNpcDialog.h"
#include "../extensions/CCRichLabel.h"
#include "../extensions/UITab.h"
#include "AppMacros.h"

TalkWithNpc::TalkWithNpc()
{
}


TalkWithNpc::~TalkWithNpc()
{
}


TalkWithNpc* TalkWithNpc::create()
{
	auto talkWithNpc = new TalkWithNpc();
	if(talkWithNpc && talkWithNpc->init())
	{
		talkWithNpc->autorelease();
		return talkWithNpc;
	}
	CC_SAFE_DELETE(talkWithNpc);
	return NULL;
}

TalkWithNpc* TalkWithNpc::create(CNpcDialog * npcDialog )
{
	auto talkWithNpc = new TalkWithNpc();
	if (talkWithNpc && talkWithNpc->init(npcDialog))
	{
		talkWithNpc->autorelease();
		return talkWithNpc;
	}
	CC_SAFE_DELETE(talkWithNpc);
	return NULL;
}

bool TalkWithNpc::init()
{
	if (UIScene::init())
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();
		//加载UI
		if(LoadSceneLayer::TalkWithNpcLayer->getParent() != NULL)
		{
			LoadSceneLayer::TalkWithNpcLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (Layout*)LoadSceneLayer::s_pPanel->copy();
		auto ppanel = LoadSceneLayer::TalkWithNpcLayer;
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(350, 400));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto button_close = (Button *)Helper::seekWidgetByName(ppanel,"Button_close");
		button_close->setTouchEnabled(true);
		button_close->addTouchEventListener(CC_CALLBACK_2(TalkWithNpc::CloseEvent, this));
		ImageView_headImage = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_touxiang");;
		ImageView_name = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_mingzi");

		//richLabel_description
		auto label_des = CCRichLabel::createWithString("去新手村杀五只棍犬加快两个",Size(285,50),this,NULL,0);
		auto desSize = label_des->getContentSize();
		int height = desSize.height;
		//ScrollView_description
		auto m_contentScrollView = cocos2d::extension::ScrollView::create(Size(290,120));
		m_contentScrollView->setViewSize(Size(290, 120));
		m_contentScrollView->setIgnoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(TableView::Direction::VERTICAL);
		m_contentScrollView->setAnchorPoint(Vec2(0,0));
		m_contentScrollView->setPosition(Vec2(33,173));
		m_contentScrollView->setBounceable(false);
		m_pLayer->addChild(m_contentScrollView);
		if (height > 120)
		{
			m_contentScrollView->setContentSize(Size(290,height));
			m_contentScrollView->setContentOffset(Vec2(0,120-height));
		}
		else
		{
			m_contentScrollView->setContentSize(Size(290,120));
		}
		m_contentScrollView->setClippingToBounds(true);

		m_contentScrollView->addChild(label_des);
		label_des->setIgnoreAnchorPointForPosition(false);
		label_des->setAnchorPoint(Vec2(0,1));
		label_des->setPosition(Vec2(3,m_contentScrollView->getContentSize().height));


		//m_tableView = TableView::create(this,Size(296,132));
		//m_tableView->setDirection(TableView::Direction::VERTICAL);
		//m_tableView->setAnchorPoint(Vec2(0,0));
		//m_tableView->setPosition(Vec2(27,30));
		//m_tableView->setDelegate(this);
		//m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//this->addChild(m_tableView);
		
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		//this->setContentSize(designResolutionSize);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(TalkWithNpc::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(TalkWithNpc::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(TalkWithNpc::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(TalkWithNpc::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

bool TalkWithNpc::init(CNpcDialog * npcDialog)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();
		//加载UI
		if(LoadSceneLayer::TalkWithNpcLayer->getParent() != NULL)
		{
			LoadSceneLayer::TalkWithNpcLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (Layout*)LoadSceneLayer::s_pPanel->copy();
		auto ppanel = LoadSceneLayer::TalkWithNpcLayer;
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(350, 400));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto button_close = (Button *)Helper::seekWidgetByName(ppanel,"Button_close");
		button_close->setTouchEnabled(true);
		button_close->addTouchEventListener(CC_CALLBACK_2(TalkWithNpc::CloseEvent, this));
		ImageView_headImage = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_touxiang");;
		ImageView_name = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_mingzi");

		auto label_welcom = Text::create("hello every hello every hello every hello every hello every hello every ", APP_FONT_NAME, 20);
		label_welcom->setTextAreaSize(Size(280,120));
		label_welcom->setTextHorizontalAlignment(TextHAlignment::LEFT);
		label_welcom->setAnchorPoint(Vec2(0,1));
		label_welcom->setPosition(Vec2(34,367));
		m_pLayer->addChild(label_welcom);

// 		if (npcDialog->options_size()>0)
// 		{
// 			m_tableView = TableView::create(this,Size(296,132));
// 			m_tableView->setDirection(TableView::Direction::VERTICAL);
// 			m_tableView->setAnchorPoint(Vec2(0,0));
// 			m_tableView->setPosition(Vec2(27,30));
// 			m_tableView->setDelegate(this);
// 			m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
// 			this->addChild(m_tableView);
// 		}

		//m_describeLabel = Label::createWithTTF("123456789","Arial",16);
		//m_describeLabel->setAnchorPoint(Vec2(0.5f,0.5f));

		//m_scrollView = ScrollView::create(m_describeLabel->getContentSize(),m_describeLabel);
		//m_scrollView->setViewSize(Size(296, 132));
		//m_scrollView->setAnchorPoint(Vec2::ZERO);
		//m_scrollView->setContentOffset(Vec2(0,0));
		//m_scrollView->setTouchEnabled(true);
		//m_scrollView->setDirection(TableView::Direction::VERTICAL);
		//m_scrollView->setPosition(Vec2(27,164));
		//m_scrollView->setDelegate(this);
		//m_scrollView->setBounceable(true);
		//m_scrollView->setClippingToBounds(true);
		//addChild(m_scrollView);

		//m_tableView = TableView::create(this,Size(296,132));
		//m_tableView->setDirection(TableView::Direction::VERTICAL);
		//m_tableView->setAnchorPoint(Vec2(0,0));
		//m_tableView->setPosition(Vec2(27,30));
		//m_tableView->setDelegate(this);
		//m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//this->addChild(m_tableView);
		//
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		//this->setContentSize(designResolutionSize);

		return true;
	}
	return false;
}


void TalkWithNpc::onEnter()
{
	UIScene::onEnter();

	auto  action = Sequence::create(
		ScaleTo::create(0.15f*1/2,1.1f),
		ScaleTo::create(0.15f*1/3,1.0f),
		NULL);

	runAction(action);

	////this->setTouchEnabled(true);
	//this->setContentSize(Size(800,480));


}

void TalkWithNpc::onExit()
{
	UIScene::onExit();
}


bool TalkWithNpc::onTouchBegan(Touch *touch, Event * pEvent)
{
	return this->resignFirstResponder(touch,this,false);
}

void TalkWithNpc::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void TalkWithNpc::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void TalkWithNpc::onTouchMoved(Touch *touch, Event * pEvent)
{
}



Size TalkWithNpc::tableCellSizeForIndex(TableView *table, unsigned int idx)
{
	if (idx == 0)
	{
		return Size(289,37);
	}
	else
	{
		return Size(289, 37);
	}

}

TableViewCell* TalkWithNpc::tableCellAtIndex(TableView *table, ssize_t  idx)
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell=new TableViewCell();
	cell->autorelease();

	auto pCellBg = Sprite::create("res_ui/renwua/button_0.png");
	pCellBg->setAnchorPoint(Vec2(0, 0));
	pCellBg->setPosition(Vec2(0, 0));
	cell->addChild(pCellBg);

	auto pMissionName = Label::createWithTTF("123456789",APP_FONT_NAME,16);
	pMissionName->setAnchorPoint(Vec2(0.5f,0.5f));
	pMissionName->setPosition(Vec2(pCellBg->getContentSize().width/2,pCellBg->getContentSize().height/2));
	auto shadowColor = Color4B::BLACK;   // black
	pMissionName->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
	cell->addChild(pMissionName);

	return cell;

}


ssize_t TalkWithNpc::numberOfCellsInTableView(TableView *table)
{
	return 2;
}

void TalkWithNpc::tableCellTouched(TableView* table, TableViewCell* cell)
{

}

void TalkWithNpc::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void TalkWithNpc::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

void TalkWithNpc::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}



