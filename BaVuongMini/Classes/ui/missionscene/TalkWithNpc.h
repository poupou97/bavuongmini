
#ifndef _MISSIONSCENE_TALKWITHNPC_
#define _MISSIONSCENE_TALKWITHNPC_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
class CNpcDialog;

class TalkWithNpc : public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	TalkWithNpc();
	~TalkWithNpc();

	static TalkWithNpc* create();
	bool init();

	static TalkWithNpc* create(CNpcDialog * npcDialog);
	bool init(CNpcDialog * npcDialog);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

private:
	ImageView * ImageView_headImage;
	ImageView * ImageView_name;
	TableView * m_tableView;
	cocos2d::extension::ScrollView * m_scrollView;
	Label * m_describeLabel;
};
#endif;