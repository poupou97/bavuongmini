
#ifndef _MISSIONSCENE_HANDLEMISSION_
#define _MISSIONSCENE_HANDLEMISSION_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;

class MissionInfo;

class HandleMission : public UIScene
{
public:
	enum handleType
	{
		getMission,
		submitMission
	};

	HandleMission();
	~HandleMission();

	static HandleMission* create(MissionInfo * missionInfo);
	bool init(MissionInfo * missionInfo);

	virtual void update( float dt );

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);


	std::string rewardIconName[5];
	std::string rewardLabelName[5];

	int getTargetNpcId();

	handleType getCurHandlerType();

public: 
	Button * Button_getMission;
	Button * Button_finishMission;

	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	//��ȡ/�ύ��ť
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();
private:
	MissionInfo * curMissionInfo;
	int targetNpcId;
	handleType curHandleType;
	Button * Button_Handle;
	cocos2d::extension::ScrollView * m_scrollView;
	Label * m_describeLabel;
	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void GetMissionEvent(Ref *pSender, Widget::TouchEventType type);
	void FinishMissionEvent(Ref *pSender, Widget::TouchEventType type);

	ImageView * reward_di_1;
	ImageView * reward_di_2;
	ImageView * reward_di_3;
	ImageView * reward_di_4;
	ImageView * reward_di_5;


	//��ѧ
	int mTutorialScriptInstanceId;
};
#endif;

