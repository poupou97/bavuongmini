#ifndef _MISSIONSCENE_TALKMISSIONUI_H_
#define _MISSIONSCENE_TALKMISSIONUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class MissionInfo;

class TalkMissionUI : public UIScene
{
public:
	TalkMissionUI();
	~TalkMissionUI();

	static TalkMissionUI* create(MissionInfo * missionInfo);
	bool init(MissionInfo * missionInfo);

	virtual void update( float dt );

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void EnterEvent(Ref *pSender, Widget::TouchEventType type);

	int getTargetNpcId();

private:
	MissionInfo * curMissionInfo;
	int targetNpcId;

};

#endif
