#include "HandleMission.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../extensions/CCRichLabel.h"
#include "../extensions/UITab.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "MissionRewardGoodsItem.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "MissionManager.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../utils/GameUtils.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"

using namespace CocosDenshion;

#define HANDLEMISSION_PARTICLEATBUTTON_TAG 21
#define HANDLEMISSION_TUTORIALINDICATOR_TAG 22

HandleMission::HandleMission() : 
curHandleType(getMission),
targetNpcId(0)
{
	rewardIconName[0] = "rewardIcon_0";
	rewardIconName[1] = "rewardIcon_1";
	rewardIconName[2] = "rewardIcon_2";
	rewardIconName[3] = "rewardIcon_3";
	rewardIconName[4] = "rewardIcon_4";
	rewardLabelName[0] = "rewardLabel_0";
	rewardLabelName[1] = "rewardLabel_1";
	rewardLabelName[2] = "rewardLabel_2";
	rewardLabelName[3] = "rewardLabel_3";
	rewardLabelName[4] = "rewardLabel_4";
}


HandleMission::~HandleMission()
{
	delete curMissionInfo;
}

void HandleMission::update( float dt )
{
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission) != NULL)
	{
		if (targetNpcId == 0)   //û��Ŀ�NPC�ʱ����Ҫ��
			return;

	 	if (GameView::getInstance()->missionManager->isOutOfMissionTalkArea(this->getTargetNpcId()))
	 	{
	 		this->closeAnim();
	 	}
	}
}

HandleMission* HandleMission::create(MissionInfo * missionInfo)
{
	auto handleMission = new HandleMission();
	if (handleMission && handleMission->init(missionInfo))
	{
		handleMission->autorelease();
		return handleMission;
	}
	CC_SAFE_DELETE(handleMission);
	return NULL;

}

bool HandleMission::init(MissionInfo * missionInfo)
{
	if (UIScene::init())
	{
		curMissionInfo = new MissionInfo();
		curMissionInfo->CopyFrom(*missionInfo);

		if (missionInfo->has_tip())
		{
			targetNpcId = missionInfo->tip().npcid();
		}
		else if (missionInfo->action().has_moveto())
		{
			if (missionInfo->action().moveto().has_targetnpc())
			{
				targetNpcId = missionInfo->action().moveto().targetnpc().npcid();
			}
		}
		else
		{
		}

		GameView::getInstance()->missionManager->m_nTargetNpcId = targetNpcId;

		auto winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();
		//���UI
		if(LoadSceneLayer::TalkWithNpcLayer->getParent() != NULL)
		{
			LoadSceneLayer::TalkWithNpcLayer->removeFromParentAndCleanup(false);
		}
		auto ppanel = LoadSceneLayer::TalkWithNpcLayer;
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(350, 420));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(HandleMission::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);
		
		auto panel_mission = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_renwu");
		panel_mission->setVisible(true);
		Button_getMission = (Button*)Helper::seekWidgetByName(ppanel,"Button_lingqu");
		Button_getMission->setTouchEnabled(true);
		Button_getMission->setPressedActionEnabled(true);
		Button_getMission->addTouchEventListener(CC_CALLBACK_2(HandleMission::GetMissionEvent, this));
		Button_finishMission = (Button*)Helper::seekWidgetByName(ppanel,"Button_tijiao");
		Button_finishMission->setTouchEnabled(true);
		Button_finishMission->setPressedActionEnabled(true);
		Button_finishMission->addTouchEventListener(CC_CALLBACK_2(HandleMission::FinishMissionEvent, this));

		reward_di_1 = (ImageView * )Helper::seekWidgetByName(ppanel,"ImageView_di1");
		reward_di_2 = (ImageView * )Helper::seekWidgetByName(ppanel,"ImageView_di2");
		reward_di_3 = (ImageView * )Helper::seekWidgetByName(ppanel,"ImageView_di3");
		reward_di_4 = (ImageView * )Helper::seekWidgetByName(ppanel,"ImageView_di4");
		reward_di_1->setVisible(false);
		reward_di_2->setVisible(false);
		reward_di_3->setVisible(false);
		reward_di_4->setVisible(false);

		if(this->getChildByTag(CCTUTORIALINDICATORTAG) == NULL)
		{
			// the particle effect
			//if (GameView::getInstance()->myplayer->getActiveRole()->level() <= 20)
			//{
			//	CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("tuowei0.plist",80,60);
			//	tutorialParticle->setPosition(Vec2(Button_getMission->getPosition().x,Button_getMission->getPosition().y-25));
			//	tutorialParticle->setTag(HANDLEMISSION_PARTICLEATBUTTON_TAG); 
			//	this->addChild(tutorialParticle);
			//}

			// the hand
			if (GameView::getInstance()->myplayer->getActiveRole()->level() <= 10)
			{
				auto guideHand = (CCTutorialIndicator*)this->getChildByTag(HANDLEMISSION_TUTORIALINDICATOR_TAG);
				if (guideHand == NULL)
				{
					guideHand = CCTutorialIndicator::create("",Vec2(0,0),CCTutorialIndicator::Direction_LU,true,false);
					guideHand->setPosition(Vec2(Button_getMission->getPosition().x,Button_getMission->getPosition().y));
					guideHand->setTag(HANDLEMISSION_TUTORIALINDICATOR_TAG);
					guideHand->addHighLightFrameAction(115,52);
					guideHand->addCenterAnm(115,52);
					this->addChild(guideHand);
				}
			}
		}

		// load animation
		CNpcInfo * npcInfo = NULL;
		if (targetNpcId == 0)
		{
			if (missionInfo->action().has_specifiedui())
			{
				if (missionInfo->action().specifiedui().has_para())
				{
					std::map<long long ,CNpcInfo*>::const_iterator cIter;
					cIter = GameWorld::NpcInfos.find(missionInfo->action().specifiedui().para());
					if (cIter == GameWorld::NpcInfos.end()) // �û�ҵ�����ָ�END��  
					{
						npcInfo = NULL;
					}
					else
					{
						npcInfo = GameWorld::NpcInfos[missionInfo->action().specifiedui().para()];
					}
				}
			}
		}
		else
		{
			std::map<long long ,CNpcInfo*>::const_iterator cIter;
			cIter = GameWorld::NpcInfos.find(targetNpcId);
			if (cIter == GameWorld::NpcInfos.end()) // �û�ҵ�����ָ�END��  
			{
				npcInfo = NULL;
			}
			else
			{
				npcInfo = GameWorld::NpcInfos[targetNpcId];
			}
		}

		std::string animFileName = "res_ui/npc/";
		if (npcInfo == NULL)//�û�����NPC
		{
			animFileName.append("cz");
		}
		else
		{
			animFileName.append(npcInfo->icon.c_str());
		}
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/npc/caiwenji.anm";
		auto m_pAnim = CCLegendAnimation::create(animFileName);
		if (m_pAnim)
		{
			m_pAnim->setPlayLoop(true);
			m_pAnim->setReleaseWhenStop(false);
			m_pAnim->setScale(1.0f);
			m_pAnim->setPosition(Vec2(33,312));
			//m_pAnim->setPlaySpeed(.35f);
			addChild(m_pAnim);
		}

		auto sprite_anmFrame = Sprite::create("res_ui/renwua/tietu2.png");
		sprite_anmFrame->setAnchorPoint(Vec2(0.5,0.5f));
		sprite_anmFrame->setPosition(Vec2(116,356));
		addChild(sprite_anmFrame);


		auto ImageView_name = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_mingzi");

		FontDefinition strokeShaodwTextDef;
		strokeShaodwTextDef._fontName = std::string("res_ui/font/simhei.ttf");
		Color3B tintColorGreen   =  { 54, 92, 7 };
		strokeShaodwTextDef._fontFillColor   = tintColorGreen;   // �������ɫ
		strokeShaodwTextDef._fontSize = 22;   // �����С
		// 		strokeShaodwTextDef.m_stroke.m_strokeEnabled = true;   // �Ƿ񹴱
		// 		strokeShaodwTextDef.m_stroke.m_strokeColor   = blackColor;   // ߹�����ɫ����ɫ
		// 		strokeShaodwTextDef.m_stroke.m_strokeSize    = 2.0;   // ���ߵĴ�С
		/*Label* l_nameLabel = Label::createWithTTFWithFontDefinition(npcInfo->npcName.c_str(), strokeShaodwTextDef);
		Color3B shadowColor = Color3B(0,0,0);   // black
		l_nameLabel->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		l_nameLabel->setAnchorPoint(Vec2(0.5,0.5f));
		l_nameLabel->setPosition(Vec2(184,359));
		addChild(l_nameLabel);*/
		Label * l_nameLabel;
		if (npcInfo == NULL) //û�����NPC
		{
			l_nameLabel = Label::createWithTTF(StringDataManager::getString("mission_teachNpcName"),"res_ui/font/simhei.ttf",18);
		}
		else
		{
			l_nameLabel = Label::createWithTTF(npcInfo->npcName.c_str(),"res_ui/font/simhei.ttf",18);
		}
		l_nameLabel->setAnchorPoint(Vec2(0.5,0.5f));
		l_nameLabel->setPosition(Vec2(207,327));
		Color4B shadowColor = Color4B::BLACK;   // black
		l_nameLabel->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		addChild(l_nameLabel);
// 		ImageView * rewardImage = ImageView::create();
// 		rewardImage->loadTexture("res_ui/renwua/renwujianglitu.png");
// 		rewardImage->setAnchorPoint(Vec2::ZERO);
// 		rewardImage->setPosition(Vec2(132,129));
// 		m_pLayer->addChild(rewardImage);

// 		Button_Handle = Button::create();
// 		Button_Handle->loadTextures("res_ui/renwua/renwulingqu.png","res_ui/renwua/renwulingqu.png","");
// 		Button_Handle->setAnchorPoint(Vec2::ZERO);
// 		Button_Handle->setTouchEnabled(true);
// 		Button_Handle->setPosition(Vec2(30,65));
// 		Button_Handle->addTouchEventListener(CC_CALLBACK_2(HandleMission::HandleEvent));
// 		m_pLayer->addChild(Button_Handle);

		auto lbf_reward_gold  = (Text*)Helper::seekWidgetByName(ppanel,"Label_goldValue");
		auto lbf_reward_exp  = (Text*)Helper::seekWidgetByName(ppanel,"Label_expValue");

		if (missionInfo->has_rewards())
		{
			char s_gold[20];
			sprintf(s_gold,"%d",missionInfo->rewards().gold());
			lbf_reward_gold->setString(s_gold);

			char s_exp[20];
			sprintf(s_exp,"%d",missionInfo->rewards().exp());
			lbf_reward_exp->setString(s_exp);

			if (missionInfo->rewards().goods_size()>0)
			{
				for (int i =0;i<missionInfo->rewards().goods_size();++i)
				{
					GoodsInfo * goodInfo = new GoodsInfo();
					goodInfo->CopyFrom(missionInfo->rewards().goods(i).goods());
					int num = missionInfo->rewards().goods(i).quantity();

					MissionRewardGoodsItem * mrGoodsItem = MissionRewardGoodsItem::create(goodInfo,num);
					mrGoodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
					mrGoodsItem->setPosition(Vec2(82+58*i,106));
					mrGoodsItem->setName(rewardIconName[i].c_str());
					mrGoodsItem->setScale(0.8f);
					m_pLayer->addChild(mrGoodsItem);

					delete goodInfo;
				}
			}
		}

		auto l_phyValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_phyValue");
		l_phyValue->setVisible(false);

		if (missionInfo->missionstate() < 1)
		{
			curHandleType = getMission;
			Button_getMission->setVisible(true);
			Button_finishMission->setVisible(false);
			//Button_Handle->loadTextures("res_ui/renwua/renwulingqu.png","res_ui/renwua/renwulingqu.png","");

			if (missionInfo->has_physicalpower() && missionInfo->physicalpower() >0)
			{
				l_phyValue->setVisible(true);
				char  str_phyValue[10];
				sprintf(str_phyValue,"%d",missionInfo->physicalpower());
				l_phyValue->setString(str_phyValue);
			}
		}
		else 
		{
			curHandleType = submitMission;
			Button_getMission->setVisible(false);
			Button_finishMission->setVisible(true);
			//Button_Handle->loadTextures("res_ui/renwua/renwuwancheng.png","res_ui/renwua/renwuwancheng.png","");
		}

		//description
		std::string mission_description = "";
		if (missionInfo->has_tip())
		{
			mission_description.append(missionInfo->tip().dialog());
		}
		else
		{
			mission_description.append(missionInfo->dialog());
		}
// 		Label * label_des = Label::createWithTTF(mission_description.c_str(),"res_ui/font/simhei.ttf",18,Size(285,0),TextHAlignment::LEFT,TextVAlignment::TOP);
// 		label_des->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		auto label_des = CCRichLabel::createWithString(mission_description.c_str(),Size(285, 0),this,NULL,0,18,5);
		auto desSize = label_des->getContentSize();
		int height = desSize.height;
		//ScrollView_description
		auto m_contentScrollView = cocos2d::extension::ScrollView::create(Size(290,99));
		m_contentScrollView->setViewSize(Size(290, 99));
		m_contentScrollView->setIgnoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		m_contentScrollView->setAnchorPoint(Vec2(0,0));
		m_contentScrollView->setPosition(Vec2(30,205));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_pLayer->addChild(m_contentScrollView);
		if (height > 99)
		{
			m_contentScrollView->setContentSize(Size(290,height));
			m_contentScrollView->setContentOffset(Vec2(0,99-height));
		}
		else
		{
			m_contentScrollView->setContentSize(Size(290,99));
		}
		m_contentScrollView->setClippingToBounds(true);

		m_contentScrollView->addChild(label_des);
		label_des->setIgnoreAnchorPointForPosition(false);
		label_des->setAnchorPoint(Vec2(0,1));
		label_des->setPosition(Vec2(3,m_contentScrollView->getContentSize().height-3));
		

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(350,420));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(HandleMission::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(HandleMission::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(HandleMission::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(HandleMission::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;

}


void HandleMission::onEnter()
{
	UIScene::onEnter();

	auto action = Sequence::create(
		ScaleTo::create(0.15f*1/2,1.1f),
		ScaleTo::create(0.15f*1/3,1.0f),
		NULL);

	runAction(action);

	////this->setTouchEnabled(true);
	//this->setContentSize(Size(800,480));


}

void HandleMission::onExit()
{
	UIScene::onExit();
}


bool HandleMission::onTouchBegan(Touch *touch, Event * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	Vec2 location = touch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	return false;
}

void HandleMission::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void HandleMission::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void HandleMission::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void HandleMission::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void HandleMission::GetMissionEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (curMissionInfo->has_tip())
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("mission_notFinish"));
		}
		else
		{
			auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if (sc != NULL)
				sc->endCommand(pSender);

			GameUtils::playGameSound(MISSION_ACCEPT, 2, false);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(7002, this);
			this->closeAnim();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void HandleMission::FinishMissionEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (curMissionInfo->has_tip())
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("mission_notFinish"));
		}
		else
		{
			auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if (sc != NULL)
				sc->endCommand(pSender);

			GameMessageProcessor::sharedMsgProcessor()->sendReq(7007, (char *)curMissionInfo->missionpackageid().c_str());
			this->closeAnim();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

int HandleMission::getTargetNpcId()
{
	return targetNpcId;
}

HandleMission::handleType HandleMission::getCurHandlerType()
{
	return curHandleType;
}

void HandleMission::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void HandleMission::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	if (this->getChildByTag(HANDLEMISSION_PARTICLEATBUTTON_TAG))
	{
		this->getChildByTag(HANDLEMISSION_PARTICLEATBUTTON_TAG)->removeFromParent();
	}

	if (this->getChildByTag(HANDLEMISSION_TUTORIALINDICATOR_TAG))
	{
		this->getChildByTag(HANDLEMISSION_TUTORIALINDICATOR_TAG)->removeFromParent();
	}

	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,70,50);
// 	tutorialIndicator->setPosition(Vec2(pos.x+55,pos.y+80));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,115,52,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x,pos.y+30+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void HandleMission::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

