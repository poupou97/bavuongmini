#include "RewardItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "MissionRewardGoodsItem.h"
#include "AppMacros.h"

RewardItem::RewardItem()
{
}


RewardItem::~RewardItem()
{
}

RewardItem * RewardItem::create(GoodsInfo* goods,int num)
{
	auto rewardItem = new RewardItem();
	if (rewardItem && rewardItem->init(goods,num))
	{
		rewardItem->autorelease();
		return rewardItem;
	}
	CC_SAFE_DELETE(rewardItem);
	return NULL;
}

bool RewardItem::init(GoodsInfo* goods,int num)
{
	if (UIScene::init())
	{
		auto mrGoodsItem = MissionRewardGoodsItem::create(goods,num);
		mrGoodsItem->setAnchorPoint( Vec2( 0, 0 ) );  
		mrGoodsItem->setPosition( Vec2( 0, 0) );  
		mrGoodsItem->setScale(0.8f);
		m_pLayer->addChild(mrGoodsItem);

		label_rewardName = Label::createWithTTF(goods->name().c_str(), APP_FONT_NAME, 14);
		label_rewardName->setString(goods->name().c_str());
		label_rewardName->setIgnoreAnchorPointForPosition( false );  
		label_rewardName->setAnchorPoint( Vec2( 0, 0 ) );  
		label_rewardName->setPosition( Vec2( 50 + 10, 28) );
		auto shadowColor = Color4B::BLACK;   // black
		label_rewardName->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		this->addChild(label_rewardName);
		
		std::string str_rewardNum = "  X";
		
		char str_num[20];
		sprintf(str_num,"%d",num);
		str_rewardNum.append(str_num);
		label_rewardNum->setString(str_rewardNum.c_str());
		label_rewardNum = Label::createWithTTF(str_rewardNum.c_str(), APP_FONT_NAME, 14);
		label_rewardNum->setIgnoreAnchorPointForPosition( false );  
		label_rewardNum->setAnchorPoint( Vec2( 0, 0 ) );  
		label_rewardNum->setPosition( Vec2( 50 + 10, 0) );  
		label_rewardNum->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		this->addChild(label_rewardNum);

		this->setContentSize(Size(150,50));
		return true;
	}
	return false;
}
