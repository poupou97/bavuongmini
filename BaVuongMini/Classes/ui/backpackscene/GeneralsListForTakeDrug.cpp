#include "GeneralsListForTakeDrug.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "PacPageView.h"
#include "AppMacros.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "PackageScene.h"
#include "../../ui/extensions/Counter.h"
#include "../../messageclient/element/GoodsInfo.h"

GeneralsListForTakeDrug::GeneralsListForTakeDrug():
curRoleId(-1)
{
}


GeneralsListForTakeDrug::~GeneralsListForTakeDrug()
{
	delete curFolder;
}

GeneralsListForTakeDrug * GeneralsListForTakeDrug::create(FolderInfo * folderInfo)
{
	auto generalsListForTakeDrug = new GeneralsListForTakeDrug();
	if (generalsListForTakeDrug && generalsListForTakeDrug->init(folderInfo))
	{
		generalsListForTakeDrug->autorelease();
		return generalsListForTakeDrug;
	}
	CC_SAFE_DELETE(generalsListForTakeDrug);
	return NULL;
}

bool GeneralsListForTakeDrug::init(FolderInfo * folderInfo)
{
	if (UIScene::init())
	{
		curFolder = new FolderInfo();
		curFolder->CopyFrom(*folderInfo);
		curPackageItemIndex = folderInfo->id();

// 		Layout * mainPanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/cure_popupo_1.json");
// 		mainPanel->setAnchorPoint(Vec2(0.f,0.f));
// 		mainPanel->setPosition(Vec2(0,0));
// 		m_pLayer->addChild(mainPanel);
		auto bg = ImageView::create();
		bg->loadTexture("gamescene_state/zhujiemian3/renwuduiwu/renwu_di.png");
		bg->setAnchorPoint(Vec2::ZERO);
		bg->setPosition(Vec2::ZERO);
		bg->setScale9Enabled(true);
		bg->setCapInsets(Rect(12,12,1,1));
		bg->setContentSize(Size(204,383));
		m_pLayer->addChild(bg);

		//�佫�б
		auto generalList_tableView = TableView::create(this,Size(188,375));
		generalList_tableView->setDirection(TableView::Direction::VERTICAL);
		generalList_tableView->setAnchorPoint(Vec2(0,0));
		generalList_tableView->setPosition(Vec2(8,4));
		generalList_tableView->setContentOffset(Vec2(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setContentOffset(Vec2(10,0));
		generalList_tableView->setClippingToBounds(true);
		generalList_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(generalList_tableView);

		this->setContentSize(Size(204,383));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		return true;
	}
	return false;
}

void GeneralsListForTakeDrug::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GeneralsListForTakeDrug::onExit()
{
	UIScene::onExit();
}

bool GeneralsListForTakeDrug::onTouchBegan( Touch *touch, Event * pEvent )
{
	return resignFirstResponder(touch,this,false);
}

void GeneralsListForTakeDrug::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void GeneralsListForTakeDrug::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void GeneralsListForTakeDrug::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void GeneralsListForTakeDrug::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void GeneralsListForTakeDrug::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void GeneralsListForTakeDrug::tableCellTouched( TableView* table, TableViewCell* cell )
{
	int index = cell->getIdx();
	curRoleId = GameView::getInstance()->generalsInLineList.at(index)->id();
	
	if (curFolder->goods().clazz() != GOODS_CLASS_WUJIANGJINGYANJIU)  //28 ��佫���鵤
	{
		auto temp = new PackageScene::IdxAndNum();
		temp->index = curPackageItemIndex;
		temp->num = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)curRoleId,temp);
		delete temp;

		this->closeAnim();
	}
	else
	{
		if(curFolder->quantity() > 1)
		{
			//�����
			GameView::getInstance()->showCounter(this,callfuncO_selector(GeneralsListForTakeDrug::showCountForGeneralExp),curFolder->quantity());
		}
		else
		{
			PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
			temp->index = curPackageItemIndex;
			temp->num = 1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)curRoleId,temp);
			delete temp;

			this->closeAnim();
		}
	}

}

cocos2d::Size GeneralsListForTakeDrug::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(187,73);
}

cocos2d::extension::TableViewCell* GeneralsListForTakeDrug::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

 	cell = GeneralsListForTakeDrugCell::create(GameView::getInstance()->generalsInLineList.at(idx)); 

	return cell;
}

ssize_t GeneralsListForTakeDrug::numberOfCellsInTableView( TableView *table )
{
	return GameView::getInstance()->generalsInLineList.size();
}

void GeneralsListForTakeDrug::showCountForGeneralExp( Ref *pSender )
{
	auto counter_ =(Counter *)pSender;
	int amount_;
	if (counter_ != NULL)
	{
		amount_ =counter_->getInputNum();
		PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
		temp->index = curPackageItemIndex;
		temp->num = amount_;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)curRoleId,temp);
		delete temp;

		this->closeAnim();
	}else
	{
	}
}


/////////////////////////////////////////////////////

#define ktag_sprite_bigFrame 456
GeneralsListForTakeDrugCell::GeneralsListForTakeDrugCell()
{

}

GeneralsListForTakeDrugCell::~GeneralsListForTakeDrugCell()
{
	delete curGeneralBaseMsg;
}

GeneralsListForTakeDrugCell* GeneralsListForTakeDrugCell::create( CGeneralBaseMsg * generalBaseMsg )
{
	auto generalsListForTakeDrugCell = new GeneralsListForTakeDrugCell();
	if (generalsListForTakeDrugCell && generalsListForTakeDrugCell->init(generalBaseMsg))
	{
		generalsListForTakeDrugCell->autorelease();
		return generalsListForTakeDrugCell;
	}
	CC_SAFE_DELETE(generalsListForTakeDrugCell);
	return NULL;
}

bool GeneralsListForTakeDrugCell::init( CGeneralBaseMsg * generalBaseMsg )
{
	if (TableViewCell::init())
	{
		curGeneralBaseMsg = new CGeneralBaseMsg();
		curGeneralBaseMsg->CopyFrom(*generalBaseMsg);

		auto generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];
		//��߿
		if (this->getChildByTag(ktag_sprite_bigFrame))
		{
			this->getChildByTag(ktag_sprite_bigFrame)->removeFromParent();
		}

		auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/chongwutouxiang/di_a.png");
		sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
		sprite_bigFrame->setPosition(Vec2(0, 0));
		sprite_bigFrame->setPreferredSize(Size(187,70));
		sprite_bigFrame->setCapInsets(Rect(16,34,1,1));
		sprite_bigFrame->setTag(ktag_sprite_bigFrame);
		sprite_bigFrame->setLocalZOrder(-1);
		addChild(sprite_bigFrame);

		//�С�߿
		auto  smaillFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_allb.png");
		smaillFrame->setAnchorPoint(Vec2(0, 0));
		smaillFrame->setPosition(Vec2(10, 8));
		smaillFrame->setCapInsets(Rect(15,15,1,1));
		smaillFrame->setContentSize(Size(52,52));
		addChild(smaillFrame);

		//��б��е�ͷ��ͼ�
		std::string icon_path = "res_ui/generals46X45/";
		icon_path.append(generalMsgFromDb->get_head_photo());
		icon_path.append(".png");
		auto sprite_icon = Sprite::create(icon_path.c_str());
		sprite_icon->setAnchorPoint(Vec2(0.5f, 0.5f));
		sprite_icon->setPosition(Vec2(smaillFrame->getContentSize().width/2, smaillFrame->getContentSize().height/2));
		smaillFrame->addChild(sprite_icon);

		//�ȼ�
		auto sprite_lvFrame = cocos2d::extension::Scale9Sprite::create("res_ui/lv_kuang.png");
		sprite_lvFrame->setAnchorPoint(Vec2(0, 0));
		sprite_lvFrame->setPosition(Vec2(46, 5));
		addChild(sprite_lvFrame);

		std::string _lv = "LV";
		char s_level [5];
		sprintf(s_level,"%d",generalBaseMsg->level());
		_lv.append(s_level);
		auto label_level = Label::createWithTTF(s_level,APP_FONT_NAME,12);
		label_level->setAnchorPoint(Vec2(0.5f,0.5f));
		label_level->setPosition(Vec2(smaillFrame->getContentSize().width-2,2));
		label_level->setPosition(Vec2(sprite_lvFrame->getPosition().x+sprite_lvFrame->getContentSize().width/2,sprite_lvFrame->getPosition().y+sprite_lvFrame->getContentSize().height/2));
		addChild(label_level);
		//���
		if (generalBaseMsg->evolution() > 0)
		{
			auto imageView_rank = ActorUtils::createGeneralRankSprite(generalBaseMsg->evolution());
			imageView_rank->setScale(0.75f);
			imageView_rank->setAnchorPoint(Vec2(0.5f,0.5f));
			imageView_rank->setPosition(Vec2(59,55));
			addChild(imageView_rank);
		}

		//��佫ϡ�ж
		if (generalBaseMsg->rare()>0)
		{
			auto sprite_star = Sprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			sprite_star->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_star->setPosition(Vec2(15,15));
			sprite_star->setScale(0.5f);
			addChild(sprite_star);
		}

		//�ְҵ
		std::string str_professionIcon = "gamescene_state/zhujiemian3/chongwutouxiang/";
		switch(generalMsgFromDb->get_profession())
		{
		case 1:
			{
				str_professionIcon.append("mj.png");
			}
			break;
		case 2:
			{
				str_professionIcon.append("gm.png");
			}
			break;
		case 3:
			{
				str_professionIcon.append("hj.png");
			}
			break;
		case 4:
			{
				str_professionIcon.append("ss.png");
			}
			break;
		}
		auto sprite_profession = Sprite::create(str_professionIcon.c_str());
		sprite_profession->setAnchorPoint(Vec2(0.5f, 0.5f));
		sprite_profession->setPosition(Vec2(87, 56));
		addChild(sprite_profession);

		//�佫���
		auto sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80_new.png");
		sprite_nameFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
		sprite_nameFrame->setPosition(Vec2(137, 54));
		sprite_nameFrame->setPreferredSize(Size(73,17));
		addChild(sprite_nameFrame);
		auto label_name = Label::createWithTTF(generalMsgFromDb->name().c_str(),APP_FONT_NAME,16);
		label_name->setAnchorPoint(Vec2(0.5f, 0.5f));
		label_name->setPosition(Vec2(137, 53));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		addChild(label_name);
		//����
		auto label_hp = Label::createWithTTF("HP",APP_FONT_NAME,14);
		label_hp->setAnchorPoint(Vec2(0.5f, 0.5f));
		label_hp->setPosition(Vec2(88, 38));
		addChild(label_hp);
		auto imageView_hpBg = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/chongwutouxiang/di_c.png");
		imageView_hpBg->setPreferredSize(Size(76,11)); 
		imageView_hpBg->setAnchorPoint(Vec2(0,0.5f));
		imageView_hpBg->setPosition(Vec2(99,39));
		addChild(imageView_hpBg);
		auto imageView_hp = Sprite::create("gamescene_state/zhujiemian3/chongwutouxiang/hp_2.png");
		imageView_hp->setAnchorPoint(Vec2(0,0.5f));
		imageView_hp->setPosition(Vec2(99,40));
		addChild(imageView_hp);

		//��
		auto label_mp = Label::createWithTTF("MP",APP_FONT_NAME,14);
		label_mp->setAnchorPoint(Vec2(0.5f, 0.5f));
		label_mp->setPosition(Vec2(88, 27));
		addChild(label_mp);
		auto imageView_mpBg = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/chongwutouxiang/di_c.png");
		imageView_mpBg->setPreferredSize(Size(76,11)); 
		imageView_mpBg->setAnchorPoint(Vec2(0,0.5f));
		imageView_mpBg->setPosition(Vec2(99,26));
		addChild(imageView_mpBg);
		auto imageView_mp = Sprite::create("gamescene_state/zhujiemian3/chongwutouxiang/mp_2.png");
		imageView_mp->setAnchorPoint(Vec2(0,0.5f));
		imageView_mp->setPosition(Vec2(99,27));
		addChild(imageView_mp);

		int h_cur = 0;
		int h_all = 0;
		int m_cur = 0;
		int m_all = 0;
		for (int i = 0;i<(int)GameView::getInstance()->generalsInLineDetailList.size();++i)
		{
			if (generalBaseMsg->id() == GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
			{
				h_cur = GameView::getInstance()->generalsInLineDetailList.at(i)->activerole().hp();
				h_all = GameView::getInstance()->generalsInLineDetailList.at(i)->activerole().maxhp();
				m_cur = GameView::getInstance()->generalsInLineDetailList.at(i)->activerole().mp();
				m_all = GameView::getInstance()->generalsInLineDetailList.at(i)->activerole().maxmp();
			}
		}
		float h_scale = (h_cur*1.0f)/(h_all*1.0f);
		if (h_scale > 1.0f)
			h_scale = 1.0f;

		float m_scale = (m_cur*1.0f)/(m_all*1.0f);
		if (m_scale > 1.0f)
			m_scale = 1.0f;

		imageView_hp->setTextureRect(Rect(0,0,imageView_hp->getContentSize().width*h_scale,imageView_hp->getContentSize().height));
		imageView_mp->setTextureRect(Rect(0,0,imageView_mp->getContentSize().width*m_scale,imageView_mp->getContentSize().height));

		//�佫���
		if (h_cur <= 0)  
		{
			//�׿��
// 			SpriteFrame * spriteFrame = SpriteFrame::create("gamescene_state/zhujiemian3/chongwutouxiang/di_b.png",Rect(0,0,180,70));
// 			SpriteBatchNode * batchnode = SpriteBatchNode::createWithTexture(spriteFrame->getTexture(), 9);
//			sprite_bigFrame->updateWithBatchNode(batchnode, spriteFrame->getRect(), spriteFrame->isRotated(), Rect(150,38,1,1);
			if (this->getChildByTag(ktag_sprite_bigFrame))
			{
				this->getChildByTag(ktag_sprite_bigFrame)->removeFromParent();
			}

			auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/chongwutouxiang/di_b.png");
			sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
			sprite_bigFrame->setPosition(Vec2(0, 0));
			sprite_bigFrame->setPreferredSize(Size(187,70));
			sprite_bigFrame->setTag(ktag_sprite_bigFrame);
			sprite_bigFrame->setLocalZOrder(-1);
			addChild(sprite_bigFrame);
			auto label_died = Label::createWithTTF(StringDataManager::getString("generals_died"),APP_FONT_NAME,14);
			label_died->setAnchorPoint(Vec2(0, 0.5f));
			label_died->setPosition(Vec2(108, 13));
			addChild(label_died);
		}


		return true;
	}
	return false;
}

CGeneralBaseMsg* GeneralsListForTakeDrugCell::getCurGeneralBaseMsg()
{
	return curGeneralBaseMsg;
}
