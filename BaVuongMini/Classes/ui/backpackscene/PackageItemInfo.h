

#ifndef _BACKPACKSCENE_PACKAGEITEMINFO_H
#define _BACKPACKSCENE_PACKAGEITEMINFO_H

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "GoodsItemInfoBase.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../messageclient/element/FolderInfo.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class PackageScene;
class LoadSceneState;
class FolderInfo;
class GoodsInfo;
class PacPageView;
class PackageItemInfo : public GoodsItemInfoBase
{
public:
	PackageItemInfo();
	~PackageItemInfo();

	static PackageItemInfo * create(FolderInfo * folder,std::vector<CEquipment *>equipmentVector,long long generalId);
	bool init(FolderInfo * folder,std::vector<CEquipment *>equipmentVector,long long generalId);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void dressOnEvent(Ref *pSender, Widget::TouchEventType type);
	void sureToDressOn(Ref * pSender);

	void callBackPick(Ref *pSender, Widget::TouchEventType type);
	void callBackPickSure(Ref * obj);
	//���
	void deleteEvent(Ref *pSender, Widget::TouchEventType type);
	//������(����������OK)
	void showCountForDelete(Ref *pSender);
	void sureToDelete(Ref * pSender);

	/*add equipment*/
	void showEquipStrength(Ref * obj);
	void lvUpEvent(Ref *pSender, Widget::TouchEventType type);
	void shortCutEvent(Ref *pSender, Widget::TouchEventType type);
	void useEvent(Ref *pSender, Widget::TouchEventType type);
	void PutInEvent(Ref *pSender, Widget::TouchEventType type);
	void systhesisEvent(Ref *pSender, Widget::TouchEventType type);
	void showEquipSysthesisUI(Node *pNode,void * folderInfo);
	
	void SellEvent(Ref *pSender, Widget::TouchEventType type);
	void SellEventNum(Ref *pSender);
	
	void GetAnnesEvent(Ref *pSender, Widget::TouchEventType type);
	//void GetAuctionEvent(Ref * pSender);
	void callBackSearch(Ref *pSender, Widget::TouchEventType type);
	void callBackConsign(Ref *pSender, Widget::TouchEventType type);
	void cancelAuction(Ref * pSender);
	void showGoodsInfo(Ref *pSender, Widget::TouchEventType type);
	void showGoodsInfoPrivateui(Ref *pSender, Widget::TouchEventType type);
	void showCEquipment(Ref *pSender, Widget::TouchEventType type);

	void showStrengthStone(Ref * obj);

	void useAllEvent(Ref *pSender);

	void setCurEquipIsUseAble(bool useAble);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

private:
	struct assistEquipStruct
	{
		int source_;
		int  pob_;
		long long petid_;
		int index_;
		int playType;
	};
// 	char * curItemName;
// 
// 	ImageView * ImageView_head;
// 	Label * Label_name;
// 	Label * Label_type;
// 	Label * Label_price;
// 	Label * Label_useLevel;
	std::string buttonImagePath;

	//㽫Ҫ��������
	int deleteAmount;

public:
	GoodsInfo * curGoods;
	FolderInfo * curFolders;
public:
	//Ϊ�˽�ѧ��� ��ȡ��װ����ť��λ�
	//Button * btn_addMainEquip;
	Button * Button_dressOn;

	//ý�ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	//��ѧ
	int mTutorialScriptInstanceId;
};
#endif;
