
#ifndef _BACKPACKSCENE_USEMISSIONPROPUI_H_
#define _BACKPACKSCENE_USEMISSIONPROPUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"

/******
des : ʹ�ý�ȡ�����ߺ󵯳�Ľ�������
auto : yangjun 
date : 2014.08.22
*********/

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CMissionReward;

class UseMissionPropUI : public UIScene
{
public:

	UseMissionPropUI();
	~UseMissionPropUI();

	static UseMissionPropUI* create(int npcId,const char * des,CMissionReward * missionReward);
	bool init(int npcId,const char * des,CMissionReward * missionReward);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);


	std::string rewardIconName[5];
	std::string rewardLabelName[5];

	int getTargetNpcId();

public: 
	Button * Button_getMission;
	Button * Button_finishMission;
private:
	int targetNpcId;
	Button * Button_Handle;
	cocos2d::extension::ScrollView * m_scrollView;
	Label * m_describeLabel;
	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void GetMissionEvent(Ref *pSender, Widget::TouchEventType type);
	void FinishMissionEvent(Ref * pSender);

	ImageView * reward_di_1;
	ImageView * reward_di_2;
	ImageView * reward_di_3;
	ImageView * reward_di_4;
	ImageView * reward_di_5;
};
#endif;

