#include "EquipmentItem.h"
#include "GameView.h"
#include "AppMacros.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "PackageScene.h"
#include "../../messageclient/element/CEquipment.h"
#include "GoodsItemInfoBase.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "EquipmentItemInfos.h"
#include "../../utils/StaticDataManager.h"

#define  MengBan_Red_Name "mengban_red"

EquipmentItem::EquipmentItem() :
index(0)
{
}


EquipmentItem::~EquipmentItem()
{
	delete curEquipment;
}

EquipmentItem * EquipmentItem::create(CEquipment* equipment)
{
	auto widget = new EquipmentItem();
	if (widget && widget->init(equipment))
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}

bool EquipmentItem::init(CEquipment* equipment)
{
	if (Widget::init())
	{
	curEquipment = new CEquipment();
	curEquipment->CopyFrom(*equipment);

	if (equipment->has_goods() == true)
	{
		/*********************�ж�װ������ɫ***************************/
		std::string frameColorPath;
		if (equipment->goods().equipmentdetail().quality() == 1)
		{
			frameColorPath = EquipWhiteFramePath;
		}
		else if (equipment->goods().equipmentdetail().quality() == 2)
		{
			frameColorPath = EquipGreenFramePath;
		}
		else if (equipment->goods().equipmentdetail().quality() == 3)
		{
			frameColorPath = EquipBlueFramePath;
		}
		else if (equipment->goods().equipmentdetail().quality() == 4)
		{
			frameColorPath = EquipPurpleFramePath;
		}
		else if (equipment->goods().equipmentdetail().quality() == 5)
		{
			frameColorPath = EquipOrangeFramePath;
		}
		else
		{
			frameColorPath = EquipVoidFramePath;
		}

		auto Btn_goodsItemFrame = Button::create();
		Btn_goodsItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_goodsItemFrame->setTouchEnabled(true);
		Btn_goodsItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_goodsItemFrame->setPosition(Vec2(Btn_goodsItemFrame->getContentSize().width/2,Btn_goodsItemFrame->getContentSize().height/2));
		Btn_goodsItemFrame->setPressedActionEnabled(true);
		Btn_goodsItemFrame->addTouchEventListener(CC_CALLBACK_2(EquipmentItem::EquipmentItemEvent, this));
		this->addChild(Btn_goodsItemFrame);

		auto uiiImageView_goodsItem = ImageView::create();
		std::string iconPath = "res_ui/props_icon/";
		iconPath.append(equipment->goods().icon().c_str());
		iconPath.append(".png");
		uiiImageView_goodsItem->loadTexture(iconPath.c_str());
		uiiImageView_goodsItem->setScale(0.9f);
		uiiImageView_goodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
		uiiImageView_goodsItem->setPosition(Vec2(0,0));
		Btn_goodsItemFrame->addChild(uiiImageView_goodsItem);

		if (equipment->goods().binding() == 1)
		{
			auto ImageView_bound = ImageView::create();
			ImageView_bound->loadTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(Vec2(0,1.0f));
			ImageView_bound->setScale(0.85f);
			ImageView_bound->setPosition(Vec2(7-Btn_goodsItemFrame->getContentSize().width/2,56-Btn_goodsItemFrame->getContentSize().height/2));
			Btn_goodsItemFrame->addChild(ImageView_bound);
		}

		//�����װ��
		if (equipment->goods().has_equipmentdetail())
		{
			if (equipment->goods().equipmentdetail().gradelevel() > 0)  //����ȼ�
			{
				std::string ss_gradeLevel = "+";
				char str_gradeLevel [10];
				sprintf(str_gradeLevel,"%d",equipment->goods().equipmentdetail().gradelevel());
				ss_gradeLevel.append(str_gradeLevel);
				auto Lable_gradeLevel = Label::createWithTTF(ss_gradeLevel.c_str(), APP_FONT_NAME, 13);
				Lable_gradeLevel->setAnchorPoint(Vec2(1.0f,1.0f));
				Lable_gradeLevel->setPosition(Vec2(Btn_goodsItemFrame->getContentSize().width/2-8,Btn_goodsItemFrame->getContentSize().height/2-6));
				Lable_gradeLevel->setName("Lable_gradeLevel");
				Btn_goodsItemFrame->addChild(Lable_gradeLevel);
			}

			if (equipment->goods().equipmentdetail().starlevel() > 0)  //�Ǽ�
			{
				char str_starLevel [10];
				sprintf(str_starLevel,"%d",equipment->goods().equipmentdetail().starlevel());
				auto Lable_starLevel = Label::createWithTTF(str_starLevel, APP_FONT_NAME, 13);
				Lable_starLevel->setAnchorPoint(Vec2(0,0));
				Lable_starLevel->setPosition(Vec2(7-Btn_goodsItemFrame->getContentSize().width/2,5-Btn_goodsItemFrame->getContentSize().height/2));
				Lable_starLevel->setName("Lable_starLevel");
				Btn_goodsItemFrame->addChild(Lable_starLevel);

				auto ImageView_star = ImageView::create();
				ImageView_star->loadTexture("res_ui/star_on.png");
				ImageView_star->setAnchorPoint(Vec2(0,0));
				ImageView_star->setPosition(Vec2(Lable_starLevel->getContentSize().width,2));
				ImageView_star->setVisible(true);
				ImageView_star->setScale(0.5f);
				Lable_starLevel->addChild(ImageView_star);
			}
		}


		if (equipment->goods().equipmentdetail().durable()> 10)
		{
			this->setRed(false);
		}
		else
		{
			this->setRed(true);
		}

		this->setAnchorPoint(Vec2::ZERO);
		this->setContentSize(Size(63,63));
		this->setLocalZOrder(100);
		switch(equipment->part())
		{
		case 0 :
			this->setPosition(Vec2(311, 342));    //ͷ�
			break;
		case 1 :
			this->setPosition(Vec2(284, 144));  
			break;
		case 2 :
			this->setPosition(Vec2(67, 273));   //��·
			break;
		case 3 :
			this->setPosition(Vec2(311, 205));    // ��
			break;
		case 4 :
			this->setPosition(Vec2(67, 342));    //����
			break;
		case 5 :
			this->setPosition(Vec2(284, 79));
			break;
		case 6 :
			this->setPosition(Vec2(29, 79));
			break;
		case 7 :
			this->setPosition(Vec2(67, 205));    //��ָ
			break;
		case 8 :
			this->setPosition(Vec2(29, 144));   
			break;
		case 9 :
			this->setPosition(Vec2(311, 273));    //Ь
			break;
		case 10 :
			this->setPosition(Vec2(29, 79));
			break;
		}
	}
	else
	{
	}

	return true;
	}
	return false;
}

void EquipmentItem::EquipmentItemEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto pScene = Director::getInstance()->getRunningScene();
		auto uiScene = (Layer*)pScene->getChildren().at(0);
		auto winSize = Director::getInstance()->getVisibleSize();

		auto packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
		//packageScene->curEquipmentIndex = curEquipment->id();
		packageScene->curEquipment = curEquipment;
		// 	GoodsInfo * goodsinfo =new GoodsInfo();
		// 	goodsinfo->CopyFrom(curEquipment->goods());
		auto equipmentItemInfos = EquipmentItemInfos::create(curEquipment, packageScene->selectActorId);
		equipmentItemInfos->setIgnoreAnchorPointForPosition(false);
		equipmentItemInfos->setAnchorPoint(Vec2(0.5f, 0.5f));
		//equipmentItemInfos->setPosition(Vec2(winSize.width/2,winSize.height/2));
		equipmentItemInfos->setTag(kTagGoodsItemInfoBase);
		GameView::getInstance()->getMainUIScene()->addChild(equipmentItemInfos);
		//��¼���װ��ʱ��ʱ���佫id
		equipmentItemInfos->pacTabviewSelectindex = packageScene->selectTabviewIndex;
		/*	delete goodsinfo;*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}


Vec2 EquipmentItem::autoGetPosition(Ref * pSender)
{
	auto winSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Vec2 cur_positon = ((EquipmentItem*)(((Button*)pSender)->getParent()))->getPosition();
	Size cur_size = Size(63,63);
	if (cur_positon.x+cur_size.width+272+(winSize.width-designResolutionSize.width) < winSize.width)
	{
			//���Ҳ��м���ʾ	
			int x= cur_positon.x+cur_size.width+(winSize.width-designResolutionSize.width)/2;
			int y = (winSize.height-360)/2;
			return Vec2(x,y);
	}
	return Vec2(winSize.width/2,winSize.height/2);
}

void EquipmentItem::setRed( bool isRed )
{
	if (this->getChildByName(MengBan_Red_Name))
	{
		this->getChildByName(MengBan_Red_Name)->removeFromParent();
	}

	if (isRed)
	{
		auto m_mengban_red = ImageView::create();
		m_mengban_red->loadTexture("res_ui/mengban_red55.png");
		m_mengban_red->setScale9Enabled(true);
		m_mengban_red->setContentSize(Size(63,63));
		m_mengban_red->setCapInsets(Rect(5,5,1,1));
		m_mengban_red->setName(MengBan_Red_Name);
		m_mengban_red->setAnchorPoint(Vec2(0,0));
		m_mengban_red->setPosition(Vec2(0,0));
		this->addChild(m_mengban_red);

		//add lable
		Label * l_des = Label::createWithTTF(StringDataManager::getString("packageitem_notAvailable"), APP_FONT_NAME, 18);
		l_des->setAnchorPoint(Vec2(0.5f,0.5f));
		l_des->setPosition(Vec2(m_mengban_red->getContentSize().width/2,m_mengban_red->getContentSize().height/2));
		m_mengban_red->addChild(l_des);
	}
	else
	{
	}
}
