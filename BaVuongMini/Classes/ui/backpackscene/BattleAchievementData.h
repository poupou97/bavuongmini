#ifndef _UI_BACKPACKSCENE_BATTLEACHIEVEMENTDATA_H_
#define _UI_BACKPACKSCENE_BATTLEACHIEVEMENTDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ս��ģ�飨���ڱ�����������ݣ�
 * @author liuliang
 * @version 0.1.0
 * @date 2014.08.08
 */
class CPkDailyReward;

class BattleAchievementData
{
public:
	static BattleAchievementData * s_battleAchData;
	static BattleAchievementData * instance();

private:
	BattleAchievementData(void);
	~BattleAchievementData(void);	

private:
	int m_nRank;						// ����
	bool m_bHasGetGift;					// �Ƿ���ȡ����
	
public:
	std::vector<CPkDailyReward *> m_vector_reward;

	void initVectorFromInternet();


public:
	void set_rank(int nRank);
	int get_rank();

	void set_hasGetGift(bool bFlag);
	bool get_hasGetGift();
};

#endif

