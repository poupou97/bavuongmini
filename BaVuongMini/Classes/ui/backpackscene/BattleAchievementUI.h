#ifndef BACKPACKSCENEUI_BATTLEACHIEVEMENTUI_H
#define BACKPACKSCENEUI_BATTLEACHIEVEMENTUI_H

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class BattleAchievementUI : public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	BattleAchievementUI();
	~BattleAchievementUI();

	static BattleAchievementUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void ReloadTableViewWithOutChangeOffSet();

	void initDataFromInternet();

private:
	Layer * m_base_layer;
	TableView * m_tableView;
	Text * m_label_rank;

private:
	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void getRewardEvent(Ref *pSender, Widget::TouchEventType type);
};

#endif

