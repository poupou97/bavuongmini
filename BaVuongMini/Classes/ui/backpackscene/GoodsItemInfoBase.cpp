#include "GoodsItemInfoBase.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GameView.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/CEquipProperty.h"
#include "../../utils/StaticDataManager.h"
#include "../generals_ui/GeneralsUI.h"
#include "../generals_ui/GeneralsStrategiesUI.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../gamescene_state/MainScene.h"
#include "PackageScene.h"
#include "../../ui/equipMent_ui/EquipMentGeneralList.h"
#include "../../ui/extensions/QuiryUI.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../ui/Chat_ui/ChatUI.h"
#include "../../ui/Chat_ui/BackPackage.h"
#include "../../utils/GameUtils.h"
#include "PackageItemInfo.h"
#include "../../ui/extensions/ShowSystemInfo.h"
#include "AppMacros.h"

GoodsItemInfoBase::GoodsItemInfoBase()
{
	selfEquipSuit = 0;
	isSetSuitColor =false;
	isRoleEquip_ =false;
	buttonImagePath = "res_ui/new_button_1.png";
}


GoodsItemInfoBase::~GoodsItemInfoBase()
{
	delete goodsInfo_General;

	std::vector<CEquipment*>::iterator iter;
	for (iter = selfEquipVector.begin(); iter != selfEquipVector.end(); ++iter)
	{
		delete *iter;
	}
	selfEquipVector.clear();
}

GoodsItemInfoBase * GoodsItemInfoBase::create(GoodsInfo * goodsInfo,std::vector<CEquipment *> curEquipment,long long generalId /*= 0*/ )
{
	GoodsItemInfoBase * goodsbase= new GoodsItemInfoBase();
	if (goodsbase && goodsbase->init(goodsInfo,curEquipment,generalId))
	{
		goodsbase->autorelease();
		return goodsbase;
	}
	CC_SAFE_DELETE(goodsbase);
	return NULL;
}

bool GoodsItemInfoBase::init(GoodsInfo * goodsInfo,std::vector<CEquipment *> curEquipment,long long generalId)
{
	if (UIScene::init())
	{	
		Size size= Director::getInstance()->getVisibleSize();
		goodsInfo_General =new GoodsInfo();
		goodsInfo_General->CopyFrom(*goodsInfo);

// 		if (goodsInfo->equipmentclazz()>0 && goodsInfo->equipmentclazz()<11)
// 		{
// 			ImageView *mengban = ImageView::create();
// 			mengban->loadTexture("res_ui/zhezhao80.png");
// 			mengban->setScale9Enabled(true);
// 			mengban->setContentSize(size);
// 			mengban->setAnchorPoint(Vec2(0.5f,0.5f));
// 			mengban->setPosition(Vec2(548/2+40,362/2));
// 			m_pLayer->addChild(mengban);
// 		}
		
		generalLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
		generalProfession = BasePlayer::getProfessionIdxByName(GameView::getInstance()->myplayer->getActiveRole()->profession());
	
		auto imageFrame =ImageView::create();
		imageFrame->loadTexture("res_ui/zhezhao01.png");
		imageFrame->setAnchorPoint(Vec2(0,0));
		imageFrame->setScale9Enabled(true);
		imageFrame->setContentSize(Size(274,362));
		imageFrame->setCapInsets(Rect(50,50,1,1));
		imageFrame->setPosition(Vec2(0,0));
		m_pLayer->addChild(imageFrame);
		
		scrollView_info = ui::ScrollView::create();
		scrollView_info->setTouchEnabled(true);
		scrollView_info->setDirection(ui::ScrollView::Direction::VERTICAL);
		scrollView_info->setContentSize(Size(274,303));
		scrollView_info->setPosition(Vec2(0,54));
		scrollView_info->setBounceEnabled(true);
		scrollView_info->setLocalZOrder(10);
		m_pLayer->addChild(scrollView_info);

		touchPanelInfo = Layout::create();
		touchPanelInfo->setContentSize(Size(274,304));
		touchPanelInfo->setAnchorPoint(Vec2(0,1));
		touchPanelInfo->setTouchEnabled(true);
		touchPanelInfo->setLocalZOrder(10);
		scrollView_info->addChild(touchPanelInfo);

		if (goodsInfo->equipmentclazz()>0 && goodsInfo->equipmentclazz()<11)
		{
			auto bodyImageFrame =ImageView::create();
			bodyImageFrame->loadTexture("res_ui/zhezhao01.png");
			bodyImageFrame->setScale9Enabled(true);
			bodyImageFrame->setContentSize(Size(274,362));
			bodyImageFrame->setCapInsets(Rect(50,50,1,1));
			bodyImageFrame->setAnchorPoint(Vec2(0,0));
			bodyImageFrame->setPosition(Vec2(274,0));
			m_pLayer->addChild(bodyImageFrame);

			//if role lv is not enouhg not show generalbutton
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(26);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[26];
			}
			if (GameView::getInstance()->myplayer->getActiveRole()->level() >= openlevel)
			{
				this->addGeneralButton(generalId);
			}

			this->getSelfEquipVector(goodsInfo,curEquipment);
			
			this->showTouchEquipMentInfo(goodsInfo,0);

			flag_toucheSelfEquip =ImageView::create();
			flag_toucheSelfEquip->loadTexture("res_ui/yichuzhan.png");
			flag_toucheSelfEquip->setAnchorPoint(Vec2(1,1));
			flag_toucheSelfEquip->setPosition(Vec2(touchPanelInfo->getContentSize().width,360));
			flag_toucheSelfEquip->setLocalZOrder(10);
			m_pLayer->addChild(flag_toucheSelfEquip);
			flag_toucheSelfEquip->setVisible(false);
			this->isTouchSameEquipment();

			scrollView_info->setContentSize(Size(548,304));

			bodyPanelInfo = Layout::create();
			bodyPanelInfo->setContentSize(Size(274,304));
			bodyPanelInfo->setAnchorPoint(Vec2(0,1));
			bodyPanelInfo->setPosition(Vec2(touchPanelInfo->getContentSize().width,scrollView_info->getContentSize().height));
			bodyPanelInfo->setTouchEnabled(true);
			bodyPanelInfo->setLocalZOrder(10);
			scrollView_info->addChild(bodyPanelInfo);

			const char *strings_addMain = StringDataManager::getString("equipinfo_notEquip");
			notEquipment =Label::createWithTTF(strings_addMain, APP_FONT_NAME, 20);
			notEquipment->setAnchorPoint(Vec2(0.5f,0.5f));
			notEquipment->setPosition(Vec2(2*touchPanelInfo->getContentSize().width*3/4,200));
			m_pLayer->addChild(notEquipment);

			flag_equip =ImageView::create();
			flag_equip->loadTexture("res_ui/yichuzhan.png");
			flag_equip->setAnchorPoint(Vec2(1,1));
			flag_equip->setPosition(Vec2(2*touchPanelInfo->getContentSize().width,360));
			flag_equip->setLocalZOrder(10);
			m_pLayer->addChild(flag_equip);
		
			for (int i=0;i<curEquipment.size();i++)
			{
				if (goodsInfo->equipmentclazz()==curEquipment.at(i)->goods().equipmentclazz())
				{
					auto playerEquip = new GoodsInfo();
					playerEquip->CopyFrom(curEquipment.at(i)->goods());
					isRoleEquip_ = true;
					isSetSuitColor = true;
					this->showTouchEquipMentInfo(playerEquip,1);
					delete playerEquip;
					break;
				}
			}
			
			if (isRoleEquip_ ==false)
			{
				flag_equip->setVisible(false);
				notEquipment->setVisible(true);
			}else
			{
				flag_equip->setVisible(true);
				notEquipment->setVisible(false);
			}

			int width = touchPanelInfo->getContentSize().width + bodyPanelInfo->getContentSize().width;
			if (touchPanelInfo->getContentSize().height > bodyPanelInfo->getContentSize().height)
			{
				touchPanelInfo->setPosition(Vec2(0,touchPanelInfo->getContentSize().height));
				bodyPanelInfo->setPosition(Vec2(touchPanelInfo->getContentSize().width,touchPanelInfo->getContentSize().height));
				scrollView_info->setInnerContainerSize(Size(width, touchPanelInfo->getContentSize().height));
			}else
			{
				touchPanelInfo->setPosition(Vec2(0,bodyPanelInfo->getContentSize().height));
				bodyPanelInfo->setPosition(Vec2(touchPanelInfo->getContentSize().width,bodyPanelInfo->getContentSize().height));
				scrollView_info->setInnerContainerSize(Size(width, bodyPanelInfo->getContentSize().height));
			}
			////this->setTouchEnabled(true);
			////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
			this->setContentSize(Size(548,362));

			if (GameView::getInstance()->myplayer->getActiveRole()->level() >= openlevel)
			{
				this->setPosition(Vec2(size.width/2 - 40,size.height/2)); 
			}else
			{
				this->setPosition(Vec2(size.width/2,size.height/2)); 
			}

		}else
		{
			this->showTouchGoodsInfo(goodsInfo);
			touchPanelInfo->setPosition(Vec2(0,touchPanelInfo->getContentSize().height));
			scrollView_info->setInnerContainerSize(Size(touchPanelInfo->getContentSize().width, touchPanelInfo->getContentSize().height));
			
			////this->setTouchEnabled(true);
			////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
			this->setContentSize(Size(274,362));
			this->setPosition(Vec2(size.width/2,size.height/2));
		}

		return true;
	}
	return false;
}
void GoodsItemInfoBase::showTouchGoodsInfo(GoodsInfo * goodsInfo)
{
	Size size= scrollView_info->getInnerContainerSize();

	int quality_ =goodsInfo->quality();
	std::string qualityStr_ = getEquipmentQualityByIndex(quality_);
	auto headImageFrame =ImageView::create();
	headImageFrame->loadTexture(qualityStr_.c_str());
	headImageFrame->setScale9Enabled(true);
	headImageFrame->setContentSize(Size(50,50));
	headImageFrame->setCapInsets(Rect(1.5f,1.5f,1.0f,1.0f));
	headImageFrame->setAnchorPoint(Vec2(0.5f,0.5f));
	touchPanelInfo->addChild(headImageFrame);

	std::string headImageStr ="res_ui/props_icon/";
	headImageStr.append(goodsInfo->icon());
	headImageStr.append(".png");

	headImage_goods = ImageView::create();
	headImage_goods->loadTexture(headImageStr.c_str());
	touchPanelInfo->addChild(headImage_goods);

	Color3B color_ =getEquipmentColorByQuality(quality_);
	label_goods_name=Label::createWithTTF(goodsInfo->name().c_str(), APP_FONT_NAME, 16);
	label_goods_name->setAnchorPoint(Vec2(0,0));
	label_goods_name->setColor(color_);
	touchPanelInfo->addChild(label_goods_name);

	int goodsBingding_ = goodsInfo->binding();
	auto binding_label=Label::createWithTTF(" ", APP_FONT_NAME, 16);
	binding_label->setAnchorPoint(Vec2(0,0));
	if (goodsBingding_ == 1)
	{
		const char *string = StringDataManager::getString("equipInfo_bingding");
		binding_label->setString(string);
		binding_label->setColor(Color3B(255,0,0));
	}else
	{
		const char *string = StringDataManager::getString("equipInfo_notBingding");
		binding_label->setString(string);
		binding_label->setColor(Color3B(153,248,167));
	}
	touchPanelInfo->addChild(binding_label);

	///////////////////////////////////////////
	auto boundary_first= ImageView::create();
	boundary_first->setAnchorPoint(Vec2(0.5f,0.5f));
	boundary_first->loadTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
	touchPanelInfo->addChild(boundary_first);

	const char *str_uselevel = StringDataManager::getString("equipment_info_useLevel");
	auto m_goodsuselevel=Label::createWithTTF(str_uselevel, APP_FONT_NAME, 16);
	m_goodsuselevel->setAnchorPoint(Vec2(0,0));
	m_goodsuselevel->setColor(Color3B(101,255,221));
	touchPanelInfo->addChild(m_goodsuselevel);

	int gradeId = goodsInfo->uselevel();
	char * gradeStr =new char[10];
	sprintf(gradeStr,"%d",gradeId);
	label_goods_uselevel=Label::createWithTTF(gradeStr, APP_FONT_NAME, 16);
	label_goods_uselevel->setAnchorPoint(Vec2(0,0));
	label_goods_uselevel->setColor(Color3B(101,255,221));
	touchPanelInfo->addChild(label_goods_uselevel);
	delete []gradeStr;

	const char *str_price = StringDataManager::getString("goods_info_price");
	auto m_goodsPrice=Label::createWithTTF(str_price, APP_FONT_NAME, 16);
	m_goodsPrice->setAnchorPoint(Vec2(0,0));
	m_goodsPrice->setColor(Color3B(101,255,221));
	touchPanelInfo->addChild(m_goodsPrice);

	int priceId= goodsInfo->sellprice();
	char priceStr[10];
	sprintf(priceStr,"%d",priceId);
	label_goodsPrice=Label::createWithTTF(priceStr, APP_FONT_NAME, 16);
	label_goodsPrice->setAnchorPoint(Vec2(0,0));
	label_goodsPrice->setColor(Color3B(101,255,221));
	touchPanelInfo->addChild(label_goodsPrice);

	auto image_SellPriceType = ImageView::create();
	image_SellPriceType->setAnchorPoint(Vec2(0,0));
	image_SellPriceType->loadTexture("res_ui/coins.png");
	touchPanelInfo->addChild(image_SellPriceType);
	///////////////////////////////////////////
	auto boundary_second= ImageView::create();
	boundary_second->setAnchorPoint(Vec2(0.5f,0.5f));
	boundary_second->loadTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
	touchPanelInfo->addChild(boundary_second);
	
	auto textArea = TextField::create();
	textArea->setTextAreaSize(Size(250, 90));
	textArea->setAnchorPoint(Vec2(0,0.5f));
	textArea->setTextHorizontalAlignment(TextHAlignment::LEFT);
	textArea->setTextVerticalAlignment(TextVAlignment::TOP);
	textArea->setString(goodsInfo->desc().c_str());
	textArea->setFontSize(16);
	textArea->setColor(Color3B(228,173,4));
	touchPanelInfo->addChild(textArea);        

	int touchFirst =headImage_goods->getBoundingBox().size.height + boundary_first->getBoundingBox().size.height;
	int touchSecond = m_goodsuselevel->getBoundingBox().size.height+ m_goodsPrice->getBoundingBox().size.height;
	int toucheThird = boundary_second->getBoundingBox().size.height + textArea->getBoundingBox().size.height;
	//int touchHight_ = touchFirst + touchSecond + toucheThird;
	touchHight=touchFirst + touchSecond + toucheThird;
	int innerWidth=scrollView_info->getBoundingBox().size.width;
	int innerHeight=scrollView_info->getBoundingBox().size.height + touchHight/2;

	if (touchHight <= scrollView_info->getBoundingBox().size.height)
	{
		innerHeight =scrollView_info->getBoundingBox().size.height;
	}

	scrollView_info->setInnerContainerSize(Size(innerWidth,innerHeight));
	touchPanelInfo->setContentSize(Size(innerWidth,innerHeight));
	headImage_goods->setPosition(Vec2(43,innerHeight-39));
	headImageFrame->setPosition(Vec2(43,innerHeight-39));
	label_goods_name->setPosition(Vec2(100,innerHeight-40));
	binding_label->setPosition(Vec2(100,label_goods_name->getPosition().y-20));
	boundary_first->setPosition(Vec2(size.width/2,headImage_goods->getPosition().y - headImage_goods->getBoundingBox().size.height/2 -10));
	m_goodsuselevel->setPosition(Vec2(16,boundary_first->getPosition().y - 28));
	label_goods_uselevel->setPosition(Vec2(m_goodsuselevel->getContentSize().width+20,boundary_first->getPosition().y - 28));
	m_goodsPrice->setPosition(Vec2(16,m_goodsuselevel->getPosition().y - 16));
	label_goodsPrice->setPosition(Vec2(m_goodsPrice->getContentSize().width+20,label_goods_uselevel->getPosition().y - 16));
	image_SellPriceType->setPosition(Vec2(label_goodsPrice->getPosition().x +label_goodsPrice->getContentSize().width + 5,label_goodsPrice->getPosition().y));
	boundary_second->setPosition(Vec2(size.width/2,m_goodsPrice->getPosition().y - 10));
	textArea->setPosition(Vec2(20,boundary_second->getPosition().y - textArea->getContentSize().height/2-15));
}

void GoodsItemInfoBase::showTouchEquipMentInfo(GoodsInfo * goodsInfo,int index)
{
	bool isHavesuibt =false;

	int w_space = 30;

	baseValueNum=0;
	starValueNum=0;
	activeValueNum=0;
	gemValueNum=0;
	suitValueNum =0;
	Layout * panelbackGround;
	if (index == 0)
	{
		panelbackGround =touchPanelInfo;
	}else
	{
		panelbackGround =bodyPanelInfo;
	}

	int quality_ = goodsInfo->equipmentdetail().quality();
	std::string qualityStr_ = getEquipmentQualityByIndex(quality_);

	auto btn_frame = Button::create();
	btn_frame->loadTextures(qualityStr_.c_str(),qualityStr_.c_str(),"");
	btn_frame->setTouchEnabled(true);
	btn_frame->setPressedActionEnabled(true);
	btn_frame->addTouchEventListener(CC_CALLBACK_2(GoodsItemInfoBase::callBackQualityDes, this));
	btn_frame->setAnchorPoint(Vec2(0.5f,0.5f));
	panelbackGround->addChild(btn_frame);

	std::string headImageStr ="res_ui/props_icon/";
	headImageStr.append(goodsInfo->icon());
	headImageStr.append(".png");
	auto headImage_equip= ImageView::create();
	headImage_equip->loadTexture(headImageStr.c_str());
	headImage_equip->setAnchorPoint(Vec2(0.5f,0.5f));
	headImage_equip->setPosition(Vec2(0,0));
	btn_frame->addChild(headImage_equip);

	int refineLv_ = goodsInfo->equipmentdetail().gradelevel();
	if (refineLv_ > 0)
	{
		char equipRefineStr[5];
		sprintf(equipRefineStr,"%d",refineLv_);
		std::string showRefineLv = "+";
		showRefineLv.append(equipRefineStr);
		auto label_refine = Label::createWithTTF(showRefineLv.c_str(), APP_FONT_NAME, 12);
		label_refine->setAnchorPoint(Vec2(1,1));
		label_refine->setPosition(Vec2(btn_frame->getContentSize().width/2 - 4,btn_frame->getContentSize().height/2 -1));
		btn_frame->addChild(label_refine);
	}

	int starLv_ = goodsInfo->equipmentdetail().starlevel();
	if (starLv_ > 0)
	{
		char equipStarStr[5];
		sprintf(equipStarStr,"%d",starLv_);
		auto label_star = Label::createWithTTF(equipStarStr, APP_FONT_NAME, 12);
		label_star->setAnchorPoint(Vec2(0,0));
		label_star->setPosition(Vec2(-btn_frame->getContentSize().width/2+2,-btn_frame->getContentSize().height/2+2));
		btn_frame->addChild(label_star);

		auto starlevel_int = ImageView::create();
		starlevel_int->loadTexture("res_ui/star_on.png");
		starlevel_int->setAnchorPoint(Vec2(0,0));
		starlevel_int->setPosition(Vec2(label_star->getPosition().x+label_star->getContentSize().width,label_star->getPosition().y));
		starlevel_int->setScale(0.5f);
		btn_frame->addChild(starlevel_int);
	}

	int goodsBingding_ = goodsInfo->binding();
	if (goodsBingding_)
	{
		auto Image_Bingding= ImageView::create();
		Image_Bingding->loadTexture("res_ui/binding.png");
		Image_Bingding->setAnchorPoint(Vec2(0,1));
		Image_Bingding->setPosition(Vec2(-btn_frame->getContentSize().width/2+2,btn_frame->getContentSize().height/2 - 2));
		Image_Bingding->setScale(0.8f);
		btn_frame->addChild(Image_Bingding);
	}
	
 	std::string showgrade_ =goodsInfo->name();
// 	int userGrade_ =goodsInfo->equipmentdetail().gradelevel();
// 	if (userGrade_ > 0)
// 	{
// 		char userGradeStr[5];
// 		sprintf(userGradeStr,"%d",userGrade_);
// 		showgrade_.append("(+");
// 		showgrade_.append(userGradeStr);
// 		showgrade_.append(")");
// 	}
	Color3B equipmentColor_ = getEquipmentColorByQuality(quality_);
	//updata color
	auto label_equipName=Label::createWithTTF(showgrade_.c_str(), APP_FONT_NAME, 16);
	label_equipName->setAnchorPoint(Vec2(0,0));
	label_equipName->setColor(equipmentColor_);
	panelbackGround->addChild(label_equipName);
	
	const char *str_profession = StringDataManager::getString("equipment_info_profession");
	auto m_profession=Label::createWithTTF(str_profession, APP_FONT_NAME, 16);
	m_profession->setAnchorPoint(Vec2(0,0));
	m_profession->setColor(Color3B(39,238,194));
	panelbackGround->addChild(m_profession);

	std::string professionName = BasePlayer::getProfessionNameIdxByIndex(goodsInfo->equipmentdetail().profession());
	auto label_profession=Label::createWithTTF(professionName.c_str(), APP_FONT_NAME, 16);
	label_profession->setAnchorPoint(Vec2(0,0));
	label_profession->setName("toucheEquipPression");
	label_profession->setColor(Color3B(39,238,194));
	panelbackGround->addChild(label_profession);

	auto binding_label=Label::createWithTTF(" ", APP_FONT_NAME, 16);
	binding_label->setAnchorPoint(Vec2(0,0));
	if (goodsBingding_ == 1)
	{
		const char *string = StringDataManager::getString("equipInfo_bingding");
		binding_label->setString(string);
		binding_label->setColor(Color3B(255,0,0));
	}else
	{
		const char *string = StringDataManager::getString("equipInfo_notBingding");
		binding_label->setString(string);
		binding_label->setColor(Color3B(153,248,167));
	}
	panelbackGround->addChild(binding_label);

	int imageStarIconHigh_ = 24;
	auto boundary_Zero= ImageView::create();
	boundary_Zero->setAnchorPoint(Vec2(0.5f,0.5f));
	boundary_Zero->loadTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
	panelbackGround->addChild(boundary_Zero);
	
	//scrollview high and wight
	for (int i=0;i<goodsInfo->equipmentdetail().properties_size();i++)
	{
		if (goodsInfo->equipmentdetail().properties(i).type()==5 || goodsInfo->equipmentdetail().properties(i).type()==6 )
		{
			touchHight =boundary_Zero->getBoundingBox().size.height *6+binding_label->getBoundingBox().size.height*30 +imageStarIconHigh_+290;
			isHavesuibt = true;
		}else
		{
			touchHight =boundary_Zero->getBoundingBox().size.height *5 +binding_label->getBoundingBox().size.height*24+imageStarIconHigh_+165;
		}
	}
	
	int innerWidth=scrollView_info->getBoundingBox().size.width;
	int innerHeight=scrollView_info->getBoundingBox().size.height + touchHight/2;
	scrollView_info->setInnerContainerSize(Size(innerWidth,innerHeight));

	panelbackGround->setContentSize(Size(274,innerHeight));
	
	int indexWidth =panelbackGround->getContentSize().width;
	btn_frame->setPosition(Vec2(58,innerHeight-40));//44
	//headImage_equip->setPosition(Vec2(44 ,innerHeight-38));
	label_equipName->setPosition(Vec2(104,innerHeight-32));//90
	m_profession->setPosition(Vec2(104 ,innerHeight-50));
	label_profession->setPosition(Vec2(m_profession->getContentSize().width+104,innerHeight-50));
	binding_label->setPosition(Vec2(104,m_profession->getPosition().y - 18));

	auto power_labelBmDes = Label::createWithBMFont("res_ui/font/ziti_3.fnt",StringDataManager::getString("equipment_info_powerCount"));
	power_labelBmDes->setAnchorPoint(Vec2(0,0));
	power_labelBmDes->setPosition(Vec2(30.5f,binding_label->getPosition().y - 33));
	panelbackGround->addChild(power_labelBmDes);

	int fightCapacity = goodsInfo->equipmentdetail().fightcapacity();
	char fightStr[50];
	sprintf(fightStr,"%d",fightCapacity);

	auto power_labelBmNum = Label::createWithBMFont("res_ui/font/ziti_1.fnt", fightStr);
	power_labelBmNum->setAnchorPoint(Vec2(0,0));
	power_labelBmNum->setPosition(Vec2(power_labelBmDes->getPosition().x+power_labelBmDes->getContentSize().width,
										power_labelBmDes->getPosition().y-7));
	power_labelBmNum->setScale(1.3f);
	panelbackGround->addChild(power_labelBmNum);

	/*
	int cur_starLevel = goodsInfo->equipmentdetail().starlevel();
	int m_star_all= cur_starLevel/2;
	int m_star_hafl =cur_starLevel%2;

	for (int i=1;i<=m_star_all;i++)
	{
		auto starlevel_int = ImageView::create();
		starlevel_int->setAnchorPoint(Vec2(1,0));
		starlevel_int->setPosition(Vec2(10+25*i,binding_label->getPosition().y -25));
		starlevel_int->loadTexture("res_ui/star_on.png");
		panelbackGround->addChild(starlevel_int);
	}

	if (m_star_hafl > 0)
	{
		auto starlevel_half = ImageView::create();
		starlevel_half->setAnchorPoint(Vec2(1,0));
		starlevel_half->setPosition(Vec2(10+25*(m_star_all+1),binding_label->getPosition().y -25));
		starlevel_half->loadTexture("res_ui/star_half.png");
		panelbackGround->addChild(starlevel_half);
	}

	for (int m= m_star_all +m_star_hafl;m<10;m++)
	{
		auto starlevel_All = ImageView::create();
		starlevel_All->setAnchorPoint(Vec2(1,0));
		starlevel_All->setPosition(Vec2(35+25*m,binding_label->getPosition().y - 25));
		starlevel_All->loadTexture("res_ui/star_off.png");
		panelbackGround->addChild(starlevel_All);
	}
	*/
	int starPosition_ = binding_label->getPosition().y - 25;
	boundary_Zero->setPosition(Vec2(panelbackGround->getContentSize().width/2,starPosition_ - 10));
	///////////////
	const char *str_uselevel = StringDataManager::getString("equipment_info_useLevel");
	auto m_uselevel=Label::createWithTTF(str_uselevel, APP_FONT_NAME, 16);
	m_uselevel->setAnchorPoint(Vec2(0,0));
	m_uselevel->setColor(Color3B(101,255,221));
	m_uselevel->setPosition(Vec2(w_space,boundary_Zero->getPosition().y - 28));//16
	panelbackGround->addChild(m_uselevel);

	int useLevel_ = goodsInfo->equipmentdetail().usergrade();
	int selfLevel =  GameView::getInstance()->myplayer->getActiveRole()->level();
	char uselevelStr[10];
	sprintf(uselevelStr,"%d",useLevel_);
	auto label_uselevel=Label::createWithTTF(uselevelStr, APP_FONT_NAME, 16);
	label_uselevel->setAnchorPoint(Vec2(0,0));
	label_uselevel->setColor(Color3B(101,255,221));
	label_uselevel->setName("toucheEquipLevel");
	label_uselevel->setPosition(Vec2(m_uselevel->getContentSize().width+m_uselevel->getPosition().x+2,boundary_Zero->getPosition().y - 28));
	panelbackGround->addChild(label_uselevel);

	if (index == 0)
	{
		this->setEquipProfessionAndLevelColor(generalProfession,generalLevel);
	}
	
	////////////
	const char *str_durle = StringDataManager::getString("equipment_info_durle");
	auto equipDurleLabel =Label::createWithTTF(str_durle, APP_FONT_NAME, 16);
	equipDurleLabel->setAnchorPoint(Vec2(0,0));
	equipDurleLabel->setColor(Color3B(101,255,221));
	equipDurleLabel->setPosition(Vec2(w_space,m_uselevel->getPosition().y -20));
	panelbackGround->addChild(equipDurleLabel);

	int m_durleVale = goodsInfo->equipmentdetail().durable();
	int m_maxDurleVale = goodsInfo->equipmentdetail().maxdurable();
	char durleValueStr_[5];
	sprintf(durleValueStr_,"%d",m_durleVale);
	char maxdurleValueStr_[5];
	sprintf(maxdurleValueStr_,"%d",m_maxDurleVale);

	std::string durableStr_ =durleValueStr_;
	durableStr_.append("/");
	durableStr_.append(maxdurleValueStr_);
	auto labelDurleVale =Label::createWithTTF(durableStr_.c_str(), APP_FONT_NAME, 16);
	labelDurleVale->setAnchorPoint(Vec2(0,0));
	labelDurleVale->setColor(Color3B(101,255,221));
	labelDurleVale->setPosition(Vec2(equipDurleLabel->getContentSize().width+equipDurleLabel->getPosition().x+2,equipDurleLabel->getPosition().y));
	panelbackGround->addChild(labelDurleVale);
	if (m_durleVale <= 10 )
	{
		equipDurleLabel->setColor(Color3B(255,0,0));
		labelDurleVale->setColor(Color3B(255,0,0));

		std::string repairLabelStr = "(";
		const char *str_durle = StringDataManager::getString("equipment_info_repair");
		repairLabelStr.append(str_durle);
		repairLabelStr.append(")");

		auto repairLabel =Label::createWithTTF(repairLabelStr.c_str(), APP_FONT_NAME, 16);
		repairLabel->setAnchorPoint(Vec2(0,0));
		repairLabel->setColor(Color3B(255,0,0));
		repairLabel->setPosition(Vec2(labelDurleVale->getContentSize().width+labelDurleVale->getPosition().x,labelDurleVale->getPosition().y));
		panelbackGround->addChild(repairLabel);
	}

	const char *str_equipPrice_ = StringDataManager::getString("equipment_info_price");
	auto equipPriceLabel =Label::createWithTTF(str_equipPrice_, APP_FONT_NAME, 16);
	equipPriceLabel->setAnchorPoint(Vec2(0,0));
	equipPriceLabel->setColor(Color3B(101,255,221));
	equipPriceLabel->setPosition(Vec2(w_space,equipDurleLabel->getPosition().y -20));
	panelbackGround->addChild(equipPriceLabel);

	int m_priceValue =goodsInfo->sellprice();
	char priceValueStr_[5];
	sprintf(priceValueStr_,"%d",m_priceValue);
	auto labelPriceValue =Label::createWithTTF(priceValueStr_, APP_FONT_NAME, 16);
	labelPriceValue->setAnchorPoint(Vec2(0,0));
	labelPriceValue->setColor(Color3B(101,255,221));
	labelPriceValue->setPosition(Vec2(equipPriceLabel->getContentSize().width+equipPriceLabel->getPosition().x+2,equipPriceLabel->getPosition().y));
	panelbackGround->addChild(labelPriceValue);

	auto image_SellPriceType = ImageView::create();
	image_SellPriceType->setAnchorPoint(Vec2(0,0));
	image_SellPriceType->loadTexture("res_ui/coins.png");
	image_SellPriceType->setPosition(Vec2(labelPriceValue->getPosition().x + labelPriceValue->getContentSize().width + 5,labelPriceValue->getPosition().y));
	panelbackGround->addChild(image_SellPriceType);
	////////////////
	/*
	const char *str_powerCount = StringDataManager::getString("equipment_info_powerCount");
	Label * m_powerCount=Label::create();
	m_powerCount->setAnchorPoint(Vec2(0,0));
	m_powerCount->setText(str_powerCount);
	m_powerCount->setFontSize(16);
	m_powerCount->setColor(Color3B(101,255,221));
	m_powerCount->setPosition(Vec2(16,equipPriceLabel->getPosition().y - 20));
	panelbackGround->addChild(m_powerCount);
	int fightCapacity_ =goodsInfo->equipmentdetail().fightcapacity();
	char figStr[10];
	sprintf(figStr,"%d",fightCapacity_);
	Label * label_powercount=Label::create();
	label_powercount->setAnchorPoint(Vec2(0,0));
	label_powercount->setText(figStr);
	label_powercount->setFontSize(16);
	label_powercount->setColor(Color3B(101,255,221));
	label_powercount->setPosition(Vec2(m_powerCount->getContentSize().width+20,equipPriceLabel->getPosition().y - 20));
	panelbackGround->addChild(label_powercount);
	*/
	///////////////////////////////////base property list
	auto boundary_touch_first= ImageView::create();
	boundary_touch_first->setAnchorPoint(Vec2(0.5f,0.5f));
	boundary_touch_first->loadTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
	boundary_touch_first->setPosition(Vec2(panelbackGround->getContentSize().width/2,image_SellPriceType->getPosition().y - 10));
	panelbackGround->addChild(boundary_touch_first);

	const char *str_equipPropertyBase = StringDataManager::getString("equipment_info_property_base");
	auto label_equipProperty_base=Label::createWithTTF(str_equipPropertyBase, APP_FONT_NAME, 16);
	label_equipProperty_base->setAnchorPoint(Vec2(0,0));
	label_equipProperty_base->setColor(Color3B(228,173,4));
	label_equipProperty_base->setPosition(Vec2(w_space,boundary_touch_first->getPosition().y - 28));
	panelbackGround->addChild(label_equipProperty_base);

	//show activeValue
	boundary_Active= ImageView::create();
	boundary_Active->setAnchorPoint(Vec2(0.5f,0.5f));
	boundary_Active->loadTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
	panelbackGround->addChild(boundary_Active);

	const char *str_equipPropertyactivate = StringDataManager::getString("equipment_info_property_activate");
	label_equipProperty_active=Label::createWithTTF(str_equipPropertyactivate, APP_FONT_NAME, 16);
	label_equipProperty_active->setAnchorPoint(Vec2(0,0));
	label_equipProperty_active->setColor(Color3B(228,173,4));
	panelbackGround->addChild(label_equipProperty_active);
	
	for (int i=0;i<5;i++)
	{
		const char *str_label = StringDataManager::getString("equip_showActive_label");

 		const char *str_first_value = StringDataManager::getString("equip_showActive_labelValue");

		std::string strings_ = "";
		if (i==0)
		{
			strings_.append(str_first_value);
		}else
		{
			int num = i*5;
			char numStr[50];
			sprintf(numStr,str_label,num);
			strings_.append(numStr);
		}
		
		activePropertyDes=Label::createWithTTF(strings_.c_str(), APP_FONT_NAME, 16);
		activePropertyDes->setAnchorPoint(Vec2(0,0));
		activePropertyDes->setColor(Color3B(255,255,172));
		activePropertyDes->setTag(EQUIPMENTACTIVETAG+100*index+i);
		panelbackGround->addChild(activePropertyDes);
	}
	//show gemValue
	boundary_Gem= ImageView::create();
	boundary_Gem->setAnchorPoint(Vec2(0.5f,0.5f));
	boundary_Gem->loadTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
	panelbackGround->addChild(boundary_Gem);

	const char *str_equipPropertygem = StringDataManager::getString("equipment_info_property_gem");
	label_equipProperty_gem=Label::createWithTTF(str_equipPropertygem, APP_FONT_NAME, 16);
	label_equipProperty_gem->setAnchorPoint(Vec2(0,0));
	label_equipProperty_gem->setColor(Color3B(228,173,4));
	panelbackGround->addChild(label_equipProperty_gem);
	const char *str_ = StringDataManager::getString("equip_showgem_labelhole");
	for (int i=0;i<3;i++)
	{
		gemPropertyDes=Label::createWithTTF(str_, APP_FONT_NAME, 16);
		gemPropertyDes->setAnchorPoint(Vec2(0,0));
		gemPropertyDes->setTag(EQUIPMENTGEMTAG+100*index+i);
		panelbackGround->addChild(gemPropertyDes);
	}

	///refresh value
	for (int i=0;i<goodsInfo->equipmentdetail().properties_size();i++)
	{
		switch (goodsInfo->equipmentdetail().properties(i).type())
		{
		case 1:
			{
				//base property list
				std::string baseName = goodsInfo->equipmentdetail().properties(i).name();
				baseName.append(":");

				basePropertyDes=Label::createWithTTF(baseName.c_str(), APP_FONT_NAME, 16);
				basePropertyDes->setAnchorPoint(Vec2(0,0));
				basePropertyDes->setColor(Color3B(255,255,172));
				basePropertyDes->setPosition(Vec2(w_space,label_equipProperty_base->getPosition().y - 20*(baseValueNum+1)));//50
				panelbackGround->addChild(basePropertyDes);

				std::string basePropertyValueStr="";
				for (int propindex=0;propindex<goodsInfo->equipmentdetail().properties(i).value_size();propindex++)
				{
					char basePropertyStr[10];
					sprintf(basePropertyStr,"%d",goodsInfo->equipmentdetail().properties(i).value(propindex));
					if (propindex==0)
					{
						basePropertyValueStr.append(basePropertyStr);
					}else
					{
						basePropertyValueStr.append("(+");
						basePropertyValueStr.append(basePropertyStr);
						basePropertyValueStr.append(")");
					}
				}
				basePropertyValue=Label::createWithTTF(basePropertyValueStr.c_str(), APP_FONT_NAME, 16);
				basePropertyValue->setAnchorPoint(Vec2(0,0));
				basePropertyValue->setColor(Color3B(255,255,172));
				basePropertyValue->setPosition(Vec2(basePropertyDes->getPosition().x+basePropertyDes->getContentSize().width+4,basePropertyDes->getPosition().y));
				panelbackGround->addChild(basePropertyValue);
				baseValueNum++;
			}break;
		case 2:
			{
				////star property list
				if (starValueNum==0)
				{
					boundary_Star= ImageView::create();
					boundary_Star->setAnchorPoint(Vec2(0.5f,0.5f));
					boundary_Star->loadTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
					boundary_Star->setPosition(Vec2(panelbackGround->getContentSize().width/2,basePropertyDes->getPosition().y - 10));
					panelbackGround->addChild(boundary_Star);

					const char *str_equipPropertyStar = StringDataManager::getString("equipment_info_property_star");
					label_equipProperty_star=Label::createWithTTF(str_equipPropertyStar, APP_FONT_NAME, 16);
					label_equipProperty_star->setAnchorPoint(Vec2(0,0));
					label_equipProperty_star->setColor(Color3B(228,173,4));
					label_equipProperty_star->setPosition(Vec2(w_space,boundary_Star->getPosition().y - 28));
					panelbackGround->addChild(label_equipProperty_star);
				}
			
				std::string starDes_begine = goodsInfo->equipmentdetail().properties(i).name().substr(0,3);
				starPropertyDes=Label::createWithTTF(starDes_begine.c_str(), APP_FONT_NAME, 16);
				starPropertyDes->setAnchorPoint(Vec2(0,0));
				starPropertyDes->setColor(Color3B(255,255,172));
				starPropertyDes->setPosition(Vec2(w_space,label_equipProperty_star->getPosition().y -20*(starValueNum+1)));//50
				panelbackGround->addChild(starPropertyDes);

				std::string starDes_end = goodsInfo->equipmentdetail().properties(i).name().substr(3,6);
				starDes_end.append(":");
				auto starPropertyDes_end =Label::createWithTTF(starDes_end.c_str(), APP_FONT_NAME, 16);
				starPropertyDes_end->setAnchorPoint(Vec2(1,0));
				starPropertyDes_end->setColor(Color3B(255,255,172));
				starPropertyDes_end->setPosition(Vec2(w_space+68,starPropertyDes->getPosition().y));//50
				panelbackGround->addChild(starPropertyDes_end);

				std::string starPropertyValueStr="";
				int starPropertySize = goodsInfo->equipmentdetail().properties(i).value_size();
				for (int valueIndex=0;valueIndex<starPropertySize;valueIndex++)
				{
					char basePropertyStr[10];
					sprintf(basePropertyStr,"%d",goodsInfo->equipmentdetail().properties(i).value(valueIndex));
				
					if (valueIndex==0)
					{
						starPropertyValueStr.append(basePropertyStr);
					}else
					{
						starPropertyValueStr.append("(+");
						starPropertyValueStr.append(basePropertyStr);
						starPropertyValueStr.append(")");
					}
				}

				starPropertyValue=Label::createWithTTF(starPropertyValueStr.c_str(), APP_FONT_NAME, 16);
				starPropertyValue->setAnchorPoint(Vec2(0,0));
				starPropertyValue->setColor(Color3B(255,255,172));
				starPropertyValue->setPosition(Vec2(starPropertyDes_end->getPosition().x+4,starPropertyDes->getPosition().y));
				panelbackGround->addChild(starPropertyValue);
				starValueNum++;
			}break;
		case 3:
			{
				std::string activeDes_byte = goodsInfo->equipmentdetail().properties(i).name();
				activeDes_byte.append(":");
				auto label_activeDes =(Label *)panelbackGround->getChildByTag(EQUIPMENTACTIVETAG+100*index+activeValueNum);
				label_activeDes->setString(activeDes_byte.c_str());

				if (goodsInfo->equipmentdetail().properties(i).name().size() <= 6)//string size is > 6 ,string byte =  4 
				{
					std::string activeDes_begin = goodsInfo->equipmentdetail().properties(i).name().substr(0,3);
					label_activeDes->setString(activeDes_begin.c_str());

					std::string activeDes_end = goodsInfo->equipmentdetail().properties(i).name().substr(3,6);
					activeDes_end.append(":");
					auto label_activeDes_End =Label::createWithTTF(activeDes_end.c_str(), APP_FONT_NAME, 16);
					label_activeDes_End->setAnchorPoint(Vec2(1,0));
					label_activeDes_End->setColor(Color3B(255,255,172));
					label_activeDes_End->setTag(EQUIPMENTACTIVE_END_TAG+100*index+activeValueNum); 
					label_activeDes_End->setPosition(Vec2(w_space+68,starPropertyDes->getPosition().y));//50
					panelbackGround->addChild(label_activeDes_End);
				}

				int activeProperty =goodsInfo->equipmentdetail().properties(i).value(0);
				char activePropertyStr[10];
				sprintf(activePropertyStr,"%d",activeProperty);

				activePropertyValue=Label::createWithTTF(activePropertyStr, APP_FONT_NAME, 16);
				activePropertyValue->setAnchorPoint(Vec2(0,0));
				activePropertyValue->setColor(Color3B(255,255,172));
				activePropertyValue->setTag(EQUIPMENTACTIVEVALUETAG+100*index+activeValueNum);
				panelbackGround->addChild(activePropertyValue);
				activeValueNum++;
			}break;
		case 4:
			{
				auto label_gemDes =(Label *)panelbackGround->getChildByTag(EQUIPMENTGEMTAG+100*index+gemValueNum);
				int gemProperty =goodsInfo->equipmentdetail().properties(i).value(0);
				if (gemProperty > 0)
				{
					std::string gemIconStr_ = goodsInfo->equipmentdetail().properties(i).description();
					std::string gemIcon ="res_ui/props_icon/";
					gemIcon.append(gemIconStr_);
					gemIcon.append(".png");

					auto imageStarIcon = ImageView::create();
					imageStarIcon->loadTexture(gemIcon.c_str());
					imageStarIcon->setAnchorPoint(Vec2(0,0));
					imageStarIcon->setPosition(Vec2(0,0));
					imageStarIcon->setTag(EQUIPMENTGEMICONTAG+ 100*index + gemValueNum);
					imageStarIcon->setScale(0.4f);
					panelbackGround->addChild(imageStarIcon);

					std::string gemDes = goodsInfo->equipmentdetail().properties(i).name();
					label_gemDes->setColor(Color3B(113,227,255));
					label_gemDes->setString(gemDes.c_str());

					char gemPropertyStr[10];
					sprintf(gemPropertyStr,"%d",gemProperty);
					std::string gemPropertyValueStr_ ="";
					gemPropertyValueStr_.append("+");
					gemPropertyValueStr_.append(gemPropertyStr);

					auto gemPropertyValue=Label::createWithTTF(gemPropertyValueStr_.c_str(), APP_FONT_NAME, 16);
					gemPropertyValue->setAnchorPoint(Vec2(0,0));
					gemPropertyValue->setColor(Color3B(113,227,255));
					gemPropertyValue->setTag(EQUIPMENTGEMVALUETAG+100*index+gemValueNum);
					panelbackGround->addChild(gemPropertyValue);
				}else
				{
					const char *strings_ = StringDataManager::getString("equip_showgem_labelKaiKong");
					label_gemDes->setString(strings_);
				}
				gemValueNum++;
			}break;
		case 5:
		case 6:
			{
				/////////////////////suit
				if (suitValueNum==0)
				{
					boundary_Suit= ImageView::create();
					boundary_Suit->setAnchorPoint(Vec2(0.5f,0.5f));
					boundary_Suit->loadTexture("res_ui/wupinzhuangbeixiangqing/gaoguang.png");
					panelbackGround->addChild(boundary_Suit);

					const char *str_equipSuit = StringDataManager::getString("equipment_info_property_suit");
					label_equipProperty_suit=Label::createWithTTF(str_equipSuit, APP_FONT_NAME, 16);
					label_equipProperty_suit->setAnchorPoint(Vec2(0,0));
					label_equipProperty_suit->setColor(Color3B(228,173,4));
					panelbackGround->addChild(label_equipProperty_suit);
				}
				//
				auto suitAmountLabel=Label::createWithTTF(" ", APP_FONT_NAME, 16);
				suitAmountLabel->setAnchorPoint(Vec2(0,0));
				suitAmountLabel->setColor(Color3B(199,199,199));
				//suitAmountLabel->setText(suitDes.c_str());
				suitAmountLabel->setTag(EQUIPMENTSUITNAME+100*index+suitValueNum);
				panelbackGround->addChild(suitAmountLabel);

				std::string suitDes = goodsInfo->equipmentdetail().properties(i).name();
				auto suitPropertyDes=Label::createWithTTF(suitDes.c_str(), APP_FONT_NAME, 16);
				suitPropertyDes->setAnchorPoint(Vec2(0,0));
				suitPropertyDes->setColor(Color3B(199,199,199));
				suitPropertyDes->setTag(EQUIPMENTSUITTAG+100*index+suitValueNum);
				panelbackGround->addChild(suitPropertyDes);

				int suitProperty =goodsInfo->equipmentdetail().properties(i).value(0);
				char suitPropertyStr[10];
				sprintf(suitPropertyStr,"%d",suitProperty);
				
				auto suitPropertyValue=Label::createWithTTF(suitPropertyStr, APP_FONT_NAME, 16);
				suitPropertyValue->setAnchorPoint(Vec2(0,0));
				suitPropertyValue->setTag(EQUIPMENTSUITVALUETAG+100*index+suitValueNum);
				panelbackGround->addChild(suitPropertyValue);
				
				suitValueNum++;
			}break;
		}  
	}

	boundary_Active->setPosition(Vec2(panelbackGround->getContentSize().width/2,starPropertyValue->getPosition().y - 10));
	label_equipProperty_active->setPosition(Vec2(w_space,boundary_Active->getPosition().y - 28));
	for (int i=0;i<activeValueNum;i++)
	{
		auto label_activeDes =(Label *)panelbackGround->getChildByTag(EQUIPMENTACTIVETAG+100*index+i);
		label_activeDes->setPosition(Vec2(w_space,label_equipProperty_active->getPosition().y -20*(i+1)));

		int x_ = label_activeDes->getPosition().x+ label_activeDes->getContentSize().width;

		if (panelbackGround->getChildByTag(EQUIPMENTACTIVE_END_TAG+100*index+i))
		{
			auto label_activeDes_end =(Label *)panelbackGround->getChildByTag(EQUIPMENTACTIVE_END_TAG+100*index+i);
			label_activeDes_end->setPosition(Vec2(w_space+68,label_activeDes->getPosition().y));

			x_ = w_space + 68;
		}
		
		auto label_activeValue =(Label*)panelbackGround->getChildByTag(EQUIPMENTACTIVEVALUETAG+100*index+i);
		label_activeValue->setPosition(Vec2(x_+4,label_activeDes->getPosition().y));
	}

	for(int i=0;i<5-activeValueNum;i++)
	{
		auto label_activeDes =(Label *)panelbackGround->getChildByTag(EQUIPMENTACTIVETAG+100*index+activeValueNum+i);
		label_activeDes->setPosition(Vec2(w_space,label_equipProperty_active->getPosition().y -20*(activeValueNum+i+1)));
	}

	//boundary_Gem  label_equipProperty_gem
	boundary_Gem->setPosition(Vec2(panelbackGround->getContentSize().width/2,activePropertyDes->getPosition().y -10));
	label_equipProperty_gem->setPosition(Vec2(w_space,boundary_Gem->getPosition().y -28));
	for (int i=0;i<gemValueNum;i++)
	{
		auto imageGemIcon = (ImageView *)panelbackGround->getChildByTag(EQUIPMENTGEMICONTAG+100*index+i);
		auto label_gemDes =(Label *)panelbackGround->getChildByTag(EQUIPMENTGEMTAG+100*index+i);
		if (imageGemIcon != NULL)
		{
			imageGemIcon->setPosition(Vec2(w_space,label_equipProperty_gem->getPosition().y - 20* (i+1)));
			label_gemDes->setPosition(Vec2(imageGemIcon->getPosition().x+imageGemIcon->getContentSize().width *0.4,label_equipProperty_gem->getPosition().y - 20* (i+1)));
		}else
		{
			label_gemDes->setPosition(Vec2(w_space,label_equipProperty_gem->getPosition().y - 20* (i+1)));
		}
		auto label_gemValue =(Label *)panelbackGround->getChildByTag(EQUIPMENTGEMVALUETAG+100*index+i);
		if (label_gemValue!= NULL)
		{
			label_gemValue->setPosition(Vec2(label_gemDes->getPosition().x +label_gemDes->getContentSize().width,label_gemDes->getPosition().y));
		}
	}
	for (int i=0;i<3 - gemValueNum;i++)
	{
		auto label_gemDes =(Label *)panelbackGround->getChildByTag(EQUIPMENTGEMTAG+100*index+gemValueNum+i);
		label_gemDes->setPosition(Vec2(w_space,label_equipProperty_gem->getPosition().y - 20* (gemValueNum + i+1)));
	}

	///suit
	if (isHavesuibt ==true)
	{
		boundary_Suit->setPosition(Vec2(panelbackGround->getContentSize().width/2,gemPropertyDes->getPosition().y - 10));
		label_equipProperty_suit->setPosition(Vec2(w_space,boundary_Suit->getPosition().y - 28));
		int selfEquipSuitHighLight = 0;
		//set suit isHighLight
		if (index == 0)
		{
			auto quiryui = (QuiryUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagQuiryUI);
			if (quiryui != NULL)
			{
				selfEquipSuitHighLight = quiryui->getRoleEquipAmount()-1;
			}else
			{
				if (isSetSuitColor == true)
				{
					selfEquipSuitHighLight = selfEquipSuit-1;
				}
			}
		}

		if (index == 1)
		{
			selfEquipSuit = 0;
			if (isSetSuitColor == true)
			{
				for (int suitIndex=0;suitIndex<selfEquipVector.size();suitIndex++)
				{
					if (goodsInfo->equipmentdetail().suitid() == selfEquipVector.at(suitIndex)->goods().equipmentdetail().suitid())
					{
						selfEquipSuit++;
					}
				}
				selfEquipSuitHighLight = selfEquipSuit-1;
			}
		}
		int suitAmout_ = selfEquipSuitHighLight+1;
		for (int i=0;i<suitValueNum;i++)
		{
			const char *str_equipSuit = StringDataManager::getString("equipment_info_property_suitDescription");
			char suitAmount[5];
			if (suitAmout_ > i+2 )
			{
				sprintf(suitAmount,"%d",i+2);
			}else
			{
				sprintf(suitAmount,"%d",suitAmout_);
			}
			char suitIndex[5];
			sprintf(suitIndex,"%d",i+2);
			
			std::string suitDes = goodsInfo->name().substr(0,6);
			suitDes.append(str_equipSuit);
			suitDes.append("+(");
			suitDes.append(suitAmount);
			suitDes.append("/");
			suitDes.append(suitIndex);
			suitDes.append("):");

			auto label_suitName =(Label *)panelbackGround->getChildByTag(EQUIPMENTSUITNAME+100*index+i);
			label_suitName->setString(suitDes.c_str());
			label_suitName->setPosition(Vec2(w_space,label_equipProperty_suit->getPosition().y -20*(i+1)));

			auto label_suitDes =(Label *)panelbackGround->getChildByTag(EQUIPMENTSUITTAG+100*index+i);
			label_suitDes->setPosition(Vec2(label_suitName->getPosition().x+label_suitName->getContentSize().width+4,label_suitName->getPosition().y));
			auto label_suitValue =(Label *)panelbackGround->getChildByTag(EQUIPMENTSUITVALUETAG+100*index+i);
			label_suitValue->setPosition(Vec2(label_suitDes->getPosition().x+label_suitDes->getContentSize().width,label_suitDes->getPosition().y ));
			
			if (selfEquipSuitHighLight >0)
			{
				label_suitName->setColor(Color3B(211,5,255));
				label_suitDes->setColor(Color3B(211,5,255));
				label_suitValue->setColor(Color3B(211,5,255));
				selfEquipSuitHighLight--;
			}
		}
	}
}

void GoodsItemInfoBase::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GoodsItemInfoBase::onExit()
{
	UIScene::onExit();
}

bool GoodsItemInfoBase::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return resignFirstResponder(pTouch,this,false);
}

void GoodsItemInfoBase::showGeneralEquipMentInfo(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto rolebutton_ = (Button *)m_pLayer->getChildByTag(10);
		auto roleSelectImage = (ImageView *)rolebutton_->getChildByTag(1101);
		if (roleSelectImage != NULL)
		{
			roleSelectImage->removeFromParentAndCleanup(true);
		}

		int generalNum = GameView::getInstance()->generalsInLineList.size();
		for (int btnIndex = 0; btnIndex< generalNum; btnIndex++)
		{
			auto button_ = (Button*)m_pLayer->getChildByTag(btnIndex);
			auto roleSelectImage = (ImageView *)button_->getChildByTag(1101);
			if (roleSelectImage != NULL)
			{
				roleSelectImage->removeFromParentAndCleanup(true);
			}
		}
		auto btn = (Button *)pSender;
		// add the selected flag
		auto roleSelectButton_ = ImageView::create();
		roleSelectButton_->setScale9Enabled(true);
		roleSelectButton_->setContentSize(Size(142, 62));
		roleSelectButton_->setCapInsets(Rect(15, 15, 1, 1));
		roleSelectButton_->loadTexture("res_ui/highlight.png");
		roleSelectButton_->setAnchorPoint(Vec2(0.5f, 0.5f));
		roleSelectButton_->setPosition(Vec2(0, 0));
		roleSelectButton_->setTag(1101);
		btn->addChild(roleSelectButton_);

		//if package
		auto packageui = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
		int index_ = btn->getTag();
		bodyPanelInfo->removeAllChildren();

		std::string selectActorPressionStr_ = "";
		int m_level = 0;
		if (index_ == 10)
		{
			selectActorPressionStr_ = GameView::getInstance()->myplayer->getActiveRole()->profession();
			m_level = GameView::getInstance()->myplayer->getActiveRole()->level();
			bool isExitequip = false;
			for (int i = 0; i<GameView::getInstance()->EquipListItem.size(); i++)
			{
				if (goodsInfo_General->equipmentclazz() == GameView::getInstance()->EquipListItem.at(i)->goods().equipmentclazz())
				{
					auto playerEquip = new GoodsInfo();
					playerEquip->CopyFrom(GameView::getInstance()->EquipListItem.at(i)->goods());
					this->showTouchEquipMentInfo(playerEquip, 1);// show player equipment goodsinfo
					int width = touchPanelInfo->getContentSize().width + bodyPanelInfo->getContentSize().width;
					if (touchPanelInfo->getContentSize().height > bodyPanelInfo->getContentSize().height)
					{
						touchPanelInfo->setPosition(Vec2(0, touchPanelInfo->getContentSize().height));
						bodyPanelInfo->setPosition(Vec2(touchPanelInfo->getContentSize().width, touchPanelInfo->getContentSize().height));
						scrollView_info->setInnerContainerSize(Size(width, touchPanelInfo->getContentSize().height));
					}
					else
					{
						touchPanelInfo->setPosition(Vec2(0, bodyPanelInfo->getContentSize().height));
						bodyPanelInfo->setPosition(Vec2(touchPanelInfo->getContentSize().width, bodyPanelInfo->getContentSize().height));
						scrollView_info->setInnerContainerSize(Size(width, bodyPanelInfo->getContentSize().height));
					}
					delete playerEquip;
					isExitequip = true;
				}
			}
			if (isExitequip == true)
			{
				notEquipment->setVisible(false);
				flag_equip->setVisible(true);
			}
			else
			{
				notEquipment->setVisible(true);
				flag_equip->setVisible(false);
			}

			if (packageui != NULL)
			{
				packageui->generalList_tableView->cellAtIndex(0);
			}
		}
		else
		{
			long long generalIndex_ = GameView::getInstance()->generalsInLineList.at(index_)->id();

			bool isEquipment = false;
			for (int i = 0; i<GameView::getInstance()->generalsInLineDetailList.size(); i++)
			{
				long long generalId = GameView::getInstance()->generalsInLineDetailList.at(i)->generalid();
				if (generalIndex_ == generalId)
				{
					selectActorPressionStr_ = GameView::getInstance()->generalsInLineDetailList.at(i)->profession();
					m_level = GameView::getInstance()->generalsInLineDetailList.at(i)->activerole().level();
					for (int j = 0; j<GameView::getInstance()->generalsInLineDetailList.at(i)->equipments_size(); j++)
					{
						if (goodsInfo_General->equipmentclazz() == GameView::getInstance()->generalsInLineDetailList.at(i)->equipments(j).goods().equipmentclazz())
						{
							auto playerEquip = new GoodsInfo();
							playerEquip->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->equipments(j).goods());
							this->showTouchEquipMentInfo(playerEquip, 1);// show player equipment goodsinfo

							int width = touchPanelInfo->getContentSize().width + bodyPanelInfo->getContentSize().width;
							if (touchPanelInfo->getContentSize().height > bodyPanelInfo->getContentSize().height)
							{
								touchPanelInfo->setPosition(Vec2(0, touchPanelInfo->getContentSize().height));
								bodyPanelInfo->setPosition(Vec2(touchPanelInfo->getContentSize().width, touchPanelInfo->getContentSize().height));
								scrollView_info->setInnerContainerSize(Size(width, touchPanelInfo->getContentSize().height));
							}
							else
							{
								touchPanelInfo->setPosition(Vec2(0, bodyPanelInfo->getContentSize().height));
								bodyPanelInfo->setPosition(Vec2(touchPanelInfo->getContentSize().width, bodyPanelInfo->getContentSize().height));
								scrollView_info->setInnerContainerSize(Size(width, bodyPanelInfo->getContentSize().height));
							}
							delete playerEquip;

							isEquipment = true;
							break;
						}
					}
				}
			}
			if (isEquipment == false)
			{
				notEquipment->setVisible(true);
				flag_equip->setVisible(false);
			}
			else
			{
				notEquipment->setVisible(false);
				flag_equip->setVisible(true);
			}

			if (packageui != NULL)
			{
				packageui->generalList_tableView->cellAtIndex(index_ + 1);
			}
		}
		int pressionIndex_ = BasePlayer::getProfessionIdxByName(selectActorPressionStr_.c_str());
		this->setEquipProfessionAndLevelColor(pressionIndex_, m_level);

		this->isTouchSameEquipment();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

std::string GoodsItemInfoBase::getEquipmentQualityByIndex( int quality )
{
	std::string frameColorPath = "res_ui/";
	switch(quality)
	{
	case 1:
		{
			frameColorPath.append("smdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("smdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("smdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("smdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("smdi_orange");
		}break;
	default:
		frameColorPath.append("smdi_white");
	}
	frameColorPath.append(".png");
	return frameColorPath;
}

cocos2d::Color3B GoodsItemInfoBase::getEquipmentColorByQuality( int quality )
{
	Color3B color_;
	switch(quality)
	{
	case 1:
		{
			color_ =Color3B(255,255,255);
		}break;
	case 2:
		{
			color_=Color3B(136,234,31);
		}break;
	case 3:
		{
			color_=Color3B(15,202,250);
		}break;
	case 4:
		{
			color_=Color3B(255,62,253);
		}break;
	case 5:
		{
			color_=Color3B(250,155,15);
		}break;
	default:
		color_ =Color3B(255,255,255);
	}

	return color_;
}

void GoodsItemInfoBase::getSelfEquipVector(GoodsInfo * goodsInfo,std::vector<CEquipment *> vector_)
{
	selfEquipSuit = 0;
	for (int i=0;i<vector_.size();i++)
	{
		auto selfEquip = new CEquipment();
		selfEquip->CopyFrom(*vector_.at(i));

		if (goodsInfo->instanceid()==vector_.at(i)->goods().instanceid())
		{
			isSetSuitColor = true;
		}
		
		if (goodsInfo->equipmentdetail().suitid() == vector_.at(i)->goods().equipmentdetail().suitid())
		{
			selfEquipSuit++;
		}
		
		selfEquipVector.push_back(selfEquip);
	}
	
	/*
	PackageScene * packscene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	if (packscene != NULL )
	{
		if (packscene->selectActorId  == 0)
		{
			this->getRoleEquipmentList();
		}else
		{
			this->getGeneralEquipmentList(packscene->selectActorId);
		}
	}else
	{
		ChatUI * chatui_ = (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
		if (chatui_ != NULL)
		{
			Layer * layer_ = (Layer *)chatui_->getChildByTag(CHATLayer3);
			BackPackage * backpack_ = (BackPackage *)layer_->getChildByTag(PACKAGEAGE);
			if (backpack_ != NULL)
			{
				if (backpack_->curRoleIndex == 0)
				{
					this->getRoleEquipmentList();
				}else
				{
					long long generalId_ = GameView::getInstance()->generalsInLineDetailList.at(backpack_->curRoleIndex - 1)->generalid();
					this->getGeneralEquipmentList(generalId_);
				}
			}else
			{
				this->getRoleEquipmentList();
			}
		}else
		{
			this->getRoleEquipmentList();
		}
	}
	//
	isRoleEquip_ = false;
	isSetSuitColor = false;
	//是否是套装 套装suitid suitamount
	for (int suitIndex=0;suitIndex<selfEquipVector.size();suitIndex++)
	{
		if (goodsInfo_General->equipmentdetail().suitid() == selfEquipVector.at(suitIndex)->goods().equipmentdetail().suitid())
		{
			selfEquipSuit++;
		}

		if (strcmp(goodsInfo_General->id().c_str(),selfEquipVector.at(suitIndex)->goods().id().c_str())==0)
		{
			isSetSuitColor = true;
		}

		if (goodsInfo_General->equipmentclazz()== selfEquipVector.at(suitIndex)->goods().equipmentclazz())
		{
			isRoleEquip_ =true;
		}
	}
	*/
}

void GoodsItemInfoBase::isEquipOfBody( GoodsInfo * goods )
{
	/*
	isRoleEquip_ = false;
	isSetSuitColor = false;
	for (int i=0;i<selfEquipVector.size();i++)
	{
		if (strcmp(goods->id().c_str(),selfEquipVector.at(i)->goods().id().c_str())==0)
		{
			isSetSuitColor = true;
		}

		if (goods->equipmentclazz()== selfEquipVector.at(i)->goods().equipmentclazz())
		{
			isRoleEquip_ =true;
		}
	}
	*/
}

void GoodsItemInfoBase::setEquipProfessionAndLevelColor( int pression_,int level_ )
{
	auto labelPre = (Label *) touchPanelInfo->getChildByName("toucheEquipPression");
	if (pression_ == goodsInfo_General->equipmentdetail().profession())
	{
		labelPre->setColor(Color3B(39,238,194));
	}else
	{
		labelPre->setColor(Color3B(255,0,0));
	}

	auto labelLevel_ = (Label *)touchPanelInfo->getChildByName("toucheEquipLevel");
	if (level_ >= goodsInfo_General->equipmentdetail().usergrade())
	{
		labelLevel_->setColor(Color3B(101,255,221));
	}else
	{
		labelLevel_->setColor(Color3B(255,0,0));
	}

	//set button is not touch
	auto packItem_ = (PackageItemInfo *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase);
	if (packItem_)
	{
		if (pression_ == goodsInfo_General->equipmentdetail().profession() && level_ >= goodsInfo_General->equipmentdetail().usergrade())
		{
			packItem_->setCurEquipIsUseAble(true);
		}else
		{
			packItem_->setCurEquipIsUseAble(false);
		}
	}
}

void GoodsItemInfoBase::isTouchSameEquipment()
{
	flag_toucheSelfEquip->setVisible(false);
	long long goodsINstanceId = goodsInfo_General->instanceid();
	for(int i=0;i<selfEquipVector.size();i++)
	{
		long long selfEquipInstanceId = selfEquipVector.at(i)->goods().instanceid();
		if (goodsINstanceId == selfEquipInstanceId)
		{
			flag_toucheSelfEquip->setVisible(true);
		}
	}
}

void GoodsItemInfoBase::addGeneralButton(long long generalId)
{
	auto roleFrameBtn_= Button::create();
	roleFrameBtn_->setTouchEnabled(true);
	roleFrameBtn_->setPressedActionEnabled(true);
	roleFrameBtn_->addTouchEventListener(CC_CALLBACK_2(GoodsItemInfoBase::showGeneralEquipMentInfo, this));
	roleFrameBtn_->setAnchorPoint(Vec2(0.5f,0.5f));
	roleFrameBtn_->setScale9Enabled(true);
	roleFrameBtn_->setContentSize(Size(140,60));
	roleFrameBtn_->setCapInsets(Rect(15,30,1,1));
	roleFrameBtn_->setPosition(Vec2(548+70,330));
	roleFrameBtn_->loadTextures("res_ui/kuang0_new.png","res_ui/kuang0_new.png","");
	roleFrameBtn_->setTag(10);
	m_pLayer->addChild(roleFrameBtn_);

	auto roleFrameBg_ =ImageView::create();
	roleFrameBg_->loadTexture("res_ui/round.png");
	roleFrameBg_->setAnchorPoint(Vec2(0.5f,0.5f));
	roleFrameBg_->setPosition(Vec2(-40,0));
	roleFrameBtn_->addChild(roleFrameBg_);

	std::string iconPath_ = BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str();
	auto roleIconImage_ =ImageView::create();
	roleIconImage_->loadTexture(iconPath_.c_str());
	roleIconImage_->setAnchorPoint(Vec2(0.5f,0.5f));
	roleIconImage_->setPosition(Vec2(0,0));
	roleIconImage_->setScale(0.8f);
	roleFrameBg_->addChild(roleIconImage_);

	auto roleLvbackGround = ImageView::create();
	roleLvbackGround->setAnchorPoint(Vec2(0,0));
	roleLvbackGround->loadTexture("res_ui/zhezhao80.png");
	roleLvbackGround->setScale9Enabled(true);
	roleLvbackGround->setPosition(Vec2(-25,-20));
	//roleLvbackGround->setCapInsets(Rect(5,10,1,4));
	roleLvbackGround->setContentSize(Size(32,14));
	roleFrameBtn_->addChild(roleLvbackGround);

	int roleLevelIndex_ = GameView::getInstance()->myplayer->getActiveRole()->level();
	char roleLevelStr[10];
	sprintf(roleLevelStr,"%d",roleLevelIndex_);
	std::string roleLevelString_ = "LV";
	roleLevelString_.append(roleLevelStr);
	auto roleLvLabel = Label::createWithTTF(roleLevelString_.c_str(), APP_FONT_NAME, 12);
	roleLvLabel->setAnchorPoint(Vec2(0,0));
	roleLvLabel->setPosition(Vec2(-25,-20));
	roleFrameBtn_->addChild(roleLvLabel);

	std::string roleNameStr_ = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
	auto roleNameLabel = Label::createWithTTF(roleNameStr_.c_str(), APP_FONT_NAME, 18);
	roleNameLabel->setAnchorPoint(Vec2(0.5f,0));
	roleNameLabel->setPosition(Vec2(-5 + roleFrameBg_->getContentSize().width/2,0));
	roleFrameBtn_->addChild(roleNameLabel);

	std::string pressionStr_ = GameView::getInstance()->myplayer->getActiveRole()->profession();
	auto rolePressionLabel = Label::createWithTTF(pressionStr_.c_str(), APP_FONT_NAME, 18);
	rolePressionLabel->setAnchorPoint(Vec2(0,0));
	rolePressionLabel->setPosition(Vec2(10,-20));
	roleFrameBtn_->addChild(rolePressionLabel);
	
	// the image for the selected flag
	auto roleSelectButton_ =ImageView::create();
	roleSelectButton_->setScale9Enabled(true);
	roleSelectButton_->setContentSize(Size(142,62));
	roleSelectButton_->setCapInsets(Rect(15,15,1,1));
	roleSelectButton_->loadTexture("res_ui/highlight.png");
	roleSelectButton_->setAnchorPoint(Vec2(0.5f,0.5f));
	roleSelectButton_->setTag(1101);
	roleSelectButton_->setPosition(Vec2(0,0));

	// check if MyPlayer has been selected
	/*
	PackageScene * packscene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	if (packscene != NULL )
	{
		if (packscene->selectActorId == 0)    // 0 means that MyPlayer is selected
		{
			roleFrameBtn_->addChild(roleSelectButton_);
		}
	}
	else
	{
		roleFrameBtn_->addChild(roleSelectButton_);
	}
	*/
	if (generalId ==0)// 0 means that MyPlayer is selected
	{
		roleFrameBtn_->addChild(roleSelectButton_);
		generalProfession = BasePlayer::getProfessionIdxByName(pressionStr_);
		generalLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
	}
	
	int generalNum = GameView::getInstance()->generalsInLineList.size();
	for (int i=0;i<generalNum;i++)
	{	
		auto generalBtn= Button::create();
		generalBtn->setTouchEnabled(true);
		generalBtn->setPressedActionEnabled(true);
		generalBtn->addTouchEventListener(CC_CALLBACK_2(GoodsItemInfoBase::showGeneralEquipMentInfo, this));
		generalBtn->setAnchorPoint(Vec2(0.5f,0.5f));
		generalBtn->setScale9Enabled(true);
		generalBtn->setContentSize(Size(140,60));
		generalBtn->setCapInsets(Rect(15,30,1,1));
		generalBtn->setPosition(Vec2(548+70,269 - 61*i));
		generalBtn->loadTextures("res_ui/kuang0_new.png","res_ui/kuang0_new.png","");
		generalBtn->setTag(i);
		m_pLayer->addChild(generalBtn);

		auto smaillFrame =ImageView::create();
		smaillFrame->loadTexture("res_ui/round.png");
		smaillFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		smaillFrame->setPosition(Vec2(-40,0));
		generalBtn->addChild(smaillFrame);

		auto generalLvbackGround = ImageView::create();
		generalLvbackGround->setAnchorPoint(Vec2(0,0));
		generalLvbackGround->loadTexture("res_ui/zhezhao80.png");
		generalLvbackGround->setScale9Enabled(true);
		generalLvbackGround->setContentSize(Size(34,14));
		generalLvbackGround->setPosition(Vec2(-28,-20));
		generalBtn->addChild(generalLvbackGround);

		int generalLevel_ = GameView::getInstance()->generalsInLineList.at(i)->level();
		char generalLevelStr[10];
		sprintf(generalLevelStr,"%d",generalLevel_);
		std::string generalLevelString_ = "LV";
		generalLevelString_.append(generalLevelStr);
		auto generalLvLabel = Label::createWithTTF(generalLevelString_.c_str(), APP_FONT_NAME, 12);
		generalLvLabel->setAnchorPoint(Vec2(0,0));
		generalLvLabel->setPosition(Vec2(-25,-20));
		generalBtn->addChild(generalLvLabel);

		auto generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[GameView::getInstance()->generalsInLineList.at(i)->modelid()];
		std::string generalNameStr_ = generalMsgFromDb->name();
		int generalIndexQuality = GameView::getInstance()->generalsInLineList.at(i)->currentquality();
		Color3B generalColor_ =GameView::getInstance()->getGeneralsColorByQuality(generalIndexQuality);
		auto generalNameLabel = Label::createWithTTF(generalNameStr_.c_str(), APP_FONT_NAME, 18);
		generalNameLabel->setAnchorPoint(Vec2(0.5f,0));
		generalNameLabel->setPosition(Vec2(smaillFrame->getContentSize().width/2,0 ));
		generalNameLabel->setColor(generalColor_);
		generalBtn->addChild(generalNameLabel);

		int generalPressionIndex_ = generalMsgFromDb->get_profession();
		std::string generalPressionStr_ = BasePlayer::getProfessionNameIdxByIndex(generalPressionIndex_);

		auto generalPressionLabel = Label::createWithTTF(generalPressionStr_.c_str(), APP_FONT_NAME, 18);
		generalPressionLabel->setAnchorPoint(Vec2(0,0));
		generalPressionLabel->setPosition(Vec2(10,-20));
		generalBtn->addChild(generalPressionLabel);

		// check if the General is selected
		if (GameView::getInstance()->generalsInLineList.at(i)->id() == generalId)
		{
			generalBtn->addChild(roleSelectButton_);
			generalProfession = BasePlayer::getProfessionIdxByName(generalPressionStr_);
			generalLevel = GameView::getInstance()->generalsInLineList.at(i)->level();
		}

		/*
		if (packscene != NULL )
		{
			long long generalIndex_ = GameView::getInstance()->generalsInLineList.at(i)->id();
			if (packscene->selectActorId == generalIndex_)
			{
				generalBtn->addChild(roleSelectButton_);
				generalProfession = BasePlayer::getProfessionIdxByName(generalPressionStr_);
			}
		}
		*/
		//general icon
		//CGeneralBaseMsg * generalBaseMsgFromDB  = GeneralsConfigData::s_generalsBaseMsgData[GameView::getInstance()->generalsInLineList.at(i)->modelid()];
		std::string sprite_icon_path = "res_ui/generals46X45/";
		sprite_icon_path.append(generalMsgFromDb->get_head_photo());
		sprite_icon_path.append(".png");

		auto generalIcon_ =ImageView::create();
		generalIcon_->loadTexture(sprite_icon_path.c_str());
		generalIcon_->setAnchorPoint(Vec2(0.5f,0.5f));
		generalIcon_->setPosition(Vec2(0,0));
		smaillFrame->addChild(generalIcon_);
	}
}

void GoodsItemInfoBase::getRoleEquipmentList()
{
	/*
	// show player equipment goodsinfo
	for (int i=0;i<GameView::getInstance()->EquipListItem.size();i++)
	{
		CEquipment * selfEquip = new CEquipment();
		selfEquip->CopyFrom(*GameView::getInstance()->EquipListItem.at(i));
		selfEquipVector.push_back(selfEquip);
	}
	*/
}

void GoodsItemInfoBase::getGeneralEquipmentList( long long generalId )
{
	/*
	for (int generalindex=0;generalindex <GameView::getInstance()->generalsInLineDetailList.size();generalindex++)
	{
		if (generalId == GameView::getInstance()->generalsInLineDetailList.at(generalindex)->generalid())
		{
			// show general equipment goodsinfo
			int generalEquipVectorSize = GameView::getInstance()->generalsInLineDetailList.at(generalindex)->equipments_size();
			for (int i=0;i<generalEquipVectorSize;i++)
			{
				CEquipment * selfEquip = new CEquipment();
				selfEquip->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(generalindex)->equipments(i));
				selfEquipVector.push_back(selfEquip);
			}
		}
	}
	*/
}

void GoodsItemInfoBase::callBackQualityDes(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		std::map<int, std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(17);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end())
		{
			return;
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			{
				Size winSize = Director::getInstance()->getVisibleSize();
				auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[17].c_str());
				GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				showSystemInfo->setIgnoreAnchorPointForPosition(false);
				showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				showSystemInfo->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}
