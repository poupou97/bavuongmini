#ifndef _BACKPACKSCENE_GOODSITEMINFOBASE_H
#define _BACKPACKSCENE_GOODSITEMINFOBASE_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GoodsInfo;
class CEquipProperty;
class CEquipment;
class GoodsItemInfoBase:public UIScene
{
public:
	GoodsItemInfoBase();
	~GoodsItemInfoBase();

	static GoodsItemInfoBase *create(GoodsInfo * goodsInfo,std::vector<CEquipment *> curEquipment,long long generalId = 0);
	
	bool init(GoodsInfo * goodsInfo,std::vector<CEquipment *> curEquipment,long long generalId);
	virtual void onEnter();
	virtual void onExit();
	
	void showTouchEquipMentInfo(GoodsInfo * goodsInfo,int index);
	void showTouchGoodsInfo(GoodsInfo * goodsInfo);
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
protected:
	std::string buttonImagePath;
	enum {
		EQUIPMEGTBASEDESTAG=0,
		EQUIPMEGTBASEVALUETAG=10,

		EQUIPMENTSTARTAG=20,
		EQUIPMENTSTARVALUETAG=30,

		EQUIPMENTACTIVETAG=40,
		EQUIPMENTACTIVE_END_TAG=45,
		EQUIPMENTACTIVEVALUETAG=50,

		EQUIPMENTGEMTAG=60,
		EQUIPMENTGEMVALUETAG=70,
		EQUIPMENTGEMICONTAG = 75,
		EQUIPMENTSUITTAG=80,
		EQUIPMENTSUITVALUETAG=90,
		EQUIPMENTSUITNAME = 100,
		};
public:
	Layout * touchPanelInfo;
	Layout *bodyPanelInfo;
	ui::ScrollView * scrollView_info;
	//std::vector<CEquipProperty *> equipmentPropertyV;
public:
	int touchHight;
	//Layer * layerScene;
	//equipment
	//ImageView * headImage_equip;
	//Label * label_equipName;
	//Label * label_profession;
	//Label * binding_label;
	//int cur_starLevel;
	//ImageView* starlevel_All;
	//ImageView * starlevel_All_copy;
	//ImageView* starlevel_int;
	//ImageView* starlevel_half;
	//Label * label_uselevel;
	//Label * label_powercount;
	//base list
	//ImageView * boundary_touch_first;
	//Label *label_equipProperty_base;
	Label *basePropertyDes;
	Label *basePropertyValue;
	//star
	ImageView * boundary_Star;
	Label *label_equipProperty_star;
	Label *starPropertyDes;
	Label *starPropertyValue;
	//active
	ImageView * boundary_Active;
	Label *label_equipProperty_active;
	Label *activePropertyDes;
	Label *activePropertyValue;
	////gem
	ImageView * boundary_Gem;
	Label *label_equipProperty_gem;
	Label *gemPropertyDes;
	//Label *gemPropertyValue;
	//suit
	ImageView * boundary_Suit;
	Label *label_equipProperty_suit;

	
	int baseValueNum;
	int starValueNum;
	int activeValueNum;
	int gemValueNum;
	int suitValueNum;
	std::string getEquipmentQualityByIndex(int quality);
	Color3B getEquipmentColorByQuality(int quality);
	bool isRoleEquip_;//�Ƿ���װ��
	bool isSetSuitColor;
	void isEquipOfBody(GoodsInfo * goods);
	void getSelfEquipVector(GoodsInfo * goodsInfo,std::vector<CEquipment *> vector_);
	//is touch self equip
	ImageView * flag_toucheSelfEquip;
	void isTouchSameEquipment();
public:
	///goods
	GoodsInfo * goodsInfo_General;

	std::vector<CEquipment *> curShowEquipment;
	long long curGeneralId;
	
	ImageView * headImage_goods;
	Label * label_goods_name;
	//Label * label_goodsType;
	Label * label_goods_uselevel;
	Label * label_goodsPrice;
	Label * label_description;

	int generalProfession;
	int generalLevel;
	void addGeneralButton(long long generalId);
	void showGeneralEquipMentInfo(Ref *pSender, Widget::TouchEventType type);
	//std::string pression;
	void setEquipProfessionAndLevelColor(int pression_,int level_);

	void getRoleEquipmentList();
	void getGeneralEquipmentList(long long generalId);


	//touch des getEquipmentQualityByIndex
	void callBackQualityDes(Ref *pSender, Widget::TouchEventType type);
private:
	ImageView * flag_equip;
	Label * notEquipment;
	int selfEquipSuit;
	std::vector<CEquipment *> selfEquipVector;
};

#endif