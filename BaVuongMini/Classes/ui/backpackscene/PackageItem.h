
#ifndef _BACKPACKSCENE_PACKAGEITEM_H_
#define _BACKPACKSCENE_PACKAGEITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class FolderInfo;
class GoodsInfo;

typedef enum {
	COMMONITEM,
	VOIDITEM,
	LOCKITEM,
}PacType;

class PackageItem :public ui::Widget
{
public:
	PackageItem();
	~PackageItem();
	static PackageItem * create(FolderInfo* folder);
	bool init(FolderInfo* folder);
	static PackageItem * create(GoodsInfo* goods);
	bool initWithGoods(GoodsInfo* goods);
	static PackageItem * createLockedItem(int folderIndex);
	bool initLockedItem(int folderIndex);
	virtual void setLabelNameText(char * str);
	//�Ƿ���ʾ�����Ĭ����ʾ��
	virtual void setNumVisible(bool visible);
	//�Ƿ���ʾ�󶨱�ʶ��Ĭ�ϲ���ʾ��
	virtual void setBoundVisible(bool visible);
	//�Ƿ���ʾ��ʯ���ԣ�Ĭ�ϲ���ʾ��
	virtual void setBoundJewelPropertyVisible(bool visible);
	//get cur gem property
	std::string getCurGemIconPath(std::string goodsId);

	//������
	FolderInfo * curFolder;
	//Ӹ�����������һ�
	GoodsInfo * curGood;
	//////////////////change by liuzhenxing
	void setGray(bool isGray,std::string isShowGoodsNum);
	void setRed(bool isRed);
	void setAvailable(bool isAvailable);

	void BeganToRunCD();

	float getCurProgress();
public:
	//��ѧ��װ��
	Button * Btn_pacItemFrame;

	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	//��ѧ
	int mTutorialScriptInstanceId;
protected:
	int index ;
	Label * Lable_name;
	Label * Lable_num;
	PacType cur_type;

	void PackageItemEvent(Ref *pSender, Widget::TouchEventType type);

	void GoodItemEvent(Ref *pSender, Widget::TouchEventType type);

	Vec2 autoGetPosition(Ref * pSender);

	void BtnLockEvent(Ref *pSender, Widget::TouchEventType type);

	void SureEvent(Ref * pSender);
	void CancelEvent(Ref * pSender);

	void drugCoolDownCallBack(Node* node);

	long millisecondNow();
	bool isDoubleTouch();
	long lastTouchTime;
private:
	ImageView *ImageView_packageItem;
	ImageView *ImageView_bound;

	ProgressTimer* mProgressTimer_skill;
};

#endif;