
#ifndef _BACKPACKSCENE_GENERALSLISTFORTAKEDRUG_H_
#define _BACKPACKSCENE_GENERALSLISTFORTAKEDRUG_H_

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class FolderInfo;

/////////////////////////////////
/**
 * �����е���佫ҩƷ��ʹ�õ�����佫�б
 * @author yangjun
 * @version 0.1.0
 * @date 2014.1.23
 */
class GeneralsListForTakeDrug : public UIScene,public TableViewDelegate,public TableViewDataSource
{
public:
	GeneralsListForTakeDrug();
	~GeneralsListForTakeDrug();

	static GeneralsListForTakeDrug * create(FolderInfo * folderInfo);
	bool init(FolderInfo * folderInfo);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void showCountForGeneralExp(Ref *pSender);
public: 
	int curPackageItemIndex;
	FolderInfo * curFolder;
	long long curRoleId;
};

/////////////////////////////////////////////////////////////
class CGeneralBaseMsg;

class GeneralsListForTakeDrugCell : public TableViewCell
{
public:
	GeneralsListForTakeDrugCell();
	~GeneralsListForTakeDrugCell();
	static GeneralsListForTakeDrugCell* create(CGeneralBaseMsg * generalBaseMsg);
	bool init(CGeneralBaseMsg * generalBaseMsg);

	CGeneralBaseMsg* getCurGeneralBaseMsg();

private:
	CGeneralBaseMsg * curGeneralBaseMsg; 
	int m_index;
};


#endif

