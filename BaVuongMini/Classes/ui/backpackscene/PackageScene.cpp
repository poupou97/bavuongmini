#include "PackageScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"
#include "GameView.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "PackageItem.h"
#include "EquipmentItem.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../extensions/UITab.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../generals_ui/GeneralsUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../../gamescene_state/role/GameActorAnimation.h"
#include "../../messageclient/element/CActiveRole.h"
#include "../goldstore_ui/GoldStoreUI.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "AppMacros.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CDrug.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"
#include "../../utils/GameUtils.h"
#include "../generals_ui/RecuriteActionItem.h"
#include "../../ui/storehouse_ui/StoreHouseUI.h"
#include "2d/CCParticleSystem.h"
#include "../../utils/GameConfig.h"
#include "../extensions/ShowSystemInfo.h"
#include "BattleAchievementUI.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "../../ui/extensions/RecruitGeneralCard.h"

using namespace CocosDenshion;

#define PROFESSION_MENGJIANG "res_ui/renwubeibao/mj.png"
#define PROFESSION_GUIMOU "res_ui/renwubeibao/gm.png"
#define PROFESSION_HAOJIE "res_ui/renwubeibao/hj.png"
#define PROFESSION_SHENSHE "res_ui/renwubeibao/ss.png"

#define  RoleAnimationTag 687
#define  kTag_generalLevelInfo 265;

enum{
	Equipment_Helmet,
	Equipment_Clothes,
	Equipment_Weapon,
	Equipment_Ornament,
	Equipment_Shoes,
	Equipment_Ring,
};

// UIPageViewTest
PackageScene::PackageScene():
selectActorId(0),
selectTabviewIndex(0)
{
	//coordinate for equipmentlist
	for (int m = 2;m>=0;m--)
	{
		for (int n = 1;n>=0;n--)
		{
			int _m = 2-m;
			int _n = 1-n;

			Coordinate temp;
			temp.x = 23+_n*243;
			temp.y = 1+m*122;

			Coordinate_equip.push_back(temp);
		}
	}
	//equipmentitem name 
	for (int i = 0; i<12; i++)
	{
		std::string equipBaseName = "equipment_";
		char num[4];
		int tmpID = i;
		sprintf(num, "0%d", tmpID);
		std::string number = num;
		equipBaseName.append(num);
		equipmentItem_name[i] = equipBaseName;
	}
}

PackageScene::~PackageScene()
{
}

PackageScene * PackageScene::create()
{
	auto widget = new PackageScene();
	if (widget && widget->init())
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}


void PackageScene::onEnter()
{
	GeneralsListBase::onEnter();

	auto  action = Sequence::create(
		ScaleTo::create(0.15f*1/2,1.1f),
		ScaleTo::create(0.15f*1/3,1.0f),
		NULL);

	runAction(action);

	GameUtils::playGameSound(PACKAGE_OPEN, 2, false);
	////this->setTouchEnabled(true);
	//this->setContentSize(Size(800,480));
}

void PackageScene::onExit()
{
	GeneralsListBase::onExit();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

bool PackageScene::init()
{
    if (GeneralsListBase::init())
    {
		Size winsize = Director::getInstance()->getVisibleSize();

		this->scheduleUpdate();

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(800, 480));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);
		
		//加载UI
		if(LoadSceneLayer::s_pPanel->getParent() != NULL)
		{
			LoadSceneLayer::s_pPanel->removeFromParentAndCleanup(false);
		}

 		ppanel = LoadSceneLayer::s_pPanel;
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->getVirtualRenderer()->setContentSize(Size(800, 480));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		const char * firstStr = StringDataManager::getString("UIName_ren");
		const char * secondStr = StringDataManager::getString("UIName_wu");
		const char * thirdStr = StringDataManager::getString("UIName_bei_1");
		const char * fourthStr = StringDataManager::getString("UIName_bao_1");
		auto atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,fourthStr);
		atmature->setPosition(Vec2(30,240));
		u_layer->addChild(atmature);

		//关闭按钮
		Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->setPressedActionEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(PackageScene::closeBeganEvent,this));
		//整理按钮
		auto Button_sort = (Button*)Helper::seekWidgetByName(ppanel,"Button_zhengli");
		Button_sort->setTouchEnabled(true);
		Button_sort->addTouchEventListener(CC_CALLBACK_2(PackageScene::SortEvent, this));
		Button_sort->setPressedActionEnabled(true);

		auto Button_goldStore = (Button*)Helper::seekWidgetByName(ppanel,"Button_shangcheng");
		Button_goldStore->setTouchEnabled(true);
		Button_goldStore->addTouchEventListener(CC_CALLBACK_2(PackageScene::GoldStoreEvent, this));
		Button_goldStore->setPressedActionEnabled(true);

		auto Button_remoteStoreHouse = (Button*)Helper::seekWidgetByName(ppanel,"Button_remoteStoreHouse");
		Button_remoteStoreHouse->setTouchEnabled(true);
		Button_remoteStoreHouse->addTouchEventListener(CC_CALLBACK_2(PackageScene::RemoteStoreHouseEvent, this));
		Button_remoteStoreHouse->setPressedActionEnabled(true);
		//one key of all equip 
		auto Button_allEquip = (Button*)Helper::seekWidgetByName(ppanel,"Button_allEquip_temp");
		Button_allEquip->setTouchEnabled(true);
		Button_allEquip->addTouchEventListener(CC_CALLBACK_2(PackageScene::oneKeyAllEquip, this));
		Button_allEquip->setPressedActionEnabled(true);

		// vip开关控制
		//bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		//if (vipEnabled)
		//{
		//	Button_remoteStoreHouse->setVisible(true);
		//}
		//else
		//{
		//	Button_remoteStoreHouse->setVisible(false);
		//}

		// 战功奖励
		auto Button_achievement = (Button*)Helper::seekWidgetByName(ppanel,"Button_achievement");
		Button_achievement->setTouchEnabled(true);
		Button_achievement->addTouchEventListener(CC_CALLBACK_2(PackageScene::AchievementEvent,this));
		Button_achievement->setPressedActionEnabled(true);
		Button_achievement->setVisible(true);

		//称谓
		auto Button_title = (Button*)Helper::seekWidgetByName(ppanel,"Button_title");
		Button_title->setTouchEnabled(true);
		Button_title->addTouchEventListener(CC_CALLBACK_2(PackageScene::TitleEvent, this));
		Button_title->setPressedActionEnabled(true);
		Button_title->setVisible(false);

		//获得面板
		playerPanel = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_renwu");
		dataPanel = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_property");
		infoPanle = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_informaion");
		playerPanel->setVisible(true);
		dataPanel->setVisible(false);
		infoPanle->setVisible(false);

		ImageView_wuqi = (ImageView*)Helper::seekWidgetByName(playerPanel, "LV4_wuqi");
		ImageView_toukui = (ImageView*)Helper::seekWidgetByName(playerPanel, "LV4_toukui");
		ImageView_yifu = (ImageView*)Helper::seekWidgetByName(playerPanel, "LV4_yifu");
		ImageView_xiezi = (ImageView*)Helper::seekWidgetByName(playerPanel, "LV4_xiezi");
		ImageView_jiezhi = (ImageView*)Helper::seekWidgetByName(playerPanel, "LV4_jiezhi");
		ImageView_peishi = (ImageView*)Helper::seekWidgetByName(playerPanel, "LV4_peishi");
        
        auto pScrollView_information = (ui::ScrollView*)Helper::seekWidgetByName(infoPanle, "ScrollView_information");
        pScrollView_information->setBounceEnabled(true);
        
		auto pScrollView_property = (ui::ScrollView*)Helper::seekWidgetByName(dataPanel, "ScrollView_property");
        pScrollView_property->setBounceEnabled(true);

		auto btn_firstDes = (Button*)Helper::seekWidgetByName(pScrollView_property, "Button_firstDes");
		btn_firstDes->setTouchEnabled(true);
		btn_firstDes->addTouchEventListener(CC_CALLBACK_2(PackageScene::FirstGradeDesEvent, this));
		auto btn_secondtDes = (Button*)Helper::seekWidgetByName(pScrollView_property, "Button_secondDes");
		btn_secondtDes->setTouchEnabled(true);
		btn_secondtDes->addTouchEventListener(CC_CALLBACK_2(PackageScene::SecondGradeDesEvent, this));

		playerLayer = Layer::create();
		u_layer->addChild(playerLayer);
		playerLayer->setVisible(true);

		//rola name
		Label_roleName = (Text*)Helper::seekWidgetByName(playerPanel,"Label_roleName");
        //Label_roleName->setStrokeEnabled(false);
        Label_roleName->setColor(Color3B(255,255,255));   // must use white color to show the special simbol correctly, for example, sun
		Label_roleName->setString(GameView::getInstance()->myplayer->getActiveRole()->rolebase().name().c_str());
		//Label_roleName->setColor(Color3B(47,93,13));
		Label_roleName->setVisible(true);
		//general name
		l_generalName = CCRichLabel::createWithString("456456", Size(100,20),NULL,NULL);
		l_generalName->setAnchorPoint(Vec2(0.5f,0.5f));
		l_generalName->setPosition(Vec2(220,387));
		l_generalName->setScale(0.9f);
		playerLayer->addChild(l_generalName);
		l_generalName->setVisible(false);

		// vip isopen
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (vipEnabled)
		{
			if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() > 0)
			{
				auto widget_VipInfo = MainScene::addVipInfoByLevelForWidget(GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel());
				playerLayer->addChild(widget_VipInfo);
				widget_VipInfo->setName("widget_VipInfo");
				widget_VipInfo->setPosition(Vec2(Label_roleName->getPosition().x+Label_roleName->getContentSize().width/2+15,Label_roleName->getPosition().y));
			}
		}

		imageView_country = (ImageView *)Helper::seekWidgetByName(playerPanel,"ImageView_country");
		int countryId = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().country();
		if (countryId > 0 && countryId <6)
		{
			std::string countryIdName = "country_";
			char id[2];
			sprintf(id, "%d", countryId);
			countryIdName.append(id);
			std::string iconPathName = "res_ui/country_icon/";
			iconPathName.append(countryIdName);
			iconPathName.append(".png");
			imageView_country->loadTexture(iconPathName.c_str());
			imageView_country->setPosition(Vec2(Label_roleName->getPosition().x-Label_roleName->getContentSize().width/2-imageView_country->getContentSize().width/2,imageView_country->getPosition().y));
		}

		imageView_professionName = (ImageView *)Helper::seekWidgetByName(playerPanel,"ImageView_jobvalue");

		l_roleLevel = (Text*)Helper::seekWidgetByName(playerPanel,"Label_roleLevel");
        //l_roleLevel->setStrokeEnabled(false);
		l_fightPoint = (Text*)Helper::seekWidgetByName(playerPanel,"Label_figntpoint_value");
        //l_fightPoint->setStrokeEnabled(false);
// 		l_allFightPoint = (Text*)Helper::seekWidgetByName(playerPanel,"Label_allfigntpoint_value");
//         l_allFightPoint->setStrokeEnabled(false);
		//->setColor(Color3B(255,255,255));
		//l_allFightPoint->setVisible(false);
		/*
		ParticleSystem* particleFire = ParticleSystemQuad::create("animation/texiao/particledesigner/huo2.plist");
		particleFire->setPositionType(ParticleSystem::PositionType::FREE);
		particleFire->setPosition(Vec2(143,162));
		particleFire->setAnchorPoint(Vec2(0.5f,0.5f));
		particleFire->setVisible(true);
		//particleFire->setLife(0.5f);
		particleFire->setLifeVar(0);
		particleFire->setScale(0.5f);
		particleFire->setScaleX(0.6f);
		playerLayer->addChild(particleFire);

		ParticleSystem* particleFire1 = ParticleSystemQuad::create("animation/texiao/particledesigner/huo2.plist");
		particleFire1->setPositionType(ParticleSystem::PositionType::FREE);
		particleFire1->setPosition(Vec2(147,165));
		particleFire1->setAnchorPoint(Vec2(0.5f,0.5f));
		particleFire1->setVisible(true);
		//particleFire1->setLife(0.5f);
		particleFire1->setLifeVar(0);
		particleFire1->setScale(0.5f);
		particleFire1->setScaleX(0.6f);
		playerLayer->addChild(particleFire1);
		*/
		auto u_tempLayer = Layer::create();
		playerLayer->addChild(u_tempLayer);

		/*Label * l_allFPName = Label::create();
		l_allFPName->setText(StringDataManager::getString("rank_capacity"));
		l_allFPName->setFntFile("res_ui/font/ziti_1.fnt");
		l_allFPName->setAnchorPoint(Vec2(0.5f,0.5f));
		l_allFPName->setPosition(Vec2(185,167));
		l_allFPName->setScale(0.6f);
		u_tempLayer->addChild(l_allFPName);*/
		auto Image_allFPName = ImageView::create();
		Image_allFPName->loadTexture("res_ui/renwubeibao/zi2.png");
		Image_allFPName->setAnchorPoint(Vec2(0.5f,0.5f));
		Image_allFPName->setPosition(Vec2(103,162));
		Image_allFPName->setScale(1.0f);
		u_tempLayer->addChild(Image_allFPName);

		l_allFightPoint = Label::createWithBMFont("res_ui/font/ziti_1.fnt", "123456789");
		l_allFightPoint->setAnchorPoint(Vec2(0.5f,0.5f));
		l_allFightPoint->setPosition(Vec2(186,162));
		l_allFightPoint->setScale(0.6f);
		u_tempLayer->addChild(l_allFightPoint);
// 
// 		Label * l_allFPName = Label::create();
// 		l_allFPName->setText(StringDataManager::getString("rank_capacity"));
// 		l_allFPName->setFontName(APP_FONT_NAME);
// 		l_allFPName->setFontSize(18);
// 		l_allFPName->setAnchorPoint(Vec2(0.5f,0.5f));
// 		l_allFPName->setPosition(Vec2(185,169));
// 		playerLayer->addChild(l_allFPName);
// 
// 		Label * l_allFPValue = Label::create();
// 		l_allFPValue->setText("123456789");
// 		l_allFPValue->setFontName(APP_FONT_NAME);
// 		l_allFPValue->setFontSize(18);
// 		l_allFPValue->setAnchorPoint(l_allFightPoint->getAnchorPoint());
// 		l_allFPValue->setPosition(l_allFightPoint->getPosition());
// 		playerLayer->addChild(l_allFPValue);
		
		l_nameValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_nameValue");
        l_nameValue->setColor(Color3B(255,255,255));
        //l_nameValue->setStrokeEnabled(false);
		l_levelValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_lvValue");
        //l_levelValue->setStrokeEnabled(false);
		l_powerValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_powerValue");
        //l_powerValue->setStrokeEnabled(false);
		l_aglieValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_agileValue");  //敏捷
        //l_aglieValue->setStrokeEnabled(false);
		l_intelligenceValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_intelligenceValue");  //智力
        //l_intelligenceValue->setStrokeEnabled(false);
		l_focusValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_focusValue");  //专注
        //l_focusValue->setStrokeEnabled(false);
		l_curHpValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_HpMin");
        //l_curHpValue->enableOutline(Color4B::BLACK, 2.0f);
		auto lGang1 = (Text*)Helper::seekWidgetByName(dataPanel,"Label_gang1");
        lGang1->enableOutline(Color4B::BLACK, 2);
		l_allHpValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_HpMax");
        l_allHpValue->enableOutline(Color4B::BLACK, 2);
		imageView_hp = (ImageView *)Helper::seekWidgetByName(dataPanel,"ImageView_hp");
		l_curMpValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_MpMin");
        l_curMpValue->enableOutline(Color4B::BLACK, 2);
		auto lGang2 = (Text*)Helper::seekWidgetByName(dataPanel,"Label_gang2");
        lGang2->enableOutline(Color4B::BLACK, 2);
		l_allMpValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_Mpmax");
        l_allMpValue->enableOutline(Color4B::BLACK, 2);
		imageView_mp = (ImageView *)Helper::seekWidgetByName(dataPanel,"ImageView_mp");
		l_phyAttackValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_PhysicalAttackValue");
        //l_phyAttackValue->setStrokeEnabled(false);
		l_magicAttackValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_MagicAttackValue");
        //l_magicAttackValue->setStrokeEnabled(false);
		l_phyDenValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_Physicaldefense_Digital");
        //l_phyDenValue->setStrokeEnabled(false);
		l_magicDenValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_power_physical_defense");
        //l_magicDenValue->setStrokeEnabled(false);
		l_hitValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_hitValue");  //命中
        //l_hitValue->setStrokeEnabled(false);
		l_dodgeValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_dodgeValue"); //闪避
        //l_dodgeValue->setStrokeEnabled(false);
		l_critValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_critValue"); //暴击
        //l_critValue->setStrokeEnabled(false);
		l_critDamageValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_CritDamageValue"); //暴击伤害
        //l_critDamageValue->setStrokeEnabled(false);
		l_atkSpeedValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_AttackSpeedValue");
        //l_atkSpeedValue->setStrokeEnabled(false);
		l_moveSpeedValue = (Text*)Helper::seekWidgetByName(dataPanel,"Label_SpeedValue");
        //l_moveSpeedValue->setStrokeEnabled(false);

		l_roleNameValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_roleNameValue"); ///
        l_roleNameValue->setColor(Color3B(255,255,255));
        //l_roleNameValue->setStrokeEnabled(false);
		l_roleLvValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_roleLvValue");///
        //l_roleLvValue->setStrokeEnabled(false);
		l_jobValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_jobValue");
        //l_jobValue->setStrokeEnabled(false);
		l_genderValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_genderValue");
        //l_genderValue->setStrokeEnabled(false);
		l_countryValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_countryValue");
        //l_countryValue->setStrokeEnabled(false);
		l_titleValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_titileValue");
        //l_titleValue->setStrokeEnabled(false);
		l_factionValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_factionValue");
        //l_factionValue->setStrokeEnabled(false);
		l_honorValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_officialValue");
        //l_honorValue->setStrokeEnabled(false);
		l_familyValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_familyValue");
        //l_familyValue->setStrokeEnabled(false);
		l_battleAchValue = (Text*)Helper::seekWidgetByName(ppanel, "Label_zhangongValue");
		//l_battleAchValue->setStrokeEnabled(false);
		//l_experienceValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_experienceValue"); 
		l_physicalValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_experienceValue");
        //l_physicalValue->setStrokeEnabled(false);
		l_expValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_exp_caps");
        l_expValue->enableOutline(Color4B::BLACK, 2);
		imageView_exp = (ImageView * )Helper::seekWidgetByName(ppanel,"ImageView_exp_schedule");

		ReloadProperty();
		ReloadInfo();
			
		//jinbi
		int m_goldValue = GameView::getInstance()->getPlayerGold();
		char GoldStr_[20];
		sprintf(GoldStr_,"%d",m_goldValue);
		Label_gold = (Text*)Helper::seekWidgetByName(ppanel,"Label_gold");
		Label_gold->setString(GoldStr_);
		//yuanbao
		int m_goldIngot =  GameView::getInstance()->getPlayerGoldIngot();
		char GoldIngotStr_[20];
		sprintf(GoldIngotStr_,"%d",m_goldIngot);
		Label_goldIngot = (Text*)Helper::seekWidgetByName(ppanel,"Label_goldIngold");
		Label_goldIngot->setString(GoldIngotStr_);
		//bangdingyuanbao
		Label_bingGoldIngot = (Text*)Helper::seekWidgetByName(ppanel,"Label_binghGoldIngold");
		int m_bingGoldIngot =  GameView::getInstance()->getPlayerBindGoldIngot();
		char bingGoldIngotStr_[20];
		sprintf(bingGoldIngotStr_,"%d",m_bingGoldIngot);
		Label_bingGoldIngot->setString(bingGoldIngotStr_);

		//当前显示的Page
		currentPage = (ImageView *)Helper::seekWidgetByName(ppanel,"dian_on");

		const char * normalImage = "res_ui/tab_1_off.png";
		const char * selectImage = "res_ui/tab_1_on.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_1_on.png";
		//左侧TAB
		const char *str1 = StringDataManager::getString("packagescene_tab_role");
		char* p1 =const_cast<char*>(str1);
		const char *str2 = StringDataManager::getString("packagescene_tab_property");
		char* p2 =const_cast<char*>(str2);
		const char *str3 = StringDataManager::getString("packagescene_tab_infomation");
		char* p3 =const_cast<char*>(str3);

		char * leftItemName[] ={p1,p2,p3};
		leftTab = UITab::createWithText(3,normalImage,selectImage,finalImage,leftItemName,HORIZONTAL,9);
		leftTab->setAnchorPoint(Vec2(0,0));
		leftTab->setPosition(Vec2(58,415));
		leftTab->setHighLightImage((char * )highLightImage);
		leftTab->setDefaultPanelByIndex(0);
		this->ChangeLeftPanelByIndex(0);
		leftTab->addIndexChangedEvent(this,coco_indexchangedselector(PackageScene::LeftIndexChangedEvent));
		leftTab->setPressedActionEnabled(true);
		u_layer->addChild(leftTab);

		// Create the page view
		reCreatePageVeiw();

// 		Layer * u_pageFrameLayer = Layer::create();
// 		u_pageFrameLayer->setIgnoreAnchorPointForPosition(false);
// 		u_pageFrameLayer->setAnchorPoint(Vec2(0.5f,0.5f));
// 		u_pageFrameLayer->setContentSize(Size(800, 480));
// 		u_pageFrameLayer->setPosition(Vec2(winsize.width/2,winsize.height/2));
// 		this->addChild(u_pageFrameLayer);
// 		ImageView * pageView_kuang = ImageView::create();
// 		pageView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setScale9Enabled(true);
// 		pageView_kuang->setContentSize(Size(363,323));
// 		pageView_kuang->setCapInsets(Rect(30,30,1,1));
// 		pageView_kuang->setAnchorPoint(Vec2(0,0));
// 		pageView_kuang->setPosition(Vec2(390,90));
// 		u_pageFrameLayer->addChild(pageView_kuang);
		
		auto my = GameView::getInstance()->myplayer;
		// show the player's equipment

// 		std::vector<CEquipment *>::iterator iter_compair;
// 		for (iter_compair =GameView::getInstance()->compairEquipVector.begin();iter_compair!=GameView::getInstance()->compairEquipVector.end();iter_compair++)
// 		{
// 			delete * iter_compair;
// 		}
// 		GameView::getInstance()->compairEquipVector.clear();
// 
// 		for (int i=0;i<GameView::getInstance()->EquipListItem.size();i++)
// 		{
// 			CEquipment * equip_ =new CEquipment();
// 			equip_->CopyFrom( * GameView::getInstance()->EquipListItem.at(i));
// 			GameView::getInstance()->compairEquipVector.push_back(equip_);
// 		}
		RefreshEquipment(GameView::getInstance()->EquipListItem);
		// show the player's animation
		RefreshRoleAnimation((CActiveRole*)my->getActiveRole());
		// show the role's(container generals) Info
		RefreshRoleInfo((CActiveRole*)GameView::getInstance()->myplayer->getActiveRole(),GameView::getInstance()->myplayer->player->fightpoint());

		auto testButton = Button::create();
		testButton->setTouchEnabled(true);
		testButton->loadTextures("res_ui/button_2_off.png","res_ui/button_2_off.png","");
		testButton->setAnchorPoint(Vec2::ZERO);
		testButton->setPosition(Vec2(200,200));
		testButton->addTouchEventListener(CC_CALLBACK_2(PackageScene::closeBeganEvent, this));
		m_pLayer->addChild(testButton);
		testButton->setVisible(false);

		this->generalsListStatus = HaveNext;
		//武将列表
		generalList_tableView = TableView::create(this,Size(319,109));
		//generalList_tableView->setSelectedEnable(true);
		//generalList_tableView->setSelectedScale9Texture("res_ui/highlight.png", Rect(26, 26, 1, 1), Vec2(0,0));
		generalList_tableView->setDirection(TableView::Direction::HORIZONTAL);
		generalList_tableView->setAnchorPoint(Vec2(0,0));
		generalList_tableView->setPosition(Vec2(62,26));
		generalList_tableView->setContentOffset(Vec2(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//generalList_tableView->setPressedActionEnabled(true);
		playerLayer->addChild(generalList_tableView);

		tempLayer = Layer::create();
		tempLayer->setIgnoreAnchorPointForPosition(false);
		tempLayer->setAnchorPoint(Vec2(0.5f,0.5f));
		tempLayer->setContentSize(Size(800, 480));
		tempLayer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(tempLayer);
// 		ImageView * roleList_kuang = ImageView::create();
// 		roleList_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		roleList_kuang->setScale9Enabled(true);
// 		roleList_kuang->setContentSize(Size(321,114));
// 		roleList_kuang->setCapInsets(Rect(30,30,1,1));
// 		roleList_kuang->setAnchorPoint(Vec2(0,0));
// 		roleList_kuang->setPosition(Vec2(61,35));
// 		tempLayer->addChild(roleList_kuang);
		auto imageView_leftFlag = ImageView::create();
		imageView_leftFlag->loadTexture("res_ui/shousuo.png");
		imageView_leftFlag->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_leftFlag->setPosition(Vec2(76,85));
		imageView_leftFlag->setRotation(-135);
		imageView_leftFlag->setScale(0.5f);
		tempLayer->addChild(imageView_leftFlag);
		auto imageView_rightFlag = ImageView::create();
		imageView_rightFlag->loadTexture("res_ui/shousuo.png");
		imageView_rightFlag->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_rightFlag->setPosition(Vec2(367,85));
		imageView_rightFlag->setRotation(45);
		imageView_rightFlag->setScale(0.5f);
		tempLayer->addChild(imageView_rightFlag);
		
		int openlevel = 0;//shor item equip is open level
		std::map<int,int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(26);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[26];
		}

		if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
		{
			auto btn_mengban = Button::create();
			btn_mengban->setTouchEnabled(true);
			btn_mengban->loadTextures("res_ui/renwubeibao/mengban_green.png","res_ui/renwubeibao/mengban_green.png","");
			btn_mengban->setScale9Enabled(true);
			btn_mengban->setContentSize(Size(320,108));
			btn_mengban->setAnchorPoint(Vec2(0.5f,0.5f));
			btn_mengban->setPosition(Vec2(221,85));
			btn_mengban->setOpacity(200);
			tempLayer->addChild(btn_mengban);
			auto image_desFrame = ImageView::create();
			image_desFrame->loadTexture("res_ui/renwubeibao/zi_mengban.png");
 			image_desFrame->setScale9Enabled(true);
 			image_desFrame->setContentSize(Size(240,29));
			image_desFrame->setCapInsets(Rect(56,11,1,1));
			image_desFrame->setAnchorPoint(Vec2(0.5f,0.5f));
			image_desFrame->setPosition(Vec2(221,85));
			image_desFrame->setOpacity(200);
			tempLayer->addChild(image_desFrame);
			std::string str_des = "";
			char s_lv [10];
			sprintf(s_lv,"%d",openlevel);
			str_des.append(s_lv);
			str_des.append(StringDataManager::getString("package_openGeneralEquipment"));
			auto l_des = Label::createWithTTF(str_des.c_str(), APP_FONT_NAME, 20);
			l_des->setAnchorPoint(Vec2(0.5f,0.5f));
			l_des->setPosition(Vec2(221,85));
			tempLayer->addChild(l_des);
		}
		

		//refresh cd
		auto shortCutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortCutLayer)
		{
			for (int i = 0;i<7;++i)  // there are total 7 shortcut slots
			{
				if (i < GameView::getInstance()->shortCutList.size())
				{
					auto c_shortcut = GameView::getInstance()->shortCutList.at(i);
					if (c_shortcut->storedtype() == 2)    // this is item
					{
						if(CDrug::isDrug(c_shortcut->skillpropid()))
						{
							if (shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100))
							{
								auto shortcutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100);
								this->pacPageView->RefreshCD(c_shortcut->skillpropid());
							}
						}
					}
				}
			}
		}

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);

        return true;
    }
    return false;
}

void PackageScene::RefreshEquipment(std::vector<CEquipment *>& equipmentList)
{
	//delete old data
	for(int i = 0;i<12;++i)
	{
		if (playerPanel->getChildByName(this->equipmentItem_name[i].c_str()) != NULL)
		{
			playerPanel->getChildByName(this->equipmentItem_name[i].c_str())->removeFromParent();
			this->setPresentVoidEquipItem(i,true);
		}
	}
	//add new data
	int size = equipmentList.size();
	for (unsigned int i = 0; i < equipmentList.size(); i++)
	{
		auto tempEquip = equipmentList.at(i);
		auto equipment = EquipmentItem::create(tempEquip);
		equipment->setAnchorPoint(Vec2(0,0));
		//equipment->setContentSize(Size(90,109));
		equipment->setScale(1.0f);
		equipment->setName(this->equipmentItem_name[tempEquip->part()].c_str());
		playerPanel->addChild(equipment);

		this->setPresentVoidEquipItem(tempEquip->part(),false);
	}
}


void PackageScene::RefreshRoleAnimation( CActiveRole * activeRole )
{
	if (playerLayer->getChildByTag(RoleAnimationTag))
	{
		playerLayer->getChildByTag(RoleAnimationTag)->removeFromParent();
	}

	std::string roleFigureName = ActorUtils::getActorFigureName(*activeRole);
	
	std::string roleWeaponEffectForRefine = "";
	std::string roleWeaponEffectForStar = "";
	if (selectTabviewIndex == 0)
	{
		int equipSize = GameView::getInstance()->EquipListItem.size();
		for (int i=0;i<equipSize;i++)
		{
			if (GameView::getInstance()->EquipListItem.at(i)->part() == 4)
			{
				int pression_ = GameView::getInstance()->EquipListItem.at(i)->goods().equipmentdetail().profession();
				int refineLevel =  GameView::getInstance()->EquipListItem.at(i)->goods().equipmentdetail().gradelevel();
				int starLevel =  GameView::getInstance()->EquipListItem.at(i)->goods().equipmentdetail().starlevel();

				roleWeaponEffectForRefine = ActorUtils::getWeapEffectByRefineLevel(pression_,refineLevel);
				roleWeaponEffectForStar = ActorUtils::getWeapEffectByStarLevel(pression_, starLevel);
			}
		}
	}
	auto pAnim = ActorUtils::createActorAnimation(roleFigureName.c_str(), activeRole->hand().c_str(),
		roleWeaponEffectForRefine.c_str(), roleWeaponEffectForStar.c_str());
	pAnim->setPosition(Vec2(220,235));
	pAnim->setTag(RoleAnimationTag);
	playerLayer->addChild(pAnim);
}

GameActorAnimation* PackageScene::getRoleAnimation()
{
	if (playerLayer->getChildByTag(RoleAnimationTag))
	{
		return (GameActorAnimation*)playerLayer->getChildByTag(RoleAnimationTag);
	}
	else
	{
		return NULL;
	}
}

void PackageScene::RefreshRoleInfo(CActiveRole * activeRole,int fightpoint)
{
	if (GameView::getInstance()->myplayer->getRoleId() == activeRole->rolebase().roleid())
	{
		Label_roleName->setVisible(true);
		l_generalName->setVisible(false);

		imageView_country->setVisible(true);
		if (playerLayer->getChildByName("widget_VipInfo"))
		{
			playerLayer->getChildByName("widget_VipInfo")->setVisible(true);
		}
	}
	else
	{
		Label_roleName->setVisible(false);
		l_generalName->setVisible(true);
		//名字
		l_generalName->setString(activeRole->rolebase().name().c_str());

		imageView_country->setVisible(false);
		if (playerLayer->getChildByName("widget_VipInfo"))
		{
			playerLayer->getChildByName("widget_VipInfo")->setVisible(false);
		}
	}

	//职业
	switch(BasePlayer::getProfessionIdxByName(activeRole->profession().c_str()))
	{
	case PROFESSION_MJ_INDEX:
		{
			imageView_professionName->loadTexture(PROFESSION_MENGJIANG);
		}
		break;
	case PROFESSION_GM_INDEX:
		{
			imageView_professionName->loadTexture(PROFESSION_GUIMOU);
		}
		break;
	case PROFESSION_HJ_INDEX:
		{
			imageView_professionName->loadTexture(PROFESSION_HAOJIE);
		}
		break;
	case PROFESSION_SS_INDEX:
		{
			imageView_professionName->loadTexture(PROFESSION_SHENSHE);
		}
		break;
	default:
		{
			imageView_professionName->loadTexture(PROFESSION_MENGJIANG);
		}
		break;
	}
	//等级
	std::string str_lv = "LV.";
	char s_level[20];
	sprintf(s_level,"%d",activeRole->level());
	str_lv.append(s_level);
	l_roleLevel->setString(str_lv.c_str());

 	char s_fightpoint[20];
	sprintf(s_fightpoint,"%d",fightpoint);
    //l_fightPoint->setStrokeEnabled(false);
	l_fightPoint->setString(s_fightpoint);
	//总战斗力
	int allFightPoint = GameView::getInstance()->myplayer->player->fightpoint();
	for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();++i)
	{
		allFightPoint += GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint();
	}
	char s_allFightPoint[20];
	sprintf(s_allFightPoint,"%d",allFightPoint);
    //l_allFightPoint->setStrokeEnabled(false);
	l_allFightPoint->setString(s_allFightPoint);
}

void PackageScene::closeBeganEvent(Ref *pSender, Widget::TouchEventType type)
{
	CCLOG("close Began");
}

void PackageScene::closeMovedEvent(Ref *pSender)
{
	CCLOG("clsoe Moved");
}
void PackageScene::closeEndedEvent(Ref *pSender)
{
	CCLOG("close ended");

	Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);

	this->closeAnim();
}

void PackageScene::SortEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		CCLOG("Sort ended");
		if (m_remainArrangePac > 0.f)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("package_arrange_canNot"));
			return;
		}

		m_remainArrangePac = 15.0f;
		//与服务器通讯，获取数据,整理背包
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1308, (void *)1);
		GameUtils::playGameSound(PACKAGE_SORT, 2, false);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void PackageScene::GoldStoreEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		CCLOG("GoldStoreEvent");
		GoldStoreUI *goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
		if (goldStoreUI == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			goldStoreUI = GoldStoreUI::create();
			GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI, 0, kTagGoldStoreUI);

			goldStoreUI->setIgnoreAnchorPointForPosition(false);
			goldStoreUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			goldStoreUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void PackageScene::TitleEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void PackageScene::AchievementEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
		auto tmpBattleAchUI = (BattleAchievementUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBattleAchievementUI);
		if (NULL == tmpBattleAchUI)
		{
			Size winSize = Director::getInstance()->getVisibleSize();

			auto battleAchUI = BattleAchievementUI::create();
			battleAchUI->setIgnoreAnchorPointForPosition(false);
			battleAchUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			battleAchUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			battleAchUI->setTag(kTagBattleAchievementUI);

			GameView::getInstance()->getMainUIScene()->addChild(battleAchUI);

			// 请求服务器(请求战功日常奖励Req5101)
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5101);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void PackageScene::update(float dt)
{
	//金币
	char str_gold[20];
	sprintf(str_gold,"%d",GameView::getInstance()->getPlayerGold());
	Label_gold->setString(str_gold);
	//元宝
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	Label_goldIngot->setString(str_ingot);
	//绑定元宝
	char bingGoldIngotStr_[20];
	sprintf(bingGoldIngotStr_,"%d",GameView::getInstance()->getPlayerBindGoldIngot());
	Label_bingGoldIngot->setString(bingGoldIngotStr_);

	m_remainArrangePac -= 1.0f/60;
	if (m_remainArrangePac < 0)
		m_remainArrangePac = 0.f;
}


bool PackageScene::onTouchBegan(Touch *touch, Event * pEvent)
{
	return this->resignFirstResponder(touch,this,false);
}

void PackageScene::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void PackageScene::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void PackageScene::onTouchMoved(Touch *touch, Event * pEvent)
{
}

 void PackageScene::menuCloseCallback(Ref* pSender)
 {
	 //this->removeFromParent();

	 auto  action = Sequence::create(
		 ScaleTo::create(0.15f*1/3,1.1f),
		 ScaleTo::create(0.15f*1/2,1.0f),
		 RemoveSelf::create(),
		 NULL);

	 runAction(action);
 }

 void PackageScene::PlayerEvent(Ref* pSender)
 {
	 CCLOG("player player ");
	 //this->playerItemImage->setScale(1.1f);
	 //this->dataItemImage->setScale(1.0f);

	 this->playerItemImage->setColor(Color3B(0, 255, 0));
	 this->dataItemImage->setColor(Color3B(255, 255, 255));

	 //this->setindex(0)
 }
 void PackageScene::DateEvent(Ref* pSender)
 {
	CCLOG("data data ");
	//this->playerItemImage->setScale(1.0f);
	//this->dataItemImage->setScale(1.1f);

	this->playerItemImage->setColor(Color3B(255, 255, 255));
	this->dataItemImage->setColor(Color3B(0, 255, 0));
 }

 void PackageScene::ChangeLeftPanelByIndex(int index)
 {
	switch(index)
	{
	case 0 :
			CCLOG("00");
			playerPanel->setVisible(true);
			dataPanel->setVisible(false);
			break;   
	case 1 :
			CCLOG("01");
			playerPanel->setVisible(false);
			dataPanel->setVisible(true);
			break;
	}
 }

 void PackageScene::LeftIndexChangedEvent(Ref* pSender)
 {
	CCLOG("UIPageViewTest");
	//CCLOG("this.index = %d ",leftTab->getCurrentIndex());
	
	switch((int)leftTab->getCurrentIndex())
	{
	case 0 :
			CCLOG("00");
			playerPanel->setVisible(true);
			playerLayer->setVisible(true);
			tempLayer->setVisible(true);
			dataPanel->setVisible(false);
			infoPanle->setVisible(false);
			break;   
	case 1 :
			CCLOG("01");
			playerPanel->setVisible(false);
			playerLayer->setVisible(false);
			tempLayer->setVisible(false);
			dataPanel->setVisible(true);
			infoPanle->setVisible(false);
			ReloadProperty();
			break;
	case 2 :
			CCLOG("02");
			playerPanel->setVisible(false);
			playerLayer->setVisible(false);
			tempLayer->setVisible(false);
			dataPanel->setVisible(false);
			infoPanle->setVisible(true);
			break;
	}
 }

  void PackageScene::CurrentPageViewChanged(Ref *pSender, PageView::EventType type)
  {
	  CCLOG("currentIndex ==========  %d",(int)(pacPageView->getPageView()->getCurrentPageIndex()));

	  if (type == PageView::EventType::TURNING) {
		  auto pageView = dynamic_cast<PageView*>(pSender);
		  switch (static_cast<int>(pageView->getCurrentPageIndex()))
		  {
		  case 0:
			  currentPage->setPosition(Vec2(530, 78));
			  break;
		  case 1:
			  currentPage->setPosition(Vec2(551, 78));
			  break;
		  case 2:
			  currentPage->setPosition(Vec2(570, 78));
			  break;
		  case 3:
			  currentPage->setPosition(Vec2(589, 78));
			  break;
		  case 4:
			  currentPage->setPosition(Vec2(608, 78));
			  break;
		  }
	  }

  }

  void PackageScene::reCreatePageVeiw()
  {
	  pacPageView = GameView::getInstance()->pacPageView;
	  //pacPageView->getPageView()->addEventListenerPageView(this,SEL_PageViewEvent(PackageScene::CurrentPageViewChanged));
	  pacPageView->getPageView()->addEventListener((PageView::ccPageViewCallback)CC_CALLBACK_2(PackageScene::CurrentPageViewChanged, this));
	  u_layer->addChild(pacPageView);
	  pacPageView->setPosition(Vec2(397,99));
	  pacPageView->setCurUITag(kTagBackpack);
	  pacPageView->setVisible(true);

	  auto seq = Sequence::create(DelayTime::create(0.05f),CallFunc::create(CC_CALLBACK_0(PackageScene::PageScrollToDefault,this)),NULL);
	  pacPageView->runAction(seq);
	  //pacPageView->getPageView()->scrollToPage(0);
	  //CurrentPageViewChanged(pacPageView);

	  pacPageView->checkCDOnBegan();
  }

  void PackageScene::sdEvent( Ref *pSender )
  {
		CCLOG("sdEvent ");
  }


  void PackageScene::ReloadProperty()
  {
	  auto _player = GameView::getInstance()->myplayer->player;
	  auto activeRole = GameView::getInstance()->myplayer->getActiveRole();
	  //名字
	  l_nameValue->setString(activeRole->rolebase().name().c_str());
	  //等级
	  char s_level[20];
	  sprintf(s_level,"%d",activeRole->level());
	  l_levelValue->setString(s_level);

	  char s_strength[20];
	  sprintf(s_strength,"%d",_player->aptitude().strength());
	  l_powerValue->setString(s_strength);
	  //敏捷
	  char s_dexterity[20];
	  sprintf(s_dexterity,"%d",_player->aptitude().dexterity());
	  l_aglieValue->setString(s_dexterity);
	  //智力
	  char s_intelligence[20];
	  sprintf(s_intelligence,"%d",_player->aptitude().intelligence());
	  l_intelligenceValue->setString(s_intelligence);
	  //专注
	  char s_focus[20];
	  sprintf(s_focus,"%d",_player->aptitude().focus());
	  l_focusValue->setString(s_focus);

	  char s_hp[20];
	  sprintf(s_hp,"%d",activeRole->hp());
	  l_curHpValue->setString(s_hp);
	  char s_maxhp[20];
	  sprintf(s_maxhp,"%d",activeRole->maxhp());
	  l_allHpValue->setString(s_maxhp);

	  float blood_scale = (float)((activeRole->hp()*1.0f)/(activeRole->maxhp()*1.0f));
	  if (blood_scale > 1.0f)
		  blood_scale = 1.0f;

	  imageView_hp->setTextureRect(Rect(0,0,114*blood_scale,12));
	  imageView_hp->setScaleX(blood_scale);

	  char s_mp[20];
	  sprintf(s_mp,"%d",activeRole->mp());
	  l_curMpValue->setString(s_mp);
	  char s_maxmp[20];
	  sprintf(s_maxmp,"%d",activeRole->maxmp());
	  l_allMpValue->setString(s_maxmp);
	  float magic_scale = (float)((activeRole->mp()*1.0f)/(activeRole->maxmp()*1.0f));
	  if (magic_scale > 1.0f)
		  magic_scale = 1.0f;

	  imageView_mp->setTextureRect(Rect(0,0,114*magic_scale,12));
	  imageView_mp->setScaleX(magic_scale);

	  std::string phyAttackValue = "";
	  char s_minattack[20];
	  sprintf(s_minattack,"%d",_player->aptitude().minattack());
	  phyAttackValue.append(s_minattack);
	  phyAttackValue.append("-");
	  char s_maxattack[20];
	  sprintf(s_maxattack,"%d",_player->aptitude().maxattack());
	  phyAttackValue.append(s_maxattack);
	   l_phyAttackValue->setString(phyAttackValue.c_str());

	   std::string magicAttackValue = "";
	   char s_minmagicattack [20];
	   sprintf(s_minmagicattack,"%d",_player->aptitude().minmagicattack());
	   magicAttackValue.append(s_minmagicattack);
	   magicAttackValue.append("-");
	   char s_maxmagicattack[20];
	   sprintf(s_maxmagicattack,"%d",_player->aptitude().maxmagicattack());
	   magicAttackValue.append(s_maxmagicattack);
	   l_magicAttackValue->setString(magicAttackValue.c_str());

	   std::string phyDenValue = "";
	   char s_mindefend [20];
	   sprintf(s_mindefend,"%d",_player->aptitude().mindefend());
	   phyDenValue.append(s_mindefend);
	   phyDenValue.append("-");
	   char s_maxdefend[20];
	   sprintf(s_maxdefend,"%d",_player->aptitude().maxdefend());
	   phyDenValue.append(s_maxdefend);
	   l_phyDenValue->setString(phyDenValue.c_str());

	   std::string magicDenValue = "";
	   char s_minmagicdefend[20];
	   sprintf(s_minmagicdefend,"%d",_player->aptitude().minmagicdefend());
	   magicDenValue.append(s_minmagicdefend);
	   magicDenValue.append("-");
	   char s_maxmagicdefend[20];
	   sprintf(s_maxmagicdefend,"%d",_player->aptitude().maxmagicdefend());
	   magicDenValue.append(s_maxmagicdefend);
	   l_magicDenValue->setString(magicDenValue.c_str());

	  char s_hit[20];
	  sprintf(s_hit,"%d",_player->aptitude().hit());
	  l_hitValue->setString(s_hit); //命中
	  char s_dodge[20];
	  sprintf(s_dodge,"%d",_player->aptitude().dodge());
	  l_dodgeValue->setString(s_dodge); //闪避
	  char s_crit[20];
	  sprintf(s_crit,"%d",_player->aptitude().crit());
	  l_critValue->setString(s_crit); //暴击
	  char s_critdamage[20];
	  sprintf(s_critdamage,"%d",_player->aptitude().critdamage());
	  l_critDamageValue->setString(s_critdamage); //暴击伤害
	  char s_attackspeed[20];
	  sprintf(s_attackspeed,"%d",_player->aptitude().attackspeed());
	  l_atkSpeedValue->setString(s_attackspeed); //
	  char s_mspeed[20];
	  sprintf(s_mspeed,"%d",_player->aptitude().movespeed());
	  l_moveSpeedValue->setString(s_mspeed); //移动速度
  }

  void PackageScene::ReloadInfo()
  {
	  auto _player = GameView::getInstance()->myplayer->player;
	  auto activeRole = GameView::getInstance()->myplayer->getActiveRole();

	  l_roleNameValue->setString(activeRole->rolebase().name().c_str());

	  char s_level[20];
	  sprintf(s_level,"%d",activeRole->level());
	  l_roleLvValue->setString(s_level);

	  l_jobValue->setString(activeRole->profession().c_str());

	  if (activeRole->rolebase().gender() == 1)
	  {
		  //man
		  l_genderValue->setString(StringDataManager::getString("role_gender_nan"));
	  }
	  else
	  {
		  //woman
		  l_genderValue->setString(StringDataManager::getString("role_gender_nv"));
	  }
	 
	  l_countryValue->setString(CMapInfo::getCountryName(activeRole->playerbaseinfo().country()));
	  l_titleValue->setString(activeRole->rolebase().title().c_str());
	  l_factionValue->setString(_player->factionname().c_str());
	
	  // 荣誉
	  long long longHonorPoint = _player->honorpoint();
	  char charHonorPoint[20];
	  sprintf(charHonorPoint, "%lld", longHonorPoint);
	  l_honorValue->setString(charHonorPoint);

	  // 家族
	  l_familyValue->setString( activeRole->playerbaseinfo().factionname().c_str());

	  // 战功
	  int nBattleAchievement = _player->pkpoint();
	  char charBattleAch[20];
	  sprintf(charBattleAch, "%d", nBattleAchievement);
	  l_battleAchValue->setString(charBattleAch);

	  std::string str_physicalValue = "";
	  char s_physicalValue[20];
	  sprintf(s_physicalValue,"%d",GameView::getInstance()->myplayer->getPhysicalValue());
	  str_physicalValue.append(s_physicalValue);
	  str_physicalValue.append("/");
	  char s_physicalCapacity[20];
	  sprintf(s_physicalCapacity,"%d",GameView::getInstance()->myplayer->getPhysicalCapacity());
	  str_physicalValue.append(s_physicalCapacity);
	  l_physicalValue->setString(str_physicalValue.c_str());
	  
	  

	  std::string expValue = "";
	  char s_experience[20];
	  sprintf(s_experience,"%d",_player->experience());
	  expValue.append(s_experience);
	  expValue.append("/");
	  char s_nextlevelexperience[20];
	  sprintf(s_nextlevelexperience,"%d",_player->nextlevelexperience());
	  expValue.append(s_nextlevelexperience);
	  l_expValue->setString(expValue.c_str());

	  float scaleValue = (_player->experience()*1.0f)/(_player->nextlevelexperience()*1.0f);
	  imageView_exp->setScaleX(scaleValue);
  }

  void PackageScene::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
  {

  }

  void PackageScene::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
  {

  }

  void PackageScene::tableCellTouched( TableView* table, TableViewCell* cell )
  {
	  int i = cell->getIdx();

	  // the same one, ignore
	  if(selectTabviewIndex == i)
		 return;

	  selectTabviewIndex = i;
	  if (i == 0)
	  {
		  selectActorId = 0;
		  RefreshEquipment(GameView::getInstance()->EquipListItem);
		  RefreshRoleAnimation((CActiveRole*)GameView::getInstance()->myplayer->getActiveRole());
		  RefreshRoleInfo((CActiveRole*)GameView::getInstance()->myplayer->getActiveRole(),GameView::getInstance()->myplayer->player->fightpoint());
	  }
	  else
	  {
		  auto tempCell = dynamic_cast<GeneralsSmallHeadCell*>(cell);
		  if (tempCell != NULL)
		  {
			  selectActorId = tempCell->getCurGeneralBaseMsg()->id();
			  curGeneralBaseMsg = tempCell->getCurGeneralBaseMsg();

			  bool isExist = false;
			  for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();++i)
			  {
				  if (GameView::getInstance()->generalsInLineDetailList.at(i)->generalid() == curGeneralBaseMsg->id())
				  {
					  auto _activerole = new CActiveRole();
					  _activerole->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->activerole());
					  RefreshRoleAnimation(_activerole);
					  RefreshRoleInfo(_activerole,GameView::getInstance()->generalsInLineDetailList.at(i)->fightpoint());
					  delete _activerole;

					  std::vector<CEquipment *> temp ;
					  for (int j = 0;j<GameView::getInstance()->generalsInLineDetailList.at(i)->equipments_size();++j)
					  {
						  auto temp_equipment = new CEquipment();
						  temp_equipment->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->equipments(j));
						  temp.push_back(temp_equipment);
					  }
					  //refresh Equipment
					  RefreshEquipment(temp);
					  //delete vector
					  std::vector<CEquipment*>::iterator iter;
					  for (iter = temp.begin(); iter != temp.end(); ++iter)
					  {
						  delete *iter;
					  }
					  temp.clear();
					  isExist = true;
					  break;  
				  }
			  }
			  if(!isExist)  
			  {
				  //请求详细信息
				  GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)curGeneralBaseMsg->id());
			  }
		  }
	  }
  }

  cocos2d::Size PackageScene::tableCellSizeForIndex( TableView *table, unsigned int idx )
  {
		return Size(81,103);
  }

  cocos2d::extension::TableViewCell* PackageScene::tableCellAtIndex( TableView *table, ssize_t  idx )
  {
	  auto cell = table->dequeueCell();   // this method must be called
	  
	  if (idx == 0)
	  {
		  cell = new TableViewCell();
		  cell->autorelease();
		  auto smaillFrame = Sprite::create("res_ui/generals_white.png");
		  smaillFrame->setAnchorPoint(Vec2(0, 0));
		  smaillFrame->setPosition(Vec2(0, 0));
		  smaillFrame->setScaleX(0.75f);
		  smaillFrame->setScaleY(0.7f);
		  cell->addChild(smaillFrame);
		  //列表中的头像图标
		  auto sprite_icon = Sprite::create(BasePlayer::getBigHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str());
		  sprite_icon->setAnchorPoint(Vec2(0.5f, 0.5f));
		  sprite_icon->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), smaillFrame->getContentSize().height/2*smaillFrame->getScaleY()));
		  sprite_icon->setScale(0.75f);
		  cell->addChild(sprite_icon);
		  //
// 		  cocos2d::extension::Scale9Sprite * sprite_lvFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80.png");
// 		  sprite_lvFrame->setAnchorPoint(Vec2(1.0f, 0));
// 		  sprite_lvFrame->setContentSize(Size(34,13));
// 		  sprite_lvFrame->setPosition(Vec2(smaillFrame->getContentSize().width*smaillFrame->getScaleX()-5, 21));
// 		  cell->addChild(sprite_lvFrame);
// 		  //等级
// 		  std::string _lv = "LV";
// 		  char s_level[5];
// 		  sprintf(s_level,"%d",GameView::getInstance()->myplayer->getActiveRole()->level());
// 		  _lv.append(s_level);
// 		  Label * label_lv = Label::createWithTTF(_lv.c_str(),APP_FONT_NAME,13);
// 		  label_lv->setAnchorPoint(Vec2(1.0f, 0));
// 		  label_lv->setPosition(Vec2(smaillFrame->getContentSize().width*smaillFrame->getScaleX()-5, 19));
// 		  label_lv->enableStroke(Color3B(0, 0, 0), 2.0f);
// 		  cell->addChild(label_lv);
		  //武将名字黑底
		  auto sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/name_di3.png");
		  sprite_nameFrame->setCapInsets(Rect(10,10,1,5));
		  sprite_nameFrame->setAnchorPoint(Vec2(0.5f, 0));
		  sprite_nameFrame->setContentSize(Size(71,13));
		  sprite_nameFrame->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 8));
		  cell->addChild(sprite_nameFrame);
// 		  Sprite * sprite_nameFrame = Sprite::create("res_ui/name_di2.png");
// 		  sprite_nameFrame->setAnchorPoint(Vec2(0.5f, 0));
// 		  sprite_nameFrame->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 6));
// 		  sprite_nameFrame->setScaleX(1.1f);
// 		  cell->addChild(sprite_nameFrame);
		  //武将名字
		  const char * generalRoleName_ = StringDataManager::getString("generalList_RoleName");
		  auto label_name = Label::createWithTTF(generalRoleName_,APP_FONT_NAME,14);
		  label_name->setAnchorPoint(Vec2(0.5f, 0));
		  label_name->setPosition(Vec2(smaillFrame->getContentSize().width/2*smaillFrame->getScaleX(), 5));
		  //label_name->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		  label_name->setColor(Color3B(0,255,0));
		  cell->addChild(label_name);
		  //profession
		  std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(GameView::getInstance()->myplayer->getProfession());
		  auto sprite_profession = Sprite::create(generalProfess_.c_str());
		  sprite_profession->setAnchorPoint(Vec2(0.5f,0.5f));
		  sprite_profession->setPosition(Vec2(62 ,
			  32));
		  sprite_profession->setScale(0.6f);
		  cell->addChild(sprite_profession);
	  }
	  else
	  {
		   cell = GeneralsSmallHeadCell::create(GameView::getInstance()->generalsInLineList.at(idx-1)); 
	  }
	 
// 	  if(idx == GameView::getInstance()->generalsInLineList.size()-3)
// 	  {
// 			if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
// 			{
// 					//req for next
// 					 GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 					 temp->page = this->s_mCurPage+1;
// 					 temp->pageSize = this->everyPageNum;
// 					 temp->type = 1;
// 					 GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 					 delete temp;
// 			 }
// 	  }
	  return cell;
  }

ssize_t PackageScene::numberOfCellsInTableView( TableView *table )
  {
		 // return generalBaseMsgList.size()+1;
	   return GameView::getInstance()->generalsInLineList.size()+1;
  }


  void PackageScene::ReloadGeneralsTableView()
  {
	  Vec2 offset = this->generalList_tableView->getContentOffset();
	  this->generalList_tableView->reloadData();
  }

  void PackageScene::ReloadGeneralsTableViewWithoutChangeOffSet()
  {
	  Vec2 offset = this->generalList_tableView->getContentOffset();
	  this->generalList_tableView->reloadData();
	  this->generalList_tableView->setContentOffset(offset);
  }

  void PackageScene::registerScriptCommand( int scriptId )
  {
	  mTutorialScriptInstanceId = scriptId;
  }

  void PackageScene::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
  {
	  Size winSize = Director::getInstance()->getWinSize();
	  int _w = (winSize.width-800)/2;
	  int _h = (winSize.height - 480)/2;
// 	  CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	  tutorialIndicator->setPosition(Vec2(pos.x-60+_w,pos.y-75+_h));
// 	  tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	  this->addChild(tutorialIndicator);

	  auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,40,40);
	  tutorialIndicator->setDrawNodePos(Vec2(pos.x+_w,pos.y+_h));
	  tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	  auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	  if (mainScene)
	  {
		  mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	  }
  }

  void PackageScene::removeCCTutorialIndicator()
  {
// 	  CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
// 	  if(tutorialIndicator != NULL)
// 		  tutorialIndicator->removeFromParent();

	  auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	  if (mainScene)
	  {
		  auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		  if(teachingGuide != NULL)
			  teachingGuide->removeFromParent();
	  }
  }

  void PackageScene::RefreshGeneralLevelInTableView( long long generalId )
  {
	  for(int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	  {
		  if (generalId == GameView::getInstance()->generalsInLineList.at(i)->id())
		  {
			  auto cell = dynamic_cast<GeneralsSmallHeadCell*>(generalList_tableView->cellAtIndex(i+1));
			  if (!cell)
				  continue;

			  if (cell->getCurGeneralBaseMsg()->id() == generalId)
			  {
				  Label * l_lv = dynamic_cast<Label*>(cell->getChildByTag(265));
				  if (l_lv)
				  {
					  //武将等级
					  std::string _lv = "LV";
					  char s[5];
					  sprintf(s,"%d",GameView::getInstance()->generalsInLineList.at(i)->level());
					  _lv.append(s);
					  l_lv->setString(_lv.c_str());
				  }
			  }
		  }
	  }
  }

  void PackageScene::RemoteStoreHouseEvent(Ref *pSender, Widget::TouchEventType type)
  {

	  switch (type)
	  {
	  case Widget::TouchEventType::BEGAN:
		  break;

	  case Widget::TouchEventType::MOVED:
		  break;

	  case Widget::TouchEventType::ENDED:
	  {
		  auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		  if (!mainScene)
			  return;

		  mainScene->createRemoteStoreHouseUI();
	  }
	  break;

	  case Widget::TouchEventType::CANCELED:
		  break;

	  default:
		  break;
	  }


  }

  void PackageScene::PageScrollToDefault()
  {
	  pacPageView->getPageView()->scrollToPage(0);
	  CurrentPageViewChanged(pacPageView, PageView::EventType::TURNING);
  }


  void PackageScene::FirstGradeDesEvent(Ref *pSender, Widget::TouchEventType type)
  {

	  switch (type)
	  {
	  case Widget::TouchEventType::BEGAN:
		  break;

	  case Widget::TouchEventType::MOVED:
		  break;

	  case Widget::TouchEventType::ENDED:
	  {

		  Size winsize = Director::getInstance()->getVisibleSize();
		  int index = 9;

		  switch (GameView::getInstance()->myplayer->getProfession())
		  {
		  case PROFESSION_MJ_INDEX:
		  {
			  index = 9;
		  }
		  break;
		  case PROFESSION_GM_INDEX:
		  {
			  index = 10;
		  }
		  break;
		  case PROFESSION_HJ_INDEX:
		  {
			  index = 11;
		  }
		  break;
		  case PROFESSION_SS_INDEX:
		  {
			  index = 12;
		  }
		  break;
		  }

		  std::map<int, std::string>::const_iterator cIter;
		  cIter = SystemInfoConfigData::s_systemInfoList.find(index);
		  if (cIter == SystemInfoConfigData::s_systemInfoList.end())
		  {
			  return;
		  }
		  else
		  {
			  if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			  {
				  auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[index].c_str());
				  GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				  showSystemInfo->setIgnoreAnchorPointForPosition(false);
				  showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				  showSystemInfo->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			  }
		  }
	  }
	  break;

	  case Widget::TouchEventType::CANCELED:
		  break;

	  default:
		  break;
	  }


  }

  void PackageScene::SecondGradeDesEvent(Ref *pSender, Widget::TouchEventType type)
  {

	  switch (type)
	  {
	  case Widget::TouchEventType::BEGAN:
		  break;

	  case Widget::TouchEventType::MOVED:
		  break;

	  case Widget::TouchEventType::ENDED:
	  {
		  Size winsize = Director::getInstance()->getVisibleSize();
		  std::map<int, std::string>::const_iterator cIter;
		  cIter = SystemInfoConfigData::s_systemInfoList.find(13);
		  if (cIter == SystemInfoConfigData::s_systemInfoList.end())
		  {
			  return;
		  }
		  else
		  {
			  if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			  {
				  auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[13].c_str());
				  GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				  showSystemInfo->setIgnoreAnchorPointForPosition(false);
				  showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				  showSystemInfo->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			  }
		  }
	  }
	  break;

	  case Widget::TouchEventType::CANCELED:
		  break;

	  default:
		  break;
	  }


  }

  void PackageScene::setPresentVoidEquipItem( int part,bool isPresent )
  {
	  switch(part)
	  {
	  case 0 :
		  ImageView_toukui->setVisible(isPresent);    //toukui
		  break;
	  case 1 :
		  ImageView_toukui->setVisible(isPresent);  
		  break;
	  case 2 :
		  ImageView_yifu->setVisible(isPresent);   //yifu
		  break;
	  case 3 :
		  ImageView_peishi->setVisible(isPresent);    //yao
		  break;
	  case 4 :
		  ImageView_wuqi->setVisible(isPresent);    //wuqi
		  break;
	  case 5 :
		  ImageView_toukui->setVisible(isPresent);
		  break;
	  case 6 :
		  ImageView_toukui->setVisible(isPresent);
		  break;
	  case 7 :
		  ImageView_jiezhi->setVisible(isPresent);   //jiezhi
		  break;
	  case 8 :
		  ImageView_toukui->setVisible(isPresent);
		  break;
	  case 9 :
		  ImageView_xiezi->setVisible(isPresent);    //xie
		  break;
	  case 10 :
		  ImageView_toukui->setVisible(isPresent);
		  break;
	  }
  }

  void PackageScene::oneKeyAllEquip(Ref *pSender, Widget::TouchEventType type)
  {

	  switch (type)
	  {
	  case Widget::TouchEventType::BEGAN:
		  break;

	  case Widget::TouchEventType::MOVED:
		  break;

	  case Widget::TouchEventType::ENDED:
	  {
		  GameMessageProcessor::sharedMsgProcessor()->sendReq(1330, (void *)selectActorId);
	  }
	  break;

	  case Widget::TouchEventType::CANCELED:
		  break;

	  default:
		  break;
	  }

	  
  }

  ///////////////////////////////////////////////////////////

  GeneralsSmallHeadCell::GeneralsSmallHeadCell()
  {

  }

  GeneralsSmallHeadCell::~GeneralsSmallHeadCell()
  {

  }

  GeneralsSmallHeadCell* GeneralsSmallHeadCell::create(CGeneralBaseMsg * generalBaseMsg)
  {
	  GeneralsSmallHeadCell * generalsSmallHeadCell = new GeneralsSmallHeadCell();
	  if (generalsSmallHeadCell && generalsSmallHeadCell->init(generalBaseMsg))
	  {
		  generalsSmallHeadCell->autorelease();
		  return generalsSmallHeadCell;
	  }
	  CC_SAFE_DELETE(generalsSmallHeadCell);
	  return NULL;
  }

  bool GeneralsSmallHeadCell::init(CGeneralBaseMsg * generalBaseMsg)
  {
	  if (TableViewCell::init())
	  {
		  curGeneralBaseMsg = generalBaseMsg;
		  auto generalBaseMsgFromDB  = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];

		  auto sprite_frame = Sprite::create(GeneralsUI::getBigHeadFramePath(generalBaseMsg->currentquality()).c_str());
		  sprite_frame->setAnchorPoint(Vec2(0, 0));
		  sprite_frame->setPosition(Vec2(0, 0));
		  sprite_frame->setScaleX(0.75f);
		  sprite_frame->setScaleY(0.7f);
		  addChild(sprite_frame);

		  //列表中的头像图标
		  std::string sprite_icon_path = "res_ui/generals/";
		  sprite_icon_path.append(generalBaseMsgFromDB->get_half_photo());
		  sprite_icon_path.append(".anm");
		  auto la_head = CCLegendAnimation::create(sprite_icon_path);
		  if (la_head)
		  {
			  la_head->setPlayLoop(true);
			  la_head->setReleaseWhenStop(false);
			  la_head->setScale(0.7f*0.75f);	
			  la_head->setPosition(Vec2(4,13));
			  addChild(la_head);
		  }
// 		  //等级
// 		  cocos2d::extension::Scale9Sprite * sprite_lvFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80.png");
// 		  sprite_lvFrame->setAnchorPoint(Vec2(1.0f, 0));
// 		  sprite_lvFrame->setContentSize(Size(34,13));
// 		  sprite_lvFrame->setPosition(Vec2(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, 21));
// 		  addChild(sprite_lvFrame);
// 		  //武将等级
// 		  std::string _lv = "LV";
// 		  char s[5];
// 		  sprintf(s,"%d",generalBaseMsg->level());
// 		  _lv.append(s);
// 		  Label * label_lv = Label::createWithTTF(_lv.c_str(),APP_FONT_NAME,13);
// 		  label_lv->setAnchorPoint(Vec2(1.0f, 0));
// 		  label_lv->setPosition(Vec2(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, 19));
// 		  label_lv->enableStroke(Color3B(0, 0, 0), 2.0f);
// // 		  Color3B shadowColor = Color3B(0,0,0);   // black
// // 		  label_lv->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
// 		  label_lv->setTag(265);
// 		  addChild(label_lv);
		  //武将名字黑底
		  auto sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/name_di3.png");
		  sprite_nameFrame->setCapInsets(Rect(10,10,1,5));
		  sprite_nameFrame->setAnchorPoint(Vec2(0.5f, 0));
		  sprite_nameFrame->setContentSize(Size(72,13));
		  sprite_nameFrame->setPosition(Vec2(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 8));
		  addChild(sprite_nameFrame);
// 		  cocos2d::extension::Scale9Sprite * sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zidi2.png");
// 		  sprite_nameFrame->setCapInsets(Rect(31,8,1,1));
// 		  sprite_nameFrame->setAnchorPoint(Vec2(0.5f, 0));
// 		  sprite_nameFrame->setContentSize(Size(72,13));
// 		  sprite_nameFrame->setPosition(Vec2(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 6));
		  //addChild(sprite_nameFrame);
// 		  Sprite * sprite_nameFrame = Sprite::create("res_ui/name_di2.png");
// 		  sprite_nameFrame->setAnchorPoint(Vec2(0.5f, 0));
// 		  sprite_nameFrame->setPosition(Vec2(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 6));
// 		  sprite_nameFrame->setScaleX(1.1f);
// 		  addChild(sprite_nameFrame);
		  //武将名字
		  auto label_name = Label::createWithTTF(generalBaseMsgFromDB->name().c_str(),APP_FONT_NAME,14);
		  label_name->setAnchorPoint(Vec2(0.5f, 0));
          label_name->enableOutline(Color4B(0, 0, 0, 255), 2.0f);
		  label_name->setPosition(Vec2(sprite_frame->getContentSize().width/2*sprite_frame->getScaleX(), 5));
/*		  label_name->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);*/
		  label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		  addChild(label_name);
		  //稀有度
		  if (generalBaseMsg->rare()>0)
		  {
			  auto sprite_star = Sprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			  sprite_star->setAnchorPoint(Vec2(0,0));
			  sprite_star->setScale(0.6f);
			  sprite_star->setPosition(Vec2(5, 21));
			  addChild(sprite_star);
		  }
		  //武将军衔
		  if (generalBaseMsg->evolution() > 0)
		  {
			  auto sprite_rank = ActorUtils::createGeneralRankSprite(generalBaseMsg->evolution());
			  sprite_rank->setAnchorPoint(Vec2(1.0f,1.0f));
			  sprite_rank->setScale(0.7f);
			  sprite_rank->setPosition(Vec2(sprite_frame->getContentSize().width*sprite_frame->getScaleX()-5, sprite_frame->getContentSize().height*sprite_frame->getScaleY()-6));
			  addChild(sprite_rank);
		  }

		  if(generalBaseMsg->fightstatus() == GeneralsListUI::InBattle)
		  {
			  //出战标识
			  auto imageView_inBattle = Sprite::create("res_ui/wujiang/play.png");
			  imageView_inBattle->setAnchorPoint(Vec2(0.5,0.5));
			  imageView_inBattle->setPosition(Vec2(17,85));
			  imageView_inBattle->setScale(0.8f);
			  addChild(imageView_inBattle);
		  }
		 
		  //profession
		  std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(generalBaseMsgFromDB->get_profession());
		  auto sprite_profession = Sprite::create(generalProfess_.c_str());
		  sprite_profession->setAnchorPoint(Vec2(0.5f,0.5f));
		  sprite_profession->setPosition(Vec2(62 ,
			  32));
		  sprite_profession->setScale(0.6f);
		  addChild(sprite_profession);
		  return true;
	  }
	  return false;
  }

  CGeneralBaseMsg* GeneralsSmallHeadCell::getCurGeneralBaseMsg()
  {
	  return this->curGeneralBaseMsg;
  }














  