
#ifndef _BACKPACKSCENE_EQUIPMETNINFOS_H
#define _BACKPACKSCENE_EQUIPMETNINFOS_H

#include "../extensions/UIScene.h"
#include "GoodsItemInfoBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class PackageItemInfo;
class PackageScene;
class LoadSceneState;
class CEquipment;

class EquipmentItemInfos : public GoodsItemInfoBase
{
public:
	EquipmentItemInfos();
	~EquipmentItemInfos();
	
	static EquipmentItemInfos * create(CEquipment * equipment,long long generalId);
	bool init(CEquipment * equipment,long long generalId);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent); 
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void callBackRepair(Ref *pSender, Widget::TouchEventType type);
	void callBackPick(Ref *pSender, Widget::TouchEventType type);
	void callBackPickSure(Ref * obj);
	void getOffEvent(Ref *pSender, Widget::TouchEventType type); /*******�********/
	void lvUpEvent(Ref *pSender, Widget::TouchEventType type);   /*******��********/
	void showEquipStrength(Node * pNode,void * equip);
	int pacTabviewSelectindex;
private:
	std::string buttonImagePath;
	CEquipment * generalEquip;
	//general id 
	long long m_generalId;
	struct assistEquipStruct
	{
		int source_;
		int  pob_;
		long long petid_;
		int index_;
		int playType;
	};
};
#endif;
