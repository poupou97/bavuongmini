#include "PacPageView.h"
#include "../../../../../cocos2dx/platform/CCPlatformMacros.h"
#include "../backpackscene/PackageItem.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../messageclient/element/CDrug.h"
#include "PackageScene.h"

PacPageView::PacPageView():
pageNum(5),
itemNumEveryPage(16),
curUITag(kTagMainSceneRoot)
{
	//coordinate for packageitem
	for (int m = 3;m>=0;m--)
	{
		for (int n = 3;n>=0;n--)
		{
			int _m = 3-m;
			int _n = 3-n;

			Coordinate temp ;
			temp.x = 6+_n*85;
			temp.y = 1+m*76;

			CoordinateVector.push_back(temp);
		}
	}
	//UIPageView_Panel_Name
	for (int i = 0;i<pageNum;i++)
	{
		std::string pageViewBaseName = "pageViewPanel_";
		char num[4];
		int tmpID = i;
		sprintf(num, "0%d", tmpID);
		std::string number = num;
		pageViewBaseName.append(num);
		pageViewPanelName[i] = pageViewBaseName;
	}
	//packageItem name
	for (int i=0 ;i<itemNumEveryPage; ++i)
	{
		std::string pacBaseName = "packageItem_";
		char num[4];
		int tmpID = i;
		if(tmpID < 10)
			sprintf(num, "0%d", tmpID);
		else
			sprintf(num, "%d", tmpID);
		std::string number = num;
		pacBaseName.append(num);
		packageItem_name[i] = pacBaseName;
	}
}


PacPageView::~PacPageView()
{
}

PacPageView * PacPageView::create()
{
	auto pacPageView = new PacPageView();
	if (pacPageView && pacPageView->init())
	{
		pacPageView->autorelease();
		return pacPageView;
	}
	CC_SAFE_DELETE(pacPageView);
	return NULL;
}

bool PacPageView::init()
{
	if (UIScene::init())
	{
		// Create the page view
		m_pageView = ui::PageView::create();
		m_pageView->setTouchEnabled(true);
		m_pageView->setContentSize(Size(350, 313));
		m_pageView->setPosition(Vec2(0,0));
		m_pageView->setName("pageView");
		m_pageView->setTag(kTagPageView);
		//currentPage->setPosition(Vec2(541,58));

		int idx = 0;
		for (int i = 0; i < pageNum; ++i)
		{
			auto panel = Layout::create();
			panel->setContentSize(Size(350, 313));
			panel->setName(pageViewPanelName[i].c_str());
			panel->setTouchEnabled(true);
			if (GameView::getInstance()->AllPacItem.size()<=itemNumEveryPage*(i+1))
			{
				if (itemNumEveryPage*i < GameView::getInstance()->AllPacItem.size())
				{
					for (int m = itemNumEveryPage*i;m<GameView::getInstance()->AllPacItem.size();m++)
					{
						auto packageItem = PackageItem::create(GameView::getInstance()->AllPacItem.at(m));
						packageItem->setAnchorPoint(Vec2(0,0));
						packageItem->setPosition(Vec2(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
						packageItem->setName(packageItem_name[idx%itemNumEveryPage].c_str());
						//packageItem->setContentSize(Size(91,84));
						packageItem->setScale(1.0f);
						panel->addChild(packageItem);
						packageItem->setBoundVisible(true);
						idx++;
					}
					for (int n = GameView::getInstance()->AllPacItem.size();n<itemNumEveryPage*(i+1);n++)
					{
						//create locked 
						auto packageItem = PackageItem::createLockedItem(n);
						packageItem->setAnchorPoint(Vec2(0,0));
						packageItem->setPosition(Vec2(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
						packageItem->setName(packageItem_name[idx%itemNumEveryPage].c_str());
						//packageItem->setContentSize(Size(91,84));
						packageItem->setScale(1.0f);
						panel->addChild(packageItem);
						idx++;
					}
				}
				else
				{
					for (int n = itemNumEveryPage*i;n<itemNumEveryPage*(i+1);n++)
					{
						//create locked 
						auto packageItem = PackageItem::createLockedItem(n);
						packageItem->setAnchorPoint(Vec2(0,0));
						packageItem->setPosition(Vec2(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
						packageItem->setName(packageItem_name[idx%itemNumEveryPage].c_str());
						//packageItem->setContentSize(Size(91,84));
						panel->addChild(packageItem);
						packageItem->setScale(1.0f);
						idx++;
					}
				}
			}
			else
			{
				for (int m = itemNumEveryPage*i;m<itemNumEveryPage*(i+1);m++)
				{
					auto tempFolder = GameView::getInstance()->AllPacItem.at(m);
					int size = GameView::getInstance()->AllPacItem.size();
					int dd = ((FolderInfo*)(GameView::getInstance()->AllPacItem.at(m)))->id();
					auto packageItem = PackageItem::create(GameView::getInstance()->AllPacItem.at(m));
					packageItem->setAnchorPoint(Vec2(0,0));
					packageItem->setPosition(Vec2(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
					packageItem->setName(packageItem_name[idx%itemNumEveryPage].c_str());
					//packageItem->setContentSize(Size(91,84));
					packageItem->setScale(1.0f);
					panel->addChild(packageItem);
					packageItem->setBoundVisible(true);
					idx++;
				}
			}
			m_pageView->addPage(panel);

		}
		//m_pageView->setLocalZOrder(200);
		//m_pageView->addCurrentPageViewChangedEvent(this,coco_CurrentPageViewChanged(PackageScene::CurrentPageViewChanged));
		m_pLayer->addChild(m_pageView);
		return true;
	}
	return false;
	
}

PageView * PacPageView::getPageView()
{
	return m_pageView;
}

PackageItem * PacPageView::getPackageItem( int index )
{
	int pageIndex = index/itemNumEveryPage;
	auto _curPanel = (Layout *)m_pageView->getChildByName(pageViewPanelName[pageIndex].c_str());
	auto packageItem_ = (PackageItem*)_curPanel->getChildByName(packageItem_name[index%itemNumEveryPage].c_str());
	return packageItem_;
}

void PacPageView::ReloadOnePacItem(FolderInfo * folderInfo)
{
	int id = folderInfo->id();
	int pageIndex  = 0 ;

	if (id/this->itemNumEveryPage < 1)
	{
		pageIndex = 0;
	}
	else if (id/this->itemNumEveryPage < 2)
	{
		pageIndex = 1;
	}
	else if (id/this->itemNumEveryPage < 3)
	{
		pageIndex = 2;
	}
	else if (id/this->itemNumEveryPage < 4)
	{
		pageIndex = 3;
	}
	else if (id/this->itemNumEveryPage < 5)
	{
		pageIndex = 4;
	}

	//old data
	auto oldFolder = new FolderInfo();
	//delete old
	auto panel = (Layout*)this->getPageView()->getChildByName(this->pageViewPanelName[pageIndex].c_str());
	auto oldPacItem = (PackageItem *)panel->getChildByName(this->packageItem_name[id%this->itemNumEveryPage].c_str());
	oldFolder->CopyFrom(*oldPacItem->curFolder);
	oldPacItem->removeFromParentAndCleanup(true);

	auto packageItem = PackageItem::create(folderInfo);
	packageItem->setAnchorPoint(Vec2(0,0));
	packageItem->setPosition(Vec2(this->CoordinateVector.at(id%this->itemNumEveryPage).x, this->CoordinateVector.at(id%this->itemNumEveryPage).y));
	packageItem->setName(this->packageItem_name[id%this->itemNumEveryPage].c_str());
	packageItem->setScale(1.0f);
	panel->addChild(packageItem);
	packageItem->setBoundVisible(true);

	//refresh cd
 	if (GameView::getInstance()->getGameScene())
 	{
 		if (GameView::getInstance()->getMainUIScene())
 		{
 // 			PackageScene * packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
 // 			if (!packageScene)
 // 				return;
 
 			if (!folderInfo->has_goods())
 				return;
 
 			if (!CDrug::isDrug(folderInfo->goods().id()))
 				return;
 
 			if (!oldFolder->has_quantity())
 				return;
 
 			if (oldFolder->quantity() <= 0)
 				return;
 
 			if (oldFolder->quantity() <= folderInfo->quantity())
 				return;
 
 			GameView::getInstance()->pacPageView->RefreshCD(folderInfo->goods().id());
 		}
 	}

	delete oldFolder;
}

int PacPageView::getCurUITag()
{
	return curUITag;
}

void PacPageView::setCurUITag(int tag)
{
	curUITag = tag;
}


void PacPageView::ReloadData()
{
	for (int i = 0;i<pageNum;i++)
	{
		auto tempPanel = (Layout*)this->getPageView()->getChildByName(this->pageViewPanelName[i].c_str());
		for (int j = 0;j<itemNumEveryPage;j++)
		{
			//CCLOG("packageItem_name = %s",this->packageItem_name[j].c_str());
			auto tempItem = (PackageItem *)tempPanel->getChildByName(this->packageItem_name[j].c_str());
			if (tempItem)
				tempItem->removeFromParentAndCleanup(false);
			
			int a = j;
		}

	}
	if (this->getPageView()->getParent() != NULL)
	{
		this->getPageView()->removeAllChildrenWithCleanup(true);
	}
	reloadInit();

}

void PacPageView::reloadInit()
{
	reCreatePageVeiw();
}

void PacPageView::reCreatePageVeiw()
{
	if (GameView::getInstance()->pacPageView->getReferenceCount()>0)
	{
		GameView::getInstance()->pacPageView->release();
	}
	GameView::getInstance()->pacPageView = PacPageView::create();
	GameView::getInstance()->pacPageView->retain();
}

void PacPageView::SetCurFolderGray( bool isGray ,int index,std::string isShowGoodsNUm /*= ""*/)
{
	if (GameView::getInstance()->AllPacItem.at(index)->has_goods())
	{
		int pageIndex = index/itemNumEveryPage;
		auto _curPanel = (Layout *)m_pageView->getChildByName(pageViewPanelName[pageIndex].c_str());
		auto _curPackageItem = (PackageItem*)_curPanel->getChildByName(packageItem_name[index%itemNumEveryPage].c_str());
		_curPackageItem->setGray(isGray,isShowGoodsNUm);
	}
}


void PacPageView::RefreshCD( std::string drugId)
{
	for (int i = 0; i<GameView::getInstance()->AllPacItem.size(); ++i)
	{
		auto tempFolder = GameView::getInstance()->AllPacItem.at(i);
		if (!tempFolder->has_goods())
			continue;

		if (!CDrug::isDrug(tempFolder->goods().id()))
			continue;

		if (strcmp(tempFolder->goods().id().c_str(),drugId.c_str()) != 0)
			continue;

		auto packageItem = (PackageItem *)this->getPackageItem(tempFolder->id());
		if (packageItem)
			packageItem->BeganToRunCD();

	}
}

void PacPageView::checkCDOnBegan()
{
	for (int i = 0; i<GameView::getInstance()->AllPacItem.size(); ++i)
	{
		auto tempFolder = GameView::getInstance()->AllPacItem.at(i);
		if (!tempFolder->has_goods())
			continue;

		if (!CDrug::isDrug(tempFolder->goods().id()))
			continue;

		if (!DrugManager::getInstance()->getDrugById(tempFolder->goods().id()))
			continue;

		auto packageItem = (PackageItem *)this->getPackageItem(tempFolder->id());
		if (packageItem)
			packageItem->BeganToRunCD();
	}
}



