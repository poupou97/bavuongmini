#include "UseMissionPropUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../extensions/CCRichLabel.h"
#include "../extensions/UITab.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/element/CMissionReward.h"
#include "../missionscene/MissionRewardGoodsItem.h"
#include "cocostudio\CCSGUIReader.h"

using namespace CocosDenshion;

UseMissionPropUI::UseMissionPropUI() : 
targetNpcId(0)
{
	rewardIconName[0] = "rewardIcon_0";
	rewardIconName[1] = "rewardIcon_1";
	rewardIconName[2] = "rewardIcon_2";
	rewardIconName[3] = "rewardIcon_3";
	rewardIconName[4] = "rewardIcon_4";
	rewardLabelName[0] = "rewardLabel_0";
	rewardLabelName[1] = "rewardLabel_1";
	rewardLabelName[2] = "rewardLabel_2";
	rewardLabelName[3] = "rewardLabel_3";
	rewardLabelName[4] = "rewardLabel_4";
}


UseMissionPropUI::~UseMissionPropUI()
{
}


UseMissionPropUI* UseMissionPropUI::create(int npcId,const char * des,CMissionReward * missionReward)
{
	auto useMissionPropUI = new UseMissionPropUI();
	if (useMissionPropUI && useMissionPropUI->init(npcId,des,missionReward))
	{
		useMissionPropUI->autorelease();
		return useMissionPropUI;
	}
	CC_SAFE_DELETE(useMissionPropUI);
	return NULL;

}

bool UseMissionPropUI::init(int npcId,const char * des,CMissionReward * missionReward)
{
	if (UIScene::init())
	{
		targetNpcId = npcId;

		Size winsize = Director::getInstance()->getVisibleSize();
		//���UI
		auto ppanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/useMissionProp_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(350, 400));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(UseMissionPropUI::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);
		
		auto panel_mission = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_renwu");
		panel_mission->setVisible(true);
		Button_getMission = (Button*)Helper::seekWidgetByName(ppanel,"Button_lingqu");
		Button_getMission->setTouchEnabled(true);
		Button_getMission->setPressedActionEnabled(true);
		Button_getMission->setVisible(true);
		Button_getMission->addTouchEventListener(CC_CALLBACK_2(UseMissionPropUI::GetMissionEvent,this));
		Button_finishMission = (Button*)Helper::seekWidgetByName(ppanel,"Button_tijiao");
		Button_finishMission->setVisible(false);

		reward_di_1 = (ImageView * )Helper::seekWidgetByName(ppanel,"ImageView_di1");
		reward_di_2 = (ImageView * )Helper::seekWidgetByName(ppanel,"ImageView_di2");
		reward_di_3 = (ImageView * )Helper::seekWidgetByName(ppanel,"ImageView_di3");
		reward_di_4 = (ImageView * )Helper::seekWidgetByName(ppanel,"ImageView_di4");
		reward_di_1->setVisible(false);
		reward_di_2->setVisible(false);
		reward_di_3->setVisible(false);
		reward_di_4->setVisible(false);

		// load animation
		
		auto npcInfo = GameWorld::NpcInfos[targetNpcId];
		std::string animFileName = "res_ui/npc/";
		if (targetNpcId == 0)//�û�����NPC
		{
			animFileName.append("cz");
		}
		else
		{
			if (npcInfo)
			{
				animFileName.append(npcInfo->icon.c_str());
			}
			else
			{
				animFileName.append("cz");
			}
		}
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/npc/caiwenji.anm";
		auto m_pAnim = CCLegendAnimation::create(animFileName);
		if (m_pAnim)
		{
			m_pAnim->setPlayLoop(true);
			m_pAnim->setReleaseWhenStop(false);
			m_pAnim->setScale(1.0f);
			m_pAnim->setPosition(Vec2(33,291));
			//m_pAnim->setPlaySpeed(.35f);
			addChild(m_pAnim);
		}

		auto sprite_anmFrame = Sprite::create("res_ui/renwua/tietu2.png");
		sprite_anmFrame->setAnchorPoint(Vec2(0.5,0.5f));
		sprite_anmFrame->setPosition(Vec2(115,335));
		addChild(sprite_anmFrame);


		auto ImageView_name = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_mingzi");

		FontDefinition strokeShaodwTextDef;
		strokeShaodwTextDef._fontName = std::string("res_ui/font/simhei.ttf");
		Color3B tintColorGreen   =  { 54, 92, 7 };
		strokeShaodwTextDef._fontFillColor = tintColorGreen;   // �������ɫ
		strokeShaodwTextDef._fontSize = 22;   // �����С

		Label * l_nameLabel;
		if (targetNpcId == 0) //û�����NPC
		{
			l_nameLabel = Label::createWithTTF(StringDataManager::getString("mission_teachNpcName"),"res_ui/font/simhei.ttf",18);
		}
		else
		{
			if (npcInfo)
			{
				l_nameLabel = Label::createWithTTF(npcInfo->npcName.c_str(),"res_ui/font/simhei.ttf",18);
			}
			else
			{
				l_nameLabel = Label::createWithTTF(StringDataManager::getString("mission_teachNpcName"),"res_ui/font/simhei.ttf",18);
			}
		}
		l_nameLabel->setAnchorPoint(Vec2(0.5,0.5f));
		l_nameLabel->setPosition(Vec2(207,307));
		auto shadowColor = Color4B::BLACK;   // black
		l_nameLabel->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		addChild(l_nameLabel);

		auto lbf_reward_gold  = (Text*)Helper::seekWidgetByName(ppanel,"Label_goldValue");
		auto lbf_reward_exp  = (Text*)Helper::seekWidgetByName(ppanel,"Label_expValue");

		char s_gold[20];
		sprintf(s_gold,"%d",missionReward->gold());
		lbf_reward_gold->setString(s_gold);

		char s_exp[20];
		sprintf(s_exp,"%d",missionReward->exp());
		lbf_reward_exp->setString(s_exp);

		if (missionReward->goods_size() > 0)
		{
			for (int i =0;i<missionReward->goods_size();++i)
			{
				auto goodInfo = new GoodsInfo();
				goodInfo->CopyFrom(missionReward->goods(i).goods());
				int num = missionReward->goods(i).quantity();

				auto mrGoodsItem = MissionRewardGoodsItem::create(goodInfo,num);
				mrGoodsItem->setAnchorPoint(Vec2(0.5f,0.5f));
				mrGoodsItem->setPosition(Vec2(82+58*i,85));
				mrGoodsItem->setName(rewardIconName[i].c_str());
				mrGoodsItem->setScale(0.8f);
				m_pLayer->addChild(mrGoodsItem);

				delete goodInfo;
			}
		}

		//description
		std::string mission_description = des;
		auto label_des = CCRichLabel::createWithString(mission_description.c_str(),Size(285, 0),this,NULL,0,18,5);
		Size desSize = label_des->getContentSize();
		int height = desSize.height;
		//ScrollView_description
		auto m_contentScrollView = cocos2d::extension::ScrollView::create(Size(290,99));
		m_contentScrollView->setViewSize(Size(290, 99));
		m_contentScrollView->setIgnoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		m_contentScrollView->setAnchorPoint(Vec2(0,0));
		m_contentScrollView->setPosition(Vec2(30,185));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_pLayer->addChild(m_contentScrollView);
		if (height > 99)
		{
			m_contentScrollView->setContentSize(Size(290,height));
			m_contentScrollView->setContentOffset(Vec2(0,99-height));
		}
		else
		{
			m_contentScrollView->setContentSize(Size(290,99));
		}
		m_contentScrollView->setClippingToBounds(true);

		m_contentScrollView->addChild(label_des);
		label_des->setIgnoreAnchorPointForPosition(false);
		label_des->setAnchorPoint(Vec2(0,1));
		label_des->setPosition(Vec2(3,m_contentScrollView->getContentSize().height-3));
		

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(350,400));

		return true;
	}
	return false;

}


void UseMissionPropUI::onEnter()
{
	UIScene::onEnter();

	auto  action = Sequence::create(
		ScaleTo::create(0.15f*1/2,1.1f),
		ScaleTo::create(0.15f*1/3,1.0f),
		NULL);

	runAction(action);

}

void UseMissionPropUI::onExit()
{
	UIScene::onExit();
}


bool UseMissionPropUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	Vec2 location = touch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(7021, this);
	this->closeAnim();
	return false;
}

void UseMissionPropUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void UseMissionPropUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void UseMissionPropUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void UseMissionPropUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(7021, this);
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void UseMissionPropUI::GetMissionEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameUtils::playGameSound(MISSION_ACCEPT, 2, false);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(7022, this);
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

int UseMissionPropUI::getTargetNpcId()
{
	return targetNpcId;
}