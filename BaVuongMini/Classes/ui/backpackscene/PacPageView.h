
#ifndef _PACKAGESCENE_PACPAGEVIEW_H
#define _PACKAGESCENE_PACPAGEVIEW_H

#include "../extensions/UIScene.h"
//#include "PackageItem.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CEquipment;
class FolderInfo;
class PackageItem;
typedef enum
{
	kTagPageView = 10
};

struct Coordinate
{
	float x;
	float y;
};

class PacPageView : public UIScene
{
public:
	PacPageView();
	~PacPageView();

	static PacPageView * create();
	bool init();
	void ReloadOnePacItem(FolderInfo* folderInfo);

	int getCurUITag();
	void setCurUITag(int tag);

	void reloadInit();
	void reCreatePageVeiw();
	void ReloadData();

	//���õ�ǰ����������ɫ
	void SetCurFolderGray(bool isGray,int index,std::string isShowGoodsNUm = "");

public:
	std::vector<Coordinate> CoordinateVector;

	std::string pageViewPanelName[5];
	std::string packageItem_name[16];

	int curPackageItemIndex;
	FolderInfo * curFolder;

	int pageNum;
	int itemNumEveryPage ;

private: 
	PageView * m_pageView;
	int curUITag;
public: 
	PageView * getPageView();
	PackageItem * getPackageItem(int index);

	void RefreshCD( std::string drugId);

	//ÿ�����¸�ֵʱ�����
	void checkCDOnBegan();
};
#endif;

