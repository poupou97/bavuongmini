#include "EquipmentItemInfos.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "PackageItemInfo.h"
#include "PackageScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/CEquipment.h"
#include "../equipMent_ui/EquipMentUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CFunctionOpenLevel.h"
#include "AppMacros.h"
#include "../../messageclient/element/MapStarPickAmount.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"

EquipmentItemInfos::EquipmentItemInfos()
{
	buttonImagePath = "res_ui/new_button_1.png";
	//pacTabviewSelectindex = 0;
}


EquipmentItemInfos::~EquipmentItemInfos()
{
	delete generalEquip;
}

EquipmentItemInfos * EquipmentItemInfos::create(CEquipment * equipment,long long generalId)
{
	auto equipmentItem_info = new EquipmentItemInfos();
	if (equipmentItem_info && equipmentItem_info->init(equipment,generalId))
	{
		equipmentItem_info->autorelease();
		return equipmentItem_info;
	}
	CC_SAFE_DELETE(equipmentItem_info);
	return NULL;
}

bool EquipmentItemInfos::init(CEquipment * equipment,long long generalId)
{
	auto goods_ =new GoodsInfo();
	goods_->CopyFrom(equipment->goods());

	generalEquip = new CEquipment();
	generalEquip->CopyFrom(*equipment);
	m_generalId = generalId;

	std::vector<CEquipment *> equipVector;
	if (m_generalId != 0)
	{
		for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
		{
			if (GameView::getInstance()->generalsInLineDetailList.at(i)->generalid() == m_generalId)
			{
				for (int j=0;j<GameView::getInstance()->generalsInLineDetailList.at(i)->equipments_size() ;j++)
				{
					auto equip_ = new CEquipment();
					equip_->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->equipments(j));	
					equipVector.push_back(equip_);
				}
			}
		}
	}else
	{
		for (int i=0;i<GameView::getInstance()->EquipListItem.size();i++)
		{
			auto equip_ = new CEquipment();
			equip_->CopyFrom( *GameView::getInstance()->EquipListItem.at(i));	
			equipVector.push_back(equip_);
		}
	}
	
	
	if (GoodsItemInfoBase::init(goods_,equipVector,m_generalId))
	{
		delete goods_;
		//pick star
		auto Button_pickStar= Button::create();
		Button_pickStar->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_pickStar->setTouchEnabled(true);
		Button_pickStar->setPressedActionEnabled(true);
		Button_pickStar->addTouchEventListener(CC_CALLBACK_2(EquipmentItemInfos::callBackPick, this));
		Button_pickStar->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_pickStar->setScale9Enabled(true);
		Button_pickStar->setContentSize(Size(80,43));
		Button_pickStar->setCapInsets(Rect(18,9,2,23));
		//Button_pickStar->setPosition(Vec2(141,25));

		auto Label_pick = Label::createWithTTF((const char *)"摘星", APP_FONT_NAME, 18);
		Label_pick->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_pick->setPosition(Vec2(0,0));
		Button_pickStar->addChild(Label_pick);
		m_pLayer->addChild(Button_pickStar);
		//repair equip
		auto Button_repair= Button::create();
		Button_repair->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_repair->setTouchEnabled(true);
		Button_repair->setPressedActionEnabled(true);
		Button_repair->addTouchEventListener(CC_CALLBACK_2(EquipmentItemInfos::callBackRepair, this));
		Button_repair->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_repair->setScale9Enabled(true);
		Button_repair->setContentSize(Size(80,43));
		Button_repair->setCapInsets(Rect(18,9,2,23));
		Button_repair->setPosition(Vec2(141,25));
		m_pLayer->addChild(Button_repair);

		auto Label_repair = Label::createWithTTF((const char *)"修理", APP_FONT_NAME, 18);
		Label_repair->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_repair->setPosition(Vec2(0,0));
		Button_repair->addChild(Label_repair);
		//卸下按钮
		auto Button_getOff= Button::create();
		Button_getOff->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_getOff->setTouchEnabled(true);
		Button_getOff->setPressedActionEnabled(true);
		Button_getOff->addTouchEventListener(CC_CALLBACK_2(EquipmentItemInfos::getOffEvent, this));
		Button_getOff->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_getOff->setScale9Enabled(true);
		Button_getOff->setContentSize(Size(80,43));
		Button_getOff->setCapInsets(Rect(18,9,2,23));
		//Button_getOff->setPosition(Vec2(58,25));
		Button_getOff->setPosition(Vec2(55,25));

		auto Label_getOff = Label::createWithTTF((const char *)"卸下", APP_FONT_NAME, 18);
		Label_getOff->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_getOff->setPosition(Vec2(0,0));
		Button_getOff->addChild(Label_getOff);
		m_pLayer->addChild(Button_getOff);
		//强化按钮
		auto Button_LVUp= Button::create();
		Button_LVUp->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		Button_LVUp->setTouchEnabled(true);
		Button_LVUp->setPressedActionEnabled(true);
		Button_LVUp->addTouchEventListener(CC_CALLBACK_2(EquipmentItemInfos::lvUpEvent, this));
		Button_LVUp->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_LVUp->setScale9Enabled(true);
		Button_LVUp->setContentSize(Size(80,43));
		Button_LVUp->setCapInsets(Rect(18,9,2,23));
		Button_LVUp->setPosition(Vec2(224,25));

		auto Label_LVUp = Label::createWithTTF((const char *)"强化", APP_FONT_NAME, 18);
		Label_LVUp->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_LVUp->setPosition(Vec2(0,0));
		Button_LVUp->addChild(Label_LVUp);
		m_pLayer->addChild(Button_LVUp);

		//强化按等级开放
		int roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
		int openLevel = 0;
		for (int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
		{
			if (strcmp("btn_strengthen",FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str()) == 0)
			{
				openLevel = FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel();
				break;
			}
		}

		/*
		if (openLevel <= roleLevel)     //可以显示
		{
			Button_LVUp->setVisible(true);
			Button_getOff->setPosition(Vec2(58,25));
		}
		else
		{
			Button_LVUp->setVisible(false);
			Button_getOff->setPosition(Vec2(141,25));
		}
		*/

		int btn_num = 1;
		if (equipment->goods().equipmentdetail().starlevel() > 0)
		{
			btn_num++;
		}

		if (equipment->goods().equipmentdetail().durable() <= 10)
		{
			btn_num++;
		}

		if (openLevel <= roleLevel)
		{
			btn_num++;
		}

		switch(btn_num)
		{
		case 1:
			{
				Button_pickStar->setVisible(false);
				Button_repair->setVisible(false);
				Button_LVUp->setVisible(false);
				Button_getOff->setVisible(true);
				Button_getOff->setPosition(Vec2(141,25));
			}break;
		case 2:
			{
				Button_pickStar->setVisible(false);
				Button_repair->setVisible(false);
				Button_LVUp->setVisible(false);
				Button_getOff->setVisible(true);
				Button_getOff->setPosition(Vec2(58,25));

				if (equipment->goods().equipmentdetail().starlevel() > 0)
				{
					Button_pickStar->setVisible(true);
					Button_pickStar->setPosition(Vec2(224,25));
				}

				if (equipment->goods().equipmentdetail().durable() <= 10)
				{
					Button_repair->setVisible(true);
					Button_repair->setPosition(Vec2(224,25));
				}

				if (openLevel <= roleLevel)
				{
					Button_LVUp->setVisible(true);
					Button_LVUp->setPosition(Vec2(224,25));
				}
			}break;
		case 3:
			{
				Button_pickStar->setVisible(false);
				Button_repair->setVisible(false);
				Button_LVUp->setVisible(false);
				Button_getOff->setVisible(true);
				Button_getOff->setPosition(Vec2(58,25));

				bool temp_ = false;
				if (equipment->goods().equipmentdetail().starlevel() > 0)
				{
					Button_pickStar->setVisible(true);
					Button_pickStar->setPosition(Vec2(141,25));
					temp_ = true;
				}

				if (equipment->goods().equipmentdetail().durable() <= 10)
				{
					Button_repair->setVisible(true);
					if (temp_ == true)
					{
						Button_repair->setPosition(Vec2(224,25));
					}else
					{
						Button_repair->setPosition(Vec2(141,25));
					}
				}

				if (openLevel <= roleLevel)
				{
					Button_LVUp->setVisible(true);
					Button_LVUp->setPosition(Vec2(224,25));
				}
			}break;
		case 4:
			{
				scrollView_info->setContentSize(Size(548,260));
				scrollView_info->setPosition(Vec2(0,97));

				Button_getOff->setPosition(Vec2(80,70));
				Button_repair->setPosition(Vec2(200,70));
				Button_LVUp->setPosition(Vec2(80,25));
				Button_pickStar->setPosition(Vec2(200,25));
			}break;
		}

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setAnchorPoint(Vec2(0,0));
		//this->setContentSize(Size(background->getContentSize().width,background->getContentSize().height));


		std::vector<CEquipment *>::iterator iter;
		for (iter = equipVector.begin(); iter!= equipVector.end();iter++)
		{
			delete *iter;
		}
		equipVector.clear();

		return true;
	}
	return false;
}

void EquipmentItemInfos::onEnter()
{
	//GoodsItemInfoBase::onEnter();
	UIScene::onEnter();
	this->openAnim();
}
void EquipmentItemInfos::onExit()
{
	UIScene::onExit();
}

bool EquipmentItemInfos::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return this->resignFirstResponder(pTouch,this,false);
}
void EquipmentItemInfos::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void EquipmentItemInfos::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void EquipmentItemInfos::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void EquipmentItemInfos::lvUpEvent(Ref *pSender, Widget::TouchEventType type)
{
	CCLOG("lvUpEvent");
	//关闭详细信息界面
	//auto equipment = new CEquipment();
	//equipment->CopyFrom(*generalEquip);


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->setVisible(false);
		auto packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
		//pacTabviewSelectindex  = packageScene->selectTabviewIndex;
		if (packageScene != NULL)
		{
			packageScene->closeEndedEvent(NULL);
		}


		auto action = (ActionInterval *)Sequence::create(
			DelayTime::create(0.5f),
			CallFuncN::create(CC_CALLBACK_1(EquipmentItemInfos::showEquipStrength, this, (void *)generalEquip)),
			RemoveSelf::create(),
			NULL);
		runAction(action);
		//delete equipment;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	

}

void EquipmentItemInfos::getOffEvent(Ref *pSender, Widget::TouchEventType type)
{
	CCLOG("EquipmentInfo::getOffEvent");
	

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
		if (packageScene->selectActorId != m_generalId)
		{
			int tmpe_selectIndex = 0;
			int generalNum = GameView::getInstance()->generalsInLineList.size();
			for (int i = 0; i<generalNum; i++)
			{
				if (m_generalId == GameView::getInstance()->generalsInLineList.at(i)->id())
				{
					tmpe_selectIndex = i + 1;
				}
			}
			packageScene->generalList_tableView->cellAtIndex(tmpe_selectIndex);
		}
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1307, (void *)generalEquip->part(), (void *)m_generalId);
		GameUtils::playGameSound(EQUIP_UNLOAD, 2, false);
		//关闭详细信息界面
		this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


	
}

void EquipmentItemInfos::callBackPick(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
		packageScene->selectGeneralEquipPart = generalEquip->part();
		packageScene->selectGeneralEquipStarLevel = generalEquip->goods().equipmentdetail().starlevel();
		this->removeFromParentAndCleanup(true);

		const char *strBegin = StringDataManager::getString("packBackGoodsPickSureBegin");
		const char *strEnd = StringDataManager::getString("packBackGoodsPickSureEnd");

		auto equipStarLevel = StrengthStarConfigData::s_EquipStarAmount[packageScene->selectGeneralEquipStarLevel];
		int starCount_ = equipStarLevel->get_count();
		char str[20];
		sprintf(str, "%d", starCount_);
		std::string string_ = strBegin;
		string_.append(str);
		string_.append(strEnd);
		GameView::getInstance()->showPopupWindow(string_, 2, this, CC_CALLFUNCO_SELECTOR(EquipmentItemInfos::callBackPickSure), NULL);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void EquipmentItemInfos::callBackPickSure( Ref * obj )
{
	auto packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	assistEquipStruct assists_={1,packageScene->selectGeneralEquipPart,packageScene->selectActorId,0,2};
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1601,&assists_);

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1608,this);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1699,this);
}

void EquipmentItemInfos::showEquipStrength(Node * pNode,void * equip)
{
	auto equipment = (CEquipment *)equip;
	Size winSize = Director::getInstance()->getVisibleSize();
	if(GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI) == NULL)
	{
		auto equipui=EquipMentUi::create();
		equipui->setIgnoreAnchorPointForPosition(false);
		equipui->setAnchorPoint(Vec2(0.5f,0.5f));
		equipui->setPosition(Vec2(winSize.width/2,winSize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(equipui,0,ktagEquipMentUI);
		equipui->refreshBackPack(0,-1);

		equipui->generalList->generalList_tableView->cellAtIndex(pacTabviewSelectindex);
 		equipui->generalMainId = m_generalId;
		equipui->addRoleEquipment(equipment);
	}
}

void EquipmentItemInfos::callBackRepair(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5211);
		this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}




