#include "PackageItemInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../Mail_ui/MailUI.h"
#include "../Auction_ui/AuctionUi.h"
#include "../Chat_ui/BackPackage.h"
#include "../Chat_ui/ChatUI.h"
#include "PackageScene.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../equipMent_ui/EquipMentUi.h"
#include "../storehouse_ui/StoreHouseUI.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutConfigure.h"
#include "../../gamescene_state/MainScene.h"
#include "../extensions/Counter.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CDrug.h"
#include "GeneralsListForTakeDrug.h"
#include "../../messageclient/element/CFunctionOpenLevel.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "AppMacros.h"
#include "PacPageView.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../messageclient/element/MapStarPickAmount.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"
#include "../shop_ui/ShopUI.h"
#include "../../ui/Chat_ui/PrivateChatUi.h"
#include "../../ui/extensions/UITab.h"
#include "../../legend_script/CCTeachingGuide.h"

// optional int32 additionProperty = 8;
// /** 是否可装备到快捷栏 */
// equip_shortcuts,
// 	/** 是否可直接使用*/
// 	direct_use,	
// 	/** 使用后消失*/
// 	used_dissolve,
// 	/** 可强化*/
// 	strengthen,
// 	/** 可镶嵌*/
// 	inlay,
// 
// 	/** 丢弃后显示 */
// 	discard_show,
// 	/** 丢弃后消失 */
// 	discard_dissolve,
// 
// 	/** 可交易*/
// 	tradeable,
// 	/** 使用前可交易*/
// 	before_use_tradeable,
// 
// 	/** 掉落*/	
// 	dropable,
// 	/** 百分百掉落*/
// 	absolute_dropable,
// 
// 	/** 是否可拍卖*/
// 	auction,
// 	/** 可出售*/
// 	saleable,	
// 	/** 可存入仓库*/
// 	storage,
// 	/** 可穿着*/
// 	wearable,
// 	/** 可洗炼*/
// 	baptize,
// 	/** 可继承*/
// 	inherit,
// 	/** 可做为镶嵌材料*/
// 	inlay_meterial,
// 	/** 可一次使用多个*/
// 	mutil_use
// 	;
// 
// 
// public int intValue() {
// 	switch(this) {
// 	case equip_shortcuts:
// 		return 0X00000001;
// 	case direct_use:	
// 		return 0X00000002;
// 	case used_dissolve:
// 		return 0X00000004;
// 	case strengthen:
// 		return 0X00000008;
// 	case inlay:
// 		return 0X00000010;
// 	case discard_show:
// 		return 0X00000020;
// 	case discard_dissolve:
// 		return 0X00000040;
// 	case tradeable:
// 		return 0X00000080;
// 	case before_use_tradeable:
// 		return 0X00000100;
// 	case dropable:
// 		return 0X00000200;
// 	case absolute_dropable:
// 		return 0X00000400;
// 	case auction:
// 		return 0X00000800;
// 	case saleable:	
// 		return 0X00001000;
// 	case storage:
// 		return 0X00002000;
// 	case wearable:
// 		return 0X00004000;
// 	case baptize:
// 		return 0X00008000;
// 	case inherit:
// 		return 0X00010000;
// 	case inlay_meterial:
// 		return 0X00020000;
// 	case mutil_use:
// 		return 0X00040000;



PackageItemInfo::PackageItemInfo():
deleteAmount(0)
{
	buttonImagePath = "res_ui/new_button_1.png";
	//this->setLocalZOrder(5000);
}

PackageItemInfo::~PackageItemInfo()
{
	delete curGoods;
	delete curFolders;
}

PackageItemInfo * PackageItemInfo::create(FolderInfo * folder,std::vector<CEquipment *>equipmentVector,long long generalId)
{
	auto packageItem_info = new PackageItemInfo();
	if (packageItem_info && packageItem_info->init(folder,equipmentVector,generalId))
	{
		packageItem_info->autorelease();
		return packageItem_info;
	}
	CC_SAFE_DELETE(packageItem_info);
	return NULL;
}

bool PackageItemInfo::init(FolderInfo * folder,std::vector<CEquipment *>equipmentVector,long long generalId)
{
	curGoods =new GoodsInfo();
	curGoods->CopyFrom(folder->goods());

	curFolders =new FolderInfo();
	curFolders->CopyFrom(*folder);


	if (GoodsItemInfoBase::init(curGoods,equipmentVector,generalId))
	{
		switch(GameView::getInstance()->pacPageView->getCurUITag())
		{
		case kTagBackpack :
			{	
				if (folder->goods().equipmentclazz() == 0)
				{
					if (folder->goods().canshortcut()  == 1)
					{
						auto Button_delete= Button::create();
						Button_delete->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_delete->setTouchEnabled(true);
						Button_delete->setPressedActionEnabled(true);
						Button_delete->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::deleteEvent,this));
						Button_delete->setAnchorPoint(Vec2(0.5f,0.5f));
						Button_delete->setScale9Enabled(true);
						Button_delete->setContentSize(Size(80,43));
						Button_delete->setCapInsets(Rect(18,9,2,23));
						Button_delete->setPosition(Vec2(55,25));

						auto Label_delete = Label::createWithTTF((const char *)"丢弃", APP_FONT_NAME, 16);
						Label_delete->setAnchorPoint(Vec2(0.5f,0.5f));
						Label_delete->setPosition(Vec2(0,0));
						Button_delete->addChild(Label_delete);
						//normalInfoPanel->addChild(Button_delete);
						m_pLayer->addChild(Button_delete);

						auto Button_shortCut= Button::create();
						Button_shortCut->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_shortCut->setTouchEnabled(true);
						Button_shortCut->setPressedActionEnabled(true);
						Button_shortCut->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::shortCutEvent,this));
						Button_shortCut->setAnchorPoint(Vec2(0.5f,0.5f));
						Button_shortCut->setScale9Enabled(true);
						Button_shortCut->setContentSize(Size(80,43));
						Button_shortCut->setCapInsets(Rect(18,9,2,23));
						Button_shortCut->setPosition(Vec2(141,25));

						auto Label_shortCut = Label::createWithTTF((const char *)"快捷", APP_FONT_NAME, 16);
						Label_shortCut->setAnchorPoint(Vec2(0.5f,0.5f));
						Label_shortCut->setPosition(Vec2(0,0));
						Button_shortCut->addChild(Label_shortCut);
						//normalInfoPanel->addChild(Button_delete);
						m_pLayer->addChild(Button_shortCut);


						auto Button_use= Button::create();
						Button_use->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_use->setTouchEnabled(true);
						Button_use->setPressedActionEnabled(true);
						Button_use->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::useEvent, this));
						Button_use->setAnchorPoint(Vec2(0.5f,0.5f));
						Button_use->setScale9Enabled(true);
						Button_use->setContentSize(Size(80,43));
						Button_use->setCapInsets(Rect(18,9,2,23));
						Button_use->setPosition(Vec2(225,25));
						//使用按钮

						auto Label_use = Label::createWithTTF((const char *)"使用", APP_FONT_NAME, 16);
						Label_use->setAnchorPoint(Vec2(0.5f,0.5f));
						Label_use->setPosition(Vec2(0,0));
						Button_use->addChild(Label_use);
						//normalInfoPanel->addChild(Button_delete);
						m_pLayer->addChild(Button_use);

						//合成按钮
						auto Button_systhesis= Button::create();
						Button_systhesis->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_systhesis->setTouchEnabled(true);
						Button_systhesis->setPressedActionEnabled(true);
						Button_systhesis->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::systhesisEvent, this));
						Button_systhesis->setAnchorPoint(Vec2(0.5f,0.5f));
						Button_systhesis->setScale9Enabled(true);
						Button_systhesis->setContentSize(Size(80,43));
						Button_systhesis->setCapInsets(Rect(18,9,2,23));
						Button_systhesis->setPosition(Vec2(225,25));

						auto Label_systhesis = Label::createWithTTF((const char *)"合成", APP_FONT_NAME, 16);
						Label_systhesis->setAnchorPoint(Vec2(0.5f,0.5f));
						Label_systhesis->setPosition(Vec2(0,0));
						Button_systhesis->addChild(Label_systhesis);
						m_pLayer->addChild(Button_systhesis);

						if (((folder->goods().additionproperty() & 0X00000002)  !=0)||((folder->goods().additionproperty() & 0X00040000)  !=0) )//可以直接使用
						{
							if(!folder->goods().mergeable())  //合成不显示(只有三个)
							{
								Button_systhesis->setVisible(false);
								Button_delete->setPosition(Vec2(57,25));
								Button_systhesis->setPosition(Vec2(141,25));
								Button_use->setPosition(Vec2(225,25));
							}
							else   //四个都显示
							{
								scrollView_info->setContentSize(Size(548,260));
								scrollView_info->setPosition(Vec2(0,97));
								Button_use->setPosition(Vec2(80,70));
								Button_shortCut->setPosition(Vec2(200,70));
								Button_delete->setPosition(Vec2(80,25));
								Button_systhesis->setPosition(Vec2(200,25));
							}
						}
						else
						{
							Button_use->setVisible(false);

							if(!folder->goods().mergeable())  //合成不显示(只有两个)
							{
								Button_systhesis->setVisible(false);
								Button_delete->setPosition(Vec2(80,25));
								Button_shortCut->setPosition(Vec2(200,25));
							}
							else   //只有使用不显示(有三个)
							{
								Button_delete->setPosition(Vec2(57,25));
								Button_shortCut->setPosition(Vec2(141,25));
								Button_systhesis->setPosition(Vec2(225,25));
							}
						}
					}
					else
					{
						//使用按钮
						auto Button_use= Button::create();
						Button_use->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_use->setTouchEnabled(true);
						Button_use->setPressedActionEnabled(true);
						Button_use->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::useEvent, this));
						Button_use->setAnchorPoint(Vec2(0.5f,0.5f));
						Button_use->setScale9Enabled(true);
						Button_use->setContentSize(Size(91,43));
						Button_use->setCapInsets(Rect(18,9,2,23));
						Button_use->setPosition(Vec2(55,25));
						
						auto Label_use = Label::createWithTTF((const char *)"使用", APP_FONT_NAME, 16);
						Label_use->setAnchorPoint(Vec2(0.5f,0.5f));
						Label_use->setPosition(Vec2(0,0));
						Button_use->addChild(Label_use);
						//normalInfoPanel->addChild(Button_delete);
						m_pLayer->addChild(Button_use);

						//丢弃按钮
						auto Button_delete= Button::create();
						Button_delete->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_delete->setTouchEnabled(true);
						Button_delete->setPressedActionEnabled(true);
						Button_delete->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::deleteEvent, this));
						Button_delete->setAnchorPoint(Vec2(0.5f,0.5f));
						Button_delete->setScale9Enabled(true);
						Button_delete->setContentSize(Size(91,43));
						Button_delete->setCapInsets(Rect(18,9,2,23));
						Button_delete->setPosition(Vec2(141,25));
 
						auto Label_delete = Label::createWithTTF((const char *)"丢弃", APP_FONT_NAME, 16);
						Label_delete->setAnchorPoint(Vec2(0.5f,0.5f));
						Label_delete->setPosition(Vec2(0,0));
						Button_delete->addChild(Label_delete);
						//normalInfoPanel->addChild(Button_delete);
						m_pLayer->addChild(Button_delete);

						//合成按钮
						auto Button_systhesis= Button::create();
						Button_systhesis->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
						Button_systhesis->setTouchEnabled(true);
						Button_systhesis->setPressedActionEnabled(true);
						Button_systhesis->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::systhesisEvent, this));
						Button_systhesis->setAnchorPoint(Vec2(0.5f,0.5f));
						Button_systhesis->setScale9Enabled(true);
						Button_systhesis->setContentSize(Size(91,43));
						Button_systhesis->setCapInsets(Rect(18,9,2,23));
						Button_systhesis->setPosition(Vec2(225,25));

						auto Label_systhesis = Label::createWithTTF((const char *)"合成", APP_FONT_NAME, 16);
						Label_systhesis->setAnchorPoint(Vec2(0.5f,0.5f));
						Label_systhesis->setPosition(Vec2(0,0));
						Button_systhesis->addChild(Label_systhesis);
						m_pLayer->addChild(Button_systhesis);

						if (((folder->goods().additionproperty() & 0X00000002)  !=0)||((folder->goods().additionproperty() & 0X00040000)  !=0)  )//可以直接使用
						{
							if(!folder->goods().mergeable())
							{
								Button_systhesis->setVisible(false);
								Button_use->setPosition(Vec2(200,25));
								Button_delete->setPosition(Vec2(80,25));
							}
						}
						else
						{
							Button_use->setVisible(false);
							if (!folder->goods().mergeable())
							{
								Button_systhesis->setVisible(false);
								Button_delete->setPosition(Vec2(141,25));
							}
							else
							{
								Button_delete->setPosition(Vec2(80,25));
								Button_systhesis->setPosition(Vec2(200,25));
							}
							
						}
					}
					
				}
				else if (folder->goods().equipmentclazz()>0 && folder->goods().equipmentclazz()<10)
				{
					auto Button_pickStar= Button::create();
					Button_pickStar->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_pickStar->setTouchEnabled(true);
					Button_pickStar->setPressedActionEnabled(true);
					Button_pickStar->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::callBackPick, this));
					Button_pickStar->setAnchorPoint(Vec2(0.5f,0.5f));
					Button_pickStar->setScale9Enabled(true);
					Button_pickStar->setContentSize(Size(80,43));
					Button_pickStar->setCapInsets(Rect(18,9,2,23));
					Button_pickStar->setPosition(Vec2(141,25));

					auto Label_pick = Label::createWithTTF((const char *)"摘星", APP_FONT_NAME, 18);
					Label_pick->setAnchorPoint(Vec2(0.5f,0.5f));
					Label_pick->setPosition(Vec2(0,0));
					Button_pickStar->addChild(Label_pick);
					m_pLayer->addChild(Button_pickStar);


					auto Button_delete= Button::create();
					Button_delete->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_delete->setTouchEnabled(true);
					Button_delete->setPressedActionEnabled(true);
					Button_delete->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::deleteEvent, this));
					Button_delete->setAnchorPoint(Vec2(0.5f,0.5f));
					Button_delete->setScale9Enabled(true);
					Button_delete->setContentSize(Size(80,43));
					Button_delete->setCapInsets(Rect(18,9,2,23));
					Button_delete->setPosition(Vec2(55,25));
					//丢弃按钮
				
					auto Label_delete = Label::createWithTTF((const char *)"丢弃", APP_FONT_NAME, 16);
					Label_delete->setAnchorPoint(Vec2(0.5f,0.5f));
					Label_delete->setPosition(Vec2(0,0));
					Button_delete->addChild(Label_delete);
					m_pLayer->addChild(Button_delete);
					//强化按钮
					
					auto Button_LVUp= Button::create();
					Button_LVUp->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_LVUp->setTouchEnabled(true);
					Button_LVUp->setPressedActionEnabled(true);
					Button_LVUp->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::lvUpEvent, this));
					Button_LVUp->setAnchorPoint(Vec2(0.5f,0.5f));
					Button_LVUp->setScale9Enabled(true);
					Button_LVUp->setContentSize(Size(80,43));
					Button_LVUp->setCapInsets(Rect(18,9,2,23));
					Button_LVUp->setPosition(Vec2(141,25));

					auto Label_LVUp = Label::createWithTTF((const char *)"强化", APP_FONT_NAME, 16);
					Label_LVUp->setAnchorPoint(Vec2(0.5f,0.5f));
					Label_LVUp->setPosition(Vec2(0,0));
					Button_LVUp->addChild(Label_LVUp);
					m_pLayer->addChild(Button_LVUp);
					
					//装备按钮
					Button_dressOn= Button::create();
					Button_dressOn->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_dressOn->setTouchEnabled(true);
					Button_dressOn->setPressedActionEnabled(true);
					Button_dressOn->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::dressOnEvent, this));
					Button_dressOn->setAnchorPoint(Vec2(0.5f,0.5f));
					Button_dressOn->setScale9Enabled(true);
					Button_dressOn->setContentSize(Size(80,43));
					Button_dressOn->setCapInsets(Rect(18,9,2,23));
					Button_dressOn->setTag(112233);
					Button_dressOn->setPosition(Vec2(224,25));

					auto Label_dressOn = Label::createWithTTF((const char *)"装备", APP_FONT_NAME, 16);
					Label_dressOn->setAnchorPoint(Vec2(0.5f,0.5f));
					Label_dressOn->setPosition(Vec2(0,0));
					Button_dressOn->addChild(Label_dressOn);
					m_pLayer->addChild(Button_dressOn);
					//强化按等级开放
					int roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
					int openLevel = 0;
					for (int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
					{
						if (strcmp("btn_strengthen",FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str()) == 0)
						{
							openLevel = FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel();
							break;
						}
					}
					if (openLevel <= roleLevel)     //可以显示
					{
						Button_LVUp->setVisible(true);
						Button_pickStar->setVisible(true);
						Button_delete->setPosition(Vec2(58,25));
						Button_dressOn->setPosition(Vec2(224,25));
					}
					else
					{
						Button_LVUp->setVisible(false);
						Button_pickStar->setVisible(false);
						Button_delete->setPosition(Vec2(80,25));
						Button_dressOn->setPosition(Vec2(200,25));
					}
					Button_pickStar->setVisible(false);

					if (folder->goods().equipmentdetail().starlevel() > 0)
					{
						scrollView_info->setContentSize(Size(548,260));
						scrollView_info->setPosition(Vec2(0,97));
						Button_pickStar->setVisible(true);
						Button_pickStar->setPosition(Vec2(58,70));
						Button_LVUp->setPosition(Vec2(224,70));
					}
				}
				break;
			}
			case kTabChat :
				{
					const char *strings_synthesize = StringDataManager::getString("goods_chat_btnAdd");
					char *str_synthesize=const_cast<char*>(strings_synthesize);

					auto Button_getAnnes= Button::create();
					Button_getAnnes->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_getAnnes->setTouchEnabled(true);
					Button_getAnnes->setPressedActionEnabled(true);
					Button_getAnnes->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::showGoodsInfo, this));
					Button_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
					Button_getAnnes->setScale9Enabled(true);
					Button_getAnnes->setContentSize(Size(110,43));
					Button_getAnnes->setCapInsets(Rect(18,9,2,23));
					Button_getAnnes->setPosition(Vec2(141,25));

					auto Label_getAnnes = Label::createWithTTF(str_synthesize, APP_FONT_NAME, 16);
					Label_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
					Label_getAnnes->setPosition(Vec2(0,0));
					Button_getAnnes->addChild(Label_getAnnes);
					m_pLayer->addChild(Button_getAnnes);
					break;
				}
			case ktagPrivateUI :
				{
					const char *strings_synthesize = StringDataManager::getString("goods_chat_btnAdd");
					char *str_synthesize=const_cast<char*>(strings_synthesize);

					auto Button_getAnnes= Button::create();
					Button_getAnnes->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_getAnnes->setTouchEnabled(true);
					Button_getAnnes->setPressedActionEnabled(true);
					Button_getAnnes->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::showGoodsInfoPrivateui, this));
					Button_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
					Button_getAnnes->setScale9Enabled(true);
					Button_getAnnes->setContentSize(Size(110,43));
					Button_getAnnes->setCapInsets(Rect(18,9,2,23));
					Button_getAnnes->setPosition(Vec2(141,25));

					auto Label_getAnnes = Label::createWithTTF(str_synthesize, APP_FONT_NAME, 16);
					Label_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
					Label_getAnnes->setPosition(Vec2(0,0));
					Button_getAnnes->addChild(Label_getAnnes);
					m_pLayer->addChild(Button_getAnnes);
					break;
				}
			case kTagMailUi :
				{
					const char *strings_synthesize = StringDataManager::getString("goods_mail_btnAdd");
					char *str_synthesize=const_cast<char*>(strings_synthesize);

					auto Button_getAnnes= Button::create();
					Button_getAnnes->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_getAnnes->setTouchEnabled(true);
					Button_getAnnes->setPressedActionEnabled(true);
					Button_getAnnes->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::GetAnnesEvent, this));
					Button_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
					Button_getAnnes->setScale9Enabled(true);
					Button_getAnnes->setContentSize(Size(110,43));
					Button_getAnnes->setCapInsets(Rect(18,9,2,23));
					Button_getAnnes->setPosition(Vec2(141,25));

					auto Label_getAnnes = Label::createWithTTF(str_synthesize, APP_FONT_NAME, 16);
					Label_getAnnes->setAnchorPoint(Vec2(0.5f,0.5f));
					Label_getAnnes->setPosition(Vec2(0,0));
					Button_getAnnes->addChild(Label_getAnnes);
					m_pLayer->addChild(Button_getAnnes);

					break;
				}
			case kTagAuction:
				{
					const char *strings_consign = StringDataManager::getString("goods_auction_consion");
					char *str_consign=const_cast<char*>(strings_consign);

					const char *strings_search = StringDataManager::getString("goods_auction_search");
					char *str_search=const_cast<char*>(strings_search);

					auto Button_consign= Button::create();
					Button_consign->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_consign->setTouchEnabled(true);
					Button_consign->setPressedActionEnabled(true);
					Button_consign->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::callBackConsign, this));
					Button_consign->setAnchorPoint(Vec2(0.5f,0.5f));
					Button_consign->setScale9Enabled(true);
					Button_consign->setContentSize(Size(91,43));
					Button_consign->setCapInsets(Rect(18,9,2,23));
					Button_consign->setPosition(Vec2(80,25));

					auto Label_consign = Label::createWithTTF(str_consign, APP_FONT_NAME, 16);
					Label_consign->setAnchorPoint(Vec2(0.5f,0.5f));
					Label_consign->setPosition(Vec2(0,0));
					Button_consign->addChild(Label_consign);
					//normalInfoPanel->addChild(Button_delete);
					m_pLayer->addChild(Button_consign);

					auto Button_search= Button::create();
					Button_search->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_search->setTouchEnabled(true);
					Button_search->setPressedActionEnabled(true);
					Button_search->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::callBackSearch,this));
					Button_search->setAnchorPoint(Vec2(0.5f,0.5f));
					Button_search->setScale9Enabled(true);
					Button_search->setContentSize(Size(91,43));
					Button_search->setCapInsets(Rect(18,9,2,23));
					Button_search->setPosition(Vec2(200,25));

					auto Label_search = Label::createWithTTF(str_search, APP_FONT_NAME, 16);
					Label_search->setAnchorPoint(Vec2(0.5f,0.5f));
					Label_search->setPosition(Vec2(0,0));
					Button_search->addChild(Label_search);
					//normalInfoPanel->addChild(Button_delete);
					m_pLayer->addChild(Button_search);
					break;
				}
			case ktagEquipMentUI:
				{
					
					auto equip= (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
					//add main equip
					auto btn_addMainEquip= Button::create();
					btn_addMainEquip->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					btn_addMainEquip->setTouchEnabled(true);
					btn_addMainEquip->setPressedActionEnabled(true);
					btn_addMainEquip->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::showCEquipment, this));
					btn_addMainEquip->setAnchorPoint(Vec2(0.5f,0.5f));
					btn_addMainEquip->setScale9Enabled(true);
					btn_addMainEquip->setContentSize(Size(91,43));
					btn_addMainEquip->setCapInsets(Rect(18,9,2,23));
					btn_addMainEquip->setPosition(Vec2(141,25));
					const char *strings_;
					if (equip->currType == 2 && folder->goods().clazz()== GOODS_CLASS_XIANGQIANCAILIAO)
					{
						strings_ = StringDataManager::getString("equip_strength_equipLayily");
					}else
					{
						strings_ = StringDataManager::getString("goods_equipment_main");	
					}

					auto Label_mainEquip = Label::createWithTTF(strings_, APP_FONT_NAME, 16);
					Label_mainEquip->setAnchorPoint(Vec2(0.5f,0.5f));
					Label_mainEquip->setPosition(Vec2(0,0));
					btn_addMainEquip->addChild(Label_mainEquip);
					m_pLayer->addChild(btn_addMainEquip);
					
					break;
				}
			case kTagStoreHouseUI:
				{
					auto Button_putIn= Button::create();
					Button_putIn->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_putIn->setTouchEnabled(true);
					Button_putIn->setPressedActionEnabled(true);
					Button_putIn->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::PutInEvent, this));
					Button_putIn->setAnchorPoint(Vec2(0.5f,0.5f));
					Button_putIn->setScale9Enabled(true);
					Button_putIn->setContentSize(Size(91,43));
					Button_putIn->setCapInsets(Rect(18,9,2,23));
					Button_putIn->setPosition(Vec2(141,25));

					const char *strings_putIn = StringDataManager::getString("storeHouse_putIn");
					auto Label_putIn = Label::createWithTTF(strings_putIn, APP_FONT_NAME, 16);
					Label_putIn->setAnchorPoint(Vec2(0.5f,0.5f));
					Label_putIn->setPosition(Vec2(0,0));
					Button_putIn->addChild(Label_putIn);
					m_pLayer->addChild(Button_putIn);

					break;
				}
			case kTagShopUI :
				{

					auto Button_sell= Button::create();
					Button_sell->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
					Button_sell->setTouchEnabled(true);
					Button_sell->setPressedActionEnabled(true);
					Button_sell->addTouchEventListener(CC_CALLBACK_2(PackageItemInfo::SellEvent,this));
					Button_sell->setAnchorPoint(Vec2(0.5f,0.5f));
					Button_sell->setScale9Enabled(true);
					Button_sell->setContentSize(Size(91,43));
					Button_sell->setCapInsets(Rect(18,9,2,23));
					Button_sell->setPosition(Vec2(141,25));

					auto Label_sell = Label::createWithTTF((const char *)"出售", APP_FONT_NAME, 16);
					Label_sell->setAnchorPoint(Vec2(0.5f,0.5f));
					Label_sell->setPosition(Vec2(0,0));
					Button_sell->addChild(Label_sell);
					m_pLayer->addChild(Button_sell);
					break;
				}
		}
		
		
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setAnchorPoint(Vec2(0,0));
		//this->setContentSize(Size(background->getContentSize().width,background->getContentSize().height));
		//this->setContentSize(Size(274,362));

		return true;
	}
	return false;
}

void PackageItemInfo::onEnter()
{
	UIScene::onEnter();
	auto  action = Sequence::create(
		ScaleTo::create(0.15f*1/2,1.1f),
		ScaleTo::create(0.15f*1/3,1.0f),
		NULL);
	runAction(action);
}
void PackageItemInfo::onExit()
{
	UIScene::onExit();
}

bool PackageItemInfo::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return this->resignFirstResponder(pTouch,this,false);
}
void PackageItemInfo::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void PackageItemInfo::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void PackageItemInfo::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void PackageItemInfo::dressOnEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		if (this->isClosing())
			return;

		// 	auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		// 	if(sc != NULL)
		// 		sc->endCommand(pSender);

		if (GameView::getInstance()->pacPageView->curFolder->goods().binding() == 1)
		{
			auto packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1303, GameView::getInstance()->pacPageView->curFolder, (void *)packageScene->selectActorId);
			GameUtils::playGameSound(EQUIP_DRESS, 2, false);
			//关闭详细信息界面
			this->removeFromParentAndCleanup(true);
		}
		else
		{
			GameView::getInstance()->showPopupWindow(StringDataManager::getString("packageitemInfo_isSureToDressOn"), 2, this, CC_CALLFUNCO_SELECTOR(PackageItemInfo::sureToDressOn), NULL);
		}

	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void PackageItemInfo::sureToDressOn(Ref * pSender)
{
	auto packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1303,GameView::getInstance()->pacPageView->curFolder,(void *)packageScene->selectActorId);
	GameUtils::playGameSound(EQUIP_DRESS, 2, false);
	//关闭详细信息界面
	this->removeFromParentAndCleanup(true);
}

void PackageItemInfo::deleteEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->isClosing())
			return;

		if (GameView::getInstance()->pacPageView->curFolder->quantity() == 1)
		{
			deleteAmount = 1;
			GameView::getInstance()->showPopupWindow(StringDataManager::getString("packageitem_delete"), 2, this, CC_CALLFUNCO_SELECTOR(PackageItemInfo::sureToDelete), NULL);
		}
		else
		{
			//弹出计算器
			GameView::getInstance()->showCounter(this, CC_CALLFUNCO_SELECTOR(PackageItemInfo::showCountForDelete), GameView::getInstance()->pacPageView->curFolder->quantity());
		}

		GameUtils::playGameSound(ITEM_DISCARD, 2, false);
		// 	if (curGoods->quality() >= 3)
		// 	{
		// 		GameView::getInstance()->showPopupWindow(StringDataManager::getString("packageitem_delete"),2,this,SEL_CallFuncO(PackageItemInfo::sureToDelete),NULL);
		// 	}
		// 	else
		// 	{
		// 		GameMessageProcessor::sharedMsgProcessor()->sendReq(1313, this);
		// 		//关闭详细信息界面
		// 		this->removeFromParentAndCleanup(true);
		// 	}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void PackageItemInfo::showCountForDelete(Ref *pSender)
{
	auto counter_ =(Counter *)pSender;
	int amount_;
	if (counter_ != NULL)
	{
		amount_ =counter_->getInputNum();
	}else
	{
		amount_ =1;
	}

	deleteAmount = amount_;

	if (amount_ > GameView::getInstance()->pacPageView->curFolder->quantity())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("packageitemInfo_notEnough"));
	}
	else
	{
		if (GameView::getInstance()->pacPageView->curFolder->goods().quality() >= 3)  //蓝色品阶以上的需要二次确认
		{
		 	GameView::getInstance()->showPopupWindow(StringDataManager::getString("packageitem_delete"),2,this, CC_CALLFUNCO_SELECTOR(PackageItemInfo::sureToDelete),NULL);
		}
		else
		{
		 	GameMessageProcessor::sharedMsgProcessor()->sendReq(1313, (void *)GameView::getInstance()->pacPageView->curPackageItemIndex,(void *)deleteAmount);
		 	//关闭详细信息界面
		 	this->removeFromParentAndCleanup(true);
		}
	}
}

void PackageItemInfo::sureToDelete(Ref * pSender)
{
	if (deleteAmount <= 0)
		return;

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1313, (void *)GameView::getInstance()->pacPageView->curPackageItemIndex,(void *)deleteAmount);
	//关闭详细信息界面
	this->removeFromParentAndCleanup(true);
}

void PackageItemInfo::lvUpEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->isClosing())
			return;

		CCLOG("lvUpEvent");
		//关闭详细信息界面
		this->setVisible(false);
		PackageScene *packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
		if (packageScene != NULL)
		{
			packageScene->closeEndedEvent(NULL);
		}

		ActionInterval * action = (ActionInterval *)Sequence::create(DelayTime::create(0.5f),
			CallFuncN::create(CC_CALLBACK_1(PackageItemInfo::showEquipStrength, this)),
			RemoveSelf::create(),
			NULL);
		runAction(action);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void PackageItemInfo::shortCutEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->isClosing())
			return;

		CCLOG("shortCutEvent");
		//打开快捷面板
		Size winsize = Director::getInstance()->getVisibleSize();
		auto shortcutConfigure = ShortcutConfigure::create();
		shortcutConfigure->setTag(kTagShortcutConfigure);
		GameView::getInstance()->getMainUIScene()->addChild(shortcutConfigure);
		shortcutConfigure->setIgnoreAnchorPointForPosition(false);
		shortcutConfigure->setAnchorPoint(Vec2(0.5f, 0.5f));
		shortcutConfigure->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
		//关闭详细信息界面
		this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}
void PackageItemInfo::useEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->isClosing())
			return;

		CCLOG("useEvent");
		auto packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
		if (packageScene)
		{
			// 		FolderInfo * folderInfo = new FolderInfo();
			// 		for (int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
			// 		{
			// 			if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
			// 			{
			// 				if (strcmp(curGoods->id().c_str(),GameView::getInstance()->AllPacItem.at(i)->goods().id().c_str()) == 0)
			// 				{
			// 					folderInfo->CopyFrom(*GameView::getInstance()->AllPacItem.at(i));
			// 					break;
			// 				}
			// 			}
			// 		}

			if (!curFolders->has_goods())
				return;

			if ((curFolders->goods().additionproperty() & 0X00040000) != 0)  //可以一次性使用多个
			{
				if (curFolders->quantity() == 1)
				{
					PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
					temp->index = GameView::getInstance()->pacPageView->curPackageItemIndex;
					temp->num = 1;
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0, temp);
					delete temp;

					this->removeFromParentAndCleanup(true);
				}
				else
				{
					//弹出计算器
					GameView::getInstance()->showCounter(this, CC_CALLFUNCO_SELECTOR(PackageItemInfo::useAllEvent), GameView::getInstance()->pacPageView->curFolder->quantity());
				}
			}
			else
			{
				if (curGoods->clazz() == 30 || curGoods->clazz() == 28)//武将用（复活）/武将用（经验丹）
				{
					if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralListForTakeDrug) == NULL)
					{
						Size winSize = Director::getInstance()->getVisibleSize();
						GeneralsListForTakeDrug * generalListForTakeDrug = GeneralsListForTakeDrug::create(curFolders);
						generalListForTakeDrug->setIgnoreAnchorPointForPosition(false);
						generalListForTakeDrug->setAnchorPoint(Vec2(0.5f, 0.5f));
						generalListForTakeDrug->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
						GameView::getInstance()->getMainUIScene()->addChild(generalListForTakeDrug, 0, kTagGeneralListForTakeDrug);
					}
				}
				else if (curGoods->clazz() == 1)  //是药水
				{
					std::map<std::string, CDrug*>::const_iterator cIter;
					cIter = CDrugMsgConfigData::s_drugBaseMsg.find(curGoods->id());
					if (cIter != CDrugMsgConfigData::s_drugBaseMsg.end()) // 没找到就是指向END了  
					{
						auto temp = CDrugMsgConfigData::s_drugBaseMsg.find(curGoods->id())->second;
						if (temp->get_type_flag() == 2)//武将用
						{
							if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralListForTakeDrug) == NULL)
							{
								Size winSize = Director::getInstance()->getVisibleSize();
								GeneralsListForTakeDrug * generalListForTakeDrug = GeneralsListForTakeDrug::create(curFolders);
								generalListForTakeDrug->setIgnoreAnchorPointForPosition(false);
								generalListForTakeDrug->setAnchorPoint(Vec2(0.5f, 0.5f));
								generalListForTakeDrug->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
								GameView::getInstance()->getMainUIScene()->addChild(generalListForTakeDrug, 0, kTagGeneralListForTakeDrug);
							}
						}
						else  //角色用
						{
							PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
							temp->index = GameView::getInstance()->pacPageView->curPackageItemIndex;
							temp->num = 1;
							GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)packageScene->selectActorId, temp);
							delete temp;
						}
					}
				}
				else
				{
					PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
					temp->index = GameView::getInstance()->pacPageView->curPackageItemIndex;
					temp->num = 1;
					GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0, temp);
					delete temp;
				}
				this->removeFromParentAndCleanup(true);
			}
		}
		else
		{
		}
		GameUtils::playGameSound(PROP_USE, 2, false);
		//关闭详细信息界面
		//this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}


 void PackageItemInfo::scrollViewDidScroll(cocos2d::extension::ScrollView* view)
{
	
}
 
 void PackageItemInfo::scrollViewDidZoom(cocos2d::extension::ScrollView* view)
 {

 }

 void PackageItemInfo::GetAnnesEvent(Ref *pSender, Widget::TouchEventType type)
 {

	 switch (type)
	 {
	 case Widget::TouchEventType::BEGAN:
		 break;

	 case Widget::TouchEventType::MOVED:
		 break;

	 case Widget::TouchEventType::ENDED:
	 {
		 if (this->isClosing())
			 return;

		 CCLOG("GetAnnesEvent");
		 //添加附件
		 int isbingding = GameView::getInstance()->AllPacItem.at(GameView::getInstance()->pacPageView->curPackageItemIndex)->goods().binding();
		 if (isbingding == 0)
		 {
			 MailUI *mail_ui = (MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
			 mail_ui->callBackAddexan(pSender);
		 }
		 else
		 {
			 const char *string_ = StringDataManager::getString("mail_ofnotSendValue");
			 GameView::getInstance()->showAlertDialog(string_);
		 }
		 removeFromParentAndCleanup(true);
	 }
	 break;

	 case Widget::TouchEventType::CANCELED:
		 break;

	 default:
		 break;
	 }
 }

 void PackageItemInfo::cancelAuction( Ref * pSender )
 {
	 if(this->isClosing())
		 return;

	 //cancel auction
	 this->removeFromParentAndCleanup(true);

 }

 void PackageItemInfo::callBackSearch(Ref *pSender, Widget::TouchEventType type)
 {

	 switch (type)
	 {
	 case Widget::TouchEventType::BEGAN:
		 break;

	 case Widget::TouchEventType::MOVED:
		 break;

	 case Widget::TouchEventType::ENDED:
	 {
		 if (this->isClosing())
			 return;

		 AuctionUi *auctionui = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);

		 if (auctionui == NULL)
		 {
			 return;
		 }

		 auctionui->labelType->setString(auctionui->acutionTypevector.at(0)->typedesc().c_str());
		 auctionui->setAuctiontype = auctionui->acutionTypevector.at(0)->type();
		 auctionui->isSelectChat = false;
		 auctionui->labelKind->setString(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->subtypedescs(0).c_str());
		 auctionui->labelLevel->setString(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->leveldescs(0).c_str());
		 auctionui->labelQuality->setString(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->qualitydescs(0).c_str());

		 const char *strings_goldAll = StringDataManager::getString("auction_SelectMoney");
		 auctionui->label_gold->setString(strings_goldAll);

		 auctionui->setpageindex = 1;
		 auctionui->setAuctiontype = 0;
		 auctionui->setAuctionsubtype = 0;
		 auctionui->setlevel = 0;
		 auctionui->setMoney = 0;
		 auctionui->setquality = 0;
		 auctionui->setorderby = 0;
		 auctionui->setorderbydesc = 0;

		 auctionui->setname = GameView::getInstance()->AllPacItem.at(auctionui->pageView->curPackageItemIndex)->goods().name();
		 GameMessageProcessor::sharedMsgProcessor()->sendReq(1801, auctionui);
		 removeFromParentAndCleanup(true);
		 auctionui->callBack_btn_back(pSender, Widget::TouchEventType::ENDED);
	 }
	 break;

	 case Widget::TouchEventType::CANCELED:
		 break;

	 default:
		 break;
	 }


 }

 void PackageItemInfo::callBackConsign(Ref *pSender, Widget::TouchEventType type)
 {

	 switch (type)
	 {
	 case Widget::TouchEventType::BEGAN:
		 break;

	 case Widget::TouchEventType::MOVED:
		 break;

	 case Widget::TouchEventType::ENDED:
	 {
		 if (this->isClosing())
			 return;

		 //add auction
		 int isbingding = GameView::getInstance()->AllPacItem.at(GameView::getInstance()->pacPageView->curPackageItemIndex)->goods().binding();
		 if (isbingding == 0)
		 {
			 auto auctionui = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
			 auctionui->addAuctionPacInfo(pSender);
		 }
		 else
		 {
			 const char *string_ = StringDataManager::getString("mail_ofnotSendValue");
			 GameView::getInstance()->showAlertDialog(string_);
		 }
		 removeFromParentAndCleanup(true);
	 }
	 break;

	 case Widget::TouchEventType::CANCELED:
		 break;

	 default:
		 break;
	 }

 }

 void PackageItemInfo::showGoodsInfo(Ref *pSender, Widget::TouchEventType type)
 {

	 switch (type)
	 {
	 case Widget::TouchEventType::BEGAN:
		 break;

	 case Widget::TouchEventType::MOVED:
		 break;

	 case Widget::TouchEventType::ENDED:
	 {

		 if (this->isClosing())
			 return;

		 auto package = (BackPackage *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat)->getChildByTag(PACKAGEAGE);
		 package->callBack(pSender);
		 removeFromParentAndCleanup(true);
	 }
	 break;

	 case Widget::TouchEventType::CANCELED:
		 break;

	 default:
		 break;
	 }
 }

 void PackageItemInfo::showGoodsInfoPrivateui(Ref *pSender, Widget::TouchEventType type)
 {

	 switch (type)
	 {
	 case Widget::TouchEventType::BEGAN:
		 break;

	 case Widget::TouchEventType::MOVED:
		 break;

	 case Widget::TouchEventType::ENDED:
	 {
		 if (this->isClosing())
			 return;

		 auto privateui_ = (PrivateChatUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagPrivateUI);
		 auto package = (BackPackage *)privateui_->layer2->getChildByTag(PACKAGEAGE);
		 package->callBackOfPrivateUi();
		 removeFromParentAndCleanup(true);
	 }
	 break;

	 case Widget::TouchEventType::CANCELED:
		 break;

	 default:
		 break;
	 }


 }

 void PackageItemInfo::showCEquipment(Ref *pSender, Widget::TouchEventType type)
 {

	 switch (type)
	 {
	 case Widget::TouchEventType::BEGAN:
		 break;

	 case Widget::TouchEventType::MOVED:
		 break;

	 case Widget::TouchEventType::ENDED:
	 {

		 if (this->isClosing())
			 return;

		 auto btn = (Button *)pSender;
		 auto equip_ui = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		 equip_ui->addOperationFloder(curFolders);

		 removeFromParentAndCleanup(true);
	 }
	 break;

	 case Widget::TouchEventType::CANCELED:
		 break;

	 default:
		 break;
	 }
 }

 void PackageItemInfo::showStrengthStone( Ref * obj )
 {
	 //add assist strengthStone
	 /*
	 EquipMentUi *equip_ui = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	 equip_ui->addStrengthStone(obj);
	 removeFromParentAndCleanup(true);
	 */
 }

 void PackageItemInfo::useAllEvent( Ref *pSender )
 {
	 auto counter_ =(Counter *)pSender;
	 int amount_;
	 if (counter_ != NULL)
	 {
		 amount_ =counter_->getInputNum();
	 }else
	 {
		 amount_ =1;
	 }

	 if (amount_ > GameView::getInstance()->pacPageView->curFolder->quantity())
	 {
		 GameView::getInstance()->showAlertDialog(StringDataManager::getString("packageitemInfo_notEnough"));
	 }
	 else
	 {
		 PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
		 temp->index = GameView::getInstance()->pacPageView->curPackageItemIndex;
		 temp->num = amount_;
		 GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0,temp);
		 delete temp;
	 }

	 this->removeFromParentAndCleanup(true);
 }

 void PackageItemInfo::PutInEvent(Ref *pSender, Widget::TouchEventType type)
 {

	 switch (type)
	 {
	 case Widget::TouchEventType::BEGAN:
		 break;

	 case Widget::TouchEventType::MOVED:
		 break;

	 case Widget::TouchEventType::ENDED:
	 {
		 if (this->isClosing())
			 return;

		 auto storeHouseui = (StoreHouseUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStoreHouseUI);
		 storeHouseui->PutInOrOutEvent(pSender);
		 removeFromParentAndCleanup(true);
	 }
	 break;

	 case Widget::TouchEventType::CANCELED:
		 break;

	 default:
		 break;
	 }


 }

 void PackageItemInfo::SellEvent(Ref *pSender, Widget::TouchEventType type)
 {

	 switch (type)
	 {
	 case Widget::TouchEventType::BEGAN:
		 break;

	 case Widget::TouchEventType::MOVED:
		 break;

	 case Widget::TouchEventType::ENDED:
	 {
		 if (this->isClosing())
			 return;

		 if (curFolders->quantity() <= 1)
		 {
			 ShopUI::sellGoodsStruct goodsStruct = { GameView::getInstance()->pacPageView->curPackageItemIndex,curFolders->quantity(),0 };
			 GameMessageProcessor::sharedMsgProcessor()->sendReq(1135, &goodsStruct);
			 GameUtils::playGameSound(PROP_SALE, 2, false);
			 this->removeFromParent();
		 }
		 else
		 {
			 GameView::getInstance()->showCounter(this, CC_CALLFUNCO_SELECTOR(PackageItemInfo::SellEventNum), curFolders->quantity());
		 }
	 }
	 break;

	 case Widget::TouchEventType::CANCELED:
		 break;

	 default:
		 break;
	 }

 }

 void PackageItemInfo::SellEventNum( Ref *pSender )
 {
	 auto counter_ =(Counter *)pSender;
	 int amount_;
	 if (counter_ != NULL)
	 {
		 amount_ =counter_->getInputNum();
	 }else
	 {
		 amount_ =1;
	 }

	 if (amount_ <= 0)
	 {

	 }
	 else
	 {
		 ShopUI::sellGoodsStruct goodsStruct= {GameView::getInstance()->pacPageView->curPackageItemIndex,amount_,0};
		 GameMessageProcessor::sharedMsgProcessor()->sendReq(1135,&goodsStruct);
	 }
	 
	 GameUtils::playGameSound(PROP_SALE, 2, false);
	 this->removeFromParent();
 }

 void PackageItemInfo::showEquipStrength( Ref * obj )
 {
	 Size winSize = Director::getInstance()->getVisibleSize();
	 if(GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI) == NULL)
	 {
		 auto equip=EquipMentUi::create();
		 equip->setIgnoreAnchorPointForPosition(false);
		 equip->setAnchorPoint(Vec2(0.5f,0.5f));
		 equip->setPosition(Vec2(winSize.width/2,winSize.height/2));
		 GameView::getInstance()->getMainUIScene()->addChild(equip,0,ktagEquipMentUI);
		 equip->refreshBackPack(0,-1);

		 auto folders = new FolderInfo(*GameView::getInstance()->pacPageView->curFolder);
		equip->addMainEquipment(folders);
		delete folders;
	 }
 }

 //////////////////////
 void PackageItemInfo::registerScriptCommand( int scriptId )
 {
	 mTutorialScriptInstanceId = scriptId;
 }

 void PackageItemInfo::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
 {
	 Size winSize = Director::getInstance()->getWinSize();
// 	 CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,35,50);
// 	 tutorialIndicator->setPosition(Vec2(pos.x+55,pos.y+80));
// 	 tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	 m_pLayer->addChild(tutorialIndicator);

	 int _w = (winSize.width-800)/2;
	 int _h = (winSize.height - 480)/2;

	 auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,80,43);
	 tutorialIndicator->setDrawNodePos(Vec2(pos.x+85.5f+_w,pos.y+59.5f+_h));
	 tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	 auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	 if (mainScene)
	 {
		 mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	 }
 }

 void PackageItemInfo::removeCCTutorialIndicator()
 {
// 	 CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(m_pLayer->getChildByTag(CCTUTORIALINDICATORTAG));
// 	 if(tutorialIndicator != NULL)
// 		 tutorialIndicator->removeFromParent();

	 auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	 if (mainScene)
	 {
		 auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		 if(teachingGuide != NULL)
			 teachingGuide->removeFromParent();
	 }
 }

 void PackageItemInfo::callBackPick(Ref *pSender, Widget::TouchEventType type)
 {

	 switch (type)
	 {
	 case Widget::TouchEventType::BEGAN:
		 break;

	 case Widget::TouchEventType::MOVED:
		 break;

	 case Widget::TouchEventType::ENDED:
	 {
		 this->removeFromParentAndCleanup(true);
		 const char *strBegin = StringDataManager::getString("packBackGoodsPickSureBegin");
		 const char *strEnd = StringDataManager::getString("packBackGoodsPickSureEnd");

		 int starLevel = GameView::getInstance()->AllPacItem.at(GameView::getInstance()->pacPageView->curPackageItemIndex)->goods().equipmentdetail().starlevel();

		 auto equipStarLevel = StrengthStarConfigData::s_EquipStarAmount[starLevel];
		 int starCount_ = equipStarLevel->get_count();
		 char str[20];
		 sprintf(str, "%d", starCount_);

		 std::string string_ = strBegin;
		 string_.append(str);
		 string_.append(strEnd);
		 GameView::getInstance()->showPopupWindow(string_, 2, this, CC_CALLFUNCO_SELECTOR(PackageItemInfo::callBackPickSure), NULL);
	 }
	 break;

	 case Widget::TouchEventType::CANCELED:
		 break;

	 default:
		 break;
	 }


 }

 void PackageItemInfo::callBackPickSure( Ref * obj )
 {
	 assistEquipStruct assists_={2,0,0,GameView::getInstance()->pacPageView->curPackageItemIndex,2};
	 GameMessageProcessor::sharedMsgProcessor()->sendReq(1601,&assists_);

	 GameMessageProcessor::sharedMsgProcessor()->sendReq(1608,this);
	 GameMessageProcessor::sharedMsgProcessor()->sendReq(1699,this);
 }

 void PackageItemInfo::systhesisEvent(Ref *pSender, Widget::TouchEventType type)
 {

	 switch (type)
	 {
	 case Widget::TouchEventType::BEGAN:
		 break;

	 case Widget::TouchEventType::MOVED:
		 break;

	 case Widget::TouchEventType::ENDED:
	 {
		 //关闭详细信息界面
		 this->setVisible(false);
		 auto packageScene = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
		 if (packageScene != NULL)
		 {
			 packageScene->closeEndedEvent(NULL);
		 }

		 auto action = (ActionInterval *)Sequence::create(
			 DelayTime::create(0.5f),
			 CallFuncN::create(CC_CALLBACK_1(PackageItemInfo::showEquipSysthesisUI, this, (void *)curFolders)),
			 RemoveSelf::create(),
			 NULL);
		 runAction(action);
	 }
	 break;

	 case Widget::TouchEventType::CANCELED:
		 break;

	 default:
		 break;
	 }

 }

 void PackageItemInfo::showEquipSysthesisUI( Node *pNode ,void * folderInfo)
 {
	 Size winSize = Director::getInstance()->getVisibleSize();
	 auto folder = (FolderInfo *)(folderInfo);
	 if (folder)
	 {
		 if(GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI) == NULL)
		 {
			 auto equipui=EquipMentUi::create();
			 equipui->setIgnoreAnchorPointForPosition(false);
			 equipui->setAnchorPoint(Vec2(0.5f,0.5f));
			 equipui->setPosition(Vec2(winSize.width/2,winSize.height/2));
			 GameView::getInstance()->getMainUIScene()->addChild(equipui,0,ktagEquipMentUI);

			 equipui->equipTab->setDefaultPanelByIndex(4);
			 equipui->callBackChangeTab( equipui->equipTab);
			 equipui->addMainEquipment(folder);
		 }
	 }
 }

 void PackageItemInfo::setCurEquipIsUseAble( bool useAble )
 {
	 auto btn_ = (Button *)m_pLayer->getChildByTag(112233);
	 if (btn_ != NULL)
	 {
		 if (useAble)
		 {
			 Button_dressOn->setTouchEnabled(true);
			 Button_dressOn->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		 }else
		 {
			 Button_dressOn->setTouchEnabled(false);
			 Button_dressOn->loadTextures("res_ui/new_button_001.png","res_ui/new_button_001.png","");
		 }
	 }
 }

