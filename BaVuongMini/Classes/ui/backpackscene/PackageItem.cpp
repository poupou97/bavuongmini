#include "PackageItem.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "PackageItemInfo.h"
#include "GameView.h"
#include "../Mail_ui/MailUI.h"
#include "AppMacros.h"
#include "../Auction_ui/AuctionUi.h"
#include "../Chat_ui/ChatUI.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../utils/GameUtils.h"
#include "PacPageView.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "GoodsItemInfoBase.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "PackageScene.h"
#include "../../messageclient/element/CDrug.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../ui/Chat_ui/BackPackage.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/CGemPropertyMap.h"
#include "../../legend_script/CCTeachingGuide.h"

#define voidFramePath "res_ui/di_none.png"

#define whiteFramePath "res_ui/di_white.png"
#define greenFramePath "res_ui/di_green.png"
#define blueFramePath "res_ui/di_bule.png"
#define purpleFramePath "res_ui/di_purple.png"
#define orangeFramePath "res_ui/di_orange.png"

#define fontBgPath "res_ui/zidi.png"

#define  MengBan_Red_Name "m_mengban_red"
#define  MengBan_Black_Name "m_mengban_black"
#define  MengBan_Avalible "m_mengban_black"

PackageItem::PackageItem():
index(0),
cur_type(COMMONITEM)
{
	curFolder = new FolderInfo();
}


PackageItem::~PackageItem()
{
	delete curFolder;
}


PackageItem * PackageItem::create(FolderInfo* folder)
{
	auto widget = new PackageItem();
	if (widget && widget->init(folder))
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}
bool PackageItem::init(FolderInfo* folder)
{
	if (Widget::init())
	{
		curFolder->CopyFrom(*folder);
		index = folder->id();
		ImageView_bound = ImageView::create();
		if (folder->has_goods() == true)
		{
			cur_type = COMMONITEM;

			/*********************判断颜色***************************/
			std::string frameColorPath;
			if (folder->goods().quality() == 1)
			{
				frameColorPath = whiteFramePath;
			}
			else if (folder->goods().quality() == 2)
			{
				frameColorPath = greenFramePath;
			}
			else if (folder->goods().quality() == 3)
			{
				frameColorPath = blueFramePath;
			}
			else if (folder->goods().quality() == 4)
			{
				frameColorPath = purpleFramePath;
			}
			else if (folder->goods().quality() == 5)
			{
				frameColorPath = orangeFramePath;
			}
			else
			{
				frameColorPath = voidFramePath;
			}

			Btn_pacItemFrame = Button::create();
			Btn_pacItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
			Btn_pacItemFrame->setTouchEnabled(true);
// 			Btn_pacItemFrame->setScale9Enabled(true);
// 			Rect cr = Rect(9.5f,9.5f,6.0f,6.0f);
// 			Btn_pacItemFrame->setCapInsets(cr);
// 			Btn_pacItemFrame->setContentSize(Size(91,84));
			Btn_pacItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
			Btn_pacItemFrame->setPosition(Vec2(Btn_pacItemFrame->getContentSize().width/2,Btn_pacItemFrame->getContentSize().height/2));
			Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(PackageItem::PackageItemEvent, this));
			Btn_pacItemFrame->setPressedActionEnabled(true);
			this->addChild(Btn_pacItemFrame);

			ImageView_packageItem = ImageView::create();
			if (strcmp(folder->goods().icon().c_str(),"") == 0)
			{
				ImageView_packageItem->loadTexture("res_ui/props_icon/prop.png");
			}
			else
			{
				std::string _path = "res_ui/props_icon/";
				_path.append(folder->goods().icon().c_str());
				_path.append(".png");
				ImageView_packageItem->loadTexture(_path.c_str());
			}
			//ImageView_packageItem->loadTexture("res_ui/props_icon/cubuyi.png");
			ImageView_packageItem->setAnchorPoint(Vec2(0.5f,0.5f));
			ImageView_packageItem->setPosition(Vec2(0,7));   //(Vec2(42,43));
			Btn_pacItemFrame->addChild(ImageView_packageItem);

			Lable_name = Label::createWithTTF(folder->goods().name().c_str(), APP_FONT_NAME, 12);
			Lable_name->setColor(GameView::getInstance()->getGoodsColorByQuality(folder->goods().quality()));
			Lable_name->setAnchorPoint(Vec2(0.5f,1.0f));
			Lable_name->setPosition(Vec2(0,-18));
			Lable_name->setName("Label_name");
			Btn_pacItemFrame->addChild(Lable_name);

			Lable_num = Label::createWithTTF("", APP_FONT_NAME, 13);
			if (folder->quantity() > 1)
			{
				char s_num[5];
				sprintf(s_num,"%d",folder->quantity());
				Lable_num->setString(s_num);
			}
			else
			{
				Lable_num->setString("");
			}

			Lable_num->setAnchorPoint(Vec2(0.0f,1.0f));
			Lable_num->setPosition(Vec2(33-Lable_num->getContentSize().width,-3));
			Lable_num->setName("Label_num");
			Btn_pacItemFrame->addChild(Lable_num);

			ImageView_bound->loadTexture("res_ui/binding.png");
			ImageView_bound->setAnchorPoint(Vec2(1.0f,0.0f));
			ImageView_bound->setPosition(Vec2(-18,14));
			ImageView_bound->setVisible(false);
			
			if (folder->goods().binding() == 1)
			{
				Btn_pacItemFrame->addChild(ImageView_bound);
			}
			

			//如果是装备
			if(folder->goods().has_equipmentdetail())
			{
				if (folder->goods().equipmentdetail().gradelevel() > 0)  //精练等级
				{
					std::string ss_gradeLevel = "+";
					char str_gradeLevel [10];
					sprintf(str_gradeLevel,"%d",folder->goods().equipmentdetail().gradelevel());
					ss_gradeLevel.append(str_gradeLevel);
					auto Lable_gradeLevel = Label::createWithTTF(ss_gradeLevel.c_str(), APP_FONT_NAME, 13);
					Lable_gradeLevel->setAnchorPoint(Vec2(0.0f,0.0f));
					Lable_gradeLevel->setPosition(Vec2(30-Lable_gradeLevel->getContentSize().width,31-Lable_gradeLevel->getContentSize().height));
					Lable_gradeLevel->setName("Lable_gradeLevel");
					Btn_pacItemFrame->addChild(Lable_gradeLevel);
				}

				if (folder->goods().equipmentdetail().starlevel() > 0)  //星级
				{
					char str_starLevel [10];
					sprintf(str_starLevel,"%d",folder->goods().equipmentdetail().starlevel());
					auto Lable_starLevel = Label::createWithTTF(str_starLevel, APP_FONT_NAME, 13);
					Lable_starLevel->setAnchorPoint(Vec2(1.0f,1.0f));
					Lable_starLevel->setPosition(Vec2(-34+Lable_starLevel->getContentSize().width,-17+Lable_starLevel->getContentSize().height));
					Lable_starLevel->setName("Lable_starLevel");
					Btn_pacItemFrame->addChild(Lable_starLevel);

					auto ImageView_star = ImageView::create();
					ImageView_star->loadTexture("res_ui/star_on.png");
					ImageView_star->setAnchorPoint(Vec2(1.0f,1.0f));
					ImageView_star->setPosition(Vec2(Lable_starLevel->getContentSize().height,-2));
					ImageView_star->setVisible(true);
					ImageView_star->setScale(0.5f);
					Lable_starLevel->addChild(ImageView_star);
				}
			}

			//装备耐久为零
			if (folder->goods().equipmentclazz()>0 && folder->goods().equipmentclazz()<11)
			{
				if (folder->goods().equipmentdetail().durable() > 10)
				{
					this->setRed(false);
				}
				else
				{
					this->setRed(true);
				}
			}

			//如果是宝石
			if (folder->goods().clazz() == GOODS_CLASS_XIANGQIANCAILIAO)
			{
				std::string goodsId = folder->goods().id();
				std::string goodsIcon = getCurGemIconPath(goodsId);

				auto ImageView_gemPropertyIcon = ImageView::create();
				ImageView_gemPropertyIcon->loadTexture(goodsIcon.c_str());
				ImageView_gemPropertyIcon->setAnchorPoint(Vec2(0,0));
				ImageView_gemPropertyIcon->setPosition(Vec2(30-ImageView_gemPropertyIcon->getContentSize().width,31-ImageView_gemPropertyIcon->getContentSize().height));
				ImageView_gemPropertyIcon->setVisible(true);
				ImageView_gemPropertyIcon->setName("gemPropertyIcon");
				Btn_pacItemFrame->addChild(ImageView_gemPropertyIcon);
				ImageView_gemPropertyIcon->setVisible(false);
			}
		}
		else
		{
			this->cur_type = VOIDITEM;
			//init(VOIDITEM);
			auto imageView_background = ImageView::create();
			imageView_background->loadTexture(voidFramePath);
			imageView_background->setAnchorPoint(Vec2::ZERO);
			imageView_background->setPosition(Vec2(0,0));
			imageView_background->setName("imageView_background");
			this->addChild(imageView_background);
		}

		Sprite * progress_skill = Sprite::create("res_ui/zhezhao_75.png");
		mProgressTimer_skill = ProgressTimer::create(progress_skill);
		mProgressTimer_skill->setAnchorPoint(Vec2(0.5f, 0.5f));
		mProgressTimer_skill->setPosition(Vec2(83/2,75/2));
		mProgressTimer_skill->setVisible(false);
		mProgressTimer_skill->setType(ProgressTimer::Type::RADIAL);
		mProgressTimer_skill->setReverseDirection(true); // 设置进度条为逆时针
		this->getVirtualRenderer()->addChild(mProgressTimer_skill,100);

		//this->setTouchEnabled(true);
		this->setContentSize(Size(83,75));
		return true;
	}
	return false;
}

PackageItem * PackageItem::create(GoodsInfo* goods)
{
	auto widget = new PackageItem();
	if (widget && widget->initWithGoods(goods))
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}

bool PackageItem::initWithGoods(GoodsInfo* goods)
{
	if (Widget::init())
	{
		curGood = goods;
		/*********************判断颜色***************************/
		std::string frameColorPath;
		if (goods->quality() == 1)
		{
			frameColorPath = whiteFramePath;
		}
		else if (goods->quality() == 2)
		{
			frameColorPath = greenFramePath;
		}
		else if (goods->quality() == 3)
		{
			frameColorPath = blueFramePath;
		}
		else if (goods->quality() == 4)
		{
			frameColorPath = purpleFramePath;
		}
		else if (goods->quality() == 5)
		{
			frameColorPath = orangeFramePath;
		}
		else
		{
			frameColorPath = voidFramePath;
		}

		Btn_pacItemFrame = Button::create();
		Btn_pacItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		Btn_pacItemFrame->setTouchEnabled(true);
// 		Btn_pacItemFrame->setScale9Enabled(true);
// 		Rect cr = Rect(9.5f,9.5f,6.0f,6.0f);
// 		Btn_pacItemFrame->setCapInsets(cr);
// 		Btn_pacItemFrame->setContentSize(Size(83,75));
		Btn_pacItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_pacItemFrame->setPosition(Vec2(Btn_pacItemFrame->getContentSize().width/2,Btn_pacItemFrame->getContentSize().height/2));
		Btn_pacItemFrame->addTouchEventListener(CC_CALLBACK_2(PackageItem::GoodItemEvent, this));
		Btn_pacItemFrame->setPressedActionEnabled(true);
		this->addChild(Btn_pacItemFrame);

		ImageView_packageItem = ImageView::create();
		if (strcmp(goods->icon().c_str(),"") == 0)
		{
			ImageView_packageItem->loadTexture("res_ui/props_icon/prop.png");
		}
		else
		{
			std::string _path = "res_ui/props_icon/";
			_path.append(goods->icon().c_str());
			_path.append(".png");
			ImageView_packageItem->loadTexture(_path.c_str());
		}
		ImageView_packageItem->setAnchorPoint(Vec2(0.5f,0.5f));
		ImageView_packageItem->setPosition(Vec2(0,7));
		Btn_pacItemFrame->addChild(ImageView_packageItem);

		Lable_name = Label::createWithTTF(goods->name().c_str(), APP_FONT_NAME, 12);
		//CCLOG("%s",goods->name().c_str());
		Lable_name->setAnchorPoint(Vec2(0.5f,1.0f));
		Lable_name->setPosition(Vec2(0,-18));
		Lable_name->setName("Label_name");
		Btn_pacItemFrame->addChild(Lable_name);
		Lable_name->setColor(GameView::getInstance()->getGoodsColorByQuality(goods->quality()));

		Lable_num = Label::createWithTTF("", APP_FONT_NAME, 13);
// 		if (goods->availabletime() > 1)
// 		{
// 			char s_num[5];
// 			sprintf(s_num,"%d",goods->availabletime());
// 			Lable_num->setText(s_num);
// 		}
// 		else
// 		{
			Lable_num->setString("");
//		}
		Lable_num->setAnchorPoint(Vec2(0,1.0f));
		Lable_num->setPosition(Vec2(33-Lable_num->getContentSize().width,-3));
		Lable_num->setName("Label_num");
		Btn_pacItemFrame->addChild(Lable_num);

		ImageView_bound = ImageView::create();
		ImageView_bound->loadTexture("res_ui/binding.png");
		ImageView_bound->setAnchorPoint(Vec2(1.0f,0));
		ImageView_bound->setPosition(Vec2(-18,14));
		ImageView_bound->setVisible(false);

		//如果是装备
		if(goods->has_equipmentdetail())
		{
			if (goods->equipmentdetail().gradelevel() > 0)  //精练等级
			{
				std::string ss_gradeLevel = "+";
				char str_gradeLevel [10];
				sprintf(str_gradeLevel,"%d",goods->equipmentdetail().gradelevel());
				ss_gradeLevel.append(str_gradeLevel);
				auto Lable_gradeLevel = Label::createWithTTF(ss_gradeLevel.c_str(), APP_FONT_NAME, 13);
				Lable_gradeLevel->setAnchorPoint(Vec2(0,0));
				Lable_gradeLevel->setPosition(Vec2(30-Lable_gradeLevel->getContentSize().width,31-Lable_gradeLevel->getContentSize().height));
				Lable_gradeLevel->setName("Lable_gradeLevel");
				Btn_pacItemFrame->addChild(Lable_gradeLevel);
			}

			if (goods->equipmentdetail().starlevel() > 0)  //星级
			{
				char str_starLevel [10];
				sprintf(str_starLevel,"%d",goods->equipmentdetail().starlevel());
				auto Lable_starLevel = Label::createWithTTF(str_starLevel, APP_FONT_NAME, 13);
				Lable_starLevel->setAnchorPoint(Vec2(1.0f,1.0f));
				Lable_starLevel->setPosition(Vec2(-34+Lable_starLevel->getContentSize().width,-17+Lable_starLevel->getContentSize().height));
				Lable_starLevel->setName("Lable_starLevel");
				Btn_pacItemFrame->addChild(Lable_starLevel);

				auto ImageView_star = ImageView::create();
				ImageView_star->loadTexture("res_ui/star_on.png");
				ImageView_star->setAnchorPoint(Vec2(1.0f,1.0f));
				ImageView_star->setPosition(Vec2(Lable_starLevel->getContentSize().height,-2));
				ImageView_star->setVisible(true);
				ImageView_star->setScale(0.5f);
				Lable_starLevel->addChild(ImageView_star);
			}
		}


		if (goods->binding() == 1)
		{
			Btn_pacItemFrame->addChild(ImageView_bound);
		}

		if ((goods->equipmentclazz()>0) && (goods->equipmentclazz()<11))
		{
			if (goods->equipmentdetail().durable() > 10)
			{
				this->setRed(false);
			}
			else
			{
				this->setRed(true);
			}
		}

		//this->setTouchEnabled(true);
		this->setContentSize(Size(83,75));

		return true;
	}
	return false;
}

PackageItem * PackageItem::createLockedItem(int folderIndex)
{
	auto widget = new PackageItem();
	if (widget && widget->initLockedItem(folderIndex))
	{
		widget->autorelease();
		return widget;
	}
	CC_SAFE_DELETE(widget);
	return NULL;
}
bool PackageItem::initLockedItem(int folderIndex)
{
	if (Widget::init())
	{
		this->setContentSize(Size(83,75));
		this->cur_type = LOCKITEM;
		this->index = folderIndex;

		Button * Btn_lockFrame = Button::create();
		Btn_lockFrame->loadTextures(voidFramePath,voidFramePath,"");
		Btn_lockFrame->setTouchEnabled(true);
		Btn_lockFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_lockFrame->setPosition(Vec2(Btn_lockFrame->getContentSize().width/2,Btn_lockFrame->getContentSize().height/2));
		Btn_lockFrame->addTouchEventListener(CC_CALLBACK_2(PackageItem::BtnLockEvent, this));
		Btn_lockFrame->setPressedActionEnabled(true);
		this->addChild(Btn_lockFrame);

		//锁定图标
		auto imageView_lock = ImageView::create();
		imageView_lock->loadTexture("res_ui/suo.png");
		imageView_lock->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_lock->setPosition(Vec2(0,0));
		imageView_lock->setName("imageView_lock");
		Btn_lockFrame->addChild(imageView_lock);

		return true;
	}
	return false;
}

void PackageItem::setLabelNameText(char * str)
{
	this->Lable_name->setString(str);
}

void PackageItem::PackageItemEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);
		CCLOG("PackageItemEvent PackageItemEvent PackageItemEvent");

		if ((bool)isDoubleTouch())
		{
			CCLOG("165435165432165465415");
		}

		auto pScene = Director::getInstance()->getRunningScene();
		Layer * uiScene = (Layer*)pScene->getChildren().at(0);

		GameView::getInstance()->pacPageView->curFolder = curFolder;
		GameView::getInstance()->pacPageView->curPackageItemIndex = curFolder->id();
		Size size = Director::getInstance()->getVisibleSize();
		// 	EquipMentUi *equip_ui = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		// 	if (equip_ui != NULL)
		// 	{
		// 		if (equip_ui->currType == 2 && curFolder->goods().clazz()== 3)
		// 		{
		// 			PackageItemInfo * packageItemInfo = PackageItemInfo::create(curFolder,GameView::getInstance()->EquipListItem,0);
		// 			packageItemInfo->setIgnoreAnchorPointForPosition(false);
		// 			packageItemInfo->setAnchorPoint(Vec2(0.5f,0.5f));
		// 			packageItemInfo->setTag(kTagGoodsItemInfoBase);
		// 			GameView::getInstance()->getMainUIScene()->addChild(packageItemInfo);
		// 		}else
		// 		{
		// 			equip_ui->addOperationFloder(curFolder);
		// 		}
		// 		return;
		// 	}

		PackageItemInfo * packageItemInfo;
		switch (GameView::getInstance()->pacPageView->getCurUITag())
		{
		case kTagBackpack:
		{
			std::vector<CEquipment *> equipVector;
			PackageScene * package_ = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
			if (package_ != NULL)
			{
				if (package_->selectActorId != 0)
				{
					for (int i = 0; i<GameView::getInstance()->generalsInLineDetailList.size(); i++)
					{
						if (GameView::getInstance()->generalsInLineDetailList.at(i)->generalid() == package_->selectActorId)
						{
							for (int j = 0; j<GameView::getInstance()->generalsInLineDetailList.at(i)->equipments_size(); j++)
							{
								CEquipment * equip_ = new CEquipment();
								equip_->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->equipments(j));
								equipVector.push_back(equip_);
							}
						}
					}
					packageItemInfo = PackageItemInfo::create(curFolder, equipVector, package_->selectActorId);

					std::vector<CEquipment *>::iterator iter;
					for (iter = equipVector.begin(); iter != equipVector.end(); iter++)
					{
						delete *iter;
					}
					equipVector.clear();
				}
				else
				{
					packageItemInfo = PackageItemInfo::create(curFolder, GameView::getInstance()->EquipListItem, package_->selectActorId);
				}
			}
		}break;
		case kTabChat:
		{
			ChatUI * chatui_ = (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
			if (chatui_ != NULL)
			{
				BackPackage * backage_ = (BackPackage *)chatui_->layer_3->getChildByTag(PACKAGEAGE);
				long long generalId = 0;
				if (backage_->curRoleIndex == 0)
				{
					generalId = 0;
					packageItemInfo = PackageItemInfo::create(curFolder, GameView::getInstance()->EquipListItem, 0);
				}
				else
				{
					generalId = GameView::getInstance()->generalsInLineDetailList.at(backage_->curRoleIndex - 1)->generalid();
					packageItemInfo = PackageItemInfo::create(curFolder, backage_->playerEquipments, generalId);
				}
			}
		}break;
		case ktagEquipMentUI:
		{
			EquipMentUi *equip_ui = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
			if (equip_ui != NULL)
			{
				PackageItemInfo * packageItemInfo = PackageItemInfo::create(curFolder, equip_ui->generalEquipVector, equip_ui->mes_petid);
				packageItemInfo->setIgnoreAnchorPointForPosition(false);
				packageItemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				packageItemInfo->setTag(kTagGoodsItemInfoBase);
				GameView::getInstance()->getMainUIScene()->addChild(packageItemInfo);

				/*
				if (equip_ui->currType == 2 && curFolder->goods().clazz()== 3)
				{
				PackageItemInfo * packageItemInfo = PackageItemInfo::create(curFolder,equip_ui->generalEquipVector,equip_ui->mes_petid);
				packageItemInfo->setIgnoreAnchorPointForPosition(false);
				packageItemInfo->setAnchorPoint(Vec2(0.5f,0.5f));
				packageItemInfo->setTag(kTagGoodsItemInfoBase);
				GameView::getInstance()->getMainUIScene()->addChild(packageItemInfo);
				}else
				{
				equip_ui->addOperationFloder(curFolder);
				}
				*/
				return;
			}
		}break;
		default:
		{
			packageItemInfo = PackageItemInfo::create(curFolder, GameView::getInstance()->EquipListItem, 0);
		}break;
		}

		//PackageItemInfo * packageItemInfo = PackageItemInfo::create(curFolder,GameView::getInstance()->EquipListItem,0);
		//packageItemInfo = PackageItemInfo::create(curFolder,GameView::getInstance()->EquipListItem,0);
		packageItemInfo->setIgnoreAnchorPointForPosition(false);
		packageItemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
		//packageItemInfo->setPosition(autoGetPosition(pSender));
		//packageItemInfo->setPosition(Vec2(size.width/2,size.height/2));
		packageItemInfo->setTag(kTagGoodsItemInfoBase);
		GameView::getInstance()->getMainUIScene()->addChild(packageItemInfo);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void PackageItem::GoodItemEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if ((bool)isDoubleTouch())
		{
			CCLOG("165435165432165465415");
		}

		Size size = Director::getInstance()->getVisibleSize();
		GoodsItemInfoBase * goodsItemInfoBase = GoodsItemInfoBase::create(curGood, GameView::getInstance()->EquipListItem, 0);
		goodsItemInfoBase->setIgnoreAnchorPointForPosition(false);
		goodsItemInfoBase->setAnchorPoint(Vec2(0.5f, 0.5f));
		//goodsItemInfoBase->setPosition(Vec2(size.width/2,size.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

Vec2 PackageItem::autoGetPosition(Ref * pSender)
{

	Size winSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	Vec2 cur_positon = ((PackageItem*)(((Button*)pSender)->getParent()))->getPosition();
	Size cur_size = Size(83,75);
	if (380+GameView::getInstance()->pacPageView->getPositionX()+cur_positon.x+cur_size.width+272+(winSize.width-designResolutionSize.width)/2 < winSize.width)
	{
		//可以在右侧显示
		if(86+cur_positon.y > 360)
		{
			//可以在右下显示
			int x= 380+cur_positon.x+cur_size.width+origin.x+(winSize.width-designResolutionSize.width)/2+GameView::getInstance()->pacPageView->getPositionX();
			int y = 86+cur_positon.y+origin.y;
			return Vec2(x,y);
		}
		else
		{
			//在右侧中间显示
			int x= 380+cur_positon.x+cur_size.width+origin.x+(winSize.width-designResolutionSize.width)/2+GameView::getInstance()->pacPageView->getPositionX();
			int y = (480-360)/2;
			return Vec2(x,y);
		}
	}
	else
	{
		//在左侧显示
		if(86+cur_positon.y > 360)
		{
			//可以在左下显示
			int x= 380+cur_positon.x-272+origin.x+(winSize.width-designResolutionSize.width)/2+GameView::getInstance()->pacPageView->getPositionX();
			int y = 86+cur_positon.y-360+origin.y;
			return Vec2(x,y);
		}
		else
		{
			//在左侧中间显示
			int x= 380+cur_positon.x-272+origin.x+(winSize.width-designResolutionSize.width)/2+GameView::getInstance()->pacPageView->getPositionX();
			int y = (480-360)/2;
			return Vec2(x,y);
		}

	}

}

// 判断是不是 双击
 bool PackageItem::isDoubleTouch(){
	//static long lastTouchTime=0;
	long thisTouchTime=GameUtils::millisecondNow();
	if(abs(thisTouchTime-lastTouchTime)<250){
		lastTouchTime=0;
		return true;
	}
	else{
		lastTouchTime=GameUtils::millisecondNow();
		return false;
	}
 }

 void PackageItem::setGray( bool isGray,std::string isShowGoodsNum)
 {
	 if (this->getChildByName(MengBan_Black_Name))
	 {
		 this->getChildByName(MengBan_Black_Name)->removeFromParent();
	 }

	 if (isGray)
	 {
		 if (curFolder->quantity() <=1)
		 {
			 Btn_pacItemFrame->setTouchEnabled(false);
		 }

		 auto m_mengban_black = ImageView::create();
		 m_mengban_black->loadTexture("res_ui/mengban_black65.png");
		 m_mengban_black->setScale9Enabled(true);
		 m_mengban_black->setContentSize(Size(83,75));
		 m_mengban_black->setCapInsets(Rect(5,5,1,1));
		 m_mengban_black->setName(MengBan_Black_Name);
		 m_mengban_black->setAnchorPoint(Vec2(0,0));
		 m_mengban_black->setPosition(Vec2(0,0));
		 this->addChild(m_mengban_black);

		 auto image_selectIcon = ImageView::create();
		 image_selectIcon->loadTexture("res_ui/greengou.png");
		 image_selectIcon->setAnchorPoint(Vec2(0.5f,0.5f));
		 image_selectIcon->setPosition(Vec2(m_mengban_black->getContentSize().width/2 + 3,
			 m_mengban_black->getContentSize().height - image_selectIcon->getContentSize().height+2));
		 m_mengban_black->addChild(image_selectIcon);

		 if (strlen(isShowGoodsNum.c_str()) > 0)
		 {
			 auto label_num = Label::createWithTTF(isShowGoodsNum.c_str(), APP_FONT_NAME, 16);
			 label_num->setAnchorPoint(Vec2(0.5f,0.5f));
			 label_num->setPosition(Vec2(m_mengban_black->getContentSize().width/2,label_num->getContentSize().height*3/2));
			 m_mengban_black->addChild(label_num);
		 }
	 }
	 else
	 {
		 Btn_pacItemFrame->setTouchEnabled(true);
	 }
 }

 void PackageItem::setRed( bool isRed )
 {
	 if (this->getChildByName(MengBan_Red_Name))
	 {
		 this->getChildByName(MengBan_Red_Name)->removeFromParent();
	 }

	 if (isRed)
	 {
		 auto m_mengban_red = ImageView::create();
		 m_mengban_red->loadTexture("res_ui/mengban_red55.png");
		 m_mengban_red->setScale9Enabled(true);
		 m_mengban_red->setContentSize(Size(83,75));
		 m_mengban_red->setCapInsets(Rect(5,5,1,1));
		 m_mengban_red->setName(MengBan_Red_Name);
		 m_mengban_red->setAnchorPoint(Vec2(0,0));
		 m_mengban_red->setPosition(Vec2(0,0));
		 this->addChild(m_mengban_red);

		 auto l_des = Label::createWithTTF(StringDataManager::getString("packageitem_notAvailable"), APP_FONT_NAME, 18);
		 l_des->setAnchorPoint(Vec2(0.5f,0.5f));
		 l_des->setPosition(Vec2(m_mengban_red->getContentSize().width/2,m_mengban_red->getContentSize().height/2));
		 m_mengban_red->addChild(l_des);
	 }
	 else
	 {
	 }
 }

 void PackageItem::setAvailable(bool isAvailable)
 {
	 if (this->getChildByName(MengBan_Avalible))
	 {
		 this->getChildByName(MengBan_Avalible)->removeFromParent();
	 }

	 if (isAvailable)
	 {
		 Btn_pacItemFrame->setTouchEnabled(true);
	 }
	 else
	 {
		 Btn_pacItemFrame->setTouchEnabled(false);
		 //add mengban
		 auto m_mengban_black = ImageView::create();
		 m_mengban_black->loadTexture("res_ui/mengban_black65.png");
		 m_mengban_black->setScale9Enabled(true);
		 m_mengban_black->setContentSize(Size(83,75));
		 m_mengban_black->setCapInsets(Rect(5,5,1,1));
		 m_mengban_black->setName(MengBan_Avalible);
		 m_mengban_black->setAnchorPoint(Vec2(0,0));
		 m_mengban_black->setPosition(Vec2(0,0));
		 this->addChild(m_mengban_black);
		 /*
		 auto m_able = ImageView::create();
		 m_able->loadTexture("res_ui/font/nono.png");
		 m_able->setAnchorPoint(Vec2(0.5f,0.5f));
		 m_able->setPosition(Vec2(41,37));
		 m_mengban_black->addChild(m_able);

		
		 //add lable
		 auto l_des = Label::create();
		 l_des->setText(StringDataManager::getString("packageitem_notAvailable"));
		 l_des->setFontName(APP_FONT_NAME);
		 l_des->setFontSize(18);
		 l_des->setAnchorPoint(Vec2(0.5f,0.5f));
		 l_des->setPosition(Vec2(41,37));
		 m_mengban_black->addChild(l_des);
		 */

	 }
 }

 void PackageItem::BtnLockEvent(Ref *pSender, Widget::TouchEventType type)
 {

	 switch (type)
	 {
	 case Widget::TouchEventType::BEGAN:
		 break;

	 case Widget::TouchEventType::MOVED:
		 break;

	 case Widget::TouchEventType::ENDED:
	 {
		 if (this->index >= (int)GameView::getInstance()->AllPacItem.size())
		 {
			 // 		 int num = this->index + 1 - GameView::getInstance()->AllPacItem.size();
			 // 		 std::string dialog = "suer to open ";
			 // 		 char * s = new char [5];
			 // 		 sprintf(s,"%d",num);
			 // 		 dialog.append(s);
			 // 		 delete [] s;
			 // 		 dialog.append(" folder ? ");
			 // 		 GameView::getInstance()->showPopupWindow(dialog,2,this,SEL_CallFuncO(PackageItem::SureEvent),SEL_CallFuncO(PackageItem::CancelEvent));
			 int num = this->index + 1 - GameView::getInstance()->AllPacItem.size();
			 GameMessageProcessor::sharedMsgProcessor()->sendReq(1320, (void *)0, (void *)num);
		 }
	 }
	 break;

	 case Widget::TouchEventType::CANCELED:
		 break;

	 default:
		 break;
	 }

 }

 void PackageItem::SureEvent( Ref * pSender )
 {
	 int num =  this->index + 1 - GameView::getInstance()->AllPacItem.size();
	 GameMessageProcessor::sharedMsgProcessor()->sendReq(1320,(void *)0,(void *)num);
 }

 void PackageItem::CancelEvent( Ref * pSender )
 {

 }

 void PackageItem::setNumVisible( bool visible )
 {
	 Lable_num->setVisible(visible);
 }

 void PackageItem::setBoundVisible( bool visible )
 {
	 ImageView_bound->setVisible(visible);
 }

 void PackageItem::setBoundJewelPropertyVisible( bool visible )
 {
	//gem 
	 if (Btn_pacItemFrame->getChildByName("gemPropertyIcon"))
	 {
		 Btn_pacItemFrame->getChildByName("gemPropertyIcon")->setVisible(visible);
	 }
 }
 
 ////////////////
 void PackageItem::registerScriptCommand( int scriptId )
 {
	 mTutorialScriptInstanceId = scriptId;
 }

 void PackageItem::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
 {
	 Size winSize = Director::getInstance()->getWinSize();
	 int _w = (winSize.width-800)/2;
	 int _h = (winSize.height - 480)/2;
// 	 CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,40,60);
// 	 tutorialIndicator->setAnchorPoint(Vec2(0,0));
// 	 tutorialIndicator->setPosition(Vec2(pos.x+ GameView::getInstance()->pacPageView->getPositionX()+_w,
// 													pos.y+130+GameView::getInstance()->pacPageView->getPositionY()+_h));
// 	 tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	 tutorialIndicator->setLocalZOrder(10);
// 	 PackageScene * packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
// 	 packageScene->addChild(tutorialIndicator);

	 auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,83,75);
	 tutorialIndicator->setDrawNodePos(Vec2(pos.x+GameView::getInstance()->pacPageView->getPositionX()+_w +42,
												pos.y+36.5f+GameView::getInstance()->pacPageView->getPositionY()+_h));
	 tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	 auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	 if (mainScene)
	 {
		 mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	 }
 }

 void PackageItem::removeCCTutorialIndicator()
 {
// 	  PackageScene * packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
// 	  if (packageScene != NULL)
// 	  {
// 		  CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(packageScene->getChildByTag(CCTUTORIALINDICATORTAG));
// 		  if(tutorialIndicator != NULL)
// 			  tutorialIndicator->removeFromParent();
// 	  }

	 auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	 if (mainScene)
	 {
		 CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		 if(teachingGuide != NULL)
			 teachingGuide->removeFromParent();
	 }
	
 }


 void PackageItem::BeganToRunCD()
 {
	 //CD
	 if (!curFolder->has_goods())
		 return;

	 if (!CDrug::isDrug(curFolder->goods().id()))
		 return;

	 //CD时间
	 float mCDTime_drug = 0.0f;
	 float mRemainTime_drug = 0.0f;
	 if (DrugManager::getInstance()->getDrugById(curFolder->goods().id()))
	 {
		 auto pDrug = DrugManager::getInstance()->getDrugById(curFolder->goods().id());
		 if (!pDrug)
			 return;

		 mCDTime_drug = pDrug->get_cool_time()/1000.0f;
		 mRemainTime_drug = pDrug->getRemainTime()/1000.0f;
		 //CCLOG("remainTime = %f",mRemainTime_drug);

		 if (mRemainTime_drug <= 0.f)
		 {
			 if (mProgressTimer_skill->isVisible())
				 mProgressTimer_skill->setVisible(false);

			 return;
		 }

		 mProgressTimer_skill->setVisible(true);

		 auto action_progress_from_to = ProgressFromTo::create(mRemainTime_drug , mRemainTime_drug/mCDTime_drug*100, 0);     
		 auto action_callback = CallFuncN::create(CC_CALLBACK_1(PackageItem::drugCoolDownCallBack,this));
		 mProgressTimer_skill->runAction(Sequence::create(action_progress_from_to, action_callback, NULL));
	 }
 }

 void PackageItem::drugCoolDownCallBack( Node* node )
 {
	 mProgressTimer_skill->setVisible(false);
 }

 float PackageItem::getCurProgress()
 {
	 return mProgressTimer_skill->getPercentage();
 }

 std::string PackageItem::getCurGemIconPath( std::string goodsId )
 {
	auto gemMsg  = gempropertyConfig::s_GemPropertyMsgData[goodsId];
	int gemClazz = gemMsg->get_propClazz();
	std::string gemIconpath = "res_ui/font/property/";
	switch(gemClazz)
	{
	case 1:
		{
			gemIconpath.append("strength");
		}break;
	case 2:
		{
			gemIconpath.append("dexterity");
		}break;
	case 3:
		{
			gemIconpath.append("intelligence");
		}break;
	case 4:
		{
			gemIconpath.append("focus");
		}break;
	case 11:
		{
			gemIconpath.append("hp_capacity");
		}break;
	case 12:
		{
			gemIconpath.append("mp_capacity");	
		}break;
	case 13:
		{
			gemIconpath.append("min_attack");
		}break;
	case 14:
		{
			gemIconpath.append("max_attack");
		}break;
	case 15:
		{
			gemIconpath.append("min_magic_attack");
		}break;
	case 16:
		{
			gemIconpath.append("max_magic_attack");
		}break;
	case 17:
		{
			gemIconpath.append("min_defend");
		}break;
	case 18:
		{
			gemIconpath.append("max_defend");
		}break;
	case 19:
		{
			gemIconpath.append("min_magic_defend");
		}break;
	case 20:
		{
			gemIconpath.append("max_magic_defend");
		}break;
	case 21:
		{
			gemIconpath.append("hit");
		}break;
	case 22:
		{
			gemIconpath.append("dodge");
		}break;
	case 23:
		{
			gemIconpath.append("crit");
		}break;
	case 24:
		{
			gemIconpath.append("crit_damage");
		}break;
	case 25:
		{
			gemIconpath.append("attack_speed");
		}break;
	case 26:
		{
			//gemIconpath.append("move_speed");
		}break;
	}
	gemIconpath.append(".png");
	return gemIconpath;
 }
