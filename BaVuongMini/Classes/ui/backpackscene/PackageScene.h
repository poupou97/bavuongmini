#ifndef _PACKAGESCENE_PACKAGESCENE_H
#define _PACKAGESCENE_PACKAGESCENE_H

#include "../extensions/UIScene.h"
#include "PacPageView.h"
#include "../generals_ui/GeneralsListBase.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GameSceneState;
class UITab;
class GoodsInfo;
class FolderInfo;
class CEquipment;
class CGeneralBaseMsg;
class CActiveRole;
class CCRichLabel;
class GameActorAnimation;

class PackageScene : public GeneralsListBase,public TableViewDelegate,public TableViewDataSource
{
friend class MailUI;
public:

	struct IdxAndNum{
		int index;
		int num;
	};

 	std::vector<Coordinate> Coordinate_equip;
 	std::string equipmentItem_name[12];

	int curEquipmentIndex;
	CEquipment * curEquipment;

	int itemNumEveryPage ;


private:
	PackageScene();
public:
    
    ~PackageScene();
    bool init();
	static PackageScene* create();
	//void reloadInit();
	void reCreatePageVeiw();

	void RefreshEquipment(std::vector<CEquipment *>& equipmentList);
	void RefreshRoleAnimation(CActiveRole * activeRole);
	void RefreshRoleInfo(CActiveRole * activeRole,int fightpoint);
	GameActorAnimation* getRoleAnimation();

	virtual void onEnter();
	virtual void onExit();

	void closeBeganEvent(Ref *pSender, Widget::TouchEventType type);
	void closeMovedEvent(Ref *pSender);
    void closeEndedEvent(Ref *pSender);

	void SortEvent(Ref *pSender, Widget::TouchEventType type);
	void GoldStoreEvent(Ref *pSender, Widget::TouchEventType type);
	void TitleEvent(Ref *pSender, Widget::TouchEventType type);
	void AchievementEvent(Ref *pSender, Widget::TouchEventType type);								// ս��

	void RemoteStoreHouseEvent(Ref *pSender, Widget::TouchEventType type);

	void oneKeyAllEquip(Ref *pSender, Widget::TouchEventType type);
		 
 	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
 	virtual void onTouchEnded(Touch *touch, Event * pEvent);
 	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
 	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void menuCloseCallback(Ref* pSender);
	
	virtual void update(float dt);

	void PlayerEvent(Ref* pSender);
	void DateEvent(Ref* pSender);
	void LeftIndexChangedEvent(Ref* pSender);
	void ChangeLeftPanelByIndex(int index);

	void CurrentPageViewChanged(Ref *pSender, PageView::EventType type);

	//void ReloadData();
	//void ReloadOnePacItem(FolderInfo* folderInfo);
	void sdEvent(Ref *pSender);

	void ReloadProperty();
	void ReloadInfo();

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void ReloadGeneralsTableView();
	void ReloadGeneralsTableViewWithoutChangeOffSet();

	void RefreshGeneralLevelInTableView(long long generalId);

	void PageScrollToDefault();

	//
	void setPresentVoidEquipItem(int part,bool isPresent);

	//�һ���������Խ��
	void FirstGradeDesEvent(Ref *pSender, Widget::TouchEventType type);
	void SecondGradeDesEvent(Ref *pSender, Widget::TouchEventType type);

public:
	Button * Button_close;
	//ܽ�ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();
	//��ѧ
	int mTutorialScriptInstanceId;
public:
	Layout *ppanel;
	Layer *u_layer;
	Layer * playerLayer;
	Layer *tempLayer;

	MenuItemImage* playerItemImage;
	MenuItemImage* dataItemImage;

	PacPageView * pacPageView ;

	UITab * leftTab ;
	UITab * rightTab ;

	Layout * playerPanel;
	Layout * dataPanel;
	Layout * infoPanle;

	ImageView * currentPage;

	Layout * allGoodsPanel;
	Layout * equipmentPanel;
	Layout * normalGoods;

	Vec2 a;

	//std::vector<CGeneralBaseMsg *>generalBaseMsgList;
	TableView * generalList_tableView;
	CGeneralBaseMsg * curGeneralBaseMsg;

	//��ǰѡ�еActorID
	long long selectActorId;
	//cur select general equip part
	int selectGeneralEquipPart;
	//cur select general equip stalevel
	int selectGeneralEquipStarLevel;
	//cur select tabview of index
	int selectTabviewIndex;


	float m_remainArrangePac;
private:
	//�������
	Text * Label_roleName;
	//��佫���
	CCRichLabel * l_generalName;
	Text * l_roleLevel;
	ImageView * imageView_professionName;
	ImageView * imageView_country;
	Text * l_fightPoint;
	Label * l_allFightPoint;

	/**************����***************/
	Text * l_nameValue;
	Text * l_levelValue;
	Text * l_powerValue;
	Text * l_aglieValue;  //���
	Text * l_intelligenceValue;  //����
	Text * l_focusValue;  //רע
	Text * l_curHpValue;
	Text * l_allHpValue;
	ImageView * imageView_hp;
	Text * l_curMpValue;
	Text * l_allMpValue;
	ImageView * imageView_mp;
	Text * l_phyAttackValue;
	Text * l_magicAttackValue;
	Text * l_phyDenValue;
	Text * l_magicDenValue;
	Text * l_hitValue;  //���
	Text * l_dodgeValue; //���
	Text * l_critValue;  //ܱ��
	Text * l_critDamageValue;//���˺�
	Text * l_atkSpeedValue;
	Text * l_moveSpeedValue;

	
	/**************��Ϣ***************/
	Text * l_roleNameValue;  //���
	Text * l_roleLvValue;  //ֵȼ�
	Text * l_jobValue;  //ְҵ
	Text * l_genderValue;  //�Ա
	Text * l_countryValue;   //��
	Text * l_titleValue;   //ҳƺ
	Text * l_factionValue;   //Ű��
	Text * l_honorValue;  //����
	Text * l_familyValue;  //���
	Text * l_battleAchValue;		// �ս��
	//Label * l_experienceValue;  //���
	Text * l_physicalValue;  //���
	Text * l_expValue;
	ImageView * imageView_exp; //�����

	Text *Label_gold;
	Text *Label_goldIngot;
	Text *Label_bingGoldIngot;

	//equipment
	ImageView * ImageView_wuqi;
	ImageView * ImageView_toukui;
	ImageView * ImageView_yifu;
	ImageView * ImageView_xiezi;
	ImageView * ImageView_jiezhi;
	ImageView * ImageView_peishi;
};


/////////////////////////////////
/**
 * ��佫Сͷ�
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.17
 */
class CGeneralBaseMsg;

class GeneralsSmallHeadCell : public TableViewCell
{
public:
	GeneralsSmallHeadCell();
	~GeneralsSmallHeadCell();
	static GeneralsSmallHeadCell* create(CGeneralBaseMsg * generalBaseMsg);
	bool init(CGeneralBaseMsg * generalBaseMsg);

	CGeneralBaseMsg* getCurGeneralBaseMsg();

private:
	CGeneralBaseMsg * curGeneralBaseMsg; 
};

#endif
