#ifndef _H_REISTERNOTICEUI_H_
#define _H_REISTERNOTICEUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class RegisterNoticeui:public UIScene
{
public:
	RegisterNoticeui(void);
	~RegisterNoticeui(void);

	static RegisterNoticeui * create();
	bool init();
	void onEnter();
	void onExit();
	
	void callBackExit(Ref *pSender, Widget::TouchEventType type);

};
#endif;

