#include "RegisterNoticeui.h"
#include "../extensions/CCRichLabel.h"
#include "GameView.h"
#include "cocostudio\CCSGUIReader.h"


RegisterNoticeui::RegisterNoticeui(void)
{
}


RegisterNoticeui::~RegisterNoticeui(void)
{
}

RegisterNoticeui * RegisterNoticeui::create()
{
	auto resisterui = new RegisterNoticeui();
	if (resisterui && resisterui->init())
	{
		resisterui->autorelease();
		return resisterui;
	}
	CC_SAFE_DELETE(resisterui);
	return NULL;
}

bool RegisterNoticeui::init()
{
	if (UIScene::init())
	{
		Size winsize=Director::getInstance()->getVisibleSize();

		auto layer_ = Layer::create();
		layer_->setIgnoreAnchorPointForPosition(false);
		layer_->setAnchorPoint(Vec2(0.5f,0.5f));
		layer_->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layer_->setContentSize(Size(800,480));
		this->addChild(layer_);
		/*
		ImageView * backGround = ImageView::create();
		backGround->loadTexture("res_ui/zhezhao01.png");
		backGround->setAnchorPoint(Vec2(0.5f,0.5f));
		backGround->setScale9Enabled(true);
		backGround->setCapInsets(Rect(51,51,1,1));
		backGround->setContentSize(Size(500,300));
		backGround->setPosition(Vec2(layer_->getContentSize().width/2,layer_->getContentSize().height/2));
		layer_->addChild(backGround);

		ImageView * logSp = ImageView::create();
		logSp->loadTexture("res_ui/dlgg.png");
		logSp->setAnchorPoint(Vec2(0.5f,0.5f));
		logSp->setPosition(Vec2(0,backGround->getContentSize().height/2 - 20));
		backGround->addChild(logSp);

		Button * button_close= Button::create();
		button_close->loadTextures("res_ui/close.png","res_ui/close.png","");
		button_close->setAnchorPoint(Vec2(0.5f,0.5f));
		button_close->setPosition(Vec2(backGround->getContentSize().width/2-3,backGround->getContentSize().height/2-3));
		button_close->setPressedActionEnabled(true);
		button_close->setTouchEnabled(true);
		button_close->addTouchEventListener(CC_CALLBACK_2(RegisterNoticeui::callBackExit));
		backGround->addChild(button_close);
		*/

		auto ppanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/loginbulletin_1.json");
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->setPosition(Vec2(layer_->getContentSize().width/2,layer_->getContentSize().height/2));
		layer_->addChild(ppanel);

		auto button_close=(Button *)Helper::seekWidgetByName(ppanel,"Button_close");
		button_close->setPressedActionEnabled(true);
		button_close->setTouchEnabled(true);
		button_close->addTouchEventListener(CC_CALLBACK_2(RegisterNoticeui::callBackExit, this));

		auto scrollView = cocos2d::extension::ScrollView::create(Size(460,280));
		scrollView->setContentSize(Size(460,280));
		scrollView->setIgnoreAnchorPointForPosition(false);
		scrollView->setTouchEnabled(true);
		scrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		scrollView->setAnchorPoint(Vec2(0.5f,0.5f));
		scrollView->setPosition(Vec2(layer_->getContentSize().width/2,layer_->getContentSize().height/2 - 25));
		scrollView->setBounceable(true);
		scrollView->setClippingToBounds(true);
		layer_->addChild(scrollView);	

		std::string strings_ = GameView::getInstance()->registerNoticeVector.at(0);
		auto labelLink=CCRichLabel::createWithString(strings_.c_str(),Size(460,250),NULL,NULL,0,20,2);
		labelLink->setAnchorPoint(Vec2(0,1));		
		scrollView->addChild(labelLink,1);
		if (labelLink->getContentSize().height > scrollView->getContentSize().height)
		{
			scrollView->setContentSize(Size(460,labelLink->getContentSize().height));
			scrollView->setContentOffset(Vec2(0,280 - labelLink->getContentSize().height ));
		}
		labelLink->setPosition(Vec2(5,scrollView->getContentSize().height - labelLink->getContentSize().height));
		//setTouchEnabled(true);
		//setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		setContentSize(winsize);
		
		return true;
	}
	return false;
}

void RegisterNoticeui::onEnter()
{
	UIScene::onEnter();
}

void RegisterNoticeui::onExit()
{
	UIScene::onExit();
}

void RegisterNoticeui::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}
