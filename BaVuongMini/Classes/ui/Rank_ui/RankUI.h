#ifndef _UI_RANK_RANKUI_H_
#define _UI_RANK_RANKUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../extensions/UITab.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ���аUI
 * @author liuliang
 * @version 0.1.0
 * @date 2014.04.08
 */

#define  TAG_SCROLLVIEW 50

class CRoleMessage;
class CHomeMessage;
class ControlTree;
class RankUI:public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	RankUI(void);
	~RankUI(void);

public:
	static Layout * s_pPanel;
	static RankUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//�������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);


	//add scrolle tree by liuzhenxing
	ControlTree * tree_;
	void callBackTreeEvent(Ref * obj);
	void callBackSecondTreeEvent(Ref * obj);

	void refreshDataByTreeIndex();
	void refreshDataBySecondTreeIndex(int index);
private:
	Layer * m_base_layer;																							// ��layer
	//TableView * m_leftView;																						// ���tableView
	TableView * m_dataView;																						// ���tableView

	UITab * m_tab_pro;																							// professsion_tab
	UITab * m_tab_country;																						// country_tab
																													

	Layout * m_panel_level;																					// ݵȼ�
	Layout * m_panel_combat;																					// ս���
	Layout * m_panel_coin;																						// ��
	Layout * m_panel_family;																					// Ҽ��
	Layout * m_panel_battleAch;																				// �ս��
	Layout * m_panel_flower;																					// �ʻ�
	

	Text * m_label_myRankLabel;																				// �ҵ����label
	Text * m_label_myRankNum;																				// ��ҵ����num
	Text * m_label_myRankInfo;																				// �û���ϰ񣬼���Ŷ���������ʻ��

	std::vector<std::string > m_vector_tableView;

	bool m_bHasReqInternet;																							// ��Ƿ��Ѿ���������������

public:
	void set_hasReqInternet(bool bFlag);
	bool get_hasReqInternet();

	Layer* getBaseLayer();

private:
	void initUI();
	void initTimeFlag();				
	void reloadTimeFlag(int nRankType, int nProfession);
	void reloadTimeFlag();

	void judgeTimeFlag();

public:
	void callBackBtnClose(Ref *pSender, Widget::TouchEventType type);
	void callBackBtnFamilyInfo(Ref * pSender);

	void proTypeTabIndexChangedEvent(Ref * pSender);
	void countryTypeTabIndexChangedEvent(Ref * pSender);
	void flowerTypeTabIndexChagedEvent(Ref * pSender);

	void reloadDataTableView();																						// ݸ�� scrollView data

	void initDataFromDb(std::vector<CRoleMessage *> vector_roleMessage);				// ´dbӻ�ȡ��ݵ�m_vector_rankPlayer
	void initDataFromDb(std::vector<CHomeMessage *> vector_familyMessage);		// �dbӻ�ȡ��ݵ�m_vector_family

	void refreshDataTableView();																						// ˢ�dataTableView
	void refreshDataTableViewWithChangeOffSet();															// �ˢ�dataTableView£���ƫ�ƣ�

	void reloadMyRankLabel(int nRankItemType);											// ��� ����е����ͣ�ְҵ����ң�

	std::string getProfession(int nProfession);
	std::string getCountry(int nCountry);

	enum
	{
		RANKITEMTYPE_PROFESSION = 0,													// ְҵ	
		RANKITEMTYPE_COUNTRY,															// ��
		RANKITEMTYPE_FLOWER,															// ��ʻ�
	};
};

struct RankDataStruct
{
	int nType;																												//������ 0:�ȼ�;1:ս�����2:��ң�3:ս����4�����壻5���ʻ�
	int nWork;																												//ְҵ 0.ȫ����1.�ͽ���2.��ı��3.��ܡ�4.���䡢
	int nPage;																												//�ڼ�ҳ
};

struct RankPlayerStruct
{
	int nRank;																												// ���
	std::string strName;																								// ���
	int nMount;																											// Ƶȼ� or ս��� or �� or ��ʻ�
	std::string strProfession;																						// ְҵ
	std::string strCountry;																							// ��
	int nCountry;
	int nVipLevel;                                                                                                        // VIPҵȼ�
	int nLevel;
	int nFlowerType;

	long long longPlayerId;																							// ��ݵid
	int nPageCur;
	int nPageAll;
	int nMyPlayerRank;
	long long longMyPlayerId;																						// ĵ�½�˺ŵid
	
};

struct RankFamilyStruct
{
	int nRank;																												// ����
	std::string strFamilyName;																					// ������
	int nLevel;																												// Ƶȼ�
	int nFamilyMember;																								// ���
	int nFamilyMemberMax;																							// �����������
	std::string strCountry;																							// ޹�

	int nPageCur;
	int nPageAll;
	int nMyRank;

	long long longFamilyId;
	std::string strCreaterName;
	std::string strAnnouncement;
};

#endif

