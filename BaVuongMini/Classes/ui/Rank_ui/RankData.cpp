#include "RankData.h"
#include "../../messageclient/element/CRoleMessage.h"
#include "../../messageclient/element/CHomeMessage.h"


RankData * RankData::s_rankData = NULL;

RankData::RankData(void)
	:m_longTimeFlag(-1)
	,m_nRankType(0)
	,m_nProOrCountry(0)
	,m_longTimeRefreshFlag(-1)
{

}


RankData::~RankData(void)
{
	this->clearVectorPlayerInternet();
	this->clearVectorPlayerDb();
	this->clearVectorFamilyInternet();
	this->clearVectorFamilyDb();

	m_nRankType = 0;
	m_nProOrCountry = 0;
	m_longTimeFlag = -1;
	m_longTimeRefreshFlag = -1;
}

RankData * RankData::instance()
{
	if (NULL == s_rankData)
	{
		s_rankData = new RankData();
	}

	return s_rankData;
}

void RankData::clearVectorPlayerInternet()
{
	std::vector<CRoleMessage *>::iterator iter;
	for (iter = m_vector_player_internet.begin(); iter != m_vector_player_internet.end(); iter++)
	{
		delete *iter;
	}
	m_vector_player_internet.clear();
}

void RankData::clearVectorFamilyInternet()
{
	std::vector<CHomeMessage *>::iterator iter;
	for (iter = m_vector_family_internet.begin(); iter != m_vector_family_internet.end(); iter++)
	{
		delete *iter;
	}
	m_vector_family_internet.clear();
}

void RankData::set_timeFlag( long long longTimeFlag )
{
	this->m_longTimeFlag = longTimeFlag;
}

long long RankData::get_timeFlag()
{
	return this->m_longTimeFlag;
}

void RankData::clearVectorPlayerDb()
{
	std::vector<CRoleMessage *>::iterator iter;
	for (iter = m_vector_player_db.begin(); iter != m_vector_player_db.end(); iter++)
	{
		delete *iter;
	}
	m_vector_player_db.clear();
}

void RankData::clearVectorFamilyDb()
{
	std::vector<CHomeMessage *>::iterator iter;
	for (iter = m_vector_family_db.begin(); iter != m_vector_family_db.end(); iter++)
	{
		delete *iter;
	}
	m_vector_family_db.clear();
}

void RankData::clearAllData()
{
	this->clearVectorPlayerInternet();
	this->clearVectorPlayerDb();
	this->clearVectorFamilyInternet();
	this->clearVectorFamilyDb();

	m_nRankType = 0;
	m_nProOrCountry = 0;
	m_longTimeFlag = -1;
	//m_longTimeRefreshFlag = -1;
}

void RankData::set_rankType( int nRankType )
{
	this->m_nRankType = nRankType;
}

int RankData::get_rankType()
{
	return this->m_nRankType;
}

void RankData::set_proOrCountry( int nProfession )
{
	this->m_nProOrCountry = nProfession;
}

int RankData::get_proOrCountry()
{
	return this->m_nProOrCountry;
}

void RankData::set_timeRefreshFlag( long long longTimeRefreshFlag )
{
	this->m_longTimeRefreshFlag = longTimeRefreshFlag;
}

long long RankData::get_timeRefreshFlag()
{
	return this->m_longTimeRefreshFlag;
}





