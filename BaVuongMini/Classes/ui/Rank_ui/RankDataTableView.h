#ifndef _UI_RANK_RANKDATATABLEVIEW_H_
#define _UI_RANK_RANKDATATABLEVIEW_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ���а�ģ�飨tableView��
 * @author liuliang
 * @version 0.1.0
 * @date 2014.04.09
 */
struct RankPlayerStruct;
struct RankFamilyStruct;

class RankDataTableView:public TableViewDataSource, public TableViewDelegate
{
public:
	static RankDataTableView * s_rankDataTableView;
	static RankDataTableView * instance();

private:
	RankDataTableView(void);
	~RankDataTableView(void);

public:
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

public:
	std::vector<RankPlayerStruct *> m_vector_rankPlayer;
	std::vector<RankFamilyStruct *> m_vector_rankFamily;

	void clearVectorRankPlayer();
	void clearVectorRankFamily();

	void refreshDataTableStatus( int nPageCur, int nPageAll);
	Color3B getFontColor(int nRank);

	void setSelectItemDefault();

private:
	void initCell(TableViewCell * cell, unsigned int nIndex, int nRankType);

	int m_nPageCur;
	int m_nPageAll;
	int m_nPageStatus;

	int m_nLastSelectCellId;
	int m_nSelectCellId;

public:
	void set_pageCur(int nPageCur);
	int get_pageCur();

	void set_pageAll(int nPageAll);
	int get_pageAll();

	// dataTable��Ƿ�����һҳ����һҳ
	enum DataTableStatus
	{
		TableHaveLast = 0,					// ��һҳ
		TableHaveNext,
		TableHaveBoth,
		TableHaveNone,
	};

private:
	Sprite* setRankPic(float positionX, float positionY, int nRank);
};

#endif

