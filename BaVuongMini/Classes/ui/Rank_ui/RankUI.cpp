#include "RankUI.h"
#include "SimpleAudioEngine.h"
#include "../../GameAudio.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../../AppMacros.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CRoleMessage.h"
#include "RankData.h"
#include "RankDataTableView.h"
#include "../../messageclient/element/CHomeMessage.h"
#include "RankDb.h"
#include "../../utils/GameUtils.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/family_ui/ApplyFamily.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../ui/extensions/ControlTree.h"

using namespace CocosDenshion;

Layout * RankUI::s_pPanel;

const Size SizeCellSize = Size(128, 44);
const Size SizeTableView = Size(135, 390);
const Size SizeSecondTree = Size(130,38);

const Vec2 Vec2TableView = Vec2(77, 43);

const Size SizeDataTableViewGeneral = Size(530, 288);
const Vec2 Vec2DataTableView = Vec2(222, 75);

const Size SizeDataTableViewFamily = Size(530, 330);

const Vec2 Vec2ProTypeTab = Vec2(208, 407);

/////////////////////////////////////////////////////////////////////////////////////////////

RankUI::RankUI(void)
	:m_base_layer(NULL)
	//,m_leftView(NULL)
	,m_dataView(NULL)
	,m_tab_pro(NULL)
	,m_panel_level(NULL)
	,m_panel_combat(NULL)
	,m_panel_coin(NULL)
	,m_panel_family(NULL)
	,m_panel_battleAch(NULL)
	,m_panel_flower(NULL)
	,m_label_myRankLabel(NULL)
	,m_label_myRankNum(NULL)
	,m_label_myRankInfo(NULL)
	,m_tab_country(NULL)
{

}


RankUI::~RankUI(void)
{
	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		m_dataView->removeFromParent();
	}

	RankDataTableView::instance()->clearVectorRankPlayer();
	RankDataTableView::instance()->clearVectorRankFamily();

	RankData::instance()->clearAllData();


}

RankUI* RankUI::create()
{
	auto rankUI = new RankUI();
	if (rankUI && rankUI->init())
	{
		rankUI->autorelease();
		return rankUI;
	}
	CC_SAFE_DELETE(rankUI);
	return NULL;
}

bool RankUI::init()
{
	if (UIScene::init())
	{
		auto winSize = Director::getInstance()->getVisibleSize();

		m_base_layer = Layer::create();
		m_base_layer->setIgnoreAnchorPointForPosition(false);
		m_base_layer->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_base_layer->setContentSize(Size(800, 480));
		m_base_layer->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		this->addChild(m_base_layer);

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winSize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0, 0));
		m_pLayer->addChild(mengban);

		if(LoadSceneLayer::rankLayout->getParent() != NULL)
		{
			LoadSceneLayer::rankLayout->removeFromParentAndCleanup(false);
		}

		s_pPanel = LoadSceneLayer::rankLayout;
		s_pPanel->setAnchorPoint(Vec2(0.5f, 0.5f));
		s_pPanel->getVirtualRenderer()->setContentSize(Size(800, 480));
		s_pPanel->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		s_pPanel->setScale(1.0f);
		s_pPanel->setTouchEnabled(true);
		m_pLayer->addChild(s_pPanel);

		// ��ʼ�����(��й��)
		const char * firstStr = StringDataManager::getString("UIName_rank_pai");
		const char * secondStr = StringDataManager::getString("UIName_rank_hang");
		const char * thirdStr = StringDataManager::getString("UIName_rank_bang");
		auto atmature = MainScene::createPendantAnm(firstStr, secondStr, thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		m_base_layer->addChild(atmature);

		// ��ʼ�� vector_tableView
		const char *str1 = StringDataManager::getString("rank_level");
		char* p1 = const_cast<char*>(str1);
		m_vector_tableView.push_back(p1);

		const char *str2 = StringDataManager::getString("rank_capacity");
		char* p2 = const_cast<char*>(str2);
		m_vector_tableView.push_back(p2);

		const char *str3 = StringDataManager::getString("rank_coin");
		char* p3 = const_cast<char*>(str3);
		m_vector_tableView.push_back(p3);

		const char *str4 = StringDataManager::getString("rank_battleAchievement");
		char* p4 = const_cast<char*>(str4);
		m_vector_tableView.push_back(p4);

		const char *str5 = StringDataManager::getString("rank_family");
		char* p5 = const_cast<char*>(str5);
		m_vector_tableView.push_back(p5);

		const char *str6 = StringDataManager::getString("rank_flower");
		char* p6 = const_cast<char*>(str6);
		m_vector_tableView.push_back(p6);

		// ��ʼ��UI
		initUI();

		// ȡ��ʱ��
		initTimeFlag();

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winSize);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(RankUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(RankUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(RankUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(RankUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void RankUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_OPEN, false);
}

void RankUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

bool RankUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void RankUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void RankUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void RankUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void RankUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void RankUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void RankUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	RankData::instance()->set_rankType(cell->getIdx());

	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		RankDataTableView::instance()->clearVectorRankPlayer();
		RankDataTableView::instance()->clearVectorRankFamily();

		m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	// �ÿ�ε�����itemģ���Ҫ������ѡitem�index�Ϊdefault
	RankDataTableView::instance()->setSelectItemDefault();

	// 0:�ȼ�;1:ս�����2:��ң�3:ս����4�����壻5���ʻ�
	switch (RankData::instance()->get_rankType())
	{
	case 0:
		{
			m_panel_level->setVisible(true);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_family->setVisible(false);
			m_panel_battleAch->setVisible(false);

			//m_tab_pro->setVisible(true);
			//m_tab_country->setVisible(false);

			//m_tab_pro->setDefaultPanelByIndex(0);
			RankData::instance()->set_proOrCountry(0);

			// ����ʱ��
			reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 1:
		{
			m_panel_combat->setVisible(true);
			m_panel_level->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_family->setVisible(false);
			m_panel_battleAch->setVisible(false);

			//m_tab_pro->setVisible(true);
			//m_tab_country->setVisible(false);

			//m_tab_pro->setDefaultPanelByIndex(0);
			RankData::instance()->set_proOrCountry(0);

			// ����ʱ��
			reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 2:
		{
			m_panel_coin->setVisible(true);
			m_panel_family->setVisible(false);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_battleAch->setVisible(false);

			//m_tab_pro->setVisible(false);
			//m_tab_country->setVisible(false);

			//m_tab_pro->setDefaultPanelByIndex(0);
			RankData::instance()->set_proOrCountry(0);

			// ����ʱ��
			reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 3:
		{
			m_panel_battleAch->setVisible(true);
			m_panel_family->setVisible(false);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);

			//m_tab_country->setVisible(true);
			//m_tab_pro->setVisible(false);

			//m_tab_country->setDefaultPanelByIndex(0);
			RankData::instance()->set_proOrCountry(0);

			// ����ʱ��
			reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 4:
		{
			m_panel_family->setVisible(true);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_battleAch->setVisible(false);

			//m_tab_pro->setVisible(false);
			//m_tab_country->setVisible(false);

			// ����ʱ��
			reloadTimeFlag();
		}
		break;
	}
}

cocos2d::Size RankUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return SizeCellSize;
}

cocos2d::extension::TableViewCell* RankUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();

	if (idx < m_vector_tableView.size())
	{
		// ��ͼ
		auto sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
		sprite_frame->setContentSize(Size(SizeCellSize.width - 10, SizeCellSize.height - 2));
		sprite_frame->setCapInsets(Rect(15, 12, 5, 49));
		sprite_frame->setAnchorPoint(Vec2::ZERO);
		sprite_frame->setPosition(Vec2(5, 1));
		cell->addChild(sprite_frame);

		std::string str_rankName =	 m_vector_tableView.at(idx);
		auto label_rankName = Label::createWithTTF(str_rankName.c_str(), APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::CENTER);
		label_rankName->setColor(Color3B(255, 255, 255));
		auto color = Color4B::BLACK;
		label_rankName->enableShadow(color, Size(1.0f, -1.0f), 1.0f);
		label_rankName->setAnchorPoint(Vec2(0.5f, 0.5f));
		label_rankName->setHorizontalAlignment(TextHAlignment::CENTER);
		label_rankName->setPosition(Vec2(sprite_frame->getContentSize().width / 2 + sprite_frame->getPositionX(), sprite_frame->getContentSize().height / 2 + sprite_frame->getPositionY()));

		cell->addChild(label_rankName);
	}

	return cell;
}

ssize_t RankUI::numberOfCellsInTableView( TableView *table )
{
	return m_vector_tableView.size();
}

void RankUI::initUI()
{
	// close btn
	auto pBtnClose = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnabled(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addTouchEventListener(CC_CALLBACK_2(RankUI::callBackBtnClose, this));

	// ui panel ��ȡ
	m_panel_level = (Layout *)Helper::seekWidgetByName(s_pPanel, "Panel_lv");
	m_panel_level->setVisible(true);

	m_panel_combat = (Layout *)Helper::seekWidgetByName(s_pPanel, "Panel_combat");
	m_panel_combat->setVisible(false);

	m_panel_coin = (Layout *)Helper::seekWidgetByName(s_pPanel, "Panel_coins");
	m_panel_coin->setVisible(false);

	m_panel_family = (Layout *)Helper::seekWidgetByName(s_pPanel, "Panel_family");
	m_panel_family->setVisible(false);

	m_panel_battleAch = (Layout *)Helper::seekWidgetByName(s_pPanel, "Panel_battleAch");
	m_panel_battleAch->setVisible(false);

	m_panel_flower = (Layout *)Helper::seekWidgetByName(s_pPanel, "Panel_flower");
	m_panel_flower->setVisible(false);

	// �ҵ����label
	m_label_myRankLabel = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_myRankLabel");
	m_label_myRankLabel->setVisible(true);

	// ��ҵ����num
	m_label_myRankNum = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_myRankNum");
	m_label_myRankNum->setVisible(true);

	// ��ʻ�������ʾ
	m_label_myRankInfo = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_myRankInfo");
	m_label_myRankInfo->setVisible(false);

	// tab_pro
	const char * normalImage = "res_ui/tab_1_off.png";
	const char * selectImage = "res_ui/tab_1_on.png";
	const char * finalImage = "";
	const char * highLightImage = "res_ui/tab_1_on.png";

	const char *strAll = StringDataManager::getString("rank_all");
	char* pAll = const_cast<char*>(strAll);
	const char *strMengjiang  = StringDataManager::getString("rank_mengjiang");
	char* pMengjiang = const_cast<char*>(strMengjiang);
	const char *strHaojie  = StringDataManager::getString("rank_haojie");
	char* pHaojie = const_cast<char*>(strHaojie);
	const char *strShenshe  = StringDataManager::getString("rank_shenshe");
	char* pShenshe = const_cast<char*>(strShenshe);
	const char *strGuimou  = StringDataManager::getString("rank_guimou");
	char* pGuimou = const_cast<char*>(strGuimou);

	char* pHighLightImage = const_cast<char*>(highLightImage);

	char * mainNames[] ={pAll, pMengjiang, pHaojie, pShenshe, pGuimou};
	/*m_tab_pro = UITab::createWithBMFont(5, normalImage, selectImage, finalImage, mainNames,"res_ui/font/ziti_1.fnt", HORIZONTAL, 2);
	m_tab_pro->setAnchorPoint(Vec2(0, 0));
	m_tab_pro->setPosition(Vec2ProTypeTab);
	m_tab_pro->setHighLightImage(pHighLightImage);
	m_tab_pro->setDefaultPanelByIndex(0);
	m_tab_pro->addIndexChangedEvent(this, coco_indexchangedselector(RankUI::proTypeTabIndexChangedEvent));
	m_tab_pro->setPressedActionEnabled(true);*/
	//m_base_layer->addChild(m_tab_pro);

	// tab_country
	const char *strWeiGuo = StringDataManager::getString("rank_weiguo");
	char* pWeiGuo = const_cast<char*>(strWeiGuo);
	const char *strShuGuo = StringDataManager::getString("rank_shuguo");
	char* pShuGuo = const_cast<char*>(strShuGuo);
	const char *strWuGuo = StringDataManager::getString("rank_wuguo");
	char* pWuGuo = const_cast<char*>(strWuGuo);

	char * countryNames[] ={pAll, pWeiGuo, pShuGuo, pWuGuo};
	/*m_tab_country = UITab::createWithBMFont(4, normalImage, selectImage, finalImage, countryNames,"res_ui/font/ziti_1.fnt", HORIZONTAL, 2);
	m_tab_country->setAnchorPoint(Vec2(0, 0));
	m_tab_country->setPosition(Vec2ProTypeTab);
	m_tab_country->setHighLightImage(pHighLightImage);
	m_tab_country->setDefaultPanelByIndex(0);
	m_tab_country->addIndexChangedEvent(this, coco_indexchangedselector(RankUI::countryTypeTabIndexChangedEvent));
	m_tab_country->setPressedActionEnabled(true);*/
	//m_base_layer->addChild(m_tab_country);

	// tab_flower
	const char * strZuoRiShouHua = StringDataManager::getString("rank_zuorishouhua");
	char * pZuoRiShouHua = const_cast<char*>(strZuoRiShouHua);
	const char * strZuoRiSongHua = StringDataManager::getString("rank_zuorisonghua");
	char * pZuoRiSongHua = const_cast<char*>(strZuoRiSongHua);
	const char * strLiShiShouHua = StringDataManager::getString("rank_lishishouhua");
	char * pLiShiShouHua = const_cast<char*>(strLiShiShouHua);
	const char * strLiShiSongHua = StringDataManager::getString("rank_lishisonghua");
	char * pLiShiSongHua = const_cast<char*>(strLiShiSongHua);

	// ��ʾ���е�һ�tab
	//m_tab_pro->setVisible(true);
	//m_tab_country->setVisible(false);

	/*
	// ��tableView
	m_leftView = TableView::create(this, SizeTableView);
	m_leftView ->setSelectedEnable(true);   // �֧��ѡ��״̬����ʾ
	m_leftView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(26, 24, 1, 7), Vec2(0,0));   // ���þŹ���ͼƬ
	m_leftView->setDirection(TableView::Direction::VERTICAL);
	m_leftView->setAnchorPoint(Vec2(0, 0));
	m_leftView->setPosition(Vec2TableView);
	m_leftView->setDelegate(this);
	m_leftView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	m_leftView->setPressedActionEnabled(true);
	m_base_layer->addChild(m_leftView);
	*/
	// ���tableView���
	//m_leftView->reloadData();

	// ݸ��scrollViewData
	this->reloadDataTableView();


	//add scroll tree
	std::vector<TreeStructInfo *> tempTreeVector;
	for (int i = 0;i<m_vector_tableView.size();i++)
	{
		auto treeInfo_ = new TreeStructInfo();
		treeInfo_->setTreeName(m_vector_tableView.at(i));

		const char *str_level = StringDataManager::getString("rank_level");
		if (strcmp(m_vector_tableView.at(i).c_str(),str_level)==0)
		{
			//set first treebranch is index
			treeInfo_->addSecondVector(pAll);
			treeInfo_->addSecondVector(pMengjiang);
			treeInfo_->addSecondVector(pHaojie);
			treeInfo_->addSecondVector(pShenshe);
			treeInfo_->addSecondVector(pGuimou);
		}

		const char *str_capcity = StringDataManager::getString("rank_capacity");
		if (strcmp(m_vector_tableView.at(i).c_str(),str_capcity)==0)
		{
			treeInfo_->addSecondVector(pAll);
			treeInfo_->addSecondVector(pMengjiang);
			treeInfo_->addSecondVector(pHaojie);
			treeInfo_->addSecondVector(pShenshe);
			treeInfo_->addSecondVector(pGuimou);
		}

		const char *str_coin = StringDataManager::getString("rank_coin");
		if (strcmp(m_vector_tableView.at(i).c_str(),str_coin)==0)
		{
			//branch tree is null
			treeInfo_->addSecondVector(pAll);
		}

		const char *str_battleAchievement = StringDataManager::getString("rank_battleAchievement");
		if (strcmp(m_vector_tableView.at(i).c_str(),str_battleAchievement)==0)
		{
			treeInfo_->addSecondVector(pAll);
			treeInfo_->addSecondVector(pWeiGuo);
			treeInfo_->addSecondVector(pShuGuo);
			treeInfo_->addSecondVector(pWuGuo);
		}

		const char *str_family = StringDataManager::getString("rank_family");
		if (strcmp(m_vector_tableView.at(i).c_str(),str_family)==0)
		{
			//branch tree is null
			treeInfo_->addSecondVector(pAll);
		}

		const char *str_flower = StringDataManager::getString("rank_flower");
		if (strcmp(m_vector_tableView.at(i).c_str(),str_flower)==0)
		{
			treeInfo_->addSecondVector(pZuoRiShouHua);
			treeInfo_->addSecondVector(pZuoRiSongHua);
			treeInfo_->addSecondVector(pLiShiShouHua);
			treeInfo_->addSecondVector(pLiShiSongHua);
		}

		tempTreeVector.push_back(treeInfo_);
	}
	 
	tree_ = ControlTree::create(tempTreeVector,SizeTableView,SizeCellSize,SizeSecondTree);
	tree_->setAnchorPoint(Vec2(0.5f,0.5f));
	tree_->setPosition(Vec2(61,43));
	//tree_->addcallBackTreeEvent(this,SEL_CallFuncO(RankUI::callBackTreeEvent));
	tree_->addcallBackSecondTreeEvent(this,SEL_CallFuncO(&RankUI::callBackSecondTreeEvent));
	m_base_layer->addChild(tree_);

}

void RankUI::callBackBtnClose(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void RankUI::proTypeTabIndexChangedEvent( Ref * pSender )
{
	//�ְҵ 0.ȫ����1.�ͽ���2.��ı��3.��ܡ�4.���䡢��Э�鶨�壩
	//ְҵ 0.ȫ����1.�ͽ���2.��ܡ�3.���䡢4.��ı   ��UI��˳�

	switch(((UITab*)pSender)->getCurrentIndex())
	{
	case 0: 
		{
			RankData::instance()->set_proOrCountry(0);
		}
		break;
	case 1:
		{
			RankData::instance()->set_proOrCountry(1);
		}
		break;
	case 2:
		{
			RankData::instance()->set_proOrCountry(3);
		}
		break;
	case 3:
		{
			RankData::instance()->set_proOrCountry(4);
		}
		break;
	case 4:
		{
			RankData::instance()->set_proOrCountry(2);
		}
		break;
	}
	
	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		RankDataTableView::instance()->clearVectorRankPlayer();

		m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	// �ÿ�ε�����itemģ���Ҫ������ѡitem�index�Ϊdefault
	RankDataTableView::instance()->setSelectItemDefault();

	reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
}


void RankUI::countryTypeTabIndexChangedEvent( Ref * pSender )
{
	//ְҵ 0.ȫ����1.κ�2.���3.���

	switch(((UITab*)pSender)->getCurrentIndex())
	{
	case 0: 
		{
			RankData::instance()->set_proOrCountry(0);
		}
		break;
	case 1:
		{
			RankData::instance()->set_proOrCountry(1);
		}
		break;
	case 2:
		{
			RankData::instance()->set_proOrCountry(2);
		}
		break;
	case 3:
		{
			RankData::instance()->set_proOrCountry(3);
		}
		break;
	}

	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		RankDataTableView::instance()->clearVectorRankPlayer();

		m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	// �ÿ�ε�����itemģ���Ҫ������ѡitem�index�Ϊdefault
	RankDataTableView::instance()->setSelectItemDefault();

	reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
}

void RankUI::flowerTypeTabIndexChagedEvent( Ref * pSender )
{
	// �ʻ� 1.�����ջ���2.�����ͻ���3.��ʷ�ջ���4.��ʷ�ͻ�
	
	switch(((UITab*)pSender)->getCurrentIndex())
	{
	case 0: 
		{
			RankData::instance()->set_proOrCountry(1);
		}
		break;
	case 1:
		{
			RankData::instance()->set_proOrCountry(2);
		}
		break;
	case 2:
		{
			RankData::instance()->set_proOrCountry(3);
		}
		break;
	case 3:
		{
			RankData::instance()->set_proOrCountry(4);
		}
		break;
	}

	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		RankDataTableView::instance()->clearVectorRankPlayer();

		m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	// ÿ�ε�����itemģ���Ҫ������ѡitem�index�Ϊdefault
	RankDataTableView::instance()->setSelectItemDefault();

	reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
	
}

void RankUI::reloadDataTableView()
{
	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{

	}
	else
	{
		m_dataView = TableView::create(RankDataTableView::instance(), SizeDataTableViewGeneral);
		m_dataView->setTouchEnabled(true);
		//m_dataView->setSelectedScale9Texture("res_ui/mengban_green.png", Rect(7, 7, 1, 1), Vec2(0,0));   // ���þŹ���ͼƬ
		m_dataView->setDirection(TableView::Direction::VERTICAL);
		m_dataView->setAnchorPoint(Vec2(0, 0));
		m_dataView->setPosition(Vec2DataTableView);
		m_dataView->setDelegate(RankDataTableView::instance());
		m_dataView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//m_dataView->setPressedActionEnabled(false);
		m_dataView->setTag(TAG_SCROLLVIEW);

		int nRankType = RankData::instance()->get_rankType();
		if (0 == nRankType || 1 == nRankType)
		{
			m_dataView->setViewSize(SizeDataTableViewFamily);
			m_dataView->setTouchEnabled(true);
		}
		else if (2 == nRankType)
		{
			m_dataView->setViewSize(SizeDataTableViewFamily);
			m_dataView->setTouchEnabled(true);
		}
		else if (3 == nRankType)
		{
			m_dataView->setViewSize(SizeDataTableViewFamily);
			m_dataView->setTouchEnabled(true);
		}
		else if (4 == nRankType)
		{
			m_dataView->setViewSize(SizeDataTableViewFamily);
			m_dataView->setTouchEnabled(false);
		}
		else if (5 == nRankType)
		{
			m_dataView->setViewSize(SizeDataTableViewFamily);
			m_dataView->setTouchEnabled(false);
		}
		

		m_base_layer->addChild(m_dataView);

		refreshDataTableView();
	}


	if (0 == RankData::instance()->get_rankType() || 1 == RankData::instance()->get_rankType() || 2 == RankData::instance()->get_rankType())
	{
		initDataFromDb(RankData::instance()->m_vector_player_db);

		reloadMyRankLabel(RANKITEMTYPE_PROFESSION);
	}
	else if (3 == RankData::instance()->get_rankType())
	{
		initDataFromDb(RankData::instance()->m_vector_player_db);

		reloadMyRankLabel(RANKITEMTYPE_COUNTRY);
	}
	else if (4 == RankData::instance()->get_rankType())
	{
		initDataFromDb(RankData::instance()->m_vector_family_db);

		reloadMyRankLabel(RANKITEMTYPE_PROFESSION);
	}
	else if (5 == RankData::instance()->get_rankType())
	{
		initDataFromDb(RankData::instance()->m_vector_player_db);

		reloadMyRankLabel(RANKITEMTYPE_FLOWER);
	}
}

void RankUI::initDataFromDb( std::vector<CRoleMessage *> vector_roleMessage )
{
	// ��vector 
	RankDataTableView::instance()->clearVectorRankPlayer();

	// db����� ݱ�� 浽 m_vector_rankPlayer�
	int nVectorSize = vector_roleMessage.size();
	for (int i = 0; i < nVectorSize; i++)
	{
		long long longPlayerId = vector_roleMessage.at(i)->id();
		int nRank = vector_roleMessage.at(i)->index();
		std::string  strName = vector_roleMessage.at(i)->name();
		int nMount = vector_roleMessage.at(i)->mount();
		int nProfession = vector_roleMessage.at(i)->profession();
		int nCountry = vector_roleMessage.at(i)->country();
		int nPageCur = vector_roleMessage.at(i)->get_pageCur();
		int nPageAll = vector_roleMessage.at(i)->get_pageAll();
		int nMyRank = vector_roleMessage.at(i)->get_myPlayerRank();
		long long longMyPlayerId = vector_roleMessage.at(i)->get_myPlayerId();
		int nVipLevel = vector_roleMessage.at(i)->viplevel();
		int nLevel = vector_roleMessage.at(i)->level();
		int nFlowerType = vector_roleMessage.at(i)->get_flowerType();

		std::string strProfession = getProfession(nProfession);
		std::string strCountry = getCountry(nCountry);

		RankPlayerStruct * rankPlayerStruct = new RankPlayerStruct();
		rankPlayerStruct->longPlayerId = longPlayerId;
		rankPlayerStruct->nRank = nRank;
		rankPlayerStruct->strName = strName;
		rankPlayerStruct->nMount = nMount;
		rankPlayerStruct->strProfession = strProfession;
		rankPlayerStruct->strCountry = strCountry;
		rankPlayerStruct->nCountry = nCountry;
		rankPlayerStruct->nPageCur = nPageCur;
		rankPlayerStruct->nPageAll = nPageAll;
		rankPlayerStruct->nMyPlayerRank = nMyRank;
		rankPlayerStruct->longMyPlayerId = longMyPlayerId;
		rankPlayerStruct->nVipLevel = nVipLevel;
		rankPlayerStruct->nLevel = nLevel;
		rankPlayerStruct->nFlowerType = nFlowerType;

		RankDataTableView::instance()->m_vector_rankPlayer.push_back(rankPlayerStruct);
	}

	refreshDataTableViewWithChangeOffSet();
}

void RankUI::initDataFromDb( std::vector<CHomeMessage *> vector_familyMessage )
{
	// ���vector(vector_family)
	RankDataTableView::instance()->clearVectorRankFamily();

	// db����� ݱ�� 浽 m_vector_rankFamily�
	int nVectorSize = vector_familyMessage.size();
	for (int i = 0;i < nVectorSize; i++)
	{
		long long longFamilyId = vector_familyMessage.at(i)->id();
		int nRank = vector_familyMessage.at(i)->index();
		std::string strFamilyName = vector_familyMessage.at(i)->name();
		int nLevel = vector_familyMessage.at(i)->level();
		int nFamilyMember = vector_familyMessage.at(i)->number();
		int nFamilyMemberMax = vector_familyMessage.at(i)->numberall();
		int nCountry = vector_familyMessage.at(i)->country();
		int nPageCur = vector_familyMessage.at(i)->get_pageCur();
		int nPageAll = vector_familyMessage.at(i)->get_pageAll();
		int nMyRank = vector_familyMessage.at(i)->get_myRank();
		std::string strCreaterName = vector_familyMessage.at(i)->creatername();
		std::string strAnnouncement = vector_familyMessage.at(i)->announcement();

		std::string strCountry = getCountry(nCountry);

		auto rankFamilyStruct = new RankFamilyStruct();
		rankFamilyStruct->longFamilyId = longFamilyId;
		rankFamilyStruct->nRank = nRank;
		rankFamilyStruct->strFamilyName = strFamilyName;
		rankFamilyStruct->nLevel = nLevel;
		rankFamilyStruct->nFamilyMember = nFamilyMember;
		rankFamilyStruct->nFamilyMemberMax = nFamilyMemberMax;
		rankFamilyStruct->strCountry = strCountry;
		rankFamilyStruct->nPageCur = nPageCur;
		rankFamilyStruct->nPageAll = nPageAll;
		rankFamilyStruct->nMyRank = nMyRank;
		rankFamilyStruct->strCreaterName = strCreaterName;
		rankFamilyStruct->strAnnouncement = strAnnouncement;

		RankDataTableView::instance()->m_vector_rankFamily.push_back(rankFamilyStruct);
	}

	refreshDataTableViewWithChangeOffSet();
}

std::string RankUI::getProfession( int nProfession )
{
	std::string strProfession = "";

	// 0:�ȫ��;1:�ͽ�;2:��ı;3:��;4:����
	switch (nProfession)
	{
	case 0:
		{
			const char *strAll = StringDataManager::getString("rank_all");
			strProfession = strAll;
		}
		break;
	case 1:
		{
			const char *strMengjiang = StringDataManager::getString("rank_mengjiang");
			strProfession = strMengjiang;
		}
		break;
	case 2:
		{
			const char *strGuimou = StringDataManager::getString("rank_guimou");
			strProfession = strGuimou;
		}
		break;
	case 3:
		{
			const char *strHaojie = StringDataManager::getString("rank_haojie");
			strProfession = strHaojie;
		}
		break;
	case 4:
		{
			const char *strShenshe = StringDataManager::getString("rank_shenshe");
			strProfession = strShenshe;
		}
		break;
	default:
		{

		}
	}

	return strProfession;
}

std::string RankUI::getCountry( int nCountry )
{
	// 0:�ȫ�� 1:κ��2���3񣻣��
	const char * charCountry = CMapInfo::getCountryStr(nCountry);

	return charCountry;
}

void RankUI::set_hasReqInternet( bool bFlag )
{
	this->m_bHasReqInternet = bFlag;
}

bool RankUI::get_hasReqInternet()
{
	return this->m_bHasReqInternet;
}

void RankUI::refreshDataTableView()
{
	m_dataView->reloadData();
}

void RankUI::refreshDataTableViewWithChangeOffSet()
{
	Vec2 offSetPoint = m_dataView->getContentOffset();
	int offsetHeight = m_dataView->getContentSize().height + offSetPoint.y;

	m_dataView->reloadData();

	Vec2 temp = Vec2(offSetPoint.x, offsetHeight - m_dataView->getContentSize().height);
	m_dataView->setContentOffset(temp); 
}

void RankUI::initTimeFlag()
{
	// ��жϷ���������ʱ��
	// ��ѯ��ݿ
	std::string str_tableName = "t_rank_player_all";
	long long timeRefreshFlag = RankDb::instance()->checkDbTimeRefreshFlag(str_tableName);

	if (timeRefreshFlag != RankData::instance()->get_timeRefreshFlag())
	{
		// �ʱ���ʧЧ
		RankDb::instance()->timeIsOut();

		return ;
	}
	
	// �ж�ʱ��
	if (-1 == RankData::instance()->get_timeFlag() || -1 == RankData::instance()->get_timeRefreshFlag())
	{
		// ��ѯ��ݿ
		std::string str_tableName = "t_rank_player_all";
		long long timeFlag = RankDb::instance()->checkDbTimeFlag(str_tableName);
		long long timeRefreshFlag = RankDb::instance()->checkDbTimeRefreshFlag(str_tableName);

		// �� timeFlag���0ڣ�˵����ݿ�Ϊ�
		if (0 == timeFlag || 0 == timeRefreshFlag)
		{
			// ���������-----��������������� �ȼ���ȫ�����0�ҳ��
			auto rankDataStruct = new RankDataStruct();
			rankDataStruct->nType = 0;
			rankDataStruct->nWork = 0;
			rankDataStruct->nPage = 0;

			///////////////////////////////////////////////////////////////////////
			/*char str_page[10];
			sprintf(str_page, "%d", rankDataStruct->nPage);
			std::string str_show = "request pageNum:";
			str_show.append(str_page);

			GameView::getInstance()->showAlertDialog(str_show);*/
			//////////////////////////////////////////////////////////////////////

			GameMessageProcessor::sharedMsgProcessor()->sendReq(2300, (void *)rankDataStruct);
		}
		else
		{
			// ݻ�ȡʱ��
			RankData::instance()->set_timeFlag(timeFlag);
			RankData::instance()->set_timeRefreshFlag(timeRefreshFlag);

			// ��ж�ʱ��
			judgeTimeFlag();
		}
	}
	else
	{
		// ��ж�ʱ��
		judgeTimeFlag();
	}
}

void RankUI::reloadTimeFlag( int nRankType, int nProfession )
{
	// tableName
	std::string str_tableName = "";
	if (0 == nProfession && 3 != nRankType)
	{
		// ��ѯ��ݿ
		str_tableName = "t_rank_player_all";
	}
	else if(0 == nProfession && 3 == nRankType)
	{
		str_tableName = "t_rank_country_all";
	}
	else
	{
		str_tableName = "t_rank_player";
	}

	// �db���ȡ��ݵ� vector(��ʱ�vector)
	std::vector<CRoleMessage *> tmpVector;
	RankDb::instance()->getDataFromDb(tmpVector, nRankType, nProfession);
	int nSizeVector = tmpVector.size();

	// ��ж���Ҫ��ʾ������Ƿ�Ϊ�
	if (nSizeVector > 0)
	{
		// ��ж�ʱ����Ƿ���
		// �ʱ�� �� ����ڵ�ʱ��Աȡ���timeFlag ��� milliscondNow()ڣ���1�������ݿ⣨2���رմ��壬���´򿪣�3��return;���򣬶�ȡ��ݿ���
		if (GameUtils::millisecondNow() >= RankData::instance()->get_timeFlag())
		{
			//GameView::getInstance()->showAlertDialog("rankUI reloadTimeFlag timeFlag equals");

			// �ʱ����Ѹ��
			RankDb::instance()->timeIsOut();

			return;
		}
		else
		{
			// ¶�ȡ��ݿ���----ݴdb���ȡ��ݵ� vector(m_vector_player_db)
			RankDb::instance()->getDataFromDb(RankData::instance()->m_vector_player_db, nRankType, nProfession);

			this->reloadDataTableView();
		}
	}
	else
	{
		// ��������-----��������������� �ȼ���ȫ�����0�ҳ��
		auto rankDataStruct = new RankDataStruct();
		rankDataStruct->nType = nRankType;
		rankDataStruct->nWork = nProfession;
		rankDataStruct->nPage = 0;

		///////////////////////////////////////////////////////////////////////////////
		/*char str_page[10];
		sprintf(str_page, "%d", rankDataStruct->nPage);
		std::string str_show = "request pageNum:";
		str_show.append(str_page);

		GameView::getInstance()->showAlertDialog(str_show);*/
		///////////////////////////////////////////////////////////////////////////////

		GameMessageProcessor::sharedMsgProcessor()->sendReq(2300, (void *)rankDataStruct);
	}
}

void RankUI::reloadTimeFlag()
{
	std::string str_tableName= "t_rank_family";
	// ݴdb���ȡ��ݵ� vector(��ʱ�vector)
	std::vector<CHomeMessage *> tmpVector;
	RankDb::instance()->getDataFromDb(tmpVector);
	int nSizeVector = tmpVector.size();

	if (nSizeVector > 0)
	{
		// ��ж�ʱ����Ƿ���
		// �ʱ�� �� ����ڵ�ʱ��Աȡ���timeFlag ��� milliscondNow()ڣ���1�������ݿ⣨2���رմ��壬���´򿪣�3��return;���򣬶�ȡ��ݿ���
		if (GameUtils::millisecondNow() >= RankData::instance()->get_timeFlag())
		{
			//GameView::getInstance()->showAlertDialog("reloadTimeFlag timeFlag equals");

			// �ʱ����Ѹ��
			RankDb::instance()->timeIsOut();

			return;
		}
		else
		{
			// ¶�ȡ��ݿ���----ݴdb���ȡ��ݵ� vector(m_vector_player_db)
			RankDb::instance()->getDataFromDb(RankData::instance()->m_vector_family_db);

			this->reloadDataTableView();
		}
	}
	else
	{
		// ��������-----��������������� �ȼ���ȫ�����0�ҳ��
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2301, (void *)0);
	}
}

Layer* RankUI::getBaseLayer()
{
	return this->m_base_layer;
}

void RankUI::reloadMyRankLabel(int nRankItemType)
{
	if (RANKITEMTYPE_PROFESSION == nRankItemType)
	{
		std::string str_PlayerProfession = GameView::getInstance()->myplayer->getActiveRole()->profession();
		std::string str_ClickProfession = getProfession(RankData::instance()->get_proOrCountry());

		const char* strAll = StringDataManager::getString("rank_all");
		char* pAll = const_cast<char*>(strAll);

		//�˵����ѡ���"�ȫ��" ��� ߶�Ӧ�Լ�ְҵ���佫�ƺ
		if (str_ClickProfession == pAll || str_ClickProfession == str_PlayerProfession)
		{
			m_label_myRankLabel->setVisible(true);
			m_label_myRankNum->setVisible(true);
		}
		else
		{
			m_label_myRankLabel->setVisible(false);
			m_label_myRankNum->setVisible(false);
		}
	}
	else if (RANKITEMTYPE_COUNTRY == nRankItemType)
	{
		std::string str_PlayerCountry = getCountry(GameView::getInstance()->getMapInfo()->country());
		std::string str_ClickCountry = getCountry(RankData::instance()->get_proOrCountry());

		const char* strAll = StringDataManager::getString("rank_all");
		char* pAll = const_cast<char*>(strAll);

		//�˵����ѡ���"�ȫ��" ��� ߶�Ӧ�Լ���
		if (str_ClickCountry == pAll || str_ClickCountry == str_PlayerCountry)
		{
			m_label_myRankLabel->setVisible(true);
			m_label_myRankNum->setVisible(true);
		}
		else
		{
			m_label_myRankLabel->setVisible(false);
			m_label_myRankNum->setVisible(false);
		}
	}
	else if (RANKITEMTYPE_FLOWER == nRankItemType)
	{
		m_label_myRankLabel->setVisible(true);
		m_label_myRankNum->setVisible(true);
	}

	// Ҹ�����
	int nMyRank = 0;

	int nRankType = RankData::instance()->get_rankType();
	if (0 == nRankType || 1 == nRankType || 2 == nRankType || 3 == nRankType || 5 == nRankType)
	{
		const char* char_wodepaihang = StringDataManager::getString("rank_wodepaiming");
		m_label_myRankLabel->setString(char_wodepaihang);

		std::vector<RankPlayerStruct *> vector_rankPlayer = RankDataTableView::instance()->m_vector_rankPlayer;
		if (vector_rankPlayer.size() > 0)
		{
			nMyRank = vector_rankPlayer.at(0)->nMyPlayerRank;
		}
	}
	else if (4 == nRankType)
	{
		const char* char_wodejiazupaihang = StringDataManager::getString("rank_wodejiazupaiming");
		m_label_myRankLabel->setString(char_wodejiazupaihang);

		std::vector<RankFamilyStruct *> vector_rankFamily = RankDataTableView::instance()->m_vector_rankFamily;
		if (vector_rankFamily.size() > 0)
		{
			nMyRank = vector_rankFamily.at(0)->nMyRank;
		}

		m_label_myRankLabel->setVisible(true);
		m_label_myRankNum->setVisible(true);
	}

	std::string str_myRankNum = "";

	m_label_myRankInfo->setVisible(false);

	// ��ҵ�����Ϊ-1��˵� ��¿������а�δˢ�(��ʻ������)⣻Ϊ-2��˵� ���壨δ�����κμ��壩
	if (-1 == nMyRank)
	{
		// 
		if (5 == nRankType)
		{
			m_label_myRankInfo->setVisible(true);

			m_label_myRankLabel->setVisible(false);
			m_label_myRankNum->setVisible(false);
		}
		else
		{
			const char* char_notJoinInRank = StringDataManager::getString("rank_rankNoRefresh");
			str_myRankNum = char_notJoinInRank;
		}
	}
	else if(-2 == nMyRank)
	{
		const char* char_notJoinInFamily = StringDataManager::getString("rank_nojiazu");
		str_myRankNum = char_notJoinInFamily;
	}
	else 
	{
		char str_myRank[20];
		sprintf(str_myRank, "%d", nMyRank);
		str_myRankNum = str_myRank;
	}

	m_label_myRankNum->setString(str_myRankNum.c_str());
	m_label_myRankNum->setPosition(Vec2(m_label_myRankLabel->getPosition().x + m_label_myRankLabel->getContentSize().width, m_label_myRankNum->getPosition().y));
}

void RankUI::judgeTimeFlag()
{
	// �ж�ʱ����Ƿ���
	// �ʱ�� �� ����ڵ�ʱ��Աȡ���timeFlag ��� milliscondNow()ڣ���1�������ݿ⣨2����䵱ǰ�û������nRankTypeĺnProfessionͣ�3��return;���򣬶�ȡ��ݿ���
	if (GameUtils::millisecondNow() >= RankData::instance()->get_timeFlag())
	{
		//GameView::getInstance()->showAlertDialog("rankUI initTimeflag timeFlag equals");

		// �ʱ���ʧЧ
		RankDb::instance()->timeIsOut();
	}
	else
	{
		// �жϸ���ݿ ��Ƿ ��Ӧ ��Ӧ����ң�id � name ͹�ͬȷ�ϣ�
		std::string str_tableName = "t_rank_player_all";
		long long longMyPlayerIdDb = RankDb::instance()->checkMyPlayerId(str_tableName);
		std::string str_myPlayerNameDb = RankDb::instance()->checkMyPlayerName(str_tableName);

		// ��ȡ�û�id
		RoleBase* base = GameView::getInstance()->myplayer->getActiveRole()->mutable_rolebase();
		long long longRoleId = base->roleid();
		std::string str_myPlayerName = base->name();

		if (longMyPlayerIdDb == longRoleId && str_myPlayerNameDb == str_myPlayerName)
		{
			// ��ȡ��ݿ���----ݴdb���ȡ��ݵ� vector(m_vector_player_db)
			RankDb::instance()->getDataFromDb(RankData::instance()->m_vector_player_db, RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());

			this->reloadDataTableView();
		}
		else 
		{
			// �û���ݿⲻһ�£����»�ȡ������Ϊ ʱ���ʧЧ
			RankDb::instance()->timeIsOut();
		}
	}
}

void RankUI::callBackBtnFamilyInfo( Ref * pSender )
{
	auto pMenuItem = (MenuItemSprite *)pSender;
	int nRank = pMenuItem->getTag();

	/*char charId[20];
	sprintf(charId, "%d", nRank);

	GameView::getInstance()->showAlertDialog(charId);*/


	std::vector<RankFamilyStruct *> vector_rankFamily = RankDataTableView::instance()->m_vector_rankFamily;
	int nIndex = nRank - 1;

	std::string str_familyName = vector_rankFamily.at(nIndex)->strFamilyName;
	std::string str_familyCreaterName = vector_rankFamily.at(nIndex)->strCreaterName;
	long long longFamilyId = vector_rankFamily.at(nIndex)->longFamilyId;
	std::string str_announcement = vector_rankFamily.at(nIndex)->strAnnouncement;

	int nFamilyMember = vector_rankFamily.at(nIndex)->nFamilyMember;
	char charFamilyMember[10];
	sprintf(charFamilyMember, "%d", nFamilyMember);

	int nFamilyMemberMax = vector_rankFamily.at(nIndex)->nFamilyMemberMax;
	char charFamilyMemberMax[10];
	sprintf(charFamilyMemberMax, "%d", nFamilyMemberMax);

	std::string str_familyNum = "";
	str_familyNum.append(charFamilyMember);
	str_familyNum.append("/");
	str_familyNum.append(charFamilyMemberMax);


	auto winsize= Director::getInstance()->getVisibleSize();
	
	auto checkFamily =ApplyChackFamily::create(str_familyName, str_familyCreaterName, longFamilyId, str_familyNum, str_announcement);
	checkFamily->setIgnoreAnchorPointForPosition(false);
	checkFamily->setAnchorPoint(Vec2(0.5, 0.5f));
	checkFamily->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
	GameView::getInstance()->getMainUIScene()->addChild(checkFamily);
}

void RankUI::callBackTreeEvent( Ref * obj )
{
	auto tree_ = (ControlTree *)obj;
	//int index_ = tree_->getTouchTreeIndex();
	//RankData::instance()->set_rankType(index_);

}

void RankUI::callBackSecondTreeEvent( Ref * obj )
{
	auto tree_ = (ControlTree *)obj;

	int index_ = tree_->getTouchTreeIndex();
	RankData::instance()->set_rankType(index_);

	int index_panel = tree_->getSecondTreeByEveryPanelIndex();
	RankData::instance()->set_proOrCountry(index_panel);

	refreshDataByTreeIndex();
	//
	const char *str_level = StringDataManager::getString("rank_level");
	if (strcmp(m_vector_tableView.at(index_).c_str(),str_level)==0)
	{
		refreshDataBySecondTreeIndex(index_panel);
	}

	const char *str_capcity = StringDataManager::getString("rank_capacity");
	if (strcmp(m_vector_tableView.at(index_).c_str(),str_capcity)==0)
	{
		refreshDataBySecondTreeIndex(index_panel);
	}
	const char *str_coin = StringDataManager::getString("rank_coin");
	if (strcmp(m_vector_tableView.at(index_).c_str(),str_coin)==0)
	{
		//branch tree is null
	}
	const char *str_battleAchievement = StringDataManager::getString("rank_battleAchievement");
	if (strcmp(m_vector_tableView.at(index_).c_str(),str_battleAchievement)==0)
	{
		//ְҵ 0.ȫ����1.κ�2.���3.���
		switch(index_panel)
		{
		case 0: 
			{
				RankData::instance()->set_proOrCountry(0);
			}
			break;
		case 1:
			{
				RankData::instance()->set_proOrCountry(1);
			}
			break;
		case 2:
			{
				RankData::instance()->set_proOrCountry(2);
			}
			break;
		case 3:
			{
				RankData::instance()->set_proOrCountry(3);
			}
			break;
		}

		if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
		{
			RankDataTableView::instance()->clearVectorRankPlayer();

			m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
		}

		// �ÿ�ε�����itemģ���Ҫ������ѡitem�index�Ϊdefault
		RankDataTableView::instance()->setSelectItemDefault();
		reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
	}
	const char *str_family = StringDataManager::getString("rank_family");
	if (strcmp(m_vector_tableView.at(index_).c_str(),str_family)==0)
	{
		//branch tree is null
	}

	const char *str_flower = StringDataManager::getString("rank_flower");
	if (strcmp(m_vector_tableView.at(index_).c_str(),str_flower)==0)
	{
		// �ʻ� 1.�����ջ���2.�����ͻ���3.��ʷ�ջ���4.��ʷ�ͻ�
		switch(index_panel)
		{
		case 0: 
			{
				RankData::instance()->set_proOrCountry(1);
			}
			break;
		case 1:
			{
				RankData::instance()->set_proOrCountry(2);
			}
			break;
		case 2:
			{
				RankData::instance()->set_proOrCountry(3);
			}
			break;
		case 3:
			{
				RankData::instance()->set_proOrCountry(4);
			}
			break;
		}

		if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
		{
			RankDataTableView::instance()->clearVectorRankPlayer();

			m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
		}

		// ÿ�ε�����itemģ���Ҫ������ѡitem�index�Ϊdefault
		RankDataTableView::instance()->setSelectItemDefault();
		reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
	}
}

void RankUI::refreshDataByTreeIndex()
{
	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		RankDataTableView::instance()->clearVectorRankPlayer();
		RankDataTableView::instance()->clearVectorRankFamily();

		m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}
	// ÿ�ε�����itemģ���Ҫ������ѡitem�index�Ϊdefault
	RankDataTableView::instance()->setSelectItemDefault();

	// 0:�ȼ�;1:ս�����2:��ң�3:ս����4�����壻5���ʻ�
	switch (RankData::instance()->get_rankType())
	{
	case 0:
		{
			m_panel_level->setVisible(true);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_family->setVisible(false);
			m_panel_battleAch->setVisible(false);
			m_panel_flower->setVisible(false);

			//m_tab_pro->setVisible(true);
			//m_tab_country->setVisible(false);

			//m_tab_pro->setDefaultPanelByIndex(0);
			RankData::instance()->set_proOrCountry(0);

			// ����ʱ��
			//reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 1:
		{
			m_panel_combat->setVisible(true);
			m_panel_level->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_family->setVisible(false);
			m_panel_battleAch->setVisible(false);
			m_panel_flower->setVisible(false);

			RankData::instance()->set_proOrCountry(0);

			// ����ʱ��
			//reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 2:
		{
			m_panel_coin->setVisible(true);
			m_panel_family->setVisible(false);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_battleAch->setVisible(false);
			m_panel_flower->setVisible(false);

			RankData::instance()->set_proOrCountry(0);

			// ����ʱ��
			reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 3:
		{
			m_panel_battleAch->setVisible(true);
			m_panel_family->setVisible(false);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_flower->setVisible(false);

			RankData::instance()->set_proOrCountry(0);

			// ����ʱ��
			//reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
		}
		break;
	case 4:
		{
			m_panel_family->setVisible(true);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_battleAch->setVisible(false);
			m_panel_flower->setVisible(false);

			// ����ʱ��
			reloadTimeFlag();
		}
		break;
	case 5:
		{
			m_panel_flower->setVisible(true);
			m_panel_level->setVisible(false);
			m_panel_combat->setVisible(false);
			m_panel_coin->setVisible(false);
			m_panel_family->setVisible(false);
			m_panel_battleAch->setVisible(false);
			
			RankData::instance()->set_proOrCountry(1);
		}
	}
}

void RankUI::refreshDataBySecondTreeIndex( int index )
{
	//�ְҵ 0.ȫ����1.�ͽ���2.��ı��3.��ܡ�4.���䡢��Э�鶨�壩
	//ְҵ 0.ȫ����1.�ͽ���2.��ܡ�3.���䡢4.��ı   ��UI��˳�
	switch(index)
	{
	case 0: 
		{
			RankData::instance()->set_proOrCountry(0);
		}
		break;
	case 1:
		{
			RankData::instance()->set_proOrCountry(1);
		}
		break;
	case 2:
		{
			RankData::instance()->set_proOrCountry(3);
		}
		break;
	case 3:
		{
			RankData::instance()->set_proOrCountry(4);
		}
		break;
	case 4:
		{
			RankData::instance()->set_proOrCountry(2);
		}
		break;
	}

	if (m_base_layer->getChildByTag(TAG_SCROLLVIEW) != NULL)
	{
		RankDataTableView::instance()->clearVectorRankPlayer();

		m_base_layer->getChildByTag(TAG_SCROLLVIEW)->removeFromParent();
	}

	// �ÿ�ε�����itemģ���Ҫ������ѡitem�index�Ϊdefault
	RankDataTableView::instance()->setSelectItemDefault();
	reloadTimeFlag(RankData::instance()->get_rankType(), RankData::instance()->get_proOrCountry());
}




