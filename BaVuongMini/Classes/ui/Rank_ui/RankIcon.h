#ifndef _UI_RANK_RANKICON_H_
#define _UI_RANK_RANKICON_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ���аIcon
 * @author liuliang
 * @version 0.1.0
 * @date 2014.04.08
 */

class RankIcon:public UIScene
{
public:
	RankIcon(void);
	~RankIcon(void);

public:
	static RankIcon* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

public:
	Button * m_btn_rankIcon;							// �ɵ��ICON
	Layer * m_layer_rank;								// ���Ҫ�õ��layer
	Label * m_label_rank_text;						// 	ICON��·���ʾ�����	
	cocos2d::extension::Scale9Sprite * m_spirte_fontBg;				// �������ʾ�ĵ�ͼ

public:
	void callBackRank(Ref *pSender, Widget::TouchEventType type);

};

#endif

