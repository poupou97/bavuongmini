#ifndef _UI_RANK_RANKDB_H_
#define _UI_RANK_RANKDB_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ���а�ģ�飨����DB���ݿ������
 * @author liuliang
 * @version 0.1.0
 * @date 2014.04.09
 */
class CRoleMessage;
class CHomeMessage;

class RankDb
{
public:
	static RankDb * s_rankDb;
	static RankDb * instance();

private:
	RankDb(void);
	~RankDb(void);

public:
	long long checkDbTimeFlag(std::string strTableName);																																//  ��ѯ db ���ݿ��е�ʱ����������ֵΪ0��˵�� ���ݿ�Ϊ�գ�
	long long checkMyPlayerId(std::string strTableName);																																	//  ��ѯ db ���ݿ��� ��ҵ�id��ȷ���ҵ�������ͬһ���ˣ�
	std::string checkMyPlayerName(std::string strTableName);																															//	 ��ѯ db ���ݿ��� ��ҵ�name��id  �� nameȷ����ͬһ����---��ͬ��������ͬһ��������Ϣ�ǲ�һ���ģ�

	long long checkDbTimeRefreshFlag(std::string strTableName);																														//  ��ѯ db ���ݿ���timeRefreshFlag

	void insertData(std::string strTableName, std::vector<CRoleMessage *> vector_roleMessage);																//  �������ݵ� ָ�� ��
	void insertData(std::string strTableName, std::vector<CHomeMessage *> vector_familyMessage);													

	void deleteTableData(std::string strTableName);																																			//  ɾ�����е�����

	void getDataFromDb(std::vector<CRoleMessage *> &vector_roleMessage, int nRankType, int nProfession);											//  ��db�л�ȡ���ݱ��浽vector�У�������ʾdb�е����ݣ�
	void getDataFromDb(std::vector<CHomeMessage *> &vector_familyMessage);

	void timeIsOut();																																															//  timeIsOut
};

#endif

