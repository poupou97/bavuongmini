#include "RankIcon.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "RankUI.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"

RankIcon::RankIcon(void)
	:m_btn_rankIcon(NULL)
	,m_layer_rank(NULL)
	,m_label_rank_text(NULL)
	,m_spirte_fontBg(NULL)
{

}


RankIcon::~RankIcon(void)
{
	
}

RankIcon* RankIcon::create()
{
	auto rankIcon = new RankIcon();
	if (rankIcon && rankIcon->init())
	{
		rankIcon->autorelease();
		return rankIcon;
	}
	CC_SAFE_DELETE(rankIcon);
	return NULL;
}

bool RankIcon::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		// icon
		m_btn_rankIcon = Button::create();
		m_btn_rankIcon->setTouchEnabled(true);
		m_btn_rankIcon->setPressedActionEnabled(true);

		m_btn_rankIcon->loadTextures("gamescene_state/zhujiemian3/tubiao/active.png", "gamescene_state/zhujiemian3/tubiao/active.png","");
		
		m_btn_rankIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_btn_rankIcon->setPosition(Vec2( winSize.width - 70 - 159 - 80 - 80, winSize.height-70 - 80));
		m_btn_rankIcon->addTouchEventListener(CC_CALLBACK_2(RankIcon::callBackRank, this));

		// label
		const char * str_active = StringDataManager::getString("activy_chartInfo2");

		m_label_rank_text = Label::createWithTTF(str_active, APP_FONT_NAME, 14);
		m_label_rank_text->setColor(Color3B(255, 246, 0));
		m_label_rank_text->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_rank_text->setPosition(Vec2(m_btn_rankIcon->getPosition().x, m_btn_rankIcon->getPosition().y - m_btn_rankIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = cocos2d::extension::Scale9Sprite::create(Rect(5, 10, 1, 4) , "res_ui/zhezhao70.png");
		m_spirte_fontBg->setPreferredSize(Size(47, 14));
		m_spirte_fontBg->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_rank_text->getPosition());


		m_layer_rank = Layer::create();
		//m_layer_rank->setTouchEnabled(true);
		m_layer_rank->setAnchorPoint(Vec2::ZERO);
		m_layer_rank->setPosition(Vec2::ZERO);
		m_layer_rank->setContentSize(winSize);

		m_layer_rank->addChild(m_btn_rankIcon);

		addChild(m_layer_rank);

		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void RankIcon::callBackRank(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
	}
	break;

	case Widget::TouchEventType::CANCELED:
	{
		// ��ʼ�� UI WINDOW��
		Size winSize = Director::getInstance()->getVisibleSize();

		auto pTmpRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
		if (NULL == pTmpRankUI)
		{
			auto pRankUI = RankUI::create();
			pRankUI->setIgnoreAnchorPointForPosition(false);
			pRankUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			pRankUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			pRankUI->setTag(kTagRankUI);

			GameView::getInstance()->getMainUIScene()->addChild(pRankUI);
		}
	}
		break;

	default:
		break;
	}
}

void RankIcon::onEnter()
{
	UIScene::onEnter();
}

void RankIcon::onExit()
{
	UIScene::onExit();
}
