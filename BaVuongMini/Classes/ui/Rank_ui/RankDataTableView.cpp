#include "RankDataTableView.h"
#include "RankUI.h"
#include "../../AppMacros.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "RankData.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../gamescene_state/sceneelement/TargetInfo.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/GameConfig.h"

const Size SizeDataCellSize = Size(510, 37);

#define PAGE_NUM 10
#define TAG_SELECT_ITEM 600

RankDataTableView * RankDataTableView::s_rankDataTableView = NULL;

RankDataTableView * RankDataTableView::instance()
{
	if (NULL == s_rankDataTableView)
	{
		s_rankDataTableView = new RankDataTableView();
	}

	return s_rankDataTableView;
}

RankDataTableView::RankDataTableView( void )
	:m_nPageCur(0)
	,m_nPageAll(0)
	,m_nPageStatus(TableHaveNext)
	,m_nLastSelectCellId(0)
	,m_nSelectCellId(0)
{

}

RankDataTableView::~RankDataTableView( void )
{
	clearVectorRankPlayer();
	clearVectorRankFamily();
}

void RankDataTableView::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void RankDataTableView::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void RankDataTableView::tableCellTouched( TableView* table, TableViewCell* cell )
{
	// ����ѡ��ʱ�ĵ�ͼ
	m_nSelectCellId = cell->getIdx();

	//ȡ���ϴ�ѡ��״̬�����±���ѡ��״̬
	if(table->cellAtIndex(m_nLastSelectCellId))
	{
		if (table->cellAtIndex(m_nLastSelectCellId)->getChildByTag(TAG_SELECT_ITEM))
		{
			table->cellAtIndex(m_nLastSelectCellId)->getChildByTag(TAG_SELECT_ITEM)->setVisible(false);
		}
	}

	if (cell->getChildByTag(TAG_SELECT_ITEM))
	{
		cell->getChildByTag(TAG_SELECT_ITEM)->setVisible(true);
	}

	m_nLastSelectCellId = cell->getIdx();


	// ��������߼�
	auto pRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
	if (NULL == pRankUI)
	{
		//GameView::getInstance()->showAlertDialog("tableView error");
		return ;
	}
	if (NULL == pRankUI->getBaseLayer()->getChildByTag(TAG_SCROLLVIEW))
	{
		//GameView::getInstance()->showAlertDialog("tableView error");
		return ;
	}

	auto dataView = (TableView *)pRankUI->getBaseLayer()->getChildByTag(TAG_SCROLLVIEW);

	float dataViewPositionX = dataView->getPositionX();
	float dataViewPositionY = dataView->getPositionY();

	float rankUIPositionX = pRankUI->getPositionX();
	float rankUIPositionY = pRankUI->getPositionY();

	float dataViewWidth = dataView->getContentSize().width;
	float dataViewHeight = dataView->getContentSize().height;

	float finalPositionX = dataViewPositionX + rankUIPositionX;
	float finalPositionY = dataViewPositionY + rankUIPositionY;

	int nRankType = RankData::instance()->get_rankType();

	long long longPlayerId = 0;
	std::string strPlayerName = "";
	std::string country_ = "";
	int vipLevel = 0;
	int level = 0;
	std::string  pressionStr = "";
	if (0 == nRankType || 1 == nRankType || 2 == nRankType || 3 == nRankType || 5 == nRankType)
	{
		auto rankPlayerStruct = m_vector_rankPlayer.at(cell->getIdx());
		longPlayerId = rankPlayerStruct->longPlayerId;
		strPlayerName = rankPlayerStruct->strName;
		country_ = rankPlayerStruct->strCountry;
		vipLevel = rankPlayerStruct->nVipLevel;
		pressionStr = rankPlayerStruct->strProfession;
	}
	else if (4 == nRankType)
	{
		return ;
	}

	if (0 == longPlayerId && "" == strPlayerName)
	{
		//GameView::getInstance()->showAlertDialog("playerId or playerName is error");
		return ;
	}

	auto base = GameView::getInstance()->myplayer->getActiveRole()->mutable_rolebase();
	long long longRoleId = base->roleid();
	// ������а�� �ѡ�е����Լ����򲻵�� ����
	if (longPlayerId == longRoleId)
	{
		return ;
	}
	
	auto winSize = Director::getInstance()->getVisibleSize();

	auto list = (TargetInfoList*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagMainSceneTargetInfo);
	if (list != NULL)
	{
		list->removeFromParentAndCleanup(true);
	}

	int country_Id = 0;
	int pressionId = BasePlayer::getProfessionIdxByName(pressionStr);
	auto tarlist = TargetInfoList::create(longPlayerId, strPlayerName,country_Id,vipLevel,level,pressionId);
	tarlist->setIgnoreAnchorPointForPosition(false);
	tarlist->setAnchorPoint(Vec2(0, 0));
	tarlist->setTag(ktagMainSceneTargetInfo);
	tarlist->setPosition(Vec2(finalPositionX, finalPositionY));
	//tarlist->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
	GameView::getInstance()->getMainUIScene()->addChild(tarlist);
}

cocos2d::Size RankDataTableView::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return SizeDataCellSize;
}

cocos2d::extension::TableViewCell* RankDataTableView::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();


	int nRankType = RankData::instance()->get_rankType();

	if (0 == nRankType || 1 == nRankType || 2 == nRankType || 3 == nRankType || 5 == nRankType)
	{
		if (idx < m_vector_rankPlayer.size())
		{
			initCell(cell, idx, nRankType);
		}

		if (m_vector_rankPlayer.size() > 0)
		{
			int nLastIndex = m_vector_rankPlayer.size() - 1;
			int tmp_pageCur = m_vector_rankPlayer.at(nLastIndex)->nPageCur;
			int tmp_pageAll = m_vector_rankPlayer.at(nLastIndex)->nPageAll;

			this->refreshDataTableStatus(tmp_pageCur, tmp_pageAll);
		}
		else
		{
			GameView::getInstance()->showAlertDialog("page error");
		}

		if (idx == m_vector_rankPlayer.size() - 1)
		{
			if (TableHaveBoth == m_nPageStatus || TableHaveNext == m_nPageStatus)
			{
				// �������һҳ����
				int nPage = m_nPageCur + 1;
				if (nPage > m_nPageAll)
				{
					//GameView::getInstance()->showAlertDialog("page error");
					return cell;
				}

				//////////////////////////////////////////////// debug�ʱ����ʾ��Ϣ
				/*char str_page[10];
				sprintf(str_page, "%d", nPage);
				std::string str_show = str_page;

				GameView::getInstance()->showAlertDialog(str_show);*/
				/////////////////////////////////////////////////////////////////

				auto rankDataStruct = new RankDataStruct();
				rankDataStruct->nType = RankData::instance()->get_rankType();
				rankDataStruct->nWork = RankData::instance()->get_proOrCountry();
				rankDataStruct->nPage = nPage;

				GameMessageProcessor::sharedMsgProcessor()->sendReq(2300, (void *)rankDataStruct);

				delete rankDataStruct;
			}
		}
	}
	else if (4 == nRankType)
	{
		if (idx < m_vector_rankFamily.size())
		{
			initCell(cell, idx, nRankType);
		}

		if (m_vector_rankFamily.size() > 0)
		{
			int nLastIndex = m_vector_rankFamily.size() - 1;
			int tmp_pageCur = m_vector_rankFamily.at(nLastIndex)->nPageCur;
			int tmp_pageAll = m_vector_rankFamily.at(nLastIndex)->nPageAll;

			this->refreshDataTableStatus(tmp_pageCur, tmp_pageAll);
		}
		else
		{
			//GameView::getInstance()->showAlertDialog("page error");
		}
		

		if (idx == m_vector_rankFamily.size() - 1)
		{
			if (TableHaveBoth == m_nPageStatus || TableHaveNext == m_nPageStatus)
			{
				// ������һҳ����
				int nPage = m_nPageCur + 1;
				if (nPage > m_nPageAll)
				{
					//GameView::getInstance()->showAlertDialog("page error");
					return cell;
				}

				/////////////////////////////////////////////////////////////////////
				/*char str_page[10];
				sprintf(str_page, "%d", nPage);
				std::string str_show = str_page;

				GameView::getInstance()->showAlertDialog(str_show);*/
				/////////////////////////////////////////////////////////////////////

				GameMessageProcessor::sharedMsgProcessor()->sendReq(2301, (void *)nPage);
			}
		}
	}


	return cell;
}

ssize_t RankDataTableView::numberOfCellsInTableView( TableView *table )
{
	int nRankType = RankData::instance()->get_rankType();

	if (0 == nRankType || 1 == nRankType || 2 == nRankType || 3 == nRankType || 5 == nRankType)
	{
		return m_vector_rankPlayer.size();
	}
	else if (4 == nRankType)
	{
		return m_vector_rankFamily.size();
	}

	return 0;

}

void RankDataTableView::clearVectorRankPlayer()
{
	std::vector<RankPlayerStruct *>::iterator iter;
	for (iter = m_vector_rankPlayer.begin(); iter != m_vector_rankPlayer.end(); iter++)
	{
		delete *iter;
	}
	m_vector_rankPlayer.clear();
}

void RankDataTableView::clearVectorRankFamily()
{
	std::vector<RankFamilyStruct *>::iterator iter;
	for (iter = m_vector_rankFamily.begin(); iter != m_vector_rankFamily.end(); iter++)
	{
		delete *iter;
	}
	m_vector_rankFamily.clear();
}



void RankDataTableView::initCell( TableViewCell * cell, unsigned int nIndex, int nRankType)
{
	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(5, 5, 1, 1) , "res_ui/mengban_green.png");
	pHighlightSpr->setPreferredSize(Size(SizeDataCellSize.width - 4, SizeDataCellSize.height - 2));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setPosition(Vec2(2,1));
	pHighlightSpr->setTag(TAG_SELECT_ITEM);
	cell->addChild(pHighlightSpr);
	pHighlightSpr->setVisible(false);

	if (this->m_nSelectCellId == nIndex)
	{
		pHighlightSpr->setVisible(true);
	}

	if (0 == nRankType || 1 == nRankType)
	{
		// ݵ�ͼ
		auto sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
		sprite_frame->setContentSize(Size(SizeDataCellSize.width - 4, SizeDataCellSize.height - 2));
		sprite_frame->setCapInsets(Rect(9, 15, 1, 1));
		sprite_frame->setAnchorPoint(Vec2::ZERO);
		sprite_frame->setPosition(Vec2(2, 1));
		cell->addChild(sprite_frame);

		Color3B color = Color3B(0, 0, 0);
		//Color3B colorFont = Color3B(47, 93, 13);

		// ���
		int nRank = m_vector_rankPlayer.at(nIndex)->nRank;
		Color3B colorFont = getFontColor(nRank);

		if (nRank > 10)
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);
			auto m_label_rank = Label::createWithTTF(char_rank, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
			m_label_rank->setColor(colorFont);
			//m_label_rank->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
			m_label_rank->setAnchorPoint(Vec2(0.5f, 0.5f));
			m_label_rank->setHorizontalAlignment(TextHAlignment::CENTER);
			m_label_rank->setPosition(Vec2(54, sprite_frame->getContentSize().height / 2));
			cell->addChild(m_label_rank);
		}
		else
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);
			
			auto Label = Label::createWithBMFont("res_ui/font/ziti_3.fnt", char_rank) ;
			Label->setAnchorPoint(Vec2(0.5f, 0.5f));
			Label->setPosition(Vec2(56, sprite_frame->getContentSize().height / 2));

			auto tmpLayer = Layer::create();
			tmpLayer->addChild(Label);
			cell->addChild(tmpLayer);

			// �ǰ����ӻʹڱ�־
			auto spriteRank = setRankPic(Label->getPosition().x, Label->getPosition().y, nRank);
			if (NULL != spriteRank)
			{
				cell->addChild(spriteRank);
			}
		}

		// ��
		std::string str_name = m_vector_rankPlayer.at(nIndex)->strName;
		auto m_label_name = Label::createWithTTF(str_name.c_str(), APP_FONT_NAME, 18);
		m_label_name->setColor(colorFont);
		//m_label_name->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_name->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_name->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_name->setPosition(Vec2(156, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_name);

		// vipƿ��ؿ��
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (true == vipEnabled)
		{
			//VIPƵȼ�
			auto pNode = MainScene::addVipInfoByLevelForNode(m_vector_rankPlayer.at(nIndex)->nVipLevel);
			//Node *pNode = MainScene::addVipInfoByLevelForNode(5);
			if (pNode)
			{
				cell->addChild(pNode);
				pNode->setPosition(Vec2(m_label_name->getPosition().x+m_label_name->getContentSize().width/2+13,m_label_name->getPosition().y));
			}
		}

		// �ȼ� or ս��� or ��
		int nMount = m_vector_rankPlayer.at(nIndex)->nMount;
		char char_mount[20];
		sprintf(char_mount, "%d", nMount);
		auto m_label_mount = Label::createWithTTF(char_mount, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_mount->setColor(colorFont);
		//m_label_mount->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_mount->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_mount->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_mount->setPosition(Vec2(260, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_mount);

		// �ְҵ
		std::string str_profession = m_vector_rankPlayer.at(nIndex)->strProfession;
		auto m_label_profession = Label::createWithTTF(str_profession.c_str(), APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_profession->setColor(colorFont);
		//m_label_profession->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_profession->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_profession->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_profession->setPosition(Vec2(360, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_profession);

		// ��
		std::string str_country = m_vector_rankPlayer.at(nIndex)->strCountry;
		auto m_label_country = Label::createWithTTF(str_country.c_str(), APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_country->setColor(colorFont);
		//m_label_country->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_country->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_country->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_country->setPosition(Vec2(457, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_country);
	}
	else if (2 == nRankType)
	{
		// ҵ�ͼ
		auto sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
		sprite_frame->setContentSize(Size(SizeDataCellSize.width - 4, SizeDataCellSize.height - 2));
		sprite_frame->setCapInsets(Rect(9, 15, 1, 1));
		sprite_frame->setAnchorPoint(Vec2::ZERO);
		sprite_frame->setPosition(Vec2(2, 1));
		cell->addChild(sprite_frame);

		Color3B color = Color3B(0, 0, 0);
		//Color3B colorFont = Color3B(47, 93, 13);

		// ���
		int nRank = m_vector_rankPlayer.at(nIndex)->nRank;
		Color3B colorFont = getFontColor(nRank);

		if (nRank > 10)
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);
			auto m_label_rank = Label::createWithTTF(char_rank, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
			m_label_rank->setColor(colorFont);
			//m_label_rank->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
			m_label_rank->setAnchorPoint(Vec2(0.5f, 0.5f));
			m_label_rank->setHorizontalAlignment(TextHAlignment::CENTER);
			m_label_rank->setPosition(Vec2(54, sprite_frame->getContentSize().height / 2));
			cell->addChild(m_label_rank);
		}
		else
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);

			auto Label = Label::createWithBMFont("res_ui/font/ziti_3.fnt", char_rank);
			Label->setAnchorPoint(Vec2(0.5f, 0.5f));
			Label->setPosition(Vec2(56, sprite_frame->getContentSize().height / 2));

			auto tmpLayer = Layer::create();
			tmpLayer->addChild(Label);
			cell->addChild(tmpLayer);

			// �ǰ����ӻʹڱ�־
			auto spriteRank = setRankPic(Label->getPosition().x, Label->getPosition().y, nRank);
			if (NULL != spriteRank)
			{
				cell->addChild(spriteRank);
			}
		}

		// ��
		std::string str_name = m_vector_rankPlayer.at(nIndex)->strName;
		auto m_label_name = Label::createWithTTF(str_name.c_str(), APP_FONT_NAME, 18);
		m_label_name->setColor(colorFont);
		//m_label_name->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_name->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_name->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_name->setPosition(Vec2(156, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_name);

		// vipƿ��ؿ��
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (true == vipEnabled)
		{
			//VIPƵȼ�
			auto pNode = MainScene::addVipInfoByLevelForNode(m_vector_rankPlayer.at(nIndex)->nVipLevel);
			//Node *pNode = MainScene::addVipInfoByLevelForNode(5);
			if (pNode)
			{
				cell->addChild(pNode);
				pNode->setPosition(Vec2(m_label_name->getPosition().x+m_label_name->getContentSize().width/2+13,m_label_name->getPosition().y));
			}
		}

		// �ȼ� or ս��� or ��
		int nMount = m_vector_rankPlayer.at(nIndex)->nMount;
		char char_mount[20];
		sprintf(char_mount, "%d", nMount);
		auto m_label_mount = Label::createWithTTF(char_mount, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_mount->setColor(colorFont);
		//m_label_mount->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_mount->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_mount->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_mount->setPosition(Vec2(275, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_mount);

		// �ְҵ
		std::string str_profession = m_vector_rankPlayer.at(nIndex)->strProfession;
		auto m_label_profession = Label::createWithTTF(str_profession.c_str(), APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_profession->setColor(colorFont);
		//m_label_profession->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_profession->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_profession->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_profession->setPosition(Vec2(380, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_profession);

		// ��
		std::string str_country = m_vector_rankPlayer.at(nIndex)->strCountry;
		auto m_label_country = Label::createWithTTF(str_country.c_str(), APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_country->setColor(colorFont);
		//m_label_country->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_country->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_country->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_country->setPosition(Vec2(464, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_country);
	}
	else if (3 == nRankType)   // �ս������� ���� �ս�� �ȼ� ְҵ��
	{
		// ��ͼ
		auto sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
		sprite_frame->setContentSize(Size(SizeDataCellSize.width - 4, SizeDataCellSize.height - 2));
		sprite_frame->setCapInsets(Rect(9, 15, 1, 1));
		sprite_frame->setAnchorPoint(Vec2::ZERO);
		sprite_frame->setPosition(Vec2(2, 1));
		cell->addChild(sprite_frame);

		Color3B color = Color3B(0, 0, 0);
		//Color3B colorFont = Color3B(47, 93, 13);

		// ���
		int nRank = m_vector_rankPlayer.at(nIndex)->nRank;
		Color3B colorFont = getFontColor(nRank);

		if (nRank > 10)
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);
			auto m_label_rank = Label::createWithTTF(char_rank, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
			m_label_rank->setColor(colorFont);
			//m_label_rank->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
			m_label_rank->setAnchorPoint(Vec2(0.5f, 0.5f));
			m_label_rank->setHorizontalAlignment(TextHAlignment::CENTER);
			m_label_rank->setPosition(Vec2(54, sprite_frame->getContentSize().height / 2));
			cell->addChild(m_label_rank);
		}
		else
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);

			auto Label = Label::createWithBMFont("res_ui/font/ziti_3.fnt", char_rank);
			Label->setAnchorPoint(Vec2(0.5f, 0.5f));
			Label->setPosition(Vec2(56, sprite_frame->getContentSize().height / 2));

			auto tmpLayer = Layer::create();
			tmpLayer->addChild(Label);
			cell->addChild(tmpLayer);

			// �ǰ����ӻʹڱ�־
			auto spriteRank = setRankPic(Label->getPosition().x, Label->getPosition().y, nRank);
			if (NULL != spriteRank)
			{
				cell->addChild(spriteRank);
			}
		}

		// ��(ƹ�+����+vip)
	
		// ����
		std::string str_name = m_vector_rankPlayer.at(nIndex)->strName;
		auto m_label_name = Label::createWithTTF(str_name.c_str(), APP_FONT_NAME, 18);
		m_label_name->setColor(colorFont);
		//m_label_name->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_name->setAnchorPoint(Vec2(0, 0.5f));
		m_label_name->setHorizontalAlignment(TextHAlignment::LEFT);
		m_label_name->setPosition(Vec2(138, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_name);

		// ֹ�
		int nCountryId = m_vector_rankPlayer.at(nIndex)->nCountry;
		if (nCountryId > 0 && nCountryId < 5)
		{
			std::string strCountryIdName = "country_";
			char charCountryId[20];
			sprintf(charCountryId, "%d", nCountryId);
			strCountryIdName.append(charCountryId);
			std::string strIconPathName = "res_ui/country_icon/";
			strIconPathName.append(strCountryIdName);
			strIconPathName.append(".png");
			auto spriteCountry = Sprite::create(strIconPathName.c_str());
			spriteCountry->setAnchorPoint(Vec2(1.0f, 0.5f));
			spriteCountry->setPosition(Vec2(m_label_name->getPositionX(), sprite_frame->getContentSize().height / 2));
			spriteCountry->setScale(0.7f);
			cell->addChild(spriteCountry);
		}

		// vipҿ��ؿ��
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (true == vipEnabled)
		{
			//VIPƵȼ�
			auto pNode = MainScene::addVipInfoByLevelForNode(m_vector_rankPlayer.at(nIndex)->nVipLevel);
			//Node *pNode = MainScene::addVipInfoByLevelForNode(5);
			if (pNode)
			{
				cell->addChild(pNode);
				pNode->setAnchorPoint(Vec2(0, 0.5f));
				pNode->setPosition(Vec2(m_label_name->getPosition().x + m_label_name->getContentSize().width, m_label_name->getPosition().y));
			}
		}

		// �ȼ� or ս��� or �� or �ս��
		int nMount = m_vector_rankPlayer.at(nIndex)->nMount;
		char char_mount[20];
		sprintf(char_mount, "%d", nMount);
		auto m_label_mount = Label::createWithTTF(char_mount, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_mount->setColor(colorFont);
		//m_label_mount->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_mount->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_mount->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_mount->setPosition(Vec2(286, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_mount);
		
		// �ȼ�
		int nLevel = m_vector_rankPlayer.at(nIndex)->nLevel;
		char char_level[20];
		sprintf(char_level, "%d", nLevel);
		auto m_label_level = Label::createWithTTF(char_level, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_level->setColor(colorFont);
		m_label_level->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_level->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_level->setPosition(Vec2(380, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_level);


		// ְҵ
		std::string str_profession = m_vector_rankPlayer.at(nIndex)->strProfession;
		auto m_label_profession = Label::createWithTTF(str_profession.c_str(), APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_profession->setColor(colorFont);
		//m_label_profession->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_profession->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_profession->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_profession->setPosition(Vec2(465, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_profession);
	}
	else if (4 == nRankType)
	{
		// ��ͼ
		auto sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
		sprite_frame->setContentSize(Size(SizeDataCellSize.width - 4, SizeDataCellSize.height - 2));
		sprite_frame->setCapInsets(Rect(9, 15, 1, 1));
		sprite_frame->setAnchorPoint(Vec2::ZERO);
		sprite_frame->setPosition(Vec2(2, 1));
		cell->addChild(sprite_frame);

		Color3B color = Color3B(0, 0, 0);
		//Color3B colorFont = Color3B(47, 93, 13);

		// ���
		int nRank = m_vector_rankFamily.at(nIndex)->nRank;
		Color3B colorFont = getFontColor(nRank);

		if (nRank > 10)
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);
			auto m_label_rank = Label::createWithTTF(char_rank, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
			m_label_rank->setColor(colorFont);
			//m_label_rank->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
			m_label_rank->setAnchorPoint(Vec2(0.5f, 0.5f));
			m_label_rank->setHorizontalAlignment(TextHAlignment::CENTER);
			m_label_rank->setPosition(Vec2(54, sprite_frame->getContentSize().height / 2));
			cell->addChild(m_label_rank);
		}
		else
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);

			auto Label = Label::createWithBMFont("res_ui/font/ziti_3.fnt", char_rank);
			Label->setAnchorPoint(Vec2(0.5f, 0.5f));
			Label->setPosition(Vec2(56, sprite_frame->getContentSize().height / 2));			// ����X����Ϊ54���˴�Ϊ56��ԭ������һ������ö��뷽ʽ��һ����

			auto tmpLayer = Layer::create();
			tmpLayer->addChild(Label);
			cell->addChild(tmpLayer);

			// �ǰ����ӻʹڱ�־
			auto spriteRank = setRankPic(Label->getPosition().x, Label->getPosition().y, nRank);
			if (NULL != spriteRank)
			{
				cell->addChild(spriteRank);
			}
		}

		// ������
		std::string str_familyName = m_vector_rankFamily.at(nIndex)->strFamilyName;
		auto m_label_familyName = Label::createWithTTF(str_familyName.c_str(), APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_familyName->setColor(colorFont);
		//m_label_familyName->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_familyName->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_familyName->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_familyName->setPosition(Vec2(156, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_familyName);

		// Ƶȼ�
		int nLevel = m_vector_rankFamily.at(nIndex)->nLevel;
		char char_level[20];
		sprintf(char_level, "%d", nLevel);
		auto m_label_level = Label::createWithTTF(char_level, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_level->setColor(colorFont);
		//m_label_level->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_level->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_level->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_level->setPosition(Vec2(242, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_level);

		// ���
		int nFamilyMember = m_vector_rankFamily.at(nIndex)->nFamilyMember;
		char char_familyMember[20];
		sprintf(char_familyMember, "%d", nFamilyMember);
		int nFamilyMemberMax = m_vector_rankFamily.at(nIndex)->nFamilyMemberMax;
		char char_familyMemberMax[20];
		sprintf(char_familyMemberMax, "%d", nFamilyMemberMax);

		std::string str_familyNum = "";
		str_familyNum.append(char_familyMember);
		str_familyNum.append("/");
		str_familyNum.append(char_familyMemberMax);

		auto m_label_familyNum = Label::createWithTTF(str_familyNum.c_str(), APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_familyNum->setColor(colorFont);
		//m_label_playerNum->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_familyNum->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_familyNum->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_familyNum->setPosition(Vec2(315, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_familyNum);

		// ��
		std::string str_country = m_vector_rankFamily.at(nIndex)->strCountry;
		auto m_label_country = Label::createWithTTF(str_country.c_str(), APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_country->setColor(colorFont);
		//m_label_country->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_country->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_country->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_country->setPosition(Vec2(390, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_country);

		// Ҳ鿴��ť
		auto sprite_btn = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_2.png");
		sprite_btn->setCapInsets(Rect(18, 9, 2, 23));
		sprite_btn->setContentSize(Size(83, 38));

		auto pRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
		if (NULL != pRankUI)
		{
			auto item = MenuItemSprite::create(sprite_btn, sprite_btn, CC_CALLBACK_1(RankUI::callBackBtnFamilyInfo, pRankUI));
			item->setScale(1.2f);
			item->setTag(nRank);			// � Լ������ ���Ϊtag

			auto pSelectMenu = CCMoveableMenu::create(item, NULL);
			pSelectMenu->setAnchorPoint(Vec2(0, 0));
			pSelectMenu->setPosition(Vec2(470, sprite_frame->getContentSize().height / 2));

			// �鿴�text
			const char * char_chakan = StringDataManager::getString("rank_chankan");
			char * str_chakan = const_cast<char *>(char_chakan);
			auto lable_btnText = Label::createWithTTF(str_chakan, APP_FONT_NAME, 18, Size(200, 0), TextHAlignment::LEFT);
			lable_btnText->setColor(Color3B(255, 255, 255));
			//lable_btnText->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
			lable_btnText->setAnchorPoint(Vec2(0, 0));
			lable_btnText->setPosition(Vec2((lable_btnText->getContentSize().width - sprite_btn->getContentSize().width) / 2 - 34, (sprite_btn->getContentSize().height - lable_btnText->getContentSize().height) / 2));
			item->addChild(lable_btnText);

			cell->addChild(pSelectMenu);
		}
	}
	else if (5 == nRankType)
	{
		// ĵ�ͼ
		auto sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/LV3_dikuang.png");
		sprite_frame->setContentSize(Size(SizeDataCellSize.width - 4, SizeDataCellSize.height - 2));
		sprite_frame->setCapInsets(Rect(9, 15, 1, 1));
		sprite_frame->setAnchorPoint(Vec2::ZERO);
		sprite_frame->setPosition(Vec2(2, 1));
		cell->addChild(sprite_frame);

		Color3B color = Color3B(0, 0, 0);
		//Color3B colorFont = Color3B(47, 93, 13);

		// ���
		int nRank = m_vector_rankPlayer.at(nIndex)->nRank;
		Color3B colorFont = getFontColor(nRank);

		if (nRank > 10)
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);
			auto m_label_rank = Label::createWithTTF(char_rank, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
			m_label_rank->setColor(colorFont);
			//m_label_rank->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
			m_label_rank->setAnchorPoint(Vec2(0.5f, 0.5f));
			m_label_rank->setHorizontalAlignment(TextHAlignment::CENTER);
			m_label_rank->setPosition(Vec2(54, sprite_frame->getContentSize().height / 2));
			cell->addChild(m_label_rank);
		}
		else
		{
			char char_rank[20];
			sprintf(char_rank, "%d", nRank);

			auto Label = Label::createWithBMFont("res_ui/font/ziti_3.fnt",char_rank);
			Label->setAnchorPoint(Vec2(0.5f, 0.5f));
			Label->setPosition(Vec2(56, sprite_frame->getContentSize().height / 2));

			auto tmpLayer = Layer::create();
			tmpLayer->addChild(Label);
			cell->addChild(tmpLayer);

			// �ǰ����ӻʹڱ�־
			auto spriteRank = setRankPic(Label->getPosition().x, Label->getPosition().y, nRank);
			if (NULL != spriteRank)
			{
				cell->addChild(spriteRank);
			}
		}

		// ��
		std::string str_name = m_vector_rankPlayer.at(nIndex)->strName;
		auto m_label_name = Label::createWithTTF(str_name.c_str(), APP_FONT_NAME, 18);
		m_label_name->setColor(colorFont);
		//m_label_name->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_name->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_name->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_name->setPosition(Vec2(166, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_name);

		// vipƿ��ؿ��
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (true == vipEnabled)
		{
			//VIPƵȼ�
			auto pNode = MainScene::addVipInfoByLevelForNode(m_vector_rankPlayer.at(nIndex)->nVipLevel);
			//Node *pNode = MainScene::addVipInfoByLevelForNode(5);
			if (pNode)
			{
				cell->addChild(pNode);
				pNode->setPosition(Vec2(m_label_name->getPosition().x+m_label_name->getContentSize().width/2+13,m_label_name->getPosition().y));
			}
		}

		// �ʻ��
		int nMount = m_vector_rankPlayer.at(nIndex)->nMount;
		char char_mount[20];
		sprintf(char_mount, "%d", nMount);
		auto m_label_mount = Label::createWithTTF(char_mount, APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_mount->setColor(colorFont);
		//m_label_mount->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_mount->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_mount->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_mount->setPosition(Vec2(293, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_mount);

		// �ְҵ
		//std::string str_profession = m_vector_rankPlayer.at(nIndex)->strProfession;
		//Label * m_label_profession = Label::createWithTTF(str_profession.c_str(), APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		//m_label_profession->setColor(colorFont);
		////m_label_profession->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		//m_label_profession->setAnchorPoint(Vec2(0.5f, 0.5f));
		//m_label_profession->setHorizontalAlignment(TextHAlignment::CENTER);
		//m_label_profession->setPosition(Vec2(360, sprite_frame->getContentSize().height / 2));
		//cell->addChild(m_label_profession);

		// ��
		std::string str_country = m_vector_rankPlayer.at(nIndex)->strCountry;
		auto m_label_country = Label::createWithTTF(str_country.c_str(), APP_FONT_NAME, 18, Size(150, 0), TextHAlignment::LEFT);
		m_label_country->setColor(colorFont);
		//m_label_country->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0f, color);
		m_label_country->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_country->setHorizontalAlignment(TextHAlignment::CENTER);
		m_label_country->setPosition(Vec2(422, sprite_frame->getContentSize().height / 2));
		cell->addChild(m_label_country);
	}
}

void RankDataTableView::set_pageCur( int nPageCur )
{
	this->m_nPageCur = nPageCur;
}

int RankDataTableView::get_pageCur()
{
	return this->m_nPageCur;
}

void RankDataTableView::set_pageAll( int nPageAll )
{
	this->m_nPageAll = nPageAll;
}

int RankDataTableView::get_pageAll()
{
	return this->m_nPageAll;
}

void RankDataTableView::refreshDataTableStatus( int nPageCur, int nPageAll )
{
	m_nPageCur = nPageCur;
	m_nPageAll = nPageAll;
	if (0 == nPageCur)
	{
		if (nPageCur < nPageAll - 1)
		{
			this->m_nPageStatus = TableHaveNext;
		}
		else
		{
			this->m_nPageStatus = TableHaveNone;
		}
	}
	else
	{
		if (nPageCur < nPageAll - 1)
		{
			this->m_nPageStatus = TableHaveBoth;
		}
		else
		{
			this->m_nPageStatus = TableHaveLast;
		}
	}
}

cocos2d::Color3B RankDataTableView::getFontColor( int nRank )
{
	// �Ĭ�
	Color3B colorFont = Color3B(47, 93, 13);
	if (1 == nRank)
	{
		colorFont = Color3B(155, 84, 0);
	}
	else if (2 == nRank)
	{
		colorFont = Color3B(169, 2, 155);
	}
	else if(3 == nRank)
	{
		colorFont = Color3B(0, 64, 227);
	}

	return colorFont;
}

Sprite* RankDataTableView::setRankPic( float positionX, float positionY, int nRank)
{
	Sprite* spriteRank = NULL; 

	if (1 == nRank)
	{
		spriteRank = Sprite::create("res_ui/rank1.png");
	}
	else if (2 == nRank)
	{
		spriteRank = Sprite::create("res_ui/rank2.png");
	}
	else if (3 == nRank)
	{
		spriteRank = Sprite::create("res_ui/rank3.png");
	}

	if (NULL != spriteRank)
	{
		spriteRank->setAnchorPoint(Vec2(0.5f, 0.5f));
		spriteRank->setIgnoreAnchorPointForPosition(false);
		spriteRank->setPosition(Vec2(positionX - 2, positionY));
	}

	return spriteRank;
}

void RankDataTableView::setSelectItemDefault()
{
	this->m_nLastSelectCellId = 0;
	this->m_nSelectCellId = 0;
}





