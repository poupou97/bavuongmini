#ifndef _UI_RANK_RANKDATA_H_
#define _UI_RANK_RANKDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ���а�ģ�飨���ڱ�����������ݣ�
 * @author liuliang
 * @version 0.1.0
 * @date 2014.04.09
 */

class CRoleMessage;
class CHomeMessage;

class RankData
{
public:
	static RankData * s_rankData;
	static RankData * instance();

private:
	RankData(void);
	~RankData(void);

public:
	std::vector<CRoleMessage *> m_vector_player_internet;
	std::vector<CRoleMessage *> m_vector_player_db;

	std::vector<CHomeMessage *> m_vector_family_internet;
	std::vector<CHomeMessage *> m_vector_family_db;

private:
	long long m_longTimeFlag;																						// ʱ�������������ʱ��� + millisecondNow()��
	int m_nRankType;																										
	int m_nProOrCountry;									// ְҵ ���� ����
	long long m_longTimeRefreshFlag;
	
public:
	void set_hasReqInternet(bool bFlag);
	bool get_hasReqInternet();

	void set_timeFlag(long long longTimeFlag);
	long long get_timeFlag();

	void set_rankType(int nRankType);
	int get_rankType();

	void set_proOrCountry(int nProfession);
	int get_proOrCountry();

	void set_timeRefreshFlag(long long longTimeRefreshFlag);
	long long get_timeRefreshFlag();

public:
	void clearVectorPlayerInternet();
	void clearVectorFamilyInternet();

	void clearVectorPlayerDb();
	void clearVectorFamilyDb();

	void clearAllData();
};

#endif

