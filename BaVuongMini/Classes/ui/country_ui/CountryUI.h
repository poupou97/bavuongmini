#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CountryUI:public UIScene
{
public:
	CountryUI(void);
	~CountryUI(void);

	static CountryUI *create();
	bool init();
	
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void menuSelectCountry(Ref *pSender, Widget::TouchEventType type);
	void updateCountryButton(int country);
	void enterCallBack(Ref *pSender, Widget::TouchEventType type);

	static int mCountryFlag;
};

