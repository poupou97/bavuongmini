#include "CountryUI.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"

#define COUNTRY_1_TAG 1
#define COUNTRY_2_TAG 2
#define COUNTRY_3_TAG 3
#define HIGHLIGHT_TAG 55
#define HIGHLIGHT_TRANS_TIME 0.5f
#define ROLECOLOR 150,150,150
#define TABLE_LIGHT_TRANS_TIME 2.0f

int CountryUI::mCountryFlag = 3;

using namespace cocos2d::ui;

CountryUI::CountryUI(void)
{
}


CountryUI::~CountryUI(void)
{
}

CountryUI * CountryUI::create()
{
	auto countryui=new CountryUI();
	if (countryui && countryui->init())
	{
		countryui->autorelease();
		return countryui;
	}
	CC_SAFE_DELETE(countryui);
	return NULL;
}

bool CountryUI::init()
{
	if (UIScene::init())
	{
		Size s=Director::getInstance()->getVisibleSize();

		auto layer=Layer::create();
		layer->setIgnoreAnchorPointForPosition(false);
		layer->setAnchorPoint(Vec2(0.5f,0.5f));
		layer->setPosition(Vec2(s.width/2,s.height/2));
		layer->setContentSize(Size(UI_DESIGN_RESOLUTION_WIDTH,UI_DESIGN_RESOLUTION_HEIGHT));
		layer->setTag(255);
		addChild(layer);

		auto mengban = ui::ImageView::create();
		mengban->loadTexture("newcommerstory/zhezhao.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(s);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		mengban->setOpacity(240);
		m_pLayer->addChild(mengban);
		
		auto countrySelect = ui::ImageView::create();
		countrySelect->loadTexture("res_ui/country_icon/xzgj.png");
		countrySelect->setAnchorPoint(Vec2(0.5f,0.5f));
		countrySelect->setPosition(Vec2(400,430));
		layer->addChild(countrySelect);
		
		auto right = ui::ImageView::create();
		right->loadTexture("res_ui/country_icon/hw.png");
		right->setAnchorPoint(Vec2(0.5f,0.5f));
		right->setPosition(Vec2(530,430));
		layer->addChild(right);
		
		auto left = ui::ImageView::create();
		left->loadTexture("res_ui/country_icon/hw.png");
		left->setAnchorPoint(Vec2(0.5f,0.5f));
		left->setPosition(Vec2(270,430));
		left->setRotation(180);
		layer->addChild(left);

		auto action = RepeatForever::create(
								Spawn::create(
									Sequence::create(
										FadeTo::create(TABLE_LIGHT_TRANS_TIME, 100), 
										FadeTo::create(TABLE_LIGHT_TRANS_TIME, 255), 
										NULL),
									Sequence::create(
										ScaleTo::create(TABLE_LIGHT_TRANS_TIME, 2.2f, 2.05f), 
										ScaleTo::create(TABLE_LIGHT_TRANS_TIME, 2.1f, 1.95f), 
										NULL),
									NULL));
		auto country1 = Button::create();
		country1->setTouchEnabled(true);
		country1->loadTextures("res_ui/country_icon/weiguo.png", "res_ui/country_icon/weiguo.png", "");
		country1->setTag(COUNTRY_1_TAG);
		country1->setPosition(Vec2(160,240));
		//country1->setScale(0.6f);
		country1->addTouchEventListener(CC_CALLBACK_2(CountryUI::menuSelectCountry, this));
		country1->setPressedActionEnabled(true);

		auto highLightFrame1 = ui::ImageView::create();
		highLightFrame1->loadTexture("res_ui/country_icon/lightb.png");
		highLightFrame1->setPosition(Vec2(0.5f, 0.5f));
		highLightFrame1->setLocalZOrder(-1);
		highLightFrame1->setVisible(false);
		highLightFrame1->setScaleX(2.15f);
		highLightFrame1->setScaleY(2.0f);
		highLightFrame1->setTag(HIGHLIGHT_TAG);
		country1->addChild(highLightFrame1);
		
		highLightFrame1->runAction(action);

		auto country2 = Button::create();
		country2->setTouchEnabled(true);
		country2->loadTextures("res_ui/country_icon/shuguo.png", "res_ui/country_icon/shuguo.png", "");
		country2->setTag(COUNTRY_2_TAG);
		country2->setPosition(Vec2(400,240));
		//country2->setScale(0.6f);
		country2->addTouchEventListener(CC_CALLBACK_2(CountryUI::menuSelectCountry, this));
		country2->setPressedActionEnabled(true);

		auto highLightFrame2 = ui::ImageView::create();
		highLightFrame2->loadTexture("res_ui/country_icon/lightb.png");
		highLightFrame2->setPosition(Vec2(0.5f, 0.5f));
		highLightFrame2->setLocalZOrder(-1);
		highLightFrame2->setVisible(false);
		highLightFrame2->setScaleX(2.15f);
		highLightFrame2->setScaleY(2.0f);
		highLightFrame2->setTag(HIGHLIGHT_TAG);
		country2->addChild(highLightFrame2);

		highLightFrame2->runAction(action);

		auto country3 = Button::create();
		country3->setTouchEnabled(true);
		country3->loadTextures("res_ui/country_icon/wuguo.png", "res_ui/country_icon/wuguo.png", "");
		country3->setTag(COUNTRY_3_TAG);
		country3->setPosition(Vec2(640,240));
		//country3->setScale(0.6f);
		country3->addTouchEventListener(CC_CALLBACK_2(CountryUI::menuSelectCountry, this));
		country3->setPressedActionEnabled(true);

		auto highLightFrame3 = ui::ImageView::create();
		highLightFrame3->loadTexture("res_ui/country_icon/lightb.png");
		highLightFrame3->setPosition(Vec2(0.5f, 0.5f));
		highLightFrame3->setLocalZOrder(-1);
		highLightFrame3->setVisible(false);
		highLightFrame3->setScaleX(2.15f);
		highLightFrame3->setScaleY(2.0f);
		highLightFrame3->setTag(HIGHLIGHT_TAG);
		country3->addChild(highLightFrame3);

		highLightFrame3->runAction(action);

		layer->addChild(country1);
		layer->addChild(country2);
		layer->addChild(country3);

		updateCountryButton(mCountryFlag);
		
		auto enter = Button::create();
		enter->setTouchEnabled(true);
		enter->loadTextures("res_ui/new_button_13.png", "res_ui/new_button_13.png", "");
		enter->setPosition(Vec2(400,60));
		enter->addTouchEventListener(CC_CALLBACK_2(CountryUI::enterCallBack, this));
		enter->setPressedActionEnabled(true);
		layer->addChild(enter);

		auto goldSum = Label::createWithTTF(StringDataManager::getString("CountrySelect"), APP_FONT_NAME, 24);
		goldSum->enableOutline(Color4B::BLACK,2);
		goldSum->setAnchorPoint(Vec2(0.5f,0.5f));
		goldSum->setPosition(Vec2(0, -8));
		enter->addChild(goldSum);
		
		/*
		CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("xuanzhuan.plist",48,72);
		tutorialParticle->setPosition(Vec2(0, -36));
		tutorialParticle->setAnchorPoint(Vec2(0.5f, 0.5f));
		enter->addChild(tutorialParticle);
	*/
		
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(s);
		return true;
	}
	return false;
}

bool CountryUI::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return true;
}
void CountryUI::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void CountryUI::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void CountryUI::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void CountryUI::enterCallBack(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5103, (void*)&mCountryFlag);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void CountryUI::menuSelectCountry(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		auto btn = (Button *)pSender;
		mCountryFlag = btn->getTag();

		updateCountryButton(mCountryFlag);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void CountryUI::updateCountryButton(int country)
{
	auto action = RepeatForever::create(
								Spawn::create(
									Sequence::create(
										FadeTo::create(TABLE_LIGHT_TRANS_TIME, 100), 
										FadeTo::create(TABLE_LIGHT_TRANS_TIME, 255), 
										NULL),
									Sequence::create(
										ScaleTo::create(TABLE_LIGHT_TRANS_TIME, 2.2f, 2.05f), 
										ScaleTo::create(TABLE_LIGHT_TRANS_TIME, 2.1f, 1.95f), 
										NULL),
									NULL));

	auto layer = (Layer*)getChildByTag(255);
	for(int i = 1; i <= 3; i++)
	{
		auto btn = (Button *)layer->getChildByTag(i);
		auto highlightframe = (ImageView*)btn->getChildByTag(HIGHLIGHT_TAG);
		if(i == country)
		{
			btn->setColor(Color3B(255, 255, 255));
			btn->setScale(1.0f);
			highlightframe->setVisible(true);
			highlightframe->runAction(action);
		}
		else
		{
			btn->setColor(Color3B(ROLECOLOR));
			btn->setScale(0.9f);
			highlightframe->setVisible(false);
		}
	}
}
