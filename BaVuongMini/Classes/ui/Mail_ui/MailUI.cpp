#include "MailUI.h"
#include "../extensions/UITab.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../../messageclient/element/CMailInfo.h"
#include "../backpackscene/PackageItem.h"
#include "../backpackscene/PackageScene.h"
#include "../extensions/PopupWindow.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "MailFriend.h"
#include "../extensions/Counter.h"
#include "../../messageclient/element/CAttachedProps.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../backpackscene/PacPageView.h"
#include "../extensions/CCRichLabel.h"
#include "../extensions/RichTextInput.h"
#include "MailList.h"
#include "../../messageclient/element/CMailAttachment.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../utils/StaticDataManager.h"
#include "../goldstore_ui/GoldStoreUI.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../messageclient/element/CDrug.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"
#include "../../utils/GameUtils.h"
#include "../../utils/StrUtils.h"
#include "../extensions/FloderItem.h"

using namespace CocosDenshion;

MailUI::MailUI(void)
{
	mailList_Id=0;
	selectAnnexNum=0;
	mail_attactMent=false;
	firstIsNull=true;
	mailAttachment_amount=0;
	curMailPage = 0;
	isHasNextPage = false;
	operationType =0;
	curTabType= 0;

	sendMailCollectcoins = 0;
	sendMailCollectinIngots = 0;
}


MailUI::~MailUI(void)
{
	std::vector<CMailInfo *>::iterator iter;
	for (iter=sourceDataVector.begin();iter!=sourceDataVector.end();iter++)
	{
		delete *iter;
	}
	sourceDataVector.clear();

	auto mailAnnexui1 =(FloderItemInstance *)sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON1);
	if (mailAnnexui1 != NULL)
	{
		reductionAnnex1(NULL);
	}

	auto mailAnnexui2 =(FloderItemInstance *)sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON2);
	if (mailAnnexui2 != NULL)
	{
		reductionAnnex2(NULL);
	}
}

MailUI * MailUI::create()
{
	auto mailui=new MailUI();
	if (mailui && mailui->init())
	{
		mailui->autorelease();
		return mailui;
	}
	CC_SAFE_DELETE(mailui);
	return NULL;
}

bool MailUI::init()
{
	if (UIScene::init())
	{

		auto size=Director::getInstance()->getVisibleSize();

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(size);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		if(LoadSceneLayer::mailPanel->getParent() != NULL)
		{
			LoadSceneLayer::mailPanel->removeFromParentAndCleanup(false);
		}

		auto ppanel = LoadSceneLayer::mailPanel;
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->setPosition(Vec2(size.width/2,size.height/2));
		m_pLayer->addChild(ppanel);
	
		layer_= Layer::create();
		layer_->setIgnoreAnchorPointForPosition(false);
		layer_->setAnchorPoint(Vec2(0.5f,0.5f));
		layer_->setPosition(Vec2(size.width/2,size.height/2));
		layer_->setContentSize(Size(800,480));
		this->addChild(layer_,0,MAILADDLAYER);

		const char * secondStr = StringDataManager::getString("UIName_mail_you");
		const char * thirdStr = StringDataManager::getString("UIName_mial_jian");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		layer_->addChild(atmature);

		currentPage = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_dianon");
		/////////backpage
		pageView = GameView::getInstance()->pacPageView;
		pageView->getPageView()->addEventListener((PageView::ccPageViewCallback)CC_CALLBACK_2(MailUI::CurrentPageViewChanged, this));
		pageView->setPosition(Vec2(396,97));
		layer_->addChild(pageView,0);
		pageView->setVisible(false);
		pageView->setCurUITag(kTagMailUi);
		auto seq = Sequence::create(DelayTime::create(0.05f),CallFunc::create(CC_CALLBACK_0(MailUI::PageScrollToDefault,this)),NULL);
		pageView->runAction(seq);
		//CurrentPageViewChanged(pageView);
		pageView->checkCDOnBegan();

		//refresh cd
		auto shortCutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortCutLayer)
		{
			for (int i = 0;i<7;++i)  //һ��7�����
			{
				if (i < GameView::getInstance()->shortCutList.size())
				{
					auto c_shortcut = GameView::getInstance()->shortCutList.at(i);
					if (c_shortcut->storedtype() == 2)    //����������Ʒ
					{
						if(CDrug::isDrug(c_shortcut->skillpropid()))
						{
							if (shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100))
							{
								auto shortcutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100);
								this->pageView->RefreshCD(c_shortcut->skillpropid());
							}
						}
					}
				}
			}
		}

		const char *str_shouxin = StringDataManager::getString("mail_uitab_shouxin_1");
		char *shouxin_left =const_cast<char*>(str_shouxin);

		const char *str_xiexin = StringDataManager::getString("mail_uitab_xiexin_2");
		char *xiexin_left=const_cast<char*>(str_xiexin);


		const char * highLightImage = "res_ui/tab_1_on.png";
		char * labelChange[] ={shouxin_left,xiexin_left};
		//channelLabel= UITab::createWithImage(2,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",labelChange,HORIZONTAL,5);
		channelLabel= UITab::createWithText(2,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",labelChange,HORIZONTAL,5);
		channelLabel->setAnchorPoint(Vec2(0.5f,0.5f));
		channelLabel->setPosition(Vec2(70,413));
		channelLabel->setPressedActionEnabled(true);
		channelLabel->setHighLightImage((char * )highLightImage);
		channelLabel->setDefaultPanelByIndex(0);
		channelLabel->addIndexChangedEvent(this,coco_indexchangedselector(MailUI::callBackChangeMail));
		layer_->addChild(channelLabel);
		/*
		const char * highImage = "res_ui/tab_b.png";
		char * labelMail[] ={"res_ui/youjian/xitong.png","res_ui/youjian/wanjia.png"};
		channelLabelMail= UITab::createWithImage(2,"res_ui/tab_b_off.png","res_ui/tab_b.png","",labelMail,VERTICAL,8);
		channelLabelMail->setPressedActionEnabled(true);
		channelLabelMail->setAnchorPoint(Vec2(0,0));
		channelLabelMail->setPosition(Vec2(758,405));
		channelLabelMail->setHighLightImage((char * )highImage);
		channelLabelMail->setDefaultPanelByIndex(0);
		channelLabelMail->addIndexChangedEvent(this,coco_indexchangedselector(MailUI::callBackChangeMailSender));
		layer_->addChild(channelLabelMail);
		*/
		auto button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		button_close->setTouchEnabled(true);
		button_close->setPressedActionEnabled(true);
		button_close->addTouchEventListener(CC_CALLBACK_2(MailUI::callbackClose, this));
		//receive mail
		getMailLayer=Layer::create();
		getMailLayer->setIgnoreAnchorPointForPosition(false);
		getMailLayer->setAnchorPoint(Vec2(0.5f,0.5f));
		getMailLayer->setContentSize(Size(800,480));
		getMailLayer->setPosition(Vec2(size.width/2,size.height/2));
		addChild(getMailLayer,1,TAGMAILLAYER);
		readPanel = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_shouxin");

		extractAnnex= (Button*)Helper::seekWidgetByName(ppanel,"Button_tiqufujian");
		extractAnnex->setPressedActionEnabled(true);
		extractAnnex->setTouchEnabled(true);
		extractAnnex->addTouchEventListener(CC_CALLBACK_2(MailUI::callBackExtractAnnex, this));

		auto deleteMail= (Button*)Helper::seekWidgetByName(ppanel,"Button_shanchu");
		deleteMail->setTouchEnabled(true);
		deleteMail->setPressedActionEnabled(true);
		deleteMail->addTouchEventListener(CC_CALLBACK_2(MailUI::callBackDeleteMail, this));

		replyMail= (Button *)Helper::seekWidgetByName(ppanel,"Button_huixin");
		replyMail->setTouchEnabled(true);
		replyMail->setPressedActionEnabled(true);
		replyMail->addTouchEventListener(CC_CALLBACK_2(MailUI::callBackReplyMail, this));
		replyMail->setVisible(false);

		auto mailTittleBg = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_biaotishurukuang");
		mailTittleBg->removeAllChildren();

		mailTitle=Label::createWithTTF("", APP_FONT_NAME, 16);
		mailTitle->setAnchorPoint(Vec2(0,0));
		mailTitle->setPosition(Vec2(8,8));
		mailTittleBg->addChild(mailTitle);
	
		m_scrollView = ui::ScrollView::create();
		m_scrollView->setTouchEnabled(true);
		m_scrollView->setDirection(ui::ScrollView::Direction::VERTICAL);
		m_scrollView->setContentSize(Size(285,150));
		m_scrollView->setPosition(Vec2(440,190));
		m_scrollView->setBounceEnabled(true);
		m_scrollView->setLocalZOrder(10);
		getMailLayer->addChild(m_scrollView);

		mailContentPanel = Layout::create();
		mailContentPanel->setAnchorPoint(Vec2(0,0));
		mailContentPanel->setPosition(Vec2(0,0));
		mailContentPanel->setContentSize(Size(274,362));
		mailContentPanel->setTouchEnabled(true);
		mailContentPanel->setLocalZOrder(10);
		m_scrollView->addChild(mailContentPanel);

		image_mailItem_1 = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_fubenkuang_Clone");
		image_mailItem_2 = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_fubenkuang");
		image_mailItem_1->setVisible(true);
		image_mailItem_2->setVisible(true);
		///send mail
		sendMailLayer=Layer::create();
		sendMailLayer->setIgnoreAnchorPointForPosition(false);
		sendMailLayer->setAnchorPoint(Vec2(0.5f,0.5f));
		sendMailLayer->setPosition(Vec2(size.width/2,size.height/2));
		sendMailLayer->setContentSize(Size(800,480));
		addChild(sendMailLayer,1,TAGSENDMAILLAYER);
		
// 		ImageView * pageView_kuang = ImageView::create();
// 		pageView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setScale9Enabled(true);
// 		pageView_kuang->setContentSize(Size(363,323));
// 		pageView_kuang->setCapInsets(Rect(30,30,1,1));
// 		pageView_kuang->setAnchorPoint(Vec2(0,0));
// 		pageView_kuang->setLocalZOrder(10);
// 		pageView_kuang->setPosition(Vec2(389,89));
// 		sendMailLayer->addChild(pageView_kuang);
		writePanel = (Layout*)Helper::seekWidgetByName(ppanel,"Panel_xiexin");
		
		mailNull = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_null");
		mailNull->setVisible(false);
		//friend find
		auto friendSelect=(Button *)Helper::seekWidgetByName(ppanel,"Button_haoyou");
		friendSelect->setTouchEnabled(true);
		friendSelect->setPressedActionEnabled(true);
		friendSelect->addTouchEventListener(CC_CALLBACK_2(MailUI::callBackFriendList, this));
		
		//intputBox  friendName
		friendName=new RichTextInputBox();
		//friendName->setInputBoxWidth(217);
		friendName->setInputBoxWidth(260);
		friendName->setCharLimit(10);
		friendName->setAnchorPoint(Vec2(0,0));
		friendName->setPosition(Vec2(139,370));
		friendName->setScale(0.8f);
		layer_->addChild(friendName,2,FRIENDNAMERICHINPUT);
		friendName->autorelease();
		//intputBox  title
		titleBox=new RichTextInputBox();
		//titleBox->setInputBoxWidth(217);
		titleBox->setInputBoxWidth(260);
		titleBox->setAnchorPoint(Vec2(0,0));
		titleBox->setPosition(Vec2(139,328));
		titleBox->setScale(0.8f);
		layer_->addChild(titleBox,2,TITLERICHINPUT);
		titleBox->setCharLimit(10);
		titleBox->autorelease();
		//intputBox  content
		contentBox=new RichTextInputBox();
		contentBox->setMultiLinesMode(true);
		contentBox->setInputBoxWidth(285);
        contentBox->setInputBoxHeight(150);
        contentBox->setDirection(RichTextInputBox::kCCRichInputDirectionVertical);
		contentBox->setAnchorPoint(Vec2(0,0));
		contentBox->setScale(0.9f);
		contentBox->setPosition(Vec2(75,290));
		contentBox->setCharLimit(200);
		layer_->addChild(contentBox,2,CONTENTRICHINPUT);
		contentBox->release();
		//delete
		auto cleanText= (Button*)Helper::seekWidgetByName(ppanel,"Button_qingkong");
		cleanText->setTouchEnabled(true);
		cleanText->setPressedActionEnabled(true);
		cleanText->addTouchEventListener(CC_CALLBACK_2(MailUI::callBackClean, this));
		//send mail
		auto sendMail= (Button*)Helper::seekWidgetByName(ppanel,"Button_fasong");
		sendMail->setTouchEnabled(true);
		sendMail->setPressedActionEnabled(true);
		sendMail->addTouchEventListener(CC_CALLBACK_2(MailUI::callBackSendMail, this));
		///////////
		auto button_shop= (Button*)Helper::seekWidgetByName(ppanel,"Button_shangcheng");
		button_shop->setTouchEnabled(true);
		button_shop->setPressedActionEnabled(true);
		button_shop->addTouchEventListener(CC_CALLBACK_2(MailUI::callBackShop, this));
		
		label_Gold =(Text*)Helper::seekWidgetByName(ppanel,"Labeljinbi_num");
		label_GoldIngold =(Text*)Helper::seekWidgetByName(ppanel,"Labelyuanbao_num");
		this->refreshPlayerMoney();
		//////////////////
		auto buttonCoins = (Button *)Helper::seekWidgetByName(ppanel,"button_jinbi_count");
		buttonCoins->setTouchEnabled(true);
		//buttonCoins->setPressedActionEnabled(true);
		buttonCoins->addTouchEventListener(CC_CALLBACK_2(MailUI::collectCoins, this));

		labelCoins = (Text*)Helper::seekWidgetByName(ppanel,"Labeljinbi_num_0");
		labelCoins->setString("0");

		auto buttonIngot = (Button *)Helper::seekWidgetByName(ppanel,"button_yuanbao_count");
		buttonIngot->setTouchEnabled(true);
		//buttonIngot->setPressedActionEnabled(true);
		buttonIngot->addTouchEventListener(CC_CALLBACK_2(MailUI::collectIngot, this));
		labelIngot = (Text*)Helper::seekWidgetByName(ppanel,"Labelyuanbao_num_0");
		labelIngot->setString("0");

		//imageCostgetMail = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_zhifu");
		//imageCostgetMail->setVisible(false);
		readMailCoinsImage = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_jinbikuang_0_0");
		readMailCoinsImage->setVisible(false);
		readMailCoinsLabel = (Text*)Helper::seekWidgetByName(ppanel,"Labeljinbi_num_0_0");
		readMailCoinsLabel->setString("0");

		readMailIngotImage = (ImageView *)Helper::seekWidgetByName(ppanel,"ImageView_yuanbao_0_0");
		readMailIngotImage->setVisible(false);
		readMailIngotLabel = (Text*)Helper::seekWidgetByName(ppanel,"Labelyuanbao_num_0_0");
		readMailIngotLabel->setString("0");
		///////////////
		getMailLayer->setVisible(false);
		readPanel->setVisible(false);
		mailNull->setVisible(true);

		//channelLabelMail->setVisible(true);
		writePanel->setVisible(false);
		sendMailLayer->setVisible(false);
		friendName->setVisible(false);
		titleBox->setVisible(false);
		contentBox->setVisible(false);
		
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(size);
		return true;
	}
	return false;
}

void MailUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);

	for (int i=0;i<GameView::getInstance()->mailVectorOfSystem.size();i++)
	{
		auto m_mailSource = new CMailInfo();
		m_mailSource->CopyFrom(*GameView::getInstance()->mailVectorOfSystem.at(i));

		sourceDataVector.push_back(m_mailSource);
	}

	mailList=MailList::create();
	mailList->setIgnoreAnchorPointForPosition(false);
	mailList->setAnchorPoint(Vec2(0,0));
	mailList->setPosition(Vec2(74,42));
	layer_->addChild(mailList,3,MAILLISTTAG);
	mailList->init();
	mailList->setVisible(false);
}

void MailUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

void MailUI::callBackChangeMail( Ref * obj )
{
	auto m_tab=(UITab *)obj;
	int num= m_tab->getCurrentIndex();	
	switch(num)
	{
	case 0:
		{
			readPanel->setVisible(true);
			mailList->setVisible(true);
			if (sourceDataVector.size()>0)
			{
				mailNull->setVisible(false);
				getMailLayer->setVisible(true);
				readPanel->setVisible(true);
				//channelLabelMail->setVisible(true);
				mailList->setVisible(true);
			}else
			{
				mailNull->setVisible(true);
				getMailLayer->setVisible(false);
				readPanel->setVisible(false);
				//channelLabelMail->setVisible(true);
				mailList->setVisible(false);
			}

			writePanel->setVisible(false);
			sendMailLayer->setVisible(false);
			friendName->setVisible(false);
			titleBox->setVisible(false);
			contentBox->setVisible(false);
			pageView->setVisible(false);
		}break;
	case 1:
		{
			readPanel->setVisible(false);
			getMailLayer->setVisible(false);
			mailList->setVisible(false);
			mailNull->setVisible(false);
			//channelLabelMail->setVisible(false);
			writePanel->setVisible(true);
			sendMailLayer->setVisible(true);
			friendName->setVisible(true);
			titleBox->setVisible(true);
			contentBox->setVisible(true);
			pageView->setVisible(true);
		}break;
	}
}

void MailUI::callBackSendMail(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		const char * playerName = friendName->getInputString();
		if (playerName == NULL)
		{
			const char *str_ = StringDataManager::getString("mailui_sendPlayer");
			GameView::getInstance()->showAlertDialog(str_);
			return;
		}
		const char* titleText = titleBox->getInputString();
		const char *contentText = contentBox->getInputString();
		if (titleText == NULL && contentText == NULL)
		{
			const char *str_ = StringDataManager::getString("mailui_sendTittle");
			GameView::getInstance()->showAlertDialog(str_);
			return;
		}
		if (titleText != NULL)
		{
			mail_subject = titleText;
		}

		if (contentText != NULL)
		{
			mail_text = contentText;
		}

		if (mailAttachment_amount >0)
		{
			mail_attactMent = true;
		}
		receiverName = playerName;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2000, this);

		GameUtils::playGameSound(MAIL_SEND, 2, false);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MailUI::callBackFriendList(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto mailFriendui = (MailFriend *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagPopFriendListUI);
		if (mailFriendui == NULL)
		{
			auto winsize = Director::getInstance()->getVisibleSize();
			auto mailFriend = MailFriend::create(kTagMailUi);
			mailFriend->setIgnoreAnchorPointForPosition(false);
			mailFriend->setAnchorPoint(Vec2(0.5f, 0.5f));
			mailFriend->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			GameView::getInstance()->getMainUIScene()->addChild(mailFriend, 3, kTagPopFriendListUI);

			FriendStruct friend1 = { 0,20,mailFriend->setFriendsPage,0 };
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202, &friend1);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MailUI::callBackClean(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		contentBox->deleteAllInputString();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}


void MailUI::callBackAddexan( Ref * obj )
{
	if (this->isClosing())
		return;

	auto btn=(Button * )obj;
	int btnId=btn->getTag();

	if (mailAttachment_amount <2)
	{
		int goodsNum = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity();
		if (goodsNum >1)
		{
			GameView::getInstance()->showCounter(this, callfuncO_selector(MailUI::counterAnnex),goodsNum);
		}else
		{
			this->counterAnnex(NULL);
		}

	}else
	{		
		const char *str_ = StringDataManager::getString("mailui_addAnnex");
		GameView::getInstance()->showAlertDialog(str_);	
	}

}

void MailUI::callBackDeleteMail(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (sourceDataVector.size() <= 0)
		{
			return;
		}
		std::string string_ = "";
		if (sourceDataVector.size() <= mailList_Id)
		{
			mailList_Id--;
		}
		if (sourceDataVector.at(mailList_Id)->attachment_size() > 0 && sourceDataVector.at(mailList_Id)->mutable_attachment(0)->state() == 0)
		{
			const char * mailOfAttachement = StringDataManager::getString("mailui_ofAttachsExit");
			string_.append(mailOfAttachement);
		}
		const char * mailOfDelete = StringDataManager::getString("mailui_delete");
		string_.append(mailOfDelete);
		/*
		Size winSize=Director::getInstance()->getVisibleSize();
		PopupWindow * popupWindow_ =PopupWindow::create(string_,2,PopupWindow::KtypeNeverRemove,10);
		popupWindow_->setIgnoreAnchorPointForPosition(false);
		popupWindow_->setAnchorPoint(Vec2(0.5f,0.5f));
		popupWindow_->setPosition(Vec2(winSize.width/2,winSize.height/2));
		popupWindow_->setButtonPosition();
		popupWindow_->AddcallBackEvent(this,callfuncO_selector(MailUI::deleteMailSure),NULL);
		GameView::getInstance()->getMainUIScene()->addChild(popupWindow_, 100);
		*/

		GameView::getInstance()->showPopupWindow(string_, 2, this, callfuncO_selector(MailUI::deleteMailSure), NULL);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void MailUI::callBackExtractAnnex(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (sourceDataVector.size() <= 0)
		{
			return;
		}
		if (sourceDataVector.size() <= mailList_Id)
		{
			mailList_Id--;
		}
		int gold_ = sourceDataVector.at(mailList_Id)->gold();
		int goldIngot_ = sourceDataVector.at(mailList_Id)->goldingot();
		if (gold_> 0 || goldIngot_ > 0)
		{
			std::string tempString = "";
			const char* string_ = StringDataManager::getString("mail_payToMail");
			tempString.append(string_);
			if (gold_ > 0)
			{
				char goldStr[50];
				const char* templateStr = StringDataManager::getString("mail_payToMailOfGold");
				sprintf(goldStr, templateStr, gold_);
				//tempString.append(StrUtils::applyColor(goldStr,Color3B(255,0,0)).c_str());
				tempString.append(goldStr);
			}
			if (gold_ > 0 && goldIngot_ > 0)
			{
				const char* templateStr = StringDataManager::getString("mail_payToMailOfGoldAnd");
				tempString.append(templateStr);
			}

			if (goldIngot_ > 0)
			{
				char goldIngotStr[50];
				const char* templateStr = StringDataManager::getString("mail_payToMailOfGoldIngot");
				sprintf(goldIngotStr, templateStr, goldIngot_);
				tempString.append(goldIngotStr);
			}
			const char* tempStringPayTomail = StringDataManager::getString("mail_payToMailOfSureGetback");
			tempString.append(tempStringPayTomail);
			GameView::getInstance()->showPopupWindow(tempString, 2, this, callfuncO_selector(MailUI::callBackSurePayToMoney), NULL);
		}
		else
		{
			operationType = 3;
			long long mailindex_ = sourceDataVector.at(mailList_Id)->id();
			mailList->tableView->cellAtIndex(mailList_Id);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2002, (void *)3, (void *)mailindex_);
			GameUtils::playGameSound(ANNEX_EXTRACT, 2, false);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void MailUI::callBackSurePayToMoney( Ref * obj )
{
	operationType = 3;
	long long mailindex_ =sourceDataVector.at(mailList_Id)->id();
	mailList->tableView->cellAtIndex(mailList_Id);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2002,(void *)3,(void *)mailindex_);
	GameUtils::playGameSound(ANNEX_EXTRACT, 2, false);
}

void MailUI::deleteMailSure( Ref * obj )
{	
	if (sourceDataVector.size() <=mailList_Id)
	{
		mailList_Id--;
	}
	operationType = 2;
	long long deleteMailId =sourceDataVector.at(mailList_Id)->id();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2002,(void *)2,(void *)deleteMailId);
	GameUtils::playGameSound(MAIL_DELETE, 2, false);
	//operation vector
	std::vector<CMailInfo *>::iterator iter= sourceDataVector.begin() + mailList_Id;
	auto mailV=*iter ;
	sourceDataVector.erase(iter);
	delete mailV;

	auto tabMail=(TableView *)mailList->getChildByTag(MAILTABTAG);
	tabMail->reloadData();
	showMailInfo(mailList_Id);

	for (int i=0;i<GameView::getInstance()->mailVectorOfSystem.size();i++)
	{
		if (GameView::getInstance()->mailVectorOfSystem.at(i)->id()==deleteMailId)
		{
			std::vector<CMailInfo *>::iterator iter= GameView::getInstance()->mailVectorOfSystem.begin() + i;
			auto systemMailV=*iter ;
			GameView::getInstance()->mailVectorOfSystem.erase(iter);
			delete systemMailV;
		}
	}
	/*
	for (int i=0;i<GameView::getInstance()->mailVectorOfPlayer.size();i++)
	{
		if (GameView::getInstance()->mailVectorOfPlayer.at(i)->id()==deleteMailId)
		{
			std::vector<CMailInfo *>::iterator iter= GameView::getInstance()->mailVectorOfPlayer.begin() + i;
			CMailInfo * systemMailV=*iter ;
			GameView::getInstance()->mailVectorOfPlayer.erase(iter);
			delete systemMailV;
		}
	}
	*/
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	//mainScene->checkIsNewRemind();
	mainScene->remindMail();
}

void MailUI::callBackReplyMail(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (mailList_Id <0 || sourceDataVector.size() <= 0 || sourceDataVector.size() < mailList_Id + 1)
		{
			return;
		}
		std::string sendName_ = sourceDataVector.at(mailList_Id)->sendername();
		friendName->deleteAllInputString();
		friendName->onTextFieldInsertText(NULL, sendName_.c_str(), 30);

		channelLabel->setDefaultPanelByIndex(1);

		readPanel->setVisible(false);
		getMailLayer->setVisible(false);
		mailList->setVisible(false);
		//channelLabelMail->setVisible(false);
		writePanel->setVisible(true);
		sendMailLayer->setVisible(true);
		friendName->setVisible(true);
		titleBox->setVisible(true);
		contentBox->setVisible(true);
		pageView->setVisible(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void MailUI::callBackChangeMailSender( Ref * obj )
{
	/*
	UITab * m_tab=(UITab *)obj;
	int num= m_tab->getCurrentIndex();

	std::vector<CMailInfo *>::iterator iter;
	for (iter=sourceDataVector.begin();iter!=sourceDataVector.end();iter++)
	{
		delete *iter;
	}
	sourceDataVector.clear();
	
	switch (num)
	{
	case 0:
		{
			curTabType = 0;
			curMailPage = 0;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void *)curMailPage);
			replyMail->setVisible(false);
		}break;
	case 1:
		{
			curTabType =1;
			replyMail->setVisible(true);
			curMailPage = 0;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void *)curMailPage);

		}break;
	}
	*/
}

void MailUI::callBackShop(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
		if (goldStoreUI == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			goldStoreUI = GoldStoreUI::create();
			GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI, 0, kTagGoldStoreUI);

			goldStoreUI->setIgnoreAnchorPointForPosition(false);
			goldStoreUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			goldStoreUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MailUI::CurrentPageViewChanged(Ref *pSender, PageView::EventType type)
{

	if (type == PageView::EventType::TURNING) {
		auto pageView = dynamic_cast<PageView*>(pSender);
		switch (static_cast<int>(pageView->getCurrentPageIndex()))
		{
		case 0:
			currentPage->setPosition(Vec2(523, 72));
			break;
		case 1:
			currentPage->setPosition(Vec2(545, 72));
			break;
		case 2:
			currentPage->setPosition(Vec2(565, 72));
			break;
		case 3:
			currentPage->setPosition(Vec2(586, 72));
			break;
		case 4:
			currentPage->setPosition(Vec2(607, 72));
			break;
		}
	}
}

void MailUI::counterAnnex( Ref * obj )
{	
	auto couter_ = (Counter*)obj;
	if (couter_ != NULL)
	{
		mail_amount =couter_->getInputNum();
	}else
	{
		mail_amount =1;
	}
	
	if (mail_amount <= 0)
	{
		const char *strings = StringDataManager::getString("mail_country");
		GameView::getInstance()->showAlertDialog(strings);
		return;
	}

	int goodsNum_ = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity();
	if (mail_amount>goodsNum_ )
	{
		mail_amount = goodsNum_;
	}

	if (sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON1) != NULL)
	{
		auto annex_1 = (FloderItemInstance *)sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON1);

		if (pageView->curPackageItemIndex == annex_1->getFloderIndex())
		{
			reductionAnnex1(NULL);
		}
	}

	if (sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON2) != NULL)
	{
		auto annex_2 = (FloderItemInstance *)sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON2);

		if (pageView->curPackageItemIndex == annex_2->getFloderIndex())
		{
			reductionAnnex2(NULL);
		}
	}


	packageIndex = pageView->curPackageItemIndex;

	auto temp = new CAttachedProps();
	temp->set_folderidx(packageIndex);
	temp->set_amount(mail_amount);
	attachePropVector.push_back(temp);

	if (GameView::getInstance()->AllPacItem.at(packageIndex)->has_goods())
	{
		int goodsCount_ = GameView::getInstance()->AllPacItem.at(packageIndex)->quantity();
		char goodsUseNum[50];
		sprintf(goodsUseNum,"%d",mail_amount);
		char goodsAllNum[50];
		sprintf(goodsAllNum,"%d",goodsCount_);
		std::string goodsUseAndCount = goodsUseNum;
		goodsUseAndCount.append("/");
		goodsUseAndCount.append(goodsAllNum);

		GameView::getInstance()->pacPageView->SetCurFolderGray(true,packageIndex,goodsUseAndCount);
	}

	if (firstIsNull == true)
	{
		auto temp_folder_ = new FolderInfo();
		temp_folder_->CopyFrom(*GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex));
		auto mail_Item = FloderItemInstance::create(temp_folder_,FloderItemInstance::ktype_mail_addAnnex,mail_amount);
		mail_Item->setIgnoreAnchorPointForPosition(false);
		mail_Item->setAnchorPoint(Vec2(0,0));
		mail_Item->setPosition(Vec2(79,96));
		mail_Item->setTag(REDUCTIONANNEXBUTTON1);
		sendMailLayer->addChild(mail_Item);
		delete temp_folder_;
		firstIsNull=false;
	}else
	{
		auto temp_folder_ = new FolderInfo();
		temp_folder_->CopyFrom(*GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex));
		auto mail_Item = FloderItemInstance::create(temp_folder_,FloderItemInstance::ktype_mail_addAnnex,mail_amount);
		mail_Item->setIgnoreAnchorPointForPosition(false);
		mail_Item->setAnchorPoint(Vec2(0,0));
		mail_Item->setPosition(Vec2(153,96));
		mail_Item->setTag(REDUCTIONANNEXBUTTON2);
		sendMailLayer->addChild(mail_Item);
		delete temp_folder_;
	}
	mailAttachment_amount++;
}

void MailUI::reductionAnnex1( Ref *obj )
{
	auto mailAnnexui =(FloderItemInstance *)sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON1);
	if (mailAnnexui == NULL)
	{
		return;
	}
	mailAnnexui->removeFromParentAndCleanup(true);
	firstIsNull=true;
	mailAttachment_amount--;
	//selectAnnexNum--;

	if (GameView::getInstance()->AllPacItem.at(attachePropVector.at(0)->folderidx())->has_goods())
	{
		GameView::getInstance()->pacPageView->SetCurFolderGray(false,attachePropVector.at(0)->folderidx());
	}

	if (mailAttachment_amount <= 0)
	{
		sendMailCollectcoins =  0;
		labelCoins->setString("0");
		sendMailCollectinIngots =  0;
		labelIngot->setString("0");
	}

	std::vector<CAttachedProps *>::iterator iter=attachePropVector.begin();
	auto attache=*iter;
	attachePropVector.erase(iter);		
	delete attache;
}
void MailUI::reductionAnnex2( Ref *obj )
{
	auto mailAnnexui =(FloderItemInstance *)sendMailLayer->getChildByTag(REDUCTIONANNEXBUTTON2);
	if (mailAnnexui == NULL)
	{
		return;
	}
	mailAnnexui->removeFromParentAndCleanup(true);
	mailAttachment_amount--;
	//selectAnnexNum--;

	if (attachePropVector.size()>1)
	{
		if (GameView::getInstance()->AllPacItem.at(attachePropVector.at(1)->folderidx())->has_goods())
		{
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,attachePropVector.at(1)->folderidx());
		}

		std::vector<CAttachedProps *>::iterator iter=attachePropVector.begin()+1;
		auto attache=*iter;
		attachePropVector.erase(iter);	
		delete attache; 
	}else
	{
		if (GameView::getInstance()->AllPacItem.at(attachePropVector.at(0)->folderidx())->has_goods())
		{
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,attachePropVector.at(0)->folderidx());
		}

		std::vector<CAttachedProps *>::iterator iter=attachePropVector.begin();
		auto attache=*iter;
		attachePropVector.erase(iter);	
		delete attache;
	}

	if (mailAttachment_amount <= 0)
	{
		sendMailCollectcoins =  0;
		labelCoins->setString("0");
		sendMailCollectinIngots =  0;
		labelIngot->setString("0");
	}
}

void MailUI::callbackClose(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void MailUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void MailUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

bool MailUI::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return resignFirstResponder(pTouch,this,false);
}

void MailUI::showMailInfo( int idx )
{
	mailContentPanel->removeAllChildren();
	mailTitle->setVisible(false);
	for (int i=0;i<2;i++)
	{
		auto goodsui = (GoodsItemInfoBase *)getMailLayer->getChildByTag(MAILANNEXT+i);
		if (goodsui!= NULL)
		{
			goodsui->removeFromParentAndCleanup(true);
		}
	}

	image_mailItem_1->setVisible(true);
	image_mailItem_2->setVisible(true);

	if (sourceDataVector.size() > idx)
	{
		if (sourceDataVector.at(idx)->sender() == LLONG_MAX)
		{
			replyMail->setVisible(false);
		}else
		{
			replyMail->setVisible(true);
		}

		std::string labelSubject =sourceDataVector.at(idx)->subject();
		std::string labelText = sourceDataVector.at(idx)->text();
		std::string labelName =sourceDataVector.at(idx)->sendername();
		long long timeSecond = sourceDataVector.at(idx)->sendtime();

		mailTitle->setString(labelSubject.c_str());
		mailTitle->setVisible(true);
		auto mailContentTest = Label::createWithTTF(labelText.c_str(), APP_FONT_NAME, 16);
		mailContentTest->setAnchorPoint(Vec2(0,0));
		mailContentPanel->addChild(mailContentTest);

		//get m_scrollView height and width
		int touchHight=mailContentTest->getContentSize().height;
		int innerWidth=m_scrollView->getBoundingBox().size.width;
		int innerHeight=m_scrollView->getBoundingBox().size.height + touchHight/2;
		m_scrollView->setInnerContainerSize(Size(innerWidth,innerHeight));
		
		mailContentTest->setPosition(Vec2(10,innerHeight - touchHight));
		if (sourceDataVector.at(idx)->goldingot() > 0|| sourceDataVector.at(idx)->gold() > 0)
		{
			char coinstr[10];
			sprintf(coinstr,"%d",sourceDataVector.at(idx)->gold());
			char ingotstr[10];
			sprintf(ingotstr,"%d",sourceDataVector.at(idx)->goldingot());
			//imageCostgetMail->setVisible(true);
			readMailIngotImage->setVisible(true);
			readMailCoinsImage->setVisible(true);
			readMailCoinsLabel->setString(coinstr);
			readMailIngotLabel->setString(ingotstr);
		}else
		{
			//imageCostgetMail->setVisible(false);
			readMailIngotImage->setVisible(false);
			readMailCoinsImage->setVisible(false);
		}

		if (sourceDataVector.at(idx)->attachment_size()>0 && sourceDataVector.at(idx)->mutable_attachment(0)->state()==0)//�и�� && 0 �δ��ȡ
		{
			extractAnnex->setVisible(true);
			int vectorSize = sourceDataVector.at(idx)->attachment_size();
			for (int index=0;index<vectorSize;index++)
			{
				auto goodsInfo =new GoodsInfo();
				goodsInfo->CopyFrom(sourceDataVector.at(idx)->mutable_attachment(index)->good());
				
				int goodsAmount = sourceDataVector.at(idx)->mutable_attachment(index)->amount();
				auto mailgoodsinfo =GoodsItemInstance::create(goodsInfo,GoodsItemInstance::ktype_mail_getBackAnnex,goodsAmount);
				mailgoodsinfo->setIgnoreAnchorPointForPosition(false);
				mailgoodsinfo->setAnchorPoint(Vec2(0,0));
				mailgoodsinfo->setScale(0.95f);
				mailgoodsinfo->setPosition(Vec2(440.2f +74*index,106));
				mailgoodsinfo->setTag(MAILANNEXT+ index);
				getMailLayer->addChild(mailgoodsinfo);
				delete goodsInfo;

				if (index == 0)
				{
					image_mailItem_1->setVisible(false);	
				}
				if(index == 1)
				{					
					image_mailItem_2->setVisible(false);
				}
				

			}
		}else
		{
			extractAnnex->setVisible(false);
		}
	}
}

void MailUI::refreshPlayerMoney()
{
	int m_goldValue = GameView::getInstance()->getPlayerGold();
	char GoldStr_[20];
	sprintf(GoldStr_,"%d",m_goldValue);
	label_Gold->setString(GoldStr_);

	int m_goldIngot =  GameView::getInstance()->getPlayerGoldIngot();
	char GoldIngotStr_[20];
	sprintf(GoldIngotStr_,"%d",m_goldIngot);
	label_GoldIngold->setString(GoldIngotStr_);
}

void MailUI::collectCoins(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->isClosing())
			return;

		int openlevel = 0;
		std::map<int, int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(14);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[14];
		}
		int selfLevel_ = GameView::getInstance()->myplayer->getActiveRole()->level();
		if (selfLevel_ >= openlevel)
		{
			if (mailAttachment_amount <= 0)
			{
				const char *strings_ = StringDataManager::getString("mail_pleaseAddExtranFirst");
				GameView::getInstance()->showAlertDialog(strings_);
			}
			else
			{
				GameView::getInstance()->showCounter(this, callfuncO_selector(MailUI::setCollectCoinsValue));
			}
		}
		else
		{
			char openLevelStr[100];
			const char *strings = StringDataManager::getString("mail_OfMoneyOpenLevel");
			sprintf(openLevelStr, strings, openlevel);
			GameView::getInstance()->showAlertDialog(openLevelStr);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MailUI::collectIngot(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->isClosing())
			return;

		int openlevel = 0;
		std::map<int, int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(14);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // �û�ҵ�����ָ�END��  
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[14];
		}
		int selfLevel_ = GameView::getInstance()->myplayer->getActiveRole()->level();
		if (selfLevel_ >= openlevel)
		{
			if (mailAttachment_amount <= 0)
			{
				const char *strings_ = StringDataManager::getString("mail_pleaseAddExtranFirst");
				GameView::getInstance()->showAlertDialog(strings_);
			}
			else
			{
				GameView::getInstance()->showCounter(this, callfuncO_selector(MailUI::setCollectIngotValue));
			}
		}
		else
		{
			char openLevelStr[100];
			const char *strings = StringDataManager::getString("mail_OfMoneyOpenLevel");
			sprintf(openLevelStr, strings, openlevel);
			GameView::getInstance()->showAlertDialog(openLevelStr);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MailUI::setCollectCoinsValue( Ref * obj )
{
	auto count_ = (Counter *)obj;
	int num_ = count_->getInputNum();

	sendMailCollectcoins =  num_;
	char numStr[10];
	sprintf(numStr,"%d",num_);
	labelCoins->setString(numStr);
	
}

void MailUI::setCollectIngotValue( Ref * obj )
{
	auto count_ = (Counter *)obj;
	int num_ = count_->getInputNum();
	sendMailCollectinIngots =  num_;
	char numStr[10];
	sprintf(numStr,"%d",num_);
	labelIngot->setString(numStr);
}

void MailUI::PageScrollToDefault()
{
	pageView->getPageView()->scrollToPage(0);
	CurrentPageViewChanged(pageView, PageView::EventType::TURNING);
}




