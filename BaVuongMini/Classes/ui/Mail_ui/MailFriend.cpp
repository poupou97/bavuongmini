#include "MailFriend.h"
#include "GameView.h"
#include "../Friend_ui/FriendInfo.h"
#include "MailUI.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "AppMacros.h"
#include "../../messageclient/element/CRelationPlayer.h"

MailFriend::MailFriend(void)
{
	setFriendsPage =0;
	uitag_ = 0;
}


MailFriend::~MailFriend(void)
{
	std::vector<CRelationPlayer *>::iterator iter_relation;
	for (iter_relation=GameView::getInstance()->relationSourceVector.begin();iter_relation!=GameView::getInstance()->relationSourceVector.end();iter_relation++)
	{
		delete *iter_relation;
	}
	GameView::getInstance()->relationSourceVector.clear();
}

MailFriend* MailFriend::create(int usedUiTag)
{
	auto mailfriend=new MailFriend();
	if (mailfriend && mailfriend->init(usedUiTag))
	{
		mailfriend->autorelease();
		return mailfriend;
	}
	CC_SAFE_DELETE(mailfriend);
	return NULL;
}

bool MailFriend::init(int usedUiTag)
{
	if (UIScene::init())
	{
		auto panel=LoadSceneLayer::mailFriendPopup;
		panel->setAnchorPoint(Vec2(0,0));
		panel->setPosition(Vec2(0,0));
		m_pLayer->addChild(panel);

		uitag_ = usedUiTag;

		auto tableView = TableView::create(this, Size(675,310));
		tableView->setDirection(TableView::Direction::VERTICAL);
		tableView->setAnchorPoint(Vec2(0,0));
		tableView->setPosition(Vec2(36,48));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(tableView,0,MAILFRIENDTABVIEW);
		tableView->reloadData();

// 		cocos2d::extension::Scale9Sprite * pageView_kuang = cocos2d::extension::Scale9Sprite::create("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setPreferredSize(Size(658,330));
// 		pageView_kuang->setCapInsets(Rect(32,32,1,1));
// 		pageView_kuang->setAnchorPoint(Vec2(0,0));
// 		pageView_kuang->setPosition(Vec2(53,41));
// 		addChild(pageView_kuang,1);

		labelBM_isNotFriend = (Text*)Helper::seekWidgetByName(panel,"Label_friendsIsNull");
		labelBM_isNotFriend->setVisible(false);
		
		auto layer_ = Layer::create();
		addChild(layer_,2);
		auto button_close= Button::create();
		button_close->loadTextures("res_ui/close.png","res_ui/close.png","");
		button_close->setTouchEnabled(true);
		button_close->setPressedActionEnabled(true);
		button_close->addTouchEventListener(CC_CALLBACK_2(MailFriend::callBackExit, this));
		button_close->setAnchorPoint(Vec2(0.5f,0.5f));
		//button_close->setPosition(Vec2(panel->getContentSize().width,panel->getContentSize().height));
		button_close->setPosition(Vec2(718,372));
		layer_->addChild(button_close);

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(panel->getContentSize());

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(MailFriend::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(MailFriend::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(MailFriend::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(MailFriend::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void MailFriend::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void MailFriend::onExit()
{
	UIScene::onExit();
}

void MailFriend::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
}

cocos2d::Size MailFriend::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(640,80);
}

cocos2d::extension::TableViewCell* MailFriend::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell =table->dequeueCell();// this method must be called

	cell=new TableViewCell();
	cell->autorelease();
	int friendNum = GameView::getInstance()->relationSourceVector.size();
	if (idx == friendNum/2)
	{
		if (friendNum%2 == 0)
		{
			auto friendInfoL =FriendInfo::create(2*idx,uitag_);
			friendInfoL->setAnchorPoint(Vec2(0.5f,0.5f));
			friendInfoL->setPosition(Vec2(-5,-10));
			friendInfoL->setScale(0.95f);
			cell->addChild(friendInfoL);

			auto friendInfoR =FriendInfo::create(2*idx+1,uitag_);
			friendInfoR->setAnchorPoint(Vec2(0.5f,0.5f));
			friendInfoR->setPosition(Vec2(315,-10));
			friendInfoR->setScale(0.95f);
			cell->addChild(friendInfoR);
		}else
		{
			auto friendInfoL =FriendInfo::create(2*idx,uitag_);
			friendInfoL->setAnchorPoint(Vec2(0.5f,0.5f));
			friendInfoL->setPosition(Vec2(-5,-10));
			friendInfoL->setScale(0.95f);
			cell->addChild(friendInfoL);
		}
	}
	else
	{
		auto friendInfoL =FriendInfo::create(2*idx,uitag_);
		friendInfoL->setAnchorPoint(Vec2(0.5f,0.5f));
		friendInfoL->setPosition(Vec2(-5,-10));
		friendInfoL->setScale(0.95f);
		cell->addChild(friendInfoL);

		auto friendInfoR =FriendInfo::create(2*idx+1,uitag_);
		friendInfoR->setAnchorPoint(Vec2(0.5f,0.5f));
		friendInfoR->setScale(0.95f);
		friendInfoR->setPosition(Vec2(315,-10));
		cell->addChild(friendInfoR);
	}
	
	if(idx == friendNum/2 - 2)
	{
		if (setFriendsPage < totalFriendsPageNum - 1)
		{
			setFriendsPage++;
			FriendStruct friend1={0,20,setFriendsPage,0};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);
		}
	}

	return cell;
}

ssize_t MailFriend::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	int friendnum =GameView::getInstance()->relationSourceVector.size();

	if (friendnum%2 == 0)
	{
		return friendnum/2;
	}else
	{
		return friendnum/2+1;
	}
}

void MailFriend::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{
	
}

void MailFriend::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

bool MailFriend::onTouchBegan( Touch *pTouch, Event *pEvent )
{

	return resignFirstResponder(pTouch,this,false);
}

void MailFriend::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void MailFriend::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void MailFriend::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void MailFriend::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void MailFriend::setShowBMfriendIsNull( bool value_ )
{
	labelBM_isNotFriend->setVisible(value_);
}




