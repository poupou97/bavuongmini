#pragma once
#include "cocos-ext.h"
#include "cocos2d.h"
#include "../../ui/extensions/UIScene.h"

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
class PacPageView;
class CCRichLabel;
class MailList;
class AnnexItem;
class RichTextInputBox;
class CMailInfo;
class CAttachedProps;


typedef enum
{
	FRIENDNAMERICHINPUT = 301,
	TITLERICHINPUT = 302,
	CONTENTRICHINPUT = 303,

	MAILADDLAYER=320,

	MAILLISTTAG=304,
	TAGMAILLAYER=305,
	TAGSENDMAILLAYER=306
	//TAGMAILFIRND=307
};
typedef enum{
	REDUCTIONANNEXBUTTON1=311,
	REDUCTIONANNEXLAB1=312,
	REDUCTIONANNEXBUTTON2=313,
	REDUCTIONANNEXLAB2=314,

	ANNEXITEM1=315,
	ANNEXITEM2=316,

	MAILANNEXT = 319
};

class MailUI:public UIScene,public ScrollViewDelegate
{
public:
	MailUI(void);
	~MailUI(void);

	static MailUI * create();
	bool init();
	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);

	void callbackClose(Ref *pSender, Widget::TouchEventType type);
	void callBackChangeMail(Ref * obj);
	void callBackChangeMailSender(Ref * obj);

	void callBackFriendList(Ref *pSender, Widget::TouchEventType type);
	void callBackShop(Ref *pSender, Widget::TouchEventType type);

	Text * label_Gold;
	Text * label_GoldIngold;
	void refreshPlayerMoney();
public:
	void callBackExtractAnnex(Ref *pSender, Widget::TouchEventType type);
	void callBackReplyMail(Ref *pSender, Widget::TouchEventType type);
	void callBackDeleteMail(Ref *pSender, Widget::TouchEventType type);
	
	void callBackSurePayToMoney(Ref * obj);
	void deleteMailSure(Ref * obj);
	int curTabType;
	Label *mailTitle;
	Layout * mailContentPanel;
	ui::ScrollView * m_scrollView;
	void showMailInfo(int idx);

	ImageView * image_mailItem_1;
	ImageView * image_mailItem_2;
public:
	//write
	void counterAnnex(Ref * obj);
	void callBackAddexan(Ref * obj);

	void reductionAnnex1(Ref *obj);
	void reductionAnnex2(Ref *obj);

	void callBackClean(Ref *pSender, Widget::TouchEventType type);
	int sendMailCollectcoins;
	int sendMailCollectinIngots;
	void callBackSendMail(Ref *pSender, Widget::TouchEventType type);
public:
	void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
public:
	int curMailPage;
	bool isHasNextPage;
	int operationType;
	std::vector<CMailInfo *> sourceDataVector;
	std::vector<CAttachedProps *> attachePropVector;

	void collectCoins(Ref *pSender, Widget::TouchEventType type);
	void setCollectCoinsValue(Ref * obj);
	void collectIngot(Ref *pSender, Widget::TouchEventType type);
	void setCollectIngotValue(Ref * obj);
	Text * labelCoins;
	Text * labelIngot;
	//ImageView * imageCostgetMail;
	ImageView * readMailCoinsImage;
	Text * readMailCoinsLabel;
	ImageView * readMailIngotImage;
	Text * readMailIngotLabel;
public:
	Layer * layer_;
	// mail info
	int receiverId; 
	std::string receiverName;
	std::string mail_subject;
	std::string mail_text;
	bool mail_attactMent;
	int mail_amount;//attach amount

	MailList * mailList;
	int selectAnnexNum;// annex rame num
	bool firstIsNull;
	int mailList_Id;
public:
	int mailAttachment_amount;
	int packageIndex;
	Layer * getMailLayer;
	Layer * sendMailLayer;
	UITab * channelLabel;
	PacPageView * pageView;
	RichTextInputBox * friendName;

	Layout * readPanel;
	Layout * writePanel;
	Layout * mailNull;
	//UITab * channelLabelMail;
	Button * replyMail;
	Button * extractAnnex;
	//Label *labelMailNum_;
	RichTextInputBox * titleBox;
	RichTextInputBox * contentBox;
	ImageView* currentPage;
	void CurrentPageViewChanged(Ref *pSender, PageView::EventType type);

	void PageScrollToDefault();
private:
	struct FriendStruct
	{
		int relationType;
		int pageNum;
		int pageIndex;
		int showOnline;
	};
};
