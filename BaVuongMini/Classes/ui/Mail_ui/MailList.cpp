#include "MailList.h"
#include "MailUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../extensions/CCMoveableMenu.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/element/CMailInfo.h"
#include "../extensions/CCRichLabel.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/GameUtils.h"

// the mail in the mailbox will be deleted after some days
#define MAIL_KEEP_TIME_NORMAL 14   // day
#define MAIL_KEEP_TIME_WITH_ATTACHMENT 30
#define  MAIL_KEEP_TIME_WITH_NEEDTOPAY 2

#define TOTAL_SECOND_FOR_ONE_DAY 86400
#define  SelectImageTag 458

MailList::MailList(void)
{
	lastSelectCellId =0;
}


MailList::~MailList(void)
{
}

MailList * MailList::create()
{
	auto mailList=new MailList();
	mailList->autorelease();
	return mailList;

}

bool MailList::init()
{
	if (UIScene::init())
	{
		auto winsize =Director::getInstance()->getVisibleSize();

		tableView = TableView::create(this, Size(270, 360));
		//tableView->setSelectedEnable(true);
		//tableView->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 24, 1, 1), Vec2(0,0));
		tableView->setDirection(TableView::Direction::VERTICAL);
		tableView->setAnchorPoint(Vec2(0,0));
		tableView->setPosition(Vec2(0,0));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		//tableView->setPressedActionEnabled(true);
		addChild(tableView,1,MAILTABTAG);
		tableView->reloadData();

// 		pageView_kuang = cocos2d::extension::Scale9Sprite::create("res_ui/LV5_dikuang_miaobian1.png");
// 		pageView_kuang->setPreferredSize(Size(287,376));
// 		pageView_kuang->setCapInsets(Rect(32,32,1,1));
// 		pageView_kuang->setAnchorPoint(Vec2(0,0));
// 		pageView_kuang->setPosition(Vec2(-7,-10));
// 		addChild(pageView_kuang,2);

		this->setContentSize(Size(270,360));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchEnded = CC_CALLBACK_2(MailList::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(MailList::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(MailList::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void MailList::onEnter()
{
	UIScene::onEnter();
}

void MailList::onExit()
{
	UIScene::onExit();
}

void MailList::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{
}

void MailList::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{	
}

void MailList::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void MailList::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void MailList::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void MailList::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	int cellIndex_ = cell->getIdx();
	auto mainscene =(MainScene *) GameView::getInstance()->getMainUIScene();
	auto mailUI=(MailUI *)mainscene->getChildByTag(kTagMailUi);
	long long mailId_ = mailUI->sourceDataVector.at(cellIndex_)->id();
	mailUI->sourceDataVector.at(cellIndex_)->set_flag(1);
	mailUI->operationType = 1;
	mailUI->mailList_Id = cellIndex_;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2002,(void *)1,(void *)mailId_);
	
	this->reSetRemind(mailId_);

	auto cellSp_ =(Sprite *)cell->getChildByTag(mailStateFlag);
	cellSp_->setVisible(false);
	mailUI->showMailInfo(cellIndex_);
}

cocos2d::Size MailList::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(270,63);
}

cocos2d::extension::TableViewCell* MailList::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	__String *string =__String::createWithFormat("%d",idx);
	auto cell=table->dequeueCell();
	cell=new TableViewCell();
	cell->autorelease();

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	auto mailUI=(MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
	//mailUI->sourceDataVector.at(0)->set_flag(1);
	cell = MailListCell::create(idx);
	//this->reSetRemind(mailUI->sourceDataVector.at(0)->id());

	if (idx == mailUI->sourceDataVector.size() - 3)
	{
		if (mailUI->isHasNextPage ==true)
		{
			mailUI->curMailPage++;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001,(void*)mailUI->curMailPage);
		}
	}
	return cell;
}

ssize_t MailList::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	auto mailUI=(MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
	return mailUI->sourceDataVector.size();
}

void MailList::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void MailList::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void MailList::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void MailList::reSetRemind(long long mailId)
{
	auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();

	for (unsigned int mailIndex =0;mailIndex<GameView::getInstance()->mailVectorOfSystem.size();mailIndex++)
	{
		if (mailId == GameView::getInstance()->mailVectorOfSystem.at(mailIndex)->id())
		{
			GameView::getInstance()->mailVectorOfSystem.at(mailIndex)->set_flag(1);
		}
	}
	/*
	for (unsigned int mailIndex =0;mailIndex<GameView::getInstance()->mailVectorOfPlayer.size();mailIndex++)
	{
		if (mailId == GameView::getInstance()->mailVectorOfPlayer.at(mailIndex)->id())
		{
			GameView::getInstance()->mailVectorOfPlayer.at(mailIndex)->set_flag(1);
		}
	}
	*/
	mainscene->remindMail();
}

//////////////////////////////////////////////////////////
MailListCell::MailListCell(void)
{
}


MailListCell::~MailListCell(void)
{
}

MailListCell * MailListCell::create(int idx)
{
	auto cell=new MailListCell();
	if (cell && cell->init(idx))
	{
		cell->autorelease();
		return cell;
	}
	CC_SAFE_DELETE(cell);
	return NULL;
}

bool MailListCell::init(int idx)
{
	if (TableViewCell::init())
	{
		auto maillUI= (MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
		auto mailList =(MailList *)maillUI->mailList;

		auto imageBackGround = cocos2d::extension::Scale9Sprite::create("res_ui/kuang01_new.png");
		imageBackGround->setPreferredSize(Size(270,63));
		imageBackGround->setCapInsets(Rect(15,30,1,1));
		imageBackGround->setAnchorPoint(Vec2(0,0));
		imageBackGround->setPosition(Vec2(0,0));
		addChild(imageBackGround);

		const char *str_Source_ = StringDataManager::getString("mail_sendSource");
		const char *str_MailPlayerof = StringDataManager::getString("mail_sendsourceOfMail");
		std::string text_label =maillUI->sourceDataVector.at(idx)->sendername();

		auto shadowColor = Color4B::BLACK;
		auto labelSource_=Label::createWithTTF(str_Source_,APP_FONT_NAME,20);
		labelSource_->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		labelSource_->setAnchorPoint(Vec2(0,0));
		labelSource_->setPosition(Vec2(40,25));
		addChild(labelSource_,10);

		Color3B SourceName;
		if (/*maillUI->curTabType == 0*/maillUI->sourceDataVector.at(idx)->sender() == LLONG_MAX)
		{
			SourceName = Color3B(255,0,1);
		}else
		{
			SourceName = Color3B(60,255,0);
		}
		
		auto labelSourceName_  =Label::createWithTTF(text_label.c_str(), APP_FONT_NAME, 20);
		labelSourceName_->setAnchorPoint(Vec2(0,0));
		labelSourceName_->setPosition(Vec2(labelSource_->getPosition().x+ labelSource_->getContentSize().width,25));
		labelSourceName_->setColor(SourceName);

		addChild(labelSourceName_,10);

		auto labelContentStr_=Label::createWithTTF(str_MailPlayerof,APP_FONT_NAME,20);
		labelContentStr_->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		labelContentStr_->setAnchorPoint(Vec2(0,0));
		labelContentStr_->setPosition(Vec2(labelSourceName_->getPosition().x+ labelSourceName_->getContentSize().width,25));
		addChild(labelContentStr_,10);

		auto mailFlag=Sprite::create("res_ui/newMailFlag.png");
		mailFlag->setAnchorPoint(Vec2(0,0));
		mailFlag->setPosition(Vec2(20,30));
		mailFlag->setVisible(false);
		mailFlag->setTag(mailStateFlag);
		addChild(mailFlag);

		// show the remain time
		// calc the remain day
		long long sendtime = maillUI->sourceDataVector.at(idx)->sendtime();
		int elapseDay = (GameUtils::getDateSecond() - sendtime / 1000) / TOTAL_SECOND_FOR_ONE_DAY;
		int keepDay = MAIL_KEEP_TIME_NORMAL;
		if(maillUI->sourceDataVector.at(idx)->attachment_size() > 0 )   // with attachment
		{
			if (maillUI->sourceDataVector.at(idx)->goldingot() > 0 || maillUI->sourceDataVector.at(idx)->gold() > 0)
			{
				keepDay = MAIL_KEEP_TIME_WITH_NEEDTOPAY;
			}else
			{
				keepDay = MAIL_KEEP_TIME_WITH_ATTACHMENT;
			}
		}
		int remain_day = keepDay - elapseDay;
		if(remain_day <= 0)
			remain_day = 1;
	
		char remainTimeStr[20];
		const char* templateStr = StringDataManager::getString("mail_remaintime");
		sprintf(remainTimeStr, templateStr, remain_day);

		auto labelRemainTime = Label::createWithTTF(remainTimeStr,APP_FONT_NAME,16);
		labelRemainTime->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		labelRemainTime->setAnchorPoint(Vec2(0,0));
		labelRemainTime->setPosition(Vec2(40,5));
		addChild(labelRemainTime, 10);

		if (maillUI->sourceDataVector.at(idx)->flag() == 0 || maillUI->sourceDataVector.at(idx)->flag() == 3)
		{
			mailFlag->setVisible(true);
		}

		if (maillUI->sourceDataVector.at(idx)->attachment_size()>0 && maillUI->sourceDataVector.at(idx)->attachment(0).state()==0)
		{
			auto mailAttach=Sprite::create("res_ui/youjian/kejierenwuup.png");
			mailAttach->setAnchorPoint(Vec2(0,0));
			mailAttach->setPosition(Vec2(230,20));
			mailAttach->setTag(mailAttachmentStateFlag);
			addChild(mailAttach);
		}

		return true;
	}
	return false;
}

void MailListCell::onEnter()
{
	TableViewCell::onEnter();
}

void MailListCell::onExit()
{
	TableViewCell::onExit();
}