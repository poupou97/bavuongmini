#pragma once

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

typedef enum{
	MAILTABTAG = 333,
	TABALEVIEW_HIGHLIGHT_TAG =334,
	TABLEVIEW_HIGHLIGHT_SELECT_TAG = 335
};
class MailList:public UIScene,public cocos2d::extension::TableViewDataSource,public cocos2d::extension::TableViewDelegate
{
public:
	MailList(void);
	~MailList(void);

	static MailList * create();

	bool init();
	virtual void onEnter();
	virtual void onExit();

    virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
    virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
    
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	void reSetRemind(long long mailId);
	// default implements are used to call script callback if exist
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
public:
	TableView* tableView;
	int lastSelectCellId;

	//cocos2d::extension::Scale9Sprite * pageView_kuang;
};


///////////////////////////////// mail cell
#define mailStateFlag 10
#define mailAttachmentStateFlag 20
class MailListCell:public TableViewCell
{
public:
	MailListCell(void);
	~MailListCell(void);

	static MailListCell *create(int idx);
	bool init(int idx);

	void onEnter();
	void onExit();
};
