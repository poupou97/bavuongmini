#include "ShopUI.h"
#include "../extensions/UITab.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GameView.h"
#include "../extensions/CCMoveableMenu.h"
#include "../backpackscene/PacPageView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CSalableCommodity.h"
#include "../extensions/Counter.h"
#include "ShopItemInfo.h"
#include "ShopItemBuyInfo.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../messageclient/element/CDrug.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"
#include "../backpackscene/PacPageView.h"

#define CellBase_Buy_Tag 100
#define CellBase_Info_Tag 1000
#define  SelectImageTag 458

ShopUI::ShopUI():
isBuyOrSell(true),
isSelectBtn(false)
{
}


ShopUI::~ShopUI()
{
	delete curSalableCommodity;
	delete curBuyBackCommodity;
}

ShopUI * ShopUI::create()
{
	auto shopUI = new ShopUI();
	if(shopUI && shopUI->init())
	{
		shopUI->autorelease();
		return shopUI;
	}
	CC_SAFE_DELETE(shopUI);
	return NULL;
}

bool ShopUI::init()
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();

		this->scheduleUpdate();

		curSalableCommodity = new CSalableCommodity();
		curBuyBackCommodity = new CSalableCommodity();

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(800, 480));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		//���UI
		if(LoadSceneLayer::ShopLayer->getParent() != NULL)
		{
			LoadSceneLayer::ShopLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::ShopLayer;
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->getVirtualRenderer()->setContentSize(Size(800, 480));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		const char * secondStr = StringDataManager::getString("UIName_shang");
		const char * thirdStr = StringDataManager::getString("UIName_dian");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		u_layer->addChild(atmature);


		//عرհ�ť
		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->setPressedActionEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(ShopUI::CloseEvent, this));
		//����ť
		auto Button_sort = (Button*)Helper::seekWidgetByName(ppanel,"Button_ArrangeBackpack");
		Button_sort->setTouchEnabled(true);
		Button_sort->setPressedActionEnabled(true);
		Button_sort->addTouchEventListener(CC_CALLBACK_2(ShopUI::SortEvent, this));

		pointRight = (ImageView *)Helper::seekWidgetByName(ppanel,"dian_on_right");

		//jinbi
		int m_goldValue = GameView::getInstance()->getPlayerGold();
		char GoldStr_[20];
		sprintf(GoldStr_,"%d",m_goldValue);
		Label_gold = (Text*)Helper::seekWidgetByName(ppanel,"Label_gold");
		Label_gold->setString(GoldStr_);
		//yuanbao
		int m_goldIngot =  GameView::getInstance()->getPlayerGoldIngot();
		char GoldIngotStr_[20];
		sprintf(GoldIngotStr_,"%d",m_goldIngot);
		Label_goldIngot = (Text*)Helper::seekWidgetByName(ppanel,"Label_goldIngold");
		Label_goldIngot->setString(GoldIngotStr_);

		//maintab
		const char * normalImage = "res_ui/tab_1_off.png";
		const char * selectImage = "res_ui/tab_1_on.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_1_on.png";
		//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((__String*)strings->objectForKey("goods_auction_buy"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("goods_auction_buy");
		char* p1 =const_cast<char*>(str1);
		//const char *str2  = ((__String*)strings->objectForKey("goods_shop_buyBack"))->m_sString.c_str();
		const char *str2 = StringDataManager::getString("goods_shop_buyBack");
		char* p2 =const_cast<char*>(str2);

		char * mainNames[] ={p1,p2};
		mainTab = UITab::createWithText(2,normalImage,selectImage,finalImage,mainNames,HORIZONTAL,5);
		mainTab->setAnchorPoint(Vec2(0,0));
		mainTab->setPosition(Vec2(69,415));
		mainTab->setHighLightImage((char * )highLightImage);
		mainTab->setDefaultPanelByIndex(0);
		mainTab->addIndexChangedEvent(this,coco_indexchangedselector(ShopUI::MainTabIndexChangedEvent));
		mainTab->setPressedActionEnabled(true);
		u_layer->addChild(mainTab);

		m_tableView = TableView::create(this,Size(301,362));
		m_tableView ->setTouchEnabled(true);   // ֧��ѡ��״̬����ʾ
		//m_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(26, 26, 1, 1), Vec2(0,-1)); 
		//m_tableView->setPressedActionEnabled(true);
		m_tableView->setDirection(TableView::Direction::VERTICAL);
		m_tableView->setAnchorPoint(Vec2(0,0));
		m_tableView->setPosition(Vec2(76,45));
		m_tableView->setDelegate(this);
		m_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		u_layer->addChild(m_tableView);

		//�Ҳ౳�
		m_pacPageView = GameView::getInstance()->pacPageView;
		m_pacPageView->getPageView()->addEventListener((PageView::ccPageViewCallback)CC_CALLBACK_2(ShopUI::RightPageViewChanged, this));
		u_layer->addChild(m_pacPageView);
		m_pacPageView->setPosition(Vec2(399,100));
		m_pacPageView->setCurUITag(kTagShopUI);
		m_pacPageView->setVisible(true);
		m_pacPageView->checkCDOnBegan();
		auto seq = Sequence::create(DelayTime::create(0.05f),CallFunc::create(CC_CALLBACK_0(ShopUI::PageScrollToDefault,this)),NULL);
		m_pacPageView->runAction(seq);

		//refresh cd
		auto shortCutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortCutLayer)
		{
			for (int i = 0;i<7;++i)  //�һ��7�����
			{
				if (i < GameView::getInstance()->shortCutList.size())
				{
					auto c_shortcut = GameView::getInstance()->shortCutList.at(i);
					if (c_shortcut->storedtype() == 2)    //����������Ʒ
					{
						if(CDrug::isDrug(c_shortcut->skillpropid()))
						{
							if (shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100))
							{
								auto shortcutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100);
								this->m_pacPageView->RefreshCD(c_shortcut->skillpropid());
							}
						}
					}
				}
			}
		}

		auto u_pageFrameLayer = Layer::create();
		u_pageFrameLayer->setIgnoreAnchorPointForPosition(false);
		u_pageFrameLayer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_pageFrameLayer->setContentSize(Size(800, 480));
		u_pageFrameLayer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		this->addChild(u_pageFrameLayer);

// 		ImageView * m_pacPageView_kuang = ImageView::create();
// 		m_pacPageView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1_o.png");
// 		m_pacPageView_kuang->setScale9Enabled(true);
// 		m_pacPageView_kuang->setContentSize(Size(368,35));
// 		m_pacPageView_kuang->setCapInsets(Rect(30,0,1,1));
// 		m_pacPageView_kuang->setAnchorPoint(Vec2(0.5f,0.5f));
// 		m_pacPageView_kuang->setPosition(Vec2(571,396));
// 		u_pageFrameLayer->addChild(m_pacPageView_kuang);

// 		ImageView * m_tableView_kuang = ImageView::create();
// 		m_tableView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		m_tableView_kuang->setScale9Enabled(true);
// 		m_tableView_kuang->setContentSize(Size(320,377));
// 		m_tableView_kuang->setCapInsets(Rect(30,30,1,1));
// 		m_tableView_kuang->setAnchorPoint(Vec2(0,0));
// 		m_tableView_kuang->setPosition(Vec2(62,35));
// 		u_pageFrameLayer->addChild(m_tableView_kuang

		this->setContentSize(winsize);
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(ShopUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(ShopUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(ShopUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(ShopUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void ShopUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void ShopUI::onExit()
{
	UIScene::onExit();
}


bool ShopUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	return this->resignFirstResponder(touch,this,false);
}

void ShopUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void ShopUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void ShopUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}


void ShopUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

Size ShopUI::tableCellSizeForIndex(TableView *table, unsigned int idx)
{
// 	if (mainTab->getCurrentIndex() <= 1)
// 	{
// 		if (idx == 0)
// 		{
// 			return Size(298,70);
// 		}
// 		else
// 		{
// 			return Size(298, 63);
// 		}
// 	}
// 	else
// 	{
		return Size(295, 69);
//	}


}

TableViewCell* ShopUI::tableCellAtIndex(TableView *table, ssize_t  idx)
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	cell=new TableViewCell();
	cell->autorelease();

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);
	
	if (isBuyOrSell)
	{
		auto temp = GameView::getInstance()->shopItemList.at(idx);

		auto pCellBg = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
		pCellBg->setPreferredSize((Size(295,66)));
		pCellBg->setAnchorPoint(Vec2(0, 0));
		pCellBg->setPosition(Vec2(0, 0));
		cell->addChild(pCellBg);

		//��ƷIcon�׿
		std::string iconFramePath;
		switch(temp->goods().quality())
		{
		case 1 :
			{
				iconFramePath = "res_ui/smdi_white.png";
			}
			break;
		case 2 :
			{
				iconFramePath = "res_ui/smdi_green.png";
			}
			break;
		case 3 :
			{
				iconFramePath = "res_ui/smdi_bule.png";
			}
			break;
		case 4 :
			{
				iconFramePath = "res_ui/smdi_purple.png";
			}
			break;
		case 5 :
			{
				iconFramePath = "res_ui/smdi_orange.png";
			}
			break;
		}
// 		Sprite *  smaillFrame = Sprite::create(iconFramePath.c_str());
// 		smaillFrame->setAnchorPoint(Vec2(0, 0));
// 		smaillFrame->setPosition(Vec2(15, 10));
// 		cell->addChild(smaillFrame);
// 		
		auto spFrameBg = Sprite::create(iconFramePath.c_str());

		auto smaillFrame = MenuItemSprite::create(spFrameBg, spFrameBg, spFrameBg, CC_CALLBACK_1(ShopUI::SellInfoEvent,this));
		smaillFrame->setTag(CellBase_Info_Tag+idx);
		smaillFrame->setScale(1.0f);
		auto btn_smaillFrame = CCMoveableMenu::create(smaillFrame,NULL);
		btn_smaillFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_smaillFrame->setPosition(Vec2(38,33));
		cell->addChild(btn_smaillFrame);

		std::string pheadPath = "res_ui/props_icon/";
		pheadPath.append(temp->goods().icon().c_str());
		pheadPath.append(".png");
		auto phead = Sprite::create(pheadPath.c_str());
		phead->setIgnoreAnchorPointForPosition(false);
		phead->setAnchorPoint(Vec2(0, 0.5f));
		phead->setPosition(Vec2(17, pCellBg->getContentSize().height/2+1));
		phead->setScale(0.85f);
		cell->addChild(phead);

		auto pName = Label::createWithTTF(temp->goods().name().c_str(),APP_FONT_NAME,18);
		pName->setAnchorPoint(Vec2(0.5f,0.5f));
		pName->setPosition(Vec2(140,42));
		pName->setColor(GameView::getInstance()->getGoodsColorByQuality(temp->goods().quality()));
		cell->addChild(pName);

		if(temp->pricetype() == 1)
		{
			auto sprite_price_gold = Sprite::create("res_ui/coins.png");
			sprite_price_gold->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_price_gold->setPosition(Vec2(106,23));
			cell->addChild(sprite_price_gold);
		}
		else
		{
			auto sprite_price_goldIngold = Sprite::create("res_ui/ingot.png");
			sprite_price_goldIngold->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_price_goldIngold->setPosition(Vec2(106,23));
			cell->addChild(sprite_price_goldIngold);
		}

		char s_price[20];
		sprintf(s_price,"%d",temp->price());
		auto pPrice = Label::createWithTTF(s_price,APP_FONT_NAME,16);
		pPrice->setAnchorPoint(Vec2(0.5f,0.5f));
		pPrice->setPosition(Vec2(155,23));
		cell->addChild(pPrice);


		auto spBg = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_2.png");
		spBg->setPreferredSize(Size(71,39));

		auto itemSprite = MenuItemSprite::create(spBg, spBg, spBg, CC_CALLBACK_1(ShopUI::BuyEvent,this));
		itemSprite->setTag(CellBase_Buy_Tag+idx);
		itemSprite->setScale(1.3f);
		auto btn_buy = CCMoveableMenu::create(itemSprite,NULL);
		btn_buy->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_buy->setPosition(Vec2(246,34));
		cell->addChild(btn_buy);

		//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((__String*)strings->objectForKey("goods_auction_buy"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("goods_auction_buy");
		char* p1 =const_cast<char*>(str1);
		auto l_buy=Label::createWithTTF(p1,APP_FONT_NAME,20);
		l_buy->setAnchorPoint(Vec2(0.5f,0.5f));
		l_buy->setPosition(Vec2(71/2,39/2));
		itemSprite->addChild(l_buy);
	}
	else
	{
		auto temp = GameView::getInstance()->buyBackList.at(idx);

		auto pCellBg = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
		pCellBg->setPreferredSize((Size(295,66)));
		pCellBg->setAnchorPoint(Vec2(0, 0));
		pCellBg->setPosition(Vec2(0, 0));
		cell->addChild(pCellBg);

		//���ƷIcon�׿
		std::string iconFramePath;
		switch(temp->goods().quality())
		{
		case 1 :
			{
				iconFramePath = "res_ui/smdi_white.png";
			}
			break;
		case 2 :
			{
				iconFramePath = "res_ui/smdi_green.png";
			}
			break;
		case 3 :
			{
				iconFramePath = "res_ui/smdi_bule.png";
			}
			break;
		case 4 :
			{
				iconFramePath = "res_ui/smdi_purple.png";
			}
			break;
		case 5 :
			{
				iconFramePath = "res_ui/smdi_orange.png";
			}
			break;
		default:
			iconFramePath = "res_ui/smdi_white.png";
		}
// 		Sprite *  smaillFrame = Sprite::create(iconFramePath.c_str());
// 		smaillFrame->setAnchorPoint(Vec2(0, 0));
// 		smaillFrame->setPosition(Vec2(15, 10));
// 		cell->addChild(smaillFrame);

		auto spFrameBg = Sprite::create(iconFramePath.c_str());

		auto smaillFrame = MenuItemSprite::create(spFrameBg, spFrameBg, spFrameBg, CC_CALLBACK_1(ShopUI::BuyBackInfoEvent,this));
		smaillFrame->setTag(CellBase_Info_Tag+idx);
		smaillFrame->setScale(1.0f);
		auto btn_smaillFrame = CCMoveableMenu::create(smaillFrame,NULL);
		btn_smaillFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_smaillFrame->setPosition(Vec2(38,33));
		cell->addChild(btn_smaillFrame);

		std::string pheadPath = "res_ui/props_icon/";
		pheadPath.append(temp->goods().icon().c_str());
		pheadPath.append(".png");
		auto phead = Sprite::create(pheadPath.c_str());
		phead->setIgnoreAnchorPointForPosition(false);
		phead->setAnchorPoint(Vec2(0, 0.5f));
		phead->setPosition(Vec2(17, pCellBg->getContentSize().height/2+1));
		phead->setScale(0.85f);
		cell->addChild(phead);

		auto pName = Label::createWithTTF(temp->goods().name().c_str(),APP_FONT_NAME,18);
		pName->setAnchorPoint(Vec2(0.5f,0.5f));
		pName->setPosition(Vec2(130,42));
		pName->setColor(GameView::getInstance()->getGoodsColorByQuality(temp->goods().quality()));
		cell->addChild(pName);

		std::string str_num = "*";
		char s_num[20];
		sprintf(s_num,"%d",temp->quantity());
		str_num.append(s_num);
		auto pNum = Label::createWithTTF(str_num.c_str(),APP_FONT_NAME,18);
		pNum->setAnchorPoint(Vec2(0.5f,0.5f));
		pNum->setPosition(Vec2(pName->getPositionX()+pName->getContentSize().width/2+10,42));
		cell->addChild(pNum);

		if(temp->pricetype() == 1)
		{
			auto sprite_price_gold = Sprite::create("res_ui/coins.png");
			sprite_price_gold->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_price_gold->setPosition(Vec2(106,23));
			cell->addChild(sprite_price_gold);
		}
		else
		{
			auto sprite_price_goldIngold = Sprite::create("res_ui/ingot.png");
			sprite_price_goldIngold->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_price_goldIngold->setPosition(Vec2(106,23));
			cell->addChild(sprite_price_goldIngold);
		}


		char s_price[20];
		sprintf(s_price,"%d",temp->goods().sellprice()*temp->quantity());
		auto pPrice = Label::createWithTTF(s_price,APP_FONT_NAME,16);
		pPrice->setAnchorPoint(Vec2(0.5f,0.5f));
		pPrice->setPosition(Vec2(155,23));
		cell->addChild(pPrice);

		auto spBg = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_2.png");
		spBg->setPreferredSize(Size(71,39));

		auto itemSprite = MenuItemSprite::create(spBg, spBg, spBg, CC_CALLBACK_1(ShopUI::BuyBackEvent,this));
		itemSprite->setTag(CellBase_Buy_Tag+idx);
		itemSprite->setScale(1.3f);
		auto btn_buy = CCMoveableMenu::create(itemSprite,NULL);
		btn_buy->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_buy->setPosition(Vec2(246,34));
		cell->addChild(btn_buy);

		//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((__String*)strings->objectForKey("goods_shop_buyBack"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("goods_shop_buyBack");
		char* p1 =const_cast<char*>(str1);
		auto l_buy=Label::createWithTTF(p1,APP_FONT_NAME,20);
		l_buy->setAnchorPoint(Vec2(0.5f,0.5f));
		l_buy->setPosition(Vec2(71/2,39/2));
		itemSprite->addChild(l_buy);

	}

	return cell;

}
ssize_t ShopUI::numberOfCellsInTableView(TableView *table)
{
		if (isBuyOrSell)
		{
			return GameView::getInstance()->shopItemList.size();
		}
		else
		{
			int buyBackItemNum = 0;
			for(int i =0;i<GameView::getInstance()->buyBackList.size();++i)
			{
				if (GameView::getInstance()->buyBackList.at(i)->has_goods())
				{
					buyBackItemNum++;
				}
			}
			return buyBackItemNum;
		}
}

void ShopUI::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void ShopUI::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void ShopUI::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void ShopUI::tableCellTouched(TableView* table, TableViewCell* cell)
{
// 	if (!isSelectBtn)
// 	{
// 		if (isBuyOrSell)
// 		{
// 			int idx = cell->getIdx();
// 			//curSalableCommodity = GameView::getInstance()->shopItemList.at(idx);
// 			curSalableCommodity->CopyFrom(*GameView::getInstance()->shopItemList.at(idx));
// 
// 			Size size= Director::getInstance()->getVisibleSize();
// 			ShopItemInfo * shopItemInfo = ShopItemInfo::create(curSalableCommodity);
// 			shopItemInfo->setIgnoreAnchorPointForPosition(false);
// 			shopItemInfo->setAnchorPoint(Vec2(0.5f,0.5f));
// 			//shopItemInfo->setPosition(Vec2(size.width/2,size.height/2));
// 			GameView::getInstance()->getMainUIScene()->addChild(shopItemInfo);
// 		}
// 		else
// 		{
// 			int idx = cell->getIdx();
// 			//curBuyBackCommodity = GameView::getInstance()->buyBackList.at(idx);
// 			curBuyBackCommodity->CopyFrom(*GameView::getInstance()->buyBackList.at(idx));
// 
// 			Size size= Director::getInstance()->getVisibleSize();
// 			ShopItemInfo * shopItemInfo = ShopItemInfo::create(curBuyBackCommodity);
// 			shopItemInfo->setIgnoreAnchorPointForPosition(false);
// 			shopItemInfo->setAnchorPoint(Vec2(0.5f,0.5f));
// 			//shopItemInfo->setPosition(Vec2(size.width/2,size.height/2));
// 			GameView::getInstance()->getMainUIScene()->addChild(shopItemInfo);
// 		}
// 	}
}
void ShopUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void ShopUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

void ShopUI::MainTabIndexChangedEvent(Ref* pSender)
{
	switch((int)mainTab->getCurrentIndex())
	{
	case 0 :
		isBuyOrSell = true;
		break;   
	case 1 :
		isBuyOrSell = false;
		break;
	}

	this->m_tableView->reloadData();
}

void ShopUI::BuyEvent( Ref * pSender )
{
	auto ps = (MenuItemSprite*)pSender;
	curSalableCommodity->CopyFrom(*GameView::getInstance()->shopItemList.at(ps->getTag()-CellBase_Buy_Tag));
	
	auto size= Director::getInstance()->getVisibleSize();
	auto shopItemBuyInfo = ShopItemBuyInfo::create(curSalableCommodity);
	shopItemBuyInfo->setIgnoreAnchorPointForPosition(false);
	shopItemBuyInfo->setAnchorPoint(Vec2(0.5f,0.5f));
	shopItemBuyInfo->setPosition(Vec2(size.width/2,size.height/2));
	shopItemBuyInfo->setTag(kTagShopItemBuyInfo);
	GameView::getInstance()->getMainUIScene()->addChild(shopItemBuyInfo);

	isSelectBtn = true;
}

void ShopUI::BuyBackEvent( Ref * pSender )
{
	auto ps = (MenuItemSprite*)pSender;
	curSalableCommodity->CopyFrom(*GameView::getInstance()->buyBackList.at(ps->getTag()-CellBase_Buy_Tag));

// 	Size size= Director::getInstance()->getVisibleSize();
// 	ShopItemBuyInfo * shopItemBuyInfo = ShopItemBuyInfo::create(curSalableCommodity);
// 	shopItemBuyInfo->setIgnoreAnchorPointForPosition(false);
// 	shopItemBuyInfo->setAnchorPoint(Vec2(0.5f,0.5f));
// 	shopItemBuyInfo->setPosition(Vec2(size.width/2,size.height/2));
// 	shopItemBuyInfo->setTag(kTagShopItemBuyInfo);
// 	GameView::getInstance()->getMainUIScene()->addChild(shopItemBuyInfo);

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1134,(void *)curSalableCommodity->id(),(void *)curSalableCommodity->quantity());

	isSelectBtn = true;
}

void ShopUI::RightPageViewChanged(Ref *pSender, PageView::EventType type)
{
	if (type == PageView::EventType::TURNING) {
		auto pageView = dynamic_cast<PageView*>(pSender);
		switch (static_cast<int>(pageView->getCurrentPageIndex()))
		{
		case 0:
			pointRight->setPosition(Vec2(525, 77));
			break;
		case 1:
			pointRight->setPosition(Vec2(546, 77));
			break;
		case 2:
			pointRight->setPosition(Vec2(565, 77));
			break;
		case 3:
			pointRight->setPosition(Vec2(584, 77));
			break;
		case 4:
			pointRight->setPosition(Vec2(603, 77));
			break;
		}
	}
}

void ShopUI::ReloadData()
{
	this->m_tableView->reloadData();
}

void ShopUI::ReloadDataWithOutChangeOffSet()
{
	Vec2 _s = this->m_tableView->getContentOffset();
	int _h = this->m_tableView->getContentSize().height + _s.y;
	this->m_tableView->reloadData();
	Vec2 temp = Vec2(_s.x,_h - this->m_tableView->getContentSize().height);
	this->m_tableView->setContentOffset(temp); 
}

void ShopUI::SortEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (m_remainArrangePac > 0.f)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("package_arrange_canNot"));
			return;
		}

		m_remainArrangePac = 15.0f;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1308, (void *)1);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}


void ShopUI::update(float dt)
{
	char str_gold[20];
	sprintf(str_gold,"%d",GameView::getInstance()->getPlayerGold());
	Label_gold->setString(str_gold);
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	Label_goldIngot->setString(str_ingot);

	m_remainArrangePac -= 1.0f/60;
	if (m_remainArrangePac < 0)
		m_remainArrangePac = 0.f;
}

void ShopUI::PageScrollToDefault()
{
	m_pacPageView->getPageView()->scrollToPage(0);
	RightPageViewChanged(m_pacPageView, PageView::EventType::TURNING);
}

void ShopUI::SellInfoEvent( Ref * pSender )
{
	auto ps = (MenuItemSprite*)pSender;
	curSalableCommodity->CopyFrom(*GameView::getInstance()->shopItemList.at(ps->getTag()-CellBase_Info_Tag));

	auto size= Director::getInstance()->getVisibleSize();
	auto shopItemInfo = ShopItemInfo::create(curSalableCommodity);
	shopItemInfo->setIgnoreAnchorPointForPosition(false);
	shopItemInfo->setAnchorPoint(Vec2(0.5f,0.5f));
	GameView::getInstance()->getMainUIScene()->addChild(shopItemInfo);
}

void ShopUI::BuyBackInfoEvent( Ref * pSender )
{
	auto ps = (MenuItemSprite*)pSender;
	curBuyBackCommodity->CopyFrom(*GameView::getInstance()->buyBackList.at(ps->getTag()-CellBase_Info_Tag));

	auto size= Director::getInstance()->getVisibleSize();
	auto shopItemInfo = ShopItemInfo::create(curBuyBackCommodity);
	shopItemInfo->setIgnoreAnchorPointForPosition(false);
	shopItemInfo->setAnchorPoint(Vec2(0.5f,0.5f));
	GameView::getInstance()->getMainUIScene()->addChild(shopItemInfo);
}
