
#ifndef _SHOPUI_SHOPUI_
#define _SHOPUI_SHOPUI_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
class PacPageView;
class CSalableCommodity;

class ShopUI : public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	ShopUI();
	~ShopUI();

	static ShopUI * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void SortEvent(Ref *pSender, Widget::TouchEventType type);

	void SellInfoEvent(Ref * pSender);
	void BuyBackInfoEvent(Ref * pSender);
	void BuyEvent(Ref * pSender);
	void BuyBackEvent(Ref * pSender);

	void ReloadData();
	void ReloadDataWithOutChangeOffSet();

	virtual void update(float dt);

	void PageScrollToDefault();

private:
	Layout * ppanel;
	Layer * u_layer;
	Text *Label_gold;
	Text *Label_goldIngot;
	
	TableView * m_tableView;

	PacPageView * m_pacPageView;
	//��ұpageviewߵ�ָʾ�
	ImageView * pointRight;

	//㵱ǰѡ����̵���Ʒ�
	CSalableCommodity * curSalableCommodity;
	//ǰѡ��Ļع��
	CSalableCommodity * curBuyBackCommodity;

	float m_remainArrangePac;
	
public: 
	UITab * mainTab ;
	bool isBuyOrSell;
	//��Ƿ����˹��/�ع���ť
	bool isSelectBtn;

	struct sellGoodsStruct
	{
		int folderIndex;
		int amount;
		int autoflag;
	};

private:
	void MainTabIndexChangedEvent(Ref * pSender);
	void RightPageViewChanged(Ref *pSender, PageView::EventType type);
};

#endif

