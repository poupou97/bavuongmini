#include "ShopItemInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/element/CSalableCommodity.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"


ShopItemInfo::ShopItemInfo()
{
}


ShopItemInfo::~ShopItemInfo()
{
}

ShopItemInfo * ShopItemInfo::create( CSalableCommodity * salableCommodity )
{
	auto shopItemInfo = new ShopItemInfo();
	if (shopItemInfo && shopItemInfo->init(salableCommodity))
	{
		shopItemInfo->autorelease();
		return shopItemInfo;
	}
	CC_SAFE_DELETE(shopItemInfo);
	return NULL;
}

bool ShopItemInfo::init( CSalableCommodity * salableCommodity )
{
	auto goodsInfo = new GoodsInfo();
	goodsInfo->CopyFrom(salableCommodity->goods());
	if (GoodsItemInfoBase::init(goodsInfo,GameView::getInstance()->EquipListItem,0))
	{
		delete goodsInfo;

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setAnchorPoint(Vec2(0,0));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(ShopItemInfo::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(ShopItemInfo::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(ShopItemInfo::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(ShopItemInfo::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void ShopItemInfo::onEnter()
{
	GoodsItemInfoBase::onEnter();
	this->openAnim();
}

void ShopItemInfo::onExit()
{
	GoodsItemInfoBase::onExit();
}

bool ShopItemInfo::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return resignFirstResponder(pTouch,this,true);
}

void ShopItemInfo::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void ShopItemInfo::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void ShopItemInfo::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

