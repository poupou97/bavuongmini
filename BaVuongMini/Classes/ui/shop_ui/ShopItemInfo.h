
#ifndef _SHOPUI_SHOPITEMINFO_H_
#define _SHOPUI_SHOPITEMINFO_H_

#include "../backpackscene/GoodsItemInfoBase.h"

class CSalableCommodity;

class ShopItemInfo : public GoodsItemInfoBase
{
public:
	ShopItemInfo();
	~ShopItemInfo();

	static ShopItemInfo * create(CSalableCommodity * salableCommodity);
	bool init(CSalableCommodity * salableCommodity);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
};

#endif
