#ifndef _STOREHOUSE_STOREHOUSEINFO_H
#define _STOREHOUSE_STOREHOUSEINFO_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../backpackscene/GoodsItemInfoBase.h"

class FolderInfo;

class StoreHouseInfo : public GoodsItemInfoBase
{
public:
	StoreHouseInfo();
	~StoreHouseInfo();

	static StoreHouseInfo * create(FolderInfo * folder);
	bool init(FolderInfo * folder);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void GetOutEvent(Ref *pSender, Widget::TouchEventType type);
	void GetStoreItemNum(Ref *pSender);

private:
	int  m_nfolderIndex;
	int  m_quantity;

};

#endif

