#include "StoreHouseUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GameView.h"
#include "StoreHouseItem.h"
#include "StoreHousePageView.h"
#include "../backpackscene/PacPageView.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../extensions/Counter.h"
#include "../goldstore_ui/GoldStoreUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../messageclient/element/CDrug.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutSlot.h"

StoreHouseUI::StoreHouseUI():
storeOrPac(false),
m_remainArrangePac(0.0f),
m_remainArrangeStore(0.0f)
{
// 	//coordinate for packageitem
// 	for (int m = 4;m>=0;m--)
// 	{
// 		for (int n = 3;n>=0;n--)
// 		{
// 			int _m = 4-m;
// 			int _n = 3-n;
// 
// 			Coordinate temp ;
// 			temp.x = 6+_n*68;
// 			temp.y = 1+m*64;
// 
// 			CoordinateVector.push_back(temp);
// 		}
// 	}
// 	//UIPageView_Panel_Name
// 	for (int i = 0;i<4;i++)
// 	{
// 		std::string storePageViewBaseName = "storePageViewPanel_";
// 		char num[4];
// 		int tmpID = i;
// 		sprintf(num, "0%d", tmpID);
// 		std::string number = num;
// 		storePageViewBaseName.append(num);
// 		storePageViewPanelName[i] = storePageViewBaseName;
// 	}
// 	//storeItem name
// 	for (int i=0 ;i<itemNumEveryPage; ++i)
// 	{
// 		std::string storeBaseName = "storeItem_";
// 		char num[4];
// 		int tmpID = i;
// 		if(tmpID < 10)
// 			sprintf(num, "0%d", tmpID);
// 		else
// 			sprintf(num, "%d", tmpID);
// 		std::string number = num;
// 		storeBaseName.append(num);
// 		storeItem_name[i] = storeBaseName;
// 	}
}


StoreHouseUI::~StoreHouseUI()
{
}

StoreHouseUI* StoreHouseUI::create()
{
	auto storeHouseUI = new StoreHouseUI();
	if (storeHouseUI && storeHouseUI->init())
	{
		storeHouseUI->autorelease();
		return storeHouseUI;
	}
	CC_SAFE_DELETE(storeHouseUI);
	return NULL;
}

bool StoreHouseUI::init()
{
	if (UIScene::init())
	{
		auto winsize = Director::getInstance()->getVisibleSize();

		this->scheduleUpdate();

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(800, 480));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		//���UI
		//ppanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("renwubeibao_2/renwubeibao_1.json");
		if(LoadSceneLayer::StoreHouseLayer->getParent() != NULL)
		{
			LoadSceneLayer::StoreHouseLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (Layout*)LoadSceneLayer::s_pPanel->copy();
		ppanel = LoadSceneLayer::StoreHouseLayer;
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->getVirtualRenderer()->setContentSize(Size(800, 480));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		const char * secondStr = StringDataManager::getString("UIName_cang");
		const char * thirdStr = StringDataManager::getString("UIName_ku");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		u_layer->addChild(atmature);

		pointLeft = (ImageView *)Helper::seekWidgetByName(ppanel,"dian_on_left");
		pointLeft->setPosition(Vec2(178,94));
		pointRight = (ImageView *)Helper::seekWidgetByName(ppanel,"dian_on_right");

		//عرհ�ť
		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->setPressedActionEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(StoreHouseUI::CloseEvent, this));
		//�������ť
		auto Button_sortPac = (Button*)Helper::seekWidgetByName(ppanel,"Button_ArrangeBackpack");
		Button_sortPac->setTouchEnabled(true);
		Button_sortPac->addTouchEventListener(CC_CALLBACK_2(StoreHouseUI::SortPacEvent, this));
		Button_sortPac->setPressedActionEnabled(true);
		//����ֿⰴť
		auto Button_sortStore = (Button*)Helper::seekWidgetByName(ppanel,"Button_ArrangeWarehouse");
		Button_sortStore->setTouchEnabled(true);
		Button_sortStore->addTouchEventListener(CC_CALLBACK_2(StoreHouseUI::SortStoreHouseEvent, this));
		Button_sortStore->setPressedActionEnabled(true);
		//�̳ǰ�ť
		auto Button_shop = (Button*)Helper::seekWidgetByName(ppanel,"Button_shop");
		Button_shop->setTouchEnabled(true);
		Button_shop->addTouchEventListener(CC_CALLBACK_2(StoreHouseUI::ShopEvent, this));
		Button_shop->setPressedActionEnabled(true);

		//jinbi
		int m_goldValue = GameView::getInstance()->getPlayerGold();
		char GoldStr_[20];
		sprintf(GoldStr_,"%d",m_goldValue);
		Label_gold = (Text*)Helper::seekWidgetByName(ppanel,"Label_gold");
		Label_gold->setString(GoldStr_);
		//yuanbao
		int m_goldIngot =  GameView::getInstance()->getPlayerGoldIngot();
		char GoldIngotStr_[20];
		sprintf(GoldIngotStr_,"%d",m_goldIngot);
		Label_goldIngot = (Text*)Helper::seekWidgetByName(ppanel,"Label_goldIngold");
		Label_goldIngot->setString(GoldIngotStr_);


		// Create the page view
		//reloadInit();
		//���ֿ
		m_storePageView = GameView::getInstance()->storeHousePageView;
		m_storePageView->getPageView()->addEventListener((PageView::ccPageViewCallback)CC_CALLBACK_2(StoreHouseUI::LeftPageViewChanged, this));
		u_layer->addChild(m_storePageView);
		m_storePageView->setPosition(Vec2(75,119));
		m_storePageView->setCurUITag(kTagStoreHouseUI);
		m_storePageView->setVisible(true);
		
		//��Ҳ౳�
		m_pacPageView = GameView::getInstance()->pacPageView;
		m_pacPageView->getPageView()->addEventListener((PageView::ccPageViewCallback)CC_CALLBACK_2(StoreHouseUI::RightPageViewChanged, this));
		u_layer->addChild(m_pacPageView);
		m_pacPageView->setPosition(Vec2(389,125));
		m_pacPageView->setCurUITag(kTagStoreHouseUI);
		m_pacPageView->setVisible(true);
		auto seq = Sequence::create(DelayTime::create(0.05f),CallFunc::create(CC_CALLBACK_0(StoreHouseUI::PageScrollToDefault,this)),NULL);
		m_pacPageView->runAction(seq);
		m_pacPageView->checkCDOnBegan();
		//m_pacPageView->getPageView()->scrollToPage(0);
		//m_pacPageView->getPageView()->onTouchMoved(m_pacPageView->getPageView()->getPage(1)->getPosition());

		//refresh cd
		auto shortCutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortCutLayer)
		{
			for (int i = 0;i<7;++i)  //�һ��7�����
			{
				if (i < GameView::getInstance()->shortCutList.size())
				{
					auto c_shortcut = GameView::getInstance()->shortCutList.at(i);
					if (c_shortcut->storedtype() == 2)    //����������Ʒ
					{
						if(CDrug::isDrug(c_shortcut->skillpropid()))
						{
							if (shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100))
							{
								ShortcutSlot * shortcutSlot = (ShortcutSlot*)shortCutLayer->getChildByTag(shortCutLayer->shortcurTag[c_shortcut->index()]+100);
								this->m_pacPageView->RefreshCD(c_shortcut->skillpropid());
							}
						}
					}
				}
			}
		}

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(StoreHouseUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(StoreHouseUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(StoreHouseUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(StoreHouseUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	} 
	return false;
}

void StoreHouseUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void StoreHouseUI::onExit()
{
	UIScene::onExit();
}

bool StoreHouseUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	Vec2 location = touch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	return false;
}

void StoreHouseUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void StoreHouseUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void StoreHouseUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}


void StoreHouseUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void StoreHouseUI::SortPacEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (m_remainArrangePac > 0.f)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("package_arrange_canNot"));
			return;
		}

		m_remainArrangePac = 15.0f;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1308, (void *)1);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void StoreHouseUI::SortStoreHouseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (m_remainArrangeStore > 0.f)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("package_arrange_canNot"));
			return;
		}

		m_remainArrangeStore = 15.0f;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1308, (void *)2);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void StoreHouseUI::ShopEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
		if (goldStoreUI == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			goldStoreUI = GoldStoreUI::create();
			GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI, 0, kTagGoldStoreUI);

			goldStoreUI->setIgnoreAnchorPointForPosition(false);
			goldStoreUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			goldStoreUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void StoreHouseUI::LeftPageViewChanged(Ref *pSender, PageView::EventType type)
{

	if (type == PageView::EventType::TURNING) {
		auto pageView = dynamic_cast<PageView*>(pSender);
		switch (static_cast<int>(pageView->getCurrentPageIndex()))
		{
		case 0:
			pointLeft->setPosition(Vec2(178, 94));
			break;
		case 1:
			pointLeft->setPosition(Vec2(199, 94));
			break;
		case 2:
			pointLeft->setPosition(Vec2(218, 94));
			break;
		case 3:
			pointLeft->setPosition(Vec2(237, 94));
			break;
		}
	}
}

void StoreHouseUI::RightPageViewChanged(Ref *pSender, PageView::EventType type)
{

	if (type == PageView::EventType::TURNING) {
		auto pageView = dynamic_cast<PageView*>(pSender);
		switch (static_cast<int>(pageView->getCurrentPageIndex()))
		{
		case 0:
			pointRight->setPosition(Vec2(513, 94));
			break;
		case 1:
			pointRight->setPosition(Vec2(534, 94));
			break;
		case 2:
			pointRight->setPosition(Vec2(553, 94));
			break;
		case 3:
			pointRight->setPosition(Vec2(572, 94));
			break;
		case 4:
			pointRight->setPosition(Vec2(591, 94));
			break;
		}
	}
}

void StoreHouseUI::PutInOrOutEvent( Ref * pSender )
{
	if (this->isClosing())
		return;

 	if (GameView::getInstance()->pacPageView->curFolder->quantity()>1)
 	{
 		GameView::getInstance()->showCounter(this, callfuncO_selector(StoreHouseUI::GetStoreItemNum),GameView::getInstance()->pacPageView->curFolder->quantity());
 	}
 	else
 	{
		auto temp = new ReqData();
		temp->mode = 0;
		temp->source = GameView::getInstance()->pacPageView->curPackageItemIndex;
		temp->num = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1319,temp);
		delete temp;
	}
	
}

void StoreHouseUI::ReloadData()
{
	this->m_storePageView->ReloadData();

	//���ֿ
	if (GameView::getInstance()->storeHousePageView->getParent() != NULL) 
	{
		GameView::getInstance()->storeHousePageView->removeFromParent();
	}

	m_storePageView = GameView::getInstance()->storeHousePageView;
	m_storePageView->getPageView()->addEventListener((PageView::ccPageViewCallback)CC_CALLBACK_2(StoreHouseUI::LeftPageViewChanged, this));
	u_layer->addChild(m_storePageView);
	m_storePageView->setPosition(Vec2(75,119));
	m_storePageView->setCurUITag(kTagStoreHouseUI);
	m_storePageView->setVisible(true);
}
// 
// void StoreHouseUI::reloadInit()
// {
// 	m_storePageView = UIPageView::create();
// 	m_storePageView->setTouchEnabled(true);
// 	m_storePageView->setContentSize(Size(278, 324));
// 	m_storePageView->setPosition(Vec2(77,116));
// 
// 	int idx = 0;
// 	for (int i = 0; i < 4; ++i)
// 	{
// 		Layout* panel = Layout::create();
// 		panel->setContentSize(Size(278, 324));
// 		panel->setName(storePageViewPanelName[i].c_str());
// 		panel->setTouchEnabled(true);
// 		if ((int)GameView::getInstance()->storeHouseItemList.size()<=itemNumEveryPage*(i+1))
// 		{
// 			if (itemNumEveryPage*i < (int)GameView::getInstance()->storeHouseItemList.size())
// 			{
// 				for (int m = itemNumEveryPage*i;m<(int)GameView::getInstance()->storeHouseItemList.size();m++)
// 				{
// 					StoreHouseItem * storeHouseItem = StoreHouseItem::create(GameView::getInstance()->storeHouseItemList.at(m));
// 					storeHouseItem->setAnchorPoint(Vec2(0,0));
// 					storeHouseItem->setPosition(Vec2(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
// 					storeHouseItem->setName(storeItem_name[idx%itemNumEveryPage].c_str());
// 					//packageItem->setContentSize(Size(91,84));
// 					storeHouseItem->setScale(1.0f);
// 					panel->addChild(storeHouseItem);
// 					idx++;
// 				}
// 				for (int n = GameView::getInstance()->storeHouseItemList.size();n<itemNumEveryPage*(i+1);n++)
// 				{
// 					//create locked 
// 					StoreHouseItem * storeHouseItem = StoreHouseItem::createLockedItem(n);
// 					storeHouseItem->setAnchorPoint(Vec2(0,0));
// 					storeHouseItem->setPosition(Vec2(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
// 					storeHouseItem->setName(storeItem_name[idx%itemNumEveryPage].c_str());
// 					//packageItem->setContentSize(Size(91,84));
// 					storeHouseItem->setScale(1.0f);
// 					panel->addChild(storeHouseItem);
// 					idx++;
// 				}
// 			}
// 			else
// 			{
// 				for (int n = itemNumEveryPage*i;n<itemNumEveryPage*(i+1);n++)
// 				{
// 					//create locked 
// 					StoreHouseItem * storeHouseItem = StoreHouseItem::createLockedItem(n);
// 					storeHouseItem->setAnchorPoint(Vec2(0,0));
// 					storeHouseItem->setPosition(Vec2(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
// 					storeHouseItem->setName(storeItem_name[idx%itemNumEveryPage].c_str());
// 					//packageItem->setContentSize(Size(91,84));
// 					storeHouseItem->setScale(1.0f);
// 					panel->addChild(storeHouseItem);
// 					idx++;
// 				}
// 			}
// 		}
// 		else
// 		{
// 			for (int m = 0;m<itemNumEveryPage*(i+1);m++)
// 			{
// 				StoreHouseItem * storeHouseItem = StoreHouseItem::create(GameView::getInstance()->storeHouseItemList.at(m));
// 				storeHouseItem->setAnchorPoint(Vec2(0,0));
// 				storeHouseItem->setPosition(Vec2(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
// 				storeHouseItem->setName(storeItem_name[idx%itemNumEveryPage].c_str());
// 				//packageItem->setContentSize(Size(91,84));
// 				storeHouseItem->setScale(1.0f);
// 				panel->addChild(storeHouseItem);
// 				idx++;
// 			}
// 		}
// 		m_storePageView->addPage(panel);
// 		m_storePageView->addPageTurningEvent(this,coco_PageView_PageTurning_selector(StoreHouseUI::LeftPageViewChanged));
// 
// 	}
// 	u_layer->addChild(m_storePageView);
// 
// }

void StoreHouseUI::ReloadOneStoreHouseItem( FolderInfo * folderInfo )
{
	int id = folderInfo->id();
	int pageIndex  = 0 ;

	if (id/this->m_storePageView->itemNumEveryPage < 1)
	{
		pageIndex = 0;
	}
	else if (id/this->m_storePageView->itemNumEveryPage < 2)
	{
		pageIndex = 1;
	}
	else if (id/this->m_storePageView->itemNumEveryPage < 3)
	{
		pageIndex = 2;
	}
	else if (id/this->m_storePageView->itemNumEveryPage < 4)
	{
		pageIndex = 3;
	}
	//delete old
	auto panel = (Layout*)m_storePageView->getPageView()->getChildByName(m_storePageView->storePageViewPanelName[pageIndex].c_str());
	auto oldStoreHouseItem = (StoreHouseItem *)panel->getChildByName(this->m_storePageView->storeItem_name[id%this->m_storePageView->itemNumEveryPage].c_str());
	oldStoreHouseItem->removeFromParentAndCleanup(true);

	auto storeHouseItem = StoreHouseItem::create(folderInfo);
	storeHouseItem->setAnchorPoint(Vec2(0,0));
	storeHouseItem->setPosition(Vec2(this->m_storePageView->CoordinateVector.at(id%this->m_storePageView->itemNumEveryPage).x, this->m_storePageView->CoordinateVector.at(id%this->m_storePageView->itemNumEveryPage).y));
	storeHouseItem->setName(this->m_storePageView->storeItem_name[id%this->m_storePageView->itemNumEveryPage].c_str());
	storeHouseItem->setScale(1.0f);
	panel->addChild(storeHouseItem);
}

void StoreHouseUI::GetStoreItemNum( Ref* pSender )
{
	auto counter =(Counter *)pSender;

	auto temp = new ReqData();
	temp->mode = 0;
	temp->source = GameView::getInstance()->pacPageView->curPackageItemIndex;
	temp->num = counter->getInputNum();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1319,temp);
	delete temp;

}

void StoreHouseUI::update( float dt )
{
	//��
	char str_gold[20];
	sprintf(str_gold,"%d",GameView::getInstance()->getPlayerGold());
	Label_gold->setString(str_gold);
	//�Ԫ��
	char str_ingot[20];
	sprintf(str_ingot,"%d",GameView::getInstance()->getPlayerGoldIngot());
	Label_goldIngot->setString(str_ingot);

	m_remainArrangePac -= 1.0f/60;
	if (m_remainArrangePac < 0)
		m_remainArrangePac = 0.f;

	m_remainArrangeStore -= 1.0f/60;
	if (m_remainArrangeStore < 0)
		m_remainArrangeStore = 0.f;
}

void StoreHouseUI::PageScrollToDefault()
{
	m_pacPageView->getPageView()->scrollToPage(0);
	RightPageViewChanged(m_pacPageView, PageView::EventType::TURNING);
}
