
#ifndef _STOREHOUSEUI_STOREHOUSEPAGEVIEW_H
#define _STOREHOUSEUI_STOREHOUSEPAGEVIEW_H

#include "../extensions/UIScene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GoodsInfo;
class FolderInfo;
class StoreHouseItem;

typedef enum
{
	kTagStoreHousePageView = 10
};

struct Coordinate_StoreHouseItem
{
	float x;
	float y;
};

class StoreHousePageView : public UIScene
{
public:
	StoreHousePageView();
	~StoreHousePageView();

	static StoreHousePageView * create();
	bool init();
	void ReloadOneStoreHouseItem(FolderInfo* folderInfo);

	int getCurUITag();
	void setCurUITag(int tag);

	void reloadInit();
	void reCreatePageVeiw();
	void ReloadData();

public:
	std::vector<Coordinate_StoreHouseItem> CoordinateVector;

	std::string storePageViewPanelName[4];
	std::string storeItem_name[20];

	int curStoreItemIndex;
	FolderInfo * curStoreFolder;

	int pageNum;
	int itemNumEveryPage ;

private: 
	PageView * m_storePageView;
	int curUITag;
public: 
	PageView * getPageView();
	StoreHouseItem * getStoresHouseItem(int index);
};
#endif;

