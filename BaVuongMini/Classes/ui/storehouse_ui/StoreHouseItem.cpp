#include "StoreHouseItem.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../backpackscene/PackageItemInfo.h"
#include "StoreHouseInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "AppMacros.h"


#define voidFramePath "res_ui/sdi_none.png"
#define whiteFramePath "res_ui/sdi_white.png"
#define greenFramePath "res_ui/sdi_green.png"
#define blueFramePath "res_ui/sdi_bule.png"
#define purpleFramePath "res_ui/sdi_purple.png"
#define orangeFramePath "res_ui/sdi_orange.png"

#define fontBgPath "res_ui/zidi.png"

#define M_Scale_Cur 0.95f

StoreHouseItem::StoreHouseItem()
{
}


StoreHouseItem::~StoreHouseItem()
{
}

StoreHouseItem * StoreHouseItem::create( FolderInfo* storeFolder )
{
	auto storeHouseItem = new StoreHouseItem();
	if (storeHouseItem && storeHouseItem->init(storeFolder))
	{
		storeHouseItem->autorelease();
		return storeHouseItem;
	}
	CC_SAFE_DELETE(storeHouseItem);
	return NULL;
}

bool StoreHouseItem::init( FolderInfo* storeFolder )
{
	if (Widget::init())
	{
		curStoreFolder = storeFolder;
		index = storeFolder->id();
		if (storeFolder->has_goods() == true)
		{
			cur_type = COMMONSTOREHOUSEITEM;

			/*********************�ж�װ������ɫ***************************/
			std::string frameColorPath;
			if (storeFolder->goods().quality() == 1)
			{
				frameColorPath = whiteFramePath;
			}
			else if (storeFolder->goods().quality() == 2)
			{
				frameColorPath = greenFramePath;
			}
			else if (storeFolder->goods().quality() == 3)
			{
				frameColorPath = blueFramePath;
			}
			else if (storeFolder->goods().quality() == 4)
			{
				frameColorPath = purpleFramePath;
			}
			else if (storeFolder->goods().quality() == 5)
			{
				frameColorPath = orangeFramePath;
			}
			else
			{
				frameColorPath = voidFramePath;
			}

			auto Btn_storeItemFrame = Button::create();
			Btn_storeItemFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
			Btn_storeItemFrame->setTouchEnabled(true);
			Btn_storeItemFrame->setAnchorPoint(Vec2(0.5f,0.5f));
			Btn_storeItemFrame->setPosition(Vec2(Btn_storeItemFrame->getContentSize().width/2,Btn_storeItemFrame->getContentSize().height/2));
			Btn_storeItemFrame->setScale(M_Scale_Cur);
			Btn_storeItemFrame->setPressedActionEnabled(true);
			Btn_storeItemFrame->addTouchEventListener(CC_CALLBACK_2(StoreHouseItem::StoreHouseItemEvent, this));
			this->addChild(Btn_storeItemFrame);

			auto ImageView_storeItem = ImageView::create();
			//ImageView_storeItem->loadTexture("res_ui/props_icon/cubuyi_n.png");
			if (strcmp(storeFolder->goods().icon().c_str(),"") == 0)
			{
				ImageView_storeItem->loadTexture("res_ui/props_icon/cubuyi.png");
			}
			else
			{
				std::string _path = "res_ui/props_icon/";
				_path.append(storeFolder->goods().icon().c_str());
				_path.append(".png");
				ImageView_storeItem->loadTexture(_path.c_str());
			}
			ImageView_storeItem->setAnchorPoint(Vec2(0.5f,0.5f));
			ImageView_storeItem->setPosition(Vec2(0,0));
			ImageView_storeItem->setScale(M_Scale_Cur);
			Btn_storeItemFrame->addChild(ImageView_storeItem);

			
			char s_num[5];
			sprintf(s_num,"%d",storeFolder->quantity());
			Label_num = Label::createWithTTF(s_num, APP_FONT_NAME, 14);
			Label_num->setAnchorPoint(Vec2(1.0f,0));
			Label_num->setPosition(Vec2(25,-26));
			Btn_storeItemFrame->addChild(Label_num);
			
			if (storeFolder->goods().binding() == 1)
			{
				auto ImageView_bound = ImageView::create();
				ImageView_bound->loadTexture("res_ui/binding.png");
				ImageView_bound->setAnchorPoint(Vec2(0,1.0f));
				ImageView_bound->setScale(M_Scale_Cur);
				ImageView_bound->setPosition(Vec2(-25,26));
				Btn_storeItemFrame->addChild(ImageView_bound);
			}

			//�����װ��
			if(storeFolder->goods().has_equipmentdetail())
			{
				if (storeFolder->goods().equipmentdetail().gradelevel() > 0)  //����ȼ�
				{
					std::string ss_gradeLevel = "+";
					char str_gradeLevel [10];
					sprintf(str_gradeLevel,"%d",storeFolder->goods().equipmentdetail().gradelevel());
					ss_gradeLevel.append(str_gradeLevel);

					auto Lable_gradeLevel = Label::createWithTTF(ss_gradeLevel.c_str(), APP_FONT_NAME, 14);
					Lable_gradeLevel->setAnchorPoint(Vec2(1.0f,1.0f));
					Lable_gradeLevel->setPosition(Vec2(25,26));
					Lable_gradeLevel->setName("Lable_gradeLevel");
					Btn_storeItemFrame->addChild(Lable_gradeLevel);
				}

				if (storeFolder->goods().equipmentdetail().starlevel() > 0)  //�Ǽ�
				{
					char str_starLevel [10];
					sprintf(str_starLevel,"%d",storeFolder->goods().equipmentdetail().starlevel());

					auto Lable_starLevel = Label::createWithTTF(str_starLevel, APP_FONT_NAME, 14);
					Lable_starLevel->setAnchorPoint(Vec2(0,0));
					Lable_starLevel->setPosition(Vec2(-25,-26));
					Lable_starLevel->setName("Lable_starLevel");
					Btn_storeItemFrame->addChild(Lable_starLevel);

					auto ImageView_star = ImageView::create();
					ImageView_star->loadTexture("res_ui/star_on.png");
					ImageView_star->setAnchorPoint(Vec2(0,0));
					ImageView_star->setPosition(Vec2(Lable_starLevel->getContentSize().width,2));
					ImageView_star->setVisible(true);
					ImageView_star->setScale(0.5f);
					Lable_starLevel->addChild(ImageView_star);
				}
			}
		}
		else
		{
			this->cur_type = VOIDSTOREHOUSEITEM;
			//init(VOIDITEM);
			auto imageView_background = ImageView::create();
			imageView_background->loadTexture(voidFramePath);
			imageView_background->setAnchorPoint(Vec2(.5f,.5f));
			imageView_background->setPosition(Vec2(imageView_background->getContentSize().width/2,imageView_background->getContentSize().height/2));
			imageView_background->setScale(M_Scale_Cur);
			imageView_background->setName("imageView_background");
			this->addChild(imageView_background);
		}
		//this->setTouchEnabled(true);
		this->setContentSize(Size(60,60));
		return true;
	}
	return false;
}

StoreHouseItem * StoreHouseItem::createLockedItem( int storeFolderIndex )
{
	auto storeHouseItem = new StoreHouseItem();
	if (storeHouseItem && storeHouseItem->initLockedItem(storeFolderIndex))
	{
		storeHouseItem->autorelease();
		return storeHouseItem;
	}
	CC_SAFE_DELETE(storeHouseItem);
	return NULL;
}

bool StoreHouseItem::initLockedItem( int storeFolderIndex )
{
	if (Widget::init())
	{
		this->setContentSize(Size(60,60));
		this->cur_type = LOCKSTOREHOUSEITEM;
		this->index = storeFolderIndex;

		auto Btn_lockFrame = Button::create();
		Btn_lockFrame->loadTextures(voidFramePath,voidFramePath,"");
		Btn_lockFrame->setTouchEnabled(true);
		Btn_lockFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Btn_lockFrame->setPosition(Vec2(Btn_lockFrame->getContentSize().width/2,Btn_lockFrame->getContentSize().height/2));
		Btn_lockFrame->setScale(M_Scale_Cur);
		Btn_lockFrame->setPressedActionEnabled(true);
		Btn_lockFrame->addTouchEventListener(CC_CALLBACK_2(StoreHouseItem::BtnLockEvent, this));
		this->addChild(Btn_lockFrame);

		//��ͼ�
		auto imageView_lock = ImageView::create();
		imageView_lock->loadTexture("res_ui/suo.png");
		imageView_lock->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_lock->setPosition(Vec2(0,0));
		imageView_lock->setName("imageView_lock");
		Btn_lockFrame->addChild(imageView_lock);

		return true;
	}
	return false;
}

void StoreHouseItem::StoreHouseItemEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto winSize = Director::getInstance()->getVisibleSize();
		auto storeHouseInfo = StoreHouseInfo::create(curStoreFolder);
		storeHouseInfo->setIgnoreAnchorPointForPosition(false);
		storeHouseInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
		//storeHouseInfo->setPosition(Vec2(winSize.width/2,winSize.height/2));
		GameView::getInstance()->getMainUIScene()->addChild(storeHouseInfo);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void StoreHouseItem::BtnLockEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->index >= (int)GameView::getInstance()->storeHouseItemList.size())
		{
			// 		int num = this->index + 1 - GameView::getInstance()->storeHouseItemList.size();
			// 		std::string dialog = "suer to open ";
			// 		char * s = new char [5];
			// 		sprintf(s,"%d",num);
			// 		dialog.append(s);
			// 		delete [] s;
			// 		dialog.append(" folder ? ");
			// 		GameView::getInstance()->showPopupWindow(dialog,2,this,SEL_CallFuncO(StoreHouseItem::SureEvent),SEL_CallFuncO(StoreHouseItem::CancelEvent));
			int num = this->index + 1 - GameView::getInstance()->storeHouseItemList.size();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1320, (void *)1, (void *)num);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

cocos2d::Vec2 StoreHouseItem::autoGetPosition( Ref * pSender )
{
	return Vec2(400,240);
}

void StoreHouseItem::SureEvent( Ref * pSender )
{
	int num =  this->index + 1 - GameView::getInstance()->storeHouseItemList.size();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1320,(void *)1,(void *)num);
}

void StoreHouseItem::CancelEvent( Ref * pSender )
{

}
