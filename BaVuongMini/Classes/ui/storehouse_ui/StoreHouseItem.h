
#ifndef _STOREHOUSE_STOREHOSUEITEM_H_
#define _STOREHOUSE_STOREHOSUEITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class FolderInfo;

typedef enum {
	COMMONSTOREHOUSEITEM,
	VOIDSTOREHOUSEITEM,
	LOCKSTOREHOUSEITEM,
}storeHouseItemType;


class StoreHouseItem : public Widget
{
public:
	StoreHouseItem();
	~StoreHouseItem();

	static StoreHouseItem * create(FolderInfo* storeFolder);
	bool init(FolderInfo* storeFolder);
	static StoreHouseItem * createLockedItem(int storeFolderIndex);
	bool initLockedItem(int storeFolderIndex);
	FolderInfo * curStoreFolder;

	void StoreHouseItemEvent(Ref *pSender, Widget::TouchEventType type);
	Vec2 autoGetPosition(Ref * pSender);

	void BtnLockEvent(Ref *pSender, Widget::TouchEventType type);
	void SureEvent(Ref * pSender);
	void CancelEvent(Ref * pSender);

protected:
	storeHouseItemType cur_type;
	int index ;
	ImageView *ImageView_storeItem;
	Label * Label_num;

};

#endif

