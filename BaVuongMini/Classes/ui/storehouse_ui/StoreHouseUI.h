#ifndef _STOREHOUSEUI_STOREHOUSEUI_H
#define _STOREHOUSEUI_STOREHOUSEUI_H

#include "../extensions/UIScene.h"
#include "../backpackscene/PacPageView.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class FolderInfo;
class PacPageView;
class StoreHousePageView;

class StoreHouseUI : public UIScene
{
public:
	StoreHouseUI();
	~StoreHouseUI();

	struct ReqData{
		int mode;
		int source;
		int num;
	};

	static StoreHouseUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

// 	std::vector<Coordinate> CoordinateVector;
// 
// 	std::string storePageViewPanelName[4];
// 	std::string storeItem_name[20];
// 
// 	int curStoreItemIndex;
// 	FolderInfo * curStoreFolder;
// 
// 	int itemNumEveryPage ;

	//�����Ǳ�����ǲֿ��Ĭ��Ϊ���
	bool storeOrPac ;

	void PutInOrOutEvent(Ref * pSender);

	void ReloadData();
	void ReloadOneStoreHouseItem(FolderInfo * folderInfo);

	void PageScrollToDefault();

private: 
	Layer * u_layer;
	Layout * ppanel;
	StoreHousePageView * m_storePageView;
	PacPageView * m_pacPageView;

	//���pageviewߵ�ָʾ�
	ImageView * pointLeft;
	//��ұpageviewߵ�ָʾ�
	ImageView * pointRight;

	Text *Label_gold;
	Text *Label_goldIngot;

	void reloadInit();

	virtual void update(float dt);

public:
	float m_remainArrangePac;
	float m_remainArrangeStore;
private:
	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void SortPacEvent(Ref *pSender, Widget::TouchEventType type);
	void SortStoreHouseEvent(Ref *pSender, Widget::TouchEventType type);
	void ShopEvent(Ref *pSender, Widget::TouchEventType type);

	void LeftPageViewChanged(Ref *pSender, PageView::EventType type);
	void RightPageViewChanged(Ref *pSender, PageView::EventType type);

	void GetStoreItemNum(Ref* pSender);
};

#endif