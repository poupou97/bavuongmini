#include "StoreHousePageView.h"
#include "../backpackscene/PackageItem.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../messageclient/element/CDrug.h"
#include "StoreHouseItem.h"

StoreHousePageView::StoreHousePageView():
pageNum(4),
itemNumEveryPage(20),
curUITag(kTagMainSceneRoot)
{
	//coordinate for storeHouseItem
	for (int m = 4;m>=0;m--)
	{
		for (int n = 3;n>=0;n--)
		{
			int _m = 4-m;
			int _n = 3-n;

			Coordinate_StoreHouseItem temp ;
			temp.x = 6+_n*68;
			temp.y = 1+m*63;

			CoordinateVector.push_back(temp);
		}
	}

	//UIPageView_Panel_Name
	for (int i = 0;i<pageNum;i++)
	{
		std::string storePageViewBaseName = "storePageViewPanel_";
		char num[4];
		int tmpID = i;
		sprintf(num, "0%d", tmpID);
		std::string number = num;
		storePageViewBaseName.append(num);
		storePageViewPanelName[i] = storePageViewBaseName;
	}
	//storeHouseItem name
	for (int i=0 ;i<itemNumEveryPage; ++i)
	{
		std::string storeBaseName = "storeItem_";
		char num[4];
		int tmpID = i;
		if(tmpID < 10)
			sprintf(num, "0%d", tmpID);
		else
			sprintf(num, "%d", tmpID);
		std::string number = num;
		storeBaseName.append(num);
		storeItem_name[i] = storeBaseName;
	}
}


StoreHousePageView::~StoreHousePageView()
{
}

StoreHousePageView * StoreHousePageView::create()
{
	auto storeHousePageView = new StoreHousePageView();
	if (storeHousePageView && storeHousePageView->init())
	{
		storeHousePageView->autorelease();
		return storeHousePageView;
	}
	CC_SAFE_DELETE(storeHousePageView);
	return NULL;
}

bool StoreHousePageView::init()
{
	if (UIScene::init())
	{
		// Create the page view
		m_storePageView = PageView::create();
		m_storePageView->setTouchEnabled(true);
		m_storePageView->setContentSize(Size(278, 324));
		m_storePageView->setPosition(Vec2(0,0));
		m_storePageView->setName("m_storePageView");
		m_storePageView->setTag(kTagStoreHousePageView);

		int idx = 0;
		for (int i = 0; i < pageNum; ++i)
		{
			auto panel = Layout::create();
			panel->setContentSize(Size(278, 324));
			panel->setName(storePageViewPanelName[i].c_str());
			panel->setTouchEnabled(true);
			if ((int)GameView::getInstance()->storeHouseItemList.size()<=itemNumEveryPage*(i+1))
			{
				if (itemNumEveryPage*i < (int)GameView::getInstance()->storeHouseItemList.size())
				{
					for (int m = itemNumEveryPage*i;m<(int)GameView::getInstance()->storeHouseItemList.size();m++)
					{
						auto storeHouseItem = StoreHouseItem::create(GameView::getInstance()->storeHouseItemList.at(m));
						storeHouseItem->setAnchorPoint(Vec2(0,0));
						storeHouseItem->setPosition(Vec2(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
						storeHouseItem->setName(storeItem_name[idx%itemNumEveryPage].c_str());
						//packageItem->setContentSize(Size(91,84));
						storeHouseItem->setScale(1.0f);
						panel->addChild(storeHouseItem);
						idx++;
					}
					for (int n = GameView::getInstance()->storeHouseItemList.size();n<itemNumEveryPage*(i+1);n++)
					{
						//create locked 
						auto storeHouseItem = StoreHouseItem::createLockedItem(n);
						storeHouseItem->setAnchorPoint(Vec2(0,0));
						storeHouseItem->setPosition(Vec2(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
						storeHouseItem->setName(storeItem_name[idx%itemNumEveryPage].c_str());
						//packageItem->setContentSize(Size(91,84));
						storeHouseItem->setScale(1.0f);
						panel->addChild(storeHouseItem);
						idx++;
					}
				}
				else
				{
					for (int n = itemNumEveryPage*i;n<itemNumEveryPage*(i+1);n++)
					{
						//create locked 
						auto storeHouseItem = StoreHouseItem::createLockedItem(n);
						storeHouseItem->setAnchorPoint(Vec2(0,0));
						storeHouseItem->setPosition(Vec2(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
						storeHouseItem->setName(storeItem_name[idx%itemNumEveryPage].c_str());
						//packageItem->setContentSize(Size(91,84));
						storeHouseItem->setScale(1.0f);
						panel->addChild(storeHouseItem);
						idx++;
					}
				}
			}
			else
			{
				for (int m = itemNumEveryPage*i;m<itemNumEveryPage*(i+1);m++)
				{
					auto storeHouseItem = StoreHouseItem::create(GameView::getInstance()->storeHouseItemList.at(m));
					storeHouseItem->setAnchorPoint(Vec2(0,0));
					storeHouseItem->setPosition(Vec2(CoordinateVector.at(idx%itemNumEveryPage).x, CoordinateVector.at(idx%itemNumEveryPage).y));
					storeHouseItem->setName(storeItem_name[idx%itemNumEveryPage].c_str());
					//packageItem->setContentSize(Size(91,84));
					storeHouseItem->setScale(1.0f);
					panel->addChild(storeHouseItem);
					idx++;
				}
			}
			m_storePageView->addPage(panel);
		}
		m_pLayer->addChild(m_storePageView);

		return true;
	}
	return false;
}

PageView * StoreHousePageView::getPageView()
{
	return m_storePageView;
}

StoreHouseItem * StoreHousePageView::getStoresHouseItem( int index )
{
	int pageIndex = index/itemNumEveryPage;
	auto _curPanel = (Layout *)m_storePageView->getChildByName(storeItem_name[pageIndex].c_str());
	auto storeHouseItem = (StoreHouseItem*)_curPanel->getChildByName(storeItem_name[index%itemNumEveryPage].c_str());
	return storeHouseItem;
}

void StoreHousePageView::ReloadOneStoreHouseItem(FolderInfo * folderInfo)
{
	int id = folderInfo->id();
	int pageIndex  = 0 ;

	if (id/this->itemNumEveryPage < 1)
	{
		pageIndex = 0;
	}
	else if (id/this->itemNumEveryPage < 2)
	{
		pageIndex = 1;
	}
	else if (id/this->itemNumEveryPage < 3)
	{
		pageIndex = 2;
	}
	else if (id/this->itemNumEveryPage < 4)
	{
		pageIndex = 3;
	}
	else if (id/this->itemNumEveryPage < 5)
	{
		pageIndex = 4;
	}

	//delete old
	auto panel = (Layout*)this->getPageView()->getChildByName(this->storeItem_name[pageIndex].c_str());
	auto oldStoreHouseItem = (StoreHouseItem *)panel->getChildByName(this->storeItem_name[id%this->itemNumEveryPage].c_str());
	oldStoreHouseItem->removeFromParent();

	auto storeHouseItem = StoreHouseItem::create(folderInfo);
	storeHouseItem->setAnchorPoint(Vec2(0,0));
	storeHouseItem->setPosition(Vec2(this->CoordinateVector.at(id%this->itemNumEveryPage).x, this->CoordinateVector.at(id%this->itemNumEveryPage).y));
	storeHouseItem->setName(this->storeItem_name[id%this->itemNumEveryPage].c_str());
	storeHouseItem->setScale(1.0f);
	panel->addChild(storeHouseItem);
}

int StoreHousePageView::getCurUITag()
{
	return curUITag;
}

void StoreHousePageView::setCurUITag(int tag)
{
	curUITag = tag;
}


void StoreHousePageView::ReloadData()
{
	for (int i = 0;i<pageNum;i++)
	{
		auto tempPanel = (Layout*)this->getPageView()->getChildByName(this->storePageViewPanelName[i].c_str());
		for (int j = 0;j<itemNumEveryPage;j++)
		{
			auto tempItem = (StoreHouseItem *)tempPanel->getChildByName(this->storeItem_name[j].c_str());
			if (tempItem)
				tempItem->removeFromParentAndCleanup(false);

			int a = j;
		}

	}
	if (this->getPageView()->getParent() != NULL)
	{
		this->getPageView()->removeAllChildrenWithCleanup(true);
	}
	reloadInit();

}

void StoreHousePageView::reloadInit()
{
	reCreatePageVeiw();
}

void StoreHousePageView::reCreatePageVeiw()
{
	if (GameView::getInstance()->storeHousePageView->getReferenceCount()>0)
	{
		GameView::getInstance()->storeHousePageView->release();
	}
	GameView::getInstance()->storeHousePageView = StoreHousePageView::create();
	GameView::getInstance()->storeHousePageView->retain();
}

