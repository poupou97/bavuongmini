#ifndef _STOREHOUSEUI_STOREHOUSESETTINGUI_H
#define _STOREHOUSEUI_STOREHOUSESETTINGUI_H

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class StoreHouseSettingUI : public UIScene
{
public:
	StoreHouseSettingUI();
	~StoreHouseSettingUI();

	static StoreHouseSettingUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	Layout * ppanel;
	Layer * u_layer;

	Layout * panel_enterPassWord;
	Layer * layer_enterPassWord;
	Layout * panel_changePassWord;
	Layer * layer_changePassWord;
	Layout * panel_setPassWord;
	Layer * layer_setPassWord;

private:
	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

};

#endif

