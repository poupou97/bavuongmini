#include "StoreHouseInfo.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../GameView.h"
#include "StoreHouseUI.h"
#include "../extensions/Counter.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"

StoreHouseInfo::StoreHouseInfo():
m_nfolderIndex(0)
{
}


StoreHouseInfo::~StoreHouseInfo()
{
}

StoreHouseInfo * StoreHouseInfo::create( FolderInfo * folder )
{
	auto storeHosueInfo = new StoreHouseInfo();
	if (storeHosueInfo && storeHosueInfo->init(folder))
	{
		storeHosueInfo->autorelease();
		return storeHosueInfo;
	}
	CC_SAFE_DELETE(storeHosueInfo);
	return NULL;
}

bool StoreHouseInfo::init( FolderInfo * folder )
{
	auto goodsInfo = new GoodsInfo();
	goodsInfo->CopyFrom(folder->goods());
	m_nfolderIndex = folder->id();
	m_quantity = folder->quantity();
	if (goodsInfo != NULL)
	if (GoodsItemInfoBase::init(goodsInfo,GameView::getInstance()->EquipListItem,0))
	{
		delete goodsInfo;
		//�Ӳֿ�ȡ�
		auto Button_getOut = Button::create();
		Button_getOut->loadTextures(buttonImagePath.c_str(),
			buttonImagePath.c_str(),
			"");
		Button_getOut->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_getOut->setPosition(Vec2(141,25));
		Button_getOut->setScale9Enabled(true);
		Button_getOut->setContentSize(Size(91,43));
		Button_getOut->setCapInsets(Rect(18,9,2,23));
		Button_getOut->setTouchEnabled(true);
		Button_getOut->addTouchEventListener(CC_CALLBACK_2(StoreHouseInfo::GetOutEvent, this));
		Button_getOut->setPressedActionEnabled(true);

		const char *strings_getOut = StringDataManager::getString("storeHouse_getOut");

		auto Label_getOut = Label::createWithTTF(strings_getOut, APP_FONT_NAME, 18);
		Label_getOut->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_getOut->setPosition(Vec2(0,0));
		Button_getOut->addChild(Label_getOut);
		m_pLayer->addChild(Button_getOut);

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setAnchorPoint(Vec2(0,0));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(StoreHouseInfo::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(StoreHouseInfo::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(StoreHouseInfo::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(StoreHouseInfo::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void StoreHouseInfo::onEnter()
{
	GoodsItemInfoBase::onEnter();
	this->openAnim();
}
void StoreHouseInfo::onExit()
{
	GoodsItemInfoBase::onExit();
}

bool StoreHouseInfo::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return this->resignFirstResponder(pTouch,this,false);
}
void StoreHouseInfo::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void StoreHouseInfo::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void StoreHouseInfo::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}


void StoreHouseInfo::GetOutEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->isClosing())
			return;

		if (this->m_quantity>1)
		{
			GameView::getInstance()->showCounter(this, callfuncO_selector(StoreHouseInfo::GetStoreItemNum), this->m_quantity);
		}
		else
		{
			auto temp = new StoreHouseUI::ReqData();
			temp->mode = 1;
			temp->source = this->m_nfolderIndex;
			temp->num = 1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1319, temp);
			delete temp;

			this->removeFromParent();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void StoreHouseInfo::GetStoreItemNum( Ref *pSender )
{
	auto counter =(Counter *)pSender;

	StoreHouseUI::ReqData * temp = new StoreHouseUI::ReqData();
	temp->mode = 1;
	temp->source = this->m_nfolderIndex;
	temp->num = counter->getInputNum();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1319,temp);
	delete temp;

	this->removeFromParent();
}
