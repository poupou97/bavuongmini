#include "StoreHouseSettingUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/RichTextInput.h"
#include "cocostudio\CCSGUIReader.h"

StoreHouseSettingUI::StoreHouseSettingUI()
{
}


StoreHouseSettingUI::~StoreHouseSettingUI()
{
}

StoreHouseSettingUI* StoreHouseSettingUI::create()
{
	auto storeHouseSettingUI = new StoreHouseSettingUI();
	if (storeHouseSettingUI && storeHouseSettingUI->init())
	{
		storeHouseSettingUI->autorelease();
		return storeHouseSettingUI;
	}
	CC_SAFE_DELETE(storeHouseSettingUI);
	return NULL;
}

bool StoreHouseSettingUI::init()
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();

		this->scheduleUpdate();

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(800, 480));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		//���UI
		if(LoadSceneLayer::StoreHouseSettingLayer == NULL)
		{
			LoadSceneLayer::StoreHouseSettingLayer=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/password_popupo_1.json");
			LoadSceneLayer::StoreHouseSettingLayer->retain();
		}
		if(LoadSceneLayer::StoreHouseSettingLayer->getParent() != NULL)
		{
			LoadSceneLayer::StoreHouseSettingLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (Layout*)LoadSceneLayer::s_pPanel->copy();
		ppanel = LoadSceneLayer::StoreHouseSettingLayer;
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->getVirtualRenderer()->setContentSize(Size(354, 315));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		//عرհ�ť
		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->setPressedActionEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(StoreHouseSettingUI::CloseEvent, this));

		panel_enterPassWord = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_EnterPassword");
		panel_enterPassWord->setVisible(false);
		layer_enterPassWord = Layer::create();
		layer_enterPassWord->setVisible(false);
		panel_changePassWord = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_ChangePassword");
		panel_changePassWord->setVisible(false);
		layer_changePassWord = Layer::create();
		layer_changePassWord->setVisible(false);
		panel_setPassWord = (Layout *)Helper::seekWidgetByName(ppanel,"Panel_SetPassword");
		panel_setPassWord->setVisible(false);
		layer_setPassWord = Layer::create();
		layer_setPassWord->setVisible(false);
		///////////
		auto Enter_enterPassWord = new RichTextInputBox();
		Enter_enterPassWord->setInputBoxWidth(163);
		Enter_enterPassWord->setAnchorPoint(Vec2(0,0));
		Enter_enterPassWord->setPosition(Vec2(131,142));
		Enter_enterPassWord->setCharLimit(10);
		layer_enterPassWord->addChild(Enter_enterPassWord);
		Enter_enterPassWord->autorelease();
		////////////
		auto Set_enterPassWord = new RichTextInputBox();
		Set_enterPassWord->setInputBoxWidth(163);
		Set_enterPassWord->setAnchorPoint(Vec2(0,0));
		Set_enterPassWord->setPosition(Vec2(131,165));
		Set_enterPassWord->setCharLimit(10);
		layer_setPassWord->addChild(Set_enterPassWord);
		Set_enterPassWord->autorelease();
		auto Set_enterPassWordAgain = new RichTextInputBox();
		Set_enterPassWordAgain->setInputBoxWidth(163);
		Set_enterPassWordAgain->setAnchorPoint(Vec2(0,0));
		Set_enterPassWordAgain->setPosition(Vec2(131,120));
		Set_enterPassWordAgain->setCharLimit(10);
		layer_setPassWord->addChild(Set_enterPassWordAgain);
		Set_enterPassWordAgain->autorelease();
		///////////
		auto Change_enterOldPassWord = new RichTextInputBox();
		Change_enterOldPassWord->setInputBoxWidth(163);
		Change_enterOldPassWord->setAnchorPoint(Vec2(0,0));
		Change_enterOldPassWord->setPosition(Vec2(131,174));
		Change_enterOldPassWord->setCharLimit(10);
		layer_changePassWord->addChild(Change_enterOldPassWord);
		Change_enterOldPassWord->autorelease();
		auto Change_enterNewPassWord = new RichTextInputBox();
		Change_enterNewPassWord->setInputBoxWidth(163);
		Change_enterNewPassWord->setAnchorPoint(Vec2(0,0));
		Change_enterNewPassWord->setPosition(Vec2(131,132));
		Change_enterNewPassWord->setCharLimit(10);
		layer_changePassWord->addChild(Change_enterNewPassWord);
		Change_enterNewPassWord->autorelease();
		auto Change_enterNewPassWordAgain = new RichTextInputBox();
		Change_enterNewPassWordAgain->setInputBoxWidth(163);
		Change_enterNewPassWordAgain->setAnchorPoint(Vec2(0,0));
		Change_enterNewPassWordAgain->setPosition(Vec2(131,89));
		Change_enterNewPassWordAgain->setCharLimit(10);
		layer_changePassWord->addChild(Change_enterNewPassWordAgain);
		Change_enterNewPassWordAgain->autorelease();

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(StoreHouseSettingUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(StoreHouseSettingUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(StoreHouseSettingUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(StoreHouseSettingUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void StoreHouseSettingUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void StoreHouseSettingUI::onExit()
{
	UIScene::onExit();
}

bool StoreHouseSettingUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	Vec2 location = touch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}

	return false;
}

void StoreHouseSettingUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void StoreHouseSettingUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void StoreHouseSettingUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void StoreHouseSettingUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}
