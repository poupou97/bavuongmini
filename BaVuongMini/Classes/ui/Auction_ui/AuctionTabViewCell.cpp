#include "AuctionTabViewCell.h"
#include "../extensions/CCMoveableMenu.h"
#include "GameView.h"
#include "../Auction_ui/AuctionUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CAuctionInfo.h"
#include "../../utils/StaticDataManager.h"
#include "AuctionGoodsInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "AppMacros.h"


AuctionTabViewCell::AuctionTabViewCell(void)
{
}


AuctionTabViewCell::~AuctionTabViewCell(void)
{
}

AuctionTabViewCell* AuctionTabViewCell::create(int idx)
{
	auto cell=new AuctionTabViewCell();
	if (cell && cell->init(idx))
	{
		cell->autorelease();
		return cell;
	}
	CC_SAFE_DELETE(cell);
	return NULL;
}

bool AuctionTabViewCell::init(int idx)
{
	if (TableViewCell::init())
	{
		winsize = Director::getInstance()->getVisibleSize();
		auctionui =(AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
		currId=idx;

		auto imageBackGround = cocos2d::extension::Scale9Sprite::create("res_ui/kuang0_new.png");
		imageBackGround->setPreferredSize(Size(460,70));
		imageBackGround->setCapInsets(Rect(15,30,1,1));
		MenuItemSprite *item;
		if (auctionui->tabsType==0)
		{			
			item = MenuItemSprite::create(imageBackGround, imageBackGround, imageBackGround, CC_CALLBACK_1(AuctionTabViewCell::goodsPurchase,this));
			auto selectMenu=CCMoveableMenu::create(item,NULL);
			selectMenu->setAnchorPoint(Vec2(0,0));
			selectMenu->setPosition(Vec2(232,35));
			addChild(selectMenu);

		}else
		if (auctionui->tabsType==1)
		{
			item = MenuItemSprite::create(imageBackGround, imageBackGround, imageBackGround, CC_CALLBACK_1(AuctionTabViewCell::goodsConsign,this));
			auto selectMenu=CCMoveableMenu::create(item,NULL);
			selectMenu->setAnchorPoint(Vec2(0,0));
			selectMenu->setPosition(Vec2(232,35));
			addChild(selectMenu);
		}
		item->setScale(1.0f);
		int price_= GameView::getInstance()->auctionSourceVector.at(idx)->price();
		int period_= GameView::getInstance()->auctionSourceVector.at(idx)->period();
		std::string goodsIcon_ = GameView::getInstance()->auctionSourceVector.at(idx)->goods().icon();
		std::string goodsName_ =GameView::getInstance()->auctionSourceVector.at(idx)->goods().name();
		int count_ = GameView::getInstance()->auctionSourceVector.at(idx)->quantity();
		int goodsLevel_ =GameView::getInstance()->auctionSourceVector.at(idx)->goods().uselevel();
		int priceType_ =GameView::getInstance()->auctionSourceVector.at(idx)->currency();

		int equipmentquality_ =GameView::getInstance()->auctionSourceVector.at(idx)->goods().quality();
		std::string qualityStr_ = getEquipmentQualityByIndex(equipmentquality_);
		auto cloud = cocos2d::extension::Scale9Sprite::create(qualityStr_.c_str());
		cloud->setPreferredSize(Size(50,50));
		cloud->setAnchorPoint(Vec2(0.5f,0.5f));
		cloud->setPosition(Vec2(45,36));
		addChild(cloud);

		std::string icon_ ="res_ui/props_icon/"; 
		icon_.append(goodsIcon_);
		icon_.append(".png");
		auto spbg=Sprite::create(icon_.c_str());
		spbg->setAnchorPoint(Vec2(0.5f,0.5f));
		spbg->setPosition(Vec2(45,35));
		spbg->setScale(0.9f);
		addChild(spbg);

		char amountStr[10];
		sprintf(amountStr,"%d",count_);
		auto goodsCount = Label::createWithTTF(amountStr, APP_FONT_NAME, 16);
		goodsCount->setAnchorPoint(Vec2(1,1));
		goodsCount->setPosition(Vec2(spbg->getContentSize().width-2,20));
		spbg->addChild(goodsCount);


// 		Label * goodsCount  =Label::createWithTTF();
// 		goodsCount->setAnchorPoint(Vec2(1,1));
// 		goodsCount->setPosition(Vec2(spbg->getContentSize().width,20));
// 		goodsCount->setFontSize(16);
// 		goodsCount->setString(amountStr);
// 		spbg->addChild(goodsCount);

		auto equipmentColor_ = getEquipmentColorByQuality(equipmentquality_);
		auto label_goods = Label::createWithTTF(amountStr, APP_FONT_NAME, 16);
		label_goods->setAnchorPoint(Vec2(0,0));
		label_goods->setPosition(Vec2(45+cloud->getContentSize().width/2,30));
		label_goods->setColor(equipmentColor_);
		label_goods->setString(goodsName_.c_str());
		addChild(label_goods);


// 		Label * label_goods  =Label::createWithTTF();
// 		label_goods->setAnchorPoint(Vec2(0,0));
// 		label_goods->setPosition(Vec2(45+cloud->getContentSize().width/2,30));
// 		label_goods->setFontSize(16);
// 		label_goods->setColor(equipmentColor_);
// 		label_goods->setString(goodsName_.c_str());
// 		addChild(label_goods);

		char proplevelStr[10];
		sprintf(proplevelStr,"%d",goodsLevel_);
		auto label_propLebel = Label::createWithTTF(proplevelStr, APP_FONT_NAME, 16);
		label_propLebel->setAnchorPoint(Vec2(0,0));
		label_propLebel->setPosition(Vec2(180,30));
		addChild(label_propLebel);


// 		Label * label_propLebel  =Label::createWithTTF();
// 		label_propLebel->setAnchorPoint(Vec2(0,0));
// 		label_propLebel->setPosition(Vec2(180,30));
// 		label_propLebel->setFontSize(16);
// 		label_propLebel->setString(proplevelStr);
// 		addChild(label_propLebel);

		const char *str_rematin = StringDataManager::getString("auction_remaintime");
		int time_Hour = period_/3600;

		std::string remaim_time="";
		char preiodStr[10];
		if (period_%3600 > 0)
		{
			time_Hour+=1;
		}
		sprintf(preiodStr,"%d",time_Hour);
		remaim_time.append(preiodStr);
		remaim_time.append(str_rematin);

		auto label_preiod = Label::createWithTTF(remaim_time.c_str(), APP_FONT_NAME, 16);
		label_preiod->setAnchorPoint(Vec2(0,0));
		label_preiod->setPosition(Vec2(250,30));
		addChild(label_preiod);

// 		Label * label_preiod  =Label::createWithTTF();
// 		label_preiod->setAnchorPoint(Vec2(0,0));
// 		label_preiod->setPosition(Vec2(250,30));
// 		label_preiod->setFontSize(16);
// 		label_preiod->setString(remaim_time.c_str());
// 		addChild(label_preiod);


		//ingot.png  coins.png
		std::string pricetypeStr_ ="res_ui/"; 
		if (priceType_ == 1)
		{
			pricetypeStr_.append("coins");
		}else
		{
			pricetypeStr_.append("ingot");
		}
		pricetypeStr_.append(".png");
		auto priceTypeIcon_=Sprite::create(pricetypeStr_.c_str());
		priceTypeIcon_->setAnchorPoint(Vec2(0,0));
		priceTypeIcon_->setPosition(Vec2(340 ,30));
		addChild(priceTypeIcon_);

		char priceStr[10];
		sprintf(priceStr,"%d",price_);
		auto label_price = Label::createWithTTF(priceStr, APP_FONT_NAME, 16);
		label_price->setAnchorPoint(Vec2(0,0));
		label_price->setPosition(Vec2(340+priceTypeIcon_->getContentSize().width+5,30));
		addChild(label_price);

// 		Label * label_price  =Label::createWithTTF();
// 		label_price->setAnchorPoint(Vec2(0,0));
// 		label_price->setPosition(Vec2(360+priceTypeIcon_->getContentSize().width+5,30));
// 		label_price->setFontSize(16);
// 		label_price->setString(priceStr);
// 		addChild(label_price);
		return true;
	}
	return false;
}

void AuctionTabViewCell::onEnter()
{
	TableViewCell::onEnter();
}

void AuctionTabViewCell::onExit()
{
	TableViewCell::onExit();
}
void AuctionTabViewCell::goodsPurchase( Ref * obj )
{
	/*
	GoodsInfo * goods_ =new GoodsInfo();
	goods_->CopyFrom(GameView::getInstance()->auctionSourceVector.at(currId)->goods());  

	AuctionGoodsInfo * purchase_ =AuctionGoodsInfo::create(goods_,auctionui->tabsType);
	purchase_->setIgnoreAnchorPointForPosition(false);
	purchase_->setAnchorPoint(Vec2(0.5f,0.5f));
	//purchase_->setPosition(Vec2(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(purchase_);
	*/
}

void AuctionTabViewCell::goodsConsign( Ref * obj )
{
	/*
	GoodsInfo * goods_ =new GoodsInfo();
	goods_->CopyFrom(GameView::getInstance()->auctionSourceVector.at(currId)->goods());  

	AuctionGoodsInfo * consign =AuctionGoodsInfo::create(goods_,auctionui->tabsType);
	consign->setIgnoreAnchorPointForPosition(false);
	consign->setAnchorPoint(Vec2(0.5f,0.5f));
	//consign->setPosition(Vec2(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(consign);
	*/
}

std::string AuctionTabViewCell::getEquipmentQualityByIndex( int quality )
{
	std::string frameColorPath = "res_ui/";
	switch(quality)
	{
	case 1:
		{
			frameColorPath.append("smdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("smdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("smdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("smdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("smdi_orange");
		}break;
	}
	frameColorPath.append(".png");
	return frameColorPath;
}

cocos2d::Color3B AuctionTabViewCell::getEquipmentColorByQuality( int quality )
{
	Color3B color_;
	switch(quality)
	{
	case 1:
		{
			color_ =Color3B(255,255,255);
		}break;
	case 2:
		{
			color_=Color3B(136,234,31);
		}break;
	case 3:
		{
			color_=Color3B(15,202,250);
		}break;
	case 4:
		{
			color_=Color3B(255,62,253);
		}break;
	case 5:
		{
			color_=Color3B(250,155,15);
		}break;
	}

	return color_;
}
