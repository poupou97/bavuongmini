#include "AuctionGoodsInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/StaticDataManager.h"
#include "AuctionUi.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "AppMacros.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"

AuctionGoodsInfo::AuctionGoodsInfo(void)
{
}


AuctionGoodsInfo::~AuctionGoodsInfo(void)
{
}

AuctionGoodsInfo * AuctionGoodsInfo::create( GoodsInfo * goods,int curTab )
{
	auto auction_ =new AuctionGoodsInfo();
	if (auction_ && auction_->init(goods,curTab))
	{
		auction_->autorelease();
		return auction_;
	}
	CC_SAFE_DELETE(auction_);
	return NULL;
}

bool AuctionGoodsInfo::init(GoodsInfo * goods,int curTab)
{
	if (GoodsItemInfoBase::init(goods,GameView::getInstance()->EquipListItem,0))
	{
		m_goodsName = goods->name();
		switch(curTab)
		{
		case 0:
			{
				const char *str_buy = StringDataManager::getString("goods_auction_buy");
				const char *str_cancel = StringDataManager::getString("goods_auction_search");
				//��� ����
				auto Button_purchase= Button::create();
				Button_purchase->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
				Button_purchase->setTouchEnabled(true);
				Button_purchase->setPressedActionEnabled(true);
				Button_purchase->addTouchEventListener(CC_CALLBACK_2(AuctionGoodsInfo::callBackPurchase, this));
				Button_purchase->setAnchorPoint(Vec2(0.5f,0.5f));
				Button_purchase->setScale9Enabled(true);
				Button_purchase->setContentSize(Size(80,43));
				Button_purchase->setCapInsets(Rect(18,9,2,23));
				Button_purchase->setPosition(Vec2(80,25));

				auto Label_purchase = Label::createWithTTF(str_buy, APP_FONT_NAME, 18);
				Label_purchase->setAnchorPoint(Vec2(0.5f,0.5f));
				Label_purchase->setPosition(Vec2(0,0));
				Button_purchase->addChild(Label_purchase);
				m_pLayer->addChild(Button_purchase);

				auto Button_cancel= Button::create();
				Button_cancel->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
				Button_cancel->setTouchEnabled(true);
				Button_cancel->setPressedActionEnabled(true);
				Button_cancel->addTouchEventListener(CC_CALLBACK_2(AuctionGoodsInfo::callBackSearch,this));
				Button_cancel->setAnchorPoint(Vec2(0.5f,0.5f));
				Button_cancel->setScale9Enabled(true);
				Button_cancel->setContentSize(Size(80,43));
				Button_cancel->setCapInsets(Rect(18,9,2,23));
				Button_cancel->setPosition(Vec2(200,25));

				auto Label_cancel= Label::createWithTTF(str_cancel, APP_FONT_NAME, 18);
				Label_cancel->setAnchorPoint(Vec2(0.5f,0.5f));
				Label_cancel->setPosition(Vec2(0,0));
				Button_cancel->addChild(Label_cancel);
				m_pLayer->addChild(Button_cancel);
			}break;
		case 1:
			{
				const char *str_recove = StringDataManager::getString("goods_auction_recove");
				const char *str_cancel = StringDataManager::getString("goods_auction_search");
				// ��� ����
				auto Button_recover= Button::create();
				Button_recover->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
				Button_recover->setTouchEnabled(true);
				Button_recover->setPressedActionEnabled(true);
				Button_recover->addTouchEventListener(CC_CALLBACK_2(AuctionGoodsInfo::callBackRecover,this));
				Button_recover->setAnchorPoint(Vec2(0.5f,0.5f));
				Button_recover->setScale9Enabled(true);
				Button_recover->setContentSize(Size(80,43));
				Button_recover->setCapInsets(Rect(18,9,2,23));
				Button_recover->setPosition(Vec2(80,25));

				auto Label_recover = Label::createWithTTF(str_recove, APP_FONT_NAME, 18);
				Label_recover->setAnchorPoint(Vec2(0.5f,0.5f));
				Label_recover->setPosition(Vec2(0,0));
				Button_recover->addChild(Label_recover);
				m_pLayer->addChild(Button_recover);

				auto Button_cancel= Button::create();
				Button_cancel->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
				Button_cancel->setTouchEnabled(true);
				Button_cancel->setPressedActionEnabled(true);
				Button_cancel->addTouchEventListener(CC_CALLBACK_2(AuctionGoodsInfo::callBackSearch, this));
				Button_cancel->setAnchorPoint(Vec2(0.5f,0.5f));
				Button_cancel->setScale9Enabled(true);
				Button_cancel->setContentSize(Size(80,43));
				Button_cancel->setCapInsets(Rect(18,9,2,23));
				Button_cancel->setPosition(Vec2(200,25));

				auto Label_cancel = Label::createWithTTF(str_cancel, APP_FONT_NAME, 18);
				Label_cancel->setAnchorPoint(Vec2(0.5f,0.5f));
				Label_cancel->setPosition(Vec2(0,0));
				Button_cancel->addChild(Label_cancel);
				m_pLayer->addChild(Button_cancel);
			}break;;

		}
		return true;
	}
	return false;
}

void AuctionGoodsInfo::onEnter()
{
	GoodsItemInfoBase::onEnter();

}

void AuctionGoodsInfo::onExit()
{
	GoodsItemInfoBase::onExit();
}

bool AuctionGoodsInfo::onTouchBegan( Touch * pTouch,Event * pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}

void AuctionGoodsInfo::callBackPurchase(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//goodsid
		auto auctionui = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
		auctionui->setopType = 5;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1803, auctionui);
		GameUtils::playGameSound(PURCHASE_SUCCESS, 2, false);

		this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void AuctionGoodsInfo::callBackRecover(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//goodsid
		auto auctionui = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
		auctionui->setopType = 3;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1803, auctionui);
		this->removeFromParentAndCleanup(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void AuctionGoodsInfo::callBackCancel(Ref *pSender, Widget::TouchEventType type)
{
	this->removeFromParentAndCleanup(true);
}

void AuctionGoodsInfo::callBackSearch(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto auctionui = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
		if (auctionui == NULL)
		{
			return;
		}

		auctionui->labelType->setString(auctionui->acutionTypevector.at(0)->typedesc().c_str());
		auctionui->setAuctiontype = auctionui->acutionTypevector.at(0)->type();
		auctionui->isSelectChat = false;
		auctionui->labelKind->setString(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->subtypedescs(0).c_str());
		auctionui->labelLevel->setString(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->leveldescs(0).c_str());
		auctionui->labelQuality->setString(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->qualitydescs(0).c_str());
		const char *strings_goldAll = StringDataManager::getString("auction_SelectMoney");
		auctionui->label_gold->setString(strings_goldAll);

		auctionui->setpageindex = 1;
		auctionui->setAuctiontype = 0;
		auctionui->setAuctionsubtype = 0;
		auctionui->setlevel = 0;
		auctionui->setMoney = 0;
		auctionui->setquality = 0;
		auctionui->setorderby = 0;
		auctionui->setorderbydesc = 0;

		auctionui->setname = m_goodsName;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1801, auctionui);
		removeFromParentAndCleanup(true);
		auctionui->callBack_btn_back(pSender, Widget::TouchEventType::ENDED);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}
