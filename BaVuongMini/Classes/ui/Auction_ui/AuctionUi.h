#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/element/CAuctionType.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class AnnexItem;
class AuctionTabView;
class PacPageView;
class RichTextInputBox;
class UITab;
typedef enum
{
	AUCTIONTABVIEW = 401,
	TAGWHEREEXLIST=402,
	AUCTIONLAYER=420,
	AUCTIONCONSIGGOODSINFONTAG = 421
};
enum
{
	PURCHASETABS=0,
	CONSIGNTABS=1
	
};

class AuctionUi:public UIScene
{
public:
	AuctionUi(void);
	~AuctionUi(void);

	static AuctionUi * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

public:
	void callBackchangebuyOrsale(Ref * obj); 

	Text* Label_selfConsignIsNull;

	void callBack_btn_buyOfqualitySort(Ref * obj, Widget::TouchEventType type);
	void callBack_btn_LvSort(Ref *pSender, Widget::TouchEventType type);
	void callBack_btn_remainTimeSort(Ref *pSender, Widget::TouchEventType type);
	void callBack_btn_priceSort(Ref *pSender, Widget::TouchEventType type);

	void callBack_btn_typeList(Ref *pSender, Widget::TouchEventType type);
	void callBack_btn_lv(Ref *pSender, Widget::TouchEventType type);
	void callBack_btn_kind(Ref *pSender, Widget::TouchEventType type);
	void callBack_btn_quality(Ref *pSender, Widget::TouchEventType type);
	void callBack_btn_gold(Ref *pSender, Widget::TouchEventType type);
	
	void callBAck_btn_find(Ref *pSender, Widget::TouchEventType type);

	void callBack_btn_EnterConsignMent(Ref *pSender, Widget::TouchEventType type);

	void callBack_btn_priceCounter(Ref *pSender, Widget::TouchEventType type);
	void callBack_btn_amountCounter(Ref *pSender, Widget::TouchEventType type);
	void callBack_Pricecounter(Ref *obj);
	void callBack_amountCounter(Ref* obj);

	CheckBox * checkbox_gold;
	CheckBox * checkbox_Money;
	void setGoldDeal(Ref* pSender, CheckBox::EventType type);
	void setMoneyDeal(Ref* pSender, CheckBox::EventType type);

	void setGoldDealCopyCheckbox(Ref *pSender, Widget::TouchEventType type);
	void setMoneyCopyCheckbox(Ref *pSender, Widget::TouchEventType type);

	void callBack_btn_back(Ref *pSender, Widget::TouchEventType type);
	void callBack_btn_consignment(Ref *pSender, Widget::TouchEventType type);
	
	void addAuctionPacInfo(Ref * obj);
	
	void getbackAuction(Ref * obj);

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	void CurrentPageViewChanged(Ref *pSender, PageView::EventType type);
	void callBack_btn_colse(Ref *pSender, Widget::TouchEventType type);

	void PageScrollToDefault();

	bool isSelectChat;
	Text * goldlabel_;
	Text * goldIngot;
	void refreshPlayerMoney();
	PacPageView *pageView;
	RichTextInputBox * checkInfoBox;
	UITab *buyOrsale;
	Layer *layer_;
	Text * handCharge;

	int curMaxConsignGoodsNum;
public:
	Layout* consignSales;
	Layout* purchase;
	Layout* puerchaseSelect;
	Layout* consignSelect;
	ImageView * iamge_;
	Text * labelPrice;
	Text * labelAmount;

	ImageView * dealMoney;
	ImageView * changeCost;
	Text * changeCostLabel;
	Text * consignAmount;

	AuctionTabView * auctionView;
	ImageView *currentPage;
public:
	Text*labelType;
	Text*labelLevel;
	Text*labelKind;
	Text*labelQuality;
	Text * label_gold;
private:
	ImageView * buyandquality;
	ImageView * image_level;
	ImageView * image_remaintime;
	ImageView * image_priceup;

	bool buyandqualityLift;
	bool image_levelLift;
	bool image_remaintimLift;
	bool image_priceupLift;

	AnnexItem *auction;
	bool isConsignGoods;
public:
	//req1801
	int setAuctiontype;
	int setAuctionsubtype;
	int setprofession;
	int setlevel;
	int setquality;
	int setMoney;
	int setcurrency;
	std::string setname;
	//const char* setname;
	int setpageindex;
	int setorderby;
	int setorderbydesc;
	////////
	bool hasMorePage;

	int successConsign;
public:
	//req 1802
	int setIndex;
	int setPrice;
	int setPropNum;
	int setPriceType;//����ж ϴ
public:
	//req 1803
	int setopType;
	int goodsid;
public:
	std::vector<CAuctionType *>acutionTypevector;
	int tabsType;
};	

