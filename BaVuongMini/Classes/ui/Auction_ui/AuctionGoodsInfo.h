

#ifndef AUCTION_AUCTIONGOODSINFO_
#define AUCTION_AUCTIONGOODSINFO_

#include "../backpackscene/GoodsItemInfoBase.h"
class AuctionUi;
class AuctionGoodsInfo:public GoodsItemInfoBase
{
public:
	AuctionGoodsInfo(void);
	~AuctionGoodsInfo(void);
	
	static AuctionGoodsInfo * create(GoodsInfo * goods,int curTab);
	
	bool init(GoodsInfo * goods,int curTab);
	virtual void onEnter();
	virtual void onExit();
	

	virtual bool onTouchBegan(Touch * pTouch,Event * pEvent);

	void callBackPurchase(Ref *pSender, Widget::TouchEventType type);
	void callBackSearch(Ref *pSender, Widget::TouchEventType type);

	void callBackRecover(Ref *pSender, Widget::TouchEventType type);
	void callBackCancel(Ref *pSender, Widget::TouchEventType type);
private:
	std::string m_goodsName;

};

#endif;