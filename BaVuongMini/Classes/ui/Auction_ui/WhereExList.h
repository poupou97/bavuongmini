#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

enum{
	TYPELIST=0,
	KINDLIST,
	LEVELLIST,
	QUALITYLIST,
	GOLDLIST
};

class WhereExList:public UIScene
{
public:
	WhereExList(void);
	~WhereExList(void);

	static WhereExList *create(int type);
	
	bool init(int type);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	
	void selectType(Ref * obj);
	void selectKind(Ref * obj);
	void selectLevel(Ref * obj);
	void selectQuality(Ref * obj);
	void seletGold(Ref * obj);
public:

};

