#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class AuctionUi;
class AuctionTabViewCell:public TableViewCell
{
public:
	AuctionTabViewCell(void);
	~AuctionTabViewCell(void);

	static AuctionTabViewCell*create(int idx);
	bool init(int idx);

	void onEnter();
	void onExit();

	void goodsPurchase(Ref * obj);
	void goodsConsign(Ref * obj);

	std::string getEquipmentQualityByIndex(int quality);
	Color3B getEquipmentColorByQuality(int quality);
private:
	int currId;
	Size winsize;
	AuctionUi * auctionui;
};

