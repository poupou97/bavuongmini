#include "AuctionUi.h"
#include "../extensions/UITab.h"
#include "../extensions/RichTextInput.h"
#include "GameView.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "WhereExList.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CAuctionInfo.h"
#include "../extensions/Counter.h"
#include "../backpackscene/PacPageView.h"
#include "AuctionTabView.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"
#include "../extensions/FloderItem.h"
#include "../../utils/StrUtils.h"

AuctionUi::AuctionUi(void)
{
	buyandqualityLift=false;
	image_levelLift=false;
	image_remaintimLift=false;
	image_priceupLift=false;

	isConsignGoods=false;
	tabsType=0;

	hasMorePage=false;
	///////
	setAuctiontype=0;
	setAuctionsubtype=0;
	setprofession=0;
	setlevel=0;
	setquality=0;
	setpageindex=1;
	setorderby=0;
	setorderbydesc=0;
	setMoney = 0;

	setPrice=0;
	setPropNum=0;
	setPriceType =1;
	//setCurrency=1;
	//success consign goods
	successConsign=0;

	isSelectChat =false;

	curMaxConsignGoodsNum = 0;
}


AuctionUi::~AuctionUi(void)
{
	std::vector<CAuctionInfo*>::iterator iter;
	for (iter=GameView::getInstance()->auctionSourceVector.begin();iter != GameView::getInstance()->auctionSourceVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->auctionSourceVector.clear();

	for(iter=GameView::getInstance()->auctionConsignVector.begin();iter!=GameView::getInstance()->auctionConsignVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->auctionConsignVector.clear();

	for(iter=GameView::getInstance()->selfConsignVector.begin();iter!=GameView::getInstance()->selfConsignVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->selfConsignVector.clear();

	std::vector<CAuctionType *>::iterator typeV;
	for (typeV =acutionTypevector.begin();typeV!= acutionTypevector.end();typeV++ )
	{
		delete * typeV;
	}
	acutionTypevector.clear();

	this->getbackAuction(NULL);
}

AuctionUi * AuctionUi::create()
{
	AuctionUi * auction=new AuctionUi();
	if (auction && auction->init())
	{
		auction->autorelease();
		return auction;
	}
	CC_SAFE_DELETE(auction);
	return NULL;
}

bool AuctionUi::init()
{
	if (UIScene::init())
	{

		Size size=Director::getInstance()->getVisibleSize();

		ImageView *mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(size);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		if(LoadSceneLayer::auctionPanel->getParent() != NULL)
		{
			LoadSceneLayer::auctionPanel->removeFromParentAndCleanup(false);
		}

		Layout * panel=LoadSceneLayer::auctionPanel;
		panel->setAnchorPoint(Vec2(0.5f,0.5f));
		panel->setPosition(Vec2(size.width/2,size.height/2));
		m_pLayer->addChild(panel);

		layer_=Layer::create();
		layer_->setIgnoreAnchorPointForPosition(false);
		layer_->setAnchorPoint(Vec2(0.5f,0.5f));
		layer_->setPosition(Vec2(size.width/2,size.height/2));
		layer_->setContentSize(Size(800,480));
		addChild(layer_,0,AUCTIONLAYER);

		const char * firstStr = StringDataManager::getString("UIName_auction_ji");
		const char * secondStr = StringDataManager::getString("UIName_auction_shou");
		const char * thirdStr = StringDataManager::getString("UIName_auction_hang");
		auto atmature = MainScene::createPendantAnm(firstStr,secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		layer_->addChild(atmature);

		const char *strings_consign = StringDataManager::getString("goods_auction_consion");
		char *consign_left=const_cast<char*>(strings_consign);

		const char *strings_buy = StringDataManager::getString("goods_auction_buy");
		char *buy_left=const_cast<char*>(strings_buy);

		const char *highLightImage="res_ui/tab_1_on.png";
		char * label[] = {buy_left,consign_left};
		//buyOrsale=UITab::createWithBMFont(2,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",label,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
		buyOrsale=UITab::createWithText(2,"res_ui/tab_1_off.png","res_ui/tab_1_on.png","",label,HORIZONTAL,5);
		buyOrsale->setPressedActionEnabled(true);
		buyOrsale->setAnchorPoint(Vec2(0.5f,0.5f));
		buyOrsale->setPosition(Vec2(70,413));
		buyOrsale->setHighLightImage((char *)highLightImage);
		buyOrsale->setDefaultPanelByIndex(0);
		buyOrsale->addIndexChangedEvent(this,coco_indexchangedselector(AuctionUi::callBackchangebuyOrsale));
		layer_->addChild(buyOrsale);

		consignSales=(Layout *)Helper::seekWidgetByName(panel,"Panel_jishou_b");
		consignSales->setVisible(false);
		purchase =(Layout *)Helper::seekWidgetByName(panel,"Panel_goumai");
		purchase->setVisible(true);

		puerchaseSelect =(Layout *)Helper::seekWidgetByName(panel,"Panel_goumai_b");
		puerchaseSelect->setVisible(true);

		consignSelect =(Layout *)Helper::seekWidgetByName(panel,"Panel_jishou_a");
		consignSelect->setVisible(false);

		auto btn_buyOfquality=(Button *)Helper::seekWidgetByName(panel,"Button_goumaipinzhi");
		btn_buyOfquality->setTouchEnabled(true);
		btn_buyOfquality->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_buyOfqualitySort, this));

		buyandquality =(ImageView *)Helper::seekWidgetByName(panel,"ImageView_buyandquality_up");
		buyandquality->loadTexture("res_ui/jt_down.png");
		
		auto btn_LvSort=(Button *)Helper::seekWidgetByName(panel,"Button_dengji");
		btn_LvSort->setTouchEnabled(true);
		btn_LvSort->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_LvSort,this));
		
		image_level =(ImageView *)Helper::seekWidgetByName(panel,"ImageView_level_up2");
		image_level->loadTexture("res_ui/jt_down.png");

		auto btn_remainTime=(Button *)Helper::seekWidgetByName(panel,"Button_shengyushijian");
		btn_remainTime->setTouchEnabled(true);
		btn_remainTime->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_remainTimeSort,this));

		image_remaintime =(ImageView *)Helper::seekWidgetByName(panel,"ImageView_remaintime_up2_Clone");
		image_remaintime->loadTexture("res_ui/jt_down.png");

		auto btn_price=(Button *)Helper::seekWidgetByName(panel,"Button_jiage");
		btn_price->setTouchEnabled(true);
		btn_price->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_priceSort,this));

		image_priceup =(ImageView *)Helper::seekWidgetByName(panel,"ImageView_price_up3");
		image_priceup->loadTexture("res_ui/jt_down.png");

		goldlabel_=(Text*)Helper::seekWidgetByName(panel,"Label_yinzi");

		goldIngot=(Text*)Helper::seekWidgetByName(panel,"Label_yuanbao");

		this->refreshPlayerMoney();
		auto btn_type =(Button *) Helper::seekWidgetByName(panel,"Button_leixing");
		btn_type->setPressedActionEnabled(true);
		btn_type->setTouchEnabled(true);
		btn_type->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_typeList,this));
		 
		const char *strings_type = StringDataManager::getString("auction_SelectType");
		labelType =(Text*)Helper::seekWidgetByName(panel,"Label_leixing");
		labelType->setString(strings_type);

		auto btn_kind =(Button *) Helper::seekWidgetByName(panel,"Button_kind");
		btn_kind->setPressedActionEnabled(true);
		btn_kind->setTouchEnabled(true);
		btn_kind->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_kind,this));

		const char *strings_kind = StringDataManager::getString("auction_SelectKind");
		labelKind =(Text*)Helper::seekWidgetByName(panel,"Label_kind");
		labelKind->setString(strings_kind);

		auto btn_Level =(Button *) Helper::seekWidgetByName(panel,"Button_level");
		btn_Level->setPressedActionEnabled(true);
		btn_Level->setTouchEnabled(true);
		btn_Level->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_lv,this));

		const char *strings_level = StringDataManager::getString("auction_SelectLevel");
		labelLevel =(Text*)Helper::seekWidgetByName(panel,"Label_level");
		labelLevel->setString(strings_level);

		auto btn_quality =(Button *) Helper::seekWidgetByName(panel,"Button_pinzhi");
		btn_quality->setPressedActionEnabled(true);
		btn_quality->setTouchEnabled(true);
		btn_quality->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_quality,this));

		const char *strings_quality = StringDataManager::getString("auction_SelectQuality");
		labelQuality =(Text*)Helper::seekWidgetByName(panel,"Label_pinzhi");
		labelQuality->setString(strings_quality);

		auto btn_gold =(Button *) Helper::seekWidgetByName(panel,"Button_gold");
		btn_gold->setPressedActionEnabled(true);
		btn_gold->setTouchEnabled(true);
		btn_gold->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_gold,this));

		const char *strings_gold = StringDataManager::getString("auction_SelectMoney");
		label_gold =(Text*)Helper::seekWidgetByName(panel,"Label_kind_0");
		label_gold->setString(strings_gold);

		checkInfoBox =new RichTextInputBox();
		checkInfoBox->setInputBoxWidth(150);
		checkInfoBox->setAnchorPoint(Vec2(0,0));
		checkInfoBox->setPosition(Vec2(575,102));
		checkInfoBox->setCharLimit(8);
		layer_->addChild(checkInfoBox);
		checkInfoBox->autorelease();
		
		auto btn_find=(Button *)Helper::seekWidgetByName(panel,"Button_chaxun");
		btn_find->setPressedActionEnabled(true);
		btn_find->setTouchEnabled(true);
		btn_find->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBAck_btn_find,this));
		
		auto btn_enterConsign = (Button *)Helper::seekWidgetByName(panel,"Button_jishouwupin");
		btn_enterConsign->setPressedActionEnabled(true);
		btn_enterConsign->setTouchEnabled(true);
		btn_enterConsign->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_EnterConsignMent,this));

		//Label * friendReminder1 =(Text*)Helper::seekWidgetByName(panel,"TextArea_wenzimore_b");
		//Label * friendReminder2 =(Text*)Helper::seekWidgetByName(panel,"TextArea_wenzimore");

		auto btn_Inputprice =(Button *)Helper::seekWidgetByName(panel,"Button_price");
		btn_Inputprice->setTouchEnabled(true);
		btn_Inputprice->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_priceCounter,this));
		auto btn_Inputamount =(Button *)Helper::seekWidgetByName(panel,"Button_3671");
		btn_Inputamount->setTouchEnabled(true);
		btn_Inputamount->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_amountCounter,this));

		dealMoney = (ImageView *)Helper::seekWidgetByName(panel,"ImageView_249_0");

		handCharge =(Text*)Helper::seekWidgetByName(panel,"Label_10liang");
		handCharge->setString("");

		changeCost = (ImageView *)Helper::seekWidgetByName(panel,"ImageView_img1");
		changeCost->loadTexture("res_ui/coins.png");

		changeCostLabel =(Text*)Helper::seekWidgetByName(panel,"Label_10liang_2");
		changeCostLabel->setString("");

		Label_selfConsignIsNull =(Text*)Helper::seekWidgetByName(panel,"Label_selfConsignIsNull");
		Label_selfConsignIsNull->setVisible(false);


		//////////////////////////////////////////////////////////////////////////
		std::map<int,std::string>::const_iterator auctionCIter;
		auctionCIter = SystemInfoConfigData::s_systemInfoList.find(6);
		if (auctionCIter == SystemInfoConfigData::s_systemInfoList.end()) // û�ҵ�����ָ�END��  
		{
		}
		else
		{
			auto auctionDes = (Text*)Helper::seekWidgetByName(panel,"TextArea_wenzimore_b");
			auctionDes->setString(SystemInfoConfigData::s_systemInfoList[6].c_str());
		}


		checkbox_gold =(ui::CheckBox *)Helper::seekWidgetByName(panel,"CheckBox_Gold");
		checkbox_gold->setTouchEnabled(true);
		checkbox_gold->addEventListener(CC_CALLBACK_2(AuctionUi::setGoldDeal,this));
		checkbox_gold->setSelected(true);

		auto image_copyCheckBox_gold = (Button *)Helper::seekWidgetByName(panel,"ImageView_249");
		image_copyCheckBox_gold->setTouchEnabled(true);
		image_copyCheckBox_gold->addTouchEventListener(CC_CALLBACK_2(AuctionUi::setGoldDealCopyCheckbox,this));

		auto label_copyCheckBox_gold = (Button *)Helper::seekWidgetByName(panel,"Label_250");
		label_copyCheckBox_gold->setTouchEnabled(true);
		label_copyCheckBox_gold->addTouchEventListener(CC_CALLBACK_2(AuctionUi::setGoldDealCopyCheckbox,this));
		
		checkbox_Money =(ui::CheckBox *)Helper::seekWidgetByName(panel,"CheckBox_yuanbao");
		checkbox_Money->setTouchEnabled(true);
		checkbox_Money->addEventListener(CC_CALLBACK_2(AuctionUi::setMoneyDeal,this));
		checkbox_Money->setSelected(false);

		auto image_copyCheckBox_money = (Button *)Helper::seekWidgetByName(panel,"ImageView_251");
		image_copyCheckBox_money->setTouchEnabled(true);
		image_copyCheckBox_money->addTouchEventListener(CC_CALLBACK_2(AuctionUi::setMoneyCopyCheckbox,this));

		auto label_copyCheckBox_money = (Button *)Helper::seekWidgetByName(panel,"Label_252");
		label_copyCheckBox_money->setTouchEnabled(true);
		label_copyCheckBox_money->addTouchEventListener(CC_CALLBACK_2(AuctionUi::setMoneyCopyCheckbox,this));

		iamge_ = (ImageView *)Helper::seekWidgetByName(panel,"ImageView_3511");
		iamge_->loadTexture("res_ui/coins.png");
		labelPrice = (Text*)Helper::seekWidgetByName(panel,"Label_Price_");
		labelPrice->setString("");

		labelAmount=(Text*)Helper::seekWidgetByName(panel,"labelamountstr_");
		labelAmount->setString("");

		auto btn_back =(Button *)Helper::seekWidgetByName(panel,"Button_fanhui");
		btn_back->setPressedActionEnabled(true);
		btn_back->setTouchEnabled(true);
		btn_back->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_back,this));

		auto btn_consignment =(Button *)Helper::seekWidgetByName(panel,"Button_jishou");
		btn_consignment->setPressedActionEnabled(true);
		btn_consignment->setTouchEnabled(true);
		btn_consignment->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_consignment,this));

		currentPage = (ImageView *)Helper::seekWidgetByName(panel,"ImageView_dianOn");
		/////////backpage
		pageView = GameView::getInstance()->pacPageView;
		pageView->setAnchorPoint(Vec2(0,0));
		pageView->setPosition(Vec2(393,96));
		pageView->getPageView()->addEventListener((PageView::ccPageViewCallback)CC_CALLBACK_2(AuctionUi::CurrentPageViewChanged, this));
		layer_->addChild(pageView);
		pageView->setCurUITag(kTagAuction);
		pageView->setVisible(false);
		auto seq = Sequence::create(DelayTime::create(0.05f),CallFunc::create(CC_CALLBACK_0(AuctionUi::PageScrollToDefault, this)),NULL);
		pageView->runAction(seq);
		//CurrentPageViewChanged(pageView);
		pageView->checkCDOnBegan();

		int consignBeginNum=GameView::getInstance()->auctionConsignVector.size();
		consignAmount= (Text*)Helper::seekWidgetByName(panel,"Label_date");

		char consignSecond[10];
		sprintf(consignSecond,"%d",consignBeginNum);
		char consignMaxGoodsNum[10];
		sprintf(consignMaxGoodsNum,"%d",curMaxConsignGoodsNum);
		
		std::string consignBegin="";
		consignBegin.append(consignSecond);
		consignBegin.append("/");
		consignBegin.append(consignMaxGoodsNum);
		consignAmount->setString(consignBegin.c_str());

		auto btn_close =(Button *)Helper::seekWidgetByName(panel,"Button_close");
		btn_close->setPressedActionEnabled(true);
		btn_close->setTouchEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(AuctionUi::callBack_btn_colse,this));

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(size);
		return true;
	}
	return false;
}

void AuctionUi::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	
	auctionView=AuctionTabView::create();
	auctionView->setIgnoreAnchorPointForPosition(false);
	auctionView->setAnchorPoint(Vec2(0,0));
	auctionView->setPosition(Vec2(74,50));
	layer_->addChild(auctionView,1,AUCTIONTABVIEW);
	auctionView->init();
	
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1804,this);

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1805,this);
}

void AuctionUi::onExit()
{
	UIScene::onExit();
}

void AuctionUi::callBackchangebuyOrsale( Ref * obj )
{
	auto tab =(UITab *)obj;
	int num= tab->getCurrentIndex();

	std::vector<CAuctionInfo *>::iterator iterSource;
	for (iterSource=GameView::getInstance()->auctionSourceVector.begin();iterSource!=GameView::getInstance()->auctionSourceVector.end();iterSource++)
	{
		delete *iterSource;
	}
	GameView::getInstance()->auctionSourceVector.clear();
	switch (num)
	{
	case PURCHASETABS:
		{
			tabsType=0;

			consignSales->setVisible(false);
			purchase->setVisible(true);
			puerchaseSelect->setVisible(true);
			consignSelect->setVisible(false);
			pageView ->setVisible(false);
			auctionView->setVisible(true);
			checkInfoBox->setVisible(true);
			for(int i=0;i<GameView::getInstance()->auctionConsignVector.size();i++)
			{
				auto auction=new CAuctionInfo();
				auction->CopyFrom(*GameView::getInstance()->auctionConsignVector.at(i));
				GameView::getInstance()->auctionSourceVector.push_back(auction);
			}
			this->getbackAuction(NULL);
		}break;
	case CONSIGNTABS:
		{
			tabsType=1;
			consignSales->setVisible(false);
			purchase->setVisible(true);
			puerchaseSelect->setVisible(false);
			consignSelect->setVisible(true);
			pageView ->setVisible(false);
			auctionView->setVisible(true);
			checkInfoBox->setVisible(false);
			for(int i=0;i<GameView::getInstance()->selfConsignVector.size();i++)
			{
				auto auction=new CAuctionInfo();
				auction->CopyFrom(*GameView::getInstance()->selfConsignVector.at(i));
				GameView::getInstance()->auctionSourceVector.push_back(auction);
			}
			this->getbackAuction(NULL);
		}break;

	}

	int consignNum=GameView::getInstance()->selfConsignVector.size();
	std::string consignBegin="";
	char consignSecond[10];
	sprintf(consignSecond,"%d",consignNum);
	char consignMaxGoodsNum[10];
	sprintf(consignMaxGoodsNum,"%d",curMaxConsignGoodsNum);
	consignBegin.append(consignSecond);
	consignBegin.append("/");
	consignBegin.append(consignMaxGoodsNum);
	consignAmount->setString(consignBegin.c_str());
	
	TableView * tabview= (TableView *)auctionView->getChildByTag(AUCTIONCCTABVIEW);
	tabview->reloadData();
}

void AuctionUi::callBack_btn_typeList(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn_ = (Button *)pSender;

		auto whereex = WhereExList::create(0);
		whereex->setAnchorPoint(Vec2(1, 1));
		whereex->setPosition(Vec2(btn_->getPosition().x - btn_->getContentSize().width - 155, layer_->getContentSize().height / 2 - 100));
		layer_->addChild(whereex, 2, TAGWHEREEXLIST);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void AuctionUi::callBack_btn_kind(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn_ = (Button *)pSender;

		auto whereex = WhereExList::create(1);
		whereex->setAnchorPoint(Vec2(1, 1));
		if (whereex->getContentSize().height > 280)
		{
			whereex->setPosition(Vec2(btn_->getPosition().x - btn_->getContentSize().width - 155, layer_->getContentSize().height / 2 - 100));
		}
		else
		{
			whereex->setPosition(Vec2(btn_->getPosition().x - btn_->getContentSize().width - 155, layer_->getContentSize().height / 2 - 100));
		}

		layer_->addChild(whereex, 2, TAGWHEREEXLIST);

		/*
		if (isSelectChat==true)
		{
		auto whereex =WhereExList::create(1);
		whereex->setAnchorPoint(Vec2(1,1));
		if (whereex->getContentSize().height > 280)
		{
		whereex->setPosition(Vec2(btn_->getPosition().x-btn_->getContentSize().width-115,layer_->getContentSize().height/2 - 110));
		}else
		{
		whereex->setPosition(Vec2(btn_->getPosition().x-btn_->getContentSize().width-115,layer_->getContentSize().height/2 - 110));
		}

		layer_->addChild(whereex,2,TAGWHEREEXLIST);
		}else
		{
		const char *strings_ = StringDataManager::getString("auction_type");
		GameView::getInstance()->showAlertDialog(strings_);
		}
		*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void AuctionUi::callBack_btn_lv(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn_ = (Button *)pSender;

		auto whereex = WhereExList::create(2);
		whereex->setAnchorPoint(Vec2(1, 1));
		if (whereex->getContentSize().height >280)
		{
			whereex->setPosition(Vec2(btn_->getPosition().x - btn_->getContentSize().width - 155, layer_->getContentSize().height / 2 - 100));
		}
		else
		{
			whereex->setPosition(Vec2(btn_->getPosition().x - btn_->getContentSize().width - 155, layer_->getContentSize().height / 2 - 100));
		}

		layer_->addChild(whereex, 2, TAGWHEREEXLIST);

		/*
		if (isSelectChat==true)
		{
		auto whereex =WhereExList::create(2);
		whereex->setAnchorPoint(Vec2(1,1));
		if (whereex->getContentSize().height >280 )
		{
		whereex->setPosition(Vec2(btn_->getPosition().x-btn_->getContentSize().width-205,layer_->getContentSize().height/2 - 110));
		}else
		{
		whereex->setPosition(Vec2(btn_->getPosition().x-btn_->getContentSize().width-205,layer_->getContentSize().height/2 - 110));
		}

		layer_->addChild(whereex,2,TAGWHEREEXLIST);
		}else
		{
		const char *strings_ = StringDataManager::getString("auction_type");
		GameView::getInstance()->showAlertDialog(strings_);
		}
		*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void AuctionUi::callBack_btn_quality(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn_ = (Button *)pSender;

		auto whereex = WhereExList::create(3);
		whereex->setAnchorPoint(Vec2(1, 1));
		whereex->setPosition(Vec2(btn_->getPosition().x - btn_->getContentSize().width - 155, layer_->getContentSize().height / 2 - 100));
		layer_->addChild(whereex, 2, TAGWHEREEXLIST);

		/*
		if (isSelectChat==true)
		{
		auto whereex =WhereExList::create(3);
		whereex->setAnchorPoint(Vec2(1,1));
		whereex->setPosition(Vec2(btn_->getPosition().x-btn_->getContentSize().width-205,layer_->getContentSize().height/2 - 110));
		layer_->addChild(whereex,2,TAGWHEREEXLIST);
		}else
		{
		const char *strings_ = StringDataManager::getString("auction_type");
		GameView::getInstance()->showAlertDialog(strings_);
		}
		*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void AuctionUi::callBack_btn_gold(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn_ = (Button *)pSender;
		auto whereex = WhereExList::create(4);
		whereex->setAnchorPoint(Vec2(1, 1));
		whereex->setPosition(Vec2(btn_->getPosition().x - btn_->getContentSize().width - 155, layer_->getContentSize().height / 2 - 100));
		layer_->addChild(whereex, 2, TAGWHEREEXLIST);

		/*
		if (isSelectChat==true)
		{
		auto whereex =WhereExList::create(4);
		whereex->setAnchorPoint(Vec2(1,1));
		whereex->setPosition(Vec2(btn_->getPosition().x-btn_->getContentSize().width-115,layer_->getContentSize().height/2 - 110));
		layer_->addChild(whereex,2,TAGWHEREEXLIST);
		}else
		{
		const char *strings_ = StringDataManager::getString("auction_type");
		GameView::getInstance()->showAlertDialog(strings_);
		}
		*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void AuctionUi::callBAck_btn_find(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		const char* chat = checkInfoBox->getInputString();
		if (chat == NULL)
		{
			//const char *strings_ = StringDataManager::getString("auction_search");
			//GameView::getInstance()->showAlertDialog(strings_);
			//return;
			setname = "";
		}
		else
		{
			setname = checkInfoBox->getInputString();
		}

		std::vector<CAuctionInfo *>::iterator iter;
		for (iter = GameView::getInstance()->auctionSourceVector.begin(); iter != GameView::getInstance()->auctionSourceVector.end(); iter++)
		{
			delete *iter;
		}
		GameView::getInstance()->auctionSourceVector.clear();

		setpageindex = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1801, this);
		//checkInfoBox->deleteAllInputString();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void AuctionUi::callBack_btn_EnterConsignMent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		consignSales->setVisible(true);
		purchase->setVisible(false);
		puerchaseSelect->setVisible(false);
		consignSelect->setVisible(false);

		pageView->setVisible(true);
		auctionView->setVisible(false);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void AuctionUi::callBack_btn_consignment(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (isConsignGoods == false)
		{
			const char *str = StringDataManager::getString("auction_selectConsignGoods");
			GameView::getInstance()->showAlertDialog(str);
			return;
		}

		if (setPropNum <= 0)
		{
			const char *str = StringDataManager::getString("auction_conNum");
			GameView::getInstance()->showAlertDialog(str);
			return;
		}
		if (setPrice <= 0)
		{
			const char *str = StringDataManager::getString("auction_conPrice");
			GameView::getInstance()->showAlertDialog(str);
			return;
		}

		//setIndex = pageView->curPackageItemIndex;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1802, this);
		GameUtils::playGameSound(AUCTION_SUCCESS, 2, false);
		this->getbackAuction(NULL);
		// 	labelAmount->setText("");
		// 	labelPrice->setText("");
		// 	handCharge->setText("");
		// 	isConsignGoods=false;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}


void AuctionUi::callBack_btn_back(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		std::vector<CAuctionInfo *>::iterator iterSource;
		for (iterSource = GameView::getInstance()->auctionSourceVector.begin(); iterSource != GameView::getInstance()->auctionSourceVector.end(); iterSource++)
		{
			delete *iterSource;
		}
		GameView::getInstance()->auctionSourceVector.clear();

		tabsType = 0;
		consignSales->setVisible(false);
		purchase->setVisible(true);
		puerchaseSelect->setVisible(true);
		consignSelect->setVisible(false);
		pageView->setVisible(false);
		auctionView->setVisible(true);
		checkInfoBox->setVisible(true);

		for (int i = 0; i<GameView::getInstance()->selfConsignVector.size(); i++)
		{
			auto auction = new CAuctionInfo();
			auction->CopyFrom(*GameView::getInstance()->selfConsignVector.at(i));
			GameView::getInstance()->auctionSourceVector.push_back(auction);
		}
		auctionView->tabelView->reloadData();

		this->getbackAuction(NULL);
		buyOrsale->setDefaultPanelByIndex(0);
		int consignNum = GameView::getInstance()->selfConsignVector.size();
		std::string consignBegin = "";
		char consignSecond[10];
		sprintf(consignSecond, "%d", consignNum);
		char consignMaxGoodsNum[10];
		sprintf(consignMaxGoodsNum, "%d", curMaxConsignGoodsNum);
		consignBegin.append(consignSecond);
		consignBegin.append("/");
		consignBegin.append(consignMaxGoodsNum);
		consignAmount->setString(consignBegin.c_str());
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void AuctionUi::callBack_btn_colse(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

bool compareGoodsOfQualityUp(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods1->goods().quality() < auctionGoods2->goods().quality();
}

bool compareGoodsOfQualityDown(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods2->goods().quality() < auctionGoods1->goods().quality();
}

void AuctionUi::callBack_btn_buyOfqualitySort(Ref * obj, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (buyandqualityLift == true)
		{
			//down
			setorderby = 1;
			setorderbydesc = 1;
			buyandquality->loadTexture("res_ui/jt_down.png");
			buyandqualityLift = false;
			sort(GameView::getInstance()->auctionSourceVector.begin(), GameView::getInstance()->auctionSourceVector.end(), compareGoodsOfQualityDown);
		}
		else
		{
			//up
			setorderby = 1;
			setorderbydesc = 0;
			buyandquality->loadTexture("res_ui/jt_up.png");
			buyandqualityLift = true;
			sort(GameView::getInstance()->auctionSourceVector.begin(), GameView::getInstance()->auctionSourceVector.end(), compareGoodsOfQualityUp);
		}
		auctionView->tabelView->reloadData();
		/*
		std::vector<CAuctionInfo *>::iterator iter;
		for (iter=GameView::getInstance()->auctionSourceVector.begin();iter!=GameView::getInstance()->auctionSourceVector.end();iter++)
		{
		delete *iter;
		}

		for(iter=GameView::getInstance()->auctionConsignVector.begin();iter!=GameView::getInstance()->auctionConsignVector.end();iter++)
		{
		delete *iter;
		}

		GameView::getInstance()->auctionConsignVector.clear();
		GameView::getInstance()->auctionSourceVector.clear();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,this);
		*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

bool compareGoodsOfUseLevelUp(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods1->goods().uselevel() < auctionGoods2->goods().uselevel();
}

bool compareGoodsOfUseLevelDown(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods2->goods().uselevel() < auctionGoods1->goods().uselevel();
}

void AuctionUi::callBack_btn_LvSort(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (image_levelLift == true)
		{
			//down
			setorderby = 2;
			setorderbydesc = 1;
			image_level->loadTexture("res_ui/jt_down.png");
			image_levelLift = false;
			sort(GameView::getInstance()->auctionSourceVector.begin(), GameView::getInstance()->auctionSourceVector.end(), compareGoodsOfUseLevelDown);
		}
		else
		{
			//up
			setorderby = 2;
			setorderbydesc = 0;
			image_level->loadTexture("res_ui/jt_up.png");
			image_levelLift = true;
			sort(GameView::getInstance()->auctionSourceVector.begin(), GameView::getInstance()->auctionSourceVector.end(), compareGoodsOfUseLevelUp);
		}
		auctionView->tabelView->reloadData();
		/*
		std::vector<CAuctionInfo *>::iterator iter;
		for (iter=GameView::getInstance()->auctionSourceVector.begin();iter!=GameView::getInstance()->auctionSourceVector.end();iter++)
		{
		delete *iter;
		}
		for(iter=GameView::getInstance()->auctionConsignVector.begin();iter!=GameView::getInstance()->auctionConsignVector.end();iter++)
		{
		delete *iter;
		}

		GameView::getInstance()->auctionConsignVector.clear();
		GameView::getInstance()->auctionSourceVector.clear();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,this);
		*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

bool compareGoodsOfRemTimeUp(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods1->period() < auctionGoods2->period();
}

bool compareGoodsOfRemTimeDown(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods2->period() < auctionGoods1->period();
}

void AuctionUi::callBack_btn_remainTimeSort(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (image_remaintimLift == true)
		{
			//down
			setorderby = 3;
			setorderbydesc = 1;
			image_remaintime->loadTexture("res_ui/jt_down.png");
			image_remaintimLift = false;
			sort(GameView::getInstance()->auctionSourceVector.begin(), GameView::getInstance()->auctionSourceVector.end(), compareGoodsOfRemTimeDown);
		}
		else
		{
			//up
			setorderby = 3;
			setorderbydesc = 0;
			image_remaintime->loadTexture("res_ui/jt_up.png");
			image_remaintimLift = true;
			sort(GameView::getInstance()->auctionSourceVector.begin(), GameView::getInstance()->auctionSourceVector.end(), compareGoodsOfRemTimeUp);
		}
		auctionView->tabelView->reloadData();
		/*
		std::vector<CAuctionInfo *>::iterator iter;
		for (iter=GameView::getInstance()->auctionSourceVector.begin();iter!=GameView::getInstance()->auctionSourceVector.end();iter++)
		{
		delete *iter;
		}

		for(iter=GameView::getInstance()->auctionConsignVector.begin();iter!=GameView::getInstance()->auctionConsignVector.end();iter++)
		{
		delete *iter;
		}

		GameView::getInstance()->auctionConsignVector.clear();
		GameView::getInstance()->auctionSourceVector.clear();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,this);
		*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

bool compareGoodsOfPriceUp(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods1->price() < auctionGoods2->price();
}

bool compareGoodsOfPriceDown(CAuctionInfo * auctionGoods1,CAuctionInfo *auctionGoods2)
{
	return auctionGoods2->price() < auctionGoods1->price();
}

void AuctionUi::callBack_btn_priceSort(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (image_priceupLift == true)
		{
			//down
			setorderby = 4;
			setorderbydesc = 1;
			image_priceup->loadTexture("res_ui/jt_down.png");
			image_priceupLift = false;
			sort(GameView::getInstance()->auctionSourceVector.begin(), GameView::getInstance()->auctionSourceVector.end(), compareGoodsOfPriceDown);
		}
		else
		{
			//up
			setorderby = 4;
			setorderbydesc = 0;
			image_priceup->loadTexture("res_ui/jt_up.png");
			image_priceupLift = true;
			sort(GameView::getInstance()->auctionSourceVector.begin(), GameView::getInstance()->auctionSourceVector.end(), compareGoodsOfPriceUp);
		}

		auctionView->tabelView->reloadData();
		/*
		std::vector<CAuctionInfo *>::iterator iter;
		for (iter=GameView::getInstance()->auctionSourceVector.begin();iter!=GameView::getInstance()->auctionSourceVector.end();iter++)
		{
		delete *iter;
		}

		for(iter=GameView::getInstance()->auctionConsignVector.begin();iter!=GameView::getInstance()->auctionConsignVector.end();iter++)
		{
		delete *iter;
		}

		GameView::getInstance()->auctionConsignVector.clear();
		GameView::getInstance()->auctionSourceVector.clear();
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,this);
		*/
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void AuctionUi::callBack_btn_priceCounter(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (isConsignGoods == true)
		{
			GameView::getInstance()->showCounter(this, callfuncO_selector(AuctionUi::callBack_Pricecounter));
		}
		else
		{
			const char *strings_ = StringDataManager::getString("auction_selectConsignGoods");
			GameView::getInstance()->showAlertDialog(strings_);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


	
}

void AuctionUi::callBack_btn_amountCounter(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (isConsignGoods == true)
		{
			GameView::getInstance()->showCounter(this, callfuncO_selector(AuctionUi::callBack_amountCounter), setPropNum);
		}
		else
		{
			const char *strings_ = StringDataManager::getString("auction_selectConsignGoods");
			GameView::getInstance()->showAlertDialog(strings_);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void AuctionUi::callBack_Pricecounter( Ref *obj )
{
	auto priceCounter =(Counter *)obj;
	setPrice= priceCounter->getInputNum();
	char labelNum[10];
	sprintf(labelNum,"%d",setPrice);
	labelPrice->setString(labelNum);
	int changeCost_ = setPrice * 0.05;
	char changeCostValue[10];
	sprintf(changeCostValue,"%d",changeCost_);
	changeCostLabel->setString(changeCostValue);
}

void AuctionUi::callBack_amountCounter( Ref* obj )
{
	Counter * priceCounter =(Counter *)obj;
	setPropNum= priceCounter->getInputNum();

	if (setPropNum <= 0)
	{
		return;
	}

	if (priceCounter->getInputNum() > GameView::getInstance()->AllPacItem.at(setIndex)->quantity() )
	{
		setPropNum = GameView::getInstance()->AllPacItem.at(setIndex)->quantity();
	}
	char labelNum[15];
	sprintf(labelNum,"%d",setPropNum);
	labelAmount->setString(labelNum);
	if (setPropNum>0)
	{
		float type_ = 1;
		int goodsType_ = GameView::getInstance()->AllPacItem.at(setIndex)->goods().clazz();
		std::map<int,std::string>::const_iterator cIter;
		cIter = VipValueConfig::s_vipValue.find(17);
		if (cIter == VipValueConfig::s_vipValue.end())
		{

		}
		else
		{
			std::string value_ = VipValueConfig::s_vipValue[17];
			std::vector<std::string > stringVector_ = StrUtils::split(value_,";");
			for (int i = 0;i<stringVector_.size();i++)
			{
				std::vector<std::string > stringVector_2 = StrUtils::split(stringVector_.at(i),":");
				char typeStr[10];
				sprintf(typeStr,"%d",goodsType_);
				if (strcmp(stringVector_2.at(0).c_str(),typeStr) == 0)
				{
					std::string str_ = stringVector_2.at(1);
					type_ = std::atof(str_.c_str());    
				}
			}
		}
		int qulity = GameView::getInstance()->AllPacItem.at(setIndex)->goods().quality();
		int levell_ =GameView::getInstance()->AllPacItem.at(setIndex)->goods().uselevel();

		int price_1 = (qulity *10 + levell_*2)*type_*setPropNum;
		float price_2 = (qulity *10 + levell_*2)*type_*setPropNum;

		if (price_2 > price_1)
		{
			price_1+=1;
		}

		char priceStr_[15];
		sprintf(priceStr_,"%d",price_1);
		handCharge->setString(priceStr_);

		int goodsCount_ = GameView::getInstance()->AllPacItem.at(setIndex)->quantity();
		char goodsUseNum[50];
		sprintf(goodsUseNum,"%d",setPropNum);
		char goodsAllNum[50];
		sprintf(goodsAllNum,"%d",goodsCount_);
		std::string goodsUseAndCount = goodsUseNum;
		goodsUseAndCount.append("/");
		goodsUseAndCount.append(goodsAllNum);
		GameView::getInstance()->pacPageView->SetCurFolderGray(true,setIndex,goodsUseAndCount);
	}
}

bool AuctionUi::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void AuctionUi::CurrentPageViewChanged(Ref *pSender, PageView::EventType type)
{

	if (type == PageView::EventType::TURNING) {
		auto pageView = dynamic_cast<PageView*>(pSender);
		switch (static_cast<int>(pageView->getCurrentPageIndex()))
		{
		case 0:
			currentPage->setPosition(Vec2(520, 66));
			break;
		case 1:
			currentPage->setPosition(Vec2(542, 66));
			break;
		case 2:
			currentPage->setPosition(Vec2(562, 66));
			break;
		case 3:
			currentPage->setPosition(Vec2(583, 66));
			break;
		case 4:
			currentPage->setPosition(Vec2(604, 66));
			break;
		}
	}
}

void AuctionUi::addAuctionPacInfo( Ref * obj )
{
	if (isConsignGoods==false)
	{
		if (successConsign > curMaxConsignGoodsNum)
		{
			const char *strings_ = StringDataManager::getString("auction_curConsignMaxCount");
			GameView::getInstance()->showAlertDialog(strings_);
			return;
		}

		FolderInfo * temp_folder_ = new FolderInfo();
		temp_folder_->CopyFrom(*GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex));
		FloderItemInstance * auction_Item = FloderItemInstance::create(temp_folder_,FloderItemInstance::ktype_auction);
		auction_Item->setIgnoreAnchorPointForPosition(false);
		auction_Item->setAnchorPoint(Vec2(0,0));
		auction_Item->setPosition(Vec2(216.8f,335.5f));
		auction_Item->setTag(AUCTIONCONSIGGOODSINFONTAG);
		layer_->addChild(auction_Item);
		delete temp_folder_;


		if (GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->has_goods())
		{
			int goodsCount_ = GameView::getInstance()->AllPacItem.at(pageView->curPackageItemIndex)->quantity();
			char goodsUseNum[50];
			sprintf(goodsUseNum,"%d",goodsCount_);
			char goodsAllNum[50];
			sprintf(goodsAllNum,"%d",goodsCount_);
			std::string goodsUseAndCount = goodsUseNum;
			goodsUseAndCount.append("/");
			goodsUseAndCount.append(goodsAllNum);

			GameView::getInstance()->pacPageView->SetCurFolderGray(true,pageView->curPackageItemIndex,goodsUseAndCount);
		}
		isConsignGoods=true;
		setIndex = pageView->curPackageItemIndex;

		int goodsNum_ = GameView::getInstance()->AllPacItem.at(setIndex)->quantity();
		char labelNum[10];
		sprintf(labelNum,"%d",goodsNum_);
		labelAmount->setString(labelNum);
		
		setPropNum = goodsNum_;
		if (goodsNum_>0)
		{
			float type_ = 1;
			int goodsType_ = GameView::getInstance()->AllPacItem.at(setIndex)->goods().clazz();
			std::map<int,std::string>::const_iterator cIter;
			cIter = VipValueConfig::s_vipValue.find(17);
			if (cIter == VipValueConfig::s_vipValue.end()) // �û�ҵ�����ָ�END��  
			{

			}
			else
			{
				std::string value_ = VipValueConfig::s_vipValue[17];
				std::vector<std::string > stringVector_ = StrUtils::split(value_,";");
				for (int i = 0;i<stringVector_.size();i++)
				{
					std::vector<std::string > stringVector_2 = StrUtils::split(stringVector_.at(i),":");
					char typeStr[10];
					sprintf(typeStr,"%d",goodsType_);
					if (strcmp(stringVector_2.at(0).c_str(),typeStr) == 0)
					{
						std::string str_ = stringVector_2.at(1);
						type_ = std::atof(str_.c_str());    
					}
				}
			}
			int qulity =GameView::getInstance()->AllPacItem.at(setIndex)->goods().quality();
			int levell_ =GameView::getInstance()->AllPacItem.at(setIndex)->goods().uselevel();
			int price_1 = (qulity *10 + levell_*2)*type_*setPropNum;
			float price_2 = (qulity *10 + levell_*2)*type_*setPropNum;

			if (price_2 > price_1)
			{
				price_1+=1;
			}

			char priceStr_[10];
			sprintf(priceStr_,"%d",price_1);
			handCharge->setString(priceStr_);
		}

	}else
	{
		const char *strings_ = StringDataManager::getString("auction_consignNum");
		GameView::getInstance()->showAlertDialog(strings_);
	}

}

void AuctionUi::getbackAuction( Ref * obj )
{
	isConsignGoods=false;
	handCharge->setString("");
	labelAmount->setString("");
	labelPrice->setString("");
	setPropNum = 0;
	setPrice = 0;
	changeCostLabel->setString("");
	FloderItemInstance * auctionGoodsInfo =(FloderItemInstance*)layer_->getChildByTag(AUCTIONCONSIGGOODSINFONTAG);
	if (auctionGoodsInfo != NULL)
	{
		auctionGoodsInfo->removeFromParentAndCleanup(true);
		
		if (GameView::getInstance()->AllPacItem.at(setIndex)->has_goods())
		{
			GameView::getInstance()->pacPageView->SetCurFolderGray(false,setIndex);
		}
	}
}

void AuctionUi::setGoldDeal(Ref* pSender, CheckBox::EventType type)
{
	auto checkbox =(ui::CheckBox *)pSender;

	checkbox_gold->setSelected(true);
	checkbox_Money->setSelected(false);
	setPriceType = 1;

	changeCost->loadTexture("res_ui/coins.png");
	iamge_->loadTexture("res_ui/coins.png");
}

void AuctionUi::setGoldDealCopyCheckbox(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		checkbox_gold->setSelected(true);
		checkbox_Money->setSelected(false);
		setPriceType = 1;

		changeCost->loadTexture("res_ui/coins.png");
		iamge_->loadTexture("res_ui/coins.png");
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void AuctionUi::setMoneyCopyCheckbox(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		checkbox_Money->setSelected(true);
		checkbox_gold->setSelected(false);
		setPriceType = 2;

		changeCost->loadTexture("res_ui/ingot.png");
		iamge_->loadTexture("res_ui/ingot.png");
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void AuctionUi::setMoneyDeal(Ref* pSender, CheckBox::EventType type)
{
	auto checkbox =(ui::CheckBox *)pSender;
	checkbox_Money->setSelected(true);
	checkbox_gold->setSelected(false);
	setPriceType = 2;

	changeCost->loadTexture("res_ui/ingot.png");
	iamge_->loadTexture("res_ui/ingot.png");
}

void AuctionUi::refreshPlayerMoney()
{
	int m_goldValue = GameView::getInstance()->getPlayerGold();
	char GoldStr_[20];
	sprintf(GoldStr_,"%d",m_goldValue);
	goldlabel_->setString(GoldStr_);

	int m_goldIngot =  GameView::getInstance()->getPlayerGoldIngot();
	char GoldIngotStr_[20];
	sprintf(GoldIngotStr_,"%d",m_goldIngot);
	goldIngot->setString(GoldIngotStr_);
}

void AuctionUi::PageScrollToDefault()
{
	pageView->getPageView()->scrollToPage(0);
	CurrentPageViewChanged(pageView, PageView::EventType::TURNING);
}








