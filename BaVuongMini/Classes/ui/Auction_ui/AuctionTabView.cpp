#include "AuctionTabView.h"
#include "../extensions/CCMoveableMenu.h"
#include "AuctionUi.h"
#include "GameView.h"
#include "AuctionTabViewCell.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CAuctionInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "AuctionGoodsInfo.h"
#include "../../ui/extensions/RichTextInput.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../../utils/StrUtils.h"

#define  SelectImageTag 458

AuctionTabView::AuctionTabView(void)
{
	selectGoodsId=0;
}


AuctionTabView::~AuctionTabView(void)
{
}

AuctionTabView * AuctionTabView::create()
{
	auto auctionTabview =new AuctionTabView();
	auctionTabview->autorelease();
	return auctionTabview;
}

bool AuctionTabView::init()
{
	if (UIScene::init())
	{

		tabelView =TableView::create(this,Size(465,320));
		//tabelView->setSelectedEnable(true);
		//tabelView->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 24, 1, 1), Vec2(0,0));
		//tabelView->setPressedActionEnabled(true);
		tabelView->setDirection(TableView::Direction::VERTICAL);
		tabelView->setAnchorPoint(Vec2(0,0));
		tabelView->setPosition(Vec2(0,0));
		tabelView->setDelegate(this);
		tabelView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(tabelView,1,AUCTIONCCTABVIEW);
		tabelView->reloadData();

		this->setContentSize(Size(465,320));
		return true;
	}
	return false;
}

void AuctionTabView::onEnter()
{
	UIScene::onEnter();
}

void AuctionTabView::onExit()
{
	UIScene::onExit();
}

void AuctionTabView::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void AuctionTabView::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void AuctionTabView::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void AuctionTabView::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	CCLOG("cell->getIdx() = %d",cell->getIdx());
	selectGoodsId=cell->getIdx();
	auto auctionui =(AuctionUi *)this->getParent()->getParent();
	auctionui->goodsid = GameView::getInstance()->auctionSourceVector.at(selectGoodsId)->id();
	
	auto goods_ =new GoodsInfo();
	goods_->CopyFrom(GameView::getInstance()->auctionSourceVector.at(selectGoodsId)->goods());  

	auto purchase_ =AuctionGoodsInfo::create(goods_,auctionui->tabsType);
	purchase_->setIgnoreAnchorPointForPosition(false);
	purchase_->setAnchorPoint(Vec2(0.5f,0.5f));
	//purchase_->setPosition(Vec2(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(purchase_);

	delete goods_;
}

cocos2d::Size AuctionTabView::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(465,70);
}

cocos2d::extension::TableViewCell* AuctionTabView::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	__String * string =__String::createWithFormat("%d",idx);	
	auto cell =(AuctionTabViewCell *)table->dequeueCell();

	cell=AuctionTabViewCell::create(idx);

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	auto auction = (AuctionUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	if (idx == GameView::getInstance()->auctionSourceVector.size() - 3 && auction->tabsType == 0)
	{
		if (auction->hasMorePage == true )
		{
			auction->setpageindex++;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1801,auction);
		}
	}
	return cell;
}

ssize_t AuctionTabView::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	int num = GameView::getInstance()->auctionSourceVector.size();
	auto auctionUi_ = (AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	if (auctionUi_->tabsType == 1 && num <= 0)
	{
		auctionUi_->Label_selfConsignIsNull->setVisible(true);
	}else
	{
		auctionUi_->Label_selfConsignIsNull->setVisible(false);
	}
	auto rl_des = (CCRichLabel *)auctionUi_->layer_->getChildByTag(123465);
	if (rl_des!= NULL)
	{
		rl_des->removeFromParentAndCleanup(true);
	}

	if (auctionUi_->tabsType == 0 && num <= 0)
	{
		std::string infoStr_ = auctionUi_->setname;
		if (strcmp(infoStr_.c_str(),"") !=0)
		{
			const char * str1 = StringDataManager::getString("auction_checkIsNull_begin");
			std::string str3 = StrUtils::applyColor(infoStr_.c_str(),Color3B(255,0,0));
			const char * str2 = StringDataManager::getString("auction_checkIsNull_end");

			std::string temp_ = str1;
			temp_.append(str3.c_str());
			temp_.append(str2);

			RichLabelDefinition def;
			def.strokeEnabled = false;
			
			rl_des = CCRichLabel::create(temp_.c_str(),Size(465,20),def);
			rl_des->setAnchorPoint(Vec2(0.5f,0));
			rl_des->setPosition(Vec2(306,210));
			rl_des->setTag(123465);
			auctionUi_->layer_->addChild(rl_des);
		}
	}

	return num;
}

void AuctionTabView::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void AuctionTabView::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}
void AuctionTabView::onTouchCancelled( Touch *pTouch, Event *pEvent )
{
}

void AuctionTabView::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{
}

void AuctionTabView::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}


