#include "WhereExList.h"
#include "../extensions/UITab.h"
#include "../Auction_ui/AuctionUi.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"

#define UITAB_VERTICAL_INTERVAL_SIZE (-2)

WhereExList::WhereExList(void)
{
}


WhereExList::~WhereExList(void)
{
}

WhereExList * WhereExList::create(int type)
{
	WhereExList * whereex=new WhereExList();
	if (whereex && whereex->init(type))
	{
		whereex->autorelease();
		return whereex;
	}
	CC_SAFE_DELETE(whereex);
	return NULL;

}

bool WhereExList::init( int type )
{
	if (UIScene::init())
	{
		auto background = ImageView::create();
		background->loadTexture("res_ui/dikuang_1.png");
		background->setScale9Enabled(true);
		background->setContentSize(Size(110,260));
		background->setAnchorPoint(Vec2(0,0));
		background->setPosition(Vec2(0,0));
		m_pLayer->addChild(background);

		auto scrollList = ui::ScrollView::create();
		scrollList->setTouchEnabled(true);
		scrollList->setDirection(ui::ScrollView::Direction::VERTICAL);
		scrollList->setContentSize(Size(110,240));
		scrollList->setPosition(Vec2(0,10));
		scrollList->setBounceEnabled(true);
		m_pLayer->addChild(scrollList);
		
		auto auction=(AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
		switch (type)
		{
		case TYPELIST:
			{
				int num= auction->acutionTypevector.size();

				if (num*42 > scrollList->getContentSize().height)
				{
					scrollList->setInnerContainerSize(Size(105,num*42));
				}

				char** label= new char*[num];
				for (int i=0;i<num;i++)
				{
					label[i] =(char *)auction->acutionTypevector.at(i)->typedesc().c_str();
				}
				

				const char * Chaneel_highLightImage="res_ui/new_button_5.png";
				auto tabKindSelect=UITab::createWithText(num,"res_ui/new_button_5.png","res_ui/new_button_5.png","",label,VERTICAL,UITAB_VERTICAL_INTERVAL_SIZE,18);
				tabKindSelect->setPosition(Vec2(6,scrollList->getInnerContainerSize().height ));
				tabKindSelect->setHighLightImage((char *) Chaneel_highLightImage);
				tabKindSelect->setDefaultPanelByIndex(0);
				tabKindSelect->addIndexChangedEvent(this,coco_indexchangedselector(WhereExList::selectType));
				tabKindSelect->setPressedActionEnabled(true);
				scrollList->addChild(tabKindSelect);

				CC_SAFE_DELETE_ARRAY(label);
				this->setContentSize(Size(110,260));

			}break;
		case KINDLIST:
			{
 				int num= auction->acutionTypevector.at(auction->setAuctiontype)->subtypedescs_size();
				if (num*42 > scrollList->getContentSize().height)
				{
					scrollList->setInnerContainerSize(Size(105,num*42));
				}

				char** label= new char*[num];
				for (int i=0;i<num;i++)
				{
					label[i] =(char *)auction->acutionTypevector.at(auction->setAuctiontype)->subtypedescs(i).c_str();
				}

				const char * Chaneel_highLightImage="res_ui/new_button_5.png";
				auto tabKindSelect=UITab::createWithText(num,"res_ui/new_button_5.png","res_ui/new_button_5.png","",label,VERTICAL,UITAB_VERTICAL_INTERVAL_SIZE,18);
				tabKindSelect->setPosition(Vec2(5,scrollList->getInnerContainerSize().height));
				tabKindSelect->setHighLightImage((char *) Chaneel_highLightImage);
				tabKindSelect->setDefaultPanelByIndex(0);
				tabKindSelect->addIndexChangedEvent(this,coco_indexchangedselector(WhereExList::selectKind));
				tabKindSelect->setPressedActionEnabled(true);
				scrollList->addChild(tabKindSelect);

				CC_SAFE_DELETE_ARRAY(label);
				this->setContentSize(Size(110,260));
			}break;
		case LEVELLIST:
			{
				int num= auction->acutionTypevector.at(auction->setAuctiontype)->leveldescs_size();
				if (num*42 > scrollList->getContentSize().height)
				{
					scrollList->setInnerContainerSize(Size(105,num*42));
				}
				char* label[10];
				for (int i=0;i<num;i++)
				{
					if (i == 0)
					{
						label[i] = (char *)auction->acutionTypevector.at(auction->setAuctiontype)->leveldescs(i).c_str();
					}else
					{
						std::string lvDes = "";
						const char *stringDes_ = StringDataManager::getString("rank_level");
						lvDes.append(stringDes_);
						lvDes.append(auction->acutionTypevector.at(auction->setAuctiontype)->leveldescs(i).c_str());
						label[i] = new char[20];
						strcpy(label[i],lvDes.c_str());
					}
				}
				
				const char * Chaneel_highLightImage="res_ui/new_button_5.png";
				auto tabLevelSelect=UITab::createWithText(num,"res_ui/new_button_5.png","res_ui/new_button_5.png","",label,VERTICAL,UITAB_VERTICAL_INTERVAL_SIZE,18);
				tabLevelSelect->setPosition(Vec2(5,scrollList->getInnerContainerSize().height));
				tabLevelSelect->setHighLightImage((char *) Chaneel_highLightImage);
				tabLevelSelect->setDefaultPanelByIndex(0);
				tabLevelSelect->addIndexChangedEvent(this,coco_indexchangedselector(WhereExList::selectLevel));
				tabLevelSelect->setPressedActionEnabled(true);
				scrollList->addChild(tabLevelSelect);

				this->setContentSize(Size(110,260));
			}break;
		case QUALITYLIST:
			{
				int num= auction->acutionTypevector.at(auction->setAuctiontype)->qualitydescs_size();
				if (num*42 > scrollList->getContentSize().height)
				{
					scrollList->setInnerContainerSize(Size(105,num*42));
				}
				char** label= new char*[num];
				for (int i=0;i<num;i++)
				{
					label[i] =(char *)auction->acutionTypevector.at(auction->setAuctiontype)->qualitydescs(i).c_str();
				}

				const char * Chaneel_highLightImage="res_ui/new_button_5.png";
				auto tabQualitySelect=UITab::createWithText(num,"res_ui/new_button_5.png","res_ui/new_button_5.png","",label,VERTICAL,UITAB_VERTICAL_INTERVAL_SIZE,18);
				tabQualitySelect->setPosition(Vec2(5,scrollList->getInnerContainerSize().height));
				tabQualitySelect->setHighLightImage((char *) Chaneel_highLightImage);
				tabQualitySelect->setDefaultPanelByIndex(0);
				tabQualitySelect->addIndexChangedEvent(this,coco_indexchangedselector(WhereExList::selectQuality));
				tabQualitySelect->setPressedActionEnabled(true);
				scrollList->addChild(tabQualitySelect);

				CC_SAFE_DELETE_ARRAY(label);
				this->setContentSize(Size(110,260));
			}break;
		case GOLDLIST:
			{
				const char *strings_goldAll = StringDataManager::getString("auction_SelectMoney");
				char *strings_1 =const_cast<char*>(strings_goldAll);
				const char *strings_gold = StringDataManager::getString("skillui_gold");
				char *strings_2 =const_cast<char*>(strings_gold);
				const char *strings_goldGoint = StringDataManager::getString("skillui_goldingold");
				char *strings_3 =const_cast<char*>(strings_goldGoint);

				char * label[] = {strings_1,strings_2,strings_3};
				const char * Chaneel_highLightImage="res_ui/new_button_5.png";
				auto tabQualitySelect=UITab::createWithText(3,"res_ui/new_button_5.png","res_ui/new_button_5.png","",label,VERTICAL,UITAB_VERTICAL_INTERVAL_SIZE,18);
				tabQualitySelect->setPosition(Vec2(5,scrollList->getInnerContainerSize().height));
				tabQualitySelect->setHighLightImage((char *) Chaneel_highLightImage);
				tabQualitySelect->setDefaultPanelByIndex(0);
				tabQualitySelect->addIndexChangedEvent(this,coco_indexchangedselector(WhereExList::seletGold));
				tabQualitySelect->setPressedActionEnabled(true);
				scrollList->addChild(tabQualitySelect);

				this->setContentSize(Size(110,260));
			}break;
		}

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}

void WhereExList::onEnter()
{
	UIScene::onEnter();
}

void WhereExList::onExit()
{
	UIScene::onExit();
}

void WhereExList::selectType( Ref * obj )
{
	auto tab =(UITab *)obj;
	int index_ = tab->getCurrentIndex();
	auto auctionui =(AuctionUi *)this->getParent()->getParent();

	if (index_ == 0)
	{
		auctionui->isSelectChat = false;
	}

	auctionui->labelKind->setString(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->subtypedescs(0).c_str());
	auctionui->setAuctionsubtype=0;

	auctionui->labelLevel->setString(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->leveldescs(0).c_str());
	auctionui->setlevel=0;

	auctionui->labelQuality->setString(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->qualitydescs(0).c_str());
	auctionui->setquality=0;

	const char *strings_goldAll = StringDataManager::getString("auction_SelectMoney");
	auctionui->label_gold->setString(strings_goldAll);
	auctionui->setMoney =0;

	auctionui->labelType->setString(auctionui->acutionTypevector.at(index_)->typedesc().c_str());
	auctionui->setAuctiontype= auctionui->acutionTypevector.at(index_)->type();
	auctionui->isSelectChat = true;
	this->removeFromParentAndCleanup(true);
}

void WhereExList::selectKind( Ref * obj )
{
	auto tab =(UITab *)obj;
	int index_= tab->getCurrentIndex();
	auto auctionui =(AuctionUi *)this->getParent()->getParent();
	
	auctionui->labelKind->setString(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->subtypedescs(index_).c_str());
	auctionui->setAuctionsubtype=auctionui->acutionTypevector.at(auctionui->setAuctiontype)->subtypes(index_);

	this->removeFromParentAndCleanup(true);
}

void WhereExList::selectLevel( Ref * obj )
{
	auto tab =(UITab *)obj;
	int index_= tab->getCurrentIndex();
	auto auctionui =(AuctionUi *)this->getParent()->getParent();

	if (index_ == 0)
	{
		auctionui->labelLevel->setString(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->leveldescs(index_).c_str());
	}else
	{
		std::string lvDes = "";
		const char *stringDes_ = StringDataManager::getString("rank_level");
		lvDes.append(stringDes_);
		lvDes.append(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->leveldescs(index_).c_str());

		auctionui->labelLevel->setString(lvDes.c_str());
	}
	auctionui->setlevel=auctionui->acutionTypevector.at(auctionui->setAuctiontype)->levels(index_);
	this->removeFromParentAndCleanup(true);
}

void WhereExList::selectQuality( Ref * obj )
{
	auto tab =(UITab *)obj;
	int index_= tab->getCurrentIndex();
	auto auctionui =(AuctionUi *)this->getParent()->getParent();
	
	auctionui->labelQuality->setString(auctionui->acutionTypevector.at(auctionui->setAuctiontype)->qualitydescs(index_).c_str());
	auctionui->setquality=auctionui->acutionTypevector.at(auctionui->setAuctiontype)->qualitys(index_);

	this->removeFromParentAndCleanup(true);
}

void WhereExList::seletGold( Ref * obj )
{
	auto tab =(UITab *)obj;
	int index_= tab->getCurrentIndex();
	auto auctionui =(AuctionUi *)this->getParent()->getParent();

	if (index_ == 0)
	{
		const char *strings_goldAll = StringDataManager::getString("auction_SelectMoney");
		auctionui->label_gold->setString(strings_goldAll);
	}else if (index_ == 1)
	{
		const char *strings_gold = StringDataManager::getString("skillui_gold");
		auctionui->label_gold->setString(strings_gold);
	}else
	{
		const char *strings_goldGoint = StringDataManager::getString("skillui_goldingold");
		auctionui->label_gold->setString(strings_goldGoint);
	}
	auctionui->setMoney = index_;

	this->removeFromParentAndCleanup(true);
}

bool WhereExList::onTouchBegan( Touch *touch, Event * pEvent )
{
	return this->resignFirstResponder(touch,this,false,false);
}