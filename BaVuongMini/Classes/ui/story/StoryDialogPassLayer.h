#ifndef _STORY_STORYDIALOGPASSLAYER_H_
#define _STORY_STORYDIALOGPASSLAYER_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/**
  * ������layer
  */
class StoryDialogPassLayer : public Layer
{
public:
	StoryDialogPassLayer();
	~StoryDialogPassLayer();

	static StoryDialogPassLayer* create();
	virtual bool init();

	virtual void update(float dt);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch* touch, Event* event);
	virtual void onTouchEnded(Touch* touch, Event* event);

public:
	void callBackSpriteSkip(Ref* obj);																	// ĵ�Skip�ʱ����Ӧ
	void refreshDelegate();
private:
	Sprite * m_pSpriteSkip;
	Sprite * m_pSpriteBg;
};

#endif
