#include "StoryBlackBand.h"
#include "StoryDialogPassLayer.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/GameSceneCamera.h"

StoryBlackBand::StoryBlackBand()
{
	////setTouchEnabled(true);
	//scheduleUpdate();
}

StoryBlackBand::~StoryBlackBand()
{
}

StoryBlackBand* StoryBlackBand::create()
{
	auto pStoryBlackBand = new StoryBlackBand();
    if (pStoryBlackBand && pStoryBlackBand->init())
    {
        pStoryBlackBand->autorelease();
        return pStoryBlackBand;
    }
    CC_SAFE_DELETE(pStoryBlackBand);
    return NULL;
}

bool StoryBlackBand::init()
{
	if(Layer::init())
	{
		auto winSize = Director::getInstance()->getVisibleSize();

		// black band at the top
		/*Sprite * sprite_bg_up = Sprite::create("res_ui/zhezhao_upside.png", Rect(0, 0, winSize.width, STORYBLACKBAND_TOP_HEIGHT));
		sprite_bg_up->setPosition(Vec2(0, winSize.height - STORYBLACKBAND_TOP_HEIGHT));
		sprite_bg_up->setAnchorPoint(Vec2::ZERO);
		sprite_bg_up->setIgnoreAnchorPointForPosition(false);
		addChild(sprite_bg_up);*/


		//// black band at the top
		//cocos2d::extension::Scale9Sprite * pBgUpSprite = cocos2d::extension::Scale9Sprite::create(Rect(12, 13, 500, 1) , "res_ui/zhezhao_mo.png");
		//pBgUpSprite->setPreferredSize(Size(winSize.width, STORYBLACKBAND_TOP_HEIGHT));
		//pBgUpSprite->setAnchorPoint(Vec2::ZERO);
		//pBgUpSprite->setIgnoreAnchorPointForPosition(false);
		//pBgUpSprite->setPosition(Vec2(0, winSize.height));
		//pBgUpSprite->setScaleY(-1);
		//addChild(pBgUpSprite);


		/*LayerColor *background = LayerColor::create(Color4B(0,0,0,255), s.width, STORYBLACKBAND_TOP_HEIGHT);
		background->setPosition(Vec2(0,s.height-STORYBLACKBAND_TOP_HEIGHT));
		addChild(background);*/


		
		//ParticleSystem* particleEffect = ParticleSystemQuad::create("animation/texiao/particledesigner/xingxingqunti.plist");
		//particleEffect->setPositionType(ParticleSystem::PositionType::FREE);
		//particleEffect->setPosition(Vec2(0, STORYBLACKBAND_TOP_HEIGHT));
		//particleEffect->setAnchorPoint(Vec2(0.5f,0.5f));
		//particleEffect->setVisible(true);
		//particleEffect->setGravity(Vec2(0,-100));
		//particleEffect->setScaleX(1.5f);
		//background->addChild(particleEffect, 1);

		// black band at the bottom
		/*Sprite * sprite_bg_bottom = Sprite::create("res_ui/zhezhao_below.png", Rect(0, 0, winSize.width, STORYBLACKBAND_HEIGHT));
		sprite_bg_bottom->setPosition(Vec2::ZERO);
		sprite_bg_bottom->setAnchorPoint(Vec2::ZERO);
		sprite_bg_bottom->setIgnoreAnchorPointForPosition(false);
		addChild(sprite_bg_bottom);*/

		// black band at the bottom
		auto pBgBottomSprite = cocos2d::extension::Scale9Sprite::create(Rect(12, 13, 500, 1) , "res_ui/zhezhao_mo.png");
		pBgBottomSprite->setOpacity(192);
		pBgBottomSprite->setPreferredSize(Size(winSize.width, STORYBLACKBAND_HEIGHT));
		pBgBottomSprite->setAnchorPoint(Vec2::ZERO);
		pBgBottomSprite->setIgnoreAnchorPointForPosition(false);
		//pBgBottomSprite->setPosition(Vec2::ZERO);
		pBgBottomSprite->setPosition(Vec2(0, 0 - STORYBLACKBAND_HEIGHT));
		addChild(pBgBottomSprite);
		


		/*background = LayerColor::create(Color4B(0,0,0,255), s.width, STORYBLACKBAND_HEIGHT);
		background->setPosition(Vec2(0,0));
		addChild(background);*/
		
		//particleEffect = ParticleSystemQuad::create("animation/texiao/particledesigner/xingxingqunti.plist");
		//particleEffect->setPositionType(ParticleSystem::PositionType::FREE);
		//particleEffect->setPosition(Vec2(0.5f,0.5f));
		//particleEffect->setAnchorPoint(Vec2(0.5f,0.5f));
		//particleEffect->setVisible(true);
		//particleEffect->setGravity(Vec2(0,100));
		//particleEffect->setScaleX(1.5f);
		//background->addChild(particleEffect, 1);
		// 

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(StoryBlackBand::onTouchBegan, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
		
		return true;
	}
	return false;
}

void StoryBlackBand::update(float dt)
{

}

bool StoryBlackBand::onTouchBegan(Touch* touch, Event* event)
{
	// �ھ���ģʽʱ����ش����¼�����ҵ����Ϸ����
	return true;
}

void StoryBlackBand::endActionCallBack(Node * object)
{
	auto pNode = (Node *)object;
	if (NULL != pNode)
	{
		pNode->removeFromParent();
	}

	auto scene = GameView::getInstance()->getGameScene();
	if (NULL != scene)
	{
		scene->getSceneCamera()->FollowActor(NULL);
	}
}
