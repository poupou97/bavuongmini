#include "StoryDialogPassLayer.h"
#include "../../legend_script/ScriptManager.h"
#include "../../GameView.h"

StoryDialogPassLayer::StoryDialogPassLayer()
	:m_pSpriteSkip(NULL)
	,m_pSpriteBg(NULL)
{
	
}

StoryDialogPassLayer::~StoryDialogPassLayer()
{

}

StoryDialogPassLayer* StoryDialogPassLayer::create()
{
    auto pDialogPassLayer = new StoryDialogPassLayer();
    if (pDialogPassLayer && pDialogPassLayer->init())
    {
        pDialogPassLayer->autorelease();
        return pDialogPassLayer;
    }
    CC_SAFE_DELETE(pDialogPassLayer);
    return NULL;
}

bool StoryDialogPassLayer::init()
{
	if(Layer::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		// �layer
		auto pBaseLayer = Layer::create();
		pBaseLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
		pBaseLayer->setContentSize(winSize);
		pBaseLayer->setPosition(Vec2::ZERO);
		addChild(pBaseLayer);

		// skip sprite
		m_pSpriteSkip = Sprite::create("res_ui/dianji_zi.png");

		m_pSpriteBg = Sprite::create("res_ui/moji_4.png");
		m_pSpriteBg->setScaleX(1.3f);
		m_pSpriteBg->setScaleY(1.6f);
		m_pSpriteBg->setAnchorPoint(Vec2::ZERO);
		m_pSpriteBg->setPosition((Vec2(winSize.width - m_pSpriteSkip->getContentSize().width - 22 - 10, winSize.height - m_pSpriteSkip->getContentSize().height - 15 - 4)));
		addChild(m_pSpriteBg);

		m_pSpriteSkip->setAnchorPoint(Vec2::ZERO);
		m_pSpriteSkip->setPosition((Vec2(winSize.width - m_pSpriteSkip->getContentSize().width - 22, winSize.height - m_pSpriteSkip->getContentSize().height - 15)));
		addChild(m_pSpriteSkip);

		
		this->setContentSize(winSize);
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(StoryDialogPassLayer::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(StoryDialogPassLayer::onTouchEnded, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void StoryDialogPassLayer::update(float dt)
{

}

bool StoryDialogPassLayer::onTouchBegan(Touch* touch, Event* event)
{
	Vec2 touchpoint = touch->getLocation();		 //��ȡ������

	// 괥���� �� skip spriteڷ�Χ�ڣ��������飨ʵ�ֵ��ͼƬ��Ч�
	if (touchpoint.x >  m_pSpriteSkip->getPositionX() - 50 && touchpoint.x < m_pSpriteSkip->getPositionX() + m_pSpriteSkip->getContentSize().width + 50 &&
		touchpoint.y > m_pSpriteSkip->getPositionY() - 50 && touchpoint.y < m_pSpriteSkip->getPositionY() + m_pSpriteSkip->getContentSize().height + 50)
	{
		return true;
	}

	return false;
}

void StoryDialogPassLayer::onTouchEnded(Touch* touch, Event* event)
{
	Vec2 touchpoint = touch->getLocation();		 //��ȡ������
	// 괥���� �� skip spriteڷ�Χ�ڣ��������飨ʵ�ֵ��ͼƬ��Ч�
	if (touchpoint.x >  m_pSpriteSkip->getPositionX() - 50 && touchpoint.x < m_pSpriteSkip->getPositionX() + m_pSpriteSkip->getContentSize().width + 50 &&
		touchpoint.y > m_pSpriteSkip->getPositionY() - 50 && touchpoint.y < m_pSpriteSkip->getPositionY() + m_pSpriteSkip->getContentSize().height + 50)
	{
		ScriptManager::getInstance()->skipScript();
	}
}

void StoryDialogPassLayer::onEnter()
{
	//Director::getInstance()->getTouchDispatcher()->addTargetedDelegate(this, -127, true);
}

void StoryDialogPassLayer::onExit()
{
	//Director::getInstance()->getTouchDispatcher()->removeDelegate(this);
}