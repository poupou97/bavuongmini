#include "StoryDialog.h"

#include "../extensions/CCRichLabel.h"
#include "GameView.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "StoryBlackBand.h"
#include "AppMacros.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/command/MovieScripts.h"
#include "../../utils/GameConfig.h"

#define STORYDIALOG_SIMPLE_DIALOG_TAG 101
#define STORYDIALOG_SIMPLE_DIALOG_FADEIN_TAG 102

std::string StoryDialog::s_lastName = "";

StoryDialog::StoryDialog()
{
	//scheduleUpdate();
}

StoryDialog::~StoryDialog()
{
}

StoryDialog* StoryDialog::create(const char* content, const char* headFigure, int headPosition, const char* roleName)
{
    auto pDialog = new StoryDialog();
    if (pDialog && pDialog->init(content, headFigure, headPosition, roleName))
    {
        pDialog->autorelease();
        return pDialog;
    }
    CC_SAFE_DELETE(pDialog);
    return NULL;
}

bool StoryDialog::init(const char* content, const char* headFigure, int headPosition, const char* roleName)
{
	if(Layer::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		const int blackBandHeight = STORYBLACKBAND_HEIGHT;
		const int headIconWidth = 166;
		const int roleNameHeight = 26;
		const int headIconY = 45;
		const int nOffsetY = 7;
		const int nLabelOffsetX = 10;
		const int nHeadOffsetY = 30;   // 105

		// dialog content
		/*CCRichLabel* pRichLabel = CCRichLabel::createWithString(content, Size(winSize.width - headIconWidth, blackBandHeight), NULL, NULL);
		addChild(pRichLabel);*/
		CCRichLabel * pRichLabel = NULL;

		// name
		Label* pNameLabel = NULL;

		// name_di
		auto pSpriteNameFrame = Sprite::create("res_ui/huabian_a.png");
		pSpriteNameFrame->setIgnoreAnchorPointForPosition(false);
		pSpriteNameFrame->setAnchorPoint(Vec2(0.5f, 0));

		// head icon
		Sprite* pHeadSprite = NULL;
		std::string currentName = "";
		int profession = GameView::getInstance()->myplayer->getProfession();
		if(strcmp(headFigure, STORY_HERO_ID) == 0)
		{
			// MyPlayer's head icon
			pHeadSprite = Sprite::create(BasePlayer::getHalfBodyPathByProfession(profession).c_str());
			if(pHeadSprite == NULL)
			{
				pHeadSprite = Sprite::create();   // it's hero
			}
				
			// role name
			//const char* name = GameView::getInstance()->myplayer->getActorName().c_str();
			const char* name = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name().c_str();
			pNameLabel = Label::createWithTTF(name, APP_FONT_NAME, 20);
			currentName = name;
			//pNameLabel = Label::createWithTTF("hero", APP_FONT_NAME, 20);   // test code
			pNameLabel->setColor(Color3B(234, 222, 0));
			
		}
		else if(strcmp(headFigure, STORY_HEROINE_ID) == 0)
		{
			// test code
			//profession = PROFESSION_GM_INDEX;

			// head icon
			std::string figureName = StoryDialog::getHeroineFigureName(profession);
			pHeadSprite = createHeadSprite(figureName.c_str());

			// role name
			std::string name =  StoryDialog::getHeroineName(profession);
			pNameLabel = Label::createWithTTF(name.c_str(), APP_FONT_NAME, 20);
			currentName = name;
			pNameLabel->setColor(Color3B(234, 222, 0));
			
		}
		else
		{
			// head icon
			pHeadSprite = createHeadSprite(headFigure);

			// role name
			pNameLabel = Label::createWithTTF(roleName, APP_FONT_NAME, 20);
			currentName = roleName;
			pNameLabel->setColor(Color3B(234, 222, 0));
			
		}

		// dialog content
		int nWidthRichLabel = winSize.width - (nLabelOffsetX + headIconWidth);
		pRichLabel = CCRichLabel::createWithString(content,  Size(nWidthRichLabel, blackBandHeight), NULL, NULL, 0, 20.f, 3);
		addChild(pRichLabel);
		
		//CCAssert(pSprite != NULL, "should not be nil");
		float headSpritePosX = 0;
		float headSpritePosY = 0;
        if(pHeadSprite != NULL)
		{
			pHeadSprite->setVisible(false);
			addChild(pHeadSprite);

			const float ACTION_MOVE_DISTANCE = 20;
			if(headPosition == head_left) 
			{
				headSpritePosX = (headIconWidth - pHeadSprite->getContentSize().width) / 2;
				headSpritePosY = nHeadOffsetY;
				pHeadSprite->setAnchorPoint(Vec2(0,0));
				pHeadSprite->setPosition(Vec2(headSpritePosX-ACTION_MOVE_DISTANCE, headSpritePosY));

				addChild(pSpriteNameFrame);

				// name
				pNameLabel->setAnchorPoint(Vec2(0.5f, 0));
				pNameLabel->setPosition(Vec2(headIconWidth/2, 5));
				addChild(pNameLabel);

				// name_di
				pSpriteNameFrame->setPosition(Vec2(pNameLabel->getPositionX() + 6, 
					pNameLabel->getPositionY() - (pSpriteNameFrame->getContentSize().height - pNameLabel->getContentSize().height) / 2 + 5));
			}
			else
			{
				headSpritePosX = winSize.width - headIconWidth + (headIconWidth - pHeadSprite->getContentSize().width) / 2;
				headSpritePosY = nHeadOffsetY;
				pHeadSprite->setAnchorPoint(Vec2(0, 0));
				pHeadSprite->setPosition(Vec2(headSpritePosX+ACTION_MOVE_DISTANCE, headSpritePosY));

				addChild(pSpriteNameFrame);

				// name
				pNameLabel->setAnchorPoint(Vec2(0.5f, 0));
				pNameLabel->setPosition(Vec2(winSize.width - headIconWidth/2, 5));
				addChild(pNameLabel);

				//name_di
				pSpriteNameFrame->setPosition(Vec2(pNameLabel->getPositionX() + 6, 
					pNameLabel->getPositionY() - (pSpriteNameFrame->getContentSize().height - pNameLabel->getContentSize().height) / 2 + 5));
			}
			if(StoryDialog::s_lastName == currentName)
			{
				pHeadSprite->setVisible(true);
				pHeadSprite->setPosition(Vec2(headSpritePosX, headSpritePosY));
			}
			else
			{
				auto move_action = Spawn::create(
					MoveTo::create(0.2f, Vec2(headSpritePosX, headSpritePosY)),
					FadeIn::create(0.2f),
					NULL);
				auto action = Sequence::create(
					DelayTime::create(0.05f),
					Show::create(),
					move_action,
					NULL);
				pHeadSprite->runAction(action);
			}

			// head icon ���ֲ
			//Sprite * pHeadZheZhaoSprite = Sprite::create("res_ui/zhezhao_role.png", Rect(0, 0, 116, 16));
			/*Sprite * pHeadZheZhaoSprite = Sprite::create("res_ui/zhezhao_role.png", Rect(0, 0, pHeadSprite->getContentSize().width, 16));
			pHeadZheZhaoSprite->setAnchorPoint(Vec2::ZERO);
			pHeadZheZhaoSprite->setIgnoreAnchorPointForPosition(false);
			pHeadZheZhaoSprite->setPosition(Vec2(headSpritePosX, headSpritePosY));
			addChild(pHeadZheZhaoSprite);*/
		}
		StoryDialog::s_lastName = currentName;

		if(headPosition == head_left) 
		{
			int _offsetY = STORYBLACKBAND_HEIGHT - 2 * nOffsetY;
			//pNameLabel->setAnchorPoint(Vec2(0, 0));
			//pNameLabel->setPosition(Vec2(headIconWidth, _offsetY - pNameLabel->getContentSize().height));

			pRichLabel->setAnchorPoint(Vec2(0,0));
			//pRichLabel->setPosition(Vec2(headIconWidth, pNameLabel->getPositionY() - pRichLabel->getContentSize().height));
			pRichLabel->setPosition(Vec2(headIconWidth, _offsetY - pRichLabel->getContentSize().height));
		}
		else 
		{
			int _offsetY = STORYBLACKBAND_HEIGHT - 2 * nOffsetY;
			//pNameLabel->setAnchorPoint(Vec2(0, 0));
			//pNameLabel->setPosition(Vec2(winSize.width - headIconWidth - pNameLabel->getContentSize().width, _offsetY - pNameLabel->getContentSize().height));

			pRichLabel->setAnchorPoint(Vec2(0,0));
			//pRichLabel->setPosition(Vec2(winSize.width - headIconWidth - pRichLabel->getContentSize().width, pNameLabel->getPositionY() - pRichLabel->getContentSize().height));
			pRichLabel->setPosition(Vec2(winSize.width - headIconWidth - pRichLabel->getContentSize().width, _offsetY - pRichLabel->getContentSize().height));
		}

		// �layer
		auto pBaseLayer = Layer::create();
		pBaseLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
		pBaseLayer->setContentSize(winSize);
		pBaseLayer->setPosition(Vec2::ZERO);
		addChild(pBaseLayer);

		/******************************���ɫС�������ȥ���Ϊ��С�֣�***********************************************/
		//// ��ɫ��С��
		//Sprite * pTrangleSprite = Sprite::create("res_ui/triangle_green.png");
		//pTrangleSprite->setAnchorPoint(Vec2::ZERO);
		//pTrangleSprite->setIgnoreAnchorPointForPosition(false);
		//pTrangleSprite->setPosition(Vec2(pRichLabel->getPositionX() + pRichLabel->getEndPos().x, pRichLabel->getPositionY() + pRichLabel->getEndPos().y - 2));
		//addChild(pTrangleSprite);

		//// ����¶�� �Ч�
		//MoveBy * pActionMoveByDown = MoveBy::create(1.0f, Vec2(0, 8));
		//MoveBy * pActionMoveByUp = MoveBy::create(1.0f, Vec2(0, -8)); 
		//CCEaseSineInOut * pActionExpoIn = CCEaseSineInOut::create(pActionMoveByDown);
		//CCEaseSineInOut * pActionExpoOut = CCEaseSineInOut::create(pActionMoveByUp);
		//Sequence * pActionSequence = Sequence::createWithTwoActions(pActionExpoIn, pActionExpoOut);
		//RepeatForever * pActionRepeat = RepeatForever::create(pActionSequence);
		//pTrangleSprite->runAction(pActionRepeat);
		/*************************************************************************************************************/

		// �С��ָ�
		std::string animFileName = "animation/texiao/renwutexiao/shou/shou.anm";
		auto m_pAnim = CCLegendAnimation::create(animFileName);
		m_pAnim->setPlayLoop(true);
		m_pAnim->setPosition(Vec2(pRichLabel->getPositionX() + pRichLabel->getEndPos().x, pRichLabel->getPositionY() + pRichLabel->getEndPos().y - 2));
		m_pAnim->setReleaseWhenStop(true);
		this->addChild(m_pAnim);

		// 15�����Զ����þ��
		const float AUTO_SKIP_TIME = 15.0f;

		auto action =(ActionInterval *)Sequence::create(
			DelayTime::create(AUTO_SKIP_TIME),
			CallFunc::create(CC_CALLBACK_0(StoryDialog::skipDialog,this)),
			NULL);
		this->runAction(action);

		this->setContentSize(winSize);
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(StoryDialog::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(StoryDialog::onTouchEnded, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

StoryDialog* StoryDialog::create(const char* content, float fadeInTime)
{
	auto pDialog = new StoryDialog();
    if (pDialog && pDialog->init(content, fadeInTime))
    {
        pDialog->autorelease();
        return pDialog;
    }
    CC_SAFE_DELETE(pDialog);
    return NULL;
}

bool StoryDialog::init(const char* content, float fadeInTime)
{
	if(Layer::init())
	{
		Size s = Director::getInstance()->getVisibleSize();

		const int dialogHeight = 30;

		// dialog content
		auto pRichLabel = CCRichLabel::createWithString(content, Size(s.width, dialogHeight), NULL, NULL, 0, 30.f);
		pRichLabel->setAnchorPoint(Vec2(0.5f,0.5f));
		pRichLabel->setPosition(Vec2(s.width/2, s.height/2));

		pRichLabel->setOpacity(0);
		auto fadeInAction = FadeIn::create(fadeInTime);
		fadeInAction->setTag(STORYDIALOG_SIMPLE_DIALOG_FADEIN_TAG);
		pRichLabel->runAction(fadeInAction);

		addChild(pRichLabel,0, STORYDIALOG_SIMPLE_DIALOG_TAG);

		auto action =(ActionInterval *)Sequence::create(
				DelayTime::create(fadeInTime + 2.0f),
				CallFunc::create(CC_CALLBACK_0(StoryDialog::skipDialog,this)),
				NULL);
		this->runAction(action);

		this->setContentSize(s);
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		return true;
	}
	return false;
}

void StoryDialog::update(float dt)
{

}

void StoryDialog::skipDialog()
{
	// dialog is over
	auto sc = ScriptManager::getInstance()->getScriptById(m_pDialogScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(this);

	this->removeFromParent();
}

bool StoryDialog::onTouchBegan(Touch* touch, Event* event)
{
	return true;
}

void StoryDialog::onTouchEnded(Touch* touch, Event* event)
{
	// 鵱�����֣����ڵ���ʱ������Ӧ���
	auto pSimpleDiaglogNode = this->getChildByTag(STORYDIALOG_SIMPLE_DIALOG_TAG);
	if(pSimpleDiaglogNode != NULL)
	{
		if(pSimpleDiaglogNode->getActionByTag(STORYDIALOG_SIMPLE_DIALOG_FADEIN_TAG) != NULL)
			return;
	}

	// dialog is over
	skipDialog();
}

void StoryDialog::registerScriptCommand(int scriptId)
{
	m_pDialogScriptInstanceId = scriptId;
}

std::string StoryDialog::getHeroineName(int profession)
{
	const char* _id = NULL;
	if(profession == PROFESSION_MJ_INDEX)
	{
		_id = "heroine_1_name";
	}
	else if (profession == PROFESSION_GM_INDEX)
	{
		_id = "heroine_2_name";
	}
	else if (profession == PROFESSION_HJ_INDEX)
	{
		_id = "heroine_3_name";
	}
	else if (profession == PROFESSION_SS_INDEX)
	{
		_id = "heroine_4_name";
	}
	CCAssert(_id != NULL, "wrong data");
	return GameConfig::getStringForKey(_id);
}
std::string StoryDialog::getHeroineFigureName(int profession)
{
	const char* _id = NULL;
	if(profession == PROFESSION_MJ_INDEX)
	{
		_id = "heroine_1_figure";
	}
	else if (profession == PROFESSION_GM_INDEX)
	{
		_id = "heroine_2_figure";
	}
	else if (profession == PROFESSION_HJ_INDEX)
	{
		_id = "heroine_3_figure";
	}
	else if (profession == PROFESSION_SS_INDEX)
	{
		_id = "heroine_4_figure";
	}
	CCAssert(_id != NULL, "wrong data");
	return GameConfig::getStringForKey(_id);
}
std::string StoryDialog::getHeroineAnimName(int profession)
{
	const char* _id = NULL;
	if(profession == PROFESSION_MJ_INDEX)
	{
		_id = "heroine_1_animation";
	}
	else if (profession == PROFESSION_GM_INDEX)
	{
		_id = "heroine_2_animation";
	}
	else if (profession == PROFESSION_HJ_INDEX)
	{
		_id = "heroine_3_animation";
	}
	else if (profession == PROFESSION_SS_INDEX)
	{
		_id = "heroine_4_animation";
	}
	CCAssert(_id != NULL, "wrong data");
	return GameConfig::getStringForKey(_id);
}

Sprite* StoryDialog::createHeadSprite(const char* headFigure)
{
	Sprite* pHeadSprite = NULL;

	// head icon
	std::string headFigureName = headFigure;
	int index = headFigureName.find(".png");
	headFigureName = headFigureName.substr(0, index);

	std::string headFigureFileName = "res_ui/";
	headFigureFileName.append(headFigureName);
	headFigureFileName.append(".png");
	pHeadSprite = Sprite::create(headFigureFileName.c_str());

	//// use big picture
	//std::string headFigureFileName = "res_ui/";
	//headFigureFileName.append(headFigureName);
	//headFigureFileName.append("_big.png");
	//pHeadSprite = Sprite::create(headFigureFileName.c_str());
	//if(pHeadSprite == NULL)   // no big picture, then use the normal one
	//{
	//	headFigureFileName = "res_ui/";
	//	headFigureFileName.append(headFigureName);
	//	headFigureFileName.append(".png");
	//	pHeadSprite = Sprite::create(headFigureFileName.c_str());
	//}

	return pHeadSprite;
}