#ifndef _STORY_STORYBLACKBAND_H_
#define _STORY_STORYBLACKBAND_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

#define STORYBLACKBAND_HEIGHT 114
#define STORYBLACKBAND_TOP_HEIGHT 60

/**
  * Display a black band at the top and bottom
  * @Author zhaogang
  * @Date 2014/1/4
  */
class StoryBlackBand : public Layer
{
public:
    StoryBlackBand();
	~StoryBlackBand();

	static StoryBlackBand* create();
	virtual bool init();

	virtual void update(float dt);

	virtual bool onTouchBegan(Touch* touch, Event* event);

public:
	void endActionCallBack(Node * object);

private:

};

#endif
