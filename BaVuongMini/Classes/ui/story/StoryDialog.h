#ifndef _STORY_STORYDIALOG_H_
#define _STORY_STORYDIALOG_H_

#include "cocos2d.h"
#include "../../legend_script/ScriptHandlerProtocol.h"

USING_NS_CC;

#define STORY_HERO_ID "hero"
#define STORY_HEROINE_ID "heroine"

/**
  * Display a dialog at the bottom of the screen
  */
class StoryDialog : public Layer, public ScriptHandlerProtocol
{
public:
	enum HeadPositionLayout {
		head_left = 0,
		head_right
	};

public:
    StoryDialog();
	~StoryDialog();

	static StoryDialog* create(const char* content, const char* headFigure, int headPosition, const char* roleName);
	virtual bool init(const char* content, const char* headFigure, int headPosition, const char* roleName);

	static StoryDialog* create(const char* content, float fadeInTime);
	virtual bool init(const char* content, float fadeInTime);

	virtual void update(float dt);

	virtual bool onTouchBegan(Touch* touch, Event* event);
	virtual void onTouchEnded(Touch* touch, Event* event);

	virtual void registerScriptCommand(int scriptId);

	static std::string getHeroineName(int profession);
	static std::string getHeroineFigureName(int profession);
	static std::string getHeroineAnimName(int profession);

protected:
	Sprite* createHeadSprite(const char* headFigure);

	void skipDialog();

private:
	int m_pDialogScriptInstanceId;
	static std::string s_lastName;
};

#endif
