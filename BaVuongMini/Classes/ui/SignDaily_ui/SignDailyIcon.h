#ifndef _UI_SIGNDAILY_SIGNDAILYICON_H_
#define _UI_SIGNDAILY_SIGNDAILYICON_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ǩ��Icon
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.18
 */


class CCTutorialParticle;

class SignDailyIcon:public UIScene
{
public:
	SignDailyIcon(void);
	~SignDailyIcon(void);

public:
	static SignDailyIcon* create();
	bool init();
	void update(float dt);

	virtual void onEnter();
	virtual void onExit();

public:
	Button * m_btn_signDailyIcon;					// �ɵ��ICON
	Layer * m_layer_signDaily;							// ���Ҫ�õ��layer
	Label * m_label_signDailyIcon_text;			// 	ICON��·���ʾ�����	
	cocos2d::extension::Scale9Sprite * m_spirte_fontBg;				// �������ʾ�ĵ�ͼ

public:
	void callBackBtnSign(Ref *pSender, Widget::TouchEventType type);

public:
	void initDataFromIntent();								// ��ʼ����ݴintent
	void setIsCanSign(bool bIsCanSign);			// ���� ��Ƿ��ǩ��
	void refreshIcon();										// ���ICON ui

private:
	 CCTutorialParticle * m_tutorialParticle;			// ����߽�����Ч
	 bool m_bIsCanSign;										// �Ƿ����ǩ��
	

};

#endif

