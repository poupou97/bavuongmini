#include "SignDailyIcon.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "SignDailyData.h"
#include "SignDaily.h"
#include "SignDailyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"

#define  TAG_TUTORIALPARTICLE 600

SignDailyIcon::SignDailyIcon(void)
	:m_btn_signDailyIcon(NULL)
	,m_layer_signDaily(NULL)
	,m_label_signDailyIcon_text(NULL)
	,m_spirte_fontBg(NULL)
	,m_bIsCanSign(false)
{

}


SignDailyIcon::~SignDailyIcon(void)
{
	
}

SignDailyIcon* SignDailyIcon::create()
{
	auto signDailyIcon = new SignDailyIcon();
	if (signDailyIcon && signDailyIcon->init())
	{
		signDailyIcon->autorelease();
		return signDailyIcon;
	}
	CC_SAFE_DELETE(signDailyIcon);
	return NULL;
}

bool SignDailyIcon::init()
{
	if (UIScene::init())
	{
		auto winSize = Director::getInstance()->getVisibleSize();

		// icon
		m_btn_signDailyIcon = Button::create();
		m_btn_signDailyIcon->setTouchEnabled(true);
		m_btn_signDailyIcon->setPressedActionEnabled(true);
		//m_btn_signDailyIcon->loadTextures("gamescene_state/zhujiemian2/tubiao/OnlineAwards.png", "gamescene_state/zhujiemian2/tubiao/OnlineAwards.png","");
		m_btn_signDailyIcon->loadTextures("res_ui/meiriqiandao.png", "res_ui/meiriqiandao.png","");
		m_btn_signDailyIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_btn_signDailyIcon->setPosition(Vec2( winSize.width - 70 - 159 - 80, winSize.height-70));
		m_btn_signDailyIcon->addTouchEventListener(CC_CALLBACK_2(SignDailyIcon::callBackBtnSign, this));

		// label
		m_label_signDailyIcon_text = Label::createWithTTF("bu ke qian dao", APP_FONT_NAME, 14);
		m_label_signDailyIcon_text->setColor(Color3B(255, 246, 0));
		m_label_signDailyIcon_text->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_label_signDailyIcon_text->setPosition(Vec2(m_btn_signDailyIcon->getPosition().x, m_btn_signDailyIcon->getPosition().y - m_btn_signDailyIcon->getContentSize().height / 2 - 5));


		// text_bg
		m_spirte_fontBg = cocos2d::extension::Scale9Sprite::create(Rect(5, 10, 1, 4) , "res_ui/zhezhao70.png");
		m_spirte_fontBg->setPreferredSize(Size(47, 14));
		m_spirte_fontBg->setAnchorPoint(Vec2(0.5f, 0.5f));
		m_spirte_fontBg->setPosition(m_label_signDailyIcon_text->getPosition());


		m_layer_signDaily = Layer::create();
		//m_layer_signDaily->setTouchEnabled(true);
		m_layer_signDaily->setAnchorPoint(Vec2::ZERO);
		m_layer_signDaily->setPosition(Vec2::ZERO);
		m_layer_signDaily->setContentSize(winSize);

		m_layer_signDaily->addChild(m_spirte_fontBg, -10);
		m_layer_signDaily->addChild(m_btn_signDailyIcon);
		m_layer_signDaily->addChild(m_label_signDailyIcon_text);

		addChild(m_layer_signDaily);

		this->schedule(schedule_selector(SignDailyIcon::update),1.0f);

		this->setContentSize(winSize);

		return true;
	}
	return false;
}

void SignDailyIcon::callBackBtnSign(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

	}
	break;

	case Widget::TouchEventType::CANCELED:
	{
		// ��ʼ�� UI WINDOW��
		auto winSize = Director::getInstance()->getVisibleSize();

		auto pTmpSignDailyUI = (SignDailyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagSignDailyUI);
		if (NULL == pTmpSignDailyUI)
		{
			auto pSignDailyUI = SignDailyUI::create();
			pSignDailyUI->setIgnoreAnchorPointForPosition(false);
			pSignDailyUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			pSignDailyUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			pSignDailyUI->setTag(kTagSignDailyUI);

			GameView::getInstance()->getMainUIScene()->addChild(pSignDailyUI);

			pSignDailyUI->refreshUI();
		}
	}
		break;

	default:
		break;
	}

}

void SignDailyIcon::onEnter()
{
	UIScene::onEnter();
}

void SignDailyIcon::onExit()
{
	UIScene::onExit();
}

void SignDailyIcon::initDataFromIntent()
{
	std::vector<SignDaily *> vector_native_sign = SignDailyData::instance()->m_vector_native_sign;
	int nSignSize = vector_native_sign.size();
	for (int i = 0; i < nSignSize; i++)
	{
		int nGiftIndex = vector_native_sign.at(i)->m_nGiftIndex;
		int nGiftStatus = vector_native_sign.at(i)->m_nStatus;

		if (2 == nGiftStatus)
		{
			m_bIsCanSign = true;

			break;
		}
	}

	refreshIcon();
}

void SignDailyIcon::setIsCanSign( bool bIsCanSign )
{
	this->m_bIsCanSign = bIsCanSign;
}

void SignDailyIcon::update( float dt )
{
	refreshIcon();
}

void SignDailyIcon::refreshIcon()
{
	// ��� ����ǩ����� text���ʾ����ǩ������������ʾ����ǩ����
	if (m_bIsCanSign)
	{
		std::string str_text = "";

		const char *str1 = StringDataManager::getString("label_keqiandao");
		str_text.append(str1);

		m_label_signDailyIcon_text->setString(str_text.c_str());

		// ��ǩ���Ļ�������ʾ �����Ч
		auto tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL == tutorialParticle)
		{
			m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", m_btn_signDailyIcon->getContentSize().width, m_btn_signDailyIcon->getContentSize().height + 10);
			m_tutorialParticle->setPosition(Vec2(m_btn_signDailyIcon->getPosition().x, m_btn_signDailyIcon->getPosition().y - 35));
			m_tutorialParticle->setAnchorPoint(Vec2(0.5f, 0.5f));
			m_tutorialParticle->setTag(TAG_TUTORIALPARTICLE);

			this->addChild(m_tutorialParticle);
		}

	}
	else
	{
		std::string str_text = "";

		const char *str2 = StringDataManager::getString("label_yiqiandao");
		str_text.append(str2);

		m_label_signDailyIcon_text->setString(str_text.c_str());

		// ����ǩ���Ļ��������Ч ȥ�
		auto tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL != tutorialParticle)
		{
			this->removeChild(m_tutorialParticle);
		}

	}

}

