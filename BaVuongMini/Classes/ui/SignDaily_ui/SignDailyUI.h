#ifndef _UI_SIGNDAILY_SIGNDAILYUI_H_
#define _UI_SIGNDAILY_SIGNDAILYUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ǩ��UI
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.18
 */

class SignDaily;
class CCTutorialParticle;

class SignDailyUI:public UIScene
{
public:
	SignDailyUI(void);
	~SignDailyUI(void);

public:
	static Layout * s_pPanel;
	static SignDailyUI* create();
	bool init();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);

	virtual void onEnter();
	virtual void onExit();

public:
	void callBackBtnClolse(Ref *pSender, Widget::TouchEventType type);																	// �رհ�ť����Ӧ
	void callBackBtnSign(Ref *pSender, Widget::TouchEventType type);																		// ǩ����ť����Ӧ
	void callBackBtnSignGift(Ref *pSender, Widget::TouchEventType type);																	// �����ʱ����Ӧ

public:
	void refreshUI();																										// � ÷������� ݸ�� UI
	void getGiftSuc(const int nGiftNum, const int nGiftStatus);										// »�ȡ����ɹ�

private:
	void initUI();
	void initBtnStatus(SignDaily * signDaily);																	// ��ʼ��Btn��״̬
	void initGoodsInfo(SignDaily* signDaily);																	// ��ʼ��goods����Ϣ
	int getGiftNum();																							// �÷���� ��е ��� ݳ�ʼ�� ��ǰ���ڽ��н�� �ı�

	int m_nGiftNum;																								// ŵ�ǰ���ڽ��е GiftNum;

	CCTutorialParticle* m_tutorialParticle;																		// Ļ��ƿ���ȡ��ť�������Ч

private:
	std::string getEquipmentQualityByIndex(int quality);													// �� �Ʒ� ׻�øgoods���Ҫ�ĵ�ͼ
	void initParticleForGoods();

private:
	enum TagBtnSign{
		TAG_BTN_SIGN_1 = 101,
		TAG_BTN_SIGN_2 = 102,
		TAG_BTN_SIGN_3 = 103,
		TAG_BTN_SIGN_4 = 104,
		TAG_BTN_SIGN_5 = 105,
		TAG_BTN_SIGN_6 = 106,
		TAG_BTN_SIGN_7 = 107,
	};

	enum TagSignGift1{
		TAG_SIGN_GIFT_1_1 = 1011,
		TAG_SIGN_GIFT_2_1 = 1021,
		TAG_SIGN_GIFT_3_1 = 1031,
		TAG_SIGN_GIFT_4_1 = 1041,
		TAG_SIGN_GIFT_5_1 = 1051,
		TAG_SIGN_GIFT_6_1 = 1061,
	};

	enum TagSignGift2{
		TAG_SIGN_GIFT_1_2 = 1012,
		TAG_SIGN_GIFT_2_2 = 1022,
		TAG_SIGN_GIFT_3_2 = 1032,
		TAG_SIGN_GIFT_4_2 = 1042,
		TAG_SIGN_GIFT_5_2 = 1052,
		TAG_SIGN_GIFT_6_2 = 1062,
	};

	enum TagSignGift7All{
		TAG_SIGN_GIFT_7_1 = 1071,
		TAG_SIGN_GIFT_7_2 = 1072,
		TAG_SIGN_GIFT_7_3 = 1073,
		TAG_SIGN_GIFT_7_4 = 1074,
	};

	TagBtnSign m_array_tagBtnSign[7];									// tag���
	TagSignGift1 m_array_tagSignGift1[6];
	TagSignGift2 m_array_tagSignGift2[6];
	TagSignGift7All m_array_tagSignGfit7All[4];
};

#endif

