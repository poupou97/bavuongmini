#ifndef _UI_SIGNDAILY_SIGNDAILY_H_
#define _UI_SIGNDAILY_SIGNDAILY_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ǩ��ģ�飨��� ڶ��UI�����Ԫ�أ�
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.18
 */

class GoodsInfo;

class SignDaily
{
public:
	SignDaily(void);
	~SignDaily(void);

public:
	Button * m_btn_sign;									// ǩ��btn
	Text * m_label_btnText;								// ǩ��btn�textģ�ǩ��������ȡ��

	ImageView * m_image_red;							// ��ǰ���ڽ��е�ǩ������ɫ����������ʾ��

	ImageView * m_image_text;							// ��X�� image
	Text * m_label_timeNeed;							// ���X��	

	Button * m_btn_gift1_frame;				// �ǩ�����1
	ImageView * m_image_gift1_icon;
	Text * m_label_gift1_num;

	Button * m_btn_gift2_frame;				// �ǩ�����2
	ImageView * m_image_gift2_icon;
	Text * m_label_gift2_num;

	Button * m_btn_gift3_frame;				// �ǩ�����3
	ImageView * m_image_gift3_icon;
	Text * m_label_gift3_num;

	Button * m_btn_gift4_frame;				// �ǩ�����4
	ImageView * m_image_gift4_icon;
	Text * m_label_gift4_num;

	int m_nGiftIndex;												// �ǰ�����
	int m_nStatus;													// Ž�����ȡ״̬��1.������ȡ���2.����ȡ���3.�����ȡ������ϣ�
	
	std::vector<GoodsInfo *> m_vector_goods;	// OneSignEverydayGift �ṹ��� repeated Goods
	std::vector<int > m_vector_goodsNum;			// OneSignEverydayGift нṹ��� repeated int32

};

#endif

