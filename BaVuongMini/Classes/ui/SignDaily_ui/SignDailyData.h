#ifndef _UI_SIGNDAILY_SIGNDAILYDATA_H_
#define _UI_SIGNDAILY_SIGNDAILYDATA_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * ǩ��ģ�飨���ڱ�����������ݣ�
 * @author liuliang
 * @version 0.1.0
 * @date 2014.03.18
 */

class COneSignEverydayGift;
class SignDaily;
class GoodsInfo;

class SignDailyData
{
public:
	static SignDailyData * s_signDailyData;
	static SignDailyData * instance();

private:
	SignDailyData(void);
	~SignDailyData(void);

public:
	void initDataFromIntent();																									// �� m_vector_internt ���µ� m_vector_native_sign
	GoodsInfo* getGoosInfo(unsigned int nGift, int nGiftNum);												// nGift��ţ�nGiftNum �ڼ�����������0��ʼ��

	void setIsLogin(int nIsLogin);																							// �����Ƿ�Ϊ ��һ�ε�½
	int getIsLogin();
	bool isCanSign();																												// �Ƿ����ǩ��

	void setLoginPlayerLevel(int nPlayerLevel);
	int getLoginPalyerLevel();


public:
	std::vector<COneSignEverydayGift *> m_vector_internt_sign;											// ���� ������ �������ݵ� vector
	std::vector<SignDaily *> m_vector_native_sign;																// ���� ���� �� �����ݵ� vector
	
private:
	int m_nIsLogin;																													// �ǵ�½����24������ 0���ͣ�1��½
	int m_nLoginPlayerLevel;																									// ��½ʱ��ɫ�ĵȼ�
	

};

#endif

