#include "SignDailyUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "SignDailyData.h"
#include "SignDaily.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/COneSignEverydayGift.h"
#include "../../GameView.h"
#include "SignDailyIcon.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../backpackscene/GoodsItemInfoBase.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/GameUtils.h"

using namespace CocosDenshion;

Layout * SignDailyUI::s_pPanel;

#define  TAG_TUTORIALPARTICLE 500

SignDailyUI::SignDailyUI(void)
	:m_nGiftNum(0)
{
	m_array_tagBtnSign[0] = TAG_BTN_SIGN_1; 
	m_array_tagBtnSign[1] = TAG_BTN_SIGN_2; 
	m_array_tagBtnSign[2] = TAG_BTN_SIGN_3; 
	m_array_tagBtnSign[3] = TAG_BTN_SIGN_4; 
	m_array_tagBtnSign[4] = TAG_BTN_SIGN_5; 
	m_array_tagBtnSign[5] = TAG_BTN_SIGN_6; 
	m_array_tagBtnSign[6] = TAG_BTN_SIGN_7; 

	m_array_tagSignGift1[0] = TAG_SIGN_GIFT_1_1;
	m_array_tagSignGift1[1] = TAG_SIGN_GIFT_2_1;
	m_array_tagSignGift1[2] = TAG_SIGN_GIFT_3_1;
	m_array_tagSignGift1[3] = TAG_SIGN_GIFT_4_1;
	m_array_tagSignGift1[4] = TAG_SIGN_GIFT_5_1;
	m_array_tagSignGift1[5] = TAG_SIGN_GIFT_6_1;

	m_array_tagSignGift2[0] = TAG_SIGN_GIFT_1_2;
	m_array_tagSignGift2[1] = TAG_SIGN_GIFT_2_2;
	m_array_tagSignGift2[2] = TAG_SIGN_GIFT_3_2;
	m_array_tagSignGift2[3] = TAG_SIGN_GIFT_4_2;
	m_array_tagSignGift2[4] = TAG_SIGN_GIFT_5_2;
	m_array_tagSignGift2[5] = TAG_SIGN_GIFT_6_2;

	m_array_tagSignGfit7All[0] = TAG_SIGN_GIFT_7_1;
	m_array_tagSignGfit7All[1] = TAG_SIGN_GIFT_7_2;
	m_array_tagSignGfit7All[2] = TAG_SIGN_GIFT_7_3;
	m_array_tagSignGfit7All[3] = TAG_SIGN_GIFT_7_4;
}


SignDailyUI::~SignDailyUI(void)
{

}

SignDailyUI* SignDailyUI::create()
{
	auto signDailyUI = new SignDailyUI();
	if (signDailyUI && signDailyUI->init())
	{
		signDailyUI->autorelease();
		return signDailyUI;
	}
	CC_SAFE_DELETE(signDailyUI);
	return NULL;
}

bool SignDailyUI::init()
{
	if (UIScene::init())
	{
		if(LoadSceneLayer::signDailyLayout->getParent() != NULL)
		{
			LoadSceneLayer::signDailyLayout->removeFromParentAndCleanup(false);
		}
		s_pPanel = LoadSceneLayer::signDailyLayout;

		m_pLayer->addChild(s_pPanel);

		initUI();

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(790, 440));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(SignDailyUI::onTouchBegan, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		//this->refreshUI();

		return true;
	}
	return false;
}

void SignDailyUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void SignDailyUI::onExit()
{
	UIScene::onExit();

	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

void SignDailyUI::callBackBtnClolse(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void SignDailyUI::callBackBtnSign(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto pUIBtn = (Button *)pSender;
		int nSignGiftIndex = pUIBtn->getTag() - TAG_BTN_SIGN_1;

		// �������� �ǩ����nSignGiftIndex :�ڼ����ǩ����
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5113, (void *)nSignGiftIndex);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void SignDailyUI::callBackBtnSignGift(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto pImageView = (Button *)pSender;
		Size size = Director::getInstance()->getVisibleSize();

		int nGift = 0;										// �ڼ���Ľ���0ӿ�ʼ��
		int nGiftNum = 0;								// �ڼ����

		for (int i = 0; i < 7; i++)
		{
			if (m_array_tagSignGift1[i] == pImageView->getTag())
			{
				nGift = i;
				nGiftNum = 0;

				break;
			}

			if (m_array_tagSignGift2[i] == pImageView->getTag())
			{
				nGift = i;
				nGiftNum = 1;

				break;
			}

			if (i < 4 && m_array_tagSignGfit7All[i] == pImageView->getTag())
			{
				nGift = 6;
				nGiftNum = i;

				break;
			}
		}

		auto pGoodsInfo = SignDailyData::instance()->getGoosInfo(nGift, nGiftNum);
		auto goodsItemInfoBase = GoodsItemInfoBase::create(pGoodsInfo, GameView::getInstance()->EquipListItem, 0);
		goodsItemInfoBase->setIgnoreAnchorPointForPosition(false);
		goodsItemInfoBase->setAnchorPoint(Vec2(0.5f, 0.5f));
		//goodsItemInfoBase->setPosition(Vec2(size.width / 2, size.height / 2));

		GameView::getInstance()->getMainUIScene()->addChild(goodsItemInfoBase);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void SignDailyUI::initUI()
{
	auto pBtnClose = (Button*)Helper::seekWidgetByName(s_pPanel, "Button_close");
	pBtnClose->setTouchEnabled(true);
	pBtnClose->setPressedActionEnabled(true);
	pBtnClose->addTouchEventListener(CC_CALLBACK_2(SignDailyUI::callBackBtnClolse,this));
}

void SignDailyUI::refreshUI()
{
	std::vector<SignDaily *> vector_signDaily = SignDailyData::instance()->m_vector_native_sign;

	// �ǰ6��� ��ʼ������һ�£��7ڸ�ǩ����� ���Ϊ ��������б䣨4�����Ҫ�����ʼ��
	for (unsigned int i = 0; (i + 1) < vector_signDaily.size(); i++)
	{
		char s_num_i[10];
		sprintf(s_num_i, "%d", (i + 1) * 10 + (i + 1));

		char s_num_1[10];
		sprintf(s_num_1, "%d", 1);

		char s_num_2[10];
		sprintf(s_num_2, "%d", 2);

		// m_btn_sign
		std::string str_btn_gift = "";
		str_btn_gift.append("Button_");
		str_btn_gift.append(s_num_i);

		// m_label_btnText
		std::string str_label_btnGift_text = "";
		str_label_btnGift_text.append("Label_");
		str_label_btnGift_text.append(s_num_i);
		str_label_btnGift_text.append("_text");

		// m_image_red
		std::string str_image_red = "";
		str_image_red.append("ImageView_");
		str_image_red.append(s_num_i);
		str_image_red.append("_red");

		// m_image_text
		std::string str_image_text = "";
		str_image_text.append("ImageView_");
		str_image_text.append(s_num_i);
		str_image_text.append("_notime");

		// m_label_timeNeed
		std::string str_label_timeNeed = "";
		str_label_timeNeed.append("Label_");
		str_label_timeNeed.append(s_num_i);
		str_label_timeNeed.append("_notime");

		// m_image_gift1_frame
		std::string str_image_gift1_frame = "";
		str_image_gift1_frame.append("Button_");
		str_image_gift1_frame.append(s_num_i);
		str_image_gift1_frame.append("_gift");
		str_image_gift1_frame.append(s_num_1);

		// m_image_gift1_icon
		std::string str_image_gift1_icon = "";
		str_image_gift1_icon.append("ImageView_");
		str_image_gift1_icon.append(s_num_i);
		str_image_gift1_icon.append("_gift");
		str_image_gift1_icon.append(s_num_1);
		str_image_gift1_icon.append("_icon");

		// m_label_gift1_num
		std::string str_image_gift1_num = "";
		str_image_gift1_num.append("Label_");
		str_image_gift1_num.append(s_num_i);
		str_image_gift1_num.append("_gift");
		str_image_gift1_num.append(s_num_1);
		str_image_gift1_num.append("_num");

		// m_image_gift2_frame
		std::string str_image_gift2_frame = "";
		str_image_gift2_frame.append("Button_");
		str_image_gift2_frame.append(s_num_i);
		str_image_gift2_frame.append("_gift");
		str_image_gift2_frame.append(s_num_2);

		// m_image_gift2_icon
		std::string str_image_gift2_icon = "";
		str_image_gift2_icon.append("ImageView_");
		str_image_gift2_icon.append(s_num_i);
		str_image_gift2_icon.append("_gift");
		str_image_gift2_icon.append(s_num_2);
		str_image_gift2_icon.append("_icon");

		// m_label_gift2_num
		std::string str_image_gift2_num = "";
		str_image_gift2_num.append("Label_");
		str_image_gift2_num.append(s_num_i);
		str_image_gift2_num.append("_gift");
		str_image_gift2_num.append(s_num_2);
		str_image_gift2_num.append("_num");

		//ǩ��btn
		vector_signDaily.at(i)->m_btn_sign = (Button *)Helper::seekWidgetByName(s_pPanel, str_btn_gift.c_str());
		vector_signDaily.at(i)->m_btn_sign->setTouchEnabled(true);
		vector_signDaily.at(i)->m_btn_sign->setPressedActionEnabled(true);
		vector_signDaily.at(i)->m_btn_sign->addTouchEventListener(CC_CALLBACK_2(SignDailyUI::callBackBtnSign, this));
		vector_signDaily.at(i)->m_btn_sign->setTag(m_array_tagBtnSign[i]);
		vector_signDaily.at(i)->m_btn_sign->setVisible(true);

		//ǩ��btn���ı�
		vector_signDaily.at(i)->m_label_btnText = (Text*)Helper::seekWidgetByName(s_pPanel, str_label_btnGift_text.c_str());
		vector_signDaily.at(i)->m_label_btnText->setVisible(true);

		//��ɫ ������ʾ
		vector_signDaily.at(i)->m_image_red = (ImageView *)Helper::seekWidgetByName(s_pPanel, str_image_red.c_str());
		vector_signDaily.at(i)->m_image_red->setVisible(false);

		// ��X�� �ĵ�ͼ
		vector_signDaily.at(i)->m_image_text = (ImageView*)Helper::seekWidgetByName(s_pPanel, str_image_text.c_str());
		vector_signDaily.at(i)->m_image_text->setVisible(false);

		// ��X�� ���ı�
		vector_signDaily.at(i)->m_label_timeNeed = (Text*)Helper::seekWidgetByName(s_pPanel, str_label_timeNeed.c_str());
		vector_signDaily.at(i)->m_label_timeNeed->setColor(Color3B(255, 246, 0));
		vector_signDaily.at(i)->m_label_timeNeed->setVisible(false);

		vector_signDaily.at(i)->m_btn_gift1_frame = (Button *)Helper::seekWidgetByName(s_pPanel, str_image_gift1_frame.c_str());
		vector_signDaily.at(i)->m_image_gift1_icon = (ImageView *)Helper::seekWidgetByName(s_pPanel, str_image_gift1_icon.c_str());
		vector_signDaily.at(i)->m_label_gift1_num = (Text*)Helper::seekWidgetByName(s_pPanel, str_image_gift1_num.c_str());

		vector_signDaily.at(i)->m_btn_gift2_frame = (Button *)Helper::seekWidgetByName(s_pPanel, str_image_gift2_frame.c_str());
		vector_signDaily.at(i)->m_image_gift2_icon = (ImageView *)Helper::seekWidgetByName(s_pPanel, str_image_gift2_icon.c_str());
		vector_signDaily.at(i)->m_label_gift2_num = (Text*)Helper::seekWidgetByName(s_pPanel, str_image_gift2_num.c_str());

		vector_signDaily.at(i)->m_btn_gift1_frame->setVisible(false);
		vector_signDaily.at(i)->m_image_gift1_icon->setVisible(false);
		vector_signDaily.at(i)->m_label_gift1_num->setVisible(false);

		vector_signDaily.at(i)->m_btn_gift2_frame->setVisible(false);
		vector_signDaily.at(i)->m_image_gift2_icon->setVisible(false);
		vector_signDaily.at(i)->m_label_gift2_num->setVisible(false);

		vector_signDaily.at(i)->m_btn_gift1_frame->setTag(m_array_tagSignGift1[i]);
		vector_signDaily.at(i)->m_btn_gift2_frame->setTag(m_array_tagSignGift2[i]);

		vector_signDaily.at(i)->m_btn_gift1_frame->setTouchEnabled(true);
		vector_signDaily.at(i)->m_btn_gift2_frame->setTouchEnabled(true);

		vector_signDaily.at(i)->m_btn_gift1_frame->setScale(0.8f);
		vector_signDaily.at(i)->m_btn_gift2_frame->setScale(0.8f);

		vector_signDaily.at(i)->m_btn_gift1_frame->setPressedActionEnabled(true);
		vector_signDaily.at(i)->m_btn_gift2_frame->setPressedActionEnabled(true);

		vector_signDaily.at(i)->m_btn_gift1_frame->addTouchEventListener(CC_CALLBACK_2(SignDailyUI::callBackBtnSignGift, this));
		vector_signDaily.at(i)->m_btn_gift2_frame->addTouchEventListener(CC_CALLBACK_2(SignDailyUI::callBackBtnSignGift, this));

		initBtnStatus(vector_signDaily.at(i));
		initGoodsInfo(vector_signDaily.at(i));
	}

	// �7����ǩ��������ʼ��
	int nSignSize = vector_signDaily.size();
	if (7 == nSignSize)
	{
		int nIndex = nSignSize - 1;
		vector_signDaily.at(nIndex)->m_btn_sign = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_77");
		vector_signDaily.at(nIndex)->m_btn_sign->setTouchEnabled(true);
		vector_signDaily.at(nIndex)->m_btn_sign->setPressedActionEnabled(true);
		vector_signDaily.at(nIndex)->m_btn_sign->addTouchEventListener(CC_CALLBACK_2(SignDailyUI::callBackBtnSign, this));
		vector_signDaily.at(nIndex)->m_btn_sign->setTag(m_array_tagBtnSign[nSignSize - 1]);
		vector_signDaily.at(nIndex)->m_btn_sign->setVisible(true);

		vector_signDaily.at(nIndex)->m_label_btnText = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_77_text");
		vector_signDaily.at(nIndex)->m_label_btnText->setVisible(true);

		vector_signDaily.at(nIndex)->m_image_red = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_77_red");
		vector_signDaily.at(nIndex)->m_image_red->setVisible(false);

		vector_signDaily.at(nIndex)->m_image_text = (ImageView*)Helper::seekWidgetByName(s_pPanel, "ImageView_77_notime");
		vector_signDaily.at(nIndex)->m_image_text->setVisible(false);

		vector_signDaily.at(nIndex)->m_label_timeNeed = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_77_notime");
		vector_signDaily.at(nIndex)->m_label_timeNeed->setColor(Color3B(255, 246, 0));
		vector_signDaily.at(nIndex)->m_label_timeNeed->setVisible(false);

		vector_signDaily.at(nIndex)->m_btn_gift1_frame = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_77_gift1");
		vector_signDaily.at(nIndex)->m_image_gift1_icon = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_77_gift1_icon");
		vector_signDaily.at(nIndex)->m_label_gift1_num = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_77_gift1_num");

		vector_signDaily.at(nIndex)->m_btn_gift2_frame = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_77_gift2");
		vector_signDaily.at(nIndex)->m_image_gift2_icon = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_77_gift2_icon");
		vector_signDaily.at(nIndex)->m_label_gift2_num = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_77_gift2_num");

		vector_signDaily.at(nIndex)->m_btn_gift3_frame = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_77_gift3");
		vector_signDaily.at(nIndex)->m_image_gift3_icon = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_77_gift3_icon");
		vector_signDaily.at(nIndex)->m_label_gift3_num = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_77_gift3_num");

		vector_signDaily.at(nIndex)->m_btn_gift4_frame = (Button *)Helper::seekWidgetByName(s_pPanel, "Button_77_gift4");
		vector_signDaily.at(nIndex)->m_image_gift4_icon = (ImageView *)Helper::seekWidgetByName(s_pPanel, "ImageView_77_gift4_icon");
		vector_signDaily.at(nIndex)->m_label_gift4_num = (Text*)Helper::seekWidgetByName(s_pPanel, "Label_77_gift4_num");

		vector_signDaily.at(nIndex)->m_btn_gift1_frame->setVisible(false);
		vector_signDaily.at(nIndex)->m_image_gift1_icon->setVisible(false);
		vector_signDaily.at(nIndex)->m_label_gift1_num->setVisible(false);

		vector_signDaily.at(nIndex)->m_btn_gift2_frame->setVisible(false);
		vector_signDaily.at(nIndex)->m_image_gift2_icon->setVisible(false);
		vector_signDaily.at(nIndex)->m_label_gift2_num->setVisible(false);

		vector_signDaily.at(nIndex)->m_btn_gift3_frame->setVisible(false);
		vector_signDaily.at(nIndex)->m_image_gift3_icon->setVisible(false);
		vector_signDaily.at(nIndex)->m_label_gift3_num->setVisible(false);

		vector_signDaily.at(nIndex)->m_btn_gift4_frame->setVisible(false);
		vector_signDaily.at(nIndex)->m_image_gift4_icon->setVisible(false);
		vector_signDaily.at(nIndex)->m_label_gift4_num->setVisible(false);

		vector_signDaily.at(nIndex)->m_btn_gift1_frame->setTag(m_array_tagSignGfit7All[0]);
		vector_signDaily.at(nIndex)->m_btn_gift2_frame->setTag(m_array_tagSignGfit7All[1]);
		vector_signDaily.at(nIndex)->m_btn_gift3_frame->setTag(m_array_tagSignGfit7All[2]);
		vector_signDaily.at(nIndex)->m_btn_gift4_frame->setTag(m_array_tagSignGfit7All[3]);

		vector_signDaily.at(nIndex)->m_btn_gift1_frame->setTouchEnabled(true);
		vector_signDaily.at(nIndex)->m_btn_gift2_frame->setTouchEnabled(true);
		vector_signDaily.at(nIndex)->m_btn_gift3_frame->setTouchEnabled(true);
		vector_signDaily.at(nIndex)->m_btn_gift4_frame->setTouchEnabled(true);

		vector_signDaily.at(nIndex)->m_btn_gift1_frame->setScale(0.8f);
		vector_signDaily.at(nIndex)->m_btn_gift2_frame->setScale(0.8f);
		vector_signDaily.at(nIndex)->m_btn_gift3_frame->setScale(0.8f);
		vector_signDaily.at(nIndex)->m_btn_gift4_frame->setScale(0.8f);

		vector_signDaily.at(nIndex)->m_btn_gift1_frame->setPressedActionEnabled(true);
		vector_signDaily.at(nIndex)->m_btn_gift2_frame->setPressedActionEnabled(true);
		vector_signDaily.at(nIndex)->m_btn_gift3_frame->setPressedActionEnabled(true);
		vector_signDaily.at(nIndex)->m_btn_gift4_frame->setPressedActionEnabled(true);

		vector_signDaily.at(nIndex)->m_btn_gift1_frame->addTouchEventListener(CC_CALLBACK_2(SignDailyUI::callBackBtnSignGift, this));
		vector_signDaily.at(nIndex)->m_btn_gift2_frame->addTouchEventListener(CC_CALLBACK_2(SignDailyUI::callBackBtnSignGift, this));
		vector_signDaily.at(nIndex)->m_btn_gift3_frame->addTouchEventListener(CC_CALLBACK_2(SignDailyUI::callBackBtnSignGift, this));
		vector_signDaily.at(nIndex)->m_btn_gift4_frame->addTouchEventListener(CC_CALLBACK_2(SignDailyUI::callBackBtnSignGift, this));

		// ǩ�������콱�����ӻ���Ч�
		initParticleForGoods();

		initBtnStatus(vector_signDaily.at(nIndex));
		initGoodsInfo(vector_signDaily.at(nIndex));
	}
}

void SignDailyUI::initBtnStatus( SignDaily * signDaily )
{
	int nGiftIndex = signDaily->m_nGiftIndex;
	int nGiftStatus = signDaily->m_nStatus;

	/** �����Ʒ״̬ ��1.������ȡ���2.����ȡ���3.�����ȡ������*/
	if (1 == nGiftStatus)
	{
		// ϲ�����ȡ����ɫ����ʾ����X��족��btn�����
		signDaily->m_btn_sign->loadTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");
		signDaily->m_btn_sign->setTouchEnabled(false);
		signDaily->m_btn_sign->setVisible(false);

		const char *str_lianxu = StringDataManager::getString("label_lianxu");
		const char *str_tian = StringDataManager::getString("label_tian");

		char str_num[10];
		sprintf(str_num, "%d", nGiftIndex + 1);

		std::string str_text = "";
		str_text.append(str_lianxu);
		str_text.append(str_num);
		str_text.append(str_tian);

		signDaily->m_label_btnText->setString("");
		signDaily->m_label_btnText->setVisible(false);

		signDaily->m_image_text->setVisible(true);
		signDaily->m_label_timeNeed->setString(str_text.c_str());
		signDaily->m_label_timeNeed->setVisible(true);
	}
	else if (2 == nGiftStatus)
	{
		// ÿ���ȡ����ɫ����ʾ��ǩ������btn���ã�������ʾ
		signDaily->m_image_red->setVisible(true);

		signDaily->m_btn_sign->loadTextures("res_ui/new_button_2.png", "res_ui/new_button_2.png", "");
		signDaily->m_btn_sign->setTouchEnabled(true);

		const char *str_lingqu = StringDataManager::getString("btn_qiandao");
		signDaily->m_label_btnText->setString(str_lingqu);
		signDaily->m_label_btnText->setVisible(true);

		signDaily->m_image_text->setVisible(false);
		signDaily->m_label_timeNeed->setString("");
		signDaily->m_label_timeNeed->setVisible(false);

		// �������ȡ�Ļ������ ������Ч
		auto tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
		if (NULL == tutorialParticle)
		{
			auto btn_sign = signDaily->m_btn_sign;
			m_tutorialParticle = CCTutorialParticle::create("tuowei0.plist", btn_sign->getContentSize().width + 25, btn_sign->getContentSize().height + 5);
			m_tutorialParticle->setPosition(Vec2(btn_sign->getPosition().x, btn_sign->getPosition().y - 22));
			m_tutorialParticle->setAnchorPoint(Vec2(0.5f, 0.5f));
			m_tutorialParticle->setTag(TAG_TUTORIALPARTICLE);

			this->addChild(m_tutorialParticle);
		}
	}
	else if (3 ==  nGiftStatus)
	{
		// ����ȡ����ɫ����ʾ������ȡ����btn�����
		signDaily->m_btn_sign->loadTextures("res_ui/new_button_001.png", "res_ui/new_button_001.png", "");
		signDaily->m_btn_sign->setTouchEnabled(false);
		signDaily->m_btn_sign->setVisible(true);

		const char *str_yilingqu = StringDataManager::getString("btn_yilingqu");
		signDaily->m_label_btnText->setString(str_yilingqu);
		signDaily->m_label_btnText->setVisible(true);

		signDaily->m_image_text->setVisible(false);
		signDaily->m_label_timeNeed->setString("");
		signDaily->m_label_timeNeed->setVisible(false);
	}
}

void SignDailyUI::initGoodsInfo( SignDaily* signDaily )
{
	// ���� goods icon ú frame
	std::vector<GoodsInfo *> vector_goods = signDaily->m_vector_goods;
	for (unsigned int i = 0; i < vector_goods.size(); i++)
	{
		std::string goods_icon = vector_goods.at(i)->icon();

		std::string goodsIcon_ = "res_ui/props_icon/";
		goodsIcon_.append(goods_icon);
		goodsIcon_.append(".png");

		// ͸�goods�Ʒ��ѡ���ͼ
		int quality_ = vector_goods.at(i)->quality();
		std::string qualityStr_ = getEquipmentQualityByIndex(quality_);

		if (0 == i)
		{
			signDaily->m_image_gift1_icon->loadTexture(goodsIcon_.c_str());
			signDaily->m_image_gift1_icon->setVisible(true);
			signDaily->m_btn_gift1_frame->loadTextures(qualityStr_.c_str(), qualityStr_.c_str(), qualityStr_.c_str());
			signDaily->m_btn_gift1_frame->setVisible(true);
		}
		else if (1 == i)
		{
			signDaily->m_image_gift2_icon->loadTexture(goodsIcon_.c_str());
			signDaily->m_image_gift2_icon->setVisible(true);
			signDaily->m_btn_gift2_frame->loadTextures(qualityStr_.c_str(), qualityStr_.c_str(), qualityStr_.c_str());
			signDaily->m_btn_gift2_frame->setVisible(true);
		}
		else if (2 == i)
		{
			signDaily->m_image_gift3_icon->loadTexture(goodsIcon_.c_str());
			signDaily->m_image_gift3_icon->setVisible(true);
			signDaily->m_btn_gift3_frame->loadTextures(qualityStr_.c_str(), qualityStr_.c_str(), qualityStr_.c_str());
			signDaily->m_btn_gift3_frame->setVisible(true);
		}
		else if (3 == i)
		{
			signDaily->m_image_gift4_icon->loadTexture(goodsIcon_.c_str());
			signDaily->m_image_gift4_icon->setVisible(true);
			signDaily->m_btn_gift4_frame->loadTextures(qualityStr_.c_str(), qualityStr_.c_str(), qualityStr_.c_str());
			signDaily->m_btn_gift4_frame->setVisible(true);
		}
	}

	// ��� goods ����
	std::vector<int > vector_goodsNum = signDaily->m_vector_goodsNum;
	for (unsigned int j = 0; j < vector_goodsNum.size(); j++)
	{
		int goods_num = vector_goodsNum.at(j);

		char sText[10];
		sprintf(sText, "%d", goods_num);

		if (0 == j)
		{
			signDaily->m_label_gift1_num->setString(sText);
			signDaily->m_label_gift1_num->setVisible(true);
		}
		else if (1 == j)
		{
			signDaily->m_label_gift2_num->setString(sText);
			signDaily->m_label_gift2_num->setVisible(true);
		}
		else if (2 == j)
		{
			signDaily->m_label_gift3_num->setString(sText);
			signDaily->m_label_gift3_num->setVisible(true);
		}
		else if (3 == j)
		{
			signDaily->m_label_gift4_num->setString(sText);
			signDaily->m_label_gift4_num->setVisible(true);
		}
	}
}

std::string SignDailyUI::getEquipmentQualityByIndex( int quality )
{
	std::string frameColorPath = "res_ui/";
	switch(quality)
	{
	case 1:
		{
			frameColorPath.append("sdi_white");
		}break;
	case 2:
		{
			frameColorPath.append("sdi_green");
		}break;
	case 3:
		{
			frameColorPath.append("sdi_bule");
		}break;
	case 4:
		{
			frameColorPath.append("sdi_purple");
		}break;
	case 5:
		{
			frameColorPath.append("sdi_orange");
		}break;
	}
	frameColorPath.append(".png");
	return frameColorPath;
}

void SignDailyUI::getGiftSuc( const int nGiftNum, const int nGiftStatus )
{
	std::vector<COneSignEverydayGift *> vector_internet	 = SignDailyData::instance()->m_vector_internt_sign;
	for (unsigned int i = 0; i < vector_internet.size(); i++)
	{
		// ǰ�� Ľ�� �ȫ�� ʱ����Ϊ 0��status ��Ϊ ����ȡ��3��
		if (vector_internet.at(i)->number() < vector_internet.at(nGiftNum)->number())
		{
			vector_internet.at(i)->set_status(3);
		}
		else if (vector_internet.at(i)->number() == vector_internet.at(nGiftNum)->number())
		{
			vector_internet.at(i)->set_status(3);

			// ȥ� ��ư�ť�������ЧЧ�
			auto tutorialParticle = (CCTutorialParticle*)this->getChildByTag(TAG_TUTORIALPARTICLE);
			if (NULL != tutorialParticle)
			{
				this->removeChild(m_tutorialParticle);
			}
		}
	}

	SignDailyData::instance()->initDataFromIntent();

	// ������ȡ���� �ͬ��� icon(�ͨ� native����ͬ��)
	auto pSignDailyIcon = (SignDailyIcon *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagSignDailyIcon);
	if (NULL != pSignDailyIcon)
	{
		pSignDailyIcon->setIsCanSign(false);
		pSignDailyIcon->initDataFromIntent();
	}

	// ���window UI
	refreshUI();
}

int SignDailyUI::getGiftNum()
{
	std::vector<COneSignEverydayGift *> vector_internet= SignDailyData::instance()->m_vector_internt_sign;
	for (unsigned int i=0; i < vector_internet.size(); i++)
	{

		int nGiftNum =  vector_internet.at(i)->number();
		int nGiftStatus = vector_internet.at(i)->status();	

		// ����ǰ״̬ Ϊ ����ȡ�����ѯ��һ�������Ļ� ��ǰ��� �Ϊ ������ȡ�Ľ��
		if (3 == nGiftStatus)
		{
			continue;
		}
		else
		{
			m_nGiftNum = nGiftNum;
			return nGiftNum;
		}
	}

	return 0;
}

bool SignDailyUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return resignFirstResponder(touch, this, false);
}

void SignDailyUI::initParticleForGoods()
{
	std::vector<SignDaily *> vector_signDaily = SignDailyData::instance()->m_vector_native_sign;

	// �7����ǩ�����
	int nSignSize = vector_signDaily.size();
	if (7 == nSignSize)
	{
		int nIndex = nSignSize - 1;

		float width = vector_signDaily.at(nIndex)->m_btn_gift1_frame->getContentSize().width * 0.8f;
		float height = vector_signDaily.at(nIndex)->m_btn_gift1_frame->getContentSize().height * 0.8f;

		float xPos = vector_signDaily.at(nIndex)->m_btn_gift1_frame->getPosition().x - width / 2.0f;
		float yPos = vector_signDaily.at(nIndex)->m_btn_gift1_frame->getPosition().y - height / 2.0f;

		// ��Cocostudio ݶ�ȡ�json��ļ������Ľ��4�
		int nGiftSize = 4;
		for (int i = 0; i < nGiftSize; i ++)
		{
			float speed = 100.f;

			float nOff = 73.0f;

			std::string _path = "animation/texiao/particledesigner/";
			_path.append("tuowei0.plist");

			ParticleSystemQuad* particleEffect_1 = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
			particleEffect_1->setPosition(Vec2(xPos + i * 83, yPos));
			//particleEffect_1->setStartSize(30);
			particleEffect_1->setPositionType(ParticleSystem::PositionType::FREE);
			particleEffect_1->setVisible(true);
			particleEffect_1->setScale(1.0f);
			//particleEffect_1->setLife(1.5f);
			//particleEffect_1->setStartColor(color);
			//particleEffect_1->setEndColor(color);
			this->addChild(particleEffect_1);

			Sequence * sequence_1 = Sequence::create(
				MoveTo::create(height*1.0f/speed,Vec2(xPos + i * nOff, yPos + height)),
				MoveTo::create(width*1.0f/speed,Vec2(xPos + i * nOff + width, yPos + height)),
				MoveTo::create(height*1.0f/speed,Vec2(xPos + i * nOff + width, yPos)),
				MoveTo::create(width*1.0f/speed,Vec2(xPos + i * nOff, yPos)),
				NULL
				);
			RepeatForever * repeatForeverAnm_1 = RepeatForever::create(sequence_1);
			particleEffect_1->runAction(repeatForeverAnm_1);


			ParticleSystemQuad* particleEffect_2 = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
			particleEffect_2->setPosition(Vec2(xPos + i * nOff + 2 + width,yPos + 2 + height));
			//particleEffect_2->setStartSize(30);
			particleEffect_2->setPositionType(ParticleSystem::PositionType::FREE);
			particleEffect_2->setVisible(true);
			particleEffect_2->setScale(1.0f);
			//particleEffect_2->setLife(1.5f);
			//particleEffect_2->setStartColor(color);
			//particleEffect_2->setEndColor(color);
			this->addChild(particleEffect_2);

			Sequence * sequence_2 = Sequence::create(
				MoveTo::create(height*1.0f/speed,Vec2(xPos + i * nOff + width, yPos)),
				MoveTo::create(width*1.0f/speed,Vec2(xPos + i * nOff, yPos)),
				MoveTo::create(height*1.0f/speed,Vec2(xPos + i * nOff, yPos  + height)),
				MoveTo::create(width*1.0f/speed,Vec2(xPos + i * nOff + width, yPos  + height)),
				NULL
				);
			RepeatForever * repeatForeverAnm_2 = RepeatForever::create(sequence_2);
			particleEffect_2->runAction(repeatForeverAnm_2);
		}
	}
}

