#ifndef  _GENERALSUI_GENERALSUI_H_
#define _GENERALSUI_GENERALSUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
class CActionDetail;
class CGeneralBaseMsg;
class CGeneralDetail;
class GeneralsListUI;
class GeneralsEvolutionUI;
class GeneralsTeachUI;
class GeneralsRecuriteUI;
class SingleRecuriteResultUI;
class TenTimesRecuriteResultUI;
class GeneralsStrategiesUI;
class GeneralsSkillsUI;
class GameFightSkill;

#define  BASERECURITEACTIONITEMTAG 50 
#define  BETTERRECURITEACTIONITEMTAG 51
#define  BESTRECURITEACTIONITEMTAG 52 

#define  TenTimeRecruitResultBaseTag 120

class GeneralsUI : public UIScene
{
public:
	GeneralsUI();
	~GeneralsUI();

	struct ReqListParameter{
		int page;
		int pageSize;
		int type ;
		long long generalId;
	};

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	static GeneralsUI * create();
	bool init();

public:
	Layer * u_layer;
	static Layout * ppanel;

    Button * Button_close;

	//�佫��ļ
	static GeneralsRecuriteUI * generalsRecuriteUI;
	//�佫������ļ����
	static SingleRecuriteResultUI * singleRecuriteResultUI;
	//��佫ʮ����
	static TenTimesRecuriteResultUI * tenTimesRecuriteResultUI;

	//��佫�б
	static GeneralsListUI * generalsListUI;
	//��佫�
	static GeneralsEvolutionUI * generalsEvolutionUI;
	//��佫����
	static GeneralsTeachUI * generalsTeachUI;

	//�
	static GeneralsStrategiesUI * generalsStrategiesUI;

	//��佫���
	static GeneralsSkillsUI * generalsSkillsUI;

	UITab * mainTab;

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void IndexChangedEvent(Ref* pSender);

	//�ÿ�δ򿪶�ͣ����ļ���
	void setToDefaultTab();

	void setToTabByIndex(int idx);

	//�ͨ�Int��͵�Ʒ����ݻ�ȡstring�͵�Ʒ�
	static std::string getQuality(int quality);
	//�ͨ�Int��͵�Ʒ����ݻ�ȡ�佫ͷ�������
	static std::string getBigHeadFramePath(int quality);
	static std::string getSmallHeadFramePath(int quality);

	//ֽ�ѧ
	virtual void registerScriptCommand(int scriptId);
	//�uitab(�㽫̨)��
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	//Ӹuitab(��ҵ��佫)��
	void addCCTutorialIndicator2(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	//Ӹ�رհ�ť��
	void addCCTutorialIndicator3(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	//Ӹuitab(��)���
	void addCCTutorialIndicator4(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

public:
	//ӽ�ѧ
	int mTutorialScriptInstanceId;

};



/////////////////////////////////
/**
 * �佫�����µġ��ҵ��佫���е��佫�б��
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.2
 */

class CGeneralBaseMsg;
class CCMoveableMenu;

class GeneralsListCell : public TableViewCell
{
public:
	GeneralsListCell();
	~GeneralsListCell();
	static GeneralsListCell* create(int index,CGeneralBaseMsg * generalBaseMsg);
	bool init(int index,CGeneralBaseMsg * generalBaseMsg);

	CGeneralBaseMsg* getCurGeneralBaseMsg();

	void RefreshCell(int index,CGeneralBaseMsg * generalBaseMsg);
private:
	void FirstMenuEvent(Ref * pSender);
	void SecondMenuEvent(Ref * pSender);

private:
	CGeneralBaseMsg * curGeneralBaseMsg; 
	int m_index;
	//��ѧ
	int mTutorialScriptInstanceId;
	//��ѧ��һ���ǵڶ��ť
	bool isFirstOrSecond;

	int m_nNowState;						// 1:չʾ��0���ջ

private:
	Sprite * sprite_icon;
	Label * label_level;
	Label * label_name;
	Label * label_rank;

	CCMoveableMenu* firstMenu;
	CCMoveableMenu* secondMenu;
	Label * label_firstMenu;
	Label * label_secondMenu;

	Label * m_label_generalShow;
};


#endif

