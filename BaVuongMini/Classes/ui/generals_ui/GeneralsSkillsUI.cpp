#include "GeneralsSkillsUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GeneralsUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "../../GameView.h"
#include "TeachAndEvolutionCell.h"
#include "NormalGeneralsHeadItem.h"
#include "../../messageclient/element/CFightWayBase.h"
#include "../extensions/UITab.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../gamescene_state/role/GameActor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "GeneralsShortcutSlot.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutUIConstant.h"
#include "../../ui/extensions/ShowSystemInfo.h"

#define  GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG 456
#define  GENERALSSHORTCUTSLOT_TAG 567
#define  SCROLLVIEW_SKILLDESINFO_TAG 678

bool SortGeneralCellData(GeneralSkillCellData * cellData1,GeneralSkillCellData * cellData2)
{
	if (cellData2->curRealLevel < cellData1->curRealLevel)
	{
		return true;
	}
	else if (cellData2->curRealLevel == cellData1->curRealLevel)
	{
		if (cellData1->curGameFightSkill->getCBaseSkill()->usemodel() < cellData2->curGameFightSkill->getCBaseSkill()->usemodel())
		{
			return true;
		}
		else if (cellData1->curGameFightSkill->getCBaseSkill()->usemodel() == cellData2->curGameFightSkill->getCBaseSkill()->usemodel())
		{
			if (cellData1->curGameFightSkill->getCBaseSkill()->get_quality() < cellData2->curGameFightSkill->getCBaseSkill()->get_quality())      
			{
				return true;
			}
			else if (cellData1->curGameFightSkill->getCBaseSkill()->get_quality() < cellData2->curGameFightSkill->getCBaseSkill()->get_quality())
			{
				return cellData1->curGameFightSkill->getCBaseSkill()->get_complex_order() < cellData2->curGameFightSkill->getCBaseSkill()->get_complex_order();
			}
		}
	}
	return false;
}

GeneralsSkillsUI::GeneralsSkillsUI() :
skillType(Warrior),
curSkillId(""),
selectCellId(0),
lastSelectCellId(0)
{
}

GeneralsSkillsUI::~GeneralsSkillsUI()
{
	std::vector<GeneralSkillCellData*>::iterator _iter;
	for (_iter = DataSourceList.begin(); _iter != DataSourceList.end(); ++_iter)
	{
		delete *_iter;
	}
	DataSourceList.clear();
}
GeneralsSkillsUI* GeneralsSkillsUI::create()
{
	auto generalsSkillsUI = new GeneralsSkillsUI();
	if (generalsSkillsUI && generalsSkillsUI->init())
	{
		generalsSkillsUI->autorelease();
		return generalsSkillsUI;
	}
	CC_SAFE_DELETE(generalsSkillsUI);
	return NULL;
}

bool GeneralsSkillsUI::init()
{
	if (UIScene::init())
	{
		panel_skills = (Layout*)Helper::seekWidgetByName(GeneralsUI::ppanel,"Panel_skill");
		panel_skills->setVisible(true);

		Layer_skills = Layer::create();
		addChild(Layer_skills);

		auto btn_null_skillUpLevel = (Button *)Helper::seekWidgetByName(panel_skills,"Button_null_slillUpLevel");
		btn_null_skillUpLevel->setTouchEnabled(true);
		btn_null_skillUpLevel->addTouchEventListener(CC_CALLBACK_2(GeneralsSkillsUI::NullSkillUpEvent, this));

		btn_levelUp = (Button *)Helper::seekWidgetByName(panel_skills,"Button_skillUpgrade");
		btn_levelUp->setTouchEnabled(true);
		btn_levelUp->setPressedActionEnabled(true);
		btn_levelUp->addTouchEventListener(CC_CALLBACK_2(GeneralsSkillsUI::LeveUpEvent, this));

		l_levelUp = (Text*)Helper::seekWidgetByName(panel_skills,"Label_skillUpgrade");

	    i_skillFrame = (ImageView*)Helper::seekWidgetByName(panel_skills,"ImageView_skill_di"); 
		i_skillImage = (ImageView*)Helper::seekWidgetByName(panel_skills,"ImageView_skill"); 
		l_skillName = (Text*)Helper::seekWidgetByName(panel_skills,"Label_SkillName"); 
		l_skillType = (Text*)Helper::seekWidgetByName(panel_skills,"Label_SkillType"); 
		l_skillLvValue = (Text*)Helper::seekWidgetByName(panel_skills,"Label_LvValue"); 
		//l_skillQualityValue = (Text*)Helper::seekWidgetByName(panel_skills,"Label_QualityValue");
		imageView_skillQuality = (ImageView*)Helper::seekWidgetByName(panel_skills,"ImageView_abc");
		l_skillMpValue = (Text*)Helper::seekWidgetByName(panel_skills,"Label_MpValue"); 
		l_skillCDTimeValue = (Text*)Helper::seekWidgetByName(panel_skills,"Label_CDTimeValue"); 
		l_skillDistanceValue = (Text*)Helper::seekWidgetByName(panel_skills,"Label_DistanceValue"); 
		
		const char * normalImage = "res_ui/tab_1_off.png";
		const char * selectImage = "res_ui/tab_1_on.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_1_on.png";

		//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *str1  = ((__String*)strings->objectForKey("profession_mengjiang"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("profession_mengjiang");
		char* p1 =const_cast<char*>(str1);
		//const char *str2  = ((__String*)strings->objectForKey("profession_guimou"))->m_sString.c_str();
		const char *str2 = StringDataManager::getString("profession_guimou");
		char* p2 =const_cast<char*>(str2);
		//const char *str3  = ((__String*)strings->objectForKey("profession_haojie"))->m_sString.c_str();
		const char *str3 = StringDataManager::getString("profession_haojie");
		char* p3 =const_cast<char*>(str3);
		//const char *str4  = ((__String*)strings->objectForKey("profession_shenshe"))->m_sString.c_str();
		const char *str4 = StringDataManager::getString("profession_shenshe");
		char* p4 =const_cast<char*>(str4);

		char * mainNames[] ={p1,p2,p3,p4};
		m_mainTab  = UITab::createWithText(4,normalImage,selectImage,finalImage,mainNames,HORIZONTAL,5);
		m_mainTab->setAnchorPoint(Vec2(0,0));
		m_mainTab->setPosition(Vec2(65,412));
		m_mainTab->setHighLightImage((char * )highLightImage);
		m_mainTab->setDefaultPanelByIndex(0);
		m_mainTab->addIndexChangedEvent(this,coco_indexchangedselector(GeneralsSkillsUI::MainIndexChangedEvent));
		m_mainTab->setPressedActionEnabled(true);
		Layer_skills->addChild(m_mainTab);

		generalsSkillsList_tableView = TableView::create(this,Size(256,360));
// 		generalsSkillsList_tableView ->setSelectedEnable(true);   // ֧��ѡ��״̬����ʾ
// 		generalsSkillsList_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(26, 26, 1, 1), Vec2(0,-1)); 
		//generalsSkillsList_tableView->setPressedActionEnabled(true);
		generalsSkillsList_tableView->setDirection(TableView::Direction::VERTICAL);
		generalsSkillsList_tableView->setAnchorPoint(Vec2(0,0));
		generalsSkillsList_tableView->setPosition(Vec2(72,41));
		generalsSkillsList_tableView->setDelegate(this);
		generalsSkillsList_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		Layer_skills->addChild(generalsSkillsList_tableView);

		Layer_upper = Layer::create();
		addChild(Layer_upper);
// 
// 		ImageView * tableView_kuang = ImageView::create();
// 		tableView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		tableView_kuang->setScale9Enabled(true);
// 		tableView_kuang->setContentSize(Size(243,374));
// 		tableView_kuang->setCapInsets(Rect(30,30,1,1));
// 		tableView_kuang->setAnchorPoint(Vec2(0,0));
// 		tableView_kuang->setPosition(Vec2(64,35));
// 		Layer_upper->addChild(tableView_kuang);

		return true;
	}
	return false;
}

void GeneralsSkillsUI::onEnter()
{
	UIScene::onEnter();
}

void GeneralsSkillsUI::onExit()
{
	UIScene::onExit();
}

void GeneralsSkillsUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void GeneralsSkillsUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

void GeneralsSkillsUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	int i = cell->getIdx();
	curSkillId = DataSourceList.at(i)->curGameFightSkill->getId();

 	selectCellId = cell->getIdx();
// 	Size _s = table->getContentOffset();
// 	//ȡ���ϴ�ѡ��״̬�����±���ѡ��״̬
// 	//table->reloadData();
 	if(table->cellAtIndex(lastSelectCellId))
 	{
 		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG))
 		{
 			table->cellAtIndex(lastSelectCellId)->getChildByTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
 		}
 	}
	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1) , "res_ui/highlight.png");
 	pHighlightSpr->setPreferredSize(Size(225,66));
 	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
 	pHighlightSpr->setTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG);
 	cell->addChild(pHighlightSpr);
 
 	lastSelectCellId = cell->getIdx();
 
// 	table->setContentOffset(_s); 

	RefreshSkillInfo(DataSourceList.at(i)->curGameFightSkill);
	RefreshBtnStatus(DataSourceList.at(i)->curRealLevel);
}

cocos2d::Size GeneralsSkillsUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(225,65);
}

cocos2d::extension::TableViewCell* GeneralsSkillsUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called
	if (!cell)
	{
		cell = GeneralSkillCell::create(DataSourceList.at(idx)); 
		if (this->selectCellId == idx)
		{
			auto tempCell = dynamic_cast<GeneralSkillCell*>(cell);
			if (tempCell)
			{
				tempCell->setSelectSelf(true);
			}			
		}
	}
	else
	{
		auto tempCell = dynamic_cast<GeneralSkillCell*>(cell);
		if (tempCell->getChildByTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG))
		{
			tempCell->getChildByTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
		}
		tempCell->RefreshCell(DataSourceList.at(idx));
		if (this->selectCellId == idx)
		{
			tempCell->setSelectSelf(true);		
		}
	}
	return cell;
}

ssize_t GeneralsSkillsUI::numberOfCellsInTableView( TableView *table )
{
	return DataSourceList.size();
}


void GeneralsSkillsUI::setVisible( bool visible )
{
	panel_skills->setVisible(visible);
	Layer_skills->setVisible(visible);
	Layer_upper->setVisible(visible);
}

void GeneralsSkillsUI::MainIndexChangedEvent( Ref *pSender )
{
	switch((int)m_mainTab->getCurrentIndex())
	{
	case 0 :
		{
			skillType = Warrior;
		}
		break;   
	case 1 :
		{
			skillType = Magic;
		}
		break;
	case 2 :
		{
			skillType = Ranger;
		}
		break;
	case 3 :
		{
			skillType = Warlock;
		}
		break;
	}

	UpdateDataByType(skillType);
	sort(DataSourceList.begin(),DataSourceList.end(),SortGeneralCellData);
	this->generalsSkillsList_tableView->reloadData();
	AutoSetOffSet();
	if (DataSourceList.size() > 0)
	{
		curSkillId = DataSourceList.at(0)->curGameFightSkill->getId();
		this->generalsSkillsList_tableView->cellAtIndex(0);
		RefreshSkillInfo(DataSourceList.at(0)->curGameFightSkill);
		RefreshBtnStatus(DataSourceList.at(0)->curRealLevel);
	}

}

void GeneralsSkillsUI::UpdateDataByType( SkillTpye s_t )
{
	std::vector<GeneralSkillCellData*>::iterator iter;
	for (iter = DataSourceList.begin(); iter != DataSourceList.end(); ++iter)
	{
		delete *iter;
	}
	DataSourceList.clear();

	std::map<std::string, CBaseSkill*>& baseSkillData = StaticDataBaseSkill::s_baseSkillData;
	for (std::map<std::string, CBaseSkill*>::iterator it = baseSkillData.begin(); it != baseSkillData.end(); ++it) 
	{
		auto tempBaseSkill = it->second;
		if (tempBaseSkill->get_quality()>0 && tempBaseSkill->get_profession() == s_t)
		{
			GeneralSkillCellData * cellData = new GeneralSkillCellData();
			cellData->curGameFightSkill->initSkill(tempBaseSkill->id(),tempBaseSkill->id(), 1, GameActor::type_pet);
			cellData->curRealLevel = 0;
			DataSourceList.push_back(cellData);
		}
	}

	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
	{
		for (int i = 0;i<DataSourceList.size();++i)
		{
			auto cellData = DataSourceList.at(i);
			if (strcmp(cellData->curGameFightSkill->getId().c_str(),it->second->getId().c_str()) == 0)
			{
				cellData->curGameFightSkill->initSkill(it->second->getId(),it->second->getId(), it->second->getLevel(), GameActor::type_pet);
				cellData->curRealLevel =  it->second->getLevel();
			}
		}
	}

	/*
	GeneralsUI * generalsui = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
	
	if (s_t == Warrior)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(),it->second->getLevel(), GameActor::type_player);
			
			if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//�������
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 1)//��ͽ�
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
			else
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 1)//�ͽ�
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
		}
	}
	else if(s_t == Magic)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(),it->second->getLevel(), GameActor::type_player);

			if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//�������
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 2)//ܹ�ı
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
			else
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 2)//��ı
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
		}
	}
	else if (s_t == Ranger)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(),it->second->getId(), it->second->getLevel(), GameActor::type_player);

			if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//�������
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 3)//ܺ�
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
			else
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 3)//ܺ�
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
		}
	}
	else if(s_t == Warlock)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			GameFightSkill * gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(),it->second->getLevel(), GameActor::type_player);

			if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//ܱ������
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 4)//����
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
			else
			{
				if (gameFightSkill->getCBaseSkill()->get_profession() == 4)//����
				{
					DataSourceList.push_back(gameFightSkill);
				}
			}
		}
	}
	*/
}

void GeneralsSkillsUI::LeveUpEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (strcmp(curSkillId.c_str(), "") == 0)
		{
			//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *str1  = ((__String*)strings->objectForKey("skillui_peleaseselcetskill"))->m_sString.c_str();
			const char *str1 = StringDataManager::getString("skillui_peleaseselcetskill");
			char* p1 = const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(p1);
		}
		else
		{
			const char * _idx = curSkillId.c_str();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5054, (void *)_idx);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsSkillsUI::setToDefault()
{	
	skillType = Warrior;
	UpdateDataByType(skillType);
	sort(DataSourceList.begin(),DataSourceList.end(),SortGeneralCellData);
	m_mainTab->setDefaultPanelByIndex(0);
	this->generalsSkillsList_tableView->reloadData();
	AutoSetOffSet();
	if (DataSourceList.size() > 0)
	{
		curSkillId = DataSourceList.at(0)->curGameFightSkill->getId();
		this->generalsSkillsList_tableView->cellAtIndex(0);
		RefreshSkillInfo(DataSourceList.at(0)->curGameFightSkill);
		RefreshBtnStatus(DataSourceList.at(0)->curRealLevel);
	}

}

void GeneralsSkillsUI::RefreshData()
{
	UpdateDataByType(skillType);
	sort(DataSourceList.begin(),DataSourceList.end(),SortGeneralCellData);

// 	Size _s = this->generalsSkillsList_tableView->getContentOffset();
// 	//�ȡ���ϴ�ѡ��״̬�����±���ѡ��״̬
// 	this->generalsSkillsList_tableView->reloadData();
// 	this->generalsSkillsList_tableView->setContentOffset(_s); 
	this->generalsSkillsList_tableView->reloadData();
	AutoSetOffSet();

	for (int i = 0;i<DataSourceList.size();++i)
	{
		if (curSkillId == DataSourceList.at(i)->curGameFightSkill->getId())
		{
			RefreshSkillInfo(DataSourceList.at(i)->curGameFightSkill);
			RefreshBtnStatus(DataSourceList.at(i)->curRealLevel);
		}
	}
}

void GeneralsSkillsUI::RefreshSkillInfo( GameFightSkill * gameFightSkill )
{
// 	//i_skillFrame; 
// 	
// 	std::string skillIconPath = "res_ui/jineng_icon/";
// 
// 	if (gameFightSkill->getId() != "")
// 	{
// 		if (strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 			||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 			||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 			||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 		{
// 			skillIconPath.append("jineng_2");
// 		}
// 		else
// 		{
// 			skillIconPath.append(gameFightSkill->getCBaseSkill()->icon());
// 		}
// 
// 		skillIconPath.append(".png");
// 	}
// 	else
// 	{
// 		skillIconPath = "gamescene_state/jinengqu/suibian.png";
// 	}
// 	i_skillImage->loadTexture(skillIconPath.c_str()) ;

	if (Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG))
	{
		Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG)->removeFromParent();
	}

	auto generalsShortCutSlot = GeneralsShortcutSlot::create(gameFightSkill);
	generalsShortCutSlot->setIgnoreAnchorPointForPosition(false);
	generalsShortCutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
	generalsShortCutSlot->setPosition(Vec2(393,355));
	generalsShortCutSlot->setTouchEnabled(false);
	Layer_skills->addChild(generalsShortCutSlot,0,GENERALSSHORTCUTSLOT_TAG);

	l_skillName->setString(gameFightSkill->getCBaseSkill()->name().c_str());

	//�/����
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setString(skillinfo_zhudongjineng);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setString(skillinfo_zhudongjineng);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setString(skillinfo_zhudongjineng);
	}

	char s_level[20];
	sprintf(s_level,"%d",gameFightSkill->getCFightSkill()->level());
	l_skillLvValue->setString(s_level);
	//l_skillQualityValue->setText("");
	switch(gameFightSkill->getCBaseSkill()->get_quality())
	{
	case 1:
		{
			imageView_skillQuality->loadTexture("res_ui/o_white.png");
		}
		break;
	case 2:
		{
			imageView_skillQuality->loadTexture("res_ui/o_green.png");
		}
		break;
	case 3:
		{
			imageView_skillQuality->loadTexture("res_ui/o_blue.png");
		}
		break;
	case 4:
		{
			imageView_skillQuality->loadTexture("res_ui/o_purple.png");
		}
		break;
	case 5:
		{
			imageView_skillQuality->loadTexture("res_ui/o_orange.png");
		}
		break;
	default:
		imageView_skillQuality->loadTexture("res_ui/o_white.png");
	}
	char s_mp[20];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	l_skillMpValue->setString(s_mp);
	char s_intervaltime[20];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	l_skillCDTimeValue->setString(s_intervaltime);
	char s_distance[20];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	l_skillDistanceValue ->setString(s_distance);

	refreshSkillDes(gameFightSkill->getCFightSkill()->description(),gameFightSkill->getCFightSkill()->get_next_description());

	if(Layer_skills->getChildByName("panel_levelInfo"))
	{
		Layer_skills->getChildByName("panel_levelInfo")->removeFromParent();
	}
	auto panel_levelInfo  = Layout::create();
	Layer_skills->addChild(panel_levelInfo);
	panel_levelInfo->setName("panel_levelInfo");

	/////////////////////////////////////////////////////
	int m_required_level = 0;
	int m_required_point = 0;
	std::string m_str_pre_skill = "";
	std::string m_pre_skill_id = "";
	int m_pre_skill_level = 0;
	int m_required_gold= 0;
	int m_required_skillBookValue = 0;
	if (gameFightSkill->getLevel() == 0)
	{
		//�ȼ�
		if (gameFightSkill->getCFightSkill()->get_required_level()>0)
		{
			m_required_level = gameFightSkill->getCFightSkill()->get_required_level();
		}
		//���ܵ
		if (gameFightSkill->getCFightSkill()->get_required_point()>0)
		{
			m_required_point = gameFightSkill->getCFightSkill()->get_required_point();
		}
		//XXX㼼�
		if (gameFightSkill->getCFightSkill()->get_pre_skill() != "")
		{
			auto temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_pre_skill()];
			m_str_pre_skill.append(temp->name().c_str());
			m_pre_skill_id.append(temp->id());

			m_pre_skill_level = gameFightSkill->getCFightSkill()->get_pre_skill_level();
		}
		//ܽ�
		if (gameFightSkill->getCFightSkill()->get_required_gold()>0)
		{
			m_required_gold = gameFightSkill->getCFightSkill()->get_required_gold();
		}
		//Ҽ����
		if (gameFightSkill->getCFightSkill()->get_required_num()>0)
		{
			m_required_skillBookValue = gameFightSkill->getCFightSkill()->get_required_num();
		}
	}
	else
	{
		//�ȼ�
		if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
		{
			m_required_level = gameFightSkill->getCFightSkill()->get_next_required_level();
		}
		//���ܵ
		if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
		{
			m_required_point = gameFightSkill->getCFightSkill()->get_next_required_point();
		}
		//XXX㼼�
		if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
		{
			auto temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
			m_str_pre_skill.append(temp->name());
			m_pre_skill_id.append(temp->id());

			m_pre_skill_level = gameFightSkill->getCFightSkill()->get_next_pre_skill_level();
		}
		//ܽ�
		if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
		{
			m_required_gold = gameFightSkill->getCFightSkill()->get_next_required_gold();
		}
		//ҽ����
		if (gameFightSkill->getCFightSkill()->get_next_required_num()>0)
		{
			m_required_skillBookValue = gameFightSkill->getCFightSkill()->get_next_required_num();
		}
	}

	/////////////////////////////////////////////////////
	int lineNum = 0;
	//���ǵȼ�
	if (m_required_level>0)
	{
		lineNum++;
		auto l_playerLv = Label::createWithTTF(StringDataManager::getString("skillinfo_zhujuedengji"), APP_FONT_NAME, 16);
		l_playerLv->setColor(Color3B(47,93,13));
		l_playerLv->setAnchorPoint(Vec2(0,0));
		l_playerLv->setPosition(Vec2(330,118-lineNum*20));
		l_playerLv->setName("l_playerLv");
		panel_levelInfo->addChild(l_playerLv);

		auto l_playerLv_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_playerLv_colon->setColor(Color3B(47,93,13));
		l_playerLv_colon->setAnchorPoint(Vec2(0,0));
		l_playerLv_colon->setPosition(Vec2(394,118-lineNum*20));
		l_playerLv_colon->setName("l_playerLv_colon");
		panel_levelInfo->addChild(l_playerLv_colon);

		char s_next_required_level[20];
		sprintf(s_next_required_level,"%d",m_required_level);
		auto l_playerLvValue = Label::createWithTTF(s_next_required_level, APP_FONT_NAME, 16);
		l_playerLvValue->setColor(Color3B(35,93,13));
		l_playerLvValue->setAnchorPoint(Vec2(0,0));
		l_playerLvValue->setPosition(Vec2(404,118-lineNum*20));
		panel_levelInfo->addChild(l_playerLvValue);
		// 
		if (GameView::getInstance()->myplayer->getActiveRole()->level()<m_required_level)  //����ȼ�δ�ﵽ������Ҫ�
		{
		 	l_playerLv->setColor(Color3B(212,59,59));
			l_playerLv_colon->setColor(Color3B(212,59,59));
		 	l_playerLvValue->setColor(Color3B(212,59,59));
		}
	}
	//��ܵ
	if (m_required_point>0)
	{
		lineNum++;

		auto widget_skillPoint = Widget::create();
		panel_levelInfo->addChild(widget_skillPoint);
		widget_skillPoint->setName("widget_skillPoint");
		widget_skillPoint->setAnchorPoint(Vec2(0,0));
		widget_skillPoint->setPosition(Vec2(330,118-lineNum*20));

		std::string str_skillPoint = StringDataManager::getString("skillinfo_jinengdian");
		for(int i = 0;i<3;i++)
		{
			std::string str_name = "l_skillPoint";
			char s_index[5];
			sprintf(s_index,"%d",i);
			str_name.append(s_index);
			auto l_skillPoint = Label::createWithTTF(str_skillPoint.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
			l_skillPoint->setColor(Color3B(47,93,13));
			l_skillPoint->setAnchorPoint(Vec2(0,0));
			l_skillPoint->setName(str_name.c_str());
			widget_skillPoint->addChild(l_skillPoint);
			l_skillPoint->setPosition(Vec2(24*i,0));
		}

		auto l_skillPoint_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_skillPoint_colon->setColor(Color3B(47,93,13));
		l_skillPoint_colon->setAnchorPoint(Vec2(0,0));
		l_skillPoint_colon->setPosition(Vec2(394,118-lineNum*20));
		l_skillPoint_colon->setName("l_skillPoint_colon");
		panel_levelInfo->addChild(l_skillPoint_colon);

		char s_required_point[20];
		sprintf(s_required_point,"%d",m_required_point);
		auto l_skillPointValue = Label::createWithTTF(s_required_point, APP_FONT_NAME, 16);
		l_skillPointValue->setColor(Color3B(47,93,13));
		l_skillPointValue->setAnchorPoint(Vec2(0,0));
		l_skillPointValue->setPosition(Vec2(404,118-lineNum*20));
		panel_levelInfo->addChild(l_skillPointValue);

		if(GameView::getInstance()->myplayer->player->skillpoint()<m_required_point)//㼼�ܵ㲻�
		{
			for(int i = 0;i<3;i++)
			{
				std::string str_name = "l_skillPoint";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				Label * l_skillPoint = (Label*)widget_skillPoint->getChildByName(str_name.c_str());
				if (l_skillPoint)
				{
					l_skillPoint->setColor(Color3B(212,59,59));
				}
			}
			l_skillPoint_colon->setColor(Color3B(212,59,59));
			l_skillPointValue->setColor(Color3B(212,59,59));
		}
	}
	//XXX㼼�
	if (m_str_pre_skill != "")
	{
		lineNum++;
		auto widget_pre_skill = Widget::create();
		panel_levelInfo->addChild(widget_pre_skill);
		widget_pre_skill->setName("widget_pre_skill");
		widget_pre_skill->setAnchorPoint(Vec2(0,0));
		widget_pre_skill->setPosition(Vec2(330,118-lineNum*20));
		if (m_str_pre_skill.size()/3 < 4)
		{
			for(int i = 0;i<m_str_pre_skill.size()/3;i++)
			{
				std::string str_name = "l_preSkill";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);

				auto l_preSkill = Label::createWithTTF(m_str_pre_skill.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
				l_preSkill->setColor(Color3B(47,93,13));
				l_preSkill->setAnchorPoint(Vec2(0,0));
				widget_pre_skill->addChild(l_preSkill);
				l_preSkill->setName(str_name.c_str());
				if (m_str_pre_skill.size()/3 == 1)
				{
					l_preSkill->setPosition(Vec2(0,0));
				}
				else if (m_str_pre_skill.size()/3 == 2)
				{
					l_preSkill->setPosition(Vec2(48*i,0));
				}
				else if (m_str_pre_skill.size()/3 == 3)
				{
					l_preSkill->setPosition(Vec2(24*i,0));
				}
			}
		}
		else
		{
			auto l_preSkill = Label::createWithTTF(m_str_pre_skill.c_str(), APP_FONT_NAME, 16);
			l_preSkill->setColor(Color3B(47,93,13));
			l_preSkill->setAnchorPoint(Vec2(0,0));
			l_preSkill->setPosition(Vec2(0,0));
			widget_pre_skill->addChild(l_preSkill);
			l_preSkill->setName("l_preSkill");
		}

		auto l_preSkill_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_preSkill_colon->setColor(Color3B(47,93,13));
		l_preSkill_colon->setAnchorPoint(Vec2(0,0));
		l_preSkill_colon->setPosition(Vec2(394,118-lineNum*20));
		l_preSkill_colon->setName("l_preSkill_colon");
		panel_levelInfo->addChild(l_preSkill_colon);

		char s_pre_skill_level[20];
		sprintf(s_pre_skill_level,"%d",m_pre_skill_level);
		auto l_preSkillValue = Label::createWithTTF(s_pre_skill_level, APP_FONT_NAME, 16);
		l_preSkillValue->setColor(Color3B(47,93,13));
		l_preSkillValue->setAnchorPoint(Vec2(0,0));
		l_preSkillValue->setPosition(Vec2(404,118-lineNum*20));
		l_preSkillValue->setName("l_preSkillValue");
		panel_levelInfo->addChild(l_preSkillValue);

		std::map<std::string,GameFightSkill*>::const_iterator cIter;
		cIter = GameView::getInstance()->GameFightSkillList.find(m_pre_skill_id);
		if (cIter == GameView::getInstance()->GameFightSkillList.end()) // �û�ҵ�����ָ�END��  
		{
		}
		else
		{
			auto gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[m_pre_skill_id];
			if (gameFightSkillStuded)
			{
				if (gameFightSkillStuded->getLevel() < m_pre_skill_level)
				{
					if (m_str_pre_skill.size()/3 < 4)
					{
						for(int i = 0;i<m_str_pre_skill.size()/3;i++)
						{
							std::string str_name = "l_preSkill";
							char s_index[5];
							sprintf(s_index,"%d",i);
							str_name.append(s_index);

							auto l_preSkill = (Label*)widget_pre_skill->getChildByName(str_name.c_str());
							if (l_preSkill)
							{
								l_preSkill->setColor(Color3B(212,59,59));
							}
						}
					}
					else
					{
						auto l_preSkill = (Label*)widget_pre_skill->getChildByName("l_preSkill");
						if (l_preSkill)
						{
							l_preSkill->setColor(Color3B(212,59,59));
						}
					}
					l_preSkill_colon->setColor(Color3B(212,59,59));
					l_preSkillValue->setColor(Color3B(212,59,59));
				}
			}
		}
	}
	//˽�
	if (m_required_gold>0)
	{
		lineNum++;
		 
		auto widget_gold = Widget::create();
		panel_levelInfo->addChild(widget_gold);
		widget_gold->setPosition(Vec2(330,118-lineNum*20));

		std::string str_gold = StringDataManager::getString("skillinfo_jinbi");
		for(int i = 0;i<2;i++)
		{
			std::string str_name = "l_gold";
			char s_index[5];
			sprintf(s_index,"%d",i);
			str_name.append(s_index);
			auto l_gold = Label::createWithTTF(str_gold.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
			l_gold->setColor(Color3B(47,93,13));
			l_gold->setAnchorPoint(Vec2(0,0));
			l_gold->setName(str_name.c_str());
			widget_gold->addChild(l_gold);
			l_gold->setPosition(Vec2(48*i,0));
		}

		auto l_gold_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_gold_colon->setColor(Color3B(47,93,13));
		l_gold_colon->setAnchorPoint(Vec2(0,0));
		l_gold_colon->setPosition(Vec2(394,118-lineNum*20));
		l_gold_colon->setName("l_gold_colon");
		panel_levelInfo->addChild(l_gold_colon);

		char s_required_gold[20];
		sprintf(s_required_gold,"%d",m_required_gold);
		auto l_goldValue = Label::createWithTTF(s_required_gold, APP_FONT_NAME, 16);
		l_goldValue->setColor(Color3B(47,93,13));
		l_goldValue->setAnchorPoint(Vec2(0,0));
		l_goldValue->setPosition(Vec2(404,118-lineNum*20));
		panel_levelInfo->addChild(l_goldValue);

 	 	if(GameView::getInstance()->getPlayerGold()<m_required_gold)//ҽ�Ҳ��
 	 	{
			for(int i = 0;i<2;i++)
			{
				std::string str_name = "l_gold";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				auto l_gold = (Label*)widget_gold->getChildByName(str_name.c_str());
				if (l_gold)
				{
					l_gold->setColor(Color3B(212,59,59));
				}
			}
			l_gold_colon->setColor(Color3B(212,59,59));
 	 		l_goldValue->setColor(Color3B(212,59,59));
 	 	}
	}
	//㼼���
	if (m_required_skillBookValue>0)
	{
		lineNum++;

		auto widget_skillBook = Widget::create();
		panel_levelInfo->addChild(widget_skillBook);
		widget_skillBook->setName("widget_skillBook");
		widget_skillBook->setPosition(Vec2(330,118-lineNum*20));

		std::string str_skillbook = GeneralsSkillsUI::getSkillQualityStr(gameFightSkill->getCBaseSkill()->get_quality());
		str_skillbook.append(StringDataManager::getString("skillinfo_jinengshu"));
		auto l_skillbook = Label::createWithTTF(str_skillbook.c_str(), APP_FONT_NAME, 16);
		l_skillbook->setColor(Color3B(47,93,13));
		l_skillbook->setAnchorPoint(Vec2(0,0));
		l_skillbook->setName("l_skillbook");
		widget_skillBook->addChild(l_skillbook);
		l_skillbook->setPosition(Vec2(0,0));

		auto l_skillbook_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_skillbook_colon->setColor(Color3B(47,93,13));
		l_skillbook_colon->setAnchorPoint(Vec2(0,0));
		l_skillbook_colon->setPosition(Vec2(394,118-lineNum*20));
		l_skillbook_colon->setName("l_skillbook_colon");
		panel_levelInfo->addChild(l_skillbook_colon);

		char s_required_skillBook[20];
		sprintf(s_required_skillBook,"%d",m_required_skillBookValue);
		auto l_skillBookValue = Label::createWithTTF(s_required_skillBook, APP_FONT_NAME, 16);
		l_skillBookValue->setColor(Color3B(47,93,13));
		l_skillBookValue->setAnchorPoint(Vec2(0,0));
		l_skillBookValue->setPosition(Vec2(404,118-lineNum*20));
		panel_levelInfo->addChild(l_skillBookValue);

		int propNum = 0;
		for (int i = 0;i<GameView::getInstance()->AllPacItem.size();++i)
		{
			auto folder = GameView::getInstance()->AllPacItem.at(i);
			if (!folder->has_goods())
				continue;

			if (folder->goods().id() == gameFightSkill->getCFightSkill()->get_required_prop())
			{
				propNum += folder->quantity();
			}
		}
		if(propNum<gameFightSkill->getCFightSkill()->get_required_num())//鼼���鲻�
		{
			l_skillbook->setColor(Color3B(212,59,59));
			l_skillbook_colon->setColor(Color3B(212,59,59));
			l_skillBookValue->setColor(Color3B(212,59,59));
		}
	}
}

std::string GeneralsSkillsUI::getCurSkillId()
{
	return curSkillId;
}

void GeneralsSkillsUI::RefreshBtnStatus( int curRealLevel )
{
	if (curRealLevel > 0)
	{
		l_levelUp->setString(StringDataManager::getString("generals_strategies_upgrade"));
	}
	else
	{
		l_levelUp->setString(StringDataManager::getString("generals_strategies_study"));
	}
}

void GeneralsSkillsUI::AutoSetOffSet()
{
	int selectIndex = -1;
	for (int i = 0;i<DataSourceList.size();i++)
	{
		if (curSkillId == DataSourceList.at(i)->curGameFightSkill->getId())
		{
			selectIndex = i;
			break;
		}
	}

	if (selectIndex == -1)
		return;

	//strategiesList_tableView->selectCell(selectIndex);
	this->generalsSkillsList_tableView->cellAtIndex(selectIndex);

	if (DataSourceList.size()*tableCellSizeForIndex(generalsSkillsList_tableView,0).height < generalsSkillsList_tableView->getViewSize().height)
	{
	}
	else
	{
		if ((DataSourceList.size()-selectIndex)*tableCellSizeForIndex(generalsSkillsList_tableView,0).height  <= generalsSkillsList_tableView->getViewSize().height)  //�����󼸸
		{
			generalsSkillsList_tableView->setContentOffset(Vec2(0,0));
		}
		else
		{
			int beginHeight = generalsSkillsList_tableView->getViewSize().height - DataSourceList.size()*tableCellSizeForIndex(generalsSkillsList_tableView,0).height;
			generalsSkillsList_tableView->setContentOffset(Vec2(0,beginHeight+selectIndex*tableCellSizeForIndex(generalsSkillsList_tableView,0).height));
		}
	}
}

void GeneralsSkillsUI::NullSkillUpEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		std::map<int, std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(19);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end())
		{
			return;
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			{
				auto winSize = Director::getInstance()->getVisibleSize();
				auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[19].c_str());
				GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				showSystemInfo->setIgnoreAnchorPointForPosition(false);
				showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				showSystemInfo->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsSkillsUI::refreshSkillDes( std::string curSkillDes,std::string nextSkillDes )
{
	//skill des text
	if (Layer_skills->getChildByTag(SCROLLVIEW_SKILLDESINFO_TAG))
	{
		auto scroll_ = (cocos2d::extension::ScrollView *)Layer_skills->getChildByTag(SCROLLVIEW_SKILLDESINFO_TAG);
		scroll_->removeFromParentAndCleanup(true);
	}

	auto scrollView_info = ui::ScrollView::create();
	scrollView_info->setTouchEnabled(true);
	scrollView_info->setDirection(ui::ScrollView::Direction::VERTICAL);
	scrollView_info->setContentSize(Size(264,303));
	scrollView_info->setPosition(Vec2(495,97));
	scrollView_info->setBounceEnabled(true);
	scrollView_info->setTag(SCROLLVIEW_SKILLDESINFO_TAG);
	Layer_skills->addChild(scrollView_info);

	auto color_ = Color3B(47,93,13);

	auto mo_1 = ImageView::create();
	mo_1->loadTexture("res_ui/mo_2.png");
	mo_1->setAnchorPoint(Vec2(0,0));
	mo_1->setScale(0.7f);
	scrollView_info->addChild(mo_1);

	auto labelBM_1 = Label::createWithBMFont("res_ui/font/ziti_3.fnt", StringDataManager::getString("robotui_skill_des_setSkill"));
	labelBM_1->setAnchorPoint(Vec2(0,0));
	scrollView_info->addChild(labelBM_1);
	auto label_des=Text::create(curSkillDes.c_str(), APP_FONT_NAME, 18);
	label_des->setAnchorPoint(Vec2(0,0));
	label_des->setColor(color_);
	label_des->setTextAreaSize(Size(240,0));
	scrollView_info->addChild(label_des);

	auto mo_2 = ImageView::create();
	mo_2->loadTexture("res_ui/mo_2.png");
	mo_2->setAnchorPoint(Vec2(0,0));
	mo_2->setScale(0.7f);
	scrollView_info->addChild(mo_2);

	auto labelBM_2 = Label::createWithBMFont("res_ui/font/ziti_3.fnt", StringDataManager::getString("robotui_nextSkill_des_setSkill"));
	labelBM_2->setAnchorPoint(Vec2(0,0));
	scrollView_info->addChild(labelBM_2);
	auto label_next_des=Text::create(nextSkillDes.c_str(), APP_FONT_NAME, 18);
	label_next_des->setAnchorPoint(Vec2(0,0));
	label_next_des->setColor(color_);
	label_next_des->setTextAreaSize(Size(240,0));
	scrollView_info->addChild(label_next_des);

	int h_ = mo_1->getContentSize().height * 2;
	int temp_min_h = 100;

	if (label_des->getContentSize().height > temp_min_h)
	{
		h_+= label_des->getContentSize().height;
	}else
	{
		h_+= temp_min_h;
	}

	if (label_next_des->getContentSize().height > temp_min_h)
	{
		h_+= label_next_des->getContentSize().height;
	}else
	{
		h_+= temp_min_h;
	}


	int innerWidth = scrollView_info->getBoundingBox().size.width;
	int innerHeight = h_;
	if (h_ < scrollView_info->getBoundingBox().size.height)
	{
		innerHeight = scrollView_info->getBoundingBox().size.height;
	}

	scrollView_info->setInnerContainerSize(Size(innerWidth,innerHeight));

	mo_1->setPosition(Vec2(0,innerHeight - mo_1->getContentSize().height ));
	labelBM_1->setPosition(Vec2(10,mo_1->getPosition().y+2));
	label_des->setPosition(Vec2(0,mo_1->getPosition().y - label_des->getContentSize().height));

	mo_2->setPosition(Vec2(0,label_des->getPosition().y - 30));
	labelBM_2->setPosition(Vec2(10,mo_2->getPosition().y+2));
	label_next_des->setPosition(Vec2(0,mo_2->getPosition().y - label_next_des->getContentSize().height));
}

std::string GeneralsSkillsUI::getSkillQualityStr( int skillQuality )
{
	std::string temp = "";
	switch(skillQuality)
	{
	case 0:
		{
			temp.append(StringDataManager::getString("generals_skill_white"));
		}
		break;
	case 1:
		{
			temp.append(StringDataManager::getString("generals_skill_white"));
		}
		break;
	case 2:
		{
			temp.append(StringDataManager::getString("generals_skill_green"));
		}
		break;
	case 3:
		{
			temp.append(StringDataManager::getString("generals_skill_blue"));
		}
		break;
	case 4:
		{
			temp.append(StringDataManager::getString("generals_skill_purple"));
		}
		break;
	case 5:
		{
			temp.append(StringDataManager::getString("generals_skill_orange"));
		}
		break;
	default:
		temp.append(StringDataManager::getString("generals_skill_white"));
	}

	return temp;
}

//////////////////////////////////////////////////////////////////////////////

GeneralSkillCellData::GeneralSkillCellData()
{
	curGameFightSkill = new GameFightSkill();
}

GeneralSkillCellData::~GeneralSkillCellData()
{
	CC_SAFE_DELETE(curGameFightSkill);
}


//////////////////////////////////////////////////////////////////////////////

#define  KTAG_Sprite_SkillVariety 50

GeneralSkillCell::GeneralSkillCell()
{

}

GeneralSkillCell::~GeneralSkillCell()
{
}

GeneralSkillCell* GeneralSkillCell::create( GeneralSkillCellData * cellData )
{
	auto generalSkillCell = new GeneralSkillCell();
	if(generalSkillCell && generalSkillCell->init(cellData))
	{
		generalSkillCell->autorelease();
		return generalSkillCell;
	}
	CC_SAFE_DELETE(generalSkillCell);
}

bool GeneralSkillCell::init( GeneralSkillCellData * cellData )
{
	if(TableViewCell::init())
	{
		auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang01_new.png");
		sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
		sprite_bigFrame->setPosition(Vec2(0, 0));
		sprite_bigFrame->setContentSize(Size(225,63));
		sprite_bigFrame->setCapInsets(Rect(15,31,1,1));
		this->addChild(sprite_bigFrame);

		//�С�߿
		auto  smaillFrame = Sprite::create("res_ui/jinengdi.png");
		smaillFrame->setAnchorPoint(Vec2(0, 0));
		smaillFrame->setPosition(Vec2(19,5));
		smaillFrame->setScale(0.91f);
		this->addChild(smaillFrame);

		auto sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_diaa.png");
		sprite_nameFrame->setAnchorPoint(Vec2(0, 0));
		sprite_nameFrame->setPosition(Vec2(83, 17));
		sprite_nameFrame->setCapInsets(Rect(11,11,1,1));
		sprite_nameFrame->setContentSize(Size(130,30));
		sprite_bigFrame->addChild(sprite_nameFrame);

		std::string skillIconPath = "res_ui/jineng_icon/";

		if (cellData->curGameFightSkill->getId() != "")
		{
			skillIconPath.append(cellData->curGameFightSkill->getCBaseSkill()->icon());
			skillIconPath.append(".png");
		}
		else
		{
			skillIconPath = "gamescene_state/zhujiemian3/jinengqu/suibian.png";
		}

		auto baseSkill = StaticDataBaseSkill::s_baseSkillData[cellData->curGameFightSkill->getId()];
		CCAssert(baseSkill != NULL, "baseSkill should not be null");

		sprite_IconImage = Sprite::create(skillIconPath.c_str());
		if(sprite_IconImage != NULL)
		{
			sprite_IconImage->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_IconImage->setPosition(Vec2(45,31));
			this->addChild(sprite_IconImage);

			auto  sprite_lvFrame = Sprite::create("res_ui/jineng/jineng_zhezhao.png");
			sprite_lvFrame->setAnchorPoint(Vec2(0.5f,0.f));
			sprite_lvFrame->setPosition(Vec2(45/2+1,0));
			sprite_IconImage->addChild(sprite_lvFrame);

			std::string str_lv = "";
			char s_curLv [5];
			sprintf(s_curLv,"%d",cellData->curGameFightSkill->getLevel());
			str_lv.append(s_curLv);
			str_lv.append("/");
			char max_lv [5];
			sprintf(max_lv,"%d",cellData->curGameFightSkill->getCBaseSkill()->maxlevel());
			str_lv.append(max_lv);

			l_lv = Label::createWithTTF(str_lv.c_str(),APP_FONT_NAME,12);
			l_lv->setAnchorPoint(Vec2(0.5f,0.f));
			l_lv->setPosition(Vec2(sprite_lvFrame->getPosition().x,sprite_lvFrame->getPosition().y));
			sprite_lvFrame->addChild(l_lv);
		}

		// 	Sprite * sprite_highLight = Sprite::create(HIGHLIGHT1);
		// 	sprite_highLight->setAnchorPoint(Vec2(0.5f, 0.5f));
		// 	sprite_highLight->setPosition(Vec2(27,28));
		// 	sprite_frame->addChild(sprite_highLight);

		//skill variety icon
		if (this->getChildByTag(KTAG_Sprite_SkillVariety))
		{
			this->getChildByTag(KTAG_Sprite_SkillVariety)->removeFromParent();
		}
		if (baseSkill->usemodel() == 0)
		{
			auto sprite_skillVariety = SkillScene::GetSkillVarirtySprite(baseSkill->id().c_str());
			if (sprite_skillVariety)
			{
				sprite_skillVariety->setAnchorPoint(Vec2(0.5f,0.5f));
				sprite_skillVariety->setPosition(Vec2(60,29));
				sprite_skillVariety->setScale(.85f);
				this->addChild(sprite_skillVariety);
				sprite_skillVariety->setTag(KTAG_Sprite_SkillVariety);
			}
		}

		label_skillName = Label::createWithTTF(cellData->curGameFightSkill->getCBaseSkill()->name().c_str(),APP_FONT_NAME,18);
		label_skillName->setAnchorPoint(Vec2(0, 0.5f));
		label_skillName->setPosition(Vec2(100,31));
		// 	Color3B shadowColor = Color3B(0,0,0);   // black
		// 	label_skillName->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		this->addChild(label_skillName);

		if (baseSkill->get_quality() == 1)
		{
			label_skillName->setColor(Color3B(255,255,255));
		}
		else if (baseSkill->get_quality()  == 2)
		{
			label_skillName->setColor(Color3B(0,255,0));
		}
		else if (baseSkill->get_quality()  == 3)
		{
			label_skillName->setColor(Color3B(0,255,255));
		}
		else if (baseSkill->get_quality()  == 4)
		{
			label_skillName->setColor(Color3B(255,0,228));
		}
		else if (baseSkill->get_quality()  == 5)
		{
			label_skillName->setColor(Color3B(254,124,0));
		}
		else
		{
			label_skillName->setColor(Color3B(255,255,255));
		}

//  	 	if (this->selectCellId == idx)
//  	 	{
//  	 		cocos2d::extension::Scale9Sprite* pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1) , "res_ui/highlight.png");
//  	 		pHighlightSpr->setPreferredSize(Size(225,66));
//  	 		pHighlightSpr->setAnchorPoint(Vec2::ZERO);
//  	 		pHighlightSpr->setTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG);
//  	 		this->addChild(pHighlightSpr);
//  	 	}

		sprite_blackFrame = Sprite::create(PROGRESS_SKILL);
		sprite_blackFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_blackFrame->setPosition(Vec2(45,31));
		sprite_blackFrame->setScale(1.0f);
		this->addChild(sprite_blackFrame,1);

		auto sprite_lock = Sprite::create("res_ui/suo_2.png");
		sprite_lock->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_lock->setPosition(Vec2(sprite_blackFrame->getContentSize().width/2,sprite_blackFrame->getContentSize().height/2));
		sprite_lock->setScale(0.85f);
		sprite_blackFrame->addChild(sprite_lock);

		if (cellData->curRealLevel > 0)
		{
			sprite_blackFrame->setVisible(false);
		}
		else
		{
			sprite_blackFrame->setVisible(true);
		}

		return true;
	}
	return false;
}

void GeneralSkillCell::RefreshCell( GeneralSkillCellData * cellData )
{
	std::string skillIconPath = "res_ui/jineng_icon/";

	if (cellData->curGameFightSkill->getId() != "")
	{
		skillIconPath.append(cellData->curGameFightSkill->getCBaseSkill()->icon());
		skillIconPath.append(".png");
	}
	else
	{
		skillIconPath = "gamescene_state/zhujiemian3/jinengqu/suibian.png";
	}

	auto baseSkill = StaticDataBaseSkill::s_baseSkillData[cellData->curGameFightSkill->getId()];
	CCAssert(baseSkill != NULL, "baseSkill should not be null");

	auto texture = Director::getInstance()->getTextureCache()->addImage(skillIconPath.c_str());
	sprite_IconImage->setTexture(texture);
	std::string str_lv = "";
	char s_curLv [5];
	sprintf(s_curLv,"%d",cellData->curGameFightSkill->getLevel());
	str_lv.append(s_curLv);
	str_lv.append("/");
	char max_lv [5];
	sprintf(max_lv,"%d",cellData->curGameFightSkill->getCBaseSkill()->maxlevel());
	str_lv.append(max_lv);
	l_lv->setString(str_lv.c_str());

	//skill variety icon
	if (this->getChildByTag(KTAG_Sprite_SkillVariety))
	{
		this->getChildByTag(KTAG_Sprite_SkillVariety)->removeFromParent();
	}
	if (baseSkill->usemodel() == 0)
	{
		auto sprite_skillVariety = SkillScene::GetSkillVarirtySprite(baseSkill->id().c_str());
		if (sprite_skillVariety)
		{
			sprite_skillVariety->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_skillVariety->setPosition(Vec2(60,29));
			sprite_skillVariety->setScale(.85f);
			this->addChild(sprite_skillVariety);
			sprite_skillVariety->setTag(KTAG_Sprite_SkillVariety);
		}
	}

	label_skillName->setString(cellData->curGameFightSkill->getCBaseSkill()->name().c_str());
	if (baseSkill->get_quality() == 1)
	{
		label_skillName->setColor(Color3B(255,255,255));
	}
	else if (baseSkill->get_quality()  == 2)
	{
		label_skillName->setColor(Color3B(0,255,0));
	}
	else if (baseSkill->get_quality()  == 3)
	{
		label_skillName->setColor(Color3B(0,255,255));
	}
	else if (baseSkill->get_quality()  == 4)
	{
		label_skillName->setColor(Color3B(255,0,228));
	}
	else if (baseSkill->get_quality()  == 5)
	{
		label_skillName->setColor(Color3B(254,124,0));
	}
	else
	{
		label_skillName->setColor(Color3B(255,255,255));
	}

	if (cellData->curRealLevel > 0)
	{
		sprite_blackFrame->setVisible(false);
	}
	else
	{
		sprite_blackFrame->setVisible(true);
	}

	this->setScale(1.0f);
}

void GeneralSkillCell::setSelectSelf( bool isvisible )
{
	if (isvisible)
	{
		auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1) , "res_ui/highlight.png");
		pHighlightSpr->setPreferredSize(Size(225,66));
		pHighlightSpr->setAnchorPoint(Vec2::ZERO);
		pHighlightSpr->setTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG);
		this->addChild(pHighlightSpr);
	}
	else
	{
		if (this->getChildByTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG))
		{
			this->getChildByTag(GENERALSSKILLSUI_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
		}
	}
}
