#include "TenTimesRecuriteResultUI.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "GeneralsUI.h"
#include "GameView.h"
#include "../../messageclient/element/CActionDetail.h"
#include "GeneralsHeadItemBase.h"
#include "GeneralsRecuriteUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "GeneralsListUI.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "RecuriteActionItem.h"
#include "../../ui/extensions/RecruitGeneralCard.h"
#include "AppMacros.h"


TenTimesRecuriteResultUI::TenTimesRecuriteResultUI()
{
}


TenTimesRecuriteResultUI::~TenTimesRecuriteResultUI()
{
}

TenTimesRecuriteResultUI* TenTimesRecuriteResultUI::create()
{
	auto tenTimesRecuriteResultUI = new TenTimesRecuriteResultUI();
	if (tenTimesRecuriteResultUI && tenTimesRecuriteResultUI->init())
	{
		tenTimesRecuriteResultUI->autorelease();
		return tenTimesRecuriteResultUI;
	}
	CC_SAFE_DELETE(tenTimesRecuriteResultUI);
	return NULL;
}

bool TenTimesRecuriteResultUI::init()
{
	if (UIScene::init())
	{
		auto winSize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winSize);
		mengban->setAnchorPoint(Vec2(0.5f,0.5f));
		mengban->setPosition(Vec2(722/2,438/2));
		m_pLayer->addChild(mengban);

		//���UI
		if(LoadSceneLayer::TenTimesRecuriteResultLayer->getParent() != NULL)
		{
			LoadSceneLayer::TenTimesRecuriteResultLayer->removeFromParentAndCleanup(false);
		}
		//�ʮ�������
		panel_tenTimesRecruit = LoadSceneLayer::TenTimesRecuriteResultLayer;
		panel_tenTimesRecruit->setAnchorPoint(Vec2(0.0f,0.0f));
		panel_tenTimesRecruit->getVirtualRenderer()->setContentSize(Size(722, 438));
		panel_tenTimesRecruit->setPosition(Vec2::ZERO);
		panel_tenTimesRecruit->setTouchEnabled(true);
		m_pLayer->addChild(panel_tenTimesRecruit);

		panel_btn = (Layout*)Helper::seekWidgetByName(panel_tenTimesRecruit,"Panel_btn");
		panel_btn->setVisible(true);

		btn_close = (Button*)Helper::seekWidgetByName(panel_tenTimesRecruit,"Button_quit");
		btn_close->setVisible(false);
		btn_close->setTouchEnabled(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(TenTimesRecuriteResultUI::CloseEvent, this));

		//�ʮ�����
		Layer_tenTimesRecruit = Layer::create();
		addChild(Layer_tenTimesRecruit);

// 		Button * Btn_close = (Button*)Helper::seekWidgetByName(panel_tenTimesRecruit,"Button_close");
// 		Btn_close->setTouchEnabled(true);
// 		Btn_close->setPressedActionEnabled(true);
// 		Btn_close->addTouchEventListener(CC_CALLBACK_2(TenTimesRecuriteResultUI::CloseEvent));
		//�ʮ��鷵�
		auto Btn_tenTimes_back = (Button*)Helper::seekWidgetByName(panel_tenTimesRecruit,"Button_back");
		Btn_tenTimes_back->setTouchEnabled(true);
		Btn_tenTimes_back->setPressedActionEnabled(true);
		Btn_tenTimes_back->addTouchEventListener(CC_CALLBACK_2(TenTimesRecuriteResultUI::TenTimesBackEvent, this));
		//�ʮ������һ�
		auto Btn_tenTimes_again = (Button*)Helper::seekWidgetByName(panel_tenTimesRecruit,"Button_again");
		Btn_tenTimes_again->setTouchEnabled(true);
		Btn_tenTimes_again->setPressedActionEnabled(true);
		Btn_tenTimes_again->addTouchEventListener(CC_CALLBACK_2(TenTimesRecuriteResultUI::TenTimesAgainEvent, this));

		lable_tenTimes_gold = (Text*)Helper::seekWidgetByName(panel_tenTimesRecruit,"Label_IngotValue");

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(722,438));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(TenTimesRecuriteResultUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(TenTimesRecuriteResultUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(TenTimesRecuriteResultUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(TenTimesRecuriteResultUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void TenTimesRecuriteResultUI::onEnter()
{
	UIScene::onEnter();
	//this->openAnim();
}

void TenTimesRecuriteResultUI::onExit()
{
	UIScene::onExit();
}

bool TenTimesRecuriteResultUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	return true;
}

void TenTimesRecuriteResultUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void TenTimesRecuriteResultUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void TenTimesRecuriteResultUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void TenTimesRecuriteResultUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//this->closeAnim();
		this->removeFromParent();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}


void TenTimesRecuriteResultUI::TenTimesBackEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		//this->closeAnim();
		this->removeFromParent();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void TenTimesRecuriteResultUI::TenTimesAgainEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GeneralsUI::generalsRecuriteUI->TenTimesRecuritEvent(pSender, type);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void TenTimesRecuriteResultUI::RefreshTenTimesRecruitData( std::vector<CGeneralDetail *> temp )
{
	for (int i =0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		if (GameView::getInstance()->actionDetailList.at(i)->type() == TEN_TIMES)
		{
			char s[20];
			sprintf(s,"%d",GameView::getInstance()->actionDetailList.at(i)->gold());
			this->lable_tenTimes_gold->setString(s);
		}
	}


	for (int i = 0;i<10;++i)
	{
	 	if (Layer_tenTimesRecruit->getChildByTag(TenTimeRecruitResultBaseTag+i))
	 	{
	 		Layer_tenTimesRecruit->getChildByTag(TenTimeRecruitResultBaseTag+i)->removeFromParent();
	 	}
	}
	 
	for (int i = 0;i<temp.size();++i)
	{
//  		GeneralsHeadItemBase * generalsHeadItemBase = GeneralsHeadItemBase::create(temp.at(i)->modelid());
//  		generalsHeadItemBase->setAnchorPoint(Vec2(0.5f,0.5f));
//  		generalsHeadItemBase->setTag(TenTimeRecruitResultBaseTag+i);
// 		generalsHeadItemBase->showSilhouetteEffect();
//  		Layer_tenTimesRecruit->addChild(generalsHeadItemBase);
// 
// 		ActionInterval * action =(ActionInterval *)Sequence::create(
// 			CCTintTo::create(0.5f, 255,255,255),
// 			CallFunc::create(generalsHeadItemBase,callfunc_selector(GeneralsHeadItemBase::showBonusEffect)),
// 			NULL);
// 		generalsHeadItemBase->runAction(action);
// 
// 		ccBezierConfig bezier;
//  		if (i<5)
//  		{
//  			generalsHeadItemBase->setPosition(Vec2(60 + i*121,233));
//  		}
//  		else
//  		{
//  			generalsHeadItemBase->setPosition(Vec2(60 + (i%5)*121,84));
// 		}
// 
// 		if (generalsHeadItemBase->getLegendHead())
// 		{
// 			FiniteTimeAction * action = Sequence::create(
// 				DelayTime::create(1.0f),
// 				CCTintTo::create(0.3f, 255,255,255),
// 				NULL);
// 			generalsHeadItemBase->getLegendHead()->runAction(action);
// 		}
// 		
// 		FiniteTimeAction * action1 = Sequence::create(
// 			DelayTime::create(1.0f),
// 			CCTintTo::create(0.3f, 255,255,255),
// 			NULL);
// 		generalsHeadItemBase->label_name->runAction(action1);
// 
// 		FiniteTimeAction * action2 = Sequence::create(
// 			DelayTime::create(1.0f),
// 			CCTintTo::create(0.3f, 255,255,255),
// 			NULL);
// 		generalsHeadItemBase->imageView_star->runAction(action2);
// 
// 		if (generalsHeadItemBase->getFiveStarAnm())
// 		{
// 			FiniteTimeAction * action3 = Sequence::create(
// 				DelayTime::create(1.0f),
// 				CCTintTo::create(0.3f, 255,255,255),
// 				NULL);
// 			generalsHeadItemBase->getFiveStarAnm()->runAction(action3);
// 		}
// 
// 
		auto generalsHeadItem = GeneralCardItem::create(temp.at(i));
		generalsHeadItem->setAnchorPoint(Vec2(0.5f,0.5f));
		generalsHeadItem->setTag(TenTimeRecruitResultBaseTag+i);
		Layer_tenTimesRecruit->addChild(generalsHeadItem);
		if (i<5)
		{
			generalsHeadItem->setPosition(Vec2(60 + i*121,233));
		}
		else
		{
			generalsHeadItem->setPosition(Vec2(60 + (i%5)*121,84));
		}
// 
// 		ImageView* imageGeneral_bg = ImageView::create();
// 		imageGeneral_bg->loadTexture("res_ui/generals_white.png");
// 		imageGeneral_bg->setAnchorPoint(Vec2(0.5f,0.5f));
// 		imageGeneral_bg->setTag(TenTimeRecruitResultBaseTag+i);
// 		Layer_tenTimesRecruit->addChild(imageGeneral_bg);
// 		if (i<5)
// 		{
// 			imageGeneral_bg->setPosition(Vec2(60 + i*121,233));
// 		}
// 		else
// 		{
// 			imageGeneral_bg->setPosition(Vec2(60 + (i%5)*121,84));
// 		}
// 
// 		//generalInfo
// 		CGeneralBaseMsg * generalBaseInfo  = GeneralsConfigData::s_generalsBaseMsgData[temp.at(i)->modelid()];
// 		ImageView * imageGeneral_Icon = ImageView::create();
// 		imageGeneral_Icon->loadTexture(generalBaseInfo->get_half_photo().c_str());
// 		imageGeneral_Icon->setAnchorPoint(Vec2(0.5f,0));
// 		imageGeneral_Icon->setPosition(Vec2(0,63-imageGeneral_bg->getContentSize().height/2));
// 		imageGeneral_bg->addChild(imageGeneral_Icon);
// 
// 		std::string generalName_ = generalBaseInfo->name();
// 		Label * Label_generalName=Label::create();
// 		Label_generalName->setText(generalName_.c_str());
// 		Label_generalName->setAnchorPoint(Vec2(0.5f,0.5f));
// 		Label_generalName->setPosition(Vec2(0,-imageGeneral_bg->getContentSize().height/2+Label_generalName->getContentSize().height*3 - 5));
// 		Label_generalName->setFontSize(20);
// 		imageGeneral_bg->addChild(Label_generalName);
// 
// 		//rare
// 		std::string generalRare_ = RecuriteActionItem::getStarPathByNum(generalBaseInfo->rare());
// 		imageGeneral_Star = ImageView::create();
// 		imageGeneral_Star->loadTexture(generalRare_.c_str());
// 		imageGeneral_Star->setAnchorPoint(Vec2(0.5f,0.5f));
// 		imageGeneral_Star->setPosition(Vec2(imageGeneral_Star->getContentSize().width/2 - imageGeneral_bg->getContentSize().width/2+13,
// 			Label_generalName->getPosition().y+imageGeneral_Star->getContentSize().height));
// 		imageGeneral_Star->setScale(2.0f);
// 		imageGeneral_bg->addChild(imageGeneral_Star);
// 		imageGeneral_Star->setVisible(false);
// 
// 		//profession
// 		std::string generalProfess_ = getGeneralProfessionIconPath(generalBaseInfo->get_profession());
// 		imageGeneral_profession = ImageView::create();
// 		imageGeneral_profession->loadTexture(generalProfess_.c_str());
// 		imageGeneral_profession->setAnchorPoint(Vec2(0.5f,0.5f));
// 		imageGeneral_profession->setPosition(Vec2(imageGeneral_bg->getContentSize().width/2 - imageGeneral_profession->getContentSize().width/2 - 14,
// 			imageGeneral_Star->getPosition().y));
// 		imageGeneral_profession->setScale(2.0f);
// 		imageGeneral_bg->addChild(imageGeneral_profession);
// 		imageGeneral_profession->setVisible(false);
// 			
// 		//begin action
// 		imageGeneral_bg->setScale(1.0f);
// 		imageGeneral_bg->setColor(Color3B(0,0,0));
// 
// 		ActionInterval * action =(ActionInterval *)Sequence::create(
// 			CCTintTo::create(0.5f, 255,255,255),
// 			CallFunc::create(this,callfunc_selector(RecruitGeneralCard::createParticleSys)),
// 			DelayTime::create(0.5f),
// 			CallFunc::create(this, callfunc_selector(RecruitGeneralCard::lightActions)),
// 			NULL);
// 		imageGeneral_bg->runAction(action);
// 
// 		//head imageView action
// 		imageGeneral_Icon->setColor(Color3B(0,0,0));
// 		FiniteTimeAction * head_action = Sequence::create(
// 			DelayTime::create(1.0f),
// 			CCTintTo::create(0.3f, 255,255,255),
// 			NULL);
// 		imageGeneral_Icon->runAction(head_action);
// 		//name label action
// 		Label_generalName->setColor(Color3B(0,0,0));
// 		FiniteTimeAction * nameLabel_action = Sequence::create(
// 			DelayTime::create(1.0f),
// 			CCTintTo::create(0.3f, 255,255,255),
// 			NULL);
// 		Label_generalName->runAction(nameLabel_action);

 	}
}

void TenTimesRecuriteResultUI::RefreshBtnPresent( bool isPresent )
{
	panel_btn->setVisible(isPresent);
	btn_close->setVisible(!isPresent);
}


/////////////////////////////////////////////////////////////////////////////////////////

#define kTag_head_anm 10
GeneralCardItem::GeneralCardItem(void)
{
}


GeneralCardItem::~GeneralCardItem(void)
{
}

GeneralCardItem * GeneralCardItem::create(CGeneralDetail * generalInfo)
{
	auto recruitCard = new GeneralCardItem();
	if (recruitCard && recruitCard->init(generalInfo))
	{
		recruitCard->autorelease();
		return recruitCard;
	}
	CC_SAFE_DELETE(recruitCard);
	return NULL;
}

bool GeneralCardItem::init(CGeneralDetail * generalInfo)
{
	if (UIScene::init())
	{
		winSize =Director::getInstance()->getVisibleSize();

		u_layer = Layer::create();
		m_pLayer->addChild(u_layer);

		imageGeneral_bg = ImageView::create();
		imageGeneral_bg->loadTexture("res_ui/generals_white.png");
		imageGeneral_bg->setAnchorPoint(Vec2(0.5f,0.5f));
		imageGeneral_bg->setPosition(Vec2(imageGeneral_bg->getContentSize().width/2,imageGeneral_bg->getContentSize().height/2));
		u_layer->addChild(imageGeneral_bg);

		//generalInfo
		auto generalBaseInfo  = GeneralsConfigData::s_generalsBaseMsgData[generalInfo->modelid()];
// 		imageGeneral_icon = ImageView::create();
// 		imageGeneral_icon->loadTexture(generalBaseInfo->get_half_photo().c_str());
// 		imageGeneral_icon->setAnchorPoint(Vec2(0.5f,0));
// 		imageGeneral_icon->setPosition(Vec2(0,63-imageGeneral_bg->getContentSize().height/2));
// 		imageGeneral_bg->addChild(imageGeneral_icon);

		std::string animFileName = "res_ui/generals/";
		animFileName.append(generalBaseInfo->get_half_photo());
		animFileName.append(".anm");
		auto imageGeneral_icon = CCLegendAnimation::create(animFileName);
		if (imageGeneral_icon)
		{
			imageGeneral_icon->setPlayLoop(true);
			imageGeneral_icon->setReleaseWhenStop(false);
			imageGeneral_icon->setScale(.7f);	
			imageGeneral_icon->setPosition(Vec2(-49,-imageGeneral_bg->getContentSize().height/2+26));
			imageGeneral_bg->addChild(imageGeneral_icon);
		}

		std::string generalName_ = generalBaseInfo->name();
		auto Label_generalName=Label::createWithTTF(generalName_.c_str(), APP_FONT_NAME, 12);
		Label_generalName->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_generalName->setPosition(Vec2(0,18-imageGeneral_bg->getContentSize().height/2));
		imageGeneral_bg->addChild(Label_generalName);

		//rare
		std::string generalRare_ = RecuriteActionItem::getStarPathByNum(generalBaseInfo->rare());
		imageGeneral_Star = ImageView::create();
		imageGeneral_Star->loadTexture(generalRare_.c_str());
		imageGeneral_Star->setAnchorPoint(Vec2(0.5f,0.5f));
		imageGeneral_Star->setPosition(Vec2(imageGeneral_Star->getContentSize().width/2 - imageGeneral_bg->getContentSize().width/2+1,
			Label_generalName->getPosition().y+imageGeneral_Star->getContentSize().height/2+3));
		imageGeneral_Star->setScale(0.8f);
		imageGeneral_bg->addChild(imageGeneral_Star);

		//profession
		std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(generalBaseInfo->get_profession());
		imageGeneral_profession = ImageView::create();
		imageGeneral_profession->loadTexture(generalProfess_.c_str());
		imageGeneral_profession->setAnchorPoint(Vec2(0.5f,0.5f));
		imageGeneral_profession->setPosition(Vec2(imageGeneral_bg->getContentSize().width/2 - imageGeneral_profession->getContentSize().width/2-1 ,
			imageGeneral_Star->getPosition().y));
		imageGeneral_profession->setScale(0.8f);
		imageGeneral_bg->addChild(imageGeneral_profession);

		//begin action
		imageGeneral_bg->setScale(1.0f);
		imageGeneral_bg->setColor(Color3B(0,0,0));
		imageGeneral_Star->setScale(0.8f);
		imageGeneral_Star->setColor(Color3B(0,0,0));
		imageGeneral_profession->setScale(0.8f);
		imageGeneral_profession->setColor(Color3B(0,0,0));

		auto action =(ActionInterval *)Sequence::create(
			CCTintTo::create(0.5f, 255,255,255),
			CallFunc::create(CC_CALLBACK_0(GeneralCardItem::showBonusEffect,this)),
			NULL);
		imageGeneral_bg->runAction(action);

		//head imageView action
		imageGeneral_icon->setColor(Color3B(0,0,0));
		auto head_action = Sequence::create(
			DelayTime::create(1.0f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		imageGeneral_icon->runAction(head_action);
		//name label action
		Label_generalName->setColor(Color3B(0,0,0));
		auto nameLabel_action = Sequence::create(
			DelayTime::create(1.0f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		Label_generalName->runAction(nameLabel_action);
		//rare action
		imageGeneral_Star->setColor(Color3B(0,0,0));
		auto general_Star_action = Sequence::create(
			DelayTime::create(1.0f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		imageGeneral_Star->runAction(general_Star_action);
		//profession action
		imageGeneral_profession->setColor(Color3B(0,0,0));
		auto general_profession_action = Sequence::create(
			DelayTime::create(1.0f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		imageGeneral_profession->runAction(general_profession_action);

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(imageGeneral_bg->getContentSize());

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(GeneralCardItem::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(GeneralCardItem::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(GeneralCardItem::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(GeneralCardItem::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void GeneralCardItem::onEnter()
{
	UIScene::onEnter();
}

void GeneralCardItem::onExit()
{
	UIScene::onExit();
}

bool GeneralCardItem::onTouchBegan(Touch *touch, Event * pEvent)
{
	return false;
}

void GeneralCardItem::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void GeneralCardItem::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void GeneralCardItem::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void GeneralCardItem::generalStarActions()
{
	imageGeneral_Star->setVisible(true);
	auto action =(ActionInterval *)Sequence::create(
		ScaleTo::create(0.2f,0.8f,0.8f),
		CallFunc::create(CC_CALLBACK_0(GeneralCardItem::generalProfessionActions,this)),
		NULL);

	imageGeneral_Star->runAction(action);
}

void GeneralCardItem::generalProfessionActions()
{
	imageGeneral_profession->setVisible(true);
	imageGeneral_profession->runAction(ScaleTo::create(0.2f,0.8f,0.8f));
}

void GeneralCardItem::showBonusEffect()
{
	auto pNode = BonusSpecialEffect::create();
	pNode->setPosition(Vec2(this->getContentSize().width/2, this->getContentSize().height/2+20));
	pNode->setScale(0.7f);
	addChild(pNode);
}

void GeneralCardItem::showStarAndProfession()
{
	this->generalStarActions();
}
