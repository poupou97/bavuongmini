#ifndef  _GENERALSUI_GENERALSSKILLSUI_H_
#define _GENERALSUI_GENERALSSKILLSUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "./GeneralsListBase.h"

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGeneralBaseMsg;
class CGeneralDetail;
class NormalGeneralsHeadItemBase;
class CFightWayBase;
class UITab;
class GameFightSkill;


///////////////////////////////////////////////////////////////////////
class GeneralSkillCellData
{
public:
	GeneralSkillCellData();
	~GeneralSkillCellData();

public:
	GameFightSkill * curGameFightSkill;
	int curRealLevel;
};


/////////////////////////////////
/**
 * �佫�����µ ��佫�����
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.9
 */

class GeneralsSkillsUI : public UIScene,public TableViewDelegate,public TableViewDataSource
{
public:
	GeneralsSkillsUI();
	~GeneralsSkillsUI();

	enum SkillTpye{
		Warrior = 1,
		Magic,
		Ranger,
		Warlock
	};

	static GeneralsSkillsUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//ദ������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void LeveUpEvent(Ref *pSender, Widget::TouchEventType type);
	void NullSkillUpEvent(Ref *pSender, Widget::TouchEventType type);

private:
	int lastSelectCellId;
	int selectCellId;

	Layout * panel_skills; //����
	Layer * Layer_skills;
	Layer * Layer_upper;

	Button *btn_levelUp;
	Text *l_levelUp;

	TableView * generalsSkillsList_tableView;	
	SkillTpye skillType ;

	UITab * m_mainTab;

	std::vector<GeneralSkillCellData *>DataSourceList;

	std::string curSkillId;

	ImageView* i_skillFrame;
	ImageView* i_skillImage;
	Text* l_skillName;
	Text* l_skillType;
	Text* l_skillLvValue;
	//Label* l_skillQualityValue;
	ImageView * imageView_skillQuality;
	Text* l_skillMpValue;
	Text* l_skillCDTimeValue ;
	Text* l_skillDistanceValue;

private:
	void MainIndexChangedEvent(Ref *pSender);
	void UpdateDataByType(SkillTpye s_t);
	//��Զ������б��ƫ���
	void AutoSetOffSet();

	void refreshSkillDes(std::string curSkillDes,std::string nextSkillDes);

public:
	void setToDefault();
	void RefreshData();
	virtual void setVisible(bool visible);

	void RefreshSkillInfo(GameFightSkill * gameFightSkill);
	void RefreshBtnStatus(int curRealLevel);
	std::string getCurSkillId();

	static std::string getSkillQualityStr(int skillQuality);
};


/////////////////////////////////
/**
 * �佫�����µġ��佫���"��е��佫�б��
 * @author yangjun
 * @version 0.1.0
 * @date 2014.11.28
 */

class GeneralSkillCell : public TableViewCell
{
public:
	GeneralSkillCell();
	~GeneralSkillCell();
	static GeneralSkillCell* create(GeneralSkillCellData * cellData);
	bool init(GeneralSkillCellData * cellData);

	void RefreshCell(GeneralSkillCellData * cellData);
	void setSelectSelf(bool isvisible);
private:
	Sprite *  sprite_IconImage;
	Label * l_lv;
	Label * label_skillName;

	Sprite* sprite_blackFrame;
};
#endif

