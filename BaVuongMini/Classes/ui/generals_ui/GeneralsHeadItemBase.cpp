
#include "GeneralsHeadItemBase.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../utils/StaticDataManager.h"
#include "GeneralsUI.h"
#include "GameView.h"
#include "RecuriteActionItem.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "ShowBigGeneralsHead.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/extensions/RecruitGeneralCard.h"
#include "AppMacros.h"

#define KTag_FiveStar_Anm 345
#define kTagLegendAnm_Head 10  

GeneralsHeadItemBase::GeneralsHeadItemBase():
m_nCreateType(-1)
{
	m_pGeneralBaseMsg = new CGeneralBaseMsg();
}

GeneralsHeadItemBase::~GeneralsHeadItemBase()
{
	delete m_pGeneralBaseMsg;
}


void GeneralsHeadItemBase::onEnter()
{
	UIScene::onEnter();
}

void GeneralsHeadItemBase::onExit()
{
	UIScene::onExit();
}


GeneralsHeadItemBase * GeneralsHeadItemBase::create(CGeneralBaseMsg * generalBaseMsg)
{
	auto generalsHeadItemBase = new GeneralsHeadItemBase();
	if (generalsHeadItemBase && generalsHeadItemBase->init(generalBaseMsg))
	{
		generalsHeadItemBase->autorelease();
		return generalsHeadItemBase;
	}
	CC_SAFE_DELETE(generalsHeadItemBase);
	return NULL;
}

GeneralsHeadItemBase * GeneralsHeadItemBase::create( int generalModleId)
{
	auto generalsHeadItemBase = new GeneralsHeadItemBase();
	if (generalsHeadItemBase && generalsHeadItemBase->init(generalModleId))
	{
		generalsHeadItemBase->autorelease();
		return generalsHeadItemBase;
	}
	CC_SAFE_DELETE(generalsHeadItemBase);
	return NULL;
}

GeneralsHeadItemBase * GeneralsHeadItemBase::create( int generalModleId,int currentquality )
{
	auto generalsHeadItemBase = new GeneralsHeadItemBase();
	if (generalsHeadItemBase && generalsHeadItemBase->init(generalModleId,currentquality))
	{
		generalsHeadItemBase->autorelease();
		return generalsHeadItemBase;
	}
	CC_SAFE_DELETE(generalsHeadItemBase);
	return NULL;
}

bool GeneralsHeadItemBase::init(CGeneralBaseMsg * generalBaseMsg)
{
	if (UIScene::init())
	{
		m_nCreateType = CT_withGeneralBaseMsg;
		m_pGeneralBaseMsg->CopyFrom(*generalBaseMsg);

		layer_up = Layer::create();
		addChild(layer_up);

		auto generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];

		//�߿
		frame = Button::create();
		frame->loadTextures(GeneralsUI::getBigHeadFramePath(generalBaseMsg->currentquality()).c_str(),GeneralsUI::getBigHeadFramePath(generalBaseMsg->currentquality()).c_str(),"");
		frame->setScale9Enabled(true);
		frame->setContentSize(Size(108,147));
		frame->setAnchorPoint(Vec2::ZERO);
		frame->setPosition(Vec2(0,0));
		frame->setTouchEnabled(false);
		frame->addTouchEventListener(CC_CALLBACK_2(GeneralsHeadItemBase::FrameEvent, this));
		m_pLayer->addChild(frame);

		//�ͷ�
// 		std::string icon_path = "res_ui/generals/";
// 		icon_path.append(generalMsgFromDb->get_half_photo());
// 		icon_path.append(".png");
// 		imageView_head = ImageView::create();
// 		imageView_head->loadTexture(icon_path.c_str());
// 		imageView_head->setAnchorPoint(Vec2(0.5f,0));
// 		imageView_head->setPosition(Vec2(frame->getContentSize().width/2,27));
// 		m_pLayer->addChild(imageView_head);

		std::string animFileName = "res_ui/generals/";
		animFileName.append(generalMsgFromDb->get_half_photo());
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/generals/caiwenji.anm";
		la_head = CCLegendAnimation::create(animFileName);
		if (la_head)
		{
			la_head->setPlayLoop(true);
			la_head->setReleaseWhenStop(false);
			la_head->setScale(.7f);	
			la_head->setPosition(Vec2(5,27));
			la_head->setTag(kTagLegendAnm_Head);
			m_pLayer->addChild(la_head);
		}

		//����ֺڵ
//  		black_namebg = ImageView::create(); 
//  		black_namebg->loadTexture("res_ui/zhezhao80.png");
//  		black_namebg->setScale9Enabled(true);
//  		black_namebg->setContentSize(Size(98,18));
//  		black_namebg->setAnchorPoint(Vec2::ZERO);
//  		black_namebg->setPosition(Vec2(0,0));
//  		m_pLayer->addChild(black_namebg);
		//����
		label_name = Label::createWithTTF(generalMsgFromDb->name().c_str(), APP_FONT_NAME, 12);
		label_name->setAnchorPoint(Vec2(0.5f,0.5f));
		label_name->setPosition(Vec2(frame->getContentSize().width/2,18));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		layer_up->addChild(label_name);

		//�ϡ�ж
		if (generalBaseMsg->rare()>0)
		{
			imageView_star = ImageView::create();
			imageView_star->loadTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(Vec2::ZERO);
			imageView_star->setScale(0.8f);
			imageView_star->setPosition(Vec2(6,27));
			imageView_star->setName("ImageView_Star");
			layer_up->addChild(imageView_star);

			if (generalBaseMsg->rare() >= 5)
			{
				// load animation
				std::string animFileName = "animation/ui/wj/wj2.anm";
				auto m_pAnim = CCLegendAnimation::create(animFileName);
				m_pAnim->setPlayLoop(true);
				m_pAnim->setReleaseWhenStop(false);
 				m_pAnim->setScale(.8f);
 				m_pAnim->setPosition(Vec2(6,27));
 				m_pAnim->setPlaySpeed(.35f);
				m_pAnim->setTag(KTag_FiveStar_Anm);
				layer_up->addChild(m_pAnim);
			}
		}
// 		for( int  i = 0; i<generalBaseMsg->rare();++i)
// 		{
// 			ImageView * star = ImageView::create();
// 			star->loadTexture("res_ui/star_on.png");
// 			star->setAnchorPoint(Vec2::ZERO);
// 			star->setScale(0.8f);
// 			star->setPosition(Vec2(6+19*i,28));
// 			m_pLayer->addChild(star);
// 		}

		//profession
		std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(generalMsgFromDb->get_profession());
		imageView_prefession = ImageView::create();
		imageView_prefession->loadTexture(generalProfess_.c_str());
		imageView_prefession->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_prefession->setPosition(Vec2(frame->getContentSize().width - imageView_prefession->getContentSize().width/2-2 ,
			42));
		imageView_prefession->setScale(0.8f);
		layer_up->addChild(imageView_prefession);
		imageView_prefession->setName("imageView_prefession");

		this->setContentSize(Size(108,147));
		return true;
	}
	return false;
}

bool GeneralsHeadItemBase::init( int generalModleId )
{
	if (UIScene::init())
	{
		m_nCreateType = CT_withModleId;
		m_nGeneralModleId = generalModleId;

		layer_up = Layer::create();
		addChild(layer_up);

		auto generalBaseMsg = GeneralsConfigData::s_generalsBaseMsgData[generalModleId];
		//ȱ߿
		frame = Button::create();
		frame->loadTextures(GeneralsUI::getBigHeadFramePath(1).c_str(),GeneralsUI::getBigHeadFramePath(1).c_str(),"");    //�Ĭ���1Ǽ���1-10Ϊ��ɫ��
		frame->setScale9Enabled(true);
		frame->setContentSize(Size(108,147));
		frame->setAnchorPoint(Vec2::ZERO);
		frame->setPosition(Vec2(0,0));
		frame->setTouchEnabled(false);
		frame->addTouchEventListener(CC_CALLBACK_2(GeneralsHeadItemBase::FrameEvent, this));
		m_pLayer->addChild(frame);
		//���ֺڵ
// 		black_namebg = ImageView::create(); 
// 		black_namebg->loadTexture("res_ui/zidi.png");
// 		black_namebg->setScale9Enabled(true);
// 		black_namebg->setContentSize(Size(98,18));
// 		black_namebg->setAnchorPoint(Vec2::ZERO);
// 		black_namebg->setPosition(Vec2(0,0));
// 		m_pLayer->addChild(black_namebg);
		//����
		label_name = Label::create();
		label_name->setString(generalBaseMsg->name().c_str());
		//label_name->setText("asd");
		label_name->setAnchorPoint(Vec2(0.5f,0.5f));
		label_name->setPosition(Vec2(frame->getContentSize().width/2,18));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(1));
		layer_up->addChild(label_name);
		//�ͷ�
// 		std::string icon_path = "res_ui/generals/";
// 		icon_path.append(generalBaseMsg->get_half_photo());
// 		//icon_path.append("guanyu");
// 		icon_path.append(".png");
// 		imageView_head = ImageView::create();
// 		imageView_head->loadTexture(icon_path.c_str());
// 		imageView_head->setAnchorPoint(Vec2(0.5f,0));
// 		imageView_head->setPosition(Vec2(frame->getContentSize().width/2,27));
// 		m_pLayer->addChild(imageView_head);

		std::string animFileName = "res_ui/generals/";
		animFileName.append(generalBaseMsg->get_half_photo());
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/generals/caiwenji.anm";
		la_head = CCLegendAnimation::create(animFileName);
		if (la_head)
		{
			la_head->setPlayLoop(true);
			la_head->setReleaseWhenStop(false);
			la_head->setScale(.7f);	
			la_head->setPosition(Vec2(5,27));
			la_head->setTag(kTagLegendAnm_Head);
			m_pLayer->addChild(la_head);
		}
		//�ϡ�ж
		if (generalBaseMsg->rare()>0)
		{
			imageView_star = ImageView::create();
			imageView_star->loadTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(Vec2::ZERO);
			imageView_star->setScale(0.8f);
			imageView_star->setPosition(Vec2(6,27));
			imageView_star->setName("ImageView_Star");
			layer_up->addChild(imageView_star);

			if (generalBaseMsg->rare() >= 5)
			{
				// load animation
				std::string animFileName = "animation/ui/wj/wj2.anm";
				auto m_pAnim = CCLegendAnimation::create(animFileName);
				m_pAnim->setPlayLoop(true);
				m_pAnim->setReleaseWhenStop(true);
				m_pAnim->setScale(.8f);
				m_pAnim->setPosition(Vec2(6,27));
				m_pAnim->setPlaySpeed(.35f);
				m_pAnim->setTag(KTag_FiveStar_Anm);
				layer_up->addChild(m_pAnim);
			}
		}
		//for( int  i = 0; i<5;++i)
// 		for( int  i = 0; i<generalBaseMsg->rare();++i)
// 		{
// 			ImageView * star = ImageView::create();
// 			star->loadTexture("res_ui/star_on.png");
// 			star->setAnchorPoint(Vec2::ZERO);
// 			star->setScale(0.8f);
// 			star->setPosition(Vec2(6+19*i,28));
// 			m_pLayer->addChild(star);
// 		}
		//profession
		std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(generalBaseMsg->get_profession());
		imageView_prefession = ImageView::create();
		imageView_prefession->loadTexture(generalProfess_.c_str());
		imageView_prefession->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_prefession->setPosition(Vec2(frame->getContentSize().width - imageView_prefession->getContentSize().width/2-2 ,
			42));
		imageView_prefession->setScale(0.8f);
		layer_up->addChild(imageView_prefession);
		imageView_prefession->setName("imageView_prefession");

		this->setContentSize(Size(108,147));
		return true;
	}
	return false;
}

bool GeneralsHeadItemBase::init( int generalModleId,int currentquality )
{
	if (UIScene::init())
	{
		m_nCreateType = CT_withModleIdAndQuality;
		m_nGeneralModleId = generalModleId;
		m_nCurrentquality = currentquality;

		layer_up = Layer::create();
		addChild(layer_up);

		auto generalBaseMsg = GeneralsConfigData::s_generalsBaseMsgData[generalModleId];
		//ȱ߿
		frame = Button::create();
		frame->loadTextures(GeneralsUI::getBigHeadFramePath(currentquality).c_str(),GeneralsUI::getBigHeadFramePath(currentquality).c_str(),"");    //1-10�Ϊ��ɫ��
		frame->setScale9Enabled(true);
		frame->setContentSize(Size(108,147));
		frame->setAnchorPoint(Vec2::ZERO);
		frame->setPosition(Vec2(0,0));
		frame->setTouchEnabled(false);
		frame->addTouchEventListener(CC_CALLBACK_2(GeneralsHeadItemBase::FrameEvent, this));
		m_pLayer->addChild(frame);

		//���
		label_name = Label::createWithTTF(generalBaseMsg->name().c_str(), APP_FONT_NAME, 12);
		//label_name->setText("asd");
		label_name->setAnchorPoint(Vec2(0.5f,0.5f));
		label_name->setPosition(Vec2(frame->getContentSize().width/2,18));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(1));
		layer_up->addChild(label_name);
		//�ͷ�
// 		std::string icon_path = "res_ui/generals/";
// 		icon_path.append(generalBaseMsg->get_half_photo());
// 		//icon_path.append("guanyu");
// 		icon_path.append(".png");
// 		imageView_head = ImageView::create();
// 		imageView_head->loadTexture(icon_path.c_str());
// 		imageView_head->setAnchorPoint(Vec2(0.5f,0));
// 		imageView_head->setPosition(Vec2(frame->getContentSize().width/2,27));
// 		m_pLayer->addChild(imageView_head);

		std::string animFileName = "res_ui/generals/";
		animFileName.append(generalBaseMsg->get_half_photo());
		animFileName.append(".anm");
		//std::string animFileName = "res_ui/generals/caiwenji.anm";
		la_head = CCLegendAnimation::create(animFileName);
		if (la_head)
		{
			la_head->setPlayLoop(true);
			la_head->setReleaseWhenStop(false);
			la_head->setScale(.7f);	
			la_head->setPosition(Vec2(5,27));
			la_head->setTag(kTagLegendAnm_Head);
			m_pLayer->addChild(la_head);
		}
		//�ϡ�ж
		if (generalBaseMsg->rare()>0)
		{
			imageView_star = ImageView::create();
			imageView_star->loadTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(Vec2::ZERO);
			imageView_star->setScale(0.8f);
			imageView_star->setPosition(Vec2(6,27));
			imageView_star->setName("ImageView_Star");
			layer_up->addChild(imageView_star);

			if (generalBaseMsg->rare() >= 5)
			{
				// load animation
				std::string animFileName = "animation/ui/wj/wj2.anm";
				auto m_pAnim = CCLegendAnimation::create(animFileName);
				m_pAnim->setPlayLoop(true);
				m_pAnim->setReleaseWhenStop(true);
				m_pAnim->setScale(.8f);
				m_pAnim->setPosition(Vec2(6,27));
				m_pAnim->setPlaySpeed(.35f);
				m_pAnim->setTag(KTag_FiveStar_Anm);
				layer_up->addChild(m_pAnim);
			}
		}
		//for( int  i = 0; i<5;++i)
// 		for( int  i = 0; i<generalBaseMsg->rare();++i)
// 		{
// 			ImageView * star = ImageView::create();
// 			star->loadTexture("res_ui/star_on.png");
// 			star->setAnchorPoint(Vec2::ZERO);
// 			star->setScale(0.8f);
// 			star->setPosition(Vec2(6+19*i,28));
// 			m_pLayer->addChild(star);
// 		}
		//profession
		std::string generalProfess_ = RecruitGeneralCard::getGeneralProfessionIconPath(generalBaseMsg->get_profession());
		imageView_prefession = ImageView::create();
		imageView_prefession->loadTexture(generalProfess_.c_str());
		imageView_prefession->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_prefession->setPosition(Vec2(frame->getContentSize().width - imageView_prefession->getContentSize().width/2-2 ,
			42));
		imageView_prefession->setScale(0.8f);
		layer_up->addChild(imageView_prefession);
		imageView_prefession->setName("imageView_prefession");

		this->setContentSize(Size(108,147));
		return true;
	}
	return false;
}

void GeneralsHeadItemBase::showSilhouetteEffect()
{
	//imageView_head->setColor(Color3B(0,0,0));
	if (m_pLayer->getChildByTag(kTagLegendAnm_Head))
	{
		auto anm = (CCLegendAnimation*)m_pLayer->getChildByTag(kTagLegendAnm_Head);
		anm->setColor(Color3B(0,0,0));
	}

	//label_name->setVisible(false);
	label_name->setColor(Color3B(0,0,0));
	if (layer_up->getChildByName("ImageView_Star"))
	{
		layer_up->getChildByName("ImageView_Star")->setColor(Color3B(0,0,0));
	}

	if (layer_up->getChildByTag(KTag_FiveStar_Anm))
	{
		auto anm = (CCLegendAnimation*)layer_up->getChildByTag(KTag_FiveStar_Anm);
		anm->setColor(Color3B(0,0,0));
	}
}

void GeneralsHeadItemBase::removeSilhouetteEffect()
{
	//imageView_head->setColor(Color3B(255,255,255));
	if (m_pLayer->getChildByTag(kTagLegendAnm_Head))
	{
		auto anm = (CCLegendAnimation*)m_pLayer->getChildByTag(kTagLegendAnm_Head);
		anm->setColor(Color3B(0,0,0));
	}

	//label_name->setVisible(true);
	label_name->setColor(Color3B(255,255,255));
	if (layer_up->getChildByName("ImageView_Star"))
	{
		layer_up->getChildByName("ImageView_Star")->setColor(Color3B(255,255,255));
	}

	if (layer_up->getChildByTag(KTag_FiveStar_Anm))
	{
		auto anm = (CCLegendAnimation*)layer_up->getChildByTag(KTag_FiveStar_Anm);
		anm->setColor(Color3B(255,255,255));
	}
}

void GeneralsHeadItemBase::FrameEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		switch (m_nCreateType)
		{
		case CT_withGeneralBaseMsg:
		{
			auto bigHead = (ShowBigGeneralsHead*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShowBigGeneralsHead);
			if (bigHead == NULL)
			{
				bigHead = ShowBigGeneralsHead::create(m_pGeneralBaseMsg);
				GameView::getInstance()->getMainUIScene()->addChild(bigHead, 0, kTagShowBigGeneralsHead);
				bigHead->setIgnoreAnchorPointForPosition(false);
				bigHead->setAnchorPoint(Vec2(0.5f, 0.5f));
				bigHead->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				bigHead->showBeginAction();
			}
		}
		break;
		case CT_withModleId:
		{
			auto bigHead = (ShowBigGeneralsHead*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShowBigGeneralsHead);
			if (bigHead == NULL)
			{
				bigHead = ShowBigGeneralsHead::create(m_nGeneralModleId);
				GameView::getInstance()->getMainUIScene()->addChild(bigHead, 0, kTagShowBigGeneralsHead);
				bigHead->setIgnoreAnchorPointForPosition(false);
				bigHead->setAnchorPoint(Vec2(0.5f, 0.5f));
				bigHead->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				bigHead->showBeginAction();
			}
		}
		break;
		case CT_withModleIdAndQuality:
		{
			auto bigHead = (ShowBigGeneralsHead*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShowBigGeneralsHead);
			if (bigHead == NULL)
			{
				bigHead = ShowBigGeneralsHead::create(m_nGeneralModleId, m_nCurrentquality);
				GameView::getInstance()->getMainUIScene()->addChild(bigHead, 0, kTagShowBigGeneralsHead);
				bigHead->setIgnoreAnchorPointForPosition(false);
				bigHead->setAnchorPoint(Vec2(0.5f, 0.5f));
				bigHead->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				bigHead->showBeginAction();
			}
		}
		break;
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsHeadItemBase::setCanShowBigCard( bool isCan )
{
	frame->setTouchEnabled(isCan);
}

CCLegendAnimation * GeneralsHeadItemBase::getLegendHead()
{
	if (m_pLayer->getChildByTag(kTagLegendAnm_Head))
	{
		auto anm = (CCLegendAnimation*)m_pLayer->getChildByTag(kTagLegendAnm_Head);
		return anm;
	}
	else
	{
		return NULL;
	}
}

CCLegendAnimation * GeneralsHeadItemBase::getFiveStarAnm()
{
	if (layer_up->getChildByTag(KTag_FiveStar_Anm))
	{
		auto anm = (CCLegendAnimation*)layer_up->getChildByTag(KTag_FiveStar_Anm);
		return anm;
	}
	else
	{
		return NULL;
	}
}
