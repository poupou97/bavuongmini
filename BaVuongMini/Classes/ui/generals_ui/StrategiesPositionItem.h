
#ifndef _GENERALSUI_STRATEGIESPOSITIONITEM
#define _GENERALSUI_STRATEGIESPOSITIONITEM

#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;
/////////////////////////////////
/**
 * �佫�����µġ��󷨽��桱�е���λ��Ϣ�
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.16
 */

class CGeneralBaseMsg;

class StrategiesPositionItem : public UIScene
{
public:
	StrategiesPositionItem();
	~StrategiesPositionItem();

	static StrategiesPositionItem * create(CGeneralBaseMsg * generalBaseMsg);
	bool init(CGeneralBaseMsg * generalBaseMsg);

	static StrategiesPositionItem * create(int index);
	bool init(int index);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void RefreshGeneralsStatus(int _status);

	void RefreshAddedProperty();

private:
	void FightStatusEvent(Ref *pSender);
	void HeadFrameEvent(Ref *pSender, Widget::TouchEventType type);

private:
	Label * l_fightStatus;
	Button * btn_fightStatus;
	Label * l_additive;
public:
	//1-5
	int positionIndex;
	//���Ϣ 0����� 1󣬳�ս 2
	int fightStatus;
	//��λ���Ƿ����佫
	bool isHaveGeneral;
	//��ǰ�佫id
	long long genenralsId;

};

#endif