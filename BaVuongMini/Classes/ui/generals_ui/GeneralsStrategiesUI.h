#ifndef  _GENERALSUI_GENERALSSTRATEGIESUI_H_
#define _GENERALSUI_GENERALSSTRATEGIESUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGeneralBaseMsg;
class CGeneralDetail;
class NormalGeneralsHeadItemBase;
class CFightWayBase;

#define GeneralsStrategiesPropertyBase "generals_strategies_property_"

/////////////////////////////////
/**
 * �佫�����µ ����
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.4
 */

class GeneralsStrategiesUI : public UIScene,public TableViewDelegate,public TableViewDataSource
{
public:
	GeneralsStrategiesUI();
	~GeneralsStrategiesUI();

	enum PropertyType{
		fightway_strength = 1,
        fightway_dexterity,
        fightway_intelligence,
        fightway_focus,
		fightway_hp_capacity = 11,
		fightway_mp_capacity,
		fightway_min_attack,
		fightway_max_attack,
		fightway_min_magic_attack,
		fightway_max_magic_attack,
		fightway_min_defend,
		fightway_max_defend,
		fightway_min_magic_defend,
		fightway_max_magic_defend,
		fightway_hit,
		fightway_dodge,
		fightway_crit,
        fightway_crit_damage,
		fightway_attack_speed,
		fightway_move_speed,
	};

	enum FightWayStatus{
		NotStuded = 0,  //�δѧ�
	    Studed,              //���ѧδʹ�
		Highest               //��
	};

	static GeneralsStrategiesUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//�������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void StrategiesResetEvent(Ref *pSender, Widget::TouchEventType type);
	void StrategiesLeveUpEvent(Ref *pSender, Widget::TouchEventType type);
	void StrategiesUseEvent(Ref *pSender, Widget::TouchEventType type);
	void StrategiesNotUseEvent(Ref *pSender, Widget::TouchEventType type);
	//���ϸ˵�
	void StrategiesDetailEvent(Ref *pSender, Widget::TouchEventType type);

	int getActiveFightWay();
	void setActiveFightWay(int _id);

	void setActiveIndicator();

	void RefreshStrategiesInfo(CFightWayBase * fightWayBase);
	void RefreshButtonState(CFightWayBase * fightWayBase);

	//�����ˢ����
	void RefreshAllInfo(CFightWayBase * fightWayBase);

	void setStrategiesListToDefault();

private:
	int lastSelectCellId;

	Layout * panel_strategies; //�����
	Layer * Layer_strategies;
	Layer * Layer_upper;
	Vec2 slotPos[5] ;

	Text *l_upgrade;
	Button *btn_closeStrategies;

	Text * l_strategieName;
	Text * l_addPropDes;

	//�ѡ�еCellĵIndex
	int selectCellIndex;

	TableView * strategiesList_tableView;
	//ĵ�ǰ�����id
	int activeFightWayId ;
	//󷨵�ǰѡ���id
	int curSelectFightWayId;
	//󷨵�ǰѡ����״̬
	FightWayStatus curFightWayStatus;

public:
	//���б
	std::vector<CFightWayBase *> generalsStrategiesList;
	//������佫�б
	//std::vector<CGeneralBaseMsg *>generalsInLineList;

	int getCurSelectFightWayId();
	//�ˢ���󷨽�����Ϣ
	void RefreshData();
	//ˢ�����б
	void RefreshGeneralsStrategiesList();
	//�ˢ�����������佫��Ϣ
	void RefreshAllGeneralsInLine();
	//ˢ�µ��������佫��Ϣ
	void RefreshOneGeneralsInLine(CGeneralBaseMsg * generalBaseMsg);
	//�Զ������б��ƫ���
	void AutoSetOffSet();

	void RefreshListToDefault();
	void RefreshListWithoutChangeOffSet();

	virtual bool isVisible();
	virtual void setVisible(bool visible);

	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	//ѧϰ�
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

public:
	Button *btn_levelUpStrategies;
	Button *btn_openStrategies;
private:
	//󷨽�ѧ
	int mTutorialScriptInstanceId;
};

#endif

