#include "GeneralsUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../extensions/UITab.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CActionDetail.h"
#include "GameView.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "./GeneralsListUI.h"
#include "GeneralsEvolutionUI.h"
#include "GeneralsTeachUI.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/MainScene.h"
#include "GeneralsHeadItemBase.h"
#include "GeneralsRecuriteUI.h"
#include "TenTimesRecuriteResultUI.h"
#include "SingleRecuriteResultUI.h"
#include "GeneralsStrategiesUI.h"
#include "GeneralsSkillsUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/UITutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "AppMacros.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../gamescene_state/role/ShowBaseGeneral.h"

using namespace CocosDenshion;

#define  kTagRankImageView 596
#define  kTagStarImageView 597
#define  kTagInBattleFlag 598

Layout* GeneralsUI::ppanel = NULL;
//�佫��ļ
GeneralsRecuriteUI * GeneralsUI::generalsRecuriteUI = NULL;
//�佫������ļ��
SingleRecuriteResultUI * GeneralsUI::singleRecuriteResultUI = NULL;
//��佫ʮ����
TenTimesRecuriteResultUI * GeneralsUI::tenTimesRecuriteResultUI = NULL;
//��佫�б
GeneralsListUI * GeneralsUI::generalsListUI = NULL;
//��佫�
GeneralsEvolutionUI * GeneralsUI::generalsEvolutionUI = NULL;
//��佫����
GeneralsTeachUI * GeneralsUI::generalsTeachUI = NULL;
//�
GeneralsStrategiesUI * GeneralsUI::generalsStrategiesUI = NULL;
//��佫���
GeneralsSkillsUI * GeneralsUI::generalsSkillsUI = NULL;


GeneralsUI::GeneralsUI()
{
}


GeneralsUI::~GeneralsUI()
{
	CCLOG("123456");
}

GeneralsUI * GeneralsUI::create()
{
	auto generals = new GeneralsUI();
	if (generals && generals->init())
	{
		generals->autorelease();
		return generals;
	}
	CC_SAFE_DELETE(generals);
	return NULL;
}

bool GeneralsUI::init()
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		//this->scheduleUpdate();

		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		u_layer->setContentSize(Size(800, 480));
		u_layer->setPosition(Vec2::ZERO);
		u_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		addChild(u_layer);

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2::ZERO);
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		//ܼ��UI
		if(LoadSceneLayer::GeneralsLayer->getParent() != NULL)
		{
			LoadSceneLayer::GeneralsLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::GeneralsLayer;
		ppanel->getVirtualRenderer()->setContentSize(Size(800, 480));
		ppanel->setAnchorPoint(Vec2(0.5f,0.5f));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setTouchEnabled(true);
		ppanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(ppanel);

		const char * secondStr = StringDataManager::getString("UIName_wu_1");
		const char * thirdStr = StringDataManager::getString("UIName_jiang");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		u_layer->addChild(atmature);
	
		//��佫��ļ
		generalsRecuriteUI = GeneralsRecuriteUI::create();
		u_layer->addChild(generalsRecuriteUI);
		generalsRecuriteUI->setVisible(true);
		//�佫������ļ��
// 		singleRecuriteResultUI = SingleRecuriteResultUI::create();
// 		u_layer->addChild(singleRecuriteResultUI);
// 		singleRecuriteResultUI->setVisible(false);
		//��佫ʮ����
// 		tenTimesRecuriteResultUI = TenTimesRecuriteResultUI::create();
// 		u_layer->addChild(tenTimesRecuriteResultUI);
// 		tenTimesRecuriteResultUI->setVisible(false);
		//��佫�б
		generalsListUI = GeneralsListUI::create();
		u_layer->addChild(generalsListUI);
		generalsListUI->setVisible(false);
		//��佫�
		generalsEvolutionUI = GeneralsEvolutionUI::create();
		u_layer->addChild(generalsEvolutionUI);
		generalsEvolutionUI->setVisible(false);
		//��佫����
		generalsTeachUI = GeneralsTeachUI::create(generalsListUI->getCurGeneralBaseMsg());
		u_layer->addChild(generalsTeachUI);
		generalsTeachUI->setVisible(false);
		//�
		generalsStrategiesUI = GeneralsStrategiesUI::create();
		u_layer->addChild(generalsStrategiesUI);
		generalsStrategiesUI->setVisible(false);
		//��佫���
		generalsSkillsUI = GeneralsSkillsUI::create();
		u_layer->addChild(generalsSkillsUI);
		generalsSkillsUI->setVisible(false);

		//ܹرհ�ť
		Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setVisible(true);
		Button_close->setTouchEnabled(true);
		Button_close->setPressedActionEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(GeneralsUI::CloseEvent, this));

		const char * normalImage = "res_ui/tab_b_off.png";
		const char * selectImage = "res_ui/tab_b.png";
		const char * finalImage = "";
		const char * highLightImage = "res_ui/tab_b.png";
		//char * ItemName[] ={"res_ui/wujiang/new/zi_wujiang.png","res_ui/wujiang/new/zi_wode.png","res_ui/wujiang/new/zi_wujiangjineng.png","res_ui/wujiang/new/zi_zhenfa.png"};
		const char *str1 = StringDataManager::getString("generals_tab_dianjiangtai");
		char* p1 =const_cast<char*>(str1);
		const char *str2 = StringDataManager::getString("generals_tab_wodewujiangi");
		char* p2 =const_cast<char*>(str2);
		const char *str3 = StringDataManager::getString("generals_tab_wujiangjineng");
		char* p3 =const_cast<char*>(str3);
		const char *str4 = StringDataManager::getString("generals_tab_zhenfa");
		char* p4 =const_cast<char*>(str4);
		char * ItemName[] ={p1,p2,p3,p4};
		mainTab  = UITab::createWithText(4,normalImage,selectImage,finalImage,ItemName,VERTICAL,-5,18);
		mainTab->setAnchorPoint(Vec2(0,0));
		mainTab->setPosition(Vec2(758,405));
		mainTab->setHighLightImage((char * )highLightImage);
		mainTab->setHightLightLabelColor(Color3B(47,93,13));
		mainTab->setNormalLabelColor(Color3B(255,255,255));
		mainTab->setDefaultPanelByIndex(0);
		mainTab->addIndexChangedEvent(this,coco_indexchangedselector(GeneralsUI::IndexChangedEvent));
		mainTab->setPressedActionEnabled(true);
		u_layer->addChild(mainTab);

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);

		//add by yangjun for test at 2014.1.8
		//ScriptManager::getInstance()->runScript("script/general1.sc");

		return true;
	}
	return false;
}



void GeneralsUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
	GameUtils::playGameSound(SCENE_OPEN, 2, false);
}

void GeneralsUI::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

bool GeneralsUI::onTouchBegan(Touch *touch, Event * pEvent)
{
		return true;
}

void GeneralsUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void GeneralsUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void GeneralsUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void GeneralsUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		// 	if (mainScene->generalHeadLayer->getChildByTag(66))
		// 	{
		// 		GeneralsHeadManager * generalsHeadManager = (GeneralsHeadManager*)mainScene->generalHeadLayer->getChildByTag(66);
		// 		generalsHeadManager->setGeneralHeadTouchEnable(true);
		// 	}

		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		generalsRecuriteUI->CloseEvent(pSender);
		this->removeFromParentAndCleanup(false);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsUI::setToDefaultTab()
{
	//singleRecuriteResultUI->setVisible(false);
	//tenTimesRecuriteResultUI->setVisible(false);
	generalsEvolutionUI->setVisible(false);
	generalsTeachUI->setVisible(false);

	generalsRecuriteUI->setVisible(true);
	generalsListUI->setVisible(false);
	generalsStrategiesUI->setVisible(false);
	generalsSkillsUI->setVisible(false);

	mainTab->setDefaultPanelByIndex(0);

	mainTab->setHightLightLabelColor(Color3B(47,93,13));
	mainTab->setNormalLabelColor(Color3B(255,255,255));
}

void GeneralsUI::setToTabByIndex(int idx)
{
	generalsEvolutionUI->setVisible(false);
	generalsTeachUI->setVisible(false);

	switch (idx)
	{
	case 0:
		{
			generalsRecuriteUI->setVisible(true);
			generalsListUI->setVisible(false);
			generalsStrategiesUI->setVisible(false);
			generalsSkillsUI->setVisible(false);

			mainTab->setDefaultPanelByIndex(0);
		}
		break;
	case 1:
		{
			generalsRecuriteUI->setVisible(false);
			generalsListUI->setVisible(true);
			generalsStrategiesUI->setVisible(false);
			generalsSkillsUI->setVisible(false);
			if (GameView::getInstance()->generalBaseMsgList.size()>0)
			{
				generalsListUI->curGeneralBaseMsg->CopyFrom(*(GameView::getInstance()->generalBaseMsgList.at(0)));
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)generalsListUI->curGeneralBaseMsg->id());
				generalsListUI->selectCellId = 0;
				generalsListUI->lastSelectCellId = 0;
				generalsListUI->RefreshGeneralsList();
			}

			mainTab->setDefaultPanelByIndex(1);
		}
		break;
	case 2:
		{
			generalsRecuriteUI->setVisible(false);
			generalsListUI->setVisible(false);
			generalsStrategiesUI->setVisible(false);
			generalsSkillsUI->setVisible(true);
			generalsSkillsUI->setToDefault();

			mainTab->setDefaultPanelByIndex(2);
		}
		break;
	case 3:
		{
			//����ȼ�
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(1);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[1];
			}

			if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				generalsRecuriteUI->setVisible(false);
				generalsListUI->setVisible(false);
				generalsStrategiesUI->setVisible(true);
				generalsSkillsUI->setVisible(false);
				//�ˢ���󷨽����е��佫
				generalsStrategiesUI->RefreshAllGeneralsInLine();
				//generalsStrategiesUI->RefreshListWithoutChangeOffSet();	
				generalsStrategiesUI->RefreshListToDefault();

				mainTab->setDefaultPanelByIndex(3);
			}
			else
			{
// 				std::string str_des = StringDataManager::getString("feature_will_be_open_function");
// 				char str_level[20];
// 				sprintf(str_level,"%d",openlevel);
// 				str_des.append(str_level);
// 				str_des.append(StringDataManager::getString("feature_will_be_open_open"));
// 				GameView::getInstance()->showAlertDialog(str_des.c_str());
			}
		}
		break;
	}

	mainTab->setHightLightLabelColor(Color3B(47,93,13));
	mainTab->setNormalLabelColor(Color3B(255,255,255));
}

void GeneralsUI::IndexChangedEvent(Ref* pSender)
{
	generalsEvolutionUI->setVisible(false);
	generalsTeachUI->setVisible(false);

	mainTab->setHightLightLabelColor(Color3B(47,93,13));
	mainTab->setNormalLabelColor(Color3B(255,255,255));

	switch((int)mainTab->getCurrentIndex())
	{
	case 0 :   //�㽫̨
		{
			generalsRecuriteUI->setVisible(true);
			generalsListUI->setVisible(false);
			generalsStrategiesUI->setVisible(false);
			generalsSkillsUI->setVisible(false);

			auto sc1 = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc1 != NULL)
				sc1->endCommand(mainTab->getChildByTag(150));

		}
		break;   
	case 1 :  //�佫�б
		{
			generalsRecuriteUI->setVisible(false);
			generalsListUI->setVisible(true);
			generalsStrategiesUI->setVisible(false);
			generalsSkillsUI->setVisible(false);

			if (GameView::getInstance()->generalBaseMsgList.size()>0)
				{
				//delete generalsListUI->curGeneralBaseMsg;
				//generalsListUI->curGeneralBaseMsg = new CGeneralBaseMsg();
				generalsListUI->curGeneralBaseMsg->CopyFrom(*(GameView::getInstance()->generalBaseMsgList.at(0)));
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)generalsListUI->curGeneralBaseMsg->id());
				generalsListUI->selectCellId = 0;
				generalsListUI->lastSelectCellId = 0;
				generalsListUI->RefreshGeneralsList();
				}

			auto sc2 = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc2 != NULL)
				sc2->endCommand(mainTab->getChildByTag(151));

		}
		break;
	case 2 ://��佫���
		{
			//ܿ���ȼ�
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(24);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[24];
			}

			if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				generalsRecuriteUI->setVisible(false);
				generalsListUI->setVisible(false);
				generalsStrategiesUI->setVisible(false);
				generalsSkillsUI->setVisible(true);
				generalsSkillsUI->setToDefault();
			}
			else
			{
				std::string str_des = StringDataManager::getString("feature_will_be_open_function");
				char str_level[20];
				sprintf(str_level,"%d",openlevel);
				str_des.append(str_level);
				str_des.append(StringDataManager::getString("feature_will_be_open_open"));
				GameView::getInstance()->showAlertDialog(str_des.c_str());
			}
		}
		break;
	case 3 :  //��
		{
			//󷨿���ȼ�
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(1);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[1];
			}
			
			if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				generalsRecuriteUI->setVisible(false);
				generalsListUI->setVisible(false);
				generalsStrategiesUI->setVisible(true);
				generalsSkillsUI->setVisible(false);
				//�ˢ���󷨽����е��佫
				generalsStrategiesUI->RefreshAllGeneralsInLine();
				//generalsStrategiesUI->RefreshListWithoutChangeOffSet();	
				generalsStrategiesUI->RefreshListToDefault();

				auto sc3 = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
				if(sc3 != NULL)
					sc3->endCommand(mainTab->getChildByTag(153));
			}
			else
			{
				std::string str_des = StringDataManager::getString("feature_will_be_open_function");
				char str_level[20];
				sprintf(str_level,"%d",openlevel);
				str_des.append(str_level);
				str_des.append(StringDataManager::getString("feature_will_be_open_open"));
				GameView::getInstance()->showAlertDialog(str_des.c_str());
			}
		}
		break;
	
	}
}

std::string GeneralsUI::getQuality( int quality )
{
	std::string generals_quality_color_base = "generals_quality_color_";
	std::string generals_quality_base = "generals_quality_";
	std::string string_quality = "";

	if (quality < 11)                                                                //��ɫ
	{
		generals_quality_color_base.append("1");
	}
	else if (quality>10 && quality < 21)                              //��ɫ
	{
		generals_quality_color_base.append("2");
	}
	else if (quality>20 && quality < 31)                              //�ɫ
	{
		generals_quality_color_base.append("3");
	}
	else if (quality>30 && quality < 41)                              //��ɫ
	{
		generals_quality_color_base.append("4");
	}
	else if (quality>40 && quality < 51)                              //��ɫ
	{
		generals_quality_color_base.append("5");
	}
	else
	{

	}

	char s[5];
	if (quality%10 == 0)
	{
		int temp = 10;
		sprintf(s,"%d",temp);
	}
	else
	{
		sprintf(s,"%d",(quality%10));
	}
	
	generals_quality_base.append(s);

	const char *generals_quality_color = StringDataManager::getString(generals_quality_color_base.c_str());
	const char *generals_quality = StringDataManager::getString(generals_quality_base.c_str());
	const char *generals_quality_jie = StringDataManager::getString("generals_quality_jie");
	string_quality.append(generals_quality_color);
	string_quality.append(generals_quality);
	string_quality.append(generals_quality_jie);

	return string_quality;
}

std::string GeneralsUI::getBigHeadFramePath( int quality )
{
	std::string frameNameBase = "res_ui/";
	if (quality < 11)                                                                //��ɫ
	{
		frameNameBase.append("generals_white");
	}
	else if (quality>10 && quality < 21)                              //��ɫ
	{
		frameNameBase.append("generals_green");
	}
	else if (quality>20 && quality < 31)                              //�ɫ
	{
		frameNameBase.append("generals_blue");
	}
	else if (quality>30 && quality < 41)                              //��ɫ
	{
		frameNameBase.append("generals_purple");
	}
	else if (quality>40 && quality < 51)                              //��ɫ
	{
		frameNameBase.append("generals_orange");
	}
	else
	{
		frameNameBase.append("generals_white");
	}

	frameNameBase.append(".png");
	
	return frameNameBase;
}

std::string GeneralsUI::getSmallHeadFramePath( int quality )
{
	std::string frameNameBase = "res_ui/biankuang_";
	if (quality < 11)                                                                //��ɫ
	{
		frameNameBase.append("1");
	}
	else if (quality>10 && quality < 21)                              //��ɫ
	{
		frameNameBase.append("2");
	}
	else if (quality>20 && quality < 31)                              //�ɫ
	{
		frameNameBase.append("3");
	}
	else if (quality>30 && quality < 41)                              //��ɫ
	{
		frameNameBase.append("4");
	}
	else if (quality>40 && quality < 51)                              //��ɫ
	{
		frameNameBase.append("5");
		
	}
	else
	{
		frameNameBase.append("1");
	}

	frameNameBase.append(".png");
	return frameNameBase;
}



 


 //�
//  void GeneralsUI::EvolutionEvent( Ref * pSender )
//  {
// 	 panel_recruit->setVisible(false);
//	 Layer_recruit->setVisible(false);

//	 panel_generalsList->setVisible(false);
//	 Layer_generalsList->setVisible(false);

//	 panel_generalsInfo->setVisible(false);
// 
// 	 panel_teach->setVisible(false);
// 	 Layer_teach->setVisible(false);
// 
// 	 panel_evolution->setVisible(true);
// 	 Layer_evolution->setVisible(true);
//  }
//  //���
//  void GeneralsUI::TeachEvent( Ref * pSender )
//  {
// 	 panel_recruit->setVisible(false);
//	 Layer_recruit->setVisible(false);

//	 panel_generalsList->setVisible(false);
//	 Layer_generalsList->setVisible(false);

//	 panel_generalsInfo->setVisible(false);
// 
// 	 panel_evolution->setVisible(false);
// 	 Layer_evolution->setVisible(false);
// 
// 	 panel_teach->setVisible(true);
// 	 Layer_teach->setVisible(true);
// 
//  }
//  //�ָ�
//  void GeneralsUI::RegainEvent( Ref * pSender )
//  {
// 
//  }


void GeneralsUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralsUI::addCCTutorialIndicator1( const char* content,Vec2 pos ,CCTutorialIndicator::Direction direction)
{
	auto winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,50,80);
// 	tutorialIndicator->setPosition(Vec2(pos.x-45+_w,pos.y+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,36,90,true,true,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+23+_w,pos.y-47+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralsUI::addCCTutorialIndicator2( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	auto winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,15,100);
// 	tutorialIndicator->setPosition(Vec2(pos.x-25+_w,pos.y-22+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,36,90,true,true,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+23+_w,pos.y-142+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralsUI::addCCTutorialIndicator3( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	auto winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	tutorialIndicator->setPosition(Vec2(pos.x-60+_w,pos.y-75+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,40,40,true,true,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralsUI::addCCTutorialIndicator4( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	auto winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,15,100);
// 	tutorialIndicator->setPosition(Vec2(pos.x-25+_w,pos.y-210+_h));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,36,90,true,true,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+23+_w,pos.y-332+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralsUI::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}


 /////////////////////////////////////////////////////

 GeneralsListCell::GeneralsListCell()
	 :m_index(0)
	 ,m_nNowState(0)
	 ,m_label_generalShow(NULL)
 {

 }

 GeneralsListCell::~GeneralsListCell()
 {
	 delete curGeneralBaseMsg;
 }

 GeneralsListCell* GeneralsListCell::create(int index,CGeneralBaseMsg * generalBaseMsg)
 {
	 auto generalsListCell = new GeneralsListCell();
	 if (generalsListCell && generalsListCell->init(index,generalBaseMsg))
	 {
		 generalsListCell->autorelease();
		 return generalsListCell;
	 }
	 CC_SAFE_DELETE(generalsListCell);
	 return NULL;
 }

 bool GeneralsListCell::init(int index,CGeneralBaseMsg * generalBaseMsg)
 {
	 if (TableViewCell::init())
	 {
		 m_index = index;
		 curGeneralBaseMsg = new CGeneralBaseMsg();
		 curGeneralBaseMsg->CopyFrom(*generalBaseMsg);
		 auto generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];
		 //��߿
		 auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang01_new.png");
		 sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
		 sprite_bigFrame->setPosition(Vec2(0, 0));
		 sprite_bigFrame->setCapInsets(Rect(15,31,1,1));
		 sprite_bigFrame->setPreferredSize(Size(235,72));
		 addChild(sprite_bigFrame);
		 
// 		 //�С�߿
		 auto  smaillFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_allb.png");
		 smaillFrame->setAnchorPoint(Vec2(0, 0));
		 smaillFrame->setPosition(Vec2(12, 9));
		 smaillFrame->setCapInsets(Rect(15,15,1,1));
		 smaillFrame->setContentSize(Size(52,52));
		 addChild(smaillFrame);

		 auto sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_diaa.png");
		 sprite_nameFrame->setAnchorPoint(Vec2(0, 0));
		 sprite_nameFrame->setPosition(Vec2(85, 37));
		 sprite_nameFrame->setCapInsets(Rect(11,11,1,1));
		 sprite_nameFrame->setContentSize(Size(144,26));
		 sprite_bigFrame->addChild(sprite_nameFrame);

// 		 //����ɫ��С�߿
// 		Rect cr = Rect(9.5f,9.5f,6.0f,6.0f);
// 		auto  sprite_smallFrame = cocos2d::extension::Scale9Sprite::create(GeneralsUI::getSmallHeadFramePath(generalBaseMsg->currentquality()).c_str());
// 		sprite_smallFrame->setAnchorPoint(Vec2(0, 0));
// 		sprite_smallFrame->setPosition(Vec2(12, 13));
// 		sprite_smallFrame->setPreferredSize(Size(59,66));
// 		sprite_smallFrame->setCapInsets(cr);
// 		addChild(sprite_smallFrame);
		 

		 //��б��е�ͷ��ͼ�
		 std::string icon_path = "res_ui/generals46X45/";
		 icon_path.append(generalMsgFromDb->get_head_photo());
		 icon_path.append(".png");
		 sprite_icon = Sprite::create(icon_path.c_str());
		 sprite_icon->setAnchorPoint(Vec2(0, 0));
		 sprite_icon->setPosition(Vec2(16, 13));
		 addChild(sprite_icon);

		 //�ȼ�
		 auto sprite_lvFrame = Sprite::create("res_ui/lv_kuang.png");
		 sprite_lvFrame->setAnchorPoint(Vec2(0, 0));
		 sprite_lvFrame->setPosition(Vec2(49, 4));
		 addChild(sprite_lvFrame);

		 std::string _lv = "LV";
		 char s_level [5];
		 sprintf(s_level,"%d",generalBaseMsg->level());
		 _lv.append(s_level);
		 label_level = Label::createWithTTF(s_level,APP_FONT_NAME,12);
		 auto shadowColor = Color4B::BLACK;   // black
		 label_level->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		 label_level->setAnchorPoint(Vec2(0.5f,0.5f));
		 label_level->setPosition(Vec2(63,11));
		 addChild(label_level);
		 //���
		 if (generalBaseMsg->evolution() > 0)
		 {
			 auto imageView_rank = ActorUtils::createGeneralRankSprite(generalBaseMsg->evolution());
			 imageView_rank->setAnchorPoint(Vec2(1.0f,1.0f));
			 imageView_rank->setPosition(Vec2(75,69));
			 imageView_rank->setScale(0.75f);
			 imageView_rank->setTag(kTagRankImageView);
			 addChild(imageView_rank);
		 }
		 
		 //�ϡ�ж
		 if (generalBaseMsg->rare()>0)
		 {
			 auto imageView_star = Sprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			 imageView_star->setAnchorPoint(Vec2(0.5f,0.5f));
			 imageView_star->setPosition(Vec2(15,15));
			 imageView_star->setScale(0.5f);
			 imageView_star->setTag(kTagStarImageView);
			 addChild(imageView_star);
		 }

		 //��佫���
		 label_name = Label::createWithTTF(generalMsgFromDb->name().c_str(),APP_FONT_NAME,16);
		 label_name->setAnchorPoint(Vec2(0, 0));
		 label_name->setPosition(Vec2(92, 40));
		 label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		 addChild(label_name);
		 //��佫Ʒ�
		 std::string general_rank = GeneralsUI::getQuality(generalBaseMsg->currentquality());
		 label_rank = Label::createWithTTF(general_rank.c_str(),APP_FONT_NAME,16);
		 label_rank->setAnchorPoint(Vec2(0, 0));
		 label_rank->setPosition(Vec2(160, 40));
		 label_rank->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		 sprite_bigFrame->addChild(label_rank);

		 auto  s_normalImage_1 = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_1.png");
		 s_normalImage_1->setPreferredSize(Size(80,36));
		 s_normalImage_1->setCapInsets(Rect(18,9,2,23));
		 auto firstMenuImage = MenuItemSprite::create(s_normalImage_1, s_normalImage_1, s_normalImage_1, CC_CALLBACK_1(GeneralsListCell::FirstMenuEvent,this));
		 firstMenuImage->setScale(1.3f);
		 firstMenuImage->setAnchorPoint(Vec2(0.5f,0.5f));
		 firstMenu = CCMoveableMenu::create(firstMenuImage, NULL);
		 firstMenu->setContentSize(Size(80,36));
		 firstMenu->setPosition(Vec2(189,15));
		 firstMenu->setScale(0.8f);
		 sprite_bigFrame->addChild(firstMenu);
		 label_firstMenu = Label::createWithTTF("up",APP_FONT_NAME,16);
		 label_firstMenu->setAnchorPoint(Vec2(.5f,.5f));
		 label_firstMenu->setPosition(Vec2(40,18));
		 label_firstMenu->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		 firstMenuImage->addChild(label_firstMenu);

// 		 auto  s_normalImage_2 = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_1.png");
// 		 s_normalImage_2->setPreferredSize(Size(80,36));
// 		 s_normalImage_2->setCapInsets(Rect(18,9,2,23));
// 		 MenuItemSprite * secondMenuImage = MenuItemSprite::create(s_normalImage_2,s_normalImage_2,s_normalImage_2,this,menu_selector(GeneralsListCell::SecondMenuEvent));
// 		 secondMenuImage->setZoomScale(1.3f);
// 		 secondMenuImage->setAnchorPoint(Vec2(0.5f,0.5f));
// 		 secondMenu = CCMoveableMenu::create(secondMenuImage, NULL);
// 		 secondMenu->setContentSize(Size(80,36));
// 		 secondMenu->setPosition(Vec2(189,15));
// 		 secondMenu->setScale(0.8f);
// 		 sprite_bigFrame->addChild(secondMenu);
// 		 label_secondMenu = Label::createWithTTF("down",APP_FONT_NAME,16);
// 		 label_secondMenu->setAnchorPoint(Vec2(.5f,.5f));
// 		 label_secondMenu->setPosition(Vec2(40,18));
// 		 label_secondMenu->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
// 		 secondMenuImage->addChild(label_secondMenu);

		 if (generalBaseMsg->fightstatus() == GeneralsListUI::NoBattle)
		 {
			 const char *str1 = StringDataManager::getString("generals_fighttype_shangzhen");
			 char* p1 =const_cast<char*>(str1);
			 label_firstMenu->setString(p1);
		 }
		 else
		 {
			 const char *str1 = StringDataManager::getString("generals_fighttype_xiuxi");
			 char* p1 =const_cast<char*>(str1);
			 label_firstMenu->setString(p1);

			//������ʶ
			auto imageView_inLine= Sprite::create("res_ui/wujiang/play.png");
			imageView_inLine->setScale(0.8f);
			imageView_inLine->setAnchorPoint(Vec2(0.5,0.5));
			imageView_inLine->setPosition(Vec2(16,57));
			imageView_inLine->setTag(kTagInBattleFlag);
			addChild(imageView_inLine);
		 }
		 
		 // չʾ�(�չʾ�佫�ı�ʶ)
		 const char *str_general_show = StringDataManager::getString("generals_show_zhanshizhong");
		 m_label_generalShow = Label::createWithTTF(str_general_show, APP_FONT_NAME, 16);
		 m_label_generalShow->setAnchorPoint(Vec2(0.5f, 0.5f));
		 m_label_generalShow->setPosition(Vec2(109, 18));
		 addChild(m_label_generalShow);


		 // 1:��չʾ��0���ջأ�2��չʾ����ť��أ�
		 int nState = ShowMyGeneral::getInstance()->getShowState(generalBaseMsg->id());
		 if (1 == nState)
		 {
			 m_nNowState = 1;

			 m_label_generalShow->setVisible(false);
		 }
		 else if(0 == nState)
		 {
			 m_nNowState = 0;

			 m_label_generalShow->setVisible(true);
		 }

		 return true;
		
	 }
	 return false;
 }

 void GeneralsListCell::FirstMenuEvent( Ref * pSender )
 {
	 if (curGeneralBaseMsg->fightstatus() == GeneralsListUI::NoBattle)
	 {
		 auto generalsui = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
		 auto generalsListUI = GeneralsUI::generalsListUI;
		 if (generalsListUI->isFirstOrSecondMenu)
		 {
			 if (generalsListUI->isFirstTutorial)
			 {
				 if (m_index == 0)
				 {
					 auto sc = ScriptManager::getInstance()->getScriptById(generalsListUI->mTutorialScriptInstanceId);
					 if(sc != NULL)
						 sc->endCommand(generalsListUI->generalList_tableView);
				 }
			 }
			 if (generalsListUI->isSecondTutorial)
			 {
				 if (m_index == 2)
				 {
					 auto sc = ScriptManager::getInstance()->getScriptById(generalsListUI->mTutorialScriptInstanceId);
					 if(sc != NULL)
						 sc->endCommand(generalsListUI->generalList_tableView);
				 }
			 }
		 }

		 bool isCan = false;
		 //����ȼ�
		 int openlevel = -1;
		 switch(GameView::getInstance()->generalsInLineList.size())
		 {
		 case 0:
			 {
				 std::map<int,int>::const_iterator cIter;
				 cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(8);
				 if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
				 {
				 }
				 else
				 {
					 openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[8];
					 if (GameView::getInstance()->myplayer->getActiveRole()->level()>=openlevel)
						 isCan = true;
				 }
			 }
			 break;
		 case 1:
			 {
				 std::map<int,int>::const_iterator cIter;
				 cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(9);
				 if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // �û�ҵ�����ָ�END��  
				 {
				 }
				 else
				 {
					 openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[9];
					 if (GameView::getInstance()->myplayer->getActiveRole()->level()>=openlevel)
						 isCan = true;
				 }
			 }
			 break;
		 case 2:
			 {
				 std::map<int,int>::const_iterator cIter;
				 cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(10);
				 if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // �û�ҵ�����ָ�END��  
				 {
				 }
				 else
				 {
					 openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[10];
					 if (GameView::getInstance()->myplayer->getActiveRole()->level()>=openlevel)
						 isCan = true;
				 }
			 }
			 break;
		 case 3:
			 {
				 std::map<int,int>::const_iterator cIter;
				 cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(11);
				 if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // �û�ҵ�����ָ�END��  
				 {
				 }
				 else
				 {
					 openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[11];
					 if (GameView::getInstance()->myplayer->getActiveRole()->level()>=openlevel)
						 isCan = true;
				 }
			 }
			 break;
		 case 4:
			 {
				 std::map<int,int>::const_iterator cIter;
				 cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(12);
				 if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // �û�ҵ�����ָ�END��  
				 {
				 }
				 else
				 {
					 openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[12];
					 if (GameView::getInstance()->myplayer->getActiveRole()->level()>=openlevel)
						 isCan = true;
				 }
			 }
			 break;
		 }

		 if (GameView::getInstance()->generalsInLineList.size() < 5)
		 {
			 if (isCan)
			 {
				 GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)curGeneralBaseMsg->id(),(void *)1);
			 }
			 else
			 {
				 std::string des = "";
				 char s_openLevel[20];
				 sprintf(s_openLevel,"%d",openlevel);
				 des.append(s_openLevel);
				 des.append(StringDataManager::getString("generals_dialog_onLine_1"));
				 char s_num[20];
				 sprintf(s_num,"%d",GameView::getInstance()->generalsInLineList.size()+1);
				 des.append(s_num);
				 des.append(StringDataManager::getString("generals_dialog_onLine_2"));
				 GameView::getInstance()->showAlertDialog(des.c_str());
			 }
		 }
		 else
		 {
			 GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)curGeneralBaseMsg->id(),(void *)1);
		 }
		
	 }
	 else
	 {
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)curGeneralBaseMsg->id(),(void *)0);
	 }
 }

 void GeneralsListCell::SecondMenuEvent( Ref * pSender )
 {
	 if(curGeneralBaseMsg->fightstatus() == GeneralsListUI::InBattle)
	 {
		 GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)curGeneralBaseMsg->id(),(void *)1);
	 }
	 else if (curGeneralBaseMsg->fightstatus() == GeneralsListUI::HoldTheLine)
	 {
		 auto generalsui = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);
		 auto generalsListUI = GeneralsUI::generalsListUI;
		 if (generalsListUI->isFirstOrSecondMenu != true)
		 {
			 if (generalsListUI->isFirstTutorial)
			 {
				 if (m_index == 0)
				 {
					 auto sc = ScriptManager::getInstance()->getScriptById(generalsListUI->mTutorialScriptInstanceId);
					 if(sc != NULL)
						 sc->endCommand(generalsListUI->generalList_tableView);
				 }
			 }
			
			 if (generalsListUI->isSecondTutorial)
			 {
				 if (m_index == 1)
				 {
					 auto sc = ScriptManager::getInstance()->getScriptById(generalsListUI->mTutorialScriptInstanceId);
					 if(sc != NULL)
						 sc->endCommand(generalsListUI->generalList_tableView);
				 }
			 }
			 
		 }
		 
		 GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)curGeneralBaseMsg->id(),(void *)2);
	 }
 }

 CGeneralBaseMsg* GeneralsListCell::getCurGeneralBaseMsg()
 {
	 return this->curGeneralBaseMsg;
 }

 void GeneralsListCell::RefreshCell( int index,CGeneralBaseMsg * generalBaseMsg )
 {
	 m_index = index;
	 curGeneralBaseMsg->CopyFrom(*generalBaseMsg);
	 auto generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];

	 //��б��е�ͷ��ͼ�
	 std::string icon_path = "res_ui/generals46X45/";
	 icon_path.append(generalMsgFromDb->get_head_photo());
	 icon_path.append(".png");
	 auto texture = Director::getInstance()->getTextureCache()->addImage(icon_path.c_str());
	 sprite_icon->setTexture(texture);

	 //�ȼ�
	 std::string _lv = "LV";
	 char s_level [5];
	 sprintf(s_level,"%d",generalBaseMsg->level());
	 _lv.append(s_level);
	 label_level->setString(s_level);

	 //���
	 if (this->getChildByTag(kTagRankImageView))
	 {
		 this->getChildByTag(kTagRankImageView)->removeFromParent();
	 }

	 if (generalBaseMsg->evolution() > 0)
	 {
		 auto imageView_rank = ActorUtils::createGeneralRankSprite(generalBaseMsg->evolution());
		 imageView_rank->setAnchorPoint(Vec2(1.0f,1.0f));
		 imageView_rank->setPosition(Vec2(75,69));
		 imageView_rank->setScale(0.75f);
		 imageView_rank->setTag(kTagRankImageView);
		 addChild(imageView_rank);
	 }

	 //�ϡ�ж
	 if (this->getChildByTag(kTagStarImageView))
	 {
		 this->getChildByTag(kTagStarImageView)->removeFromParent();
	 }

	 if (generalBaseMsg->rare()>0)
	 {
		 auto imageView_star = Sprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
		 imageView_star->setAnchorPoint(Vec2(0.5f,0.5f));
		 imageView_star->setPosition(Vec2(15,15));
		 imageView_star->setScale(0.5f);
		 imageView_star->setTag(kTagStarImageView);
		 addChild(imageView_star);
	 }

	 //��佫���
	 label_name->setString(generalMsgFromDb->name().c_str());
	 label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
	 //��佫Ʒ�
	 std::string general_rank = GeneralsUI::getQuality(generalBaseMsg->currentquality());
	 label_rank->setString(general_rank.c_str());
	 label_rank->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));

	 if (this->getChildByTag(kTagInBattleFlag))
	 {
		 this->getChildByTag(kTagInBattleFlag)->removeFromParent();
	 }

	 if (generalBaseMsg->fightstatus() == GeneralsListUI::NoBattle)
	 {
		 const char *str1 = StringDataManager::getString("generals_fighttype_shangzhen");
		 char* p1 =const_cast<char*>(str1);
		 label_firstMenu->setString(p1);
	 }
	 else
	 {
		 const char *str1 = StringDataManager::getString("generals_fighttype_xiuxi");
		 char* p1 =const_cast<char*>(str1);
		 label_firstMenu->setString(p1);

 		//������ʶ
 		auto imageView_inLine = Sprite::create("res_ui/wujiang/play.png");
 		imageView_inLine->setScale(0.8f);
 		imageView_inLine->setAnchorPoint(Vec2(0.5,0.5));
 		imageView_inLine->setPosition(Vec2(16,57));
 		imageView_inLine->setTag(kTagInBattleFlag);
 		addChild(imageView_inLine);
	 }

	 // 1:��չʾ��0���ջأ�2��չʾ����ť��أ�
	 int nState = ShowMyGeneral::getInstance()->getShowState(generalBaseMsg->id());
	 if (1 == nState)
	 {
		 m_nNowState = 1;

		 m_label_generalShow->setVisible(false);
	 }
	 else if(0 == nState)
	 {
		 m_nNowState = 0;

		 m_label_generalShow->setVisible(true);
	 }
 }


 


