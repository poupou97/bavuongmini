#ifndef  _GENERALSUI_GENERALSLISTBASE_H_
#define _GENERALSUI_GENERALSLISTBASE_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGeneralBaseMsg;
class CGeneralDetail;

/////////////////////////////////
/**
 * �佫�����µġ��ҵ��佫���е��佫�б��
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.3
 */

class GeneralsListBase : public UIScene
{
public:
	GeneralsListBase();
	~GeneralsListBase();
	static GeneralsListBase* create();
	bool init();

	//��佫��״̬
	enum GeneralsFightStatus{
		NoBattle = 0,
		HoldTheLine,
		InBattle 
	};

	//�佫�б��Ƿ�����һҳ����һҳ
	enum GeneralsListStatus{
		HaveLast = 0,
		HaveNext,
		HaveBoth,
		HaveNone,
	};

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	//���Listµ�״̬
	//page   ��ǰҳ�
	//allPageNum   ���ҳ�
	void RefreshGeneralsListStatus( int page, int allPageNum);

	bool getIsCanReq();
	void setIsCanReq(bool _value);

private:
	//�������Ƿ��Ѿ����أ��Ѿ�������ݲ��ܼ������
	bool isCanReq;

public:
	GeneralsListStatus  generalsListStatus;
	int everyPageNum;

	int s_mCurPage;
	int s_mAllPageNum;

	//���������������ݻ��Ǽ�����ӵڶ�ҳ...
	bool isReqNewly;

};

#endif

