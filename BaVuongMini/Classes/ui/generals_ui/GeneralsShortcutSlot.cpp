#include "GeneralsShortcutSlot.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "GeneralsUI.h"
#include "generals_popup_ui/GeneralsSkillInfoUI.h"
#include "../../gamescene_state/MainScene.h"
#include "generals_popup_ui/GeneralsSkillListUI.h"
#include "GeneralsListUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "GeneralsShortcutLayer.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "AppMacros.h"
#include "../../utils/StrUtils.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../gamescene_state/role/MyPlayer.h"


GeneralsShortcutSlot::GeneralsShortcutSlot():
skillType(Actived),
skillState(NotOpen),
skillId(""),
index(-1)
{
}


GeneralsShortcutSlot::~GeneralsShortcutSlot()
{
}

GeneralsShortcutSlot* GeneralsShortcutSlot::create(int quality,CShortCut * shortcut)
{
	auto generalsShortcutSlot = new GeneralsShortcutSlot();
	if (generalsShortcutSlot && generalsShortcutSlot->init(quality,shortcut))
	{
		generalsShortcutSlot->autorelease();
		return generalsShortcutSlot;
	}
	CC_SAFE_DELETE(generalsShortcutSlot);
	return NULL;
}

GeneralsShortcutSlot* GeneralsShortcutSlot::create( int quality,int slotIndex )
{
	auto generalsShortcutSlot = new GeneralsShortcutSlot();
	if (generalsShortcutSlot && generalsShortcutSlot->init(quality,slotIndex))
	{
		generalsShortcutSlot->autorelease();
		return generalsShortcutSlot;
	}
	CC_SAFE_DELETE(generalsShortcutSlot);
	return NULL;
}

GeneralsShortcutSlot* GeneralsShortcutSlot::create( GameFightSkill * gameFightSkill )
{
	auto generalsShortcutSlot = new GeneralsShortcutSlot();
	if (generalsShortcutSlot && generalsShortcutSlot->init(gameFightSkill))
	{
		generalsShortcutSlot->autorelease();
		return generalsShortcutSlot;
	}
	CC_SAFE_DELETE(generalsShortcutSlot);
	return NULL;
}

bool GeneralsShortcutSlot::init(int quality,CShortCut * shortcut)
{
	if (UIScene::init())
	{
		skillId = shortcut->skillpropid();
		index = shortcut->index();

		UpdateSkillType(shortcut->index());
		UpdateSkillState(shortcut,quality);

		auto baseSkill = StaticDataBaseSkill::s_baseSkillData[skillId];
		CCAssert(baseSkill != NULL, "baseSkill should not be null");

		//�
		std::string frameColorPath;
		if (baseSkill->get_quality() == 1)
		{
			frameColorPath = GENERALSKILLWhiteFramePath;
		}
		else if (baseSkill->get_quality()  == 2)
		{
			frameColorPath = GENERALSKILLGreenFramePath;
		}
		else if (baseSkill->get_quality()  == 3)
		{
			frameColorPath = GENERALSKILLBlueFramePath;
		}
		else if (baseSkill->get_quality()  == 4)
		{
			frameColorPath = GENERALSKILLPurpleFramePath;
		}
		else if (baseSkill->get_quality()  == 5)
		{
			frameColorPath = GENERALSKILLOrangeFramePath;
		}
		else
		{
			frameColorPath = GENERALSKILLVoidFramePath;
		}
		btn_bgFrame = Button::create();
		btn_bgFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		btn_bgFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_bgFrame->setPosition(Vec2(btn_bgFrame->getContentSize().width/2,btn_bgFrame->getContentSize().height/2));
		btn_bgFrame->setTouchEnabled(true);
		btn_bgFrame->setPressedActionEnabled(true);
		btn_bgFrame->addTouchEventListener(CC_CALLBACK_2(GeneralsShortcutSlot::DoThing, this));
		m_pLayer->addChild(btn_bgFrame);

		

		std::string skillIconPath = "res_ui/jineng_icon/";

		if (this->skillState == Used)
		{
// 			if (strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 				||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 				||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 				||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 			{
// 				skillIconPath.append("jineng_2");
// 			}
// 			else
// 			{
				auto baseSkill = StaticDataBaseSkill::s_baseSkillData[skillId];
				if (baseSkill)
				{
					skillIconPath.append(baseSkill->icon());
				}
				else
				{
					skillIconPath.append("jineng_2");
				}
				
//			}

			skillIconPath.append(".png");
		}
		else if (this->skillState == Opened)
		{
			skillIconPath = "res_ui/plus_general.png";
		}
		else if (this->skillState == NotOpen)
		{
			skillIconPath = "res_ui/suo.png";
		}
		
		auto imageView_skill = ImageView::create();
		imageView_skill->loadTexture(skillIconPath.c_str());
		imageView_skill->setAnchorPoint(Vec2(0.5f,0.5f));
// 		if (this->skillState == Opened)
// 		{
// 			imageView_skill->setScale(0.7f);
// 		}
// 		else
// 		{
// 			imageView_skill->setScale(1.0f);
// 		}

		imageView_skill->setPosition(Vec2(0,0));
		btn_bgFrame->addChild(imageView_skill);

// 		ImageView * imageView_hightLight = ImageView::create();
// 		imageView_hightLight->loadTexture(HIGHLIGHT3);
// 		imageView_hightLight->setAnchorPoint(Vec2(0.5f,0.5f));
// 		imageView_hightLight->setPosition(Vec2(0,0));
// 		btn_bgFrame->addChild(imageView_hightLight);

		this->setContentSize(btn_bgFrame->getContentSize());

		//skill variety icon
		auto image_skillVariety = SkillScene::GetSkillVarirtyImageView(skillId.c_str());
		if (image_skillVariety)
		{
			image_skillVariety->setAnchorPoint(Vec2(0.5f,0.5f));
			image_skillVariety->setPosition(Vec2(15,-4));
			image_skillVariety->setScale(0.85f);
			btn_bgFrame->addChild(image_skillVariety);
		}

		if (this->skillType == Actived)
		{
			this->setScale(1.0f);
		}
		else if (this->skillType == Passived)
		{
			//򱻶�
			auto imageView_passived = ImageView::create();
			imageView_passived->loadTexture("res_ui/jineng/xiaoyuanquan_bei.png");
			imageView_passived->setAnchorPoint(Vec2(0.5f,0.5f));
			imageView_passived->setPosition(Vec2(-15,23));
			btn_bgFrame->addChild(imageView_passived);

			this->setScale(0.8f);
		}
		else{}

		//�Ѿ��м��
		std::string str_lv = "";
		if (this->skillState == Used)
		{
			if (strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
				||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
				||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
				||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)  //�����ͨ���1/1��
			{
				str_lv.append("1/1");
			}
			else
			{
				char s_curLv [5];
				sprintf(s_curLv,"%d",shortcut->skilllevel());
				str_lv.append(s_curLv);
				str_lv.append("/");
				auto baseSkill = StaticDataBaseSkill::s_baseSkillData[skillId];
				if (baseSkill)
				{
					char max_lv [5];
					sprintf(max_lv,"%d",baseSkill->maxlevel());
					str_lv.append(max_lv);
				}
				else
				{
					skillIconPath.append("10");
				}
			}

			auto imageView_lvFrame = ImageView::create();
			imageView_lvFrame->loadTexture("res_ui/jineng/jineng_zhezhao.png");
			imageView_lvFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
			imageView_lvFrame->setPosition(Vec2(0, -18));
// 			imageView_lvFrame->setScale9Enabled(true);
// 			imageView_lvFrame->setContentSize(Size(32,11));
			btn_bgFrame->addChild(imageView_lvFrame);

			auto l_lv = Label::createWithTTF(str_lv.c_str(), APP_FONT_NAME, 12);
			l_lv->setAnchorPoint(Vec2(0.5f,0.5f));
			l_lv->setPosition(Vec2(imageView_lvFrame->getPosition().x,imageView_lvFrame->getPosition().y));
			btn_bgFrame->addChild(l_lv);

		}

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(GeneralsShortcutSlot::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(GeneralsShortcutSlot::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(GeneralsShortcutSlot::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(GeneralsShortcutSlot::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
		
		return true;
	}
	return false;
}

bool GeneralsShortcutSlot::init( int quality,int slotIndex )
{
	if (UIScene::init())
	{
		index = slotIndex;

		UpdateSkillType(slotIndex);
		UpdateSkillState(slotIndex,quality);

		//�
		btn_bgFrame = Button::create();
		btn_bgFrame->loadTextures(GENERALSKILLEMPTYPATH,GENERALSKILLEMPTYPATH,"");
		btn_bgFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_bgFrame->setPosition(Vec2(btn_bgFrame->getContentSize().width/2,btn_bgFrame->getContentSize().height/2));
		btn_bgFrame->setTouchEnabled(true);
		btn_bgFrame->setPressedActionEnabled(true);
		btn_bgFrame->addTouchEventListener(CC_CALLBACK_2(GeneralsShortcutSlot::DoThing, this));
		m_pLayer->addChild(btn_bgFrame);

		std::string skillIconPath = "";

		if (this->skillState == Used)
		{
		}
		else if (this->skillState == Opened)
		{
			skillIconPath = "res_ui/plus_general.png";
		}
		else if (this->skillState == NotOpen)
		{
			skillIconPath = "res_ui/suo.png";
		}

		auto imageView_skill = ImageView::create();
		imageView_skill->loadTexture(skillIconPath.c_str());
		imageView_skill->setAnchorPoint(Vec2(0.5f,0.5f));

// 		if (this->skillState == Opened)
// 		{
// 			imageView_skill->setScale(0.7f);
// 		}
// 		else
// 		{
// 			imageView_skill->setScale(1.0f);
// 		}

		imageView_skill->setPosition(Vec2(0,0));
		btn_bgFrame->addChild(imageView_skill);

// 		ImageView * imageView_hightLight = ImageView::create();
// 		imageView_hightLight->loadTexture(HIGHLIGHT3);
// 		imageView_hightLight->setAnchorPoint(Vec2(0.5f,0.5f));
// 		imageView_hightLight->setPosition(Vec2(0,0));
// 		btn_bgFrame->addChild(imageView_hightLight);

		this->setContentSize(btn_bgFrame->getContentSize());

		if (this->skillType == Actived)
		{
			this->setScale(1.0f);
		}
		else if (this->skillType == Passived)
		{
			//򱻶�
			auto imageView_passived = ImageView::create();
			imageView_passived->loadTexture("res_ui/jineng/xiaoyuanquan_bei.png");
			imageView_passived->setAnchorPoint(Vec2(0.5f,0.5f));
			imageView_passived->setPosition(Vec2(-15,23));
			btn_bgFrame->addChild(imageView_passived);

			this->setScale(0.8f);
		}
		else{}

		return true;
	}
	return false;
}

bool GeneralsShortcutSlot::init( GameFightSkill * gameFightSkill )
{
	if (UIScene::init())
	{
		auto baseSkill = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getId()];
		CCAssert(baseSkill != NULL, "baseSkill should not be null");

		//�
		std::string frameColorPath;
		if (baseSkill->get_quality() == 1)
		{
			frameColorPath = GENERALSKILLWhiteFramePath;
		}
		else if (baseSkill->get_quality()  == 2)
		{
			frameColorPath = GENERALSKILLGreenFramePath;
		}
		else if (baseSkill->get_quality()  == 3)
		{
			frameColorPath = GENERALSKILLBlueFramePath;
		}
		else if (baseSkill->get_quality()  == 4)
		{
			frameColorPath = GENERALSKILLPurpleFramePath;
		}
		else if (baseSkill->get_quality()  == 5)
		{
			frameColorPath = GENERALSKILLOrangeFramePath;
		}
		else
		{
			frameColorPath = GENERALSKILLVoidFramePath;
		}
		btn_bgFrame = Button::create();
		btn_bgFrame->loadTextures(frameColorPath.c_str(),frameColorPath.c_str(),"");
		btn_bgFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_bgFrame->setPosition(Vec2(btn_bgFrame->getContentSize().width/2,btn_bgFrame->getContentSize().height/2));
		btn_bgFrame->setTouchEnabled(true);
		btn_bgFrame->setPressedActionEnabled(true);
		btn_bgFrame->addTouchEventListener(CC_CALLBACK_2(GeneralsShortcutSlot::DoThing, this));
		m_pLayer->addChild(btn_bgFrame);

		std::string skillIconPath = "res_ui/jineng_icon/";

// 		if (strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 			||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 			||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 			||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 		{
// 			skillIconPath.append("jineng_2");
// 			this->skillType = Actived;
// 		}
// 		else
// 		{
			if (baseSkill)
			{
				skillIconPath.append(baseSkill->icon());
			}
			else
			{
				skillIconPath.append("jineng_2");
			}

			if (baseSkill->usemodel() == 0)
			{
				this->skillType = Actived;
			}
			else
			{
				this->skillType = Passived;
			}

//		}
		skillIconPath.append(".png");

		auto imageView_skill = ImageView::create();
		imageView_skill->loadTexture(skillIconPath.c_str());
		imageView_skill->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_skill->setPosition(Vec2(0,0));
		btn_bgFrame->addChild(imageView_skill);

		this->setContentSize(btn_bgFrame->getContentSize());

		//skill variety icon
		auto image_skillVariety = SkillScene::GetSkillVarirtyImageView(gameFightSkill->getId().c_str());
		if (image_skillVariety)
		{
			image_skillVariety->setAnchorPoint(Vec2(0.5f,0.5f));
			image_skillVariety->setPosition(Vec2(15,-4));
			image_skillVariety->setScale(0.85f);
			btn_bgFrame->addChild(image_skillVariety);
		}

		if (this->skillType == Actived)
		{
			this->setScale(1.0f);
		}
		else if (this->skillType == Passived)
		{
			//򱻶�
			auto imageView_passived = ImageView::create();
			imageView_passived->loadTexture("res_ui/jineng/xiaoyuanquan_bei.png");
			imageView_passived->setAnchorPoint(Vec2(0.5f,0.5f));
			imageView_passived->setPosition(Vec2(-15,23));
			btn_bgFrame->addChild(imageView_passived);

			this->setScale(0.8f);
		}
		else{}

		//�Ѿ��м��
		std::string str_lv = "";
		if (strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
			||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
			||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
			||strcmp(gameFightSkill->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)  //�����ͨ���1/1��
		{
			str_lv.append("1/1");
		}
		else
		{
			char s_curLv [5];
			sprintf(s_curLv,"%d",gameFightSkill->getLevel());
			str_lv.append(s_curLv);
			str_lv.append("/");
			if (baseSkill)
			{
				char max_lv [5];
				sprintf(max_lv,"%d",baseSkill->maxlevel());
				str_lv.append(max_lv);
			}
			else
			{
				skillIconPath.append("10");
			}
		}

		auto imageView_lvFrame = ImageView::create();
		imageView_lvFrame->loadTexture("res_ui/jineng/jineng_zhezhao.png");
		imageView_lvFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
		imageView_lvFrame->setPosition(Vec2(0, -18));
// 		imageView_lvFrame->setScale9Enabled(true);
// 		imageView_lvFrame->setContentSize(Size(32,11));
		btn_bgFrame->addChild(imageView_lvFrame);

		auto l_lv = Label::createWithTTF(str_lv.c_str(), APP_FONT_NAME, 12);
		l_lv->setAnchorPoint(Vec2(0.5f,0.5f));
		l_lv->setPosition(Vec2(imageView_lvFrame->getPosition().x,imageView_lvFrame->getPosition().y));
		btn_bgFrame->addChild(l_lv);

		return true;
	}
	return false;
}

void GeneralsShortcutSlot::onEnter()
{
	UIScene::onEnter();
}
void GeneralsShortcutSlot::onExit()
{
	UIScene::onExit();
}	

bool GeneralsShortcutSlot::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return false;	
}
void GeneralsShortcutSlot::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void GeneralsShortcutSlot::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void GeneralsShortcutSlot::onTouchCancelled(Touch *pTouch, Event *pEvent)
{
}

void GeneralsShortcutSlot::UpdateSkillType( int slotIndex )
{
// 	if (slotIndex%2 == 0)
// 	{
// 		skillType = Actived;
// 	}
// 	else if (slotIndex%2 == 1)
// 	{
// 		skillType = Passived;
// 	}
	if (slotIndex == 0)
	{
		skillType = Actived;
	}
	else
	{
		skillType = Passived;
	}
}

void GeneralsShortcutSlot::UpdateSkillState( int slotIndex,int quality )
{
	if (slotIndex <= (quality-1)/10)
	{
		skillState = Opened;
	}
	else
	{
		skillState = NotOpen;
	}
}

void GeneralsShortcutSlot::UpdateSkillState( CShortCut * shortcut,int quality )
{
	if(shortcut->has_skillpropid())
	{
		if (shortcut->storedtype() == 1)
		{
			//std::string skillid = shortcut->skillpropid();
			//GameFightSkill * gameFightSkill = new GameFightSkill();
			//gameFightSkill->setId(skillid);
			//gameFightSkill->getCBaseSkill();
			skillState = Used;
		}
	}
	else
	{
		skillState = Opened;
	}
}

void GeneralsShortcutSlot::DoThing(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (index == 0)
		{
			auto generalListUI = (GeneralsListUI *)GeneralsUI::generalsListUI;
			if (generalListUI && GameView::getInstance()->generalBaseMsgList.size() > 0)
			{
				if (generalListUI->Layer_generalsList->getChildByTag(1000) != NULL)
				{
					auto temp = (GeneralsShortcutLayer *)generalListUI->Layer_generalsList->getChildByTag(1000);
					auto sc = ScriptManager::getInstance()->getScriptById(temp->mTutorialScriptInstanceId);
					if (sc != NULL)
						sc->endCommand(this);
				}
			}
		}


		if (skillState == NotOpen)
		{
			//did not open
			std::string str_des = StringDataManager::getString("generals_teach_willOpen_des1");
			std::string str_teach_willOpen = "generals_teach_willOpen_";
			if (index>0)
			{
				char s_index[5];
				sprintf(s_index, "%d", index);
				str_teach_willOpen.append(s_index);
				std::string str_lv = StringDataManager::getString(str_teach_willOpen.c_str());
				str_des.append(StrUtils::applyColor(str_lv.c_str(), GameView::getInstance()->getGeneralsColorByQuality((index + 1) * 10)));
				str_des.append(StringDataManager::getString("generals_teach_willOpen_des2"));

				GameView::getInstance()->showAlertDialog(str_des.c_str());
			}
		}
		else if (skillState == Opened)
		{
			//����ȼ�
			int openlevel = 0;
			std::map<int, int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(24);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[24];
			}

			if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				std::string str_des = StringDataManager::getString("feature_will_be_open_function");
				char str_level[20];
				sprintf(str_level, "%d", openlevel);
				str_des.append(str_level);
				str_des.append(StringDataManager::getString("feature_will_be_open_open"));
				GameView::getInstance()->showAlertDialog(str_des.c_str());
				return;
			}

			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSkillListUI) == NULL)
			{
				auto winSize = Director::getInstance()->getVisibleSize();
				auto generalsSkillListUI = GeneralsSkillListUI::create(skill_profession, index);
				if (generalsSkillListUI)
				{
					generalsSkillListUI->setIgnoreAnchorPointForPosition(false);
					generalsSkillListUI->setAnchorPoint(Vec2(0.5f, 0.5f));
					generalsSkillListUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
					GameView::getInstance()->getMainUIScene()->addChild(generalsSkillListUI, 0, kTagGeneralsSkillListUI);
				}
			}
		}
		else if (skillState == Used)
		{
			if (skillId.size()>0)
			{
				if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI))
				{
					auto genenralsUI = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);

					if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSkillInfoUI))
					{
					}
					else
					{
						std::map<std::string, GameFightSkill*>::iterator iter = GameView::getInstance()->GeneralsGameFightSkillList.find(skillId);
						if (GameView::getInstance()->GeneralsGameFightSkillList.end() != iter)
						{
							if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSkillInfoUI) == NULL)
							{
								Size winSize = Director::getInstance()->getVisibleSize();
								auto temp = iter->second;
								auto generalsSkillInfoUI = GeneralsSkillInfoUI::create(temp, index);
								generalsSkillInfoUI->setIgnoreAnchorPointForPosition(false);
								generalsSkillInfoUI->setAnchorPoint(Vec2(0.5f, 0.5f));
								generalsSkillInfoUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
								generalsSkillInfoUI->setTag(kTagGeneralsSkillInfoUI);
								GameView::getInstance()->getMainUIScene()->addChild(generalsSkillInfoUI, 0, kTagGeneralsSkillInfoUI);
								generalsSkillInfoUI->setSkillProfession(skill_profession);
							}
						}
					}

				}
			}
		}
		else {}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsShortcutSlot::setSkillProfession( GeneralsSkillsUI::SkillTpye st )
{
	skill_profession = st;
}

GeneralsSkillsUI::SkillTpye GeneralsShortcutSlot::getSkillProfession()
{
	return skill_profession;
}

void GeneralsShortcutSlot::setTouchEnabled( bool value )
{
	btn_bgFrame->setTouchEnabled(value);
}

int GeneralsShortcutSlot::getIndex()
{
	return index;
}

