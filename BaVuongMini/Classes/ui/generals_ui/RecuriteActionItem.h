#ifndef  _GENERALS_RECURITEACTIONITEM_H_
#define _GENERALS_RECURITEACTIONITEM_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * �佫�����µġ��佫��ļ���е���ͨ��һ�㣬��Ʒ��ļ
 * ��Ҫ�������״̬��ˢ��ʱ����ʾ
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.3
 */

class UITab;
class CActionDetail;


class RecuriteActionItem : public UIScene
{
public:
	RecuriteActionItem();
	~RecuriteActionItem();
	
	enum State{
		RunCD,
		Free,
		Gold
	};

// 	enum Style{
// 		RetainNum,
// 		Free,
// 		Gold
// 	};

	static RecuriteActionItem * create(CActionDetail * actionDetail);
	bool init(CActionDetail * actionDetail);

	static std::string getStarPathByNum(int num);
	void update(float dt);
	void refreshData();
    static std::string timeFormatToString(long long t);

public: 
	CActionDetail * curActionDetail;
	float m_fRecordTime;
	State state ;

	long long cdTime;
	int actionMaxNumber ;
	int playerRemainNumber ;
	double lastChangeTime;

private:
	ImageView * retainImage;
	Label * label_num;
	ImageView * freeImage;
	Label * label_time;
	ImageView* freeBefore;
	ImageView * image_gold;
	Label * label_gold;
	ImageView * image_noFreeTime;
};
#endif

