
#include "NormalGeneralsHeadItem.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "GeneralsHeadItemBase.h"
#include "../../legend_engine/CCLegendAnimation.h"


NormalGeneralsHeadItemBase::NormalGeneralsHeadItemBase()
{

}

NormalGeneralsHeadItemBase::~NormalGeneralsHeadItemBase()
{

}

NormalGeneralsHeadItemBase * NormalGeneralsHeadItemBase::create(CGeneralBaseMsg * generalBaseMsg)
{
	auto normalGeneralsHeadItemBase = new NormalGeneralsHeadItemBase();
	if (normalGeneralsHeadItemBase && normalGeneralsHeadItemBase->initWithGeneralBaseMsg(generalBaseMsg))
	{
		normalGeneralsHeadItemBase->autorelease();
		return normalGeneralsHeadItemBase;
	}
	CC_SAFE_DELETE(normalGeneralsHeadItemBase);
	return NULL;
}

NormalGeneralsHeadItemBase * NormalGeneralsHeadItemBase::create( int generalId )
{
	auto normalGeneralsHeadItemBase = new NormalGeneralsHeadItemBase();
	if (normalGeneralsHeadItemBase && normalGeneralsHeadItemBase->initWithGeneralId(generalId))
	{
		normalGeneralsHeadItemBase->autorelease();
		return normalGeneralsHeadItemBase;
	}
	CC_SAFE_DELETE(normalGeneralsHeadItemBase);
	return NULL;
}

NormalGeneralsHeadItemBase * NormalGeneralsHeadItemBase::create( CGeneralDetail * generalDetail )
{
	auto normalGeneralsHeadItemBase = new NormalGeneralsHeadItemBase();
	if (normalGeneralsHeadItemBase && normalGeneralsHeadItemBase->initWithGeneralDetail(generalDetail))
	{
		normalGeneralsHeadItemBase->autorelease();
		return normalGeneralsHeadItemBase;
	}
	CC_SAFE_DELETE(normalGeneralsHeadItemBase);
	return NULL;
}

bool NormalGeneralsHeadItemBase::initWithGeneralBaseMsg(CGeneralBaseMsg * generalBaseMsg)
{
	if (GeneralsHeadItemBase::init(generalBaseMsg))
	{
		u_layer = Layer::create();
		addChild(u_layer);
// 
// 		//�ȼ���ͼ
// 		ImageView * imageView_lvFrame = ImageView::create();
// 		imageView_lvFrame->loadTexture("res_ui/zhezhao80.png");
// 		imageView_lvFrame->setScale9Enabled(true);
// 		imageView_lvFrame->setContentSize(Size(43,15));
// 		imageView_lvFrame->setAnchorPoint(Vec2(1.0f,0));
// 		imageView_lvFrame->setPosition(Vec2(102,25));
// 		u_layer->addChild(imageView_lvFrame);
// 		//�ȼ�
// 		std::string _lv = "LV";
// 		char s_level[5];
// 		sprintf(s_level,"%d",generalBaseMsg->level());
// 		_lv.append(s_level);
// 		Label * label_level = Label::create();
// 		label_level->setText(_lv.c_str());
// 		label_level->setAnchorPoint(Vec2(0.5f,0.5f));
// 		label_level->setPosition(Vec2(-20,8));
// 		imageView_lvFrame->addChild(label_level);
		//���
		if (generalBaseMsg->evolution() > 0)
		{
			std::string rankPathBase = "res_ui/rank/rank";
			char s_evolution[5];
			sprintf(s_evolution,"%d",generalBaseMsg->evolution());
			rankPathBase.append(s_evolution);
			rankPathBase.append(".png");
			auto imageView_rank = ui::ImageView::create();
			imageView_rank->loadTexture(rankPathBase.c_str());
			imageView_rank->setAnchorPoint(Vec2(.5f,.5f));
			imageView_rank->setPosition(Vec2(101-14,137-14));
			imageView_rank->setName("imageView_rank");
			u_layer->addChild(imageView_rank);
		}

		this->setContentSize(Size(108,147));

		return true;
	}
	return false;
}

bool NormalGeneralsHeadItemBase::initWithGeneralId( int generalModleId )
{
	if (GeneralsHeadItemBase::init(generalModleId))
	{
		u_layer = Layer::create();
		addChild(u_layer);

// 		//εȼ���ͼ
// 		ImageView * imageView_lvFrame = ImageView::create();
// 		imageView_lvFrame->loadTexture("res_ui/zhezhao80.png");
// 		imageView_lvFrame->setScale9Enabled(true);
// 		imageView_lvFrame->setContentSize(Size(43,15));
// 		imageView_lvFrame->setAnchorPoint(Vec2(1.0f,0));
// 		imageView_lvFrame->setPosition(Vec2(102,25));
// 		u_layer->addChild(imageView_lvFrame);
// 		//�ȼ�
// 		std::string _lv = "LV";
// 		char s_lv[5];
// 		sprintf(s_lv,"%d",1);
// 		_lv.append(s_lv);
// 		Label * label_level = Label::create();
// 		label_level->setText(_lv.c_str());
// 		label_level->setAnchorPoint(Vec2(0.5f,0.5f));
// 		label_level->setPosition(Vec2(-20,8));
// 		imageView_lvFrame->addChild(label_level);
		//���
// 		std::string rankPathBase = "res_ui/rank/rank";
// 		char s_rank[5];
// 		sprintf(s_rank,"%d",1);
// 		rankPathBase.append(s_rank);
// 		rankPathBase.append(".png");
// 		ImageView * imageView_rank = ImageView::create();
// 		imageView_rank->loadTexture(rankPathBase.c_str());
// 		imageView_rank->setAnchorPoint(Vec2(.5f,.5f));
// 		imageView_rank->setPosition(Vec2(101-14,137-14));
// 		imageView_rank->setName("imageView_rank");
// 		u_layer->addChild(imageView_rank);

		this->setContentSize(Size(108,147));

		return true;
	}
	return false;
}

bool NormalGeneralsHeadItemBase::initWithGeneralDetail( CGeneralDetail * generalDetail )
{
	if (GeneralsHeadItemBase::init(generalDetail->modelid(),generalDetail->currentquality()))
	{
		u_layer = Layer::create();
		addChild(u_layer);

		//εȼ�
// 		std::string _lv = "LV";
// 		char * s = new char [5];
// 		sprintf(s,"%d",level);
// 		_lv.append(s);
// 		Label * label_level = Label::create();
// 		label_level->setText(_lv.c_str());
// 		delete [] s;
// 		label_level->setAnchorPoint(Vec2(1.0f,0));
// 		label_level->setPosition(Vec2(imageView_head->getContentSize().width,46));
// 		m_pLayer->addChild(label_level);
		//���
		if (generalDetail->evolution() > 0)
		{
			std::string rankPathBase = "res_ui/rank/rank";
			char s_evolution[5];
			sprintf(s_evolution,"%d",generalDetail->evolution());
			rankPathBase.append(s_evolution);
			rankPathBase.append(".png");
			auto imageView_rank = ui::ImageView::create();
			imageView_rank->loadTexture(rankPathBase.c_str());
			imageView_rank->setAnchorPoint(Vec2(.5f,.5f));
			imageView_rank->setPosition(Vec2(101-14,137-14));
			imageView_rank->setName("imageView_rank");
			u_layer->addChild(imageView_rank);
		}

		this->setContentSize(Size(108,147));

		return true;
	}
	return false;
}

void NormalGeneralsHeadItemBase::playAnmOfEvolution()
{
	auto imageView_rank = (ui::ImageView*)u_layer->getChildByName("imageView_rank");
	if (imageView_rank)
	{
		imageView_rank->setScale(3.0f);
		auto action = Sequence::create(
			DelayTime::create(0.8f),
			ScaleTo::create(0.2f,1.0f),
			NULL);
		imageView_rank->runAction(action);
	}
}
