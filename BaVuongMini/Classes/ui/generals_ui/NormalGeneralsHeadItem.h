
#ifndef _GENERALSUI_NORMALGENERALSHEADITEM_H_
#define _GENERALSUI_NORMALGENERALSHEADITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "GeneralsHeadItemBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;
/////////////////////////////////
/**
 * �佫�����µ ���ͨ�佫ͷ���
 * �����˵ȼ��;��
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.4
 */
class CGeneralBaseMsg;
class CGeneralDetail;

class NormalGeneralsHeadItemBase :public GeneralsHeadItemBase
{
public:
	NormalGeneralsHeadItemBase();
	~NormalGeneralsHeadItemBase();

	static NormalGeneralsHeadItemBase * create(CGeneralBaseMsg * generalBaseMsg);
	bool initWithGeneralBaseMsg(CGeneralBaseMsg * generalBaseMsg);

	static NormalGeneralsHeadItemBase * create(int generalModleId);
	bool initWithGeneralId(int generalModleId);

	static NormalGeneralsHeadItemBase * create(CGeneralDetail * generalDetail);
	bool initWithGeneralDetail(CGeneralDetail * generalDetail);

	void playAnmOfEvolution();

protected:
	Layer * u_layer;
};

#endif