#include "GeneralsSkillInfoUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "GameView.h"
#include "../../../gamescene_state/MainScene.h"
#include "GeneralsSkillListUI.h"
#include "../../../messageclient/element/CBaseSkill.h"
#include "../../../messageclient/element/CFightSkill.h"
#include "../../../gamescene_state/skill/GameFightSkill.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../skillscene/SkillScene.h"
#include "../GeneralsUI.h"
#include "../GeneralsListUI.h"
#include "../../../messageclient/element/CGeneralBaseMsg.h"
#include "../../../legend_script/CCTutorialParticle.h"
#include "../../../legend_script/CCTutorialIndicator.h"
#include "../../../legend_script/ScriptManager.h"
#include "../../../legend_script/Script.h"
#include "AppMacros.h"
#include "../../../messageclient/element/FolderInfo.h"
#include "../GeneralsSkillsUI.h"

#define  kTag_ScrollView 30

GeneralsSkillInfoUI::GeneralsSkillInfoUI():
skillSlotIndex(-1),
curSkillId("")
{
}


GeneralsSkillInfoUI::~GeneralsSkillInfoUI()
{
}

GeneralsSkillInfoUI * GeneralsSkillInfoUI::create(GameFightSkill * gameFightSkill,int slotIndex)
{
	auto generalsSkillInfoUI = new GeneralsSkillInfoUI();
	if (generalsSkillInfoUI && generalsSkillInfoUI->init(gameFightSkill,slotIndex))
	{
		generalsSkillInfoUI->autorelease();
		return generalsSkillInfoUI;
	}
	CC_SAFE_DELETE(generalsSkillInfoUI);
	return NULL;
}

bool GeneralsSkillInfoUI::init(GameFightSkill * gameFightSkill,int slotIndex)
{
	if (UIScene::init())
	{
		skillSlotIndex = slotIndex;
		curSkillId = gameFightSkill->getId();

		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();
		//���UI
		if(LoadSceneLayer::GeneralsSkillInfoLayer->getParent() != NULL)
		{
			LoadSceneLayer::GeneralsSkillInfoLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::GeneralsSkillInfoLayer;

		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(288, 377));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto btn_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnabled( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(GeneralsSkillInfoUI::CloseEvent, this));
		btn_close->setVisible(false);

		auto L_skillName = (Text*)Helper::seekWidgetByName(ppanel,"Label_skill_name");
		L_skillName->setString(gameFightSkill->getCBaseSkill()->name().c_str());
		//���˫
		btn_superSkill = (Button*)Helper::seekWidgetByName(ppanel,"Button_SuperSkill");
		btn_superSkill->setTouchEnabled( true );
		btn_superSkill->setPressedActionEnabled(true);
		btn_superSkill->addTouchEventListener(CC_CALLBACK_2(GeneralsSkillInfoUI::SuperSkillEvent, this));

		if(gameFightSkill->getCBaseSkill()->usemodel() == 0)//����
		{
			btn_superSkill->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
			btn_superSkill->setTouchEnabled(true);
		}
		else
		{
			btn_superSkill->loadTextures("res_ui/new_button_001.png","res_ui/new_button_001.png","");
			btn_superSkill->setTouchEnabled(false);
		}


		//��
		auto btn_lvUp = (Button*)Helper::seekWidgetByName(ppanel,"Button_upgrade");
		btn_lvUp->setTouchEnabled( true );
		btn_lvUp->setPressedActionEnabled(true);
		btn_lvUp->addTouchEventListener(CC_CALLBACK_2(GeneralsSkillInfoUI::LvUpEvent, this));
		//��滻
		auto btn_replace = (Button*)Helper::seekWidgetByName(ppanel,"Button_shift");
		btn_replace->setTouchEnabled( true );
		btn_replace->setPressedActionEnabled(true);
		btn_replace->addTouchEventListener(CC_CALLBACK_2(GeneralsSkillInfoUI::ReplaceEvent, this));

		RefreshSkillInfo(gameFightSkill);

		this->setContentSize(Size(288,377));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(GeneralsSkillInfoUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(GeneralsSkillInfoUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(GeneralsSkillInfoUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(GeneralsSkillInfoUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void GeneralsSkillInfoUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GeneralsSkillInfoUI::SuperSkillEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		SkillScene::ReqForPreeless * temp = new SkillScene::ReqForPreeless();
		temp->skillid = curSkillId;
		temp->grid = skillSlotIndex;
		temp->roleId = GeneralsUI::generalsListUI->getCurGeneralBaseMsg()->id();
		temp->roleType = GameActor::type_pet;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5075, temp);
		delete temp;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsSkillInfoUI::LvUpEvent(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//����ȼ�
		int openlevel = 0;
		std::map<int, int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(24);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[24];
		}

		if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
		{
			std::string str_des = StringDataManager::getString("feature_will_be_open_function");
			char str_level[20];
			sprintf(str_level, "%d", openlevel);
			str_des.append(str_level);
			str_des.append(StringDataManager::getString("feature_will_be_open_open"));
			GameView::getInstance()->showAlertDialog(str_des.c_str());
			return;
		}

		if (strcmp(curSkillId.c_str(), "") == 0)
		{
			//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *str1  = ((__String*)strings->objectForKey("skillui_peleaseselcetskill"))->m_sString.c_str();
			const char *str1 = StringDataManager::getString("skillui_peleaseselcetskill");
			char* p1 = const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(p1);
		}
		else
		{
			const char * _idx = curSkillId.c_str();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5054, (void *)_idx);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsSkillInfoUI::ReplaceEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//˿���ȼ�
		int openlevel = 0;
		std::map<int, int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(24);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[24];
		}

		if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
		{
			std::string str_des = StringDataManager::getString("feature_will_be_open_function");
			char str_level[20];
			sprintf(str_level, "%d", openlevel);
			str_des.append(str_level);
			str_des.append(StringDataManager::getString("feature_will_be_open_open"));
			GameView::getInstance()->showAlertDialog(str_des.c_str());
			return;
		}

		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSkillListUI) == NULL)
		{
			this->removeFromParent();
			Size winSize = Director::getInstance()->getVisibleSize();
			GeneralsSkillListUI * generalsSkillListUI = GeneralsSkillListUI::create(skill_profession, skillSlotIndex);
			if (generalsSkillListUI)
			{
				generalsSkillListUI->setIgnoreAnchorPointForPosition(false);
				generalsSkillListUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				generalsSkillListUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				GameView::getInstance()->getMainUIScene()->addChild(generalsSkillListUI, 0, kTagGeneralsSkillListUI);
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsSkillInfoUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GeneralsSkillInfoUI::onExit()
{
	UIScene::onExit();
}

bool GeneralsSkillInfoUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	//return true;
	return resignFirstResponder(touch,this,false);
}

void GeneralsSkillInfoUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void GeneralsSkillInfoUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void GeneralsSkillInfoUI::onTouchMoved( Touch *touch, Event * pEvent )
{
}

void GeneralsSkillInfoUI::setSkillProfession( GeneralsSkillsUI::SkillTpye skill_prof )
{
	this->skill_profession = skill_prof;
}

GeneralsSkillsUI::SkillTpye GeneralsSkillInfoUI::getSkillProfession()
{
	return this->skill_profession;
}

std::string GeneralsSkillInfoUI::getProfessionBySkill( std::string skillid )
{
	const char *str1 = StringDataManager::getString("profession_mengjiang");
	char* profession_mengjiang =const_cast<char*>(str1);
	const char *str2 = StringDataManager::getString("profession_guimou");
	char* profession_guimou =const_cast<char*>(str2);
	const char *str3 = StringDataManager::getString("profession_haojie");
	char* profession_haojie =const_cast<char*>(str3);
	const char *str4 = StringDataManager::getString("profession_shenshe");
	char* profession_shenshe =const_cast<char*>(str4);

	int profession_index = 0;
	std::map<std::string,CBaseSkill*>::const_iterator cIter;
	cIter = StaticDataBaseSkill::s_baseSkillData.find(skillid);
	if (cIter == StaticDataBaseSkill::s_baseSkillData.end()) // �û�ҵ�����ָ�END��  
	{
		CCAssert(false,"skill not found");
	}
	else
	{
		profession_index = cIter->second->get_profession();
	}

	switch(profession_index)
	{
	case 1:
		{
			return profession_mengjiang;
		}
		break;
	case 2:
		{
			return profession_guimou;
		}
		break;
	case 3:
		{
			return profession_haojie;
		}
		break;
	case 4:
		{
			return profession_shenshe;
		}
		break;
	}

// 	std::string tempid1 = skillid.substr(0,1);
// 	if (strcmp(tempid1.c_str(),"P") == 0)  //˱������
// 	{
// 		std::string tempid3 = skillid.substr(1,1);
// 		if (strcmp(tempid3.c_str(),"A") == 0)
// 		{
// 			return profession_mengjiang;
// 		}
// 		else if (strcmp(tempid3.c_str(),"B") == 0)
// 		{
// 			return profession_guimou;
// 		}
// 		else if (strcmp(tempid3.c_str(),"C") == 0)
// 		{
// 			return profession_haojie;
// 		}
// 		else if (strcmp(tempid3.c_str(),"D") == 0)
// 		{
// 			return profession_shenshe;
// 		}
// 	}
// 	else
// 	{
// 		std::string tempid2 = skillid.substr(0,1);
// 		if (strcmp(tempid2.c_str(),"A") == 0)
// 		{
// 			return profession_mengjiang;
// 		}
// 		else if (strcmp(tempid2.c_str(),"B") == 0)
// 		{
// 			return profession_guimou;
// 		}
// 		else if (strcmp(tempid2.c_str(),"C") == 0)
// 		{
// 			return profession_haojie;
// 		}
// 		else if (strcmp(tempid2.c_str(),"D") == 0)
// 		{
// 			return profession_shenshe;
// 		}
// 	}
}


void GeneralsSkillInfoUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralsSkillInfoUI::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	// 	UITutorialIndicator * tutorialIndicator = UITutorialIndicator::create();
	// 	tutorialIndicator->setPosition(pos);
	// 	tutorialIndicator->setTag(UITUTORIALINDICATORTAG);
	// 	u_layer->addChild(tutorialIndicator);
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
	auto tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,60,50);
	//tutorialIndicator->setPosition(Vec2(pos.x+40+_w,pos.y+65+_h));
	tutorialIndicator->setPosition(Vec2(pos.x+40,pos.y+65));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->addChild(tutorialIndicator);

	auto tutorialParticle = CCTutorialParticle::create("tuowei0.plist",60,50);
	//tutorialParticle->setPosition(Vec2(pos.x+5+_w,pos.y-15+_h));
	tutorialParticle->setPosition(Vec2(pos.x+5,pos.y-15));
	tutorialParticle->setTag(CCTUTORIALPARTICLETAG);
	this->addChild(tutorialParticle);
}

void GeneralsSkillInfoUI::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto tutorialParticle = dynamic_cast<CCTutorialParticle*>(this->getChildByTag(CCTUTORIALPARTICLETAG));
	if(tutorialParticle != NULL)
		tutorialParticle->removeFromParent();
}

void GeneralsSkillInfoUI::RefreshSkillInfo( GameFightSkill * gameFightSkill )
{
	if (curSkillId != gameFightSkill->getId())
		return;

	//create scrollerView
	auto scrollView_skillInfo = (ui::ScrollView*)m_pLayer->getChildByTag(kTag_ScrollView);
	if (scrollView_skillInfo == NULL)
	{
		scrollView_skillInfo = ui::ScrollView::create();
		scrollView_skillInfo->setTouchEnabled(true);
		scrollView_skillInfo->setDirection(ui::ScrollView::Direction::VERTICAL);
		scrollView_skillInfo->setContentSize(Size(240,238));
		scrollView_skillInfo->setPosition(Vec2(24,84));
		scrollView_skillInfo->setBounceEnabled(true);
		scrollView_skillInfo->setTag(kTag_ScrollView);
		m_pLayer->addChild(scrollView_skillInfo);
	}
	//create elements
	int scroll_height = 0;
	//ܼ�����
	auto image_nameFrame = (ImageView *)scrollView_skillInfo->getChildByName("image_nameFrame");
	if (image_nameFrame == NULL)
	{
		image_nameFrame = ImageView::create();
		image_nameFrame->loadTexture("res_ui/moji_0.png");
		image_nameFrame->setScale(0.6f);
		image_nameFrame->setAnchorPoint(Vec2(0,0));
		image_nameFrame->setName("image_nameFrame");
		scrollView_skillInfo->addChild(image_nameFrame);
	}

	auto l_name = (Label *)scrollView_skillInfo->getChildByName("l_name");
	if (l_name == NULL)
	{
		l_name = Label::createWithTTF(gameFightSkill->getCBaseSkill()->name().c_str(), APP_FONT_NAME, 18);
		l_name->setColor(Color3B(196,255,68));
		l_name->setAnchorPoint(Vec2(0,0));
		l_name->setName("l_name");
		scrollView_skillInfo->addChild(l_name);
	}
	else
	{
		l_name->setString(gameFightSkill->getCBaseSkill()->name().c_str());
	}

	int zeroLineHeight = image_nameFrame->getContentSize().height;
	scroll_height = zeroLineHeight;

	//first line
	auto image_first_line = (ImageView *)scrollView_skillInfo->getChildByName("image_first_line");
	if (image_first_line == NULL)
	{
		image_first_line = ImageView::create();
		image_first_line->loadTexture("res_ui/henggang.png");
		image_first_line->setScaleX(0.9f);
		image_first_line->setAnchorPoint(Vec2(0,0));
		image_first_line->setName("image_first_line");
		scrollView_skillInfo->addChild(image_first_line);
	}
	int zeroLineHeight_1 = zeroLineHeight + image_first_line->getContentSize().height+6;
	scroll_height = zeroLineHeight_1;
	//��/����
	std::string str_type = "";
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		str_type.append(skillinfo_zhudongjineng);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *beidongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_beidongjineng =const_cast<char*>(beidongjineng);
		str_type.append(skillinfo_beidongjineng);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		str_type.append(skillinfo_zhudongjineng);
	}
	auto l_type = (Label *)scrollView_skillInfo->getChildByName("l_type");
	if (l_type == NULL)
	{
		l_type = Label::createWithTTF(str_type.c_str(), APP_FONT_NAME, 16);
		l_type->setColor(Color3B(212,255,151));
		l_type->setAnchorPoint(Vec2(0,0));
		l_type->setName("l_type");
		scrollView_skillInfo->addChild(l_type);
	}
	else
	{
		l_type->setString(str_type.c_str());
	}
	//MP��
	auto l_mp = (Label *)scrollView_skillInfo->getChildByName("l_mp");
	if (l_mp == NULL)
	{
		l_mp = Label::createWithTTF(StringDataManager::getString("skillinfo_mofaxiaohao"), APP_FONT_NAME, 16);
		l_mp->setColor(Color3B(212,255,151));
		l_mp->setAnchorPoint(Vec2(0,0));
		l_mp->setName("l_mp");
		scrollView_skillInfo->addChild(l_mp);
	}
	char s_mp[5];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	auto l_mpValue = (Label *)scrollView_skillInfo->getChildByName("l_mpValue");
	if (l_mpValue == NULL)
	{
		l_mpValue = Label::createWithTTF(s_mp, APP_FONT_NAME, 16);
		l_mpValue->setColor(Color3B(212,255,151));
		l_mpValue->setAnchorPoint(Vec2(0,0));
		l_mpValue->setName("l_mpValue");
		scrollView_skillInfo->addChild(l_mpValue);
	}
	else
	{
		l_mpValue->setString(s_mp);
	}
	int firstLineHeight = zeroLineHeight_1 + l_type->getContentSize().height;
	scroll_height = firstLineHeight;
	//ĵȼ�
	auto l_lv = (Label *)scrollView_skillInfo->getChildByName("l_lv");
	if (l_lv == NULL)
	{
		l_lv = Label::createWithTTF(StringDataManager::getString("skillinfo_dengji"), APP_FONT_NAME, 16);
		l_lv->setColor(Color3B(212,255,151));
		l_lv->setAnchorPoint(Vec2(0,0));
		l_lv->setName("l_lv");
		scrollView_skillInfo->addChild(l_lv);
	}

	char s_curLevel[5];
	sprintf(s_curLevel,"%d",gameFightSkill->getCFightSkill()->level());
	auto l_lvValue = (Label *)scrollView_skillInfo->getChildByName("l_lvValue");
	if (l_lvValue == NULL)
	{
		l_lvValue = Label::createWithTTF(s_curLevel, APP_FONT_NAME, 16);
		l_lvValue->setColor(Color3B(212,255,151));
		l_lvValue->setAnchorPoint(Vec2(0,0));
		l_lvValue->setName("l_lvValue");
		scrollView_skillInfo->addChild(l_lvValue);
	}
	else
	{
		l_lvValue->setString(s_curLevel);
	}
	//CDʱ� 
	auto l_cd = (Label *)scrollView_skillInfo->getChildByName("l_cd");
	if (l_cd == NULL)
	{
		l_cd = Label::createWithTTF(StringDataManager::getString("skillinfo_lengqueshijian"), APP_FONT_NAME, 16);
		l_cd->setColor(Color3B(212,255,151));
		l_cd->setAnchorPoint(Vec2(0,0));
		l_cd->setName("l_cd");
		scrollView_skillInfo->addChild(l_cd);
	}
	char s_intervaltime[5];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	auto l_cdValue = (Label *)scrollView_skillInfo->getChildByName("l_cdValue");
	if (l_cdValue == NULL)
	{
		l_cdValue = Label::createWithTTF(s_intervaltime, APP_FONT_NAME, 16);
		l_cdValue->setColor(Color3B(212,255,151));
		l_cdValue->setAnchorPoint(Vec2(0,0));
		l_cdValue->setName("l_cdValue");
		scrollView_skillInfo->addChild(l_cdValue);
	}
	else
	{
		l_cdValue->setString(s_intervaltime);
	}
	int secondLineHeight = firstLineHeight + l_lv->getContentSize().height;
	scroll_height = secondLineHeight;
	//�ְҵ
	auto l_pro = (Label *)scrollView_skillInfo->getChildByName("l_pro");
	if (l_pro == NULL)
	{
		l_pro = Label::createWithTTF(StringDataManager::getString("skillinfo_zhiye"), APP_FONT_NAME,16);
		l_pro->setColor(Color3B(212,255,151));
		l_pro->setAnchorPoint(Vec2(0,0));
		l_pro->setName("l_pro");
		scrollView_skillInfo->addChild(l_pro);
	}
	auto l_proValue = (Label *)scrollView_skillInfo->getChildByName("l_proValue");
	if (l_proValue == NULL)
	{
		l_proValue = Label::createWithTTF(GameView::getInstance()->myplayer->getActiveRole()->profession().c_str(), APP_FONT_NAME, 16);
		l_proValue->setColor(Color3B(212,255,151));
		l_proValue->setAnchorPoint(Vec2(0,0));
		l_proValue->setName("l_proValue");
		scrollView_skillInfo->addChild(l_proValue);
	}
	else
	{
		l_proValue->setString(GameView::getInstance()->myplayer->getActiveRole()->profession().c_str());
	}
	//ʩ�����
	auto l_dis = (Label *)scrollView_skillInfo->getChildByName("l_dis");
	if (l_dis == NULL)
	{
		l_dis = Label::createWithTTF(StringDataManager::getString("skillinfo_shifajuli"), APP_FONT_NAME, 16);
		l_dis->setColor(Color3B(212,255,151));
		l_dis->setAnchorPoint(Vec2(0,0));
		l_dis->setName("l_dis");
		scrollView_skillInfo->addChild(l_dis);
	}
	char s_distance[5];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	auto l_disValue = (Label *)scrollView_skillInfo->getChildByName("l_disValue");
	if (l_disValue == NULL)
	{
		l_disValue = Label::createWithTTF(s_distance, APP_FONT_NAME, 16);
		l_disValue->setColor(Color3B(212,255,151));
		l_disValue->setAnchorPoint(Vec2(0,0));
		l_disValue->setName("l_disValue");
		scrollView_skillInfo->addChild(l_disValue);
	}
	else
	{
		l_disValue->setString(s_distance);
	}
	int thirdLineHeight = secondLineHeight + l_pro->getContentSize().height;
	scroll_height = thirdLineHeight;
	//��
	auto l_des = (Text *)scrollView_skillInfo->getChildByName("l_des");
	if (l_des == NULL)
	{
		l_des = Text::create(gameFightSkill->getCFightSkill()->description().c_str(), APP_FONT_NAME, 16);
		l_des->setTextAreaSize(Size(230, 0 ));
		l_des->setColor(Color3B(30,255,0));
		l_des->setAnchorPoint(Vec2(0,0));
		l_des->setName("l_des");
		scrollView_skillInfo->addChild(l_des);
	}
	else
	{
		l_des->setString(gameFightSkill->getCFightSkill()->description().c_str());
	}
	int fourthLineHeight = thirdLineHeight+l_des->getContentSize().height;
	scroll_height = fourthLineHeight;

	//next info
	if (scrollView_skillInfo->getChildByName("panel_nextInfo"))
	{
		scrollView_skillInfo->getChildByName("panel_nextInfo")->removeFromParent();
	}
	auto panel_nextInfo = Layout::create();
	panel_nextInfo->setName("panel_nextInfo");
	scrollView_skillInfo->addChild(panel_nextInfo);

	int fifthLineHeight = 0;
	int sixthLineHeight = 0;
	int seventhLineHeight = 0;
	int seventhLineHeight_1 = 0;
	int eighthLineHeight = 0;
	int ninthLineHeight = 0;
	int tenthLineHeight = 0;
	int eleventhLineHeight = 0;
	int twelfthLineHeight = 0;

	std::string m_str_next_des = "";
	int m_required_level = 0;
	int m_required_point = 0;
	std::string m_str_pre_skill = "";
	std::string m_pre_skill_id = "";
	int m_required_pre_skil_level = 0;
	int m_required_gold = 0;
	int m_required_skillBookValue = 0;

	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		//get data
		m_str_next_des.append(gameFightSkill->getCFightSkill()->get_next_description());
		//���ǵȼ�
		m_required_level = gameFightSkill->getCFightSkill()->get_next_required_level();
		//���ܵ
		m_required_point = gameFightSkill->getCFightSkill()->get_next_required_point();
		//XXX㼼�
		if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
		{
			CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
			m_str_pre_skill.append(temp->name().c_str());
			m_pre_skill_id.append(temp->id());

			m_required_pre_skil_level = gameFightSkill->getCFightSkill()->get_next_pre_skill_level();
		}
		//ܽ�
		m_required_gold = gameFightSkill->getCFightSkill()->get_next_required_gold();
		//Ҽ����
		m_required_skillBookValue = gameFightSkill->getCFightSkill()->get_next_required_num();

		//create ui
		//���һ��
		auto l_nextLv = Label::createWithTTF(StringDataManager::getString("skillinfo_xiayiji"), APP_FONT_NAME, 16);
		l_nextLv->setColor(Color3B(0,255,255));
		l_nextLv->setAnchorPoint(Vec2(0,0));
		l_nextLv->setName("l_nextLv");
		panel_nextInfo->addChild(l_nextLv);

		fifthLineHeight = fourthLineHeight+l_nextLv->getContentSize().height*2;
		scroll_height = fifthLineHeight;
		/////////////////////////////////////////////////////
		auto l_nextLvDes = Text::create(gameFightSkill->getCFightSkill()->get_next_description().c_str(), APP_FONT_NAME, 16);
		l_nextLvDes->setTextAreaSize(Size(230, 0 ));
		l_nextLvDes->setColor(Color3B(0,255,255));
		l_nextLvDes->setAnchorPoint(Vec2(0,0));
		l_nextLvDes->setName("l_nextLvDes");
		panel_nextInfo->addChild(l_nextLvDes);

		sixthLineHeight = fifthLineHeight+l_nextLvDes->getContentSize().height;
		scroll_height = sixthLineHeight;

		//�����
		auto image_upgradeNeed = ImageView::create();
		image_upgradeNeed->loadTexture("res_ui/moji_0.png");
		image_upgradeNeed->setScale(0.6f);
		image_upgradeNeed->setAnchorPoint(Vec2(0,0));
		image_upgradeNeed->setName("image_upgradeNeed");
		panel_nextInfo->addChild(image_upgradeNeed);

		auto l_upgradeNeed = Label::createWithTTF(StringDataManager::getString("skillinfo_xuexixuqiu"), APP_FONT_NAME, 18);
		l_upgradeNeed->setColor(Color3B(196,255,68));
		l_upgradeNeed->setAnchorPoint(Vec2(0,0));
		l_upgradeNeed->setName("l_upgradeNeed");
		panel_nextInfo->addChild(l_upgradeNeed);

		seventhLineHeight = sixthLineHeight + image_upgradeNeed->getContentSize().height;
		scroll_height = seventhLineHeight;


		//////////////////////////////////////////////////////
		//
		auto image_second_line = ImageView::create();
		image_second_line->loadTexture("res_ui/henggang.png");
		image_second_line->setScale(0.9f);
		image_second_line->setAnchorPoint(Vec2(0,0));
		image_second_line->setName("image_second_line");
		panel_nextInfo->addChild(image_second_line);

		seventhLineHeight_1 = seventhLineHeight + image_second_line->getContentSize().height+7;
		scroll_height = seventhLineHeight_1;

		//���ǵȼ�
		if (m_required_level > 0)
		{
			auto l_playerLv = Label::createWithTTF(StringDataManager::getString("skillinfo_zhujuedengji"), APP_FONT_NAME, 16);
			l_playerLv->setColor(Color3B(212,255,151));
			l_playerLv->setAnchorPoint(Vec2(0,0));
			l_playerLv->setName("l_playerLv");
			panel_nextInfo->addChild(l_playerLv);

			auto l_playerLv_colon = Label::createWithTTF(":", APP_FONT_NAME, 16);
			l_playerLv_colon->setColor(Color3B(212,255,151));
			l_playerLv_colon->setAnchorPoint(Vec2(0,0));
			l_playerLv_colon->setName("l_playerLv_colon");
			panel_nextInfo->addChild(l_playerLv_colon);

			char s_required_level[5];
			sprintf(s_required_level,"%d",m_required_level);
			auto l_playerLvValue = Text::create(s_required_level, APP_FONT_NAME, 16);
			l_playerLvValue->setTextAreaSize(Size(230, 0 ));
			l_playerLvValue->setColor(Color3B(212,255,151));
			l_playerLvValue->setAnchorPoint(Vec2(0,0));
			l_playerLvValue->setName("l_playerLvValue");
			panel_nextInfo->addChild(l_playerLvValue);

			eighthLineHeight = seventhLineHeight_1 + l_playerLv->getContentSize().height;
			scroll_height = eighthLineHeight;

			if (GameView::getInstance()->myplayer->getActiveRole()->level()<m_required_level)  //����ȼ�δ�ﵽ������Ҫ�
			{
				l_playerLv->setColor(Color3B(255,51,51));
				l_playerLv_colon->setColor(Color3B(255,51,51));
				l_playerLvValue->setColor(Color3B(255,51,51));
			}
		}
		else
		{
			eighthLineHeight = seventhLineHeight_1;
			scroll_height = eighthLineHeight;
		}
		//��ܵ
		if (m_required_point > 0)
		{
			auto widget_skillPoint = Widget::create();
			panel_nextInfo->addChild(widget_skillPoint);
			widget_skillPoint->setName("widget_skillPoint");

			std::string str_skillPoint = StringDataManager::getString("skillinfo_jinengdian");
			for(int i = 0;i<3;i++)
			{
				std::string str_name = "l_skillPoint";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				auto l_skillPoint = Label::createWithTTF(str_skillPoint.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
				l_skillPoint->setColor(Color3B(212,255,151));
				l_skillPoint->setAnchorPoint(Vec2(0,0));
				l_skillPoint->setName(str_name.c_str());
				widget_skillPoint->addChild(l_skillPoint);
				l_skillPoint->setPosition(Vec2(24*i,0));
			}

			auto l_skillPoint_colon = Label::createWithTTF(":", APP_FONT_NAME, 16);
			l_skillPoint_colon->setColor(Color3B(212,255,151));
			l_skillPoint_colon->setAnchorPoint(Vec2(0,0));
			l_skillPoint_colon->setName("l_skillPoint_colon");
			panel_nextInfo->addChild(l_skillPoint_colon);

			char s_required_point[10];
			sprintf(s_required_point,"%d",m_required_point);
			auto l_skillPointValue = Text::create(s_required_point, APP_FONT_NAME, 16);
			l_skillPointValue->setTextAreaSize(Size(230, 0 ));
			l_skillPointValue->setColor(Color3B(212,255,151));
			l_skillPointValue->setAnchorPoint(Vec2(0,0));
			l_skillPointValue->setName("l_skillPointValue");
			panel_nextInfo->addChild(l_skillPointValue);

			ninthLineHeight = eighthLineHeight + l_skillPointValue->getContentSize().height;
			scroll_height = ninthLineHeight;

			if (GameView::getInstance()->myplayer->player->skillpoint()<m_required_point)  //����＼�ܵ㲻�
			{
				for(int i = 0;i<3;i++)
				{
					std::string str_name = "l_skillPoint";
					char s_index[5];
					sprintf(s_index,"%d",i);
					str_name.append(s_index);
					auto l_skillPoint = (Label*)widget_skillPoint->getChildByName(str_name.c_str());
					if (l_skillPoint)
					{
						l_skillPoint->setColor(Color3B(255,51,51));
					}
				}
				l_skillPoint_colon->setColor(Color3B(255,51,51));
				l_skillPointValue->setColor(Color3B(255,51,51));
			}
		}
		else
		{
			ninthLineHeight = eighthLineHeight;
			scroll_height = ninthLineHeight;
		}
		//XXX㼼�
		if (m_str_pre_skill != "")
		{
			auto widget_pre_skill = Widget::create();
			panel_nextInfo->addChild(widget_pre_skill);
			widget_pre_skill->setName("widget_pre_skill");
			if (m_str_pre_skill.size()/3 < 4)
			{
				for(int i = 0;i<m_str_pre_skill.size()/3;i++)
				{
					std::string str_name = "l_preSkill";
					char s_index[5];
					sprintf(s_index,"%d",i);
					str_name.append(s_index);

					auto l_preSkill = Label::createWithTTF(m_str_pre_skill.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
					l_preSkill->setColor(Color3B(212,255,151));
					l_preSkill->setAnchorPoint(Vec2(0,0));
					widget_pre_skill->addChild(l_preSkill);
					l_preSkill->setName(str_name.c_str());
					if (m_str_pre_skill.size()/3 == 1)
					{
						l_preSkill->setPosition(Vec2(0,0));
					}
					else if (m_str_pre_skill.size()/3 == 2)
					{
						l_preSkill->setPosition(Vec2(48*i,0));
					}
					else if (m_str_pre_skill.size()/3 == 3)
					{
						l_preSkill->setPosition(Vec2(24*i,0));
					}
				}
			}
			else
			{
				auto l_preSkill = Label::createWithTTF(m_str_pre_skill.c_str(), APP_FONT_NAME, 16);
				l_preSkill->setColor(Color3B(212,255,151));
				l_preSkill->setAnchorPoint(Vec2(0,0));
				l_preSkill->setPosition(Vec2(0,0));
				widget_pre_skill->addChild(l_preSkill);
				l_preSkill->setName("l_preSkill");
			}

			auto l_preSkill_colon = Label::createWithTTF(":", APP_FONT_NAME, 16);
			l_preSkill_colon->setColor(Color3B(212,255,151));
			l_preSkill_colon->setAnchorPoint(Vec2(0,0));
			l_preSkill_colon->setName("l_preSkill_colon");
			panel_nextInfo->addChild(l_preSkill_colon);

			char s_pre_skill_level[5];
			sprintf(s_pre_skill_level,"%d",m_required_pre_skil_level);
			auto l_preSkillValue = Text::create(s_pre_skill_level, APP_FONT_NAME, 16);
			l_preSkillValue->setTextAreaSize(Size(230, 0 ));
			l_preSkillValue->setColor(Color3B(212,255,151));
			l_preSkillValue->setAnchorPoint(Vec2(0,0));
			l_preSkillValue->setName("l_preSkillValue");
			panel_nextInfo->addChild(l_preSkillValue);

			tenthLineHeight = ninthLineHeight + l_preSkillValue->getContentSize().height;
			scroll_height = tenthLineHeight;

			std::map<std::string,GameFightSkill*>::const_iterator cIter;
			cIter = GameView::getInstance()->GameFightSkillList.find(m_pre_skill_id);
			if (cIter == GameView::getInstance()->GameFightSkillList.end()) // �û�ҵ�����ָ�END��  
			{
				auto gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[m_pre_skill_id];
				if (gameFightSkillStuded)
				{
					if (gameFightSkillStuded->getLevel() < m_required_pre_skil_level)
					{
						if (m_str_pre_skill.size()/3 < 4)
						{
							for(int i = 0;i<m_str_pre_skill.size()/3;i++)
							{
								std::string str_name = "l_preSkill";
								char s_index[5];
								sprintf(s_index,"%d",i);
								str_name.append(s_index);

								auto l_preSkill = (Label*)widget_pre_skill->getChildByName(str_name.c_str());
								if (l_preSkill)
								{
									l_preSkill->setColor(Color3B(255,51,51));
								}
							}
						}
						else
						{
							auto l_preSkill = (Label*)widget_pre_skill->getChildByName("l_preSkill");
							if (l_preSkill)
							{
								l_preSkill->setColor(Color3B(255,51,51));
							}
						}
						l_preSkill_colon->setColor(Color3B(255,51,51));
						l_preSkillValue->setColor(Color3B(255,51,51));
					}
				}
			}
		}
		else
		{
			tenthLineHeight = ninthLineHeight;
			scroll_height = tenthLineHeight;
		}
		//˽�
		if (m_required_gold > 0)
		{
			auto widget_gold = Widget::create();
			panel_nextInfo->addChild(widget_gold);
			widget_gold->setName("widget_gold");

			std::string str_gold = StringDataManager::getString("skillinfo_jinbi");
			for(int i = 0;i<2;i++)
			{
				std::string str_name = "l_gold";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				auto l_gold = Label::createWithTTF(str_gold.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
				l_gold->setColor(Color3B(212,255,151));
				l_gold->setAnchorPoint(Vec2(0,0));
				l_gold->setName(str_name.c_str());
				widget_gold->addChild(l_gold);
				l_gold->setPosition(Vec2(48*i,0));
			}

			auto l_gold_colon = Label::createWithTTF(":", APP_FONT_NAME, 16);
			l_gold_colon->setColor(Color3B(212,255,151));
			l_gold_colon->setAnchorPoint(Vec2(0,0));
			l_gold_colon->setName("l_gold_colon");
			panel_nextInfo->addChild(l_gold_colon);

			char s_required_gold[20];
			sprintf(s_required_gold,"%d",m_required_gold);
			auto l_goldValue = Label::createWithTTF(s_required_gold, APP_FONT_NAME, 16);
			l_goldValue->setColor(Color3B(212,255,151));
			l_goldValue->setAnchorPoint(Vec2(0,0));
			l_goldValue->setName("l_goldValue");
			panel_nextInfo->addChild(l_goldValue);

			auto image_gold = ImageView::create();
			image_gold->loadTexture("res_ui/coins.png");
			image_gold->setAnchorPoint(Vec2(0,0));
			image_gold->setName("image_gold");
			image_gold->setScale(0.85f);
			panel_nextInfo->addChild(image_gold);

			eleventhLineHeight = tenthLineHeight + l_goldValue->getContentSize().height;
			scroll_height = eleventhLineHeight;

			if(GameView::getInstance()->getPlayerGold()<m_required_gold)//ҽ�Ҳ��
			{
				for(int i = 0;i<2;i++)
				{
					std::string str_name = "l_gold";
					char s_index[5];
					sprintf(s_index,"%d",i);
					str_name.append(s_index);
					auto l_gold = (Label*)widget_gold->getChildByName(str_name.c_str());
					if (l_gold)
					{
						l_gold->setColor(Color3B(255,51,51));
					}
				}
				l_gold_colon->setColor(Color3B(255,51,51));
				l_goldValue->setColor(Color3B(255,51,51));
			}
		}
		else
		{
			eleventhLineHeight = tenthLineHeight;
			scroll_height = eleventhLineHeight;
		}
		//㼼���
		if (m_required_skillBookValue>0)
		{
			auto widget_skillBook = Widget::create();
			panel_nextInfo->addChild(widget_skillBook);
			widget_skillBook->setName("widget_skillBook");

			std::string str_skillbook = GeneralsSkillsUI::getSkillQualityStr(gameFightSkill->getCBaseSkill()->get_quality());
			str_skillbook.append(StringDataManager::getString("skillinfo_jinengshu"));
			auto l_skillbook = Label::createWithTTF(str_skillbook.c_str(), APP_FONT_NAME, 16);
			l_skillbook->setColor(Color3B(212,255,151));
			l_skillbook->setAnchorPoint(Vec2(0,0));
			l_skillbook->setName("l_skillbook");
			widget_skillBook->addChild(l_skillbook);
			l_skillbook->setPosition(Vec2(0,0));

			auto l_skillbook_colon = Label::createWithTTF(":", APP_FONT_NAME, 16);
			l_skillbook_colon->setColor(Color3B(212,255,151));
			l_skillbook_colon->setAnchorPoint(Vec2(0,0));
			l_skillbook_colon->setName("l_skillbook_colon");
			panel_nextInfo->addChild(l_skillbook_colon);

			char s_required_skillBook[20];
			sprintf(s_required_skillBook,"%d",m_required_skillBookValue);
			auto l_skillBookValue = Label::createWithTTF(s_required_skillBook, APP_FONT_NAME, 16);
			l_skillBookValue->setColor(Color3B(212,255,151));
			l_skillBookValue->setAnchorPoint(Vec2(0,0));
			panel_nextInfo->addChild(l_skillBookValue);
			l_skillBookValue->setName("l_skillBookValue");

			twelfthLineHeight = eleventhLineHeight + l_skillBookValue->getContentSize().height;
			scroll_height = twelfthLineHeight;

			int propNum = 0;
			for (int i = 0;i<GameView::getInstance()->AllPacItem.size();++i)
			{
				auto folder = GameView::getInstance()->AllPacItem.at(i);
				if (!folder->has_goods())
					continue;

				if (folder->goods().id() == gameFightSkill->getCFightSkill()->get_required_prop())
				{
					propNum += folder->quantity();
				}
			}
			if(propNum<gameFightSkill->getCFightSkill()->get_required_num())//鼼���鲻�
			{
				l_skillbook->setColor(Color3B(255,51,51));
				l_skillbook_colon->setColor(Color3B(255,51,51));
				l_skillBookValue->setColor(Color3B(255,51,51));
			}
		}
		else
		{
			twelfthLineHeight = eleventhLineHeight;
			scroll_height = twelfthLineHeight;
		}
	}

	int innerWidth = scrollView_skillInfo->getBoundingBox().size.width;
	int innerHeight = scroll_height;
	if (scroll_height < scrollView_skillInfo->getBoundingBox().size.height)
	{
		innerHeight = scrollView_skillInfo->getBoundingBox().size.height;
	}
	scrollView_skillInfo->setInnerContainerSize(Size(innerWidth,innerHeight));

	image_nameFrame->setPosition(Vec2(8,innerHeight - zeroLineHeight));
	l_name->setPosition(Vec2(15,innerHeight - zeroLineHeight));
	image_first_line->setPosition(Vec2(5,innerHeight - zeroLineHeight_1+5));
	l_type->setPosition(Vec2(8,innerHeight - firstLineHeight));
	l_mp->setPosition(Vec2(105,innerHeight - firstLineHeight));
	l_mpValue->setPosition(Vec2(105+l_mp->getContentSize().width+2,innerHeight - firstLineHeight));
	l_lv->setPosition(Vec2(8,innerHeight - secondLineHeight));
	l_lvValue->setPosition(Vec2(8+l_lv->getContentSize().width+2,innerHeight - secondLineHeight));
	l_cd->setPosition(Vec2(105,innerHeight - secondLineHeight));
	l_cdValue->setPosition(Vec2(105+l_cd ->getContentSize().width+2,innerHeight - secondLineHeight));
	l_pro->setPosition(Vec2(8,innerHeight - thirdLineHeight));
	l_proValue->setPosition(Vec2(8+l_pro->getContentSize().width+2,innerHeight - thirdLineHeight));
	l_dis->setPosition(Vec2(105,innerHeight - thirdLineHeight));
	l_disValue->setPosition(Vec2(105+l_dis->getContentSize().width+2,innerHeight - thirdLineHeight));

	if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		l_mp->setVisible(false);
		l_mpValue->setVisible(false);
		l_cd->setVisible(false);
		l_cdValue->setVisible(false);
		l_dis->setVisible(false);
		l_disValue->setVisible(false);
	}
	else
	{
		l_mp->setVisible(true);
		l_mpValue->setVisible(true);
		l_cd->setVisible(true);
		l_cdValue->setVisible(true);
		l_dis->setVisible(true);
		l_disValue->setVisible(true);
	}

	l_des->setPosition(Vec2(8,innerHeight - fourthLineHeight));
	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		if (panel_nextInfo->getChildByName("l_nextLv"))
		{
			panel_nextInfo->getChildByName("l_nextLv")->setPosition(Vec2(8,innerHeight - fifthLineHeight));
		}

		if (panel_nextInfo->getChildByName("l_nextLvDes"))
		{
			panel_nextInfo->getChildByName("l_nextLvDes")->setPosition(Vec2(8,innerHeight - sixthLineHeight));
		}

		if (panel_nextInfo->getChildByName("image_upgradeNeed"))
		{
			panel_nextInfo->getChildByName("image_upgradeNeed")->setPosition(Vec2(8,innerHeight - seventhLineHeight));
		}

		if (panel_nextInfo->getChildByName("l_upgradeNeed"))
		{
			panel_nextInfo->getChildByName("l_upgradeNeed")->setPosition(Vec2(15,innerHeight - seventhLineHeight));
		}

		if (panel_nextInfo->getChildByName("image_second_line"))
		{
			panel_nextInfo->getChildByName("image_second_line")->setPosition(Vec2(5,innerHeight - seventhLineHeight_1+5));
		}

		int temp_width = 64;

		//���ǵȼ�
		if (m_required_level>0)
		{
			if (panel_nextInfo->getChildByName("l_playerLv"))
			{
				panel_nextInfo->getChildByName("l_playerLv")->setPosition(Vec2(8,innerHeight - eighthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_playerLv_colon"))
			{
				panel_nextInfo->getChildByName("l_playerLv_colon")->setPosition(Vec2(76,innerHeight - eighthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_playerLvValue"))
			{
				panel_nextInfo->getChildByName("l_playerLvValue")->setPosition(Vec2(85,innerHeight - eighthLineHeight));
			}
		}
		//���ܵ
		if (m_required_point>0)
		{
			if (panel_nextInfo->getChildByName("widget_skillPoint"))
			{
				panel_nextInfo->getChildByName("widget_skillPoint")->setPosition(Vec2(8,innerHeight - ninthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_skillPoint_colon"))
			{
				panel_nextInfo->getChildByName("l_skillPoint_colon")->setPosition(Vec2(76,innerHeight - ninthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_skillPointValue"))
			{
				panel_nextInfo->getChildByName("l_skillPointValue")->setPosition(Vec2(85,innerHeight - ninthLineHeight));
			}
		}
		//XXX㼼�
		if (m_str_pre_skill != "")
		{
			if (panel_nextInfo->getChildByName("widget_pre_skill"))
			{
				panel_nextInfo->getChildByName("widget_pre_skill")->setPosition(Vec2(8,innerHeight - tenthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_preSkill_colon"))
			{
				panel_nextInfo->getChildByName("l_preSkill_colon")->setPosition(Vec2(76,innerHeight - tenthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_preSkillValue"))
			{
				panel_nextInfo->getChildByName("l_preSkillValue")->setPosition(Vec2(85,innerHeight - tenthLineHeight));
			}
		}
		//ܽ�
		if (m_required_gold > 0)
		{
			if (panel_nextInfo->getChildByName("widget_gold"))
			{
				panel_nextInfo->getChildByName("widget_gold")->setPosition(Vec2(8,innerHeight - eleventhLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_gold_colon"))
			{
				panel_nextInfo->getChildByName("l_gold_colon")->setPosition(Vec2(76,innerHeight - eleventhLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_goldValue"))
			{
				panel_nextInfo->getChildByName("l_goldValue")->setPosition(Vec2(85,innerHeight - eleventhLineHeight));
			}
			if (panel_nextInfo->getChildByName("image_gold"))
			{
				panel_nextInfo->getChildByName("image_gold")->setPosition(Vec2(88+panel_nextInfo->getChildByName("l_goldValue")->getContentSize().width,innerHeight - eleventhLineHeight));
			}
		}
		//Ҽ����
		if(m_required_skillBookValue > 0)
		{
			if (panel_nextInfo->getChildByName("widget_skillBook"))
			{
				panel_nextInfo->getChildByName("widget_skillBook")->setPosition(Vec2(8,innerHeight - twelfthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_skillbook_colon"))
			{
				panel_nextInfo->getChildByName("l_skillbook_colon")->setPosition(Vec2(76,innerHeight - twelfthLineHeight));
			}
			if (panel_nextInfo->getChildByName("l_skillBookValue"))
			{
				panel_nextInfo->getChildByName("l_skillBookValue")->setPosition(Vec2(85,innerHeight - twelfthLineHeight));
			}
		}
	}
	scrollView_skillInfo->jumpToTop();

	/*
	if (this->getChildByTag(kTag_ScrollView) != NULL)
	{
		this->getChildByTag(kTag_ScrollView)->removeFromParent();
	}

	int scroll_height = 0;
	//鼼����
	Sprite * s_nameFrame = Sprite::create("res_ui/moji_0.png");
	s_nameFrame->setScaleY(0.6f);
	Label * l_name = Label::createWithTTF(gameFightSkill->getCBaseSkill()->name().c_str(),APP_FONT_NAME,18);
	l_name->setColor(Color3B(196,255,68));
	int zeroLineHeight = s_nameFrame->getContentSize().height;
	scroll_height = zeroLineHeight;

	//
	Sprite * s_first_line = Sprite::create("res_ui/henggang.png");
	s_first_line->setScaleX(0.9f);
	int zeroLineHeight_1 = zeroLineHeight + s_first_line->getContentSize().height+6;
	scroll_height = zeroLineHeight_1;

	//��/����
	Label * l_type;
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_type = Label::createWithTTF(skillinfo_zhudongjineng,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_type = Label::createWithTTF(skillinfo_zhudongjineng,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_type = Label::createWithTTF(skillinfo_zhudongjineng,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	}
	l_type->setColor(Color3B(212,255,151));
	//�ȼ�
	//const char *dengji  = ((__String*)strings->objectForKey("skillinfo_dengji"))->m_sString.c_str();
	const char *dengji = StringDataManager::getString("skillinfo_dengji");
	char* skillinfo_dengji =const_cast<char*>(dengji);
	Label * l_lv = Label::createWithTTF(skillinfo_dengji,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_lv->setColor(Color3B(212,255,151));
	char s_level[5];
	sprintf(s_level,"%d",gameFightSkill->getCFightSkill()->level());
	Label * l_lvValue = Label::createWithTTF(s_level,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_lvValue->setColor(Color3B(212,255,151));

	int firstLineHeight = zeroLineHeight_1 + l_type->getContentSize().height;
	scroll_height = firstLineHeight;
	///////////////////////2////////////////////////////
	//MP��
	//const char *mofaxiaohao  = ((__String*)strings->objectForKey("skillinfo_mofaxiaohao"))->m_sString.c_str();
	const char *mofaxiaohao = StringDataManager::getString("skillinfo_mofaxiaohao");
	char* skillinfo_mofaxiaohao =const_cast<char*>(mofaxiaohao);
	Label * l_mp = Label::createWithTTF(skillinfo_mofaxiaohao,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_mp->setColor(Color3B(212,255,151));
	char s_mp[20];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	Label * l_mpValue = Label::createWithTTF(s_mp,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_mpValue->setColor(Color3B(212,255,151));
	//CD�ʱ� 
	//const char *lengqueshijian  = ((__String*)strings->objectForKey("skillinfo_lengqueshijian"))->m_sString.c_str();
	const char *lengqueshijian = StringDataManager::getString("skillinfo_lengqueshijian");
	char* skillinfo_lengqueshijian =const_cast<char*>(lengqueshijian);
	Label * l_cd = Label::createWithTTF(skillinfo_lengqueshijian,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_cd->setColor(Color3B(212,255,151));
	char s_intervaltime[20];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	Label * l_cdValue = Label::createWithTTF(s_intervaltime,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_cdValue->setColor(Color3B(212,255,151));

	int secondLineHeight = firstLineHeight + l_mp->getContentSize().height;
	scroll_height = secondLineHeight;
	///////////////////////3////////////////////////////
	//�ʩ�����
	//const char *shifajuli  = ((__String*)strings->objectForKey("skillinfo_shifajuli"))->m_sString.c_str();
	const char *shifajuli = StringDataManager::getString("skillinfo_shifajuli");
	char* skillinfo_shifajuli =const_cast<char*>(shifajuli);
	Label * l_dis = Label::createWithTTF(skillinfo_shifajuli,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_dis->setColor(Color3B(212,255,151));
	char s_distance[20];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	Label * l_disValue = Label::createWithTTF(s_distance,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_disValue->setColor(Color3B(212,255,151));
	//�ְҵ
	//const char *zhiye  = ((__String*)strings->objectForKey("skillinfo_zhiye"))->m_sString.c_str();
	const char *zhiye = StringDataManager::getString("skillinfo_zhiye");
	char* skillinfo_zhiye =const_cast<char*>(zhiye);
	Label * l_pro = Label::createWithTTF(skillinfo_zhiye,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
	l_pro->setColor(Color3B(212,255,151));
	Label * l_proValue = Label::createWithTTF(this->getProfessionBySkill(gameFightSkill->getId()).c_str(),APP_FONT_NAME,16,Size(100, 0 ), TextHAlignment::LEFT);
	l_proValue->setColor(Color3B(212,255,151));

	int thirdLineHeight = secondLineHeight + l_dis->getContentSize().height;
	scroll_height = thirdLineHeight;
	////////////////////4///////////////////////////////
	//��
	Label * t_des = Label::createWithTTF(gameFightSkill->getCFightSkill()->description().c_str(),APP_FONT_NAME,16,Size(235, 0 ), TextHAlignment::LEFT);
	t_des->setColor(Color3B(30,255,0));

	int fourthLineHeight = thirdLineHeight+t_des->getContentSize().height;
	scroll_height = fourthLineHeight;
	/////////////////////////////////////////////////////
	int fifthLineHeight = 0;
	int sixthLineHeight = 0;
	int seventhLineHeight = 0;
	int seventhLineHeight_1 = 0;
	int eighthLineHeight = 0;
	int ninthLineHeight = 0;
	int tenthLineHeight = 0;
	int eleventhLineHeight = 0;
	int twelfthLineHeight = 0;
	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		//���һ��
		//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
		//const char *xiayiji  = ((__String*)strings->objectForKey("skillinfo_xiayiji"))->m_sString.c_str();
		const char *xiayiji = StringDataManager::getString("skillinfo_xiayiji");
		char* skillinfo_xiayiji =const_cast<char*>(xiayiji);
		l_nextLv = Label::createWithTTF(skillinfo_xiayiji,APP_FONT_NAME,16,Size(100, 0 ), TextHAlignment::LEFT);
		l_nextLv->setColor(Color3B(0,255,255));

		fifthLineHeight = fourthLineHeight+l_nextLv->getContentSize().height*2;
		scroll_height = fifthLineHeight;
		/////////////////////////////////////////////////////
		t_nextLvDes = Label::createWithTTF(gameFightSkill->getCFightSkill()->get_next_description().c_str(),APP_FONT_NAME,16,Size(235, 0 ), TextHAlignment::LEFT);
		t_nextLvDes->setColor(Color3B(0,255,255));

		sixthLineHeight = fifthLineHeight+t_nextLvDes->getContentSize().height;
		scroll_height = sixthLineHeight;
		/////////////////////////////////////////////////////
		//�����
		s_upgradeNeed = Sprite::create("res_ui/moji_0.png");
		s_upgradeNeed->setScaleY(0.6f);
		//const char *shenjixuqiu  = ((__String*)strings->objectForKey("skillinfo_shengjixuqiu"))->m_sString.c_str();
		const char *shenjixuqiu = StringDataManager::getString("skillinfo_shengjixuqiu");
		char* skillinfo_shenjixuqiu =const_cast<char*>(shenjixuqiu);
		l_upgradeNeed = Label::createWithTTF(skillinfo_shenjixuqiu,APP_FONT_NAME,18);
		l_upgradeNeed->setColor(Color3B(196,255,68));
		seventhLineHeight = sixthLineHeight + s_upgradeNeed->getContentSize().height;
		scroll_height = seventhLineHeight;

		//////////////////////////////////////////////////////
		//
		s_second_line = Sprite::create("res_ui/henggang.png");
		s_second_line->setScaleX(0.9f);
		seventhLineHeight_1 = seventhLineHeight + s_first_line->getContentSize().height+7;
		scroll_height = seventhLineHeight_1;

		/////////////////////////////////////////////////////
		//���ǵȼ�
		if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
		{
						Label * l_playerLv = Label::create();
			l_playerLv->setText(StringDataManager::getString("skillinfo_zhujuedengji"));
			l_playerLv->setFontName(APP_FONT_NAME);
			l_playerLv->setFontSize(16);
			l_playerLv->setColor(Color3B(212,255,151));
			l_playerLv->setAnchorPoint(Vec2(0,0));
			l_playerLv->setName("l_playerLv");
			panel_nextInfo->addChild(l_playerLv);

			Label * l_playerLv_colon = Label::create();
			l_playerLv_colon->setText(":");
			l_playerLv_colon->setFontName(APP_FONT_NAME);
			l_playerLv_colon->setFontSize(16);
			l_playerLv_colon->setColor(Color3B(212,255,151));
			l_playerLv_colon->setAnchorPoint(Vec2(0,0));
			l_playerLv_colon->setName("l_playerLv_colon");
			panel_nextInfo->addChild(l_playerLv_colon);

			char s_required_level[5];
			sprintf(s_required_level,"%d",m_required_level);
			Label * l_playerLvValue = Label::create();
			l_playerLvValue->setText(s_required_level);
			l_playerLvValue->setFontName(APP_FONT_NAME);
			l_playerLvValue->setFontSize(16);
			l_playerLvValue->setTextAreaSize(Size(230, 0 ));
			l_playerLvValue->setColor(Color3B(212,255,151));
			l_playerLvValue->setAnchorPoint(Vec2(0,0));
			l_playerLvValue->setName("l_playerLvValue");
			panel_nextInfo->addChild(l_playerLvValue);

			eighthLineHeight = seventhLineHeight_1 + l_playerLv->getContentSize().height;
			scroll_height = eighthLineHeight;

			if (GameView::getInstance()->myplayer->getActiveRole()->level()<m_required_level)  //����ȼ�δ�ﵽ������Ҫ�
			{
				l_playerLv->setColor(Color3B(255,51,51));
				l_playerLv_colon->setColor(Color3B(255,51,51));
				l_playerLvValue->setColor(Color3B(255,51,51));
			}



			const char *zhujuedengji = StringDataManager::getString("skillinfo_zhujuedengji");
			char* skillinfo_zhujuedengji =const_cast<char*>(zhujuedengji);
			l_playerLv = Label::createWithTTF(skillinfo_zhujuedengji,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
			l_playerLv->setColor(Color3B(212,255,151));
			char s_next_required_level[20];
			sprintf(s_next_required_level,"%d",gameFightSkill->getCFightSkill()->get_next_required_level());
			l_playerLvValue = Label::createWithTTF(s_next_required_level,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
			l_playerLvValue->setColor(Color3B(212,255,151));

			eighthLineHeight = seventhLineHeight_1 + l_playerLv->getContentSize().height;
			scroll_height = eighthLineHeight;

			if (GameView::getInstance()->myplayer->getActiveRole()->level()<gameFightSkill->getCFightSkill()->get_next_required_level())  //�����ȼ�δ�ﵽ������Ҫ�
			{
				l_playerLv->setColor(Color3B(255,51,51));
				l_playerLvValue->setColor(Color3B(255,51,51));
			}
		}
		else
		{
			eighthLineHeight = seventhLineHeight_1;
			scroll_height = eighthLineHeight;
		}
		//��ܵ
		if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
		{
			//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *jinengdian  = ((__String*)strings->objectForKey("skillinfo_jinengdian"))->m_sString.c_str();
			const char *jinengdian = StringDataManager::getString("skillinfo_jinengdian");
			char* skillinfo_jinengdian =const_cast<char*>(jinengdian);
			l_skillPoint = Label::createWithTTF(skillinfo_jinengdian,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
			l_skillPoint->setColor(Color3B(212,255,151));

			char s_next_required_point[20];
			sprintf(s_next_required_point,"%d",gameFightSkill->getCFightSkill()->get_next_required_point());
			l_skillPointValue = Label::createWithTTF(s_next_required_point,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
			l_skillPointValue->setColor(Color3B(212,255,151));

			ninthLineHeight = eighthLineHeight + l_skillPoint->getContentSize().height;
			scroll_height = ninthLineHeight;

			if(GameView::getInstance()->myplayer->player->skillpoint()<gameFightSkill->getCFightSkill()->get_next_required_point())//㼼�ܵ㲻�
			{
				l_skillPoint->setColor(Color3B(255,51,51));
				l_skillPointValue->setColor(Color3B(255,51,51));
			}
		}
		else
		{
			ninthLineHeight = eighthLineHeight;
			scroll_height = ninthLineHeight;
		}
		//XXX㼼�
		if (gameFightSkill->getCFightSkill()->get_pre_skill() != "")
		{
			std::string skillname = "";
			CBaseSkill * temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_pre_skill()];
			skillname.append(temp->name().c_str());
			skillname.append(": ");
			l_preSkill = Label::createWithTTF(skillname.c_str(),APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
			l_preSkill->setColor(Color3B(212,255,151));
			char s_pre_skill_level[20];
			sprintf(s_pre_skill_level,"%d",gameFightSkill->getCFightSkill()->get_pre_skill_level());
			l_preSkillValue = Label::createWithTTF(s_pre_skill_level,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
			l_preSkillValue->setColor(Color3B(212,255,151));

			tenthLineHeight = ninthLineHeight + l_preSkill->getContentSize().height;
			scroll_height = tenthLineHeight;

			GameFightSkill * gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[gameFightSkill->getCFightSkill()->get_pre_skill()];
			if (gameFightSkillStuded)
			{
				if (gameFightSkillStuded->getLevel() < gameFightSkill->getCFightSkill()->get_next_pre_skill_level())
				{
					l_preSkill->setColor(Color3B(255,51,51));
					l_preSkillValue->setColor(Color3B(255,51,51));
				}
			}

		}
		else
		{
			tenthLineHeight = ninthLineHeight;
			scroll_height = tenthLineHeight;
		}
		//ܽ�
		if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
		{
			//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *jinbi  = ((__String*)strings->objectForKey("skillinfo_jinbi"))->m_sString.c_str();
			const char *jinbi = StringDataManager::getString("skillinfo_jinbi");
			char* skillinfo_jinbi =const_cast<char*>(jinbi);
			l_gold = Label::createWithTTF(skillinfo_jinbi,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
			l_gold->setColor(Color3B(212,255,151));
			char s_next_required_gold[20];
			sprintf(s_next_required_gold,"%d",gameFightSkill->getCFightSkill()->get_next_required_gold());
			l_goldValue = Label::createWithTTF(s_next_required_gold,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
			l_goldValue->setColor(Color3B(212,255,151));

			eleventhLineHeight = tenthLineHeight + l_gold->getContentSize().height;
			scroll_height = eleventhLineHeight;

			if(GameView::getInstance()->getPlayerGold()<gameFightSkill->getCFightSkill()->get_next_required_gold())//ҽ�Ҳ��
			{
				l_gold->setColor(Color3B(255,51,51));
				l_goldValue->setColor(Color3B(255,51,51));
			}

		}
		else
		{
			eleventhLineHeight = tenthLineHeight;
			scroll_height = eleventhLineHeight;
		}
		//㼼���
		if (gameFightSkill->getCFightSkill()->get_next_required_num()>0)
		{
			const char *jinengshu = StringDataManager::getString("skillinfo_jinengshu");
			char* skillinfo_jinengshu =const_cast<char*>(jinengshu);
			l_prop = Label::createWithTTF(skillinfo_jinengshu,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
			l_prop->setColor(Color3B(212,255,151));
			char s_required_skillBook[20];
			sprintf(s_required_skillBook,"%d",gameFightSkill->getCFightSkill()->get_next_required_num());
			l_propValue = Label::createWithTTF(s_required_skillBook,APP_FONT_NAME,16,Size(0, 0 ), TextHAlignment::LEFT);
			l_propValue->setColor(Color3B(212,255,151));

			twelfthLineHeight = eleventhLineHeight + l_prop->getContentSize().height;
			scroll_height = twelfthLineHeight;

			int propNum = 0;
			for (int i = 0;i<GameView::getInstance()->AllPacItem.size();++i)
			{
				FolderInfo * folder = GameView::getInstance()->AllPacItem.at(i);
				if (!folder->has_goods())
					continue;

				if (folder->goods().id() == gameFightSkill->getCFightSkill()->get_required_prop())
				{
					propNum += folder->quantity();
				}
			}
			if(propNum<gameFightSkill->getCFightSkill()->get_next_required_num())//鼼���鲻�
			{
				l_prop->setColor(Color3B(212,59,59));
				l_propValue->setColor(Color3B(212,59,59));
			}
		}
	}

	ScrollView * m_scrollView = ScrollView::create(Size(240,238));
	m_scrollView->setViewSize(Size(240, 238));
	m_scrollView->setIgnoreAnchorPointForPosition(false);
	m_scrollView->setTouchEnabled(true);
	m_scrollView->setDirection(TableView::Direction::VERTICAL);
	m_scrollView->setAnchorPoint(Vec2(0,0));
	m_scrollView->setPosition(Vec2(24,84));
	m_scrollView->setBounceable(true);
	m_scrollView->setClippingToBounds(true);
	m_scrollView->setTag(kTag_ScrollView);
	addChild(m_scrollView);
	if (scroll_height > 239)
	{
		m_scrollView->setContentSize(Size(240,scroll_height));
		m_scrollView->setContentOffset(Vec2(0,238-scroll_height));  
	}
	else
	{
		m_scrollView->setContentSize(Vec2(240,238));
	}
	m_scrollView->setClippingToBounds(true);

	s_nameFrame->setIgnoreAnchorPointForPosition(false);
	s_nameFrame->setAnchorPoint(Vec2(0,0));
	m_scrollView->addChild(s_nameFrame);
	s_nameFrame->setPosition(Vec2(8,m_scrollView->getContentSize().height - zeroLineHeight));

	m_scrollView->addChild(l_name);
	l_name->setPosition(Vec2(15,m_scrollView->getContentSize().height - zeroLineHeight));

	m_scrollView->addChild(s_first_line);
	s_first_line->setPosition(Vec2(5,m_scrollView->getContentSize().height - zeroLineHeight_1+5));

	l_type->setIgnoreAnchorPointForPosition( false );  
	l_type->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_type);
	l_type->setPosition(Vec2(8,m_scrollView->getContentSize().height - firstLineHeight));

	l_mp->setIgnoreAnchorPointForPosition( false );  
	l_mp->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_mp);
	l_mp->setPosition(Vec2(105,m_scrollView->getContentSize().height - firstLineHeight));

	l_mpValue->setIgnoreAnchorPointForPosition( false );  
	l_mpValue->setAnchorPoint( Vec2( 0.5f, 0.5f ) );  
	m_scrollView->addChild(l_mpValue);
	l_mpValue->setPosition(Vec2(105+l_mp->getContentSize().width+2,m_scrollView->getContentSize().height - firstLineHeight));

	l_lv->setIgnoreAnchorPointForPosition( false );  
	l_lv->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_lv);
	l_lv->setPosition(Vec2(8,m_scrollView->getContentSize().height - secondLineHeight));

	l_lvValue->setIgnoreAnchorPointForPosition( false );  
	l_lvValue->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_lvValue);
	l_lvValue->setPosition(Vec2(8+l_lv->getContentSize().width+2,m_scrollView->getContentSize().height - secondLineHeight));

	l_cd->setIgnoreAnchorPointForPosition( false );  
	l_cd->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_cd);
	l_cd->setPosition(Vec2(105,m_scrollView->getContentSize().height - secondLineHeight));

	l_cdValue->setIgnoreAnchorPointForPosition( false );  
	l_cdValue->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_cdValue);
	l_cdValue->setPosition(Vec2(105+l_cd ->getContentSize().width+2,m_scrollView->getContentSize().height - secondLineHeight));

	l_pro->setIgnoreAnchorPointForPosition( false );  
	l_pro->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_pro);
	l_pro->setPosition(Vec2(8,m_scrollView->getContentSize().height - thirdLineHeight));

	l_proValue->setIgnoreAnchorPointForPosition( false );  
	l_proValue->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_proValue);
	l_proValue->setPosition(Vec2(8+l_pro->getContentSize().width+2,m_scrollView->getContentSize().height - thirdLineHeight));

	l_dis->setIgnoreAnchorPointForPosition( false );  
	l_dis->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_dis);
	l_dis->setPosition(Vec2(105,m_scrollView->getContentSize().height - thirdLineHeight));

	l_disValue->setIgnoreAnchorPointForPosition( false );  
	l_disValue->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(l_disValue);
	l_disValue->setPosition(Vec2(105+l_dis->getContentSize().width+2,m_scrollView->getContentSize().height - thirdLineHeight));

	if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		l_mp->setVisible(false);
		l_mpValue->setVisible(false);
		l_cd->setVisible(false);
		l_cdValue->setVisible(false);
		l_dis->setVisible(false);
		l_disValue->setVisible(false);
	}

	t_des->setIgnoreAnchorPointForPosition( false );  
	t_des->setAnchorPoint( Vec2( 0, 0 ) );  
	m_scrollView->addChild(t_des);
	t_des->setPosition(Vec2(8,m_scrollView->getContentSize().height - fourthLineHeight));

	if (gameFightSkill->getLevel()<gameFightSkill->getCBaseSkill()->maxlevel())
	{
		l_nextLv->setIgnoreAnchorPointForPosition( false );  
		l_nextLv->setAnchorPoint( Vec2( 0, 0 ) );  
		m_scrollView->addChild(l_nextLv);
		l_nextLv->setPosition(Vec2(8,m_scrollView->getContentSize().height - fifthLineHeight));

		t_nextLvDes->setIgnoreAnchorPointForPosition( false );  
		t_nextLvDes->setAnchorPoint( Vec2( 0, 0 ) );  
		m_scrollView->addChild(t_nextLvDes);
		t_nextLvDes->setPosition(Vec2(8,m_scrollView->getContentSize().height - sixthLineHeight));

		s_upgradeNeed->setIgnoreAnchorPointForPosition( false );  
		s_upgradeNeed->setAnchorPoint( Vec2( 0, 0 ) );  
		m_scrollView->addChild(s_upgradeNeed);
		s_upgradeNeed->setPosition(Vec2(8,m_scrollView->getContentSize().height - seventhLineHeight));

		m_scrollView->addChild(l_upgradeNeed);
		l_upgradeNeed->setPosition(Vec2(15,m_scrollView->getContentSize().height - seventhLineHeight));

		m_scrollView->addChild(s_second_line);
		s_second_line->setPosition(Vec2(5,m_scrollView->getContentSize().height - seventhLineHeight_1+5));

		//���ǵȼ�
		if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
		{
			l_playerLv->setIgnoreAnchorPointForPosition( false );  
			l_playerLv->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(l_playerLv);
			l_playerLv->setPosition(Vec2(8,m_scrollView->getContentSize().height - eighthLineHeight));

			l_playerLvValue->setIgnoreAnchorPointForPosition( false );  
			l_playerLvValue->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(l_playerLvValue);
			l_playerLvValue->setPosition(Vec2(8+l_playerLv->getContentSize().width+5,m_scrollView->getContentSize().height - eighthLineHeight));
		}
		//���ܵ
		if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
		{
			l_skillPoint->setIgnoreAnchorPointForPosition( false );  
			l_skillPoint->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(l_skillPoint);
			l_skillPoint->setPosition(Vec2(8,m_scrollView->getContentSize().height - ninthLineHeight));

			l_skillPointValue->setIgnoreAnchorPointForPosition( false );  
			l_skillPointValue->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(l_skillPointValue);
			l_skillPointValue->setPosition(Vec2(8+l_skillPoint->getContentSize().width+5,m_scrollView->getContentSize().height - ninthLineHeight));
		}
		//XXX㼼�
		if (gameFightSkill->getCFightSkill()->get_pre_skill() != "")
		{
			l_preSkill->setIgnoreAnchorPointForPosition( false );  
			l_preSkill->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(l_preSkill);
			l_preSkill->setPosition(Vec2(8,m_scrollView->getContentSize().height - tenthLineHeight));

			l_preSkillValue->setIgnoreAnchorPointForPosition( false );  
			l_preSkillValue->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(l_preSkillValue);
			l_preSkillValue->setPosition(Vec2(8+l_preSkill->getContentSize().width+5,m_scrollView->getContentSize().height - tenthLineHeight));
		}
		//ܽ�
		if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
		{
			l_gold->setIgnoreAnchorPointForPosition( false );  
			l_gold->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(l_gold);
			l_gold->setPosition(Vec2(8,m_scrollView->getContentSize().height - eleventhLineHeight));

			l_goldValue->setIgnoreAnchorPointForPosition( false );  
			l_goldValue->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(l_goldValue);
			l_goldValue->setPosition(Vec2(8+l_gold->getContentSize().width+5,m_scrollView->getContentSize().height - eleventhLineHeight));
		}
		//Ҽ����
		if (gameFightSkill->getCFightSkill()->get_next_required_num()>0)
		{
			l_prop->setIgnoreAnchorPointForPosition( false );  
			l_prop->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(l_prop);
			l_prop->setPosition(Vec2(8,m_scrollView->getContentSize().height - twelfthLineHeight));

			l_propValue->setIgnoreAnchorPointForPosition( false );  
			l_propValue->setAnchorPoint( Vec2( 0, 0 ) );  
			m_scrollView->addChild(l_propValue);
			l_propValue->setPosition(Vec2(8+l_prop->getContentSize().width+5,m_scrollView->getContentSize().height - twelfthLineHeight));
		}
	}
	*/
}


