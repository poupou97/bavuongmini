
#ifndef _GENERALSPOPUPUI_STRATEGIESUPGRADEUI_H_
#define _GENERALSPOPUPUI_STRATEGIESUPGRADEUI_H_

#include "../../extensions/UIScene.h"
#include "../../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CFightWayBase;

class StrategiesUpgradeUI : public UIScene
{
public:
	StrategiesUpgradeUI();
	~StrategiesUpgradeUI();

	static StrategiesUpgradeUI * create(int selectFightWayId);
	bool init(int selectFightWayId);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void UpgradeEvent(Ref *pSender, Widget::TouchEventType type);
	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	void RefreshPropertyInfo(CFightWayBase * fightWayBase);
	void RefreshButtonStatus();

private:
	Text * l_name;
	Text * l_level;
	Text * l_upgrade;
	Text * l_property1_l;
	Text * l_property1_r;
	Text * l_property2_l;
	Text * l_property2_r;
	Text * l_property3_l;
	Text * l_property3_r;
	Text * l_property4_l;
	Text * l_property4_r;
	Text * l_property5_l;
	Text * l_property5_r;
	Text * l_costGoldValue;
	Text * l_costConsumeValue;

public:
	CFightWayBase * curFightWayBase ;

	Button * Button_close;
	Button * Button_upgrade;

	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	//ѧϰ�
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	//󷨹رս��
	void addCCTutorialIndicator2(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

private:
	//��ѧ
	int mTutorialScriptInstanceId;
};

#endif
