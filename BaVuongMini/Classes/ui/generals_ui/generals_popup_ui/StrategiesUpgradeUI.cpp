#include "StrategiesUpgradeUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "../../../messageclient/element/CFightWayBase.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../messageclient/element/CFightWayGrowingUp.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../GeneralsUI.h"
#include "../GeneralsStrategiesUI.h"
#include "../../../legend_script/CCTutorialIndicator.h"
#include "../../../legend_script/Script.h"
#include "../../../legend_script/ScriptManager.h"
#include "../../../legend_script/CCTeachingGuide.h"
#include "../../../gamescene_state/MainScene.h"

#define  Max_Level 10

StrategiesUpgradeUI::StrategiesUpgradeUI()
{
}


StrategiesUpgradeUI::~StrategiesUpgradeUI()
{
	delete curFightWayBase;
}

StrategiesUpgradeUI * StrategiesUpgradeUI::create(int selectFightWayId)
{
	auto strategiesUpgradeUI = new StrategiesUpgradeUI();
	if (strategiesUpgradeUI && strategiesUpgradeUI->init(selectFightWayId))
	{
		strategiesUpgradeUI->autorelease();
		return strategiesUpgradeUI;
	}
	CC_SAFE_DELETE(strategiesUpgradeUI);
	return NULL;
}

bool StrategiesUpgradeUI::init(int selectFightWayId)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate(); 
		curFightWayBase = new CFightWayBase();		
		for (int i = 0;i<FightWayConfigData::s_fightWayBase.size();++i)
		{
			if (selectFightWayId == FightWayConfigData::s_fightWayBase.at(i)->fightwayid())
			{
				curFightWayBase->CopyFrom(*FightWayConfigData::s_fightWayBase.at(i));
				curFightWayBase->set_name(FightWayConfigData::s_fightWayBase.at(i)->get_name());
				curFightWayBase->set_kValue(FightWayConfigData::s_fightWayBase.at(i)->get_kValue());
				curFightWayBase->set_icon(FightWayConfigData::s_fightWayBase.at(i)->get_icon());
				curFightWayBase->set_description(FightWayConfigData::s_fightWayBase.at(i)->get_description());
				break;
			}
		}
		if (GeneralsUI::generalsStrategiesUI)
		{
			for (int i = 0;i<GeneralsUI::generalsStrategiesUI->generalsStrategiesList.size();++i)
			{
				if (selectFightWayId == GeneralsUI::generalsStrategiesUI->generalsStrategiesList.at(i)->fightwayid())
				{
					curFightWayBase->CopyFrom(*GeneralsUI::generalsStrategiesUI->generalsStrategiesList.at(i));
					break;
				}
			}
		}
		//���UI
		if(LoadSceneLayer::StrategiesUpgradeLayer->getParent() != NULL)
		{
			LoadSceneLayer::StrategiesUpgradeLayer->removeFromParentAndCleanup(false);
		}
		auto ppanel = LoadSceneLayer::StrategiesUpgradeLayer;
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(350, 400));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(StrategiesUpgradeUI::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);

		Button_upgrade = (Button*)Helper::seekWidgetByName(ppanel,"Button_upgrade");
		Button_upgrade->setTouchEnabled(true);
		Button_upgrade->addTouchEventListener(CC_CALLBACK_2(StrategiesUpgradeUI::UpgradeEvent, this));
		Button_upgrade->setPressedActionEnabled(true);

		l_name = (Text*)Helper::seekWidgetByName(ppanel,"Label_nameValue");
		l_level = (Text*)Helper::seekWidgetByName(ppanel,"Label_lvValue");

		l_costGoldValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_ingot");
		l_costConsumeValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_consume_value");

		//���˫״̬��������Ϣ
		auto t_musouDes = (TextField*)Helper::seekWidgetByName(ppanel,"TextArea_consume");
		t_musouDes->setString(curFightWayBase->get_description().c_str());

		l_property1_l = (Text*)Helper::seekWidgetByName(ppanel,"Label_CurProperty_1");
		l_property1_r = (Text*)Helper::seekWidgetByName(ppanel,"Label_MaXProperty_1");
		l_property2_l = (Text*)Helper::seekWidgetByName(ppanel,"Label_CurProperty_2");
		l_property2_r = (Text*)Helper::seekWidgetByName(ppanel,"Label_MaxProperty_2");
		l_property3_l = (Text*)Helper::seekWidgetByName(ppanel,"Label_CurProperty_3");
		l_property3_r = (Text*)Helper::seekWidgetByName(ppanel,"Label_MaxProperty_3");
		l_property4_l = (Text*)Helper::seekWidgetByName(ppanel,"Label_CurProperty_4");
		l_property4_r = (Text*)Helper::seekWidgetByName(ppanel,"Label_MaxProperty_4");
		l_property5_l = (Text*)Helper::seekWidgetByName(ppanel,"Label_CurProperty_5");
		l_property5_r = (Text*)Helper::seekWidgetByName(ppanel,"Label_MaxProperty_5");
		
		RefreshPropertyInfo(curFightWayBase);

		l_upgrade = (Text*)Helper::seekWidgetByName(Button_upgrade,"Label_upgrade");
		RefreshButtonStatus();

		this->setContentSize(Size(454,452));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		//this->setTouchPriority(-1);
		//m_pLayer->setTouchPriority(-2);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(StrategiesUpgradeUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(StrategiesUpgradeUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(StrategiesUpgradeUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(StrategiesUpgradeUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void StrategiesUpgradeUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void StrategiesUpgradeUI::onExit()
{
	UIScene::onExit();
}

bool StrategiesUpgradeUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void StrategiesUpgradeUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void StrategiesUpgradeUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void StrategiesUpgradeUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void StrategiesUpgradeUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void StrategiesUpgradeUI::UpgradeEvent(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (curFightWayBase->level() >= Max_Level)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_strategies_maxLevel"));
		}
		else
		{
			CFightWayGrowingUp * nextFightWayGrowingUp;
			if (curFightWayBase->level()>0)
			{
				if (curFightWayBase->level() == 10)
				{
					nextFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[curFightWayBase->fightwayid()]->at(10);
				}
				else
				{
					nextFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[curFightWayBase->fightwayid()]->at(curFightWayBase->level() + 1);
				}
			}
			else
			{
				nextFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[curFightWayBase->fightwayid()]->at(1);
			}

			GameMessageProcessor::sharedMsgProcessor()->sendReq(5074, (void *)curFightWayBase->fightwayid());

			auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if (sc != NULL)
				sc->endCommand(pSender);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void StrategiesUpgradeUI::RefreshButtonStatus()
{
	//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
	if (curFightWayBase->level()>0)
	{
		//const char *str_upgrade  = ((__String*)strings->objectForKey("generals_strategies_upgrade"))->m_sString.c_str();
		const char *str_upgrade = StringDataManager::getString("generals_strategies_upgrade");
		l_upgrade->setString(str_upgrade);
	}
	else
	{
		//const char *str_study  = ((__String*)strings->objectForKey("generals_strategies_study"))->m_sString.c_str();
		const char *str_study = StringDataManager::getString("generals_strategies_study");
		l_upgrade->setString(str_study);
	}
}

void StrategiesUpgradeUI::RefreshPropertyInfo( CFightWayBase * fightWayBase )
{
	CFightWayGrowingUp * curFightWayGrowingUp;
	CFightWayGrowingUp * nextFightWayGrowingUp;
	if (fightWayBase->level()>0)
	{
		if (fightWayBase->level() == 10)
		{
			curFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(fightWayBase->level());
			nextFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(fightWayBase->level());
		}
		else
		{
			curFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(fightWayBase->level());
			nextFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(fightWayBase->level()+1);
		}
	}
	else
	{
		curFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(1);
		nextFightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(1);
	}

	l_name->setString(fightWayBase->get_name().c_str());
	std::string str_lv = "LV";
	char s_lv[10];
	if (fightWayBase->level() <= 1)
	{
		sprintf(s_lv,"%d",1);
	}
	else
	{
		sprintf(s_lv,"%d",fightWayBase->level());
	}
	str_lv.append(s_lv);
	l_level->setString(str_lv.c_str());

	//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
	//1��λ���ԣ��
	std::string str_property1_l = "generals_strategies_property_";
	char s_grid1_property_l[10];
	sprintf(s_grid1_property_l,"%d",curFightWayGrowingUp->get_grid1_property());
	str_property1_l.append(s_grid1_property_l);
	//const char *shuxing_1_l  = ((__String*)strings->objectForKey(str_property1_l))->m_sString.c_str();
	const char *shuxing_1_l = StringDataManager::getString(str_property1_l.c_str());
	std::string property1_l_value;
	property1_l_value.append(shuxing_1_l);
	property1_l_value.append("+");
	if (fightWayBase->level()>0)
	{
		char s_grid1_value[10];
		sprintf(s_grid1_value,"%.1f",curFightWayGrowingUp->get_grid1_value());
		property1_l_value.append(s_grid1_value);
	}
	else
	{
		property1_l_value.append("0");
	}
	property1_l_value.append("%");
	l_property1_l->setString(property1_l_value.c_str());
	//1󣩺�λ���ԣ��ң�
	std::string str_property1_r = "generals_strategies_property_";
	char s_grid1_property_r[10];
	sprintf(s_grid1_property_r,"%d",nextFightWayGrowingUp->get_grid1_property());
	str_property1_r.append(s_grid1_property_r);
	//const char *shuxing_1_r  = ((__String*)strings->objectForKey(str_property1_r))->m_sString.c_str();
	const char *shuxing_1_r = StringDataManager::getString(str_property1_r.c_str());
	std::string property1_r_value;
	property1_r_value.append(shuxing_1_r);
	property1_r_value.append("+");

	char s_grid1_value_r[10];
	sprintf(s_grid1_value_r,"%.1f",nextFightWayGrowingUp->get_grid1_value());
	property1_r_value.append(s_grid1_value_r);

	property1_r_value.append("%");
	l_property1_r->setString(property1_r_value.c_str());


	//2��λ���ԣ��
	std::string str_property2_l = "generals_strategies_property_";
	char s_grid2_property_l[10];
	sprintf(s_grid2_property_l,"%d",curFightWayGrowingUp->get_grid2_property());
	str_property2_l.append(s_grid2_property_l);
	//const char *shuxing_2_l  = ((__String*)strings->objectForKey(str_property2_l))->m_sString.c_str();
	const char *shuxing_2_l = StringDataManager::getString(str_property2_l.c_str());
	std::string property2_l_value;
	property2_l_value.append(shuxing_2_l);
	property2_l_value.append("+");
	if (fightWayBase->level()>0)
	{
		char s_grid2_value_l[10];
		sprintf(s_grid2_value_l,"%.1f",curFightWayGrowingUp->get_grid2_value());
		property2_l_value.append(s_grid2_value_l);
	}
	else
	{
		property2_l_value.append("0");
	}
	property2_l_value.append("%");
	l_property2_l->setString(property2_l_value.c_str());
	//2󣩺�λ���ԣ��ң�
	std::string str_property2_r = "generals_strategies_property_";
	char s_grid2_property_r[10];
	sprintf(s_grid2_property_r,"%d",nextFightWayGrowingUp->get_grid2_property());
	str_property2_r.append(s_grid2_property_r);
	//const char *shuxing_2_r  = ((__String*)strings->objectForKey(str_property2_r))->m_sString.c_str();
	const char *shuxing_2_r = StringDataManager::getString(str_property2_r.c_str());
	std::string property2_r_value;
	property2_r_value.append(shuxing_2_r);
	property2_r_value.append("+");
	char s_grid2_value_r[10];
	sprintf(s_grid2_value_r,"%.1f",nextFightWayGrowingUp->get_grid2_value());
	property2_r_value.append(s_grid2_value_r);
	property2_r_value.append("%");
	l_property2_r->setString(property2_r_value.c_str());

	//3��λ���ԣ��
	std::string str_property3_l = "generals_strategies_property_";
	char s_grid3_property_l[10];
	sprintf(s_grid3_property_l,"%d",curFightWayGrowingUp->get_grid3_property());
	str_property3_l.append(s_grid3_property_l);
	//const char *shuxing_3_l  = ((__String*)strings->objectForKey(str_property3_l))->m_sString.c_str();
	const char *shuxing_3_l = StringDataManager::getString(str_property3_l.c_str());
	std::string property3_l_value;
	property3_l_value.append(shuxing_3_l);
	property3_l_value.append("+");
	if (fightWayBase->level()>0)
	{
		char s_grid3_value_l[10];
		sprintf(s_grid3_value_l,"%.1f",curFightWayGrowingUp->get_grid3_value());
		property3_l_value.append(s_grid3_value_l);
	}
	else
	{
		property3_l_value.append("0");
	}
	property3_l_value.append("%");
	l_property3_l->setString(property3_l_value.c_str());
	//3󣩺�λ���ԣ��ң�
	std::string str_property3_r = "generals_strategies_property_";
	char s_grid3_property_r[10];
	sprintf(s_grid3_property_r,"%d",nextFightWayGrowingUp->get_grid3_property());
	str_property3_r.append(s_grid3_property_r);
	//const char *shuxing_3_r  = ((__String*)strings->objectForKey(str_property3_r))->m_sString.c_str();
	const char *shuxing_3_r = StringDataManager::getString(str_property3_r.c_str());
	std::string property3_r_value;
	property3_r_value.append(shuxing_3_r);
	property3_r_value.append("+");
	char s_grid3_value[10];
	sprintf(s_grid3_value,"%.1f",nextFightWayGrowingUp->get_grid3_value());
	property3_r_value.append(s_grid3_value);
	property3_r_value.append("%");
	l_property3_r->setString(property3_r_value.c_str());


	//4��λ���ԣ��
	std::string str_property4_l = "generals_strategies_property_";
	char s_grid4_property_l[10];
	sprintf(s_grid4_property_l,"%d",curFightWayGrowingUp->get_grid4_property());
	str_property4_l.append(s_grid4_property_l);
	//const char *shuxing_4_l  = ((__String*)strings->objectForKey(str_property4_l))->m_sString.c_str();
	const char *shuxing_4_l = StringDataManager::getString(str_property4_l.c_str());
	std::string property4_l_value;
	property4_l_value.append(shuxing_4_l);
	property4_l_value.append("+");
	if (fightWayBase->level()>0)
	{
		char s_grid4_value_l[10];
		sprintf(s_grid4_value_l,"%.1f",curFightWayGrowingUp->get_grid4_value());
		property4_l_value.append(s_grid4_value_l);
	}
	else
	{
		property4_l_value.append("0");
	}
	property4_l_value.append("%");
	l_property4_l->setString(property4_l_value.c_str());
	//4󣩺�λ���ԣ��ң�
	std::string str_property4_r = "generals_strategies_property_";
	char s_grid4_property_r[10];
	sprintf(s_grid4_property_r,"%d",nextFightWayGrowingUp->get_grid4_property());
	str_property4_r.append(s_grid4_property_r);
	//const char *shuxing_4_r  = ((__String*)strings->objectForKey(str_property4_r))->m_sString.c_str();
	const char *shuxing_4_r = StringDataManager::getString(str_property4_r.c_str());
	std::string property4_r_value;
	property4_r_value.append(shuxing_4_r);
	property4_r_value.append("+");
	char s_grid4_value_r[10];
	sprintf(s_grid4_value_r,"%.1f",nextFightWayGrowingUp->get_grid4_value());
	property4_r_value.append(s_grid4_value_r);
	property4_r_value.append("%");
	l_property4_r->setString(property4_r_value.c_str());


	//5��λ���ԣ��
	std::string str_property5_l = "generals_strategies_property_";
	char s_grid5_property_l[10];
	sprintf(s_grid5_property_l,"%d",curFightWayGrowingUp->get_grid5_property());
	str_property5_l.append(s_grid5_property_l);
	//const char *shuxing_5_l  = ((__String*)strings->objectForKey(str_property5_l))->m_sString.c_str();
	const char *shuxing_5_l = StringDataManager::getString(str_property5_l.c_str());
	std::string property5_l_value;
	property5_l_value.append(shuxing_5_l);
	property5_l_value.append("+");
	if (fightWayBase->level()>0)
	{
		char s_grid5_value_l[10];
		sprintf(s_grid5_value_l,"%.1f",curFightWayGrowingUp->get_grid5_value());
		property5_l_value.append(s_grid5_value_l);
	}
	else
	{
		property5_l_value.append("0");
	}
	property5_l_value.append("%");
	l_property5_l->setString(property5_l_value.c_str());
	//5󣩺�λ���ԣ��ң�
	std::string str_property5_r = "generals_strategies_property_";
	char s_grid5_property_r[10];
	sprintf(s_grid5_property_r,"%d",nextFightWayGrowingUp->get_grid5_property());
	str_property5_r.append(s_grid5_property_r);
	//const char *shuxing_5_r  = ((__String*)strings->objectForKey(str_property5_r))->m_sString.c_str();
	const char *shuxing_5_r = StringDataManager::getString(str_property5_r.c_str());
	std::string property5_r_value;
	property5_r_value.append(shuxing_5_r);
	property5_r_value.append("+");
	char s_grid5_value_r[10];
	sprintf(s_grid5_value_r,"%.1f",nextFightWayGrowingUp->get_grid5_value());
	property5_r_value.append(s_grid5_value_r);
	property5_r_value.append("%");
	l_property5_r->setString(property5_r_value.c_str());

	//��ĵĽ�Һ����
	char s_required_experience[20];
	sprintf(s_required_experience,"%d",nextFightWayGrowingUp->get_required_experience());
	l_costConsumeValue->setString(s_required_experience);
	char s_required_gold[20];
	sprintf(s_required_gold,"%d",nextFightWayGrowingUp->get_required_gold());
	l_costGoldValue->setString(s_required_gold);
}

void StrategiesUpgradeUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void StrategiesUpgradeUI::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,70,60);
// 	tutorialIndicator->setPosition(Vec2(pos.x-40,pos.y+95));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,100,43,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+174+_w,pos.y+14+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void StrategiesUpgradeUI::addCCTutorialIndicator2( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
// 	tutorialIndicator->setPosition(Vec2(pos.x-60,pos.y-75));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,40,40,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+174+_w,pos.y+14+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void StrategiesUpgradeUI::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}
