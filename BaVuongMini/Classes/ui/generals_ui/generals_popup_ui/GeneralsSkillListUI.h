
#ifndef _GENERALSPOPUPUI_GENERALSSKILLLISTUI_H_
#define _GENERALSPOPUPUI_GENERALSSKILLLISTUI_H_

class GameFightSkill;

#include "../../extensions/UIScene.h"
#include "../GeneralsSkillsUI.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GeneralsSkillListUI : public UIScene,public TableViewDelegate,public TableViewDataSource
{
public:
	GeneralsSkillListUI();
	~GeneralsSkillListUI();

	struct ReqData{
		long long generalsid ;
		std::string skillid ;
		int index;
	};

	static GeneralsSkillListUI * create(GeneralsSkillsUI::SkillTpye skill_type,int index);
	bool init(GeneralsSkillsUI::SkillTpye skill_type,int index );

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	//�װ�
	void EquipEvent(Ref *pSender, Widget::TouchEventType type);

	void RefreshSkillInfo(GameFightSkill * gameFightSkill);

	void UpdateDataByType(GeneralsSkillsUI::SkillTpye s_t,int index);
	void showSkillDes(std::string curSkillDes,std::string nextSkillDes);

	void NullSkillDesEvent(Ref *pSender, Widget::TouchEventType type);
public: 
	std::vector<GameFightSkill *>DataSourceList;
	TableView * generalsSkillsList_tableView;
	ImageView* i_skillFrame;
	ImageView* i_skillImage;
	Text* l_skillName;
	Text* l_skillType;
	Text* l_skillLvValue ;
	ImageView *imageView_skillQuality;
	Text* l_skillQualityValue;
	Text* l_skillMpValue;
	Text* l_skillCDTimeValue ;
	Text* l_skillDistanceValue;
	// ui::TextField * t_skillDescription;
	// ui::TextField * t_skillNextDescription;

private:
	Layer * Layer_skills;
	Layer * Layer_upper;
	std::string curSkillId;
	int curSlotIndex;
};

#endif