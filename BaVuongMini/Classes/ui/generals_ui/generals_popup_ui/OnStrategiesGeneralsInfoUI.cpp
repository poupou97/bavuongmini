#include "OnStrategiesGeneralsInfoUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "GameView.h"
#include "../TeachAndEvolutionCell.h"
#include "../GeneralsUI.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../messageclient/element/CGeneralBaseMsg.h"


OnStrategiesGeneralsInfoUI::OnStrategiesGeneralsInfoUI():
selectItemIndex(-1),
curGeneralId(-1),
isHaveGeneral(false),
selectGeneralId(-1)
{
}


OnStrategiesGeneralsInfoUI::~OnStrategiesGeneralsInfoUI()
{
}

OnStrategiesGeneralsInfoUI * OnStrategiesGeneralsInfoUI::create(int positionIndex)
{
	auto onStrategiesGeneralsInfoUI = new OnStrategiesGeneralsInfoUI();
	if (onStrategiesGeneralsInfoUI && onStrategiesGeneralsInfoUI->init(positionIndex))
	{
		onStrategiesGeneralsInfoUI->autorelease();
		return onStrategiesGeneralsInfoUI;
	}
	CC_SAFE_DELETE(onStrategiesGeneralsInfoUI);
	return NULL;
}

OnStrategiesGeneralsInfoUI * OnStrategiesGeneralsInfoUI::create(int positionIndex,long long generalsID)
{
	auto onStrategiesGeneralsInfoUI = new OnStrategiesGeneralsInfoUI();
	if (onStrategiesGeneralsInfoUI && onStrategiesGeneralsInfoUI->init(positionIndex,generalsID))
	{
		onStrategiesGeneralsInfoUI->autorelease();
		return onStrategiesGeneralsInfoUI;
	}
	CC_SAFE_DELETE(onStrategiesGeneralsInfoUI);
	return NULL;
}

bool OnStrategiesGeneralsInfoUI::init(int positionIndex)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		selectItemIndex = positionIndex;
		isHaveGeneral = false;

		//���UI
		if(LoadSceneLayer::OnStrategiesGeneralsInfoLayer->getParent() != NULL)
		{
			LoadSceneLayer::OnStrategiesGeneralsInfoLayer->removeFromParentAndCleanup(false);
		}

		auto ppanel = LoadSceneLayer::OnStrategiesGeneralsInfoLayer;
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);
// 
// 		Button_relax = (Button*)Helper::seekWidgetByName(ppanel,"Button_rest");
// 		Button_relax->setTouchEnabled(true);
// 		Button_relax->addTouchEventListener(CC_CALLBACK_2(OnStrategiesGeneralsInfoUI::RelaxEvent));
// 		Button_relax->setPressedActionEnabled(true);
// 
// 		if (isHaveGeneral)
// 		{
// 			Button_relax->setVisible(true);
// 		}
// 		else
// 		{
// 			Button_relax->setVisible(false);
// 		}

		//refresh data
		RefreshDataSource();

		//��佫�б
		generalList_tableView = TableView::create(this,Size(261,377));
// 		generalList_tableView ->setSelectedEnable(true);   // �֧��ѡ��״̬����ʾ
// 		generalList_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(26, 26, 1, 1), Vec2(0,5)); 
// 		generalList_tableView->setPressedActionEnabled(true);
		generalList_tableView->setDirection(TableView::Direction::VERTICAL);
		generalList_tableView->setAnchorPoint(Vec2(0,0));
		generalList_tableView->setPosition(Vec2(31,32));
		generalList_tableView->setContentOffset(Vec2(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(generalList_tableView);

		//this->setTouchEnabled(true);
		this->setContentSize(ppanel->getContentSize());
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(OnStrategiesGeneralsInfoUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(OnStrategiesGeneralsInfoUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(OnStrategiesGeneralsInfoUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(OnStrategiesGeneralsInfoUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

bool OnStrategiesGeneralsInfoUI::init(int positionIndex,long long generalsID)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		selectItemIndex = positionIndex;
		isHaveGeneral = true;
		curGeneralId = generalsID;

		//���UI
		if(LoadSceneLayer::OnStrategiesGeneralsInfoLayer->getParent() != NULL)
		{
			LoadSceneLayer::OnStrategiesGeneralsInfoLayer->removeFromParentAndCleanup(false);
		}

		auto ppanel = LoadSceneLayer::OnStrategiesGeneralsInfoLayer;
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(318, 436));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

// 		Button_relax = (Button*)Helper::seekWidgetByName(ppanel,"Button_rest");
// 		Button_relax->setTouchEnabled(true);
// 		Button_relax->addTouchEventListener(CC_CALLBACK_2(OnStrategiesGeneralsInfoUI::RelaxEvent));
// 		Button_relax->setPressedActionEnabled(true);
// 
// 		if (isHaveGeneral)
// 		{
// 			Button_relax->setVisible(true);
// 		}
// 		else
// 		{
// 			Button_relax->setVisible(false);
// 		}

		//refresh data
		RefreshDataSource();
		//��佫�б
		generalList_tableView = TableView::create(this,Size(261,377));
		generalList_tableView->setDirection(TableView::Direction::VERTICAL);
		generalList_tableView->setAnchorPoint(Vec2(0,0));
		generalList_tableView->setPosition(Vec2(32,32));
		generalList_tableView->setContentOffset(Vec2(0,0));
		generalList_tableView->setDelegate(this);
		generalList_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(generalList_tableView);

		//this->setTouchEnabled(true);
		this->setContentSize(Size(318,436));
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		return true;
	}
	return false;
}

void OnStrategiesGeneralsInfoUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void OnStrategiesGeneralsInfoUI::onExit()
{
	UIScene::onExit();
	std::vector<CGeneralBaseMsg *>::iterator iter_generalslistforfightway;
	for (iter_generalslistforfightway=generalsListForFightWay.begin();iter_generalslistforfightway !=generalsListForFightWay.end();++iter_generalslistforfightway)
	{
		delete *iter_generalslistforfightway;
	}
	generalsListForFightWay.clear();
}

bool OnStrategiesGeneralsInfoUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return resignFirstResponder(touch,this,false);
}

void OnStrategiesGeneralsInfoUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void OnStrategiesGeneralsInfoUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void OnStrategiesGeneralsInfoUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void OnStrategiesGeneralsInfoUI::RelaxEvent( Ref * pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)curGeneralId,(void *)0);

	this->closeAnim();
}

void OnStrategiesGeneralsInfoUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{

}

void OnStrategiesGeneralsInfoUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{

}

void OnStrategiesGeneralsInfoUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	int i = cell->getIdx();
	auto tempCell = dynamic_cast<GeneralsListCell*>(cell);

	//��滻
 	long long _id = generalsListForFightWay.at(i)->id();
 	GameMessageProcessor::sharedMsgProcessor()->sendReq(5073,(void *)_id,(void *)selectItemIndex);

	this->closeAnim();

// 	int i = cell->getIdx();
// 	if (generalsListStatus == HaveNone)
// 	{
// 		//�滻
// 		long long _id = GameView::getInstance()->generalsListForFightWay.at(i)->id();
// 		GameMessageProcessor::sharedMsgProcessor()->sendReq(5073,(void *)_id,(void *)selectItemIndex);
// 	}
// 	else if (generalsListStatus == HaveLast)
// 	{
// 		if (i == 0)
// 		{
// 			//req for last
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage-1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 4;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//�滻
// 			long long _id = GameView::getInstance()->generalsListForFightWay.at(i-1)->id();
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5073,(void *)_id,(void *)selectItemIndex);
// 		}
// 	}
// 	else if (generalsListStatus == HaveNext)
// 	{
// 		if (i == GameView::getInstance()->generalsListForFightWay.size())
// 		{
// 			//req for next
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage+1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 4;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//�滻
// 			long long _id = GameView::getInstance()->generalsListForFightWay.at(i)->id();
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5073,(void *)_id,(void *)selectItemIndex);
// 		}
// 	}
// 	else if (generalsListStatus == HaveBoth)
// 	{
// 		if (i == 0)
// 		{
// 			//req for lase
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage-1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 4;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else if (i == GameView::getInstance()->generalsListForFightWay.size()+1)
// 		{
// 			//req for next
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage+1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 4;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//�滻
// 			long long _id = GameView::getInstance()->generalsListForFightWay.at(i-1)->id();
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5073,(void *)_id,(void *)selectItemIndex);
// 		}
// 	}

}

cocos2d::Size OnStrategiesGeneralsInfoUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(260,72);
}

cocos2d::extension::TableViewCell* OnStrategiesGeneralsInfoUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = TeachAndEvolutionCell::create(generalsListForFightWay.at(idx)); 
// 	if(idx == generalsListForFightWay.size()-3)
// 	{
// 		if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
// 		{
// 			//req for next
// 			this->isReqNewly = false;
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage+1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 4;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 	}

	return cell;
}

ssize_t OnStrategiesGeneralsInfoUI::numberOfCellsInTableView( TableView *table )
{
	return generalsListForFightWay.size();
// 	if (generalsListStatus == HaveNone)
// 	{
// 		return GameView::getInstance()->generalsListForFightWay.size();
// 	}
// 	else if (generalsListStatus == HaveLast ||generalsListStatus == HaveNext)
// 	{
// 		return GameView::getInstance()->generalsListForFightWay.size()+1;
// 	}
// 	else if (generalsListStatus == HaveBoth)
// 	{
// 		return GameView::getInstance()->generalsListForFightWay.size()+2;
// 	}
// 	else
// 	{
// 		return 0;
// 	}
}


void OnStrategiesGeneralsInfoUI::RefreshGeneralsList()
{
	this->generalList_tableView->reloadData();
}

void OnStrategiesGeneralsInfoUI::RefreshGeneralsListWithOutChangeOffSet()
{
	Vec2 _s = generalList_tableView->getContentOffset();
	int _h = generalList_tableView->getContentSize().height + _s.y;
	generalList_tableView->reloadData();
	Vec2 temp = Vec2(_s.x,_h - generalList_tableView->getContentSize().height);
	generalList_tableView->setContentOffset(temp); 
}

void OnStrategiesGeneralsInfoUI::RefreshDataSource()
{
	//delete old
	std::vector<CGeneralBaseMsg *>::iterator iter_generalslistforfightway;
	for (iter_generalslistforfightway=generalsListForFightWay.begin();iter_generalslistforfightway !=generalsListForFightWay.end();++iter_generalslistforfightway)
	{
		delete *iter_generalslistforfightway;
	}
	generalsListForFightWay.clear();
	//add new 
	for(int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	{
		if (curGeneralId == -1)
		{
			auto temp = new CGeneralBaseMsg();
			temp->CopyFrom(*GameView::getInstance()->generalsInLineList.at(i));
			generalsListForFightWay.push_back(temp);
		}
		else
		{
			if (curGeneralId != GameView::getInstance()->generalsInLineList.at(i)->id())
			{
				auto temp = new CGeneralBaseMsg();
				temp->CopyFrom(*GameView::getInstance()->generalsInLineList.at(i));
				generalsListForFightWay.push_back(temp);
			}
		}
	}
}
