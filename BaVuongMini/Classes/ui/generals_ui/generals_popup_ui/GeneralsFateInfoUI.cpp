#include "GeneralsFateInfoUI.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "../../../messageclient/element/CGeneralDetail.h"
#include "../GeneralsHeadItemBase.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../messageclient/element/CGeneralBaseMsg.h"
#include "../../../messageclient/element/CFateBaseMsg.h"
#include "AppMacros.h"
#include "../NormalGeneralsHeadItem.h"

GeneralsFateInfoUI::GeneralsFateInfoUI()
{
}


GeneralsFateInfoUI::~GeneralsFateInfoUI()
{
}

GeneralsFateInfoUI * GeneralsFateInfoUI::create(CGeneralDetail * generalsDetail)
{
	auto generalsFateInfoUI = new GeneralsFateInfoUI();
	if (generalsFateInfoUI && generalsFateInfoUI->init(generalsDetail))
	{
		generalsFateInfoUI->autorelease();
		return generalsFateInfoUI;
	}
	CC_SAFE_DELETE(generalsFateInfoUI);
	return NULL;
}

bool GeneralsFateInfoUI::init(CGeneralDetail * generalsDetail)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		if (generalsDetail->generalid() == 0)
			return true;

		//���UI
		if(LoadSceneLayer::GeneralsFateInfoLayer->getParent() != NULL)
		{
			LoadSceneLayer::GeneralsFateInfoLayer->removeFromParentAndCleanup(false);
		}
		ppanel = LoadSceneLayer::GeneralsFateInfoLayer;

		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(389, 420));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		auto btn_close = (ui::Button*)ui::Helper::seekWidgetByName(ppanel,"Button_close");
		btn_close->setTouchEnabled( true );
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(GeneralsFateInfoUI::CloseEvent, this));

		//ش���ݿ���佫���ܣ�ͷ�񣬰��������
		auto generalBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalsDetail->modelid()];

		l_fateName_1 = (Label *)ui::Helper::seekWidgetByName(ppanel,"Label_super_skill_1");
		l_fateName_1->setString("");
		l_fateName_2 = (Label *)ui::Helper::seekWidgetByName(ppanel,"Label_super_skill_2");
		l_fateName_2->setString("");
		l_fateName_3 = (Label *)ui::Helper::seekWidgetByName(ppanel,"Label_super_skill_3");
		l_fateName_3->setString("");
		l_fateName_4 = (Label *)ui::Helper::seekWidgetByName(ppanel,"Label_super_skill_4");
		l_fateName_4->setString("");
		l_fateName_5 = (Label *)ui::Helper::seekWidgetByName(ppanel,"Label_super_skill_5");
		l_fateName_5->setString("");

// 		t_fateDes_1 = ( ui::TextField *)Helper::seekWidgetByName(l_fateName_1,"TextArea_SkillContent");
// 		t_fateDes_1->setText("");
// 		t_fateDes_2 = ( ui::TextField *)Helper::seekWidgetByName(l_fateName_2,"TextArea_SkillContent");
// 		t_fateDes_2->setText("");
// 		t_fateDes_3 = ( ui::TextField *)Helper::seekWidgetByName(l_fateName_3,"TextArea_SkillContent");
// 		t_fateDes_3->setText("");
// 		t_fateDes_4 = ( ui::TextField *)Helper::seekWidgetByName(l_fateName_4,"TextArea_SkillContent");
// 		t_fateDes_4->setText("");
// 		t_fateDes_5 = ( ui::TextField *)Helper::seekWidgetByName(l_fateName_5,"TextArea_SkillContent");
// 		t_fateDes_5->setText("");
		l_fateDes_1 = Label::createWithTTF("",APP_FONT_NAME,15,Size(229,0),TextHAlignment::LEFT,TextVAlignment::TOP);
		l_fateDes_1->setAnchorPoint(Vec2(0,1.0f));
		l_fateDes_1->setPosition(Vec2(121,222));
		l_fateDes_1->setColor(Color3B(47,93,13));
		addChild(l_fateDes_1);
		l_fateDes_2 = Label::createWithTTF("",APP_FONT_NAME,15,Size(229,0),TextHAlignment::LEFT,TextVAlignment::TOP);
		l_fateDes_2->setAnchorPoint(Vec2(0,1.0f));
		l_fateDes_2->setPosition(Vec2(121,185));
		l_fateDes_2->setColor(Color3B(47,93,13));
		addChild(l_fateDes_2);
		l_fateDes_3 = Label::createWithTTF("",APP_FONT_NAME,15,Size(229,0),TextHAlignment::LEFT,TextVAlignment::TOP);
		l_fateDes_3->setAnchorPoint(Vec2(0,1.0f));
		l_fateDes_3->setPosition(Vec2(121,150));
		l_fateDes_3->setColor(Color3B(47,93,13));
		addChild(l_fateDes_3);
		l_fateDes_4 = Label::createWithTTF("",APP_FONT_NAME,15,Size(229,0),TextHAlignment::LEFT,TextVAlignment::TOP);
		l_fateDes_4->setAnchorPoint(Vec2(0,1.0f));
		l_fateDes_4->setPosition(Vec2(121,113));
		l_fateDes_4->setColor(Color3B(47,93,13));
		addChild(l_fateDes_4);
		l_fateDes_5 = Label::createWithTTF("",APP_FONT_NAME,15,Size(229,0),TextHAlignment::LEFT,TextVAlignment::TOP);
		l_fateDes_5->setAnchorPoint(Vec2(0,1.0f));
		l_fateDes_5->setPosition(Vec2(121,77));
		l_fateDes_5->setColor(Color3B(47,93,13));
		addChild(l_fateDes_5);

		RefreshFateInfo(generalsDetail);

		auto headItem = NormalGeneralsHeadItemBase::create(generalsDetail);
		headItem->setAnchorPoint(Vec2(0,0));
		headItem->setPosition(Vec2(30,245));
		addChild(headItem);

		auto l_description = Label::createWithTTF(generalBaseMsgFromDb->get_description().c_str(),APP_FONT_NAME,16,Size(195,0),TextHAlignment::LEFT);
		int height = l_description->getContentSize().height;
		auto shadowColor = Color4B::BLACK;   // black
		l_description->enableShadow(shadowColor,Size(1.0f, -1.0f), 1.0f);

		auto m_contentScrollView = cocos2d::extension::ScrollView::create(Size(198,120));
		m_contentScrollView->setViewSize(Size(198, 120));
		m_contentScrollView->setIgnoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(true);
		m_contentScrollView->setDirection(cocos2d::extension::ScrollView::Direction::VERTICAL);
		m_contentScrollView->setAnchorPoint(Vec2(0,0));
		m_contentScrollView->setPosition(Vec2(151,258));
		m_contentScrollView->setBounceable(true);
		m_contentScrollView->setClippingToBounds(true);
		m_pLayer->addChild(m_contentScrollView);
// 		UIScrollView * m_scrollView = cocos2d::extension::ScrollView::create();
// 		m_scrollView->setTouchEnabled(true);
// 		m_scrollView->setDirection(SCROLLVIEW_DIR_VERTICAL);
// 		m_scrollView->setContentSize(Size(204,125));
// 		m_scrollView->setPosition(Vec2(133,244));
// 		m_pLayer->addChild(m_scrollView);

		if (height > 120)
		{
			m_contentScrollView->setContentSize(Size(198,height));
			m_contentScrollView->setContentOffset(Vec2(0,120-height));
		}
		else
		{
			m_contentScrollView->setContentSize(Size(198,120));
		}

		m_contentScrollView->addChild(l_description);
		l_description->setIgnoreAnchorPointForPosition(false);
		l_description->setAnchorPoint(Vec2(0,1));
		l_description->setPosition(Vec2(4,m_contentScrollView->getContentSize().height));


		this->setContentSize(Size(389,420));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(GeneralsFateInfoUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(GeneralsFateInfoUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(GeneralsFateInfoUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(GeneralsFateInfoUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void GeneralsFateInfoUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GeneralsFateInfoUI::onExit()
{
	UIScene::onExit();
}

bool GeneralsFateInfoUI::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void GeneralsFateInfoUI::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void GeneralsFateInfoUI::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void GeneralsFateInfoUI::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void GeneralsFateInfoUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsFateInfoUI::RefreshFateInfo( CGeneralDetail * generalsDetail )
{
	if (generalsDetail->fates_size()>0)
	{
		for (int i = 0;i<generalsDetail->fates_size();++i)
		{
			auto temp = FateBaseMsgConfigData::s_fateBaseMsg[generalsDetail->fates(i).fateid()];
			if (temp == NULL)
				return;

			if (i == 0)
			{
				l_fateName_1->setString(generalsDetail->fates(i).fatename().c_str());
				l_fateDes_1->setString(temp->get_description().c_str());
			}
			else if(i == 1)
			{
				l_fateName_2->setString(generalsDetail->fates(i).fatename().c_str());
				l_fateDes_2->setString(temp->get_description().c_str());
			}
			else if(i == 2)
			{
				l_fateName_3->setString(generalsDetail->fates(i).fatename().c_str());
				l_fateDes_3->setString(temp->get_description().c_str());
			}
			else if(i == 3)
			{
				l_fateName_4->setString(generalsDetail->fates(i).fatename().c_str());
				l_fateDes_4->setString(temp->get_description().c_str());
			}
			else if(i == 4)
			{
				l_fateName_5->setString(generalsDetail->fates(i).fatename().c_str());
				l_fateDes_5->setString(temp->get_description().c_str());
			}
			else if(i == 5)
			{
			}
		}
	}
}

