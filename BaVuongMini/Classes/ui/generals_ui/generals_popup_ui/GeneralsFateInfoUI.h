
#ifndef _GENERALSPOPUPUI_GENERALSFATEINFO_H_
#define _GENERALSPOPUPUI_GENERALSFATEINFO_H_

#include "../../extensions/UIScene.h"
#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGeneralDetail;

class GeneralsFateInfoUI : public UIScene
{
public:
	GeneralsFateInfoUI();
	~GeneralsFateInfoUI();

	static GeneralsFateInfoUI * create(CGeneralDetail * generalsDetail);
	bool init(CGeneralDetail * generalsDetail);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void RefreshFateInfo(CGeneralDetail * generalsDetail);

private:
	ui::Layout *ppanel;

	Label * l_fateName_1;
	Label * l_fateName_2;
	Label * l_fateName_3;
	Label * l_fateName_4;
	Label * l_fateName_5;

	Label * l_fateDes_1;
	Label * l_fateDes_2;
	Label * l_fateDes_3;
	Label * l_fateDes_4;
	Label * l_fateDes_5;

private:
	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
};

#endif