#include "GeneralsSkillListUI.h"
#include "../../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../../loadscene_state/LoadSceneState.h"
#include "../../../gamescene_state/skill/GameFightSkill.h"
#include "../GeneralsSkillsUI.h"
#include "../GeneralsUI.h"
#include "../../../GameView.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../gamescene_state/MainScene.h"
#include "../../../gamescene_state/role/GameActor.h"
#include "../GeneralsListUI.h"
#include "../../../messageclient/element/CGeneralBaseMsg.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "AppMacros.h"
#include "../GeneralsShortcutSlot.h"
#include "../../../ui/skillscene/SkillScene.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../ui/extensions/ShowSystemInfo.h"
#include "../../../gamescene_state/role/MyPlayer.h"
#include "../../../messageclient/element/FolderInfo.h"

#define GENERALSSHORTCUTSLOT_TAG 785 
#define  SelectImageTag 458

GeneralsSkillListUI::GeneralsSkillListUI():
curSkillId(""),
curSlotIndex(-1)
{
}


GeneralsSkillListUI::~GeneralsSkillListUI()
{
	std::vector<GameFightSkill*>::iterator iter;
	for (iter = DataSourceList.begin(); iter != DataSourceList.end(); ++iter)
	{
		delete *iter;
	}
	DataSourceList.clear();
}

GeneralsSkillListUI* GeneralsSkillListUI::create(GeneralsSkillsUI::SkillTpye skill_type,int index)
{
	auto generalsSkillListUI = new GeneralsSkillListUI();
	if (generalsSkillListUI && generalsSkillListUI->init(skill_type,index))
	{
		generalsSkillListUI->autorelease();
		return generalsSkillListUI;
	}
	CC_SAFE_DELETE(generalsSkillListUI);
	return NULL;
}

bool GeneralsSkillListUI::init(GeneralsSkillsUI::SkillTpye skill_type,int index)
{
	if (UIScene::init())
	{
		Size winsize = Director::getInstance()->getVisibleSize();
		//this->scheduleUpdate();

		curSlotIndex = index;
		UpdateDataByType(skill_type,index);

		if (DataSourceList.size()<=0)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_skill_notStudied"));
			return false;
		}

		//���UI
		if(LoadSceneLayer::GeneralsSkillListLayer->getParent() != NULL)
		{
			LoadSceneLayer::GeneralsSkillListLayer->removeFromParentAndCleanup(false);
		}
		auto ppanel = LoadSceneLayer::GeneralsSkillListLayer;
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->getVirtualRenderer()->setContentSize(Size(703, 416));
		ppanel->setPosition(Vec2::ZERO);
		ppanel->setScale(1.0f);
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		Layer_skills = Layer::create();
		addChild(Layer_skills);

		auto Button_close = (Button*)Helper::seekWidgetByName(ppanel,"Button_close");
		Button_close->setTouchEnabled(true);
		Button_close->addTouchEventListener(CC_CALLBACK_2(GeneralsSkillListUI::CloseEvent, this));
		Button_close->setPressedActionEnabled(true);

		auto btn_equip = (Button *)Helper::seekWidgetByName(ppanel,"Button_set");
		btn_equip->setTouchEnabled(true);
		btn_equip->setPressedActionEnabled(true);
		btn_equip->addTouchEventListener(CC_CALLBACK_2(GeneralsSkillListUI::EquipEvent, this));

		auto btn_null_skillUpLevel = (Button *)Helper::seekWidgetByName(ppanel,"Button_null_skillEquip");
		btn_null_skillUpLevel->setTouchEnabled(true);
		btn_null_skillUpLevel->addTouchEventListener(CC_CALLBACK_2(GeneralsSkillListUI::NullSkillDesEvent, this));


		//i_skillFrame = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_skill_di"); 
		//i_skillImage = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_skill"); 
		l_skillName = (Text*)Helper::seekWidgetByName(ppanel,"Label_SkillName"); 
		l_skillType = (Text*)Helper::seekWidgetByName(ppanel,"Label_SkillType"); 
		l_skillLvValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_LvValue"); 
		imageView_skillQuality = (ImageView*)Helper::seekWidgetByName(ppanel,"ImageView_abc");
		imageView_skillQuality->setVisible(true);
		l_skillQualityValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_QualityValue");
		l_skillMpValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_MpValue"); 
		l_skillCDTimeValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_CDTimeValue"); 
		l_skillDistanceValue = (Text*)Helper::seekWidgetByName(ppanel,"Label_DistanceValue"); 
		//t_skillDescription = ( ui::TextField*)Helper::seekWidgetByName(ppanel,"TextArea_SkillRarity");
		//t_skillNextDescription = ( ui::TextField*)Helper::seekWidgetByName(ppanel,"TextArea_NextSkillRarity");

		generalsSkillsList_tableView = TableView::create(this,Size(230,356));
		//generalsSkillsList_tableView ->setSelectedEnable(true);   // �֧��ѡ��״̬����ʾ
		//generalsSkillsList_tableView ->setSelectedScale9Texture("res_ui/highlight.png", Rect(26, 26, 1, 1), Vec2(0,-1)); 
		//generalsSkillsList_tableView->setPressedActionEnabled(true);
		generalsSkillsList_tableView->setDirection(TableView::Direction::VERTICAL);
		generalsSkillsList_tableView->setAnchorPoint(Vec2(0,0));
		generalsSkillsList_tableView->setPosition(Vec2(30,29));
		generalsSkillsList_tableView->setDelegate(this);
		generalsSkillsList_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		Layer_skills->addChild(generalsSkillsList_tableView);

		Layer_upper = Layer::create();
		addChild(Layer_upper);

// 		ImageView * tableView_kuang = ImageView::create();
// 		tableView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		tableView_kuang->setScale9Enabled(true);
// 		tableView_kuang->setContentSize(Size(243,374));
// 		tableView_kuang->setCapInsets(Rect(30,30,1,1));
// 		tableView_kuang->setAnchorPoint(Vec2(0,0));
// 		tableView_kuang->setPosition(Vec2(19,21));
// 		Layer_upper->addChild(tableView_kuang);

		if (DataSourceList.size()>0)
		{
			curSkillId = DataSourceList.at(0)->getId();
			RefreshSkillInfo(DataSourceList.at(0));
		}

		this->setContentSize(Size(703,416));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(GeneralsSkillListUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(GeneralsSkillListUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(GeneralsSkillListUI::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(GeneralsSkillListUI::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void GeneralsSkillListUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GeneralsSkillListUI::onExit()
{
	UIScene::onExit();
}

bool GeneralsSkillListUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	return true;
}

void GeneralsSkillListUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void GeneralsSkillListUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void GeneralsSkillListUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void GeneralsSkillListUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void GeneralsSkillListUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void GeneralsSkillListUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

void GeneralsSkillListUI::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void GeneralsSkillListUI::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void GeneralsSkillListUI::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void GeneralsSkillListUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	int i = cell->getIdx();
	curSkillId = DataSourceList.at(i)->getId();
	RefreshSkillInfo(DataSourceList.at(i));
}

cocos2d::Size GeneralsSkillListUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(221,65);
}

cocos2d::extension::TableViewCell* GeneralsSkillListUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

		cell = new TableViewCell();
		cell->autorelease();

		auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
		spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
		spriteBg->setAnchorPoint(Vec2::ZERO);
		spriteBg->setPosition(Vec2(0, 0));
		//spriteBg->setColor(Color3B(0, 0, 0));
		spriteBg->setTag(SelectImageTag);
		spriteBg->setOpacity(0);
		cell->addChild(spriteBg);

		auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang01_new.png");
		sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
		sprite_bigFrame->setPosition(Vec2(0, 0));
		sprite_bigFrame->setContentSize(Size(220,63));
		sprite_bigFrame->setCapInsets(Rect(15,31,1,1));
		cell->addChild(sprite_bigFrame);

		//С�߿
// 		cocos2d::extension::Scale9Sprite *  smaillFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_allb.png");
// 		smaillFrame->setAnchorPoint(Vec2(0, 0));
// 		smaillFrame->setPosition(Vec2(18,5));
// 		smaillFrame->setCapInsets(Rect(15,15,1,1));
// 		smaillFrame->setContentSize(Size(52,52));
// 		cell->addChild(smaillFrame);
		auto  smaillFrame = Sprite::create("res_ui/jinengdi.png");
		smaillFrame->setAnchorPoint(Vec2(0, 0));
		smaillFrame->setPosition(Vec2(19,5));
		smaillFrame->setScale(0.91f);
		cell->addChild(smaillFrame);

		auto sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_diaa.png");
		sprite_nameFrame->setAnchorPoint(Vec2(0, 0));
		sprite_nameFrame->setPosition(Vec2(83, 17));
		sprite_nameFrame->setCapInsets(Rect(11,11,1,1));
		sprite_nameFrame->setContentSize(Size(130,30));
		sprite_bigFrame->addChild(sprite_nameFrame);

		std::string skillIconPath = "res_ui/jineng_icon/";

		if (DataSourceList.at(idx)->getId() != "")
		{
// 			if (strcmp(DataSourceList.at(idx)->getId().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 				||strcmp(DataSourceList.at(idx)->getId().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 				||strcmp(DataSourceList.at(idx)->getId().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 				||strcmp(DataSourceList.at(idx)->getId().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 			{
// 				skillIconPath.append("jineng_2");
// 			}
// 			else
// 			{
				skillIconPath.append(DataSourceList.at(idx)->getCBaseSkill()->icon());
			//}

			skillIconPath.append(".png");
		}
		else
		{
			skillIconPath = "gamescene_state/zhujiemian3/jinengqu/suibian.png";
		}

		auto  sprite_image = Sprite::create(skillIconPath.c_str());
		if(sprite_image != NULL)
		{
			sprite_image->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_image->setPosition(Vec2(45,31));
			cell->addChild(sprite_image);

			auto  sprite_lvFrame = Sprite::create("res_ui/jineng/jineng_zhezhao.png");
			sprite_lvFrame->setAnchorPoint(Vec2(0.5f,0.f));
			sprite_lvFrame->setPosition(Vec2(45/2+1,0));
			sprite_image->addChild(sprite_lvFrame);

			std::string str_lv = "";
			char s_curLv [5];
			sprintf(s_curLv,"%d",DataSourceList.at(idx)->getLevel());
			str_lv.append(s_curLv);
			str_lv.append("/");
			char max_lv [5];
			sprintf(max_lv,"%d",DataSourceList.at(idx)->getCBaseSkill()->maxlevel());
			str_lv.append(max_lv);

			auto l_lv = Label::createWithTTF(str_lv.c_str(),APP_FONT_NAME,12);
			l_lv->setAnchorPoint(Vec2(0.5f,0.f));
			l_lv->setPosition(Vec2(sprite_lvFrame->getPosition().x,sprite_lvFrame->getPosition().y));
			sprite_lvFrame->addChild(l_lv);
		}

		//skill variety icon
		auto sprite_skillVariety = SkillScene::GetSkillVarirtySprite(DataSourceList.at(idx)->getId().c_str());
		if (sprite_skillVariety)
		{
			sprite_skillVariety->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_skillVariety->setPosition(Vec2(60,29));
			sprite_skillVariety->setScale(.85f);
			cell->addChild(sprite_skillVariety);
		}

		auto label_skillName = Label::createWithTTF(DataSourceList.at(idx)->getCBaseSkill()->name().c_str(),APP_FONT_NAME,18);
		label_skillName->setAnchorPoint(Vec2(0, 0.5f));
		label_skillName->setPosition(Vec2(100,31));
// 		Color3B shadowColor = Color3B(0,0,0);   // black
// 		label_skillName->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
		cell->addChild(label_skillName);

		if (DataSourceList.at(idx)->getCBaseSkill()->get_quality() == 1)
		{
			label_skillName->setColor(Color3B(255,255,255));
		}
		else if (DataSourceList.at(idx)->getCBaseSkill()->get_quality()  == 2)
		{
			label_skillName->setColor(Color3B(0,255,0));
		}
		else if (DataSourceList.at(idx)->getCBaseSkill()->get_quality()  == 3)
		{
			label_skillName->setColor(Color3B(0,255,255));
		}
		else if (DataSourceList.at(idx)->getCBaseSkill()->get_quality()  == 4)
		{
			label_skillName->setColor(Color3B(255,0,228));
		}
		else if (DataSourceList.at(idx)->getCBaseSkill()->get_quality()  == 5)
		{
			label_skillName->setColor(Color3B(254,124,0));
		}
		else
		{
			label_skillName->setColor(Color3B(255,255,255));
		}
	
	return cell;
}

ssize_t GeneralsSkillListUI::numberOfCellsInTableView( TableView *table )
{
	return  DataSourceList.size();
}

void GeneralsSkillListUI::EquipEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto temp = new ReqData();
		temp->generalsid = GeneralsUI::generalsListUI->getCurGeneralBaseMsg()->id();
		temp->skillid = curSkillId;
		temp->index = curSlotIndex;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5053, temp);
		delete temp;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GeneralsSkillListUI::RefreshSkillInfo( GameFightSkill * gameFightSkill )
{
// 	if (Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG))
// 	{
// 		Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG)->removeFromParent();
// 	}
// 
// 	GeneralsShortcutSlot * generalsShortCutSlot = GeneralsShortcutSlot::create(gameFightSkill);
// 	generalsShortCutSlot->setIgnoreAnchorPointForPosition(false);
// 	generalsShortCutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
// 	generalsShortCutSlot->setPosition(Vec2(341,346));
// 	generalsShortCutSlot->setTouchEnabled(false);
// 	Layer_skills->addChild(generalsShortCutSlot,0,GENERALSSHORTCUTSLOT_TAG);
// 
// 	l_skillName->setText(gameFightSkill->getCBaseSkill()->name().c_str());
// 	char s_level[5];
// 	sprintf(s_level,"%d",gameFightSkill->getCFightSkill()->level());
// 	l_skillLvValue->setText(s_level);
// 	l_skillQualityValue->setText("");
// 	char s_mp[20];
// 	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
// 	l_skillMpValue->setText(s_mp);
// 	char s_intervaltime[5];
// 	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
// 	l_skillCDTimeValue->setText(s_intervaltime);
// 	char s_distance[5];
// 	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
// 	l_skillDistanceValue ->setText(s_distance);
// 	t_skillDescription->setText(gameFightSkill->getCFightSkill()->description().c_str());
// 	t_skillNextDescription->setText(gameFightSkill->getCFightSkill()->nextleveldescription().c_str());


	if (Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG))
	{
		Layer_skills->getChildByTag(GENERALSSHORTCUTSLOT_TAG)->removeFromParent();
	}

	auto generalsShortCutSlot = GeneralsShortcutSlot::create(gameFightSkill);
	generalsShortCutSlot->setIgnoreAnchorPointForPosition(false);
	generalsShortCutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
	generalsShortCutSlot->setPosition(Vec2(342,336));
	generalsShortCutSlot->setTouchEnabled(false);
	Layer_skills->addChild(generalsShortCutSlot,0,GENERALSSHORTCUTSLOT_TAG);

	l_skillName->setString(gameFightSkill->getCBaseSkill()->name().c_str());

	//��/����
	if (gameFightSkill->getCBaseSkill()->usemodel() == 0)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setString(skillinfo_zhudongjineng);
	}
	else if (gameFightSkill->getCBaseSkill()->usemodel() == 2)
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_beidongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setString(skillinfo_zhudongjineng);
	}
	else
	{
		const char *zhudongjineng = StringDataManager::getString("skillinfo_zhudongjineng");
		char* skillinfo_zhudongjineng =const_cast<char*>(zhudongjineng);
		l_skillType->setString(skillinfo_zhudongjineng);
	}

	char s_level[20];
	sprintf(s_level,"%d",gameFightSkill->getCFightSkill()->level());
	l_skillLvValue->setString(s_level);
	//l_skillQualityValue->setText("");
	switch(gameFightSkill->getCBaseSkill()->get_quality())
	{
	case 1:
		{
			imageView_skillQuality->loadTexture("res_ui/o_white.png");
		}
		break;
	case 2:
		{
			imageView_skillQuality->loadTexture("res_ui/o_green.png");
		}
		break;
	case 3:
		{
			imageView_skillQuality->loadTexture("res_ui/o_blue.png");
		}
		break;
	case 4:
		{
			imageView_skillQuality->loadTexture("res_ui/o_purple.png");
		}
		break;
	case 5:
		{
			imageView_skillQuality->loadTexture("res_ui/o_orange.png");
		}
		break;
	default:
		imageView_skillQuality->loadTexture("res_ui/o_white.png");
	}
	char s_mp[20];
	sprintf(s_mp,"%d",gameFightSkill->getCFightSkill()->mp());
	l_skillMpValue->setString(s_mp);
	char s_intervaltime[20];
	sprintf(s_intervaltime,"%d",gameFightSkill->getCFightSkill()->intervaltime()/1000);
	l_skillCDTimeValue->setString(s_intervaltime);
	char s_distance[20];
	sprintf(s_distance,"%d",gameFightSkill->getCFightSkill()->distance());
	l_skillDistanceValue ->setString(s_distance);
	//t_skillDescription->setText(gameFightSkill->getCFightSkill()->description().c_str());
	//t_skillNextDescription->setText(gameFightSkill->getCFightSkill()->get_next_description().c_str());
	this->showSkillDes(gameFightSkill->getCFightSkill()->description(),gameFightSkill->getCFightSkill()->get_next_description());

	if(Layer_skills->getChildByName("panel_levelInfo"))
	{
		Layer_skills->getChildByName("panel_levelInfo")->removeFromParent();
	}
	auto panel_levelInfo  = Layout::create();
	Layer_skills->addChild(panel_levelInfo);
	panel_levelInfo->setName("panel_levelInfo");

	/////////////////////////////////////////////////////
	int m_required_level = 0;
	int m_required_point = 0;
	std::string m_str_pre_skill = "";
	std::string m_pre_skill_id = "";
	int m_pre_skill_level = 0;
	int m_required_gold= 0;
	int m_required_skillBookValue = 0;
	if (gameFightSkill->getLevel() == 0)
	{
		//�ȼ�
		if (gameFightSkill->getCFightSkill()->get_required_level()>0)
		{
			m_required_level = gameFightSkill->getCFightSkill()->get_required_level();
		}
		//���ܵ
		if (gameFightSkill->getCFightSkill()->get_required_point()>0)
		{
			m_required_point = gameFightSkill->getCFightSkill()->get_required_point();
		}
		//XXX㼼�
		if (gameFightSkill->getCFightSkill()->get_pre_skill() != "")
		{
			auto temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_pre_skill()];
			m_str_pre_skill.append(temp->name().c_str());
			m_pre_skill_id.append(temp->id());

			m_pre_skill_level = gameFightSkill->getCFightSkill()->get_pre_skill_level();
		}
		//ܽ�
		if (gameFightSkill->getCFightSkill()->get_required_gold()>0)
		{
			m_required_gold = gameFightSkill->getCFightSkill()->get_required_gold();
		}
		//Ҽ����
		if (gameFightSkill->getCFightSkill()->get_required_num()>0)
		{
			m_required_skillBookValue = gameFightSkill->getCFightSkill()->get_required_num();
		}
	}
	else
	{
		//�ȼ�
		if (gameFightSkill->getCFightSkill()->get_next_required_level()>0)
		{
			m_required_level = gameFightSkill->getCFightSkill()->get_next_required_level();
		}
		//���ܵ
		if (gameFightSkill->getCFightSkill()->get_next_required_point()>0)
		{
			m_required_point = gameFightSkill->getCFightSkill()->get_next_required_point();
		}
		//XXX㼼�
		if (gameFightSkill->getCFightSkill()->get_next_pre_skill() != "")
		{
			auto temp = StaticDataBaseSkill::s_baseSkillData[gameFightSkill->getCFightSkill()->get_next_pre_skill()];
			m_str_pre_skill.append(temp->name());
			m_pre_skill_id.append(temp->id());

			m_pre_skill_level = gameFightSkill->getCFightSkill()->get_next_pre_skill_level();
		}
		//ܽ�
		if (gameFightSkill->getCFightSkill()->get_next_required_gold()>0)
		{
			m_required_gold = gameFightSkill->getCFightSkill()->get_next_required_gold();
		}
		//ҽ����
		if (gameFightSkill->getCFightSkill()->get_next_required_num()>0)
		{
			m_required_skillBookValue = gameFightSkill->getCFightSkill()->get_next_required_num();
		}
	}

	/////////////////////////////////////////////////////
	int lineNum = 0;
	//���ǵȼ�
	if (m_required_level>0)
	{
		lineNum++;
		auto l_playerLv = Label::createWithTTF(StringDataManager::getString("skillinfo_zhujuedengji"), APP_FONT_NAME, 16);
		l_playerLv->setColor(Color3B(47,93,13));
		l_playerLv->setAnchorPoint(Vec2(0,0));
		l_playerLv->setPosition(Vec2(283,102-lineNum*20));//(330,118)
		l_playerLv->setName("l_playerLv");
		panel_levelInfo->addChild(l_playerLv);

		auto l_playerLv_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_playerLv_colon->setColor(Color3B(47,93,13));
		l_playerLv_colon->setAnchorPoint(Vec2(0,0));
		l_playerLv_colon->setPosition(Vec2(347,102-lineNum*20));
		l_playerLv_colon->setName("l_playerLv_colon");
		panel_levelInfo->addChild(l_playerLv_colon);

		char s_next_required_level[20];
		sprintf(s_next_required_level,"%d",m_required_level);
		auto l_playerLvValue = Label::createWithTTF(s_next_required_level, APP_FONT_NAME, 16);
		l_playerLvValue->setColor(Color3B(35,93,13));
		l_playerLvValue->setAnchorPoint(Vec2(0,0));
		l_playerLvValue->setPosition(Vec2(356,102-lineNum*20));
		panel_levelInfo->addChild(l_playerLvValue);
		// 
		if (GameView::getInstance()->myplayer->getActiveRole()->level()<m_required_level)  //����ȼ�δ�ﵽ������Ҫ�
		{
			l_playerLv->setColor(Color3B(212,59,59));
			l_playerLv_colon->setColor(Color3B(212,59,59));
			l_playerLvValue->setColor(Color3B(212,59,59));
		}
	}
	//��ܵ
	if (m_required_point>0)
	{
		lineNum++;

		auto widget_skillPoint = Widget::create();
		panel_levelInfo->addChild(widget_skillPoint);
		widget_skillPoint->setName("widget_skillPoint");
		widget_skillPoint->setAnchorPoint(Vec2(0,0));
		widget_skillPoint->setPosition(Vec2(283,102-lineNum*20));

		std::string str_skillPoint = StringDataManager::getString("skillinfo_jinengdian");
		for(int i = 0;i<3;i++)
		{
			std::string str_name = "l_skillPoint";
			char s_index[5];
			sprintf(s_index,"%d",i);
			str_name.append(s_index);
			auto l_skillPoint = Label::createWithTTF(str_skillPoint.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
			l_skillPoint->setColor(Color3B(47,93,13));
			l_skillPoint->setAnchorPoint(Vec2(0,0));
			l_skillPoint->setName(str_name.c_str());
			widget_skillPoint->addChild(l_skillPoint);
			l_skillPoint->setPosition(Vec2(24*i,0));
		}

		auto l_skillPoint_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_skillPoint_colon->setColor(Color3B(47,93,13));
		l_skillPoint_colon->setAnchorPoint(Vec2(0,0));
		l_skillPoint_colon->setPosition(Vec2(347,102-lineNum*20));
		l_skillPoint_colon->setName("l_skillPoint_colon");
		panel_levelInfo->addChild(l_skillPoint_colon);

		char s_required_point[20];
		sprintf(s_required_point,"%d",m_required_point);
		auto l_skillPointValue = Label::createWithTTF(s_required_point, APP_FONT_NAME, 16);
		l_skillPointValue->setColor(Color3B(47,93,13));
		l_skillPointValue->setAnchorPoint(Vec2(0,0));
		l_skillPointValue->setPosition(Vec2(356,102-lineNum*20));
		panel_levelInfo->addChild(l_skillPointValue);

		if(GameView::getInstance()->myplayer->player->skillpoint()<m_required_point)//㼼�ܵ㲻�
		{
			for(int i = 0;i<3;i++)
			{
				std::string str_name = "l_skillPoint";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				auto l_skillPoint = (Label*)widget_skillPoint->getChildByName(str_name.c_str());
				if (l_skillPoint)
				{
					l_skillPoint->setColor(Color3B(212,59,59));
				}
			}
			l_skillPoint_colon->setColor(Color3B(212,59,59));
			l_skillPointValue->setColor(Color3B(212,59,59));
		}
	}
	//XXX㼼�
	if (m_str_pre_skill != "")
	{
		lineNum++;
		auto widget_pre_skill = Widget::create();
		panel_levelInfo->addChild(widget_pre_skill);
		widget_pre_skill->setName("widget_pre_skill");
		widget_pre_skill->setAnchorPoint(Vec2(0,0));
		widget_pre_skill->setPosition(Vec2(283,102-lineNum*20));
		if (m_str_pre_skill.size()/3 < 4)
		{
			for(int i = 0;i<m_str_pre_skill.size()/3;i++)
			{
				std::string str_name = "l_preSkill";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);

				auto l_preSkill = Label::createWithTTF(m_str_pre_skill.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
				l_preSkill->setColor(Color3B(47,93,13));
				l_preSkill->setAnchorPoint(Vec2(0,0));
				widget_pre_skill->addChild(l_preSkill);
				l_preSkill->setName(str_name.c_str());
				if (m_str_pre_skill.size()/3 == 1)
				{
					l_preSkill->setPosition(Vec2(0,0));
				}
				else if (m_str_pre_skill.size()/3 == 2)
				{
					l_preSkill->setPosition(Vec2(48*i,0));
				}
				else if (m_str_pre_skill.size()/3 == 3)
				{
					l_preSkill->setPosition(Vec2(24*i,0));
				}
			}
		}
		else
		{
			auto l_preSkill = Label::createWithTTF(m_str_pre_skill.c_str(), APP_FONT_NAME, 16);
			l_preSkill->setColor(Color3B(47,93,13));
			l_preSkill->setAnchorPoint(Vec2(0,0));
			l_preSkill->setPosition(Vec2(0,0));
			widget_pre_skill->addChild(l_preSkill);
			l_preSkill->setName("l_preSkill");
		}

		auto l_preSkill_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_preSkill_colon->setColor(Color3B(47,93,13));
		l_preSkill_colon->setAnchorPoint(Vec2(0,0));
		l_preSkill_colon->setPosition(Vec2(347,102-lineNum*20));
		l_preSkill_colon->setName("l_preSkill_colon");
		panel_levelInfo->addChild(l_preSkill_colon);

		char s_pre_skill_level[20];
		sprintf(s_pre_skill_level,"%d",m_pre_skill_level);
		auto l_preSkillValue = Label::createWithTTF(s_pre_skill_level, APP_FONT_NAME, 16);
		l_preSkillValue->setColor(Color3B(47,93,13));
		l_preSkillValue->setAnchorPoint(Vec2(0,0));
		l_preSkillValue->setPosition(Vec2(356,102-lineNum*20));
		l_preSkillValue->setName("l_preSkillValue");
		panel_levelInfo->addChild(l_preSkillValue);

		std::map<std::string,GameFightSkill*>::const_iterator cIter;
		cIter = GameView::getInstance()->GameFightSkillList.find(m_pre_skill_id);
		if (cIter == GameView::getInstance()->GameFightSkillList.end()) // �û�ҵ�����ָ�END��  
		{
		}
		else
		{
			auto gameFightSkillStuded = GameView::getInstance()->GameFightSkillList[m_pre_skill_id];
		 	if (gameFightSkillStuded)
		 	{
		 		if (gameFightSkillStuded->getLevel() < m_pre_skill_level)
		 		{
					if (m_str_pre_skill.size()/3 < 4)
					{
						for(int i = 0;i<m_str_pre_skill.size()/3;i++)
						{
							std::string str_name = "l_preSkill";
							char s_index[5];
							sprintf(s_index,"%d",i);
							str_name.append(s_index);

							auto l_preSkill = (Label*)widget_pre_skill->getChildByName(str_name.c_str());
							if (l_preSkill)
							{
								l_preSkill->setColor(Color3B(212,59,59));
							}
						}
					}
					else
					{
						auto l_preSkill = (Label*)widget_pre_skill->getChildByName("l_preSkill");
						if (l_preSkill)
						{
							l_preSkill->setColor(Color3B(212,59,59));
						}
					}
					l_preSkill_colon->setColor(Color3B(212,59,59));
		 			l_preSkillValue->setColor(Color3B(212,59,59));
		 		}
		 	}
		}
	}
	//˽�
	if (m_required_gold>0)
	{
		lineNum++;

		auto widget_gold = Widget::create();
		panel_levelInfo->addChild(widget_gold);
		widget_gold->setPosition(Vec2(283,102-lineNum*20));

		std::string str_gold = StringDataManager::getString("skillinfo_jinbi");
		for(int i = 0;i<2;i++)
		{
			std::string str_name = "l_gold";
			char s_index[5];
			sprintf(s_index,"%d",i);
			str_name.append(s_index);
			auto l_gold = Label::createWithTTF(str_gold.substr(i * 3, 3).c_str(), APP_FONT_NAME, 16);
			l_gold->setColor(Color3B(47,93,13));
			l_gold->setAnchorPoint(Vec2(0,0));
			l_gold->setName(str_name.c_str());
			widget_gold->addChild(l_gold);
			l_gold->setPosition(Vec2(48*i,0));
		}

		auto l_gold_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_gold_colon->setColor(Color3B(47,93,13));
		l_gold_colon->setAnchorPoint(Vec2(0,0));
		l_gold_colon->setPosition(Vec2(347,102-lineNum*20));
		l_gold_colon->setName("l_gold_colon");
		panel_levelInfo->addChild(l_gold_colon);

		char s_required_gold[20];
		sprintf(s_required_gold,"%d",m_required_gold);
		auto l_goldValue = Label::createWithTTF(s_required_gold, APP_FONT_NAME, 16);
		l_goldValue->setColor(Color3B(47,93,13));
		l_goldValue->setAnchorPoint(Vec2(0,0));
		l_goldValue->setPosition(Vec2(356,102-lineNum*20));
		panel_levelInfo->addChild(l_goldValue);

		if(GameView::getInstance()->getPlayerGold()<m_required_gold)//ҽ�Ҳ��
		{
			for(int i = 0;i<2;i++)
			{
				std::string str_name = "l_gold";
				char s_index[5];
				sprintf(s_index,"%d",i);
				str_name.append(s_index);
				auto l_gold = (Label*)widget_gold->getChildByName(str_name.c_str());
				if (l_gold)
				{
					l_gold->setColor(Color3B(212,59,59));
				}
			}
			l_gold_colon->setColor(Color3B(212,59,59));
			l_goldValue->setColor(Color3B(212,59,59));
		}
	}
	//㼼���
	if (m_required_skillBookValue>0)
	{
		lineNum++;

		auto widget_skillBook = Widget::create();
		panel_levelInfo->addChild(widget_skillBook);
		widget_skillBook->setName("widget_skillBook");
		widget_skillBook->setPosition(Vec2(283,102-lineNum*20));

		std::string str_skillbook = GeneralsSkillsUI::getSkillQualityStr(gameFightSkill->getCBaseSkill()->get_quality());
		str_skillbook.append(StringDataManager::getString("skillinfo_jinengshu"));
		auto l_skillbook = Label::createWithTTF(str_skillbook.c_str(), APP_FONT_NAME, 16);
		l_skillbook->setColor(Color3B(47,93,13));
		l_skillbook->setAnchorPoint(Vec2(0,0));
		l_skillbook->setName("l_skillbook");
		widget_skillBook->addChild(l_skillbook);
		l_skillbook->setPosition(Vec2(0,0));

		auto l_skillbook_colon = Label::createWithTTF(StringDataManager::getString("skillinfo_maohao"), APP_FONT_NAME, 16);
		l_skillbook_colon->setColor(Color3B(47,93,13));
		l_skillbook_colon->setAnchorPoint(Vec2(0,0));
		l_skillbook_colon->setPosition(Vec2(347,102-lineNum*20));
		l_skillbook_colon->setName("l_skillbook_colon");
		panel_levelInfo->addChild(l_skillbook_colon);

		char s_required_skillBook[20];
		sprintf(s_required_skillBook,"%d",m_required_skillBookValue);
		auto l_skillBookValue = Label::createWithTTF(s_required_skillBook, APP_FONT_NAME, 16);
		l_skillBookValue->setColor(Color3B(47,93,13));
		l_skillBookValue->setAnchorPoint(Vec2(0,0));
		l_skillBookValue->setPosition(Vec2(356,102-lineNum*20));
		panel_levelInfo->addChild(l_skillBookValue);

		int propNum = 0;
		for (int i = 0;i<GameView::getInstance()->AllPacItem.size();++i)
		{
			auto folder = GameView::getInstance()->AllPacItem.at(i);
			if (!folder->has_goods())
				continue;

			if (folder->goods().id() == gameFightSkill->getCFightSkill()->get_required_prop())
			{
				propNum += folder->quantity();
			}
		}
		if(propNum<gameFightSkill->getCFightSkill()->get_required_num())//鼼���鲻�
		{
			l_skillbook->setColor(Color3B(212,59,59));
			l_skillbook_colon->setColor(Color3B(212,59,59));
			l_skillBookValue->setColor(Color3B(212,59,59));
		}
	}
}

void GeneralsSkillListUI::UpdateDataByType( GeneralsSkillsUI::SkillTpye s_t,int index )
{
	std::vector<GameFightSkill*>::iterator iter;
	for (iter = DataSourceList.begin(); iter != DataSourceList.end(); ++iter)
	{
		delete *iter;
	}
	DataSourceList.clear();

	auto generalsui = (GeneralsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI);

	if (s_t == GeneralsSkillsUI::Warrior)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			auto  gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(),it->second->getId(),it->second->getLevel(), GameActor::type_player);

			if (gameFightSkill->getCBaseSkill()->get_profession() == 1)//��ͽ�
			{
				if (index == 0)  
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 0)//����
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
				else
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//ܱ������
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
			}
		}
	}
	else if(s_t == GeneralsSkillsUI::Magic)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			auto gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(),it->second->getLevel(), GameActor::type_player);

			if (gameFightSkill->getCBaseSkill()->get_profession() == 2)//ܹ�ı
			{
				if (index == 0)  
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 0)//����
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
				else
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//ܱ������
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
			}
		}
	}
	else if (s_t == GeneralsSkillsUI::Ranger)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			auto gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(),it->second->getLevel(), GameActor::type_player);

			if (gameFightSkill->getCBaseSkill()->get_profession() == 3)//ܺ�
			{
				if (index == 0)  
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 0)//�����
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
				else
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//ܱ������
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
			}
		}
	}
	else if(s_t == GeneralsSkillsUI::Warlock)
	{
		std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GeneralsGameFightSkillList.begin();
		for ( ; it != GameView::getInstance()->GeneralsGameFightSkillList.end(); ++ it )
		{
			auto gameFightSkill = new GameFightSkill();
			gameFightSkill->initSkill(it->second->getId(), it->second->getId(),it->second->getLevel(), GameActor::type_player);

			if (gameFightSkill->getCBaseSkill()->get_profession() == 4)//����
			{
				if (index == 0)  
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 0)//�����
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
				else
				{
					if(gameFightSkill->getCBaseSkill()->usemodel() == 2)//ܱ������
					{
						DataSourceList.push_back(gameFightSkill);
					}
				}
			}
		}
	}
}

void GeneralsSkillListUI::showSkillDes( std::string curSkillDes,std::string nextSkillDes )
{
	//skill des text
	if (m_pLayer->getChildByTag(123456))
	{
		auto scroll_ = (cocos2d::extension::ScrollView *)m_pLayer->getChildByTag(123456);
		scroll_->removeFromParentAndCleanup(true);
	}

	auto scrollView_info = ui::ScrollView::create();
	scrollView_info->setTouchEnabled(true);
	scrollView_info->setDirection(ui::ScrollView::Direction::VERTICAL);
	scrollView_info->setContentSize(Size(264,303));
	scrollView_info->setPosition(Vec2(425,75));
	scrollView_info->setBounceEnabled(true);
	scrollView_info->setTag(123456);
	m_pLayer->addChild(scrollView_info);

	auto color_ = Color3B(47,93,13);

	auto mo_1 = ImageView::create();
	mo_1->loadTexture("res_ui/mo_2.png");
	mo_1->setAnchorPoint(Vec2(0,0));
	mo_1->setScale(0.7f);
	scrollView_info->addChild(mo_1);

	auto labelBM_1 = Label::createWithBMFont("res_ui/font/ziti_3.fnt", StringDataManager::getString("robotui_skill_des_setSkill") );
	labelBM_1->setAnchorPoint(Vec2(0,0));
	scrollView_info->addChild(labelBM_1);

	auto label_des= Text::create(curSkillDes.c_str(), APP_FONT_NAME, 16);
	label_des->setAnchorPoint(Vec2(0,0));
	label_des->setColor(color_);
	label_des->setTextAreaSize(Size(240,0));
	scrollView_info->addChild(label_des);

	auto mo_2 = ImageView::create();
	mo_2->loadTexture("res_ui/mo_2.png");
	mo_2->setAnchorPoint(Vec2(0,0));
	mo_2->setScale(0.7f);
	scrollView_info->addChild(mo_2);

	auto labelBM_2 = Label::createWithBMFont("res_ui/font/ziti_3.fnt",StringDataManager::getString("robotui_nextSkill_des_setSkill"));
	labelBM_2->setAnchorPoint(Vec2(0,0));
	scrollView_info->addChild(labelBM_2);

	auto label_next_des=Text::create(nextSkillDes.c_str(), APP_FONT_NAME, 16);
	label_next_des->setAnchorPoint(Vec2(0,0));
	label_next_des->setColor(color_);
	label_next_des->setTextAreaSize(Size(240,0));
	scrollView_info->addChild(label_next_des);

	int h_ = mo_1->getContentSize().height * 2;
	int temp_min_h = 100;

	if (label_des->getContentSize().height > temp_min_h)
	{
		h_+= label_des->getContentSize().height;
	}else
	{
		h_+= temp_min_h;
	}

	if (label_next_des->getContentSize().height > temp_min_h)
	{
		h_+= label_next_des->getContentSize().height;
	}else
	{
		h_+= temp_min_h;
	}


	int innerWidth = scrollView_info->getBoundingBox().size.width;
	int innerHeight = h_;
	if (h_ < scrollView_info->getBoundingBox().size.height)
	{
		innerHeight = scrollView_info->getBoundingBox().size.height;
	}

	scrollView_info->setInnerContainerSize(Size(innerWidth,innerHeight));

	mo_1->setPosition(Vec2(0,innerHeight - mo_1->getContentSize().height ));
	labelBM_1->setPosition(Vec2(10,mo_1->getPosition().y+2));
	label_des->setPosition(Vec2(0,mo_1->getPosition().y - label_des->getContentSize().height));

	mo_2->setPosition(Vec2(0,label_des->getPosition().y - 30));
	labelBM_2->setPosition(Vec2(10,mo_2->getPosition().y+2));
	label_next_des->setPosition(Vec2(0,mo_2->getPosition().y - label_next_des->getContentSize().height));
}

void GeneralsSkillListUI::NullSkillDesEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		std::map<int, std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(19);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end())
		{
			return;
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			{
				Size winSize = Director::getInstance()->getVisibleSize();
				auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[19].c_str());
				GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				showSystemInfo->setIgnoreAnchorPointForPosition(false);
				showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				showSystemInfo->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}
