#ifndef _GENETALSPOPUPUI_GENERALSSKILLUI_H_
#define _GENETALSPOPUPUI_GENERALSSKILLUI_H_

#include "../../extensions/UIScene.h"
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../GeneralsSkillsUI.h"
#include "../../../legend_script/CCTutorialIndicator.h"

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class GameFightSkill;

class GeneralsSkillInfoUI : public UIScene
{
public:
	GeneralsSkillInfoUI();
	~GeneralsSkillInfoUI();

	static GeneralsSkillInfoUI * create(GameFightSkill * gameFightSkill,int slotIndex);
	bool init(GameFightSkill * gameFightSkill,int slotIndex);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void setSkillProfession(GeneralsSkillsUI::SkillTpye skill_prof);
	GeneralsSkillsUI::SkillTpye getSkillProfession();

	void RefreshSkillInfo(GameFightSkill * gameFightSkill);

private:
	std::string getProfessionBySkill(std::string skillid);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void SuperSkillEvent(Ref *pSender, Widget::TouchEventType type);
	void LvUpEvent(Ref *pSender, Widget::TouchEventType type);
	void ReplaceEvent(Ref *pSender, Widget::TouchEventType type);

public:
	std::string curSkillId;
	int skillSlotIndex;
	//��˫��ť
	Button * btn_superSkill;

private:
	Layout * ppanel;
	ui::ScrollView * m_scrollView;

	//��ǰ�������ڵ��佫ְҵ�������
	GeneralsSkillsUI::SkillTpye skill_profession;
	
	//���һ�����
	Label * l_nextLv;
	//���һ��
	Label * t_nextLvDes;
	//����Ҫ
	Sprite * s_upgradeNeed;
	Label * l_upgradeNeed;
	Sprite * s_second_line;
	//����ȼ�
	Label * l_playerLv;
	Label * l_playerLvValue;
	//���ܵ
	Label * l_skillPoint;
	Label * l_skillPointValue;
	//�ǰ�ü��
	Label * l_preSkill;
	Label * l_preSkillValue;
	//ܽ�
	Label * l_gold;
	Label * l_goldValue;
	//ҵ�
	Label * l_prop;
	Label * l_propValue;

public:
	//߽�ѧ
	virtual void registerScriptCommand(int scriptId);
	//��갴ť
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();
	//��ѧ
	int mTutorialScriptInstanceId;
};

#endif
