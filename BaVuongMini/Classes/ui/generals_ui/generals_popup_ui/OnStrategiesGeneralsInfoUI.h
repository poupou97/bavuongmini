
#ifndef _GENERALSPOPUPUI_ONSTRATEGIESGENERALSINFOUI_H_
#define _GENERALSPOPUPUI_ONSTRATEGIESGENERALSINFOUI_H_

#include "../../extensions/UIScene.h"
#include "../GeneralsListBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/////////////////////////////////
/**
 * �滻����ʱ���佫�б��
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.16
 */
class CGeneralBaseMsg;

class OnStrategiesGeneralsInfoUI : public UIScene,public TableViewDelegate,public TableViewDataSource
{
public:
	OnStrategiesGeneralsInfoUI();
	~OnStrategiesGeneralsInfoUI();

	static OnStrategiesGeneralsInfoUI * create(int positionIndex);
	bool init(int positionIndex);
	
	static OnStrategiesGeneralsInfoUI * create(int positionIndex,long long generalsID);
	bool init(int positionIndex,long long generalsID);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//ദ������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	void RelaxEvent(Ref * pSender);

	//�ˢ���б���
	void RefreshDataSource();
	//�ˢ���佫�б
	void RefreshGeneralsList();
	//�ˢ���佫�б(tableView ��ƫ������)
	void RefreshGeneralsListWithOutChangeOffSet();

public:
	// ��󷨵�������б���Ϣ
	std::vector<CGeneralBaseMsg * > generalsListForFightWay;

	int selectItemIndex ;
	long long curGeneralId;
	//��λ���Ƿ����佫
	bool isHaveGeneral;
	//ѡ�е��佫ID
	long long selectGeneralId;

	TableView * generalList_tableView;

	Button * Button_relax;
};

#endif
