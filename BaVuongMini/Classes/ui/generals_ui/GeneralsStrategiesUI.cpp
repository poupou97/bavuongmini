#include "GeneralsStrategiesUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GeneralsUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "GameView.h"
#include "NormalGeneralsHeadItem.h"
#include "../../messageclient/element/CFightWayBase.h"
#include "StrategiesPositionItem.h"
#include "generals_popup_ui/StrategiesUpgradeUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/element/CFightWayGrowingUp.h"
#include "GeneralsListBase.h"
#include "AppMacros.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../extensions/ShowSystemInfo.h"
#include "../../legend_script/CCTeachingGuide.h"

#define PositionItemBaseTag 55
#define M_SCROLLVIEWTAG 120
#define GENERALSSTRATRGIESUI_TABALEVIEW_HIGHLIGHT_TAG 456

//����������ȼ�Ϊ����1���������󷨣���2����ѧ���󷨣���3���ȼ��ߵ��󷨣���4�����������š��ţ�
bool SortFightWay(CFightWayBase * fightWay1 , CFightWayBase *fightWay2)
{
	if (fightWay1->activeflag() == 1)      //�������
	{
		return true;
	}
	else if (fightWay2->activeflag() == 1)      //��������
	{
		return false;
	}
	else
	{
		if (fightWay1->level() == fightWay2->level())
		{
			return (fightWay1->get_sortingOrder()> fightWay2->get_sortingOrder());
		}
		else
		{
			return (fightWay1->level()> fightWay2->level());
		}
	}
	return false;
}

GeneralsStrategiesUI::GeneralsStrategiesUI():
curSelectFightWayId(-1),
activeFightWayId(-1),
selectCellIndex(0),
curFightWayStatus(NotStuded),
lastSelectCellId(0)
{
	slotPos[0].x = 347;
	slotPos[0].y = 279;
	slotPos[1].x = 481;
	slotPos[1].y = 279;
	slotPos[2].x = 347;
	slotPos[2].y = 105;
	slotPos[3].x = 481;
	slotPos[3].y = 105;
	slotPos[4].x = 616;
	slotPos[4].y = 105;
}

GeneralsStrategiesUI::~GeneralsStrategiesUI()
{
	std::vector<CFightWayBase*>::iterator iter_generalStrategies;
	for (iter_generalStrategies = generalsStrategiesList.begin(); iter_generalStrategies != generalsStrategiesList.end(); ++iter_generalStrategies)
	{
		delete *iter_generalStrategies;
	}
	generalsStrategiesList.clear();

}
GeneralsStrategiesUI* GeneralsStrategiesUI::create()
{
	auto generalsStrategiesUI = new GeneralsStrategiesUI();
	if (generalsStrategiesUI && generalsStrategiesUI->init())
	{
		generalsStrategiesUI->autorelease();
		return generalsStrategiesUI;
	}
	CC_SAFE_DELETE(generalsStrategiesUI);
	return NULL;
}

bool GeneralsStrategiesUI::init()
{
	if (UIScene::init())
	{
		setStrategiesListToDefault();

		sort(generalsStrategiesList.begin(),generalsStrategiesList.end(),SortFightWay);

		//��佫����
		panel_strategies = (Layout*)Helper::seekWidgetByName(GeneralsUI::ppanel,"Panel_array");
		panel_strategies->setVisible(true);
		//��佫�󷨲
		Layer_strategies = Layer::create();
		addChild(Layer_strategies);
		//�һ����Ϣ
		auto btn_reset = (Button*)Helper::seekWidgetByName(panel_strategies,"Button_reset");
		btn_reset->setTouchEnabled(true);
		btn_reset->setPressedActionEnabled(true);
		btn_reset->addTouchEventListener(CC_CALLBACK_2(GeneralsStrategiesUI::StrategiesResetEvent, this));
		btn_reset->setVisible(false);
		//���
		btn_levelUpStrategies = (Button*)Helper::seekWidgetByName(panel_strategies,"Button_upgrade");   
		btn_levelUpStrategies->setTouchEnabled(true);
		btn_levelUpStrategies->setPressedActionEnabled(true);
		btn_levelUpStrategies->addTouchEventListener(CC_CALLBACK_2(GeneralsStrategiesUI::StrategiesLeveUpEvent, this));

		l_upgrade = (Text*)Helper::seekWidgetByName(btn_levelUpStrategies,"Label_upgrade");   

		//������
		btn_openStrategies = (Button*)Helper::seekWidgetByName(panel_strategies,"Button_use");  
		btn_openStrategies->setTouchEnabled(true);
		btn_openStrategies->setPressedActionEnabled(true);
		btn_openStrategies->addTouchEventListener(CC_CALLBACK_2(GeneralsStrategiesUI::StrategiesUseEvent, this));
		//󷨹ر��
		btn_closeStrategies = (Button*)Helper::seekWidgetByName(panel_strategies,"Button_notUse");  
		btn_closeStrategies->setTouchEnabled(true);
		btn_closeStrategies->setPressedActionEnabled(true);
		btn_closeStrategies->addTouchEventListener(CC_CALLBACK_2(GeneralsStrategiesUI::StrategiesNotUseEvent, this));

		auto btn_detail = (Button*)Helper::seekWidgetByName(panel_strategies,"Button_detail");
		btn_detail->setTouchEnabled(true);
		btn_detail->setPressedActionEnabled(true);
		btn_detail->addTouchEventListener(CC_CALLBACK_2(GeneralsStrategiesUI::StrategiesDetailEvent, this));

		l_strategieName = (Text*)Helper::seekWidgetByName(panel_strategies,"Label_name");  
		l_addPropDes = (Text*)Helper::seekWidgetByName(panel_strategies,"Label_buff");  

		//����б
		strategiesList_tableView = TableView::create(this,Size(261,401));
		strategiesList_tableView->setDirection(TableView::Direction::VERTICAL);
		strategiesList_tableView->setAnchorPoint(Vec2(0,0));
		strategiesList_tableView->setPosition(Vec2(74,41));
		strategiesList_tableView->setDelegate(this);
		strategiesList_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		Layer_strategies->addChild(strategiesList_tableView);

		Layer_upper = Layer::create();
		addChild(Layer_upper);

// 		ImageView * tableView_kuang = ImageView::create();
// 		tableView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		tableView_kuang->setScale9Enabled(true);
// 		tableView_kuang->setContentSize(Size(265,417));
// 		tableView_kuang->setCapInsets(Rect(30,30,1,1));
// 		tableView_kuang->setAnchorPoint(Vec2(0,0));
// 		tableView_kuang->setPosition(Vec2(63,33));
// 		Layer_upper->addChild(tableView_kuang);

		//�����Ĭ��ֵ
		RefreshListToDefault();

		return true;
	}
	return false;
}

void GeneralsStrategiesUI::StrategiesResetEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		for (int i = 0; i<GameView::getInstance()->generalsInLineList.size(); ++i)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5056, (void *)GameView::getInstance()->generalsInLineList.at(i)->id(), (void *)0);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsStrategiesUI::StrategiesLeveUpEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (curSelectFightWayId > 0)
		{
			//GameMessageProcessor::sharedMsgProcessor()->sendReq(5074,(void *)curSelectFightWayId);
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesUpGradeUI) == NULL)
			{
				Size winSize = Director::getInstance()->getVisibleSize();
				auto strategiesUpgradeUI = StrategiesUpgradeUI::create(curSelectFightWayId);
				strategiesUpgradeUI->setIgnoreAnchorPointForPosition(false);
				strategiesUpgradeUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				strategiesUpgradeUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				GameView::getInstance()->getMainUIScene()->addChild(strategiesUpgradeUI, 0, kTagStrategiesUpGradeUI);

				auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
				if (sc != NULL)
					sc->endCommand(pSender);
			}
		}
		else
		{
			//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *str1  = ((__String*)strings->objectForKey("generals_strategies_pleaseSelectStrategies"))->m_sString.c_str();
			const char *str1 = StringDataManager::getString("generals_strategies_pleaseSelectStrategies");
			char* p1 = const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(p1);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsStrategiesUI::StrategiesUseEvent(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (curSelectFightWayId > 0)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5072, (void *)curSelectFightWayId, (void *)1);

			auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if (sc != NULL)
				sc->endCommand(pSender);
		}
		else
		{
			//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *str1  = ((__String*)strings->objectForKey("generals_strategies_pleaseSelectStrategies"))->m_sString.c_str();
			const char *str1 = StringDataManager::getString("generals_strategies_pleaseSelectStrategies");
			char* p1 = const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(p1);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GeneralsStrategiesUI::StrategiesNotUseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (curSelectFightWayId > 0)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5072, (void *)curSelectFightWayId, (void *)0);
		}
		else
		{
			//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
			//const char *str1  = ((__String*)strings->objectForKey("generals_strategies_pleaseSelectStrategies"))->m_sString.c_str();
			const char *str1 = StringDataManager::getString("generals_strategies_pleaseSelectStrategies");
			char* p1 = const_cast<char*>(str1);
			GameView::getInstance()->showAlertDialog(p1);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsStrategiesUI::StrategiesDetailEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto winSize = Director::getInstance()->getVisibleSize();

		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo))
			return;

		// 	StrategiesDetailInfo *strategiesDetailInfo = StrategiesDetailInfo::create(curSelectFightWayId);
		// 	GameView::getInstance()->getMainUIScene()->addChild(strategiesDetailInfo,0,kTagStrategiesDetailInfo);
		// 
		// 	strategiesDetailInfo->setIgnoreAnchorPointForPosition(false);
		// 	strategiesDetailInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
		// 	strategiesDetailInfo->setPosition(Vec2(winSize.width/2, winSize.height/2));

		//����Ϣ���
		std::map<int, std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(1);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end()) // �û�ҵ�����ָ�END��  
		{
			return;
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			{
				auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[1].c_str());
				GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				showSystemInfo->setIgnoreAnchorPointForPosition(false);
				showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				showSystemInfo->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsStrategiesUI::onEnter()
{
	UIScene::onEnter();
}

void GeneralsStrategiesUI::onExit()
{
	UIScene::onExit();
}

void GeneralsStrategiesUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void GeneralsStrategiesUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

void GeneralsStrategiesUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	int i = cell->getIdx();
	curSelectFightWayId = generalsStrategiesList.at(i)->fightwayid();

	//�ѡ��״̬�� 
	selectCellIndex = cell->getIdx();
	auto _s = table->getContentOffset();
	table->reloadData();
	table->setContentOffset(_s); 

	//������󷨵�ǰ״̬
	if (generalsStrategiesList.at(i)->level()==0)
	{
		curFightWayStatus = NotStuded;
	}
	else 
	{
		if (generalsStrategiesList.at(i)->level()==10)
		{
			curFightWayStatus = Highest;
		}
		else
		{
			curFightWayStatus = Studed;
		}
	}

	//����
	l_strategieName->setString(generalsStrategiesList.at(i)->get_name().c_str());
	//����������Լ����Լӳ
	RefreshStrategiesInfo(generalsStrategiesList.at(i));
	RefreshButtonState(generalsStrategiesList.at(i));

	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(GENERALSSTRATRGIESUI_TABALEVIEW_HIGHLIGHT_TAG))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(GENERALSSTRATRGIESUI_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
		}
	}

	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1) , "res_ui/highlight.png");
	pHighlightSpr->setPreferredSize(Size(245,65));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setTag(GENERALSSTRATRGIESUI_TABALEVIEW_HIGHLIGHT_TAG);
	cell->addChild(pHighlightSpr);

	lastSelectCellId = cell->getIdx();
}

cocos2d::Size GeneralsStrategiesUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(245,65);
}

cocos2d::extension::TableViewCell* GeneralsStrategiesUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called

	cell = new TableViewCell();
	cell->autorelease();
	//ɴ�߿
	auto sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang01_new.png");
	sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
	sprite_bigFrame->setPosition(Vec2(0, 0));
	sprite_bigFrame->setCapInsets(Rect(15,31,1,1));
	sprite_bigFrame->setContentSize(Size(244,63));
	cell->addChild(sprite_bigFrame);
	//�����С�߿
	auto sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_diaa.png");
	sprite_nameFrame->setAnchorPoint(Vec2(0, 0));
	sprite_nameFrame->setPosition(Vec2(83, 17));
	sprite_nameFrame->setCapInsets(Rect(11,11,1,1));
	sprite_nameFrame->setContentSize(Size(146,30));
	sprite_bigFrame->addChild(sprite_nameFrame);

	//�ͷ��С�߿
	auto sprite_headFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_allb.png");
	sprite_headFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
	sprite_headFrame->setPosition(Vec2(41, sprite_bigFrame->getContentSize().height/2));
	sprite_headFrame->setCapInsets(Rect(12,12,1,1));
	sprite_headFrame->setContentSize(Size(52,52));
	sprite_bigFrame->addChild(sprite_headFrame);

	if (this->selectCellIndex == idx)
	{
		auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1) , "res_ui/highlight.png");
		pHighlightSpr->setPreferredSize(Size(244,65));
		pHighlightSpr->setAnchorPoint(Vec2::ZERO);
		pHighlightSpr->setTag(GENERALSSTRATRGIESUI_TABALEVIEW_HIGHLIGHT_TAG);
		cell->addChild(pHighlightSpr);
	}

	std::string str_iconPath = "res_ui/MatrixMethod/";
	str_iconPath.append(generalsStrategiesList.at(idx)->get_icon());
	str_iconPath.append(".png");
	auto  sprite_image = Sprite::create(str_iconPath.c_str());
	sprite_image->setAnchorPoint(Vec2(0.5f,0.5f));
	sprite_image->setPosition(Vec2(41,sprite_bigFrame->getContentSize().height/2));
	sprite_bigFrame->addChild(sprite_image);

	auto label_stratrgiesName = Label::createWithTTF(generalsStrategiesList.at(idx)->get_name().c_str(),APP_FONT_NAME,15);
	label_stratrgiesName->setAnchorPoint(Vec2(0.5f, 0.5f));
	label_stratrgiesName->setPosition(Vec2(117,sprite_bigFrame->getContentSize().height/2));
	auto shadowColor = Color4B::BLACK;   // black
	label_stratrgiesName->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
	sprite_bigFrame->addChild(label_stratrgiesName);

	auto sprite_mengban = cocos2d::extension::Scale9Sprite::create(Rect(1, 1, 1, 1) , "res_ui/zhezhao50.png");
	sprite_mengban->setPreferredSize(Size(43,43));
	sprite_mengban->setAnchorPoint(Vec2(0.5f,0.5f));
	sprite_mengban->setPosition(Vec2(41,sprite_bigFrame->getContentSize().height/2));
	sprite_bigFrame->addChild(sprite_mengban);

	auto  sprite_disabled = Sprite::create("res_ui/suo_2.png");
	sprite_disabled->setAnchorPoint(Vec2(0.5f,0.5f));
	sprite_disabled->setPosition(Vec2(41,sprite_bigFrame->getContentSize().height/2));
	sprite_disabled->setScale(0.8f);
	sprite_bigFrame->addChild(sprite_disabled);

	std::string str_level = "LV ";
	char s_level[5];
	if (generalsStrategiesList.at(idx)->level() <= 1)
	{
		sprintf(s_level,"%d",1);
	}
	else
	{
		sprintf(s_level,"%d",generalsStrategiesList.at(idx)->level());
	}
	str_level.append(s_level);
	auto label_stratrgiesLv = Label::createWithTTF(str_level.c_str(),APP_FONT_NAME,15);
	label_stratrgiesLv->setAnchorPoint(Vec2(0, 0.5f));
	label_stratrgiesLv->setPosition(Vec2(173,sprite_bigFrame->getContentSize().height/2));
	label_stratrgiesLv->enableShadow(shadowColor,Size(1.0f, -1.0f), 1.0f);
	sprite_bigFrame->addChild(label_stratrgiesLv);

	//����ñ�ʶ
	auto  sprite_flag = Sprite::create("res_ui/wujiang/StartUsing.png");
	sprite_flag->setAnchorPoint(Vec2(0.5,0.5f));
	sprite_flag->setPosition(Vec2(15,sprite_bigFrame->getContentSize().height/2+12));
	sprite_bigFrame->addChild(sprite_flag);
	sprite_flag->setVisible(false);

	if (generalsStrategiesList.at(idx)->level() <= 0)
	{
		sprite_mengban->setVisible(true);
		sprite_disabled->setVisible(true);
	}
	else
	{
		sprite_mengban->setVisible(false);
		sprite_disabled->setVisible(false);
	}

	if (generalsStrategiesList.at(idx)->activeflag() == 0)
	{
		GameUtils::addGray((Sprite*)sprite_bigFrame);
		sprite_flag->setVisible(false);
	}
	else 
	{
		GameUtils::removeGray((Sprite*)sprite_bigFrame);
		sprite_flag->setVisible(true);
	}

	return cell;
}

ssize_t GeneralsStrategiesUI::numberOfCellsInTableView( TableView *table )
{
	return generalsStrategiesList.size();
}

void GeneralsStrategiesUI::RefreshGeneralsStrategiesList()
{
	sort(generalsStrategiesList.begin(),generalsStrategiesList.end(),SortFightWay);
	this->strategiesList_tableView->reloadData();
	AutoSetOffSet();
}

void GeneralsStrategiesUI::setVisible( bool visible )
{
	panel_strategies->setVisible(visible);
	Layer_strategies->setVisible(visible);
	Layer_upper->setVisible(visible);
}


bool GeneralsStrategiesUI::isVisible()
{
	return panel_strategies->isVisible();
}


int GeneralsStrategiesUI::getActiveFightWay()
{
	return this->activeFightWayId;
}

void GeneralsStrategiesUI::setActiveFightWay( int _id )
{
	this->activeFightWayId = _id;
}

void GeneralsStrategiesUI::setActiveIndicator()
{
	if (this->activeFightWayId > 0)
	{

	}
}

int GeneralsStrategiesUI::getCurSelectFightWayId()
{
	return curSelectFightWayId;
}

void GeneralsStrategiesUI::RefreshData()
{
	this->RefreshGeneralsStrategiesList();
}

void GeneralsStrategiesUI::RefreshAllGeneralsInLine()
{
	for (int i = 0;i<5;++i)
	{
		if (Layer_strategies->getChildByTag(PositionItemBaseTag + i+1))
		{
			Layer_strategies->getChildByTag(PositionItemBaseTag + i+1)->removeFromParent();
		}
	}

	for(int i = 0;i<5;++i)
	{
		bool ishavegeneral = false;
		for (int j = 0;j<GameView::getInstance()->generalsInLineList.size();++j)
		{
			if (GameView::getInstance()->generalsInLineList.at(j)->order() == i+1)
			{
				auto positionItem = StrategiesPositionItem::create(GameView::getInstance()->generalsInLineList.at(j));
				positionItem->setIgnoreAnchorPointForPosition(false);
				positionItem->setAnchorPoint(Vec2(0,0));
				positionItem->setPosition(slotPos[i]);
				Layer_strategies->addChild(positionItem,0,PositionItemBaseTag+i+1);
				ishavegeneral = true;
				break;
			}
		}

		if (!ishavegeneral)
		{
			auto positionItem = StrategiesPositionItem::create(i+1);
			positionItem->setIgnoreAnchorPointForPosition(false);
			positionItem->setAnchorPoint(Vec2(0,0));
			positionItem->setPosition(slotPos[i]);
			Layer_strategies->addChild(positionItem,0,PositionItemBaseTag+i+1);
		}
	}
}

void GeneralsStrategiesUI::RefreshOneGeneralsInLine(CGeneralBaseMsg * generalBaseMsg)
{	
	for (int i = 0;i<6;i++)
	{
		auto temp = (StrategiesPositionItem * )Layer_strategies->getChildByTag(PositionItemBaseTag + i);
		if (!temp)
			continue;

		if (temp->genenralsId == generalBaseMsg->id())
		{
			int index = temp->positionIndex;
			temp->removeFromParent();

			auto positionItem = StrategiesPositionItem::create(index);
			positionItem->setIgnoreAnchorPointForPosition(false);
			positionItem->setAnchorPoint(Vec2(0,0));
			positionItem->setPosition(slotPos[index-1]);
			Layer_strategies->addChild(positionItem,0,PositionItemBaseTag+index);
		}
	}

	if (Layer_strategies->getChildByTag(PositionItemBaseTag + generalBaseMsg->order()))
	{
		Layer_strategies->getChildByTag(PositionItemBaseTag + generalBaseMsg->order())->removeFromParent();
	}
 
	auto positionItem = StrategiesPositionItem::create(generalBaseMsg);
	positionItem->setIgnoreAnchorPointForPosition(false);
	positionItem->setAnchorPoint(Vec2(0,0));
	positionItem->setPosition(slotPos[generalBaseMsg->order()-1]);
	Layer_strategies->addChild(positionItem,0,PositionItemBaseTag+generalBaseMsg->order());
}

void GeneralsStrategiesUI::RefreshStrategiesInfo( CFightWayBase * fightWayBase )
{
	//����
	//l_strategieName->setText(fightWayBase->get_name().c_str());

	CFightWayGrowingUp * fightWayGrowingUp;
	if (fightWayBase->level()>0)
	{
		fightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(fightWayBase->level());
	}
	else
	{
		fightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[fightWayBase->fightwayid()]->at(1);
	}

	//����״̬��������Ϣ
	std::string str_property = "";
	str_property.append("generals_strategies_property_");
	char s_complex_property[10];
	sprintf(s_complex_property,"%d",fightWayGrowingUp->get_complex_property());
	str_property.append(s_complex_property);
	const char *shuxing = StringDataManager::getString(str_property.c_str());

	std::string property_value = shuxing;
	property_value.append("+");
	int general_property_all = 0;
	for(int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	{
		auto temp = GameView::getInstance()->generalsInLineList.at(i);
		general_property_all += (temp->currentquality() + temp->evolution() + temp->rare());
	}
	int complex_value = 0;
	//if (fightWayBase->level()>0)
	//{
		complex_value = fightWayGrowingUp->get_complex_value();
	//}
	//else
	//{
	//}
	int countLevel = 1;
	if (fightWayBase->level() > 1)
	{
		countLevel = fightWayBase->level();
	}
	float final_result = 0.02*general_property_all*countLevel*complex_value;
	char str_final_result[20];
	sprintf(str_final_result,"%.f",final_result);
	property_value.append(str_final_result);
	property_value.append("%");
	l_addPropDes->setString(property_value.c_str());

	//ˢ�¸����λ�����Լӳ���Ϣ
	for (int i = 0;i<5;++i)
	{
		if (Layer_strategies->getChildByTag(PositionItemBaseTag + i+1))
		{
			auto positionItem = (StrategiesPositionItem *)Layer_strategies->getChildByTag(PositionItemBaseTag + i+1);
			positionItem->RefreshAddedProperty();
		}
	}
}


void GeneralsStrategiesUI::RefreshButtonState( CFightWayBase * fightWayBase )
{
	if (fightWayBase->level()>0)
	{
		btn_openStrategies->setVisible(true);
		btn_openStrategies->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
		btn_openStrategies->setTouchEnabled(true);
		btn_closeStrategies->setVisible(false);

		const char *str_upgrade = StringDataManager::getString("generals_strategies_upgradeFightWay");
		l_upgrade->setString(str_upgrade);
	}
	else
	{
		btn_openStrategies->setVisible(true);
		btn_openStrategies->loadTextures("res_ui/new_button_001.png","res_ui/new_button_001.png","");
		btn_openStrategies->setTouchEnabled(false);
		btn_closeStrategies->setVisible(false);

		const char *str_study = StringDataManager::getString("generals_strategies_studyFightWay");
		l_upgrade->setString(str_study);
	}

	//���ÿ��/�رհ�ť
	if(fightWayBase->activeflag() == 1)
	{
		btn_openStrategies->setVisible(false);
		btn_closeStrategies->setVisible(true);
	}
	else
	{
		btn_openStrategies->setVisible(true);
		btn_closeStrategies->setVisible(false);
	}
}

void GeneralsStrategiesUI::RefreshListToDefault()
{
	if (generalsStrategiesList.size()<=0)
		return;

	for (int i = 0;i<generalsStrategiesList.size();++i)
	{
		if (generalsStrategiesList.at(i)->activeflag() == 1)
		{
			activeFightWayId = generalsStrategiesList.at(i)->fightwayid();
		}
	}

	selectCellIndex = 0;
	curSelectFightWayId = generalsStrategiesList.at(0)->fightwayid();

	if (generalsStrategiesList.at(selectCellIndex)->level()==0)
	{
		curFightWayStatus = NotStuded;
	}
	else 
	{
		if (generalsStrategiesList.at(selectCellIndex)->level()==10)
		{
			curFightWayStatus = Highest;
		}
		else
		{
			curFightWayStatus = Studed;
		}
	}

	l_strategieName->setString(generalsStrategiesList.at(selectCellIndex)->get_name().c_str());
	RefreshStrategiesInfo(generalsStrategiesList.at(selectCellIndex));
	RefreshButtonState(generalsStrategiesList.at(selectCellIndex));

	sort(generalsStrategiesList.begin(),generalsStrategiesList.end(),SortFightWay);
	strategiesList_tableView->reloadData();
	AutoSetOffSet();
}

void GeneralsStrategiesUI::RefreshListWithoutChangeOffSet()
{
	for (int i = 0;i<generalsStrategiesList.size();++i)
	{
		if (generalsStrategiesList.at(i)->activeflag() == 1)
		{
			activeFightWayId = generalsStrategiesList.at(i)->fightwayid();
			//selectCellIndex = i;
			//curSelectFightWayId = generalsStrategiesList.at(i)->fightwayid();
		}
	}

	if (activeFightWayId > 0)
	{

	}
	else
	{
		if (generalsStrategiesList.size()>0)
		{
			selectCellIndex = 0;
			curSelectFightWayId = generalsStrategiesList.at(0)->fightwayid();
		}
	}

	if (generalsStrategiesList.at(selectCellIndex)->level()==0)
	{
		curFightWayStatus = NotStuded;
	}
	else 
	{
		if (generalsStrategiesList.at(selectCellIndex)->level()==10)
		{
			curFightWayStatus = Highest;
		}
		else
		{
			curFightWayStatus = Studed;
		}
	}

	RefreshStrategiesInfo(generalsStrategiesList.at(selectCellIndex));
	RefreshButtonState(generalsStrategiesList.at(selectCellIndex));


	sort(generalsStrategiesList.begin(),generalsStrategiesList.end(),SortFightWay);
	Vec2 _pt = strategiesList_tableView->getContentOffset();
	strategiesList_tableView->reloadData();
	strategiesList_tableView->setContentOffset(_pt);
}

void GeneralsStrategiesUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralsStrategiesUI::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,70,60);
// 	tutorialIndicator->setPosition(Vec2(pos.x-40,pos.y+95));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	Layer_upper->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,100,43,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralsStrategiesUI::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(Layer_upper->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void GeneralsStrategiesUI::RefreshAllInfo( CFightWayBase * fightWayBase )
{
	if(curSelectFightWayId != fightWayBase->fightwayid())
		return;

	//�����󷨵�ǰ״̬
	if (fightWayBase->level()==0)
	{
		curFightWayStatus = NotStuded;
	}
	else 
	{
		if (fightWayBase->level()==10)
		{
			curFightWayStatus = Highest;
		}
		else
		{
			curFightWayStatus = Studed;
		}
	}
	//���������Լ����Լӳ
	RefreshStrategiesInfo(fightWayBase);
	RefreshButtonState(fightWayBase);

	//�����ѧϰ/��ť����ʾ
	//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
// 	if (curFightWayStatus == NotStuded)
// 	{
// 		//const char *str_study  = ((__String*)strings->objectForKey("generals_strategies_studyFightWay"))->m_sString.c_str();
// 		const char *str_study = StringDataManager::getString("generals_strategies_studyFightWay");
// 		l_upgrade->setText(str_study);
// 	}
// 	else
// 	{
// 		//const char *str_upgrade  = ((__String*)strings->objectForKey("generals_strategies_upgradeFightWay"))->m_sString.c_str();
// 		const char *str_upgrade = StringDataManager::getString("generals_strategies_upgradeFightWay");
// 		l_upgrade->setText(str_upgrade);
// 	}


}

void GeneralsStrategiesUI::AutoSetOffSet()
{
	int selectIndex = -1;
	for (int i = 0;i<generalsStrategiesList.size();i++)
	{
		if (curSelectFightWayId == generalsStrategiesList.at(i)->fightwayid())
		{
			selectIndex = i;
			break;
		}
	}

	if (selectIndex == -1)
		return;

	strategiesList_tableView->cellAtIndex(selectIndex);

	if (generalsStrategiesList.size()*tableCellSizeForIndex(strategiesList_tableView,0).height < strategiesList_tableView->getViewSize().height)
	{
		//m_tableView->setContentOffset(Vec2(0,0));
	}
	else
	{
		if ((generalsStrategiesList.size()-selectIndex)*tableCellSizeForIndex(strategiesList_tableView,0).height  <= strategiesList_tableView->getViewSize().height)  //����󼸸
		{
			strategiesList_tableView->setContentOffset(Vec2(0,0));
		}
		else
		{
			int beginHeight = strategiesList_tableView->getViewSize().height - generalsStrategiesList.size()*tableCellSizeForIndex(strategiesList_tableView,0).height;
			strategiesList_tableView->setContentOffset(Vec2(0,beginHeight+selectIndex*tableCellSizeForIndex(strategiesList_tableView,0).height));
		}
	}
}

void GeneralsStrategiesUI::setStrategiesListToDefault()
{
	if (generalsStrategiesList.size()>0)
	{
		std::vector<CFightWayBase*>::iterator iter_generalStrategies;
		for (iter_generalStrategies = generalsStrategiesList.begin(); iter_generalStrategies != generalsStrategiesList.end(); ++iter_generalStrategies)
		{
			delete *iter_generalStrategies;
		}
		generalsStrategiesList.clear();
	}

	for (int i = 0;i<FightWayConfigData::s_fightWayBase.size();++i)
	{
		auto temp = new CFightWayBase();
		temp->CopyFrom(*FightWayConfigData::s_fightWayBase.at(i));
		temp->set_name(FightWayConfigData::s_fightWayBase.at(i)->get_name());
		temp->set_kValue(FightWayConfigData::s_fightWayBase.at(i)->get_kValue());
		temp->set_icon(FightWayConfigData::s_fightWayBase.at(i)->get_icon());
		temp->set_description(FightWayConfigData::s_fightWayBase.at(i)->get_description());
		generalsStrategiesList.push_back(temp);
	}
}
