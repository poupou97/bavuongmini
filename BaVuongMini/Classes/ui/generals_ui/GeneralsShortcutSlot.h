

#ifndef _GENERALSUI_GENERALSSHORTCUTSLOT_H_
#define _GENERALSUI_GENERALSSHORTCUTSLOT_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "GeneralsSkillsUI.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CShortCut;
class GameFightSkill;

#define GENERALSKILLEMPTYPATH "res_ui/round_none.png"

#define GENERALSKILLVoidFramePath "res_ui/round_none.png"
#define GENERALSKILLWhiteFramePath "res_ui/round_white.png"
#define GENERALSKILLGreenFramePath "res_ui/round_green.png"
#define GENERALSKILLBlueFramePath "res_ui/round_blue.png"
#define GENERALSKILLPurpleFramePath "res_ui/round_purple.png"
#define GENERALSKILLOrangeFramePath "res_ui/round_orange.png"

class GeneralsShortcutSlot : public UIScene 
{
public:
	GeneralsShortcutSlot();
	~GeneralsShortcutSlot();
	//���ͣ�����ܣ��������
	enum SkillType{
		Actived,
		Passived
	};
	//�״̬��δ�����ѿ�����װ�
	enum SkillState
	{
		NotOpen,
		Opened,
		Used
	};

	static GeneralsShortcutSlot* create(int quality,CShortCut * shortcut);
	bool init(int quality,CShortCut * shortcut);

	static GeneralsShortcutSlot* create(int quality,int slotIndex);
	bool init(int quality,int slotIndex);

	static GeneralsShortcutSlot* create(GameFightSkill * gameFightSkill);
	bool init(GameFightSkill * gameFightSkill);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void UpdateSkillType(int slotIndex);
	void UpdateSkillState(int slotIndex,int quality);
	void UpdateSkillState(CShortCut * shortcut,int quality);

	void DoThing(Ref *pSender, Widget::TouchEventType type);

	void setSkillProfession(GeneralsSkillsUI::SkillTpye st);
	GeneralsSkillsUI::SkillTpye getSkillProfession();

	int getIndex();
	virtual void setTouchEnabled(bool value);

private:
	SkillType skillType;
	SkillState skillState;

	ui::Button * btn_bgFrame;

	GeneralsSkillsUI::SkillTpye skill_profession;

	std::string skillId;
	int index;
};

#endif

