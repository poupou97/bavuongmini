#include "StrategiesPositionItem.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "GeneralsListBase.h"
#include "NormalGeneralsHeadItem.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "generals_popup_ui/OnStrategiesGeneralsInfoUI.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "GeneralsUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CFightWayBase.h"
#include "../../messageclient/element/CFightWayGrowingUp.h"
#include "GeneralsStrategiesUI.h"
#include "AppMacros.h"
#include "RecuriteActionItem.h"


StrategiesPositionItem::StrategiesPositionItem():
positionIndex(1),
fightStatus(1),
isHaveGeneral(false),
genenralsId(-1)
{
}


StrategiesPositionItem::~StrategiesPositionItem()
{
}

StrategiesPositionItem * StrategiesPositionItem::create(CGeneralBaseMsg * generalBaseMsg)
{
	auto strategiesPositionItem = new StrategiesPositionItem();
	if (strategiesPositionItem && strategiesPositionItem->init(generalBaseMsg))
	{
		strategiesPositionItem->autorelease();
		return strategiesPositionItem;
	}
	CC_SAFE_DELETE(strategiesPositionItem);
	return NULL;
}

StrategiesPositionItem * StrategiesPositionItem::create( int index )
{
	auto strategiesPositionItem = new StrategiesPositionItem();
	if (strategiesPositionItem && strategiesPositionItem->init(index))
	{
		strategiesPositionItem->autorelease();
		return strategiesPositionItem;
	}
	CC_SAFE_DELETE(strategiesPositionItem);
	return NULL;
}

bool StrategiesPositionItem::init(CGeneralBaseMsg * generalBaseMsg)
{
	if (UIScene::init())
	{
		if (generalBaseMsg->order() == 0)
			return true;

		positionIndex = generalBaseMsg->order();
		fightStatus = generalBaseMsg->fightstatus();
		isHaveGeneral = true;
		genenralsId = generalBaseMsg->id();

		auto imageView_Frame = ImageView::create();
		imageView_Frame->loadTexture("res_ui/no4_di.png");
		imageView_Frame->setScale9Enabled(true);
		imageView_Frame->setCapInsets(Rect(7,7,1,1));
		imageView_Frame->setContentSize(Size(127,165));
		imageView_Frame->setAnchorPoint(Vec2(0,0));
		imageView_Frame->setPosition(Vec2(0,0));
		m_pLayer->addChild(imageView_Frame);

// 		ImageView * imageView_miaobian = ImageView::create();
// 		imageView_miaobian->loadTexture("res_ui/miaobian1.png");
// 		imageView_miaobian->setScale9Enabled(true);
// 		imageView_miaobian->setContentSize(Size(126,163));
// 		imageView_miaobian->setCapInsets(Rect(4,4,1,1));
// 		imageView_miaobian->setAnchorPoint(Vec2(0,0));
// 		imageView_miaobian->setPosition(Vec2(-1,-1));
// 		m_pLayer->addChild(imageView_miaobian);

		auto imageView_desFrame = ImageView::create();
		imageView_desFrame->loadTexture("res_ui/zhezhao80.png");
		imageView_desFrame->setScale9Enabled(true);
		imageView_desFrame->setContentSize(Size(118,23));
		imageView_desFrame->setAnchorPoint(Vec2(0,0));
		imageView_desFrame->setPosition(Vec2(4,3));
		imageView_Frame->addChild(imageView_desFrame);

		//�ײ��ļӳ���ʾ
		l_additive = Label::createWithTTF("asdf1234", APP_FONT_NAME, 16);
		l_additive->setColor(Color3B(108,255,0));
		l_additive->setAnchorPoint(Vec2(0.5f,0.5f));
		l_additive->setPosition(Vec2(62,13));
		m_pLayer->addChild(l_additive);


		//���Ϸ���״̬��ť
// 		btn_fightStatus = Button::create();
// 		btn_fightStatus->loadTextures("res_ui/new_button_00.png","res_ui/new_button_00.png","");
// 		btn_fightStatus->setScale9Enabled(true);
// 		btn_fightStatus->setContentSize(Size(126,34));
// 		btn_fightStatus->setAnchorPoint(Vec2(0.5f,0.5f));
// 		btn_fightStatus->setCapInsets(Rect(31,0,1,0));
// 		btn_fightStatus->setPosition(Vec2(62,147));
// 		btn_fightStatus->setTouchEnabled(true);
// 		btn_fightStatus->addTouchEventListener(CC_CALLBACK_2(StrategiesPositionItem::FightStatusEvent));
// 		m_pLayer->addChild(btn_fightStatus);
		//��
// 		Label * l_index = Label::create();
// 		char s_positionIndex[3];
// 		sprintf(s_positionIndex,"%d",positionIndex);
// 		l_index->setText(s_positionIndex);
// 		l_index->setAnchorPoint(Vec2(0,0));
// 		l_index->setPosition(Vec2(-29,-9));
// 		l_index->setFontName(APP_FONT_NAME);
// 		l_index->setFontSize(18);
// 		btn_fightStatus->addChild(l_index);
		//�״̬����ʾ
// 		l_fightStatus = Label::create();
// 		RefreshGeneralsStatus(generalBaseMsg->fightstatus());
// 		l_fightStatus->setAnchorPoint(Vec2(0,0));
// 		l_fightStatus->setPosition(Vec2(-12,-9));
// 		l_fightStatus->setFontName(APP_FONT_NAME);
// 		l_fightStatus->setFontSize(18);
// 		btn_fightStatus->addChild(l_fightStatus);
		//�м��ͷ�񱳾��
		auto btn_headFrame = Button::create();
		btn_headFrame->loadTextures("res_ui/generals_white.png","res_ui/generals_white.png","");
		btn_headFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_headFrame->setPosition(Vec2(62,92));
		btn_headFrame->setTouchEnabled(true);
		btn_headFrame->addTouchEventListener(CC_CALLBACK_2(StrategiesPositionItem::HeadFrameEvent, this));
		imageView_Frame->addChild(btn_headFrame);
		btn_headFrame->setScale(0.93f);
		//��м��ͷ�
		if (generalBaseMsg->fightstatus() == GeneralsListBase::NoBattle)
		{
			//��м��ͷ�(�û��ʱ��һ��Ӻ)
// 			ImageView * imageView_head = ImageView::create();
// 			imageView_head->loadTexture("res_ui/plus.png");
// 			imageView_head->setAnchorPoint(Vec2(0.5f,0.5f));
// 			imageView_head->setPosition(Vec2(62,79));
// 			m_pLayer->addChild(imageView_head);
		}
		else
		{
			auto normalGeneralsHeadItem = NormalGeneralsHeadItemBase::create(generalBaseMsg);
			normalGeneralsHeadItem->setIgnoreAnchorPointForPosition(false);
			normalGeneralsHeadItem->setAnchorPoint(Vec2(0.5f,0.5f));
			normalGeneralsHeadItem->setPosition(Vec2(62,92));
			addChild(normalGeneralsHeadItem);
			normalGeneralsHeadItem->setScale(0.93f);
// 			CGeneralBaseMsg * generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];
// 			//ű߿
// 			ImageView *frame = ImageView::create();
// 			frame->loadTexture(GeneralsUI::getBigHeadFramePath(generalBaseMsg->currentquality()).c_str());
// 			frame->setScale9Enabled(true);
// 			frame->setContentSize(Size(108,110));
// 			frame->setAnchorPoint(Vec2::ZERO);
// 			frame->setPosition(Vec2(8,24));
// 			m_pLayer->addChild(frame);
// 			//�ͷ�
// 			std::string icon_path = "res_ui/generals/";
// 			icon_path.append(generalMsgFromDb->get_half_photo());
// 			icon_path.append(".png");
// 			ImageView *imageView_head = ImageView::create();
// 			imageView_head->loadTexture(icon_path.c_str());
// 			imageView_head->setAnchorPoint(Vec2(0.5f,0));
// 			imageView_head->setPosition(Vec2(frame->getPosition().x+frame->getContentSize().width/2,31));
// 			imageView_head->setScale(0.85f);
// 			m_pLayer->addChild(imageView_head);
// 			//����ֺڵ
//  			ImageView *black_namebg = ImageView::create(); 
//  			black_namebg->loadTexture("res_ui/name_di3.png");
//  			black_namebg->setAnchorPoint(Vec2(0.5f,0));
// 			black_namebg->setScale9Enabled(true);
// 			black_namebg->setCapInsets(Rect(12,18,1,1));
// 			black_namebg->setContentSize(Size(96,19));
//  			black_namebg->setPosition(Vec2(frame->getPosition().x+frame->getContentSize().width/2,31));
//  			m_pLayer->addChild(black_namebg);
// 			//����
// 			Label *label_name = Label::create();
// 			label_name->setText(generalMsgFromDb->name().c_str());
// 			label_name->setFontName(APP_FONT_NAME);
// 			label_name->setFontSize(16);
// 			label_name->setAnchorPoint(Vec2(0.5f,0.5f));
// 			label_name->setPosition(Vec2(frame->getPosition().x+frame->getContentSize().width/2,black_namebg->getPosition().y+black_namebg->getContentSize().height/2));
// 			label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
// 			m_pLayer->addChild(label_name);
// 
// 			//�ϡ�ж
// 			if (generalBaseMsg->rare()>0)
// 			{
// 				ImageView * star = ImageView::create();
// 				star->loadTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
// 				star->setAnchorPoint(Vec2::ZERO);
// 				star->setScale(0.8f);
// 				star->setPosition(Vec2(11,40));
// 				m_pLayer->addChild(star);
// 			}
// 
// 			//ȵȼ���ͼ
// 			ImageView * imageView_lvFrame = ImageView::create();
// 			imageView_lvFrame->loadTexture("res_ui/zhezhao80.png");
// 			imageView_lvFrame->setScale9Enabled(true);
// 			imageView_lvFrame->setContentSize(Size(43,15));
// 			imageView_lvFrame->setAnchorPoint(Vec2(1.0f,0));
// 			imageView_lvFrame->setPosition(Vec2(110,50));
// 			m_pLayer->addChild(imageView_lvFrame);
// 			//�ȼ�
// 			std::string _lv = "LV";
// 			char s_level[5];
// 			sprintf(s_level,"%d",generalBaseMsg->level());
// 			_lv.append(s_level);
// 			Label * label_level = Label::create();
// 			label_level->setText(_lv.c_str());
// 			label_level->setAnchorPoint(Vec2(0.5f,0.5f));
// 			label_level->setPosition(Vec2(-20,8));
// 			imageView_lvFrame->addChild(label_level);
// 			//���
// 			std::string rankPathBase = "res_ui/rank/rank";
// 			char s_evolution[5];
// 			sprintf(s_evolution,"%d",generalBaseMsg->evolution());
// 			rankPathBase.append(s_evolution);
// 			rankPathBase.append(".png");
// 			ImageView * imageView_rank = ImageView::create();
// 			imageView_rank->loadTexture(rankPathBase.c_str());
// 			imageView_rank->setAnchorPoint(Vec2(1.0f,1.0f));
// 			imageView_rank->setPosition(Vec2(107,122));
// 			m_pLayer->addChild(imageView_rank);
		}

		auto u_layer_pos = Layer::create();
		addChild(u_layer_pos);

		std::string str_postionPath = "res_ui/wujiang/";
		switch(positionIndex)
		{
		case 1:
			{
				str_postionPath.append("one");
			}
			break;
		case 2:
			{
				str_postionPath.append("two");
			}
			break;
		case 3:
			{
				str_postionPath.append("three");
			}
			break;
		case 4:
			{
				str_postionPath.append("four");
			}
			break;
		case 5:
			{
				str_postionPath.append("five");
			}
			break;
		}
		str_postionPath.append(".png");
		auto imageView_pos = ImageView::create();
		imageView_pos->loadTexture(str_postionPath.c_str());
		imageView_pos->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_pos->setPosition(Vec2(21,140));
		u_layer_pos->addChild(imageView_pos);

		RefreshAddedProperty();

		this->setContentSize(Size(127,165));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(StrategiesPositionItem::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(StrategiesPositionItem::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(StrategiesPositionItem::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(StrategiesPositionItem::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

bool StrategiesPositionItem::init( int index )
{
	if (UIScene::init())
	{
		positionIndex = index;
		fightStatus = GeneralsListBase::HoldTheLine;
		isHaveGeneral = false;

		auto imageView_Frame = ImageView::create();
		imageView_Frame->loadTexture("res_ui/no4_di.png");
		imageView_Frame->setScale9Enabled(true);
		imageView_Frame->setCapInsets(Rect(7,7,1,1));
		imageView_Frame->setContentSize(Size(127,165));
		imageView_Frame->setAnchorPoint(Vec2(0,0));
		imageView_Frame->setPosition(Vec2(0,0));
		m_pLayer->addChild(imageView_Frame);

// 		ImageView * imageView_miaobian = ImageView::create();
// 		imageView_miaobian->loadTexture("res_ui/miaobian1.png");
// 		imageView_miaobian->setScale9Enabled(true);
// 		imageView_miaobian->setContentSize(Size(126,163));
// 		imageView_miaobian->setCapInsets(Rect(4,4,1,1));
// 		imageView_miaobian->setAnchorPoint(Vec2(0,0));
// 		imageView_miaobian->setPosition(Vec2(-1,-1));
// 		m_pLayer->addChild(imageView_miaobian);

		auto imageView_desFrame = ImageView::create();
		imageView_desFrame->loadTexture("res_ui/zhezhao80.png");
		imageView_desFrame->setScale9Enabled(true);
		imageView_desFrame->setContentSize(Size(118,23));
		imageView_desFrame->setAnchorPoint(Vec2(0,0));
		imageView_desFrame->setPosition(Vec2(4,3));
		imageView_Frame->addChild(imageView_desFrame);

		//εײ��ļӳ���ʾ
		l_additive = Label::createWithTTF("asdf1234", APP_FONT_NAME, 16);
		l_additive->setColor(Color3B(108,255,0));
		l_additive->setAnchorPoint(Vec2(0.5f,0.5f));
		l_additive->setPosition(Vec2(62,13));
		m_pLayer->addChild(l_additive);

		//���Ϸ���״̬��ť
// 		btn_fightStatus = Button::create();
// 		btn_fightStatus->loadTextures("res_ui/new_button_00.png","res_ui/new_button_00.png","");
// 		btn_fightStatus->setScale9Enabled(true);
// 		btn_fightStatus->setContentSize(Size(126,34));
// 		btn_fightStatus->setAnchorPoint(Vec2(0.5f,0.5f));
// 		btn_fightStatus->setPosition(Vec2(62,147));
// 		btn_fightStatus->setTouchEnabled(true);
// 		btn_fightStatus->addTouchEventListener(CC_CALLBACK_2(StrategiesPositionItem::FightStatusEvent));
// 		m_pLayer->addChild(btn_fightStatus);
		//��
// 		Label * l_index = Label::create();
// 		char s_positionIndex[3];
// 		sprintf(s_positionIndex,"%d",positionIndex);
// 		l_index->setText(s_positionIndex);
// 		l_index->setAnchorPoint(Vec2(0,0));
// 		l_index->setPosition(Vec2(-29,-7));
// 		l_index->setFontName(APP_FONT_NAME);
// 		l_index->setFontSize(18);
// 		btn_fightStatus->addChild(l_index);
		//�״̬����ʾ
// 		l_fightStatus = Label::create();
// 		RefreshGeneralsStatus(GeneralsListBase::NoBattle);
// 		l_fightStatus->setAnchorPoint(Vec2(0,0));
// 		l_fightStatus->setPosition(Vec2(-12,-7));
// 		l_fightStatus->setFontName(APP_FONT_NAME);
// 		l_fightStatus->setFontSize(18);
// 		btn_fightStatus->addChild(l_fightStatus);
		//�м��ͷ�񱳾��
		auto btn_headFrame = Button::create();
		btn_headFrame->loadTextures("res_ui/generals_white.png","res_ui/generals_white.png","");
		btn_headFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_headFrame->setPosition(Vec2(62,92));
		btn_headFrame->setTouchEnabled(true);
		btn_headFrame->addTouchEventListener(CC_CALLBACK_2(StrategiesPositionItem::HeadFrameEvent, this));
		imageView_Frame->addChild(btn_headFrame);
		btn_headFrame->setScale(0.93f);
		//��м��ͷ�(�û��ʱ��һ��Ӻ)
// 		ImageView * imageView_head = ImageView::create();
// 		imageView_head->loadTexture("res_ui/plus.png");
// 		imageView_head->setAnchorPoint(Vec2(0.5f,0.5f));
// 		imageView_head->setPosition(Vec2(62,79));
// 		m_pLayer->addChild(imageView_head);

		auto u_layer_pos = Layer::create();
		addChild(u_layer_pos);

		std::string str_postionPath = "res_ui/wujiang/";
		switch(index)
		{
		case 1:
			{
				str_postionPath.append("one");
			}
			break;
		case 2:
			{
				str_postionPath.append("two");
			}
			break;
		case 3:
			{
				str_postionPath.append("three");
			}
			break;
		case 4:
			{
				str_postionPath.append("four");
			}
			break;
		case 5:
			{
				str_postionPath.append("five");
			}
			break;
		}
		str_postionPath.append(".png");
		auto imageView_pos = ImageView::create();
		imageView_pos->loadTexture(str_postionPath.c_str());
		imageView_pos->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_pos->setPosition(Vec2(21,140));
		u_layer_pos->addChild(imageView_pos);

		RefreshAddedProperty();

		this->setContentSize(Size(127,165));

		return true;
	}
	return false;
}

void StrategiesPositionItem::onEnter()
{
	UIScene::onEnter();
}

void StrategiesPositionItem::onExit()
{
	UIScene::onExit();
}

bool StrategiesPositionItem::onTouchBegan( Touch *touch, Event * pEvent )
{
	return false;
}

void StrategiesPositionItem::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void StrategiesPositionItem::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void StrategiesPositionItem::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void StrategiesPositionItem::FightStatusEvent( Ref *pSender )
{
	if (genenralsId > 0)
	{
		if (fightStatus == GeneralsListBase::HoldTheLine)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)genenralsId,(void *)2);
		}
		else if (fightStatus == GeneralsListBase::InBattle)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5056,(void *)genenralsId,(void *)1);
		}
	}
	
}

void StrategiesPositionItem::HeadFrameEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsListForFightWaysUI) == NULL)
		{
			auto winSize = Director::getInstance()->getVisibleSize();
			if (isHaveGeneral)
			{
				auto onStrategiesGeneralsInfoUI = OnStrategiesGeneralsInfoUI::create(positionIndex, genenralsId);
				onStrategiesGeneralsInfoUI->setIgnoreAnchorPointForPosition(false);
				onStrategiesGeneralsInfoUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				onStrategiesGeneralsInfoUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				GameView::getInstance()->getMainUIScene()->addChild(onStrategiesGeneralsInfoUI, 0, kTagGeneralsListForFightWaysUI);
			}
			else
			{
				auto onStrategiesGeneralsInfoUI = OnStrategiesGeneralsInfoUI::create(positionIndex);
				onStrategiesGeneralsInfoUI->setIgnoreAnchorPointForPosition(false);
				onStrategiesGeneralsInfoUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				onStrategiesGeneralsInfoUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				GameView::getInstance()->getMainUIScene()->addChild(onStrategiesGeneralsInfoUI, 0, kTagGeneralsListForFightWaysUI);
			}

		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void StrategiesPositionItem::RefreshAddedProperty()
{
	int fightwayLevel = 0;
	for(int i = 0;i<GeneralsUI::generalsStrategiesUI->generalsStrategiesList.size();i++)
	{
		if (GeneralsUI::generalsStrategiesUI->getCurSelectFightWayId() == GeneralsUI::generalsStrategiesUI->generalsStrategiesList.at(i)->fightwayid())
		{
			fightwayLevel = GeneralsUI::generalsStrategiesUI->generalsStrategiesList.at(i)->level();
			break;
		}
	}
	CFightWayGrowingUp * fightWayGrowingUp;
	if (fightwayLevel == 0)
	{
		fightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[GeneralsUI::generalsStrategiesUI->getCurSelectFightWayId()]->at(1);
	}
	else if (fightwayLevel == 10)
	{
		fightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[GeneralsUI::generalsStrategiesUI->getCurSelectFightWayId()]->at(10);
	}
	else
	{
		fightWayGrowingUp = FightWayGrowingUpConfigData::s_fightWayGrowingUp[GeneralsUI::generalsStrategiesUI->getCurSelectFightWayId()]->at(fightwayLevel);
	}

	if (fightWayGrowingUp != NULL)
		switch(positionIndex)
	{
		case 1 :
			{
				std::string str_property1 = "generals_strategies_property_";
				char s_grid1_property[10];
				sprintf(s_grid1_property,"%d",fightWayGrowingUp->get_grid1_property());
				str_property1.append(s_grid1_property);
				const char *shuxing_1 = StringDataManager::getString(str_property1.c_str());
				std::string property1_value = shuxing_1;
				property1_value.append("+");
// 				if (fightwayLevel>0)
// 				{
					char s_grid1_value[10];
					sprintf(s_grid1_value,"%.f",fightWayGrowingUp->get_grid1_value());
					property1_value.append(s_grid1_value);
// 				}
// 				else
// 				{
// 					property1_value.append("0");
// 				}
				property1_value.append("%");
				l_additive->setString(property1_value.c_str());
			}
			break;
		case 2 :
			{
				std::string str_property2 = "generals_strategies_property_";
				char s_grid2_property[10];
				sprintf(s_grid2_property,"%d",fightWayGrowingUp->get_grid2_property());
				str_property2.append(s_grid2_property);
				const char *shuxing_2 = StringDataManager::getString(str_property2.c_str());
				std::string property2_value = shuxing_2;
				property2_value.append("+");
// 				if (fightwayLevel>0)
// 				{
					char s_grid2_value[10];
					sprintf(s_grid2_value,"%.f",fightWayGrowingUp->get_grid2_value());
					property2_value.append(s_grid2_value);
// 				}
// 				else
// 				{
// 					property2_value.append("0");
// 				}
				property2_value.append("%");
				l_additive->setString(property2_value.c_str());
			}
			break;
		case 3 :
			{
				std::string str_property3 = "generals_strategies_property_";
				char s_grid3_property[10];
				sprintf(s_grid3_property,"%d",fightWayGrowingUp->get_grid3_property());
				str_property3.append(s_grid3_property);
				const char *shuxing_3 = StringDataManager::getString(str_property3.c_str());
				std::string property3_value = shuxing_3;
				property3_value.append("+");
// 				if (fightwayLevel>0)
// 				{
					char s_grid3_value[10];
					sprintf(s_grid3_value,"%.f",fightWayGrowingUp->get_grid3_value());
					property3_value.append(s_grid3_value);
// 				}
// 				else
// 				{
// 					property3_value.append("0");
// 				}
				property3_value.append("%");
				l_additive->setString(property3_value.c_str());
			}
			break;
		case 4 :
			{
				std::string str_property4 = "generals_strategies_property_";
				char s_grid4_property[10];
				sprintf(s_grid4_property,"%d",fightWayGrowingUp->get_grid4_property());
				str_property4.append(s_grid4_property);
				const char *shuxing_4 = StringDataManager::getString(str_property4.c_str());
				std::string property4_value = shuxing_4;
				property4_value.append("+");
// 				if (fightwayLevel>0)
// 				{
					char s_grid4_value[10];
					sprintf(s_grid4_value,"%.f",fightWayGrowingUp->get_grid4_value());
					property4_value.append(s_grid4_value);
// 				}
// 				else
// 				{
// 					property4_value.append("0");
// 				}
				property4_value.append("%");
				l_additive->setString(property4_value.c_str());
			}
			break;
		case 5 :
			{
				std::string str_property5 = "generals_strategies_property_";
				char s_grid5_property[10];
				sprintf(s_grid5_property,"%d",fightWayGrowingUp->get_grid5_property());
				str_property5.append(s_grid5_property);
				const char *shuxing_5 = StringDataManager::getString(str_property5.c_str());
				std::string property5_value = shuxing_5;
				property5_value.append("+");
// 				if (fightwayLevel>0)
// 				{
					char s_grid5_value[10];
					sprintf(s_grid5_value,"%.f",fightWayGrowingUp->get_grid5_value());
					property5_value.append(s_grid5_value);
// 				}
// 				else
// 				{
// 					property5_value.append("0");
// 				}
				property5_value.append("%");
				l_additive->setString(property5_value.c_str());
			}
			break;
	}
}

void StrategiesPositionItem::RefreshGeneralsStatus( int _status )
{
	fightStatus = _status;
	//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
	if (fightStatus == GeneralsListBase::InBattle)
	{
		//const char *str1  = ((__String*)strings->objectForKey("generals_fighttype_chuzhan"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("generals_fighttype_chuzhan");
		char* p1 =const_cast<char*>(str1);
		l_fightStatus->setString(p1);

		btn_fightStatus->loadTextures("res_ui/new_button_000.png","res_ui/new_button_000.png","");
	}
	else if(fightStatus == GeneralsListBase::HoldTheLine ||fightStatus == GeneralsListBase::NoBattle)
	{
		//const char *str1  = ((__String*)strings->objectForKey("generals_fighttype_luezhen"))->m_sString.c_str();
		const char *str1 = StringDataManager::getString("generals_fighttype_luezhen");
		char* p1 =const_cast<char*>(str1);
		l_fightStatus->setString(p1);

		btn_fightStatus->loadTextures("res_ui/new_button_00.png","res_ui/new_button_00.png","");
	}
}
