
#include "ShowBigGeneralsHead.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../utils/StaticDataManager.h"
#include "GeneralsUI.h"
#include "GameView.h"
#include "RecuriteActionItem.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "AppMacros.h"

#define M_Frame_Scale 1.0f

#define KTag_FiveStar_Anm 345
#define kTagLegendAnm_Head 10  
#define kTagBeginAnm 20

ShowBigGeneralsHead::ShowBigGeneralsHead():
m_nCreateType(-1)
{
	m_pGeneralBaseMsg = new CGeneralBaseMsg();
}

ShowBigGeneralsHead::~ShowBigGeneralsHead()
{
	delete m_pGeneralBaseMsg;
}


void ShowBigGeneralsHead::onEnter()
{
	UIScene::onEnter();
}

void ShowBigGeneralsHead::onExit()
{
	UIScene::onExit();
}


ShowBigGeneralsHead * ShowBigGeneralsHead::create(CGeneralBaseMsg * generalBaseMsg)
{
	auto showBigGeneralsHead = new ShowBigGeneralsHead();
	if (showBigGeneralsHead && showBigGeneralsHead->init(generalBaseMsg))
	{
		showBigGeneralsHead->autorelease();
		return showBigGeneralsHead;
	}
	CC_SAFE_DELETE(showBigGeneralsHead);
	return NULL;
}

ShowBigGeneralsHead * ShowBigGeneralsHead::create( int generalModleId)
{
	auto showBigGeneralsHead = new ShowBigGeneralsHead();
	if (showBigGeneralsHead && showBigGeneralsHead->init(generalModleId))
	{
		showBigGeneralsHead->autorelease();
		return showBigGeneralsHead;
	}
	CC_SAFE_DELETE(showBigGeneralsHead);
	return NULL;
}

ShowBigGeneralsHead * ShowBigGeneralsHead::create( int generalModleId,int currentquality )
{
	auto showBigGeneralsHead = new ShowBigGeneralsHead();
	if (showBigGeneralsHead && showBigGeneralsHead->init(generalModleId,currentquality))
	{
		showBigGeneralsHead->autorelease();
		return showBigGeneralsHead;
	}
	CC_SAFE_DELETE(showBigGeneralsHead);
	return NULL;
}

bool ShowBigGeneralsHead::init(CGeneralBaseMsg * generalBaseMsg)
{
	if (UIScene::init())
	{
		m_nCreateType = CT_withGeneralBaseMsg;
		m_pGeneralBaseMsg->CopyFrom(*generalBaseMsg);

		auto generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];

		//�߿
		frame = ImageView::create();
		frame->loadTexture(getBigHeadFramePath(generalBaseMsg->currentquality()).c_str());
		frame->setAnchorPoint(Vec2::ZERO);
		frame->setPosition(Vec2(0,0));
		frame->setScale(M_Frame_Scale);
		m_pLayer->addChild(frame);

		//�ͷ�
		std::string icon_path = "res_ui/general/";
		icon_path.append(generalMsgFromDb->get_half_photo());
		icon_path.append(".png");
		imageView_head = ImageView::create();
		imageView_head->loadTexture(icon_path.c_str());
		imageView_head->setAnchorPoint(Vec2(0.5f,0));
		imageView_head->setPosition(Vec2(frame->getContentSize().width/2,64));
		m_pLayer->addChild(imageView_head);

		//����
		label_name = Label::createWithTTF(generalMsgFromDb->name().c_str(), APP_FONT_NAME, 20);
		label_name->setAnchorPoint(Vec2(0.5f,0.5f));
		label_name->setAnchorPoint(Vec2(0.5f,0.5f));
		label_name->setPosition(Vec2(frame->getContentSize().width/2,42));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		m_pLayer->addChild(label_name);

		//�ϡ�ж
		if (generalBaseMsg->rare()>0)
		{
			imageView_star = ImageView::create();
			imageView_star->loadTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(Vec2::ZERO);
			imageView_star->setScale(0.8f);
			imageView_star->setPosition(Vec2(6,27));
			imageView_star->setName("ImageView_Star");
			m_pLayer->addChild(imageView_star);

			if (generalBaseMsg->rare() >= 5)
			{
				// load animation
				std::string animFileName = "animation/ui/wj/wj2.anm";
				auto m_pAnim = CCLegendAnimation::create(animFileName);
				m_pAnim->setPlayLoop(true);
				m_pAnim->setReleaseWhenStop(false);
				m_pAnim->setScale(.8f);
				m_pAnim->setPosition(Vec2(6,27));
				m_pAnim->setPlaySpeed(.35f);
				m_pAnim->setTag(KTag_FiveStar_Anm);
				m_pLayer->addChild(m_pAnim);
			}
		}

		this->setContentSize(Size(frame->getContentSize().width*M_Frame_Scale,frame->getContentSize().height*M_Frame_Scale));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(ShowBigGeneralsHead::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(ShowBigGeneralsHead::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(ShowBigGeneralsHead::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(ShowBigGeneralsHead::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

bool ShowBigGeneralsHead::init( int generalModleId )
{
	if (UIScene::init())
	{
		m_nCreateType = CT_withModleId;
		m_nGeneralModleId = generalModleId;

		auto generalBaseMsg = GeneralsConfigData::s_generalsBaseMsgData[generalModleId];
		//ȱ߿
		frame = ImageView::create();
		frame->loadTexture(getBigHeadFramePath(1).c_str());    //�Ĭ���1Ǽ���1-10Ϊ��ɫ��
		frame->setAnchorPoint(Vec2::ZERO);
		frame->setPosition(Vec2(0,0));
		frame->setScale(M_Frame_Scale);
		m_pLayer->addChild(frame);

		//���
		label_name = Label::createWithTTF(generalBaseMsg->name().c_str(), APP_FONT_NAME, 20);
		label_name->setAnchorPoint(Vec2(0.5f,0.5f));
		label_name->setAnchorPoint(Vec2(0.5f,0.5f));
		label_name->setPosition(Vec2(frame->getContentSize().width/2,42));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(1));
		m_pLayer->addChild(label_name);
		//�ͷ�
		std::string icon_path = "res_ui/general/";
		icon_path.append(generalBaseMsg->get_half_photo());
		//icon_path.append("guanyu");
		icon_path.append(".png");
		imageView_head = ImageView::create();
		imageView_head->loadTexture(icon_path.c_str());
		imageView_head->setAnchorPoint(Vec2(0.5f,0));
		imageView_head->setPosition(Vec2(frame->getContentSize().width/2,64));
		m_pLayer->addChild(imageView_head);

		//�ϡ�ж
		if (generalBaseMsg->rare()>0)
		{
			imageView_star = ImageView::create();
			imageView_star->loadTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(Vec2::ZERO);
			imageView_star->setScale(1.0f);
			imageView_star->setPosition(Vec2(15,63));
			imageView_star->setName("ImageView_Star");
			m_pLayer->addChild(imageView_star);

			if (generalBaseMsg->rare() >= 5)
			{
				// load animation
				std::string animFileName = "animation/ui/wj/wj2.anm";
				auto m_pAnim = CCLegendAnimation::create(animFileName);
				m_pAnim->setPlayLoop(true);
				m_pAnim->setReleaseWhenStop(true);
				m_pAnim->setScale(1.0f);
				m_pAnim->setPosition(Vec2(15,63));
				m_pAnim->setPlaySpeed(.35f);
				m_pAnim->setTag(KTag_FiveStar_Anm);
				m_pLayer->addChild(m_pAnim);
			}
		}

		this->setContentSize(Size(frame->getContentSize().width*M_Frame_Scale,frame->getContentSize().height*M_Frame_Scale));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}

bool ShowBigGeneralsHead::init( int generalModleId,int currentquality )
{
	if (UIScene::init())
	{
		m_nCreateType = CT_withModleIdAndQuality;
		m_nGeneralModleId = generalModleId;
		m_nCurrentquality = currentquality;

		auto generalBaseMsg = GeneralsConfigData::s_generalsBaseMsgData[generalModleId];
		//ȱ߿
		frame = ImageView::create();
		frame->loadTexture(getBigHeadFramePath(currentquality).c_str());    //1-10�Ϊ��ɫ��
		frame->setAnchorPoint(Vec2::ZERO);
		frame->setPosition(Vec2(0,0));
		frame->setScale(M_Frame_Scale);
		m_pLayer->addChild(frame);

		//���
		label_name = Label::createWithTTF(generalBaseMsg->name().c_str(), APP_FONT_NAME, 20);
		label_name->setAnchorPoint(Vec2(0.5f,0.5f));
		label_name->setPosition(Vec2(frame->getContentSize().width/2,42));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(1));
		m_pLayer->addChild(label_name);
		//�ͷ�
 		std::string icon_path = "res_ui/general/";
 		icon_path.append(generalBaseMsg->get_half_photo());
 		//icon_path.append("guanyu");
 		icon_path.append(".png");
 		imageView_head = ImageView::create();
 		imageView_head->loadTexture(icon_path.c_str());
 		imageView_head->setAnchorPoint(Vec2(0.5f,0));
 		imageView_head->setPosition(Vec2(frame->getContentSize().width/2,64));
 		m_pLayer->addChild(imageView_head);

		//�ϡ�ж
		if (generalBaseMsg->rare()>0)
		{
			imageView_star = ImageView::create();
			imageView_star->loadTexture(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(Vec2::ZERO);
			imageView_star->setScale(1.0f);
			imageView_star->setPosition(Vec2(15,63));
			imageView_star->setName("ImageView_Star");
			m_pLayer->addChild(imageView_star);

			if (generalBaseMsg->rare() >= 5)
			{
				// load animation
				std::string animFileName = "animation/ui/wj/wj2.anm";
				auto m_pAnim = CCLegendAnimation::create(animFileName);
				m_pAnim->setPlayLoop(true);
				m_pAnim->setReleaseWhenStop(true);
				m_pAnim->setScale(1.0f);
				m_pAnim->setPosition(Vec2(15,63));
				m_pAnim->setPlaySpeed(.35f);
				m_pAnim->setTag(KTag_FiveStar_Anm);
				m_pLayer->addChild(m_pAnim);
			}
		}

		this->setContentSize(Size(frame->getContentSize().width*M_Frame_Scale,frame->getContentSize().height*M_Frame_Scale));
		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}

bool ShowBigGeneralsHead::onTouchBegan( Touch *touch, Event * pEvent )
{
	if (this->getActionByTag(kTagBeginAnm))
	{
		return true;
	}

	return resignFirstResponder(touch,this,false);
}

void ShowBigGeneralsHead::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void ShowBigGeneralsHead::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void ShowBigGeneralsHead::onTouchMoved( Touch *touch, Event * pEvent )
{

}


std::string ShowBigGeneralsHead::getBigHeadFramePath( int quality )
{
	std::string frameNameBase ;
	if (quality < 11)                                                                //Ȱ�ɫ
	{
		frameNameBase = BigGeneralsFramePath_White;
	}
	else if (quality>10 && quality < 21)                              //��ɫ
	{
		frameNameBase = BigGeneralsFramePath_Green;
	}
	else if (quality>20 && quality < 31)                              //�ɫ
	{
		frameNameBase = BigGeneralsFramePath_Blue;
	}
	else if (quality>30 && quality < 41)                              //��ɫ
	{
		frameNameBase = BigGeneralsFramePath_Purpe;
	}
	else if (quality>40 && quality < 51)                              //��ɫ
	{
		frameNameBase = BigGeneralsFramePath_Orange;
	}
	else
	{
		frameNameBase = BigGeneralsFramePath_White;
	}
	return frameNameBase;
}

void ShowBigGeneralsHead::showBeginAction()
{
	auto winSize = Director::getInstance()->getVisibleSize();
	//test 1
// 	this->setPosition(Vec2(-this->getContentSize().width,winSize.height/2));
// 	ActionInterval*  actionTo = CCJumpTo::create(1.5f, Vec2(winSize.width/2,winSize.height/2), 60, 3);
// 	this->runAction(actionTo);
// 	actionTo->setTag(kTagBeginAnm);
	//test 2
	auto shuffle = CCShuffleTiles::create(1.5, Size(16,12), 15);
	auto shuffle_back = shuffle->reverse();
	auto delay = DelayTime::create(.001f);
	auto sequence = Sequence::create(shuffle, delay, shuffle_back, NULL);
	this->runAction(sequence);
	sequence->setTag(kTagBeginAnm);
}
