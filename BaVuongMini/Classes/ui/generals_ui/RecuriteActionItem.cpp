#include "RecuriteActionItem.h"
#include "../../messageclient/element/CActionDetail.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"
#include "../../GameView.h"


RecuriteActionItem::RecuriteActionItem():
lastChangeTime(0)
{
}


RecuriteActionItem::~RecuriteActionItem()
{
	delete curActionDetail;
}

RecuriteActionItem * RecuriteActionItem::create(CActionDetail * actionDetail)
{
	auto recuriteActionItem = new RecuriteActionItem();
	if (recuriteActionItem && recuriteActionItem->init(actionDetail))
	{
		recuriteActionItem->autorelease();
		return recuriteActionItem;
	}
	CC_SAFE_DELETE(recuriteActionItem);
	return NULL;
}

bool RecuriteActionItem::init(CActionDetail * actionDetail)
{
	if (UIScene::init())
	{
		curActionDetail = new CActionDetail();
		curActionDetail->CopyFrom(*actionDetail);
		//this->lastChangeTime = GameUtils::millisecondNow();
		cdTime =  actionDetail->cdtime();
		/*m_fRecordTime = m_fTime =   GameUtils::millisecondNow() - actionDetail->cdtime();*/
		playerRemainNumber = curActionDetail->playerremainnumber();

	    retainImage = ImageView::create();
		retainImage->loadTexture("res_ui/wujiang/jin.png");
		retainImage->setAnchorPoint(Vec2(0,0));
		retainImage->setPosition(Vec2(18,62));
		m_pLayer->addChild(retainImage);
		label_num = Label::createWithTTF("2/5", APP_FONT_NAME, 14);
		label_num->setColor(Color3B(47,93,13));
		label_num->setAnchorPoint(Vec2(0.5f,0.5f));
		label_num->setPosition(Vec2(179,72));
		m_pLayer->addChild(label_num);

		freeImage = ImageView::create();
		freeImage->loadTexture("res_ui/wujiang/ben.png");
		freeImage->setAnchorPoint(Vec2(0,0));
		freeImage->setPosition(Vec2(59,62));
		m_pLayer->addChild(freeImage);

	    label_time = Label::createWithTTF("12:25:45", APP_FONT_NAME, 14);
		label_time->setColor(Color3B(47,93,13));
		label_time->setAnchorPoint(Vec2(0.5f,0.5f));
		label_time->setPosition(Vec2(75,84));
		m_pLayer->addChild(label_time);
		freeBefore = ImageView::create();
		freeBefore->loadTexture("res_ui/wujiang/hou.png");
		freeBefore->setAnchorPoint(Vec2(0,0));
		freeBefore->setPosition(Vec2(125,73));
		m_pLayer->addChild(freeBefore);
		image_gold = ImageView::create();
		image_gold->loadTexture("res_ui/ingot.png");
		image_gold->setAnchorPoint(Vec2(0,0));
		image_gold->setPosition(Vec2(82,52));
		m_pLayer->addChild(image_gold);
		
		char s_gold[20];
		sprintf(s_gold,"%d",actionDetail->gold());
		label_gold = Label::createWithTTF(s_gold, APP_FONT_NAME, 14);
		label_gold->setColor(Color3B(47,93,13));
		label_gold->setAnchorPoint(Vec2(0,0));
		label_gold->setPosition(Vec2(121,55));
		m_pLayer->addChild(label_gold);

		image_noFreeTime = ImageView::create();
		image_noFreeTime->loadTexture("res_ui/wujiang/jina.png");
		image_noFreeTime->setAnchorPoint(Vec2(0,0));
		image_noFreeTime->setPosition(Vec2(40,73));
		m_pLayer->addChild(image_noFreeTime);

		retainImage->setVisible(false);
		label_num->setVisible(false);
		freeImage->setVisible(false);
		label_time->setVisible(false);
		freeBefore->setVisible(false);
		image_gold->setVisible(false);
		label_gold->setVisible(false);
		image_noFreeTime->setVisible(false);

		if (actionDetail->show1rare()>0)
		{
			auto star_normal = ImageView::create();
			star_normal->loadTexture(getStarPathByNum(actionDetail->show1rare()).c_str());
			star_normal->setPosition(Vec2(71,125));
			m_pLayer->addChild(star_normal);
		}

		if (actionDetail->show2rare()>0)
		{
			auto star_good = ImageView::create();
			star_good->loadTexture(getStarPathByNum(actionDetail->show2rare()).c_str());
			star_good->setPosition(Vec2(120,125));
			m_pLayer->addChild(star_good);
		}

		if (actionDetail->show3rare()>0)
		{
			auto star_wonderful = ImageView::create();
			star_wonderful->loadTexture(getStarPathByNum(actionDetail->show3rare()).c_str());
			star_wonderful->setPosition(Vec2(171,125));
			m_pLayer->addChild(star_wonderful);
		}

		this->setContentSize(Size(215,207));

		refreshData();
		this->schedule(schedule_selector(RecuriteActionItem::update),1.0f);
		return true;
	}
	return false;
}

void RecuriteActionItem::update( float dt )
{
// 	    cdTime -= 1000;
// 		m_fRecordTime = cdTime;
// 		if (m_fRecordTime < 0)
// 		{
// 			m_fRecordTime = 0;
// 		}

		refreshData();
}

void RecuriteActionItem::refreshData()
{
	auto mainscene_ =(MainScene *)GameView::getInstance()->getMainUIScene();
	for(int i = 0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		auto tempActionDetail = GameView::getInstance()->actionDetailList.at(i);
		if (tempActionDetail->type() == curActionDetail->type())
		{
			if (tempActionDetail->type() == BASE)
			{
				actionMaxNumber = tempActionDetail->actionmaxnumber();
				playerRemainNumber = tempActionDetail->playerremainnumber();
				std::string str_des = "";
				char s_playerRemainNumber[10];
				sprintf(s_playerRemainNumber,"%d",tempActionDetail->playerremainnumber());
				str_des.append(s_playerRemainNumber);
				str_des.append("/");
				char s_actionMaxNumber[10];
				sprintf(s_actionMaxNumber,"%d",actionMaxNumber);
				str_des.append(s_actionMaxNumber);
				label_num->setString(str_des.c_str());

				if (playerRemainNumber>0)   /********�Nڴ���ѳ********/
				{
					if (tempActionDetail->cdtime() > 0)   /********黨��Ԫ���********/
					{
						state = Gold;
						retainImage->setVisible(false);
						label_num->setVisible(false);
						freeImage->setVisible(false);
						label_time->setVisible(true);
						label_time->setString(timeFormatToString(tempActionDetail->cdtime()/1000).c_str());
						freeBefore->setVisible(true);
						image_gold->setVisible(true);
						label_gold->setVisible(true);
						image_noFreeTime->setVisible(false);
					}
					else
					{
						state = Free;
						retainImage->setVisible(true);
						label_num->setVisible(true);
						freeImage->setVisible(false);
						label_time->setVisible(false);
						freeBefore->setVisible(false);
						image_gold->setVisible(false);
						label_gold->setVisible(false);
						image_noFreeTime->setVisible(false);
					}
				}
				else/********黨��Ԫ���********/
				{
					state = Gold;
					retainImage->setVisible(false);
					label_num->setVisible(false);
					freeImage->setVisible(false);
					label_time->setVisible(false);
					freeBefore->setVisible(false);
					image_gold->setVisible(true);
					label_gold->setVisible(true);
					image_noFreeTime->setVisible(true);
				}
			}
			else
			{
				if (tempActionDetail->cdtime() > 0)   /********黨��Ԫ���********/
				{
					state = Gold;
					retainImage->setVisible(false);
					label_num->setVisible(false);
					freeImage->setVisible(false);
					label_time->setVisible(true);
					label_time->setString(timeFormatToString(tempActionDetail->cdtime()/1000).c_str());
					freeBefore->setVisible(true);
					image_gold->setVisible(true);
					label_gold->setVisible(true);
					image_noFreeTime->setVisible(false);
				}
				else
				{
					state = Free;                                                   /********���ѳ1�********/
					retainImage->setVisible(false);
					label_num->setVisible(false);
					freeImage->setVisible(true);
					label_time->setVisible(false);
					freeBefore->setVisible(false);
					image_gold->setVisible(false);
					label_gold->setVisible(false);
					image_noFreeTime->setVisible(false);
				}
			}
		}
	}
 
}

std::string RecuriteActionItem::timeFormatToString( long long t )
{
	int int_h = t/60/60;
	int int_m = (t%3600)/60;
	int int_s = (t%3600)%60;

	//�ʱ
	std::string timeString = "";
	char str_h[10];
	sprintf(str_h,"%d",int_h);
	if (int_h <= 0)
	{

	}
	else if (int_h > 0 && int_h < 10)
	{
		timeString.append("0");
		timeString.append(str_h);
		timeString.append(":");
	}
	else
	{
		timeString.append(str_h);
		timeString.append(":");
	}
	
	//�
	char str_m[10];
	sprintf(str_m,"%d",int_m);
	if (int_m >= 0 && int_m < 10)
	{
		timeString.append("0");
		timeString.append(str_m);
	}
	else
	{
		timeString.append(str_m);
	}
	timeString.append(":");

	char str_s[10];
	sprintf(str_s,"%d",int_s);
	if (int_s >= 0 && int_s < 10)
	{
		timeString.append("0");
		timeString.append(str_s);
	}
	else
	{
		timeString.append(str_s);
	}

	return timeString;
}

std::string RecuriteActionItem::getStarPathByNum( int num )
{
	std::string path;
	if (num == 1)
	{
		path = "res_ui/wujiang/star_one.png";
	}
	else if (num == 2)
	{
		path = "res_ui/wujiang/star_two.png";
	}
	else if (num == 3)
	{
		path = "res_ui/wujiang/star_three.png";
	}
	else if (num == 4)
	{
		path = "res_ui/wujiang/star_four.png";
	}
	else if (num == 5)
	{
		path = "res_ui/wujiang/star_five.png";
	}
	else
	{
		path = "res_ui/wujiang/star_one.png";
	}

	return path;
}
