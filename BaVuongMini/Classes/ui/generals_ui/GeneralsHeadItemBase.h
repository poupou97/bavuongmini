
#ifndef _GENERALSUI_GENERALSHEADITEM_H_
#define _GENERALSUI_GENERALSHEADITEM_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;
class CCLegendAnimation;

/////////////////////////////////
/**
 * �佫�����µ ��佫ͷ����
 * �ͷ�����֣�ϡ�ж
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.4
 */

class CGeneralBaseMsg;
class CGeneralDetail;

class GeneralsHeadItemBase :public UIScene
{
public:
	GeneralsHeadItemBase();
	~GeneralsHeadItemBase();

	enum CreateType
	{
		CT_withGeneralBaseMsg = 0,
		CT_withModleId,
		CT_withModleIdAndQuality,
	};

	virtual void onEnter();
	virtual void onExit();

	static GeneralsHeadItemBase * create(CGeneralBaseMsg * generalBaseMsg);
	static GeneralsHeadItemBase * create(int generalModleId);
	static GeneralsHeadItemBase * create(int generalModleId,int currentquality);
	bool init(CGeneralBaseMsg * generalBaseMsg);
	bool init(int generalModleId);
	bool init(int generalModleId,int currentquality);

	//ȼ�ӰЧ�
	void showSilhouetteEffect();
	void removeSilhouetteEffect();

	//������Ƿ����ͨ����չʾ���
	void setCanShowBigCard(bool isCan);

	CCLegendAnimation * getLegendHead();
	CCLegendAnimation * getFiveStarAnm();

public:
	Button * frame;
	ImageView * black_namebg;
	Label * label_name;
	
	ImageView * imageView_star;
	ImageView * imageView_prefession;

	//Ƴ�׿��ͷ��ʣ��Ķ��ӵ������
	Layer * layer_up;

	int m_nCreateType;

	CGeneralBaseMsg * m_pGeneralBaseMsg;
	int m_nGeneralModleId;
	int m_nCurrentquality;

	void FrameEvent(Ref *pSender, Widget::TouchEventType type);

private:
	CCLegendAnimation * la_head;
};

#endif