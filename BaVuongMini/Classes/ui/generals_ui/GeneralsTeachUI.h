#ifndef  _GENERALSUI_GENERALSTEACHUI_H_
#define _GENERALSUI_GENERALSTEACHUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "./GeneralsListBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGeneralBaseMsg;
class CGeneralDetail;
class NormalGeneralsHeadItemBase;

/////////////////////////////////
/**
 * �佫�����µ ��佫�����ࣨ�̳���ͬ���ƣ�
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.4
 */

class GeneralsTeachUI : public GeneralsListBase,public TableViewDelegate,public TableViewDataSource
{
public:
	GeneralsTeachUI();
	~GeneralsTeachUI();

	struct headSlotInfo{
		bool isHave;
		Vec2 loc;
		int tag;
	};

	static GeneralsTeachUI* create(CGeneralBaseMsg * generalsBaseMsg);
	bool init(CGeneralBaseMsg * generalsBaseMsg);

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	headSlotInfo* getEmptyItem();
	//��Ƿ��Ѿ���ӵ�����λ�
	bool isAddedToTeach(long long _id);
	void addToTeach(CGeneralBaseMsg * generalsBaseMsg,headSlotInfo* headslotinfo);

	void TeachEvent(Ref *pSender, Widget::TouchEventType type);
	//�һ�
	void ConvenientTeachEvent(Ref *pSender, Widget::TouchEventType type);
	void ResetEvent(Ref *pSender, Widget::TouchEventType type);
	void BackEvent(Ref *pSender, Widget::TouchEventType type);

	void BeginTeachAnm();

	//������ߵ�ͷ��Ȼ����ã�����һ����
	void ProgressToFinishCall();
	//�����û�ߵ�ͷ��Ȼ����ã�û����һ����
	void ProgressNotFinishCall();
	void ResetCallFucTime();
	//��������һ���ߵ�ͷ����ʱ��Ҫ����Ԥ����ĳ��
	void FinalProgressToFinishCall(Node * pNode,void * scale);

private:
	ui::Layout * panel_teach; //ȴ�����
	Layer * Layer_teach;
	Layer * Layer_upper;

	headSlotInfo* headSlotInfo1;
	headSlotInfo* headSlotInfo2;
	headSlotInfo* headSlotInfo3;
	headSlotInfo* headSlotInfo4;

	TableView * generalList_tableView;	

	Text * l_curTeachExp;
	Text * l_constValue;

	Label * l_CapsExp_value;
	ProgressTimer *sp_exp_cur;
	ProgressTimer *sp_exp_preView;

	//���ProgressToFinishCallõĴ��
	int m_nCallProgressFunctionTimes;

	std::string nextTeachValue;

	//�Ԥ������ĵȼ�
	int m_nPreViewUpgradeLevel;


	/////////////////////PreView Info////////////////////////////
	//int m_nPreViewLevel;
	//int m_nPreViewExp;

	Text * lbf_hp;
	Text * lbf_max_attack;
	Text * lbf_max_magic_attack;
	Text * l_hpValue_cur;
	Text * l_hpValue_next;
	Text * l_attackValue_cur;
	Text * l_attackValue_next;

	ImageView * image_jiantou_first;
	ImageView * image_jiantou_second;
	//////////////////////////////////////////////////////////////

	ImageView * ImageView_MainGeneralFrame;

public:
	long long curTeachGeneralId ;
	CGeneralBaseMsg * curTeachGeneral;
	CGeneralBaseMsg * oldTeachGeneral;
	CGeneralDetail * curTeachGeneralDetail;
	CGeneralDetail * oldTeachGeneralDetail;
	std::vector<CGeneralBaseMsg *> generalsTeachList;

	CGeneralBaseMsg * curVictimGeneralBaseMsg;

	int curExpValue;
	int costGoldValue;

	//����������Ʒ������ĸ
	std::vector<long long> victimList;

	//�ˢ���佫�б
	void RefreshGeneralsList();
	//�ˢ���佫�б(tableView ��ƫ������)
	void RefreshGeneralsListWithOutChangeOffSet();

	void RefreshGeneralsAddedPropertyCur(CGeneralDetail * generalDetail);
	void RefreshGeneralsAddedPropertyPreView();

	void setGeneralsAddedPropertyToDefault(CGeneralDetail * generalDetail);

	void createTeachHeadItem(CGeneralBaseMsg * generalsBaseMsg);
	void refreshTeachHeadItem(CGeneralBaseMsg * generalsBaseMsg);
	void refreshEvolutionHeadFrame(CGeneralBaseMsg * generalsBaseMsg);
	//䴫����ɺ󣬰ѽ�����Ϊ��ʼ
	void setToDefault();

	virtual bool isVisible();
	virtual void setVisible(bool visible);

	void AddBonusSpecialEffect();

	//add AptitudePopupEffect
	void AddAptitudePopupEffect();
};

#endif

