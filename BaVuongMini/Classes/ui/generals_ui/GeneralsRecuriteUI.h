#ifndef  _GENERALSUI_GENERALSRECURITEUI_H_
#define _GENERALSUI_GENERALSRECURITEUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "GeneralsListBase.h"
#include "../extensions/UIScene.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;
class CGeneralBaseMsg;
class CGeneralDetail;
class CActionDetail;

/////////////////////////////////
/**
 * �佫�����µġ��㽫̨���е���ļ�
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.9
 */

class GeneralsRecuriteUI : public UIScene
{
public:
	GeneralsRecuriteUI();
	~GeneralsRecuriteUI();

	struct ReqData{
		int actionType;
		int recruitType;
	};

	enum RecuriteActionType{
		BASE = 1,
		BETTER = 2,
		BEST = 3,
		TEN_TIMES = 4,
	};

	
	static GeneralsRecuriteUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

public:
	void CloseEvent(Ref *pSender);
	void NormalRecuritEvent(Ref *pSender, Widget::TouchEventType type);
	void GoodRecuritEvent(Ref *pSender, Widget::TouchEventType type);
	void WonderfulRecuritEvent(Ref *pSender, Widget::TouchEventType type);
	void TenTimesRecuritEvent(Ref *pSender, Widget::TouchEventType type);

	void NullGoodEvent(Ref *pSender, Widget::TouchEventType type);
	void NullBetterEvent(Ref *pSender, Widget::TouchEventType type);
	void NullBestEvent(Ref *pSender, Widget::TouchEventType type);

	void createFiveBestGeneral();   /**  ഴ�����͹ݵ�����ţ�ƿ��  **/
	void RefreshRecruitData();       /**  �ˢ�����͹ݵ�������ļ�Ļ���Ϣ  **/
	void ReloadNormalType(CActionDetail * actionDetail);	

	//������ļ�ɹ�����ʾ
	void presentRecruiteResult(CGeneralDetail * generalDetail,int actionType ,int recruitGeneralCardType,bool isPresentBtn = true);
	//���
	void DetailInfoEvent(Ref *pSender);
	//����һ�
	void RecuriteAgainEvent(Ref *pSender);
	
	//�ʮ�����ļ�ɹ�����ʾ
	void presentTenTimesRecruiteResult(std::vector<CGeneralDetail *> temp,bool isPresentBtn = true);

	virtual void setVisible(bool visible);

	RecuriteActionType getCurRecuriteActionType();


	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();

public:
	Layout * panel_recruit; //��ļ��
	Layer * Layer_recruit;
	Layer *Layer_upper;

	//���ͨ��ļ
    Button * Button_normalRecurit;
	//������ļ
	Button * Button_goodRecurit;
	//��Ʒ��ļ
	Button * Button_wonderfulRecurit;
	//ʮ��é®
	Button * Button_tenTimesRecurit;

private:
	RecuriteActionType curActionType;

	//������ļ�ɹ����·��������Ϣ
	CGeneralDetail * m_pCurGeneralDetail;
	int m_nActionType;
	bool m_bIsPresentBtn;

	//��ѧ
	int mTutorialScriptInstanceId;
};


/////////////////////////////////
/**
 * ����ƣ������佫��ļ���棩
 * @author yangjun
 * @version 0.1.0
 * @date 2014.08.5
 */

class GeneralRecuritMarquee :public Node
{
public:
	GeneralRecuritMarquee();
	~GeneralRecuritMarquee();

	virtual void update(float dt);

	static GeneralRecuritMarquee * create(int width, int height);
	bool init(int width, int height);

private:
	Vec2 m_layerRecurite1Pos;
	Vec2 m_layerRecurite2Pos;
	int m_nWidth;
	int m_nHeight;

	Layer * l_recurite_1;
	Layer * l_recurite_2;
};


#endif

