
#ifndef _GENERALSUI_GENERALSSHORTCUTLAYER_H_
#define _GENERALSUI_GENERALSSHORTCUTLAYER_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "GeneralsSkillsUI.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CShortCut;
class CGeneralDetail;

class GeneralsShortcutLayer : public UIScene
{
private:
	Vec2 slotPos[5] ;

public:
	Layer * layer_shortcut;
	Layer * layer_musou;

	GeneralsSkillsUI::SkillTpye curSkillType;
	CGeneralDetail * curGeneralDetail;
	//��¼����һ����˫���
	std::string m_sLastMumouId;
	std::string m_sCurMumouId;
	//ܼ�¼����һ���佫Id
	long long m_nLastGeneralId;

public:
	GeneralsShortcutLayer();
	~GeneralsShortcutLayer();

	static GeneralsShortcutLayer * create(CGeneralDetail * generalDetail);
	bool init(CGeneralDetail * generalDetail);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void RefreshGeneralsShortcutSlot( CGeneralDetail * generalDetail );

	void setSkillTypeByGeneralsDetail( CGeneralDetail *generalsDetail );

	//ˢ���佫���(ܵ������)
	void RefreshOneShortcutSlot(long long generalsId,CShortCut * shortCut);

	//ˢ���佫���(����п���)
	void RefreshAllShortcutSlot(long long generalsId,std::vector<CShortCut*> shortCutList);

//	//��ʼ����˫���
// 	void InitMusouSkill(CGeneralDetail * generalDetail);
// 
// 	//�������˫���
// 	void AddMusouNormal(int index);
// 	void AddMusouWithAnimation(int index);


	//ܽ�ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();
	//��ѧ
	int mTutorialScriptInstanceId;

};

#endif

