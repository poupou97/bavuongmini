#ifndef _GENERALSUI_GENERALSEVOTUTIONUI_H_
#define _GENERALSUI_GENERALSEVOTUTIONUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "./GeneralsListBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGeneralBaseMsg;
class CGeneralDetail;

#define ImageView_MainGeneralFrame_White "res_ui/wujiang/di_white.png"
#define ImageView_MainGeneralFrame_Green "res_ui/wujiang/di_green.png"
#define ImageView_MainGeneralFrame_Blue "res_ui/wujiang/di_blue.png"
#define ImageView_MainGeneralFrame_Purple "res_ui/wujiang/di_purple.png"
#define ImageView_MainGeneralFrame_Orange "res_ui/wujiang/di_orange.png"

/////////////////////////////////
/**
 * �佫�����µ ��佫���
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.4
 */

class GeneralsEvolutionUI : public GeneralsListBase,public TableViewDelegate,public TableViewDataSource
{
public:
	GeneralsEvolutionUI();
	~GeneralsEvolutionUI();


	static GeneralsEvolutionUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//ദ������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

private:
	Layout * panel_evolution; //����
	Layer * Layer_evolution;
	Layer * Layer_upper;
	Button * btn_reset;   //����
	Button * btn_levelUp; //��
	Button * btn_teach;  //��

	ImageView * ImageView_MainGeneralFrame;

	Text * Label_CapsExp_value;
	ImageView * ImageView_exp_cur;
	ImageView * ImageView_exp_preView;

	Label * l_CapsExp_value;
	ProgressTimer *sp_exp_cur;
	ProgressTimer *sp_exp_preView;

	Text * Label_curStrenth;
	Text * Label_addStrenth;
	Text * Label_curDexterity;
	Text * Label_addDexterity;
	Text * Label_curIntelligence;
	Text * Label_addIntelligence;
	Text * Label_curFocus;
	Text * Label_addFocus;
	Text * L_constValue;

// 	int curStrenth;
// 	int nextStrenth;
// 	int curDexterity;
// 	int nextDexterity;
// 	int curIntelligence;
// 	int nextIntelligence;
// 	int curFocus;
// 	int nextFocus;

	TableView * generalList_tableView;	
	
	//��ǰ�����
	int curEvolutionNum;
	//�����������ٴο����
	int allEvolutionNum;

public:
	std::vector<CGeneralBaseMsg *> evolutionGeneralsList;

	long long curEvolutionGeneralId ;
	CGeneralBaseMsg * curEvolutionGeneralBaseMsg;
	CGeneralDetail * curEvolutionGeneralDetail;
	CGeneralDetail * oldEvolutionGeneralDetail;

	long long curVictimGeneralId;
	CGeneralBaseMsg * curGeneralBaseMsg;

	long long lastVictimGeneralId;

	//�ˢ���佫�б
	void RefreshGeneralsList();
	//�ˢ���佫�б(tableView ��ƫ������)
	void RefreshGeneralsListWithOutChangeOffSet();

	void BackEvent(Ref *pSender, Widget::TouchEventType type);
	void ResetEvent(Ref *pSender, Widget::TouchEventType type);
	void EvolutionEvent(Ref *pSender, Widget::TouchEventType type);
	void SureToEvolution(Ref *pSender);

	void createEvolutionHead(CGeneralBaseMsg * generalsBaseMsg);
	void refreshEvolutionHead(CGeneralBaseMsg * generalsBaseMsg,bool isUpgraded = false);
	void refreshEvolutionHeadFrame(CGeneralBaseMsg * generalsBaseMsg);
	void createVictimHead();
	//�ˢ�µ�ǰ�佫��������Ϣ�Լ���һ����������Ϣ
	void RefreshGeneralEvolutionInfo(CGeneralDetail *generalsDetail);
	//�����ˢ�
	void RefreshGeneralEvolutionProgress(CGeneralBaseMsg * generalsBaseMsg);
	//½����Ԥ�
	void RefreshGeneralEvolutionProgressPreView(CGeneralBaseMsg * generalsBaseMsg);
	//�����ˢ��ΪĬ��״̬
	void RefreshGeneralEvolutionProgressToDefault(CGeneralBaseMsg * generalsBaseMsg);

	void ProgressToFinishCall();

	virtual bool isVisible();
	virtual void setVisible(bool visible);

	//add bonusSpecialEffect
	void AddBonusSpecialEffect();

	//add AptitudePopupEffect
	void AddAptitudePopupEffect();
};

#endif

