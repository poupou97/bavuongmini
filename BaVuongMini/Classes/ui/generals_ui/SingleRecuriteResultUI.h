#ifndef  _GENERALSUI_SINGLERECURITERESULTUI_H_
#define _GENERALSUI_SINGLERECURITERESULTUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "GeneralsListBase.h"
#include "../extensions/UIScene.h"
#include "GeneralsSkillsUI.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGeneralBaseMsg;
class CGeneralDetail;
class CActionDetail;

/////////////////////////////////
/**
 * �佫�����µġ��㽫̨���еĵ�����ļ����
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.9
 */

class SingleRecuriteResultUI : public UIScene
{
public:
	SingleRecuriteResultUI();
	~SingleRecuriteResultUI();

	static SingleRecuriteResultUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	//෵�أ��ر���ļ�ɹ����棩
	void RecruiteBackEvent(Ref *pSender, Widget::TouchEventType type);
	//���һ�Σ�������ļ��
	void RecruiteAgainEvent(Ref *pSender, Widget::TouchEventType type);

	//ˢ�°�ť����ʾ��
	void RefreshBtnPresent(bool isPresent);
	//�ˢ����ļ�ɹ�����Ļ��
	void RefreshRecruiteCost(int actionType);
	//�ˢ����ļ�ɹ������������Ϣ
	void RefreshRecruiteResult(CGeneralDetail * generalDetail);
	//ˢ���佫Ե�
	void RefreshGeneralsFate(CGeneralDetail * generalDetail);
	//�ˢ���佫���
	void RefreshGeneralsSkill(CGeneralDetail * generalDetail);
	//�ˢ���佫ͷ�
	void RefreshGeneralsHead(CGeneralDetail * generalDetail);

	void FateEvent(Ref *pSender, Widget::TouchEventType type);

	void setSkillTypeByGeneralsDetail(CGeneralDetail *generalsDetail);

	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	//�رյ�ǰ���
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();

	void createParticleSys();

public: 
	Layout * panel_generalsInfo;//��佫��ϸ��Ϣ(���γɹ���ļ�)
	Layer * Layer_generalsInfo;

	Layout * panel_btn;

	Button * Btn_close;
	CGeneralDetail * curGeneralDetail;
private:
	Vec2 slotPos[5] ;

	Text *	l_result_profession; //�ְҵ
	Text *	l_result_combatPower ; //ս�
	Text *	l_result_hp ; //Ѫ�ֵ
	ImageView * Image_result_hp;
	Text *	l_result_mp ; //���ֵ
	ImageView * Image_result_mp;
	Text * l_result_powerValue;//��
	Text * l_result_aglieValue;  //��
	Text * l_result_intelligenceValue;  //����
	Text * l_result_focusValue;  //רע
	Text * l_result_phyAttackValue;
	Text * l_result_magicAttackValue;
	Text * l_result_phyDenValue;
	Text * l_result_magicDenValue;
	Text * l_result_hitValue;  //���
	Text * l_result_dodgeValue; //���
	Text * l_result_critValue;  //ܱ��
	Text * l_result_critDamageValue;//���˺�
	Text * l_result_atkSpeedValue;

	Button * btn_fate_1;
	Button * btn_fate_2;
	Button * btn_fate_3;
	Button * btn_fate_4;
	Button * btn_fate_5;
	Button * btn_fate_6;
	Text * l_fate_1;
	Text * l_fate_2;
	Text * l_fate_3;
	Text * l_fate_4;
	Text * l_fate_5;
	Text * l_fate_6;

	Text * lable_singleCost_gold;

	GeneralsSkillsUI::SkillTpye curSkillType;

private:
	//��ѧ
	int mTutorialScriptInstanceId;
};

#endif

