#ifndef  _GENERALSUI_GENERALSLISTUI_H_
#define _GENERALSUI_GENERALSLISTUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "GeneralsListBase.h"
#include "GeneralsSkillsUI.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGeneralBaseMsg;
class CGeneralDetail;
class CShortCut;
class GeneralsShortcutLayer;
class GeneralsListCell;

/////////////////////////////////
/**
 * �佫�����µġ��ҵ��佫���е��佫�б��
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.3
 */

class GeneralsListUI : public GeneralsListBase,public TableViewDelegate,public TableViewDataSource
{
public:
	GeneralsListUI();
	~GeneralsListUI();
	static GeneralsListUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//ദ������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	//�ˢ���佫�б
	void RefreshGeneralsList();
	//�ˢ���佫�б(tableView ��ƫ������)
	void RefreshGeneralsListWithOutChangeOffSet();

	void RefreshGeneralsLayerData();

	CGeneralBaseMsg* getCurGeneralBaseMsg();

	//�����������
	void addChatBubble(const char* text, float duration);

	//ݽ�ѧ
	std::string m_content;
	Vec2 m_pos;
	CCTutorialIndicator::Direction m_direction;
	int mTutorialScriptInstanceId;

	//��һ�ν�ѧ���ǵڶ�ν�ѧ
	bool isFirstTutorial;
	bool isSecondTutorial;
	//��ѧ��ĵ�һ��ť�����󣩻��ǵڶ��ť����ս��
	bool isFirstOrSecondMenu;
	//���/��ս
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction,bool firstTutorialOrSecond,bool firstMenuOrSecond);
	void addCCTutorialIndicator2(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction,bool firstOrSecond);
	void removeCCTutorialIndicator();
	virtual void registerScriptCommand(int scriptId);
	

private:
	Layout * panel_generalsList; //�佫�б���
public:
	Layer * Layer_generalsList;
	Layer * Layer_generalsTop;  //�λ���佫�б��֮�

	int lastSelectCellId;
	int selectCellId;

	bool isInFirstGeneralTutorial;
	/******************************************** ��佫�б���*********************************************/
private:

	Button *	Btn_evolution; //�
	Button *	Btn_teach ; //���
	Text *	l_hp ; //Ѫ�ֵ
	ui::ImageView * Image_hp;
	Text *	l_mp ; //���ֵ
	ui::ImageView * Image_mp;
	Text *	l_exp ; //����ֵ
	ui::ImageView * Image_exp;
	Text *	l_profession; //ְҵ
	Text *	l_combatPower ; //ս�
	Text *	l_inBattleTime ; //��սʱ�
	Text *	l_restTime ; //���ȴʱ�
	Text * l_powerValue;//���
	Text * l_aglieValue;  //��
	Text * l_intelligenceValue;  //����
	Text * l_focusValue;  //רע
	Text * l_phyAttackValue;
	Text * l_magicAttackValue;
	Text * l_phyDenValue;
	Text * l_magicDenValue;
	Text * l_hitValue;  //���
	Text * l_dodgeValue; //���
	Text * l_critValue;  //ܱ��
	Text * l_critDamageValue;//���˺�
	Text * l_atkSpeedValue;
	Button * Btn_show_general;	// չʾ
	Button * Btn_hide_general;	// �ջ

	Button * btn_fate_1;
	Button * btn_fate_2;
	Button * btn_fate_3;
	Button * btn_fate_4;
	Button * btn_fate_5;
	Button * btn_fate_6;
	Text * l_fate_1;
	Text * l_fate_2;
	Text * l_fate_3;
	Text * l_fate_4;
	Text * l_fate_5;
	Text * l_fate_6;

	void EvolutionEvent(Ref *pSender, Widget::TouchEventType type);
	void TeachEvent(Ref *pSender, Widget::TouchEventType type);
	void FateEvent(Ref *pSender, Widget::TouchEventType type);

	void ShowGeneralEvent(Ref *pSender, Widget::TouchEventType type);
	void HideGeneralEvent(Ref *pSender, Widget::TouchEventType type);

public:
	GeneralsListCell * firstCell;
	GeneralsListCell * secondCell;
	//void setSkillTypeByGeneralsDetail(CGeneralDetail *generalsDetail);
public:

	virtual bool isVisible();
	virtual void setVisible(bool visible);

	//�ˢ���佫��ϸ��Ϣ
	void RefreshGeneralsInfo(CGeneralDetail * generalDetail);
	//ˢ���佫Ե�
	void RefreshGeneralsFate(CGeneralDetail * generalDetail);
	//�ˢ���佫���
	void RefreshGeneralsSkill(CGeneralDetail * generalDetail);
	//�ˢ���佫���(ܵ������)
	void RefreshOneGeneralsSkillSlot(long long generalsId,CShortCut * shortCut);
	//ˢ���佫���(����п���)
	void RefreshAllGeneralsSkillSlot(int generalsId,std::vector<CShortCut*> shortcutList);
	//ˢ���佫�Ƿ���Խ
	void RefreshEvolutionEnabled(CGeneralDetail * generalDetail);

	// ��佫�б���Ϣ(�佫�б���)
	//std::vector<CGeneralBaseMsg * > generalBaseMsgList;

	TableView * generalList_tableView;

	CGeneralBaseMsg * curGeneralBaseMsg; 
	CGeneralDetail * curGeneralDetail;

	GeneralsShortcutLayer * generalsShortcutLayer;

};

#endif

