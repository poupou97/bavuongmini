#ifndef  _GENERALSUI_TEACHANDEVOLUTIONCELL_H_
#define _GENERALSUI_TEACHANDEVOLUTIONCELL_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../extensions/UIScene.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "./GeneralsListBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGeneralBaseMsg;
class CGeneralDetail;

class TeachAndEvolutionCell : public TableViewCell
{
public:
	TeachAndEvolutionCell();
	~TeachAndEvolutionCell();
	static TeachAndEvolutionCell* create(CGeneralBaseMsg * generalBaseMsg);
	bool init(CGeneralBaseMsg * generalBaseMsg);

	void refreshCell(CGeneralBaseMsg * generalBaseMsg);

	CGeneralBaseMsg* getCurGeneralBaseMsg();

	//�Ƿ��
	void setGray(bool able);

private:

private:
	CGeneralBaseMsg * curGeneralBaseMsg; 

	cocos2d::extension::Scale9Sprite * sprite_bigFrame;
	cocos2d::extension::Scale9Sprite * sprite_bigFrame_mengban;

	Sprite * sprite_icon;
	Label * label_lv;
	Label * label_name;
	Label * label_rank;
};

#endif

