
#ifndef _GENERALSUI_SHOWBIGGENERALSHEAD_H_
#define _GENERALSUI_SHOWBIGGENERALSHEAD_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CCLegendAnimation;

#define BigGeneralsFramePath_White    "res_ui/generals_dia.png"
#define BigGeneralsFramePath_Green   "res_ui/generals_diagreen.png"
#define BigGeneralsFramePath_Blue      "res_ui/generals_diablue.png"
#define BigGeneralsFramePath_Purpe    "res_ui/generals_diapurple.png"
#define BigGeneralsFramePath_Orange  "res_ui/generals_diaorange.png"

/////////////////////////////////
/**
 * �佫����չʾЧ�
 * @author yangjun
 * @version 0.1.0
 * @date 2014.8.7
 */

class CGeneralBaseMsg;
class CGeneralDetail;

class ShowBigGeneralsHead :public UIScene
{
public:
	ShowBigGeneralsHead();
	~ShowBigGeneralsHead();

	enum CreateType
	{
		CT_withGeneralBaseMsg = 0,
		CT_withModleId,
		CT_withModleIdAndQuality,
	};

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	static ShowBigGeneralsHead * create(CGeneralBaseMsg * generalBaseMsg);
	static ShowBigGeneralsHead * create(int generalModleId);
	static ShowBigGeneralsHead * create(int generalModleId,int currentquality);
	bool init(CGeneralBaseMsg * generalBaseMsg);
	bool init(int generalModleId);
	bool init(int generalModleId,int currentquality);

	std::string getBigHeadFramePath( int quality );

	//beginAction
	void showBeginAction();

public:
	ImageView * frame;
	ImageView * black_namebg;
	Label * label_name;
	ImageView * imageView_head;
	ImageView * imageView_star;

	int m_nCreateType;

	CGeneralBaseMsg * m_pGeneralBaseMsg;
	int m_nGeneralModleId;
	int m_nCurrentquality;
};

#endif