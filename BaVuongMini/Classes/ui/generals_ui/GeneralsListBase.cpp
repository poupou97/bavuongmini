#include "GeneralsListBase.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GeneralsUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"

GeneralsListBase::GeneralsListBase():
generalsListStatus(HaveNone),
everyPageNum(20),
s_mCurPage(0),
isReqNewly(false),
isCanReq(false)
{

}

GeneralsListBase::~GeneralsListBase()
{

}
GeneralsListBase* GeneralsListBase::create()
{
	auto generalsListBase = new GeneralsListBase();
	if (generalsListBase && generalsListBase->init())
	{
		generalsListBase->autorelease();
		return generalsListBase;
	}
	CC_SAFE_DELETE(generalsListBase);
	return NULL;
}

bool GeneralsListBase::init()
{
	if (UIScene::init())
	{

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(GeneralsListBase::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(GeneralsListBase::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(GeneralsListBase::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(GeneralsListBase::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
		return true;
	}
	return false;
}

void GeneralsListBase::onEnter()
{
	UIScene::onEnter();
}

void GeneralsListBase::onExit()
{
	UIScene::onExit();
}

bool GeneralsListBase::onTouchBegan(Touch *touch, Event * pEvent)
{
	return true;
}

void GeneralsListBase::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void GeneralsListBase::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void GeneralsListBase::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void GeneralsListBase::RefreshGeneralsListStatus( int page, int allPageNum )
{
	s_mCurPage = page;
	s_mAllPageNum = allPageNum;
	if (page == 0)
	{
		if (page < allPageNum-1)
		{
			this->generalsListStatus = HaveNext;
		}
		else
		{
			this->generalsListStatus = HaveNone;
		}
	}
	else
	{
		if (page < allPageNum-1)
		{
			this->generalsListStatus = HaveBoth;
		}
		else
		{
			this->generalsListStatus = HaveLast;
		}
	}
}

bool GeneralsListBase::getIsCanReq()
{
	return isCanReq;
}

void GeneralsListBase::setIsCanReq( bool _value )
{
	isCanReq = _value;
}
