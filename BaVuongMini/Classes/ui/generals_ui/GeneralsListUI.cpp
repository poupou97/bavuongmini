#include "GeneralsListUI.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "GeneralsUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../GameView.h"
#include "NormalGeneralsHeadItem.h"
#include "GeneralsTeachUI.h"
#include "GeneralsEvolutionUI.h"
#include "../../messageclient/protobuf/GeneralMessage.pb.h"
#include "generals_popup_ui/GeneralsFateInfoUI.h"
#include "../../gamescene_state/MainScene.h"
#include "GeneralsShortcutSlot.h"
#include "../../messageclient/element/CShortCut.h"
#include "generals_popup_ui/GeneralsSkillListUI.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "GeneralsShortcutLayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "AppMacros.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/GameUtils.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "../../gamescene_state/role/ShowBaseGeneral.h"

#define  SHORTCUTLAYERTAG 1000
#define  HeadItemTag  20
#define  GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG 456

//////////////////////////////////////////////////

GeneralsListUI::GeneralsListUI():
isFirstTutorial(false),
isSecondTutorial(false),
isFirstOrSecondMenu(true),
selectCellId(0),
lastSelectCellId(0),
isInFirstGeneralTutorial(false),
Btn_show_general(NULL),
Btn_hide_general(NULL)
{
// 	if (generalBaseMsgList.size()>0)
// 	{
// 		curGeneralBaseMsg->CopyFrom(*generalBaseMsgList.at(0));
// 	}
// 	else 
// 	{
// 	}

}

GeneralsListUI::~GeneralsListUI()
{
	delete curGeneralBaseMsg;
	delete curGeneralDetail;
}
GeneralsListUI* GeneralsListUI::create()
{
	auto generalsListUI = new GeneralsListUI();
	if (generalsListUI && generalsListUI->init())
	{
		generalsListUI->autorelease();
		return generalsListUI;
	}
	CC_SAFE_DELETE(generalsListUI);
	return NULL;
}

bool GeneralsListUI::init()
{
	if (UIScene::init())
	{
		curGeneralDetail = new CGeneralDetail();
		curGeneralBaseMsg  = new CGeneralBaseMsg();

		Size winsize = Director::getInstance()->getVisibleSize();

		//�佫�б
		panel_generalsList = (Layout*)Helper::seekWidgetByName(GeneralsUI::ppanel,"Panel_generals_detail");
		panel_generalsList->setVisible(true);
		//  ��佫�б�
		Layer_generalsList = Layer::create();
// 		Layer_generalsList->setIgnoreAnchorPointForPosition(false);
// 		Layer_generalsList->setAnchorPoint(Vec2(0.5f,0.5f));
// 		Layer_generalsList->setContentSize(Size(800, 480));
// 		Layer_generalsList->setPosition(Vec2(winsize.width/2,winsize.height/2));

		addChild(Layer_generalsList);
// 		generalsList = GeneralsListUI::create();
// 		Layer_generalsList->addChild(generalsList);
// 		Layer_generalsList->setVisible(false);
// 		
		auto pScrollView = (cocos2d::extension::ScrollView*)Helper::seekWidgetByName(panel_generalsList, "ScrollView_property");
		pScrollView->setBounceable(true);

		Btn_evolution = (Button*)Helper::seekWidgetByName(panel_generalsList,"Button_evolution"); //�
		Btn_evolution->setTouchEnabled(true);
		Btn_evolution->setPressedActionEnabled(true);
		Btn_evolution->addTouchEventListener(CC_CALLBACK_2(GeneralsListUI::EvolutionEvent, this));

		Btn_teach = (Button*)Helper::seekWidgetByName(panel_generalsList,"Button_teach"); //���
		Btn_teach->setTouchEnabled(true);
		Btn_teach->setPressedActionEnabled(true);
		Btn_teach->addTouchEventListener(CC_CALLBACK_2(GeneralsListUI::TeachEvent, this));

		Btn_show_general = (Button*)Helper::seekWidgetByName(panel_generalsList, "Button_show");	// չʾ
		Btn_show_general->setTouchEnabled(true);
		Btn_show_general->setPressedActionEnabled(true);
		Btn_show_general->addTouchEventListener(CC_CALLBACK_2(GeneralsListUI::ShowGeneralEvent, this));
		Btn_show_general->setVisible(true);

		Btn_hide_general = (Button*)Helper::seekWidgetByName(panel_generalsList, "Button_hide");	// �ջ
		Btn_hide_general->setTouchEnabled(true);
		Btn_hide_general->setPressedActionEnabled(true);
		Btn_hide_general->addTouchEventListener(CC_CALLBACK_2(GeneralsListUI::HideGeneralEvent, this));
		Btn_hide_general->setVisible(false);

		l_hp = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_hp"); //�Ѫ�ֵ
        l_hp->enableOutline(Color4B::BLACK, 2.0f);
		Image_hp = (ImageView* )Helper::seekWidgetByName(panel_generalsList,"ImageView_hp");
		l_mp = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_mp"); //���ֵ
        l_mp->enableOutline(Color4B::BLACK, 2.0f);
		Image_mp = (ImageView* )Helper::seekWidgetByName(panel_generalsList,"ImageView_mp");
		//l_exp = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_exp"); //����ֵ
        //l_exp->enableOutline(Color4B::BLACK, 2.0f);
		//Image_exp = (ImageView* )Helper::seekWidgetByName(panel_generalsList,"ImageView_exp");
		l_profession = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_job"); //ְҵ
		l_combatPower = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_job_combat"); //ս�
		l_inBattleTime = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_inBattleValue"); //��սʱ�
		l_restTime = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_restValue"); //���Ϣʱ�
		l_powerValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_power");
		l_aglieValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_aglie");  //���
		l_intelligenceValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_intelligence");  //����
		l_focusValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_focus");  //רע
		l_phyAttackValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_physical_attacks");
		l_magicAttackValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_magic_attacks");
		l_phyDenValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_power_physical_defense");
		l_magicDenValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_power_magic_defense");
		l_hitValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_hit");  //���
		l_dodgeValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_dodge"); //���
		l_critValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_crit"); //ܱ��
		l_critDamageValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_crit_damage"); //���˺�
		l_atkSpeedValue = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_attack_speed");

		btn_fate_1 = (Button*)Helper::seekWidgetByName(panel_generalsList,"Button_fate_1");
		btn_fate_1->setTouchEnabled(true);
		btn_fate_1->setPressedActionEnabled(true);
		btn_fate_1->addTouchEventListener(CC_CALLBACK_2(GeneralsListUI::FateEvent, this));
		btn_fate_2 = (Button*)Helper::seekWidgetByName(panel_generalsList,"Button_fate_2");
		btn_fate_2->setTouchEnabled(true);
		btn_fate_2->setPressedActionEnabled(true);
		btn_fate_2->addTouchEventListener(CC_CALLBACK_2(GeneralsListUI::FateEvent, this));
		btn_fate_3 = (Button*)Helper::seekWidgetByName(panel_generalsList,"Button_fate_3");
		btn_fate_3->setTouchEnabled(true);
		btn_fate_3->setPressedActionEnabled(true);
		btn_fate_3->addTouchEventListener(CC_CALLBACK_2(GeneralsListUI::FateEvent, this));
		btn_fate_4 = (Button*)Helper::seekWidgetByName(panel_generalsList,"Button_fate_6");
		btn_fate_4->setTouchEnabled(true);
		btn_fate_4->setPressedActionEnabled(true);
		btn_fate_4->addTouchEventListener(CC_CALLBACK_2(GeneralsListUI::FateEvent, this));
		btn_fate_5 = (Button*)Helper::seekWidgetByName(panel_generalsList,"Button_fate_5");
		btn_fate_5->setTouchEnabled(true);
		btn_fate_5->setPressedActionEnabled(true);
		btn_fate_5->addTouchEventListener(CC_CALLBACK_2(GeneralsListUI::FateEvent, this));
		btn_fate_6 = (Button*)Helper::seekWidgetByName(panel_generalsList,"Button_fate_4");
		btn_fate_6->setTouchEnabled(true);
		btn_fate_6->setPressedActionEnabled(true);
		btn_fate_6->addTouchEventListener(CC_CALLBACK_2(GeneralsListUI::FateEvent, this));

		l_fate_1 = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_fate_1");
		l_fate_1->setString("");
		l_fate_2 = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_fate_2");
		l_fate_2->setString("");
		l_fate_3 = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_fate_3");
		l_fate_3->setString("");
		l_fate_4 = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_fate_6");
		l_fate_4->setString("");
		l_fate_5 = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_fate_5");
		l_fate_5->setString("");
		l_fate_6 = (Text*)Helper::seekWidgetByName(panel_generalsList,"Label_fate_4");
		l_fate_6->setString("");

		this->generalsListStatus = HaveNext;
		//�佫�б
		generalList_tableView = TableView::create(this,Size(245,398));
		generalList_tableView->setDirection(TableView::Direction::VERTICAL);
		generalList_tableView->setAnchorPoint(Vec2(0,0));
		generalList_tableView->setPosition(Vec2(74,42));
		generalList_tableView->setContentOffset(Vec2(0,0));
		generalList_tableView->setDelegate(this);
		//generalList_tableView->setClippingToBounds(false);
		generalList_tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		Layer_generalsList->addChild(generalList_tableView);

		
		Layer_generalsTop = Layer::create();
// 		Layer_generalsTop->setIgnoreAnchorPointForPosition(false);
// 		Layer_generalsTop->setAnchorPoint(Vec2(0.5f,0.5f));
// 		Layer_generalsTop->setContentSize(Size(800, 480));
// 		Layer_generalsTop->setPosition(Vec2(winsize.width/2,winsize.height/2));

		addChild(Layer_generalsTop);

// 		ImageView * tableView_kuang = ImageView::create();
// 		tableView_kuang->loadTexture("res_ui/LV5_dikuang_miaobian1.png");
// 		tableView_kuang->setScale9Enabled(true);
// 		tableView_kuang->setContentSize(Size(260,414));
// 		tableView_kuang->setCapInsets(Rect(30,30,1,1));
// 		tableView_kuang->setAnchorPoint(Vec2(0,0));
// 		tableView_kuang->setPosition(Vec2(64,34));
// 		Layer_generalsTop->addChild(tableView_kuang);

		return true;
	}
	return false;
}

void GeneralsListUI::onEnter()
{
	GeneralsListBase::onEnter();
}

void GeneralsListUI::onExit()
{
	GeneralsListBase::onExit();
// 	std::vector<CGeneralBaseMsg*>::iterator iter_generalBaseMsgList;
// 	for (iter_generalBaseMsgList = generalBaseMsgList.begin(); iter_generalBaseMsgList != generalBaseMsgList.end(); ++iter_generalBaseMsgList)
// 	{
// 		delete *iter_generalBaseMsgList;
// 	}
// 	generalBaseMsgList.clear();
}

void GeneralsListUI::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void GeneralsListUI::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

void GeneralsListUI::tableCellTouched( TableView* table, TableViewCell* cell )
{
	int i = cell->getIdx();
	auto tempCell = dynamic_cast<GeneralsListCell*>(cell);
	if (!tempCell)
		return;
	
	curGeneralBaseMsg->CopyFrom(*tempCell->getCurGeneralBaseMsg());
	
	selectCellId = cell->getIdx();
	//�ȡ���ϴ�ѡ��״̬�����±���ѡ��״̬
	if(table->cellAtIndex(lastSelectCellId))
	{
		if (table->cellAtIndex(lastSelectCellId)->getChildByTag(GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG))
		{
			table->cellAtIndex(lastSelectCellId)->getChildByTag(GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
		}
	}

	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1) , "res_ui/highlight.png");
	pHighlightSpr->setPreferredSize(Size(235,72));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setTag(GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG);
	cell->addChild(pHighlightSpr);

	lastSelectCellId = cell->getIdx();

//	if (generalsListStatus == HaveNone)
//	{
		//������ϸ��Ϣ
		if (curGeneralBaseMsg != NULL)
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)curGeneralBaseMsg->id());
	//}
// 	else if (generalsListStatus == HaveLast)
// 	{
// 		if (i == 0)
// 		{
// 			//req for last
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage-1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 1;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//������ϸ��Ϣ
// 			if (curGeneralBaseMsg != NULL)
// 				GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)curGeneralBaseMsg->id());
// 		}
// 	}
// 	else if (generalsListStatus == HaveNext)
// 	{
// 		if (i == generalBaseMsgList.size())
// 		{
// 			//req for next
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage+1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 1;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//������ϸ��Ϣ
// 			if (curGeneralBaseMsg != NULL)
// 				GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)curGeneralBaseMsg->id());
// 		}
// 	}
// 	else if (generalsListStatus == HaveBoth)
// 	{
// 		if (i == 0)
// 		{
// 			//req for lase
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage-1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 1;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else if (i == generalBaseMsgList.size()+1)
// 		{
// 			//req for next
// 			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
// 			temp->page = this->s_mCurPage+1;
// 			temp->pageSize = this->everyPageNum;
// 			temp->type = 1;
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
// 			delete temp;
// 		}
// 		else
// 		{
// 			//������ϸ��Ϣ
// 			if (curGeneralBaseMsg != NULL)
// 				GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)curGeneralBaseMsg->id());
// 		}
// 	}

}

cocos2d::Size GeneralsListUI::tableCellSizeForIndex( TableView *table, unsigned int idx )
{
	return Size(235,74);
}

cocos2d::extension::TableViewCell* GeneralsListUI::tableCellAtIndex( TableView *table, ssize_t  idx )
{
	auto cell = table->dequeueCell();   // this method must be called
	if (!cell)
	{
		cell = GeneralsListCell::create(idx,GameView::getInstance()->generalBaseMsgList.at(idx)); 
	}
	else
	{
		auto tempCell = dynamic_cast<GeneralsListCell*>(cell);
		if (tempCell->getChildByTag(GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG))
		{
			tempCell->getChildByTag(GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
		}
		tempCell->RefreshCell(idx,GameView::getInstance()->generalBaseMsgList.at(idx));
	}

	if (idx == 0)
	{
		firstCell = dynamic_cast<GeneralsListCell*>(cell);
	}
	else if (idx == 1)
	{
		secondCell = dynamic_cast<GeneralsListCell*>(cell);
	}

	if(idx == GameView::getInstance()->generalBaseMsgList.size()-4)
	{
		if (generalsListStatus == HaveNext || generalsListStatus == HaveBoth)
		{
			if (getIsCanReq())
			{
				//req for next
				this->isReqNewly = false;
				auto temp = new GeneralsUI::ReqListParameter();
				temp->page = this->s_mCurPage+1;
				temp->pageSize = this->everyPageNum;
				temp->type = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
				setIsCanReq(false);
				delete temp;
			}
		}
	}

	if (this->selectCellId == idx)
	{
		auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1) , "res_ui/highlight.png");
		pHighlightSpr->setPreferredSize(Size(235,72));
		pHighlightSpr->setAnchorPoint(Vec2::ZERO);
		pHighlightSpr->setTag(GENERALSLISTUI_TABALEVIEW_HIGHLIGHT_TAG);
		cell->addChild(pHighlightSpr);
	}
	return cell;
}

ssize_t GeneralsListUI::numberOfCellsInTableView( TableView *table )
 {
	 return GameView::getInstance()->generalBaseMsgList.size();
}

void GeneralsListUI::RefreshGeneralsList()
{
	generalList_tableView->reloadData();
}


void GeneralsListUI::RefreshGeneralsListWithOutChangeOffSet()
{
	Vec2 _s = generalList_tableView->getContentOffset();
	int _h = generalList_tableView->getContentSize().height + _s.y;
	generalList_tableView->reloadData();
	Vec2 temp = Vec2(_s.x,_h - generalList_tableView->getContentSize().height);
	generalList_tableView->setContentOffset(temp); 
}


CGeneralBaseMsg* GeneralsListUI::getCurGeneralBaseMsg()
{
	return this->curGeneralBaseMsg;
}

void GeneralsListUI::RefreshGeneralsLayerData()
{
	if (Layer_generalsList->getChildByTag(HeadItemTag))
	{
		Layer_generalsList->getChildByTag(HeadItemTag)->removeFromParent();
	}

	NormalGeneralsHeadItemBase * headItem = NormalGeneralsHeadItemBase::create(curGeneralDetail);
	headItem->setAnchorPoint(Vec2(0,0));
	headItem->setPosition(Vec2(360,285));
	headItem->setTag(HeadItemTag);
	//headItem->setCanShowBigCard(true);
	Layer_generalsList->addChild(headItem);
}

//�
void GeneralsListUI::EvolutionEvent(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//���ȼ�
		int openlevel = 0;
		std::map<int, int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(3);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[3];
		}

		if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
		{
			if (curGeneralBaseMsg->has_id())
			{
				if (curGeneralBaseMsg->evolution() == 20)
				{
					GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_evolution_maxLevel"));
				}
				else
				{
					this->setVisible(false);
					GeneralsUI::generalsEvolutionUI->setVisible(true);

					GeneralsUI::generalsEvolutionUI->isReqNewly = true;
					GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
					temp->page = 0;
					temp->pageSize = GeneralsUI::generalsListUI->everyPageNum;
					temp->type = 3;
					temp->generalId = curGeneralBaseMsg->id();
					GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp);
					delete temp;


					GeneralsUI::generalsEvolutionUI->createEvolutionHead(curGeneralBaseMsg);
					GeneralsUI::generalsEvolutionUI->RefreshGeneralEvolutionInfo(curGeneralDetail);
				}
			}
			else
			{
				//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
				//const char *str1  = ((__String*)strings->objectForKey("generals_pleaseaddgenerals"))->m_sString.c_str();
				const char *str1 = StringDataManager::getString("generals_pleaseaddgenerals");
				char* p1 = const_cast<char*>(str1);
				GameView::getInstance()->showAlertDialog(p1);
			}
		}
		else
		{
			std::string str_des = StringDataManager::getString("feature_will_be_open_function");
			char str_level[20];
			sprintf(str_level, "%d", openlevel);
			str_des.append(str_level);
			str_des.append(StringDataManager::getString("feature_will_be_open_open"));
			GameView::getInstance()->showAlertDialog(str_des.c_str());
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}
//˴���
void GeneralsListUI::TeachEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//����ȼ�
		int openlevel = 0;
		std::map<int, int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(4);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[4];
		}

		if (openlevel <= GameView::getInstance()->myplayer->getActiveRole()->level())
		{
			if (curGeneralBaseMsg->has_id())
			{
				if (curGeneralBaseMsg->currentquality() == 50)
				{
					GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_teach_maxLevel"));
				}
				else
				{
					this->setVisible(false);
					GeneralsUI::generalsTeachUI->setVisible(true);

					GeneralsUI::generalsTeachUI->isReqNewly = true;
					GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
					temp->page = 0;
					temp->pageSize = GeneralsUI::generalsListUI->everyPageNum;
					temp->type = 2;
					temp->generalId = curGeneralBaseMsg->id();
					GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp);
					delete temp;

					GeneralsUI::generalsTeachUI->createTeachHeadItem(curGeneralBaseMsg);
					GeneralsUI::generalsTeachUI->setGeneralsAddedPropertyToDefault(curGeneralDetail);
					GeneralsUI::generalsTeachUI->ResetEvent(pSender,Widget::TouchEventType::ENDED);
				}
			}
			else
			{
				//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
				//const char *str1  = ((__String*)strings->objectForKey("generals_pleaseaddgenerals"))->m_sString.c_str();
				const char *str1 = StringDataManager::getString("generals_pleaseaddgenerals");
				char* p1 = const_cast<char*>(str1);
				GameView::getInstance()->showAlertDialog(p1);
			}
		}
		else
		{
			std::string str_des = StringDataManager::getString("feature_will_be_open_function");
			char str_level[20];
			sprintf(str_level, "%d", openlevel);
			str_des.append(str_level);
			str_des.append(StringDataManager::getString("feature_will_be_open_open"));
			GameView::getInstance()->showAlertDialog(str_des.c_str());
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsListUI::RefreshGeneralsInfo( CGeneralDetail * generalDetail )
{
	curGeneralDetail->CopyFrom(*generalDetail);
	//evolution
	curGeneralBaseMsg->set_evolution(curGeneralDetail->evolution());
	curGeneralBaseMsg->set_evolutionexp(curGeneralDetail->evolutionexp());
	//teach
	curGeneralBaseMsg->set_currentquality(curGeneralDetail->currentquality());
	curGeneralBaseMsg->set_qualityexp(curGeneralDetail->qualityexp());

	this->RefreshGeneralsLayerData();

	std::string hpValue = "";
	char s_hp[30];
	sprintf(s_hp,"%d",generalDetail->activerole().hp());
	hpValue.append(s_hp);
	hpValue.append("/");
	char s_maxhp[30];
	sprintf(s_maxhp,"%d",generalDetail->activerole().maxhp());
	hpValue.append(s_maxhp);
	l_hp->setString(hpValue.c_str());//�Ѫ�ֵ

	float hpScaleValue = (generalDetail->activerole().hp()*1.0f)/(generalDetail->activerole().maxhp()*1.0f);
	if (hpScaleValue > 1.0f )
		hpScaleValue = 1.0f;

	Image_hp->setScaleX(hpScaleValue*1.03f);

// 	std::string mpValue = "";
// 	char s_mp[30];
// 	sprintf(s_mp,"%d",generalDetail->activerole().mp());
// 	mpValue.append(s_mp);
// 	mpValue.append("/");
// 	char s_maxmp[30];
// 	sprintf(s_maxmp,"%d",generalDetail->activerole().maxmp());
// 	mpValue.append(s_maxmp);
// 	l_mp->setText(mpValue.c_str());//���ֵ
// 
// 	float mpScaleValue = (generalDetail->activerole().mp()*1.0f)/(generalDetail->activerole().maxmp()*1.0f);
// 	if (mpScaleValue > 1.0f )
// 		mpScaleValue = 1.0f;
// 
// 	Image_mp->setScaleX(mpScaleValue*1.03f);

// 	std::string expValue = "";
// 	char s_experience[30];
// 	sprintf(s_experience,"%d",generalDetail->experience());
// 	expValue.append(s_experience);
// 	expValue.append("/");
// 	char s_nextlevelexperience[30];
// 	sprintf(s_nextlevelexperience,"%d",generalDetail->nextlevelexperience());
// 	expValue.append(s_nextlevelexperience);
// 	l_exp->setText(expValue.c_str());   //����ֵ
// 
// 	float expScaleValue = (generalDetail->experience()*1.0f)/(generalDetail->nextlevelexperience()*1.0f);
// 	if (expScaleValue > 1.0f )
// 		expScaleValue = 1.0f;
// 
// 	Image_exp->setScaleX(expScaleValue*1.03f);

	l_profession->setString(generalDetail->profession().c_str()); //ְҵ

	char s_fightpoint[30];
	sprintf(s_fightpoint,"%d",generalDetail->fightpoint());
	l_combatPower->setString(s_fightpoint) ; //ս�

	const char *strings_s = StringDataManager::getString("generals_strategies_s");
	char strInbattleTime[20];
	sprintf(strInbattleTime,strings_s,generalDetail->showtimebase()/1000);
	l_inBattleTime->setString(strInbattleTime) ;

	char strRestTime[20];
	sprintf(strRestTime,strings_s,generalDetail->resttimebase()/1000);
	l_restTime->setString(strRestTime) ;

	char s_strength[30];
	sprintf(s_strength,"%d",generalDetail->aptitude().strength());
	l_powerValue->setString(s_strength) ; //��

	char s_dexterity[30];
	sprintf(s_dexterity,"%d",generalDetail->aptitude().dexterity());
	l_aglieValue->setString(s_dexterity) ;  //��

	char s_intelligence [30];
	sprintf(s_intelligence,"%d",generalDetail->aptitude().intelligence());
	l_intelligenceValue->setString(s_intelligence) ;  //����

	char s_focus[30];
	sprintf(s_focus,"%d",generalDetail->aptitude().focus());
	l_focusValue->setString(s_focus) ;  //רע

	std::string phyAttackValue = "";
	char s_minattack[30];
	sprintf(s_minattack,"%d",generalDetail->aptitude().minattack());
	phyAttackValue.append(s_minattack);
	phyAttackValue.append("-");
	char s_maxattack[30];
	sprintf(s_maxattack,"%d",generalDetail->aptitude().maxattack());
	phyAttackValue.append(s_maxattack);
	l_phyAttackValue->setString(phyAttackValue.c_str());

	std::string magicAttackValue = "";
	char s_minmagicattack[30];
	sprintf(s_minmagicattack,"%d",generalDetail->aptitude().minmagicattack());
	magicAttackValue.append(s_minmagicattack);
	magicAttackValue.append("-");
	char s_maxmagicattack[30];
	sprintf(s_maxmagicattack,"%d",generalDetail->aptitude().maxmagicattack());
	magicAttackValue.append(s_maxmagicattack);
	l_magicAttackValue->setString(magicAttackValue.c_str());

 	std::string phyDenValue = "";
 	char s_mindefend[30];
 	sprintf(s_mindefend,"%d",generalDetail->aptitude().mindefend());
 	phyDenValue.append(s_mindefend);
 	phyDenValue.append("-");
 	char s_maxdefend[30];
 	sprintf(s_maxdefend,"%d",generalDetail->aptitude().maxdefend());
 	phyDenValue.append(s_maxdefend);
 	l_phyDenValue->setString(phyDenValue.c_str());
	 
	std::string magicDenValue = "";
	char s_minmagicdefend[30];
	sprintf(s_minmagicdefend,"%d",generalDetail->aptitude().minmagicdefend());
	magicDenValue.append(s_minmagicdefend);
	magicDenValue.append("-");
	char s_maxmagicdefend[30];
	sprintf(s_maxmagicdefend,"%d",generalDetail->aptitude().maxmagicdefend());
	magicDenValue.append(s_maxmagicdefend);
	l_magicDenValue->setString(magicDenValue.c_str());

	char s_hit[30];
	sprintf(s_hit,"%d",generalDetail->aptitude().hit());
	l_hitValue->setString(s_hit); //���
	char s_dodge[30];
	sprintf(s_dodge,"%d",generalDetail->aptitude().dodge());
	l_dodgeValue->setString(s_dodge); //���
	char s_crit[30];
	sprintf(s_crit,"%d",generalDetail->aptitude().crit());
	l_critValue->setString(s_crit); //ܱ��
	char s_critdamage[30];
	sprintf(s_critdamage,"%d",generalDetail->aptitude().critdamage());
	l_critDamageValue->setString(s_critdamage); //���˺�
	char s_attackspeed[30];
	sprintf(s_attackspeed,"%d",generalDetail->aptitude().attackspeed());
	l_atkSpeedValue->setString(s_attackspeed); //���
	
	// �ˢ� �չʾ or �ջ
	int nState = ShowMyGeneral::getInstance()->getShowState(generalDetail->generalid());
	if (1 == nState)
	{
		Btn_show_general->setVisible(true);
		Btn_hide_general->setVisible(false);
	}
	else if(0 == nState)
	{
		Btn_show_general->setVisible(false);
		Btn_hide_general->setVisible(true);
	}

	RefreshGeneralsFate(generalDetail);
// 	setSkillTypeByGeneralsDetail(generalDetail);   //���ˢ��ְҵ��ˢ�¿�����������¼�˵�ǰ�佫��ְҵ��
 	RefreshGeneralsSkill(generalDetail);
	
	RefreshEvolutionEnabled(generalDetail);
}

bool GeneralsListUI::isVisible()
{
	return panel_generalsList->isVisible();
}

void GeneralsListUI::setVisible( bool visible )
{
	panel_generalsList->setVisible(visible);
	Layer_generalsList->setVisible(visible);
	Layer_generalsTop->setVisible(visible);
}

void GeneralsListUI::RefreshGeneralsFate( CGeneralDetail * generalDetail )
{
	l_fate_1->setString("");
	l_fate_2->setString("");
	l_fate_3->setString("");
	l_fate_4->setString("");
	l_fate_5->setString("");
	l_fate_6->setString("");

	btn_fate_1->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_2->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_3->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_4->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_5->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_6->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");

	btn_fate_1->setVisible(true);
	btn_fate_2->setVisible(true);
	btn_fate_3->setVisible(true);
	btn_fate_4->setVisible(true);
	btn_fate_5->setVisible(true);
	btn_fate_6->setVisible(true);

	if (generalDetail->fates_size()>0)
	{
		for (int i = 0;i<generalDetail->fates_size();++i)
		{
			if (i == 0)
			{
				l_fate_1->setString(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_1->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 1)
			{
				l_fate_2->setString(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_2->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 2)
			{
				l_fate_3->setString(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_3->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 3)
			{
				l_fate_4->setString(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_4->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 4)
			{
				l_fate_5->setString(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_5->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 5)
			{
				l_fate_6->setString(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_6->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
		}
// 		for (int i = generalDetail->fates_size();i<6;++i)
// 		{
// 			switch(i)
// 			{
// 			case 0:
// 				btn_fate_1->setVisible(false);
// 				break;
// 			case 1:
// 				btn_fate_2->setVisible(false);
// 				break;
// 			case 2:
// 				btn_fate_3->setVisible(false);
// 				break;
// 			case 3:
// 				btn_fate_4->setVisible(false);
// 				break;
// 			case 4:
// 				btn_fate_5->setVisible(false);
// 				break;
// 			case 5:
// 				btn_fate_6->setVisible(false);
// 				break;
// 			}
// 		}
	}
// 	else
// 	{
// 		btn_fate_1->setVisible(false);
// 		btn_fate_2->setVisible(false);
// 		btn_fate_3->setVisible(false);
// 		btn_fate_4->setVisible(false);
// 		btn_fate_5->setVisible(false);
// 		btn_fate_6->setVisible(false);
// 	}
}

void GeneralsListUI::FateEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsFateInfoUI) == NULL)
		{
			Size winsize = Director::getInstance()->getVisibleSize();
			auto generalsFateInfoUI = GeneralsFateInfoUI::create(curGeneralDetail);
			generalsFateInfoUI->setIgnoreAnchorPointForPosition(false);
			generalsFateInfoUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			generalsFateInfoUI->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			GameView::getInstance()->getMainUIScene()->addChild(generalsFateInfoUI, 0, kTagGeneralsFateInfoUI);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GeneralsListUI::ShowGeneralEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		// չʾ/�ջ ��佫
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5171, (void *)curGeneralBaseMsg->id(), (void *)1);

		//ˢ���佫�б
		GeneralsUI::generalsListUI->isReqNewly = true;
		auto temp = new GeneralsUI::ReqListParameter();
		temp->page = 0;
		temp->pageSize = 20;
		temp->type = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp);
		delete temp;

		Btn_show_general->setVisible(false);
		Btn_hide_general->setVisible(true);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsListUI::HideGeneralEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		// �չʾ/�ջ ��佫
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5171, (void *)curGeneralBaseMsg->id(), (void *)0);

		//ˢ���佫�б
		GeneralsUI::generalsListUI->isReqNewly = true;
		auto temp = new GeneralsUI::ReqListParameter();
		temp->page = 0;
		temp->pageSize = 20;
		temp->type = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp);
		delete temp;

		Btn_show_general->setVisible(true);
		Btn_hide_general->setVisible(false);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsListUI::RefreshGeneralsSkill( CGeneralDetail * generalDetail )
{
	long long generalsid = -1;
	std::string musouid = "";
	if (Layer_generalsList->getChildByTag(SHORTCUTLAYERTAG) != NULL)
	{
		GeneralsShortcutLayer * temp = (GeneralsShortcutLayer *)Layer_generalsList->getChildByTag(SHORTCUTLAYERTAG);
		generalsid = temp->curGeneralDetail->generalid();
		musouid = temp->m_sCurMumouId;
		Layer_generalsList->getChildByTag(SHORTCUTLAYERTAG)->removeFromParent();
	}

	Size winSize = Director::getInstance()->getVisibleSize();
	generalsShortcutLayer = GeneralsShortcutLayer::create(generalDetail);
	generalsShortcutLayer->setIgnoreAnchorPointForPosition(false);
	generalsShortcutLayer->setAnchorPoint(Vec2(0.5f,0.5f));
	generalsShortcutLayer->setPosition(Vec2(winSize.width/2,winSize.height/2));
	Layer_generalsList->addChild(generalsShortcutLayer,0,SHORTCUTLAYERTAG);
	generalsShortcutLayer->m_nLastGeneralId = generalsid;
	generalsShortcutLayer->m_sLastMumouId = musouid;
	//generalsShortcutLayer->InitMusouSkill(generalDetail);
	

// 	for (int i = 0;i<5;++i)
// 	{
// 		if (Layer_generalsList->getChildByTag(ShortSlotBaseTag+i))
// 		{
// 			Layer_generalsList->getChildByTag(ShortSlotBaseTag+i)->removeFromParent();
// 		}
// 	}
// 
// 	if (generalDetail->shortcuts_size()>0)
// 	{
// 		for (int i = 0;i<generalDetail->shortcuts_size();++i)
// 		{
// 			CShortCut * shortcut = new CShortCut();
// 			shortcut->CopyFrom(generalDetail->shortcuts(i));
// 			GeneralsShortcutSlot * genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->currentquality(),shortcut);
// 			genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
// 			genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
// 			genentalsShortcutSlot->setPosition(slotPos[i]);
// 			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
// 			genentalsShortcutSlot->setSkillProfession(curSkillType);
// 			Layer_generalsList->addChild(genentalsShortcutSlot);
// 
// 			if (shortcut->complexflag() == 1)
// 			{
// 
// 			}
// 			delete shortcut;
// 		}
// 		for (int i = generalDetail->shortcuts_size();i<5;++i)
// 		{
// 			GeneralsShortcutSlot * genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->currentquality(),i);
// 			genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
// 			genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
// 			genentalsShortcutSlot->setPosition(slotPos[i]);
// 			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
// 			genentalsShortcutSlot->setSkillProfession(curSkillType);
// 			Layer_generalsList->addChild(genentalsShortcutSlot);
// 		}
// 	}
// 	else
// 	{
// 		for (int i = 0;i<5;++i)
// 		{
// 			GeneralsShortcutSlot * genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->currentquality(),i);
// 			genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
// 			genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
// 			genentalsShortcutSlot->setPosition(slotPos[i]);
// 			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
// 			genentalsShortcutSlot->setSkillProfession(curSkillType);
// 			Layer_generalsList->addChild(genentalsShortcutSlot);
// 		}
// 	}
}

void GeneralsListUI::RefreshOneGeneralsSkillSlot(long long generalsId,CShortCut * shortCut)
{
	this->generalsShortcutLayer->RefreshOneShortcutSlot(generalsId,shortCut);
}

void GeneralsListUI::RefreshAllGeneralsSkillSlot(int generalsId,std::vector<CShortCut*> shortcutList)
{
	this->generalsShortcutLayer->RefreshAllShortcutSlot(generalsId,shortcutList);
}

#define BASEFIGHTER_CHATBUBBLE_OFFSET_Y 124
void GeneralsListUI::addChatBubble( const char* text, float duration )
{
	auto bubble = Node::create();
	bubble->setPosition(Vec2(0,0));
	bubble->setAnchorPoint(Vec2(0.5f,0.f));

	int MAX_WIDTH = 192;
	int MARGINS = 2;
	// first, create a label which its width is less than MAX_WIDTH
	auto label = Label::createWithTTF(text, APP_FONT_NAME, 24);
	auto shadowColor = Color4B::BLACK;   // black
	label->enableShadow(shadowColor,Size(1.0f, -1.0f), 1.0f);
	if(label->getContentSize().width > MAX_WIDTH)
	{
		label->release();
		label = Label::createWithTTF(text, APP_FONT_NAME, 24, Size(192, 0), TextHAlignment::LEFT);
		auto shadowColor = Color4B::BLACK;   // black
		label->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
	}
	label->setAnchorPoint(Vec2(0.5f, 0.f));
	label->setPosition( Vec2(0, MARGINS) );
	bubble->addChild(label, 1);

	// then, according the above label to set background's size 
	Size s = Director::getInstance()->getVisibleSize();
	auto tmp = Sprite::create("extensions/buttonHighlighted.png");
	Size size = tmp->getContentSize();
	Rect fullRect = Rect(0,0, size.width, size.height);
	Rect insetRect = Rect(5,4,size.width-5-5, size.height-6-4);
	tmp->release();
	auto backGround = cocos2d::extension::Scale9Sprite::create("extensions/buttonHighlighted.png", fullRect, insetRect );
	backGround->setPreferredSize(Size(label->getContentSize().width + MARGINS*2, label->getContentSize().height + MARGINS*2));   //  Size(256, 50)
	//backGround->setPosition(Vec2(s.width/2, s.height/2));
	backGround->setAnchorPoint(Vec2(0.5f, 0.f));
	backGround->setOpacity(180);
	bubble->addChild(backGround, 0);

	auto action = (ActionInterval*)Sequence::create
		(
		Show::create(),
		DelayTime::create(duration),
		RemoveSelf::create(),
		NULL
		);
	bubble->runAction(action);

	Layer_generalsList->addChild(bubble,0,BASEFIGHTER_CHATBUBBLE_OFFSET_Y);
}

void GeneralsListUI::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction ,bool firstTutorialOrSecond,bool firstMenuOrSecond)
{
	if(firstTutorialOrSecond)
	{
		isFirstTutorial = true;
		isSecondTutorial = false;
	}
	else
	{
		isFirstTutorial = false;
		isSecondTutorial = true;
	}

	isFirstOrSecondMenu = firstMenuOrSecond;

// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,40,35);
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	Layer_generalsTop->addChild(tutorialIndicator);
// 	if (firstTutorialOrSecond)   //��һ�
// 	{
// 		if (firstMenuOrSecond)    //����
// 		{
// 			tutorialIndicator->setPosition(Vec2(pos.x+160,pos.y+260));//(160,280)
// 		}
// 		else                                   //��ս
// 		{
// 			tutorialIndicator->setPosition(Vec2(pos.x+245,pos.y+260));//(250,280)
// 		}
// 	}
// 	else                                     //�ڶ��
// 	{
// 		if (firstMenuOrSecond)    //����
// 		{
// 			tutorialIndicator->setPosition(Vec2(pos.x+242,pos.y+110));
// 		}
// 		else                                   //��ս
// 		{
// 			tutorialIndicator->setPosition(Vec2(pos.x+242,pos.y+110));
// 		}
// 	}

	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,65,26,true,true,true);
	if (firstTutorialOrSecond)   //��һ�
	{
		tutorialIndicator->setDrawNodePos(Vec2(pos.x+197+_w,pos.y+270+_h));
	}
	else                                     //еڶ��
	{
		tutorialIndicator->setDrawNodePos(Vec2(pos.x+197+_w,pos.y+195+_h));
	}
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralsListUI::addCCTutorialIndicator2( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction ,bool firstOrSecond )
{
// 	isFirstOrSecondTutorial = true;
// 	isFirstOrSecondMenu = firstOrSecond;
// 
// 	m_content = content;
// 	m_pos = pos ;
// 	m_direction = direction;
// 
// 	this->generalList_tableView->reloadData();
}

void GeneralsListUI::removeCCTutorialIndicator()
{
 	isFirstTutorial = false;
 	isSecondTutorial = false;
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(Layer_generalsTop->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void GeneralsListUI::registerScriptCommand( int scriptId )
{
	 mTutorialScriptInstanceId = scriptId;
}

void GeneralsListUI::RefreshEvolutionEnabled( CGeneralDetail * generalDetail )
{
	if (generalDetail->evolutenum() >= 2)
	{
		Btn_evolution->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
	}
	else
	{
		Btn_evolution->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	}
}




