#include "GeneralsRecuriteUI.h"
#include "GeneralsUI.h"
#include "../../messageclient/element/CActionDetail.h"
#include "../../GameView.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "RecuriteActionItem.h"
#include "GeneralsHeadItemBase.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "SingleRecuriteResultUI.h"
#include "TenTimesRecuriteResultUI.h"
#include "../../legend_script/UITutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "GeneralsListUI.h"
#include "../extensions/RecruitGeneralCard.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/extensions/ShowSystemInfo.h"
#include "../../legend_script/CCTeachingGuide.h"

#define kTagGeneralRecuritMarquee 123

GeneralsRecuriteUI::GeneralsRecuriteUI():
curActionType(BASE)
{
}

GeneralsRecuriteUI::~GeneralsRecuriteUI()
{
	delete m_pCurGeneralDetail;
}

void GeneralsRecuriteUI::onEnter()
{
	UIScene::onEnter();
}

void GeneralsRecuriteUI::onExit()
{
	UIScene::onExit();
}

bool GeneralsRecuriteUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	return false;
}

void GeneralsRecuriteUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void GeneralsRecuriteUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void GeneralsRecuriteUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}

GeneralsRecuriteUI* GeneralsRecuriteUI::create()
{
	auto generalsRecuriteUI = new GeneralsRecuriteUI();
	if (generalsRecuriteUI && generalsRecuriteUI->init())
	{
		generalsRecuriteUI->autorelease();
		return generalsRecuriteUI;
	}
	CC_SAFE_DELETE(generalsRecuriteUI);
	return NULL;
}

bool GeneralsRecuriteUI::init()
{
	if (UIScene::init())
	{
		m_pCurGeneralDetail = new CGeneralDetail();

		auto winsize = Director::getInstance()->getVisibleSize();
		//��ļ��
		panel_recruit = (Layout*)Helper::seekWidgetByName(GeneralsUI::ppanel,"Panel_generals");
		panel_recruit->setVisible(true);
		//  ���ļ�
		Layer_recruit = Layer::create();
// 		Layer_recruit->setIgnoreAnchorPointForPosition(false);
// 		Layer_recruit->setAnchorPoint(Vec2(0.5f,0.5f));
// 		Layer_recruit->setContentSize(Size(800, 480));
// 		Layer_recruit->setPosition(Vec2(winsize.width/2,winsize.height/2));

		addChild(Layer_recruit);

		Layer_upper = Layer::create();
		addChild(Layer_upper);

		auto tableView_kuang = ImageView::create();
		tableView_kuang->loadTexture("res_ui/LV5_dikuang_001.png");
		tableView_kuang->setRotation(90);
		tableView_kuang->setScale9Enabled(true);
		tableView_kuang->setContentSize(Size(180,31));
		tableView_kuang->setCapInsets(Rect(32,31,1,1));
		tableView_kuang->setAnchorPoint(Vec2(0.5f,0.5f));
		tableView_kuang->setPosition(Vec2(79,358));
		Layer_upper->addChild(tableView_kuang);

		auto btn_null_good = (Button*)Helper::seekWidgetByName(panel_recruit,"Button_null_good");
		btn_null_good->setTouchEnabled(true);
		btn_null_good->addTouchEventListener(CC_CALLBACK_2(GeneralsRecuriteUI::NullGoodEvent, this));
		auto btn_null_better = (Button*)Helper::seekWidgetByName(panel_recruit,"Button_null_better");
		btn_null_better->setTouchEnabled(true);
		btn_null_better->addTouchEventListener(CC_CALLBACK_2(GeneralsRecuriteUI::NullBetterEvent, this));
		auto btn_null_best = (Button*)Helper::seekWidgetByName(panel_recruit,"Button_null_best");
		btn_null_best->setTouchEnabled(true);
		btn_null_best->addTouchEventListener(CC_CALLBACK_2(GeneralsRecuriteUI::NullBestEvent, this));

		//���ͨ��ļ
		Button_normalRecurit = (Button*)Helper::seekWidgetByName(panel_recruit,"Button_putongzhaomu");
		Button_normalRecurit->setTouchEnabled(true);
		Button_normalRecurit->setPressedActionEnabled(true);
		Button_normalRecurit->addTouchEventListener(CC_CALLBACK_2(GeneralsRecuriteUI::NormalRecuritEvent, this));
		//������ļ
		Button_goodRecurit = (Button*)Helper::seekWidgetByName(panel_recruit,"Button_youxiuzhaomu");
		Button_goodRecurit->setTouchEnabled(true);
		Button_goodRecurit->setPressedActionEnabled(true);
		Button_goodRecurit->addTouchEventListener(CC_CALLBACK_2(GeneralsRecuriteUI::GoodRecuritEvent, this));
		//��Ʒ��ļ
		Button_wonderfulRecurit = (Button*)Helper::seekWidgetByName(panel_recruit,"Button_jipinzhaomu");
		Button_wonderfulRecurit->setTouchEnabled(true);
		Button_wonderfulRecurit->setPressedActionEnabled(true);
		Button_wonderfulRecurit->addTouchEventListener(CC_CALLBACK_2(GeneralsRecuriteUI::WonderfulRecuritEvent, this));
		//ʮ��é®
		Button_tenTimesRecurit = (Button*)Helper::seekWidgetByName(panel_recruit,"Button_shigumaolu");
		Button_tenTimesRecurit->setVisible(true);
		Button_tenTimesRecurit->setTouchEnabled(true);
		Button_tenTimesRecurit->setPressedActionEnabled(true);
		Button_tenTimesRecurit->addTouchEventListener(CC_CALLBACK_2(GeneralsRecuriteUI::TenTimesRecuritEvent, this));
		auto l_tenTimesCostGold = (Text*)Helper::seekWidgetByName(panel_recruit,"Label_1000");
		for (int i = 0;i<GameView::getInstance()->actionDetailList.size();++i)
		{
			auto actionDetail = GameView::getInstance()->actionDetailList.at(i);
			if(actionDetail->type() == TEN_TIMES)
			{
				char s_gold[20];
				sprintf(s_gold,"%d",actionDetail->gold());
				l_tenTimesCostGold->setString(s_gold);
			}
		}

		RefreshRecruitData();

		createFiveBestGeneral();

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);

		return true;
	}
	return false;
}

void GeneralsRecuriteUI::CloseEvent( Ref *pSender )
{
	for (int i = 0;i<3;++i)
	{
		if (Layer_recruit->getChildByTag(50+i))
		{
			auto tempRecurite = ( RecuriteActionItem *)Layer_recruit->getChildByTag(50+i);
			tempRecurite->lastChangeTime = GameUtils::millisecondNow();
		}
	}
}

void GeneralsRecuriteUI::NormalRecuritEvent(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		curActionType = BASE;
		auto tempdata = new ReqData();
		tempdata->actionType = BASE;
		if (Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG))
		{
			auto temp = (RecuriteActionItem *)Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG);
			if (temp->state == RecuriteActionItem::Free)
			{
				tempdata->recruitType = FREE;
			}
			else
			{
				tempdata->recruitType = GOLD;
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsRecuriteUI::GoodRecuritEvent(Ref *pSender, Widget::TouchEventType type)
{


	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		curActionType = BETTER;
		auto tempdata = new ReqData();
		tempdata->actionType = BETTER;
		if (Layer_recruit->getChildByTag(BETTERRECURITEACTIONITEMTAG))
		{
			auto temp = (RecuriteActionItem *)Layer_recruit->getChildByTag(BETTERRECURITEACTIONITEMTAG);
			if (temp->state == RecuriteActionItem::Free)
			{
				tempdata->recruitType = FREE;
			}
			else
			{
				tempdata->recruitType = GOLD;
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;

		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsRecuriteUI::WonderfulRecuritEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		curActionType = BEST;
		auto tempdata = new ReqData();
		tempdata->actionType = BEST;
		if (Layer_recruit->getChildByTag(BESTRECURITEACTIONITEMTAG))
		{
			auto temp = (RecuriteActionItem *)Layer_recruit->getChildByTag(BESTRECURITEACTIONITEMTAG);
			if (temp->state == RecuriteActionItem::Free)
			{
				tempdata->recruitType = FREE;
			}
			else
			{
				tempdata->recruitType = GOLD;
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;

		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsRecuriteUI::TenTimesRecuritEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		curActionType = TEN_TIMES;
		auto tempdata = new ReqData();
		tempdata->actionType = TEN_TIMES;
		tempdata->recruitType = GOLD;

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}


void GeneralsRecuriteUI::createFiveBestGeneral()
{
// 	for (int i =0;i<GameView::getInstance()->actionDetailList.size();++i)
// 	{
// 		if (GameView::getInstance()->actionDetailList.at(i)->type() == TEN_TIMES)
// 		{
// 			for (int j = 0;j<GameView::getInstance()->actionDetailList.at(i)->generals_size();++j)
// 			{
// 				if (j < 5)
// 				{
// 					int _id = GameView::getInstance()->actionDetailList.at(i)->generals(j);
// 					GeneralsHeadItemBase * generalsHeadItemBase = GeneralsHeadItemBase::create(_id);
// 					generalsHeadItemBase->setAnchorPoint(Vec2(0,0));
// 					generalsHeadItemBase->setPosition(Vec2(72 + j*111,254));
// 					Layer_recruit->addChild(generalsHeadItemBase);
// 
// 					int width = generalsHeadItemBase->getContentSize().width-4;
// 					int height = generalsHeadItemBase->getContentSize().height-4;
// 					float speed = 150.f;
// 					Color4F color = Color4F(Color3B(255, 222, 0));
// 					std::string _path = "animation/texiao/particledesigner/";
// 					_path.append("cardAround.plist");
// 
// 					ParticleSystemQuad* particleEffect_1 = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
// 					particleEffect_1->setPosition(Vec2(72 + j*111+2,254+2));
// 					particleEffect_1->setStartSize(30);
// 					particleEffect_1->setPositionType(ParticleSystem::PositionType::FREE);
// 					particleEffect_1->setVisible(true);
// 					particleEffect_1->setScale(1.0f);
// 					particleEffect_1->setLife(1.5f);
// 					//particleEffect_1->setStartColor(color);
// 					//particleEffect_1->setEndColor(color);
// 					Layer_recruit->addChild(particleEffect_1);
// 
// 					Sequence * sequence_1 = Sequence::create(
// 						MoveTo::create(height*1.0f/speed,Vec2(72 + j*111+2,254+2+height)),
// 						MoveTo::create(width*1.0f/speed,Vec2(72 + j*111+2+width,254+2+height)),
// 						MoveTo::create(height*1.0f/speed,Vec2(72 + j*111+2+width,254+2)),
// 						MoveTo::create(width*1.0f/speed,Vec2(72 + j*111+2,254+2)),
// 						NULL
// 						);
// 					RepeatForever * repeatForeverAnm_1 = RepeatForever::create(sequence_1);
// 					particleEffect_1->runAction(repeatForeverAnm_1);
// 
// 					ParticleSystemQuad* particleEffect_2 = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
// 					particleEffect_2->setPosition(Vec2(72 + j*111+2+width,254+2+height));
// 					particleEffect_2->setStartSize(30);
// 					particleEffect_2->setPositionType(ParticleSystem::PositionType::FREE);
// 					particleEffect_2->setVisible(true);
// 					particleEffect_2->setScale(1.0f);
// 					particleEffect_2->setLife(1.5f);
// 					//particleEffect_2->setStartColor(color);
// 					//particleEffect_2->setEndColor(color);
// 					Layer_recruit->addChild(particleEffect_2);
// 
// 					Sequence * sequence_2 = Sequence::create(
// 						MoveTo::create(height*1.0f/speed,Vec2(72 + j*111+2+width,254+2)),
// 						MoveTo::create(width*1.0f/speed,Vec2(72 + j*111+2,254+2)),
// 						MoveTo::create(height*1.0f/speed,Vec2(72 + j*111+2,254+2+height)),
// 						MoveTo::create(width*1.0f/speed,Vec2(72 + j*111+2+width,254+2+height)),
// 						NULL
// 						);
// 					RepeatForever * repeatForeverAnm_2 = RepeatForever::create(sequence_2);
// 					particleEffect_2->runAction(repeatForeverAnm_2);
// 
// // 					ParticleSystemQuad* particleEffect_3 = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
// // 					particleEffect_3->setPosition(Vec2(72 + j*111+2,254+2+height));
// // 					particleEffect_3->setStartSize(10);
// // 					particleEffect_3->setPositionType(ParticleSystem::PositionType::FREE);
// // 					particleEffect_3->setVisible(true);
// // 					particleEffect_3->setScale(1.0f);
// // 					Layer_recruit->addChild(particleEffect_3);
// // 
// // 					Sequence * sequence_3 = Sequence::create(
// // 						MoveTo::create(width*1.0f/speed,Vec2(72 + j*111+2+width,254+2+height)),
// // 						MoveTo::create(height*1.0f/speed,Vec2(72 + j*111+2+width,254+2)),
// // 						MoveTo::create(width*1.0f/speed,Vec2(72 + j*111+2,254+2)),
// // 						MoveTo::create(height*1.0f/speed,Vec2(72 + j*111+2,254+2+height)),
// // 						NULL
// // 						);
// // 					RepeatForever * repeatForeverAnm_3 = RepeatForever::create(sequence_3);
// // 					particleEffect_3->runAction(repeatForeverAnm_3);
// // 
// // 					ParticleSystemQuad* particleEffect_4 = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
// // 					particleEffect_4->setPosition(Vec2(72 + j*111+2+width,254+2));
// // 					particleEffect_4->setStartSize(10);
// // 					particleEffect_4->setPositionType(ParticleSystem::PositionType::FREE);
// // 					particleEffect_4->setVisible(true);
// // 					particleEffect_4->setScale(1.0f);
// // 					Layer_recruit->addChild(particleEffect_4);
// // 
// // 					Sequence * sequence_4 = Sequence::create(
// // 						MoveTo::create(width*1.0f/speed,Vec2(72 + j*111+2,254+2)),
// // 						MoveTo::create(height*1.0f/speed,Vec2(72 + j*111+2,254+2+height)),
// // 						MoveTo::create(width*1.0f/speed,Vec2(72 + j*111+2+width,254+2+height)),
// // 						MoveTo::create(height*1.0f/speed,Vec2(72 + j*111+2+width,254+2)),
// // 						NULL
// // 						);
// // 					RepeatForever * repeatForeverAnm_4 = RepeatForever::create(sequence_4);
// // 					particleEffect_4->runAction(repeatForeverAnm_4);
// 				}
// 			}
// 		}
// 	}

	//���� 
	if (Layer_recruit->getChildByTag(kTagGeneralRecuritMarquee) == NULL)
	{
		auto pMarquee = GeneralRecuritMarquee::create(554,  147);
		pMarquee->setTag(kTagGeneralRecuritMarquee);
		pMarquee->setPosition(Vec2(70,284));
		Layer_recruit->addChild(pMarquee);
	}
}

void GeneralsRecuriteUI::RefreshRecruitData()
{
	for (int i = 0;i<3;++i)
	{
		if (Layer_recruit->getChildByTag(50+i))
		{
			auto tempRecurite = ( RecuriteActionItem *)Layer_recruit->getChildByTag(50+i);
			tempRecurite->removeFromParent();
		}
	}

	for(int i = 0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		auto temp = GameView::getInstance()->actionDetailList.at(i);
		if(temp->type() != TEN_TIMES)
		{
			ReloadNormalType(temp);
		}
	}
}

void GeneralsRecuriteUI::ReloadNormalType(CActionDetail * actionDetail)
{
	if (actionDetail->type() == BASE)
	{
		auto BaseRecurite = RecuriteActionItem::create(actionDetail);
		BaseRecurite->setIgnoreAnchorPointForPosition(false);
		BaseRecurite->setAnchorPoint(Vec2(0,0));
		BaseRecurite->setPosition(Vec2(62,52));
		BaseRecurite->setTag(BASERECURITEACTIONITEMTAG);
		Layer_recruit->addChild(BaseRecurite);
	}
	else if (actionDetail->type() == BETTER)
	{
		auto BetterRecurite = RecuriteActionItem::create(actionDetail);
		BetterRecurite->setIgnoreAnchorPointForPosition(false);
		BetterRecurite->setAnchorPoint(Vec2(0,0));
		BetterRecurite->setPosition(Vec2(294,52));
		BetterRecurite->setTag(BETTERRECURITEACTIONITEMTAG);
		Layer_recruit->addChild(BetterRecurite);
	}
	else if (actionDetail->type() == BEST)
	{
		auto BestRecurite = RecuriteActionItem::create(actionDetail);
		BestRecurite->setIgnoreAnchorPointForPosition(false);
		BestRecurite->setAnchorPoint(Vec2(0,0));
		BestRecurite->setPosition(Vec2(526,52));
		BestRecurite->setTag(BESTRECURITEACTIONITEMTAG);
		Layer_recruit->addChild(BestRecurite);
	}
}

void GeneralsRecuriteUI::setVisible( bool visible )
{
	panel_recruit->setVisible(visible);
	Layer_recruit->setVisible(visible);
	Layer_upper->setVisible(visible);

	if (visible)
	{
		for (int i = 0;i<3;++i)
		{
			if (Layer_recruit->getChildByTag(50+i))
			{
				auto tempRecurite = ( RecuriteActionItem *)Layer_recruit->getChildByTag(50+i);
				tempRecurite->refreshData();
			}
		}
	}
}

void GeneralsRecuriteUI::presentRecruiteResult( CGeneralDetail * generalDetail,int actionType,int recruitGeneralCardType,bool isPresentBtn )
{
	m_pCurGeneralDetail->CopyFrom(*generalDetail);
	switch(actionType)
	{
	case 1:
		curActionType = BASE;
		break;
	case 2:
		curActionType = BETTER;
		break;
	case 3:
		curActionType = BEST;
		break;
	}
	
	m_bIsPresentBtn = false;

	auto winSize = Director::getInstance()->getVisibleSize();

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRecuriteGeneralCardUI))
		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRecuriteGeneralCardUI)->removeFromParent();

	auto recuitCard_ = RecruitGeneralCard::create(recruitGeneralCardType,generalDetail->modelid());
	recuitCard_->setIgnoreAnchorPointForPosition(false);
	recuitCard_->setAnchorPoint(Vec2(0.5f,0.5f));
	recuitCard_->setPosition(Vec2(winSize.width/2,winSize.height/2));
	recuitCard_->AddcallBackFirstEvent(this,SEL_CallFuncO(&GeneralsRecuriteUI::DetailInfoEvent));
	recuitCard_->AddcallBacksecondEvent(this,NULL);
	recuitCard_->AddcallBackThirdEvent(this,SEL_CallFuncO(&GeneralsRecuriteUI::RecuriteAgainEvent));
	recuitCard_->setTag(kTagRecuriteGeneralCardUI);
	GameView::getInstance()->getMainUIScene()->addChild(recuitCard_);
	recuitCard_->RefreshCostValueByActionType(actionType);

// 	SingleRecuriteResultUI * singleRecuriteResultUI = (SingleRecuriteResultUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSingleRecuriteResultUI);
// 	if (singleRecuriteResultUI == NULL)
// 	{
// 		Size winSize = Director::getInstance()->getVisibleSize();
// 		SingleRecuriteResultUI * singleRecuriteResultLayer = SingleRecuriteResultUI::create();
// 		GameView::getInstance()->getMainUIScene()->addChild(singleRecuriteResultLayer,0,kTagGeneralsSingleRecuriteResultUI);
// 		singleRecuriteResultLayer->setIgnoreAnchorPointForPosition(false);
// 		singleRecuriteResultLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
// 		singleRecuriteResultLayer->setPosition(Vec2(winSize.width/2, winSize.height/2));
// 		singleRecuriteResultLayer->RefreshRecruiteResult(generalDetail);
// 		singleRecuriteResultLayer->RefreshRecruiteCost(actionType);
// 		singleRecuriteResultLayer->RefreshBtnPresent(isPresentBtn);
// 	}
// 	else
// 	{
// 		singleRecuriteResultUI->RefreshRecruiteResult(generalDetail);
// 		singleRecuriteResultUI->RefreshRecruiteCost(actionType);
// 		singleRecuriteResultUI->RefreshBtnPresent(isPresentBtn);
// 	}
}



void GeneralsRecuriteUI::presentTenTimesRecruiteResult( std::vector<CGeneralDetail *> temp ,bool isPresentBtn)
{	
	auto tenTimesRecuriteResultUI = (TenTimesRecuriteResultUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsTenTimesRecuriteResultUI);
	if (tenTimesRecuriteResultUI == NULL)
	{
		auto winSize = Director::getInstance()->getVisibleSize();
		auto tenTimesRecuriteResultLayer = TenTimesRecuriteResultUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(tenTimesRecuriteResultLayer,0,kTagGeneralsTenTimesRecuriteResultUI);
		tenTimesRecuriteResultLayer->setIgnoreAnchorPointForPosition(false);
		tenTimesRecuriteResultLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
		tenTimesRecuriteResultLayer->setPosition(Vec2(winSize.width/2, winSize.height/2));
		tenTimesRecuriteResultLayer->RefreshTenTimesRecruitData(temp);
		tenTimesRecuriteResultLayer->RefreshBtnPresent(isPresentBtn);
	}
	else
	{
		tenTimesRecuriteResultUI->RefreshTenTimesRecruitData(temp);
		tenTimesRecuriteResultUI->RefreshBtnPresent(isPresentBtn);
	}
}

GeneralsRecuriteUI::RecuriteActionType GeneralsRecuriteUI::getCurRecuriteActionType()
{
	return curActionType;
}


void GeneralsRecuriteUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralsRecuriteUI::addCCTutorialIndicator( const char* content,Vec2 pos ,CCTutorialIndicator::Direction direction )
{
	auto winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height - 480)/2;
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,70,50);
// 	tutorialIndicator->setPosition(Vec2(pos.x-40,pos.y+90));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	Layer_recruit->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,120,40,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+_w,pos.y+_h));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralsRecuriteUI::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(Layer_recruit->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();
	
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void GeneralsRecuriteUI::DetailInfoEvent( Ref *pSender )
{
	auto singleRecuriteResultUI = (SingleRecuriteResultUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsSingleRecuriteResultUI);
	if (singleRecuriteResultUI == NULL)
	{
		auto winSize = Director::getInstance()->getVisibleSize();
		auto singleRecuriteResultLayer = SingleRecuriteResultUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(singleRecuriteResultLayer,0,kTagGeneralsSingleRecuriteResultUI);
		singleRecuriteResultLayer->setIgnoreAnchorPointForPosition(false);
		singleRecuriteResultLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
		singleRecuriteResultLayer->setPosition(Vec2(winSize.width/2, winSize.height/2));
		singleRecuriteResultLayer->RefreshRecruiteResult(m_pCurGeneralDetail);
		singleRecuriteResultLayer->RefreshRecruiteCost(m_nActionType);
		singleRecuriteResultLayer->RefreshBtnPresent(m_bIsPresentBtn);
	}
	else
	{
		singleRecuriteResultUI->RefreshRecruiteResult(m_pCurGeneralDetail);
		singleRecuriteResultUI->RefreshRecruiteCost(m_nActionType);
		singleRecuriteResultUI->RefreshBtnPresent(m_bIsPresentBtn);
	}
}


void GeneralsRecuriteUI::RecuriteAgainEvent( Ref *pSender )
{
	if (this->getCurRecuriteActionType() == BASE)
	{
		auto tempdata = new GeneralsRecuriteUI::ReqData();
		tempdata->actionType = BASE;
		if (Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG))
		{
			auto temp = (RecuriteActionItem *)Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG);
			if (temp->state == RecuriteActionItem::Free)
			{
				tempdata->recruitType = FREE;
			}
			else
			{
				tempdata->recruitType = GOLD;
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;
	}
	else if (this->getCurRecuriteActionType() == BETTER)
	{
		auto tempdata = new GeneralsRecuriteUI::ReqData();
		tempdata->actionType = BETTER;
		if (Layer_recruit->getChildByTag(BETTERRECURITEACTIONITEMTAG))
		{
			auto temp = (RecuriteActionItem *)Layer_recruit->getChildByTag(BETTERRECURITEACTIONITEMTAG);
			if (temp->state == RecuriteActionItem::Free)
			{
				tempdata->recruitType = FREE;
			}
			else
			{
				tempdata->recruitType = GOLD;
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;
	}
	else if (this->getCurRecuriteActionType() == BEST)
	{
		auto tempdata = new GeneralsRecuriteUI::ReqData();
		tempdata->actionType = BEST;
		if (Layer_recruit->getChildByTag(BESTRECURITEACTIONITEMTAG))
		{
			auto temp = (RecuriteActionItem *)Layer_recruit->getChildByTag(BESTRECURITEACTIONITEMTAG);
			if (temp->state == RecuriteActionItem::Free)
			{
				tempdata->recruitType = FREE;
			}
			else
			{
				tempdata->recruitType = GOLD;
			}
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
		delete tempdata;
	}
}

void GeneralsRecuriteUI::NullGoodEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		std::map<int, std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(14);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end())
		{
			return;
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			{
				auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[14].c_str());
				GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				showSystemInfo->setIgnoreAnchorPointForPosition(false);
				showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				showSystemInfo->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsRecuriteUI::NullBetterEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		std::map<int, std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(15);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end())
		{
			return;
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			{
				auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[15].c_str());
				GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				showSystemInfo->setIgnoreAnchorPointForPosition(false);
				showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				showSystemInfo->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GeneralsRecuriteUI::NullBestEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto winsize = Director::getInstance()->getVisibleSize();
		std::map<int, std::string>::const_iterator cIter;
		cIter = SystemInfoConfigData::s_systemInfoList.find(16);
		if (cIter == SystemInfoConfigData::s_systemInfoList.end())
		{
			return;
		}
		else
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStrategiesDetailInfo) == NULL)
			{
				auto showSystemInfo = ShowSystemInfo::create(SystemInfoConfigData::s_systemInfoList[16].c_str());
				GameView::getInstance()->getMainUIScene()->addChild(showSystemInfo, 0, kTagStrategiesDetailInfo);
				showSystemInfo->setIgnoreAnchorPointForPosition(false);
				showSystemInfo->setAnchorPoint(Vec2(0.5f, 0.5f));
				showSystemInfo->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
GeneralRecuritMarquee::GeneralRecuritMarquee()
	:m_layerRecurite1Pos(Vec2(0, 0))
	,m_layerRecurite2Pos(Vec2(0, 0))
	,m_nWidth(0)
	,m_nHeight(0)
{

}

GeneralRecuritMarquee::~GeneralRecuritMarquee()
{
	this->unscheduleUpdate();
}

GeneralRecuritMarquee * GeneralRecuritMarquee::create(int width, int height )
{
	auto marquee = new GeneralRecuritMarquee();
	if (marquee && marquee->init(width, height))
	{
		marquee->autorelease();
		return marquee;
	}
	CC_SAFE_DELETE(marquee);
	return NULL;
}

bool GeneralRecuritMarquee::init(int width, int height )
{
	if (Node::init())
	{
		m_nWidth = width;
		m_nHeight = height;

		auto m_contentScrollView = cocos2d::extension::ScrollView::create(Size(width,height));
		m_contentScrollView->setViewSize(Size(width, height));
		m_contentScrollView->setIgnoreAnchorPointForPosition(false);
		m_contentScrollView->setTouchEnabled(false);
		m_contentScrollView->setDirection(cocos2d::extension::ScrollView::Direction::HORIZONTAL);
		m_contentScrollView->setAnchorPoint(Vec2(0,0));
		m_contentScrollView->setPosition(Vec2(0,0));
		m_contentScrollView->setBounceable(false);
		addChild(m_contentScrollView);

		//create recurite 1
		l_recurite_1 = Layer::create();
		l_recurite_1->setContentSize(Size(width,height));
		m_contentScrollView->addChild(l_recurite_1);

		for (int i =0;i<GameView::getInstance()->actionDetailList.size();++i)
		{
			if (GameView::getInstance()->actionDetailList.at(i)->type() == TEN_TIMES)
			{
				for (int j = 0;j<GameView::getInstance()->actionDetailList.at(i)->generals_size();++j)
				{
					if (j < 5)
					{
						int _id = GameView::getInstance()->actionDetailList.at(i)->generals(j);
						GeneralsHeadItemBase * generalsHeadItemBase = GeneralsHeadItemBase::create(_id);
						generalsHeadItemBase->setAnchorPoint(Vec2(0,0));
						generalsHeadItemBase->setPosition(Vec2(0 + j*111,0));
						l_recurite_1->addChild(generalsHeadItemBase);
						generalsHeadItemBase->visit();

						int width = generalsHeadItemBase->getContentSize().width-4;
						int height = generalsHeadItemBase->getContentSize().height-4;
						float speed = 150.f;
						auto color = Color4F(Color3B(255, 222, 0));
						std::string _path = "animation/texiao/particledesigner/";
						_path.append("cardAround.plist");

						auto particleEffect_1 = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
						particleEffect_1->setPosition(Vec2( j*111+2,2));
						particleEffect_1->setStartSize(30);
						particleEffect_1->setPositionType(ParticleSystem::PositionType::RELATIVE);
						particleEffect_1->setVisible(true);
						particleEffect_1->setScale(1.0f);
						particleEffect_1->setLife(1.5f);
						//particleEffect_1->setStartColor(color);
						//particleEffect_1->setEndColor(color);
						l_recurite_1->addChild(particleEffect_1);

						auto sequence_1 = Sequence::create(
							MoveTo::create(height*1.0f/speed,Vec2(j*111+2,2+height)),
							MoveTo::create(width*1.0f/speed,Vec2(j*111+2+width,2+height)),
							MoveTo::create(height*1.0f/speed,Vec2(j*111+2+width,2)),
							MoveTo::create(width*1.0f/speed,Vec2(j*111+2,2)),
							NULL
							);
						auto repeatForeverAnm_1 = RepeatForever::create(sequence_1);
						particleEffect_1->runAction(repeatForeverAnm_1);

						auto particleEffect_2 = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
						particleEffect_2->setPosition(Vec2(j*111+2+width,2+height));
						particleEffect_2->setStartSize(30);
						particleEffect_2->setPositionType(ParticleSystem::PositionType::RELATIVE);
						particleEffect_2->setVisible(true);
						particleEffect_2->setScale(1.0f);
						particleEffect_2->setLife(1.5f);
						//particleEffect_2->setStartColor(color);
						//particleEffect_2->setEndColor(color);
						l_recurite_1->addChild(particleEffect_2);

						auto sequence_2 = Sequence::create(
							MoveTo::create(height*1.0f/speed,Vec2(j*111+2+width,2)),
							MoveTo::create(width*1.0f/speed,Vec2(j*111+2,2)),
							MoveTo::create(height*1.0f/speed,Vec2(j*111+2,2+height)),
							MoveTo::create(width*1.0f/speed,Vec2(j*111+2+width,2+height)),
							NULL
							);
						auto repeatForeverAnm_2 = RepeatForever::create(sequence_2);
						particleEffect_2->runAction(repeatForeverAnm_2);
					}
				}
			}
		}


		//create recurite 2
		l_recurite_2 = Layer::create();
		l_recurite_2->setContentSize(Size(width,height));
		m_contentScrollView->addChild(l_recurite_2);

		for (int i =0;i<GameView::getInstance()->actionDetailList.size();++i)
		{
			if (GameView::getInstance()->actionDetailList.at(i)->type() == TEN_TIMES)
			{
				for (int j = 0;j<GameView::getInstance()->actionDetailList.at(i)->generals_size();++j)
				{
					if (j < 5)
					{
						int _id = GameView::getInstance()->actionDetailList.at(i)->generals(j);
						auto generalsHeadItemBase = GeneralsHeadItemBase::create(_id);
						generalsHeadItemBase->setAnchorPoint(Vec2(0,0));
						generalsHeadItemBase->setPosition(Vec2(0 + j*111,0));
						l_recurite_2->addChild(generalsHeadItemBase);
						generalsHeadItemBase->visit();

						int width = generalsHeadItemBase->getContentSize().width-4;
						int height = generalsHeadItemBase->getContentSize().height-4;
						float speed = 150.f;
						auto color = Color4F(Color3B(255, 222, 0));
						std::string _path = "animation/texiao/particledesigner/";
						_path.append("cardAround.plist");

						auto particleEffect_1 = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
						particleEffect_1->setPosition(Vec2( j*111+2,2));
						particleEffect_1->setStartSize(30);
						particleEffect_1->setPositionType(ParticleSystem::PositionType::RELATIVE);
						particleEffect_1->setVisible(true);
						particleEffect_1->setScale(1.0f);
						particleEffect_1->setLife(1.5f);
						//particleEffect_1->setStartColor(color);
						//particleEffect_1->setEndColor(color);
						l_recurite_2->addChild(particleEffect_1);

						auto sequence_1 = Sequence::create(
							MoveTo::create(height*1.0f/speed,Vec2(j*111+2,2+height)),
							MoveTo::create(width*1.0f/speed,Vec2(j*111+2+width,2+height)),
							MoveTo::create(height*1.0f/speed,Vec2(j*111+2+width,2)),
							MoveTo::create(width*1.0f/speed,Vec2(j*111+2,2)),
							NULL
							);
						auto repeatForeverAnm_1 = RepeatForever::create(sequence_1);
						particleEffect_1->runAction(repeatForeverAnm_1);

						auto particleEffect_2 = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
						particleEffect_2->setPosition(Vec2(j*111+2+width,2+height));
						particleEffect_2->setStartSize(30);
						particleEffect_2->setPositionType(ParticleSystem::PositionType::RELATIVE);
						particleEffect_2->setVisible(true);
						particleEffect_2->setScale(1.0f);
						particleEffect_2->setLife(1.5f);
						//particleEffect_2->setStartColor(color);
						//particleEffect_2->setEndColor(color);
						l_recurite_2->addChild(particleEffect_2);

						auto sequence_2 = Sequence::create(
							MoveTo::create(height*1.0f/speed,Vec2(j*111+2+width,2)),
							MoveTo::create(width*1.0f/speed,Vec2(j*111+2,2)),
							MoveTo::create(height*1.0f/speed,Vec2(j*111+2,2+height)),
							MoveTo::create(width*1.0f/speed,Vec2(j*111+2+width,2+height)),
							NULL
							);
						auto repeatForeverAnm_2 = RepeatForever::create(sequence_2);
						particleEffect_2->runAction(repeatForeverAnm_2);
					}
				}
			}
		}

		m_layerRecurite1Pos = Vec2(0,0);
		m_layerRecurite2Pos = Vec2(-m_nWidth,0);

		this->scheduleUpdate();

		return true;
	}

	return false;
}

void GeneralRecuritMarquee::update( float dt )
{
  	m_layerRecurite1Pos.y = 0;
	m_layerRecurite2Pos.y = 0;
  
  	m_layerRecurite1Pos.x = m_layerRecurite1Pos.x + dt * 20;
	m_layerRecurite2Pos.x = m_layerRecurite2Pos.x + dt * 20;
  	if (m_layerRecurite1Pos.x  > m_nWidth)
  	{
		m_layerRecurite1Pos.x = -m_nWidth;
  	}

	if (m_layerRecurite2Pos.x  > m_nWidth)
	{
		m_layerRecurite2Pos.x = -m_nWidth;
	}
  
  	l_recurite_1->setPosition(m_layerRecurite1Pos);
	l_recurite_2->setPosition(m_layerRecurite2Pos);
}