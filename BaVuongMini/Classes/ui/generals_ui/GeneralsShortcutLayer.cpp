#include "GeneralsShortcutLayer.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "GeneralsShortcutSlot.h"
#include "../../messageclient/element/CShortCut.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"

#define  ShortSlotBaseTag 300
#define  MuSouSkillIdentityName "WuShuangBiaoShi"
#define  MuSouSkillIdentityTag 500

GeneralsShortcutLayer::GeneralsShortcutLayer():
m_sCurMumouId(""),
m_sLastMumouId(""),
m_nLastGeneralId(-1)
{
	curSkillType = GeneralsSkillsUI::Warrior;

	slotPos[0].x = 412;
	slotPos[0].y = 75;
	slotPos[1].x = 487;
	slotPos[1].y = 75;
	slotPos[2].x = 560;
	slotPos[2].y = 75;
	slotPos[3].x = 637;
	slotPos[3].y = 75;
	slotPos[4].x = 709;
	slotPos[4].y = 75;
}


GeneralsShortcutLayer::~GeneralsShortcutLayer()
{
	delete curGeneralDetail;
}

GeneralsShortcutLayer * GeneralsShortcutLayer::create(CGeneralDetail * generalDetail)
{
	auto generalsShortcutLayer = new GeneralsShortcutLayer();
	if (generalsShortcutLayer && generalsShortcutLayer->init(generalDetail))
	{
		generalsShortcutLayer->autorelease();
		return generalsShortcutLayer;
	}
	CC_SAFE_DELETE(generalsShortcutLayer);
	return NULL;
}

bool GeneralsShortcutLayer::init(CGeneralDetail * generalDetail)
{
	if (UIScene::init())
	{
		curGeneralDetail = new CGeneralDetail();
		curGeneralDetail->CopyFrom(*generalDetail);

		layer_shortcut = Layer::create();
		this->addChild(layer_shortcut);

		layer_musou = Layer::create();
		this->addChild(layer_musou);

		for (int i = 0;i<generalDetail->shortcuts_size();++i)
		{
			if (generalDetail->shortcuts(i).complexflag() == 1)
			{
				m_sCurMumouId = generalDetail->shortcuts(i).skillpropid();
			}
		}

		setSkillTypeByGeneralsDetail(generalDetail);
		RefreshGeneralsShortcutSlot(generalDetail);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(GeneralsShortcutLayer::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(GeneralsShortcutLayer::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(GeneralsShortcutLayer::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(GeneralsShortcutLayer::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void GeneralsShortcutLayer::onEnter()
{
	UIScene::onEnter();
}

void GeneralsShortcutLayer::onExit()
{
	UIScene::onExit();
}

bool GeneralsShortcutLayer::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return true;
}

void GeneralsShortcutLayer::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void GeneralsShortcutLayer::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void GeneralsShortcutLayer::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}


void GeneralsShortcutLayer::RefreshGeneralsShortcutSlot( CGeneralDetail * generalDetail )
{
	for (int i = 0;i<5;++i)
	{
		if (layer_shortcut->getChildByTag(ShortSlotBaseTag+i))
		{
			layer_shortcut->getChildByTag(ShortSlotBaseTag+i)->removeFromParent();
		}
	}

	if (generalDetail->shortcuts_size()>0)
	{
		for (int i = 0;i<generalDetail->shortcuts_size();++i)
		{
			auto shortcut = new CShortCut();
			shortcut->CopyFrom(generalDetail->shortcuts(i));
			auto genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->currentquality(),shortcut);
			genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
			genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
			genentalsShortcutSlot->setPosition(slotPos[shortcut->index()]);
			genentalsShortcutSlot->setTag(ShortSlotBaseTag+shortcut->index());
			genentalsShortcutSlot->setSkillProfession(curSkillType);
			layer_shortcut->addChild(genentalsShortcutSlot);

			if (shortcut->complexflag() == 1)
			{

			}
			delete shortcut;
		}
		for (int i = 0;i<5;++i)
		{
			bool isExist = false;
			for (int m = 0;m<generalDetail->shortcuts_size();++m)
			{
				if (i == generalDetail->shortcuts(m).index())
				{
					isExist = true;
					break;
				}
			}

			if(!isExist)
			{
				auto genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->currentquality(),i);
				genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
				genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
				genentalsShortcutSlot->setPosition(slotPos[i]);
				genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
				genentalsShortcutSlot->setSkillProfession(curSkillType);
				layer_shortcut->addChild(genentalsShortcutSlot);
			}
		}
	}
	else
	{
		for (int i = 0;i<5;++i)
		{
			auto genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->currentquality(),i);
			genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
			genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
			genentalsShortcutSlot->setPosition(slotPos[i]);
			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
			genentalsShortcutSlot->setSkillProfession(curSkillType);
			layer_shortcut->addChild(genentalsShortcutSlot);
		}
	}
}

void GeneralsShortcutLayer::setSkillTypeByGeneralsDetail( CGeneralDetail *generalsDetail )
{
	if(generalsDetail->profession() == PROFESSION_MJ_NAME)
	{
		curSkillType = GeneralsSkillsUI::Warrior;
	}
	else if (generalsDetail->profession() == PROFESSION_GM_NAME)
	{
		curSkillType = GeneralsSkillsUI::Magic;
	}
	else if (generalsDetail->profession()  == PROFESSION_HJ_NAME)
	{
		curSkillType = GeneralsSkillsUI::Ranger;
	}
	else if (generalsDetail->profession()  == PROFESSION_SS_NAME)
	{
		curSkillType = GeneralsSkillsUI::Warlock;
	}
}

void GeneralsShortcutLayer::RefreshOneShortcutSlot( long long generalsId,CShortCut * shortCut )
{
	if (curGeneralDetail->generalid() == generalsId)
	{
// 		for (int i = 0;i<curGeneralDetail->shortcuts_size();i++)
// 		{
// 			if (curGeneralDetail->shortcuts(i).index() == shortCut->index())
// 			{
// 				if (curGeneralDetail->shortcuts(i).complexflag() != 1 && shortCut->complexflag() == 1)
// 				{
// 					this->AddMusouWithAnimation(shortCut->index());
// 				}
// 				else if (curGeneralDetail->shortcuts(i).complexflag() == 1 && shortCut->complexflag() == 1)
// 				{
// 					this->AddMusouNormal(shortCut->index());
// 				}
// 				else
// 				{
// 					if (layer_musou->getChildByTag(MuSouSkillIdentityTag) != NULL)
// 					{
// 						layer_musou->getChildByTag(MuSouSkillIdentityTag)->removeFromParent();
// 					}
// 				}
// 
// 				curGeneralDetail->mutable_shortcuts(i)->CopyFrom(*shortCut);
// 				break;
// 			}
// 		}

		if (layer_shortcut->getChildByTag(ShortSlotBaseTag+shortCut->index()))
		{
			layer_shortcut->getChildByTag(ShortSlotBaseTag+shortCut->index())->removeFromParent();
		}

		if (shortCut->has_skillpropid())
		{
			if(strcmp(shortCut->skillpropid().c_str(),"") == 0)
			{
				auto genentalsShortcutSlot = GeneralsShortcutSlot::create(curGeneralDetail->currentquality(),shortCut->index());
				genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
				genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
				genentalsShortcutSlot->setPosition(slotPos[shortCut->index()]);
				genentalsShortcutSlot->setTag(ShortSlotBaseTag+shortCut->index());
				genentalsShortcutSlot->setSkillProfession(curSkillType);
				layer_shortcut->addChild(genentalsShortcutSlot);
			}
			else
			{
				auto genentalsShortcutSlot = GeneralsShortcutSlot::create(curGeneralDetail->rare(),shortCut);
				genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
				genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
				genentalsShortcutSlot->setPosition(slotPos[shortCut->index()]);
				genentalsShortcutSlot->setTag(ShortSlotBaseTag+shortCut->index());
				genentalsShortcutSlot->setSkillProfession(curSkillType);
				layer_shortcut->addChild(genentalsShortcutSlot);

// 				if (shortCut->complexflag() == 1)
// 				{
// 					AddMusouNormal(shortCut->index());
// 				}
			}
		}
		else
		{
			auto genentalsShortcutSlot = GeneralsShortcutSlot::create(curGeneralDetail->currentquality(),shortCut->index());
			genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
			genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
			genentalsShortcutSlot->setPosition(slotPos[shortCut->index()]);
			genentalsShortcutSlot->setTag(ShortSlotBaseTag+shortCut->index());
			genentalsShortcutSlot->setSkillProfession(curSkillType);
			layer_shortcut->addChild(genentalsShortcutSlot);
		}
	}
}

void GeneralsShortcutLayer::RefreshAllShortcutSlot(long long generalsId,std::vector<CShortCut*> shortCutList)
{
	if (curGeneralDetail->generalid() == generalsId)
	{
// 		for (int i = 0;i<shortCutList.size();i++)
// 		{
// 			if (curGeneralDetail->shortcuts(i).index() == shortCutList.at(i)->index())
// 			{
// 				if (curGeneralDetail->shortcuts(i).complexflag() != 1 && shortCutList.at(i)->complexflag() == 1)
// 				{
// 					this->AddMusouWithAnimation(shortCutList.at(i)->index());
// 				}
// 				else if (curGeneralDetail->shortcuts(i).complexflag() == 1 && shortCutList.at(i)->complexflag() == 1)
// 				{
// 					this->AddMusouNormal(shortCutList.at(i)->index());
// 				}
// 				else
// 				{
// 					if (layer_musou->getChildByTag(MuSouSkillIdentityTag) != NULL)
// 					{
// 						layer_musou->getChildByTag(MuSouSkillIdentityTag)->removeFromParent();
// 					}
// 				}
// 
// 				curGeneralDetail->mutable_shortcuts(i)->CopyFrom(*shortCutList.at(i));
// 				break;
// 			}
// 		}

		//delete all old shortcutSlot
		for (int i = 0;i<5;++i)
		{
			if (layer_shortcut->getChildByTag(ShortSlotBaseTag+i))
			{
				layer_shortcut->getChildByTag(ShortSlotBaseTag+i)->removeFromParent();
			}
		}
		//delete MusouFlag
		if (layer_musou->getChildByTag(MuSouSkillIdentityTag) != NULL)
		{
			layer_musou->getChildByTag(MuSouSkillIdentityTag)->removeFromParent();
		}

		if (shortCutList.size()>0)
		{
			for (int i = 0;i<shortCutList.size();++i)
			{
				auto shortcut = new CShortCut();
				shortcut->CopyFrom(*shortCutList.at(i));
				auto genentalsShortcutSlot = GeneralsShortcutSlot::create(curGeneralDetail->currentquality(),shortcut);
				genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
				genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
				genentalsShortcutSlot->setPosition(slotPos[shortcut->index()]);
				genentalsShortcutSlot->setTag(ShortSlotBaseTag+shortcut->index());
				genentalsShortcutSlot->setSkillProfession(curSkillType);
				layer_shortcut->addChild(genentalsShortcutSlot);

// 				if (shortcut->complexflag() == 1)
// 				{
// 					AddMusouNormal(shortcut->index());
// 				}
				delete shortcut;
			}
			for (int i = 0;i<5;++i)
			{
				bool isExist = false;
				for (int m = 0;m<shortCutList.size();++m)
				{
					if (i == shortCutList.at(m)->index())
					{
						isExist = true;
						break;
					}
				}

				if(!isExist)
				{
					auto genentalsShortcutSlot = GeneralsShortcutSlot::create(curGeneralDetail->currentquality(),i);
					genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
					genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
					genentalsShortcutSlot->setPosition(slotPos[i]);
					genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
					genentalsShortcutSlot->setSkillProfession(curSkillType);
					layer_shortcut->addChild(genentalsShortcutSlot);
				}
			}
		}
		else
		{
			for (int i = 0;i<5;++i)
			{
				auto genentalsShortcutSlot = GeneralsShortcutSlot::create(curGeneralDetail->currentquality(),i);
				genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
				genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
				genentalsShortcutSlot->setPosition(slotPos[i]);
				genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
				genentalsShortcutSlot->setSkillProfession(curSkillType);
				layer_shortcut->addChild(genentalsShortcutSlot);
			}
		}
	}
}
// 
// void GeneralsShortcutLayer::InitMusouSkill( CGeneralDetail * generalDetail )
// {
// 	//��˫���ܵ�λ�
// 	int index = -1;
// 	for (int i =0;i<generalDetail->shortcuts_size();++i)
// 	{
// 		if (generalDetail->shortcuts(i).complexflag() == 1)
// 		{
// 			index = generalDetail->shortcuts(i).index();
// 			this->m_sCurMumouId = generalDetail->shortcuts(i).skillpropid();
// 		}
// 	}
// 	
// 	if (index != -1)
// 	{
// 		AddMusouNormal(index);
// // 		if (m_nLastGeneralId == generalDetail->generalid())
// // 		{
// // 			if (strcmp(this->m_sCurMumouId.c_str(),this->m_sLastMumouId.c_str()) != 0)
// // 			{
// // 				//ö���
// // 			}
// // 			else
// // 			{
// // 				AddMusouNormal(index);
// // 			}
// // 		}
// // 		else
// // 		{ 
// // 			AddMusouNormal(index);
// // 		}
// 	}
// }
// 
// void GeneralsShortcutLayer::AddMusouNormal( int index )
// {
// 	if (layer_shortcut->getChildByTag(ShortSlotBaseTag+index))
// 	{
// 		if (layer_musou->getChildByTag(MuSouSkillIdentityTag) != NULL)
// 		{
// 			layer_musou->getChildByTag(MuSouSkillIdentityTag)->removeFromParent();
// 		}
// 
// 		//�����˫��ʶ
// 		Sprite * imageView_musou = Sprite::create("res_ui/longhun.png");
// 		imageView_musou->setAnchorPoint(Vec2(0.5f,0.5f));
// 		imageView_musou->setPosition(Vec2(slotPos[index].x,slotPos[index].y+7));
// 		imageView_musou->setScale(1.1f);
// 		layer_musou->addChild(imageView_musou,0,MuSouSkillIdentityTag);
// 	}
// }
// 
// void GeneralsShortcutLayer::AddMusouWithAnimation( int index )
// {
// 	if (layer_shortcut->getChildByTag(ShortSlotBaseTag+index))
// 	{
//  		CCLOG("AddMusouWithAnimation");
// 		if (layer_musou->getChildByTag(MuSouSkillIdentityTag) != NULL)
// 		{
// 			layer_musou->getChildByTag(MuSouSkillIdentityTag)->removeFromParent();
// 		}
// 
// 		//�����˫��ʶ
// 		Sprite * imageView_musou = Sprite::create("res_ui/longhun.png");
// 		imageView_musou->setAnchorPoint(Vec2(0.5f,0.5f));
// 		imageView_musou->setPosition(Vec2(slotPos[index].x,slotPos[index].y+7));
// 		imageView_musou->setScale(3.0f);
// 		imageView_musou->setVisible(false);
// 		layer_musou->addChild(imageView_musou,0,MuSouSkillIdentityTag);
// 		FiniteTimeAction*  action = Sequence::create(
// 			DelayTime::create(0.5f),
// 			Show::create(),
// 			CCEaseElasticIn::create(ScaleTo::create(0.7f,1.1f)),
// 			NULL);
// 		imageView_musou->runAction(action);
// 	}
// }


void GeneralsShortcutLayer::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralsShortcutLayer::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	// 	UITutorialIndicator * tutorialIndicator = UITutorialIndicator::create();
	// 	tutorialIndicator->setPosition(pos);
	// 	tutorialIndicator->setTag(24);
	// 	m_pLayer->addChild(tutorialIndicator);
	auto tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,50,70);
	tutorialIndicator->setPosition(Vec2(pos.x+50,pos.y+70));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	layer_shortcut->addChild(tutorialIndicator);

// 	CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("tuowei0.plist",50,70);
// 	tutorialParticle->setPosition(Vec2(pos.x,pos.y-35));
// 	tutorialParticle->setTag(CCTUTORIALPARTICLETAG);
// 	layer_shortcut->addChild(tutorialParticle);
}

void GeneralsShortcutLayer::removeCCTutorialIndicator()
{
	// 	UITutorialIndicator* tutorialIndicator = dynamic_cast<UITutorialIndicator*>(m_pLayer->getChildByTag(24));
	// 	if(tutorialIndicator != NULL)
	// 		tutorialIndicator->removeFromParent();
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(layer_shortcut->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto tutorialParticle = dynamic_cast<CCTutorialParticle*>(layer_shortcut->getChildByTag(CCTUTORIALPARTICLETAG));
	if(tutorialParticle != NULL)
		tutorialParticle->removeFromParent();
}
