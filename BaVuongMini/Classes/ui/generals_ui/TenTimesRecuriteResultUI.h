#ifndef  _GENERALSUI_TENTIMESRECURITERESULTUI_H_
#define _GENERALSUI_TENTIMESRECURITERESULTUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "GeneralsListBase.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class CGeneralBaseMsg;
class CGeneralDetail;
class CActionDetail;

/////////////////////////////////
/**
 * �佫�����µġ��㽫̨���е�ʮ������
 * @author yangjun
 * @version 0.1.0
 * @date 2013.12.9
 */

class TenTimesRecuriteResultUI : public UIScene
{
public:
	TenTimesRecuriteResultUI();
	~TenTimesRecuriteResultUI();

	static TenTimesRecuriteResultUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);


	void CloseEvent(Ref *pSender, Widget::TouchEventType type);

	//�ʮ��鷵�
	void TenTimesBackEvent(Ref *pSender, Widget::TouchEventType type);
	//�ʮ������һ�
	void TenTimesAgainEvent(Ref *pSender, Widget::TouchEventType type);

	//�ˢ��ʮ�����ļ�����
	void RefreshTenTimesRecruitData( std::vector<CGeneralDetail *> temp );

	//�ˢ�°�ť����ʾ��
	void RefreshBtnPresent(bool isPresent);

public:
	Layout * panel_tenTimesRecruit;  //�ʮ�������
	Layer * Layer_tenTimesRecruit;

	Layout * panel_btn;
	Button * btn_close;

private:
	Text * lable_tenTimes_gold;

};



////////////////////////////////////////////////////////////////////////////////
/*
* �ʮ��é®��ļ�佫���
*/
class CGeneralDetail;
class GeneralCardItem:public UIScene
{
public:
	GeneralCardItem(void);
	~GeneralCardItem(void);

	static GeneralCardItem * create(CGeneralDetail * generalInfo);
	bool init(CGeneralDetail * generalInfo);
	void onEnter();
	void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	std::string getGeneralIconPath(std::string generalIcon);
	void callBackShowGeneral(Ref * obj);

	void showStarAndProfession();

	void showBonusEffect();

	void generalStarActions();

	void generalProfessionActions();

private:
	ui::ImageView * imageGeneral_bg;
	//ui::ImageView * imageGeneral_icon;
	ui::ImageView * imageGeneral_Star;
	ui::ImageView * imageGeneral_profession;
	Size winSize;
	Layer * u_layer;
};

#endif

