#include "SingleRecuriteResultUI.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "GeneralsUI.h"
#include "GeneralsRecuriteUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "generals_popup_ui/GeneralsFateInfoUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CShortCut.h"
#include "GeneralsShortcutSlot.h"
#include "NormalGeneralsHeadItem.h"
#include "RecuriteActionItem.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "../../messageclient/element/CActionDetail.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "GeneralsListUI.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../legend_engine/CCLegendAnimation.h"

#define HeadItemTag 20
#define ShortSlotBaseTag 40

SingleRecuriteResultUI::SingleRecuriteResultUI()
{
	//curGeneralDetail = new CGeneralDetail();

	slotPos[0].x = 153;
	slotPos[0].y = 138;
	slotPos[1].x = 200;
	slotPos[1].y = 70;
	slotPos[2].x = 250;
	slotPos[2].y = 138;
	slotPos[3].x = 294;
	slotPos[3].y = 70;
	slotPos[4].x = 347;
	slotPos[4].y = 138;
}


SingleRecuriteResultUI::~SingleRecuriteResultUI()
{
	delete curGeneralDetail;
}

SingleRecuriteResultUI* SingleRecuriteResultUI::create()
{
	auto singleRecuriteResultUI = new SingleRecuriteResultUI();
	if (singleRecuriteResultUI && singleRecuriteResultUI->init())
	{
		singleRecuriteResultUI->autorelease();
		return singleRecuriteResultUI;
	}
	CC_SAFE_DELETE(singleRecuriteResultUI);
	return NULL;
}

bool SingleRecuriteResultUI::init()
{
	if (UIScene::init())
	{
		auto winSize = Director::getInstance()->getVisibleSize();
		this->scheduleUpdate();

		curGeneralDetail = new CGeneralDetail();
		//���UI
		if(LoadSceneLayer::SingleRecuriteResultLayer->getParent() != NULL)
		{
			LoadSceneLayer::SingleRecuriteResultLayer->removeFromParentAndCleanup(false);
		}
		//ppanel = (Layout*)LoadSceneLayer::s_pPanel->copy();
		panel_generalsInfo = LoadSceneLayer::SingleRecuriteResultLayer;
		panel_generalsInfo->setAnchorPoint(Vec2(0.0f,0.0f));
		panel_generalsInfo->getVirtualRenderer()->setContentSize(Size(676, 419));
		panel_generalsInfo->setPosition(Vec2::ZERO);
		panel_generalsInfo->setTouchEnabled(true);
		m_pLayer->addChild(panel_generalsInfo);

		panel_btn = (Layout*)Helper::seekWidgetByName(panel_generalsInfo,"Panel_btn");
		panel_btn->setVisible(true);

		auto muScrollView = (cocos2d::extension::ScrollView*)Helper::seekWidgetByName(panel_generalsInfo,"ScrollView_property");
		muScrollView->setTouchEnabled(true);
		muScrollView->setBounceable(true);
		//���ļ�ɹ������ϸ��Ϣ
// 		panel_generalsInfo = (Layout*)Helper::seekWidgetByName(GeneralsUI::ppanel,"Panel_get_generals");
// 		panel_generalsInfo->setVisible(false);
		//��ļ�ɹ���Ĳ
		Layer_generalsInfo = Layer::create();
		addChild(Layer_generalsInfo);
		Btn_close = (Button*)Helper::seekWidgetByName(panel_generalsInfo,"Button_close");
		Btn_close->setTouchEnabled(true);
		Btn_close->setPressedActionEnabled(true);
		Btn_close->addTouchEventListener(CC_CALLBACK_2(SingleRecuriteResultUI::CloseEvent, this));
		//���ļ�ɹ��󷵻
		auto Btn_recruitBack = (Button*)Helper::seekWidgetByName(panel_generalsInfo,"Button_back");
		Btn_recruitBack->setTouchEnabled(true);
		Btn_recruitBack->setPressedActionEnabled(true);
		Btn_recruitBack->addTouchEventListener(CC_CALLBACK_2(SingleRecuriteResultUI::RecruiteBackEvent, this));
		//���ļ�ɹ����ٳ�һ�Σ���Ӧ�ģ�
		auto Btn_recruitAgain = (Button*)Helper::seekWidgetByName(panel_generalsInfo,"Button_again");
		Btn_recruitAgain->setTouchEnabled(true);
		Btn_recruitAgain->setPressedActionEnabled(true);
		Btn_recruitAgain->addTouchEventListener(CC_CALLBACK_2(SingleRecuriteResultUI::RecruiteAgainEvent, this));

		//Button * Btn_Back = (Button *)Helper::seekWidgetByName(panel_generalsInfo,"Label_job"); 
		l_result_profession = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_job"); //ְҵ
		l_result_combatPower = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_job_combat"); //ս�
		l_result_hp = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_hp"); //Ѫ�ֵ
        l_result_hp->enableOutline(Color4B::BLACK, 2.0f);
		Image_result_hp = (ImageView*)Helper::seekWidgetByName(panel_generalsInfo,"ImageView_hp"); //Ѫ�
		l_result_mp = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_mp"); //���ֵ
        l_result_mp->enableOutline(Color4B::BLACK, 2.0f);
		Image_result_mp = (ImageView*)Helper::seekWidgetByName(panel_generalsInfo,"ImageView_mp"); //���
		l_result_powerValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_power");//��
		l_result_aglieValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_aglie");  //��
		l_result_intelligenceValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_intelligence");  //����
		l_result_focusValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_focus");  //רע
		l_result_phyAttackValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_physical_attacks");
		l_result_magicAttackValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_magic_attacks");
		l_result_phyDenValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_power_physical_defense");
		l_result_magicDenValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_power_magic_defense");
		l_result_hitValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_hit");  //���
		l_result_dodgeValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_dodge"); //���
		l_result_critValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_crit");  //ܱ��
		l_result_critDamageValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_crit_damage");//���˺�
		l_result_atkSpeedValue = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_attack_speed");

		btn_fate_1 = (Button*)Helper::seekWidgetByName(panel_generalsInfo,"Button_fate_1");
		btn_fate_1->setTouchEnabled(true);
		btn_fate_1->setPressedActionEnabled(true);
		btn_fate_1->addTouchEventListener(CC_CALLBACK_2(SingleRecuriteResultUI::FateEvent, this));
		btn_fate_2 = (Button*)Helper::seekWidgetByName(panel_generalsInfo,"Button_fate_2");
		btn_fate_2->setTouchEnabled(true);
		btn_fate_2->setPressedActionEnabled(true);
		btn_fate_2->addTouchEventListener(CC_CALLBACK_2(SingleRecuriteResultUI::FateEvent, this));
		btn_fate_3 = (Button*)Helper::seekWidgetByName(panel_generalsInfo,"Button_fate_3");
		btn_fate_3->setTouchEnabled(true);
		btn_fate_3->setPressedActionEnabled(true);
		btn_fate_3->addTouchEventListener(CC_CALLBACK_2(SingleRecuriteResultUI::FateEvent, this));
		btn_fate_4 = (Button*)Helper::seekWidgetByName(panel_generalsInfo,"Button_fate_6");
		btn_fate_4->setTouchEnabled(true);
		btn_fate_4->setPressedActionEnabled(true);
		btn_fate_4->addTouchEventListener(CC_CALLBACK_2(SingleRecuriteResultUI::FateEvent, this));
		btn_fate_5 = (Button*)Helper::seekWidgetByName(panel_generalsInfo,"Button_fate_5");
		btn_fate_5->setTouchEnabled(true);
		btn_fate_5->setPressedActionEnabled(true);
		btn_fate_5->addTouchEventListener(CC_CALLBACK_2(SingleRecuriteResultUI::FateEvent, this));
		btn_fate_6 = (Button*)Helper::seekWidgetByName(panel_generalsInfo,"Button_fate_4");
		btn_fate_6->setTouchEnabled(true);
		btn_fate_6->setPressedActionEnabled(true);
		btn_fate_6->addTouchEventListener(CC_CALLBACK_2(SingleRecuriteResultUI::FateEvent, this));

		l_fate_1 = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_fate_1");
		l_fate_1->setString("");
		l_fate_2 = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_fate_2");
		l_fate_2->setString("");
		l_fate_3 = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_fate_3");
		l_fate_3->setString("");
		l_fate_4 = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_fate_6");
		l_fate_4->setString("");
		l_fate_5 = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_fate_5");
		l_fate_5->setString("");
		l_fate_6 = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_fate_4");
		l_fate_6->setString("");

		lable_singleCost_gold = (Text*)Helper::seekWidgetByName(panel_generalsInfo,"Label_IngotValue");

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(676,419));

		return true;
	}
	return false;
}

void SingleRecuriteResultUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void SingleRecuriteResultUI::onExit()
{
	UIScene::onExit();
}

bool SingleRecuriteResultUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	//return this->resignFirstResponder(touch,this,false);
	return true;
}

void SingleRecuriteResultUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void SingleRecuriteResultUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void SingleRecuriteResultUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void SingleRecuriteResultUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void SingleRecuriteResultUI::RecruiteBackEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void SingleRecuriteResultUI::RecruiteAgainEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GeneralsUI::generalsRecuriteUI->getCurRecuriteActionType() == BASE)
		{
			auto tempdata = new GeneralsRecuriteUI::ReqData();
			tempdata->actionType = BASE;
			if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG))
			{
				auto temp = (RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG);
				if (temp->state == RecuriteActionItem::Free)
				{
					tempdata->recruitType = FREE;
				}
				else
				{
					tempdata->recruitType = GOLD;
				}
			}

			GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
			delete tempdata;
		}
		else if (GeneralsUI::generalsRecuriteUI->getCurRecuriteActionType() == BETTER)
		{
			auto tempdata = new GeneralsRecuriteUI::ReqData();
			tempdata->actionType = BETTER;
			if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BETTERRECURITEACTIONITEMTAG))
			{
				auto temp = (RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BETTERRECURITEACTIONITEMTAG);
				if (temp->state == RecuriteActionItem::Free)
				{
					tempdata->recruitType = FREE;
				}
				else
				{
					tempdata->recruitType = GOLD;
				}
			}

			GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
			delete tempdata;
		}
		else if (GeneralsUI::generalsRecuriteUI->getCurRecuriteActionType() == BEST)
		{
			auto tempdata = new GeneralsRecuriteUI::ReqData();
			tempdata->actionType = BEST;
			if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BESTRECURITEACTIONITEMTAG))
			{
				auto temp = (RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BESTRECURITEACTIONITEMTAG);
				if (temp->state == RecuriteActionItem::Free)
				{
					tempdata->recruitType = FREE;
				}
				else
				{
					tempdata->recruitType = GOLD;
				}
			}

			GameMessageProcessor::sharedMsgProcessor()->sendReq(5061, tempdata);
			delete tempdata;
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void SingleRecuriteResultUI::RefreshRecruiteCost(int actionType)
{
	for (int i =0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		if (GameView::getInstance()->actionDetailList.at(i)->type() == actionType)
		{
			char s_gold[20];
			sprintf(s_gold,"%d",GameView::getInstance()->actionDetailList.at(i)->gold());
			this->lable_singleCost_gold->setString(s_gold);
		}
	}
}

void SingleRecuriteResultUI::RefreshRecruiteResult( CGeneralDetail * generalDetail )
{
	curGeneralDetail->CopyFrom(*generalDetail);

	l_result_profession->setString(generalDetail->profession().c_str()); //ְҵ

	char s_fightpoint[30];
	sprintf(s_fightpoint,"%d",generalDetail->fightpoint());
	l_result_combatPower->setString(s_fightpoint) ; //ս�

	std::string hpValue = "";
	char s_hp[30];
	sprintf(s_hp,"%d",generalDetail->activerole().hp());
	hpValue.append(s_hp);
	hpValue.append("/");
	char s_maxhp[30];
	sprintf(s_maxhp,"%d",generalDetail->activerole().maxhp());
	hpValue.append(s_maxhp);
	l_result_hp->setString(hpValue.c_str());//Ѫ�ֵ

	float hpScaleValue = (generalDetail->activerole().hp()*1.0f)/(generalDetail->activerole().maxhp()*1.0f);
	//Image_result_hp->setScale(hpScaleValue);
	if (hpScaleValue >= 1.0f)
		hpScaleValue = 1.0f;

	Image_result_hp->setTextureRect(Rect(0,0,Image_result_hp->getContentSize().width*hpScaleValue,Image_result_hp->getContentSize().height));


	std::string mpValue = "";
	char s_mp[30];
	sprintf(s_mp,"%d",generalDetail->activerole().mp());
	mpValue.append(s_mp);
	mpValue.append("/");
	char s_maxmp[30];
	sprintf(s_maxmp,"%d",generalDetail->activerole().maxmp());
	mpValue.append(s_maxmp);
	l_result_mp->setString(mpValue.c_str());//���ֵ

	float mpScaleValue = (generalDetail->activerole().mp()*1.0f)/(generalDetail->activerole().maxmp()*1.0f);
	//Image_result_mp->setScale(mpScaleValue);
	if (mpScaleValue >= 1.0f)
		mpScaleValue = 1.0f;

	Image_result_mp->setTextureRect(Rect(0,0,Image_result_mp->getContentSize().width*mpScaleValue,Image_result_mp->getContentSize().height));

	char s_strength[30];
	sprintf(s_strength,"%d",generalDetail->aptitude().strength());
	l_result_powerValue->setString(s_strength) ; //��

	char s_dexterity[30];
	sprintf(s_dexterity,"%d",generalDetail->aptitude().dexterity());
	l_result_aglieValue->setString(s_dexterity) ;  //��

	char s_intelligence[30];
	sprintf(s_intelligence,"%d",generalDetail->aptitude().intelligence());
	l_result_intelligenceValue->setString(s_intelligence) ;  //����

	char s_focus[30];
	sprintf(s_focus,"%d",generalDetail->aptitude().focus());
	l_result_focusValue->setString(s_focus) ;  //רע

	std::string phyAttackValue = "";
	char s_minattack[30];
	sprintf(s_minattack,"%d",generalDetail->aptitude().minattack());
	phyAttackValue.append(s_minattack);
	phyAttackValue.append("-");
	char s_maxattack[30];
	sprintf(s_maxattack,"%d",generalDetail->aptitude().maxattack());
	phyAttackValue.append(s_maxattack);
	l_result_phyAttackValue->setString(phyAttackValue.c_str());

	std::string magicAttackValue = "";
	char s_minmagicattack[30];
	sprintf(s_minmagicattack,"%d",generalDetail->aptitude().minmagicattack());
	magicAttackValue.append(s_minmagicattack);
	magicAttackValue.append("-");
	char s_maxmagicattack[30];
	sprintf(s_maxmagicattack,"%d",generalDetail->aptitude().maxmagicattack());
	magicAttackValue.append(s_maxmagicattack);
	l_result_magicAttackValue->setString(magicAttackValue.c_str());

	std::string phyDenValue = "";
	char s_mindefend[30];
	sprintf(s_mindefend,"%d",generalDetail->aptitude().mindefend());
	phyDenValue.append(s_mindefend);
	phyDenValue.append("-");
	char s_maxdefend[30];
	sprintf(s_maxdefend,"%d",generalDetail->aptitude().maxdefend());
	phyDenValue.append(s_maxdefend);
	l_result_phyDenValue->setString(phyDenValue.c_str());

	std::string magicDenValue = "";
	char s_minmagicdefend[30];
	sprintf(s_minmagicdefend,"%d",generalDetail->aptitude().minmagicdefend());
	magicDenValue.append(s_minmagicdefend);
	magicDenValue.append("-");
	char s_maxmagicdefend[30];
	sprintf(s_maxmagicdefend,"%d",generalDetail->aptitude().maxmagicdefend());
	magicDenValue.append(s_maxmagicdefend);
	l_result_magicDenValue->setString(magicDenValue.c_str());

	char s_aptitude[30];
	sprintf(s_aptitude,"%d",generalDetail->aptitude().hit());
	l_result_hitValue->setString(s_aptitude); //���
	char s_dodge[30];
	sprintf(s_dodge,"%d",generalDetail->aptitude().dodge());
	l_result_dodgeValue->setString(s_dodge); //���
	char s_crit[30];
	sprintf(s_crit,"%d",generalDetail->aptitude().crit());
	l_result_critValue->setString(s_crit); //ܱ��
	char s_critdamage[30];
	sprintf(s_critdamage,"%d",generalDetail->aptitude().critdamage());
	l_result_critDamageValue->setString(s_critdamage); //���˺�
	char s_attackspeed[30];
	sprintf(s_attackspeed,"%d",generalDetail->aptitude().attackspeed());
	l_result_atkSpeedValue->setString(s_attackspeed); //���

	setSkillTypeByGeneralsDetail(generalDetail);
	RefreshGeneralsFate(generalDetail);
//	RefreshGeneralsSkill(generalDetail);
	RefreshGeneralsHead(generalDetail);
}


void SingleRecuriteResultUI::FateEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsFateInfoUI) == NULL)
		{
			auto winsize = Director::getInstance()->getVisibleSize();
			auto generalsFateInfoUI = GeneralsFateInfoUI::create(curGeneralDetail);
			generalsFateInfoUI->setIgnoreAnchorPointForPosition(false);
			generalsFateInfoUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			generalsFateInfoUI->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			GameView::getInstance()->getMainUIScene()->addChild(generalsFateInfoUI, 0, kTagGeneralsFateInfoUI);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void SingleRecuriteResultUI::RefreshGeneralsFate( CGeneralDetail * generalDetail )
{
	l_fate_1->setString("");
	l_fate_2->setString("");
	l_fate_3->setString("");
	l_fate_4->setString("");
	l_fate_5->setString("");
	l_fate_6->setString("");

	btn_fate_1->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_2->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_3->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_4->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_5->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");
	btn_fate_6->loadTextures("res_ui/new_button_1.png","res_ui/new_button_1.png","");

	if (generalDetail->fates_size()>0)
	{
		for (int i = 0;i<generalDetail->fates_size();++i)
		{
			if (i == 0)
			{
				l_fate_1->setString(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_1->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 1)
			{
				l_fate_2->setString(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_2->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 2)
			{
				l_fate_3->setString(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_3->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 3)
			{
				l_fate_4->setString(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_4->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 4)
			{
				l_fate_5->setString(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_5->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
			else if(i == 5)
			{
				l_fate_6->setString(generalDetail->fates(i).fatename().c_str());
				if (generalDetail->fates(i).actived())
				{
					btn_fate_6->loadTextures("res_ui/new_button_2.png","res_ui/new_button_2.png","");
				}
			}
		}
	}
}

void SingleRecuriteResultUI::RefreshGeneralsSkill( CGeneralDetail * generalDetail )
{
	for (int i = 0;i<5;++i)
	{
		if (Layer_generalsInfo->getChildByTag(ShortSlotBaseTag+i))
		{
			Layer_generalsInfo->getChildByTag(ShortSlotBaseTag+i)->removeFromParent();
		}
	}

	if (generalDetail->shortcuts_size()>0)
	{
		for (int i = 0;i<generalDetail->shortcuts_size();++i)
		{
			auto shortcut = new CShortCut();
			shortcut->CopyFrom(generalDetail->shortcuts(i));
			auto genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->rare(),shortcut);
			delete shortcut;
			genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
			genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
			genentalsShortcutSlot->setPosition(slotPos[i]);
			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
			genentalsShortcutSlot->setSkillProfession(curSkillType);
			genentalsShortcutSlot->setTouchEnabled(false);
			Layer_generalsInfo->addChild(genentalsShortcutSlot);
		}
		for (int i = generalDetail->shortcuts_size();i<5;++i)
		{
			auto genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->rare(),i);
			genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
			genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
			genentalsShortcutSlot->setPosition(slotPos[i]);
			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
			genentalsShortcutSlot->setSkillProfession(curSkillType);
			genentalsShortcutSlot->setTouchEnabled(false);
			Layer_generalsInfo->addChild(genentalsShortcutSlot);
		}
	}
	else
	{
		for (int i = 0;i<5;++i)
		{
			auto genentalsShortcutSlot = GeneralsShortcutSlot::create(generalDetail->rare(),i);
			genentalsShortcutSlot->setIgnoreAnchorPointForPosition(false);
			genentalsShortcutSlot->setAnchorPoint(Vec2(0.5f,0.5f));
			genentalsShortcutSlot->setPosition(slotPos[i]);
			genentalsShortcutSlot->setTag(ShortSlotBaseTag+i);
			genentalsShortcutSlot->setSkillProfession(curSkillType);
			genentalsShortcutSlot->setTouchEnabled(false);
			Layer_generalsInfo->addChild(genentalsShortcutSlot);
		}
	}
}

void SingleRecuriteResultUI::RefreshGeneralsHead( CGeneralDetail * generalDetail )
{
	if (Layer_generalsInfo->getChildByTag(HeadItemTag))
	{
		Layer_generalsInfo->getChildByTag(HeadItemTag)->removeFromParent();
	}

	auto headItem = GeneralsHeadItemBase::create(curGeneralDetail->modelid());
	headItem->setAnchorPoint(Vec2(0,0));
	headItem->setPosition(Vec2(120,239));
	headItem->setTag(HeadItemTag);
	Layer_generalsInfo->addChild(headItem);
	
	
	
	/*********************************    first  ************************************/
// 	GeneralsHeadItemBase * headItem = GeneralsHeadItemBase::create(curGeneralDetail->modelid());
// 	headItem->setAnchorPoint(Vec2(0.5f,0.5f));
// 	headItem->setPosition(Vec2(70,190));
// 	headItem->setTag(HeadItemTag);
// 	headItem->setScale(0.01f);
// 	Layer_generalsInfo->addChild(headItem);
// 
// 	ccBezierConfig bezier;
// 	bezier.controlPoint_1 = Vec2(70,190);
// 	bezier.controlPoint_2 = Vec2(384,300);
// 	bezier.endPosition = Vec2(174,280);
// 	FiniteTimeAction * sequence = Sequence::create(
// 		Spawn::create(CCBezierTo::create(1.0f,bezier),ScaleTo::create(1.0f,1.3f),RotateBy::create(1.0f,720,0),NULL),
// 		Spawn::create(DelayTime::create(0.5f),CallFunc::create(this,callfunc_selector(SingleRecuriteResultUI::createParticleSys)),NULL),
// 		Spawn::create(MoveTo::create(0.15f,Vec2(120,239)),ScaleTo::create(0.15f,1.0f),NULL),
// 		NULL);
// 	headItem->runAction(sequence);

	/*********************************    second  ************************************/
	/*
	GeneralsHeadItemBase * headItem = GeneralsHeadItemBase::create(curGeneralDetail->modelid());
	headItem->setAnchorPoint(Vec2(0,0));
	headItem->setPosition(Vec2(120,239));
	headItem->setTag(HeadItemTag);
	headItem->showSilhouetteEffect();
	Layer_generalsInfo->addChild(headItem);

	FiniteTimeAction * sequence = Sequence::create(
		DelayTime::create(0.4f),
		CallFunc::create(this,callfunc_selector(SingleRecuriteResultUI::createParticleSys)),
		NULL);
	headItem->runAction(sequence);

	if (headItem->getLegendHead())
	{
		FiniteTimeAction * action = Sequence::create(
			DelayTime::create(0.8f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		headItem->getLegendHead()->runAction(action);
	}

	FiniteTimeAction * action1 = Sequence::create(
		DelayTime::create(0.8f),
		CCTintTo::create(0.3f, 255,255,255),
		NULL);
	headItem->label_name->runAction(action1);

	FiniteTimeAction * action2 = Sequence::create(
		DelayTime::create(0.8f),
		CCTintTo::create(0.3f, 255,255,255),
		NULL);
	headItem->imageView_star->runAction(action2);

	if (headItem->getFiveStarAnm())
	{
		FiniteTimeAction * action3 = Sequence::create(
			DelayTime::create(0.8f),
			CCTintTo::create(0.3f, 255,255,255),
			NULL);
		headItem->getFiveStarAnm()->runAction(action3);
	}
	*/
}

void SingleRecuriteResultUI::setSkillTypeByGeneralsDetail( CGeneralDetail *generalsDetail )
{
	if(generalsDetail->profession() == PROFESSION_MJ_NAME)
	{
		curSkillType = GeneralsSkillsUI::Warrior;
	}
	else if (generalsDetail->profession() == PROFESSION_GM_NAME)
	{
		curSkillType = GeneralsSkillsUI::Magic;
	}
	else if (generalsDetail->profession()  == PROFESSION_HJ_NAME)
	{
		curSkillType = GeneralsSkillsUI::Ranger;
	}
	else if (generalsDetail->profession()  == PROFESSION_SS_NAME)
	{
		curSkillType = GeneralsSkillsUI::Warlock;
	}
}

void SingleRecuriteResultUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void SingleRecuriteResultUI::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	auto tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,20,40);
	tutorialIndicator->setPosition(Vec2(pos.x-60,pos.y-75));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->addChild(tutorialIndicator);
}

void SingleRecuriteResultUI::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();
}

void SingleRecuriteResultUI::createParticleSys()
{
	//ParticleSystem* particleEffect = ParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo3.plist");
	//particleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	//particleEffect->setPosition(Vec2(225,262));
	//particleEffect->setScale(1.3f);
	//addChild(particleEffect);
	//particleEffect->setVisible(false);
	//FiniteTimeAction * action = Sequence::create(
	//	DelayTime::create(0.0f),
	//	Show::create(),
	//	NULL);
	//particleEffect->runAction(action);

	//ParticleSystem* particleEffect1 = ParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo1.plist");
	//particleEffect1->setPositionType(ParticleSystem::PositionType::GROUPED);
	//particleEffect1->setPosition(Vec2(174+35-45,280+160-30));
	//particleEffect1->setScale(1.3f);
	//addChild(particleEffect1);
	//particleEffect1->setVisible(false);
	//FiniteTimeAction * action1 = Sequence::create(
	//	DelayTime::create(0.15f),
	//	Show::create(),
	//	NULL);
	//particleEffect1->runAction(action1);
	//ParticleSystem* particleEffect2 = ParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo2.plist");
	//particleEffect2->setPositionType(ParticleSystem::PositionType::GROUPED);
	//particleEffect2->setPosition(Vec2(174-20-45,280+80-30));
	//particleEffect2->setScale(1.3f);
	//addChild(particleEffect2);
	//particleEffect2->setVisible(false);
	//FiniteTimeAction * action2 = Sequence::create(
	//	DelayTime::create(0.4f),
	//	Show::create(),
	//	NULL);
	//particleEffect2->runAction(action2);
	//ParticleSystem* particleEffect3 = ParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo3.plist");
	//particleEffect3->setPositionType(ParticleSystem::PositionType::GROUPED);
	//particleEffect3->setPosition(Vec2(174+120-45,280+120-30));
	//particleEffect3->setScale(1.3f);
	//addChild(particleEffect3);
	//particleEffect3->setVisible(false);
	//FiniteTimeAction * action3 = Sequence::create(
	//	DelayTime::create(0.7f),
	//	Show::create(),
	//	NULL);
	//particleEffect3->runAction(action3);

	// Bonus Special Effect
	auto pNode = BonusSpecialEffect::create();
	pNode->setPosition(Vec2(175, 315));
	addChild(pNode);
}

void SingleRecuriteResultUI::RefreshBtnPresent( bool isPresent )
{
	panel_btn->setVisible(isPresent);
}
