#include "TeachAndEvolutionCell.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../utils/StaticDataManager.h"
#include "GeneralsUI.h"
#include "GameView.h"
#include "AppMacros.h"
#include "RecuriteActionItem.h"
#include "../../gamescene_state/role/ActorUtils.h"

#define kTag_Icon_Evolution 44
#define kTag_Icon_Rare 45

TeachAndEvolutionCell::TeachAndEvolutionCell()
{
}


TeachAndEvolutionCell::~TeachAndEvolutionCell()
{
}

TeachAndEvolutionCell* TeachAndEvolutionCell::create(CGeneralBaseMsg * generalBaseMsg)
{
	auto teachAndEvolutionCell = new TeachAndEvolutionCell();
	if (teachAndEvolutionCell && teachAndEvolutionCell->init(generalBaseMsg))
	{
		teachAndEvolutionCell->autorelease();
		return teachAndEvolutionCell;
	}
	CC_SAFE_DELETE(teachAndEvolutionCell);
	return NULL;
}

bool TeachAndEvolutionCell::init(CGeneralBaseMsg * generalBaseMsg)
{
	if (TableViewCell::init())
	{
		auto generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];
		curGeneralBaseMsg = generalBaseMsg;
		//��߿
		sprite_bigFrame = cocos2d::extension::Scale9Sprite::create("res_ui/kuang01_new.png");
		sprite_bigFrame->setAnchorPoint(Vec2(0, 0));
		sprite_bigFrame->setPosition(Vec2(0, 0));
		sprite_bigFrame->setCapInsets(Rect(15,31,1,1));
		sprite_bigFrame->setContentSize(Size(237,69));
		addChild(sprite_bigFrame);
		//�С�߿
		auto smaillFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_allb.png");
		smaillFrame->setAnchorPoint(Vec2(0, 0));
		smaillFrame->setPosition(Vec2(12, 8));
		smaillFrame->setCapInsets(Rect(15,15,1,1));
		smaillFrame->setContentSize(Size(52,52));
		sprite_bigFrame->addChild(smaillFrame);

		auto sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_diaa.png");
		sprite_nameFrame->setAnchorPoint(Vec2(0, 0));
		sprite_nameFrame->setPosition(Vec2(83, 19));
		sprite_nameFrame->setCapInsets(Rect(11,11,1,1));
		sprite_nameFrame->setContentSize(Size(146,30));
		sprite_bigFrame->addChild(sprite_nameFrame);

		//����ɫ��С�߿
// 		Rect cr = Rect(9.5f,9.5f,6.0f,6.0f);
// 		auto sprite_smallFrame = cocos2d::extension::Scale9Sprite::create(GeneralsUI::getSmallHeadFramePath(generalBaseMsg->currentquality()).c_str());
// 		sprite_smallFrame->setAnchorPoint(Vec2(0, 0));
// 		sprite_smallFrame->setPosition(Vec2(12,12));
// 		sprite_smallFrame->setPreferredSize(Size(51,61));
// 		sprite_smallFrame->setCapInsets(cr);
// 		sprite_bigFrame->addChild(sprite_smallFrame);

		//��б��е�ͷ��ͼ�
		std::string icon_path = "res_ui/generals46X45/";
		icon_path.append(generalMsgFromDb->get_head_photo());
		icon_path.append(".png");
		sprite_icon = Sprite::create(icon_path.c_str());
		sprite_icon->setAnchorPoint(Vec2(0.5f, 0.5f));
		sprite_icon->setPosition(Vec2(38, 33));
		sprite_bigFrame->addChild(sprite_icon);
// 		//�ȼ�
		auto sprite_lvFrame = Sprite::create("res_ui/lv_kuang.png");
		sprite_lvFrame->setAnchorPoint(Vec2(0, 0));
		sprite_lvFrame->setPosition(Vec2(49, 4));
		sprite_bigFrame->addChild(sprite_lvFrame);
		//�佫�ȼ�
		std::string _lv = "LV";
		char general_lv[5];
		sprintf(general_lv,"%d",generalBaseMsg->level());
		_lv.append(general_lv);
		label_lv = Label::createWithTTF(general_lv,APP_FONT_NAME,12);
		auto shadowColor = Color4B::BLACK;   // black
		label_lv->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		label_lv->setAnchorPoint(Vec2(0.5f,0.5f));
		label_lv->setPosition(Vec2(63,11));
		sprite_bigFrame->addChild(label_lv);

		//���
		if (generalBaseMsg->evolution() > 0)
		{
			auto imageView_rank = ActorUtils::createGeneralRankSprite(generalBaseMsg->evolution());
			imageView_rank->setAnchorPoint(Vec2(1.0f,1.0f));
			imageView_rank->setPosition(Vec2(74,66));
			imageView_rank->setScale(0.75f);
			imageView_rank->setTag(kTag_Icon_Evolution);
			sprite_bigFrame->addChild(imageView_rank);
		}
		//�ϡ�ж
		if (generalBaseMsg->rare()>0)
		{
			auto imageView_star = Sprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
			imageView_star->setAnchorPoint(Vec2(0.5f,0.5f));
			imageView_star->setPosition(Vec2(15,15));
			imageView_star->setScale(0.5f);
			imageView_star->setTag(kTag_Icon_Rare);
			sprite_bigFrame->addChild(imageView_star);
		}

		//��佫���
		label_name = Label::createWithTTF(generalMsgFromDb->name().c_str(),APP_FONT_NAME,16);
		label_name->setAnchorPoint(Vec2(0.0f, 0.5f));
		label_name->setPosition(Vec2(93, 33));
		label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		sprite_bigFrame->addChild(label_name);
		//��佫Ʒ�
		std::string general_rank = GeneralsUI::getQuality(generalBaseMsg->currentquality());
		label_rank = Label::createWithTTF(general_rank.c_str(),APP_FONT_NAME,16);
		label_rank->setAnchorPoint(Vec2(0, 0.5f));
		label_rank->setPosition(Vec2(160, 33));
		label_rank->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
		sprite_bigFrame->addChild(label_rank);

// 		if (generalBaseMsg->rare()>0)
// 		{
// 			Sprite * temp = Sprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
// 			temp->setAnchorPoint(Vec2(0.5f,0.5f));
// 			temp->setPosition(Vec2(232,35));
// 			sprite_bigFrame->addChild(temp);
// 		}

		sprite_bigFrame_mengban = cocos2d::extension::Scale9Sprite::create("res_ui/mengban_black65.png");
		sprite_bigFrame_mengban->setAnchorPoint(Vec2(0, 0));
		sprite_bigFrame_mengban->setPosition(Vec2(0, 0));
		sprite_bigFrame_mengban->setPreferredSize(Size(237,69));
		addChild(sprite_bigFrame_mengban);
		sprite_bigFrame_mengban->setVisible(false);
		
		return true;
	}
	return false;
}

CGeneralBaseMsg* TeachAndEvolutionCell::getCurGeneralBaseMsg()
{
	return this->curGeneralBaseMsg;
}

void TeachAndEvolutionCell::setGray( bool able )
{
	sprite_bigFrame_mengban->setVisible(able);
}

void TeachAndEvolutionCell::refreshCell( CGeneralBaseMsg * generalBaseMsg )
{
	auto generalMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];
	curGeneralBaseMsg = generalBaseMsg;

	//��б��е�ͷ��ͼ�
	std::string icon_path = "res_ui/generals46X45/";
	icon_path.append(generalMsgFromDb->get_head_photo());
	icon_path.append(".png");
	auto texture = Director::getInstance()->getTextureCache()->addImage(icon_path.c_str());
	sprite_icon->setTexture(texture);

	//��佫�ȼ�
	std::string _lv = "LV";
	char general_lv[5];
	sprintf(general_lv,"%d",generalBaseMsg->level());
	_lv.append(general_lv);
	label_lv->setString(general_lv);
	//���
	if (sprite_bigFrame->getChildByTag(kTag_Icon_Evolution))
	{
		sprite_bigFrame->getChildByTag(kTag_Icon_Evolution)->removeFromParent();
	}
	if (generalBaseMsg->evolution() > 0)
	{
		auto imageView_rank = ActorUtils::createGeneralRankSprite(generalBaseMsg->evolution());
		imageView_rank->setAnchorPoint(Vec2(1.0f,1.0f));
		imageView_rank->setPosition(Vec2(74,67));
		imageView_rank->setScale(0.75f);
		imageView_rank->setTag(kTag_Icon_Evolution);
		sprite_bigFrame->addChild(imageView_rank);
	}
	//�ϡ�ж
	if (sprite_bigFrame->getChildByTag(kTag_Icon_Rare))
	{
		sprite_bigFrame->getChildByTag(kTag_Icon_Rare)->removeFromParent();
	}
	if (generalBaseMsg->rare()>0)
	{
		auto imageView_star = Sprite::create(RecuriteActionItem::getStarPathByNum(generalBaseMsg->rare()).c_str());
		imageView_star->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_star->setPosition(Vec2(15,15));
		imageView_star->setScale(0.5f);
		imageView_star->setTag(kTag_Icon_Rare);
		sprite_bigFrame->addChild(imageView_star);
	}

	//��佫���
	label_name->setString(generalMsgFromDb->name().c_str());
	label_name->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));
	//��佫Ʒ�
	std::string general_rank = GeneralsUI::getQuality(generalBaseMsg->currentquality());
	label_rank->setString(general_rank.c_str());
	label_rank->setColor(GameView::getInstance()->getGeneralsColorByQuality(generalBaseMsg->currentquality()));

	setGray(false);
}



