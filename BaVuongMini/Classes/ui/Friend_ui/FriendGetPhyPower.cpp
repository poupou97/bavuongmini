#include "FriendGetPhyPower.h"
#include "../../messageclient/element/CPlayerGetPhypower.h"
#include "../../GameView.h"
#include "FriendInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "FriendInfoList.h"
#include "../../ui/GameUIConstant.h"
#include "cocostudio\CCSGUIReader.h"

FriendGetPhyPower::FriendGetPhyPower(void)
{
	lastSelectCellId = 0;
	curVectorIndex = 0;
}


FriendGetPhyPower::~FriendGetPhyPower(void)
{
}

FriendGetPhyPower * FriendGetPhyPower::create(int count)
{
	auto phyPower_ = new FriendGetPhyPower();
	if (phyPower_ && phyPower_->init(count))
	{
		phyPower_->autorelease();
		return phyPower_;
	}
	CC_SAFE_DELETE(phyPower_);
	return NULL;
}

bool FriendGetPhyPower::init(int count)
{
	if (UIScene::init())
	{
		winsize= Director::getInstance()->getVisibleSize();
		m_getPhypowerCount = count;
		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		auto mainPanel=(Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/lingtili_1.json");
		mainPanel->setAnchorPoint(Vec2(0.5f,0.5f));
		mainPanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(mainPanel);

		setPhypowerTouchCellType(phypower_ktypeTouchCellEvent);

		remPhyPowerCount_ = (Text*)Helper::seekWidgetByName(mainPanel,"Label_PhyPowerCount");
		char string_count[10];
		sprintf(string_count,"%d",count);
		remPhyPowerCount_->setString(string_count);

		Label_phypowerNull = (Text*)Helper::seekWidgetByName(mainPanel,"Label_notphyPowerCount");
		Label_phypowerNull->setVisible(false);

		phypower_layer= Layer::create();
		phypower_layer->setIgnoreAnchorPointForPosition(false);
		phypower_layer->setAnchorPoint(Vec2(0.5f,0.5f));
		phypower_layer->setPosition(Vec2(winsize.width/2,winsize.height/2));
		phypower_layer->setContentSize(Size(800,480));
		this->addChild(phypower_layer,10);


		auto btn_close = (Button *)Helper::seekWidgetByName(mainPanel,"Button_close");
		btn_close->setTouchEnabled(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->addTouchEventListener(CC_CALLBACK_2(FriendGetPhyPower::callBackClose, this));

		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void FriendGetPhyPower::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	phypowerTavleView = TableView::create(this, Size(682, 315));
	phypowerTavleView->setDirection(TableView::Direction::VERTICAL);
	phypowerTavleView->setAnchorPoint(Vec2(0,0));
	phypowerTavleView->setPosition(Vec2(60,100));
	phypowerTavleView->setDelegate(this);
	phypowerTavleView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	phypower_layer->addChild(phypowerTavleView);
}

void FriendGetPhyPower::onExit()
{
	UIScene::onExit();
}

void FriendGetPhyPower::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{

}

void FriendGetPhyPower::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

void FriendGetPhyPower::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	int cellId = cell->getIdx();
	
	if (table->cellAtIndex(lastSelectCellId) != NULL)
	{
		auto friendCell = (FriendInfo *)table->cellAtIndex(lastSelectCellId)->getChildByTag(lastSelectCellTag);
		if (friendCell != NULL)
		{
			if (friendCell->getChildByTag(1313) != NULL)
			{
				friendCell->getChildByTag(1313)->removeFromParent();
			}
		}
	}

	auto curPhyower_ = (FriendInfo *)table->cellAtIndex(cellId)->getChildByTag(curSelectCellTag);
	if (curPhyower_ == NULL)
	{
		return;
	}
	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(15, 30, 1, 1) , "res_ui/highlight.png");
	pHighlightSpr->setPreferredSize(Size(322,80));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setTag(1313);
	pHighlightSpr->setPosition(Vec2(18,1));
	curPhyower_->addChild(pHighlightSpr,20);

	if (getPhypowerTouchCellType() == phypower_ktypeTouchCellEvent)
	{
		auto infolist = FriendInfoList::create(7);
		infolist->setIgnoreAnchorPointForPosition(false);
		infolist->setAnchorPoint(Vec2(0,0.5f));
		infolist->setLocalZOrder(10);
		infolist->setPosition(Vec2(100,infolist->getContentSize().height+50));
		phypower_layer->addChild(infolist);
	}
	
	setPhypowerTouchCellType(phypower_ktypeTouchCellEvent);
	lastSelectCellTag = curSelectCellTag;
	lastSelectCellId =cell->getIdx();
}

cocos2d::Size FriendGetPhyPower::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(671,80);
}

cocos2d::extension::TableViewCell* FriendGetPhyPower::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell =table->dequeueCell();   // this method must be called

	cell=new TableViewCell();
	cell->autorelease();

	int phypowerSize = GameView::getInstance()->playerPhypowervector.size();
	
	if (idx == phypowerSize/2)
	{
		if (phypowerSize%2 == 0)
		{
			auto friendInfoL =FriendInfo::create(2*idx,kTagGetphyPowerUi);
			friendInfoL->setAnchorPoint(Vec2(0.5f,0.5f));
			friendInfoL->setPosition(Vec2(0,0));
			friendInfoL->setTag(phypowerCellLeft);
			cell->addChild(friendInfoL);

			auto friendInfoR =FriendInfo::create(2*idx+1,kTagGetphyPowerUi);
			friendInfoR->setAnchorPoint(Vec2(0.5f,0.5f));
			friendInfoR->setPosition(Vec2(325,0));
			friendInfoR->setTag(phypowerCellRight);
			cell->addChild(friendInfoR);
		}else
		{
			auto friendInfoL =FriendInfo::create(2*idx,kTagGetphyPowerUi);
			friendInfoL->setAnchorPoint(Vec2(0.5f,0.5f));
			friendInfoL->setPosition(Vec2(0,0));
			friendInfoL->setTag(phypowerCellLeft);
			cell->addChild(friendInfoL);
		}
	}
	else
	{
		auto friendInfoL =FriendInfo::create(2*idx,kTagGetphyPowerUi);
		friendInfoL->setAnchorPoint(Vec2(0.5f,0.5f));
		friendInfoL->setPosition(Vec2(0,0));
		friendInfoL->setTag(phypowerCellLeft);
		cell->addChild(friendInfoL);

		auto friendInfoR =FriendInfo::create(2*idx+1,kTagGetphyPowerUi);
		friendInfoR->setAnchorPoint(Vec2(0.5f,0.5f));
		friendInfoR->setPosition(Vec2(325,0));
		friendInfoR->setTag(phypowerCellRight);
		cell->addChild(friendInfoR);
	}
	return cell;
}

ssize_t FriendGetPhyPower::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	int vectorSize =  GameView::getInstance()->playerPhypowervector.size();

	if (vectorSize%2 == 0)
	{
		return vectorSize/2;
	}else
	{
		return vectorSize/2 + 1;
	}
}

bool FriendGetPhyPower::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return resignFirstResponder(pTouch,this,false);
}

void FriendGetPhyPower::callBackClose(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void FriendGetPhyPower::setPhypowerTouchCellType( int type_ )
{
	m_curTouchType = type_;
}

int FriendGetPhyPower::getPhypowerTouchCellType()
{
	return m_curTouchType;
}

void FriendGetPhyPower::setShowPhypowerlabel( bool isShow )
{
	Label_phypowerNull->setVisible(isShow);
}

