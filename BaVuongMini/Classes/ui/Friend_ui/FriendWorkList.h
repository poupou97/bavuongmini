#ifndef _UI_FRIEND_FRIENDWORKLIST_H_
#define _UI_FRIEND_FRIENDWORKLIST_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "../generals_ui/GeneralsListBase.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
typedef enum
{
	FRIENDWORKLISTTAB = 503,
	FRIENDINFOCELLL = 508,
	FRIENDINFOCELLR = 509
};

class FriendWorkList:public GeneralsListBase,public cocos2d::extension::TableViewDataSource,public cocos2d::extension::TableViewDelegate
{
public:
	FriendWorkList(void);
	~FriendWorkList(void);

	struct FriendStruct
	{
		int relationType;
		int pageNum;
		int pageIndex;
		int showOnline;
	};

	static FriendWorkList *create();

	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	// default implements are used to call script callback if exist
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);


	void ReloadTableViewWithoutChangeOffSet();

public:
	TableView *tableView;
	int totalPageNum;
	int lastSelectCellId;
	int lastSelectCellTag;
private:
	Size winSize;
	Layer * layer_friend;
	int friendNum;
};

#endif

