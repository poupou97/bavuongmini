#include "TeamMemberList.h"
#include "../extensions/UITab.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../Chat_ui/ChatUI.h"
#include "../Chat_ui/AddPrivateUi.h"
#include "../extensions/RichTextInput.h"
#include "../../GameView.h"
#include "../../messageclient/element/CMapTeam.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CTeamMember.h"
#include "../extensions/QuiryUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Mail_ui/MailUI.h"
#include "../../gamescene_state/MainScene.h"
#include "AppMacros.h"


TeamMemberList::TeamMemberList(void)
{
}


TeamMemberList::~TeamMemberList(void)
{
	delete m_teammenber;
}

TeamMemberList * TeamMemberList::create(CTeamMember * teammenber,long long leaderid)
{
	auto list=new TeamMemberList();
	if (list && list->init(teammenber,leaderid))
	{
		list->autorelease();
		return list;
	}
	CC_SAFE_DELETE(list);
	return NULL;
}

bool TeamMemberList::init(CTeamMember * teammenber,long long leaderid)
{
	if (UIScene::init())
	{
		auto playerBg=Button::create();
		playerBg->setScale9Enabled(true);
		playerBg->setContentSize(Size(284,55));
		playerBg->setCapInsets(Rect(15,30,1,1));
		playerBg->setTouchEnabled(true);
		playerBg->loadTextures("res_ui/kuang01_new.png","res_ui/kuang01_new.png","");
		playerBg->setAnchorPoint(Vec2(0,0));
		playerBg->setPosition(Vec2(0,0));
		playerBg->setTag(teammenber->roleid());
		playerBg->addTouchEventListener(CC_CALLBACK_2(TeamMemberList::callBackUitab, this));
		m_pLayer->addChild(playerBg);
		
		std::string teamName_=teammenber->name();
		int pressionid_=teammenber->profession();
		int level_=teammenber->level();
		long long playerId_ =teammenber->roleid();
		int vipLevel_ =  teammenber->viplevel();
		int isOnline_ = teammenber->isonline();
		//////icon name pression lv
		auto playerIconBg_ =ImageView::create();
		playerIconBg_->loadTexture("res_ui/LV4_all.png");
		playerIconBg_->setAnchorPoint(Vec2(0.5f,0.5f));
		playerIconBg_->setPosition(Vec2(40,26));
		playerIconBg_->setScale(0.8f);
		playerBg->addChild(playerIconBg_);

		std::string icon_path =  BasePlayer::getHeadPathByProfession(pressionid_);
		auto playerIcon_ =ImageView::create();
		playerIcon_->loadTexture(icon_path.c_str());
		playerIcon_->setAnchorPoint(Vec2(0.5f,0.5f));
		playerIcon_->setPosition(Vec2(-6,1));
		playerIcon_->setScale(0.8f);
		playerIconBg_->addChild(playerIcon_);

		char memberLevelStr[10];
		sprintf(memberLevelStr,"%d",level_);
		auto label_memberLv_ =Label::createWithTTF(memberLevelStr, APP_FONT_NAME, 14);
		label_memberLv_->setAnchorPoint(Vec2(1,0));
		label_memberLv_->setPosition(Vec2(playerIconBg_->getContentSize().width/2 - 5, - playerIconBg_->getContentSize().height/2));
		playerIconBg_->addChild(label_memberLv_);

		if (playerId_ == leaderid)
		{
			auto playerIcon_ =ImageView::create();
			playerIcon_->loadTexture("res_ui/dui.png");
			playerIcon_->setAnchorPoint(Vec2(0,0));
			playerIcon_->setPosition(Vec2(3,30));
			playerBg->addChild(playerIcon_);
		}
		
		auto label_teamName =Label::createWithTTF(teamName_.c_str(), APP_FONT_NAME, 16);
		label_teamName->setAnchorPoint(Vec2(0,0));
		label_teamName->setPosition(Vec2(75,20));
		playerBg->addChild(label_teamName);

		if (vipLevel_ > 0)
		{
			auto vipWidge = MainScene::addVipInfoByLevelForWidget(vipLevel_);
			playerBg->addChild(vipWidge);
			vipWidge->setPosition(Vec2(label_teamName->getPosition().x+ label_teamName->getContentSize().width + vipWidge->getContentSize().width/2,
										label_teamName->getPosition().y + vipWidge->getContentSize().height/2));
		}

		std::string pressionName_ = BasePlayer::getProfessionNameIdxByIndex(pressionid_);
		auto label_pression =Label::createWithTTF(pressionName_.c_str(), APP_FONT_NAME, 16);
		label_pression->setAnchorPoint(Vec2(0,0));
		label_pression->setPosition(Vec2(180,20));
		playerBg->addChild(label_pression);
		/*
		char string_lv[5];
		sprintf(string_lv,"%d",level_);
		std::string playerlevelStr_ = "LV";
		playerlevelStr_.append(string_lv);
		*/
		std::string onlineString_ = "";
		if (isOnline_ == 1)
		{
			const char *  onLine_ = StringDataManager::getString("Arena_roleStype_online");
			onlineString_.append(onLine_);
		}else
		{
			const char *  onLine_ = StringDataManager::getString("Arena_roleStype_notOnline");
			onlineString_.append(onLine_);

			auto onLineSpIcon_ =ImageView::create();
			onLineSpIcon_->loadTexture("res_ui/haoyouzhezhao.png");
			onLineSpIcon_->setScale9Enabled(true);
			onLineSpIcon_->setContentSize(Size(284,55));
			onLineSpIcon_->setCapInsets(Rect(10,10,1,1));
			onLineSpIcon_->setAnchorPoint(Vec2(0,0));
			onLineSpIcon_->setPosition(Vec2(0,0));
			playerBg->addChild(onLineSpIcon_);
			onLineSpIcon_->setLocalZOrder(10);
		}
		
		auto label_onLine =Label::createWithTTF(onlineString_.c_str(), APP_FONT_NAME, 16);
		label_onLine->setAnchorPoint(Vec2(0,0));
		label_onLine->setPosition(Vec2(230,20));
		playerBg->addChild(label_onLine);
		
		m_teammenber=new CTeamMember();
		m_teammenber->CopyFrom(*teammenber);
		m_leaderid = leaderid;

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(TeamMemberList::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(TeamMemberList::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(TeamMemberList::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(TeamMemberList::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);
		
		return true;
	}
	return false;
}

void TeamMemberList::onEnter()
{
	UIScene::onEnter();
}

void TeamMemberList::onExit()
{
	UIScene::onExit();
	
}

void TeamMemberList::callBackUitab(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

	}
	break;

	case Widget::TouchEventType::CANCELED:
	{
		//if touched is self return false
		auto button_ = (Button *)pSender;
		int numIdx_ = button_->getTag();

		long long selfId = GameView::getInstance()->myplayer->getRoleId();
		if (selfId != numIdx_)
		{
			Size winsize = Director::getInstance()->getVisibleSize();
			TeamMemberListOperator * listOperator = TeamMemberListOperator::create(m_teammenber, m_leaderid);
			listOperator->setIgnoreAnchorPointForPosition(false);
			listOperator->setAnchorPoint(Vec2(0.5f, 0.5f));
			//listOperator->setPosition(Vec2(0,0));
			GameView::getInstance()->getMainUIScene()->addChild(listOperator);
			listOperator->setPosition(Vec2(winsize.width / 2 - 20, winsize.height / 2 + listOperator->getContentSize().height));
			/*
			if (selfId == m_leaderid)
			{
			//listOperator->setPosition(Vec2(winsize.width/2,550));
			listOperator->setPosition(Vec2(winsize.width/2- 50,winsize.height/2+listOperator->getContentSize().height));
			}else
			{
			listOperator->setPosition(Vec2(435,395));
			}
			*/
		}
	}
		break;

	default:
		break;
	}
}

bool TeamMemberList::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	Vec2 location = pTouch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}
	
	return false;
}

void TeamMemberList::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void TeamMemberList::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void TeamMemberList::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}



///////////////////////////////////
TeamMemberListOperator::TeamMemberListOperator( void )
{

}

TeamMemberListOperator::~TeamMemberListOperator( void )
{
	delete mm_teammenber;
}

TeamMemberListOperator * TeamMemberListOperator::create( CTeamMember * teammenber,long long leaderid )
{
	auto listOPerator =new TeamMemberListOperator();
	if (listOPerator && listOPerator->init(teammenber,leaderid))
	{
		listOPerator->autorelease();
		return listOPerator;
	}
	CC_SAFE_DELETE(listOPerator);
	return NULL;
}

bool TeamMemberListOperator::init( CTeamMember * teammenber,long long leaderid )
{
	if (UIScene::init())
	{
		winSize=Director::getInstance()->getVisibleSize();

		const char *strings_addfriend = StringDataManager::getString("friend_addfriend");
		char *addfriend_left=const_cast<char*>(strings_addfriend);

		const char *strings_delete = StringDataManager::getString("friend_delete");
		char *delete_left=const_cast<char*>(strings_delete);

		const char *strings_check = StringDataManager::getString("friend_check");
		char *check_left=const_cast<char*>(strings_check);

		const char *strings_team = StringDataManager::getString("friend_team");
		char *team_left=const_cast<char*>(strings_team);

		const char *strings_mail = StringDataManager::getString("friend_mail");
		char *mail_left=const_cast<char*>(strings_mail);

		const char *strings_black = StringDataManager::getString("friend_black");
		char *black_left=const_cast<char*>(strings_black);

		const char *strings_private = StringDataManager::getString("chatui_private");
		char *private_left=const_cast<char*>(strings_private);

		const char *strings_enemy = StringDataManager::getString("friend_enemy");
		char *enemy_left=const_cast<char*>(strings_enemy);

		const char *strings_track = StringDataManager::getString("friend_track");
		char *track_left=const_cast<char*>(strings_track);

		const char *strings_kickout = StringDataManager::getString("team_leaderKick");
		char *kickout_left=const_cast<char*>(strings_kickout);

		const char *strings_leaderout = StringDataManager::getString("team_leaderOut");
		char *leaderout_left=const_cast<char*>(strings_leaderout);

		long long selfid= GameView::getInstance()->myplayer->getRoleId();	
		if (selfid == leaderid)
		{
			const char * Chaneel_normalImage="res_ui/new_button_5.png";
			const char * Channel_selectImage = "res_ui/new_button_5.png";
			const char * Channel_finalImage = "";
			const char * Chaneel_highLightImage="res_ui/new_button_5.png";

			char * label[] = {private_left,check_left,leaderout_left,kickout_left,mail_left,addfriend_left,black_left};
			UITab* friendTab=UITab::createWithText(7,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
			friendTab->setAnchorPoint(Vec2(0,0));
			friendTab->setPosition(Vec2(0,0));
			friendTab->setHighLightImage((char *) Chaneel_highLightImage);
			friendTab->setDefaultPanelByIndex(0);
			friendTab->setAutoClose(true);
			friendTab->addIndexChangedEvent(this,coco_indexchangedselector(TeamMemberListOperator::callBack1));
			friendTab->setPressedActionEnabled(true);
			m_pLayer->addChild(friendTab);

			setContentSize(Size(103,280));
		}else
		{
			const char * Chaneel_normalImage="res_ui/new_button_5.png";
			const char * Channel_selectImage = "res_ui/new_button_5.png";
			const char * Channel_finalImage = "";
			const char * Chaneel_highLightImage="res_ui/new_button_5.png";

			char * label[] = {private_left,check_left,mail_left,addfriend_left,black_left};
			auto friendTab=UITab::createWithText(5,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
			friendTab->setAnchorPoint(Vec2(0,0));
			friendTab->setPosition(Vec2(0,0));
			friendTab->setHighLightImage((char *) Chaneel_highLightImage);
			friendTab->setDefaultPanelByIndex(0);
			friendTab->setAutoClose(true);
			friendTab->addIndexChangedEvent(this,coco_indexchangedselector(TeamMemberListOperator::callBack2));
			friendTab->setPressedActionEnabled(true);
			m_pLayer->addChild(friendTab);

			setContentSize(Size(103,200));
		}
		mm_teammenber =new CTeamMember();
		mm_teammenber->CopyFrom(*teammenber);

		//setTouchEnabled(true);
		//setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(TeamMemberListOperator::onTouchBegan, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void TeamMemberListOperator::onEnter()
{
	UIScene::onEnter();
}

void TeamMemberListOperator::onExit()
{
	UIScene::onExit();
}

bool TeamMemberListOperator::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return this->resignFirstResponder(pTouch,this,false,false);
}

void TeamMemberListOperator::callBack1( Ref * obj )
{
	int num= ((UITab *) obj)->getCurrentIndex();
	long long memberid = mm_teammenber->roleid();
	std::string memberName = mm_teammenber->name();
	removeFromParentAndCleanup(true);
	switch(num)
	{
	case 0:
		{
			long long id_ = mm_teammenber->roleid();
			std::string name_ = mm_teammenber->name();
			int countryId = 0;
			int vipLv = mm_teammenber->viplevel();
			int lv = mm_teammenber->level();
			int pressionId = mm_teammenber->profession();

			auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(id_,name_,countryId,vipLv,lv,pressionId);

			/*
			ChatUI * chatui_ = ChatUI::create();
			chatui_->setIgnoreAnchorPointForPosition(false);
			chatui_->setAnchorPoint(Vec2(0.5f,0.5f));
			chatui_->setPosition(Vec2(winSize.width/2,winSize.height/2));
			chatui_->setTag(kTabChat);
			GameView::getInstance()->getMainUIScene()->addChild(chatui_);

			chatui_->selectChannelId =0;
			const char *str_private = StringDataManager::getString("chatui_private");
			char *private_left=const_cast<char*>(str_private);
			chatui_->labelchanel->setText(private_left);
			chatui_->channelLabel->setDefaultPanelByIndex(6);
			chatui_->addPrivate();


			std::string friendName_= memberName;
			AddPrivateUi * privateui=(AddPrivateUi *)chatui_->layer_3->getChildByTag(PRIVATEUI);
			privateui->textBox_private->onTextFieldInsertText(NULL,friendName_.c_str(),30);
			*/
		}break;
	case 1:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)memberid);
		}break;
	case 2:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)1,(void *)memberid);
		}break;
	case 3:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)0,(void *)memberid);
		}break;
	case 4:
		{
		auto mail_ui=MailUI::create();
			mail_ui->setIgnoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(Vec2(0.5f,0.5f));
			mail_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001, (void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			std::string friendName_= memberName;
			mail_ui->friendName->onTextFieldInsertText(NULL,friendName_.c_str(),30);
		}break;
	case 5:
		{
			FriendStruct friend1={0,0,memberid,memberName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	case 6:
		{
			FriendStruct friend1={0,1,memberid,memberName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	}	
}

void TeamMemberListOperator::callBack2( Ref * obj )
{
	int num= ((UITab *) obj)->getCurrentIndex();
	long long memberid = mm_teammenber->roleid();
	std::string memberName = mm_teammenber->name();
	removeFromParentAndCleanup(true);
	switch(num)
	{
	case 0:
		{
			long long id_ = mm_teammenber->roleid();
			std::string name_ = mm_teammenber->name();
			int countryId = 0;
			int vipLv = mm_teammenber->viplevel();
			int lv = mm_teammenber->level();
			int pressionId = mm_teammenber->profession();

			auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(id_,name_,countryId,vipLv,lv,pressionId);



			/*
			ChatUI * chatui_ = ChatUI::create();
			chatui_->setIgnoreAnchorPointForPosition(false);
			chatui_->setAnchorPoint(Vec2(0.5f,0.5f));
			chatui_->setPosition(Vec2(winSize.width/2,winSize.height/2));
			chatui_->setTag(kTabChat);
			GameView::getInstance()->getMainUIScene()->addChild(chatui_);

			chatui_->selectChannelId =0;
			const char *str_private = StringDataManager::getString("chatui_private");
			char *private_left=const_cast<char*>(str_private);
			chatui_->labelchanel->setText(private_left);
			chatui_->channelLabel->setDefaultPanelByIndex(6);
			chatui_->addPrivate();


			std::string friendName_= memberName;
			AddPrivateUi * privateui=(AddPrivateUi *)chatui_->layer_3->getChildByTag(PRIVATEUI);
			privateui->textBox_private->onTextFieldInsertText(NULL,friendName_.c_str(),30);
			*/
		}break;
	case 1:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)memberid);
		}break;
	case 2:
		{
			auto mail_ui=MailUI::create();
			mail_ui->setIgnoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(Vec2(0.5f,0.5f));
			mail_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001, (void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			std::string friendName_= memberName;
			mail_ui->friendName->onTextFieldInsertText(NULL,friendName_.c_str(),30);
		}break;
	case 3:
		{
			FriendStruct friend1={0,0,memberid,memberName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	case 4:
		{
			FriendStruct friend1={0,1,memberid,memberName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	}	
}


