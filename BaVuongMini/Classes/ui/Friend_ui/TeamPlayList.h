#ifndef _UI_FRIEND_TEAMPLAYERLIST_H_
#define _UI_FRIEND_TEAMPLAYERLIST_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"

class TeamPlayList:public UIScene
{
public:
	TeamPlayList(void);
	~TeamPlayList(void);

	static TeamPlayList *create();
	bool init();

	void onEnter();
	void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void callBack(Ref * obj);
};

#endif

