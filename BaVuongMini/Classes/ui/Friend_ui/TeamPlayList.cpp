#include "TeamPlayList.h"
#include "../extensions/UITab.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../Chat_ui/ChatUI.h"
#include "../Chat_ui/AddPrivateUi.h"
#include "../extensions/RichTextInput.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"

TeamPlayList::TeamPlayList(void)
{
}


TeamPlayList::~TeamPlayList(void)
{
}

TeamPlayList * TeamPlayList::create()
{
	auto list=new TeamPlayList();
	if (list && list->init())
	{
		list->autorelease();
		return list;
	}
	CC_SAFE_DELETE(list);
	return NULL;
}

bool TeamPlayList::init()
{
	if (UIScene::init())
	{

		const char * Chaneel_normalImage="res_ui/button_6_on.png";
		const char * Channel_selectImage = "";
		const char * Channel_finalImage = "";
		const char * Chaneel_highLightImage="res_ui/button_6_on.png";
		//char * label[] = {"friend","private","check","invite","black","transfer","kickOut"};
		char * label[] = {"kickOut","transfer","black","invite","check","private","friend"};
		//UITab *tabSelectButton=UITab::createWithText(7,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-5);
		auto tabSelectButton=UITab::createWithText(7,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-5);
		tabSelectButton->setAnchorPoint(Vec2(0,0));
		tabSelectButton->setPosition(Vec2(500,50));
		tabSelectButton->setHighLightImage((char *) Chaneel_highLightImage);
		tabSelectButton->setDefaultPanelByIndex(0);
		tabSelectButton->setAutoClose(true);
		tabSelectButton->addIndexChangedEvent(this,coco_indexchangedselector(TeamPlayList::callBack));
		tabSelectButton->setPressedActionEnabled(true);
		m_pLayer->addChild(tabSelectButton);


		//setTouchEnabled(true);
		//setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		setContentSize(Size(100,240));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(TeamPlayList::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(TeamPlayList::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(TeamPlayList::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(TeamPlayList::onTouchCancelled, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void TeamPlayList::onEnter()
{
	UIScene::onEnter();
}

void TeamPlayList::onExit()
{
	UIScene::onExit();
	
}

void TeamPlayList::callBack( Ref * obj )
{
	int num= ((UITab *) obj)->getCurrentIndex();

	switch(num)
	{
	case 0:
		{
			//kcickout
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)0);
		}break;
	case 1:
		{
			//transfer
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)1);
		}break;
	case 2:
		{
			//blcak
// 			friend_ui->operation_=0;
// 			friend_ui->relationtype_=1;
// 			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
// 			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();
//			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,(void *));
		}break;
	case 3:
		{
			//invite
			//GameMessageProcessor::sharedMsgProcessor()->sendReq(1401,this);
		}break;
	case 4:
		{
			//check

		}break;
	case 5:
		{
			/*
			long long id_ = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			std::string name_ = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();
			int countryId = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->country();
			int vipLv = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->viplevel();
			int lv = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->level();
			int pressionId = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->profession();

			MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(id_,name_,countryId,vipLv,lv,pressionId);
			*/

			
			Size winSize=Director::getInstance()->getVisibleSize();
			auto chatui_ =ChatUI::create();
			chatui_->setIgnoreAnchorPointForPosition(false);
			chatui_->setAnchorPoint(Vec2(0.5f,0.5f));
			chatui_->setPosition(Vec2(winSize.width/2,winSize.height/2));
			chatui_->setTag(kTabChat);
			Director::getInstance()->getRunningScene()->addChild(chatui_, 100);
			//chatui_->tabSelectButton->setDefaultPanelByIndex(3);
			//chatui_->tabSelectButton->setHighLightImage();
			//chatui_->selectButton_channel(chatui_->tabSelectButton);  private


			//friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			//friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();

			//std::string friendName_=friend_ui->playerName_;
			auto privateui=(AddPrivateUi *)chatui_->layer_3->getChildByTag(PRIVATEUI);
			privateui->textBox_private->onTextFieldInsertText(NULL,"private",30);
			
		}break;
	case 6:
		{
			//friend
// 			friend_ui->operation_=0;
// 			friend_ui->relationtype_=0;
// 			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
// 			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();
			//GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,this);
		}break;
	}

}

bool TeamPlayList::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return this->resignFirstResponder(pTouch,this,false);
}

void TeamPlayList::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void TeamPlayList::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void TeamPlayList::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}


