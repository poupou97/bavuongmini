#ifndef _UI_FRIEND_AROUNDPLAYERTABVIEW_H_
#define _UI_FRIEND_AROUNDPLAYERTABVIEW_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

typedef enum
{
	AROUNDPLAYERTABVIEW = 510,
	PLAYEROTHERFRIENDCELLL = 528,
	PLAYEROTHERFRIENDCELLR = 529
};

class AroundPlayerTabview:public UIScene,public cocos2d::extension::TableViewDataSource,public cocos2d::extension::TableViewDelegate
{
public:
	AroundPlayerTabview(void);
	~AroundPlayerTabview(void);

	static AroundPlayerTabview * create();
	bool init();
	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	// default implements are used to call script callback if exist
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);


	//void callBack(Ref * obj);

	int lastSelectCellId;
	int lastSelectCellTag;
	
};

#endif

