#include "AroundPlayerTabviewCell.h"
#include "../extensions/CCMoveableMenu.h"
#include "GameView.h"
#include "../../messageclient/element/CAroundPlayer.h"
#include "FriendUi.h"
#include "FriendInfoList.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../gamescene_state/MainScene.h"

AroundPlayerTabviewCell::AroundPlayerTabviewCell(void)
{
}


AroundPlayerTabviewCell::~AroundPlayerTabviewCell(void)
{
}

AroundPlayerTabviewCell * AroundPlayerTabviewCell::create(int idx)
{
	auto aroundplayer= new AroundPlayerTabviewCell();
	if (aroundplayer && aroundplayer->init(idx))
	{
		aroundplayer->autorelease();
		return aroundplayer;
	}
	CC_SAFE_DELETE(aroundplayer);
	return NULL;
}

bool AroundPlayerTabviewCell::init(int idx)
{
	if (UIScene::init())
	{
		selectCellId =idx;

		std::string roleName_= GameView::getInstance()->aroundPlayerVector.at(idx)->rolename();
		long long roldId_= GameView::getInstance()->aroundPlayerVector.at(idx)->roleid();
		int profession_ =GameView::getInstance()->aroundPlayerVector.at(idx)->profession();
		int level_= GameView::getInstance()->aroundPlayerVector.at(idx)->level();
		auto imageBackGround = cocos2d::extension::Scale9Sprite::create("res_ui/kuang01_new.png");
		imageBackGround->setPreferredSize(Size(320,78));
		imageBackGround->setCapInsets(Rect(15,30,1,1));
		imageBackGround->setLocalZOrder(0);

		auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
		auto itemFriend = MenuItemSprite::create(imageBackGround, imageBackGround, imageBackGround,
			CC_CALLBACK_1(AroundPlayerTabviewCell::callBack,this));
		itemFriend->setScale(1.0f);
		auto select_friendL=CCMoveableMenu::create(itemFriend,NULL);
		select_friendL->setAnchorPoint(Vec2(0,0));
		select_friendL->setPosition(Vec2(20+imageBackGround->getContentSize().width/2,35));
		addChild(select_friendL);

		auto iconBackGround = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_allb.png");
		iconBackGround->setPreferredSize(Size(60,59));//62
		iconBackGround->setAnchorPoint(Vec2(0.5f,0.5f));
		iconBackGround->setPosition(Vec2(59,35.5f));//60 35
		addChild(iconBackGround);

		std::string playerIcon_ = BasePlayer::getHeadPathByProfession(profession_);
		auto friendHead =Sprite::create(playerIcon_.c_str());
		friendHead->setAnchorPoint(Vec2(0.5f,0.5f));
		friendHead->setScale(0.8f);
		friendHead->setPosition(Vec2(59,34));//59 35
		addChild(friendHead);

		auto label_playName = Label::createWithTTF(roleName_.c_str(), APP_FONT_NAME, 16);
		label_playName->setAnchorPoint(Vec2(0,0));
		label_playName->setPosition(Vec2(150,40));
		addChild(label_playName,10);

		int vipLevel = GameView::getInstance()->aroundPlayerVector.at(idx)->viplevel();
		if (vipLevel > 0)
		{
			auto vipNode = MainScene::addVipInfoByLevelForNode(vipLevel);
			addChild(vipNode);
			vipNode->setPosition(Vec2(label_playName->getPositionX() + label_playName->getContentSize().width + vipNode->getContentSize().width/2,
											label_playName->getPositionY()+ vipNode->getContentSize().height/2));
		}


		std::string pression_name =BasePlayer::getProfessionNameIdxByIndex(profession_);
		const char *pressStr_ = StringDataManager::getString("friend_pressionName");
		std::string pressionStr_ =pressStr_;
		pressionStr_.append(pression_name);

		auto label_playprofession = Label::createWithTTF(pressionStr_.c_str(), APP_FONT_NAME, 16);
		label_playprofession->setAnchorPoint(Vec2(0,0));
		label_playprofession->setPosition(Vec2(115,15));
		addChild(label_playprofession,10);

		char string_lv[5];
		sprintf(string_lv,"%d",level_);
		const char *levelstr_ = StringDataManager::getString("friend_playerLevel");
		std::string playerlevelStr_ =levelstr_;
		playerlevelStr_.append(string_lv);

		auto label_level = Label::createWithTTF(playerlevelStr_.c_str(), APP_FONT_NAME, 16);
		label_level->setAnchorPoint(Vec2(0,0));
		label_level->setPosition(Vec2(230,15));
		addChild(label_level,10);

		return true;
	}
	return false;
}

void AroundPlayerTabviewCell::onEnter()
{
	UIScene::onEnter();
}

void AroundPlayerTabviewCell::onExit()
{
	UIScene::onExit();
}

void AroundPlayerTabviewCell::callBack( Ref * obj )
{
	auto friendui =(FriendUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
 	friendui->cellIdSelect_ = selectCellId;
	friendui->cellSelectTag = this->getTag();
}


