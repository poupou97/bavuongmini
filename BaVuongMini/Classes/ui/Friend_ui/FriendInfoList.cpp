#include "FriendInfoList.h"
#include "../Chat_ui/ChatUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../Mail_ui/MailUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "FriendUi.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../Chat_ui/AddPrivateUi.h"
#include "../../messageclient/element/CRelationPlayer.h"
#include "../extensions/UITab.h"
#include "../extensions/RichTextInput.h"
#include "../../messageclient/element/CAroundPlayer.h"
#include "../extensions/QuiryUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Chat_ui/PrivateChatUi.h"
#include "../../gamescene_state/MainScene.h"
#include "FriendGetPhyPower.h"
#include "../../messageclient/element/CPlayerGetPhypower.h"
#include "../giveFlower/GiveFlower.h"

FriendInfoList::FriendInfoList(void)
{
	
}


FriendInfoList::~FriendInfoList(void)
{
}

FriendInfoList * FriendInfoList::create(int currType)
{
	auto infoList =new FriendInfoList();
	if (infoList && infoList->init(currType))
	{
		infoList->autorelease();
		return infoList;
	}
	CC_SAFE_DELETE(infoList);
	return NULL;
}

bool FriendInfoList::init(int currType)
{
	if (UIScene::init())
	{
		const char *str_addfriend = StringDataManager::getString("friend_addfriend");
		char *addfriend_left=const_cast<char*>(str_addfriend);	

		const char *str_delete = StringDataManager::getString("friend_delete");
		char *delete_left=const_cast<char*>(str_delete);

		const char *str_check = StringDataManager::getString("friend_check");
		char *check_left=const_cast<char*>(str_check);

		const char *str_team = StringDataManager::getString("friend_team");
		char *team_left=const_cast<char*>(str_team);

		const char *str_mail = StringDataManager::getString("friend_mail");
		char *mail_left=const_cast<char*>(str_mail);

		const char *str_black = StringDataManager::getString("friend_black");
		char *black_left=const_cast<char*>(str_black);

		const char *str_private = StringDataManager::getString("chatui_private");
		char *private_left=const_cast<char*>(str_private);

		const char *str_enemy = StringDataManager::getString("friend_enemy");
		char *enemy_left=const_cast<char*>(str_enemy);

		const char *str_track = StringDataManager::getString("friend_track");
		char *track_left=const_cast<char*>(str_track);

		const char *str_kickout = StringDataManager::getString("team_leaderKick");
		char *kickout_left=const_cast<char*>(str_kickout);

		const char *str_leaderout = StringDataManager::getString("team_leaderOut");
		char *leaderout_left=const_cast<char*>(str_leaderout);

		const char *str_giveFlower = StringDataManager::getString("friend_giveFlower");
		char *giveFlower_left=const_cast<char*>(str_giveFlower);

		switch(currType)
		{
		case 0:
			{
				const char * Chaneel_normalImage="res_ui/new_button_5.png";
				const char * Channel_selectImage = "res_ui/new_button_5.png";
				const char * Channel_finalImage = "";
				const char * Chaneel_highLightImage="res_ui/new_button_5.png";
				char * label[] = {private_left,check_left,team_left,delete_left,mail_left,black_left,giveFlower_left};
				auto friendTab=UITab::createWithText(7,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);

				//char * label[] = {private_left,check_left,team_left,delete_left,mail_left,black_left};
				//UITab* friendTab=UITab::createWithText(6,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
				friendTab->setAnchorPoint(Vec2(0,0));
				friendTab->setPosition(Vec2(0,200));
				friendTab->setHighLightImage((char *) Chaneel_highLightImage);
				friendTab->setDefaultPanelByIndex(0);
				friendTab->setAutoClose(true);
				friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FriendInfoList::callBackFriendList));
				friendTab->setPressedActionEnabled(true);
				m_pLayer->addChild(friendTab);
				
			}break;
		case 1:
			{
				const char * Chaneel_normalImage="res_ui/new_button_5.png";
				const char * Channel_selectImage = "res_ui/new_button_5.png";
				const char * Channel_finalImage = "";
				const char * Chaneel_highLightImage="res_ui/new_button_5.png";
				char * label[] = {check_left,enemy_left,delete_left,addfriend_left};
				auto friendTab=UITab::createWithText(4,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
				friendTab->setAnchorPoint(Vec2(0,0));
				friendTab->setPosition(Vec2(0,200));
				friendTab->setHighLightImage((char *) Chaneel_highLightImage);
				friendTab->setDefaultPanelByIndex(0);
				friendTab->setAutoClose(true);
				friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FriendInfoList::callBackBackList));
				friendTab->setPressedActionEnabled(true);
				m_pLayer->addChild(friendTab);

			}break;
		case 2:
			{
				const char * Chaneel_normalImage="res_ui/new_button_5.png";
				const char * Channel_selectImage = "res_ui/new_button_5.png";
				const char * Channel_finalImage = "";
				const char * Chaneel_highLightImage="res_ui/new_button_5.png";
				//char * label[] = {check_left,black_left,delete_left,track_left};
				char * label[] = {check_left,black_left,delete_left};
				auto friendTab=UITab::createWithText(3,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
				friendTab->setAnchorPoint(Vec2(0,0));
				friendTab->setPosition(Vec2(0,200));
				friendTab->setHighLightImage((char *) Chaneel_highLightImage);
				friendTab->setDefaultPanelByIndex(0);
				friendTab->setAutoClose(true);
				friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FriendInfoList::callBackEnemyList));
				friendTab->setPressedActionEnabled(true);
				m_pLayer->addChild(friendTab);
				
			}break;
		case 3:
			{
				/*
				//让位 加好友 私聊 查看 拉人踢出 拉黑
				const char * Chaneel_normalImage="res_ui/button_10.png";
				const char * Channel_selectImage = "res_ui/button_10.png";
				const char * Channel_finalImage = "";
				const char * Chaneel_highLightImage="res_ui/button_10.png";

				char * label[] = {track_left,delete_left,black_left,check_left};
				UITab* friendTab=UITab::createWithText(4,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-5);
				friendTab->setAnchorPoint(Vec2(0,0));
				friendTab->setPosition(Vec2(10,5));
				friendTab->setHighLightImage((char *) Chaneel_highLightImage);
				friendTab->setDefaultPanelByIndex(3);
				friendTab->setAutoClose(true);
				friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FriendInfoList::callBackEnemyList));
				m_pLayer->addChild(friendTab);
				*/
			}break;
		case 4:
			{
				const char * Chaneel_normalImage="res_ui/new_button_5.png";
				const char * Channel_selectImage = "res_ui/new_button_5.png";
				const char * Channel_finalImage = "";
				const char * Chaneel_highLightImage="res_ui/new_button_5.png";

				char * label[] = {private_left,check_left,team_left,addfriend_left,black_left};
				auto friendTab=UITab::createWithText(5,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
				friendTab->setAnchorPoint(Vec2(0,0));
				friendTab->setPosition(Vec2(10,250));
				friendTab->setHighLightImage((char *) Chaneel_highLightImage);
				friendTab->setDefaultPanelByIndex(3);
				friendTab->setAutoClose(true);
				friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FriendInfoList::callBackAroundPlayer));
				friendTab->setPressedActionEnabled(true);
				m_pLayer->addChild(friendTab);
			}break;
		case 5:
			{
				const char * Chaneel_normalImage="res_ui/new_button_5.png";
				const char * Channel_selectImage = "res_ui/new_button_5.png";
				const char * Channel_finalImage = "";
				const char * Chaneel_highLightImage="res_ui/new_button_5.png";
				char * label[] = {private_left,check_left,team_left,addfriend_left,mail_left,black_left};
				auto friendTab=UITab::createWithText(6,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
				friendTab->setAnchorPoint(Vec2(0,0));
				friendTab->setPosition(Vec2(0,200));
				friendTab->setHighLightImage((char *) Chaneel_highLightImage);
				friendTab->setDefaultPanelByIndex(0);
				friendTab->setAutoClose(true);
				friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FriendInfoList::callBackCheckList));
				friendTab->setPressedActionEnabled(true);
				m_pLayer->addChild(friendTab);
			}break;
		case 6:
			{
				const char * Chaneel_normalImage="res_ui/new_button_5.png";
				const char * Channel_selectImage = "res_ui/new_button_5.png";
				const char * Channel_finalImage = "";
				const char * Chaneel_highLightImage="res_ui/new_button_5.png";
				char * label[] = {private_left,check_left,team_left,addfriend_left,mail_left,black_left};
				auto friendTab=UITab::createWithText(6,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
				friendTab->setAnchorPoint(Vec2(0,0));
				friendTab->setPosition(Vec2(0,200));
				friendTab->setHighLightImage((char *) Chaneel_highLightImage);
				friendTab->setDefaultPanelByIndex(0);
				friendTab->setAutoClose(true);
				friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FriendInfoList::callBackCheckList));
				friendTab->setPressedActionEnabled(true);
				m_pLayer->addChild(friendTab);
			}break;
		case 7:
			{
				const char * Chaneel_normalImage="res_ui/new_button_5.png";
				const char * Channel_selectImage = "res_ui/new_button_5.png";
				const char * Channel_finalImage = "";
				const char * Chaneel_highLightImage="res_ui/new_button_5.png";
				char * label[] = {private_left,check_left,team_left,addfriend_left,mail_left,black_left};
				auto friendTab=UITab::createWithText(6,Chaneel_normalImage,Channel_selectImage,Channel_finalImage,label,VERTICAL,-2);
				friendTab->setAnchorPoint(Vec2(0,0));
				friendTab->setPosition(Vec2(0,200));
				friendTab->setHighLightImage((char *) Chaneel_highLightImage);
				friendTab->setDefaultPanelByIndex(0);
				friendTab->setAutoClose(true);
				friendTab->addIndexChangedEvent(this,coco_indexchangedselector(FriendInfoList::callBackPhypower));
				friendTab->setPressedActionEnabled(true);
				m_pLayer->addChild(friendTab);
			}break;
		}
 		//this->setTouchEnabled(true);
 		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
 		this->setContentSize(Size(123,300));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(FriendInfoList::onTouchBegan, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}

	return false;
}

void FriendInfoList::onEnter()
{
	UIScene::onEnter();
}

void FriendInfoList::onExit()
{
	UIScene::onExit();
}

void FriendInfoList::callBackFriendList( Ref * obj )
{
	auto tabFriendList=(UITab *)obj;
	int num= tabFriendList->getCurrentIndex();
	auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	Size winSize =Director::getInstance()->getVisibleSize();
	switch(num)
	{
	case 0:
		{
			int online_ = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->online();
			if (online_ == 0)
			{
				const char *strings_ = StringDataManager::getString("friend_playerNotLineOfChat");
				GameView::getInstance()->showAlertDialog(strings_);
			}else
			{
				long long id_ = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
				std::string name_ = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();
				int countryId = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->country();
				int vipLv = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->viplevel();
				int lv = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->level();
				int pressionId = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->profession();

				auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
				mainscene_->addPrivateChatUi(id_,name_,countryId,vipLv,lv,pressionId);
				
				/*
				ChatUI * chatui_ = ChatUI::create();
				chatui_->setIgnoreAnchorPointForPosition(false);
				chatui_->setAnchorPoint(Vec2(0.5f,0.5f));
				chatui_->setPosition(Vec2(winSize.width/2,winSize.height/2));
				chatui_->setTag(kTabChat);
				GameView::getInstance()->getMainUIScene()->addChild(chatui_);

				chatui_->selectChannelId =0;
				const char *str_private = StringDataManager::getString("chatui_private");
				char *private_left=const_cast<char*>(str_private);
				chatui_->labelchanel->setText(private_left);
				chatui_->channelLabel->setDefaultPanelByIndex(6);
				chatui_->addPrivate();

				friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
				friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();

				std::string friendName_=friend_ui->playerName_;
				AddPrivateUi * privateui=(AddPrivateUi *)chatui_->layer_3->getChildByTag(PRIVATEUI);
				privateui->textBox_private->onTextFieldInsertText(NULL,friendName_.c_str(),30);
				*/
			}
		}break;
	case 1:
		{
			//playerid  
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			long long selectId=friend_ui->playerId_;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)selectId);
		}break;
	case 2:
		{
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			long long selectId=friend_ui->playerId_;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1401,(void *)selectId);
			
		}break;
	case 3:
		{
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();

			const char *strings = StringDataManager::getString("friend_deleteFriendSure");
			GameView::getInstance()->showPopupWindow(strings,2,this,callfuncO_selector(FriendInfoList::deleteFriendSure),NULL);
		}break;
	case 4:
		{
			auto mail_ui=MailUI::create();
			mail_ui->setIgnoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(Vec2(0.5f,0.5f));
			mail_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001, (void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();

			mail_ui->receiverId =friend_ui->playerId_;
			mail_ui->receiverName = friend_ui->playerName_;

			std::string friendName_=friend_ui->playerName_;
			mail_ui->friendName->onTextFieldInsertText(NULL,friendName_.c_str(),30);
		}break;
	case 5:
		{
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();

			const char *strings = StringDataManager::getString("friend_addblackSure");
			GameView::getInstance()->showPopupWindow(strings,2,this,callfuncO_selector(FriendInfoList::backListFriendSure),NULL);

		}break;
	case 6:
		{
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();
			int pressionId = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->profession();
			int countryId = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->country();
			int vipLv = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->viplevel();
			int lv = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->level();
			
			auto flower_ = GiveFlower::create(friend_ui->playerId_,friend_ui->playerName_,pressionId,lv,countryId,vipLv);
			flower_->setIgnoreAnchorPointForPosition(false);
			flower_->setAnchorPoint(Vec2(0.5f,0.5f));
			flower_->setPosition(Vec2(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(flower_);
		}break;
	}
	this->removeFromParentAndCleanup(false);
}
void FriendInfoList::callBackBackList( Ref * obj )
{
	auto tabFriendList=(UITab *)obj;
	int num= tabFriendList->getCurrentIndex();
	auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	Size winSize= Director::getInstance()->getVisibleSize();
	switch(num)
	{
	case 0:
		{
			long long playerId = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)playerId);	
		}break;
	case 1:
		{
			friend_ui->operation_=0;
			friend_ui->relationtype_=2;
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();
			FriendStruct friend1={0,2,friend_ui->playerId_,friend_ui->playerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);	
		}break;
	case 2:
		{

			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();

			const char *strings = StringDataManager::getString("friend_deleteFriendSure");
			GameView::getInstance()->showPopupWindow(strings,2,this,callfuncO_selector(FriendInfoList::deleteFriendSure),NULL);
		}break;
	case 3:
		{
			friend_ui->operation_=0;
			friend_ui->relationtype_=0;
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();
			FriendStruct friend1={0,0,friend_ui->playerId_,friend_ui->playerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);	

			std::vector<CRelationPlayer *>::iterator iter=GameView::getInstance()->relationSourceVector.begin()+friend_ui->cellIdSelect_;
			CRelationPlayer * player=*iter;
			GameView::getInstance()->relationSourceVector.erase(iter);
			delete player;
		}break;
	}
	this->removeFromParentAndCleanup(false);
}

void FriendInfoList::callBackEnemyList( Ref * obj )
{
	auto tabFriendList=(UITab *)obj;
	int num= tabFriendList->getCurrentIndex();
	auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	Size winSize= Director::getInstance()->getVisibleSize();
	switch(num)
	{
	case 0:
		{
			long long playerid = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)playerid);	
		}break;
	case 1:
		{
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();

			const char *strings = StringDataManager::getString("friend_addblackSure");
			GameView::getInstance()->showPopupWindow(strings,2,this,callfuncO_selector(FriendInfoList::backListFriendSure),NULL);
		}break;
	case 2:
		{
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();

			const char *strings = StringDataManager::getString("friend_deleteFriendSure");
			GameView::getInstance()->showPopupWindow(strings,2,this,callfuncO_selector(FriendInfoList::deleteFriendSure),NULL);
		}break;
	case 3:
		{
			const char *strings_ = StringDataManager::getString("feature_will_be_open");
			GameView::getInstance()->showAlertDialog(strings_);
		}break;
	}
	this->removeFromParentAndCleanup(false);
}



void FriendInfoList::deleteFriendSure( Ref * obj )
{
	auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	friend_ui->operation_=1;
	friend_ui->relationtype_= friend_ui->curType;
	FriendStruct friend1={1,friend_ui->relationtype_,friend_ui->playerId_,friend_ui->playerName_};
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);

	std::vector<CRelationPlayer *>::iterator iter=GameView::getInstance()->relationSourceVector.begin()+friend_ui->cellIdSelect_;
	CRelationPlayer * player=*iter;
	GameView::getInstance()->relationSourceVector.erase(iter);
	delete player;
}

void FriendInfoList::backListFriendSure( Ref * obj )
{
	auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	friend_ui->operation_=0;
	friend_ui->relationtype_=friend_ui->curType;
	FriendStruct friend1={0,1,friend_ui->playerId_,friend_ui->playerName_};
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);	
	//if curtype is friend delete
	if (friend_ui->curType == 0)
	{
		std::vector<CRelationPlayer *>::iterator iter=GameView::getInstance()->relationSourceVector.begin()+friend_ui->cellIdSelect_;
		CRelationPlayer * player=*iter;
		GameView::getInstance()->relationSourceVector.erase(iter);
		delete player;
	}
}


bool FriendInfoList::onTouchBegan(Touch *touch, Event * pEvent)
{
	return resignFirstResponder(touch,this,false,false);
}

void FriendInfoList::callBackAroundPlayer( Ref * obj )
{
	auto tabFriendList=(UITab *)obj;
	int num= tabFriendList->getCurrentIndex();
	auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	Size winSize=Director::getInstance()->getVisibleSize();
	switch(num)
	{
	case 0:
		{

			long long id_ = GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->roleid();
			std::string name_ = GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->rolename();
			int countryId = 0;
			int vipLv = GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->viplevel();
			int lv = GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->level();
			int pressionId = GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->profession();

			auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(id_,name_,countryId,vipLv,lv,pressionId);

			/*
			Size winSize=Director::getInstance()->getVisibleSize();
			ChatUI * chatui_ =ChatUI::create();
			chatui_->setIgnoreAnchorPointForPosition(false);
			chatui_->setAnchorPoint(Vec2(0.5f,0.5f));
			chatui_->setPosition(Vec2(winSize.width/2,winSize.height/2));
			chatui_->setTag(kTabChat);
			GameView::getInstance()->getMainUIScene()->addChild(chatui_);

			chatui_->selectChannelId =0;
			const char *str_private = StringDataManager::getString("chatui_private");
			char *private_left=const_cast<char*>(str_private);
			chatui_->labelchanel->setText(private_left);
			chatui_->channelLabel->setDefaultPanelByIndex(6);
			chatui_->addPrivate();
			friend_ui->playerId_=GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->roleid();
			friend_ui->playerName_=GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->rolename();
			std::string friendName_=friend_ui->playerName_;
			AddPrivateUi * privateui=(AddPrivateUi *)chatui_->layer_3->getChildByTag(PRIVATEUI);
			privateui->textBox_private->onTextFieldInsertText(NULL,friendName_.c_str(),30);
		*/
		}break;
	case 1:
		{
			long long selectId= GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->roleid();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)selectId);

		}break;
	case 2:
		{
			friend_ui->playerId_=GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->roleid();
			long long selectId= friend_ui->playerId_;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1401,(void *)selectId);
		}break;
	case 3:
		{
			friend_ui->operation_=0;
			friend_ui->relationtype_=0;
			friend_ui->playerId_=GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->roleid();
			friend_ui->playerName_=GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->rolename();
			FriendStruct friend1={0,0,friend_ui->playerId_,friend_ui->playerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);	
		}break;
	case 4:
		{
			friend_ui->playerId_=GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->roleid();
			friend_ui->playerName_=GameView::getInstance()->aroundPlayerVector.at(friend_ui->cellIdSelect_)->rolename();

			const char *strings = StringDataManager::getString("friend_addblackSure");
			char *str=const_cast<char*>(strings);
			GameView::getInstance()->showPopupWindow(str,2,this,callfuncO_selector(FriendInfoList::backListFriendSure),NULL);
		}break;
	}
	this->removeFromParentAndCleanup(false);
}

void FriendInfoList::callBackCheckList( Ref * obj )
{
	auto tabFriendList=(UITab *)obj;
	int num= tabFriendList->getCurrentIndex();
	auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	Size winSize =Director::getInstance()->getVisibleSize();
	switch(num)
	{
	case 0:
		{
			long long id_ = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			std::string name_ = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();
			int countryId = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->country();
			int vipLv = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->viplevel();
			int lv = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->level();
			int pressionId = GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->profession();

			auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(id_,name_,countryId,vipLv,lv,pressionId);
			/*
			Size winSize=Director::getInstance()->getVisibleSize();
			ChatUI * chatui_ = ChatUI::create();
			chatui_->setIgnoreAnchorPointForPosition(false);
			chatui_->setAnchorPoint(Vec2(0.5f,0.5f));
			chatui_->setPosition(Vec2(winSize.width/2,winSize.height/2));
			chatui_->setTag(kTabChat);
			GameView::getInstance()->getMainUIScene()->addChild(chatui_);

			chatui_->selectChannelId =0;
			const char *str_private = StringDataManager::getString("chatui_private");
			char *private_left=const_cast<char*>(str_private);
			chatui_->labelchanel->setText(private_left);
			chatui_->channelLabel->setDefaultPanelByIndex(6);
			chatui_->addPrivate();

			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();

			std::string friendName_=friend_ui->playerName_;
			AddPrivateUi * privateui=(AddPrivateUi *)chatui_->layer_3->getChildByTag(PRIVATEUI);
			privateui->textBox_private->onTextFieldInsertText(NULL,friendName_.c_str(),30);
			*/
		}break;
	case 1:
		{
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			long long selectId=friend_ui->playerId_;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)selectId);
		}break;
	case 2:
		{
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			long long selectId=friend_ui->playerId_;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1401,(void *)selectId);

		}break;
	case 3:
		{
			friend_ui->operation_=0;
			friend_ui->relationtype_=0;
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();
			FriendStruct friend1={0,0,friend_ui->playerId_,friend_ui->playerName_};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);	
		}break;
	case 4:
		{
			auto mail_ui=MailUI::create();
			mail_ui->setIgnoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(Vec2(0.5f,0.5f));
			mail_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001, (void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();

			mail_ui->receiverId =friend_ui->playerId_;
			mail_ui->receiverName = friend_ui->playerName_;

			std::string friendName_=friend_ui->playerName_;
			mail_ui->friendName->onTextFieldInsertText(NULL,friendName_.c_str(),30);
		}break;
	case 5:
		{
			friend_ui->playerId_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playerid();
			friend_ui->playerName_=GameView::getInstance()->relationSourceVector.at(friend_ui->cellIdSelect_)->playername();

			const char *strings = StringDataManager::getString("friend_addblackSure");
			GameView::getInstance()->showPopupWindow(strings,2,this,callfuncO_selector(FriendInfoList::backListFriendSure),NULL);

		}break;
	}
	this->removeFromParentAndCleanup(false);
}

void FriendInfoList::callBackPhypower( Ref * obj )
{
	auto tabFriendList=(UITab *)obj;
	int num= tabFriendList->getCurrentIndex();
	auto phypower_ui=(FriendGetPhyPower *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGetphyPowerUi);
	Size winSize =Director::getInstance()->getVisibleSize();
	switch(num)
	{
	case 0:
		{
			long long id_ = GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->id();
			std::string name_ = GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->name();
			int countryId = GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->country();
			int vipLv = GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->viplevel();
			int lv = GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->level();
			int pressionId = GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->profession();

			auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(id_,name_,countryId,vipLv,lv,pressionId);
		}break;
	case 1:
		{
			long long selectId = GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->id();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)selectId);
		}break;
	case 2:
		{
			long long selectId = GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->id();
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1401,(void *)selectId);
			
		}break;
	case 3:
		{
			long long selectId = GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->id();
			std::string selectname = GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->name();
			FriendStruct friend1={0,0,selectId,selectname};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);	
		}break;
	case 4:
		{
			auto mail_ui=MailUI::create();
			mail_ui->setIgnoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(Vec2(0.5f,0.5f));
			mail_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001, (void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			std::string friendName_= GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->name();
			mail_ui->friendName->onTextFieldInsertText(NULL,friendName_.c_str(),30);
		}break;
	case 5:
		{
			phypower_ui->phypower_playerId = GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->id();
			phypower_ui->phypower_playerName = GameView::getInstance()->playerPhypowervector.at(phypower_ui->curVectorIndex)->name();

			const char *strings = StringDataManager::getString("friend_addblackSure");
			GameView::getInstance()->showPopupWindow(strings,2,this,callfuncO_selector(FriendInfoList::backListPhypowerSure),NULL);

		}break;
	}
	this->removeFromParentAndCleanup(false);
}

void FriendInfoList::deldtePhypowerSureDelete( Ref * obj )
{

}

void FriendInfoList::backListPhypowerSure( Ref * obj )
{
	auto phypower_ui=(FriendGetPhyPower *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGetphyPowerUi);
	FriendStruct friend1={0,1,phypower_ui->phypower_playerId,phypower_ui->phypower_playerName};
	GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);	
}







