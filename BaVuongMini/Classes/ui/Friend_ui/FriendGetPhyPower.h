#ifndef FRIEND_GETPHYPOWER_H
#define FRIEND_GETPHYPOWER_H

#include "../extensions/UIScene.h"
class CPlayerGetPhypower;

enum
{
	phypower_ktypeTouchCellEvent = 0,
	phypower_ktypeTouchCellOfMenuEvent,
};

class FriendGetPhyPower:public UIScene,public TableViewDataSource,public TableViewDelegate
{
public:
	FriendGetPhyPower(void);
	~FriendGetPhyPower(void);

	enum
	{
		phypowerCellLeft = 10,
		phypowerCellRight = 20,
	};

	static FriendGetPhyPower * create(int count);
	bool init(int count);
	void onEnter();
	void onExit();


	void callBackClose(Ref *pSender, Widget::TouchEventType type);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	// default implements are used to call script callback if exist
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);

	void setPhypowerTouchCellType(int type_);
	int getPhypowerTouchCellType();

	void setShowPhypowerlabel(bool isShow);
public:
	Size winsize;
	TableView * phypowerTavleView;
	Text * remPhyPowerCount_;

	int lastSelectCellId;
	int lastSelectCellTag;
	int curSelectCellTag;
	int m_curTouchType;
	Layer *phypower_layer;
	int m_getPhypowerCount;
	Text * Label_phypowerNull;
public:
	int curVectorIndex;
	long long phypower_playerId;
	std::string phypower_playerName;

};
#endif
