#include "AroundPlayerTabview.h"
#include "GameView.h"
#include "../../messageclient/element/CAroundPlayer.h"
#include "../extensions/CCMoveableMenu.h"
#include "../Friend_ui/AroundPlayerTabviewCell.h"
#include "../extensions/UITab.h"
#include "FriendUi.h"
#include "FriendInfoList.h"
#include "../../gamescene_state/MainScene.h"
#include "FriendInfo.h"

AroundPlayerTabview::AroundPlayerTabview(void)
{
	lastSelectCellId =0;
	lastSelectCellTag =PLAYEROTHERFRIENDCELLL;
}

AroundPlayerTabview::~AroundPlayerTabview(void)
{
}

AroundPlayerTabview * AroundPlayerTabview::create()
{
	auto aroundPlayer= new AroundPlayerTabview();
	aroundPlayer->autorelease();
	return aroundPlayer;
}

bool AroundPlayerTabview::init()
{
	if (UIScene::init())
	{
		auto tableView = TableView::create(this, Size(670, 350));
		tableView->setDirection(TableView::Direction::VERTICAL);
		tableView->setAnchorPoint(Vec2(0,0));
		tableView->setPosition(Vec2(0,0));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(tableView,1,AROUNDPLAYERTABVIEW);
		tableView->reloadData();

		this->setContentSize(Size(670,350));

		return true;
	}
	return false;
}

void AroundPlayerTabview::onEnter()
{
	UIScene::onEnter();
}

void AroundPlayerTabview::onExit()
{
	UIScene::onExit();
}

void AroundPlayerTabview::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{

}

void AroundPlayerTabview::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

void AroundPlayerTabview::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	int cellId= cell->getIdx();

	auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);

	if (table->cellAtIndex(lastSelectCellId) != NULL)
	{
		auto friendCell = (FriendInfo *)table->cellAtIndex(lastSelectCellId)->getChildByTag(lastSelectCellTag);
		if (friendCell != NULL)
		{
			if (friendCell->getChildByTag(1212) != NULL)
			{
				friendCell->getChildByTag(1212)->removeFromParent();
			}
		}
	}
	
	auto friendCurCell = (FriendInfo *)table->cellAtIndex(cellId)->getChildByTag(friend_ui->cellSelectTag);
	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(15, 30, 1, 1) , "res_ui/highlight.png");
	pHighlightSpr->setPreferredSize(Size(322,80));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setTag(1212);
	pHighlightSpr->setPosition(Vec2(18,-6));
	friendCurCell->addChild(pHighlightSpr);
	
	int pointX = friendCurCell->getPosition().x;
	int pointY = cell->getPosition().y;
	int offX = table->getContentOffset().x;
	int offY = table->getContentOffset().y;

	auto infolist =FriendInfoList::create(friend_ui->curType);
	infolist->setIgnoreAnchorPointForPosition(false);
	infolist->setAnchorPoint(Vec2(0,1));
	infolist->setLocalZOrder(10);
	/*
	if (offY + pointY +infolist->getContentSize().height >= 480)
	{
		infolist->setPosition(Vec2(pointX + offX + 100,offY + pointY - infolist->getContentSize().height/2));
	}else
	{
		infolist->setPosition(Vec2(pointX + offX + 100,offY + pointY));
	}
	*/
	infolist->setPosition(Vec2(pointX + offX + 100 ,friend_ui->layer_->getContentSize().height/2+infolist->getContentSize().height/2+50));
	friend_ui->layer_->addChild(infolist);

 	lastSelectCellId =cell->getIdx();
	lastSelectCellTag = friend_ui->cellSelectTag;
}

cocos2d::Size AroundPlayerTabview::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(700,80);
}

cocos2d::extension::TableViewCell* AroundPlayerTabview::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell =table->dequeueCell();   // this method must be called

	cell=new TableViewCell();
	cell->autorelease();
	int aroundPlayerNum= GameView::getInstance()->aroundPlayerVector.size();

	if (aroundPlayerNum/2==idx)
	{
		if (aroundPlayerNum%2 == 1)
		{
			auto aroundplayerCell= AroundPlayerTabviewCell::create(2*idx);
			aroundplayerCell->setAnchorPoint(Vec2(0,0));
			aroundplayerCell->setPosition(Vec2(0,0));
			aroundplayerCell->setTag(PLAYEROTHERFRIENDCELLL);
			cell->addChild(aroundplayerCell);

		}else
		{
			auto aroundplayerCellL= AroundPlayerTabviewCell::create(2*idx);
			aroundplayerCellL->setAnchorPoint(Vec2(0,0));
			aroundplayerCellL->setPosition(Vec2(0,0));
			aroundplayerCellL->setTag(PLAYEROTHERFRIENDCELLL);
			cell->addChild(aroundplayerCellL);

			auto aroundplayerCellR= AroundPlayerTabviewCell::create(2*idx+1);
			aroundplayerCellR->setAnchorPoint(Vec2(0,0));
			aroundplayerCellR->setPosition(Vec2(325,0));
			aroundplayerCellR->setTag(PLAYEROTHERFRIENDCELLR);
			cell->addChild(aroundplayerCellR);

		}
	}else
	{
		auto aroundplayerCellL= AroundPlayerTabviewCell::create(2*idx);
		aroundplayerCellL->setAnchorPoint(Vec2(0,0));
		aroundplayerCellL->setPosition(Vec2(0,0));
		aroundplayerCellL->setTag(PLAYEROTHERFRIENDCELLL);
		cell->addChild(aroundplayerCellL);

		auto aroundplayerCellR= AroundPlayerTabviewCell::create(2*idx+1);
		aroundplayerCellR->setAnchorPoint(Vec2(0,0));
		aroundplayerCellR->setPosition(Vec2(325,0));
		aroundplayerCellR->setTag(PLAYEROTHERFRIENDCELLR);
		cell->addChild(aroundplayerCellR);

	}

	return cell;
}

ssize_t AroundPlayerTabview::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	int vectorSize = GameView::getInstance()->aroundPlayerVector.size();
	int numEnd = vectorSize%2;
	int numCellNum = vectorSize/2+ numEnd;
	return numCellNum;
}

bool AroundPlayerTabview::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return resignFirstResponder(pTouch,this,false); 
}

// void AroundPlayerTabview::callBack( Ref * obj )
// {
// 	FriendUi * friendui =(FriendUi *)this->getParent();
// 	int currSelectType=friendui->curType;
// 	FriendInfoList * infolist =FriendInfoList::create(currSelectType);
// 	infolist->setIgnoreAnchorPointForPosition(false);
// 	infolist->setAnchorPoint(Vec2(0,0));
// 	infolist->setPosition(Vec2(200,100));
// 	Director::getInstance()->getRunningScene()->addChild(infolist);
// }

