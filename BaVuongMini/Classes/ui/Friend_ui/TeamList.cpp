#include "TeamList.h"
#include "GameView.h"
#include "../../messageclient/element/CMapTeam.h"
#include "../extensions/CCMoveableMenu.h"
#include "FriendUi.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/GameActor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"

#define  SelectImageTag 458

TeamList::TeamList(void)
{
	selectId=0;
}


TeamList::~TeamList(void)
{
}

TeamList * TeamList::create()
{
	auto team=new TeamList();
	team->autorelease();
	return team;
}

bool TeamList::init()
{
	if (UIScene::init())
	{

		auto tableView = TableView::create(this, Size(325,275));
		tableView->setDirection(TableView::Direction::VERTICAL);
		//tableView->setSelectedEnable(true);
		//tableView->setSelectedScale9Texture("res_ui/highlight.png", Rect(25, 24, 1, 1), Vec2(0,0));
		tableView->setAnchorPoint(Vec2(0,0));
		tableView->setPosition(Vec2(0,0));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(tableView,1,TEAMLISTTABVIEW);
		tableView->reloadData();
		
		return true;
	}
	return false;
}

void TeamList::onEnter()
{
	UIScene::onEnter();
}

void TeamList::onExit()
{
	UIScene::onExit();
}

void TeamList::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{

}

void TeamList::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

void TeamList::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	spriteBg->setOpacity(100);
}
void TeamList::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	auto spriteBg = (Sprite*)cell->getChildByTag(SelectImageTag);
	(cell->getIdx() % 2 == 0) ? spriteBg->setOpacity(0) : spriteBg->setOpacity(80);
}
void TeamList::tableCellWillRecycle(TableView* table, TableViewCell* cell)
{

}

void TeamList::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	selectId =cell->getIdx();
	auto friendui=(FriendUi *)this->getParent()->getParent();
	friendui->showTeamPlayer(selectId);
}

cocos2d::Size TeamList::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(323,70);
}

cocos2d::extension::TableViewCell* TeamList::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell =table->dequeueCell();   // this method must be called
	cell=new TableViewCell();
	cell->autorelease();

	auto spriteBg = cocos2d::extension::Scale9Sprite::create(Rect(26, 26, 1, 1), "res_ui/highlight.png");
	spriteBg->setPreferredSize(Size(table->getContentSize().width, cell->getContentSize().height));
	spriteBg->setAnchorPoint(Vec2::ZERO);
	spriteBg->setPosition(Vec2(0, 0));
	//spriteBg->setColor(Color3B(0, 0, 0));
	spriteBg->setTag(SelectImageTag);
	spriteBg->setOpacity(0);
	cell->addChild(spriteBg);

	auto friendui=(FriendUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	
	auto spbg= cocos2d::extension::Scale9Sprite::create("res_ui/kuang01_new.png");
	spbg->setAnchorPoint(Vec2(0,0));
	spbg->setPosition(Vec2(0,0));
	spbg->setPreferredSize(Size(321,68));
	spbg->setCapInsets(Rect(15,30,1,1));
	cell->addChild(spbg);

	int teamid_=GameView::getInstance()->mapteamVector.at(idx)->teamid();
	long long playerid_= GameView::getInstance()->mapteamVector.at(idx)->leaderid();
	long long playerSelfId =  GameView::getInstance()->myplayer->getRoleId();
	//self have team
	if (teamid_ == friendui->playerTeamId)
	{
		/////////self is leader
		if (playerid_ == playerSelfId)
		{
			const char *strings_dismiss = StringDataManager::getString("team_dismiss");
			char *dimissTeam=const_cast<char*>(strings_dismiss);	

			const char *strings_exitTeam = StringDataManager::getString("team_exit");
			char *exitTeam=const_cast<char*>(strings_exitTeam);

			auto bg_dismiss = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_2.png");
			bg_dismiss->setPreferredSize(Size(80,36));
			bg_dismiss->setCapInsets(Rect(18,9,2,23));

			auto btn_dismiss = MenuItemSprite::create(bg_dismiss, bg_dismiss, bg_dismiss, CC_CALLBACK_1(TeamList::callBackDismiss,this));
			btn_dismiss->setScale(0.5f);
			auto menu_dismiss=CCMoveableMenu::create(btn_dismiss,NULL);
			menu_dismiss->setAnchorPoint(Vec2(0,0));
			menu_dismiss->setPosition(Vec2(195,38));
			cell->addChild(menu_dismiss);
			auto label_dismiss= Label::createWithTTF(dimissTeam,APP_FONT_NAME,18);
			label_dismiss->setAnchorPoint(Vec2(0,0));
			label_dismiss->setPosition(Vec2(175,28));
			cell->addChild(label_dismiss);

			auto bg_exit = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_1.png");
			bg_exit->setPreferredSize(Size(80,36));
			bg_exit->setCapInsets(Rect(18,9,2,23));

			auto btn_exit = MenuItemSprite::create(bg_exit, bg_exit, bg_exit, CC_CALLBACK_1(TeamList::callBackExit,this));
			btn_exit->setScale(0.5f);
			auto menu_exit=CCMoveableMenu::create(btn_exit,NULL);
			menu_exit->setAnchorPoint(Vec2(0,0));
			menu_exit->setPosition(Vec2(275,38));
			cell->addChild(menu_exit);
			auto label_exit= Label::createWithTTF(exitTeam,APP_FONT_NAME,18);
			label_exit->setAnchorPoint(Vec2(0,0));
			label_exit->setPosition(Vec2(255,28));
			cell->addChild(label_exit);
		}else
		{
			const char *strings_appaly = StringDataManager::getString("team_exit");
			char *applyTeam=const_cast<char*>(strings_appaly);

			auto bg_exit = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_1.png");
			bg_exit->setPreferredSize(Size(80,36));
			bg_exit->setCapInsets(Rect(18,9,2,23));
			auto btn_exit = MenuItemSprite::create(bg_exit, bg_exit, bg_exit, CC_CALLBACK_1(TeamList::callBackExit,this));
			btn_exit->setScale(0.5f);
			auto menu_exit=CCMoveableMenu::create(btn_exit,NULL);
			menu_exit->setAnchorPoint(Vec2(0,0));
			menu_exit->setPosition(Vec2(275,38));
			cell->addChild(menu_exit);

			auto label_exit= Label::createWithTTF(applyTeam, APP_FONT_NAME,18);
			label_exit->setAnchorPoint(Vec2(0,0));
			label_exit->setPosition(Vec2(255,28));
			cell->addChild(label_exit);
		}

	}else
	{
		const char *strings_appaly = StringDataManager::getString("team_apply");
		char *applyTeam=const_cast<char*>(strings_appaly);

		auto bg_apply = cocos2d::extension::Scale9Sprite::create("res_ui/new_button_2.png");
		bg_apply->setPreferredSize(Size(80,36));
		bg_apply->setCapInsets(Rect(18,9,2,23));
		auto btn_apply = MenuItemSprite::create(bg_apply, bg_apply, bg_apply, CC_CALLBACK_1(TeamList::callBackApply,this));
		btn_apply->setScale(0.5f);
		btn_apply->setTag(playerid_);
		auto menu_apply=CCMoveableMenu::create(btn_apply,NULL);
		menu_apply->setAnchorPoint(Vec2(0,0));
		menu_apply->setPosition(Vec2(275,38));
		cell->addChild(menu_apply);

		auto label_exit= Label::createWithTTF(applyTeam, APP_FONT_NAME,18);
		label_exit->setAnchorPoint(Vec2(0,0));
		label_exit->setPosition(Vec2(255,28));
		cell->addChild(label_exit);
	}

	auto name_spbg = cocos2d::extension::Scale9Sprite::create("res_ui/LV4_diaa.png");
	name_spbg->setPreferredSize(Size(140,27));
	name_spbg->setAnchorPoint(Vec2(0.5f,0.5f));
	name_spbg->setPosition(Vec2(name_spbg->getContentSize().width/2+10,38));
	cell->addChild(name_spbg);

	std::string teamName_=GameView::getInstance()->mapteamVector.at(idx)->teamname();
	auto label_teamName =Label::createWithTTF(teamName_.c_str(), APP_FONT_NAME, 18);
	label_teamName->setAnchorPoint(Vec2(0.5f,0.5f));
	label_teamName->setPosition(Vec2(name_spbg->getContentSize().width/2,name_spbg->getContentSize().height/2));
	name_spbg->addChild(label_teamName,10);
	
	return cell;
}

ssize_t TeamList::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	return GameView::getInstance()->mapteamVector.size();
}

bool TeamList::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return resignFirstResponder(pTouch,this,false);
}

void TeamList::callBackApply( Ref * obj )
{
	auto menu_= (MenuItemSprite *)obj;
	long long selfId=menu_->getTag(); 
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1401,(void *)selfId);
}

void TeamList::callBackExit( Ref * obj )
{
	int teamWork=3;
	int playerId=0;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)teamWork,(void *)playerId);
}

void TeamList::callBackDismiss( Ref * obj )
{
	auto strings= __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
	const char *str = ((__String*)strings->objectForKey("friend_dismissSure"))->_string.c_str();
	char * applyTeam = const_cast<char*>(str);
	GameView::getInstance()->showPopupWindow(applyTeam,2,this,SEL_CallFuncO(&TeamList::callBackDismissSure),NULL);
}

void TeamList::callBackDismissSure( Ref * obj )
{
	int teamWork=2;
	int playerId=0;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)teamWork,(void *)playerId);
}
