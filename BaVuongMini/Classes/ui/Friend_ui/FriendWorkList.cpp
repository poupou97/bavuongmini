#include "FriendWorkList.h"
#include "../extensions/CCMoveableMenu.h"
#include "GameView.h"
#include "../Chat_ui/ChatUI.h"
#include "../Mail_ui/MailUI.h"
#include "FriendInfo.h"
#include "FriendUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/generals_ui/GeneralsListBase.h"
#include "FriendInfoList.h"


FriendWorkList::FriendWorkList(void)
{
	winSize=Director::getInstance()->getVisibleSize();
	friendNum=0;
	lastSelectCellId =0;
	lastSelectCellTag = FRIENDINFOCELLL;
}


FriendWorkList::~FriendWorkList(void)
{
}

FriendWorkList * FriendWorkList::create()
{
	auto friendworklist=new FriendWorkList();
	friendworklist->autorelease();
	return friendworklist;
}

bool FriendWorkList::init()
{
	if (UIScene::init())
	{
		tableView = TableView::create(this, Size(671, 305));
		tableView->setDirection(TableView::Direction::VERTICAL);
		tableView->setAnchorPoint(Vec2(0,0));
		tableView->setPosition(Vec2(35,66));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(tableView,1,FRIENDWORKLISTTAB);
		tableView->reloadData();
		auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
		friend_ui->setTouchCellType(ktypeTouchCellEvent);

		this->setContentSize(Size(715,340));
		return true;
	}
	return false;
}

void FriendWorkList::onEnter()
{
	UIScene::onEnter();
}

void FriendWorkList::onExit()
{
	UIScene::onExit();
}

void FriendWorkList::scrollViewDidScroll( cocos2d::extension::ScrollView * view )
{

}

void FriendWorkList::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}

void FriendWorkList::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	int cellId = cell->getIdx();
	auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);

	if (table->cellAtIndex(lastSelectCellId) != NULL)
	{
		auto friendCell = (FriendInfo *)table->cellAtIndex(lastSelectCellId)->getChildByTag(lastSelectCellTag);
		if (friendCell != NULL)
		{
			if (friendCell->getChildByTag(1212) != NULL)
			{
				friendCell->getChildByTag(1212)->removeFromParent();
			}
		}
	}
	
	auto friendCurCell = (FriendInfo *)table->cellAtIndex(cellId)->getChildByTag(friend_ui->cellSelectTag);
	if (friendCurCell == NULL)
	{
		return;
	}
	auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(15, 30, 1, 1) , "res_ui/highlight.png");
	pHighlightSpr->setPreferredSize(Size(322,80));
	pHighlightSpr->setAnchorPoint(Vec2::ZERO);
	pHighlightSpr->setTag(1212);
	pHighlightSpr->setPosition(Vec2(18,1));
	friendCurCell->addChild(pHighlightSpr,20);
	
	// type =ktypeTouchCellEvent 
	// type =ktypeTouchCellOfMenuEvent
	if (friend_ui->getTouchCellType() == ktypeTouchCellEvent)
	{
		int pointX = friendCurCell->getPosition().x;
		int pointY = cell->getPosition().y;
		int offX = table->getContentOffset().x;
		int offY = table->getContentOffset().y;

		auto infolist =FriendInfoList::create(friend_ui->curType);
		infolist->setIgnoreAnchorPointForPosition(false);
		infolist->setAnchorPoint(Vec2(0,1));
		infolist->setLocalZOrder(10);
		infolist->setPosition(Vec2(pointX + offX + 100 ,friend_ui->layer_->getContentSize().height/2+infolist->getContentSize().height/2+80));
		friend_ui->layer_->addChild(infolist);
	}
	friend_ui->setTouchCellType(ktypeTouchCellEvent);
 	lastSelectCellId =cell->getIdx();
	lastSelectCellTag = friend_ui->cellSelectTag;
}

cocos2d::Size FriendWorkList::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	return Size(671,80);
}

cocos2d::extension::TableViewCell* FriendWorkList::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell =table->dequeueCell();   // this method must be called

	cell=new TableViewCell();
	cell->autorelease();
	
	int friendNum = GameView::getInstance()->relationSourceVector.size();
	auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);

	if (idx == friendNum/2)
	{
		if (friendNum%2 == 0)
		{
			auto friendInfoL =FriendInfo::create(2*idx,kTagFriendUi);
			friendInfoL->setAnchorPoint(Vec2(0.5f,0.5f));
			friendInfoL->setPosition(Vec2(0,0));
			friendInfoL->setTag(FRIENDINFOCELLL);
			cell->addChild(friendInfoL);

			auto friendInfoR =FriendInfo::create(2*idx+1,kTagFriendUi);
			friendInfoR->setAnchorPoint(Vec2(0.5f,0.5f));
			friendInfoR->setPosition(Vec2(325,0));
			friendInfoR->setTag(FRIENDINFOCELLR);
			cell->addChild(friendInfoR);
		}else
		{
			auto friendInfoL =FriendInfo::create(2*idx,kTagFriendUi);
			friendInfoL->setAnchorPoint(Vec2(0.5f,0.5f));
			friendInfoL->setPosition(Vec2(0,0));
			friendInfoL->setTag(FRIENDINFOCELLL);
			cell->addChild(friendInfoL);
		}
	}
	else
	{
		auto friendInfoL =FriendInfo::create(2*idx,kTagFriendUi);
		friendInfoL->setAnchorPoint(Vec2(0.5f,0.5f));
		friendInfoL->setPosition(Vec2(0,0));
		friendInfoL->setTag(FRIENDINFOCELLL);
		cell->addChild(friendInfoL);

		auto friendInfoR =FriendInfo::create(2*idx+1,kTagFriendUi);
		friendInfoR->setAnchorPoint(Vec2(0.5f,0.5f));
		friendInfoR->setPosition(Vec2(325,0));
		friendInfoR->setTag(FRIENDINFOCELLR);
		cell->addChild(friendInfoR);
	}

	if(idx == friendNum/2 - 3)
	{
		auto friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
		if (friend_ui->pageIndexSelect < totalPageNum-1 )
		{
			friend_ui->pageIndexSelect++;
			switch(friend_ui->curType)
			{
			case CHECKTABLIST_ONEKEY:
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(2210,friend_ui);
				}break;
			case CHECKTABLIST_CHECKBOX:
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(2209,friend_ui);
				}break;
			default:
				{
					FriendStruct friend1={friend_ui->curType,20,friend_ui->pageIndexSelect,0};
					GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);
				}break;
			}
		}
	}
	return cell;
}

ssize_t FriendWorkList::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	int vectorSize = GameView::getInstance()->relationSourceVector.size();
	
	if (vectorSize%2 == 0)
	{
		return vectorSize/2;
	}else
	{
		return vectorSize/2 + 1;
	}
}

bool FriendWorkList::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return resignFirstResponder(pTouch,this,false);
}

void FriendWorkList::ReloadTableViewWithoutChangeOffSet()
{
	Vec2 offset = this->tableView->getContentOffset();
	this->tableView->reloadData();
	this->tableView->setContentOffset(offset);
}

