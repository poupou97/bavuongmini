#include "FriendUi.h"
#include "FriendWorkList.h"
#include "GameView.h"
#include "../extensions/CCMoveableMenu.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CRelationPlayer.h"
#include "../extensions/Counter.h"
#include "../extensions/RichTextInput.h"
#include "../extensions/UITab.h"
#include "TeamList.h"
#include "TeamMemberList.h"
#include "../../messageclient/element/CMapTeam.h"
#include "AroundPlayerTabview.h"
#include "../../messageclient/element/CTeamMember.h"
#include "../../utils/StaticDataManager.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"

using namespace CocosDenshion;

FriendUi::FriendUi(void)
{
	curType=FRIENDLIST;

	sexSelect=2;
	professionSelect=0;
	showOnlineSelect=0;
	pageNumSelect=20;//每页显示d数量
	pageIndexSelect=0;

	selectIndex=0;
	fuctionValue=0;

	minLvSelect = 0;
	maxLvSelect = 0;

	totalPlayerNum_ = 0;
}


FriendUi::~FriendUi(void)
{
	std::vector<CRelationPlayer *>::iterator iter;
	for (iter=GameView::getInstance()->relationFriendVector.begin();iter!=GameView::getInstance()->relationFriendVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationFriendVector.clear();

	for (iter=GameView::getInstance()->relationBlackVector.begin();iter!=GameView::getInstance()->relationBlackVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationBlackVector.clear();


	for (iter=GameView::getInstance()->relationEnemyVector.begin();iter!=GameView::getInstance()->relationEnemyVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationEnemyVector.clear();


	for (iter=GameView::getInstance()->relationSourceVector.begin();iter!=GameView::getInstance()->relationSourceVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationSourceVector.clear();
}

FriendUi * FriendUi::create()
{
	auto friendui=new FriendUi();

	if (friendui && friendui->init())
	{
		friendui->autorelease();
		return friendui;
	}
	CC_SAFE_DELETE(friendui);
	return NULL;
}

bool FriendUi::init()
{
	if (UIScene::init())
	{

		winsize=Director::getInstance()->getVisibleSize();

		auto mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winsize);
		mengban->setAnchorPoint(Vec2(0,0));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);

		if(LoadSceneLayer::friendPanelBg->getParent() != NULL)
		{
			LoadSceneLayer::friendPanelBg->removeFromParentAndCleanup(false);
		}

		friendPanel=LoadSceneLayer::friendPanelBg;
		friendPanel->setAnchorPoint(Vec2(0.5f,0.5f));
		friendPanel->setPosition(Vec2(winsize.width/2,winsize.height/2));
		m_pLayer->addChild(friendPanel);

		layer_= Layer::create();
		layer_->setIgnoreAnchorPointForPosition(false);
		layer_->setAnchorPoint(Vec2(0.5f,0.5f));
		layer_->setPosition(Vec2(winsize.width/2,winsize.height/2));
		layer_->setContentSize(Size(800,480));
		this->addChild(layer_,0,FRIENDADDLAYER);

		const char * secondStr = StringDataManager::getString("UIName_friend_hao");
		const char * thirdStr = StringDataManager::getString("UIName_friend_you");
		auto atmature = MainScene::createPendantAnm("",secondStr,thirdStr,"");
		atmature->setPosition(Vec2(30,240));
		layer_->addChild(atmature);


		const char *str_friend = StringDataManager::getString("friend_friend");
		char *friend_left=const_cast<char*>(str_friend);	

		const char *str_enemy = StringDataManager::getString("friend_enemy");
		char *enemy_left=const_cast<char*>(str_enemy);

		const char *str_black = StringDataManager::getString("friend_black");
		char *black_left=const_cast<char*>(str_black);

		const char *str_team = StringDataManager::getString("chatui_team");
		char *team_left=const_cast<char*>(str_team);

		const char *str_around = StringDataManager::getString("friend_around");
		char *around_left=const_cast<char*>(str_around);

		const char * highLightImage = "res_ui/tab_4_on.png";
		char * labelChange[] ={friend_left,enemy_left,black_left,team_left,around_left};
		//friendType= UITab::createWithBMFont(5,"res_ui/tab_4_off.png","res_ui/tab_4_on.png","",labelChange,"res_ui/font/ziti_1.fnt",HORIZONTAL,5);
		friendType= UITab::createWithText(5,"res_ui/tab_4_off.png","res_ui/tab_4_on.png","",labelChange,HORIZONTAL,5);
		friendType->setAnchorPoint(Vec2(0.5f,0.5f));
		friendType->setPosition(Vec2(70,413));
		friendType->setPressedActionEnabled(true);
		friendType->setHighLightImage((char * )highLightImage);
		friendType->setDefaultPanelByIndex(0);
		friendType->setLocalZOrder(2);
		friendType->addIndexChangedEvent(this,coco_indexchangedselector(FriendUi::callBackChangeFriendType));
		layer_->addChild(friendType);

		const char * highLightwork = "res_ui/tab_b.png";
		//char * labelwork[] ={"res_ui/haoyou/haoyou2.png","res_ui/haoyou/linshi.png","res_ui/haoyou/chazhao.png"};

		const char *str1_haoyou = StringDataManager::getString("friend_uitab_haoyou");
		char* p1_haoyou =const_cast<char*>(str1_haoyou);
		const char *str2_linshi = StringDataManager::getString("friend_uitab_linshi");
		char* p2_linshi =const_cast<char*>(str2_linshi);
		const char *str2_chazhao = StringDataManager::getString("friend_uitab_chazhao");
		char* p3_chazhao =const_cast<char*>(str2_chazhao);

		char * labelImage[] ={p1_haoyou,p2_linshi,p3_chazhao};
		friendWork = UITab::createWithText(3,"res_ui/tab_b_off.png","res_ui/tab_b.png","",labelImage,VERTICAL,-5,18);
		//friendWork= UITab::createWithImage(3,"res_ui/tab_b_off.png","res_ui/tab_b.png","",labelwork,VERTICAL,-5);
		friendWork->setAnchorPoint(Vec2(0.5f,0.5f));
		friendWork->setPosition(Vec2(758,405));
		friendWork->setPressedActionEnabled(true);
		friendWork->setHighLightImage((char * )highLightwork);
		friendWork->setHightLightLabelColor(Color3B(47,93,13));
		friendWork->setNormalLabelColor(Color3B(255,255,255));
		friendWork->setDefaultPanelByIndex(0);
		friendWork->setLocalZOrder(2);
		friendWork->addIndexChangedEvent(this,coco_indexchangedselector(FriendUi::callBackFriendButton));
		layer_->addChild(friendWork);

		palPanel =(Layout *)Helper::seekWidgetByName(friendPanel,"Panel_haoyou");
		palPanel->setVisible(true);
		checkPanel=(Layout *)Helper::seekWidgetByName(friendPanel,"Panel_chazhao");
		checkPanel->setVisible(false);
		
		btn_phypower = (Button *)Helper::seekWidgetByName(friendPanel,"Button_getPhypower");
		btn_phypower->setPressedActionEnabled(true);
		btn_phypower->setTouchEnabled(true);
		btn_phypower->addTouchEventListener(CC_CALLBACK_2(FriendUi::callBackPhypower, this));

		checkResultPanel =(Layout *)Helper::seekWidgetByName(friendPanel,"Panel_chazhaojieguo");
		checkResultPanel->setVisible(false);
		labclCheckResult = (Text*)Helper::seekWidgetByName(friendPanel,"chazhaojieguoNumValue");
		labclCheckResult->setString("0");

		panelTeam=(Layout *)Helper::seekWidgetByName(friendPanel,"Panel_zudui"); 
		panelTeam->setVisible(false);

		panelOtherplayer=(Layout *)Helper::seekWidgetByName(friendPanel,"Panel_other players");
		panelOtherplayer->setVisible(false);

		isShowFriendCheckbox=(CheckBox *)Helper::seekWidgetByName(friendPanel,"CheckBox_xianshizaixian");
		isShowFriendCheckbox->setTouchEnabled(true);
		isShowFriendCheckbox->setSelected(false);
		isShowFriendCheckbox->addEventListener(CC_CALLBACK_2(FriendUi::showfriendOfLine, this));
		isShowFriendCheckbox->setVisible(true);

		labelFriendType = (Text*)Helper::seekWidgetByName(friendPanel,"Label_795");

// 		int onLineFriendNum =GameView::getInstance()->relationSourceVector.size();
// 		char friendnum_[10]; 
// 		sprintf(friendnum_,"%d",onLineFriendNum);

		label_Friendnum =(Text*)Helper::seekWidgetByName(friendPanel,"showLabelNumValue"); 
		label_Friendnum->setString("0/50");
		label_Friendnum->setVisible(true);

		////check yijianzhengyou   panel=checkpanel
		auto chackButton=(Button *)Helper::seekWidgetByName(friendPanel,"Button_chazhao");
		chackButton->setPressedActionEnabled(true);
		chackButton->setTouchEnabled(true);
		chackButton->addTouchEventListener(CC_CALLBACK_2(FriendUi::callBackChackButton, this));

		auto keyPersonal=(Button *)Helper::seekWidgetByName(friendPanel,"Button_yijianzhengyou");
		keyPersonal->setTouchEnabled(true);
		keyPersonal->setPressedActionEnabled(true);
		keyPersonal->addTouchEventListener(CC_CALLBACK_2(FriendUi::callBackKeyPersonalButton, this));

		nameBox=new RichTextInputBox();
		nameBox->setInputBoxWidth(310);
		nameBox->setAnchorPoint(Vec2(0,0));
		nameBox->setPosition(Vec2(164,340));
		nameBox->setCharLimit(10);
		layer_->addChild(nameBox);
		nameBox->setVisible(false);
		nameBox->autorelease();

		////find profession
		CheckBox_pro_HaoJie =(CheckBox *)Helper::seekWidgetByName(friendPanel,"CheckBox_zhiye_mo_haojie");
		CheckBox_pro_HaoJie->setTouchEnabled(true);
		CheckBox_pro_HaoJie->setSelected(false);

		CheckBox_pro_MengJiang =(CheckBox *)Helper::seekWidgetByName(friendPanel,"CheckBox_zhiye_mo_mengjiang");
		CheckBox_pro_MengJiang->setTouchEnabled(true);
		CheckBox_pro_MengJiang->setSelected(false);

		CheckBox_pro_GuiMou =(CheckBox *)Helper::seekWidgetByName(friendPanel,"CheckBox_zhiye_mo_guimou");
		CheckBox_pro_GuiMou->setTouchEnabled(true);
		CheckBox_pro_GuiMou->setSelected(false);

		CheckBox_pro_ShenShe =(CheckBox *)Helper::seekWidgetByName(friendPanel,"CheckBox_zhiye_mo_shenshe");
		CheckBox_pro_ShenShe->setTouchEnabled(true);
		CheckBox_pro_ShenShe->setSelected(false);
		/*

		UICheckBox * checkBoxbuyHp =(UICheckBox *)Helper::seekWidgetByName(protectionPanel,"CheckBox_honggou_2");
		checkBoxbuyHp->setTouchEnabled(true);
		checkBoxbuyHp->addEventListenerCheckBox(this,checkboxselectedeventselector(RobotMainUI::setCheckBoxBuyHp));
		*/
		checkBox_country_yi = (CheckBox *)Helper::seekWidgetByName(friendPanel,"CheckBox_country_yi");
		checkBox_country_yi->setTouchEnabled(true);
		checkBox_country_yi->setSelected(false);
		//checkBox_country_yi->addEventListenerCheckBox(this,checkboxselectedeventselector());

		checkBox_country_yang = (CheckBox *)Helper::seekWidgetByName(friendPanel,"CheckBox_country_yang");
		checkBox_country_yang->setTouchEnabled(true);
		checkBox_country_yang->setSelected(false);
		//checkBox_country_yang->addEventListenerCheckBox(this,checkboxselectedeventselector());

		checkBox_country_jing = (CheckBox *)Helper::seekWidgetByName(friendPanel,"CheckBox_country_jing");
		checkBox_country_jing->setTouchEnabled(true);
		checkBox_country_jing->setSelected(false);
		//checkBox_country_jing->addEventListenerCheckBox();
		/*
		checkBox_country_you = (UICheckBox *)Helper::seekWidgetByName(friendPanel,"CheckBox_country_you");
		checkBox_country_you->setTouchEnabled(true);
		checkBox_country_you->setSelected(false);

		checkBox_country_liang = (UICheckBox *)Helper::seekWidgetByName(friendPanel,"CheckBox_country_liang");
		checkBox_country_liang->setTouchEnabled(true);
		checkBox_country_liang->setSelected(false);
		*/

		//Button_dengjikuang1  Button_dengjikuang1
		auto Button_LV1=(Button *)Helper::seekWidgetByName(friendPanel,"Button_dengjikuang1");
		Button_LV1->setTouchEnabled(true);
		Button_LV1->addTouchEventListener(CC_CALLBACK_2(FriendUi::callBackPopCounterLV1, this));

		auto Button_LV2=(Button *)Helper::seekWidgetByName(friendPanel,"Button_dengjikuang2");
		Button_LV2->setTouchEnabled(true);
		Button_LV2->addTouchEventListener(CC_CALLBACK_2(FriendUi::callBackPopCounterLV2, this));

		label_lv1= (Text*)Helper::seekWidgetByName(friendPanel,"Label_LV1");
		label_lv1->setString("");
		label_lv2= (Text*)Helper::seekWidgetByName(friendPanel,"Label_LV2");
		label_lv2->setString("");

		button_backCheck =(Button *)Helper::seekWidgetByName(friendPanel,"Button_fanhui");
		button_backCheck->setPressedActionEnabled(true);
		button_backCheck->setTouchEnabled(true);
		button_backCheck->addTouchEventListener(CC_CALLBACK_2(FriendUi::callBackButton_backCheck, this));
		
		button_addFriend =(Button *)Helper::seekWidgetByName(friendPanel,"Button_yijiantianjia");
		button_addFriend->setPressedActionEnabled(true);
		button_addFriend->setTouchEnabled(true);
		button_addFriend->addTouchEventListener(CC_CALLBACK_2(FriendUi::callbackButton_addFriend, this));

		CheckBox_accept =(CheckBox *)Helper::seekWidgetByName(friendPanel,"CheckBox_accept");
		CheckBox_accept->setTouchEnabled(true);
		if (MyPlayerAIConfig::getAcceptTeam() == true)
		{
			CheckBox_accept->setSelected(true);
		}else
		{
			CheckBox_accept->setSelected(false);
		}
		CheckBox_accept->addEventListener(CC_CALLBACK_2(FriendUi::selectAcceptTeam,this));


		CheckBox_refuse=(CheckBox *)Helper::seekWidgetByName(friendPanel,"CheckBox_refuse");
		CheckBox_refuse->setTouchEnabled(true);
		if (MyPlayerAIConfig::getRefuseTeam() == true)
		{
			CheckBox_refuse->setSelected(true);
		}else
		{
			CheckBox_refuse->setSelected(false);
		}
		
		CheckBox_refuse->addEventListener(CC_CALLBACK_2(FriendUi::selectRefuseTeam,this));

		auto button_callTogether =(Button *)Helper::seekWidgetByName(friendPanel,"Button_538");
		button_callTogether->setPressedActionEnabled(true);
		button_callTogether->setTouchEnabled(true);
		button_callTogether->addTouchEventListener(CC_CALLBACK_2(FriendUi::callToTogether, this));

		auto button_autoTeam =(Button *)Helper::seekWidgetByName(friendPanel,"Button_538_0");
		button_autoTeam->setPressedActionEnabled(true);
		button_autoTeam->setTouchEnabled(true);
		button_autoTeam->addTouchEventListener(CC_CALLBACK_2(FriendUi::autoApplyTeam, this));
		//button exit
		auto button_exit =(Button *)Helper::seekWidgetByName(friendPanel,"Button_close");
		button_exit->setPressedActionEnabled(true);
		button_exit->setTouchEnabled(true);
		button_exit->addTouchEventListener(CC_CALLBACK_2(FriendUi::callBackExit, this));


		//this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winsize);
		return true;
	}
	return false;
}

void FriendUi::onEnter()
{
	UIScene::onEnter();
	this->openAnim();

	GameUtils::playGameSound(SCENE_OPEN, 2, false);

	friendWorkList =FriendWorkList::create();
	friendWorkList->setIgnoreAnchorPointForPosition(false);
	friendWorkList->setAnchorPoint(Vec2(0,0));
	friendWorkList->setPosition(Vec2(25,25));
	layer_->addChild(friendWorkList,2,FRIENDWORKLISTTAG);
	friendWorkList->init();

	teamList=TeamList::create();
	teamList->setIgnoreAnchorPointForPosition(false);
	teamList->setAnchorPoint(Vec2(0,0));
	teamList->setPosition(Vec2(74,91));
	layer_->addChild(teamList,2,TEAMLISTTAG);
	teamList->init();
	teamList->setVisible(false);
	//////
	aroundplayer= AroundPlayerTabview::create();
	aroundplayer->setIgnoreAnchorPointForPosition(false);
	aroundplayer->setAnchorPoint(Vec2(0,0));
	aroundplayer->setPosition(Vec2(60,51));
	layer_->addChild(aroundplayer);
	aroundplayer->init();
	aroundplayer->setVisible(false);
}

void FriendUi::onExit()
{
	UIScene::onExit();
	SimpleAudioEngine::getInstance()->playEffect(SCENE_CLOSE, false);
}

void FriendUi::callBackChangeFriendType( Ref * obj )
{
	auto m_tab=(UITab *)obj;
	int num= m_tab->getCurrentIndex();

	pageIndexSelect=0;
	switch(num)
	{
	case 0:
		{
			curType=FRIENDLIST;
			FriendList friend1={curType,pageNumSelect,pageIndexSelect,showOnlineSelect};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);

			friendWorkList->setVisible(true);
			teamList->setVisible(false);
			aroundplayer->setVisible(false);

			friendWork->setVisible(true);
			nameBox->setVisible(false);
			
			palPanel->setVisible(true);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(false);

			refreshCheckCountryAndPression(false);
// 			CheckBox_pro_HaoJie->setSelected(false);
// 			CheckBox_pro_GuiMou->setSelected(false);
// 			CheckBox_pro_MengJiang->setSelected(false);
// 			CheckBox_pro_ShenShe->setSelected(false);

			auto teamlayer = (Layer *)layer_->getChildByTag(LayerOFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}

		}break;
	case 1:
		{
			curType=ENEMYLIST;

			FriendList friend1={curType,pageNumSelect,pageIndexSelect,showOnlineSelect};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);

			friendWorkList->setVisible(true);
			teamList->setVisible(false);
			aroundplayer->setVisible(false);

			friendWork->setVisible(false);
			nameBox->setVisible(false);
			
			palPanel->setVisible(true);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(false);

			refreshCheckCountryAndPression(false);
// 			CheckBox_pro_HaoJie->setSelected(false);
// 			CheckBox_pro_GuiMou->setSelected(false);
// 			CheckBox_pro_MengJiang->setSelected(false);
// 			CheckBox_pro_ShenShe->setSelected(false);
			
			auto teamlayer = (Layer *)layer_->getChildByTag(LayerOFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}
		}break;
	case 2:
		{
			curType=BACKLIST;
			FriendList friend1={curType,pageNumSelect,pageIndexSelect,showOnlineSelect};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);

			friendWorkList->setVisible(true);
			teamList->setVisible(false);
			aroundplayer->setVisible(false);

			friendWork->setVisible(false);
			nameBox->setVisible(false);
			
			palPanel->setVisible(true);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(false);

			refreshCheckCountryAndPression(false);
// 			CheckBox_pro_HaoJie->setSelected(false);
// 			CheckBox_pro_GuiMou->setSelected(false);
// 			CheckBox_pro_MengJiang->setSelected(false);
// 			CheckBox_pro_ShenShe->setSelected(false);

			auto teamlayer = (Layer *)layer_->getChildByTag(LayerOFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}

		}break;
	case 3:
		{
			curType=TEAMPLAYER;
			//team
			void *teamId =(void *)0;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1408,teamId);
			
			friendWorkList->setVisible(false);
			teamList->setVisible(true);
			aroundplayer->setVisible(false);

			friendWork->setVisible(false);
			nameBox->setVisible(false);

			palPanel->setVisible(false);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(true);
			panelOtherplayer->setVisible(false);

			refreshCheckCountryAndPression(false);
// 			CheckBox_pro_HaoJie->setSelected(false);
// 			CheckBox_pro_GuiMou->setSelected(false);
// 			CheckBox_pro_MengJiang->setSelected(false);
// 			CheckBox_pro_ShenShe->setSelected(false);
		
			if (GameView::getInstance()->mapteamVector.size()>0)
			{
				this->showTeamPlayer(0);
			}
			
		}break;
	case 4:
		{
			curType=AROUNDPLAYER;
			void *otherPlayer = (void *)1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1408,otherPlayer);
			
			friendWorkList->setVisible(false);
			teamList->setVisible(false);
			aroundplayer->setVisible(true);
			//otherplayer
			friendWork->setVisible(false);
			nameBox->setVisible(false);
			
			palPanel->setVisible(false);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(true);
			
			refreshCheckCountryAndPression(false);
// 			CheckBox_pro_HaoJie->setSelected(false);
// 			CheckBox_pro_GuiMou->setSelected(false);
// 			CheckBox_pro_MengJiang->setSelected(false);
// 			CheckBox_pro_ShenShe->setSelected(false);

			auto teamlayer = (Layer *)layer_->getChildByTag(LayerOFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}
		}break;
	}
	//this->reloadSourceData(obj);
}

void FriendUi::callBackFriendButton( Ref * obj )
{
	auto m_tab=(UITab *)obj;

	m_tab->setHightLightLabelColor(Color3B(47,93,13));
	m_tab->setNormalLabelColor(Color3B(255,255,255));


	int num= m_tab->getCurrentIndex();

	std::vector<CRelationPlayer *>::iterator iter;
	for (iter=GameView::getInstance()->relationSourceVector.begin();iter!=GameView::getInstance()->relationSourceVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationSourceVector.clear();
	pageIndexSelect=0;
	switch(num)
	{
	case 0:
		{
			curType=FRIENDLIST;
			FriendList friend1={curType,pageNumSelect,pageIndexSelect,0};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202,&friend1);

			friendWorkList->setVisible(true);
			teamList->setVisible(false);
			aroundplayer->setVisible(false);

			friendWork->setVisible(true);
			nameBox->setVisible(false);

			palPanel->setVisible(true);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(false);
			
			auto teamlayer = (Layer *)layer_->getChildByTag(LayerOFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}
		}break;
	case 1:
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2211,this);

			friendWorkList->setVisible(true);
			teamList->setVisible(false);
			aroundplayer->setVisible(false);

			friendWork->setVisible(true);
			nameBox->setVisible(false);

			palPanel->setVisible(true);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(false);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(false);
			auto teamlayer = (Layer *)layer_->getChildByTag(LayerOFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}
		}break;
	case 2:
		{
			friendWorkList->setVisible(false);
			teamList->setVisible(false);
			aroundplayer->setVisible(false);

			label_lv1->setString("");
			label_lv2->setString("");
			friendWork->setVisible(true);
			nameBox->setVisible(true);

			palPanel->setVisible(false);
			checkResultPanel->setVisible(false);
			checkPanel->setVisible(true);
			panelTeam->setVisible(false);
			panelOtherplayer->setVisible(false);

			minLvSelect = 0;
			maxLvSelect = 0;

			auto teamlayer = (Layer *)layer_->getChildByTag(LayerOFTEAMTAG);
			if (teamlayer != NULL)
			{
				teamlayer->removeFromParentAndCleanup(true);
			}
		}break;
	}
}
void FriendUi::callBackExit(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

void FriendUi::callBackChackButton(Ref *pSender, Widget::TouchEventType type)
{	

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		countryVector.clear();
		if (checkBox_country_yi->isSelected() == true)
		{
			countryVector.push_back(1);
		}
		if (checkBox_country_yang->isSelected() == true)
		{
			countryVector.push_back(2);
		}
		if (checkBox_country_jing->isSelected() == true)
		{
			countryVector.push_back(3);
		}
		/*
		if (checkBox_country_you->isSelected() == true)
		{
		countryVector.push_back(4);
		}
		if (checkBox_country_liang->isSelected() == true)
		{
		countryVector.push_back(5);
		}
		*/

		professionVector.clear();
		if (CheckBox_pro_MengJiang->isSelected() == true)//猛将、鬼谋、豪杰、神射
		{
			professionSelect = 1;
			professionVector.push_back(professionSelect);
		}

		if (CheckBox_pro_GuiMou->isSelected() == true)
		{
			professionSelect = 2;
			professionVector.push_back(professionSelect);
		}
		if (CheckBox_pro_HaoJie->isSelected() == true)
		{
			professionSelect = 3;
			professionVector.push_back(professionSelect);
		}
		if (CheckBox_pro_ShenShe->isSelected() == true)
		{
			professionSelect = 4;
			professionVector.push_back(professionSelect);
		}

		const char* name_ = nameBox->getInputString();
		// 	if(name_ == NULL && professionVector.size() <= 0 && countryVector.size() <= 0 && maxLvSelect <=0 && minLvSelect<=0 )
		// 	{		
		// 		const char *strings = StringDataManager::getString("friend_search");
		// 		GameView::getInstance()->showAlertDialog(strings);
		// 		return;
		// 	}

		if (isShowFriendCheckbox->isSelected() == true)
		{
			showOnlineSelect = 1;
		}
		else
		{
			showOnlineSelect = 0;
		}
		if (name_ == NULL)
		{
			nameString_ = "";
		}
		else
		{
			nameString_ = name_;
		}
		pageIndexSelect = 0;
		curType = CHECKTABLIST_CHECKBOX;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2209, this);
		///////////
		label_lv1->setString("");
		label_lv2->setString("");
		palPanel->setVisible(true);
		nameBox->deleteAllInputString();

		nameBox->setVisible(false);
		palPanel->setVisible(false);
		friendWorkList->setVisible(true);
		checkResultPanel->setVisible(true);
		checkPanel->setVisible(false);
		button_backCheck->setVisible(true);
		button_addFriend->setVisible(true);
		refreshCheckCountryAndPression(false);
		// 	CheckBox_pro_HaoJie->setSelected(false);
		// 	CheckBox_pro_GuiMou->setSelected(false);
		// 	CheckBox_pro_MengJiang->setSelected(false);
		// 	CheckBox_pro_ShenShe->setSelected(false);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FriendUi::callBackKeyPersonalButton(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (isShowFriendCheckbox->isSelected() == true)
		{
			showOnlineSelect = 1;
		}
		else
		{
			showOnlineSelect = 0;
		}
		curType = CHECKTABLIST_ONEKEY;
		pageIndexSelect = 0;
		//随机选出与玩家角色级别相差最少的10名其他非好友玩家角色
		GameMessageProcessor::sharedMsgProcessor()->sendReq(2210, this);

		palPanel->setVisible(false);
		friendWorkList->setVisible(true);
		checkResultPanel->setVisible(true);
		checkPanel->setVisible(false);
		button_backCheck->setVisible(true);
		button_addFriend->setVisible(true);

		nameBox->setVisible(false);

		CheckBox_pro_HaoJie->setSelected(false);
		CheckBox_pro_GuiMou->setSelected(false);
		CheckBox_pro_MengJiang->setSelected(false);
		CheckBox_pro_ShenShe->setSelected(false);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FriendUi::callBackButton_backCheck(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//查找
		palPanel->setVisible(false);
		friendWorkList->setVisible(false);
		checkPanel->setVisible(true);
		nameBox->setVisible(true);

		label_lv1->setString("");
		label_lv2->setString("");
		checkResultPanel->setVisible(false);
		checkPanel->setVisible(true);
		panelTeam->setVisible(false);
		panelOtherplayer->setVisible(false);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FriendUi::callbackButton_addFriend(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		operation_ = 0;
		relationtype_ = 0;

		for (int i = 0; i<GameView::getInstance()->relationSourceVector.size(); i++)
		{
			playerId_ = GameView::getInstance()->relationSourceVector.at(i)->playerid();
			playerName_ = GameView::getInstance()->relationSourceVector.at(i)->playername();
			FriendStruct friend1 = { 0,0,playerId_,playerName_ };
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200, &friend1);
		}

		refreshCheckCountryAndPression(false);
		//成员变化 在2201里面处理过了
		//this->reloadSourceData();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

bool FriendUi::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return true;
}

void FriendUi::callBackPopCounterLV1(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->isClosing())
			return;

		GameView::getInstance()->showCounter(this, callfuncO_selector(FriendUi::callBackLabelLV1));
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}
void FriendUi::callBackPopCounterLV2(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (this->isClosing())
			return;

		GameView::getInstance()->showCounter(this, callfuncO_selector(FriendUi::callBackLabelLV2));
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void FriendUi::callBackLabelLV1( Ref * obj )
{
	auto couter_ =(Counter *)obj;
	if (maxLvSelect != 0)
	{
		if (couter_->getInputNum() > 999999 || couter_->getInputNum() > maxLvSelect)
		{
			const char *str_friend = StringDataManager::getString("friend_putIn_maxLevel_not");
			GameView::getInstance()->showAlertDialog(str_friend);
		}else
		{
			minLvSelect = couter_->getInputNum();
			char string_LV[10];
			sprintf(string_LV,"%d",minLvSelect);
			label_lv1->setString(string_LV);
		}
	}else
	{
		if (couter_->getInputNum() > 999999)
		{
			const char *str_friend = StringDataManager::getString("friend_putIn_maxLevel_not");
			GameView::getInstance()->showAlertDialog(str_friend);
		}else
		{
			minLvSelect = couter_->getInputNum();
			char string_LV[10];
			sprintf(string_LV,"%d",minLvSelect);
			label_lv1->setString(string_LV);
		}
	}
}

void FriendUi::callBackLabelLV2( Ref * obj )
{
	auto couter_ =(Counter *)obj;

	if (couter_->getInputNum() > 999999 || couter_->getInputNum() < minLvSelect )
	{
		const char *str_friend = StringDataManager::getString("friend_putIn_maxLevel_not");
		GameView::getInstance()->showAlertDialog(str_friend);
	}else
	{
		maxLvSelect = couter_->getInputNum();
		char string_LV[10];
		sprintf(string_LV,"%d",maxLvSelect);
		label_lv2->setString(string_LV);
	}
}

void FriendUi::showfriendOfLine(Ref* pSender, CheckBox::EventType type)
{

	std::vector<CRelationPlayer *>::iterator iter;
	for (iter = GameView::getInstance()->relationSourceVector.begin(); iter != GameView::getInstance()->relationSourceVector.end(); iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->relationSourceVector.clear();

	int temp_num = 0;
	if (isShowFriendCheckbox->isSelected() == false)
	{
		showOnlineSelect = 1;

		for (int i = 0; i<GameView::getInstance()->relationFriendVector.size(); i++)
		{
			if (GameView::getInstance()->relationFriendVector.at(i)->online() == 1)
			{
				auto player = new CRelationPlayer();
				player->CopyFrom(*(GameView::getInstance()->relationFriendVector.at(i)));

				GameView::getInstance()->relationSourceVector.push_back(player);
				temp_num++;
			}
		}
	}
	else
	{
		showOnlineSelect = 0;
		for (int i = 0; i<GameView::getInstance()->relationFriendVector.size(); i++)
		{
			auto player = new CRelationPlayer();
			player->CopyFrom(*(GameView::getInstance()->relationFriendVector.at(i)));

			GameView::getInstance()->relationSourceVector.push_back(player);

			temp_num++;
		}
	}
	this->reloadSourceData(temp_num);

}

void FriendUi::showTeamPlayer(int idx)
{
	auto teamlayer = (Layer *)layer_->getChildByTag(LayerOFTEAMTAG);
	if (teamlayer != NULL)
	{
		teamlayer->removeFromParentAndCleanup(true);
	}
	if (GameView::getInstance()->mapteamVector.size()<=0)
	{
		return;
	}
	if (curType != 3)
	{
		return;
	}
	auto layerOfTeam =Layer::create();
	layerOfTeam->setIgnoreAnchorPointForPosition(false);
	layerOfTeam->setAnchorPoint(Vec2(0.5f,0.5f));
	layerOfTeam->setPosition(Vec2(winsize.width/2,winsize.height/2));
	layerOfTeam->setTag(LayerOFTEAMTAG);
	layer_->addChild(layerOfTeam);
	for (int i=0;i<GameView::getInstance()->mapteamVector.at(idx)->teammembers_size();i++)
	{
		long long leaderid = GameView::getInstance()->mapteamVector.at(idx)->leaderid();
		auto teammenber = new CTeamMember();
		teammenber->CopyFrom(GameView::getInstance()->mapteamVector.at(idx)->teammembers(i));

		auto teamlistPlayer =TeamMemberList::create(teammenber,leaderid);
		teamlistPlayer->setAnchorPoint(Vec2(0,0));
		teamlistPlayer->setPosition(Vec2(438,310-i*55));
		teamlistPlayer->setTag(50+i);
		teamlistPlayer->setLocalZOrder(10);
		layerOfTeam->addChild(teamlistPlayer);
		delete teammenber;
	}
}

void FriendUi::selectAcceptTeam(Ref* pSender, CheckBox::EventType type)
{
	auto checkbox_ = (CheckBox *)pSender;
	if (checkbox_->isSelected() == true)
	{
		selectIndex=0;
		fuctionValue=1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1405,(void *)selectIndex,(void *)fuctionValue);
		CheckBox_refuse->setSelected(false);
		MyPlayerAIConfig::setAcceptTeam(true);
		MyPlayerAIConfig::setRefuseTeam(false);
	}else
	{
		MyPlayerAIConfig::setAcceptTeam(false);
		selectIndex=0;
		fuctionValue=0;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1405,(void *)selectIndex,(void *)fuctionValue);
	}
}

void FriendUi::selectRefuseTeam(Ref* pSender, CheckBox::EventType type)
{
	auto check_ = (CheckBox *)pSender;
	if (check_->isSelected() == true)
	{
		selectIndex=1;
		fuctionValue=1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1405,(void *)selectIndex,(void *)fuctionValue);
		CheckBox_accept->setSelected(false);
		MyPlayerAIConfig::setAcceptTeam(false);
		MyPlayerAIConfig::setRefuseTeam(true);
	}else
	{
		MyPlayerAIConfig::setRefuseTeam(false);
		selectIndex=1;
		fuctionValue=0;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1405,(void *)selectIndex,(void *)fuctionValue);
	}
}

void FriendUi::callToTogether(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1414);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void FriendUi::autoApplyTeam(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1413);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}


void FriendUi::reloadSourceData(int playerNum)
{
	auto friendTab =(TableView *)friendWorkList->getChildByTag(FRIENDWORKLISTTAB);
	friendTab->reloadData();

	switch(curType)
	{
	case 0:
		{
			const char *str_friend = StringDataManager::getString("friend_LabelshowStr_friend");
			labelFriendType->setString(str_friend);
		}break;
	case 1:
		{
			const char *str_friend = StringDataManager::getString("friend_LabelshowStr_black");
			labelFriendType->setString(str_friend);
		}break;
	case 2:
		{	
			const char *str_friend = StringDataManager::getString("friend_LabelshowStr_enemy");
			labelFriendType->setString(str_friend);
		}break;
	}

	std::string showFriend = "";
	char friendnum_[10];
	sprintf(friendnum_,"%d",playerNum);
	showFriend.append(friendnum_);
	showFriend.append("/50");
	label_Friendnum->setString(showFriend.c_str());
}

void FriendUi::refreshCheckCountryAndPression( bool isSelect )
{
	CheckBox_pro_HaoJie->setSelected(isSelect);
	CheckBox_pro_MengJiang->setSelected(isSelect);
	CheckBox_pro_GuiMou->setSelected(isSelect);
	CheckBox_pro_ShenShe->setSelected(isSelect);

	checkBox_country_yi->setSelected(isSelect);
	checkBox_country_yang->setSelected(isSelect);
	checkBox_country_jing->setSelected(isSelect);
	//checkBox_country_you->setSelected(isSelect);
	//checkBox_country_liang->setSelected(isSelect);
}

void FriendUi::callBackPhypower(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5132);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void FriendUi::setTouchCellType(int type_)
{
	m_touchType = type_;
}

int FriendUi::getTouchCellType()
{
	return m_touchType;
}
