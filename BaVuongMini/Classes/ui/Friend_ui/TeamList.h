#ifndef _UI_FRIEND_TEAMLIST_H_
#define _UI_FRIEND_TEAMLIST_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

typedef enum
{
	TEAMLISTTABVIEW = 504
};
class TeamList:public UIScene,public TableViewDataSource,public TableViewDelegate
{
public:
	TeamList(void);
	~TeamList(void);

	static TeamList * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView * view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);

	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);

	// default implements are used to call script callback if exist
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
private:
	int selectId;
	void callBackDismiss(Ref * obj);
	void callBackApply(Ref * obj);
	void callBackExit(Ref * obj);

	void callBackDismissSure(Ref * obj);
};

#endif