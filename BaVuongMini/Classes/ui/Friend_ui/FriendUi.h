#ifndef _UI_FRIEND_FRIENDUI_H_
#define _UI_FRIEND_FRIENDUI_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class TeamList;
class UITab;
class RichTextInputBox;
class AroundPlayerTabview;
class FriendWorkList;
typedef enum
{
	FRIENDWORKLISTTAG = 501,
	TEAMLISTTAG=502,
	FRIENDADDLAYER=520,
	TEAMLISTLAYERTAG=521,
	LayerOFTEAMTAG = 550
};

enum
{
	FRIENDLIST=0,
	BACKLIST=1,
	ENEMYLIST=2,
	TEAMPLAYER=3,
	AROUNDPLAYER=4,
	CHECKTABLIST_ONEKEY = 5,
	CHECKTABLIST_CHECKBOX = 6,
};

enum
{
	ktypeTouchCellEvent = 0,
	ktypeTouchCellOfMenuEvent,
};

class FriendUi:public UIScene
{
public:
	FriendUi(void);
	~FriendUi(void);

	static FriendUi *create();
	bool init();
	void onEnter();
	void onExit();

	void callBackChangeFriendType(Ref * obj);
	void callBackFriendButton(Ref * obj);
	void callBackChackButton(Ref *pSender, Widget::TouchEventType type);
	void callBackKeyPersonalButton(Ref *pSender, Widget::TouchEventType type);

	void callBackPopCounterLV1(Ref *pSender, Widget::TouchEventType type);
	void callBackPopCounterLV2(Ref *pSender, Widget::TouchEventType type);
	void callBackLabelLV1(Ref * obj);
	void callBackLabelLV2(Ref * obj);

	void callBackPhypower(Ref *pSender, Widget::TouchEventType type);

	void callBackExit(Ref *pSender, Widget::TouchEventType type);

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);

	void reloadSourceData(int playerNum);
	void showfriendOfLine(Ref* pSender, CheckBox::EventType type);
	Text * labelFriendType;
	Text * label_Friendnum;
	Text * labclCheckResult;
	Layer * layer_;
	FriendWorkList* friendWorkList;
	Size winsize;

	UITab* friendType;
	Button * btn_phypower;
	int totalPlayerNum_;//total Player num
public:
	void setTouchCellType(int type_);
	int getTouchCellType();
	int m_touchType;
public:
	//team
	TeamList * teamList;
	void showTeamPlayer(int idx);
	void selectAcceptTeam(Ref* pSender, CheckBox::EventType type);
	void selectRefuseTeam(Ref* pSender, CheckBox::EventType type);

	void callToTogether(Ref *pSender, Widget::TouchEventType type);
	void autoApplyTeam(Ref *pSender, Widget::TouchEventType type);
private:
	UITab* friendWork;

	Layout *friendPanel;
	Layout *palPanel; 
	Layout *checkPanel;
	Layout *checkResultPanel;
	
	Layout *panelTeam;
	Layout *panelOtherplayer;
	Label * image_friendNum;
	CheckBox * CheckBox_pro_HaoJie;
	CheckBox * CheckBox_pro_MengJiang;
	CheckBox * CheckBox_pro_GuiMou;
	CheckBox * CheckBox_pro_ShenShe;

	CheckBox * checkBox_country_yi;
	CheckBox * checkBox_country_yang;
	CheckBox * checkBox_country_jing;
	//UICheckBox * checkBox_country_you;
	//UICheckBox * checkBox_country_liang;

	void refreshCheckCountryAndPression(bool isSelect);

	CheckBox *isShowFriendCheckbox;//���panel ��Ƿ�ֻ��ʾ���
	//Label * showFriendLabel;//label ��Ƿ�ֻ��ʾ���ߺ��

	Button *button_backCheck;//ѷ�� checkpanel
	Button *button_addFriend;//�һ����

	CheckBox * CheckBox_accept;//ӽ�� ܾܾ
	CheckBox * CheckBox_refuse;

	Text * label_lv1;
	Text * label_lv2;

	RichTextInputBox *nameBox;
public:
	void callBackButton_backCheck(Ref *pSender, Widget::TouchEventType type);
	void callbackButton_addFriend(Ref *pSender, Widget::TouchEventType type);


public:
	int operation_;
	int relationtype_;
	long long playerId_;
	std::string playerName_;

	std::vector<int>professionVector;
	std::vector<int>countryVector;
	std::string nameString_;//name check string
	int sexSelect;
	int professionSelect;
	int minLvSelect;
	int maxLvSelect;
	int showOnlineSelect;
	int pageNumSelect;
	int pageIndexSelect;
	/////////
	int cellIdSelect_;
	int cellSelectTag;
	int playerTeamId;
public:
	int curType;
	void selectWorkFriend(Ref * obj);
	//void teamPlayerList(Ref * obj);
	AroundPlayerTabview * aroundplayer;
public:
	int selectIndex;
	int fuctionValue;
	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};

	struct FriendList
	{
		int relationType;
		int pageNum;
		int pageIndex;
		int showOnline;
	};
};

#endif

