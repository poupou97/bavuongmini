#ifndef _UI_FRIEND_FRIENDINFOLIST_H_
#define _UI_FRIEND_FRIENDINFOLIST_H_


#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"


enum {
	_PRIVATE=0,
	_CHECK,
	_TEAM,
	_MAIL,
	_DELETE,
	_BACKLIST,
	_ADDFRIEND,
	_REVENGE,
};
class FriendInfoList:public UIScene
{
public:
	FriendInfoList(void);
	~FriendInfoList(void);

	static FriendInfoList * create(int currType);

	bool init(int currType);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *touch, Event * pEvent);

	void callBackFriendList(Ref * obj);
	void callBackBackList(Ref * obj);
	void callBackEnemyList(Ref * obj);
	void callBackAroundPlayer(Ref * obj);
	void callBackCheckList(Ref * obj);

	void callBackPhypower(Ref * obj);

	void deleteFriendSure(Ref * obj);
	void backListFriendSure(Ref * obj);
public:
	void deldtePhypowerSureDelete(Ref * obj);
	void backListPhypowerSure(Ref * obj);

public:
	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};
};

#endif

