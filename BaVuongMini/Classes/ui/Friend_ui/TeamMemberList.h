#ifndef _TEAMMEMBERLIST_H
#define _TEAMMEMBERLIST_H

#include "../extensions/UIScene.h"


class CTeamMember;
class TeamMemberList:public UIScene
{
public:
	TeamMemberList(void);
	~TeamMemberList(void);

	static TeamMemberList *create(CTeamMember * teammenber,long long leaderid);
	bool init(CTeamMember * teammenber,long long leaderid);

	void onEnter();
	void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
	void callBackUitab(Ref *pSender, Widget::TouchEventType type);
private:
	CTeamMember * m_teammenber;
	long long m_leaderid;

};

////////////////////////////////////////////////////
class TeamMemberListOperator:public UIScene
{
public:
	TeamMemberListOperator(void);
	~TeamMemberListOperator(void);

	static TeamMemberListOperator *create(CTeamMember * teammenber,long long leaderid);
	bool init(CTeamMember * teammenber,long long leaderid);

	void onEnter();
	void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);

	void callBack1(Ref * obj);
	void callBack2(Ref * obj);
private:
	Size winSize;
	CTeamMember * mm_teammenber;

	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};
};

#endif