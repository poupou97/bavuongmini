#ifndef _UI_FRIEND_AROUNDPLAYERTABVIEWCELL_H_
#define _UI_FRIEND_AROUNDPLAYERTABVIEWCELL_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../extensions/UIScene.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class AroundPlayerTabviewCell:public UIScene
{
public:
	AroundPlayerTabviewCell(void);
	~AroundPlayerTabviewCell(void);

	static AroundPlayerTabviewCell * create(int idx);
	bool init(int idx);
	void onEnter();
	void onExit();

	void callBack(Ref * obj);
private:
	int selectCellId;
};

#endif
