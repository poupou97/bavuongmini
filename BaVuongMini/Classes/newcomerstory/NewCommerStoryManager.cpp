#include "NewCommerStoryManager.h"
#include "NewCommerStory.h"
#include "command/NSTextDesAnm.h"
#include "../GameView.h"
#include "../gamescene_state/GameSceneState.h"
#include "command/NSRunMovieScript.h"
#include "command/NSFight.h"
#include "command/NSMove.h"
#include "../gamescene_state/MainScene.h"
#include "../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../gamescene_state/role/ActorUtils.h"
#include "../gamescene_state/role/BasePlayer.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../messageclient/GameMessageProcessor.h"
#include "command/NSSelectCountry.h"
#include "../gamescene_state/sceneelement/TriangleButton.h"
#include "../messageclient/element/CGeneralBaseMsg.h"
#include "../messageclient/element/CGeneralDetail.h"
#include "../gamescene_state/sceneelement/GeneralsInfoUI.h"
#include "../gamescene_state/role/MyPlayerAIConfig.h"
#include "../gamescene_state/role/MyPlayerAI.h"

NewCommerStoryManager* NewCommerStoryManager::s_instance = NULL;

NewCommerStoryManager::NewCommerStoryManager():
m_bIsNewComer(false),
m_bIsSelectCountryFinished(false),
m_bIsReqForUseMusouSkill(false)
{
	m_storyState = ss_notStart;
	m_pCurStory = new NewCommerStory();
}


NewCommerStoryManager::~NewCommerStoryManager()
{
	delete m_pCurStory;
}

NewCommerStoryManager* NewCommerStoryManager::getInstance() {
	if (s_instance == NULL) {
		s_instance = new NewCommerStoryManager();
	}
	return s_instance;
}	

void NewCommerStoryManager::initData()
{
	//delete old data
	std::vector<NewCommerStory*>::iterator iter;
	for (unsigned int i = 0; i < mAllStoryCommandList.size(); i++) {
		iter = mAllStoryCommandList.begin() + i;
		auto ns = *iter;
		mAllStoryCommandList.erase(iter);
		i--;
		continue;
	}
	mAllStoryCommandList.clear();

	//add new data
	//���
	auto textDesAnm = new NSTextDesAnm1();
	textDesAnm->init();
	mAllStoryCommandList.push_back(textDesAnm);
 	//ֲ��ž��鶯��1���������ŽǺ͵�һ���ƽ�
 	auto runMovieScript = new NSRunMovieScript();
 	runMovieScript->init("Newplayer1.sc");
 	mAllStoryCommandList.push_back(runMovieScript);
	// ���Ҳ���1֡������ܽ�ѧ��ɱ��һ���ƽ�
	std::map<long long,int >tempList1;
	tempList1.insert(make_pair(528,3));
	tempList1.insert(make_pair(529,3));
	tempList1.insert(make_pair(530,3));
	NSFight5 * fight1 = new NSFight5();
	fight1->init(tempList1);
	mAllStoryCommandList.push_back(fight1);
	//��ƶ���¥���
	NSMoveToStairs * moveToStair  = new NSMoveToStairs();
	moveToStair->init();
	mAllStoryCommandList.push_back(moveToStair);
	//²��ž��鶯��6�������ֵڶ�ƽ�
	runMovieScript = new NSRunMovieScript();
	runMovieScript->init("Newplayer2.sc");
	mAllStoryCommandList.push_back(runMovieScript);
	//���Ҳ���2֡����佫���󡣡����佫��ս��ѧ
	std::map<long long,int >tempList2;
	tempList2.insert(make_pair(531,3));
	tempList2.insert(make_pair(532,3));
	tempList2.insert(make_pair(533,3));
	NSFight6 * fight2 = new NSFight6();
	fight2->init(tempList2);
	mAllStoryCommandList.push_back(fight2);
	//���ž��鶯��6������ʾ�����
	runMovieScript = new NSRunMovieScript();
	runMovieScript->init("Newplayer3.sc");
	mAllStoryCommandList.push_back(runMovieScript);
	//���Ҳ���2֡����ƶ�������
	NSMove * move  = new NSMove();
	move->init();
	mAllStoryCommandList.push_back(move);
	//Ų��ž��鶯��1����Ӣ���ѹ����˹
	runMovieScript = new NSRunMovieScript();
	runMovieScript->init("Newplayer4.sc");
	mAllStoryCommandList.push_back(runMovieScript);
	//���Ҳ���֡���ȫ���佫���󡣡����佫��ս�ڶ�������ѧ
	std::map<long long,int >tempList3;
	tempList3.insert(make_pair(499,1));
	tempList3.insert(make_pair(534,6));
	tempList3.insert(make_pair(535,5));
	tempList3.insert(make_pair(536,5));
	tempList3.insert(make_pair(537,9));
	NSFight7 * fight3 = new NSFight7();
	fight3->init(tempList3);
	mAllStoryCommandList.push_back(fight3);
	//���ž��鶯��1�������һ����
	runMovieScript = new NSRunMovieScript();
	runMovieScript->init("Newplayer5.sc");
	mAllStoryCommandList.push_back(runMovieScript);
	//�ѡ���
	NSSelectCountry * selectCountry = new NSSelectCountry();
	selectCountry->init();
	mAllStoryCommandList.push_back(selectCountry);

// 	//Ҳ��ž��鶯��1�����������뻤��ս��
// 	auto runMovieScript = new NSRunMovieScript();
// 	runMovieScript->init("Newplayer1.sc");
// 	mAllStoryCommandList.push_back(runMovieScript);
// 	// ��Ҳ���1֡����ƶ������弼�ܽ�ѧ��ɱǰ����
// 	std::map<long long,int >tempList1;
// 	tempList1.insert(make_pair(500,1));
// 	NSFight1 * fight1 = new NSFight1();
// 	fight1->init(tempList1);
// 	mAllStoryCommandList.push_back(fight1);
// 	//ʣ������
// 	runMovieScript = new NSRunMovieScript();
// 	runMovieScript->init("Newplayer6.sc");
// 	mAllStoryCommandList.push_back(runMovieScript);
// 	// ��Ҳ���1֡���Ⱥ�弼�ܽ�ѧ��ɱʣ������
// 	std::map<long long,int >tempList4;
// 	tempList4.insert(make_pair(501,1));
// 	tempList4.insert(make_pair(502,1));
// 	tempList4.insert(make_pair(503,1));
// 	NSFight4 * fight4 = new NSFight4();
// 	fight4->init(tempList4);
// 	mAllStoryCommandList.push_back(fight4);
// 	//���ž��鶯��6������ʾ�����
// 	runMovieScript = new NSRunMovieScript();
// 	runMovieScript->init("Newplayer2.sc");
// 	mAllStoryCommandList.push_back(runMovieScript);
// 	//���Ҳ���2֡����ƶ�������
// 	NSMove * move  = new NSMove();
// 	move->init();
// 	mAllStoryCommandList.push_back(move);
// 	//ŶԻ�5����ʥŮ������֣����˶Ի�
// 	runMovieScript = new NSRunMovieScript();
// 	runMovieScript->init("Newplayer3.sc");
// 	mAllStoryCommandList.push_back(runMovieScript);
// 	//��Ҳ���3֡�����ħ�ǡ�������Ůս��
// 	std::map<long long,int >tempList2;
// 	tempList2.insert(make_pair(499,1));
// 	tempList2.insert(make_pair(504,2));
// 	tempList2.insert(make_pair(505,2));
// 	tempList2.insert(make_pair(506,2));
// 	tempList2.insert(make_pair(507,2));
// 	tempList2.insert(make_pair(508,2));
// 	tempList2.insert(make_pair(509,2));
// 	tempList2.insert(make_pair(510,2));
// 	tempList2.insert(make_pair(511,2));
// 	NSFight2 *fight2 = new NSFight2();
// 	fight2->init(tempList2);
// 	mAllStoryCommandList.push_back(fight2);
// 	//�Ի�7����������꼼�
// 	runMovieScript = new NSRunMovieScript();
// 	runMovieScript->init("Newplayer4.sc");
// 	mAllStoryCommandList.push_back(runMovieScript);
// 	//���Ҳ���4֡���ʩ����꼼�
// 	std::map<long long,int >tempList3;
// 	tempList3.insert(make_pair(499,1));
// 	tempList3.insert(make_pair(504,5));
// 	tempList3.insert(make_pair(505,5));
// 	tempList3.insert(make_pair(506,5));
// 	tempList3.insert(make_pair(507,5));
// 	tempList3.insert(make_pair(508,2));
// 	tempList3.insert(make_pair(509,2));
// 	tempList3.insert(make_pair(510,2));
// 	tempList3.insert(make_pair(511,2));
// 	NSFight3 *fight3 = new NSFight3();
// 	fight3->init(tempList3);
// 	mAllStoryCommandList.push_back(fight3);
// 	//ܶԻ�8����ħ�����
// 	runMovieScript = new NSRunMovieScript();
// 	runMovieScript->init("Newplayer5.sc");
// 	mAllStoryCommandList.push_back(runMovieScript);
// 	//�ѡ���
// 	NSSelectCountry * selectCountry = new NSSelectCountry();
// 	selectCountry->init();
// 	mAllStoryCommandList.push_back(selectCountry);
}

void NewCommerStoryManager::update()
{
	if (m_storyState != ss_running)
		return;

    //Ҹ���ִ�Command
	std::vector<NewCommerStory*>::iterator iter;
	for (unsigned int i = 0; i < mAllStoryCommandList.size(); i++) {
		iter = mAllStoryCommandList.begin() + i;
		auto ns = *iter;
		if (ns == NULL) {
			mAllStoryCommandList.erase(iter);
			i--;
			continue;
		}
		if (ns->isEnd()) {
			// �ָ���Ѿ������ͷ
			mAllStoryCommandList.erase(iter);
			i--;
			continue;
		}
		m_pCurStory = ns;
		ns->update();
		break;
	}

	if (mAllStoryCommandList.size() <= 0)
	{
		//endStory();
		m_storyState = ss_ended;
		// login in game
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5106);
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1001);
	}
}

void NewCommerStoryManager::startStory()
{
	//Ű�����������vector��
	initData();

	m_storyState = ss_running;
}

void NewCommerStoryManager::endStory()
{
	//select country
	//GameMessageProcessor::sharedMsgProcessor()->sendReq(5103, (void*)1);

// 	// login in game
// 	GameMessageProcessor::sharedMsgProcessor()->sendReq(5106);
// 	GameMessageProcessor::sharedMsgProcessor()->sendReq(1001);

	m_bIsNewComer = false;
	m_storyState = ss_ended;
	m_bIsSelectCountryFinished = false;
	m_bIsReqForUseMusouSkill = false;

	//аѿͻ��˴���佫��Ϣ�ÿ
	std::vector<CGeneralBaseMsg*>::iterator iter_base;
	for (iter_base = GameView::getInstance()->generalsInLineList.begin(); iter_base != GameView::getInstance()->generalsInLineList.end(); ++iter_base)
	{
		delete *iter_base;
	}
	GameView::getInstance()->generalsInLineList.clear();

	std::vector<CGeneralDetail*>::iterator iter_detail;
	for (iter_detail = GameView::getInstance()->generalsInLineDetailList.begin(); iter_detail != GameView::getInstance()->generalsInLineDetailList.end(); ++iter_detail)
	{
		delete *iter_detail;
	}
	GameView::getInstance()->generalsInLineDetailList.clear();

	//set autoKill off
	MyPlayerAIConfig::setAutomaticSkill(0);
	MyPlayerAIConfig::enableRobot(false);
	GameView::getInstance()->myplayer->getMyPlayerAI()->stop();

	this->setMainSceneState(1);
}

void NewCommerStoryManager::setMainSceneState( int state )
{
	MainScene * mainScene = GameView::getInstance()->getMainUIScene();
	if (!mainScene)
		return;

	if(state == 1)
	{
		Layer * myHeadLayer = (Layer*)mainScene->getChildByTag(kTagMyHeadInfo);
		if (myHeadLayer)
		{
			myHeadLayer->setVisible(true);
		}

		Layer * headMenuLayer = (Layer*)mainScene->getChildByTag(kTagHeadMenu);
		if (headMenuLayer)
		{
			headMenuLayer->setVisible(true);
		}

		mainScene->Button_mapOpen->setVisible(true);
		Layer * guideMapLayer = (Layer*)mainScene->getChildByTag(kTagGuideMapUI);
		if (guideMapLayer)
		{
			guideMapLayer->setVisible(true);
		}

		mainScene->generalHeadLayer->setVisible(true);
		mainScene->targetInfoLayer->setVisible(true);
		mainScene->chatLayer->setVisible(true);
		mainScene->ExpLayer->setVisible(true);

		Layer * missionAndTeamLayer = (Layer*)mainScene->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeamLayer)
		{
			missionAndTeamLayer->setVisible(true);
		}

		ShortcutLayer * shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
		if (shortcutLayer)
		{
			shortcutLayer->setVisible(true);
			shortcutLayer->setMusouShortCutSlotPresent(true);
		}

		Node * pJoystick = (Layer*)mainScene->getChildByTag(kTagVirtualJoystick);
		if (pJoystick)
		{
			pJoystick->setVisible(true);
		}

		//����½���ǰ�ť
		TriangleButton * triangleBtn = (TriangleButton*)mainScene->getChildByTag(78);
		if (triangleBtn)
		{
			triangleBtn->setVisible(true);
		}
	}
	else if (state == 2)
	{
		Layer * myHeadLayer = (Layer*)mainScene->getChildByTag(kTagMyHeadInfo);
		if (myHeadLayer)
		{
			myHeadLayer->setVisible(false);
		}

		Layer * headMenuLayer = (Layer*)mainScene->getChildByTag(kTagHeadMenu);
		if (headMenuLayer)
		{
			headMenuLayer->setVisible(false);
		}

		mainScene->Button_mapOpen->setVisible(false);
		Layer * guideMapLayer = (Layer*)mainScene->getChildByTag(kTagGuideMapUI);
		if (guideMapLayer)
		{
			guideMapLayer->setVisible(false);
		}

		mainScene->generalHeadLayer->setVisible(false);
		mainScene->targetInfoLayer->setVisible(false);
		mainScene->chatLayer->setVisible(false);
		mainScene->ExpLayer->setVisible(false);

		Layer * missionAndTeamLayer = (Layer*)mainScene->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeamLayer)
		{
			missionAndTeamLayer->setVisible(false);
		}

		ShortcutLayer * shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
		if (shortcutLayer)
		{
			shortcutLayer->setVisible(false);
			shortcutLayer->setMusouShortCutSlotPresent(false);
		}

		Node * pJoystick = (Layer*)mainScene->getChildByTag(kTagVirtualJoystick);
		if (pJoystick)
		{
			pJoystick->setVisible(false);
		}

		//���½���ǰ�ť
		TriangleButton * triangleBtn = (TriangleButton*)mainScene->getChildByTag(78);
		if (triangleBtn)
		{
			triangleBtn->setVisible(false);
		}
	}
	else
	{

	}
}

void NewCommerStoryManager::setMyPlayerEffect( int state )
{
	if (!GameView::getInstance()->myplayer->getAnim())
		return;

	std::string pressionStr = GameView::getInstance()->myplayer->getActiveRole()->profession();
	int pression_ = BasePlayer::getProfessionIdxByName(pressionStr);

	if(state == 1)
	{
		BasePlayer::switchWeapon("", GameView::getInstance()->myplayer->getAnim(), pression_);
		GameView::getInstance()->myplayer->getActiveRole()->set_hand("");
		GameView::getInstance()->myplayer->initWeaponEffect();
	}
	else if (state == 2)
	{
		int refineLevel_ = 20;
		int starLevel_ = 20;

		BasePlayer::switchWeapon(this->getBestWeaponByProfession(pression_).c_str(), GameView::getInstance()->myplayer->getAnim(), pression_);
		GameView::getInstance()->myplayer->getActiveRole()->set_hand(this->getBestWeaponByProfession(pression_).c_str());

		std::string effectNameForRefine = ActorUtils::getWeapEffectByRefineLevel(pression_,refineLevel_);
		GameView::getInstance()->myplayer->addWeaponEffect(effectNameForRefine.c_str());

		std::string effectNameForStar = ActorUtils::getWeapEffectByStarLevel(pression_, starLevel_);
		GameView::getInstance()->myplayer->addWeaponEffect(effectNameForStar.c_str());
	}
	else
	{}
}

void NewCommerStoryManager::clear()
{
	this->m_storyState = NewCommerStoryManager::ss_notStart;
	m_bIsNewComer = false;
	m_bIsSelectCountryFinished = false;
	m_bIsReqForUseMusouSkill = false;
	std::vector<NewCommerStory*>::iterator iter;
	for (unsigned int i = 0; i < mAllStoryCommandList.size(); i++) {
		iter = mAllStoryCommandList.begin() + i;
		auto ns = *iter;
		if (ns == NULL) {
			mAllStoryCommandList.erase(iter);
		}
	}

	m_pDiedEmptyIdList.clear();
}

std::string NewCommerStoryManager::getBestWeaponByProfession( int profession )
{
	std::string pressionName_="";
	switch(profession)
	{
	case PROFESSION_MJ_INDEX:
		{
			pressionName_.append("lance08");
		}break;
	case PROFESSION_GM_INDEX:
		{
			pressionName_.append("fan08");
		}break;
	case PROFESSION_HJ_INDEX:
		{
			pressionName_.append("knife08");
		}break;
	case PROFESSION_SS_INDEX:
		{
			pressionName_.append("bow08");
		}break;
	}
	return pressionName_;
}

void NewCommerStoryManager::startAutoGuideToCallGeneral()
{
	MainScene * mainScene = GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI*)mainScene->getGeneralInfoUI();
		if (generalsInfoUI)
		{
			generalsInfoUI->setAutoGuideToCallGeneral(true);
		}
	}
}

void NewCommerStoryManager::endAutoGuideToCallGeneral()
{
	MainScene * mainScene = GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI*)mainScene->getGeneralInfoUI();
		if (generalsInfoUI)
		{
			generalsInfoUI->setAutoGuideToCallGeneral(false);
		}
	}
}
