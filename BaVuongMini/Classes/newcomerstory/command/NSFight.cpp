#include "NSFight.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/MainAnimationScene.h"
#include "../../legend_script/ScriptManager.h"
#include "../NewCommerStoryManager.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../gamescene_state/role/SimpleActor.h"
#include "../../ui/GameUIConstant.h"
#include "../../gamescene_state/sceneelement/GeneralsInfoUI.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../gamescene_state/role/MyPlayerAI.h"

NSFight1::NSFight1(void)
{
}


NSFight1::~NSFight1(void)
{
}

void* NSFight1::createInstance()
{
	return new NSFight1() ;
}

void NSFight1::init( std::map<long long,int> targetEmptyIdList )
{
	m_pTargetEmptyIdList.clear();
	std::map<long long,int>::iterator it = targetEmptyIdList.begin();
	for ( ; it != targetEmptyIdList.end(); ++ it )
	{
		long long templateId  = it->first;
		int num = it->second;
		m_pTargetEmptyIdList.insert(make_pair(templateId,num));
	}

	setState(STATE_INITED);
}

void NSFight1::update() {
	switch (mState) {
	case STATE_INITED:
		{
			GameView::getInstance()->myplayer->setVisible(true);
			//ˢ�¹��
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5107,(void *)1);
			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSFight");

			auto mainScene = GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				NewCommerStoryManager::getInstance()->setMainSceneState(2);
				auto shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
				if (shortcutLayer)
				{
					shortcutLayer->setVisible(true);
					shortcutLayer->setMusouShortCutSlotPresent(false);
					//just present one shortCurSlot
					shortcutLayer->presentShortCutSlot(1);
				}

				auto pJoystick = (Layer*)mainScene->getChildByTag(kTagVirtualJoystick);
				if (pJoystick)
				{
					pJoystick->setVisible(true);
				}

				//�Ѵ�����������Ϊ���ɴ��
				GameView::getInstance()->getGameScene()->setTiledMapLimit(17,17,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(16,18,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(17,18,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(18,18,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(16,19,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(17,19,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(18,19,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(17,20,0);
				
				//��ѧ���ƶ��͵㼼�ܴ�
				std::string scriptFileName = "script/nsMoveAndKillMonster.sc";
				ScriptManager::getInstance()->runScript(scriptFileName);

				setState(STATE_UPDATE);
			}	
		}
		break;
	case STATE_UPDATE:
		{
			for (std::map<long long,int>::iterator it_all = NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.begin() ; it_all != NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.end(); ++ it_all )
			{
				long long templateId_all  = it_all->first;
				int num_all = it_all->second;
				for (std::map<long long,int>::iterator it_cur = this->m_pTargetEmptyIdList.begin() ; it_cur != this->m_pTargetEmptyIdList.end(); ++ it_cur )
				{
					long long templateId_cur  = it_cur->first;
					int num_cur = it_cur->second;
					if (templateId_cur == templateId_all && num_all >= num_cur)
					{
						this->m_pTargetEmptyIdList.erase(it_cur);
						break;;
					}
				}
			}

			if (this->m_pTargetEmptyIdList.size() <= 0)
			{
				setState(STATE_END);
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////

NSFight2::NSFight2(void)
{
}


NSFight2::~NSFight2(void)
{
}

void* NSFight2::createInstance()
{
	return new NSFight2() ;
}

void NSFight2::init( std::map<long long,int> targetEmptyIdList )
{
	m_nDelayTime = 0.f;

	m_pTargetEmptyIdList.clear();
	std::map<long long,int>::iterator it = targetEmptyIdList.begin();
	for ( ; it != targetEmptyIdList.end(); ++ it )
	{
		long long templateId  = it->first;
		int num = it->second;
		m_pTargetEmptyIdList.insert(make_pair(templateId,num));
	}

	setState(STATE_INITED);
}

void NSFight2::update() {
	switch (mState) {
	case STATE_INITED:
		{
			GameView::getInstance()->myplayer->setVisible(true);
			//�ˢ�¹��
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5107,(void *)3);
			//��г��佫
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5105);

			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSFight2");

			auto mainScene = GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				NewCommerStoryManager::getInstance()->setMainSceneState(2);
				auto shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
				if (shortcutLayer)
				{
					shortcutLayer->setVisible(true);
					shortcutLayer->setMusouShortCutSlotPresent(false);
					//present three shortCurSlot
					shortcutLayer->presentShortCutSlot(3);
				}

				auto pJoystick = (Layer*)mainScene->getChildByTag(kTagVirtualJoystick);
				if (pJoystick)
				{
					pJoystick->setVisible(true);
				}

				//��ѧ���㼼�ܴ�
				std::string scriptFileName = "script/nsKillMonster.sc";
				ScriptManager::getInstance()->runScript(scriptFileName);

				setState(STATE_UPDATE);
			}	
		}
		break;
	case STATE_UPDATE:
		{
			for (std::map<long long,int>::iterator it_all = NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.begin() ; it_all != NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.end(); ++ it_all )
			{
				long long templateId_all  = it_all->first;
				int num_all = it_all->second;
				for (std::map<long long,int>::iterator it_cur = this->m_pTargetEmptyIdList.begin() ; it_cur != this->m_pTargetEmptyIdList.end(); ++ it_cur )
				{
					long long templateId_cur  = it_cur->first;
					int num_cur = it_cur->second;
					if (templateId_cur == templateId_all && num_all >= num_cur)
					{
						this->m_pTargetEmptyIdList.erase(it_cur);
						break;;
					}
				}
			}

			if (this->m_pTargetEmptyIdList.size() <= 0)
			{
				setState(STATE_END);
				break;
			}

			if (m_nDelayTime > 15.0f)
			{
				setState(STATE_END);
			}
			else
			{
				m_nDelayTime += 1.0f/60;
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}




////////////////////////////////////////////////////////////////////////////////////////////////

NSFight3::NSFight3(void)
{
}


NSFight3::~NSFight3(void)
{
}

void* NSFight3::createInstance()
{
	return new NSFight3() ;
}

void NSFight3::init( std::map<long long,int> targetEmptyIdList )
{
	m_nDelayTime = 0.f;

	m_pTargetEmptyIdList.clear();
	std::map<long long,int>::iterator it = targetEmptyIdList.begin();
	for ( ; it != targetEmptyIdList.end(); ++ it )
	{
		long long templateId  = it->first;
		int num = it->second;
		m_pTargetEmptyIdList.insert(make_pair(templateId,num));
	}

	setState(STATE_INITED);
}

void NSFight3::update() {
	switch (mState) {
	case STATE_INITED:
		{
			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSFight3");

			auto mainScene = GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				NewCommerStoryManager::getInstance()->setMainSceneState(2);
				auto shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
				if (shortcutLayer)
				{
					shortcutLayer->setVisible(true);
					shortcutLayer->createMusouShortCutSlotForce();
					shortcutLayer->setMusouShortCutSlotPresent(true);
				}

				auto pJoystick = (Layer*)mainScene->getChildByTag(kTagVirtualJoystick);
				if (pJoystick)
				{
					pJoystick->setVisible(true);
				}

				//ֽ�ѧ��ʹ����꼼�
				std::string scriptFileName = "script/nsMusouSkill.sc";
				ScriptManager::getInstance()->runScript(scriptFileName);

				setState(STATE_UPDATE);
			}	
		}
		break;
	case STATE_UPDATE:
		{
			for (std::map<long long,int>::iterator it_all = NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.begin() ; it_all != NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.end(); ++ it_all )
			{
				long long templateId_all  = it_all->first;
				int num_all = it_all->second;
				for (std::map<long long,int>::iterator it_cur = this->m_pTargetEmptyIdList.begin() ; it_cur != this->m_pTargetEmptyIdList.end(); ++ it_cur )
				{
					long long templateId_cur  = it_cur->first;
					int num_cur = it_cur->second;
					if (templateId_cur == templateId_all && num_all >= num_cur)
					{
						this->m_pTargetEmptyIdList.erase(it_cur);
						break;;
					}
				}
			}

			if (this->m_pTargetEmptyIdList.size() <= 0)
			{
				auto mainScene = GameView::getInstance()->getMainUIScene();
				auto shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
				if (shortcutLayer)
				{
					shortcutLayer->setVisible(true);
					shortcutLayer->recoverMusouShortCutSlot();
				}

				//�����Ĭ�Ϲ�
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5104);
				setState(STATE_END);
				break;
			}

			if (NewCommerStoryManager::getInstance()->IsReqForUseMusouSkill())
			{
				m_nDelayTime += 1.0f/60;
				if (m_nDelayTime >= 1.0f)
				{
					//notice server to delete all monsters
					GameMessageProcessor::sharedMsgProcessor()->sendReq(5108);
					//delete all monsters in client
					GameSceneLayer* scene = GameView::getInstance()->getGameScene();
					std::map<long long,GameActor*>::iterator it = scene->getActorsMap().begin();
					for ( ;it != scene->getActorsMap().end(); ++it)
					{
						Monster* pMonster = dynamic_cast<Monster*>(it->second);
						if(pMonster != NULL)
						{
// 							pMonster->removeFromParentAndCleanup(true);
// 							pMonster->release();
// 							scene->getActorsMap().erase(it->first);
							scene->removeActor(it->first);
							it = scene->getActorsMap().begin();
							continue;
						}
					}

					auto mainScene = GameView::getInstance()->getMainUIScene();
					auto shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
					if (shortcutLayer)
					{
						shortcutLayer->setVisible(true);
						shortcutLayer->recoverMusouShortCutSlot();
					}

					//�����Ĭ�Ϲ�
					GameMessageProcessor::sharedMsgProcessor()->sendReq(5104);
					setState(STATE_END);
					break;
				}
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}

/////////////////////////////////////////////////////////////////////////////////

NSFight4::NSFight4(void)
{
}


NSFight4::~NSFight4(void)
{
}

void* NSFight4::createInstance()
{
	return new NSFight4() ;
}

void NSFight4::init( std::map<long long,int> targetEmptyIdList )
{
	m_pTargetEmptyIdList.clear();
	std::map<long long,int>::iterator it = targetEmptyIdList.begin();
	for ( ; it != targetEmptyIdList.end(); ++ it )
	{
		long long templateId  = it->first;
		int num = it->second;
		m_pTargetEmptyIdList.insert(make_pair(templateId,num));
	}

	setState(STATE_INITED);
}

void NSFight4::update() {
	switch (mState) {
	case STATE_INITED:
		{
			//�ˢ�¹��
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5107,(void *)2);
			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSFight4");

			auto mainScene = GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				NewCommerStoryManager::getInstance()->setMainSceneState(2);
				auto shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
				if (shortcutLayer)
				{
					shortcutLayer->setVisible(true);
					shortcutLayer->setMusouShortCutSlotPresent(false);
					//just present one shortCurSlot
					shortcutLayer->presentShortCutSlot(2);
				}

				auto pJoystick = (Layer*)mainScene->getChildByTag(kTagVirtualJoystick);
				if (pJoystick)
				{
					pJoystick->setVisible(true);
				}

				//�Ѵ�����������Ϊ���ɴ��
				GameView::getInstance()->getGameScene()->setTiledMapLimit(41,48,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(42,48,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(43,48,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(44,48,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(41,49,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(42,49,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(43,49,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(44,49,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(41,51,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(42,51,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(43,51,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(44,51,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(41,52,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(42,52,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(43,52,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(44,52,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(41,53,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(42,53,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(43,53,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(44,53,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(41,54,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(42,54,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(43,54,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(44,54,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(41,55,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(42,55,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(43,55,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(44,55,0);

				//��ѧ��Ⱥ�弼�ܴ�
				std::string scriptFileName = "script/nsUseGroupSkill.sc";
				ScriptManager::getInstance()->runScript(scriptFileName);

				setState(STATE_UPDATE);
			}	
		}
		break;
	case STATE_UPDATE:
		{
			for (std::map<long long,int>::iterator it_all = NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.begin() ; it_all != NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.end(); ++ it_all )
			{
				long long templateId_all  = it_all->first;
				int num_all = it_all->second;
				for (std::map<long long,int>::iterator it_cur = this->m_pTargetEmptyIdList.begin() ; it_cur != this->m_pTargetEmptyIdList.end(); ++ it_cur )
				{
					long long templateId_cur  = it_cur->first;
					int num_cur = it_cur->second;
					if (templateId_cur == templateId_all && num_all >= num_cur)
					{
						this->m_pTargetEmptyIdList.erase(it_cur);
						break;;
					}
				}
			}

			if (this->m_pTargetEmptyIdList.size() <= 0)
			{
				setState(STATE_END);
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}



////////////////////////////////////////////////////////////////////////////////////////////////

NSFight5::NSFight5(void)
{
}


NSFight5::~NSFight5(void)
{
}

void* NSFight5::createInstance()
{
	return new NSFight5() ;
}

void NSFight5::init( std::map<long long,int> targetEmptyIdList )
{
	m_nDelayTime = 0.f;

	m_pTargetEmptyIdList.clear();
	std::map<long long,int>::iterator it = targetEmptyIdList.begin();
	for ( ; it != targetEmptyIdList.end(); ++ it )
	{
		long long templateId  = it->first;
		int num = it->second;
		m_pTargetEmptyIdList.insert(make_pair(templateId,num));
	}

	setState(STATE_INITED);
}

void NSFight5::update() {
	switch (mState) {
	case STATE_INITED:
		{
			GameView::getInstance()->myplayer->setVisible(true);
			//�ˢ�¹��
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5107,(void *)1);

			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSFight5");

			//�Ѵ�����������Ϊ���ɴ��
			GameView::getInstance()->getGameScene()->setTiledMapLimit(17,17,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(16,18,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(17,18,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(18,18,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(16,19,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(17,19,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(18,19,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(17,20,0);

			//��м�¥�ݴ���Ϊ���ɴ��
			GameView::getInstance()->getGameScene()->setTiledMapLimit(4,20,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(4,21,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(5,19,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(5,20,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(6,18,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(6,19,0);
			//��դ�
			GameSceneLayer* scene = GameView::getInstance()->getGameScene();
			std::string animFileName = "animation/texiao/changjingtexiao/ZHAMEN/zhamen2.anm";
			SimpleActor* simpleActor = new SimpleActor();
			simpleActor->setGameScene(scene);
			simpleActor->setPosition(Vec2(343,1024-650));
			simpleActor->setLayerId(0);   // ground layer
			simpleActor->loadAnim(animFileName.c_str(),true);
			Node* actorLayer = scene->getActorLayer();
			actorLayer->addChild(simpleActor,-1,600);
			simpleActor->release();

			auto mainScene = GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				NewCommerStoryManager::getInstance()->setMainSceneState(2);
				auto shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
				if (shortcutLayer)
				{
					shortcutLayer->setVisible(true);
					shortcutLayer->setMusouShortCutSlotPresent(false);
					//present three shortCurSlot
					shortcutLayer->presentShortCutSlot(3);
				}

				auto pJoystick = (Layer*)mainScene->getChildByTag(kTagVirtualJoystick);
				if (pJoystick)
				{
					pJoystick->setVisible(true);
				}

// 				//��ѧ���㼼�ܴ�
// 				std::string scriptFileName = "script/nsKillMonster.sc";
// 				ScriptManager::getInstance()->runScript(scriptFileName);
				//ֽ�ѧ���ƶ��͵㼼�ܴ�
				std::string scriptFileName = "script/nsMoveAndKillMonster.sc";
				ScriptManager::getInstance()->runScript(scriptFileName);

				setState(STATE_UPDATE);
			}	
		}
		break;
	case STATE_UPDATE:
		{
			for (std::map<long long,int>::iterator it_all = NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.begin() ; it_all != NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.end(); ++ it_all )
			{
				long long templateId_all  = it_all->first;
				int num_all = it_all->second;
				for (std::map<long long,int>::iterator it_cur = this->m_pTargetEmptyIdList.begin() ; it_cur != this->m_pTargetEmptyIdList.end(); ++ it_cur )
				{
					long long templateId_cur  = it_cur->first;
					int num_cur = it_cur->second;
					if (templateId_cur == templateId_all && num_all >= num_cur)
					{
						this->m_pTargetEmptyIdList.erase(it_cur);
						CCLOG("erase erase erase !!!");
						break;;
					}
				}
			}

			if (this->m_pTargetEmptyIdList.size() <= 0)
			{
				setState(STATE_END);
				break;
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}


////////////////////////////////////////////////////////////////////////////////////////////////

NSFight6::NSFight6(void)
{
}


NSFight6::~NSFight6(void)
{
}

void* NSFight6::createInstance()
{
	return new NSFight6() ;
}

void NSFight6::init( std::map<long long,int> targetEmptyIdList )
{
	m_nDelayTime = 0.f;

	m_pTargetEmptyIdList.clear();
	std::map<long long,int>::iterator it = targetEmptyIdList.begin();
	for ( ; it != targetEmptyIdList.end(); ++ it )
	{
		long long templateId  = it->first;
		int num = it->second;
		m_pTargetEmptyIdList.insert(make_pair(templateId,num));
	}

	setState(STATE_INITED);
}

void NSFight6::update() {
	switch (mState) {
	case STATE_INITED:
		{
			GameView::getInstance()->myplayer->setVisible(true);
			//�ˢ�¹��
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5107,(void *)2);
			//��һ���佫���
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5105,(void *)1);

			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSFight6");

			auto mainScene = GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				NewCommerStoryManager::getInstance()->setMainSceneState(2);

				mainScene->generalHeadLayer->setVisible(true);

				auto shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
				if (shortcutLayer)
				{
					shortcutLayer->setVisible(true);
					shortcutLayer->setMusouShortCutSlotPresent(false);
					//present three shortCurSlot
					shortcutLayer->presentShortCutSlot(3);
				}

				auto pJoystick = (Layer*)mainScene->getChildByTag(kTagVirtualJoystick);
				if (pJoystick)
				{
					pJoystick->setVisible(true);
				}

				//��ѧ���㼼�ܴ�,��ٽ�ѧ�佫��ս
				std::string scriptFileName = "script/nsKillMonsterAndGeneralFight.sc";
				ScriptManager::getInstance()->runScript(scriptFileName);

				setState(STATE_UPDATE);
			}	
		}
		break;
	case STATE_UPDATE:
		{
			for (std::map<long long,int>::iterator it_all = NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.begin() ; it_all != NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.end(); ++ it_all )
			{
				long long templateId_all  = it_all->first;
				int num_all = it_all->second;
				for (std::map<long long,int>::iterator it_cur = this->m_pTargetEmptyIdList.begin() ; it_cur != this->m_pTargetEmptyIdList.end(); ++ it_cur )
				{
					long long templateId_cur  = it_cur->first;
					int num_cur = it_cur->second;
					if (templateId_cur == templateId_all && num_all >= num_cur)
					{
						this->m_pTargetEmptyIdList.erase(it_cur);
						break;;
					}
				}
			}

			if (this->m_pTargetEmptyIdList.size() <= 0)
			{
				setState(STATE_END);
				break;
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}




////////////////////////////////////////////////////////////////////////////////////////////////

NSFight7::NSFight7(void)
{
}


NSFight7::~NSFight7(void)
{
}

void* NSFight7::createInstance()
{
	return new NSFight7() ;
}

void NSFight7::init( std::map<long long,int> targetEmptyIdList )
{
	isRunningAction = false;
	actionTime = 0.f;
	m_nDelayTime = 0.f;

	m_pTargetEmptyIdList.clear();
	std::map<long long,int>::iterator it = targetEmptyIdList.begin();
	for ( ; it != targetEmptyIdList.end(); ++ it )
	{
		long long templateId  = it->first;
		int num = it->second;
		m_pTargetEmptyIdList.insert(make_pair(templateId,num));
	}

	setState(STATE_INITED);
}

void NSFight7::update() {
	switch (mState) {
	case STATE_INITED:
		{
			GameView::getInstance()->myplayer->setVisible(true);
			//ˢ�¹��
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5107,(void *)4);
			//5���佫�����
			//GameMessageProcessor::sharedMsgProcessor()->sendReq(5105,(void *)2);

			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSFight7");

			auto mainScene = GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				NewCommerStoryManager::getInstance()->setMainSceneState(2);

				mainScene->generalHeadLayer->setVisible(true);

				auto shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
				if (shortcutLayer)
				{
					shortcutLayer->setVisible(true);
					shortcutLayer->setMusouShortCutSlotPresent(false);
					//present three shortCurSlot
					shortcutLayer->presentShortCutSlot(3);
				}

				auto pJoystick = (Layer*)mainScene->getChildByTag(kTagVirtualJoystick);
				if (pJoystick)
				{
					pJoystick->setVisible(true);
				}

				/*
				//��ѧ����ս�ڶ�������佫
				std::string scriptFileName = "script/nsKillMonsterAndGeneralallFight.sc";
				ScriptManager::getInstance()->runScript(scriptFileName);
				*/

				//set auto fight
				//set automatic skill
				MyPlayerAIConfig::setAutomaticSkill(1);
				//set star robot
				MyPlayerAIConfig::enableRobot(true);
				GameView::getInstance()->myplayer->getMyPlayerAI()->start();
				//��ѧ��ֻҪ�佫�ܳ�ս��ָ�
				if (!GameView::getInstance()->getMainUIScene())
					return;

				GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI*)GameView::getInstance()->getMainUIScene()->getGeneralInfoUI();
				if (generalsInfoUI != NULL)
					generalsInfoUI->removeCCTutorialIndicator();

				if (NewCommerStoryManager::getInstance()->IsNewComer())
				{
					NewCommerStoryManager::getInstance()->startAutoGuideToCallGeneral();
				}

				//�����Ĭ�Ϲ�
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5104);

				setState(STATE_UPDATE);
			}	
		}
		break;
	case STATE_UPDATE:
		{
			for (std::map<long long,int>::iterator it_all = NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.begin() ; it_all != NewCommerStoryManager::getInstance()->m_pDiedEmptyIdList.end(); ++ it_all )
			{
				long long templateId_all  = it_all->first;
				int num_all = it_all->second;
				for (std::map<long long,int>::iterator it_cur = this->m_pTargetEmptyIdList.begin() ; it_cur != this->m_pTargetEmptyIdList.end(); ++ it_cur )
				{
					long long templateId_cur  = it_cur->first;
					int num_cur = it_cur->second;
					if (templateId_cur == templateId_all && num_all >= num_cur)
					{
						this->m_pTargetEmptyIdList.erase(it_cur);
						break;;
					}
				}
			}

			if (this->m_pTargetEmptyIdList.size() <= 0)
			{
				if (NewCommerStoryManager::getInstance()->IsNewComer())
				{
					NewCommerStoryManager::getInstance()->endAutoGuideToCallGeneral();
				}

				if (!isRunningAction)
				{
					isRunningAction = true;
					GameSceneLayer* scene = GameView::getInstance()->getGameScene();
					ActionInterval * action =(ActionInterval *)Sequence::create(
						CallFunc::create(CC_CALLBACK_0(NSFight7::startSlowMotion,this)),
						CallFunc::create(CC_CALLBACK_0(NSFight7::addLightAnimation,this)),
						DelayTime::create(END_BATTLE_SLOWMOTION_DURATION),
						CallFunc::create(CC_CALLBACK_0(NSFight7::endSlowMotion, this)),
						NULL);
					scene->runAction(action);	
				}
				else
				{
					actionTime+=1.0f/60;
					if (actionTime > 2.0f)
					{
						actionFinishCallBack();
					}
				}

				break;
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}

void NSFight7::startSlowMotion()
{
	Director::getInstance()->getScheduler()->setTimeScale(0.2f);
}

void NSFight7::endSlowMotion()
{
	Director::getInstance()->getScheduler()->setTimeScale(1.0f);
}

void NSFight7::actionFinishCallBack()
{
	isRunningAction = false;
	setState(STATE_END);
}

void NSFight7::addLightAnimation()
{
	Size size = Director::getInstance()->getVisibleSize();
	std::string animFileName = "animation/texiao/renwutexiao/SHANGUANG/shanguang.anm";
	CCLegendAnimation *la_light = CCLegendAnimation::create(animFileName);
	if (la_light)
	{
		la_light->setPlayLoop(false);
		la_light->setReleaseWhenStop(true);
		la_light->setPlaySpeed(5.0f);
		la_light->setScale(0.85f);
		la_light->setPosition(Vec2(size.width/2,size.height/2));
		GameView::getInstance()->getMainAnimationScene()->addChild(la_light);
	}
}



