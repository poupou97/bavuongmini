#ifndef _NEWCOMMERSTORY_COMMAND_NSRUNMOVIESCRIPT_H_
#define _NEWCOMMERSTORY_COMMAND_NSRUNMOVIESCRIPT_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include <string>
#include <vector>
#include "../NewCommerStory.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace std;

/**
 * ���־���-ִ�о���ű��߼�
 * @author yangjun
 * @date 2014-9-12
 */
class NSRunMovieScript : public NewCommerStory
{
public:
	NSRunMovieScript(void);
	~NSRunMovieScript(void);

	static void* createInstance() ;
	void init(const char * scriptPath);

	virtual void update();

private:
	std::string m_sScriptPath;
};

#endif
