#include "NSTextDesAnm.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../AppMacros.h"
#include "../NewCommerStoryManager.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"

NSTextDesAnm1::NSTextDesAnm1(void)
{
}


NSTextDesAnm1::~NSTextDesAnm1(void)
{
}

void* NSTextDesAnm1::createInstance()
{
	return new NSTextDesAnm1() ;
}

void NSTextDesAnm1::init()
{
	setState(STATE_INITED);
}

void NSTextDesAnm1::update() {

	auto scene = Director::getInstance()->getRunningScene();
	
	switch (mState) {
	case STATE_INITED:
		{
			Director::getInstance()->setNextDeltaTimeZero(true);
			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSTextDesAnm");

			//����治��ʾ
		 	NewCommerStoryManager::getInstance()->setMainSceneState(2);

			Size winSize  = Director::getInstance()->getVisibleSize();
			auto textDesAnmUI = NSTextDesAnmUI1::create();
			textDesAnmUI->setIgnoreAnchorPointForPosition(false);
			textDesAnmUI->setAnchorPoint(Vec2(.5f,.5f));
			textDesAnmUI->setPosition(Vec2(winSize.width/2,winSize.height/2));
			textDesAnmUI->setTag(kTag_NewLayer);
			scene->addChild(textDesAnmUI,10000);

			setState(STATE_UPDATE);
		}
		break;
	case STATE_UPDATE:
		{
			if (scene->getChildByTag(kTag_NewLayer) == NULL)
			{
				GameView::getInstance()->myplayer->setVisible(false);
				setState(STATE_END);
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}



//////////////////////////////////

NSTextDesAnmUI1::NSTextDesAnmUI1( void )
{

}

NSTextDesAnmUI1::~NSTextDesAnmUI1( void )
{

}

NSTextDesAnmUI1 * NSTextDesAnmUI1::create()
{
	auto ui =new NSTextDesAnmUI1();
	if (ui && ui->init())
	{
		ui->autorelease();
		return ui;
	}
	CC_SAFE_DELETE(ui);
	return NULL;
}

bool NSTextDesAnmUI1::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		auto mengban = ImageView::create();
		mengban->loadTexture("newcommerstory/zhezhao.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winSize);
		mengban->setAnchorPoint(Vec2(0.5f,0.5f));
		mengban->setPosition(Vec2(winSize.width/2,winSize.height/2));
		m_pLayer->addChild(mengban);

		auto sq = Sequence::create(
			DelayTime::create(0.2f),
			CallFunc::create(CC_CALLBACK_0(NSTextDesAnmUI1::createUI, this)),
			NULL);
		this->runAction(sq);

		////this->setTouchEnabled(true);
		this->setContentSize(winSize);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}

void NSTextDesAnmUI1::onEnter()
{
	UIScene::onEnter();
}

void NSTextDesAnmUI1::onExit()
{
	UIScene::onExit();
}

bool NSTextDesAnmUI1::onTouchBegan( Touch *touch, Event * pEvent )
{
	return true;
}

void NSTextDesAnmUI1::onTouchEnded( Touch *touch, Event * pEvent )
{

}

void NSTextDesAnmUI1::onTouchCancelled( Touch *touch, Event * pEvent )
{

}

void NSTextDesAnmUI1::onTouchMoved( Touch *touch, Event * pEvent )
{

}

void NSTextDesAnmUI1::createUI()
{
	Size winSize = Director::getInstance()->getVisibleSize();

//  	Label * l_des1 = Label::create();
//  	l_des1->setFontName(APP_FONT_NAME);
//  	l_des1->setFontSize(30);
//  	//l_des1->setColor(Color3B(99,132,18));
//  	l_des1->setText(StringDataManager::getString("NewComerStory_TextAnm_des1"));
//  	l_des1->setAnchorPoint(Vec2(.5f,.5f));
//  	l_des1->setPosition(Vec2(winSize.width/2,winSize.height/2 + 60));
//  	m_pLayer->addChild(l_des1);
//  	l_des1->setVisible(false);
	auto imageView_des1 = ImageView::create();
	imageView_des1->loadTexture("newcommerstory/1.png");
	imageView_des1->setAnchorPoint(Vec2(.5f,.5f));
	imageView_des1->setPosition(Vec2(winSize.width/2,winSize.height/2 + 60));
	m_pLayer->addChild(imageView_des1);
	imageView_des1->setVisible(false);
 	 
//  	Label * l_des2 = Label::create();
//  	l_des2->setFontName(APP_FONT_NAME);
//  	l_des2->setFontSize(30);
//  	//l_des2->setColor(Color3B(99,132,18));
//  	l_des2->setText(StringDataManager::getString("NewComerStory_TextAnm_des2"));
//  	l_des2->setAnchorPoint(Vec2(.5f,.5f));
//  	l_des2->setPosition(Vec2(winSize.width/2,winSize.height/2));
//  	m_pLayer->addChild(l_des2);
//  	l_des2->setVisible(false);
	auto imageView_des2 = ImageView::create();
	imageView_des2->loadTexture("newcommerstory/2.png");
	imageView_des2->setAnchorPoint(Vec2(.5f,.5f));
	imageView_des2->setPosition(Vec2(winSize.width/2,winSize.height/2));
	m_pLayer->addChild(imageView_des2);
	imageView_des2->setVisible(false);
 	 
//  	Label * l_des3 = Label::create();
//  	l_des3->setFontName(APP_FONT_NAME);
//  	l_des3->setFontSize(30);
//  	//l_des3->setColor(Color3B(99,132,18));
//  	l_des3->setText(StringDataManager::getString("NewComerStory_TextAnm_des3"));
//  	l_des3->setAnchorPoint(Vec2(.5f,.5f));
//  	l_des3->setPosition(Vec2(winSize.width/2,winSize.height/2 - 60));
//  	m_pLayer->addChild(l_des3);
//  	l_des3->setVisible(false);
	auto imageView_des3 = ImageView::create();
	imageView_des3->loadTexture("newcommerstory/3.png");
	imageView_des3->setAnchorPoint(Vec2(.5f,.5f));
	imageView_des3->setPosition(Vec2(winSize.width/2,winSize.height/2-60));
	m_pLayer->addChild(imageView_des3);
	imageView_des3->setVisible(false);

	auto imageView_des4 = ImageView::create();
	imageView_des4->loadTexture("newcommerstory/open.png");
	imageView_des4->setAnchorPoint(Vec2(.5f,.5f));
	imageView_des4->setPosition(Vec2(winSize.width/2,winSize.height/2));
	m_pLayer->addChild(imageView_des4);
	imageView_des4->setVisible(false);
 	 
 	auto action_l_des1 = Sequence::create(
 	 	DelayTime::create(1.0f),
 	 	Show::create(),
 	 	FadeIn::create(0.8f),
 	 	DelayTime::create(4.0f),
 	 	FadeOut::create(0.8f),
 	 	RemoveSelf::create(),
 	 	NULL
 	 	);
 	imageView_des1->runAction(action_l_des1);
 	 
 	auto action_l_des2 = Sequence::create(
 	 	DelayTime::create(2.8f),
 	 	Show::create(),
 	 	FadeIn::create(0.8f),
 	 	DelayTime::create(3.0f),
 	 	FadeOut::create(0.8f),
 	 	RemoveSelf::create(),
 	 	NULL
 	 	);
 	imageView_des2->runAction(action_l_des2);
 	 
 	auto action_l_des3 = Sequence::create(
 	 	DelayTime::create(4.6f),
 	 	Show::create(),
 	 	FadeIn::create(0.8f),
 	 	DelayTime::create(2.0f),
 	 	FadeOut::create(0.8f),
 	 	RemoveSelf::create(),
 	 	NULL
 	 	);
 	imageView_des3->runAction(action_l_des3);

	auto action_l_des4 = Sequence::create(
		DelayTime::create(8.5f),
		Show::create(),
		FadeIn::create(0.8f),
		DelayTime::create(2.0f),
		FadeOut::create(1.3f),
		RemoveSelf::create(),
		NULL
		);
	imageView_des4->runAction(action_l_des4);

	auto action_this = Sequence::create(
		DelayTime::create(13.f),
		RemoveSelf::create(),
		NULL
		);
	this->runAction(action_this);

	//addEffect
	float firstPosX = 200*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH);
	float secondPosX = 400*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH);
	float thirdPosX = 600*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH);
	std::string effectPathName = "animation/texiao/particledesigner/huo2.plist";
	// effect animation
	auto pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	pParticleEffect->setPosition(Vec2(firstPosX,10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	pParticleEffect->setPosition(Vec2(secondPosX,10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	pParticleEffect->setPosition(Vec2(thirdPosX,10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	 
	pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	pParticleEffect->setPosition(Vec2(firstPosX,10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	pParticleEffect->setPosition(Vec2(secondPosX,10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	pParticleEffect->setPosition(Vec2(thirdPosX,10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	 
	pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	pParticleEffect->setRotation(180);
	pParticleEffect->setPosition(Vec2(firstPosX,winSize.height-10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	pParticleEffect->setRotation(180);
	pParticleEffect->setPosition(Vec2(secondPosX,winSize.height-10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	pParticleEffect->setRotation(180);
	pParticleEffect->setPosition(Vec2(thirdPosX,winSize.height-10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	 
	pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	pParticleEffect->setRotation(180);
	pParticleEffect->setPosition(Vec2(firstPosX,winSize.height-10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	pParticleEffect->setRotation(180);
	pParticleEffect->setPosition(Vec2(secondPosX,winSize.height-10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);
	pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
	pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	pParticleEffect->setRotation(180);
	pParticleEffect->setPosition(Vec2(thirdPosX,winSize.height-10));
	pParticleEffect->setScaleX(1.0f);
	this->addChild(pParticleEffect);

	for (int i = 0;i<3;i++)
	{
		pParticleEffect = ParticleSystemQuad::create("animation/texiao/particledesigner/huomiao1.plist");
		pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
		pParticleEffect->setPosition(Vec2(winSize.width/4*(i+1),winSize.height/2));
		pParticleEffect->setScaleX(1.5f);
		this->addChild(pParticleEffect);
	}
}
