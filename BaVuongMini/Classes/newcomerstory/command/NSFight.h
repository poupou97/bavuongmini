#ifndef _NEWCOMMERSTORY_COMMAND_NSFIGHT_H_
#define _NEWCOMMERSTORY_COMMAND_NSFIGHT_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include <string>
#include <vector>
#include "../NewCommerStory.h"

USING_NS_CC;
USING_NS_CC_EXT;

using namespace std;

/**
 * ���־��-�ս���߼�
 * @author yangjun
 * @date 2014-9-12
 */

////////////////////////////////////////////////////////////////////////////////////////////////////
/**************��Ҳ���1֡����ƶ�+���弼�ܽ�ѧ����һ�δ�************/
class NSFight1 : public NewCommerStory
{
public:
	NSFight1(void);
	~NSFight1(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/**************���Ҳ���3֡�����ħ�ǡ�������Ůս��************/
class NSFight2 : public NewCommerStory
{
public:
	NSFight2(void);
	~NSFight2(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
	float m_nDelayTime;
};


////////////////////////////////////////////////////////////////////////////////////////////////////
/**************��Ҳ���4֡���ʩ����꼼�************/
class NSFight3 : public NewCommerStory
{
public:
	NSFight3(void);
	~NSFight3(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
	float m_nDelayTime;
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/**************���Ҳ���1֡���Ⱥ�弼�ܽ�ѧ����************/
class NSFight4 : public NewCommerStory
{
public:
	NSFight4(void);
	~NSFight4(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
};




////////////////////////////////////////////////////////////////////////////////////////////////////
/**************���Ҳ���֡�ɱ��һ���ƽ�************/
class NSFight5 : public NewCommerStory
{
public:
	NSFight5(void);
	~NSFight5(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
	float m_nDelayTime;
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/**************���Ҳ���֡����佫���󣬳�ս************/
class NSFight6 : public NewCommerStory
{
public:
	NSFight6(void);
	~NSFight6(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
	float m_nDelayTime;
};

////////////////////////////////////////////////////////////////////////////////////////////////////
/**************��Ҳ���֡������佫���󣬽�ѧ��ս�ڶ���佫��������佫************/
class NSFight7 : public NewCommerStory,public Node
{
public:
	NSFight7(void);
	~NSFight7(void);

	static void* createInstance() ;
	void init(std::map<long long,int> targetEmptyIdList);

	virtual void update();

private:
	std::map<long long,int> m_pTargetEmptyIdList;
	float m_nDelayTime;

	bool isRunningAction;
	float actionTime;
	void startSlowMotion();
	void endSlowMotion();
	void actionFinishCallBack();

	void addLightAnimation();
};
#endif
