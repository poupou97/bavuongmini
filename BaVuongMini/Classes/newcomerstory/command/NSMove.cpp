#include "NSMove.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../legend_script/ScriptManager.h"
#include "../NewCommerStoryManager.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "../../gamescene_state/role/SimpleActor.h"

NSMove::NSMove(void)
{
}


NSMove::~NSMove(void)
{
}

void* NSMove::createInstance()
{
	return new NSMove() ;
}

void NSMove::init()
{
	setState(STATE_INITED);
}

void NSMove::update() {
	switch (mState) {
	case STATE_INITED:
		{
			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSMove");

			//�Ѵ�����������Ϊ�ɴ��
			GameView::getInstance()->getGameScene()->setTiledMapLimit(17,17,1);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(16,18,1);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(17,18,1);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(18,18,1);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(16,19,1);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(17,19,1);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(18,19,1);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(17,20,1);

			//�����ϳ���ָ���ͷ
			//pos
			Vec2 Pos[3] ;
// 			Pos[0].x = 191;
// 			Pos[0].y = 1024-211;
// 			Pos[1].x = 351;
// 			Pos[1].y = 1024-386;
// 			Pos[2].x = 363;
// 			Pos[2].y = 1024-630;
// 			Pos[3].x = 540;
// 			Pos[3].y = 1024-779;
// 			Pos[4].x = 839;
// 			Pos[4].y = 1024-803;
// 			Pos[5].x = 1019;
// 			Pos[5].y = 1024-691;
			Pos[0].x = 540;
			Pos[0].y = 1024-779;
			Pos[1].x = 839;
			Pos[1].y = 1024-783;
			Pos[2].x = 1019;
			Pos[2].y = 1024-691;
			//rotation
			int rota[3];
// 			rota[0] = 5;
// 			rota[1] = 4;
// 			rota[2] = 5;
// 			rota[3] = 5;
// 			rota[4] = 7;
// 			rota[5] = 7;
			rota[0] = 5;
			rota[1] = 7;
			rota[2] = 7;
			for (int i = 0;i<3;i++)
			{
				auto scene = GameView::getInstance()->getGameScene();
				std::string animFileName = "animation/texiao/jiemiantexiao/xunlu/xunlu2.anm";
				auto simpleActor = new SimpleActor();
				simpleActor->setGameScene(scene);
				//simpleActor->setWorldPosition(Vec2(608,1632));
				simpleActor->setPosition(Pos[i]);
				simpleActor->setLayerId(0);   // ground layer
				simpleActor->loadAnim(animFileName.c_str(),true);
				//simpleActor->setRotation(rota[i]);
				simpleActor->getLegendAnim()->setAction(rota[i]);
				auto actorLayer = scene->getActorLayer();
				actorLayer->addChild(simpleActor,-1);
				simpleActor->release();
			}
			

			//��ѧ���ƶ�
			std::string scriptFileName = "script/nsMove.sc";
			ScriptManager::getInstance()->runScript(scriptFileName);

			setState(STATE_UPDATE);
		}
		break;
	case STATE_UPDATE:
		{
			//����Ƿ��͵�����ħ��̳
			GameView* gv = GameView::getInstance();
			if (gv->getMapInfo()->mapid() == "smjt.level")
			{
				gv->myplayer->setVisible(false);
				//��������ȫ���佫
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5105,(void *)2);
				setState(STATE_END);
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}

////////////////////////////////////////////////////////////////////////////

NSMoveToStairs::NSMoveToStairs(void)
{
}


NSMoveToStairs::~NSMoveToStairs(void)
{
}

void* NSMoveToStairs::createInstance()
{
	return new NSMoveToStairs() ;
}

void NSMoveToStairs::init()
{
	setState(STATE_INITED);
}

void NSMoveToStairs::update() {
	switch (mState) {
	case STATE_INITED:
		{
			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSMoveToStairs");

			//�Ѵ�����������Ϊ���ɴ��
			GameView::getInstance()->getGameScene()->setTiledMapLimit(17,17,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(16,18,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(17,18,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(18,18,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(16,19,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(17,19,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(18,19,0);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(17,20,0);

			//��м�¥�ݴ���Ϊ�ɴ��
			GameView::getInstance()->getGameScene()->setTiledMapLimit(4,20,1);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(4,21,1);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(5,19,1);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(5,20,1);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(6,18,1);
			GameView::getInstance()->getGameScene()->setTiledMapLimit(6,19,1);
			//�ȥդ�
			auto scene = GameView::getInstance()->getGameScene();
			auto actorLayer = scene->getActorLayer();
			if(actorLayer->getChildByTag(600))
			{
				actorLayer->getChildByTag(600)->removeFromParent();
			}

			//�����ϳ���ָ���ͷ
			//pos
			Vec2 Pos[4] ;
			Pos[0].x = 191;
			Pos[0].y = 1024-211;
			Pos[1].x = 351;
			Pos[1].y = 1024-386;
			Pos[2].x = 363;
			Pos[2].y = 1024-630;
			Pos[3].x = 540;
			Pos[3].y = 1024-779;
			//rotation
			int rota[4];
			rota[0] = 5;
			rota[1] = 4;
			rota[2] = 5;
			rota[3] = 5;
			for (int i = 0;i<4;i++)
			{
				auto scene = GameView::getInstance()->getGameScene();
				std::string animFileName = "animation/texiao/jiemiantexiao/xunlu/xunlu2.anm";
				auto simpleActor = new SimpleActor();
				simpleActor->setGameScene(scene);
				//simpleActor->setWorldPosition(Vec2(608,1632));
				simpleActor->setPosition(Pos[i]);
				simpleActor->setLayerId(0);   // ground layer
				simpleActor->loadAnim(animFileName.c_str(),true);
				//simpleActor->setRotation(rota[i]);
				simpleActor->getLegendAnim()->setAction(rota[i]);
				auto actorLayer = scene->getActorLayer();
				actorLayer->addChild(simpleActor,-1,300+i);
				simpleActor->release();
			}


			//��ѧ���ƶ�
// 			std::string scriptFileName = "script/nsMove.sc";
// 			ScriptManager::getInstance()->runScript(scriptFileName);

			setState(STATE_UPDATE);
		}
		break;
	case STATE_UPDATE:
		{
			//����Ƿ����¥�
			if(GameView::getInstance()->myplayer->getWorldPosition().x>448 && GameView::getInstance()->myplayer->getWorldPosition().y>(1984-GameView::getInstance()->myplayer->getWorldPosition().x*2+576))
			{
				//�ȥ��ָ���ͷ
				for (int i = 0;i<7;i++)
				{
					auto scene = GameView::getInstance()->getGameScene();
					auto actorLayer = scene->getActorLayer();
					if (actorLayer->getChildByTag(300+i))
					{
						actorLayer->getChildByTag(300+i)->removeFromParent();
					}
				}
				//�м�¥�ݴ���Ϊ���ɴ��
				GameView::getInstance()->getGameScene()->setTiledMapLimit(4,20,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(4,21,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(5,19,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(5,20,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(6,18,0);
				GameView::getInstance()->getGameScene()->setTiledMapLimit(6,19,0);

				//��դ�
				auto scene = GameView::getInstance()->getGameScene();
				std::string animFileName = "animation/texiao/changjingtexiao/ZHAMEN/zhamen2.anm";
				auto simpleActor = new SimpleActor();
				simpleActor->setGameScene(scene);
				simpleActor->setPosition(Vec2(343,1024-650));
				simpleActor->setLayerId(0);   // ground layer
				simpleActor->loadAnim(animFileName.c_str(),true);
				auto actorLayer = scene->getActorLayer();
				actorLayer->addChild(simpleActor,-1,600);
				simpleActor->release();

				setState(STATE_END);
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}

