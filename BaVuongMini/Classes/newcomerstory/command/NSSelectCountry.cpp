#include "NSSelectCountry.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../AppMacros.h"
#include "../../gamescene_state/MainScene.h"
#include "../../legend_script/ScriptManager.h"
#include "../NewCommerStoryManager.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../ui/country_ui/CountryUI.h"
#include "../../messageclient/GameMessageProcessor.h"

NSSelectCountry::NSSelectCountry(void)
{
}


NSSelectCountry::~NSSelectCountry(void)
{
}

void* NSSelectCountry::createInstance()
{
	return new NSSelectCountry() ;
}

void NSSelectCountry::init()
{
	setState(STATE_INITED);
}

void NSSelectCountry::update() {

	auto scene = Director::getInstance()->getRunningScene();

	switch (mState) {
	case STATE_INITED:
		{
			setState(STATE_START);
		}
	case STATE_START:
		{
			CCLOG("start NSSelectCountry");

			//��ѡ���ҽ��
			Size winSize  = Director::getInstance()->getVisibleSize();
			CountryUI * countryUI = CountryUI::create();
			countryUI->setIgnoreAnchorPointForPosition(false);
			countryUI->setAnchorPoint(Vec2(.5f,.5f));
			countryUI->setPosition(Vec2(winSize.width/2,winSize.height/2));
			countryUI->setTag(kTag_NewLayer);
			scene->addChild(countryUI,10000);

			setState(STATE_UPDATE);
		}
		break;
	case STATE_UPDATE:
		{
			//����Ƿ������ѡ���
			if (NewCommerStoryManager::getInstance()->IsSelectCountryFinished())
			{
				//close selectCountryUI
				if (scene->getChildByTag(kTag_NewLayer))
				{
					scene->getChildByTag(kTag_NewLayer)->removeFromParent();
				}
				//set state
				setState(STATE_END);
			}
		}
		break;
	case STATE_END:
		break;
	default:
		CCLOG("please init data");
		break;
	}
}

