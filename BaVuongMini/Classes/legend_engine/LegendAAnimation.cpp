#include "LegendAAnimation.h"

#include "CCBinaryFileReader.h"
#include "../utils/GameUtils.h"

AModule::AModule()
{
}

AModule::~AModule()
{
}

AFrame::AFrame()
: aModules(NULL)
, frameDelay(0)
, amoduleNum(0)
{
}

AFrame::~AFrame()
{
	for(int i = 0; i < amoduleNum; i++)   
		CC_SAFE_DELETE(aModules[i]);  
	CC_SAFE_DELETE_ARRAY(aModules);
}

AAction::AAction()
: aFrames(NULL)
, aframeNum(0)
{
}

AAction::~AAction()
{
	for(int i = 0; i < aframeNum; i++)   
		CC_SAFE_DELETE(aFrames[i]);  
	CC_SAFE_DELETE_ARRAY(aFrames);
}

AAnimation::AAnimation()
: m_imageNum(0)
, m_moduleData(NULL)
, m_pModuleNum(NULL)
, m_actionsNum(0)
, m_actions(NULL)
{

}

AAnimation::~AAnimation()
{
	// delete array
	for(int i = 0; i < m_imageNum; i++)   
		CC_SAFE_DELETE_ARRAY(m_moduleData[i]);  
	CC_SAFE_DELETE_ARRAY(m_moduleData);

	CC_SAFE_DELETE_ARRAY(m_pModuleNum);

	for(int i = 0; i < m_actionsNum; i++)   
		CC_SAFE_DELETE(m_actions[i]);  
	CC_SAFE_DELETE_ARRAY(m_actions);
}

void AAnimation::loadAnimationData(const char* anmFile)
{
	//std::string anmPath = anmFile;
	m_anmFileName = anmFile;
	CCAssert(m_anmFileName.size() > 0, "anmFile is empty");

	CCBinaryFileReader* reader = new CCBinaryFileReader(anmFile);

	// the animation's image number
	m_imageNum = reader->readByte();

	CCAssert(m_imageNum <= MAX_AIMAGE_NUM, "there are too many images in this animation!");

	// load module data
	m_moduleData = new short*[m_imageNum];
	m_pModuleNum = new int[m_imageNum];
	for(int i = 0; i < m_imageNum; i++)
	{
		__String * name = reader->readString();
		this->m_strImageFilenames[i] = name->getCString();
		this->m_strImageFilenames[i] = GameUtils::getFullPathFilename(m_strImageFilenames[i], m_anmFileName);
		
		short moduleNum = reader->readByte();
		m_pModuleNum[i] = moduleNum;
		this->m_moduleData[i] = new short[moduleNum<<2];
		for(int j = 0, k = 0; j < moduleNum; j++)
		{
			this->m_moduleData[i][k++] = reader->readShort();   // offsetx
			this->m_moduleData[i][k++] = reader->readShort();   // offsety
			this->m_moduleData[i][k++] = reader->readShort();   // width
			this->m_moduleData[i][k++] = reader->readShort();   // height
		}
	}

	// load actions
	unsigned char actCnt = reader->readByte();
	m_actionsNum = actCnt;
	m_actions = new AAction*[actCnt];
	for(int i = 0; i < actCnt; i++)
	{
		m_actions[i] = new AAction();
		m_actions[i]->animTickCnt = 0; // init AAction's animTickCnt

		// load AFrame
		unsigned char frameCnt = reader->readByte();
		m_actions[i]->aframeNum = frameCnt;
		m_actions[i]->aFrames = new AFrame*[frameCnt];
		for(int j = 0; j < frameCnt; j++)
		{
			unsigned char aModCnt = reader->readByte();
			unsigned char frameDelay = reader->readByte(); 
			
			auto frm = new AFrame();
			frm->frameDelay = frameDelay;
			m_actions[i]->animTickCnt += frameDelay;   // udpate AAction.animTickCnt
			frm->amoduleNum = aModCnt;
			
			// load AModule
			frm->aModules = new AModule*[aModCnt]; 
			for(int k = 0; k < aModCnt; k++)
			{
				auto amod = new AModule();
				
				amod->moduleID = reader->readByte();   // module id
				amod->imageID = reader->readByte();   // image id

				// transform info
				float m00 = reader->readFloat();
				float m01 = reader->readFloat();
				float m02 = reader->readFloat();
				float m10 = reader->readFloat();
				float m11 = reader->readFloat();
				float m12 = reader->readFloat();
				amod->affineTrans = AffineTransformMake(m00, m10, m01, m11, 
															m02, m12);

				//amod.initSurroundingRect();

				//amod->colorValue = (int)reader->readByte();   // alpha
				amod->colorValue = reader->readInt(false);   // argb color

				frm->aModules[k] = amod;
			}
			//frm.initSurroundingRect();
			
			m_actions[i]->aFrames[j] = frm;
		}
		//m_actions[i].initSurroundingRect();
	}

	CC_SAFE_DELETE(reader);
}

std::string AAnimation::getAnmFileName()
{
	return m_anmFileName;
}

int AAnimation::getModuleNum(int imgIdx)
{
	return this->m_pModuleNum[imgIdx];
}

std::string AAnimation::getImagePath(int imgIdx)
{
	CCAssert(imgIdx < MAX_AIMAGE_NUM, "out of range");

	std::string fullFileName = m_strImageFilenames[imgIdx];
	CCAssert(fullFileName.size() > 0, "no file name");

	std::string path;
	int lastSlashIndex = fullFileName.find_last_of("/\\");
	if(lastSlashIndex == -1) {   // not found
		path = "";
	}
	else {
		path = fullFileName.substr(0, lastSlashIndex+1);
	}

	return path;
}
std::string AAnimation::getImageFileName(int imgIdx)
{
	return this->m_strImageFilenames[imgIdx];
}

/**
 * get the number of AAction in an AAnimation
 * @return
 */
int AAnimation::getAActionNum()
{
	return m_actionsNum;
}

int AAnimation::getAnimTickNum(int actIdx)
{
	if(actIdx < 0 || actIdx >= getAActionNum())
	{
		return -1;
	}
	if(m_actions[actIdx] == NULL)
		return 0;

	return m_actions[actIdx]->animTickCnt;
}

/**
 * 获取特定动作中所有的帧数
 * @param actIdx
 * @return
 */
int AAnimation::getActionFrameNum(int actIdx)
{
	if(actIdx < 0 || actIdx >= getAActionNum())
	{
		return -1;
	}
	if(m_actions[actIdx] == NULL)
		return 0;
	return m_actions[actIdx]->aframeNum;
}

/**
 * 获取特定动作中特定帧的时间间隔
 * @param actIdx
 * @param frameIdx
 * @return
 */
int AAnimation::getAFrameDelay(int actIdx, int frameIdx)
{
	if(actIdx < 0 || actIdx >= getAActionNum())
		return -1;

	if(frameIdx < 0 || frameIdx >= getActionFrameNum(actIdx))
		return -1;

	if(m_actions[actIdx] == NULL || m_actions[actIdx]->aFrames[frameIdx] == NULL )
		return 0;
	return m_actions[actIdx]->aFrames[frameIdx]->frameDelay; 
}

/**
 * 根据animTick获取特定动作中，相应的aframeIdx
 * @param animTick
 * @return
 */
int AAnimation::getAFrameIdxByAnimTick(int actIdx, int animTick)
{
	if(actIdx < 0 || actIdx >= getAActionNum())
	{
		return -1;
	}
	if(m_actions[actIdx] == NULL)
		return -1;

	if(animTick >= m_actions[actIdx]->animTickCnt)
		return getActionFrameNum(actIdx);
	if(animTick < 0)
		return 0;

	int aframeIdx = 0;
	int delayCounter = 0;
	int tmpAnimTick = 0;
	while(tmpAnimTick < animTick)
	{
		int delay = getAFrameDelay(actIdx, aframeIdx);
		if(delayCounter < delay - 1)
		{
			 delayCounter++;
		}
		else
		{
			aframeIdx++;
			delayCounter = 0;
		}

		tmpAnimTick++;
	}

	return aframeIdx;
}
