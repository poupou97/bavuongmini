
#include "GameWorld.h"

#include "../legend_engine/CCBinaryFileReader.h"
#include "../messageclient/element/DoorInfo.h"
#include "../messageclient/element/MapInfo.h"
#include "../messageclient/element/CNpcInfo.h"
#include "../messageclient/element/CPickingInfo.h"
#include "../utils/GraphSTDijkstra/GraphSTDijkstra.h"
#include "../utils/GraphSTDijkstra/NextAdjNode.h"

std::map<std::string, int> GameWorld::doorIndex;
std::map<int, std::string> GameWorld::indexdoor;
std::map<std::string, std::vector<std::string>*> GameWorld::maps;
std::map<std::string, MapInfo*> GameWorld::MapInfos;
std::map<long long, CNpcInfo*> GameWorld::NpcInfos;
std::map<int, CPickingInfo*> GameWorld::PickingInfos;

GraphSTDijkstra* GameWorld::sLevelGraph = NULL;

void GameWorld::loadWorldConfigure()
{
	auto reader = new CCBinaryFileReader("level/configure");

	// 地图数量
	int size = reader->readShort();
	for (int i = 0; i < size; i++) {
		MapInfo * mapInfo = new MapInfo();
		mapInfo->mapId = reader->readString()->getCString();
		mapInfo->name = reader->readString()->getCString();
				
		std::vector<std::string>* desMapList = new std::vector<std::string>();
		doorIndex[mapInfo->mapId] = i;
		maps[mapInfo->mapId] = desMapList;
		indexdoor[i] = mapInfo->mapId;
				
		int doorSize = reader->readByte();
		//mapInfo.doors = new std::list<DoorInfo*>();
		for (int j = 0; j < doorSize; j++) {
			auto doorInfo = new DoorInfo();
			doorInfo->mapId = reader->readString()->getCString();
			int SizeXy = reader->readInt(true);
			for (int k = 0; k < SizeXy; k++) {
				short* xy = new short[2];
				xy[0] = reader->readShort();
				xy[1] = reader->readShort();
				doorInfo->doorXY.push_back(xy);
			}
			//mapInfo.doors[j] = doorInfo;
			mapInfo->doors.push_back(doorInfo);
			desMapList->push_back(doorInfo->mapId);

			MapInfos[mapInfo->mapId] = mapInfo;
		}
	}

	// Npc
	size = reader->readShort();
	for (int i = 0; i < size; i++) {
		auto npcInfo = new CNpcInfo();
		npcInfo->npcId = reader->readInt(true);
		npcInfo->npcName = reader->readString()->getCString();
		npcInfo->normal = reader->readByte();
		npcInfo->intro = reader->readString()->getCString();
		npcInfo->icon = reader->readString()->getCString();
		npcInfo->welcome = reader->readString()->getCString();
		npcInfo->des = reader->readString()->getCString();
		npcInfo->byteValue = reader->readByte();
		npcInfo->mapIcon = reader->readString()->getCString();

		NpcInfos[npcInfo->npcId] = npcInfo;
	}

	// 采集物
	size = reader->readShort();
	for (int i = 0; i < size; i++) {
		auto pickingInfo = new CPickingInfo();
		pickingInfo->id = reader->readInt(true);
		pickingInfo->name = reader->readString()->getCString();
		pickingInfo->icon = reader->readString()->getCString();
		pickingInfo->avatar = reader->readString()->getCString();
		pickingInfo->description = reader->readString()->getCString();
		pickingInfo->actionTime = reader->readInt(true);
		pickingInfo->actionCdTime = reader->readInt(true);
		pickingInfo->actionMaxNumber = reader->readInt(true);
		pickingInfo->field = reader->readBool();
		pickingInfo->requirement = reader->readString()->getCString();

		PickingInfos[pickingInfo->id] = pickingInfo;
	}

	CC_SAFE_DELETE(reader);

	// debug
	//for(int i=0; i < (int)maps.size(); i++){
	//	CCLOG("map %d,    %s", i, indexdoor.at(i).c_str());
	//}

	// 根据Level configure的信息，构建图
	int totalNodeNum = maps.size();
	//GraphAdjList** graphAdjList= new GraphAdjList*[totalNodeNum];
	std::vector<GraphAdjList*>* graphAdjList = new std::vector<GraphAdjList*>();
	graphAdjList->resize(totalNodeNum);

	for(int i=0; i<totalNodeNum; i++){
		//后继节点
		NextAdjNode * nextTemp = NULL;
		NextAdjNode * lastAdjNode = NULL;
			
		std::string mapid = indexdoor[i];
		std::vector<std::string>* doors = maps[mapid];
		if(doors->size() == 0){
			graphAdjList->at(i) = new GraphAdjList(NULL);
		}else{
			for(int j=0; j < (int)doors->size(); j++){
				// door所指向的结点并不存在，错误的图数据
				//if(doorIndex.at(doors->at(j)) == NULL) {
				if(doorIndex[doors->at(j)] == -1) {
					continue;
				}
				nextTemp = new NextAdjNode(doorIndex[doors->at(j)],1);
				if(graphAdjList->at(i) == NULL) {
					graphAdjList->at(i) = new GraphAdjList(nextTemp);
				}else{
					lastAdjNode->nextNode = nextTemp;
				}
				lastAdjNode = nextTemp;
			}
		}
	}
		
	//for(int i=0; i<totalNodeNum; i++){
	//	CCLOG("map %d     %s", i, indexdoor.at(i).c_str());
	//}
		
	sLevelGraph = new GraphSTDijkstra();
	sLevelGraph->setGraph(graphAdjList);

	// test code
	//std::string searchFrom = "gd.level";
	//std::string searchTo = "cac.level";
	////起始点
	//int from  = (int)doorIndex.at(searchFrom);
	////目标点
	//int to = (int)doorIndex.at(searchTo);
	//bool cango = sLevelGraph->searchPath(from, to);
	//if(!cango){
	//	CCLOG("can not reach");
	//}else{
	//	std::vector<int>* doorpath = sLevelGraph->getPath(from, to);
	//	for(int i=doorpath->size()-1; i>=0; i--){
	//		CCLOG("%d", doorpath->at(i));
	//		CCLOG("name: %s", indexdoor.at(doorpath->at(i)).c_str());
	//	}
	//	delete doorpath;
	//}

	//std::string searchFrom = "gd.level";
	//std::string searchTo = "xsc.level";
	//std::vector<std::string>* paths = searchLevelPath(searchFrom, searchTo);
}

std::vector<std::string>* GameWorld::searchLevelPath(std::string from, std::string to) {		
	//起始点
	CCAssert(doorIndex.find(from) != doorIndex.end(), "map can not be found");
	int fromIdx  = (int)doorIndex[from];

	//目标点
	CCAssert(doorIndex.find(to) != doorIndex.end(), "map can not be found");
	int toIdx = (int)doorIndex[to];

	bool cango = sLevelGraph->searchPath(fromIdx, toIdx);
	if(!cango){
		return NULL;
	}else{
		std::vector<std::string>* searchResult = new std::vector<std::string>();
		std::vector<int>* doorpath = sLevelGraph->getPath(fromIdx, toIdx);
		for(int i=doorpath->size()-1; i>=0; i--){
			searchResult->push_back(indexdoor.at(doorpath->at(i)));
		}
		return searchResult;
	}
}