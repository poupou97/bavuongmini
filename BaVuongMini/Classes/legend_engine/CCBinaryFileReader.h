#ifndef _LEGEND_CCBINARYFILEREADER_H_
#define _LEGEND_CCBINARYFILEREADER_H_

#include "cocos2d.h"

USING_NS_CC;

class CCBinaryFileReader
{
public:
	CCBinaryFileReader(const char *binFile);
	virtual ~CCBinaryFileReader(void);

    /* Parse methods. */
    int readInt(bool pSigned);
	short readShort();
    unsigned char readByte();
    bool readBool();
    float readFloat();
	__String * readString();

private:
	unsigned char * mBytes;
    int mCurrentByte;
	int mCurrentBit;

private:
    bool getBit();
    void alignBits();
};

#endif