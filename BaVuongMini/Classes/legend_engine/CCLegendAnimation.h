
#ifndef __LEGEND_ANIMATION_CCLEGENDANIMATION_H__
#define __LEGEND_ANIMATION_CCLEGENDANIMATION_H__

#include "cocos2d.h"

#define CCLEGENDANIMATION_SPECIAL_AMODULE_BASE_ZORDER 100

class AAnimation;

USING_NS_CC;
using namespace std;

class CCLegendAModule : public Sprite
{
public:
    CCLegendAModule();
    virtual ~CCLegendAModule();
    virtual bool isDirty(void);
    //virtual AffineTransform nodeToParentTransform(void);
	virtual Mat4& getNodeToParentTransform();

	void setTransform(float a, float b, float c, float d, float tx, float ty);
	void setTransform(AffineTransform trans);

	void setModuleRect(int x, int y, int w, int h);
	void setImageName(std::string& fileName);

	CCLegendAModule* clone();

	void reset();

	// the original image idx and module idx
	int imgIdx;
	int modIdx;

private:
	int modX;
	int modY;
	int modW;
	int modH;

	std::string m_sFileName; 
};

class CCLegendAnimation : public __NodeRGBA
{
public:
	static CCLegendAnimation* create(std::string animFile, int actionIdx = 0);

public:
	CCLegendAnimation();
    ~CCLegendAnimation();

    virtual void draw(Renderer *renderer, const Mat4& transform, uint32_t flags);
	virtual void update(float dt);

	virtual void onEnter();
	virtual void onExit();

	bool initWithAnmFilename(const char* filename);

	CC_SYNTHESIZE(AAnimation*, m_pAnimationData, AnimationData)

	CC_SYNTHESIZE(int, m_actIdx, Action)

	CC_SYNTHESIZE(bool, m_bPlayLoop, PlayLoop)

	CC_SYNTHESIZE(float, m_nPlaySpeed, PlaySpeed)

	void play();
	void stop();
	void restart();
	inline bool isPlaying() { return m_bIsPlaying; };
	/** get the percent of the playback */
	float getPlayPercent();
	
	void synchronizeFrom(CCLegendAnimation* other);

	void loadTexture(int imgIdx, Texture2D* newTexture);

	Rect getSurroundingRect(int actId, int aFrameId);

	/* when animation is stop, release itself */
	inline void setReleaseWhenStop(bool value) { m_bReleaseWhenStop = value; };

	std::string getImageFileName(unsigned int imgIdx);
	void setImageFileName(unsigned int imgIdx, std::string& newImageFileName);

	// add special amodule, for example, weapon amodule in the player's animation
	CCLegendAModule* addSpecialAModule(int imgIdx, int modIdx);

	// CCLegendAnimationCacheȡ�1�CCLegendAnimation�ʱ����Ҫ�������
	void reset();

private:
	CCLegendAModule* createAModule(int imgIdx, int amodIdx);

	void drawAFrame(int actId, int aFrameId);

	// cache CCLegendAModule
	int getKey(int imgIdx, int modIdx);
    CCLegendAModule* addCCLegendAModule(int imgIdx, int modIdx);

private:
	/** array of all CCLegendAModule */
	CCLegendAModule*** m_pAllAModules;

	// update animation
	int m_aframeIdx;
	int m_delayCounter;
	int m_animTick;
	float m_timeElapse;

	bool m_bReleaseWhenStop;

	bool m_bIsPlaying;   // the animation is playing or not

	// key: <imgIdx, modIdx>, int ( imgIdx << 16 | modIdx )
	std::map<int, std::vector<CCLegendAModule*> > m_CCLegendAModulePool;

	// special amodule map
	std::map<int, CCLegendAModule*> m_specialAModuleMap;

	/**
	  * some times, the image of the animation will be replacd with other one
	  * this variable will record the new image file name
	  */ 
	std::vector<std::string> m_imageFileNames;
};

#endif // __LEGEND_ANIMATION_CCLEGENDANIMATION_H__
