
#ifndef __LEGEND_LEGENDAANIMATION_H__
#define __LEGEND_LEGENDAANIMATION_H__

#include "cocos2d.h"

USING_NS_CC;
using namespace std;

// the max number of the animation's image
#define MAX_AIMAGE_NUM 4

class AModule
{
public:
	int moduleID;
	int imageID;

	AffineTransform affineTrans;
	int colorValue;

public:
    AModule();
    virtual ~AModule();
};

class AFrame
{
public:
	AModule** aModules;
	int frameDelay;
	int amoduleNum;   // the number of (AModule*)aModules

public:
    AFrame();
    virtual ~AFrame();
};

class AAction
{
public:
	AFrame** aFrames;
	int aframeNum;   // the number of (AFrame*)aFrames
	int animTickCnt;   // the total number of animation tick ( include frameDelay )

public:
    AAction();
    virtual ~AAction();
};

/** An AAnimation object is the data from *.anm (Legend animation file)
*/
class AAnimation : public Ref
{
public:
    AAnimation();
    virtual ~AAnimation();

	void loadAnimationData(const char* anmFile);

	CC_SYNTHESIZE(int, m_imageNum, ImgNum)
	/** the module count of each image in this animation */
	int getModuleNum(int imgIdx);

	std::string getAnmFileName();
	std::string getImagePath(int imgIdx);
	/** the image file name */
	std::string getImageFileName(int imgIdx);

	/**
	 * length: module num * 4, offsetX, offsetY, width, height 
	 */
	CC_SYNTHESIZE(short**, m_moduleData, ModuleData)

	CC_SYNTHESIZE(AAction**, m_actions, Actions)

	int getAActionNum();

	/**
	 * get the total anmation tick count (include each aframe's delay)
	 */
	int getAnimTickNum(int actIdx);

	int getActionFrameNum(int actIdx);
	int getAFrameDelay(int actIdx, int frameIdx);

	int getAFrameIdxByAnimTick(int actIdx, int animTick);

private:
	std::string m_anmFileName;

	int* m_pModuleNum;
	std::string m_strImageFilenames[MAX_AIMAGE_NUM];

	int m_actionsNum;
};

#endif // __LEGEND_LEGENDAANIMATION_H__
