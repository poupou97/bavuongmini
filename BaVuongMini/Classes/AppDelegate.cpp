/****************************************************************************
 Copyright (c) 2017-2018 Xiamen Yaji Software Co., Ltd.
 
 http://www.cocos2d-x.org
 
 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:
 
 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.
 
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 ****************************************************************************/

#include "AppDelegate.h"

#include <vector>
#include <string>


#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS
#include "../Classes/GameView.h"
#include "../Classes/logo_state/LogoState.h"
#include "../Classes/login_state/LoginState.h"
#include "../Classes/test_state/TestState.h"
#include "../Classes/utils/GameResourceManager.h"
#include "../Classes/AppMacros.h"
#include "SimpleAudioEngine.h"
#include "../Classes/loadres_state/LoadResState.h"
#include "../Classes/gamescene_state/role/MyPlayer.h"
#include "../Classes/gamescene_state/role/MyPlayerAIConfig.h"

#include "NotificationClass.h"

#else

#include "GameView.h"
#include "logo_state/LogoState.h"
#include "login_state/LoginState.h"
#include "test_state/TestState.h"
#include "utils/GameResourceManager.h"
#include "AppMacros.h"
#include "SimpleAudioEngine.h"
#include "loadres_state/LoadResState.h"
#include "gamescene_state/role/MyPlayer.h"
#include "gamescene_state/role/MyPlayerAIConfig.h"
#endif


// #define USE_AUDIO_ENGINE 1
 #define USE_SIMPLE_AUDIO_ENGINE 1

#if USE_AUDIO_ENGINE && USE_SIMPLE_AUDIO_ENGINE
#error "Don't use AudioEngine and SimpleAudioEngine at the same time. Please just select one in your game!"
#endif

#if USE_AUDIO_ENGINE
#include "audio/include/AudioEngine.h"
using namespace cocos2d::experimental;
#elif USE_SIMPLE_AUDIO_ENGINE
#include "audio/include/SimpleAudioEngine.h"
using namespace CocosDenshion;
#endif

USING_NS_CC;

//static cocos2d::Size designResolutionSize = cocos2d::Size(480, 320);
//static cocos2d::Size smallResolutionSize = cocos2d::Size(480, 320);
//static cocos2d::Size mediumResolutionSize = cocos2d::Size(1024, 768);
//static cocos2d::Size largeResolutionSize = cocos2d::Size(2048, 1536);

AppDelegate::AppDelegate()
{
}

AppDelegate::~AppDelegate() 
{
	//set robot data save xml
	MyPlayerAIConfig::savePushConfig();
#if USE_AUDIO_ENGINE
    AudioEngine::end();
#elif USE_SIMPLE_AUDIO_ENGINE
    SimpleAudioEngine::end();
#endif
}

// if you want a different context, modify the value of glContextAttrs
// it will affect all platforms
void AppDelegate::initGLContextAttrs()
{
    // set OpenGL context attributes: red,green,blue,alpha,depth,stencil,multisamplesCount
    GLContextAttrs glContextAttrs = {8, 8, 8, 8, 24, 8, 0};

    GLView::setGLContextAttrs(glContextAttrs);
}

// if you want to use the package manager to install more packages,  
// don't modify or remove this function
static int register_all_packages()
{
    return 0; //flag for packages manager
}

bool AppDelegate::applicationDidFinishLaunching() {
    // initialize director
    auto director = Director::getInstance();
    auto glview = director->getOpenGLView();

	Application::Platform target = getTargetPlatform();
	//target = kTargetIpad;
	if (target == Application::Platform::OS_IPAD)
	{
		//designResolutionSize = cocos2d::Size(1024, 768);   // (1024,768)
		//designResolutionSize = cocos2d::Size(960, 720);   // (1024,768)
		designResolutionSize = cocos2d::Size(900, 675);   // (1024,768)
		//designResolutionSize = cocos2d::Size(854, 640);   // (1024,768)
		//designResolutionSize = cocos2d::Size(827, 620);   // (1024,768)
		//designResolutionSize = cocos2d::Size(800, 600);   // (1024,768)
	}

    if(!glview) {
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32) || (CC_TARGET_PLATFORM == CC_PLATFORM_MAC) || (CC_TARGET_PLATFORM == CC_PLATFORM_LINUX)
        glview = GLViewImpl::createWithRect("BaVuongMini", cocos2d::Rect(0, 0, designResolutionSize.width, designResolutionSize.height));
#else
        glview = GLViewImpl::create("BaVuongMini");
#endif
        director->setOpenGLView(glview);
    }

    // turn on display FPS
    director->setDisplayStats(true);

    // set FPS. the default value is 1.0/60 if you don't call this
    director->setAnimationInterval(1.0f / 60);

    /*// Set the design resolution
    glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::NO_BORDER);
    auto frameSize = glview->getFrameSize();
    // if the frame's height is larger than the height of medium size.
    if (frameSize.height > mediumResolutionSize.height)
    {        
        director->setContentScaleFactor(MIN(largeResolutionSize.height/designResolutionSize.height, largeResolutionSize.width/designResolutionSize.width));
    }
    // if the frame's height is larger than the height of small size.
    else if (frameSize.height > smallResolutionSize.height)
    {        
        director->setContentScaleFactor(MIN(mediumResolutionSize.height/designResolutionSize.height, mediumResolutionSize.width/designResolutionSize.width));
    }
    // if the frame's height is smaller than the height of medium size.
    else
    {        
        director->setContentScaleFactor(MIN(smallResolutionSize.height/designResolutionSize.height, smallResolutionSize.width/designResolutionSize.width));
    }*/

	// Set the design resolution
	glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::FIXED_HEIGHT);
	if (glview->getDesignResolutionSize().width < designResolutionSize.width)
	{
		glview->setDesignResolutionSize(designResolutionSize.width, designResolutionSize.height, ResolutionPolicy::FIXED_WIDTH);
		if (glview->getDesignResolutionSize().height < designResolutionSize.height)
		{
			// in this situation, maybe we can use pEGLView->getFrameSize() for design resolution
			CCAssert(false, "no suitable design resolution can be applied");
		}
	}
	CCLOG("Design Resolution: %d * %d", (int)glview->getDesignResolutionSize().width, (int)glview->getDesignResolutionSize().height);

	auto frameSize = glview->getFrameSize();

	std::vector<std::string> searchPaths;

	director->setContentScaleFactor(1.0f);

	auto fileUtils = FileUtils::getInstance();
	fileUtils->setSearchPaths(searchPaths);

	fileUtils->addSearchPath(fileUtils->getWritablePath().c_str());

	// turn on display FPS
	director->setDisplayStats(true);

	// set FPS. the default value is 1.0/60 if you don't call this
	director->setAnimationInterval(1.0 / 60);

    register_all_packages();

	// prepare resource
	GameResourceManager* mgr = new GameResourceManager();
	mgr->prepareStaticData();
	delete mgr;

	// enter the first state for the game
	GameView::getInstance()->init();
	GameState* pScene = new LoadResState();
	//GameState* pScene = new TestState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}

    return true;
}

// This function will be called when the app is inactive. Note, when receiving a phone call it is invoked.
void AppDelegate::applicationDidEnterBackground() {
    Director::getInstance()->stopAnimation();

#if USE_AUDIO_ENGINE
    AudioEngine::pauseAll();
#elif USE_SIMPLE_AUDIO_ENGINE
    SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
    SimpleAudioEngine::getInstance()->pauseAllEffects();
#endif

	Director::getInstance()->stopAnimation();

	// if you use SimpleAudioEngine, it must be pause
	SimpleAudioEngine::getInstance()->pauseBackgroundMusic();
	//set robot data save xml
	MyPlayerAIConfig::savePushConfig();

	// push notification for ios
	this->pushNotificationForIOS();
}

// this function will be called when the app is active again
void AppDelegate::applicationWillEnterForeground() {
    Director::getInstance()->startAnimation();

#if USE_AUDIO_ENGINE
    AudioEngine::resumeAll();
#elif USE_SIMPLE_AUDIO_ENGINE
    SimpleAudioEngine::getInstance()->resumeBackgroundMusic();
    SimpleAudioEngine::getInstance()->resumeAllEffects();
#endif
}

void AppDelegate::pushNotificationForIOS()
{
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	int nLevelGame = GameView::getInstance()->myplayer->getActiveRole()->level();
	int nLevelServer = LoginLayer::getLastServerRoleLevel();
	if (0 == nLevelServer)
	{
		int nLevelXML = UserDefault::getInstance()->getIntegerForKey("Push_Level", 0);
		if (0 != nLevelXML)
		{
			if (nLevelXML >= 20)
			{
				// 体力推送
				[NotificationClass pushNotificationPowerSend];
			}
		}
	}
	else
	{
		if (nLevelGame >= 20 || nLevelServer >= 20)
		{
			// 体力推送
			[NotificationClass pushNotificationPowerSend];
		}

		int nLevelXML = UserDefault::getInstance()->getIntegerForKey("Push_Level", 0);
		if (0 != nLevelXML)
		{
			if (nLevelXML >= 20)
			{
				// 体力推送
				[NotificationClass pushNotificationPowerSend];
			}
		}
		else if (nLevelGame >= nLevelServer && nLevelGame > nLevelXML)
		{
			UserDefault::getInstance()->setIntegerForKey("Push_Level", nLevelGame);

			UserDefault::getInstance()->flush();
		}
		else if (nLevelServer >= nLevelGame && nLevelServer > nLevelXML)
		{
			UserDefault::getInstance()->setIntegerForKey("Push_Level", nLevelServer);

			UserDefault::getInstance()->flush();
		}
	}

#endif
}
