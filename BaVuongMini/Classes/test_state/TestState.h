#ifndef _GAMESTATE_TEST_STATE_H_
#define _GAMESTATE_TEST_STATE_H_

#include "../GameStateBasic.h"
#include "../legend_script/ScriptHandlerProtocol.h"

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class Script;

/**
 * test class, 
 * demonstrate some code samples, run some testcase, etc.
 */

class TestLayer : public Layer, public ScriptHandlerProtocol, public TableViewDataSource, public TableViewDelegate
{
public:
    TestLayer();
	~TestLayer();

	enum State
	{
		State_perfect = 0,
		State_workRoom = 1,
		State_touchEnable = 2,

	};

	virtual void update(float dt);

	void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//void registerWithTouchDispatcher();

	//virtual void TouchesEnded(__Set *pTouches, Event *pEvent);

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch* pTouch, Event* event);

	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	virtual void tableCellWillRecycle(TableView* table, TableViewCell* cell);
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	virtual ssize_t numberOfCellsInTableView(TableView *table);

	virtual void registerScriptCommand(int scriptId);
	void addTutorialIndicator(const char* content);
	void removeTutorialIndicator();
	void setTutorialIndicatorPosition(Vec2 pos);

private:
	void testScene();

	void testSqlite();

	void setEffectPosition(Node* effectNode, Vec2 startPoint, Vec2 endPoint);
	void testLightningEffect();

	void testLabelColorByMark();

	void testGameActorAnimation();

	void testGraySprite();

	void testScript();

	void testStory();
    
	// special effect
    void testParticle();
	void showEffectCallback(Ref* pSender);
	void testMotionStreak();

	void testLegendAnimation();
	void testLegendTiledMap();

	void testTableView();

	void testCocosStudioScene();

	void testAPI();
    
    void testCCRichLabel();

	void testOpenUrl();

	void testClippingNode();
	void testClippingNode_Spark();

	void testSearchGeneral();
private:
	int m_state;

	struct timeval m_sStartTime;
	double m_dStartTime;

	void menuCallback(Ref* pSender);
	void menuSendCallback(Ref* pSender);
	void menuSendCallback1(Ref* pSender);
	void menuRichLabelButtonCallback(Ref* pSender);
	void skipScriptCallback(Ref* pSender);
	void runScriptCallback(Ref* pSender);
	void openURLCallback(Ref* pSender);

	Sprite * spLog;
	void setCurrentState(Ref * obj);
	char* testBytes;
	State m_curState;

	int mTutorialScriptInstanceId;

	int m_tableviewCellNumber;
};

class TestState : public GameState
{
public:
	~TestState();

    virtual void runThisState();
	
};

#endif // _GAMESTATE_LOGO_STATE_H_
