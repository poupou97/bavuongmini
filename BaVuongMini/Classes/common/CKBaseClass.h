//
//  CKBaseClass.h
//  Blog_C++_Reflection
//
//  Created by 晓龙 张 on 12-2-28.
//  Copyright (c) 2012年 chukong-inc. All rights reserved.
//

#ifndef Blog_C___Reflection_CKBaseClass_h
#define Blog_C___Reflection_CKBaseClass_h

#include "CKDynamicClass.h" 

class CKBaseClass ;

typedef void (*setValue)(CKBaseClass *t, void* c) ;

class CKBaseClass
{
private:
    DECLARE_CLASS(CKBaseClass)
    
public:
    CKBaseClass() ;
    virtual ~CKBaseClass() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;
    map<string, setValue> m_propertyMap ;
} ;

#define SYNTHESIZE(classType, varType, varName)    \
public:                                             \
inline static void set##varName(CKBaseClass*cp, void*value){ \
    classType* tp = (classType*)cp ;                        \
    tp->varName = (varType)value ;                      \
}                                                       \
inline varType get##varName(void) const {                \
    return varName ;                                      \
}


#endif
