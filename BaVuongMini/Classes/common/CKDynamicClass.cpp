//
//  CKDynamicClass.cpp
//  Blog_C++_Reflection
//
//  Created by 晓龙 张 on 12-2-28.
//  Copyright (c) 2012年 chukong-inc. All rights reserved.
//

#include "CKDynamicClass.h"

CKDynamicClass::CKDynamicClass(string name, createClass method) 
{
    CKClassFactory::sharedClassFactory().registClass(name, method) ;
}