//
//  CKClassFactory.h
//  Blog_C++_Reflection
//
//  Created by 晓龙 张 on 12-2-28.
//  Copyright (c) 2012年 chukong-inc. All rights reserved.
//

#ifndef Blog_C___Reflection_CKClassFactory_h
#define Blog_C___Reflection_CKClassFactory_h

#include<string>
#include<map>
#include<iostream>
using namespace std ;

typedef void* (*createClass)(void) ;

class CKClassFactory
{
public:
    CKClassFactory() ;
    
    virtual ~CKClassFactory() ;
    
    void* getClassByName(string className) ;
    
    void registClass(string name, createClass method) ;
    
    static CKClassFactory& sharedClassFactory() ;
    
private:
    map<string, createClass> m_classMap ;
} ;


#endif
