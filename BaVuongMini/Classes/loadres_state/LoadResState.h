#ifndef _GAMESTATE_LOADRES_STATE_H_
#define _GAMESTATE_LOADRES_STATE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../GameStateBasic.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"

#include "../messageclient/reqsender/ReqSenderProtocol.h"
#include "network\HttpClient.h"
//#include "AssetsManager/AssetsManager.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/**
 * update and display the loading interface
 * Author: Zhao Gang
 * Date: 2013/11/7
 */
class LoadResLayer : public Layer, public cocos2d::extension::AssetsManagerDelegateProtocol
{
public:
    LoadResLayer();

	void onVersionCheck();
	void onVersionCheckCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);

	void createDownloadedDir(const char* filePath);

	AssetsManager* getAssetsManager(int index);

    virtual void onError(cocos2d::extension::AssetsManager::ErrorCode errorCode);
    virtual void onProgress(int percent);
    virtual void onSuccess(int index);

	static void* thread_load(void* arg);
	void loadAllRes();

	virtual void update(float dt);
private:
	int CreateDir(const char *pDir);
    void repeatFunc();
	void callbackDownload(Ref *pSender, Widget::TouchEventType type);
	void updateLabel();
private:
	std::string m_strResUrl;
	std::vector<ResFile> m_vecResFile;

	int m_iResVersion;
	std::string pathToSave;
	Label *pProgressLabel;

	Sprite *pSprite;
	int m_iSizeResFile;

	Layer* layer;

	bool m_isResLoadComplete;
	int m_iState;
	long double m_dStartTime;
	long double m_dEndedTime;

	std::string downloadUrl;
	int m_llAllResSize;
	int m_hasDownloadSize;
	int _percent;
	int m_iReconnectNum;
};

class LoadResState : public GameState
{
public:
	LoadResState();

    virtual void runThisState();
};

void* loadAllRes(void *data);

#endif
