#include "LoadSceneState.h"
#include "../gamescene_state/GameSceneState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../messageclient/element/CMapInfo.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../gamescene_state/MainScene.h"
#include "../gamescene_state/sceneelement/GuideMap.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../utils/GameUtils.h"
#include "../GameUserDefault.h"
#include "cocostudio\CCSGUIReader.h"

#define LOAD_METHOD_ASYNC 0

#define LOADSCENE_SLIDER_NAME "slider"

using namespace CocosDenshion;

Layout* LoadSceneLayer::s_pPanel = NULL;
Layout* LoadSceneLayer::MissionSceneLayer = NULL;
Layout* LoadSceneLayer::TalkWithNpcLayer = NULL;
Layout* LoadSceneLayer::chatPanel = NULL;
Layout* LoadSceneLayer::expressPanel = NULL;
Layout* LoadSceneLayer::backPacPanel =  NULL;
Layout* LoadSceneLayer::mailPanel = NULL;
Layout *LoadSceneLayer::mailFriendPopup =NULL;
Layout* LoadSceneLayer::friendPanelBg = NULL;
Layout* LoadSceneLayer::friendlistPanel = NULL;
Layout* LoadSceneLayer::equipMentPanel=NULL;
Layout* LoadSceneLayer::auctionPanel = NULL;
Layout* LoadSceneLayer::SkillSceneLayer = NULL;
Layout* LoadSceneLayer::GeneralsLayer = NULL;
Layout* LoadSceneLayer::StoreHouseLayer = NULL;
Layout* LoadSceneLayer::StoreHouseSettingLayer = NULL;
Layout* LoadSceneLayer::quiryPanel = NULL;
Layout* LoadSceneLayer::GeneralsFateInfoLayer = NULL;
Layout* LoadSceneLayer::GeneralsSkillInfoLayer = NULL;
Layout* LoadSceneLayer::GeneralsSkillListLayer = NULL;
Layout* LoadSceneLayer::StrategiesUpgradeLayer = NULL;
Layout* LoadSceneLayer::OnStrategiesGeneralsInfoLayer = NULL;
Layout* LoadSceneLayer::SingleRecuriteResultLayer = NULL;
Layout* LoadSceneLayer::TenTimesRecuriteResultLayer = NULL;

Layout* LoadSceneLayer::ShopLayer=NULL;
Layout* LoadSceneLayer::mapPanel=NULL;
Layout* LoadSceneLayer::setLayout =NULL;
Layout* LoadSceneLayer::familyMainPanel=NULL;
Layout* LoadSceneLayer::familyManagePanel=NULL;
Layout* LoadSceneLayer::familyApplyPanel=NULL;
Layout* LoadSceneLayer::instanceDetailLayout =NULL;
Layout* LoadSceneLayer::instanceEndLayout =NULL;
Layout* LoadSceneLayer::onHookKillPanel =NULL;

//Layout* LoadSceneLayer::onCountryPanel =NULL;

Layout* LoadSceneLayer::GoldStoreLayer = NULL;
Layout* LoadSceneLayer::BuyCalculatorsLayer = NULL;

Layout* LoadSceneLayer::StrategiesDetailInfoPopUpLayer = NULL;

Layout* LoadSceneLayer::onlineAwardsLayout = NULL;
Layout* LoadSceneLayer::signDailyLayout = NULL;
Layout* LoadSceneLayer::activeLayout = NULL;
Layout* LoadSceneLayer::activeShopPanel = NULL;
Layout* LoadSceneLayer::rankLayout = NULL;
Layout* LoadSceneLayer::ExtraRewardsLayer = NULL;
Layout* LoadSceneLayer::OffLineArenaLayer = NULL;
Layout* LoadSceneLayer::RankListLayer = NULL;

Layout* LoadSceneLayer::vipDetatilPanel = NULL;
Layout* LoadSceneLayer::vipEveryDayRewardPanel = NULL;

Layout* LoadSceneLayer::questionLayout = NULL;
Layout* LoadSceneLayer::RechargePanel = NULL;


enum LoadScene_SubState
{
	State_Init = 0,
	State_LoadResStep_1,
	State_LoadResStep_2,
	State_LoadResStep_3,
	State_Finished,
};

LoadSceneLayer::LoadSceneLayer()
: m_state(State_Init)
, m_bLoadResFinished(false)
, time_(0)
{
	////setTouchEnabled(true);

	// close keyboard(change scene need close keyboard)
	auto pGlView = Director::getInstance()->getOpenGLView();
	if (pGlView)
	{
		pGlView->setIMEKeyboardState(false);
	}

    Size s = Director::getInstance()->getVisibleSize();

	if(GameView::getInstance()->getMapInfo()->country())
	{
		UserDefault::getInstance()->setIntegerForKey(CURRENT_COUNTRY, GameView::getInstance()->getMapInfo()->country());
		UserDefault::getInstance()->flush();
	}

	// game logo
	auto pSprite = Sprite::create(getLoadingImage());
	if(((float)s.width/(float)1136) > ((float)s.height/(float)640))
	{
		pSprite->setScaleX((float)s.width/(float)1136);
		pSprite->setScaleY((float)s.width/(float)1136);
	}
	else
	{
		pSprite->setScaleX((float)s.height/(float)640);
		pSprite->setScaleY((float)s.height/(float)640);
	}
	pSprite->setPosition(Vec2(s.width/2, s.height/2));
	pSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
	addChild(pSprite, 0);

	layer= Layer::create();

	//layer->setPosition(Vec2(s.width/2,30));
	layer->setPosition(Vec2(s.width/2,s.height/2));
	addChild(layer);

	float posY = s.height * 0.3f;
	auto mengban = ImageView::create();
	mengban->loadTexture("res_ui/zhezhao80_new.png");
	mengban->setScale9Enabled(true);
	mengban->setContentSize(Size(s.width, 100));
	mengban->setAnchorPoint(Vec2(0.5f,0.5f));
	mengban->setPosition(Vec2(0,-posY));
	mengban->setOpacity(130);
	layer->addChild(mengban);

	auto text=Label::createWithTTF(getLoadingText().c_str(), APP_FONT_NAME, 20);
	text->setPosition(Vec2(0,-posY));
	auto shadowColor = Color4B::BLACK;   // black
	text->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
	layer->addChild(text);

	// Create the slider
    auto slider = Slider::create();

	slider->setName(LOADSCENE_SLIDER_NAME);
    //slider->setTouchEnabled(true);
    slider->loadBarTexture("");
    slider->loadSlidBallTextures("res_ui/LOADING/light.png", "res_ui/LOADING/light.png", "");
    slider->loadProgressBarTexture("res_ui/progress_bar.png");
	slider->setScale9Enabled(true);
	slider->setContentSize(Size(s.width, 40));
    slider->setPosition(Vec2(0,-(posY+50)));
	slider->setPercent(0);

    layer->addChild(slider);
	
//  auto pSprite = Sprite::create("images/mm01.png");
//  pSprite->setPosition(Vec2(s.width/2, s.height/2));
// 	pSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
//	addChild(pSprite, 0);

//     m_pLabelLoading = Label::createWithTTF("loading...", "Arial", 50);
//     m_pLabelLoading->setPosition(Vec2(s.width / 2, 40));
//     this->addChild(m_pLabelLoading);

#if LOAD_METHOD_ASYNC
	m_pLabelPercent = Label::createWithTTF("%0", APP_FONT_NAME, 50);
    m_pLabelPercent->setPosition(Vec2(s.width / 2, s.height / 2 + 20));
	Color3B shadowColor = Color3B(0,0,0);   // black
	m_pLabelPercent->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
    this->addChild(m_pLabelPercent);
#endif

	this->scheduleUpdate();
}

void LoadSceneLayer::loadingCallBack(Ref *obj)
{
    ++m_nNumberOfLoadedSprites;
    char tmp[10];
    sprintf(tmp,"%%%d", (int)(((float)m_nNumberOfLoadedSprites / m_nNumberOfSprites) * 100));
    m_pLabelPercent->setString(tmp);

    if (m_nNumberOfLoadedSprites == m_nNumberOfSprites)
    {
		m_bLoadResFinished = true;
    }
}

void LoadSceneLayer::update(float dt)
{
	auto slider =(Slider*)(layer->getChildByName(LOADSCENE_SLIDER_NAME));
	if(m_state == State_Init)
	{
		loadCommonRes();
		slider->setPercent(5);
		m_state = State_LoadResStep_1;
	}
	else if(m_state == State_LoadResStep_1)
	{
		loadUIWidgetRes();
		slider->setPercent(30);
		m_state = State_LoadResStep_2;
	}
	else if(m_state == State_LoadResStep_2)
	{
		preLoadResource(GameView::getInstance()->getMapInfo()->mapid().c_str());
		slider->setPercent(60);
		m_state = State_LoadResStep_3;
	}
	else if(m_state == State_LoadResStep_3)
	{
		if(m_bLoadResFinished)
			m_state = State_Finished;
			slider->setPercent(99);
	}
	else if(m_state == State_Finished)
	{
		//this->removeChild(m_pLabelLoading, true);
        //this->removeChild(m_pLabelPercent, true);

		// create the test scene and run it
		auto pScene = new GameSceneState();
		if (pScene)
		{
			if(!GameView::getInstance()->myplayer->isMapReached(GameView::getInstance()->getMapInfo()->country(), GameView::getInstance()->getMapInfo()->mapid().c_str()))
			{
				GameView::getInstance()->myplayer->addCurrentReachedMap(GameView::getInstance()->getMapInfo()->country(), GameView::getInstance()->getMapInfo()->mapid());
			}

			pScene->runThisState();
			pScene->release();
		}
		
	}


}

void LoadSceneLayer::preLoadResource(const char *path)
{
	//std::vector<std::string>* sceneResList = GameSceneLayer::generateResourceList("xsc.level");
	std::vector<std::string>* sceneResList = GameSceneLayer::generateResourceList(path);

	m_nNumberOfSprites = sceneResList->size();;
	m_nNumberOfLoadedSprites = 0;

	// load textrues
	vector<std::string>::iterator iter;
	for (iter = sceneResList->begin(); iter != sceneResList->end(); ++iter)
	{
#if LOAD_METHOD_ASYNC
		// addImageAsync() method maybe has memory lead, so please pay attention to it
		Director::getInstance()->getTextureCache()->addImageAsync((*iter).c_str(), this, callfuncO_selector(LoadSceneLayer::loadingCallBack));
#else
		// synchronize load resource, please set finished flag immediately
		Director::getInstance()->getTextureCache()->addImage((*iter).c_str());
		m_bLoadResFinished = true;
#endif
	}

	delete sceneResList;
}

std::string LoadSceneLayer::getLoadingText()
{
	int size = LoadingTextData::s_LoadingTexts.size();
	if(size <= 0)
	{
		LoadingTextData::load("game.db");
		size = LoadingTextData::s_LoadingTexts.size();
	}

	unsigned int index = GameUtils::getRandomNum(size);
	//CCLOG("index: %d, text: %s", index, LoadingTextData::s_LoadingTexts.at(index).c_str());
	CCAssert(index >= 0 && index < size, "out of range");
	return LoadingTextData::s_LoadingTexts.at(index);
}

char* LoadSceneLayer::getLoadingImage()
{
	int size = 4;
	char* image[] = {"images/loding1.jpg", "images/loding2.jpg", "images/loding3.jpg", "images/loding4.jpg"};
	unsigned int index = GameUtils::getRandomNum(size);
	CCAssert(index >= 0 && index < size, "out of range");
	return image[index];
}

void LoadSceneLayer::loadCommonRes()
{
}

void LoadSceneLayer::loadLayoutFromJson(Layout** dest, const char* fileName)
{
	if(*dest == NULL)
	{
		long long currentTime = GameUtils::millisecondNow();

		std::string fullPathName = "res_ui/";
		fullPathName.append(fileName);

		*dest = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile(fullPathName.c_str());
		(*dest)->retain();

		long deltaTime = GameUtils::millisecondNow() - currentTime;
		CCLOG("load UI, cost time: %d", deltaTime);
	}
}

void LoadSceneLayer::loadUIWidgetRes()
{
	loadLayoutFromJson(&LoadSceneLayer::s_pPanel, "renwubeibao_1.json");
	loadLayoutFromJson(&LoadSceneLayer::SkillSceneLayer, "jineng_1.json");
	loadLayoutFromJson(&LoadSceneLayer::equipMentPanel, "zhuangbei_1.json");
	loadLayoutFromJson(&LoadSceneLayer::onlineAwardsLayout, "OnlineAwards_1.json");
	loadLayoutFromJson(&LoadSceneLayer::GeneralsLayer, "wujiang_1.json");
	loadLayoutFromJson(&LoadSceneLayer::friendPanelBg, "haoyou_1.json");
	loadLayoutFromJson(&LoadSceneLayer::auctionPanel, "jishouhang_1.json");
	loadLayoutFromJson(&LoadSceneLayer::mapPanel, "map_1.json");
	loadLayoutFromJson(&LoadSceneLayer::familyMainPanel, "family_main_1.json");
	loadLayoutFromJson(&LoadSceneLayer::signDailyLayout, "Registration_1.json");
	loadLayoutFromJson(&LoadSceneLayer::mailPanel, "youjian_1.json");
	loadLayoutFromJson(&LoadSceneLayer::onHookKillPanel, "on_hook_1.json");
	loadLayoutFromJson(&LoadSceneLayer::vipEveryDayRewardPanel, "vip1_1.json");
	loadLayoutFromJson(&LoadSceneLayer::activeLayout, "Activity_1.json");
	loadLayoutFromJson(&LoadSceneLayer::OffLineArenaLayer, "arena_1.json");
	loadLayoutFromJson(&LoadSceneLayer::SingleRecuriteResultLayer, "wujiang_get_popupo_1.json");
	loadLayoutFromJson(&LoadSceneLayer::TenTimesRecuriteResultLayer, "wujiang_SuperGet_popupo_1.json");
}

////////////////////////////////////////////////////////////////////////////

LoadSceneState::LoadSceneState()
{
	this->setTag(GameView::STATE_LOAD_GAME);
}

void LoadSceneState::runThisState()
{
    auto pLayer = new LoadSceneLayer();
    addChild(pLayer);

    Director::getInstance()->replaceScene(this);
    pLayer->release();
}
