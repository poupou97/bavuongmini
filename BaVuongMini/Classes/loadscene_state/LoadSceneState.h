#ifndef _GAMESTATE_LOADSCENE_STATE_H_
#define _GAMESTATE_LOADSCENE_STATE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../GameStateBasic.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

/**
 * update and display the loading interface
 * Author: Zhao Gang
 * Date: 2013/11/7
 */
class LoadSceneLayer : public Layer
{
public:
    LoadSceneLayer();

	virtual void update(float dt);

	void preLoadResource(const char *path);
	void loadingCallBack(cocos2d::Ref *obj);

	static Layout* s_pPanel;
	static Layout* MissionSceneLayer;
	static Layout* TalkWithNpcLayer;
	///////////
	static Layout* chatPanel;
	static Layout* expressPanel;
	static Layout* backPacPanel;
	static Layout* mailPanel;
	static Layout* mailFriendPopup;
	static Layout* friendPanelBg;
	static Layout* equipMentPanel;
	static Layout* friendlistPanel;
	static Layout* familyMainPanel;
	static Layout* familyApplyPanel;
	static Layout* familyManagePanel;
	static Layout* quiryPanel;

	static Layout* mapPanel;
	static Layout* auctionPanel;
	static Layout* SkillSceneLayer;
	static Layout* GeneralsLayer;
	static Layout* StoreHouseLayer;
	static Layout* StoreHouseSettingLayer;
	static Layout* GeneralsFateInfoLayer;
	static Layout* GeneralsSkillInfoLayer;
	static Layout* GeneralsSkillListLayer;
	static Layout* StrategiesUpgradeLayer;
	static Layout* OnStrategiesGeneralsInfoLayer;
	static Layout* SingleRecuriteResultLayer;
	static Layout* TenTimesRecuriteResultLayer;

	static Layout* ShopLayer;
	static Layout* setLayout;
	static Layout* instanceDetailLayout;
	static Layout* instanceEndLayout;

	static Layout* onHookKillPanel;

	//static Layout* onCountryPanel;

	static Layout* GoldStoreLayer;
	//�������Ĺ�����
	static Layout* BuyCalculatorsLayer;

	static Layout* StrategiesDetailInfoPopUpLayer;

	static Layout* onlineAwardsLayout;
	static Layout* signDailyLayout;
	static Layout* activeLayout;
	static Layout* activeShopPanel;
	static Layout* rankLayout;
	static Layout* ExtraRewardsLayer;

	static Layout* OffLineArenaLayer;
	static Layout* RankListLayer;

	static Layout* vipDetatilPanel;
	static Layout* vipEveryDayRewardPanel;

	static Layout* questionLayout;
	static Layout* RechargePanel;

	static void loadLayoutFromJson(Layout** dest, const char* fileName);

	char * mapPath;

private:
	std::string getLoadingText();
	void loadCommonRes();
	void loadUIWidgetRes();

	char* getLoadingImage();

private:
    cocos2d::Label *m_pLabelLoading;
    cocos2d::Label *m_pLabelPercent;
    int m_nNumberOfSprites;
    int m_nNumberOfLoadedSprites;

	int m_state;
	bool m_bLoadResFinished;

	ProgressTimer *progressTimer;

	Layer * layer;
	int time_;
};

class LoadSceneState : public GameState
{
public:
	LoadSceneState();

    virtual void runThisState();
};

#endif
