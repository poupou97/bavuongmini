#include "GameStateBasic.h"

#include "messageclient/GameMessageProcessor.h"
#include "messageclient/ClientNetEngine.h"
#include "GameView.h"

GameState::GameState()
{
    Scene::init();

	scheduleUpdate();
}

void GameState::onEnter()
{
    Scene::onEnter();

	// the common function for all gamestates
	// todo

	// release previous GameState's resource !!!
	// at this point, the previous GameState has been released. ( it happeded in CCDirector::setNextScene() )
	// so calling purgeCachedData() will release all textures which were created in previous GameState
	Director::getInstance()->purgeCachedData();
}

bool GameState::canHandleMessage()
{
	return true;
}

void GameState::update(float dt)
{
	GameMessageProcessor::sharedMsgProcessor()->checkNetwork();

	// dynamic message handling
	int nHandleMsgCnt = ClientNetEngine::sharedSocketEngine()->getMsgSize() / 30;   // 30 is the baseline FPS
	if(nHandleMsgCnt <= 0)
		nHandleMsgCnt = 1;
	//CCLOG("total message size: %d, HandleMsgCnt: %d", ClientNetEngine::sharedSocketEngine()->getMsgSize(), nHandleMsgCnt);

	// do not handle message, for example when it's loading state
	for(int i = 0; i < nHandleMsgCnt; i++)
	{
		if(canHandleMessage())
			GameMessageProcessor::sharedMsgProcessor()->socketUpdate();
	}
}
