#include "DefaultServer.h"
#include "LoginState.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "../classes/gamescene_state/gamescenestate.h"
#include "../classes/loadscene_state/loadscenestate.h"
#include "../classes/legend_engine/cclegendanimation.h"
#include "../classes/legend_engine/legendlevel.h"
#include "../classes/ui/extensions/ccrichlabel.h"
#include "../classes/ui/extensions/richelement.h"

#include "../classes/utils/gameutils.h"

#include "../classes/messageclient/gamemessageprocessor.h"
#include "../classes/messageclient/clientnetengine.h"

#include "../classes/ui/extensions/richtextinput.h"
#include "../classes/gameview.h"
#include "../classes/messageclient/protobuf/loginmessage.pb.h"
#include "../classes/login_state/login.h"
#include "../classes/login_state/createrole.h"
#include "../classes/ui/generals_ui/generalsui.h"
#include "../classes/utils/staticdatamanager.h"
#include "../classes/gamescene_state/role/baseplayer.h"
#include "../classes/appmacros.h"
#include "../classes/gamescene_state/role/myplayeraiconfig.h"
#include "../classes/gamescene_state/role/myplayer.h"
#include "../classes/gamescene_state/role/myplayerai.h"
#include "../classes/gameuserdefault.h"
#include "../classes/login_state/moutainbackgroundlayer.h"
#include "../classes/ui/registernotice/registernoticeui.h"

#include "share_sdk/shareloginstate.h"

#else

#include "../gamescene_state/GameSceneState.h"
#include "../loadscene_state/LoadSceneState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../legend_engine/LegendLevel.h"
#include "../ui/extensions/CCRichLabel.h"
#include "../ui/extensions/RichElement.h"

#include "../utils/GameUtils.h"

#include "../messageclient/GameMessageProcessor.h"
#include "../messageclient/ClientNetEngine.h"

#include "../ui/extensions/RichTextInput.h"
#include "../GameView.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "Login.h"
#include "CreateRole.h"
#include "../ui/generals_ui/GeneralsUI.h"
#include "../utils/StaticDataManager.h"
#include "../gamescene_state/role/BasePlayer.h"
#include "AppMacros.h"
#include "../gamescene_state/role/MyPlayerAIConfig.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../gamescene_state/role/MyPlayerAI.h"
#include "GameUserDefault.h"
#include "MoutainBackGroundLayer.h"
#include "../ui/registerNotice/RegisterNoticeui.h"

#endif


//#define THREEKINGDOMS_HOST_INTERNET "192.168.1.138"//"61.149.218.125"//"qianliang2013.vicp.cc"//"192.168.1.117"
//#define THREEKINGDOMS_PORT_INTERNET 8088
//#define THREEKINGDOMS_HOST_WANMEI "124.202.137.33"
//#define THREEKINGDOMS_PORT_WANMEI 8088
#define THREEKINGDOMS_HOST_STUDIO "139.99.49.200"  
#define THREEKINGDOMS_PORT_STUDIO 8188
//#define THREEKINGDOMS_HOST_GBK "192.168.1.118"  
//#define THREEKINGDOMS_PORT_GBK 8088  
//#define THREEKINGDOMS_HOST_ZHAGNGJING "192.168.1.132"  
//#define THREEKINGDOMS_PORT_ZHAGNGJING 8088  
//#define THREEKINGDOMS_HOST_LOCAL "127.0.0.1"  
//#define THREEKINGDOMS_PORT_LOCAL 8088  

#define MAX_LOGIN_ROLE_NUM 24

#define LASTSERVER_TAG 201
#define HIGHLIGHT_FRAME_TAG 202
#define NEWSERVER_TAG 203
#define WAIT_TIME 30*1000

#define TABLEVIEW_LAST_TAG 55
#define TABLEVIEW_TAG 56

#define CLIP_TIME 3.0f
#define DELAY_TIME 15.0f

#define FRAME_TAG 60
#define STATUS_TAG 61

enum {
	kStateStart = 0,
	kStateWait = 1,
	kStateReload = 2,
};

enum {
	kStateRoleList = 0,
	kStateConnectingGameServer,
};

enum {
	kTagPreparingBackGround = 2,
	kTagPreparingTip = 3,
	kTagServerTip = 4,
	kTagRoleListMenu = 16,   // last one !! please add more tag before it
};

int DefaultServerLayer::mCurrentServerId = -1;

DefaultServerLayer::DefaultServerLayer()
: m_state(kStateRoleList)
,isLastSelected(true)
,isTableViewTouched(true)
,m_iStatus(kStateStart)
{
	////setTouchEnabled(true);

	m_pServerListRsp = new ServerListRsp();
	Login::getServerListInfo(m_pServerListRsp);

	if(mCurrentServerId < 0)
	{
		int i;
		for(i = 0; i < m_pServerListRsp->serverinfos_size(); i++)
		{
			if(m_pServerListRsp->lastserverid() == m_pServerListRsp->serverinfos(i).serverid())
			{
				lastServerIndex = i;
				break;
			}
		}
		if(i == m_pServerListRsp->serverinfos_size())
		{
			lastServerIndex = 0;
		}
		mCurrentServerId = lastServerIndex;
	}
	initLayer();

	scheduleUpdate();
}

DefaultServerLayer::~DefaultServerLayer()
{

}

void DefaultServerLayer::initLayer()
{
	Size s = Director::getInstance()->getVisibleSize();
	
	auto pBackGround = MoutainBackGroundLayer::create();
	pBackGround->setBackGroundColor(Color3B(255, 255, 255));
	addChild(pBackGround);

	auto layer= Layer::create();
	addChild(layer, 9999, 255);

	auto m_pLayer= Layer::create();
	m_pLayer->setIgnoreAnchorPointForPosition(false);
	// ê�������ģ�����������Ļ���ĵ
	m_pLayer->setAnchorPoint(Vec2(0.5f,0.5f));
	m_pLayer->setPosition(Vec2(s.width/2,s.height/2));  
	m_pLayer->setTag(255);
	// ����û�׼�ֱ��
	m_pLayer->setContentSize(Size(UI_DESIGN_RESOLUTION_WIDTH, UI_DESIGN_RESOLUTION_HEIGHT));
	addChild(m_pLayer);
	
// 	//gamelogo
// 	Sprite* pSpriteLogo = Sprite::create("res_ui/select_the_sercer/gamelogo.png");
// 	pSpriteLogo->setPosition(Vec2(410, 340));
// 	pSpriteLogo->setAnchorPoint(Vec2(0.5f, 0.5f));
// 	pSpriteLogo->setScale(1.0f);
// 	pSpriteLogo->setOpacity(255);
// 	m_pLayer->addChild(pSpriteLogo, 1);
// 	
// 	//yanjing
// 	Sprite* pSpriteYanjing = Sprite::create("images/yanjing.png");
// 	pSpriteYanjing->setPosition(Vec2(pSpriteLogo->getContentSize().width/2+5, pSpriteLogo->getContentSize().height - 38));
// 	pSpriteYanjing->setAnchorPoint(Vec2(0.5f, 0.5f));
// 	pSpriteYanjing->setScaleX(2.0f);
// 	pSpriteYanjing->setScaleY(1.16f);
// 	pSpriteLogo->addChild(pSpriteYanjing,1);
// 
// 	auto  action1 = FadeTo::create(1.2f,30);
// 	CCDelayTime * delay_1 = DelayTime::create(1.5f);
// 	auto  action2 = FadeTo::create(1.2f,255);
// 	CCDelayTime * delay_2 = DelayTime::create(0.6f);
// 	RepeatForever * repeapAction = RepeatForever::create(Sequence::create(action1,delay_1,action2,delay_2,NULL));
// 	pSpriteYanjing->runAction(repeapAction);
	
// 	ClippingNode* clip = ClippingNode::create();//ʴ����ü�ڵ
// 	Sprite* gameTitle = Sprite::create("res_ui/select_the_sercer/gamelogo_text_mask.png");
// 	clip->setStencil(gameTitle);//����òü�ģ�
// 	clip->setAlphaThreshold(0.0f);//�����͸�����ֵ
// 	//clip->setInverted(true);
// 	clip->setContentSize(Size(gameTitle->getContentSize().width,gameTitle->getContentSize().height));//���òü�ڵ��С    
// 	Size clipSize = clip->getContentSize();//��ȡ�ü�ڵ��С
// 	clip->setPosition(Vec2(410,340));//���òü�ڵ�λ�
// 

	//Sprite* gameTitle_show = Sprite::create("res_ui/select_the_sercer/gamelogo.png");//ô���Ҫ��ʾ�Ķ��
// 	Sprite* spark = Sprite::create("images/guangtiao.png");//󴴽�������
// 	//clip->addChild(gameTitle_show,2);//��Ҫ��ʾ�����ݷ��ڲü�ڵ��У���ʵ����ֱ�clip->addChild(gameTitle,1);Ӵ˴�Ϊ��˵������ʾ���
// 	spark->setPosition(Vec2(-400, 0));//������������λ�
// 	clip->addChild(spark,3);//����������鵽�ü�ڵ
// 	m_pLayer->addChild(clip,1);//���Ӳü�ڵ
// 
// 	MoveTo* moveAction = MoveTo::create(CLIP_TIME, Vec2(clipSize.width, 0));//㴴������ڵ�Ķ��
// 	MoveTo* moveBack = MoveTo::create(0, Vec2(-clipSize.width, 0));
// 	CCDelayTime* delayTime = DelayTime::create(DELAY_TIME);
// 	Sequence* seq = Sequence::create(moveAction, delayTime, moveBack, NULL);
// 	RepeatForever* repreatAction = RepeatForever::create(seq);
// 	spark->runAction(repreatAction);//���ڵ��ظ�ִ�ж��

	LoginState::addAppVersionInfo(this);

	//ImageView * ImageView_biankuang= (ImageView *)Helper::seekWidgetByName(serverListPanel,"ImageView_biankuang1");
	
	auto frame = Button::create();
	frame->loadTextures("res_ui/select_the_sercer/namedi.png", "res_ui/select_the_sercer/namedi.png", "");
	frame->setAnchorPoint(Vec2(0.5f, 0.5f));
	frame->setScale9Enabled(true);
	frame->setCapInsets(Rect(14,14,1,1));
	//sprite->setPreferredSize(Size(370,55));
	frame->setContentSize(Size(321,50));
	frame->setPosition(Vec2(400, 160));
	frame->setTouchEnabled(true);
	frame->setPressedActionEnabled(true);
	frame->setTag(FRAME_TAG);
	frame->addTouchEventListener(CC_CALLBACK_2(DefaultServerLayer::onLastServerGetClicked, this));
	m_pLayer->addChild(frame);
	
	auto serverStatus = ui::ImageView::create();
	switch(m_pServerListRsp->serverinfos(mCurrentServerId).status())
	{
		case 1:
			serverStatus->loadTexture("res_ui/select_the_sercer/green_new.png");
			break;
		case 2:
			serverStatus->loadTexture("res_ui/select_the_sercer/yellow_new.png");
			break;
		case 3:
			serverStatus->loadTexture("res_ui/select_the_sercer/red_new.png");
			break;
		case 4:
			serverStatus->loadTexture("res_ui/select_the_sercer/gray_new.png");
			break;
		case 5:
			serverStatus->loadTexture("res_ui/select_the_sercer/gray_new.png");
			break;
		default:
			break;
	}
	serverStatus->setVisible(true);
	serverStatus->setAnchorPoint(Vec2(0.5f,0.5f));
	//serverStatus->setPosition(Vec2(-130, 0));
	serverStatus->setScale(0.85f);
	serverStatus->setTag(STATUS_TAG);
	serverStatus->setPosition(Vec2(30, frame->getBoundingBox().size.height / 2));
	frame->addChild(serverStatus);

	auto serverName =Label::createWithTTF(m_pServerListRsp->serverinfos(mCurrentServerId).name().c_str(), APP_FONT_NAME, 18);
	serverName->enableOutline(Color4B::BLACK, 2.0f);
	serverName->setAnchorPoint(Vec2(0,0.5f));
	serverName->setPosition(Vec2(60, frame->getBoundingBox().size.height / 2));
	serverName->setColor(Color3B(0, 255, 6));
	frame->addChild(serverName);
	
	auto serverChange = Label::createWithTTF(StringDataManager::getString("server_change"), APP_FONT_NAME, 18);
	serverChange->enableOutline(Color4B::BLACK, 2.0f);
	serverChange->setAnchorPoint(Vec2(0.5f,0.5f));
	serverChange->setPosition(Vec2(frame->getBoundingBox().size.width / 2 + 100, frame->getBoundingBox().size.height / 2));
	serverChange->setColor(Color3B(255, 255, 255));
	frame->addChild(serverChange);
	
	auto change = Button::create();
	change->setAnchorPoint(Vec2(0.5f, 0.5f));
	change->setTouchEnabled(true);
	change->loadTextures("res_ui/tab_4_off.png", "res_ui/tab_4_off.png", "");
	change->setPosition(Vec2(s.width-80, s.height-45));
	change->addTouchEventListener(CC_CALLBACK_2(DefaultServerLayer::onChangeGetClicked, this));
	change->setPressedActionEnabled(true);
	layer->addChild(change);
	
	auto change_label = Label::createWithTTF(StringDataManager::getString("account_change"), APP_FONT_NAME, 20);
	change_label->enableOutline(Color4B::BLACK, 2.0f);
	change_label->setAnchorPoint(Vec2(0.5f,0.5f));
	change_label->setPosition(Vec2(change->getBoundingBox().size.width / 2, change->getBoundingBox().size.height / 2));
	//serverChange->setColor(Color3B(255, 255, 255));
	change->addChild(change_label);
	
	auto enter = Button::create();
	enter->setAnchorPoint(Vec2(0.5f, 0.5f));
	enter->setTouchEnabled(true);
	enter->loadTextures("res_ui/creating_a_role/button_di.png", "res_ui/creating_a_role/button_di.png", "");
	enter->setScale9Enabled(true);
	enter->setCapInsets(Rect(50,30,1,1));
	enter->setContentSize(Size(150, 56));
	enter->setPosition(Vec2(400, 80));
	enter->addTouchEventListener(CC_CALLBACK_2(DefaultServerLayer::onEnterGetClicked, this));
	enter->setPressedActionEnabled(true);
	m_pLayer->addChild(enter);
	
	auto enterText = ui::ImageView::create();
	enterText->loadTexture("res_ui/select_the_sercer/enter.png");
	enterText->setVisible(true);
	enterText->setPosition(Vec2(enter->getBoundingBox().size.width / 2, enter->getBoundingBox().size.height / 2));
	enter->addChild(enterText);

	//add registerNotice
	int registerSize = GameView::getInstance()->registerNoticeVector.size();
	if (GameView::getInstance()->registerNoticeIsShow == true && registerSize > 0)
	{
		std::string strings_ = GameView::getInstance()->registerNoticeVector.at(0);
		if (strlen(strings_.c_str()) >0)
		{
			GameView::getInstance()->registerNoticeIsShow = false;
			auto registerui = RegisterNoticeui::create();
			registerui->setIgnoreAnchorPointForPosition(false);
			registerui->setAnchorPoint(Vec2(0.5f,0.5f));
			registerui->setPosition(Vec2(s.width/2,s.height/2));
			layer->addChild(registerui, 1);
		}
	}
}

void DefaultServerLayer::TouchesEnded(__Set *pTouches, Event *pEvent)
{

}

void DefaultServerLayer::onLastServerGetClicked(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto pScene = new LoginState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void DefaultServerLayer::onChangeGetClicked(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

		// SDK�������루ע��ӿڣ�
		ShareLoginState::logout();

#endif

		auto pScene = new LoginGameState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void DefaultServerLayer::onEnterGetClicked(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (4 == m_pServerListRsp->serverinfos(mCurrentServerId).status())
		{
			//GameView::getInstance()->showAlertDialog(StringDataManager::getString("server_status_maintain"));
			GameView::getInstance()->showAlertDialog(m_pServerListRsp->serverinfos(mCurrentServerId).statusvalue().c_str());
			return;
		}
		else if (5 == m_pServerListRsp->serverinfos(mCurrentServerId).status())
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("server_status_willopen"));
			return;
		}

		CCLOG("m_pServerListRsp->serverinfos(0).ip().c_str() = %s \n", m_pServerListRsp->serverinfos(mCurrentServerId).ip().c_str());
		CCLOG("m_pServerListRsp->serverinfos(0).port() = %d \n", m_pServerListRsp->serverinfos(mCurrentServerId).port());
		ClientNetEngine::sharedSocketEngine()->setAddress(m_pServerListRsp->serverinfos(mCurrentServerId).ip().c_str(), m_pServerListRsp->serverinfos(mCurrentServerId).port());

		if (1 == m_pServerListRsp->serverinfos(mCurrentServerId).has_roleid())
		{
			LoginLayer::mRoleId = m_pServerListRsp->serverinfos(mCurrentServerId).roleid();

			// tip's background
			Size s = Director::getInstance()->getVisibleSize();
			Rect insetRect = Rect(32, 15, 1, 1);
			auto backGround = cocos2d::extension::Scale9Sprite::create("res_ui/kuang_1.png");
			backGround->setCapInsets(insetRect);
			backGround->setPreferredSize(Size(550, 50));
			backGround->setPosition(Vec2(s.width / 2, s.height / 2));
			backGround->setAnchorPoint(Vec2(0.5f, 0.5f));
			//backGround->setTag(kTagPreparingBackGround);
			backGround->setVisible(false);
			addChild(backGround);

			// show tip
			auto label = Label::createWithTTF(StringDataManager::getString("con_enter_game"), APP_FONT_NAME, 25);
			addChild(label);
			//label->setTag(kTagPreparingTip);
			label->setAnchorPoint(Vec2(0.5f, 0.5f));
			label->setPosition(Vec2(s.width / 2, s.height / 2));
			label->setVisible(false);

			auto action = (ActionInterval*)Sequence::create
			(
				//Show::create(),
				DelayTime::create(0.5f),
				CallFunc::create(CC_CALLBACK_0(DefaultServerLayer::connectGameServer, this)),
				NULL
			);
			label->runAction(action);

			UserDefault::getInstance()->setIntegerForKey("general_mode", 1);


#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

			// ������������ʾ��
			ShareLoginState::showFloatView();

#endif
		}
		else
		{
			onRoleListReq();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void DefaultServerLayer::onRoleListReq()
{    
    // test 1
    {
		cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
        std::string url = Login::s_loginserver_ip;
        url.append(":48688/rolelist");
        request->setUrl(url.c_str());
        request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
        request->setResponseCallback(this, httpresponse_selector(DefaultServerLayer::onRoleListRequestCompleted));

		RoleListReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);
		httpReq.set_serverid(m_pServerListRsp->serverinfos(mCurrentServerId).serverid());

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
		cocos2d::network::HttpClient::getInstance()->send(request);
        request->release();
    }
}

void DefaultServerLayer::onRoleListRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());

	RoleListRsp roleListRsp;
	CCAssert(response->getResponseData()->size() > 0, "should not be empty");
	roleListRsp.ParseFromString(strdata);

	CCLOG("response.result = %d", roleListRsp.result());

	if(1 == roleListRsp.result()&&roleListRsp.roleinfos().size())
	{
		LoginLayer::mRoleId = roleListRsp.roleinfos(0).id();

		// tip's background
		Size s = Director::getInstance()->getVisibleSize();
		Rect insetRect = Rect(32,15,1,1);
		auto backGround = cocos2d::extension::Scale9Sprite::create("res_ui/kuang_1.png");
		backGround->setCapInsets(insetRect);
		backGround->setPreferredSize(Size(550, 50));
		backGround->setPosition(Vec2(s.width/2, s.height/2));
		backGround->setAnchorPoint(Vec2(0.5f, 0.5f));
		backGround->setTag(kTagPreparingBackGround);
		backGround->setVisible(false);
		addChild(backGround);

		// show tip
		auto label = Label::createWithTTF(StringDataManager::getString("con_enter_game"), "Arial", 25);
		addChild(label);
		label->setTag(kTagPreparingTip);
		label->setAnchorPoint(Vec2(0.5f, 0.5f));
		label->setPosition( Vec2(s.width/2, s.height/2) );
		label->setVisible(false);

		auto action = (ActionInterval*)Sequence::create
			(
				//Show::create(),
				DelayTime::create(0.5f),
				CallFunc::create(CC_CALLBACK_0(DefaultServerLayer::connectGameServer, this)),
				NULL
			);
		label->runAction(action);

		m_state = kStateConnectingGameServer;

		MyPlayerAIConfig::setAutomaticSkill(0);
		MyPlayerAIConfig::enableRobot(false);
	}
	else
	{
		onGetMinReq();
	}
}

void DefaultServerLayer::onGetMinReq()
{    
    // test 1
    {
		cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
        std::string url = Login::s_loginserver_ip;
        url.append(":48688/countryprofession");
        request->setUrl(url.c_str());
        request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
        request->setResponseCallback(this, httpresponse_selector(DefaultServerLayer::onGetMinResp));

		CountryProfessionReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);
		httpReq.set_serverid(m_pServerListRsp->serverinfos(mCurrentServerId).serverid());

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
		cocos2d::network::HttpClient::getInstance()->send(request);
        request->release();
    }
}

void DefaultServerLayer::onGetMinResp(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());

	CountryProfessionRsp countryProfessionRsp;
	CCAssert(response->getResponseData()->size() > 0, "should not be empty");
	countryProfessionRsp.ParseFromString(strdata);

	CCLOG("response.result = %d", countryProfessionRsp.result());

	if(1 == countryProfessionRsp.result())
	{
		LoginLayer::mCountryMin = countryProfessionRsp.country();
		LoginLayer::mProfessionMin = countryProfessionRsp.profession();

		auto pScene = new CreateRoleState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
	else
	{
		GameView::getInstance()->showAlertDialog(countryProfessionRsp.resultmessage());
	}
}

void DefaultServerLayer::requestServerList()
{    
    // test 1
    {
		cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
        std::string url = Login::s_loginserver_ip;
        url.append(":48688/serverlist");
        request->setUrl(url.c_str());
        request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
        request->setResponseCallback(this, httpresponse_selector(DefaultServerLayer::onRequestServerListCompleted));

		ServerListReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
		cocos2d::network::HttpClient::getInstance()->send(request);
        request->release();
    }
}

void DefaultServerLayer::onRequestServerListCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	m_pServerListRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pServerListRsp->result());
	/*
	CCLOG("m_pServerListRsp->serverinfos().roleid() = %d",m_pServerListRsp->serverinfos(0).roleid());
	CCLOG("response.serverinfos_size = %d", m_pServerListRsp->serverinfos_size());
	CCLOG("m_pServerListRsp->serverinfos(0).has_roleid() = %d",m_pServerListRsp->serverinfos(0).has_roleid());
	CCLOG("m_pServerListRsp->serverinfos(0).roleid() = %lld\n",m_pServerListRsp->serverinfos(0).roleid());*/
	if(1 == m_pServerListRsp->result())
	{
		reloadServerInfo();
	}
	else
	{
		//GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_server_list"));
	}
}

void DefaultServerLayer::reloadServerInfo()
{
	auto layer = (Layer*)getChildByTag(255);
	auto frame = (Button*)layer->getChildByTag(FRAME_TAG);
	ui::ImageView* status = (ui::ImageView*)frame->getChildByTag(STATUS_TAG);
	switch(m_pServerListRsp->serverinfos(mCurrentServerId).status())
	{
		case 1:
			status->loadTexture("res_ui/select_the_sercer/green_new.png");
			break;
		case 2:
			status->loadTexture("res_ui/select_the_sercer/yellow_new.png");
			break;
		case 3:
			status->loadTexture("res_ui/select_the_sercer/red_new.png");
			break;
		case 4:
			status->loadTexture("res_ui/select_the_sercer/gray_new.png");
			break;
		case 5:
			status->loadTexture("res_ui/select_the_sercer/gray_new.png");
			break;
		default:
			break;
	}
}

void DefaultServerLayer::update(float dt)
{
	switch(m_iStatus)
	{
	case kStateStart:
		struct timeval m_sStartTime;
		gettimeofday(&m_sStartTime, NULL);
		m_dStartTime = m_sStartTime.tv_sec*1000 + m_sStartTime.tv_usec/1000;

		m_iStatus = kStateWait;
		break;
	case kStateWait:
		struct timeval m_sEndedTime;
		gettimeofday(&m_sEndedTime, NULL);
		m_dEndedTime = m_sEndedTime.tv_sec*1000 + m_sEndedTime.tv_usec/1000;
		//wait 30 second to convert scene
		if(m_dEndedTime - m_dStartTime >= WAIT_TIME)
		{
			m_iStatus = kStateReload;
		}
		break;
	case kStateReload:
		requestServerList();
		m_iStatus = kStateStart;
		break;
	default:
		break;
	}
}

void DefaultServerLayer::connectGameServer()
{
	CCLOG("connectGameServer\n");
	if(ClientNetEngine::sharedSocketEngine()->getReadyState() == ClientNetEngine::kStateOpen)
		return;
	ClientNetEngine::sharedSocketEngine()->init(*(GameView::getInstance()));
}

void DefaultServerLayer::onSocketOpen(ClientNetEngine* socketEngine)
{
	// request to enter game
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1001, this);

	auto pScene = new GameSceneState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}
}
void DefaultServerLayer::onSocketClose(ClientNetEngine* socketEngine)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	// player can re-connect
	if(m_state == kStateConnectingGameServer)
	{
		this->getChildByTag(kTagPreparingBackGround)->removeFromParent();
		this->getChildByTag(kTagPreparingTip)->removeFromParent();
		// enable role list
		/*for(int i = 0; i < MAX_LOGIN_ROLE_NUM; i++)
		{
			Menu* menu = (Menu*)this->getChildByTag(kTagRoleListMenu+i);
			menu->setEnabled(true);
		}
		*/
		m_state = kStateRoleList;
	}
}
void DefaultServerLayer::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

///////////////////////////////////////////////////////////////////////////////////////

enum {
	kTagMainLayer = 1,
};

DefaultServerState::~DefaultServerState()
{
}

void DefaultServerState::runThisState()
{
    auto pLayer = new DefaultServerLayer();
    addChild(pLayer, 0, kTagMainLayer);
	pLayer->release();

	if(Director::getInstance()->getRunningScene() != NULL)
		Director::getInstance()->replaceScene(this);
	else
		Director::getInstance()->runWithScene(this);
}

void DefaultServerState::onSocketOpen(ClientNetEngine* socketEngine)
{
	auto layer = (DefaultServerLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketOpen(socketEngine);
}
void DefaultServerState::onSocketClose(ClientNetEngine* socketEngine)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	auto layer = (DefaultServerLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketClose(socketEngine);
}
void DefaultServerState::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}
