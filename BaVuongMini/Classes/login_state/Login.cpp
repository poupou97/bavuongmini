#include "Login.h"
#include <string>

#include "LoginState.h"
#include "../GameView.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "../messageclient/reqsender/ReqSenderProtocol.h"
#include "../ui/extensions/RichTextInput.h"
#include "SimpleAudioEngine.h"
#include "../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../GameUserDefault.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../utils/GameUtils.h"
#include "GameAudio.h"
#include "MoutainBackGroundLayer.h"

#include "../messageclient/GameMessageProcessor.h"
#include "../messageclient/ClientNetEngine.h"
#include "DefaultServer.h"

#define IS_CLEAR_ENABLE 0
#define CLIP_TIME 3.0f
#define DELAY_TIME 15.0f

using namespace CocosDenshion;

USING_NS_CC;
USING_NS_CC_EXT;

ServerListRsp* Login::m_pServerListRsp = new ServerListRsp();
long long Login::userId = 1;
string Login::sessionId = "";
//std::string Login::s_loginserver_ip = THREEKINGDOMS_LOGINSERVER_OFFICIAL;
std::string Login::s_loginserver_ip = THREEKINGDOMS_LOGINSERVER_STUDIO;

Login::Login() 
: m_labelStatusCode(NULL)
{
	//stop the backgroundmusic when the Socket Closed         by:liutao  2014-1-23
	//SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	SimpleAudioEngine::getInstance()->stopAllEffects();
	GameUtils::playGameSound(MUSIC_KING, 1, true);

    Size winSize = Director::getInstance()->getWinSize();

	GameView::getInstance()->ResetData();

    const int MARGIN = 40;
    const int SPACE = 35;

	auto pBackGround = MoutainBackGroundLayer::create();
	addChild(pBackGround);
	
// 	cocos2d::extension::Scale9Sprite* p = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80.png");
// 	p->setPosition(Vec2(winSize.width/2,winSize.height/2));
// 	p->setPreferredSize(winSize);
// 	p->setAnchorPoint(Vec2(0.5f, 0.5f));
// 	addChild(p);
	//-------------
	/*
	//gamelogo
	auto pSpriteLogo = Sprite::create("res_ui/select_the_sercer/gamelogo.png");
	pSpriteLogo->setPosition(Vec2(winSize.width/2+10, winSize.height/2+100));
	pSpriteLogo->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSpriteLogo->setScale(0.5f);
	pSpriteLogo->setOpacity(64);
	addChild(pSpriteLogo, 1);

	//yanjing
	auto pSpriteYanjing = Sprite::create("images/yanjing.png");
	pSpriteYanjing->setPosition(Vec2(pSpriteLogo->getContentSize().width/2+5, pSpriteLogo->getContentSize().height - 38));
	pSpriteYanjing->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSpriteYanjing->setScaleX(2.0f);
	pSpriteYanjing->setScaleY(1.16f);
	pSpriteLogo->addChild(pSpriteYanjing,1);

	auto  action1 = FadeTo::create(1.2f,30);
	CCDelayTime * delay_1 = DelayTime::create(1.5f);
	auto  action2 = FadeTo::create(1.2f,255);
	CCDelayTime * delay_2 = DelayTime::create(0.6f);
	RepeatForever * repeapAction = RepeatForever::create(Sequence::create(action1,delay_1,action2,delay_2,NULL));
	pSpriteYanjing->runAction(repeapAction);

	//runaction
	ActionInterval*  action_scale_and_fadein = Spawn::create(
		FadeTo::create(1.1f,255),
		ScaleTo::create(1.1f,1.0f),
		NULL);
	FiniteTimeAction*  gameLog_scale_1 = Sequence::create(
		//ScaleTo::create(1.5f,1.0f),
		action_scale_and_fadein,
		CallFuncN::create( this, callfuncN_selector(Login::gameLogRunAction) ), 
		NULL);
	pSpriteLogo->runAction(gameLog_scale_1);
	
	*/
	//-------------
	LoginState::addAppVersionInfo(this);

	// reset the delta time for smooth particle animation
	// because the previous scene is a loading scene, the delta time is stop when loading resource
	Director::getInstance()->setNextDeltaTimeZero(true);

	auto layer= Layer::create();

	auto enter = Button::create();
	enter->setAnchorPoint(Vec2(0.5f, 0.5f));
	enter->setTouchEnabled(true);
	enter->loadTextures("res_ui/creating_a_role/button_di.png", "res_ui/creating_a_role/button_di.png", "");
	enter->setPosition(Vec2(-60,-150));
	enter->addTouchEventListener(CC_CALLBACK_2(Login::onMenuGetLoginClicked, this));
	enter->setPressedActionEnabled(true);
	
	auto loginText = ImageView::create();
	loginText->loadTexture("res_ui/first/button_denglu.png");
	loginText->setAnchorPoint(Vec2(0.5f, 0.5f));
	loginText->setVisible(true);
	loginText->setPosition(Vec2(enter->getBoundingBox().size.width/2, enter->getBoundingBox().size.height / 2));
	enter->addChild(loginText);

	auto buttonRegister = Button::create();
	buttonRegister->setAnchorPoint(Vec2(0.5f, 0.5f));
	buttonRegister->setTouchEnabled(true);
	buttonRegister->loadTextures("res_ui/creating_a_role/button_di.png", "res_ui/creating_a_role/button_di.png", "");
	buttonRegister->setPosition(Vec2(60,-150));
	buttonRegister->addTouchEventListener(CC_CALLBACK_2(Login::onMenuGetRegisterClicked, this));
	buttonRegister->setPressedActionEnabled(true);
	
	auto registerImage = ImageView::create();
	registerImage->setAnchorPoint(Vec2(0.5f, 0.5f));
	registerImage->loadTexture("res_ui/first/button_zhuce.png");
	registerImage->setVisible(true);
	registerImage->setPosition(Vec2(buttonRegister->getBoundingBox().size.width / 2, buttonRegister->getBoundingBox().size.height / 2));
	
	buttonRegister->addChild(registerImage);

#if IS_CLEAR_ENABLE
	// clear account and password
	auto buttonClear = Button::create();
    buttonClear->setTouchEnabled(true);
    buttonClear->loadTextures("res_ui/new_button_7.png", "res_ui/new_button_7.png", "");
	buttonClear->setPosition(Vec2(150,-50));
	buttonClear->addTouchEventListener(CC_CALLBACK_2(Login::onMenuClear));
	buttonClear->setPressedActionEnabled(true);

	ImageView * deleteImage = ImageView::create();
	deleteImage->loadTexture("res_ui/liaotian/delete.png");
	deleteImage->setVisible(true);
	buttonClear->addChild(deleteImage);
	layer->addChild(buttonClear);
#endif

    layer->addChild(enter);
	layer->addChild(buttonRegister);
	layer->setPosition(Vec2(winSize.width/2,winSize.height/2));
	addChild(layer);

	auto shadowColor = Color4B::BLACK;   // black

	auto account = Label::createWithTTF("账号", APP_FONT_NAME, 20);
	account->enableShadow(shadowColor,Size(1.0f, -1.0f), 1.0f);
	account->setPosition(Vec2(-130,-30));
	account->setColor(Color3B(255,255,255));
	account->enableShadow(shadowColor, Size(2.0, -2.0), 0.5f);
	layer->addChild(account);

	auto password = Label::createWithTTF("密码", APP_FONT_NAME, 20);
	password->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
	password->setPosition(Vec2(-130,-80));
	password->setColor(Color3B(255,255,255));
	password->enableShadow(shadowColor, Size(2.0, -2.0), 0.5f);
	layer->addChild(password);
	
	auto inputBox=ui::ImageView::create();
	inputBox->loadTexture("res_ui/liaotian/shurukuang.png");
	inputBox->setAnchorPoint(Vec2(0.5f,0.5f));
	inputBox->setPosition(Vec2(0,-30));
	inputBox->setScale9Enabled(true);
	inputBox->setContentSize(Size(200,34));
	layer->addChild(inputBox);

	auto inputPin=ui::ImageView::create();
	inputPin->loadTexture("res_ui/liaotian/shurukuang.png");
	inputPin->setAnchorPoint(Vec2(0.5f,0.5f));
	inputPin->setPosition(Vec2(0,-80));
	inputPin->setScale9Enabled(true);
	inputPin->setContentSize(Size(200,34));
	layer->addChild(inputPin);
	
	pAccountInputBox = new RichTextInputBox();
	pAccountInputBox->setCharLimit(20);
	pAccountInputBox->setInputBoxWidth(200);
	
	pAccountInputBox->setPosition(Vec2(-100,-45));
	layer->addChild(pAccountInputBox);
	pAccountInputBox->autorelease();

	pPinInputBoxr = new RichTextInputBox();
	pPinInputBoxr->setCharLimit(20);
	pPinInputBoxr->setInputBoxWidth(200);
	
	pPinInputBoxr->setPosition(Vec2(-100,-95));
	layer->addChild(pPinInputBoxr);
	pPinInputBoxr->autorelease();
}

Login::~Login()
{
    //CCHttpClient::getInstance()->destroyInstance();
}

void Login::onEnter()
{
	Layer::onEnter();

	string account = UserDefault::getInstance()->getStringForKey("account");
	string password = UserDefault::getInstance()->getStringForKey("password");

	if(std::string() == pAccountInputBox->getInputString())
	{
		pAccountInputBox->onTextFieldInsertText(NULL, account.c_str(), account.size());
	}
	if(std::string() == pPinInputBoxr->getInputString())
	{
		pPinInputBoxr->onTextFieldInsertText(NULL, password.c_str(), password.size());
	}
}

void Login::onVersionCheck()
{    
    // test 1
    {
		cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
        std::string url = Login::s_loginserver_ip;
		//std::string url = "192.168.1.122";
        url.append(":48688/versioncheck");
        request->setUrl(url.c_str());
        request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
        request->setResponseCallback(this, httpresponse_selector(Login::onVersionCheckCompleted));

		VersionCheckReq httpReq;
		httpReq.set_clienttype("");
		httpReq.set_clientversion(GAME_CODE_VERSION);
		CCLOG(GAME_CODE_VERSION);
		int resVersion = UserDefault::getInstance()->getIntegerForKey(KEY_OF_RES_VERSION, GAME_RES_VERSION);
		httpReq.set_resversion(resVersion);
		httpReq.set_channelid(0);
		httpReq.set_batchid(0);
		httpReq.set_recommendid(0);
		httpReq.set_extensionfield("");
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
		httpReq.set_platform("");
#else
		httpReq.set_platform(OPERATION_PLATFORM);
#endif

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
		cocos2d::network::HttpClient::getInstance()->send(request);
        request->release();
    }
}

void Login::onVersionCheckCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());

        return;
    }
    
	VersionCheckRsp versionCheckRsp;
	if(response->getResponseData()->size() <= 0)
	{
		versionCheckRsp.set_result(1);   // server no response, so the default value is 1, indicate that there is no update
	}
	else
	{
		//CCAssert(response->getResponseData()->size() > 0, "should not be empty");
		versionCheckRsp.ParseFromString(response->getResponseData()->data());

		CCLOG("response.result = %d", versionCheckRsp.result());
	}

	int result = versionCheckRsp.result();
	if(1 == result||3 == result)
	{
		onMenuGetTestClicked();
	}
	else if(2 == result)
	{
		GameView::getInstance()->showAlertDialog("请下载最新客户端");
	}
}

void Login::onMenuGetTestClicked()
{    
    // test 1
    {
		cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
        std::string url = s_loginserver_ip;
        url.append(":48688/serverlist");
        request->setUrl(url.c_str());
        request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
        request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestCompleted));

		ServerListReq httpReq;
		httpReq.set_userid(userId);
		httpReq.set_sessionid(sessionId);

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
        request->setTag("POST");
		//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
		cocos2d::network::HttpClient::getInstance()->send(request);
        request->release();
    }
}

void Login::onHttpRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	m_pServerListRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pServerListRsp->result());
	/*
	CCLOG("m_pServerListRsp->serverinfos().roleid() = %d",m_pServerListRsp->serverinfos(0).roleid());
	CCLOG("response.serverinfos_size = %d", m_pServerListRsp->serverinfos_size());
	CCLOG("m_pServerListRsp->serverinfos(0).has_roleid() = %d",m_pServerListRsp->serverinfos(0).has_roleid());
	CCLOG("m_pServerListRsp->serverinfos(0).roleid() = %lld\n",m_pServerListRsp->serverinfos(0).roleid());*/
	if(1 == m_pServerListRsp->result())
	{
		GameView::getInstance()->registerNoticeIsShow = true;

		GameView::getInstance()->registerNoticeVector.clear();

		GameView::getInstance()->registerNoticeVector.push_back(m_pServerListRsp->loginnotice());

		auto pScene = new DefaultServerState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
	else
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_server_list"));
	}
}

void Login::onMenuGetLoginClicked(Ref *pSender, Widget::TouchEventType type)
{    

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (nullptr == pAccountInputBox->getInputString())
		{
			GameView::getInstance()->showAlertDialog("帐号不能为空");
			return;
		}
		else
		{
			UserDefault::getInstance()->setStringForKey("account", pAccountInputBox->getInputString());
		}
		if (nullptr == pPinInputBoxr->getInputString())
		{
			GameView::getInstance()->showAlertDialog("密码不能为空");
			return;
		}
		else
		{
			UserDefault::getInstance()->setStringForKey("password", pPinInputBoxr->getInputString());
		}

		// test 1
		{
			cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
			std::string url = s_loginserver_ip;
			url.append(":48688/login");
			request->setUrl(url.c_str());
			request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
			request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestLogin));

			std::string strUniqueId = GameUtils::getAppUniqueID();

			LoginReq httpReq;
			httpReq.set_account(pAccountInputBox->getInputString());
			httpReq.set_authenticid(pPinInputBoxr->getInputString());
			//useless element
			httpReq.set_accountnum(0);
			httpReq.set_accountpt("test");
			httpReq.set_imei(strUniqueId);
			httpReq.set_ua(strUniqueId);
			httpReq.set_channelid(LoginReq::QIANLIANG);
			httpReq.set_batchid(0);
			httpReq.set_recommendid(0);
			httpReq.set_systemversion("test");
			httpReq.set_nettype(0);
			httpReq.set_apkwithres(0);
			httpReq.set_platformuserid("test");

			string msgData;
			httpReq.SerializeToString(&msgData);

			request->setRequestData(msgData.c_str(), msgData.size());
			request->setTag("POST");
			//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
			cocos2d::network::HttpClient::getInstance()->send(request);
			request->release();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void Login::onHttpRequestLogin(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	auto m_pRegisterRsp = new LoginRsp();
	m_pRegisterRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pRegisterRsp->result());

	if(1 == m_pRegisterRsp->result())
	{
		userId = m_pRegisterRsp->userid();
		sessionId = m_pRegisterRsp->sessionid();
		onVersionCheck();
	}
	else
	{
		GameView::getInstance()->showAlertDialog(m_pRegisterRsp->resultmessage());
	}
}


void Login::onMenuClear(cocos2d::Ref *sender)
{
	pAccountInputBox->deleteAllInputString();
	pPinInputBoxr->deleteAllInputString();
}

void Login::onMenuGetRegisterClicked(Ref *pSender, Widget::TouchEventType type)
{    

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		// test 1
		{
			cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
			std::string url = s_loginserver_ip;
			url.append(":48688/auth_regist");
			request->setUrl(url.c_str());
			request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
			request->setResponseCallback(this, httpresponse_selector(Login::onHttpRequestRegister));

			AuthRegistReq httpReq;
			httpReq.set_account(pAccountInputBox->getInputString());
			httpReq.set_password(pPinInputBoxr->getInputString());

			string msgData;
			httpReq.SerializeToString(&msgData);

			request->setRequestData(msgData.c_str(), msgData.size());
			request->setTag("POST");
			//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
			cocos2d::network::HttpClient::getInstance()->send(request);
			request->release();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	if(NULL == pAccountInputBox->getInputString())
	{
        GameView::getInstance()->showAlertDialog("帐号不能为空");
		return;
	}
	if(NULL == pPinInputBoxr->getInputString())
	{
        GameView::getInstance()->showAlertDialog("密码不能为空");
		return;
	}
    
}

void Login::onHttpRequestRegister(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }
    
    // dump data
	std::string strdata;
    strdata.assign(response->getResponseData()->data(), response->getResponseData()->size());
	auto m_pLoginRsp = new AuthRegistRsp();
	m_pLoginRsp->ParseFromString(strdata);

	CCLOG("response.result = %d", m_pLoginRsp->result());

	if(1 == m_pLoginRsp->result())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_register_success"));
	}
	else
	{
		GameView::getInstance()->showAlertDialog(m_pLoginRsp->resultmessage());
	}
}


void Login::getServerListInfo(ServerListRsp* httpRsp)
{
	*httpRsp = *m_pServerListRsp;
}

void Login::onSocketOpen(ClientNetEngine* socketEngine)
{
}
void Login::onSocketClose(ClientNetEngine* socketEngine)
{
}
void Login::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

void Login::gameLogRunAction(Node* pTarget)
{
	Size winSize = Director::getInstance()->getWinSize();

// 	auto pSpriteLogo_copy = Sprite::create("res_ui/select_the_sercer/gamelogo.png");
// 	pSpriteLogo_copy->setPosition(Vec2(winSize.width/2+10, winSize.height/2+100));
// 	pSpriteLogo_copy->setAnchorPoint(Vec2(0.5f, 0.5f));
// 	addChild(pSpriteLogo_copy, 1);
// 
// 	CCDelayTime * gameLogCopy_delay_1 = DelayTime::create(1.0f);
// 	Action*  action_ss = Spawn::create(
// 		FadeTo::create(1.0f,0),
// 		ScaleTo::create(1.0f,1.3f),
// 		NULL);
// 
// 	Sequence * gameLog_seq = Sequence::create(gameLogCopy_delay_1,action_ss,NULL);
// 	pSpriteLogo_copy->runAction(action_ss);

	auto clip = ClippingNode::create();//创建裁剪节点
	auto gameTitle = Sprite::create("res_ui/select_the_sercer/gamelogo_text_mask.png");
	clip->setStencil(gameTitle);//设置裁剪模板
	clip->setAlphaThreshold(0.0f);//设置透明度阈值
	//clip->setInverted(true);
	clip->setContentSize(Size(gameTitle->getContentSize().width,gameTitle->getContentSize().height));//设置裁剪节点大小    
	Size clipSize = clip->getContentSize();//获取裁剪节点大小
	clip->setPosition(Vec2(winSize.width/2+10,winSize.height/2+100));//设置裁剪节点位置


// 	//auto gameTitle_show = Sprite::create("res_ui/select_the_sercer/gamelogo.png");//创建要显示的对象
// 	auto spark = Sprite::create("images/guangtiao.png");//创建闪亮精灵
// 	//clip->addChild(gameTitle_show,2);//把要显示的内容放在裁剪节点中，其实可以直接clip->addChild(gameTitle,1);此处为了说明更改显示内容
// 	spark->setPosition(Vec2(-winSize.width/2, 0));//设置闪亮精灵位置
// 	clip->addChild(spark,3);//添加闪亮精灵到裁剪节点
// 	addChild(clip,1);//添加裁剪节点
// 
// 	MoveTo* moveAction = MoveTo::create(CLIP_TIME, Vec2(clipSize.width, 0));//创建精灵节点的动作
// 	MoveTo* moveBack = MoveTo::create(0, Vec2(-clipSize.width, 0));
// 	CCDelayTime* delayTime = DelayTime::create(DELAY_TIME);
// 	Sequence* seq = Sequence::create(moveAction, delayTime, moveBack, NULL);
// 	RepeatForever* repreatAction = RepeatForever::create(seq);
// 	spark->runAction(repreatAction);//精灵节点重复执行
}


/*
void runLogin()
{
    Scene *pScene = Scene::create();
    Login *pLayer = new Login();
    pScene->addChild(pLayer);
    
    Director::getInstance()->replaceScene(pScene);
    pLayer->release();
}
*/

enum {
	kTagMainLayer = 1,
};

LoginGameState::~LoginGameState()
{
}

void LoginGameState::runThisState()
{
	auto pLayer = new Login();
	addChild(pLayer, 0, kTagMainLayer);
	pLayer->release();

	if(Director::getInstance()->getRunningScene() != NULL)
		Director::getInstance()->replaceScene(this);
	else
		Director::getInstance()->runWithScene(this);
}

void LoginGameState::onSocketOpen(ClientNetEngine* socketEngine)
{
}
void LoginGameState::onSocketClose(ClientNetEngine* socketEngine)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	auto layer = (Login*)this->getChildByTag(kTagMainLayer);
	layer->onSocketClose(socketEngine);
}
void LoginGameState::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}
