#include "SpriteBlur.h"

SpriteBlur::~SpriteBlur()
{
    CCNotificationCenter::getInstance()->removeObserver(this, EVENT_COME_TO_FOREGROUND);
}

SpriteBlur* SpriteBlur::create(const char *pszFileName)
{
    auto pRet = new SpriteBlur();
    if (pRet && pRet->initWithFile(pszFileName))
    {
        pRet->autorelease();
    }
    else
    {
        CC_SAFE_DELETE(pRet);
    }
    
    return pRet;
}

void SpriteBlur::listenBackToForeground(Ref *obj)
{
	setGLProgram(NULL);
    initProgram();
}

bool SpriteBlur::initWithTexture(Texture2D* texture, const Rect& rect)
{
    if( Sprite::initWithTexture(texture, rect) ) 
    {
        CCNotificationCenter::getInstance()->addObserver(this,
                                                                      callfuncO_selector(SpriteBlur::listenBackToForeground),
                                                                      EVENT_COME_TO_FOREGROUND,
                                                                      NULL);
        
        Size s = getTexture()->getContentSizeInPixels();

        blur_ = Vec2(1/s.width, 1/s.height);
        sub_[0] = sub_[1] = sub_[2] = sub_[3] = 0;

        this->initProgram();
        
        return true;
    }

    return false;
}

void SpriteBlur::initProgram()
{
    auto fragSource = (GLchar*) __String::createWithContentsOfFile(
                                FileUtils::getInstance()->fullPathForFilename("blur.fsh").c_str())->getCString();
    auto pProgram = new GLProgram();
    pProgram->initWithByteArrays(ccPositionTextureColor_vert, fragSource);
	setGLProgram(pProgram);
    pProgram->release();
    
    CHECK_GL_ERROR_DEBUG();
    
	getGLProgram()->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
	getGLProgram()->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_COLOR, GLProgram::VERTEX_ATTRIB_COLOR);
	getGLProgram()->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
    
    CHECK_GL_ERROR_DEBUG();
    
	getGLProgram()->link();
    
    CHECK_GL_ERROR_DEBUG();
    
	getGLProgram()->updateUniforms();
    
    CHECK_GL_ERROR_DEBUG();
    
    subLocation = glGetUniformLocation(getGLProgram()->getProgram(), "substract");
    blurLocation = glGetUniformLocation(getGLProgram()->getProgram(), "blurSize");
    
    CHECK_GL_ERROR_DEBUG();
}

void SpriteBlur::draw(Renderer *renderer, const Mat4& transform, uint32_t flags)
{
	GL::enableVertexAttribs(GL::VERTEX_ATTRIB_FLAG_POS_COLOR_TEX);
	BlendFunc blend = getBlendFunc();
	GL::blendFunc(blend.src, blend.dst);

	getGLProgram()->use();
	getGLProgram()->setUniformsForBuiltins();
	getGLProgram()->setUniformLocationWith2f(blurLocation, blur_.x, blur_.y);
	getGLProgram()->setUniformLocationWith4fv(subLocation, sub_, 1);

	GL::bindTexture2D( getTexture()->getName());

    //
    // Attributes
    //
#define kQuadSize sizeof(_quad.bl)
    long offset = (long)&_quad;

    // vertex
    int diff = offsetof( V3F_C4B_T2F, vertices);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_POSITION, 3, GL_FLOAT, GL_FALSE, kQuadSize, (void*) (offset + diff));

    // texCoods
    diff = offsetof( V3F_C4B_T2F, texCoords);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_TEX_COORD, 2, GL_FLOAT, GL_FALSE, kQuadSize, (void*)(offset + diff));

    // color
    diff = offsetof( V3F_C4B_T2F, colors);
    glVertexAttribPointer(GLProgram::VERTEX_ATTRIB_COLOR, 4, GL_UNSIGNED_BYTE, GL_TRUE, kQuadSize, (void*)(offset + diff));


    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

    CC_INCREMENT_GL_DRAWS(1);
}

void SpriteBlur::setBlurSize(float f)
{
    Size s = getTexture()->getContentSizeInPixels();

    blur_ = Vec2(1/s.width, 1/s.height);
    blur_ = blur_*f;
}
