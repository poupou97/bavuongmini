#include "CreateRole.h"
#include "SpriteBlur.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "../gamescene_state/GameSceneState.h"
#include "../loadscene_state/LoadSceneState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../legend_engine/LegendLevel.h"
#include "../ui/extensions/CCRichLabel.h"
#include "../ui/extensions/RichElement.h"

#include "../utils/GameUtils.h"

#include "../messageclient/GameMessageProcessor.h"
#include "../messageclient/ClientNetEngine.h"

#include "../ui/extensions/RichTextInput.h"
#include "../GameView.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "Login.h"
#include "LoginState.h"
#include "../ui/country_ui/CountryUI.h"
#include "../utils/StaticDataManager.h"
#include "../ui/Chat_ui/ChatCell.h"
#include "../GameUserDefault.h"
#include "../utils/GameUtils.h"
#include "../legend_script/CCTutorialParticle.h"
#include "../login_state/MoutainBackGroundLayer.h"
#include "../login_state/DefaultServer.h"

//#include "share_sdk/ShareCreateRole.h"

#else

#include "../gamescene_state/GameSceneState.h"
#include "../loadscene_state/LoadSceneState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../legend_engine/LegendLevel.h"
#include "../ui/extensions/CCRichLabel.h"
#include "../ui/extensions/RichElement.h"

#include "../utils/GameUtils.h"

#include "../messageclient/GameMessageProcessor.h"
#include "../messageclient/ClientNetEngine.h"

#include "../ui/extensions/RichTextInput.h"
#include "../GameView.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "Login.h"
#include "LoginState.h"
#include "../ui/country_ui/CountryUI.h"
#include "../utils/StaticDataManager.h"
#include "../ui/Chat_ui/ChatCell.h"
#include "../GameUserDefault.h"
#include "../utils/GameUtils.h"
#include "../legend_script/CCTutorialParticle.h"
#include "MoutainBackGroundLayer.h"
#include "DefaultServer.h"

#endif

#define SPRITE_MENGJIANG "res_ui/creating_a_role/mengjiang.png"
#define SPRITE_GUIMOU "res_ui/creating_a_role/guimou.png"
#define SPRITE_HAOJIE "res_ui/creating_a_role/haojie.png"
#define SPRITE_SHENSHE "res_ui/creating_a_role/shenshe.png"
#define PROFESSION_MENGJIANG "res_ui/creating_a_role/mengjiang1.png"
#define PROFESSION_GUIMOU "res_ui/creating_a_role/guimou1.png"
#define PROFESSION_HAOJIE "res_ui/creating_a_role/haojie1.png"
#define PROFESSION_SHENSHE "res_ui/creating_a_role/shenshe1.png"
#define INTRODUCTION_MENGJIANG "res_ui/creating_a_role/01-shi.png"
#define INTRODUCTION_GUIMOU "res_ui/creating_a_role/04-shi.png"
#define INTRODUCTION_HAOJIE "res_ui/creating_a_role/02-shi.png"
#define INTRODUCTION_SHENSHE "res_ui/creating_a_role/03-shi.png"
#define SPRITE_TABLE "res_ui/creating_a_role/taizi_a.png"
#define SPRITE_TABLE_LIGHT "res_ui/creating_a_role/taizi_b.png"
#define FRAME_NONE "res_ui/round_none.png"
#define FRAME_GREEN "res_ui/round_green.png"

#define ROLECOLOR 64,64,64
#define ACTION_TAG 77
#define PROFESSION_ICON 100
#define SPRITE_ICON_TAG 13
#define SPRITE_FRONT_TAG 200
#define RUNACTION_TIME 0.2f
#define DYNAMIC_EFFECT_FADETO 1.1f
#define SPRITE_OPACITY 200
#define INTRODUCTION_TIME 0.2f
#define DELAY_TIME 0

#define COMMON_TAG 9996
#define COUNTRY_TAG 9999
#define LAYER_TAG 9998
#define PROMPT_LOG_TAG 9997
#define FRONT_TAG 800
#define MIDDLE_TAG 500
#define BACK_TAG 201
#define COUNTRY_1_TAG 1
#define COUNTRY_2_TAG 2
#define COUNTRY_3_TAG 3
#define HIGHLIGHT_TAG 55
#define BLUR_LEFT_TAG 60
#define BLUR_RIGHT_TAG 61
#define TABLE_LEFT_TAG 71
#define TABLE_RIGHT_TAG 72
#define ARROW_LEFT 81
#define ARROW_RIGHT 82

#define CCTUTORIALPARTICLE_TAG 56
#define TABLE_TAG 57
#define TABLE_LIGHT_TAG 58

#define IS_CLEAR_ENABLE 0
#define HIGHLIGHT_TRANS_TIME 0.5f
#define TABLE_LIGHT_TRANS_TIME 1.0f
#define ICON_SCALE_MAX 3.0f

#define OFFSET_SPRITE_MIDDLE 240
#define OFFSET_WIDTH 215

enum {
	kStateRoleList = 0,
	kStateConnectingGameServer,
};

enum {
	kTagPreparingBackGround = 2,
	kTagPreparingTip = 3,
	kTagServerTip = 4,
	kTagRoleListMenu = 16,   // last one !! please add more tag before it
};

enum
{
    kTagSprite1 = 1,
	kTagSprite2 = 2,
	kTagSprite3 = 3,
	kTagSprite4 = 4,
};

enum
{
	kTagStatus1 = 1,
	kTagStatus2 = 2,
	kTagStatus3 = 3,
	kTagStatus4 = 4,
};

enum
{
	kTagTouchedNot = 0,
	kTagTouchedLeft = 1,
	kTagTouchedRight = 2,
};

enum
{
	kStateTouchedNone = 0,
	kStateTouchedLeft = 1,
	kStateTouchedRight = 2,
};

CreateRoleLayer::CreateRoleLayer()
: m_state(kStateRoleList)
, mCurrentServerId(0)
,m_tBeginPos(Vec2::ZERO)
,mSpriteStatus(LoginLayer::mProfessionMin)
,mCountryFlag(LoginLayer::mCountryMin)
,isTouchSlipped(kTagTouchedNot)
,mZOrder(1)
,isRunActionTwice(false)
,m_iTouchedState(kStateTouchedNone)
{
	////setTouchEnabled(true);
	scheduleUpdate();

    Size s = Director::getInstance()->getVisibleSize();
	
	m_pServerListRsp = new ServerListRsp();
	Login::getServerListInfo(m_pServerListRsp);

	m_pRandomNameRsp = new RandomNameRsp();

	mCurrentServerId = LoginLayer::getCurrentServerId();

	auto pBackGround = MoutainBackGroundLayer::create();
	pBackGround->setBackGroundColor(Color3B(255, 255, 255));
	addChild(pBackGround);
	
	auto mengban = cocos2d::extension::Scale9Sprite::create("newcommerstory/zhezhao.png");
	mengban->setPreferredSize(s);
	mengban->setAnchorPoint(Vec2(0,0));
	mengban->setPosition(Vec2(0,0));
	mengban->setOpacity(150);
	addChild(mengban);
		
	//ParticleSystem* particle = ParticleSystemQuad::create("animation/texiao/particledesigner/xin1.plist");
	//particle->setPositionType(ParticleSystem::PositionType::FREE);
	//particle->setPosition(Vec2(s.width/2-40, s.height/2-120));
	//particle->setAnchorPoint(Vec2(0.5f,0.5f));
	//particle->setVisible(true);
	////particleFire->setLife(0.5f);
	//particle->setLifeVar(0);
	//particle->setScale(0.7f);
	//addChild(particle, COMMON_TAG);

	//ParticleSystem* particle1 = ParticleSystemQuad::create("animation/texiao/particledesigner/xin1.plist");
	//particle1->setPositionType(ParticleSystem::PositionType::FREE);
	//particle1->setPosition(Vec2(s.width/2+40, s.height/2-120));
	//particle1->setAnchorPoint(Vec2(0.5f,0.5f));
	//particle1->setVisible(true);
	////particleFire->setLife(0.5f);
	//particle1->setLifeVar(0);
	//particle1->setScale(0.7f);
	//addChild(particle1, COMMON_TAG);
	//
	auto layer= Layer::create();
	addChild(layer, LAYER_TAG, 255);

	initProfessionSprite(mSpriteStatus);

	//initCountrySprite(mCountryFlag);

	initProfessionIcon(mSpriteStatus);
	
	auto arrowRight = Button::create();
    arrowRight->setTouchEnabled(true);
    arrowRight->loadTextures("res_ui/creating_a_role/arrow01.png", "res_ui/creating_a_role/arrow01.png", "");
	arrowRight->setAnchorPoint(Vec2(0.5f,0.5f));
	arrowRight->setPosition(Vec2(s.width-25, s.height/2));
	arrowRight->setTag(ARROW_RIGHT);
	arrowRight->addTouchEventListener(CC_CALLBACK_2(CreateRoleLayer::onClickedArrow, this));
	arrowRight->setPressedActionEnabled(true);
	layer->addChild(arrowRight);
	
	auto arrowLeft = Button::create();
    arrowLeft->setTouchEnabled(true);
    arrowLeft->loadTextures("res_ui/creating_a_role/arrow02.png", "res_ui/creating_a_role/arrow02.png", "");
	arrowLeft->setAnchorPoint(Vec2(0.5f,0.5f));
	arrowLeft->setPosition(Vec2(25, s.height/2));
	//arrowLeft->setRotation(180);
	//arrowLeft->setScaleY(-1.0f);
	arrowLeft->setTag(ARROW_LEFT);
	arrowLeft->addTouchEventListener(CC_CALLBACK_2(CreateRoleLayer::onClickedArrow, this));
	arrowLeft->setPressedActionEnabled(true);
	layer->addChild(arrowLeft);
		
	auto inputBox=ui::ImageView::create();
	inputBox->loadTexture("res_ui/creating_a_role/di.png");
	inputBox->setAnchorPoint(Vec2(0.5f,0.5f));
	inputBox->setPosition(Vec2(s.width/2,25));
	layer->addChild(inputBox);
	
	//// "role name"
	//auto inputName=ui::ImageView::create();
	//inputName->loadTexture("res_ui/creating_a_role/name_di.png");
	//inputName->setAnchorPoint(Vec2(0.5f,0.5f));
	//inputName->setPosition(Vec2(s.width/2-115,30));
	//inputName->setScale(0.8f);
	//layer->addChild(inputName);

// 	Label * Label_name = Label::create();
// 	Label_name->setAnchorPoint(Vec2(0.5f,0.5f));
// 	Label_name->setPosition(Vec2(s.width/2-125,30));
// 	Label_name->setText(StringDataManager::getString("createUI_roleName"));
// 	Label_name->setFntFile("res_ui/font/ziti_1.fnt");
// 	layer->addChild(Label_name);

	auto randomName = Button::create();
    randomName->setTouchEnabled(true);
    randomName->loadTextures("res_ui/creating_a_role/button1up.png", "res_ui/creating_a_role/button1up.png", "");
	randomName->setPosition(Vec2(s.width/2+115,23));
	//randomName->setScale(1.5f);
	randomName->addTouchEventListener(CC_CALLBACK_2(CreateRoleLayer::onTouchedRandomName, this));
	randomName->setPressedActionEnabled(true);

	pNameLayer = new RichTextInputBox();
	pNameLayer->setCharLimit(5);
	pNameLayer->setInputBoxWidth(155);
	
	pNameLayer->setPosition(Vec2(s.width/2-50,10));
	layer->addChild(pNameLayer);
	pNameLayer->autorelease();

	auto enter = Button::create();
    enter->setTouchEnabled(true);
    enter->loadTextures("res_ui/creating_a_role/button_di2.png", "res_ui/creating_a_role/button_di2.png", "");
	enter->setPosition(Vec2(s.width-80, 80));
	enter->addTouchEventListener(CC_CALLBACK_2(CreateRoleLayer::menuSelectRoleCallback, this));
	enter->setPressedActionEnabled(true);
	////
	//CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("xuanzhuan.plist",48,72);
	//tutorialParticle->setPosition(Vec2(0, -36));
	//tutorialParticle->setAnchorPoint(Vec2(0.5f, 0.5f));
	//enter->addChild(tutorialParticle);
	//
	auto enterText = ui::ImageView::create();
	enterText->loadTexture("res_ui/creating_a_role/enter_game.png");
	enterText->setVisible(true);
	enter->addChild(enterText);

	auto back = Button::create();
    back->setTouchEnabled(true);
    back->loadTextures("res_ui/creating_a_role/button_di.png", "res_ui/creating_a_role/button_di.png", "");
	back->setPosition(Vec2(80, 80));
	back->addTouchEventListener(CC_CALLBACK_2(CreateRoleLayer::onButtonGetClicked, this));
	back->setPressedActionEnabled(true);
	
	auto backText = ui::ImageView::create();
	backText->loadTexture("res_ui/creating_a_role/back.png");
	backText->setVisible(true);
	back->addChild(backText);

	layer->addChild(randomName);
	//layer->addChild(country);
	layer->addChild(enter);
	layer->addChild(back);
	layer->setPosition(Vec2(0,0));

	onTouchedRandomName(NULL, Widget::TouchEventType::ENDED);

	//ClientNetEngine::sharedSocketEngine()->setAddress(THREEKINGDOMS_HOST_STUDIO, THREEKINGDOMS_PORT_STUDIO);
}

CreateRoleLayer::~CreateRoleLayer()
{
	delete m_pServerListRsp;
	delete  m_pRandomNameRsp;
}

void CreateRoleLayer::updateCountryButton(int country)
{
	auto layer = (Layer*)getChildByTag(255);
	for(int i = 1; i <= 3; i++)
	{
		auto btn = (Button *)layer->getChildByTag(i);
		auto highlightframe = (ui::ImageView*)btn->getChildByTag(HIGHLIGHT_TAG);
		if(i == country)
		{
			btn->setColor(Color3B(255, 255, 255));
			highlightframe->setVisible(true);
		}
		else
		{
			btn->setColor(Color3B(ROLECOLOR));
			highlightframe->setVisible(false);
		}
	}
}

/*
void CreateRoleLayer::initCountrySprite(int minCountry)
{
	Size s = Director::getInstance()->getVisibleSize();

	auto pSpriteText = Sprite::create();
	pSpriteText->setPosition(Vec2(70, s.height-280));
	//pSpriteText->setScale(0.9f);
	pSpriteText->setAnchorPoint(Vec2(0.5f, 0.5f));
	addChild(pSpriteText, 1);

	switch(minCountry)
	{
	case 1:
		pSpriteText->initWithFile("res_ui/creating_a_role/yizhou.png");
		break;
	case 2:
		pSpriteText->initWithFile("res_ui/creating_a_role/yangzhou.png");
		break;
	case 3:
		pSpriteText->initWithFile("res_ui/creating_a_role/jingzhou.png");
		break;
	case 4:
		pSpriteText->initWithFile("res_ui/creating_a_role/youzhou.png");
		break;
	case 5:
		pSpriteText->initWithFile("res_ui/creating_a_role/liangzhou.png");
		break;
	default:
		break;
	}
}
*/
void CreateRoleLayer::spriteRunAction(float time, int mode)
{
	Size size = Director::getInstance()->getVisibleSize();
	
	auto sprite1 = (SpriteBlur*)getChildByTag(kTagSprite1);
	auto table1 = (SpriteBlur*)sprite1->getChildByTag(TABLE_TAG);
	sprite1->setVisible(true);
	sprite1->setBlurSize(0);
	table1->setBlurSize(0);
	auto sprite2 = (SpriteBlur*)getChildByTag(kTagSprite2);
	auto table2 = (SpriteBlur*)sprite2->getChildByTag(TABLE_TAG);
	sprite2->setVisible(true);
	sprite2->setBlurSize(0);
	table2->setBlurSize(0);
	auto sprite3 = (SpriteBlur*)getChildByTag(kTagSprite3);
	auto table3 = (SpriteBlur*)sprite3->getChildByTag(TABLE_TAG);
	sprite3->setVisible(true);
	sprite3->setBlurSize(0);
	table3->setBlurSize(0);
	auto sprite4 = (SpriteBlur*)getChildByTag(kTagSprite4);
	auto table4 = (SpriteBlur*)sprite4->getChildByTag(TABLE_TAG);
	sprite4->setVisible(true);
	sprite4->setBlurSize(0);
	table4->setBlurSize(0);
	auto profession1 = getChildByTag(5);
	auto profession2 = getChildByTag(6);
	auto profession3 = getChildByTag(7);
	auto profession4 = getChildByTag(8);
	auto introduction1 = getChildByTag(9);
	auto introduction2 = getChildByTag(10);
	auto introduction3 = getChildByTag(11);
	auto introduction4 = getChildByTag(12);

	hideBlurSprite();
	
	auto iconAction = Spawn::create(
								Sequence::create(
									MoveTo::create(0, Vec2(size.width, size.height)),
									MoveTo::create(INTRODUCTION_TIME, Vec2(size.width/2+110, size.height/2+100)),
									NULL),
								Sequence::create(
									ScaleTo::create(0, ICON_SCALE_MAX), 
									ScaleTo::create(INTRODUCTION_TIME, 0.7f), 
									//ScaleTo::create(0.1f, 0.9f),
									//ScaleTo::create(0.1f, 0.8f),
									NULL),
								/*Sequence::create(
									CCSkewTo::create(0.1f, 90.0f, 0), 
									CCSkewTo::create(0.5f, 0, 0), 
									NULL),*/
								NULL);
	
	auto actionIntro = Spawn::create(
								Sequence::create(
									DelayTime::create(DELAY_TIME),
									MoveTo::create(0, Vec2(size.width, 0)),
									MoveTo::create(INTRODUCTION_TIME, Vec2(size.width/2+110, size.height/2-50)),
									NULL),
								Sequence::create(
									DelayTime::create(DELAY_TIME),
									ScaleTo::create(0, ICON_SCALE_MAX), 
									ScaleTo::create(INTRODUCTION_TIME, 0.9f), 
									//ScaleTo::create(0.1f, 0.9f),
									//ScaleTo::create(0.1f, 0.8f),
									NULL),
								/*Sequence::create(
									CCSkewTo::create(0.1f, 90.0f, 0), 
									CCSkewTo::create(0.5f, 0, 0), 
									NULL),*/
								NULL);

	//auto iconAction = Spawn::create(
	//						Sequence::create(
	//							ScaleTo::create(0, ICON_SCALE_MAX), 
	//							ScaleTo::create(0.1f, 0.7f), 
	//							//ScaleTo::create(0.1f, 0.9f),
	//							//ScaleTo::create(0.1f, 0.8f),
	//							NULL),
	//						/*Sequence::create(
	//							CCSkewTo::create(0.1f, 90.0f, 0), 
	//							CCSkewTo::create(0.5f, 0, 0), 
	//							NULL),*/
	//						NULL);
	//
	//auto introductionAction = Sequence::create(
	//									MoveTo::create(0, Vec2(size.width+20, size.height/2+20)), 
	//									MoveTo::create(0.2f, Vec2(size.width-90, size.height/2+20)), 
	//									MoveTo::create(0.1f, Vec2(size.width-60, size.height/2+20)),
	//									MoveTo::create(0.1f, Vec2(size.width-70, size.height/2+20)),
	//									NULL);

	if(mode == kTagTouchedRight)
	{
		auto action = (ActionInterval*)Spawn::create(
					MoveTo::create(time, Vec2(size.width/2+OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, size.height/2+80) ) ,
					FadeTo::create(time, SPRITE_OPACITY),
					ScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL);
		action->setTag(ACTION_TAG);
		switch(mSpriteStatus)
		{
			case 1:
				sprite1->runAction(action);
				sprite4->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_WIDTH, size.height/2) ) ,
					FadeTo::create(time, 255),
					ScaleTo::create(time, 1.0f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite2->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2, size.height/2+120) ) ,
					FadeTo::create(time, 10),
					ScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite3->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, size.height/2+80) ) ,
					FadeTo::create(time, SPRITE_OPACITY),
					ScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));

				profession4->runAction(iconAction);
				introduction4->runAction(actionIntro);
				profession1->setVisible(false);
				profession2->setVisible(false);
				profession3->setVisible(false);
				profession4->setVisible(true);
				introduction1->setVisible(false);
				introduction2->setVisible(false);
				introduction3->setVisible(false);
				introduction4->setVisible(true);
				
				break;
			case 2:
				sprite2->runAction(action);
				sprite1->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_WIDTH, size.height/2) ) ,
					FadeTo::create(time, 255),
					ScaleTo::create(time, 1.0f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite3->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2, size.height/2+120) ) ,
					FadeTo::create(time, 10),
					ScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite4->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, size.height/2+80) ) ,
					FadeTo::create(time, SPRITE_OPACITY),
					ScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));

				profession1->runAction(iconAction);
				introduction1->runAction(actionIntro);
				profession1->setVisible(true);
				profession2->setVisible(false);
				profession3->setVisible(false);
				profession4->setVisible(false);
				introduction1->setVisible(true);
				introduction2->setVisible(false);
				introduction3->setVisible(false);
				introduction4->setVisible(false);
				
				break;
			case 3:
				sprite3->runAction(action);
				sprite2->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_WIDTH, size.height/2) ) ,
					FadeTo::create(time, 255),
					ScaleTo::create(time, 1.0f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite4->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2, size.height/2+120) ) ,
					FadeTo::create(time, 10),
					ScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite1->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, size.height/2+80) ) ,
					FadeTo::create(time, SPRITE_OPACITY),
					ScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));

				profession2->runAction(iconAction);
				introduction2->runAction(actionIntro);
				profession1->setVisible(false);
				profession2->setVisible(true);
				profession3->setVisible(false);
				profession4->setVisible(false);
				introduction1->setVisible(false);
				introduction2->setVisible(true);
				introduction3->setVisible(false);
				introduction4->setVisible(false);
				
				break;
			case 4:
					
				sprite4->runAction(action);
				sprite3->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_WIDTH, size.height/2) ) ,
					FadeTo::create(time, 255),
					ScaleTo::create(time, 1.0f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite1->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2, size.height/2+120) ) ,
					FadeTo::create(time, 10),
					ScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite2->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, size.height/2+80) ) ,
					FadeTo::create(time, SPRITE_OPACITY),
					ScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				
				profession3->runAction(iconAction);
				introduction3->runAction(actionIntro);
				profession1->setVisible(false);
				profession2->setVisible(false);
				profession3->setVisible(true);
				profession4->setVisible(false);
				introduction1->setVisible(false);
				introduction2->setVisible(false);
				introduction3->setVisible(true);
				introduction4->setVisible(false);
				break;

			default:
				break;
		}
		mActionSpriteTag = mSpriteStatus;
		if(1==mSpriteStatus)
			mSpriteStatus = 4;
		else
			mSpriteStatus--;

		isTouchSlipped = mode;
		updateProfessionIcon(mSpriteStatus);
	}
	else if(mode == kTagTouchedLeft)
	{
		auto action = (ActionInterval*)Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, size.height/2+80) ) ,
					FadeTo::create(time, SPRITE_OPACITY),
					ScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL);
		action->setTag(ACTION_TAG);
		switch(mSpriteStatus)
		{
			case 1:
				sprite1->runAction(action);
				sprite2->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_WIDTH, size.height/2) ) ,
					FadeTo::create(time, 255),
					ScaleTo::create(time, 1.0f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite4->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2, size.height/2+120) ) ,
					FadeTo::create(time, 10),
					ScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite3->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2+OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, size.height/2+80) ) ,
					FadeTo::create(time, SPRITE_OPACITY),
					ScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
					
				profession2->runAction(iconAction);
				introduction2->runAction(actionIntro);
				profession1->setVisible(false);
				profession2->setVisible(true);
				profession3->setVisible(false);
				profession4->setVisible(false);
				introduction1->setVisible(false);
				introduction2->setVisible(true);
				introduction3->setVisible(false);
				introduction4->setVisible(false);
				
				break;
			case 2:
				sprite2->runAction(action);
				sprite3->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_WIDTH, size.height/2) ) ,
					FadeTo::create(time, 255),
					ScaleTo::create(time, 1.0f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite1->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2, size.height/2+120) ) ,
					FadeTo::create(time, 10),
					ScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite4->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2+OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, size.height/2+80) ) ,
					FadeTo::create(time, SPRITE_OPACITY),
					ScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
					
				profession3->runAction(iconAction);
				introduction3->runAction(actionIntro);
				profession1->setVisible(false);
				profession2->setVisible(false);
				profession3->setVisible(true);
				profession4->setVisible(false);
				introduction1->setVisible(false);
				introduction2->setVisible(false);
				introduction3->setVisible(true);
				introduction4->setVisible(false);
				
				break;
			case 3:
				sprite3->runAction(action);
				sprite4->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_WIDTH, size.height/2) ) ,
					FadeTo::create(time, 255),
					ScaleTo::create(time, 1.0f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite2->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2, size.height/2+120) ) ,
					FadeTo::create(time, 10),
					ScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite1->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2+OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, size.height/2+80) ) ,
					FadeTo::create(time, SPRITE_OPACITY),
					ScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
					
				profession4->runAction(iconAction);
				introduction4->runAction(actionIntro);
				profession1->setVisible(false);
				profession2->setVisible(false);
				profession3->setVisible(false);
				profession4->setVisible(true);
				introduction1->setVisible(false);
				introduction2->setVisible(false);
				introduction3->setVisible(false);
				introduction4->setVisible(true);
				
				break;
			case 4:
				sprite4->runAction(action);
				sprite1->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2-OFFSET_WIDTH, size.height/2) ) ,
					FadeTo::create(time, 255),
					ScaleTo::create(time, 1.0f),
					CCTintTo::create(time, 255, 255, 255),
					NULL));
				sprite3->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2, size.height/2+120) ) ,
					FadeTo::create(time, 10),
					ScaleTo::create(time, 0),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
				sprite2->runAction( Spawn::create(
					MoveTo::create(time, Vec2(size.width/2+OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, size.height/2+80) ) ,
					FadeTo::create(time, SPRITE_OPACITY),
					ScaleTo::create(time, 0.5f),
					CCTintTo::create(time, ROLECOLOR),
					NULL));
					
				profession1->runAction(iconAction);
				introduction1->runAction(actionIntro);
				profession1->setVisible(true);
				profession2->setVisible(false);
				profession3->setVisible(false);
				profession4->setVisible(false);
				introduction1->setVisible(true);
				introduction2->setVisible(false);
				introduction3->setVisible(false);
				introduction4->setVisible(false);
				
				break;
			default:
				break;
		}
		mActionSpriteTag = mSpriteStatus;
		if(4==mSpriteStatus)
			mSpriteStatus = 1;
		else
			mSpriteStatus++;

		isTouchSlipped = mode;
		updateProfessionIcon(mSpriteStatus);
	}
}

void CreateRoleLayer::onClickIconButton(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button *)pSender;
		auto imageView = (ui::ImageView*)btn->getChildByTag(SPRITE_ICON_TAG);
		imageView->setScale(0.85f);

		int btnTag = btn->getTag() - PROFESSION_ICON;

		if (isTouchSlipped || btnTag == mSpriteStatus)
		{
			return;
		}

		if ((btnTag - mSpriteStatus) % 2)
		{
			if (btnTag - mSpriteStatus == -1 || btnTag - mSpriteStatus == 3)
			{
				spriteRunAction(RUNACTION_TIME, kTagTouchedRight);
			}
			else if (btnTag - mSpriteStatus == 1 || btnTag - mSpriteStatus == -3)
			{
				spriteRunAction(RUNACTION_TIME, kTagTouchedLeft);
			}
		}
		else
		{
			isRunActionTwice = true;
			spriteRunAction(RUNACTION_TIME, kTagTouchedLeft);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void CreateRoleLayer::initProfessionIcon(int profession)
{
	Size s = Director::getInstance()->getVisibleSize();

	auto pLayer = (Layer*)getChildByTag(255);

	auto pSpriteFrame1 = Button::create();
	pSpriteFrame1->setTouchEnabled(true);
    pSpriteFrame1->loadTextures(FRAME_NONE, FRAME_NONE, "");
	pSpriteFrame1->setPosition(Vec2(s.width/2 - pSpriteFrame1->getContentSize().width*1.5f - 10, s.height-35));
	pSpriteFrame1->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSpriteFrame1->setPressedActionEnabled(true);
	pSpriteFrame1->setTag(PROFESSION_ICON+kTagSprite1);
	pSpriteFrame1->addTouchEventListener(CC_CALLBACK_2(CreateRoleLayer::onClickIconButton, this));
	pLayer->addChild(pSpriteFrame1);

	auto highlightFrame1 = ui::ImageView::create();
	highlightFrame1->loadTexture("res_ui/creating_a_role/ground_a.png");
	highlightFrame1->setAnchorPoint(Vec2(0.5f, 0.5f));
	highlightFrame1->setTag(HIGHLIGHT_TAG);
	highlightFrame1->setVisible(false);
	pSpriteFrame1->addChild(highlightFrame1);
	
	highlightFrame1->runAction(RepeatForever::create(
									Spawn::create(
										Sequence::create(
											ScaleTo::create(HIGHLIGHT_TRANS_TIME, 0.75f),
											ScaleTo::create(HIGHLIGHT_TRANS_TIME, 0.80f),
											NULL),
										RotateBy::create(HIGHLIGHT_TRANS_TIME*2, 180),
										NULL
										)));

	/*
	CCTutorialParticle * tutorialParticle1 = CCTutorialParticle::create("xuanzhuan.plist",35,52);
	tutorialParticle1->setPosition(Vec2(s.width/2 - pSpriteFrame1->getContentSize().width*1.5f - 10, s.height-56));
	tutorialParticle1->setTag(CCTUTORIALPARTICLE_TAG+kTagSprite1);
	tutorialParticle1->setAnchorPoint(Vec2(0.5f, 0.5f));
	tutorialParticle1->setVisible(false);
	pLayer->addChild(tutorialParticle1);
	*/
	std::string iconPath = BasePlayer::getHeadPathByProfession(1);
	auto pSprite1 = ui::ImageView::create();
	pSprite1->loadTexture(iconPath.c_str());
	//pSprite1->setPosition(Vec2(pSprite1->getContentSize().width/2, pSprite1->getContentSize().height/2));
	pSprite1->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSprite1->setScale(0.85f);
	pSprite1->setColor(Color3B(ROLECOLOR));
	pSprite1->setTag(SPRITE_ICON_TAG);
	pSpriteFrame1->addChild(pSprite1);
	
	auto pSpriteFrame2 = Button::create();
	pSpriteFrame2->setTouchEnabled(true);
    pSpriteFrame2->loadTextures(FRAME_NONE, FRAME_NONE, "");
	pSpriteFrame2->setPosition(Vec2(s.width/2 - pSpriteFrame2->getContentSize().width/2 - 5, s.height-35));
	pSpriteFrame2->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSpriteFrame2->setPressedActionEnabled(true);
	pSpriteFrame2->setTag(PROFESSION_ICON+kTagSprite2);
	pSpriteFrame2->addTouchEventListener(CC_CALLBACK_2(CreateRoleLayer::onClickIconButton, this));
	pLayer->addChild(pSpriteFrame2);

	auto highlightFrame2 = ui::ImageView::create();
	highlightFrame2->loadTexture("res_ui/creating_a_role/ground_a.png");
	highlightFrame2->setAnchorPoint(Vec2(0.5f, 0.5f));
	highlightFrame2->setTag(HIGHLIGHT_TAG);
	highlightFrame2->setVisible(false);
	pSpriteFrame2->addChild(highlightFrame2);
	
	highlightFrame2->runAction(RepeatForever::create(
									Spawn::create(
										Sequence::create(
											ScaleTo::create(HIGHLIGHT_TRANS_TIME, 0.75f),
											ScaleTo::create(HIGHLIGHT_TRANS_TIME, 0.80f),
											NULL),
										RotateBy::create(HIGHLIGHT_TRANS_TIME*2, 180),
										NULL
										)));

	/*
	CCTutorialParticle * tutorialParticle2 = CCTutorialParticle::create("xuanzhuan.plist",35,52);
	tutorialParticle2->setPosition(Vec2(0,-26));
	tutorialParticle2->setTag(CCTUTORIALPARTICLE_TAG);
	tutorialParticle2->setAnchorPoint(Vec2(0.5f, 0.5f));
	tutorialParticle2->setVisible(false);
	pSpriteFrame2->addChild(tutorialParticle2);
	*/
	iconPath = BasePlayer::getHeadPathByProfession(2);
	auto pSprite2 = ui::ImageView::create();
	pSprite2->loadTexture(iconPath.c_str());
	//pSprite2->setPosition(Vec2(pSprite2->getContentSize().width/2, pSprite2->getContentSize().height/2));
	pSprite2->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSprite2->setScale(0.85f);
	pSprite2->setColor(Color3B(ROLECOLOR));
	pSprite2->setTag(SPRITE_ICON_TAG);
	pSpriteFrame2->addChild(pSprite2);

	auto pSpriteFrame3 = Button::create();
	pSpriteFrame3->setTouchEnabled(true);
    pSpriteFrame3->loadTextures(FRAME_NONE, FRAME_NONE, "");
	pSpriteFrame3->setPosition(Vec2(s.width/2 + pSpriteFrame2->getContentSize().width/2 + 5, s.height-35));
	pSpriteFrame3->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSpriteFrame3->setPressedActionEnabled(true);
	pSpriteFrame3->setTag(PROFESSION_ICON+kTagSprite3);
	pSpriteFrame3->addTouchEventListener(CC_CALLBACK_2(CreateRoleLayer::onClickIconButton, this));
	pLayer->addChild(pSpriteFrame3);

	auto highlightFrame3 = ui::ImageView::create();
	highlightFrame3->loadTexture("res_ui/creating_a_role/ground_a.png");
	highlightFrame3->setAnchorPoint(Vec2(0.5f, 0.5f));
	highlightFrame3->setTag(HIGHLIGHT_TAG);
	highlightFrame3->setVisible(false);
	pSpriteFrame3->addChild(highlightFrame3);
	
	highlightFrame3->runAction(RepeatForever::create(
									Spawn::create(
										Sequence::create(
											ScaleTo::create(HIGHLIGHT_TRANS_TIME, 0.75f),
											ScaleTo::create(HIGHLIGHT_TRANS_TIME, 0.80f),
											NULL),
										RotateBy::create(HIGHLIGHT_TRANS_TIME*2, 180),
										NULL
										)));

	/*
	CCTutorialParticle * tutorialParticle3 = CCTutorialParticle::create("xuanzhuan.plist",35,52);
	tutorialParticle3->setPosition(Vec2(0,-26));
	tutorialParticle3->setTag(CCTUTORIALPARTICLE_TAG);
	tutorialParticle3->setAnchorPoint(Vec2(0.5f, 0.5f));
	tutorialParticle3->setVisible(false);
	pSpriteFrame3->addChild(tutorialParticle3);
	*/
	iconPath = BasePlayer::getHeadPathByProfession(3);
	auto pSprite3 = ui::ImageView::create();
	pSprite3->loadTexture(iconPath.c_str());
	//pSprite3->setPosition(Vec2(pSprite3->getContentSize().width/2, pSprite3->getContentSize().height/2));
	pSprite3->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSprite3->setScale(0.85f);
	pSprite3->setColor(Color3B(ROLECOLOR));
	pSprite3->setTag(SPRITE_ICON_TAG);
	pSpriteFrame3->addChild(pSprite3);

	auto pSpriteFrame4 = Button::create();
	pSpriteFrame4->setTouchEnabled(true);
    pSpriteFrame4->loadTextures(FRAME_NONE, FRAME_NONE, "");
	pSpriteFrame4->setPosition(Vec2(s.width/2 + pSpriteFrame2->getContentSize().width*1.5f + 10, s.height-35));
	pSpriteFrame4->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSpriteFrame4->setPressedActionEnabled(true);
	pSpriteFrame4->setTag(PROFESSION_ICON+kTagSprite4);
	pSpriteFrame4->addTouchEventListener(CC_CALLBACK_2(CreateRoleLayer::onClickIconButton, this));
	pLayer->addChild(pSpriteFrame4);

	auto highlightFrame4 = ui::ImageView::create();
	highlightFrame4->loadTexture("res_ui/creating_a_role/ground_a.png");
	highlightFrame4->setAnchorPoint(Vec2(0.5f, 0.5f));
	highlightFrame4->setTag(HIGHLIGHT_TAG);
	highlightFrame4->setVisible(false);
	pSpriteFrame4->addChild(highlightFrame4);
	
	highlightFrame4->runAction(RepeatForever::create(
									Spawn::create(
										Sequence::create(
											ScaleTo::create(HIGHLIGHT_TRANS_TIME, 0.75f),
											ScaleTo::create(HIGHLIGHT_TRANS_TIME, 0.80f),
											NULL),
										RotateBy::create(HIGHLIGHT_TRANS_TIME*2, 180),
										NULL
										)));

	/*
	CCTutorialParticle * tutorialParticle4 = CCTutorialParticle::create("xuanzhuan.plist",35,52);
	tutorialParticle4->setPosition(Vec2(0,-26));
	tutorialParticle4->setTag(CCTUTORIALPARTICLE_TAG);
	tutorialParticle4->setAnchorPoint(Vec2(0.5f, 0.5f));
	tutorialParticle4->setVisible(false);
	pSpriteFrame4->addChild(tutorialParticle4);
	*/
	iconPath = BasePlayer::getHeadPathByProfession(4);
	auto pSprite4 = ui::ImageView::create();
	pSprite4->loadTexture(iconPath.c_str());
	//pSprite4->setPosition(Vec2(pSprite4->getContentSize().width/2, pSprite4->getContentSize().height/2));
	pSprite4->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSprite4->setScale(0.85f);
	pSprite4->setColor(Color3B(ROLECOLOR));
	pSprite4->setTag(SPRITE_ICON_TAG);
	pSpriteFrame4->addChild(pSprite4);

	switch(profession)
	{
	case 1:
		pSpriteFrame1->loadTextures(FRAME_GREEN, FRAME_GREEN, "");
		pSprite1->setColor(Color3B(255, 255, 255));
		highlightFrame1->setVisible(true);
		//tutorialParticle1->setVisible(true);
		break;
	case 2:
		pSpriteFrame2->loadTextures(FRAME_GREEN, FRAME_GREEN, "");
		pSprite2->setColor(Color3B(255, 255, 255));
		highlightFrame2->setVisible(true);
		//tutorialParticle2->setVisible(true);
		break;
	case 3:
		pSpriteFrame3->loadTextures(FRAME_GREEN, FRAME_GREEN, "");
		pSprite3->setColor(Color3B(255, 255, 255));
		highlightFrame3->setVisible(true);
		//tutorialParticle3->setVisible(true);
		break;
	case 4:
		pSpriteFrame4->loadTextures(FRAME_GREEN, FRAME_GREEN, "");
		pSprite4->setColor(Color3B(255, 255, 255));
		highlightFrame4->setVisible(true);
		//tutorialParticle4->setVisible(true);
		break;
	default:
		break;
	}
}

void CreateRoleLayer::updateProfessionIcon(int profession)
{
	auto pLayer = (Layer*)getChildByTag(255);

	auto spriteFrame1 = (Button*)pLayer->getChildByTag(PROFESSION_ICON+kTagSprite1);
	auto sprite1 = (ui::ImageView*)spriteFrame1->getChildByTag(SPRITE_ICON_TAG);
	auto highlightFrame1 = (ui::ImageView*)spriteFrame1->getChildByTag(HIGHLIGHT_TAG);
	auto spriteFrame2 = (Button*)pLayer->getChildByTag(PROFESSION_ICON+kTagSprite2);
	auto sprite2 = (ui::ImageView*)spriteFrame2->getChildByTag(SPRITE_ICON_TAG);
	auto highlightFrame2 = (ui::ImageView*)spriteFrame2->getChildByTag(HIGHLIGHT_TAG);
	auto spriteFrame3 = (Button*)pLayer->getChildByTag(PROFESSION_ICON+kTagSprite3);
	auto sprite3 = (ui::ImageView*)spriteFrame3->getChildByTag(SPRITE_ICON_TAG);
	auto highlightFrame3 = (ui::ImageView*)spriteFrame3->getChildByTag(HIGHLIGHT_TAG);
	auto spriteFrame4 = (Button*)pLayer->getChildByTag(PROFESSION_ICON+kTagSprite4);
	auto sprite4 = (ui::ImageView*)spriteFrame4->getChildByTag(SPRITE_ICON_TAG);
	auto highlightFrame4 = (ui::ImageView*)spriteFrame4->getChildByTag(HIGHLIGHT_TAG);

	spriteFrame1->loadTextures(FRAME_NONE, FRAME_NONE, "");
	spriteFrame2->loadTextures(FRAME_NONE, FRAME_NONE, "");
	spriteFrame3->loadTextures(FRAME_NONE, FRAME_NONE, "");
	spriteFrame4->loadTextures(FRAME_NONE, FRAME_NONE, "");
	sprite1->setColor(Color3B(ROLECOLOR));
	sprite2->setColor(Color3B(ROLECOLOR));
	sprite3->setColor(Color3B(ROLECOLOR));
	sprite4->setColor(Color3B(ROLECOLOR));
	highlightFrame1->setVisible(false);
	highlightFrame2->setVisible(false);
	highlightFrame3->setVisible(false);
	highlightFrame4->setVisible(false);

	switch(profession)
	{
	case 1:
		spriteFrame1->loadTextures(FRAME_GREEN, FRAME_GREEN, "");
		sprite1->setColor(Color3B(255, 255, 255));
		highlightFrame1->setVisible(true);
		break;
	case 2:
		spriteFrame2->loadTextures(FRAME_GREEN, FRAME_GREEN, "");
		sprite2->setColor(Color3B(255, 255, 255));
		highlightFrame2->setVisible(true);
		break;
	case 3:
		spriteFrame3->loadTextures(FRAME_GREEN, FRAME_GREEN, "");
		sprite3->setColor(Color3B(255, 255, 255));
		highlightFrame3->setVisible(true);
		break;
	case 4:
		spriteFrame4->loadTextures(FRAME_GREEN, FRAME_GREEN, "");
		sprite4->setColor(Color3B(255, 255, 255));
		highlightFrame4->setVisible(true);
		break;
	default:
		break;
	}
}

void CreateRoleLayer::updateSpriteFront(int profession)
{
	auto pSprite = (Sprite*)getChildByTag(profession);
	pSprite->setLocalZOrder(++mZOrder);
	//pSprite->setVisible(true);
}

void CreateRoleLayer::initProfessionSprite(int minProfession)
{
	Size s = Director::getInstance()->getVisibleSize();

	const float MOVE_UP_AND_DOWN_DURATION = 2.5f;
	const int MOVE_UP_AND_DOWN_DISTANCE = 5;
	
	auto action = Spawn::create(
								Sequence::create(
									MoveTo::create(0, Vec2(s.width, s.height)),
									MoveTo::create(INTRODUCTION_TIME, Vec2(s.width/2+110, s.height/2+100)),
									NULL),
								Sequence::create(
									ScaleTo::create(0, ICON_SCALE_MAX), 
									ScaleTo::create(INTRODUCTION_TIME, 0.7f), 
									//ScaleTo::create(0.1f, 0.9f),
									//ScaleTo::create(0.1f, 0.8f),
									NULL),
								/*Sequence::create(
									CCSkewTo::create(0.1f, 90.0f, 0), 
									CCSkewTo::create(0.5f, 0, 0), 
									NULL),*/
								NULL);
	
	auto actionIntro = Spawn::create(
								Sequence::create(
									DelayTime::create(DELAY_TIME),
									MoveTo::create(0, Vec2(s.width, 0)),
									MoveTo::create(INTRODUCTION_TIME, Vec2(s.width/2+110, s.height/2-50)),
									NULL),
								Sequence::create(
									DelayTime::create(DELAY_TIME),
									ScaleTo::create(0, ICON_SCALE_MAX), 
									ScaleTo::create(INTRODUCTION_TIME, 0.9f), 
									//ScaleTo::create(0.1f, 0.9f),
									//ScaleTo::create(0.1f, 0.8f),
									NULL),
								/*Sequence::create(
									CCSkewTo::create(0.1f, 90.0f, 0), 
									CCSkewTo::create(0.5f, 0, 0), 
									NULL),*/
								NULL);

	//auto introductionAction = Sequence::create(
	//									MoveTo::create(0, Vec2(s.width+20, s.height/2+20)), 
	//									MoveTo::create(0.2f, Vec2(s.width-90, s.height/2+20)), 
	//									MoveTo::create(0.1f, Vec2(s.width-60, s.height/2+20)),
	//									MoveTo::create(0.1f, Vec2(s.width-70, s.height/2+20)),
	//									NULL);

	auto pSprite1 = SpriteBlur::create(SPRITE_MENGJIANG);
	pSprite1->setBlurSize(0);
	pSprite1->setCascadeColorEnabled(true);
	pSprite1->setCascadeOpacityEnabled(true);
	pSprite1->setPosition(Vec2(s.width/2-OFFSET_WIDTH, s.height/2));
	//pSprite1->setScale(0.9f);
	pSprite1->setAnchorPoint(Vec2(0, 0.5f));
	addChild(pSprite1, 1, kTagSprite1);

	MoveBy * moveUpAction = MoveBy::create(MOVE_UP_AND_DOWN_DURATION, Vec2(0, MOVE_UP_AND_DOWN_DISTANCE));
	MoveBy * moveDownAction = MoveBy::create(MOVE_UP_AND_DOWN_DURATION, Vec2(0, -MOVE_UP_AND_DOWN_DISTANCE));
	RepeatForever * repeapAction = RepeatForever::create(Sequence::create(moveUpAction, moveDownAction, NULL));
	pSprite1->runAction(repeapAction);

	auto pSprite2 = SpriteBlur::create(SPRITE_GUIMOU);
	pSprite2->setBlurSize(0);
	pSprite2->setCascadeColorEnabled(true);
	pSprite2->setCascadeOpacityEnabled(true);
	pSprite2->setPosition(Vec2(s.width/2+OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, s.height/2+80));
	pSprite2->setScale(0.5f);
	pSprite2->setAnchorPoint(Vec2(0, 0.5f));
	pSprite2->setColor(Color3B(ROLECOLOR));
	addChild(pSprite2, 1, kTagSprite2);

	moveUpAction = MoveBy::create(MOVE_UP_AND_DOWN_DURATION, Vec2(0, -MOVE_UP_AND_DOWN_DISTANCE));
	moveDownAction = MoveBy::create(MOVE_UP_AND_DOWN_DURATION, Vec2(0, MOVE_UP_AND_DOWN_DISTANCE));
	repeapAction = RepeatForever::create(Sequence::create(moveUpAction, moveDownAction, NULL));
	pSprite2->runAction(repeapAction);

	auto pSprite3 = SpriteBlur::create(SPRITE_HAOJIE);
	pSprite3->setBlurSize(0);
	pSprite3->setCascadeColorEnabled(true);
	pSprite3->setCascadeOpacityEnabled(true);
	pSprite3->setPosition(Vec2(s.width/2, s.height/2+120));
	pSprite3->setScale(0);
	pSprite3->setAnchorPoint(Vec2(0, 0.5f));
	pSprite3->setColor(Color3B(ROLECOLOR));
	addChild(pSprite3, 1, kTagSprite3);

	moveUpAction = MoveBy::create(MOVE_UP_AND_DOWN_DURATION, Vec2(0, MOVE_UP_AND_DOWN_DISTANCE));
	moveDownAction = MoveBy::create(MOVE_UP_AND_DOWN_DURATION, Vec2(0, -MOVE_UP_AND_DOWN_DISTANCE));
	repeapAction = RepeatForever::create(Sequence::create(moveUpAction, moveDownAction, NULL));
	pSprite3->runAction(repeapAction);

	auto pSprite4 = SpriteBlur::create(SPRITE_SHENSHE);
	pSprite4->setBlurSize(0);
	pSprite4->setCascadeColorEnabled(true);
	pSprite4->setCascadeOpacityEnabled(true);
	pSprite4->setPosition(Vec2(s.width/2-OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, s.height/2+80));
	pSprite4->setScale(0.5f);
	pSprite4->setAnchorPoint(Vec2(0, 0.5f));
	pSprite4->setColor(Color3B(ROLECOLOR));
	addChild(pSprite4, 1, kTagSprite4);

	moveUpAction = MoveBy::create(MOVE_UP_AND_DOWN_DURATION, Vec2(0, -MOVE_UP_AND_DOWN_DISTANCE));
	moveDownAction = MoveBy::create(MOVE_UP_AND_DOWN_DURATION, Vec2(0, MOVE_UP_AND_DOWN_DISTANCE));
	repeapAction = RepeatForever::create(Sequence::create(moveUpAction, moveDownAction, NULL));
	pSprite4->runAction(repeapAction);

	auto pSpriteTable1 = SpriteBlur::create(SPRITE_TABLE);
	pSpriteTable1->setPosition(Vec2(OFFSET_WIDTH, -135));
	pSpriteTable1->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSpriteTable1->setTag(TABLE_TAG);
	pSpriteTable1->setBlurSize(0);
	pSprite1->addChild(pSpriteTable1, -2);
	////
	//auto pSpriteTableLight1 = Sprite::create(SPRITE_TABLE_LIGHT);
	//pSpriteTableLight1->setPosition(Vec2(pSpriteTable1->getContentSize().width/2+10, pSpriteTable1->getContentSize().height-70));
	//pSpriteTableLight1->setAnchorPoint(Vec2(0.5f, 0.5f));
	//pSpriteTableLight1->setVisible(true);
	//pSpriteTableLight1->setTag(TABLE_LIGHT_TAG);
	//pSpriteTable1->addChild(pSpriteTableLight1, 1);

	//pSpriteTableLight1->runAction(RepeatForever::create(
	//									Sequence::create(
	//										FadeTo::create(TABLE_LIGHT_TRANS_TIME, 0), 
	//										FadeTo::create(TABLE_LIGHT_TRANS_TIME, 255), 
	//										NULL)));

	auto pSpriteTable2 = SpriteBlur::create(SPRITE_TABLE);
	pSpriteTable2->setPosition(Vec2(OFFSET_WIDTH, -135));
	pSpriteTable2->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSpriteTable2->setTag(TABLE_TAG);
	pSpriteTable2->setBlurSize(0);
	pSprite2->addChild(pSpriteTable2, -2);
	////
	//auto pSpriteTableLight2 = Sprite::create(SPRITE_TABLE_LIGHT);
	//pSpriteTableLight2->setPosition(Vec2(pSpriteTable1->getContentSize().width/2+10, pSpriteTable1->getContentSize().height-70));
	//pSpriteTableLight2->setAnchorPoint(Vec2(0.5f, 0.5f));
	//pSpriteTableLight2->setVisible(false);
	//pSpriteTableLight2->setTag(TABLE_LIGHT_TAG);
	//pSpriteTable2->addChild(pSpriteTableLight2, 1);

	//pSpriteTableLight2->runAction(RepeatForever::create(
	//									Sequence::create(
	//										FadeTo::create(TABLE_LIGHT_TRANS_TIME, 0), 
	//										FadeTo::create(TABLE_LIGHT_TRANS_TIME, 255), 
	//										NULL)));

	auto pSpriteTable3 = SpriteBlur::create(SPRITE_TABLE);
	pSpriteTable3->setPosition(Vec2(OFFSET_WIDTH, -135));
	pSpriteTable3->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSpriteTable3->setTag(TABLE_TAG);
	pSpriteTable3->setBlurSize(0);
	pSprite3->addChild(pSpriteTable3, -2);
	////
	//auto pSpriteTableLight3 = Sprite::create(SPRITE_TABLE_LIGHT);
	//pSpriteTableLight3->setPosition(Vec2(pSpriteTable1->getContentSize().width/2+10, pSpriteTable1->getContentSize().height-70));
	//pSpriteTableLight3->setAnchorPoint(Vec2(0.5f, 0.5f));
	//pSpriteTableLight3->setVisible(false);
	//pSpriteTableLight3->setTag(TABLE_LIGHT_TAG);
	//pSpriteTable3->addChild(pSpriteTableLight3, 1);

	//pSpriteTableLight3->runAction(RepeatForever::create(
	//									Sequence::create(
	//										FadeTo::create(TABLE_LIGHT_TRANS_TIME, 0), 
	//										FadeTo::create(TABLE_LIGHT_TRANS_TIME, 255), 
	//										NULL)));

	auto pSpriteTable4 = SpriteBlur::create(SPRITE_TABLE);
	pSpriteTable4->setPosition(Vec2(OFFSET_WIDTH, -135));
	pSpriteTable4->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSpriteTable4->setTag(TABLE_TAG);
	pSpriteTable4->setBlurSize(0);
	pSprite4->addChild(pSpriteTable4, -2);
	////
	//auto pSpriteTableLight4 = Sprite::create(SPRITE_TABLE_LIGHT);
	//pSpriteTableLight4->setPosition(Vec2(pSpriteTable1->getContentSize().width/2+10, pSpriteTable1->getContentSize().height-70));
	//pSpriteTableLight4->setAnchorPoint(Vec2(0.5f, 0.5f));
	//pSpriteTableLight4->setVisible(false);
	//pSpriteTableLight4->setTag(TABLE_LIGHT_TAG);
	//pSpriteTable4->addChild(pSpriteTableLight4, 1);

	//pSpriteTableLight4->runAction(RepeatForever::create(
	//									Sequence::create(
	//										FadeTo::create(TABLE_LIGHT_TRANS_TIME, 0), 
	//										FadeTo::create(TABLE_LIGHT_TRANS_TIME, 255), 
	//										NULL)));

	pSprite2->setColor(Color3B(ROLECOLOR));
	pSprite3->setColor(Color3B(ROLECOLOR));
	pSprite4->setColor(Color3B(ROLECOLOR));

	Vec2 profession_icon_pos = Vec2(s.width, s.height);
	Sprite *profession1=Sprite::create(PROFESSION_MENGJIANG);
	profession1->setAnchorPoint(Vec2(0.5f,0.5f));
	profession1->setPosition(profession_icon_pos);
	//profession1->setScale(0.8f);
	profession1->setVisible(true);
	addChild(profession1, COMMON_TAG, 5);

	Sprite *profession2=Sprite::create(PROFESSION_GUIMOU);
	profession2->setAnchorPoint(Vec2(0.5f,0.5f));
	profession2->setPosition(profession_icon_pos);
	//profession2->setScale(0.8f);
	profession2->setVisible(false);
	addChild(profession2, COMMON_TAG, 6);

	Sprite *profession3=Sprite::create(PROFESSION_HAOJIE);
	profession3->setAnchorPoint(Vec2(0.5f,0.5f));
	profession3->setPosition(profession_icon_pos);
	//profession3->setScale(0.8f);
	profession3->setVisible(false);
	addChild(profession3, COMMON_TAG, 7);

	Sprite *profession4=Sprite::create(PROFESSION_SHENSHE);
	profession4->setAnchorPoint(Vec2(0.5f,0.5f));
	profession4->setPosition(profession_icon_pos);
	//profession4->setScale(0.8f);
	profession4->setVisible(false);
	//profession4->setScale(0);
	addChild(profession4, COMMON_TAG, 8);

	Vec2 profession_introduction_pos = Vec2(s.width, 0);
	Sprite *introduction1=Sprite::create(INTRODUCTION_MENGJIANG);
	introduction1->setAnchorPoint(Vec2(0.5f,0.5f));
	introduction1->setPosition(profession_introduction_pos);
	introduction1->setVisible(true);
	//introduction1->setScale(1.0f);
	addChild(introduction1, COMMON_TAG, 9);
	
	Sprite *ink=Sprite::create("res_ui/creating_a_role/modi.png");
	ink->setAnchorPoint(Vec2(0.5f,0.5f));
	ink->setPosition(Vec2(introduction1->getContentSize().width/2-5, introduction1->getContentSize().height/2+15));
	ink->setScaleX(2.3f);
	ink->setScaleY(1.9f);
	introduction1->addChild(ink, -1);

	Sprite *introduction2=Sprite::create(INTRODUCTION_GUIMOU);
	introduction2->setAnchorPoint(Vec2(0.5f,0.5f));
	introduction2->setPosition(profession_introduction_pos);
	introduction2->setVisible(false);
	//introduction2->setScale(0);
	addChild(introduction2, COMMON_TAG, 10);
	
	ink=Sprite::create("res_ui/creating_a_role/modi.png");
	ink->setAnchorPoint(Vec2(0.5f,0.5f));
	ink->setPosition(Vec2(introduction2->getContentSize().width/2-5, introduction2->getContentSize().height/2+15));
	ink->setScaleX(2.3f);
	ink->setScaleY(1.9f);
	introduction2->addChild(ink, -1);

	Sprite *introduction3=Sprite::create(INTRODUCTION_HAOJIE);
	introduction3->setAnchorPoint(Vec2(0.5f,0.5f));
	introduction3->setPosition(profession_introduction_pos);
	introduction3->setVisible(false);
	//introduction3->setScale(0);
	addChild(introduction3, COMMON_TAG, 11);
	
	ink=Sprite::create("res_ui/creating_a_role/modi.png");
	ink->setAnchorPoint(Vec2(0.5f,0.5f));
	ink->setPosition(Vec2(introduction3->getContentSize().width/2-5, introduction3->getContentSize().height/2+15));
	ink->setScaleX(2.3f);
	ink->setScaleY(1.9f);
	introduction3->addChild(ink, -1);

	Sprite *introduction4=Sprite::create(INTRODUCTION_SHENSHE);
	introduction4->setAnchorPoint(Vec2(0.5f,0.5f));
	introduction4->setPosition(profession_introduction_pos);
	introduction4->setVisible(false);
	//introduction4->setScale(0);
	addChild(introduction4, COMMON_TAG, 12);
	
	ink=Sprite::create("res_ui/creating_a_role/modi.png");
	ink->setAnchorPoint(Vec2(0.5f,0.5f));
	ink->setPosition(Vec2(introduction4->getContentSize().width/2-5, introduction4->getContentSize().height/2+15));
	ink->setScaleX(2.3f);
	ink->setScaleY(1.9f);
	introduction4->addChild(ink, -1);

	switch(minProfession)
	{
	case 1:
		pSprite1->setLocalZOrder(FRONT_TAG);
		pSprite2->setLocalZOrder(MIDDLE_TAG);
		showBlurSprite(pSprite2, BLUR_RIGHT_TAG);
		pSprite4->setLocalZOrder(MIDDLE_TAG);
		showBlurSprite(pSprite4, BLUR_LEFT_TAG);
		pSprite3->setLocalZOrder(BACK_TAG);
		
		profession1->runAction(action);
		introduction1->runAction(actionIntro);
		break;
	case 2:
		pSprite2->setLocalZOrder(FRONT_TAG);
		pSprite3->setLocalZOrder(MIDDLE_TAG);
		pSprite1->setLocalZOrder(MIDDLE_TAG);
		pSprite4->setLocalZOrder(BACK_TAG);

		pSprite2->setPosition(Vec2(s.width/2-OFFSET_WIDTH, s.height/2));
		pSprite2->setScale(1.0f);
		pSprite2->setColor(Color3B(255, 255, 255));
		pSprite3->setPosition(Vec2(s.width/2+OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, s.height/2+80));
		pSprite3->setScale(0.5f);
		pSprite3->setOpacity(SPRITE_OPACITY);
		pSprite3->setColor(Color3B(ROLECOLOR));
		pSprite4->setPosition(Vec2(s.width/2, s.height/2+120));
		pSprite4->setScale(0);
		pSprite4->setOpacity(10);
		pSprite4->setColor(Color3B(ROLECOLOR));
		pSprite1->setPosition(Vec2(s.width/2-OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, s.height/2+80));
		pSprite1->setScale(0.5f);
		pSprite1->setOpacity(SPRITE_OPACITY);
		pSprite1->setColor(Color3B(ROLECOLOR));
		profession1->setVisible(false);
		profession2->setVisible(true);
		profession3->setVisible(false);
		profession4->setVisible(false);
		////pSpriteTableLight1->setVisible(false);
		//pSpriteTableLight2->setVisible(true);
		//pSpriteTableLight3->setVisible(false);
		//pSpriteTableLight4->setVisible(false);
		//
		profession2->runAction(action);
		showBlurSprite(pSprite3, BLUR_RIGHT_TAG);
		showBlurSprite(pSprite1, BLUR_LEFT_TAG);
		introduction1->setVisible(false);
		introduction2->setVisible(true);
		introduction3->setVisible(false);
		introduction4->setVisible(false);
		introduction2->runAction(actionIntro);
		break;
	case 3:
		pSprite3->setLocalZOrder(FRONT_TAG);
		pSprite4->setLocalZOrder(MIDDLE_TAG);
		pSprite2->setLocalZOrder(MIDDLE_TAG);
		pSprite1->setLocalZOrder(BACK_TAG);

		pSprite3->setPosition(Vec2(s.width/2-OFFSET_WIDTH, s.height/2));
		pSprite3->setScale(1.0f);
		pSprite3->setColor(Color3B(255, 255, 255));
		pSprite4->setPosition(Vec2(s.width/2+OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, s.height/2+80));
		pSprite4->setScale(0.5f);
		pSprite4->setOpacity(SPRITE_OPACITY);
		pSprite4->setColor(Color3B(ROLECOLOR));
		pSprite1->setPosition(Vec2(s.width/2, s.height/2+120));
		pSprite1->setScale(0);
		pSprite1->setOpacity(10);
		pSprite1->setColor(Color3B(ROLECOLOR));
		pSprite2->setPosition(Vec2(s.width/2-OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, s.height/2+80));
		pSprite2->setScale(0.5f);
		pSprite2->setOpacity(SPRITE_OPACITY);
		pSprite2->setColor(Color3B(ROLECOLOR));
		profession1->setVisible(false);
		profession2->setVisible(false);
		profession3->setVisible(true);
		profession4->setVisible(false);
		////pSpriteTableLight1->setVisible(false);
		//pSpriteTableLight2->setVisible(false);
		//pSpriteTableLight3->setVisible(true);
		//pSpriteTableLight4->setVisible(false);
		//
		profession3->runAction(action);
		showBlurSprite(pSprite4, BLUR_RIGHT_TAG);
		showBlurSprite(pSprite2, BLUR_LEFT_TAG);

		introduction1->setVisible(false);
		introduction2->setVisible(false);
		introduction3->setVisible(true);
		introduction4->setVisible(false);
		introduction3->runAction(actionIntro);
		break;
	case 4:
		pSprite4->setLocalZOrder(FRONT_TAG);
		pSprite1->setLocalZOrder(MIDDLE_TAG);
		pSprite3->setLocalZOrder(MIDDLE_TAG);
		pSprite2->setLocalZOrder(BACK_TAG);

		pSprite4->setPosition(Vec2(s.width/2-OFFSET_WIDTH, s.height/2));
		pSprite4->setScale(1.0f);
		pSprite4->setColor(Color3B(255, 255, 255));
		pSprite1->setPosition(Vec2(s.width/2+OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, s.height/2+80));
		pSprite1->setScale(0.5f);
		pSprite1->setOpacity(SPRITE_OPACITY);
		pSprite1->setColor(Color3B(ROLECOLOR));
		pSprite2->setPosition(Vec2(s.width/2, s.height/2+120));
		pSprite2->setScale(0);
		pSprite2->setOpacity(10);
		pSprite2->setColor(Color3B(ROLECOLOR));
		pSprite3->setPosition(Vec2(s.width/2-OFFSET_SPRITE_MIDDLE-OFFSET_WIDTH*0.5f, s.height/2+80));
		pSprite3->setScale(0.5f);
		pSprite3->setOpacity(SPRITE_OPACITY);
		pSprite3->setColor(Color3B(ROLECOLOR));
		profession1->setVisible(false);
		profession2->setVisible(false);
		profession3->setVisible(false);
		profession4->setVisible(true);
		////pSpriteTableLight1->setVisible(false);
		//pSpriteTableLight2->setVisible(false);
		//pSpriteTableLight3->setVisible(false);
		//pSpriteTableLight4->setVisible(true);
		//
		profession4->runAction(action);
		showBlurSprite(pSprite1, BLUR_RIGHT_TAG);
		showBlurSprite(pSprite3, BLUR_LEFT_TAG);

		introduction1->setVisible(false);
		introduction2->setVisible(false);
		introduction3->setVisible(false);
		introduction4->setVisible(true);
		introduction4->runAction(actionIntro);
		break;
	default:
		break;
	}
}

void CreateRoleLayer::connectGameServer()
{
	ClientNetEngine::sharedSocketEngine()->init(*(GameView::getInstance()));
}

void CreateRoleLayer::update(float dt)
{
	if(isTouchSlipped)
	{
		auto sprite = (SpriteBlur*)getChildByTag(mActionSpriteTag);
		auto pSpriteTable = (Sprite*)sprite->getChildByTag(TABLE_TAG);
		//auto pSpriteTableLight = (Sprite*)pSpriteTable->getChildByTag(TABLE_LIGHT_TAG);
		//pSpriteTableLight->setVisible(false);
		auto action = sprite->getActionByTag(ACTION_TAG);
		auto spriteFront = getChildByTag(mSpriteStatus);
		auto pSpriteTableFront = (Sprite*)spriteFront->getChildByTag(TABLE_TAG);
		//auto pSpriteTableLightFront = (Sprite*)pSpriteTableFront->getChildByTag(TABLE_LIGHT_TAG);
		//pSpriteTableLightFront->setVisible(true);
		int iSpriteMiddle, iSpriteBack;
		SpriteBlur* spriteMiddle;
		SpriteBlur* spriteBack;
		if(spriteFront->getScale() >= sprite->getScale())
		{
			spriteFront->setLocalZOrder(FRONT_TAG);
			sprite->setLocalZOrder(MIDDLE_TAG);
			if(isTouchSlipped == kTagTouchedLeft)
			{
				if(mSpriteStatus == 4)
				{
					iSpriteMiddle = 1;
				}
				else
				{
					iSpriteMiddle = mSpriteStatus+1;
				}

				if(mActionSpriteTag == 1)
				{
					iSpriteBack = 4;
				}
				else
				{
					iSpriteBack = mActionSpriteTag-1;
				}
				spriteMiddle = (SpriteBlur*)getChildByTag(iSpriteMiddle);
				spriteBack = (SpriteBlur*)getChildByTag(iSpriteBack);
				spriteMiddle->setLocalZOrder(MIDDLE_TAG);
				spriteBack->setLocalZOrder(BACK_TAG);
			}
			else if(isTouchSlipped == kTagTouchedRight)
			{
				if(mSpriteStatus == 1)
				{
					iSpriteMiddle = 4;
				}
				else
				{
					iSpriteMiddle = mSpriteStatus-1;
				}

				if(mActionSpriteTag == 4)
				{
					iSpriteBack = 1;
				}
				else
				{
					iSpriteBack = mActionSpriteTag+1;
				}
				spriteMiddle = (SpriteBlur*)getChildByTag(iSpriteMiddle);
				spriteBack = (SpriteBlur*)getChildByTag(iSpriteBack);
				spriteMiddle->setLocalZOrder(MIDDLE_TAG);
				spriteBack->setLocalZOrder(BACK_TAG);
			}
		}
		if(action == NULL)
		{
			if(isTouchSlipped == kTagTouchedRight)
			{
				showBlurSprite(spriteMiddle, BLUR_LEFT_TAG);
				showBlurSprite(sprite, BLUR_RIGHT_TAG);
			}
			else
			{
				showBlurSprite(spriteMiddle, BLUR_RIGHT_TAG);
				showBlurSprite(sprite, BLUR_LEFT_TAG);
			}
			isTouchSlipped = kTagTouchedNot;
			if(isRunActionTwice)
			{
				spriteRunAction(RUNACTION_TIME, kTagTouchedLeft);
				isRunActionTwice = false;
			}
		}
	}
}

void CreateRoleLayer::showBlurSprite(SpriteBlur* sprite, int tag)
{
	sprite->setBlurSize(0.9f);
	auto table = (SpriteBlur*)sprite->getChildByTag(TABLE_TAG);
	table->setBlurSize(1.0f);
	return;
	auto blurLeft = Sprite::create(getBlurSpritePath(sprite->getTag()));
	blurLeft->setPosition(sprite->getPosition());
	blurLeft->setScale(2.0f);
	blurLeft->setAnchorPoint(sprite->getAnchorPoint());
	//blurLeft->setColor(Color3B(ROLECOLOR));
	blurLeft->setVisible(true);
	addChild(blurLeft, MIDDLE_TAG, tag);
	
	MoveBy * moveUpAction = MoveBy::create(2.5f, Vec2(0, 5));
	MoveBy * moveDownAction = MoveBy::create(2.5f, Vec2(0, -5));
	RepeatForever * repeapAction = RepeatForever::create(Sequence::create(moveUpAction, moveDownAction, NULL));
	blurLeft->runAction(repeapAction);

	auto pSpriteTable = Sprite::create("res_ui/creating_a_role/taizi_b.png");
	pSpriteTable->setPosition(sprite->getPosition()+Vec2(OFFSET_WIDTH*0.5f, -165));
	pSpriteTable->setAnchorPoint(Vec2(0.5f, 0.5f));
	pSpriteTable->setScale(1.55f);
	if(tag == BLUR_LEFT_TAG)
	{
		pSpriteTable->setTag(TABLE_LEFT_TAG);
	}
	else
	{
		pSpriteTable->setTag(TABLE_RIGHT_TAG);
	}
	pSpriteTable->setVisible(true);
	addChild(pSpriteTable, 1);
	
	moveUpAction = MoveBy::create(2.5f, Vec2(0, 5));
	moveDownAction = MoveBy::create(2.5f, Vec2(0, -5));
	repeapAction = RepeatForever::create(Sequence::create(moveUpAction, moveDownAction, NULL));
	pSpriteTable->runAction(repeapAction);
}

void CreateRoleLayer::hideBlurSprite()
{
	return;
	auto left = (Sprite*)getChildByTag(BLUR_LEFT_TAG);
	auto right = (Sprite*)getChildByTag(BLUR_RIGHT_TAG);
	auto tableLeft = (Sprite*)getChildByTag(TABLE_LEFT_TAG);
	auto tableRight = (Sprite*)getChildByTag(TABLE_RIGHT_TAG);
	left->removeFromParentAndCleanup(true);
	right->removeFromParentAndCleanup(true);
	tableLeft->removeFromParentAndCleanup(true);
	tableRight->removeFromParentAndCleanup(true);
}

char* CreateRoleLayer::getBlurSpritePath(int status)
{
	switch(status)
	{
	case 1:
		return "res_ui/creating_a_role/mengjiang2.png";
	case 2:
		return "res_ui/creating_a_role/guimou2.png";
	case 3:
		return "res_ui/creating_a_role/haojie2.png";
	case 4:
		return "res_ui/creating_a_role/shenshe2.png";
	}
	return NULL;
}

void CreateRoleLayer::onButtonGetClicked(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

		// SDK������루ע��ӿڣ�
		// [������ɫ�Ľ��淵� ������ �ѡ�������Ľ��棬֮ǰ� ǵ�¼���棨���ص�¼������Ҫ��ע��ӿڣ�����ѡ���������治��Ҫ��ע��ӿڣ�]
		//ShareCreateRole::logout();

#endif

		//runLogin();
		auto pScene = new DefaultServerState();
		if (pScene)
		{
			pScene->runThisState();
			pScene->release();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void CreateRoleLayer::onClickedArrow(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto btn = (Button*)pSender;
		if (btn->getTag() == ARROW_LEFT)
			spriteRunAction(RUNACTION_TIME, kTagTouchedRight);
		else
			spriteRunAction(RUNACTION_TIME, kTagTouchedLeft);
	}

	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void CreateRoleLayer::onTouchedRandomName(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		// test 1
		{
			cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
			std::string url = Login::s_loginserver_ip;
			url.append(":48688/randomname");
			request->setUrl(url.c_str());
			request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
			request->setResponseCallback(this, httpresponse_selector(CreateRoleLayer::onHttpRequestCompleted));

			RandomNameReq httpReq;
			httpReq.set_userid(Login::userId);
			httpReq.set_sessionid(Login::sessionId);
			if ((mSpriteStatus == 1) || (mSpriteStatus == 4))
			{
				httpReq.set_gender(1);
			}
			else
			{
				httpReq.set_gender(0);
			}
			httpReq.set_serverid(m_pServerListRsp->serverinfos(mCurrentServerId).serverid());

			string msgData;
			httpReq.SerializeToString(&msgData);

			request->setRequestData(msgData.c_str(), msgData.size());
			request->setTag("POST");
			//CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
			cocos2d::network::HttpClient::getInstance()->send(request);
			request->release();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void CreateRoleLayer::onHttpRequestCompletedCreateRole(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
	if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }

	CreateRoleRsp createRoleRsp;
	CCAssert(response->getResponseData()->size() > 0, "should not be empty");
	createRoleRsp.ParseFromString(response->getResponseData()->data());

	CCLOG("createRoleRsp.result() = %d\n", createRoleRsp.result());
	CCLOG("createRoleRsp.resultmessage() = %s\n",createRoleRsp.resultmessage().c_str());

   if(1 == createRoleRsp.result())
	{
		LoginLayer::mRoleId = createRoleRsp.roleid();
		CCLOG("LoginLayer::mRoleId = %d\n", LoginLayer::mRoleId);

		Size s = Director::getInstance()->getVisibleSize();
		Rect insetRect = Rect(32,15,1,1);
		auto backGround = cocos2d::extension::Scale9Sprite::create("res_ui/kuang_1.png");
		backGround->setCapInsets(insetRect);
		backGround->setPreferredSize(Size(550, 50));
		backGround->setPosition(Vec2(s.width/2, s.height/2));
		backGround->setAnchorPoint(Vec2(0.5f, 0.5f));
		backGround->setTag(kTagPreparingBackGround);
		backGround->setVisible(false);
		addChild(backGround, PROMPT_LOG_TAG);

		// show tip
		auto label = Label::createWithTTF(StringDataManager::getString("con_enter_game"), "Arial", 25);
		addChild(label,PROMPT_LOG_TAG);
		label->setTag(kTagPreparingTip);
		label->setAnchorPoint(Vec2(0.5f, 0.5f));
		label->setPosition( Vec2(s.width/2, s.height/2) );
		label->setVisible(false);


		ActionInterval *action = (ActionInterval*)Sequence::create
			(
				//Show::create(),
				DelayTime::create(0.5f),
				CallFunc::create(CC_CALLBACK_0(CreateRoleLayer::connectGameServer, this)),
				NULL
			);
		label->runAction(action);

		m_state = kStateConnectingGameServer;

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

		// ��ʾ���������
//        ShareCreateRole::showFloatView();

#endif
	}
	else
	{
		GameView::getInstance()->showAlertDialog(createRoleRsp.resultmessage());
	}

}

void CreateRoleLayer::onHttpRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response)
{
    if (!response)
    {
        return;
    }
    
    // You can get original request type from: response->request->reqType
    if (0 != strlen(response->getHttpRequest()->getTag())) 
    {
        CCLOG("%s completed", response->getHttpRequest()->getTag());
    }
    
    int statusCode = response->getResponseCode();
    char statusString[64] = {};
    sprintf(statusString, "HTTP Status Code: %d, tag = %s", statusCode, response->getHttpRequest()->getTag());
    //m_labelStatusCode->setString(statusString);
    CCLOG("response code: %d", statusCode);
    
    if (!response->isSucceed()) 
    {
        CCLOG("response failed");
        CCLOG("error buffer: %s", response->getErrorBuffer());
        return;
    }

   
    // dump data
	m_pRandomNameRsp->ParseFromString(response->getResponseData()->data());

	if(NULL != pNameLayer->getInputString())
	{
		pNameLayer->deleteAllInputString();
	}
	pNameLayer->onTextFieldInsertText(NULL, m_pRandomNameRsp->rolename().c_str(), m_pRandomNameRsp->rolename().size());
	/*
	auto labelRandomName ;
	labelRandomName = (Label*)getChildByTag(55);

	labelRandomName->setString(m_pRandomNameRsp->rolename().c_str());
	*/
	CCLOG("response.rolename = %s", m_pRandomNameRsp->rolename().c_str());
}

void CreateRoleLayer::menuSelectCountry(Ref* pSender)
{
	Size winSize = Director::getInstance()->getVisibleSize();

	auto btn = (Button *)pSender;
	mCountryFlag = btn->getTag();

	updateCountryButton(mCountryFlag);
}

void CreateRoleLayer::menuSelectRoleCallback(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (NULL == pNameLayer->getInputString())
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("login_random_name_tip"));
			return;
		}
		cocos2d::network::HttpRequest* request = new cocos2d::network::HttpRequest();
		std::string url = Login::s_loginserver_ip;
		url.append(":48688/createrole");
		request->setUrl(url.c_str());
		request->setRequestType(cocos2d::network::HttpRequest::Type::POST);
		request->setResponseCallback(this, httpresponse_selector(CreateRoleLayer::onHttpRequestCompletedCreateRole));

		CreateRoleReq httpReq;
		httpReq.set_userid(Login::userId);
		httpReq.set_sessionid(Login::sessionId);
		httpReq.set_serverid(m_pServerListRsp->serverinfos(mCurrentServerId).serverid());
		CCLOG("m_pServerListRsp->serverinfos(0).serverid() = %d", m_pServerListRsp->serverinfos(mCurrentServerId).serverid());
		httpReq.set_mode(0);

		httpReq.set_rolename(pNameLayer->getInputString());
		httpReq.set_profession(mSpriteStatus);
		if ((mSpriteStatus == 1) || (mSpriteStatus == 4))
		{
			httpReq.set_gender(1);
		}
		else
		{
			httpReq.set_gender(0);
		}
		httpReq.set_faceid("test");
		httpReq.set_country(0);

		// mac
		std::string macId = GameUtils::getAppUniqueID();
		httpReq.set_mac(macId);

		// os � platform
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
		httpReq.set_os(1);
		httpReq.set_platform("laoh");
#elif defined(CC_TARGET_OS_IPHONE)
		httpReq.set_os(MACHINE_TYPE);
		httpReq.set_platform(OPERATION_PLATFORM);
#endif

		string msgData;
		httpReq.SerializeToString(&msgData);

		request->setRequestData(msgData.c_str(), msgData.size());
		request->setTag("POST");
		CCLOG("request data = %s, msgData = %s, sizeof(request) = %d", request, &msgData, sizeof(*request));
		cocos2d::network::HttpClient::getInstance()->send(request);
		request->release();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}



}

void CreateRoleLayer::onSocketOpen(ClientNetEngine* socketEngine)
{
	// request to enter game
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1001, this);

	auto pScene = new GameSceneState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}
}
void CreateRoleLayer::onSocketClose(ClientNetEngine* socketEngine)
{	
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	// player can re-connect
	if(m_state == kStateConnectingGameServer)
	{
		this->getChildByTag(kTagPreparingBackGround)->removeFromParent();
		this->getChildByTag(kTagPreparingTip)->removeFromParent();
		// enable role list
		/*for(int i = 0; i < MAX_LOGIN_ROLE_NUM; i++)
		{
			Menu* menu = (Menu*)this->getChildByTag(kTagRoleListMenu+i);
			menu->setEnabled(true);
		}
		*/
		m_state = kStateRoleList;
	}
}
void CreateRoleLayer::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

int CreateRoleLayer::isTouchedPosInButton(Vec2 touchPos)
{
	Size s = Director::getInstance()->getVisibleSize();
	if(touchPos.x < s.width
		&&touchPos.x > s.width/2+180-100
		&&touchPos.y < s.height/2+80+120
		&&touchPos.y > s.height/2+80-180)
	{
		return kStateTouchedRight;
	}
	else if(touchPos.x < s.width/2-180+100
		&&touchPos.x > 0
		&&touchPos.y < s.height/2+80+120
		&&touchPos.y > s.height/2+80-180)
	{
		return kStateTouchedLeft;
	}
	else
	{
		return kStateTouchedNone;
	}
}

void CreateRoleLayer::TouchesBegan(__Set *pTouches, Event *pEvent)
{
	__SetIterator it = pTouches->begin();
    Touch* touch = (Touch*)(*it);

    m_tBeginPos = touch->getLocation();    
	m_iTouchedState = isTouchedPosInButton(m_tBeginPos);

	struct timeval m_sStartTime;
	gettimeofday(&m_sStartTime, NULL);
	m_dStartTime = m_sStartTime.tv_sec*1000 + m_sStartTime.tv_usec/1000;
}

void CreateRoleLayer::TouchesMoved(__Set *pTouches, Event *pEvent)
{
}

void CreateRoleLayer::TouchesEnded(__Set *pTouches, Event *pEvent)
{
	__SetIterator it = pTouches->begin();
    Touch* touch = (Touch*)(*it);
    
    Vec2 location = touch->getLocation();

	struct timeval m_sEndedTime;
	gettimeofday(&m_sEndedTime, NULL);
	m_dEndedTime = m_sEndedTime.tv_sec*1000 + m_sEndedTime.tv_usec/1000;

	if(m_iTouchedState != kStateTouchedNone
		&&isTouchedPosInButton(location)!=kStateTouchedNone
		&&(location.x - m_tBeginPos.x < 5||location.x - m_tBeginPos.x > -5)
		&&!isTouchSlipped)
	{
		if(m_iTouchedState == kStateTouchedLeft)
			spriteRunAction(RUNACTION_TIME, kTagTouchedRight);
		else
			spriteRunAction(RUNACTION_TIME, kTagTouchedLeft);
	}
	else
	{
		float velocity = (float)(location.x - m_tBeginPos.x)/(float)(m_dEndedTime - m_dStartTime);

		if((velocity > 0.25f||velocity < -0.25f)&&(!isTouchSlipped))
		{
			this->stopAllActions();
			if(velocity > 0.0f)
			{
				float time = (200.0f/velocity)/1000.0f;
				spriteRunAction(RUNACTION_TIME, kTagTouchedRight);
			}
			else if(velocity < 0.0f)
			{
				float time = -(200.0f/velocity)/1000.0f;
				spriteRunAction(RUNACTION_TIME, kTagTouchedLeft);
			}
		}
	}
}

///////////////////////////////////////////////////////////////////////////////////////

enum {
	kTagMainLayer = 1,
};

CreateRoleState::~CreateRoleState()
{
}

void CreateRoleState::runThisState()
{
    auto pLayer = new CreateRoleLayer();
    addChild(pLayer, 0, kTagMainLayer);
	pLayer->release();

	if(Director::getInstance()->getRunningScene() != NULL)
		Director::getInstance()->replaceScene(this);
	else
		Director::getInstance()->runWithScene(this);
}

void CreateRoleState::onSocketOpen(ClientNetEngine* socketEngine)
{
	auto layer = (CreateRoleLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketOpen(socketEngine);
}
void CreateRoleState::onSocketClose(ClientNetEngine* socketEngine)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	auto layer = (CreateRoleLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketClose(socketEngine);
}
void CreateRoleState::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}
