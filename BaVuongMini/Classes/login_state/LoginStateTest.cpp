#include "LoginStateTest.h"

#include "../gamescene_state/GameSceneState.h"
#include "../loadscene_state/LoadSceneState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../legend_engine/LegendLevel.h"
#include "../ui/extensions/CCRichLabel.h"
#include "../ui/extensions/RichElement.h"

#include "../utils/GameUtils.h"

#include "../messageclient/GameMessageProcessor.h"
#include "../messageclient/ClientNetEngine.h"

#include "../ui/extensions/RichTextInput.h"
#include "../GameView.h"
#include "../ui/generals_ui/GeneralsUI.h"
#include "../utils/StaticDataManager.h"

#define THREEKINGDOMS_HOST_INTERNET "qianliang2013.vicp.cc"//"61.149.218.125"//"192.168.1.117"
#define THREEKINGDOMS_PORT_INTERNET 8088
#define THREEKINGDOMS_HOST_WANMEI "124.202.137.33"
#define THREEKINGDOMS_PORT_WANMEI 8088
#define THREEKINGDOMS_HOST_STUDIO "139.99.49.200"  
#define THREEKINGDOMS_PORT_STUDIO 8188
#define THREEKINGDOMS_HOST_GBK "192.168.1.118"  
#define THREEKINGDOMS_PORT_GBK 8088  
#define THREEKINGDOMS_HOST_ZHAGNGJING "192.168.1.132"  
#define THREEKINGDOMS_PORT_ZHAGNGJING 8088  
#define THREEKINGDOMS_HOST_LOCAL "127.0.0.1"  
#define THREEKINGDOMS_PORT_LOCAL 8088  

//static std::string MonsterAnimActionNameList[] =
static const char* role_list[] = 
{
    "YangJun(16)",
	"YangJun1(17)",
    "ZG(MJ)(18)",
	"ZG(GM)(19)",
	"LZX(20)",
	"LZX(21)",
	"GBK(HJ)(22)",
	"GBK(SS)(23)",
    "Test01(24)",
	"Test02(25)",
    "Test03(26)",
	"Test04(27)",
	"Test05(28)",
	"Test05(29)",
	"Test05(30)",
	"Test05(31)",
	"Test05(32)",
	"Test05(33)",
	"Test05(34)",
	"Test05(35)",
	"MengJiang(36)",
	"GuiMou(37)",
	"HaoJie(38)",
	"ShenShe(39)",
	"Test(40)",
	"Test(41)",
	"Test(42)",
}; 
#define MAX_LOGIN_ROLE_NUM 27
#define MAX_ROLE_COLUMN_NUM 9

static const char* server_list[] = 
{
    "Studio",
    "GBK",
	"ZhangJing",
	"Local",
	"Internet",
	"WANMEI",
}; 
#define MAX_GAMESERVER_NUM 6

enum {
	kStateRoleList = 0,
	kStateConnectingGameServer,
};

enum {
	kTagPreparingBackGround = 2,
	kTagPreparingTip = 3,
	kTagServerTip = 4,
	kTagRoleListMenu = 16,   // last one !! please add more tag before it
};

LoginTestLayer::LoginTestLayer()
: m_state(kStateRoleList)
, mCurrentServerId(0)
{
	////setTouchEnabled(true);
	scheduleUpdate();

    Size s = Director::getInstance()->getVisibleSize();

	// current server tip
	auto label = Label::createWithTTF("Server: ", "Arial", 25);
	label->setAnchorPoint(Vec2(0,0));
	label->setPosition(Vec2(5, s.height - 10));
	//addChild(label);

	auto server_name_label = Label::createWithTTF("", "Arial", 30);
	server_name_label->setAnchorPoint(Vec2(0,0));
	server_name_label->setPosition(Vec2(5, s.height - 50));
	addChild(server_name_label, 0, kTagServerTip);
	server_name_label->setString(server_list[mCurrentServerId]);

	// role list
	for(int i = 0; i < MAX_LOGIN_ROLE_NUM; i++)
	{
		MenuItemFont::setFontSize(25);
		MenuItemFont::setFontName("Marker Felt");
		auto link = MenuItemFont::create(role_list[i], CC_CALLBACK_1(LoginTestLayer::menuSelectRoleCallback,this));
		link->setPositionX(link->getContentSize().width/2);
		link->setColor(Color3B(255, 0, 0));
		link->setAnchorPoint(Vec2(0.5f,0));
		link->setTag(kTagRoleListMenu+i);
		
		auto menu = Menu::create(link, NULL);
		int offsetX = i / MAX_ROLE_COLUMN_NUM;
		int offsetY = i % MAX_ROLE_COLUMN_NUM;
		menu->setPosition( Vec2( 150 + offsetX * 230, s.height-50-offsetY*50) );
		menu->setTag(kTagRoleListMenu+i);
		addChild(menu);
	}

	// server list
	for(int i = 0; i < MAX_GAMESERVER_NUM; i++)
	{
		MenuItemFont::setFontSize(25);
		MenuItemFont::setFontName("Marker Felt");
		auto link = MenuItemFont::create(server_list[i], CC_CALLBACK_1(LoginTestLayer::menuSelectServerCallback,this));
		link->setPositionX(link->getContentSize().width/2);
		link->setColor(Color3B(0, 255, 0));
		link->setAnchorPoint(Vec2(0.5f,0));
		link->setTag(i);
		
		auto menu = Menu::create(link, NULL);
		menu->setPosition( Vec2( 5, s.height-100-i*50) );
		addChild(menu);
	}

	ClientNetEngine::sharedSocketEngine()->setAddress(THREEKINGDOMS_HOST_STUDIO, THREEKINGDOMS_PORT_STUDIO);
}

LoginTestLayer::~LoginTestLayer()
{
}

void LoginTestLayer::TouchesEnded(__Set *pTouches, Event *pEvent)
{

}

void LoginTestLayer::connectGameServer()
{
	ClientNetEngine::sharedSocketEngine()->init(*(GameView::getInstance()));
}

void LoginTestLayer::update(float dt)
{
	if(m_state == kStateRoleList)
	{

	}
	else if(m_state == kStateConnectingGameServer)
	{
		// todo
		// if timeout, back to kStateRoleList
	}
}

void LoginTestLayer::menuSelectServerCallback(Ref* pSender)
{
	auto button = (Node*)pSender;
	mCurrentServerId = button->getTag();

	// current server tip
	auto label = (Label*)this->getChildByTag(kTagServerTip);
	label->setString(server_list[mCurrentServerId]);

	if(mCurrentServerId == 0)
		ClientNetEngine::sharedSocketEngine()->setAddress(THREEKINGDOMS_HOST_STUDIO, THREEKINGDOMS_PORT_STUDIO);
	else if(mCurrentServerId == 1)
		ClientNetEngine::sharedSocketEngine()->setAddress(THREEKINGDOMS_HOST_GBK, THREEKINGDOMS_PORT_GBK);
	else if(mCurrentServerId == 2)
		ClientNetEngine::sharedSocketEngine()->setAddress(THREEKINGDOMS_HOST_ZHAGNGJING, THREEKINGDOMS_PORT_ZHAGNGJING);
	else if(mCurrentServerId == 3)
		ClientNetEngine::sharedSocketEngine()->setAddress(THREEKINGDOMS_HOST_LOCAL, THREEKINGDOMS_PORT_LOCAL);
	else if(mCurrentServerId == 4)
		ClientNetEngine::sharedSocketEngine()->setAddress(THREEKINGDOMS_HOST_INTERNET, THREEKINGDOMS_PORT_INTERNET);
	else
		ClientNetEngine::sharedSocketEngine()->setAddress(THREEKINGDOMS_HOST_WANMEI, THREEKINGDOMS_PORT_WANMEI);
}

void LoginTestLayer::menuSelectRoleCallback(Ref* pSender)
{

	auto button = (Node*)pSender;
	mSelectedId = button->getTag();

	//
	// changet to kStateConnectingGameServer
	//
	// disable role list
	for(int i = 0; i < MAX_LOGIN_ROLE_NUM; i++)
	{
		auto menu = (Menu*)this->getChildByTag(kTagRoleListMenu+i);
		menu->setEnabled(false);
	}
	
	// tip's background
	Size s = Director::getInstance()->getVisibleSize();
	Rect insetRect = Rect(32,15,1,1);
	auto backGround = cocos2d::extension::Scale9Sprite::create("res_ui/kuang_1.png");
	backGround->setCapInsets(insetRect);
	backGround->setPreferredSize(Size(550, 50));
	backGround->setPosition(Vec2(s.width/2, s.height/2));
	backGround->setAnchorPoint(Vec2(0.5f, 0.5f));
	backGround->setTag(kTagPreparingBackGround);
	addChild(backGround);

	// show tip
	auto label = Label::createWithTTF(StringDataManager::getString("con_enter_game"), "Arial", 25);
	addChild(label);
	label->setTag(kTagPreparingTip);
	label->setAnchorPoint(Vec2(0.5f, 0.5f));
	label->setPosition( Vec2(s.width/2, s.height/2) );

    auto action = (ActionInterval*)Sequence::create
        (
            Show::create(),
			DelayTime::create(0.5f),
            CallFunc::create(CC_CALLBACK_0(LoginTestLayer::connectGameServer,this)),
            NULL
        );
	label->runAction(action);

	m_state = kStateConnectingGameServer;
}

void LoginTestLayer::onSocketOpen(ClientNetEngine* socketEngine)
{
	// request to enter game
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1001, this);
}
void LoginTestLayer::onSocketClose(ClientNetEngine* socketEngine)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	// player can re-connect
	if(m_state == kStateConnectingGameServer)
	{
		this->getChildByTag(kTagPreparingBackGround)->removeFromParent();
		this->getChildByTag(kTagPreparingTip)->removeFromParent();
		// enable role list
		for(int i = 0; i < MAX_LOGIN_ROLE_NUM; i++)
		{
			auto menu = (Menu*)this->getChildByTag(kTagRoleListMenu+i);
			menu->setEnabled(true);
		}

		m_state = kStateRoleList;
	}
}
void LoginTestLayer::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}

///////////////////////////////////////////////////////////////////////////////////////

enum {
	kTagMainLayer = 1,
};

LoginTestState::~LoginTestState()
{
}

void LoginTestState::runThisState()
{
    auto pLayer = new LoginTestLayer();
    addChild(pLayer, 0, kTagMainLayer);
	pLayer->release();

	if(Director::getInstance()->getRunningScene() != NULL)
		Director::getInstance()->replaceScene(this);
	else
		Director::getInstance()->runWithScene(this);
}

void LoginTestState::onSocketOpen(ClientNetEngine* socketEngine)
{
	auto layer = (LoginTestLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketOpen(socketEngine);
}
void LoginTestState::onSocketClose(ClientNetEngine* socketEngine)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("con_reconnection"));

	auto layer = (LoginTestLayer*)this->getChildByTag(kTagMainLayer);
	layer->onSocketClose(socketEngine);
}
void LoginTestState::onSocketError(ClientNetEngine* socketEngines, const ClientNetEngine::ErrorCode& error)
{
}
