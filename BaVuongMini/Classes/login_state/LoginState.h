#ifndef _GAMESTATE_LOGIN_STATE_H_
#define _GAMESTATE_LOGIN_STATE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../GameStateBasic.h"
#include "../messageclient/ClientNetEngine.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "../ui/extensions/UIScene.h"
#include "network\HttpClient.h"

USING_NS_CC;
USING_NS_CC_EXT;

/**
 login to the game server
 */
class LoginLayer : public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
    LoginLayer();
	virtual ~LoginLayer();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

	virtual void update(float dt);

	virtual void TouchesEnded(__Set *pTouches, Event *pEvent);

	inline int getSelectedRoleId() { return mSelectedId; };

	void onButtonGetClicked(Ref *pSender, Widget::TouchEventType type);
	void onEnterButtonGetClicked(cocos2d::Ref *sender);

	void onMenuGetTestClicked();
	void onHttpRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);

	void onRoleListReq();
	void onRoleListRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
	
	static long  mRoleId;
	static int mCountryMin;
	static int mProfessionMin;

	virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
    virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view) {};
    virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view) {}
    virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
    virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
    virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
    virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);
	/*return the level of the role in last server
		if the level is none,return 0*/
	static int getLastServerRoleLevel();
	static void reconnectServer();

	TableView *tableView;
	TableView* tableViewLast;

	static int s_iLastServerRoleLevel;
private:
	void menuSelectRoleCallback(Ref* pSender);
	void menuSelectServerCallback(Ref* pSender);

	void connectGameServer();

	void initLastServer(int idx);
	void onLastServerGetClicked(Ref *pSender, Widget::TouchEventType type);

	void requestServerList();
	void onRequestServerListCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
	void reloadServerInfo();
private:
	int m_state;

	long double m_dStartTime;
	long double m_dEndedTime;

	int mSelectedId;

	static int mCurrentServerId;

	ServerListRsp* m_pServerListRsp;

	int mTouchedId;
	int mTouchedStatus;
	int lastServerIndex;
	bool isLastSelected;
	bool isTableViewTouched;

	int m_iStatus;
public:
	static int getCurrentServerId(){return mCurrentServerId;};
	
};

class LoginState : public GameState
{
public:
	~LoginState();

    virtual void runThisState();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

	static void addAppVersionInfo(Node* parent);

};

#endif // _GAMESTATE_LOGIN_STATE_H_
