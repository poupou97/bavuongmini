#ifndef _GAMESTATE_CREATEROLE_STATE_H_
#define _GAMESTATE_CREATEROLE_STATE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../GameStateBasic.h"
#include "../messageclient/ClientNetEngine.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "../messageclient/reqsender/ReqSenderProtocol.h"
#include "Login.h"
#include "../gamescene_state/role/BasePlayer.h"
#include "SpriteBlur.h"
#include "network\HttpClient.h"

USING_NS_CC;

/**
 create a role to the game server
 */
class CreateRoleLayer : public Layer, public ClientNetEngine::Delegate
{
public:
    CreateRoleLayer();
	virtual ~CreateRoleLayer();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

	virtual void update(float dt);

	virtual void TouchesEnded(__Set *pTouches, Event *pEvent);
	virtual void TouchesBegan(__Set *pTouches, Event *pEvent);
	virtual void TouchesMoved(__Set *pTouches, Event *pEvent);

	inline int getSelectedRoleId() { return mSelectedId; }

	void onButtonGetClicked(Ref *pSender, Widget::TouchEventType type);
	
	void onHttpRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);

	void onHttpRequestCompletedCreateRole(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);

	void TouchedLiangZhou(Ref *pSender);
	void TouchedYouZhou(Ref *pSender);
	void TouchedYiZhou(Ref *pSender);
	void TouchedJingZhou(Ref *pSender);
	void TouchedYangZhou(Ref *pSender);

	void callBackExit(Ref * obj);
private:
	void menuSelectRoleCallback(Ref *pSender, Widget::TouchEventType type);
	void menuSelectCountry(Ref* pSender);
	void onTouchedRandomName(Ref *pSender, Widget::TouchEventType type);
	void onClickedArrow(Ref *pSender, Widget::TouchEventType type);
	void connectGameServer();
	void initProfessionSprite(int minProfession);
	//void initCountrySprite(int minCountry);
	void initProfessionIcon(int profession);
	void updateProfessionIcon(int profession);
	void updateSpriteFront(int profession);
	void onClickIconButton(Ref *pSender, Widget::TouchEventType type);
	void spriteRunAction(float time, int mode);
	void updateCountryButton(int country);
	void onClickSpriteButton(Ref *senderz, cocos2d::extension::Control::EventType controlEvent);
	int isTouchedPosInButton(Vec2 touchPos);
	
	void setSpriteState(float scale, int mode);
	void setSpriteRightState(Sprite* sprite, float scale, int status);
	void setSpriteLeftState(Sprite* sprite, float scale, int status);

	void showBlurSprite(SpriteBlur* sprite, int flag);
	void hideBlurSprite();
	char* getBlurSpritePath(int status);
private:
	int m_state;

	//struct timeval m_sStartTime;
	long double m_dStartTime;
	long double m_dEndedTime;

	int mSpriteStatus;

	int mSelectedId;

	int mCurrentServerId;

	RandomNameRsp* m_pRandomNameRsp;

	ServerListRsp* m_pServerListRsp;
	RichTextInputBox * pNameLayer;
	
	Vec2 m_tBeginPos;

	int mCountryFlag;
	int isTouchSlipped;
	int mActionSpriteTag;
	int mZOrder;
	bool isRunActionTwice;

	int m_iTouchedState;
	int m_iNum;

	Sprite* m_pBlurLeft;
	Sprite* m_pBlurRight;
protected:
	Layer *m_CountryLayer;
};

class CreateRoleState : public GameState
{
public:
	~CreateRoleState();

    virtual void runThisState();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

};

#endif // _GAMESTATE_LOGIN_STATE_H_
