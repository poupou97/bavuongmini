#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"
#include "../GameStateBasic.h"
#include "../messageclient/ClientNetEngine.h"
#include "../messageclient/protobuf/LoginMessage.pb.h"
#include "../ui/extensions/UIScene.h"
#include "network\HttpClient.h"

USING_NS_CC;
USING_NS_CC_EXT;

/**
 login to the game server
 */
class DefaultServerLayer : public UIScene
{
public:
    DefaultServerLayer();
	virtual ~DefaultServerLayer();

	void initLayer();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);
	
	virtual void update(float dt);

	virtual void TouchesEnded(__Set *pTouches, Event *pEvent);

private:
	void menuSelectRoleCallback(Ref* pSender);
	void menuSelectServerCallback(Ref* pSender);

	void connectGameServer();

	void initLastServer(int idx);
	void onLastServerGetClicked(Ref *pSender, Widget::TouchEventType type);
	
	void onRoleListReq();
	void onRoleListRequestCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);
	void onGetMinReq();
	void onGetMinResp(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);

	void requestServerList();
	void onRequestServerListCompleted(cocos2d::network::HttpClient *sender, cocos2d::network::HttpResponse *response);

	void reloadServerInfo();
	void onEnterGetClicked(Ref *pSender, Widget::TouchEventType type);
	void onChangeGetClicked(Ref *pSender, Widget::TouchEventType type);
private:
	int m_state;

	long double m_dStartTime;
	long double m_dEndedTime;

	int mSelectedId;

	static int mCurrentServerId;

	ServerListRsp* m_pServerListRsp;

	int mTouchedId;
	int mTouchedStatus;
	int lastServerIndex;
	bool isLastSelected;
	bool isTableViewTouched;

	int m_iStatus;
public:
	static int getCurrentServerId(){return mCurrentServerId;};
	static void setCurrentServerId(int serverIndex){mCurrentServerId = serverIndex;}
	
};

class DefaultServerState : public GameState
{
public:
	~DefaultServerState();

    virtual void runThisState();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);
};
