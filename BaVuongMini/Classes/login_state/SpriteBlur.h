#pragma once
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class SpriteBlur : public Sprite
{
public:
    ~SpriteBlur();
    void setBlurSize(float f);
    bool initWithTexture(Texture2D* texture, const Rect&  rect);
    void draw(Renderer *renderer, const Mat4& transform, uint32_t flags);
    void initProgram();
    void listenBackToForeground(Ref *obj);

    static SpriteBlur* create(const char *pszFileName);

    Vec2 blur_;
    GLfloat    sub_[4];

    GLuint    blurLocation;
    GLuint    subLocation;
};
