#include "MoutainBackGroundLayer.h"

#include "../legend_engine/CCLegendAnimation.h"

#define BG1_ROLL_TIME 60.0f
#define BG1_FIRST_TAG 120
#define BG1_SECOND_TAG 121

#define BG2_ROLL_TIME 100.0f
#define BG2_FIRST_TAG 130
#define BG2_SECOND_TAG 131

#define BG3_ROLL_TIME 200.0f
#define BG3_FIRST_TAG 140
#define BG3_SECOND_TAG 141

#define BG4_ROLL_TIME 1000.0f
#define BG4_FIRST_TAG 150
#define BG4_SECOND_TAG 151

#define BG_IMAGE_HEIGHT 640

MoutainBackGroundLayer::MoutainBackGroundLayer()
{

}

MoutainBackGroundLayer::~MoutainBackGroundLayer()
{
}

MoutainBackGroundLayer* MoutainBackGroundLayer::create()
{
    auto pRet = new MoutainBackGroundLayer();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

bool MoutainBackGroundLayer::init()
{
	if(Layer::init())
	{
		Size winSize = Director::getInstance()->getWinSize();

		BG4_scroll();
		BG3_scroll();
		BG2_scroll();
		BG1_scroll();

		//addBirds(this);

		

		//addBuddhaLight(pSprite, pSpriteNext);

		return true;
	}
	return false;
}

void MoutainBackGroundLayer::addBirds(Node* parent)
{
	Size s = Director::getInstance()->getVisibleSize();

	const char* birdAnimFileName = "animation/texiao/changjingtexiao/xianhe/xianhe.anm";

	// birds
	// the head bird
	auto pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(Vec2(0.5f,0.5f));
	pDoorNode->setPosition(Vec2(s.width/2-200, s.height-50));
	pDoorNode->setScale(0.33f);
	parent->addChild(pDoorNode);

	pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(Vec2(0.5f,0.5f));
	pDoorNode->setPosition(Vec2(s.width/2-213, s.height-42));
	pDoorNode->setScale(0.28f);
	parent->addChild(pDoorNode);

	pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(Vec2(0.5f,0.5f));
	pDoorNode->setPosition(Vec2(s.width/2-226, s.height-34));
	pDoorNode->setScale(0.26f);
	parent->addChild(pDoorNode);

	pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(Vec2(0.5f,0.5f));
	pDoorNode->setPosition(Vec2(s.width/2-225, s.height-60));
	pDoorNode->setScale(0.38f);
	parent->addChild(pDoorNode);

	pDoorNode = CCLegendAnimation::create(birdAnimFileName);
	pDoorNode->setPlayLoop(true);
	pDoorNode->setReleaseWhenStop(false);
	pDoorNode->setAnchorPoint(Vec2(0.5f,0.5f));
	pDoorNode->setPosition(Vec2(s.width/2-250, s.height-70));
	pDoorNode->setScale(0.4f);
	parent->addChild(pDoorNode);
}

void MoutainBackGroundLayer::addWaterfall1(Node* parent, Node* next)
{
	Size s = Director::getInstance()->getVisibleSize();

	const char* pWaterfallAnm = "animation/texiao/changjingtexiao/pubu/pubu03.anm";

	CCLegendAnimation* pWaterfallLong = CCLegendAnimation::create(pWaterfallAnm);
	pWaterfallLong->setPlayLoop(true);
	pWaterfallLong->setReleaseWhenStop(false);
	pWaterfallLong->setAnchorPoint(Vec2(0.5f,0.5f));
	pWaterfallLong->setPosition(Vec2(255, 120));
	pWaterfallLong->setScaleY(1.55f);
	//pWaterfall->setScaleX(0.6f);
	parent->addChild(pWaterfallLong);

	CCLegendAnimation* pWaterfallLongNext = CCLegendAnimation::create(pWaterfallAnm);
	pWaterfallLongNext->setPlayLoop(true);
	pWaterfallLongNext->setReleaseWhenStop(false);
	pWaterfallLongNext->setAnchorPoint(Vec2(0.5f,0.5f));
	pWaterfallLongNext->setPosition(Vec2(255, 120));
	pWaterfallLongNext->setScaleY(1.55f);
	//pWaterfall->setScaleX(0.6f);
	next->addChild(pWaterfallLongNext);
}
void MoutainBackGroundLayer::addWaterfall2(Node* parent, Node* next)
{
	Size s = Director::getInstance()->getVisibleSize();

	const char* pWaterfallAnm = "animation/texiao/changjingtexiao/pubu/pubu03.anm";
	
	auto pWaterfallShort = CCLegendAnimation::create(pWaterfallAnm);
	pWaterfallShort->setPlayLoop(true);
	pWaterfallShort->setReleaseWhenStop(false);
	pWaterfallShort->setAnchorPoint(Vec2(0.5f,0.5f));
	pWaterfallShort->setPosition(Vec2(1905, 20));
	//pWaterfallShort->setName("water");
	pWaterfallShort->setTag(0);
	pWaterfallShort->setScaleY(0.4f);
	pWaterfallShort->setScaleX(-3.0f);
	parent->addChild(pWaterfallShort,0,"water");


	
	auto pRiver = CCLegendAnimation::create(pWaterfallAnm);
	pRiver->setPlayLoop(true);
	pRiver->setReleaseWhenStop(false);
	pRiver->setAnchorPoint(Vec2(0.5f,0.5f));
	pRiver->setPosition(Vec2(1935, 65));
	//pRiver->setName("river");
	pRiver->setTag(1);
	pRiver->setScaleY(0.7f);
	pRiver->setScaleX(-1.2f);
	pRiver->setRotation(60);
	parent->addChild(pRiver,0,"river");

	auto pWaterfallShortNext = CCLegendAnimation::create(pWaterfallAnm);
	pWaterfallShortNext->setPlayLoop(true);
	pWaterfallShortNext->setReleaseWhenStop(false);
	pWaterfallShortNext->setAnchorPoint(Vec2(0.5f,0.5f));
	pWaterfallShortNext->setPosition(Vec2(1905, 20));
	pWaterfallShortNext->setScaleY(0.4f);
	pWaterfallShortNext->setScaleX(-3.0f);
	next->addChild(pWaterfallShortNext);
	
	auto pRiverNext = CCLegendAnimation::create(pWaterfallAnm);
	pRiverNext->setPlayLoop(true);
	pRiverNext->setReleaseWhenStop(false);
	pRiverNext->setAnchorPoint(Vec2(0.5f,0.5f));
	pRiverNext->setPosition(Vec2(1935, 65));
	pRiverNext->setScaleY(0.7f);
	pRiverNext->setScaleX(-1.2f);
	pRiverNext->setRotation(60);
	next->addChild(pRiverNext);
}

ParticleSystem* MoutainBackGroundLayer::generateSmokeEffect()
{
	const char* EFFECTNAME = "animation/texiao/particledesigner/langyan.plist";

	// smoke effect
	auto particleEffect = ParticleSystemQuad::create(EFFECTNAME);
	particleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);

	// fire effect
	const char* FIRE_EFFECTNAME = "animation/texiao/particledesigner/huo2.plist";
	auto fireEffect = ParticleSystemQuad::create(FIRE_EFFECTNAME);
	fireEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	fireEffect->setScaleX(0.2f);
	fireEffect->setPosition(0, 0);

	particleEffect->addChild(fireEffect);

	return particleEffect;
}

void MoutainBackGroundLayer::addSmoke1(Node* parent, Node* next)
{
	// effect 1
	auto particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.2f);
	particleEffect->setPosition(Vec2(74, 471));
	parent->addChild(particleEffect);

	particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.2f);
	particleEffect->setPosition(Vec2(74, 471));
	next->addChild(particleEffect);

	// effect 2
	particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.3f);
	particleEffect->setPosition(Vec2(1507, 362));
	parent->addChild(particleEffect);

	particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.3f);
	particleEffect->setPosition(Vec2(1507, 362));
	next->addChild(particleEffect);

	// effect 3
	particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.3f);
	particleEffect->setPosition(Vec2(1053, 354));
	parent->addChild(particleEffect);

	particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.3f);
	particleEffect->setPosition(Vec2(1053, 354));
	next->addChild(particleEffect);
}

void MoutainBackGroundLayer::addSmoke2(Node* parent, Node* next)
{
	// effect 1
	auto particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.1f);
	particleEffect->setPosition(Vec2(1247, 563));
	parent->addChild(particleEffect);

	particleEffect = generateSmokeEffect();
	particleEffect->setScale(0.1f);
	particleEffect->setPosition(Vec2(1247, 563));
	next->addChild(particleEffect);
}

void MoutainBackGroundLayer::addBuddhaLight(Node* parent, Node* next)
{
	Size s = Director::getInstance()->getVisibleSize();

	const char* pBuddhaLightAnm = "animation/texiao/renwutexiao/feguang/feguang.anm";

	auto pWaterfallLong = CCLegendAnimation::create(pBuddhaLightAnm);
	pWaterfallLong->setPlayLoop(true);
	pWaterfallLong->setReleaseWhenStop(false);
	pWaterfallLong->setAnchorPoint(Vec2(0.5f,0.5f));
	pWaterfallLong->setPosition(Vec2(1472, 178));
	parent->addChild(pWaterfallLong);

	auto pWaterfallLongNext = CCLegendAnimation::create(pBuddhaLightAnm);
	pWaterfallLongNext->setPlayLoop(true);
	pWaterfallLongNext->setReleaseWhenStop(false);
	pWaterfallLongNext->setAnchorPoint(Vec2(0.5f,0.5f));
	pWaterfallLongNext->setPosition(Vec2(1472, 178));
	next->addChild(pWaterfallLongNext);

}

void MoutainBackGroundLayer::BG1_scroll()
{
	Size winSize = Director::getInstance()->getWinSize();

	const char* moutain_picture_file = "images/denglu_fc1.png";

	//background image
	Sprite* pSprite = Sprite::create(moutain_picture_file);
	pSprite->setPosition(Vec2(0, 0));
	pSprite->setAnchorPoint(Vec2(0, 0));
	float scaleValue;
	scaleValue = (float)winSize.height/(float)BG_IMAGE_HEIGHT;
	pSprite->setScale(scaleValue);
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;
	pSprite->setTag(BG1_FIRST_TAG);
	//pSprite->setColor(Color3B(160, 160, 160));
	addChild(pSprite, 0);

	auto pSpriteNext = Sprite::create(moutain_picture_file);
	pSpriteNext->setPosition(Vec2(winSize.width-1, 0));
	pSpriteNext->setAnchorPoint(Vec2(0, 0));
	pSpriteNext->setScale(scaleValue);
	pSpriteNext->setTag(BG1_SECOND_TAG);
	addChild(pSpriteNext, 0);

	auto moveToAction = MoveTo::create(BG1_ROLL_TIME,Vec2(-pSprite->getContentSize().width*scaleValue, 0));
	auto moveToReturn = MoveTo::create(0,Vec2(winSize.width-1, 0));
	auto moveToStart = MoveTo::create(BG1_ROLL_TIME/widthMultiple,Vec2(0, 0));
	auto repeapAction = RepeatForever::create(
		Sequence::create(
			moveToAction,
			moveToReturn,
			DelayTime::create(BG1_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			moveToStart,
			NULL));
	pSprite->runAction(repeapAction);
	
	auto nextAction = Sequence::create(
		DelayTime::create(BG1_ROLL_TIME*(widthMultiple-1)/widthMultiple), 
		CallFunc::create(CC_CALLBACK_0(MoutainBackGroundLayer::BG1_repeatFunc,this)),
		NULL);
	pSpriteNext->runAction(nextAction);

	addWaterfall1(pSprite, pSpriteNext);
}
void MoutainBackGroundLayer::BG1_repeatFunc()
{
	Size winSize = Director::getInstance()->getWinSize();
	auto pSprite = (Sprite*)getChildByTag(BG1_SECOND_TAG);
	float scaleValue = pSprite->getScale();
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;

	auto moveToActionNext = MoveTo::create(BG1_ROLL_TIME*(widthMultiple+1)/widthMultiple,Vec2(-pSprite->getContentSize().width*scaleValue, 0));
	auto moveToReturnNext = MoveTo::create(0,Vec2(winSize.width-1, 0));
	auto repeapActionNext = RepeatForever::create(
		Sequence::create(
			moveToActionNext,
			moveToReturnNext,
			DelayTime::create(BG1_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			NULL));
	pSprite->runAction(repeapActionNext);
}

void MoutainBackGroundLayer::BG2_scroll()
{
	Size winSize = Director::getInstance()->getWinSize();

	const char* moutain_picture_file = "images/denglu_fc2.png";

	//background image
	Sprite* pSprite = Sprite::create(moutain_picture_file);
	pSprite->setPosition(Vec2(0, 0));
	pSprite->setAnchorPoint(Vec2(0, 0));
	float scaleValue;
	scaleValue = (float)winSize.height/(float)BG_IMAGE_HEIGHT;
	pSprite->setScale(scaleValue);
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;
	pSprite->setTag(BG2_FIRST_TAG);
	//pSprite->setColor(Color3B(160, 160, 160));
	addChild(pSprite, 0);

	auto pSpriteNext = Sprite::create(moutain_picture_file);
	pSpriteNext->setPosition(Vec2(winSize.width-1, 0));
	pSpriteNext->setAnchorPoint(Vec2(0, 0));
	pSpriteNext->setScale(scaleValue);
	pSpriteNext->setTag(BG2_SECOND_TAG);
	addChild(pSpriteNext, 0);

	auto moveToAction = MoveTo::create(BG2_ROLL_TIME,Vec2(-pSprite->getContentSize().width*scaleValue, 0));
	auto moveToReturn = MoveTo::create(0,Vec2(winSize.width-1, 0));
	auto moveToStart = MoveTo::create(BG2_ROLL_TIME/widthMultiple,Vec2(0, 0));
	auto repeapAction = RepeatForever::create(
		Sequence::create(
			moveToAction,
			moveToReturn,
			DelayTime::create(BG2_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			moveToStart,
			NULL));
	pSprite->runAction(repeapAction);
	
	auto nextAction = Sequence::create(
		DelayTime::create(BG2_ROLL_TIME*(widthMultiple-1)/widthMultiple), 
		CallFunc::create(CC_CALLBACK_0(MoutainBackGroundLayer::BG2_repeatFunc,this)),
		NULL);
	pSpriteNext->runAction(nextAction);

	addWaterfall2(pSprite, pSpriteNext);

	addSmoke1(pSprite, pSpriteNext);
}
void MoutainBackGroundLayer::BG2_repeatFunc()
{
	Size winSize = Director::getInstance()->getWinSize();
	auto pSprite = (Sprite*)getChildByTag(BG2_SECOND_TAG);
	float scaleValue = pSprite->getScale();
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;

	auto moveToActionNext = MoveTo::create(BG2_ROLL_TIME*(widthMultiple+1)/widthMultiple,Vec2(-pSprite->getContentSize().width*scaleValue, 0));
	auto moveToReturnNext = MoveTo::create(0,Vec2(winSize.width-1, 0));
	auto repeapActionNext = RepeatForever::create(
		Sequence::create(
			moveToActionNext,
			moveToReturnNext,
			DelayTime::create(BG2_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			NULL));
	pSprite->runAction(repeapActionNext);
}

void MoutainBackGroundLayer::BG3_scroll()
{
	Size winSize = Director::getInstance()->getWinSize();

	const char* moutain_picture_file = "images/denglu_fc3.png";

	//background image
	auto pSprite = Sprite::create(moutain_picture_file);
	pSprite->setPosition(Vec2(0, 0));
	pSprite->setAnchorPoint(Vec2(0, 0));
	float scaleValue;
	scaleValue = (float)winSize.height/(float)BG_IMAGE_HEIGHT;
	pSprite->setScale(scaleValue);
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;
	pSprite->setTag(BG3_FIRST_TAG);
	//pSprite->setColor(Color3B(160, 160, 160));
	addChild(pSprite, 0);

	auto pSpriteNext = Sprite::create(moutain_picture_file);
	pSpriteNext->setPosition(Vec2(winSize.width-1, 0));
	pSpriteNext->setAnchorPoint(Vec2(0, 0));
	pSpriteNext->setScale(scaleValue);
	pSpriteNext->setTag(BG3_SECOND_TAG);
	addChild(pSpriteNext, 0);

	auto moveToAction = MoveTo::create(BG3_ROLL_TIME,Vec2(-pSprite->getContentSize().width*scaleValue, 0));
	auto moveToReturn = MoveTo::create(0,Vec2(winSize.width-1, 0));
	auto moveToStart = MoveTo::create(BG3_ROLL_TIME/widthMultiple,Vec2(0, 0));
	auto repeapAction = RepeatForever::create(
		Sequence::create(
			moveToAction,
			moveToReturn,
			DelayTime::create(BG3_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			moveToStart,
			NULL));
	pSprite->runAction(repeapAction);
	
	auto nextAction = Sequence::create(
		DelayTime::create(BG3_ROLL_TIME*(widthMultiple-1)/widthMultiple), 
		CallFunc::create(CC_CALLBACK_0(MoutainBackGroundLayer::BG3_repeatFunc,this)),
		NULL);
	pSpriteNext->runAction(nextAction);

	addSmoke2(pSprite, pSpriteNext);
}
void MoutainBackGroundLayer::BG3_repeatFunc()
{
	Size winSize = Director::getInstance()->getWinSize();
	auto pSprite = (Sprite*)getChildByTag(BG3_SECOND_TAG);
	float scaleValue = pSprite->getScale();
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;

	auto moveToActionNext = MoveTo::create(BG3_ROLL_TIME*(widthMultiple+1)/widthMultiple,Vec2(-pSprite->getContentSize().width*scaleValue, 0));
	auto moveToReturnNext = MoveTo::create(0,Vec2(winSize.width-1, 0));
	auto repeapActionNext = RepeatForever::create(
		Sequence::create(
			moveToActionNext,
			moveToReturnNext,
			DelayTime::create(BG3_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			NULL));
	pSprite->runAction(repeapActionNext);
}

void MoutainBackGroundLayer::BG4_scroll()
{
	Size winSize = Director::getInstance()->getWinSize();

	const char* moutain_picture_file = "images/denglu_fc4.jpg";

	//background image
	auto pSprite = Sprite::create(moutain_picture_file);
	pSprite->setPosition(Vec2(0, 0));
	pSprite->setAnchorPoint(Vec2(0, 0));
	float scaleValue;
	scaleValue = (float)winSize.height/(float)BG_IMAGE_HEIGHT;
	pSprite->setScale(scaleValue);
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;
	pSprite->setTag(BG4_FIRST_TAG);
	//pSprite->setColor(Color3B(160, 160, 160));
	addChild(pSprite, 0);

	auto pSpriteNext = Sprite::create(moutain_picture_file);
	pSpriteNext->setPosition(Vec2(winSize.width-1, 0));
	pSpriteNext->setAnchorPoint(Vec2(0, 0));
	pSpriteNext->setScale(scaleValue);
	pSpriteNext->setTag(BG4_SECOND_TAG);
	addChild(pSpriteNext, 0);

	auto moveToAction = MoveTo::create(BG4_ROLL_TIME,Vec2(-pSprite->getContentSize().width*scaleValue, 0));
	auto moveToReturn = MoveTo::create(0,Vec2(winSize.width-1, 0));
	auto moveToStart = MoveTo::create(BG4_ROLL_TIME/widthMultiple,Vec2(0, 0));
	auto repeapAction = RepeatForever::create(
		Sequence::create(
			moveToAction,
			moveToReturn,
			DelayTime::create(BG4_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			moveToStart,
			NULL));
	pSprite->runAction(repeapAction);
	
	auto nextAction = Sequence::create(
		DelayTime::create(BG4_ROLL_TIME*(widthMultiple-1)/widthMultiple), 
		CallFunc::create(CC_CALLBACK_0(MoutainBackGroundLayer::BG4_repeatFunc,this)),
		NULL);
	pSpriteNext->runAction(nextAction);
}
void MoutainBackGroundLayer::BG4_repeatFunc()
{
	Size winSize = Director::getInstance()->getWinSize();
	auto pSprite = (Sprite*)getChildByTag(BG4_SECOND_TAG);
	float scaleValue = pSprite->getScale();
	float widthMultiple = pSprite->getContentSize().width*scaleValue/(float)winSize.width;

	auto moveToActionNext = MoveTo::create(BG4_ROLL_TIME*(widthMultiple+1)/widthMultiple,Vec2(-pSprite->getContentSize().width*scaleValue, 0));
	auto moveToReturnNext = MoveTo::create(0,Vec2(winSize.width-1, 0));
	auto repeapActionNext = RepeatForever::create(
		Sequence::create(
			moveToActionNext,
			moveToReturnNext,
			DelayTime::create(BG4_ROLL_TIME*(widthMultiple-1)/widthMultiple),
			NULL));
	pSprite->runAction(repeapActionNext);
}

void MoutainBackGroundLayer::setBackGroundColor(const Color3B& color3)
{
	auto pSprite1 = (Sprite*)getChildByTag(BG1_FIRST_TAG);
	pSprite1->setColor(color3);
	auto pSprite2 = (Sprite*)getChildByTag(BG1_SECOND_TAG);
	pSprite2->setColor(color3);

	pSprite1 = (Sprite*)getChildByTag(BG2_FIRST_TAG);
	pSprite1->setColor(color3);
	pSprite2 = (Sprite*)getChildByTag(BG2_SECOND_TAG);
	pSprite2->setColor(color3);

	pSprite1 = (Sprite*)getChildByTag(BG3_FIRST_TAG);
	pSprite1->setColor(color3);
	pSprite2 = (Sprite*)getChildByTag(BG3_SECOND_TAG);
	pSprite2->setColor(color3);

	pSprite1 = (Sprite*)getChildByTag(BG4_FIRST_TAG);
	pSprite1->setColor(color3);
	pSprite2 = (Sprite*)getChildByTag(BG4_SECOND_TAG);
	pSprite2->setColor(color3);
}

void MoutainBackGroundLayer::TouchesBegan(__Set *pTouches, Event *pEvent)
{
}

void MoutainBackGroundLayer::TouchesMoved(__Set *pTouches, Event *pEvent)
{
}

void MoutainBackGroundLayer::TouchesEnded(__Set *pTouches, Event *pEvent)
{
}
