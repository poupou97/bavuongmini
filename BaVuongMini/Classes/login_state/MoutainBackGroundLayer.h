#ifndef _GAMESTATE_MOUTAINBACKGROUNDLAYER_STATE_H_
#define _GAMESTATE_MOUTAINBACKGROUNDLAYER_STATE_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;

/**
  * the background layer which scrolls from right to left
 */
class MoutainBackGroundLayer : public __LayerRGBA
{
public:
    MoutainBackGroundLayer();
	virtual ~MoutainBackGroundLayer();

	virtual void TouchesEnded(__Set *pTouches, Event *pEvent);
	virtual void TouchesBegan(__Set *pTouches, Event *pEvent);
	virtual void TouchesMoved(__Set *pTouches, Event *pEvent);

	static MoutainBackGroundLayer* create();
	bool init();

	void setBackGroundColor(const Color3B& color3);

private:
	void addBirds(Node* parent);
	void addWaterfall1(Node* parent, Node* next);
	void addWaterfall2(Node* parent, Node* next);
	void addBuddhaLight(Node* parent, Node* next);
	ParticleSystem* generateSmokeEffect();
	void addSmoke1(Node* parent, Node* next);
	void addSmoke2(Node* parent, Node* next);

	void BG1_scroll();
	void BG1_repeatFunc();

	void BG2_scroll();
	void BG2_repeatFunc();

	void BG3_scroll();
	void BG3_repeatFunc();

	void BG4_scroll();
	void BG4_repeatFunc();
};

#endif
