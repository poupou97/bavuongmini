#ifndef _GAMEUSERDEFAULT_H_
#define _GAMEUSERDEFAULT_H_

#define MUSIC		"music"
#define SOUND		"sound"
#define CURRENT_COUNTRY	"current_country"

#define KEY_OF_RES_VERSION   "current-version-code"

#define GAME_CODE_VERSION "1.0.2"
#define GAME_RES_VERSION 6980

/*
 **********************************************************
 ostype
 1:IOSԽ����2����׿���ڣ�3��IOS�ٷ����壻4��IOS�ٷ�����
 5�������汾��6��IOSԽ��HD��7����׿HD��8��IOS�ٷ�HD
 **********************************************************
 ��������
 91��wl91   PP:25pp    ����IOS:dlyy	  �ϻ�IOS��laoh
 ͬ���ƣ�tngb	ƻ�����ã�kypg  itools:itls
 ��˼����:i4yy �������֣�hmyy   xy���֣�xyyy
 **********************************************************
 */

#if defined(CHANNEL_LAOHU_1)
#define MACHINE_TYPE 1
#define OPERATION_PLATFORM "laoh"
#elif defined(CHANNEL_91_1)
#define MACHINE_TYPE 1
#define OPERATION_PLATFORM "wl91"
#elif defined(CHANNEL_PP_1)
#define MACHINE_TYPE 1
#define OPERATION_PLATFORM "25pp"
#elif defined(CHANNEL_DANGLE_1)
#define MACHINE_TYPE 1
#define OPERATION_PLATFORM "dlyy"
#elif defined(CHANNEL_TONGBUTUI_1)
#define MACHINE_TYPE 1
#define OPERATION_PLATFORM "tngb"
#elif defined(CHANNEL_KYPG_1)
#define MACHINE_TYPE 1
#define OPERATION_PLATFORM "kypg"
#elif defined(CHANNEL_ITLS_1)
#define MACHINE_TYPE 1
#define OPERATION_PLATFORM "itls"
#elif defined(CHANNEL_I4_1)
#define MACHINE_TYPE 1
#define OPERATION_PLATFORM "i4yy"
#elif defined(CHANNEL_HAIMA_1)
#define MACHINE_TYPE 1
#define OPERATION_PLATFORM "hmyy"
#elif defined(CHANNEL_XY_1)
#define MACHINE_TYPE 1
#define OPERATION_PLATFORM "xyyy"
#elif defined(CHANNEL_LAOHU_2)
#define MACHINE_TYPE 2
#define OPERATION_PLATFORM "laoh"
#elif defined(CHANNEL_91_2)
#define MACHINE_TYPE 2
#define OPERATION_PLATFORM "wl91"
#elif defined(CHANNEL_PP_2)
#define MACHINE_TYPE 2
#define OPERATION_PLATFORM "25pp"
#elif defined(CHANNEL_DANGLE_2)
#define MACHINE_TYPE 2
#define OPERATION_PLATFORM "dlyy"
#elif defined(CHANNEL_TONGBUTUI_2)
#define MACHINE_TYPE 2
#define OPERATION_PLATFORM "tngb"
#elif defined(CHANNEL_KYPG_2)
#define MACHINE_TYPE 2
#define OPERATION_PLATFORM "kypg"
#elif defined(CHANNEL_ITLS_2)
#define MACHINE_TYPE 2
#define OPERATION_PLATFORM "itls"
#elif defined(CHANNEL_I4_2)
#define MACHINE_TYPE 2
#define OPERATION_PLATFORM "i4yy"
#elif defined(CHANNEL_HAIMA_2)
#define MACHINE_TYPE 2
#define OPERATION_PLATFORM "hmyy"
#elif defined(CHANNEL_XY_2)
#define MACHINE_TYPE 2
#define OPERATION_PLATFORM "xyyy"
#endif 

#endif