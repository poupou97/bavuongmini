
#include "GameMessageProcessor.h"

#include "ClientNetEngine.h"
#include "../common/CKBaseClass.h"
#include "pushhandler/PushHandlerProtocol.h"
#include "reqsender/ReqSenderProtocol.h"
#include "../utils/GameUtils.h"
#include "GameView.h"

#define COMMAND_ID_MAX_LEN 6   // command id is a number which is less than 10,000

#define PING_TIME_INTERVAL 30000   // half a minute
#define BREAK_NETWORK_TIME_INTERVAL 150000   // 2.5 minutes

static GameMessageProcessor *s_SharedMsgProcessor = NULL;

GameMessageProcessor* GameMessageProcessor::sharedMsgProcessor(void)
{
    if (!s_SharedMsgProcessor)
    {
		s_SharedMsgProcessor = new GameMessageProcessor();
    }

    return s_SharedMsgProcessor;
}

GameMessageProcessor::GameMessageProcessor() 
{
	m_pingTimeCounter = GameUtils::millisecondNow();
}
GameMessageProcessor::~GameMessageProcessor() 
{
    
}

void GameMessageProcessor::checkNetwork()
{
	if(ClientNetEngine::sharedSocketEngine()->getReadyState() != ClientNetEngine::kStateOpen)
		return;

	sendHeartbeat();

	long long lastCommunicationTime = ClientNetEngine::sharedSocketEngine()->getLastRecieveTime();
	// if the client has not recieved any data, using the established time of the socket connection
	if(lastCommunicationTime == -1)
	{
		lastCommunicationTime = ClientNetEngine::sharedSocketEngine()->getEstablishedTime();
	}

	if (GameUtils::millisecondNow() - lastCommunicationTime > BREAK_NETWORK_TIME_INTERVAL)
	{
		// break network by client
		ClientNetEngine::sharedSocketEngine()->close();
	}
}

void GameMessageProcessor::sendHeartbeat()
{
	long long currentTime = GameUtils::millisecondNow();
	if (currentTime - m_pingTimeCounter > PING_TIME_INTERVAL) {
		//CCLOG("heart beat, current time: %lld, last time: %lld", currentTime, m_pingTimeCounter);
		sendReq(1000, this);
		m_pingTimeCounter = GameUtils::millisecondNow();
	}
}

void GameMessageProcessor::socketUpdate()
{
	CommonMessage comMessage = ClientNetEngine::sharedSocketEngine()->getMsg();
	if(!comMessage.has_cmdid())   // no command id, so it's null message
		return;

	//// debug code
// 	if(comMessage.cmdid() == -7003)
// 	{
// 		threekingdoms::protocol::Push1105 bean;
// 		bean.ParseFromString(comMessage.data());
// 		if (bean.has_activerole()) {
// 			int j= 3;
// 		}
// 	}

	char name[COMMAND_ID_MAX_LEN];
	sprintf(name, "%d", comMessage.cmdid());   // message id
	std::string commandIdStr = name;
	std::string commandId = commandIdStr.substr(1);
	//CCLOG("command id: %s", commandId.c_str());
	if (comMessage.cmdid() == 0)
	{
		commandId = "0";
	}
	std::string pushhandlerClassName = "PushHandler";
	auto pVar = (CKBaseClass*)CKClassFactory::sharedClassFactory().getClassByName(pushhandlerClassName.append(commandId));
	auto pPushHandlerProtocol = dynamic_cast<PushHandlerProtocol*>(pVar);
	if(pPushHandlerProtocol == NULL)
		//CCAssert(false, "PushHandlerxxxx has not been defined.");
		CCLOG("PushHandler%d has not been defined.", comMessage.cmdid());
	else
		pPushHandlerProtocol->handle(&comMessage);

	delete pVar;

}

void GameMessageProcessor::sendReq(int commandId, void* source, void* source1)
{
	char name[COMMAND_ID_MAX_LEN];
	sprintf(name, "%d", commandId);   // message id
	std::string className = "ReqSender";
	auto pVar = (CKBaseClass*)CKClassFactory::sharedClassFactory().getClassByName(className.append(name));
	auto pReqSenderProtocol = dynamic_cast<ReqSenderProtocol*>(pVar);
	if(pReqSenderProtocol == NULL)
		CCAssert(false, "ReqSenderxxxx has not been defined.");
	pReqSenderProtocol->send(source, source1);
		
	delete pVar;
}