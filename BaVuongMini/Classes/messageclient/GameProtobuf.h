#ifndef __GAMEMESSAGEMACROS_H__
#define __GAMEMESSAGEMACROS_H__

#include "protobuf/MainMessage.pb.h"

#define USING_NS_THREEKINGDOMS_PROTOCOL using namespace com::future::threekingdoms::server::transport::protocol;

#endif /* __GAMEMESSAGEMACROS_H__ */
