#include "ReqSender2803.h"
#include "../ClientNetEngine.h"
#include "../protobuf/QuestionMessage.pb.h"
#include "../../ui/Question_ui/QuestionData.h"

IMPLEMENT_CLASS(ReqSender2803)
	ReqSender2803::ReqSender2803() 
{

}
ReqSender2803::~ReqSender2803() 
{
}
void* ReqSender2803::createInstance()
{
	return new ReqSender2803() ;
}
void ReqSender2803::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2803::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender2803::send(void* source, void* source1)
{
	QuestionData::instance()->set_isGetedResult(false);

	CommonMessage comMessage;
	comMessage.set_cmdid(2803);

	Req2803 message1;
	message1.set_optionidx((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

	
}