
#ifndef Blog_C___Reflection_ReqSender7016_h
#define Blog_C___Reflection_ReqSender7016_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender7016: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender7016)

public:
	SYNTHESIZE(ReqSender7016, int*, m_pValue)

	ReqSender7016() ;
	virtual ~ReqSender7016() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
