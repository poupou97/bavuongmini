
#include "ReqSender1414.h"

#include "../ClientNetEngine.h"

#include "../protobuf/RelationMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "GameView.h"


IMPLEMENT_CLASS(ReqSender1414)
ReqSender1414::ReqSender1414() 
{
    
}
ReqSender1414::~ReqSender1414() 
{
}
void* ReqSender1414::createInstance()
{
    return new ReqSender1414() ;
}
void ReqSender1414::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1414::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1414::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1414);
	

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}