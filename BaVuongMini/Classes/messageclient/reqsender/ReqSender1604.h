

#ifndef Blog_C___Reflection_ReqSender1604_h
#define Blog_C___Reflection_ReqSender1604_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1604 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1604)
    
public:
    SYNTHESIZE(ReqSender1604, int*, m_pValue)
    
    ReqSender1604() ;
    virtual ~ReqSender1604() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
