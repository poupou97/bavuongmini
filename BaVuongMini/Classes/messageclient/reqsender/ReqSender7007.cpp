
#include "ReqSender7007.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../gamescene_state/sceneelement/MissionCell.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"

IMPLEMENT_CLASS(ReqSender7007)

	ReqSender7007::ReqSender7007() 
{

}
ReqSender7007::~ReqSender7007() 
{

}
void* ReqSender7007::createInstance()
{
	return new ReqSender7007() ;
}
void ReqSender7007::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender7007::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender7007::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(7007);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req7007 message1;

	char * pid = (char *)source;
	message1.set_missionpackageid(pid);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}