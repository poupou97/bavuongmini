
#include "ReqSender1104.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(ReqSender1104)

ReqSender1104::ReqSender1104() 
{
    
}
ReqSender1104::~ReqSender1104() 
{
    
}
void* ReqSender1104::createInstance()
{
    return new ReqSender1104() ;
}
void ReqSender1104::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1104::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1104::send(void* source, void* source1)
{
	MyPlayer* player = (MyPlayer*)source;

	CommonMessage comMessage;
	comMessage.set_cmdid(1104);

	Req1104 message2;
	message2.set_x(player->getWorldPosition().x);
	message2.set_y(player->getWorldPosition().y);

	//CCLOG("send msg: %d, MyPlayer Position: ( %f, %f ) ", comMessage.cmdid(), player->getWorldPosition().x, player->getWorldPosition().y);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}