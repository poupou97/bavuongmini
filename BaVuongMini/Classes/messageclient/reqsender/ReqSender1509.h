

#ifndef Blog_C___Family_ReqSender1509_h
#define Blog_C___Family_ReqSender1509_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1509 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1509)
    
public:
	SYNTHESIZE(ReqSender1509, int*, m_pValue)

	ReqSender1509() ;
    virtual ~ReqSender1509() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
