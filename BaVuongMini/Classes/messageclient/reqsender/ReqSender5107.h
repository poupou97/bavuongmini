
#ifndef Blog_C___Reflection_ReqSender5107_h
#define Blog_C___Reflection_ReqSender5107_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5107: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5107)

public:
	SYNTHESIZE(ReqSender5107, int*, m_pValue)

	ReqSender5107() ;
	virtual ~ReqSender5107() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
