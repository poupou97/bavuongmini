
#include "ReqSender5105.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5105)

ReqSender5105::ReqSender5105() 
{

}
ReqSender5105::~ReqSender5105() 
{

}
void* ReqSender5105::createInstance()
{
	return new ReqSender5105() ;
}
void ReqSender5105::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5105::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5105::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5105);
	CCLOG("send msg: %d", comMessage.cmdid());

	ReqAddFightGeneral5105 message1;
	message1.set_group((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}