
#include "ReqSender1002.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../GameView.h"

IMPLEMENT_CLASS(ReqSender1002)
ReqSender1002::ReqSender1002() 
{
    
}
ReqSender1002::~ReqSender1002() 
{
    
}
void* ReqSender1002::createInstance()
{
    return new ReqSender1002() ;
}
void ReqSender1002::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1002::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1002::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1002);

	CCLOG("send msg: %d, logout", comMessage.cmdid());

	Req1002 message2;
	message2.set_offlinepractice(false);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
    
    // when the player want to re-login, the disconnection tip will not be shown
	GameView::getInstance()->showSocketCloseInfo = false;

}