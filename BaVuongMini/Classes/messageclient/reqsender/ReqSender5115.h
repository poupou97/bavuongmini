#ifndef Blog_C___Reflection_ReqSender5115_h
#define Blog_C___Reflection_ReqSender5115_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5115: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5115)

public:
	SYNTHESIZE(ReqSender5115, int*, m_pValue)

	ReqSender5115() ;
	virtual ~ReqSender5115() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
