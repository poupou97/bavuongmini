#include "ReqSender1320.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender1320)

	ReqSender1320::ReqSender1320() 
{

}
ReqSender1320::~ReqSender1320() 
{

}
void* ReqSender1320::createInstance()
{
	return new ReqSender1320() ;
}
void ReqSender1320::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1320::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1320::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1320);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req1320 message1;

	message1.set_type((int )source);
	message1.set_num((int) source1);

	string msgData; 
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}