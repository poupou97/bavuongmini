
#include "ReqSender1530.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1530)
ReqSender1530::ReqSender1530() 
{
    
}
ReqSender1530::~ReqSender1530() 
{
}
void* ReqSender1530::createInstance()
{
    return new ReqSender1530() ;
}
void ReqSender1530::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1530::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1530::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1530);
	
	
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}