

#ifndef Blog_C___Reflection_ReqSender1126_h
#define Blog_C___Reflection_ReqSender1126_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

/**
 * 玩家在移动过程中，客户端定时发送玩家位置给服务器， 服务器同步给其他同屏玩家。
 */
class ReqSender1126 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1126)
    
public:
    SYNTHESIZE(ReqSender1126, int*, m_pValue)
    
    ReqSender1126() ;
    virtual ~ReqSender1126() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
