
#include "ReqSender1408.h"

#include "../ClientNetEngine.h"
#include "../protobuf/RelationMessage.pb.h"



IMPLEMENT_CLASS(ReqSender1408)
ReqSender1408::ReqSender1408() 
{
    
}
ReqSender1408::~ReqSender1408() 
{
}
void* ReqSender1408::createInstance()
{
    return new ReqSender1408() ;
}
void ReqSender1408::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1408::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1408::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1408);
	
	Req1408 message1;

	message1.set_type((int)source);//0=查看队伍  1=查看玩家
	

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}