#ifndef Blog_C___Reflection_ReqSender3000_h
#define Blog_C___Reflection_ReqSender3000_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender3000 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender3000)

public:
	SYNTHESIZE(ReqSender3000, int*, m_pValue)

		ReqSender3000() ;
	virtual ~ReqSender3000() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
