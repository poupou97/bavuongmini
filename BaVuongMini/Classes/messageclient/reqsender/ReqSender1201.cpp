
#include "ReqSender1201.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/skill/SkillSeed.h"

IMPLEMENT_CLASS(ReqSender1201)

ReqSender1201::ReqSender1201() 
{
    
}
ReqSender1201::~ReqSender1201() 
{
    
}
void* ReqSender1201::createInstance()
{
    return new ReqSender1201() ;
}
void ReqSender1201::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1201::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1201::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1201);

	Req1201 message2;
	SkillSeed* ss = (SkillSeed*)source;
	message2.set_targetid(ss->defender);
	message2.set_x(ss->x);
	message2.set_y(ss->y);
	message2.set_skillid(ss->skillId);

	//CCLOG("send msg: %d, skill id: %s", comMessage.cmdid(), ss->skillId.c_str());

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}