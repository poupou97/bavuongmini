
#ifndef Blog_C___Reflection_ReqSender7023_h
#define Blog_C___Reflection_ReqSender7023_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender7023: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender7023)

public:
	SYNTHESIZE(ReqSender7023, int*, m_pValue)

	ReqSender7023() ;
	virtual ~ReqSender7023() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
