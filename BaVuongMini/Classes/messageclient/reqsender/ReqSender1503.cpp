
#include "ReqSender1503.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1503)
ReqSender1503::ReqSender1503() 
{
    
}
ReqSender1503::~ReqSender1503() 
{
}
void* ReqSender1503::createInstance()
{
    return new ReqSender1503() ;
}
void ReqSender1503::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1503::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1503::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1503);
	
	Req1503 message1;

	message1.set_type((int)source);
	message1.set_roleid((long long)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}