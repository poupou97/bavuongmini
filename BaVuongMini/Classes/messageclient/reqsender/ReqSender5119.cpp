#include "ReqSender5119.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../../ui/Active_ui/ActiveShopItemBuyInfo.h"
#include "../protobuf/AdditionStore.pb.h"

IMPLEMENT_CLASS(ReqSender5119)

	ReqSender5119::ReqSender5119() 
{

}
ReqSender5119::~ReqSender5119() 
{

}
void* ReqSender5119::createInstance()
{
	return new ReqSender5119() ;
}
void ReqSender5119::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5119::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5119::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5119);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}