
#include "ReqSender1533.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1533)
	ReqSender1533::ReqSender1533() 
{

}
ReqSender1533::~ReqSender1533() 
{
}
void* ReqSender1533::createInstance()
{
	return new ReqSender1533() ;
}
void ReqSender1533::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1533::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1533::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1533);

	Req1533 message1;
	message1.set_pageno((int)source);
	message1.set_pagesize((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}