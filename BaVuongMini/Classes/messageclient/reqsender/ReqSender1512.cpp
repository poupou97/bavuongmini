
#include "ReqSender1512.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1512)
ReqSender1512::ReqSender1512() 
{
    
}
ReqSender1512::~ReqSender1512() 
{
}
void* ReqSender1512::createInstance()
{
    return new ReqSender1512() ;
}
void ReqSender1512::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1512::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1512::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1512);
	
	Req1512 message1;
	
	message1.set_page((int)source);
	message1.set_online((int)source1);
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}