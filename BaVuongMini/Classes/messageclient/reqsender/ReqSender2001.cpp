
#include "ReqSender2001.h"

#include "../ClientNetEngine.h"

#include "../protobuf/MailMessage.pb.h"
#include "../../ui/Mail_ui/MailUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"



IMPLEMENT_CLASS(ReqSender2001)
ReqSender2001::ReqSender2001() 
{
    
}
ReqSender2001::~ReqSender2001() 
{
}
void* ReqSender2001::createInstance()
{
    return new ReqSender2001() ;
}
void ReqSender2001::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2001::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender2001::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2001);

	Req2001 message1;	
	message1.set_page((int)source);


	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}