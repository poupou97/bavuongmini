
#include "ReqSender5093.h"

#include "../ClientNetEngine.h"


IMPLEMENT_CLASS(ReqSender5093)

ReqSender5093::ReqSender5093() 
{

}
ReqSender5093::~ReqSender5093() 
{

}
void* ReqSender5093::createInstance()
{
	return new ReqSender5093() ;
}
void ReqSender5093::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5093::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5093::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5093);
	
	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}