
#include "ReqSender1635.h"

#include "../ClientNetEngine.h"
#include "../protobuf/ItemMessage.pb.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/FolderInfo.h"
#include "../protobuf/EquipmentMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1635)

ReqSender1635::ReqSender1635() 
{

}
ReqSender1635::~ReqSender1635() 
{

}
void* ReqSender1635::createInstance()
{
	return new ReqSender1635() ;
}
void ReqSender1635::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1635::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1635::send(void* source, void* source1)
{
	// set message id
	CommonMessage comMessage;
	comMessage.set_cmdid(1635);
	Req1635 message1;

	message1.set_repairtype((int)source);
	message1.set_index((int)source1);
	message1.set_autoflag(1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}