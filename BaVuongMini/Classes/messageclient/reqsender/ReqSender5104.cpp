
#include "ReqSender5104.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(ReqSender5104)

ReqSender5104::ReqSender5104() 
{

}
ReqSender5104::~ReqSender5104() 
{

}
void* ReqSender5104::createInstance()
{
	return new ReqSender5104() ;
}
void ReqSender5104::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5104::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5104::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5104);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}