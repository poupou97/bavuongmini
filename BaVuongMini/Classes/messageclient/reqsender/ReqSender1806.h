

#ifndef Blog_C___Reflection_ReqSender1806_h
#define Blog_C___Reflection_ReqSender1806_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1806 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1806)
    
public:
    SYNTHESIZE(ReqSender1806, int*, m_pValue)
    
    ReqSender1806() ;
    virtual ~ReqSender1806() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
