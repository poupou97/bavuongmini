
#include "ReqSender2000.h"

#include "../ClientNetEngine.h"

#include "../protobuf/MailMessage.pb.h"
#include "../../ui/Mail_ui/MailUI.h"

#include "../../GameView.h"
#include "../../messageclient/element/CAttachedProps.h"

IMPLEMENT_CLASS(ReqSender2000)
ReqSender2000::ReqSender2000() 
{
    
}
ReqSender2000::~ReqSender2000() 
{
}
void* ReqSender2000::createInstance()
{
    return new ReqSender2000() ;
}
void ReqSender2000::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2000::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender2000::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2000);

	Req2000 message1;

	MailUI * m_mailUi=(MailUI *)source; 
	message1.set_subject(m_mailUi->mail_subject);
	message1.set_text( m_mailUi->mail_text);
	message1.set_receiver(m_mailUi->receiverId);
	message1.set_receivername(m_mailUi->receiverName);
	message1.set_hasattachment(m_mailUi->mail_attactMent);
	message1.set_gold(m_mailUi->sendMailCollectcoins);
	message1.set_goldingot(m_mailUi->sendMailCollectinIngots);

	if (m_mailUi->mail_attactMent==true)
	{
		for (int i=0;i<m_mailUi->attachePropVector.size();i++)
		{
			AttachedProps * temp =message1.add_attachedprops();
			
			temp->set_amount(m_mailUi->attachePropVector.at(i)->amount());
			temp->set_folderidx(m_mailUi->attachePropVector.at(i)->folderidx());
		}
	}
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}