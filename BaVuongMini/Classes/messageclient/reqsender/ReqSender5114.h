
#ifndef Blog_C___Reflection_ReqSender5114_h
#define Blog_C___Reflection_ReqSender5114_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5114: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5114)

public:
	SYNTHESIZE(ReqSender5114, int*, m_pValue)

	ReqSender5114() ;
	virtual ~ReqSender5114() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
