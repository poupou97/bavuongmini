#include "ReqSender1322.h"
#include "../ClientNetEngine.h"
#include "../protobuf/ItemMessage.pb.h"



IMPLEMENT_CLASS(ReqSender1322)

ReqSender1322::ReqSender1322() 
{

}
ReqSender1322::~ReqSender1322() 
{

}
void* ReqSender1322::createInstance()
{
	return new ReqSender1322() ;
}
void ReqSender1322::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1322::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1322::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1322);
	CCLOG("send msg: %d", comMessage.cmdid());

	Req1322 message1;

	message1.set_instatnceid((long long)source);
	message1.set_propid((const char *)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}
