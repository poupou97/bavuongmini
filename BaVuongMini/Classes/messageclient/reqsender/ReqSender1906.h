
#ifndef Blog_C___Reflection_ReqSender1906_h
#define Blog_C___Reflection_ReqSender1906_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1906 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1906)
    
public:
    SYNTHESIZE(ReqSender1906, int*, m_pValue)
    
    ReqSender1906() ;
    virtual ~ReqSender1906() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
