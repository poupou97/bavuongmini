
#ifndef Blog_C___Reflection_ReqSender1322_h
#define Blog_C___Reflection_ReqSender1322_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1322 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1322)

public:
	SYNTHESIZE(ReqSender1322, int*, m_pValue)

	ReqSender1322() ;
	virtual ~ReqSender1322() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
