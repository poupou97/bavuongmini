
#include "ReqSender7014.h"

#include "../ClientNetEngine.h"
#include "../protobuf/MissionMessage.pb.h"

IMPLEMENT_CLASS(ReqSender7014)

ReqSender7014::ReqSender7014() 
{

}
ReqSender7014::~ReqSender7014() 
{

}
void* ReqSender7014::createInstance()
{
	return new ReqSender7014() ;
}
void ReqSender7014::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender7014::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender7014::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(7014);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	ReqBoardInfo7014 message1;

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}