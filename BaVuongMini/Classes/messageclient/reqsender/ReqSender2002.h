

#ifndef Blog_C___Reflection_ReqSender2002_h
#define Blog_C___Reflection_ReqSender2002_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender2002 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender2002)
    
public:
    SYNTHESIZE(ReqSender2002, int*, m_pValue)
    
    ReqSender2002() ;
    virtual ~ReqSender2002() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
