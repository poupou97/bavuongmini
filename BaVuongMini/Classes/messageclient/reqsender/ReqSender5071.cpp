
#include "ReqSender5071.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5071)

	ReqSender5071::ReqSender5071() 
{

}
ReqSender5071::~ReqSender5071() 
{

}
void* ReqSender5071::createInstance()
{
	return new ReqSender5071() ;
}
void ReqSender5071::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5071::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5071::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5071);
	//CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}