
#include "ReqSender1929.h"

#include "../ClientNetEngine.h"
#include "../protobuf/CopyMessage.pb.h" 
#include "../../ui/FivePersonInstance/FivePersonInstance.h"

IMPLEMENT_CLASS(ReqSender1929)
	ReqSender1929::ReqSender1929() 
{

}
ReqSender1929::~ReqSender1929() 
{

}
void* ReqSender1929::createInstance()
{
	return new ReqSender1929() ;
}
void ReqSender1929::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1929::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1929::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1929);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}