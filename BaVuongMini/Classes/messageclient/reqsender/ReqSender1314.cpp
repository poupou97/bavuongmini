#include "ReqSender1314.h"
#include "../protobuf/ModelMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../protobuf/ItemMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1314)

	ReqSender1314::ReqSender1314() 
{

}
ReqSender1314::~ReqSender1314() 
{

}
void* ReqSender1314::createInstance()
{
	return new ReqSender1314() ;
}
void ReqSender1314::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1314::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1314::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1314);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req1314 message1;

// 	string msgData;
// 	message1.SerializeToString(&msgData);
// 	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}
