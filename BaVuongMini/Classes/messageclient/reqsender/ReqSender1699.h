

#ifndef Blog_C___EquipMent_ReqSender1699_h
#define Blog_C___EquipMent_ReqSender1699_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1699 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1699)
    
public:
    SYNTHESIZE(ReqSender1699, int*, m_pValue)
    
    ReqSender1699() ;
    virtual ~ReqSender1699() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
