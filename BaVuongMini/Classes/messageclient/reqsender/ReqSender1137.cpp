
#include "ReqSender1137.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/NPCMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1137)
ReqSender1137::ReqSender1137() 
{

}
ReqSender1137::~ReqSender1137() 
{

}
void* ReqSender1137::createInstance()
{
	return new ReqSender1137() ;
}
void ReqSender1137::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1137::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1137::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1137);
	ReqBuyDrug1137 message1;

	char *goodsId =(char *)source;
	message1.set_propid(goodsId);
	message1.set_amount((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);


	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}