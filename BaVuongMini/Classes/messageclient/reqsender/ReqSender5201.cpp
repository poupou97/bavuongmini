
#include "ReqSender5201.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../protobuf/DailyGiftSign.pb.h"
#include "../protobuf/VipMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5201)

	ReqSender5201::ReqSender5201() 
{

}
ReqSender5201::~ReqSender5201() 
{

}
void* ReqSender5201::createInstance()
{
	return new ReqSender5201() ;
}
void ReqSender5201::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5201::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5201::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5201);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5201 message1;
	message1.set_viplevel((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}