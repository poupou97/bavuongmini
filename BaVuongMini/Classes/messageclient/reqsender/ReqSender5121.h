
#ifndef Blog_C___Reflection_ReqSender5121_h
#define Blog_C___Reflection_ReqSender5121_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5121: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5121)

public:
	SYNTHESIZE(ReqSender5121, int*, m_pValue)

		ReqSender5121() ;
	virtual ~ReqSender5121() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
