
#ifndef Blog_C___Reflection_ReqSender1313_h
#define Blog_C___Reflection_ReqSender1313_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1313 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1313)

public:
	SYNTHESIZE(ReqSender1313, int*, m_pValue)

	ReqSender1313() ;
	virtual ~ReqSender1313() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
