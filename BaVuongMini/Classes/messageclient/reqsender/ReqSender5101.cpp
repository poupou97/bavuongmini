
#include "ReqSender5101.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender5101)

ReqSender5101::ReqSender5101() 
{

}
ReqSender5101::~ReqSender5101() 
{

}
void* ReqSender5101::createInstance()
{
	return new ReqSender5101() ;
}
void ReqSender5101::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5101::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5101::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5101);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}