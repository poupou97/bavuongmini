
#ifndef Blog_C___Reflection_ReqSender7021_h
#define Blog_C___Reflection_ReqSender7021_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender7021: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender7021)

public:
	SYNTHESIZE(ReqSender7021, int*, m_pValue)

		ReqSender7021() ;
	virtual ~ReqSender7021() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
