

#ifndef Blog_C___Reflection_ReqSender1201_h
#define Blog_C___Reflection_ReqSender1201_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1201 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1201)
    
public:
    SYNTHESIZE(ReqSender1201, int*, m_pValue)
    
    ReqSender1201() ;
    virtual ~ReqSender1201() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
