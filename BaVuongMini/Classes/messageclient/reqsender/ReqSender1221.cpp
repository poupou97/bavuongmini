
#include "ReqSender1221.h"

#include "../ClientNetEngine.h"
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/skill/SkillSeed.h"

IMPLEMENT_CLASS(ReqSender1221)

ReqSender1221::ReqSender1221() 
{
    
}
ReqSender1221::~ReqSender1221() 
{
    
}
void* ReqSender1221::createInstance()
{
    return new ReqSender1221() ;
}
void ReqSender1221::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1221::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1221::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1221);

	Req1221 message2;
	SkillSeed* ss = (SkillSeed*)source;
	//CCAssert(ss != NULL, "should not be nil");
	if(ss != NULL)
	{
		message2.set_targetid(ss->defender);
		message2.set_x(ss->x);
		message2.set_y(ss->y);
	}
	CCLOG("send msg: %d, launch musou skill");

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}