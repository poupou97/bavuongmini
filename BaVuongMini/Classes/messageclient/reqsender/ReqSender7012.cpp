
#include "ReqSender7012.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../gamescene_state/sceneelement/MissionCell.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"

IMPLEMENT_CLASS(ReqSender7012)

	ReqSender7012::ReqSender7012() 
{

}
ReqSender7012::~ReqSender7012() 
{

}
void* ReqSender7012::createInstance()
{
	return new ReqSender7012() ;
}
void ReqSender7012::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender7012::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender7012::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(7012);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	ReqOverTalk7012 message1;

	message1.set_npcid((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}