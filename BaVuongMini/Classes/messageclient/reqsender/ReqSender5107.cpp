
#include "ReqSender5107.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender5107)

ReqSender5107::ReqSender5107() 
{

}
ReqSender5107::~ReqSender5107() 
{

}
void* ReqSender5107::createInstance()
{
	return new ReqSender5107() ;
}
void ReqSender5107::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5107::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5107::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5107);
	CCLOG("send msg: %d", comMessage.cmdid());

	Req5107 message1;
	message1.set_time((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}