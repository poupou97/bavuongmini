#ifndef Blog_C___Reflection_ReqSender1926_h
#define Blog_C___Reflection_ReqSender1926_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1926 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1926)

public:
	SYNTHESIZE(ReqSender1926, int*, m_pValue)

	ReqSender1926() ;
	virtual ~ReqSender1926() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
