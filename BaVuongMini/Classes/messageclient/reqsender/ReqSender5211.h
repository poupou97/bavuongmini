#ifndef Blog_C___Reflection_ReqSender5211_h
#define Blog_C___Reflection_ReqSender5211_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5211: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5211)

public:
	SYNTHESIZE(ReqSender5211, int*, m_pValue)

	ReqSender5211() ;
	virtual ~ReqSender5211() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
