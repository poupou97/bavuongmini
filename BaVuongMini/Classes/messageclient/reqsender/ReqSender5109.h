

#ifndef Blog_C___Reflection_ReqSender5109_h
#define Blog_C___Reflection_ReqSender5109_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5109 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5109)

public:
	SYNTHESIZE(ReqSender5109, int*, m_pValue)

		ReqSender5109() ;
	virtual ~ReqSender5109() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
