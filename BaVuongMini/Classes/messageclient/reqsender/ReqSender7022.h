
#ifndef Blog_C___Reflection_ReqSender7022_h
#define Blog_C___Reflection_ReqSender7022_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender7022: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender7022)

public:
	SYNTHESIZE(ReqSender7022, int*, m_pValue)

		ReqSender7022() ;
	virtual ~ReqSender7022() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
