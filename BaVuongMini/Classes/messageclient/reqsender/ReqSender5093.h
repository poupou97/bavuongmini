#ifndef Blog_C_prizePhysical_ReqSender5093_h
#define Blog_C_prizePhysical_ReqSender5093_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5093 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5093)

public:
	SYNTHESIZE(ReqSender5093, int*, m_pValue)

	ReqSender5093() ;
	virtual ~ReqSender5093() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
