

#ifndef Blog_C___Reflection_ReqSender1110_h
#define Blog_C___Reflection_ReqSender1110_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

/*
* ����
*/
class ReqSender1110 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1110)

public:
	SYNTHESIZE(ReqSender1110, int*, m_pValue)

		ReqSender1110() ;
	virtual ~ReqSender1110() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
