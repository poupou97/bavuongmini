
#include "ReqSender5051.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../ui/generals_ui/GeneralsUI.h"

IMPLEMENT_CLASS(ReqSender5051)

	ReqSender5051::ReqSender5051() 
{

}
ReqSender5051::~ReqSender5051() 
{

}
void* ReqSender5051::createInstance()
{
	return new ReqSender5051() ;
}
void ReqSender5051::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5051::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5051::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5051);
	//CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5051 message1;

	GeneralsUI::ReqListParameter * temp = (GeneralsUI::ReqListParameter*)source;
	message1.set_page(temp->page);
	message1.set_pagesize(temp->pageSize);
	message1.set_type(temp->type);
	message1.set_generalid(temp->generalId);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}