
#include "ReqSender1548.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1548)
	ReqSender1548::ReqSender1548() 
{

}
ReqSender1548::~ReqSender1548() 
{
}
void* ReqSender1548::createInstance()
{
	return new ReqSender1548() ;
}
void ReqSender1548::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1548::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1548::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1548);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}