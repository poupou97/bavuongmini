
#ifndef Blog_C___Reflection_ReqSender1302_h
#define Blog_C___Reflection_ReqSender1302_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1302 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1302)

public:
	SYNTHESIZE(ReqSender1302, int*, m_pValue)

		ReqSender1302() ;
	virtual ~ReqSender1302() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
