
#ifndef Blog_C___Reflection_ReqSender5201_h
#define Blog_C___Reflection_ReqSender5201_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5201: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5201)

public:
	SYNTHESIZE(ReqSender5201, int*, m_pValue)

		ReqSender5201() ;
	virtual ~ReqSender5201() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
