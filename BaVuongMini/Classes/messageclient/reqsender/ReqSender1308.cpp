#include "ReqSender1308.h"
#include "../protobuf/MainMessage.pb.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender1308)

ReqSender1308::ReqSender1308() 
{

}
ReqSender1308::~ReqSender1308() 
{

}
void* ReqSender1308::createInstance()
{
	return new ReqSender1308() ;
}
void ReqSender1308::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1308::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1308::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1308);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req1308 message1;
	message1.set_type((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}