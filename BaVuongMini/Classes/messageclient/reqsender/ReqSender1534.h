

#ifndef Blog_C___Family_ReqSender1534_h
#define Blog_C___Family_ReqSender1534_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1534 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1534)

public:
	SYNTHESIZE(ReqSender1534, int*, m_pValue)

		ReqSender1534() ;
	virtual ~ReqSender1534() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
