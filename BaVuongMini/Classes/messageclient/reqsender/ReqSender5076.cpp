
#include "ReqSender5076.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../ui/skillscene/SkillScene.h"

IMPLEMENT_CLASS(ReqSender5076)

	ReqSender5076::ReqSender5076() 
{

}
ReqSender5076::~ReqSender5076() 
{

}
void* ReqSender5076::createInstance()
{
	return new ReqSender5076() ;
}
void ReqSender5076::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5076::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5076::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5076);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}