

#ifndef Blog_C___Reflection_ReqSender1200_h
#define Blog_C___Reflection_ReqSender1200_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1200 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1200)
    
public:
    SYNTHESIZE(ReqSender1200, int*, m_pValue)
    
    ReqSender1200() ;
    virtual ~ReqSender1200() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
