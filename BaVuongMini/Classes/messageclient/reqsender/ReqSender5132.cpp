
#include "ReqSender5132.h"

#include "../ClientNetEngine.h"
#include "../protobuf/DailyGiftSign.pb.h"

IMPLEMENT_CLASS(ReqSender5132)

ReqSender5132::ReqSender5132() 
{

}
ReqSender5132::~ReqSender5132() 
{

}
void* ReqSender5132::createInstance()
{
	return new ReqSender5132() ;
}
void ReqSender5132::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5132::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5132::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5132);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}