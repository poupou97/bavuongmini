

#ifndef Blog_C___Reflection_ReqSender2203_h
#define Blog_C___Reflection_ReqSender2203_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender2203 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender2203)
    
public:
    SYNTHESIZE(ReqSender2203, int*, m_pValue)
    
    ReqSender2203() ;
    virtual ~ReqSender2203() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

	struct ChatInfoStruct
	{
		int channelId;
		std::string chatContent;
		long long listenerId;
		std::string listenerName;
	};
} ;

#endif
