
#include "ReqSender1806.h"

#include "../ClientNetEngine.h"
#include "../protobuf/AuctionMessage.pb.h"
#include "../../ui/Auction_ui/AuctionUi.h"


IMPLEMENT_CLASS(ReqSender1806)
ReqSender1806::ReqSender1806() 
{
    
}
ReqSender1806::~ReqSender1806() 
{
}
void* ReqSender1806::createInstance()
{
    return new ReqSender1806() ;
}
void ReqSender1806::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1806::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1806::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1806);
	
	Req1806 message1;


	//AuctionUi* auctionui = (AuctionUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);

	/*
	message1.set_amount();//元宝数量
	message1.set_period();//有效时间 秒
	message1.set_price();//售价
	message1.set_currency();//价格类型 1:金币
	*/

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}