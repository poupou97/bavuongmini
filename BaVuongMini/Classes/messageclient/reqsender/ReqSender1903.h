

#ifndef Blog_C___Reflection_ReqSender1903_h
#define Blog_C___Reflection_ReqSender1903_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1903 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1903)
    
public:
    SYNTHESIZE(ReqSender1903, int*, m_pValue)
    
    ReqSender1903() ;
    virtual ~ReqSender1903() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
