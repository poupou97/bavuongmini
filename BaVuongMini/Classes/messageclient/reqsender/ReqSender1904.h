
#ifndef Blog_C___Reflection_ReqSender1904_h
#define Blog_C___Reflection_ReqSender1904_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1904 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1904)
    
public:
    SYNTHESIZE(ReqSender1904, int*, m_pValue)
    
    ReqSender1904() ;
    virtual ~ReqSender1904() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
