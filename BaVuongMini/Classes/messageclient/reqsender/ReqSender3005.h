#ifndef Blog_C___Reflection_ReqSender3005_h
#define Blog_C___Reflection_ReqSender3005_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender3005 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender3005)

public:
	SYNTHESIZE(ReqSender3005, int*, m_pValue)

		ReqSender3005() ;
	virtual ~ReqSender3005() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
