#ifndef Blog_C___Reflection_ReqSender5106_h
#define Blog_C___Reflection_ReqSender5106_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5106: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5106)

public:
	SYNTHESIZE(ReqSender5106, int*, m_pValue)

		ReqSender5106() ;
	virtual ~ReqSender5106() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
