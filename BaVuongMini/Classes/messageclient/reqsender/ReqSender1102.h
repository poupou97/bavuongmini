

#ifndef Blog_C___Reflection_ReqSender1102_h
#define Blog_C___Reflection_ReqSender1102_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

/**
 * 客户端通知服务器玩家开始向某个方向移动。 无回复。触发服务器计算同屏
 */
class ReqSender1102 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1102)
    
public:
    SYNTHESIZE(ReqSender1102, int*, m_pValue)
    
    ReqSender1102() ;
    virtual ~ReqSender1102() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
