

#ifndef Blog_C___Reflection_ReqSender1114_h
#define Blog_C___Reflection_ReqSender1114_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

/*
* 传送到指定地图的指定位置
*/
class ReqSender1114 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1114)

public:
	SYNTHESIZE(ReqSender1114, int*, m_pValue)

		ReqSender1114() ;
	virtual ~ReqSender1114() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
