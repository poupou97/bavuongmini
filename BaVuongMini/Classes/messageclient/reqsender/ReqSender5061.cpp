
#include "ReqSender5061.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/CollectMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../gamescene_state/sceneelement/MissionCell.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../element/CPushCollectInfo.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../protobuf/RecruitMessage.pb.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsRecuriteUI.h"

IMPLEMENT_CLASS(ReqSender5061)

	ReqSender5061::ReqSender5061() 
{

}
ReqSender5061::~ReqSender5061() 
{

}
void* ReqSender5061::createInstance()
{
	return new ReqSender5061() ;
}
void ReqSender5061::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5061::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5061::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5061);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	ReqRecruit5061 message1;
	GeneralsRecuriteUI::ReqData * temp = (GeneralsRecuriteUI::ReqData*)(source);
	ActionType  actionType ;
	switch(temp->actionType)
	{
	case BASE:
		actionType = BASE;
		break;
	case BETTER:
		actionType = BETTER;
		break;
	case BEST:
		actionType = BEST;
		break;
	case TEN_TIMES:
		actionType = TEN_TIMES;
		break;
	}

	RecruitType recuritType;
	switch(temp->recruitType)
	{
	case FREE:
		recuritType = FREE;
		break;
	case GOLD:
		recuritType = GOLD;
		break;
	}
	message1.set_actiontype(actionType);
	message1.set_recruittype(recuritType);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}