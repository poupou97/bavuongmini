
#include "ReqSender7009.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../gamescene_state/sceneelement/MissionCell.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"

IMPLEMENT_CLASS(ReqSender7009)

	ReqSender7009::ReqSender7009() 
{

}
ReqSender7009::~ReqSender7009() 
{

}
void* ReqSender7009::createInstance()
{
	return new ReqSender7009() ;
}
void ReqSender7009::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender7009::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender7009::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(7009);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req7009 message1;

	std::string pid = GameView::getInstance()->missionManager->curMissionInfo->missionpackageid();
	std::string mid = GameView::getInstance()->missionManager->curMissionInfo->missionid();
	int mseq = GameView::getInstance()->missionManager->curMissionInfo->missionsequnce();
	message1.set_missionpackageid(pid);
// 	message1.set_missionid(mid);
// 	message1.set_missionsequnce(mseq);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}