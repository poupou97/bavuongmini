#ifndef Blog_C___Reflection_ReqSender1922_h
#define Blog_C___Reflection_ReqSender1922_h

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1922 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1922)
    
public:
    SYNTHESIZE(ReqSender1922, int*, m_pValue)
    
    ReqSender1922() ;
    virtual ~ReqSender1922() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
