
#include "ReqSender1307.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CEquipment.h"


IMPLEMENT_CLASS(ReqSender1307)

ReqSender1307::ReqSender1307() 
{
	
}
ReqSender1307::~ReqSender1307() 
{

}
void* ReqSender1307::createInstance()
{
	return new ReqSender1307() ;
}
void ReqSender1307::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1307::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1307::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1307);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req1307 message1;
	PackageScene * packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	message1.set_pob((int)source);
	message1.set_generalid((long long)source1);


	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}