
#include "ReqSender1306.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(ReqSender1306)

	ReqSender1306::ReqSender1306() 
{

}
ReqSender1306::~ReqSender1306() 
{

}
void* ReqSender1306::createInstance()
{
	return new ReqSender1306() ;
}
void ReqSender1306::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1306::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1306::send(void* source, void* source1)
{
	// set message id
	CommonMessage comMessage;
	comMessage.set_cmdid(1306);
	Req1306 message1;
	// set message content
	PackageScene * packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}