
#ifndef Blog_C___Reflection_ReqSender1306_h
#define Blog_C___Reflection_ReqSender1306_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1131 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1131)

public:
	SYNTHESIZE(ReqSender1131, int*, m_pValue)

		ReqSender1131() ;
	virtual ~ReqSender1131() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
