
#ifndef Blog_C___Reflection_ReqSender5059_h
#define Blog_C___Reflection_ReqSender5059_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5059 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5059)

public:
	SYNTHESIZE(ReqSender5059, int*, m_pValue)

	ReqSender5059() ;
	virtual ~ReqSender5059() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
