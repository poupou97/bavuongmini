
#include "ReqSender5057.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../ui/generals_ui/GeneralsUI.h"

IMPLEMENT_CLASS(ReqSender5057)

	ReqSender5057::ReqSender5057() 
{

}
ReqSender5057::~ReqSender5057() 
{

}
void* ReqSender5057::createInstance()
{
	return new ReqSender5057() ;
}
void ReqSender5057::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5057::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5057::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5057);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5057 message1;

	message1.set_generalid((long long)source);
	message1.set_eatgeneralid((long long)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}