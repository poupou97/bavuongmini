

#ifndef Blog_C___Reflection_ReqSender1804_h
#define Blog_C___Reflection_ReqSender1804_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1804 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1804)
    
public:
    SYNTHESIZE(ReqSender1804, int*, m_pValue)
    
    ReqSender1804() ;
    virtual ~ReqSender1804() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
