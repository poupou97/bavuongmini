
#include "ReqSender1544.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1544)
ReqSender1544::ReqSender1544() 
{

}
ReqSender1544::~ReqSender1544() 
{
}
void* ReqSender1544::createInstance()
{
	return new ReqSender1544() ;
}
void ReqSender1544::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1544::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1544::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1544);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}