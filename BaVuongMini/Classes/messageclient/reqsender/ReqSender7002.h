
#ifndef Blog_C___Reflection_ReqSender7002_h
#define Blog_C___Reflection_ReqSender7002_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender7002 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender7002)

public:
	SYNTHESIZE(ReqSender7002, int*, m_pValue)

		ReqSender7002() ;
	virtual ~ReqSender7002() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
