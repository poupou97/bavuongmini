
#include "ReqSender3001.h"
#include "../ClientNetEngine.h"
#include "../protobuf/OfflineFight.pb.h"

IMPLEMENT_CLASS(ReqSender3001)
	ReqSender3001::ReqSender3001() 
{

}
ReqSender3001::~ReqSender3001() 
{
}
void* ReqSender3001::createInstance()
{
	return new ReqSender3001() ;
}
void ReqSender3001::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender3001::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender3001::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(3001);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}