
#ifndef Blog_C___Reflection_ReqSender5104_h
#define Blog_C___Reflection_ReqSender5104_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5104: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5104)

public:
	SYNTHESIZE(ReqSender5104, int*, m_pValue)

	ReqSender5104() ;
	virtual ~ReqSender5104() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
