
#ifndef Blog_C___Reflection_ReqSender5101_h
#define Blog_C___Reflection_ReqSender5101_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5101: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5101)

public:
	SYNTHESIZE(ReqSender5101, int*, m_pValue)

	ReqSender5101() ;
	virtual ~ReqSender5101() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
