
#ifndef Blog_C___Reflection_ReqSender1308_h
#define Blog_C___Reflection_ReqSender1308_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1308 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1308)

public:
	SYNTHESIZE(ReqSender1308, int*, m_pValue)

		ReqSender1308() ;
	virtual ~ReqSender1308() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
