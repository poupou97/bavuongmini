
#include "ReqSender1509.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1509)
ReqSender1509::ReqSender1509() 
{
    
}
ReqSender1509::~ReqSender1509() 
{
}
void* ReqSender1509::createInstance()
{
    return new ReqSender1509() ;
}
void ReqSender1509::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1509::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1509::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1509);
	
	Req1509 message1;

	char * propId = (char *)source;
	message1.set_propid(propId);
	message1.set_amount((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}