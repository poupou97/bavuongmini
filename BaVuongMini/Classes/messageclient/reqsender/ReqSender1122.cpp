
#include "ReqSender1122.h"
#include "../ClientNetEngine.h"
#include "../element/CShortCut.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutConfigure.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(ReqSender1122)
	ReqSender1122::ReqSender1122() 
{

}
ReqSender1122::~ReqSender1122() 
{

}
void* ReqSender1122::createInstance()
{
	return new ReqSender1122() ;
}
void ReqSender1122::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1122::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1122::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1122);

	CShortCut * shortcut = (CShortCut *)(source);

	//��ͬ�Ĳ���ͬʱװ�
// 	for (int i = 0;i<GameView::getInstance()->shortCutList.size();++i)
// 	{
// 		if (GameView::getInstance()->shortCutList.at(i)->complexflag() != 1)
// 		{
// 			if (GameView::getInstance()->shortCutList.at(i)->storedtype() == shortcut->storedtype())
// 			{
// 				if (strcmp(GameView::getInstance()->shortCutList.at(i)->skillpropid().c_str(),shortcut->skillpropid().c_str()) == 0)
// 				{
// 					const char *dialog_shortCutLayer = StringDataManager::getString("shortCutLayer_equiped");
// 					GameView::getInstance()->showAlertDialog(dialog_shortCutLayer);
// 
// 					return;
// 				}
// 			}
// 		}
// 	}

// 	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutConfigure))
// 	{
// 		ShortcutConfigure * temp = (ShortcutConfigure * )(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutConfigure));
// 		//temp->closeAnim();
// 		temp->removeFromParent();
// 	}

	Req1122 message1;
	message1.set_flag(0);
	message1.set_index(shortcut->index()-100);
	message1.set_skillpropid(shortcut->skillpropid());
	message1.set_storedtype(shortcut->storedtype());

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}