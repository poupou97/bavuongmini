
#ifndef Blog_C___Reflection_ReqSender5011_h
#define Blog_C___Reflection_ReqSender5011_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5011 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5011)

public:
	SYNTHESIZE(ReqSender5011, int*, m_pValue)

		ReqSender5011() ;
	virtual ~ReqSender5011() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
