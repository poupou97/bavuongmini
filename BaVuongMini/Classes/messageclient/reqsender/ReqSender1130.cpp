
#include "ReqSender1130.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/NPCMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1130)
	ReqSender1130::ReqSender1130() 
{

}
ReqSender1130::~ReqSender1130() 
{

}
void* ReqSender1130::createInstance()
{
	return new ReqSender1130() ;
}
void ReqSender1130::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1130::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1130::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1130);

	Req1130 message1;

	message1.set_functionid((int)source);
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}