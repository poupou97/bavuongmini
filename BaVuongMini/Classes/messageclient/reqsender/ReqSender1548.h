

#ifndef Blog_C___Reflection_ReqSender1548_h
#define Blog_C___Reflection_ReqSender1548_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1548 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1548)

public:
	SYNTHESIZE(ReqSender1548, int*, m_pValue)

	ReqSender1548() ;
	virtual ~ReqSender1548() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
