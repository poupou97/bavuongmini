
#include "ReqSender5011.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/CollectMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../gamescene_state/sceneelement/MissionCell.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../element/CPushCollectInfo.h"
#include "../../gamescene_state/role/PickingActor.h"

IMPLEMENT_CLASS(ReqSender5011)

	ReqSender5011::ReqSender5011() 
{

}
ReqSender5011::~ReqSender5011() 
{

}
void* ReqSender5011::createInstance()
{
	return new ReqSender5011() ;
}
void ReqSender5011::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5011::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5011::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5011);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	ReqStartCollect5011 message1;

	PickingActor * pickingActor = (PickingActor *)source;
	message1.set_instanceid(pickingActor->p_collectInfo->instanceid());
	message1.set_x(pickingActor->p_collectInfo->x());
	message1.set_y(pickingActor->p_collectInfo->y());

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}