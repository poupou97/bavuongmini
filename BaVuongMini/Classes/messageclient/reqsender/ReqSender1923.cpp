#include "ReqSender1923.h"

#include "../ClientNetEngine.h"
#include "../protobuf/CopyMessage.pb.h" 
#include "../../ui/FivePersonInstance/FivePersonInstance.h"

IMPLEMENT_CLASS(ReqSender1923)
ReqSender1923::ReqSender1923() 
{
    
}
ReqSender1923::~ReqSender1923() 
{
    
}
void* ReqSender1923::createInstance()
{
    return new ReqSender1923() ;
}
void ReqSender1923::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1923::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1923::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1923);

	CCLOG("send msg: %d, request to call frient to enter the instance ", comMessage.cmdid());

	Req1923 message2;
	message2.set_friendid((long long)source);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}