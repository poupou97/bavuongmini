
#include "ReqSender1926.h"

#include "../ClientNetEngine.h"
#include "../protobuf/CopyMessage.pb.h" 
#include "../../ui/FivePersonInstance/FivePersonInstance.h"

IMPLEMENT_CLASS(ReqSender1926)
ReqSender1926::ReqSender1926() 
{

}
ReqSender1926::~ReqSender1926() 
{

}
void* ReqSender1926::createInstance()
{
	return new ReqSender1926() ;
}
void ReqSender1926::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1926::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1926::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1926);

	CCLOG("send msg: %d, request to exit the instance ", comMessage.cmdid());

	Req1926 message2;
	message2.set_copyid((int)source);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}