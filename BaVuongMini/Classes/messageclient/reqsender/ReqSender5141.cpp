
#include "ReqSender5141.h"

#include "../protobuf/DailyMessage.pb.h"  
#include "../ClientNetEngine.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(ReqSender5141)

ReqSender5141::ReqSender5141() 
{

}
ReqSender5141::~ReqSender5141() 
{

}
void* ReqSender5141::createInstance()
{
	return new ReqSender5141() ;
}
void ReqSender5141::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5141::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5141::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5141);
	
	Req5141 message1;

	message1.set_id((long long)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}