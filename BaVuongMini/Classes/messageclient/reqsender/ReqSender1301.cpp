
#include "ReqSender1301.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(ReqSender1301)

	ReqSender1301::ReqSender1301() 
{

}
ReqSender1301::~ReqSender1301() 
{

}
void* ReqSender1301::createInstance()
{
	return new ReqSender1301() ;
}
void ReqSender1301::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1301::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1301::send(void* source, void* source1)
{
	// set message id
	CommonMessage comMessage;
	comMessage.set_cmdid(1301);

	// set message content

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}