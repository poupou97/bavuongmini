
#ifndef Blog_C___Reflection_ReqSender5301_h
#define Blog_C___Reflection_ReqSender5301_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5301: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5301)

public:
	SYNTHESIZE(ReqSender5301, int*, m_pValue)

		ReqSender5301() ;
	virtual ~ReqSender5301() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
