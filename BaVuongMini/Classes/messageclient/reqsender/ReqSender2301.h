#ifndef Blog_C___Reflection_ReqSender2301_h
#define Blog_C___Reflection_ReqSender2301_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender2301: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender2301)

public:
	SYNTHESIZE(ReqSender2301, int*, m_pValue)

	ReqSender2301() ;
	virtual ~ReqSender2301() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
