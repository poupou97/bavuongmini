
#ifndef Blog_C___Reflection_ReqSender5072_h
#define Blog_C___Reflection_ReqSender5072_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5072 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5072)

public:
	SYNTHESIZE(ReqSender5072, int*, m_pValue)

		ReqSender5072() ;
	virtual ~ReqSender5072() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
