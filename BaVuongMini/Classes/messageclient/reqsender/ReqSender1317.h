
#ifndef Blog_C___Reflection_ReqSender1317_h
#define Blog_C___Reflection_ReqSender1317_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1317 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1317)

public:
	SYNTHESIZE(ReqSender1317, int*, m_pValue)

		ReqSender1317() ;
	virtual ~ReqSender1317() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
