#include "ReqSender1317.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender1317)

	ReqSender1317::ReqSender1317() 
{

}
ReqSender1317::~ReqSender1317() 
{

}
void* ReqSender1317::createInstance()
{
	return new ReqSender1317() ;
}
void ReqSender1317::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1317::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1317::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1317);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req1320 message1;

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}