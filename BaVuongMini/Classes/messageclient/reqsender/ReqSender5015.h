
#ifndef Blog_C___Reflection_ReqSender5015_h
#define Blog_C___Reflection_ReqSender5015_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5015: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5015)

public:
	SYNTHESIZE(ReqSender5015, int*, m_pValue)

	ReqSender5015() ;
	virtual ~ReqSender5015() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
