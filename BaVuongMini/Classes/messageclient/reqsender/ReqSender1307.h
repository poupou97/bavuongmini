
#ifndef Blog_C___Reflection_ReqSender1307_h
#define Blog_C___Reflection_ReqSender1307_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1307 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1307)

public:
	SYNTHESIZE(ReqSender1307, int*, m_pValue)

		ReqSender1307() ;
	virtual ~ReqSender1307() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
