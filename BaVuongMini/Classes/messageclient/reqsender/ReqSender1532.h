

#ifndef Blog_C___Family_ReqSender1532_h
#define Blog_C___Family_ReqSender1532_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1532 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1532)

public:
	SYNTHESIZE(ReqSender1532, int*, m_pValue)

		ReqSender1532() ;
	virtual ~ReqSender1532() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
