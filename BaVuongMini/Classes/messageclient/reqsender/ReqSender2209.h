

#ifndef Blog_C___Reflection_ReqSender2209_h
#define Blog_C___Reflection_ReqSender2209_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender2209 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender2209)
    
public:
    SYNTHESIZE(ReqSender2209, int*, m_pValue)
    
    ReqSender2209() ;
    virtual ~ReqSender2209() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
