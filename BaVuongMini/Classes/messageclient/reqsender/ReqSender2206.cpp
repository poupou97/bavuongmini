#include "ReqSender2206.h"
#include "../ClientNetEngine.h"
#include "../protobuf/RelationMessage.pb.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CRelationPlayer.h"

IMPLEMENT_CLASS(ReqSender2206)
ReqSender2206::ReqSender2206() 
{
    
}
ReqSender2206::~ReqSender2206() 
{
}
void* ReqSender2206::createInstance()
{
    return new ReqSender2206() ;
}
void ReqSender2206::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2206::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender2206::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2206);
	
	Rsp2206 message1;

	/**
	 * 客户端确认是否同意加为好友的申请(Req2206)。
	* 本条指令虽然是Rsp开头，但是是由客户端发到服务器。
	*/

	MainScene * mainscene =(MainScene *)GameView::getInstance()->getMainUIScene();
	message1.set_applicantid(mainscene->getInteractOfPlayerId());
	message1.set_agree(mainscene->getInteractOfPlayer());

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}