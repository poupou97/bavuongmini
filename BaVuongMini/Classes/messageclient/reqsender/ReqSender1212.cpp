
#include "ReqSender1212.h"
#include "../ClientNetEngine.h"

#include "../protobuf/FightMessage.pb.h"



IMPLEMENT_CLASS(ReqSender1212)
ReqSender1212::ReqSender1212() 
{

}
ReqSender1212::~ReqSender1212() 
{

}
void* ReqSender1212::createInstance()
{
	return new ReqSender1212() ;
}
void ReqSender1212::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1212::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1212::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1212);
	ReqSyncPickupSeting1212 message1;

	ReqSyncPickupSeting1212 * pick_ = (ReqSyncPickupSeting1212 *)source;

	message1.set_type(pick_->type());
	for (int i=0;i<pick_->list_size();i++)
	{
		message1.add_list(pick_->list(i));
	}
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}