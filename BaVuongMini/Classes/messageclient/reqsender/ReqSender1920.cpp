
#include "ReqSender1920.h"

#include "../ClientNetEngine.h"

#include "../protobuf/CopyMessage.pb.h"  

IMPLEMENT_CLASS(ReqSender1920)
ReqSender1920::ReqSender1920() 
{
    
}
ReqSender1920::~ReqSender1920() 
{
    
}
void* ReqSender1920::createInstance()
{
    return new ReqSender1920() ;
}
void ReqSender1920::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1920::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1920::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1920);

	CCLOG("send msg: %d, request to enter instance waiting sequence", comMessage.cmdid());

	Req1920 message2;
	message2.set_copyid((int)source);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}