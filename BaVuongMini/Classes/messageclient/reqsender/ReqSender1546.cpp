
#include "ReqSender1546.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1546)
	ReqSender1546::ReqSender1546() 
{

}
ReqSender1546::~ReqSender1546() 
{
}
void* ReqSender1546::createInstance()
{
	return new ReqSender1546() ;
}
void ReqSender1546::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1546::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1546::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1546);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}