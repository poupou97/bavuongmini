
#include "ReqSender5056.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5056)

	ReqSender5056::ReqSender5056() 
{

}
ReqSender5056::~ReqSender5056() 
{

}
void* ReqSender5056::createInstance()
{
	return new ReqSender5056() ;
}
void ReqSender5056::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5056::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5056::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5056);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5056 message1;

	message1.set_generalid((long long )source);
	message1.set_fightstatus((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}