
#ifndef Blog_C___Reflection_ReqSender1303_h
#define Blog_C___Reflection_ReqSender1303_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1303 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1303)

public:
	SYNTHESIZE(ReqSender1303, int*, m_pValue)

		ReqSender1303() ;
	virtual ~ReqSender1303() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
