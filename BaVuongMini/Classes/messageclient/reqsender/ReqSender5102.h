#ifndef Blog_C___Reflection_ReqSender5102_h
#define Blog_C___Reflection_ReqSender5102_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5102: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5102)

public:
	SYNTHESIZE(ReqSender5102, int*, m_pValue)

	ReqSender5102() ;
	virtual ~ReqSender5102() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
