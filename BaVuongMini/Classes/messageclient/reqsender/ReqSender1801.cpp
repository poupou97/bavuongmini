
#include "ReqSender1801.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/RelationMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"

#include "GameView.h"
#include "../protobuf/AuctionMessage.pb.h"
#include "../../ui/Auction_ui/AuctionUi.h"


IMPLEMENT_CLASS(ReqSender1801)
ReqSender1801::ReqSender1801() 
{
    
}
ReqSender1801::~ReqSender1801() 
{
}
void* ReqSender1801::createInstance()
{
    return new ReqSender1801() ;
}
void ReqSender1801::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1801::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1801::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1801);
	
	Req1801 message1;

	AuctionUi* auctionui = (AuctionUi *)source;//(AuctionUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);

	message1.set_auctiontype(auctionui->setAuctiontype);// 寄售物类型 0、不做查询条件 1、全部 2、武器 3、防具 4、饰品 5、其他
	message1.set_auctionsubtype(auctionui->setAuctionsubtype);//寄售物子类型（目前只在其他分类中用到） 0、不做查询条件 1、镶嵌材料2、继承材料3、元宝)
	message1.set_level(auctionui->setlevel);//物品等级 0、不做查询条件
	message1.set_quality(auctionui->setquality);//品质 0、不做查询条件 1、次品 2凡品 3下品 4中品 5上品 6圣品 7帝品 8仙品
	message1.set_name(auctionui->setname);//物品名称 空为不做查询条件
	message1.set_pageindex(auctionui->setpageindex);//第几页
	message1.set_orderby(auctionui->setorderby);//0默认（上架时间） 物品品质  等级 剩余时间 价格
	message1.set_orderbydesc(auctionui->setorderbydesc);//0升序 1降序
	message1.set_moneytype(auctionui->setMoney);//0全部货币 1 金币 2 元宝

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}