

#ifndef Blog_C___Reflection_ReqSender1130_h
#define Blog_C___Reflection_ReqSender1130_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

/*
* 选择了NPC界面中的功能
*/
class ReqSender1130 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1130)

public:
	SYNTHESIZE(ReqSender1130, int*, m_pValue)

		ReqSender1130() ;
	virtual ~ReqSender1130() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
