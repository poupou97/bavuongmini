
#include "ReqSender1401.h"

#include "../ClientNetEngine.h"

#include "../protobuf/RelationMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "GameView.h"


IMPLEMENT_CLASS(ReqSender1401)
ReqSender1401::ReqSender1401() 
{
    
}
ReqSender1401::~ReqSender1401() 
{
}
void* ReqSender1401::createInstance()
{
    return new ReqSender1401() ;
}
void ReqSender1401::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1401::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1401::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1401);
	
	Req1401 message1;

	message1.set_playerid((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}