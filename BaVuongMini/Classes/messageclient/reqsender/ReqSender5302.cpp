
#include "ReqSender5302.h"

#include "../protobuf/WorldBossMessage.pb.h"
#include "../ClientNetEngine.h"

IMPLEMENT_CLASS(ReqSender5302)

	ReqSender5302::ReqSender5302() 
{

}
ReqSender5302::~ReqSender5302() 
{

}
void* ReqSender5302::createInstance()
{
	return new ReqSender5302() ;
}
void ReqSender5302::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5302::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5302::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5302);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}