#ifndef __REQSENDER_PROTOCOL_H__
#define __REQSENDER_PROTOCOL_H__

#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
USING_NS_CC;

/**
 * Common interface for ReqSenders
 */
class ReqSenderProtocol
{
public:
    virtual void send(void* source, void* source1 = NULL) = 0;
};

#endif