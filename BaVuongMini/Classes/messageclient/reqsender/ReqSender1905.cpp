
#include "ReqSender1905.h"

#include "../ClientNetEngine.h"

#include "../protobuf/CopyMessage.pb.h"  
#include "../../ui/challengeRound/ShowLevelUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../ui/challengeRound/SingleRankListUi.h"

IMPLEMENT_CLASS(ReqSender1905)
ReqSender1905::ReqSender1905() 
{
    
}
ReqSender1905::~ReqSender1905() 
{
    
}
void* ReqSender1905::createInstance()
{
    return new ReqSender1905() ;
}
void ReqSender1905::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1905::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1905::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1905);

	ReqPrizeInfo1905 message2;

	message2.set_clazz((int)source);
	
	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}