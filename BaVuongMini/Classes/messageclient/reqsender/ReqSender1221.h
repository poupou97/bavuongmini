

#ifndef Blog_C___Reflection_ReqSender1221_h
#define Blog_C___Reflection_ReqSender1221_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1221 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1221)
    
public:
    SYNTHESIZE(ReqSender1221, int*, m_pValue)
    
    ReqSender1221() ;
    virtual ~ReqSender1221() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
