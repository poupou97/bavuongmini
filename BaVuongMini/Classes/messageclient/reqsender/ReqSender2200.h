

#ifndef Blog_C___Reflection_ReqSender2200_h
#define Blog_C___Reflection_ReqSender2200_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender2200 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender2200)
    
public:
    SYNTHESIZE(ReqSender2200, int*, m_pValue)
    
    ReqSender2200() ;
    virtual ~ReqSender2200() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};
} ;

#endif
