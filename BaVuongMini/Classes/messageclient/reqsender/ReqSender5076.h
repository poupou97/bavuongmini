#ifndef Blog_C___Reflection_ReqSender5076_h
#define Blog_C___Reflection_ReqSender5076_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5076 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5076)

public:
	SYNTHESIZE(ReqSender5076, int*, m_pValue)

		ReqSender5076() ;
	virtual ~ReqSender5076() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
