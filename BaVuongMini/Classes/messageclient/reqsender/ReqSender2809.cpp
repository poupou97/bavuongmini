
#include "ReqSender2809.h"
#include "../ClientNetEngine.h"
#include "../protobuf/OfflineFight.pb.h"
#include "../protobuf/QuestionMessage.pb.h"

IMPLEMENT_CLASS(ReqSender2809)
	ReqSender2809::ReqSender2809() 
{

}
ReqSender2809::~ReqSender2809() 
{
}
void* ReqSender2809::createInstance()
{
	return new ReqSender2809() ;
}
void ReqSender2809::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender2809::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender2809::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(2809);

	Req2809 message1;

	message1.set_status(int(source));

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}