
#include "ReqSender1114.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1114)
	ReqSender1114::ReqSender1114() 
{

}
ReqSender1114::~ReqSender1114() 
{

}
void* ReqSender1114::createInstance()
{
	return new ReqSender1114() ;
}
void ReqSender1114::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1114::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1114::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1114);

	// no content
	Req1114 message1;
	char *mapid =(char *)source;
	Vec2* pos = (Vec2*)source1;
	message1.set_mapid(mapid);
	message1.set_x(pos->x);
	message1.set_y(pos->y);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}