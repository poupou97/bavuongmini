

#ifndef Blog_C___Family_ReqSender1531_h
#define Blog_C___Family_ReqSender1531_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1531 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1531)

public:
	SYNTHESIZE(ReqSender1531, int*, m_pValue)

		ReqSender1531() ;
	virtual ~ReqSender1531() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
