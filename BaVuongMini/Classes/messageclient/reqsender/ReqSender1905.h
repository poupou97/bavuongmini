
#ifndef Blog_C___Reflection_ReqSender1905_h
#define Blog_C___Reflection_ReqSender1905_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1905 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1905)
    
public:
    SYNTHESIZE(ReqSender1905, int*, m_pValue)
    
    ReqSender1905() ;
    virtual ~ReqSender1905() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
