
#include "ReqSender1704.h"
#include "../ClientNetEngine.h"
#include "../protobuf/AdditionStore.pb.h"

IMPLEMENT_CLASS(ReqSender1704)
	ReqSender1704::ReqSender1704() 
{

}
ReqSender1704::~ReqSender1704() 
{
}
void* ReqSender1704::createInstance()
{
	return new ReqSender1704() ;
}
void ReqSender1704::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1704::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1704::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1704);

	ReqGoldStoreCommodity1704 message1;
	if ((int)source == 0)
	{
		message1.set_isbindstore(false);
	}
	else
	{
		message1.set_isbindstore(true);
	}

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}