
#include "ReqSender1313.h"

#include "../ClientNetEngine.h"
#include "../protobuf/ItemMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/FolderInfo.h"

IMPLEMENT_CLASS(ReqSender1313)

	ReqSender1313::ReqSender1313() 
{

}
ReqSender1313::~ReqSender1313() 
{

}
void* ReqSender1313::createInstance()
{
	return new ReqSender1313() ;
}
void ReqSender1313::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1313::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1313::send(void* source, void* source1)
{
	// set message id
	CommonMessage comMessage;
	comMessage.set_cmdid(1313);
	Req1313 message1;

	// set message content
	//PackageScene * packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	//message1.set_folder(packageScene->pacPageView->curPackageItemIndex);
	message1.set_folder(int(source));
	message1.set_amount(int(source1));

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}