

#ifndef Blog_C___Family_ReqSender1500_h
#define Blog_C___Family_ReqSender1500_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1500 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1500)
    
public:
	SYNTHESIZE(ReqSender1500, int*, m_pValue)

	ReqSender1500() ;
    virtual ~ReqSender1500() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
