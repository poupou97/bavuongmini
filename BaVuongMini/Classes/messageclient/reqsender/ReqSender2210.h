

#ifndef Blog_C___Reflection_ReqSender2210_h
#define Blog_C___Reflection_ReqSender2210_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender2210 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender2210)
    
public:
    SYNTHESIZE(ReqSender2210, int*, m_pValue)
    
    ReqSender2210() ;
    virtual ~ReqSender2210() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
} ;

#endif
