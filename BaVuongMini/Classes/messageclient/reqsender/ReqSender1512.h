
#ifndef Blog_C___Family_ReqSender1512_h
#define Blog_C___Family_ReqSender1512_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1512 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1512)
    
public:
	SYNTHESIZE(ReqSender1512, int*, m_pValue)

	ReqSender1512() ;
    virtual ~ReqSender1512() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
