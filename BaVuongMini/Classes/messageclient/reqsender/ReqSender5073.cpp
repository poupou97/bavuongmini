
#include "ReqSender5073.h"

#include "../ClientNetEngine.h"
#include "../protobuf/GeneralMessage.pb.h"

IMPLEMENT_CLASS(ReqSender5073)

	ReqSender5073::ReqSender5073() 
{

}
ReqSender5073::~ReqSender5073() 
{

}
void* ReqSender5073::createInstance()
{
	return new ReqSender5073() ;
}
void ReqSender5073::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5073::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5073::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5073);
	CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5073 message1;
	message1.set_generalid((int)source);
	message1.set_grid((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);

}