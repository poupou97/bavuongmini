
#include "ReqSender7013.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../protobuf/MissionMessage.pb.h"

IMPLEMENT_CLASS(ReqSender7013)

ReqSender7013::ReqSender7013() 
{

}
ReqSender7013::~ReqSender7013() 
{

}
void* ReqSender7013::createInstance()
{
	return new ReqSender7013() ;
}
void ReqSender7013::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender7013::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender7013::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(7013);
	CCLOG("send msg: %d", comMessage.cmdid());

	int nGameType = (int)source1;

	// set message content
	ReqPlayedGame7013 message1;
	message1.set_difficult((int)source);
	message1.set_gametype((com::future::threekingdoms::server::transport::protocol::GameType)nGameType);
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}