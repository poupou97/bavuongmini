
#include "ReqSender1405.h"

#include "../ClientNetEngine.h" 
#include "../protobuf/RelationMessage.pb.h"



IMPLEMENT_CLASS(ReqSender1405)
ReqSender1405::ReqSender1405() 
{
    
}
ReqSender1405::~ReqSender1405() 
{
}
void* ReqSender1405::createInstance()
{
    return new ReqSender1405() ;
}
void ReqSender1405::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1405::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1405::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1405);
	
	Req1405 message1;

	
	message1.set_index((int)source);//复选框编号 0, 1
	message1.set_fuctionvalue((int)source1);//设置状态值  0=取消  1=选中
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}