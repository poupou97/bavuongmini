
#ifndef Blog_C___Reflection_ReqSender5132_h
#define Blog_C___Reflection_ReqSender5132_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5132: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5132)

public:
	SYNTHESIZE(ReqSender5132, int*, m_pValue)

	ReqSender5132() ;
	virtual ~ReqSender5132() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
