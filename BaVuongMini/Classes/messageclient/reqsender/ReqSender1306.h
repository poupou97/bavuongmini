
#ifndef Blog_C___Reflection_ReqSender1306_h
#define Blog_C___Reflection_ReqSender1306_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1306 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1306)

public:
	SYNTHESIZE(ReqSender1306, int*, m_pValue)

		ReqSender1306() ;
	virtual ~ReqSender1306() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
