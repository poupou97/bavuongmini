
#include "ReqSender1921.h"

#include "../ClientNetEngine.h"

#include "../protobuf/CopyMessage.pb.h"  

IMPLEMENT_CLASS(ReqSender1921)
ReqSender1921::ReqSender1921() 
{
    
}
ReqSender1921::~ReqSender1921() 
{
    
}
void* ReqSender1921::createInstance()
{
    return new ReqSender1921() ;
}
void ReqSender1921::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1921::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1921::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1921);

	CCLOG("send msg: %d, request to enter the instance directly", comMessage.cmdid());

	Req1921 message2;
	message2.set_copyid((int)source);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}