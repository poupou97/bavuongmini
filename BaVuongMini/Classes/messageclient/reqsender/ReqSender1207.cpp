
#include "ReqSender1207.h"

#include "../ClientNetEngine.h"
#include "../protobuf/FightMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1207)
ReqSender1207::ReqSender1207() 
{
    
}
ReqSender1207::~ReqSender1207() 
{
    
}
void* ReqSender1207::createInstance()
{
    return new ReqSender1207() ;
}
void ReqSender1207::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1207::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1207::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1207);

	CCLOG("send msg: %d, pk mode: %d", comMessage.cmdid(), (int)source);

	Req1207 message2;
	message2.set_status((int)source);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}