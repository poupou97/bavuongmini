
#ifndef Blog_C___Reflection_ReqSender5052_h
#define Blog_C___Reflection_ReqSender5052_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5052 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5052)

public:
	SYNTHESIZE(ReqSender5052, int*, m_pValue)

		ReqSender5052() ;
	virtual ~ReqSender5052() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
