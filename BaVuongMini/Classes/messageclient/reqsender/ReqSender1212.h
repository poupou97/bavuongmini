

#ifndef Blog_C___Reflection_ReqSender1212_h
#define Blog_C___Reflection_ReqSender1212_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

/*
* ʰȡ��Ʒ
*/
class ReqSender1212 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1212)

public:
	SYNTHESIZE(ReqSender1212, int*, m_pValue)

	ReqSender1212() ;
	virtual ~ReqSender1212() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
