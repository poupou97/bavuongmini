

#ifndef Blog_C___Reflection_ReqSender1000_h
#define Blog_C___Reflection_ReqSender1000_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

/*
* heart beat, for checking network is connected
*/
class ReqSender1000 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1000)
    
public:
    SYNTHESIZE(ReqSender1000, int*, m_pValue)
    
    ReqSender1000() ;
    virtual ~ReqSender1000() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
