
#include "ReqSender5113.h"

#include "../protobuf/GeneralMessage.pb.h"
#include "../ClientNetEngine.h"
#include "../protobuf/DailyGiftSign.pb.h"

IMPLEMENT_CLASS(ReqSender5113)

ReqSender5113::ReqSender5113() 
{

}
ReqSender5113::~ReqSender5113() 
{

}
void* ReqSender5113::createInstance()
{
	return new ReqSender5113() ;
}
void ReqSender5113::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5113::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5113::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5113);
	//CCLOG("send msg: %d", comMessage.cmdid());

	// set message content
	Req5113 message1;
	message1.set_number((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}