
#include "ReqSender3004.h"
#include "../ClientNetEngine.h"
#include "../protobuf/OfflineFight.pb.h"

IMPLEMENT_CLASS(ReqSender3004)
	ReqSender3004::ReqSender3004() 
{

}
ReqSender3004::~ReqSender3004() 
{
}
void* ReqSender3004::createInstance()
{
	return new ReqSender3004() ;
}
void ReqSender3004::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender3004::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender3004::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(3004);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}