
#include "ReqSender1925.h"

#include "../ClientNetEngine.h"
#include "../protobuf/CopyMessage.pb.h" 
#include "../../ui/FivePersonInstance/FivePersonInstance.h"

IMPLEMENT_CLASS(ReqSender1925)
	ReqSender1925::ReqSender1925() 
{

}
ReqSender1925::~ReqSender1925() 
{

}
void* ReqSender1925::createInstance()
{
	return new ReqSender1925() ;
}
void ReqSender1925::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1925::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1925::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1925);

	CCLOG("send msg: %d, request to exit the instance ", comMessage.cmdid());

	Req1925 message2;
	message2.set_grid((int)source);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}