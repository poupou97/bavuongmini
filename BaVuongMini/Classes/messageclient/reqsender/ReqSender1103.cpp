
#include "ReqSender1103.h"

#include "../ClientNetEngine.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(ReqSender1103)

ReqSender1103::ReqSender1103() 
{
    
}
ReqSender1103::~ReqSender1103() 
{
    
}
void* ReqSender1103::createInstance()
{
    return new ReqSender1103() ;
}
void ReqSender1103::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1103::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1103::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1103);

	// set message content
	MyPlayer* player = (MyPlayer*)source;

	Req1103 message2;
	message2.set_x(player->getWorldPosition().x);
	message2.set_y(player->getWorldPosition().y);

	//CCLOG("send msg: %d, MyPlayer Position: ( %f, %f ) ", comMessage.cmdid(), player->getWorldPosition().x, player->getWorldPosition().y);

	string msgData;
	message2.SerializeToString(&msgData);
	comMessage.set_data(msgData);
    
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}