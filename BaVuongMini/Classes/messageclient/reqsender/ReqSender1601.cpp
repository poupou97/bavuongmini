
#include "ReqSender1601.h"
#include "../ClientNetEngine.h"
#include "../protobuf/EquipmentMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1601)
ReqSender1601::ReqSender1601() 
{
    
}
ReqSender1601::~ReqSender1601() 
{
}
void* ReqSender1601::createInstance()
{
    return new ReqSender1601() ;
}
void ReqSender1601::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1601::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1601::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1601);
	
	Req1601 message1;
	
	assistEquipStruct *  equipValue = (assistEquipStruct *)source;
	switch (equipValue->source_)
	{
	case 1:
		{
			message1.set_source(equipValue->source_);
			message1.set_petid(equipValue->petid_);
			message1.set_pob(equipValue->pob_);
		}break;
	case 2:
		{
			message1.set_source(equipValue->source_);
			message1.set_index(equipValue->index_);
		}break;
	}
	message1.set_playtype(equipValue->playType);//0������(Ĭ��) 1������
	
	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}