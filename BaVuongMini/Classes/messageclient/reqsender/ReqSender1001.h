

#ifndef Blog_C___Reflection_ReqSender1001_h
#define Blog_C___Reflection_ReqSender1001_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1001 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1001)
    
public:
    SYNTHESIZE(ReqSender1001, int*, m_pValue)
    
    ReqSender1001() ;
    virtual ~ReqSender1001() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
