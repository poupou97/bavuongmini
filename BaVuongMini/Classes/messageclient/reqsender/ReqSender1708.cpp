
#include "ReqSender1708.h"
#include "../ClientNetEngine.h"
#include "../protobuf/GuildMessage.pb.h"


IMPLEMENT_CLASS(ReqSender1708)
ReqSender1708::ReqSender1708() 
{
    
}
ReqSender1708::~ReqSender1708() 
{
}
void* ReqSender1708::createInstance()
{
    return new ReqSender1708() ;
}
void ReqSender1708::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1708::display() 
{
    cout << *getm_pValue() << endl ;
}

void ReqSender1708::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1708);
	
	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}