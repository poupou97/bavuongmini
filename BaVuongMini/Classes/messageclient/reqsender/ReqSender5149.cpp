
#include "ReqSender5149.h"
#include "../ClientNetEngine.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/DailyGiftSign.pb.h"

IMPLEMENT_CLASS(ReqSender5149)

ReqSender5149::ReqSender5149() 
{

}
ReqSender5149::~ReqSender5149() 
{

}
void* ReqSender5149::createInstance()
{
	return new ReqSender5149() ;
}
void ReqSender5149::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender5149::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender5149::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(5149);

	Req5149 message1;

	message1.set_day((int)source);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}