

#ifndef Blog_C___Reflection_ReqSender1921_h
#define Blog_C___Reflection_ReqSender1921_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender1921 : public CKBaseClass, public ReqSenderProtocol
{
private:
    DECLARE_CLASS(ReqSender1921)
    
public:
    SYNTHESIZE(ReqSender1921, int*, m_pValue)
    
    ReqSender1921() ;
    virtual ~ReqSender1921() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
    int *m_pValue ;

} ;

#endif
