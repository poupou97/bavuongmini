#ifndef Blog_C___Reflection_ReqSender2805_h
#define Blog_C___Reflection_ReqSender2805_h

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender2805 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender2805)

public:
	SYNTHESIZE(ReqSender2805, int*, m_pValue)

		ReqSender2805() ;
	virtual ~ReqSender2805() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;

} ;

#endif
