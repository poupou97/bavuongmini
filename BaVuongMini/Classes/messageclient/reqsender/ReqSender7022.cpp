
#include "ReqSender7022.h"

#include "../ClientNetEngine.h"
#include "../protobuf/MissionMessage.pb.h"

IMPLEMENT_CLASS(ReqSender7022)

	ReqSender7022::ReqSender7022() 
{

}
ReqSender7022::~ReqSender7022() 
{

}
void* ReqSender7022::createInstance()
{
	return new ReqSender7022() ;
}
void ReqSender7022::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender7022::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender7022::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(7022);
	CCLOG("send msg: %d", comMessage.cmdid());

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}