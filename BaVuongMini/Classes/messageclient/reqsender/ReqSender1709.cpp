
#include "ReqSender1709.h"
#include "../ClientNetEngine.h"
#include "../protobuf/AdditionStore.pb.h"

IMPLEMENT_CLASS(ReqSender1709)
ReqSender1709::ReqSender1709() 
{

}
ReqSender1709::~ReqSender1709() 
{
}
void* ReqSender1709::createInstance()
{
	return new ReqSender1709() ;
}
void ReqSender1709::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1709::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1709::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1709);

	ReqBuyGuildCommodity1709 message1;

	message1.set_id((int)source);
	message1.set_amount((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}