
#ifndef Blog_C___Reflection_ReqSender5103_h
#define Blog_C___Reflection_ReqSender5103_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender5103: public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender5103)

public:
	SYNTHESIZE(ReqSender5103, int*, m_pValue)

	ReqSender5103() ;
	virtual ~ReqSender5103() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
