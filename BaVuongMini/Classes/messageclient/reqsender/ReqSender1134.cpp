
#include "ReqSender1134.h"

#include "../ClientNetEngine.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/NPCMessage.pb.h"

IMPLEMENT_CLASS(ReqSender1134)
	ReqSender1134::ReqSender1134() 
{

}
ReqSender1134::~ReqSender1134() 
{

}
void* ReqSender1134::createInstance()
{
	return new ReqSender1134() ;
}
void ReqSender1134::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void ReqSender1134::display() 
{
	cout << *getm_pValue() << endl ;
}

void ReqSender1134::send(void* source, void* source1)
{
	CommonMessage comMessage;
	comMessage.set_cmdid(1134);
	Req1134 message1;

	message1.set_folder((int)source);
	message1.set_amount((int)source1);

	string msgData;
	message1.SerializeToString(&msgData);
	comMessage.set_data(msgData);

	ClientNetEngine::sharedSocketEngine()->send(comMessage);
}