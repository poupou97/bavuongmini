
#ifndef Blog_C___Reflection_ReqSender1301_h
#define Blog_C___Reflection_ReqSender1301_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

/*
* 请求打开背包
*/
class ReqSender1301 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender1301)

public:
	SYNTHESIZE(ReqSender1301, int*, m_pValue)

		ReqSender1301() ;
	virtual ~ReqSender1301() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
