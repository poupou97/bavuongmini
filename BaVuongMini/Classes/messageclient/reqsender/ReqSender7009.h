
#ifndef Blog_C___Reflection_ReqSender7009_h
#define Blog_C___Reflection_ReqSender7009_h

#include "cocos2d.h"
USING_NS_CC;

#include "../../common/CKBaseClass.h"
#include "ReqSenderProtocol.h"

class ReqSender7009 : public CKBaseClass, public ReqSenderProtocol
{
private:
	DECLARE_CLASS(ReqSender7009)

public:
	SYNTHESIZE(ReqSender7009, int*, m_pValue)

		ReqSender7009() ;
	virtual ~ReqSender7009() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void send(void* source, void* source1 = NULL);

protected:
	int *m_pValue ;
};
#endif
