
#ifndef _MESSAGECLIENT_PROTOCOLHELPER_H_
#define _MESSAGECLIENT_PROTOCOLHELPER_H_

#include "cocos2d.h"
USING_NS_CC;

#include "GameProtobuf.h"
USING_NS_THREEKINGDOMS_PROTOCOL;

#include "protobuf/FightMessage.pb.h"

class BaseFighter;

class ProtocolHelper
{
public:
	ProtocolHelper();
	virtual ~ProtocolHelper();

    /** returns a shared instance of the GameMessageProcessor */
    static ProtocolHelper* sharedProtocolHelper(void);

	// ���¸���״̬( buff and debuff )
	void updateFighterStatus(::google::protobuf::RepeatedPtrField< ExtStatusInfo >* exStatusList, BaseFighter* target);

	// ����HP and MP, ˢ���������ϵĽ�ɫ��Ϣ���
	void updateFighterHPMP(BaseFighter* target, int newHP, int newMP);

} ;

#endif
