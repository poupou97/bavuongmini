
#ifndef _MESSAGECLIENT_CGENERALDETAIL_H_
#define _MESSAGECLIENT_CGENERALDETAIL_H_

#include "../GameProtobuf.h"
#include "../protobuf/GeneralMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CGeneralDetail : public GeneralDetail
{
public:
	CGeneralDetail();
	virtual ~CGeneralDetail();
};

#endif