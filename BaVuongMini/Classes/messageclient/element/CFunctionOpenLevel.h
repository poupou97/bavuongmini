#ifndef _MESSAGECLIENT_CFUNCTIONOPENLEVEL_H_
#define _MESSAGECLIENT_CFUNCTIONOPENLEVEL_H_

#include <string>

class CFunctionOpenLevel
{
public:
	CFunctionOpenLevel();
	~CFunctionOpenLevel();

	void set_functionId(std::string _value);
	std::string get_functionId();
	void set_functionName(std::string _value);
	std::string get_functionName();
	void set_functionType(int _value);
	int get_functionType();
	void set_requiredLevel(int _value);
	int get_requiredLevel();
	void set_positionIndex(int _value);
	int get_positionIndex();

private:
	std::string functionId;
	std::string functionName;
	int functionType;
	int requiredLevel ;
	int positionIndex;
};

#endif

