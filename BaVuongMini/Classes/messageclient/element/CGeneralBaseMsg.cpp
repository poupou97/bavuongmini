#include "CGeneralBaseMsg.h"
#include "../../utils/GameUtils.h"
#include "GameView.h"


CGeneralBaseMsg::CGeneralBaseMsg()
{
	m_changeMoment = 0;
//	m_nLeftTime = 0;
}


CGeneralBaseMsg::~CGeneralBaseMsg()
{
}

void CGeneralBaseMsg::set_modelID(int _value )
{
	modelID = _value;
}

int CGeneralBaseMsg::get_modelId()
{
	return modelID;
}

void CGeneralBaseMsg::set_description( std::string _value )
{
	description = _value;
}

std::string CGeneralBaseMsg::get_description()
{
	return description;
}

void CGeneralBaseMsg::set_half_photo( std::string _value )
{
	half_photo = _value;
}

std::string CGeneralBaseMsg::get_half_photo()
{
	return half_photo;
}

void CGeneralBaseMsg::set_head_photo( std::string _value )
{
	head_photo = _value;
}

std::string CGeneralBaseMsg::get_head_photo()
{
	return head_photo;
}

void CGeneralBaseMsg::set_profession( int _value )
{
	profession = _value;
}

int CGeneralBaseMsg::get_profession()
{
	return profession;
}

void CGeneralBaseMsg::set_avatar( std::string _value )
{
	avatar = _value;
}

std::string CGeneralBaseMsg::get_avatar()
{
	return avatar;
}

void CGeneralBaseMsg::set_changeMoment( long long _value )
{
	m_changeMoment = _value;
}

long long CGeneralBaseMsg::get_changeMoment()
{
	return m_changeMoment;
}

// void CGeneralBaseMsg::set_LeftTime( long long _value )
// {
// 	m_nLeftTime = _value;
// }
// 
// long long CGeneralBaseMsg::get_LeftTime()
// {
// 	if (m_nLeftTime <= 0)
// 		return 0;
// 
// 	return m_nLeftTime;
// }

void CGeneralBaseMsg::startCD()
{
	if (this->revivetime() <= 0)
	{
		remainTime = this->showlefttime();
	}
	else
	{
		remainTime = this->revivetime();
	}
	//remainTime = this->showlefttime();
	coolTimeCount = GameUtils::millisecondNow();
	m_bIsInCD = true;
}
void CGeneralBaseMsg::updateCD()
{
	if(m_bIsInCD)
	{
		if (this->revivetime() <= 0)
		{
			remainTime = (this->showlefttime() + 500) - (GameUtils::millisecondNow() - coolTimeCount);
		}
		else
		{
			remainTime = (this->revivetime() + 500) - (GameUtils::millisecondNow() - coolTimeCount);
		}

		if(remainTime <= 0)
		{
			m_bIsInCD = false;
		}
	}
}
bool CGeneralBaseMsg::isInCD()
{
	return m_bIsInCD;
}

bool CGeneralBaseMsg::isOnLine(long long generalid)
{
	for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();i++)
	{
		if (generalid == GameView::getInstance()->generalsInLineList.at(i)->id())
			return true;
	}

	return false;
}

long long CGeneralBaseMsg::getRemainTime()
{
	if(m_bIsInCD)
	{
		return remainTime;
	}
	else
	{
		return 0;
	}
}





////////////////////////////////////////////////////////

GeneralsStateManager::GeneralsStateManager()
{
}

GeneralsStateManager::~GeneralsStateManager()
{
}

static GeneralsStateManager *s_GeneralStateManager = NULL;

GeneralsStateManager* GeneralsStateManager::getInstance(void)
{
	if (!s_GeneralStateManager)
	{
		s_GeneralStateManager = new GeneralsStateManager();
	}

	return s_GeneralStateManager;
}

void GeneralsStateManager::update(float dt)
{
	for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();i++)
	{
		CGeneralBaseMsg * temp = GameView::getInstance()->generalsInLineList.at(i);
		if (temp)
			temp->updateCD();
	}
}

CGeneralBaseMsg * GeneralsStateManager::getGeneralBaseMsgById( long long generalId )
{
	for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();i++)
	{
		CGeneralBaseMsg * temp = GameView::getInstance()->generalsInLineList.at(i);
		if (temp->id() == generalId)
			return temp;
	}

	return NULL;
}
