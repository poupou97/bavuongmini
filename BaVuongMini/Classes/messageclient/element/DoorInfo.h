#ifndef _MESSAGECLIENT_ELEMENT_DOORINFO_H_
#define _MESSAGECLIENT_ELEMENT_DOORINFO_H_

#include <string>
#include <vector>

class DoorInfo {
public:
	/**  目标地图id */
	std::string mapId;
	/**  坐标系 */
	std::vector<short*> doorXY;
};

#endif