#ifndef _MESSAGECLIENT_CEQUIPHOLE_H_
#define _MESSAGECLIENT_CEQUIPHOLE_H_

#include "../protobuf/EquipmentMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;
class CEquipHole:public EquipHole
{
public:
	CEquipHole();
	~CEquipHole();
};
#endif