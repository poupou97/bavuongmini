#include "CNewFunctionRemind.h"


CNewFunctionRemind::CNewFunctionRemind()
{
}


CNewFunctionRemind::~CNewFunctionRemind()
{
}

void CNewFunctionRemind::setId( int _value )
{
	id = _value;
}

int CNewFunctionRemind::getId()
{
	return id;
}

void CNewFunctionRemind::setLevel( int _value )
{
	level = _value;
}

int CNewFunctionRemind::getLevel()
{
	return level;
}

void CNewFunctionRemind::setDes( std::string _value )
{
	des = _value;
}

std::string CNewFunctionRemind::getDes()
{
	return des;
}

void CNewFunctionRemind::setIcon( std::string _value )
{
	icon = _value;
}

std::string CNewFunctionRemind::getIcon()
{
	return icon;
}
