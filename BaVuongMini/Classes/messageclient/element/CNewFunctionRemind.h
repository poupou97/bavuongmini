#ifndef _MESSAGECLIENT_ELEMENT_CNEWFUNCTIONREMIND_H_
#define _MESSAGECLIENT_ELEMENT_CNEWFUNCTIONREMIND_H_

#include <string>
#include <vector>

class CNewFunctionRemind {
public:
	CNewFunctionRemind() ;
	virtual ~CNewFunctionRemind() ;
private:
	int id;
	int level;
	std::string des;
	std::string icon;
public:
	void setId(int _value);
	int getId();
	void setLevel(int _value);
	int getLevel();
	void setDes(std::string _value);
	std::string getDes();
	void setIcon(std::string _value);
	std::string getIcon();
};

#endif

