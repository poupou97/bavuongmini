#ifndef _MESSAGECLIENT_STRENGTHADDVALUE_
#define _MESSAGECLIENT_STRENGTHADDVALUE_

#include <string>
#include <vector>
class MapRefineDiffAddValue
{
public:
	MapRefineDiffAddValue(void);
	~MapRefineDiffAddValue(void);

	void set_level(int _vale);
	int get_level();

	void set_clazz(int _value);
	int get_clazz();

	void set_exp(int exp);
	int get_exp();

	void set_minAttack(int _value);
	int get_minAttack();

	void set_maxAttack(int _value);
	int get_maxAttack();

	void set_minmagic_Attack(int _value);
	int get_minmagicAttack();

	void set_maxmagic_Attack(int _value);
	int get_maxmagicAttack();

	void set_min_define(int _value);
	int get_min_define();

	void set_max_define(int _value);
	int get_max_define();

	void set_min_magic_define(int _value);
	int get_min_magic_define();

	void set_max_magic_define(int _value);
	int get_max_magic_define();

	void set_hp(int _value);
	int get_hp();

	void set_mp(int _value);
	int get_mp();
private:
	int m_level;
	int m_clazz;
	int m_exp;
	int m_minattack;
	int m_maxattack;
	int m_minmagicattack;
	int m_maxmagicattack;
	int m_mindefine;
	int m_maxdefine;
	int m_minmagicdefine;
	int m_maxmagicdefine;
	int m_hp;
	int m_mp;
};

#endif
