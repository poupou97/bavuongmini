#include "CFunctionOpenLevel.h"


CFunctionOpenLevel::CFunctionOpenLevel()
{
}


CFunctionOpenLevel::~CFunctionOpenLevel()
{
}

void CFunctionOpenLevel::set_functionId( std::string _value )
{
	functionId = _value;
}

std::string CFunctionOpenLevel::get_functionId()
{
	return functionId;
}

void CFunctionOpenLevel::set_functionName( std::string _value )
{
	functionName = _value;
}

std::string CFunctionOpenLevel::get_functionName()
{
	return functionName;
}

void CFunctionOpenLevel::set_functionType( int _value )
{
	functionType = _value;
}

int CFunctionOpenLevel::get_functionType()
{
	return functionType;
}

void CFunctionOpenLevel::set_requiredLevel( int _value )
{
	requiredLevel = _value;
}

int CFunctionOpenLevel::get_requiredLevel()
{
	return requiredLevel;
}

void CFunctionOpenLevel::set_positionIndex( int _value )
{
	positionIndex = _value;
}

int CFunctionOpenLevel::get_positionIndex()
{
	return positionIndex;
}
