#include "MapWeapEffect.h"


MapWeapEffect::MapWeapEffect(void)
{
}


MapWeapEffect::~MapWeapEffect(void)
{
}

void MapWeapEffect::set_priority( int value_ )
{
	m_priority = value_;
}

int MapWeapEffect::get_priority()
{
	return m_priority;
}

void MapWeapEffect::set_weapPression( int value_ )
{
	m_weapPression = value_;
}

int MapWeapEffect::get_weapPression()
{
	return m_weapPression;
}

void MapWeapEffect::set_RefineLevel( int value_ )
{
	m_refineLevel = value_;
}

int MapWeapEffect::get_RefineLevel()
{
	return m_refineLevel;
}

void MapWeapEffect::set_StarLevel( int value_ )
{
	m_starLevel = value_;
}

int MapWeapEffect::get_starLevel()
{
	return m_starLevel;
}

void MapWeapEffect::set_weapEffectRefine( std::string value_ )
{
	m_weapEffectRefine = value_;
}

std::string MapWeapEffect::get_weapEffectRefine()
{
	return m_weapEffectRefine;
}

void MapWeapEffect::set_weapEffectStar( std::string value_ )
{
	m_weapEffectStar = value_;
}

std::string MapWeapEffect::get_weapEffectStar()
{
	return m_weapEffectStar;
}
