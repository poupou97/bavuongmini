#include "CSingCopyClazz.h"


CSingCopyClazz::CSingCopyClazz()
{
}


CSingCopyClazz::~CSingCopyClazz()
{
}

void CSingCopyClazz::set_clazz( int vale_ )
{
	m_clazz = vale_;
}

int CSingCopyClazz::get_clazz()
{
	return m_clazz;
}

void CSingCopyClazz::set_starLevel( int vale_ )
{
	m_starlv = vale_;
}

int CSingCopyClazz::get_starLevel()
{
	return m_starlv;
}


void CSingCopyClazz::set_endLevel( int vale_ )
{
	m_endlv = vale_;
}

int CSingCopyClazz::get_endLevel()
{
	return m_endlv;
}

void CSingCopyClazz::set_name( std::string value_ )
{
	m_name = value_;
}

std::string CSingCopyClazz::get_name()
{
	return m_name;
}

void CSingCopyClazz::set_icon( std::string value_ )
{
	m_icon = value_;
}

std::string CSingCopyClazz::get_icon()
{
	return m_icon;
}

void CSingCopyClazz::set_resume( int value_ )
{
	m_resume = value_;
}

int CSingCopyClazz::get_resume()
{
	return m_resume;
}

void CSingCopyClazz::set_reward( std::string value_ )
{
	m_reward = value_;
}

std::string CSingCopyClazz::get_reward()
{
	return m_reward;
}

