
#ifndef _MESSAGECLIENT_CAUCTIONINFO_H_
#define _MESSAGECLIENT_CAUCTIONINFO_H_

#include "../GameProtobuf.h"
#include "../protobuf/AuctionMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CAuctionInfo : public AuctionInfo
{
public:
	CAuctionInfo();
	~CAuctionInfo();
};

#endif

