#ifndef _MESSAGECLIENT_CHOMEMESSAGE_H_
#define _MESSAGECLIENT_CHOMEMESSAGE_H_

#include "../GameProtobuf.h"
#include "../protobuf/DailyMessage.pb.h"


USING_NS_THREEKINGDOMS_PROTOCOL;

class CHomeMessage:public HomeMessage
{
public:
	CHomeMessage(void);
	~CHomeMessage(void);

private:
	long long m_longTimeFlag;
	int m_nPageCur;
	int m_nPageAll;
	int m_nMyRank;
	/*std::string m_strCreaterName;
	std::string m_strAnnouncement;*/
	long long m_longTimeRefreshFlag;

public:
	void set_timeFlag(long long timeFlag);
	long long get_timeFlag();

	void set_pageCur(int nPageCur);
	int get_pageCur();

	void set_pageAll(int nPageAll);
	int get_pageAll();

	void set_myRank(int nMyRank);
	int get_myRank();

	/*void set_createrName(std::string strCreaterName);
	std::string get_createrName();

	void set_announcement(std::string strAnnouncement);
	std::string get_announcement();*/

	void set_timeRefreshFlag(long long timeRefreshFlag);
	long long get_timeRefreshFlag();

};
#endif;
