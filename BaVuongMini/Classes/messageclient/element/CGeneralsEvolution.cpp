#include "CGeneralsEvolution.h"


CGeneralsEvolution::CGeneralsEvolution()
{
}


CGeneralsEvolution::~CGeneralsEvolution()
{
}

int CGeneralsEvolution::get_prpfession()
{
	return profession;
}

void CGeneralsEvolution::set_profession( int _value )
{
	profession = _value;
}

int CGeneralsEvolution::get_rare()
{
	return rare;
}

void CGeneralsEvolution::set_rare( int _value )
{
	rare = _value;
}

int CGeneralsEvolution::get_evolution()
{
	return evolution;
}

void CGeneralsEvolution::set_evolution( int _value )
{
	evolution = _value;
}

int CGeneralsEvolution::get_strenth()
{
	return strenth;
}

void CGeneralsEvolution::set_strenth( int _value )
{
	strenth = _value;
}

int CGeneralsEvolution::get_dexterity()
{
	return dexterity;
}

void CGeneralsEvolution::set_dexterity( int _value )
{
	dexterity = _value;
}

int CGeneralsEvolution::get_intelligence()
{
	return intelligence;
}

void CGeneralsEvolution::set_intelligence( int _value )
{
	intelligence = _value;
}

int CGeneralsEvolution::get_focus()
{
	return focus;
}

void CGeneralsEvolution::set_focus( int _value )
{
	focus = _value;
}

int CGeneralsEvolution::get_need_card()
{
	return need_card;
}

void CGeneralsEvolution::set_need_card( int _value )
{
	need_card = _value;
}

int CGeneralsEvolution::get_cost()
{
	return cost;
}

void CGeneralsEvolution::set_cost( int _value )
{
	cost = _value;
}
