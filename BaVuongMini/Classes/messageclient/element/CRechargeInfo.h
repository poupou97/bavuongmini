#ifndef _MESSAGECLIENT_ELEMENT_CRECHARGEINFO_H_
#define _MESSAGECLIENT_ELEMENT_CRECHARGEINFO_H_

#include <string>
#include <map>

class CRechargeInfo
{
public:
	CRechargeInfo(void);
	~CRechargeInfo(void);

private:
	int id;
	int recharge_value;
	int send_value;
	int suggest;
	std::string icon;
	int orderId;

public:
	void set_id(int _value);
	int get_id();
	void set_recharge_value(int _value);
	int get_recharge_value();
	void set_send_value(int _value);
	int get_send_value();
	void set_suggest(int _value);
	int get_suggest();
	void set_icon(std::string _value);
	std::string get_icon();
	void set_orderId(int _value);
	int get_orderId();
};

#endif

