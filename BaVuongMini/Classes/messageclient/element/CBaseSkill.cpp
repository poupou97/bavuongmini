#include "CBaseSkill.h"


CBaseSkill::CBaseSkill()
{
}


CBaseSkill::~CBaseSkill()
{
}

void CBaseSkill::set_quality( int _value )
{
	quality = _value;
}

int CBaseSkill::get_quality()
{
	return quality;
}

void CBaseSkill::set_profession( int _value )
{
	profession = _value;
}

int CBaseSkill::get_profession()
{
	return profession;
}

void CBaseSkill::set_complex_order( int _value )
{
	complex_order = _value;
}

int CBaseSkill::get_complex_order()
{
	return complex_order;
}

void CBaseSkill::set_skill_variety( int _value )
{
	skill_variety = _value;
}

int CBaseSkill::get_skill_variety()
{
	return skill_variety;
}

void CBaseSkill::set_owner( int _value )
{
	owner = _value;
}

int CBaseSkill::get_owner()
{
	return owner;
}
