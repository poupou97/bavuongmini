
#ifndef _MESSAGECLIENT_CMUSOUTALENT_H_
#define _MESSAGECLIENT_CMUSOUTALENT_H_

#include <string>

class CMusouTalent
{
public:
	CMusouTalent();
	virtual ~CMusouTalent();

	void copyFrom(CMusouTalent * temp);

	void set_id(int _value);
	int get_id();
	void set_clazz(int _value);
	int get_clazz();
	void set_idx(int _value);
	int get_idx();
	void set_level(int _value);
	int get_level();
	void set_name(std::string _value);
	std::string get_name();
	void set_description(std::string _value);
	std::string get_description();
	void set_require_level(int _value);
	int get_require_level();
	void set_require_essence(int _value);
	int get_require_essence();
	void set_icon(std::string _value);
	std::string get_icon();

private:
	int id;
	int clazz;
	int idx;
	int level;
	std::string name;
	std::string description;
	int require_level;
	int require_essence;
	std::string icon;
};

#endif

