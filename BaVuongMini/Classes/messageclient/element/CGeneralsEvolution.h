#ifndef _MESSAGECLIENT_CGENERALSEVOLUTION_H_
#define _MESSAGECLIENT_CGENERALSEVOLUTION_H_

class CGeneralsEvolution
{
public:
	CGeneralsEvolution();
	~CGeneralsEvolution();

	int get_prpfession();
	void set_profession(int _value);
	int get_rare();
	void set_rare(int _value);
	int get_evolution();
	void set_evolution(int _value);
	int get_strenth();
	void set_strenth(int _value);
	int get_dexterity();
	void set_dexterity(int _value);
	int get_intelligence();
	void set_intelligence(int _value);
	int get_focus();
	void set_focus(int _value);
	int get_need_card();
	void set_need_card(int _value);
	int get_cost();
	void set_cost(int _value);

private:
	int profession;
	int rare;
	int evolution;
	int strenth;
	int dexterity;
	int intelligence;
	int focus;
	int need_card;
	int cost;
};

#endif;

