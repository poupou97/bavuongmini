#ifndef _GUILDMESSAGE_GUILDMEMBERBASE_
#define _GUILDMESSAGE_GUILDMEMBERBASE_
#include "cocos2d.h"
#include "../GameProtobuf.h"
#include "../protobuf/GuildMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;
class CGuildMemberBase:public GuildMemberBase
{
public:
	CGuildMemberBase();
	~CGuildMemberBase();
};

#endif;