#ifndef _MESSAGECLIENT_CONEMOUTHSIGNGIFT_H_
#define _MESSAGECLIENT_CONEMOUTHSIGNGIFT_H_

#include "../GameProtobuf.h"
#include "../protobuf/DailyGiftSign.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class COneMouthSignGift:public OneMouthSignGift
{
public:
	COneMouthSignGift();
	~COneMouthSignGift();

};
#endif;
