
#ifndef _ELEMENT_CPUSHCOLLECTINFO_H_
#define _ELEMENT_CPUSHCOLLECTINFO_H_

#include "../GameProtobuf.h"
#include "../protobuf/CollectMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CPushCollectInfo : public PushCollectInfo
{
public:
	CPushCollectInfo();
	~CPushCollectInfo();
};

#endif
