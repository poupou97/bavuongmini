#ifndef _MESSAGECLIENT_CSIMPLEPARTNER_H_
#define _MESSAGECLIENT_CSIMPLEPARTNER_H_

#include "../../common/CKBaseClass.h"
#include "../protobuf/OfflineFight.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CSimplePartner : public SimplePartner
{
public:
	CSimplePartner(void);
	~CSimplePartner(void);
};

#endif

