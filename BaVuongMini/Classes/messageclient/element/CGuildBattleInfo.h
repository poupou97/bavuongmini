
#ifndef _MESSAGECLIENT_CGUILDBATTLEINFO_H_
#define _MESSAGECLIENT_CGUILDBATTLEINFO_H_

#include "../GameProtobuf.h"
#include "../protobuf/GuildMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CGuildBattleInfo:public GuildBattleInfo
{
public:
	CGuildBattleInfo();
	~CGuildBattleInfo();
};
#endif

