
#ifndef _MESSAGECLIENT_CGUIDFIGHTRANKING_H_ 
#define _MESSAGECLIENT_CGUIDFIGHTRANKING_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CGuildFightRanking:public GuildFightRanking
{
public:
	CGuildFightRanking(void);
	~CGuildFightRanking(void);
};
#endif;
