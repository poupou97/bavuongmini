
#ifndef _MESSAGECLIENT_CMISSIONGOODS_H_
#define _MESSAGECLIENT_CMISSIONGOODS_H_

#include "../protobuf/PlayerMessage.pb.h"
#include "../GameProtobuf.h"
#include "../protobuf/MissionMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CMissionGoods : public MissionGoods
{
public:
	CMissionGoods();
	~CMissionGoods();
};
#endif;

