
#ifndef _MESSAGECLIENT_CFIGHTWAYGROWINGUP_H_
#define _MESSAGECLIENT_CFIGHTWAYGROWINGUP_H_

#include "../../common/CKBaseClass.h"
#include "../GameProtobuf.h"
#include "../protobuf/GeneralMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CFightWayGrowingUp
{
public:
	CFightWayGrowingUp();
	virtual ~CFightWayGrowingUp();

	void set_id(int _value);
	int get_id();
	void set_fightway_id(int _value);
	int get_fightway_id();
	void set_level(int _value);
	int get_level();
	void set_required_experience(int _value);
	int get_required_experience();
	void set_grid1_property(int _value);
	int get_grid1_property();
	void set_grid1_value(float _value);
	float get_grid1_value();
	void set_grid2_property(int _value);
	int get_grid2_property();
	void set_grid2_value(float _value);
	float get_grid2_value();
	void set_grid3_property(int _value);
	int get_grid3_property();
	void set_grid3_value(float _value);
	float get_grid3_value();
	void set_grid4_property(int _value);
	int get_grid4_property();
	void set_grid4_value(float _value);
	float get_grid4_value();
	void set_grid5_property(int _value);
	int get_grid5_property();
	void set_grid5_value(float _value);
	float get_grid5_value();
	void set_complex_property(int _value);
	int get_complex_property();
	void set_complex_value(float _value);
	float get_complex_value();
	void set_required_gold(int _value);
	int get_required_gold();

private:
	int id ;
	int fightway_id;
	int level;
	int required_experience;
	int grid1_property;
	float grid1_value;
	int grid2_property;
	float grid2_value;
	int grid3_property;
	float grid3_value;
	int grid4_property;
	float grid4_value;
	int grid5_property;
	float grid5_value;
	int complex_property;
	float complex_value;
	int required_gold;
};

#endif
