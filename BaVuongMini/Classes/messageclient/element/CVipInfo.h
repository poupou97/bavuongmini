
#ifndef _MESSAGECLIENT_CVIPINFO_H_
#define _MESSAGECLIENT_CVIPINFO_H_

#include <string>

class CVipInfo
{
public:
	CVipInfo();
	~CVipInfo();

private:
	int vip_level;
	std::string type;
	int type_id;
	int add_type;
	long long add_number;

public:
	int get_vip_level();
	void set_vip_level(int _value);
	std::string get_type();
	void set_type(std::string _value);
	int get_type_id();
	void set_type_id(int _value);
	int get_add_type();
	void set_add_type(int _value);
	long long get_add_number();
	void set_add_number(long long _value);
};

#endif

