#ifndef _EQUIPREFINEFACTOR_
#define _EQUIPREFINEFACTOR_
#include <string>
#include <vector>
class MapRefineValueFactor
{
public:
	MapRefineValueFactor(void);
	~MapRefineValueFactor(void);

	void set_level(int _value);
	int get_level();

	void set_quality(int _value);
	int get_quality();

	void set_factor(float _value);
	float get_factor();
private:
	int m_level;
	int m_quality;
	float m_factor;
};

#endif