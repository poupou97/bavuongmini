#ifndef _MESSAGECLIENT_STAREQUIPMENTPICKAMOUNT_
#define _MESSAGECLIENT_STAREQUIPMENTPICKAMOUNT_

#include <string>
#include <vector>

class MapStarPickAmount
{
public:
	MapStarPickAmount(void);
	~MapStarPickAmount(void);

	void set_level(int _vale);
	int get_level();

	void set_count(int _amount);
	int get_count();

private:
	int m_level;
	int m_count;
};

#endif
