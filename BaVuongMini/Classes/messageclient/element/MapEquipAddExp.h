#ifndef _MESSAGECLIENT_STRENGTHEQUIPMENTADDEXP_
#define _MESSAGECLIENT_STRENGTHEQUIPMENTADDEXP_

#include <string>
#include <vector>

class MapEquipAddExp
{
public:
	MapEquipAddExp(void);
	~MapEquipAddExp(void);

	void set_level(int _vale);
	int get_level();

	void set_exp(int exp);
	int get_exp();
private:
	int m_level;
	int m_exp;
};

#endif
