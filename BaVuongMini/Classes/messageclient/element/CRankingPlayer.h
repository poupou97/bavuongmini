
#ifndef _MESSAGECLIENT_CRANKINGPLAYER_H_
#define _MESSAGECLIENT_CRANKINGPLAYER_H_

#include "../protobuf/CopyMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CRankingPlayer: public RankingPlayer
{
public:
    CRankingPlayer() ;
    virtual ~CRankingPlayer() ;

} ;

#endif
