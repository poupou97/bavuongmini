#ifndef _MESSAGECLIENT_CGENERALTEACJADDEDPROPERTY_H_
#define _MESSAGECLIENT_CGENERALTEACJADDEDPROPERTY_H_

class CGeneralTeachAddedProperty
{
public:
	CGeneralTeachAddedProperty();
	~CGeneralTeachAddedProperty();

	int get_id();
	void set_id(int _value);
	int get_quality();
	void set_quality(int _value);
	int get_star();
	void set_star(int _value);
	int get_profession();
	void set_profession(int _value);
	int get_hp_capacity();
	void set_hp_capacity(int _value);
	int get_max_attack();
	void set_max_attack(int _value);
	int get_max_magic_attack();
	void set_max_magic_attack(int _value);

private:
	int id;
	int quality;
	int star;
	int profession;
	int hp_capacity;
	int max_attack;
	int max_magic_attack;
};

#endif