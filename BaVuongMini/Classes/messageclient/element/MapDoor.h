#ifndef _MESSAGECLIENT_ELEMENT_MAPDOOR_H_
#define _MESSAGECLIENT_ELEMENT_MAPDOOR_H_

#include <string>

class MapDoor {
public:
	short x;
	short y;
	
	/** target map id */
	std::string targetMapId;
	/** target map name */
	std::string targetMapName;
};

#endif