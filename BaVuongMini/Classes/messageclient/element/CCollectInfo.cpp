#include "CCollectInfo.h"


CCollectInfo::CCollectInfo(void)
{
}


CCollectInfo::~CCollectInfo(void)
{
}

void CCollectInfo::set_templateId( int _value )
{
	templateId = _value;
}

int CCollectInfo::get_templateId()
{
	return templateId;
}

void CCollectInfo::set_name( std::string _value )
{
	name = _value;
}

std::string CCollectInfo::get_name()
{
	return name;
}

void CCollectInfo::set_icon( std::string _value )
{
	icon = _value;
}

std::string CCollectInfo::get_icon()
{
	return icon;
}

void CCollectInfo::set_avatar( std::string _value )
{
	avatar = _value;
}

std::string CCollectInfo::get_avatar()
{
	return avatar;
}

void CCollectInfo::set_des( std::string _value )
{
	des = _value;
}

std::string CCollectInfo::get_des()
{
	return des;
}

void CCollectInfo::set_activity_anm( std::string _value )
{
	activity_anm = _value;
}

std::string CCollectInfo::get_activity_anm()
{
	return activity_anm;
}

void CCollectInfo::set_invaild_anm( std::string _value )
{
	invaild_anm = _value;
}

std::string CCollectInfo::get_invaild_anm()
{
	return invaild_anm;
}

void CCollectInfo::set_activity_offSet_x( int _value )
{
	activity_offSet_x = _value;
}

int CCollectInfo::get_activity_offSet_x()
{
	return activity_offSet_x;
}

void CCollectInfo::set_activity_offSet_y( int _value )
{
	activity_offSet_y = _value;
}

int CCollectInfo::get_activity_offSet_y()
{
	return activity_offSet_y;
}

void CCollectInfo::set_invaild_offSet_x( int _value )
{
	invaild_offSet_x = _value;
}

int CCollectInfo::get_invaild_offSet_x()
{
	return invaild_offSet_x;
}

void CCollectInfo::set_invaild_offSet_y( int _value )
{
	invaild_offSet_y = _value;
}

int CCollectInfo::get_invaild_offSet_y()
{
	return invaild_offSet_y;
}
