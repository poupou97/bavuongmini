
#ifndef _MESSAGECLIENT_CREWARDUNIT_H_
#define _MESSAGECLIENT_CREWARDUNIT_H_

#include "../protobuf/PlayerMessage.pb.h"
#include "../GameProtobuf.h"
#include "../protobuf/MissionMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CMissionReward : public MissionReward
{
public:
	CMissionReward();
	~CMissionReward();
};
#endif;

