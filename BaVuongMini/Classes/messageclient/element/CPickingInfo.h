
#ifndef _MESSAGECLIENT_CPICKINGINFO_H_
#define _MESSAGECLIENT_CPICKINGINFO_H_

#include <string>

class CPickingInfo {
public:
	CPickingInfo() ;
    virtual ~CPickingInfo();

public:
	int		id;
	std::string name;
	std::string	icon;
	std::string	avatar;
	std::string	description;
	int		actionTime;
	int		actionCdTime;
	int		actionMaxNumber;
	bool	field;
	std::string	requirement;
};

#endif