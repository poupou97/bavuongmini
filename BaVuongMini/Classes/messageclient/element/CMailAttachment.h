

#ifndef _MESSAGECLIENT_CMAILATTACHMENT_H_
#define _MESSAGECLIENT_CMAILATTACHMENT_H_

#include "../../common/CKBaseClass.h"
#include "../protobuf/MailMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CMailAttachment:public MailAttachment
{
public:
	CMailAttachment(void);
	~CMailAttachment(void);
};

#endif;