
#ifndef _MESSAGECLIENT_CFIGHTSKILL_H_
#define _MESSAGECLIENT_CFIGHTSKILL_H_

#include "../../common/CKBaseClass.h"
#include "../protobuf/FightMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CFightSkill : public FightSkill
{
public:
	CFightSkill();
	virtual ~CFightSkill();

	void set_required_level(int _value);
	int get_required_level();
	void set_required_gold(int _value);
	int get_required_gold();
	void set_required_point(int _value);
	int get_required_point();
	void set_required_prop(std::string _value);
	std::string get_required_prop();
	void set_required_num(int _value);
	int get_required_num();
	void set_pre_skill(std::string _value);
	std::string get_pre_skill();
	void set_pre_skill_level(int _value);
	int get_pre_skill_level();

	void set_next_description(std::string _value);
	std::string get_next_description();
	void set_next_required_level(int _value);
	int get_next_required_level();
	void set_next_required_gold(int _value);
	int get_next_required_gold();
	void set_next_required_point(int _value);
	int get_next_required_point();
	void set_next_required_prop(std::string _value);
	std::string get_next_required_prop();
	void set_next_required_num(int _value);
	int get_next_required_num();
	void set_next_pre_skill(std::string _value);
	std::string get_next_pre_skill();
	void set_next_pre_skill_level(int _value);
	int get_next_pre_skill_level();

	void set_back_prop(std::string _value);
	std::string get_back_prop();
	void set_back_num(int _value);
	int get_back_num();

private:
	
	int required_level;
	int required_gold;
	int required_point;
	std::string required_prop;
	int required_num;
	std::string pre_skill;
	int  pre_skill_level;

	std::string next_description;
	int next_required_level;
	int next_required_gold;
	int next_required_point;
	std::string next_required_prop;
	int next_required_num;
	std::string next_pre_skill;
	int  next_pre_skill_level;

	std::string back_prop;
	int back_num;
};

#endif

