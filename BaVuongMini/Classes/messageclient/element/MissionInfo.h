
#ifndef _MESSAGECLIENT_MISSIONINFO_H_
#define _MESSAGECLIENT_MISSIONINFO_H_

#include "../protobuf/MissionMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class MissionInfo : public MissionInformation
{
public:
	MissionInfo() ;
	virtual ~MissionInfo() ;

public:
	//状态是否发生变化（默认为是）
	bool isStateChanged;
	//是否需要指引（默认为是）
	bool isNeedTeach;
} ;
#endif;