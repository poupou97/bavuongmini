
#ifndef _MESSAGECLIENT_CSHORTCUT_H_
#define _MESSAGECLIENT_CSHORTCUT_H_

#include "../../common/CKBaseClass.h"
#include "../protobuf/ModelMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CShortCut : public ShortCut
{
public:
	CShortCut();
	~CShortCut();

	int goodNum;
};
#endif;

