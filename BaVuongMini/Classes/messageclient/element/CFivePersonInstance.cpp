#include "CFivePersonInstance.h"


CFivePersonInstance::CFivePersonInstance()
{
}


CFivePersonInstance::~CFivePersonInstance()
{
}

void CFivePersonInstance::set_id( int _value )
{
	id = _value;
}

int CFivePersonInstance::get_id()
{
	return id;
}

void CFivePersonInstance::set_keyValue( std::string _value )
{
	keyValue = _value;
}

std::string CFivePersonInstance::get_keyValue()
{
	return keyValue;
}

void CFivePersonInstance::set_name( std::string _value )
{
	name = _value;
}

std::string CFivePersonInstance::get_name()
{
	return name;
}

void CFivePersonInstance::set_difficult( int _value )
{
	difficult = _value;
}

int CFivePersonInstance::get_difficult()
{
	return difficult;
}

void CFivePersonInstance::set_open_level( int _value )
{
	open_level = _value;
}

int CFivePersonInstance::get_open_level()
{
	return open_level;
}

void CFivePersonInstance::set_day_time( int _value )
{
	day_time = _value;
}

int CFivePersonInstance::get_day_time()
{
	return day_time;
}

void CFivePersonInstance::set_suggest_num( int _value )
{
	suggest_num = _value;
}

int CFivePersonInstance::get_suggest_num()
{
	return suggest_num;
}

void CFivePersonInstance::set_start_time( std::string _value )
{
	start_time = _value;
}

std::string CFivePersonInstance::get_start_time()
{
	return start_time;
}

void CFivePersonInstance::set_end_time( std::string _value )
{
	end_time = _value;
}

std::string CFivePersonInstance::get_end_time()
{
	return end_time;
}

void CFivePersonInstance::set_consume( int _value )
{
	consume = _value;
}

int CFivePersonInstance::get_consume()
{
	return consume;
}

void CFivePersonInstance::set_map_id( std::string _value )
{
	map_id = _value;
}

std::string CFivePersonInstance::get_map_id()
{
	return map_id;
}

void CFivePersonInstance::set_enter_x( int _value )
{
	enter_x = _value;
}

int CFivePersonInstance::get_enter_x()
{
	return enter_x;
}

void CFivePersonInstance::set_enter_y( int _value )
{
	enter_y = _value;
}

int CFivePersonInstance::get_enter_y()
{
	return enter_y;
}

void CFivePersonInstance::set_reward_xp( int _value )
{
	reward_xp = _value;
}

int CFivePersonInstance::get_reward_xp()
{
	return reward_xp;
}

void CFivePersonInstance::set_reward_gold( int _value )
{
	reward_gold = _value;
}

int CFivePersonInstance::get_reward_gold()
{
	return reward_gold;
}

void CFivePersonInstance::set_reward_gold_type( int _value )
{
	reward_gold_type = _value;
}

int CFivePersonInstance::get_reward_gold_type()
{
	return reward_gold_type;
}

void CFivePersonInstance::set_description( std::string _value )
{
	description = _value;
}

std::string CFivePersonInstance::get_description()
{
	return description;
}

void CFivePersonInstance::set_follow_map( std::string _value )
{
	follow_map = _value;
}

std::string CFivePersonInstance::get_follow_map()
{
	return follow_map;
}

void CFivePersonInstance::set_fight_point( int _value )
{
	fight_point = _value;
}

int CFivePersonInstance::get_fight_point()
{
	return fight_point;
}

void CFivePersonInstance::set_picture( std::string _value )
{
	picture = _value;
}

std::string CFivePersonInstance::get_picture()
{
	return picture;
}

void CFivePersonInstance::set_dragon_value( int _value )
{
	dragon_value = _value;
}

int CFivePersonInstance::get_dragon_value()
{
	return dragon_value;
}

void CFivePersonInstance::set_boss_count( int _value )
{
	boss_count = _value;
}

int CFivePersonInstance::get_boss_count()
{
	return boss_count;
}

void CFivePersonInstance::set_monster_count( int _value )
{
	monster_count = _value;
}

int CFivePersonInstance::get_monster_count()
{
	return monster_count;
}
