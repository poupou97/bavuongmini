#include "CFightSkill.h"


CFightSkill::CFightSkill()
{
}


CFightSkill::~CFightSkill()
{
}

void CFightSkill::set_required_level( int _value )
{
	required_level = _value;
}

int CFightSkill::get_required_level()
{
	return required_level;
}

void CFightSkill::set_required_gold( int _value )
{
	required_gold = _value;
}

int CFightSkill::get_required_gold()
{
	return required_gold;
}

void CFightSkill::set_required_point( int _value )
{
	required_point = _value;
}

int CFightSkill::get_required_point()
{
	return required_point;
}

void CFightSkill::set_required_prop( std::string _value )
{
	required_prop = _value;
}

std::string CFightSkill::get_required_prop()
{
	return required_prop;
}

void CFightSkill::set_required_num( int _value )
{
	required_num = _value;
}

int CFightSkill::get_required_num()
{
	return required_num;
}
void CFightSkill::set_pre_skill( std::string _value )
{
	pre_skill = _value;
}

std::string CFightSkill::get_pre_skill()
{
	return pre_skill;
}

void CFightSkill::set_pre_skill_level( int _value )
{
	pre_skill_level = _value;
}

int CFightSkill::get_pre_skill_level()
{
	return pre_skill_level;
}



void CFightSkill::set_next_description( std::string _value )
{
	next_description = _value;
}

std::string CFightSkill::get_next_description()
{
	return next_description;
}

void CFightSkill::set_next_required_level( int _value )
{
	next_required_level = _value;
}

int CFightSkill::get_next_required_level()
{
	return next_required_level;
}

void CFightSkill::set_next_required_gold( int _value )
{
	next_required_gold = _value;
}

int CFightSkill::get_next_required_gold()
{
	return next_required_gold;
}

void CFightSkill::set_next_required_point( int _value )
{
	next_required_point = _value;
}

int CFightSkill::get_next_required_point()
{
	return next_required_point;
}

void CFightSkill::set_next_required_prop( std::string _value )
{
	next_required_prop = _value;
}

std::string CFightSkill::get_next_required_prop()
{
	return next_required_prop;
}

void CFightSkill::set_next_required_num( int _value )
{
	next_required_num = _value;
}

int CFightSkill::get_next_required_num()
{
	return next_required_num;
}

void CFightSkill::set_next_pre_skill( std::string _value )
{
	next_pre_skill = _value;
}

std::string CFightSkill::get_next_pre_skill()
{
	return next_pre_skill;
}

void CFightSkill::set_next_pre_skill_level( int _value )
{
	next_pre_skill_level = _value;
}

int CFightSkill::get_next_pre_skill_level()
{
	return next_pre_skill_level;
}

void CFightSkill::set_back_prop( std::string _value )
{
	back_prop = _value;
}

std::string CFightSkill::get_back_prop()
{
	return back_prop;
}

void CFightSkill::set_back_num( int _value )
{
	back_num = _value;
}

int CFightSkill::get_back_num()
{
	return back_num;
}


