
#ifndef _MESSAGECLIENT_CEXTSTATUSINFO_H_
#define _MESSAGECLIENT_CEXTSTATUSINFO_H_

#include "../protobuf/FightMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CExtStatusInfo: public ExtStatusInfo
{
public:
    CExtStatusInfo() ;
    virtual ~CExtStatusInfo() ;

public:

} ;

#endif
