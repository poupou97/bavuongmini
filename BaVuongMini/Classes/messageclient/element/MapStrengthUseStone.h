#ifndef _MAPSTRENGTHSTONE_H_
#define _MAPSTRENGTHSTONE_H_
#include <string>
#include <vector>
class MapStrengthUseStone
{
public:
	MapStrengthUseStone(void);
	~MapStrengthUseStone(void);

	void set_type(int _value);
	int get_type();

	void set_level(int _value);
	int get_level();

	void set_stoneId(std::string _value);
	std::string get_stoneId();

	void set_stoneName(std::string _value);
	std::string get_stoneName();

	void set_limitLevel(std::string _value);
	std::string get_limitLevel();
private:
	int m_type;
	int m_level;
	std::string stoneId;
	std::string m_stoneName;
	std::string m_limitLevel;
};

#endif