
#include "CMapInfo.h"

#include "../../utils/StaticDataManager.h"

CMapInfo::CMapInfo() 
{
    
}

CMapInfo::~CMapInfo() 
{
    
}

const char* CMapInfo::getCountryName(int countryId)
{
	CCAssert(countryId >= country_1 && countryId <= country_max, "out of range");

	// ���ҵ������ַ���λ��font/strings.xml��
	// "country_1", "country_2", etc
	std::string countryIdName = "country_";
	char id[2];
	sprintf(id, "%d", countryId);
	countryIdName.append(id);

	return StringDataManager::getString(countryIdName.c_str());
}

const char* CMapInfo::getCountrySimpleName(int countryId)
{
	CCAssert(countryId >= country_1 && countryId <= country_max, "out of range");

	// ���ҵ������ַ���λ��font/strings.xml��
	// "country_simple_1", "country_simple_2", etc
	std::string countryIdName = "country_simple_";
	char id[2];
	sprintf(id, "%d", countryId);
	countryIdName.append(id);

	return StringDataManager::getString(countryIdName.c_str());
}

void CMapInfo::setLastMapId( std::string _value )
{
	m_sLastMapId = _value;
}

std::string CMapInfo::getLastMapId()
{
	return m_sLastMapId;
}

const char* CMapInfo::getCountryStr( int countryId )
{
	CCAssert(countryId >= country_none && countryId <= country_max, "out of range");

	// ���ҵ������ַ���λ��font/strings.xml��
	// "country_1", "country_2", etc
	std::string countryIdName = "country_";
	char id[2];
	sprintf(id, "%d", countryId);
	countryIdName.append(id);

	return StringDataManager::getString(countryIdName.c_str());
}
