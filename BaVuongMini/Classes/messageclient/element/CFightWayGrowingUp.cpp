#include "CFightWayGrowingUp.h"


CFightWayGrowingUp::CFightWayGrowingUp()
{
}


CFightWayGrowingUp::~CFightWayGrowingUp()
{
}

void CFightWayGrowingUp::set_id( int _value )
{
	id = _value;
}

int CFightWayGrowingUp::get_id()
{
	return id;
}

void CFightWayGrowingUp::set_fightway_id( int _value )
{
	fightway_id = _value;
}

int CFightWayGrowingUp::get_fightway_id()
{
	return fightway_id;
}

void CFightWayGrowingUp::set_level( int _value )
{
	level = _value;
}

int CFightWayGrowingUp::get_level()
{
	return level;
}

void CFightWayGrowingUp::set_required_experience( int _value )
{
	required_experience = _value;
}

int CFightWayGrowingUp::get_required_experience()
{
	return required_experience;
}

void CFightWayGrowingUp::set_grid1_property( int _value )
{
	grid1_property = _value;
}

int CFightWayGrowingUp::get_grid1_property()
{
	return grid1_property;
}

void CFightWayGrowingUp::set_grid1_value( float _value )
{
	grid1_value = _value;
}

float CFightWayGrowingUp::get_grid1_value()
{
	return grid1_value;
}

void CFightWayGrowingUp::set_grid2_property( int _value )
{
	grid2_property = _value;
}

int CFightWayGrowingUp::get_grid2_property()
{
	return grid2_property;
}

void CFightWayGrowingUp::set_grid2_value( float _value )
{
	grid2_value = _value;
}

float CFightWayGrowingUp::get_grid2_value()
{
	return grid2_value;
}

void CFightWayGrowingUp::set_grid3_property( int _value )
{
	grid3_property = _value;
}

int CFightWayGrowingUp::get_grid3_property()
{
	return grid3_property;
}

void CFightWayGrowingUp::set_grid3_value( float _value )
{
	grid3_value = _value;
}

float CFightWayGrowingUp::get_grid3_value()
{
	return grid3_value;
}

void CFightWayGrowingUp::set_grid4_property( int _value )
{
	grid4_property = _value;
}

int CFightWayGrowingUp::get_grid4_property()
{
	return grid4_property;
}

void CFightWayGrowingUp::set_grid4_value( float _value )
{
	grid4_value = _value;
}

float CFightWayGrowingUp::get_grid4_value()
{
	return grid4_value;
}

void CFightWayGrowingUp::set_grid5_property( int _value )
{
	grid5_property = _value;
}

int CFightWayGrowingUp::get_grid5_property()
{
	return grid5_property;
}

void CFightWayGrowingUp::set_grid5_value( float _value )
{
	grid5_value = _value;
}

float CFightWayGrowingUp::get_grid5_value()
{
	return grid5_value;
}

void CFightWayGrowingUp::set_complex_property( int _value )
{
	complex_property = _value;
}

int CFightWayGrowingUp::get_complex_property()
{
	return complex_property;
}

void CFightWayGrowingUp::set_complex_value( float _value )
{
	complex_value = _value;
}

float CFightWayGrowingUp::get_complex_value()
{
	return complex_value;
}

void CFightWayGrowingUp::set_required_gold( int _value )
{
	required_gold = _value;
}

int CFightWayGrowingUp::get_required_gold()
{
	return required_gold;
}
