#ifndef _MESSAGECLIENT_CPOWERSENDINFO_H_
#define _MESSAGECLIENT_CPOWERSENDINFO_H_

#include "../GameProtobuf.h"
#include "../protobuf/RecruitMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CPowerSendInfo
{
public:
	CPowerSendInfo() ;
	virtual ~CPowerSendInfo() ;

private:
	int	m_nId;
	std::string m_strSendTime;
	int	m_nPowerValue;

public:
	void set_id(int nId);
	int get_id();

	void set_sendTime(std::string strSendTime);
	std::string get_sendTime();

	void set_powerValue(int nPowerValue);
	int get_powerValue();
} ;

#endif
