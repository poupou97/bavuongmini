#ifndef _MESSAGECLIENT_ELEMENT_CDRUG_H_
#define _MESSAGECLIENT_ELEMENT_CDRUG_H_

#include <string>
#include <map>

class CDrug
{
public:
	CDrug();
	~CDrug();

	/** check if the prop is drug */
	static bool isDrug(std::string propId);

	/**
	   * ������ȴʱ�����
	   */
	void updateCD();
	void startCD();
	bool isInCD();

	void set_id(int _value);
	int get_id();
	void set_prop_id(std::string _value);
	std::string get_prop_id();
	void set_hp(int _value);
	int get_hp();
	void set_mp(int _value);
	int get_mp();
	void set_once_hp(int _value);
	int get_once_hp();
	void set_once_mp(int _value);
	int get_once_mp();
	void set_once_hp_upper(int _value);
	int get_once_hp_upper();
	void set_once_mp_upper(int _value);
	int get_once_mp_upper();
	void set_type_flag(int _value);
	int get_type_flag();
	void set_cool_time(int _value);
	int get_cool_time();

	long long getRemainTime();

private:
	int id;
	std::string prop_id;
	int hp;
	int mp;
	int once_hp;
	int once_mp;
	int once_hp_upper;
	int once_mp_upper;
	int type_flag;
	int cool_time;   // ��ȴ��ʱ��

	bool m_bIsInCD;
	long long coolTimeCount;   // ��ȴ���ʱ��ϵͳʱ��(ms)
	long long remainTime;
};

////////////////////////////////////////////////////////

/**
  * �������ҩƷ�����
  * Ŀǰ��Ҫ����ĳ��ҩƷ��CDˢ��
  */
class DrugManager
{
public:
	DrugManager();
	~DrugManager();

	static DrugManager* getInstance();

	void update(float dt);

	CDrug* getDrugById(std::string drugId);

private:
	std::map<std::string, CDrug*> m_DrugMap;   // ����ҩƷ���͵��б�
};

#endif

