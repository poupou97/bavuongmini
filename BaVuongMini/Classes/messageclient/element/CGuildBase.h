
#ifndef _GUIILDMESSAGE_GUILDBASE_H
#define _GUIILDMESSAGE_GUILDBASE_H

#include "../GameProtobuf.h"
#include "../protobuf/GuildMessage.pb.h"

#include "cocos2d.h"
USING_NS_THREEKINGDOMS_PROTOCOL;
class CGuildBase:public GuildBase
{
public:
	enum {
		position_none = 0,
		position_master = 1,
		position_second_master = 2,
		position_member = 3
	};

public:
	CGuildBase();
	~CGuildBase();
};
#endif
