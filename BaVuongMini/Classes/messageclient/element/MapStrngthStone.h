#ifndef _MESSAGECLIENT_STRENGTHSTONE_
#define _MESSAGECLIENT_STRENGTHSTONE_

#include <string>
#include <vector>

class MapStrngthStone
{
public:
	MapStrngthStone(void);
	~MapStrngthStone(void);

	void set_id(std::string _vale);
	std::string get_id();

	void set_exp(int exp);
	int get_exp();

private:
	std::string m_propId;
	int m_exp;
};

#endif
