#include "CFivePersonInstanceEndInfo.h"
#include "CRewardProp.h"


CFivePersonInstanceEndInfo::CFivePersonInstanceEndInfo()
{
}


CFivePersonInstanceEndInfo::~CFivePersonInstanceEndInfo()
{
	std::vector<CRewardProp*>::iterator iter;
	for (iter = rewardProps.begin(); iter != rewardProps.end(); ++iter)
	{
		delete *iter;
	}
	rewardProps.clear();
}

void CFivePersonInstanceEndInfo::set_instanceId( int _value )
{
	instanceId = _value;
}

int CFivePersonInstanceEndInfo::get_instanceId()
{
	return instanceId;
}

void CFivePersonInstanceEndInfo::set_star( int _value )
{
	star = _value;
}

int CFivePersonInstanceEndInfo::get_star()
{
	return star;
}

void CFivePersonInstanceEndInfo::set_exp( int _value )
{
	exp = _value;
}

int CFivePersonInstanceEndInfo::get_exp()
{
	return exp;
}

void CFivePersonInstanceEndInfo::set_goldType( int _value )
{
	goldType = _value;
}

int CFivePersonInstanceEndInfo::get_goldType()
{
	return goldType;
}

void CFivePersonInstanceEndInfo::set_gold( int _value )
{
	gold = _value;
}

int CFivePersonInstanceEndInfo::get_gold()
{
	return gold;
}

void CFivePersonInstanceEndInfo::set_baseExp( int _value )
{
	baseExp = _value;
}

int CFivePersonInstanceEndInfo::get_baseExp()
{
	return baseExp;
}

void CFivePersonInstanceEndInfo::set_baseGold( int _value )
{
	baseGold = _value;
}

int CFivePersonInstanceEndInfo::get_baseGold()
{
	return baseGold;
}

void CFivePersonInstanceEndInfo::set_timeAdd( int _value )
{
	timeAdd = _value;
}

int CFivePersonInstanceEndInfo::get_timeAdd()
{
	return timeAdd;
}

void CFivePersonInstanceEndInfo::set_skillAdd( int _value )
{
	skillAdd = _value;
}

int CFivePersonInstanceEndInfo::get_skillAdd()
{
	return skillAdd;
}

void CFivePersonInstanceEndInfo::set_vipAdd( int _value )
{
	vipAdd = _value;
}

int CFivePersonInstanceEndInfo::get_vipAdd()
{
	return vipAdd;
}

void CFivePersonInstanceEndInfo::set_dragonValue( int _value )
{
	dragonValue = _value;
}

int CFivePersonInstanceEndInfo::get_dragonValue()
{
	return dragonValue;
}

void CFivePersonInstanceEndInfo::set_timeDouble( int _value )
{
	timeDouble = _value;
}

int CFivePersonInstanceEndInfo::get_timeDouble()
{
	return timeDouble;
}

void CFivePersonInstanceEndInfo::copyFrom( CFivePersonInstanceEndInfo* info)
{
	this->set_instanceId(info->get_instanceId());
	this->set_star(info->get_star());
	this->set_exp(info->get_exp());
	this->set_goldType(info->get_goldType());
	this->set_gold(info->get_gold());
	this->set_baseExp(info->get_baseExp());
	this->set_baseGold(info->get_baseGold());
	this->set_timeAdd(info->get_timeAdd());
	this->set_skillAdd(info->get_skillAdd());
	this->set_vipAdd(info->get_vipAdd());
	this->set_dragonValue(info->get_dragonValue());
	this->set_timeDouble(info->get_timeDouble());

	if (rewardProps.size()>0)
	{
		std::vector<CRewardProp*>::iterator iter;
		for (iter = rewardProps.begin(); iter != rewardProps.end(); ++iter)
		{
			delete *iter;
		}
		rewardProps.clear();
	}

	for(int i= 0;i<info->rewardProps.size();++i)
	{
		CRewardProp * rewardProp = new CRewardProp();
		rewardProp->CopyFrom(*info->rewardProps.at(i));
		this->rewardProps.push_back(rewardProp);
	}
}


