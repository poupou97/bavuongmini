
#ifndef _MESSAGECLIENT_CTRANKINGPRIZE_H_
#define _MESSAGECLIENT_CTRANKINGPRIZE_H_

#include "../protobuf/CopyMessage.pb.h"
#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CTRankingPrize: public TRankingPrize
{
public:
    CTRankingPrize() ;
    virtual ~CTRankingPrize() ;
} ;

#endif
