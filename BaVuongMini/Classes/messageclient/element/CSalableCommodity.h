
#ifndef _MESSAGECLIENT_CSALABLECOMMODITY_H_
#define _MESSAGECLIENT_CSALABLECOMMODITY_H_

#include "../../common/CKBaseClass.h"
#include "../GameProtobuf.h"
#include "../protobuf/NPCMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CSalableCommodity : public SalableCommodity
{
public:
	CSalableCommodity();
	~CSalableCommodity();

	int goodNum;
};
#endif;

