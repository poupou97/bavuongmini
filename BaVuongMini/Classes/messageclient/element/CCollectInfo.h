#ifndef _MESSAGECLIENT_ELEMENT_CCOLLECTINFO_H_
#define _MESSAGECLIENT_ELEMENT_CCOLLECTINFO_H_

#include <string>
#include <map>

class CCollectInfo
{
public:
	CCollectInfo(void);
	~CCollectInfo(void);

private:
	int templateId;
	std::string name;
	std::string icon;
	std::string avatar;
	std::string des;
	std::string activity_anm;
	int activity_offSet_x;
	int activity_offSet_y;
	std::string invaild_anm;
	int invaild_offSet_x;
	int invaild_offSet_y;

public:
	void set_templateId(int _value);
	int get_templateId();
	void set_name(std::string _value);
	std::string get_name();
	void set_icon(std::string _value);
	std::string get_icon();
	void set_avatar(std::string _value);
	std::string get_avatar();
	void set_des(std::string _value);
	std::string get_des();
	void set_activity_anm(std::string _value);
	std::string get_activity_anm();
	void set_activity_offSet_x(int _value);
	int get_activity_offSet_x();
	void set_activity_offSet_y(int _value);
	int get_activity_offSet_y();
	void set_invaild_anm(std::string _value);
	std::string get_invaild_anm();
	void set_invaild_offSet_x(int _value);
	int get_invaild_offSet_x();
	void set_invaild_offSet_y(int _value);
	int get_invaild_offSet_y();
};

#endif

