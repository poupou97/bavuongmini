#include "MapStrengthUseStone.h"


MapStrengthUseStone::MapStrengthUseStone(void)
{
}


MapStrengthUseStone::~MapStrengthUseStone(void)
{
}

void MapStrengthUseStone::set_type( int _value )
{
	m_type = _value;
}

int MapStrengthUseStone::get_type()
{
	return m_type;
}

void MapStrengthUseStone::set_level( int _value )
{
	m_level = _value;
}

int MapStrengthUseStone::get_level()
{
	return m_level;
}

void MapStrengthUseStone::set_stoneId( std::string _value )
{
	stoneId = _value;
}

std::string MapStrengthUseStone::get_stoneId()
{
	return stoneId;
}

void MapStrengthUseStone::set_stoneName( std::string _value )
{
	m_stoneName = _value;
}

std::string MapStrengthUseStone::get_stoneName()
{
	return m_stoneName;
}

void MapStrengthUseStone::set_limitLevel( std::string _value )
{
	m_limitLevel = _value;
}

std::string MapStrengthUseStone::get_limitLevel()
{
	return m_limitLevel;
}


