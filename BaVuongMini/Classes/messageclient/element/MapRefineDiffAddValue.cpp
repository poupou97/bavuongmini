#include "MapRefineDiffAddValue.h"


MapRefineDiffAddValue::MapRefineDiffAddValue(void)
{
}


MapRefineDiffAddValue::~MapRefineDiffAddValue(void)
{
}
void MapRefineDiffAddValue::set_level( int _vale )
{
	m_level = _vale;
}

int MapRefineDiffAddValue::get_level()
{
	return m_level;
}

void MapRefineDiffAddValue::set_exp( int exp )
{
	m_exp = exp;
}

int MapRefineDiffAddValue::get_exp()
{
	return m_exp;
}

void MapRefineDiffAddValue::set_minAttack( int _value )
{
	m_minattack = _value;
}

int MapRefineDiffAddValue::get_minAttack()
{
	return m_minattack;
}

void MapRefineDiffAddValue::set_maxAttack( int _value )
{
	m_maxattack = _value;
}

int MapRefineDiffAddValue::get_maxAttack()
{
	return m_maxattack;
}

void MapRefineDiffAddValue::set_minmagic_Attack( int _value )
{
	m_minmagicattack = _value;
}

int MapRefineDiffAddValue::get_minmagicAttack()
{
	return m_minmagicattack;
}

void MapRefineDiffAddValue::set_maxmagic_Attack( int _value )
{
	m_maxmagicattack = _value;
}

int MapRefineDiffAddValue::get_maxmagicAttack()
{
	return m_maxmagicattack;
}

void MapRefineDiffAddValue::set_min_define( int _value )
{
	m_mindefine = _value;
}

int MapRefineDiffAddValue::get_min_define()
{
	return m_mindefine;
}

void MapRefineDiffAddValue::set_max_define( int _value )
{
	m_maxdefine = _value;
}

int MapRefineDiffAddValue::get_max_define()
{
	return m_maxdefine;
}

void MapRefineDiffAddValue::set_min_magic_define( int _value )
{
	m_minmagicdefine = _value;
}

int MapRefineDiffAddValue::get_min_magic_define()
{
	return m_minmagicdefine;
}

void MapRefineDiffAddValue::set_max_magic_define( int _value )
{
	m_maxmagicdefine = _value;
}

int MapRefineDiffAddValue::get_max_magic_define()
{
	return m_maxmagicdefine;
}

void MapRefineDiffAddValue::set_hp( int _value )
{
	m_hp = _value;
}

int MapRefineDiffAddValue::get_hp()
{
	return m_hp;
}

void MapRefineDiffAddValue::set_mp( int _value )
{
	m_mp = _value;
}

int MapRefineDiffAddValue::get_mp()
{
	return m_mp;
}

void MapRefineDiffAddValue::set_clazz( int _value )
{
	m_clazz = _value;
}

int MapRefineDiffAddValue::get_clazz()
{
	return m_clazz;
}

