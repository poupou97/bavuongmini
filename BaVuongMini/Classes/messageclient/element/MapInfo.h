#ifndef _MESSAGECLIENT_ELEMENT_MAPINFO_H_
#define _MESSAGECLIENT_ELEMENT_MAPINFO_H_

#include "DoorInfo.h"
#include <string>
#include <vector>

class MapInfo {
public:
	/** id singleton*/
	std::string mapId;
	/** name */
	std::string name;
	
	std::vector<DoorInfo*> doors;
};

#endif