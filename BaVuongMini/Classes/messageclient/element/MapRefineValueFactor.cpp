#include "MapRefineValueFactor.h"


MapRefineValueFactor::MapRefineValueFactor(void)
{
}


MapRefineValueFactor::~MapRefineValueFactor(void)
{
}

void MapRefineValueFactor::set_level( int _value )
{
	m_level = _value;
}

int MapRefineValueFactor::get_level()
{
	return m_level;
}

void MapRefineValueFactor::set_quality( int _value )
{
	m_quality = _value;
}

int MapRefineValueFactor::get_quality()
{
	return m_quality;
}

void MapRefineValueFactor::set_factor( float _value )
{
	m_factor = _value;
}

float MapRefineValueFactor::get_factor()
{
	return m_factor;
}
