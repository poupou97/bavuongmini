#ifndef _MESSAGECLIENT_CPARTNER_H_
#define _MESSAGECLIENT_CPARTNER_H_

#include "../GameProtobuf.h"
#include "../protobuf/OfflineFight.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class CPartner : public Partner
{
public:
	CPartner();
	~CPartner();
};

#endif
