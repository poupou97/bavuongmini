#ifndef _MESSAGECLIENT_MARQUEESTRINGCONFIG_
#define _MESSAGECLIENT_MARQUEESTRINGCONFIG_

#include <string>
#include <vector>

class MapMarquee
{
public:
	MapMarquee(void);
	~MapMarquee(void);

	void set_id(int id_);
	int get_id();

	void set_message(std::string string_);
	std::string get_message();

	void set_prioity(int value_);
	int get_prioity();
private:
	std::string m_strings;
	int m_id;
	int m_prioity;
};

#endif
