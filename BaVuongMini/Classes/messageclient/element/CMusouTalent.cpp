#include "CMusouTalent.h"


CMusouTalent::CMusouTalent()
{
}


CMusouTalent::~CMusouTalent()
{
}

void CMusouTalent::copyFrom( CMusouTalent * temp )
{
	this->id = temp->id;
	this->clazz = temp->clazz;
	this->idx = temp->idx;
	this->level = temp->level;
	this->name = temp->name;
	this->description = temp->description;
	this->require_level = temp->require_level;
	this->require_essence = temp->require_essence;
	this->icon = temp->icon;
}


void CMusouTalent::set_id( int _value )
{
	id = _value;
}

int CMusouTalent::get_id()
{
	return id;
}

void CMusouTalent::set_clazz( int _value )
{
	clazz = _value;
}

int CMusouTalent::get_clazz()
{
	return clazz;
}

void CMusouTalent::set_idx( int _value )
{
	idx = _value;
}

int CMusouTalent::get_idx()
{
	return idx;
}

void CMusouTalent::set_level( int _value )
{
	level = _value;
}

int CMusouTalent::get_level()
{
	return level;
}

void CMusouTalent::set_name( std::string _value )
{
	name = _value;
}

std::string CMusouTalent::get_name()
{
	return name;
}

void CMusouTalent::set_description( std::string _value )
{
	description = _value;
}

std::string CMusouTalent::get_description()
{
	return description;
}

void CMusouTalent::set_require_level( int _value )
{
	require_level = _value;
}

int CMusouTalent::get_require_level()
{
	return require_level;
}

void CMusouTalent::set_require_essence( int _value )
{
	require_essence = _value;
}

int CMusouTalent::get_require_essence()
{
	return require_essence;
}

void CMusouTalent::set_icon( std::string _value )
{
	icon = _value;
}

std::string CMusouTalent::get_icon()
{
	return icon;
}

