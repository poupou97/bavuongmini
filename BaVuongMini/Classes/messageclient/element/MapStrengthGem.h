#ifndef _MESSAGECLIENT_STRENGTHGEM_
#define _MESSAGECLIENT_STRENGTHGEM_

#include <string>
#include <vector>

class MapStrengthGem
{
public:
	MapStrengthGem(void);
	~MapStrengthGem(void);

	void set_id(std::string _vale);
	std::string get_id();

	void set_gemLevel(int _level);
	int get_gemLevel();

private:
	std::string m_propId;
	int m_level;
};

#endif
