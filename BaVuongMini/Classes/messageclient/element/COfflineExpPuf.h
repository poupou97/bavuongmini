#ifndef _MESSAGECLIENT_CONELINEEXPPUF_H_
#define _MESSAGECLIENT_CONELINEEXPPUF_H_

#include "../GameProtobuf.h"
#include "../protobuf/QuestionMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class COfflineExpPuf:public OfflineExpPuf
{
public:
	COfflineExpPuf();
	~COfflineExpPuf();

	bool m_bIsGeted;
};
#endif;
