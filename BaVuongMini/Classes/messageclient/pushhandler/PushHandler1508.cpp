
#include "PushHandler1508.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/CGuildBase.h"
#include "../element/CGuildMemberBase.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/OtherPlayer.h"



IMPLEMENT_CLASS(PushHandler1508)

PushHandler1508::PushHandler1508() 
{
    
}
PushHandler1508::~PushHandler1508() 
{
    
}
void* PushHandler1508::createInstance()
{
    return new PushHandler1508() ;
}
void PushHandler1508::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1508::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1508::handle(CommonMessage* mb)
{
	Rsp1508 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	std::string roleName_ = bean.name();
	long long roleId_ = bean.roleid();
	//bean.level();
	int curPost_ = bean.profession();
	MainScene * mianscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	FamilyUI * familyui_ =(FamilyUI *)mianscene_->getChildByTag(kTagFamilyUI);

	BasePlayer* pPlayer = dynamic_cast<BasePlayer*>(GameView::getInstance()->getGameScene()->getActor(roleId_));
	if(pPlayer != NULL)
	{
		if (bean.type() ==1)   // remove the player from the guild
		{
			pPlayer->getActiveRole()->mutable_playerbaseinfo()->set_factionname("");
			pPlayer->getActiveRole()->mutable_playerbaseinfo()->set_factionid(0);
			pPlayer->getActiveRole()->mutable_playerbaseinfo()->set_factionposition(CGuildBase::position_none);
			pPlayer->addFamilyName("", CGuildBase::position_none);

			if (roleId_ == GameView::getInstance()->myplayer->getRoleId() )
			{
				const char *str_ = StringDataManager::getString("family_nibeitichujiazu");
				GameView::getInstance()->showAlertDialog(str_);

				if (familyui_ != NULL)
				{
					familyui_->closeAnim();
				}
			}
		}
		else if(bean.type() == 2)   // upgrade to second master
		{
			pPlayer->getActiveRole()->mutable_playerbaseinfo()->set_factionposition(CGuildBase::position_second_master);
			pPlayer->addFamilyName(pPlayer->getActiveRole()->playerbaseinfo().factionname().c_str(),
				pPlayer->getActiveRole()->playerbaseinfo().factionposition());
		}
		else if(bean.type() == 3)   // downgrade to member
		{
			pPlayer->getActiveRole()->mutable_playerbaseinfo()->set_factionposition(CGuildBase::position_member);
			pPlayer->addFamilyName(pPlayer->getActiveRole()->playerbaseinfo().factionname().c_str(),
				pPlayer->getActiveRole()->playerbaseinfo().factionposition());
		}
	}

	if (familyui_== NULL)
	{
		return;
	}
	
	int num_ = familyui_->vectorGuildMemberBase.size();
	int operationType_ = bean.type();
	switch(operationType_)
	{
	case 1:
		{			
			for (int i=0;i<num_;i++)
			{
				if (familyui_->vectorGuildMemberBase.at(i)->id() == roleId_)
				{
					std::vector<CGuildMemberBase *>::iterator iter= familyui_->vectorGuildMemberBase.begin() + i;
					CGuildMemberBase * memberBaseV = *iter;
					familyui_->vectorGuildMemberBase.erase(iter);
					delete memberBaseV;
					break;
				}
			}
			const char *str_ = StringDataManager::getString("family_exit");
			std::string strings_ =roleName_.append(str_);
			GameView::getInstance()->showAlertDialog(strings_);
			familyui_->tableviewMember->reloadData();

			familyui_->curFamilyMemberNum--;
			//familyui_->maxFamilyMemberNum = bean.guildbase().maxmembernumber();

			char curMemberStr[5];
			sprintf(curMemberStr,"%d",familyui_->curFamilyMemberNum);
			char maxMemberStr[5];
			sprintf(maxMemberStr,"%d",familyui_->maxFamilyMemberNum);
			std::string member_ =curMemberStr;
			member_.append("/");
			member_.append(maxMemberStr);
			familyui_->label_familyNumber->setString(member_.c_str());

			/*
			if (GameView::getInstance()->getGameScene()->getActor(roleId_))
			{
				BasePlayer* otherPlayer = dynamic_cast<BasePlayer*>(GameView::getInstance()->getGameScene()->getActor(roleId_));
				otherPlayer->addFamilyName("", CGuildBase::position_none);
			}
			*/
		}break;
	case 2:
		{
			for (int i=0;i<num_;i++)
			{
				if (familyui_->vectorGuildMemberBase.at(i)->id() == roleId_)
				{
					familyui_->vectorGuildMemberBase.at(i)->set_post(2);
				}
			}
			familyui_->tableviewMember->reloadData();

			const char *str_ = StringDataManager::getString("family_renmingweizhanglao");
			std::string strings_ =roleName_.append(str_);
			GameView::getInstance()->showAlertDialog(strings_);

			if (roleId_ == familyui_->selfId_)
			{
				std::string strings_ = StringDataManager::getString("family_member_SecondMaster");
				familyui_->label_myPost->setString(strings_.c_str());

				familyui_->btn_repairNotice->setTouchEnabled(true);
				familyui_->btn_repairManifesto->setTouchEnabled(true);
				familyui_->btn_mamage->setVisible(true);
			}
		
		}break;
	case 3:
		{
			for (int i=0;i<num_;i++)
			{
				if (familyui_->vectorGuildMemberBase.at(i)->id() == roleId_)
				{
					familyui_->vectorGuildMemberBase.at(i)->set_post(3);
				}
			}
			const char *str_ = StringDataManager::getString("family_beijiechuzhiwei");
			std::string strings_ =roleName_.append(str_);
			GameView::getInstance()->showAlertDialog(strings_);
			familyui_->tableviewMember->reloadData();

			if (roleId_ == familyui_->selfId_)
			{
				std::string strings_ = StringDataManager::getString("family_member_member");
				familyui_->label_myPost->setString(strings_.c_str());
				familyui_->selfPost_ = 3;

				familyui_->btn_repairNotice->setTouchEnabled(false);
				familyui_->btn_repairManifesto->setTouchEnabled(false);
				familyui_->btn_mamage->setVisible(false);
			}
		}break;
	case 4:
		{
			familyui_->tableviewMember->reloadData();
			/*
			const char *str_ ="" ;//StringDataManager::getString("family_exit");
			std::string strings_ =roleName_.append(str_);
			GameView::getInstance()->showAlertDialog(strings_);
			*/
		}break;
	}
}
