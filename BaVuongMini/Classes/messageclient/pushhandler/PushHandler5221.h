#ifndef Blog_C___Reflection_PushHandler5221_h
#define Blog_C___Reflection_PushHandler5221_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5221 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5221)

public:
	SYNTHESIZE(PushHandler5221, int*, m_pValue)

	PushHandler5221() ;
	virtual ~PushHandler5221() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
