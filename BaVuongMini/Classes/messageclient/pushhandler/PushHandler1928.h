
#ifndef Blog_C___Reflection_PushHandler1928_h
#define Blog_C___Reflection_PushHandler1928_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
USING_NS_CC;

/**
 *  副本通关星级
 */
class PushHandler1928 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1928)
    
public:
    SYNTHESIZE(PushHandler1928, int*, m_pValue)
    
    PushHandler1928() ;
    virtual ~PushHandler1928() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
