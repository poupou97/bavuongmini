#include "PushHandler5112.h"
#include "../protobuf/DailyGiftSign.pb.h"
#include "../element/COneSignEverydayGift.h"
#include "../../ui/SignDaily_ui/SignDailyData.h"
#include "../../GameView.h"
#include "../../ui/SignDaily_ui/SignDailyIcon.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/SignDaily_ui/SignDailyUI.h"
#include "../../ui/Rank_ui/RankData.h"
#include "../../ui/GameUIConstant.h"
#include "../../ui/Reward_ui/RewardUi.h"
#include "../../messageclient/element/CRewardBase.h"

IMPLEMENT_CLASS(PushHandler5112)

PushHandler5112::PushHandler5112() 
{

}
PushHandler5112::~PushHandler5112() 
{
	
}
void* PushHandler5112::createInstance()
{
	return new PushHandler5112() ;
}
void PushHandler5112::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5112::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5112::handle(CommonMessage* mb)
{
	Push5112 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// �����ݣ�����������е���ݣ�
	std::vector<COneSignEverydayGift *>::iterator iter;
	for (iter = SignDailyData::instance()->m_vector_internt_sign.begin(); iter != SignDailyData::instance()->m_vector_internt_sign.end(); iter++)
	{
		delete *iter;
	}
	SignDailyData::instance()->m_vector_internt_sign.clear();


	// ��ʼ�� ������������
	int size = bean.auctioninfos_size();
	for (int i = 0; i<size; i++)
	{
		COneSignEverydayGift * oneSignDailyGift =new COneSignEverydayGift();
		oneSignDailyGift->CopyFrom(bean.auctioninfos(i));

		SignDailyData::instance()->m_vector_internt_sign.push_back(oneSignDailyGift);
	}

	/** ��ǵ�½���24ǵ���� 0����ͣ�1��½*/
	int nIsLogn = bean.islogin();
	SignDailyData::instance()->setIsLogin(nIsLogn);

	/*  ������������͵ ķ�����ˢ��ʱ�(����а-����������ʱ�)*/
	long long longTimeRefresh = bean.toptime();
	RankData::instance()->set_timeRefreshFlag(longTimeRefresh);

	if (1 == nIsLogn)
	{
		// ��¼��½ʱ�û��ȼ�
		SignDailyData::instance()->setLoginPlayerLevel(GameView::getInstance()->myplayer->getActiveRole()->level());
	}

	// �� internt �� ݸ��µ� native��
	SignDailyData::instance()->initDataFromIntent();

	// ��жMainScene��Ƿ���
	if (NULL != GameView::getInstance()->getGameScene())
	{
		// ��ж iconϴ����Ƿ���ڣ��������� ������ݣ����򣬴������
		SignDailyIcon* pSignDailyIcon = (SignDailyIcon *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagSignDailyIcon);
		if (NULL != pSignDailyIcon)
		{
			pSignDailyIcon->initDataFromIntent();
		}

		// ��� ��콱 ��״̬(ǩ��)
		if (SignDailyData::instance()->isCanSign())
		{
			for (int i = 0;i < GameView::getInstance()->rewardvector.size();i++)
			{
				if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_SIGNDAILY)
				{
					GameView::getInstance()->rewardvector.at(i)->setRewardState(1);
					RewardUi::setRewardListParticle();
				}
			}
		}

		// �ж UIϴ����Ƿ���:ڴ������� ������ݣ�
		SignDailyUI* pSignDailyUI = (SignDailyUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagSignDailyUI);
		if (NULL != pSignDailyUI)
		{
			pSignDailyUI->refreshUI();
		}
		//else if (NULL == pSignDailyUI && 1 == nIsLogn && GameView::getInstance()->myplayer->getActiveRole()->level() >= SIGN_DAILYGIFT_LEVEL)					// ��һ�ε�½
		//{
		//	Size winSize = Director::getInstance()->getVisibleSize();

		//	SignDailyUI * pSignDailyUI = SignDailyUI::create();
		//	pSignDailyUI->setIgnoreAnchorPointForPosition(false);
		//	pSignDailyUI->setAnchorPoint(Vec2(0.5f, 0.5f));
		//	pSignDailyUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		//	pSignDailyUI->setTag(kTagSignDailyUI);
		//	
		//	GameView::getInstance()->getMainUIScene()->addChild(pSignDailyUI);

		//	pSignDailyUI->refreshUI();
		//}
	}
}