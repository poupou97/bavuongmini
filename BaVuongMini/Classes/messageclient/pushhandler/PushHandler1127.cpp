
#include "PushHandler1127.h"
#include "../../GameView.h"
#include "../GameMessageProcessor.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/extensions/PopupPanel.h"


//using namespace threekingdoms::protocol;

IMPLEMENT_CLASS(PushHandler1127)

PushHandler1127::PushHandler1127() 
{
    
}
PushHandler1127::~PushHandler1127() 
{
    std::vector<PopupPanel::PopuBoardOption *>::iterator iter;
	for (iter= OptionVector.begin();iter!= OptionVector.end();iter++ )
	{
		delete *iter;
	}
	OptionVector.clear();

}
void* PushHandler1127::createInstance()
{
    return new PushHandler1127() ;
}
void PushHandler1127::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1127::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1127::handle(CommonMessage* mb)
{
	Push1127 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	
	int type_ =bean.type();//

	std::string resultStr = bean.text();
	int align_ =bean.align();
	int times_ =bean.time();

	int default_ =bean.defaultid();

	int inputType_ =bean.inputtype();
	int maxSize =bean.maxsize();
	int hasOptions_ =bean.hasoption();
	
	if(bean.type() == 4)
	{
		GameView::getInstance()->showAlertDialog(resultStr.c_str());
		return;
	}

	int option_id= bean.options_size();
	for (int i=0;i<option_id;i++)
	{
		PopupPanel::PopuBoardOption *optionStruct = new PopupPanel::PopuBoardOption();
		optionStruct->clientFunction_ = bean.options(i).clientfunction();
		optionStruct->id_ =bean.options(i).id();
		optionStruct->msg_ =bean.options(i).msg();
		optionStruct->title_ = bean.options(i).titel();
		OptionVector.push_back(optionStruct);
		
	}
	PopupPanel::PopupBoardPara popupBoardStruct ={type_,resultStr,align_,times_,default_,inputType_,maxSize,hasOptions_};

	Size winsize =Director::getInstance()->getVisibleSize();
	PopupPanel *  popup_ = PopupPanel::create(popupBoardStruct,OptionVector);
	popup_->setIgnoreAnchorPointForPosition(false);
	popup_->setAnchorPoint(Vec2(0.5f,0.5f));
	popup_->setPosition(Vec2(winsize.width/2,winsize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(popup_);

}
