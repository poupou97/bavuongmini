
#ifndef Blog_C___Reflection_PushHandler5301_h
#define Blog_C___Reflection_PushHandler5301_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5301 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5301)

public:
	SYNTHESIZE(PushHandler5301, int*, m_pValue)

		PushHandler5301() ;
	virtual ~PushHandler5301() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
