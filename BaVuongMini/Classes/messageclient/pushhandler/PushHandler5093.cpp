
#include "PushHandler5093.h"

#include "../protobuf/FightMessage.pb.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/GameUIConstant.h"
#include "../element/CRewardBase.h"
#include "../../ui/Reward_ui/RewardUi.h"

IMPLEMENT_CLASS(PushHandler5093)

PushHandler5093::PushHandler5093() 
{

}
PushHandler5093::~PushHandler5093() 
{

}
void* PushHandler5093::createInstance()
{
	return new PushHandler5093() ;
}
void PushHandler5093::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5093::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5093::handle(CommonMessage* mb)
{
	Push5093 bean;
	bean.ParseFromString(mb->data());

	if (bean.status() == 1)
	{
		bool isHave = false;
		for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
		{
			if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_PHYSICAL_DAY)
			{
				isHave = true;
			}
		}

		if (isHave == false)
		{
			RewardUi::addRewardListEvent(REWARD_LIST_ID_PHYSICAL_DAY);
		}
	}else
	{
		//remove remind list
		RewardUi::removeRewardListEvent(REWARD_LIST_ID_PHYSICAL_DAY);

		if (bean.value() > 0)
		{
			const char * strings_ = StringDataManager::getString("remindgetPrizephysice_lingqutili");
			char prizeValue[50];
			sprintf(prizeValue,strings_,bean.value());
			GameView::getInstance()->showAlertDialog(prizeValue);
		}
	}
}