
#include "PushHandler1627.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "../element/CAdditionProperty.h"
#include "GameView.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/CEquipHole.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/equipMent_ui/SysthesisUI.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/MainAnimationScene.h"


IMPLEMENT_CLASS(PushHandler1627)

PushHandler1627::PushHandler1627() 
{

}
PushHandler1627::~PushHandler1627() 
{

}
void* PushHandler1627::createInstance()
{
	return new PushHandler1627() ;
}
void PushHandler1627::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1627::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1627::handle(CommonMessage* mb)
{
	ResMerge1627 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), "synthesis sucess");
	
	if (bean.success())
	{
		//GameView::getInstance()->showAlertDialog(StringDataManager::getString("synthesis_sucess"));

		if(GameView::getInstance())
		{
			MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				//mainScene->addInterfaceAnm("hccg/hccg.anm");
				MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
				if (mainAnmScene)
				{
					mainAnmScene->addInterfaceAnm("hccg/hccg.anm");
				}

				EquipMentUi * equipmentUI = (EquipMentUi*)mainScene->getChildByTag(ktagEquipMentUI);
				if (equipmentUI)
				{
					if (equipmentUI->synthesisLayer->isVisible())
					{
						SysthesisUI * systhesisiUI = (SysthesisUI*)equipmentUI->synthesisLayer->getChildByTag(CLASS_SYSTHESIS_TAG);
						if (systhesisiUI)
						{
							if (systhesisiUI->getCurSysthesisInfo() != NULL)
							{
								systhesisiUI->refreshUI(systhesisiUI->getCurSysthesisInfo());
								systhesisiUI->refreshContrelTree();
								systhesisiUI->addBonusSpecialEffect();
							}
						}
					}
				}
			}
		}
	}
}
