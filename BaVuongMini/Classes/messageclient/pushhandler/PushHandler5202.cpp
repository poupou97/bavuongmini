#include "PushHandler5202.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../ui/skillscene/MusouSkillTalent.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../element/CMusouTalent.h"
#include "../protobuf/VipMessage.pb.h"
#include "../element/CRewardProp.h"
#include "../../ui/vip_ui/VipDetailUI.h"
#include "../../ui/vip_ui/FirstBuyVipUI.h"

IMPLEMENT_CLASS(PushHandler5202)

	PushHandler5202::PushHandler5202() 
{

}
PushHandler5202::~PushHandler5202() 
{

}

void* PushHandler5202::createInstance()
{
	return new PushHandler5202() ;
}
void PushHandler5202::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5202::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5202::handle(CommonMessage* mb)
{
	Rsp5202 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (!GameView::getInstance())
		return;

	if (!GameView::getInstance()->getGameScene())
		return;

	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (!mainscene)
		return;

	std::vector<CRewardProp*> tempList;
	for (int i = 0; i < bean.rewardprops_size(); i++) 
	{
		CRewardProp * rewardProp = new CRewardProp();
		rewardProp->CopyFrom(bean.rewardprops(i));
		tempList.push_back(rewardProp);
	}
	
	if (bean.rewardtype() > 0)
	{
		VipDetailUI *vipScene = (VipDetailUI*)mainscene->getChildByTag(kTagVipDetailUI);
		if (vipScene)
		{
			vipScene->RefreshGoodsRewards(tempList);
		}
	}
	else
	{
		FirstBuyVipUI *firstBuyVipUI = (FirstBuyVipUI*)mainscene->getChildByTag(kTagFirstBuyVipUI);
		if (firstBuyVipUI)
		{
			firstBuyVipUI->RefreshGoodsRewards(tempList);
		}
	}

	std::vector<CRewardProp*>::iterator iter;
	for (iter = tempList.begin(); iter != tempList.end(); ++iter)
	{
		delete *iter;
	}
	tempList.clear();
	
}

