
#ifndef Blog_C___Reflection_PushHandler1929_h
#define Blog_C___Reflection_PushHandler1929_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
#include "../../ui/extensions/UIScene.h"
USING_NS_CC;

/**
 *  副本通关星级
 */
class PushHandler1929 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1929)
    
public:
    SYNTHESIZE(PushHandler1929, int*, m_pValue)
    
    PushHandler1929() ;
    virtual ~PushHandler1929() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
