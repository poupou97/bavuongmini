
#include "PushHandler5072.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsStrategiesUI.h"
#include "../../gamescene_state/role/GameActor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../element/CFightWayBase.h"

IMPLEMENT_CLASS(PushHandler5072)

PushHandler5072::PushHandler5072() 
{

}
PushHandler5072::~PushHandler5072() 
{

}
void* PushHandler5072::createInstance()
{
	return new PushHandler5072() ;
}
void PushHandler5072::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5072::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5072::handle(CommonMessage* mb)
{
	Resp5072 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	if (scene->getMainUIScene()->getChildByTag(kTagGeneralsUI))
	{
		if (bean.has_error())
		{
			GameView::getInstance()->showAlertDialog(bean.error().msg());
		}
		else
		{
			MyPlayer* me = GameView::getInstance()->myplayer;
			me->setActiveStrategyId(-1);

			GeneralsUI * genentalsUI = (GeneralsUI*)scene->getMainUIScene()->getChildByTag(kTagGeneralsUI);
			for (unsigned int i = 0;i<genentalsUI->generalsStrategiesUI->generalsStrategiesList.size();++i)
			{
				genentalsUI->generalsStrategiesUI->generalsStrategiesList.at(i)->set_activeflag(0);
				if (genentalsUI->generalsStrategiesUI->generalsStrategiesList.at(i)->fightwayid() == bean.fightwayid())
				{
					genentalsUI->generalsStrategiesUI->generalsStrategiesList.at(i)->set_activeflag(bean.flag());
				}

				// find the active strategy
				if(genentalsUI->generalsStrategiesUI->generalsStrategiesList.at(i)->activeflag() == 1)
				{
					me->setActiveStrategyId(genentalsUI->generalsStrategiesUI->generalsStrategiesList.at(i)->fightwayid());
				}
			}

			genentalsUI->generalsStrategiesUI->setActiveFightWay(bean.fightwayid());
			//genentalsUI->generalsStrategiesUI->RefreshListWithoutChangeOffSet();
			genentalsUI->generalsStrategiesUI->RefreshData();
		}
	}
}
