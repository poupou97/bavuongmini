#include "PushHandler1201.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"

IMPLEMENT_CLASS(PushHandler1201)

PushHandler1201::PushHandler1201() 
{
    
}
PushHandler1201::~PushHandler1201() 
{
    
}
void* PushHandler1201::createInstance()
{
    return new PushHandler1201() ;
}
void PushHandler1201::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1201::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1201::handle(CommonMessage* mb)
{
	Push1201 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, skill validation, result: %d", mb->cmdid(), bean.result());

	// use skill failed, becuase the distance is not enough
	if(bean.reason() == 1)   // the distance is too long
	{
		if (MyPlayerAIConfig::getAutomaticSkill() == 1)
			MyPlayerAI::autoMove(GameView::getInstance()->myplayer, 128);
	}
}
