#include "PushHandler5201.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../ui/skillscene/MusouSkillTalent.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../element/CMusouTalent.h"
#include "../protobuf/VipMessage.pb.h"
#include "../protobuf/ModelMessage.pb.h"
#include "../../gamescene_state/sceneelement/MyHeadInfo.h"
#include "../../ui/offlineExp_ui/OffLineExpUI.h"
#include "gamescene_state/sceneelement/GuideMap.h"

IMPLEMENT_CLASS(PushHandler5201)

	PushHandler5201::PushHandler5201() 
{

}
PushHandler5201::~PushHandler5201() 
{

}
void* PushHandler5201::createInstance()
{
	return new PushHandler5201() ;
}
void PushHandler5201::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5201::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5201::handle(CommonMessage* mb)
{
	Rsp5201 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (!GameView::getInstance())
		return;

	ActiveRole * activeRole = GameView::getInstance()->myplayer->getActiveRole();
	activeRole->mutable_playerbaseinfo()->set_viplevel(bean.viplevel());
	activeRole->mutable_playerbaseinfo()->set_rechargeflag(bean.rechargeflag());
	activeRole->mutable_playerbaseinfo()->set_vipremaintime(bean.vipremaintime());
	GameView::getInstance()->myplayer->showActorName(true);

	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainscene_ != NULL)
	{
		//mainscene_->remindReward();
		mainscene_->curMyHeadInfo->ReloadVipInfo();

		if (bean.rechargeflag() == 1)  //���ֵ
		{
			GuideMap * guideMap = (GuideMap*)mainscene_->getChildByTag(kTagGuideMapUI);
			if (guideMap)
			{
				if (guideMap->getBtnFirstReCharge())
				{
					guideMap->getBtnFirstReCharge()->removeFromParent();
				}
			}
		}
	}

	OffLineExpUI * offLineExpUI = (OffLineExpUI*)mainscene_->getChildByTag(kTagOffLineExpUI);
	if (offLineExpUI)
	{
		offLineExpUI->setCurRoleVipLevel(bean.viplevel());
		offLineExpUI->setToDefault();
		offLineExpUI->RefreshUI();
	}
}