
#ifndef Blog_C___Reflection_PushHandler7010_h
#define Blog_C___Reflection_PushHandler7010_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler7010 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler7010)

public:
	SYNTHESIZE(PushHandler7010, int*, m_pValue)

		PushHandler7010() ;
	virtual ~PushHandler7010() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
