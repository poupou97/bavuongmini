
#ifndef Blog_C___Reflection_PushHandler5201_h
#define Blog_C___Reflection_PushHandler5201_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5201 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5201)

public:
	SYNTHESIZE(PushHandler5201, int*, m_pValue)

		PushHandler5201() ;
	virtual ~PushHandler5201() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
