#include "PushHandler2805.h"
#include "../protobuf/QuestionMessage.pb.h"
#include "../../ui/Question_ui/QuestionData.h"
#include "../../GameView.h"
#include "../element/CPropReward.h"
#include "../../ui/Question_ui/QuestionEndUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/GoodsInfo.h"
#include "../../ui/Question_ui/QuestionRewardCardItem.h"

#define  PAGE_NUM 10

IMPLEMENT_CLASS(PushHandler2805)

	PushHandler2805::PushHandler2805() 
{

}
PushHandler2805::~PushHandler2805() 
{

}
void* PushHandler2805::createInstance()
{
	return new PushHandler2805() ;
}
void PushHandler2805::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2805::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2805::handle(CommonMessage* mb)
{
	Rsp2805 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//update ui
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	QuestionEndUI *questionEndUI = (QuestionEndUI*)mainscene->getChildByTag(kTagQuestionEndUI);
	if(questionEndUI != NULL)
	{
		GoodsInfo * goods = new GoodsInfo();
		goods->CopyFrom(bean.goods());

		int amount = bean.amount();

		if (QuestionData::instance()->get_reward_index() >= 0)
		{
			QuestionRewardCardItem * item = (QuestionRewardCardItem*)questionEndUI->u_layer->getChildByTag(kTag_Base_RewardCardItem + QuestionData::instance()->get_reward_index());
			if (item)
			{
				item->RefreshCardInfo(goods,amount);
				item->FlopAnimationToFront();
			}
		}

		if (questionEndUI->get_RemainNumber()-1 >= 0)
		{
			questionEndUI->set_RemainNumber(questionEndUI->get_RemainNumber()-1);
			QuestionData::instance()->set_cardNum(questionEndUI->get_RemainNumber());
		}
		
		delete goods;
	}

}