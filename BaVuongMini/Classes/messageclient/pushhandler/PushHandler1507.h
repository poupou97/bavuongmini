
#ifndef Blog_C___Family_PushHandler1507_h
#define Blog_C___Family_PushHandler1507_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1507 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1507)
    
public:
    SYNTHESIZE(PushHandler1507, int*, m_pValue)
    
    PushHandler1507() ;
    virtual ~PushHandler1507() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
