
#include "PushHandler7011.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../ui/missionscene/ExtraRewardsUI.h"
#include "../element/CMissionGoods.h"
#include "../element/CMissionReward.h"

IMPLEMENT_CLASS(PushHandler7011)

PushHandler7011::PushHandler7011() 
{

}
PushHandler7011::~PushHandler7011() 
{

}
void* PushHandler7011::createInstance()
{
	return new PushHandler7011() ;
}
void PushHandler7011::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler7011::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler7011::handle(CommonMessage* mb)
{
	PushExtraReward7011 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	std::string str_des = bean.title();

	//create ui
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagExtraRewardsUI))
	{
		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagExtraRewardsUI)->removeFromParent();
	}

	CMissionReward *missionReward = new CMissionReward();
	missionReward->CopyFrom(bean.reward());
	Size winSize = Director::getInstance()->getVisibleSize();
	ExtraRewardsUI * extraRewardsUI = ExtraRewardsUI::create(missionReward,str_des);
	GameView::getInstance()->getMainUIScene()->addChild(extraRewardsUI);
	extraRewardsUI->setTag(kTagExtraRewardsUI);
	extraRewardsUI->setIgnoreAnchorPointForPosition(false);
	extraRewardsUI->setAnchorPoint(Vec2(0.5f, 0.5f));
	extraRewardsUI->setPosition(Vec2(winSize.width/2, winSize.height/2));
	delete missionReward;
}
