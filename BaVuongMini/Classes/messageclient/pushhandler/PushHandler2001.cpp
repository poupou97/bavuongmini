
#include "PushHandler2001.h"
#include "../protobuf/MailMessage.pb.h"
#include "../../ui/Mail_ui/MailUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "GameView.h"
#include "../../ui/Mail_ui/MailList.h"
#include "../../messageclient/element/CMailInfo.h"
#include "../../ui/extensions/UITab.h"
#include "../../gamescene_state/MainScene.h"



IMPLEMENT_CLASS(PushHandler2001)

PushHandler2001::PushHandler2001() 
{

}
PushHandler2001::~PushHandler2001() 
{

}
void* PushHandler2001::createInstance()
{
	return new PushHandler2001() ;
}
void PushHandler2001::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2001::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2001::handle(CommonMessage* mb)
{
	Rsp2001 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	MailUI * mailui =(MailUI *)mainscene_->getChildByTag(kTagMailUi);
	if (mainscene_ != NULL && mailui == NULL)
	{
		std::vector<CMailInfo*>::iterator iter_systemMail;
		for (iter_systemMail = GameView::getInstance()->mailVectorOfSystem.begin(); iter_systemMail != GameView::getInstance()->mailVectorOfSystem.end(); ++iter_systemMail)
		{
			delete *iter_systemMail;
		}
		GameView::getInstance()->mailVectorOfSystem.clear();


		for (int i=0;i<bean.mails_size();i++)
		{
			CMailInfo * m_mail = new CMailInfo();
			m_mail->CopyFrom(bean.mails(i));
			GameView::getInstance()->mailVectorOfSystem.push_back(m_mail);
		}

		mainscene_->remindMail();
		return;
	}

	if (mailui != NULL)
	{
		if (mailui->curMailPage == 0)
		{
			std::vector<CMailInfo*>::iterator iterSystem;
			for (iterSystem = GameView::getInstance()->mailVectorOfSystem.begin(); iterSystem != GameView::getInstance()->mailVectorOfSystem.end(); ++iterSystem)
			{
				delete *iterSystem;
			}
			GameView::getInstance()->mailVectorOfSystem.clear();
		}

		for (int i=0;i<bean.mails_size();i++)
		{
			CMailInfo * m_mail = new CMailInfo();
			m_mail->CopyFrom(bean.mails(i));
			GameView::getInstance()->mailVectorOfSystem.push_back(m_mail);
		}

		std::vector<CMailInfo*>::iterator iterSource;
		for (iterSource = mailui->sourceDataVector.begin(); iterSource != mailui->sourceDataVector.end(); ++iterSource)
		{
			delete *iterSource;
		}
		mailui->sourceDataVector.clear();

		for (int i=0;i<GameView::getInstance()->mailVectorOfSystem.size();i++)
		{
			CMailInfo * m_mail = new CMailInfo();
			m_mail->CopyFrom(*GameView::getInstance()->mailVectorOfSystem.at(i));
			mailui->sourceDataVector.push_back(m_mail);
		}

		if (mailui->curMailPage < bean.page() - 1)
		{
			mailui->isHasNextPage =true;
		}else
		{
			mailui->isHasNextPage =false;
		}
		if (mailui->sourceDataVector.size()>0)
		{
			mailui->getMailLayer->setVisible(true);
			mailui->readPanel->setVisible(true);
			mailui->mailNull->setVisible(false);
			mailui->showMailInfo(0);
			TableView * tabMail=(TableView *)mailui->mailList->getChildByTag(MAILTABTAG);
			if (mailui->curMailPage == 0)
			{
				tabMail->reloadData();
			}else
			{
				Vec2 off_ = tabMail->getContentOffset();
				int h_ = tabMail->getContentSize().height + off_.y;
				tabMail->reloadData();	
				Vec2 temp = Vec2(off_.x,h_ - tabMail->getContentSize().height);
				tabMail->setContentOffset(temp);
			}
			
			mailui->mailList->setVisible(true);
		}else
		{
			mailui->getMailLayer->setVisible(false);
			mailui->readPanel->setVisible(false);
			mailui->mailNull->setVisible(true);

			TableView * tabMail=(TableView *)mailui->mailList->getChildByTag(MAILTABTAG);
			//tabMail->reloadData();
			mailui->mailList->setVisible(false);
		}

		if(mailui->channelLabel->getCurrentIndex() ==1)
		{
			mailui->getMailLayer->setVisible(false);
			mailui->readPanel->setVisible(false);
			mailui->mailNull->setVisible(false);
			mailui->sendMailLayer->setVisible(true);
			mailui->writePanel->setVisible(true);
			mailui->mailList->setVisible(false);
		}
	}
	
	
	
	/*

// 	std::vector<CMailInfo*>::iterator iterPlayer;
// 	for (iterPlayer = GameView::getInstance()->mailVectorOfPlayer.begin(); iterPlayer != GameView::getInstance()->mailVectorOfPlayer.end(); ++iterPlayer)
// 	{
// 		delete *iterPlayer;
// 	}
// 	GameView::getInstance()->mailVectorOfPlayer.clear();


	for (int i=0;i<bean.mails_size();i++)
	{
		CMailInfo * m_mail = new CMailInfo();
		m_mail->CopyFrom(bean.mails(i));
		GameView::getInstance()->mailVectorOfSystem.push_back(m_mail);
		
// 		long long name_systemId = m_mail->sender();
// 		if (name_systemId == LLONG_MAX)
// 		{
// 			GameView::getInstance()->mailVectorOfSystem.push_back(m_mail);
// 		}else
// 		{
// 			GameView::getInstance()->mailVectorOfPlayer.push_back(m_mail);
// 		}
		
	}

// 	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
// 	if (mainscene_ != NULL)
// 	{
// 		mainscene_->remindMail();
// 	}
// 	MailUI * mailui =(MailUI *)mainscene_->getChildByTag(kTagMailUi);
// 	if (mailui == NULL)
// 		return;
	
// 	for(int mailIndex =0;mailIndex <GameView::getInstance()->mailVectorOfPlayer.size();mailIndex++)
// 	{
// 		if (GameView::getInstance()->mailVectorOfPlayer.at(mailIndex)->flag() != 1)
// 		{
// 			mailui->channelLabelMail->setDefaultPanelByIndex(1);
// 			mailui->curTabType =1;
// 			mailui->replyMail->setVisible(true);
// 			
// 		}
// 	}
	

	if (mailui->curMailPage == 0)
	{
		std::vector<CMailInfo*>::iterator iterSource;
		for (iterSource =mailui->sourceDataVector.begin(); iterSource != mailui->sourceDataVector.end(); ++iterSource)
		{
			delete *iterSource;
		}
		mailui->sourceDataVector.clear();
	}
	
	for (int i=0;i<mail_num;i++)
	{
		CMailInfo * m_mail = new CMailInfo();
		m_mail->CopyFrom(bean.mails(i));
		mailui->sourceDataVector.push_back(m_mail);
		
// 		long long name_systemId = m_mail->sender();
// 		if (name_systemId == LLONG_MAX )
// 		{
// 			if ( mailui->curTabType == 0)
// 			{
// 				mailui->sourceDataVector.push_back(m_mail);
// 			}
// 		}else
// 		{
// 			if (mailui->curTabType == 1)
// 			{
// 				mailui->sourceDataVector.push_back(m_mail);
// 			}
// 		}
		
	}

	if (mailui->curMailPage < bean.page() - 1)
	{
		mailui->isHasNextPage =true;
	}else
	{
		mailui->isHasNextPage =false;
	}
	if (mailui->sourceDataVector.size()>0)
	{
 		mailui->getMailLayer->setVisible(true);
 		mailui->readPanel->setVisible(true);
 		mailui->mailNull->setVisible(false);
		mailui->showMailInfo(0);
		TableView * tabMail=(TableView *)mailui->mailList->getChildByTag(MAILTABTAG);
		tabMail->reloadData();
		mailui->mailList->setVisible(true);
	}else
	{
 		mailui->getMailLayer->setVisible(false);
 		mailui->readPanel->setVisible(false);
 		mailui->mailNull->setVisible(true);

		TableView * tabMail=(TableView *)mailui->mailList->getChildByTag(MAILTABTAG);
		tabMail->reloadData();
		mailui->mailList->setVisible(true);
	}
	
	if(mailui->channelLabel->getCurrentIndex() ==1)
	{
		mailui->getMailLayer->setVisible(false);
		mailui->readPanel->setVisible(false);
		mailui->mailNull->setVisible(false);

		mailui->sendMailLayer->setVisible(true);
		mailui->writePanel->setVisible(true);
		mailui->mailList->setVisible(false);
	}
	*/
}
