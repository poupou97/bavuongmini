
#include "PushHandler1545.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightUI.h"
#include "../element/CGuildDailyReward.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightRewardsUI.h"

IMPLEMENT_CLASS(PushHandler1545)

PushHandler1545::PushHandler1545() 
{

}
PushHandler1545::~PushHandler1545() 
{

}
void* PushHandler1545::createInstance()
{
	return new PushHandler1545() ;
}
void PushHandler1545::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1545::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1545::handle(CommonMessage* mb)
{
	Rsp1545 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// �������
	FamilyFightData::setFamilyFightRanking(bean.guildranking());
	//�����ȡ״̬
	FamilyFightData::setFamilyRewardStatus(bean.rewardstatus());

	//delete old rewardInfo
	std::vector<CGuildDailyReward*>::iterator iter;
	for (iter = FamilyFightData::getInstance()->familyFightRewardList.begin(); iter != FamilyFightData::getInstance()->familyFightRewardList.end(); ++iter)
	{
		delete *iter;
	}
	FamilyFightData::getInstance()->familyFightRewardList.clear();
	//add new
	for (int i = 0;i<bean.reward_size();++i)
	{
		CGuildDailyReward * temp = new CGuildDailyReward();
		temp->CopyFrom(bean.reward(i));
		FamilyFightData::getInstance()->familyFightRewardList.push_back(temp);
	}

	//�����Ƿ������ȡ
	//�Ƿ��н��
	bool isExistReward = false;
	if (FamilyFightData::getInstance()->familyFightRewardList.size()>0)
	{
		CGuildDailyReward* guideReward = FamilyFightData::getInstance()->familyFightRewardList.at(FamilyFightData::getInstance()->familyFightRewardList.size()-1);
		if (FamilyFightData::getInstance()->getFamilyFightRanking()>0 && FamilyFightData::getInstance()->getFamilyFightRanking() <= guideReward->endrank())
		{
			isExistReward = true;
		}
	}
	if(isExistReward && (!FamilyFightData::getInstance()->getFamilyRewardStatus()))
	{
		if (GameView::getInstance()->getMainUIScene())
		{
			FamilyUI *familyui = (FamilyUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
			if (familyui)
			{
				FamilyFightUI * familyFightUI = (FamilyFightUI*)familyui->GetFamilyFightUI();
				if (familyFightUI)
				{
					familyFightUI->AddParticleForBtnGetReward();
				}
			}
		}
	}

	FamilyFightRewardsUI * familyFightRewardUI = (FamilyFightRewardsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightRewardUI);
	if (familyFightRewardUI)
	{
		familyFightRewardUI->ReloadTableViewWithOutChangeOffSet();
		familyFightRewardUI->RefreshMyRanking();
	}
}
