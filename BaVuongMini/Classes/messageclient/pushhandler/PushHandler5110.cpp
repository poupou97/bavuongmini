#include "PushHandler5110.h"

#include "../element/COneOnlineGift.h"
#include "../element/GoodsInfo.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/OnlineReward_ui/OnLineRewardUI.h"
#include "../protobuf/DailyGift.pb.h"
#include "../../ui/OnlineReward_ui/OnlineGiftData.h"
#include "../../ui/OnlineReward_ui/OnlineGiftIconWidget.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../ui/OnlineReward_ui/OnlineGiftLayer.h"


IMPLEMENT_CLASS(PushHandler5110)

	PushHandler5110::PushHandler5110() 
{

}
PushHandler5110::~PushHandler5110() 
{
	
}
void* PushHandler5110::createInstance()
{
	return new PushHandler5110() ;
}
void PushHandler5110::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5110::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5110::handle(CommonMessage* mb)
{
	Rsp5110 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// �����
	std::vector<COneOnlineGift *>::iterator iter;
	for (iter = OnlineGiftData::instance()->m_vector_oneOnLineGift.begin(); iter != OnlineGiftData::instance()->m_vector_oneOnLineGift.end(); iter++)
	{
		delete *iter;
	}
	OnlineGiftData::instance()->m_vector_oneOnLineGift.clear();


	// ݳ�ʼ�� ������������
	int size = bean.auctioninfos_size();
	for (int i = 0; i<size; i++)
	{
		COneOnlineGift * dayGift =new COneOnlineGift();
		dayGift->CopyFrom(bean.auctioninfos(i));

		OnlineGiftData::instance()->m_vector_oneOnLineGift.push_back(dayGift);
	}

	OnlineGiftData::instance()->initDataFromIntent();

	if (NULL != GameView::getInstance()->getGameScene())
	{
		GuideMap * pGuideMap = (GuideMap *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
		if (NULL != pGuideMap)
		{
			// ��ж����߽��icon��Ƿ���ڣ���icon��岻���ڣ�˵���1���ȼ��������ޣ������������߽��2�֮ǰ�����н�������ȡ��3���û������
			// ������ڵĻ���˵� �֮ǰ�Ľ���û����ȡ�
			if (pGuideMap->getActionLayer())
			{
				OnlineGiftIconWidget* pOnlineGiftIconWidget = (OnlineGiftIconWidget *)pGuideMap->getActionLayer()->getChildByTag(kTagOnLineGiftIcon);
				if (NULL != pOnlineGiftIconWidget)
				{
					// ��������������������ȡ ��Ϊ ����ȡ��˵� �ȼ�������� ޽����еiconĴ���رգ���� ���´��壨icon��� � UIʹ��壩
					if (OnlineGiftData::instance()->isAllGiftHasGet())
					{
						pOnlineGiftIconWidget->allGiftGetIconExit();
					}
					else	
					{
						// icon���
						pOnlineGiftIconWidget->initDataFromIntent();

						// ���UI´��
						OnLineRewardUI * pOnLineRewardUI = (OnLineRewardUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftUI);
						if (NULL != pOnLineRewardUI)
						{
							pOnLineRewardUI->refreshData();
							pOnLineRewardUI->refreshTimeAndNum();					// ��� ½���� ź �ʱ�
							pOnLineRewardUI->update(0);
						}
					}
				}
			}
		}

		OnlineGiftLayer * pOnlineGiftLayer = (OnlineGiftLayer *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftLayer);
		if (NULL != pOnlineGiftLayer)
		{
			pOnlineGiftLayer->initDataFromIntent();
		}
	}




	//if (NULL !=  GameView::getInstance()->getGameScene())
	//{
	//	// ��ж����߽��icon��Ƿ���;���icon��岻���ڣ�˵���1���ȼ��������ޣ�������� ����߽��2�֮ǰ�����н�������ȡ��3���û������
	//	OnlineGiftIconWidget * pOnlineGiftIconWidget = (OnlineGiftIconWidget *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftIcon);
	//	if (NULL == pOnlineGiftIconWidget)
	//	{
	//		// ��������������������ȡ ��Ϊ ����ȡ��˵� �ȼ�������� ޲��ٴ���icon���
	//		if (OnlineGiftData::instance()->isAllGiftHasGet())
	//		{
	//			// 岻�ٴ������
	//		}
	//		else
	//		{
	//			//崴�����壨��ʱӦΪ 24����½�����û�г���ȼ����ޣ������ ��û������ߣ�

	//			if (NULL != GameView::getInstance()->getMainUIScene())
	//			{
	//				OnlineGiftIconWidget * pOnlineGiftIcon = (OnlineGiftIconWidget *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftIcon);
	//				if (NULL != pOnlineGiftIcon)
	//				{
	//					pOnlineGiftIcon->initDataFromIntent();
	//				}
	//				else
	//				{
	//					/*OnlineGiftIcon * pOnlineGiftIcon = OnlineGiftIcon::create();
	//					pOnlineGiftIcon->setIgnoreAnchorPointForPosition(false);
	//					pOnlineGiftIcon->setAnchorPoint(Vec2(0,0));
	//					pOnlineGiftIcon->setPosition(Vec2(0,0));
	//					pOnlineGiftIcon->setTag(kTagOnLineGiftIcon);
	//					pOnlineGiftIcon->initDataFromIntent();

	//					GameView::getInstance()->getMainUIScene()->addChild(pOnlineGiftIcon);*/
	//				}
	//			}

	//		}
	//	}
	//	else
	//	{
	//		// ������ڣ�˵� �֮ǰ�Ľ���û����ȡ�

	//		// ��������������������ȡ ��Ϊ ����ȡ��˵� �ȼ�������� ޽����еiconĴ���رգ���� ���´��壨icon��� � UIʹ��壩
	//		if (OnlineGiftData::instance()->isAllGiftHasGet())
	//		{
	//			pOnlineGiftIconWidget->allGiftGetIconExit();
	//		}
	//		else
	//		{
	//			// icon���
	//			pOnlineGiftIconWidget->initDataFromIntent();

	//			// UI崰�
	//			OnLineRewardUI * pOnLineRewardUI = (OnLineRewardUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftUI);
	//			if (NULL != pOnLineRewardUI)
	//			{
	//				pOnLineRewardUI->refreshData();
	//				pOnLineRewardUI->refreshTimeAndNum();					// ��� ½���� ź �ʱ�
	//				pOnLineRewardUI->update(0);
	//			}
	//		}
	//	}
	//}


}