#ifndef Blog_C___Reflection_PushHandler1201_h
#define Blog_C___Reflection_PushHandler1201_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1201 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1201)
    
public:
    SYNTHESIZE(PushHandler1201, int*, m_pValue)
    
    PushHandler1201() ;
    virtual ~PushHandler1201() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
