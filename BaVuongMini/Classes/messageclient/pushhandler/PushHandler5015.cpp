#include "PushHandler5015.h"

#include "../protobuf/CollectMessage.pb.h"  
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../element/CMapInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Active_ui/ActiveManager.h"

IMPLEMENT_CLASS(PushHandler5015)

PushHandler5015::PushHandler5015() 
{
    
}
PushHandler5015::~PushHandler5015() 
{
    
}
void* PushHandler5015::createInstance()
{
    return new PushHandler5015() ;
}
void PushHandler5015::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5015::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler5015::handle(CommonMessage* mb)
{
	ResBoxPosition5015 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// ��ʼ�� ������������
	// bean.positions_size()
	/*bean.positions(0)
	int size = bean.positions_size();
	for (int i = 0; i < size; i++)
	{
		COneOnlineGift * dayGift =new COneOnlineGift();
		dayGift->CopyFrom(bean.auctioninfos(i));

		OnlineGiftData::instance()->m_vector_oneOnLineGift.push_back(dayGift);
	}*/

	int nSize = bean.positions_size();
	if (nSize <= 0)
	{
		return;
	}


	// �˵���ʱ �����ͼ����겻���
	if (INVAILD == bean.positions(0).state())
	{
		// ùر ջ�Ծ� ȴ��
		ActiveUI * activeUI = (ActiveUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
		if(NULL != activeUI)
		{
			activeUI->closeAnim();
		}

		//// ���ʾ��ң���ǰ���䲻���
		//const char * charInfo = StringDataManager::getString("activy_presentBoxInfo");
		//GameView::getInstance()->showAlertDialog(charInfo);

		//return;
	}

	std::string targetMapId = bean.positions(0).mapid();
	int xPos = bean.positions(0).x();
	int yPos = bean.positions(0).y();

	int posX = GameView::getInstance()->getGameScene()->tileToPositionX(xPos);
	int posY = GameView::getInstance()->getGameScene()->tileToPositionY(yPos);

	Vec2 targetPos = Vec2(posX, posY);

	//// �Ѱ·�߼�
	//if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	//{
	//	//same map
	//	Vec2 cp_role = GameView::getInstance()->myplayer->getPosition();
	//	Vec2 cp_target = targetPos;
	//	if (sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)) < 64)
	//	{
	//		return;
	//	}
	//	else
	//	{
	//		if (targetPos.x == 0 && targetPos.y == 0)
	//		{
	//			return;
	//		}

	//		MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
	//		// NOT reachable, so find the near one

	//		cmd->targetPosition = targetPos;
	//		cmd->method = MyPlayerCommandMove::method_searchpath;
	//		bool bSwitchCommandImmediately = true;
	//		if(GameView::getInstance()->myplayer->isAttacking())
	//			bSwitchCommandImmediately = false;
	//		GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
	//	}
	//}
	//else
	//{
	//	MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
	//	cmd->targetMapId = targetMapId;
	//	if (targetPos.x == 0 && targetPos.y == 0)
	//	{
	//	}
	//	else
	//	{
	//		cmd->targetPosition = targetPos;
	//	}
	//	cmd->method = MyPlayerCommandMove::method_searchpath;
	//	bool bSwitchCommandImmediately = true;
	//	if(GameView::getInstance()->myplayer->isAttacking())
	//		bSwitchCommandImmediately = false;
	//	GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
	//}

	// �����Զ�Ѱ·
	// �ж��Ƿ�ӵ�mainUIScene
	ActiveManager * pActiveManager = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
	if (NULL == pActiveManager)
	{
		ActiveManager* activeManager = ActiveManager::create();
		activeManager->set_targetMapId(targetMapId);
		activeManager->set_targetPos(targetPos);
		activeManager->setLayerUserful(true);
		activeManager->set_activeType(ActiveManager::TYPE_PRESENTBOX);
		activeManager->setTag(kTagActiveLayer);

		GameView::getInstance()->getMainUIScene()->addChild(activeManager);
	}
	else 
	{
		pActiveManager->set_targetMapId(targetMapId);
		pActiveManager->set_targetPos(targetPos);
		pActiveManager->setLayerUserful(true);
		pActiveManager->set_activeType(ActiveManager::TYPE_PRESENTBOX);
	}

	// Ѱ·�߼�
	if (strcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(),targetMapId.c_str()) == 0)
	{
		////same map
		//Vec2 cp_role = GameView::getInstance()->myplayer->getPosition();
		//Vec2 cp_target = targetPos;
		//if (sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)) < 64)
		//{
		//	return;
		//}
		//else
		//{
		//	if (targetPos.x == 0 && targetPos.y == 0)
		//	{
		//		return;
		//	}

		//	MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
		//	// NOT reachable, so find the near one

		//	cmd->targetPosition = targetPos;
		//	cmd->method = MyPlayerCommandMove::method_searchpath;
		//	bool bSwitchCommandImmediately = true;
		//	if(GameView::getInstance()->myplayer->isAttacking())
		//		bSwitchCommandImmediately = false;
		//	GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	
		//}
		
		// ��ʾ������ͼ�б��䣬��ע����ҡ�
		const char * charInfo = StringDataManager::getString("PresentBox_tip_1");
		GameView::getInstance()->showAlertDialog(charInfo);
		
	}
	else
	{
		// ����ͬһ��ͼ��ֱ�ӷɵ�Ŀ�ĵ
		ActiveManager::AcrossMapTransport(targetMapId, targetPos);

		// ��ж��Ƿ�ӵ�mainUIScene
		ActiveManager * pActiveLayer = (ActiveManager *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveLayer);
		if (NULL == pActiveLayer)
		{
			ActiveManager* activeLayer = ActiveManager::create();
			activeLayer->set_transportFinish(false);
			activeLayer->setTag(kTagActiveLayer);

			GameView::getInstance()->getMainUIScene()->addChild(activeLayer);
		}
		else 
		{
			pActiveLayer->set_transportFinish(false);
		}
	}

	ActiveUI * activeUI = (ActiveUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	if(NULL != activeUI)
	{
		activeUI->closeAnim();
	}
}
