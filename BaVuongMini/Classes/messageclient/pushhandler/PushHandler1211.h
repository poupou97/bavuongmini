
#ifndef Blog_C___Reflection_PushHandler1211_h
#define Blog_C___Reflection_PushHandler1211_h

#include "PushHandlerProtocol.h"
#include "../../common/CKBaseClass.h"
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class BaseFighter;

class PushHandler1211 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1211)

public:
	SYNTHESIZE(PushHandler1211, int*, m_pValue)

	PushHandler1211() ;
	virtual ~PushHandler1211() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

private:
	void createNameAction(BaseFighter* pBaseFighter);
	void createLevelupText(int level);
} ;

#endif
