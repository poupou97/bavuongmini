
#ifndef Blog_C___Reflection_PushHandler1109_h
#define Blog_C___Reflection_PushHandler1109_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

using namespace std;

#include "cocos2d.h"
USING_NS_CC;

class BaseFighter;

class PushHandler1109 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1109)
    
public:
    SYNTHESIZE(PushHandler1109, int*, m_pValue)
    
    PushHandler1109() ;
    virtual ~PushHandler1109() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
