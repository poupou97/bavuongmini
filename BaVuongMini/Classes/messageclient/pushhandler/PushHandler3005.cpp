
#include "PushHandler3005.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "GameView.h"
#include "../protobuf/OfflineFight.pb.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(PushHandler3005)

	PushHandler3005::PushHandler3005() 
{

}
PushHandler3005::~PushHandler3005() 
{

}
void* PushHandler3005::createInstance()
{
	return new PushHandler3005() ;
}
void PushHandler3005::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler3005::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler3005::handle(CommonMessage* mb)
{
	PushUpdateFightTimes3005 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());


	if (GameView::getInstance()->getGameScene() == NULL)
		return;

	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainscene_ != NULL)
	{
		int m_times = bean.fightconfig().fighttimesupvalue() - bean.fightconfig().fighttimes();
		GameView::getInstance()->setRemainTimesOfArena(m_times);
		mainscene_->remindOffArena();
	}

	OffLineArenaUI * offLineArenaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
	if (offLineArenaUI == NULL)
		return;

	//ˢ����ս���
	offLineArenaUI->RefreshFightConfig(bean.fightconfig().fighttimes(),bean.fightconfig().fighttimesupvalue(),bean.fightconfig().remainbuytimes());

}
