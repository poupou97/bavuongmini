#include "PushHandler1905.h"

#include "../protobuf/CopyMessage.pb.h"
#include "../../ui/challengeRound/ShowLevelUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/CTRankingPrize.h"
#include "../../ui/challengeRound/RankRewardUi.h"
#include "../../utils/StaticDataManager.h"
#include "../element/CRewardBase.h"
#include "../../ui/GameUIConstant.h"
#include "../../ui/Reward_ui/RewardUi.h"


IMPLEMENT_CLASS(PushHandler1905)

PushHandler1905::PushHandler1905() 
{

}
PushHandler1905::~PushHandler1905() 
{

}
void* PushHandler1905::createInstance()
{
	return new PushHandler1905() ;
}
void PushHandler1905::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1905::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1905::handle(CommonMessage* mb)
{
	ResPrizeInfo1905 bean;
	bean.ParseFromString(mb->data());
	
	RankRewardUi * rewardui_ = (RankRewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyReward);
	if (rewardui_)
	{
		if (bean.playerranking() == 0)
		{
			const char * selfRank_ = StringDataManager::getString("singCopy_rankingIsNotSort");
			rewardui_->label_selfRank->setString(selfRank_);
		}else
		{
			char selfRankStr[10];
			sprintf(selfRankStr,"%d",bean.playerranking());
			rewardui_->label_selfRank->setString(selfRankStr);
		}

		for (int i =0;i<bean.prizes_size();i++)
		{
			CTRankingPrize * prizes_ =new CTRankingPrize();
			prizes_->CopyFrom(bean.prizes(i));

			rewardui_->rankPrizevector.push_back(prizes_);
		}
		rewardui_->tableView_reward->reloadData();
	}
	// get reward
	if (bean.playerranking() != 0)
	{
		bool isHave_chushiniudao = false;
		bool isHave_xiaoyoumingqi = false;
		bool isHave_jiangongliye = false;

		for (int i = 0; i<GameView::getInstance()->rewardvector.size();i++ )
		{
			if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_SINGCOPYGETREWARD_CHUSHINIUDAO)
			{
				isHave_chushiniudao = true;
			}

			if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_SINGCOPYGETREWARD_XIAOYOUMINGQI)
			{
				isHave_xiaoyoumingqi = true;
			}

			if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_SINGCOPYGETREWARD_JIANGONGLEYE)
			{
				isHave_jiangongliye = true;
			}
		}

		bool isCanPrize_ = false;
		for (int i =0;i<bean.prizes_size();i++)
		{
			if (bean.prizes(i).canreceive())
			{
				isCanPrize_ = true;
			}
		}

		switch(bean.clazz())
		{
		case 1:
			{
				if (isHave_chushiniudao ==false && isCanPrize_ == true)
				{
					RewardUi::addRewardListEvent(REWARD_LIST_ID_SINGCOPYGETREWARD_CHUSHINIUDAO);
				}
			}break;
		case 2:
			{
				if (isHave_xiaoyoumingqi == false && isCanPrize_ == true)
				{
					RewardUi::addRewardListEvent(REWARD_LIST_ID_SINGCOPYGETREWARD_XIAOYOUMINGQI);
				}
			}break;
		case 3:
			{
				if (isHave_jiangongliye == false && isCanPrize_ == true)
				{
					RewardUi::addRewardListEvent(REWARD_LIST_ID_SINGCOPYGETREWARD_JIANGONGLEYE);
				}
			}break;
		}
	}
}