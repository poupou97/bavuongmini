
#ifndef Blog_C___Reflection_PushHandler5013_h
#define Blog_C___Reflection_PushHandler5013_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5013 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler5013)
    
public:
    SYNTHESIZE(PushHandler5013, int*, m_pValue)
    
    PushHandler5013() ;
    virtual ~PushHandler5013() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
