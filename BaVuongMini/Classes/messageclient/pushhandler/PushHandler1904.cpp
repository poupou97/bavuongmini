#include "PushHandler1904.h"

#include "../protobuf/CopyMessage.pb.h"
#include "../../ui/challengeRound/ShowLevelUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/CTowerHistory.h"
#include "../../ui/challengeRound/SingleRankListUi.h"
#include "../element/CRankingPlayer.h"
#include "../../utils/StaticDataManager.h"


IMPLEMENT_CLASS(PushHandler1904)

PushHandler1904::PushHandler1904() 
{

}
PushHandler1904::~PushHandler1904() 
{

}
void* PushHandler1904::createInstance()
{
	return new PushHandler1904() ;
}
void PushHandler1904::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1904::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1904::handle(CommonMessage* mb)
{
	ResTowerRanking1904 bean;
	bean.ParseFromString(mb->data());
	
	SingleRankListUi * rangklistui = (SingleRankListUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyRank);

	if (bean.currentranking() == 0)
	{
		const char * notSort_ = StringDataManager::getString("singCopy_rankingIsNotSort");
		rangklistui->labelSelfRank->setString(notSort_);
	}else
	{
		char selfRank[10];
		sprintf(selfRank,"%d",bean.currentranking());
		rangklistui->labelSelfRank->setString(selfRank);
	}

	//rangklistui->curRanklistPage
	if (bean.page() < bean.total())
	{
		for (int i=0;i<bean.player_size();i++)
		{
			CRankingPlayer * rankplayer = new CRankingPlayer();
			rankplayer->CopyFrom(bean.player(i));

			rangklistui->rangkPlayervector.push_back(rankplayer);
		}
		rangklistui->tableView_ranking->reloadData();

		if (bean.page() < bean.total() -1)
		{
			rangklistui->booIsHaveNext = true;
		}else
		{
			rangklistui->booIsHaveNext = false;
		}
	}
}