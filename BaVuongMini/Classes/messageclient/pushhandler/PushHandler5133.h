
#ifndef Blog_C___Reflection_PushHandler5133_h
#define Blog_C___Reflection_PushHandler5133_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5133 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5133)

public:
	SYNTHESIZE(PushHandler5133, int*, m_pValue)

	PushHandler5133() ;
	virtual ~PushHandler5133() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
