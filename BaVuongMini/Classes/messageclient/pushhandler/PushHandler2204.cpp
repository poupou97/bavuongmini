
#include "PushHandler2204.h"
#include "../../ui/extensions/RichTextInput.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/role/BaseFighterConstant.h"
#include "../../messageclient/protobuf/RelationMessage.pb.h"
#include "../../gamescene_state/sceneelement/ChatWindows.h"
#include "../../ui/Chat_ui/ChatCell.h"
#include "../../ui/Chat_ui/ChatUI.h"
#include "../../GameView.h"
#include "../../messageclient/element/CRelationPlayer.h"
#include "../../ui/Chat_ui/Tablist.h"
#include "../../utils/GameUtils.h"
#include "GameAudio.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/Chat_ui/PrivateChatUi.h"
#include "../../ui/Chat_ui/PrivateContent.h"
#include "../../ui/Chat_ui/PrivateCell.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/CCTutorialParticle.h"

IMPLEMENT_CLASS(PushHandler2204)

PushHandler2204::PushHandler2204() 
{
    
}
PushHandler2204::~PushHandler2204() 
{
    
}
void* PushHandler2204::createInstance()
{
    return new PushHandler2204() ;
}
void PushHandler2204::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2204::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler2204::handle(CommonMessage* mb)
{
	Push2204 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, chat message", mb->cmdid());
	
	// create a new ChatCell
	long long playerId= bean.speakerid();
	std::string playName=bean.speakername();
	std::string playCountry = bean.country();
	std::string str=bean.content();
	int channelid= bean.channelid();
	int viplevel_ = bean.viplevel();

	if (bean.channelid() == 0 && playerId == GameView::getInstance()->myplayer->getActiveRole()->rolebase().roleid())
	{
		playCountry = bean.listenercountry();
	}
	
	if (bean.channelid() == 0 && strcmp(bean.listenername().c_str(),"")==0)
	{
		const char *string_  = StringDataManager::getString("friend_playerNotLineOfChat");
		GameView::getInstance()->showAlertDialog(string_);
		return;
	}
	
	if(!channelid)
	{
		GameUtils::playGameSound(CHAT_PRIVATE, 2, false);
	}

	if (channelid == 0)
	{
		Scene* pScene = Director::getInstance()->getRunningScene();
		if(pScene->getTag() != GameView::STATE_GAME)
		{
			return;
		}
		else
		{
			GameSceneState* pState = (GameSceneState*)pScene;
			if(pState->getState() == GameSceneState::state_load)
			{
				return;
			}
		}

		GameSceneLayer* scene = GameView::getInstance()->getGameScene();
		if(pScene->getTag() != GameView::STATE_GAME)
		{
			return;
		}
		else
		{
			GameSceneState* pState = (GameSceneState*)pScene;
			if(pState->getState() == GameSceneState::state_load)
			{
				return;
			}
		}

		PrivateCell * private_Cell  = PrivateCell::create(bean.speaker().profession(),playName.c_str(),str.c_str(),playerId,viplevel_,bean.listenername());
		private_Cell->retain();
		GameView::getInstance()->chatPrivateContentVector.push_back(private_Cell);

		bool isHave = false;
		long long myplayerId = GameView::getInstance()->myplayer->getActiveRole()->rolebase().roleid();
		for (int i=0;i<GameView::getInstance()->chatPrivateSpeakerVector.size();i++)
		{
			if (GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playerid() == playerId || playerId == myplayerId)
			{
				isHave = true;
			}
		}
		PrivateChatUi * privateui = (PrivateChatUi *)scene->getMainUIScene()->getChildByTag(ktagPrivateUI);
		if (isHave == false)
		{
			CRelationPlayer * relation_ =new CRelationPlayer();
			relation_->CopyFrom(bean.speaker());
			GameView::getInstance()->chatPrivateSpeakerVector.push_back(relation_);

			if (privateui != NULL)
			{
				privateui->nameTabview->reloadData();
			}
		}
		std::string m_selectName = "";
		if (privateui != NULL)
		{
			m_selectName = privateui->selectPlayerName;
			if (privateui->selectPlayerId == bean.speakerid()|| myplayerId == bean.speakerid() )
			{
				std::string selectname = "";
				if (privateui->selectPlayerId == bean.speakerid())
				{
					selectname = bean.speakername();
				}
				
				if (myplayerId == bean.speakerid())
				{
					selectname = bean.listenername();
				}
				privateui->showPrivateContent(selectname);
			}

			for (int i =0;i<GameView::getInstance()->chatPrivateSpeakerVector.size();i++ )
			{
				if (strcmp(bean.speakername().c_str(),GameView::getInstance()->chatPrivateSpeakerVector.at(i)->playername().c_str())==0)
				{
					//refresh  all exist playerInfo
					GameView::getInstance()->chatPrivateSpeakerVector.at(i)->set_playerid(bean.speakerid());
					GameView::getInstance()->chatPrivateSpeakerVector.at(i)->set_playername(bean.speakername());
					GameView::getInstance()->chatPrivateSpeakerVector.at(i)->set_country(bean.speaker().country());
					GameView::getInstance()->chatPrivateSpeakerVector.at(i)->set_viplevel(bean.viplevel());
					GameView::getInstance()->chatPrivateSpeakerVector.at(i)->set_level(bean.speaker().level());
					GameView::getInstance()->chatPrivateSpeakerVector.at(i)->set_profession(bean.speaker().profession());

					privateui->nameTabview->updateCellAtIndex(i);
				}
			}
		}
		if (strcmp(m_selectName.c_str(),bean.speakername().c_str())==0 || bean.speakerid() == myplayerId)
		{
			//if speaker is self or selectCell Name  igor
		}else
		{
			CRelationPlayer * relationInfo_ =new CRelationPlayer();
			relationInfo_->CopyFrom(bean.speaker());
			GameView::getInstance()->chatPrivateNewMessageInfo.push_back(relationInfo_);

			if (privateui != NULL)
			{
				privateui->setNewMessageFlag(true);
			}
		}
	}

	// cache chat cell
	ChatCell * chatCell=ChatCell::create(channelid,playCountry,playName.c_str(),str.c_str(),playerId,true,viplevel_,bean.listenerid(),bean.listenername());
	chatCell->retain();
	GameView::getInstance()->chatCell_Vdata.push_back(chatCell);
	int size = GameView::getInstance()->chatCell_Vdata.size();
	if (size > 30)
	{
		std::vector<ChatCell *>::iterator iter=GameView::getInstance()->chatCell_Vdata.begin();
		ChatCell * cellv_=* iter;
		GameView::getInstance()->chatCell_Vdata.erase(iter);
		cellv_->release();
	}

	// it's not game state, ignore
	Scene* pScene = Director::getInstance()->getRunningScene();
	if(pScene->getTag() != GameView::STATE_GAME)
	{
		return;
	}
	else
	{
		GameSceneState* pState = (GameSceneState*)pScene;
		if(pState->getState() == GameSceneState::state_load)
		{
			return;
		}
	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	if (channelid == 9)//acoustic is show on mainscene == 9
	{
		MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		if (mainscene != NULL)
		{
			MainScene::chatCellInfo cellInfo = {bean.country(),bean.speakername(),bean.content(),bean.viplevel()};
			mainscene->chatInfoMarqueeVector.push_back(cellInfo);
		}
	}

	if (channelid != 4)
	{
		MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		if (mainscene != NULL)
		{
			if (mainscene->getChildByTag(kTabChat))
			{
				ChatUI * pChatUi= (ChatUI *)mainscene->getChildByTag(kTabChat);
				pChatUi->textBox->deleteAllInputString();
			}
		}
	}
	
	// chat bubble
	BaseFighter* bf = (BaseFighter*)scene->getActor(playerId);
	if(bf != NULL)
	{
		Node* pFamilyNameNode = bf->getChildByTag(PLAYER_FAMILY_NAME_TAG);
		if(pFamilyNameNode != NULL)
			bf->addChatBubble(str.c_str(), 5.0f, PLAYER_WITH_FAMILYNAME_CHATBUBBLE_OFFSETY);
		else
			bf->addChatBubble(str.c_str(), 5.0f);
	}

	// if the chat message comes from MyPlayer, show chat bubble
	//if(BaseFighter::getMyPlayerId() == playerId)
	//{
	//	BaseFighter* pRole = dynamic_cast<BaseFighter*>(scene->getActor(playerId));
	//	if(pRole != NULL)
	//		pRole->addChatBubble(str.c_str(), 3);
	//}

	// for the reason of performance ( fps )
	// when the chat main window is opened, ignore the mini chat window at the bottom of the main ui interface
	//Ŀǰ����ʲô����Լ�˵�Ļ�������뵽С����

	ChatWindows::addToMiniChatWindow(channelid,playCountry,playName.c_str(),str.c_str(),playerId,false,viplevel_,bean.listenerid(),bean.listenername());
	ChatUI * pChatUi= (ChatUI *)scene->getMainUIScene()->getChildByTag(kTabChat);
	if (pChatUi != NULL)
	{
		// update GameView::getInstance()->selectCell_vdata
		pChatUi->refreshChatUIInfoByChanelIndex(pChatUi->getChannelIndex());
		TableView* tableView = (TableView*)pChatUi->listLayer->getChildByTag(TABVIEW_TAG);
		Vec2 off_ =tableView->getContentOffset();
		int h_ = tableView->getContentSize().height + off_.y;

		tableView->reloadData();

		if(tableView->getContentSize().height > tableView->getViewSize().height)
		{
			if (pChatUi->whetherOfRoll==false)
			{
				Vec2 temp = Vec2(off_.x,h_- tableView->getContentSize().height);
				tableView->setContentOffset(temp);
			}else
			{
				tableView->setContentOffset(tableView->maxContainerOffset(), false);
			}			
		}
	}
	

	/*
	if (pChatUi==NULL)
	{
		ChatWindows::addToMiniChatWindow(channelid,playCountry,playName.c_str(),str.c_str(),playerId,false,viplevel_,bean.listenerid(),bean.listenername());
		return;
	}
	else
	{
		// update GameView::getInstance()->selectCell_vdata
		pChatUi->refreshChatUIInfoByChanelIndex(pChatUi->getChannelIndex());
		TableView* tableView = (TableView*)pChatUi->listLayer->getChildByTag(TABVIEW_TAG);
		Vec2 off_ =tableView->getContentOffset();
		int h_ = tableView->getContentSize().height + off_.y;

		tableView->reloadData();
		
  		if(tableView->getContentSize().height > tableView->getViewSize().height)
		{
			if (pChatUi->whetherOfRoll==false)
			{
				Vec2 temp = Vec2(off_.x,h_- tableView->getContentSize().height);
				tableView->setContentOffset(temp);
			}else
			{
				tableView->setContentOffset(tableView->maxContainerOffset(), false);
			}			
		}
	}
	*/
}
