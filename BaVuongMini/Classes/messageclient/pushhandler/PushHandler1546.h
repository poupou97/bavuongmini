
#ifndef Blog_C___Family_PushHandler1546_h
#define Blog_C___Family_PushHandler1546_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1546 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1546)

public:
	SYNTHESIZE(PushHandler1546, int*, m_pValue)

	PushHandler1546() ;
	virtual ~PushHandler1546() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
