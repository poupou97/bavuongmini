
#ifndef Blog_C___Reflection_PushHandler1111_h
#define Blog_C___Reflection_PushHandler1111_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

using namespace std;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1111 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1111)
public:
	SYNTHESIZE(PushHandler1111, int*, m_pValue)

		PushHandler1111() ;
	virtual ~PushHandler1111() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;
} ;

#endif