
#include "PushHandler1540.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../GameMessageProcessor.h"

IMPLEMENT_CLASS(PushHandler1540)

	PushHandler1540::PushHandler1540() 
{

}
PushHandler1540::~PushHandler1540() 
{

}
void* PushHandler1540::createInstance()
{
	return new PushHandler1540() ;
}
void PushHandler1540::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1540::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1540::handle(CommonMessage* mb)
{
	Push1540 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, instance message", mb->cmdid());

	FamilyFightData::setCurrentBattleId(bean.battleid());
	FamilyFightData::setStatus(bean.status());
	switch(bean.status())
	{
	case FamilyFightData::status_none:
		{
		}
		break;
	case FamilyFightData::status_in_sequence:
		{

		}
		break;
	case FamilyFightData::status_prepareForFight:
		{

		}
		break;
	case FamilyFightData::status_in_fight:
		{

		}
		break;
	case FamilyFightData::status_fightEnded:
		{

		}
		break;
	}

	if (GameView::getInstance()->getMainUIScene())
	{
		FamilyUI * familyUI = (FamilyUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		if (familyUI)
		{
			if(familyUI->GetFamilyFightUI())
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1544);
			}
		}
	}
}
