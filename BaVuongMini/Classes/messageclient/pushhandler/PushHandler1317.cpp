#include "PushHandler1317.h"

#include "../protobuf/ItemMessage.pb.h"  
#include "../../GameView.h"
#include "../element/FolderInfo.h"
#include "../../ui/storehouse_ui/StoreHouseUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(PushHandler1317)

PushHandler1317::PushHandler1317() 
{

}
PushHandler1317::~PushHandler1317() 
{

}
void* PushHandler1317::createInstance()
{
	return new PushHandler1317() ;
}
void PushHandler1317::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1317::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1317::handle(CommonMessage* mb)
{
	Push1317 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameView::getInstance()->storeHouseItemList.erase(GameView::getInstance()->storeHouseItemList.begin(),GameView::getInstance()->storeHouseItemList.end());

	std::vector<FolderInfo*>::iterator iter;
	for (iter = GameView::getInstance()->storeHouseItemList.begin(); iter != GameView::getInstance()->storeHouseItemList.end(); ++iter)
	{
		delete *iter;
	}
	GameView::getInstance()->storeHouseItemList.clear();

	GameView* gameView = GameView::getInstance();
	int a = bean.folders_size();
	for (int i = 0;i<bean.folders_size();i++)
	{
		FolderInfo * _folder = new FolderInfo();
		_folder->CopyFrom(bean.folders(i));
		gameView->storeHouseItemList.push_back(_folder);
	}

	// it's not game state, ignore
	Scene* pScene = Director::getInstance()->getRunningScene();
	if(pScene->getTag() != GameView::STATE_GAME)
	{
		return;
	}
	else
	{
		GameSceneState* pState = (GameSceneState*)pScene;
		if(pState->getState() == GameSceneState::state_load)
		{
			return;
		}
	}

	StoreHouseUI * storeHouseUI = (StoreHouseUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagStoreHouseUI);
	if (storeHouseUI != NULL)
	{
		storeHouseUI->ReloadData();
	}

	//add by yangjun 2013.11.8
// 	if (gameView->getGameScene()->getChildByTag(kTagShortcutLayer))
// 	{
// 		ShortcutLayer * shortcutLayer = (ShortcutLayer *)gameView->getGameScene()->getChildByTag(kTagShortcutLayer);
// 		shortcutLayer->RefreshAll();
// 	}

}
