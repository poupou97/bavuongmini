
#include "PushHandler1211.h"

#include "../protobuf/FightMessage.pb.h"
#include "../protobuf/ModelMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../GameView.h"
#include "../ProtocolHelper.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../element/CActiveRole.h"
#include "../../gamescene_state/sceneelement/HeadMenu.h"
#include "../../utils/StaticDataManager.h"
#include "../element/CFunctionOpenLevel.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../gamescene_state/sceneelement/NewFunctionRemindUI.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../gamescene_state/role/BaseFighterConstant.h"
#include "../element/CGeneralBaseMsg.h"
#include "../../gamescene_state/sceneelement/GeneralsInfoUI.h"
#include "../../ui/moneyTree_ui/MoneyTreeData.h"
#include "../../ui/GameUIConstant.h"
#include "../../ui/Reward_ui/RewardUi.h"

#define POPUP_LEVELUP_EFFECT_ZORDER 301

using namespace CocosDenshion;

IMPLEMENT_CLASS(PushHandler1211)

PushHandler1211::PushHandler1211() 
{

}
PushHandler1211::~PushHandler1211() 
{

}
void* PushHandler1211::createInstance()
{
	return new PushHandler1211() ;
}
void PushHandler1211::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1211::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1211::handle(CommonMessage* mb)
{
	Push1211 bean;
	bean.ParseFromString(mb->data());

	//CCLOG("msg: %d, player levelup", mb->cmdid());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(bean.roldid()));
	if(target == NULL)
		return;

	MyPlayer* me = GameView::getInstance()->myplayer;
	//// aptitude popup
	//if(bean.has_aptitude() && GameView::getInstance()->isOwn(bean.roldid()))
	//{
	//	Node* pEffect = AptitudePopupEffect::create(me->player->mutable_aptitude(), bean.mutable_aptitude());
	//	me->addEffect(pEffect, false, MYPLAYER_APTITUDE_EFFECT_HEIGHT);
	//}
	if (GameView::getInstance()->isOwn(bean.roldid())) 
	{
		me->player->set_experience(bean.experience());
		me->player->set_nextlevelexperience(bean.nextlevelexperience());
		me->player->set_skillpoint(bean.skillpoint());

		if (bean.has_aptitude())
		{
			me->player->mutable_aptitude()->CopyFrom(bean.aptitude());
		}

		// refresh UI
		// ReloadTarget(target) will be called in the updateFighterHPMP
		//GameView::getInstance()->getMainUIScene()->ReloadTarget(target);
	}
	ActiveRole * activeRole = dynamic_cast<ActiveRole*>(target->getActiveRole());
	if (activeRole)
	{
		activeRole->set_level(bean.currlevel());
		activeRole->set_maxhp(bean.maxhp());
		activeRole->set_maxmp(bean.maxmp());
	}
	ProtocolHelper::sharedProtocolHelper()->updateFighterHPMP(target, bean.hp(), bean.mp());

	MainScene* mainScene = scene->getMainUIScene();
 	if (mainScene != NULL)
 	{
 		if (GameView::getInstance()->isOwn(bean.roldid())) 
 		{
 			mainScene->isLevelUped = true;
 			//mainScene->addInterfaceAnm("gxsj/gxsj.anm");
 		}
 	}
	// the level up effect on the role's body
	BaseFighter* pPlayer = dynamic_cast<BaseFighter*>(scene->getActor(bean.roldid()));
	if(pPlayer != NULL && pPlayer->isMyPlayer())
	{
		//CCLegendAnimation* effect = CCLegendAnimation::create("animation/texiao/renwutexiao/SJTX/sjtx1.anm");
		//pPlayer->addEffect(effect, false, 0);

		Size winSize = Director::getInstance()->getVisibleSize();
		// alpha mask layer
		//LayerColor *mask = LayerColor::create(Color4B(0,0,0,128));
		LayerColor *mask = LayerColor::create(Color4B(0,0,0,0));
		mask->setContentSize(winSize);
		Director::getInstance()->getRunningScene()->addChild(mask, POPUP_LEVELUP_EFFECT_ZORDER);
		FiniteTimeAction*  action0 = Sequence::create(
			//Show::create(),
			FadeTo::create(0.6f, 100),
			DelayTime::create(0.3f),
			FadeOut::create(1.6f),
			RemoveSelf::create(),
			NULL);
		mask->runAction(action0);

		CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/renwushengji/lvlup.anm");
		pAnim->setScale(1.4f);
		pAnim->setPosition(Vec2(winSize.width/2,winSize.height/2));
		Director::getInstance()->getRunningScene()->addChild(pAnim, POPUP_LEVELUP_EFFECT_ZORDER);

		createLevelupText(bean.currlevel());

		// the text of "gong xi sheng ji"
		//this->createNameAction(pPlayer);

		/*
		// add effect on the foot
		CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/renwushengji/lvlup.anm");
		pAnim->setScale(1.2f);
		//CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/FUHUO/fuhuo1.anm");
		pPlayer->addEffect(pAnim, false, 0);
		*/

		GameUtils::playGameSound(MYPLAYER_UPGRADE, 2, false);
	}

	//����鿴�Ƿ��ڸõȼ��������¹��ܣ��ײ��˵����
	HeadMenu * headMenu = (HeadMenu *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHeadMenu);
	if(headMenu)
	{
		int num = 0;
		for(unsigned int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() != 1)
				continue;

			if (bean.currlevel() == FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel())
			{
				//headMenu->addNewFunction(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i));
				num++;
				if (num == 0)
				{
					headMenu->addNewFunction(headMenu,FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i));
				}
				else
				{
					FiniteTimeAction*  action = Sequence::create(
						DelayTime::create(num*0.3f),
						CallFuncN::create(CC_CALLBACK_1(HeadMenu::addNewFunction, headMenu, (void *)FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i))),
						NULL);
					headMenu->runAction(action);
				}
					
				//change by yangjun 2014.9.26
// 				if(strcmp(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str(),"btn_generals") == 0)//���佫
// 				{
// 						MainScene * mainScene = GameView::getInstance()->getMainUIScene();
// 						if (mainScene)
// 						{
// 							mainScene->initGeneral();
// 						}
// 				}
			}
		}
	}

	//����鿴�Ƿ��ڸõȼ��������¹��ܣ�1�ŵ�ͼ���
	GuideMap * guideMap = (GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if(guideMap)
	{
		for(unsigned int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() != 2)
				continue;

			if (bean.currlevel() == FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel())
			{
				guideMap->addNewFunctionForArea1(guideMap,FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i));
			}
		}
	}

	//򣩱���鿴�Ƿ��ڸõȼ��������¹��ܣ�2�ŵ�ͼ���
	if(guideMap)
	{
		int num = 0;
		for(unsigned int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() != 3)
				continue;

			if (bean.currlevel() == FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel())
			{
				num++;
				if (num == 1)
				{
					guideMap->addNewFunctionForArea2(guideMap,FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i));
				}
				else
				{

					FiniteTimeAction*  action = Sequence::create(
						DelayTime::create(num*0.5f),
						CallFuncN::create(CC_CALLBACK_1(GuideMap::addNewFunctionForArea2, guideMap, (void *)FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i))),
						NULL);
					guideMap->runAction(action);
				}

			}
		}
	}

	//򣩰��ȼ��������ܿ���
	ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	if (shortcutLayer)
	{
		shortcutLayer->addNewShortcutSlot();
	}

	//�Ƿ������佫ͷ�
	GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI *)GameView::getInstance()->getMainUIScene()->getGeneralInfoUI();
	if(generalsInfoUI)
	{
		//���ȼ�
		int openlevel = 0;
		//�����һ���佫��ӦID   8   һ����
		for (int i = 0;i<5;i++)
		{
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(8+i);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // �û�ҵ�����ָ�END��  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[8+i];
			}

			if (openlevel == bean.currlevel())
			{
				generalsInfoUI->RefreshAllGeneralItem();
			}
		}
	}

	//��¹�����ʾ
	if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNewFunctionRemindUI))
	{
		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNewFunctionRemindUI)->removeFromParent();
	}

	Size winSize = Director::getInstance()->getVisibleSize();
	NewFunctionRemindUI* newFunctionRemindUI = NewFunctionRemindUI::create(bean.currlevel());
	if (newFunctionRemindUI)
	{
		newFunctionRemindUI->setIgnoreAnchorPointForPosition(false);
		newFunctionRemindUI->setAnchorPoint(Vec2(0.5f,0.5f));
		newFunctionRemindUI->setPosition(Vec2(winSize.width/2,winSize.height/2));
		newFunctionRemindUI->setTag(kTagNewFunctionRemindUI);
		GameView::getInstance()->getMainUIScene()->addChild(newFunctionRemindUI,ZOrder_ToolTips);
	}
	
	int roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
	for (unsigned int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();i++)
	{
		if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() == 2)
		{
			if (strcmp(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str(),"btn_remind") == 0)
			{
				if ( GameView::getInstance()->myplayer->getActiveRole()->level() >= FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel())
				{
					GameView::getInstance()->getMainUIScene()->setRemindOpened(true);
					break;
				}
			}
		}
	}
	mainScene->checkIsNewRemind();

	/**
	  * the fight point effect will be activated by general's fight point change, so ignore here
	  **/
	//int m_old_AllFightPoint = GameView::getInstance()->myplayer->player->fightpoint();
	//for (unsigned int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	//{
	//	m_old_AllFightPoint += GameView::getInstance()->generalsInLineList.at(i)->fightpoint();
	//}
	//int m_new_AllFightPoint = bean.fightpoint();
	//for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	//{
	//	m_new_AllFightPoint += GameView::getInstance()->generalsInLineList.at(i)->fightpoint();
	//}
	//if (m_old_AllFightPoint != m_new_AllFightPoint && GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFightPointChange)==NULL)
	//{
	//	FightPointChangeEffect * fightEffect_ = FightPointChangeEffect::create(m_old_AllFightPoint,m_new_AllFightPoint);
	//	if (fightEffect_)
	//	{
	//		fightEffect_->setTag(kTagFightPointChange);
	//		fightEffect_->setLocalZOrder(SCENE_STORY_LAYER_BASE_ZORDER);
	//		GameView::getInstance()->getMainUIScene()->addChild(fightEffect_);
	//	}
	//}

	GameView::getInstance()->myplayer->player->set_fightpoint(bean.fightpoint());

	//when level is up, robot is change the drug hp mp 
	if (bean.currlevel() == 15)
	{
		MyPlayerAIConfig::reSetRobotUseDrug(bean.currlevel());
	}

	//money tree reward
// 	if (GameView::getInstance()->myplayer->getActiveRole()->level() == 20)
// 	{
// 		if (!MoneyTreeData::instance()->hasAllGiftGet())
// 		{
// 			RewardUi::addRewardListEvent(REWARD_LIST_ID_MONEYTREE);
// 		}
// 	}
}

void PushHandler1211::createNameAction( BaseFighter* pBaseFighter )
{
	if (NULL == pBaseFighter)
	{
		return;
	}

	// add name action
	Node* name_node = Node::create();
	name_node->setCascadeOpacityEnabled(true);

	/*
	Sprite* spr = Sprite::create("res_ui/generalname_di.png");
	spr->setAnchorPoint(Vec2(0.5f,0.5f));
	name_node->addChild(spr);

	// � generals_strategies_upgrade
	const char *str_update_tmp = StringDataManager::getString("generals_strategies_upgrade");
	std::string str_update = "";
	str_update.append(str_update_tmp);
	str_update.append("!");

	CCLabel* skillNameLabel = CCLabel::create(str_update.c_str(), "res_ui/font/ziti_3.fnt");
	skillNameLabel->setAnchorPoint(Vec2(0.5f,0.9f));
	name_node->addChild(skillNameLabel);
	*/

	CCLegendAnimation* pAnim = CCLegendAnimation::create("animation/texiao/jiemiantexiao/gxsj/gxsj.anm");
	pAnim->setPlayLoop(true);
	name_node->addChild(pAnim);

	Action* spawn_action = Spawn::create(
		FadeOut::create(0.25f),
		ScaleTo::create(0.25f, 2.0f),
		NULL);

	Action*  action = Sequence::create(
		MoveBy::create(0.5f, Vec2(0, 65)),
		DelayTime::create(0.5f),
		spawn_action,
		RemoveSelf::create(),
		NULL);
	name_node->runAction(action);
	
	// add animation to scene layer
	//name_node->setPosition(Vec2(pBaseFighter->getPositionX(), pBaseFighter->getPositionY() + 5));
	//pBaseFighter->getGameScene()->getActorLayer()->addChild(name_node, SCENE_TOP_LAYER_BASE_ZORDER);

	// add animation to UI layer
	if(pBaseFighter->isMyPlayer())
	{
		//Node* sceneLayer = pBaseFighter->getGameScene()->getChildByTag(GameSceneLayer::kTagSceneLayer);
		//Vec2 screenPos = pBaseFighter->getPosition() + sceneLayer->getPosition();
		//name_node->setPosition(screenPos);

		Size winSize = Director::getInstance()->getVisibleSize();
		name_node->setPosition(Vec2(winSize.width/2,winSize.height/2));

		Director::getInstance()->getRunningScene()->addChild(name_node, POPUP_LEVELUP_EFFECT_ZORDER);
	}
}

void PushHandler1211::createLevelupText(int level)
{
	Size winSize = Director::getInstance()->getVisibleSize();

	Node* pTextNode = Node::create();
	pTextNode->setCascadeOpacityEnabled(true);
	//char level_up_text[30];
	//const char* templateStr = StringDataManager::getString("role_level_up");
	//sprintf(level_up_text, templateStr, bean.currlevel());
	const char* font_name = "res_ui/font/ziti_14.fnt";
	// text 1
	const char* lvlup_1_str = StringDataManager::getString("role_level_up_1");
	auto lvlup_label_1 = Label::createWithBMFont(font_name,lvlup_1_str );
	lvlup_label_1->setAnchorPoint(Vec2(0, 0));
	lvlup_label_1->setPosition(Vec2(0, 0));
	pTextNode->addChild(lvlup_label_1);
	// level num
	char level_num[5];
	sprintf(level_num, "%d", level);
	auto lvlup_num = Label::createWithBMFont("res_ui/font/ziti_14.fnt", level_num);
	const float lvlup_num_scale = 1.35f;
	lvlup_num->setScale(lvlup_num_scale);
	lvlup_num->setAnchorPoint(Vec2(0,0));
	lvlup_num->setPosition(Vec2(lvlup_label_1->getContentSize().width, -8.0f));
	float lvlup_num_width = lvlup_num->getContentSize().width * lvlup_num_scale;
	float lvlup_num_height = lvlup_num->getContentSize().height * lvlup_num_scale;
	pTextNode->addChild(lvlup_num);
	// text 2
	const char* lvlup_2_str = StringDataManager::getString("role_level_up_2");
	auto lvlup_label_2 = Label::createWithBMFont(font_name, lvlup_2_str);
	lvlup_label_2->setAnchorPoint(Vec2(0, 0));
	lvlup_label_2->setPosition(Vec2((lvlup_label_1->getContentSize().width+lvlup_num_width), 0));
	pTextNode->addChild(lvlup_label_2);

	pTextNode->setContentSize(Size(lvlup_label_1->getContentSize().width + lvlup_num_width + lvlup_label_2->getContentSize().width, lvlup_num_height));
	pTextNode->setPosition(Vec2(winSize.width/2, winSize.height/2));
	pTextNode->setAnchorPoint(Vec2(0.5f, 0.5f));
	pTextNode->setScale(2.0f);

	Spawn* spawnAction = Spawn::create(
		MoveBy::create(1.0f, Vec2(0, 600)),
		FadeOut::create(0.85f),
		NULL);
	pTextNode->runAction(Sequence::create(
		DelayTime::create(0.1f),
		Show::create(),
		ScaleTo::create(0.3f, 1.0f),
		DelayTime::create(1.8f),
		//FadeOut::create(0.9f),
		//MoveBy::create(0.8f, Vec2(0, 600)),
		spawnAction,
		RemoveSelf::create(),
		NULL));
	Director::getInstance()->getRunningScene()->addChild(pTextNode, POPUP_LEVELUP_EFFECT_ZORDER);
}