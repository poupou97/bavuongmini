
#ifndef Blog_C___CheckOtherEquip_PushHandler1142_h
#define Blog_C___CheckOtherEquip_PushHandler1142_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1142 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1142)
    
public:
    SYNTHESIZE(PushHandler1142, int*, m_pValue)
    
    PushHandler1142() ;
    virtual ~PushHandler1142() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
