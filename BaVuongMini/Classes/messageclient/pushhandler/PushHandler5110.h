
#ifndef Blog_C___Reflection_PushHandler5110_h
#define Blog_C___Reflection_PushHandler5110_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5110 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5110)

public:
	SYNTHESIZE(PushHandler5110, int*, m_pValue)

		PushHandler5110() ;
	virtual ~PushHandler5110() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
