#include "PushHandler1121.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Chat_ui/ChatCell.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/ChatWindows.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/Chat_ui/ChatUI.h"
#include "../../ui/Chat_ui/Tablist.h"
#include "../../utils/StrUtils.h"
#include "../../ui/vip_ui/VipDetailUI.h"
//#include "../../gamescene_state/role/ActorUtils.h"

IMPLEMENT_CLASS(PushHandler1121)

PushHandler1121::PushHandler1121() 
{

}
PushHandler1121::~PushHandler1121() 
{

}
void* PushHandler1121::createInstance()
{
	return new PushHandler1121() ;
}
void PushHandler1121::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1121::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1121::handle(CommonMessage* mb)
{
	Push1121 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int gold_ = bean.wallet().gold();
	int goldBing_ = bean.wallet().bindgold();

	int goldIngot_ = bean.wallet().goldingot();
	int goldIngotBind_ = bean.wallet().bindgoldingot();

	int goldAll = gold_+ goldBing_;
	//int goldIngotAll = goldIngot_+ goldIngotBind_;

	///���Ǯ�ҵı䶯��ʾ
	std::string strings = "";
	int nChannelId = 4;//ϵͳ��Ϣ
	if (bean.wallet().firstflag() != 1)
	{
		/*
		if (goldAll - GameView::getInstance()->getPlayerGold() >100 )
		{
			const char * str1_  = StringDataManager::getString("getPlayerAdd");
			const char * str2_  = StringDataManager::getString("getPlayerAddOfGold");

			char diffStr[20];
			sprintf(diffStr,"%d",(goldAll - GameView::getInstance()->getPlayerGold()));

			strings.append(str1_);
			strings.append(diffStr);
			strings.append(str2_);

			ChatWindows::addToMiniChatWindow(nChannelId,"","",strings,NULL,false,0,0,"");
		}
		*/
		if(goldAll < GameView::getInstance()->getPlayerGold())
		{
			const char * str1_  = StringDataManager::getString("getPlayerCost");
			const char * str2_  = StringDataManager::getString("getPlayerAddOfGold");

			char diffStr[20];
			sprintf(diffStr,"%d",( GameView::getInstance()->getPlayerGold() - goldAll));

			strings.append(str1_);
			strings.append(diffStr);
			strings.append(str2_);
			ChatWindows::addToMiniChatWindow(nChannelId,"","",strings,NULL,false,0,0,"");
		}
		
		if (goldIngot_ > GameView::getInstance()->getPlayerGoldIngot() )
		{
			const char * str1_  = StringDataManager::getString("getPlayerAdd");
			//const char * str2_  = StringDataManager::getString("getPlayerAddOfGoldIngot");
			const char * str2_  = "/e40";   // /e40 is the icon the ingot

			char diffStr[20];
			sprintf(diffStr,"%d",(goldIngot_ - GameView::getInstance()->getPlayerGoldIngot()));

			std::string _str = "";
			_str.append(str1_);
			_str.append(diffStr);
			_str.append(str2_);

			//ChatWindows::addToMiniChatWindow(nChannelId,"","",_str,NULL,false,0,0,"");
			GameView::getInstance()->showAlertDialog(_str.c_str());
		}
		
		/*
		if(goldIngot_ < GameView::getInstance()->getPlayerGoldIngot() )
		{
			const char * str1_  = StringDataManager::getString("getPlayerCost");
			const char * str2_  = StringDataManager::getString("getPlayerAddOfGoldIngot");

			char diffStr[20];
			sprintf(diffStr,"%d",(GameView::getInstance()->getPlayerGoldIngot() - goldIngot_));

			strings.append(str1_);
			strings.append(diffStr);
			strings.append(str2_);
			ChatWindows::addToMiniChatWindow(nChannelId,"","",strings,NULL,false,0,0,"");
		}
		*/
		
		if (goldIngotBind_ > GameView::getInstance()->getPlayerBindGoldIngot() )
		{
			const char * str1_  = StringDataManager::getString("getPlayerAdd");
			//const char * str2_  = StringDataManager::getString("getPlayerAddOfBingGoldIngot");
			const char * str2_  = "/e41";   // /e41 is the icon the bind_ingot

			char diffStr[20];
			sprintf(diffStr,"%d",(goldIngotBind_ - GameView::getInstance()->getPlayerBindGoldIngot()));

			std::string _str = "";
			_str.append(str1_);
			_str.append(diffStr);
			_str.append(str2_);
			//ChatWindows::addToMiniChatWindow(nChannelId,"","",_str,NULL,false,0,0,"");
			GameView::getInstance()->showAlertDialog(_str.c_str());
		}
		
		if(goldIngotBind_ < GameView::getInstance()->getPlayerBindGoldIngot() )
		{
			const char * str1_  = StringDataManager::getString("getPlayerCost");
			const char * str2_  = StringDataManager::getString("getPlayerAddOfBingGoldIngot");

			char diffStr[20];
			sprintf(diffStr,"%d",(GameView::getInstance()->getPlayerBindGoldIngot() - goldIngotBind_));

			strings.append(str1_);
			strings.append(diffStr);
			strings.append(str2_);
			ChatWindows::addToMiniChatWindow(nChannelId,"","",strings,NULL,false,0,0,"");
		}

		int stringsSize = strlen(strings.c_str());
		if (stringsSize > 0)
		{
			ChatUI::addToMainChatUiMessage(nChannelId,strings);
		}
	}

	int nGotGold = goldAll - GameView::getInstance()->getPlayerGold();
	//if(nGotGold > 0)
	//	ActorUtils::addGoldMessageToScene(nGotGold);

	// show gold by alert
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	bool isShopOpened = false;
	if(scene != NULL && scene->getMainUIScene()->getChildByTag(kTagShopUI) != NULL)
		isShopOpened = true;
	if ((scene != NULL && nGotGold >= 100) || (isShopOpened && nGotGold > 0))
	{
		std::string wholeString = StringDataManager::getString("mission_self_recieved");
		
		char goldStr[10];
		sprintf(goldStr, "%d", nGotGold);

		wholeString.append(StrUtils::applyColor(goldStr, Color3B(255, 0, 0)));
		//wholeString.append(StringDataManager::getString("getPlayerAddOfGold"));
		wholeString.append("/e39");   // /e39 is the icon of the gold

		GameView::getInstance()->showAlertDialog(wholeString.c_str());
	}

	GameView::getInstance()->setPlayerGold(goldAll);
	GameView::getInstance()->setPlayerGoldIngot(goldIngot_);
	GameView::getInstance()->setPlayerBindGoldIngot(goldIngotBind_); 

	if(scene != NULL)
	{
		MainScene * mainScene = (MainScene *)scene->getMainUIScene();
		if (mainScene != NULL)
		{
			mainScene->remindOfSkill();

			VipDetailUI * vipDetailUI = (VipDetailUI*)mainScene->getChildByTag(kTagVipDetailUI);
			if (vipDetailUI)
			{
				vipDetailUI->refreshInGotValue();
			}
		}
	}
}
