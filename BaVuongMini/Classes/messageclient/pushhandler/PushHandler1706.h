
#ifndef Blog_C___Reflection_PushHandler1706_h
#define Blog_C___Reflection_PushHandler1706_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1706 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1706)

public:
	SYNTHESIZE(PushHandler1706, int*, m_pValue)

		PushHandler1706() ;
	virtual ~PushHandler1706() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
