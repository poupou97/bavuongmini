
#include "PushHandler3002.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../protobuf/OfflineFight.pb.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../element/CHonorCommodity.h"
#include "../element/CPartner.h"
#include "../../ui/offlinearena_ui/RankListUI.h"
#include "../element/CSimplePartner.h"

IMPLEMENT_CLASS(PushHandler3002)

PushHandler3002::PushHandler3002() 
{

}
PushHandler3002::~PushHandler3002() 
{

}
void* PushHandler3002::createInstance()
{
	return new PushHandler3002() ;
}
void PushHandler3002::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler3002::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler3002::handle(CommonMessage* mb)
{
	ResRanking3002 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (GameView::getInstance()->getGameScene() == NULL)
		return;

	if (GameView::getInstance()->getMainUIScene() == NULL)
		return;

	RankListUI * rankListUI = (RankListUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagRankListUI);
	if (rankListUI == NULL)
		return;

	//refreshData
	if (rankListUI->isReqNewly)
	{
		std::vector<CSimplePartner *>::iterator iter;
		for (iter = rankListUI->curRankingList.begin(); iter != rankListUI->curRankingList.end(); ++iter)
		{
			delete *iter;
		}
		rankListUI->curRankingList.clear();
	}
		
	for(int i = 0;i<bean.ranking_size();++i)
	{
		CSimplePartner * temp = new CSimplePartner();
		temp->CopyFrom(bean.ranking(i));
		rankListUI->curRankingList.push_back(temp);
	}

	int totalPage = bean.total();
	if (bean.total() > 5)
	{
		totalPage = 5;
	}

	rankListUI->RefreshGeneralsListStatus(bean.page(),totalPage);
	rankListUI->RefreshDataWithOutChangeOffSet();
	rankListUI->setIsCanReq(true);
}
