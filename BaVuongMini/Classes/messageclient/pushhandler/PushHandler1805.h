
#ifndef Blog_C___Reflection_PushHandler1805_h
#define Blog_C___Reflection_PushHandler1805_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1805 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1805)
    
public:
    SYNTHESIZE(PushHandler1805, int*, m_pValue)
    
    PushHandler1805() ;
    virtual ~PushHandler1805() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
