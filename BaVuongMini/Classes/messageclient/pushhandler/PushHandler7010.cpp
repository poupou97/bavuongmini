
#include "PushHandler7010.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../legend_script/ScriptManager.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(PushHandler7010)

	PushHandler7010::PushHandler7010() 
{

}
PushHandler7010::~PushHandler7010() 
{

}
void* PushHandler7010::createInstance()
{
	return new PushHandler7010() ;
}
void PushHandler7010::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler7010::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler7010::handle(CommonMessage* mb)
{
	PushMissionScript7010 bean;
	bean.ParseFromString(mb->data());

	// script name
	std::string name = bean.name();
	std::string scriptFileName = "script/";
	scriptFileName.append(name);
	scriptFileName.append(".sc");
	//CCLOG("msg: %d, mission script: %s", mb->cmdid(), scriptFileName.c_str());

	// script parameter
	std::string para = bean.para();
	if(bean.has_para())
	{
		// todo
	}

// 	if (GameView::getInstance())
// 	{
// 		if (GameView::getInstance()->getGameScene())
// 		{
// 			if (GameView::getInstance()->myplayer->getActiveRole()->level() <= K_GuideForMainMission_Level)
// 			{
// 				MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
// 				if (mainScene)
// 				{
// 					MissionAndTeam * missionAndTeam = (MissionAndTeam*)mainScene->getChildByTag(kTagMissionAndTeam);
// 					if (missionAndTeam)
// 					{
// 						missionAndTeam->removeGuidePen();
// 					}
// 				}
// 			}
// 		}
// 	}
	ScriptManager::getInstance()->runScript(scriptFileName);
}
