#include "PushHandler5120.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"


IMPLEMENT_CLASS(PushHandler5120)

	PushHandler5120::PushHandler5120() 
{

}
PushHandler5120::~PushHandler5120() 
{

}
void* PushHandler5120::createInstance()
{
	return new PushHandler5120() ;
}
void PushHandler5120::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5120::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5120::handle(CommonMessage* mb)
{
	Push5120 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (!GameView::getInstance())
		return;

	GameView::getInstance()->myplayer->setMusouEssence(bean.dragonvalue());
	//delete old
	GameView::getInstance()->m_musouTalentList.clear();
	//add new
	for(int i = 0;i<bean.talent_size();++i)
	{
		GameView::getInstance()->m_musouTalentList.insert(make_pair(bean.talent(i).talentid(),bean.talent(i).level()));
	}

}