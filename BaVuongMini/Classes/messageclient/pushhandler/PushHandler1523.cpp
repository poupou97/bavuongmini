
#include "PushHandler1523.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/extensions/CCRichLabel.h"



IMPLEMENT_CLASS(PushHandler1523)

PushHandler1523::PushHandler1523() 
{
    
}
PushHandler1523::~PushHandler1523() 
{
    
}
void* PushHandler1523::createInstance()
{
    return new PushHandler1523() ;
}
void PushHandler1523::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1523::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1523::handle(CommonMessage* mb)
{
	Rsp1523 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int type_ = bean.type();
	if (type_==0)
	{
		const char *str_ = StringDataManager::getString("family_notice_success_of");	
		GameView::getInstance()->showAlertDialog(str_);
		FamilyUI *familyui =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		if (familyui != NULL)
		{
			familyui->familyManifestoLabel->setString(familyui->familyNotice);
			int hh_ =familyui->familyManifestoLabel->getContentSize().height;
			if (hh_ < familyui->manifestoScrollView->getContentSize().height)
			{
				hh_ =familyui->manifestoScrollView->getContentSize().height;
			}
			familyui->manifestoScrollView->setContentSize(Size(270,hh_));
			familyui->familyManifestoLabel->setPosition(Vec2(0,familyui->manifestoScrollView->getContentSize().height - familyui->familyManifestoLabel->getContentSize().height-10));
		}
	
	}else
	{
		const char *str_ = StringDataManager::getString("family_notice_fail_of");
		GameView::getInstance()->showAlertDialog(str_);
	}
}
