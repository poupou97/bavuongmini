#ifndef Blog_C___Reflection_PushHandler2206_h
#define Blog_C___Reflection_PushHandler2206_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler2206 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler2206)
    
public:
    SYNTHESIZE(PushHandler2206, int*, m_pValue)
    
    PushHandler2206() ;
    virtual ~PushHandler2206() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
