
#ifndef Blog_C___Reflection_PushHandler5303_h
#define Blog_C___Reflection_PushHandler5303_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5303 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5303)

public:
	SYNTHESIZE(PushHandler5303, int*, m_pValue)

		PushHandler5303() ;
	virtual ~PushHandler5303() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
