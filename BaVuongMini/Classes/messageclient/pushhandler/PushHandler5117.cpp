#include "PushHandler5117.h"
#include "../protobuf/ActiveMessage.pb.h"
#include "../../ui/Active_ui/ActiveData.h"
#include "../element/CActiveLabel.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../../ui/Chat_ui/ChatCell.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/sceneelement/ChatWindows.h"
#include "../../ui/Chat_ui/Tablist.h"
#include "../../ui/Chat_ui/ChatUI.h"

IMPLEMENT_CLASS(PushHandler5117)

PushHandler5117::PushHandler5117() 
{

}
PushHandler5117::~PushHandler5117() 
{
	
}
void* PushHandler5117::createInstance()
{
	return new PushHandler5117() ;
}
void PushHandler5117::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5117::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5117::handle(CommonMessage* mb)
{
	Push5117 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//// �� ձ�����������
	//ActiveData::instance()->clearActiveLabel();

	//// ݱ����������ݵ� ActiveData
	//int nActiveLabelSize = bean.label_size();
	//for (int i = 0; i < nActiveLabelSize; i++)
	//{
	//	CActiveLabel* activeLabel = new CActiveLabel();
	//	activeLabel->CopyFrom(bean.label(i));

	//	ActiveData::instance()->m_vector_internet_label.push_back(activeLabel);
	//}

	//// ��� ·������� ݵ� native_note
	//ActiveData::instance()->initNoteFromInternet();
	
	// Ҫ���µ���
	CActiveLabel * activeLabel = new CActiveLabel();
	activeLabel->CopyFrom(bean.label());

	// ݸ��·��������
	unsigned int nLabelSize = ActiveData::instance()->m_vector_internet_label.size();
	for (unsigned int i = 0; i < nLabelSize; i++)
	{
		if (activeLabel->id() == ActiveData::instance()->m_vector_internet_label.at(i)->id())
		{
			ActiveData::instance()->m_vector_internet_label.at(i) = activeLabel;
		}
	}

	// ���һ�Ծ�
	int nOldActive = ActiveData::instance()->getUserActive();
	int nNewActive = bean.number();
	int nGetActive = nNewActive - nOldActive;

	// ȸ��»�Ծ�
	ActiveData::instance()->setUserActive(bean.number());

	// ȸ��½��ջ�Ծ�
	ActiveData::instance()->set_todayActive(bean.numbertoday());

	// ȸ�� ·�������ݵ� native_label
	ActiveData::instance()->initLabelFromInternet();

	// ֪ͨ���´��
	MainScene* mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	if(mainLayer->getChildByTag(kTagActiveUI) != NULL)
	{
		ActiveUI * activeUI = (ActiveUI *)(mainLayer->getChildByTag(kTagActiveUI));
		if (NULL != activeUI)
		{
			activeUI->initLabelFromInternet();
			activeUI->initUserActiveFromInternet();
			activeUI->initTodayActiveFromInternet();
			activeUI->refrehTableView();

			if (0 == nGetActive)
			{
				const char * charInfo = StringDataManager::getString("activy_refresh");
				GameView::getInstance()->showAlertDialog(charInfo);
			}
		}
	}

	// 嵱��һ�û�Ծ��Ϊ0ʱ�����·���Ϣ����û�Ծ��Ϊ0˵�������»��
	if (0 == nGetActive)
	{
		return;
	}

	// ϵͳƵ����Ϣ����� ͻ�ö��ٻ�Ծ�
	int nChannelId = 4;				// �ϵͳƵ�

	const char * str_chat1 = StringDataManager::getString("activy_chartInfo1");
	char * str_chatTmp1 = const_cast<char*>(str_chat1);

	const char * str_chat2 = StringDataManager::getString("activy_chartInfo2");
	char * str_chatTmp2 = const_cast<char*>(str_chat2);

	char str_activeNum[4];
	sprintf(str_activeNum, "%d", nGetActive);

	std::string str_activeInfo = "";
	str_activeInfo.append(str_chatTmp1);
	str_activeInfo.append(str_activeNum);
	str_activeInfo.append(str_chatTmp2);

	// ��ΪshowAlert 
	GameView::getInstance()->showAlertDialog(str_activeInfo);

	// �˶δ���Ϊ�������� ͻ�ȡ��Ծ�ȵ���ʾ��Ϣ
	/*----------------------------------------------------------------------------

	// cache chat cell(���)壨����巢�ͻ�û�Ծ�ȵ���Ϣ��
	if (strlen(str_activeInfo.c_str()) > 0)
	{
		ChatUI::addToMainChatUiMessage(nChannelId,str_activeInfo);
	}
	

	// ��С���巢�ͻ�û�Ծ�ȵ���Ϣ
	// for the reason of performance ( fps )
	// when the chat main window is opened, ignore the mini chat window at the bottom of the main ui interface
	if(mainLayer->getChildByTag(kTabChat) == NULL)
	{
		ChatWindows::addToMiniChatWindow(nChannelId,"","",str_activeInfo,NULL,false,0,0,"");
	}
	
	----------------------------------------------------------------------------*/
}