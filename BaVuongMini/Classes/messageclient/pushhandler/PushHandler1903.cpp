#include "PushHandler1903.h"

#include "../protobuf/CopyMessage.pb.h"
#include "../../ui/challengeRound/ShowLevelUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/CTowerHistory.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../ui/challengeRound/SingCopyInstance.h"


IMPLEMENT_CLASS(PushHandler1903)

PushHandler1903::PushHandler1903() 
{

}
PushHandler1903::~PushHandler1903() 
{

}
void* PushHandler1903::createInstance()
{
	return new PushHandler1903() ;
}
void PushHandler1903::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1903::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1903::handle(CommonMessage* mb)
{
	ResTowerHistory1903 bean;
	bean.ParseFromString(mb->data());
	
	ShowLevelUi * levelui = (ShowLevelUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopylevel);
	if (levelui)
	{
		char maxLevelStr[10];
		sprintf(maxLevelStr,"%d",bean.maxlevel());
		levelui->labelTodayMaxLevel->setString(maxLevelStr);

		int m_useTime = bean.usetime()/1000;
		std::string useTime_str =  RecuriteActionItem::timeFormatToString(m_useTime);
		levelui->labelTodayMaxUsetime->setString(useTime_str.c_str());

		for (int i=0;i<bean.history_size();i++)
		{
			CTowerHistory * temp_= new CTowerHistory();
			temp_->CopyFrom(bean.history(i));
			levelui->singCopyHistoryVector.push_back(temp_);
		}	

		levelui->setTodaymaxlevel(bean.maxlevel());

		int temp_page = levelui->singCopyHistoryVector.size()/levelui->m_everyPageNum;
		int temp_pageNum = levelui->singCopyHistoryVector.size()%levelui->m_everyPageNum;
		
		SingCopyInstance::setMaxLevel(bean.totallevel());

		int maxPage_num = bean.totallevel()/levelui->m_everyPageNum;
		if (bean.totallevel()%levelui->m_everyPageNum > 0)
		{
			maxPage_num++;
		}

		if (temp_page >=maxPage_num )
		{
			levelui->addPageViewIndex(temp_page);
		}else
		{
			levelui->addPageViewIndex(temp_page+1);
		}
	}
	//SingCopyInstance::getInstance()->setMaxLevel(bean.maxlevel());
}