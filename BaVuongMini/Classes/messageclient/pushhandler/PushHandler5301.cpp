#include "PushHandler5301.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../protobuf/WorldBossMessage.pb.h"
#include "../../ui/Active_ui/WorldBoss_ui/WorldBossData.h"
#include "../element/CBoss.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../../ui/Active_ui/WorldBoss_ui/WorldBossUI.h"

IMPLEMENT_CLASS(PushHandler5301)

	PushHandler5301::PushHandler5301() 
{

}
PushHandler5301::~PushHandler5301() 
{

}

void* PushHandler5301::createInstance()
{
	return new PushHandler5301() ;
}
void PushHandler5301::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5301::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5301::handle(CommonMessage* mb)
{
	ResBossList5301 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// update data
	WorldBossData::getInstance()->clearBossDataList();
	for(int i = 0 ;i < bean.list_size();++i)
	{
		CBoss * tempBoss = new CBoss();
		tempBoss->CopyFrom(bean.list(i));
		WorldBossData::getInstance()->m_worldBossDataList.push_back(tempBoss);
	}

	//refresh ui
	if (!GameView::getInstance())
		return;

	if (!GameView::getInstance()->getGameScene())
		return;

	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (!mainscene)
		return;

	WorldBossUI * worldBossUI = (WorldBossUI*)mainscene->getChildByTag(kTagWorldBossUI);
	if (worldBossUI)
	{
		worldBossUI->RefreshTableView();
	}
}

