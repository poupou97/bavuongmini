
#ifndef Blog_C___Reflection_PushHandler5149_h
#define Blog_C___Reflection_PushHandler5149_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"

USING_NS_CC;

class PushHandler5149 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5149)

public:
	SYNTHESIZE(PushHandler5149, int*, m_pValue)

	PushHandler5149() ;
	virtual ~PushHandler5149() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);


protected:
	int *m_pValue ;
} ;

#endif
