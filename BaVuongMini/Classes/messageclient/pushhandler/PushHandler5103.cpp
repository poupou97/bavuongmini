#include "PushHandler5103.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../element/CPkDailyReward.h"
#include "../../ui/backpackscene/BattleAchievementData.h"
#include "../../ui/backpackscene/BattleAchievementUI.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../newcomerstory/NewCommerStoryManager.h"

IMPLEMENT_CLASS(PushHandler5103)

PushHandler5103::PushHandler5103() 
{

}
PushHandler5103::~PushHandler5103() 
{
	
}
void* PushHandler5103::createInstance()
{
	return new PushHandler5103() ;
}
void PushHandler5103::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5103::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5103::handle(CommonMessage* mb)
{
	Rsp5103 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if(bean.result())
	{
		GameView::getInstance()->showAlertDialog(bean.countrymessage().c_str());
		NewCommerStoryManager::getInstance()->setIsSelectCountryFinished(true);
	}
	else
	{
		GameView::getInstance()->showAlertDialog(bean.countrymessage().c_str());
	}
}