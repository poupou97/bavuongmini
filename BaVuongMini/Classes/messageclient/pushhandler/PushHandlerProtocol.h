#ifndef __PUSHHANDLER_PROTOCOL_H__
#define __PUSHHANDLER_PROTOCOL_H__

#include "../GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

/**
 * Common interface for PushHandler
 */
class PushHandlerProtocol
{
public:
	virtual void handle(CommonMessage* mb) = 0;
};

#endif