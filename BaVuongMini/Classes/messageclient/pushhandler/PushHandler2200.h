
#ifndef Blog_C___Reflection_PushHandler2200_h
#define Blog_C___Reflection_PushHandler2200_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "../protobuf/MainMessage.pb.h"
#include "../protobuf/PlayerMessage.pb.h"

#include "cocos2d.h"
#include "cocos-ext.h"
USING_NS_CC;
USING_NS_CC_EXT;
class PushHandler2200 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler2200)
    
public:
    SYNTHESIZE(PushHandler2200, int*, m_pValue)
    
    PushHandler2200() ;
    virtual ~PushHandler2200() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
