#include "PushHandler5138.h"
#include "../protobuf/DailyGift.pb.h"
#include "../../utils/StaticDataManager.h"
#include "GameView.h"
#include "../../ui/moneyTree_ui/MoneyTreeData.h"
#include "../../ui/moneyTree_ui/MoneyTreeTimeManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/moneyTree_ui/MoneyTreeUI.h"
#include "../../ui/Reward_ui/RewardUi.h"
#include "../element/CRewardBase.h"
#include "../../ui/GameUIConstant.h"

IMPLEMENT_CLASS(PushHandler5138)

PushHandler5138::PushHandler5138() 
{

}
PushHandler5138::~PushHandler5138() 
{
	
}
void* PushHandler5138::createInstance()
{
	return new PushHandler5138() ;
}
void PushHandler5138::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5138::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5138::handle(CommonMessage* mb)
{
	Rsp5138 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int nTargetCount = bean.number();					// ����ָ���ĵڼ��
	int nStatus = bean.status();						// �״̬��0ʧ�ܣ�1�ɹ�
	int nNeedTime = bean.time();						// ��һ����Ҫ��ʱ�䣨���룩
	
	if (1 == nStatus)
	{
		int nMaxCount = MoneyTreeData::instance()->get_maxNum();
		if (nMaxCount == nTargetCount)		// moneytree times is == 0
		{
			//remove reward list of moneyTree
			RewardUi::removeRewardListEvent(REWARD_LIST_ID_MONEYTREE);

			this->initData(nTargetCount, 0);
		}
		else
		{
			this->initData(nTargetCount, nNeedTime);

			for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
			{
				if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_MONEYTREE)
				{
					GameView::getInstance()->rewardvector.at(i)->setRewardState(0);
					RewardUi::setRewardListParticle();

					if (GameView::getInstance()->getMainUIScene())
					{
						RewardUi * reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
						if (reward_ui != NULL)
						{
							reward_ui->tablevie_reward->updateCellAtIndex(i);
						}
					}
				}
			}
		}
		
	}
	else if (0 == nStatus)
	{
		const char * str_errorInfo = StringDataManager::getString("MoneyTree_errorInfo");
		char * str_tmp_errorInfo = const_cast<char *>(str_errorInfo);
		GameView::getInstance()->showAlertDialog(str_tmp_errorInfo);
	}
}

void PushHandler5138::initData( int nTargetNum, int nNeedTime )
{
	// ���nCurrentCount
	MoneyTreeData::instance()->set_currentCount(nTargetNum + 1);

	// ø��µ���ʱʱ�
	MoneyTreeData::instance()->set_needTime(nNeedTime);

	// refresh moneyTree ui
	// refresh moneyTreeTimeManager

	if(NULL != GameView::getInstance()->getMainUIScene())
	{
		// �����������ͬ��
		MoneyTreeTimeManager * pMoneyTreeTimeManager = (MoneyTreeTimeManager *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMoneyTreeManager);
		if (NULL != pMoneyTreeTimeManager)
		{
			pMoneyTreeTimeManager->initDataFromIntent();
		}

		// refresh moneyTree ui
		MoneyTreeUI * pMoneyTreeUI = (MoneyTreeUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMoneyTreeUI);
		if (NULL != pMoneyTreeUI)
		{
			// ���ҡǮ���Ч�
			pMoneyTreeUI->addMoneyTreeAnmation();
			pMoneyTreeUI->initDataFromInternet();
		}
	}
}
