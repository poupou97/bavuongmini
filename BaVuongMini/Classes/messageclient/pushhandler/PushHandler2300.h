#ifndef Blog_C___Reflection_PushHandler2300_h
#define Blog_C___Reflection_PushHandler2300_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler2300 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler2300)

public:
	SYNTHESIZE(PushHandler2300, int*, m_pValue)

	PushHandler2300() ;
	virtual ~PushHandler2300() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
