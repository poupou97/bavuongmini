#include "PushHandler5221.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../ui/Active_ui/ActiveManager.h"
#include "../../utils/StaticDataManager.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../element/CMapInfo.h"

IMPLEMENT_CLASS(PushHandler5221)

PushHandler5221::PushHandler5221() 
{

}
PushHandler5221::~PushHandler5221() 
{

}
void* PushHandler5221::createInstance()
{
	return new PushHandler5221() ;
}
void PushHandler5221::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5221::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5221::handle(CommonMessage* mb)
{
	Push5221 bean;
	bean.ParseFromString(mb->data());
	
	if (GameView::getInstance()->getMapInfo()->maptype() != com::future::threekingdoms::server::transport::protocol::coliseum)
		return;

	MissionAndTeam * missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
	if (missionAndTeam)
	{
		for(unsigned int i = 0;i<missionAndTeam->generalsInArena_other.size();i++)
		{
			MissionAndTeam::GeneralInfoInArena * general =  missionAndTeam->generalsInArena_other.at(i);
			if (bean.generalid() == general->generalId)
			{
				general->isFinishedFight = true;
			}
		}

		TableView * offLineArenaTableView_right = (TableView*)missionAndTeam->getRightOffLineArenaTableView();
		if (offLineArenaTableView_right)
			offLineArenaTableView_right->reloadData();
	}
}