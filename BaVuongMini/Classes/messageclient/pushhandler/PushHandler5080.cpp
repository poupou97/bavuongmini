
#include "PushHandler5080.h"

#include "../GameMessageProcessor.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(PushHandler5080)

PushHandler5080::PushHandler5080() 
{
    
}
PushHandler5080::~PushHandler5080() 
{
    
}
void* PushHandler5080::createInstance()
{
    return new PushHandler5080() ;
}
void PushHandler5080::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5080::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler5080::handle(CommonMessage* mb)
{
	Push5080 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameView::getInstance()->myplayer->setReachedMap(bean);

	//GameMessageProcessor::sharedMsgProcessor()->setPingTime(GameUtils::millisecondNow());
}