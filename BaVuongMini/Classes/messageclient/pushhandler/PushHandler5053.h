
#ifndef Blog_C___Reflection_PushHandler5053_h
#define Blog_C___Reflection_PushHandler5053_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5053 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5053)

public:
	SYNTHESIZE(PushHandler5053, int*, m_pValue)

		PushHandler5053() ;
	virtual ~PushHandler5053() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
