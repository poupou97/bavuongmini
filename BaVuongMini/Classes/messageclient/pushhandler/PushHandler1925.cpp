#include "PushHandler1925.h"

#include "../protobuf/CopyMessage.pb.h"
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../ui/FivePersonInstance/InstanceEndUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/CFivePersonInstanceEndInfo.h"
#include "../element/CRewardProp.h"
#include "../../ui/FivePersonInstance/InstanceRewardUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/GoodsInfo.h"
#include "../../ui/FivePersonInstance/RewardCardItem.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(PushHandler1925)

	PushHandler1925::PushHandler1925() 
{

}
PushHandler1925::~PushHandler1925() 
{

}
void* PushHandler1925::createInstance()
{
	return new PushHandler1925() ;
}
void PushHandler1925::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1925::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1925::handle(CommonMessage* mb)
{
	Rsp1925 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, instance is over", mb->cmdid());

	MainScene* mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	CCAssert(mainLayer != NULL, "should not be nil");

	InstanceEndUI * temp = dynamic_cast<InstanceEndUI*>(mainLayer->getChildByTag(kTagInstanceEndUI));
	if (temp)
	{
		CRewardProp * rewardProp = new CRewardProp();
		rewardProp->CopyFrom(bean.rewardprop());

		if (temp->getULayer())
		{
			if (temp->getULayer()->getChildByTag(RewardCardItemBaseTag + rewardProp->grid()) != NULL)
			{
				RewardCardItem * rewardCardItem = (RewardCardItem*)temp->getULayer()->getChildByTag(RewardCardItemBaseTag + rewardProp->grid());
				rewardCardItem->RefreshCardInfo(rewardProp,bean.rolename(),bean.roleid());
				rewardCardItem->FlopAnimationToFront();
			}
		}

		delete rewardProp;

		if (GameView::getInstance()->isOwn(bean.roleid()))
		{
			//���´�����
			temp->setRemainNumValue(temp->getAllNumValue() - bean.rewardtimes());
			temp->setPayForCardValue();
		}
	}
}