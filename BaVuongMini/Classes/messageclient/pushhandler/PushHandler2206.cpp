
#include "PushHandler2206.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/RelationMessage.pb.h"
#include "GameView.h"
#include "../element/CRelationPlayer.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(PushHandler2206)

PushHandler2206::PushHandler2206() 
{
    
}
PushHandler2206::~PushHandler2206() 
{
    
}
void* PushHandler2206::createInstance()
{
    return new PushHandler2206() ;
}
void PushHandler2206::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2206::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler2206::handle(CommonMessage* mb)
{
	Req2206 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	/**
	* �ͻ��������Ϊ����ָ���͵�������󣬷������������Ϣ����Ŀ����ң�����ȷ�ϡ�
	* ����ָ����Ȼ�Reqǿ�ͷ���������ɷ�������ͻ��ˡ�
	*/
	
	CRelationPlayer * relationPlayer = new CRelationPlayer();
	relationPlayer->CopyFrom(bean.applicant());
	MainScene * mainui = (MainScene *)GameView::getInstance()->getMainUIScene();
	mainui->interactRelationOfPlayers(relationPlayer);
	delete relationPlayer;
	
}
