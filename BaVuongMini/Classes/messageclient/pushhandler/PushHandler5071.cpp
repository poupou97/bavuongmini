
#include "PushHandler5071.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsStrategiesUI.h"
#include "../../gamescene_state/role/GameActor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../element/CFightWayBase.h"

IMPLEMENT_CLASS(PushHandler5071)

PushHandler5071::PushHandler5071() 
{

}
PushHandler5071::~PushHandler5071() 
{

}
void* PushHandler5071::createInstance()
{
	return new PushHandler5071() ;
}
void PushHandler5071::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5071::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5071::handle(CommonMessage* mb)
{
	Resp5071 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	GeneralsUI * generalsUI = (GeneralsUI *)MainScene::GeneralsScene;
	//if (scene->getMainUIScene()->getChildByTag(kTagGeneralsUI))
	if(generalsUI)
	{
		if (bean.has_error())
		{
			GameView::getInstance()->showAlertDialog(bean.error().msg());
		}
		else
		{
			//GeneralsUI * generalsUI = (GeneralsUI*)scene->getMainUIScene()->getChildByTag(kTagGeneralsUI);

// 			std::vector<CFightWayBase *>::iterator iter_strategies;
// 			for(iter_strategies=genentalsUI->generalsStrategiesUI->generalsStrategiesList.begin();iter_strategies!=genentalsUI->generalsStrategiesUI->generalsStrategiesList.end();++iter_strategies)
// 			{
// 				delete *iter_strategies;
// 			}
// 			genentalsUI->generalsStrategiesUI->generalsStrategiesList.clear();

// 			int b = bean.fightways_size();
// 			for (int i = 0;i<b;i++)
// 			{
// 				CFightWayBase* fightWayBase = new CFightWayBase();
// 				fightWayBase->CopyFrom(bean.fightways(i));
// 				genentalsUI->generalsStrategiesUI->generalsStrategiesList.push_back(fightWayBase);
// 			}

// 			genentalsUI->generalsStrategiesUI->setActiveFightWay(bean.activefightway());
// 			genentalsUI->generalsStrategiesUI->RefreshData();

			generalsUI->generalsStrategiesUI->setStrategiesListToDefault();
			for (unsigned int i = 0;i<generalsUI->generalsStrategiesUI->generalsStrategiesList.size();++i)
			{
				CFightWayBase* fightWayBase = generalsUI->generalsStrategiesUI->generalsStrategiesList.at(i);
				for (int j = 0;j<bean.fightways_size();j++)
				{
					CFightWayBase* temp = new CFightWayBase();
					temp->CopyFrom(bean.fightways(j));
					if (fightWayBase->fightwayid() == temp->fightwayid())
					{
						fightWayBase->set_level(temp->level());
						fightWayBase->set_activeflag(temp->activeflag());
					}

					delete temp;

					// find the active strategy
					if(fightWayBase->activeflag() == 1)
					{
						GameView::getInstance()->myplayer->setActiveStrategyId(fightWayBase->fightwayid());
					}
				}
			}

			generalsUI->generalsStrategiesUI->RefreshData();
		}
	}
}
