#include "PushHandler5302.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../protobuf/WorldBossMessage.pb.h"
#include "../../ui/Active_ui/WorldBoss_ui/WorldBossData.h"
#include "../element/CUnReceivePrize.h"
#include "../../ui/Active_ui/WorldBoss_ui/GetRewardUI.h"

IMPLEMENT_CLASS(PushHandler5302)

	PushHandler5302::PushHandler5302() 
{

}
PushHandler5302::~PushHandler5302() 
{

}

void* PushHandler5302::createInstance()
{
	return new PushHandler5302() ;
}
void PushHandler5302::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5302::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5302::handle(CommonMessage* mb)
{
	ResUnReceivePrize5302 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// update data
	WorldBossData::getInstance()->clearUnReceiveRewardList();
	for(int i = 0 ;i < bean.prizes_size();++i)
	{
		CUnReceivePrize * temp = new CUnReceivePrize();
		temp->CopyFrom(bean.prizes(i));
		WorldBossData::getInstance()->m_unReceiveRewardList.push_back(temp);
	}

	if (!GameView::getInstance())
		return;

	if (!GameView::getInstance()->getGameScene())
		return;

	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (!mainscene)
		return;

	GetRewardUI * getRewardUI = (GetRewardUI*)mainscene->getChildByTag(kTagGetRewardUI);
	if (getRewardUI)
	{
		getRewardUI->RefreshUI();
	}
}

