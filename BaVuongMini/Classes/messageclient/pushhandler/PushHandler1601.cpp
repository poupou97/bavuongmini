
#include "PushHandler1601.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "GameView.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CAdditionProperty.h"
#include "../../ui/equipMent_ui/RefreshEquipData.h"


IMPLEMENT_CLASS(PushHandler1601)

PushHandler1601::PushHandler1601() 
{
    
}
PushHandler1601::~PushHandler1601() 
{
    
}
void* PushHandler1601::createInstance()
{
	return new PushHandler1601() ;
}
void PushHandler1601::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1601::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1601::handle(CommonMessage* mb)
{
	Rsp1601 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	bean.strengthproperties();//�������ֵ
	bean.experience();//װ����ǰ����ֵ
	bean.strengthgrade();//װ����ǰ����ȼ�

	EquipMentUi * equip= dynamic_cast<EquipMentUi *>(GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI));
	if(equip == NULL)
		return;

	std::vector<CAdditionProperty *>::iterator iter;
	for (iter = equip->additionProperty.begin(); iter != equip->additionProperty.end(); ++iter)
	{
		delete *iter;
	}
	equip->additionProperty.clear();
	for (int i=0;i<bean.strengthproperties_size();i++)
	{
		CAdditionProperty * addproperty_ =new CAdditionProperty();
		addproperty_->CopyFrom(bean.strengthproperties(i));
		equip->additionProperty.push_back(addproperty_);

	}
	int curExScale =bean.experience(0);
	equip->curBeginExp = curExScale;
	int nextExScale= bean.experience(1);
	int allExScale= curExScale+nextExScale;
	equip->setEquipBeginLevel(bean.strengthgrade());
	equip->equipLookRefineLevel_ = bean.strengthgrade();
	//��ǵ�ǰѡ�еı������Ʒ
	equip->refineMainEquip =true;
	equip->refreshCurState();
	if (bean.strengthgrade() >0)
	{
// 		std::string curRefineLevelString = "+";
// 		char curRefineLevelStr[10];
// 		sprintf(curRefineLevelStr,"%d",bean.strengthgrade());
// 		curRefineLevelString.append(curRefineLevelStr);
// 		equip->labelFont_curRefinelevel->setText(curRefineLevelString.c_str());
// 		equip->labelFont_curRefinelevel->setVisible(true);
	}
	
	equip->refreshBackPack(equip->currType,equip->equipLookRefineLevel_);
	
	RefreshEquipData::instance()->getOperationGeneral();
	
	equip->refreshEquipProperty(curExScale,allExScale,false);
}
