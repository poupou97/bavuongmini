
#include "PushHandler1805.h"


#include "../../gamescene_state/MainScene.h"
#include "../protobuf/AuctionMessage.pb.h"
#include "GameView.h"
#include "../../ui/Auction_ui/AuctionUi.h"
#include "../element/CAuctionInfo.h"
#include "../../ui/Auction_ui/AuctionTabView.h"

IMPLEMENT_CLASS(PushHandler1805)

PushHandler1805::PushHandler1805() 
{
    
}
PushHandler1805::~PushHandler1805() 
{
    
}
void* PushHandler1805::createInstance()
{
    return new PushHandler1805() ;
}
void PushHandler1805::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1805::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1805::handle(CommonMessage* mb)
{
	Rsp1805 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	std::vector<CAuctionInfo *>::iterator iter;
	for (iter=GameView::getInstance()->auctionSourceVector.begin();iter!=GameView::getInstance()->auctionSourceVector.end();iter++)
	{
		delete * iter;
	}
	GameView::getInstance()->auctionSourceVector.clear();

	for (iter=GameView::getInstance()->selfConsignVector.begin();iter!=GameView::getInstance()->selfConsignVector.end();iter++)
	{
		delete *iter;
	}
	GameView::getInstance()->selfConsignVector.clear();

	
	for (int i=0;i<bean.auctioninfos_size();i++)
	{
		CAuctionInfo * auctioninfo =new CAuctionInfo();
		auctioninfo->CopyFrom(bean.auctioninfos(i));

		GameView::getInstance()->selfConsignVector.push_back(auctioninfo);
	}

	AuctionUi * auctionui =(AuctionUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	if (auctionui != NULL)
	{
		TableView * auctionTab=(TableView *)auctionui->getChildByTag(AUCTIONLAYER)->getChildByTag(AUCTIONTABVIEW)->getChildByTag(AUCTIONCCTABVIEW);
		auctionTab->reloadData();
		auctionui->curMaxConsignGoodsNum = bean.limitnumber();
	}
}
