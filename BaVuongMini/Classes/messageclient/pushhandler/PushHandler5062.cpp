
#include "PushHandler5062.h"

#include "../protobuf/CollectMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../element/CPickingInfo.h"
#include "../../GameView.h"
#include "../../legend_engine/GameWorld.h"
#include "../element/CPushCollectInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/RecruitMessage.pb.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../ui/generals_ui/GeneralsRecuriteUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CActionDetail.h"

IMPLEMENT_CLASS(PushHandler5062)

	PushHandler5062::PushHandler5062() 
{

}
PushHandler5062::~PushHandler5062() 
{

}
void* PushHandler5062::createInstance()
{
	return new PushHandler5062() ;
}
void PushHandler5062::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5062::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5062::handle(CommonMessage* mb)
{
	PushRecruitRemainNumberReset5062 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	ActionType actionType = bean.type();
	int playerRemianNumber = bean.playerremainnum();

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	for(int i = 0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		CActionDetail * temp = GameView::getInstance()->actionDetailList.at(i);
		if (actionType == temp->type())
		{
			if (actionType == BASE)
			{
				temp->set_playerremainnumber(playerRemianNumber);
			}
		}
	}

// 	//���ÿ���ļ������Ҫ����ͨ��ļÿ�մ���ˢ�£�
// 	if (scene->getMainUIScene()->getChildByTag(kTagGeneralsUI) != NULL)
// 	{
// 		GeneralsUI * generalui = (GeneralsUI *)scene->getMainUIScene()->getChildByTag(kTagGeneralsUI);
// 		if(actionType == BASE)
// 		{
// 			if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG))
// 			{
// 				RecuriteActionItem * tempRecurite = ( RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(BASERECURITEACTIONITEMTAG);
// 				tempRecurite->playerRemainNumber = playerRemianNumber;
// 			}
// 		}
// 	}
		
}
