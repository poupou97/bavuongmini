
#ifndef Blog_C___Family_PushHandler1541_h
#define Blog_C___Family_PushHandler1541_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1541 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1541)

public:
	SYNTHESIZE(PushHandler1541, int*, m_pValue)

		PushHandler1541() ;
	virtual ~PushHandler1541() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
