
#ifndef Blog_C___Reflection_PushHandler1107_h
#define Blog_C___Reflection_PushHandler1107_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

using namespace std;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1107 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1107)
public:
    SYNTHESIZE(PushHandler1107, int*, m_pValue)
    
    PushHandler1107() ;
    virtual ~PushHandler1107() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
} ;

#endif