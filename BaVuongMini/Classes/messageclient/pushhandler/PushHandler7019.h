
#ifndef Blog_C___Reflection_PushHandler7019_h
#define Blog_C___Reflection_PushHandler7019_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler7019 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler7019)

public:
	SYNTHESIZE(PushHandler7019, int*, m_pValue)

		PushHandler7019() ;
	virtual ~PushHandler7019() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
