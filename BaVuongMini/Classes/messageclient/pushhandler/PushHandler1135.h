
#ifndef Blog_C___Reflection_PushHandler1135_h
#define Blog_C___Reflection_PushHandler1135_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1135 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1135)
public:
	SYNTHESIZE(PushHandler1135, int*, m_pValue)

		PushHandler1135() ;
	virtual ~PushHandler1135() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;
} ;

#endif