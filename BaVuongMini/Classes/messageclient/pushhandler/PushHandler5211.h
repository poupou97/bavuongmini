#ifndef Blog_C___Reflection_PushHandler5211_h
#define Blog_C___Reflection_PushHandler5211_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5211 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5211)

public:
	SYNTHESIZE(PushHandler5211, int*, m_pValue)

	PushHandler5211() ;
	virtual ~PushHandler5211() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
