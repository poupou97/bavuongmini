#include "PushHandler5104.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../../ui/country_ui/CountryUI.h"

IMPLEMENT_CLASS(PushHandler5104)

PushHandler5104::PushHandler5104() 
{

}
PushHandler5104::~PushHandler5104() 
{
	
}
void* PushHandler5104::createInstance()
{
	return new PushHandler5104() ;
}
void PushHandler5104::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5104::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5104::handle(CommonMessage* mb)
{
	Rsp5104 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	CountryUI::mCountryFlag = bean.country();
}