
#include "PushHandler1409.h"

#include "../protobuf/InteractMessage.pb.h"  
#include "GameView.h"
#include "../element/CMapTeam.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Friend_ui/TeamList.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(PushHandler1409)

PushHandler1409::PushHandler1409() 
{
  
}
PushHandler1409::~PushHandler1409() 
{
    
}
void* PushHandler1409::createInstance()
{
    return new PushHandler1409() ;
}
void PushHandler1409::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1409::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1409::handle(CommonMessage* mb)
{
	Push1409 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	std::vector<CMapTeam*>::iterator iter;

	for (iter = GameView::getInstance()->mapteamVector.begin(); iter != GameView::getInstance()->mapteamVector.end(); ++iter)
	{
		delete *iter;
	}
	GameView::getInstance()->mapteamVector.clear();

	//�����Ա�б
	for (int i=0;i<bean.members_size();i++)
	{
		CMapTeam * team=new CMapTeam();
		team->CopyFrom(bean.members(i));
		GameView::getInstance()->mapteamVector.push_back(team);
	}
	FriendUi * friendui= (FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	for (int i=0;i<GameView::getInstance()->mapteamVector.size();i++)
	{
		for (int j=0;j<GameView::getInstance()->mapteamVector.at(i)->teammembers_size();j++)
		{
			if (GameView::getInstance()->mapteamVector.at(i)->teammembers(j).roleid()== GameView::getInstance()->myplayer->getRoleId())
			{
				friendui->playerTeamId= GameView::getInstance()->mapteamVector.at(i)->teamid();
			}
		}
	}
	if (friendui != NULL)
	{
		TableView * tab=(TableView *)friendui->teamList->getChildByTag(TEAMLISTTABVIEW);
		tab->reloadData();
		friendui->showTeamPlayer(0);
	}

}
