
#ifndef Blog_C___Reflection_PushHandler1209_h
#define Blog_C___Reflection_PushHandler1209_h

#include "PushHandlerProtocol.h"
#include "../../common/CKBaseClass.h"

class PushHandler1209 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1209)
    
public:
    SYNTHESIZE(PushHandler1209, int*, m_pValue)
    
    PushHandler1209() ;
    virtual ~PushHandler1209() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
