
#ifndef Blog_C___Reflection_PushHandler7011_h
#define Blog_C___Reflection_PushHandler7011_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler7011 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler7011)

public:
	SYNTHESIZE(PushHandler7011, int*, m_pValue)

		PushHandler7011() ;
	virtual ~PushHandler7011() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
