
#ifndef Blog_C___Reflection_PushHandler1621_h
#define Blog_C___Reflection_PushHandler1621_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1621 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1621)
    
public:
    SYNTHESIZE(PushHandler1621, int*, m_pValue)
    
    PushHandler1621() ;
    virtual ~PushHandler1621() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
