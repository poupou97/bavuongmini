
#include "PushHandler7018.h"

#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../../ui/rewardTask_ui/RewardTaskData.h"

IMPLEMENT_CLASS(PushHandler7018)

	PushHandler7018::PushHandler7018() 
{

}
PushHandler7018::~PushHandler7018() 
{

}
void* PushHandler7018::createInstance()
{
	return new PushHandler7018() ;
}
void PushHandler7018::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler7018::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler7018::handle(CommonMessage* mb)
{
	PushRefreshBoardCdTime7018 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//refresh remianCDTime
	RewardTaskData::getInstance()->setRemainCDTime(bean.remaincd());
}
