
#ifndef Blog_C___Reflection_PushHandler1202_h
#define Blog_C___Reflection_PushHandler1202_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
#include "../protobuf/FightMessage.pb.h"

USING_NS_CC;

class BaseFighter;

class PushHandler1202 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1202)
    
public:
    SYNTHESIZE(PushHandler1202, int*, m_pValue)
    
    PushHandler1202() ;
    virtual ~PushHandler1202() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:
	/**
	* 设置被攻击者的伤害及状态（HP，MP，效果）
	*/
	void setDefenders(BaseFighter* attacker, Push1202 bean);

protected:
    int *m_pValue ;

} ;

#endif
