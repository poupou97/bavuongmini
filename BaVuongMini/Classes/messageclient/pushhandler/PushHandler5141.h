
#ifndef Blog_C___Reflection_PushHandler5141_h
#define Blog_C___Reflection_PushHandler5141_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"

USING_NS_CC;

class PushHandler5141 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5141)

public:
	SYNTHESIZE(PushHandler5141, int*, m_pValue)

	PushHandler5141() ;
	virtual ~PushHandler5141() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);


protected:
	int *m_pValue ;
} ;

#endif
