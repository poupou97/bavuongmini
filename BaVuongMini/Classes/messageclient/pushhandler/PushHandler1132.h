
#ifndef Blog_C___Reflection_PushHandler1132_h
#define Blog_C___Reflection_PushHandler1132_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1132 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1132)
public:
	SYNTHESIZE(PushHandler1132, int*, m_pValue)

	PushHandler1132() ;
	virtual ~PushHandler1132() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;
} ;

#endif