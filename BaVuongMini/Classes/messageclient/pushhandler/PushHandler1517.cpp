
#include "PushHandler1517.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/family_ui/ManageFamily.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CGuildMemberBase.h"
#include "../element/CGuildRecord.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../element/CGuildBase.h"
#include "../../gamescene_state/role/BasePlayer.h"

IMPLEMENT_CLASS(PushHandler1517)

PushHandler1517::PushHandler1517() 
{
    
}
PushHandler1517::~PushHandler1517() 
{
    
}
void* PushHandler1517::createInstance()
{
    return new PushHandler1517() ;
}
void PushHandler1517::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1517::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1517::handle(CommonMessage* mb)
{
	Push1517 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// modify the family position info on the player's head
	for (int i=0;i< bean.members_size();i++)
	{
		long long roleId = bean.members(i).id();
		BasePlayer* pPlayer = dynamic_cast<BasePlayer*>(GameView::getInstance()->getGameScene()->getActor(roleId));
		if(pPlayer != NULL)
		{
			int position = bean.members(i).post();
			pPlayer->getActiveRole()->mutable_playerbaseinfo()->set_factionposition(position);
			pPlayer->addFamilyName(pPlayer->getActiveRole()->playerbaseinfo().factionname().c_str(), position);
		}
	}

	std::string strLeader = "";
	std::string strLeaderEnd_ = "";
	long long roleIdBegin = 0;
	int postBegin = 0;
	int postEnd = 0;
	long long roleIdEnd = 0;
	for (int i=0;i< bean.members_size();i++)
	{
		if (bean.members(i).post() == CGuildBase::position_master)
		{
			strLeader = bean.members(i).name();
			roleIdBegin = bean.members(i).id();
			postBegin = bean.members(i).post();
		}

		if (bean.members(i).post() != CGuildBase::position_master)
		{
			strLeaderEnd_ = bean.members(i).name();
			roleIdEnd = bean.members(i).id();
			postEnd = bean.members(i).post();
		}
	}
	
	const char * str_ = StringDataManager::getString("family_zuzhangchuanwei");
	strLeaderEnd_.append(str_);
	strLeaderEnd_.append(strLeader);
	GameView::getInstance()->showAlertDialog(strLeaderEnd_.c_str());

	FamilyUI * familyui =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	//���ְλ�䶯 ��ˢ���Լ���ְλ ���ǰ���ڼ��UI  ��return 
	//�ͬʱˢ�UI½���ְλ��Ϣ �Լ�����ְȨ
	if (familyui == NULL)
	{
		return;
	}
	for (int i=0;i<familyui->vectorGuildMemberBase.size();i++)
	{
		if (familyui->vectorGuildMemberBase.at(i)->id() == roleIdBegin)
		{
			familyui->vectorGuildMemberBase.at(i)->set_post(postBegin);
		}

		if (familyui->vectorGuildMemberBase.at(i)->id() == roleIdEnd)
		{
			familyui->vectorGuildMemberBase.at(i)->set_post(postEnd);
		}
	}

	if (roleIdBegin == familyui->selfId_ )
	{
		std::string strings_ = StringDataManager::getString("family_member_Master");
		familyui->label_myPost->setString(strings_.c_str());
		familyui->selfPost_ = postBegin;
	}

	if (roleIdEnd == familyui->selfId_ )
	{
		std::string strings_ = StringDataManager::getString("family_member_member");
		familyui->label_myPost->setString(strings_.c_str());
		familyui->selfPost_ = postEnd;

		familyui->btn_repairNotice->setTouchEnabled(false);
		familyui->btn_repairManifesto->setTouchEnabled(false);
		familyui->btn_mamage->setVisible(false);
	}

	familyui->tableviewMember->reloadData();	
}
