
#ifndef Blog_C___Reflection_PushHandler1801_h
#define Blog_C___Reflection_PushHandler1801_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1801 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1801)
    
public:
    SYNTHESIZE(PushHandler1801, int*, m_pValue)
    
    PushHandler1801() ;
    virtual ~PushHandler1801() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
