
#ifndef Blog_C___Family_PushHandler1542_h
#define Blog_C___Family_PushHandler1542_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1542 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1542)

public:
	SYNTHESIZE(PushHandler1542, int*, m_pValue)

		PushHandler1542() ;
	virtual ~PushHandler1542() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
