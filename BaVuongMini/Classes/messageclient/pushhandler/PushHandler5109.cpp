#include "PushHandler5109.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../ui/country_ui/CountryUI.h"
#include "../../GameView.h"
#include "../element/CGeneralBaseMsg.h"
#include "../element/CGeneralDetail.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/GeneralsInfoUI.h"
#include "../element/CMapInfo.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"

IMPLEMENT_CLASS(PushHandler5109)

PushHandler5109::PushHandler5109() 
{

}
PushHandler5109::~PushHandler5109() 
{

}
void* PushHandler5109::createInstance()
{
	return new PushHandler5109() ;
}
void PushHandler5109::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5109::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5109::handle(CommonMessage* mb)
{
	Push5109 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//update data
	//ʵʱ���������佫��Ϣ(generalsInLineList)
	for(int i = 0 ; i<GameView::getInstance()->generalsInLineList.size();++i)
	{
		CGeneralBaseMsg * temp = GameView::getInstance()->generalsInLineList.at(i);
		if (temp->id() == bean.id())
		{
			temp->set_showtype(bean.showtype());
			temp->set_showlefttime(bean.showlefttime());
			//temp->set_LeftTime(bean.showlefttime());
			temp->set_revivetime(bean.revivetime());
			temp->startCD();
			break;
		}
	}
	//ʵʱ���������佫��Ϣ(generalsInLineDetailList)
	for(int i = 0 ; i<GameView::getInstance()->generalsInLineDetailList.size();++i)
	{
		CGeneralDetail * temp = GameView::getInstance()->generalsInLineDetailList.at(i);
		if (temp->generalid() == bean.id())
		{
			temp->set_showtype(bean.showtype());
			temp->set_showlefttime(bean.showlefttime());
			temp->set_revivetime(bean.revivetime());
			break;
		}
	}

	//refresh UI
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI*)mainScene->getGeneralInfoUI();
		if (generalsInfoUI)
			generalsInfoUI->RefreshOneGeneralItem(bean.id());
	}

	//refresh missionAndTeam in offLineArena
	if (mainScene)
	{
		if (GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::coliseum)
		{
			MissionAndTeam * missionAndTeam = (MissionAndTeam*)mainScene->getChildByTag(kTagMissionAndTeam);
			if (missionAndTeam)
			{
				for(unsigned int i = 0;i<missionAndTeam->generalsInArena_mine.size();i++)
				{
					MissionAndTeam::GeneralInfoInArena * general =  missionAndTeam->generalsInArena_mine.at(i);
					if (bean.id() == general->generalId)
					{
						if (bean.revivetime() > 0)   //died
						{
							general->isFinishedFight = true;
						}
						else
						{
							if (bean.showtype() == 0)   //rest
							{
								general->isFinishedFight = true;
							}
						}
					}
				}

				TableView * offLineArenaTableView_left = (TableView*)missionAndTeam->getLeftOffLineArenaTableView();
				if (offLineArenaTableView_left)
					offLineArenaTableView_left->reloadData();
			}
		}
	}
}