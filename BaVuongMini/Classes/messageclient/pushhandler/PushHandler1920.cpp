#include "PushHandler1920.h"

#include "../protobuf/CopyMessage.pb.h"
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../GameView.h"
#include "../../ui/FivePersonInstance/InstanceDetailUI.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../utils/StaticDataManager.h"
#include "../element/CFivePersonInstance.h"
#include "../../legend_script/ScriptManager.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(PushHandler1920)

PushHandler1920::PushHandler1920() 
{
    
}
PushHandler1920::~PushHandler1920() 
{
    
}
void* PushHandler1920::createInstance()
{
    return new PushHandler1920() ;
}
void PushHandler1920::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1920::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1920::handle(CommonMessage* mb)
{
	Push1920 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, instance message", mb->cmdid());

	// 通知类型 1:进入排队 2：退出排队 3：进入副本 4：退出副本
	if(bean.type() == 1)   // 进入排队
	{
		FivePersonInstance::setStatus(FivePersonInstance::status_in_sequence);
		FivePersonInstance::setCurrentInstanceId(-1);
		FivePersonInstance::setSignedUpId(bean.copyid());

		std::string str_des = StringDataManager::getString("fivePerson_signUp_des_1");
		for (int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
		{
			CFivePersonInstance * fivePersonInstance = FivePersonInstanceConfigData::s_fivePersonInstance.at(i);
			if (FivePersonInstance::getSignedUpId() == fivePersonInstance->get_id())
			{
				str_des.append(fivePersonInstance->get_name().c_str());
				break;
			}
		}
		str_des.append(StringDataManager::getString("fivePerson_signUp_des_2"));
		GameView::getInstance()->showAlertDialog(str_des.c_str());
	}
	else if (bean.type() == 2)   //退出排队
	{
		FivePersonInstance::setStatus(FivePersonInstance::status_none);
		FivePersonInstance::setCurrentInstanceId(-1);
		FivePersonInstance::setSignedUpId(-1);

		std::string str_des = StringDataManager::getString("fivePerson_cancelSignUp_des_1");
		for (int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
		{
			CFivePersonInstance * fivePersonInstance = FivePersonInstanceConfigData::s_fivePersonInstance.at(i);
			if (bean.copyid() == fivePersonInstance->get_id())
			{
				str_des.append(fivePersonInstance->get_name().c_str());
				break;
			}
		}
		str_des.append(StringDataManager::getString("fivePerson_cancelSignUp_des_2"));
		GameView::getInstance()->showAlertDialog(str_des.c_str());
	}
	else if(bean.type() == 3)   // 进入副本
	{
		FivePersonInstance::setStatus(FivePersonInstance::status_in_instance);
		FivePersonInstance::setCurrentInstanceId(bean.copyid());
		FivePersonInstance::setSignedUpId(-1);
		if (GameView::getInstance()->getGameScene())
		{
			MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				//mainScene->guideMap->reloadToDefaultConfig(bean.copyid());
				MissionAndTeam * missionAndTeam = (MissionAndTeam*)mainScene->getChildByTag(kTagMissionAndTeam);
				if (missionAndTeam)
				{
					missionAndTeam->reloadToDefaultConfig(bean.copyid());
				}

				//如果该副本星级为零，开启教学，指引他点任务小面板自动杀怪
				bool isNeedOpen = true;
				std::map<int, int>::iterator it = FivePersonInstance::getInstance()->s_m_copyStar.begin();
				for ( ; it != FivePersonInstance::getInstance()->s_m_copyStar.end(); ++ it )
				{
					if (it->first == FivePersonInstance::getInstance()->getCurrentInstanceId())
					{
						int star =  it->second;
						if (star > 0)
						{
							isNeedOpen = false;
						}

						break;
					}
				}
				if (isNeedOpen)
				{
					ScriptManager::getInstance()->runScript("script/instanceGuide.sc");
				}
			}
		}
	}
	else if(bean.type() == 4)   // 退出副本
	{
		FivePersonInstance::setStatus(FivePersonInstance::status_none);
		FivePersonInstance::setCurrentInstanceId(-1);
		FivePersonInstance::setSignedUpId(-1);
		
		MyPlayerAIConfig::setAutomaticSkill(0);
		MyPlayerAIConfig::enableRobot(false);
		GameView::getInstance()->myplayer->getMyPlayerAI()->stop();
		if (GameView::getInstance()->getGameScene())
		{
			if (GameView::getInstance()->getMainUIScene())
			{
				GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
				if (guide_)
				{
					guide_->setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
				}
			}
		}
	}

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	InstanceDetailUI * instanceDetail = (InstanceDetailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagInstanceDetailUI);
	if (!instanceDetail)
		return;

	if (FivePersonInstance::getSignedUpId() != -1)
	{
		instanceDetail->btnSignUp->setVisible(false);
		instanceDetail->btnCancelSign->setVisible(true);
	}
	else
	{
		instanceDetail->btnSignUp->setVisible(true);
		instanceDetail->btnCancelSign->setVisible(false);
	}
}