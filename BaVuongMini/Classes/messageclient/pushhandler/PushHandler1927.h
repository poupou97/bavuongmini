
#ifndef Blog_C___Reflection_PushHandler1927_h
#define Blog_C___Reflection_PushHandler1927_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
USING_NS_CC;

/**
 *  副本进度刷新
 */
class PushHandler1927 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1927)
    
public:
    SYNTHESIZE(PushHandler1927, int*, m_pValue)
    
    PushHandler1927() ;
    virtual ~PushHandler1927() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
