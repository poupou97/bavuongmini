#include "PushHandler5101.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../element/CPkDailyReward.h"
#include "../../ui/backpackscene/BattleAchievementData.h"
#include "../../ui/backpackscene/BattleAchievementUI.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"

IMPLEMENT_CLASS(PushHandler5101)

PushHandler5101::PushHandler5101() 
{

}
PushHandler5101::~PushHandler5101() 
{
	
}
void* PushHandler5101::createInstance()
{
	return new PushHandler5101() ;
}
void PushHandler5101::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5101::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5101::handle(CommonMessage* mb)
{
	Rsp5101 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// �����
	std::vector<CPkDailyReward *>::iterator iter;
	for (iter = BattleAchievementData::instance()->m_vector_reward.begin(); iter != BattleAchievementData::instance()->m_vector_reward.end(); iter++)
	{
		delete *iter;
	}
	BattleAchievementData::instance()->m_vector_reward.clear();


	// ݳ�ʼ�� ������������
	int size = bean.reward_size();
	for (int i = 0; i < size; i++)
	{
		CPkDailyReward * pkDailyReward = new CPkDailyReward();
		pkDailyReward->CopyFrom(bean.reward(i));

		BattleAchievementData::instance()->m_vector_reward.push_back(pkDailyReward);
	}

	// ����
	int nRank = bean.ranking();
	BattleAchievementData::instance()->set_rank(nRank);

	// �����ȡ״̬
	bool bHasGetGift = bean.rewardstatus();
	BattleAchievementData::instance()->set_hasGetGift(bHasGetGift);


	// ��ʼ��UI��
	BattleAchievementUI *tmpBattleAchUI = (BattleAchievementUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBattleAchievementUI);
	if (NULL != tmpBattleAchUI)
	{
		tmpBattleAchUI->initDataFromInternet();
	}

}