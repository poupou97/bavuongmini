
#ifndef Blog_C___Reflection_PushHandler3020_h
#define Blog_C___Reflection_PushHandler3020_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"
#include "cocos2d.h"

USING_NS_CC;

class PushHandler3020 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler3020)

public:
	SYNTHESIZE(PushHandler3020, int*, m_pValue)

		PushHandler3020() ;
	virtual ~PushHandler3020() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
