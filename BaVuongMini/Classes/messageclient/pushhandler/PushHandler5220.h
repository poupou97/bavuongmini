#ifndef Blog_C___Reflection_PushHandler5220_h
#define Blog_C___Reflection_PushHandler5220_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5220 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5220)

public:
	SYNTHESIZE(PushHandler5220, int*, m_pValue)

		PushHandler5220() ;
	virtual ~PushHandler5220() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
