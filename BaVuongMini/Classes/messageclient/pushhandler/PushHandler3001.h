
#ifndef Blog_C___Reflection_PushHandler3001_h
#define Blog_C___Reflection_PushHandler3001_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"
#include "cocos2d.h"

USING_NS_CC;

class PushHandler3001 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler3001)

public:
	SYNTHESIZE(PushHandler3001, int*, m_pValue)

		PushHandler3001() ;
	virtual ~PushHandler3001() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
