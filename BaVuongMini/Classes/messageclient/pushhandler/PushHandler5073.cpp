
#include "PushHandler5073.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsStrategiesUI.h"
#include "../../gamescene_state/role/GameActor.h"
#include "../element/CFightWayBase.h"
#include "../element/CGeneralBaseMsg.h"
#include "../GameMessageProcessor.h"
#include "../../ui/generals_ui/GeneralsListBase.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../ui/generals_ui/GeneralsEvolutionUI.h"
#include "../../ui/generals_ui/GeneralsTeachUI.h"
#include "../../gamescene_state/role/General.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../gamescene_state/sceneelement/GeneralsInfoUI.h"
#include "../../gamescene_state/MainAnimationScene.h"
#include "../../gamescene_state/EffectDispatch.h"

IMPLEMENT_CLASS(PushHandler5073)

PushHandler5073::PushHandler5073() 
{

}
PushHandler5073::~PushHandler5073() 
{

}
void* PushHandler5073::createInstance()
{
	return new PushHandler5073() ;
}
void PushHandler5073::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5073::display() 
{
	cout << *getm_pValue() << endl ;
}

bool SortGeneralBaseMsgList(CGeneralBaseMsg * temp1 , CGeneralBaseMsg * temp2)
{
// 	if (temp1->fightstatus() > temp2->fightstatus())      
// 	{
// 		return true;
// 	}
// 	else if (temp1->fightstatus() == temp2->fightstatus())  //����ս״̬��ͬ����ϡ�ж����
// 	{
		if (temp1->rare() > temp2->rare())      
		{
			return true;
		}
		else if (temp1->rare() == temp2->rare())//����ϡ�ж���ͬ������ȼ����
		{
			if (temp1->evolution() > temp2->evolution())      
			{
				return true;
			}
			else if (temp1->evolution() == temp2->evolution())//�����ȼ���ͬ����Ʒ�ʵȼ����
			{
				if (temp1->currentquality() > temp2->currentquality())      
				{
					return true;
				}
				else if (temp1->currentquality() == temp2->currentquality())//����Ʒ�ʵȼ���ͬ����ID���
				{
// 					if (temp1->level() > temp2->level())      
// 					{
// 						return true;
// 					}
// 					else
// 					{
// 						return false;
// 					}
					return temp1->modelid() < temp2->modelid();
				}
			}
		}
//	}

	return false;
}

void PushHandler5073::handle(CommonMessage* mb)
{
	Resp5073 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, modify the generals position", mb->cmdid());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	if (bean.has_error())
	{
		GameView::getInstance()->showAlertDialog(bean.error().msg());
		return;
	}
	
	int m_old_AllFightPoint = GameView::getInstance()->myplayer->player->fightpoint();
	for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	{
		m_old_AllFightPoint += GameView::getInstance()->generalsInLineList.at(i)->fightpoint();
	}

	int m_general_status = 0;  //0:��ޱ仯  1: ���  2����
	//�ʵʱ���������佫��Ϣ
	for(int i = 0;i<bean.generalbase_size();++i)
	{
		if(bean.generalbase(i).fightstatus() == GeneralsListBase::NoBattle)
		{
			for(int j = 0 ; j<GameView::getInstance()->generalsInLineList.size();++j)
			{
				CGeneralBaseMsg * temp = GameView::getInstance()->generalsInLineList.at(j);
				if (temp->id() == bean.generalbase(i).id())
				{
					std::vector<CGeneralBaseMsg*>::iterator iter = GameView::getInstance()->generalsInLineList.begin()+j;
					GameView::getInstance()->generalsInLineList.erase(iter);
					delete temp;
					m_general_status = 2;
				}
			}
		}
		else
		{
			bool isHave = false;
			for(int j = 0 ; j<GameView::getInstance()->generalsInLineList.size();++j)
			{
				CGeneralBaseMsg * temp = GameView::getInstance()->generalsInLineList.at(j);
				if (temp->id() == bean.generalbase(i).id())
				{
					if (bean.generalbase(i).fightstatus() == GeneralsListBase::InBattle)       //��ս״̬
					{
						if (((temp->currentquality()-1)/10) < ((bean.generalbase(i).currentquality()-1)/10))   //Ʒ�ʷ���仯
						{
							if (GameView::getInstance()->getGameScene()->getActor(temp->id()))
							{
								General * general_temp = dynamic_cast<General*>(GameView::getInstance()->getGameScene()->getActor(temp->id()));  //activerole�еĽ�ɫ����Ӧ�ñ�ɫ ����û�
								if (general_temp)
								{
									//�ˢ�³����е��佫ͷ��������ɫ
									general_temp->modifyActorNameColor(GameView::getInstance()->getGeneralsColorByQuality(bean.generalbase(i).currentquality()));
									general_temp->showActorName(true);
									//ˢ�³����е��佫ͷ���Rank
									general_temp->addRank(bean.generalbase(i).evolution());
									//�ˢ��������佫ͷ�������������ɫ
									MainScene *mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
									if (mainScene)
									{
// 										if (mainScene->generalHeadLayer->getChildByTag(66))
// 										{
// 											GeneralsHeadManager * generalsHeadManeger = (GeneralsHeadManager *)mainScene->generalHeadLayer->getChildByTag(66);
// 											if (generalsHeadManeger)
// 											{
// 												generalsHeadManeger->RefreshGeneralNameColor(temp->id(),GameView::getInstance()->getGeneralsColorByQuality(bean.generalbase(i).currentquality()));
// 											}
// 										}
									}
								}
							}
						}
					}

					temp->CopyFrom(bean.generalbase(i));
					temp->startCD();
					//����˿̵�ʱ�
					temp->set_changeMoment(GameUtils::millisecondNow());
					isHave = true;
				}
			}

			if (isHave == false)
			{
				CGeneralBaseMsg * temp  = new CGeneralBaseMsg();
				temp->CopyFrom(bean.generalbase(i));
				temp->startCD();
				GameView::getInstance()->generalsInLineList.push_back(temp);
				m_general_status = 1;

				//䱣��˿̵�ʱ�
				temp->set_changeMoment(GameUtils::millisecondNow());
			}
		}
	}

	//sort data
	if (GameView::getInstance()->generalsInLineList.size()>=2)
	{
		sort(GameView::getInstance()->generalsInLineList.begin(),GameView::getInstance()->generalsInLineList.end(),SortGeneralBaseMsgList);
	}

	//add by yangjun 2014.9.29
	MainScene * mainScene = GameView::getInstance()->getMainUIScene();
	if (mainScene != NULL)
	{
		GeneralsInfoUI * generalsUI = (GeneralsInfoUI*)mainScene->getGeneralInfoUI();
		if (generalsUI)
		{
			if (m_general_status == 0)
			{
				for(int i = 0;i<bean.generalbase_size();i++)
				{
					generalsUI->ResetCDByGeneralId(bean.generalbase(i).id());
				}
			}
			else
			{
				generalsUI->RefreshAllGeneralItem();
			}
		}
	}

	for(int i = 0;i<GameView::getInstance()->generalBaseMsgList.size();++i)
	{
		CGeneralBaseMsg * temp = GameView::getInstance()->generalBaseMsgList.at(i);
		
		for(int j = 0;j<bean.generalbase_size();j++)
		{
			if (temp->id() == bean.generalbase(j).id())
			{
				temp->CopyFrom(bean.generalbase(j));
			}
		}
	}

	for(int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	{
		CGeneralBaseMsg * temp1 = GameView::getInstance()->generalsInLineList.at(i);

		for(int j = 0;j<bean.generalbase_size();j++)
		{
			if (temp1->id() == bean.generalbase(j).id())
			{
				temp1->CopyFrom(bean.generalbase(j));
			}
		}
	}

	for(int i =0;i<bean.generalbase_size();++i)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)bean.generalbase(i).id());
	}

	if (scene->getMainUIScene()->getChildByTag(kTagGeneralsUI))
	{
		if (bean.cause() != 4)
		{
			//�ˢ���佫�б
			GeneralsUI::generalsListUI->isReqNewly = true;
			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
			temp->page = 0;
			temp->pageSize = 20;
			temp->type = 1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
			delete temp;
		}

		//�ˢ���󷨽��
		if (GeneralsUI::generalsStrategiesUI->isVisible())
		{
			for(int i = 0;i<bean.generalbase_size();++i)
			{
				CGeneralBaseMsg * temp  = new CGeneralBaseMsg();
				temp->CopyFrom(bean.generalbase(i));
				GeneralsUI::generalsStrategiesUI->RefreshOneGeneralsInLine(temp);
				delete temp;

				if(bean.generalbase(i).order() == 0)
				{
					GeneralsUI::generalsStrategiesUI->RefreshAllGeneralsInLine();
				}
			}
		}
		
		//�ˢ�½���
		if (bean.cause() == 2)
		{
			if (GeneralsUI::generalsEvolutionUI->isVisible())
			{
				for (int i = 0;i<bean.generalbase_size();++i)
				{
					CGeneralBaseMsg * temp = new CGeneralBaseMsg();
					temp->CopyFrom(bean.generalbase(i));
					if (GeneralsUI::generalsEvolutionUI->curEvolutionGeneralId == temp->id())
					{
						bool isUpgrade = false;
						if (bean.cause() == 2)
						{
							if(temp->evolution() - GeneralsUI::generalsEvolutionUI->curEvolutionGeneralBaseMsg->evolution() > 0)
							{
								//mainScene->addInterfaceAnm("jhdjts/jhdjts.anm");
								MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
								if (mainAnmScene)
								{
									mainAnmScene->addInterfaceAnm("jhdjts/jhdjts.anm");
								}
								isUpgrade = true;
							}
						}

						GeneralsUI::generalsEvolutionUI->refreshEvolutionHead(temp,isUpgrade);
						//add bonusSpecialEffect
						GeneralsUI::generalsEvolutionUI->AddBonusSpecialEffect();
						//�ˢ��һ���б
						GeneralsUI::generalsEvolutionUI->isReqNewly = true;
						GeneralsUI::ReqListParameter * tempReq = new GeneralsUI::ReqListParameter();
						tempReq->page = 0;
						tempReq->pageSize = GeneralsUI::generalsListUI->everyPageNum;
						tempReq->type = 3;
						tempReq->generalId = temp->id();
						GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,tempReq);
						delete tempReq;
					}

					delete temp;
				}
			}
		}	
		
		//�ˢ�´������
		if (bean.cause() == 1)
		{
			if (GeneralsUI::generalsTeachUI->isVisible())
			{
				for (int i = 0;i<bean.generalbase_size();++i)
				{
					CGeneralBaseMsg * temp = new CGeneralBaseMsg();
					temp->CopyFrom(bean.generalbase(i));
					if (GeneralsUI::generalsTeachUI->curTeachGeneralId == temp->id())
					{
						if (bean.cause() == 1)
						{
							if(temp->currentquality() - GeneralsUI::generalsTeachUI->curTeachGeneral->currentquality() > 0)
							{
								//mainScene->addInterfaceAnm("cgdjts/cgdjts.anm");
								MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
								if (mainAnmScene)
								{
									mainAnmScene->addInterfaceAnm("cgdjts/cgdjts.anm");
								}
							}
						}

						GeneralsUI::generalsTeachUI->refreshTeachHeadItem(temp);
						//add bonusSpecialEffect
						GeneralsUI::generalsTeachUI->AddBonusSpecialEffect();
						/*GeneralsUI::generalsTeachUI->setToDefault();*/
						//�ˢ��һ���б
						GeneralsUI::generalsTeachUI->isReqNewly = true;
						GeneralsUI::ReqListParameter * tempReq = new GeneralsUI::ReqListParameter();
						tempReq->page = 0;
						tempReq->pageSize = GeneralsUI::generalsListUI->everyPageNum;
						tempReq->type = 2;
						tempReq->generalId = temp->id();
						GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,tempReq);
						delete tempReq;
					}

					delete temp;
				}
			}
		}
	
// 		for(int i =0;i<bean.generalbase_size();++i)
// 		{
// 			GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)bean.generalbase(i).id());
// 		}
	}	

	//get change of  general fightPoint 
	int m_new_AllFightPoint = GameView::getInstance()->myplayer->player->fightpoint();
	for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	{
		m_new_AllFightPoint += GameView::getInstance()->generalsInLineList.at(i)->fightpoint();
	}
	if (m_old_AllFightPoint != m_new_AllFightPoint)
	{
// 		FightPointChangeEffect * fightEffect_ = FightPointChangeEffect::create(m_old_AllFightPoint,m_new_AllFightPoint);
// 		if(fightEffect_)
// 		{
// 			fightEffect_->setTag(kTagFightPointChange);
// 			fightEffect_->setLocalZOrder(SCENE_STORY_LAYER_BASE_ZORDER);
// 			GameView::getInstance()->getMainUIScene()->addChild(fightEffect_);
// 		}
		/*
		MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
		if (mainAnmScene)
		{
			mainAnmScene->addFightPointChangeEffect(m_old_AllFightPoint,m_new_AllFightPoint);
		}
		*/
		EffectOfFightPointChange * temp_effect = new EffectOfFightPointChange();
		temp_effect->setOldFight(m_old_AllFightPoint);
		temp_effect->setNewFight(m_new_AllFightPoint);
		temp_effect->setType(type_fightPoint);
		EffectDispatcher::pushEffectToVector(temp_effect);
	}
}
