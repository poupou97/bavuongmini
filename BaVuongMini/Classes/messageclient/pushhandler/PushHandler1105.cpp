
#include "PushHandler1105.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../gamescene_state/role/General.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/GameScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CMonsterBaseInfo.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../newcomerstory/NewCommerStoryManager.h"
#include "../../gamescene_state/role/ShowBaseGeneral.h"

IMPLEMENT_CLASS(PushHandler1105)

PushHandler1105::PushHandler1105() 
{
    
}
PushHandler1105::~PushHandler1105() 
{
    
}
void* PushHandler1105::createInstance()
{
    return new PushHandler1105() ;
}
void PushHandler1105::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1105::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1105::handle(CommonMessage* mb)
{
	Push1105 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, position( %d, %d ), role id: %lld", mb->cmdid(), bean.x(), bean.y(), bean.playerid());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	/***** 游戏中 ******/
	createRoleInScene(&bean);
	return;

	//BaseFighter* otherPlayer = dynamic_cast<BaseFighter*>(scene->getActor(bean.playerid()));
	GameActor* actor = scene->getActor(bean.playerid());
	if(actor == NULL){
		createRoleInScene(&bean);
	}else{
		//CCAssert(false, "server should not re-push the role info");
		return;

		//// 已有角色
		//if(bean.newenter() == 1){
		//	//if (bean.flag != 0 && aa.type == BaseFighter.TYPE_PLAYER) {
		//	//	// 属性可能有变化，需要重置
		//	//	OtherPlayer op = (OtherPlayer) aa;
		//	//	reset(op, bean, true);
		//	//}
		//	//resetRoleInScene(bean, aa);

		//	// 由于是新进入的actor，只需设置其位置，无需设定移动目的地
		//	otherPlayer->setWorldPosition(Vec2(bean.x(), bean.y()));
		//	otherPlayer->setPosition(scene->convertToCocos2DSpace(otherPlayer->getWorldPosition()));
		//}else{
		//	//if (aa.type == BaseFighter.TYPE_PLAYER) {
		//	//	// 属性可能有变化，需要重置
		//	//	OtherPlayer op = (OtherPlayer) aa;
		//	//	// 不用设置角色方向
		//	//	reset(op, bean, false);
		//	//}
		//	// 同屏玩家，设置其位置，或者移动的目标，无需进行resetRoleInScene()

		//	// setup move command
		//	BaseFighterCommandMove* cmd = new BaseFighterCommandMove();
		//	//cmd->targetPosition = Vec2(bean.x(), bean.y());
		//	if(bean.movestate() == 0 /*&& bean.touchx()  == 0 && bean.touchy() == 0*/)
		//	{
		//		cmd->targetPosition = Vec2(bean.x(), bean.y());
		//		cmd->stopAtEndPoint = true;
		//	}
		//	else
		//	{
		//		cmd->targetPosition = Vec2(bean.x(), bean.y());
		//		cmd->stopAtEndPoint = false;
		//	}
		//	//else
		//	//{
		//	//	Vec2 target;
		//	//	if(bean.touchx()  == 0 && bean.touchy() == 0)
		//	//	{
		//	//		target.x = bean.x();
		//	//		target.y = bean.y();
		//	//	}
		//	//	else
		//	//	{
		//	//		target.x = bean.touchx();
		//	//		target.y = bean.touchy();
		//	//	}
		//	//	cmd->targetPosition = target;
		//	//	// try search path, and confirm the CommandMove's behaviour
		//	//	otherPlayer->searchPath(cmd->targetPosition);
		//	//	if(cmd->targetPosition.getDistance(otherPlayer->getWorldPosition()) > (64 * 2))
		//	//	//if(otherPlayer->getPaths()->size() >= 2)
		//	//		cmd->stopAtEndPoint = true;
		//	//	else
		//	//		cmd->stopAtEndPoint = false;
		//	//}

		//	//// if otherPlayer is moving to this target, ignore this command
		//	//bool bDoCommand = true;
		//	//ActorCommand* actorCommand = otherPlayer->getCommand();
		//	//BaseFighterCommandMove* currentMoveCmd = NULL;
		//	//if(actorCommand != NULL && actorCommand->getType() == ActorCommand::type_attack)
		//	//{
		//	//	currentMoveCmd = (BaseFighterCommandMove*)actorCommand;
		//	//	if(currentMoveCmd->targetPosition.equals(cmd->targetPosition))
		//	//	{
		//	//		delete cmd;
		//	//		bDoCommand = false;
		//	//	}
		//	//}
		//	//if(bDoCommand)
		//	//	otherPlayer->setNextCommand(cmd, true);
		//	otherPlayer->setNextCommand(cmd, true);
		//}
	}
}

void PushHandler1105::createRoleInScene(Push1105* bean){

	if (!bean->has_activerole()) {
		return;
	}
		
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	// First, clear old actor
	long long roleId = bean->playerid();
	if(BaseFighter::getMyPlayerId() == roleId)
		return;
	GameActor* oldActor = scene->getActor(roleId);
	if(oldActor != NULL) {
		Monster* pMonster = dynamic_cast<Monster*>(oldActor);
		if(pMonster != NULL)
		{
			if(pMonster->isDead())
			{
				scene->putActorToClientVector(oldActor);
				scene->getActorsMap().erase(oldActor->getRoleId());
			}
			else
			{
				scene->removeActor(roleId);
			}
		}
		else
		{
			scene->removeActor(roleId);
		}
		//scene->removeActor(roleId);
	}

	if (bean->activerole().type() == GameActor::type_monster) {
		Monster* monster01 = new Monster();
		monster01->setRoleId(bean->playerid());
		scene->putActor(bean->playerid(), monster01);
		monster01->setAnimDir(bean->face());

		monster01->getActiveRole()->CopyFrom(bean->activerole());

		// first, set world position, then load figure by init()
		monster01->setWorldPosition(Vec2(bean->x(), bean->y()));
		monster01->setRealWorldPosition(Vec2(bean->x(), bean->y()));
		monster01->setPositionImmediately(scene->convertToCocos2DSpace(monster01->getWorldPosition()));
		monster01->setBornPosition(Vec2(bean->x(),bean->y()));
		monster01->init(monster01->getActiveRole()->rolebase().figure().c_str());

		monster01->setGameScene(scene);

		monster01->setActorName(bean->activerole().rolebase().name().c_str());

		// add new actor into cocos2d-x render scene
		Node* actorLayer = scene->getActorLayer();
		actorLayer->addChild(monster01);
		monster01->release();

		// 只有复活的怪物，才有出生效果，第一次进场景，加载出来的怪物没有出生效果
		if(GameScene::s_bornMonsterMap[monster01->getRoleId()] != NULL)
		{
			monster01->onBorn();
		}
		GameScene::s_bornMonsterMap[monster01->getRoleId()] = monster01;

		// set the monster's clazz
		if(bean->activerole().has_templateid())
		{
			int templateId = bean->activerole().templateid();
			monster01->setClazz(StaticDataMonsterBaseInfo::s_monsterBase[templateId]->clazz);
		}

		// add the special foot effect only for the boss
		if(monster01->getClazz() == CMonsterBaseInfo::clazz_boss)
		{
			if (NewCommerStoryManager::getInstance()->IsNewComer())
			{
				// Patch: boss - zhang jiao
				if(monster01->getActiveRole()->templateid() == 499)
				{
					addFootFlagForBoss(monster01);
				}
			}
			else
			{
				addFootFlagForBoss(monster01);
			}
		}

	} else if (bean->activerole().type() == GameActor::type_pet) {

		MyPlayer* pMyPlayer = GameView::getInstance()->myplayer;
		if (NULL != pMyPlayer)
		{
			if (pMyPlayer->getRoleId() == bean->ownerid())						// 自己的“展示”武将
			{
				// 在武将创建出来前，把先前的“展示”武将移除（如果有.的话）
				ShowMyGeneral::getInstance()->generalHide();
			}
			else																// 别人的“展示”武将
			{
				// 将 otherPlayer的武将 去掉
				Node* pTmpActorLayer = scene->getActorLayer();
				if (NULL != pTmpActorLayer)
				{
					OtherPlayer* pTmpOtherPlayer = dynamic_cast<OtherPlayer*>(scene->getActor(bean->ownerid()));
					if (NULL != pTmpOtherPlayer)
					{
						pTmpOtherPlayer->removeShowGeneral();
					}
				}
			}
		}

		//refreshGeneralsInfoInArena(bean);

		General* pActor = new General();
		pActor->setRoleId(bean->playerid());
		scene->putActor(bean->playerid(), pActor);

		pActor->setOwnerId(bean->ownerid());

		pActor->getActiveRole()->CopyFrom(bean->activerole());

		// init position
		pActor->setWorldPosition(Vec2(bean->x(), bean->y()));
		pActor->setRealWorldPosition(Vec2(bean->x(), bean->y()));
		// 初始化时，将其cocos postion也直接进行初始化，可以解决切换武将时的龙魂链条异常问题
		pActor->setPositionImmediately(scene->convertToCocos2DSpace(pActor->getWorldPosition()));
		
		if(pActor->isMyPlayerGroup())
		{
			pActor->setAnimDir(GameView::getInstance()->myplayer->getAnimDir());   // 主角自己的武将，跟主角一个方向
		}
		pActor->init(pActor->getActiveRole()->rolebase().figure().c_str());
		pActor->setActorName(bean->activerole().rolebase().name().c_str());
		pActor->showActorName(true);
		if(pActor->isMyPlayerGroup())
			pActor->showFlag(true);

		pActor->setGameScene(scene);

		// 展示武将的进化等级
		pActor->addRank(pActor->getActiveRole()->generalbaseinfo().evolution());
		// show general's rare icon
		//CGeneralBaseMsg * generalBaseMsg = GeneralsStateManager::getInstance()->getGeneralBaseMsgById( pActor->getActiveRole()->rolebase().roleid());
		//if(generalBaseMsg != NULL)
		//	pActor->addRareIcon(generalBaseMsg->rare());
		int modelId = pActor->getActiveRole()->generalbaseinfo().templateid();
		CGeneralBaseMsg * generalBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[modelId];
		pActor->addRareIcon(generalBaseMsgFromDb->rare());

		//add by yangjun 2014.9.18
		//change by yangjun 2014.9.26
// 		if(pActor->isMyPlayerGroup())// 主角自己的武将，刷新武将头像
// 		{
// 			MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
// 			if(mainScene)
// 			{
// 				mainScene->RefreshGeneralInfo(pActor);
// 			}
// 		}

		// add new actor into cocos2d-x render scene
		Node* actorLayer = scene->getActorLayer();
		actorLayer->addChild(pActor);
		pActor->release();

		// 死亡
		//if (activeRole != null && activeRole.stat == 1) {
		//	aActor.setAct(BaseFighter.ACT_FALL);
		//}

		BasePlayer* master = dynamic_cast<BasePlayer*>(scene->getActor(pActor->getOwnerId()));
		if(master != NULL) {
			// 更新武将列表
			master->updateAliveGeneralsList();
		}
		if(pActor->isMyPlayerGroup()) {
			// 创建武将头像
			//change by yangjun 2014.3.12
// 			MainScene * mainScene = GameView::getInstance()->getMainUIScene();
// 			if (mainScene != NULL)
// 				mainScene->RefreshGeneral();

			// myplayer->getAliveGenerals()

			//CCLegendAnimation* effectNode = CCLegendAnimation::create("animation/texiao/renwutexiao/appear.anm");
			//pActor->addEffect(effectNode, false, 0);

			pActor->onBorn();
		}

	} else if(bean->activerole().type() == GameActor::type_player){
		OtherPlayer* otherPlayer = new OtherPlayer();
		otherPlayer->setRoleId(bean->playerid());
		scene->putActor(bean->playerid(), otherPlayer);

		otherPlayer->getActiveRole()->CopyFrom(bean->activerole());
		CCAssert(bean->activerole().has_profession(), "profession filed should not be nil");
		otherPlayer->setProfession(BasePlayer::getProfessionIdxByName(bean->activerole().profession()));

		otherPlayer->setMoveSpeed(BASIC_PLAYER_MOVESPEED);
		// 由于是新进入的actor，只需设置其位置，无需设定移动目的地
		otherPlayer->setWorldPosition(Vec2(bean->x(), bean->y()));
		otherPlayer->setRealWorldPosition(Vec2(bean->x(), bean->y()));
		otherPlayer->setPositionImmediately(scene->convertToCocos2DSpace(otherPlayer->getWorldPosition()));
		otherPlayer->init(otherPlayer->getActiveRole()->rolebase().figure().c_str());
		otherPlayer->setActorName(bean->activerole().rolebase().name().c_str());
		otherPlayer->showActorName(true);

		otherPlayer->setGameScene(scene);
		//if pleyer enter field show weapEffect
		
		otherPlayer->initWeaponEffect();

		// add new actor into cocos2d-x render scene
		Node* actorLayer = scene->getActorLayer();
		actorLayer->addChild(otherPlayer);
		otherPlayer->release();

		if (GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::coliseum)
		{
			MissionAndTeam * missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
			if (missionAndTeam)
			{
				missionAndTeam->p_enemyInfoInArena->generalId = bean->playerid();
				missionAndTeam->p_enemyInfoInArena->name = bean->activerole().rolebase().name();
				missionAndTeam->p_enemyInfoInArena->level = bean->activerole().level();
				missionAndTeam->p_enemyInfoInArena->cur_hp = bean->activerole().hp();
				missionAndTeam->p_enemyInfoInArena->max_hp = bean->activerole().maxhp();
			}
			TableView * offLineArenaTableView_right = (TableView*)missionAndTeam->getRightOffLineArenaTableView();
			if (offLineArenaTableView_right)
				offLineArenaTableView_right->reloadData();
		}

		// 其他玩家 展示武将 创建
		ShowOtherGeneral* pShowOtherGeneral = new ShowOtherGeneral();
		pShowOtherGeneral->set_otherPlayer(otherPlayer);
		pShowOtherGeneral->autorelease();

		if (pShowOtherGeneral->canGeneralShow())
		{
			pShowOtherGeneral->generalShow();
		}

	}else if(bean->activerole().type() == GameActor::type_npc){
		// todo, for dynamic NPC

	}
}

void PushHandler1105::refreshGeneralsInfoInArena( Push1105* bean )
{
	if (GameView::getInstance()->getMapInfo()->maptype() != com::future::threekingdoms::server::transport::protocol::coliseum)
		return;

	General* pActor = new General();
	pActor->setRoleId(bean->playerid());
	pActor->setOwnerId(bean->ownerid());
	pActor->getActiveRole()->CopyFrom(bean->activerole());

	if(pActor->isMyPlayerGroup())
	{
		MissionAndTeam::GeneralInfoInArena * temp = new MissionAndTeam::GeneralInfoInArena();
		temp->generalId = bean->playerid();
		temp->name = bean->activerole().rolebase().name().c_str();
		temp->level = bean->activerole().level();
		temp->cur_hp = bean->activerole().maxhp();
		temp->max_hp = bean->activerole().hp();
		
		MissionAndTeam * missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeam)
		{
			bool isExist = false;
			for(unsigned int i = 0;i<missionAndTeam->generalsInArena_mine.size();i++)
			{
				MissionAndTeam::GeneralInfoInArena * general =  missionAndTeam->generalsInArena_mine.at(i);
				if (temp->generalId == general->generalId)
				{
					general->generalId = temp->generalId;
					general->name = temp->name;
					general->level = temp->level;
					general->cur_hp = temp->cur_hp;
					general->max_hp = temp->max_hp;
					isExist = true;

					delete temp;
				}
			}
			if (!isExist)
			{
				missionAndTeam->generalsInArena_mine.push_back(temp);
			}

			TableView * offLineArenaTableView_left = (TableView*)missionAndTeam->getLeftOffLineArenaTableView();
			if (offLineArenaTableView_left)
				offLineArenaTableView_left->reloadData();
		}
	}
	else
	{
		MissionAndTeam::GeneralInfoInArena * temp = new MissionAndTeam::GeneralInfoInArena();
		temp->generalId = bean->playerid();
		temp->name = bean->activerole().rolebase().name().c_str();
		temp->level = bean->activerole().level();
		temp->cur_hp = bean->activerole().maxhp();
		temp->max_hp = bean->activerole().hp();

		MissionAndTeam * missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeam)
		{
			bool isExist = false;
			for(unsigned int i = 0;i<missionAndTeam->generalsInArena_other.size();i++)
			{
				MissionAndTeam::GeneralInfoInArena * general =  missionAndTeam->generalsInArena_other.at(i);
				if (temp->generalId == general->generalId)
				{
					general->generalId = temp->generalId;
					general->name = temp->name;
					general->level = temp->level;
					general->cur_hp = temp->cur_hp;
					general->max_hp = temp->max_hp;
					isExist = true;

					delete temp;
				}
			}
			if (!isExist)
			{
				missionAndTeam->generalsInArena_other.push_back(temp);
			}

			TableView * offLineArenaTableView_right = (TableView*)missionAndTeam->getRightOffLineArenaTableView();
			if (offLineArenaTableView_right)
				offLineArenaTableView_right->reloadData();
		}
	}

	delete pActor;
}

void PushHandler1105::addFootFlagForBoss( BaseFighter* actor )
{
	std::string str_spritePath = "animation/texiao/renwutexiao/bossfazhen/bossfazhen3.png";
	auto pSprite1 = Sprite::create(str_spritePath.c_str());
	pSprite1->setAnchorPoint(Vec2(0.5f, 0.5f));
	FiniteTimeAction* rotate_action = RotateBy::create(3.0f, 90);
	auto repeapAction1 = RepeatForever::create(Sequence::create(rotate_action,NULL));
	pSprite1->runAction(repeapAction1);

	str_spritePath = "animation/texiao/renwutexiao/bossfazhen/bossfazhen4.png";
	auto pSprite2 = Sprite::create(str_spritePath.c_str());
	pSprite2->setScale(1.3f);
	pSprite2->setAnchorPoint(Vec2(0.5f, 0.5f));
	auto  action1 = FadeTo::create(0.25f,64);
	auto  action2 = FadeTo::create(0.25f,192);
	auto  action3 = FadeTo::create(0.5f,255);
	auto  action4 = FadeTo::create(0.25f,192);
	auto  action5 = FadeTo::create(0.25f,64);
	auto  action6 = DelayTime::create(0.1f);
	auto repeapAction2 = RepeatForever::create(
		Sequence::create(action1,action2,action3,action4,action5,action6,NULL));
	pSprite2->runAction(repeapAction2);
	auto repeapAction = RepeatForever::create(Sequence::create(rotate_action,NULL));
	pSprite2->runAction(rotate_action);

	Node* pNode = Node::create();
	//pNode->setCascadeOpacityEnabled(true);
	pNode->setScale(1.1f, 0.55f);

	pNode->addChild(pSprite2);
	pNode->addChild(pSprite1);

	actor->addEffect(pNode, true, 0);
}