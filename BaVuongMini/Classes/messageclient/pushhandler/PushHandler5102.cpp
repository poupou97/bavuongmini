#include "PushHandler5102.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../element/CPkDailyReward.h"
#include "../../ui/backpackscene/BattleAchievementData.h"
#include "../../ui/backpackscene/BattleAchievementUI.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../ui/GameUIConstant.h"
#include "../element/CRewardBase.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Reward_ui/RewardUi.h"

IMPLEMENT_CLASS(PushHandler5102)

PushHandler5102::PushHandler5102() 
{

}
PushHandler5102::~PushHandler5102() 
{
	
}
void* PushHandler5102::createInstance()
{
	return new PushHandler5102() ;
}
void PushHandler5102::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5102::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5102::handle(CommonMessage* mb)
{
	Push5102 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// ������ȡ״̬
	bool bHasGetGift = bean.rewardstatus();
	BattleAchievementData::instance()->set_hasGetGift(bHasGetGift);

	MainScene * scene = GameView::getInstance()->getMainUIScene();
	if (NULL != scene)
	{
		// ��ʼ��UI��
		BattleAchievementUI *tmpBattleAchUI = (BattleAchievementUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBattleAchievementUI);
		if (NULL != tmpBattleAchUI)
		{
			tmpBattleAchUI->initDataFromInternet();
		}
	}

	if (bHasGetGift == false)
	{
		//push remind list
		bool isHave = false;
		for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
		{
			if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_BATTLEACHIEVEMENT)
			{
				isHave = true;
			}
		}

		if (isHave == false)
		{
			RewardUi::addRewardListEvent(REWARD_LIST_ID_BATTLEACHIEVEMENT);
		}
	}else
	{
		//remove remind list
		RewardUi::removeRewardListEvent(REWARD_LIST_ID_BATTLEACHIEVEMENT);
	}
}