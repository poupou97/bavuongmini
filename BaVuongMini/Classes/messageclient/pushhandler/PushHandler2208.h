
#ifndef Blog_C___Reflection_PushHandler2208_h
#define Blog_C___Reflection_PushHandler2208_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler2208 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler2208)
    
public:
    SYNTHESIZE(PushHandler2208, int*, m_pValue)
    
    PushHandler2208() ;
    virtual ~PushHandler2208() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
