#ifndef Blog_C___Reflection_PushHandler2804_h
#define Blog_C___Reflection_PushHandler2804_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class CPropReward;

class PushHandler2804 : public CKBaseClass, public PushHandlerProtocol,public Node
{
private:
	DECLARE_CLASS(PushHandler2804)

public:
	SYNTHESIZE(PushHandler2804, int*, m_pValue)

		PushHandler2804() ;
	virtual ~PushHandler2804() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

	void showQuestionEndUI(Node* sender);

protected:
	int *m_pValue ;

} ;

#endif
