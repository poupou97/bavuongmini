
#ifndef Blog_C___Reflection_PushHandler1125_h
#define Blog_C___Reflection_PushHandler1125_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "../protobuf/MainMessage.pb.h"

using namespace std;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1125 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1125)
public:
	SYNTHESIZE(PushHandler1125, int*, m_pValue)

		PushHandler1125() ;
	virtual ~PushHandler1125() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;
} ;

#endif