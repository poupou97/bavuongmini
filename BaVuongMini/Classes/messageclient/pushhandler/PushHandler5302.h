
#ifndef Blog_C___Reflection_PushHandler5302_h
#define Blog_C___Reflection_PushHandler5302_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5302 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5302)

public:
	SYNTHESIZE(PushHandler5302, int*, m_pValue)

		PushHandler5302() ;
	virtual ~PushHandler5302() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
