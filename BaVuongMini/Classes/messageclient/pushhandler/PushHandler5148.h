
#ifndef Blog_C___Reflection_PushHandler5148_h
#define Blog_C___Reflection_PushHandler5148_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"

USING_NS_CC;

class PushHandler5148 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5148)

public:
	SYNTHESIZE(PushHandler5148, int*, m_pValue)

	PushHandler5148() ;
	virtual ~PushHandler5148() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);


protected:
	int *m_pValue ;
} ;

#endif
