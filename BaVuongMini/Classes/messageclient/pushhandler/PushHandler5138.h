#ifndef Blog_C___Reflection_PushHandler5138_h
#define Blog_C___Reflection_PushHandler5138_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5138 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5138)

public:
	SYNTHESIZE(PushHandler5138, int*, m_pValue)

	PushHandler5138() ;
	virtual ~PushHandler5138() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

private:
	void initData(int nTargetNum, int nNeedTime);

} ;

#endif
