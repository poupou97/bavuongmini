
#ifndef Blog_C___Reflection_PushHandler3000_h
#define Blog_C___Reflection_PushHandler3000_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"
#include "cocos2d.h"

USING_NS_CC;

class PushHandler3000 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler3000)

public:
	SYNTHESIZE(PushHandler3000, int*, m_pValue)

		PushHandler3000() ;
	virtual ~PushHandler3000() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
