#include "PushHandler2804.h"
#include "../protobuf/QuestionMessage.pb.h"
#include "../../ui/Question_ui/QuestionData.h"
#include "../../GameView.h"
#include "../element/CPropReward.h"
#include "../../ui/Question_ui/QuestionEndUI.h"
#include "../../gamescene_state/MainScene.h"

#define  PAGE_NUM 10

IMPLEMENT_CLASS(PushHandler2804)

	PushHandler2804::PushHandler2804() 
{

}
PushHandler2804::~PushHandler2804() 
{

}
void* PushHandler2804::createInstance()
{
	return new PushHandler2804() ;
}
void PushHandler2804::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2804::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2804::handle(CommonMessage* mb)
{
	Push2804 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (QuestionData::instance()->get_cardNum() <= 0)
		return;

	std::vector<CPropReward *>tempList;
	// add data
	for (int i = 0;i<bean.propreward_size();i++)
	{
		CPropReward * temp = new CPropReward();
		temp->CopyFrom(bean.propreward(i));
		tempList.push_back(temp);
	}

	//update ui
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	QuestionEndUI *questionEndUI = (QuestionEndUI*)mainscene->getChildByTag(kTagQuestionEndUI);
	if(questionEndUI == NULL)
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		questionEndUI = QuestionEndUI::create();
		GameView::getInstance()->getMainUIScene()->addChild(questionEndUI,0,kTagQuestionEndUI);
		questionEndUI->setIgnoreAnchorPointForPosition(false);
		questionEndUI->setAnchorPoint(Vec2(0.5f, 0.5f));
		questionEndUI->setPosition(Vec2(winSize.width/2, winSize.height/2));
		questionEndUI->initReward(tempList);
		questionEndUI->setVisible(false);

		ActionInterval * m_action =(ActionInterval *)Sequence::create(
			DelayTime::create(1.0f),
			CallFuncN::create(CC_CALLBACK_0(PushHandler2804::showQuestionEndUI, this, questionEndUI)),
			NULL);

		questionEndUI->runAction(m_action);
	}

	//delete data
	std::vector<CPropReward*>::iterator iter;
	for (iter = tempList.begin(); iter != tempList.end(); ++iter)
	{
		delete *iter;
	}
	tempList.clear();

// 	Node* scene = GameView::getInstance()->getGameScene();
// 	if(scene)
// 	{
// 		ActionInterval * m_action =(ActionInterval *)Sequence::create(
// 			DelayTime::create(1.0f),
// 	 		CallFuncN::create(scene, callfuncN_selector(PushHandler2804::showQuestionEndUI)),
// 	 		NULL);
// 	 
// 		scene->runAction(m_action);
// 	}

}

void PushHandler2804::showQuestionEndUI( Node* sender )
{
	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	QuestionEndUI *questionEndUI = (QuestionEndUI*)mainscene->getChildByTag(kTagQuestionEndUI);
	if(questionEndUI)
		questionEndUI->setVisible(true);
}
