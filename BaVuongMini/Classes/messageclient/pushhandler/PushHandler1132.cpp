#include "PushHandler1132.h"

#include "../protobuf/NPCMessage.pb.h"

#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/CSalableCommodity.h"
#include "../../ui/shop_ui/ShopUI.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(PushHandler1132)

PushHandler1132::PushHandler1132() 
{

}
PushHandler1132::~PushHandler1132() 
{

}
void* PushHandler1132::createInstance()
{
	return new PushHandler1132() ;
}
void PushHandler1132::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1132::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1132::handle(CommonMessage* mb)
{
	Push1132 bean;
	bean.ParseFromString(mb->data());

	CCLOG("msg: %d, NPC shop", mb->cmdid());

	//delete old shopItemList
	std::vector<CSalableCommodity*>::iterator iter_shopItemList;
	for (iter_shopItemList = GameView::getInstance()->shopItemList.begin(); iter_shopItemList != GameView::getInstance()->shopItemList.end(); ++iter_shopItemList)
	{
		delete *iter_shopItemList;
	}
	GameView::getInstance()->shopItemList.clear();
	//delete old buyBackList
	std::vector<CSalableCommodity*>::iterator iter_buyBackList;
	for (iter_buyBackList = GameView::getInstance()->buyBackList.begin(); iter_buyBackList != GameView::getInstance()->buyBackList.end(); ++iter_buyBackList)
	{
		delete *iter_buyBackList;
	}
	GameView::getInstance()->buyBackList.clear();

	// NPC���۵���Ʒ�б
	if (bean.commodities_size()>0)
	{
		for (int i = 0;i<bean.commodities_size();++i)
		{
			CSalableCommodity * temp = new CSalableCommodity();
			temp->CopyFrom(bean.commodities(i));
			GameView::getInstance()->shopItemList.push_back(temp);
		}
	}

	// �ɻع���Ʒ�б
	if (bean.buybackcommodities_size()>0)
	{
		for (int i = 0;i<bean.buybackcommodities_size();++i)
		{
			if (bean.buybackcommodities(i).has_goods())
			{
				CSalableCommodity * temp = new CSalableCommodity();
				temp->CopyFrom(bean.buybackcommodities(i));
				GameView::getInstance()->buyBackList.push_back(temp);
			}
		}
	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	if (scene->getMainUIScene()->getChildByTag(kTagShopUI))
	{
		ShopUI * shopUI = (ShopUI*)scene->getMainUIScene()->getChildByTag(kTagShopUI);
		shopUI->ReloadData();
	}
}