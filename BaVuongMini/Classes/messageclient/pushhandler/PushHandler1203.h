
#ifndef Blog_C___Reflection_PushHandler1203_h
#define Blog_C___Reflection_PushHandler1203_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1203 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1203)
    
public:
    SYNTHESIZE(PushHandler1203, int*, m_pValue)
    
    PushHandler1203() ;
    virtual ~PushHandler1203() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
