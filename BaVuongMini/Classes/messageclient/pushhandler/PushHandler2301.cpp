#include "PushHandler2301.h"
#include "../protobuf/DailyMessage.pb.h"
#include "../../ui/Rank_ui/RankDataTableView.h"
#include "../element/CHomeMessage.h"
#include "../../ui/Rank_ui/RankData.h"
#include "../../ui/Rank_ui/RankDb.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Rank_ui/RankUI.h"
#include "../../utils/GameUtils.h"

#define  PAGE_NUM 10

IMPLEMENT_CLASS(PushHandler2301)

PushHandler2301::PushHandler2301() 
{

}
PushHandler2301::~PushHandler2301() 
{
	
}
void* PushHandler2301::createInstance()
{
	return new PushHandler2301() ;
}
void PushHandler2301::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2301::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2301::handle(CommonMessage* mb)
{
	Rsp2301 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// ���µ�ʱ�䣨ʱ����
	long long longTimeBean = bean.time();
	long long longTimeRefreshBean = bean.timerefresh();

	// �ҵ����
	int nMyRank = bean.index();

	if (-1 == RankData::instance()->get_timeFlag())
	{
		long long longTimeFinal = longTimeBean + GameUtils::millisecondNow();
		RankData::instance()->set_timeFlag(longTimeFinal);
	}
	else
	{
		// �ʱ�� �� ����ڵ�ʱ��Աȡ���timeFlag ��� milliscondNow()ڣ���1�������ݿ⣨2���رմ��壬���´򿪣�3��return
		if (GameUtils::millisecondNow() >= RankData::instance()->get_timeFlag())
		{
			//GameView::getInstance()->showAlertDialog("pushHandler2301 timeFlag equals");

			// ʱ�����
			RankDb::instance()->timeIsOut();

			return;
		}
	}

	if (-1 == RankData::instance()->get_timeRefreshFlag())
	{
		RankData::instance()->set_timeRefreshFlag(longTimeRefreshBean);
	}
	else
	{
		if (longTimeRefreshBean != RankData::instance()->get_timeRefreshFlag())
		{
			// �˵��ʱ���ʧЧ
			RankDb::instance()->timeIsOut();
		}
	}

	// ��ǰҳ � ���ҳ�
	int nPageCur = bean.pagenumber();
	int nPageAll = bean.pageall();

	// ��� µ�ǰҳ � ���ҳ� �״̬
	RankDataTableView::instance()->refreshDataTableStatus(nPageCur, nPageAll);

	// ��FamilyData��m_vector_family_internetе���
	RankData::instance()->clearVectorFamilyInternet();

	// HomeMessage���ӵ�vector
	int nSizeHomeMessage = bean.roles_size();
	for (int i = 0; i < nSizeHomeMessage; i++)
	{
		CHomeMessage * homeMessage = new CHomeMessage();
		homeMessage->CopyFrom(bean.roles(i));
		homeMessage->set_timeFlag(RankData::instance()->get_timeFlag());
		homeMessage->set_pageCur(nPageCur);
		homeMessage->set_pageAll(nPageAll);
		homeMessage->set_myRank(nMyRank);
		homeMessage->set_timeRefreshFlag(RankData::instance()->get_timeRefreshFlag());
		RankData::instance()->m_vector_family_internet.push_back(homeMessage);
	}

	// tableName
	std::string strTableName = "t_rank_family";

	// �db��л�ȡ��ݵ�����ʱ�vectorģ�
	std::vector<CHomeMessage *> tmpVectorFamily;
	RankDb::instance()->getDataFromDb(tmpVectorFamily);
	int nSizeVector = tmpVectorFamily.size();
	int nPageCurDb = nSizeVector / PAGE_NUM;


	// ������� �������ݵ ĵ�ǰҳ ��� db��е�ҳ��ʱ���Ż���db
	if (nPageCurDb - 1 < nPageCur)
	{
		// 뽫��ݲ�����ݿ
		RankDb::instance()->insertData(strTableName, RankData::instance()->m_vector_family_internet);
	}
	else
	{
		// ��� µ�ǰҳ � ���ҳ� �״̬
		RankDataTableView::instance()->refreshDataTableStatus(nPageCurDb - 1, nPageAll);
	}


	// �db���ȡ��ݵ�vector(m_vector_family_db)
	RankDb::instance()->getDataFromDb(RankData::instance()->m_vector_family_db);

	// ֪ͨ ���UI���
	if (NULL != GameView::getInstance()->getMainUIScene())
	{
		RankUI* pRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
		if (NULL != pRankUI)
		{
			pRankUI->reloadDataTableView();
		}
	}

	// ������ʱvector
	std::vector<CHomeMessage *>::iterator iterTmpVector;
	for (iterTmpVector = tmpVectorFamily.begin(); iterTmpVector != tmpVectorFamily.end(); iterTmpVector++)
	{
		delete *iterTmpVector;
	}
	tmpVectorFamily.clear();
}