#include "PushHandler2803.h"
#include "../protobuf/QuestionMessage.pb.h"
#include "../../ui/Question_ui/QuestionData.h"
#include "../../GameView.h"
#include "../GameMessageProcessor.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/Question_ui/QuestionUI.h"
#include "../../gamescene_state/MainScene.h"

#define  PAGE_NUM 10

IMPLEMENT_CLASS(PushHandler2803)

PushHandler2803::PushHandler2803() 
{

}
PushHandler2803::~PushHandler2803() 
{
	
}
void* PushHandler2803::createInstance()
{
	return new PushHandler2803() ;
}
void PushHandler2803::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2803::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2803::handle(CommonMessage* mb)
{
	Rsp2803 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int nResult = bean.result();														// �����
	std::string strMsg = bean.msg();												// ������ص���Ϣ����ʱ���ã�
	int nRightAnswerQuestion = bean.right();									// �Ѿ���Ե���Ŀ��Ŀ
	int nCardNum = bean.rewardnumber();										// �ɷ��ƴ��
	int nHasAnswerQuestion = bean.answertimes();						// ��ѻش���

	// QuestionData���������
	QuestionData::instance()->set_result(nResult);
	QuestionData::instance()->set_rightAnswerQuestion(nRightAnswerQuestion);
	QuestionData::instance()->set_cardNum(nCardNum);
	QuestionData::instance()->set_hasAnswerQuestion(nHasAnswerQuestion);

	// QuestionUI���ʾresult
	std::string str_icon_path = "res_ui/font/";
	if (0 == nResult)
	{
		str_icon_path.append("answerno.png");
	}
	else if(1 == nResult)
	{
		str_icon_path.append("answeryes.png");
	}
	MainScene* mianScene = GameView::getInstance()->getMainUIScene();
	if(mianScene)
	{
		if (mianScene->getChildByTag(200))
			mianScene->getChildByTag(200)->removeFromParent();

		Size winSize = Director::getInstance()->getVisibleSize();
		Sprite * sprite_result_icon = Sprite::create(str_icon_path.c_str());
		sprite_result_icon->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_result_icon->setPosition(Vec2(winSize.width/2,winSize.height*0.7f));
		sprite_result_icon->setTag(200);
		sprite_result_icon->setScale(1.0f);
		sprite_result_icon->setOpacity(0);
		mianScene->addChild(sprite_result_icon);

		ActionInterval * m_action =(ActionInterval *)Sequence::create(
			FadeIn::create(0.3f),
			DelayTime::create(0.5f),
			FadeOut::create(0.3f),
			RemoveSelf::create(),
			NULL);
		sprite_result_icon->runAction(m_action);
	}

	// ֪ͨ ���QuestionUI���
	if (NULL != GameView::getInstance()->getMainUIScene())
	{
		QuestionUI* pQuestionUI = (QuestionUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
		if (NULL != pQuestionUI)
		{
			pQuestionUI->initQuestionInfoFromServer();
			pQuestionUI->initBaseInfoFromServer();

			if (QuestionData::instance()->get_hasAnswerQuestion() <= 20)
			{
				pQuestionUI->HandleResult(nResult,bean.trueoption());
			}
		}
	}
}