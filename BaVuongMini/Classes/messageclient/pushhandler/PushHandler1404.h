
#ifndef Blog_C___Reflection_PushHandler1404_h
#define Blog_C___Reflection_PushHandler1404_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1404 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1404)
    
public:
    SYNTHESIZE(PushHandler1404, int*, m_pValue)
    
    PushHandler1404() ;
    virtual ~PushHandler1404() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
