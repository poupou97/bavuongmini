
#include "PushHandler1304.h"

#include "../protobuf/ItemMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../GameView.h"
#include "../element/CEquipment.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "../../gamescene_state/role/MyPlayer.h"

IMPLEMENT_CLASS(PushHandler1304)

PushHandler1304::PushHandler1304() 
{

}
PushHandler1304::~PushHandler1304() 
{

}
void* PushHandler1304::createInstance()
{
	return new PushHandler1304() ;
}
void PushHandler1304::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1304::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1304::handle(CommonMessage* mb)
{
	Push1304 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, role id: %lld", mb->cmdid(), bean.roleid());

	int roleId = bean.roleid();
	int hp = bean.hp();
	int mp = bean.mp();
	int maxhp = bean.maxhp();
	int maxmp = bean.maxmp();
	int pob = bean.pob();
	std::string avatar = bean.avatar();
	int wingHaloStyle = bean.winghalostyle();
	int wingColre = bean.wingcolor();

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if (scene != NULL)
	{
		OtherPlayer* other = dynamic_cast<OtherPlayer*>(scene->getActor(roleId));
		if(other != NULL)
		{
			other->getActiveRole()->mutable_equipmentshowinfo()->CopyFrom(bean.equipmentshowinfo());

			// switch equiment for the player
			switch (pob) {		
			case CEquipment::HAND:
			{
				other->initWeaponEffect();

				BasePlayer::switchWeapon(bean.avatar().c_str(), other->getAnim(), other->getProfession());
				other->getActiveRole()->set_hand(bean.avatar());
			}break;	
			default:
				break;
			}
		}
	}
}
