
#include "PushHandler5013.h"

#include "../protobuf/CollectMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../element/CPickingInfo.h"
#include "../../GameView.h"
#include "../../legend_engine/GameWorld.h"
#include "../element/CPushCollectInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../gamescene_state/role/PresentBox.h"
#include "../../utils/GameConfig.h"

IMPLEMENT_CLASS(PushHandler5013)

PushHandler5013::PushHandler5013() 
{
    
}
PushHandler5013::~PushHandler5013() 
{
    
}
void* PushHandler5013::createInstance()
{
    return new PushHandler5013() ;
}
void PushHandler5013::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5013::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler5013::handle(CommonMessage* mb)
{
	PushBoxState5013 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
	{
		return;
	}
		
	long long tmpInstanceId = bean.instanceid();
	int xPos = bean.x();
	int yPos = bean.y();


	PresentBox * pPresentBox = new PresentBox();
	pPresentBox->setRoleId(tmpInstanceId);
	//pPresentBox->init("baoxiang1");
	std::string str_animation = GameConfig::getStringForKey("present_animation");
	pPresentBox->initPresentBox(str_animation.c_str());

	if (ACTIVITY == bean.state())
	{
		pPresentBox->setActivied(true);
	}
	else if (INVAILD == bean.state())
	{
		pPresentBox->setActivied(false);
	}

	// ���Ϊ�����ã�˵���͵���Ϣ� Ǵ˱��䱻������ʰȡ����Ҫ��Ķ������ڵ�ͼ���Ƴ�˱��
	if (false == pPresentBox->getActivied())
	{
		PresentBox * tmpPresentBox = (PresentBox*)GameView::getInstance()->getGameScene()->getActor(tmpInstanceId);
		if (NULL != tmpPresentBox)
		{
			scene->removeActor(tmpInstanceId);

			pPresentBox->release();

			return;
		}
	}

	int actualPosX = GameSceneLayer::tileToPositionX(xPos);
	int actualPosY = GameSceneLayer::tileToPositionY(yPos);

	pPresentBox->setWorldPosition(Vec2(actualPosX, actualPosY));
	pPresentBox->setGameScene(scene);

	scene->putActor(tmpInstanceId, pPresentBox);

	// add new actor into cocos2d-x render scene
	Node * actorLayer = scene->getActorLayer();
	actorLayer->addChild(pPresentBox);

	pPresentBox->release();
}
