
#ifndef Blog_C___Reflection_PushHandler2004_h
#define Blog_C___Reflection_PushHandler2004_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"

USING_NS_CC;

class PushHandler2004 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler2004)

public:
	SYNTHESIZE(PushHandler2004, int*, m_pValue)

		PushHandler2004() ;
	virtual ~PushHandler2004() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
