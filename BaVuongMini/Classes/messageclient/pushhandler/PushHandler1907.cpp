#include "PushHandler1907.h"

#include "../protobuf/CopyMessage.pb.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"

IMPLEMENT_CLASS(PushHandler1907)

PushHandler1907::PushHandler1907() 
{

}
PushHandler1907::~PushHandler1907() 
{

}
void* PushHandler1907::createInstance()
{
	return new PushHandler1907() ;
}
void PushHandler1907::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1907::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1907::handle(CommonMessage* mb)
{
	Push1907 bean;
	bean.ParseFromString(mb->data());

	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();

	if (!mainscene)
		return;

	if (MissionAndTeam::getPanelType() != MissionAndTeam::type_SingCopyChanllengeOnly)
		return;

	MissionAndTeam * missionAndTeam = (MissionAndTeam *)mainscene->getChildByTag(kTagMissionAndTeam);
	if (!missionAndTeam)
		return;
	
	missionAndTeam->updateSingCopyCellInfo(bean.level(),bean.curmonsternumber(),bean.monsternumbers(),bean.remainmonster(),bean.usetime(),bean.timecount());
}