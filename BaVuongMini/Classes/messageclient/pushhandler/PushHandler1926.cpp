#include "PushHandler1926.h"

#include "../protobuf/CopyMessage.pb.h"
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../ui/FivePersonInstance/InstanceEndUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/CFivePersonInstanceEndInfo.h"
#include "../element/CRewardProp.h"
#include "../../ui/FivePersonInstance/InstanceRewardUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/GoodsInfo.h"
#include "../../ui/FivePersonInstance/InstanceDetailUI.h"

IMPLEMENT_CLASS(PushHandler1926)

PushHandler1926::PushHandler1926() 
{

}
PushHandler1926::~PushHandler1926() 
{

}
void* PushHandler1926::createInstance()
{
	return new PushHandler1926() ;
}
void PushHandler1926::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1926::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1926::handle(CommonMessage* mb)
{
	Rsp1926 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, instance is over", mb->cmdid());

	//delete old
	std::vector<GoodsInfo*>::iterator iter;
	for (iter = FivePersonInstance::getInstance()->s_m_rewardGoods.begin(); iter != FivePersonInstance::getInstance()->s_m_rewardGoods.end(); ++iter)
	{
		delete *iter;
	}
	FivePersonInstance::getInstance()->s_m_rewardGoods.clear();
	//add new
	for (int i = 0;i<bean.goods_size();i++)
	{
		GoodsInfo * goodsinfo = new GoodsInfo();
		goodsinfo->CopyFrom(bean.goods(i));
		FivePersonInstance::getInstance()->s_m_rewardGoods.push_back(goodsinfo);
	}
	//refersh UI
	MainScene* mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	CCAssert(mainLayer != NULL, "should not be nil");

	InstanceDetailUI * _ui = (InstanceDetailUI*)mainLayer->getChildByTag(kTagInstanceDetailUI);
	if(_ui != NULL)
	{
		_ui->RefreshRewardsInfo();
	}

// 	if (mainLayer->getChildByTag(kTagInstanceRewardUI) != NULL)
// 	{
// 		InstanceRewardUI * temp = (InstanceRewardUI*)mainLayer->getChildByTag(kTagInstanceRewardUI);
// 		for (int i = 0;i<bean.goods_size();i++)
// 		{
// 			GoodsInfo * goodsinfo = new GoodsInfo();
// 			goodsinfo->CopyFrom(bean.goods(i));
// 			temp->rewardGoods.push_back(goodsinfo);
// 		}
// 		temp->ReloadData();
// 	}

}