
#include "PushHandler2202.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"

#include "../protobuf/RelationMessage.pb.h"
#include "GameView.h"
#include "../element/CRelationPlayer.h"
#include "../../ui/Friend_ui/FriendWorkList.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../ui/Mail_ui/MailFriend.h"
#include "../../ui/Chat_ui/ChatUI.h"

IMPLEMENT_CLASS(PushHandler2202)

PushHandler2202::PushHandler2202() 
{
    
}
PushHandler2202::~PushHandler2202() 
{
    
}
void* PushHandler2202::createInstance()
{
    return new PushHandler2202() ;
}
void PushHandler2202::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2202::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler2202::handle(CommonMessage* mb)
{
	Rsp2202 bean;
	bean.ParseFromString(mb->data());

	int listNum=bean.playerlist_size();

	FriendUi * friend_ui=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);	
	MailFriend * mailFriend =(MailFriend *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagPopFriendListUI);
	if (friend_ui != NULL && friend_ui->pageIndexSelect == 0)
	{
		std::vector<CRelationPlayer *>::iterator iter;
		for (iter=GameView::getInstance()->relationFriendVector.begin();iter!=GameView::getInstance()->relationFriendVector.end();iter++)
		{
			delete *iter;
		}
		GameView::getInstance()->relationFriendVector.clear();

		for (iter=GameView::getInstance()->relationBlackVector.begin();iter!=GameView::getInstance()->relationBlackVector.end();iter++)
		{
			delete *iter;
		}
		GameView::getInstance()->relationBlackVector.clear();


		for (iter=GameView::getInstance()->relationEnemyVector.begin();iter!=GameView::getInstance()->relationEnemyVector.end();iter++)
		{
			delete *iter;
		}
		GameView::getInstance()->relationEnemyVector.clear();


		for (iter=GameView::getInstance()->relationSourceVector.begin();iter!=GameView::getInstance()->relationSourceVector.end();iter++)
		{
			delete *iter;
		}
		GameView::getInstance()->relationSourceVector.clear();
	}
	
	if (mailFriend != NULL && mailFriend->setFriendsPage ==0)
	{
		std::vector<CRelationPlayer *>::iterator iter;
		for (iter=GameView::getInstance()->relationFriendVector.begin();iter!=GameView::getInstance()->relationFriendVector.end();iter++)
		{
			delete *iter;
		}
		GameView::getInstance()->relationFriendVector.clear();

		for (iter=GameView::getInstance()->relationSourceVector.begin();iter!=GameView::getInstance()->relationSourceVector.end();iter++)
		{
			delete *iter;
		}
		GameView::getInstance()->relationSourceVector.clear();
	}
	
	for (int i=0;i<listNum;i++)
	{
		CRelationPlayer * relationPlayerinfo = new CRelationPlayer();
		relationPlayerinfo->CopyFrom(bean.playerlist(i));
		GameView::getInstance()->relationSourceVector.push_back(relationPlayerinfo);

		switch(bean.relationtype())
		{
		case 0:
			{
				CRelationPlayer * relationFriend = new CRelationPlayer();
				relationFriend->CopyFrom(bean.playerlist(i));
				//friend
				GameView::getInstance()->relationFriendVector.push_back(relationFriend);
			}break;
		case 1:
			{
				CRelationPlayer * relationblack = new CRelationPlayer();
				relationblack->CopyFrom(bean.playerlist(i));
				//black	
				GameView::getInstance()->relationBlackVector.push_back(relationblack);
			}break;
		case 2:
			{
				//enemy 
				CRelationPlayer * relationEnemy = new CRelationPlayer();
				relationEnemy->CopyFrom(bean.playerlist(i));
				GameView::getInstance()->relationEnemyVector.push_back(relationEnemy);
			}break;
		}
	}

	if (mailFriend !=NULL)
	{
		if (listNum<=0)
		{
			mailFriend->setShowBMfriendIsNull(true);
		}else
		{
			mailFriend->totalFriendsPageNum= bean.totalpagenum();
			mailFriend->setShowBMfriendIsNull(false);
			TableView * friendTab =(TableView *)mailFriend->getChildByTag(MAILFRIENDTABVIEW);

			Vec2 off_ = friendTab->getContentOffset();
			int h_ = friendTab->getContentSize().height+ off_.y;
			friendTab->reloadData();
			Vec2 temp = Vec2(off_.x,h_- friendTab->getContentSize().height);
			friendTab->setContentOffset(temp);
		}
	}

	if (friend_ui !=NULL)
	{
		friend_ui->totalPlayerNum_ = bean.totalplayernum();
		friend_ui->reloadSourceData(friend_ui->totalPlayerNum_);

		FriendWorkList * friendworklist_ =(FriendWorkList *)friend_ui->getChildByTag(FRIENDADDLAYER)->getChildByTag(FRIENDWORKLISTTAG);
		TableView * friendTab =(TableView *)friendworklist_->getChildByTag(FRIENDWORKLISTTAB);

		friendworklist_->totalPageNum = bean.totalpagenum();

		Vec2 off_ = friendTab->getContentOffset();
		int h_ = friendTab->getContentSize().height+ off_.y;
		friendTab->reloadData();
		Vec2 temp = Vec2(off_.x,h_- friendTab->getContentSize().height);
		friendTab->setContentOffset(temp);
	}
}
