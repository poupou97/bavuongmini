
#include "PushHandler3001.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../protobuf/OfflineFight.pb.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../../ui/GameUIConstant.h"
#include "../element/CRewardBase.h"
#include "../../ui/Reward_ui/RewardUi.h"

IMPLEMENT_CLASS(PushHandler3001)

	PushHandler3001::PushHandler3001() 
{

}
PushHandler3001::~PushHandler3001() 
{

}
void* PushHandler3001::createInstance()
{
	return new PushHandler3001() ;
}
void PushHandler3001::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler3001::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler3001::handle(CommonMessage* mb)
{
	ResUpdateReceiveTime3001 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (GameView::getInstance()->getGameScene() == NULL)
		return;

	if (GameView::getInstance()->getMainUIScene() == NULL)
		return;

	long long temp_delayTime = bean.remainreceivetime();
	if (temp_delayTime != 0)
	{
		temp_delayTime+= 1000*60*2;
	}
	
	GameView::getInstance()->getMainUIScene()->setOffAreaPrizeTime(temp_delayTime);
	//GameView::getInstance()->getMainUIScene()->remindOffArenaPrize();

	RewardUi::removeRewardListEvent(REWARD_LIST_ID_OFFARENA);

	OffLineArenaUI * offLineArenaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
	if (offLineArenaUI == NULL)
		return;

	offLineArenaUI->setRemaintimes(bean.remainreceivetime());
}
