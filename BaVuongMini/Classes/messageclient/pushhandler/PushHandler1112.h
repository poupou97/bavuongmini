
#ifndef Blog_C___Reflection_PushHandler1112_h
#define Blog_C___Reflection_PushHandler1112_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

using namespace std;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1112 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1112)
public:
    SYNTHESIZE(PushHandler1112, int*, m_pValue)
    
    PushHandler1112() ;
    virtual ~PushHandler1112() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
} ;

#endif