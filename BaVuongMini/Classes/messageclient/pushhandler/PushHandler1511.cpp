
#include "PushHandler1511.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CGuildRecord.h"
#include "../../ui/family_ui/FamilyUI.h"



IMPLEMENT_CLASS(PushHandler1511)

PushHandler1511::PushHandler1511() 
{
    
}
PushHandler1511::~PushHandler1511() 
{
    
}
void* PushHandler1511::createInstance()
{
    return new PushHandler1511() ;
}
void PushHandler1511::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1511::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1511::handle(CommonMessage* mb)
{
	Rsp1511 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	std::vector<CGuildRecord *>::iterator iter;
	for (iter=GameView::getInstance()->guildRecordVector.begin();iter!=GameView::getInstance()->guildRecordVector.end();iter++)
	{
		delete * iter;
	}
	GameView::getInstance()->guildRecordVector.clear();

	for (int i=0;i<bean.records_size();i++)
	{
		CGuildRecord * guildRecond =new CGuildRecord();
		guildRecond->CopyFrom(bean.records(i));
		GameView::getInstance()->guildRecordVector.push_back(guildRecond);
	}
	FamilyUI * familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);

	if (GameView::getInstance()->guildRecordVector.size() >0)
	{
		familyui->familyState_panel_infoIsNUll->setVisible(false);
	}else
	{
		familyui->familyState_panel_infoIsNUll->setVisible(true);
	}
	
	familyui->tableviewstate->reloadData();
}
