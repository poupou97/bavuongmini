#include "PushHandler1921.h"

#include "../protobuf/CopyMessage.pb.h"
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../ui/FivePersonInstance/InstanceEndUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/CFivePersonInstanceEndInfo.h"
#include "../element/CRewardProp.h"

IMPLEMENT_CLASS(PushHandler1921)

PushHandler1921::PushHandler1921() 
{
    
}
PushHandler1921::~PushHandler1921() 
{
    
}
void* PushHandler1921::createInstance()
{
    return new PushHandler1921() ;
}
void PushHandler1921::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1921::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1921::handle(CommonMessage* mb)
{
	Push1921 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, instance is over", mb->cmdid());

	MainScene* mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	CCAssert(mainLayer != NULL, "should not be nil");

	Size winSize = Director::getInstance()->getVisibleSize();

	if(FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
	{
		CFivePersonInstanceEndInfo * fivePersonInstanceEndInfo = new CFivePersonInstanceEndInfo();
		fivePersonInstanceEndInfo->set_instanceId(bean.copyid());
		fivePersonInstanceEndInfo->set_star(bean.star());
		fivePersonInstanceEndInfo->set_exp(bean.exp());
		fivePersonInstanceEndInfo->set_goldType(bean.goldtype());
		fivePersonInstanceEndInfo->set_gold(bean.gold());
		fivePersonInstanceEndInfo->set_baseExp(bean.basexp());
		fivePersonInstanceEndInfo->set_baseGold(bean.basgold());
		fivePersonInstanceEndInfo->set_timeAdd(bean.timeadd());
		fivePersonInstanceEndInfo->set_skillAdd(bean.skilladd());
		fivePersonInstanceEndInfo->set_vipAdd(bean.vipadd());
		fivePersonInstanceEndInfo->set_dragonValue(bean.dragonvalue());
		fivePersonInstanceEndInfo->set_timeDouble(bean.timedouble());
		for(int i= 0;i<bean.rewardprops_size();++i)
		{
			CRewardProp * rewardProp = new CRewardProp();
			rewardProp->CopyFrom(bean.rewardprops(i));
			fivePersonInstanceEndInfo->rewardProps.push_back(rewardProp);
		}

		InstanceEndUI* _ui=InstanceEndUI::create(fivePersonInstanceEndInfo);
		_ui->setIgnoreAnchorPointForPosition(false);
		_ui->setAnchorPoint(Vec2(0.5f,0.5f));
		_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));
		_ui->setTag(kTagInstanceEndUI);
		mainLayer->addChild(_ui);

		delete fivePersonInstanceEndInfo;
	}
}