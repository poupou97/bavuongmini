
#include "PushHandler1802.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../protobuf/AuctionMessage.pb.h"
#include "GameView.h"
#include "../../ui/Auction_ui/AuctionUi.h"
#include "../element/CAuctionInfo.h"
#include "../../ui/Auction_ui/AuctionTabView.h"
#include "../../utils/StaticDataManager.h"

IMPLEMENT_CLASS(PushHandler1802)

PushHandler1802::PushHandler1802() 
{

}
PushHandler1802::~PushHandler1802() 
{
    
}
void* PushHandler1802::createInstance()
{
    return new PushHandler1802() ;
}
void PushHandler1802::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1802::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1802::handle(CommonMessage* mb)
{
	Rsp1802 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	AuctionUi * auctionui =(AuctionUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	if (bean.result()==1)//�� 1�ɹ� 2��ʧ� ܳɹ��򷵻ؼ�����Ϣ
	{
		CAuctionInfo * auctioninfo=new CAuctionInfo();
		auctioninfo->CopyFrom(bean.auctioninfo());

		CAuctionInfo * auctioninfo1=new CAuctionInfo();
		auctioninfo1->CopyFrom(bean.auctioninfo());

		GameView::getInstance()->auctionConsignVector.push_back(auctioninfo);
		GameView::getInstance()->selfConsignVector.push_back(auctioninfo1);

		const char *strings_  = StringDataManager::getString("auction_successGoods");
		GameView::getInstance()->showAlertDialog(strings_);
		auctionui->successConsign++;

		auctionui->refreshPlayerMoney();
	}else
	{
		return;
	}
	
	TableView * auctionTab=(TableView *)auctionui->getChildByTag(AUCTIONLAYER)->getChildByTag(AUCTIONTABVIEW)->getChildByTag(AUCTIONCCTABVIEW);
	auctionTab->reloadData();	
}
