
#include "PushHandler1106.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"

#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../element/CMapInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/CActiveRole.h"
#include "../../ui/offlinearena_ui/OffLineArenaState.h"
#include "../../utils/GameUtils.h"
#include "../../ui/offlinearena_ui/OffLineArenaCountDownUI.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../ui/challengeRound/SingleCopyResult.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Active_ui/ActiveManager.h"
#include "../../newcomerstory/NewCommerStoryManager.h"
#include "../../gamescene_state/LoadScene.h"

IMPLEMENT_CLASS(PushHandler1106)

PushHandler1106::PushHandler1106() 
{
    
}
PushHandler1106::~PushHandler1106() 
{
    
}
void* PushHandler1106::createInstance()
{
    return new PushHandler1106() ;
}
void PushHandler1106::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1106::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1106::handle(CommonMessage* mb)
{
	Push1106 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, enter new map: %s", mb->cmdid(), bean.mapid().c_str());

	GameView* gv = GameView::getInstance();

	// prepare next level's info
	SceneLoadLayer::s_newMapInfo.CopyFrom(bean);
	SceneLoadLayer::s_myNewWorldPosition = Vec2(bean.x(), bean.y());

	// todo
	//// 场景中的排行榜NPC，比如第一职业的雕像
	//if (!gv.gameUnpackPush.topNpcs.isEmpty()) {
	//	gv.gameUnpackPush.topNpcs.clear();
	//}
	//// 加载场景中所有NPC
	//if (bean.topNpcs != null) {
	//	for (MBTopNpc topNpc : bean.topNpcs) {
	//		gv.gameUnpackPush.topNpcs.put(String.valueOf(topNpc.roleId),
	//				topNpc.name);
	//	}
	//}
	vector<CActiveRole*>::iterator iter;
	for (iter = gv->allNPCsVector.begin(); iter != gv->allNPCsVector.end(); ++iter)
	{
		delete *iter;
	}
	gv->allNPCsVector.clear();

	for(int i = 0; i < bean.npcrole_size(); i++)
	{
		CActiveRole* activeRole = new CActiveRole();
		activeRole->CopyFrom(bean.npcrole(i));
		gv->allNPCsVector.push_back(activeRole);
	}

	//Director::getInstance()->purgeCachedData();
	//Director::getInstance()->getRunningScene()->stopAllActions();
	//GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	//if(scene != NULL)
	//	scene->pauseSchedulerAndActions();

	Scene* pScene = Director::getInstance()->getRunningScene();
	if(pScene->getTag() == GameView::STATE_GAME)
	{
		GameSceneState* pState = (GameSceneState*)pScene;
		if(pState->getState() == GameSceneState::state_load)
		{
			SceneLoadLayer* pLoadSceneNode = (SceneLoadLayer*)pState->getChildByTag(GameSceneState::TAG_LOADSCENE);
			if(pLoadSceneNode != NULL)
			{
				pLoadSceneNode->setState(SceneLoadLayer::State_Init);
				return;
			}
		}
		pState->changeToLoadState();
	}
	else
	{
		SceneLoadLayer* pLoadSceneNode = (SceneLoadLayer*)pScene->getChildByTag(GameSceneState::TAG_LOADSCENE);
		if(pLoadSceneNode != NULL)
			pLoadSceneNode->setState(SceneLoadLayer::State_Init);
	}
}