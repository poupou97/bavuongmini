
#include "PushHandler2005.h"
#include "GameView.h"
#include "../protobuf/MailMessage.pb.h"
#include "../../ui/Mail_ui/MailUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CMailAttachment.h"
#include "../element/CMailInfo.h"
#include "../../ui/extensions/RichTextInput.h"
#include "../element/CAttachedProps.h"
#include "../../ui/Mail_ui/MailList.h"
IMPLEMENT_CLASS(PushHandler2005)

PushHandler2005::PushHandler2005() 
{
    
}
PushHandler2005::~PushHandler2005() 
{
    
}
void* PushHandler2005::createInstance()
{
    return new PushHandler2005() ;
}
void PushHandler2005::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2005::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler2005::handle(CommonMessage* mb)
{
	Rsp2005 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	std::string string_ = bean.msg();
	GameView::getInstance()->showAlertDialog(string_);

	if (bean.result()==1)
	{
		//���ͳɹ� ��շ������ݼ����
		MailUI * mailui = (MailUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMailUi);
		if (mailui== NULL)
		 return;
		//mailui->friendName->deleteAllInputString();
		mailui->titleBox->deleteAllInputString();
		mailui->contentBox->deleteAllInputString();

		mailui->sendMailLayer->removeAllChildren();
		
		mailui->firstIsNull =true;
		mailui->mailAttachment_amount = 0;	

		mailui->sendMailCollectcoins =  0;
		mailui->labelCoins->setString("0");

		mailui->sendMailCollectinIngots =  0;
		mailui->labelIngot->setString("0");

		std::vector<CAttachedProps *>::iterator iter;
		for (iter =mailui->attachePropVector.begin();iter!= mailui->attachePropVector.end();iter++)
		{
			delete *iter;
		}
		mailui->attachePropVector.clear();
	}
}
