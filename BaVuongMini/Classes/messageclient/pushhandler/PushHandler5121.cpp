#include "PushHandler5121.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../ui/skillscene/MusouSkillTalent.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../element/CMusouTalent.h"

IMPLEMENT_CLASS(PushHandler5121)

PushHandler5121::PushHandler5121() 
{

}
PushHandler5121::~PushHandler5121() 
{

}
void* PushHandler5121::createInstance()
{
	return new PushHandler5121() ;
}
void PushHandler5121::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5121::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5121::handle(CommonMessage* mb)
{
	Resp5121 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (!GameView::getInstance())
		return;

	if (bean.talent_size() <= 0 )
		return;
	
	if (bean.talent(0).talentid() == 0)
	{
		GameView::getInstance()->myplayer->setMusouEssence(bean.dragonvalue());
		if (GameView::getInstance()->getMainUIScene())
		{
			SkillScene *skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			if (skillScene)
			{
				skillScene->musouSkillTalent->RefreshEssenceValue();
			}
		}
	}
	else
	{
		//update data
		std::map<int,int>::iterator iter;
		for (iter = GameView::getInstance()->m_musouTalentList.begin(); iter != GameView::getInstance()->m_musouTalentList.end(); ++iter)
		{
			MusouTalentConfigData::IdAndLevel temp;
			temp.idx = iter->first;
			temp.level = iter->second;

			if (temp.idx == bean.talent(0).talentid())
			{
				iter->second = bean.talent(0).level();
			}
		}

		GameView::getInstance()->myplayer->setMusouEssence(bean.dragonvalue());

		if (GameView::getInstance()->getMainUIScene())
		{
			SkillScene *skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			if (skillScene)
			{
				for(int i =0;i<skillScene->musouSkillTalent->curMusouTalentList.size();++i)
				{
					if (skillScene->musouSkillTalent->curMusouTalentList.at(i)->get_idx() == bean.talent(0).talentid())
					{
						MusouTalentConfigData::IdAndLevel temp;
						temp.idx = bean.talent(0).talentid();
						temp.level = bean.talent(0).level();

						std::map<MusouTalentConfigData::IdAndLevel,CMusouTalent*>::const_iterator cIter;
						cIter = MusouTalentConfigData::s_musouTalentMsg.find(temp);
						if (cIter == MusouTalentConfigData::s_musouTalentMsg.end()) // û�ҵ�����ָ�END��  
						{
							return;
						}
						else
						{
							CMusouTalent * musouTalent = skillScene->musouSkillTalent->curMusouTalentList.at(i);
							musouTalent->copyFrom(cIter->second);
							skillScene->musouSkillTalent->RefreshTalentInfo(skillScene->musouSkillTalent->curMusouTalentList.at(i));
							skillScene->musouSkillTalent->RefreshTalentItemLevel(skillScene->musouSkillTalent->curMusouTalentList.at(i));
							skillScene->musouSkillTalent->AddBonusSpecialEffect(skillScene->musouSkillTalent->curMusouTalentList.at(i));
						}

					}
				}
				skillScene->musouSkillTalent->RefreshEssenceValue();

			}
		}
	}
}