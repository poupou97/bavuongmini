
#include "PushHandler1803.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../protobuf/AuctionMessage.pb.h"
#include "GameView.h"
#include "../../ui/Auction_ui/AuctionUi.h"
#include "../../ui/Auction_ui/AuctionTabView.h"
#include "../element/CAuctionInfo.h"
IMPLEMENT_CLASS(PushHandler1803)

PushHandler1803::PushHandler1803() 
{
    
}
PushHandler1803::~PushHandler1803() 
{
    
}
void* PushHandler1803::createInstance()
{
    return new PushHandler1803() ;
}
void PushHandler1803::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1803::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1803::handle(CommonMessage* mb)
{
	Rsp1803 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	

	bean.optype();//2��ͣ� 3ۡ�ȡ� 4ء���Ǯ 5�����
	bean.result();//1򡢳ɹ� 2��ʧ�
	bean.auctionid();//ܼ���ƷID
	
	if (bean.result()==1)
	{ 	
		for (int i=0;i<GameView::getInstance()->auctionSourceVector.size();i++)
		{
			if (bean.auctionid() == GameView::getInstance()->auctionSourceVector.at(i)->id())
			{
				std::vector<CAuctionInfo *>::iterator iter=GameView::getInstance()->auctionSourceVector.begin()+i;
				CAuctionInfo * auction= *iter;
				GameView::getInstance()->auctionSourceVector.erase(iter);
				delete auction;
			}
		}
		AuctionUi * auctionui =(AuctionUi*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
		TableView * auctionTab=(TableView *)auctionui->getChildByTag(AUCTIONLAYER)->getChildByTag(AUCTIONTABVIEW)->getChildByTag(AUCTIONCCTABVIEW);
		auctionTab->reloadData();

		if (bean.optype() == 3)
		{
			auctionui->successConsign--;
			int consignNum = GameView::getInstance()->auctionSourceVector.size();
			std::string consignBegin="";
			char consignSecond[10];
			sprintf(consignSecond,"%d",consignNum);
			char consignMaxGoodsNum[10];
			sprintf(consignMaxGoodsNum,"%d",auctionui->curMaxConsignGoodsNum);
			consignBegin.append(consignSecond);
			consignBegin.append("/");
			consignBegin.append(consignMaxGoodsNum);
			auctionui->consignAmount->setString(consignBegin.c_str());
		}
		
		for (int i=0;i<GameView::getInstance()->auctionConsignVector.size();i++)
		{
			if (bean.auctionid() == GameView::getInstance()->auctionConsignVector.at(i)->id())
			{
				std::vector<CAuctionInfo *>::iterator iter=GameView::getInstance()->auctionConsignVector.begin()+i;
				CAuctionInfo * auction= *iter;
				GameView::getInstance()->auctionConsignVector.erase(iter);
				delete auction;
			}
		}

		for (int i=0;i<GameView::getInstance()->selfConsignVector.size();i++)
		{
			if (bean.auctionid() == GameView::getInstance()->selfConsignVector.at(i)->id())
			{
				std::vector<CAuctionInfo *>::iterator iter=GameView::getInstance()->selfConsignVector.begin()+i;
				CAuctionInfo * auction= *iter;
				GameView::getInstance()->selfConsignVector.erase(iter);
				delete auction;
			}
		}

		auctionui->refreshPlayerMoney();
	}
}
