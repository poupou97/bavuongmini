
#ifndef Blog_C___Reflection_PushHandler2209_h
#define Blog_C___Reflection_PushHandler2209_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"

USING_NS_CC;

class PushHandler2209 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler2209)
    
public:
    SYNTHESIZE(PushHandler2209, int*, m_pValue)
    
    PushHandler2209() ;
    virtual ~PushHandler2209() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
