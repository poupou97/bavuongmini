
#ifndef Blog_C___Reflection_PushHandler5142_h
#define Blog_C___Reflection_PushHandler5142_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"

USING_NS_CC;

class PushHandler5142 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5142)

public:
	SYNTHESIZE(PushHandler5142, int*, m_pValue)

	PushHandler5142() ;
	virtual ~PushHandler5142() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);


protected:
	int *m_pValue ;
} ;

#endif
