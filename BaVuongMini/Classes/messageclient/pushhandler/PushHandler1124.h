
#ifndef Blog_C___Reflection_PushHandler1124_h
#define Blog_C___Reflection_PushHandler1124_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "../protobuf/MainMessage.pb.h"

using namespace std;

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1124 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1124)
public:
	SYNTHESIZE(PushHandler1124, int*, m_pValue)

		PushHandler1124() ;
	virtual ~PushHandler1124() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;
} ;

#endif