#include "PushHandler1927.h"

#include "../protobuf/CopyMessage.pb.h"
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../ui/FivePersonInstance/InstanceEndUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/CFivePersonInstanceEndInfo.h"
#include "../element/CRewardProp.h"
#include "../../ui/FivePersonInstance/InstanceRewardUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/GoodsInfo.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"

IMPLEMENT_CLASS(PushHandler1927)

	PushHandler1927::PushHandler1927() 
{

}
PushHandler1927::~PushHandler1927() 
{

}
void* PushHandler1927::createInstance()
{
	return new PushHandler1927() ;
}
void PushHandler1927::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1927::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1927::handle(CommonMessage* mb)
{
	Rsp1927 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, instance is over", mb->cmdid());

	if (!GameView::getInstance()->getGameScene())
		return;

	MainScene* mainScene = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
	if (!mainScene)
		return;

	if (FivePersonInstance::getStatus() != FivePersonInstance::status_in_instance)
		return;

	MissionAndTeam * missionAndTeam = (MissionAndTeam*)mainScene->getChildByTag(kTagMissionAndTeam);
	if (missionAndTeam)
	{
		if (missionAndTeam->getPanelType() == MissionAndTeam::type_FiveInstanceAndTeam)
		{
			missionAndTeam->updateInstanceSchedule(bean.boss(),bean.monster());
		}
	}
}