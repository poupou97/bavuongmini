
#include "PushHandler1510.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CGuildBase.h"

IMPLEMENT_CLASS(PushHandler1510)

PushHandler1510::PushHandler1510() 
{
    
}
PushHandler1510::~PushHandler1510() 
{
    
}
void* PushHandler1510::createInstance()
{
    return new PushHandler1510() ;
}
void PushHandler1510::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1510::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1510::handle(CommonMessage* mb)
{
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameView::getInstance()->myplayer->getActiveRole()->mutable_playerbaseinfo()->set_factionid(0);
	GameView::getInstance()->myplayer->getActiveRole()->mutable_playerbaseinfo()->set_factionposition(CGuildBase::position_none);
	GameView::getInstance()->myplayer->getActiveRole()->mutable_playerbaseinfo()->set_factionname("");
	GameView::getInstance()->myplayer->addFamilyName("", CGuildBase::position_none);
	FamilyUI * familyui =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	if (familyui != NULL)
	{
		familyui->callBackExit(NULL, Widget::TouchEventType::ENDED);
	}
}
