
#ifndef Blog_C___Reflection_PushHandler5109_h
#define Blog_C___Reflection_PushHandler5109_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5109 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5109)

public:
	SYNTHESIZE(PushHandler5109, int*, m_pValue)

		PushHandler5109() ;
	virtual ~PushHandler5109() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
