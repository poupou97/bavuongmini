
#ifndef Blog_C___Reflection_PushHandler1303_h
#define Blog_C___Reflection_PushHandler1303_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class GameSceneLayer;

class PushHandler1303 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1303)

public:
	SYNTHESIZE(PushHandler1303, int*, m_pValue)

	PushHandler1303() ;
	virtual ~PushHandler1303() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:
	// 卸下身上的某件装备
	// 目前主要针对武器（因为武器角色手中的武器会改变形象）
	void unEquip(GameSceneLayer* scene, int part);

protected:
	int *m_pValue ;

} ;

#endif
