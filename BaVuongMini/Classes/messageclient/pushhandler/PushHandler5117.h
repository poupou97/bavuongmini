
#ifndef Blog_C___Reflection_PushHandler5117_h
#define Blog_C___Reflection_PushHandler5117_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5117 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5117)

public:
	SYNTHESIZE(PushHandler5117, int*, m_pValue)

	PushHandler5117() ;
	virtual ~PushHandler5117() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
