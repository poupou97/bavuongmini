
#include "PushHandler1505.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/CGuildMemberBase.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/MainScene.h"


IMPLEMENT_CLASS(PushHandler1505)

PushHandler1505::PushHandler1505() 
{
    
}
PushHandler1505::~PushHandler1505() 
{
    
}
void* PushHandler1505::createInstance()
{
    return new PushHandler1505() ;
}
void PushHandler1505::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1505::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1505::handle(CommonMessage* mb)
{
	Rsp1505 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	MainScene * mainscene_ =(MainScene *)GameView::getInstance()->getMainUIScene();
	int applySize = bean.members_size();
	GameView::getInstance()->applyPlayersize = applySize;
	//mainscene_->checkIsNewRemind();
	mainscene_->remindFamilyApply();
	FamilyUI * familyui_ =(FamilyUI *)mainscene_->getChildByTag(kTagFamilyUI);
	if(familyui_== NULL)
	{
		return;
	}

	for(int i=0;i<bean.members_size();i++)
	{
		CGuildMemberBase * guildMember =new CGuildMemberBase();
		guildMember->CopyFrom(bean.members(i));
		familyui_->vectorApplyGuildMember.push_back(guildMember);
	}

	if (bean.hasmorepage()==1)
	{
		familyui_->hasNextPageOfManageMember =true;
	}else
	{
		familyui_->hasNextPageOfManageMember =false;
	}
	
}
