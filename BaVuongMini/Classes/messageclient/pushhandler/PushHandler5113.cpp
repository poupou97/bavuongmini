#include "PushHandler5113.h"
#include "../protobuf/DailyGiftSign.pb.h"
#include "GameView.h"
#include "../../ui/SignDaily_ui/SignDailyUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/SignDaily_ui/SignDailyData.h"
#include "../element/COneSignEverydayGift.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Reward_ui/RewardUi.h"
#include "../../ui/GameUIConstant.h"
#include "../element/CRewardBase.h"


IMPLEMENT_CLASS(PushHandler5113)

PushHandler5113::PushHandler5113() 
{

}
PushHandler5113::~PushHandler5113() 
{
	
}
void* PushHandler5113::createInstance()
{
	return new PushHandler5113() ;
}
void PushHandler5113::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5113::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5113::handle(CommonMessage* mb)
{
	Rsp5113 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int nNumGift = bean.number();				// �����(���ȡ�ĵڼ���Ľ��)
	int nGiftStatus = bean.status();				// ���ȡ״̬

	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	SignDailyUI * pSignDailyUI = (SignDailyUI *)mainscene_->getChildByTag(kTagSignDailyUI);
	if(pSignDailyUI == NULL)
		return;
	int nVectorIntentGiftSize =  SignDailyData::instance()->m_vector_internt_sign.size();
	for (int i = 0; i < nVectorIntentGiftSize; i++)
	{
		// �����ţ��ڼ����ǩ�����
		if (nNumGift == SignDailyData::instance()->m_vector_internt_sign.at(i)->number())
		{
			/** �����Ʒ״̬ ��0.�ɹ���-1.������*/
			if (0 == nGiftStatus)
			{
				// �Ʒ��ȡ�ɹ�:(1)���UI�״̬
				pSignDailyUI->getGiftSuc(nNumGift, 3);

				// signDaily is success delete remind list ----- add by liuzhenxing
				for (int i = 0;i<GameView::getInstance()->rewardvector.size();i++)
				{
					if (GameView::getInstance()->rewardvector.at(i)->getId() == REWARD_LIST_ID_SIGNDAILY)
					{
						GameView::getInstance()->rewardvector.at(i)->setRewardState(0);

						RewardUi * reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
						if (reward_ui != NULL)
						{
							reward_ui->tablevie_reward->updateCellAtIndex(i);
							RewardUi::setRewardListParticle();
							break;
						}
					}
				}


			}
			else if (-1 == nGiftStatus)
			{
				// ��Ʒ��ȡʧ�:(1)ܵ�����ʾ��Ϣ��2�����µ�ǰ��������ʱ�䣬������
				const char* str1 = StringDataManager::getString("label_qiandaobeibaoman");
				char* p1 = const_cast<char*>(str1);
				GameView::getInstance()->showAlertDialog(p1);
			}

		}
	}
}