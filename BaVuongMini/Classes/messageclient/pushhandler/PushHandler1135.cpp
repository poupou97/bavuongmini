#include "PushHandler1135.h"

#include "../protobuf/NPCMessage.pb.h"

#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BaseFighter.h"

IMPLEMENT_CLASS(PushHandler1135)

PushHandler1135::PushHandler1135() 
{

}
PushHandler1135::~PushHandler1135() 
{

}
void* PushHandler1135::createInstance()
{
	return new PushHandler1135() ;
}
void PushHandler1135::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1135::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1135::handle(CommonMessage* mb)
{
	Rsp1135 bean;
	bean.ParseFromString(mb->data());

	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
}