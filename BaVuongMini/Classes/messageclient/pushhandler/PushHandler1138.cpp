#include "PushHandler1138.h"

#include "../protobuf/NPCMessage.pb.h"

#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../element/CSalableCommodity.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/shop_ui/ShopUI.h"
#include "GameAudio.h"
#include "../../utils/GameUtils.h"

IMPLEMENT_CLASS(PushHandler1138)

	PushHandler1138::PushHandler1138() 
{

}
PushHandler1138::~PushHandler1138() 
{

}
void* PushHandler1138::createInstance()
{
	return new PushHandler1138() ;
}
void PushHandler1138::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1138::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1138::handle(CommonMessage* mb)
{
	SellResult1138 bean;
	bean.ParseFromString(mb->data());

	CCLOG("msg: %d, buy something", mb->cmdid());

	GameUtils::playGameSound(PROP_PURCHASE, 2, false);
	GameView::getInstance()->showAlertDialog(bean.msg().c_str());

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase))
	{
		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase)->removeFromParent();
	}

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreItemBuyInfo))
	{
		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreItemBuyInfo)->removeFromParent();
	}

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShopItemBuyInfo))
	{
		ShopUI * shopUI = (ShopUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShopUI);
		if (shopUI)
			shopUI->isSelectBtn = false;

		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShopItemBuyInfo)->removeFromParent();
	}

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHonorShopItemBuyInfo))
	{
		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHonorShopItemBuyInfo)->removeFromParent();
	}

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveShopItemBuyInfo))
	{
		GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveShopItemBuyInfo)->removeFromParent();
	}
}