
#include "PushHandler5092.h"

#include "../protobuf/FightMessage.pb.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(PushHandler5092)

	PushHandler5092::PushHandler5092() 
{

}
PushHandler5092::~PushHandler5092() 
{

}
void* PushHandler5092::createInstance()
{
	return new PushHandler5092() ;
}
void PushHandler5092::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5092::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5092::handle(CommonMessage* mb)
{
	Push5092 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	MyPlayer* myplayer = GameView::getInstance()->myplayer;

	if (!myplayer)
		return;

	myplayer->setPhysicalValue(bean.value());
	myplayer->setPhysicalCapacity(bean.capacity());

	if (!GameView::getInstance()->getGameScene())
		return;

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (!mainScene)
		return;

	mainScene->ReloadMyPlayerData();

}