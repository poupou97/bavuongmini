#include "PushHandler5303.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../protobuf/WorldBossMessage.pb.h"
#include "../element/CUnReceivePrize.h"
#include "../../ui/Active_ui/WorldBoss_ui/WorldBossData.h"
#include "../../ui/Active_ui/WorldBoss_ui/GetRewardUI.h"
#include "../../ui/Reward_ui/RewardUi.h"
#include "../../ui/GameUIConstant.h"

IMPLEMENT_CLASS(PushHandler5303)

	PushHandler5303::PushHandler5303() 
{

}
PushHandler5303::~PushHandler5303() 
{

}

void* PushHandler5303::createInstance()
{
	return new PushHandler5303() ;
}
void PushHandler5303::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5303::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5303::handle(CommonMessage* mb)
{
	ResRecievePrize5303 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (!GameView::getInstance())
		return;

	if (!GameView::getInstance()->getGameScene())
		return;

	MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (!mainscene)
		return;

	if (bean.success())
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("WorldBossUI_getReward_success"));

		for (int i = 0;i<WorldBossData::getInstance()->m_unReceiveRewardList.size();i++)
		{
			CUnReceivePrize * temp = WorldBossData::getInstance()->m_unReceiveRewardList.at(i);
			if (temp->id() == bean.id())
			{
				vector<CUnReceivePrize*>::iterator iter = WorldBossData::getInstance()->m_unReceiveRewardList.begin()+i;
				WorldBossData::getInstance()->m_unReceiveRewardList.erase(iter);
				delete temp;
				break;
			}
		}

		GetRewardUI * getRewardUI = (GetRewardUI*)mainscene->getChildByTag(kTagGetRewardUI);
		if (getRewardUI)
		{
			getRewardUI->RefreshUI();
		}
	}
	else
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("WorldBossUI_getReward_faild"));
	}
	
	if (WorldBossData::getInstance()->m_unReceiveRewardList.size() <=0 )
	{
		RewardUi::removeRewardListEvent(REWARD_LIST_ID_WORLDBOSS);
	}
}

