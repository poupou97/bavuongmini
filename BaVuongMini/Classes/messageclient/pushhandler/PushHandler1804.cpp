
#include "PushHandler1804.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../protobuf/AuctionMessage.pb.h"
#include "../../GameView.h"
#include "../../ui/Auction_ui/AuctionUi.h"
#include "../element/CAuctionType.h"

IMPLEMENT_CLASS(PushHandler1804)

PushHandler1804::PushHandler1804() 
{
    
}
PushHandler1804::~PushHandler1804() 
{
    
}
void* PushHandler1804::createInstance()
{
    return new PushHandler1804() ;
}
void PushHandler1804::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1804::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1804::handle(CommonMessage* mb)
{
	Rsp1804 bean;
	bean.ParseFromString(mb->data());
	
	AuctionUi * auction=(AuctionUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagAuction);
	if (auction == NULL)
	{
		return;
	}
	
	std::vector<CAuctionType *>::iterator iter;
	for (iter =auction->acutionTypevector.begin();iter!= auction->acutionTypevector.end();iter++ )
	{
		delete * iter;
	}
	auction->acutionTypevector.clear();
	
	int typeNum_ = bean.auctiontypes_size();
	for (int i=0;i<typeNum_;i++)
	{
		CAuctionType * auctionType_ =new CAuctionType();
		auctionType_->CopyFrom(bean.auctiontypes(i));
		auction->acutionTypevector.push_back(auctionType_);
	}

}
