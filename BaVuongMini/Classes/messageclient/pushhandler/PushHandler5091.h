
#ifndef Blog_C___Reflection_PushHandler5091_h
#define Blog_C___Reflection_PushHandler5091_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5091 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler5091)
    
public:
    SYNTHESIZE(PushHandler5091, int*, m_pValue)
    
    PushHandler5091() ;
    virtual ~PushHandler5091() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
