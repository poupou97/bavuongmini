#include "PushHandler1906.h"

#include "../protobuf/CopyMessage.pb.h"
#include "../../ui/challengeRound/ShowLevelUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/CTRankingPrize.h"
#include "../../ui/challengeRound/RankRewardUi.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Reward_ui/RewardUi.h"
#include "../../ui/GameUIConstant.h"


IMPLEMENT_CLASS(PushHandler1906)

PushHandler1906::PushHandler1906() 
{

}
PushHandler1906::~PushHandler1906() 
{

}
void* PushHandler1906::createInstance()
{
	return new PushHandler1906() ;
}
void PushHandler1906::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1906::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1906::handle(CommonMessage* mb)
{
	ResRecreivePrizeSucc1906 bean;
	bean.ParseFromString(mb->data());
	
	if (bean.succ() == 1)
	{
		RankRewardUi * rewardui_  = (RankRewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyReward);
		if (rewardui_)
		{
			for (int i =0;i<rewardui_->rankPrizevector.size();i++)
			{
				if (rewardui_->rankPrizevector.at(i)->canreceive() == 1)
				{
					rewardui_->rankPrizevector.at(i)->set_canreceive(false);
					rewardui_->tableView_reward->updateCellAtIndex(i);
				}
			}
		}
		const char *strings = StringDataManager::getString("singCopy_getPrizeOfAll");
		GameView::getInstance()->showAlertDialog(strings);
		///remind 
		switch(bean.clazz())
		{
		case 1:
			{
				RewardUi::removeRewardListEvent(REWARD_LIST_ID_SINGCOPYGETREWARD_CHUSHINIUDAO);
			}break;
		case 2:
			{
				RewardUi::removeRewardListEvent(REWARD_LIST_ID_SINGCOPYGETREWARD_XIAOYOUMINGQI);
			}break;
		case 3:
			{
				RewardUi::removeRewardListEvent(REWARD_LIST_ID_SINGCOPYGETREWARD_JIANGONGLEYE);
			}break;
		}
	}
}