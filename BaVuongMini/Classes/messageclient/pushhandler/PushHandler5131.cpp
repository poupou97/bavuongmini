#include "PushHandler5131.h"
#include "../protobuf/ActiveMessage.pb.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Active_ui/ActiveData.h"

IMPLEMENT_CLASS(PushHandler5131)

PushHandler5131::PushHandler5131() 
{

}
PushHandler5131::~PushHandler5131() 
{
	
}
void* PushHandler5131::createInstance()
{
	return new PushHandler5131() ;
}
void PushHandler5131::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5131::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5131::handle(CommonMessage* mb)
{
	Rsp5131 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int nActiveNeed = bean.number();					// ��ȡ���Ƕ��ٻ�Ծ�ȵĽ��
	int nPresentStatus = bean.status();				// ���ȡ״̬ 0.�ɹ���,-1�����ˣ�1������ȡ���
	
	if (0 == nPresentStatus)
	{
		// ˸��btn�״̬�����½��ջ�Ծ�vectorȣ�
		ActiveData::instance()->updateTodayActive(nActiveNeed);
	}
	
	ActiveUI* pTmpActiveUI = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	if (NULL != pTmpActiveUI)
	{
		// ���ActiveUI
		pTmpActiveUI->initActiveUIData();
	}
}