
#ifndef Blog_C___Reflection_PushHandler3004_h
#define Blog_C___Reflection_PushHandler3004_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"
#include "cocos2d.h"

USING_NS_CC;

class PushHandler3004 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler3004)

public:
	SYNTHESIZE(PushHandler3004, int*, m_pValue)

		PushHandler3004() ;
	virtual ~PushHandler3004() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
