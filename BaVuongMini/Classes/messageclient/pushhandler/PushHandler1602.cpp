
#include "PushHandler1602.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../element/CAdditionProperty.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/equipMent_ui/RefreshEquipData.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../ui/GameUIConstant.h"
#include "../../gamescene_state/MainAnimationScene.h"
#include "../../gamescene_state/EffectDispatch.h"
#include "../../utils/GameUtils.h"
#include "GameAudio.h"

IMPLEMENT_CLASS(PushHandler1602)

PushHandler1602::PushHandler1602() 
{
    
}
PushHandler1602::~PushHandler1602() 
{
    
}
void* PushHandler1602::createInstance()
{
	return new PushHandler1602() ;
}
void PushHandler1602::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1602::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1602::handle(CommonMessage* mb)
{
	Rsp1602 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int curEx = bean.experience(0);//�����װ������ֵ
	int remEx =bean.experience(1);//��������һ������ľ���ֵ
	int curRefineLv = bean.strengthgrade();//�����װ������ȼ�
	bean.strengthproperties();//(���)�����

	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	EquipMentUi * equip =(EquipMentUi *)mainscene->getChildByTag(ktagEquipMentUI);
	if (bean.result() ==1)
	{
		MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
		if (mainAnmScene)
		{
			mainAnmScene->addInterfaceAnm("jlcg/jlcg.anm");
		}
		
		equip->firstAssist =false;
		equip->secondtAssist =false;
		equip->thirdAssist =false;
		equip->fourAssist =false;
		equip->fiveAssist =false;
		equip->assistVector.clear();
		equip->assStoneVector.clear();
		equip->stoneOfIndexRefine = 0;
		equip->stoneOfAmountRefine = 0;

		//if success refresh general equipment
		std::vector<CAdditionProperty *>::iterator iter;
		for (iter = equip->additionProperty.begin(); iter != equip->additionProperty.end(); ++iter)
		{
			delete *iter;
		}
		equip->additionProperty.clear();
		for (int i=0;i<bean.strengthproperties_size();i++)
		{
			CAdditionProperty * addproperty_ =new CAdditionProperty();
			addproperty_->CopyFrom(bean.strengthproperties(i));
			equip->additionProperty.push_back(addproperty_);
		}
		if (equip->tabviewSelectIndex != 0)
		{
			equip->generalList->generalList_tableView->cellAtIndex(equip->tabviewSelectIndex);
		}
	}else
	{
		MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
		if (mainAnmScene)
		{
			mainAnmScene->addInterfaceAnm("jlcg/jlcg.anm");
		}
	}
	equip->refineCost = 0;
	int allEx =curEx+ remEx;

	equip->equipLookRefineLevel_ = bean.strengthgrade();// refine level
	std::string curRefineLevelString = "+";
	char curRefineLevelStr[10];
	sprintf(curRefineLevelStr,"%d",bean.strengthgrade());
	curRefineLevelString.append(curRefineLevelStr);
	//equip->labelFont_curRefinelevel->setText(curRefineLevelString.c_str());
	//equip->labelFont_curRefinelevel->setVisible(true);
	equip->refreshEquipProperty(curEx,allEx,true);
	equip->refreshPlayerMoney();

	bool isAction = false;
	if (equip->getEquipBeginLevel() < equip->equipLookRefineLevel_)
	{
		isAction = true;
	}
	
	RefreshEquipData::instance()->setRefineEndValue(curRefineLv,isAction);
	
	
	equip->refreshBackPack(equip->currType,equip->equipLookRefineLevel_);
	equip->removeAssistAndStone(operatorDelete);
	equip->setEquipBeginLevel(bean.strengthgrade());

	Node* pEffect = BonusSpecialEffect::create();
	pEffect->setPosition(Vec2(EQUIPMENT_UI_LEFT_SLOT_X, EQUIPMENT_UI_LEFT_SLOT_Y));
	pEffect->setScale(EQUIPMENT_UI_BONUSSPECIALEFFECT_SCALE);
	equip->refineLayer->addChild(pEffect);

	GameUtils::playGameSound(REFINE_CLICK, 2, false);

	EffectEquipItem::reloadShortItem();
}
