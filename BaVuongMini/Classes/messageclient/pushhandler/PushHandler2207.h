
#ifndef Blog_C___Reflection_PushHandler2207_h
#define Blog_C___Reflection_PushHandler2207_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
#include "../../ui/Chat_ui/ChatCell.h"
USING_NS_CC;

class PushHandler2207 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler2207)
    
public:
    SYNTHESIZE(PushHandler2207, int*, m_pValue)
    
    PushHandler2207() ;
    virtual ~PushHandler2207() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
