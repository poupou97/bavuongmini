
#include "PushHandler1607.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../element/CAdditionProperty.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/equipMent_ui/RefreshEquipData.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../ui/GameUIConstant.h"
#include "../../gamescene_state/MainAnimationScene.h"
#include "../../gamescene_state/EffectDispatch.h"

using namespace CocosDenshion;

IMPLEMENT_CLASS(PushHandler1607)

PushHandler1607::PushHandler1607() 
{
    
}
PushHandler1607::~PushHandler1607() 
{
    
}
void* PushHandler1607::createInstance()
{
	return new PushHandler1607() ;
}
void PushHandler1607::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1607::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1607::handle(CommonMessage* mb)
{
	Rsp1607 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int resuilt = bean.result();// 1 success 0 fail
	MainScene * mainscene_ =(MainScene *)GameView::getInstance()->getMainUIScene();
	EquipMentUi * equipUI= (EquipMentUi*)mainscene_->getChildByTag(ktagEquipMentUI);
	if (equipUI == NULL)
	{
		return;
	}
	if (resuilt ==1)
	{
		GameUtils::playGameSound(STAR_SUCCESS, 2, false);
		//mainscene_->addInterfaceAnm("sxcg/sxcg.anm");
		MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
		if (mainAnmScene)
		{
			mainAnmScene->addInterfaceAnm("sxcg/sxcg.anm");
		}
	}else
	{
		GameUtils::playGameSound(STAR_FAIL, 2, false);
		//mainscene_->addInterfaceAnm("sxsb/sxsb.anm");
		MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
		if (mainAnmScene)
		{
			mainAnmScene->addInterfaceAnm("sxsb/sxsb.anm");
		}
	}
	int starlevel= bean.starlevel();//star level

	equipUI->stoneOfAmountStar = 0;
	equipUI->stoneOfIndexStar = 0;
	equipUI->StrengthStone = false;
	if (equipUI->tabviewSelectIndex != 0)
	{
		equipUI->generalList->generalList_tableView->cellAtIndex(equipUI->tabviewSelectIndex);
	}

	std::vector<CAdditionProperty *>::iterator iter;
	for (iter=equipUI->additionProperty_star.begin();iter!=equipUI->additionProperty_star.end();iter++)
	{
		delete *iter;
	}
	equipUI->additionProperty_star.clear();
	for (int i=0;i<bean.starproperties_size();i++)
	{
		CAdditionProperty * addproperty =new CAdditionProperty();
		addproperty->CopyFrom(bean.starproperties(i));
		equipUI->additionProperty_star.push_back(addproperty);
	}

	int curLuck =bean.starluck();
	int maxLuck = bean.starmaxluck();
	equipUI->starLevelValue =bean.starlevel();
	
	equipUI->refreshEquipProperty_star(curLuck,maxLuck,0,true);
	equipUI->refreshPlayerMoney();
	equipUI->curMainEquipStarLevel = bean.starlevel();
	
	bool isAction = false;
	if (resuilt == 1)
	{
		isAction = true;
	}
	RefreshEquipData::instance()->setStarEndValue(starlevel,isAction);
	
	equipUI->removeAssistAndStone(operatorDelete);

	equipUI->refreshBackPack(equipUI->currType,equipUI->curMainEquipStarLevel);

	if (resuilt == 1)
	{
		Node* pEffect = BonusSpecialEffect::create();
		pEffect->setPosition(Vec2(EQUIPMENT_UI_LEFT_SLOT_X, EQUIPMENT_UI_LEFT_SLOT_Y));
		pEffect->setScale(EQUIPMENT_UI_BONUSSPECIALEFFECT_SCALE);
		equipUI->starPropertyLayer->addChild(pEffect);
	}

	EffectEquipItem::reloadShortItem();
}
