
#ifndef Blog_C___Reflection_PushHandler1304_h
#define Blog_C___Reflection_PushHandler1304_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1304 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1304)

public:
	SYNTHESIZE(PushHandler1304, int*, m_pValue)

		PushHandler1304() ;
	virtual ~PushHandler1304() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
