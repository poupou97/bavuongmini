#include "PushHandler5118.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/COneVipGift.h"

IMPLEMENT_CLASS(PushHandler5118)

	PushHandler5118::PushHandler5118() 
{

}
PushHandler5118::~PushHandler5118() 
{

}
void* PushHandler5118::createInstance()
{
	return new PushHandler5118() ;
}
void PushHandler5118::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5118::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5118::handle(CommonMessage* mb)
{
	push5118 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//delete old
	std::vector<COneVipGift*>::iterator iter_oneVipGift;
	for (iter_oneVipGift = GameView::getInstance()->m_vipEveryDayRewardsList.begin(); iter_oneVipGift != GameView::getInstance()->m_vipEveryDayRewardsList.end(); ++iter_oneVipGift)
	{
		delete *iter_oneVipGift;
	}
	GameView::getInstance()->m_vipEveryDayRewardsList.clear();

	for(int i = 0;i<bean.goods_size();++i)
	{
		COneVipGift * temp = new COneVipGift();
		temp->CopyFrom(bean.goods(i));
		GameView::getInstance()->m_vipEveryDayRewardsList.push_back(temp);
	}

	if (!GameView::getInstance()->getGameScene())
		return;

	if (GameView::getInstance()->getMainUIScene() != NULL)
	{
		MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
		mainscene_->remindReward();
	}
}