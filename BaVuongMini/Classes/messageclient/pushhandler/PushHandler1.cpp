#include "PushHandler1.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../element/MapMarquee.h"
#include "../../ui/extensions/MarqueeControl.h"

IMPLEMENT_CLASS(PushHandler1)

PushHandler1::PushHandler1() 
{

}
PushHandler1::~PushHandler1() 
{

}
void* PushHandler1::createInstance()
{
	return new PushHandler1() ;
}
void PushHandler1::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1::handle(CommonMessage* mb)
{
	Push1 bean;
	bean.ParseFromString(mb->data());

	//CCLOG("msg: %d, marquee message", mb->cmdid());
	
	if (GameView::getInstance()->getGameScene() == NULL)
		return;
	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainscene == NULL)
		return;

	int index_ = bean.id();

	MapMarquee * marquee_ =MarqueeStringConfigData::s_MarqueeStrings[index_];
	if (marquee_ == NULL)
	{
		return;
	}

	std::string strings_ = marquee_->get_message();
	int priority_ =  marquee_->get_prioity();

	//�����ʱ������Ϣû��������Ϣ�����ȼ��� 
	//kTagMarqueeControlBaseId  555
	MarqueeControl * marqueeui =(MarqueeControl *)mainscene->getChildByTag(555);
	if (marqueeui != NULL)
	{
		if (priority_ < marqueeui->getPrioity())
		{
			marqueeui->removeFromParentAndCleanup(true);

			for (int i = 0; i <bean.param_size();i++)
			{
				std::string replaceStr = bean.param(i);
				std::string str_ = "{}";
				int pos_ = strings_.find(str_);
				if (pos_!= -1)
				{
					strings_.replace(pos_,str_.length(),replaceStr);
				}
			}
			MainScene::marqueeStruct struct_ ={strings_.c_str(),priority_};
			mainscene->marqueeString.push_back(struct_);
		}else
		{
			return;
		}
	}else
	{
		for (int i = 0; i <bean.param_size();i++)
		{
			std::string replaceStr = bean.param(i);
			std::string str_ = "{}";
			int pos_ = strings_.find(str_);
			if (pos_!= -1)
			{
				strings_.replace(pos_,str_.length(),replaceStr);
			}
		}
		MainScene::marqueeStruct struct_ ={strings_.c_str(),priority_};
		mainscene->marqueeString.push_back(struct_);
	}
}