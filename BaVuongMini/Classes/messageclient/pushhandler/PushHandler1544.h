
#ifndef Blog_C___Family_PushHandler1544_h
#define Blog_C___Family_PushHandler1544_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1544 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1544)

public:
	SYNTHESIZE(PushHandler1544, int*, m_pValue)

		PushHandler1544() ;
	virtual ~PushHandler1544() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
