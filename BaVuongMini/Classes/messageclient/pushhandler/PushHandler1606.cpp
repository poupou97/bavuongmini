
#include "PushHandler1606.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"


IMPLEMENT_CLASS(PushHandler1606)

PushHandler1606::PushHandler1606() 
{
    
}
PushHandler1606::~PushHandler1606() 
{
    
}
void* PushHandler1606::createInstance()
{
	return new PushHandler1606() ;
}
void PushHandler1606::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1606::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1606::handle(CommonMessage* mb)
{
	Rsp1606 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	EquipMentUi * equipUI=(EquipMentUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	double successRate_= bean.successrate()*100;

	int costStar_ = bean.starcost();
	if (successRate_  >= 100)
	{
		successRate_ = 100;
	}
	
	std::string str_ ="";
	char rate_[15];
	sprintf(rate_,"%.0f",successRate_);
	str_.append(rate_);
	str_.append("%");
	equipUI->successRate->setString(str_.c_str());
	
	char costStr[10];
	sprintf(costStr,"%d",costStar_);
	equipUI->starCost_label->setString(costStr);
}
