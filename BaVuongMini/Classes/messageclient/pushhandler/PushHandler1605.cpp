
#include "PushHandler1605.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CAdditionProperty.h"
#include "../../ui/equipMent_ui/RefreshEquipData.h"


IMPLEMENT_CLASS(PushHandler1605)

PushHandler1605::PushHandler1605() 
{
    
}
PushHandler1605::~PushHandler1605() 
{
    
}
void* PushHandler1605::createInstance()
{
	return new PushHandler1605() ;
}
void PushHandler1605::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1605::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1605::handle(CommonMessage* mb)
{
	Rsp1605 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	EquipMentUi * equip= (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	if(equip == NULL)
	{
		return;
	}
	std::vector<CAdditionProperty *>::iterator iter;
	for (iter=equip->additionProperty_star.begin();iter!=equip->additionProperty_star.end();iter++)
	{
		delete *iter;
	}
	equip->additionProperty_star.clear();
	for (int i=0;i<bean.starproperties_size();i++)
	{
		CAdditionProperty * addproperty =new CAdditionProperty();
		addproperty->CopyFrom(bean.starproperties(i));
		equip->additionProperty_star.push_back(addproperty);
	}
	int curLuck =bean.starluck();//��Դ
	int maxLuck =bean.starmaxluck();
	equip->setCurEquipExStar(curLuck);
	int starConst_ = 0; 
	equip->starLevelValue =bean.starlevel();
	equip->curMainEquipStarLevel = bean.starlevel();
	//��ǵ�ǰѡ�еı������Ʒ
	equip->starMainEquip =true;
	equip->refreshCurState();
	
	RefreshEquipData::instance()->getOperationGeneral();

	equip->refreshEquipProperty_star(curLuck,maxLuck,starConst_);
	
	equip->refreshBackPack(equip->currType,equip->curMainEquipStarLevel);
}
