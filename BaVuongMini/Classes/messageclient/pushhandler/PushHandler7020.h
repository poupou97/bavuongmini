
#ifndef Blog_C___Reflection_PushHandler7020_h
#define Blog_C___Reflection_PushHandler7020_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler7020 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler7020)

public:
	SYNTHESIZE(PushHandler7020, int*, m_pValue)

		PushHandler7020() ;
	virtual ~PushHandler7020() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
