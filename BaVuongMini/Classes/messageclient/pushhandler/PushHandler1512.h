
#ifndef Blog_C___Family_PushHandler1512_h
#define Blog_C___Family_PushHandler1512_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1512 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1512)
    
public:
    SYNTHESIZE(PushHandler1512, int*, m_pValue)
    
    PushHandler1512() ;
    virtual ~PushHandler1512() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
