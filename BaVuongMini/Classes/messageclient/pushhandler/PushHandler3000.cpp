
#include "PushHandler3000.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "GameView.h"
#include "../protobuf/OfflineFight.pb.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../element/CPartner.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/offlinearena_ui/OffLineArenaData.h"
#include "../../ui/GameUIConstant.h"

IMPLEMENT_CLASS(PushHandler3000)

	PushHandler3000::PushHandler3000() 
{

}
PushHandler3000::~PushHandler3000() 
{

}
void* PushHandler3000::createInstance()
{
	return new PushHandler3000() ;
}
void PushHandler3000::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler3000::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler3000::handle(CommonMessage* mb)
{
	ResOft3000 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (GameView::getInstance()->getGameScene() == NULL)
		return;

	OffLineArenaData::getInstance()->m_nMyRoleRanking = bean.playerranking();

	MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainscene_ != NULL && bean.playerranking() != 1)
	{
		int m_times = bean.fightconfig().fighttimesupvalue() - bean.fightconfig().fighttimes();
		GameView::getInstance()->setRemainTimesOfArena(m_times);
		mainscene_->remindOffArena();
	}
	
	if (mainscene_ != NULL)
	{
		long long temp_delayTime = bean.honorconfig().remainreceivetime();
		if (temp_delayTime != 0)
		{
			temp_delayTime+= 1000*60*2;
		}
		mainscene_->setOffAreaPrizeTime(temp_delayTime);
	}
	
	OffLineArenaUI * offLineArenaUI = (OffLineArenaUI*)mainscene_->getChildByTag(kTagOffLineArenaUI);
	if (offLineArenaUI == NULL)
		return;

	std::vector<CPartner *>tempList;
	for (int i = 0;i<bean.challengelist_size();++i)
	{
		CPartner * partner = new CPartner();
		partner->CopyFrom(bean.challengelist(i));
		tempList.push_back(partner);
	}
	//refresh my ranking
	offLineArenaUI->RefreshMyRankingConfig(bean.playerranking());
	//refresh ranking display
	offLineArenaUI->RefreshArenaDisplay(tempList);
	//refresh rankHonor and time
	offLineArenaUI->RefreshHonorConfig(bean.honorconfig().config(),bean.honorconfig().remainreceivetime());
	//refresh fightTime
	offLineArenaUI->RefreshFightConfig(bean.fightconfig().fighttimes(),bean.fightconfig().fighttimesupvalue(),bean.fightconfig().remainbuytimes());

	std::vector<CPartner*>::iterator iter;
	for (iter = tempList.begin(); iter != tempList.end(); ++iter)
	{
		delete *iter;
	}
	tempList.clear();

}
