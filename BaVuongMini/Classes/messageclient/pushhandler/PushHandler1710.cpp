#include "PushHandler1710.h"
#include "../../GameView.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/family_ui/FamilyShop.h"


IMPLEMENT_CLASS(PushHandler1710)

PushHandler1710::PushHandler1710() 
{
    
}
PushHandler1710::~PushHandler1710() 
{
    
}
void* PushHandler1710::createInstance()
{
    return new PushHandler1710() ;
}
void PushHandler1710::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1710::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1710::handle(CommonMessage* mb)
{
	PushPlayerGuildDonate1710 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	FamilyUI * familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	FamilyShop* familyshop = (FamilyShop *)GameView::getInstance()->getMainUIScene()->getChildByTag(familyShoptag);
	familyui->remPersonContrbution = bean.donate();;
	char personStrRemContrbution[20];
	sprintf(personStrRemContrbution,"%d",familyui->remPersonContrbution);
	familyui->label_myRemContribution->setString(personStrRemContrbution);

	familyshop->labelContrbution->setString(personStrRemContrbution);
}
