
#ifndef Blog_C___Family_PushHandler1513_h
#define Blog_C___Family_PushHandler1513_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1513 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1513)
    
public:
    SYNTHESIZE(PushHandler1513, int*, m_pValue)
    
    PushHandler1513() ;
    virtual ~PushHandler1513() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
