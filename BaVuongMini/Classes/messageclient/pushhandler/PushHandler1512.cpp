
#include "PushHandler1512.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/family_ui/ManageFamily.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CGuildMemberBase.h"
#include "../element/CGuildRecord.h"
#include "../../ui/family_ui/FamilyUI.h"



IMPLEMENT_CLASS(PushHandler1512)

PushHandler1512::PushHandler1512() 
{
    
}
PushHandler1512::~PushHandler1512() 
{
    
}
void* PushHandler1512::createInstance()
{
    return new PushHandler1512() ;
}
void PushHandler1512::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1512::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1512::handle(CommonMessage* mb)
{
	Rsp1512 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//bean.page();
	//bean.hasmorepage();

	FamilyUI * familyui =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	std::vector<CGuildMemberBase*>::iterator iter;
	for (iter = familyui->vectorGuildMemberBase.begin(); iter != familyui->vectorGuildMemberBase.end(); ++iter)
	{
		delete *iter;
	}
	familyui->vectorGuildMemberBase.clear();

	for (int i=0;i< bean.members_size();i++)
	{
		CGuildMemberBase * member_ =new CGuildMemberBase();
		member_->CopyFrom(bean.members(i));
		familyui->vectorGuildMemberBase.push_back(member_);
	}
	/*
	//get self is post in family
	int vectorSize =familyui->vectorGuildMemberBase.size();
	for (int i=0;i<vectorSize;i++)
	{
		if (familyui->selfId_ == familyui->vectorGuildMemberBase.at(i)->id())
		{
			familyui->selfPost_ = familyui->vectorGuildMemberBase.at(i)->post();
		}
	}

	if (familyui->selfPost_ != 1)
	{
		familyui->btn_repairNotice->setTouchEnabled(false);
		familyui->btn_repairManifesto->setTouchEnabled(false);
	}
	*/
	if (bean.hasmorepage() ==1)
	{
		familyui->hasNextPageOfMember = true;
	}else
	{
		familyui->hasNextPageOfMember = false;
	}
	familyui->tableviewMember->reloadData();
}
