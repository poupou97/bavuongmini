#ifndef Blog_C___Reflection_PushHandler1010_h
#define Blog_C___Reflection_PushHandler1010_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1010 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1010)

public:
	SYNTHESIZE(PushHandler1010, int*, m_pValue)

	PushHandler1010() ;
	virtual ~PushHandler1010() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:
	void addBattleAchMessageToScene(int nPkPoint);

protected:
	int *m_pValue ;

} ;

#endif
