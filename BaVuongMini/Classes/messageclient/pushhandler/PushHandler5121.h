
#ifndef Blog_C___Reflection_PushHandler5121_h
#define Blog_C___Reflection_PushHandler5121_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5121 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5121)

public:
	SYNTHESIZE(PushHandler5121, int*, m_pValue)

		PushHandler5121() ;
	virtual ~PushHandler5121() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
