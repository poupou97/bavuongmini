
#ifndef Blog_C___Reflection_PushHandler5131_h
#define Blog_C___Reflection_PushHandler5131_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5131 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5131)

public:
	SYNTHESIZE(PushHandler5131, int*, m_pValue)

	PushHandler5131() ;
	virtual ~PushHandler5131() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
