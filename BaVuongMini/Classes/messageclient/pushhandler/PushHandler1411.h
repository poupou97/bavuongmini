
#ifndef Blog_C___Reflection_PushHandler1411_h
#define Blog_C___Reflection_PushHandler1411_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1411 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1411)
    
public:
    SYNTHESIZE(PushHandler1411, int*, m_pValue)
    
    PushHandler1411() ;
    virtual ~PushHandler1411() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
