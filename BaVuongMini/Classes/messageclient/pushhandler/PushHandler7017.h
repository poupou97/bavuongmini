
#ifndef Blog_C___Reflection_PushHandler7017_h
#define Blog_C___Reflection_PushHandler7017_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler7017 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler7017)

public:
	SYNTHESIZE(PushHandler7017, int*, m_pValue)

		PushHandler7017() ;
	virtual ~PushHandler7017() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
