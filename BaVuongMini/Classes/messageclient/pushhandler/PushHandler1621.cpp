
#include "PushHandler1621.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "../element/CAdditionProperty.h"
#include "GameView.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CEquipHole.h"
#include "../../ui/equipMent_ui/RefreshEquipData.h"



IMPLEMENT_CLASS(PushHandler1621)

PushHandler1621::PushHandler1621() 
{
    
}
PushHandler1621::~PushHandler1621() 
{
    
}
void* PushHandler1621::createInstance()
{
	return new PushHandler1621() ;
}
void PushHandler1621::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1621::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1621::handle(CommonMessage* mb)
{
	Push1621 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
		
	EquipMentUi * equip= (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
	std::vector<CEquipHole *>::iterator iter;
	for (iter=equip->equipmentHoleV.begin();iter!=equip->equipmentHoleV.end();iter++)
	{
		delete *iter;
	}
	equip->equipmentHoleV.clear();

	for(int i=0;i<bean.equipholes_size();i++)
	{
		CEquipHole * equiphole =new CEquipHole();
		equiphole->CopyFrom(bean.equipholes(i));
		equip->equipmentHoleV.push_back(equiphole);
	}
	//��ǵ�ǰѡ�еı������Ʒ
	equip->gemMainEquip =true;
	equip->refreshCurState();
	
	RefreshEquipData::instance()->getOperationGeneral();
	equip->refreshGemValue(false);

	equip->refreshBackPack(equip->currType,-1);
}
