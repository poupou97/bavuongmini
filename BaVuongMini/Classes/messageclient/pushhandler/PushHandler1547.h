
#ifndef Blog_C___Family_PushHandler1547_h
#define Blog_C___Family_PushHandler1547_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1547 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1547)

public:
	SYNTHESIZE(PushHandler1547, int*, m_pValue)

		PushHandler1547() ;
	virtual ~PushHandler1547() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
