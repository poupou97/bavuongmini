
#ifndef Blog_C___Reflection_PushHandler5095_h
#define Blog_C___Reflection_PushHandler5095_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5095 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5095)

public:
	SYNTHESIZE(PushHandler5095, int*, m_pValue)

	PushHandler5095() ;
	virtual ~PushHandler5095() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
