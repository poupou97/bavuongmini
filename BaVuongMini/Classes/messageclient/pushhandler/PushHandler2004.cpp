
#include "PushHandler2004.h"
#include "../protobuf/MailMessage.pb.h"
#include "GameView.h"
#include "../element/CMailAttachment.h"
#include "../../ui/Mail_ui/MailUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/CMailInfo.h"
#include "../../utils/StaticDataManager.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../ui/backpackscene/GoodsItemInfoBase.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Mail_ui/MailList.h"
#include "../../utils/GameUtils.h"

using namespace CocosDenshion;

IMPLEMENT_CLASS(PushHandler2004)

	PushHandler2004::PushHandler2004() 
{

}
PushHandler2004::~PushHandler2004() 
{

}
void* PushHandler2004::createInstance()
{
	return new PushHandler2004() ;
}
void PushHandler2004::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2004::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2004::handle(CommonMessage* mb)
{
	Push2004 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;
	MainScene * mainscene_ = scene->getMainUIScene();

	if (bean.mail().flag() == 0)
	{
		const char *string_ = StringDataManager::getString("mail_haveanewMail");
		GameView::getInstance()->showAlertDialog(string_);
		GameUtils::playGameSound(MAIL_NEW, 2, false);
		//new mail
		CMailInfo * mailinfo =new CMailInfo();
		mailinfo->CopyFrom(bean.mail());
		GameView::getInstance()->mailVectorOfSystem.push_back(mailinfo);
		/*
		long long name_systemId = mailinfo->sender();
		if (name_systemId == LLONG_MAX)
		{
			GameView::getInstance()->mailVectorOfSystem.push_back(mailinfo);
		}else
		{
			GameView::getInstance()->mailVectorOfPlayer.push_back(mailinfo);
		}
		*/
		//mainscene_->checkIsNewRemind();
		mainscene_->remindMail();
	}else
	{
		//��ȡ��� �ѯ���Ƿ�ɾ����ʼ
		MailUI * mailui=(MailUI *)mainscene_->getChildByTag(kTagMailUi);
		mailui->sourceDataVector.at(mailui->mailList_Id)->mutable_attachment(0)->set_state(1);
		mailui->extractAnnex->setVisible(false);
		Sprite * sp_ = (Sprite *)mailui->mailList->tableView->cellAtIndex(mailui->mailList_Id)->getChildByTag(mailAttachmentStateFlag);
		if (sp_ != NULL)
		{
			sp_->removeFromParentAndCleanup(true);
		}
		
		if (mailui != NULL)
		{
			for (int i=0;i<2;i++)
			{
				GoodsItemInfoBase * goodsui = (GoodsItemInfoBase *)mailui->getMailLayer->getChildByTag(MAILANNEXT+i);
				if (goodsui!= NULL)
				{
					goodsui->removeFromParentAndCleanup(true);
				}
			}
			const char *str_ = StringDataManager::getString("mailui_annex_suredelete");
			/*
			Size winSize=Director::getInstance()->getVisibleSize();
			PopupWindow * popupWindow_ =PopupWindow::create(str_,2,PopupWindow::KtypeNeverRemove,10);
			popupWindow_->setIgnoreAnchorPointForPosition(false);
			popupWindow_->setAnchorPoint(Vec2(0.5f,0.5f));
			popupWindow_->setPosition(Vec2(winSize.width/2,winSize.height/2));
			popupWindow_->setButtonPosition();
			popupWindow_->AddcallBackEvent(mailui,callfuncO_selector(MailUI::deleteMailSure),NULL);
			GameView::getInstance()->getMainUIScene()->addChild(popupWindow_);
			*/




			GameView::getInstance()->showPopupWindow(str_,2,mailui,callfuncO_selector(MailUI::deleteMailSure),NULL);
		}
		mailui ->refreshPlayerMoney();
		/*
		//show player get gold and goldIngot
		int gold_ = 0;
		int goldIngot_ = 0;
		for (int attachmentIndex = 0;attachmentIndex < mailui->sourceDataVector.at(mailui->mailList_Id)->attachment_size();attachmentIndex++)
		{
			if (mailui->sourceDataVector.at(mailui->mailList_Id)->attachment(attachmentIndex).gold() > 0)
			{
				gold_ = mailui->sourceDataVector.at(mailui->mailList_Id)->attachment(attachmentIndex).gold();
			}
			
			if (mailui->sourceDataVector.at(mailui->mailList_Id)->attachment(attachmentIndex).goldingot() > 0)
			{
				goldIngot_ = mailui->sourceDataVector.at(mailui->mailList_Id)->attachment(attachmentIndex).goldingot();
			}
			
		}
	
		if (gold_ > 0 || goldIngot_ > 0)
		{
			std::string tempString = "";
			const char* string_ = StringDataManager::getString("mail_selfGetPayTomail");
			tempString.append(string_);
			if (gold_ > 0)
			{
				char goldStr[50];
				const char* templateStr = StringDataManager::getString("mail_payToMailOfGold");
				sprintf(goldStr,templateStr,gold_);
				tempString.append(goldStr);
			}
			if (goldIngot_ > 0)
			{
				char goldIngotStr[50];
				const char* templateStr = StringDataManager::getString("mail_payToMailOfGoldIngot");
				sprintf(goldIngotStr,templateStr,goldIngot_);
				tempString.append(goldIngotStr);
			}
			GameView::getInstance()->showAlertDialog(tempString);
		}
		*/
	}
}
