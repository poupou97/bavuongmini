#ifndef Blog_C___Reflection_PushHandler5115_h
#define Blog_C___Reflection_PushHandler5115_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5115 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5115)

public:
	SYNTHESIZE(PushHandler5115, int*, m_pValue)

	PushHandler5115() ;
	virtual ~PushHandler5115() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
