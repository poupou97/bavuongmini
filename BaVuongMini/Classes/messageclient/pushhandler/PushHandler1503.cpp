
#include "PushHandler1503.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/CGuildRecord.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../element/CGuildMemberBase.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/family_ui/ManageFamily.h"
#include "../../utils/StaticDataManager.h"



IMPLEMENT_CLASS(PushHandler1503)

PushHandler1503::PushHandler1503() 
{
    
}
PushHandler1503::~PushHandler1503() 
{
    
}
void* PushHandler1503::createInstance()
{
    return new PushHandler1503() ;
}
void PushHandler1503::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1503::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1503::handle(CommonMessage* mb)
{
	Rsp1503 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	FamilyUI * familyui = (FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	if (bean.type() == 1)
	{
		if (bean.result() == 0)
		{
			std::string name_ = "";
			if (familyui != NULL)
			{
				for (int i=0;i<familyui->vectorApplyGuildMember.size();i++)
				{
					if (familyui->vectorApplyGuildMember.at(i)->id() == bean.roleid())
					{
						name_ = familyui->vectorApplyGuildMember.at(i)->name();
						std::vector<CGuildMemberBase *>::iterator iter= familyui->vectorApplyGuildMember.begin() + i;
						CGuildMemberBase * applyPlayer=*iter ;
						familyui->vectorApplyGuildMember.erase(iter);
						delete applyPlayer;
					}
				}
				ManageFamily * manageFamily_ = (ManageFamily *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagManageFamily);
				if (manageFamily_ != NULL)
				{
					manageFamily_->tableviewstate->reloadData();
					MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
					GameView::getInstance()->applyPlayersize = familyui->vectorApplyGuildMember.size();
					mainscene_->remindFamilyApply();
				}
			}

			if (strlen(name_.c_str()) > 0)
			{
				const char *str1  = StringDataManager::getString("family_isAgreeName_");;
				const char *str2  = StringDataManager::getString("family_isAddFamilyName_");;
				std::string string_ = str1;
				string_.append(name_);
				string_.append(str2);
				GameView::getInstance()->showAlertDialog(string_);

				familyui->curFamilyMemberNum++;
				//familyui->maxFamilyMemberNum = bean.guildbase().maxmembernumber();

				char curMemberStr[5];
				sprintf(curMemberStr,"%d",familyui->curFamilyMemberNum);
				char maxMemberStr[5];
				sprintf(maxMemberStr,"%d",familyui->maxFamilyMemberNum);
				std::string member_ =curMemberStr;
				member_.append("/");
				member_.append(maxMemberStr);
				familyui->label_familyNumber->setString(member_.c_str());
			}
			/*
			//family member
			int curMember_ =bean.guildbase().currmembernumber();
			int maxMember_ =bean.guildbase().maxmembernumber();
			char curMemberStr[5];
			sprintf(curMemberStr,"%d",curMember_);
			char maxMemberStr[5];
			sprintf(maxMemberStr,"%d",maxMember_);
			std::string member_ =curMemberStr;
			member_.append("/");
			member_.append(maxMemberStr);
			familyui->label_familyNumber->setText(member_.c_str());
			*/
		}
	}
	
	if (bean.type() == 2)
	{
		if (bean.result() == 0)
		{
			std::string name_ = "";
			if (familyui != NULL)
			{
				for (int i=0;i<familyui->vectorApplyGuildMember.size();i++)
				{
					if (familyui->vectorApplyGuildMember.at(i)->id()==bean.roleid())
					{
						name_ = familyui->vectorApplyGuildMember.at(i)->name();
						std::vector<CGuildMemberBase *>::iterator iter= familyui->vectorApplyGuildMember.begin() + i;
						CGuildMemberBase * applyPlayer=*iter ;
						familyui->vectorApplyGuildMember.erase(iter);
						delete applyPlayer;
					}
				}

				ManageFamily * manageFamily_ = (ManageFamily *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagManageFamily);
				if (manageFamily_ != NULL)
				{
					manageFamily_->tableviewstate->reloadData();
					MainScene * mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
					GameView::getInstance()->applyPlayersize = familyui->vectorApplyGuildMember.size();
					mainscene_->remindFamilyApply();
				}
			}

			if (strlen(name_.c_str()) > 0)
			{
				const char *str1  = StringDataManager::getString("family_isRefuseName_");;
				const char *str2  = StringDataManager::getString("family_isApplyFamilyName_");;
				std::string string_ = str1;
				string_.append(name_);
				string_.append(str2);
				GameView::getInstance()->showAlertDialog(string_);
			}
		}
	}
	
	if (bean.result() == -1)
	{
		GameView::getInstance()->showAlertDialog(bean.msg());
	}
}
