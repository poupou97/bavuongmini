#include "PushHandler1111.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../gamescene_state/role/MonsterOwnedStates.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../gamescene_state/LoadScene.h"

IMPLEMENT_CLASS(PushHandler1111)

PushHandler1111::PushHandler1111() 
{

}
PushHandler1111::~PushHandler1111() 
{

}
void* PushHandler1111::createInstance()
{
	return new PushHandler1111() ;
}
void PushHandler1111::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1111::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1111::handle(CommonMessage* mb)
{
	Rsp1111 bean;
	bean.ParseFromString(mb->data());

	CCLOG("msg: %d", mb->cmdid());

	GameView* gv = GameView::getInstance();

	gv->getMapInfo()->set_channel(bean.idx());
	gv->getMapInfo()->set_x(bean.x());
	gv->getMapInfo()->set_y(bean.y());

	//��ʼ����
	gv->myplayer->clear();	
	gv->myplayer->setDissociate(false);
	gv->myplayer->setGameScene(NULL);
	gv->myplayer->setWorldPosition(Vec2(bean.x(), bean.y()));

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene != NULL)
		scene->pause();
// 	GameState* pScene = new LoadSceneState();
// 	if (pScene)
// 	{
// 		pScene->runThisState();
// 		pScene->release();
// 	}

	Scene* pScene = Director::getInstance()->getRunningScene();
	if(pScene->getTag() == GameView::STATE_GAME)
	{
		GameSceneState* pState = (GameSceneState*)pScene;
		if(pState->getState() == GameSceneState::state_load)
		{
			SceneLoadLayer* pLoadSceneNode = (SceneLoadLayer*)pState->getChildByTag(GameSceneState::TAG_LOADSCENE);
			if(pLoadSceneNode != NULL)
			{
				pLoadSceneNode->setState(SceneLoadLayer::State_Init);
				return;
			}
		}
		pState->changeToLoadState();
	}
	else
	{
		SceneLoadLayer* pLoadSceneNode = (SceneLoadLayer*)pScene->getChildByTag(GameSceneState::TAG_LOADSCENE);
		if(pLoadSceneNode != NULL)
			pLoadSceneNode->setState(SceneLoadLayer::State_Init);
	}
}