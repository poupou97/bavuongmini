#include "PushHandler5141.h"

#include "../../GameView.h"
#include "../protobuf/DailyMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/giveFlower/GetFlower.h"
#include "../../gamescene_state/role/BasePlayer.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/ActorUtils.h"

IMPLEMENT_CLASS(PushHandler5141)

PushHandler5141::PushHandler5141() 
{

}
PushHandler5141::~PushHandler5141() 
{
	
}
void* PushHandler5141::createInstance()
{
	return new PushHandler5141() ;
}
void PushHandler5141::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5141::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5141::handle(CommonMessage* mb)
{
	Push5141 bean;
	bean.ParseFromString(mb->data());

	MainScene * mainScene_ = GameView::getInstance()->getMainUIScene();
	if (mainScene_ == NULL)
	{
		return;
	}
	const char * str_1 = StringDataManager::getString("giveFlower_giveFlowerPupop");
	char str_str[100];
	sprintf(str_str,str_1,bean.number());
	const char * str_2 = StringDataManager::getString("giveFlower_giveFlowerPupopIsThank");
	std::string str_ = bean.name();
	str_.append(str_str);
	str_.append(bean.flowername());
	str_.append(str_2);

	Size winSize=Director::getInstance()->getVisibleSize();
	GetFlower * flower_ = GetFlower::create(bean.id(),bean.name(),str_);
	flower_->setIgnoreAnchorPointForPosition(false);
	flower_->setAnchorPoint(Vec2(0.5f,0.5f));
	flower_->setPosition(Vec2(winSize.width/2,winSize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(flower_);

	///
	int pressionId_ = GameView::getInstance()->myplayer->getProfession();
	if (BasePlayer::getRoleSexByProfessionId(pressionId_) == 2)
	{
		MyPlayer* m_pMyPlayer = GameView::getInstance()->myplayer;
		ActorUtils::addFlowerParticle(m_pMyPlayer);
	}
}

