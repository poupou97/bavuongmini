#include "PushHandler1125.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"

#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../gamescene_state/role/MonsterOwnedStates.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/skill/GameFightSkill.h"
#include "../element/CFightSkill.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/skillscene/SkillScene.h"

IMPLEMENT_CLASS(PushHandler1125)

PushHandler1125::PushHandler1125() 
{

}
PushHandler1125::~PushHandler1125() 
{

}
void* PushHandler1125::createInstance()
{
	return new PushHandler1125() ;
}
void PushHandler1125::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1125::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1125::handle(CommonMessage* mb)
{
	Push1125 bean;
	bean.ParseFromString(mb->data());

	//CCLOG("msg: %d, skill", mb->cmdid());

	GameView::getInstance()->GameFightSkillList.erase(GameView::getInstance()->GameFightSkillList.begin(),GameView::getInstance()->GameFightSkillList.end());
	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
	{
		GameFightSkill * tempSkill = it->second;
		delete tempSkill;
	}
	GameView::getInstance()->GameFightSkillList.clear();

	GameView::getInstance()->CBaseSkillList.erase(GameView::getInstance()->CBaseSkillList.begin(),GameView::getInstance()->CBaseSkillList.end());
	std::vector<CBaseSkill*>::iterator iter;
	for (iter = GameView::getInstance()->CBaseSkillList.begin(); iter != GameView::getInstance()->CBaseSkillList.end(); ++iter)
	{
		delete *iter;
	}
	GameView::getInstance()->CBaseSkillList.clear();

	std::vector<CFightSkill*>fightSkillList;
	GameView* gameView = GameView::getInstance();
	int a = bean.professionskills_size();
	for (int i = 0;i<a;i++)
	{
		CBaseSkill* baseSkill = new CBaseSkill();
		//baseSkill->CopyFrom(bean.professionskills(i));
		baseSkill->CopyFrom(*(StaticDataBaseSkill::s_baseSkillData[bean.professionskills(i).id()]));
		baseSkill->set_owner(StaticDataBaseSkill::s_baseSkillData[bean.professionskills(i).id()]->get_owner());
		GameView::getInstance()->CBaseSkillList.push_back(baseSkill);
	}
	int b = bean.fightskills_size();

	//���ܱ������
	if (b <= 1)
	{
		int allpoint = 0;
		if (GameView::getInstance()->myplayer->getActiveRole()->level()>=5)
		{
			allpoint = GameView::getInstance()->myplayer->getActiveRole()->level() - 4;
		}
		else{}
		GameView::getInstance()->myplayer->player->set_skillpoint(allpoint);
	}

	for (int i = 0;i<b;i++)
	{
		CFightSkill* fightSkill = new CFightSkill();
		fightSkill->CopyFrom(bean.fightskills(i));
		fightSkillList.push_back(fightSkill);

		CBaseSkill * temp = new CBaseSkill();
		for(int j = 0;j<GameView::getInstance()->CBaseSkillList.size();j++)
		{
			std::string fightSkillId = fightSkill->id();
			std::string baseSkillId = GameView::getInstance()->CBaseSkillList.at(j)->id();
			if (strcmp(fightSkillId.c_str(),baseSkillId.c_str()) == 0)
			{
				temp->CopyFrom(*GameView::getInstance()->CBaseSkillList.at(j));
				break;
			}
		}

		GameFightSkill * gameFightSkill = new GameFightSkill();
		gameFightSkill->CopyFromFightSkill(fightSkillList.at(i));
		gameFightSkill->initSkill(fightSkillList.at(i)->id(), fightSkillList.at(i)->id(), fightSkillList.at(i)->level(), GameActor::type_player);
		gameView->GameFightSkillList.insert(make_pair(gameFightSkill->getId(),gameFightSkill));

		delete temp;
	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	SkillScene * skillScene = (SkillScene *)scene->getMainUIScene()->getChildByTag(KTagSkillScene);
	if (skillScene != NULL)
	{
		skillScene->RefreshData();
		skillScene->RefreshToDefault();
		skillScene->RefreshSkillPoint();
	}
}