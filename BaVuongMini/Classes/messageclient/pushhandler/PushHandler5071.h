
#ifndef Blog_C___Reflection_PushHandler5071_h
#define Blog_C___Reflection_PushHandler5071_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5071 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5071)

public:
	SYNTHESIZE(PushHandler5071, int*, m_pValue)

		PushHandler5071() ;
	virtual ~PushHandler5071() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
