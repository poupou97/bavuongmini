
#ifndef Blog_C___Family_PushHandler1503_h
#define Blog_C___Family_PushHandler1503_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1503 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1503)
    
public:
    SYNTHESIZE(PushHandler1503, int*, m_pValue)
    
    PushHandler1503() ;
    virtual ~PushHandler1503() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
