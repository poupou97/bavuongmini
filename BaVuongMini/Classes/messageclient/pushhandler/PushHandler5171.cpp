#include "PushHandler5171.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../element/CShowGeneralInfo.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/General.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../utils/StaticDataManager.h"
#include "../element/CGeneralBaseMsg.h"
#include "../../gamescene_state/role/ShowBaseGeneral.h"


IMPLEMENT_CLASS(PushHandler5171)

PushHandler5171::PushHandler5171() 
{

}
PushHandler5171::~PushHandler5171() 
{

}
void* PushHandler5171::createInstance()
{
	return new PushHandler5171() ;
}
void PushHandler5171::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5171::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5171::handle(CommonMessage* mb)
{
	Rsp5171 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	CShowGeneralInfo* pShowGeneralInfo = new CShowGeneralInfo();
	pShowGeneralInfo->CopyFrom(bean.showgeneral());

	// ���������ǰ�����۴˴���չʾ�����ջ��佫���Ȱ���ǰ���佫�ջأ�����еĻ���
	ShowMyGeneral::getInstance()->generalHide();

	int nActionType = bean.actiontype();
	long long longGeneralId = pShowGeneralInfo->generalid();
	int nTemplateId = pShowGeneralInfo->templateid();
	int nCurrentQuality = pShowGeneralInfo->currentquality();
	int nEvolution = pShowGeneralInfo->evolution();

	ShowMyGeneral::getInstance()->set_generalId(longGeneralId);
	ShowMyGeneral::getInstance()->set_templateId(nTemplateId);
	ShowMyGeneral::getInstance()->set_currentQuality(nCurrentQuality);
	ShowMyGeneral::getInstance()->set_evolution(nEvolution);

	ShowMyGeneral::getInstance()->set_actionType(nActionType);


	// չʾ�佫 or �ջ��佫
	if (ShowMyGeneral::SHOW_ACTIONTYPE == nActionType)
	{
		const char *str1 = StringDataManager::getString("generals_show_zhanshi_sucess");
		char* p1 =const_cast<char*>(str1);

		GameView::getInstance()->showAlertDialog(p1);

		if (ShowMyGeneral::getInstance()->canGeneralShow())
		{
			// չʾ�佫
			ShowMyGeneral::getInstance()->generalShow(pShowGeneralInfo);
		}
	}
	else if(ShowMyGeneral::HIDE_ACTIONTYPE == nActionType)
	{
		const char *str1 = StringDataManager::getString("generals_show_shouhui_sucess");
		char* p1 =const_cast<char*>(str1);

		GameView::getInstance()->showAlertDialog(p1);

		// �ջ��佫(�˴�ò�ƿ��Բ����ã���Ϊ ����ͷ�Ѿ����ù)
		ShowMyGeneral::getInstance()->generalHide();
	}

	CC_SAFE_DELETE(pShowGeneralInfo);
}