
#ifndef Blog_C___Reflection_PushHandler5080_h
#define Blog_C___Reflection_PushHandler5080_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

/*
 * the heartbeat response message
 * Author: Zhao Gang
 */
class PushHandler5080 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler5080)
    
public:
    SYNTHESIZE(PushHandler5080, int*, m_pValue)
    
    PushHandler5080() ;
    virtual ~PushHandler5080() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
