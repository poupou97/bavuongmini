
#ifndef Blog_C___Family_PushHandler1506_h
#define Blog_C___Family_PushHandler1506_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1506 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1506)
    
public:
    SYNTHESIZE(PushHandler1506, int*, m_pValue)
    
    PushHandler1506() ;
    virtual ~PushHandler1506() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
