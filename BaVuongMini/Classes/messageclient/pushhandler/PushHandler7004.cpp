
#include "PushHandler7004.h"

#include "../protobuf/MainMessage.pb.h"
#include "../protobuf/PlayerMessage.pb.h"  
#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/OtherPlayer.h"
#include "../../gamescene_state/role/OtherPlayerOwnedStates.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/FolderInfo.h"
#include "../../ui/backpackscene/PackageItem.h"
#include "../protobuf/MissionMessage.pb.h"
#include "../element/MissionInfo.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../ui/missionscene/MissionScene.h"
#include "../../gamescene_state/role/FunctionNPC.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../ui/extensions//UITab.h"
#include "../../gamescene_state/sceneelement/MissionCell.h"
#include "../../utils/StaticDataManager.h"
#include "../../utils/StrUtils.h"
#include "../../gamescene_state/EffectDispatch.h"

IMPLEMENT_CLASS(PushHandler7004)

PushHandler7004::PushHandler7004() 
{

}
PushHandler7004::~PushHandler7004() 
{

}
void* PushHandler7004::createInstance()
{
	return new PushHandler7004() ;
}
void PushHandler7004::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler7004::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler7004::handle(CommonMessage* mb)
{
	Push7004 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	std::string pacId = bean.missionpackageid();
//	std::string missId = bean.missionid();

	bool isOrNotAddToMainList = false;
	MissionInfo * oldMissionInfo = new MissionInfo();

	//update missionList
	for (int i = 0;i<(int)GameView::getInstance()->missionManager->MissionList.size();i++)
	{
		MissionInfo * tempMInfo = GameView::getInstance()->missionManager->MissionList.at(i);
		if (strcmp(tempMInfo->missionpackageid().c_str(),pacId.c_str()) == 0)
		{
			if (strcmp(tempMInfo->missionid().c_str(),bean.missioninfo().missionid().c_str()) == 0)
			{
				oldMissionInfo->CopyFrom(*tempMInfo);
				tempMInfo->CopyFrom(bean.missioninfo());
			}
		}
	}

	int changeStatus = 0; //(0:update  1:insert  2:remove)
	int changeCellIndex = 0;
	bool isReloadWithDefaultOffSet = true;
	//update missionList_Main
	if (bean.missioninfo().missionpackagetype() > 2 && bean.missioninfo().missionstate() == 0)   //�ճ���������
	{
		for (int i = 0;i<(int)GameView::getInstance()->missionManager->MissionList_MainScene.size();i++)
		{
			MissionInfo * tempInfo = GameView::getInstance()->missionManager->MissionList_MainScene.at(i);
			if (strcmp(tempInfo->missionpackageid().c_str(),pacId.c_str()) == 0)
			{
				vector<MissionInfo*>::iterator iter = GameView::getInstance()->missionManager->MissionList_MainScene.begin()+i;
				GameView::getInstance()->missionManager->MissionList_MainScene.erase(iter);
				delete tempInfo;
				changeStatus = 2;
				changeCellIndex = i;
				break;
			}
		}
	}
	if (bean.missioninfo().missionpackagetype() <= 2 ||(bean.missioninfo().missionpackagetype() > 2 && bean.missioninfo().missionstate() > 0))
	{
		bool isExist = false;
		for (int i = 0;i<(int)GameView::getInstance()->missionManager->MissionList_MainScene.size();i++)
		{
			MissionInfo * tempInfo = GameView::getInstance()->missionManager->MissionList_MainScene.at(i);
			if (strcmp(tempInfo->missionpackageid().c_str(),pacId.c_str()) == 0)
			{
				if (strcmp(tempInfo->missionid().c_str(),bean.missioninfo().missionid().c_str()) == 0)
				{
					bool isstatechanged = true;
					if(tempInfo->missionstate()== bean.missioninfo().missionstate())
					{
						isstatechanged = false;
					}

					tempInfo->CopyFrom(bean.missioninfo());
					tempInfo->isStateChanged = isstatechanged;
					tempInfo->isNeedTeach = true;
					changeStatus = 0;
					changeCellIndex = i; 

					isExist = true;
					break;
				}
			}
		}

		if (!isExist)
		{
			MissionInfo * newMission = new MissionInfo();
			newMission->CopyFrom(bean.missioninfo());
			//GameView::getInstance()->missionManager->MissionList_MainScene.push_back(newMission);
			changeStatus = 1;

			if (GameView::getInstance()->missionManager->m_sLastMissionPackageId == newMission->missionpackageid())
			{
				GameView::getInstance()->missionManager->MissionList_MainScene.insert(GameView::getInstance()->missionManager->MissionList_MainScene.begin()+GameView::getInstance()->missionManager->m_nLastMissionIndexInMain,newMission);
				changeCellIndex = GameView::getInstance()->missionManager->m_nLastMissionIndexInMain;
				isReloadWithDefaultOffSet = false;
			}
			else
			{
				GameView::getInstance()->missionManager->MissionList_MainScene.push_back(newMission);
				changeCellIndex = GameView::getInstance()->missionManager->MissionList_MainScene.size()-1; 
			}
		}
	}
	
	// it's not game state, ignore
	Scene* pScene = Director::getInstance()->getRunningScene();
	if(pScene->getTag() != GameView::STATE_GAME)
	{
		return;
	}
	else
	{
		GameSceneState* pState = (GameSceneState*)pScene;
		if(pState->getState() == GameSceneState::state_load)
		{
			return;
		}
	}

	MainScene * mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
	MissionInfo * tempMission = new MissionInfo();
	tempMission->CopyFrom(bean.missioninfo());
	if ((oldMissionInfo->missionstate() == dispatched) && (tempMission->missionstate() != dispatched))
	//if (oldMissionInfo->missionstate() == dispatched)
	{
		//mainscene->addInterfaceAnm("jsrw/jsrw.anm");
		EffectOfNormal * effect_normal = new EffectOfNormal();
		effect_normal->setEffectStrPath("jsrw/jsrw.anm");
		effect_normal->setType(type_normal);
		EffectDispatcher::pushEffectToVector(effect_normal);
	}

	delete tempMission;

	if (mainscene->getChildByTag(kTagMissionAndTeam) != NULL)
	{
		MissionAndTeam * missionAndTeam = (MissionAndTeam *)mainscene->getChildByTag(kTagMissionAndTeam);
		//missionAndTeam->reloadMissionDataWithoutChangeOffSet();
		switch(changeStatus)
		{
		case 0:  //update
			{
				missionAndTeam->missionTableView->updateCellAtIndex(changeCellIndex);
				MissionCell *tempCell = dynamic_cast<MissionCell*>(missionAndTeam->missionTableView->cellAtIndex(changeCellIndex));
				if (tempCell)
				{
					if (GameView::getInstance()->missionManager->MissionList_MainScene.size() > changeCellIndex)
					{
						MissionInfo * tempInfo = GameView::getInstance()->missionManager->MissionList_MainScene.at(changeCellIndex);
						if (tempInfo->action().action() == moveToFight || tempInfo->action().action() == moveToCollection )
						{
							int index = tempInfo->summary().find("0/");   // ignore, 0/10, keep 1/10, 2/10 etc.
							if(index < 0)
								GameView::getInstance()->showAlertDialog(tempInfo->summary().c_str());
						}

						if (tempInfo->missionstate() == done)
						{
							std::string finishedTip = StringDataManager::getString("chat_goodsInfoShowBegin");   // "["
							finishedTip.append(tempInfo->missionname());
							finishedTip.append(StringDataManager::getString("chat_goodsInfoShowEnd"));   // "]"
							//finishedTip = StrUtils::applyColor(finishedTip.c_str(), Color3B(0, 255, 0));
							finishedTip.append(StringDataManager::getString("mission_finished"));
							tempCell->addFinishAnm();
							GameView::getInstance()->showAlertDialog(finishedTip);
						}
					}
				}
			}
			break;
		case 1:  //insert
			{
				//missionAndTeam->missionTableView->insertCellAtIndex(changeCellIndex);
				if (isReloadWithDefaultOffSet)
				{
					missionAndTeam->reloadMissionDataWithoutChangeOffSet(missionAndTeam->missionTableView->getContentOffset(),missionAndTeam->missionTableView->getContentSize());
				}
				else
				{
					missionAndTeam->reloadMissionDataWithoutChangeOffSet(GameView::getInstance()->missionManager->m_pLastMissionOffSet,GameView::getInstance()->missionManager->m_missionTableViewSize);
				}

				MissionCell *tempCell = dynamic_cast<MissionCell*>(missionAndTeam->missionTableView->cellAtIndex(changeCellIndex));
				if (tempCell)
				{
					tempCell->addFinishAnm();
				}
			}
			break;
		case 2:  //remove
			{
				//missionAndTeam->missionTableView->removeCellAtIndex(changeCellIndex);
				missionAndTeam->reloadMissionData();
			}
			break;
		}
		
	}

	if (mainscene->getChildByTag(kTagMissionScene) != NULL)
	{
		MissionScene * missionScene = (MissionScene *)mainscene->getChildByTag(kTagMissionScene);
		int para = 0;
		if (missionScene->mainTab->getCurrentIndex() == 0)
		{
			para = 1;
		}

		if (missionScene->curMissionInfo->has_missionpackageid())
		{
			if (strcmp(missionScene->curMissionInfo->missionpackageid().c_str(),bean.missionpackageid().c_str()) == 0)
			{
				if (changeStatus == 2)  //remove
				{
					missionScene->updataCurDataSourceByMissionState(para);
					missionScene->m_tableView->reloadData();
					//�����Ĭ�
					if (missionScene->curMissionDataSource.size()>0)
					{
						missionScene->curMissionInfo->CopyFrom(*(missionScene->curMissionDataSource.at(0)));
						missionScene->m_tableView->cellAtIndex(0);
						missionScene->selectCellIdx = 0;
					}

					missionScene->ReloadMissionInfoData();
				}
				else
				{
					missionScene->updataCurDataSourceByMissionState(para);
					missionScene->curMissionInfo->CopyFrom(bean.missioninfo());
					missionScene->ReloadMissionInfoData();
				}
			}
		}
	}


	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	MissionInfo * npcMissionInfo = new MissionInfo();
	npcMissionInfo->CopyFrom(bean.missioninfo());
	if (oldMissionInfo->has_missionpackageid())
	{
		if(oldMissionInfo->action().moveto().has_targetnpc())
		{
			FunctionNPC* fn = dynamic_cast<FunctionNPC*>(scene->getActor(oldMissionInfo->action().moveto().targetnpc().npcid()));
			if (fn != NULL)
			{
				fn->RefreshNpc();
			}
		}
		else if (oldMissionInfo->has_tip())
		{
			FunctionNPC* fn = dynamic_cast<FunctionNPC*>(scene->getActor(oldMissionInfo->tip().npcid()));
			if (fn != NULL)
			{
				fn->RefreshNpc();
			}
		}
	}

	
	if (npcMissionInfo->action().moveto().has_targetnpc())
	{
		FunctionNPC* fn = dynamic_cast<FunctionNPC*>(scene->getActor(npcMissionInfo->action().moveto().targetnpc().npcid()));
		if (fn != NULL)
		{
			fn->RefreshNpc();
		}
	}
	else if (npcMissionInfo->has_tip())
	{
		FunctionNPC* fn = dynamic_cast<FunctionNPC*>(scene->getActor(npcMissionInfo->tip().npcid()));
		if (fn != NULL)
		{
			fn->RefreshNpc();
		}
	}

//	if (npcMissionInfo->missionstate() == accepted)
//	{
		GameView::getInstance()->missionManager->autoPopUpHandleMission();
//	}

	delete npcMissionInfo;

	//mainscene->checkIsNewRemind();
	mainscene->remindMission();

	//add by yangjun 2014.9.26   test for autoFinishMission
	//ϴ�ֺͲɼ�������ɺ��Զ�ȥ�����
// 	if (mainscene)
// 	{
// 		MissionAndTeam * missionAndTeam = (MissionAndTeam *)mainscene->getChildByTag(kTagMissionAndTeam);
// 		if (missionAndTeam)
// 		{
// 			MissionInfo * tempMission = new MissionInfo();
// 			tempMission->CopyFrom(bean.missioninfo());
// 			if (tempMission->missionstate() == done)
// 			{
// 				if (oldMissionInfo->action().action() == moveToFight || oldMissionInfo->action().action() == moveToCollection )
// 				{
// 					GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*tempMission);
// 					GameView::getInstance()->missionManager->addMission(GameView::getInstance()->missionManager->curMissionInfo);
// 				}
// 			}
// 
// 			delete tempMission;
// 		}
// 	}

	delete oldMissionInfo;
}
