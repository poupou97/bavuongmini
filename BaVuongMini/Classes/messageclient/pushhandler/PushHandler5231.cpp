#include "PushHandler5231.h"
#include "../protobuf/PlayerMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../element/CTargetPosition.h"
#include "../element/CTargetNpc.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../element/CMapInfo.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../ui/Active_ui/ActiveManager.h"
#include "../../utils/StaticDataManager.h"

IMPLEMENT_CLASS(PushHandler5231)

PushHandler5231::PushHandler5231() 
{

}
PushHandler5231::~PushHandler5231() 
{
	
}
void* PushHandler5231::createInstance()
{
	return new PushHandler5231() ;
}
void PushHandler5231::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5231::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5231::handle(CommonMessage* mb)
{
	Rsp5231 bean;
	bean.ParseFromString(mb->data());
	
	UserDefault::getInstance()->setBoolForKey("showNearbyPlayer", bean.seeother());
	UserDefault::getInstance()->flush();
}