
#include "PushHandler1411.h"

#include "../protobuf/InteractMessage.pb.h"  
#include "GameView.h"
#include "../element/CAroundPlayer.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Friend_ui/TeamList.h"
#include "../../ui/Friend_ui/AroundPlayerTabview.h"

IMPLEMENT_CLASS(PushHandler1411)

PushHandler1411::PushHandler1411() 
{
    
}
PushHandler1411::~PushHandler1411() 
{
    
}
void* PushHandler1411::createInstance()
{
    return new PushHandler1411() ;
}
void PushHandler1411::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1411::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1411::handle(CommonMessage* mb)
{
	Push1411 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	std::vector<CAroundPlayer *>::iterator iter;
	for (iter = GameView::getInstance()->aroundPlayerVector.begin(); iter != GameView::getInstance()->aroundPlayerVector.end(); ++iter)
	{
		delete *iter;
	}
	GameView::getInstance()->aroundPlayerVector.clear();


	bean.members();// �����Ա�б
	
	for (int i=0;i<bean.members_size();i++ )
	{
		CAroundPlayer * player=new CAroundPlayer();
		player->CopyFrom(bean.members(i));

		GameView::getInstance()->aroundPlayerVector.push_back(player);
	}

	FriendUi * friendui= (FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	TableView * tab=(TableView *)friendui->aroundplayer->getChildByTag(AROUNDPLAYERTABVIEW);
	tab->reloadData();

}
