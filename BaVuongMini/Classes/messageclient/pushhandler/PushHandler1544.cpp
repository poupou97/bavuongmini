
#include "PushHandler1544.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightUI.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightRewardsUI.h"

IMPLEMENT_CLASS(PushHandler1544)

PushHandler1544::PushHandler1544() 
{

}
PushHandler1544::~PushHandler1544() 
{

}
void* PushHandler1544::createInstance()
{
	return new PushHandler1544() ;
}
void PushHandler1544::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1544::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1544::handle(CommonMessage* mb)
{
	Rsp1544 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	FamilyFightData::setFamilyFightTimeClass(bean.timeclass());
	FamilyFightData::isRegisted = bean.registstatus();
	FamilyFightData::setFamilyFightFightClasss(bean.fightstatus());
	FamilyFightData::setFamilyFightRanking(bean.guildranking());
	FamilyFightData::setFamilyFightScore(bean.guildscore());

	if (bean.has_battletime())
	{
		if (bean.battletime().iscancel() == 1)
		{
			FamilyFightData::isCancel = true;
		}
		else
		{
			FamilyFightData::isCancel = false;
		}

		if (bean.battletime().has_curtime())
		{
			FamilyFightData::setCurTime(bean.battletime().curtime());
		}

		if (bean.battletime().has_nexttime())
		{
			FamilyFightData::setNextTime(bean.battletime().nexttime());
		}

		if (bean.battletime().has_entrylefttime())
		{
			FamilyFightData::setEntryLeftTime(bean.battletime().entrylefttime());
		}
	}

	if (GameView::getInstance()->getMainUIScene())
	{
		FamilyUI * familyUI = (FamilyUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
		if (familyUI)
		{
			if(familyUI->GetFamilyFightUI())
			{
				FamilyFightUI * familyFightUI = familyUI->GetFamilyFightUI();
				familyFightUI->RefreshBtnSignUpStatus();
				familyFightUI->RefreshFamilyRankingAndScore();
				familyFightUI->RefreshTimeInfo();
			}
		}

		FamilyFightRewardsUI * familyFightRewardUI = (FamilyFightRewardsUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightRewardUI);
		if (familyFightRewardUI)
		{
			familyFightRewardUI->RefreshMyRanking();
		}
	}
}
