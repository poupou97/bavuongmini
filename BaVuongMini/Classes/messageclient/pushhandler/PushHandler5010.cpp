
#include "PushHandler5010.h"

#include "../protobuf/CollectMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../element/CPickingInfo.h"
#include "../../GameView.h"
#include "../../legend_engine/GameWorld.h"
#include "../element/CPushCollectInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/missionscene/MissionManager.h"

IMPLEMENT_CLASS(PushHandler5010)

PushHandler5010::PushHandler5010() 
{
    
}
PushHandler5010::~PushHandler5010() 
{
    
}
void* PushHandler5010::createInstance()
{
    return new PushHandler5010() ;
}
void PushHandler5010::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5010::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler5010::handle(CommonMessage* mb)
{
	PushCollectInfos5010 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	for(int i = 0; i < bean.infos_size(); i++)
	{
		PushCollectInfo* pInfo = bean.mutable_infos(i);

		CPickingInfo* pickingInfo = GameWorld::PickingInfos[pInfo->id()];   // get template Id
		
		PickingActor* actor = new PickingActor();
		actor->setRoleId(pInfo->instanceid());
		actor->p_collectInfo->CopyFrom(bean.infos(i));
		actor->set_templateId(actor->p_collectInfo->id());
		scene->putActor(pInfo->instanceid(), actor);
		actor->setActorName(pickingInfo->name.c_str());

		actor->init(pickingInfo->avatar.c_str());
		if (pInfo->state() == ACTIVITY)
		{
			actor->setActivied(true);
		}
		else if (pInfo->state() == INVAILD)
		{
			actor->setActivied(false);
		}
		int posX = GameSceneLayer::tileToPositionX(pInfo->x());
		int posY = GameSceneLayer::tileToPositionY(pInfo->y());
		actor->setGameScene(scene);
		actor->setWorldPosition(Vec2(posX, posY));

		// add new actor into cocos2d-x render scene
		Node* actorLayer = scene->getActorLayer();
		actorLayer->addChild(actor);

		actor->release();

	}
}
