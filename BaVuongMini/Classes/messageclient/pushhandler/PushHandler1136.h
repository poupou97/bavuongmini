
#ifndef Blog_C___Reflection_PushHandler1136_h
#define Blog_C___Reflection_PushHandler1136_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1136 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1136)
public:
	SYNTHESIZE(PushHandler1136, int*, m_pValue)

		PushHandler1136() ;
	virtual ~PushHandler1136() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;
} ;

#endif