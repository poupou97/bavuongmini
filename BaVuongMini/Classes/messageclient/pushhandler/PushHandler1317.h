
#ifndef Blog_C___Reflection_PushHandler1317_h
#define Blog_C___Reflection_PushHandler1317_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler1317 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler1317)

public:
	SYNTHESIZE(PushHandler1317, int*, m_pValue)

		PushHandler1317() ;
	virtual ~PushHandler1317() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

private:

protected:
	int *m_pValue ;

} ;

#endif
