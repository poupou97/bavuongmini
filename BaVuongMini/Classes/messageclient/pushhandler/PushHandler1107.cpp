#include "PushHandler1107.h"

#include "../protobuf/PlayerMessage.pb.h"  
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../gamescene_state/GameScene.h"
#include "../../gamescene_state/role/Monster.h"

IMPLEMENT_CLASS(PushHandler1107)

PushHandler1107::PushHandler1107() 
{
    
}
PushHandler1107::~PushHandler1107() 
{
    
}
void* PushHandler1107::createInstance()
{
    return new PushHandler1107() ;
}
void PushHandler1107::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1107::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1107::handle(CommonMessage* mb)
{
	Push1107 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, role id: %lld, out of view", mb->cmdid(), bean.playerid());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;
	GameActor* actor = scene->getActor(bean.playerid());
	if(actor != NULL)
	{
		GameScene::s_bornMonsterMap.erase(actor->getRoleId());

		Monster* pMonster = dynamic_cast<Monster*>(actor);
		if(pMonster != NULL)
		{
			// this actor will be dead, so let it be
			if(pMonster->isAction(ACT_DIE))
				return;

			pMonster->changeAction(ACT_DISAPPEAR);
			return;
		}

		actor->setDestroy(true);
	}
}