
#include "PushHandler1402.h"

#include "../protobuf/InteractMessage.pb.h"  
#include "../element/CMapTeam.h"
#include "GameView.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/CTeamMember.h"
#include "../../ui/Friend_ui/TeamList.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../gamescene_state/sceneelement/MyHeadInfo.h"
#include "../GameMessageProcessor.h"


IMPLEMENT_CLASS(PushHandler1402)

PushHandler1402::PushHandler1402() 
{
    
}
PushHandler1402::~PushHandler1402() 
{
    
}
void* PushHandler1402::createInstance()
{
    return new PushHandler1402() ;
}
void PushHandler1402::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1402::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1402::handle(CommonMessage* mb)
{
	Push1402 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	std::vector<CTeamMember *>::iterator iter;
	for (iter=GameView::getInstance()->teamMemberVector.begin();iter!=GameView::getInstance()->teamMemberVector.end();++iter)
	{
		delete * iter;
	}
	GameView::getInstance()->teamMemberVector.clear();

	for (int i=0;i<bean.members_size();i++)
	{
		CTeamMember * teammember =new CTeamMember();
		teammember->CopyFrom(bean.members(i));
		GameView::getInstance()->teamMemberVector.push_back(teammember);
	}
	if (GameView::getInstance()->getMainUIScene() == NULL)
	{
		return;
	}
	
	MissionAndTeam * mainSceneTeam_ =(MissionAndTeam *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
	mainSceneTeam_->leaderId =	bean.leaderid();
	
	
	FriendUi * friendui_=(FriendUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFriendUi);
	if (friendui_ !=NULL && friendui_->curType == 3)
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1408,(void *)0);
	}
	
// 	MainScene * mainscene=(MainScene *) GameView::getInstance()->getMainUIScene();
// 	if (mainscene != NULL)
// 	{
// 		Button * button = (Button *)mainscene->curMyHeadInfo->teamInLine;
// 		button->setVisible(true);
// 	}
}
