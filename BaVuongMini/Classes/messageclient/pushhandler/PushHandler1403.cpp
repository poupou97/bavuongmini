
#include "PushHandler1403.h"

#include "../protobuf/InteractMessage.pb.h"  
#include "../element/CMapTeam.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../gamescene_state/sceneelement/MyHeadInfo.h"
#include "../element/CTeamMember.h"

IMPLEMENT_CLASS(PushHandler1403)

PushHandler1403::PushHandler1403() 
{
    
}
PushHandler1403::~PushHandler1403() 
{
    
}
void* PushHandler1403::createInstance()
{
    return new PushHandler1403() ;
}
void PushHandler1403::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1403::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1403::handle(CommonMessage* mb)
{
	Push1403 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	bool isExist = false;
	//���vector �����Ѿ��ӹ �����Ҫ�ټ���������Ϣ
	for (int i=0;i<GameView::getInstance()->teamMemberVector.size();i++)
	{
		if (bean.member().roleid() == GameView::getInstance()->teamMemberVector.at(i)->roleid())
		{
			isExist =true;
		}
	}
	if (isExist ==false)
	{
		CTeamMember * teammember =new CTeamMember();
		teammember->CopyFrom(bean.member());
		GameView::getInstance()->teamMemberVector.push_back(teammember);
	}
	
	MissionAndTeam * mainSceneTeam_ =(MissionAndTeam *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
	if (mainSceneTeam_ != NULL)
	{
		mainSceneTeam_->refreshTeamPlayer();
	}
// 	MainScene * mainscene=(MainScene *) GameView::getInstance()->getMainUIScene();
// 	if (mainscene != NULL)
//	{
//		Button * button = (Button *)mainscene->curMyHeadInfo->teamInLine;
// 		button->setVisible(true);
}
