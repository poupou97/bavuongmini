
#ifndef Blog_C___Reflection_PushHandler1802_h
#define Blog_C___Reflection_PushHandler1802_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1802 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1802)
    
public:
    SYNTHESIZE(PushHandler1802, int*, m_pValue)
    
    PushHandler1802() ;
    virtual ~PushHandler1802() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
} ;

#endif
