
#ifndef Blog_C___Family_PushHandler1710_h
#define Blog_C___Family_PushHandler1710_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1710 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1710)
    
public:
    SYNTHESIZE(PushHandler1710, int*, m_pValue)
    
    PushHandler1710() ;
    virtual ~PushHandler1710() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
