
#include "PushHandler1210.h"

#include "../protobuf/FightMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../../GameView.h"
#include "../ProtocolHelper.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Chat_ui/ChatCell.h"
#include "../../gamescene_state/sceneelement/ChatWindows.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../../AppMacros.h"
#include "../../ui/Chat_ui/ChatUI.h"
#include "../../ui/Chat_ui/Tablist.h"
#include "../element/CGeneralBaseMsg.h"
#include "../../ui/generals_ui/GeneralsListBase.h"
#include "../../utils/StrUtils.h"
#include "../../gamescene_state/role/BaseFighterConstant.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "../../gamescene_state/MainAnimationScene.h"

#define Speed_Action_exp 12.f

IMPLEMENT_CLASS(PushHandler1210)

PushHandler1210::PushHandler1210() 
{

}
PushHandler1210::~PushHandler1210() 
{

}
void* PushHandler1210::createInstance()
{
	return new PushHandler1210() ;
}
void PushHandler1210::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1210::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1210::handle(CommonMessage* mb)
{
	Push1210 bean;
	bean.ParseFromString(mb->data());

	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int curExp = bean.currexp();
	int getExp = bean.experience();

	// update myplayer's exp value
	MyPlayer* me = GameView::getInstance()->myplayer;
	me->player->set_experience(curExp);

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;
	MainScene* mainScene = scene->getMainUIScene();
	if(mainScene == NULL)
		return;

	// update the experience bar at the bottom of the screen
	int nextExp = GameView::getInstance()->myplayer->player->nextlevelexperience();   //��һ�����
	if (getExp*1.0f/nextExp > .05f)
	{
		if(mainScene->isLevelUped)
		{
			mainScene->isLevelUped = false;
			Size winSize = Director::getInstance()->getVisibleSize();
			GetExpEffect * expEffect = GetExpEffect::create(curExp,nextExp,true);
			expEffect->setIgnoreAnchorPointForPosition(false);
			expEffect->setAnchorPoint(Vec2(.5f,.5f));
			expEffect->setPosition(Vec2(winSize.width/2,winSize.height/2));
			mainScene->getMainUIAnimationLayer()->addChild(expEffect);
		}
		else
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			GetExpEffect * expEffect = GetExpEffect::create(curExp,nextExp,false);
			expEffect->setIgnoreAnchorPointForPosition(false);
			expEffect->setAnchorPoint(Vec2(.5f,.5f));
			expEffect->setPosition(Vec2(winSize.width/2,winSize.height/2));
			mainScene->getMainUIAnimationLayer()->addChild(expEffect);
		}
	}
	else
	{
		if(mainScene->isLevelUped)
		{
			mainScene->isLevelUped = false;
			float scaleValue = (float)(curExp)/(nextExp);
			Sequence * sequence = Sequence::create(ScaleTo::create((1.f-mainScene->ImageView_exp->getScaleX())*100.f/Speed_Action_exp,1.0f,1.0f),ScaleTo::create(0.001f,0.0f,1.0f),ScaleTo::create(scaleValue*100.f/Speed_Action_exp,scaleValue,1.0f),NULL);
			mainScene->ImageView_exp->runAction(sequence);	
		}
		else
		{
			float scaleValue = (float)(curExp)/(nextExp);
			Sequence * sequence = Sequence::create(ScaleTo::create((scaleValue - mainScene->ImageView_exp->getScaleX())*100.f/Speed_Action_exp,scaleValue,1.0f),NULL);
			mainScene->ImageView_exp->runAction(sequence);
		}
	}

	char diffStr[20];
	sprintf(diffStr,"%d",getExp);
	if (getExp >= 100)
	{
		// put the message to chat UI
		const char * str1_  = StringDataManager::getString("getPlayerAdd");
		const char * str2_  = StringDataManager::getString("getPlayerAddOfAddExp");
		std::string strings = "";
		strings.append(str1_);
		strings.append(diffStr);
		strings.append(str2_);
		int nChannelId = 4;//�ϵͳ��ϢƵ�
		if (strlen(strings.c_str()) > 0)
		{
			ChatUI * chatui_ = (ChatUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTabChat);
// 			if (chatui_ == NULL)
// 			{
// 				ChatWindows::addToMiniChatWindow(nChannelId,"","",strings,NULL,false,0,0,"");
// 			}

			ChatUI::addToMainChatUiMessage(nChannelId,strings);
		}
	}

	//CCRichLabel *pLabel = CCRichLabel::createWithString(strings.c_str(), Size(420,38), NULL,NULL, 0, 15);
	//pLabel->setAnchorPoint(Vec2(0.f, 0.f));
	////pLabel->setPosition(Vec2(450, 80));
	//pLabel->setPosition(Vec2(0, 0));
	//FiniteTimeAction*  action = Sequence::create(
	//	MoveBy::create(0.4f,  Vec2(0, 30)),
	//	DelayTime::create(0.5f),
	//	RemoveSelf::create(),
	//	NULL);
	//pLabel->runAction(action);
	//Node* pMessageWindow = mainScene->getChildByTag(kTagMessageWindows);
	//pMessageWindow->addChild(pLabel);

	// add message to game scene
	std::string expStr = "+";
	expStr.append(diffStr);
	expStr.append("EXP");
	//Label* pLabel = Label::createWithTTF(expStr.c_str(), APP_FONT_NAME, 20);
	//pLabel->setColor(Color3B(164, 73, 164));
	auto pLabel = Label::createWithBMFont("res_ui/font/ziti_7.fnt", expStr.c_str());
	pLabel->setAnchorPoint(Vec2(0.5f,0.0f));
	pLabel->setScale(1.2f);

	pLabel->setPosition(Vec2(me->getPositionX() + 80, me->getPositionY() + 20));
	Action*  action = Sequence::create(
		MoveBy::create(0.8f, Vec2(0, 40)),
		RemoveSelf::create(),
		NULL);
	pLabel->runAction(action);
	scene->getActorLayer()->addChild(pLabel, SCENE_TOP_LAYER_BASE_ZORDER);

	// put the info at the TOP layer
	//Vec2 screenPos = me->getPosition() + scene->getChildByTag(GameSceneLayer::kTagSceneLayer)->getPosition();
	//pLabel->setPosition(Vec2(screenPos.x + 80, screenPos.y + 20));
	//Action*  action = Sequence::create(
	//	MoveBy::create(0.8f, Vec2(0, 40)),
	//	RemoveSelf::create(),
	//	NULL);
	//pLabel->runAction(action);
	//Scene* runningScene = Director::getInstance()->getRunningScene();
	//runningScene->addChild(pLabel);

	//addToMiniChatWindow(strings, nChannelId);

	// show experience by alert
	if (getExp >= 1000)
	{
		std::string str = generateExperienceAlertString("mission_self_recieved", getExp);
		GameView::getInstance()->showAlertDialog(str.c_str());

		//// general exp
		//bool general_fightstatus_inbattle = false;
		//bool general_fightstatus_holdtheline = false;
		//std::vector<CGeneralBaseMsg *>& inLineList = GameView::getInstance()->generalsInLineList;
		//for (unsigned int i = 0; i<inLineList.size(); i++)
		//{
		//	if(inLineList.at(i)->fightstatus() == GeneralsListBase::HoldTheLine)
		//		general_fightstatus_holdtheline = true;
		//	if(inLineList.at(i)->fightstatus() == GeneralsListBase::InBattle)
		//		general_fightstatus_inbattle = true;
		//}
		//if(general_fightstatus_inbattle)
		//{
		//	std::string str = generateExperienceAlertString("mission_general_inbattle_recieved", getExp*GENERAL_INBATTLE_EXPERIENCE_RATIO);
		//	GameView::getInstance()->showAlertDialog(str.c_str());
		//}
		//if(general_fightstatus_holdtheline)
		//{
		//	std::string str = generateExperienceAlertString("mission_general_holdtheline_recieved", getExp*GENERAL_HOLDTHELINE_EXPERIENCE_RATIO);
		//	GameView::getInstance()->showAlertDialog(str.c_str());
		//}
	}
}

std::string PushHandler1210::generateExperienceAlertString(const char* stringId, int exp)
{
	std::string wholeString = StringDataManager::getString(stringId);

	char expStr[10];
	sprintf(expStr, "%d", exp);

	wholeString.append(StrUtils::applyColor(expStr, Color3B(255, 0, 0)));

	//wholeString.append(StringDataManager::getString("getPlayerAddOfAddExp"));
	wholeString.append("/e42");   // /e42 is the icon of the exp

	return wholeString;
}
