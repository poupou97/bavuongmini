
#ifndef Blog_C___Reflection_PushHandler5072_h
#define Blog_C___Reflection_PushHandler5072_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5072 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5072)

public:
	SYNTHESIZE(PushHandler5072, int*, m_pValue)

		PushHandler5072() ;
	virtual ~PushHandler5072() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
