#include "PushHandler5220.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../ui/Active_ui/ActiveManager.h"
#include "../../utils/StaticDataManager.h"
#include "../element/CMapInfo.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../element/CGeneralDetail.h"

IMPLEMENT_CLASS(PushHandler5220)

PushHandler5220::PushHandler5220() 
{

}
PushHandler5220::~PushHandler5220() 
{

}
void* PushHandler5220::createInstance()
{
	return new PushHandler5220() ;
}
void PushHandler5220::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5220::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5220::handle(CommonMessage* mb)
{
	Push5220 bean;
	bean.ParseFromString(mb->data());
	
	if (GameView::getInstance()->getMapInfo()->maptype() != com::future::threekingdoms::server::transport::protocol::coliseum)
		return;
	
	//reload self data
	for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
	{
		CGeneralDetail * generalDetail = GameView::getInstance()->generalsInLineDetailList.at(i);
		MissionAndTeam::GeneralInfoInArena * temp = new MissionAndTeam::GeneralInfoInArena();
		temp->generalId = generalDetail->generalid();
		temp->name = generalDetail->activerole().rolebase().name().c_str();
		temp->level = generalDetail->activerole().level();
		temp->cur_hp = generalDetail->activerole().maxhp();
		temp->max_hp = generalDetail->activerole().hp();
		temp->isFinishedFight = false;
		temp->star = generalDetail->rare();

		MissionAndTeam * missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeam)
		{
			bool isExist = false;
			for(unsigned int i = 0;i<missionAndTeam->generalsInArena_mine.size();i++)
			{
				MissionAndTeam::GeneralInfoInArena * general =  missionAndTeam->generalsInArena_mine.at(i);
				if (temp->generalId == general->generalId)
				{
					general->generalId = temp->generalId;
					general->name = temp->name;
					general->level = temp->level;
					general->cur_hp = temp->cur_hp;
					general->max_hp = temp->max_hp;
					general->isFinishedFight = temp->isFinishedFight;
					general->star = temp->star;
					isExist = true;

					delete temp;
				}
			}
			if (!isExist)
			{
				missionAndTeam->generalsInArena_mine.push_back(temp);
			}

			TableView * offLineArenaTableView_left = (TableView*)missionAndTeam->getLeftOffLineArenaTableView();
			if (offLineArenaTableView_left)
				offLineArenaTableView_left->reloadData();
		}
	}
	//reload empty data
	for (int i = 0;i<bean.generals_size();i++)
	{
		GeneralDetail * generalDetail = bean.mutable_generals(i);
		MissionAndTeam::GeneralInfoInArena * temp = new MissionAndTeam::GeneralInfoInArena();
		temp->generalId = generalDetail->generalid();
		temp->name = generalDetail->activerole().rolebase().name().c_str();
		temp->level = generalDetail->activerole().level();
		temp->cur_hp = generalDetail->activerole().maxhp();
		temp->max_hp = generalDetail->activerole().hp();
		temp->isFinishedFight = false;
		temp->star = generalDetail->rare();

		MissionAndTeam * missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeam)
		{
			bool isExist = false;
			for(unsigned int i = 0;i<missionAndTeam->generalsInArena_other.size();i++)
			{
				MissionAndTeam::GeneralInfoInArena * general =  missionAndTeam->generalsInArena_other.at(i);
				if (temp->generalId == general->generalId)
				{
					general->generalId = temp->generalId;
					general->name = temp->name;
					general->level = temp->level;
					general->cur_hp = temp->cur_hp;
					general->max_hp = temp->max_hp;
					general->isFinishedFight = temp->isFinishedFight;
					general->star = temp->star;
					isExist = true;

					delete temp;
				}
			}
			if (!isExist)
			{
				missionAndTeam->generalsInArena_other.push_back(temp);
			}

			TableView * offLineArenaTableView_right = (TableView*)missionAndTeam->getRightOffLineArenaTableView();
			if (offLineArenaTableView_right)
				offLineArenaTableView_right->reloadData();
		}
	}
}