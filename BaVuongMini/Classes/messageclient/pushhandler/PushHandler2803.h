#ifndef Blog_C___Reflection_PushHandler2803_h
#define Blog_C___Reflection_PushHandler2803_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler2803 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler2803)

public:
	SYNTHESIZE(PushHandler2803, int*, m_pValue)

	PushHandler2803() ;
	virtual ~PushHandler2803() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
