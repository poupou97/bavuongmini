
#include "PushHandler5111.h"


#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../protobuf/DailyGift.pb.h"
#include "../../ui/OnlineReward_ui/OnLineRewardUI.h"
#include "../../messageclient/element/COneOnlineGift.h"
#include "../GameMessageProcessor.h"
#include "../../ui/OnlineReward_ui/OnlineGiftData.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/GameUIConstant.h"
#include "../../ui/Reward_ui/RewardUi.h"


IMPLEMENT_CLASS(PushHandler5111)

PushHandler5111::PushHandler5111() 
{

}
PushHandler5111::~PushHandler5111() 
{
	
}
void* PushHandler5111::createInstance()
{
	return new PushHandler5111() ;
}
void PushHandler5111::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5111::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5111::handle(CommonMessage* mb)
{
	Rsp5111 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int nNumGift = bean.number();				// �����
	int nGiftStatus = bean.status();				// ���ȡ״̬

	OnLineRewardUI * pOnLineRewardUI = (OnLineRewardUI *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOnLineGiftUI);
	if(pOnLineRewardUI == NULL)
		return;
	int nVectorOneOnlineGiftSize = OnlineGiftData::instance()->m_vector_oneOnLineGift.size();
	for (int i = 0; i < nVectorOneOnlineGiftSize; i++)
	{
		// �����
		if (nNumGift == OnlineGiftData::instance()->m_vector_oneOnLineGift.at(i)->number())
		{
			 /** Ž�����Ʒ״̬ ��0.�ɹ������.���Ҫ�೤ʱ�������ȡ��Ʒ  -1.������*/
			if (0 == nGiftStatus)
			{
				// �Ʒ��ȡ�ɹ�:(1)��ʼ��һ����ĵ���ʱ
				pOnLineRewardUI->giftGetSuc(nNumGift, 3);

				RewardUi::removeRewardListEvent(REWARD_LIST_ID_ONLINEREWARD);
			}
			else if(-1 == nGiftStatus)
			{
				// ��Ʒ��ȡʧ�:(1)ܵ�����ʾ��Ϣ��2�����µ�ǰ��������ʱ�䣬������
				const char* str1 = StringDataManager::getString("label_bagisfull");
				char* p1 = const_cast<char*>(str1);
				GameView::getInstance()->showAlertDialog(p1);
			}
		}
		
	}

}