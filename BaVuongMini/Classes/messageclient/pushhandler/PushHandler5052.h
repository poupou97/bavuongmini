
#ifndef Blog_C___Reflection_PushHandler5052_h
#define Blog_C___Reflection_PushHandler5052_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5052 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5052)

public:
	SYNTHESIZE(PushHandler5052, int*, m_pValue)

		PushHandler5052() ;
	virtual ~PushHandler5052() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
