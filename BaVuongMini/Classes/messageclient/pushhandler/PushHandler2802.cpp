#include "PushHandler2802.h"
#include "../protobuf/QuestionMessage.pb.h"
#include "../../ui/Question_ui/QuestionData.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/Question_ui/QuestionUI.h"

#define  PAGE_NUM 10

IMPLEMENT_CLASS(PushHandler2802)

PushHandler2802::PushHandler2802() 
{

}
PushHandler2802::~PushHandler2802() 
{
	
}
void* PushHandler2802::createInstance()
{
	return new PushHandler2802() ;
}
void PushHandler2802::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2802::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2802::handle(CommonMessage* mb)
{
	Rsp2802 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	// ��QuestionData
	QuestionData::instance()->clearData();

	std::string strQuestionTitle = bean.question();							// ���Ŀ�����⣩
	int nMoney = bean.money();														// ��Ǯ
	int nExp = bean.exp();																// ���

	// 4��ѡ�
	int nSizeQuestionContent = bean.options_size();
	for (int i = 0; i < nSizeQuestionContent; i++)
	{
		std::string strContent = bean.options(i);
		QuestionData::instance()->m_vector_questionContent.push_back(strContent);
	}

	QuestionData::instance()->set_questionTitle(strQuestionTitle);
	QuestionData::instance()->set_money(nMoney);
	QuestionData::instance()->set_exp(nExp);

	// �֪ͨ ���QuestionUI���
	if (NULL != GameView::getInstance()->getMainUIScene())
	{
		QuestionUI* pQuestionUI = (QuestionUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
		if (NULL != pQuestionUI)
		{
			pQuestionUI->initQuestionInfoFromServer();
			pQuestionUI->initBaseInfoFromServer();
		}
	}
}