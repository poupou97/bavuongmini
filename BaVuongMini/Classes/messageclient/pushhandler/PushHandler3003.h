
#ifndef Blog_C___Reflection_PushHandler3003_h
#define Blog_C___Reflection_PushHandler3003_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"
#include "cocos2d.h"

USING_NS_CC;

class PushHandler3003 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler3003)

public:
	SYNTHESIZE(PushHandler3003, int*, m_pValue)

		PushHandler3003() ;
	virtual ~PushHandler3003() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
