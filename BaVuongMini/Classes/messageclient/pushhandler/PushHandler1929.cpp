#include "PushHandler1929.h"

#include "../protobuf/CopyMessage.pb.h"
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../ui/FivePersonInstance/InstanceEndUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../element/CFivePersonInstanceEndInfo.h"
#include "../element/CRewardProp.h"
#include "../../ui/FivePersonInstance/InstanceRewardUI.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/GoodsInfo.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerOwnedCommand.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"

IMPLEMENT_CLASS(PushHandler1929)

PushHandler1929::PushHandler1929() 
{

}
PushHandler1929::~PushHandler1929() 
{

}
void* PushHandler1929::createInstance()
{
	return new PushHandler1929() ;
}
void PushHandler1929::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1929::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1929::handle(CommonMessage* mb)
{
	Rsp1929 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, instance is over", mb->cmdid());

	if (bean.monsters_size() <= 0)
		return;

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	int shortestDistance = -1;
	long long selectedRoleId = NULL_ROLE_ID;
	int pos_x =  0;
	int pos_y =  0;

	Vec2 myPosition = GameView::getInstance()->myplayer->getWorldPosition();

	for (int i = 0;i<bean.monsters_size();++i)
	{
		int currentDistance = myPosition.getDistance(Vec2(bean.monsters(i).x()*1.0f,bean.monsters(i).y()*1.0f));
		if(shortestDistance == -1)   // first one
		{
			shortestDistance = currentDistance;
			pos_x = bean.monsters(i).x();
			pos_y = bean.monsters(i).y();
			selectedRoleId = bean.monsters(i).monsterid();
		}
		else
		{
			if(shortestDistance > currentDistance)   // compare distance
			{
				shortestDistance = currentDistance;
				pos_x = bean.monsters(i).x();
				pos_y = bean.monsters(i).y();
				selectedRoleId = bean.monsters(i).monsterid();
			}
		}
	}

	if(selectedRoleId != NULL_ROLE_ID)
	{
		//same map
		Vec2 cp_target = Vec2(pos_x*1.0f,pos_y*1.0f);
		if (cp_target.x == 0 && cp_target.y == 0)
		{
			return;
		}

		MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
		// NOT reachable, so find the near one
		short tileX = GameView::getInstance()->getGameScene()->positionToTileX(cp_target.x);
		short tileY = GameView::getInstance()->getGameScene()->positionToTileY(cp_target.y);
		cmd->targetPosition = cp_target;
		cmd->method = MyPlayerCommandMove::method_searchpath;
		bool bSwitchCommandImmediately = true;
		if(GameView::getInstance()->myplayer->isAttacking())
			bSwitchCommandImmediately = false;
		GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	

		MainScene* mainScene = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
		if (!mainScene)
			return;

		if (FivePersonInstance::getStatus() != FivePersonInstance::status_in_instance)
			return;

		MissionAndTeam * missionAndTeam = (MissionAndTeam*)mainScene->getChildByTag(kTagMissionAndTeam);
		if (missionAndTeam)
		{
			if (missionAndTeam->getPanelType() == MissionAndTeam::type_FiveInstanceAndTeam)
			{
				missionAndTeam->DoThingWhenFinishedMove(cp_target);
			}
		}
	}
}