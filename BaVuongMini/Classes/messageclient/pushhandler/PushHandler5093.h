
#ifndef Blog_C___Reflection_PushHandler5093_h
#define Blog_C___Reflection_PushHandler5093_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5093 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5093)

public:
	SYNTHESIZE(PushHandler5093, int*, m_pValue)

	PushHandler5093() ;
	virtual ~PushHandler5093() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
