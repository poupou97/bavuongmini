
#ifndef Blog_C___Reflection_PushHandler1407_h
#define Blog_C___Reflection_PushHandler1407_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1407 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1407)
    
public:
    SYNTHESIZE(PushHandler1407, int*, m_pValue)
    
    PushHandler1407() ;
    virtual ~PushHandler1407() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
