#include "PushHandler5148.h"

#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../protobuf/DailyGiftSign.pb.h"
#include "../../ui/signMonthly_ui/SignMonthlyData.h"
#include "../element/COneMouthSignGift.h"
#include "../element/FolderInfo.h"

IMPLEMENT_CLASS(PushHandler5148)

PushHandler5148::PushHandler5148() 
{

}
PushHandler5148::~PushHandler5148() 
{

}
void* PushHandler5148::createInstance()
{
	return new PushHandler5148() ;
}
void PushHandler5148::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5148::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5148::handle(CommonMessage* mb)
{
	Push5148 bean;
	bean.ParseFromString(mb->data());

	if (!GameView::getInstance())
		return;
	
	//update data
	SignMonthlyData::getInstance()->clearSignGiftData();
	for(int i = 0 ;i < bean.goods_size();++i)
	{
		COneMouthSignGift * tempSignGift= new COneMouthSignGift();
		tempSignGift->CopyFrom(bean.goods(i));
		SignMonthlyData::getInstance()->m_oneMonthSighGiftData.push_back(tempSignGift);
	}

	//refresh iscanGetGift
	bool isExist = false;
	for (int i = 0;i<SignMonthlyData::getInstance()->m_oneMonthSighGiftData.size();i++)
	{
		if (SignMonthlyData::getInstance()->m_oneMonthSighGiftData.at(i)->status() == 2)
		{
			isExist = true;
			break;
		}
	}
	if (isExist)
	{
		SignMonthlyData::getInstance()->m_bCanGetGift = true;
	}
	else
	{
		SignMonthlyData::getInstance()->m_bCanGetGift = false;
	}

	SignMonthlyData::getInstance()->m_nCurMonth = bean.mouth();
	SignMonthlyData::getInstance()->m_nSignedNum = bean.loadnumber();
	SignMonthlyData::getInstance()->m_strGeneralId = bean.generalid();
}

