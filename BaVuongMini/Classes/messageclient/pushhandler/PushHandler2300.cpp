#include "PushHandler2300.h"
#include "../protobuf/DailyMessage.pb.h"
#include "../element/CRoleMessage.h"
#include "../../ui/Rank_ui/RankData.h"
#include "../../ui/Rank_ui/RankDb.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../ui/Rank_ui/RankUI.h"
#include "../../ui/Rank_ui/RankDataTableView.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/role/MyPlayer.h"

#define  PAGE_NUM 10

IMPLEMENT_CLASS(PushHandler2300)

PushHandler2300::PushHandler2300() 
{

}
PushHandler2300::~PushHandler2300() 
{
	
}
void* PushHandler2300::createInstance()
{
	return new PushHandler2300() ;
}
void PushHandler2300::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler2300::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler2300::handle(CommonMessage* mb)
{
	Rsp2300 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	//������ 0�ȼ���1ս�����2����3�ս����4���, 5��ʻ�
	int nType = bean.type();

	//ְҵ 0.ȫ����1.�ͽ���2.��ı��3.��ܡ�4.���䡢
	//�� 0.ҹ�����1.κ��2.�3.��⡢
	//�ʻ� 1.�����ջ���2.�����ͻ���3.��ʷ�ջ���4.��ʷ�ͻ�
	int nWork = bean.work();

	//�ҵ����
	int nMyPlayerRank = bean.index();

	//��û�id�name
	RoleBase* base = GameView::getInstance()->myplayer->getActiveRole()->mutable_rolebase();
	long long longMyPlayerId = base->roleid();
	std::string str_myPlayerName = base->name();

	 //͸��µ�ʱ�(�ʱ��)----������ص�� ǻ���೤ʱ� ������ݣ�����ʱ ��� �������� + millisecondNow;
	long long longTimeBean = bean.time();

	// ݷ����������ʱ����������ĺͱ��صĶԱȣ���һ��ʱ���ʧЧ��ɾ��������ݣ��������κδ�����
	long long longTimeRefreshBean = bean.timerefresh();

	if (-1 == RankData::instance()->get_timeFlag())
	{
		long long longTimeFinal = longTimeBean + GameUtils::millisecondNow();
		RankData::instance()->set_timeFlag(longTimeFinal);
	}
	else
	{
		// ʱ�� �� ����ڵ�ʱ��Աȡ���timeFlag ��� milliscondNow()ڣ���1�������ݿ⣨2�������û���ǰҳ��3��return
		if (GameUtils::millisecondNow() >= RankData::instance()->get_timeFlag())
		{
			  //GameView::getInstance()->showAlertDialog("push2300 timeFlag equals");

			  // ʱ�����
			  RankDb::instance()->timeIsOut();

			  return;
		}
	}

	if (-1 == RankData::instance()->get_timeRefreshFlag())
	{
		RankData::instance()->set_timeRefreshFlag(longTimeRefreshBean);
	}
	else
	{
		// �ʱ�� �� ����ڵ timeRefresh�ʱ��Աȡ�����ȣ�� �˵��ʱ���ʧЧ
		if (longTimeRefreshBean != RankData::instance()->get_timeRefreshFlag())
		{
			// ʱ�����
			RankDb::instance()->timeIsOut();

			return;
		}
	}

	// µ�ǰҳ � ���ҳ�
	int nPageCur = bean.pagenumber();
	int nPageAll = bean.pageall();

	// ��� µ�ǰҳ � ���ҳ� �״̬
	RankDataTableView::instance()->refreshDataTableStatus(nPageCur, nPageAll);

	// �����ʻ�type
	int nFlowerType = 0;
	if (5 == nType)
	{
		nFlowerType = nWork;
	}

	// ��RankData��m_vector_player_internetе���
	RankData::instance()->clearVectorPlayerInternet();

	// roleMessage���ӵ�vector
	int nSizeRoleMessage = bean.roles_size();
	for (int i = 0; i < nSizeRoleMessage; i++)
	{
		CRoleMessage * roleMesage = new CRoleMessage();
		roleMesage->CopyFrom(bean.roles(i));
		roleMesage->set_rankType(nType);
		roleMesage->set_timeFlag(RankData::instance()->get_timeFlag());															// �����ʱ������Ҫ���㣩
		roleMesage->set_pageCur(nPageCur);
		roleMesage->set_pageAll(nPageAll);
		roleMesage->set_myPlayerRank(nMyPlayerRank);
		roleMesage->set_myPlayerId(longMyPlayerId);
		roleMesage->set_timeRefreshFlag(RankData::instance()->get_timeRefreshFlag());
		roleMesage->set_myPlayerName(str_myPlayerName);
		roleMesage->set_flowerType(nFlowerType);
		RankData::instance()->m_vector_player_internet.push_back(roleMesage);
	}

	// tableName
	std::string strTableName = "";
	if (0 == nWork && 3 != nType)
	{
		strTableName = "t_rank_player_all";
	}
	else if (0 == nWork && 3 == nType)
	{
		strTableName = "t_rank_country_all";
	}
	else
	{
		strTableName = "t_rank_player";
	}

	// �db���ȡ��ݵ� vector(��ʱ�vector)
	std::vector<CRoleMessage *> tmpVector;
	RankDb::instance()->getDataFromDb(tmpVector, nType, nWork);
	int nSizeVector = tmpVector.size();
	int nPageCurDb = nSizeVector / PAGE_NUM;


	// ĵ� ����� �������ݵ ĵ�ǰҳ ��� db��е�ҳ��ʱ���Ż���db
	if (nPageCurDb - 1< nPageCur)
	{
		// 뽫��ݲ�����ݿ
		RankDb::instance()->insertData(strTableName, RankData::instance()->m_vector_player_internet);
	}
	else
	{
		// ��� µ�ǰҳ � ���ҳ� �״̬
		RankDataTableView::instance()->refreshDataTableStatus(nPageCurDb - 1, nPageAll);
	}

	// �db���ȡ��ݵ� vector(m_vector_player_db)
	RankDb::instance()->getDataFromDb(RankData::instance()->m_vector_player_db, nType, nWork);

	// ֪ͨ ���UI���
	if (NULL != GameView::getInstance()->getMainUIScene())
	{
		RankUI* pRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
		if (NULL != pRankUI)
		{
			pRankUI->reloadDataTableView();
		}
	}


	// ������ʱvector
	std::vector<CRoleMessage *>::iterator iterTmpVector;
	for (iterTmpVector = tmpVector.begin(); iterTmpVector != tmpVector.end(); iterTmpVector++)
	{
		delete *iterTmpVector;
	}
	tmpVector.clear();
}