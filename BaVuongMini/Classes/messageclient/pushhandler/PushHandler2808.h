
#ifndef Blog_C___CheckOtherEquip_PushHandler2808_h
#define Blog_C___CheckOtherEquip_PushHandler2808_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler2808 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler2808)

public:
	SYNTHESIZE(PushHandler2808, int*, m_pValue)

		PushHandler2808() ;
	virtual ~PushHandler2808() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
