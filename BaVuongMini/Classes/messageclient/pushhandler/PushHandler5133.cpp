#include "PushHandler5133.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../protobuf/DailyGift.pb.h"
#include "../element/CPlayerGetPhypower.h"
#include "../../ui/Friend_ui/FriendGetPhyPower.h"
#include "../../ui/GameUIConstant.h"
#include "../element/CRewardBase.h"
#include "../../ui/Reward_ui/RewardUi.h"

IMPLEMENT_CLASS(PushHandler5133)

PushHandler5133::PushHandler5133() 
{

}
PushHandler5133::~PushHandler5133() 
{
	
}
void* PushHandler5133::createInstance()
{
	return new PushHandler5133() ;
}
void PushHandler5133::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5133::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5133::handle(CommonMessage* mb)
{
	Rsp5133 bean;
	bean.ParseFromString(mb->data());
	
	if (bean.status() == 1)
	{
		int temp_value = 0;
		for (int i =0;i<GameView::getInstance()->playerPhypowervector.size();i++)
		{
			if (bean.playerid() == GameView::getInstance()->playerPhypowervector.at(i)->id())
			{
				temp_value =  GameView::getInstance()->playerPhypowervector.at(i)->value();

				std::vector<CPlayerGetPhypower *>::iterator iter = GameView::getInstance()->playerPhypowervector.begin()+i;
				CPlayerGetPhypower * temp_phyPower = *iter;
				GameView::getInstance()->playerPhypowervector.erase(iter);
				delete temp_phyPower;
			}
		}

		FriendGetPhyPower * phypower_ui = (FriendGetPhyPower *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGetphyPowerUi);
		if (phypower_ui!= NULL)
		{
			phypower_ui->phypowerTavleView->reloadData();
			phypower_ui->m_getPhypowerCount -= temp_value;

			if (phypower_ui->m_getPhypowerCount <= 0)
			{
				phypower_ui->m_getPhypowerCount = 0;
			}
			char string_count[10];
			sprintf(string_count,"%d",phypower_ui->m_getPhypowerCount);
			phypower_ui->remPhyPowerCount_->setString(string_count);
		}

		if (temp_value > 0)
		{
			const char * str = StringDataManager::getString("remindgetPrizephysice_lingqutili");
			char string_[100];
			sprintf(string_,str,temp_value);
			GameView::getInstance()->showAlertDialog(string_);
		}

		if (GameView::getInstance()->playerPhypowervector.size() <= 0)
		{
			RewardUi::removeRewardListEvent(REWARD_LIST_ID_PHYSICAL_FRIEND);
			MainScene * mainscen_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscen_->remindGetFriendPhypower();
			if (phypower_ui!= NULL)
			{
				phypower_ui->setShowPhypowerlabel(true);
			}
		}

		if (phypower_ui!= NULL)
		{
			if (phypower_ui->m_getPhypowerCount <= 0)
			{
				RewardUi::removeRewardListEvent(REWARD_LIST_ID_PHYSICAL_FRIEND);
				MainScene * mainscen_ = (MainScene *)GameView::getInstance()->getMainUIScene();
				mainscen_->remindGetFriendPhypower();
			}
		}
	}
}