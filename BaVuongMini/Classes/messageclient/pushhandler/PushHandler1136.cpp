#include "PushHandler1136.h"

#include "../protobuf/NPCMessage.pb.h"

#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/BaseFighter.h"
#include "../element/CSalableCommodity.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/shop_ui/ShopUI.h"
#include "../../ui/extensions/UITab.h"

IMPLEMENT_CLASS(PushHandler1136)

	PushHandler1136::PushHandler1136() 
{

}
PushHandler1136::~PushHandler1136() 
{

}
void* PushHandler1136::createInstance()
{
	return new PushHandler1136() ;
}
void PushHandler1136::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1136::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1136::handle(CommonMessage* mb)
{
	Push1136 bean;
	bean.ParseFromString(mb->data());

	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	int type = bean.source();
	if (type == 0)
	{
		int id = bean.commodity().id();
		int pageIndex  = 0 ;
		//change data
		bool isHave = false;
		for(int i =0;i<GameView::getInstance()->buyBackList.size();++i)
		{
			if (GameView::getInstance()->buyBackList.at(i)->id() == id)
			{
				if (bean.commodity().has_goods())
				{
					CSalableCommodity* temp = GameView::getInstance()->buyBackList.at(i);
					temp->CopyFrom(bean.commodity());
				}
				else
				{
					std::vector<CSalableCommodity * >::iterator iter = GameView::getInstance()->buyBackList.begin() + i;
					CSalableCommodity* pRemoved = *iter;
					GameView::getInstance()->buyBackList.erase(iter);
					delete pRemoved;			
				}
				/*CSalableCommodity* temp = GameView::getInstance()->buyBackList.at(i);
				temp->CopyFrom(bean.commodity());*/
// 				std::vector<CSalableCommodity * >::iterator iter = GameView::getInstance()->buyBackList.begin() + i;
// 				CSalableCommodity* pRemoved = *iter;
// 				GameView::getInstance()->buyBackList.erase(iter);
// 				delete pRemoved;			
// 				if (bean.commodity().has_goods())
// 				{
// 					CSalableCommodity * temp = new CSalableCommodity();
// 					temp->CopyFrom(bean.commodity());
// 					GameView::getInstance()->buyBackList.insert(GameView::getInstance()->buyBackList.begin()+i,temp);
//				}

				isHave = true;
			}
		}

		if (!isHave)
		{
 			if (bean.commodity().has_goods())
 			{
				CSalableCommodity * temp = new CSalableCommodity();
				temp->CopyFrom(bean.commodity());
				GameView::getInstance()->buyBackList.push_back(temp);
			}
		}

		//add by yangjun 2013.12.14
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShopUI))
		{
			ShopUI * shopUi = (ShopUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShopUI);
			if (shopUi->mainTab->getCurrentIndex() == 0)
			{
				//shopUi->ReloadDataWithOutChangeOffSet();
			}
			else
			{
				shopUi->ReloadData();
			}
		}
	}
	else if (type == 1)
	{

	}

}