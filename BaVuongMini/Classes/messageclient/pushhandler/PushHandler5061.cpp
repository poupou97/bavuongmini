#include "PushHandler5061.h"

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

#include "../../messageclient/protobuf/CollectMessage.pb.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../../messageclient/element/CPickingInfo.h"
#include "../../GameView.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/CPushCollectInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsRecuriteUI.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../messageclient/element/CActionDetail.h"
#include "../../ui/extensions/RecruitGeneralCard.h"

//#include "NotificationClass.h"

#else

#include "../protobuf/CollectMessage.pb.h"  
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/role/PickingActor.h"
#include "../element/CPickingInfo.h"
#include "../../GameView.h"
#include "../../legend_engine/GameWorld.h"
#include "../element/CPushCollectInfo.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../protobuf/RecruitMessage.pb.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsRecuriteUI.h"
#include "../element/CGeneralDetail.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../utils/GameUtils.h"
#include "../GameMessageProcessor.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../element/CActionDetail.h"
#include "../../ui/extensions/RecruitGeneralCard.h"

#endif

IMPLEMENT_CLASS(PushHandler5061)

PushHandler5061::PushHandler5061() 
{

}
PushHandler5061::~PushHandler5061() 
{

}
void* PushHandler5061::createInstance()
{
	return new PushHandler5061() ;
}
void PushHandler5061::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5061::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5061::handle(CommonMessage* mb)
{
	ResRecruit5061 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	ActionType actionType = bean.type();
	long long cdTime = bean.cdtime();
	int playerRemianNumber = bean.playerremiannumber();

	for(int i = 0;i<GameView::getInstance()->actionDetailList.size();++i)
	{
		CActionDetail * temp = GameView::getInstance()->actionDetailList.at(i);
		if (actionType == temp->type())
		{
			if (actionType == BASE)
			{
				temp->set_playerremainnumber(playerRemianNumber);
			}

			if (cdTime > 0)
			{
				temp->set_cdtime(cdTime);
			}	
		}
	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	if (scene->getMainUIScene()->getChildByTag(kTagGeneralsUI) != NULL)
	{
		GeneralsUI * generalui = (GeneralsUI *)scene->getMainUIScene()->getChildByTag(kTagGeneralsUI);
		if(actionType == TEN_TIMES)
		{
			std::vector<CGeneralDetail * >tempVector ;
			for (int i = 0;i<bean.generals_size();++i)
			{
				CGeneralDetail * tempGD = new CGeneralDetail();
				tempGD->CopyFrom(bean.generals(i));
				tempVector.push_back(tempGD);
			}
			//��ʾʮ���Ľ����
			GeneralsUI::generalsRecuriteUI->presentTenTimesRecruiteResult(tempVector);

			std::vector<CGeneralDetail*>::iterator iter_generalDetail;
			for (iter_generalDetail = tempVector.begin(); iter_generalDetail != tempVector.end(); ++iter_generalDetail)
			{
				delete *iter_generalDetail;
			}
			tempVector.clear();
		}
		else
		{
			if (bean.generals_size() == 1)
			{
				CGeneralDetail * tempData = new CGeneralDetail();
				tempData->CopyFrom(bean.generals(0));
				GeneralsUI::generalsRecuriteUI->presentRecruiteResult(tempData,bean.type(),RecruitGeneralCard::kTypeCommonRecuriteGeneral);
				delete tempData;
			}
			else
			{}
		}
		GeneralsUI::generalsListUI->isReqNewly = true;
		GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
		temp->page = 0;
		temp->pageSize = GeneralsUI::generalsListUI->everyPageNum;
		temp->type = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
		delete temp;
	}

#if CC_TARGET_PLATFORM == CC_PLATFORM_IOS

	// ��佫��ļ �������
	std::vector<CActionDetail*> actionDetailList = GameView::getInstance()->actionDetailList;
	int nSize = actionDetailList.size();
	if (4 == nSize)
	{
		for (int i = 0; i < nSize; i++)
		{
			CActionDetail * pActionDetail = actionDetailList.at(i);
			if (BASE == pActionDetail->type())
			{
				//[NotificationClass unregisterNotificationGeneral:1];
			}
			else if (BETTER == pActionDetail->type())
			{
				//[NotificationClass unregisterNotificationGeneral:2];
			}
			else if (BEST == pActionDetail->type())
			{
				//[NotificationClass unregisterNotificationGeneral:3];
			}
		}

		for (int i = 0; i < nSize; i++)
		{
			CActionDetail * pActionDetail = actionDetailList.at(i);
			if (BASE == pActionDetail->type())
			{
				//				long long longCDTime = pActionDetail->cdtime();
				//                int nMaxFreeGetCard = pActionDetail->playerremainnumber();
				//                
				//                if (nMaxFreeGetCard > 1)
				//                {
				//                    [NotificationClass pushNotificationGeneral:1 andCDTime:longCDTime];
				//                }
				long long longCDTime = pActionDetail->cdtime();
				//[NotificationClass pushNotificationGeneral:2 andCDTime:longCDTime];

			}
			else if (BETTER == pActionDetail->type())
			{
				long long longCDTime = pActionDetail->cdtime();
				//[NotificationClass pushNotificationGeneral:2 andCDTime:longCDTime];
			}
			else if (BEST == pActionDetail->type())
			{
				long long longCDTime = pActionDetail->cdtime();
				//[NotificationClass pushNotificationGeneral:3 andCDTime:longCDTime];
			}
		}
	}

#endif

}
