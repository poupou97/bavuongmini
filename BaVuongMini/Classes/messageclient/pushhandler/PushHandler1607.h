
#ifndef Blog_C___Reflection_PushHandler1607_h
#define Blog_C___Reflection_PushHandler1607_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;
class PushHandler1607 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler1607)
    
public:
    SYNTHESIZE(PushHandler1607, int*, m_pValue)
    
    PushHandler1607() ;
    virtual ~PushHandler1607() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
