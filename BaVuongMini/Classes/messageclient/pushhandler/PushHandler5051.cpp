
#include "PushHandler5051.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "GameView.h"
#include "../element/CGeneralBaseMsg.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../ui/generals_ui/GeneralsTeachUI.h"
#include "../../ui/generals_ui/GeneralsEvolutionUI.h"
#include "../../ui/generals_ui/generals_popup_ui/OnStrategiesGeneralsInfoUI.h"
#include "../../ui/generals_ui/GeneralsStrategiesUI.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../gamescene_state/role/General.h"
#include "../GameMessageProcessor.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/sceneelement/GeneralsInfoUI.h"


IMPLEMENT_CLASS(PushHandler5051)

	PushHandler5051::PushHandler5051() 
{

}
PushHandler5051::~PushHandler5051() 
{

}
void* PushHandler5051::createInstance()
{
	return new PushHandler5051() ;
}
void PushHandler5051::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5051::display() 
{
	cout << *getm_pValue() << endl ;
}

bool SortGeneralBaseMsg(CGeneralBaseMsg * temp1 , CGeneralBaseMsg * temp2)
{
// 	if (temp1->fightstatus() > temp2->fightstatus())      
// 	{
// 		return true;
// 	}
// 	else if (temp1->fightstatus() == temp2->fightstatus())  //����ս״̬��ͬ����ϡ�ж����
//	{
		if (temp1->rare() > temp2->rare())      
		{
			return true;
		}
		else if (temp1->rare() == temp2->rare())//����ϡ�ж���ͬ������ȼ����
		{
			if (temp1->evolution() > temp2->evolution())      
			{
				return true;
			}
			else if (temp1->evolution() == temp2->evolution())//�����ȼ���ͬ����Ʒ�ʵȼ����
			{
				if (temp1->currentquality() > temp2->currentquality())      
				{
					return true;
				}
				else if (temp1->currentquality() == temp2->currentquality())//����Ʒ�ʵȼ���ͬ����ID���
				{
// 					if (temp1->level() > temp2->level())      
// 					{
// 						return true;
// 					}
// 					else
// 					{
// 						return false;
// 					}
					return temp1->modelid() < temp2->modelid();
				}
			}
		}
//	}

	return false;
}

void PushHandler5051::handle(CommonMessage* mb)
{
	Resp5051 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (bean.type() == 1) //all
	{
		//��佫�б���
		if (MainScene::GeneralsScene)
		{
			if (GeneralsUI::generalsListUI->isReqNewly)
			{
				if (GameView::getInstance()->generalBaseMsgList.size()>0)
				{
					std::vector<CGeneralBaseMsg*>::iterator iter;
					for (iter = GameView::getInstance()->generalBaseMsgList.begin(); iter != GameView::getInstance()->generalBaseMsgList.end(); ++iter)
					{
						delete *iter;
					}
					GameView::getInstance()->generalBaseMsgList.clear();
				}

				for (int i = 0;i<bean.generals_size();++i)
				{
					CGeneralBaseMsg * temp = new CGeneralBaseMsg();
					temp->CopyFrom(bean.generals(i));
					GameView::getInstance()->generalBaseMsgList.push_back(temp);
				}
			}
			else
			{
				for (int i = 0;i<bean.generals_size();++i)
				{
					CGeneralBaseMsg * temp = new CGeneralBaseMsg();
					temp->CopyFrom(bean.generals(i));
					GameView::getInstance()->generalBaseMsgList.push_back(temp);
				}
			}
		}

		//ݱ�����
// 		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack))
// 		{
// 			PackageScene * packageScene = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
// 			for (int i = 0;i<bean.generals_size();++i)
// 			{
// 				CGeneralBaseMsg * temp = new CGeneralBaseMsg();
// 				temp->CopyFrom(bean.generals(i));
// 				packageScene->generalBaseMsgList.push_back(temp);
// 			}
// 		}

		//equipment
// 		if (GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI))
// 		{
// 			EquipMentUi * equipmentui = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
// 			for (int i = 0;i<bean.generals_size();++i)
// 			{
// 				CGeneralBaseMsg * temp = new CGeneralBaseMsg();
// 				temp->CopyFrom(bean.generals(i));
// 				equipmentui->generalList->generalBaseMsgList.push_back(temp);
// 			}
// 			equipmentui->generalList->generalList_tableView->reloadData();
// 		}
	}
	else if(bean.type() == 2)   //teach
	{
		if (GeneralsUI::generalsTeachUI->isReqNewly)
		{
			if (GeneralsUI::generalsTeachUI->generalsTeachList.size()>0)
			{
				std::vector<CGeneralBaseMsg*>::iterator iter;
				for (iter = GeneralsUI::generalsTeachUI->generalsTeachList.begin(); iter != GeneralsUI::generalsTeachUI->generalsTeachList.end(); ++iter)
				{
					delete *iter;
				}
				GeneralsUI::generalsTeachUI->generalsTeachList.clear();
			}
		}

		for (int i = 0;i<bean.generals_size();++i)
		{
			CGeneralBaseMsg * temp = new CGeneralBaseMsg();
			temp->CopyFrom(bean.generals(i));
			GeneralsUI::generalsTeachUI->generalsTeachList.push_back(temp);
		}
		
	}
	else if (bean.type() == 3)  //evolutiond
	{
		if (GeneralsUI::generalsEvolutionUI->isReqNewly)
		{
			if (GeneralsUI::generalsEvolutionUI->evolutionGeneralsList.size()>0)
			{
				std::vector<CGeneralBaseMsg*>::iterator iter;
				for (iter = GeneralsUI::generalsEvolutionUI->evolutionGeneralsList.begin(); iter != GeneralsUI::generalsEvolutionUI->evolutionGeneralsList.end(); ++iter)
				{
					delete *iter;
				}
				GeneralsUI::generalsEvolutionUI->evolutionGeneralsList.clear();
			}
		}

		for (int i = 0;i<bean.generals_size();++i)
		{
			CGeneralBaseMsg * temp = new CGeneralBaseMsg();
			temp->CopyFrom(bean.generals(i));
			GeneralsUI::generalsEvolutionUI->evolutionGeneralsList.push_back(temp);
		}
		
	}
	else if (bean.type() == 4)  //��󷨽��浯�����������佫�б���
	{
// 		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsListForFightWaysUI))
// 		{
// 			OnStrategiesGeneralsInfoUI * onStrategiesGeneralsInfoUI = (OnStrategiesGeneralsInfoUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsListForFightWaysUI);
// 			if (onStrategiesGeneralsInfoUI->isReqNewly)
// 			{
// 				if (onStrategiesGeneralsInfoUI->generalsListForFightWay.size()>0)
// 				{
// 					std::vector<CGeneralBaseMsg*>::iterator iter;
// 					for (iter = onStrategiesGeneralsInfoUI->generalsListForFightWay.begin(); iter != onStrategiesGeneralsInfoUI->generalsListForFightWay.end(); ++iter)
// 					{
// 						delete *iter;
// 					}
// 					onStrategiesGeneralsInfoUI->generalsListForFightWay.clear();
// 				}
// 			}
// 
// 			for (int i = 0;i<bean.generals_size();++i)
// 			{
// 				CGeneralBaseMsg * temp = new CGeneralBaseMsg();
// 				temp->CopyFrom(bean.generals(i));
// 				onStrategiesGeneralsInfoUI->generalsListForFightWay.push_back(temp);
// 			}
// 			
// 		}
	}
	else if (bean.type() == 5)  //�������佫�б
	{
		if (GameView::getInstance()->generalsInLineList.size()>0)
		{
			std::vector<CGeneralBaseMsg*>::iterator iter;
			for (iter = GameView::getInstance()->generalsInLineList.begin(); iter != GameView::getInstance()->generalsInLineList.end(); ++iter)
			{
				delete *iter;
			}
			GameView::getInstance()->generalsInLineList.clear();
		}

		for (int i = 0;i<bean.generals_size();++i)
		{
			CGeneralBaseMsg * temp = new CGeneralBaseMsg();
			temp->CopyFrom(bean.generals(i));
			GameView::getInstance()->generalsInLineList.push_back(temp);
			temp->startCD();

			//�����˿̵�ʱ�
			temp->set_changeMoment(GameUtils::millisecondNow());

			GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)bean.generals(i).id());
		}

		//sort data
		if (GameView::getInstance()->generalsInLineList.size()>=2)
		{
			sort(GameView::getInstance()->generalsInLineList.begin(),GameView::getInstance()->generalsInLineList.end(),SortGeneralBaseMsg);
		}

		GameSceneLayer* scene = GameView::getInstance()->getGameScene();
		if(scene == NULL)
			return;

		//add by yangjun 2014.3.12
		MainScene * mainScene = GameView::getInstance()->getMainUIScene();
		//change by yangjun 2014.9.26
// 		if (mainScene != NULL)
// 			mainScene->RefreshGeneral();

		//add by yangjun 2014.9.26
		if (mainScene != NULL)
		{
			GeneralsInfoUI * generalsUI = (GeneralsInfoUI*)mainScene->getGeneralInfoUI();
			if (generalsUI)
			{
				generalsUI->RefreshAllGeneralItem();
			}
		}
	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	if (bean.type() == 5)
	{
		MainScene * mainScene = GameView::getInstance()->getMainUIScene();
		if (mainScene != NULL)
		{
			std::vector<CGeneralBaseMsg *>& generalsList = GameView::getInstance()->generalsInLineList;
			for (int i = 0;i<generalsList.size();++i)
			{
				if(generalsList.at(i)->fightstatus() == 2)
				{
					General * general = dynamic_cast<General *>(scene->getActor(generalsList.at(i)->id()));
					if (general)
					{
						//change by yangjun 2014.9.26
						//mainScene->RefreshGeneralInfo(general);

						//�ˢ�³����е��佫ͷ���Rank
						general->addRank(generalsList.at(i)->evolution());
						//�ˢ��������佫ͷ�������������ɫ
// 						if(mainScene->generalHeadLayer != NULL)
// 						{
// 							if (mainScene->generalHeadLayer->getChildByTag(66))
// 							{
// 								GeneralsHeadManager * generalsHeadManeger = (GeneralsHeadManager *)mainScene->generalHeadLayer->getChildByTag(66);
// 								if (generalsHeadManeger)
// 								{
// 									generalsHeadManeger->RefreshGeneralNameColor(generalsList.at(i)->id(),GameView::getInstance()->getGeneralsColorByQuality(generalsList.at(i)->currentquality()));
// 								}
// 							}
// 						}
					}
				}
			}
		}
			
			
	}

	//������
	if (scene->getMainUIScene()->getChildByTag(kTagBackpack))
	{
		PackageScene * packageScene = (PackageScene *)scene->getMainUIScene()->getChildByTag(kTagBackpack);
		packageScene->RefreshGeneralsListStatus(bean.page(),bean.pagecount());
		packageScene->ReloadGeneralsTableViewWithoutChangeOffSet();
	}
	//��佫���
	if (scene->getMainUIScene()->getChildByTag(kTagGeneralsUI))
	{
		if (bean.type() == 1)
		{
			//����õ�ǰ�б��״̬���Ƿ�����һҳ����һҳ��
			GeneralsUI::generalsListUI->RefreshGeneralsListStatus(bean.page(),bean.pagecount());
			if (GeneralsUI::generalsListUI->isVisible())
			{
				if(bean.page() == 0)
				{
					if (GeneralsUI::generalsListUI->isInFirstGeneralTutorial)
					{
						if (GameView::getInstance()->generalBaseMsgList.size()>0)
						{
							GeneralsUI::generalsListUI->curGeneralBaseMsg->CopyFrom(*(GameView::getInstance()->generalBaseMsgList.at(0)));
							GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void *)GeneralsUI::generalsListUI->curGeneralBaseMsg->id());
							GeneralsUI::generalsListUI->selectCellId = 0;
							GeneralsUI::generalsListUI->lastSelectCellId = 0;
						}

						GeneralsUI::generalsListUI->isInFirstGeneralTutorial = false;
					}

					//ˢ�TabelViwe(��佫�б���)
					GeneralsUI::generalsListUI->RefreshGeneralsList();
				}
				else
				{
					//�ˢ�TabelViwe(��佫�б���)
					GeneralsUI::generalsListUI->RefreshGeneralsListWithOutChangeOffSet();
				}
			}

			GeneralsUI::generalsListUI->setIsCanReq(true);
		}
		else if (bean.type() == 2)
		{
			//����õ�ǰ�б��״̬���Ƿ�����һҳ����һҳ��
			GeneralsUI::generalsTeachUI->RefreshGeneralsListStatus(bean.page(),bean.pagecount());
			if(bean.page() == 0)
			{
				//ˢ�TabelViwe(��佫�б���)
				GeneralsUI::generalsTeachUI->RefreshGeneralsList();
			}
			else
			{
				//�ˢ�TabelViwe(��佫�б���)
				GeneralsUI::generalsTeachUI->RefreshGeneralsListWithOutChangeOffSet();
			}

			GeneralsUI::generalsTeachUI->setIsCanReq(true);
		}
		else if (bean.type() == 3)
		{
			//����õ�ǰ�б��״̬���Ƿ�����һҳ����һҳ��
			GeneralsUI::generalsEvolutionUI->RefreshGeneralsListStatus(bean.page(),bean.pagecount());
			if(bean.page() == 0)
			{
				//ˢ�TabelViwe(��佫�б���)
				GeneralsUI::generalsEvolutionUI->RefreshGeneralsList();
			}
			else
			{
				//�ˢ�TabelViwe(��佫�б���)
				GeneralsUI::generalsEvolutionUI->RefreshGeneralsListWithOutChangeOffSet();
			}

			GeneralsUI::generalsEvolutionUI->setIsCanReq(true);
		}
		else if (bean.type() == 4)
		{
// 			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsListForFightWaysUI))
// 			{
// 				OnStrategiesGeneralsInfoUI * onStrategiesGeneralsInfoUI = (OnStrategiesGeneralsInfoUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsListForFightWaysUI);
// 				//����õ�ǰ�б��״̬���Ƿ�����һҳ����һҳ��
// 				onStrategiesGeneralsInfoUI->RefreshGeneralsListStatus(bean.page(),bean.pagecount());
// 				if(bean.page() == 0)
// 				{
// 					//ˢ�TabelViwe(��佫�б���)
// 					onStrategiesGeneralsInfoUI->RefreshGeneralsList();
// 				}
// 				else
// 				{
// 					//�ˢ�TabelViwe(��佫�б���)
// 					onStrategiesGeneralsInfoUI->RefreshGeneralsListWithOutChangeOffSet();
// 				}
// 			}
		}
		else if (bean.type() == 5)
		{
			GeneralsUI::generalsStrategiesUI->RefreshAllGeneralsInLine();
		}
	}
}
