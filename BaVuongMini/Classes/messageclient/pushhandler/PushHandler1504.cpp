
#include "PushHandler1504.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/family_ui/ManageFamily.h"
#include "../../gamescene_state/MainScene.h"
#include "../element/CGuildMemberBase.h"
#include "../element/CGuildBase.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/family_ui/FamilyAlms.h"
#include "../../ui/extensions/CCRichLabel.h"



IMPLEMENT_CLASS(PushHandler1504)

PushHandler1504::PushHandler1504() 
{
    
}
PushHandler1504::~PushHandler1504() 
{
    
}
void* PushHandler1504::createInstance()
{
    return new PushHandler1504() ;
}
void PushHandler1504::registProperty() 
{
    m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1504::display() 
{
    cout << *getm_pValue() << endl ;
}

void PushHandler1504::handle(CommonMessage* mb)
{
	Push1504 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());
	
	GameView::getInstance()->myplayer->getActiveRole()->mutable_playerbaseinfo()->set_factionid(bean.guildbase().id());
	GameView::getInstance()->myplayer->getActiveRole()->mutable_playerbaseinfo()->set_factionposition(bean.post());
	GameView::getInstance()->myplayer->getActiveRole()->mutable_playerbaseinfo()->set_factionname(bean.guildbase().name());
	GameView::getInstance()->myplayer->addFamilyName(bean.guildbase().name().c_str(), bean.post());
	FamilyUI * familyui =(FamilyUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyUI);
	if (familyui ==NULL)
	{
		return;
	}
	//family name
	std::string familyName_ =bean.guildbase().name();
	familyui->label_familyName->setString(familyName_.c_str());
	//family level
	familyui->familyLevel = bean.guildbase().level();
	char levelStr[10];
	sprintf(levelStr,"%d",familyui->familyLevel);
	familyui->label_familyLevel->setString(levelStr);
	//family member
	familyui->curFamilyMemberNum = bean.guildbase().currmembernumber();
	familyui->maxFamilyMemberNum = bean.guildbase().maxmembernumber();

	char curMemberStr[5];
	sprintf(curMemberStr,"%d",familyui->curFamilyMemberNum);
	char maxMemberStr[5];
	sprintf(maxMemberStr,"%d",familyui->maxFamilyMemberNum);
	std::string member_ =curMemberStr;
	member_.append("/");
	member_.append(maxMemberStr);
	familyui->label_familyNumber->setString(member_.c_str());
	/////family contribution
	familyui->curFamilyContrbution = bean.guildbase().donate();
	familyui->nextFamilyContrbution = bean.updatedonate();
	std::string contrbutionStr_ = "";
	if (bean.updatedonate() > 0)
	{
		char curContributonStr[20];
		sprintf(curContributonStr,"%ld",familyui->curFamilyContrbution);
		char nectContributonStr[20];
		sprintf(nectContributonStr,"%ld",familyui->nextFamilyContrbution);

		contrbutionStr_.append(curContributonStr);
		contrbutionStr_.append("/");
		contrbutionStr_.append(nectContributonStr);
	}else
	{
		const char * familyContrbution_  = StringDataManager::getString("familyLveleIsFull");
		contrbutionStr_.append(familyContrbution_);
	}
	familyui->label_familyContribution->setString(contrbutionStr_.c_str());
	//person all contribution 
	long long allContribution_ = bean.memberbase().guilddonate();
	char personStrContrbution[30];
	sprintf(personStrContrbution,"%lld",allContribution_);
	int size_ = strlen(personStrContrbution);
// 	if (size_ >11)
// 	{
// 		sprintf(personStrContrbution,"%lld",99999999999);
// 	}
// 	familyui->label_myAllContribution->setText(personStrContrbution);
	//person rem contribution 
	familyui->remPersonContrbution = bean.memberbase().guildremaindonate();
	char personStrRemContrbution[20];
	sprintf(personStrRemContrbution,"%lld",familyui->remPersonContrbution);	
	int sizeRemContrbution_ = strlen(personStrRemContrbution);
	if (sizeRemContrbution_ >11)
	{
		sprintf(personStrRemContrbution,"%lld",99999999999);
	}
	familyui->label_myRemContribution->setString(personStrRemContrbution);
	
	//family btn_repairManifesto
	std::string manifesto = bean.guildbase().declaration();
	if (strlen(manifesto.c_str()) > 0)
	{
		familyui->familyManifestoLabel->setString(manifesto.c_str());
	}else
	{
		const char * familyWriteIsEmpty  = StringDataManager::getString("familyMangerIsNotWrite");
		//GameView::getInstance()->showAlertDialog(familyWriteIsEmpty);
		familyui->familyManifestoLabel->setString(familyWriteIsEmpty);
	}
	int hh_ =familyui->familyManifestoLabel->getContentSize().height;
	if (hh_ < familyui->manifestoScrollView->getContentSize().height)
	{
		hh_ =familyui->manifestoScrollView->getContentSize().height;
	}
	familyui->manifestoScrollView->setContentSize(Size(270,hh_));
	familyui->familyManifestoLabel->setPosition(Vec2(0,familyui->manifestoScrollView->getContentSize().height - familyui->familyManifestoLabel->getContentSize().height-10));
	
	//family notice
	std::string notice_ = bean.announcement();
	if (strlen(notice_.c_str()) >0)
	{
		familyui->familyNoticeLabel->setString(notice_.c_str());
	}else
	{
		const char * familyWriteIsEmpty  = StringDataManager::getString("familyMangerIsNotWrite");
		familyui->familyNoticeLabel->setString(familyWriteIsEmpty);
	}
	
	int h_ =familyui->familyNoticeLabel->getContentSize().height;
	if (h_ < familyui->noticeScrollView->getContentSize().height)
	{
		h_ =familyui->noticeScrollView->getContentSize().height;
	}
	
	familyui->noticeScrollView->setContentSize(Size(270,h_));
	familyui->familyNoticeLabel->setPosition(Vec2(0,familyui->noticeScrollView->getContentSize().height - familyui->familyNoticeLabel->getContentSize().height-10));

	const char *strings;
	switch(bean.post())
	{
	case CGuildBase::position_master:
		{
			strings = StringDataManager::getString("family_member_Master");
			familyui->btn_mamage->setVisible(true);
		}break;
	case CGuildBase::position_second_master:
		{
			strings = StringDataManager::getString("family_member_SecondMaster");
			familyui->btn_mamage->setVisible(true);
		}break;
	case CGuildBase::position_member:
		{
			strings = StringDataManager::getString("family_member_member");
			familyui->btn_mamage->setVisible(false);
		}break;
	}
	familyui->label_myPost->setString(strings);
	familyui->selfPost_ = bean.post();

	if (familyui->selfPost_ != 1)
	{
		familyui->btn_repairNotice->setTouchEnabled(false);
		familyui->btn_repairManifesto->setTouchEnabled(false);
	}

	//btn manage
	if (bean.flag()==1)
	{
		//�����Ч
		//familyui->btn_mamage->
	}

	CGuildBase * guild_ =new CGuildBase();
	guild_->CopyFrom(bean.guildbase());
	familyui->vectorGuildbase.push_back(guild_);

	FamilyAlms * familyAlms_ =(FamilyAlms* )GameView::getInstance()->getMainUIScene()->getChildByTag(familyAlms);
	if (familyAlms_ != NULL)
	{
		familyAlms_->refreshInfo(false);
	}
}
