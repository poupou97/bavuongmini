
#include "PushHandler5056.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../GameView.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../element/CGeneralBaseMsg.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../ui/generals_ui/GeneralsStrategiesUI.h"
#include "../GameMessageProcessor.h"


IMPLEMENT_CLASS(PushHandler5056)

PushHandler5056::PushHandler5056() 
{

}
PushHandler5056::~PushHandler5056() 
{

}
void* PushHandler5056::createInstance()
{
	return new PushHandler5056() ;
}
void PushHandler5056::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5056::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5056::handle(CommonMessage* mb)
{
	Resp5056 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, modify one general's fight status", mb->cmdid());

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	long long generalsid = bean.generalid();
	int status = bean.fightstatus();

	if (bean.has_error())
	{
		GameView::getInstance()->showAlertDialog(bean.error().msg());
	}
	else
	{
		GeneralsUI::generalsListUI->isReqNewly = true;
		GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
		temp->page = 0;
		temp->pageSize = 20;
		temp->type = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
		delete temp;

		if (scene->getMainUIScene()->getChildByTag(kTagGeneralsUI) )
		{
			if (GeneralsUI::generalsStrategiesUI->isVisible() == true)
			{
				//ˢ���󷨽����е��佫
				GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
				temp->page = 0;
				temp->pageSize = 20;
				temp->type = 5;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
				delete temp;
			}
			else if (GeneralsUI::generalsListUI->isVisible() == true)
			{
				if (status == 2)
				{
					GeneralsUI::generalsListUI->addChatBubble("hello everyone ",3);
				}
			}
		}
	}
}
