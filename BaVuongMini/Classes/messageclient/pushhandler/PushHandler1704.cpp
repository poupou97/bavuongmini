
#include "PushHandler1704.h"

#include "../protobuf/EquipmentMessage.pb.h"  
#include "../element/CAdditionProperty.h"
#include "GameView.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../element/CEquipHole.h"
#include "../protobuf/AdditionStore.pb.h"
#include "../element/CLable.h"



IMPLEMENT_CLASS(PushHandler1704)

PushHandler1704::PushHandler1704() 
{

}
PushHandler1704::~PushHandler1704() 
{

}
void* PushHandler1704::createInstance()
{
	return new PushHandler1704() ;
}
void PushHandler1704::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1704::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1704::handle(CommonMessage* mb)
{
	ResGoldStoreCommodity1704 bean;
	bean.ParseFromString(mb->data());
	//CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (bean.isbindstore())
	{
		//delete old data
		GameView::getInstance()->bingGoldStoreList.erase(GameView::getInstance()->bingGoldStoreList.begin(),GameView::getInstance()->bingGoldStoreList.end());
		std::vector<CLable*>::iterator iter;
		for (iter = GameView::getInstance()->bingGoldStoreList.begin(); iter != GameView::getInstance()->bingGoldStoreList.end(); ++iter)
		{
			delete *iter;
		}
		GameView::getInstance()->bingGoldStoreList.clear();

		//add new data
		for (int i = 0;i<bean.lable_size();++i)
		{
			CLable * _Lable = new CLable();
			_Lable->CopyFrom(bean.lable(i));
			GameView::getInstance()->bingGoldStoreList.push_back(_Lable);
		}
	}
	else
	{
		//delete old data
		GameView::getInstance()->goldStoreList.erase(GameView::getInstance()->goldStoreList.begin(),GameView::getInstance()->goldStoreList.end());
		std::vector<CLable*>::iterator iter;
		for (iter = GameView::getInstance()->goldStoreList.begin(); iter != GameView::getInstance()->goldStoreList.end(); ++iter)
		{
			delete *iter;
		}
		GameView::getInstance()->goldStoreList.clear();

		//add new data
		for (int i = 0;i<bean.lable_size();++i)
		{
			CLable * _Lable = new CLable();
			_Lable->CopyFrom(bean.lable(i));
			GameView::getInstance()->goldStoreList.push_back(_Lable);

		}
	}

}
