
#ifndef Blog_C___Reflection_PushHandler0_h
#define Blog_C___Reflection_PushHandler0_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "../protobuf/MainMessage.pb.h"
#include "../protobuf/PlayerMessage.pb.h"

using namespace std;
//using namespace threekingdoms::protocol;

#include "cocos2d.h"
USING_NS_CC;
class PushHandler0 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler0)
    
public:
    SYNTHESIZE(PushHandler0, int*, m_pValue)
    
    PushHandler0() ;
    virtual ~PushHandler0() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;
	
} ;

#endif
