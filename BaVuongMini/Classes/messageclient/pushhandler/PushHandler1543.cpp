
#include "PushHandler1543.h"
#include "../protobuf/GuildMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightResultUI.h"

IMPLEMENT_CLASS(PushHandler1543)

	PushHandler1543::PushHandler1543() 
{

}
PushHandler1543::~PushHandler1543() 
{

}
void* PushHandler1543::createInstance()
{
	return new PushHandler1543() ;
}
void PushHandler1543::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler1543::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler1543::handle(CommonMessage* mb)
{
	Push1543 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

	if (!GameView::getInstance())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	if(GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFamilyFightResultUI) == NULL)
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		FamilyFightResultUI * resultUI = FamilyFightResultUI::create(bean);
		resultUI->setIgnoreAnchorPointForPosition(false);
		resultUI->setAnchorPoint(Vec2(0.5f,0.5f));
		resultUI->setPosition(Vec2(winSize.width/2,winSize.height/2-20));
		resultUI->setTag(kTagFamilyFightResultUI);
		GameView::getInstance()->getMainUIScene()->addChild(resultUI);
	}
}
