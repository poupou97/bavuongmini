
#ifndef Blog_C___Reflection_PushHandler5015_h
#define Blog_C___Reflection_PushHandler5015_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5015 : public CKBaseClass, public PushHandlerProtocol
{
private:
    DECLARE_CLASS(PushHandler5015)
    
public:
    SYNTHESIZE(PushHandler5015, int*, m_pValue)
    
    PushHandler5015() ;
    virtual ~PushHandler5015() ;
    static void* createInstance() ;
    virtual void registProperty() ;
    virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
    int *m_pValue ;

} ;

#endif
