
#include "PushHandler5053.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../protobuf/GeneralMessage.pb.h"
#include "../../GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../element/CShortCut.h"
#include "../../ui/generals_ui/generals_popup_ui/GeneralsSkillListUI.h"
#include "../../ui/generals_ui/GeneralsShortcutLayer.h"
#include "../../ui/generals_ui/generals_popup_ui/GeneralsSkillInfoUI.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../ui/extensions/UITab.h"
#include "../../ui/skillscene/GeneralMusouSkillListUI.h"
#include "../../ui/skillscene/MusouSkillScene.h"
#include "../../messageclient/element/CGeneralDetail.h"


IMPLEMENT_CLASS(PushHandler5053)

PushHandler5053::PushHandler5053() 
{

}
PushHandler5053::~PushHandler5053() 
{

}
void* PushHandler5053::createInstance()
{
	return new PushHandler5053() ;
}
void PushHandler5053::registProperty() 
{
	m_propertyMap.insert(pair<string, setValue>("setm_pValue", setm_pValue)) ;
}
void PushHandler5053::display() 
{
	cout << *getm_pValue() << endl ;
}

void PushHandler5053::handle(CommonMessage* mb)
{
	Rsp5053 bean;
	bean.ParseFromString(mb->data());
	CCLOG("msg: %d, %s", mb->cmdid(), mb->data().c_str());

// 	if (bean.wushouflag()== 1)       //  ���β�����������꼼�
// 	{
// 		//update inLineGeneral's shoruCut
// 		for(int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
// 		{
// 			if (bean.generalid() == GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
// 			{
// 				for(int j = 0;j<GameView::getInstance()->generalsInLineDetailList.at(i)->shortcuts_size();++j)
// 				{
// 					CShortCut * temp = new CShortCut();
// 					temp->CopyFrom(GameView::getInstance()->generalsInLineDetailList.at(i)->shortcuts(j));
// 					for(int m = 0;m<bean.shortcut_size();++m)
// 					{
// 						//GameView::getInstance()->generalsInLineDetailList.at(i)->mutable_shortcuts(j)->set_complexflag(0);
// 
// 						if (temp->index() == bean.shortcut(m).index())
// 						{
// 								GameView::getInstance()->generalsInLineDetailList.at(i)->mutable_shortcuts(j)->set_complexflag(bean.shortcut(m).complexflag());
// 						}
// 					}
// 					delete temp;
// 				}
// 				break;
// 			}
// 		}
// 	}
// 	else
// 	{
		//update inLineGeneral's shoruCut
		for(int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();i++)
		{
			CGeneralDetail * generalDetail = GameView::getInstance()->generalsInLineDetailList.at(i);
			if (bean.generalid() == generalDetail->generalid())
			{
				generalDetail->mutable_shortcuts()->Clear();

				for(int m = 0;m<bean.shortcut_size();++m)
				{
					ShortCut * shortCut = generalDetail->add_shortcuts();
					shortCut->set_index(bean.shortcut(m).index());
					shortCut->set_storedtype(bean.shortcut(m).storedtype());
					shortCut->set_skillpropid(bean.shortcut(m).skillpropid());
					shortCut->set_icon(bean.shortcut(m).icon());
					shortCut->set_name(bean.shortcut(m).name());
					shortCut->set_skilllevel(bean.shortcut(m).skilllevel());
					shortCut->set_complexflag(bean.shortcut(m).complexflag());
				}

				break;
			}
		}
//	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	if (scene->getMainUIScene()->getChildByTag(KTagSkillScene))
	{
		if (bean.has_error())
		{
			GameView::getInstance()->showAlertDialog(bean.error().msg());
		}
		else
		{
			SkillScene * skillScene = (SkillScene *)scene->getMainUIScene()->getChildByTag(KTagSkillScene);
			if (skillScene->SkillTypeTab->getCurrentIndex() == 1)
			{
				for (int i = 0 ;i<bean.shortcut_size();++i)
				{
					CShortCut * shortcut = new CShortCut();
					shortcut->CopyFrom(bean.shortcut(i));
					if (shortcut->complexflag() == 1)
					{
						skillScene->musouSkillLayer->generalMusouSkillListUI->AddMusouForGeneral(bean.generalid(),shortcut->index());
					}		
					delete shortcut;
				}
			}
		}
	}

	if (scene->getMainUIScene()->getChildByTag(kTagGeneralsUI))
	{
		if (bean.has_error())
		{
			GameView::getInstance()->showAlertDialog(bean.error().msg());
		}
		else
		{
			if (scene->getMainUIScene()->getChildByTag(kTagGeneralsSkillListUI))
			{
				GeneralsSkillListUI * generalsListUI = (GeneralsSkillListUI *)scene->getMainUIScene()->getChildByTag(kTagGeneralsSkillListUI);
				generalsListUI->closeAnim();
			}

			if (scene->getMainUIScene()->getChildByTag(kTagGeneralsSkillInfoUI))
			{
				GeneralsSkillInfoUI * generalsSkillInfoUI = (GeneralsSkillInfoUI *)scene->getMainUIScene()->getChildByTag(kTagGeneralsSkillInfoUI);
				generalsSkillInfoUI->closeAnim();
			}
			if (GeneralsUI::generalsListUI->isVisible())
			{
				if (bean.wushouflag()== 1)       //  ܱ��β�����������꼼�
				{
// 					for (int i = 0 ;i<bean.shortcut_size();++i)
// 					{
// 						if (bean.shortcut(i).complexflag() == 1)
// 						{
// 							GeneralsUI::generalsListUI->generalsShortcutLayer->AddMusouWithAnimation(bean.shortcut(i).index());
// 						}
// 					}
				}
				else
				{
					std::vector<CShortCut*>tempList;
					for (int i = 0 ;i<bean.shortcut_size();++i)
					{
						CShortCut * shortcut = new CShortCut();
						shortcut->CopyFrom(bean.shortcut(i));
						tempList.push_back(shortcut);
					}
					GeneralsUI::generalsListUI->RefreshAllGeneralsSkillSlot(bean.generalid(),tempList);
					std::vector<CShortCut*>::iterator iter;
					for (iter = tempList.begin(); iter != tempList.end(); ++iter)
					{
						delete *iter;
					}
					tempList.clear();
				}
			}
		}
	}
}
