
#ifndef Blog_C___Reflection_PushHandler5063_h
#define Blog_C___Reflection_PushHandler5063_h

#include "../../common/CKBaseClass.h"
#include "PushHandlerProtocol.h"

#include "cocos2d.h"
USING_NS_CC;

class PushHandler5063 : public CKBaseClass, public PushHandlerProtocol
{
private:
	DECLARE_CLASS(PushHandler5063)

public:
	SYNTHESIZE(PushHandler5063, int*, m_pValue)

		PushHandler5063() ;
	virtual ~PushHandler5063() ;
	static void* createInstance() ;
	virtual void registProperty() ;
	virtual void display() ;

	virtual void handle(CommonMessage* mb);

protected:
	int *m_pValue ;

} ;

#endif
