#include "CCTutorialParticle.h"
#include "../gamescene_state/SimpleEffectManager.h"


CCTutorialParticle::CCTutorialParticle()
{
}


CCTutorialParticle::~CCTutorialParticle()
{
}

CCTutorialParticle * CCTutorialParticle::create( const char* anmName,float controlX, float controlY )
{
	auto tutorialParticle = new CCTutorialParticle();
	if (tutorialParticle && tutorialParticle->init(anmName,controlX,controlY))
	{
		tutorialParticle->autorelease();
		return tutorialParticle;
	}
	CC_SAFE_DELETE(tutorialParticle);
	return NULL;
}

bool CCTutorialParticle::init( const char* anmName,float controlX, float controlY )
{
	if (Node::init())
	{
		std::string _path = "animation/texiao/particledesigner/";
		_path.append(anmName);
		//_path.append("tuowei0.plist");
		auto particleEffect = ParticleSystemQuad::create(_path.c_str());   //tuowei0.plist
		particleEffect->setPosition(Vec2(0,0));
		particleEffect->setPositionType(ParticleSystem::PositionType::FREE);
		//particleEffect->setLife(1.0f);
		//particleEffect->setLifeVar(0.3f);
		particleEffect->setVisible(true);
		this->addChild(particleEffect);

		auto path2 = SimpleEffectManager::RoundRectPathAction(controlX, controlY, 0 );
		particleEffect->runAction(path2);

		return true;
	}
	return false;
}

