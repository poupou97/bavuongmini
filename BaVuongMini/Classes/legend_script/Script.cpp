#include "Script.h"

#include "../utils/GameUtils.h"
#include "../utils/StrUtils.h"

int Script::s_newScriptId = 0;

Script::Script()
: mState(STATE_START)
, m_pHandler(NULL)
{
	id = s_newScriptId++;
}

Script::~Script()
{
}

void Script::set(string& cmd, string& para) {
	mCmd = cmd;
	mPara = StrUtils::split(para, ",");
	// 去空格
	for (unsigned int i = 0; i < mPara.size(); i++) {
		mPara[i] = StrUtils::trim(mPara[i]);
	}
}

void Script::setCommandHandler(Ref* owner) {
	m_pHandler = owner;
}
void Script::endCommand(Ref* pSender) {
	if(m_pHandler == pSender)
		setState(STATE_END);
}

void Script::setState(int state) {
	mState = state;
}

void Script::setScriptManager(ScriptManager* scriptManager) {
	mScriptManager = scriptManager;
}

bool Script::goEnd() {
	bool goEnd = false;
	long time = GameUtils::millisecondNow();
	if (time - mStartTime >= mDelayTime) {
		goEnd = true;
	}
	return goEnd;
}
