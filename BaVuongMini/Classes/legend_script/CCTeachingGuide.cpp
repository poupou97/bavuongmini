#include "CCTeachingGuide.h"
#include "CCTutorialIndicator.h"
#include "../ui/GameUIConstant.h"
#include "../gamescene_state/MainScene.h"
#include "GameView.h"
#include "ScriptManager.h"

#define kTagTutorialIndicator 15

CCTeachingGuide::CCTeachingGuide(void)
{
}

CCTeachingGuide::~CCTeachingGuide(void)
{
}

CCTeachingGuide * CCTeachingGuide::create( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction,int clipWidth,int clipHeight,bool isAddToMainScene /*= false*/,bool isDesFrameVisible /*= true*/ ,bool isHandFlip)
{
	auto teachingGuide = new CCTeachingGuide();
	if (teachingGuide && teachingGuide->init(content,pos,direction,clipWidth,clipHeight,isAddToMainScene,isDesFrameVisible,isHandFlip))
	{
		teachingGuide->autorelease();
		return teachingGuide;
	}
	CC_SAFE_DELETE(teachingGuide);
	return NULL;
}

bool CCTeachingGuide::init( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction,int clipWidth,int clipHeight,bool isAddToMainScene,bool isDesFrameVisible,bool isHandFlip )
{
	if (Layer::init())
	{
		m_nDelayTime = 0;
		m_bIsTimeUP = false;

		m_nClipWidth = clipWidth;
		m_nClipHeight = clipHeight;
		m_curDirection = direction;

		//test by yangjun 
 		Size visibleSize = Director::getInstance()->getVisibleSize();
 		//�����ü�ڵ
 		clip = ClippingNode::create();
 		clip->setInverted(true);
 		clip->setAlphaThreshold(1.0f);
 		addChild(clip);
 		//�Ϊ�ü�ڵ����һ���ɫ��͸��ĵװ
 		auto back = LayerColor::create(Color4B(0,0,0,0),visibleSize.width,visibleSize.height);
 		clip->addChild(back);
 		 
 		front = DrawNode::create();
 		Color4F yellow = {1,1,0,1};
 		Vec2 rect[4] = {Vec2(-m_nClipWidth/2,m_nClipHeight/2),Vec2(m_nClipWidth/2,m_nClipHeight/2),Vec2(m_nClipWidth/2,-m_nClipHeight/2),Vec2(-m_nClipWidth/2,-m_nClipHeight/2)};
 		front->drawPolygon(rect,4,yellow,0,yellow);
 		clip->setStencil(front);

		tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,isAddToMainScene,isDesFrameVisible,isHandFlip);
		//tutorialIndicator->setPosition(Vec2(pos.x-45,pos.y+95));
		tutorialIndicator->setTag(kTagTutorialIndicator);
		this->addChild(tutorialIndicator);
		tutorialIndicator->addCenterAnm(m_nClipWidth,m_nClipHeight);

		sp_yellowFrame = cocos2d::extension::Scale9Sprite::create("res_ui/yellow_frame.png");
		sp_yellowFrame->setCapInsets(Rect(5,5,1,1));
		sp_yellowFrame->setPreferredSize(Size(m_nClipWidth*6.0f,m_nClipHeight*6.0f));
		sp_yellowFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
		this->addChild(sp_yellowFrame);
		sp_yellowFrame->setVisible(false);

		playYellowFrameAnm();
		
		this->schedule(schedule_selector(CCTeachingGuide::updateTime),1.0f);

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(visibleSize);

		//this->setTouchPriority(TPriority_Tutorial);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(CCTeachingGuide::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(CCTeachingGuide::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(CCTeachingGuide::onTouchMoved, this);
		touchListener->onTouchCancelled = CC_CALLBACK_2(CCTeachingGuide::onTouchCancelled, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void CCTeachingGuide::setDrawNodePos( Vec2 pt )
{
	front->setPosition(pt);
	sp_yellowFrame->setPosition(pt);

	CCTutorialIndicator * temp = (CCTutorialIndicator*)this->getChildByTag(kTagTutorialIndicator);
	if (temp)
	{
		tutorialIndicator->setPosition(Vec2(pt.x,pt.y));
	}
}

void CCTeachingGuide::onEnter()
{
	Layer::onEnter();
}

void CCTeachingGuide::onExit()
{
	Layer::onExit();
}

bool CCTeachingGuide::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	if (!m_bIsTimeUP)
	{
		Size visibleSize=Director::getInstance()->getVisibleSize();

		Vec2 point=Director::getInstance()->convertToGL(pTouch->getLocationInView());
		Rect rect=Rect(front->getPositionX()-m_nClipWidth/2,front->getPositionY()-m_nClipHeight/2,m_nClipWidth,m_nClipHeight);
		if (rect.containsPoint(point))
		{
			return false;
		}

		playYellowFrameAnm();
		return true;
	}
	else
	{
		return false;
	}
}

void CCTeachingGuide::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void CCTeachingGuide::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void CCTeachingGuide::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

#define ktag_yellowFrame_Anm 69
void CCTeachingGuide::playYellowFrameAnm()
{
	if (sp_yellowFrame->getActionByTag(ktag_yellowFrame_Anm))
		sp_yellowFrame->stopActionByTag(ktag_yellowFrame_Anm);

	sp_yellowFrame->setScale(1.0f);

	Sequence * sequence = Sequence::create(
		Show::create(),
		ScaleTo::create(0.5f,1.0f/6.0f,1.0f/6.0f),
		CCHide::create(),
		NULL);
	sp_yellowFrame->runAction(sequence);
	sequence->setTag(ktag_yellowFrame_Anm);
}

void CCTeachingGuide::updateTime( float dt )
{
	if (m_bIsTimeUP)
		return;

	m_nDelayTime++;
	if (m_nDelayTime > SCRIPT_CCTEACHINGGUIDE_DELAY_TIME)
	{
		m_bIsTimeUP = true;
	}
}

void CCTeachingGuide::CloseUIForTutorial()
{
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (!mainScene)
		return;

	for (int i = 0;i<ScriptManager::getInstance()->mCloseUIData.size();i++)
	{
		if (mainScene->getChildByTag(ScriptManager::getInstance()->mCloseUIData.at(i)))
		{
			mainScene->getChildByTag(ScriptManager::getInstance()->mCloseUIData.at(i))->removeFromParent();
		}
	}
}

