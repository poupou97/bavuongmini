#include "SCTutorialStrength.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../ui/equipMent_ui/GeneralEquipMent.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../messageclient/element/GoodsInfo.h"


IMPLEMENT_CLASS(SCTutorialEquipmentStep1)

SCTutorialEquipmentStep1::SCTutorialEquipmentStep1()
{
}

SCTutorialEquipmentStep1::~SCTutorialEquipmentStep1()
{
}

void* SCTutorialEquipmentStep1::createInstance()
{
	return new SCTutorialEquipmentStep1() ;
}

void SCTutorialEquipmentStep1::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);
			bool isOpStudy =false;
			for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
			{
				if ((GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() > 0 &&GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz()<11) ||
								GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO)
				{
					isOpStudy =true;
				}
			}
			if (GameView::getInstance()->EquipListItem.size()<=0 || isOpStudy ==false)
			{
				setState(STATE_END);
			}else
			{
				auto equipUi =(EquipMentUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
				if (equipUi != NULL)
				{
					auto generalEquip =(GeneralEquipMent *)equipUi->roleLayer->getChildByTag(equipUi->generalWeapTag);
					if (generalEquip != NULL)
					{
						generalEquip->addCCTutorialIndicator(content.c_str(),Vec2(0,0),CCTutorialIndicator::Direction_RD);
						generalEquip->registerScriptCommand(this->getId());

						//Btn_pacItemFrame
						this->setCommandHandler(generalEquip->Btn_pacItemFrame);
						setState(STATE_UPDATE);
					}

				}
			}
			
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquipmentStep1::init() {
	CCLOG("SCTutorialRobotStep1() init");
}

void SCTutorialEquipmentStep1::release() {
	CCLOG("SCTutorialRobotStep1() release");

	bool isOpStudy =false;
	for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
	{
		if ((GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() > 0 &&GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz()<11) ||
			GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO)
		{
			isOpStudy =true;
		}
	}
	if (GameView::getInstance()->EquipListItem.size()<=0 || isOpStudy ==false)
	{
		return;
	}else
	{
		if (!GameView::getInstance()->getGameScene())
			return;

		if (!GameView::getInstance()->getMainUIScene())
			return;

		auto equipUi =(EquipMentUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		if (equipUi != NULL)
		{
			auto generalEquip =(GeneralEquipMent *)equipUi->roleLayer->getChildByTag(equipUi->generalWeapTag);
			generalEquip->removeCCTutorialIndicator();
		}
	
	}
}

/////////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialEquipmentStep2)

SCTutorialEquipmentStep2::SCTutorialEquipmentStep2()
{
}

SCTutorialEquipmentStep2::~SCTutorialEquipmentStep2()
{
}

void* SCTutorialEquipmentStep2::createInstance()
{
	return new SCTutorialEquipmentStep2() ;
}

void SCTutorialEquipmentStep2::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);
			bool isOpStudy =false;
			for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
			{
				if ((GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() > 0 &&GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz()<11) ||
					GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO)
				{
					isOpStudy =true;
				}
			}
			if (GameView::getInstance()->EquipListItem.size()<=0 || isOpStudy ==false)
			{
				setState(STATE_END);
			}else
			{
				GeneralEquipMentInfo * equipInfo =(GeneralEquipMentInfo *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase);
				if (equipInfo != NULL)
				{
					equipInfo->addCCTutorialIndicator(content.c_str(),equipInfo->btn_addMainEquip->getPosition(),CCTutorialIndicator::Direction_LD);
					equipInfo->registerScriptCommand(this->getId());
					this->setCommandHandler(equipInfo->btn_addMainEquip);

					setState(STATE_UPDATE);
				}else
				{
					setState(STATE_END);
				}
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquipmentStep2::init() {

}

void SCTutorialEquipmentStep2::release() {
	
	bool isOpStudy =false;
	for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
	{
		if ((GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() > 0 &&GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz()<11) ||
			GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO)
		{
			isOpStudy =true;
		}
	}
	if (GameView::getInstance()->EquipListItem.size()<=0 || isOpStudy ==false)
	{
		return;
	}else
	{
		GeneralEquipMentInfo * equipInfo =(GeneralEquipMentInfo *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase);
		if (equipInfo != NULL)
		{
			equipInfo->removeCCTutorialIndicator();
		}
	}
}

void SCTutorialEquipmentStep2::endCommand( Ref* pSender )
{
	Script::endCommand(pSender);
	release();
}

////////////////////////////
IMPLEMENT_CLASS(SCTutorialEquipMentStep3)

SCTutorialEquipMentStep3::SCTutorialEquipMentStep3()
{
}

SCTutorialEquipMentStep3::~SCTutorialEquipMentStep3()
{
}

void* SCTutorialEquipMentStep3::createInstance()
{
	return new SCTutorialEquipMentStep3() ;
}

void SCTutorialEquipMentStep3::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);

			bool isOpStudy =false;
			for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
			{
				if ((GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() > 0 &&GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz()<11) ||
					GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO)
				{
					isOpStudy =true;
				}
			}
			if (GameView::getInstance()->EquipListItem.size()<=0 || isOpStudy ==false)
			{
				setState(STATE_END);
			}else
			{
				auto equipUi =(EquipMentUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
				if (equipUi != NULL)
				{
					if (equipUi->refineMainEquip==true)
					{
						equipUi->addCCTutorialIndicator(content.c_str(),equipUi->btn_addAll->getPosition(),CCTutorialIndicator::Direction_LD);
						equipUi->registerScriptCommand(this->getId());
						this->setCommandHandler(equipUi->btn_addAll);

						setState(STATE_UPDATE);
					}
				}else
				{
					setState(STATE_END);
				}
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquipMentStep3::init() {
}

void SCTutorialEquipMentStep3::release() {

	bool isOpStudy =false;
	for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
	{
		if ((GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() > 0 &&GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz()<11) ||
			GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO)
		{
			isOpStudy =true;
		}
	}
	if (GameView::getInstance()->EquipListItem.size()<=0 || isOpStudy ==false)
	{
		return;
	}else
	{
		if (!GameView::getInstance()->getGameScene())
			return;

		if (!GameView::getInstance()->getMainUIScene())
			return;

		auto equipUi =(EquipMentUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		if (equipUi != NULL)
		{
			equipUi->removeCCTutorialIndicator();
		}
	}
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialEquipMentStep4)

SCTutorialEquipMentStep4::SCTutorialEquipMentStep4()
{
}

SCTutorialEquipMentStep4::~SCTutorialEquipMentStep4()
{
}

void* SCTutorialEquipMentStep4::createInstance()
{
	return new SCTutorialEquipMentStep4() ;
}

void SCTutorialEquipMentStep4::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);

			bool isOpStudy =false;
			for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
			{
				if ((GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() > 0 &&GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz()<11) ||
					GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO)
				{
					isOpStudy =true;
				}
			}
			if (GameView::getInstance()->EquipListItem.size()<=0 || isOpStudy ==false)
			{
				setState(STATE_END);
			}else
			{
				auto equipUi =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
				if (equipUi != NULL)
				{
					if (equipUi->refineMainEquip==true)
					{
						if (equipUi->firstAssist==true || equipUi->secondtAssist ==true || equipUi->thirdAssist==true || equipUi->fourAssist==true )
						{
							equipUi->addCCTutorialIndicator(content.c_str(),equipUi->btn_Refine->getPosition(),CCTutorialIndicator::Direction_LD);
							equipUi->registerScriptCommand(this->getId());
							this->setCommandHandler(equipUi->btn_Refine);
							setState(STATE_UPDATE);
						}

					}
				}else
				{
					setState(STATE_END);
				}
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquipMentStep4::init() {
}

void SCTutorialEquipMentStep4::release() {
	bool isOpStudy =false;
	for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
	{
		if ((GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() > 0 &&GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz()<11) ||
			GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO)
		{
			isOpStudy =true;
		}
	}
	if (GameView::getInstance()->EquipListItem.size()<=0 || isOpStudy ==false)
	{
		return;
	}else
	{
		if (!GameView::getInstance()->getGameScene())
			return;

		if (!GameView::getInstance()->getMainUIScene())
			return;

		auto equipInfo =(EquipMentUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		if (equipInfo != NULL)
		{
			equipInfo->removeCCTutorialIndicator();
		}
	}
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialEquipMentStep5)

SCTutorialEquipMentStep5::SCTutorialEquipMentStep5()
{
}

SCTutorialEquipMentStep5::~SCTutorialEquipMentStep5()
{
}

void* SCTutorialEquipMentStep5::createInstance()
{
	return new SCTutorialEquipMentStep5() ;
}

void SCTutorialEquipMentStep5::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);
			bool isOpStudy =false;
			for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
			{
				if ((GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() > 0 &&GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz()<11) ||
					GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO)
				{
					isOpStudy =true;
				}
			}
			if (GameView::getInstance()->EquipListItem.size()<=0 || isOpStudy ==false)
			{
				setState(STATE_END);
			}else
			{
				auto equipUi =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
				if (equipUi != NULL)
				{
					equipUi->addCCTutorialIndicatorClose(content.c_str(),equipUi->btn_close->getPosition(),CCTutorialIndicator::Direction_RU);
					equipUi->registerScriptCommand(this->getId());
					this->setCommandHandler(equipUi->btn_close);

					setState(STATE_UPDATE);
				}
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquipMentStep5::init() {
}

void SCTutorialEquipMentStep5::release() {

	bool isOpStudy =false;
	for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
	{
		if ((GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() > 0 &&GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz()<11) ||
			GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO)
		{
			isOpStudy =true;
		}
	}
	if (GameView::getInstance()->EquipListItem.size()<=0 || isOpStudy ==false)
	{
		return;
	}else
	{
		if (!GameView::getInstance()->getGameScene())
			return;

		if (!GameView::getInstance()->getMainUIScene())
			return;

		auto equipUi =(EquipMentUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		if (equipUi != NULL)
		{
			equipUi->removeCCTutorialIndicator();
		}
	}
}

void SCTutorialEquipMentStep5::endCommand( Ref* pSender )
{
	Script::endCommand(pSender);
	release();
}
