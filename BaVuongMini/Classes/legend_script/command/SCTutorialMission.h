#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALMISSION_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALMISSION_H_

#include "../Script.h"

/**
 *�����ѧ
 *
 * @author yangjun
 * @date 2014-3-21
 */

//////////////////////�������������ȡ����////////////////////////
class SCTutorialGetMission : public Script {
private:
	DECLARE_CLASS(SCTutorialGetMission)

public:
	SCTutorialGetMission();
	virtual ~SCTutorialGetMission();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////������������ύ����////////////////////////
class SCTutorialFinishMission : public Script {
private:
	DECLARE_CLASS(SCTutorialFinishMission)

public:
	SCTutorialFinishMission();
	virtual ~SCTutorialFinishMission();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////�������������////////////////////////
class SCTutorialOpenRewardTaskUI : public Script {
private:
	DECLARE_CLASS(SCTutorialOpenRewardTaskUI)

public:
	SCTutorialOpenRewardTaskUI();
	virtual ~SCTutorialOpenRewardTaskUI();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


//////////////////////����������///////////////////////
class SCTutorialGetRewardTask : public Script {
private:
	DECLARE_CLASS(SCTutorialGetRewardTask)

public:
	SCTutorialGetRewardTask();
	virtual ~SCTutorialGetRewardTask();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif

