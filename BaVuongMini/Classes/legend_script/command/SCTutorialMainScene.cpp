#include "SCTutorialMainScene.h"

#include "../../utils/GameUtils.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/MyHeadInfo.h"
#include "../../gamescene_state/sceneelement/HeadMenu.h"
#include "../CCTutorialIndicator.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../../gamescene_state/sceneelement/MissionCell.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../utils/Joystick.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../gamescene_state/sceneelement/GeneralsInfoUI.h"
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../newcomerstory/NewCommerStoryManager.h"
#include "../../gamescene_state/role/MyPlayerAIConfig.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerAI.h"

IMPLEMENT_CLASS(SCTutorialMainScene1)

SCTutorialMainScene1::SCTutorialMainScene1()
{
}

SCTutorialMainScene1::~SCTutorialMainScene1()
{
}

void* SCTutorialMainScene1::createInstance()
{
	return new SCTutorialMainScene1() ;
}

void SCTutorialMainScene1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene1()");

			std::string content = mPara.at(0);

			if (GameView::getInstance()->getMainUIScene()->isHeadMenuOn)
			{
				setState(STATE_END);
			}
			else
			{
				auto myHeadInfo = GameView::getInstance()->getMainUIScene()->curMyHeadInfo;
				myHeadInfo->addCCTutorialIndicator(content.c_str(),myHeadInfo->getPosition(),CCTutorialIndicator::Direction_LU);
				//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
				myHeadInfo->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(myHeadInfo->Button_headFrame) ;
				setState(STATE_UPDATE);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene1::init() {
	CCLOG("SCTutorialMainScene1() init");
}

void SCTutorialMainScene1::release() {
	CCLOG("SCTutorialMainScene1() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto myHeadInfo = GameView::getInstance()->getMainUIScene()->curMyHeadInfo;
	if(myHeadInfo != NULL)
		myHeadInfo->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialMainScene1Close)

	SCTutorialMainScene1Close::SCTutorialMainScene1Close()
{
}

SCTutorialMainScene1Close::~SCTutorialMainScene1Close()
{
}

void* SCTutorialMainScene1Close::createInstance()
{
	return new SCTutorialMainScene1Close() ;
}

void SCTutorialMainScene1Close::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene1()");

			std::string content = mPara.at(0);

			if (GameView::getInstance()->getMainUIScene()->isHeadMenuOn)
			{
				auto myHeadInfo = GameView::getInstance()->getMainUIScene()->curMyHeadInfo;
				myHeadInfo->addCCTutorialIndicator(content.c_str(),myHeadInfo->getPosition(),CCTutorialIndicator::Direction_LU);
				//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
				myHeadInfo->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(myHeadInfo->Button_headFrame) ;
				setState(STATE_UPDATE);
			}
			else
			{
				setState(STATE_END);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene1Close::init() {
	CCLOG("SCTutorialMainScene1Close() init");
}

void SCTutorialMainScene1Close::release() {
	CCLOG("SCTutorialMainScene1Close() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto myHeadInfo = GameView::getInstance()->getMainUIScene()->curMyHeadInfo;
	if(myHeadInfo != NULL)
		myHeadInfo->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialMainSceneSetMusou1)

	SCTutorialMainSceneSetMusou1::SCTutorialMainSceneSetMusou1()
{
}

SCTutorialMainSceneSetMusou1::~SCTutorialMainSceneSetMusou1()
{
}

void* SCTutorialMainSceneSetMusou1::createInstance()
{
	return new SCTutorialMainSceneSetMusou1() ;
}

void SCTutorialMainSceneSetMusou1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainSceneSetMusou1()");

			std::string content = mPara.at(0);

			if (GameView::getInstance()->myplayer->getMusouSkill() != NULL)
			{
				//setState(STATE_END);
				ScriptManager::getInstance()->stop();
			}
			else
			{
				if (GameView::getInstance()->getMainUIScene()->isHeadMenuOn)
				{
					//setState(STATE_END);
					ScriptManager::getInstance()->stop();
				}
				else
				{
					auto myHeadInfo = GameView::getInstance()->getMainUIScene()->curMyHeadInfo;
					myHeadInfo->addCCTutorialIndicator(content.c_str(),myHeadInfo->getPosition(),CCTutorialIndicator::Direction_LU);
					//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
					myHeadInfo->registerScriptCommand(this->getId()) ; 
					this->setCommandHandler(myHeadInfo->Button_headFrame) ;
					setState(STATE_UPDATE);
				}
			}
			
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainSceneSetMusou1::init() {
	CCLOG("SCTutorialMainSceneSetMusou1() init");
}

void SCTutorialMainSceneSetMusou1::release() {
	CCLOG("SCTutorialMainSceneSetMusou1() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto myHeadInfo = GameView::getInstance()->getMainUIScene()->curMyHeadInfo;
	if(myHeadInfo != NULL)
		myHeadInfo->removeCCTutorialIndicator();
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialMainScene2)

SCTutorialMainScene2::SCTutorialMainScene2()
{
}

SCTutorialMainScene2::~SCTutorialMainScene2()
{
}

void* SCTutorialMainScene2::createInstance()
{
	return new SCTutorialMainScene2() ;
}

void SCTutorialMainScene2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialTestStep2()");

			std::string content = mPara.at(0);

			auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
			headMenu->addCCTutorialIndicator(content.c_str(),headMenu->btn_skill->getPosition(),CCTutorialIndicator::Direction_LD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			headMenu->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(headMenu->btn_skill) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene2::init() {
	CCLOG("SCTutorialMainScene2() init");
}

void SCTutorialMainScene2::release() {
	CCLOG("SCTutorialMainScene2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
	if(headMenu != NULL)
		headMenu->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialMainSceneSetMusou2)

	SCTutorialMainSceneSetMusou2::SCTutorialMainSceneSetMusou2()
{
}

SCTutorialMainSceneSetMusou2::~SCTutorialMainSceneSetMusou2()
{
}

void* SCTutorialMainSceneSetMusou2::createInstance()
{
	return new SCTutorialMainSceneSetMusou2() ;
}

void SCTutorialMainSceneSetMusou2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialTestStep2()");

			std::string content = mPara.at(0);

			if (GameView::getInstance()->myplayer->getMusouSkill() != NULL)
			{
				//setState(STATE_END);
				ScriptManager::getInstance()->stop();
			}
			else
			{
				auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
				headMenu->addCCTutorialIndicator(content.c_str(),headMenu->btn_skill->getPosition(),CCTutorialIndicator::Direction_LD);
				//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
				headMenu->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(headMenu->btn_skill) ;
				setState(STATE_UPDATE);

				if (GameView::getInstance()->getMainUIScene()->isHeadMenuOn)
				{
					if (headMenu->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						headMenu->getChildByTag(CCTUTORIALINDICATORTAG)->setVisible(true);
					}

					if (headMenu->getChildByTag(CCTUTORIALPARTICLETAG))
					{
						headMenu->getChildByTag(CCTUTORIALPARTICLETAG)->setVisible(true);
					}
				}
				else
				{
					if (headMenu->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						headMenu->getChildByTag(CCTUTORIALINDICATORTAG)->setVisible(false);
					}

					if (headMenu->getChildByTag(CCTUTORIALPARTICLETAG))
					{
						headMenu->getChildByTag(CCTUTORIALPARTICLETAG)->setVisible(false);
					}
				}
			}
			
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainSceneSetMusou2::init() {
	CCLOG("SCTutorialMainSceneSetMusou2() init");
}

void SCTutorialMainSceneSetMusou2::release() {
	CCLOG("SCTutorialMainSceneSetMusou2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
	if(headMenu != NULL)
		headMenu->removeCCTutorialIndicator();
}



/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialMainSceneForFirst)

SCTutorialMainSceneForFirst::SCTutorialMainSceneForFirst()
{
}

SCTutorialMainSceneForFirst::~SCTutorialMainSceneForFirst()
{
}

void* SCTutorialMainSceneForFirst::createInstance()
{
	return new SCTutorialMainSceneForFirst() ;
}

void SCTutorialMainSceneForFirst::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainSceneForFirst()");

			std::string content = mPara.at(0);

			if (!MainScene::GeneralsScene)
			{
				setState(STATE_START);
				return;
			}

			GeneralsUI::generalsListUI->isInFirstGeneralTutorial = true;

			auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
			headMenu->addCCTutorialIndicator(content.c_str(),headMenu->btn_generals->getPosition(),CCTutorialIndicator::Direction_LD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			headMenu->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(headMenu->btn_generals) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainSceneForFirst::init() {
	CCLOG("SCTutorialMainSceneForFirst() init");
}

void SCTutorialMainSceneForFirst::release() {
	CCLOG("SCTutorialMainSceneForFirst() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
	if(headMenu != NULL)
		headMenu->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialMainSceneForSecond)

	SCTutorialMainSceneForSecond::SCTutorialMainSceneForSecond()
{
}

SCTutorialMainSceneForSecond::~SCTutorialMainSceneForSecond()
{
}

void* SCTutorialMainSceneForSecond::createInstance()
{
	return new SCTutorialMainSceneForSecond() ;
}

void SCTutorialMainSceneForSecond::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainSceneForSecond()");

			std::string content = mPara.at(0);

			if (!MainScene::GeneralsScene)
			{
				setState(STATE_START);
				return;
			}

			GeneralsUI::generalsListUI->isInFirstGeneralTutorial = false;

			auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
			headMenu->addCCTutorialIndicator(content.c_str(),headMenu->btn_generals->getPosition(),CCTutorialIndicator::Direction_LD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			headMenu->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(headMenu->btn_generals) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainSceneForSecond::init() {
	CCLOG("SCTutorialMainSceneForSecond() init");
}

void SCTutorialMainSceneForSecond::release() {
	CCLOG("SCTutorialMainSceneForSecond() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
	if(headMenu != NULL)
		headMenu->removeCCTutorialIndicator();
}



/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialMainScene4)

SCTutorialMainScene4::SCTutorialMainScene4()
{
}

SCTutorialMainScene4::~SCTutorialMainScene4()
{
}

void* SCTutorialMainScene4::createInstance()
{
	return new SCTutorialMainScene4() ;
}

void SCTutorialMainScene4::update()
{
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene4()");

			std::string content = mPara.at(0);
			
			if (GameView::getInstance()->getMainUIScene()->isMapOpen)
			{
				setState(STATE_END);
			}
			else
			{
				auto mainScene_ =(MainScene *)GameView::getInstance()->getMainUIScene();
				//mainScene_->addCCTutorialIndicator(content.c_str(),mainScene_->guideMap->getPosition(),CCTutorialIndicator::Direction_LU);
				mainScene_->addCCTutorialIndicator(content.c_str(),mainScene_->Button_mapOpen->getPosition(),CCTutorialIndicator::Direction_RU);
				mainScene_->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(mainScene_->Button_mapOpen) ;
				setState(STATE_UPDATE);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene4::init()
{
	CCLOG("SCTutorialMainScene4() init");
}

void SCTutorialMainScene4::release()
{
	CCLOG("SCTutorialMainScene4() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto mainScene_ =(MainScene *)GameView::getInstance()->getMainUIScene();
	if(mainScene_->guideMap != NULL)
		mainScene_->removeCCTutorialIndicator();
}

//////////////ǿ�����////////////////////
IMPLEMENT_CLASS(SCTutorialMainSceneSetStrength1)

SCTutorialMainSceneSetStrength1::SCTutorialMainSceneSetStrength1()
{
}

SCTutorialMainSceneSetStrength1::~SCTutorialMainSceneSetStrength1()
{
}

void* SCTutorialMainSceneSetStrength1::createInstance()
{
	return new SCTutorialMainSceneSetStrength1() ;
}

void SCTutorialMainSceneSetStrength1::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);
			bool isOpStudy =false;
			for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
			{
				if ((GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() > 0 &&GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz()<11) ||
					GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO)
				{
					isOpStudy =true;
				}
			}
			if (GameView::getInstance()->EquipListItem.size()<=0 || isOpStudy ==false)
			{
				setState(STATE_END);
			}
			else
			{
				if (GameView::getInstance()->getMainUIScene()->isHeadMenuOn)
				{
					setState(STATE_END);
				}
				else
				{
					auto myHeadInfo = GameView::getInstance()->getMainUIScene()->curMyHeadInfo;
					myHeadInfo->addCCTutorialIndicator(content.c_str(),myHeadInfo->getPosition(),CCTutorialIndicator::Direction_LU);
					myHeadInfo->registerScriptCommand(this->getId()) ; 
					this->setCommandHandler(myHeadInfo->Button_headFrame) ;
					setState(STATE_UPDATE);
				}
			}

		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainSceneSetStrength1::init() {
	CCLOG("SCTutorialMainSceneSetMusou1() init");
}

void SCTutorialMainSceneSetStrength1::release() {
	CCLOG("SCTutorialMainSceneSetMusou1() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto myHeadInfo = GameView::getInstance()->getMainUIScene()->curMyHeadInfo;
	if(myHeadInfo != NULL)
		myHeadInfo->removeCCTutorialIndicator();
}



IMPLEMENT_CLASS(SCTutorialMainScene5)

SCTutorialMainScene5::SCTutorialMainScene5()
{
}

SCTutorialMainScene5::~SCTutorialMainScene5()
{
}

void* SCTutorialMainScene5::createInstance()
{
	return new SCTutorialMainScene5() ;
}

void SCTutorialMainScene5::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);
			bool isOpStudy =false;
			for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
			{
				if ((GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() > 0 &&GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz()<11) ||
					GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_JINGLIANCAILIAO)
				{
					isOpStudy =true;
				}
			}
			if (GameView::getInstance()->EquipListItem.size()<=0 || isOpStudy ==false)
			{
				setState(STATE_END);
			}else
			{
				auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
				headMenu->addCCTutorialIndicator(content.c_str(),headMenu->btn_strengthen->getPosition(),CCTutorialIndicator::Direction_LD);
				headMenu->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(headMenu->btn_strengthen) ;
				setState(STATE_UPDATE);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene5::init() {
}

void SCTutorialMainScene5::release() {

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
	if(headMenu != NULL)
		headMenu->removeCCTutorialIndicator();
}

////////////////ǿ�����////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialMainSceneSetStrengthStar1)

SCTutorialMainSceneSetStrengthStar1::SCTutorialMainSceneSetStrengthStar1()
{
}

SCTutorialMainSceneSetStrengthStar1::~SCTutorialMainSceneSetStrengthStar1()
{
}

void* SCTutorialMainSceneSetStrengthStar1::createInstance()
{
	return new SCTutorialMainSceneSetStrengthStar1() ;
}

void SCTutorialMainSceneSetStrengthStar1::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);
			bool isOpStudy =false;
			
			if (GameView::getInstance()->EquipListItem.size()<=0)
			{
				setState(STATE_END);
			}
			else
			{
				if (GameView::getInstance()->getMainUIScene()->isHeadMenuOn)
				{
					setState(STATE_END);
				}
				else
				{
					auto myHeadInfo = GameView::getInstance()->getMainUIScene()->curMyHeadInfo;
					myHeadInfo->addCCTutorialIndicator(content.c_str(),myHeadInfo->getPosition(),CCTutorialIndicator::Direction_LU);
					myHeadInfo->registerScriptCommand(this->getId()) ; 
					this->setCommandHandler(myHeadInfo->Button_headFrame) ;
					setState(STATE_UPDATE);
				}
			}

		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainSceneSetStrengthStar1::init() {
}

void SCTutorialMainSceneSetStrengthStar1::release() {

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto myHeadInfo = GameView::getInstance()->getMainUIScene()->curMyHeadInfo;
	if(myHeadInfo != NULL)
		myHeadInfo->removeCCTutorialIndicator();
}


IMPLEMENT_CLASS(SCTutorialMainSceneStar1)

SCTutorialMainSceneStar1::SCTutorialMainSceneStar1()
{
}

SCTutorialMainSceneStar1::~SCTutorialMainSceneStar1()
{
}

void* SCTutorialMainSceneStar1::createInstance()
{
	return new SCTutorialMainSceneStar1() ;
}

void SCTutorialMainSceneStar1::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);
			
			if (GameView::getInstance()->EquipListItem.size()<=0)
			{
				setState(STATE_END);
			}else
			{
				auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
				headMenu->addCCTutorialIndicator(content.c_str(),headMenu->btn_strengthen->getPosition(),CCTutorialIndicator::Direction_LD);
				headMenu->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(headMenu->btn_strengthen) ;
				setState(STATE_UPDATE);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainSceneStar1::init() {
}

void SCTutorialMainSceneStar1::release() {
	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
	if(headMenu != NULL)
		headMenu->removeCCTutorialIndicator();
}



/////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialMainScene6)

SCTutorialMainScene6::SCTutorialMainScene6()
{
}

SCTutorialMainScene6::~SCTutorialMainScene6()
{
}

void* SCTutorialMainScene6::createInstance()
{
	return new SCTutorialMainScene6() ;
}

void SCTutorialMainScene6::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene6()");

			std::string content = mPara.at(0);

			auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
			headMenu->addCCTutorialIndicator(content.c_str(),headMenu->btn_roleIf->getPosition(),CCTutorialIndicator::Direction_LD);
			headMenu->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(headMenu->btn_roleIf) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene6::init() {
	CCLOG("SCTutorialMainScene2() init");
}

void SCTutorialMainScene6::release() {
	CCLOG("SCTutorialMainScene2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto headMenu = (HeadMenu*)GameView::getInstance()->getMainUIScene()->headMenu;
	if(headMenu != NULL)
		headMenu->removeCCTutorialIndicator();
}




/////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialMissionAndTeam1)

SCTutorialMissionAndTeam1::SCTutorialMissionAndTeam1()
{
}

SCTutorialMissionAndTeam1::~SCTutorialMissionAndTeam1()
{
}

void* SCTutorialMissionAndTeam1::createInstance()
{
	return new SCTutorialMissionAndTeam1() ;
}

void SCTutorialMissionAndTeam1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMissionAndTeam1()");

			std::string content = mPara.at(0);

			auto missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
			if (!missionAndTeam)
			{
				setState(STATE_START);
				return;
			}

			missionAndTeam->m_nTutorialIndicatorIndex = 1;
			missionAndTeam->addCCTutorialIndicator1(content.c_str(),missionAndTeam->getPosition(),CCTutorialIndicator::Direction_LU);
			missionAndTeam->registerScriptCommand(this->getId()); 
			this->setCommandHandler(missionAndTeam);
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMissionAndTeam1::init() {
	CCLOG("SCTutorialMissionAndTeam1() init");
}

void SCTutorialMissionAndTeam1::release() {
	CCLOG("SCTutorialMissionAndTeam1() release");

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene != NULL)
	{
		MainScene* mainUI = scene->getMainUIScene();
		if(mainUI != NULL)
		{
			auto missionAndTeam = (MissionAndTeam*)mainUI->getChildByTag(kTagMissionAndTeam);
			if(missionAndTeam != NULL)
				missionAndTeam->removeCCTutorialIndicator();
		}
	}
}


/////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialMissionAndTeam2)

	SCTutorialMissionAndTeam2::SCTutorialMissionAndTeam2()
{
}

SCTutorialMissionAndTeam2::~SCTutorialMissionAndTeam2()
{
}

void* SCTutorialMissionAndTeam2::createInstance()
{
	return new SCTutorialMissionAndTeam2() ;
}

void SCTutorialMissionAndTeam2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMissionAndTeam2()");

			std::string content = mPara.at(0);

			auto missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
			if (!missionAndTeam)
			{
				setState(STATE_START);
				return;
			}

			missionAndTeam->m_nTutorialIndicatorIndex = 2;
			missionAndTeam->addCCTutorialIndicator2(content.c_str(),missionAndTeam->getPosition(),CCTutorialIndicator::Direction_LD);
			missionAndTeam->registerScriptCommand(this->getId()); 
			this->setCommandHandler(missionAndTeam);
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMissionAndTeam2::init() {
	CCLOG("SCTutorialMissionAndTeam2() init");
}

void SCTutorialMissionAndTeam2::release() {
	CCLOG("SCTutorialMissionAndTeam2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
	if(missionAndTeam != NULL)
		missionAndTeam->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialMissionAndTeam3)

SCTutorialMissionAndTeam3::SCTutorialMissionAndTeam3()
{
}

SCTutorialMissionAndTeam3::~SCTutorialMissionAndTeam3()
{
}

void* SCTutorialMissionAndTeam3::createInstance()
{
	return new SCTutorialMissionAndTeam3() ;
}

void SCTutorialMissionAndTeam3::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMissionAndTeam3()");

			std::string content = mPara.at(0);

			if(FivePersonInstance::getStatus() != FivePersonInstance::status_in_instance)
			{
				setState(STATE_START);
				return;
			}

			auto missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
			if (!missionAndTeam)
			{
				setState(STATE_START);
				return;
			}
			else
			{
				if ((!missionAndTeam->isOn) || (missionAndTeam->getCurPresentPanel() != MissionAndTeam::p_fiveInstance))
				{
					setState(STATE_END);
					return;
				}
			}

			missionAndTeam->m_nTutorialIndicatorIndex = 3;
			missionAndTeam->addCCTutorialIndicator3(content.c_str(),Vec2(0,0),CCTutorialIndicator::Direction_LU);
			missionAndTeam->registerScriptCommand(this->getId()); 
			this->setCommandHandler(missionAndTeam);
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMissionAndTeam3::init() {
	CCLOG("SCTutorialMissionAndTeam3() init");
}

void SCTutorialMissionAndTeam3::release() {
	CCLOG("SCTutorialMissionAndTeam3() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto missionAndTeam = (MissionAndTeam*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
	if(missionAndTeam != NULL)
		missionAndTeam->removeCCTutorialIndicator();
}



/////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialMainScene7)

SCTutorialMainScene7::SCTutorialMainScene7()
{
}

SCTutorialMainScene7::~SCTutorialMainScene7()
{
}

void* SCTutorialMainScene7::createInstance()
{
	return new SCTutorialMainScene7() ;
}

void SCTutorialMainScene7::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene7()");

			std::string content = mPara.at(0);

			auto mainScene = GameView::getInstance()->getMainUIScene();
			auto pJoystick = (Joystick*)mainScene->getChildByTag(kTagVirtualJoystick);
			
			mainScene->addCCTutorialIndicator1(content.c_str(),pJoystick->getPosition(),CCTutorialIndicator::Direction_LD);
			//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
			mainScene->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(pJoystick) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene7::init() {
	CCLOG("SCTutorialMainScene7() init");
}

void SCTutorialMainScene7::release() {
	CCLOG("SCTutorialMainScene7() release");
	if (GameView::getInstance()->getGameScene())
	{
		auto mainScene = GameView::getInstance()->getMainUIScene();
		if(mainScene != NULL)
			mainScene->removeCCTutorialIndicator();
	}
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialMainScene8)

SCTutorialMainScene8::SCTutorialMainScene8()
{
}

SCTutorialMainScene8::~SCTutorialMainScene8()
{
}

void* SCTutorialMainScene8::createInstance()
{
	return new SCTutorialMainScene8() ;
}

void SCTutorialMainScene8::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene8()");

			std::string content = mPara.at(0);

			auto shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			shortcutLayer->addCCTutorialIndicator1(content.c_str(),shortcutLayer->getChildByTag(102)->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			shortcutLayer->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(shortcutLayer) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene8::init() {
	CCLOG("SCTutorialMainScene8() init");
}

void SCTutorialMainScene8::release() {
	CCLOG("SCTutorialMainScene8() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	if(shortcutLayer != NULL)
		shortcutLayer->removeCCTutorialIndicator();
}




/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialMainScene9)

SCTutorialMainScene9::SCTutorialMainScene9()
{
}

SCTutorialMainScene9::~SCTutorialMainScene9()
{
}

void* SCTutorialMainScene9::createInstance()
{
	return new SCTutorialMainScene9() ;
}

void SCTutorialMainScene9::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene9()");

			std::string content = mPara.at(0);

			auto shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			shortcutLayer->addCCTutorialIndicator1(content.c_str(),shortcutLayer->getChildByTag(103)->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			shortcutLayer->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(shortcutLayer->getChildByTag(103)) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene9::init() {
	CCLOG("SCTutorialMainScene9() init");
}

void SCTutorialMainScene9::release() {
	CCLOG("SCTutorialMainScene8() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	if(shortcutLayer != NULL)
		shortcutLayer->removeCCTutorialIndicator();
}





/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialMainScene10)

SCTutorialMainScene10::SCTutorialMainScene10()
{
}

SCTutorialMainScene10::~SCTutorialMainScene10()
{
}

void* SCTutorialMainScene10::createInstance()
{
	return new SCTutorialMainScene10() ;
}

void SCTutorialMainScene10::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene10()");

			std::string content = mPara.at(0);

			auto shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			shortcutLayer->addCCTutorialIndicator1(content.c_str(),shortcutLayer->getChildByTag(102)->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			shortcutLayer->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(shortcutLayer->getChildByTag(102)) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene10::init() {
	CCLOG("SCTutorialMainScene10() init");
}

void SCTutorialMainScene10::release() {
	CCLOG("SCTutorialMainScene10() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	if(shortcutLayer != NULL)
		shortcutLayer->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialMainScene11)

SCTutorialMainScene11::SCTutorialMainScene11()
{
}

SCTutorialMainScene11::~SCTutorialMainScene11()
{
}

void* SCTutorialMainScene11::createInstance()
{
	return new SCTutorialMainScene11() ;
}

void SCTutorialMainScene11::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene11()");

			std::string content = mPara.at(0);

			auto generalsInfoUI = (GeneralsInfoUI*)GameView::getInstance()->getMainUIScene()->getGeneralInfoUI();
			if (generalsInfoUI)
			{
				generalsInfoUI->addCCTutorialIndicator1(content.c_str(),generalsInfoUI->getGeneralItemByIndex(0)->getPosition(),CCTutorialIndicator::Direction_LD);
				//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
				generalsInfoUI->registerScriptCommand(this->getId()) ; 
				generalsInfoUI->mTutorialIndex = 1;
				this->setCommandHandler(generalsInfoUI) ;
				setState(STATE_UPDATE);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene11::init() {
	CCLOG("SCTutorialMainScene11() init");
}

void SCTutorialMainScene11::release() {
	CCLOG("SCTutorialMainScene11() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto generalsInfoUI = (GeneralsInfoUI*)GameView::getInstance()->getMainUIScene()->getGeneralInfoUI();
	if (generalsInfoUI != NULL)
		generalsInfoUI->removeCCTutorialIndicator();
}



/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialMainScene12)

SCTutorialMainScene12::SCTutorialMainScene12()
{
}

SCTutorialMainScene12::~SCTutorialMainScene12()
{
}

void* SCTutorialMainScene12::createInstance()
{
	return new SCTutorialMainScene12() ;
}

void SCTutorialMainScene12::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene12()");

			std::string content = mPara.at(0);

			auto generalsInfoUI = (GeneralsInfoUI*)GameView::getInstance()->getMainUIScene()->getGeneralInfoUI();
			if (generalsInfoUI)
			{
				generalsInfoUI->addCCTutorialIndicator1(content.c_str(),generalsInfoUI->getGeneralItemByIndex(1)->getPosition(),CCTutorialIndicator::Direction_LD);
				//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
				generalsInfoUI->registerScriptCommand(this->getId()) ; 
				generalsInfoUI->mTutorialIndex = 2;
				this->setCommandHandler(generalsInfoUI) ;
				setState(STATE_UPDATE);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene12::init() {
	CCLOG("SCTutorialMainScene12() init");
}

void SCTutorialMainScene12::release() {
	CCLOG("SCTutorialMainScene12() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto generalsInfoUI = (GeneralsInfoUI*)GameView::getInstance()->getMainUIScene()->getGeneralInfoUI();
	if (generalsInfoUI != NULL)
		generalsInfoUI->removeCCTutorialIndicator();

	//set auto fight
	//set automatic skill
	MyPlayerAIConfig::setAutomaticSkill(1);
	//set star robot
	MyPlayerAIConfig::enableRobot(true);
	GameView::getInstance()->myplayer->getMyPlayerAI()->start();
}




/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialMainScene13)

	SCTutorialMainScene13::SCTutorialMainScene13()
{
}

SCTutorialMainScene13::~SCTutorialMainScene13()
{
}

void* SCTutorialMainScene13::createInstance()
{
	return new SCTutorialMainScene13() ;
}

void SCTutorialMainScene13::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene13()");

			std::string content = mPara.at(0);

			auto generalsInfoUI = (GeneralsInfoUI*)GameView::getInstance()->getMainUIScene()->getGeneralInfoUI();
			if (generalsInfoUI)
			{
				generalsInfoUI->addCCTutorialIndicator1(content.c_str(),generalsInfoUI->getGeneralItemByIndex(2)->getPosition(),CCTutorialIndicator::Direction_LD);
				//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
				generalsInfoUI->registerScriptCommand(this->getId()) ;
				generalsInfoUI->mTutorialIndex = 3;
				this->setCommandHandler(generalsInfoUI) ;
				setState(STATE_UPDATE);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene13::init() {
	CCLOG("SCTutorialMainScene13() init");
}

void SCTutorialMainScene13::release() {
	CCLOG("SCTutorialMainScene13() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto generalsInfoUI = (GeneralsInfoUI*)GameView::getInstance()->getMainUIScene()->getGeneralInfoUI();
	if (generalsInfoUI != NULL)
		generalsInfoUI->removeCCTutorialIndicator();

	if (NewCommerStoryManager::getInstance()->IsNewComer())
	{
		NewCommerStoryManager::getInstance()->startAutoGuideToCallGeneral();
	}
}




/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialMainScene14)

SCTutorialMainScene14::SCTutorialMainScene14()
{
}

SCTutorialMainScene14::~SCTutorialMainScene14()
{
}

void* SCTutorialMainScene14::createInstance()
{
	return new SCTutorialMainScene14() ;
}

void SCTutorialMainScene14::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainScene14()");

			std::string content = mPara.at(0);

			auto generalsInfoUI = (GeneralsInfoUI*)GameView::getInstance()->getMainUIScene()->getGeneralInfoUI();
			if (generalsInfoUI)
			{
				generalsInfoUI->addCCTutorialIndicator(content.c_str(),generalsInfoUI->getGeneralItemByIndex(0)->getPosition(),CCTutorialIndicator::Direction_LD);
				//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
				generalsInfoUI->registerScriptCommand(this->getId()) ; 
				generalsInfoUI->mTutorialIndex = 1;
				this->setCommandHandler(generalsInfoUI) ;
				setState(STATE_UPDATE);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialMainScene14::init() {
	CCLOG("SCTutorialMainScene14() init");
}

void SCTutorialMainScene14::release() {
	CCLOG("SCTutorialMainScene14() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto generalsInfoUI = (GeneralsInfoUI*)GameView::getInstance()->getMainUIScene()->getGeneralInfoUI();
	if (generalsInfoUI != NULL)
		generalsInfoUI->removeCCTutorialIndicator();
}









