#include "MovieScripts.h"

#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../ui/story/StoryDialog.h"
#include "../../ui/story/StoryBlackBand.h"
#include "../../gamescene_state/role/SimpleFighter.h"
#include "../../gamescene_state/role/SimpleActor.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/BaseFighterOwnedCommand.h"
#include "../../gamescene_state/GameSceneCamera.h"
#include "../../ui/story/StoryDialogPassLayer.h"
#include "../../ui/story/StoryDialog.h"
#include "../../utils/GameUtils.h"
#include "../../utils/Joystick.h"
#include "../../gamescene_state/role/BaseFighterOwnedStates.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "../../gamescene_state/role/GameActorAnimation.h"
#include "../../legend_engine/LegendAAnimation.h"
#include "../../gamescene_state/role/Monster.h"
#include "../../gamescene_state/MainScene.h"

// the node instance id which is added by scripts( addScreenEffect() )
#define MOVIE_INSTANCE_BASE_TAG 300

#define MOVIE_BASE_ROLE_ID (-5000)

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCDialog)

SCDialog::SCDialog()
{
}

SCDialog::~SCDialog()
{
}

void* SCDialog::createInstance()
{
	return new SCDialog() ;
}

void SCDialog::update() {
	switch (mState) {
	case STATE_START:
		{
		//CCLOG("excute dialog()");

		CCAssert(mPara.size() == 4, "wrong parameter");

		std::string content = mPara.at(0);
		std::string head = mPara.at(1);
		int head_position = std::atoi(mPara.at(2).c_str());
		std::string roleName = mPara.at(3);

		auto pDialog = StoryDialog::create(content.c_str(), head.c_str(), head_position, roleName.c_str());
		REGISTER_SCRIPT_COMMAND(pDialog, this->getId())
		ScriptManager::getInstance()->getMovieBaseNode()->addChild(pDialog, MOVIE_DIALOG_ZORDER, MOVIE_DIALOG_TAG);

		setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to end this dialog
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCDialog::init() {
}

void SCDialog::release() {
	if(ScriptManager::getInstance()->isInMovieMode())
	{
		auto pDialog = ScriptManager::getInstance()->getMovieBaseNode()->getChildByTag(MOVIE_DIALOG_TAG);
		if(pDialog != NULL)
		{
			pDialog->removeFromParent();
		}
	}
}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCMovieStart)

SCMovieStart::SCMovieStart()
:m_longTimeFlag(0.0f)
{
}

SCMovieStart::~SCMovieStart()
{
}

void* SCMovieStart::createInstance()
{
	return new SCMovieStart() ;
}

void SCMovieStart::update() {
	float tmp = 0.0f;
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute MovieStart()");

			// hide UI
			auto scene = GameView::getInstance()->getGameScene();
			if(scene != NULL)
			{
				auto pUIScene = (Node*)GameView::getInstance()->getMainUIScene();
				if(pUIScene != NULL)
				{
					pUIScene->setVisible(false);

					Joystick * pJoystick = dynamic_cast<Joystick*>(pUIScene->getChildByTag(kTagVirtualJoystick));
					if(pJoystick != NULL)
						pJoystick->Inactive();
				}
			}

			auto pBand = StoryBlackBand::create();
			ScriptManager::getInstance()->getMovieBaseNode()->addChild(pBand, MOVIE_BLACKBANK_ZORDER, MOVIE_BLACKBANK_TAG);

			auto pActionBlackBand = MoveBy::create(0.4f, Vec2(0, STORYBLACKBAND_HEIGHT));
			pBand->runAction(pActionBlackBand);

			m_longTimeFlag = GameUtils::millisecondNow();

			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:

		if((GameUtils::millisecondNow() - m_longTimeFlag) / 1000.0f > 0.4f)
		{
			// dialogPassLayer
			auto pDialogPassLayer = StoryDialogPassLayer::create();
			ScriptManager::getInstance()->getMovieBaseNode()->addChild(pDialogPassLayer, MOVIE_DIALOGPASSLAYER_ZORDER, MOVIE_DIALOGPASSLAYER_TAG);

			setState(STATE_END);
		}
		// wait the player to end this dialog
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCMovieStart::init() {
	// this function has no parameter
	CCAssert(mPara.at(0).size() == 0, "wrong parameter");
}

void SCMovieStart::release() {
}

void SCMovieStart::storyBlackBandActionCallBack(Ref* object)
{
	auto pSCMovieStart = (SCMovieStart *)object;

	// dialogPassLayer
	auto pDialogPassLayer = StoryDialogPassLayer::create();
	ScriptManager::getInstance()->getMovieBaseNode()->addChild(pDialogPassLayer, MOVIE_DIALOGPASSLAYER_ZORDER, MOVIE_DIALOGPASSLAYER_TAG);

	pSCMovieStart->setState(STATE_END);
}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCMovieEnd)

SCMovieEnd::SCMovieEnd()
{
}

SCMovieEnd::~SCMovieEnd()
{
}

void* SCMovieEnd::createInstance()
{
	return new SCMovieEnd() ;
}

void SCMovieEnd::update() {
	switch (mState) {
	case STATE_START:
		{
		CCLOG("excute SCMovieEnd()");

		ScriptManager::getInstance()->stopMovie();
		setState(STATE_END);   // end directly, do not need update()
		}
		break;

	case STATE_UPDATE:
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCMovieEnd::init() {
	// this function has no parameter
	CCAssert(mPara.at(0).size() == 0, "wrong parameter");
}

void SCMovieEnd::release() {
}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCBlackScreenStart)

SCBlackScreenStart::SCBlackScreenStart():
m_longTimeFlag(0),
time(0.f)
{
}

SCBlackScreenStart::~SCBlackScreenStart()
{
}

void* SCBlackScreenStart::createInstance()
{
	return new SCBlackScreenStart() ;
}

void SCBlackScreenStart::update() {
	switch (mState) {
	case STATE_START:
		{
			// black screen
			if (mPara.size() == 0)
			{
				Size s = Director::getInstance()->getVisibleSize();
				auto background = LayerColor::create(Color4B(0,0,0,255), s.width, s.height);
				background->setPosition(Vec2(0,0));
				ScriptManager::getInstance()->getMovieBaseNode()->addChild(background, 0, MOVIE_BLACKSCREEN_TAG);

				setState(STATE_END);
			}
			else
			{
				std::string content = mPara.at(0);
				if(mPara.size() == 1)
					time = std::atof(mPara.at(0).c_str()) / 1000.f;

				if (time <= 0.f)
				{
					Size s = Director::getInstance()->getVisibleSize();
					auto background = LayerColor::create(Color4B(0,0,0,255), s.width, s.height);
					background->setPosition(Vec2(0,0));
					ScriptManager::getInstance()->getMovieBaseNode()->addChild(background, 0, MOVIE_BLACKSCREEN_TAG);

					setState(STATE_END);
				}
				else
				{
					Size s = Director::getInstance()->getVisibleSize();
					auto background = LayerColor::create(Color4B(0,0,0,0), s.width, s.height);
					background->setPosition(Vec2(0,0));
					ScriptManager::getInstance()->getMovieBaseNode()->addChild(background, 0, MOVIE_BLACKSCREEN_TAG);

					auto sequence = Sequence::create(
						FadeIn::create(time*1.0f),
						NULL);
					background->runAction(sequence);

					m_longTimeFlag = GameUtils::millisecondNow();

					setState(STATE_UPDATE);
				}
			}
		}
		break;

	case STATE_UPDATE:
		if((GameUtils::millisecondNow() - m_longTimeFlag) / 1000.0f >= time*1.0f)
		{
			setState(STATE_END);
		}
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCBlackScreenStart::init() {
	// this function has no parameter
	CCAssert(mPara.size() <= 1, "wrong parameter");
}

void SCBlackScreenStart::release() {
}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCBlackScreenEnd)

SCBlackScreenEnd::SCBlackScreenEnd():
m_longTimeFlag(0),
time(0.f)
{
}

SCBlackScreenEnd::~SCBlackScreenEnd()
{
}

void* SCBlackScreenEnd::createInstance()
{
	return new SCBlackScreenEnd() ;
}

void SCBlackScreenEnd::update() {
	switch (mState) {
	case STATE_START:
		{
			// remove the black screen
			auto pBlackScreen = ScriptManager::getInstance()->getMovieBaseNode()->getChildByTag(MOVIE_BLACKSCREEN_TAG);
			if(pBlackScreen != NULL)
			{
				if (mPara.size() == 0)
				{
					pBlackScreen->removeFromParent();

					setState(STATE_END);
				}
				else
				{
					std::string content = mPara.at(0);
					if(mPara.size() == 1)
						time = std::atof(mPara.at(0).c_str()) / 1000.f;

					if (time <= 0.f)
					{
						pBlackScreen->removeFromParent();

						setState(STATE_END);
					}
					else
					{
						auto sequence = Sequence::create(
							FadeOut::create(time*1.0f),
							NULL);
						pBlackScreen->runAction(sequence);

						m_longTimeFlag = GameUtils::millisecondNow();

						setState(STATE_UPDATE);
					}
				}
			}
		}
		break;

	case STATE_UPDATE:
		if((GameUtils::millisecondNow() - m_longTimeFlag) / 1000.0f >= time*1.0f)
		{
			auto pBlackScreen = ScriptManager::getInstance()->getMovieBaseNode()->getChildByTag(MOVIE_BLACKSCREEN_TAG);
			if(pBlackScreen != NULL)
			{
				pBlackScreen->removeFromParent();
			}

			setState(STATE_END);
		}
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCBlackScreenEnd::init() {
	// this function has no parameter
	CCAssert(mPara.size() <= 1, "wrong parameter");
}

void SCBlackScreenEnd::release() {
}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCSimpleDialog)

SCSimpleDialog::SCSimpleDialog()
{
}

SCSimpleDialog::~SCSimpleDialog()
{
}

void* SCSimpleDialog::createInstance()
{
	return new SCSimpleDialog() ;
}

void SCSimpleDialog::update() {
	switch (mState) {
	case STATE_START:
		{
		//CCLOG("excute dialog()");

		std::string content = mPara.at(0);
		float time = 0.f;
		if(mPara.size() == 2)
			time = std::atof(mPara.at(1).c_str()) / 1000.f;

		auto pDialog = StoryDialog::create(content.c_str(), time);
		REGISTER_SCRIPT_COMMAND(pDialog, this->getId())
		ScriptManager::getInstance()->getMovieBaseNode()->addChild(pDialog, 0, MOVIE_DIALOG_TAG);

		setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to end this dialog
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCSimpleDialog::init() {
	CCAssert(mPara.size() <= 2, "wrong parameter");
}

void SCSimpleDialog::release() {
	if(ScriptManager::getInstance()->isInMovieMode())
	{
		auto pDialog = ScriptManager::getInstance()->getMovieBaseNode()->getChildByTag(MOVIE_SIMPLEDIALOG_TAG);
		if(pDialog != NULL)
		{
			pDialog->removeFromParent();
		}
	}
}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCAddScreenEffect)

SCAddScreenEffect::SCAddScreenEffect()
{
}

SCAddScreenEffect::~SCAddScreenEffect()
{
}

void* SCAddScreenEffect::createInstance()
{
	return new SCAddScreenEffect() ;
}

void SCAddScreenEffect::update() {
	switch (mState) {
	case STATE_START:
		{
		std::string effectPathName = mPara.at(0);
		float screenAnchorPointX = std::atof(mPara.at(1).c_str());
		float screenAnchorPointY = std::atof(mPara.at(2).c_str());
		m_effectInstanceId = std::atoi(mPara.at(3).c_str());

		// *.plist
		int suffixIndex = effectPathName.find(".plist");
		if(suffixIndex > 0)
		{
			Size s = Director::getInstance()->getVisibleSize();
			// effect animation
			auto pParticleEffect = ParticleSystemQuad::create(effectPathName.c_str());
			pParticleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
			pParticleEffect->setPosition(Vec2(s.width*screenAnchorPointX,s.height*screenAnchorPointY));
			//pParticleEffect->setScale(2.0f);
			pParticleEffect->setTag(MOVIE_INSTANCE_BASE_TAG + m_effectInstanceId);
			ScriptManager::getInstance()->getMovieBaseNode()->addChild(pParticleEffect, 10);
		}

		//int suffixIndex = effectPathName.find(".anm");

		setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		// wait the player to end this dialog
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCAddScreenEffect::init() {
	CCAssert(mPara.size() == 4, "wrong parameter");
}

void SCAddScreenEffect::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCRemoveScreenEffect)

SCRemoveScreenEffect::SCRemoveScreenEffect()
{
}

SCRemoveScreenEffect::~SCRemoveScreenEffect()
{
}

void* SCRemoveScreenEffect::createInstance()
{
	return new SCRemoveScreenEffect() ;
}

void SCRemoveScreenEffect::update() {
	switch (mState) {
	case STATE_START:
		{
		int effectInstanceId = std::atoi(mPara.at(0).c_str());
		auto pEffectNode = ScriptManager::getInstance()->getMovieBaseNode()->getChildByTag(MOVIE_INSTANCE_BASE_TAG+effectInstanceId);
		if(pEffectNode != NULL)
		{
			pEffectNode->removeFromParent();
		}
		setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		// wait the player to end this dialog
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCRemoveScreenEffect::init() {
	CCAssert(mPara.size() == 1, "wrong parameter");
}

void SCRemoveScreenEffect::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCCreateNPC)

SCCreateNPC::SCCreateNPC()
{
}

SCCreateNPC::~SCCreateNPC()
{
}

void* SCCreateNPC::createInstance()
{
	return new SCCreateNPC() ;
}

void SCCreateNPC::update() {
	switch (mState) {
	case STATE_START:
		{
			// role id
			std::string roleIdStr = mPara.at(0);
			long roleId = std::atoi(roleIdStr.c_str());
			
			// "animation/generals/zsgg_cc"   --> "animation/generals/" and "zsgg_cc"
			std::string resFullPath = mPara.at(1);
			if(resFullPath == STORY_HEROINE_ID)
			{
				int profession = GameView::getInstance()->myplayer->getProfession();
				resFullPath = StoryDialog::getHeroineAnimName(profession);
			}
			int index = resFullPath.find_last_of('/');
			std::string path = resFullPath.substr(0, index+1);
			std::string roleName = resFullPath.substr(index+1);
			
			// position
			int worldX = std::atoi(mPara.at(2).c_str());
			int worldY = std::atoi(mPara.at(3).c_str());
			worldY *= 2;

			// npc name
			std::string name = mPara.at(4);
			if(name == STORY_HEROINE_ID)
			{
				int profession = GameView::getInstance()->myplayer->getProfession();
				name = StoryDialog::getHeroineName(profession);
			}

			// animation dir
			int dir = std::atoi(mPara.at(5).c_str());

			auto scene = GameView::getInstance()->getGameScene();
			if(scene != NULL)
			{
				if(roleIdStr == STORY_HERO_ID)   // "hero"
				{
					BaseFighter* me = (BaseFighter*)scene->getActor(BaseFighter::getMyPlayerId());
					me->setVisible(true);
					me->setAnimDir(dir);
					me->changeAnimation(ANI_STAND, me->getAnimDir(), true, true);
					if(worldX != -1 && worldY != -1)
					{
						me->setWorldPosition(Vec2(worldX,worldY));
					}
				}
				else
				{
					SimpleFighter* pSimpleFighter = new SimpleFighter(scene);
					pSimpleFighter->setAnimDir(dir);
					pSimpleFighter->init(path.c_str(), roleName.c_str());
					pSimpleFighter->setRoleId(MOVIE_BASE_ROLE_ID+roleId);
					pSimpleFighter->setWorldPosition(Vec2(worldX,worldY));
					scene->getMovieActorLayer()->addChild(pSimpleFighter);
					pSimpleFighter->release();

					pSimpleFighter->setActorName(name.c_str());
					pSimpleFighter->showActorName(true);
					scene->putActor(pSimpleFighter->getRoleId(), pSimpleFighter);
				}
			}

			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCCreateNPC::init() {
	CCAssert(mPara.size() == 6, "wrong parameter number");
}

void SCCreateNPC::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCDeleteNPC)

SCDeleteNPC::SCDeleteNPC()
{
}

SCDeleteNPC::~SCDeleteNPC()
{
}

void* SCDeleteNPC::createInstance()
{
	return new SCDeleteNPC() ;
}

void SCDeleteNPC::update() {
	switch (mState) {
	case STATE_START:
		{
			auto scene = GameView::getInstance()->getGameScene();
			if(scene != NULL)
			{
				long roleId = std::atoi(mPara.at(0).c_str());
				scene->removeActor(MOVIE_BASE_ROLE_ID+roleId);
			}
			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCDeleteNPC::init() {
	CCAssert(mPara.size() == 1, "wrong parameter number");
}

void SCDeleteNPC::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCClearScene)

SCClearScene::SCClearScene()
{
}

SCClearScene::~SCClearScene()
{
}

void* SCClearScene::createInstance()
{
	return new SCClearScene() ;
}

void SCClearScene::update() {
	switch (mState) {
	case STATE_START:
		{
			auto scene = GameView::getInstance()->getGameScene();
			if(scene != NULL)
			{
				scene->LoadMovieScene();

				//// hide UI
				//auto pUIScene = (Node*)scene->getMainUIScene();
				//pUIScene->setVisible(false);

				// hide normal actor layer
				//scene->getActorLayer()->setVisible(false);
				ActionInterval *actionHide = (ActionInterval*)Sequence::create(
					FadeOut::create(1.0f),
					CCHide::create(),
					NULL
				);
				scene->getActorLayer()->runAction(actionHide);

				// show movie actor layer
				__LayerRGBA* movieActorLayer = (__LayerRGBA*)scene->getMovieActorLayer();
				movieActorLayer->setVisible(true);
				movieActorLayer->setOpacity(255);

				// move MyPlayer from actor_layer to movie_actor_layer
				BaseFighter* me = (BaseFighter*)scene->getActor(BaseFighter::getMyPlayerId());
				me->removeFromParent();
				//me->setVisible(false);   // default, MyPlayer is not visible unless call CreateNPC( hero, ... )
				scene->initMyPlayer();
				ScriptManager::getInstance()->recordMyPlayerPosition(me->getWorldPosition());
				scene->getMovieActorLayer()->addChild(me);

				// do not show the foot circel
				auto flagNode = me->getChildByTag(GameActor::kTagFootFlag);
				if(flagNode != NULL)
					flagNode->setVisible(false);
			}
			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCClearScene::init() {

}

void SCClearScene::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCResumeScene)

SCResumeScene::SCResumeScene()
{
}

SCResumeScene::~SCResumeScene()
{
}

void* SCResumeScene::createInstance()
{
	return new SCResumeScene() ;
}

void SCResumeScene::update() {
	switch (mState) {
	case STATE_START:
		{
			//auto scene = GameView::getInstance()->getGameScene();
			//if(scene != NULL)
			//{
			//	// show UI
			//	auto pUIScene = (Node*)scene->getMainUIScene();
			//	pUIScene->setVisible(true);
			//}

			ScriptManager::getInstance()->resumeScene();

			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCResumeScene::init() {

}

void SCResumeScene::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCMoveNPC)

SCMoveNPC::SCMoveNPC()
{
}

SCMoveNPC::~SCMoveNPC()
{
}

void* SCMoveNPC::createInstance()
{
	return new SCMoveNPC() ;
}

void SCMoveNPC::update() {
	switch (mState) {
	case STATE_START:
		{
			// role id
			std::string roleIdStr = mPara.at(0);
			long roleId = std::atoi(roleIdStr.c_str());
			// position
			int x = std::atoi(mPara.at(1).c_str());
			int y = std::atoi(mPara.at(2).c_str());
			// convert to world coordinate
			y *= 2;   
			// time
			int time = std::atoi(mPara.at(3).c_str());

			auto scene = GameView::getInstance()->getGameScene();
			if(scene != NULL)
			{
				BaseFighter* pNPC = NULL;
				//SimpleFighter* pNPC = dynamic_cast<SimpleFighter*>(scene->getActor(MOVIE_BASE_ROLE_ID+roleId));
				if(roleIdStr == STORY_HERO_ID)   // "hero"
				{
					pNPC = (BaseFighter*)scene->getActor(BaseFighter::getMyPlayerId());

					Vec2 myPosition = ScriptManager::getInstance()->getRecordMyPlayerPosition();
					if(x == -1 && y == -1*2)
					{
						x = myPosition.x;
						y = myPosition.y;
					}
				}
				else
				{
					pNPC = dynamic_cast<SimpleFighter*>(scene->getActor(MOVIE_BASE_ROLE_ID+roleId));
				}
				if(pNPC != NULL)
				{
					BaseFighterCommandMove* cmd = new BaseFighterCommandMove();
					cmd->targetPosition = Vec2(x, y);
					pNPC->setNextCommand(cmd, false);

					// calc the move speed
					float distance = pNPC->getWorldPosition().getDistance(cmd->targetPosition);
					float speed = distance/(time/1000.f);
					pNPC->setMoveSpeed(speed);
				}
			}
			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCMoveNPC::init() {
	CCAssert(mPara.size() == 4, "wrong parameter number");
}

void SCMoveNPC::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCFlyNPC)

SCFlyNPC::SCFlyNPC()
:m_longTimeFlag(0.0f)
{
}

SCFlyNPC::~SCFlyNPC()
{
}

void* SCFlyNPC::createInstance()
{
	return new SCFlyNPC() ;
}

void SCFlyNPC::update() 
{
	switch (mState) 
	{
	case STATE_START:
		{
			// role id
			long roleId = std::atoi(mPara.at(0).c_str());
			// position
			int x = std::atoi(mPara.at(1).c_str());
			int y = std::atoi(mPara.at(2).c_str());
			// convert to world coordinate
			y *= 2;
			// time
			int time = std::atoi(mPara.at(3).c_str());


			auto scene = GameView::getInstance()->getGameScene();
			if(scene != NULL)
			{
				SimpleFighter* pNPC = dynamic_cast<SimpleFighter*>(scene->getActor(MOVIE_BASE_ROLE_ID+roleId));
				if(pNPC != NULL)
				{
					ActionJumpContext contextTmp;
					contextTmp.startTime = GameUtils::millisecondNow();
					contextTmp.targetPosition = scene->convertToCocos2DSpace(Vec2(x, y));
					contextTmp.jumpDirection = 1;
					contextTmp.jumpHeight = 50;

					//float jumpBackSpeed = 800.f;
					//contextTmp.jumpDuration = pNPC->getWorldPosition().getDistance(contextTmp.targetPosition) / jumpBackSpeed;
					contextTmp.jumpDuration = time / 1000.0f;
					contextTmp.setContext(pNPC);

					//
					pNPC->changeAnimation(MONSTER_ANI_ATTACK, pNPC->getAnimDir(), false, true);

					ActionJumpContext* context = (ActionJumpContext*)pNPC->getActionContext(ACT_JUMP);

					// adjust the direction
					Vec2 targetWorldPosition = pNPC->getGameScene()->convertToGameWorldSpace(context->targetPosition);
					Vec2 offset;
					if(context->jumpDirection == 0)   // back
					{
						offset = GameUtils::getDirection(targetWorldPosition, pNPC->getWorldPosition());
					}
					else   // ahead
					{
						offset = GameUtils::getDirection(pNPC->getWorldPosition(), targetWorldPosition);
					}
					int animDirection = pNPC->getAnimDirection(Vec2(0, 0), Vec2(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);
					//CCAssert(animDirection != -1, "invalid anim dir");
					pNPC->setAnimDir(animDirection);
					pNPC->getAnim()->setAction(animDirection);

					pNPC->runAction(MoveTo::create(context->jumpDuration, context->targetPosition));
					pNPC->getAnim()->runAction(JumpBy::create(context->jumpDuration, Vec2::ZERO, context->jumpHeight, 1));

					CCLegendAnimation* pBody = pNPC->getAnim()->getAnim(ANI_COMPONENT_BODY, MONSTER_ANI_ATTACK);
					std::string animFileName = pBody->getAnimationData()->getAnmFileName();

					auto scene = pNPC->getGameScene();
					for(int i = 1; i < 4; i++)
					{
						SimpleActor* pActor = new SimpleActor();
						pActor->setGameScene(scene);
						pActor->setWorldPosition(pNPC->getWorldPosition());
						pActor->loadAnim(animFileName.c_str(), true);
						scene->getMovieActorLayer()->addChild(pActor, SCENE_ROLE_LAYER_BASE_ZORDER);
						//scene->getActorLayer()->addChild(pActor, SCENE_ROLE_LAYER_BASE_ZORDER);
						pActor->release();
						
						int alpha = 255 - i * context->shadowAlphaStep;
						if(alpha < 0)
						{
							alpha = 0;
						}
							
						pActor->setOpacity(alpha);
						pActor->setColor(context->shadowColor);

						// delay ʱ� �� Ǹ� ���Ծʱ�������
						FiniteTimeAction* simpleActorAction = Sequence::create(
							DelayTime::create(i * time * 0.0001f),
							MoveTo::create(context->jumpDuration, context->targetPosition),
							RemoveSelf::create(),
							NULL);
						pActor->runAction(simpleActorAction);

						FiniteTimeAction* actorJumpAction = Sequence::create(
							DelayTime::create(i * time * 0.0001f),
							JumpBy::create(context->jumpDuration, Vec2::ZERO, context->jumpHeight, 1),
							NULL);
						pActor->getLegendAnim()->runAction(actorJumpAction);
					}

					pNPC->clearPath();
				}
			}

			m_longTimeFlag = GameUtils::millisecondNow();

			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		{
			// role id
			long roleId = std::atoi(mPara.at(0).c_str());

			// time
			int time = std::atoi(mPara.at(3).c_str());

			if(((GameUtils::millisecondNow() - m_longTimeFlag) / 1000.0f) > (time / 1000.0f))
			{
				auto scene = GameView::getInstance()->getGameScene();
				if(scene != NULL)
				{
					SimpleFighter* pNPC = dynamic_cast<SimpleFighter*>(scene->getActor(MOVIE_BASE_ROLE_ID + roleId));
					if(pNPC != NULL)
					{
						pNPC->changeAction(ACT_STAND);
					}
				}

				setState(STATE_END);
			}
		}
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCFlyNPC::init() {
	CCAssert(mPara.size() == 4, "wrong parameter number");
}

void SCFlyNPC::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCSetDir)

SCSetDir::SCSetDir()
{
}

SCSetDir::~SCSetDir()
{
}

void* SCSetDir::createInstance()
{
	return new SCSetDir() ;
}

void SCSetDir::update() {
	switch (mState) {
	case STATE_START:
		{
			// role id
			std::string roleIdStr = mPara.at(0);
			long roleId = std::atoi(mPara.at(0).c_str());
			// dir
			int dir = std::atoi(mPara.at(1).c_str());

			auto scene = GameView::getInstance()->getGameScene();
			if(scene != NULL)
			{
				BaseFighter* pNPC = NULL;
				if(roleIdStr == STORY_HERO_ID)   // "hero"
				{
					pNPC = (BaseFighter*)scene->getActor(BaseFighter::getMyPlayerId());
				}
				else
				{
					pNPC = dynamic_cast<SimpleFighter*>(scene->getActor(MOVIE_BASE_ROLE_ID+roleId));
				}
				if(pNPC != NULL)
				{
					pNPC->setAnimDir(dir);
					pNPC->changeAction(ACT_STAND);
				}
			}
			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCSetDir::init() {
	CCAssert(mPara.size() == 2, "wrong parameter number");
}

void SCSetDir::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCPlayAct)

SCPlayAct::SCPlayAct()
{
}

SCPlayAct::~SCPlayAct()
{
}

void* SCPlayAct::createInstance()
{
	return new SCPlayAct() ;
}

void SCPlayAct::update() {
	switch (mState) {
	case STATE_START:
		{
			// role id
			long roleId = std::atoi(mPara.at(0).c_str());
			// action
			int action = std::atoi(mPara.at(1).c_str());

			auto scene = GameView::getInstance()->getGameScene();
			if(scene != NULL)
			{
				SimpleFighter* pNPC = dynamic_cast<SimpleFighter*>(scene->getActor(MOVIE_BASE_ROLE_ID+roleId));
				if(pNPC != NULL)
				{
					pNPC->changeAction(action);
				}
			}
			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCPlayAct::init() {
	CCAssert(mPara.size() == 2, "wrong parameter number");
}

void SCPlayAct::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCSay)

SCSay::SCSay()
{
}

SCSay::~SCSay()
{
}

void* SCSay::createInstance()
{
	return new SCSay() ;
}

void SCSay::update() {
	switch (mState) {
	case STATE_START:
		{
			// role id
			std::string roleIdStr = mPara.at(0);
			long roleId = std::atoi(mPara.at(0).c_str());
			// dialog text
			std::string content = mPara.at(1);
			// time
			int time = std::atoi(mPara.at(2).c_str());

			auto scene = GameView::getInstance()->getGameScene();
			if(scene != NULL)
			{
				BaseFighter* pNPC = NULL;
				if(roleIdStr == STORY_HERO_ID)   // "hero"
				{
					pNPC = (BaseFighter*)scene->getActor(BaseFighter::getMyPlayerId());
				}
				else
				{
					pNPC = dynamic_cast<SimpleFighter*>(scene->getActor(MOVIE_BASE_ROLE_ID+roleId));
				}
				if(pNPC != NULL)
				{
					pNPC->addChatBubble(content.c_str(), time/1000.f);
				}
			}
			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCSay::init() {
	CCAssert(mPara.size() == 3, "wrong parameter number");
}

void SCSay::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCMoveCam)

SCMoveCam::SCMoveCam()
{
}

SCMoveCam::~SCMoveCam()
{
}

void* SCMoveCam::createInstance()
{
	return new SCMoveCam() ;
}

void SCMoveCam::update() {
	switch (mState) {
	case STATE_START:
		{
			// position
			int x = std::atoi(mPara.at(0).c_str());
			int y = std::atoi(mPara.at(1).c_str());
			y *= 2;
			// time
			int time = std::atoi(mPara.at(2).c_str());

			auto scene = GameView::getInstance()->getGameScene();
			if(scene != NULL)
			{
				GameSceneCamera* pCamera = scene->getSceneCamera();
				if(pCamera->getBehaviour() == GameSceneCamera::camera_behaviour_follow_actor)
				{
					pCamera->setPosition(scene->getActor(BaseFighter::getMyPlayerId())->getPosition());
				}
				if(x == -1 && y == (-1)*2)   // move back to MyPlayer
				{
					Vec2 myPosition = ScriptManager::getInstance()->getRecordMyPlayerPosition();
					pCamera->MoveTo(Vec2(myPosition.x, myPosition.y - SCENE_CAMERA_OFFSET_Y*2), time/1000.f);
				}
				else
					pCamera->MoveTo(Vec2(x,y), time/1000.f);
			}
			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCMoveCam::init() {
	CCAssert(mPara.size() == 3, "wrong parameter number");
}

void SCMoveCam::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCSetSE)

SCSetSE::SCSetSE()
{
}

SCSetSE::~SCSetSE()
{
}

void* SCSetSE::createInstance()
{
	return new SCSetSE() ;
}

void SCSetSE::update() {
	switch (mState) {
	case STATE_START:
		{
			// effect name
			std::string effectPathName = mPara.at(0);

			// position
			int x = std::atoi(mPara.at(1).c_str());
			int y = std::atoi(mPara.at(2).c_str());
			y *= 2;

			auto scene = GameView::getInstance()->getGameScene();
			if(scene != NULL)
			{
				SimpleActor* pActor = new SimpleActor();
				pActor->setGameScene(scene);
				pActor->setWorldPosition(Vec2(x,y));
				pActor->loadAnim(effectPathName.c_str());
				scene->getMovieActorLayer()->addChild(pActor);
				pActor->release();
			}
			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCSetSE::init() {
	CCAssert(mPara.size() == 3, "wrong parameter number");
}

void SCSetSE::release() {

}

/////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCSetSELoop)

SCSetSELoop::SCSetSELoop()
{
}

SCSetSELoop::~SCSetSELoop()
{
}

void* SCSetSELoop::createInstance()
{
	return new SCSetSELoop() ;
}

void SCSetSELoop::update() {
	switch (mState) {
	case STATE_START:
		{
			// effect name
			std::string effectPathName = mPara.at(0);

			// position
			int x = std::atoi(mPara.at(1).c_str());
			int y = std::atoi(mPara.at(2).c_str());
			y *= 2;

			auto scene = GameView::getInstance()->getGameScene();
			if(scene != NULL)
			{
				SimpleActor* pActor = new SimpleActor();
				pActor->setGameScene(scene);
				pActor->setWorldPosition(Vec2(x,y));
				pActor->loadAnim(effectPathName.c_str(), true);
				scene->getMovieActorLayer()->addChild(pActor);
				pActor->release();
			}
			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCSetSELoop::init() {
	CCAssert(mPara.size() == 3, "wrong parameter number");
}

void SCSetSELoop::release() {

}