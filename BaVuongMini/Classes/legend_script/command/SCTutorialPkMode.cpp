#include "SCTutorialPkMode.h"
#include "../../utils/GameUtils.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/sceneelement/MyHeadInfo.h"

IMPLEMENT_CLASS(SCTutorialPkMode1)

SCTutorialPkMode1::SCTutorialPkMode1()
{
}

SCTutorialPkMode1::~SCTutorialPkMode1()
{
}

void* SCTutorialPkMode1::createInstance()
{
	return new SCTutorialPkMode1() ;
}

void SCTutorialPkMode1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialPkMode1()");

			std::string content = mPara.at(0);

			auto myHeadInfo = GameView::getInstance()->getMainUIScene()->curMyHeadInfo;
		
			if(GameView::getInstance()->myplayer->getActiveRole()->level() < myHeadInfo->pkModeOpenLevel)
			{
				setState(STATE_START);
				return;
			}

			myHeadInfo->addCCTutorialIndicator1(content.c_str(),myHeadInfo->getPosition(),CCTutorialIndicator::Direction_LU);
			//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
			myHeadInfo->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(myHeadInfo->Button_state) ;
			setState(STATE_UPDATE);

		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialPkMode1::init() {
	CCLOG("SCTutorialPkMode1() init");
}

void SCTutorialPkMode1::release() {
	CCLOG("SCTutorialPkMode1() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto myHeadInfo = GameView::getInstance()->getMainUIScene()->curMyHeadInfo;
	if(myHeadInfo != NULL)
		myHeadInfo->removeCCTutorialIndicator();
}
