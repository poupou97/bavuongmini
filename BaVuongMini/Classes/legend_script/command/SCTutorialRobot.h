#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALROBOT_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALROBOT_H_

#include "../Script.h"

/**
 * �佫�����ѧ
 *
 * @author yangjun
 * @date 2014-1-7
 */
class SCTutorialRobotStep1 : public Script {
private:
    DECLARE_CLASS(SCTutorialRobotStep1)

public:
	SCTutorialRobotStep1();
	virtual ~SCTutorialRobotStep1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////////////////

class SCTutorialRobotStep2 : public Script {
private:
    DECLARE_CLASS(SCTutorialRobotStep2)

public:
	SCTutorialRobotStep2();
	virtual ~SCTutorialRobotStep2();

	virtual bool isBlockFunction() { return true; };

	virtual void endCommand(Ref* pSender);

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif