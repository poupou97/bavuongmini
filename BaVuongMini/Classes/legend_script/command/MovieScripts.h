#ifndef _LEGEND_SCRIPT_MOVIE_COMMANDS_H_
#define _LEGEND_SCRIPT_MOVIE_COMMANDS_H_

#include "../Script.h"


// 剧情脚本 Example:
/**
// 开启屏幕上下的黑边
MovieStart()

// content, head icon, left or right, head name
Dialog(哈哈，少侠来得正好!, npc/LiuBei.png, 0,刘备)

// 开启黑屏
BlackScreenStart()

// 屏幕中心出现简单对话
SimpleDialog(突然，远处传来一声女子的/#ff0000 尖叫/！)

// 关闭黑屏
BlackScreenEnd()

// 在屏幕上增加效果 ( 特效名, 屏幕相对坐标x, 屏幕相对坐标y, id )
AddScreenEffect(animation/texiao/particledesigner/taohua3.plist, 0.25, 1.0, 1)

// 移除之前添加的屏幕效果，指定id
RemoveScreenEffect(1)

// 关闭屏幕上下的黑边
MovieEnd()
**/

#define MOVIE_BLACKBANK_TAG 201
#define MOVIE_DIALOG_TAG 202
#define MOVIE_DIALOGPASSLAYER_TAG 203
#define MOVIE_BLACKSCREEN_TAG 204
#define MOVIE_SIMPLEDIALOG_TAG 205

#define MOVIE_BLACKBANK_ZORDER 0
#define MOVIE_DIALOG_ZORDER 1
#define MOVIE_DIALOGPASSLAYER_ZORDER 2

/**
 * 剧情对话
 *
 * @author zhaogang
 * @date 2014/1/4
 */
class SCDialog : public Script {
private:
    DECLARE_CLASS(SCDialog)

public:
	SCDialog();
	virtual ~SCDialog();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * 开启屏幕上下的黑边
 *
 * @author zhaogang
 * @date 2014/1/4
 */
class SCMovieStart : public Script{
private:
	DECLARE_CLASS(SCMovieStart)

public:
	SCMovieStart();
	virtual ~SCMovieStart();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();

private:
	void storyBlackBandActionCallBack(Ref* object);

	long long m_longTimeFlag;
};

////////////////////////////////////////////////////

/**
 * 关闭屏幕上下的黑边
 *
 * @author zhaogang
 * @date 2014/1/4
 */
class SCMovieEnd : public Script {
private:
    DECLARE_CLASS(SCMovieEnd)

public:
	SCMovieEnd();
	virtual ~SCMovieEnd();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * 开启黑屏
 *
 * @author zhaogang
 * @date 2014/3/19
 */
class SCBlackScreenStart : public Script {
private:
    DECLARE_CLASS(SCBlackScreenStart)

public:
	SCBlackScreenStart();
	virtual ~SCBlackScreenStart();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();

private:
	long long m_longTimeFlag;
	float time;
};

////////////////////////////////////////////////////

/**
 * 关闭黑屏
 *
 * @author zhaogang
 * @date 2014/3/19
 */
class SCBlackScreenEnd : public Script {
private:
    DECLARE_CLASS(SCBlackScreenEnd)

public:
	SCBlackScreenEnd();
	virtual ~SCBlackScreenEnd();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();

private:
	long long m_longTimeFlag;
	float time;
};

////////////////////////////////////////////////////

/**
 * 简单对话
 * 该对话位于屏幕中心，没有其他的样式
 * @author zhaogang
 * @date 2014/3/19
 */
class SCSimpleDialog : public Script {
private:
    DECLARE_CLASS(SCSimpleDialog)

public:
	SCSimpleDialog();
	virtual ~SCSimpleDialog();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * 增加屏幕效果
 * 比如粒子特效等
 * @author zhaogang
 * @date 2014/3/19
 */
class SCAddScreenEffect : public Script {
private:
    DECLARE_CLASS(SCAddScreenEffect)

public:
	SCAddScreenEffect();
	virtual ~SCAddScreenEffect();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();

private:
	int m_effectInstanceId;
};

////////////////////////////////////////////////////

/**
 * 移除指定的屏幕效果
 * @author zhaogang
 * @date 2014/3/19
 */
class SCRemoveScreenEffect : public Script {
private:
    DECLARE_CLASS(SCRemoveScreenEffect)

public:
	SCRemoveScreenEffect();
	virtual ~SCRemoveScreenEffect();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * 创建剧情角色对象
 * @author zhaogang
 * @date 2014/3/20
 */
class SCCreateNPC : public Script {
private:
    DECLARE_CLASS(SCCreateNPC)

public:
	SCCreateNPC();
	virtual ~SCCreateNPC();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * 删除剧情角色对象
 * @author zhaogang
 * @date 2014/3/20
 */
class SCDeleteNPC : public Script {
private:
    DECLARE_CLASS(SCDeleteNPC)

public:
	SCDeleteNPC();
	virtual ~SCDeleteNPC();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * 清除游戏场景
 * 不显示当前场景中的其他角色，只显示新创建的剧情角色
 * @author zhaogang
 * @date 2014/3/20
 */
class SCClearScene : public Script {
private:
    DECLARE_CLASS(SCClearScene)

public:
	SCClearScene();
	virtual ~SCClearScene();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * 恢复游戏场景，清除剧情场景
 * @author zhaogang
 * @date 2014/3/20
 */
class SCResumeScene : public Script {
private:
    DECLARE_CLASS(SCClearScene)

public:
	SCResumeScene();
	virtual ~SCResumeScene();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * 剧情角色的移动
 * @author zhaogang
 * @date 2014/3/20
 */
class SCMoveNPC : public Script {
private:
    DECLARE_CLASS(SCMoveNPC)

public:
	SCMoveNPC();
	virtual ~SCMoveNPC();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * let the role flying
 * @author liuliang
 * @date 2014/9/10
 */
class SCFlyNPC : public Script {
private:
    DECLARE_CLASS(SCFlyNPC)

public:
	SCFlyNPC();
	virtual ~SCFlyNPC();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();

private:
	long long m_longTimeFlag;
};

////////////////////////////////////////////////////

/**
 * 设置剧情角色的动画朝向
 * @author zhaogang
 * @date 2014/5/12
 */
class SCSetDir : public Script {
private:
    DECLARE_CLASS(SCSetDir)

public:
	SCSetDir();
	virtual ~SCSetDir();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * 剧情角色执行动作（比如攻击动作等）
 * @author zhaogang
 * @date 2014/3/21
 */
class SCPlayAct : public Script {
private:
    DECLARE_CLASS(SCPlayAct)

public:
	SCPlayAct();
	virtual ~SCPlayAct();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * 剧情角色的头顶对话
 * @author zhaogang
 * @date 2014/3/21
 */
class SCSay : public Script {
private:
    DECLARE_CLASS(SCSay)

public:
	SCSay();
	virtual ~SCSay();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * 移动镜头
 * @author zhaogang
 * @date 2014/3/21
 */
class SCMoveCam : public Script {
private:
    DECLARE_CLASS(SCMoveCam)

public:
	SCMoveCam();
	virtual ~SCMoveCam();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * 增加场景特效
 * 非屏幕特效，不同于AddScreenEffect
 * @author zhaogang
 * @date 2014/3/21
 */
class SCSetSE : public Script {
private:
    DECLARE_CLASS(SCSetSE)

public:
	SCSetSE();
	virtual ~SCSetSE();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////

/**
 * the animation is loop
 * @author zhaogang
 * @date 2014/11/10
 */
class SCSetSELoop : public Script {
private:
    DECLARE_CLASS(SCSetSELoop)

public:
	SCSetSELoop();
	virtual ~SCSetSELoop();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


#endif