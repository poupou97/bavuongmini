#include "SCTutorialEquip.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../ui/equipMent_ui/GeneralEquipMent.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../ui/backpackscene/PacPageView.h"
#include "../../ui/backpackscene/PackageItem.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../ui/backpackscene/PackageItemInfo.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../gamescene_state/role/MyPlayer.h"


IMPLEMENT_CLASS(SCTutorialEquip1)

SCTutorialEquip1::SCTutorialEquip1()
{
}

SCTutorialEquip1::~SCTutorialEquip1()
{
}

void* SCTutorialEquip1::createInstance()
{
	return new SCTutorialEquip1() ;
}

void SCTutorialEquip1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialEquip1()");
			
			auto panckScene_ = (PackageScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
			std::string content = mPara.at(0);
			bool isExit = false;
			std::string profesionStr_  = GameView::getInstance()->myplayer->getActiveRole()->profession();
			int selfProfession_ = BasePlayer::getProfessionIdxByName(profesionStr_);
			for (int i=0;i<GameView::getInstance()->AllPacItem.size();i++)
			{
				if (GameView::getInstance()->AllPacItem.at(i)->goods().equipmentclazz() == 1 &&
							GameView::getInstance()->AllPacItem.at(i)->goods().quality() == 3 &&
							GameView::getInstance()->AllPacItem.at(i)->goods().equipmentdetail().profession() == selfProfession_ && 
							GameView::getInstance()->AllPacItem.at(i)->goods().equipmentdetail().usergrade() == 15)
				{
					int index_ = GameView::getInstance()->AllPacItem.at(i)->id();
					int curPage_ = index_/16;
					panckScene_->pacPageView->getPageView()->scrollToPage(curPage_);
					panckScene_->CurrentPageViewChanged(panckScene_->pacPageView, PageView::EventType::TURNING);

					packageItem = GameView::getInstance()->pacPageView->getPackageItem(index_);
					packageItem->addCCTutorialIndicator(content.c_str(),packageItem->getPosition(),CCTutorialIndicator::Direction_RD);
					packageItem->registerScriptCommand(this->getId());
					//GameView::getInstance()->showAlertDialog(GameView::getInstance()->AllPacItem.at(i)->goods().name());
					//Btn_pacItemFrame
					this->setCommandHandler(packageItem->Btn_pacItemFrame);
					isExit  = true;
					setState(STATE_UPDATE);
					break;
				}
			}	
			if (isExit == false)
			{
				setState(STATE_END);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquip1::init() {
	CCLOG("SCTutorialRobotStep1() init");
}

void SCTutorialEquip1::release() {
	CCLOG("SCTutorialEquip1() release");

	if (packageItem != NULL)
	{
		packageItem->removeCCTutorialIndicator();
	}
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialEquip2)

SCTutorialEquip2::SCTutorialEquip2()
{
}

SCTutorialEquip2::~SCTutorialEquip2()
{
}

void* SCTutorialEquip2::createInstance()
{
	return new SCTutorialEquip2() ;
}

void SCTutorialEquip2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialEquip2()");

			std::string content = mPara.at(0);

			auto packIteminfo =(PackageItemInfo *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase);
			if (packIteminfo != NULL)
			{
				packIteminfo->addCCTutorialIndicator(content.c_str(),packIteminfo->Button_dressOn->getPosition(),CCTutorialIndicator::Direction_LD);
				packIteminfo->registerScriptCommand(this->getId());
				this->setCommandHandler(packIteminfo->Button_dressOn);

				setState(STATE_UPDATE);
			}else
			{
				setState(STATE_END);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquip2::init() {
	CCLOG("SCTutorialEquip2() init");
}

void SCTutorialEquip2::release() {
	CCLOG("SCTutorialEquip2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto packIteminfo =(PackageItemInfo *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase);
	if (packIteminfo != NULL)
	{
		packIteminfo->removeCCTutorialIndicator();
	}
}

void SCTutorialEquip2::endCommand( Ref* pSender )
{
	Script::endCommand(pSender);
	release();
}




////////////////////////////
IMPLEMENT_CLASS(SCTutorialEquip3)

SCTutorialEquip3::SCTutorialEquip3()
{
}

SCTutorialEquip3::~SCTutorialEquip3()
{
}

void* SCTutorialEquip3::createInstance()
{
	return new SCTutorialEquip3() ;
}

void SCTutorialEquip3::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep1()");

			std::string content = mPara.at(0);

			auto packageui = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
			if (packageui != NULL)
			{
				packageui->addCCTutorialIndicator(content.c_str(),packageui->Button_close->getPosition(),CCTutorialIndicator::Direction_RU);
				packageui->registerScriptCommand(this->getId());
				this->setCommandHandler(packageui->Button_close);
				setState(STATE_UPDATE);
			}else
			{
				setState(STATE_END);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquip3::init() {
	CCLOG("SCTutorialEquip3() init");
}

void SCTutorialEquip3::release() {
	CCLOG("SCTutorialEquip3() release");
	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto packageui = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
	if (packageui != NULL)
	{
		packageui->removeCCTutorialIndicator();
	}

}

void SCTutorialEquip3::endCommand( Ref* pSender )
{
	Script::endCommand(pSender);
	release();
}

