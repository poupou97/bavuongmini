#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALPKMODE_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALPKMODE_H_

#include "../Script.h"

/**
 *������ѧ
 *
 * @author yangjun
 * @date 2014-3-10
 */

//////////////////////pkģʽ////////////////////////
class SCTutorialPkMode1 : public Script {
private:
	DECLARE_CLASS(SCTutorialPkMode1)

public:
	SCTutorialPkMode1();
	virtual ~SCTutorialPkMode1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif
