#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALSTRENGTH_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALSTRENGTH_H_

#include "../Script.h"

/**
 * װ��ǿ�������ѧ
 */
class SCTutorialEquipmentStep1 : public Script {
private:
    DECLARE_CLASS(SCTutorialEquipmentStep1)

public:
	SCTutorialEquipmentStep1();
	virtual ~SCTutorialEquipmentStep1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////////////////
class SCTutorialEquipmentStep2 : public Script {
private:
    DECLARE_CLASS(SCTutorialEquipmentStep2)

public:
	SCTutorialEquipmentStep2();
	virtual ~SCTutorialEquipmentStep2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void endCommand(Ref* pSender);

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////////////////

class SCTutorialEquipMentStep3 : public Script {
private:
	DECLARE_CLASS(SCTutorialEquipMentStep3)

public:
	SCTutorialEquipMentStep3();
	virtual ~SCTutorialEquipMentStep3();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////////////////

class SCTutorialEquipMentStep4: public Script {
private:
	DECLARE_CLASS(SCTutorialEquipMentStep4)

public:
	SCTutorialEquipMentStep4();
	virtual ~SCTutorialEquipMentStep4();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////////////////

class SCTutorialEquipMentStep5 : public Script {
private:
	DECLARE_CLASS(SCTutorialEquipMentStep5)

public:
	SCTutorialEquipMentStep5();
	virtual ~SCTutorialEquipMentStep5();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void endCommand(Ref* pSender);

	virtual void update();

	virtual void init();

	virtual void release();
};
#endif