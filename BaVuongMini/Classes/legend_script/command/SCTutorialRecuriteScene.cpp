#include "SCTutorialRecuriteScene.h"
#include "../../utils/GameUtils.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/sceneelement/MyHeadInfo.h"
#include "../../ui/extensions/RecruitGeneralCard.h"

IMPLEMENT_CLASS(SCTutorialRecuriteScene)

SCTutorialRecuriteScene::SCTutorialRecuriteScene()
{
}

SCTutorialRecuriteScene::~SCTutorialRecuriteScene()
{
}

void* SCTutorialRecuriteScene::createInstance()
{
	return new SCTutorialRecuriteScene() ;
}

void SCTutorialRecuriteScene::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialRecuriteScene()");

			std::string content = mPara.at(0);

			if (!GameView::getInstance()->getMainUIScene())
			{
				setState(STATE_START);
				return;
			}

			auto recruiteCard = (RecruitGeneralCard*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRecuriteGeneralCardUI);
			if (!recruiteCard)
			{
				setState(STATE_START);
				return;
			}
			else
			{
				recruiteCard->isRunTutorials = true;
				if (!recruiteCard->isFinishedAction())
				{
					setState(STATE_START);
					return;
				}
			}

			recruiteCard->addCCTutorialIndicator1(content.c_str(),recruiteCard->getSecondButton()->getPosition(),CCTutorialIndicator::Direction_LD);
			//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
			recruiteCard->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(recruiteCard->getSecondButton()) ;
			setState(STATE_UPDATE);

		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialRecuriteScene::init() {
	CCLOG("SCTutorialRecuriteScene() init");
}

void SCTutorialRecuriteScene::release() {
	CCLOG("SCTutorialRecuriteScene() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto recruiteCard = (RecruitGeneralCard*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRecuriteGeneralCardUI);
	if (recruiteCard)
	{
		recruiteCard->isRunTutorials = false;
		recruiteCard->removeCCTutorialIndicator();
	}
}
