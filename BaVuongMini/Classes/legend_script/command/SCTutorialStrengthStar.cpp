#include "SCTutorialStrengthStar.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../ui/equipMent_ui/GeneralEquipMent.h"
#include "../../messageclient/element/CEquipment.h"
#include "../../messageclient/element/FolderInfo.h"
#include "../../ui/extensions/UITab.h"

IMPLEMENT_CLASS(SCTutorialEquipmentStarStep1)

SCTutorialEquipmentStarStep1::SCTutorialEquipmentStarStep1()
{
}

SCTutorialEquipmentStarStep1::~SCTutorialEquipmentStarStep1()
{
}

void* SCTutorialEquipmentStarStep1::createInstance()
{
	return new SCTutorialEquipmentStarStep1() ;
}

void SCTutorialEquipmentStarStep1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialMainSceneSetStrength1()");
			
			std::string content = mPara.at(0);
			
			if (GameView::getInstance()->EquipListItem.size()<=0)
			{
				setState(STATE_END);
			}else
			{
				auto equipUi =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
				if (equipUi != NULL)
				{
					equipUi->addCCTutorialIndicator2(content.c_str(),equipUi->equipTab->getPosition(),CCTutorialIndicator::Direction_LU);
					equipUi->registerScriptCommand(this->getId()) ; 
					this->setCommandHandler(equipUi->equipTab->getObjectByIndex(1)) ;
					setState(STATE_UPDATE);
				}
			}
			
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquipmentStarStep1::init() {
	CCLOG("SCTutorialMainSceneSetStrength1() init");
}

void SCTutorialEquipmentStarStep1::release() {
	CCLOG("SCTutorialMainSceneSetStrength1() release");

	if (GameView::getInstance()->EquipListItem.size()<=0 )
	{
		return;
	}else
	{
		if (!GameView::getInstance()->getGameScene())
			return;

		if (!GameView::getInstance()->getMainUIScene())
			return;

		auto equipUi =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		if (equipUi != NULL)
		{
			equipUi->removeCCTutorialIndicator();
		}
	
	}
}

/////////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialEquipmentStarStep2)

SCTutorialEquipmentStarStep2::SCTutorialEquipmentStarStep2()
{
}

SCTutorialEquipmentStarStep2::~SCTutorialEquipmentStarStep2()
{
}

void* SCTutorialEquipmentStarStep2::createInstance()
{
	return new SCTutorialEquipmentStarStep2() ;
}

void SCTutorialEquipmentStarStep2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialEquipmentStarStep2()");

			std::string content = mPara.at(0);

			if (GameView::getInstance()->EquipListItem.size()<=0)
			{
				setState(STATE_END);
			}else
			{
				auto equipUi =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
				if (equipUi != NULL)
				{
					auto generalEquip =(GeneralEquipMent *)equipUi->roleLayer->getChildByTag(equipUi->generalWeapTag);
					if (generalEquip != NULL)
					{
						generalEquip->addCCTutorialIndicator(content.c_str(),Vec2(0,0),CCTutorialIndicator::Direction_RD);
						generalEquip->registerScriptCommand(this->getId());

						this->setCommandHandler(generalEquip->Btn_pacItemFrame);
						setState(STATE_UPDATE);
					}
				}
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquipmentStarStep2::init() {
}

void SCTutorialEquipmentStarStep2::release() {

	
	if (GameView::getInstance()->EquipListItem.size()<=0 )
	{
		return;
	}else
	{
		if (!GameView::getInstance()->getGameScene())
			return;

		if (!GameView::getInstance()->getMainUIScene())
			return;

		auto equipUi =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		if (equipUi != NULL)
		{
			auto generalEquip =(GeneralEquipMent *)equipUi->roleLayer->getChildByTag(equipUi->generalWeapTag);
			if (generalEquip != NULL)
			{
				generalEquip->removeCCTutorialIndicator();
			}
		}
	}
}

void SCTutorialEquipmentStarStep2::endCommand( Ref* pSender )
{
	Script::endCommand(pSender);
	release();
}

//////////////////////////////////////////////////////////////////////


IMPLEMENT_CLASS(SCTutorialEquipmentStarStep3)

SCTutorialEquipmentStarStep3::SCTutorialEquipmentStarStep3()
{
}

SCTutorialEquipmentStarStep3::~SCTutorialEquipmentStarStep3()
{
}

void* SCTutorialEquipmentStarStep3::createInstance()
{
	return new SCTutorialEquipmentStarStep3() ;
}

void SCTutorialEquipmentStarStep3::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);

			if (GameView::getInstance()->EquipListItem.size()<=0)
			{
				setState(STATE_END);
			}else
			{
				GeneralEquipMentInfo * equipInfo =(GeneralEquipMentInfo *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase);
				if (equipInfo != NULL)
				{
					equipInfo->addCCTutorialIndicator(content.c_str(),equipInfo->btn_addMainEquip->getPosition(),CCTutorialIndicator::Direction_LD);
					equipInfo->registerScriptCommand(this->getId());
					this->setCommandHandler(equipInfo->btn_addMainEquip);

					setState(STATE_UPDATE);
				}else
				{
					setState(STATE_END);
				}
			}

		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquipmentStarStep3::init() {
}

void SCTutorialEquipmentStarStep3::release() {

	if (GameView::getInstance()->EquipListItem.size()<=0)
	{
		return;
	}else
	{
		GeneralEquipMentInfo * equipInfo =(GeneralEquipMentInfo *) GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoodsItemInfoBase);
		if (equipInfo != NULL)
		{
			equipInfo->removeCCTutorialIndicator();
		}
	}
}

void SCTutorialEquipmentStarStep3::endCommand( Ref* pSender )
{
	Script::endCommand(pSender);
	release();
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialEquipMentStarStep4)

SCTutorialEquipMentStarStep4::SCTutorialEquipMentStarStep4()
{
}

SCTutorialEquipMentStarStep4::~SCTutorialEquipMentStarStep4()
{
}

void* SCTutorialEquipMentStarStep4::createInstance()
{
	return new SCTutorialEquipMentStarStep4() ;
}

void SCTutorialEquipMentStarStep4::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);

			if (GameView::getInstance()->EquipListItem.size()<=0)
			{
				setState(STATE_END);
			}else
			{
				auto equipUi =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
				if (equipUi != NULL)
				{
					if (equipUi->starMainEquip==true)
					{
						equipUi->addCCTutorialIndicator(content.c_str(),equipUi->btn_allStar->getPosition(),CCTutorialIndicator::Direction_LD);
						equipUi->registerScriptCommand(this->getId());
						this->setCommandHandler(equipUi->btn_allStar);
						setState(STATE_UPDATE);
					}
				}else
				{
					setState(STATE_END);
				}
			}
			
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquipMentStarStep4::init() {
}

void SCTutorialEquipMentStarStep4::release() {
	
	if (GameView::getInstance()->EquipListItem.size()<=0 )
	{
		return;
	}else
	{
		if (!GameView::getInstance()->getGameScene())
			return;

		if (!GameView::getInstance()->getMainUIScene())
			return;

		auto equipInfo =(EquipMentUi *) GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		if (equipInfo != NULL)
		{
			equipInfo->removeCCTutorialIndicator();
		}
	}
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialEquipMentStarStep5)

SCTutorialEquipMentStarStep5::SCTutorialEquipMentStarStep5()
{
}

SCTutorialEquipMentStarStep5::~SCTutorialEquipMentStarStep5()
{
}

void* SCTutorialEquipMentStarStep5::createInstance()
{
	return new SCTutorialEquipMentStarStep5() ;
}

void SCTutorialEquipMentStarStep5::update() {
	switch (mState) {
	case STATE_START:
		{

			std::string content = mPara.at(0);
			
			if (GameView::getInstance()->EquipListItem.size()<=0)
			{
				setState(STATE_END);
			}else
			{
				auto equipUi =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
				if (equipUi != NULL)
				{
					equipUi->addCCTutorialIndicator(content.c_str(),equipUi->btn_upStar->getPosition(),CCTutorialIndicator::Direction_LD);
					equipUi->registerScriptCommand(this->getId());
					this->setCommandHandler(equipUi->btn_upStar);

					setState(STATE_UPDATE);
				}
				
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquipMentStarStep5::init() {
}

void SCTutorialEquipMentStarStep5::release() {

	if (GameView::getInstance()->EquipListItem.size()<=0 )
	{
		return;
	}else
	{
		if (!GameView::getInstance()->getGameScene())
			return;

		if (!GameView::getInstance()->getMainUIScene())
			return;

		auto equipUi =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		if (equipUi != NULL)
		{
			equipUi->removeCCTutorialIndicator();
		}
	}
}
///////////////////////////////////

IMPLEMENT_CLASS(SCTutorialEquipMentStarStep6)

SCTutorialEquipMentStarStep6::SCTutorialEquipMentStarStep6()
{
}

SCTutorialEquipMentStarStep6::~SCTutorialEquipMentStarStep6()
{
}

void* SCTutorialEquipMentStarStep6::createInstance()
{
	return new SCTutorialEquipMentStarStep6() ;
}

void SCTutorialEquipMentStarStep6::update() {
	switch (mState) {
	case STATE_START:
		{
			std::string content = mPara.at(0);
			if (GameView::getInstance()->EquipListItem.size()<=0)
			{
				setState(STATE_END);
			}else
			{
				auto equipUi =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
				if (equipUi != NULL)
				{
					equipUi->addCCTutorialIndicatorClose(content.c_str(),equipUi->btn_close->getPosition(),CCTutorialIndicator::Direction_RU);
					equipUi->registerScriptCommand(this->getId());
					this->setCommandHandler(equipUi->btn_close);

					setState(STATE_UPDATE);
				}
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialEquipMentStarStep6::init() {
}

void SCTutorialEquipMentStarStep6::release() {

	
	if (GameView::getInstance()->EquipListItem.size()<=0)
	{
		return;
	}else
	{
		if (!GameView::getInstance()->getGameScene())
			return;

		if (!GameView::getInstance()->getMainUIScene())
			return;

		auto equipUi =(EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		if (equipUi != NULL)
		{
			equipUi->removeCCTutorialIndicator();
		}
	}
}