#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALEQUIPMENT_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALEQUIPMENT_H_

#include "../Script.h"
#include "../../ui/backpackscene/PackageItem.h"

/**
 * ��װ����ѧ
 */
class SCTutorialEquip1 : public Script {
private:
    DECLARE_CLASS(SCTutorialEquip1)

public:
	SCTutorialEquip1();
	virtual ~SCTutorialEquip1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
private:
	PackageItem * packageItem;
};

//////////////////////////////////////////////////////

class SCTutorialEquip2 : public Script {
private:
    DECLARE_CLASS(SCTutorialEquip2)

public:
	SCTutorialEquip2();
	virtual ~SCTutorialEquip2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void endCommand(Ref* pSender);

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////////////////

class SCTutorialEquip3 : public Script {
private:
	DECLARE_CLASS(SCTutorialEquip3)

public:
	SCTutorialEquip3();
	virtual ~SCTutorialEquip3();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void endCommand(Ref* pSender);

	virtual void update();

	virtual void init();

	virtual void release();
};
#endif