#include "SCTutorialAnswerQuestion.h"
#include "../../utils/GameUtils.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/sceneelement/MyHeadInfo.h"
#include "../../messageclient/protobuf/PlayerMessage.pb.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../ui/Active_ui/ActiveData.h"
#include "../../messageclient/element/CActiveLabel.h"
#include "../../ui/Active_ui/ActiveLabelData.h"
#include "../../ui/Active_ui/ActiveUI.h"

IMPLEMENT_CLASS(SCTutorialBtnActivity)

SCTutorialBtnActivity::SCTutorialBtnActivity()
{
}

SCTutorialBtnActivity::~SCTutorialBtnActivity()
{
}

void* SCTutorialBtnActivity::createInstance()
{
	return new SCTutorialBtnActivity() ;
}

void SCTutorialBtnActivity::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialPkMode1()");

			std::string content = mPara.at(0);

			if (GameView::getInstance()->getMapInfo()->maptype() >= com::future::threekingdoms::server::transport::protocol::coliseum)
			{
				setState(STATE_END);
				return;
			}

			auto guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			if (guideMap)
			{
				if (!guideMap->getActionLayer()->isVisible())
				{
					setState(STATE_END);
					return;
				}
			}

			auto btn_activity = (Widget*)guideMap->getActionLayer()->getChildByName("btn_activeDegree");
			if (!btn_activity)
			{
				setState(STATE_END);
				return;
			}

			bool b_can = false;
			for(int i = 0;i<ActiveData::instance()->m_vector_native_activeLabel.size();i++)
			{
				auto temp = (CActiveLabel*)ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel;
				if (temp->activetype() == 5) //���
				{
					if (temp->partakeactivealready() < temp->partakeactiveall()) //⻹δ��
					{
						b_can = true;
						break;
					}
				}
			}
			if (!b_can)
			{
				setState(STATE_END);
				return;
			}

			guideMap->addCCTutorialIndicator1(content.c_str(),btn_activity->getPosition(),CCTutorialIndicator::Direction_RU);
			guideMap->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(btn_activity) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialBtnActivity::init() {
	CCLOG("SCTutorialBtnActivity() init");
}

void SCTutorialBtnActivity::release() {
	CCLOG("SCTutorialBtnActivity() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (guideMap)
	{
		guideMap->removeCCTutorialIndicator();
	}
}


//////////////////////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialBeginAnswer)

SCTutorialBeginAnswer::SCTutorialBeginAnswer()
{
}

SCTutorialBeginAnswer::~SCTutorialBeginAnswer()
{
}

void* SCTutorialBeginAnswer::createInstance()
{
	return new SCTutorialBeginAnswer() ;
}

void SCTutorialBeginAnswer::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialBeginAnswer()");

			std::string content = mPara.at(0);

			if (GameView::getInstance()->getMapInfo()->maptype() >= com::future::threekingdoms::server::transport::protocol::coliseum)
			{
				setState(STATE_END);
				return;
			}

			auto guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			if (guideMap)
			{
				if (!guideMap->getActionLayer()->isVisible())
				{
					setState(STATE_END);
					return;
				}
			}

			bool b_can = false;
			for(int i = 0;i<ActiveData::instance()->m_vector_native_activeLabel.size();i++)
			{
				auto temp = (CActiveLabel*)ActiveData::instance()->m_vector_native_activeLabel.at(i)->m_activeLabel;
				if (temp->activetype() == 5) //ɴ��
				{
					if (temp->partakeactivealready() < temp->partakeactiveall()) //⻹δ��
					{
						b_can = true;
						break;
					}
				}
			}
			if (!b_can)
			{
				setState(STATE_END);
				return;
			}

			auto activeUI = (ActiveUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
			if (!activeUI)
			{
				setState(STATE_START);
				return;
			}

			activeUI->addCCTutorialIndicator(content.c_str(),Vec2(0,0),CCTutorialIndicator::Direction_LD,1);
			activeUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(activeUI) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialBeginAnswer::init() {
	CCLOG("SCTutorialBeginAnswer() init");
}

void SCTutorialBeginAnswer::release() {
	CCLOG("SCTutorialBeginAnswer() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (guideMap)
	{
		guideMap->removeCCTutorialIndicator();
	}
}
