#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALSTRENGTH_STAR_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALSTRENGTH_STAR_H_

#include "../Script.h"

/**
 * װ��ǿ���������ǽ�ѧ
 */
class SCTutorialEquipmentStarStep1 : public Script {
private:
    DECLARE_CLASS(SCTutorialEquipmentStarStep1)

public:
	SCTutorialEquipmentStarStep1();
	virtual ~SCTutorialEquipmentStarStep1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////////
class SCTutorialEquipmentStarStep2 : public Script {
private:
	DECLARE_CLASS(SCTutorialEquipmentStarStep2)

public:
	SCTutorialEquipmentStarStep2();
	virtual ~SCTutorialEquipmentStarStep2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void endCommand(Ref* pSender);

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////////////

class SCTutorialEquipmentStarStep3: public Script {
private:
	DECLARE_CLASS(SCTutorialEquipmentStarStep3)

public:
	SCTutorialEquipmentStarStep3();
	virtual ~SCTutorialEquipmentStarStep3();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void endCommand(Ref* pSender);

	virtual void update();

	virtual void init();

	virtual void release();
};


//////////////////////////////////////////////////////

class SCTutorialEquipMentStarStep4: public Script {
private:
	DECLARE_CLASS(SCTutorialEquipMentStarStep4)

public:
	SCTutorialEquipMentStarStep4();
	virtual ~SCTutorialEquipMentStarStep4();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////////////////

class SCTutorialEquipMentStarStep5 : public Script {
private:
	DECLARE_CLASS(SCTutorialEquipMentStarStep5)

public:
	SCTutorialEquipMentStarStep5();
	virtual ~SCTutorialEquipMentStarStep5();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////////////////////////////

class SCTutorialEquipMentStarStep6 : public Script {
private:
	DECLARE_CLASS(SCTutorialEquipMentStarStep6)

public:
	SCTutorialEquipMentStarStep6();
	virtual ~SCTutorialEquipMentStarStep6();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};
#endif