#include "SCTutorialTranscript.h"
#include "../../utils/GameUtils.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutConfigure.h"
#include "SCTutorialGeneralScene.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"


IMPLEMENT_CLASS(SCTutorialTranscriptStep1)

SCTutorialTranscriptStep1::SCTutorialTranscriptStep1()
{
}

SCTutorialTranscriptStep1::~SCTutorialTranscriptStep1()
{
}

void* SCTutorialTranscriptStep1::createInstance()
{
	return new SCTutorialTranscriptStep1() ;
}

void SCTutorialTranscriptStep1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep1()");

			std::string content = mPara.at(0);
			//3210
			//MainScene * mainScene =(MainScene *)GameView::getInstance()->getMainUIScene();

			auto guideMap_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			guideMap_->addCCTutorialIndicator(content.c_str(),guideMap_->btn_instance->getPosition(),CCTutorialIndicator::Direction_RU);
			guideMap_->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(guideMap_->btn_instance) ;

			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialTranscriptStep1::init() {
	CCLOG("SCTutorialRobotStep1() init");
}

void SCTutorialTranscriptStep1::release() {
	CCLOG("SCTutorialRobotStep1() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	auto guideMap_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (guideMap_ != NULL)
		guideMap_->removeCCTutorialIndicator();
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialTranscriptStep2)

SCTutorialTranscriptStep2::SCTutorialTranscriptStep2()
{
}

SCTutorialTranscriptStep2::~SCTutorialTranscriptStep2()
{
}

void* SCTutorialTranscriptStep2::createInstance()
{
	return new SCTutorialTranscriptStep2() ;
}

void SCTutorialTranscriptStep2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialTestStep2()");

			std::string content = mPara.at(0);

// 			GeneralsRecuriteUI* generalsRecuriteUI = GeneralsUI::generalsRecuriteUI;
// 			generalsRecuriteUI->addTutorialIndicator(content.c_str(),generalsRecuriteUI->Button_wonderfulRecurit->getPosition());
// 			//REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
// 			generalsRecuriteUI->registerScriptCommand(this->getId()) ; 
//			this->setCommandHandler(generalsRecuriteUI->Button_wonderfulRecurit) ;
// 			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialTranscriptStep2::init() {
	CCLOG("SCTutorialSkillSceneStep2() init");
}

void SCTutorialTranscriptStep2::release() {
	CCLOG("SCTutorialSkillSceneStep2() release");

// 	GeneralsRecuriteUI* generalsRecuriteUI = GeneralsUI::generalsRecuriteUI;
// 	if(generalsRecuriteUI != NULL)
// 		generalsRecuriteUI->removeTutorialIndicator();
}

