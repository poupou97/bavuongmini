#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALGENERALMODE_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALGENERALMODE_H_

#include "../Script.h"

/**
 * �佫�����ѧ
 *
 * @author yangjun
 * @date 2014-4-18
 */


//////////////////////////�佫ģʽ�л�������ģʽ////////////////////////////////////
class SCTutorialGeneralModeStep1 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralModeStep1)

public:
	SCTutorialGeneralModeStep1();
	virtual ~SCTutorialGeneralModeStep1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


//////////////////////////�佫ģʽ�л���Эͬģʽ////////////////////////////////////
class SCTutorialGeneralModeStep2 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralModeStep2)

public:
	SCTutorialGeneralModeStep2();
	virtual ~SCTutorialGeneralModeStep2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////����佫ģʽ////////////////////////////////////
class SCTutorialGeneralModeStep3 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralModeStep3)

public:
	SCTutorialGeneralModeStep3();
	virtual ~SCTutorialGeneralModeStep3();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif
