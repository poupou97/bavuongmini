#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALMAINSCENE_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALMAINSCENE_H_

#include "../Script.h"

/**
 *�������ѧ
 *
 * @author yangjun
 * @date 2014-1-7
 */

//////////////////////�������ͷ��(�����˵�)////////////////////////
class SCTutorialMainScene1 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene1)

public:
	SCTutorialMainScene1();
	virtual ~SCTutorialMainScene1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////�������ͷ��(�ջز˵�)////////////////////////
class SCTutorialMainScene1Close : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene1Close)

public:
	SCTutorialMainScene1Close();
	virtual ~SCTutorialMainScene1Close();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////�������ͷ��(��һ���佫��ѧ)////////////////////////
// class SCTutorialMainSceneGeneral1 : public Script {
// private:
	//DECLARE_CLASS(SCTutorialMainSceneGeneral1)
// 
// public:
// 	SCTutorialMainSceneGeneral1();
// 	virtual ~SCTutorialMainSceneGeneral1();
// 
// 	virtual bool isBlockFunction() { return true; };
// 
// 	static void* createInstance() ;
// 
// 	virtual void update();
// 
// 	virtual void init();
// 
// 	virtual void release();
// };

//////////////////////�������ͷ��(������˫����ʱ)////////////////////////
class SCTutorialMainSceneSetMusou1 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneSetMusou1)

public:
	SCTutorialMainSceneSetMusou1();
	virtual ~SCTutorialMainSceneSetMusou1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////����˵����ϵļ���/////////////////////////////

class SCTutorialMainScene2 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene2)

public:
	SCTutorialMainScene2();
	virtual ~SCTutorialMainScene2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


/////////////////////////����˵����ϵļ���(������˫����)/////////////////////////////

class SCTutorialMainSceneSetMusou2 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneSetMusou2)

public:
	SCTutorialMainSceneSetMusou2();
	virtual ~SCTutorialMainSceneSetMusou2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////����˵����ϵ��佫(��һ���佫��ѧ��ֱ�ӽ����ҵ��佫�б����)/////////////////////////////

class SCTutorialMainSceneForFirst : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneForFirst)

public:
	SCTutorialMainSceneForFirst();
	virtual ~SCTutorialMainSceneForFirst();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////����˵����ϵ��佫(�ڶ����佫��ѧ����ѧϰ��ѧ)/////////////////////////////

class SCTutorialMainSceneForSecond : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneForSecond)

public:
	SCTutorialMainSceneForSecond();
	virtual ~SCTutorialMainSceneForSecond();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////����˵����ϵ��佫/////////////////////////////

class SCTutorialMainScene4 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene4)

public:
	SCTutorialMainScene4();
	virtual ~SCTutorialMainScene4();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


/////////////////////////�������ͷ������ǿ����/////////////////////////////
//���� ����
class SCTutorialMainSceneSetStrength1 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneSetStrength1)

public:
	SCTutorialMainSceneSetStrength1();
	virtual ~SCTutorialMainSceneSetStrength1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

class SCTutorialMainScene5 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene5)

public:
	SCTutorialMainScene5();
	virtual ~SCTutorialMainScene5();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};
//���� ����
class SCTutorialMainSceneSetStrengthStar1 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneSetStrengthStar1)

public:
	SCTutorialMainSceneSetStrengthStar1();
	virtual ~SCTutorialMainSceneSetStrengthStar1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

class SCTutorialMainSceneStar1 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainSceneStar1)

public:
	SCTutorialMainSceneStar1();
	virtual ~SCTutorialMainSceneStar1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////����˵���������/////////////////////////////

class SCTutorialMainScene6 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene6)

public:
	SCTutorialMainScene6();
	virtual ~SCTutorialMainScene6();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////ѡ������С����еĵ�һ��/////////////////////////////

class SCTutorialMissionAndTeam1 : public Script {
private:
	DECLARE_CLASS(SCTutorialMissionAndTeam1)

public:
	SCTutorialMissionAndTeam1();
	virtual ~SCTutorialMissionAndTeam1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////ѡ������С����еĵڶ���/////////////////////////////

class SCTutorialMissionAndTeam2 : public Script {
private:
	DECLARE_CLASS(SCTutorialMissionAndTeam2)

public:
	SCTutorialMissionAndTeam2();
	virtual ~SCTutorialMissionAndTeam2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


/////////////////////////���������ϢС���/////////////////////////////

class SCTutorialMissionAndTeam3 : public Script {
private:
	DECLARE_CLASS(SCTutorialMissionAndTeam3)

public:
	SCTutorialMissionAndTeam3();
	virtual ~SCTutorialMissionAndTeam3();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


/////////////////////////ͨ������ҡ�˺͵�������ƶ�/////////////////////////////

class SCTutorialMainScene7 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene7)

public:
	SCTutorialMainScene7();
	virtual ~SCTutorialMainScene7();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


////////////////////////������ʹ�ü���-���־�����//////////////////////////////

class SCTutorialMainScene8 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene8)

public:
	SCTutorialMainScene8();
	virtual ~SCTutorialMainScene8();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


////////////////////////������3��ʹ�õ��弼�ܼ���-���־�����//////////////////////////////

class SCTutorialMainScene9 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene9)

public:
	SCTutorialMainScene9();
	virtual ~SCTutorialMainScene9();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////������2��ʹ��Ⱥ�弼�ܼ���-���־�����//////////////////////////////

class SCTutorialMainScene10 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene10)

public:
	SCTutorialMainScene10();
	virtual ~SCTutorialMainScene10();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


////////////////////////���һ���佫ͷ��ʹ�佫��ս-���־�����//////////////////////////////

class SCTutorialMainScene11 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene11)

public:
	SCTutorialMainScene11();
	virtual ~SCTutorialMainScene11();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////��ڶ����佫ͷ��ʹ�佫��ս-���־�����//////////////////////////////

class SCTutorialMainScene12 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene12)

public:
	SCTutorialMainScene12();
	virtual ~SCTutorialMainScene12();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////��������佫ͷ��ʹ�佫��ս-���־�����//////////////////////////////

class SCTutorialMainScene13 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene13)

public:
	SCTutorialMainScene13();
	virtual ~SCTutorialMainScene13();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////���һ���佫ͷ��ʹ�佫��ս-������Ϸ������//////////////////////////////

class SCTutorialMainScene14 : public Script {
private:
	DECLARE_CLASS(SCTutorialMainScene14)

public:
	SCTutorialMainScene14();
	virtual ~SCTutorialMainScene14();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif

