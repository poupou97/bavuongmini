#ifndef _LEGEND_SCRIPT_COMMAND_COPY_H_
#define _LEGEND_SCRIPT_COMMAND_COPY_H_

#include "../Script.h"

/**
 * ������ѧ
 *
 * @author liuzhenxing
 * @date 2014-1-10
 */
class SCTutorialTranscriptStep1 : public Script {
private:
    DECLARE_CLASS(SCTutorialTranscriptStep1)

public:
	SCTutorialTranscriptStep1();
	virtual ~SCTutorialTranscriptStep1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////////////////

class SCTutorialTranscriptStep2 : public Script {
private:
    DECLARE_CLASS(SCTutorialTranscriptStep2)

public:
	SCTutorialTranscriptStep2();
	virtual ~SCTutorialTranscriptStep2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif