
#ifndef _LEGEND_SCRIPT_UITUTORIALINDICATOR_H_
#define _LEGEND_SCRIPT_UITUTORIALINDICATOR_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define UITUTORIALINDICATORTAG 986532

class UITutorialIndicator : public ui::Widget
{
public:
	UITutorialIndicator();
	~UITutorialIndicator();

	static UITutorialIndicator * create();
	bool init();

private:
	ui::ImageView * m_image_indicator;
	Label* m_label_des; 
};

#endif