#include "CCTutorialIndicator.h"

#include "AppMacros.h"
#include "../gamescene_state/SimpleEffectManager.h"
#include "../GameView.h"
#include "../ui/missionscene/MissionManager.h"
#include "../gamescene_state/sceneelement/MissionAndTeam.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../gamescene_state/MainScene.h"
#include "../utils/GameUtils.h"

#define HightLightFrameAction_PlaySpaceTime 10000

CCTutorialIndicator::CCTutorialIndicator():
m_direction(Direction_LD)
{
}

CCTutorialIndicator::~CCTutorialIndicator()
{
}

CCTutorialIndicator* CCTutorialIndicator::create(const char* content,Vec2 pos,Direction direction,bool isAddToMainScene,bool isDesFrameVisible ,bool isHandFilp)
{
	auto tutorialIndicator = new CCTutorialIndicator();
	if (tutorialIndicator && tutorialIndicator->init(content,pos,direction,isAddToMainScene,isDesFrameVisible,isHandFilp))
	{
		tutorialIndicator->autorelease();
		return tutorialIndicator;
	}
	CC_SAFE_DELETE(tutorialIndicator);
	return NULL;
}

bool CCTutorialIndicator::init(const char* content,Vec2 pos,Direction direction,bool isAddToMainScene,bool isDesFrameVisible,bool isHandFilp )
{
	if (Node::init())
	{
		isAutoPlayHightLightAction = false;
		//description
		m_label_des = Label::createWithTTF(content,APP_FONT_NAME,18,Size(190,0),TextHAlignment::LEFT,TextVAlignment::CENTER);
		m_label_des->setAnchorPoint(Vec2(0.5f,0.5f));
		m_label_des->setColor(Color3B(92,35,15));

		//descriptionFrame
		m_image_desFrame  = cocos2d::extension::Scale9Sprite::create("res_ui/tutorial/yindaokuang_0.png");
		m_image_desFrame->setIgnoreAnchorPointForPosition(false);
		m_image_desFrame->setCapInsets(Rect(35,18,1,34));
		if (m_label_des->getContentSize().height>40)  //���
		{
			m_image_desFrame->setContentSize(Size(230,101));
		}
		else if (m_label_des->getContentSize().height>20&&m_label_des->getContentSize().height<=40)//���
		{
			m_image_desFrame->setContentSize(Size(230,81));
		}
		else  //�һ�
		{
			m_image_desFrame->setContentSize(Size(230,69));
		}
		m_image_desFrame->setAnchorPoint(Vec2(0.5f,0.5f));


		this->addChild(m_image_desFrame);
		m_image_desFrame->addChild(m_label_des);
		m_label_des->setPosition(Vec2(m_image_desFrame->getContentSize().width/2+10,m_image_desFrame->getContentSize().height/2));

		m_anm_indicator = CCLegendAnimation::create("animation/texiao/renwutexiao/shou1/shou.anm");
		m_anm_indicator->setPlayLoop(true);
		m_anm_indicator->setReleaseWhenStop(true);
		this->addChild(m_anm_indicator);
		if (!isHandFilp)
		{
			m_anm_indicator->setScaleX(1.0f);
			switch (direction) 
			{
			case  Direction_LD :
				{
					m_image_desFrame->setPosition(Vec2(70+m_image_desFrame->getContentSize().width/2,13));
				}
				break;
			case  Direction_LU :
				{
					m_image_desFrame->setPosition(Vec2(70+m_image_desFrame->getContentSize().width/2,-25));
				}
				break;
			case  Direction_RU :
				{
					m_image_desFrame->setPosition(Vec2(-40-m_image_desFrame->getContentSize().width/2,-25));
				}
				break;
			case  Direction_RD :
				{
					m_image_desFrame->setPosition(Vec2(-40-m_image_desFrame->getContentSize().width/2,13));
				}
				break;
			}
		}
		else
		{
			m_anm_indicator->setScaleX(-1);
			switch (direction) 
			{
			case  Direction_LD :
				{
					m_image_desFrame->setPosition(Vec2(30+m_image_desFrame->getContentSize().width/2,13));
				}
				break;
			case  Direction_LU :
				{
					m_image_desFrame->setPosition(Vec2(30+m_image_desFrame->getContentSize().width/2,-25));
				}
				break;
			case  Direction_RU :
				{
					m_image_desFrame->setPosition(Vec2(-50-m_image_desFrame->getContentSize().width/2,-25));
				}
				break;
			case  Direction_RD :
				{
					m_image_desFrame->setPosition(Vec2(-50-m_image_desFrame->getContentSize().width/2,13));
				}
				break;
			}
		}

		//��ǽ�ѧ�ʻ��15Ǽ�ǰ��ָ��(�Ĭ���ǽ�ѧ)
		setPresentDesFrame(isDesFrameVisible);

		//����ѧ����Ҫ����ʱ�����ȼ���Ƿ15�ǰָ��ʴ��ڣ���������ɾ�
		if (isDesFrameVisible && isAddToMainScene)//��ǽ�ѧ�ʲ�������ӵ�������ϵ
		{
			if (GameView::getInstance())
			{
				if (GameView::getInstance()->getGameScene())
				{
					if (GameView::getInstance()->myplayer->getActiveRole()->level() <= K_GuideForMainMission_Level)
					{
						auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
						if (mainScene)
						{
							auto missionAndTeam = (MissionAndTeam*)mainScene->getChildByTag(kTagMissionAndTeam);
							if (missionAndTeam)
							{
								missionAndTeam->removeGuidePen();
							}
						}
					}
				}
			}
		}

		return true;
	}
	return false;
}



Sequence* CCTutorialIndicator::RoundRectPathAction(Direction direction,float controlX, float controlY, float w)
{
	switch (direction) 
	{
	case  Direction_LD :
		{
			ccBezierConfig bezier1;
			bezier1.controlPoint_1 = Vec2(controlX, 0);
			bezier1.controlPoint_2 = Vec2(controlX, -controlY);
			bezier1.endPosition = Vec2(0, -controlY);
			auto bezierBy1 = BezierBy::create(0.8f, bezier1);

			auto move1 = MoveBy::create(0.1f, Vec2(-w, 0));

			ccBezierConfig bezier2;
			bezier2.controlPoint_1 = Vec2(-controlX, 0);
			bezier2.controlPoint_2 = Vec2(-controlX, controlY);
			bezier2.endPosition = Vec2(0, controlY);
			auto bezierBy2 = BezierBy::create(0.8f, bezier2);

			auto move2 = MoveBy::create(0.1f, Vec2(w, 0));

			Sequence* path = NULL;

			if(w > 0)
				path = Sequence::create(bezierBy1, move1, bezierBy2, move2, NULL);
			else
				path = Sequence::create(bezierBy1, bezierBy2, NULL);
			return path;
		}
		break;
	case  Direction_LU :
		{
			ccBezierConfig bezier1;
			bezier1.controlPoint_1 = Vec2(-controlX, 0);
			bezier1.controlPoint_2 = Vec2(-controlX, controlY);
			bezier1.endPosition = Vec2(0, controlY);
			auto bezierBy1 = BezierBy::create(0.8f, bezier1);

			auto move1 = MoveBy::create(0.1f, Vec2(-w, 0));

			ccBezierConfig bezier2;
			bezier2.controlPoint_1 = Vec2(controlX, 0);
			bezier2.controlPoint_2 = Vec2(controlX, -controlY);
			bezier2.endPosition = Vec2(0, -controlY);
			auto bezierBy2 = BezierBy::create(0.8f, bezier2);

			auto move2 = MoveBy::create(0.1f, Vec2(w, 0));

			Sequence* path = NULL;

			if(w > 0)
				path = Sequence::create(move1,bezierBy1, move2, bezierBy2, NULL);
			else
				path = Sequence::create(bezierBy1, bezierBy2, NULL);
			return path;
		}
		break;
	case  Direction_RD :
		{
			auto move1 = MoveBy::create(0.1f, Vec2(w, 0));

			ccBezierConfig bezier1;
			bezier1.controlPoint_1 = Vec2(controlX, 0);
			bezier1.controlPoint_2 = Vec2(controlX, -controlY);
			bezier1.endPosition = Vec2(0, -controlY);
			auto bezierBy1 = BezierBy::create(0.8f, bezier1);

			auto move2 = MoveBy::create(0.1f, Vec2(-w, 0));
			
			ccBezierConfig bezier2;
			bezier2.controlPoint_1 = Vec2(-controlX, 0);
			bezier2.controlPoint_2 = Vec2(-controlX, controlY);
			bezier2.endPosition = Vec2(0, controlY);
			auto bezierBy2 = BezierBy::create(0.8f, bezier2);

			Sequence* path = NULL;

			if(w > 0)
				path = Sequence::create(move1,bezierBy1, move2, bezierBy2, NULL);
			else
				path = Sequence::create(bezierBy1, bezierBy2, NULL);
			return path;
		}
		break;
	case  Direction_RU :
		{
			auto move1 = MoveBy::create(0.1f, Vec2(w, 0));

			ccBezierConfig bezier1;
			bezier1.controlPoint_1 = Vec2(-controlX, 0);
			bezier1.controlPoint_2 = Vec2(-controlX, controlY);
			bezier1.endPosition = Vec2(0, controlY);
			auto bezierBy1 = BezierBy::create(0.8f, bezier1);

			auto move2 = MoveBy::create(0.1f, Vec2(-w, 0));

			ccBezierConfig bezier2;
			bezier2.controlPoint_1 = Vec2(controlX, 0);
			bezier2.controlPoint_2 = Vec2(controlX, -controlY);
			bezier2.endPosition = Vec2(0, -controlY);
			auto bezierBy2 = BezierBy::create(0.8f, bezier2);

			Sequence* path = NULL;

			if(w > 0)
				path = Sequence::create(bezierBy1,move1, bezierBy2,move2, NULL);
			else
				path = Sequence::create(bezierBy2, bezierBy1, NULL);
			return path;
		}
		break;
	}
	return NULL;
}

void CCTutorialIndicator::setPresentDesFrame( bool TOrF )
{
	//descriptionFrame
	m_image_desFrame->setVisible(TOrF);
}

#define kTag_HighLightFrame 6984
#define kTag_HighLightFrameAction 6985
void CCTutorialIndicator::addHighLightFrameAction( int width,int height,Vec2 offset,bool isAutoPlay/* = false */)
{
	m_nHightLightWidth = width;
	m_nHightLightHeight = height;
	m_pHightLightOffSet = offset;
	isAutoPlayHightLightAction = isAutoPlay;
	m_nStartTime = GameUtils::millisecondNow();
	playHightLightFrameActionOnce();

	if (isAutoPlay)
	{
		this->schedule(schedule_selector(CCTutorialIndicator::update), 1.0f);
	}
}

void CCTutorialIndicator::playHightLightFrameActionOnce()
{
	if (this->getChildByTag(kTag_HighLightFrame))
	{
		this->getChildByTag(kTag_HighLightFrame)->removeFromParent();
	}

	auto sp_yellowFrame = cocos2d::extension::Scale9Sprite::create("res_ui/yellow_frame.png");
	sp_yellowFrame->setCapInsets(Rect(5,5,1,1));
	sp_yellowFrame->setPreferredSize(Size(m_nHightLightWidth*6.0f,m_nHightLightHeight*6.0f));
	sp_yellowFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
	sp_yellowFrame->setPosition(m_pHightLightOffSet);
	this->addChild(sp_yellowFrame);
	sp_yellowFrame->setTag(kTag_HighLightFrame);
	sp_yellowFrame->setVisible(false);
	sp_yellowFrame->setScale(1.0f);

	auto sequence = Sequence::create(
		Show::create(),
		ScaleTo::create(0.5f,1.0f/6.0f,1.0f/6.0f),
		CCHide::create(),
		RemoveSelf::create(),
		NULL);
	sp_yellowFrame->runAction(sequence);
	sequence->setTag(kTag_HighLightFrameAction);
}

#define kTag_CenterAnmSprite 6994
void CCTutorialIndicator::addCenterAnm( int width,int height,Vec2 offset /*= Vec2(0,0)*/ )
{
	if (this->getChildByTag(kTag_CenterAnmSprite))
	{
		this->getChildByTag(kTag_CenterAnmSprite)->removeFromParent();
	}

	auto sp_CenterAnmSprite = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/other/light_kuang.png");
	sp_CenterAnmSprite->setCapInsets(Rect(32,32,1,1));
	sp_CenterAnmSprite->setPreferredSize(Size(width,height));
	sp_CenterAnmSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
	sp_CenterAnmSprite->setPosition(offset);
	this->addChild(sp_CenterAnmSprite);
	sp_CenterAnmSprite->setVisible(true);
	sp_CenterAnmSprite->setLocalZOrder(-1);
	sp_CenterAnmSprite->setTag(kTag_CenterAnmSprite);

	auto sequence = Sequence::create(
		ScaleTo::create(0.5f,1.05f,1.05f),
		ScaleTo::create(0.5f,0.95f,0.95f),
		NULL);
	sp_CenterAnmSprite->runAction(RepeatForever::create(sequence));
}

void CCTutorialIndicator::update( float delta )
{
	if (isAutoPlayHightLightAction)
	{
		if (GameUtils::millisecondNow() - m_nStartTime >= HightLightFrameAction_PlaySpaceTime)
		{
			m_nStartTime = GameUtils::millisecondNow();

			if (!GameView::getInstance()->getMainUIScene())
			{
				return;
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission))
			{
				return;
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagNpcTalkWindow))
			{
				return;
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkWithNpc))
			{
				return;
			}
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagTalkMissionUI))
			{
				return;
			}
			playHightLightFrameActionOnce();
		}
	}
}
