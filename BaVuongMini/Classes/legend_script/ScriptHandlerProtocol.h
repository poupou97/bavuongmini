#ifndef _LEGEND_SCRIPT_SCRIPTPROTOCOL_H_
#define _LEGEND_SCRIPT_SCRIPTPROTOCOL_H_

#include "ScriptManager.h"
#include "Script.h"

/**
 * Common interface for ScriptHandler
 */
class ScriptHandlerProtocol
{
public:
	virtual void registerScriptCommand(int scriptId) = 0;
};

#endif