#include "ScriptManager.h"

#include "Script.h"
#include <sstream>
#include "../utils/StrUtils.h"
#include "../utils/Joystick.h"
#include "../common/CKBaseClass.h"
#include "../GameView.h"
#include "../gamescene_state/GameSceneState.h"
#include "../gamescene_state/MainScene.h"
#include "../gamescene_state/GameSceneCamera.h"
#include "../gamescene_state/role/BaseFighter.h"
#include "command/MovieScripts.h"
#include "../ui/story/StoryBlackBand.h"

using namespace std;

static ScriptManager* instance;

ScriptManager::ScriptManager()
{
	initCloseUIData();
}

ScriptManager::~ScriptManager(void)
{
}

ScriptManager* ScriptManager::getInstance() {
	if (instance == NULL) {
		instance = new ScriptManager();
	}
	return instance;
}	

void ScriptManager::loadScript(std::string filename) {
	ssize_t size = 0;
	unsigned char* pData = FileUtils::getInstance()->getFileData(filename.c_str(), "rb", &size);

    __String * string = __String::createWithData(pData, size);
	std::string scriptContent = string->getCString();
	//CCLOG(scriptContent.c_str());

	std::stringstream scriptStream(string->getCString());
    std::string line;
	
	int lineNum = 0;
	int len = 0;
    while(std::getline(scriptStream, line))
    {
		line = StrUtils::trim(line);
		if (StrUtils::startsWith(line, "//") || line.compare("") == 0) {
			// 注释忽略 || 空
			continue;
		}

		len = line.length();
		if (len <= 1) {
			continue;
		}

		int lineLen = line.length();
		//CCLOG("%s", line.c_str());
		lineNum++;

		auto newCommand = decodeCmd(line);
		mAllScriptsList.push_back(newCommand);
		mGlobalScriptsMap.insert(make_pair(newCommand->getId(), newCommand));
	}
	//CCLOG("line num: %d", lineNum);
}

Script* ScriptManager::decodeCmd(string& line) {
	int start = line.find_first_of("(");
	int end = line.find_first_of(")");
	if (start == -1 || end == -1) {
		CCLOG("script error: %s", line.c_str());
		CCAssert(false, "parameter error");
		return NULL;
	}

    std::string comStr = line.substr(0, start);
	string cmd = StrUtils::trim(comStr);
	string para = line.substr(start + 1, end-start-1);
	//CCLOG("command name: %s", cmd.c_str());
	//CCLOG("parameters: %s", para.c_str());

	if (cmd.compare("") == 0) {
		CCLOG("script error: %s", cmd.c_str());
		CCAssert(false, "error: script cmd name is wrong");
		return NULL;
	}

	// 反射
	string commandName = "SC";
	commandName.append(cmd);
	auto pVar = (CKBaseClass*)CKClassFactory::sharedClassFactory().getClassByName(commandName);
	Script *scriptCmd = dynamic_cast<Script*>(pVar);
	if(scriptCmd == NULL)
		CCAssert(false, "script command has not been defined.");
	scriptCmd->set(cmd, para);
	scriptCmd->setScriptManager(this);

	return scriptCmd;
}

void ScriptManager::addNextCommands()
{
	// script is finished normally
	if (mCurScriptIndex >= mAllScriptsList.size()) {
		end();
		return;
	}

	for(unsigned int i = mCurScriptIndex; i < mAllScriptsList.size(); i++)
	{
		auto sc = mAllScriptsList.at(i);
		mExcutingScriptsList.push_back(sc);
		mCurScriptIndex++;
		sc->init();
		if(sc->isBlockFunction())
			break;
	}
}

void ScriptManager::start() {
	mCurScriptIndex = 0;
	mStart = true;

	addNextCommands();
}

void ScriptManager::clear()
{
	// first, clear the excuting scripts list
	for(unsigned int i = 0; i < mExcutingScriptsList.size(); i++) {
		mExcutingScriptsList.at(i)->release();
	}
	mExcutingScriptsList.clear();

	// then, delete all scripts
	for(unsigned int i = 0; i < mAllScriptsList.size(); i++) {
		CC_SAFE_DELETE(mAllScriptsList.at(i));
	}
	mAllScriptsList.clear();

	// reset global scripts map
	Script::s_newScriptId = 0;
	mGlobalScriptsMap.clear();
}

bool ScriptManager::runScript(std::string filename)
{
	// stop the old one
	if(mStart)
		stop();

	end();

	// first, load script
	loadScript(filename);

	// then run it
	start();

	return true;
}

void ScriptManager::skipScript()
{
	// stop the old one
	if(mStart)
		stop();

	end();
}

void ScriptManager::update()
{
	if(!mStart)
		return;

	// 更新执行Command
	std::vector<Script*>::iterator iter;
	for (unsigned int i = 0; i < mExcutingScriptsList.size(); i++) {
		iter = mExcutingScriptsList.begin() + i;
		auto sc = *iter;
		if (sc == NULL) {
			mExcutingScriptsList.erase(iter);
			i--;
			continue;
		}
		if (sc->isEnd()) {
			// 脚本指令已经结束，释放
			mExcutingScriptsList.erase(iter);
			sc->release();
			i--;
			continue;
		}
		sc->update();
	}

	// 没有可以执行的Command，将下一批command加入执行队列
	// 如何选择下一批command？即找到下一个中断型command
	if(mExcutingScriptsList.size() <= 0)
	{
		addNextCommands();
	}
}

void ScriptManager::end() {
	CCLOG("script is finished");
	mStart = false;

	clear();
}
bool ScriptManager::isStart() {
	return mStart;
}

bool ScriptManager::isInMovieMode()
{
	Node* pMovieBaseNode = NULL;
	auto scene = GameView::getInstance()->getGameScene();
	if(scene != NULL)   // in game
	{
		pMovieBaseNode = scene->getChildByTag(kTagMovieBaseNode);
	}
	else
	{
		auto currentScene = Director::getInstance()->getRunningScene();
		pMovieBaseNode = currentScene->getChildByTag(kTagMovieBaseNode);
	}

	if(pMovieBaseNode != NULL)
		return true;
	else
		return false;
}

Node* ScriptManager::getMovieBaseNode()
{
	Node* pMovieBaseNode = NULL;
	auto scene = GameView::getInstance()->getGameScene();
	if(scene != NULL)   // in game
	{
		pMovieBaseNode = scene->getChildByTag(kTagMovieBaseNode);
		if(pMovieBaseNode == NULL)
		{
			pMovieBaseNode = Node::create();
			scene->addChild(pMovieBaseNode, SCENE_STORY_LAYER_BASE_ZORDER, kTagMovieBaseNode);
		}
	}
	else
	{
		auto currentScene = Director::getInstance()->getRunningScene();
		pMovieBaseNode = currentScene->getChildByTag(kTagMovieBaseNode);
		if(pMovieBaseNode == NULL)
		{
			pMovieBaseNode = Node::create();
			currentScene->addChild(pMovieBaseNode, SCENE_STORY_LAYER_BASE_ZORDER, kTagMovieBaseNode);
		}
	}

	return pMovieBaseNode;
}

void ScriptManager::stopMovie()
{
	auto scene = GameView::getInstance()->getGameScene();

	// show UI
	if(scene != NULL)
	{
		auto pUIScene = (Node*)GameView::getInstance()->getMainUIScene();
		if(pUIScene != NULL)
			pUIScene->setVisible(true);

		auto pJoystick = dynamic_cast<Joystick*>(pUIScene->getChildByTag(kTagVirtualJoystick));
		if(pJoystick != NULL && !pJoystick->isActive())
			pJoystick->Active();
	}

	resumeScene();

	// 将 背景黑条Node 其他的Node全部隐藏
	auto pMovieBaseNode = getMovieBaseNode();
	if(pMovieBaseNode != NULL)
	{
		//__Array * pArray = pMovieBaseNode->getChildren();
		cocos2d::Vector<Node*> pArray = pMovieBaseNode->getChildren();
		for (unsigned int i = 0; i < pMovieBaseNode->getChildrenCount(); i++)
		{
			auto pTmpNode = (Node *)pArray.at(i);
			int nTag = pTmpNode->getTag();
			if (MOVIE_BLACKBANK_TAG == nTag)
			{

			}
			else
			{
				pTmpNode->setVisible(false);
			}
		}
	}

	// 添加 背景黑条的动画效果
	StoryBlackBand* pNodeBlackBand = (StoryBlackBand *)pMovieBaseNode->getChildByTag(MOVIE_BLACKBANK_TAG);
	if (pNodeBlackBand != NULL)
	{
		auto action =(ActionInterval *)Sequence::create(
			Show::create(),
			MoveBy::create(0.4f, Vec2(0, -STORYBLACKBAND_HEIGHT)),
			CallFuncN::create(CC_CALLBACK_0(StoryBlackBand::endActionCallBack, pNodeBlackBand, pMovieBaseNode)),
			NULL);
		pNodeBlackBand->runAction(action);
	}
	else
	{
		if(scene != NULL)
		{
			if(pMovieBaseNode != NULL)
				pMovieBaseNode->removeFromParent();
		}
	}
}

void ScriptManager::resumeScene()
{
	auto scene = GameView::getInstance()->getGameScene();
	if(scene != NULL)
	{
		// show normal actor layer
		auto actorLayer = (__LayerRGBA*)scene->getActorLayer();
		actorLayer->stopAllActions();
		actorLayer->setVisible(true);
		actorLayer->setOpacity(255);

		// hide movie actor layer
		//scene->getMovieActorLayer()->setVisible(false);   // show movie actor layer
		if(scene->getMovieActorLayer()->isVisible())
		{
			auto actionHide = (ActionInterval*)Sequence::create(
				FadeOut::create(1.0f),
				CallFunc::create(CC_CALLBACK_0(GameSceneLayer::removeMovieActors, scene)),
				CCHide::create(),
				NULL
			);
			scene->getMovieActorLayer()->runAction(actionHide);

			// move MyPlayer from movie_actor_layer to actor_layer
			auto me = (BaseFighter*)scene->getActor(BaseFighter::getMyPlayerId());
			me->removeFromParent();
			scene->initMyPlayer();
			me->setVisible(true);
			me->setWorldPosition(m_myplayerRealPosition);
			scene->getActorLayer()->addChild(me);

			// show the foot circle again
			auto flagNode = me->getChildByTag(GameActor::kTagFootFlag);
			if(flagNode != NULL)
				flagNode->setVisible(true);
		}
	}
}

void ScriptManager::recordMyPlayerPosition(Vec2 realPosition)
{
	m_myplayerRealPosition = realPosition;
}
Vec2 ScriptManager::getRecordMyPlayerPosition()
{
	return m_myplayerRealPosition;
}

void ScriptManager::stop()
{
	stopMovie();
}

Script* ScriptManager::getScriptById(int id)
{
    std::map<int,Script*>::iterator iter = mGlobalScriptsMap.find(id);
    if( mGlobalScriptsMap.end() != iter )
        return iter->second;

    return NULL;
}

void ScriptManager::initCloseUIData()
{
	mCloseUIData.clear();

	mCloseUIData.push_back(kTagBackpack);
	mCloseUIData.push_back(kTagGoodsItemInfoBase);
	mCloseUIData.push_back(kTagMap);
	mCloseUIData.push_back(kTabChat);
	mCloseUIData.push_back(kTagMissionScene);
	mCloseUIData.push_back(kTagMailUi);
	mCloseUIData.push_back(kTagTalkWithNpc);
	mCloseUIData.push_back(kTagHandleMission);
	mCloseUIData.push_back(kTagTalkMissionUI);
	mCloseUIData.push_back(kTagUseMissionPropUI);
	mCloseUIData.push_back(kTagFriendUi);
	mCloseUIData.push_back(kTagNpcTalkWindow);
	mCloseUIData.push_back(kTagAuction);
	mCloseUIData.push_back(KTagSkillScene);
	mCloseUIData.push_back(KtagSkillInfoScene);
	mCloseUIData.push_back(kTagShortcutConfigure);
	mCloseUIData.push_back(kTagMyBuff);
	mCloseUIData.push_back(ktagEquipMentUI);
	mCloseUIData.push_back(kTagGeneralsUI);
	mCloseUIData.push_back(kTagGeneralsSingleRecuriteResultUI);
	mCloseUIData.push_back(kTagGeneralsTenTimesRecuriteResultUI);
	mCloseUIData.push_back(kTagStoreHouseUI);
	mCloseUIData.push_back(kTagInstanceDetailUI);
	mCloseUIData.push_back(kTagInstanceEndUI);
	mCloseUIData.push_back(kTagInstanceMapUI);
	mCloseUIData.push_back(kTagGeneralsFateInfoUI);
	mCloseUIData.push_back(kTagShopUI);
	mCloseUIData.push_back(kTagGeneralsTenTimesRecuriteResultUI);
	mCloseUIData.push_back(kTagShopItemBuyInfo);
	mCloseUIData.push_back(kTagGeneralsSkillInfoUI);
	mCloseUIData.push_back(kTagGeneralsSkillListUI);
	mCloseUIData.push_back(kTagGeneralsListForFightWaysUI);
	mCloseUIData.push_back(kTagStrategiesDetailInfo);
	mCloseUIData.push_back(kTagStrategiesUpGradeUI);
	mCloseUIData.push_back(kTagPopFriendListUI);
	mCloseUIData.push_back(kTagInstanceRewardUI);
	mCloseUIData.push_back(kTagFamilyUI);
	mCloseUIData.push_back(kTagApplyFamilyUI);
	mCloseUIData.push_back(kTagManageFamilyUI);
	mCloseUIData.push_back(kTagGoldStoreUI);
	mCloseUIData.push_back(kTagGoldStoreItemBuyInfo);
	mCloseUIData.push_back(kTagGoldStoreItemInfo);
	mCloseUIData.push_back(kTagGeneralListForTakeDrug);
	mCloseUIData.push_back(kTagSetUI);
	mCloseUIData.push_back(kTagOnLineGiftUI);
	mCloseUIData.push_back(kTagSignDailyUI);
	mCloseUIData.push_back(kTagMoneyTreeUI);
	mCloseUIData.push_back(kTagActiveUI);
	mCloseUIData.push_back(kTagActiveShop);
	mCloseUIData.push_back(kTagActiveShopItemBuyInfo);
	mCloseUIData.push_back(kTagActiveShopItemInfo);
	mCloseUIData.push_back(kTagRankUI);
	mCloseUIData.push_back(kTagExtraRewardsUI);
	mCloseUIData.push_back(ktagQuiryUI);
	mCloseUIData.push_back(kTagOffLineArenaUI);
	mCloseUIData.push_back(kTagHonorShopItemBuyInfo);
	mCloseUIData.push_back(kTagHonorShopItemInfo);
	mCloseUIData.push_back(kTagOffLineArenaResultUI);
	mCloseUIData.push_back(kTagNewFunctionRemindUI);
	mCloseUIData.push_back(kTagVipDetailUI);
	mCloseUIData.push_back(kTagVipEveryDayRewardsUI);
	mCloseUIData.push_back(kTagFirstBuyVipUI);
	mCloseUIData.push_back(kTagOffLineExpUI);
	mCloseUIData.push_back(kTagQuestionUI);
	mCloseUIData.push_back(kTagQuestionEndUI);
	mCloseUIData.push_back(kTagRechargeUI);
	mCloseUIData.push_back(kTagWorldBossUI);
	mCloseUIData.push_back(kTagRewardDesUI);
	mCloseUIData.push_back(kTagGetRewardUI);
	mCloseUIData.push_back(kTagDmgRankingUI);
	mCloseUIData.push_back(ktagPrivateUI);
	mCloseUIData.push_back(kTagManageFamily);
	mCloseUIData.push_back(kTagChessBoardUI);
	mCloseUIData.push_back(ktagChallengeRoundUi);
	mCloseUIData.push_back(ktagSingCopylevel);
	mCloseUIData.push_back(ktagSingCopyRank);
	mCloseUIData.push_back(ktagSingCopyReward);
	mCloseUIData.push_back(ktagSingCopyResutUi);
	mCloseUIData.push_back(kTagFamilyFightRewardUI);
	mCloseUIData.push_back(kTagFamilyFightHistoryNotesUI);
	mCloseUIData.push_back(kTagFamilyFightIntegrationRankingUI);
	mCloseUIData.push_back(kTagFamilyFightResultUI);
	mCloseUIData.push_back(kTagBattleSituationUI);
	mCloseUIData.push_back(kTagBattleAchievementUI);
	mCloseUIData.push_back(kTagRecuriteGeneralCardUI);
	mCloseUIData.push_back(kTagGetphyPowerUi);
	mCloseUIData.push_back(kTagRobotUI);
	mCloseUIData.push_back(kTagRewardUI);
	mCloseUIData.push_back(kTagRewardTaskMainUI);
	mCloseUIData.push_back(kTagRewardTaskDetailUI);
	mCloseUIData.push_back(kTagSearchGeneralUI);
}

