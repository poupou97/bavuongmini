#include "UITutorialIndicator.h"
#include "AppMacros.h"

UITutorialIndicator::UITutorialIndicator()
{
}


UITutorialIndicator::~UITutorialIndicator()
{
}

UITutorialIndicator * UITutorialIndicator::create()
{
	auto tutorialIndicator = new UITutorialIndicator();
	if (tutorialIndicator && tutorialIndicator->init())
	{
		tutorialIndicator->autorelease();
		return tutorialIndicator;
	}
	CC_SAFE_DELETE(tutorialIndicator);
	return NULL;
}

bool UITutorialIndicator::init()
{
	if (Widget::init())
	{
		m_image_indicator = ui::ImageView::create();
		m_image_indicator->loadTexture("res_ui/liaotian/jiantou.png");
		m_image_indicator->setAnchorPoint(Vec2(0.5f,0.5f));
		m_image_indicator->setPosition(Vec2(0,0));
		this->addChild(m_image_indicator);

		m_label_des = Label::createWithTTF("hello", APP_FONT_NAME, 30);
		m_label_des->setPosition(Vec2(0, 0));
		this->addChild(m_label_des);

		return true;
	}
	return false;
}
