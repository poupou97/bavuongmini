
#ifndef _LEGEND_SCRIPT_CCTUTORIALINDICATOR_H_
#define _LEGEND_SCRIPT_CCTUTORIALINDICATOR_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCLegendAnimation;

#define CCTUTORIALINDICATORTAG 986532

class CCTutorialIndicator : public Node
{
public:
	CCTutorialIndicator();
	virtual ~CCTutorialIndicator();

	enum Direction{
		Direction_LD = 0,       // ���
		Direction_LU,				//����
		Direction_RU,				//����
		Direction_RD,				//����
	};

	static CCTutorialIndicator * create(const char* content,Vec2 pos,Direction direction,bool isAddToMainScene = false,bool isDesFrameVisible = true,bool isHandFilp = false);
	bool init(const char* content,Vec2 pos,Direction direction,bool isAddToMainScene,bool isDesFrameVisible,bool isHandFilp);

	static Sequence* RoundRectPathAction(Direction direction,float controlX, float controlY, float w);

	void addHighLightFrameAction(int width,int height,Vec2 offset = Vec2(0,0),bool isAutoPlay = false);

	void addCenterAnm(int width,int height,Vec2 offset = Vec2(0,0));

private:
	void setPresentDesFrame(bool TOrF);

	void playHightLightFrameActionOnce();
	void update(float delta);

public:
	//Sprite * m_image_indicator;
	CCLegendAnimation * m_anm_indicator;
	cocos2d::extension::Scale9Sprite *m_image_desFrame;
	Label * m_label_des;
	//ParticleSystem* m_ps_particleEffect;
private:
	Direction m_direction;
	
	long long m_nStartTime;

	int m_nHightLightWidth;
	int m_nHightLightHeight;
	Vec2 m_pHightLightOffSet;
	bool isAutoPlayHightLightAction;
};

#endif
