#ifndef _LEGEND_SCRIPT_CCTEACHINGGUIDE_H_
#define _LEGEND_SCRIPT_CCTEACHINGGUIDE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

/*******
auto : yangjun 
date : 2014.10.410
des : ���ɫ�ɰ�Ľ�ѧָ��ǿ�Ƶ�
*******/

class CCTeachingGuide :public Layer
{
public:
	CCTeachingGuide(void);
	~CCTeachingGuide(void);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	static CCTeachingGuide * create(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction,int clipWidth,int clipHeight,bool isAddToMainScene = false,bool isDesFrameVisible = true,bool isHandFlip = false);
	bool init(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction,int clipWidth,int clipHeight,bool isAddToMainScene,bool isDesFrameVisible,bool isHandFlip );

	//����òü��������
	void setDrawNodePos(Vec2 pt);

	//��ѧ����ʱǿ�ƹر�һЩ���(�ָ�����������Ԫ��ʱ)
	static void CloseUIForTutorial();
private:
	CCTutorialIndicator::Direction m_curDirection;
	CCTutorialIndicator * tutorialIndicator;
	ClippingNode * clip;
	DrawNode * front;
	//�ⷢ��Ļ�ɫ�߿
	cocos2d::extension::Scale9Sprite* sp_yellowFrame;

	int m_nClipWidth;
	int m_nClipHeight;

	void playYellowFrameAnm();

	void updateTime(float dt);
	int m_nDelayTime;
	bool m_bIsTimeUP;
};

#endif;

