﻿#include "SimpleEffectManager.h"

Node* SimpleEffectManager::generateTextEffect(int effectType, const char* text, const Color3B& color3) {
    auto label = Label::createWithTTF(text, "Marker Felt", 50);
	label->setColor(color3);   // Color3B(255, 0, 0)

	label->runAction(getEffectAction(effectType));

	return label;
}

void SimpleEffectManager::applyEffectAction(Node* effectNode, int effectType)
{
	effectNode->runAction(getEffectAction(effectType));
}

// 通过这2个控制参数，来模拟数字在角色头顶，在倒锥形的范围来显示
static int s_effectPuTongLayout_target_horizon = 0;   // 控制水平方向出现的位置
static int s_effectPuTongLayout_target_vertical = 0;   // 控制垂直方向出现的位置

Action* SimpleEffectManager::getEffectAction(int effectType)
{
    switch (effectType)
    {
    case SimpleEffectManager::EFFECT_BAOJI:
		{
			auto  action = Sequence::create(
				Show::create(),
				ScaleTo::create(0.25f*1/2,2.0f),
				ScaleTo::create(0.25f*1/3,1.0f),
				DelayTime::create(0.9f),
				FadeOut::create(0.5f),
				RemoveSelf::create(),
				NULL);

			return action;
		}
		break;

	case SimpleEffectManager::EFFECT_PUTONG:
		{
			int x = 0;
			if(s_effectPuTongLayout_target_horizon == 0) {
				//x = -15;
				s_effectPuTongLayout_target_horizon = 1;
			}
			else {
				//x = 15;
				s_effectPuTongLayout_target_horizon = 0;
			}

			int y = 0;
			float duration = 0.3f;
			if(s_effectPuTongLayout_target_vertical == 0) {
				y = 0;
				duration = 0.2f;

				if(s_effectPuTongLayout_target_horizon == 0) {
					x = -3;
				}
				else {
					x = 3;
				}

				s_effectPuTongLayout_target_vertical = 1;
			}
			else if(s_effectPuTongLayout_target_vertical == 1) {
				y = 30;
				duration = 0.3f;

				if(s_effectPuTongLayout_target_horizon == 0) {
					x = -8;
				}
				else {
					x = 8;
				}

				s_effectPuTongLayout_target_vertical = 2;
			}
			else {
				y = 60;
				duration = 0.4f;

				if(s_effectPuTongLayout_target_horizon == 0) {
					x = -18;
				}
				else {
					x = 18;
				}

				s_effectPuTongLayout_target_vertical = 0;
			}

			float totalDuration = 1.0f;
			auto action_step1= Spawn::create(
				ScaleTo::create(0.1f, 1.2f),
				MoveBy::create(duration, Vec2(x, y)),
				NULL);
			auto action_step2= Spawn::create(
				ScaleTo::create(0.08f, 0.9f),
				MoveBy::create(totalDuration - duration, Vec2(0,0)),
				NULL);
			auto action = Sequence::create(
				//FadeOut::create(1),
				//MoveBy::create(duration, Vec2(x, y)),
				action_step1,
				//MoveBy::create(totalDuration - duration, Vec2(0,0)),
				action_step2,
				FadeOut::create(0.3f),
				RemoveSelf::create(),
				NULL);

			return action;
		}
		break;

	case SimpleEffectManager::EFFECT_FLYINGUP:
		{
			auto action = Sequence::create(
				MoveBy::create(1.0f, Vec2(0, 40)),
				FadeTo::create(0.5f, 192),
				RemoveSelf::create(),
				NULL);
			return action;
		}
		break;

	default:
		CCAssert(false, "this effect type has not been implemented.");
		break;
	}

	return NULL;
}

RepeatForever* SimpleEffectManager::RoundRectPathAction(float controlX, float controlY, float w)
{
    ccBezierConfig bezier1;
    bezier1.controlPoint_1 = Vec2(-controlX, 0);
    bezier1.controlPoint_2 = Vec2(-controlX, controlY);
    bezier1.endPosition = Vec2(0, controlY);
    BezierBy * bezierBy1 = BezierBy::create(0.8f, bezier1);

    MoveBy * move1 = MoveBy::create(0.8f, Vec2(w, 0));
    
    ccBezierConfig bezier2;
    bezier2.controlPoint_1 = Vec2(controlX, 0);
    bezier2.controlPoint_2 = Vec2(controlX, -controlY);
    bezier2.endPosition = Vec2(0, -controlY);
    BezierBy * bezierBy2 = BezierBy::create(0.8f, bezier2);

    MoveBy * move2 = MoveBy::create(0.8f, Vec2(-w, 0));

	RepeatForever* path = NULL;
	if(w > 0)
		path = RepeatForever::create(Sequence::create(bezierBy1, move1, bezierBy2, move2, NULL));
	else
		path = RepeatForever::create(Sequence::create(bezierBy1, bezierBy2, NULL));
    
    return path;
}
