﻿#ifndef _GAMESCENESTATE_SIMPLEEFFECTMANAGER_H_
#define _GAMESCENESTATE_SIMPLEEFFECTMANAGER_H_

#include "cocos2d.h"

USING_NS_CC;

/**
 * 特效工具类，可以生成一些简单的特效、Action等
 * 用于战斗过程中，角色头顶的战斗效果指示等
 * 也用于界面上，粒子特效的运动轨迹等
 * Author: zhaogang
 */
class SimpleEffectManager {
public:
	enum Type {
		EFFECT_BAOJI = 0,
		EFFECT_PUTONG,
		EFFECT_FLYINGUP,   // 垂直向上飘一段距离
	};

	/**
	 * generate text effect node dynamically
	 * @param effectType
	 * @param text
	 */
	static Node* generateTextEffect(int effectType, const char* text, const Color3B& color3);

	static void applyEffectAction(Node* effectNode, int effectType);

	// detail info: http://www.cocoachina.com/gamedev/gameengine/2014/0110/7688.html
	static RepeatForever* RoundRectPathAction(float controlX, float controlY, float w);

private:
	static Action* getEffectAction(int effectType);
};

#endif