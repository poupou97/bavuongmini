﻿#include "GameStatistics.h"

std::map<long long, int> DamageStatistics::s_damageStatistic;

DamageStatistics::DamageStatistics()
{
}

DamageStatistics::~DamageStatistics()
{
}

void DamageStatistics::clear()
{
	s_damageStatistic.clear();
}

void DamageStatistics::collect(long long roleId, int damageNum)
{
	s_damageStatistic[roleId] += damageNum;
}

int DamageStatistics::getDamge(long long roleId)
{
	return s_damageStatistic[roleId];
}