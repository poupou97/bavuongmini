#include "LoadScene.h"
#include "../gamescene_state/GameSceneState.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "../messageclient/GameMessageProcessor.h"
#include "GameView.h"
#include "../messageclient/element/CMapInfo.h"
#include "../messageclient/element/CActiveRole.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../gamescene_state/MainScene.h"
#include "../gamescene_state/sceneelement/GuideMap.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../utils/GameUtils.h"
#include "../GameUserDefault.h"
#include "../loadscene_state/LoadSceneState.h"
#include "../ui/extensions/CCRichLabel.h"
#include "../ui/offlinearena_ui/OffLineArenaState.h"
#include "../messageclient/protobuf/PlayerMessage.pb.h"  
#include "../newcomerstory/NewCommerStoryManager.h"
#include "../ui/missionscene/MissionManager.h"
#include "../ui/challengeRound/SingleCopyResult.h"
#include "../ui/Active_ui/ActiveManager.h"
#include "cocostudio\CCSGUIReader.h"

#define LOAD_METHOD_ASYNC 0

#define LOADSCENE_SLIDER_NAME "slider"

using namespace CocosDenshion;

CMapInfo SceneLoadLayer::s_newMapInfo;
Vec2 SceneLoadLayer::s_myNewWorldPosition;

SceneLoadLayer::SceneLoadLayer()
: m_state(State_WaitAction)
, m_bLoadResFinished(false)
, time_(0)
, layer(NULL)
{
	////setTouchEnabled(true);

	// close keyboard(change scene need close keyboard)
	auto pGlView = Director::getInstance()->getOpenGLView();
	if (pGlView)
	{
		pGlView->setIMEKeyboardState(false);
	}

	if(GameView::getInstance()->getMapInfo()->country())
	{
		UserDefault::getInstance()->setIntegerForKey(CURRENT_COUNTRY, GameView::getInstance()->getMapInfo()->country());
		UserDefault::getInstance()->flush();
	}

	if(GameView::getInstance()->getGameScene() == NULL)
	{
		initNextLevel();

		createUI();

		m_state = State_Login;
	}

	time_ = GameUtils::millisecondNow();

	this->scheduleUpdate();
}

void SceneLoadLayer::createUI()
{
	Size s = Director::getInstance()->getVisibleSize();

	// game logo
	auto pSprite = Sprite::create(getLoadingImage());
	if(((float)s.width/(float)1136) > ((float)s.height/(float)640))
	{
		pSprite->setScaleX((float)s.width/(float)1136);
		pSprite->setScaleY((float)s.width/(float)1136);
	}
	else
	{
		pSprite->setScaleX((float)s.height/(float)640);
		pSprite->setScaleY((float)s.height/(float)640);
	}
	pSprite->setPosition(Vec2(s.width/2, s.height/2));
	pSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
	addChild(pSprite, 0);

	layer= Layer::create();

	//layer->setPosition(Vec2(s.width/2,30));
	layer->setPosition(Vec2(s.width/2,s.height/2));
	addChild(layer);

	float posY = s.height * 0.3f;
	auto mengban = ImageView::create();
	mengban->loadTexture("res_ui/zhezhao80_new.png");
	mengban->setScale9Enabled(true);
	mengban->setContentSize(Size(s.width, 100));
	mengban->setAnchorPoint(Vec2(0.5f,0.5f));
	mengban->setPosition(Vec2(0,-posY));
	mengban->setOpacity(130);
	layer->addChild(mengban);

	auto text = CCRichLabel::createWithString(getLoadingText().c_str(),Size(s.width, 100),NULL,NULL);
	//Label * text=Label::createWithTTF(getLoadingText().c_str(), APP_FONT_NAME, 20);
	text->setAnchorPoint(Vec2(0.5f, 0.5f));
	text->setPosition(Vec2(0,-posY));
	//Color3B shadowColor = Color3B(0,0,0);   // black
	//text->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
	layer->addChild(text);

	// Create the slider
    auto slider = Slider::create();

	slider->setName(LOADSCENE_SLIDER_NAME);
    //slider->setTouchEnabled(true);
    slider->loadBarTexture("");
    slider->loadSlidBallTextures("res_ui/LOADING/light.png", "res_ui/LOADING/light.png", "");
    slider->loadProgressBarTexture("res_ui/progress_bar.png");
	slider->setScale9Enabled(true);
	slider->setContentSize(Size(s.width, 10));
    slider->setPosition(Vec2(0,-(posY+50)));
	slider->setPercent(0);

    layer->addChild(slider);
	/*
	CCLegendAnimation* pBird = CCLegendAnimation::create("animation/texiao/jiemiantexiao/bird/fly.anm");
	pBird->setPlayLoop(true);
	pBird->setReleaseWhenStop(false);
	pBird->setAnchorPoint(Vec2(0.5f,0.5f));
	pBird->setPosition(Vec2(s.width/2 - 20, -(posY+70)));
	pBird->setScale(0.33f);
	layer->addChild(pBird);
	*/
//  Sprite* pSprite = Sprite::create("images/mm01.png");
//  pSprite->setPosition(Vec2(s.width/2, s.height/2));
// 	pSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
//	addChild(pSprite, 0);

//     m_pLabelLoading = Label::createWithTTF("loading...", "Arial", 50);
//     m_pLabelLoading->setPosition(Vec2(s.width / 2, 40));
//     this->addChild(m_pLabelLoading);

#if LOAD_METHOD_ASYNC
	m_pLabelPercent = Label::createWithTTF("%0", APP_FONT_NAME, 50);
    m_pLabelPercent->setPosition(Vec2(s.width / 2, s.height / 2 + 20));
	Color3B shadowColor = Color3B(0,0,0);   // black
	m_pLabelPercent->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
    this->addChild(m_pLabelPercent);
#endif
}

void SceneLoadLayer::loadingCallBack(Ref *obj)
{
    ++m_nNumberOfLoadedSprites;
    char tmp[10];
    sprintf(tmp,"%%%d", (int)(((float)m_nNumberOfLoadedSprites / m_nNumberOfSprites) * 100));
    m_pLabelPercent->setString(tmp);

    if (m_nNumberOfLoadedSprites == m_nNumberOfSprites)
    {
		m_bLoadResFinished = true;
    }
}

int SceneLoadLayer::getState()
{
	return m_state;
}

void SceneLoadLayer::setState(int state)
{
	m_state = state;
}

void SceneLoadLayer::update(float dt)
{
	static int _tick = 0;
	Slider * slider = NULL;
	if(layer != NULL)
		slider = (Slider*)(layer->getChildByName(LOADSCENE_SLIDER_NAME));
	if(m_state == State_WaitAction)
	{
		if(GameUtils::millisecondNow() - time_ > 1.0f * 350)
		{
			createUI();

			m_state = State_Init;
		}
	}
	else if(m_state == State_Login)
	{
		//do nothing
	}
	else if(m_state == State_Init)
	{
		initNextLevel();

		// remove the game scene
		auto pGameSceneState = (GameSceneState*)this->getParent();
		auto pGameSceneNode = pGameSceneState->getChildByTag(GameSceneState::TAG_GAMESCENE);
		if(pGameSceneNode != NULL)
		{
			pGameSceneNode->pause();
			pGameSceneNode->removeFromParent();
		}

		slider->setPercent(20);
		m_state = State_LoadResStep_0;
	}
	else if(m_state == State_LoadResStep_0)
	{
		loadCommonRes();
		slider->setPercent(40);
		m_state = State_LoadResStep_1;
	}
	else if(m_state == State_LoadResStep_1)
	{
		loadUIWidgetRes();
		slider->setPercent(55);
		m_state = State_LoadResStep_2;
	}
	else if(m_state == State_LoadResStep_2)
	{
		preLoadResource(GameView::getInstance()->getMapInfo()->mapid().c_str());
		slider->setPercent(80);
		m_state = State_LoadResStep_3;
	}
	else if(m_state == State_LoadResStep_3)
	{
		slider->setPercent(99);

		if(m_bLoadResFinished)
		{
			auto pGameSceneState = (GameSceneState*)this->getParent();
			pGameSceneState->prepareGameState();

			_tick = 0;
			m_state = State_Finished;
		}
	}
	else if(m_state == State_Finished)
	{
		//this->removeChild(m_pLabelLoading, true);
        //this->removeChild(m_pLabelPercent, true);
		if(_tick++ > 30)
		{
			auto pGameSceneState = (GameSceneState*)this->getParent();
			pGameSceneState->changeToGameState();
		}
	}
	else if(m_state == State_Idle)
	{
		//lazy...
	}
}

bool SceneLoadLayer::isFinishedState()
{
	if(m_state == State_Finished)
		return true;

	return false;
}

void SceneLoadLayer::preLoadResource(const char *path)
{
	//std::vector<std::string>* sceneResList = GameSceneLayer::generateResourceList("xsc.level");
	std::vector<std::string>* sceneResList = GameSceneLayer::generateResourceList(path);

	m_nNumberOfSprites = sceneResList->size();;
	m_nNumberOfLoadedSprites = 0;

	// load textrues
	vector<std::string>::iterator iter;
	for (iter = sceneResList->begin(); iter != sceneResList->end(); ++iter)
	{
#if LOAD_METHOD_ASYNC
		// addImageAsync() method maybe has memory lead, so please pay attention to it
		Director::getInstance()->getTextureCache()->addImageAsync((*iter).c_str(), this, callfuncO_selector(LoadSceneLayer::loadingCallBack));
#else
		// synchronize load resource, please set finished flag immediately
		Director::getInstance()->getTextureCache()->addImage((*iter).c_str());
		m_bLoadResFinished = true;
#endif
	}

	delete sceneResList;
}

std::string SceneLoadLayer::getLoadingText()
{
	unsigned int size = LoadingTextData::s_LoadingTexts.size();
	if(size <= 0)
	{
		LoadingTextData::load("game.db");
		size = LoadingTextData::s_LoadingTexts.size();
	}

	unsigned int index = GameUtils::getRandomNum(size);
	//CCLOG("index: %d, text: %s", index, LoadingTextData::s_LoadingTexts.at(index).c_str());
	CCAssert(index >= 0 && index < size, "out of range");
	return LoadingTextData::s_LoadingTexts.at(index);
}

char* SceneLoadLayer::getLoadingImage()
{
	unsigned int size = 4;
	char* image[] = {"images/loding1.jpg", "images/loding2.jpg", "images/loding3.jpg", "images/loding4.jpg"};
	unsigned int index = GameUtils::getRandomNum(size);
	CCAssert(index >= 0 && index < size, "out of range");
	return image[index];
}

void SceneLoadLayer::loadCommonRes()
{
}

void SceneLoadLayer::loadLayoutFromJson(Layout** dest, const char* fileName)
{
	if(*dest == NULL)
	{
		long long currentTime = GameUtils::millisecondNow();

		std::string fullPathName = "res_ui/";
		fullPathName.append(fileName);

		*dest = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile(fullPathName.c_str());
		(*dest)->retain();

		long deltaTime = GameUtils::millisecondNow() - currentTime;
		CCLOG("load UI, cost time: %d", deltaTime);
	}
}

void SceneLoadLayer::loadUIWidgetRes()
{
	loadLayoutFromJson(&LoadSceneLayer::s_pPanel, "renwubeibao_1.json");
	loadLayoutFromJson(&LoadSceneLayer::SkillSceneLayer, "jineng_1.json");
	loadLayoutFromJson(&LoadSceneLayer::equipMentPanel, "zhuangbei_1.json");
	loadLayoutFromJson(&LoadSceneLayer::onlineAwardsLayout, "OnlineAwards_1.json");
	loadLayoutFromJson(&LoadSceneLayer::GeneralsLayer, "wujiang_1.json");
	loadLayoutFromJson(&LoadSceneLayer::friendPanelBg, "haoyou_1.json");
	loadLayoutFromJson(&LoadSceneLayer::auctionPanel, "jishouhang_1.json");
	loadLayoutFromJson(&LoadSceneLayer::mapPanel, "map_1.json");
	loadLayoutFromJson(&LoadSceneLayer::familyMainPanel, "family_main_1.json");
	loadLayoutFromJson(&LoadSceneLayer::signDailyLayout, "Registration_1.json");
	loadLayoutFromJson(&LoadSceneLayer::mailPanel, "youjian_1.json");
	loadLayoutFromJson(&LoadSceneLayer::onHookKillPanel, "on_hook_1.json");
	loadLayoutFromJson(&LoadSceneLayer::vipEveryDayRewardPanel, "vip1_1.json");
	loadLayoutFromJson(&LoadSceneLayer::activeLayout, "Activity_1.json");
	loadLayoutFromJson(&LoadSceneLayer::OffLineArenaLayer, "arena_1.json");
	loadLayoutFromJson(&LoadSceneLayer::SingleRecuriteResultLayer, "wujiang_get_popupo_1.json");
	loadLayoutFromJson(&LoadSceneLayer::TenTimesRecuriteResultLayer, "wujiang_SuperGet_popupo_1.json");
}

void SceneLoadLayer::initNextLevel()
{
	auto gv = GameView::getInstance();

	//set last mapId
	gv->getMapInfo()->setLastMapId(gv->getMapInfo()->mapid());

	gv->getMapInfo()->CopyFrom(s_newMapInfo);

	// init MyPlayer
	gv->myplayer->clear();	
	gv->myplayer->setDissociate(false);
	gv->myplayer->setGameScene(NULL);
	gv->myplayer->setWorldPosition(s_myNewWorldPosition);

	//add by yangjun 2014.9.22
	if (gv->getMapInfo()->mapid() == "mjrk1.level")
	{
		if (NewCommerStoryManager::getInstance()->m_storyState == NewCommerStoryManager::ss_notStart)
		{
			NewCommerStoryManager::getInstance()->setIsNewComer(true);
			NewCommerStoryManager::getInstance()->startStory();
		}
	}

	//add by yangjun 2014.11.17
	if (NewCommerStoryManager::getInstance()->IsNewComer())
	{
		if (gv->getMapInfo()->mapid() != "mjrk1.level"&&gv->getMapInfo()->mapid() != "smjt.level")
		{
			NewCommerStoryManager::getInstance()->endStory();
		}
	}

	if (s_newMapInfo.maptype() == coliseum)
	{
		OffLineArenaState::setIsCanTouch(false);
	}

	if (MissionManager::getStatus() == MissionManager::status_waitForTransport)
	{
		MissionManager::setStatus(MissionManager::status_transportFinished);
	}

	if (ActiveManager::get_status() == ActiveManager::status_waitForTransport)
	{
		ActiveManager::set_status(ActiveManager::status_transportFinished);
	}

	if (s_newMapInfo.maptype() == tower)
	{
		auto copyResultui = (SingleCopyResult*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagSingCopyResutUi);
		if (copyResultui)
		{
			copyResultui->closeAnim();
		}
	}
}