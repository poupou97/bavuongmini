#ifndef H_EFFECTDISPATCH_H
#define H_EFFECTDISPATCH_H

#include "../ui/extensions/uiscene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
/*
*dispatch show effect in order
*/

enum
{
	type_normal = 0,
	type_fightPoint,
	type_itemWave,
	type_equipWave,
	type_skillStudy,
	type_recruit,
	//type_finishMission,
	//type_levelUp,
};


/////////////////////////////
class EffectBaseInfo:public Node
{
public:
	EffectBaseInfo();
	~EffectBaseInfo();

	void setType(int type_);
	int getType();
private:
	int m_type;
};

//////////////////////////////
class EffectDispatcher:public UIScene
{
public:
	EffectDispatcher(void);
	~EffectDispatcher(void);

	static EffectDispatcher * create();
	bool init();

	void onEnter();
	void onExit();

	static void pushEffectToVector(EffectBaseInfo * effectInfo);
	void updateEffect(float dt);

	static std::vector<EffectBaseInfo *> effectCollectionVector;
};

/////////////////////////// fightpoint change effect
class EffectOfFightPointChange:public EffectBaseInfo
{
public:
	EffectOfFightPointChange();
	~EffectOfFightPointChange();

	virtual void showEffect();

	void setOldFight(int value_);
	int getOldFight();

	void setNewFight(int value_);
	int getNewFight();
private:
	int m_oldFight;
	int m_newFight;
};

///////////////////////
/*
*����Ქ�Ÿ�����ͨ��Ч�������κ���ֵ�ģ����磺����Ч�����������Ч�������Ч
*ֻ��Ҫ������Ч��·�����ͻᲥ� 
*/
class EffectOfNormal:public EffectBaseInfo
{
public:
	EffectOfNormal();
	~EffectOfNormal();

	virtual void showEffect();

	//set effect path 
	void setEffectStrPath(std::string effectPath);
	std::string getEffectStrPath();
private:
	std::string m_effectStr;
};

////////////////////////item wave effect
class GoodsInfo;
class EffectOfItemWave:public EffectBaseInfo
{
public:
	EffectOfItemWave();
	~EffectOfItemWave();

	virtual void showEffect();

	void setItemInfo(GoodsInfo * goods);
	GoodsInfo * getItemInfo();
private:
	GoodsInfo * m_goodsinfo;
};

//////////////////////////equipItem shortCut 
/*ſ�ݴ�װ��*/
class FolderInfo;
class ShortCutItem;
class EffectEquipItem : public EffectBaseInfo
{
public:
	EffectEquipItem();
	~EffectEquipItem();

	virtual void showEffect();

	void setFolderInfo(FolderInfo * foler);
	FolderInfo * getFolderInfo();

	void setId(long long id_);
	long long getId();

	void setPriority(bool isValue);
	bool getPriority();
public:
	static std::vector<FolderInfo * > folderItemVector;

	void starCallBack(Ref * obj);

	static void reloadShortItem();

	static void refreshFolderIndex();

private:
	FolderInfo * m_folderInfo;
	long long m_id;
	bool m_priority;
};

//////////////////////////study skill 
class EffectStudySkill : public EffectBaseInfo
{
public:
	EffectStudySkill();
	~EffectStudySkill();

	virtual void showEffect();

	std::string getRoleFirstSkillId();
	std::string getSkillName(std::string m_skillId);
	std::string getSkillIcon(std::string m_skillId);

private:
	int m_uitag;
	FolderInfo * m_folderInfo;
	long long m_id;
	bool m_priority;
};

//////////////////////////normal recurit general card
class EffectRecruit : public EffectBaseInfo
{
public:
	EffectRecruit();
	~EffectRecruit();

	virtual void showEffect();
};
#endif