#ifndef _GAMESCENE_CAMERA_H_
#define _GAMESCENE_CAMERA_H_

#include "cocos2d.h"
USING_NS_CC;

class GameSceneLayer;
class GameActor;

#define SCENE_CAMERA_OFFSET_Y 30
#define SHAKE_SCREEN_DEFAULT_TIME 0.25f
#define SHAKE_SCREEN_DEFAULT_OFFSET 10

/**
 the game scene's camera node
 GameSceneLayer::setView(this->getPosition())
 */
class GameSceneCamera : public Node
{
public:
	enum {
		camera_behaviour_follow_actor = 0,
		camera_behaviour_move = 1,
		camera_behaviour_shakescreen = 2,
	};

public:
	GameSceneCamera();
	~GameSceneCamera();

	static GameSceneCamera* create(GameSceneLayer* scene, int behaviour);
	bool init(GameSceneLayer* scene, int behaviour);

	void MoveTo(Vec2 target, float time);
	void FollowActor(GameActor* actor);

	inline int getBehaviour() { return m_behaviour; };

	/** shaking the game scene */
	void shakeScreen(GameSceneLayer* scene, float time = SHAKE_SCREEN_DEFAULT_TIME);
	void zoomScreen(GameSceneLayer* scene, Vec2& centerPoint);
	void zoom(GameSceneLayer* scene, Vec2& centerPoint, float zoomFrom, float zoomTo, float duration);

	/** shaking the whole screen */
	static void shakeScreen(float time = SHAKE_SCREEN_DEFAULT_TIME, int offset = SHAKE_SCREEN_DEFAULT_OFFSET);

	Vec2 getSceneCameraPosition(GameActor* actor);

protected:
	void restoreCamera(float dt);

private:
	int m_behaviour;
	GameSceneLayer* m_pCurScene;
	GameActor* m_pFollowActor;
};

#endif