#include "GameSceneMusicController.h"

#include "SimpleAudioEngine.h"
#include "../utils/GameUtils.h"
#include "GameAudio.h"
#include "GameView.h"
#include "../messageclient/element/CMapInfo.h"

using namespace CocosDenshion;

GameSceneMusicController::GameSceneMusicController()
: m_state(music_state_init)
{
	m_stateInitTime = GameUtils::millisecondNow();
	m_state = music_state_prewait;
}

GameSceneMusicController::~GameSceneMusicController()
{
}

void GameSceneMusicController::update(float dt)
{
	switch(m_state) {
	case music_state_init:
		playSceneMusic();
		m_state = music_state_playing;
		break;

	case music_state_playing:
		if(!SimpleAudioEngine::getInstance()->isBackgroundMusicPlaying())
		{
			m_stateStartTime = GameUtils::millisecondNow();
			m_state = music_state_wait;
		}
		break;

	case music_state_wait:
		if(GameUtils::millisecondNow() - m_stateStartTime > 15000)   // 30 second
		{
			m_state = music_state_init;
		}
		break;
	case music_state_prewait:
		if(GameUtils::millisecondNow() - m_stateInitTime > 2000)   // 30 second
		{
			m_state = music_state_init;
		}
		break;
	}
}

char* GameSceneMusicController::getRandomMusicFileName(char** name, int size)
{
	unsigned int index = GameUtils::getRandomNum(size);
	CCAssert(index >= 0 && index < size, "out of range");
	return name[index];
}

void GameSceneMusicController::playSceneMusic()
{
	int size;

	SimpleAudioEngine::getInstance()->stopBackgroundMusic();
	if(!memcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(), "jyc", 3)
		||!memcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(), "cac", 3))
	{
		char* name[] = {MUSIC_KING, MUSIC_IMPERIAL};
		size = 2;
		GameUtils::playGameSound(getRandomMusicFileName(name, size), 1, false);
	}
	else if(!memcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(), "jjc", 3)
		||GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::copy
		||GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::guildFight
		||GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::tower
		||!memcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(), "xl.level", 8))
	{
		if(!memcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(), "mjrk1.level", 11))
		{
			GameUtils::playGameSound(MUSIC_NEW1, 1, false);
			return;
		}
		else if(!memcmp(GameView::getInstance()->getMapInfo()->mapid().c_str(), "smjt.level", 11))
		{
			GameUtils::playGameSound(MUSIC_NEW2, 1, false);
			return;
		}
		char* name[] = {MUSIC_RAID, MUSIC_BORDER};
		size = 2;
		GameUtils::playGameSound(getRandomMusicFileName(name, size), 1, false);
	}
	else
	{
		char* name[] = {MUSIC_ROOKIE, MUSIC_OTHER};
		size = 2;
		GameUtils::playGameSound(getRandomMusicFileName(name, size), 1, false);
	}
}