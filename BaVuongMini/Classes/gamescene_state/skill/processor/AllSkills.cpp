#include "AllSkills.h"

#include "../../role/BaseFighter.h"
#include "../../role/BasePlayer.h"
#include "../../role/General.h"
#include "../../role/MyPlayer.h"
#include "../../role/BaseFighterOwnedStates.h"
#include "../../role/BaseFighterOwnedCommand.h"
#include "../../role/SimpleActor.h"
#include "../../GameSceneState.h"
#include "../SkillSeed.h"
#include "../CSkillResult.h"
#include "../GameFightSkill.h"
#include "../../../utils/GameUtils.h"
#include "../../../GameView.h"
#include "../../../legend_engine/CCLegendAnimation.h"
#include "../../GameSceneEffects.h"
#include "../../role/ActorUtils.h"
#include "../../role/GameActorAnimation.h"
#include "../../GameSceneCamera.h"
#include "../../../legend_engine/LegendAAnimation.h"
#include "../../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../MainScene.h"
//#include "actions/ActionInterval.h"

#define SKILL_LAUNCH_SPECIAL_EFFECT_1 "animation/player/zsgr_npj/launch_foot.anm"

//////////////////////////////////////
IMPLEMENT_CLASS(AX1pfczSP)

AX1pfczSP::AX1pfczSP(){

}

AX1pfczSP::~AX1pfczSP(){

}

void* AX1pfczSP::createInstance()
{
	return new AX1pfczSP() ;
}

void AX1pfczSP::onSkillBegan(BaseFighter* attacker)
{
	//SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_2.plist", 1.0f);

	//// add the launch effect on the attacker
	//auto pAnim = CCLegendAnimation::create(SKILL_LAUNCH_SPECIAL_EFFECT_1);
	//pAnim->setScale(0.75f);
	//attacker->addEffect(pAnim, true, 0);

	// add the launch effect on the attacker
	auto pAnm = CCLegendAnimation::create("animation/texiao/renwutexiao/CHONGJIBO/chongjibo/chongjibo_xuli4.anm");
	pAnm->setScale(1.4f);
	//pAnm->setScale(1.0f, 1.3f);
	pAnm->setPlaySpeed(1.5f);
	attacker->addEffect(pAnm, true, 0);   // 25

	if(attacker->isMyPlayer())
	{
		// the big actor shoot the big arrow
		auto action = Sequence::create(
			ScaleTo::create(0.1f, 1.4f),
			DelayTime::create(0.7f),
			ScaleTo::create(0.4f, 1.0f),
			NULL);
		attacker->getAnim()->runAction(action);
	}
}

void AX1pfczSP::onSkillReleased(BaseFighter* attacker)
{
	auto scene = attacker->getGameScene();
	auto target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));

	const float effect_scale_value = 1.2f;
	bool bFlipX= false;
	if(attacker->getAnimDir() == 1)   // face to left
	{
		this->setEffectScaleX(-effect_scale_value);
		this->setEffectScaleY(effect_scale_value);
		bFlipX = true;
	}
	else if(attacker->getAnimDir() == 3)   // face to right
	{
		this->setEffectScaleX(effect_scale_value);
		this->setEffectScaleY(effect_scale_value);
	}
	else
	{
		if(target != NULL)
		{
			if(attacker->getWorldPosition().x > target->getWorldPosition().x)
			{
				this->setEffectScaleX(-effect_scale_value);
				this->setEffectScaleY(effect_scale_value);
				bFlipX = true;
			}
		}
	}

	MomentSkillProcessor::onSkillReleased(attacker);

	// extra four dragons
	int offsets[4][2] = {{128, 0}, {0, 128}, {-128, 0}, {0, -128}};
	const char* effectPathName = "animation/texiao/renwutexiao/AX1/ax1.anm";
	for(int i = 0; i < 4; i++)
	{
		SimpleActor* simpleActor = new SimpleActor();
		simpleActor->setGameScene(scene);
		simpleActor->setWorldPosition(Vec2(this->getPosX() + offsets[i][0], this->getPosY() + offsets[i][1]));
		simpleActor->loadAnim(effectPathName);
		if(bFlipX)
		{
			simpleActor->setScaleX(-0.7f);
			simpleActor->setScaleY(0.7f);
		}
		else
		{
			simpleActor->setScale(0.7f);
		}
		//simpleActor->setScale(m_effectScaleX, m_effectScaleY);
		scene->getActorLayer()->addChild(simpleActor, SCENE_ROLE_LAYER_BASE_ZORDER);
		simpleActor->release();
	}

	if(target != NULL)
	{
		if(!target->isDead())
			SkillProcessor::flyup(attacker, target);

		// add special effect on the defender
		auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/DZ1/dz1_2.anm");
		pAnim->setScale(0.8f);
		target->addEffect(pAnim, false, 60);
	}

	//// show ground effect
	//showGroundEffect("animation/texiao/renwutexiao/AX1/ax1_2.anm", scene, 0.5f, 1.5f, 2.0f);
}

//////////////////////////////////////
IMPLEMENT_CLASS(AX2yjdqSP) 

AX2yjdqSP::AX2yjdqSP()
	:m_direction_point(Vec2(0, 0))
	,m_degree(0.0f)
{

}

AX2yjdqSP::~AX2yjdqSP(){

}

void* AX2yjdqSP::createInstance()
{
	return new AX2yjdqSP() ;
}

void AX2yjdqSP::onSkillBegan(BaseFighter* attacker)
{
	//// add the launch effect on the attacker
	//auto pAnim = CCLegendAnimation::create(SKILL_LAUNCH_SPECIAL_EFFECT_1);
	//attacker->addEffect(pAnim, true, 0);

	// add the launch effect on the attacker
	std::string str_anim = "animation/texiao/renwutexiao/AX2/ax2_1.png";
	addMagicHaloToGround(attacker->getGameScene(), attacker->getWorldPosition(), str_anim, 2.0f, 1.0f, 1.5f, 0.75f);
}

void AX2yjdqSP::update(float dt)
{
	if(isRelease()){
		return;
	}

	if(this->getSkillResult() != NULL)
	{
		auto result = getSkillResult();
		auto scene = GameView::getInstance()->getGameScene();
		auto attacker = dynamic_cast<BaseFighter*>(scene->getActor(result->source()));
		if(attacker == NULL || attacker->isDead())
		{
			this->setRelease(true);
			return;
		}

		// charge is over
		ActionChargeContext* context = (ActionChargeContext*)attacker->getActionContext(ACT_CHARGE);
		if(context->finished)
			//if(GameUtils::millisecondNow()- m_chargeStartTime > m_chargeTotalTime * 1000)
		{	
			if(result->defenders_size() > 0)
			{
				Defender def = result->defenders(0);
				auto defender = dynamic_cast<BaseFighter*>(scene->getActor(def.target()));
				if(defender != NULL)
				{
					// add impact effect ( particle )
					//ParticleSystem* particleEffect = ParticleSystemQuad::create("animation/texiao/particledesigner/dust1.plist");
					//particleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
					//particleEffect->setAnchorPoint(Vec2(0.5f,0.5f));
					//particleEffect->setScaleX(2.0f);
					//particleEffect->setScaleY(1.4f);
					//particleEffect->setStartSize(100.f);
					////particleEffect->setLife(1.5f);
					////particleEffect->setLifeVar(0);
					//particleEffect->setAutoRemoveOnFinish(true);
					//Vec2 cocosPosition = scene->convertToCocos2DSpace(defender->getWorldPosition());
					//particleEffect->setPosition(cocosPosition);
					//scene->getActorLayer()->addChild(particleEffect, SCENE_STORY_LAYER_BASE_ZORDER);

					// add impact effect ( legend animation )
					SimpleActor* simpleActor = new SimpleActor();
					simpleActor->setWorldPositionZ(40);
					simpleActor->setGameScene(scene);
					simpleActor->setWorldPosition(Vec2(defender->getRealWorldPosition().x, defender->getRealWorldPosition().y + 1));
					simpleActor->loadAnim("animation/texiao/renwutexiao/AX2/ax2_2.anm");
					//simpleActor->getLegendAnim()->setPlaySpeed(0.9f);
					simpleActor->setScale(1.5f);
					scene->getActorLayer()->addChild(simpleActor);
					simpleActor->release();

					//
					this->createDragonAction(context);
				}
			}

			attacker->changeAction(ACT_STAND);
			this->setRelease(true);
		}
	}
	else   // caused by onSkillReleased
	{
		this->setRelease(true);
	}
}

void AX2yjdqSP::onSkillApplied(BaseFighter* attacker)
{
	// 根据CSkillResult, 设置冲锋的参数
	auto ret = getSkillResult();
	if(ret == NULL)
	{
		this->setRelease(true);
		return;
	}

	auto scene = attacker->getGameScene();

	Vec2 targetWorldPosition = Vec2(ret->x(), ret->y());

	// check the target position
	Vec2 startPos = attacker->getWorldPosition();
	int tileX = scene->positionToTileX(targetWorldPosition.x);
	int tileY = scene->positionToTileY(targetWorldPosition.y);
	if(scene->isLimitOnGround(tileX, tileY)) 
	{
		Vec2* newTarget = scene->findNearestPositionToTarget(startPos.x, startPos.y, targetWorldPosition.x, targetWorldPosition.y);
		if(newTarget != NULL)
		{
			targetWorldPosition.setPoint(newTarget->x, newTarget->y);
			delete newTarget;
		}
	}
	Vec2 chargeTargetPosition = scene->convertToCocos2DSpace(targetWorldPosition);

	ActionChargeContext context;
	context.chargeMoveSpeed = 800.f;   // 1000.f
	context.chargetTargetPosition = chargeTargetPosition;
	context.chargeTime = attacker->getWorldPosition().getDistance(targetWorldPosition) / context.chargeMoveSpeed;
	context.startTime = GameUtils::millisecondNow();
	context.setContext(attacker);

	//attacker->stopAllCommand();
	attacker->stopCurrentCommand();
	if(!attacker->isDead())
		attacker->changeAction(ACT_CHARGE);

	m_chargeTotalTime = context.chargeTime;
	m_chargeStartTime = GameUtils::millisecondNow();

	// add the special effect
	auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/AX2/ax2.anm");
	pAnim->setPlayLoop(true);
	float degree = GameUtils::getDegree(attacker->getPosition(), chargeTargetPosition);
	pAnim->setRotation(degree);
	pAnim->setOpacity(225);

	this->m_degree = degree;

	// calc the defender offset point
	m_direction_point = GameUtils::getDirection(attacker->getPosition(), chargeTargetPosition);

	FiniteTimeAction*  action = Sequence::create(
		DelayTime::create(m_chargeTotalTime),
		RemoveSelf::create(),
		NULL);
	pAnim->runAction(action);

	attacker->addEffect(pAnim, false, BASEFIGHTER_ROLE_HEIGHT/2);

	// synchronize the real position
	attacker->setRealWorldPosition(Vec2(ret->x(), ret->y()));

	// camera special effect
	if(attacker->isMyPlayer())
	{
		Vec2 sourcePoint = attacker->getWorldPosition();
		scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	}

	// body blur
	auto pBody = attacker->getAnim()->getAnim(ANI_COMPONENT_BODY, ANI_RUN);
	std::string animFileName = pBody->getAnimationData()->getAnmFileName();
	for(int i = 1; i < 4; i++)
	{
		SimpleActor* pActor = new SimpleActor();
		pActor->setGameScene(scene);
		pActor->setWorldPosition(attacker->getWorldPosition());
		pActor->loadAnim(animFileName.c_str(), false, attacker->getAnimDir());
		scene->getActorLayer()->addChild(pActor, SCENE_ROLE_LAYER_BASE_ZORDER);
		pActor->release();

		int alpha = 255 - (i+1) * 35;
		if(alpha < 0)
			alpha = 0;
		pActor->setOpacity(alpha);
		pActor->setColor(Color3B(255, 255, 255));

		FiniteTimeAction* simpleActorAction = Sequence::create(
			DelayTime::create(i * 0.08f),
			MoveTo::create(context.chargeTime, context.chargetTargetPosition),
			RemoveSelf::create(),
			NULL);
		pActor->runAction(simpleActorAction);
	}
}

void AX2yjdqSP::createDragonAction( ActionChargeContext* pContext)
{
	if (0 == this->m_degree)
	{
		return;
	}

	auto scene = GameView::getInstance()->getGameScene();

	//Vec2 targetWorldPosition = defdenerPos;
	Vec2 targetWorldPosition = scene->convertToGameWorldSpace(pContext->chargetTargetPosition);

	// add impact effect ( legend animation )
	SimpleActor* simpleActor = new SimpleActor();
	simpleActor->setWorldPositionZ(40);
	simpleActor->setGameScene(scene);
	simpleActor->setWorldPosition(Vec2(targetWorldPosition.x, targetWorldPosition.y));
	simpleActor->loadAnim("animation/texiao/renwutexiao/AX2/ax2.anm", true);

	// add the special effect
	auto pAnim = simpleActor->getLegendAnim();
	pAnim->setPlayLoop(true);
	pAnim->setOpacity(225);
	pAnim->setRotation(this->m_degree);


	// 龙头向前冲的效果
	float distance = 64 * 2.0f;   // 64 * 2.5f
	Vec2 offset = Vec2(m_direction_point.x, m_direction_point.y)*distance;

	const float duration_time = 0.3f;
	FiniteTimeAction* spawn_action = Spawn::create(
		MoveBy::create(duration_time, offset),
		FadeTo::create(duration_time, 225),
		ScaleTo::create(duration_time, 1.8f),
		NULL);

	FiniteTimeAction*  action = Sequence::create(
		spawn_action,
		RemoveSelf::create(),
		NULL);
	pAnim->runAction(action);

	simpleActor->runAction(Sequence::create(
		DelayTime::create(duration_time),
		RemoveSelf::create(),
		NULL));

	scene->getActorLayer()->addChild(simpleActor);
	simpleActor->release();
}

////////////////////////////////////////////////////
IMPLEMENT_CLASS(AX3yrpxSP)

AX3yrpxSP::AX3yrpxSP(){

}

AX3yrpxSP::~AX3yrpxSP(){

}

void* AX3yrpxSP::createInstance()
{
	return new AX3yrpxSP() ;
}

void AX3yrpxSP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	if(attacker->isMyPlayer())
	{
		auto scene = attacker->getGameScene();
        Vec2 sourcePoint = attacker->getWorldPosition();
		scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	}
}

//////////////////////////////////////
IMPLEMENT_CLASS(AY1hxcqSP)

AY1hxcqSP::AY1hxcqSP(){

}

AY1hxcqSP::~AY1hxcqSP(){

}

void* AY1hxcqSP::createInstance()
{
	return new AY1hxcqSP() ;
}

void AY1hxcqSP::onSkillBegan(BaseFighter* attacker)
{
	// add the launch effect on the attacker
	auto pAnim = CCLegendAnimation::create(SKILL_LAUNCH_SPECIAL_EFFECT_1);
	pAnim->setScale(1.1f);
	pAnim->setPlaySpeed(1.2f);
	attacker->addEffect(pAnim, true, 0);

	// zoom screen
	if(attacker->isMyPlayer())
	{
		auto scene = attacker->getGameScene();
        Vec2 sourcePoint = attacker->getWorldPosition();
		scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	}

	// the player jumps
	const float jumpDuration = 0.55f;
	const float jumpHeight = BASEFIGHTER_ROLE_HEIGHT * 1.5f;

	// jump action
	//FiniteTimeAction* action = Sequence::create(
	//	JumpBy::create(jumpDuration, Vec2::ZERO, jumpHeight, 1),
	//	NULL);
	//attacker->getAnim()->runAction(action);

	int animNameIdx = attacker->getAnim()->getAnimName();
	// body
	auto pBody = attacker->getAnim()->getAnim(ANI_COMPONENT_BODY, animNameIdx);
	// jump action
	FiniteTimeAction* action = Sequence::create(
		JumpBy::create(jumpDuration, Vec2::ZERO, jumpHeight, 1),
		NULL);
	pBody->runAction(action);
	// shadow
	auto pBodyShadow = attacker->getAnim()->getAnim(ANI_COMPONENT_SHADOW, animNameIdx);
	// jump action
	action = Sequence::create(
		JumpBy::create(jumpDuration, Vec2::ZERO, jumpHeight, 1),
		NULL);
	pBodyShadow->runAction(action);
	if(attacker->isReflection())
	{
		auto pReflectionANim = attacker->getAnim()->getAnim(ANI_COMPONENT_REFLECTION, animNameIdx);
		// jump action
		action = Sequence::create(
			JumpBy::create(jumpDuration, Vec2::ZERO, -jumpHeight/2, 1),   // 当角色蹦起来时，模拟其水中倒影
			NULL);
		pReflectionANim->runAction(action);
	}

	//// 拖影效果
	//int animActionIdx = attacker->getAnim()->getActionIdx();
	//auto pBody = attacker->getAnim()->getAnim(ANI_COMPONENT_BODY, animActionIdx);
	//std::string animFileName = pBody->getAnimationData()->getAnmFileName();

	//auto scene = attacker->getGameScene();
	//const int shadowNumber = 2;
	//for(int i = 1; i < shadowNumber; i++)
	//{
	//	SimpleActor* pActor = new SimpleActor();
	//	pActor->setGameScene(scene);
	//	pActor->setWorldPosition(attacker->getWorldPosition());
	//	pActor->loadAnim(animFileName.c_str());
	//	scene->getActorLayer()->addChild(pActor);
	//	pActor->release();

	//	int alpha = 255 - i * 75;
	//	if(alpha < 0)
	//		alpha = 0;
	//	pActor->setOpacity(alpha);
	//	//pActor->setColor(Color3B(150, 150, 255));
	//	pActor->setColor(Color3B(255, 255, 128));

	//	FiniteTimeAction* simpleActorAction = Sequence::create(
	//		DelayTime::create(i * 0.03f),
	//		JumpBy::create(jumpDuration, Vec2::ZERO, jumpHeight, 1),
	//		RemoveSelf::create(),
	//		NULL);
	//	pActor->runAction(simpleActorAction);
	//}
}

void AY1hxcqSP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	auto scene = attacker->getGameScene();
	if(attacker->isMyPlayer())
		scene->getSceneCamera()->shakeScreen(scene);

	// show ground effect
	showGroundEffect("animation/texiao/renwutexiao/AY1/ay1_1.anm", scene, 0.24f, 1.5f, 2.0f);
}

void AY1hxcqSP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		SkillProcessor::hitback(attacker, defender, 256, 0.25f);   // 128, 0.3f

		// show effect only
		auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/DZ1/dz1_2.anm");
		pAnim->setPlaySpeed(1.0f);
		pAnim->setScale(1.15f);
		defender->addEffect(pAnim, false, 60);
	}

	// no need to update, so release directly
	this->setRelease(true);
}

//////////////////////////////////////
IMPLEMENT_CLASS(AY2lphjSP)

AY2lphjSP::AY2lphjSP()
{

}

AY2lphjSP::~AY2lphjSP(){

}

void* AY2lphjSP::createInstance()
{
	return new AY2lphjSP() ;
}

void AY2lphjSP::onSkillBegan(BaseFighter* attacker)
{
	auto pAnm = CCLegendAnimation::create("animation/texiao/renwutexiao/CHONGJIBO/chongjibo/chongjibo_xuli4.anm");
	pAnm->setScale(1.2f);
	pAnm->setPlaySpeed(1.5f);

	attacker->addEffect(pAnm, false, 15);
}

void AY2lphjSP::onSkillApplied( BaseFighter* attacker )
{
	auto scene = attacker->getGameScene();

	// launch effect
	auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/CHONGJIBO/chongjibo4/chongjibo4.anm");
	pAnim->setPlaySpeed(1.4f);
	pAnim->setScale(1.6f, 0.8f);
	attacker->addEffect(pAnim, false, 0/*BASEFIGHTER_ROLE_HEIGHT/3*/);

	// ground effect
	SkillProcessor::addBreathingHaloToGround(scene, attacker->getWorldPosition(),
		"animation/texiao/renwutexiao/CHONGJIBO/chongjibo4/chongjibo4_ground.anm");

	this->setRelease(true);
}


//////////////////////////////////////
IMPLEMENT_CLASS(AY3ympcSP)

AY3ympcSP::AY3ympcSP(){

}

AY3ympcSP::~AY3ympcSP(){

}

void* AY3ympcSP::createInstance()
{
	return new AY3ympcSP() ;
}

void AY3ympcSP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		SkillProcessor::hitback(attacker, defender, 256, 0.25f);
	}

	// no need to update, so release directly
	this->setRelease(true);
}

void AY3ympcSP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	//auto scene = attacker->getGameScene();
	//GameActor* me = scene->getActor(BaseFighter::getMyPlayerId());
	//GameSceneCamera* pCamera = scene->getSceneCamera();
	//pCamera->shakeScreen(pCamera->getSceneCameraPosition(me));

	auto scene = attacker->getGameScene();
	if(attacker->isMyPlayer())
		scene->getSceneCamera()->shakeScreen(scene);

	// show ground effect
	showGroundEffect("animation/texiao/renwutexiao/AY1/ay1_1.anm", scene, 0.45f, 2.5f, 3.0f);
}

//////////////////////////////////////
IMPLEMENT_CLASS(AZ1ltnhSP)

AZ1ltnhSP::AZ1ltnhSP(){

}

AZ1ltnhSP::~AZ1ltnhSP(){

}

void* AZ1ltnhSP::createInstance()
{
	return new AZ1ltnhSP() ;
}

void AZ1ltnhSP::onSkillBegan(BaseFighter* attacker)
{
	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_skill.plist", 1.0f);

	//// add the launch effect on the attacker
	//auto pAnim = CCLegendAnimation::create(SKILL_LAUNCH_SPECIAL_EFFECT_1);
	//pAnim->setScale(0.75f);
	//attacker->addEffect(pAnim, true, 0);

	// add the launch effect on the attacker
	// step 1
	const float disappear_duration = 0.4f;
	auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/AZ1/az1_revert.anm");
	pAnim->setScale(1.4f);
	pAnim->setPlayLoop(true);
	//pAnim->setColor(Color3B(133, 58, 133));
	//pAnim->setColor(Color3B(64, 64, 64));
	Action* action1 = Sequence::create(
		CCTintTo::create(disappear_duration,0, 255, 255),
		//FadeIn::create(disappear_duration),
		DelayTime::create(0.2f),
		FadeOut::create(0.2f),
		//DelayTime::create(0.3f),
		RemoveSelf::create(),
		NULL);
	pAnim->runAction(action1);
	attacker->addEffect(pAnim, true, 0);

	// setp 2
	pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/AZ1/az1_revert.anm");
	pAnim->setScale(1.0f);
	pAnim->setVisible(false);
	pAnim->setPlayLoop(true);
	Action* spawnAction = Spawn::create(
		ScaleTo::create(0.4f, 2.0f),
		FadeOut::create(0.6f),
		NULL);
	Action* action2 = Sequence::create(
		DelayTime::create(disappear_duration),
		Show::create(),
		spawnAction,
		RemoveSelf::create(),
		NULL);
	pAnim->runAction(action2);
	attacker->addEffect(pAnim, true, 0);
}

void AZ1ltnhSP::onSkillReleased(BaseFighter* attacker)
{
	//this->setEffectScaleX(1.2f);
	//this->setEffectScaleY(1.2f);
	BaseFighterCommandAttack* cmd = dynamic_cast<BaseFighterCommandAttack*>(attacker->getCommand());
	auto target = dynamic_cast<BaseFighter*>(attacker->getGameScene()->getActor(this->getSkillSeed()->defender));
	if(cmd != NULL)
	{
		if(cmd->attackCount == 0)
		{
			this->setEffectScaleX(1.0f);
			this->setEffectScaleY(1.0f);

			if(target != NULL)
				SkillProcessor::hitback(attacker, target, 32, 0.19f);   // 64, 0.19
		}
		else if(cmd->attackCount == 1)
		{
			this->setEffectScaleX(1.2f);
			this->setEffectScaleY(1.2f);

			if(target != NULL)
				SkillProcessor::hitback(attacker, target, 32, 0.19f);
		}
	}
	MomentSkillProcessor::onSkillReleased(attacker);

	// shake the screen
	auto scene = attacker->getGameScene();
	if(attacker->isMyPlayer())
		scene->getSceneCamera()->shakeScreen(scene);
}

void AZ1ltnhSP::onSkillApplied(BaseFighter* attacker)
{
	//auto result = getSkillResult();
	//CCAssert(result != NULL, "skill result should not be nil");
	//for (int i = 0; i < result->defenders_size(); i++) {
	//	Defender def = result->defenders(i);

	//	auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
	//	if(defender == NULL)
	//		continue;

	//	SkillProcessor::hitback(attacker, defender, 96, 0.15f);
	//}

	// no need to update, so release directly
	this->setRelease(true);
}

//////////////////////////////////////
IMPLEMENT_CLASS(AZ1SP)

AZ1SP::AZ1SP(){

}

AZ1SP::~AZ1SP()
{
}

void* AZ1SP::createInstance()
{
	return new AZ1SP() ;
}

//////////////////////////////////////
IMPLEMENT_CLASS(AZ2yfdgSP)

AZ2yfdgSP::AZ2yfdgSP(){

}

AZ2yfdgSP::~AZ2yfdgSP(){

}

void* AZ2yfdgSP::createInstance()
{
	return new AZ2yfdgSP() ;
}

void AZ2yfdgSP::onSkillBegan(BaseFighter* attacker)
{
	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_red.plist", 1.0f);

	//// add the launch effect on the attacker
	//auto pAnim = CCLegendAnimation::create(SKILL_LAUNCH_SPECIAL_EFFECT_1);
	//pAnim->setScale(0.75f);
	//attacker->addEffect(pAnim, true, 0);

	// add the launch effect on the attacker
	auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/CX1_1/cx1_1.anm");
	//pAnim->setPlaySpeed(0.9f);
	pAnim->setPlayLoop(true);
	Action* spawnAction = Spawn::create(
		ScaleTo::create(0.4f, 1.7f),
		FadeOut::create(0.5f),
		NULL);
	Action* action2 = Sequence::create(
		DelayTime::create(0.4f),
		ScaleTo::create(0.1f, 0.8f),
		spawnAction,
		RemoveSelf::create(),
		NULL);
	pAnim->runAction(action2);
	attacker->addEffect(pAnim, true, 0);

	// ground effect
	auto scene = attacker->getGameScene();
	auto target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
	if(target != NULL)
	{
		std::string str_anim = "animation/texiao/renwutexiao/AY1/ay1_ground.anm";

		SimpleActor* pActor = new SimpleActor();
		pActor->setGameScene(scene);
		pActor->setWorldPosition(target->getWorldPosition());
		pActor->loadAnim(str_anim.c_str(), true);
		pActor->setScale(1.0f);
		pActor->setLayerId(0);

		pActor->runAction(Sequence::create(
			ScaleTo::create(0.32f, 3.6f),
			ScaleTo::create(0.4f, 1.5f),
			FadeOut::create(0.7f),
			//DelayTime::create(0.6f),
			RemoveSelf::create(),
			NULL));

		scene->getActorLayer()->addChild(pActor, SCENE_GROUND_LAYER_BASE_ZORDER);
	}
}

void AZ2yfdgSP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	//// shake the screen
	//auto scene = attacker->getGameScene();
	//if(attacker->isMyPlayer())
	//	scene->getSceneCamera()->shakeScreen(scene);
}

void AZ2yfdgSP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		SkillProcessor::shake(defender, true, 12, 20.f);
	}

	// no need to update, so release directly
	this->setRelease(true);
}

//////////////////////////////////////
IMPLEMENT_CLASS(AZ3xjsbSP)

AZ3xjsbSP::AZ3xjsbSP(){
	m_fElapsed = 0.0f;
}

AZ3xjsbSP::~AZ3xjsbSP(){

}

void* AZ3xjsbSP::createInstance()
{
	return new AZ3xjsbSP() ;
}

void AZ3xjsbSP::onSkillBegan(BaseFighter* attacker)
{
	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_2.plist", 2.0f);

	// add the launch effect on the attacker
	auto pAnim = CCLegendAnimation::create(SKILL_LAUNCH_SPECIAL_EFFECT_1);
	pAnim->setPlaySpeed(1.5f);
	pAnim->setScale(1.0f);
	attacker->addEffect(pAnim, true, 0);

	//// add the launch effect on the attacker
	//BaseFighterCommandAttack* cmd = dynamic_cast<BaseFighterCommandAttack*>(attacker->getCommand());
	//if(cmd != NULL)
	//{
	//	if(cmd->attackCount == 0)
	//	{
	//		auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/AZ3/az3_revert.anm");
	//		pAnim->setPlaySpeed(1.4f);
	//		attacker->addEffect(pAnim, false, 0);
	//	}
	//}
}

void AZ3xjsbSP::onSkillReleased(BaseFighter* attacker)
{
	this->setEffectScaleX(1.2f);
	this->setEffectScaleY(1.2f);
	/**
	BaseFighterCommandAttack* cmd = dynamic_cast<BaseFighterCommandAttack*>(attacker->getCommand());
	if(cmd != NULL)
	{
		if(cmd->attackCount == 0)
		{
			//auto scene = attacker->getGameScene();
			//auto target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
			//SkillProcessor::flyup(attacker, target);
			this->setEffectScaleX(0.6f);
			this->setEffectScaleY(0.6f);
		}
		else if(cmd->attackCount == 1)
		{
			this->setEffectScaleX(0.95f);
			this->setEffectScaleY(0.95f);
		}
		else if(cmd->attackCount == 2)
		{
			auto scene = attacker->getGameScene();
			auto target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
			if(target != NULL && !target->isDead())
				SkillProcessor::flyup(attacker, target);

			this->setEffectScaleX(1.2f);
			this->setEffectScaleY(1.2f);
		}
	}
	**/

	MomentSkillProcessor::onSkillReleased(attacker);

	//if(attacker->isMyPlayer())
	//{
	//	auto scene = attacker->getGameScene();
 //       Vec2 sourcePoint = attacker->getWorldPosition();
	//	scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	//}
}

void AZ3xjsbSP::onSkillApplied(BaseFighter* attacker)
{
	m_fStartTime = GameUtils::millisecondNow();

	auto result = getSkillResult();
	// defender
	int size = result->defenders_size();
	if(size <= 0)
	{
		m_startPoint = ActorUtils::getDefaultSkillPosition(attacker, result->skillid().c_str(), result->showeffect().c_str());
		return;
	}

	BaseFighter* defender = NULL;
	float totalX = 0;
	float totalY = 0;
	for (int i = 0; i < size; i++) {
		Defender def = result->defenders(i);
		// get defender
		defender = dynamic_cast<BaseFighter*>(attacker->getGameScene()->getActor(def.target()));
		if(defender == NULL)
		{
			this->setRelease(true);
			return;
		}
		else
		{
			totalX += defender->getWorldPosition().x;
			totalY += defender->getWorldPosition().y;
			
			if(!defender->isDead())
				SkillProcessor::flyup(attacker, defender);
		}
	}

	m_startPoint = Vec2(totalX/size, totalY/size);

	//std::string str_anim = "animation/texiao/renwutexiao/magic_halo.png";
	//addMagicHaloToGround(attacker->getGameScene(), m_startPoint, str_anim, 3.0f, 1.5f, 2.0f, 1.0f);
}

void AZ3xjsbSP::update(float dt)
{
	if(isRelease()){
		return;
	}

	if(this->getSkillResult() != NULL)
	{
		auto result = getSkillResult();

		// attacker
		auto attacker = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(result->source()));
		if(attacker == NULL || attacker->isDead())
		{
			this->setRelease(true);
			return;
		}

		if(GameUtils::millisecondNow() - m_fStartTime > 1.0f * 1100)
		{
			this->setRelease(true);
			return;
		}

		// create effect
		float m_fInterval = 0.1f;
	    m_fElapsed += dt;
        if (m_fElapsed >= m_fInterval)
        {
 			RangeRandomSpecialEffect::generate(
				"animation/texiao/renwutexiao/AZ3/az3.anm",
				attacker->getGameScene(),
				m_startPoint,
				Size(128, 128));

            m_fElapsed = 0;
        }
	}
	else
	{
		this->setRelease(true);
	}
}

//////////////////////////////////////
IMPLEMENT_CLASS(BX1cqzmSP)

BX1cqzmSP::BX1cqzmSP(){

}

BX1cqzmSP::~BX1cqzmSP(){

}

void* BX1cqzmSP::createInstance()
{
	return new BX1cqzmSP() ;
}

void BX1cqzmSP::onSkillBegan(BaseFighter* attacker)
{
	//if(attacker->isMyPlayer())
	//{
	//	auto scene = attacker->getGameScene();
 //       Vec2 sourcePoint = attacker->getWorldPosition();
	//	scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	//}

	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_blue.plist", 1.0f);

	std::string str_anim = "animation/texiao/renwutexiao/BX1_1/bx1_ground.anm";
	auto pAnim_1 = CCLegendAnimation::create(str_anim);
	//pAnim_1->setScale(0.7f);
	attacker->addEffect(pAnim_1, true, 0);

	// add the launch effect
	if(this->getSkillSeed() != NULL)
	{
		auto scene = attacker->getGameScene();
		auto target = dynamic_cast<BaseFighter*>(scene->getActor(getSkillSeed()->defender));
		if(target != NULL)
		{
			auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/XZYD/xzyd.anm");
			pAnim->setPlayLoop(true);
			pAnim->setReleaseWhenStop(false);   // only play once, then release
			pAnim->setScale(0.7f);

			pAnim->runAction(Sequence::create(
				DelayTime::create(0.8f),
				RemoveSelf::create(),
				NULL));

			target->addEffect(pAnim, false, 0);

			auto pAnim_1 = CCLegendAnimation::create("animation/texiao/renwutexiao/BX1_1/bx1_1.anm");
			//pAnim_1->setScale(0.7f);
			pAnim_1->setPlaySpeed(1.2f);
			target->addEffect(pAnim_1, true, 0);
		}
	}
}

void BX1cqzmSP::onSkillApplied(BaseFighter* attacker)
{
	//// shake the screen
	//auto scene = attacker->getGameScene();
	//if(attacker->isMyPlayer())
	//	scene->getSceneCamera()->shakeScreen(scene);

	this->setRelease(true);
}

//////////////////////////////////////
IMPLEMENT_CLASS(BX1SP)

BX1SP::BX1SP(){
}

BX1SP::~BX1SP()
{
}

void* BX1SP::createInstance()
{
	return new BX1SP() ;
}

//////////////////////////////////////
IMPLEMENT_CLASS(BX2fdsqSP)

BX2fdsqSP::BX2fdsqSP(){

}

BX2fdsqSP::~BX2fdsqSP(){

}

void* BX2fdsqSP::createInstance()
{
	return new BX2fdsqSP() ;
}

void BX2fdsqSP::init() {
	SkillProcessor::init();   // must call super calss's init()

	setHide(true);
}

void BX2fdsqSP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	// 根据玩家朝向，施法距离，以及地形的碰撞层，决定瞬移的目的地
	// calc the target position
	Vec2 targetPos = attacker->getWorldPosition();
	int dir = attacker->getAnimDir();
	if(dir == ANIM_DIR_UP)   // face to up
		targetPos.y -= FIGHTSKILL_DISTANCE_UNIT*9;
	else if(dir == ANIM_DIR_LEFT)   // face to left
		targetPos.x -= FIGHTSKILL_DISTANCE_UNIT*9;
	else if(dir == ANIM_DIR_DOWN)   // face to down
		targetPos.y += FIGHTSKILL_DISTANCE_UNIT*9;
	else if(dir == ANIM_DIR_RIGHT)   // face to right
		targetPos.x += FIGHTSKILL_DISTANCE_UNIT*9;

	Vec2* target = attacker->getGameScene()->findNearestPositionToTarget(
		attacker->getWorldPosition().x, attacker->getWorldPosition().y,
		targetPos.x, targetPos.y);
	if(target != NULL)
	{
		targetPos.x = target->x;
		targetPos.y = target->y;

		CC_SAFE_DELETE(target);
	}

	this->getSkillSeed()->x = targetPos.x;
	this->getSkillSeed()->y = targetPos.y;

	this->setSendAttackReq(true);

	// after calc new target position, then send it
	sendAttackReq(attacker);
}

void BX2fdsqSP::onSkillApplied(BaseFighter* attacker)
{
	// add shadow
	auto pBody = attacker->getAnim()->getAnim(ANI_COMPONENT_BODY, attacker->getAnimName());
	std::string animFileName = pBody->getAnimationData()->getAnmFileName();

	auto scene = attacker->getGameScene();
	SimpleActor* pActor = new SimpleActor();
	pActor->setGameScene(scene);
	pActor->setWorldPosition(attacker->getWorldPosition());
	pActor->loadAnim(animFileName.c_str(), true, attacker->getAnimDir());
	pActor->getLegendAnim()->setPlaySpeed(0.001f);   // do not play the animation
	pActor->setColor(Color3B(48,48,48));   // dark color
	FiniteTimeAction* simpleActorAction = Sequence::create(
		FadeOut::create(3.5f),   // fade out
		RemoveSelf::create(),
		NULL);
	pActor->runAction(simpleActorAction);
	scene->getActorLayer()->addChild(pActor, SCENE_ROLE_LAYER_BASE_ZORDER);
	pActor->release();

	// update new position
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	CCAssert(result->defenders_size() == 1, "if not 1, maybe error");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);
		attacker->setWorldPosition(Vec2(def.targetx(), def.targety()));
	}

	this->setRelease(true);   // release immediatelly
}

void BX2fdsqSP::onSkillBegan( BaseFighter* attacker )
{
	std::string str_anim = "animation/texiao/renwutexiao/BX2/bx2_1.png";
	addMagicHaloToGround(attacker->getGameScene(), attacker->getWorldPosition(), str_anim, 2.0f, 1.0f, 1.4f, 0.7f);
}

//////////////////////////////////////
IMPLEMENT_CLASS(BX3sszlSP)

	BX3sszlSP::BX3sszlSP(){

}

BX3sszlSP::~BX3sszlSP(){

}

void* BX3sszlSP::createInstance()
{
	return new BX3sszlSP() ;
}

void BX3sszlSP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	if(attacker->isMyPlayer())
	{
		auto scene = attacker->getGameScene();
        Vec2 sourcePoint = attacker->getWorldPosition();
		scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	}
}

//////////////////////////////////////
IMPLEMENT_CLASS(BY1tqmsSP)

BY1tqmsSP::BY1tqmsSP(){
	m_fElapsed = 0.0f;
}

BY1tqmsSP::~BY1tqmsSP(){

}

void BY1tqmsSP::update(float dt)
{
	if(isRelease()){
		return;
	}

	if(this->getSkillResult() != NULL)
	{
		auto result = getSkillResult();

		// attacker
		auto attacker = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(result->source()));
		if(attacker == NULL || attacker->isDead())
		{
			this->setRelease(true);
			return;
		}

		if(GameUtils::millisecondNow() - m_fStartTime > 1.0f * 2000)
		{
			this->setRelease(true);
			return;
		}

		// create effect
		float m_fInterval = 0.01f;
	    m_fElapsed += dt;
        if (m_fElapsed >= m_fInterval)
        {
 			RangeRandomSpecialEffect::generate(
				"animation/texiao/renwutexiao/BY1/by1.anm",
				attacker->getGameScene(),
				m_startPoint,
				Size(350, 350));

            m_fElapsed = 0;
        }
	}
	else
	{
		this->setRelease(true);
	}
}

void BY1tqmsSP::onSkillBegan(BaseFighter* attacker)
{
	auto scene = attacker->getGameScene();
	//if(attacker->isMyPlayer())
	//{
 //       Vec2 sourcePoint = attacker->getWorldPosition();
	//	scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	//}

	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_red.plist", 1.0f);

	// add the launch effect on the foot
	const float TOTAL_DURATION = 3.0f;
	const float STAY_DURATION = 2.0f;
	Sprite* pSprite = Sprite::create("animation/texiao/renwutexiao/magic_halo.png");
	pSprite->setScale(0.2f);
	FiniteTimeAction* rotate_action = Spawn::create(
		RotateBy::create(TOTAL_DURATION, 60),
		ScaleTo::create(0.4f, 1.3f),
		//FadeOut::create(DURATION),
		NULL);
	pSprite->runAction(rotate_action);

	auto pNode = Node::create();
	pNode->setCascadeOpacityEnabled(true);
	pNode->setScaleX(1.0f);
	pNode->setScaleY(0.5f);
	Vec2 pos = scene->convertToCocos2DSpace(attacker->getWorldPosition());
	pNode->setPosition(pos);
	pNode->addChild(pSprite);
	Action* node_action = Sequence::create(
		DelayTime::create(STAY_DURATION),
		FadeOut::create(TOTAL_DURATION - STAY_DURATION),
		RemoveSelf::create(),
		NULL);
	pNode->runAction(node_action);

	attacker->addEffect(pNode, true, 0);
}

void BY1tqmsSP::onSkillApplied(BaseFighter* attacker)
{
	m_fStartTime = GameUtils::millisecondNow();

	auto result = getSkillResult();
	// defender
	int size = result->defenders_size();
	if(size <= 0)
	{
		m_startPoint = ActorUtils::getDefaultSkillPosition(attacker, result->skillid().c_str(), result->showeffect().c_str());
		return;
	}

	BaseFighter* defender = NULL;
	float totalX = 0;
	float totalY = 0;
	for (int i = 0; i < size; i++) {
		Defender def = result->defenders(i);
		// get defender
		defender = dynamic_cast<BaseFighter*>(attacker->getGameScene()->getActor(def.target()));
		if(defender == NULL)
		{
			this->setRelease(true);
			return;
		}
		else
		{
			totalX += defender->getWorldPosition().x;
			totalY += defender->getWorldPosition().y;
			if(!defender->isDead())
				SkillProcessor::quake(defender, 1.5f);
		}
	}

	m_startPoint = Vec2(totalX/size, totalY/size);

	std::string str_anim = "animation/texiao/renwutexiao/magic_halo.png";
	addMagicHaloToGround(attacker->getGameScene(), m_startPoint, str_anim, 3.0f, 1.5f, 3.0f, 1.5f);
}

void* BY1tqmsSP::createInstance()
{
	return new BY1tqmsSP() ;
}

//////////////////////////////////////
IMPLEMENT_CLASS(BY2gwjxSP)

BY2gwjxSP::BY2gwjxSP(){

}

BY2gwjxSP::~BY2gwjxSP(){

}

void* BY2gwjxSP::createInstance()
{
	return new BY2gwjxSP() ;
}

void BY2gwjxSP::onSkillApplied( BaseFighter* attacker )
{
	auto scene = attacker->getGameScene();

	auto pAnm_1 = CCLegendAnimation::create("animation/texiao/renwutexiao/CHONGJIBO/chongjibo2/chongjibo2.anm");
	pAnm_1->setPlaySpeed(1.4f);
	pAnm_1->setScale(1.6f, 0.8f);
	attacker->addEffect(pAnm_1, false, 0/*BASEFIGHTER_ROLE_HEIGHT/3*/);

	// ground effect
	SkillProcessor::addBreathingHaloToGround(scene, attacker->getWorldPosition(),
		"animation/texiao/renwutexiao/CHONGJIBO/chongjibo2/chongjibo2_ground.anm");

	this->setRelease(true);
}

void BY2gwjxSP::onSkillBegan( BaseFighter* attacker )
{
	auto pAnm = CCLegendAnimation::create("animation/texiao/renwutexiao/CHONGJIBO/chongjibo/chongjibo_xuli2.anm");
	pAnm->setScale(1.2f);

	attacker->addEffect(pAnm, false, 30);
}

//////////////////////////////////////
IMPLEMENT_CLASS(BY3smzzSP)

BY3smzzSP::BY3smzzSP(){

}

BY3smzzSP::~BY3smzzSP(){

}

void* BY3smzzSP::createInstance()
{
	return new BY3smzzSP() ;
}

void BY3smzzSP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	auto scene = attacker->getGameScene();
	//
	// extra effects
	//
	Vec2 worldPosition;
	auto target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
	if(target != NULL)
	{
		worldPosition = target->getWorldPosition();
	}
	else
	{
		worldPosition = ActorUtils::getDefaultSkillPosition(attacker, getSkillSeed()->skillId.c_str(), getSkillSeed()->skillProcessorId.c_str());
	}
	Vec2 targetWorldPos = worldPosition;
	const float distance = 110;
	const char* effectPathName = "animation/texiao/renwutexiao/BY3/by3.anm";
	for(int i = 0; i < 5; i++)
	{
		auto pAnim = CCLegendAnimation::create(effectPathName);
		pAnim->stop();
		pAnim->setScale(0.8f);
		pAnim->setVisible(false);
		pAnim->runAction(Sequence::create(
			DelayTime::create(0.17f*(i+1)),
			Show::create(),
			CCPlayLegendAnimation::create(),
			NULL));

		Vec2 offset = GameUtils::getDirection(attacker->getWorldPosition(), targetWorldPos);
		offset = offset*distance*i;
		pAnim->setPosition(scene->convertToCocos2DSpace(Vec2(targetWorldPos.x + offset.x, targetWorldPos.y + offset.y)));

		scene->getActorLayer()->addChild(pAnim, SCENE_ROLE_LAYER_BASE_ZORDER);
	}

	// shake the screen
	if(attacker->isMyPlayer())
		scene->getSceneCamera()->shakeScreen(scene);
}

void BY3smzzSP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		SkillProcessor::hitback(attacker, defender, 256, 0.25f);
	}

	// no need to update, so release directly
	this->setRelease(true);
}

//////////////////////////////////////
IMPLEMENT_CLASS(BZ1ssbSP)

BZ1ssbSP::BZ1ssbSP(){

}

BZ1ssbSP::~BZ1ssbSP(){

}

void* BZ1ssbSP::createInstance()
{
	return new BZ1ssbSP() ;
}

void BZ1ssbSP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	auto scene = attacker->getGameScene();
	// extra effects
	int offsets[4][2] = {{70, 70}, {-70, 70}, {-70, -70}, {70, -70}};
	const char* effectPathName = "animation/texiao/renwutexiao/BZ1/bz1.anm";
	for(int i = 0; i < 4; i++)
	{
		SimpleActor* simpleActor = new SimpleActor();
		simpleActor->setGameScene(scene);
		simpleActor->setWorldPosition(Vec2(this->getPosX() + offsets[i][0], this->getPosY() + offsets[i][1]));
		simpleActor->loadAnim(effectPathName);
		simpleActor->setScale(0.7f);
		//simpleActor->setScale(m_effectScaleX, m_effectScaleY);
		scene->getActorLayer()->addChild(simpleActor, SCENE_ROLE_LAYER_BASE_ZORDER);
		simpleActor->release();
	}

	// ground effect
	Vec2 worldPosition;
	auto target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
	if(target != NULL)
	{
		worldPosition = target->getWorldPosition();
	}
	else
	{
		worldPosition = ActorUtils::getDefaultSkillPosition(attacker, getSkillSeed()->skillId.c_str(), getSkillSeed()->skillProcessorId.c_str());
	}
	auto pGroundAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/BZ1/bz1_ground.anm");
	pGroundAnim->setScale(0.8f);
	pGroundAnim->setPlayLoop(true);
	//pGroundAnim->setVisible(false);
	//pGroundAnim->setOpacity(26);
	pGroundAnim->setPosition(scene->convertToCocos2DSpace(worldPosition));
	pGroundAnim->runAction(Sequence::create(
		DelayTime::create(1.3f),
		FadeOut::create(0.8f),
		RemoveSelf::create(),
		NULL));
	scene->getActorLayer()->addChild(pGroundAnim, SCENE_GROUND_LAYER_BASE_ZORDER);
}

void BZ1ssbSP::onSkillApplied(BaseFighter* attacker)
{
	auto scene = attacker->getGameScene();
	if(attacker->isMyPlayer())
		scene->getSceneCamera()->shakeScreen(scene);

	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		// show effect only
		auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/BZ1/bz1_1.anm");
		pAnim->setVisible(false);
		pAnim->setPlayLoop(true);
		FiniteTimeAction*  action = Sequence::create(
			Show::create(),
			DelayTime::create(2.5f),
			FadeOut::create(0.5f),
			RemoveSelf::create(),
			NULL);
		pAnim->runAction(action);
		defender->addEffect(pAnim, false, 0);

		if(!defender->isDead())
			SkillProcessor::flyup(attacker, defender);
	}

	// no need to update, so release directly
	this->setRelease(true);
}

void BZ1ssbSP::onSkillBegan(BaseFighter* attacker)
{
	if(attacker->isMyPlayer())
	{
		auto scene = attacker->getGameScene();
        Vec2 sourcePoint = attacker->getWorldPosition();
		scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	}

	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_skill.plist", 1.0f);

	// add the launch effect on the attacker
	auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/BZ1/bz1_launch.anm");
	pAnim->setPlaySpeed(1.8f);
	attacker->addEffect(pAnim, false, 0);
}

//////////////////////////////////////
IMPLEMENT_CLASS(BZ2wqcySP)

BZ2wqcySP::BZ2wqcySP(){

}

BZ2wqcySP::~BZ2wqcySP(){

}

void BZ2wqcySP::onSkillApplied(BaseFighter* attacker)
{
	// add the special effect
	auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/BZ2/bz2.anm");
	pAnim->setPlayLoop(true);

	FiniteTimeAction*  action = Sequence::create(
		DelayTime::create(4.5f),
		RemoveSelf::create(),
		NULL);
	pAnim->runAction(action);

	attacker->addEffect(pAnim, true, 0);

	// no need to update, so release directly
	this->setRelease(true);
}

void* BZ2wqcySP::createInstance()
{
	return new BZ2wqcySP() ;
}

//////////////////////////////////////
IMPLEMENT_CLASS(BZ3ygzqSP)

BZ3ygzqSP::BZ3ygzqSP()
{
	// extra distance
	m_bFollowBehaviour = false;
	m_nExtraDistance = 64 * 5;

	// streak
	m_bExtraStreak = true;
	m_fStreakFade = 4.0f;
	m_fStreakMinSeg = 1.0f;
	m_fStreakStrokeWidth = 100.0f;
	m_StreakColor = Color3B(176,79,176);   // (255, 255, 255)

	m_bEnableRandom = true;
	m_fRandomRange = 32.f;

	m_flyingSpeed = 64 * 10;

	this->setEffectScaleX(1.2f);
	this->setEffectScaleY(1.2f);
}

BZ3ygzqSP::~BZ3ygzqSP(){

}

void BZ3ygzqSP::onSkillBegan(BaseFighter* attacker)
{
	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_skill.plist", 1.0f);

	//std::string str_anim = "animation/texiao/renwutexiao/BZ3/bz3_1.png";
	//addMagicHaloToGround(attacker->getGameScene(), attacker->getWorldPosition(), str_anim, 2.0f, 1.0f, 1.2f, 0.6f);

	std::string str_spritePath = "animation/texiao/renwutexiao/BZ3/bz3_1.png";
	const float TOTAL_DURATION = 2.0f;
	Sprite* pSprite = Sprite::create(str_spritePath.c_str());
	pSprite->setScale(2.0f);
	FiniteTimeAction* rotate_action = Spawn::create(
		RotateBy::create(TOTAL_DURATION, 650),
		ScaleTo::create(0.4f, 0.5f),
		NULL);
	pSprite->runAction(rotate_action);

	auto pNode = Node::create();
	pNode->setCascadeOpacityEnabled(true);
	pNode->setScaleX(1.2f);
	pNode->setScaleY(0.6f);
	Vec2 pos = attacker->getGameScene()->convertToCocos2DSpace(attacker->getWorldPosition());
	pNode->setPosition(pos);
	pNode->addChild(pSprite);
	Action* node_action = Sequence::create(
		DelayTime::create(TOTAL_DURATION),
		RemoveSelf::create(),
		NULL);
	pNode->runAction(node_action);

	attacker->getGameScene()->getActorLayer()->addChild(pNode, SCENE_ROLE_LAYER_BASE_ZORDER);

	// add the launch effect on the attacker
	if(attacker->isMyPlayer())
	{
		// the big actor shoot the big arrow
		auto action = Sequence::create(
			ScaleTo::create(0.2f, 1.3f),
			DelayTime::create(0.7f),
			ScaleTo::create(0.4f, 1.0f),
			NULL);
		attacker->getAnim()->runAction(action);
	}
}

void BZ3ygzqSP::onSkillReleased(BaseFighter* attacker)
{
	BaseFighterCommandAttack* cmd = dynamic_cast<BaseFighterCommandAttack*>(attacker->getCommand());
	auto target = dynamic_cast<BaseFighter*>(attacker->getGameScene()->getActor(this->getSkillSeed()->defender));
	if(cmd != NULL)
	{
		if(cmd->attackCount == 0)
		{
			this->setEffectScaleX(1.05f);
			this->setEffectScaleY(1.05f);

			if(target != NULL)
				SkillProcessor::hitback(attacker, target, 22, 0.19f);   // 64, 0.19
		}
		else if(cmd->attackCount == 1)
		{
			this->setEffectScaleX(1.0f);
			this->setEffectScaleY(1.0f);

			if(target != NULL)
				SkillProcessor::hitback(attacker, target, 22, 0.19f);
		}
		else if(cmd->attackCount == 2)
		{
			this->setEffectScaleX(1.0f);
			this->setEffectScaleY(1.0f);

			if(target != NULL)
				SkillProcessor::hitback(attacker, target, 22, 0.19f);
		}
	}

	FlyingSkillProcessor::onSkillReleased(attacker);
}

void BZ3ygzqSP::onSkillApplied(BaseFighter* attacker)
{
	//auto scene = attacker->getGameScene();

	//auto result = getSkillResult();
	//CCAssert(result != NULL, "skill result should not be nil");
	//for (int i = 0; i < result->defenders_size(); i++) {
	//	Defender def = result->defenders(i);

	//	auto defender = dynamic_cast<BaseFighter*>(scene->getActor(def.target()));
	//	if(defender == NULL || defender->isMyPlayer())
	//		continue;

	//	//SkillProcessor::flyup(attacker, defender);
	//	SkillProcessor::hitback(attacker, defender, 96, 0.15f);
	//}

	// no need to update, so release directly
	this->setRelease(true);
}

void* BZ3ygzqSP::createInstance()
{
	return new BZ3ygzqSP() ;
}

//////////////////////////
IMPLEMENT_CLASS(CX1ycwwSP)

CX1ycwwSP::CX1ycwwSP(){

}

CX1ycwwSP::~CX1ycwwSP(){

}

void CX1ycwwSP::onSkillBegan(BaseFighter* attacker)
{
	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_2.plist", 2.0f);   // WeaponEffectFire_skill.plist

	//BaseFighterCommandAttack* cmd = dynamic_cast<BaseFighterCommandAttack*>(attacker->getCommand());
	//if(cmd != NULL && cmd->attackCount == 0)
	{
		// add the launch effect on the attacker
		std::string str_anim = "animation/texiao/renwutexiao/CX1_1/cx1_1.anm";
		auto pAnim = CCLegendAnimation::create(str_anim);
		pAnim->setScale(1.25f);
		pAnim->setPlaySpeed(1.2f);
		//pAnim->setPlayLoop(true);
		//Action* action = Sequence::create(
		//	DelayTime::create(1.5f),
		//	FadeOut::create(1.0f),
		//	RemoveSelf::create(),
		//	NULL);
		//pAnim->runAction(action);
		attacker->addEffect(pAnim, true, 0);
	} 

	BaseFighterCommandAttack* cmd = dynamic_cast<BaseFighterCommandAttack*>(attacker->getCommand());
	if(cmd != NULL && cmd->attackCount == 2)
	{
		// add the launch effect on the attacker
		std::string str_anim = "animation/texiao/renwutexiao/CX1_1/cx1_ground.anm";
		auto pAnim = CCLegendAnimation::create(str_anim);
		pAnim->setScale(1.25f);
		pAnim->setPlaySpeed(1.2f);
		pAnim->setPlayLoop(true);
		Action* action = Sequence::create(
			DelayTime::create(0.5f),
			FadeOut::create(1.0f),
			RemoveSelf::create(),
			NULL);
		pAnim->runAction(action);
		attacker->addEffect(pAnim, true, 0);
	} 
}

void CX1ycwwSP::onSkillReleased(BaseFighter* attacker)
{
	BaseFighterCommandAttack* cmd = dynamic_cast<BaseFighterCommandAttack*>(attacker->getCommand());
	auto target = dynamic_cast<BaseFighter*>(attacker->getGameScene()->getActor(this->getSkillSeed()->defender));
	if(cmd != NULL)
	{
		if(cmd->attackCount == 0)
		{
			this->setEffectScaleX(1.7f);
			this->setEffectScaleY(1.7f);

			auto scene = attacker->getGameScene();
			if(attacker->isMyPlayer())
				scene->getSceneCamera()->shakeScreen(scene);

			if(target != NULL)
				SkillProcessor::hitback(attacker, target, 22, 0.19f);   // 64, 0.19
		}
		else if(cmd->attackCount == 1)
		{
			this->setEffectScaleX(2.0f);
			this->setEffectScaleY(2.0f);

			if(target != NULL)
				SkillProcessor::hitback(attacker, target, 22, 0.19f);
		}
		else if(cmd->attackCount == 2)
		{
			this->setEffectScaleX(2.2f);
			this->setEffectScaleY(2.2f);

			if(target != NULL)
				SkillProcessor::hitback(attacker, target, 22, 0.19f);
		}
	}

	MomentSkillProcessor::onSkillReleased(attacker);

	//auto scene = attacker->getGameScene();
	//if(attacker->isMyPlayer())
	//	scene->getSceneCamera()->shakeScreen(scene);
}

void CX1ycwwSP::onSkillApplied(BaseFighter* attacker)
{
	//auto result = getSkillResult();
	//CCAssert(result != NULL, "skill result should not be nil");
	//for (int i = 0; i < result->defenders_size(); i++) {
	//	Defender def = result->defenders(i);

	//	auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
	//	if(defender == NULL)
	//		continue;

	//	SkillProcessor::hitback(attacker, defender, 64, 0.19f);
	//}

	// no need to update, so release directly
	this->setRelease(true);
}

void* CX1ycwwSP::createInstance()
{
	return new CX1ycwwSP() ;
}
////////////////////////
IMPLEMENT_CLASS(CY1tdwjSP)

CY1tdwjSP::CY1tdwjSP(){

}

CY1tdwjSP::~CY1tdwjSP(){

}

void* CY1tdwjSP::createInstance()
{
	return new CY1tdwjSP() ;
}

void CY1tdwjSP::onSkillBegan(BaseFighter* attacker)
{
	std::string str_anim = "animation/texiao/renwutexiao/CY1/cy1_3.anm";
	auto pAnim = CCLegendAnimation::create(str_anim);
	pAnim->setScale(1.4f);
	//pAnim->setPlaySpeed(0.8f);
	attacker->addEffect(pAnim, true, 0);

	// zoom screen
	if(attacker->isMyPlayer())
	{
		auto scene = attacker->getGameScene();
        Vec2 sourcePoint = attacker->getWorldPosition();
		scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	}

	if(attacker->isMyPlayer())
	{
		// the player jumps
		const float jumpDuration = 0.55f;
		const float jumpHeight = BASEFIGHTER_ROLE_HEIGHT * 1.0f;

		// jump action
		//FiniteTimeAction* action = Sequence::create(
		//	JumpBy::create(jumpDuration, Vec2::ZERO, jumpHeight, 1),
		//	NULL);
		//attacker->getAnim()->runAction(action);

		int animNameIdx = attacker->getAnim()->getAnimName();
		// body
		auto pBody = attacker->getAnim()->getAnim(ANI_COMPONENT_BODY, animNameIdx);
		// jump action
		FiniteTimeAction* action = Sequence::create(
			JumpBy::create(jumpDuration, Vec2::ZERO, jumpHeight, 1),
			NULL);
		pBody->runAction(action);
		// shadow
		auto pBodyShadow = attacker->getAnim()->getAnim(ANI_COMPONENT_SHADOW, animNameIdx);
		// jump action
		action = Sequence::create(
			JumpBy::create(jumpDuration, Vec2::ZERO, jumpHeight, 1),
			NULL);
		pBodyShadow->runAction(action);
		if(attacker->isReflection())
		{
			auto pReflectionANim = attacker->getAnim()->getAnim(ANI_COMPONENT_REFLECTION, animNameIdx);
			// jump action
			action = Sequence::create(
				JumpBy::create(jumpDuration, Vec2::ZERO, -jumpHeight/2, 1),   // 当角色蹦起来时，模拟其水中倒影
				NULL);
			pReflectionANim->runAction(action);
		}
	}
}

void CY1tdwjSP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		bool bCanHitBack = true;
		// check if own group
		if(defender->getType() == GameActor::type_pet)
		{
			General* pGeneral = dynamic_cast<General*>(defender);
			if(pGeneral != NULL && pGeneral->getOwnerId() == attacker->getRoleId())
			{
				bCanHitBack = false;
			}
		}
		if(!GameView::getInstance()->myplayer->canAttackActor(def.target()))
			bCanHitBack = false;

		if(bCanHitBack)
		{
			if(!defender->isDead())
				SkillProcessor::flyup(attacker, defender);
			//SkillProcessor::hitback(attacker, defender, 128, 0.33f);
		}

		// show ground effect
		auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/CY1/cy1_1.anm");
		pAnim->setScale(0.6f);
		pAnim->setPlayLoop(true);
		pAnim->setPlaySpeed(0.8f);
		float nSpeed = pAnim->getPlaySpeed();
		FiniteTimeAction*  action = Sequence::create(
			DelayTime::create(2.0f),
			FadeOut::create(1.0f),
			RemoveSelf::create(),
			NULL);
		pAnim->runAction(action);
		defender->addEffect(pAnim, true, 0);

		// add the launch effect on the attacker
		auto pAnm = CCLegendAnimation::create("animation/texiao/renwutexiao/DX1/dx1_2.anm");
		//pAnm->setScale(0.9f);
		defender->addEffect(pAnm, false, 40);
	}

	// no need to update, so release directly
	this->setRelease(true);
}

void CY1tdwjSP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	auto scene = attacker->getGameScene();
	if(attacker->isMyPlayer())
		scene->getSceneCamera()->shakeScreen(scene);

	SkillProcessor::addWaveHaloToGround(scene, Vec2(this->getPosX(), this->getPosY()), 
		"animation/texiao/renwutexiao/CY1/cy1_1.anm", 1.0f, 3, 3.5f);

	//show ground effect
	auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/CY1/cy1_1.anm");
	pAnim->setScale(1.8f);
	pAnim->setPlayLoop(true);
	pAnim->setPlaySpeed(1.0f);

	Action* spawn_action = Spawn::create(
		FadeOut::create(4.0f),
		ScaleTo::create(3.0f, 0.4f),
		NULL);

	FiniteTimeAction*  action = Sequence::create(
		DelayTime::create(0.5f),
		spawn_action,
		RemoveSelf::create(),
		NULL);
	pAnim->runAction(action);

	attacker->addEffect(pAnim, true, 0);
}

/////////////////////////////////////////
IMPLEMENT_CLASS(CX3kcjSP)

CX3kcjSP::CX3kcjSP(){

}

CX3kcjSP::~CX3kcjSP(){

}

void* CX3kcjSP::createInstance()
{
	return new CX3kcjSP() ;
}

void CX3kcjSP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	if(attacker->isMyPlayer())
	{
		auto scene = attacker->getGameScene();
        Vec2 sourcePoint = attacker->getWorldPosition();
		scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	}
}

//////////////////////////////////////////////
IMPLEMENT_CLASS(CX2shySP)

CX2shySP::CX2shySP(){

}

CX2shySP::~CX2shySP(){

}

void* CX2shySP::createInstance()
{
	return new CX2shySP() ;
}

void CX2shySP::update(float dt)
{
}

void CX2shySP::onSkillReleased(BaseFighter* attacker)
{
	this->setRelease(true);
}

void CX2shySP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");

	// no defender, ignore
	if(result->defenders_size() <= 0)
	{
		this->setRelease(true);   // release the skill processor immediatelly
		return;
	}

	CCAssert(result->defenders_size() == 1, "if not 1, maybe error");
	auto scene = attacker->getGameScene();
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);
		// get defender
		auto defender = dynamic_cast<BaseFighter*>(scene->getActor(def.target()));
		if(defender == NULL)
		{
			this->setRelease(true);   // release the skill processor immediatelly
			return;
		}

		//defender->setWorldPosition(Vec2(def.targetx(), def.targety()));
		//defender->setPositionImmediately(scene->convertToCocos2DSpace(defender->getWorldPosition()));
		//return;
		Vec2 targetWorldPosition = Vec2(def.targetx(), def.targety());
		//Vec2 target = attacker->getGameScene()->convertToCocos2DSpace(targetWorldPosition);
		//FiniteTimeAction*  action = Sequence::create(
		//	MoveTo::create(DZ1TMSSP_KNOCKBACK_TIME, target),
		//	NULL);
		//defender->runAction(action);

		//Vec2 offset = GameUtils::getDirection(targetWorldPosition, attacker->getWorldPosition());
		//int animDirection = defender->getAnimDirection(Vec2(0, 0), Vec2(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);
		////CCAssert(animDirection != -1, "invalid anim dir");
		//defender->setAnimDir(animDirection);

		//defender->changeAction(ACT_STAND);

		////defender->changeAction(ACT_BE_KNOCKBACK);

		ActionJumpContext context;
		context.startTime = GameUtils::millisecondNow();
		context.targetPosition = scene->convertToCocos2DSpace(targetWorldPosition);
		context.jumpDirection = 1;
		context.jumpHeight = 0;
		context.shadowColor = Color3B(150, 150, 255);   // (150, 150, 255)
		context.shadowAlphaStep = 15;
		const float jumpSpeed = 700.f;   // 1000.f
		context.jumpDuration = attacker->getWorldPosition().getDistance(targetWorldPosition) / jumpSpeed;
		context.setContext(defender);

		defender->changeAction(ACT_JUMP);

		// blue flash
		auto action1 = CCTintTo::create(0.3f,32,32,255);
		auto action2 = CCTintTo::create(0.1f,192,192,255);
		auto repeapAction = Repeat::create(Sequence::create(action1,action2,NULL), 6);
		auto action = Sequence::create(
			repeapAction,
			CCTintTo::create(0.1f, 255, 255, 255),
			NULL);
		defender->setCascadeColorEnabled(true);
		defender->runAction(action);
	}

	this->setRelease(true);   // release the skill processor immediatelly
}

void CX2shySP::onSkillBegan( BaseFighter* attacker )
{
	std::string str_anim = "animation/texiao/renwutexiao/cx2_1/cx2_1.png";
	addMagicHaloToGround(attacker->getGameScene(), attacker->getWorldPosition(), str_anim, 1.5f, 0.5f, 1.4f, 0.7f);

	auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/CY2/cy2.anm");
	pAnim->setScale(0.8f);
	//pAnim->setPlaySpeed(1.8f);
	attacker->addEffect(pAnim, false, 0);
}

////////////////////////////////////////////
IMPLEMENT_CLASS(CY1SP)

CY1SP::CY1SP(){

}

CY1SP::~CY1SP()
{
}

void* CY1SP::createInstance()
{
	return new CY1SP() ;
}
///////////////////////////////////////////
IMPLEMENT_CLASS(CZ3fsjSP)

CZ3fsjSP::CZ3fsjSP(){

}

CZ3fsjSP::~CZ3fsjSP(){

}

void* CZ3fsjSP::createInstance()
{
	return new CZ3fsjSP() ;
}

void CZ3fsjSP::onSkillBegan(BaseFighter* attacker)
{
	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_red.plist", 3.5f);

	/*std::string str_anim = "animation/texiao/renwutexiao/CZ3/cz3_1.png";
	addMagicHaloToGround(attacker->getGameScene(), attacker->getWorldPosition(), str_anim, 2.0f, 1.0f, 2.0f, 1.0f);*/

	//// add the launch effect on the attacker
	//auto pAnim = CCLegendAnimation::create(SKILL_LAUNCH_SPECIAL_EFFECT_1);
	//pAnim->setColor(Color3B(255, 160, 160));   // red
	//pAnim->setScale(0.75f);
	//attacker->addEffect(pAnim, true, 0);

	// add the launch effect on the attacker
	std::string str_spritePath = "animation/texiao/renwutexiao/CZ3/cz3_1.png";
	const float TOTAL_DURATION = 2.0f;
	Sprite* pSprite = Sprite::create(str_spritePath.c_str());
	pSprite->setScale(2.2f);
	FiniteTimeAction* rotate_action = Spawn::create(
		RotateBy::create(TOTAL_DURATION, 650),
		ScaleTo::create(0.4f, 0.5f),
		NULL);
	pSprite->runAction(rotate_action);

	auto pNode = Node::create();
	pNode->setCascadeOpacityEnabled(true);
	pNode->setScaleX(1.2f);
	pNode->setScaleY(0.6f);
	Vec2 pos = attacker->getGameScene()->convertToCocos2DSpace(attacker->getWorldPosition());
	pNode->setPosition(pos);
	pNode->addChild(pSprite);
	Action* node_action = Sequence::create(
		DelayTime::create(TOTAL_DURATION),
		RemoveSelf::create(),
		NULL);
	pNode->runAction(node_action);

	attacker->getGameScene()->getActorLayer()->addChild(pNode, SCENE_ROLE_LAYER_BASE_ZORDER);
}

void CZ3fsjSP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	auto scene = attacker->getGameScene();
	//if(attacker->isMyPlayer())
	//{
 //       Vec2 sourcePoint = attacker->getWorldPosition();
	//	scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	//}

	//// show ground effect
	//SimpleActor* simpleActor = new SimpleActor();
	//simpleActor->setLayerId(0);   // ground layer
	//simpleActor->setGameScene(scene);
	//simpleActor->setWorldPosition(Vec2(this->getPosX(), this->getPosY()));
	//simpleActor->loadAnim("animation/texiao/renwutexiao/CZ3/cz3_1.anm", true);
	//simpleActor->setVisible(false);

	//ScaleTo*  action1 = ScaleTo::create(0.7f,0.7f,0.7f);
	//ScaleTo*  action2 = ScaleTo::create(0.7f,0.9f,0.9f);
	//RepeatForever* repeapAction = RepeatForever::create(Sequence::create(action1,action2,NULL));
	//simpleActor->runAction(repeapAction);

	//FiniteTimeAction*  action = Sequence::create(
	//	DelayTime::create(0.2f),   // the ground effect start time in the cz3_1.anm
	//	Show::create(),
	//	DelayTime::create(1.0f),
	//	FadeOut::create(3.0f),
	//	RemoveSelf::create(),
	//	NULL);
	//simpleActor->runAction(action);

	//auto actorLayer = scene->getActorLayer();
	//actorLayer->addChild(simpleActor);
	//simpleActor->release();

	SkillProcessor::addWaveHaloToGround(scene, Vec2(this->getPosX(), this->getPosY()), 
		"animation/texiao/renwutexiao/CZ3/cz3_1.anm", 1.0f, 4);

	// 将敌人高高的挑起来
	auto target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
	if(target != NULL && !target->isDead())
		SkillProcessor::flyup(attacker, target);
}

/////////////////////////////////////
IMPLEMENT_CLASS(CZ2wxkjSP)

CZ2wxkjSP::CZ2wxkjSP(){

}

CZ2wxkjSP::~CZ2wxkjSP(){

}

void* CZ2wxkjSP::createInstance()
{
	return new CZ2wxkjSP() ;
}

void CZ2wxkjSP::onSkillBegan(BaseFighter* attacker)
{
	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_skill.plist", 2.5f);

	// add the launch effect on the attacker
	//auto pAnim = CCLegendAnimation::create(SKILL_LAUNCH_SPECIAL_EFFECT_1);
	//pAnim->setScale(0.75f);
	//attacker->addEffect(pAnim, true, 0);

	auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/BZ1/bz1_launch_high.anm");
	//pAnim->setScale(0.85f);
	pAnim->setScale(0.7f, 0.85f);
	pAnim->setPlaySpeed(1.5f);
	attacker->addEffect(pAnim, false, 0);
}

void CZ2wxkjSP::onSkillReleased(BaseFighter* attacker)
{
	this->setEffectScaleX(1.2f);
	this->setEffectScaleY(1.15f);
	MomentSkillProcessor::onSkillReleased(attacker);

	//auto scene = attacker->getGameScene();
	//scene->getSceneCamera()->shakeScreen(scene);
}

void CZ2wxkjSP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		if(!defender->isDead())
			SkillProcessor::quake(defender, 1.3f);
	}

	// no need to update, so release directly
	this->setRelease(true);
}

///////////////////////////////////
IMPLEMENT_CLASS(CZ1mshcSP)

CZ1mshcSP::CZ1mshcSP(){

}

CZ1mshcSP::~CZ1mshcSP(){

}

void* CZ1mshcSP::createInstance()
{
	return new CZ1mshcSP() ;
}

void CZ1mshcSP::onSkillBegan( BaseFighter* attacker )
{
	std::string str_anim = "animation/texiao/renwutexiao/cz1_1/cz1_1.png";
	addMagicHaloToGround(attacker->getGameScene(), attacker->getWorldPosition(), str_anim, 2.0f, 1.0f, 1.4f, 0.7f);

	// add the launch effect on the foot
	const float TOTAL_DURATION = 3.0f;
	const float STAY_DURATION = 2.0f;
	auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/cz1_1/cz1_1.anm");
	pAnim->setPlayLoop(true);
	FiniteTimeAction* rotate_action = Spawn::create(
		RotateBy::create(TOTAL_DURATION, 60),
		ScaleTo::create(0.4f, 1.4f),
		FadeOut::create(1.0f),
		NULL);
	pAnim->runAction(rotate_action);

	auto pNode = Node::create();
	pNode->setCascadeOpacityEnabled(true);
	pNode->setScaleX(1.0f);
	pNode->setScaleY(0.5f);
	Vec2 pos = attacker->getGameScene()->convertToCocos2DSpace(attacker->getWorldPosition());
	pNode->setPosition(pos);
	pNode->addChild(pAnim);
	Action* node_action = Sequence::create(
		DelayTime::create(STAY_DURATION),
		//FadeOut::create(TOTAL_DURATION - STAY_DURATION),
		RemoveSelf::create(),
		NULL);
	pNode->runAction(node_action);

	attacker->getGameScene()->getActorLayer()->addChild(pNode, SCENE_ROLE_LAYER_BASE_ZORDER);
}

///////////////////////////////////////
IMPLEMENT_CLASS(CY3fyjtSP)

CY3fyjtSP::CY3fyjtSP(){

}

CY3fyjtSP::~CY3fyjtSP(){

}

void* CY3fyjtSP::createInstance()
{
	return new CY3fyjtSP() ;
}

void CY3fyjtSP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	if(attacker->isMyPlayer())
	{
		auto scene = attacker->getGameScene();
        Vec2 sourcePoint = attacker->getWorldPosition();
		scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	}
}

/////////////////////////////////
IMPLEMENT_CLASS(CY2dbzSP)

CY2dbzSP::CY2dbzSP(){

}

CY2dbzSP::~CY2dbzSP(){

}

void* CY2dbzSP::createInstance()
{
	return new CY2dbzSP() ;
}

void CY2dbzSP::onSkillBegan( BaseFighter* attacker )
{
	/*std::string str_anim = "animation/texiao/renwutexiao/DY2/dy2_1.png";
	addMagicHaloToGround(attacker->getGameScene(), attacker->getWorldPosition(), str_anim, 2.0f, 1.0f, 2.0f, 1.0f);*/

	auto pAnm = CCLegendAnimation::create("animation/texiao/renwutexiao/CHONGJIBO/chongjibo/chongjibo_xuli3.anm");
	pAnm->setScale(1.2f);
	attacker->addEffect(pAnm, false, 15);
}

void CY2dbzSP::onSkillApplied( BaseFighter* attacker )
{
	auto scene = attacker->getGameScene();

	auto pAnm_1 = CCLegendAnimation::create("animation/texiao/renwutexiao/CHONGJIBO/chongjibo3/chongjibo3.anm");
	pAnm_1->setPlaySpeed(1.4f);
	pAnm_1->setScale(1.6f, 0.8f);
	attacker->addEffect(pAnm_1, false, 0/*BASEFIGHTER_ROLE_HEIGHT/3*/);

	// ground effect
	SkillProcessor::addBreathingHaloToGround(scene, attacker->getWorldPosition(),
		"animation/texiao/renwutexiao/CHONGJIBO/chongjibo3/chongjibo3_ground.anm");

	this->setRelease(true);
}

///////////////////////////////////
IMPLEMENT_CLASS(DZ3hfsSP)

DZ3hfsSP::DZ3hfsSP()
{
	// extra flying
	m_bFollowBehaviour = false;
	m_nExtraDistance = 64 * 10;

	m_flyingSpeed = 64 * 16;
}

DZ3hfsSP::~DZ3hfsSP(){

}

void DZ3hfsSP::onSkillBegan(BaseFighter* attacker)
{
	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_red.plist", 0.95f);

	// add the launch effect on the attacker
	auto pAnim = CCLegendAnimation::create(SKILL_LAUNCH_SPECIAL_EFFECT_1);
	pAnim->setPlaySpeed(1.2f);
	pAnim->setScale(0.75f);
	attacker->addEffect(pAnim, true, 0);
}

void DZ3hfsSP::onSkillApplied(BaseFighter* attacker)
{
	auto scene = attacker->getGameScene();

	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(scene->getActor(def.target()));
		if(defender == NULL || defender->isMyPlayer())
			continue;

		if(!defender->isDead())
			SkillProcessor::flyup(attacker, defender);
	}

	// no need to update, so release directly
	this->setRelease(true);
}

void* DZ3hfsSP::createInstance()
{
	return new DZ3hfsSP() ;
}
////////////////////////////////
IMPLEMENT_CLASS(DZ2zljSP)

DZ2zljSP::DZ2zljSP()
{
	m_bFollowBehaviour = false;
	this->m_flyingSpeed = 64*14;
	this->m_nExtraDistance = 64*5;

	m_bEnableRandom = true;
	m_fRandomRange = 32.f;

	this->setEffectScaleX(1.2f);
	this->setEffectScaleY(1.2f);
}

DZ2zljSP::~DZ2zljSP()
{

}

void DZ2zljSP::onSkillApplied(BaseFighter* attacker)
{
	//// shake the screen
	//auto scene = attacker->getGameScene();
	//if(attacker->isMyPlayer())
	//	scene->getSceneCamera()->shakeScreen(scene);

	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		SkillProcessor::shake(defender, true, 10, 13.f);
	}

	// no need to update, so release directly
	this->setRelease(true);
}

void* DZ2zljSP::createInstance()
{
	return new DZ2zljSP() ;
}

void DZ2zljSP::onSkillBegan( BaseFighter* attacker )
{
	//SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_skill.plist", 2.5f);

	/*************************************************/
	// the animation on the attacker's head
	auto target = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(this->getSkillSeed()->defender));
	if (NULL != target)
	{
		std::string str_anim = "animation/texiao/renwutexiao/DZ2_02/DZ2_01.anm";
		auto pAnim = CCLegendAnimation::create(str_anim);

		Vec2 offset = GameUtils::getDirection(attacker->getWorldPosition(), target->getWorldPosition());
		// dir up: 0, left: 1, down: 2, right: 3
		int animDirection = attacker->getAnimDirection(Vec2(0, 0), Vec2(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);

		if (1 == animDirection)
		{
			pAnim->setScaleX((-0.7f));
		}
		pAnim->setScaleY(0.7f);

		attacker->addEffect(pAnim, false, 15);
	}
	else
	{
		std::string str_anim = "animation/texiao/renwutexiao/DZ2_02/DZ2_01.anm";
		auto pAnim = CCLegendAnimation::create(str_anim);

		// dir up: 0, left: 1, down: 2, right: 3
		int nDirection = attacker->getAnimDir();
		if (1 == nDirection)
		{
			pAnim->setScaleX((-0.7f));
		}
		pAnim->setScaleY(0.7f);

		attacker->addEffect(pAnim, false, 15);
	}
	/********************************************************************/

	Sprite* pSprite = Sprite::create("animation/texiao/renwutexiao/DZ2_02/dz2_01.png");

	FiniteTimeAction* scale_action = Sequence::create(
		ScaleTo::create(0.08f, 1.6f),
		ScaleTo::create(0.16f, 0.4f),
		NULL);

	pSprite->runAction(scale_action);

	auto pNode = Node::create();
	pNode->setCascadeOpacityEnabled(true);
	pNode->setScaleX(1.0f);
	pNode->setScaleY(0.5f);
	Vec2 pos = attacker->getGameScene()->convertToCocos2DSpace(attacker->getWorldPosition());
	pNode->setPosition(pos);
	pNode->addChild(pSprite);
	Action* node_action = Sequence::create(
			DelayTime::create(0.3f),
			RemoveSelf::create(),
			NULL);
	pNode->runAction(node_action);

	attacker->getGameScene()->getActorLayer()->addChild(pNode, SCENE_ROLE_LAYER_BASE_ZORDER);
}

//////////////////////////////

#define DZ1TMSSP_KNOCKBACK_TIME 0.15f   // second
#define KNOCKBACK_HIT_RECOVER_TIME 1.0f   // 被击退后的硬直时间

IMPLEMENT_CLASS(DZ1tmsSP)

DZ1tmsSP::DZ1tmsSP()
{
	m_bFollowBehaviour = false;
	m_nExtraDistance = 64 * 5;
	m_flyingSpeed = 64*8;

	// streak
	m_bExtraStreak = true;
	m_fStreakFade = 2.0f;
	m_fStreakMinSeg = 1.0f;
	m_fStreakStrokeWidth = 100.0f;
	m_StreakColor = Color3B(253,226,145);   // (255, 255, 255)

	m_bEnableRandom = true;
	m_fRandomRange = 32.f;

	this->setEffectScaleX(1.2f);
	this->setEffectScaleY(1.2f);
}

DZ1tmsSP::~DZ1tmsSP(){

}

void* DZ1tmsSP::createInstance()
{
	return new DZ1tmsSP() ;
}

void DZ1tmsSP::update(float dt)
{
	if(isRelease()){
		return;
	}

	if(this->getSkillSeed() != NULL)
	{
		FlyingSkillProcessor::update(dt);
	}

	if(this->getSkillResult() != NULL)
	{
		// get defender
		BaseFighter* defender = NULL;
		auto result = getSkillResult();
		for (int i = 0; i < result->defenders_size(); i++) {
			Defender def = result->defenders(i);
			defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
			break;
		}

		if(GameUtils::millisecondNow() - m_knockbackStartTime > (DZ1TMSSP_KNOCKBACK_TIME+KNOCKBACK_HIT_RECOVER_TIME) * 1000)
		{
			if(defender != NULL)
			{
				if(!defender->isDead())
					defender->changeAction(ACT_STAND);
			}

			this->setRelease(true);
		}
	}
}

void DZ1tmsSP::onSkillBegan(BaseFighter* attacker)
{
	//SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_skill.plist", 0.95f);

	//std::string str_anim = "animation/texiao/renwutexiao/DZ1/dz1_1.png";
	//addMagicHaloToGround(attacker->getGameScene(), attacker->getWorldPosition(), str_anim, 2.0f, 1.0f, 1.4f, 0.7f);

	auto pAnm = CCLegendAnimation::create("animation/texiao/renwutexiao/CHONGJIBO/chongjibo/chongjibo_xuli4.anm");
	//pAnm->setScale(1.2f);
	pAnm->setScaleX(1.4f);
	pAnm->setScaleY(1.4f);
	pAnm->setPlaySpeed(1.5f);
	attacker->addEffect(pAnm, true, 0);

	// add the launch effect on the attacker
	/*auto pAnim = CCLegendAnimation::create(SKILL_LAUNCH_SPECIAL_EFFECT_1);
	pAnim->setPlaySpeed(1.2f);
	attacker->addEffect(pAnim, true, 0);*/
	if(attacker->isMyPlayer())
	{
		// the big actor shoot the big arrow
		auto action = Sequence::create(
			ScaleTo::create(0.1f, 1.55f),
			DelayTime::create(0.7f),
			ScaleTo::create(0.4f, 1.0f),
			NULL);
		attacker->getAnim()->runAction(action);
	}
}

void DZ1tmsSP::onSkillApplied(BaseFighter* attacker)
{
	bool ret = knockback(attacker, DZ1TMSSP_KNOCKBACK_TIME, KNOCKBACK_HIT_RECOVER_TIME);
	if(ret)
		m_knockbackStartTime = GameUtils::millisecondNow();

	auto scene = attacker->getGameScene();
	if(attacker->isMyPlayer())
		scene->getSceneCamera()->shakeScreen(scene);

	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		// show effect only
		auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/DZ1/dz1_2.anm");
		pAnim->setPlaySpeed(1.15f);
		pAnim->setScale(1.0f);
		defender->addEffect(pAnim, false, 60);
	}
}

///////////////////////////
IMPLEMENT_CLASS(DY3hyhsSP)

DY3hyhsSP::DY3hyhsSP(){
	m_fElapsed = 0.f;
}

DY3hyhsSP::~DY3hyhsSP(){

}

void DY3hyhsSP::update(float dt)
{
	if(isRelease()){
		return;
	}

	if(this->getSkillResult() != NULL)
	{
		auto result = getSkillResult();

		// attacker
		auto attacker = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(result->source()));
		if(attacker == NULL || attacker->isDead())
		{
			this->setRelease(true);
			return;
		}

		if(GameUtils::millisecondNow() - m_fStartTime > 1.0f * 2000)
		{
			this->setRelease(true);
			return;
		}

		// create effect
		float m_fInterval = 0.01f;
	    m_fElapsed += dt;
        if (m_fElapsed >= m_fInterval)
        {
 			RangeRandomSpecialEffect::generate(
				"animation/texiao/renwutexiao/DY3/dy3.anm",
				attacker->getGameScene(),
				m_startPoint,
				Size(350, 350));

            m_fElapsed = 0;
        }
	}
}

void DY3hyhsSP::onSkillApplied(BaseFighter* attacker)
{
	m_fStartTime = GameUtils::millisecondNow();

	auto result = getSkillResult();

	// defender
	int size = result->defenders_size();
	if(size <= 0)
	{
		m_startPoint = ActorUtils::getDefaultSkillPosition(attacker, result->skillid().c_str(), result->showeffect().c_str());
		return;
	}

	// defender
	BaseFighter* defender = NULL;
	float totalX = 0;
	float totalY = 0;
	for (int i = 0; i < size; i++) {
		Defender def = result->defenders(i);
		// get defender
		defender = dynamic_cast<BaseFighter*>(attacker->getGameScene()->getActor(def.target()));
		if(defender == NULL)
		{
			this->setRelease(true);
			return;
		}
		else
		{
			totalX += defender->getWorldPosition().x;
			totalY += defender->getWorldPosition().y;
			if(!defender->isDead())
				SkillProcessor::quake(defender, 1.5f);
		}
	}

	m_startPoint = Vec2(totalX/size, totalY/size);

	std::string str_anim = "animation/texiao/renwutexiao/magic_halo.png";
	addMagicHaloToGround(attacker->getGameScene(), m_startPoint, str_anim, 3.0f, 1.5f, 2.2f, 1.1f);
}

void* DY3hyhsSP::createInstance()
{
	return new DY3hyhsSP() ;
}
////////////////////////////////////////
IMPLEMENT_CLASS(DY2tcdkSP)

DY2tcdkSP::DY2tcdkSP(){

}

DY2tcdkSP::~DY2tcdkSP(){

}

void* DY2tcdkSP::createInstance()
{
	return new DY2tcdkSP() ;
}

void DY2tcdkSP::onSkillBegan( BaseFighter* attacker )
{
	auto pAnm = CCLegendAnimation::create("animation/texiao/renwutexiao/CHONGJIBO/chongjibo/chongjibo_xuli1.anm");
	pAnm->setScale(1.2f);
	//pAnm->setPlaySpeed(1.2f);
	attacker->addEffect(pAnm, false, 15);

	/*
	// the player jumps
	const float jumpDuration = 1.1f;
	const int jumpTimes = 1;
	const float jumpHeight = BASEFIGHTER_ROLE_HEIGHT * 3.5f;
	int animNameIdx = attacker->getAnim()->getAnimName();
	// body
	auto pBody = attacker->getAnim()->getAnim(ANI_COMPONENT_BODY, animNameIdx);
	// jump action
	FiniteTimeAction* action = Sequence::create(
		JumpBy::create(jumpDuration, Vec2::ZERO, jumpHeight, jumpTimes),
		NULL);
	pBody->runAction(action);
	// shadow
	auto pBodyShadow = attacker->getAnim()->getAnim(ANI_COMPONENT_SHADOW, animNameIdx);
	// jump action
	action = Sequence::create(
		JumpBy::create(jumpDuration, Vec2::ZERO, jumpHeight, jumpTimes),
		NULL);
	pBodyShadow->runAction(action);
	if(attacker->isReflection())
	{
		auto pReflectionANim = attacker->getAnim()->getAnim(ANI_COMPONENT_REFLECTION, animNameIdx);
		// jump action
		action = Sequence::create(
			JumpBy::create(jumpDuration, Vec2::ZERO, -jumpHeight/2, jumpTimes),   // 当角色蹦起来时，模拟其水中倒影
			NULL);
		pReflectionANim->runAction(action);
	}
	*/
}

void DY2tcdkSP::onSkillApplied( BaseFighter* attacker )
{
	auto scene = attacker->getGameScene();

	auto pAnm_1 = CCLegendAnimation::create("animation/texiao/renwutexiao/CHONGJIBO/chongjibo1/chongjibo1.anm");
	pAnm_1->setPlaySpeed(1.4f);
	pAnm_1->setScale(1.6f, 0.8f);
	attacker->addEffect(pAnm_1, false, 0/*BASEFIGHTER_ROLE_HEIGHT/3*/);

	// ground effect
	SkillProcessor::addBreathingHaloToGround(scene, attacker->getWorldPosition(),
		"animation/texiao/renwutexiao/CHONGJIBO/chongjibo1/chongjibo1_ground.anm");

	this->setRelease(true);
}

//////////////////////////////
IMPLEMENT_CLASS(DY1srpzSP)

DY1srpzSP::DY1srpzSP(){
	//m_fElapsed = 0.f;
}

DY1srpzSP::~DY1srpzSP(){

}

//void DY1srpzSP::update(float dt)
//{
//	if(isRelease()){
//		return;
//	}
//
//	if(this->getSkillResult() != NULL)
//	{
//		auto result = getSkillResult();
//
//		// attacker
//		auto attacker = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(result->source()));
//		if(attacker == NULL || attacker->isDead())
//		{
//			this->setRelease(true);
//			return;
//		}
//
//		if(GameUtils::millisecondNow() - m_fStartTime > 1.0f * 1000)
//		{
//			this->setRelease(true);
//			return;
//		}
//
//		// create effect
//		float m_fInterval = 0.05f;
//	    m_fElapsed += dt;
//        if (m_fElapsed >= m_fInterval)
//        {
// 			RangeRandomSpecialEffect::generate(
//				"animation/texiao/renwutexiao/DY1/dy1.anm",
//				attacker->getGameScene(),
//				m_startPoint,
//				Size(256, 256));
//
//            m_fElapsed = 0;
//        }
//	}
//}
//
//void DY1srpzSP::onSkillApplied(BaseFighter* attacker)
//{
//	m_fStartTime = GameUtils::millisecondNow();
//
//	auto result = getSkillResult();
//	// defender
//	BaseFighter* defender = NULL;
//	for (int i = 0; i < result->defenders_size(); i++) {
//		Defender def = result->defenders(i);
//		// get defender
//		defender = dynamic_cast<BaseFighter*>(attacker->getGameScene()->getActor(def.target()));
//		if(defender == NULL)
//		{
//			this->setRelease(true);
//			return;
//		}
//		else
//		{
//			break;
//		}
//	}
//
//	m_startPoint = defender->getWorldPosition();
//}

void DY1srpzSP::update(float dt)
{
	if(isRelease()){
		return;
	}

	if(this->getSkillResult() != NULL)
	{
		if(GameUtils::millisecondNow() - m_fStartTime > m_fDuration * 1000)
		{
			this->setRelease(true);
			return;
		}
	}
}

void DY1srpzSP::onSkillReleased(BaseFighter* attacker)
{
	/////////////////////////////////////////////////////////
	//// multi-arrows!
	/////////////////////////////////////////////////////////
	auto scene = attacker->getGameScene();
	auto target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
	Vec2 end = ActorUtils::getDefaultSkillPosition(attacker, getSkillSeed()->skillId.c_str(), getSkillSeed()->skillProcessorId.c_str());
	end = scene->convertToCocos2DSpace(end);
	if(target != NULL)
	{
		end = target->getPosition();
	}

	const int arrowNum = 10;
	const float arrowIntervalAngle = 5.0f;
	const float arrowMoveDistance = 64*8;
	const float arrowScaleStep = 0.15f;

	m_fStartTime = GameUtils::millisecondNow();

	//Vec2 start = Vec2(attacker->getPositionX(), attacker->getPositionY()+FLYING_ITEM_OFFSET_FROM_GROUND);
	Vec2 start = attacker->getPosition();
	//Vec2 end = target->getPosition();

	float totalAngle = (arrowNum - 1) * arrowIntervalAngle;
	float initAngle = - totalAngle / 2;

	float currentAngle = initAngle;
	//end = end.rotateByAngle(start, CC_DEGREES_TO_RADIANS(initAngle));
	// the arrow on the left part
	float arrowScale = 0.8f;
	for(int i = 0; i < arrowNum; i++)
	{
		SimpleActor* arrow = new SimpleActor();
		arrow->setWorldPositionZ(30);   // FLYING_ITEM_OFFSET_FROM_GROUND
		arrow->setGameScene(attacker->getGameScene());
		//simpleActor->setWorldPosition(Vec2(targetPos.x, targetPos.y));
		//arrow->loadSprite("animation/weapon/arrow01.png");
		arrow->loadAnim("animation/texiao/renwutexiao/DY1/dy1.anm", true);
		arrow->setAnchorPoint(Vec2(0.5f, 0));
		arrow->setScale(arrowScale);
		if(i < arrowNum/2)
			arrowScale += arrowScaleStep;
		else
			arrowScale -= arrowScaleStep;

		currentAngle  += arrowIntervalAngle;
		Vec2 tmpEnd = end.rotateByAngle(start, CC_DEGREES_TO_RADIANS(currentAngle));
		//end = end.rotateByAngle(start, CC_DEGREES_TO_RADIANS(arrowIntervalAngle));
		//end = end.rotateByAngle(start, CC_DEGREES_TO_RADIANS(currentAngle));
		// the arrow will run engough distance
		Vec2 offset = GameUtils::getDirection(start, tmpEnd);

		float distanceBetweenStartAndEnd = start.getDistance(end);
		float tmpDistance = 0;
		if(arrowMoveDistance > distanceBetweenStartAndEnd)
			tmpDistance = arrowMoveDistance - distanceBetweenStartAndEnd;
		else
			tmpDistance = distanceBetweenStartAndEnd;

		offset = offset*tmpDistance;
		tmpEnd = tmpEnd+offset;

		//arrow->setPosition(start);
		arrow->setPositionImmediately(start);
		arrow->setWorldPosition(attacker->getWorldPosition());

		float degree = GameUtils::getDegree(start, tmpEnd);
		//arrow->setRotation(degree);
		arrow->getLegendAnim()->setRotation(degree);

		// calc the arrow's flying time
		const int arrowSpeed = 64*7;
		float currentDistance = start.getDistance(tmpEnd);
		m_fDuration = currentDistance / arrowSpeed;

		FiniteTimeAction*  action = Sequence::create(
			MoveTo::create(m_fDuration, tmpEnd),
			RemoveSelf::create(),
			//CallFunc::create(this, callfunc_selector(YuanChengGongJiSP::hitTarget)), 
			NULL);
		arrow->runAction(action);

		scene->getActorLayer()->addChild(arrow, SCENE_ROLE_LAYER_BASE_ZORDER);
		arrow->release();
	}

	this->setRelease(true);
}

void* DY1srpzSP::createInstance()
{
	return new DY1srpzSP() ;
}

void DY1srpzSP::onSkillBegan(BaseFighter* attacker)
{
	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_skill.plist", 1.0f);

	//if(attacker->isMyPlayer())
	//{
	//	auto scene = attacker->getGameScene();
 //       Vec2 sourcePoint = attacker->getWorldPosition();
	//	scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	//}

	// add the launch effect on the attacker
	auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/launch/launch_yellow.anm");
	pAnim->setOpacity(196);
	pAnim->setScale(0.9f);
	attacker->addEffect(pAnim, false, 0);
}

void DY1srpzSP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		SkillProcessor::hitback(attacker, defender, 256, 0.25f);
	}

	// no need to update, so release directly
	this->setRelease(true);
}

/////////////////////////////////
IMPLEMENT_CLASS(DX3bbcySP)

DX3bbcySP::DX3bbcySP()
{
	m_nExtraDistance = 64*5;
	m_flyingSpeed = 64*15;
}

DX3bbcySP::~DX3bbcySP(){

}

void* DX3bbcySP::createInstance()
{
	return new DX3bbcySP() ;
}
///////////////////////////////////////
IMPLEMENT_CLASS(DX2qlwySP)

DX2qlwySP::DX2qlwySP(){

}

DX2qlwySP::~DX2qlwySP(){

}

void* DX2qlwySP::createInstance()
{
	return new DX2qlwySP();
}

void DX2qlwySP::onSkillReleased(BaseFighter* attacker)
{
	MomentSkillProcessor::onSkillReleased(attacker);

	//if(attacker->isMyPlayer())
	//{
	//	auto scene = attacker->getGameScene();
 //       Vec2 sourcePoint = attacker->getWorldPosition();
	//	scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	//}
}

void DX2qlwySP::onSkillBegan( BaseFighter* attacker )
{
	std::string str_anim = "animation/texiao/renwutexiao/DX2/dx2_1.png";
	addMagicHaloToGround(attacker->getGameScene(), attacker->getWorldPosition(), str_anim, 2.0f, 1.0f, 1.3f, 0.65f);
}

///////////////////////////////
IMPLEMENT_CLASS(DX1lysSP)

DX1lysSP::DX1lysSP()
{
	m_bFollowBehaviour = false;

	m_bExtraStreak = true;
	m_fStreakFade = 1000.0f;
	m_fStreakMinSeg = 1.0f;
	m_fStreakStrokeWidth = 40.0f;
	m_StreakColor = Color3B(225,225,255);

	m_flyingSpeed = 64 * 20;
	m_nExtraDistance = 64 * 20;
}

DX1lysSP::~DX1lysSP()
{

}

void* DX1lysSP::createInstance()
{
	return new DX1lysSP() ;
}

void DX1lysSP::onSkillBegan(BaseFighter* attacker)
{
	if(attacker->isMyPlayer())
	{
		auto scene = attacker->getGameScene();
        Vec2 sourcePoint = attacker->getWorldPosition();
		scene->getSceneCamera()->zoomScreen(scene, sourcePoint);
	}

	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_blue.plist", 0.95f);

	// add the launch effect on the attacker
	auto pAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/launch/launch_blue.anm");
	pAnim->setScale(0.8f);
	attacker->addEffect(pAnim, false, 0);
}

void DX1lysSP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		//SkillProcessor::shake(defender, true);
		SkillProcessor::hitback(attacker, defender, 96, 0.15f);
	}

	// no need to update, so release directly
	this->setRelease(true);
}

///////////////////////////////
IMPLEMENT_CLASS(MagicDefaultAttackSP)

MagicDefaultAttackSP::MagicDefaultAttackSP()
{
	// it's magic bolt, so its speed should slower than the speed of the physical arrow
	m_flyingSpeed = 64 * 7;

	m_ExtraParticleFileName = "animation/texiao/particledesigner/flying_tail_blue.plist";
}

MagicDefaultAttackSP::~MagicDefaultAttackSP()
{
}

void* MagicDefaultAttackSP::createInstance()
{
	return new MagicDefaultAttackSP() ;
}

void MagicDefaultAttackSP::onSkillReleased(BaseFighter* attacker)
{
	FlyingSkillProcessor::onSkillReleased(attacker);

	//auto target = dynamic_cast<BaseFighter*>(attacker->getGameScene()->getActor(this->getSkillSeed()->defender));
	//bool ret = knockback(attacker, target, 16, 0.15f, 0.3f);

	if(jumpAndShoot(attacker))
		return;

	assemble(attacker);
}

void MagicDefaultAttackSP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		// 未闪避，则给予身体晃动的受击展示
		if(def.dodge() == 0 && !defender->isMyPlayerGroup() && !defender->isDead())
			SkillProcessor::shake(defender);
	}

	this->setRelease(true);   // release immediatelly
}

///////////////////////////////
IMPLEMENT_CLASS(PuTongGongJiSP)

PuTongGongJiSP::PuTongGongJiSP(){

}

PuTongGongJiSP::~PuTongGongJiSP(){

}

void* PuTongGongJiSP::createInstance()
{
	return new PuTongGongJiSP() ;
}

///////////////////////////////
IMPLEMENT_CLASS(RangerDefaultAttackSP)

RangerDefaultAttackSP::RangerDefaultAttackSP()
{
    //m_flyingSpeed = 64*16;

	m_distance_for_reached = 85;
}

RangerDefaultAttackSP::~RangerDefaultAttackSP()
{
}

void* RangerDefaultAttackSP::createInstance()
{
	return new RangerDefaultAttackSP() ;
}

void RangerDefaultAttackSP::onSkillBegan(BaseFighter* attacker)
{
	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_blue.plist", 0.9f);
}

void RangerDefaultAttackSP::onSkillReleased(BaseFighter* attacker)
{
	// extra flying
	if(attacker->isMyPlayerGroup())
	{
		m_flyingSpeed = 64*16;
		m_bFollowBehaviour = false;
		m_nExtraDistance = 64 * 20;
	}
	else
	{
		m_flyingSpeed = 64*8;
	}
	FlyingSkillProcessor::onSkillReleased(attacker);

	//auto scene = attacker->getGameScene();

	//auto target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
	//bool ret = knockback(attacker, target, 16, 0.15f, 0.3f);

	if(jumpAndShoot(attacker))
		return;

	assemble(attacker);
}

void RangerDefaultAttackSP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		// 未闪避，则给予身体晃动的受击展示
		if(def.dodge() == 0 && !defender->isMyPlayerGroup() && !defender->isDead())
			SkillProcessor::shake(defender);
	}

	this->setRelease(true);   // release immediatelly
}

///////////////////////////////
IMPLEMENT_CLASS(WarlockDefaultAttackSP)

WarlockDefaultAttackSP::WarlockDefaultAttackSP(){

}

WarlockDefaultAttackSP::~WarlockDefaultAttackSP()
{
}

void* WarlockDefaultAttackSP::createInstance()
{
	return new WarlockDefaultAttackSP() ;
}

void WarlockDefaultAttackSP::onSkillBegan(BaseFighter* attacker)
{
	SkillProcessor::showWeaponEffect(attacker, "WeaponEffectFire_blue.plist", 1.0f);
}

void WarlockDefaultAttackSP::onSkillReleased(BaseFighter* attacker)
{
	//auto scene = attacker->getGameScene();
	//auto target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
	//bool ret = knockback(attacker, target, 16, 0.15f, 0.3f);

	scatter(attacker);

	this->setRelease(true);
}

void WarlockDefaultAttackSP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		// 未闪避，则给予身体晃动的受击展示
		if(def.dodge() == 0 && !defender->isMyPlayerGroup() && !defender->isDead())
			SkillProcessor::shake(defender);
	}

	this->setRelease(true);
}

///////////////////////////////

IMPLEMENT_CLASS(WarriorDefaultAttackSP)

WarriorDefaultAttackSP::WarriorDefaultAttackSP(){

}

WarriorDefaultAttackSP::~WarriorDefaultAttackSP()
{
}

void* WarriorDefaultAttackSP::createInstance()
{
	return new WarriorDefaultAttackSP() ;
}

void WarriorDefaultAttackSP::onSkillReleased(BaseFighter* attacker)
{
	//auto scene = attacker->getGameScene();

	//auto target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
	//bool ret = knockback(attacker, target, 16, 0.15f, 0.3f);

	scatter(attacker);

	this->setRelease(true);
}

void WarriorDefaultAttackSP::onSkillApplied(BaseFighter* attacker)
{
	auto result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");
	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);

		auto defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
		if(defender == NULL)
			continue;

		// 未闪避，则给予身体晃动的受击展示
		if(def.dodge() == 0 && !defender->isMyPlayerGroup() && !defender->isDead())
			SkillProcessor::shake(defender);
	}

	this->setRelease(true);
}

///////////////////////////////
IMPLEMENT_CLASS(YuanChengGongJiSP)

YuanChengGongJiSP::YuanChengGongJiSP(){
}

YuanChengGongJiSP::~YuanChengGongJiSP()
{
}

void* YuanChengGongJiSP::createInstance()
{
	return new YuanChengGongJiSP() ;
}

void YuanChengGongJiSP::onSkillReleased(BaseFighter* attacker)
{
	FlyingSkillProcessor::onSkillReleased(attacker);

	return;
	/////////////////////////////////////////////////////////
	//// multi-arrows!
	/////////////////////////////////////////////////////////
	//const int arrowNum = 12 / 2;
	//const float arrowIntervalAngle = 4.0f;
	//Vec2 end = mTargetPosition;
	//for(int i = 0; i < arrowNum; i++)
	//{
	//	SimpleActor* arrow = new SimpleActor();
	//	arrow->setGameScene(attacker->getGameScene());
	//	//simpleActor->setWorldPosition(Vec2(targetPos.x, targetPos.y));
	//	//arrow->loadSprite("animation/weapon/arrow01.png");
	//	arrow->loadAnim("animation/texiao/renwutexiao/skill/YCGJ01.anm", true);
	//	arrow->setScale(0.5f);
	//	arrow->setAnchorPoint(Vec2(0.5f, 0));

	//	end = end.rotateByAngle(start, CC_DEGREES_TO_RADIANS(arrowIntervalAngle));
	//	// the arrow will run engough distance
	//	Vec2 offset = GameUtils::getDirection(start, end);
	//	float distance = 500;
	//	offset = offset*distance;
	//	end = end+offset;

	//	arrow->setPosition(start);

	//	float degree = GameUtils::getDegree(start, end);
	//	arrow->setRotation(degree);

	//	// calc the arrow's flying time
	//	const int arrowSpeed = 64*7;
	//	float time = start.getDistance(end) / arrowSpeed;

	//	FiniteTimeAction*  action = Sequence::create(
	//		MoveTo::create(time, end),
	//		RemoveSelf::create(),
	//		//CallFunc::create(this, callfunc_selector(YuanChengGongJiSP::hitTarget)), 
	//		NULL);
	//	arrow->runAction(action);

	//	auto sceneLayer = scene->getChildByTag(GameSceneLayer::kTagSceneLayer);
	//	auto actorLayer = sceneLayer->getChildByTag(kTagActorLayer);
	//	actorLayer->addChild(arrow, SCENE_ROLE_LAYER_BASE_ZORDER);
	//}

	//end = mTargetPosition;
	//for(int i = 0; i < arrowNum; i++)
	//{
	//	SimpleActor* arrow = new SimpleActor();
	//	arrow->setGameScene(attacker->getGameScene());
	//	//simpleActor->setWorldPosition(Vec2(targetPos.x, targetPos.y));
	//	//arrow->loadSprite("animation/weapon/arrow01.png");
	//	arrow->loadAnim("animation/texiao/renwutexiao/skill/YCGJ01.anm", true);
	//	arrow->setAnchorPoint(Vec2(0.5f, 0));
	//	arrow->setScale(0.5f);

	//	end = end.rotateByAngle(start, CC_DEGREES_TO_RADIANS(-arrowIntervalAngle));
	//	// the arrow will run engough distance
	//	Vec2 offset = GameUtils::getDirection(start, end);
	//	float distance = 500;
	//	offset = offset* distance;
	//	end = end+offset;

	//	arrow->setPosition(start);

	//	float degree = GameUtils::getDegree(start, end);
	//	arrow->setRotation(degree);

	//	// calc the arrow's flying time
	//	const int arrowSpeed = 64*7;
	//	float time = start.getDistance(end) / arrowSpeed;

	//	FiniteTimeAction*  action = Sequence::create(
	//		MoveTo::create(time, end),
	//		RemoveSelf::create(),
	//		//CallFunc::create(this, callfunc_selector(YuanChengGongJiSP::hitTarget)), 
	//		NULL);
	//	arrow->runAction(action);

	//	auto sceneLayer = scene->getChildByTag(GameSceneLayer::kTagSceneLayer);
	//	auto actorLayer = sceneLayer->getChildByTag(kTagActorLayer);
	//	actorLayer->addChild(arrow, SCENE_ROLE_LAYER_BASE_ZORDER);
	//}
}

///////////////////////////////

IMPLEMENT_CLASS(MusouSkill1SP)

MusouSkill1SP::MusouSkill1SP(){
	m_fElapsed = 0.0f;
}

MusouSkill1SP::~MusouSkill1SP()
{
}

void* MusouSkill1SP::createInstance()
{
	return new MusouSkill1SP() ;
}

void MusouSkill1SP::update(float dt)
{
	if(isRelease()){
		return;
	}

	//if(GameUtils::millisecondNow() - m_fStartTime > 1.0f * 3000)
	//{
	//	this->setRelease(true);
	//	return;
	//}

	//// create effect
	//float m_fInterval = 0.01f;
	//m_fElapsed += dt;
 //   if (m_fElapsed >= m_fInterval)
 //   {
 //		RangeRandomSpecialEffect::generate(
	//		"animation/texiao/renwutexiao/DY3/dy3.anm",
	//		attacker->getGameScene(),
	//		m_startPoint,
	//		Size(900, 900));

 //       m_fElapsed = 0;
 //   }
	//return;

	//////////////////////////////////////////////////

	if(this->getSkillResult() != NULL)
	{
		// get defender
		BaseFighter* defender = NULL;
		auto result = getSkillResult();
		for (int i = 0; i < result->defenders_size(); i++) {
			Defender def = result->defenders(i);
			defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
			if(defender == NULL)
				break;

			// set the actor's world position, by its cocos2d position
			if(defender != NULL)
			{
				auto scene = GameView::getInstance()->getGameScene();
				defender->setWorldPosition(scene->convertToGameWorldSpace(defender->getPosition()));
			}
		}

		// finished
		if(GameUtils::millisecondNow() - m_knockbackStartTime > (DZ1TMSSP_KNOCKBACK_TIME+KNOCKBACK_HIT_RECOVER_TIME) * 1000)
		{
			// get defender
			for (int i = 0; i < result->defenders_size(); i++) {
				Defender def = result->defenders(i);
				defender = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(def.target()));
				if(defender == NULL)
					break;

				// to normal action
				if(defender != NULL)
				{
					if(!defender->isDead())
						defender->changeAction(ACT_STAND);
				}
			}

			this->setRelease(true);
		}
	}
}

void MusouSkill1SP::onSkillReleased(BaseFighter* attacker)
{
	m_fStartTime = GameUtils::millisecondNow();
	m_startPoint = attacker->getWorldPosition();
}

void MusouSkill1SP::onSkillApplied(BaseFighter* attacker)
{
	// this special skill is used by onSkillReleased(), so ignore in this onSkillApplied();
	//setRelease(true);
	//return;

	auto scene = attacker->getGameScene();
	if(attacker->getRoleId() != BaseFighter::getMyPlayerId())
	{
		// add explode effect
		SimpleActor* pExplode = new SimpleActor();
		pExplode->setGameScene(scene);
		pExplode->setLayerId(2);   // sky layer
		pExplode->setWorldPosition(attacker->getWorldPosition());
		pExplode->loadAnim("animation/texiao/renwutexiao/lhjn/lhjn_2.anm");
		//pExplode->setAnchorPoint(Vec2(0.5f, 0));
		scene->getActorLayer()->addChild(pExplode, SCENE_SKY_LAYER_BASE_ZORDER);
		pExplode->release();
	}

	bool ret = knockback(attacker, DZ1TMSSP_KNOCKBACK_TIME, KNOCKBACK_HIT_RECOVER_TIME);
	if(ret)
		m_knockbackStartTime = GameUtils::millisecondNow();
	else
		setRelease(true);

	if(attacker->isMyPlayer())
	{
		// camera special effect
		auto scene = attacker->getGameScene();
        Vec2 sourcePoint = attacker->getWorldPosition();
		scene->getSceneCamera()->zoomScreen(scene, sourcePoint);

		// show "total damage" effect
		// calc total damage number
		int totalDamageNumber = 0;
		auto result = getSkillResult();
		CCAssert(result != NULL, "skill result should not be nil");
		for (int i = 0; i < result->defenders_size(); i++) {
			Defender def = result->defenders(i);
			totalDamageNumber += def.damage();
		}
		if(totalDamageNumber > 0)
		{
			auto pNode = Node::create();
			// Dmg
			//Label* labelDMG = Label::createWithTTF(StringDataManager::getString("total_dmg"), APP_FONT_NAME, 35);
			Sprite* labelDMG = Sprite::create("res_ui/font/zongshanghai.png");
			labelDMG->setColor(Color3B(255,201,14));
			labelDMG->setAnchorPoint(Vec2(1,0));
			labelDMG->setPosition(Vec2(-4, 52));
			//CCLabel* labelDMG = CCLabel::create(StringDataManager::getString("total_dmg"), "res_ui/font/ziti_1.fnt"); 
			//labelDMG->setAnchorPoint(Vec2(0,0));
			//labelDMG->setPosition(Vec2(0,0));
			pNode->addChild(labelDMG);
			// DmgValue
			NumberRollEffect * dmgEffect = NumberRollEffect::create(totalDamageNumber,"res_ui/font/ziti_13.fnt",2.0f);
			dmgEffect->setScale(1.3f);
			dmgEffect->setAnchorPoint(Vec2(1,0));
			//dmgEffect->setPosition(Vec2(labelDMG->getContentSize().width+3, 0));
			dmgEffect->setPosition(Vec2(0, 0));
			pNode->addChild(dmgEffect);

			Action* action = Sequence::create(
				DelayTime::create(3.5f),
				RemoveSelf::create(),
				NULL);
			pNode->runAction(action);

			Size winSize = Director::getInstance()->getWinSize();
			pNode->setPosition(Vec2(winSize.width/2 + 180, winSize.height/2 + 95));
			GameView::getInstance()->getMainUIScene()->addChild(pNode);

			//MyPlayer* me = GameView::getInstance()->myplayer;
			//pNode->setPosition(Vec2(me->getPositionX() + 180, me->getPositionY() + 95));
			//scene->getActorLayer()->addChild(pNode, SCENE_TOP_LAYER_BASE_ZORDER);
		}
	}
}

///////////////////////////////

IMPLEMENT_CLASS(WarlockFlashSP)

WarlockFlashSP::WarlockFlashSP(){
}

WarlockFlashSP::~WarlockFlashSP()
{
}

void* WarlockFlashSP::createInstance()
{
	return new WarlockFlashSP() ;
}

void WarlockFlashSP::update(float dt)
{
	if(isRelease()){
		return;
	}

	if(GameUtils::millisecondNow() - m_startTime > m_totalTime * 1000)
	{
		setRelease(true);
	}
}

void WarlockFlashSP::onSkillReleased(BaseFighter* attacker)
{
	//MomentSkillProcessor::onSkillReleased(attacker);

	// jump ahead
	auto scene = attacker->getGameScene();

	// it's not my generals, ignore
	if(attacker->getPosInGroup() < 1)
		return;

	auto target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
	if(target == NULL)
		return;

	jumpAhead(target, attacker);

	this->setRelease(true);
}

void WarlockFlashSP::jumpAhead(BaseFighter* target, BaseFighter* pAttacker)
{
	// jump ahead
	auto scene = pAttacker->getGameScene();

	// it's not my generals, ignore
	if(pAttacker->getPosInGroup() < 1)
		return;

	if(target == NULL)
		return;

	Vec2 targetWorldPosition =  target->getWorldPosition();

	if(pAttacker->isMyPlayerGroup())
	{
		float x = targetWorldPosition.x;
		float y = targetWorldPosition.y;
		// find the random position near the target
		const int range = 64 + 16;
		x += (CCRANDOM_0_1() - 0.5f) * range;
		y += (CCRANDOM_0_1() - 0.5f) * range;
		targetWorldPosition.setPoint(x, y);
	}

	ActionJumpContext context;
	context.startTime = GameUtils::millisecondNow();
	context.targetPosition = scene->convertToCocos2DSpace(targetWorldPosition);
	context.jumpDirection = 1;
	context.jumpHeight = 50;
	float jumpBackSpeed = 800.f;   // 900.f
	context.jumpDuration = pAttacker->getWorldPosition().getDistance(targetWorldPosition) / jumpBackSpeed;
	context.setContext(pAttacker);

	m_startTime = GameUtils::millisecondNow();
	m_totalTime = context.jumpDuration;

	pAttacker->changeAction(ACT_JUMP);
	//attacker->stopAllCommand();
}

void WarlockFlashSP::onSkillApplied(BaseFighter* attacker)
{
	auto ret = getSkillResult();
	if(ret == NULL)
		return;
	//CCAssert(ret->defenders_size() == 1, "if not 1, maybe error");
	if(ret->defenders_size() <= 0)
		return;

	// jump ahead
	auto scene = attacker->getGameScene();

	// it's not my generals, ignore
	if(attacker->getPosInGroup() < 1)
		return;

	Defender def = ret->defenders(0);
	auto target = dynamic_cast<BaseFighter*>(scene->getActor(def.target()));
	if(target == NULL)
		return;

	jumpAhead(target, attacker);

	/////////////////////////////////////////////////////
	// synchronize the real position
	attacker->setRealWorldPosition(Vec2(ret->x(), ret->y()));
}
