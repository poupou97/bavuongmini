#ifndef _SKILL_SKILLPROCESSOR_H_
#define _SKILL_SKILLPROCESSOR_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;

/**
 * ������̴������� ���ܵĻ��ƣ��߼��
 * @author zhao gang
 *
 */
class SkillSeed;
class CSkillResult;
class GameSceneLayer;
class BaseFighter;

class SkillProcessor : public Ref {
public:
	SkillProcessor();
	virtual ~SkillProcessor();

	virtual void init();
	virtual void release() {};

	// callback when BaseFighter began the attack action
	virtual void onSkillBegan(BaseFighter* attacker);
	// callback when BaseFighter finished the attack action
	virtual void onSkillReleased(BaseFighter* attacker);
	// callback when the client revieve the skill push message from server
	virtual void onSkillApplied(BaseFighter* attacker);

	virtual void update(float dt);
	
	/**
	 * Ȳ��ż����ͷ�ʱ��Ч
	 */
	void playFightSound();

	SkillSeed* getSkillSeed();
	void setSkillSeed(SkillSeed* skillSeed);

	CSkillResult* getSkillResult();
	void setSkillResult(CSkillResult* skillResult);
	
	void loadSkillEffect(const char* currEffect);
	cocos2d::Vector<Node*> getApEffects();
	void setApEffects(cocos2d::Vector<Node*>  apEffects);
	
	GameSceneLayer* getScene();
	void setScene(GameSceneLayer* scene);

	float getPosX();
	void setPosX(float mX);

	float getPosY();
	void setPosY(float mY);

	void setEffectScaleX(float sx);
	void setEffectScaleY(float sy);
	
	bool isHide();
	void setHide(bool isHide);
	
	bool isRelease();
	void setRelease(bool isRelease);
	
	/**
	 * ����Լ�����Э��������ͳ��
	 * @param attacker
	 */
	void skillDamage(BaseFighter* attacker);
	
	void sendSkillCommand(BaseFighter* attacker);

	/**
	 * м�����ʹ� ÷��͹����ܵ�����������˺�
	 */
	void countDamage(BaseFighter* attacker);
	
	// ������Ļ����
	void hideOutScreen();
	
	void updateResume(BaseFighter* attacker);

	/** set the switch which will be used for checking if sending skill request */
	inline void setSendAttackReq(bool bSend) {  m_bSendAttackReq = bSend; };

	static bool knockback(BaseFighter* attacker, BaseFighter* defender, float knockDistance, float backDuration, float hitRecoverTime = 0.5f);

	// ر�����ߣ������о����ϵĺ��ˣ����ı䵱ǰ����������ڹ��������Ź��
	static bool hitback(BaseFighter* attacker, BaseFighter* defender, float knockDistance, float backDuration);

	// ������𶯣����ڱ�actor��ܵ�ĳ�ִ����Ч�
	static void quake(BaseFighter* actor, float duration = 0.9f);

	// ����һ�ǰ��ζ������ڱ�actor��ܵ�ĳ�ִ����Ч�
	// @param shakeLeftRight if true, shake left to right, otherwise, shake forward and backward
	static void shake(BaseFighter* actor, bool shakeLeftRight = false, int shakeTimes = 3, float range = 10.0f);

	// ���˸߸ߵ������
	static void flyup(BaseFighter* attacker, BaseFighter* defender);

	static void showWeaponEffect(BaseFighter* actor, const char* effectFileName, float stayTime);

	// show ground effect, the groud effect will be disappear after a short while
	void showGroundEffect(const char* effectName, GameSceneLayer* scene, float firstDelayTime, float secondDelayTime, float fadeOutTime);

	// show the magic halo on the ground
	// for example, tqms skill
	static void addMagicHaloToGround(GameSceneLayer* scene, Vec2 worldPosition);
	static void addMagicHaloToGround(GameSceneLayer* scene, Vec2 worldPosition, std::string str_spritePath, 
		float total_duration = 1.5f, float stay_duration = 0.5f, float scale_x = 1.6f, float scale_y = 0.8f);
	// show the "wave halo" on the ground
	static void addWaveHaloToGround(GameSceneLayer* scene, Vec2 worldPosition, std::string spritePathName, float interval, int times = 1, float maxScale = 2.0f);

	static void addBreathingHaloToGround(GameSceneLayer* scene, Vec2 worldPosition, const char* effectPathName);

protected:
	/** request skill damage to server */
	void sendAttackReq(BaseFighter* attacker);

	// knokback the enemy
	bool knockback(BaseFighter* attacker, float backDuration, float hitRecoverTime = 0.5f);
	
	// ����뿪�Ͷ��������Ա���ַ�ɢ���Σ���Ǽ�����һ��
	void scatter(BaseFighter* attacker);
	// ��佫����ǹ�Զ���ۼ���һ�
	void assemble(BaseFighter* attacker);
	// ���ĳЩ����£��佫�ع�����ʵλ�( RealWorldPosition )
	void returnHome(BaseFighter* attacker);

	void addEffectToGround(BaseFighter* actor, std::string& effectName);
	void _addEffectToGround(GameSceneLayer* scene, float x, float y, std::string& effectName);

	/**
	   * @param bFoot ü�����Ч�������ϣ����ǽ��¡�Ĭ�Ϸ������
	   **/
	void addEffectToBody(BaseFighter* actor, std::string& effectName, int animDirection, bool bFoot = false);

	static bool isBoss(BaseFighter* actor);

protected:
	// the scale of the effect
	float m_effectScaleX;
	float m_effectScaleY;

private:
	// skill info restore in this variable
	SkillSeed* skillSeed;

	// skill result
	CSkillResult* mSkillResult;

	GameSceneLayer* scene;

	// the position of the skill effect
	float m_x;
	float m_y;
	
	cocos2d::Vector<Node*> apEffects;
	
	bool m_bIsRelease;
	bool m_bIsHide;

	bool m_bSendAttackReq;
};

/////////////////////////////////////////////////////////////////

/** @brief Play the CCLegendAnimation
*/
class CCPlayLegendAnimation : public ActionInstant
{
public:
    CCPlayLegendAnimation(){}
    virtual ~CCPlayLegendAnimation(){}
    //super methods
    virtual void update(float time);

public:

    /** Allocates and initializes the action */
    static CCPlayLegendAnimation * create();
};

#endif