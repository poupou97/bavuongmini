#ifndef _SKILL_SKILLPROCESSOR_FLYINGSKILLPROCESSOR_H_
#define _SKILL_SKILLPROCESSOR_FLYINGSKILLPROCESSOR_H_

#include "SkillProcessor.h"
#include "../../../common/CKBaseClass.h"

USING_NS_CC;
USING_NS_CC_EXT;

#define FLYING_ITEM_OFFSET_FROM_GROUND 45
#define FLYING_ITEM_DISTANCE_OFFSET_WHEN_LAUNCHING 32

class SimpleActor;
class BaseFighter;

/**
 * 飞行技能
 * 比如，弓箭射击、火球术等
 * @author zhaogang
 */
class FlyingSkillProcessor : public CKBaseClass, public SkillProcessor
{
	enum {
		k_state_check = 0,
		k_state_extra_distance = 1,
	};

private:
    DECLARE_CLASS(FlyingSkillProcessor)

public:
	FlyingSkillProcessor();
	virtual ~FlyingSkillProcessor();

	virtual void init();
	virtual void update(float dt);

	virtual void release();

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);

	static void* createInstance() ;
	
protected:
	BaseFighter* attacker;
	float m_flyingSpeed;   // the speed of the flying item

	bool jumpAndShoot(BaseFighter* attacker);

	// 飞行道具是否在飞到目的地后，再额外飞一些距离。用于穿透等效果的实现
	float m_nExtraDistance;

	// 飞行道具持续追踪目标，直到命中
	bool m_bFollowBehaviour;
	bool m_bReached;   // 命中目标

	// extra streak
	bool m_bExtraStreak;
	float m_fStreakFade;
	float m_fStreakMinSeg;
	float m_fStreakStrokeWidth;
	Color3B m_StreakColor;

	bool m_bExtraParticle;
	std::string m_ExtraParticleFileName;

	// random position of the target
	bool m_bEnableRandom;
	float m_fRandomRange;

	float m_distance_for_reached;

private:
	bool checkHitTarget(float dt);
	
private:
	float m_fStartTime;
	float m_fDuration;
	std::string mFlyingEndedEffect;

	SimpleActor* m_flyingActor;
	/** 技能要打击的目标 */
	long long m_targetRoleId;
	Vec2 m_targetPosition;
	MotionStreak* m_streak;
	ParticleSystemQuad* m_extraParticle;

	// 阶段一: 检测命中，若有穿透则进入阶段二，否则结束
	// 阶段二: 穿透，继续飞行
	int m_state;
	float m_stateBeginTime;
};

#endif