#ifndef _SKILL_ALLSKILLS_H_
#define _SKILL_ALLSKILLS_H_

#include "MomentSkillProcessor.h"
#include "FlyingSkillProcessor.h"

class ActionChargeContext;

/////////////////////////////////////////////////////
/**
 * 破釜沉舟
 * @author yang jun 
 */
class AX1pfczSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(AX1pfczSP)

public:
	AX1pfczSP();
	virtual ~AX1pfczSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
};

/////////////////////////////////////////////////////
/**
 * 一骑当千
 * @author yang jun 
 */
class AX2yjdqSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(AX2yjdqSP)

public:
	AX2yjdqSP();
	virtual ~AX2yjdqSP();

	static void* createInstance();

	virtual void update(float dt);

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);

private:
	long long m_chargeStartTime;
	float m_chargeTotalTime;
	Vec2 m_direction_point;
	float m_degree;

private:
	void createDragonAction(ActionChargeContext* pContext);
};

/////////////////////////////////////////////////////
/**
 * 燕人咆哮
 * @author yang jun 
 */
class AX3yrpxSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(AX3yrpxSP)

public:
	AX3yrpxSP();
	virtual ~AX3yrpxSP();

	static void* createInstance() ;

	virtual void onSkillReleased(BaseFighter* attacker);
};

/////////////////////////////////////////////////////
/**
 * 虎啸苍穹
 * @author yang jun 
 */
class AY1hxcqSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(AY1hxcqSP)

public:
	AY1hxcqSP();
	virtual ~AY1hxcqSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};

/////////////////////////////////////////////////////
/**
 * 龙盘虎踞
 * @author yang jun 
 */
class AY2lphjSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(AY2lphjSP)

public:
	AY2lphjSP();
	virtual ~AY2lphjSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);

};

/////////////////////////////////////////////////////
/**
 * 一马平川
 * @author yang jun 
 */
class AY3ympcSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(AY3ympcSP)

public:
	AY3ympcSP();
	virtual ~AY3ympcSP();

	static void* createInstance() ;

	virtual void onSkillApplied(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
};

/////////////////////////////////////////////////////
/**
 * 雷霆怒吼
 * @author yang jun 
 */
class AZ1ltnhSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(AZ1ltnhSP)

public:
	AZ1ltnhSP();
	virtual ~AZ1ltnhSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};

/////////////////////////////////////////////////////
class AZ1SP : public MomentSkillProcessor
{
private:
	DECLARE_CLASS(AZ1SP)

public:
	AZ1SP();
	virtual ~AZ1SP();

	static void* createInstance() ;

protected:

private:

};

/////////////////////////////////////////////////////
/**
 * 一夫当关
 * @author yang jun 
 */
class AZ2yfdgSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(AZ2yfdgSP)

public:
	AZ2yfdgSP();
	virtual ~AZ2yfdgSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};

/////////////////////////////////////////////////////
/**
 * 血溅十步
 * @author yang jun 
 */
class AZ3xjsbSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(AZ3xjsbSP)

public:
	AZ3xjsbSP();
	virtual ~AZ3xjsbSP();

	static void* createInstance() ;

	virtual void update(float dt);

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);

private:
	float m_fStartTime;
	float m_fElapsed;
	Vec2 m_startPoint;
};

/////////////////////////////////////////////////////
/**
 * 苍穹之昂
 * @author yang jun 
 */
class BX1cqzmSP :  public MomentSkillProcessor
{
private:
    DECLARE_CLASS(BX1cqzmSP)

public:
	BX1cqzmSP();
	virtual ~BX1cqzmSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};

/////////////////////////////////////////////////////
class BX1SP : public FlyingSkillProcessor
{
private:
	DECLARE_CLASS(BX1SP)

public:
	BX1SP();
	virtual ~BX1SP();

	static void* createInstance() ;

protected:

};

/////////////////////////////////////////////////////
/**
 * 风动三千
 * @author yang jun 
 */
class BX2fdsqSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(BX2fdsqSP)

public:
	BX2fdsqSP();
	virtual ~BX2fdsqSP();

	virtual void init();

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);

	static void* createInstance() ;

private:

};

/////////////////////////////////////////////////////
/**
 * 参商之戾
 * @author yang jun 
 */
class BX3sszlSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(BX3sszlSP)

public:
	BX3sszlSP();
	virtual ~BX3sszlSP();

	static void* createInstance() ;

	virtual void onSkillReleased(BaseFighter* attacker);
};

/////////////////////////////////////////////////////
/**
 * 天谴秘术
 * @author yang jun 
 */
class BY1tqmsSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(BY1tqmsSP)

public:
	BY1tqmsSP();
	virtual ~BY1tqmsSP();

	static void* createInstance() ;

	virtual void update(float dt);

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);

private:
	float m_fStartTime;
	float m_fElapsed;
	Vec2 m_startPoint;
};

/////////////////////////////////////////////////////
/**
 * 鼓舞军心
 * @author yang jun 
 */
class BY2gwjxSP: public MomentSkillProcessor
{
private:
    DECLARE_CLASS(BY2gwjxSP)

public:
	BY2gwjxSP();
	virtual ~BY2gwjxSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};

/////////////////////////////////////////////////////
/**
 * 死门之阵
 * @author yang jun 
 */
class BY3smzzSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(BY3smzzSP)

public:
	BY3smzzSP();
	virtual ~BY3smzzSP();

	static void* createInstance() ;

	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};

/////////////////////////////////////////////////////
/**
 * 生死变
 * @author yang jun 
 */
class BZ1ssbSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(BZ1ssbSP)

public:
	BZ1ssbSP();
	virtual ~BZ1ssbSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};

/////////////////////////////////////////////////////
/**
 * 五气朝元
 * @author yang jun 
 */
class BZ2wqcySP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(BZ2wqcySP)

public:
	BZ2wqcySP();
	virtual ~BZ2wqcySP();

	virtual void onSkillApplied(BaseFighter* attacker);

	static void* createInstance() ;
};

/////////////////////////////////////////////////////
/**
 * 一鼓作气
 * @author yang jun 
 */
class BZ3ygzqSP : public FlyingSkillProcessor
{
private:
    DECLARE_CLASS(BZ3ygzqSP)

public:
	BZ3ygzqSP();
	virtual ~BZ3ygzqSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};

/////////////////////////////////////////////////////

///////////////////////
/**
 * 运筹帷幄
 * @author yang jun 
 */
class CX1ycwwSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(CX1ycwwSP)

public:
	CX1ycwwSP();
	virtual ~CX1ycwwSP();

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);

	static void* createInstance() ;
};
//////////////////////////////////////////////////////
/**
 * 天地无极
 * @author yang jun 
 */
class CY1tdwjSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(CY1tdwjSP)

public:
	CY1tdwjSP();
	virtual ~CY1tdwjSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};
///////////////////////////////////////
/**
 * 大悲咒
 * @author yang jun 
 */
class CY2dbzSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(CY2dbzSP)

public:
	CY2dbzSP();
	virtual ~CY2dbzSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};
//////////////////////////////////
class CY1SP : public MomentSkillProcessor
{
private:
	DECLARE_CLASS(CY1SP)

public:
	CY1SP();
	virtual ~CY1SP();

	static void* createInstance() ;

protected:

private:

};
////////////////////////
/**
 * 空城计
 * @author yang jun 
 */
class CX3kcjSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(CX3kcjSP)

public:
	CX3kcjSP();
	virtual ~CX3kcjSP();

	static void* createInstance() ;

	virtual void onSkillReleased(BaseFighter* attacker);
};
///////////////////////////////////
/**
 * 失魂引
 * @author yang jun 
 */
class CX2shySP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(CX2shySP)

public:
	CX2shySP();
	virtual ~CX2shySP();

	static void* createInstance() ;

	virtual void update(float dt);

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
};
/////////////////////////////////////////
// class CY1SP : public MomentSkillProcessor
// {
// private:
// 	DECLARE_CLASS(CY1SP)
// 
// public:
// 	CY1SP();
// 	virtual ~CY1SP();
// 
// 	static void* createInstance() ;
// 
// protected:
// 
// private:
// 
// };
////////////////////////////////
/**
 * 缚神诀
 * @author yang jun 
 */
class CZ3fsjSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(CZ3fsjSP)

public:
	CZ3fsjSP();
	virtual ~CZ3fsjSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
};

/////////////////////////////////////
/**
 * 无懈可击
 * @author yang jun 
 */
class CZ2wxkjSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(CZ2wxkjSP)

public:
	CZ2wxkjSP();
	virtual ~CZ2wxkjSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};

/////////////////////////////////////////
/**
 * 妙手回春
 * @author yang jun 
 */
class CZ1mshcSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(CZ1mshcSP)

public:
	CZ1mshcSP();
	virtual ~CZ1mshcSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
};
/////////////////////////////////////////
/**
 * 凤仪九天
 * @author yang jun 
 */
class CY3fyjtSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(CY3fyjtSP)

public:
	CY3fyjtSP();
	virtual ~CY3fyjtSP();

	static void* createInstance() ;

	virtual void onSkillReleased(BaseFighter* attacker);
};
//////////////////////////////
/**
 * 大悲咒
 * @author yang jun 
 */
// class CY2dbzSP : public MomentSkillProcessor
// {
// private:
   // DECLARE_CLASS(CY2dbzSP)
// 
// public:
// 	CY2dbzSP();
// 	virtual ~CY2dbzSP();
// 
// 	static void* createInstance() ;
// };


////////////////////////////////////////////////////

/**
 * 祸伏矢
 * @author yang jun 
 */
class DZ3hfsSP : public FlyingSkillProcessor
{
private:
    DECLARE_CLASS(DZ3hfsSP)

public:
	DZ3hfsSP();
	virtual ~DZ3hfsSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};
/////////////////////////////////
/**
 * 镇龙击
 * @author yang jun 
 */
class DZ2zljSP : public FlyingSkillProcessor
{
private:
    DECLARE_CLASS(DZ2zljSP)

public:
	DZ2zljSP();
	virtual ~DZ2zljSP();

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);

	static void* createInstance() ;
};
////////////////////////////////////
/**
 * 退魔矢
 * @author yang jun 
 */
class DZ1tmsSP : public FlyingSkillProcessor
{
private:
    DECLARE_CLASS(DZ1tmsSP)

public:
	DZ1tmsSP();
	virtual ~DZ1tmsSP();

	virtual void update(float dt);

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);

	static void* createInstance() ;

private:
	long long m_knockbackStartTime;
};
//////////////////////////////////
/**
 * 后羿皇矢
 * @author yang jun 
 */
class DY3hyhsSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(DY3hyhsSP)

public:
	DY3hyhsSP();
	virtual ~DY3hyhsSP();

	virtual void update(float dt);

	static void* createInstance() ;

	virtual void onSkillApplied(BaseFighter* attacker);

private:
	float m_fStartTime;
	float m_fElapsed;
	Vec2 m_startPoint;
};
//////////////////////////////////////
/**
 * 同仇敌忾
 * @author yang jun 
 */
class DY2tcdkSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(DY2tcdkSP)

public:
	DY2tcdkSP();
	virtual ~DY2tcdkSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};
///////////////////////////////////
/**
 * 势如破竹
 * @author yang jun 
 */
class DY1srpzSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(DY1srpzSP)

public:
	DY1srpzSP();
	virtual ~DY1srpzSP();

	virtual void update(float dt);

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);

private:
	float m_fStartTime;
	float m_fDuration;
	//float m_fElapsed;
	//Vec2 m_startPoint;
};
//////////////////////////////////////////////
/**
 * 百步穿杨
 * @author yang jun 
 */
class DX3bbcySP :  public FlyingSkillProcessor
{
private:
    DECLARE_CLASS(DX3bbcySP)

public:
	DX3bbcySP();
	virtual ~DX3bbcySP();

	static void* createInstance() ;
};
///////////////////////////////////////
/**
 * 潜龙勿用
 * @author yang jun 
 */
class DX2qlwySP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(DX2qlwySP)

public:
	DX2qlwySP();
	virtual ~DX2qlwySP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
};
////////////////////////////////////////
/**
 * 落樱矢
 * @author yang jun 
 */
class DX1lysSP : public FlyingSkillProcessor
{
private:
    DECLARE_CLASS(DX1lysSP)

public:
	DX1lysSP();
	virtual ~DX1lysSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);
};

///////////////////////////////////////////////
class MagicDefaultAttackSP : public FlyingSkillProcessor
{
private:
	DECLARE_CLASS(MagicDefaultAttackSP)

public:
	MagicDefaultAttackSP();
	virtual ~MagicDefaultAttackSP();

	static void* createInstance() ;

	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);

protected:

private:

};

///////////////////////////////////////////////
/**
 * 普通攻击
 * @author zhao gang
 */
class PuTongGongJiSP : public MomentSkillProcessor
{
private:
    DECLARE_CLASS(PuTongGongJiSP)

public:
	PuTongGongJiSP();
	virtual ~PuTongGongJiSP();

	static void* createInstance() ;
};

///////////////////////////////////////////////
class RangerDefaultAttackSP : public FlyingSkillProcessor
{
private:
	DECLARE_CLASS(RangerDefaultAttackSP)

public:
	RangerDefaultAttackSP();
	virtual ~RangerDefaultAttackSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);

protected:

private:

};

///////////////////////////////////////////////
class WarlockDefaultAttackSP : public MomentSkillProcessor
{
private:
	DECLARE_CLASS(WarlockDefaultAttackSP)

public:
	WarlockDefaultAttackSP();
	virtual ~WarlockDefaultAttackSP();

	static void* createInstance() ;

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);

protected:

private:

};

///////////////////////////////////////////////
class WarriorDefaultAttackSP :  public MomentSkillProcessor
{
private:
	DECLARE_CLASS(WarriorDefaultAttackSP)

public:
	WarriorDefaultAttackSP();
	virtual ~WarriorDefaultAttackSP();

	static void* createInstance() ;

	virtual void onSkillReleased(BaseFighter* attacker);
	virtual void onSkillApplied(BaseFighter* attacker);

protected:

private:

};

///////////////////////////////////////////////
/**
 * long distance attack, using bow
 * @author zhao gang
 */
class YuanChengGongJiSP : public FlyingSkillProcessor
{
private:
    DECLARE_CLASS(YuanChengGongJiSP)

public:
	YuanChengGongJiSP();
	virtual ~YuanChengGongJiSP();

	virtual void onSkillReleased(BaseFighter* attacker);

	static void* createInstance() ;
	
protected:

};

/////////////////////////////////////////////////////
/**
 * 无双（即龙魂）额外特效
 * @author zhao gang
 */
class MusouSkill1SP : public MomentSkillProcessor
{
private:
	DECLARE_CLASS(MusouSkill1SP)

public:
	MusouSkill1SP();
	virtual ~MusouSkill1SP();

	static void* createInstance() ;

	virtual void update(float dt);

	virtual void onSkillApplied(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);

private:
	float m_fStartTime;
	float m_fElapsed;
	Vec2 m_startPoint;
	float m_knockbackStartTime;
};

/////////////////////////////////////////////////////
/**
 * 跳跃技能
 * 目前用于豪杰的快速近身（类似猛将的冲锋）
 * @author zhao gang
 */
class WarlockFlashSP : public MomentSkillProcessor
{
private:
	DECLARE_CLASS(WarlockFlashSP)

public:
	WarlockFlashSP();
	virtual ~WarlockFlashSP();

	static void* createInstance() ;

	virtual void update(float dt);

	virtual void onSkillApplied(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);

private:
	void jumpAhead(BaseFighter* target, BaseFighter* pAttacker);

	long long m_startTime;
	float m_totalTime;
};

#endif