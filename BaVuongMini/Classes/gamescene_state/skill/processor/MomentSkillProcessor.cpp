#include "MomentSkillProcessor.h"

#include "../GameFightSkill.h"
#include "../SkillSeed.h"
#include "../../../gamescene_state/role/BaseFighter.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../utils/GameUtils.h"
#include "../../../legend_engine/CCLegendAnimation.h"
#include "../../../GameView.h"

MomentSkillProcessor::MomentSkillProcessor(){
	attacker = NULL;
	defender = NULL;
}

MomentSkillProcessor::~MomentSkillProcessor(){

}
	
void MomentSkillProcessor::init() {
	SkillProcessor::init();   // must call super calss's init()

	setHide(true);

	sendAttackReq(attacker);

	attacker = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(getSkillSeed()->attacker));
}

void MomentSkillProcessor::update(float dt){
	if(isRelease()){
		return;
	}

	updateResume(attacker);
}

void MomentSkillProcessor::release(){
}

void MomentSkillProcessor::onSkillBegan(BaseFighter* attacker)
{
}
void MomentSkillProcessor::onSkillReleased(BaseFighter* attacker)
{
	auto scene = attacker->getGameScene();
	auto fightSkill = attacker->getGameFightSkill(getSkillSeed()->skillId.c_str(), getSkillSeed()->skillProcessorId.c_str());
    
    //CCLOG("role: %lld, release ( %s )", attacker->getRoleId(), getSkillSeed()->skillId.c_str());

	int animDirection = 0;   // default effect action is Zero

	char targetScopeType = fightSkill->getTargetScopeType();
	if(!GameFightSkill::isNoneLock(targetScopeType))
	{
		auto target = dynamic_cast<BaseFighter*>(scene->getActor(getSkillSeed()->defender));
		//if(target == NULL)
		//	return;

		// calc effect's direction
		if(target != NULL)
		{
			if(fightSkill->getSkillBin()->releaseEffectDir == SkillBin::effect_has_direction)
			{
				animDirection = attacker->getAnimDirection(scene->convertToCocos2DWorldSpace(attacker->getWorldPosition()), 
					scene->convertToCocos2DWorldSpace(target->getWorldPosition()), DEFAULT_ANGLES_DIRECTION_DATA);
				attacker->setAnimDir(animDirection);
			}
		}

		// new skill effect
		if(fightSkill->getSkillBin()->releaseEffect != "")
		{
			int pos = fightSkill->getSkillBin()->releaseEffectPos;
			int toGround = fightSkill->getSkillBin()->releaseEffectToGround;
			if(pos == SkillBin::effect_pos_to_target)
			{
				if(toGround == SkillBin::effect_to_ground)
				{
					if(target != NULL)
						addEffectToGround(target, fightSkill->getSkillBin()->releaseEffect);
					else
						_addEffectToGround(scene, getSkillSeed()->x, getSkillSeed()->y, fightSkill->getSkillBin()->releaseEffect);
				}
				else
				{
					if(target != NULL)
						addEffectToBody(target, fightSkill->getSkillBin()->releaseEffect, animDirection, fightSkill->getSkillBin()->releaseEffectLayer == 0);
					else
						_addEffectToGround(scene, getSkillSeed()->x, getSkillSeed()->y, fightSkill->getSkillBin()->releaseEffect);
				}
			}
			else if(pos == SkillBin::effect_pos_to_self)
			{
				if(toGround == SkillBin::effect_to_ground)
				{
					addEffectToGround(attacker, fightSkill->getSkillBin()->releaseEffect);
				}
				else
				{
					addEffectToBody(attacker, fightSkill->getSkillBin()->releaseEffect, animDirection, fightSkill->getSkillBin()->releaseEffectLayer == 0);
				}
			}
		}
	}
	else
	{
		// attack directly
		// new skill effect
		if(fightSkill->getSkillBin()->releaseEffect != "")
		{
			int toGround = fightSkill->getSkillBin()->releaseEffectToGround;
			if(toGround == SkillBin::effect_to_ground)
			{
				addEffectToGround(attacker, fightSkill->getSkillBin()->releaseEffect);
			}
			else
			{
				addEffectToBody(attacker, fightSkill->getSkillBin()->releaseEffect, animDirection, fightSkill->getSkillBin()->releaseEffectLayer == 0);
			}
		}
	}
}
