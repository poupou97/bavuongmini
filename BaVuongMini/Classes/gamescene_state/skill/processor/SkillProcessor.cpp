#include "SkillProcessor.h"

#include "../../role/BaseFighter.h"
#include "../../GameSceneState.h"
#include "../SkillSeed.h"
#include "../CSkillResult.h"
#include "../../../utils/GameUtils.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../GameView.h"
#include "../../role/BaseFighterOwnedCommand.h"
#include "../../role/BaseFighterOwnedStates.h"
#include "../../role/MyPlayer.h"
#include "../../role/GameActorAnimation.h"
#include "../../role/SimpleActor.h"
#include "../../exstatus/ExStatusType.h"
#include "../../role/BaseFighterConstant.h"
#include "../../role/Monster.h"
#include "../../../messageclient/element/CMonsterBaseInfo.h"
	
SkillProcessor::SkillProcessor()
: m_bIsRelease(false)
, m_bIsHide(false)
, skillSeed(NULL)
, mSkillResult(NULL)
, m_bSendAttackReq(false)
, m_effectScaleX(1.0f)
, m_effectScaleY(1.0f)
{
	//apEffects = __Array::create();
}

SkillProcessor::~SkillProcessor()
{
	CC_SAFE_DELETE(skillSeed);
	CC_SAFE_DELETE(mSkillResult);
}

void SkillProcessor::init()
{
	auto bf = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(this->getSkillSeed()->attacker));
	if(bf != NULL)
	{
		auto cmd = dynamic_cast<BaseFighterCommandAttack*>(bf->getCommand());
		if(cmd != NULL) {
			setSendAttackReq(cmd->sendAttackRequest);
		}
	}
}
	
/**
* ���ż����ͷ�ʱ��Ч
*/
void SkillProcessor::playFightSound(){

}

void SkillProcessor::loadSkillEffect(const char* currEffect){

}

SkillSeed* SkillProcessor::getSkillSeed() {
	return skillSeed;
}
void SkillProcessor::setSkillSeed(SkillSeed* skillSeed) {
	this->skillSeed = skillSeed;
}

CSkillResult* SkillProcessor::getSkillResult() {
	return this->mSkillResult;
}
void SkillProcessor::setSkillResult(CSkillResult* skillResult) {
	this->mSkillResult = skillResult;
}
	
cocos2d::Vector<Node*> SkillProcessor::getApEffects() {
	return apEffects;
}

void SkillProcessor::setApEffects(cocos2d::Vector<Node*>  apEffects) {
	this->apEffects = apEffects;
}
	
GameSceneLayer* SkillProcessor::getScene() {
	return scene;
}

void SkillProcessor::setScene(GameSceneLayer* scene) {
	this->scene = scene;
}

float SkillProcessor::getPosX() {
	return m_x;
}
void SkillProcessor::setPosX(float mX) {
	m_x = mX;
}
float SkillProcessor::getPosY() {
	return m_y;
}
void SkillProcessor::setPosY(float mY) {
	m_y = mY;
}

void SkillProcessor::setEffectScaleX(float sx)
{
	m_effectScaleX = sx;
}
void SkillProcessor::setEffectScaleY(float sy)
{
	m_effectScaleY = sy;
}
	
bool SkillProcessor::isHide(){
	return m_bIsHide;
}
	
void SkillProcessor::setHide(bool isHide) {
	this->m_bIsHide = isHide;
}
	
bool SkillProcessor::isRelease() {
	return m_bIsRelease;
}

void SkillProcessor::setRelease(bool isRelease) {
	this->m_bIsRelease = isRelease;
}
	
/**
	* ����Լ�����Э����������ͳ���
	* @param attacker
	*/
void SkillProcessor::skillDamage(BaseFighter* attacker){
	sendSkillCommand(attacker);
	countDamage(attacker);
}
	
void SkillProcessor::sendSkillCommand(BaseFighter* attacker) {
	//if (attacker == null 
	//		|| LocalManager.getInstance().isLocal()
	//		|| !getScene().isOwn(attacker.roleId)) {
	//	return;
	//}
	//// ��Ϸ�������ҹ������������Լ�
	//sendAttackReq(attacker);
}

/**
	* ������ʹ�� ���͹������ܵ��������������˺�
	*/
void SkillProcessor::countDamage(BaseFighter* attacker) {
	//if (attacker == null
	//		|| (!LocalManager.getInstance().isLocal() && !getScene().isOwn(attacker.roleId))) {
	//	return;
	//}
	//// ��Ϸ������ | ��Ϸ����ʱ�����������Լ�
	//doTrick(attacker);
}
	
void SkillProcessor::hideOutScreen(){
	//// ������Ļ������
	//float tx = (getScene().getSceneX() + getM_x());
	//float ty = (getScene().getSceneY() + getM_y());
	//if (tx < 0 || tx > getScene().viewWidth || ty < 0
	//		|| ty > getScene().viewHeight) {
	//	setHide(true);
	//}
}

void SkillProcessor::onSkillBegan(BaseFighter* attacker)
{
	// nothing, subclass will implement it
}
void SkillProcessor::onSkillReleased(BaseFighter* attacker)
{
	// nothing, subclass will implement it
}
void SkillProcessor::onSkillApplied(BaseFighter* attacker)
{
	this->setRelease(true);
}

void SkillProcessor::update(float dt)
{
	// nothing
}

void SkillProcessor::updateResume(BaseFighter* attacker){
	// check the cooldown time
	bool bIsCooldownOver = true;

	// todo
	//if(GameView::getInstance()->isOwn(getSkillSeed()->attacker))
	//{
	//	GameFightSkill* mfs = *(getSkillSeed()->mfSkill);
	//	if(getSkillSeed()->mfSkill == NULL){
	//		setRelease(true);
	//		return;
	//	}

	//	// �õ����ܵ�CDʱ��
	//	int coolTime = mfs->getCoolTime();
	//	// ��ԭ
	//	if (mfs->getCoolTimeCount() != -1
	//		&& GameUtils::millisecondNow() - mfs->getCoolTimeCount() > coolTime) {
	//		//mfs.resume();
	//		mfs->setCoolTimeCount(-1);
	//		bIsCooldownOver = true;
	//	}
	//	bIsCooldownOver = false;
	//}

	// �ͷ�&����
	if(isHide() && bIsCooldownOver){
		setRelease(true);
	}
}

void SkillProcessor::sendAttackReq(BaseFighter* attacker){
	// only MyPlayer can send attack message to game server
	if(GameView::getInstance()->isOwn(getSkillSeed()->attacker))
	{
		if(m_bSendAttackReq)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1201, skillSeed);
			MyPlayer* me = (MyPlayer*)attacker;
			me->startCommonCD();
		}
	}
}

bool SkillProcessor::knockback(BaseFighter* attacker, float backDuration, float hitRecoverTime)
{
	CSkillResult* result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");

	// no defender, ignore
	if(result->defenders_size() <= 0)
	{
		this->setRelease(true);   // release the skill processor immediatelly
		return false;
	}

	//CCAssert(result->defenders_size() == 1, "if not 1, maybe error");

	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);
		auto defender = dynamic_cast<BaseFighter*>(attacker->getGameScene()->getActor(def.target()));
		if(defender == NULL)
		{
			this->setRelease(true);   // release the skill processor immediatelly
			return false;
		}
		//defender->setWorldPosition(Vec2(def.targetx(), def.targety()));
		//defender->setPosition(attacker->getGameScene()->convertToCocos2DSpace(defender->getWorldPosition()));
		Vec2 targetWorldPosition = Vec2(def.targetx(), def.targety());
		Vec2 target = attacker->getGameScene()->convertToCocos2DSpace(targetWorldPosition);
		auto  action = Sequence::create(
			MoveTo::create(backDuration, target),
			DelayTime::create(hitRecoverTime),
			NULL);
		defender->runAction(action);

		Vec2 offset = GameUtils::getDirection(targetWorldPosition, attacker->getWorldPosition());
		int animDirection = defender->getAnimDirection(Vec2(0, 0), Vec2(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);
		//CCAssert(animDirection != -1, "invalid anim dir");
		defender->setAnimDir(animDirection);

		defender->changeAction(ACT_BE_KNOCKBACK);
	}

	return true;
}

bool SkillProcessor::knockback(BaseFighter* attacker, BaseFighter* defender, float knockDistance, float backDuration, float hitRecoverTime)
{
	if(defender == NULL)
	{
		return false;
	}

	if(defender->isAction(ACT_DIE))
		return false;

	if(!defender->isAction(ACT_STAND)  
		|| defender->hasExStatus(ExStatusType::immobilize)   // ������
		|| defender->hasExStatus(ExStatusType::vertigo)   // ѣ��״̬
		|| (defender->getAnim()->getActionByTag(BASEFIGHTER_GROW_ACTION) != NULL)
		//|| isBoss(defender)
		|| defender->isMyPlayerGroup()
		|| defender->getType() == GameActor::type_player)   // ��ҽ�ɫ���ᱻ"�ٻ���"
	{
		return false;
	}

	// the defender is too far from his real postion, so ignore
	if(defender->getWorldPosition().getDistance(defender->getRealWorldPosition()) >= 128)
		return false;

	// calc the knockback offset
	Vec2 dir = GameUtils::getDirection(attacker->getWorldPosition(), defender->getWorldPosition());
	// check if the target is barrier, if true, try to modify the target to reachable point
	float distance = knockDistance;   // 64 * 2.5f
	Vec2 offset = dir*distance;
	Vec2 targetWorldPos = defender->getWorldPosition()+offset;
	auto scene = attacker->getGameScene();
	int tileX = scene->positionToTileX(targetWorldPos.x);
	int tileY = scene->positionToTileY(targetWorldPos.y);
	if(scene->isLimitOnGround(tileX, tileY))
	{
		// return directly, the modification will be implemented in the future
		return false;
	}
	offset.y /= 2;
	offset.y = -offset.y;

	Vec2 targetPos = scene->convertToCocos2DSpace(targetWorldPos);
	auto  action = Sequence::create(
		MoveTo::create(backDuration, targetPos),
		//MoveBy::create(backDuration, offset),
		DelayTime::create(hitRecoverTime),
		NULL);
	defender->runAction(action);

	Vec2 cocos2dPosition = attacker->getGameScene()->convertToCocos2DSpace(attacker->getWorldPosition());
	Vec2 cocos2dTargetPos = cocos2dPosition+offset;
	targetWorldPos = attacker->getGameScene()->convertToGameWorldSpace(cocos2dTargetPos);   // get the world position
	offset = GameUtils::getDirection(targetWorldPos, attacker->getWorldPosition());
	int animDirection = defender->getAnimDirection(Vec2(0, 0), Vec2(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);
	//CCAssert(animDirection != -1, "invalid anim dir");
	defender->setAnimDir(animDirection);

	// ActionContext should be set before changeAction()
	ActionBeKnockedBackContext context;
	context.beKnockedBackDuration = backDuration + hitRecoverTime;
	context.bReturnBack = true;
	context.startTime = GameUtils::millisecondNow();
	context.setContext(defender);

	defender->changeAction(ACT_BE_KNOCKBACK);

	return true;
}

bool SkillProcessor::hitback(BaseFighter* attacker, BaseFighter* defender, float knockDistance, float backDuration)
{
	if(defender == NULL)
	{
		return false;
	}

	if(defender->isAction(ACT_DIE))
		return false;

	if(defender->hasExStatus(ExStatusType::immobilize)   // ������
		|| defender->hasExStatus(ExStatusType::vertigo)   // ѣ��״̬
		|| (defender->getAnim()->getActionByTag(BASEFIGHTER_GROW_ACTION) != NULL)
		|| (defender->getActionByTag(BASEFIGHTER_FLYUP_ACTION) != NULL)
		//|| isBoss(defender)
		|| defender->isMyPlayerGroup()
		|| defender->getType() == GameActor::type_player)   // ��ҽ�ɫ���ᱻ"�ٻ���"
	{
		return false;
	}

	// the defender is too far from his real postion, so ignore
	if(defender->getWorldPosition().getDistance(defender->getRealWorldPosition()) >= 192)
		return false;

	// calc the knockback offset
	Vec2 dir = GameUtils::getDirection(attacker->getWorldPosition(), defender->getWorldPosition());
	// check if the target is barrier, if true, try to modify the target to reachable point
	float distance = knockDistance;   // 64 * 2.5f
	Vec2 offset = dir*distance;
	Vec2 targetWorldPos = defender->getWorldPosition()+offset;
	auto scene = attacker->getGameScene();
	int tileX = scene->positionToTileX(targetWorldPos.x);
	int tileY = scene->positionToTileY(targetWorldPos.y);
	if(scene->isLimitOnGround(tileX, tileY))
	{
		// return directly, the modification will be implemented in the future
		return false;
	}
	offset.y /= 2;
	offset.y = -offset.y;

	const float idle_duration = 0.5f;
	if(defender->getAnimDir() == 1 || defender->getAnimDir() == 3)   // face to left or right
	{
		const float target_angle = 25.f;
		bool flyingToRight = true;
		Vec2 attackWorldPosition = attacker->getWorldPosition();
		// adjust the animation's direction and rotation by attacker and this monster's position
		if(attackWorldPosition.x >=  defender->getWorldPosition().x)
			flyingToRight = false;
		else
			flyingToRight = true;
		float targetAngle = flyingToRight ? target_angle : -target_angle ;
		int animDir = flyingToRight ? 1 : 3;
		defender->setAnimDir(animDir);
		if(defender->getAnim()->getRotation() != 0)
			targetAngle = 0;

		auto action1 = Sequence::create(
				RotateBy::create(backDuration, targetAngle),
				DelayTime::create(0.2f),
				//RotateBy::create(0.0001f, -targetAngle),
				RotateTo::create(0.0001f, 0),
				DelayTime::create(backDuration + idle_duration - 0.2f),
				NULL);
		defender->getAnim()->runAction(action1);
	}

	Vec2 targetPos = scene->convertToCocos2DSpace(targetWorldPos);
	auto  action = Sequence::create(
		MoveTo::create(backDuration, targetPos),
		//MoveBy::create(backDuration, offset),
		DelayTime::create(idle_duration),
		NULL);
	action->setTag(BASEFIGHTER_HITBACK_ACTION);
	defender->runAction(action);

	Vec2 cocos2dPosition = attacker->getGameScene()->convertToCocos2DSpace(attacker->getWorldPosition());
	Vec2 cocos2dTargetPos = cocos2dPosition+offset;
	targetWorldPos = attacker->getGameScene()->convertToGameWorldSpace(cocos2dTargetPos);   // get the world position
	offset = GameUtils::getDirection(targetWorldPos, attacker->getWorldPosition());
	int animDirection = defender->getAnimDirection(Vec2(0, 0), Vec2(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);
	//CCAssert(animDirection != -1, "invalid anim dir");
	defender->setAnimDir(animDirection);

	return true;
}

void SkillProcessor::quake(BaseFighter* actor, float duration)
{
	const float one_jump_time = 0.13f;
	int times = duration / one_jump_time;
	// jump effect
	if(!actor->isMyPlayerGroup() /**&& !actor->isDead()**/)
	{
		auto action = JumpBy::create(duration, Vec2::ZERO, 25, times);
		action->setTag(BASEFIGHTER_QUAKE_ACTION);
		actor->getAnim()->runAction(action);
	}
}

void SkillProcessor::shake(BaseFighter* actor, bool shakeLeftRight, int shakeTimes, float range)
{
	if(actor == NULL)
		return;

	// �����ܻ��ζ�action
	int dir = actor->getAnimDir();
	Vec2 shakePoint1, shakePoint2;
	if(shakeLeftRight)
	{
		if(dir == ANIM_DIR_UP || dir == ANIM_DIR_DOWN)
		{
			shakePoint1.setPoint(range, 0);
			shakePoint2.setPoint(-range, 0);
		}
		else
		{
			shakePoint1.setPoint(0, range);
			shakePoint2.setPoint(0, -range);
		}
	}
	else
	{
		if(dir == ANIM_DIR_UP || dir == ANIM_DIR_DOWN)
		{
			shakePoint1.setPoint(0, range);
			shakePoint2.setPoint(0, -range);
		}
		else
		{
			shakePoint1.setPoint(range, 0);
			shakePoint2.setPoint(-range, 0);
		}
	}

	const float shakeDuration = 0.06f;
	auto action1 = Repeat::create(
		Sequence::create(
		MoveBy::create(shakeDuration, shakePoint1),
		MoveBy::create(shakeDuration, shakePoint2),
		NULL), shakeTimes);
	const float extraTime = 0.15f;
	auto action2 = Sequence::create(
			CCTintTo::create(0.001f, 255, 96, 96),
			DelayTime::create((shakeDuration+shakeDuration)*shakeTimes + extraTime),
			CCTintTo::create(0.001f, 255, 255, 255),
			NULL);

	auto action = Spawn::create(action1, action2, NULL);
	action->setTag(BASEFIGHTER_SHAKE_ACTION);
	actor->getAnim()->runAction(action);
}

void SkillProcessor::flyup(BaseFighter* attacker, BaseFighter* defender)
{
	auto scene = attacker->getGameScene();
	auto target = defender;
	if(target != NULL)
	{
		if(isBoss(target))
			return;

		const float jumpDuration = 0.5f;
		const float jumpHeight = BASEFIGHTER_ROLE_HEIGHT * 2.5f;
		const float delayTime = 0.15f;

		// jump action
		auto action = Sequence::create(
			DelayTime::create(delayTime),
			JumpBy::create(jumpDuration, Vec2::ZERO, jumpHeight, 1),
			NULL);
		target->getAnim()->runAction(action);

		bool flyingToRight = true;
		Vec2 attackWorldPosition = attacker->getWorldPosition();
		// adjust the animation's direction and rotation by attacker and this monster's position
		if(attackWorldPosition.x >=  defender->getWorldPosition().x)
			flyingToRight = false;
		else
			flyingToRight = true;
		float targetAngle = flyingToRight ? 90.f : -90.f ;
		int animDir = flyingToRight ? 1 : 3;
		defender->setAnimDir(animDir);

		auto rotate_action = Sequence::create(
			DelayTime::create(delayTime),
			RotateBy::create(jumpDuration*4/5, targetAngle),
			DelayTime::create(jumpDuration*1/5),
			DelayTime::create(0.25f),
			//RotateBy::create(0.0001f, -targetAngle),
			RotateTo::create(0.0001f, 0),
			NULL);
		target->getAnim()->runAction(rotate_action);

		// get flying offset
		Vec2 dir = GameUtils::getDirection(attacker->getWorldPosition(), target->getWorldPosition());
		// check if the target is barrier, if true, try to modify the target to reachable point
		float distance = 64;
		if(target->getType() == GameActor::type_player)
			distance = 0;
		Vec2 offset = dir*distance;
		Vec2 targetPos = target->getWorldPosition()+offset;
		int tileX = scene->positionToTileX(targetPos.x);
		int tileY = scene->positionToTileY(targetPos.y);
		if(scene->isLimitOnGround(tileX, tileY))
		{
			offset.setPoint(0,0);
		}
		offset.y /= 2;
		offset.y = -offset.y;

		// move back action
		auto action2 = Sequence::create(
			DelayTime::create(delayTime),
			MoveBy::create(jumpDuration, offset),
			ScaleTo::create(0.06f, 1.0f, 0.8f),
			ScaleTo::create(0.1f, 1.0f, 1.0f),
			DelayTime::create(0.2f),
			NULL);
		action2->setTag(BASEFIGHTER_FLYUP_ACTION);
		target->runAction(action2);
	}
}

void SkillProcessor::showWeaponEffect(BaseFighter* actor, const char* effectFileName, float stayTime)
{
	auto pPlayer = dynamic_cast<BasePlayer*>(actor);
	if(pPlayer != NULL)
	{
		std::string wholeEffectFileName = "animation/weapon/weaponeffect/";
		wholeEffectFileName.append(effectFileName);

		auto cmd = dynamic_cast<BaseFighterCommandAttack*>(pPlayer->getCommand());
		if(cmd != NULL && cmd->attackCount == 0)
		{
			// "animation/weapon/weaponeffect/WeaponEffectFire_blue.plist"
			//if(!pPlayer->hasWeaponEffect(effectFileName))
			{
				pPlayer->removeAllWeaponEffects();
				pPlayer->addWeaponEffect(wholeEffectFileName.c_str());
				pPlayer->refreshWeaponEffect(stayTime);
			}
		}
	}
}

void SkillProcessor::showGroundEffect(const char* effectName, GameSceneLayer* scene, float firstDelayTime, float secondDelayTime, float fadeOutTime)
{
	// show ground effect
	auto simpleActor = new SimpleActor();
	simpleActor->setLayerId(0);   // ground layer
	simpleActor->setGameScene(scene);
	simpleActor->setWorldPosition(Vec2(this->getPosX(), this->getPosY()));
	simpleActor->loadAnim(effectName, true);
	simpleActor->setVisible(false);

	auto  action = Sequence::create(
		DelayTime::create(firstDelayTime),   // the ground effect start time in the "effectName".anm
		Show::create(),
		DelayTime::create(secondDelayTime),
		FadeOut::create(fadeOutTime),
		RemoveSelf::create(),
		NULL);
	simpleActor->runAction(action);

	auto actorLayer = scene->getActorLayer();
	actorLayer->addChild(simpleActor);
	simpleActor->release();
}

void SkillProcessor::scatter(BaseFighter* attacker)
{
	auto scene = attacker->getGameScene();

	// it is not My General
	if(attacker->getPosInGroup() <= 0)
		return;

	// if the my general is close to my group, adjust his position
	const int range = 96;   // 80
	bool isCloseToMyGroup = false;
	/*
	if(attacker->getPosInGroup() == 1)
	{
		// check other Generals
		std::vector<long long>& generals = GameView::getInstance()->myplayer->getAliveGenerals();
		for(unsigned int i = 0; i < generals.size(); i++)
		{
			BaseFighter* pActor = dynamic_cast<BaseFighter*>(scene->getActor(generals.at(i)));
			if(pActor == NULL)
				continue;
			if(pActor->getRoleId() == attacker->getRoleId())   // it's me
				continue;

			if(pActor->getWorldPosition().getDistance(attacker->getWorldPosition()) < 64)
			{
				isCloseToMyGroup = true;
				break;
			}
		}
	}
	*/
	// check the MyPlayer
	if(GameView::getInstance()->myplayer->getWorldPosition().getDistance(attacker->getWorldPosition()) < range)
	{
		isCloseToMyGroup = true;
	}

	if(isCloseToMyGroup)
	{
		// try to find the target which is not close to MyPlayer
		float random = CCRANDOM_0_1();
		float x, y;
		bool isCloseToMyPlayer = false;
		for(int i = 0; i < 1000; i++)
		{
			const float randomRange = 128;   // 96
			// find the near target
			random = CCRANDOM_0_1() - 0.5f;
			x = attacker->getRealWorldPosition().x + random * randomRange;
			//x = attacker->getWorldPosition().x + random * range;
			random = CCRANDOM_0_1() - 0.5f;
			y = attacker->getRealWorldPosition().y + random * randomRange;
			//y = attacker->getWorldPosition().y + random * range;

			// check the MyPlayer
			if(GameView::getInstance()->myplayer->getWorldPosition().getDistance(Vec2(x,y)) < range)
			{
				isCloseToMyPlayer = true;
				continue;
			}
			else
			{
				isCloseToMyPlayer = false;
				break;
			}
		}

		// setup the move command
		BaseFighterCommandMove* cmd = new BaseFighterCommandMove();
		cmd->targetPosition = Vec2(x, y);
		attacker->setNextCommand(cmd, false);
		attacker->setMoveSpeed(200.f);
	}
}

void SkillProcessor::assemble(BaseFighter* attacker)
{
	// it is not My General
	if(attacker->getPosInGroup() <= 0)
		return;

	// if the my general is too far to MyPlayer, adjust his position
	const int long_range = 64 * 4;
	bool isFarToMyPlayer = false;
	// check the MyPlayer
	if(GameView::getInstance()->myplayer->getWorldPosition().getDistance(attacker->getWorldPosition()) >= long_range)
	{
		isFarToMyPlayer = true;
	}

	// back to his REAL position
	if(isFarToMyPlayer)
	{
		// setup the move command
		BaseFighterCommandMove* cmd = new BaseFighterCommandMove();
		cmd->targetPosition = attacker->getRealWorldPosition();
		attacker->setNextCommand(cmd, false);
		attacker->setMoveSpeed(BASEFIGHTER_BASIC_MOVESPEED);
	}
}

void SkillProcessor::returnHome(BaseFighter* attacker)
{
}

void SkillProcessor::_addEffectToGround(GameSceneLayer* scene, float x, float y, std::string& effectName)
{
	auto simpleActor = new SimpleActor();
	simpleActor->setGameScene(scene);
	const int OFFSET = 2;
	simpleActor->setWorldPosition(Vec2(x, y + OFFSET));
	simpleActor->loadAnim(effectName.c_str());
	simpleActor->setScale(m_effectScaleX, m_effectScaleY);
	auto actorLayer = scene->getActorLayer();
	actorLayer->addChild(simpleActor, SCENE_ROLE_LAYER_BASE_ZORDER);
	simpleActor->release();

	setPosX(simpleActor->getWorldPosition().x);
	setPosY(simpleActor->getWorldPosition().y);
}

void SkillProcessor::addEffectToGround(BaseFighter* actor, std::string& effectName)
{
	auto scene = actor->getGameScene();

	_addEffectToGround(scene, actor->getWorldPosition().x, actor->getWorldPosition().y, effectName);
}

void SkillProcessor::addEffectToBody(BaseFighter* actor, std::string& effectName, int animDirection, bool bFoot)
{
	CCLegendAnimation* pAnim = CCLegendAnimation::create(effectName, animDirection);
	pAnim->setScale(m_effectScaleX, m_effectScaleY);
	actor->addEffect(pAnim, bFoot, 0);

	setPosX(actor->getWorldPosition().x);
	setPosY(actor->getWorldPosition().y);
}

void SkillProcessor::addMagicHaloToGround(GameSceneLayer* scene, Vec2 worldPosition)
{
	const float TOTAL_DURATION = 3.0f;
	const float STAY_DURATION = 1.5f;
	Sprite* pSprite = Sprite::create("animation/texiao/renwutexiao/magic_halo.png");
	auto rotate_action = Spawn::create(
		RotateBy::create(TOTAL_DURATION, 60),
		//FadeOut::create(DURATION),
		NULL);
	pSprite->runAction(rotate_action);

	auto pNode = Node::create();
	pNode->setCascadeOpacityEnabled(true);
	pNode->setScaleX(3.0f);
	pNode->setScaleY(1.5f);
	Vec2 pos = scene->convertToCocos2DSpace(worldPosition);
	pNode->setPosition(pos);
	pNode->addChild(pSprite);
	auto node_action = Sequence::create(
		DelayTime::create(STAY_DURATION),
		FadeOut::create(TOTAL_DURATION - STAY_DURATION),
		RemoveSelf::create(),
		NULL);
	pNode->runAction(node_action);

	scene->getActorLayer()->addChild(pNode, SCENE_ROLE_LAYER_BASE_ZORDER);
}

void SkillProcessor::addMagicHaloToGround(GameSceneLayer* scene, Vec2 worldPosition, std::string str_spritePath, 
	float total_duration, float stay_duration, float scale_x, float scale_y)
{
	const float TOTAL_DURATION = total_duration;
	const float STAY_DURATION = stay_duration;
	Sprite* pSprite = Sprite::create(str_spritePath.c_str());
	auto rotate_action = Spawn::create(
		RotateBy::create(TOTAL_DURATION, 90),
		//FadeOut::create(DURATION),
		NULL);
	pSprite->runAction(rotate_action);

	auto pNode = Node::create();
	pNode->setCascadeOpacityEnabled(true);
	pNode->setScaleX(scale_x);
	pNode->setScaleY(scale_y);
	Vec2 pos = scene->convertToCocos2DSpace(worldPosition);
	pNode->setPosition(pos);
	pNode->addChild(pSprite);
	auto node_action = Sequence::create(
		DelayTime::create(STAY_DURATION),
		FadeOut::create(TOTAL_DURATION - STAY_DURATION),
		RemoveSelf::create(),
		NULL);
	pNode->runAction(node_action);

	scene->getActorLayer()->addChild(pNode, SCENE_ROLE_LAYER_BASE_ZORDER);
}

void SkillProcessor::addWaveHaloToGround(GameSceneLayer* scene, Vec2 worldPosition, std::string spritePathName, float interval, int times, float maxScale)
{
	for(int i = 0; i < times; i++)
	{
		Node* pNode = NULL;
		int indexPng = spritePathName.find(".png");
		int indexAnm = spritePathName.find(".anm");
		if(indexPng >= 0)
		{
			pNode = Sprite::create(spritePathName.c_str());
		}
		else if(indexAnm >= 0)
		{
			auto simpleActor = new SimpleActor();
			simpleActor->setLayerId(0);   // ground layer
			simpleActor->setGameScene(scene);
			simpleActor->setWorldPosition(worldPosition);
			simpleActor->loadAnim(spritePathName.c_str(), true);
			simpleActor->setVisible(false);
			simpleActor->autorelease();

			pNode = simpleActor;
		}
		else
		{
			CCAssert(false, "wrong parameter");
		}
		pNode->setVisible(false);

		auto spawnAction = Spawn::create(
			ScaleTo::create(1.0f, maxScale),
			FadeOut::create(1.0f),
			NULL);
		auto action = Sequence::create(
			DelayTime::create(i * interval),
			Show::create(),
			spawnAction,
			RemoveSelf::create(),
			NULL);
		pNode->runAction(action);

		scene->getActorLayer()->addChild(pNode, SCENE_ROLE_LAYER_BASE_ZORDER);
	}
}

void SkillProcessor::addBreathingHaloToGround(GameSceneLayer* scene, Vec2 worldPosition, const char*effectPathName)
{
	// ground effect
	auto pGroundAnim = CCLegendAnimation::create(effectPathName);
	pGroundAnim->setScale(2.0f, 1.0f);
	pGroundAnim->setPlayLoop(true);
	pGroundAnim->setVisible(false);
	pGroundAnim->setOpacity(26);
	pGroundAnim->setPosition(scene->convertToCocos2DSpace(worldPosition));
	auto repeapAction = Repeat::create(Sequence::create(
		FadeTo::create(0.35f,110),
		FadeTo::create(0.35f,160),
		FadeTo::create(0.5f,218),
		FadeTo::create(0.35f,160),
		FadeTo::create(0.35f,110),
		//DelayTime::create(0.1f),
		NULL), 2);
	pGroundAnim->runAction(Sequence::create(
		DelayTime::create(0.7f),
		Show::create(),
		FadeTo::create(0.5f, 110),
		//DelayTime::create(2.5f),
		repeapAction,
		FadeTo::create(0.8f, 1),
		RemoveSelf::create(),
		NULL));
	scene->getActorLayer()->addChild(pGroundAnim, SCENE_GROUND_LAYER_BASE_ZORDER);
}

bool SkillProcessor::isBoss(BaseFighter* actor)
{
	auto pBoss = dynamic_cast<Monster*>(actor);
	if(pBoss != NULL)
	{
		if(pBoss->getClazz() == CMonsterBaseInfo::clazz_world_boss)
			return true;
		else if(pBoss->getClazz() == CMonsterBaseInfo::clazz_boss)
			return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////

//
// CCPlayLegendAnimation
//

CCPlayLegendAnimation* CCPlayLegendAnimation::create() 
{
    auto pRet = new CCPlayLegendAnimation();

    if (pRet) {
        pRet->autorelease();
    }

    return pRet;
}

void CCPlayLegendAnimation::update(float time) {
    CC_UNUSED_PARAM(time);
    //m_pTarget->setVisible(true);
	auto pTargetAnim = dynamic_cast<CCLegendAnimation*>(_target);
	if(pTargetAnim != NULL)
	{
		pTargetAnim->play();
	}
}
