#ifndef _SKILL_TARGETSCOPETYPE_H_
#define _SKILL_TARGETSCOPETYPE_H_

//
//  TargetScopeType.h
//
//  Created by zhao gang.
//  Copyright (c) 2013 Perfect Future. All rights reserved.
//

typedef enum  
{
	TargetScopeType_none = 0,	
	TargetScopeType_direct_selection = 1,		// 锁定单个目标
	TargetScopeType_target_scope_tile,      // 目标范围地块
	TargetScopeType_own_around_tile,        // 自身周围地块
	TargetScopeType_own_front_tile,         // 自身前方选取
	TargetScopeType_fighter_self            // 自身
} TargetScopeType;

#endif