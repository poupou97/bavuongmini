#ifndef _SKILL_SKILLMANAGER_H_
#define _SKILL_SKILLMANAGER_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;

/**
 * ���ܹ����ߣ����ݴ���ļ��ܲ���SkillSeed��CSkillResult��������Ӧ�ļ������̴���
 * @author zhaogang
 */
class SkillSeed;
class CSkillResult;
class SkillProcessor;

class SkillManager{
public:
	static SkillProcessor* createSkillProcessor(const char* processorId);

public:
	SkillManager();
	virtual ~SkillManager();
	
	/**
	 * ���һ����������
	 * @param skillSeed
	 */
	SkillProcessor* addSkillProcessor(SkillSeed* skillSeed);
		/**
	 * ���һ����������
	 * @param skillSeed
	 */
	SkillProcessor* addSkillProcessor(CSkillResult* result);
	
	/**
	 * ���������߼�
	 */
	void update(float dt);
	
	std::vector<SkillProcessor*>* getSkillProcessorList();
	void setSkillProcessorList(std::vector<SkillProcessor*>* skillProcessorList);

private:
	/**
	 * ��Ҫ���»��Ƶļ�������
	 */
	std::vector<SkillProcessor*>* skillProcessorList;
};

#endif