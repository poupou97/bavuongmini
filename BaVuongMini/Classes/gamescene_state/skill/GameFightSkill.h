#ifndef _SKILL_GAMEFIGHTSKILL_H_
#define _SKILL_GAMEFIGHTSKILL_H_

//
//  GameFightSkill.h
//
//  Created by zhao gang
//  Copyright (c) 2013 Perfect Future. All rights reserved.
//

#define FightSkill_SKILL_ID_PUTONGGONGJI			"PuTongGongJi"
#define FightSkill_SKILL_ID_WARRIORDEFAULTATTACK    "WarriorDefaultAttack"
#define FightSkill_SKILL_ID_MAGICDEFAULTATTACK      "MagicDefaultAttack"
#define FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK    "WarlockDefaultAttack"
#define FightSkill_SKILL_ID_RANGERDEFAULTATTACK     "RangerDefaultAttack"
/******************meng jiang******************/
#define FightSkill_SKILL_ID_AX1PFCZ                "AX1pfcz"
#define FightSkill_SKILL_ID_AX2YJDQ                "AX2yjdq"
#define FightSkill_SKILL_ID_AX3YRPX                "AX3yrpx"
#define FightSkill_SKILL_ID_AZ1LTNH                "AZ1ltnh"
#define FightSkill_SKILL_ID_AZ2YFDG                "AZ2yfdg"
#define FightSkill_SKILL_ID_AZ3XJSB                "AZ3xjsb"
#define FightSkill_SKILL_ID_AY1HXCQ                "AY1hxcq"
#define FightSkill_SKILL_ID_AY2LPHJ                "AY2lphj"
#define FightSkill_SKILL_ID_AY3YMPC                "AY3ympc"
/******************gui mou******************/
#define FightSkill_SKILL_ID_BX1CQZM                "BX1cqzm"
#define FightSkill_SKILL_ID_BX2FDSQ                "BX2fdsq"
#define FightSkill_SKILL_ID_BX3SSZL                "BX3sszl"
#define FightSkill_SKILL_ID_BZ1SSB                 "BZ1ssb"
#define FightSkill_SKILL_ID_BZ2WQCY                "BZ2wqcy"
#define FightSkill_SKILL_ID_BZ3YGZQ                "BZ2ygzq"
#define FightSkill_SKILL_ID_BY1TQMS                "BY1tqms"
#define FightSkill_SKILL_ID_BY2GWJX                "BY2gwjx"
#define FightSkill_SKILL_ID_BY3SMZZ                "BY3smzz"
/******************hao jie******************/
#define FightSkill_SKILL_ID_CX1YCWW                "CX1ycww"
#define FightSkill_SKILL_ID_CX2SHY                 "CX2shy"
#define FightSkill_SKILL_ID_CX3KCJ                 "CX3kcj"
#define FightSkill_SKILL_ID_CZ1MSHC                "CZ1mshc"
#define FightSkill_SKILL_ID_CZ2WXKJ                "CZ2wxkj"
#define FightSkill_SKILL_ID_CZ3FSJ                 "CZ3fsj"
#define FightSkill_SKILL_ID_CY1TDWJ                "CY1tdwj"
#define FightSkill_SKILL_ID_CY2DBZ                 "CY2dbz"
#define FightSkill_SKILL_ID_CY3FYJT                "CY3fyjt"
/******************shen she******************/
#define FightSkill_SKILL_ID_DX1LYS                 "DX1lys"
#define FightSkill_SKILL_ID_DX2QLWY                "DX2qlwy"
#define FightSkill_SKILL_ID_DX3BBCY                "DX3bbcy"
#define FightSkill_SKILL_ID_DZ1TMS                 "DZ1tms"
#define FightSkill_SKILL_ID_DZ2ZLJ                 "DZ2zlj"
#define FightSkill_SKILL_ID_DZ3HFS                 "DZ3hfs"
#define FightSkill_SKILL_ID_DY1SRPZ                "DY1srpz"
#define FightSkill_SKILL_ID_DY2TCDK                "DY2tcdk"
#define FightSkill_SKILL_ID_DY3HYHS                "DY3hyhs"


#define FightSkill_SKILL_ID_YUANCHENGGONGJI			"YuanChengGongJi"
#define FightSkill_SKILL_ID_CY1 					"CY1"
#define FightSkill_SKILL_ID_AZ1 					"AZ1"
#define FightSkill_SKILL_ID_BX1						"BX1"
#define FightSkill_SKILL_ID_KANGJUHUOHUAN 			"KangJuHuoHuan"
#define FightSkill_SKILL_ID_YOUHUOZHIGUANG			"YouHuoZhiGuang"
#define FightSkill_SKILL_ID_DIYUHUOYAN				"DiYuHuoYan"
#define FightSkill_SKILL_ID_LEIDIANSHU				"LeiDianShu"
#define FightSkill_SKILL_ID_SHUNJIANYIDONG			"ShunJianYiDong"
#define FightSkill_SKILL_ID_FENGHUOLUN				"FengHuoLun"
#define FightSkill_SKILL_ID_BAOLIEHUOYAN			"BaoLieHuoYan"
#define FightSkill_SKILL_ID_HUOQIANGSHU				"HuoQiangShu"
#define FightSkill_SKILL_ID_JIGUANGDIANYING			"JiGuangDianYing"
#define FightSkill_SKILL_ID_DIYULEIGUANG			"DiYuLeiGuang"
#define FightSkill_SKILL_ID_MOFADUN					"MoFaDun"
#define FightSkill_SKILL_ID_SHENGYANSHU				"ShengYanShu"
#define FightSkill_SKILL_ID_BINGPAOXIAO				"BingPaoXiao"
#define FightSkill_SKILL_ID_CHUJIJIANFA				"ChuJiJianFa"
#define FightSkill_SKILL_ID_GONGSHAJIANFA			"GongShaJianFa"
#define FightSkill_SKILL_ID_HUSHENZHENQI			"HuShenZhenQi"
#define FightSkill_SKILL_ID_CISHAJIANSHU			"CiShaJianShu"
#define FightSkill_SKILL_ID_BAOYUEDAO				"BaoYueDao"
#define FightSkill_SKILL_ID_YEMANCHONGZHUANG		"YeManChongZhuang"
#define FightSkill_SKILL_ID_LIEHUOJIANFA			"LieHuoJianFa"
#define FightSkill_SKILL_ID_ZHILIAOSHU				"ZhiLiaoShu"
#define FightSkill_SKILL_ID_JINGSHENZHANFA			"JingShenZhanFa"
#define FightSkill_SKILL_ID_SHIDUSHU				"ShiDuShu"
#define FightSkill_SKILL_ID_LINGHUNDAOFU			"LingHunDaoFu"
#define FightSkill_SKILL_ID_KULOUZHAOHUANSHU		"KuLouZhaoHuanShu"
#define FightSkill_SKILL_ID_YINSHENSHU				"YinShenShu"
#define FightSkill_SKILL_ID_JITIYINSHENSHU			"JiTiYinShenShu"
#define FightSkill_SKILL_ID_YOULINGDUN				"YouLingDun"
#define FightSkill_SKILL_ID_SHENSHENGZHANJIASHU		"ShenShengZhanJiaShu"
#define FightSkill_SKILL_ID_SHIZIHOU				"ShiZiHou"
#define FightSkill_SKILL_ID_KUNMOZHOU				"KunMoZhou"
#define FightSkill_SKILL_ID_QUNTIZHILIAOSHU			"QunTiZhiLiaoShu"
#define FightSkill_SKILL_ID_ZHAOHUANSHENSHOU		"ZhaoHuanShenShou"
#define FightSkill_SKILL_ID_HONGTIANLEI				"HongTianLei"
#define FightSkill_SKILL_ID_QIANGLIHONGTIANLEI		"QiangLiHongTianLei"

#define FIGHTSKILL_ID_MUSOU_EXTRA "MusouSkill1"

#define FIGHTSKILL_ID_WARLOCKFLASH "WarlockFlash"

#define FIGHTSKILL_DISTANCE_UNIT 32   // pixel, 1 yard in the skill excel is same as 32 pixel

#include "cocos2d.h"
#include "cocos-ext.h"

#include "../../messageclient/element/CFightSkill.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "TargetScopeType.h"

/**
 * 技能释放过程的配置信息，主要用于客户端控制技能动作和特效
 * @author zhaogang
 */
class SkillBin {
public:
	enum EffectDirection {
		effect_no_direction = 0,
		effect_has_direction = 1,
	};

	enum EffectLayer {
		effect_layer_onfoot = 0,
		effect_layer_onbody = 1,
	};

	enum EffectPosition {
		effect_pos_to_target = 0,
		effect_pos_to_self = 1,
	};

	enum EffectToGround {
		effect_with_target = 0,
		effect_to_ground = 1,
	};

public:
	/**技能id */
	std::string ID;

	// 攻击动作，动作1 or 2, etc.
	char action;

	/** when to release skill effect, 
	 *  0, release at the beginning of the attack animation 
	 *  100, max value, release at the end of the attack animation 
	 *  1 ~ 99, release at the middle of the attack animation
     */
	short releasePhase;

	// 伤害效果
	char damageEffect;

	// 技能刚开始释放时的特效
	std::string beginEffect;
	// 技能动作完成后，释放出来的特效
	std::string releaseEffect;
	/** 
	 * 释放出来的特效是否带有方向性
	 * 0, 无方向，则无视
	 * 1, 有方向，则需要根据攻击者和被攻击者之间的朝向，决定动画方向
	 */
	int releaseEffectDir;
	/**
	 * 技能特效的显示层级，是在脚下，还是身上
	 * 0, 脚下
	 * 1, 身上
	 */
	int releaseEffectLayer;
	/**
	 * 技能特效的显示位置，是在施法者身上，还是目标身上
	 * 0, 目标身上
	 * 1, 施法者身上
	 */
	int releaseEffectPos;
	/**
	 * 技能特效的显示位置，是在在地上，还是在相关角色身上
	 * 0, 特效跟随角色 ( 一般是跟随角色 )
	 * 1, 丢到地上
	 */
	int releaseEffectToGround;

	// 飞行道具的特效，用于远程攻击类技能
	std::string flyingItemEffect;
	// 飞行道具命中后，播放的打击特效
	std::string flyingEndedEffect;

	bool canDamage;   // 技能能否造成伤害
	bool allow_twiceattack;   // 是否允许展示二次攻击效果
	bool canKnockBack;   // 是否允许展示短距离击退敌人的效果
	bool canShakeScreen;   // 是否允许技能有震屏效果

	int attack_time;   // 攻击次数，该技能会快速的使用几次(attack_time)攻击动作
	float attack_time_modifier;   // default is 1.0f

	std::string sound;
};

/**
 * 技能实例
 * 只关心 skill id, level, 以及shortcut index，
 * 其他技能数据由 getCFightSkill(), getCBaseSkill(), getSkillBin()来提供
 * author: zhaogang
 */
class GameFightSkill
{
public:
	static bool isLockSingle(char targetScopeType);
	static bool isLockTile(char targetScopeType);
	static bool isLockOwn(char targetScopeType);
	static bool isAroundOwn(char targetScopeType);
	static bool isFrontOwn(char targetScopeType);

	/**
	  * 确定技能是否不需要锁定目标
      */
	static bool isNoneLock(char targetScopeType);

public:
	GameFightSkill();
	virtual ~GameFightSkill();

	inline void setId(std::string id) { ID = id; };
	inline std::string getId() { return ID; };

	void setLevel(int level) { this->level = level; };
	int getLevel() { return level; };

	void setCFightSkill(CFightSkill* skill);
	CFightSkill* getCFightSkill();
	CBaseSkill* getCBaseSkill();
	SkillBin* getSkillBin();

	/**
	   * 技能冷却时间相关
	   */
	void updateCD();
	void startCD();
	bool isInCD();
	int getCoolTime();
	// 获取技能的剩余cd时间
	int getRemainCD();

	inline void setShortcut(int index) { shortcuts = index; };
	inline char getShortcutIndex() { return shortcuts; };

	char getTargetScopeType();

	/**
	  * get the attack distanc for this skill
	  * @return, the distance unit is pixel 
	  */
	int getDistance();

	GameFightSkill* initSkill(std::string skillId, std::string skillProcessorId, int level, int ownerType);
	GameFightSkill* CopyFromFightSkill(CFightSkill *fs);

private:
	/**技能id */
	std::string ID;
	/**skill processor id，客户端处理技能类id*/
	std::string skillProcessorId;

	/**技能等级 */
	char level;

	char shortcuts;// 在技能栏中的位置，-1为没在技能栏

	CFightSkill* mFightSkill;   // do not delete this pointer, it is a reference pointer

    int coolTime;// 技能冷却时间
	long long coolTimeCount;   // 技能启动时的系统时间(ms)
	bool m_bIsInCD;
};

#endif