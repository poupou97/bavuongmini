#ifndef _SKILL_SERVERFIGHTCONSTANTS_H_
#define _SKILL_SERVERFIGHTCONSTANTS_H_

//
//  ServerFightConstants.h
//
//  Copyright (c) 2013 Perfect Future. All rights reserved.
//

/**
 * 技能应用模式
 */
typedef enum {
    /**主动释放 */
    SkillUseModelType_active_release = 0,
    /**主动开启 */
    SkillUseModelType_active_open,
    /** 被动 */
    SkillUseModelType_passive,
    
} SkillUseModelType;

/**
 * 宠物类型
 */
typedef enum {
    // BOSS型宠物，只能召唤一个
    PetType_boss = 1,
    // 士兵型宠物，可召唤多个
    PetType_soldier_fascinated = 2
    
} PetType;

/**
 * 召唤兽模式
 */
typedef enum {
    PetMode_attack = 1,
    PetMode_follow,
    PetMode_rest
    
} PetMode;


/**
 * 移动方式
 */
typedef enum {
    // 瞬移
    MoveMode_flash = 0,
    // 正常移动 
    MoveMode_step,
    // 野蛮冲撞
    MoveMode_collide_rude,
    //狮子吼
    MoveMode_collide_roaring,
    //抗拒火环
    MoveMode_collide_resist,
    
} MoveMode;


/**
 * 角色异常状态
 */
typedef enum {
    // 瞬移
    PoisonType_non = 0,
    // 正常移动 
    PoisonType_grey,
    // 野蛮冲撞
    PoisonType_yellow,
    //狮子吼
    PoisonType_all,
    
} PoisonType;

extern const char* PoisonType_greyProps[];
extern const char* PoisonType_yellowProps[];

#endif