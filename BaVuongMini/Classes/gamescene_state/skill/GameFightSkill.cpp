
#include "GameFightSkill.h"
#include "ServerFightConstants.h"
#include "TargetScopeType.h"
#include "../../utils/StaticDataManager.h"
#include "../role/GameActor.h"
#include "../../utils/GameUtils.h"

GameFightSkill::GameFightSkill()
: mFightSkill(NULL)
, m_bIsInCD(false)
, coolTimeCount(0)
{
}

GameFightSkill::~GameFightSkill()
{
}

void GameFightSkill::startCD()
{
	coolTimeCount = GameUtils::millisecondNow();
	m_bIsInCD = true;
	coolTime = getCFightSkill()->intervaltime();
}
void GameFightSkill::updateCD()
{
	if(m_bIsInCD)
	{
		if(GameUtils::millisecondNow() - coolTimeCount > coolTime)
		{
			m_bIsInCD = false;
		}
	}
}
bool GameFightSkill::isInCD()
{
	return m_bIsInCD;
}

int GameFightSkill::getCoolTime()
{
	return coolTime;
}

int GameFightSkill::getRemainCD()
{
	if(m_bIsInCD)
	{
		int _elapseTime = GameUtils::millisecondNow() - coolTimeCount;
		int remainTime = coolTime - _elapseTime;
		if(remainTime < 0)
			return 0;

		return remainTime;
	}
	else
	{
		return 0;
	}
}

/**
 * 是否锁定单体目标释放
 * @param lockMode
 */
bool GameFightSkill::isLockSingle(char targetScopeType){
    return targetScopeType == TargetScopeType_direct_selection;
}

/**
 * 是否拖动释放，锁定地块
 * @param lockMode
 * @return
 */
bool GameFightSkill::isLockTile(char targetScopeType){
    return targetScopeType == TargetScopeType_target_scope_tile;
}

/**
 * 是否不用选择就能释放
 * @param lockMode
 * @return
 */
bool GameFightSkill::isNoneLock(char targetScopeType){
    return GameFightSkill::isLockOwn(targetScopeType) 
		|| GameFightSkill::isAroundOwn(targetScopeType)
		|| GameFightSkill::isFrontOwn(targetScopeType)
		|| targetScopeType == TargetScopeType_none;
}

/**
 * 是否在自己身上释放
 * @param lockMode
 * @return
 */
bool GameFightSkill::isLockOwn(char targetScopeType){
    return targetScopeType == TargetScopeType_fighter_self;
}

/**
 * 是否影响周围一圈
 * @param lockMode
 * @return
 */
bool GameFightSkill::isAroundOwn(char targetScopeType){
    return targetScopeType == TargetScopeType_own_around_tile;
}

/**
 * 是否影响前方
 * @param lockMode
 * @return
 */
bool GameFightSkill::isFrontOwn(char targetScopeType){
    return targetScopeType == TargetScopeType_own_front_tile;
}
void GameFightSkill::setCFightSkill(CFightSkill* skill)
{
	mFightSkill = skill;
}
CFightSkill* GameFightSkill::getCFightSkill()
{
	return mFightSkill;
}
CBaseSkill* GameFightSkill::getCBaseSkill()
{
	return StaticDataBaseSkill::s_baseSkillData[this->ID];
}
SkillBin* GameFightSkill::getSkillBin()
{
	CCAssert(skillProcessorId != "", "should not be nil");
	return StaticDataSkillBin::s_data[this->skillProcessorId];
}

char GameFightSkill::getTargetScopeType()
{
	return this->getCFightSkill()->targetscope();
}

int GameFightSkill::getDistance()
{
	CCAssert(mFightSkill != NULL, "fightskill should not be nil");
	return mFightSkill->distance() * FIGHTSKILL_DISTANCE_UNIT;
}

GameFightSkill* GameFightSkill::initSkill(std::string skillId, std::string skillProcessorId, int level, int ownerType)
{
	this->ID = skillId;
	this->skillProcessorId = skillProcessorId;
	this->level = level;

	if(ownerType == GameActor::type_monster)
	{
		auto monsterFightSkill = StaticDataMonsterFightSkill::s_data[skillId];
		CCAssert(monsterFightSkill != NULL, "should not be nil");
		setCFightSkill(monsterFightSkill);
	}
	else
	{
		setCFightSkill(StaticDataFightSkill::s_data[skillId]->at(level));
	}

	return this;
}

GameFightSkill* GameFightSkill::CopyFromFightSkill(CFightSkill *fs)
{
	this->ID = fs->id();
	this->level = fs->level();
	return this;
}