#include "SkillManager.h"

#include "SkillSeed.h"
#include "CSkillResult.h"
#include "processor/SkillProcessor.h"
#include "../../common/CKBaseClass.h"
	
SkillManager::SkillManager (){
	skillProcessorList = new std::vector<SkillProcessor*>();
}

SkillManager::~SkillManager (){
	delete skillProcessorList;
	skillProcessorList = NULL;
}
	
SkillProcessor* SkillManager::createSkillProcessor(const char* processorId) {
	std::string skillProcessorClassName = processorId;
	skillProcessorClassName.append("SP");
	auto pVar = (CKBaseClass*)CKClassFactory::sharedClassFactory().getClassByName(skillProcessorClassName);
	auto sp = dynamic_cast<SkillProcessor*>(pVar);
	if(sp == NULL)
		CCAssert(false, "SkillProcessor has not been defined.");

	return sp;
}
	
SkillProcessor* SkillManager::addSkillProcessor(SkillSeed* skillSeed){
	auto sp = createSkillProcessor(skillSeed->skillProcessorId.c_str());
	sp->setSkillSeed(skillSeed);
	sp->init();
	skillProcessorList->push_back(sp);

	return sp;
}

SkillProcessor* SkillManager::addSkillProcessor(CSkillResult* result) {
	auto sp = createSkillProcessor(result->showeffect().c_str());
	sp->setSkillResult(result);
	skillProcessorList->push_back(sp);

	return sp;
}
	
void SkillManager::update(float dt) {
	std::vector<SkillProcessor*>::iterator iter;
	for (int i = 0; i < (int)skillProcessorList->size(); i++) {
		iter = skillProcessorList->begin() + i;
		auto sp = *iter;
		if (sp == NULL) {
			skillProcessorList->erase(iter);
			i--;
			continue;
		}
		if (sp->isRelease()) {
			// ��ǰ����ʹ���꣬�����ͷ�
			skillProcessorList->erase(iter);
			sp->release();
			CC_SAFE_DELETE(sp);
			i--;
			continue;
		}
		sp->update(dt);
	}
}

std::vector<SkillProcessor*>* SkillManager::getSkillProcessorList() {
	return skillProcessorList;
}

void SkillManager::setSkillProcessorList(std::vector<SkillProcessor*>* skillProcessorList) {
	this->skillProcessorList = skillProcessorList;
}
