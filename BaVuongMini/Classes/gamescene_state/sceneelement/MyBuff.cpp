#include "MyBuff.h"
#include "BuffCell.h"
#include "../../utils/StaticDataManager.h"
#include "../exstatus/ExStatus.h"
#include "GameView.h"
#include "../role/MyPlayer.h"
#include "BaseBuff.h"
#include "AppMacros.h"

MyBuff::MyBuff()
{
}


MyBuff::~MyBuff()
{
}


MyBuff * MyBuff::create()
{
	auto myBuff = new MyBuff();
	if (myBuff && myBuff->init())
	{
		myBuff->autorelease();
		return myBuff;
	}
	CC_SAFE_DELETE(myBuff);
	return NULL;
}

bool MyBuff::init()
{
	if (UIScene::init())
	{
		Size visibleSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();

		auto bg = ImageView::create();
		bg->loadTexture("gamescene_state/zhujiemian3/renwuduiwu/renwu_di.png");
		bg->setAnchorPoint(Vec2::ZERO);
		bg->setPosition(Vec2::ZERO);
		bg->setScale9Enabled(true);
		bg->setCapInsets(Rect(12,12,1,1));
		bg->setContentSize(Size(217,220));
		m_pLayer->addChild(bg);

		l_dialog = Label::createWithTTF(StringDataManager::getString("myBuff_noEx"), APP_FONT_NAME, 20);
		l_dialog->setAnchorPoint(Vec2(0.5f,0.5f));
		l_dialog->setPosition(Vec2(108,110));
		bg->addChild(l_dialog);
		l_dialog->setVisible(false);

		tableView = TableView::create(this,Size(217,212));
		tableView->setDirection(TableView::Direction::VERTICAL);
		tableView->setAnchorPoint(Vec2(0,0));
		tableView->setPosition(Vec2(0,4));
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		this->addChild(tableView);

		this->setIgnoreAnchorPointForPosition(false);
		this->setAnchorPoint(Vec2(0,0));
		this->setPosition(Vec2(0,0));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(217,220));
		
		return true;
	}
	return false;
}



void MyBuff::onEnter()
{
	UIScene::onEnter();
}
void MyBuff::onExit()
{
	UIScene::onExit();
}

bool MyBuff::onTouchBegan(Touch *pTouch, Event *pEvent)
{
    return this->resignFirstResponder(pTouch,this,false);
}
void MyBuff::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void MyBuff::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void MyBuff::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}


Size MyBuff::tableCellSizeForIndex(TableView *table, unsigned int idx)
{
	return Size(210,71);
}

TableViewCell* MyBuff::tableCellAtIndex(TableView *table, ssize_t  idx)
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called

	auto exStatus = GameView::getInstance()->myplayer->getExStatusVector().at(idx);
	cell = BuffCell::create(exStatus);

	return cell;

}

ssize_t MyBuff::numberOfCellsInTableView(TableView *table)
{
	if (GameView::getInstance()->myplayer->getExStatusVector().size()>0)
	{
		l_dialog->setVisible(false);
		return GameView::getInstance()->myplayer->getExStatusVector().size();
	}
	else
	{
		l_dialog->setVisible(true);
		return 0;
	}
}

void MyBuff::tableCellTouched(TableView* table, TableViewCell* cell)
{
}

void MyBuff::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void MyBuff::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}








