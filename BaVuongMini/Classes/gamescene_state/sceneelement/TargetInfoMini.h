
#ifndef _GAMESCENESTATE_TARGETINFOMINI_H_
#define _GAMESCENESTATE_TARGETINFOMINI_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"

#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

class BaseFighter;

/**
   * the blood bar on the enemy's head
   */

class TargetInfoMini : public UIScene
{
public:
	TargetInfoMini();
	~TargetInfoMini();

	static TargetInfoMini* create(BaseFighter *baseFighter);
	bool init(BaseFighter *baseFighter);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	virtual void update(float delta);

	ui::ImageView * ImageView_targetBlood;
	ui::ImageView * ImageView_targetBloodGray;

	void ReloadTargetData( BaseFighter * baseFighter, bool bInitNewTarget = false);
	
	long long baseFighterId;

private:
	float m_preScale;
	float m_nowScale;

	float m_timeDelay;
};

#endif;

