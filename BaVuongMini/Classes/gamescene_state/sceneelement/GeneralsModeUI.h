
#ifndef _GAMESCENESTATE_GENERALSMODEUI_H_
#define _GAMESCENESTATE_GENERALSMODEUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../../ui/extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class GeneralsModeUI : public UIScene
{
public:
	GeneralsModeUI(void);
	~GeneralsModeUI(void);

	static GeneralsModeUI * create(int generalMode,int autoFight);
	bool init(int generalMode,int autoFight);

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *touch, Event * pEvent);
	virtual void onTouchEnded(Touch *touch, Event * pEvent);
	virtual void onTouchCancelled(Touch *touch, Event * pEvent);
	virtual void onTouchMoved(Touch *touch, Event * pEvent);

	void DrivingEvent(Ref *pSender, Widget::TouchEventType type);
	void AssistanceEvent(Ref *pSender, Widget::TouchEventType type);
	void BeginAutoFightEvent(Ref *pSender, Widget::TouchEventType type);
	void EndAutoFightEvent(Ref *pSender, Widget::TouchEventType type);

	void applyGeneralMode( int idx,int autoFight );

private:
	//�佫ģʽ
	int m_generalMode;
	//�佫�Զ���ս
	int m_autoFight;

	Button * Button_driving;
	Button * Button_assistance;
	Button * Button_beginAutoFight;
	Button * Button_endAutoFight;
};

#endif