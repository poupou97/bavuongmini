#include "PKModeUI.h"
#include "GameView.h"
#include "../role/MyPlayer.h"
#include "AppMacros.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../utils/StaticDataManager.h"
#include "cocostudio/CCSGUIReader.h"

PKModeUI::PKModeUI()
{
}


PKModeUI::~PKModeUI()
{
}


PKModeUI * PKModeUI::create()
{
	auto mode = new PKModeUI();
	if (mode && mode->init())
	{
		mode->autorelease();
		return mode;
	}
	CC_SAFE_DELETE(mode);
	return NULL;
}

bool PKModeUI::init()
{
	if (UIScene::init())
	{
		Size visibleSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();

		auto mainPanel = (Layout*)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/pkMode_1.json");
		mainPanel->setAnchorPoint(Vec2(0.f,0.f));
		mainPanel->setPosition(Vec2(0,0));
		m_pLayer->addChild(mainPanel);

		auto button= (Button *)Helper::seekWidgetByName(mainPanel,"Button_heping");
		CCAssert(button != NULL, "should not be nil");
		button->setTouchEnabled(true);
		button->setPressedActionEnabled(true);
		button->addTouchEventListener(CC_CALLBACK_2(PKModeUI::PeaceEvent, this));

		auto l_des = (Text*)Helper::seekWidgetByName(button,"Label_des");
		l_des->setTextAreaSize(Size(135,0));

		button= (Button *)Helper::seekWidgetByName(mainPanel,"Button_shalu");
		CCAssert(button != NULL, "should not be nil");
		button->setTouchEnabled(true);
		button->setPressedActionEnabled(true);
		button->addTouchEventListener(CC_CALLBACK_2(PKModeUI::KillEvent, this));

		l_des = (Text*)Helper::seekWidgetByName(button,"Label_des");
		l_des->setTextAreaSize(Size(135,0));

		button= (Button *)Helper::seekWidgetByName(mainPanel,"Button_jiazu");
		CCAssert(button != NULL, "should not be nil");
		button->setTouchEnabled(true);
		button->setPressedActionEnabled(true);
		button->addTouchEventListener(CC_CALLBACK_2(PKModeUI::FamilyEvent, this));

		l_des = (Text*)Helper::seekWidgetByName(button,"Label_des");
		l_des->setTextAreaSize(Size(135,0));

		button= (Button *)Helper::seekWidgetByName(mainPanel,"Button_guojia");
		CCAssert(button != NULL, "should not be nil");
		button->setTouchEnabled(true);
		button->setPressedActionEnabled(true);
		button->addTouchEventListener(CC_CALLBACK_2(PKModeUI::CountryEvent, this));

		l_des = (Text*)Helper::seekWidgetByName(button,"Label_des");
		l_des->setTextAreaSize(Size(135,0));

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(mainPanel->getContentSize());

		return true;
	}
	return false;
}

void PKModeUI::onEnter()
{
	UIScene::onEnter();
}
void PKModeUI::onExit()
{
	UIScene::onExit();
}

bool PKModeUI::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return this->resignFirstResponder(pTouch,this,false);
}
void PKModeUI::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void PKModeUI::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void PKModeUI::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void PKModeUI::PeaceEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1207, (void*)0);
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void PKModeUI::KillEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1207, (void*)1);
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void PKModeUI::FamilyEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionid() == 0)
		{
			const char *dialog_family = StringDataManager::getString("PKMode_family_dialog");
			char* pkMode_dialog_family = const_cast<char*>(dialog_family);
			GameView::getInstance()->showAlertDialog(pkMode_dialog_family);
			return;
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(1207, (void*)2);
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void PKModeUI::CountryEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1207, (void*)3);
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}
