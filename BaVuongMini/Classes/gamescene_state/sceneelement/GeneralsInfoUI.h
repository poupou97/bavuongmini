#ifndef _GAMESCENESTATE_GENERALSINFOUI_H_
#define _GAMESCENESTATE_GENERALSINFOUI_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../legend_script/CCTutorialIndicator.h"


USING_NS_CC;
USING_NS_CC_EXT;

class CGeneralBaseMsg;
class CGeneralDetail;
class General;

/***********
*�����佫ͷ�
* yangjun 
* 2014.9.25
***********/
class GeneralItem : public UIScene
{
public:

	enum General_Status
	{
		status_InBattle = 0,   //��ѳ�ս
		status_InLine,           //�ڶ����У��ɳ�ս
		status_InRest ,          //��Ϣ
		status_Died               //���
	};

	GeneralItem();
	~GeneralItem();
	//create generalsItem by generalBaseMsg
	static GeneralItem* create(CGeneralBaseMsg * generalBaseMsg,int index);
	bool init(CGeneralBaseMsg * generalBaseMsg,int index);
	//create void generalsItem by index
	static GeneralItem* create(int index);
	bool init(int index);

	static GeneralItem* createClocked(int index);
	bool initClocked(int index);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent){};
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent){};
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent){};

	virtual void update(float dt);

	//�ˢ��״̬
	void RefreshSelfStatus();
	//ˢ�½��(���״̬)
	void RefreshUI();
	//ˢ�¼��ܵ���ʾ
	void RefreshSkillPresent(CGeneralDetail * generalDetail);
	//ˢ��Ѫ�
	void RefreshGeneralHP(General * general);
	void RefreshGeneralHP(CGeneralDetail * generalDetail);
	//��ʼ����Ϣ�CD
	void RunRestCD();
	void RestCoolDownCallBack(Node* node);

private:
	Layer * u_layer;
	
	//��ڳ�ͣ��ʱ�
	long long m_nBaseTimeInBattle;

public:
	Button * btn_headFrame;
	//pos
	int m_nIndex;
	//��佫id
	long long m_nGeneralId;
	//��ǰ״̬
	int m_nCurStatus;
	//�����ʱ�Ƿ���ɲ��������
	bool m_bIsRevevidCountFinish;
private:
	void HeadFrameEvent(Ref *pSender, Widget::TouchEventType type);
	void ClockedFrameEvent(Ref *pSender, Widget::TouchEventType type);
};


/***********
*���佫ͷ��UIĲ
* yangjun 
* 2014.9.25
***********/

class GeneralsInfoUI : public UIScene
{
public:
	GeneralsInfoUI();
	~GeneralsInfoUI();

	static GeneralsInfoUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void RefreshAllGeneralItem();
	void RefreshOneGeneralItem(long long generalId);
	GeneralItem * getGeneralItemByIndex(int index);

	void ResetCDByGeneralId(long long generalID);

	void RefreshSkillByGeneralDetail(CGeneralDetail * generalDetail);

	void RefreshGeneralHP(General * general);
	void RefreshGeneralHP(CGeneralDetail * generalDetail);

	//��������е���˸����Ч��ʹ���ǵĲ�������һ�
	void ResetAllFadeAction();

	//auto guide for call generals
	void setAutoGuideToCallGeneral(bool visible);
	void AutoGuideToCallGeneral(float dt);
	int curAutoGuideIndex;
private:
	Layer * u_layer;
	Layer * u_UpLayer;

public:
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	//����־����У�����Ҫǿ�ƣ�
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

	//��ѧ
	int mTutorialScriptInstanceId;
	int mTutorialIndex;
};

#endif

