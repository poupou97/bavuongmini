#include "BaseBuff.h"


BaseBuff::BaseBuff()
{
}


BaseBuff::~BaseBuff()
{
}


BaseBuff * BaseBuff::create()
{
	BaseBuff * baseBuff = new BaseBuff();
	if (baseBuff && baseBuff->init())
	{
		baseBuff->autorelease();
		return baseBuff;
	}
	CC_SAFE_DELETE(baseBuff);
	return NULL;
}

bool BaseBuff::init()
{
	if (Widget::init())
	{
		Size visibleSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();
		
		/*ImageView * ImageView_background = ImageView::create();
		ImageView_background->setTouchEnabled(true);
		ImageView_background->setScale9Enabled(true);
		ImageView_background->loadTexture("ChatUiRes/LV4_right_di.png");
		ImageView_background->setContentSize(Size(400, 80));
		ImageView_background->setAnchorPoint(Vec2::ZERO);
		ImageView_background->setPosition(Vec2(0,0));
		m_pLayer->addChild(ImageView_background);*/

		auto Button_test = ui::Button::create();
		Button_test->setTouchEnabled(true);
		Button_test->loadTextures("LV4_right_di.png", "LV4_right_di.png", "");
		Button_test->setAnchorPoint(Vec2(0,0));
		Button_test->setPosition(Vec2(0,0));
		Button_test->addTouchEventListener(CC_CALLBACK_2(BaseBuff::BuffInfo, this));
		this->addChild(Button_test);

		this->setAnchorPoint(Vec2(0,0));
		this->setPosition(Vec2(0,0));
		
		return true;
	}
	return false;
}

void BaseBuff::BuffInfo(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

