
#ifndef _GAMESCENESTATE_SHORTCUTCONFIGURE_H_
#define _GAMESCENESTATE_SHORTCUTCONFIGURE_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "ShortcutLayer.h"
#include "../../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CShortCut;

class ShortcutConfigure : public ShortcutLayer
{
public:
	ShortcutConfigure();
	~ShortcutConfigure();

	static ShortcutConfigure * create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonSkillEvent(Ref *pSender, Widget::TouchEventType type);
	void sendReqEquipSkill(int index);

	void RefreshOneShortcutConfigure(CShortCut * shortcut);

	//��ѧ
	Button * btn_close;
	Button * btn_first;
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void addCCTutorialIndicator2(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();

private:
	//��ѧ
	int mTutorialScriptInstanceId;

};
#endif;

