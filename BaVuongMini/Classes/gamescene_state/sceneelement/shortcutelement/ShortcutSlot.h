

#ifndef _GAMESCENESTATE_SHORTCUTSLOT_H_
#define _GAMESCENESTATE_SHORTCUTSLOT_H_

#include "cocos-ext.h"
#include "../../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../../messageclient/protobuf/ModelMessage.pb.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CShortCut;
class FolderInfo;

class ShortcutSlot : public UIScene 
{
public:
	ShortcutSlot();
	~ShortcutSlot();

	static ShortcutSlot* create(int index);
	bool init(int index);
	static ShortcutSlot* createPeerless();
	bool initPeerless();

public:
	enum shortcurSlotType
	{
		T_Void = 0,
		T_Skill = 1,
		T_Goods = 2,
		T_InstanceGoods = 3
	};

	enum useType
	{
		T_CanUse,
		T_CanNotUse
	};
	
	/*�±*/
	int curIndex;
	/*������*/
	int num;
	/*��*/
	Vec2 curpos;
	//data
	CShortCut * curShortCut;

	Layer * layer_icon;
	Layer * layer_num;

	//굱����Ϊ��ʱ��������˫���ܣ�
	Button * btn_bgFrame ;

	Button * btn_preelessBgFrame;
	ImageView * imageView_prellIcon;
	ImageView * imageView_time;

	useType slotUseType;
	shortcurSlotType slotType;
	std::string slotId;
	std::string slotIcon;
	ControlButton* CBSkill;
	Sprite * bgFrame;
	//cocos2d::extension::Scale9Sprite* backgroundButton;
	Sprite* progress_skill;
	ProgressTimer * mProgressTimer_skill;
	Label * label_goodsNum;

	ActionInterval* action_progress_from_to;
	CallFunc* action_callback;

	//��Ʒλ�����
	int curFolderIndex;

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void BeganToRunCD();
	void RefreshCD();

	void PreelessEvent(Ref *pSender, Widget::TouchEventType type);

	void skillCoolDownCallBack(Node* node);

	virtual void setScale(float scale);
private:
	float m_fNormalScale;

	void touchDownAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent);
	void touchDragInsideAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent);
	void touchDragOutsideAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent);
	void touchDragEnterAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent);
	void touchDragExitAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent);
	void touchUpInsideAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent);
	void touchUpOutsideAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent);
	void touchCancelAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent);

	void initType(int _typeid);

	void useSkill();
	void useGoods();

	void DoNothing(Ref *pSender);

	int getCurFonderId();

public: 
	//����ü�����ϸ��Ϣ
	void setInfo(CShortCut *shortCut);
	void setUseType(int index,bool canUse);
	//������˫������ϸ��Ϣ
	//void setPeerlessInfo(CShortCut *shortCut);

	//������˫����ŭ����ʾ
	void setPeerlessAngerEffect(float percentage);

	//ˢ��ʣ�����
	void refreshRetainNum(FolderInfo * folderInfo);

	//��ȡ��ǰCD��ȱ仯
	float getCurProgress();

private:
	void useMusouSkill();
};
#endif;

