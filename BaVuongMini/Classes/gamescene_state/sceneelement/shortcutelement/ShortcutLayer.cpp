#include "ShortcutLayer.h"
#include "ShortcutSlot.h"
#include "ShortcutUIConstant.h"
#include "../../../messageclient/element/CShortCut.h"
#include "../../../messageclient/element/FolderInfo.h"
#include "GameView.h"
#include "../../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../GameSceneState.h"
#include "../../../ui/skillscene/SkillScene.h"
#include "../../MainScene.h"
#include "../..//skill/GameFightSkill.h"
#include "../../exstatus/ExStatus.h"
#include "../../role/MyPlayer.h"
#include "../../exstatus/ExStatusType.h"
#include "../../../legend_script/UITutorialIndicator.h"
#include "../../../legend_script/Script.h"
#include "../../../legend_script/ScriptManager.h"
#include "../../../legend_script/CCTutorialIndicator.h"
#include "../../../gamescene_state/sceneelement/HeadMenu.h"
#include "../../../legend_script/CCTutorialParticle.h"
#include "../../../utils/StaticDataManager.h"
#include "../TriangleButton.h"
#include "../../../legend_script/CCTeachingGuide.h"

ShortcutLayer::ShortcutLayer():
shortcutSlotNum(5)
{
	Size winSize = Director::getInstance()->getWinSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//                  (4)
	//                (1)
	//           (2)  
	//        (3)     (0)
	const int EXPERIENCE_BAR_HEIGHT = 10;   // ������ĸ߶
	const int OFFSET_X = -10; 
	const int OFFSET_Y = 10;
	slotPos[0].x = winSize.width - 37 + origin.x + OFFSET_X;
	slotPos[0].y = 37 + origin.y + OFFSET_Y + EXPERIENCE_BAR_HEIGHT;
	slotPos[1].x = winSize.width - 32 + origin.x + OFFSET_X;
	slotPos[1].y = 119 + origin.y + OFFSET_Y + EXPERIENCE_BAR_HEIGHT;   // 114
	slotPos[2].x = winSize.width - 99 + origin.x + OFFSET_X;   // 94
	slotPos[2].y = 99 + origin.y + OFFSET_Y + EXPERIENCE_BAR_HEIGHT;   // 94
	slotPos[3].x = winSize.width - 119 + origin.x + OFFSET_X;   // 114
	slotPos[3].y = 32 + origin.y + OFFSET_Y + EXPERIENCE_BAR_HEIGHT;
	slotPos[4].x = winSize.width - 32 + origin.x + OFFSET_X;
	slotPos[4].y = 197 + origin.y + OFFSET_Y + EXPERIENCE_BAR_HEIGHT;   // 187
	

	for (int i = 0;i<shortcutSlotNum;++i)
	{
		shortcurTag[i] = i;
	}
}


ShortcutLayer::~ShortcutLayer()
{
}

ShortcutLayer* ShortcutLayer::create()
{
	auto shortcutLayer = new ShortcutLayer();
	if (shortcutLayer && shortcutLayer->init())
	{
		shortcutLayer->autorelease();
		return shortcutLayer;
	}
	CC_SAFE_DELETE(shortcutLayer);
	return NULL;
}

bool ShortcutLayer::init()
{
	if (UIScene::init())
	{
		auto winSize = Director::getInstance()->getVisibleSize();

		u_layer = Layer::create();
		addChild(u_layer);

		u_teachLayer = Layer::create();
		addChild(u_teachLayer);
		u_teachLayer->setLocalZOrder(100);

		int musouSkillOpenLevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[4];
		for (int i = 0; i<shortcutSlotNum; ++i)
		{
			if (i == shortcutSlotNum-1)
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->level() >= musouSkillOpenLevel)
				{
					auto shortcutSlot = ShortcutSlot::createPeerless();
					shortcutSlot->setAnchorPoint(Vec2::ZERO);
					shortcutSlot->setPosition(slotPos[i]);
					shortcutSlot->setTag(shortcurTag[i]+100);
					shortcutSlot->setLocalZOrder(10-i);
					shortcutSlot->setScale(1.1f);
					addChild(shortcutSlot);
				}
				else
				{
					auto btn_peerless = Button::create();
					btn_peerless->loadTextures("gamescene_state/zhujiemian3/jinengqu/wushuang_off.png","gamescene_state/zhujiemian3/jinengqu/wushuang_off.png","");
					btn_peerless->setTouchEnabled(true);
					btn_peerless->setAnchorPoint(Vec2::ZERO);
					btn_peerless->setPosition(Vec2(slotPos[i].x-btn_peerless->getContentSize().width/2,slotPos[i].y-btn_peerless->getContentSize().height/2+8));
					btn_peerless->setName("btn_peerless");
					btn_peerless->addTouchEventListener(CC_CALLBACK_2(ShortcutLayer::PeerlessEvent, this));
					u_layer->addChild(btn_peerless);
				}
			}
			else
			{
				auto shortcutSlot = ShortcutSlot::create(i);
				shortcutSlot->setAnchorPoint(Vec2::ZERO);
				shortcutSlot->setPosition(slotPos[i]);
				shortcutSlot->setTag(shortcurTag[i]+100);
				shortcutSlot->setLocalZOrder(10-i);
				int index = shortcurTag[i];
				if (DEFAULTATTACKSKILL_INDEX == index)
				{
					shortcutSlot->setScale(SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL);
				}
				else
				{
					shortcutSlot->setScale(SCALE_SHORTCUTSLOT);
				}
				
				addChild(shortcutSlot);
				shortcutSlot->setVisible(false);

				int openlevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[i];
				if (GameView::getInstance()->myplayer->getActiveRole()->level()>=openlevel)
				{
					shortcutSlot->setVisible(true);
					shortcutSlot->setPosition(slotPos[i]);
				}
			}
		}

		//Ƚ�ɫ���������ߺ�Ĭ�Ͽ���������ͨ�����
		if (GameView::getInstance()->myplayer->getActiveRole()->level() == 1)
		{
			ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(100));
			if(temp != NULL)
			{
				std::string defaultAtkSkill = "";
				if(GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_MJ_NAME)
				{
					defaultAtkSkill = "WarriorDefaultAttack";
				}
				else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_GM_NAME)
				{
					defaultAtkSkill = "MagicDefaultAttack";
				}
				else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_HJ_NAME)
				{
					defaultAtkSkill = "WarlockDefaultAttack";
				}
				else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_SS_NAME)
				{
					defaultAtkSkill = "RangerDefaultAttack";
				}

				auto tempShortCut = new CShortCut();
				tempShortCut->set_index(100);
				tempShortCut->set_storedtype(1);
				tempShortCut->set_skillpropid(defaultAtkSkill);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1122, tempShortCut);
				delete tempShortCut;
			}
		}

		for(int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
		{
			auto shortcut = new CShortCut();
			shortcut->CopyFrom(*(GameView::getInstance()->shortCutList.at(i)));
			//CCLOG("%s",shortcut->skillpropid().c_str());
			if (shortcut->complexflag() == 1)
			{
				if (GameView::getInstance()->myplayer->getActiveRole()->level() >= musouSkillOpenLevel)
				{
					GameView::getInstance()->myplayer->setMusouSkill(shortcut->skillpropid().c_str());
					//change by yangjun 2014.9.28
// 					ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(104));
// 					if(temp != NULL)
// 						temp->setPeerlessInfo(shortcut);
				}
			}
			else
			{
				if (shortcut->index() < MUSOUSKILL_INDEX)
				{
					auto temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(shortcut->index()+100));
					if(temp != NULL)
						temp->setInfo(shortcut);
				}
			}
		
			delete shortcut;
		}

		
		this->scheduleUpdate();

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winSize);
		//this->setTouchPriority(1);
		return true;
	}
	return false;
}

void ShortcutLayer::onEnter()
{
	UIScene::onEnter();
	//this->openAnim();
}
void ShortcutLayer::onExit()
{
	UIScene::onExit();
}	

bool ShortcutLayer::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	// �����Ƴ�����Ļ�����½�������ش��
// 	if (this->isVisible())
// 		return false;

	//����½���ǰ�ť
	auto triangleBtn = (TriangleButton*)GameView::getInstance()->getMainUIScene()->getChildByTag(78);
	if (triangleBtn)
	{
		if (!triangleBtn->isOpen)
			return false;
	}

	// convert the touch point to OpenGL coordinates
	Vec2 location = pTouch->getLocation();
	touchBeganPoint = pTouch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);

// 	int changeY = 0;
// 	if (GameView::getInstance()->getMainUIScene())
// 	{
// 		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
// 		if (mainScene->isHeadMenuOn)
// 		{
// 			changeY = mainScene->headMenu->getContentSize().height;
// 		}
// 		else
// 		{
// 			changeY = 0;
// 		}
// 	}
	
	if (location.y <= slotPos[3].y+27)
	{
		if (location.x > slotPos[3].x-27)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (location.y <= slotPos[2].y+27)
	{
		if (location.x > slotPos[2].x-27)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (location.y <= slotPos[1].y+27)
	{
		if (location.x > slotPos[1].x-27)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else if (location.y <= slotPos[4].y+27)
	{
		if (location.x > slotPos[4].x-27)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	else
	{
		return false;
	}
}
void ShortcutLayer::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void ShortcutLayer::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void ShortcutLayer::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void ShortcutLayer::RefreshAll()
{
	for(int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
	{
			auto fi = GameView::getInstance()->AllPacItem.at(i);
			if (fi->has_goods())
			{
				RefreshOne(fi);
			}
	}
}

void ShortcutLayer::RefreshOne(FolderInfo* folderInfo)
{
	for(int i = 0;i<8;i++)
	{
		if (i < GameView::getInstance()->shortCutList.size())
		{
			auto c_shortcut = GameView::getInstance()->shortCutList.at(i);
		 	if (c_shortcut->storedtype() == 2)    //����������Ʒ
		 	{
		 		if (folderInfo->has_goods())
		 		{
		 			if (folderInfo->goods().canshortcut())   //�����е���Ʒ����װ�
		 			{
		 				if (strcmp(folderInfo->goods().id().c_str(),c_shortcut->skillpropid().c_str()) == 0)   //�������Ʒװ�䵽�˿���
		 				{
							if (c_shortcut->index() < MUSOUSKILL_INDEX)
							{
								if (this->getChildByTag(shortcurTag[c_shortcut->index()]+100))
								{
									auto shortcutSlot = (ShortcutSlot*)this->getChildByTag(shortcurTag[c_shortcut->index()]+100);
									shortcutSlot->refreshRetainNum(folderInfo);
								}
							}
		 				}
		 			}
				}
		 	}
		}
	}
}
void ShortcutLayer::CloseEvent( Ref * pSender )
{
	auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(pSender);


/*	this->closeAnim();*/
}

void ShortcutLayer::RefreshOneShortcutSlot( CShortCut * shortcut )
{
	int index = shortcut->index();
	if (!(ShortcutSlot*)this->getChildByTag(index+100))
		return;

	auto shortcutslot = (ShortcutSlot*)this->getChildByTag(index+100);
	shortcutslot->removeFromParent();

	if (index == MUSOUSKILL_INDEX)
	{
		auto shortcutSlot = ShortcutSlot::createPeerless();
		shortcutSlot->setAnchorPoint(Vec2::ZERO);
		shortcutSlot->setPosition(slotPos[index]);
		shortcutSlot->setTag(shortcurTag[index]+100);
		shortcutSlot->setScale(SCALE_SHORTCUTSLOT_MUSOUSKILL);
		addChild(shortcutSlot);
		//change by yangjun 2014.9.28
		//shortcutSlot->setPeerlessInfo(shortcut);
	}
	else
	{
		auto shortcutSlot = ShortcutSlot::create(index);
		shortcutSlot->setAnchorPoint(Vec2::ZERO);
		shortcutSlot->setPosition(slotPos[index]);
		shortcutSlot->setTag(shortcurTag[index]+100);
		shortcutSlot->setLocalZOrder(10-index);
		int m_index = shortcurTag[index];
		if (m_index == DEFAULTATTACKSKILL_INDEX)
		{
			shortcutSlot->setScale(SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL);
		}
		else
		{
			shortcutSlot->setScale(SCALE_SHORTCUTSLOT);
		}
		addChild(shortcutSlot);
		shortcutSlot->setInfo(shortcut);
	}
}

void ShortcutLayer::RefreshMusouEffect( float percentage )
{
	auto shortcutslot = (ShortcutSlot*)this->getChildByTag(104);
	if (shortcutslot)
		shortcutslot->setPeerlessAngerEffect(percentage);
}

void ShortcutLayer::update(float dt)
{
	for (int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
	{
		auto tempshortcut = GameView::getInstance()->shortCutList.at(i);
		if (tempshortcut->complexflag() != 1 && tempshortcut->storedtype() == 1)///�ж��Ƿ����㹻��ħ��ˮ��ͷŸü��
		{
			if (tempshortcut->has_skillpropid())
			{
				if (isEnoughMagic(tempshortcut))
				{
					setSlotUseType(tempshortcut->index(),true);
				}
				else
				{
					setSlotUseType(tempshortcut->index(),false);
				}
			}
		}
		else if (tempshortcut->complexflag() != 1 && tempshortcut->storedtype() == 2)//��ж�ҩƷ�Ƿ����þ�
		{
			if (tempshortcut->has_skillpropid())
			{
				if (isEnoughGoods(tempshortcut))
				{
					setSlotUseType(tempshortcut->index(),true);
				}
				else
				{
					setSlotUseType(tempshortcut->index(),false);
				}
			}
		}
	}

	///�жϽ�ɫ�Ƿ���ѣ�,ζ����״̬
	if (this->isImmobilize())
	{
		for (int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
		{
			auto tempshortcut = GameView::getInstance()->shortCutList.at(i);
			if (tempshortcut->has_skillpropid())
			{
				setSlotUseType(i,false);
			}
		}
	}
}

void ShortcutLayer::setSlotUseType( int index,bool canUse )
{
	if (index != MUSOUSKILL_INDEX)
	{
		if(this->getChildByTag(index+100))
		{
			auto tempSlot = (ShortcutSlot *)this->getChildByTag(index+100);
			if (tempSlot->slotType != ShortcutSlot::T_Void)
			{
				tempSlot->setUseType(index,canUse);
			}
		}
	}
}

bool ShortcutLayer::isEnoughMagic( CShortCut * shortcut )
{
	//test
	if (shortcut->skillpropid() == FightSkill_SKILL_ID_PUTONGGONGJI || 
		shortcut->skillpropid() == FightSkill_SKILL_ID_WARRIORDEFAULTATTACK ||
		shortcut->skillpropid() == FightSkill_SKILL_ID_MAGICDEFAULTATTACK ||
		shortcut->skillpropid() == FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK ||
		shortcut->skillpropid() == FightSkill_SKILL_ID_RANGERDEFAULTATTACK)
	{
		return true;
	}

	if (GameView::getInstance()->GameFightSkillList.find(shortcut->skillpropid()) != GameView::getInstance()->GameFightSkillList.end())
	{
		auto skill = (GameView::getInstance()->GameFightSkillList.find(shortcut->skillpropid()))->second;
		auto activeRole = GameView::getInstance()->myplayer->getActiveRole();
		if (skill->getCFightSkill()->mp() > activeRole->mp())
		{
			return false;
		}
		else
		{
			return true;
			}
	}
	else 
	{
		return false;
	}
}

bool ShortcutLayer::isImmobilize()
{
	if(GameView::getInstance()->myplayer->hasExStatus(ExStatusType::immobilize))
	{
		return true;
	}

	return false;
}

bool ShortcutLayer::isEnoughGoods( CShortCut * shortcut )
{
	int goodsTotalNum = 0;
	if (shortcut->storedtype() == 2)
	{
		for (int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
		{
			if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
			{
				if (strcmp(shortcut->skillpropid().c_str(),GameView::getInstance()->AllPacItem.at(i)->goods().id().c_str()) == 0)
				{
					goodsTotalNum += GameView::getInstance()->AllPacItem.at(i)->quantity();
				}
			}
		}
	}

	if (goodsTotalNum > 0)
	{
		return true;
	}
	else
	{
		return false;
	}
}


void ShortcutLayer::RefreshCD( std::string skillId )
{
	for(int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
	{
		auto shortcut = new CShortCut();
		shortcut->CopyFrom(*(GameView::getInstance()->shortCutList.at(i)));
		if (shortcut->index() < MUSOUSKILL_INDEX)
		{
			if (shortcut->complexflag() != 1)
			{
				if (shortcut->has_skillpropid())
				{
					if (strcmp(shortcut->skillpropid().c_str(),skillId.c_str()) == 0)
					{
						ShortcutSlot * temp = (ShortcutSlot *)this->getChildByTag(shortcut->index()+100);
						temp->BeganToRunCD();
					};
				}
			}
		}

		delete shortcut;
	}
}


void ShortcutLayer::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void ShortcutLayer::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(Vec2(pos.x-45,pos.y+95));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	u_teachLayer->addChild(tutorialIndicator);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene->isHeadMenuOn)
	{
		mainScene->ButtonHeadEvent(NULL);
	}

	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
	CCTeachingGuide::CloseUIForTutorial();
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,70,70,true,true,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x,pos.y));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void ShortcutLayer::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(Vec2(pos.x,pos.y));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	tutorialIndicator->addHighLightFrameAction(70,70);
// 	u_teachLayer->addChild(tutorialIndicator);

	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,70,70,true,true,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x,pos.y));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void ShortcutLayer::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(u_teachLayer->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

bool SLSortByComplexOrder(GameFightSkill * gameFightSkill1 , GameFightSkill *gameFightSkill2)
{
	return (gameFightSkill1->getCBaseSkill()->get_complex_order() < gameFightSkill2->getCBaseSkill()->get_complex_order());
}

void ShortcutLayer::addNewShortcutSlot()
{
	//�����ȼ��Ὺ����±
	std::vector<int>openingVector;
	int musouSkillOpenLevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[MUSOUSKILL_INDEX];
	for (int i = 0; i<shortcutSlotNum; ++i)
	{
		if (i == shortcutSlotNum-1)
		{
			if (GameView::getInstance()->myplayer->getActiveRole()->level() == musouSkillOpenLevel)
			{
				if (u_layer->getChildByName("btn_peerless"))
				{
					u_layer->getChildByName("btn_peerless")->removeFromParent();
				}

				auto shortcutSlot = ShortcutSlot::createPeerless();
				shortcutSlot->setAnchorPoint(Vec2::ZERO);
				shortcutSlot->setPosition(slotPos[i]);
				shortcutSlot->setTag(shortcurTag[i]+100);
				shortcutSlot->setLocalZOrder(10-i);
				shortcutSlot->setScale(SCALE_SHORTCUTSLOT_MUSOUSKILL);
				addChild(shortcutSlot);
			}
		}
		else
		{
			int openlevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[i];
			if (GameView::getInstance()->myplayer->getActiveRole()->level() == openlevel)
			{
				auto shortcutSlot = (ShortcutSlot*)this->getChildByTag(shortcurTag[i]+100);
				if (shortcutSlot)
				{
					shortcutSlot->setVisible(true);
					openingVector.push_back(i);
					shortcutSlot->setPosition(slotPos[i]);
				}
			}
		}
	}
	for(int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
	{
		auto shortcut = new CShortCut();
		shortcut->CopyFrom(*(GameView::getInstance()->shortCutList.at(i)));
		//CCLOG("%s",shortcut->skillpropid().c_str());
		if (shortcut->complexflag() == 1)
		{
			if (GameView::getInstance()->myplayer->getActiveRole()->level() == musouSkillOpenLevel)
			{
				GameView::getInstance()->myplayer->setMusouSkill(shortcut->skillpropid().c_str());
				//change by yangjun 2014.9.28
// 				ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(104));
// 				if(temp != NULL)
// 					temp->setPeerlessInfo(shortcut);
			}
		}
		else
		{
			if (shortcut->index() < MUSOUSKILL_INDEX)
			{
				auto temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(shortcut->index()+100));
				if(temp != NULL)
					temp->setInfo(shortcut);
			}
		}

		delete shortcut;
	}

	//��δװ��ļ����ó��������������ȼ��������ȼ��ߵ������õ�����
	std::vector<GameFightSkill*>notEquipedList;
	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
	{
		auto tempSkill = it->second;
		if (tempSkill->getCBaseSkill()->usemodel() != 0)
			continue;

		bool isEquiped = false;
		for(int i =0;i<GameView::getInstance()->shortCutList.size();++i)
		{
			auto tempShortCut = GameView::getInstance()->shortCutList.at(i);
			if (!tempShortCut->has_skillpropid())
				continue;

			if (tempShortCut->storedtype() != 1)
				continue;

			if (strcmp(tempSkill->getId().c_str(),tempShortCut->skillpropid().c_str()) == 0)
			{
				isEquiped = true;
			}
		}

		if (isEquiped)
			continue;

		auto newGameFightSkill = new GameFightSkill();
		newGameFightSkill->initSkill(tempSkill->getId(),tempSkill->getId(),tempSkill->getLevel(),GameActor::type_player);
		notEquipedList.push_back(newGameFightSkill);
	}
	//��������ȼ�����
	sort(notEquipedList.begin(),notEquipedList.end(),SLSortByComplexOrder);

	//��δװ��ļ���װ���
	for(int i = 0;i<openingVector.size();++i)
	{
		auto shortcutSlot = dynamic_cast<ShortcutSlot*>(this->getChildByTag(shortcurTag[openingVector.at(i)]+100));
		if (shortcutSlot)
		{
			if (shortcutSlot->slotType != ShortcutSlot::T_Void)
				continue;

			if (!shortcutSlot->isVisible())
				continue;

			if(i<notEquipedList.size())
			{
				auto tempShortCut = new CShortCut();
				tempShortCut->set_index(openingVector.at(i)+100);
				tempShortCut->set_storedtype(1);
				tempShortCut->set_skillpropid(notEquipedList.at(i)->getId());
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1122, tempShortCut);
				delete tempShortCut;
			}

		}
	}
	//��ڴ���
	//chat
	std::vector<GameFightSkill *>::iterator iter_gameFightSkill;
	for (iter_gameFightSkill=notEquipedList.begin();iter_gameFightSkill !=notEquipedList.end();++iter_gameFightSkill)
	{
		delete *iter_gameFightSkill;
	}
	notEquipedList.clear();
}

void ShortcutLayer::PeerlessEvent( Ref* pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("musouskill_will_be_open"));
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
	
}

void ShortcutLayer::setMusouShortCutSlotPresent( bool isPresent )
{
	if (u_layer->getChildByName("btn_peerless"))
	{
		u_layer->getChildByName("btn_peerless")->setVisible(isPresent);
	}
	else
	{
		auto musouSlot = (ShortcutSlot*)this->getChildByTag(100+4);
		if (musouSlot)
		{
			musouSlot->setVisible(isPresent);
		}

		float sl = GameView::getInstance()->myplayer->getAngerValue()*1.0f/GameView::getInstance()->myplayer->getAngerCapacity();
		if (isPresent)
		{
			this->RefreshMusouEffect(sl);
		}
	}
}

void ShortcutLayer::createMusouShortCutSlotForce()
{
	if (u_layer->getChildByName("btn_peerless"))
	{
		u_layer->getChildByName("btn_peerless")->removeFromParent();

		auto shortcutSlot = ShortcutSlot::createPeerless();
		shortcutSlot->setAnchorPoint(Vec2::ZERO);
		shortcutSlot->setPosition(slotPos[shortcutSlotNum-1]);
		shortcutSlot->setTag(shortcurTag[shortcutSlotNum-1]+100);
		shortcutSlot->setLocalZOrder(10-(shortcutSlotNum-1));
		shortcutSlot->setScale(SCALE_SHORTCUTSLOT_MUSOUSKILL);
		addChild(shortcutSlot);

		for(int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
		{
			auto shortcut = new CShortCut();
			shortcut->CopyFrom(*(GameView::getInstance()->shortCutList.at(i)));
			//CCLOG("%s",shortcut->skillpropid().c_str());
			if (shortcut->complexflag() == 1)
			{
				GameView::getInstance()->myplayer->setMusouSkill(shortcut->skillpropid().c_str());
				//change by yangjun 2014.9.28
// 				ShortcutSlot * temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(104));
// 				if(temp != NULL)
// 					temp->setPeerlessInfo(shortcut);
			}

			delete shortcut;
		}
	}
}

void ShortcutLayer::recoverMusouShortCutSlot()
{
	if (u_layer->getChildByName("btn_peerless"))
	{
	}
	else
	{
		auto temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(104));
		if(temp != NULL)
			temp->removeFromParent();

		auto btn_peerless = Button::create();
		btn_peerless->loadTextures("gamescene_state/zhujiemian3/jinengqu/wushuang_off.png","gamescene_state/zhujiemian3/jinengqu/wushuang_off.png","");
		btn_peerless->setTouchEnabled(true);
		btn_peerless->setAnchorPoint(Vec2::ZERO);
		btn_peerless->setPosition(Vec2(slotPos[shortcutSlotNum-1].x-btn_peerless->getContentSize().width/2,slotPos[shortcutSlotNum-1].y-btn_peerless->getContentSize().height/2+8));
		btn_peerless->setName("btn_peerless");
		btn_peerless->addTouchEventListener(CC_CALLBACK_2(ShortcutLayer::PeerlessEvent, this));
		u_layer->addChild(btn_peerless);
	}
}

void ShortcutLayer::presentShortCutSlot( int num )
{
	for(int i = 1;i<(int)GameView::getInstance()->shortCutList.size();++i)
	{
		auto temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(100+i));
		if(temp != NULL)
			temp->setVisible(false);
	}

	if(num > 3)
		num = 3;

	for (int i = 0;i<num;i++)
	{
		int temp_tag = 3-i;
		auto temp = dynamic_cast<ShortcutSlot*>(this->getChildByTag(100+temp_tag));
		if(temp != NULL)
			temp->setVisible(true);
	}
}

