#include "ShortcutConfigure.h"
#include "ShortcutLayer.h"
#include "ShortcutUIConstant.h"
#include "../../../ui/extensions/UIScene.h"
#include "GameView.h"
#include "../../GameSceneState.h"
#include "../../../ui/skillscene/SkillScene.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../MainScene.h"
#include "../../../messageclient/element/CShortCut.h"
#include "../../skill/GameFightSkill.h"
#include "../../role/BasePlayer.h"
#include "../../role/MyPlayer.h"
#include "../../../ui/backpackscene/PacPageView.h"
#include "../../../messageclient/element/FolderInfo.h"
#include "../../../legend_script/UITutorialIndicator.h"
#include "../../../legend_script/Script.h"
#include "../../../legend_script/ScriptManager.h"
#include "../../../legend_script/CCTutorialIndicator.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../messageclient/element/CBaseSkill.h"
#include "../../../legend_script/CCTutorialParticle.h"
#include "AppMacros.h"

#define WIDGET_TAG_SHORTCUTSLOT_BASE 100

ShortcutConfigure::ShortcutConfigure()
{
}


ShortcutConfigure::~ShortcutConfigure()
{
}

ShortcutConfigure * ShortcutConfigure::create()
{
	auto shortcutConfigure = new ShortcutConfigure();
	if (shortcutConfigure && shortcutConfigure->init())
	{
		shortcutConfigure->autorelease();
		return shortcutConfigure;
	}
	CC_SAFE_DELETE(shortcutConfigure);
	return NULL;
}

bool ShortcutConfigure::init()
{
	if (UIScene::init())
	{
		Size screenSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();

		auto bg = ImageView::create();
		bg->loadTexture("res_ui/zhezhao80.png");
		bg->setScale9Enabled(true);
		bg->setContentSize(screenSize);
		bg->setAnchorPoint(Vec2::ZERO);
		bg->setPosition(Vec2(0,0));
		m_pLayer->addChild(bg);

		for (int i = 0; i<4; ++i)
		{
			auto button_skill = Button::create();
			button_skill->loadTextures(SKILLEMPTYPATH,SKILLEMPTYPATH,SKILLEMPTYPATH);
			button_skill->setTouchEnabled(true);
			button_skill->setAnchorPoint(Vec2(0.5f,0.5f));
			button_skill->setPosition(slotPos[i]);
			button_skill->setTag(shortcurTag[i]+WIDGET_TAG_SHORTCUTSLOT_BASE);
			button_skill->addTouchEventListener(CC_CALLBACK_2(ShortcutConfigure::ButtonSkillEvent, this));
			m_pLayer->addChild(button_skill);
			button_skill->setVisible(false);

			if(i == DEFAULTATTACKSKILL_INDEX)
			{
				button_skill->setScale(SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL);
			}
			else
			{
				button_skill->setScale(SCALE_SHORTCUTSLOT);
			}

			int openlevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[i];
			if (GameView::getInstance()->myplayer->getActiveRole()->level()>=openlevel)
			{
				button_skill->setVisible(true);
			}

			if (i == 1)
			{
				btn_first = button_skill;
			}
		}
		for(int i = 0;i<(int)GameView::getInstance()->shortCutList.size();++i)
		{
			auto shortcut = new CShortCut();
			shortcut->CopyFrom(*(GameView::getInstance()->shortCutList.at(i)));
			if (shortcut->index() < MUSOUSKILL_INDEX)
			{
				if (shortcut->complexflag() != 1)
				{
					if (shortcut->storedtype() == 1)
					{
						if (shortcut->has_skillpropid())
						{
							std::string skillIconPath = "res_ui/jineng_icon/";

							// 						if (strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
							// 							||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
							// 							||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
							// 							||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
							// 						{
							// 							skillIconPath.append("jineng_2");
							// 						}
							// 						else
							// 						{
							auto baseSkill = StaticDataBaseSkill::s_baseSkillData[shortcut->skillpropid()];
							if (baseSkill)
							{
								skillIconPath.append(baseSkill->icon());
							}
							else
							{
								skillIconPath.append("jineng_2");
							}
							//}
							skillIconPath.append(".png");

							auto tempButton = (Button *)m_pLayer->getChildByTag(shortcut->index()+WIDGET_TAG_SHORTCUTSLOT_BASE);
							auto _image = ImageView::create();
							_image->loadTexture(skillIconPath.c_str());
							_image->setAnchorPoint(Vec2(0.5f,0.5f));
							_image->setPosition(Vec2(0,0));
							tempButton->addChild(_image);
							_image->setScale(1.2f);
						}
					}
					else if (shortcut->storedtype() == 2)
					{
						if (shortcut->has_skillpropid())
						{
							std::string skillIconPath = "res_ui/props_icon/";
							skillIconPath.append(shortcut->icon());
							//skillIconPath.append("yaoshui");
							skillIconPath.append(".png");

							auto tempButton = (Button *)m_pLayer->getChildByTag(shortcut->index()+WIDGET_TAG_SHORTCUTSLOT_BASE);
							auto _image = ImageView::create();
							_image->loadTexture(skillIconPath.c_str());
							_image->setAnchorPoint(Vec2(0.5f,0.5f));
							_image->setPosition(Vec2(0,0));
							tempButton->addChild(_image);
							_image->setScale(1.0f);
						}
					}

					auto tempButton = (Button *)m_pLayer->getChildByTag(shortcut->index()+WIDGET_TAG_SHORTCUTSLOT_BASE);
					auto _hightLight = ImageView::create();
					_hightLight->loadTexture(HIGHLIGHT3);
					_hightLight->setAnchorPoint(Vec2(0.5f,0.5f));
					_hightLight->setPosition(Vec2(0,3));
					tempButton->addChild(_hightLight);
					_hightLight->setScale(1.0f);

// 					ImageView * imageView_dressUp = ImageView::create();
// 					imageView_dressUp->loadTexture("gamescene_state/zhujiemian3/jinengqu/lolo.png");
// 					imageView_dressUp->setAnchorPoint(Vec2(0.5f, 0.5f));
// 					imageView_dressUp->setPosition(Vec2(0,-23));
// 					tempButton->addChild(imageView_dressUp);
// 					if (i == DEFAULTATTACKSKILL_INDEX)
// 					{
// 						imageView_dressUp->setScale(1.0f*SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL);
// 					}
// 					else
// 					{
// 						imageView_dressUp->setScale(0.85f*SCALE_SHORTCUTSLOT);
// 					}

					if (shortcut->storedtype() == 1)
					{
						if (shortcut->has_skillpropid())
						{
							auto image_skillVariety = SkillScene::GetSkillVarirtyImageView(shortcut->skillpropid().c_str());
							if (image_skillVariety)
							{
								image_skillVariety->setAnchorPoint(Vec2(0.5f,0.5f));
								image_skillVariety->setPosition(Vec2(15,-17));
								image_skillVariety->setScale(1.15f);
								tempButton->addChild(image_skillVariety);
							}
						}
					}
				}
			}
		
			delete shortcut;
		}
		btn_close = Button::create();
		btn_close->loadTextures("res_ui/close.png","res_ui/close.png","res_ui/close.png");
		btn_close->setTouchEnabled(true);
		btn_close->setAnchorPoint(Vec2::ZERO);
		btn_close->setPosition(Vec2(screenSize.width-50,screenSize.height-50));
		btn_close->addTouchEventListener(CC_CALLBACK_2(ShortcutConfigure::CloseEvent, this));
		m_pLayer->addChild(btn_close);

		this->setContentSize(screenSize);
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}



void ShortcutConfigure::onEnter()
{
	UIScene::onEnter();
	//this->openAnim();
}
void ShortcutConfigure::onExit()
{
	UIScene::onExit();
}	

bool ShortcutConfigure::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return true;
}
void ShortcutConfigure::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void ShortcutConfigure::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void ShortcutConfigure::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void ShortcutConfigure::ButtonSkillEvent( Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		int index = ((Button*)pSender)->getTag();
		this->sendReqEquipSkill(index);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void ShortcutConfigure::CloseEvent( Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		//this->closeAnim();
		this->removeFromParent();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void ShortcutConfigure::sendReqEquipSkill(int index)
{
	auto tempShortCut = new CShortCut();
	tempShortCut->set_index(index);
	if (GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene))
	{
		auto skillScene = (SkillScene *)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
		tempShortCut->set_skillpropid(skillScene->curFightSkill->getId());
		tempShortCut->set_storedtype(1);
	}
	else if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack))
	{
		tempShortCut->set_skillpropid(GameView::getInstance()->pacPageView->curFolder->goods().id());
		tempShortCut->set_storedtype(2);
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1122, tempShortCut);
	delete tempShortCut;

	if (index-WIDGET_TAG_SHORTCUTSLOT_BASE == 1)
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(btn_first);
	}
}

void ShortcutConfigure::RefreshOneShortcutConfigure( CShortCut * shortcut )
{
	int index = shortcut->index();
	if (index >= MUSOUSKILL_INDEX)
		return;

	//delete
	auto _btn = (Button*)m_pLayer->getChildByTag(index+WIDGET_TAG_SHORTCUTSLOT_BASE);
	_btn->removeFromParent();
	
	//new 
	auto button_skill = Button::create();
	button_skill->loadTextures(SKILLEMPTYPATH,SKILLEMPTYPATH,SKILLEMPTYPATH);
	button_skill->setTouchEnabled(true);
	button_skill->setAnchorPoint(Vec2(0.5f,0.5f));
	button_skill->setPosition(slotPos[index]);
	button_skill->setTag(shortcurTag[index]+WIDGET_TAG_SHORTCUTSLOT_BASE);
	button_skill->addTouchEventListener(CC_CALLBACK_2(ShortcutConfigure::ButtonSkillEvent, this));
	m_pLayer->addChild(button_skill);

	if(index == DEFAULTATTACKSKILL_INDEX)
	{
		button_skill->setScale(SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL);
	}
	else
	{
		button_skill->setScale(SCALE_SHORTCUTSLOT);
	}

	switch((int)shortcut->storedtype())
	{
	case 0 :
		break;
	case 1 :
		if (shortcut->has_skillpropid())
		{
			std::string skillIconPath = "res_ui/jineng_icon/";

// 			if (strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_WARRIORDEFAULTATTACK) == 0
// 				||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_MAGICDEFAULTATTACK) == 0
// 				||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_RANGERDEFAULTATTACK) == 0
// 				||strcmp(shortcut->skillpropid().c_str(),FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK) == 0)
// 			{
// 				skillIconPath.append("jineng_2");
// 			}
// 			else
// 			{
				auto baseSkill = StaticDataBaseSkill::s_baseSkillData[shortcut->skillpropid()];
				if (baseSkill)
				{
					skillIconPath.append(baseSkill->icon());
				}
				else
				{
					skillIconPath.append("jineng_2");
				}
//			}
			skillIconPath.append(".png");

			auto _image = ImageView::create();
			_image->loadTexture(skillIconPath.c_str());
			_image->setAnchorPoint(Vec2(0.5f,0.5f));
			_image->setPosition(Vec2(0,0));
			button_skill->addChild(_image);
			_image->setScale(1.2f);
		}

		break;
	case 2 :
		if (shortcut->has_skillpropid())
		{
			std::string skillIconPath = "res_ui/props_icon/";
			skillIconPath.append(shortcut->icon());
			//skillIconPath.append("yaoshui");
			skillIconPath.append(".png");

			auto _image = ImageView::create();
			_image->loadTexture(skillIconPath.c_str());
			_image->setAnchorPoint(Vec2(0.5f,0.5f));
			_image->setPosition(Vec2(0,0));
			button_skill->addChild(_image);

			auto label_goodsNum = Label::createWithTTF("78",APP_FONT_NAME,14);
			label_goodsNum->setPosition(Vec2(0,0));
			auto shadowColor = Color4B::BLACK;   // black
			label_goodsNum->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
			this->addChild(label_goodsNum);
			_image->setScale(1.0f);
		}

		break;
	case 3 :
		break;
	}

	auto _hightLight = ImageView::create();
	_hightLight->loadTexture(HIGHLIGHT3);
	_hightLight->setAnchorPoint(Vec2(0.5f,0.5f));
	_hightLight->setPosition(Vec2(0,3));
	button_skill->addChild(_hightLight);
	_hightLight->setScale(1.0f);

// 	ImageView * imageView_dressUp = ImageView::create();
// 	imageView_dressUp->loadTexture("gamescene_state/zhujiemian3/jinengqu/lolo.png");
// 	imageView_dressUp->setAnchorPoint(Vec2(0.5f, 0.5f));
// 	imageView_dressUp->setPosition(Vec2(0,-23));
// 	button_skill->addChild(imageView_dressUp);
// 	if (index == DEFAULTATTACKSKILL_INDEX)
// 	{
// 		imageView_dressUp->setScale(1.0f*SCALE_SHORTCUTSLOT_DEFAULTATTACKSKILL);
// 	}
// 	else
// 	{
// 		imageView_dressUp->setScale(0.85f*SCALE_SHORTCUTSLOT);
// 	}

	if (shortcut->storedtype() == 1)
	{
		if (shortcut->has_skillpropid())
		{
			auto image_skillVariety = SkillScene::GetSkillVarirtyImageView(shortcut->skillpropid().c_str());
			if (image_skillVariety)
			{
				image_skillVariety->setAnchorPoint(Vec2(0.5f,0.5f));
				image_skillVariety->setPosition(Vec2(15,-17));
				image_skillVariety->setScale(1.15f);
				button_skill->addChild(image_skillVariety);
			}
		}
	}
}

void ShortcutConfigure::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void ShortcutConfigure::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  )
{
	auto tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,25,60);
	tutorialIndicator->setPosition(Vec2(pos.x-45,pos.y+95));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->addChild(tutorialIndicator);

// 	CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("tuowei0.plist",50,70);
// 	tutorialParticle->setPosition(Vec2(pos.x,pos.y-30));
// 	tutorialParticle->setTag(CCTUTORIALPARTICLETAG);
// 	this->addChild(tutorialParticle);
}

void ShortcutConfigure::addCCTutorialIndicator2( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  )
{
	auto tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,40,50);
	tutorialIndicator->setPosition(Vec2(pos.x-40,pos.y-40));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	this->addChild(tutorialIndicator);

// 	CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("tuowei0.plist",40,50);
// 	tutorialParticle->setPosition(Vec2(pos.x+20,pos.y-5));
// 	tutorialParticle->setTag(CCTUTORIALPARTICLETAG);
// 	this->addChild(tutorialParticle);
}

void ShortcutConfigure::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

// 	CCTutorialParticle* tutorialParticle = dynamic_cast<CCTutorialParticle*>(this->getChildByTag(CCTUTORIALPARTICLETAG));
// 	if(tutorialParticle != NULL)
// 		tutorialParticle->removeFromParent();
}
