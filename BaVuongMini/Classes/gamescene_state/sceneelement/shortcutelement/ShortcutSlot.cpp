#include "ShortcutSlot.h"
#include "ShortcutLayer.h"
#include "ShortcutUIConstant.h"
#include "../../GameSceneState.h"
#include "../../MainScene.h"
#include "../../role/MyPlayer.h"
#include "../../role/MyPlayerSimpleAI.h"
#include "GameView.h"
#include "../../role/MyPlayerOwnedCommand.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../ui/backpackscene/PacPageView.h"
#include "../../../messageclient/element/CShortCut.h"
#include "../../skill/GameFightSkill.h"
#include "../../../messageclient/element/FolderInfo.h"
#include "../../exstatus/ExStatusType.h"
#include "../../../utils/StaticDataManager.h"
#include "../../../legend_script/Script.h"
#include "../../../legend_script/ScriptManager.h"
#include "../../../messageclient/element/CBaseSkill.h"
#include "../../../gamescene_state/GameSceneEffects.h"
#include "../../skill/GameFightSkill.h"
#include "../../../messageclient/element/CDrug.h"
#include "AppMacros.h"
#include "../../../ui/backpackscene/GeneralsListForTakeDrug.h"
#include "../../../ui/backpackscene/PackageScene.h"
#include "../../../messageclient/element/GoodsInfo.h"
#include "../../../newcomerstory/NewCommerStoryManager.h"
#include "../../../ui/skillscene/SkillScene.h"
#include "../../../utils/GameUtils.h"
#include "../../../GameAudio.h"

#define ParticleEffect_Musou_Tag 258
#define kTag_CBSkill 555
#define kTag_HightLight 556
#define kTag_ProgressTimer_skill 557
#define kTag_adorn 558
#define kTag_skillVariety 559
#define kTag_FireAnm 560

ShortcutSlot::ShortcutSlot():
slotType(T_Void),
slotUseType(T_CanUse),
curFolderIndex(0.0f),
num(-1),
m_fNormalScale(1.0f)
{
}


ShortcutSlot::~ShortcutSlot()
{
	CC_SAFE_DELETE(curShortCut);
}


ShortcutSlot* ShortcutSlot::create(int index)
{
	auto shortcutSlot = new ShortcutSlot();
	if (shortcutSlot && shortcutSlot->init(index))
	{
		shortcutSlot->autorelease();
		return shortcutSlot;
	}
	CC_SAFE_DELETE(shortcutSlot);
	return NULL;
}

ShortcutSlot* ShortcutSlot::createPeerless()
{
	auto shortcutSlot = new ShortcutSlot();
	if (shortcutSlot && shortcutSlot->initPeerless())
	{
		shortcutSlot->autorelease();
		return shortcutSlot;
	}
	CC_SAFE_DELETE(shortcutSlot);
	return NULL;
}
bool ShortcutSlot::init(int index)
{
	if (UIScene::init())
	{
		curIndex = index;
		// Add the button
		/*************************SKILL****************************/  
		Rect fullRect = Rect(0,0, 0, 0);
		Rect insetRect = Rect(0,0,0,0);

		layer_icon =  Layer::create();
		layer_num =  Layer::create();
		addChild(layer_icon);
		addChild(layer_num);

		btn_bgFrame = Button::create();
		btn_bgFrame->loadTextures(SKILLEMPTYPATH,SKILLEMPTYPATH,"");
		btn_bgFrame->setTouchEnabled(true);
		btn_bgFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
		btn_bgFrame->setPosition(Vec2(0,0));
		layer_icon->addChild(btn_bgFrame);

		auto imageView_hightLight = ImageView::create();
		imageView_hightLight->loadTexture(HIGHLIGHT3);
		imageView_hightLight->setAnchorPoint(Vec2(0.5f, 0.5f));
		imageView_hightLight->setPosition(Vec2(0,3));
		imageView_hightLight->setName("gaoguang");
		btn_bgFrame->addChild(imageView_hightLight);

// 		ImageView * imageView_dressUp = ImageView::create();
// 		imageView_dressUp->loadTexture("gamescene_state/zhujiemian3/jinengqu/lolo.png");
// 		imageView_dressUp->setAnchorPoint(Vec2(0.5f, 0.5f));
// 		imageView_dressUp->setPosition(Vec2(0,0));
// 		imageView_dressUp->setName("imageView_dressUp");
// 		btn_bgFrame->addChild(imageView_dressUp);

		curShortCut = new CShortCut();

// 		if (index == 0)
// 		{
// 			btn_bgFrame->setScale(1.0f);
// 		}
// 		else
// 		{
// 			btn_bgFrame->setScale(0.85f);
// 		}

		this->setContentSize(Size(btn_bgFrame->getContentSize().width*btn_bgFrame->getScale(),btn_bgFrame->getContentSize().height*btn_bgFrame->getScale()));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}


bool ShortcutSlot::initPeerless()
{
	if (UIScene::init())
	{
		//�����Ч
		auto particleEffect_musou = ParticleSystemQuad::create("animation/texiao/particledesigner/longhundi.plist");
		particleEffect_musou->setPositionType(ParticleSystem::PositionType::GROUPED);
		particleEffect_musou->setPosition(Vec2(0,0));
		particleEffect_musou->setScale(0.8f);
		particleEffect_musou->setTag(ParticleEffect_Musou_Tag);
		addChild(particleEffect_musou);
		particleEffect_musou->setVisible(false);
		// Add the button
		/*************************PeerlessSkill****************************/  
		Rect fullRect = Rect(0,0, 0, 0);
		Rect insetRect = Rect(0,0,0,0);

		layer_icon =  Layer::create();
		layer_num =  Layer::create();
		addChild(layer_icon);
		addChild(layer_num);

		btn_preelessBgFrame = Button::create();
		btn_preelessBgFrame->loadTextures(MUSOUSKILL_DI,MUSOUSKILL_DI,"");
		btn_preelessBgFrame->setTouchEnabled(true);
		btn_preelessBgFrame->setAnchorPoint(Vec2(0.5f, 0.5f));
		btn_preelessBgFrame->setPosition(Vec2(0,0));
		btn_preelessBgFrame->addTouchEventListener(CC_CALLBACK_2(ShortcutSlot::PreelessEvent, this));
		layer_icon->addChild(btn_preelessBgFrame);

		imageView_time = ImageView::create();
		imageView_time->loadTexture(MUSOUSKILL_TIME);
		imageView_time->setAnchorPoint(Vec2(0,0));
		imageView_time->setPosition(Vec2(-29,-29));
		imageView_time->setTextureRect(Rect(0,58,0,29));
		layer_icon->addChild(imageView_time);

		imageView_prellIcon = ImageView::create();
		imageView_prellIcon->loadTexture("gamescene_state/zhujiemian3/jinengqu/huoqiuchendi.png");
		imageView_prellIcon->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_prellIcon->setPosition(Vec2(0,0));
		layer_icon->addChild(imageView_prellIcon);

		auto lam_fire = CCLegendAnimation::create("animation/texiao/changjingtexiao/longhunhuoqiu1/longhunhuoqiu1.anm");
		if (lam_fire)
		{
			lam_fire->setPlayLoop(true);
			lam_fire->setReleaseWhenStop(true);
			lam_fire->setPosition(Vec2(0,-2));
			lam_fire->setScale(0.5f);
			lam_fire->setTag(kTag_FireAnm);
			layer_icon->addChild(lam_fire);
		}

		auto imageView_bg = ImageView::create();
		imageView_bg->loadTexture(MUSOUSKILL);
		imageView_bg->setAnchorPoint(Vec2(0.5,0.5));
		imageView_bg->setPosition(Vec2(0,8));
		layer_icon->addChild(imageView_bg);

		//�߹
// 		ImageView * highLight = ImageView::create();
// 		highLight->loadTexture(HIGHLIGHT1);
// 		highLight->setAnchorPoint(Vec2(0.5f, 0.5f));
// 		highLight->setPosition(Vec2(0,1));
// 		m_pLayer->addChild(highLight);

		//float per = (GameView::getInstance()->myplayer->getAngerValue()*1.0f)/(GameView::getInstance()->myplayer->getAngerCapacity()*1.0f);
		//setPeerlessAngerEffect(per);

		curShortCut = new CShortCut();
		this->setContentSize(Size(64,64));
		////this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		return true;
	}
	return false;
}



void ShortcutSlot::touchDownAction(Ref *senderz, cocos2d::extension::Control::EventType controlEvent)
{
	auto btn = (ControlButton*)senderz;
	curIndex = curShortCut->index();

	if (slotType == T_Void)
	{

	}
	else if (slotType == T_Skill)
	{
		if (this->slotUseType == T_CanNotUse)
		{
// 			const char *str1 = StringDataManager::getString("skillSlot_canNotUse");
// 			char* p1 =const_cast<char*>(str1);
// 			GameView::getInstance()->showAlertDialog(p1);
			auto shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			if (shortcutLayer)
			{
				if (shortcutLayer->isImmobilize())
				{
					GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillSlot_isImmobilize"));
				}
				else if (!shortcutLayer->isEnoughMagic(curShortCut))
				{
					GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillSlot_magicNotEnough"));
				}
			}
	
		}
		else if (this->slotUseType == T_CanUse)
		{
			if (mProgressTimer_skill->isVisible())
			{
				//GameView::getInstance()->showAlertDialog("can not do it");
			}
			else
			{
// 				switch(slotType)
// 				{
// 				case T_Void :
// 					break;
// 				case T_Skill :
// 					{
						useSkill();
// 					}
// 					break;
// 				case T_Goods :
// 					{
// 						useGoods();
// 					}
// 					break;
// 				case T_InstanceGoods :
// 					break;
// 				}

				
			}
		}
		else{}
	}
	else if (slotType == T_Goods)
	{
		auto shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
		if (shortcutLayer)
		{
			if (!shortcutLayer->isEnoughGoods(curShortCut))
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillSlot_goodsNotEnough"));
			}
			else
			{
				useGoods();
			}
		}
	}
}
	

void ShortcutSlot::touchDragInsideAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent)
{

}

void ShortcutSlot::touchDragOutsideAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent)
{

}

void ShortcutSlot::touchDragEnterAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent)
{

}

void ShortcutSlot::touchDragExitAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent)
{

}

void ShortcutSlot::touchUpInsideAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent)
{
}

void ShortcutSlot::touchUpOutsideAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent)
{
}

void ShortcutSlot::touchCancelAction(Ref *sender, cocos2d::extension::Control::EventType controlEvent)
{

}
void ShortcutSlot::skillCoolDownCallBack( Node* node )
{
	mProgressTimer_skill->setVisible(false);
	CBSkill->setEnabled(true);

	auto sequence = Sequence::create(
		CCEaseBackIn::create(ScaleTo::create(0.15f,1.3f*m_fNormalScale)),
		ScaleTo::create(0.15f,1.0f*m_fNormalScale),
		NULL);
	this->runAction(sequence);
}
void ShortcutSlot::initType( int _typeid)
{
	switch(_typeid)
	{
	case 0 :
		slotType = T_Void;
		break;
	case 1 :
		slotType = T_Skill;
		break;
	case 2 :
		slotType = T_Goods;
		break;
	case 3 :
		slotType = T_InstanceGoods;
		break;
	}
}

void ShortcutSlot::useSkill()
{
// 	if (curIndex == 1 || curIndex == 0)
// 	{
		auto sc = ScriptManager::getInstance()->getScriptById(((ShortcutLayer*)this->getParent())->mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(this);
//	}

	//add by yangjun 2014.9.12
	auto shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	sc = ScriptManager::getInstance()->getScriptById(shortcutLayer->mTutorialScriptInstanceId);
	if(sc != NULL)
		sc->endCommand(shortcutLayer);

 	std::string skillId = curShortCut->skillpropid();
	//std::string skillId = FightSkill_SKILL_ID_PUTONGGONGJI;
	if (skillId.size()>0)
	{
// 		float mCDTime_skill;
// 		if (GameView::getInstance()->GameFightSkillList.find(skillId) != GameView::getInstance()->GameFightSkillList.end())
// 		{
// 			if (GameView::getInstance()->GameFightSkillList.find(skillId)->second->getCFightSkill()->has_intervaltime())
// 			{
// 				mCDTime_skill = GameView::getInstance()->GameFightSkillList.find(skillId)->second->getCFightSkill()->intervaltime()/1000.0f;
// 			}
// 			else
// 			{
// 				mCDTime_skill = 1.0f;
// 			}
// 		}
// 		else
// 		{
// 			mCDTime_skill = 1.0f;
// 		}

		auto myplayer = GameView::getInstance()->myplayer;
		myplayer->useSkill(skillId, true);

	}
}

void ShortcutSlot::useGoods()
{
	if(CDrug::isDrug(curShortCut->skillpropid()) )
	{
		if (!DrugManager::getInstance()->getDrugById(curShortCut->skillpropid()))
			return;

		auto pDrug = DrugManager::getInstance()->getDrugById(curShortCut->skillpropid());
		if (pDrug->isInCD())
			return;

		auto folderInfo = new FolderInfo();
		for (int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
		{
			if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
			{
				if (strcmp(curShortCut->skillpropid().c_str(),GameView::getInstance()->AllPacItem.at(i)->goods().id().c_str()) == 0)
				{
					folderInfo->CopyFrom(*GameView::getInstance()->AllPacItem.at(i));
					break;
				}
			}
		}

		if(!folderInfo->has_goods())
			return;

		if (folderInfo->goods().clazz() == GOODS_CLASS_HUIHUNDAN)//��佫�
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralListForTakeDrug) == NULL)
			{
				Size winSize = Director::getInstance()->getVisibleSize();
				auto generalListForTakeDrug = GeneralsListForTakeDrug::create(folderInfo);
				generalListForTakeDrug->setIgnoreAnchorPointForPosition(false);
				generalListForTakeDrug->setAnchorPoint(Vec2(0.5f,0.5f));
				generalListForTakeDrug->setPosition(Vec2(winSize.width/2,winSize.height/2));
				GameView::getInstance()->getMainUIScene()->addChild(generalListForTakeDrug,0,kTagGeneralListForTakeDrug);
			}
		}
		else if (folderInfo->goods().clazz() == GOODS_CLASS_YAOSHUI)
		{
			std::map<std::string,CDrug*>::const_iterator cIter;
			cIter = CDrugMsgConfigData::s_drugBaseMsg.find(folderInfo->goods().id());
			if (cIter != CDrugMsgConfigData::s_drugBaseMsg.end()) // �û�ҵ�����ָ�END��  
			{
				auto temp =  CDrugMsgConfigData::s_drugBaseMsg.find(folderInfo->goods().id())->second;
				if (temp->get_type_flag() == 2)//��佫�
				{
					if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralListForTakeDrug) == NULL)
					{
						Size winSize = Director::getInstance()->getVisibleSize();
						auto generalListForTakeDrug = GeneralsListForTakeDrug::create(folderInfo);
						generalListForTakeDrug->setIgnoreAnchorPointForPosition(false);
						generalListForTakeDrug->setAnchorPoint(Vec2(0.5f,0.5f));
						generalListForTakeDrug->setPosition(Vec2(winSize.width/2,winSize.height/2));
						GameView::getInstance()->getMainUIScene()->addChild(generalListForTakeDrug,0,kTagGeneralListForTakeDrug);
					}
				}
				else  //ý�ɫ�
				{
					int index = this->getCurFonderId();
					if(index != -1)
					{
						PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
						temp->index = index;
						temp->num = 1;
						GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0,temp);
						delete temp;
					}
					else
					{
						//GameView::getInstance()->showAlertDialog(StringDataManager::getString("potion_not_enough"));
					}
				}
			}
		}
		else
		{
			auto pDrug = DrugManager::getInstance()->getDrugById(curShortCut->skillpropid());
			if (pDrug->isInCD())
				return;

			int index = this->getCurFonderId();
			if(index != -1)
			{
				PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
				temp->index = index;
				temp->num = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0,temp);
				delete temp;
			}
			else
			{
				//GameView::getInstance()->showAlertDialog(StringDataManager::getString("potion_not_enough"));
			}
		}

		delete folderInfo;
	}
	else
	{
		int index = this->getCurFonderId();
		if(index != -1)
		{
			PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
			temp->index = index;
			temp->num = 1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)0,temp);
			delete temp;
		}
		else
		{
			//GameView::getInstance()->showAlertDialog(StringDataManager::getString("potion_not_enough"));
		}
	}
}

int ShortcutSlot::getCurFonderId()
{
	for (int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
	{
		if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
		{
			if (strcmp(curShortCut->skillpropid().c_str(),GameView::getInstance()->AllPacItem.at(i)->goods().id().c_str()) == 0)
			{
				return GameView::getInstance()->AllPacItem.at(i)->id();
			}
		}
	}

	//CCAssert(false, "please check it, should not run to here");
	return -1;
}

void ShortcutSlot::setInfo( CShortCut *shortCut )
{
	if(btn_bgFrame->getChildByName("gaoguang"))
	{
		btn_bgFrame->getChildByName("gaoguang")->removeFromParent();
	}

// 	if(btn_bgFrame->getChildByName("imageView_dressUp"))
// 	{
// 		btn_bgFrame->getChildByName("imageView_dressUp")->removeFromParent();
// 	}

	if (layer_icon->getChildByTag(kTag_CBSkill))
	{
		layer_icon->getChildByTag(kTag_CBSkill)->removeFromParent();
	}

	if (layer_icon->getChildByTag(kTag_HightLight))
	{
		layer_icon->getChildByTag(kTag_HightLight)->removeFromParent();
	}

	if (layer_icon->getChildByTag(kTag_ProgressTimer_skill))
	{
		layer_icon->getChildByTag(kTag_ProgressTimer_skill)->removeFromParent();
	}

	btn_bgFrame->setTouchEnabled(false);
	initType((int)shortCut->storedtype());
	
	curShortCut->CopyFrom(*shortCut);
	curIndex = curShortCut->index();

	Size screenSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	//ø�����Ͳ�ͬ��ʾ��ʽ��ͬ
	if (slotType == T_Void)
	{
		// ���cocos2d::extension::Scale9Sprite
		auto sprite = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/jinengqu/suibian.png");
		CBSkill = ControlButton::create(sprite);
		CBSkill->setPreferredSize(Size(46, 46));  
		CBSkill->setAnchorPoint(Vec2(0.5f, 0.5f));
		CBSkill->setPosition(Vec2(0,3));
		//CBSkill->setTouchEnabled(true);
	}
	else if (slotType == T_Skill)
	{
		auto sprite_skillIcondi = Sprite::create("gamescene_state/zhujiemian3/jinengqu/mengban_round.png");
		sprite_skillIcondi->setAnchorPoint(Vec2(0.5f,0.5f));
		sprite_skillIcondi->setPosition(Vec2(0,0));
// 		if (curIndex == 0)
// 		{
// 			sprite_skillIcondi->setScale(1.0f);
// 		}
// 		else
// 		{
// 			sprite_skillIcondi->setScale(0.85f);
// 		}
		layer_icon->addChild(sprite_skillIcondi);
		// õ��cocos2d::extension::Scale9Sprite
		std::string skillIconPath = "res_ui/jineng_icon/";

		if (shortCut->has_skillpropid())
		{
			auto baseSkill = StaticDataBaseSkill::s_baseSkillData[shortCut->skillpropid()];
			if (baseSkill)
			{
				skillIconPath.append(baseSkill->icon());
			}
			else
			{
				skillIconPath.append("jineng_2");
			}

			skillIconPath.append(".png");
		}
		else
 		{
			skillIconPath = "gamescene_state/zhujiemian3/jinengqu/suibian.png";
		}

		auto sprite = cocos2d::extension::Scale9Sprite::create(skillIconPath.c_str());
		sprite->setPreferredSize(Size(46,46));
		CBSkill = ControlButton::create(sprite);
		CBSkill->setPreferredSize(Size(46, 46));  
		CBSkill->setAnchorPoint(Vec2(0.5f, 0.5f));
		CBSkill->setPosition(Vec2(0,0));
		//CBSkill->setTouchEnabled(true);
	}
	else if (slotType == T_Goods)
	{
		// õ��cocos2d::extension::Scale9Sprite
		std::string skillIconPath = "res_ui/props_icon/";
		skillIconPath.append(shortCut->icon());
		//skillIconPath.append("yaoshui");
		skillIconPath.append(".png");

		auto sprite = cocos2d::extension::Scale9Sprite::create(skillIconPath.c_str());
		sprite->setPreferredSize(Size(46,46));
		CBSkill = ControlButton::create(sprite);
		CBSkill->setPreferredSize(Size(46, 46));  
		CBSkill->setAnchorPoint(Vec2(0.5f, 0.5f));
		CBSkill->setPosition(Vec2(0,0));
		//CBSkill->setTouchEnabled(true);

// 		auto  sprite_numFrame = cocos2d::extension::Scale9Sprite::create("");
// 		sprite_numFrame->setCapInsets(Rect());
// 		sprite_numFrame->setPreferredSize(); 
// 		sprite_numFrame->setAnchorPoint(0.5f,0);
// 		sprite_numFrame->setPosition(Vec2(0,-27));
// 		layer_num->addChild(sprite_numFrame);
		auto sprite_numFrame = Sprite::create("gamescene_state/zhujiemian3/jinengqu/zhaozhao00.png");
		sprite_numFrame->setAnchorPoint(Vec2(0.5f,0));
		sprite_numFrame->setPosition(Vec2(0,-26));
		layer_num->addChild(sprite_numFrame);

		num = 0;
		for (int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
		{
			if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
			{
				if (strcmp(curShortCut->skillpropid().c_str(),GameView::getInstance()->AllPacItem.at(i)->goods().id().c_str()) == 0)
				{
					num += GameView::getInstance()->AllPacItem.at(i)->quantity();
				}
			}
		}
		char s_num[20];
		sprintf(s_num,"%d",num);
		label_goodsNum = Label::createWithTTF(s_num,APP_FONT_NAME,12);
		label_goodsNum->setAnchorPoint(Vec2(0.5f,0));
		label_goodsNum->setPosition(Vec2(0,-30));
		auto shadowColor = Color4B::BLACK;   // black
		label_goodsNum->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		layer_num->addChild(label_goodsNum);
	}

	//ø߹
	auto highLight = Sprite::create(HIGHLIGHT3);
	highLight->setIgnoreAnchorPointForPosition(false);
	highLight->setAnchorPoint(Vec2(0.5f, 0.5f));
	highLight->setPosition(Vec2(0,3));
	//CBSkill->addChild(highLight);

	// ������ת������---����Ӱ �ϲ
	progress_skill = Sprite::create(PROGRESS_SKILL);
	mProgressTimer_skill = ProgressTimer::create(progress_skill);
	mProgressTimer_skill->setOpacity(200);
	mProgressTimer_skill->setAnchorPoint(Vec2(0.5f, 0.5f));
	mProgressTimer_skill->setPosition(Vec2(0,1));
	mProgressTimer_skill->setVisible(false);
	//CBSkill->addChild(mProgressTimer_skill, 600);  
	//CBSkill->setTouchEnabled(true);
	layer_icon->addChild(CBSkill, 1000,kTag_CBSkill);   
	layer_icon->addChild(highLight,1200,kTag_HightLight);   
	layer_icon->addChild(mProgressTimer_skill,1500,kTag_ProgressTimer_skill);   
 	CBSkill->setScale(1.2f);
 	mProgressTimer_skill->setScale(1.2f);
 	highLight->setScale(1.0f);

// 	Sprite * sprite_dressUp = Sprite::create("gamescene_state/zhujiemian3/jinengqu/lolo.png");
// 	sprite_dressUp->setAnchorPoint(Vec2(0.5f, 0.5f));
// 	sprite_dressUp->setPosition(Vec2(0,-23));
// 	layer_icon->addChild(sprite_dressUp,2000,kTag_adorn);

	//skillVariety
	if (slotType == T_Skill)
	{
		if (shortCut->has_skillpropid())
		{
			auto sprite_skillVariety = SkillScene::GetSkillVarirtySprite(shortCut->skillpropid().c_str());
			if (sprite_skillVariety)
			{
				sprite_skillVariety->setAnchorPoint(Vec2(0.5f,0.5f));
				sprite_skillVariety->setPosition(Vec2(15,-17));
				sprite_skillVariety->setScale(1.15f);
				layer_icon->addChild(sprite_skillVariety,2000,kTag_skillVariety);
			}
		}
	}

// 	if (curIndex == 0)
// 	{
// 		CBSkill->setScale(1.15f);
// 		mProgressTimer_skill->setScale(1.15f);
// 		highLight->setScale(1.0f);
// // 		sprite_dressUp->setScale(1.0f);
// // 		sprite_dressUp->setPosition(Vec2(0,-23));
// 	}
// 	else
// 	{
// 		CBSkill->setScale(1.0f);
// 		mProgressTimer_skill->setScale(1.0f);
// 		highLight->setScale(0.85f);
// // 		sprite_dressUp->setScale(0.85f);
// // 		sprite_dressUp->setPosition(Vec2(0,-20));
// 	}

	// Sets up event handlers
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchDownAction), cocos2d::extension::Control::EventType::TOUCH_DOWN);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchDragInsideAction), cocos2d::extension::Control::EventType::DRAG_INSIDE);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchDragOutsideAction), cocos2d::extension::Control::EventType::DRAG_OUTSIDE);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchDragEnterAction), cocos2d::extension::Control::EventType::DRAG_ENTER);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchDragExitAction), cocos2d::extension::Control::EventType::DRAG_EXIT);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchUpInsideAction), cocos2d::extension::Control::EventType::TOUCH_UP_INSIDE);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchUpOutsideAction), cocos2d::extension::Control::EventType::TOUCH_UP_OUTSIDE);
	CBSkill->addTargetWithActionForControlEvents(this, cccontrol_selector(ShortcutSlot::touchCancelAction), cocos2d::extension::Control::EventType::TOUCH_CANCEL);

	RefreshCD();
}

// void ShortcutSlot::setPeerlessInfo( CShortCut *shortCut )
// {
// 	btn_preelessBgFrame->addTouchEventListener(CC_CALLBACK_2(ShortcutSlot::PreelessEvent));
// 	initType((int)shortCut->storedtype());
// 
// 	curShortCut->CopyFrom(*shortCut);
// 
// 	Size screenSize = Director::getInstance()->getVisibleSize();
// 	Vec2 origin = Director::getInstance()->getVisibleOrigin();
// 	// ���cocos2d::extension::Scale9Sprite
// 	std::string skillIconPath = "res_ui/jineng_icon/";
// 
// 	if (shortCut->has_skillpropid())
// 	{
// 		CBaseSkill * baseSkill = StaticDataBaseSkill::s_baseSkillData[shortCut->skillpropid()];
// 		if (baseSkill)
// 		{
// 			skillIconPath.append(baseSkill->icon());
// 		}
// 		else
// 		{
// 			skillIconPath.append("jineng_2");
// 		}
// 
// 		skillIconPath.append(".png");
// 	}
// 	else
// 	{
// 		skillIconPath = "gamescene_state/zhujiemian3/jinengqu/suibian.png";
// 	}
// 
// 	imageView_prellIcon->loadTexture(skillIconPath.c_str());
// 
// 
// 	// the musou skill is not open, so ignore
// 	int musouSkillOpenLevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[7];
// 	if (GameView::getInstance()->myplayer->getActiveRole()->level() < musouSkillOpenLevel)
// 		return;
// 
// 	float per = (GameView::getInstance()->myplayer->getAngerValue()*1.0f)/(GameView::getInstance()->myplayer->getAngerCapacity()*1.0f);
// 	setPeerlessAngerEffect(per);
// }

void ShortcutSlot::setUseType(int index,bool canUse)
{
	if (this->getTag()-100 == index)
	{
		if (canUse)
		{
			this->slotUseType = T_CanUse;
			if (this->getChildByTag(3210) != NULL)
			{
				auto image_jinyong = (Sprite*)this->getChildByTag(3210);
				image_jinyong->removeFromParent();
			}
		}
		else
		{
			this->slotUseType = T_CanNotUse;
			//create image jinyong
			if (this->getChildByTag(3210) == NULL)
			{
				auto image_jinyong = Sprite::create("res_ui/jineng/disable.png");
				image_jinyong->setIgnoreAnchorPointForPosition(false);
				image_jinyong->setAnchorPoint(Vec2(0.5f, 0.5f));
				image_jinyong->setPosition(Vec2(0,0));
				image_jinyong->setLocalZOrder(1000);
				image_jinyong->setTag(3210);
				image_jinyong->setScale(1.35f);
				addChild(image_jinyong);

// 				if (curIndex == 0)
// 				{
// 					image_jinyong->setScale(1.1f*1.15f);
// 				}
// 				else
// 				{
// 					image_jinyong->setScale(1.1f*1.0f);
// 				}
			}
		}
	}
}

void ShortcutSlot::onEnter()
{
	UIScene::onEnter();
}
void ShortcutSlot::onExit()
{
	UIScene::onExit();
}	

bool ShortcutSlot::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return false;	
}
void ShortcutSlot::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void ShortcutSlot::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void ShortcutSlot::onTouchCancelled(Touch *pTouch, Event *pEvent)
{
}

float ShortcutSlot::getCurProgress()
{
	return mProgressTimer_skill->getPercentage();
}

void ShortcutSlot::BeganToRunCD()
{
	mProgressTimer_skill->setVisible(true);
	//CBSkill->setTouchEnabled(true);
	mProgressTimer_skill->setType(ProgressTimer::Type::RADIAL);
	mProgressTimer_skill->setReverseDirection(true); // ����ý����Ϊ��ʱ�

// 	std::string skillId = curShortCut->skillpropid();
// 	float coolTime = (float)(GameView::getInstance()->myplayer->getGameFightSkill(skillId.c_str())->getCoolTime()/1000);
	float mCDTime_skill;
	if (curShortCut->storedtype() == 1)
	{
		std::string skillId = curShortCut->skillpropid();
		if (skillId.size()>0)
		{

			if (GameView::getInstance()->GameFightSkillList.find(skillId) != GameView::getInstance()->GameFightSkillList.end())
			{
				if (GameView::getInstance()->GameFightSkillList.find(skillId)->second->getCFightSkill()->has_intervaltime())
				{
					mCDTime_skill = GameView::getInstance()->GameFightSkillList.find(skillId)->second->getCFightSkill()->intervaltime()/1000.0f;
				}
				else
				{
					mCDTime_skill = 1.0f;
				}
			}
			else
			{
				mCDTime_skill = 1.0f;
			}
		}
		else
		{
			mCDTime_skill = 1.0f;
		}
	}
	else if (curShortCut->storedtype() == 2)
	{
		if (CDrug::isDrug(curShortCut->skillpropid()))
		{
			if (DrugManager::getInstance()->getDrugById(curShortCut->skillpropid()))
			{
				auto pDrug = DrugManager::getInstance()->getDrugById(curShortCut->skillpropid());
				mCDTime_skill = pDrug->get_cool_time()/1000.0f;
			}
		}
	}
	
	//�����һ�����ʱ�䣨2014.4.10����Ŀǰ��Ҫ���ڸ�ְҵ�չ���Ĭ�cd�ʱ�
	if(mCDTime_skill <= 0)
		mCDTime_skill = 0.3f;

	action_progress_from_to = ProgressFromTo::create(mCDTime_skill , 100, 0);     
	action_callback = CallFuncN::create(CC_CALLBACK_1(ShortcutSlot::skillCoolDownCallBack,this));
	mProgressTimer_skill->runAction(Sequence::create(action_progress_from_to, action_callback, NULL));
}

void ShortcutSlot::DoNothing( Ref *pSender )
{
	//CCLOG("donothing !!!!!!!!!");
}

void ShortcutSlot::useMusouSkill()
{
	GameView::getInstance()->myplayer->useMusouSkill();

	if (NewCommerStoryManager::getInstance()->IsNewComer())
	{
		NewCommerStoryManager::getInstance()->setIsReqForUseMusouSkill(true);
	}
}

#define MUSOUSKILL_BUTTON_ACTION_TAG 634
void ShortcutSlot::PreelessEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(((ShortcutLayer*)this->getParent())->mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(this);

		if (GameView::getInstance()->myplayer->hasExStatus(ExStatusType::musou))
		{
			//CCLOG("you can use peerlessSkill now !!! ");
			// ���갴ť�Ѿ�����
			if (this->getActionByTag(MUSOUSKILL_BUTTON_ACTION_TAG) != NULL)
				return;

			auto scene = GameView::getInstance()->getGameScene();

			auto myplayer = GameView::getInstance()->myplayer;
			// Effect 1
			//myplayer->onSkillReleased(FIGHTSKILL_ID_MUSOU_EXTRA, NULL, 0, 0);
			//useMusouSkill();
			//return;
			//add sound effect
			GameUtils::playGameSound(EFFECT_MUSOU, 2, false);
			// Effect 2
			auto sceneLayer = scene->getChildByTag(GameSceneLayer::kTagSceneLayer);
			Vec2 screenPos = myplayer->getPosition() + sceneLayer->getPosition();
			MusouSpecialEffect* effectNode = MusouSpecialEffect::create(screenPos);
			scene->addChild(effectNode, SCENE_STORY_LAYER_BASE_ZORDER);

			auto action = (ActionInterval*)Sequence::create(
				DelayTime::create(effectNode->getDuration()),
				CallFunc::create(CC_CALLBACK_0(ShortcutSlot::useMusouSkill, this)),
				DelayTime::create(1.0f),   // extra delay to avoid the player click the musou skill button quickly
				NULL);
			action->setTag(MUSOUSKILL_BUTTON_ACTION_TAG);
			runAction(action);
		}
		else
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("musou_not_enough"));
			//CCLOG("not enought power to use peerlessSkill ... ");
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void ShortcutSlot::setPeerlessAngerEffect( float percentage )
{
	imageView_time->setTextureRect(Rect(0,58*(1-percentage),58,58*percentage));

	if (percentage >= 1)
	{
		if (this->getChildByTag(ParticleEffect_Musou_Tag))
		{
			(this->getChildByTag(ParticleEffect_Musou_Tag))->setVisible(true);
		}
	}
	else
	{
		if (this->getChildByTag(ParticleEffect_Musou_Tag))
		{
			(this->getChildByTag(ParticleEffect_Musou_Tag))->setVisible(false);
		}
	}

	auto lam_fire = (CCLegendAnimation*)layer_icon->getChildByTag(kTag_FireAnm);
	if (lam_fire)
	{
		lam_fire->setScale(0.5f*(1.0f+percentage));
	}
}

void ShortcutSlot::refreshRetainNum( FolderInfo * folderInfo )
{
	num = 0;
	if (folderInfo->has_goods())
	{
		for (int i = 0;i<(int)GameView::getInstance()->AllPacItem.size();++i)
		{
			if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
			{
				if (strcmp(folderInfo->goods().id().c_str(),GameView::getInstance()->AllPacItem.at(i)->goods().id().c_str()) == 0)
				{
					num += GameView::getInstance()->AllPacItem.at(i)->quantity();
				}
			}
		}
	}
	else
	{
		num = 0;
	}
	char s_num[20];
	sprintf(s_num,"%d",num);
	label_goodsNum->setString(s_num);
}

void ShortcutSlot::RefreshCD()
{
	// 	std::string skillId = curShortCut->skillpropid();
	// 	float coolTime = (float)(GameView::getInstance()->myplayer->getGameFightSkill(skillId.c_str())->getCoolTime()/1000);
	float mCDTime_skill = 0.0f;
	float remaintimes = 0.0f;
	if (curShortCut->storedtype() == 1)
	{
		std::string skillId = curShortCut->skillpropid();
		if (skillId.size()>0)
		{

			if (GameView::getInstance()->GameFightSkillList.find(skillId) != GameView::getInstance()->GameFightSkillList.end())
			{
				if (GameView::getInstance()->GameFightSkillList.find(skillId)->second->getCFightSkill()->has_intervaltime())
				{
					mCDTime_skill = GameView::getInstance()->GameFightSkillList.find(skillId)->second->getCFightSkill()->intervaltime()/1000.0f;
				}
				else
				{
					mCDTime_skill = 1.0f;
				}
			}
			else
			{
				mCDTime_skill = 1.0f;
			}
		}
		else
		{
			mCDTime_skill = 1.0f;
		}

		remaintimes = (GameView::getInstance()->myplayer->getGameFightSkill(skillId.c_str(), skillId.c_str()))->getRemainCD()/1000.0f;
	}
	else if (curShortCut->storedtype() == 2)
	{
		if (CDrug::isDrug(curShortCut->skillpropid()))
		{
			if (DrugManager::getInstance()->getDrugById(curShortCut->skillpropid()))
			{
				auto pDrug = DrugManager::getInstance()->getDrugById(curShortCut->skillpropid());
				mCDTime_skill = pDrug->get_cool_time()/1000.0f;
				remaintimes = pDrug->getRemainTime()/1000.0f;
			}
		}
	}

	//�����һ�����ʱ�䣨2014.4.10����Ŀǰ��Ҫ���ڸ�ְҵ�չ���Ĭ�cd�ʱ�
	if(mCDTime_skill <= 0)
		mCDTime_skill = 0.3f;	

	if (remaintimes <= 0)
	{
		remaintimes = 0.0f;
		return;
	}

	mProgressTimer_skill->setVisible(true);
	//CBSkill->setTouchEnabled(true);
	mProgressTimer_skill->setType(ProgressTimer::Type::RADIAL);
	mProgressTimer_skill->setReverseDirection(true); // ����ý����Ϊ��ʱ�

	action_progress_from_to = ProgressFromTo::create(remaintimes , (remaintimes/mCDTime_skill)*100, 0);     
	action_callback = CallFuncN::create(CC_CALLBACK_1(ShortcutSlot::skillCoolDownCallBack,this));
	mProgressTimer_skill->runAction(Sequence::create(action_progress_from_to, action_callback, NULL));
}

void ShortcutSlot::setScale( float scale )
{
	UIScene::setScale(scale);
	m_fNormalScale = scale;
}
