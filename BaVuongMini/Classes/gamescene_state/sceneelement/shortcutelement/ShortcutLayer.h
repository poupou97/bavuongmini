

#ifndef _GAMESCENESTATE_SHORTCUTLAYER_H_
#define _GAMESCENESTATE_SHORTCUTLAYER_H_

#include "cocos-ext.h"
#include "../../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../../legend_script/CCTutorialIndicator.h"
#include "../../../utils/StaticDataManager.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CShortCut;
class FolderInfo;

class ShortcutLayer : public UIScene
{
public:
	ShortcutLayer();
	~ShortcutLayer();

	static ShortcutLayer* create();
	virtual bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void CloseEvent(Ref * pSender);

	/******������ӷ���仯ʱ���ã�1301,1303��******/
	void RefreshAll();
	void RefreshOne(FolderInfo* folderInfo);

	void RefreshOneShortcutSlot(CShortCut * shortcut);
	void RefreshMusouEffect( float percentage );

	void setInfoByLayerType();

	virtual void update(float dt);
	void setSlotUseType(int index,bool canUse);

	void RefreshCD(std::string skillId);

	void addNewShortcutSlot();
	void PeerlessEvent(Ref* pSender, Widget::TouchEventType type);

	//
	ui::Button * btn_close;
	ui::Button * btn_first;
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	//���־����У�����Ҫǿ�ƣ�
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

	//��ѧ
	int mTutorialScriptInstanceId;

	/**************�������־��*************/
	void setMusouShortCutSlotPresent(bool isPresent);
	//�ǿ�ƴ�����꼼�ܰ�ť�����ܵȼ����
	void createMusouShortCutSlotForce();
	//���꼼�ܰ�ť�ָ�����״̬���ܵȼ����
	void recoverMusouShortCutSlot();
	//���ʾ�����ܰ�ť���������ͨ���
	void presentShortCutSlot(int num);
	/*************************************/

public:
	Layer * u_layer;
	Layer * u_teachLayer;
	int shortcutSlotNum;
	Vec2 slotPos[5] ;
	ui::ImageView * bg;
	ui::Button * closeButton;

	int shortcurTag[5];

public:
	//��ж�ħ��ˮ�Ƿ��㹻
	bool isEnoughMagic(CShortCut * shortcut);
	//�ж��Ƿ�ѣ�
	bool isImmobilize();
	//��жϱ������Ƿ��и���Ʒ
	bool isEnoughGoods(CShortCut * shortcut);

	Vec2 touchBeganPoint;

};
#endif;

