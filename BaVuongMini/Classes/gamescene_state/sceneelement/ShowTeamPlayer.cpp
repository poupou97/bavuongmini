#include "ShowTeamPlayer.h"
#include "../../messageclient/element/CTeamMember.h"
#include "../../ui/Friend_ui/TeamMemberList.h"
#include "../GameSceneState.h"
#include "GameView.h"
#include "../role/GameActor.h"
#include "../role/MyPlayer.h"
#include "../MainScene.h"
#include "../role/BasePlayer.h"
#include "../../utils/GameUtils.h"
#include "AppMacros.h"


ShowTeamPlayer::ShowTeamPlayer(void)
{
}


ShowTeamPlayer::~ShowTeamPlayer(void)
{
	
}

ShowTeamPlayer * ShowTeamPlayer::create(CTeamMember * members,long long leader)
{
	auto teamplayers=new ShowTeamPlayer();
	if (teamplayers && teamplayers->init(members,leader))
	{
		teamplayers->autorelease();
		return teamplayers;
	}
	CC_SAFE_DELETE(teamplayers);
	return NULL;
}

bool ShowTeamPlayer::init(CTeamMember * members,long long leader)
{
	if (UIScene::init())
	{
		//m_pLayer->setSwallowsTouches(false);
		
		auto playerBg = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/team_di.png");
		playerBg->setAnchorPoint(Vec2(0,0));
		playerBg->setPosition(Vec2(0,-3));
		addChild(playerBg);

		int pression_ = members->profession();
		std::string icon_path =  BasePlayer::getHeadPathByProfession(pression_);
		auto playerHead = Sprite::create(icon_path.c_str());
		playerHead->setAnchorPoint(Vec2(0,0));
		playerHead->setPosition(Vec2(3,3));
		playerHead->setScale(0.6f);
		playerBg->addChild(playerHead);

		int memberHp_ = members->hp();
		int memberMaxHp_ = members->maxhp();
		float scaleHp = (float)memberHp_/memberMaxHp_;
		auto sp_hp = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/team_hp.png");
		sp_hp->setAnchorPoint(Vec2(0,0));
		sp_hp->setPosition(Vec2(43,13));
		sp_hp->setScaleX(scaleHp);
		playerBg->addChild(sp_hp);

		int memberMp_ = members->mp();
		int memberMaxMp_ = members->maxmp();
		float scaleMp = (float)memberMp_/memberMaxMp_;
		auto sp_mp = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/team_mp.png");
		sp_mp->setAnchorPoint(Vec2(0,0));
		sp_mp->setPosition(Vec2(42,6));
		sp_mp->setScaleX(scaleMp);
		playerBg->addChild(sp_mp);

		auto headFrame = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/aa.png");
		headFrame->setAnchorPoint(Vec2(1,0));
		headFrame->setPosition(Vec2(playerBg->getContentSize().width,3));
		playerBg->addChild(headFrame);

		auto headLevelSp = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/lv_di.png");
		headLevelSp->setAnchorPoint(Vec2(0.5f,0.5f));
		headLevelSp->setPosition(Vec2(36,27));
		playerBg->addChild(headLevelSp);

		int memberLevel = members->level();
		char LevelStr[10];
		sprintf(LevelStr,"%d",memberLevel);
		auto label_level = Label::createWithTTF(LevelStr, APP_FONT_NAME, 14);
		label_level->setAnchorPoint(Vec2(0.5f,0.5f));
		label_level->setPosition(Vec2(headLevelSp->getContentSize().width/2,headLevelSp->getContentSize().height/2));
		headLevelSp->addChild(label_level);

		auto label_name = Label::createWithTTF(members->name().c_str(), APP_FONT_NAME, 14);
		label_name->setAnchorPoint(Vec2(0.5f,0));
		label_name->setPosition(Vec2(34+(playerBg->getContentSize().width-34)/2,21));
		playerBg->addChild(label_name);

		int vipLevel_ = members->viplevel();
		Sprite * vip_node;
		if (vipLevel_ > 0)
		{
			vip_node =(Sprite *)MainScene::addVipInfoByLevelForNode(vipLevel_);
			vip_node->setScale(0.6f);
			playerBg->addChild(vip_node);
			vip_node->setPosition(Vec2(label_name->getPosition().x + label_name->getContentSize().width/2 + vip_node->getContentSize().width/2,
										playerBg->getContentSize().height/2 + 11));
		}
		Sprite * leaderIcon_;
		if (members->roleid() == leader)
		{
			leaderIcon_ = Sprite::create("res_ui/dui.png");
			leaderIcon_->setAnchorPoint(Vec2(0,0));
			leaderIcon_->setPosition(Vec2(0,18));
			playerBg->addChild(leaderIcon_);
		}
		auto playerBgOnLine = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/team_mengban.png");
		playerBgOnLine->setAnchorPoint(Vec2(0,0));
		playerBgOnLine->setPosition(Vec2(0,-3));
		addChild(playerBgOnLine);
		//is not onLine
		if (members->isonline() == 1)
		{
			playerBgOnLine->setVisible(false);
		}else
		{
			playerBgOnLine->setVisible(true);
		}

		this->setIgnoreAnchorPointForPosition(false);
		this->setAnchorPoint(Vec2(0,0));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(playerBg->getContentSize().width,playerBg->getContentSize().height));
		return true;
	}
	return false;
}

void ShowTeamPlayer::onEnter()
{
	UIScene::onEnter();
}

void ShowTeamPlayer::onExit()
{
	UIScene::onExit();
}

void ShowTeamPlayer::playerInfo( Ref * obj )
{
	/*
	MainScene * mainscene =(MainScene *)GameView::getInstance()->getMainUIScene();
	Size winsize =Director::getInstance()->getVisibleSize();
	long long selfId =GameView::getInstance()->myplayer->getRoleId();
	long long numIdx_ =m_member->roleid();
	if (selfId != numIdx_)
	{
		TeamMemberListOperator * teamlistPlayer =TeamMemberListOperator::create(m_member,m_leader);
		teamlistPlayer->setAnchorPoint(Vec2(0,0));
		teamlistPlayer->setPosition(Vec2(mainscene->getChildByTag(kTagMissionAndTeam)->getPositionX()+this->getContentSize().width+70,
												winsize.height/2+teamlistPlayer->getContentSize().height/2));
		//teamlistPlayer->setPosition(Vec2(100,450));
		teamlistPlayer->setLocalZOrder(10);
		mainscene->addChild(teamlistPlayer);
	}
	*/
}

bool ShowTeamPlayer::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return false;
}

void ShowTeamPlayer::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void ShowTeamPlayer::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void ShowTeamPlayer::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}
