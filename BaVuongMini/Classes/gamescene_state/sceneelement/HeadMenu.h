
#ifndef _GAMESCENESTATE_HEADMENU_H_
#define _GAMESCENESTATE_HEADMENU_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../legend_script/CCTutorialIndicator.h"


USING_NS_CC;
USING_NS_CC_EXT;

class CFunctionOpenLevel;

class HeadMenu :public UIScene
{
public:
	HeadMenu();
	~HeadMenu();
	static HeadMenu* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);


	void ButtonRoleInfoEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonSkillEvent(Ref *pSender, Widget::TouchEventType type);
	//void ButtonPackageInfoEvent(Ref *pSender);
	void ButtonStrengthenEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonGeneralsEvent(Ref *pSender, Widget::TouchEventType type);
	//void ButtonStudyEvent(Ref *pSender);
	//void ButtonTacticalEvent(Ref *pSender);
	//void ButtonArtOfWarEvent(Ref *pSender);
	void ButtonMountEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonMissionEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonFriendEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonMailEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonFamilyEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonRankingsEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonHangUpEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonSetEvent(Ref *pSender, Widget::TouchEventType type);
	//void ButtonSoulEvent(Ref *pSender);

	void addNewFunction(Node * pNode,void * functionOpenLevel);

	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	//study button is right 
	void addCCTutorialIndicatorRD( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();
public:
	Button * btn_roleIf;
	Button * btn_skill;
	Button * btn_strengthen;
	Button * btn_generals;
	Button * Button_mail;
	Button * btn_mission;
	Button * btn_mount;
	Button * Button_hangUp;
	Button * Button_family;
	Button * Button_rankings;
	Button * Button_friend;
private:
	Size winSize;
	struct FriendStruct
	{
		int relationType;
		int pageNum;
		int pageIndex;
		int showOnline;
	};

	std::vector<CFunctionOpenLevel *>opendedFunctionVector;   //������ʾ�İ�ť(�ײ�����)
	//��ѧ
	int mTutorialScriptInstanceId;
};
#endif;
