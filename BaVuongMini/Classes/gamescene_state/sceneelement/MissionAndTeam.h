
#ifndef _GAMESCENESTATE_MISSIONANDTEAM_H_
#define _GAMESCENESTATE_MISSIONANDTEAM_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;
using namespace cocos2d::ui;

class UITab;
class MissionCell;
class CTeamMember;
class MissionAndTeam : public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:

	enum PanelTypes
	{
		type_MissionAndTeam = 0,
		type_FiveInstanceAndTeam,
		type_FamilyFightAndTeam,
		type_SingCopyChanllengeOnly,
		type_OfflineArenaOnly,
	};

	enum CurPresentPanel
	{
		p_mission = 0,
		p_team,
		p_fiveInstance,
		p_familyFight,
		p_singCopyChallenge,
		p_offLineArena,
	};

	struct GeneralInfoInArena{
		long long generalId;
		std::string name;
		int level;
		int max_hp;
		int cur_hp;
		bool isFinishedFight;
		int star;
	};

	MissionAndTeam();
	~MissionAndTeam();
	static MissionAndTeam* create();
	bool init();
	
	//�ܵ
	Layer * layer_leader;

	Layer * MissionLayer ;
	Layer * TeamLayer;
	Layer * FiveInstanceLayer;
	Layer * FamilyFightLayer;
	Layer * SingCopyLayer;
	Layer * OffLineArenaLayer;

	bool isExistMissionLayer;
	bool isExistTeamLayer;
	bool isExistFiveInstanceLayer;
	bool isExistFamilyFightLayer;
	bool isExistSingCopylayer;
	bool isExistOffLineArenaLayer;
	bool isOn ;

	Button * Button_mission;
	Button * Button_team;
	Button * Button_fiveInstance;
	Button * Button_familyFight;
	Button * Button_singCopy;
	Button * Button_offLineArena;

	void ButtonMissionEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonTeamEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonFiveInstanceEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonFamilyFightEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonSingCopyChallengeEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonOffLineArenaEvent(Ref *pSender, Widget::TouchEventType type);

	void initMissionLayer();
	void initTeamLayer();
	void initFiveInstanceLayer();
	void initFamilyFightLayer();
	void initSingCopyLayer();
	void initOffLineArenaLayer();

	//teamInvite
	Button * teamInvite_btn;
	void callBackTeamInvite(Ref *pSender, Widget::TouchEventType type);

	std::vector<CTeamMember *>selfTeamPlayerVector;
	void refreshTeamPlayer();
	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	virtual void update(float dt);

    virtual void tableCellHighlight(TableView* table, TableViewCell* cell);
    virtual void tableCellUnhighlight(TableView* table, TableViewCell* cell);
	
	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//Ĵ�������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);


	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	//��һ�
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	//�ڶ�
	void addCCTutorialIndicator2(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	//����
	void addCCTutorialIndicator3(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  );
	void removeCCTutorialIndicator();

	int m_nTutorialIndicatorIndex;

	void reloadMissionData();
	void reloadMissionDataWithoutChangeOffSet(Vec2 p_offset,Size p_contentSize);

	//15弶ǰָ������ı
	void addGuidePen();
	void removeGuidePen();

	/** �����������Ƿ����ָ����������ָ������ıʲ����֣� */
	bool isExistTutorialInMainScene();

	TableView * getLeftOffLineArenaTableView();
	TableView * getRightOffLineArenaTableView();

private:
	Button * missionAndTeam_on_off;
	ImageView * ImageView_missionAndTeam_on_off;

	Vec2 touchBeganPoint;
	int contentSizeHeight;

	Button * Button_flag;

	//Label* l_boss_Value;
	//Label* l_monster_Value;

	void MissionAndTeamOnOff(Ref *pSender, Widget::TouchEventType type);

	void popUpMission(Ref *pSender);
	void takeBackMission(Ref *pSender);

	//��ѧ
	int mTutorialScriptInstanceId;

	//�ϴε���ʱ�
	long long m_nLastTouchTime;
public:
	TableView * missionTableView ;
	TableView * teamTableView ;
	TableView * fiveInstanceTableView;
	TableView * familyFightTableView;
	TableView * singCopyTableview;

	long long leaderId;

private:
	Label * curLevel_BmFont;
	LabelAtlas * m_curLevelShowValue;
	LabelAtlas * label_monsterValue;
	Label * m_useTimeValue;
	Label * allTimeShowValue;

	int m_singCopy_curLevel;
	int m_singCopy_curMonsterNumber;
	int m_singCopy_monsterNumbers;
	int m_singCopy_remainMonster;
	int m_singCopy_useTime;
	int m_singCopy_timeCount;
public:
	//̵����ն��������Ӧ�¼
	void callBackSingCopy(Ref *pSender, Widget::TouchEventType type);
	void updateSingCopyCheckMonst(float delta);

	void setSingCopy_useTime(int time_);
	int getSingCopy_useTime();

	void setStateIsRobot(bool value_);
	bool getStateIsRobot();

private:
	static int s_panelType;
	static int s_curPresentPanel;

	//��еĹ������boss���
	int cur_boss_count;
	int cur_monster_count;
	int max_boss_count;
	int max_monster_count;

	//
	bool m_state_start;

public:
	static void setPanelType(int stype);
	static int getPanelType();

	static void setCurPresentPanel(int cpPanel);
	static int getCurPresentPanel();

	void refreshFunctionBtn();

	//����������µ�С��ͷ
	void refreshDownArrowStatus();

	//����������Ӧ�¼
	void FiveInstanceInfoEvent(Ref * pSender);
	//�Ѱ·��ɺ�ص��
	void DoThingWhenFinishedMove(Vec2 targetPos);
	Vec2 m_Target_pos_inInstance;
	//update �����(�Ѱ·��ɺ��Զ�����һ)
	void CheckDistance(float dt);

	//���ȸ��
	void reloadToDefaultConfig(int instanceId);
	void updateInstanceSchedule(int bossCount,int monsterCount);
	void updateFamilyFightRemainTime(float delta );

	//¼���ս��ȸ��
	void updateFamilyFightSchedult();
	//update ¼����(�Ѱ·��ɺ��Զ�����һ)
	Vec2 m_Target_pos_inFamilyFight;
	void CheckDistanceInFamilyFight(float dt);

	//singCopy schedule 
	void updateSingCopyCellInfo(int level,int curMon,int allMon,int remMon,int useTime,int timeCount);
	void starRunUpdate();
	void updateSingCopyCurUseTime(float delta );

	void refreshTutorial();

	//data for offLineArena//
public:
	GeneralInfoInArena* p_enemyInfoInArena;
	std::vector<GeneralInfoInArena*> generalsInArena_mine;
	std::vector<GeneralInfoInArena*> generalsInArena_other;

	long long m_nRemainTime_Arena;

	//���ʣ��ʱ����
	void updateOffLineArenaRemainTime(float delta );

	void setArenaDataToDefault();

	//data for team
public:
	void ManageTeamEvent(Ref *pSender, Widget::TouchEventType type);
	void ExitTeamEvent(Ref *pSender, Widget::TouchEventType type);
};
#endif;

