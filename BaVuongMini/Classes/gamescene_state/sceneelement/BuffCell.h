
#ifndef _GAMESCENESTATE_BUFFCELL_H_
#define _GAMESCENESTATE_BUFFCELL_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ExStatus;
class ExStatusItem;
class CCMoveableMenu;
class MissionInfo;

class BuffCell : public TableViewCell
{
public:
	BuffCell();
	~BuffCell();
	static BuffCell* create(ExStatus * exstatus);
	bool init(ExStatus * exstatus);

	virtual void update(float delta);

private:
	ExStatusItem * exStatusItem;
	Sprite * Sprite_icon;
	Label * num;
	Label * name;
	Label * time;
	int m_nRemainTime;

	ExStatus * curExstatus;

	std::string ConvertToDateFormat(int t);
};

#endif;