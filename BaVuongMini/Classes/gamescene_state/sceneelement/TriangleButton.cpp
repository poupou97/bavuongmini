#include "TriangleButton.h"
#include "GameView.h"
#include "../MainScene.h"
#include "GeneralsInfoUI.h"
#include "../../utils/Joystick.h"
#include "shortcutelement/ShortcutLayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"


#define kTag_Action_Move 10

TriangleButton::TriangleButton(void):
isOpen(true)
{
}


TriangleButton::~TriangleButton(void)
{
}

bool TriangleButton::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return false;
}

void TriangleButton::onTouchEnded( Touch *pTouch, Event *pEvent )
{
	CCLOG("TouchEnd");
}

TriangleButton* TriangleButton::create()
{
	auto pRet = new TriangleButton();
	if (pRet && pRet->init())
	{
		pRet->autorelease();
		return pRet;
	}
	
	CC_SAFE_DELETE(pRet);
	return NULL;
}

bool TriangleButton::init()
{
	if (UIScene::init())
	{
		auto btn_frame = Button::create();
		btn_frame->loadTextures("gamescene_state/zhujiemian3/jingyantiao/ssuu.png","gamescene_state/zhujiemian3/jingyantiao/ssuu.png","");
		btn_frame->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_frame->setPosition(Vec2(btn_frame->getContentSize().width/2,btn_frame->getContentSize().height/2));
		btn_frame->setTouchEnabled(true);
		btn_frame->setPressedActionEnabled(true);
		btn_frame->addTouchEventListener(CC_CALLBACK_2(TriangleButton::FrameEvent,this));
		m_pLayer->addChild(btn_frame);

		imageView_arrow = ImageView::create();
		imageView_arrow->loadTexture("gamescene_state/zhujiemian3/renwuduiwu/arrow.png");
		imageView_arrow->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_arrow->setPosition(Vec2(btn_frame->getBoundingBox().size.width/2, btn_frame->getBoundingBox().size.height / 2));
		imageView_arrow->setScale(0.5f);
		imageView_arrow->setRotation(45);
		btn_frame->addChild(imageView_arrow);

		this->setContentSize(btn_frame->getContentSize());
		////this->setTouchEnabled(false);

		return true;
	}
	return false;
}

void TriangleButton::onEnter()
{
	UIScene::onEnter();
}

void TriangleButton::onExit()
{
	UIScene::onExit();
}

void TriangleButton::FrameEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//����ȼ�
		int openlevel = 0;
		std::map<int, int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(22);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // û�ҵ�����ָ�END��  
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[22];
		}

		if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
		{
			char s_des[200];
			const char* str_des = StringDataManager::getString("shrinkage_will_be_open");
			sprintf(s_des, str_des, openlevel);
			GameView::getInstance()->showAlertDialog(s_des);
			return;
		}

		auto maiScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (!isOpen)
		{
			auto shortcutLayer = (Layer*)maiScene->getChildByTag(kTagShortcutLayer);
			if (shortcutLayer)
			{
				if (shortcutLayer->getActionByTag(kTag_Action_Move) == NULL)
				{
					auto moveLeft = MoveTo::create(0.3f, Vec2(shortcutLayer->getPositionX() - 200, shortcutLayer->getPositionY()));
					shortcutLayer->runAction(moveLeft);
					moveLeft->setTag(kTag_Action_Move);

					isOpen = true;
					imageView_arrow->setRotation(45);
					imageView_arrow->setPosition(Vec2(5, -5));
				}
			}
			//generalInfoUI down
			auto generalsInfoUI = (GeneralsInfoUI*)maiScene->getGeneralInfoUI();
			if (generalsInfoUI)
			{
				if (generalsInfoUI->getActionByTag(kTag_Action_Move) == NULL)
				{
					auto moveUp = MoveTo::create(0.3f, Vec2(generalsInfoUI->getPositionX(), 0));
					generalsInfoUI->runAction(moveUp);
					moveUp->setTag(kTag_Action_Move);
				}
			}
			//�ҡ�
			auto pJoystick = (Joystick*)maiScene->getChildByTag(kTagVirtualJoystick);
			if (pJoystick)
			{
				if (pJoystick->getActionByTag(kTag_Action_Move) == NULL)
				{
					auto moveRight = MoveTo::create(0.3f, Vec2(pJoystick->getPositionX() + 200, pJoystick->getPositionY()));
					pJoystick->runAction(moveRight);
					moveRight->setTag(kTag_Action_Move);

					pJoystick->Active();
				}
			}
		}
		else
		{
			auto shortcutLayer = (Layer*)maiScene->getChildByTag(kTagShortcutLayer);
			if (shortcutLayer)
			{
				if (shortcutLayer->getActionByTag(kTag_Action_Move) == NULL)
				{
					auto moveRight = MoveTo::create(0.3f, Vec2(shortcutLayer->getPositionX() + 200, shortcutLayer->getPositionY()));
					shortcutLayer->runAction(moveRight);
					moveRight->setTag(kTag_Action_Move);

					isOpen = false;
					imageView_arrow->setRotation(-135);
					imageView_arrow->setPosition(Vec2(4, -4));
				}
			}
			//generalInfoUI down
			auto generalsInfoUI = (GeneralsInfoUI*)maiScene->getGeneralInfoUI();
			if (generalsInfoUI)
			{
				if (generalsInfoUI->getActionByTag(kTag_Action_Move) == NULL)
				{
					auto moveDown = MoveTo::create(0.3f, Vec2(generalsInfoUI->getPositionX(), -105));
					generalsInfoUI->runAction(moveDown);
					moveDown->setTag(kTag_Action_Move);
				}
			}
			//�ҡ�
			auto pJoystick = (Joystick*)maiScene->getChildByTag(kTagVirtualJoystick);
			if (pJoystick)
			{
				if (pJoystick->getActionByTag(kTag_Action_Move) == NULL)
				{
					auto moveLeft = MoveTo::create(0.3f, Vec2(pJoystick->getPositionX() - 200, pJoystick->getPositionY()));
					pJoystick->runAction(moveLeft);
					moveLeft->setTag(kTag_Action_Move);

					pJoystick->Inactive();
				}
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void TriangleButton::setAllOpen()
{
	isOpen = true;
	imageView_arrow->setRotation(45);
	imageView_arrow->setPosition(Vec2(5,-5));

	Size winSize = Director::getInstance()->getVisibleSize();
	auto maiScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	auto shortcutLayer = (ShortcutLayer*)maiScene->getChildByTag(kTagShortcutLayer);
	if (shortcutLayer)
	{
		shortcutLayer->setPosition(Vec2(winSize.width/2,winSize.height/2));
	}
	//generalInfoUI down
	auto generalsInfoUI = (GeneralsInfoUI*)maiScene->getGeneralInfoUI();
	if (generalsInfoUI)
	{
		generalsInfoUI->setPosition(Vec2(winSize.width/2,0));
	}
	//�ҡ�
	auto pJoystick = (Joystick*)maiScene->getChildByTag(kTagVirtualJoystick);
	if(pJoystick)
	{
		pJoystick->setPosition(Vec2(0,pJoystick->getPositionY()));
	}
}
