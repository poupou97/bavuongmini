#ifndef _GAMESCENESTATE_NEWFUNCTIONREMINDUI_H_
#define _GAMESCENESTATE_NEWFUNCTIONREMINDUI_H_

#include "ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class NewFunctionRemindUI : public UIScene
{
public:
	NewFunctionRemindUI();
	~NewFunctionRemindUI();

	virtual void onEnter();
	virtual void onExit();
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	static NewFunctionRemindUI* create(int level);
	bool init(int level);

	virtual void update(float dt);

	void CloseEvent(Ref *pSender, Widget::TouchEventType type);
	void BtnEvent(Ref *pSender, Widget::TouchEventType type);

protected:
	std::map<int,std::string>newFunctionList;
	int time;
};

#endif

