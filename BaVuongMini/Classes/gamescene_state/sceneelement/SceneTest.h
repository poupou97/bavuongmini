
#ifndef _GAMESCENESTATE_SCENETEST_H_
#define _GAMESCENESTATE_SCENETEST_H_

#include "cocos-ext.h"
#include "ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "ui/extensions/UITab.h"

USING_NS_CC;
USING_NS_CC_EXT;

class SceneTest :public UIScene, public TableViewDataSource, public TableViewDelegate
{
public:
	SceneTest();
	~SceneTest();
	static SceneTest* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);

	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(TableView* table, TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(TableView *table, ssize_t  idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(TableView *table);
private:
	/////////�������///////////////
	std::string testString[3];
	/////////�������///////////////
};
#endif;

