
#ifndef _GAMESCENESTATE_TARGETINFO_H_
#define _GAMESCENESTATE_TARGETINFO_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class BaseFighter;
class UITab;
class CCRichLabel;

class TargetInfo : public UIScene
{
public:
	TargetInfo();
	~TargetInfo();

	static TargetInfo* create(BaseFighter *baseFighter);
	bool init(BaseFighter *baseFighter);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	virtual void update(float delta);

	Label * Label_targetAllBlood;
	Label * Label_targetCurBlood;
	Label * Label_targetAllMagic;
	Label * Label_targetCurMagic;
	CCRichLabel * Label_targetName;
	Button * Button_headFrame;
	Button * Button_Frame;
	ImageView * imageView_head;
	ImageView * ImageView_targetBlood;
	ImageView * ImageView_targetMagic;
	ImageView * ImageView_targetBloodGray;
	Label *  Label_targetLv;

	void initNewTargetInfo( BaseFighter * baseFighter );
	void ReloadTargetData( BaseFighter * baseFighter, bool bInitNewTarget = false);
	void ReloadBuffData();
	void ButtonTargetEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonFrameEvent(Ref *pSender, Widget::TouchEventType type);

	long long baseFighterId;
	std::string baseFightName;
	int coutryId;
	int viplevel_;
	int level_;
	int pressionId;


	UITab * Tab_target;
	bool isTabTargetOn;


public:
	float m_preScale;
	float m_nowScale;

	float m_timeDelay;

public:
	struct FriendStruct
	{
		int operation;
		int relationType;
		long long playerid;
		std::string playerName;
	};
};

//////////////////targetinfo list
class TargetInfoList : public UIScene
{
public:
	TargetInfoList();
	~TargetInfoList();

	static TargetInfoList* create(long long targetId ,std::string targetName,int countryid,int viplv,int level,int pressionId);
	bool init(long long targetId ,std::string targetName,int countryid,int viplv,int level,int pressionId);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);

	void TabTargetIndexChangedEvent(Ref * obj);
private:
	Size winSize;
	long long objId;
	std::string objName;
	int objCountryId;
	int objViplv;
	int objLv;
	int ObjPressionId;

};
#endif;

