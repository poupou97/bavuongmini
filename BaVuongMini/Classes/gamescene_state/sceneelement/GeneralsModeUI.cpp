#include "GeneralsModeUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"
#include "cocostudio\CCSGUIReader.h"


GeneralsModeUI::GeneralsModeUI(void)
{
}


GeneralsModeUI::~GeneralsModeUI(void)
{
}

GeneralsModeUI * GeneralsModeUI::create(int generalMode,int autoFight)
{
	auto generalsModeUI = new GeneralsModeUI();
	if (generalsModeUI && generalsModeUI->init(generalMode,autoFight))
	{
		generalsModeUI->autorelease();
		return generalsModeUI;
	}
	CC_SAFE_DELETE(generalsModeUI);
	return NULL;
}

bool GeneralsModeUI::init(int generalMode,int autoFight)
{
	if (UIScene::init())
	{
		//�佫ģʽ
		m_generalMode = generalMode;
		//�佫�Զ���ս
		m_autoFight = autoFight;

		Size winsize = Director::getInstance()->getVisibleSize();

		auto ppanel = (Layout *)cocostudio::GUIReader::getInstance()->widgetFromJsonFile("res_ui/wujiangmoshi_1.json");
		ppanel->setAnchorPoint(Vec2(0.0f,0.0f));
		ppanel->setPosition(Vec2::ZERO);	
		ppanel->setTouchEnabled(true);
		m_pLayer->addChild(ppanel);

		//�����
		Button_driving = (Button*)Helper::seekWidgetByName(ppanel,"Button_zhudong");
		Button_driving->setTouchEnabled(true);
		Button_driving->setPressedActionEnabled(true);
		Button_driving->addTouchEventListener(CC_CALLBACK_2(GeneralsModeUI::DrivingEvent, this));
		//����Э�
		Button_assistance = (Button*)Helper::seekWidgetByName(ppanel,"Button_xiezhu");
		Button_assistance->setTouchEnabled(true);
		Button_assistance->setPressedActionEnabled(true);
		Button_assistance->addTouchEventListener(CC_CALLBACK_2(GeneralsModeUI::AssistanceEvent, this));
		//����ս
		Button_beginAutoFight = (Button*)Helper::seekWidgetByName(ppanel,"Button_chuzhan");
		Button_beginAutoFight->setTouchEnabled(true);
		Button_beginAutoFight->setPressedActionEnabled(true);
		Button_beginAutoFight->addTouchEventListener(CC_CALLBACK_2(GeneralsModeUI::BeginAutoFightEvent, this));
		//�����ս
		Button_endAutoFight = (Button*)Helper::seekWidgetByName(ppanel,"Button_buchuzhan");
		Button_endAutoFight->setTouchEnabled(true);
		Button_endAutoFight->setPressedActionEnabled(true);
		Button_endAutoFight->addTouchEventListener(CC_CALLBACK_2(GeneralsModeUI::EndAutoFightEvent, this));

		applyGeneralMode(generalMode,autoFight);

		this->setContentSize(ppanel->getContentSize());
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);

		return true;
	}
	return false;
}


void GeneralsModeUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GeneralsModeUI::onExit()
{
	UIScene::onExit();
}


bool GeneralsModeUI::onTouchBegan(Touch *touch, Event * pEvent)
{
	return this->resignFirstResponder(touch,this,false);
}

void GeneralsModeUI::onTouchEnded(Touch *touch, Event * pEvent)
{
}

void GeneralsModeUI::onTouchCancelled(Touch *touch, Event * pEvent)
{
}

void GeneralsModeUI::onTouchMoved(Touch *touch, Event * pEvent)
{
}

void GeneralsModeUI::DrivingEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1206, (void *)2, (void *)m_autoFight);
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("general_openAutoAttack"));
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GeneralsModeUI::AssistanceEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1206, (void *)1, (void *)m_autoFight);
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("general_closeAutoAttack"));
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GeneralsModeUI::BeginAutoFightEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1206, (void *)m_generalMode, (void *)1);
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("general_openAutoOut"));
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GeneralsModeUI::EndAutoFightEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1206, (void *)m_generalMode, (void *)2);
		GameView::getInstance()->showAlertDialog(StringDataManager::getString("general_closeAutoOut"));
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}


void GeneralsModeUI::applyGeneralMode( int idx,int autoFight )
{
	m_generalMode = idx;
	m_autoFight = autoFight;

	switch(idx)
	{
	case 1:   //���
		{
			Button_driving->setVisible(true);
			Button_assistance->setVisible(false);
		}
		break;
	case 2:   //湥�
		{
			Button_driving->setVisible(false);
			Button_assistance->setVisible(true);
		}
		break;
	case 3:  //���Ϣ
		{
			Button_driving->setVisible(true);
			Button_assistance->setVisible(false);
		}
		break;
	}

	switch(autoFight)
	{
	case 1:   //�Զ�
		{
			Button_beginAutoFight->setVisible(false);
			Button_endAutoFight->setVisible(true);
		}
		break;
	case 2:   //�ֶ�
		{
			Button_beginAutoFight->setVisible(true);
			Button_endAutoFight->setVisible(false);
		}
		break;
	}
}