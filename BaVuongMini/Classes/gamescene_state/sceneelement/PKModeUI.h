

#ifndef _GAMESCENESTATE_PKMODE_H_
#define _GAMESCENESTATE_PKMODE_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class PKModeUI :public UIScene
{
public:
	PKModeUI();
	~PKModeUI();
	static PKModeUI* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);
	
	void PeaceEvent(Ref *pSender, Widget::TouchEventType type);
	void KillEvent(Ref *pSender, Widget::TouchEventType type);
	void FamilyEvent(Ref *pSender, Widget::TouchEventType type);
	void CountryEvent(Ref *pSender, Widget::TouchEventType type);
};
#endif;

