
#include "cocos2d.h"
#include "../../ui/extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCRichLabel;
class CCMoveableMenu;
class ChatCell;
typedef enum{
	TABVIEWCHATWINDOS_TAG=101
};

/**
   * �����ײ���С���촰
   **/

class ChatWindows:public UIScene, public cocos2d::extension::TableViewDataSource, public cocos2d::extension::TableViewDelegate
{
public:
	ChatWindows(void);
	~ChatWindows(void);

	static ChatWindows *create();
	bool init();
	virtual void onEnter();
	virtual void onExit();
	
	enum
	{
		chatWindowsState_show = 0,
		chatWindowsState_showUp,
		chatWindowsState_showDown
	};

	static void addToMiniChatWindow(int channel_id,std::string play_name,std::string player_country,std::string describe,long long playerid,bool isMainChat,int viplevel,long long listenerId,std::string listenerName);

	virtual void scrollViewDidScroll(cocos2d::extension::ScrollView* view);
	virtual void scrollViewDidZoom(cocos2d::extension::ScrollView* view);
	//��������¼���Լ����������һ�����
	virtual void tableCellTouched(cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell);
	//�ÿһ��Ŀ�Ⱥ͸߶
	virtual cocos2d::Size tableCellSizeForIndex(cocos2d::extension::TableView *table, unsigned int idx);
	//�����б�ÿһ������
	virtual cocos2d::extension::TableViewCell* tableCellAtIndex(cocos2d::extension::TableView *table, ssize_t idx);
	//�һ����ɶ����
	virtual ssize_t numberOfCellsInTableView(cocos2d::extension::TableView *table);


	// default implements are used to call script callback if exist
	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	void callBack(Ref * obj);	
	void showMainUi(float delta);
	void setShowMainScene(Ref * obj);
	bool showMainScene;
	static std::vector<ChatCell *>s_chatCellsDataVector;
	TableView* tableView;
	cocos2d::extension::Scale9Sprite * pImageView_chatBg;
	int remTime;
	
	void chatPrivate(Ref * obj, Widget::TouchEventType type);

	void setChatWindowsState(int value_);
	int getChatWindowsState();

private:
	Label *channel_label;
	Label *p_Name;
	Label *label_colon;
	
	int m_chatWindowsState;

	CCRichLabel *labelLink;
	int time_;
	
};

