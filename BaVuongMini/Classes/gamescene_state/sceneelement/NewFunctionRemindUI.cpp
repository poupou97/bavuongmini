#include "NewFunctionRemindUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "AppMacros.h"
#include "../../messageclient/element/CNewFunctionRemind.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../MainScene.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../ui/generals_ui/GeneralsRecuriteUI.h"
#include "../../utils/GameUtils.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../ui/Active_ui/WorldBoss_ui/WorldBossUI.h"

NewFunctionRemindUI::NewFunctionRemindUI():
time(0)
{
}


NewFunctionRemindUI::~NewFunctionRemindUI()
{
}

NewFunctionRemindUI* NewFunctionRemindUI::create(int level)
{
	auto newFunctionRemindUI = new NewFunctionRemindUI();
	if(newFunctionRemindUI && newFunctionRemindUI->init(level))
	{
		newFunctionRemindUI->autorelease();
		return newFunctionRemindUI;
	}
	CC_SAFE_DELETE(newFunctionRemindUI);
	return NULL;
}

bool NewFunctionRemindUI::init(int level)
{
	if (UIScene::init())
	{
		std::map<int, std::vector<CNewFunctionRemind*> >::const_iterator cIter;
		cIter = NewFunctionRemindConfigData::s_newFunctionRemindList.find(level);
		if (cIter == NewFunctionRemindConfigData::s_newFunctionRemindList.end()) // û�ҵ�����ָ�END��  
		{
			return false;
		}
		else
		{
			for(int i = 0;i<NewFunctionRemindConfigData::s_newFunctionRemindList[level].size();++i)
			{
				int id = NewFunctionRemindConfigData::s_newFunctionRemindList[level].at(i)->getId();
				std::string str_iconPath =  NewFunctionRemindConfigData::s_newFunctionRemindList[level].at(i)->getIcon();
				newFunctionList.insert(make_pair(id,str_iconPath));
			}
		}

		if (newFunctionList.size() <= 0)
			return false;

		Size winSize = Director::getInstance()->getVisibleSize();
		ImageView *mengban = ImageView::create();
		mengban->loadTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enabled(true);
		mengban->setContentSize(winSize);
		mengban->setAnchorPoint(Vec2(.5f,.5f));
		mengban->setPosition(Vec2(0,0));
		m_pLayer->addChild(mengban);
		
		//light
		ImageView * imageView_light = ImageView::create();
		imageView_light->loadTexture("gamescene_state/zhujiemian3/newFunction/yellowLight.png");
		imageView_light->setScale9Enabled(true);
		imageView_light->setContentSize(Size(272,94));
		imageView_light->setAnchorPoint(Vec2(0,0));
		imageView_light->setPosition(Vec2(0,0));
		m_pLayer->addChild(imageView_light);
		//bg
		ImageView * imageView_bg = ImageView::create();
		imageView_bg->loadTexture("gamescene_state/zhujiemian3/newFunction/di.png");
		imageView_bg->setAnchorPoint(Vec2(0,0));
		imageView_bg->setPosition(Vec2(0,0));
		m_pLayer->addChild(imageView_bg);
		mengban->setPosition(Vec2(imageView_bg->getContentSize().width/2,imageView_bg->getContentSize().height/2));
		//close Button
		Button* btn_close = Button::create();
		btn_close->loadTextures("res_ui/close.png","res_ui/close.png","");
		btn_close->setTouchEnabled(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_close->setPosition(Vec2(imageView_bg->getContentSize().width-5,imageView_bg->getContentSize().height-35));
		btn_close->addTouchEventListener(CC_CALLBACK_2(NewFunctionRemindUI::CloseEvent, this));
		m_pLayer->addChild(btn_close);

		//new function icon
		int i = 0;
		std::map<int, std::string>::iterator it = newFunctionList.begin();
		for ( ; it != newFunctionList.end(); ++ it )
		{
			int id = it->first;
			std::string icon = it->second;

			Button * btn_newFunction_light = Button::create();
			btn_newFunction_light->loadTextures("gamescene_state/zhujiemian3/newFunction/greenLight.png","gamescene_state/zhujiemian3/newFunction/greenLight.png","");
			btn_newFunction_light->setScale(0.9f);
			btn_newFunction_light->setTouchEnabled(true);
			btn_newFunction_light->setPressedActionEnabled(true);
			btn_newFunction_light->addTouchEventListener(CC_CALLBACK_2(NewFunctionRemindUI::BtnEvent, this));
			btn_newFunction_light->setAnchorPoint(Vec2(0.5f,0.5f));
			float space = (227 - newFunctionList.size()*btn_newFunction_light->getContentSize().width*btn_newFunction_light->getScale())*1.0f/(newFunctionList.size()+1);
			btn_newFunction_light->setPosition(Vec2(25+space*(i+1)+btn_newFunction_light->getContentSize().width*btn_newFunction_light->getScale()*(0.5f+i*1.0f),48));
			btn_newFunction_light->setTag(200+id);
			
			m_pLayer->addChild(btn_newFunction_light);

			std::string rootPath = "gamescene_state/zhujiemian3/newFunction/";
			rootPath.append(icon);
			rootPath.append(".png");
			ImageView * imageView_newFunction = ImageView::create();
			imageView_newFunction->loadTexture(rootPath.c_str());
			imageView_newFunction->setAnchorPoint(Vec2(0.5f,0.5f));
			imageView_newFunction->setPosition(Vec2(0,-1));
			btn_newFunction_light->addChild(imageView_newFunction);

			i++;
		}

		////this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(imageView_bg->getContentSize());
		//this->setTouchPriority(TPriority_ToolTips);
		//m_pLayer->setTouchPriority(TPriority_ToolTips-1);
		//this->schedule(schedule_selector(NewFunctionRemindUI::update),1.0f);

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(NewFunctionRemindUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(NewFunctionRemindUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(NewFunctionRemindUI::onTouchMoved, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}
void NewFunctionRemindUI::update( float dt )
{
	time++;
	if (time >= 3)
	{
		time = 0;
		this->closeAnim();
	}
}

void NewFunctionRemindUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void NewFunctionRemindUI::onExit()
{
	UIScene::onExit();
}

bool NewFunctionRemindUI::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	// convert the touch point to OpenGL coordinates
// 	Vec2 location = pTouch->getLocation();
// 
// 	Vec2 pos = this->getPosition();
// 	Size size = this->getContentSize();
// 	Vec2 anchorPoint = this->getAnchorPointInPoints();
// 	pos = ccpSub(pos, anchorPoint);
// 	Rect rect(pos.x, pos.y, size.width, size.height);
// 	if(rect.containsPoint(location))
// 	{
// 		return true;
// 	}
// 
// 	return false;
	//return this->resignFirstResponder(pTouch,this,false);
	return true;
}
void NewFunctionRemindUI::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void NewFunctionRemindUI::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void NewFunctionRemindUI::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void NewFunctionRemindUI::CloseEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void NewFunctionRemindUI::BtnEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		Button * btn_cur = dynamic_cast<Button*>(pSender);
		if (btn_cur)
		{
			int curID = btn_cur->getTag() - 200;
			switch (curID)
			{
			case 4:
			{
				Size s = Director::getInstance()->getVisibleSize();
				if (!MainScene::GeneralsScene)
					return;

				if (MainScene::GeneralsScene->getParent() != NULL)
				{
					MainScene::GeneralsScene->removeFromParentAndCleanup(false);
				}
				GeneralsUI * generalsUI = (GeneralsUI *)MainScene::GeneralsScene;
				GameView::getInstance()->getMainUIScene()->addChild(generalsUI, 0, kTagGeneralsUI);
				generalsUI->setIgnoreAnchorPointForPosition(false);
				generalsUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				generalsUI->setPosition(Vec2(s.width / 2, s.height / 2));

				MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
				//���ļ
				for (int i = 0; i<3; ++i)
				{
					if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50 + i))
					{
						RecuriteActionItem * tempRecurite = (RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50 + i);
						tempRecurite->cdTime -= GameUtils::millisecondNow() - tempRecurite->lastChangeTime;
					}
				}

				//generalsUI->setToDefaultTab();
				generalsUI->setToTabByIndex(1);

				//�佫�б 
				GeneralsUI::generalsListUI->isReqNewly = true;
				GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
				temp->page = 0;
				temp->pageSize = generalsUI->generalsListUI->everyPageNum;
				temp->type = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp);
				delete temp;
				//�����б
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5055, this);
				//����б
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5071, this);
				//��������佫�б 
				GeneralsUI::ReqListParameter * temp1 = new GeneralsUI::ReqListParameter();
				temp1->page = 0;
				temp1->pageSize = generalsUI->generalsListUI->everyPageNum;
				temp1->type = 5;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp1);
				delete temp1;

				mainScene->remindOfGeneral();
			}
			break;
			case 7:
			{
				Size s = Director::getInstance()->getVisibleSize();
				if (!MainScene::GeneralsScene)
					return;

				if (MainScene::GeneralsScene->getParent() != NULL)
				{
					MainScene::GeneralsScene->removeFromParentAndCleanup(false);
				}
				GeneralsUI * generalsUI = (GeneralsUI *)MainScene::GeneralsScene;
				GameView::getInstance()->getMainUIScene()->addChild(generalsUI, 0, kTagGeneralsUI);
				generalsUI->setIgnoreAnchorPointForPosition(false);
				generalsUI->setAnchorPoint(Vec2(0.5f, 0.5f));
				generalsUI->setPosition(Vec2(s.width / 2, s.height / 2));

				MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
				//���ļ
				for (int i = 0; i<3; ++i)
				{
					if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50 + i))
					{
						RecuriteActionItem * tempRecurite = (RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50 + i);
						tempRecurite->cdTime -= GameUtils::millisecondNow() - tempRecurite->lastChangeTime;
					}
				}

				//generalsUI->setToDefaultTab();
				generalsUI->setToTabByIndex(1);

				//�佫�б 
				GeneralsUI::generalsListUI->isReqNewly = true;
				GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
				temp->page = 0;
				temp->pageSize = generalsUI->generalsListUI->everyPageNum;
				temp->type = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp);
				delete temp;
				//�����б
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5055, this);
				//����б
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5071, this);
				//��������佫�б 
				GeneralsUI::ReqListParameter * temp1 = new GeneralsUI::ReqListParameter();
				temp1->page = 0;
				temp1->pageSize = generalsUI->generalsListUI->everyPageNum;
				temp1->type = 5;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp1);
				delete temp1;

				mainScene->remindOfGeneral();
			}
			break;
			case 10:
			{
				MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
				Layer *skillScene = (Layer*)mainscene->getChildByTag(KTagSkillScene);
				if (skillScene == NULL)
				{
					skillScene = SkillScene::create();
					GameView::getInstance()->getMainUIScene()->addChild(skillScene, 0, KTagSkillScene);
					skillScene->setIgnoreAnchorPointForPosition(false);
					skillScene->setAnchorPoint(Vec2(0.5f, 0.5f));
					skillScene->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				}
				mainscene->remindOfSkill();
			}
			break;
			case 12:
			{
				MainScene* mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainLayer != NULL, "should not be nil");

				Layer *worldBossUI = (Layer*)mainLayer->getChildByTag(kTagWorldBossUI);
				if (worldBossUI == NULL)
				{
					worldBossUI = WorldBossUI::create();
					GameView::getInstance()->getMainUIScene()->addChild(worldBossUI, 0, kTagWorldBossUI);
					worldBossUI->setIgnoreAnchorPointForPosition(false);
					worldBossUI->setAnchorPoint(Vec2(0.5f, 0.5f));
					worldBossUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));

					GameMessageProcessor::sharedMsgProcessor()->sendReq(5301);
				}
			}
			break;
			case 13:
			{
				MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
				Layer *skillScene = (Layer*)mainscene->getChildByTag(KTagSkillScene);
				if (skillScene == NULL)
				{
					skillScene = SkillScene::create();
					GameView::getInstance()->getMainUIScene()->addChild(skillScene, 0, KTagSkillScene);
					skillScene->setIgnoreAnchorPointForPosition(false);
					skillScene->setAnchorPoint(Vec2(0.5f, 0.5f));
					skillScene->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				}
				mainscene->remindOfSkill();
			}
			break;
			case 14:
			{
				MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
				Layer *skillScene = (Layer*)mainscene->getChildByTag(KTagSkillScene);
				if (skillScene == NULL)
				{
					skillScene = SkillScene::create();
					GameView::getInstance()->getMainUIScene()->addChild(skillScene, 0, KTagSkillScene);
					skillScene->setIgnoreAnchorPointForPosition(false);
					skillScene->setAnchorPoint(Vec2(0.5f, 0.5f));
					skillScene->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				}
				mainscene->remindOfSkill();
			}
			break;
			case 15:
			{
				MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
				Layer *skillScene = (Layer*)mainscene->getChildByTag(KTagSkillScene);
				if (skillScene == NULL)
				{
					skillScene = SkillScene::create();
					GameView::getInstance()->getMainUIScene()->addChild(skillScene, 0, KTagSkillScene);
					skillScene->setIgnoreAnchorPointForPosition(false);
					skillScene->setAnchorPoint(Vec2(0.5f, 0.5f));
					skillScene->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				}
				mainscene->remindOfSkill();
			}
			break;
			}
		}
		this->closeAnim();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}
