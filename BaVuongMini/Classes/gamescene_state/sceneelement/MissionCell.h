
#ifndef _GAMESCENESTATE_MISSIONCELL_H_
#define _GAMESCENESTATE_MISSIONCELL_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CCMoveableMenu;
class MissionInfo;
class CCRichLabel;

class MissionCell :public TableViewCell
{
public:
	MissionCell();
	~MissionCell();

	static MissionCell* Create(MissionInfo * missionInfo);
	bool init(MissionInfo * missionInfo);

	void RefreshCell(MissionInfo * missionInfo);
	void addFinishAnm();

private:
	void MissionEvent(Ref * pSender);

	Label * missionType;
	Label * missionName;
	//MenuItemFont * missionSummary;
	//Label * missionSummary;.
	CCRichLabel * missionSummary;
public:
	MissionInfo * curMissionInfo;
};

#endif;