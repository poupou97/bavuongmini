#ifndef _GAMESCENESTATE_MYPLAYERINFOMINI_H_
#define _GAMESCENESTATE_MYPLAYERINFOMINI_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

class BaseFighter;

/**
   * the blood bar on the myplaer's head
   */

class MyPlayerInfoMini : public UIScene
{
public:
	MyPlayerInfoMini();
	~MyPlayerInfoMini();

	static MyPlayerInfoMini* create(BaseFighter *baseFighter);
	bool init(BaseFighter *baseFighter);

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	ui::ImageView * ImageView_targetBlood;

	void ReloadTargetData( BaseFighter * baseFighter);
	
	long long baseFighterId;
};

#endif;

