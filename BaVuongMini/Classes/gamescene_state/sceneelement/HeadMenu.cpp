#include "HeadMenu.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "GameView.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../ui/missionscene/MissionScene.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../messageclient/element/CNpcDialog.h"
#include "../../ui/npcscene/NpcTalkWindow.h"
#include "../../ui/mapscene/MapScene.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../ui/Mail_ui/MailUI.h"
#include "../GameSceneState.h"
#include "../../ui/UISceneTest.h"
#include "../../ui/pick_ui/PickUI.h"
#include "../../ui/equipMent_ui/EquipMentUi.h"
#include "../../ui/Set_ui/SetUI.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../ui/generals_ui/GeneralsRecuriteUI.h"
#include "../MainScene.h"
#include "../../utils/GameUtils.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../messageclient/element/CActionDetail.h"
#include "../../messageclient/element/CFunctionOpenLevel.h"
#include "../../messageclient/protobuf/RecruitMessage.pb.h"
#include "../../ui/storehouse_ui/StoreHouseUI.h"
#include "../role/MyPlayer.h"
#include "../../ui/family_ui/FamilyUI.h"
#include "../../ui/generals_ui/GeneralsStrategiesUI.h"
#include "../../ui/generals_ui/GeneralsListBase.h"
#include "../../ui/family_ui/ApplyFamily.h"
#include "../../ui/family_ui/ManageFamily.h"
#include "../../utils/StaticDataManager.h"
#include "../../legend_script/UITutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../ui/robot_ui/RobotMainUI.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../messageclient/element/CGuildMemberBase.h"
#include "../../ui/Rank_ui/RankUI.h"
#include "../../legend_script/CCTeachingGuide.h"

#define FUNCTIONBUTTON_SPACE 65   //��ť��ļ�

HeadMenu::HeadMenu()
{
}


HeadMenu::~HeadMenu()
{
	std::vector<CFunctionOpenLevel*>::iterator _iter;
	for (_iter = opendedFunctionVector.begin(); _iter != opendedFunctionVector.end(); ++_iter)
	{
		delete *_iter;
	}
	opendedFunctionVector.clear();
}

bool SortByPositionIndex(CFunctionOpenLevel * fcOpenLevel1 , CFunctionOpenLevel *fcOpenLevel2)
{
	return (fcOpenLevel1->get_positionIndex() < fcOpenLevel2->get_positionIndex());
}

HeadMenu * HeadMenu::create()
{
	auto headMenu = new HeadMenu();
	if (headMenu && headMenu->init())
	{
		headMenu->autorelease();
		return headMenu;
	}
	CC_SAFE_DELETE(headMenu);
	return NULL;
}

bool HeadMenu::init()
{
	if (UIScene::init())
	{
		winSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();
		
	
		auto ImageView_bk = ImageView::create();
		ImageView_bk->loadTexture("gamescene_state/zhujiemian3/renwushuxing/kuang.png");
		ImageView_bk->setScale9Enabled(true);
		ImageView_bk->setContentSize(Size(winSize.width-43,27));
		ImageView_bk->setAnchorPoint(Vec2(0.5f,0));
		ImageView_bk->setPosition(Vec2(winSize.width/2,0));
		m_pLayer->addChild(ImageView_bk);

		auto ImageView_LeftBG = ImageView::create();
		ImageView_LeftBG->loadTexture("gamescene_state/zhujiemian3/renwushuxing/xiao.png");
		ImageView_LeftBG->setAnchorPoint(Vec2::ZERO);
		ImageView_LeftBG->setPosition(Vec2(0,-2));
		m_pLayer->addChild(ImageView_LeftBG);

		auto ImageView_RightBG = ImageView::create();
		ImageView_RightBG->loadTexture("gamescene_state/zhujiemian3/renwushuxing/xiao.png");
		ImageView_RightBG->setRotationSkewY(180);
		ImageView_RightBG->setAnchorPoint(Vec2::ZERO);
		ImageView_RightBG->setPosition(Vec2(winSize.width,-2));
		m_pLayer->addChild(ImageView_RightBG);
		//roleInfo
		btn_roleIf = Button::create();
		btn_roleIf->setTouchEnabled(true);
		btn_roleIf->loadTextures("gamescene_state/zhujiemian3/renwushuxing/renwua.png","gamescene_state/zhujiemian3/renwushuxing/renwua.png","");
		btn_roleIf->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_roleIf->setPosition(Vec2(72,28));
		btn_roleIf->addTouchEventListener(CC_CALLBACK_2(HeadMenu::ButtonRoleInfoEvent, this));
		btn_roleIf->setPressedActionEnabled(true);
		btn_roleIf->setName("btn_roleIf");
		btn_roleIf->setVisible(false);
		m_pLayer->addChild(btn_roleIf);
		//skill
		btn_skill = Button::create();
		btn_skill->setTouchEnabled(true);
		btn_skill->loadTextures("gamescene_state/zhujiemian3/renwushuxing/skill.png","gamescene_state/zhujiemian3/renwushuxing/skill.png","");
		btn_skill->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_skill->setPosition(Vec2(btn_roleIf->getPosition().x+btn_roleIf->getContentSize().width+10,btn_roleIf->getPosition().y));
		btn_skill->addTouchEventListener(CC_CALLBACK_2(HeadMenu::ButtonSkillEvent, this));
		btn_skill->setPressedActionEnabled(true);
		btn_skill->setName("btn_skill");
		btn_skill->setVisible(false);
		m_pLayer->addChild(btn_skill);
		//strengthen
		btn_strengthen  = Button::create();
		btn_strengthen->setTouchEnabled(true);
		btn_strengthen->loadTextures("gamescene_state/zhujiemian3/renwushuxing/qianghua.png","gamescene_state/zhujiemian3/renwushuxing/qianghua.png","");
		btn_strengthen->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_strengthen->setPosition(Vec2(btn_skill->getPosition().x+btn_skill->getContentSize().width+10,btn_roleIf->getPosition().y));
		btn_strengthen->addTouchEventListener(CC_CALLBACK_2(HeadMenu::ButtonStrengthenEvent, this));
		btn_strengthen->setPressedActionEnabled(true);
		btn_strengthen->setName("btn_strengthen");
		btn_strengthen->setVisible(false);
		m_pLayer->addChild(btn_strengthen);

		//generals
		btn_generals = Button::create();
		btn_generals->setTouchEnabled(true);
		btn_generals->loadTextures("gamescene_state/zhujiemian3/renwushuxing/generals.png","gamescene_state/zhujiemian3/renwushuxing/generals.png","");
		btn_generals->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_generals->setPosition(Vec2(btn_strengthen->getPosition().x+btn_strengthen->getContentSize().width+10,btn_roleIf->getPosition().y));
		btn_generals->addTouchEventListener(CC_CALLBACK_2(HeadMenu::ButtonGeneralsEvent,this));
		btn_generals->setPressedActionEnabled(true);
		btn_generals->setName("btn_generals");
		btn_generals->setVisible(false);
		m_pLayer->addChild(btn_generals);
		//mission
		btn_mission  = Button::create();
		btn_mission->setTouchEnabled(true);
		btn_mission->loadTextures("gamescene_state/zhujiemian3/renwushuxing/renwu.png","gamescene_state/zhujiemian3/renwushuxing/renwu.png","");
		btn_mission->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_mission->setPosition(Vec2(btn_generals->getPosition().x+btn_generals->getContentSize().width+10,btn_roleIf->getPosition().y));
		btn_mission->addTouchEventListener(CC_CALLBACK_2(HeadMenu::ButtonMissionEvent, this));
		btn_mission->setPressedActionEnabled(true);
		btn_mission->setName("btn_mission");
		btn_mission->setVisible(false);
		m_pLayer->addChild(btn_mission);
		//mount ���� 
		btn_mount = Button::create();
		btn_mount->setTouchEnabled(true);
		btn_mount->loadTextures("gamescene_state/zhujiemian3/renwushuxing/zuoqi.png","gamescene_state/zhujiemian3/renwushuxing/zuoqi.png","");
		btn_mount->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_mount->setPosition(Vec2(btn_mission->getPosition().x+btn_mission->getContentSize().width+10,btn_roleIf->getPosition().y));
		btn_mount->addTouchEventListener(CC_CALLBACK_2(HeadMenu::ButtonMountEvent, this));
		btn_mount->setPressedActionEnabled(true);
		btn_mount->setName("btn_mount");
		btn_mount->setVisible(false);
		m_pLayer->addChild(btn_mount);
		//friend
		Button_friend = Button::create();
		Button_friend->setTouchEnabled(true);
		Button_friend->loadTextures("gamescene_state/zhujiemian3/renwushuxing/haoyou.png","gamescene_state/zhujiemian3/renwushuxing/haoyou.png","");
		Button_friend->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_friend->setPosition(Vec2(btn_mount->getPosition().x+btn_mount->getContentSize().width+10,btn_roleIf->getPosition().y));
		Button_friend->addTouchEventListener(CC_CALLBACK_2(HeadMenu::ButtonFriendEvent, this));
		Button_friend->setPressedActionEnabled(true);
		Button_friend->setName("btn_friend");
		Button_friend->setVisible(false);
		m_pLayer->addChild(Button_friend);
		//mail
		Button_mail = Button::create();
		Button_mail->setTouchEnabled(true);
		Button_mail->loadTextures("gamescene_state/zhujiemian3/renwushuxing/mail.png","gamescene_state/zhujiemian3/renwushuxing/mail.png","");
		Button_mail->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_mail->setPosition(Vec2(Button_friend->getPosition().x+Button_friend->getContentSize().width+10,btn_roleIf->getPosition().y));
		Button_mail->addTouchEventListener(CC_CALLBACK_2(HeadMenu::ButtonMailEvent, this));
		Button_mail->setPressedActionEnabled(true);
		Button_mail->setName("btn_mail");
		Button_mail->setVisible(false);
		m_pLayer->addChild(Button_mail);
		//family
		Button_family = Button::create();
		Button_family->setTouchEnabled(true);
		Button_family->loadTextures("gamescene_state/zhujiemian3/renwushuxing/jiazu.png","gamescene_state/zhujiemian3/renwushuxing/jiazu.png","");
		Button_family->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_family->setPosition(Vec2(Button_mail->getPosition().x+Button_mail->getContentSize().width+10,btn_roleIf->getPosition().y));
		Button_family->addTouchEventListener(CC_CALLBACK_2(HeadMenu::ButtonFamilyEvent, this));
		Button_family->setPressedActionEnabled(true);
		Button_family->setName("btn_family");
		Button_family->setVisible(false);
		m_pLayer->addChild(Button_family);
		//ranking
		Button_rankings = Button::create();
		Button_rankings->setTouchEnabled(true);
		Button_rankings->loadTextures("gamescene_state/zhujiemian3/renwushuxing/paihangbang.png","gamescene_state/zhujiemian3/renwushuxing/paihangbang.png","");
		Button_rankings->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_rankings->setPosition(Vec2(Button_mail->getPosition().x+Button_mail->getContentSize().width+10,btn_roleIf->getPosition().y));
		Button_rankings->addTouchEventListener(CC_CALLBACK_2(HeadMenu::ButtonRankingsEvent, this));
		Button_rankings->setPressedActionEnabled(true);
		Button_rankings->setName("btn_rankings");
		Button_rankings->setVisible(false);
		m_pLayer->addChild(Button_rankings);
		//hangUp
		Button_hangUp = Button::create();
		Button_hangUp->setTouchEnabled(true);
		Button_hangUp->loadTextures("gamescene_state/zhujiemian3/renwushuxing/guaji.png","gamescene_state/zhujiemian3/renwushuxing/guaji.png","");
		Button_hangUp->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_hangUp->setPosition(Vec2(Button_family->getPosition().x+Button_family->getContentSize().width+10,btn_roleIf->getPosition().y));
		Button_hangUp->addTouchEventListener(CC_CALLBACK_2(HeadMenu::ButtonHangUpEvent, this));
		Button_hangUp->setPressedActionEnabled(true);
		Button_hangUp->setName("btn_hangUpSet");
		Button_hangUp->setVisible(false);
		m_pLayer->addChild(Button_hangUp);
		//set
		auto Button_set = Button::create();
		Button_set->setTouchEnabled(true);
		Button_set->loadTextures("gamescene_state/zhujiemian3/renwushuxing/set_up.png","gamescene_state/zhujiemian3/renwushuxing/set_up.png","");
		Button_set->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_set->setPosition(Vec2(Button_hangUp->getPosition().x+Button_hangUp->getContentSize().width+10,btn_roleIf->getPosition().y));
		Button_set->addTouchEventListener(CC_CALLBACK_2(HeadMenu::ButtonSetEvent, this));
		Button_set->setPressedActionEnabled(true);
		Button_set->setName("btn_set");
		Button_set->setVisible(false);
		m_pLayer->addChild(Button_set);

		int roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
		for (int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();i++)
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() == 1)       //��ǵײ��˵��
			{
				if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel()<= roleLevel)     //������ʾ
				{
					auto tempItem = new CFunctionOpenLevel();
					tempItem->set_functionId(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId());
					tempItem->set_functionName(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionName());
					tempItem->set_functionType(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType());
					tempItem->set_requiredLevel(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel());
					tempItem->set_positionIndex(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_positionIndex());
					opendedFunctionVector.push_back(tempItem);
				}
			}
		}
		sort(opendedFunctionVector.begin(),opendedFunctionVector.end(),SortByPositionIndex);
		for (int i = 0;i<opendedFunctionVector.size();i++)
		{
			if (m_pLayer->getChildByName(opendedFunctionVector.at(i)->get_functionId().c_str()))
			{
				(m_pLayer->getChildByName(opendedFunctionVector.at(i)->get_functionId().c_str()))->setVisible(true);
				(m_pLayer->getChildByName(opendedFunctionVector.at(i)->get_functionId().c_str()))->setPosition(Vec2(75+FUNCTIONBUTTON_SPACE*i*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH),28));
			}
		}

		this->setIgnoreAnchorPointForPosition(false);
		this->setAnchorPoint(Vec2(0,0));
		////this->setTouchEnabled(false);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(winSize.width,58));

		return true;
	}
	return false;
}

void HeadMenu::onEnter()
{
	UIScene::onEnter();
// 	Action * action = MoveTo::create(0.3f,Vec2(this->getPositionX(),this->getPositionY()+this->getContentSize().height));
// 	runAction(action);
}
void HeadMenu::onExit()
{
	UIScene::onExit();
}	

bool HeadMenu::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	return false;
}
void HeadMenu::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void HeadMenu::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void HeadMenu::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void HeadMenu::ButtonRoleInfoEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);
		CCLOG("ButtonPackageInfoEvent...");
		auto backageLayer = (PackageScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBackpack);
		if (backageLayer == NULL)
		{
			backageLayer = PackageScene::create();

			//Size s = Director::getInstance()->getVisibleSize();

			GameView::getInstance()->getMainUIScene()->addChild(backageLayer, 0, kTagBackpack);

			backageLayer->setIgnoreAnchorPointForPosition(false);
			backageLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
			backageLayer->setPosition(Vec2(winSize.width / 2, winSize.height / 2));

			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
			temp->page = 0;
			temp->pageSize = backageLayer->everyPageNum;
			temp->type = 1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp);
			delete temp;
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}



}

void HeadMenu::ButtonStrengthenEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//��ʱ���ͼ������
		// 	Layer *mapLayer = (Layer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMap);
		// 	if(mapLayer == NULL)
		// 	{
		// 		mapLayer = MapScene::create();
		// 
		// 		Size s = Director::getInstance()->getVisibleSize();
		// 
		// 		mapLayer->autorelease();
		// 		GameView::getInstance()->getMainUIScene()->addChild(mapLayer,0,kTagMap);
		// 
		// 		mapLayer->setIgnoreAnchorPointForPosition(false);
		// 		mapLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
		// 		mapLayer->setPosition(Vec2(s.width/2, s.height/2));
		// 	}

		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		long long currentTimeCreateUI = GameUtils::millisecondNow();
		auto equipmentUI = (EquipMentUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagEquipMentUI);
		if (equipmentUI == NULL)
		{
			auto equip = EquipMentUi::create();
			equip->setIgnoreAnchorPointForPosition(false);
			equip->setAnchorPoint(Vec2(0.5f, 0.5f));
			equip->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			GameView::getInstance()->getMainUIScene()->addChild(equip, 0, ktagEquipMentUI);
			equip->refreshBackPack(0, -1);
		}
		CCLOG("create equipMainUi cost time: %d", GameUtils::millisecondNow() - currentTimeCreateUI);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}


void HeadMenu::ButtonMountEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		// 	Size winSize=Director::getInstance()->getVisibleSize();
		// 	UISceneTest * scenetest =UISceneTest::create();
		// 	scenetest->setIgnoreAnchorPointForPosition(false);
		// 	scenetest->setAnchorPoint(Vec2(0.5f,0.5f));
		// 	scenetest->setPosition(Vec2(winSize.width/2,winSize.height/2));
		// 	GameView::getInstance()->getMainUIScene()->addChild(scenetest,4000);

		//GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void HeadMenu::ButtonMissionEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		auto missionMainLayer = (Layer*)mainscene->getChildByTag(kTagMissionScene);
		if (missionMainLayer == NULL)
		{
			missionMainLayer = MissionScene::create();

			//Size s = Director::getInstance()->getVisibleSize();

			GameView::getInstance()->getMainUIScene()->addChild(missionMainLayer, 0, kTagMissionScene);

			missionMainLayer->setIgnoreAnchorPointForPosition(false);
			missionMainLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
			missionMainLayer->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		}
		mainscene->remindMission();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void HeadMenu::ButtonFriendEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		auto friendLayer = (Layer*)mainscene->getChildByTag(kTagFriendUi);
		if (friendLayer == NULL)
		{
			auto friendui = FriendUi::create();
			friendui->setIgnoreAnchorPointForPosition(false);
			friendui->setAnchorPoint(Vec2(0.5f, 0.5f));
			friendui->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			GameView::getInstance()->getMainUIScene()->addChild(friendui, 0, kTagFriendUi);

			FriendStruct friend1 = { 0,20,0,0 };
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2202, &friend1);

			mainscene->remindGetFriendPhypower();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void HeadMenu::ButtonFamilyEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		long long roleGuildId_ = GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionid();
		if (roleGuildId_ == 0)
		{
			const char *strings = StringDataManager::getString("family_enter_gm");
			char * str = const_cast<char*>(strings);
			GameView::getInstance()->showAlertDialog(str);
		}
		else
		{
			auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
			auto familyLayer = (Layer*)mainscene->getChildByTag(kTagFamilyUI);
			if (familyLayer == NULL)
			{
				FamilyUI * familyui = FamilyUI::create();
				familyui->setIgnoreAnchorPointForPosition(false);
				familyui->setAnchorPoint(Vec2(0.5f, 0.5f));
				familyui->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				familyui->setTag(kTagFamilyUI);
				GameView::getInstance()->getMainUIScene()->addChild(familyui);
				GameMessageProcessor::sharedMsgProcessor()->sendReq(1500, NULL);

				mainscene->remindFamilyApply();
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void HeadMenu::ButtonRankingsEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		// ڳ�ʼ�� UI WINDOW��
		Size winSize = Director::getInstance()->getVisibleSize();

		auto pTmpRankUI = (RankUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRankUI);
		if (NULL == pTmpRankUI)
		{
			auto pRankUI = RankUI::create();
			pRankUI->setIgnoreAnchorPointForPosition(false);
			pRankUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			pRankUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			pRankUI->setTag(kTagRankUI);

			GameView::getInstance()->getMainUIScene()->addChild(pRankUI);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void HeadMenu::ButtonSkillEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);
		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		auto skillScene = (Layer*)mainscene->getChildByTag(KTagSkillScene);
		if (skillScene == NULL)
		{
			//Size winSize = Director::getInstance()->getVisibleSize();
			skillScene = SkillScene::create();
			GameView::getInstance()->getMainUIScene()->addChild(skillScene, 0, KTagSkillScene);
			skillScene->setIgnoreAnchorPointForPosition(false);
			skillScene->setAnchorPoint(Vec2(0.5f, 0.5f));
			skillScene->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		}
		mainscene->remindOfSkill();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void HeadMenu::ButtonGeneralsEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGeneralsUI))
			return;

		Size s = Director::getInstance()->getVisibleSize();
		if (!MainScene::GeneralsScene)
			return;

		if (MainScene::GeneralsScene->getParent() != NULL)
		{
			CCLOG("MainScene::GeneralsScene->getParent() != NULL");
			MainScene::GeneralsScene->removeFromParentAndCleanup(false);
		}
		//CCLOG("(GeneralsUI *)MainScene::GeneralsScene");
		GeneralsUI * generalsUI = (GeneralsUI *)MainScene::GeneralsScene;
		GameView::getInstance()->getMainUIScene()->addChild(generalsUI, 0, kTagGeneralsUI);
		generalsUI->setIgnoreAnchorPointForPosition(false);
		generalsUI->setAnchorPoint(Vec2(0.5f, 0.5f));
		generalsUI->setPosition(Vec2(s.width / 2, s.height / 2));

		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		// 	if (mainScene->generalHeadLayer->getChildByTag(66))
		// 	{
		// 		GeneralsHeadManager * generalsHeadManager = (GeneralsHeadManager*)mainScene->generalHeadLayer->getChildByTag(66);
		// 		generalsHeadManager->setGeneralHeadTouchEnable(false);
		// 	}


		//���ļ
		for (int i = 0; i<3; ++i)
		{
			if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50 + i))
			{
				RecuriteActionItem * tempRecurite = (RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50 + i);
				tempRecurite->cdTime -= GameUtils::millisecondNow() - tempRecurite->lastChangeTime;
			}
		}

		generalsUI->setToDefaultTab();
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
		{
			sc->endCommand(pSender);
			//generalsUI->setToTabByIndex(1);
			//GeneralsUI::generalsListUI->isInFirstGeneralTutorial = true;
			if (GeneralsUI::generalsListUI->isInFirstGeneralTutorial)
			{
				generalsUI->setToTabByIndex(1);
			}
		}

		//�佫�б 
		GeneralsUI::generalsListUI->isReqNewly = true;
		GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
		temp->page = 0;
		temp->pageSize = generalsUI->generalsListUI->everyPageNum;
		temp->type = 1;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp);
		delete temp;
		//�����б
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5055, this);
		//����б
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5071, this);
		//��������佫�б 
		GeneralsUI::ReqListParameter * temp1 = new GeneralsUI::ReqListParameter();
		temp1->page = 0;
		temp1->pageSize = generalsUI->generalsListUI->everyPageNum;
		temp1->type = 5;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp1);
		delete temp1;

		mainScene->remindOfGeneral();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void HeadMenu::ButtonMailEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		auto mailLayer = (Layer*)mainscene->getChildByTag(kTagMailUi);
		if (mailLayer == NULL)
		{
			MailUI * mailui = MailUI::create();
			mailui->setIgnoreAnchorPointForPosition(false);
			mailui->setAnchorPoint(Vec2(0.5f, 0.5f));
			mailui->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			GameView::getInstance()->getMainUIScene()->addChild(mailui, 0, kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001, (void *)mailui->curMailPage);
		}
		mainscene->remindMail();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void HeadMenu::ButtonHangUpEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);
		auto mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
		CCAssert(mainLayer != NULL, "should not be nil");

		auto robotLayer = (Layer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
		if (robotLayer == NULL)
		{
			auto _ui = RobotMainUI::create();
			_ui->setIgnoreAnchorPointForPosition(false);
			_ui->setAnchorPoint(Vec2(0.5f, 0.5f));
			_ui->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			_ui->setTag(kTagRobotUI);
			mainLayer->addChild(_ui);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void HeadMenu::ButtonSetEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		auto setLayer = (Layer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagSetUI);
		if (setLayer == NULL)
		{
			auto setui = SetUI::create();
			setui->setIgnoreAnchorPointForPosition(false);
			setui->setAnchorPoint(Vec2(0.5f, 0.5f));
			setui->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			setui->setTag(kTagSetUI);
			GameView::getInstance()->getMainUIScene()->addChild(setui);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}



}

void HeadMenu::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void HeadMenu::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(Vec2(pos.x+60,pos.y+80));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,55,55,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x,pos.y+12));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void HeadMenu::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void HeadMenu::addCCTutorialIndicatorRD( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction  )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(Vec2(pos.x-40,pos.y+80));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,55,55,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x,pos.y+12));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}



void HeadMenu::addNewFunction( Node * pNode,void * functionOpenLevel )
{
	Size winSize = Director::getInstance()->getVisibleSize();

	int lastIndex = -5;
	for (int i = 0;i<opendedFunctionVector.size();i++)
	{
		if (opendedFunctionVector.at(i)->get_positionIndex() > ((CFunctionOpenLevel * )functionOpenLevel)->get_positionIndex())
		{
			if (lastIndex == -5)
				lastIndex = i-1;

			if (m_pLayer->getChildByName(opendedFunctionVector.at(i)->get_functionId().c_str()))
			{
				auto  action = Sequence::create(
					MoveBy::create(0.5f,Vec2(FUNCTIONBUTTON_SPACE*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH),0)),
					NULL);
				(m_pLayer->getChildByName(opendedFunctionVector.at(i)->get_functionId().c_str()))->runAction(action);
			}
		}
	}

	if(lastIndex == -5)
	{
		if (m_pLayer->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))
		{
			(m_pLayer->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setVisible(true);
			(m_pLayer->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setPosition(Vec2(75+FUNCTIONBUTTON_SPACE*(opendedFunctionVector.size()*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH)),28));
		}
	}
	else
	{
		if (m_pLayer->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))
		{
			(m_pLayer->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setVisible(true);
			(m_pLayer->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setPosition(Vec2(75+FUNCTIONBUTTON_SPACE*(lastIndex + 1)*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH),28));
		}
	}


	auto temp = new CFunctionOpenLevel();
	temp->set_functionId(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId());
	temp->set_functionName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionName());
	temp->set_functionType(((CFunctionOpenLevel * )functionOpenLevel)->get_functionType());
	temp->set_requiredLevel(((CFunctionOpenLevel * )functionOpenLevel)->get_requiredLevel());
	temp->set_positionIndex(((CFunctionOpenLevel * )functionOpenLevel)->get_positionIndex());
	opendedFunctionVector.push_back(temp);
	sort(opendedFunctionVector.begin(),opendedFunctionVector.end(),SortByPositionIndex);
}


