#include "SceneTest.h"
#include "GameView.h"
#include "../../loadscene_state/LoadSceneState.h"
#include "../../messageclient/element/CMapInfo.h"


SceneTest::SceneTest()
{
	testString[0]="xsc.level";
	testString[1]="cac.level";
	testString[2]="gd.level";
}


SceneTest::~SceneTest()
{
}

SceneTest* SceneTest::create()
{
	auto sceneTest = new SceneTest();
	if (sceneTest && sceneTest->init())
	{
		sceneTest->autorelease();
		return sceneTest;
	}
	CC_SAFE_DELETE(sceneTest);
	return NULL;
}

bool SceneTest::init()
{
	if (UIScene::init())
	{
		//�������
		//////////////////������/////////////////
		Size winsize = Director::getInstance()->getVisibleSize();

		auto testBg = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/di.png");
		testBg->setAnchorPoint(Vec2::ZERO);
		testBg->setPosition(Vec2(0,0));
		testBg->setContentSize(Size(132,193));
		testBg->setScaleY(1.3f);
		testBg->setScaleX(0.8f);
		addChild(testBg);

		auto testTableView = TableView::create(this,Size(132,193));
		testTableView->setDirection(TableView::Direction::VERTICAL);
		testTableView->setAnchorPoint(Vec2::ZERO);
		testTableView->setPosition(Vec2(0,0));
		testTableView->setDelegate(this);
		testTableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		addChild(testTableView);

		this->setIgnoreAnchorPointForPosition(false);
		this->setAnchorPoint(Vec2::ZERO);
		this->setPosition(Vec2::ZERO);
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(132,193));

		//////////////////�������/////////////////
		return true;
	}
	return false;
}


void SceneTest::onEnter()
{
	UIScene::onEnter();
}
void SceneTest::onExit()
{
	UIScene::onExit();
}

bool SceneTest::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	Vec2 location = pTouch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		return true;
	}
	return false;
}
void SceneTest::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void SceneTest::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void SceneTest::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}


////////////////�������/////////////////////
Size SceneTest::tableCellSizeForIndex(TableView *table, unsigned int idx)
{
	return Size(132, 35);
}

TableViewCell* SceneTest::tableCellAtIndex(TableView *table, ssize_t  idx)
{
	__String *string = __String::createWithFormat("%d", idx);
	auto cell = table->dequeueCell();   // this method must be called
	cell=new TableViewCell();
	cell->autorelease();

	auto testCell = Label::createWithTTF(testString[idx].c_str(),"Arial",18);
	Color3B _color=Color3B(255,255,255);
	testCell->setColor(_color);
	testCell->setAnchorPoint(Vec2::ZERO);
	testCell->setPosition(Vec2(20,0));
	cell->addChild(testCell);


	return cell;

}

ssize_t SceneTest::numberOfCellsInTableView(TableView *table)
{
	return 3;
}

void SceneTest::tableCellTouched(TableView* table, TableViewCell* cell)
{
	CCLOG("this.cell.id = %d",cell->getIdx());
	Director::getInstance()->purgeCachedData();
	GameView::getInstance()->getMapInfo()->set_mapid(testString[cell->getIdx()]);
	// create the next scene and run it
	auto pScene = new LoadSceneState();
	if (pScene)
	{
		pScene->runThisState();
		pScene->release();
	}

}

void SceneTest::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
}

void SceneTest::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

////////////////�������/////////////////////
