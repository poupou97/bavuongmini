
#ifndef _GAMESCENESTATE_MYHEADINFO_H_
#define _GAMESCENESTATE_MYHEADINFO_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../legend_script/ScriptHandlerProtocol.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class MainScene;
class UITab;

class MyHeadInfo : public UIScene, public ScriptHandlerProtocol
{
public:
	MyHeadInfo();
	~MyHeadInfo();
	static MyHeadInfo* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	ImageView * ImageView_vipLv;
	Label * Label_allBlood;
	Label * Label_curBlood;
	Label * Label_allMagic;
	Label * Label_curMagic;
	Label * Label_allPhy;
	Label * Label_curPhy;
	Label * Label_name;
	Label * Label_level;
	Button * Button_headFrame;
	ImageView * ImageView_blood;
	ImageView * ImageView_magic;
	ImageView * ImageView_phy;

	ImageView * upBuff;
	ImageView * downBuff;

	ImageView * ImageView_Vip;
	Label * lbt_vipLevel;

	UITab * Tab_state;
	bool isTabStateOn;

	Button *Button_state;
	int pkModeOpenLevel;

	void applyPKMode(int PKMode);
	void TabStateIndexChangedEvent(Ref* pSender);
	void ButtonStateEvent(Ref *pSender, Widget::TouchEventType type);

	void ReloadMyPlayerData();
	void ReloadBuffData();
	void ButtonHeadEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonFrameEvent(Ref *pSender, Widget::TouchEventType type);
	void ButtonLVTeamEvent(Ref *pSender);
	void ReloadVipInfo();

	/////////team button (change by liuzhenxing)
	void callBackExitTeam(Ref * obj);
	Button * teamInLine;

	//��ѧ
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	//��ƽ
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

private:
	MainScene * mainScene;
	ImageView * stateBG;

	Layer * layer_pkmode;
	Layer * layer_headIndo;
	//��ѧ
	int mTutorialScriptInstanceId;
};

#endif;

