#include "ChatWindows.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../ui/extensions/RichTextInput.h"
#include "../../ui/Chat_ui/ChatUI.h"
#include "../GameSceneState.h"
#include "GameView.h"
#include "../../ui/Chat_ui/ChatCell.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/extensions/UITab.h"
#include "../../ui/Chat_ui/PrivateChatUi.h"
#include "../../legend_script/CCTutorialParticle.h"

#define CHATWINDOW_PREFERRED_WIDTH 260   // 330
#define CHATWINDOW_PREFERRED_HEIGHT 60
#define CHATWINDOW_MAX_HEIGHT 66

ChatWindows::ChatWindows(void)
{
	showMainScene=true;
	m_chatWindowsState = chatWindowsState_showUp;
	remTime = 45;
	time_=0;
}

ChatWindows::~ChatWindows(void)
{
}

ChatWindows * ChatWindows::create()
{
	auto chatWindows=new ChatWindows();
	if (chatWindows && chatWindows->init())
	{
		chatWindows->autorelease();
		return chatWindows;
	}
	CC_SAFE_DELETE(chatWindows);
	return NULL;

}
bool ChatWindows::init()
{
	if (UIScene::init())
	{
		Size winSize=Director::getInstance()->getVisibleSize();
		auto m_Layer= Layer::create();
		m_Layer->setIgnoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(Vec2(0.5f,0.5f));
		m_Layer->setPosition(Vec2(winSize.width/2,winSize.height/2));
		m_Layer->setContentSize(Size(UI_DESIGN_RESOLUTION_WIDTH, UI_DESIGN_RESOLUTION_HEIGHT));
		this->addChild(m_Layer);
		
		pImageView_chatBg = cocos2d::extension::Scale9Sprite::create(Rect(7, 7, 1, 1) , "res_ui/zhezhao70.png");
		pImageView_chatBg->setOpacity(160);
		pImageView_chatBg->setPreferredSize(Size(CHATWINDOW_PREFERRED_WIDTH, CHATWINDOW_PREFERRED_HEIGHT));
		pImageView_chatBg->setAnchorPoint(Vec2::ZERO);
		//pImageView_chatBg->setPosition(Vec2(58,3));   // 0
		pImageView_chatBg->setPosition(Vec2(0,3));
		addChild(pImageView_chatBg);

		tableView = TableView::create(this, Size(255, 75));  //390,75
		tableView->setDirection(TableView::Direction::VERTICAL);
		tableView->setAnchorPoint(Vec2(0,0));
		//tableView->setPosition(Vec2(63,0));   // 60
		tableView->setPosition(Vec2(5,0));   // 60
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
		tableView->setTouchEnabled(false);
		addChild(tableView, 1,TABVIEWCHATWINDOS_TAG);
		tableView->reloadData();

		schedule(schedule_selector(ChatWindows::showMainUi),0.5f);
		
		////setTouchEnabled(true);
		setContentSize(Size(CHATWINDOW_PREFERRED_WIDTH,CHATWINDOW_PREFERRED_HEIGHT));
		return true;
	}
	return false;
}
void ChatWindows::onEnter()
{
	UIScene::onEnter();
}
void ChatWindows::onExit()
{
	UIScene::onExit();
}

void ChatWindows::callBack( Ref * obj )
{
	Size s=Director::getInstance()->getVisibleSize();
	auto chat_ui = ChatUI::create();
	chat_ui->setIgnoreAnchorPointForPosition(false);
	chat_ui->setAnchorPoint(Vec2(0.5f, 0.5f));
	chat_ui->setPosition(Vec2(s.width/2,s.height/2));
	this->getParent()->addChild(chat_ui,0,kTabChat);
}

void ChatWindows::tableCellTouched( cocos2d::extension::TableView* table, cocos2d::extension::TableViewCell* cell )
{
	CCLOG("cell touched at index: %i", cell->getIdx());
}

cocos2d::Size ChatWindows::tableCellSizeForIndex( cocos2d::extension::TableView *table, unsigned int idx )
{
	int height= s_chatCellsDataVector.at(idx)->getContentSize().height*0.6f;
	return Size(400,height);  //400,height
}

cocos2d::extension::TableViewCell* ChatWindows::tableCellAtIndex( cocos2d::extension::TableView *table, ssize_t idx )
{
	auto cell =table->dequeueCell();   // this method must be called
	cell = s_chatCellsDataVector.at(idx);
	cell->setScale(0.6f);
	return cell;
}

ssize_t ChatWindows::numberOfCellsInTableView( cocos2d::extension::TableView *table )
{
	int cellLine=s_chatCellsDataVector.size();
	return cellLine;
}

bool ChatWindows::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return false;
}

void ChatWindows::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void ChatWindows::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void ChatWindows::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void ChatWindows::scrollViewDidScroll( cocos2d::extension::ScrollView* view )
{

}

void ChatWindows::scrollViewDidZoom( cocos2d::extension::ScrollView* view )
{

}


void ChatWindows::showMainUi( float delta )
{
	remTime-=delta;
	auto mainScene =(MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainScene == NULL)
	{
		return;
	}
	Button * btn_chatBubble = NULL;
	if (mainScene->chatLayer->getChildByTag(100))//#define BUTTONLAYER_TAG = 100 is mainscene define
	{
		auto layer_ = (Layer *)mainScene->chatLayer->getChildByTag(100);
		if (layer_->getChildByTag(101))
		{
			btn_chatBubble = (Button *)layer_->getChildByTag(101);//#define CHATBUTTON_TAG = 101 is mainscene define
		}
	}

	if (remTime <= 0)
	{
		remTime = 0;
		setShowMainScene(NULL);

		//set chatBtton is setOpacity
		if (btn_chatBubble)
		{
			btn_chatBubble->setOpacity(255);
		}
		
	}else
	{
		switch (this->getChatWindowsState())
		{
		case chatWindowsState_show:
			{
				//set chatBtton is setOpacity
				if (btn_chatBubble)
				{
					btn_chatBubble->setOpacity(100);
				}
				return;
			}break;
		case chatWindowsState_showUp:
			{
				if (showMainScene == true )
				{
					if (mainScene->isHeadMenuOn==true)
					{
						this->runAction(MoveTo::create(0.5f,Vec2(this->getPositionX(),this->getContentSize().height/2+10)));
					}else
					{
						this->runAction(MoveTo::create(0.5f,Vec2(this->getPositionX(),this->getContentSize().height/2+10)));
					}
					this->setChatWindowsState(chatWindowsState_show);
				}
			};
		}
	}

	

	/*
	MainScene * mainScene =(MainScene *)this->getParent();
	if (showMainScene==true )
	{
		if (this->getActionByTag(2204) != NULL)
		{
			return;
		}
		
		if (mainScene->isHeadMenuOn==true)
		{
			ActionInterval * action =(ActionInterval *)Sequence::create(
				MoveTo::create(0.5f,Vec2(this->getPositionX(),77)),
				DelayTime::create(45.0f),
				CallFuncO::create(this,callfuncO_selector(ChatWindows::setShowMainScene),NULL),
				NULL);
			action->setTag(2204);
			this->runAction(action);
		}else
		{
			ActionInterval * action =(ActionInterval *)Sequence::create(
				MoveTo::create(0.5f,Vec2(this->getPositionX(),8)),
				DelayTime::create(45.0f),
				CallFuncO::create(this,callfuncO_selector(ChatWindows::setShowMainScene),NULL),
					NULL);
			action->setTag(2204);
			this->runAction(action);
		}
	}
	*/
}

void ChatWindows::setShowMainScene(Ref * obj)
{
	this->runAction(MoveTo::create(0.5f,Vec2(this->getPositionX(),-122)));	
	showMainScene=false;
	this->setChatWindowsState(chatWindowsState_showDown);
}

void ChatWindows::chatPrivate( Ref * obj, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		int m_vectorSize = GameView::getInstance()->chatPrivateSpeakerVector.size();
		auto privateUi = PrivateChatUi::create(m_vectorSize - 1);
		privateUi->setIgnoreAnchorPointForPosition(false);
		privateUi->setAnchorPoint(Vec2(0.5f, 0.5f));
		privateUi->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		privateUi->setTag(ktagPrivateUI);
		GameView::getInstance()->getMainUIScene()->addChild(privateUi);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	
}

std::vector<ChatCell *> ChatWindows::s_chatCellsDataVector;

void ChatWindows::addToMiniChatWindow( int channel_id,std::string play_name,std::string player_country,std::string describe,long long playerid,bool isMainChat,int viplevel,long long listenerId,std::string listenerName )
{
	auto scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	// when the chat main window is opened, ignore the mini chat window at the bottom of the main ui interface
	auto mainLayer = scene->getMainUIScene();
	if (strlen(describe.c_str()) > 0)
	{
		auto chatCellSmall = ChatCell::create(channel_id,play_name,player_country, describe, playerid,isMainChat,viplevel,listenerId,listenerName);
		chatCellSmall->retain();
		ChatWindows::s_chatCellsDataVector.push_back(chatCellSmall);

		int chatOfWindowsCellLine = 0;
		bool firstFlag = true;
		int chatCellSizeOfChatWindow = ChatWindows::s_chatCellsDataVector.size();
		for (int i=chatCellSizeOfChatWindow;i > 0; i--)
		{
			auto chatcell_ = (ChatCell *)ChatWindows::s_chatCellsDataVector.at(i - 1);
			chatOfWindowsCellLine += chatcell_->getOneCellOfLine();

			if (chatOfWindowsCellLine > 4 && i != ChatWindows::s_chatCellsDataVector.size())
			{
				std::vector<ChatCell *>::iterator iterCell= ChatWindows::s_chatCellsDataVector.begin()+ (i-1);
				auto cellv_=* iterCell;
				ChatWindows::s_chatCellsDataVector.erase(iterCell);
				cellv_->release();
			}
		}

		auto chatWindows=(ChatWindows *)mainLayer->chatLayer->getChildByTag(kTagChatWindows);
		if (chatWindows == NULL)
		{
			return;
		}
		chatWindows->showMainScene = true;

		switch(chatWindows->getChatWindowsState())
		{
		case chatWindowsState_showDown:
			{
				chatWindows->setChatWindowsState(chatWindowsState_showUp);
			}break;
		case chatWindowsState_show:
			{
				//keep show
			}break;

		}
		chatWindows->remTime = 45.0f;

		auto tableViewWindows = (TableView*)chatWindows->getChildByTag(TABVIEWCHATWINDOS_TAG);
		tableViewWindows->reloadData();

		if(tableViewWindows->getContentSize().height >= CHATWINDOW_PREFERRED_HEIGHT)
		{
			chatWindows->pImageView_chatBg->setPreferredSize(Size(CHATWINDOW_PREFERRED_WIDTH,tableViewWindows->getContentSize().height+6));
			tableViewWindows->setPositionY(tableViewWindows->getContentSize().height - tableViewWindows->getViewSize().height+6);
			
			int temp_x = mainLayer->btnRemin->getPosition().x;
			//int temp_y = 120+ tableViewWindows->getContentSize().height - CHATWINDOW_PREFERRED_HEIGHT;
			int temp_y = 130;

			mainLayer->btnRemin->setPosition(Vec2(temp_x,temp_y));
			mainLayer->btnReminOfNewMessageParticle->setPosition(Vec2(mainLayer->btnRemin->getPosition().x,
										mainLayer->btnRemin->getPosition().y - mainLayer->btnRemin->getContentSize().height/2));
										
			mainLayer->btnreminRepair->setPosition(Vec2(mainLayer->btnRemin->getPosition().x +mainLayer->btnRemin->getContentSize().width/2 + mainLayer->btnreminRepair->getContentSize().width/2+10,
												mainLayer->btnRemin->getPosition().y));
		}else
		{
			chatWindows->pImageView_chatBg->setPreferredSize(Size(CHATWINDOW_PREFERRED_WIDTH,CHATWINDOW_MAX_HEIGHT));
			tableViewWindows->setPositionY(CHATWINDOW_PREFERRED_HEIGHT- tableViewWindows->getViewSize().height+6);
			
			int temp_x = mainLayer->btnRemin->getPosition().x;
			int temp_y = 130;

			mainLayer->btnRemin->setPosition(Vec2(temp_x,temp_y));
			mainLayer->btnReminOfNewMessageParticle->setPosition(Vec2(mainLayer->btnRemin->getPosition().x,
						mainLayer->btnRemin->getPosition().y - mainLayer->btnRemin->getContentSize().height/2));
						
			mainLayer->btnreminRepair->setPosition(Vec2(mainLayer->btnRemin->getPosition().x +mainLayer->btnRemin->getContentSize().width/2 + mainLayer->btnreminRepair->getContentSize().width/2+10,
				mainLayer->btnRemin->getPosition().y));
		}

		if(tableViewWindows->getContentSize().height > tableViewWindows->getViewSize().height)
		{
			tableViewWindows->setContentOffset(tableViewWindows->maxContainerOffset(), false);
		}
	}
}

void ChatWindows::setChatWindowsState( int value_ )
{
	m_chatWindowsState = value_;
}

int ChatWindows::getChatWindowsState()
{
	return m_chatWindowsState;
}
