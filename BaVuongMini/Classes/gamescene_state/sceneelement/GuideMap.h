
#ifndef _GAMESCENESTATE_GUIDEMAP_H_
#define _GAMESCENESTATE_GUIDEMAP_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class AreaMap;
class MainScene;
class CFunctionOpenLevel;
class CCTutorialParticle;
struct GMCoordinate
{
	float x;
	float y;
};

//当前处于何种界面下（主界面，副本，竞技场）
// enum SceneType
// {
// 	ST_MainScene = 0,
// 	ST_FiveInstance,
// 	ST_Arena,
// };

class GuideMap :public UIScene
{
public:
	GuideMap();
	~GuideMap();

	static GuideMap* create();
	bool init();
	virtual void update(float dt);

	void setSceneType(int mapType);

	//副本
	void InstanceEvent(Ref *pSender, Widget::TouchEventType type);
	//召唤
	void CallFriendsEvent(Ref *pSender, Widget::TouchEventType type);
	//退出副本
	void ExitEvent(Ref *pSender, Widget::TouchEventType type);
	void sureToExit(Ref *pSender);

	//exit singCopy
	void ExitSingCopy(Ref *pSender, Widget::TouchEventType type);
	void surToExitSingCopy(Ref *pSender);

	void HangUpEvent(Ref *pSender, Widget::TouchEventType type);
	void GeneralModeEvent(Ref *pSender, Widget::TouchEventType type);
	void ShopEvent(Ref *pSender, Widget::TouchEventType type);
	void ActivityEvent(Ref *pSender, Widget::TouchEventType type);
	void GiftEvent(Ref *pSender, Widget::TouchEventType type);
	void ActiveDegreeEvent(Ref *pSender, Widget::TouchEventType type);
	void EveryDayGiftEvent(Ref *pSender, Widget::TouchEventType type);
	void NewServiceEvent(Ref *pSender, Widget::TouchEventType type);
	void ArenaEvent(Ref *pSender, Widget::TouchEventType type);
	//家族战战况
	void BattleSituationEvent(Ref *pSender, Widget::TouchEventType type);
	//在线奖励
	void OnLineGiftEvent(Ref *pSender, Widget::TouchEventType type);
	//get reward list
	void callBackGetReward(Ref *pSender, Widget::TouchEventType type);
	//首充
	void FirstBuyVipEvent(Ref *pSender, Widget::TouchEventType type);

	void GoToMapScene(Ref *pSender, Widget::TouchEventType type);

	void addNewFunctionForArea1(Node * pNode,void * functionOpenLevel);
	void addNewFunctionForArea2(Node * pNode,void * functionOpenLevel);

	//退出竞技场
	void QuitArenaEvent(Ref *pSender, Widget::TouchEventType type);
	void SureToQuitArena(Ref *pSender);

	//退出家族战
	void QuitFamilyFightEvent(Ref *pSender, Widget::TouchEventType type);
	void SureToQuitFamilyFight(Ref *pSender);

	//武将模式设置默认
	void setDefaultGeneralMode(int idx);
	//武将模式切换
	void applyGeneralMode(int idx,int autoFight);
	int getGeneralMode();
	int getGeneralAutoFight();

	//remind
	Button * buttonRemind;
	Button * getBtnRemind();
	void showRemindContent(Ref *pSender, Widget::TouchEventType type);
	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);
	virtual void onTouchCancelled(Touch *pTouch, Event *pEvent);

	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	//添加到actionLayer上
	void addCCTutorialIndicator1(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void addCCTutorialIndicatorSingCopy(const char* content,Vec2 pos,CCTutorialIndicator::Direction direction );
	void removeCCTutorialIndicator();	

	//教学
	int mTutorialScriptInstanceId;
	Button * btn_instance;
	Button *  btn_offLineArena;
	Button * btn_Reward;
	CCTutorialParticle * btn_RewardParticle;
	//robot
	bool isOpenSkillMonster;
	Button *btnRobotSkill;
	Button *btnRobotset;

	//获取当前显示的layer
	Layer * getCurActiveLayer();
	//获取当前的Actionlayer
	Layer * getActionLayer();
	//获取当前的挂机按钮（开始战斗/结束战斗）
	Button * getBtnHangUp();
	//设置所有挂机按钮的状态（处于一般情况，处于副本中，处于竞技场中）
	void setAllBtnHangUpTextures(const char * str);
	//获取当前的武将模式按钮（开启主动/开启协助）
	Button * getBtnGeneralMode();
	//设置所有武将模式按钮的状态（处于一般情况，处于副本中，处于竞技场中）
	void setAllBtnGeneralModeTextures(const char * str);
	//刷新地图名称
	void RefreshMapName();

	//获取首冲按钮
	Button * getBtnFirstReCharge();

private:
	void changeUIBySceneType( int mapType );
	//所有按钮是否显示以主界面为准
	void changeBtnVisibleByType( int mapType );

	void createUIForMainScene();
	void createUIForFiveInstanceScene();
	void createUIForArenaScene();
	void createUIForFamilyFightScene();
	void createUIForSingCopyScene();

	Layer * u_layer_MainScene;
	Layer * u_layer_FiveInstanceScene;
	Layer * u_layer_ArenaScene;
	Layer * u_layer_FamilyFightScene;
	Layer * u_layer_SingCopyScene;

	bool isExistUIForMainScene;
	bool isExistUIForFiveInstanceScene;
	bool isExistUIForArenaScene;
	bool isExistUIForFamilyFightScene;
	bool isExistUIForSingCopyScene;
	//可缩回层
	Layer * layer_action;

	std::string timeFormatToString( int t );
private:
	//MainScene* mainLayer;
	Size winSize;

	/*SceneType m_curSceneType;*/

	std::vector<CFunctionOpenLevel *>GMOpendedFunctionVector_1;   //可以显示的按钮(地图区域1，功能入口类型为2)
	std::vector<CFunctionOpenLevel *>GMOpendedFunctionVector;   //可以显示的按钮(地图区域2，功能入口类型为3)
	std::vector<GMCoordinate> CoordinateVector;

	//武将模式
	int m_generalMode;
	//武将自动出战
	int m_autoFight;

	Label * l_remainTimeValue;
	int m_nRemainTime;
};

#endif

