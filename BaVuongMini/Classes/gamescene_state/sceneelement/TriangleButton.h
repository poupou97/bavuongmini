#ifndef _UTILS_TRIANGLENODE_H
#define _UTILS_TRIANGLENODE_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class TriangleButton :public UIScene
{
public:
	TriangleButton(void);
	~TriangleButton(void);

	static TriangleButton* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

	void FrameEvent(Ref *pSender, Widget::TouchEventType type);
	void setAllOpen();

public:
	ImageView * imageView_arrow;

	bool isOpen;
};

#endif

