#include "MyPlayerInfoMini.h"
#include "../role/BaseFighter.h"
#include "../role/Monster.h"
#include "../../messageclient/element/CMonsterBaseInfo.h"
#include "../../newcomerstory/NewCommerStoryManager.h"

MyPlayerInfoMini::MyPlayerInfoMini()
{
}

MyPlayerInfoMini::~MyPlayerInfoMini()
{
}

MyPlayerInfoMini* MyPlayerInfoMini::create(BaseFighter * baseFighter)
{
	auto targetInfo = new MyPlayerInfoMini();
	if (targetInfo && targetInfo->init(baseFighter))
	{
		targetInfo->autorelease();
		return targetInfo;
	}
	CC_SAFE_DELETE(targetInfo);
	return NULL;
}

bool MyPlayerInfoMini::init(BaseFighter * baseFighter)
{
	if (UIScene::init())
	{
		Size visibleSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();

		baseFighterId = baseFighter->getRoleId();

		// red part
		ImageView_targetBlood = ui::ImageView::create();
		ImageView_targetBlood->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/green_xuetiao.png");
		ImageView_targetBlood->setAnchorPoint(Vec2(0,0));
		ImageView_targetBlood->setPosition(Vec2(0,0));
		float blood_scale = baseFighter->getActiveRole()->hp() * 1.0f / baseFighter->getActiveRole()->maxhp();
		if (blood_scale > 1.0f)
			blood_scale = 1.0f;
		ImageView_targetBlood->setTextureRect(Rect(0,0,ImageView_targetBlood->getContentSize().width*blood_scale,ImageView_targetBlood->getContentSize().height));
		
		// background part
		auto ImageView_blood_frame = ui::ImageView::create();
		ImageView_blood_frame->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/xuetiao_di.png");
		ImageView_blood_frame->setScale9Enabled(true);
		ImageView_blood_frame->setCapInsets(Rect(7,7,1,1));
		int offset = 1;
		int w = ImageView_targetBlood->getContentSize().width + offset*2;
		int h = ImageView_targetBlood->getContentSize().height + offset*2;
		ImageView_blood_frame->setContentSize(Size(w,h));
		ImageView_blood_frame->setAnchorPoint(Vec2(0,0));
		ImageView_blood_frame->setPosition(Vec2(-offset,-offset));

		m_pLayer->addChild(ImageView_blood_frame);
		m_pLayer->addChild(ImageView_targetBlood);

		this->setIgnoreAnchorPointForPosition(false);
		this->setAnchorPoint(Vec2(0,0));
		////this->setTouchEnabled(false);
		this->setContentSize(Size(w, h));

		this->setScaleX(0.5f);
		this->setScaleY(0.6f);
		auto pBoss = dynamic_cast<Monster*>(baseFighter);
		if(pBoss != NULL)
		{
			if(pBoss->getClazz() == CMonsterBaseInfo::clazz_world_boss)
			{
				this->setScaleX(1.4f);
				this->setScaleY(1.4f);
			}
			else if(pBoss->getClazz() == CMonsterBaseInfo::clazz_boss)
			{
				this->setScaleX(1.2f);
				this->setScaleY(1.2f);
			}
			else if(pBoss->getClazz() == CMonsterBaseInfo::clazz_elite)
			{
				this->setScaleX(1.0f);
				this->setScaleY(1.0f);
			}
		}

		// patch
		if( NewCommerStoryManager::getInstance()->IsNewComer())
		{
			this->setVisible(false);
		}

		return true;
	}
	return false;
}

void MyPlayerInfoMini::onEnter()
{
	UIScene::onEnter();
}

void MyPlayerInfoMini::onExit()
{
	UIScene::onExit();
}

bool MyPlayerInfoMini::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return false;
}

void MyPlayerInfoMini::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void MyPlayerInfoMini::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void MyPlayerInfoMini::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void MyPlayerInfoMini::ReloadTargetData( BaseFighter * baseFighter)
{
	int nHp = baseFighter->getActiveRole()->hp();
	int nMaxHp = baseFighter->getActiveRole()->maxhp();

	float blood_scale = nHp * 1.0f / nMaxHp;

	if (blood_scale > 1.0f)
	{
		blood_scale = 1.0f;
	}
	
	ImageView_targetBlood->setTextureRect(Rect(0,0,ImageView_targetBlood->getContentSize().width*blood_scale,ImageView_targetBlood->getContentSize().height));
}

