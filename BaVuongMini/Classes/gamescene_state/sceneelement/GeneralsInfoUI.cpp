﻿#include "GeneralsInfoUI.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "GameView.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../AppMacros.h"
#include "../../messageclient/element/GoodsInfo.h"
#include "../../ui/backpackscene/PackageScene.h"
#include "../../messageclient/element/FolderInfo.h"
#include "shortcutelement/ShortcutLayer.h"
#include "shortcutelement/ShortcutUIConstant.h"
#include "../../messageclient/element/CBaseSkill.h"
#include "../../messageclient/element/CActiveRole.h"
#include "../role/General.h"
#include "../MainScene.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../ui/generals_ui/GeneralsRecuriteUI.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../utils/GameUtils.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "../../newcomerstory/NewCommerStoryManager.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../messageclient/element/CFunctionOpenLevel.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../legend_script/CCTeachingGuide.h"

#define Max_Present_GeneralNum 5
#define BaseTag_Present_GeneralNum 100

#define ActionTag_HighlightImageView 40

GeneralsInfoUI::GeneralsInfoUI():
mTutorialIndex(-1),
curAutoGuideIndex(-1)
{
}


GeneralsInfoUI::~GeneralsInfoUI()
{
}

GeneralsInfoUI* GeneralsInfoUI::create()
{
	auto generalsInfoUI = new GeneralsInfoUI();
	if (generalsInfoUI && generalsInfoUI->init())
	{
		generalsInfoUI->autorelease();
		return generalsInfoUI;
	}
	CC_SAFE_DELETE(generalsInfoUI);
	return NULL;
}

bool GeneralsInfoUI::init()
{
	if (UIScene::init())
	{
		u_layer = Layer::create();
		u_layer->setIgnoreAnchorPointForPosition(false);
		u_layer->setAnchorPoint(Vec2(0,0));
		u_layer->setContentSize(Size(UI_DESIGN_RESOLUTION_WIDTH, UI_DESIGN_RESOLUTION_HEIGHT));
		u_layer->setPosition(Vec2::ZERO);
		addChild(u_layer);

		u_UpLayer = Layer::create();
		u_UpLayer->setIgnoreAnchorPointForPosition(false);
		u_UpLayer->setAnchorPoint(Vec2(0,0));
		u_UpLayer->setContentSize(Size(UI_DESIGN_RESOLUTION_WIDTH, UI_DESIGN_RESOLUTION_HEIGHT));
		u_UpLayer->setPosition(Vec2::ZERO);
		addChild(u_UpLayer);

		////this->setTouchEnabled(false);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(UI_DESIGN_RESOLUTION_WIDTH,UI_DESIGN_RESOLUTION_HEIGHT));

		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(GeneralsInfoUI::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(GeneralsInfoUI::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(GeneralsInfoUI::onTouchMoved, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}

void GeneralsInfoUI::onEnter()
{
	UIScene::onEnter();
	RefreshAllGeneralItem();
}

void GeneralsInfoUI::onExit()
{
	UIScene::onExit();
}

bool GeneralsInfoUI::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return false;
}

void GeneralsInfoUI::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void GeneralsInfoUI::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void GeneralsInfoUI::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void GeneralsInfoUI::RefreshAllGeneralItem()
{
	//delete old
	for (int i = 0;i<Max_Present_GeneralNum;++i)
	{
		if (!u_layer->getChildByTag(BaseTag_Present_GeneralNum+i))
			continue;

		u_layer->getChildByTag(BaseTag_Present_GeneralNum+i)->removeFromParent();
	}


	if (NewCommerStoryManager::getInstance()->IsNewComer())  //新手剧情中不需要等级限制
	{
		//add new
		int curNum = GameView::getInstance()->generalsInLineList.size();
		int geginPosX = (UI_DESIGN_RESOLUTION_WIDTH-71*Max_Present_GeneralNum)/2+55;
		for (int i = 0;i<Max_Present_GeneralNum;++i)
		{
			if (i < curNum)
			{
				auto item = GeneralItem::create(GameView::getInstance()->generalsInLineList.at(i),i);
				item->setIgnoreAnchorPointForPosition(false);
				item->setAnchorPoint(Vec2(0,0));
				item->setPosition(Vec2(geginPosX + i*72,0));
				u_layer->addChild(item,0,BaseTag_Present_GeneralNum+i);
			}
			else
			{
				auto item = GeneralItem::create(i);
				item->setIgnoreAnchorPointForPosition(false);
				item->setAnchorPoint(Vec2(0,0));
				item->setPosition(Vec2(geginPosX + i*72,0));
				u_layer->addChild(item,0,BaseTag_Present_GeneralNum+i);
			}
		}
	}
	else
	{
		//开启等级
		//上阵第一个武将对应ID   8   一次增加
		int firstopenlevel = 0;
		std::map<int,int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(8);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
		{
		}
		else
		{
			firstopenlevel = BtnOpenLevelConfigData::s_btnOpenLevel[8];
		}

		if (firstopenlevel > GameView::getInstance()->myplayer->getActiveRole()->level())  //角色等级还未达到第一个武将的开启等级
		{
			return;
		}

		//add new
		int curNum = GameView::getInstance()->generalsInLineList.size();
		int geginPosX = (UI_DESIGN_RESOLUTION_WIDTH-71*Max_Present_GeneralNum)/2+55;
		//Size winSize = Director::getInstance()->getVisibleSize();
		//int geginPosX = 277*(winSize.width*1.0f/UI_DESIGN_RESOLUTION_WIDTH);
		for (int i = 0;i<Max_Present_GeneralNum;++i)
		{
			//开启等级
			//上阵第一个武将对应ID   8   一次增加
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(8+i);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[8+i];
			}

			if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())  //未开启
			{
				auto item = GeneralItem::createClocked(i);
				item->setIgnoreAnchorPointForPosition(false);
				item->setAnchorPoint(Vec2(0,0));
				item->setPosition(Vec2(geginPosX + i*72,0));
				u_layer->addChild(item,0,BaseTag_Present_GeneralNum+i);
			}
			else
			{
				if (i < curNum)
				{
					auto item = GeneralItem::create(GameView::getInstance()->generalsInLineList.at(i),i);
					item->setIgnoreAnchorPointForPosition(false);
					item->setAnchorPoint(Vec2(0,0));
					item->setPosition(Vec2(geginPosX + i*72,0));
					u_layer->addChild(item,0,BaseTag_Present_GeneralNum+i);
				}
				else
				{
					auto item = GeneralItem::create(i);
					item->setIgnoreAnchorPointForPosition(false);
					item->setAnchorPoint(Vec2(0,0));
					item->setPosition(Vec2(geginPosX + i*72,0));
					u_layer->addChild(item,0,BaseTag_Present_GeneralNum+i);
				}
			}
		}
	}

	//AllResetCD();
}

void GeneralsInfoUI::RefreshOneGeneralItem( long long generalId )
{
	CCLOG("generalId = %ld",generalId);
	for (int i = 0;i<Max_Present_GeneralNum;++i)
	{
		auto item = (GeneralItem*)u_layer->getChildByTag(BaseTag_Present_GeneralNum+i);
		if (!item)
			continue;

		if (item->m_nGeneralId == generalId)
		{
			auto sequence = Sequence::create(
				DelayTime::create(0.01f),
				CallFunc::create(CC_CALLBACK_0(GeneralItem::RefreshSelfStatus,item)),
				CallFunc::create(CC_CALLBACK_0(GeneralItem::RefreshUI,item)),
				NULL
				);
			item->runAction(sequence);
// 			item->RefreshSelfStatus();
// 			item->RefreshUI();
		}
	}
}

GeneralItem * GeneralsInfoUI::getGeneralItemByIndex( int index )
{
	 auto temp = (GeneralItem*)u_layer->getChildByTag(BaseTag_Present_GeneralNum+index);
	 if (temp)
		 return temp;

	 return NULL;
}

void GeneralsInfoUI::ResetCDByGeneralId(long long generalID)
{
	for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();i++)
	{
		auto temp = GameView::getInstance()->generalsInLineList.at(i);
		if (temp->id() == generalID)
		{
			temp->startCD();
		}
	}
}

void GeneralsInfoUI::RefreshSkillByGeneralDetail( CGeneralDetail * generalDetail )
{
	for (int i = 0;i<Max_Present_GeneralNum;++i)
	{
		auto item = (GeneralItem*)u_layer->getChildByTag(BaseTag_Present_GeneralNum+i);
		if (!item)
			continue;

		if (item->m_nGeneralId == generalDetail->generalid())
		{
			item->RefreshSkillPresent(generalDetail);
		}
	}
}

void GeneralsInfoUI::RefreshGeneralHP( General * general )
{
	for (int i = 0;i<Max_Present_GeneralNum;++i)
	{
		auto item = (GeneralItem*)u_layer->getChildByTag(BaseTag_Present_GeneralNum+i);
		if (!item)
			continue;

		if (item->m_nGeneralId == general->getActiveRole()->rolebase().roleid())
		{
			item->RefreshGeneralHP(general);
		}
	}
}

void GeneralsInfoUI::RefreshGeneralHP( CGeneralDetail * generalDetail )
{
	for (int i = 0;i<Max_Present_GeneralNum;++i)
	{
		auto item = (GeneralItem*)u_layer->getChildByTag(BaseTag_Present_GeneralNum+i);
		if (!item)
			continue;

		if (item->m_nGeneralId == generalDetail->generalid())
		{
			item->RefreshGeneralHP(generalDetail);
		}
	}
}

void GeneralsInfoUI::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GeneralsInfoUI::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
// 	auto tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(Vec2(pos.x+90,pos.y+130));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	u_UpLayer->addChild(tutorialIndicator);
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,66,66,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+33+_w,pos.y+40));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralsInfoUI::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
// 	auto tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(Vec2(pos.x+32,pos.y+35));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	tutorialIndicator->addHighLightFrameAction(60,60);
// 	u_UpLayer->addChild(tutorialIndicator);
	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,66,66,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+33+_w,pos.y+40));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GeneralsInfoUI::removeCCTutorialIndicator()
{
	this->mTutorialIndex = -1;
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(u_UpLayer->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void GeneralsInfoUI::ResetAllFadeAction()
{
	//当前出战武将数量小于2
	int curFightNum = 0;
	for (int i = 0;i<Max_Present_GeneralNum;++i)
	{
		auto temp = this->getGeneralItemByIndex(i);
		if(!temp)
			continue;

		if (temp->m_nCurStatus == GeneralItem::status_InBattle) //已经出战
		{
			curFightNum++;
		}
	}
	//重置现有的闪烁的特效，使他们的步伐保持一致。
	for (int i = 0;i<Max_Present_GeneralNum;++i)
	{
		auto temp = this->getGeneralItemByIndex(i);
		if(!temp)
			continue;

		auto pHighlightSpr = (ImageView*)temp->btn_headFrame->getChildByName("pHighlightSpr");
		if(pHighlightSpr)
		{
			if (temp->m_nCurStatus == GeneralItem::status_InLine && curFightNum < 2)
			{
				//可出战高亮闪烁
				pHighlightSpr->stopActionByTag(ActionTag_HighlightImageView);
				//闪烁(淡入淡出)
				auto sq = Sequence::create(
					FadeIn::create(1.0f),
					FadeOut::create(0.8f),
					NULL
					);
				pHighlightSpr->setVisible(true);
				pHighlightSpr->runAction(RepeatForever::create(sq));
				sq->setTag(ActionTag_HighlightImageView);
			}
			else
			{
				pHighlightSpr->stopActionByTag(ActionTag_HighlightImageView);
				pHighlightSpr->setVisible(false);
			}
		}
	}
}

void GeneralsInfoUI::setAutoGuideToCallGeneral( bool visible )
{
	if (visible)
	{
		this->schedule(CC_SCHEDULE_SELECTOR(GeneralsInfoUI::AutoGuideToCallGeneral),1.0f);
	}
	else
	{
		if (u_UpLayer->getChildByTag(CCTUTORIALINDICATORTAG))
		{
			u_UpLayer->getChildByTag(CCTUTORIALINDICATORTAG)->removeFromParent();
		}
		
		this->unschedule(CC_SCHEDULE_SELECTOR(GeneralsInfoUI::AutoGuideToCallGeneral));
	}
}

void GeneralsInfoUI::AutoGuideToCallGeneral(float dt)
{
	int inBattleNum = 0;
	for(std::size_t i = 0;i<GameView::getInstance()->generalsInLineList.size();i++)
	{
		auto generalBaseMsg = GameView::getInstance()->generalsInLineList.at(i);
		if (generalBaseMsg)
		{
			if (generalBaseMsg->fightstatus() == GeneralsListUI::InBattle)
				inBattleNum++;
		}
	}
	if (inBattleNum >= 2)
	{
		if (u_UpLayer->getChildByTag(CCTUTORIALINDICATORTAG))
		{
			u_UpLayer->getChildByTag(CCTUTORIALINDICATORTAG)->removeFromParent();
		}

		return;
	}

	for (int i = 0;i<Max_Present_GeneralNum;++i)
	{
		auto generalItem = dynamic_cast<GeneralItem*>(u_layer->getChildByTag(BaseTag_Present_GeneralNum+i));
		if (!generalItem)
			continue;

		if (generalItem->m_nCurStatus != GeneralItem::status_InLine)
		{
			if (curAutoGuideIndex == i)
			{
				if (u_UpLayer->getChildByTag(CCTUTORIALINDICATORTAG))
				{
					u_UpLayer->getChildByTag(CCTUTORIALINDICATORTAG)->removeFromParent();
				}
			}
		}
	}

	if (u_UpLayer->getChildByTag(CCTUTORIALINDICATORTAG))
	{
	 	return;
	}

	for (int i = 0;i<Max_Present_GeneralNum;++i)
	{
		auto generalItem = dynamic_cast<GeneralItem*>(u_layer->getChildByTag(BaseTag_Present_GeneralNum+i));
		if (!generalItem)
			continue;

		if (generalItem->m_nCurStatus == GeneralItem::status_InLine)
		{
			//add guide 
			Vec2 pos = generalItem->getPosition();
			auto tutorialIndicator = CCTutorialIndicator::create(StringDataManager::getString("NewComerStory_callGenerals"),pos,CCTutorialIndicator::Direction_RD,true);
			tutorialIndicator->setPosition(Vec2(pos.x+33,pos.y+40));
			tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
			tutorialIndicator->addHighLightFrameAction(66,66);
			tutorialIndicator->addCenterAnm(66,66);
			u_UpLayer->addChild(tutorialIndicator);
			curAutoGuideIndex = i;
			break;
		}
	}
}


///////////////////////////////////////////////////////////////////////////////////////////

// size : 67,77
#define GENERALITEM_WIDTH 67
#define GENERALITEM_HEIGHT 77

#define PROGRESSTIMER_REST_TAG 87
#define ACTION_MOVE_TAG 88

GeneralItem::GeneralItem():
m_nIndex(-1),
m_nGeneralId(-1),
m_nCurStatus(-1),
m_nBaseTimeInBattle(0),
m_bIsRevevidCountFinish(false)
{

}

GeneralItem::~GeneralItem()
{

}

GeneralItem* GeneralItem::create(CGeneralBaseMsg * generalBaseMsg,int index)
{
	auto generalItem = new GeneralItem();
	if (generalItem && generalItem->init(generalBaseMsg,index))
	{
		generalItem->autorelease();
		return generalItem;
	}
	CC_SAFE_DELETE(generalItem);
	return NULL;
}

GeneralItem* GeneralItem::create(int index)
{
	auto generalItem = new GeneralItem();
	if (generalItem && generalItem->init(index))
	{
		generalItem->autorelease();
		return generalItem;
	}
	CC_SAFE_DELETE(generalItem);
	return NULL;
}

GeneralItem* GeneralItem::createClocked( int index )
{
	auto generalItem = new GeneralItem();
	if (generalItem && generalItem->initClocked(index))
	{
		generalItem->autorelease();
		return generalItem;
	}
	CC_SAFE_DELETE(generalItem);
	return NULL;
}

bool GeneralItem::init(CGeneralBaseMsg * generalBaseMsg,int index)
{
	if (UIScene::init())
	{
		//m_pLayer->removeFromParent();
		//m_pLayer = Layer::create();
		//m_pLayer->setTouchPriority(10);
		//addChild(m_pLayer, 0, TAG_UISCENE_Layer);

		m_nIndex = index;
		m_nGeneralId = generalBaseMsg->id();
		m_nBaseTimeInBattle = generalBaseMsg->showtimebase();
		RefreshSelfStatus();

		auto generalBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[generalBaseMsg->modelid()];
		CCAssert(generalBaseMsgFromDb != NULL,"generalBaseMsgFromDb should not be nil");

		CGeneralDetail * generalDetail;
		bool isOk = false;
		for (int i = 0;i<GameView::getInstance()->generalsInLineDetailList.size();++i)
		{
			if (generalBaseMsg->id() == GameView::getInstance()->generalsInLineDetailList.at(i)->generalid())
			{
				generalDetail = GameView::getInstance()->generalsInLineDetailList.at(i);
				isOk = true;
				break;
			}
		}

		//headFrame
		btn_headFrame = Button::create();
		btn_headFrame->loadTextures("res_ui/general_di.png","res_ui/general_di.png","");
		btn_headFrame->setScale9Enabled(true);
		btn_headFrame->setContentSize(Size(60,60));
		btn_headFrame->setCapInsets(Rect(18,18,1,1));
		btn_headFrame->setTouchEnabled(true);
		btn_headFrame->setAnchorPoint(Vec2(.5f,.5f));
		btn_headFrame->setPosition(Vec2(GENERALITEM_WIDTH/2,btn_headFrame->getContentSize().height/2+10));
		btn_headFrame->addTouchEventListener(CC_CALLBACK_2(GeneralItem::HeadFrameEvent,this));
		btn_headFrame->setPressedActionEnabled(true);
		m_pLayer->addChild(btn_headFrame);

// 		//bloodFrame
// 		auto imageView_bloodFrame = ImageView::create();
// 		imageView_bloodFrame->loadTexture("res_ui/LV4_diaaa.png");
// 		imageView_bloodFrame->setScale9Enabled(true);
// 		imageView_bloodFrame->setContentSize(Size(67,10));
// 		imageView_bloodFrame->setCapInsets(Rect(4,4,8,8));
// 		imageView_bloodFrame->setAnchorPoint(Vec2(.0f,.0f));
// 		imageView_bloodFrame->setPosition(Vec2(-GENERALITEM_WIDTH/2,-btn_headFrame->getContentSize().height/2-10));
// 		btn_headFrame->addChild(imageView_bloodFrame);
// 		//blood
// 		auto imageView_blood = ImageView::create();
// 		imageView_blood->loadTexture("res_ui/hp123.png");
// 		imageView_blood->setAnchorPoint(Vec2(.0f,.0f));
// 		imageView_blood->setPosition(Vec2(-GENERALITEM_WIDTH/2+2,-btn_headFrame->getContentSize().height/2-8));
// 		imageView_blood->setName("imageView_blood");
// 		btn_headFrame->addChild(imageView_blood);

		//inbattle count down
		auto imageView_countDownFrame = ImageView::create();
		imageView_countDownFrame->loadTexture("res_ui/LV4_diaaa.png");
		imageView_countDownFrame->setScale9Enabled(true);
		imageView_countDownFrame->setContentSize(Size(67,10));
		imageView_countDownFrame->setCapInsets(Rect(4,4,8,8));
		imageView_countDownFrame->setAnchorPoint(Vec2(.0f,.0f));
		imageView_countDownFrame->setPosition(Vec2(-GENERALITEM_WIDTH/2,-btn_headFrame->getContentSize().height/2-10));
		imageView_countDownFrame->setName("imageView_countDownFrame");
		btn_headFrame->addChild(imageView_countDownFrame);
		imageView_countDownFrame->setLocalZOrder(3);
		// count down
		auto imageView_countDown = ImageView::create();
		imageView_countDown->loadTexture("res_ui/hp123.png");
		imageView_countDown->setAnchorPoint(Vec2(.0f,.0f));
		imageView_countDown->setPosition(Vec2(-GENERALITEM_WIDTH/2+2,-btn_headFrame->getContentSize().height/2-8));
		imageView_countDown->setName("imageView_countDown");
		btn_headFrame->addChild(imageView_countDown);
		imageView_countDown->setLocalZOrder(3);

		//head
		auto imageView_head = ImageView::create();
		std::string headPath = "res_ui/generals46X45/";
		headPath.append(generalBaseMsgFromDb->get_head_photo().c_str());
		headPath.append(".png");
		imageView_head->loadTexture(headPath.c_str());
		imageView_head->setAnchorPoint(Vec2(.5f,.5f));
		imageView_head->setPosition(Vec2(btn_headFrame->getBoundingBox().size.width/2, btn_headFrame->getBoundingBox().size.height/2));
		imageView_head->setName("imageView_head");
		btn_headFrame->addChild(imageView_head);
		imageView_head->setLocalZOrder(1);

		auto pHighlightSpr = ImageView::create();
		pHighlightSpr->loadTexture("res_ui/highlight.png");
		pHighlightSpr->setScale9Enabled(true);
		pHighlightSpr->setCapInsets(Rect(26, 26, 1, 1));
		pHighlightSpr->setContentSize(Size(62,62));
		pHighlightSpr->setAnchorPoint(Vec2(0.5f,0.5f));
		pHighlightSpr->setName("pHighlightSpr");
		btn_headFrame->addChild(pHighlightSpr);
		pHighlightSpr->setPosition(Vec2(btn_headFrame->getBoundingBox().size.width / 2, btn_headFrame->getBoundingBox().size.height / 2));
		pHighlightSpr->setVisible(false);
		pHighlightSpr->setLocalZOrder(2);

		//休息倒计时（转CD）
		auto widget_progress = Widget::create();
		btn_headFrame->addChild(widget_progress);
		widget_progress->setLocalZOrder(3);
		widget_progress->setName("widget_progress");
		auto progress_rest = Sprite::create("res_ui/zhezhao_75_60x60.png");
		auto mProgressTimer_rest = ProgressTimer::create(progress_rest);
		mProgressTimer_rest->setAnchorPoint(Vec2(0.5f, 0.5f));
		mProgressTimer_rest->setPosition(Vec2(btn_headFrame->getBoundingBox().size.width / 2, btn_headFrame->getBoundingBox().size.height / 2));
		mProgressTimer_rest->setVisible(false);
		mProgressTimer_rest->setType(ProgressTimer::Type::RADIAL);
		mProgressTimer_rest->setReverseDirection(true); // 设置进度条为逆时针
		widget_progress->getVirtualRenderer()->addChild(mProgressTimer_rest,100,PROGRESSTIMER_REST_TAG);

		//timeInBattle Frame
		auto imageView_timeInBattleFrame = ImageView::create();
		imageView_timeInBattleFrame->loadTexture("res_ui/lv_kuang.png");
		imageView_timeInBattleFrame->setScale9Enabled(true);
		imageView_timeInBattleFrame->setContentSize(Size(24,17));
		imageView_timeInBattleFrame->setCapInsets(Rect(6,5,2,2));
		imageView_timeInBattleFrame->setAnchorPoint(Vec2(0,0));
		//imageView_timeInBattleFrame->setPosition(Vec2(-btn_headFrame->getContentSize().width/2+12,-btn_headFrame->getContentSize().height/2+8));
		imageView_timeInBattleFrame->setPosition(Vec2(0,0));
		imageView_timeInBattleFrame->setName("imageView_timeInBattleFrame");
		btn_headFrame->addChild(imageView_timeInBattleFrame);
		imageView_timeInBattleFrame->setLocalZOrder(3);
		//timeInBattle
		char str_baseTimeInBattle [20];
		sprintf(str_baseTimeInBattle,"%d",m_nBaseTimeInBattle/1000);

		auto l_remainTimeInBattle = Label::createWithTTF(str_baseTimeInBattle, APP_FONT_NAME, 14);
		l_remainTimeInBattle->setAnchorPoint(Vec2(0,0));
		l_remainTimeInBattle->setPosition(Vec2(0,0));
		l_remainTimeInBattle->setName("l_remainTimeInBattle");
		btn_headFrame->addChild(l_remainTimeInBattle);
		l_remainTimeInBattle->setLocalZOrder(3);
		//skill Frame
		auto imageView_skillFrame = ImageView::create();
		imageView_skillFrame->loadTexture(SKILLEMPTYPATH);
		imageView_skillFrame->setAnchorPoint(Vec2(.5f,.5f));
		imageView_skillFrame->setPosition(Vec2(btn_headFrame->getContentSize().width,btn_headFrame->getContentSize().height));
		btn_headFrame->addChild(imageView_skillFrame);
		imageView_skillFrame->setName("imageView_skillFrame");
		imageView_skillFrame->setScale(0.5f);
		imageView_skillFrame->setLocalZOrder(3);
		//skill 
		auto imageView_skill = ImageView::create();
		imageView_skill->loadTexture("");
		imageView_skill->setAnchorPoint(Vec2(.5f,.5f));
		imageView_skill->setPosition(Vec2(btn_headFrame->getContentSize().width,btn_headFrame->getContentSize().height));
		imageView_skill->setName("imageView_skill");
		btn_headFrame->addChild(imageView_skill);
		imageView_skill->setScale(0.6f);
		imageView_skill->setLocalZOrder(3);
		//休息时间（CD）
// 		Label* l_remainTimeInRest = Label::create();
// 		l_remainTimeInRest->setText("");
// 		l_remainTimeInRest->setFontName(APP_FONT_NAME);
// 		l_remainTimeInRest->setFontSize(20);
// 		l_remainTimeInRest->setAnchorPoint(Vec2(.5f,.5f));
// 		l_remainTimeInRest->setPosition(Vec2(btn_headFrame->getContentSize().width/2,btn_headFrame->getContentSize().height/2-20));
// 		l_remainTimeInRest->setName("l_remainTimeInRest");
// 		m_pLayer->addChild(l_remainTimeInRest);
// 		l_remainTimeInRest->setVisible(false);
		//复活等待时间
		auto l_waitForRevive = Label::createWithTTF("", APP_FONT_NAME, 20);
		l_waitForRevive->setAnchorPoint(Vec2(.5f,.5f));
		l_waitForRevive->setPosition(Vec2(0,0));
		l_waitForRevive->setName("l_waitForRevive");
		//m_pLayer->addChild(l_waitForRevive);
		btn_headFrame->addChild(l_waitForRevive);
		l_waitForRevive->setVisible(false);
		l_waitForRevive->setLocalZOrder(3);

		//出战标识
		auto imageView_inBattle = ImageView::create();
		imageView_inBattle->loadTexture("res_ui/wujiang/play.png");
		imageView_inBattle->setAnchorPoint(Vec2(.5f,.5f));
		imageView_inBattle->setPosition(Vec2(-btn_headFrame->getContentSize().width/2+15,-btn_headFrame->getContentSize().height/2+15));
		imageView_inBattle->setName("imageView_inBattle");
		btn_headFrame->addChild(imageView_inBattle);
		imageView_inBattle->setScale(0.6f);
		imageView_inBattle->setVisible(false);
		imageView_inBattle->setLocalZOrder(3);

		RefreshUI();
		if (isOk)
		{
			RefreshSkillPresent(generalDetail);
		}
		
		this->scheduleUpdate();
		this->setContentSize(Size(GENERALITEM_WIDTH,GENERALITEM_HEIGHT));

		return true ;
	}
	return false;
}

bool GeneralItem::init(int index)
{
	if (UIScene::init())
	{
		m_nIndex = index;

		//m_pLayer->removeFromParent();
		//m_pLayer = Layer::create();
		//m_pLayer->setTouchPriority(10);
		//addChild(m_pLayer, 0, TAG_UISCENE_Layer);

		btn_headFrame = Button::create();
		btn_headFrame->loadTextures("res_ui/general_di.png","res_ui/general_di.png","");
		btn_headFrame->setScale9Enabled(true);
		btn_headFrame->setContentSize(Size(60,60));
		btn_headFrame->setCapInsets(Rect(18,18,1,1));
		btn_headFrame->setTouchEnabled(true);
		btn_headFrame->setAnchorPoint(Vec2(.5f,.5f));
		btn_headFrame->setPosition(Vec2(GENERALITEM_WIDTH/2,btn_headFrame->getContentSize().height/2+10));
		btn_headFrame->addTouchEventListener(CC_CALLBACK_2(GeneralItem::HeadFrameEvent, this));
		m_pLayer->addChild(btn_headFrame);
		btn_headFrame->setPressedActionEnabled(true);
		//head
		auto imageView_head = ImageView::create();
		imageView_head->loadTexture("res_ui/plus_general.png");
		imageView_head->setAnchorPoint(Vec2(.5f,.5f));
		imageView_head->setPosition(Vec2::ZERO);
		btn_headFrame->addChild(imageView_head);

		this->setContentSize(Size(GENERALITEM_WIDTH,GENERALITEM_HEIGHT));
		return true ;
	}
	return false;
}

bool GeneralItem::initClocked( int index )
{
	if (UIScene::init())
	{
		m_nIndex = index;

		//m_pLayer->removeFromParent();
		//m_pLayer = Layer::create();
		//m_pLayer->setTouchPriority(10);
		//addChild(m_pLayer, 0, TAG_UISCENE_Layer);

		btn_headFrame = Button::create();
		btn_headFrame->loadTextures("res_ui/general_di2.png","res_ui/general_di2.png","");
		btn_headFrame->setScale9Enabled(true);
		btn_headFrame->setContentSize(Size(60,60));
		btn_headFrame->setCapInsets(Rect(18,18,1,1));
		btn_headFrame->setTouchEnabled(true);
		btn_headFrame->setAnchorPoint(Vec2(.5f,.5f));
		btn_headFrame->setPosition(Vec2(GENERALITEM_WIDTH/2,btn_headFrame->getContentSize().height/2+10));
		btn_headFrame->addTouchEventListener(CC_CALLBACK_2(GeneralItem::ClockedFrameEvent, this));
		m_pLayer->addChild(btn_headFrame);
		btn_headFrame->setPressedActionEnabled(true);
		//clock
		auto imageView_clock = ImageView::create();
		imageView_clock->loadTexture("res_ui/suo.png");
		imageView_clock->setAnchorPoint(Vec2(.5f,.5f));
		imageView_clock->setPosition(Vec2(0,6));
		imageView_clock->setScale(0.8f);
		btn_headFrame->addChild(imageView_clock);
		//head
		auto l_des = Label::create();
		l_des->setString("15");
		l_des->setAnchorPoint(Vec2(.5f,.5f));
		l_des->setPosition(Vec2(0,-15));
		btn_headFrame->addChild(l_des);

		//开启等级
		//上阵第一个武将对应ID   8   一次增加
		int openlevel = 0;
		std::map<int,int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(8+index);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[8+index];
		}

		std::string str_des = "";
		char s_openLevel[10];
		sprintf(s_openLevel,"%d",openlevel);
		str_des.append(s_openLevel);
		str_des.append(StringDataManager::getString("fivePerson_ji"));
		l_des->setString(str_des.c_str());

		this->setContentSize(Size(GENERALITEM_WIDTH,GENERALITEM_HEIGHT));
		return true ;
	}
	return false;
}

void GeneralItem::onEnter()
{
	UIScene::onEnter();
}

void GeneralItem::onExit()
{
	UIScene::onExit();
}

bool GeneralItem::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return false;
}

void GeneralItem::update( float dt )
{
	RefreshSelfStatus();

	if (m_nCurStatus == -1)
		return;

	auto temp = GeneralsStateManager::getInstance()->getGeneralBaseMsgById(m_nGeneralId);
	if (!temp)
		return;

	//出战倒计时
	auto l_remainTimeInBattle = (Label*)btn_headFrame->getChildByName("l_remainTimeInBattle");
	if (l_remainTimeInBattle)
	{
		if (m_nCurStatus == status_InBattle)
		{
			char str_remainTime [20];
			sprintf(str_remainTime,"%d",temp->getRemainTime()/1000);
			l_remainTimeInBattle->setString(str_remainTime);
		}
		else
		{
			char str_baesTimeInBattle [20];
			sprintf(str_baesTimeInBattle,"%d",m_nBaseTimeInBattle/1000);
			l_remainTimeInBattle->setString(str_baesTimeInBattle);
		}
	}

	auto imageView_countDown = (ImageView*)btn_headFrame->getChildByName("imageView_countDown");
	if (imageView_countDown)
	{
		if (m_nCurStatus == status_InBattle)
		{
			float h_a = temp->getRemainTime()*1.0f;
			float h_b = temp->showtimebase()*1.0f;
			float count_scale = h_a/h_b;
			if (count_scale > 1.0f)
				count_scale = 1.0f;

			imageView_countDown->setTextureRect(Rect(0,0,imageView_countDown->getContentSize().width*count_scale,imageView_countDown->getContentSize().height));
		}
		else
		{
			imageView_countDown->setTextureRect(Rect(0,0,imageView_countDown->getContentSize().width*1.0f,imageView_countDown->getContentSize().height));

		}
	}

	//休息时间
// 	auto l_remainTimeInRest = (Label*)m_pLayer->getChildByName("l_remainTimeInRest");
// 	if (l_remainTimeInRest)
// 	{
// 		if (m_nCurStatus == status_InRest)
// 		{
// 			char str_remainTime [20];
// 			sprintf(str_remainTime,"%d",temp->getRemainTime()/1000);
// 			l_remainTimeInRest->setText(str_remainTime);
// 		}
// 	}

	//复活等待时间
	auto l_waitForRevive = (Label*)btn_headFrame->getChildByName("l_waitForRevive");
	if (l_waitForRevive)
	{
		if (m_nCurStatus == status_Died)
		{
			char str_remainTime [20];
			sprintf(str_remainTime,"%d",temp->getRemainTime()/1000);
			l_waitForRevive->setString(str_remainTime);

			if (!m_bIsRevevidCountFinish)
			{
				if (temp->getRemainTime() == 0)
				{
					GameMessageProcessor::sharedMsgProcessor()->sendReq(5052,(void*)m_nGeneralId);
					GameMessageProcessor::sharedMsgProcessor()->sendReq(5109,(void*)m_nGeneralId);
					m_bIsRevevidCountFinish = true;
				}
			}
		}
	}	
}

void GeneralItem::HeadFrameEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (m_nCurStatus == status_InBattle)//已出战
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("Genera_status_inBattle"));
		}
		else if (m_nCurStatus == status_InLine)//在队列中，可出战
		{
			if (m_nGeneralId != -1)
			{
				auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
				if (mainScene)
				{
					auto generalsInfoUI = (GeneralsInfoUI*)mainScene->getGeneralInfoUI();
					if (generalsInfoUI)
					{
						if ((m_nIndex + 1) == generalsInfoUI->mTutorialIndex)
						{
							Script* sc = ScriptManager::getInstance()->getScriptById(generalsInfoUI->mTutorialScriptInstanceId);
							if (sc != NULL)
								sc->endCommand(generalsInfoUI);
						}
					}
				}

				GameMessageProcessor::sharedMsgProcessor()->sendReq(5056, (void *)m_nGeneralId, (void *)2);
			}
		}
		else if (m_nCurStatus == status_InRest) //休息
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("Genera_status_Rest"));
		}
		else if (m_nCurStatus == status_Died) //死亡
		{
			bool isExist = false;
			for (int i = 0; i<(int)GameView::getInstance()->AllPacItem.size(); ++i)
			{
				if (GameView::getInstance()->AllPacItem.at(i)->has_goods())
				{
					if (GameView::getInstance()->AllPacItem.at(i)->goods().clazz() == GOODS_CLASS_HUIHUNDAN)//Œ‰Ω´”√£®∏¥ªÓ£©
					{
						PackageScene::IdxAndNum * temp = new PackageScene::IdxAndNum();
						temp->index = GameView::getInstance()->AllPacItem.at(i)->id();
						temp->num = 1;
						GameMessageProcessor::sharedMsgProcessor()->sendReq(1302, (void *)m_nGeneralId, temp);
						delete temp;

						isExist = true;
						break;
					}
				}
			}

			if (!isExist)
			{
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5059, (void *)m_nGeneralId);
			}
		}
		else
		{
			//默认为空，点击后弹出武将界面
			if (NewCommerStoryManager::getInstance()->IsNewComer())
			{
				GameView::getInstance()->showAlertDialog(StringDataManager::getString("Genera_hasNoGeneral"));
				return;
			}

			int generalsOpenLevel = 0;
			for (unsigned int i = 0; i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size(); ++i)
			{
				if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() != 1)
					continue;

				if (strcmp(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str(), "btn_generals") == 0)//是武将
				{
					generalsOpenLevel = FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel();
				}
			}
			if (GameView::getInstance()->myplayer->getActiveRole()->level() < generalsOpenLevel)
			{
				return;
			}

			Size s = Director::getInstance()->getVisibleSize();
			if (!MainScene::GeneralsScene)
				return;

			if (MainScene::GeneralsScene->getParent() != NULL)
			{
				MainScene::GeneralsScene->removeFromParentAndCleanup(false);
			}
			GeneralsUI * generalsUI = (GeneralsUI *)MainScene::GeneralsScene;
			GameView::getInstance()->getMainUIScene()->addChild(generalsUI, 0, kTagGeneralsUI);
			generalsUI->setIgnoreAnchorPointForPosition(false);
			generalsUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			generalsUI->setPosition(Vec2(s.width / 2, s.height / 2));

			auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
			//招募
			for (int i = 0; i<3; ++i)
			{
				if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50 + i))
				{
					RecuriteActionItem * tempRecurite = (RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50 + i);
					tempRecurite->cdTime -= GameUtils::millisecondNow() - tempRecurite->lastChangeTime;
				}
			}

			//generalsUI->setToDefaultTab();
			generalsUI->setToTabByIndex(1);

			//武将列表 
			GeneralsUI::generalsListUI->isReqNewly = true;
			GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
			temp->page = 0;
			temp->pageSize = generalsUI->generalsListUI->everyPageNum;
			temp->type = 1;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp);
			delete temp;
			//技能列表
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5055, this);
			//阵法列表
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5071, this);
			//已上阵武将列表 
			GeneralsUI::ReqListParameter * temp1 = new GeneralsUI::ReqListParameter();
			temp1->page = 0;
			temp1->pageSize = generalsUI->generalsListUI->everyPageNum;
			temp1->type = 5;
			GameMessageProcessor::sharedMsgProcessor()->sendReq(5051, temp1);
			delete temp1;

			mainScene->remindOfGeneral();
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void GeneralItem::ClockedFrameEvent(Ref *pSender, Widget::TouchEventType type)
{
	// null
}

void GeneralItem::RefreshSelfStatus()
{
	//refresh status 
	for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();++i)
	{
		auto temp = GameView::getInstance()->generalsInLineList.at(i);
		if (m_nGeneralId == temp->id())
		{
			if (temp->revivetime() <= 0)
			{
				//m_nRemainTime = temp->showlefttime()*1.0f;
				if (temp->showtype() == 0)
				{
					m_nCurStatus = status_InRest;
				}
				else if (temp->showtype() == 1)
				{
					if(temp->showlefttime() <= 0)
					{
						m_nCurStatus = status_InLine;
					}
					else
					{
						m_nCurStatus = status_InBattle;
					}
				}
			}
			else
			{
				//m_nRemainTime = temp->revivetime();
				m_nCurStatus = status_Died;
			}

			break;
		}
	}
}

void GeneralItem::RefreshUI()
{
	//在场停留时间
	if (btn_headFrame->getChildByName("l_remainTimeInBattle"))
	{
		btn_headFrame->getChildByName("l_remainTimeInBattle")->setVisible(true);
	}
	//出战状态时，向上移动
	if (m_nCurStatus == status_InBattle)
	{
		if (this->getActionByTag(ACTION_MOVE_TAG))
		{
			this->stopActionByTag(ACTION_MOVE_TAG);
		}

		MoveTo * moveto = MoveTo::create(0.1f,Vec2(this->getPositionX(),10));
		this->runAction(moveto);
		moveto->setTag(ACTION_MOVE_TAG);

		if (btn_headFrame->getChildByName("imageView_countDownFrame"))
		{
			btn_headFrame->getChildByName("imageView_countDownFrame")->setVisible(true);
		}
		//timeInBattle
		if (btn_headFrame->getChildByName("imageView_countDown"))
		{
			btn_headFrame->getChildByName("imageView_countDown")->setVisible(true);
		}
	}
	else
	{
		if (this->getActionByTag(ACTION_MOVE_TAG))
		{
			this->stopActionByTag(ACTION_MOVE_TAG);
		}

		MoveTo * moveto = MoveTo::create(0.1f,Vec2(this->getPositionX(),0));
		this->runAction(moveto);
		moveto->setTag(ACTION_MOVE_TAG);

		if (btn_headFrame->getChildByName("imageView_countDownFrame"))
		{
			btn_headFrame->getChildByName("imageView_countDownFrame")->setVisible(false);
		}
		//timeInBattle
		if (btn_headFrame->getChildByName("imageView_countDown"))
		{
			btn_headFrame->getChildByName("imageView_countDown")->setVisible(false);
		}
	}

	//休息时间
	if (m_nCurStatus == status_InRest)
	{
		this->RunRestCD();
	}
	else
	{
	}

	//复活等待时间
	if (btn_headFrame->getChildByName("l_waitForRevive"))
	{
		if (m_nCurStatus == status_Died)
		{
			btn_headFrame->getChildByName("l_waitForRevive")->setVisible(true);
			m_bIsRevevidCountFinish = false;
			//btn_headFrame->setCascadeColorEnabled(true);
			//btn_headFrame->setColor(Color3B(100,100,100));
			//头像
			if (btn_headFrame->getChildByName("imageView_head"))
			{
				btn_headFrame->getChildByName("imageView_head")->setColor(Color3B(100,100,100));
			}
		}
		else
		{
			btn_headFrame->getChildByName("l_waitForRevive")->setVisible(false);
			//btn_headFrame->setCascadeColorEnabled(true);
			//btn_headFrame->setColor(Color3B(255,255,255));
			//头像
			if (btn_headFrame->getChildByName("imageView_head"))
			{
				btn_headFrame->getChildByName("imageView_head")->setColor(Color3B(255,255,255));
			}
		}
	}

	//可出战高亮闪烁
	auto pHighlightSpr = (ImageView*)btn_headFrame->getChildByName("pHighlightSpr");
	if(pHighlightSpr)
	{
		if (m_nCurStatus == status_InLine)  //可以出战
		{
			//闪烁(淡入淡出)
			auto sq = Sequence::create(
			 	FadeIn::create(1.0f),
			 	FadeOut::create(0.8f),
			 	NULL
			 	);
			pHighlightSpr->setVisible(true);
			pHighlightSpr->runAction(RepeatForever::create(sq));
			sq->setTag(ActionTag_HighlightImageView);
		}
		else
		{
			pHighlightSpr->stopActionByTag(ActionTag_HighlightImageView);
			pHighlightSpr->setVisible(false);
		}
	}

	auto generalsInfoUI = (GeneralsInfoUI*)GameView::getInstance()->getMainUIScene()->getGeneralInfoUI();
	if(generalsInfoUI)
	{
		generalsInfoUI->ResetAllFadeAction();
	}
}

void GeneralItem::RefreshSkillPresent( CGeneralDetail * generalDetail )
{
	if (m_nGeneralId != generalDetail->generalid())
		return;

	if (btn_headFrame->getChildByName("imageView_skillFrame"))
	{
		auto imageView_skill = (ImageView*)btn_headFrame->getChildByName("imageView_skill");
		if (imageView_skill)
		{
			if (generalDetail->shortcuts_size() > 0)
			{
				for (int i = 0;i<generalDetail->shortcuts_size();i++)
				{
					if (generalDetail->shortcuts(i).storedtype() != 1)
						continue;

					if (!generalDetail->shortcuts(i).has_skillpropid())
						continue;

					std::string skillId = generalDetail->shortcuts(i).skillpropid();
					std::string skillIconPath = "res_ui/jineng_icon/";
					auto baseSkill = StaticDataBaseSkill::s_baseSkillData[skillId];
					if (baseSkill)
					{
						if (baseSkill->usemodel() != 0)
							continue;

						skillIconPath.append(baseSkill->icon());
					}
					else
					{
						skillIconPath.append("jineng_2");
					}

					skillIconPath.append(".png");

					//set
					imageView_skill->loadTexture(skillIconPath.c_str());

					//skill variety icon
					if (btn_headFrame->getChildByName("image_skillVariety") != NULL)
					{
						btn_headFrame->getChildByName("image_skillVariety")->removeFromParent();
					}
					auto image_skillVariety = SkillScene::GetSkillVarirtyImageView(skillId.c_str());
					if (image_skillVariety)
					{
						image_skillVariety->setAnchorPoint(Vec2(0.5f,0.5f));
						image_skillVariety->setPosition(Vec2(btn_headFrame->getBoundingBox().size.width, btn_headFrame->getBoundingBox().size.height));
						btn_headFrame->addChild(image_skillVariety);
						image_skillVariety->setScale(0.9f);
						image_skillVariety->setLocalZOrder(4);
						image_skillVariety->setName("image_skillVariety");
					}
				}
			}
		}
	}
}

void GeneralItem::RefreshGeneralHP( General * general )
{
	return;
	auto imageView_blood = (ImageView*)btn_headFrame->getChildByName("imageView_blood");
	if (!imageView_blood)
		return;

	float h_a = general->getActiveRole()->hp()*1.0f;
	float h_b = general->getActiveRole()->maxhp()*1.0f;
	float blood_scale = h_a/h_b;
	if (blood_scale > 1.0f)
		blood_scale = 1.0f;

	imageView_blood->setTextureRect(Rect(0,0,imageView_blood->getContentSize().width*blood_scale,imageView_blood->getContentSize().height));
}

void GeneralItem::RefreshGeneralHP( CGeneralDetail * generalDetail )
{
	return;
	auto imageView_blood = (ImageView*)btn_headFrame->getChildByName("imageView_blood");
	if (!imageView_blood)
		return;

	float h_a = generalDetail->activerole().hp()*1.0f;
	float h_b = generalDetail->activerole().maxhp()*1.0f;
	float blood_scale = h_a/h_b;
	if (blood_scale > 1.0f)
		blood_scale = 1.0f;

	imageView_blood->setTextureRect(Rect(0,0,imageView_blood->getContentSize().width*blood_scale,imageView_blood->getContentSize().height));

}

void GeneralItem::RunRestCD()
{
	//CD时间
	if (m_nCurStatus == status_InRest)
	{
		auto temp = GeneralsStateManager::getInstance()->getGeneralBaseMsgById(m_nGeneralId);
		if (!temp)
			return;

		CCLOG("RunRestCD");
		float mRemainTime_rest = temp->getRemainTime()*1.0f/1000;

		auto widget_progress = (Widget*)btn_headFrame->getChildByName("widget_progress");
		if (widget_progress)
		{
			auto progressTimer = (ProgressTimer*)widget_progress->getVirtualRenderer()->getChildByTag(PROGRESSTIMER_REST_TAG);
			if (progressTimer)
			{
				long long restTime = 90;
// 				std::map<int,std::string>::const_iterator cIter;
// 				cIter = VipValueConfig::s_vipValue.find(20);
// 				if (cIter == VipValueConfig::s_vipValue.end()) // 没找到就是指向END了  
// 				{
// 
// 				}
// 				else
// 				{
// 					std::string value = VipValueConfig::s_vipValue[1];
// 					int a = atoi(value.c_str());
// 					restTime = a;
// 				}
				restTime = temp->resttimebase()/1000;
				progressTimer->setVisible(true);
				float percentage = mRemainTime_rest*1.0f/restTime;
				//CCLOG("mRemainTime_rest = %.2f",mRemainTime_rest);
				auto action_progress_from_to = ProgressFromTo::create(mRemainTime_rest , percentage*100, 0);     
				auto action_callback = CallFuncN::create(CC_CALLBACK_1(GeneralItem::RestCoolDownCallBack,this));
				progressTimer->runAction(Sequence::create(action_progress_from_to, action_callback, NULL));
			}
		}
	}
}

void GeneralItem::RestCoolDownCallBack( Node* node )
{
	auto widget_progress = (Widget*)btn_headFrame->getChildByName("widget_progress");
	if (widget_progress)
	{
		auto progressTimer = (ProgressTimer*)widget_progress->getVirtualRenderer()->getChildByTag(PROGRESSTIMER_REST_TAG);
		if (progressTimer)
		{
			progressTimer->setVisible(false);
		}
	}
}


