#include "MyHeadInfo.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "HeadMenu.h"
#include "../MainScene.h"
#include "ChatWindows.h"
#include "../../utils/StaticDataManager.h"
#include "ExStatusItem.h"
#include "../exstatus/ExStatus.h"
#include "MyBuff.h"
#include "../../ui/extensions/UITab.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../legend_script/UITutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../SimpleEffectManager.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/element/CFunctionOpenLevel.h"
#include "../../messageclient/element/CMapInfo.h"
#include "AppMacros.h"
#include "../../utils/GameConfig.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "PKModeUI.h"

#define kTagForPKMode 698

MyHeadInfo::MyHeadInfo():
isTabStateOn(false)
{
}


MyHeadInfo::~MyHeadInfo()
{
}


MyHeadInfo* MyHeadInfo::create()
{
	auto myHeadInfo = new MyHeadInfo();
	if (myHeadInfo && myHeadInfo->init())
	{
		myHeadInfo->autorelease();
		return myHeadInfo;
	}
	CC_SAFE_DELETE(myHeadInfo);
	return NULL;
}

bool MyHeadInfo::init()
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();

		auto activeRole = GameView::getInstance()->myplayer->getActiveRole();
		pkModeOpenLevel = 30;
		for(int i = 0;i<(int)FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();++i)
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() != 5)
				continue;

			if (strcmp("btn_pkMode",FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str()) == 0)
			{
				pkModeOpenLevel = FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel();
			}
		}

		layer_pkmode = Layer::create();
		m_pLayer->addChild(layer_pkmode);
		layer_headIndo= Layer::create();
		m_pLayer->addChild(layer_headIndo);

		//和平
		Button_state = Button::create();
		Button_state->setTouchEnabled(true);
		Button_state->setPressedActionEnabled(true);
		Button_state->loadTextures("gamescene_state/zhujiemian3/zhujuetouxiang/heping_di.png", "gamescene_state/zhujiemian3/zhujuetouxiang/heping_di.png", "");
		Button_state->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_state->setPosition(Vec2(198+Button_state->getContentSize().width/2,33+Button_state->getContentSize().height/2));
		Button_state->addTouchEventListener(CC_CALLBACK_2(MyHeadInfo::ButtonStateEvent, this));
		layer_pkmode->addChild(Button_state);
		stateBG = ImageView::create();
		stateBG->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/heping.png");
		stateBG->setAnchorPoint(Vec2(0.5f,0.5f));
		stateBG->setPosition(Vec2(Button_state->getBoundingBox().size.width/2, Button_state->getBoundingBox().size.height/2));
		Button_state->addChild(stateBG);
		applyPKMode(GameView::getInstance()->myplayer->getPKMode());

		//退组
// 		teamInLine = Button::create();
// 		teamInLine->setAnchorPoint(Vec2(0.5f,0.5f));
// 		teamInLine->setTouchEnabled(true);
// 		teamInLine->loadTextures("gamescene_state/zhujiemian3/zhujuetouxiang/tuizu_di.png","gamescene_state/zhujiemian3/zhujuetouxiang/tuizu_di.png","");
// 		teamInLine->addTouchEventListener(CC_CALLBACK_2(MyHeadInfo::callBackExitTeam));
// 		teamInLine->setPosition(Vec2(234,15));
// 		teamInLine->setPressedActionEnabled(true);
// 
// 		auto imageView_quitTeam = ImageView::create();
// 		imageView_quitTeam->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/tuizu.png");
// 		imageView_quitTeam->setAnchorPoint(Vec2(0.5f,0.5f));
// 		imageView_quitTeam->setPosition(Vec2(0,0));
// 		teamInLine->addChild(imageView_quitTeam);
// 		
// 		if(GameView::getInstance()->teamMemberVector.size() >= 2)
// 		{
// 			teamInLine->setVisible(true);
// 		}
// 		else
// 		{
// 			teamInLine->setVisible(false);
// 		}
// 		layer_pkmode->addChild(teamInLine);

// 		auto ImageView_background = ImageView::create();
// 		ImageView_background->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/di.png");
// 		ImageView_background->setName("ImageView_background");
// 		ImageView_background->setAnchorPoint(Vec2(0,0));
// 		ImageView_background->setPosition(Vec2(0,0));
// 		layer_headIndo->addChild(ImageView_background);

		auto Button_Frame = Button::create();
		Button_Frame->loadTextures("gamescene_state/zhujiemian3/zhujuetouxiang/xuetiaodi.png","gamescene_state/zhujiemian3/zhujuetouxiang/xuetiaodi.png","");
		Button_Frame->setName("Button_Frame");
		Button_Frame->setTouchEnabled(true);
		Button_Frame->setPressedActionEnabled(true);
		Button_Frame->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_Frame->setPosition(Vec2(199-Button_Frame->getContentSize().width/2,74-Button_Frame->getContentSize().height/2));
		Button_Frame->addTouchEventListener(CC_CALLBACK_2(MyHeadInfo::ButtonFrameEvent, this));
		layer_headIndo->addChild(Button_Frame);

		//头像
		Button_headFrame = Button::create();
		Button_headFrame->setTouchEnabled(true);
		Button_headFrame->setPressedActionEnabled(true);
		//Button_headFrame->loadTextures(BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str(), BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str(), "");
		Button_headFrame->loadTextures("gamescene_state/zhujiemian3/zhujuetouxiang/touxiang_di.png","gamescene_state/zhujiemian3/zhujuetouxiang/touxiang_di.png", "");
		Button_headFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_headFrame->setPosition(Vec2(Button_headFrame->getContentSize().width/2,Button_headFrame->getContentSize().height/2));
		Button_headFrame->addTouchEventListener(CC_CALLBACK_2(MyHeadInfo::ButtonHeadEvent, this));
		layer_headIndo->addChild(Button_headFrame);
		//head
		auto imageView_head = ImageView::create();
		imageView_head->loadTexture(BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str());
		imageView_head->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_head->setPosition(Vec2(40,43));
		Button_headFrame->addChild(imageView_head);

		auto Label_space = Label::createWithTTF("/", APP_FONT_NAME, 12);
		Label_space->setAnchorPoint(Vec2::ZERO);
		Label_space->setPosition(Vec2(85,33));
		Label_space->setLocalZOrder(100);
		Label_space->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_space);

		char s_maxhp[10];
		sprintf(s_maxhp,"%d",activeRole->maxhp());
		Label_allBlood = Label::createWithTTF(s_maxhp, APP_FONT_NAME, 12);
		Label_allBlood->setAnchorPoint(Vec2::ZERO);
		Label_allBlood->setPosition(Vec2(Label_space->getPosition().x+Label_space->getContentSize().width+3,Label_space->getPosition().y));
		Label_allBlood->setLocalZOrder(100);
		Label_allBlood->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_allBlood);
		
		char s_hp[10];
		//itoa(activeRole->hp(),s,10);
		sprintf(s_hp,"%d",activeRole->hp());
		Label_curBlood = Label::createWithTTF(s_hp, APP_FONT_NAME, 12);
		Label_curBlood->setAnchorPoint(Vec2(1.0f,0));
		Label_curBlood->setPosition(Vec2(Label_space->getPosition().x-3,Label_space->getPosition().y));
		Label_curBlood->setLocalZOrder(100);
		Label_curBlood->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_curBlood);
		
		Label_space = Label::createWithTTF("/", APP_FONT_NAME, 12);
		Label_space->setAnchorPoint(Vec2::ZERO);
		Label_space->setPosition(Vec2(85,20));
		Label_space->setLocalZOrder(100);
		Label_space->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_space);
		
		char s_maxmp[10];
		sprintf(s_maxmp,"%d",activeRole->maxmp());
		Label_allMagic = Label::createWithTTF(s_maxmp, APP_FONT_NAME, 12);
		Label_allMagic->setAnchorPoint(Vec2::ZERO);
		Label_allMagic->setPosition(Vec2(Label_space->getPosition().x+Label_space->getContentSize().width+3,Label_space->getPosition().y));
		Label_allMagic->setLocalZOrder(100);
		Label_allMagic->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_allMagic);

		
		char s_mp[10];
		sprintf(s_mp,"%d",activeRole->mp());
		Label_curMagic = Label::createWithTTF(s_mp, APP_FONT_NAME, 12);
		Label_curMagic->setAnchorPoint(Vec2(1.0f,0));
		Label_curMagic->setPosition(Vec2(Label_space->getPosition().x-3,Label_space->getPosition().y));
		Label_curMagic->setLocalZOrder(100);
		Label_curMagic->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_curMagic);

		
		char s_level[10];
		sprintf(s_level,"%d",activeRole->level());
		Label_level = Label::createWithTTF(s_level, APP_FONT_NAME, 14);
		Label_level->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_level->setPosition(Vec2(14-Button_headFrame->getContentSize().width/2,11-Button_headFrame->getContentSize().height/2));
		Label_level->enableOutline(Color4B::BLACK, 2.0f);
		Button_headFrame->addChild(Label_level);

		auto ImageView_blood_frame = ImageView::create();
		ImageView_blood_frame->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/xuetiao_di.png");
		ImageView_blood_frame->setScale9Enabled(true);
		ImageView_blood_frame->setCapInsets(Rect(7,7,1,1));
		ImageView_blood_frame->setContentSize(Size(118,14));
		ImageView_blood_frame->setAnchorPoint(Vec2(0,0.5f));
		ImageView_blood_frame->setPosition(Vec2(25,41));
		Button_Frame->addChild(ImageView_blood_frame);

		ImageView_blood = ImageView::create();
		ImageView_blood->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/red_xuetiao.png");
		ImageView_blood->setAnchorPoint(Vec2(0,0.5f));
		ImageView_blood->setPosition(Vec2(25,41));
		char * a = (char *)Label_curBlood->getString().c_str();
		char * b = (char *)Label_allBlood->getString().c_str();
		float _a = atof(a);
		float _b = atof(b);
		float blood_scale = _a/_b;
		if (blood_scale > 1.0f)
			blood_scale = 1.0f;

		ImageView_blood->setTextureRect(Rect(0,0,ImageView_blood->getContentSize().width*blood_scale,ImageView_blood->getContentSize().height));
		Button_Frame->addChild(ImageView_blood);
		ImageView_magic = ImageView::create();
		ImageView_magic->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/blue_xuetiao.png");
		ImageView_magic->setAnchorPoint(Vec2(0,0.5f));
		ImageView_magic->setScale9Enabled(true);
		ImageView_magic->setPosition(Vec2(26,28));
		char * c = (char *)Label_curMagic->getString().c_str();
		char * d = (char *)Label_allMagic->getString().c_str();
		float _c = atof(c);
		float _d = atof(d);
		float magic_scale = _c/_d;
		if (magic_scale > 1.0f)
			magic_scale = 1.0f;

		ImageView_magic->setTextureRect(Rect(0,0,ImageView_magic->getContentSize().width*magic_scale,ImageView_magic->getContentSize().height));
		Button_Frame->addChild(ImageView_magic);

		// 体力
		auto Label_space_phy = Label::createWithTTF("/", APP_FONT_NAME, 12);
		Label_space_phy->setAnchorPoint(Vec2::ZERO);
		Label_space_phy->setPosition(Vec2(85,5));
		Label_space_phy->setLocalZOrder(100);
		Label_space_phy->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_space_phy);

		
		char s_maxPhy[10];
		sprintf(s_maxPhy,"%d",GameView::getInstance()->myplayer->getPhysicalCapacity());
		Label_allPhy = Label::createWithTTF(s_maxPhy, APP_FONT_NAME, 12);
		Label_allPhy->setAnchorPoint(Vec2::ZERO);
		Label_allPhy->setPosition(Vec2(Label_space_phy->getPosition().x+Label_space_phy->getContentSize().width+3,Label_space_phy->getPosition().y));
		Label_allPhy->setLocalZOrder(100);
		Label_allPhy->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_allPhy);

		char s_phy[10];
		sprintf(s_phy,"%d",GameView::getInstance()->myplayer->getPhysicalValue());
		Label_curPhy = Label::createWithTTF(s_phy, APP_FONT_NAME, 12);
		Label_curPhy->setAnchorPoint(Vec2(1.0f,0));
		Label_curPhy->setPosition(Vec2(Label_space_phy->getPosition().x-3,Label_space_phy->getPosition().y));
		Label_curPhy->setLocalZOrder(100);
		Label_curPhy->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_curPhy);

		ImageView_phy = ImageView::create();
		ImageView_phy->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/green_xuetiao.png");
		ImageView_phy->setAnchorPoint(Vec2(0,0.5f));
		ImageView_phy->setPosition(Vec2(29,13));
		char * a_phy = (char *)Label_curPhy->getString().c_str();
		char * b_phy = (char *)Label_allPhy->getString().c_str();
		float _a_phy = atof(a_phy);
		float _b_phy = atof(b_phy);
		float phy_scale = _a_phy/_b_phy;
		if (phy_scale > 1.0f)
			phy_scale = 1.0f;

		ImageView_phy->setTextureRect(Rect(0,0,ImageView_phy->getContentSize().width*phy_scale,ImageView_phy->getContentSize().height));
		Button_Frame->addChild(ImageView_phy);

		for (int i = 0;i<(int)GameView::getInstance()->myplayer->getExStatusVector().size();++i)
		{
			if (i>4)
				break;

			auto tempExstatus = GameView::getInstance()->myplayer->getExStatusVector().at(i);
			auto exStatusItem = ExStatusItem::create(tempExstatus);
			exStatusItem->setScale(0.5f);
			exStatusItem->setAnchorPoint(Vec2(0,0));
			exStatusItem->setPosition(Vec2(80+22*i,2));
			exStatusItem->setTag(1000+i);
			addChild(exStatusItem);
		}

		// vipInfo
		ImageView_Vip = ImageView::create();
		ImageView_Vip->loadTexture("");
		ImageView_Vip->setAnchorPoint(Vec2(0.5f,0.5f));
		ImageView_Vip->setPosition(Vec2(13-Button_headFrame->getContentSize().width/2,61-Button_headFrame->getContentSize().height/2));
		Button_headFrame->addChild(ImageView_Vip);

		lbt_vipLevel = Label::createWithBMFont("res_ui/font/ziti_3.fnt", "");
		lbt_vipLevel->setScale(0.7f);
		lbt_vipLevel->setAnchorPoint(Vec2(0.5f,0.5f));
		//lbt_vipLevel->setPosition(Vec2(27-Button_headFrame->getContentSize().width/2,56-Button_headFrame->getContentSize().height/2));
		lbt_vipLevel->setPosition(Vec2(Button_headFrame->getContentSize().width / 2, Button_headFrame->getContentSize().height / 2));
		Button_headFrame->addChild(lbt_vipLevel);

		// vip开关控制
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (vipEnabled)
		{
			ImageView_Vip->setVisible(true);
			lbt_vipLevel->setVisible(true);
			ReloadVipInfo();
		}
		else
		{
			ImageView_Vip->setVisible(false);
			lbt_vipLevel->setVisible(false);
		}

		this->setIgnoreAnchorPointForPosition(false);
		this->setAnchorPoint(Vec2(0,0));
		////this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(199,74));

		return true;
	}
	return false;
}

void MyHeadInfo::onEnter()
{
	UIScene::onEnter();
}

void MyHeadInfo::onExit()
{
	UIScene::onExit();
}

bool MyHeadInfo::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return false;

	Vec2 location = pTouch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		Rect rect_l(pos.x, pos.y, 66, size.height);
		Rect rect_r(pos.x+66, pos.y+31, size.width-80, size.height-31);
		if (rect_l.containsPoint(location))
		{
			this->ButtonHeadEvent(Button_headFrame, Widget::TouchEventType::ENDED);
		}
		else if (rect_r.containsPoint(location))
		{
			//弹出Buff列表
			Size winSize = Director::getInstance()->getVisibleSize();
			auto myBuff = MyBuff::create();
			mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
			myBuff->setIgnoreAnchorPointForPosition(false);
			myBuff->setAnchorPoint(Vec2(0,1.0f));
			myBuff->setPosition(Vec2(this->getContentSize().width,winSize.height - this->getContentSize().height));
			mainScene->addChild(myBuff,0,kTagMyBuff);
		}
		
		return true;
	}
	//this->removeFromParent();
	return false;
}

void MyHeadInfo::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void MyHeadInfo::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void MyHeadInfo::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void MyHeadInfo::ReloadMyPlayerData()
{
	auto activeRole = GameView::getInstance()->myplayer->getActiveRole();
	char s_maxhp[20];
	sprintf(s_maxhp,"%d",activeRole->maxhp());
	Label_allBlood->setString(s_maxhp);

	char s_hp[20];
	sprintf(s_hp,"%d",activeRole->hp());
	Label_curBlood->setString(s_hp);

	char s_maxmp[20];
	sprintf(s_maxmp,"%d",activeRole->maxmp());
	Label_allMagic->setString(s_maxmp);

	char s_mp[20];
	sprintf(s_mp,"%d",activeRole->mp());
	Label_curMagic->setString(s_mp);

	char s_level[20];
	sprintf(s_level,"%d",activeRole->level());
	Label_level->setString(s_level);

	char * a = (char *)Label_curBlood->getString().c_str();
	char * b = (char *)Label_allBlood->getString().c_str();
	float _a = atof(a);
	float _b = atof(b);
	float blood_scale = _a/_b;
	if (blood_scale > 1.0f)
		blood_scale = 1.0f;

	ImageView_blood->setTextureRect(Rect(0,0,ImageView_blood->getContentSize().width*blood_scale,ImageView_blood->getContentSize().height));

	char * c = (char *)Label_curMagic->getString().c_str();
	char * d = (char *)Label_allMagic->getString().c_str();
	float _c = atof(c);
	float _d = atof(d);
	blood_scale = _c/_d;
	if (blood_scale > 1.0f)
		blood_scale = 1.0f;

	ImageView_magic->setTextureRect(Rect(0,0,ImageView_magic->getContentSize().width*blood_scale,ImageView_magic->getContentSize().height));

	// 体力
	char s_maxPhy[10];
	sprintf(s_maxPhy,"%d",GameView::getInstance()->myplayer->getPhysicalCapacity());
	Label_allPhy->setString(s_maxPhy);
	char s_phy[10];
	sprintf(s_phy,"%d",GameView::getInstance()->myplayer->getPhysicalValue());
	Label_curPhy->setString(s_phy);

	char * e = (char *)Label_curPhy->getString().c_str();
	char * f = (char *)Label_allPhy->getString().c_str();
	float _e = atof(e);
	float _f = atof(f);
	float phy_scale = _e/_f;
	if (phy_scale > 1.0f)
		phy_scale = 1.0f;

	ImageView_phy->setTextureRect(Rect(0,0,ImageView_phy->getContentSize().width*phy_scale,ImageView_phy->getContentSize().height));
}

void MyHeadInfo::ButtonHeadEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//竞技场中不需要
		if (GameView::getInstance()->getMapInfo()->maptype() == coliseum)
			return;

		mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
		mainScene->ButtonHeadEvent(pSender);

		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void MyHeadInfo::ButtonLVTeamEvent( Ref *pSender )
{
	//add by yangjun 2014.10.13
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	//mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
	mainScene->ButtonLVTeamEvent(pSender, Widget::TouchEventType::ENDED);
}

void MyHeadInfo::ReloadBuffData()
{
	for (int i = 0;i<5;++i)
	{
		if (this->getChildByTag(1000+i))
		{
			auto exStatusItem = (ExStatusItem *)this->getChildByTag(1000+i);
			exStatusItem->removeFromParent();
		}
		else break;
	}

	for (int i = 0;i<(int)GameView::getInstance()->myplayer->getExStatusVector().size();++i)
	{
		if (i>4)
			break;

		auto tempExstatus = GameView::getInstance()->myplayer->getExStatusVector().at(i);

		if (tempExstatus->type()== 37 ||tempExstatus->type() == 38 ||tempExstatus->type() == 39)
			continue;

		auto exStatusItem = ExStatusItem::create(tempExstatus);
		exStatusItem->setScale(0.5f);
		exStatusItem->setAnchorPoint(Vec2(0,0));
		exStatusItem->setPosition(Vec2(80+22*i,2));
		exStatusItem->setTag(1000+i);
		addChild(exStatusItem);
	}
}

void MyHeadInfo::ButtonStateEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		if (GameView::getInstance()->myplayer->getActiveRole()->level() < pkModeOpenLevel)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("pkmode_will_be_open"));
			return;
		}

		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		if (GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::guildFight)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("pkmode_will_not_openInFamilyFight"));
			return;
		}

		// 	if (isTabStateOn == true)
		// 	{
		// 		//Tab_state->removeFromParent();
		// 		if (this->getChildByTag(kTagForPKMode))
		// 			(this->getChildByTag(kTagForPKMode))->removeFromParent();
		// 
		// 		isTabStateOn = false;
		// 	}
		// 	else
		// 	{
		// 		Layer* layer_temp = Layer::create();
		// 		addChild(layer_temp);
		// 		layer_temp->setTag(kTagForPKMode);
		// 		//弹出下拉列表
		// 		char * TabNames[] = {"gamescene_state/zhujiemian3/zhujuetouxiang/heping.png",
		// 			"gamescene_state/zhujiemian3/zhujuetouxiang/pk.png",
		// 			"gamescene_state/zhujiemian3/zhujuetouxiang/family.png",
		// 			"gamescene_state/zhujiemian3/zhujuetouxiang/country.png"};
		// 		Tab_state = UITab::createWithImage(4,"gamescene_state/zhujiemian3/zhujuetouxiang/tuizu_di.png","gamescene_state/zhujiemian3/zhujuetouxiang/tuizu_di.png","",TabNames,VERTICAL_LIST,5);
		// 		Tab_state->setAnchorPoint(Vec2(0,0));
		// 		Tab_state->setPosition(Vec2(((Button*)pSender)->getPosition().x - ((Button*)pSender)->getContentSize().width/2+6,((Button*)pSender)->getPosition().y-((Button*)pSender)->getContentSize().height/2));
		// 		Tab_state->setHighLightImage("gamescene_state/zhujiemian3/zhujuetouxiang/tuizu_di.png");
		// 		Tab_state->setDefaultPanelByIndex(0);
		// 		Tab_state->addIndexChangedEvent(this,coco_indexchangedselector(MyHeadInfo::TabStateIndexChangedEvent));
		// 		Tab_state->setAutoClose(true);
		// 		layer_temp->addChild(Tab_state);
		// 		isTabStateOn = true;
		// 	}

		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagForPKMode) == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			auto modeUI = PKModeUI::create();
			modeUI->setIgnoreAnchorPointForPosition(false);
			modeUI->setAnchorPoint(Vec2(0.f, 1.0f));
			modeUI->setPosition(Vec2(((Button*)pSender)->getPosition().x - ((Button*)pSender)->getContentSize().width / 2 + 6, winSize.height - ((Button*)pSender)->getContentSize().height));
			modeUI->setTag(kTagForPKMode);
			GameView::getInstance()->getMainUIScene()->addChild(modeUI);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void MyHeadInfo::applyPKMode(int PKMode)
{
	switch(PKMode)
	{
	case 0: 
		stateBG->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/heping.png");
		break;
	case 1: 
		stateBG->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/pk.png");
		break;
	case 2: 
		stateBG->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/family.png");
		break;
	case 3: 
		stateBG->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/country.png");
		break;
	}
}

void MyHeadInfo::TabStateIndexChangedEvent( Ref* pSender )
{
	//CCLOG("TabStateIndexChangedEvent");
// 	int index = Tab_state->getCurrentIndex();
// 
// 	if (index == 2)//family
// 	{
// 		if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().factionid() == 0)
// 		{
// 			const char *dialog_family = StringDataManager::getString("PKMode_family_dialog");
// 			char* pkMode_dialog_family =const_cast<char*>(dialog_family);
// 			GameView::getInstance()->showAlertDialog(pkMode_dialog_family);
// 			return;
// 		}
// 	}
// 
// 	GameMessageProcessor::sharedMsgProcessor()->sendReq(1207, (void*)index);
// 
// 	applyPKMode(index);
// 
// 	isTabStateOn = false;
// 	//layer_pkmode->removeWidgetAndCleanUp(Tab_state,false);
// 	//Tab_state->removeFromParent();
// 	if (this->getChildByTag(kTagForPKMode))
// 		(this->getChildByTag(kTagForPKMode))->removeFromParent();
}

void MyHeadInfo::callBackExitTeam( Ref * obj )
{
	int teamWork=3;
	int playerId=0;
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1406,(void *)teamWork,(void *)playerId);
}

void MyHeadInfo::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void MyHeadInfo::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,30,70,true);
// 	tutorialIndicator->setPosition(Vec2(pos.x+45,pos.y-100));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,76,76,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+76/2,pos.y+76/2));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
	 	mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void MyHeadInfo::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,35,35,true);
// 	tutorialIndicator->setPosition(Vec2(pos.x+45,pos.y-85));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,60,30,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+234,pos.y+56));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void MyHeadInfo::removeCCTutorialIndicator()
{
	auto tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		auto teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void MyHeadInfo::ReloadVipInfo()
{
	if (GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel() > 0)
	{
		ImageView_Vip->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/vip1.png");
		char s_level[10];
		sprintf(s_level,"%d",GameView::getInstance()->myplayer->getActiveRole()->playerbaseinfo().viplevel());
		lbt_vipLevel->setString(s_level);
	}
	else
	{
		ImageView_Vip->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/vip0.png");
		lbt_vipLevel->setString("");
	}
}

void MyHeadInfo::ButtonFrameEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//弹出Buff列表
		Size winSize = Director::getInstance()->getVisibleSize();
		mainScene = (MainScene *)GameView::getInstance()->getMainUIScene();
		if (mainScene->getChildByTag(kTagMyBuff) == NULL)
		{
			auto myBuff = MyBuff::create();
			myBuff->setIgnoreAnchorPointForPosition(false);
			myBuff->setAnchorPoint(Vec2(0, 1.0f));
			myBuff->setPosition(Vec2(this->getContentSize().width, winSize.height - this->getContentSize().height));
			mainScene->addChild(myBuff, 0, kTagMyBuff);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

