#include "GuideMap.h"

#include "../GameSceneState.h"
#include "GameView.h"
#include "../role/MyPlayer.h"
#include "../../ui/mapscene/MapScene.h"
#include "../../ui/mapscene/AreaMap.h"
#include "../../ui/FivePersonInstance/InstanceDetailUI.h"
#include "../../ui/FivePersonInstance/FivePersonInstance.h"
#include "../../ui/FivePersonInstance/InstanceFunctionPanel.h"
#include "../../ui/robot_ui/RobotMainUI.h"
#include "../MainScene.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/UISceneTest.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/ScriptManager.h"
#include "../../legend_script/Script.h"
#include "../role/MyPlayerAIConfig.h"
#include "../role/MyPlayerAI.h"
#include "../role/MyPlayerSimpleAI.h"
#include "../../ui/shop_ui/ShopUI.h"
#include "../../ui/goldstore_ui/GoldStoreUI.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/element/CFunctionOpenLevel.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "AppMacros.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../../ui/OnlineReward_ui/OnlineGiftIconWidget.h"
#include "../../ui/Active_ui/ActiveUI.h"
#include "../../ui/OnlineReward_ui/OnlineGiftData.h"
#include "../../ui/Active_ui/ActiveShopData.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/Rank_ui/RankUI.h"
#include "../../ui/offlineExp_ui/OffLineExpUI.h"
#include "../../ui/vip_ui/FirstBuyVipUI.h"
#include "../../ui/Question_ui/QuestionUI.h"
#include "../../ui/Question_ui/QuestionData.h"
#include "../../messageclient/protobuf/PlayerMessage.pb.h"
#include "../../utils/GameConfig.h"
#include "../../ui/Mail_ui/MailFriend.h"
#include "../../ui/Chat_ui/AddPrivateUi.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "../../ui/challengeRound/SingCopyInstance.h"
#include "../../utils/GameUtils.h"
#include "../../ui/challengeRound/SingCopyCountDownUI.h"
#include "../../ui/family_ui/familyFight_ui/BattleSituationUI.h"
#include "../../ui/Active_ui/ActiveIconWidget.h"
#include "../../ui/FivePersonInstance/InstanceMapUI.h"
#include "../../ui/Reward_ui/RewardUi.h"
#include "GeneralsModeUI.h"
#include "../../legend_script/CCTeachingGuide.h"
#include "MissionAndTeam.h"

#define kTagActionLayer 951
#define KTagGeneralsModeUI 952

GuideMap::GuideMap():
m_generalMode(1),
m_autoFight(2),
m_nRemainTime(600),
isExistUIForMainScene(false),
isExistUIForFiveInstanceScene(false),
isExistUIForArenaScene(false),
isExistUIForFamilyFightScene(false),
isExistUIForSingCopyScene(false)
{

	if (MyPlayerAIConfig::getAutomaticSkill() == 1)
	{
		isOpenSkillMonster = true;
	}
	

	Size winSize = Director::getInstance()->getVisibleSize();
	//2行5列
	for (int m = 1;m>=0;m--)
	{
		for (int n = 4;n>=0;n--)
		{
			int _m = 1-m;
			int _n = 4-n;

			GMCoordinate temp ;
			temp.x = winSize.width-70-_n*53;
			temp.y = winSize.height-70-_m*53;
			CoordinateVector.push_back(temp);
		}
	}
}


GuideMap::~GuideMap()
{
	std::vector<CFunctionOpenLevel*>::iterator _iter_1;
	for (_iter_1 = GMOpendedFunctionVector_1.begin(); _iter_1 != GMOpendedFunctionVector_1.end(); ++_iter_1)
	{
		delete *_iter_1;
	}
	GMOpendedFunctionVector_1.clear();

	std::vector<CFunctionOpenLevel*>::iterator _iter;
	for (_iter = GMOpendedFunctionVector.begin(); _iter != GMOpendedFunctionVector.end(); ++_iter)
	{
		delete *_iter;
	}
	GMOpendedFunctionVector.clear();
}

bool GMSortByPositionIndex(CFunctionOpenLevel * fcOpenLevel1 , CFunctionOpenLevel *fcOpenLevel2)
{
	return (fcOpenLevel1->get_positionIndex() < fcOpenLevel2->get_positionIndex());
}


void GuideMap::onEnter()
{
	UIScene::onEnter();
}

void GuideMap::onExit()
{
	UIScene::onExit();
}


GuideMap * GuideMap::create()
{
	GuideMap * guideMap = new GuideMap();
	if (guideMap && guideMap->init())
	{
		guideMap->autorelease();
		return guideMap;
	}
	CC_SAFE_DELETE(guideMap);
	return NULL;
}

bool GuideMap::init()
{
	if (UIScene::init())
	{

		//setSceneType(GameView::getInstance()->getMapInfo()->maptype());

		u_layer_MainScene = Layer::create();
		addChild(u_layer_MainScene);
		u_layer_FiveInstanceScene = Layer::create();
		addChild(u_layer_FiveInstanceScene);
		u_layer_ArenaScene = Layer::create();
		addChild(u_layer_ArenaScene);
		u_layer_FamilyFightScene = Layer::create();
		addChild(u_layer_FamilyFightScene);
		u_layer_SingCopyScene = Layer::create();
		addChild(u_layer_SingCopyScene);

// 		createUIForMainScene();
// 		createUIForFiveInstanceScene();
// 		createUIForArenaScene();
// 		createUIForFamilyFightScene();

		changeUIBySceneType(GameView::getInstance()->getMapInfo()->maptype());

		this->schedule(schedule_selector(GuideMap::update),1.0f);
		this->setAnchorPoint(Vec2(0,0));
		this->setPosition(Vec2(0,0));
		this->setContentSize(winSize);
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(winSize);
		
		return true;
	}
	return false;
}

bool GuideMap::onTouchBegan(Touch *pTouch, Event *pEvent)
{
		return false;
}
void GuideMap::onTouchMoved(Touch *pTouch, Event *pEvent)
{

}
void GuideMap::onTouchEnded(Touch *pTouch, Event *pEvent)
{

}
void GuideMap::onTouchCancelled(Touch *pTouch, Event *pEvent)
{

}

void GuideMap::InstanceEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		MainScene* mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
		CCAssert(mainLayer != NULL, "should not be nil");

		//add by yangjun 2014.10.13
		if (mainLayer)
		{
			if (mainLayer->isHeadMenuOn)
			{
				mainLayer->ButtonHeadEvent(NULL);
			}
		}

		// the player has already been in the instance map
		if (FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
		{
			// 		if(mainLayer->getChildByTag(kTagInstanceFunctionPanel) != NULL)
			// 		{
			// 			mainLayer->getChildByTag(kTagInstanceFunctionPanel)->removeFromParent();
			// 		}
			// 		else
			// 		{
			// 			InstanceFunctionPanel* _ui=InstanceFunctionPanel::create();
			// 			_ui->setIgnoreAnchorPointForPosition(false);
			// 			_ui->setAnchorPoint(Vec2(1.0f,1.0f));
			// 			_ui->setPosition(Vec2(btn_instance->getPosition().x-20, btn_instance->getPosition().y+20));
			// 			_ui->setTag(kTagInstanceFunctionPanel);
			// 			mainLayer->addChild(_ui);
			// 		}
		}
		else
		{
			// 		if(mainLayer->getChildByTag(kTagInstanceMainUI) == NULL)
			// 		{
			// 			InstanceDetailUI * _ui=InstanceDetailUI::create();
			// 			_ui->setIgnoreAnchorPointForPosition(false);
			// 			_ui->setAnchorPoint(Vec2(0.5f,0.5f));
			// 			_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));
			// 			_ui->setTag(kTagInstanceMainUI);
			// 			mainLayer->addChild(_ui);
			// 		}

			if (mainLayer->getChildByTag(kTagInstanceMapUI) == NULL)
			{
				InstanceMapUI * _ui = InstanceMapUI::create();
				_ui->setIgnoreAnchorPointForPosition(false);
				_ui->setAnchorPoint(Vec2(0.5f, 0.5f));
				_ui->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
				_ui->setTag(kTagInstanceMapUI);
				mainLayer->addChild(_ui);
			}
		}

		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}


void GuideMap::CallFriendsEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Size winSize = Director::getInstance()->getVisibleSize();
		MainScene* mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
		CCAssert(mainLayer != NULL, "should not be nil");

		//add by yangjun 2014.10.13
		if (mainLayer)
		{
			if (mainLayer->isHeadMenuOn)
			{
				mainLayer->ButtonHeadEvent(NULL);
			}
		}

		// the player has already been in the instance map
		if (FivePersonInstance::getStatus() == FivePersonInstance::status_in_instance)
		{
			if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagPopFriendListUI) == NULL)
			{
				Size winsize = Director::getInstance()->getVisibleSize();
				MailFriend * mailFriend = MailFriend::create(kTagInstanceDetailUI);
				mailFriend->setIgnoreAnchorPointForPosition(false);
				mailFriend->setAnchorPoint(Vec2(0.5f, 0.5f));
				mailFriend->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
				GameView::getInstance()->getMainUIScene()->addChild(mailFriend, 0, kTagPopFriendListUI);
				AddPrivateUi::FriendList friend1 = { 0,20,0,1 };
				GameMessageProcessor::sharedMsgProcessor()->sendReq(2202, &friend1);
			}
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}



}

void GuideMap::ExitEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		GameView::getInstance()->showPopupWindow(StringDataManager::getString("fivePerson_areYouSureToExit"), 2, this, CC_CALLFUNCO_SELECTOR(GuideMap::sureToExit), NULL, PopupWindow::KTypeRemoveByTime, 10);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GuideMap::sureToExit( Ref *pSender )
{
	//add by yangjun 2014.10.13
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1924);
}

void GuideMap::ExitSingCopy(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		GameView::getInstance()->showPopupWindow(StringDataManager::getString("singCopy_areYouSureToExit"), 2, this, CC_CALLFUNCO_SELECTOR(GuideMap::surToExitSingCopy), NULL, PopupWindow::KTypeRemoveByTime, 10);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void GuideMap::surToExitSingCopy( Ref * obj )
{
	auto missionAndTeam = (MissionAndTeam *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMissionAndTeam);
	if (missionAndTeam)
	{
		missionAndTeam->setStateIsRobot(false);
		missionAndTeam->unschedule(CC_SCHEDULE_SELECTOR(MissionAndTeam::updateSingCopyCheckMonst));
	}

	int m_clazz = SingCopyInstance::getCurSingCopyClazz();
	int m_level = SingCopyInstance::getCurSingCopyLevel();
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1902,(void *)m_clazz,(void *)m_level);
}

void GuideMap::HangUpEvent(Ref *pSender, Widget::TouchEventType type)
{
	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		for (std::size_t i = 0; i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size(); ++i)
		{
			if (strcmp(((Button*)pSender)->getName().c_str(), FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionName().c_str()) == 0)
			{
				//headMenu->addNewFunction(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i));
				if (GameView::getInstance()->myplayer->getActiveRole()->level() < FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel())
				{
					GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillui_nothavefuction"));
					return;
				}
			}
		}

		auto mainLayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
		CCAssert(mainLayer != NULL, "should not be nil");
		//if (isOpenSkillMonster==false)
		if (MyPlayerAIConfig::getAutomaticSkill() == 0)
		{
			// 		if (getBtnHangUp())
			// 		{
			// 			getBtnHangUp()->loadTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
			// 		}
			setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png");

			const char *strings_starSkill = StringDataManager::getString("robotui_StarSkillMonster");
			GameView::getInstance()->showAlertDialog(strings_starSkill);
			//set automatic skill
			MyPlayerAIConfig::setAutomaticSkill(1);
			//set star robot
			MyPlayerAIConfig::enableRobot(true);
			GameView::getInstance()->myplayer->getMyPlayerAI()->start();

			isOpenSkillMonster = true;
		}
		else
		{
			// 		if (getBtnHangUp())
			// 		{
			// 			getBtnHangUp()->loadTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png","gamescene_state/zhujiemian3/tubiao/on_hook.png","");
			// 		}
			setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");

			const char *strings_stopSkill = StringDataManager::getString("robotui_StopSkillMonster");
			GameView::getInstance()->showAlertDialog(strings_stopSkill);
			// 停止自动攻击，但挂机并不停止，可以继续吃药等挂机行为
			MyPlayerAIConfig::setAutomaticSkill(0);

			// 停止自动攻击，但会将玩家锁定的一个危险目标打死为止
			MyPlayer* me = GameView::getInstance()->myplayer;
			GameActor* pActor = me->getLockedActor();
			if (pActor != NULL)
			{
				BaseFighter* lockedFighter = dynamic_cast<BaseFighter*>(pActor);
				if (lockedFighter != NULL && lockedFighter->getChildByTag(GameActor::kTagDangerCircle) != NULL)
				{
					GameView::getInstance()->myplayer->getSimpleAI()->startKill(lockedFighter->getRoleId());
				}
			}

			isOpenSkillMonster = false;
		}

		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void GuideMap::ShopEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		for (std::size_t i = 0; i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size(); ++i)
		{
			if (strcmp(((Button*)pSender)->getName().c_str(), FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionName().c_str()) == 0)
			{
				//headMenu->addNewFunction(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i));
				if (GameView::getInstance()->myplayer->getActiveRole()->level() < FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel())
				{
					GameView::getInstance()->showAlertDialog(StringDataManager::getString("skillui_nothavefuction"));
					return;
				}
			}
		}

		GoldStoreUI *goldStoreUI = (GoldStoreUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGoldStoreUI);
		if (goldStoreUI == NULL)
		{
			Size winSize = Director::getInstance()->getVisibleSize();
			goldStoreUI = GoldStoreUI::create();
			GameView::getInstance()->getMainUIScene()->addChild(goldStoreUI, 0, kTagGoldStoreUI);

			goldStoreUI->setIgnoreAnchorPointForPosition(false);
			goldStoreUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			goldStoreUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		}
		//  	
		// 	if (QuestionData::instance()->get_hasAnswerQuestion() >= QuestionData::instance()->get_allQuestion())
		// 	{
		// 		GameView::getInstance()->showAlertDialog(StringDataManager::getString("QuestionUI_hasNoNumber"));
		// 	}
		// 	else
		// 	{
		// 		Size winSize = Director::getInstance()->getVisibleSize();
		// 		QuestionUI * pTmpQuestionUI = (QuestionUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagQuestionUI);
		// 		if (NULL == pTmpQuestionUI)
		// 		{
		// 			QuestionUI * pQuestionUI = QuestionUI::create();
		// 			pQuestionUI->setIgnoreAnchorPointForPosition(false);
		// 			pQuestionUI->setAnchorPoint(Vec2(0.5f, 0.5f));
		// 			pQuestionUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
		// 			pQuestionUI->setTag(kTagQuestionUI);
		// 			pQuestionUI->requestQuestion();				// 向服务器请求题目
		// 
		// 			GameView::getInstance()->getMainUIScene()->addChild(pQuestionUI);
		// 		}
		// 	}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void GuideMap::ActivityEvent(Ref *pSender, Widget::TouchEventType type)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
}

void GuideMap::GiftEvent(Ref *pSender, Widget::TouchEventType type)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
}

void GuideMap::ActiveDegreeEvent(Ref *pSender, Widget::TouchEventType type)
{
	//GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));

	//ActiveUI* pTmpActiveUI = (ActiveUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagActiveUI);
	//if (NULL == pTmpActiveUI)
	//{
	//	ActiveUI * pActiveUI = ActiveUI::create();
	//	pActiveUI->setIgnoreAnchorPointForPosition(false);
	//	pActiveUI->setAnchorPoint(Vec2(0.5f, 0.5f));
	//	pActiveUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
	//	pActiveUI->setTag(kTagActiveUI);
	//	
	//	GameView::getInstance()->getMainUIScene()->addChild(pActiveUI);

	//	pActiveUI->initActiveUIData();

	//	// 请求 活跃度商店数据（当活跃度商店数据不为空时，说明已经请求过服务器数据，那么不再发送请求）
	//	if (ActiveShopData::instance()->m_vector_activeShopSource.empty())
	//	{
	//		// 请求服务器领取奖品
	//		GameMessageProcessor::sharedMsgProcessor()->sendReq(5114, NULL);
	//	}
	//}
}

void GuideMap::EveryDayGiftEvent(Ref *pSender, Widget::TouchEventType type)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
}

void GuideMap::NewServiceEvent(Ref *pSender, Widget::TouchEventType type)
{
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("feature_will_be_open"));
}

void GuideMap::update(float dt)
{
// 	if (u_layer_ArenaScene->isVisible())
// 	{
// 		if (u_layer_ArenaScene->getChildByName("l_remainTimeValue"))
// 		{
// 			m_nRemainTime--;
// 			l_remainTimeValue->setText(this->timeFormatToString(m_nRemainTime).c_str());
// 
// 			if (m_nRemainTime <= 0)
// 			{
// 				//退出竞技场
// 				GameMessageProcessor::sharedMsgProcessor()->sendReq(3021);
// 			}
// 		}
// 	}
}

void GuideMap::setSceneType(int mapType)
{
	//m_curSceneType = sceneType;
	changeUIBySceneType(mapType);
}

//teach learn
void GuideMap::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void GuideMap::addCCTutorialIndicator( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(Vec2(pos.x-60,pos.y-80));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,54,54,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x,pos.y));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void GuideMap::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	if (this->getActionLayer())
	{
// 		CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 		tutorialIndicator->setPosition(Vec2(pos.x-60,pos.y-80));
// 		tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 		this->getActionLayer()->addChild(tutorialIndicator);
		auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,54,54,true);
		tutorialIndicator->setDrawNodePos(Vec2(pos.x,pos.y));
		tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
		}
	}
}

void GuideMap::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	if (this->getActionLayer())
	{
		CCTutorialIndicator* tutorialIndicator1 = dynamic_cast<CCTutorialIndicator*>(this->getActionLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(tutorialIndicator1 != NULL)
			tutorialIndicator1->removeFromParent();
	}

	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}

}

void GuideMap::GoToMapScene(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		//地图界面入口
		if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagMap) == NULL)
		{
			Size s = Director::getInstance()->getVisibleSize();
			MapScene * mapLayer = MapScene::create();
			GameView::getInstance()->getMainUIScene()->addChild(mapLayer, 0, kTagMap);
			mapLayer->setIgnoreAnchorPointForPosition(false);
			mapLayer->setAnchorPoint(Vec2(0.5f, 0.5f));
			mapLayer->setPosition(Vec2(s.width / 2, s.height / 2));
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GuideMap::addNewFunctionForArea1( Node * pNode,void * functionOpenLevel)
{
// 	if (GameView::getInstance()->getMapInfo()->maptype() == coliseum||GameView::getInstance()->getMapInfo()->maptype() == 6)
// 		return;

	//地图1号区域
	if (u_layer_MainScene->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))
	{
		(u_layer_MainScene->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setVisible(true);
	}
}

void GuideMap::addNewFunctionForArea2( Node * pNode,void * functionOpenLevel)
{
// 	if (GameView::getInstance()->getMapInfo()->maptype() == coliseum||GameView::getInstance()->getMapInfo()->maptype() == 6)
// 		return;

	if (strcmp(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str(),"btn_offLineArena") == 0)  //竞技场开启了
	{
		GameMessageProcessor::sharedMsgProcessor()->sendReq(3000);
	}

	//地图2号区域
	auto l_al = (Layer *)u_layer_MainScene->getChildByTag(kTagActionLayer);
	int lastIndex = -5;
	for (int i = 0;i<GMOpendedFunctionVector.size();++i)
	{
		if (GMOpendedFunctionVector.at(i)->get_positionIndex() > ((CFunctionOpenLevel * )functionOpenLevel)->get_positionIndex())
		{
			if (lastIndex == -5)
				lastIndex = i-1;

			
			if (l_al->getChildByName(GMOpendedFunctionVector.at(i)->get_functionId().c_str()))
			{
				if (i == 4)
				{
					FiniteTimeAction*  action = Sequence::create(
						MoveTo::create(0.15f,Vec2(CoordinateVector.at(5).x,CoordinateVector.at(5).y)),
						MoveTo::create(0.35f,Vec2(CoordinateVector.at(i+1).x,CoordinateVector.at(i+1).y)),
						NULL);
					(l_al->getChildByName(GMOpendedFunctionVector.at(i)->get_functionId().c_str()))->runAction(action);
				}
				else
				{
					FiniteTimeAction*  action = Sequence::create(
						MoveTo::create(0.5f,Vec2(CoordinateVector.at(i+1).x,CoordinateVector.at(i+1).y)),
						NULL);
					(l_al->getChildByName(GMOpendedFunctionVector.at(i)->get_functionId().c_str()))->runAction(action);
				}
			}
		}
	}

	if(lastIndex == -5)
	{
		if (l_al->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))
		{
			(l_al->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setVisible(true);
			(l_al->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setPosition(Vec2(CoordinateVector.at(GMOpendedFunctionVector.size()).x,CoordinateVector.at(GMOpendedFunctionVector.size()).y));
		}
	}
	else
	{
		if (l_al->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))
		{
			(l_al->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setVisible(true);
			(l_al->getChildByName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId().c_str()))->setPosition(Vec2(CoordinateVector.at(lastIndex + 1).x,CoordinateVector.at(lastIndex + 1).y));
		}
	}

	CFunctionOpenLevel * temp = new CFunctionOpenLevel();
	temp->set_functionId(((CFunctionOpenLevel * )functionOpenLevel)->get_functionId());
	temp->set_functionName(((CFunctionOpenLevel * )functionOpenLevel)->get_functionName());
	temp->set_functionType(((CFunctionOpenLevel * )functionOpenLevel)->get_functionType());
	temp->set_requiredLevel(((CFunctionOpenLevel * )functionOpenLevel)->get_requiredLevel());
	temp->set_positionIndex(((CFunctionOpenLevel * )functionOpenLevel)->get_positionIndex());
	GMOpendedFunctionVector.push_back(temp);
	sort(GMOpendedFunctionVector.begin(),GMOpendedFunctionVector.end(),GMSortByPositionIndex);
}

void GuideMap::OnLineGiftEvent(Ref *pSender, Widget::TouchEventType type)
{

}

void GuideMap::showRemindContent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		auto button_ = (Button *)pSender;
		auto remindui_ = RemindUi::create();
		remindui_->setIgnoreAnchorPointForPosition(false);
		remindui_->setAnchorPoint(Vec2(0.5f, 1));
		remindui_->setPosition(Vec2(button_->getPosition().x, winSize.height - button_->getContentSize().height));
		remindui_->setTag(kTagRemindUi);
		GameView::getInstance()->getMainUIScene()->addChild(remindui_);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}
void GuideMap::ArenaEvent(Ref *pSender, Widget::TouchEventType type)
{
	//add by yangjun 2014.10.13
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	if (GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI) == NULL)
	{
		Size winSize=Director::getInstance()->getVisibleSize();
		OffLineArenaUI * offLineArenaUI =OffLineArenaUI::create();
		offLineArenaUI->setIgnoreAnchorPointForPosition(false);
		offLineArenaUI->setAnchorPoint(Vec2(0.5f,0.5f));
		offLineArenaUI->setPosition(Vec2(winSize.width/2,winSize.height/2));
		offLineArenaUI->setTag(kTagOffLineArenaUI);
		GameView::getInstance()->getMainUIScene()->addChild(offLineArenaUI);

		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if(sc != NULL)
			sc->endCommand(pSender);
	}
}

void GuideMap::changeUIBySceneType( int mapType )
{
	switch(mapType)
	{
	case com::future::threekingdoms::server::transport::protocol::coliseum:           //竞技场                                                                     //竞技场
		{
			if (!isExistUIForArenaScene)
			{
				createUIForArenaScene();
			}

			m_nRemainTime = 600;

			u_layer_MainScene->setVisible(false);
			u_layer_FiveInstanceScene->setVisible(false);
			u_layer_ArenaScene->setVisible(true);
			u_layer_FamilyFightScene->setVisible(false);
			u_layer_SingCopyScene->setVisible(false);
		}
		break;
	case com::future::threekingdoms::server::transport::protocol::copy:        //副本
		{
			if (!isExistUIForFiveInstanceScene)
			{
				createUIForFiveInstanceScene();
			}

			u_layer_MainScene->setVisible(false);
			u_layer_FiveInstanceScene->setVisible(true);
			u_layer_ArenaScene->setVisible(false);
			u_layer_FamilyFightScene->setVisible(false);
			u_layer_SingCopyScene->setVisible(false);

			if (MyPlayerAIConfig::getAutomaticSkill() == 1)
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png");
			}
			else
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
			}

			//如果副本教学正在执行，则打断
			auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
			{
				if(sc->isScriptCMD("TutorialFiveInstance2"))
				{
					sc->endCommand(this->btn_instance);
				}
			}
		}
		break;
	case com::future::threekingdoms::server::transport::protocol::tower:        //singCopy
		{
			/*
			//sing copy countdown star
			SingCopyInstance::setStartTime(GameUtils::millisecondNow());
			SingCopyCountDownUI * singCopyCountDownUi = SingCopyCountDownUI::create();
			singCopyCountDownUi->setIgnoreAnchorPointForPosition(false);
			singCopyCountDownUi->setAnchorPoint(Vec2(0.5f,0.5f));
			singCopyCountDownUi->setPosition(Vec2(winSize.width/2,winSize.height/2));
			singCopyCountDownUi->setTag(kTagSingCopyCountDownUi);
			GameView::getInstance()->getMainUIScene()->addChild(singCopyCountDownUi);
			*/
			auto mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();	
			auto missionAndTeam = (MissionAndTeam *)mainscene->getChildByTag(kTagMissionAndTeam);
			if (missionAndTeam)
			{
				missionAndTeam->starRunUpdate();
			}
			
			if (!isExistUIForSingCopyScene)
			{
				createUIForSingCopyScene();
			}

			u_layer_MainScene->setVisible(false);
			u_layer_FiveInstanceScene->setVisible(false);
			u_layer_ArenaScene->setVisible(false);
			u_layer_FamilyFightScene->setVisible(false);
			u_layer_SingCopyScene->setVisible(true);

			if (MyPlayerAIConfig::getAutomaticSkill() == 1)
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png");
			}
			else
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
			}
		}break;
	case com::future::threekingdoms::server::transport::protocol::guildFight:        //家族战
		{
			if (!isExistUIForFamilyFightScene)
			{
				createUIForFamilyFightScene();
			}

			u_layer_MainScene->setVisible(false);
			u_layer_FiveInstanceScene->setVisible(false);
			u_layer_ArenaScene->setVisible(false);
			u_layer_FamilyFightScene->setVisible(true);
			u_layer_SingCopyScene->setVisible(false);

			if (MyPlayerAIConfig::getAutomaticSkill() == 1)
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png");
			}
			else
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
			}
		}
		break;
	default:
		{
			if (!isExistUIForMainScene)
			{
				createUIForMainScene();
			}

			u_layer_MainScene->setVisible(true);
			u_layer_FiveInstanceScene->setVisible(false);
			u_layer_ArenaScene->setVisible(false);
			u_layer_FamilyFightScene->setVisible(false);
			u_layer_SingCopyScene->setVisible(false);

			if (MyPlayerAIConfig::getAutomaticSkill() == 1)
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png");
			}
			else
			{
				setAllBtnHangUpTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png");
			}
		}
	}

	changeBtnVisibleByType(mapType);
}

void GuideMap::createUIForMainScene()
{
	isExistUIForMainScene = true;
	winSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto map_diBg= Button::create();
	map_diBg->loadTextures("gamescene_state/zhujiemian3/ditu/ditu_di.png","gamescene_state/zhujiemian3/ditu/ditu_di.png","");
	map_diBg->setAnchorPoint(Vec2(0.5f,0.5f));
	map_diBg->setScale9Enabled(true);
	map_diBg->setCapInsets(Rect(21,18,1,1));
	map_diBg->setContentSize(Size(153,44));
	map_diBg->setName("map_diBg");
	u_layer_MainScene->addChild(map_diBg);
	map_diBg->setTouchEnabled(true);
	map_diBg->addTouchEventListener(CC_CALLBACK_2(GuideMap::GoToMapScene, this));
	map_diBg->setPressedActionEnabled(true);
	map_diBg->setPosition(Vec2(winSize.width-map_diBg->getContentSize().width/2+origin.x,winSize.height-map_diBg->getContentSize().height/2+origin.y));

	//地图名称
	//Label * mapName = Label::create();
	//mapName->setFntFile("res_ui/font/ziti_3.fnt");
	auto mapName = Label::createWithTTF(GameView::getInstance()->getMapInfo()->mapname().c_str(), APP_FONT_NAME, 15);
	mapName->setColor(Color3B(255,216,61));  //(247,151,30) // (218,105,6)
	mapName->setAnchorPoint(Vec2(0.5f,0.5f));
	mapName->setPosition(Vec2(0,-3));
	mapName->setName("mapName");
	map_diBg->addChild(mapName);

	int countryId = GameView::getInstance()->getMapInfo()->country();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		auto countrySp_ = ImageView::create();
		countrySp_->loadTexture(iconPathName.c_str());
		countrySp_->setAnchorPoint(Vec2(1.0,0.5f));
		countrySp_->setPosition(Vec2(mapName->getPosition().x - mapName->getContentSize().width/2-1,mapName->getPosition().y));
		countrySp_->setScale(0.7f);
		countrySp_->setName("countrySp_");
		map_diBg->addChild(countrySp_);
	}
	else
	{
		mapName->setPosition(Vec2(0,-3));
	}

	std::string str_channelId = "(";
	char s_channelId[5];
	sprintf(s_channelId,"%d",GameView::getInstance()->getMapInfo()->channel()+1);
	str_channelId.append(s_channelId);
	str_channelId.append(StringDataManager::getString("GuideMap_Channel_Name"));
	str_channelId.append(")");
	auto l_channel = Label::createWithTTF(str_channelId.c_str(), APP_FONT_NAME, 15);
	l_channel->setColor(Color3B(255,216,61));  //(247,151,30) // (218,105,6)
	l_channel->setAnchorPoint(Vec2(0.f,0.5f));
	l_channel->setPosition(Vec2(mapName->getPosition().x + mapName->getContentSize().width/2-1,mapName->getPosition().y));
	l_channel->setName("l_channel");
	map_diBg->addChild(l_channel);

	//各个副本按钮等
	//first line
	//商城
	auto btn_shop = Button::create();
	btn_shop->loadTextures("gamescene_state/zhujiemian3/tubiao/rmbshop.png","gamescene_state/zhujiemian3/tubiao/rmbshop.png","");
	btn_shop->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_shop->setPressedActionEnabled(true);
	btn_shop->setTouchEnabled(true);
	btn_shop->setPosition(Vec2(winSize.width-160+origin.x,winSize.height-btn_shop->getContentSize().height/2+origin.y));
	btn_shop->addTouchEventListener(CC_CALLBACK_2(GuideMap::ShopEvent, this));
	btn_shop->setName("btn_shop");
	u_layer_MainScene->addChild(btn_shop);
	btn_shop->setVisible(false);
	//挂机
	bool aaa = MyPlayerAIConfig::isEnableRobot();
	int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
	auto btn_hangUp = Button::create();
	if (hookIndex == 1)
	{
		btn_hangUp->loadTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
	}else
	{
		btn_hangUp->loadTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png","gamescene_state/zhujiemian3/tubiao/on_hook.png","");
	}
	btn_hangUp->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_hangUp->setPressedActionEnabled(true);
	btn_hangUp->setTouchEnabled(true);
	btn_hangUp->setPosition(Vec2(btn_shop->getPosition().x-50+origin.x,winSize.height-btn_hangUp->getContentSize().height/2+origin.y));
	btn_hangUp->addTouchEventListener(CC_CALLBACK_2(GuideMap::HangUpEvent, this));
	btn_hangUp->setName("btn_hangUp_1");
	u_layer_MainScene->addChild(btn_hangUp);
	btn_hangUp->setVisible(false);

	//武将模式
	auto btn_generalMode = Button::create();
	btn_generalMode->loadTextures("gamescene_state/zhujiemian3/tubiao/driving.png","gamescene_state/zhujiemian3/tubiao/driving.png","");
	btn_generalMode->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_generalMode->setPressedActionEnabled(true);
	btn_generalMode->setTouchEnabled(true);
	btn_generalMode->setPosition(Vec2(btn_hangUp->getPosition().x-50+origin.x,winSize.height-btn_hangUp->getContentSize().height/2+origin.y));
	btn_generalMode->addTouchEventListener(CC_CALLBACK_2(GuideMap::GeneralModeEvent, this));
	btn_generalMode->setName("btn_generalMode");
	u_layer_MainScene->addChild(btn_generalMode);
	btn_generalMode->setVisible(false);

	setDefaultGeneralMode(UserDefault::getInstance()->getIntegerForKey("general_mode"));

	//remind
	buttonRemind = Button::create();
	buttonRemind->setAnchorPoint(Vec2(0.5f,0.5f));
	buttonRemind->setPosition(Vec2(btn_generalMode->getPosition().x-50+origin.x,winSize.height-btn_generalMode->getContentSize().height/2+origin.y));
	buttonRemind->loadTextures("gamescene_state/zhujiemian3/tubiao/upup.png","gamescene_state/zhujiemian3/tubiao/upup.png","");
	buttonRemind->setTouchEnabled(true);
	buttonRemind->setPressedActionEnabled(true);
	buttonRemind->addTouchEventListener(CC_CALLBACK_2(GuideMap::showRemindContent, this));
	buttonRemind->setName("btn_remind");
	u_layer_MainScene->addChild(buttonRemind);
	buttonRemind->setVisible(false);
	 
	//可缩回/弹出的层
	layer_action = Layer::create();
	u_layer_MainScene->addChild(layer_action);
	layer_action->setTag(kTagActionLayer);

	//副本
	btn_instance= Button::create();
	btn_instance->loadTextures("gamescene_state/zhujiemian3/tubiao/instance.png","gamescene_state/zhujiemian3/tubiao/instance.png","");
	btn_instance->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_instance->setPressedActionEnabled(true);
	btn_instance->setTouchEnabled(true);
	btn_instance->setPosition(Vec2(CoordinateVector.at(0).x,CoordinateVector.at(0).y));
	btn_instance->addTouchEventListener(CC_CALLBACK_2(GuideMap::InstanceEvent, this));
	btn_instance->setName("btn_instance");
	layer_action->addChild(btn_instance);
	btn_instance->setVisible(false);

	//竞技场
	btn_offLineArena= Button::create();
	btn_offLineArena->loadTextures("gamescene_state/zhujiemian3/tubiao/arena.png","gamescene_state/zhujiemian3/tubiao/arena.png","");
	btn_offLineArena->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_offLineArena->setPressedActionEnabled(true);
	btn_offLineArena->setTouchEnabled(true);
	btn_offLineArena->setPosition(Vec2(CoordinateVector.at(0).x,CoordinateVector.at(0).y));
	btn_offLineArena->addTouchEventListener(CC_CALLBACK_2(GuideMap::ArenaEvent, this));
	btn_offLineArena->setName("btn_offLineArena");
	layer_action->addChild(btn_offLineArena);
	btn_offLineArena->setVisible(false);

	btn_Reward = Button::create();
	btn_Reward->loadTextures("gamescene_state/zhujiemian3/tubiao/OnlineAwards.png","gamescene_state/zhujiemian3/tubiao/OnlineAwards.png","");
	btn_Reward->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_Reward->setPressedActionEnabled(true);
	btn_Reward->setTouchEnabled(true);
	btn_Reward->setPosition(Vec2(CoordinateVector.at(3).x,CoordinateVector.at(3).y));
	btn_Reward->addTouchEventListener(CC_CALLBACK_2(GuideMap::callBackGetReward, this));
	btn_Reward->setName("btn_onLineGift");
	btn_Reward->setVisible(false);
	layer_action->addChild(btn_Reward);

	btn_RewardParticle = CCTutorialParticle::create("tuowei0.plist",30,46);
	btn_RewardParticle->setPosition(Vec2(btn_Reward->getContentSize().width / 2, 0));
	btn_RewardParticle->setVisible(false);
	btn_Reward->addChild(btn_RewardParticle);


	/*
	//在线奖励
	if (!OnlineGiftData::instance()->isAllGiftHasGet())
	{
		OnlineGiftIconWidget *  btn_onLineGift = OnlineGiftIconWidget::create();
		btn_onLineGift->setAnchorPoint(Vec2(0.5f,0.5f));
		btn_onLineGift->setTouchEnabled(true);
		btn_onLineGift->setPosition(Vec2(CoordinateVector.at(3).x,CoordinateVector.at(3).y));
		btn_onLineGift->addTouchEventListener(CC_CALLBACK_2(GuideMap::OnLineGiftEvent));
		btn_onLineGift->setName("btn_onLineGift");
		btn_onLineGift->setTag(kTagOnLineGiftIcon);
		btn_onLineGift->setVisible(false);
		btn_onLineGift->initDataFromIntent();
		layer_action->addChild(btn_onLineGift);
	}
	*/
	// 活跃度icon
	ActiveIconWidget * btn_activeIcon = ActiveIconWidget::create();
	btn_activeIcon->setAnchorPoint(Vec2(0, 0));
	btn_activeIcon->setTouchEnabled(true);
	btn_activeIcon->setPosition(Vec2(CoordinateVector.at(1).x,CoordinateVector.at(1).y));
	btn_activeIcon->addTouchEventListener(CC_CALLBACK_2(GuideMap::ActiveDegreeEvent, this));
	btn_activeIcon->setName("btn_activeDegree");
	btn_activeIcon->setTag(kTagActiveIcon);
	btn_activeIcon->setVisible(false);
	layer_action->addChild(btn_activeIcon);

	//首冲
	ActiveRole * activeRole = GameView::getInstance()->myplayer->getActiveRole();
	if (activeRole->mutable_playerbaseinfo()->rechargeflag() == 0)
	{
		// vip开关控制
		bool vipEnabled = GameConfig::getBoolForKey("vip_enable");
		if (vipEnabled)
		{
			auto btn_firstBuyVip = Button::create();
			btn_firstBuyVip->loadTextures("gamescene_state/zhujiemian3/tubiao/firstbuy.png", "gamescene_state/zhujiemian3/tubiao/firstbuy.png", "");
			btn_firstBuyVip->setAnchorPoint(Vec2(0.5f, 0.5f));
			btn_firstBuyVip->setTouchEnabled(true);
			btn_firstBuyVip->setPressedActionEnabled(true);
			btn_firstBuyVip->setPosition(Vec2(CoordinateVector.at(1).x,CoordinateVector.at(2).y));
			btn_firstBuyVip->addTouchEventListener(CC_CALLBACK_2(GuideMap::FirstBuyVipEvent, this));
			btn_firstBuyVip->setName("btn_firstBuyVip");
			btn_firstBuyVip->setVisible(false);
			layer_action->addChild(btn_firstBuyVip);
		}
	}
	//礼包
// 		auto  gift = Button::create();
// 		gift->loadTextures("gamescene_state/tubiao/libao.png","gamescene_state/tubiao/libao.png","");
// 		gift->setAnchorPoint(Vec2(0.5f,0.5f));
// 		gift->setTouchEnabled(true);
// 		gift->setPressedActionEnabled(true);
// 		gift->setPosition(Vec2(CoordinateVector.at(2).x,CoordinateVector.at(2).y));
// 		gift->addTouchEventListener(CC_CALLBACK_2(GuideMap::GiftEvent));
// 		gift->setName("btn_gift");
// 		layer_action->addChild(gift);
// 		gift->setVisible(false);
	//活动
// 		auto activity = Button::create();
// 		activity->loadTextures("gamescene_state/tubiao/huodong.png","gamescene_state/tubiao/huodong.png","");
// 		activity->setAnchorPoint(Vec2(0.5f,0.5f));
// 		activity->setTouchEnabled(true);
// 		activity->setPressedActionEnabled(true);
// 		activity->setPosition(Vec2(CoordinateVector.at(3).x,CoordinateVector.at(3).y));
// 		activity->addTouchEventListener(CC_CALLBACK_2(GuideMap::ActiveDegreeEvent));
// 		activity->setName("btn_activity");
// 		layer_action->addChild(activity);
// 		activity->setVisible(false);
	//third line
	//kai fu huo dong 
// 		auto   newService = Button::create();
// 		newService->loadTextures("gamescene_state/tubiao/kaifuhuodong.png","gamescene_state/tubiao/kaifuhuodong.png","");
// 		newService->setAnchorPoint(Vec2(0.5f,0.5f));
// 		newService->setTouchEnabled(true);
// 		newService->setPressedActionEnabled(true);
// 		newService->setPosition(Vec2(CoordinateVector.at(4).x,CoordinateVector.at(4).y));
// 		newService->addTouchEventListener(CC_CALLBACK_2(GuideMap::NewServiceEvent));
// 		newService->setName("btn_newService");
// 		layer_action->addChild(newService);
// 		newService->setVisible(false);
	//每日领取
// 		auto  everyDayGift = Button::create();
// 		everyDayGift->loadTextures("gamescene_state/tubiao/meirilingqu.png","gamescene_state/tubiao/meirilingqu.png","");
// 		everyDayGift->setAnchorPoint(Vec2(0.5f,0.5f));
// 		everyDayGift->setTouchEnabled(true);
// 		everyDayGift->setPressedActionEnabled(true);
// 		everyDayGift->setPosition(Vec2(CoordinateVector.at(5).x,CoordinateVector.at(5).y));
// 		everyDayGift->addTouchEventListener(CC_CALLBACK_2(GuideMap::EveryDayGiftEvent));
// 		everyDayGift->setName("btn_everyDayGift");
// 		layer_action->addChild(everyDayGift);
// 		everyDayGift->setVisible(false);

	int roleLevel = GameView::getInstance()->myplayer->getActiveRole()->level();
	for (int i = 0;i<FunctionOpenLevelConfigData::s_functionOpenLevelMsg.size();i++)
	{
		if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() == 2)//地图区域（1号区域）
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel()<= roleLevel)     //可以显示
			{
				CFunctionOpenLevel * tempItem = new CFunctionOpenLevel();
				tempItem->set_functionId(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId());
				tempItem->set_functionName(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionName());
				tempItem->set_functionType(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType());
				tempItem->set_requiredLevel(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel());
				tempItem->set_positionIndex(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_positionIndex());
				GMOpendedFunctionVector_1.push_back(tempItem);
			}
		}
		else if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType() == 3)       //地图区域（2号区域）
		{
			if (FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel()<= roleLevel)     //可以显示
			{
				if (layer_action->getChildByName(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId().c_str()))
				{
					CFunctionOpenLevel * tempItem = new CFunctionOpenLevel();
					tempItem->set_functionId(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionId());
					tempItem->set_functionName(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionName());
					tempItem->set_functionType(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_functionType());
					tempItem->set_requiredLevel(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_requiredLevel());
					tempItem->set_positionIndex(FunctionOpenLevelConfigData::s_functionOpenLevelMsg.at(i)->get_positionIndex());
					GMOpendedFunctionVector.push_back(tempItem);
				}
			}
		}
   	}
	sort(GMOpendedFunctionVector.begin(),GMOpendedFunctionVector.end(),	GMSortByPositionIndex);
	int m_index = 0;
	for (int i = 0;i<GMOpendedFunctionVector.size();i++)
	{
		if (layer_action->getChildByName(GMOpendedFunctionVector.at(i)->get_functionId().c_str()))
		{
			(layer_action->getChildByName(GMOpendedFunctionVector.at(i)->get_functionId().c_str()))->setVisible(true);
			(layer_action->getChildByName(GMOpendedFunctionVector.at(i)->get_functionId().c_str()))->setPosition(Vec2(CoordinateVector.at(m_index).x,CoordinateVector.at(m_index).y));
			m_index++;
		}
	}


	for (int i = 0;i<GMOpendedFunctionVector_1.size();i++)
	{
		if (u_layer_MainScene->getChildByName(GMOpendedFunctionVector_1.at(i)->get_functionId().c_str()))
		{
			(u_layer_MainScene->getChildByName(GMOpendedFunctionVector_1.at(i)->get_functionId().c_str()))->setVisible(true);
		}
	}
}

void GuideMap::createUIForFiveInstanceScene()
{
	isExistUIForFiveInstanceScene = true;
	winSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//各个副本按钮等
	//first line
	//退出
	auto btn_exit = Button::create();
	btn_exit->loadTextures("gamescene_state/zhujiemian3/tubiao/quit.png","gamescene_state/zhujiemian3/tubiao/quit.png","");
	btn_exit->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_exit->setPressedActionEnabled(true);
	btn_exit->setTouchEnabled(true);
	btn_exit->setPosition(Vec2(winSize.width-30+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_exit->addTouchEventListener(CC_CALLBACK_2(GuideMap::ExitEvent, this));
	btn_exit->setName("btn_exit");
	u_layer_FiveInstanceScene->addChild(btn_exit);
	btn_exit->setVisible(true);
	//商城
	auto btn_shop = Button::create();
	btn_shop->loadTextures("gamescene_state/zhujiemian3/tubiao/rmbshop.png","gamescene_state/zhujiemian3/tubiao/rmbshop.png","");
	btn_shop->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_shop->setPressedActionEnabled(true);
	btn_shop->setTouchEnabled(true);
	btn_shop->setPosition(Vec2(btn_exit->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_shop->addTouchEventListener(CC_CALLBACK_2(GuideMap::ShopEvent, this));
	btn_shop->setName("btn_shop");
	u_layer_FiveInstanceScene->addChild(btn_shop);
	btn_shop->setVisible(true);
	//挂机
	bool aaa = MyPlayerAIConfig::isEnableRobot();
	int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
	Button* btn_hangUp = Button::create();
	if (hookIndex == 1)
	{
		btn_hangUp->loadTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
	}else
	{
		btn_hangUp->loadTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png","gamescene_state/zhujiemian3/tubiao/on_hook.png","");
	}
	btn_hangUp->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_hangUp->setPressedActionEnabled(true);
	btn_hangUp->setTouchEnabled(true);
	btn_hangUp->setPosition(Vec2(btn_shop->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_hangUp->addTouchEventListener(CC_CALLBACK_2(GuideMap::HangUpEvent, this));
	btn_hangUp->setName("btn_hangUp_1");
	u_layer_FiveInstanceScene->addChild(btn_hangUp);
	btn_hangUp->setVisible(true);

	//武将模式
	auto btn_generalMode = Button::create();
	btn_generalMode->loadTextures("gamescene_state/zhujiemian3/tubiao/driving.png","gamescene_state/zhujiemian3/tubiao/driving.png","");
	btn_generalMode->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_generalMode->setPressedActionEnabled(true);
	btn_generalMode->setTouchEnabled(true);
	btn_generalMode->setPosition(Vec2(btn_hangUp->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_generalMode->addTouchEventListener(CC_CALLBACK_2(GuideMap::GeneralModeEvent, this));
	btn_generalMode->setName("btn_generalMode");
	u_layer_FiveInstanceScene->addChild(btn_generalMode);

	setDefaultGeneralMode(UserDefault::getInstance()->getIntegerForKey("general_mode"));

	//remind
// 	buttonRemind = Button::create();
// 	buttonRemind->setAnchorPoint(Vec2(0.5f,0.5f));
// 	buttonRemind->setPosition(Vec2(btn_generalMode->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
// 	buttonRemind->loadTextures("gamescene_state/zhujiemian3/tubiao/upup.png","gamescene_state/zhujiemian3/tubiao/upup.png","");
// 	buttonRemind->setTouchEnabled(true);
// 	buttonRemind->setPressedActionEnabled(true);
// 	buttonRemind->addTouchEventListener(CC_CALLBACK_2(GuideMap::showRemindContent));
// 	buttonRemind->setName("btn_remind");
// 	u_layer_FiveInstanceScene->addChild(buttonRemind);
// 	buttonRemind->setVisible(true);

	//可缩回/弹出的层
	layer_action = Layer::create();
	u_layer_FiveInstanceScene->addChild(layer_action);
	layer_action->setTag(kTagActionLayer);

	//召唤
	auto btn_callFriend= Button::create();
	btn_callFriend->loadTextures("gamescene_state/zhujiemian3/tubiao/callUP.png","gamescene_state/zhujiemian3/tubiao/callUP.png","");
	btn_callFriend->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_callFriend->setPressedActionEnabled(true);
	btn_callFriend->setTouchEnabled(true);
	btn_callFriend->setPosition(Vec2(winSize.width-30+origin.x,winSize.height-btn_generalMode->getContentSize().height-30+origin.x));
	btn_callFriend->addTouchEventListener(CC_CALLBACK_2(GuideMap::CallFriendsEvent, this));
	btn_callFriend->setName("btn_callFriend");
	layer_action->addChild(btn_callFriend);
	btn_callFriend->setVisible(true);
}

void GuideMap::createUIForSingCopyScene()
{
	isExistUIForSingCopyScene = true;
	winSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto btn_exit = Button::create();
	btn_exit->loadTextures("gamescene_state/zhujiemian3/tubiao/quit.png","gamescene_state/zhujiemian3/tubiao/quit.png","");
	btn_exit->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_exit->setPressedActionEnabled(true);
	btn_exit->setTouchEnabled(true);
	btn_exit->setPosition(Vec2(winSize.width-30+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_exit->addTouchEventListener(CC_CALLBACK_2(GuideMap::ExitSingCopy, this));
	btn_exit->setName("btn_exit");
	u_layer_SingCopyScene->addChild(btn_exit);
	btn_exit->setVisible(true);

	auto btn_shop = Button::create();
	btn_shop->loadTextures("gamescene_state/zhujiemian3/tubiao/rmbshop.png","gamescene_state/zhujiemian3/tubiao/rmbshop.png","");
	btn_shop->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_shop->setPressedActionEnabled(true);
	btn_shop->setTouchEnabled(true);
	btn_shop->setPosition(Vec2(btn_exit->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_shop->addTouchEventListener(CC_CALLBACK_2(GuideMap::ShopEvent, this));
	btn_shop->setName("btn_shop");
	u_layer_SingCopyScene->addChild(btn_shop);
	btn_shop->setVisible(true);

	int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
	Button* btn_hangUp = Button::create();
	if (hookIndex == 1)
	{
		btn_hangUp->loadTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
	}else
	{
		btn_hangUp->loadTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png","gamescene_state/zhujiemian3/tubiao/on_hook.png","");
	}
	btn_hangUp->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_hangUp->setPressedActionEnabled(true);
	btn_hangUp->setTouchEnabled(true);
	btn_hangUp->setPosition(Vec2(btn_shop->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_hangUp->addTouchEventListener(CC_CALLBACK_2(GuideMap::HangUpEvent, this));
	btn_hangUp->setName("btn_hangUp_1");
	u_layer_SingCopyScene->addChild(btn_hangUp);
	btn_hangUp->setVisible(true);
	//general model
	auto btn_generalMode = Button::create();
	btn_generalMode->loadTextures("gamescene_state/zhujiemian3/tubiao/driving.png","gamescene_state/zhujiemian3/tubiao/driving.png","");
	btn_generalMode->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_generalMode->setPressedActionEnabled(true);
	btn_generalMode->setTouchEnabled(true);
	btn_generalMode->setPosition(Vec2(btn_hangUp->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
	btn_generalMode->addTouchEventListener(CC_CALLBACK_2(GuideMap::GeneralModeEvent, this));
	btn_generalMode->setName("btn_generalMode");
	u_layer_SingCopyScene->addChild(btn_generalMode);

	setDefaultGeneralMode(UserDefault::getInstance()->getIntegerForKey("general_mode"));

	//remind
// 	buttonRemind = Button::create();
// 	buttonRemind->setAnchorPoint(Vec2(0.5f,0.5f));
// 	buttonRemind->setPosition(Vec2(btn_generalMode->getPosition().x-50+origin.x,winSize.height-btn_exit->getContentSize().height/2+origin.y));
// 	buttonRemind->loadTextures("gamescene_state/zhujiemian3/tubiao/upup.png","gamescene_state/zhujiemian3/tubiao/upup.png","");
// 	buttonRemind->setTouchEnabled(true);
// 	buttonRemind->setPressedActionEnabled(true);
// 	buttonRemind->addTouchEventListener(CC_CALLBACK_2(GuideMap::showRemindContent));
// 	buttonRemind->setName("btn_remind");
// 	u_layer_SingCopyScene->addChild(buttonRemind);
// 	buttonRemind->setVisible(true);
}


void GuideMap::createUIForArenaScene()
{
	isExistUIForArenaScene = true;
	winSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	//first line
	//退出
	auto btn_quit = Button::create();
	btn_quit->loadTextures("gamescene_state/zhujiemian3/tubiao/quit.png","gamescene_state/zhujiemian3/tubiao/quit.png","");
	btn_quit->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_quit->setPressedActionEnabled(true);
	btn_quit->setTouchEnabled(true);
	btn_quit->setPosition(Vec2(winSize.width-30+origin.x,winSize.height-btn_quit->getContentSize().height/2+origin.y));
	btn_quit->addTouchEventListener(CC_CALLBACK_2(GuideMap::QuitArenaEvent, this));
	btn_quit->setName("btn_quit");
	u_layer_ArenaScene->addChild(btn_quit);
	//挂机
	int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
	Button* btn_hangUp = Button::create();
	if (hookIndex == 1)
	{
		btn_hangUp->loadTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
	}
	else
	{
		btn_hangUp->loadTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png","gamescene_state/zhujiemian3/tubiao/on_hook.png","");
	}
	btn_hangUp->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_hangUp->setPressedActionEnabled(true);
	btn_hangUp->setTouchEnabled(true);
	btn_hangUp->setPosition(Vec2(btn_quit->getPosition().x-60+origin.x,winSize.height-btn_quit->getContentSize().height/2+origin.y));
	btn_hangUp->addTouchEventListener(CC_CALLBACK_2(GuideMap::HangUpEvent, this));
	btn_hangUp->setName("btn_hangUp_1");
	u_layer_ArenaScene->addChild(btn_hangUp);
	btn_hangUp->setVisible(true);
	//武将模式
	auto btn_generalMode = Button::create();
	btn_generalMode->loadTextures("gamescene_state/zhujiemian3/tubiao/driving.png","gamescene_state/zhujiemian3/tubiao/driving.png","");
	btn_generalMode->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_generalMode->setPressedActionEnabled(true);
	btn_generalMode->setTouchEnabled(true);
	btn_generalMode->setPosition(Vec2(btn_hangUp->getPosition().x-60+origin.x,winSize.height-btn_hangUp->getContentSize().height/2+origin.y));
	btn_generalMode->addTouchEventListener(CC_CALLBACK_2(GuideMap::GeneralModeEvent, this));
	btn_generalMode->setName("btn_generalMode");
	u_layer_ArenaScene->addChild(btn_generalMode);

	setDefaultGeneralMode(UserDefault::getInstance()->getIntegerForKey("general_mode"));
}

void GuideMap::createUIForFamilyFightScene()
{
	isExistUIForFamilyFightScene = true;
	winSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto map_diBg= Button::create();
	map_diBg->loadTextures("gamescene_state/zhujiemian3/ditu/ditu_di.png","gamescene_state/zhujiemian3/ditu/ditu_di.png","");
	map_diBg->setAnchorPoint(Vec2(0.5f,0.5f));
	map_diBg->setScale9Enabled(true);
	map_diBg->setCapInsets(Rect(21,18,1,1));
	map_diBg->setContentSize(Size(153,44));
	map_diBg->setName("map_diBg");
	u_layer_FamilyFightScene->addChild(map_diBg);
	map_diBg->setTouchEnabled(true);
	map_diBg->addTouchEventListener(CC_CALLBACK_2(GuideMap::GoToMapScene, this));
	map_diBg->setPressedActionEnabled(true);
	map_diBg->setPosition(Vec2(winSize.width-map_diBg->getContentSize().width/2+origin.x,winSize.height-map_diBg->getContentSize().height/2+origin.y));

	//地图名称
	//Label * mapName = Label::create();
	//mapName->setFntFile("res_ui/font/ziti_3.fnt");
	auto mapName = Label::createWithTTF(GameView::getInstance()->getMapInfo()->mapname().c_str(), APP_FONT_NAME, 15);
	mapName->setColor(Color3B(255,216,61));  //(247,151,30) // (218,105,6)
	mapName->setAnchorPoint(Vec2(0.5f,0.5f));
	mapName->setPosition(Vec2(0, map_diBg->getBoundingBox().size.height/2));
	mapName->setName("mapName");
	map_diBg->addChild(mapName);

	int countryId = GameView::getInstance()->getMapInfo()->country();
	if (countryId > 0 && countryId <6)
	{
		std::string countryIdName = "country_";
		char id[2];
		sprintf(id, "%d", countryId);
		countryIdName.append(id);
		std::string iconPathName = "res_ui/country_icon/";
		iconPathName.append(countryIdName);
		iconPathName.append(".png");
		auto countrySp_ = ImageView::create();
		countrySp_->loadTexture(iconPathName.c_str());
		countrySp_->setAnchorPoint(Vec2(1.0,0.5f));
		countrySp_->setPosition(Vec2(mapName->getPosition().x - mapName->getContentSize().width/2-1,mapName->getPosition().y));
		countrySp_->setScale(0.7f);
		countrySp_->setName("countrySp_");
		map_diBg->addChild(countrySp_);
	}
	else
	{
		mapName->setPosition(Vec2(-10,-3));
	}

	std::string str_channelId = "(";
	char s_channelId[5];
	sprintf(s_channelId,"%d",GameView::getInstance()->getMapInfo()->channel()+1);
	str_channelId.append(s_channelId);
	str_channelId.append(StringDataManager::getString("GuideMap_Channel_Name"));
	str_channelId.append(")");
	auto l_channel = Label::createWithTTF(str_channelId.c_str(), APP_FONT_NAME, 15);
	l_channel->setColor(Color3B(255,216,61));  //(247,151,30) // (218,105,6)
	l_channel->setAnchorPoint(Vec2(0.f,0.5f));
	l_channel->setPosition(Vec2(mapName->getPosition().x + mapName->getContentSize().width/2-1,mapName->getPosition().y));
	l_channel->setName("l_channel");
	map_diBg->addChild(l_channel);

	//各个副本按钮等
	//first line
	//商城
	auto btn_shop = Button::create();
	btn_shop->loadTextures("gamescene_state/zhujiemian3/tubiao/rmbshop.png","gamescene_state/zhujiemian3/tubiao/rmbshop.png","");
	btn_shop->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_shop->setPressedActionEnabled(true);
	btn_shop->setTouchEnabled(true);
	btn_shop->setPosition(Vec2(winSize.width-160+origin.x,winSize.height-btn_shop->getContentSize().height/2+origin.y));
	btn_shop->addTouchEventListener(CC_CALLBACK_2(GuideMap::ShopEvent, this));
	btn_shop->setName("btn_shop");
	u_layer_FamilyFightScene->addChild(btn_shop);
	//挂机
	bool aaa = MyPlayerAIConfig::isEnableRobot();
	int hookIndex = MyPlayerAIConfig::getAutomaticSkill();
	Button* btn_hangUp = Button::create();
	if (hookIndex == 1)
	{
		btn_hangUp->loadTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
	}else
	{
		btn_hangUp->loadTextures("gamescene_state/zhujiemian3/tubiao/on_hook.png","gamescene_state/zhujiemian3/tubiao/on_hook.png","");
	}
	btn_hangUp->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_hangUp->setPressedActionEnabled(true);
	btn_hangUp->setTouchEnabled(true);
	btn_hangUp->setPosition(Vec2(btn_shop->getPosition().x-50+origin.x,winSize.height-btn_hangUp->getContentSize().height/2+origin.y));
	btn_hangUp->addTouchEventListener(CC_CALLBACK_2(GuideMap::HangUpEvent, this));
	btn_hangUp->setName("btn_hangUp_1");
	u_layer_FamilyFightScene->addChild(btn_hangUp);

	//武将模式
	auto btn_generalMode = Button::create();
	btn_generalMode->loadTextures("gamescene_state/zhujiemian3/tubiao/driving.png","gamescene_state/zhujiemian3/tubiao/driving.png","");
	btn_generalMode->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_generalMode->setPressedActionEnabled(true);
	btn_generalMode->setTouchEnabled(true);
	btn_generalMode->setPosition(Vec2(btn_hangUp->getPosition().x-50+origin.x,winSize.height-btn_hangUp->getContentSize().height/2+origin.y));
	btn_generalMode->addTouchEventListener(CC_CALLBACK_2(GuideMap::GeneralModeEvent, this));
	btn_generalMode->setName("btn_generalMode");
	u_layer_FamilyFightScene->addChild(btn_generalMode);

	setDefaultGeneralMode(UserDefault::getInstance()->getIntegerForKey("general_mode"));

	//退出
	auto btn_quit = Button::create();
	btn_quit->loadTextures("gamescene_state/zhujiemian3/tubiao/quit.png","gamescene_state/zhujiemian3/tubiao/quit.png","");
	btn_quit->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_quit->setPressedActionEnabled(true);
	btn_quit->setTouchEnabled(true);
	btn_quit->setPosition(Vec2(winSize.width-30+origin.x,winSize.height- map_diBg->getContentSize().height -btn_quit->getContentSize().height/2-2+origin.y));
	btn_quit->addTouchEventListener(CC_CALLBACK_2(GuideMap::QuitFamilyFightEvent, this));
	btn_quit->setName("btn_quit");
	u_layer_FamilyFightScene->addChild(btn_quit);
	//战况
	auto btn_battleSituation = Button::create();
	btn_battleSituation->loadTextures("gamescene_state/zhujiemian3/tubiao/zhankuang.png","gamescene_state/zhujiemian3/tubiao/zhankuang.png","");
	btn_battleSituation->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_battleSituation->setPressedActionEnabled(true);
	btn_battleSituation->setTouchEnabled(true);
	btn_battleSituation->setPosition(Vec2(btn_quit->getPosition().x-50+origin.x,btn_quit->getPosition().y));
	btn_battleSituation->addTouchEventListener(CC_CALLBACK_2(GuideMap::BattleSituationEvent, this));
	btn_battleSituation->setName("btn_battleSituation");
	u_layer_FamilyFightScene->addChild(btn_battleSituation);
}

Button * GuideMap::getBtnRemind()
{
	if (u_layer_MainScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_MainScene->getChildByName("btn_remind");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_FiveInstanceScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_FiveInstanceScene->getChildByName("btn_remind");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_ArenaScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_ArenaScene->getChildByName("btn_remind");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_SingCopyScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_SingCopyScene->getChildByName("btn_remind");
		if (btn_temp)
			return btn_temp;
	}

	return NULL;
}

void GuideMap::QuitArenaEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{

		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		GameView::getInstance()->showPopupWindow(StringDataManager::getString("OffLineArenaUI_quiteWillFail"), 2, this, CC_CALLFUNCO_SELECTOR(GuideMap::SureToQuitArena), NULL, PopupWindow::KTypeRemoveByTime, 10);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GuideMap::SureToQuitArena(Ref *pSender)
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(3021);
}


void GuideMap::QuitFamilyFightEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		GameView::getInstance()->showPopupWindow(StringDataManager::getString("FamilyFightUI_areYouSureToQuit"), 2, this, CC_CALLFUNCO_SELECTOR(GuideMap::SureToQuitFamilyFight), NULL, PopupWindow::KTypeRemoveByTime, 10);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void GuideMap::SureToQuitFamilyFight( Ref *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1541,(void *)FamilyFightData::getCurrentBattleId());
}


void GuideMap::GeneralModeEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		// 	if (m_generalMode == 1)
		// 	{
		// 		GameMessageProcessor::sharedMsgProcessor()->sendReq(1206,(void *)2);
		// 	}
		// 	else
		// 	{
		// 		GameMessageProcessor::sharedMsgProcessor()->sendReq(1206,(void *)1);
		// 	}
		// 
		auto sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
		if (sc != NULL)
			sc->endCommand(pSender);

		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		if (this->getChildByTag(KTagGeneralsModeUI) == NULL)
		{
			int mode = UserDefault::getInstance()->getIntegerForKey("general_mode");
			int autofight = UserDefault::getInstance()->getIntegerForKey("general_autoFight");
			auto temp = GeneralsModeUI::create(mode, autofight);
			temp->setIgnoreAnchorPointForPosition(false);
			temp->setAnchorPoint(Vec2(.5f, 1.0f));
			temp->setPosition(Vec2(this->getBtnGeneralMode()->getPosition().x, this->getBtnGeneralMode()->getPosition().y - this->getBtnGeneralMode()->getContentSize().height / 2));
			temp->setTag(KTagGeneralsModeUI);
			this->addChild(temp);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}
void GuideMap::applyGeneralMode( int idx,int autoFight )
{
	m_generalMode = idx;
	m_autoFight = autoFight;

// 	std::string str_texture_path = "gamescene_state/zhujiemian3/tubiao/";
// 	switch(idx)
// 	{
// 	case 1:   //跟随
// 		{
// 			str_texture_path.append("assistance.png");
// 			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_mode_1"));
// 		}
// 		break;
// 	case 2:   //攻击
// 		{
// 			str_texture_path.append("driving.png");
// 			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_mode_2"));
// 		}
// 		break;
// 	case 3:  //休息
// 		{
// 			str_texture_path.append("assistance.png");
// 			GameView::getInstance()->showAlertDialog(StringDataManager::getString("generals_mode_1"));
// 		}
// 		break;
// 	default:
// 		str_texture_path.append("assistance.png");
// 	}
// 
// 	setAllBtnGeneralModeTextures(str_texture_path.c_str());

	auto temp = (GeneralsModeUI*)this->getChildByTag(KTagGeneralsModeUI);
	if (temp)
	{
		temp->applyGeneralMode(m_generalMode,m_autoFight);
	}
}


void GuideMap::setDefaultGeneralMode( int idx )
{
	m_generalMode = idx;

// 	std::string str_texture_path = "gamescene_state/zhujiemian3/tubiao/";
// 	switch(idx)
// 	{
// 	case 1:   //跟随
// 		{
// 			str_texture_path.append("assistance.png");
// 		}
// 		break;
// 	case 2:   //攻击
// 		{
// 			str_texture_path.append("driving.png");
// 		}
// 		break;
// 	case 3:  //休息
// 		{
// 			str_texture_path.append("assistance.png");
// 		}
// 		break;
// 	default:
// 		str_texture_path.append("assistance.png");
// 	}

//	setAllBtnGeneralModeTextures(str_texture_path.c_str());
}

std::string GuideMap::timeFormatToString( int t )
{
	int int_h = t/60/60;
	int int_m = (t%3600)/60;
	int int_s = (t%3600)%60;

	std::string timeString = "";
	char str_h[10];
	sprintf(str_h,"%d",int_h);
	if (int_h <= 0)
	{

	}
	else if (int_h > 0 && int_h < 10)
	{
		timeString.append("0");
		timeString.append(str_h);
		timeString.append(":");
	}
	else
	{
		timeString.append(str_h);
		timeString.append(":");
	}

	char str_m[10];
	sprintf(str_m,"%d",int_m);
	if (int_m >= 0 && int_m < 10)
	{
		timeString.append("0");
		timeString.append(str_m);
	}
	else
	{
		timeString.append(str_m);
	}
	timeString.append(":");

	char str_s[10];
	sprintf(str_s,"%d",int_s);
	if (int_s >= 0 && int_s < 10)
	{
		timeString.append("0");
		timeString.append(str_s);
	}
	else
	{
		timeString.append(str_s);
	}

	return timeString;
}


void GuideMap::FirstBuyVipEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		// 初始化 UI WINDOW数据
		Size winSize = Director::getInstance()->getVisibleSize();

		FirstBuyVipUI* firstBuyVipUI = (FirstBuyVipUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagFirstBuyVipUI);
		if (firstBuyVipUI == NULL)
		{
			FirstBuyVipUI * temp = FirstBuyVipUI::create();
			temp->setIgnoreAnchorPointForPosition(false);
			temp->setAnchorPoint(Vec2(0.5f, 0.5f));
			temp->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			temp->setTag(kTagFirstBuyVipUI);
			GameView::getInstance()->getMainUIScene()->addChild(temp);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

int GuideMap::getGeneralMode()
{
	return m_generalMode;
}

int GuideMap::getGeneralAutoFight()
{
	return m_autoFight;
}

Layer * GuideMap::getCurActiveLayer()
{
	if (u_layer_MainScene->isVisible())
	{
		return u_layer_MainScene;
	}

	if (u_layer_FiveInstanceScene->isVisible())
	{
		return u_layer_FiveInstanceScene;
	}

	if (u_layer_ArenaScene->isVisible())
	{
		return u_layer_ArenaScene;
	}

	if (u_layer_FamilyFightScene->isVisible())
	{
		return u_layer_FamilyFightScene;
	}

	if (u_layer_SingCopyScene->isVisible())
	{
		return u_layer_SingCopyScene;
	}
	return NULL;
}

Layer * GuideMap::getActionLayer()
{
	if (u_layer_MainScene->isVisible())
	{
		auto tempLayer = (Layer *)u_layer_MainScene->getChildByTag(kTagActionLayer);
		if (tempLayer)
			return tempLayer;
	}
	
	if (u_layer_FiveInstanceScene->isVisible())
	{
		auto tempLayer = (Layer *)u_layer_FiveInstanceScene->getChildByTag(kTagActionLayer);
		if (tempLayer)
			return tempLayer;
	}


	return NULL;
}

Button * GuideMap::getBtnHangUp()
{
	if (u_layer_MainScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_MainScene->getChildByName("btn_hangUp_1");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_FiveInstanceScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_FiveInstanceScene->getChildByName("btn_hangUp_1");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_ArenaScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_ArenaScene->getChildByName("btn_hangUp_1");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_FamilyFightScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_FamilyFightScene->getChildByName("btn_hangUp_1");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_SingCopyScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_SingCopyScene->getChildByName("btn_hangUp_1");
		if (btn_temp)
			return btn_temp;
	}

	return NULL;
}

Button * GuideMap::getBtnGeneralMode()
{
	if (u_layer_MainScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_MainScene->getChildByName("btn_generalMode");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_FiveInstanceScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_FiveInstanceScene->getChildByName("btn_generalMode");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_ArenaScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_ArenaScene->getChildByName("btn_generalMode");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_FamilyFightScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_FamilyFightScene->getChildByName("btn_generalMode");
		if (btn_temp)
			return btn_temp;
	}

	if (u_layer_SingCopyScene->isVisible())
	{
		auto btn_temp = (Button *)u_layer_SingCopyScene->getChildByName("btn_generalMode");
		if (btn_temp)
			return btn_temp;
	}
	return NULL;
}

void GuideMap::setAllBtnHangUpTextures(const char * str )
{
	auto btn_temp = (Button *)u_layer_MainScene->getChildByName("btn_hangUp_1");
	if (btn_temp)
		btn_temp->loadTextures(str,str,"");

	btn_temp = (Button *)u_layer_FiveInstanceScene->getChildByName("btn_hangUp_1");
	if (btn_temp)
		btn_temp->loadTextures(str,str,"");

	btn_temp = (Button *)u_layer_ArenaScene->getChildByName("btn_hangUp_1");
	if (btn_temp)
		btn_temp->loadTextures(str,str,"");

	btn_temp = (Button *)u_layer_FamilyFightScene->getChildByName("btn_hangUp_1");
	if (btn_temp)
		btn_temp->loadTextures(str,str,"");

	btn_temp = (Button *)u_layer_SingCopyScene->getChildByName("btn_hangUp_1");
	if (btn_temp)
		btn_temp->loadTextures(str,str,"");

	// 判断是否 去掉 自动战斗 的anm
	


	MyPlayer* pMyPlayer = GameView::getInstance()->myplayer;

}

void GuideMap::setAllBtnGeneralModeTextures(const char * str )
{
	auto btn_temp = (Button *)u_layer_MainScene->getChildByName("btn_generalMode");
	if (btn_temp)
		btn_temp->loadTextures(str,str,"");

	btn_temp = (Button *)u_layer_FiveInstanceScene->getChildByName("btn_generalMode");
	if (btn_temp)
		btn_temp->loadTextures(str,str,"");

	btn_temp = (Button *)u_layer_ArenaScene->getChildByName("btn_generalMode");
	if (btn_temp)
		btn_temp->loadTextures(str,str,"");

	btn_temp = (Button *)u_layer_FamilyFightScene->getChildByName("btn_generalMode");
	if (btn_temp)
		btn_temp->loadTextures(str,str,"");

	btn_temp = (Button *)u_layer_SingCopyScene->getChildByName("btn_generalMode");
	if (btn_temp)
		btn_temp->loadTextures(str,str,"");
}

void GuideMap::RefreshMapName()
{
	if (!u_layer_MainScene->isVisible())
		return;

	if (u_layer_MainScene->getChildByName("map_diBg"))
	{
		auto mapName = (Label*)u_layer_MainScene->getChildByName("map_diBg")->getChildByName("mapName");
		if (!mapName)
			return;

		mapName->setString(GameView::getInstance()->getMapInfo()->mapname().c_str());

		auto countrySp_ = (ImageView*)u_layer_MainScene->getChildByName("map_diBg")->getChildByName("countrySp_");
		if (countrySp_)
		{
			int countryId = GameView::getInstance()->getMapInfo()->country();
			if (countryId > 0 && countryId <6)
			{
				std::string countryIdName = "country_";
				char id[2];
				sprintf(id, "%d", countryId);
				countryIdName.append(id);
				std::string iconPathName = "res_ui/country_icon/";
				iconPathName.append(countryIdName);
				iconPathName.append(".png");
				countrySp_->loadTexture(iconPathName.c_str());
				countrySp_->setPosition(Vec2(mapName->getPosition().x - mapName->getContentSize().width/2-1,mapName->getPosition().y));
			}
			else
			{
				countrySp_->removeFromParent();
				mapName->setPosition(Vec2(0,-3));
			}
		}
		else
		{
			int countryId = GameView::getInstance()->getMapInfo()->country();
			if (countryId > 0 && countryId <6)
			{
				std::string countryIdName = "country_";
				char id[2];
				sprintf(id, "%d", countryId);
				countryIdName.append(id);
				std::string iconPathName = "res_ui/country_icon/";
				iconPathName.append(countryIdName);
				iconPathName.append(".png");
				auto countrySp_ = ImageView::create();
				countrySp_->loadTexture(iconPathName.c_str());
				countrySp_->setAnchorPoint(Vec2(1.0,0.5f));
				countrySp_->setPosition(Vec2(mapName->getPosition().x - mapName->getContentSize().width/2-1,mapName->getPosition().y));
				countrySp_->setScale(0.7f);
				countrySp_->setName("countrySp_");
				u_layer_MainScene->getChildByName("map_diBg")->addChild(countrySp_);
			}
			else
			{
				mapName->setPosition(Vec2(0,-3));
			}
		}

		auto l_channel = (Label*)u_layer_MainScene->getChildByName("map_diBg")->getChildByName("l_channel");
		if (l_channel)
		{
			std::string str_channelId = "(";
			char s_channelId[5];
			sprintf(s_channelId,"%d",GameView::getInstance()->getMapInfo()->channel()+1);
			str_channelId.append(s_channelId);
			str_channelId.append(StringDataManager::getString("GuideMap_Channel_Name"));
			str_channelId.append(")");
			l_channel->setString(str_channelId.c_str());
			l_channel->setPosition(Vec2(mapName->getPosition().x + mapName->getContentSize().width/2-1,mapName->getPosition().y));
		}
	}
}

void GuideMap::BattleSituationEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		// 初始化 UI WINDOW数据
		Size winSize = Director::getInstance()->getVisibleSize();

		BattleSituationUI* battleSituationUI = (BattleSituationUI *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagBattleSituationUI);
		if (battleSituationUI == NULL)
		{
			battleSituationUI = BattleSituationUI::create();
			battleSituationUI->setIgnoreAnchorPointForPosition(false);
			battleSituationUI->setAnchorPoint(Vec2(0.5f, 0.5f));
			battleSituationUI->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			battleSituationUI->setTag(kTagBattleSituationUI);
			GameView::getInstance()->getMainUIScene()->addChild(battleSituationUI);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void GuideMap::callBackGetReward(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		RewardUi * reward_ui = (RewardUi *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRewardUI);
		if (reward_ui == NULL)
		{
			RewardUi * reward_ui = RewardUi::create();
			reward_ui->setIgnoreAnchorPointForPosition(false);
			reward_ui->setAnchorPoint(Vec2(0.5f, 0.5f));
			reward_ui->setPosition(Vec2(winSize.width / 2, winSize.height / 2));
			reward_ui->setTag(kTagRewardUI);
			GameView::getInstance()->getMainUIScene()->addChild(reward_ui);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void GuideMap::changeBtnVisibleByType( int mapType )
{
	switch(mapType)
	{
	case com::future::threekingdoms::server::transport::protocol::coliseum:           //竞技场
		{
			//退出
			auto btn_quit = (Button*)u_layer_ArenaScene->getChildByName("btn_quit");
			if (btn_quit)
			{
				btn_quit->setVisible(true);
			}
			
			//挂机
			auto btn_hangUp = (Button*)u_layer_ArenaScene->getChildByName("btn_hangUp_1");
			if (btn_hangUp)
			{
				btn_hangUp->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_hangUp_1");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_hangUp->setVisible(false);
					}
				}
			}

			//武将模式
			auto btn_generalMode = (Button*)u_layer_ArenaScene->getChildByName("btn_generalMode");
			if (btn_generalMode)
			{
				btn_generalMode->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_generalMode");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_generalMode->setVisible(false);
					}
				}
			}
		}
		break;
	case com::future::threekingdoms::server::transport::protocol::copy:        //副本
		{
			//各个副本按钮等
			//first line
			//退出
			auto btn_quit = (Button*)u_layer_FiveInstanceScene->getChildByName("btn_exit");
			if (btn_quit)
			{
				btn_quit->setVisible(true);
			}
			//商城
			auto btn_shop = (Button*)u_layer_FiveInstanceScene->getChildByName("btn_shop");
			if (btn_shop)
			{
				btn_shop->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_shop");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_shop->setVisible(false);
					}
				}
			}

			//挂机
			Button* btn_hangUp = (Button*)u_layer_FiveInstanceScene->getChildByName("btn_hangUp_1");
			if (btn_hangUp)
			{
				btn_hangUp->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_hangUp_1");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_hangUp->setVisible(false);
					}
				}
			}

			//武将模式
			auto btn_generalMode = (Button*)u_layer_FiveInstanceScene->getChildByName("btn_generalMode");
			if (btn_generalMode)
			{
				btn_generalMode->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_generalMode");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_generalMode->setVisible(false);
					}
				}
			}

			//remind
			auto btn_remind = (Button*)u_layer_FiveInstanceScene->getChildByName("btn_remind");
			if (btn_remind)
			{
				btn_remind->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_remind");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_remind->setVisible(false);
					}
				}
			}

			//可缩回/弹出的层
			auto layer_action_temp = (Layer*)u_layer_FiveInstanceScene->getChildByTag(kTagActionLayer);
			if (layer_action_temp)
			{
				//召唤
				auto btn_callFriend = (Button*)layer_action_temp->getChildByName("btn_callFriend");
				btn_callFriend->setVisible(true);
			}
		}
		break;
	case com::future::threekingdoms::server::transport::protocol::tower:        //singCopy
		{
			//退出
			auto btn_exit = (Button*)u_layer_SingCopyScene->getChildByName("btn_exit");
			if (btn_exit)
			{
				btn_exit->setVisible(true);
			}
			//
			Button* btn_shop = (Button*)u_layer_SingCopyScene->getChildByName("btn_shop");
			if (btn_shop)
			{
				btn_shop->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_shop");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_shop->setVisible(false);
					}
				}
			}
			//挂机
			Button* btn_hangUp = (Button*)u_layer_SingCopyScene->getChildByName("btn_hangUp_1");
			if (btn_hangUp)
			{
				btn_hangUp->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_hangUp_1");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_hangUp->setVisible(false);
					}
				}
			}
			//武将模式
			auto btn_generalMode = (Button*)u_layer_SingCopyScene->getChildByName("btn_generalMode");
			if (btn_generalMode)
			{
				btn_generalMode->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_generalMode");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_generalMode->setVisible(false);
					}
				}
			}
			//remind
			auto btn_remind = (Button*)u_layer_SingCopyScene->getChildByName("btn_remind");
			if (btn_remind)
			{
				btn_remind->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_remind");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_remind->setVisible(false);
					}
				}
			}
		}break;
	case com::future::threekingdoms::server::transport::protocol::guildFight:        //家族战
		{
			//first line
			//商城
			auto btn_shop = (Button*)u_layer_FamilyFightScene->getChildByName("btn_shop");
			if (btn_shop)
			{
				btn_shop->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_shop");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_shop->setVisible(false);
					}
				}
			}

			//挂机
			Button* btn_hangUp = (Button*)u_layer_FamilyFightScene->getChildByName("btn_hangUp_1");
			if (btn_hangUp)
			{
				btn_hangUp->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_hangUp_1");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_hangUp->setVisible(false);
					}
				}
			}

			//武将模式
			auto btn_generalMode = (Button*)u_layer_FamilyFightScene->getChildByName("btn_generalMode");
			if (btn_generalMode)
			{
				btn_generalMode->setVisible(true);
				auto btn_temp = (Button*)u_layer_MainScene->getChildByName("btn_generalMode");
				if (btn_temp)
				{
					if (!btn_temp->isVisible())
					{
						btn_generalMode->setVisible(false);
					}
				}
			}

			//退出
			auto btn_quit = (Button*)u_layer_FamilyFightScene->getChildByName("btn_quit");
			if (btn_quit)
			{
				btn_quit->setVisible(true);
			}
			//战况
			auto btn_battleSituation = (Button*)u_layer_FamilyFightScene->getChildByName("btn_battleSituation");
			if (btn_battleSituation)
			{
				btn_battleSituation->setVisible(true);
			}	
		}
		break;
	default:
		{
		}
	}
}

void GuideMap::addCCTutorialIndicatorSingCopy( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	auto tutorialIndicator = CCTeachingGuide::create(content,pos,direction,54,54,true);
	tutorialIndicator->setDrawNodePos(pos);
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	auto mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

Button * GuideMap::getBtnFirstReCharge()
{
	if (u_layer_MainScene)
	{
		auto tempLayer = (Layer *)u_layer_MainScene->getChildByTag(kTagActionLayer);
		if (tempLayer)
		{
			auto btn_temp = (Button *)tempLayer->getChildByName("btn_firstBuyVip");
			if (btn_temp)
				return btn_temp;
		}
	}
	
	return NULL;
}
