#include "MissionAndTeam.h"
#include "../GameSceneState.h"
#include "../role/MyPlayer.h"
#include "GameView.h"
#include "MissionCell.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../role/ActorCommand.h"
#include "../role/MyPlayerOwnedCommand.h"
#include "../../ui/extensions/UITab.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "ShowTeamPlayer.h"
#include "../../messageclient/element/CTeamMember.h"
#include "../../legend_script/CCTutorialParticle.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/element/CFivePersonInstance.h"
#include "../../legend_script/CCTutorialIndicator.h"
#include "../../legend_script/Script.h"
#include "../../legend_script/ScriptManager.h"
#include "MyHeadInfo.h"
#include "HeadMenu.h"
#include "GuideMap.h"
#include "../MainScene.h"
#include "shortcutelement/ShortcutLayer.h"
#include "../../ui/Friend_ui/TeamMemberList.h"
#include "../role/MyPlayerAIConfig.h"
#include "../role/MyPlayerAI.h"
#include "AppMacros.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/family_ui/familyFight_ui/FamilyFightData.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../utils/GameUtils.h"
#include "../../utils/StrUtils.h"
#include "../../ui/challengeRound/SingCopyCountDownUI.h"
#include "../../ui/Friend_ui/FriendUi.h"
#include "../../legend_script/CCTeachingGuide.h"

#define MISSION_TABALEVIEW_HIGHLIGHT_TAG 21
#define KTag_MoveAction 56
#define KTag_GuidePen 65

#define KTag_FamilyFight_Label_RemainTime 75
#define KTag_OffLineArena_Label_RemainTime 76

#define kTag_offLineArena_tableView_left 90
#define kTag_offLineArena_tableView_right 91

#define LAYER_SIZE_WIDTH 170
#define LAYER_SIZE_HEIGHT 184

#define MISSIONLAYER_LAYER_HASNEXT_TAG 210

int MissionAndTeam::s_panelType = MissionAndTeam::type_FiveInstanceAndTeam;
int MissionAndTeam::s_curPresentPanel = MissionAndTeam::p_mission;

MissionAndTeam::MissionAndTeam():
isOn(true),
m_nTutorialIndicatorIndex(-1),
cur_boss_count(0),
cur_monster_count(0),
max_boss_count(0),
max_monster_count(0),
m_Target_pos_inInstance(Vec2(0,0)),
isExistMissionLayer(false),
isExistTeamLayer(false),
isExistFiveInstanceLayer(false),
isExistFamilyFightLayer(false),
isExistSingCopylayer(false),
isExistOffLineArenaLayer(false),
m_singCopy_curLevel(0),
m_singCopy_curMonsterNumber(0),
m_singCopy_monsterNumbers(0),
m_singCopy_remainMonster(0),
m_singCopy_useTime(0),
m_singCopy_timeCount(0),
m_Target_pos_inFamilyFight(Vec2(0,0)),
m_nRemainTime_Arena(600),
missionTableView(NULL),
teamTableView(NULL),
fiveInstanceTableView(NULL),
familyFightTableView(NULL),
singCopyTableview(NULL),
m_state_start(false)
{
}


MissionAndTeam::~MissionAndTeam()
{
	delete p_enemyInfoInArena;
	std::vector<GeneralInfoInArena*>::iterator iter_mine;
	for (iter_mine = generalsInArena_mine.begin(); iter_mine != generalsInArena_mine.end(); ++iter_mine)
	{
		delete *iter_mine;
	}
	generalsInArena_mine.clear();

	std::vector<GeneralInfoInArena*>::iterator iter_other;
	for (iter_other = generalsInArena_other.begin(); iter_other != generalsInArena_other.end(); ++iter_other)
	{
		delete *iter_other;
	}
	generalsInArena_other.clear();
}

MissionAndTeam * MissionAndTeam::create()
{
	auto missionAndTeam = new MissionAndTeam();
	if (missionAndTeam && missionAndTeam->init())
	{
		missionAndTeam->autorelease();
		return missionAndTeam;
	}
	CC_SAFE_DELETE(missionAndTeam);
	return NULL;
}

bool MissionAndTeam::init()
{
	if (UIScene::init())
	{
		Size visibleSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();

		m_nLastTouchTime = GameUtils::millisecondNow();

		p_enemyInfoInArena = new GeneralInfoInArena();
// 		auto ImageView_background = ImageView::create();
// 		ImageView_background->setTouchEnabled(true);
// 		ImageView_background->setScale9Enabled(true);
// 		ImageView_background->loadTexture("gamescene_state/renwuduiwu/di.png");
// 		ImageView_background->setContentSize(Size(183, 150));
// 		ImageView_background->setAnchorPoint(Vec2::ZERO);
// 		ImageView_background->setPosition(Vec2(0,0));
// 		m_pLayer->addChild(ImageView_background);

		Size tableViewSize = Size(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT);

		layer_leader = Layer::create();
		layer_leader->setContentSize(tableViewSize);
		//layer_leader->setTouchEnabled(true);
		layer_leader->setAnchorPoint(Vec2::ZERO);
		layer_leader->setPosition(Vec2(0,0));
		addChild(layer_leader);

		//任务面板
		MissionLayer = Layer::create();
		MissionLayer->setContentSize(tableViewSize);
		//MissionLayer->setTouchEnabled(true);
		MissionLayer->setAnchorPoint(Vec2::ZERO);
		MissionLayer->setPosition(Vec2(0,0));
		layer_leader->addChild(MissionLayer);
		MissionLayer->setVisible(true);


		this->setPanelType(type_FiveInstanceAndTeam);
		this->setCurPresentPanel(p_mission);
		//默认创建人物面板
		initMissionLayer();//(size:170,184)

		//队伍面板
		TeamLayer = Layer::create();
		TeamLayer->setContentSize(tableViewSize);
		//TeamLayer->setTouchEnabled(true);
		TeamLayer->setAnchorPoint(Vec2::ZERO);
		TeamLayer->setPosition(Vec2(0,0));
		layer_leader->addChild(TeamLayer);
		TeamLayer->setVisible(false);
		//initTeamLayer();

		//副本面板
		FiveInstanceLayer = Layer::create();
		FiveInstanceLayer->setContentSize(tableViewSize);
		//FiveInstanceLayer->setTouchEnabled(true);
		FiveInstanceLayer->setAnchorPoint(Vec2::ZERO);
		FiveInstanceLayer->setPosition(Vec2(0,0));
		layer_leader->addChild(FiveInstanceLayer);
		FiveInstanceLayer->setVisible(false);
		//initFiveInstanceLayer();

		//家族战面板
		FamilyFightLayer = Layer::create();
		FamilyFightLayer->setContentSize(tableViewSize);
		//FamilyFightLayer->setTouchEnabled(true);
		FamilyFightLayer->setAnchorPoint(Vec2::ZERO);
		FamilyFightLayer->setPosition(Vec2(0,0));
		layer_leader->addChild(FamilyFightLayer);
		FamilyFightLayer->setVisible(false);
		//initFamilyFightLayer();

		//singCopy panel
		SingCopyLayer = Layer::create();
		SingCopyLayer->setContentSize(tableViewSize);
		//SingCopyLayer->setTouchEnabled(true);
		SingCopyLayer->setAnchorPoint(Vec2::ZERO);
		SingCopyLayer->setPosition(Vec2(0,0));
		layer_leader->addChild(SingCopyLayer);
		SingCopyLayer->setVisible(false);
		//initSingCopyLayer();

		//竞技场面板
		OffLineArenaLayer = Layer::create();
		OffLineArenaLayer->setContentSize(tableViewSize);
		//OffLineArenaLayer->setTouchEnabled(true);
		OffLineArenaLayer->setAnchorPoint(Vec2::ZERO);
		OffLineArenaLayer->setPosition(Vec2(0,0));
		layer_leader->addChild(OffLineArenaLayer);
		OffLineArenaLayer->setVisible(false);

		auto Layer = Layer::create();
		addChild(Layer);
		//弹出/收回
		missionAndTeam_on_off = Button::create();
		missionAndTeam_on_off->setTouchEnabled(true);
		missionAndTeam_on_off->setPressedActionEnabled(true);
		missionAndTeam_on_off->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/buttons.png", "gamescene_state/zhujiemian3/renwuduiwu/buttons.png", "");
		missionAndTeam_on_off->setAnchorPoint(Vec2(0.5f,0.5f));
		missionAndTeam_on_off->setPosition(Vec2(missionAndTeam_on_off->getContentSize().width/2,LAYER_SIZE_HEIGHT+missionAndTeam_on_off->getContentSize().height/2+1));
		missionAndTeam_on_off->addTouchEventListener(CC_CALLBACK_2(MissionAndTeam::MissionAndTeamOnOff, this));
		Layer->addChild(missionAndTeam_on_off);
		ImageView_missionAndTeam_on_off = ImageView::create();
		ImageView_missionAndTeam_on_off->loadTexture("gamescene_state/zhujiemian3/renwuduiwu/arrow.png");
		ImageView_missionAndTeam_on_off->setAnchorPoint(Vec2(0.5f,0.5f));
		ImageView_missionAndTeam_on_off->setPosition(Vec2(missionAndTeam_on_off->getBoundingBox().size.width/2, missionAndTeam_on_off->getBoundingBox().size.height/2));
		missionAndTeam_on_off->addChild(ImageView_missionAndTeam_on_off);
		ImageView_missionAndTeam_on_off->setRotation(180);
		ImageView_missionAndTeam_on_off->setScale(0.8f);

		Button_mission = Button::create();
		Button_mission->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
		Button_mission->setTouchEnabled(true);
		Button_mission->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_mission->setPosition(Vec2(missionAndTeam_on_off->getContentSize().width+Button_mission->getContentSize().width/2,LAYER_SIZE_HEIGHT+Button_mission->getContentSize().height/2+1));
		Button_mission->addTouchEventListener(CC_CALLBACK_2(MissionAndTeam::ButtonMissionEvent, this));
		Button_mission->setPressedActionEnabled(true);
		layer_leader->addChild(Button_mission);
		auto ImageView_mission = ImageView::create();
		ImageView_mission->loadTexture("gamescene_state/zhujiemian3/renwuduiwu/task.png");
		ImageView_mission->setAnchorPoint(Vec2(0.5f,0.5f));
		ImageView_mission->setPosition(Vec2(Button_mission->getBoundingBox().size.width/2, Button_mission->getBoundingBox().size.height/2));
		Button_mission->addChild(ImageView_mission);

		Button_fiveInstance = Button::create();
		Button_fiveInstance->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
		Button_fiveInstance->setTouchEnabled(true);
		Button_fiveInstance->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_fiveInstance->setPosition(Vec2(missionAndTeam_on_off->getContentSize().width+Button_fiveInstance->getContentSize().width/2,LAYER_SIZE_HEIGHT+Button_fiveInstance->getContentSize().height/2+1));
		Button_fiveInstance->addTouchEventListener(CC_CALLBACK_2(MissionAndTeam::ButtonFiveInstanceEvent, this));
		Button_fiveInstance->setPressedActionEnabled(true);
		layer_leader->addChild(Button_fiveInstance);
		auto ImageView_fiveInstance = ImageView::create();
		ImageView_fiveInstance->loadTexture("gamescene_state/zhujiemian3/renwuduiwu/information.png");
		ImageView_fiveInstance->setAnchorPoint(Vec2(0.5f,0.5f));
		ImageView_fiveInstance->setPosition(Vec2(Button_fiveInstance->getBoundingBox().size.width/2, Button_fiveInstance->getBoundingBox().size.height/2));
		Button_fiveInstance->addChild(ImageView_fiveInstance);

		Button_team = Button::create();
		Button_team->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
		Button_team->setTouchEnabled(true);
		Button_team->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_team->setPosition(Vec2(Button_mission->getPosition().x+Button_mission->getContentSize().width,LAYER_SIZE_HEIGHT+Button_team->getContentSize().height/2+1));
		Button_team->addTouchEventListener(CC_CALLBACK_2(MissionAndTeam::ButtonTeamEvent, this));
		Button_team->setPressedActionEnabled(true);
		layer_leader->addChild(Button_team);
		auto ImageView_team = ImageView::create();
		ImageView_team->loadTexture("gamescene_state/zhujiemian3/renwuduiwu/team.png");
		ImageView_team->setAnchorPoint(Vec2(0.5f,0.5f));
		ImageView_team->setPosition(Vec2(Button_team->getBoundingBox().size.width / 2, Button_team->getBoundingBox().size.height / 2));
		Button_team->addChild(ImageView_team);

		Button_familyFight = Button::create();
		Button_familyFight->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
		Button_familyFight->setTouchEnabled(true);
		Button_familyFight->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_familyFight->setPosition(Vec2(missionAndTeam_on_off->getContentSize().width+Button_familyFight->getContentSize().width/2,LAYER_SIZE_HEIGHT+Button_familyFight->getContentSize().height/2+1));
		Button_familyFight->addTouchEventListener(CC_CALLBACK_2(MissionAndTeam::ButtonFamilyFightEvent, this));
		Button_familyFight->setPressedActionEnabled(true);
		layer_leader->addChild(Button_familyFight);
		auto ImageView_familyFight = ImageView::create();
		ImageView_familyFight->loadTexture("gamescene_state/zhujiemian3/renwuduiwu/information.png");
		ImageView_familyFight->setAnchorPoint(Vec2(0.5f,0.5f));
		ImageView_familyFight->setPosition(Vec2(Button_familyFight->getBoundingBox().size.width / 2, Button_familyFight->getBoundingBox().size.height / 2));
		Button_familyFight->addChild(ImageView_familyFight);
		//add by liuzhenxing
		Button_singCopy = Button::create();
		Button_singCopy->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
		Button_singCopy->setTouchEnabled(true);
		Button_singCopy->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_singCopy->setPosition(Vec2(missionAndTeam_on_off->getContentSize().width+Button_singCopy->getContentSize().width/2,LAYER_SIZE_HEIGHT+Button_singCopy->getContentSize().height/2+1));
		Button_singCopy->addTouchEventListener(CC_CALLBACK_2(MissionAndTeam::ButtonSingCopyChallengeEvent, this));
		Button_singCopy->setPressedActionEnabled(true);
		layer_leader->addChild(Button_singCopy);
		auto ImageView_SingCopyCheallenge = ImageView::create();
		ImageView_SingCopyCheallenge->loadTexture("gamescene_state/zhujiemian3/renwuduiwu/information.png");
		ImageView_SingCopyCheallenge->setAnchorPoint(Vec2(0.5f,0.5f));
		ImageView_SingCopyCheallenge->setPosition(Vec2(Button_singCopy->getBoundingBox().size.width / 2, Button_singCopy->getBoundingBox().size.height / 2));
		Button_singCopy->addChild(ImageView_SingCopyCheallenge);

		Button_offLineArena = Button::create();
		Button_offLineArena->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
		Button_offLineArena->setTouchEnabled(true);
		Button_offLineArena->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_offLineArena->setPosition(Vec2(missionAndTeam_on_off->getContentSize().width+Button_offLineArena->getContentSize().width/2,LAYER_SIZE_HEIGHT+Button_offLineArena->getContentSize().height/2+1));
		Button_offLineArena->addTouchEventListener(CC_CALLBACK_2(MissionAndTeam::ButtonOffLineArenaEvent, this));
		Button_offLineArena->setPressedActionEnabled(true);
		layer_leader->addChild(Button_offLineArena);
		auto ImageView_offLineArena = ImageView::create();
		ImageView_offLineArena->loadTexture("gamescene_state/zhujiemian3/renwuduiwu/information.png");
		ImageView_offLineArena->setAnchorPoint(Vec2(0.5f,0.5f));
		ImageView_offLineArena  ->setPosition(Vec2(Button_offLineArena->getBoundingBox().size.width / 2, Button_offLineArena->getBoundingBox().size.height / 2));
		Button_offLineArena->addChild(ImageView_offLineArena);

		refreshFunctionBtn();

		this->setIgnoreAnchorPointForPosition(false);
		this->setAnchorPoint(Vec2(0,0));
		this->setPosition(Vec2(0,0));
		////this->setTouchEnabled(true);
		//this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		contentSizeHeight = tableViewSize.height + Button_team->getContentSize().height +5;
		this->setContentSize(Size(tableViewSize.width,contentSizeHeight));

		this->schedule(CC_SCHEDULE_SELECTOR(MissionAndTeam::updateFamilyFightRemainTime),1.0f);
		this->schedule(CC_SCHEDULE_SELECTOR(MissionAndTeam::updateOffLineArenaRemainTime),1.0f);


		auto touchListener = EventListenerTouchOneByOne::create();
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(MissionAndTeam::onTouchBegan, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(MissionAndTeam::onTouchEnded, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(MissionAndTeam::onTouchMoved, this);

		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener, this);

		return true;
	}
	return false;
}


void MissionAndTeam::onEnter()
{
	UIScene::onEnter();
}
void MissionAndTeam::onExit()
{
	UIScene::onExit();
}

bool MissionAndTeam::onTouchBegan(Touch *pTouch, Event *pEvent)
{
	touchBeganPoint = pTouch->getLocation();

	// convert the touch point to OpenGL coordinates
	Vec2 location = pTouch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	if (s_curPresentPanel == p_mission)
	{
		if (GameView::getInstance()->missionManager->MissionList_MainScene.size()>0)
		{
			if (60*GameView::getInstance()->missionManager->MissionList_MainScene.size() > this->getContentSize().height)
			{
			}
			else
			{
				pos = Vec2(this->getPosition().x,this->getPosition().y+3+60*(3-GameView::getInstance()->missionManager->MissionList_MainScene.size()));
				size = Size(this->getContentSize().width,60*(GameView::getInstance()->missionManager->MissionList_MainScene.size()));
			}
		}
		else
		{
			pos = Vec2(this->getPosition().x,60*(3-GameView::getInstance()->missionManager->MissionList_MainScene.size()));
			size = Size(0,0);
		}
	}
	else if (s_curPresentPanel == p_team)
	{
		if (GameView::getInstance()->teamMemberVector.size()>0)
		{
			if (40*GameView::getInstance()->teamMemberVector.size() > this->getContentSize().height)
			{
			}
			else
			{
				pos = Vec2(this->getPosition().x,this->getPosition().y+40*(4-GameView::getInstance()->teamMemberVector.size()));
				size = Size(this->getContentSize().width,40*(GameView::getInstance()->teamMemberVector.size()));
			}
		}
		else
		{
			pos = Vec2(this->getPosition().x,40*(4-GameView::getInstance()->teamMemberVector.size()));
			size = Size(0,0);
		}
	}
	else
	{

	}
	
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos -anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		//add by yangjun 2014.10.13
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		return true;
	}

	return false;

	// convert the touch point to OpenGL coordinates
// 	Vec2 location = pTouch->getLocation();
// 
// 	Vec2 pos = this->getPosition();
// 	Size size = this->getContentSize();
// 	Vec2 anchorPoint = this->getAnchorPointInPoints();
// 	pos = ccpSub(pos, anchorPoint);
// 	Rect rect(pos.x, pos.y, size.width, size.height);
// 	if(rect.containsPoint(location))
// 	{
// 		return true;
// 	}
// 
// 	return false;
}
void MissionAndTeam::onTouchMoved(Touch *pTouch, Event *pEvent)
{
}
void MissionAndTeam::onTouchEnded(Touch *pTouch, Event *pEvent)
{
	Vec2 touchEndedPoint = pTouch->getLocation();
	
	if (isOn)
	{
		int offsetX = touchEndedPoint.x-touchBeganPoint.x;
		int offsetY = touchEndedPoint.y-touchBeganPoint.y;
		if ( ( abs(offsetY) < 30 )  && ( offsetX < -60 ) )
		{
			//开启等级
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(21);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[21];
			}

			if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				char s_des [200];
				const char* str_des = StringDataManager::getString("shrinkage_will_be_open");
				sprintf(s_des,str_des,openlevel);
				GameView::getInstance()->showAlertDialog(s_des);
				return;
			}

			Action * action1 = MoveTo::create(0.15f,Vec2(layer_leader->getPositionX()-LAYER_SIZE_WIDTH,layer_leader->getPositionY()));
			action1->setTag(KTag_MoveAction);
			layer_leader->runAction(action1);

			isOn = false;
			ImageView_missionAndTeam_on_off->setRotation(0);
			this->setContentSize(Size(32,contentSizeHeight));

			refreshTutorial();
		}
	}
	else
	{
		if ((touchEndedPoint.x-touchBeganPoint.x) > 60 )
		{
			//开启等级
			int openlevel = 0;
			std::map<int,int>::const_iterator cIter;
			cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(21);
			if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
			{
			}
			else
			{
				openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[21];
			}

			if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
			{
				char s_des [200];
				const char* str_des = StringDataManager::getString("shrinkage_will_be_open");
				sprintf(s_des,str_des,openlevel);
				GameView::getInstance()->showAlertDialog(s_des);
				return;
			}

			//open
			Action * action1 = MoveTo::create(0.15f,Vec2(layer_leader->getPositionX()+LAYER_SIZE_WIDTH,layer_leader->getPositionY()));
			action1->setTag(KTag_MoveAction);
			layer_leader->runAction(action1);

			isOn = true;
			ImageView_missionAndTeam_on_off->setRotation(180);
			this->setContentSize(Size(LAYER_SIZE_WIDTH,contentSizeHeight));

			refreshTutorial();
		}
	}
}
void MissionAndTeam::onTouchCancelled(Touch *pTouch, Event *pEvent)
{
}

void MissionAndTeam::update( float dt )
{
}

void MissionAndTeam::ButtonMissionEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		setCurPresentPanel(p_mission);
		initMissionLayer();

		Button_mission->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "");
		MissionLayer->setVisible(true);
		missionTableView->reloadData();
		this->refreshDownArrowStatus();

		Button_team->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		TeamLayer->setVisible(false);

		Button_fiveInstance->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		FiveInstanceLayer->setVisible(false);

		Button_familyFight->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		FamilyFightLayer->setVisible(false);

		Button_singCopy->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "");
		SingCopyLayer->setVisible(false);

		Button_offLineArena->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "");
		OffLineArenaLayer->setVisible(false);

		if (!isOn)
		{
			popUpMission(pSender);
		}

		refreshTutorial();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}
}

void MissionAndTeam::ButtonTeamEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		int selfLevel_ = GameView::getInstance()->myplayer->getActiveRole()->level();
		int openlevel = 0;
		std::map<int, int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(27);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[27];
		}

		if (selfLevel_ < openlevel)
		{
			const char * str_ = StringDataManager::getString("missAndTeam_buttonTeamisOpen");

			char str_open[50];
			sprintf(str_open, str_, openlevel);

			GameView::getInstance()->showAlertDialog(str_open);
			return;
		}

		//add by yangjun 2014.10.13
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		setCurPresentPanel(p_team);
		initTeamLayer();

		Button_mission->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		MissionLayer->setVisible(false);

		Button_fiveInstance->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		FiveInstanceLayer->setVisible(false);

		Button_team->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "");
		TeamLayer->setVisible(true);
		teamTableView->reloadData();

		Button_familyFight->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		FamilyFightLayer->setVisible(false);

		Button_singCopy->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		SingCopyLayer->setVisible(false);

		Button_offLineArena->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		OffLineArenaLayer->setVisible(false);

		if (!isOn)
		{
			popUpMission(pSender);
		}

		refreshTutorial();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void MissionAndTeam::ButtonFiveInstanceEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		setCurPresentPanel(p_fiveInstance);
		initFiveInstanceLayer();

		Button_mission->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		MissionLayer->setVisible(false);

		Button_fiveInstance->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "");
		FiveInstanceLayer->setVisible(true);
		fiveInstanceTableView->reloadData();

		Button_team->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		TeamLayer->setVisible(false);

		Button_familyFight->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		FamilyFightLayer->setVisible(false);

		Button_singCopy->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "");
		SingCopyLayer->setVisible(false);

		Button_offLineArena->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "");
		OffLineArenaLayer->setVisible(false);

		if (!isOn)
		{
			popUpMission(pSender);
		}

		refreshTutorial();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void MissionAndTeam::ButtonFamilyFightEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		setCurPresentPanel(p_familyFight);
		initFamilyFightLayer();

		Button_mission->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		MissionLayer->setVisible(false);

		Button_fiveInstance->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		FiveInstanceLayer->setVisible(false);;

		Button_team->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		TeamLayer->setVisible(false);

		Button_familyFight->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "");
		FamilyFightLayer->setVisible(true);
		familyFightTableView->reloadData();

		Button_singCopy->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "");
		SingCopyLayer->setVisible(false);

		Button_offLineArena->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "");
		OffLineArenaLayer->setVisible(false);

		if (!isOn)
		{
			popUpMission(pSender);
		}

		refreshTutorial();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void MissionAndTeam::ButtonSingCopyChallengeEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

	//add by yangjun 2014.10.13
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		if (mainScene->isHeadMenuOn)
		{
			mainScene->ButtonHeadEvent(NULL);
		}
	}

	setCurPresentPanel(p_singCopyChallenge);
	initSingCopyLayer();
	
	Button_mission->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	MissionLayer->setVisible(false);

	Button_fiveInstance->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	FiveInstanceLayer->setVisible(false);

	Button_team->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	TeamLayer->setVisible(false);

	Button_familyFight->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
	FamilyFightLayer->setVisible(false);

	Button_singCopy->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	SingCopyLayer->setVisible(true);
	//singCopyTableview->reloadData();

	Button_offLineArena->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
	OffLineArenaLayer->setVisible(false);

	if(!isOn)
	{
		popUpMission(pSender);
	}

	refreshTutorial();
}


void MissionAndTeam::ButtonOffLineArenaEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//add by yangjun 2014.10.13
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			if (mainScene->isHeadMenuOn)
			{
				mainScene->ButtonHeadEvent(NULL);
			}
		}

		setCurPresentPanel(p_offLineArena);
		initOffLineArenaLayer();

		Button_mission->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		MissionLayer->setVisible(false);

		Button_fiveInstance->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		FiveInstanceLayer->setVisible(false);

		Button_team->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		TeamLayer->setVisible(false);

		Button_familyFight->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "gamescene_state/zhujiemian3/renwuduiwu/button_off.png", "");
		FamilyFightLayer->setVisible(false);

		Button_singCopy->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "");
		SingCopyLayer->setVisible(false);

		Button_offLineArena->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "gamescene_state/zhujiemian3/renwuduiwu/button_on.png", "");
		OffLineArenaLayer->setVisible(true);
		auto offLineArenaTableView_left = (TableView*)OffLineArenaLayer->getChildByTag(kTag_offLineArena_tableView_left);
		if (offLineArenaTableView_left)
			offLineArenaTableView_left->reloadData();
		auto offLineArenaTableView_right = (TableView*)OffLineArenaLayer->getChildByTag(kTag_offLineArena_tableView_right);
		if (offLineArenaTableView_right)
			offLineArenaTableView_right->reloadData();

		if (!isOn)
		{
			popUpMission(pSender);
		}

		refreshTutorial();
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}


void MissionAndTeam::initMissionLayer()
{
	if (isExistMissionLayer)
		return;
// 	auto  sprite_frame = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/renwu_di.png");
// 	sprite_frame->setAnchorPoint(Vec2(0,0));
// 	sprite_frame->setPosition(Vec2(0,0));
// 	sprite_frame->setPreferredSize(Size(160,160));
// 	sprite_frame->setCapInsets(Rect(12,12,1,1));
// 	MissionLayer->addChild(sprite_frame);

	Vec2 _s = MissionManager::getInstance()->getMissionAndTeamOffSet();

	missionTableView = TableView::create(this,Size(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	missionTableView->setDirection(TableView::Direction::VERTICAL);
	missionTableView->setAnchorPoint(Vec2(0,0));
	missionTableView->setPosition(Vec2(0,0));
	missionTableView->setDelegate(this);
	missionTableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//missionTableView->setPressedActionEnabled(true, 0.8f, 1.2f);
	MissionLayer->addChild(missionTableView);

	if (!MissionManager::getInstance()->getIsFirstCreateMissionAndTeam())
	{
		int _h = missionTableView->getContentSize().height + _s.y;
		missionTableView->reloadData();
		Vec2 temp = Vec2(_s.x,_h - missionTableView->getContentSize().height);
		missionTableView->setContentOffset(temp); 

		this->refreshDownArrowStatus();
	}

	auto layer_hasNextMission= Layer::create();
	MissionLayer->addChild(layer_hasNextMission);
	layer_hasNextMission->setTag(MISSIONLAYER_LAYER_HASNEXT_TAG);
	auto image_hasNextMission = ImageView::create();
	image_hasNextMission->loadTexture("gamescene_state/zhujiemian3/renwuduiwu/arrow2.png");
	image_hasNextMission->setAnchorPoint(Vec2(0.5f,0.5f));
	image_hasNextMission->setPosition(Vec2(LAYER_SIZE_WIDTH/2,0));
	image_hasNextMission->setName("image_hasNextMission");
	layer_hasNextMission->addChild(image_hasNextMission);
	this->refreshDownArrowStatus();

	MissionManager::getInstance()->setIsFirstCreateMissionAndTeam(false);

	isExistMissionLayer = true;
}

void MissionAndTeam::initTeamLayer()
{
	if (isExistTeamLayer)
		return;

	auto image_frame = ImageView::create();
	image_frame->loadTexture("res_ui/zhezhao70.png");
	image_frame->setAnchorPoint(Vec2(0,0));
	image_frame->setPosition(Vec2(0,0));
	image_frame->setContentSize(Size(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	image_frame->setCapInsets(Rect(7,7,1,1));
	image_frame->setOpacity(160);
	image_frame->setLocalZOrder(0);
	TeamLayer->addChild(image_frame);

	teamInvite_btn = Button::create();
	teamInvite_btn->setScale9Enabled(true);
	teamInvite_btn->setContentSize(Size(160,30));
	teamInvite_btn->setCapInsets(Rect(12,12,1,1));
	teamInvite_btn->loadTextures("res_ui/none.png","res_ui/none.png","res_ui/none.png");
	teamInvite_btn->setTouchEnabled(true);
	teamInvite_btn->setPressedActionEnabled(true);
	teamInvite_btn->setAnchorPoint(Vec2(0.5f,0.5f));
	teamInvite_btn->setPosition(Vec2(image_frame->getContentSize().width/2,image_frame->getContentSize().height - teamInvite_btn->getContentSize().height/2 - 7));
	teamInvite_btn->addTouchEventListener(CC_CALLBACK_2(MissionAndTeam::callBackTeamInvite, this));
	teamInvite_btn->setLocalZOrder(1);
	teamInvite_btn->setName("teamInvite_btn_name");
	TeamLayer->addChild(teamInvite_btn);

	auto teamInvite_label = Label::createWithBMFont("res_ui/font/ziti_3.fnt", StringDataManager::getString("mainScene_missionAndTeam_invite"));
	teamInvite_label->setAnchorPoint(Vec2(0.5f,0.5f));
	teamInvite_label->setPosition(Vec2(0,0));
	teamInvite_label->setLocalZOrder(5);
	teamInvite_btn->addChild(teamInvite_label);

	teamTableView = TableView::create(this,Size(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	teamTableView->setDirection(TableView::Direction::VERTICAL);
	teamTableView->setAnchorPoint(Vec2(0,0));
	teamTableView->setPosition(Vec2(0,4));
	teamTableView->setDelegate(this);
	teamTableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//teamTableView->setPressedActionEnabled(true, 0.8f, 1.2f);
	TeamLayer->addChild(teamTableView);
	isExistTeamLayer = true;
	refreshTeamPlayer();
 }

void MissionAndTeam::initFiveInstanceLayer()
{
	if (isExistFiveInstanceLayer)
		return;

	auto  sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao70.png");
	sprite_frame->setAnchorPoint(Vec2(0,0));
	sprite_frame->setPosition(Vec2(0,0));
	sprite_frame->setPreferredSize(Size(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	sprite_frame->setCapInsets(Rect(7,7,1,1));
	sprite_frame->setOpacity(160);
	FiveInstanceLayer->addChild(sprite_frame);

	fiveInstanceTableView = TableView::create(this,Size(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	fiveInstanceTableView->setDirection(TableView::Direction::VERTICAL);
	fiveInstanceTableView->setAnchorPoint(Vec2(0,0));
	fiveInstanceTableView->setPosition(Vec2(0,2));
	fiveInstanceTableView->setDelegate(this);
	fiveInstanceTableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//fiveInstanceTableView->setPressedActionEnabled(true, 0.8f, 1.2f);
	fiveInstanceTableView->setBounceable(false);
	FiveInstanceLayer->addChild(fiveInstanceTableView);

	auto l_des = Label::createWithTTF(StringDataManager::getString("mainScene_missionAndTeam_instanceDes"),APP_FONT_NAME,16, Size(155, 0), TextHAlignment::LEFT);
	l_des->setAnchorPoint(Vec2(0,0));
	l_des->setPosition(Vec2(6,10));
	l_des->setColor(Color3B(0,255,0));
	FiveInstanceLayer->addChild(l_des);

	isExistFiveInstanceLayer = true;
}

void MissionAndTeam::initFamilyFightLayer()
{
	if(isExistFamilyFightLayer)
		return;

	auto  sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao70.png");
	sprite_frame->setAnchorPoint(Vec2(0,0));
	sprite_frame->setPosition(Vec2(0,0));
	sprite_frame->setPreferredSize(Size(LAYER_SIZE_WIDTH,24));
	sprite_frame->setCapInsets(Rect(7,7,15,15));
	sprite_frame->setOpacity(160);
	FamilyFightLayer->addChild(sprite_frame);
	//剩余时间：
	auto l_familyFight_remainTime = Label::createWithTTF(StringDataManager::getString("Arena_remainTime"),APP_FONT_NAME,16);
	l_familyFight_remainTime->setAnchorPoint(Vec2(0,.5f));
	l_familyFight_remainTime->setPosition(Vec2(3,sprite_frame->getContentSize().height/2));
	sprite_frame->addChild(l_familyFight_remainTime);
	//00:12:34
	auto l_familyFight_remainTimeValue = Label::createWithTTF("00:15:00",APP_FONT_NAME,16);
	l_familyFight_remainTimeValue->setAnchorPoint(Vec2(0,.5f));
	l_familyFight_remainTimeValue->setPosition(Vec2(l_familyFight_remainTime->getPositionX()+l_familyFight_remainTime->getContentSize().width,sprite_frame->getContentSize().height/2));
	l_familyFight_remainTimeValue->setTag(KTag_FamilyFight_Label_RemainTime);
	FamilyFightLayer->addChild(l_familyFight_remainTimeValue);

	familyFightTableView = TableView::create(this,Size(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT-25));
	familyFightTableView->setDirection(TableView::Direction::VERTICAL);
	familyFightTableView->setAnchorPoint(Vec2(0,0));
	familyFightTableView->setPosition(Vec2(0,25));
	familyFightTableView->setDelegate(this);
	familyFightTableView->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//familyFightTableView->setPressedActionEnabled(true, 0.8f, 1.2f);
	//familyFightTableView->setBounceable(false);
	FamilyFightLayer->addChild(familyFightTableView);

	isExistFamilyFightLayer = true;
}

void  MissionAndTeam::initSingCopyLayer()
{
	if (isExistSingCopylayer)
		return;

	auto image_fram = ImageView::create();
	image_fram->loadTexture("res_ui/zhezhao70.png");
	image_fram->setScale9Enabled(true);
	image_fram->setContentSize(Size(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	image_fram->setAnchorPoint(Vec2(0,0));
	image_fram->setCapInsets(Rect(7,7,1,1));
	image_fram->setOpacity(160);
	SingCopyLayer->addChild(image_fram);

	auto scrollView_info = ui::ScrollView::create();
	scrollView_info->setTouchEnabled(true);
	scrollView_info->setDirection(ui::ScrollView::Direction::VERTICAL);
	scrollView_info->setContentSize(Size(LAYER_SIZE_WIDTH,LAYER_SIZE_HEIGHT));
	scrollView_info->setPosition(Vec2(0,0));
	scrollView_info->setBounceEnabled(true);
	scrollView_info->setTag(10);
	SingCopyLayer->addChild(scrollView_info);
	
	auto btn_sp = Button::create();
	btn_sp->setTouchEnabled(true);
	btn_sp->setPressedActionEnabled(true);
	btn_sp->loadTextures("res_ui/suibian.png","gamescene_state/zhujiemian3/renwuduiwu/light_1.png","");//
	btn_sp->setScale9Enabled(true);
	btn_sp->setContentSize(Size(LAYER_SIZE_WIDTH - 1,LAYER_SIZE_HEIGHT - 1));
	btn_sp->setAnchorPoint(Vec2(0.5f,0.5f));
	btn_sp->setPosition(Vec2(LAYER_SIZE_WIDTH/2,LAYER_SIZE_HEIGHT/2));
	btn_sp->addTouchEventListener(CC_CALLBACK_2(MissionAndTeam::callBackSingCopy, this));
	scrollView_info->addChild(btn_sp);

	const char *strings_curLevelBmfont = StringDataManager::getString("singleCopy_curLevelLabel");
	char levelStr[50];
	sprintf(levelStr,strings_curLevelBmfont,m_singCopy_curLevel);

	curLevel_BmFont = Label::createWithBMFont("res_ui/font/ziti_3.fnt", levelStr);
	curLevel_BmFont->setAnchorPoint(Vec2(0.5f,1.0));
	curLevel_BmFont->setScale(1.1f);
	curLevel_BmFont->setPosition(Vec2(0,btn_sp->getContentSize().height/2 - curLevel_BmFont->getContentSize().height/2));
	btn_sp->addChild(curLevel_BmFont);
	//test 当前进度： 1/5
	std::string curLevelShowString = StringDataManager::getString("singCopy_curLevelShowLabel");

	auto l_curLevelShowLabel = Label::createWithTTF(curLevelShowString.c_str(), APP_FONT_NAME, 16);
	l_curLevelShowLabel->setAnchorPoint(Vec2(0,1.0f));
	l_curLevelShowLabel->setPosition(Vec2(5 - btn_sp->getContentSize().width/2,curLevel_BmFont->getPosition().y - curLevel_BmFont->getContentSize().height - 2));
	l_curLevelShowLabel->setColor(Color3B(243,252,2));
	btn_sp->addChild(l_curLevelShowLabel);

	m_curLevelShowValue = LabelAtlas::create("0/0", "res_ui/font/yellow_num.png", 10, 21, '/');
	//m_curLevelShowValue->setProperty("0/0", "res_ui/font/yellow_num.png", 10, 21, "/");
	m_curLevelShowValue->setAnchorPoint(Vec2(0,1.0f));
	m_curLevelShowValue->setPosition(Vec2(5+l_curLevelShowLabel->getPosition().x+l_curLevelShowLabel->getContentSize().width,l_curLevelShowLabel->getPosition().y+2));
	btn_sp->addChild(m_curLevelShowValue);
	//monster 本波怪物剩余：50
	std::string curMonsterShowString = StringDataManager::getString("singCopy_curMonsterNum");
	auto label_monster = Label::createWithTTF(curMonsterShowString.c_str(), APP_FONT_NAME, 16);
	label_monster->setAnchorPoint(Vec2(0.f,1.0f));
	label_monster->setPosition(Vec2(5 - btn_sp->getContentSize().width/2,l_curLevelShowLabel->getPosition().y-l_curLevelShowLabel->getContentSize().height-2));
	label_monster->setColor(Color3B(243,252,2));
	btn_sp->addChild(label_monster);

	label_monsterValue = LabelAtlas::create("0", "res_ui/font/yellow_num.png",10, 12, '/');
	//label_monsterValue->setProperty("0", "res_ui/font/yellow_num.png", 10, 21, "/");
	label_monsterValue->setAnchorPoint(Vec2(0.f,1.0f));
	label_monsterValue->setPosition(Vec2(5+label_monster->getPosition().x+ label_monster->getContentSize().width,label_monster->getPosition().y+2));
	btn_sp->addChild(label_monsterValue);
	//use time 已用时：100
	std::string useTimeShowString = StringDataManager::getString("singCopy_useTime_");
	auto l_useTime = Label::createWithTTF(useTimeShowString.c_str(), APP_FONT_NAME, 16);
	l_useTime->setAnchorPoint(Vec2(0.f,1.0f));
	l_useTime->setPosition(Vec2(5- btn_sp->getContentSize().width/2,label_monster->getPosition().y - label_monster->getContentSize().height - 2));
	l_useTime->setColor(Color3B(243,252,2)); 
	btn_sp->addChild(l_useTime);
	
	m_useTimeValue = Label::createWithTTF("0", APP_FONT_NAME, 16);
	m_useTimeValue->setAnchorPoint(Vec2(0.f,1.0f));
	m_useTimeValue->setPosition(Vec2(5+l_useTime->getPosition().x+l_useTime->getContentSize().width,l_useTime->getPosition().y));
	btn_sp->addChild(m_useTimeValue);
	
	//all time 总时长：500
	std::string allTimeShowString_xian = StringDataManager::getString("singCopy_allTime_xian");
	auto l_allTime_xian = Label::createWithTTF(allTimeShowString_xian.c_str(), APP_FONT_NAME, 16);
	l_allTime_xian->setAnchorPoint(Vec2(0.f,1.0f));
	l_allTime_xian->setColor(Color3B(243,252,2)); 
	l_allTime_xian->setPosition(Vec2(5- btn_sp->getContentSize().width/2,l_useTime->getPosition().y-l_useTime->getContentSize().height-2));
	btn_sp->addChild(l_allTime_xian);

	std::string allTimeShowString_shi = StringDataManager::getString("singCopy_allTime_shi");
	auto l_allTime_shi = Label::createWithTTF(allTimeShowString_shi.c_str(), APP_FONT_NAME, 16);
	l_allTime_shi->setAnchorPoint(Vec2(1.0f,1.0f));
	l_allTime_shi->setColor(Color3B(243,252,2));
	l_allTime_shi->setPosition(Vec2(l_useTime->getPosition().x+l_useTime->getContentSize().width,l_allTime_xian->getPosition().y));
	btn_sp->addChild(l_allTime_shi);

	allTimeShowValue = Label::createWithTTF("0", APP_FONT_NAME, 16);
	allTimeShowValue->setAnchorPoint(Vec2(0.f,1.0f));
	allTimeShowValue->setPosition(Vec2(5+l_useTime->getPosition().x+l_useTime->getContentSize().width,l_allTime_xian->getPosition().y));
	btn_sp->addChild(allTimeShowValue);

	int tmep_scrollHigh = curLevel_BmFont->getContentSize().height + l_curLevelShowLabel->getContentSize().height*4+12;
	scrollView_info->setInnerContainerSize(Size(LAYER_SIZE_WIDTH,tmep_scrollHigh));
	/*
	curLevel_BmFont->setPosition(Vec2(85,scrollView_info->getContentSize().height - curLevel_BmFont->getContentSize().height/2));
	l_curLevelShowLabel->setPosition(Vec2(5,curLevel_BmFont->getPosition().y - curLevel_BmFont->getContentSize().height - 2));
	m_curLevelShowValue->setPosition(Vec2(5+l_curLevelShowLabel->getPosition().x+l_curLevelShowLabel->getContentSize().width,l_curLevelShowLabel->getPosition().y+2));

	label_monster->setPosition(Vec2(5,l_curLevelShowLabel->getPosition().y-l_curLevelShowLabel->getContentSize().height-2));
	label_monsterValue->setPosition(Vec2(5+label_monster->getPosition().x+ label_monster->getContentSize().width,label_monster->getPosition().y+2));

	l_useTime->setPosition(Vec2(5,label_monster->getPosition().y - label_monster->getContentSize().height - 2));
	m_useTimeValue->setPosition(Vec2(5+l_useTime->getPosition().x+l_useTime->getContentSize().width,l_useTime->getPosition().y));

	l_allTime_xian->setPosition(Vec2(5,l_useTime->getPosition().y-l_useTime->getContentSize().height-2));
	l_allTime_shi->setPosition(Vec2(l_useTime->getPosition().x+l_useTime->getContentSize().width,l_allTime_xian->getPosition().y));

	allTimeShowValue->setPosition(Vec2(5+l_useTime->getPosition().x+l_useTime->getContentSize().width,l_allTime_xian->getPosition().y));
	*/
	/*
	auto  sprite_frame = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/renwu_di.png");
	sprite_frame->setAnchorPoint(Vec2(0,0));
	sprite_frame->setPosition(Vec2(0,0));
	sprite_frame->setPreferredSize(Size(135,160));
	sprite_frame->setCapInsets(Rect(12,12,1,1));
	SingCopyLayer->addChild(sprite_frame);

	singCopyTableview = TableView::create(this,Size(135,156));
	singCopyTableview->setDirection(TableView::Direction::VERTICAL);
	singCopyTableview->setAnchorPoint(Vec2(0,0));
	singCopyTableview->setPosition(Vec2(0,2));
	singCopyTableview->setDelegate(this);
	singCopyTableview->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	singCopyTableview->setPressedActionEnabled(true, 0.8f, 1.2f);
	SingCopyLayer->addChild(singCopyTableview);
	*/
	isExistSingCopylayer = true;
}


void MissionAndTeam::initOffLineArenaLayer()
{
	if (isExistOffLineArenaLayer)
		return;

	auto  sprite_frame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao70.png");
	sprite_frame->setAnchorPoint(Vec2(0,0));
	sprite_frame->setPosition(Vec2(0,0));
	sprite_frame->setPreferredSize(Size(LAYER_SIZE_WIDTH,24));
	sprite_frame->setCapInsets(Rect(7,7,15,15));
	sprite_frame->setOpacity(160);
	OffLineArenaLayer->addChild(sprite_frame);
	//剩余时间：
	auto l_OffLineArena_remainTime = Label::createWithTTF(StringDataManager::getString("Arena_remainTime"),APP_FONT_NAME,16);
	l_OffLineArena_remainTime->setAnchorPoint(Vec2(0,.5f));
	l_OffLineArena_remainTime->setPosition(Vec2(3,sprite_frame->getContentSize().height/2));
	sprite_frame->addChild(l_OffLineArena_remainTime);
	//00:12:34
	auto l_OffLineArena_remainTimeValue = Label::createWithTTF("00:10:00",APP_FONT_NAME,16);
	l_OffLineArena_remainTimeValue->setAnchorPoint(Vec2(0,.5f));
	l_OffLineArena_remainTimeValue->setPosition(Vec2(l_OffLineArena_remainTime->getPositionX()+l_OffLineArena_remainTime->getContentSize().width,sprite_frame->getContentSize().height/2));
	l_OffLineArena_remainTimeValue->setTag(KTag_OffLineArena_Label_RemainTime);
	OffLineArenaLayer->addChild(l_OffLineArena_remainTimeValue);

	auto  sprite_infoFrame_left = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao70.png");
	sprite_infoFrame_left->setAnchorPoint(Vec2(0,0));
	sprite_infoFrame_left->setPosition(Vec2(0,25));
	sprite_infoFrame_left->setPreferredSize(Size(LAYER_SIZE_WIDTH/2-1,LAYER_SIZE_HEIGHT-25));
	sprite_infoFrame_left->setCapInsets(Rect(7,7,1,1));
	OffLineArenaLayer->addChild(sprite_infoFrame_left);
	auto  sprite_infoFrame_right = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao70.png");
	sprite_infoFrame_right->setAnchorPoint(Vec2(0,0));
	sprite_infoFrame_right->setPosition(Vec2(LAYER_SIZE_WIDTH/2,25));
	sprite_infoFrame_right->setPreferredSize(Size(LAYER_SIZE_WIDTH/2-1,LAYER_SIZE_HEIGHT-25));
	sprite_infoFrame_right->setCapInsets(Rect(7,7,1,1));
	OffLineArenaLayer->addChild(sprite_infoFrame_right);

	auto offLineArenaTableView_left = TableView::create(this,Size(LAYER_SIZE_WIDTH/2-1,LAYER_SIZE_HEIGHT-25));
	offLineArenaTableView_left->setDirection(TableView::Direction::VERTICAL);
	offLineArenaTableView_left->setAnchorPoint(Vec2(0,0));
	offLineArenaTableView_left->setPosition(Vec2(0,25));
	offLineArenaTableView_left->setDelegate(this);
	offLineArenaTableView_left->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//offLineArenaTableView_left->setPressedActionEnabled(true, 0.8f, 1.2f);
	offLineArenaTableView_left->setBounceable(false);
	offLineArenaTableView_left->setTag(kTag_offLineArena_tableView_left);
	OffLineArenaLayer->addChild(offLineArenaTableView_left);

	auto offLineArenaTableView_right = TableView::create(this,Size(LAYER_SIZE_WIDTH/2-1,LAYER_SIZE_HEIGHT-25));
	offLineArenaTableView_right->setDirection(TableView::Direction::VERTICAL);
	offLineArenaTableView_right->setAnchorPoint(Vec2(0,0));
	offLineArenaTableView_right->setPosition(Vec2(LAYER_SIZE_WIDTH/2,25));
	offLineArenaTableView_right->setDelegate(this);
	offLineArenaTableView_right->setVerticalFillOrder(TableView::VerticalFillOrder::TOP_DOWN);
	//offLineArenaTableView_right->setPressedActionEnabled(true, 0.8f, 1.2f);
	offLineArenaTableView_right->setBounceable(false);
	offLineArenaTableView_right->setTag(kTag_offLineArena_tableView_right);
	OffLineArenaLayer->addChild(offLineArenaTableView_right);
	
	isExistOffLineArenaLayer = true;
}


Size MissionAndTeam::tableCellSizeForIndex(TableView *table, unsigned int idx)
{
	if (getCurPresentPanel() == p_mission)
	{
		return Size(170, 61);
	}
	else if (getCurPresentPanel() == p_team)
	{
		return Size(170, 45);
	}
	else if (getCurPresentPanel() == p_fiveInstance)
	{
		return Size(170, 100);
	}
	else if (getCurPresentPanel() == p_familyFight)
	{
		return Size(170, 53);
	}
	else if (getCurPresentPanel() == p_singCopyChallenge)
	{
		return Size(130, 93);
	}
	else if (getCurPresentPanel() == p_offLineArena)
	{
		return Size(84,23);
	}
	else
	{
		return Size(170, 60);
	}
}

TableViewCell* MissionAndTeam::tableCellAtIndex(TableView *table, ssize_t  idx)
{
	__String *string = __String::createWithFormat("%d", idx);
	TableViewCell *cell = table->dequeueCell();   // this method must be called
	
	if (getCurPresentPanel() == p_mission)
	{
		//cell=MissionCell::Create(GameView::getInstance()->missionManager->MissionList_MainScene.at(idx));
		if (GameView::getInstance()->missionManager->MissionList_MainScene.size()>0)
		{
			TableViewCell *tempCell = table->dequeueCell();
			if (!tempCell)
			{
				tempCell = MissionCell::Create(GameView::getInstance()->missionManager->MissionList_MainScene.at(idx)); 
			}
			else
			{
				MissionCell * missionCell = dynamic_cast<MissionCell*>(tempCell);
				if (missionCell)
				{
					if (missionCell->getChildByTag(CCTUTORIALPARTICLETAG) != NULL)
					{
						missionCell->getChildByTag(CCTUTORIALPARTICLETAG)->removeFromParent();
					}
					missionCell->RefreshCell(GameView::getInstance()->missionManager->MissionList_MainScene.at(idx));
				}
			}

			//add by yangjun 2014.7.2
			if (idx == 0 && GameView::getInstance()->myplayer->getActiveRole()->level() <= K_GuideForMainMission_Level)
			{
				if ((GameView::getInstance()->missionManager->MissionList_MainScene.at(idx)->isNeedTeach && GameView::getInstance()->missionManager->MissionList_MainScene.at(idx)->isStateChanged) || (GameView::getInstance()->missionManager->isExistGuideForMmission))
				{
					addGuidePen();
				}
			}

			if (idx == 0 && GameView::getInstance()->myplayer->getActiveRole()->level() <= 20)
			{
				if (this->getChildByTag(CCTUTORIALINDICATORTAG) == NULL)
				{
					if (tempCell->getChildByTag(CCTUTORIALPARTICLETAG) == NULL)
					{
						if (this->getChildByTag(KTag_GuidePen) == NULL)
						{
							if (GameView::getInstance()->missionManager->MissionList_MainScene.at(idx)->isNeedTeach && GameView::getInstance()->missionManager->MissionList_MainScene.at(idx)->isStateChanged)
							{
								CCTutorialParticle * tutorialParticle = CCTutorialParticle::create("tuowei0.plist",100,55);
								tutorialParticle->setPosition(Vec2(75,0));
								tutorialParticle->setTag(CCTUTORIALPARTICLETAG);
								tempCell->addChild(tutorialParticle);
							}
						}
					}
				}
			}
			return tempCell;
		}
	}
	else if (getCurPresentPanel() == p_team)
	{
		cell=new TableViewCell();
		cell->autorelease();

		if (idx == selfTeamPlayerVector.size())
		{
			auto temp_layer = Layer::create();
			cell->addChild(temp_layer);
			//管理
			auto btn_manager = Button::create();
			btn_manager->setTouchEnabled(true);
			btn_manager->setPressedActionEnabled(true);
			btn_manager->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
			btn_manager->setScale9Enabled(true);
			btn_manager->setContentSize(Size(60,32));
			btn_manager->setCapInsets(Rect(18,18,1,1));
			btn_manager->setAnchorPoint(Vec2(0.5f,0.5f));
			btn_manager->setPosition(Vec2(47,20));
			btn_manager->addTouchEventListener(CC_CALLBACK_2(MissionAndTeam::ManageTeamEvent, this));
			temp_layer->addChild(btn_manager);

			auto l_manager = Label::createWithTTF(StringDataManager::getString("mainScene_missionAndTeam_manager"), APP_FONT_NAME, 18);

			l_manager->setAnchorPoint(Vec2(.5f,.5f));
			l_manager->setPosition(Vec2(0,0));
			btn_manager->addChild(l_manager);
			//退组
			auto btn_ExitTeam = Button::create();
			btn_ExitTeam->setTouchEnabled(true);
			btn_ExitTeam->setPressedActionEnabled(true);
			btn_ExitTeam->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
			btn_ExitTeam->setScale9Enabled(true);
			btn_ExitTeam->setContentSize(Size(60,32));
			btn_ExitTeam->setCapInsets(Rect(18,18,1,1));
			btn_ExitTeam->setAnchorPoint(Vec2(0.5f,0.5f));
			btn_ExitTeam->setPosition(Vec2(123,20));
			btn_ExitTeam->addTouchEventListener(CC_CALLBACK_2(MissionAndTeam::ExitTeamEvent, this));
			temp_layer->addChild(btn_ExitTeam);

			auto l_ExitTeam = Label::createWithTTF(StringDataManager::getString("mainScene_missionAndTeam_exitTeam"), APP_FONT_NAME, 18);
			l_ExitTeam->setAnchorPoint(Vec2(.5f,.5f));
			l_ExitTeam->setPosition(Vec2(0,0));
			btn_ExitTeam->addChild(l_ExitTeam);
		}
		else
		{
			CTeamMember * member_ =new CTeamMember();
			member_->CopyFrom(*selfTeamPlayerVector.at(idx));
			ShowTeamPlayer * team_= ShowTeamPlayer::create(member_,leaderId);
			team_->setAnchorPoint(Vec2(0,0));
			team_->setPosition(Vec2(17,0));
			team_->setTag(idx);
			cell->addChild(team_);
			delete member_;
		}
	}
	else if (getCurPresentPanel() == p_fiveInstance)
	{
		cell=new TableViewCell();
		cell->autorelease();

		auto l_win = Label::createWithBMFont("res_ui/font/ziti_3.fnt", StringDataManager::getString("fivePerson_fucPanel_win"));
		l_win->setAnchorPoint(Vec2(0.5f,1.0f));
		l_win->setPosition(Vec2(85,93));
		l_win->setScale(1.1f);
		cell->addChild(l_win);
		
		auto l_des = Label::createWithTTF(StringDataManager::getString("fivePerson_fucPanel_info"),APP_FONT_NAME,18,Size(160,0),TextHAlignment::LEFT,TextVAlignment::TOP);
		l_des->setAnchorPoint(Vec2(0.f,1.0f));
		l_des->setPosition(Vec2(5,70));
		l_des->setColor(Color3B(243,252,2)); 	//黄色
		cell->addChild(l_des);
		//Boss
		auto l_boss = Label::createWithTTF(StringDataManager::getString("fivePerson_boss"),APP_FONT_NAME,18);
		l_boss->setAnchorPoint(Vec2(0.f,1.0f));
		l_boss->setPosition(Vec2(5,l_des->getPosition().y-l_des->getContentSize().height-2));
		//l_boss->setColor(Color3B(255,51,51));   //红色
		l_boss->setColor(Color3B(243,252,2)); 	//黄色
		cell->addChild(l_boss);
		//boss value
		std::string str_all ;
		char str_curValue[20];
		sprintf(str_curValue,"%d",cur_boss_count);
		char str_maxValue[20];
		sprintf(str_maxValue,"%d",max_boss_count);
		str_all.append(str_curValue);
		str_all.append("/");
		str_all.append(str_maxValue);
		//auto l_boss_Value = Label::createWithTTF(str_all.c_str(),APP_FONT_NAME,18);
// 		CCLabel * l_boss_Value = CCLabel::create(str_all.c_str(),"res_ui/font/ziti_3.fnt");
// 		l_boss_Value->setAnchorPoint(Vec2(0.f,1.0f));
// 		l_boss_Value->setPosition(Vec2(l_boss->getPositionX()+l_boss->getContentSize().width+3,l_boss->getPosition().y));
// 		cell->addChild(l_boss_Value);
		auto l_boss_Value = CCLabelAtlas::create(str_all.c_str(),"res_ui/font/yellow_num.png", 10, 21, '/');
		l_boss_Value->setAnchorPoint(Vec2(0.f,1.0f));
		l_boss_Value->setPosition(Vec2(l_boss->getPositionX()+l_boss->getContentSize().width+3,l_boss->getPosition().y+1));
		cell->addChild(l_boss_Value);

		//Monster
		auto l_monster = Label::createWithTTF(StringDataManager::getString("fivePerson_monster"),APP_FONT_NAME,18);
		l_monster->setAnchorPoint(Vec2(0.f,1.0f));
		l_monster->setPosition(Vec2(l_boss->getPositionX(),l_boss->getPosition().y-l_boss->getContentSize().height-2));
		//l_monster->setColor(Color3B(255,51,51));   //红色
		l_monster->setColor(Color3B(243,252,2)); 	//黄色
		cell->addChild(l_monster);

		//monster value
		std::string str_all_monster ;
		char str_curMonsterValue[20];
		sprintf(str_curMonsterValue,"%d",cur_monster_count);
		char str_maxMonsterValue[20];
		sprintf(str_maxMonsterValue,"%d",max_monster_count);
		str_all_monster.append(str_curMonsterValue);
		str_all_monster.append("/");
		str_all_monster.append(str_maxMonsterValue);
// 		auto l_monster_Value = Label::createWithTTF(str_all_monster.c_str(),APP_FONT_NAME,18);
// 		l_monster_Value->setAnchorPoint(Vec2(0.f,1.0f));
// 		l_monster_Value->setPosition(Vec2(l_monster->getPositionX()+l_monster->getContentSize().width+3,l_monster->getPosition().y));
// 		cell->addChild(l_monster_Value);
		auto l_monster_Value = CCLabelAtlas::create(str_all_monster.c_str(),"res_ui/font/yellow_num.png", 10, 21, '/');
		l_monster_Value->setAnchorPoint(Vec2(0.f,1.0f));
		l_monster_Value->setPosition(Vec2(l_monster->getPositionX()+l_monster->getContentSize().width+3,l_monster->getPosition().y+1));
		cell->addChild(l_monster_Value);
	}
	else if (getCurPresentPanel() == p_familyFight)
	{
		cell=new TableViewCell();
		cell->autorelease();

		auto  cellFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao70.png");
		cellFrame->setAnchorPoint(Vec2(0,0));
		cellFrame->setPosition(Vec2(0,0));
		cellFrame->setPreferredSize(Size(170,52));
		cellFrame->setCapInsets(Rect(7,7,1,1));
		cellFrame->setOpacity(160);
		cell->addChild(cellFrame);

		auto  sp_frame = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/bossdi.png");
		sp_frame->setAnchorPoint(Vec2(0,0));
		sp_frame->setPosition(Vec2(16,5));
		sp_frame->setPreferredSize(Size(140,20));
		sp_frame->setCapInsets(Rect(12,9,1,1));
		cell->addChild(sp_frame);

		auto sp_blood = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/bosshp.png");
		sp_blood->setAnchorPoint(Vec2(0,0.5f));
		sp_blood->setPosition(Vec2(19,16));
		cell->addChild(sp_blood);

		auto l_bloodValue = Label::createWithTTF("",APP_FONT_NAME,14);
		l_bloodValue->setAnchorPoint(Vec2(0.5f,0.5f));
		l_bloodValue->setPosition(Vec2(86,16));
		cell->addChild(l_bloodValue);

		float scale = 1.0f;
		if (idx == 0)
		{
			if (FamilyFightData::getBossOwnerHpInfo().maxHp > 0)
			{
				scale = FamilyFightData::getBossOwnerHpInfo().curHp*1.0f/FamilyFightData::getBossOwnerHpInfo().maxHp;
			}
		}
		else if (idx == 1)
		{
			if (FamilyFightData::getBossCommonHpInfo().maxHp > 0)
			{
				scale = FamilyFightData::getBossCommonHpInfo().curHp*1.0f/FamilyFightData::getBossCommonHpInfo().maxHp;
			}
		}
		else if (idx == 2)
		{
			if (FamilyFightData::getBossEnemyHpInfo().maxHp > 0)
			{
				scale = FamilyFightData::getBossEnemyHpInfo().curHp*1.0f/FamilyFightData::getBossEnemyHpInfo().maxHp;
			}
		}

		int tempScale = scale*100;
		std::string str_scale;
		char s_scale[20];
		sprintf(s_scale,"%d",tempScale);
		str_scale.append(s_scale);
		str_scale.append("%");
		l_bloodValue->setString(str_scale.c_str());
		sp_blood->setTextureRect(Rect(0,0,sp_blood->getContentSize().width*scale,sp_blood->getContentSize().height));
		
		//Boss
		auto l_boss = Label::createWithTTF("",APP_FONT_NAME,16);
		l_boss->setAnchorPoint(Vec2(0.5f,0.5f));
		l_boss->setPosition(Vec2(83,35));
		//l_boss->setColor(Color3B(10,227,221));   //蓝色
		cell->addChild(l_boss);
		if (idx == 0)
		{
			l_boss->setString(StringDataManager::getString("FamilyFightUI_boss_owner"));
			l_boss->setColor(Color3B(31,252,2));             //绿色
		}
		else if (idx == 1)
		{
			l_boss->setString(StringDataManager::getString("FamilyFightUI_boss_common"));
			l_boss->setColor(Color3B(243,252,2)); 				//黄色
		}
		else if (idx == 2)
		{
			l_boss->setString(StringDataManager::getString("FamilyFightUI_boss_enemy"));
			l_boss->setColor(Color3B(252,2,2)); 				//红色
		}
	}
	else if (getCurPresentPanel() == p_singCopyChallenge)
	{
		cell=new TableViewCell();
		cell->autorelease();
	}
	else if (getCurPresentPanel() == p_offLineArena)   //竞技场
	{
		cell=new TableViewCell();
		cell->autorelease();

		if (table->getTag() == kTag_offLineArena_tableView_left)   //我方
		{
			auto frame = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di.png");
			frame->setAnchorPoint(Vec2(0,0));
			frame->setPosition(Vec2(1,0));
			cell->addChild(frame);

			auto sp_blood = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_mine.png");
			sp_blood->setAnchorPoint(Vec2(0,0.5f));
			sp_blood->setPosition(Vec2(23,9));
			cell->addChild(sp_blood);

			auto l_name= Label::createWithTTF("",APP_FONT_NAME,12);
			l_name->setAnchorPoint(Vec2(0.5f,0.5f));
			l_name->setPosition(Vec2(49,9));
			cell->addChild(l_name);

			float blood_scale = 1.0f;
			if (idx == 0)
			{
				frame->setPosition(Vec2(1,0));
				frame->setScaleX(1.0f);
				frame->setScaleY(1.0f);

				blood_scale = GameView::getInstance()->myplayer->getActiveRole()->hp()*1.0f/GameView::getInstance()->myplayer->getActiveRole()->maxhp();
				l_name->setString(StringDataManager::getString("WorldBossUI_mine"));
				sp_blood->setTextureRect(Rect(0,0,sp_blood->getContentSize().width*blood_scale,sp_blood->getContentSize().height));
				if (blood_scale <= 0.f)
				{
					auto texture = Director::getInstance()->getTextureCache()->addImage("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di2.png");
					frame->setTexture(texture);
				}
				else
				{
					auto texture = Director::getInstance()->getTextureCache()->addImage("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di.png");
					frame->setTexture(texture);
				}

				auto l_lv= Label::createWithTTF("",APP_FONT_NAME,12);
				l_lv->setAnchorPoint(Vec2(0.5f,0.5f));
				l_lv->setPosition(Vec2(12,9));
				cell->addChild(l_lv);

				char s_lv[10];
				sprintf(s_lv,"%d",GameView::getInstance()->myplayer->getActiveRole()->level());
				l_lv->setString(s_lv);
			}
			else
			{
				frame->setPosition(Vec2(20,1));
				frame->setScaleX(0.77f);
				frame->setScaleY(0.84f);

				GeneralInfoInArena * temp = generalsInArena_mine.at(idx-1);
				blood_scale = temp->cur_hp*1.0f/temp->max_hp;
				sp_blood->setTextureRect(Rect(0,0,sp_blood->getContentSize().width*blood_scale,sp_blood->getContentSize().height));

				auto sprite_star= Sprite::create(RecuriteActionItem::getStarPathByNum(temp->star).c_str());
				sprite_star->setAnchorPoint(Vec2(0.5f,0.5f));
				sprite_star->setPosition(Vec2(12,9));
				sprite_star->setScale(0.45f);
				cell->addChild(sprite_star);

				if (blood_scale <= 0.f || temp->isFinishedFight)
				{
					auto texture = Director::getInstance()->getTextureCache()->addImage("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di4.png");
					frame->setTexture(texture);
				}
				else
				{
					auto texture = Director::getInstance()->getTextureCache()->addImage("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di3.png");
					frame->setTexture(texture);
				}

				l_name->setString(StrUtils::unApplyColor(temp->name.c_str()).c_str());
			}	                                                          
		}
		else                                                                                          //敌方
		{
			if (idx == 0)
			{
				if (p_enemyInfoInArena->generalId > 0)
				{
					auto frame = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di.png");
					frame->setAnchorPoint(Vec2(0,0));
					frame->setPosition(Vec2(1,0));
					cell->addChild(frame);

					auto sp_blood = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_diren.png");
					sp_blood->setAnchorPoint(Vec2(0,0.5f));
					sp_blood->setPosition(Vec2(23,9));
					cell->addChild(sp_blood);

					auto l_name= Label::createWithTTF("",APP_FONT_NAME,12);
					l_name->setAnchorPoint(Vec2(0.5f,0.5f));
					l_name->setPosition(Vec2(49,9));
					cell->addChild(l_name);

					float blood_scale = 1.0f;                                                            
					l_name->setString(StringDataManager::getString("WorldBossUI_other"));

					blood_scale = p_enemyInfoInArena->cur_hp*1.0f/p_enemyInfoInArena->max_hp;

					auto l_lv= Label::createWithTTF("",APP_FONT_NAME,12);
					l_lv->setAnchorPoint(Vec2(0.5f,0.5f));
					l_lv->setPosition(Vec2(12,9));
					cell->addChild(l_lv);
					char s_lv[10];
					sprintf(s_lv,"%d",p_enemyInfoInArena->level);
					l_lv->setString(s_lv);

					sp_blood->setTextureRect(Rect(0,0,sp_blood->getContentSize().width*blood_scale,sp_blood->getContentSize().height));
					if (blood_scale <= 0.f)
					{
						auto texture = Director::getInstance()->getTextureCache()->addImage("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di2.png");
						frame->setTexture(texture);
					}
				}
			}
			else
			{
				auto frame = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di3.png");
				frame->setAnchorPoint(Vec2(0,0));
				frame->setPosition(Vec2(20,1));
				cell->addChild(frame);

				auto sp_blood = Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_diren.png");
				sp_blood->setAnchorPoint(Vec2(0,0.5f));
				sp_blood->setPosition(Vec2(23,9));
				cell->addChild(sp_blood);

				auto l_name= Label::createWithTTF("",APP_FONT_NAME,12);
				l_name->setAnchorPoint(Vec2(0.5f,0.5f));
				l_name->setPosition(Vec2(49,9));
				cell->addChild(l_name);

				float blood_scale = 1.0f;                                                            

				GeneralInfoInArena * temp = generalsInArena_other.at(idx-1);
				blood_scale = temp->cur_hp*1.0f/temp->max_hp;

				l_name->setString(StrUtils::unApplyColor(temp->name.c_str()).c_str());

				sp_blood->setTextureRect(Rect(0,0,sp_blood->getContentSize().width*blood_scale,sp_blood->getContentSize().height));
				if (blood_scale <= 0.f || temp->isFinishedFight)
				{
					auto texture = Director::getInstance()->getTextureCache()->addImage("gamescene_state/zhujiemian3/renwuduiwu/jingjichang_di4.png");
					frame->setTexture(texture);
				}

				auto sprite_star= Sprite::create(RecuriteActionItem::getStarPathByNum(temp->star).c_str());
				sprite_star->setAnchorPoint(Vec2(0.5f,0.5f));
				sprite_star->setPosition(Vec2(12,9));
				sprite_star->setScale(0.45f);
				cell->addChild(sprite_star);
			}
			
		}
	}

	return cell;
}

ssize_t MissionAndTeam::numberOfCellsInTableView(TableView *table)
{

	//return GameView::getInstance()->missionManager->MissionList_MainScene.size();

	if (getCurPresentPanel() == p_mission)
	{
		return GameView::getInstance()->missionManager->MissionList_MainScene.size();
	}
	else if (getCurPresentPanel() == p_team)
	{
		if(TeamLayer->getChildByName("teamInvite_btn_name"))
		{
			if (selfTeamPlayerVector.size() > 0 )
			{
				teamInvite_btn->setVisible(false);
			}else
			{
				teamInvite_btn->setVisible(true);
			}
		}

		if (selfTeamPlayerVector.size() > 0)
		{
			return selfTeamPlayerVector.size()+1;
		}
		else
		{
			return 0;
		}
	}
	else if (getCurPresentPanel() == p_fiveInstance)
	{
		return 1;
	}
	else if (getCurPresentPanel() == p_familyFight)
	{
		return 3;
	}
	else if (getCurPresentPanel() == p_singCopyChallenge)
	{
		return 0;
	}
	else if (getCurPresentPanel() == p_offLineArena)
	{
		if (table->getTag() == kTag_offLineArena_tableView_left)
		{
			return generalsInArena_mine.size()+1;
		}
		else if (table->getTag() == kTag_offLineArena_tableView_right)
		{
			return generalsInArena_other.size()+1;
		}
		else
		{
			return 0;
		}
	}
	else
	{
		return 0;
	}
}

void MissionAndTeam::tableCellTouched(TableView* table, TableViewCell* cell)
{
	if (GameUtils::millisecondNow() - m_nLastTouchTime <= 250.0f)
	{
		m_nLastTouchTime = GameUtils::millisecondNow();
		return;
	}

	m_nLastTouchTime = GameUtils::millisecondNow();
	 
	if (getCurPresentPanel() == p_mission)
	{
		//执行各种任务
		//GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*GameView::getInstance()->missionManager->MissionList_MainScene.at(cell->getIdx()));
		GameView::getInstance()->missionManager->curMissionInfo->CopyFrom(*GameView::getInstance()->missionManager->MissionList_MainScene.at(cell->getIdx()));
		GameView::getInstance()->missionManager->addMission(GameView::getInstance()->missionManager->curMissionInfo);

		if (cell->getIdx() == 0)
		{
			MissionManager::getInstance()->guideForMmission_time = 0.f;
			//add by yangjun 2014.7.2
			if (this->getChildByTag(KTag_GuidePen))
			{
				GameView::getInstance()->missionManager->MissionList_MainScene.at(cell->getIdx())->isNeedTeach = false;
				this->removeGuidePen();
			}
			

			if (cell->getChildByTag(CCTUTORIALPARTICLETAG))
			{
				GameView::getInstance()->missionManager->MissionList_MainScene.at(cell->getIdx())->isNeedTeach = false;
				cell->getChildByTag(CCTUTORIALPARTICLETAG)->removeFromParent();
			}

			if (m_nTutorialIndicatorIndex == 1)
			{
				Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
				if(sc != NULL)
					sc->endCommand(this);
			}
		}

		if (cell->getIdx() == 1)
		{
			if (cell->getChildByTag(CCTUTORIALPARTICLETAG))
			{
				GameView::getInstance()->missionManager->MissionList_MainScene.at(cell->getIdx())->isNeedTeach = false;
				cell->getChildByTag(CCTUTORIALPARTICLETAG)->removeFromParent();
			}

			if (m_nTutorialIndicatorIndex == 2)
			{
				Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
				if(sc != NULL)
					sc->endCommand(this);
			}
		}

// 		selectCellId = cell->getIdx();
// 		//取出上次选中状态，更新本次选中状态
// 		if(table->cellAtIndex(lastSelectCellId))
// 		{
// 			if (table->cellAtIndex(lastSelectCellId)->getChildByTag(MISSION_TABALEVIEW_HIGHLIGHT_TAG))
// 			{
// 				table->cellAtIndex(lastSelectCellId)->getChildByTag(MISSION_TABALEVIEW_HIGHLIGHT_TAG)->removeFromParent();
// 			}
// 		}
// 
// 		cocos2d::extension::Scale9Sprite* pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(7, 7, 1, 1) , "res_ui/kuang_0_on.png");
// 		pHighlightSpr->setPreferredSize(Size(145,40));
// 		pHighlightSpr->setAnchorPoint(Vec2::ZERO);
// 		pHighlightSpr->setTag(MISSION_TABALEVIEW_HIGHLIGHT_TAG);
// 		cell->addChild(pHighlightSpr);
// 
// 		lastSelectCellId = cell->getIdx();
	}
	else if (getCurPresentPanel() == p_team)
	{
		CCLOG("this.cell.id = %d",cell->getIdx());

		auto mainscene =(MainScene *)GameView::getInstance()->getMainUIScene();
		Size winsize =Director::getInstance()->getVisibleSize();
		long long selfId =GameView::getInstance()->myplayer->getRoleId();
		long long numIdx_ = selfTeamPlayerVector.at(cell->getIdx())->roleid();
		auto member_ =new CTeamMember();
		member_->CopyFrom(*selfTeamPlayerVector.at(cell->getIdx()));
		if (selfId != numIdx_)
		{
			TeamMemberListOperator * teamlistPlayer =TeamMemberListOperator::create(member_,leaderId);
			teamlistPlayer->setAnchorPoint(Vec2(0,0));
			teamlistPlayer->setPosition(Vec2(mainscene->getChildByTag(kTagMissionAndTeam)->getPositionX()+this->getContentSize().width,
											winsize.height/2+teamlistPlayer->getContentSize().height/2));
			teamlistPlayer->setLocalZOrder(10);
			mainscene->addChild(teamlistPlayer);
		}
		delete member_;

// 		float x = GameView::getInstance()->myplayer->MissionData[cell->getIdx()].coordinateX;
// 		float y = GameView::getInstance()->myplayer->MissionData[cell->getIdx()].coordinateY;
// 		Vec2 targetPos = Vec2(x,y);
// 		//移动命令
// 		MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
// 		cmd->targetPosition = targetPos;
// 		GameView::getInstance()->myplayer->setNextCommand(cmd, true);
	}
	else if (getCurPresentPanel() == p_fiveInstance)
	{
		FiveInstanceInfoEvent(NULL);
	}
	else if (getCurPresentPanel() == p_familyFight)
	{
		bool isNeedOpenHungUp = false;
		Vec2 targetPos = Vec2(0,0);
		if (cell->getIdx() == 0)              //寻路到我方BOSS
		{
			targetPos = Vec2(FamilyFightData::getBossOwnerPos().pos_x*1.0f,FamilyFightData::getBossOwnerPos().pos_y*1.0f);
			isNeedOpenHungUp = false;
		}
		else if (cell->getIdx() == 1)       //寻路到中立BOSS
		{
			targetPos = Vec2(FamilyFightData::getBossCommonPos().pos_x*1.0f,FamilyFightData::getBossCommonPos().pos_y*1.0f);
			isNeedOpenHungUp = true;
		}
		else if (cell->getIdx() == 2)        //寻路到敌方BOSS
		{
			targetPos = Vec2(FamilyFightData::getBossEnemyPos().pos_x*1.0f,FamilyFightData::getBossEnemyPos().pos_y*1.0f);
			isNeedOpenHungUp = true;
		}
		if (targetPos.x != 0 && targetPos.y != 0)
		{
			MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
			short tileX = GameView::getInstance()->getGameScene()->positionToTileX(targetPos.x);
			short tileY = GameView::getInstance()->getGameScene()->positionToTileY(targetPos.y);
			cmd->targetPosition = targetPos;
			cmd->method = MyPlayerCommandMove::method_searchpath;
			bool bSwitchCommandImmediately = true;
			if(GameView::getInstance()->myplayer->isAttacking())
				bSwitchCommandImmediately = false;
			GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	

			if (isNeedOpenHungUp)
			{
				m_Target_pos_inFamilyFight = targetPos;
				this->schedule(schedule_selector(MissionAndTeam::CheckDistanceInFamilyFight));
			}
		}
	}
}

void MissionAndTeam::tableCellHighlight(TableView* table, TableViewCell* cell)
{
	if (getCurPresentPanel() == p_mission || getCurPresentPanel() == p_fiveInstance ||  getCurPresentPanel() == p_familyFight )
	{
		cell->setContentSize(tableCellSizeForIndex(table, cell->getIdx()));

		// highlight sprite
		auto pHighlightSpr = cocos2d::extension::Scale9Sprite::create(Rect(11, 11, 1, 1) , "gamescene_state/zhujiemian3/renwuduiwu/light_1.png");
		pHighlightSpr->setPreferredSize(cell->getContentSize());
		pHighlightSpr->setAnchorPoint(Vec2::ZERO);
		pHighlightSpr->setTag(MISSION_TABALEVIEW_HIGHLIGHT_TAG);
		cell->addChild(pHighlightSpr);
	}
	else
	{
		
	}
	// please set cell's content size
	
}
void MissionAndTeam::tableCellUnhighlight(TableView* table, TableViewCell* cell)
{
	cell->removeChildByTag(MISSION_TABALEVIEW_HIGHLIGHT_TAG);
}

void MissionAndTeam::scrollViewDidScroll(cocos2d::extension::ScrollView* view )
{
	MissionManager::getInstance()->setMissionAndTeamOffSet(missionTableView->getContentOffset());
}

void MissionAndTeam::scrollViewDidZoom(cocos2d::extension::ScrollView* view )
{
}

void MissionAndTeam::popUpMission( Ref *pSender )
{
	ImageView_missionAndTeam_on_off->setRotation(180);

	Action * action1 = MoveTo::create(0.15f,Vec2(layer_leader->getPositionX()+LAYER_SIZE_WIDTH,layer_leader->getPositionY()));
	action1->setTag(KTag_MoveAction);
	layer_leader->runAction(action1);

// 	Action * action2 = MoveTo::create(0.15f,Vec2(LAYER_SIZE_WIDTH-missionAndTeam_on_off->getContentSize().width/2,missionAndTeam_on_off->getPosition().y));
// 	missionAndTeam_on_off->runAction(action2);

	isOn = true;
	this->setContentSize(Size(LAYER_SIZE_WIDTH,contentSizeHeight));

	refreshTutorial();
}

void MissionAndTeam::takeBackMission( Ref *pSender )
{
	ImageView_missionAndTeam_on_off->setRotation(0);

	Action * action1 = MoveTo::create(0.15f,Vec2(layer_leader->getPositionX()-LAYER_SIZE_WIDTH,layer_leader->getPositionY()));
	action1->setTag(KTag_MoveAction);
	layer_leader->runAction(action1);

// 	Action * action2 = MoveTo::create(0.15f,Vec2(missionAndTeam_on_off->getContentSize().width/2,missionAndTeam_on_off->getPosition().y));
// 	missionAndTeam_on_off->runAction(action2);

	isOn = false;
	this->setContentSize(Size(32,contentSizeHeight));

	refreshTutorial();
}


void MissionAndTeam::registerScriptCommand( int scriptId )
{
	mTutorialScriptInstanceId = scriptId;
}

void MissionAndTeam::addCCTutorialIndicator1( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	if (getCurPresentPanel() == p_mission)
	{
		if(GameView::getInstance()->missionManager->MissionList_MainScene.size()>0)
		{
			for(int i = 0;i<GameView::getInstance()->missionManager->MissionList_MainScene.size();++i)
			{
				MissionCell* cell = dynamic_cast<MissionCell*>(missionTableView->cellAtIndex(i));
				if (!cell)
					continue;

				if (cell->getChildByTag(CCTUTORIALPARTICLETAG) != NULL)
				{
					cell->getChildByTag(CCTUTORIALPARTICLETAG)->removeFromParent();
				}
			}
		}

		if (this->getChildByTag(KTag_GuidePen))
		{
			this->removeGuidePen();
		}

		Size winSize = Director::getInstance()->getWinSize();
		int _w = (winSize.width-800)/2;
		int _h = (winSize.height-480)/2;
		CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,170,60,true);
		tutorialIndicator->setDrawNodePos(Vec2(pos.x+170/2,pos.y-67));
		tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
		MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
		if (mainScene)
		{
			mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
		}
	}
	else
	{
	}

// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(Vec2(pos.x+147,pos.y+235));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);
}

void MissionAndTeam::addCCTutorialIndicator2( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	if (getCurPresentPanel() == p_mission)
	{
		if(GameView::getInstance()->missionManager->MissionList_MainScene.size()>0)
		{
			for(int i = 0;i<GameView::getInstance()->missionManager->MissionList_MainScene.size();++i)
			{
				MissionCell* cell = dynamic_cast<MissionCell*>(missionTableView->cellAtIndex(i));
				if (!cell)
					continue;

				if (cell->getChildByTag(CCTUTORIALPARTICLETAG) != NULL)
				{
					cell->getChildByTag(CCTUTORIALPARTICLETAG)->removeFromParent();
				}
			}
		}

		if (this->getChildByTag(KTag_GuidePen))
		{
			this->removeGuidePen();
		}
	}
	else
	{
	}
// 	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
// 	tutorialIndicator->setPosition(Vec2(pos.x+147,pos.y+175));
// 	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
// 	this->addChild(tutorialIndicator);

	Size winSize = Director::getInstance()->getWinSize();
	int _w = (winSize.width-800)/2;
	int _h = (winSize.height-480)/2;
	CCTeachingGuide * tutorialIndicator = CCTeachingGuide::create(content,pos,direction,170,60,true);
	tutorialIndicator->setDrawNodePos(Vec2(pos.x+170/2,pos.y-127));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		mainScene->getMainUITuturialLayer()->addChild(tutorialIndicator);
	}
}

void MissionAndTeam::addCCTutorialIndicator3( const char* content,Vec2 pos,CCTutorialIndicator::Direction direction )
{
	CCTutorialIndicator * tutorialIndicator = CCTutorialIndicator::create(content,pos,direction,true);
	tutorialIndicator->setPosition(Vec2(pos.x+105,pos.y+130));
	tutorialIndicator->setTag(CCTUTORIALINDICATORTAG);
	tutorialIndicator->addHighLightFrameAction(170,100,Vec2(-23,5));
	tutorialIndicator->addCenterAnm(170,100,Vec2(-23,5));
	this->addChild(tutorialIndicator);
}

void MissionAndTeam::removeCCTutorialIndicator()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
		tutorialIndicator->removeFromParent();

	MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (mainScene)
	{
		CCTeachingGuide* teachingGuide = dynamic_cast<CCTeachingGuide*>(mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG));
		if(teachingGuide != NULL)
			teachingGuide->removeFromParent();
	}
}

void MissionAndTeam::MissionAndTeamOnOff(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		//开启等级
		int openlevel = 0;
		std::map<int, int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(21);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[21];
		}

		if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
		{
			char s_des[200];
			const char* str_des = StringDataManager::getString("shrinkage_will_be_open");
			sprintf(s_des, str_des, openlevel);
			GameView::getInstance()->showAlertDialog(s_des);
			return;
		}

		if (layer_leader->getActionByTag(KTag_MoveAction))
			return;

		if (isOn)
		{
			// 		if (getCurPresentPanel() == p_mission)
			// 		{
			// 			if (MissionLayer->getChildByTag(KTag_MoveAction))
			// 				return;
			// 
			// 		}
			// 		else if (getCurPresentPanel() == p_team)
			// 		{
			// 			if (TeamLayer->getChildByTag(KTag_MoveAction))
			// 				return;
			// 
			// 		}

			takeBackMission(pSender);
		}
		else
		{
			// 		if (getCurPresentPanel() == p_mission)
			// 		{
			// 			if (MissionLayer->getChildByTag(KTag_MoveAction))
			// 				return;
			// 
			// 		}
			// 		else if (getCurPresentPanel() == p_team)
			// 		{
			// 			if (TeamLayer->getChildByTag(KTag_MoveAction))
			// 				return;
			// 		}

			popUpMission(pSender);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}

}

void MissionAndTeam::reloadMissionData()
{
	if (this->getCurPresentPanel() != p_mission)
		return;

	missionTableView->reloadData();
	this->refreshDownArrowStatus();

	this->removeCCTutorialIndicator();
}

void MissionAndTeam::reloadMissionDataWithoutChangeOffSet(Vec2 p_offset,Size p_contentSize)
{
	if (this->getCurPresentPanel() != p_mission)
		return;

	Vec2 _s = p_offset;
	int _h = p_contentSize.height + _s.y;
	missionTableView->reloadData();
	Vec2 temp = Vec2(_s.x,_h - missionTableView->getContentSize().height);
	missionTableView->setContentOffset(temp); 

	this->refreshDownArrowStatus();

	this->removeCCTutorialIndicator();
}

void MissionAndTeam::refreshTeamPlayer()
{
	if (isExistTeamLayer)
	{
		std::vector<CTeamMember *>::iterator iter;
		for (iter = selfTeamPlayerVector.begin(); iter != selfTeamPlayerVector.end(); ++iter)
		{
			delete * iter;
		}
		selfTeamPlayerVector.clear();
		long long selfId = GameView::getInstance()->myplayer->getRoleId();
		for (int i = 0; i< GameView::getInstance()->teamMemberVector.size(); i++)
		{
			if (selfId != GameView::getInstance()->teamMemberVector.at(i)->roleid())
			{
				CTeamMember * members_ = new CTeamMember();
				members_->CopyFrom( *GameView::getInstance()->teamMemberVector.at(i));
				selfTeamPlayerVector.push_back(members_);
			}
		}

		if (this->getCurPresentPanel() == p_team)
		{
			teamTableView->reloadData();
		}
	}
}

void MissionAndTeam::addGuidePen()
{
	if (this->getCurPresentPanel() != p_mission)
		return;

	if (isExistTutorialInMainScene())
	{
		return;
	}

	if (GameView::getInstance()->myplayer->getActiveRole()->level() <= K_GuideForMainMission_Level)
	{
		if (this->getChildByTag(CCTUTORIALINDICATORTAG) == NULL)
		{
			CCTutorialIndicator * guidePen = (CCTutorialIndicator*)this->getChildByTag(KTag_GuidePen);
			if (guidePen == NULL)
			{
				guidePen = CCTutorialIndicator::create("",Vec2(0,0),CCTutorialIndicator::Direction_LU,true,false);
				//guidePen->setPresentDesFrame(false);
				guidePen->setPosition(Vec2(105,150));
				guidePen->setTag(KTag_GuidePen);
				guidePen->addHighLightFrameAction(170,60,Vec2(-21,3),true);
				guidePen->addCenterAnm(170,60,Vec2(-21,3));
				this->addChild(guidePen);
			}
			else
			{
				guidePen->setVisible(true);
				guidePen->addHighLightFrameAction(170,60,Vec2(-21,3),true);
				guidePen->addCenterAnm(170,60,Vec2(-21,3));
			}

			GameView::getInstance()->missionManager->isExistGuideForMmission = true;
		}
	}
}

void MissionAndTeam::removeGuidePen()
{
	if (this->getChildByTag(KTag_GuidePen))
	{
		this->getChildByTag(KTag_GuidePen)->setVisible(false);
		GameView::getInstance()->missionManager->isExistGuideForMmission = false;
	}
}

bool MissionAndTeam::isExistTutorialInMainScene()
{
	if(GameView::getInstance())
	{
		if (GameView::getInstance()->getGameScene())
		{
			MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
			if (mainScene)
			{
				if (mainScene->getMainUITuturialLayer()->getChildByTag(CCTUTORIALINDICATORTAG))
				{
					return true;
				}
				//主角头像区域
				if (mainScene->getChildByTag(kTagMyHeadInfo))
				{
					if (mainScene->getChildByTag(kTagMyHeadInfo)->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						return true;
					}
				}
				//小任务面板区域
				if (this->getChildByTag(CCTUTORIALINDICATORTAG))
				{
					return true;
				}
				//底部菜单栏区域
				if (mainScene->getChildByTag(kTagHeadMenu))
				{
					if (mainScene->getChildByTag(kTagHeadMenu)->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						return true;
					}
				}
				//技能快捷栏区域
				ShortcutLayer * shortcutLayer = (ShortcutLayer*)mainScene->getChildByTag(kTagShortcutLayer);
				if (shortcutLayer)
				{
					if (shortcutLayer->u_teachLayer->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						return true;
					}
				}
				//右上角地图区域
				if (mainScene->getChildByTag(kTagGuideMapUI))
				{
					if (mainScene->getChildByTag(kTagGuideMapUI)->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						return true;
					}
				}
				//第一个武将出战，学习第一个技能面板
				if (mainScene->getChildByTag(kTagTeachRemind))
				{
					if (mainScene->getChildByTag(kTagTeachRemind)->getChildByTag(CCTUTORIALINDICATORTAG))
					{
						return true;
					}
				}
			}
		}
	}

	return false;
}

void MissionAndTeam::setPanelType( int stype )
{
	s_panelType = stype;
}

int MissionAndTeam::getPanelType()
{
	return s_panelType;
}

void MissionAndTeam::setCurPresentPanel( int cpPanel )
{
	s_curPresentPanel = cpPanel;
}

int MissionAndTeam::getCurPresentPanel()
{
	return s_curPresentPanel;
}

void MissionAndTeam::refreshFunctionBtn()
{
	switch(GameView::getInstance()->getMapInfo()->maptype())
	{
	case com::future::threekingdoms::server::transport::protocol::copy:        //副本
		{
			setPanelType(type_FiveInstanceAndTeam);
			Button_mission->setVisible(false);
			Button_fiveInstance->setVisible(true);
			Button_team->setVisible(true);
			Button_familyFight->setVisible(false);
			Button_singCopy->setVisible(false);
			Button_offLineArena->setVisible(false);

			if (getCurPresentPanel() != p_team && getCurPresentPanel() != p_fiveInstance)
			{
				setCurPresentPanel(p_fiveInstance);
				Button_mission->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				MissionLayer->setVisible(false);

				Button_fiveInstance->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				FiveInstanceLayer->setVisible(true);
				initFiveInstanceLayer();
				fiveInstanceTableView->reloadData();

				Button_familyFight->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FamilyFightLayer->setVisible(false);

				Button_team->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				TeamLayer->setVisible(false);

				Button_singCopy->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				SingCopyLayer->setVisible(false);

				Button_offLineArena->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				OffLineArenaLayer->setVisible(false);
			}
		}
		break;
	case com::future::threekingdoms::server::transport::protocol::guildFight:        //家族战
		{
			setPanelType(type_FamilyFightAndTeam);
			Button_mission->setVisible(false);
			Button_fiveInstance->setVisible(false);
			Button_team->setVisible(true);
			Button_familyFight->setVisible(true);
			Button_singCopy->setVisible(false);
			Button_offLineArena->setVisible(false);

			if (getCurPresentPanel() != p_team && getCurPresentPanel() != p_familyFight)
			{
				setCurPresentPanel(p_familyFight);
				Button_mission->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				MissionLayer->setVisible(false);

				Button_fiveInstance->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FiveInstanceLayer->setVisible(false);

				Button_familyFight->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				FamilyFightLayer->setVisible(true);
				initFamilyFightLayer();
				familyFightTableView->reloadData();

				Button_team->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				TeamLayer->setVisible(false);

				Button_singCopy->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				SingCopyLayer->setVisible(false);

				Button_offLineArena->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				OffLineArenaLayer->setVisible(false);
			}
		}
		break;
	case com::future::threekingdoms::server::transport::protocol::coliseum:  //离线竞技场
		{
			setPanelType(type_OfflineArenaOnly);
			Button_mission->setVisible(false);
			Button_fiveInstance->setVisible(false);
			Button_team->setVisible(true);
			Button_familyFight->setVisible(false);
			Button_singCopy->setVisible(false);
			Button_offLineArena->setVisible(true);

			if (getCurPresentPanel() != p_team && getCurPresentPanel() != p_offLineArena)
			{
				setCurPresentPanel(p_offLineArena);
				Button_mission->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				MissionLayer->setVisible(false);

				Button_fiveInstance->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FiveInstanceLayer->setVisible(false);

				Button_familyFight->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FamilyFightLayer->setVisible(false);

				Button_team->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				TeamLayer->setVisible(false);

				Button_singCopy->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				SingCopyLayer->setVisible(false);

				Button_offLineArena->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				OffLineArenaLayer->setVisible(true);
				initOffLineArenaLayer();
				auto offLineArenaTableView_left = (TableView*)OffLineArenaLayer->getChildByTag(kTag_offLineArena_tableView_left);
				if (offLineArenaTableView_left)
					offLineArenaTableView_left->reloadData();
				auto offLineArenaTableView_right = (TableView*)OffLineArenaLayer->getChildByTag(kTag_offLineArena_tableView_right);
				if (offLineArenaTableView_right)
					offLineArenaTableView_right->reloadData();
			}
		}break;
	case com::future::threekingdoms::server::transport::protocol::tower:  //singCopy
		{
			setPanelType(type_SingCopyChanllengeOnly);
			Button_mission->setVisible(false);
			Button_fiveInstance->setVisible(false);
			Button_team->setVisible(true);
			Button_familyFight->setVisible(false);
			Button_singCopy->setVisible(true);
			Button_offLineArena->setVisible(false);

			if (getCurPresentPanel() != p_team && getCurPresentPanel() != p_singCopyChallenge)
			{
				setCurPresentPanel(p_singCopyChallenge);
				Button_mission->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				MissionLayer->setVisible(false);

				Button_fiveInstance->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FiveInstanceLayer->setVisible(false);

				Button_familyFight->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FamilyFightLayer->setVisible(false);

				Button_team->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				TeamLayer->setVisible(false);

				Button_singCopy->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				SingCopyLayer->setVisible(true);
				initSingCopyLayer();
				//singCopyTableview->reloadData();

				Button_offLineArena->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				OffLineArenaLayer->setVisible(false);
			}
		}break;
	default:
		{
			setPanelType(type_MissionAndTeam);
			Button_mission->setVisible(true);
			Button_fiveInstance->setVisible(false);
			Button_team->setVisible(true);
			Button_familyFight->setVisible(false);
			Button_singCopy->setVisible(false);
			Button_offLineArena->setVisible(false);

			if (getCurPresentPanel() != p_team && getCurPresentPanel() != p_mission)
			{
				setCurPresentPanel(p_mission);
				Button_mission->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				MissionLayer->setVisible(true);
				initMissionLayer();
				missionTableView->reloadData();
				this->refreshDownArrowStatus();

				Button_team->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				TeamLayer->setVisible(false);

				Button_fiveInstance->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FiveInstanceLayer->setVisible(false);

				Button_familyFight->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_off.png","gamescene_state/zhujiemian3/renwuduiwu/button_off.png","");
				FamilyFightLayer->setVisible(false);

				Button_singCopy->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				SingCopyLayer->setVisible(false);

				Button_offLineArena->loadTextures("gamescene_state/zhujiemian3/renwuduiwu/button_on.png","gamescene_state/zhujiemian3/renwuduiwu/button_on.png","");
				OffLineArenaLayer->setVisible(false);
			}
			MainScene * mainScene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			if (mainScene_!= NULL)
			{
				SingCopyCountDownUI * singCopyCountDownUi_ = (SingCopyCountDownUI * )mainScene_->getChildByTag(kTagSingCopyCountDownUi);
				if (singCopyCountDownUi_ != NULL)
				{
					singCopyCountDownUi_->removeFromParentAndCleanup(true);
				}
			}
		}
	}

	refreshTutorial();
}

void MissionAndTeam::refreshDownArrowStatus()
{
	auto layer_hasNextMission= (Layer*)MissionLayer->getChildByTag(MISSIONLAYER_LAYER_HASNEXT_TAG);
	if (!layer_hasNextMission)
		return;

	auto image_hasNextMission = (ImageView*)layer_hasNextMission->getChildByName("image_hasNextMission");
	if (image_hasNextMission)
	{
		if (GameView::getInstance()->missionManager->MissionList_MainScene.size() > 3)
		{
			image_hasNextMission->setVisible(true);
		}
		else
		{
			image_hasNextMission->setVisible(false);
		}
	}
}

void MissionAndTeam::FiveInstanceInfoEvent( Ref * pSender )
{
	Size winSize = Director::getInstance()->getVisibleSize();

	if (m_nTutorialIndicatorIndex == 3)
	{
		CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
		if(tutorialIndicator != NULL)
		{
			Script* sc = ScriptManager::getInstance()->getScriptById(mTutorialScriptInstanceId);
			if(sc != NULL)
				sc->endCommand(this);
		}
	}
 		
	if(GameView::getInstance()->myplayer->isAction(ACT_ATTACK))
		return;

	if (GameView::getInstance()->myplayer->selectEnemy(winSize.width))
	{
		if (GameView::getInstance()->myplayer->hasLockedActor()) 
		{
			GameActor* lockedActor = GameView::getInstance()->myplayer->getLockedActor();
			//same map
			Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
			Vec2 cp_target = lockedActor->getWorldPosition();
			if (cp_target.x == 0 && cp_target.y == 0)
			{
				return;
			}

			MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
			// NOT reachable, so find the near one
			short tileX = GameView::getInstance()->getGameScene()->positionToTileX(cp_target.x);
			short tileY = GameView::getInstance()->getGameScene()->positionToTileY(cp_target.y);
			if(GameView::getInstance()->getGameScene()->isLimitOnGround(tileX, tileY))
			{
				cp_target = GameView::getInstance()->missionManager->getNearestReachablePos(cp_target);
			}
			cmd->targetPosition = cp_target;
			cmd->method = MyPlayerCommandMove::method_searchpath;
			bool bSwitchCommandImmediately = true;
			if(GameView::getInstance()->myplayer->isAttacking())
				bSwitchCommandImmediately = false;
			GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	

			DoThingWhenFinishedMove(cp_target);
		}
		
// 		MyPlayerAIConfig::setAutomaticSkill(1);
// 		MyPlayerAIConfig::enableRobot(true);
// 		GameView::getInstance()->myplayer->getMyPlayerAI()->start();
// 
// 		if (GameView::getInstance()->getGameScene())
// 		{
// 			if (GameView::getInstance()->getMainUIScene())
// 			{
// 				GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
// 				if (guide_)
// 				{
// 					if (guide_->getBtnHangUp())
// 					{
// 						guide_->getBtnHangUp()->loadTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
// 					}
// 				}
// 			}
// 		}
	}
	else
	{
		//req monster in instance
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1929);
	}
}


void MissionAndTeam::reloadToDefaultConfig( int instanceId )
{
	//显示副本进度（boss数量，普通怪数量）

	if (instanceId != -1)
	{
		if (FivePersonInstanceConfigData::s_fivePersonInstance.size()>0)
		{
			for(int i = 0;i<FivePersonInstanceConfigData::s_fivePersonInstance.size();++i)
			{
				if (instanceId == FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_id())
				{
					max_boss_count = FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_boss_count();
					max_monster_count = FivePersonInstanceConfigData::s_fivePersonInstance.at(i)->get_monster_count();
					cur_boss_count = 0;
					cur_monster_count = 0;
					break;
				}
			}
		}
	}

// 	//boss value
// 	std::string str_all ;
// 	char str_curValue[20];
// 	sprintf(str_curValue,"%d",cur_boss_count);
// 	char str_maxValue[20];
// 	sprintf(str_maxValue,"%d",max_boss_count);
// 	str_all.append(str_curValue);
// 	str_all.append("/");
// 	str_all.append(str_maxValue);
// 	l_boss_Value->setText(str_all.c_str());
// 	//monster value
// 	std::string str_all_monster ;
// 	char str_curMonsterValue[20];
// 	sprintf(str_curMonsterValue,"%d",cur_monster_count);
// 	char str_maxMonsterValue[20];
// 	sprintf(str_maxMonsterValue,"%d",max_monster_count);
// 	str_all_monster.append(str_curMonsterValue);
// 	str_all_monster.append("/");
// 	str_all_monster.append(str_maxMonsterValue);
// 	l_monster_Value->setText(str_all_monster.c_str());
// 	
// 	
	initFiveInstanceLayer();
	fiveInstanceTableView->reloadData();
}

void MissionAndTeam::updateInstanceSchedule( int bossCount,int monsterCount )
{
	cur_boss_count = bossCount;
	cur_monster_count = monsterCount;

	//boss value
// 	std::string str_all ;
// 	char str_curValue[20];
// 	sprintf(str_curValue,"%d",cur_boss_count);
// 	char str_maxValue[20];
// 	sprintf(str_maxValue,"%d",max_boss_count);
// 	str_all.append(str_curValue);
// 	str_all.append("/");
// 	str_all.append(str_maxValue);
// 	l_boss_Value->setText(str_all.c_str());

	//monster value
// 	std::string str_all_monster ;
// 	char str_curMonsterValue[20];
// 	sprintf(str_curMonsterValue,"%d",cur_monster_count);
// 	char str_maxMonsterValue[20];
// 	sprintf(str_maxMonsterValue,"%d",max_monster_count);
// 	str_all_monster.append(str_curMonsterValue);
// 	str_all_monster.append("/");
// 	str_all_monster.append(str_maxMonsterValue);
// 	l_monster_Value->setText(str_all_monster.c_str());
// 	
// 	
	if (isExistFiveInstanceLayer)
	{
		fiveInstanceTableView->reloadData();
	}
}


void MissionAndTeam::updateFamilyFightSchedult()
{
	familyFightTableView->reloadData();
}

void MissionAndTeam::DoThingWhenFinishedMove( Vec2 targetPos )
{
	m_Target_pos_inInstance = targetPos;
	this->schedule(schedule_selector(MissionAndTeam::CheckDistance));
}

void MissionAndTeam::CheckDistance(float dt)
{
	if (GameView::getInstance()->myplayer)
	{
		Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
		Vec2 cp_target = m_Target_pos_inInstance;
		//if (sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)) < 64)
		if (cp_role.getDistance(cp_target) < 64)
		{
			this->unschedule(schedule_selector(MissionAndTeam::CheckDistance));

			//began to kill monster
			GameView::getInstance()->myplayer->getMyPlayerAI()->startFight();

			if (GameView::getInstance()->getGameScene())
			{
				if (GameView::getInstance()->getMainUIScene())
				{
					GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
					if (guide_)
					{
						if (guide_->getBtnHangUp())
						{
							guide_->getBtnHangUp()->loadTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
						}
					}
				}
			}
		}
	}
}

void MissionAndTeam::updateSingCopyCellInfo( int level,int curMon,int allMon,int remMon,int useTime,int timeCount )
{
	if (m_singCopy_curMonsterNumber != curMon)
	{	
		Size winSize = Director::getInstance()->getVisibleSize();

		Layer  * layer_ = Layer::create();
		GameView::getInstance()->getMainUIScene()->addChild(layer_);

		std::string temp_ = "";
		if (curMon == allMon)
		{
			const char *strings_curLevelBmfont = StringDataManager::getString("singleCopy_curLevelLabel_monster_2");
			temp_.append(strings_curLevelBmfont);
		}else
		{
			const char *strings_ = StringDataManager::getString("singleCopy_curLevelLabel_monster_1");
			char levelStr[50];
			sprintf(levelStr,strings_,curMon);

			temp_.append(levelStr);
		}
		
		auto background_ =ImageView::create();
		background_->loadTexture("res_ui/zhezhao80.png");
		background_->setAnchorPoint(Vec2(0.5f,0.5f));
		background_->setScale9Enabled(true);
		background_->setContentSize(Size(winSize.width,60));
		background_->setPosition(Vec2(winSize.width/2,winSize.height*0.8f));
		layer_->addChild(background_);
		
		auto curLevel_BmFont = Label::createWithBMFont("res_ui/font/ziti_3.fnt", temp_);
		curLevel_BmFont->setAnchorPoint(Vec2(0.5f,0.5f));
		curLevel_BmFont->setPosition(Vec2(winSize.width/2,winSize.height*0.8f-2));
		curLevel_BmFont->setScale(8.0f);
		layer_->addChild(curLevel_BmFont);
		//curLevel_BmFont->runAction(ScaleTo::create(0.3f,2.5f));

		Action* spawn_action = Spawn::create(
			FadeOut::create(0.25f),
			ScaleTo::create(0.25f, 8.0f),
			NULL);

		Sequence * seq_Font = Sequence::create(
			ScaleTo::create(0.3f,2.5f),
			DelayTime::create(1.4f),
			spawn_action,
			RemoveSelf::create(),
			NULL);

		curLevel_BmFont->runAction(seq_Font);

		Sequence * seq_ = Sequence::create(
			Show::create(),
			DelayTime::create(2.0f),
			RemoveSelf::create(),
			NULL);
		layer_->runAction(seq_);
	}

	m_singCopy_curLevel = level;
	m_singCopy_curMonsterNumber = curMon;
	m_singCopy_monsterNumbers = allMon;
	m_singCopy_remainMonster = remMon;
	//m_singCopy_useTime = useTime;
	m_singCopy_timeCount = timeCount;

	const char *strings_curLevelBmfont = StringDataManager::getString("singleCopy_curLevelLabel");
	char levelStr[50];
	sprintf(levelStr,strings_curLevelBmfont,m_singCopy_curLevel);
	curLevel_BmFont->setString(levelStr);

	char monsterString[10];
	sprintf(monsterString,"%d",m_singCopy_curMonsterNumber);
	char allMonsterString[10];
	sprintf(allMonsterString,"%d",m_singCopy_monsterNumbers);
	std::string temp_monsterString ="";
	temp_monsterString.append(monsterString);
	temp_monsterString.append("/");
	temp_monsterString.append(allMonsterString);
	m_curLevelShowValue->setString(temp_monsterString.c_str());

	char remMonsterString[50];
	sprintf(remMonsterString,"%d",m_singCopy_remainMonster);
	label_monsterValue->setString(remMonsterString);

	//RecuriteActionItem::timeFormatToString(m_singCopy_useTime/1000).c_str();
	allTimeShowValue->setString(RecuriteActionItem::timeFormatToString(m_singCopy_timeCount/1000).c_str());
	//singCopyTableview->updateCellAtIndex(0);
	//singCopyTableview->reloadData();
}

void MissionAndTeam::starRunUpdate()
{
	setSingCopy_useTime(0);
	schedule(schedule_selector(MissionAndTeam::updateSingCopyCurUseTime),1.0f);
}


void MissionAndTeam::updateSingCopyCurUseTime( float delta )
{
	m_singCopy_useTime++;
	m_useTimeValue->setString(RecuriteActionItem::timeFormatToString(m_singCopy_useTime).c_str());
}

void MissionAndTeam::setSingCopy_useTime( int time_ )
{
	m_singCopy_useTime = time_;
}

int MissionAndTeam::getSingCopy_useTime()
{
	return m_singCopy_useTime;
}

void MissionAndTeam::updateFamilyFightRemainTime(float delta )
{
	//in familyFight
	if (this->getPanelType() == MissionAndTeam::type_FamilyFightAndTeam)
	{
		long long tempdata = FamilyFightData::getRemainTime();
		if (tempdata >= 0)
		{
			tempdata -= 1000 ;
			FamilyFightData::setRemainTime(tempdata);

			auto l_ff_remaintimeValue = (Label*)FamilyFightLayer->getChildByTag(KTag_FamilyFight_Label_RemainTime);
			if (l_ff_remaintimeValue)
			{
				l_ff_remaintimeValue->setString(RecuriteActionItem::timeFormatToString(FamilyFightData::getRemainTime()/1000).c_str());
			}
		}
	}
}

void MissionAndTeam::CheckDistanceInFamilyFight( float dt )
{
	if (GameView::getInstance()->myplayer)
	{
		Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
		Vec2 cp_target = m_Target_pos_inFamilyFight;
		//if (sqrt(pow((cp_role.x-cp_target.x),2)+pow((cp_role.y-cp_target.y),2)) < 64)
		if (cp_role.getDistance(cp_target) < 64)
		{
			this->unschedule(schedule_selector(MissionAndTeam::CheckDistanceInFamilyFight));

			//began to kill monster
			GameView::getInstance()->myplayer->getMyPlayerAI()->startFight();

			if (GameView::getInstance()->getGameScene())
			{
				if (GameView::getInstance()->getMainUIScene())
				{
					GuideMap * guide_ =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
					if (guide_)
					{
						if (guide_->getBtnHangUp())
						{
							guide_->getBtnHangUp()->loadTextures("gamescene_state/zhujiemian3/tubiao/off_hook.png","gamescene_state/zhujiemian3/tubiao/off_hook.png","");
						}
					}
				}
			}
		}
	}
}

void MissionAndTeam::updateOffLineArenaRemainTime( float delta )
{
	//in offLineArena
	if (this->getPanelType() == MissionAndTeam::type_OfflineArenaOnly)
	{
		if (m_nRemainTime_Arena >= 0)
		{
			m_nRemainTime_Arena-- ;

			auto l_ofl_remaintimeValue = (Label*)OffLineArenaLayer->getChildByTag(KTag_OffLineArena_Label_RemainTime);
			if (l_ofl_remaintimeValue)
			{
				l_ofl_remaintimeValue->setString(RecuriteActionItem::timeFormatToString(m_nRemainTime_Arena).c_str());
			}
		}
		else
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(3021);
		}
		
	}
}

void MissionAndTeam::setArenaDataToDefault()
{
	m_nRemainTime_Arena = 600;
	p_enemyInfoInArena->generalId = -1;
	std::vector<GeneralInfoInArena*>::iterator iter_mine;
	for (iter_mine = generalsInArena_mine.begin(); iter_mine != generalsInArena_mine.end(); ++iter_mine)
	{
		delete *iter_mine;
	}
	generalsInArena_mine.clear();

	std::vector<GeneralInfoInArena*>::iterator iter_other;
	for (iter_other = generalsInArena_other.begin(); iter_other != generalsInArena_other.end(); ++iter_other)
	{
		delete *iter_other;
	}
	generalsInArena_other.clear();
}

void MissionAndTeam::callBackTeamInvite(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Size winsize = Director::getInstance()->getVisibleSize();

		MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		FriendUi * friendLayer = (FriendUi*)mainscene->getChildByTag(kTagFriendUi);

		if (friendLayer == NULL)
		{
			FriendUi * friendui = FriendUi::create();
			friendui->setIgnoreAnchorPointForPosition(false);
			friendui->setAnchorPoint(Vec2(0.5f, 0.5f));
			friendui->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			GameView::getInstance()->getMainUIScene()->addChild(friendui, 0, kTagFriendUi);
			mainscene->remindGetFriendPhypower();

			friendui->friendType->setDefaultPanelByIndex(3);
			friendui->callBackChangeFriendType(friendui->friendType);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

TableView * MissionAndTeam::getLeftOffLineArenaTableView()
{
	auto temp = (TableView*)OffLineArenaLayer->getChildByTag(kTag_offLineArena_tableView_left);
	if (temp)
		return temp;

	return NULL;
}

TableView * MissionAndTeam::getRightOffLineArenaTableView()
{
	auto temp = (TableView*)OffLineArenaLayer->getChildByTag(kTag_offLineArena_tableView_right);
	if (temp)
		return temp;

	return NULL;
}

void MissionAndTeam::refreshTutorial()
{
	CCTutorialIndicator* tutorialIndicator = dynamic_cast<CCTutorialIndicator*>(this->getChildByTag(CCTUTORIALINDICATORTAG));
	if(tutorialIndicator != NULL)
	{
		if (!isOn)
		{
			tutorialIndicator->setVisible(false);
		}
		else
		{
			if (m_nTutorialIndicatorIndex == 1 || m_nTutorialIndicatorIndex == 2)  //任务的教学
			{
				if(getCurPresentPanel() != p_mission)
				{
					tutorialIndicator->setVisible(false);
				}
				else
				{
					tutorialIndicator->setVisible(true);
				}
			}
			else if (m_nTutorialIndicatorIndex == 3 ) //地图面板的教学
			{
				if(getCurPresentPanel() != p_fiveInstance)
				{
					tutorialIndicator->setVisible(false);
				}
				else
				{
					tutorialIndicator->setVisible(true);
				}
			}
		}
	}

	//指引任务的笔
	if (GameView::getInstance()->myplayer->getActiveRole()->level() <= K_GuideForMainMission_Level)
	{
		CCTutorialIndicator * guidePen = (CCTutorialIndicator*)this->getChildByTag(KTag_GuidePen);
		if (guidePen)
		{
			if (!isOn)
			{
				guidePen->setVisible(false);
			}
			else
			{
				if(getCurPresentPanel() != p_mission)
				{
					guidePen->setVisible(false);
				}
				else
				{
					guidePen->setVisible(true);
					guidePen->addHighLightFrameAction(170,60,Vec2(-21,3),true);
					guidePen->addCenterAnm(170,60,Vec2(-21,3));

					GameView::getInstance()->missionManager->isExistGuideForMmission = true;
				}
			}
		}
	}
}

void MissionAndTeam::ManageTeamEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		Size winsize = Director::getInstance()->getVisibleSize();

		MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
		FriendUi * friendLayer = (FriendUi*)mainscene->getChildByTag(kTagFriendUi);

		if (friendLayer == NULL)
		{
			FriendUi * friendui = FriendUi::create();
			friendui->setIgnoreAnchorPointForPosition(false);
			friendui->setAnchorPoint(Vec2(0.5f, 0.5f));
			friendui->setPosition(Vec2(winsize.width / 2, winsize.height / 2));
			GameView::getInstance()->getMainUIScene()->addChild(friendui, 0, kTagFriendUi);
			mainscene->remindGetFriendPhypower();

			friendui->friendType->setDefaultPanelByIndex(3);
			friendui->callBackChangeFriendType(friendui->friendType);
		}
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void MissionAndTeam::ExitTeamEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		int teamWork = 3;
		int playerId = 0;
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1406, (void *)teamWork, (void *)playerId);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void MissionAndTeam::callBackSingCopy(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		this->setStateIsRobot(true);
		this->schedule(CC_SCHEDULE_SELECTOR(MissionAndTeam::updateSingCopyCheckMonst), 1.0f);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}


}

void MissionAndTeam::updateSingCopyCheckMonst( float delta )
{
	Size winSize = Director::getInstance()->getVisibleSize();

	if(GameView::getInstance()->myplayer->isAction(ACT_ATTACK))
		return;

	if (GameView::getInstance()->myplayer->selectEnemy(winSize.width))
	{
		if (GameView::getInstance()->myplayer->hasLockedActor()) 
		{
			GameActor* lockedActor = GameView::getInstance()->myplayer->getLockedActor();
			//same map
			Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
			Vec2 cp_target = lockedActor->getWorldPosition();
			if (cp_target.x == 0 && cp_target.y == 0)
			{
				return;
			}

			MyPlayerCommandMove* cmd = new MyPlayerCommandMove();
			// NOT reachable, so find the near one
			short tileX = GameView::getInstance()->getGameScene()->positionToTileX(cp_target.x);
			short tileY = GameView::getInstance()->getGameScene()->positionToTileY(cp_target.y);
			if(GameView::getInstance()->getGameScene()->isLimitOnGround(tileX, tileY))
			{
				cp_target = GameView::getInstance()->missionManager->getNearestReachablePos(cp_target);
			}
			cmd->targetPosition = cp_target;
			cmd->method = MyPlayerCommandMove::method_searchpath;
			bool bSwitchCommandImmediately = true;
			if(GameView::getInstance()->myplayer->isAttacking())
				bSwitchCommandImmediately = false;
			GameView::getInstance()->myplayer->setNextCommand(cmd, bSwitchCommandImmediately);	

			DoThingWhenFinishedMove(cp_target);
		}
	}
	else
	{
		//req monster in instance
		GameMessageProcessor::sharedMsgProcessor()->sendReq(1929);
	}
}

void MissionAndTeam::setStateIsRobot( bool value_ )
{
	m_state_start = value_;
}

bool MissionAndTeam::getStateIsRobot()
{
	return m_state_start;
}



