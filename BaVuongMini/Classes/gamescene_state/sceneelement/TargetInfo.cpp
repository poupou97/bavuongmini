#include "TargetInfo.h"
#include "../role/BaseFighter.h"
#include "../../ui/Friend_ui/FriendInfoList.h"
#include "../GameSceneState.h"
#include "GameView.h"
#include "ExStatusItem.h"
#include "../../ui/extensions/UITab.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../ui/Chat_ui/ChatUI.h"
#include "../../ui/Chat_ui/AddPrivateUi.h"
#include "../../ui/extensions/RichTextInput.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../role/MyPlayer.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../role/GameActor.h"
#include "../role/BasePlayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/Mail_ui/MailUI.h"
#include "AppMacros.h"
#include "../MainScene.h"
#include "../exstatus/ExStatus.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"

#define SPEED_ACTION_BLOOD 70.0f

TargetInfo::TargetInfo()
	:isTabTargetOn(false)
	,m_preScale(0.0f)
	,m_timeDelay(0.0f)
	,m_nowScale(0.0f)
{
}


TargetInfo::~TargetInfo()
{
}

TargetInfo* TargetInfo::create(BaseFighter * baseFighter)
{
	auto targetInfo = new TargetInfo();
	if (targetInfo && targetInfo->init(baseFighter))
	{
		targetInfo->autorelease();
		return targetInfo;
	}
	CC_SAFE_DELETE(targetInfo);
	return NULL;
}

bool TargetInfo::init(BaseFighter * baseFighter)
{
	if (UIScene::init())
	{
		Size visibleSize = Director::getInstance()->getVisibleSize();
		Vec2 origin = Director::getInstance()->getVisibleOrigin();

		baseFighterId = baseFighter->getRoleId();
		
		auto activeRole = GameView::getInstance()->myplayer->getActiveRole();
		//baseFightName= activeRole->rolebase().name();
		baseFightName= baseFighter->getActorName();
		
		if (baseFighter->getType() == GameActor::type_player)
		{
			coutryId = baseFighter->getActiveRole()->playerbaseinfo().country();
			viplevel_ = baseFighter->getActiveRole()->playerbaseinfo().viplevel();
			level_ = baseFighter->getActiveRole()->level();
			pressionId = BasePlayer::getProfessionIdxByName(baseFighter->getActiveRole()->profession());
		}
		
		auto Button_Frame = Button::create();
		Button_Frame->loadTextures("gamescene_state/zhujiemian3/zhujuetouxiang/xuetiaodi.png","gamescene_state/zhujiemian3/zhujuetouxiang/xuetiaodi.png","");
		Button_Frame->setName("Button_Frame");
		Button_Frame->setTouchEnabled(true);
		//Button_Frame->setPressedActionEnabled(true,0.9f,1.1f);
		Button_Frame->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_Frame->setPosition(Vec2(199-Button_Frame->getContentSize().width/2,74-Button_Frame->getContentSize().height/2));
		Button_Frame->addTouchEventListener(CC_CALLBACK_2(TargetInfo::ButtonFrameEvent, this));
		m_pLayer->addChild(Button_Frame);
		//头像
		Button_headFrame = Button::create();
		Button_headFrame->setTouchEnabled(true);
		Button_headFrame->setPressedActionEnabled(true);
		//Button_headFrame->loadTextures(BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str(), BasePlayer::getHeadPathByProfession(GameView::getInstance()->myplayer->getProfession()).c_str(), "");
		Button_headFrame->loadTextures("gamescene_state/zhujiemian3/zhujuetouxiang/touxiang_di.png","gamescene_state/zhujiemian3/zhujuetouxiang/touxiang_di.png", "");
		Button_headFrame->setAnchorPoint(Vec2(0.5f,0.5f));
		Button_headFrame->setPosition(Vec2(Button_headFrame->getContentSize().width/2,Button_headFrame->getContentSize().height/2));
		Button_headFrame->addTouchEventListener(CC_CALLBACK_2(TargetInfo::ButtonTargetEvent, this));
		m_pLayer->addChild(Button_headFrame);

		imageView_head = ImageView::create();
		imageView_head->loadTexture("");
		imageView_head->setAnchorPoint(Vec2(0.5f,0.5f));
		imageView_head->setPosition(Vec2(1,6));
		Button_headFrame->addChild(imageView_head);
		if(baseFighter->getType() == GameActor::type_monster)
		{
			Button_headFrame->setTouchEnabled(false);

			imageView_head->setScale(1.0f);
			imageView_head->loadTexture("res_ui/generals46X45/monster.png");
		}
		else if (baseFighter->getType() == GameActor::type_player)
		{
			Button_headFrame->setTouchEnabled(true);

			auto basePlayer = dynamic_cast<BasePlayer*>(baseFighter);
			if (baseFighter)
			{
				imageView_head->loadTexture(BasePlayer::getHeadPathByProfession(basePlayer->getProfession()).c_str());
				imageView_head->setScale(1.0f);
			}
		}
		else if (baseFighter->getType() == GameActor::type_pet)
		{
			Button_headFrame->setTouchEnabled(false);
			if (baseFighter->getActiveRole()->generalbaseinfo().has_templateid())
			{
				std::map<int,CGeneralBaseMsg*>::const_iterator cIter;
				cIter = GeneralsConfigData::s_generalsBaseMsgData.find(baseFighter->getActiveRole()->generalbaseinfo().templateid());
				if (cIter == GeneralsConfigData::s_generalsBaseMsgData.end()) // 没找到就是指向END了  
				{
				}
				else
				{
					auto tempInfo = GeneralsConfigData::s_generalsBaseMsgData[baseFighter->getActiveRole()->generalbaseinfo().templateid()];
					imageView_head->setScale(1.2f);
					std::string str_generalHeadIcon = "res_ui/generals46X45/";
					str_generalHeadIcon.append(tempInfo->get_head_photo());
					str_generalHeadIcon.append(".png");
					imageView_head->loadTexture(str_generalHeadIcon.c_str());
				}
			}
		}
		else 
		{
			Button_headFrame->setTouchEnabled(false);

			imageView_head->setScale(1.0f);
		}
		//level
		char s_level [10];
		sprintf(s_level,"%d",baseFighter->getActiveRole()->level());
		Label_targetLv = Label::createWithTTF(s_level, APP_FONT_NAME, 12);
		Label_targetLv->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_targetLv->setPosition(Vec2(14-Button_headFrame->getContentSize().width/2,11-Button_headFrame->getContentSize().height/2));
		Label_targetLv->enableOutline(Color4B::BLACK, 2.0f);
		Button_headFrame->addChild(Label_targetLv);

		auto Label_space = Label::createWithTTF("/", APP_FONT_NAME, 12);
		Label_space->setAnchorPoint(Vec2::ZERO);
		Label_space->setPosition(Vec2(7,-8));
		Label_space->setLocalZOrder(100);
		Label_space->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_space);

		
		char s_maxhp[10];
		sprintf(s_maxhp,"%d",baseFighter->getActiveRole()->maxhp());
		Label_targetAllBlood = Label::createWithTTF(s_maxhp, APP_FONT_NAME, 12);
		Label_targetAllBlood->setAnchorPoint(Vec2(0,0));
		Label_targetAllBlood->setPosition(Vec2(Label_space->getPosition().x+Label_space->getContentSize().width+3,Label_space->getPosition().y));
		Label_targetAllBlood->setLocalZOrder(100);
		Label_targetAllBlood->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_targetAllBlood);

		char s_hp[10];
		sprintf(s_hp,"%d",baseFighter->getActiveRole()->hp());
		Label_targetCurBlood = Label::createWithTTF(s_hp, APP_FONT_NAME, 12);

		Label_targetCurBlood->setAnchorPoint(Vec2(1.0f,0));
		Label_targetCurBlood->setPosition(Vec2(Label_space->getPosition().x-3,Label_space->getPosition().y));
		Label_targetCurBlood->setLocalZOrder(100);
		Label_targetCurBlood->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_targetCurBlood);

		auto Label_space_magic = Label::createWithTTF("/", APP_FONT_NAME, 12);
		Label_space_magic->setAnchorPoint(Vec2::ZERO);
		Label_space_magic->setPosition(Vec2(7,-22));
		Label_space_magic->setLocalZOrder(100);
		Label_space_magic->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_space_magic);
		
		char s_maxmp[10];
		sprintf(s_maxmp,"%d",baseFighter->getActiveRole()->maxmp());
		Label_targetAllMagic = Label::createWithTTF(s_maxmp, APP_FONT_NAME, 12);
		Label_targetAllMagic->setAnchorPoint(Vec2(0,0));
		Label_targetAllMagic->setPosition(Vec2(Label_space_magic->getPosition().x+Label_space_magic->getContentSize().width+3,Label_space_magic->getPosition().y));
		Label_targetAllMagic->setLocalZOrder(100);
		Label_targetAllMagic->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_targetAllMagic);
		
		char s_mp[10];
		sprintf(s_mp,"%d",baseFighter->getActiveRole()->mp());
		Label_targetCurMagic = Label::createWithTTF(s_mp, APP_FONT_NAME, 12);
		Label_targetCurMagic->setAnchorPoint(Vec2(1.0f,0));
		Label_targetCurMagic->setPosition(Vec2(Label_space->getPosition().x-3,24));
		Label_targetCurMagic->setPosition(Vec2(Label_space_magic->getPosition().x-3,Label_space_magic->getPosition().y));
		Label_targetCurMagic->setLocalZOrder(100);
		Label_targetCurMagic->enableOutline(Color4B::BLACK, 2.0f);
		Button_Frame->addChild(Label_targetCurMagic);
		
		Label_targetName = CCRichLabel::createWithString(baseFighter->getActorName().c_str(),Size(300,20),this,NULL,0);
		Label_targetName->setAnchorPoint(Vec2(0.5f,0.5f));
		Label_targetName->setPosition(Vec2(133,62));
		Label_targetName->setPosition(Vec2(7,15));
		Label_targetName->setScale(0.6f);
		Button_Frame->addChild(Label_targetName);

		ImageView_targetBlood = ImageView::create();
		ImageView_targetBlood->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/red_xuetiao_2.png");
		ImageView_targetBlood->setAnchorPoint(Vec2(0,0.5f));
		ImageView_targetBlood->setPosition(Vec2(-49,0));
		char * a = (char *)Label_targetCurBlood->getString().c_str();
		char * b = (char *)Label_targetAllBlood->getString().c_str();
		float _a = atof(a);
		float _b = atof(b);
		float blood_scale = _a/_b;
		if (blood_scale > 1.0f)
			blood_scale = 1.0f;

		ImageView_targetBloodGray = ImageView::create();
		ImageView_targetBloodGray->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/red_xuetiao_3.png");
		ImageView_targetBloodGray->setAnchorPoint(Vec2(0,0.5f));
		ImageView_targetBloodGray->setPosition(Vec2(-49,0));


		ImageView_targetBloodGray->setTextureRect(Rect(0,0,ImageView_targetBloodGray->getContentSize().width*blood_scale,ImageView_targetBloodGray->getContentSize().height));
		Button_Frame->addChild(ImageView_targetBloodGray);

		ImageView_targetBlood->setTextureRect(Rect(0,0,ImageView_targetBlood->getContentSize().width*blood_scale,ImageView_targetBlood->getContentSize().height));
		Button_Frame->addChild(ImageView_targetBlood);

		ImageView_targetMagic = ImageView::create();
		ImageView_targetMagic->loadTexture("gamescene_state/zhujiemian3/zhujuetouxiang/blue_xuetiao_2.png");
		ImageView_targetMagic->setAnchorPoint(Vec2(0,0.5f));
		ImageView_targetMagic->setPosition(Vec2(-47,-14));
		char * a_mp = (char *)Label_targetCurMagic->getString().c_str();
		char * b_mp = (char *)Label_targetAllMagic->getString().c_str();
		float _a_mp = atof(a_mp);
		float _b_mp = atof(b_mp);
		float magic_scale = _a_mp/_b_mp;
		if (magic_scale > 1.0f)
			magic_scale = 1.0f;

		ImageView_targetMagic->setTextureRect(Rect(0,0,ImageView_targetMagic->getContentSize().width*magic_scale,ImageView_targetMagic->getContentSize().height));
		Button_Frame->addChild(ImageView_targetMagic);



		//创建目标的buff
		for (int i = 0;i<(int)baseFighter->getExStatusVector().size();++i)
		{
			if (i>4)
				break;

			auto tempExstatus = baseFighter->getExStatusVector().at(i);

			if (tempExstatus->type()== 37 ||tempExstatus->type() == 38 ||tempExstatus->type() == 39)
				continue;

			auto exStatusItem = ExStatusItem::create(tempExstatus);
			exStatusItem->setScale(0.5f);
			exStatusItem->setAnchorPoint(Vec2(0,0));
			exStatusItem->setPosition(Vec2(80+22*i,2));
			exStatusItem->setTag(1000+i);
			addChild(exStatusItem);
		}


		this->setIgnoreAnchorPointForPosition(false);
		this->setAnchorPoint(Vec2(0,0));
		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(199,74));

		this->scheduleUpdate();

		return true;
	}
	return false;
}

void TargetInfo::onEnter()
{
	UIScene::onEnter();
}

void TargetInfo::onExit()
{
	UIScene::onExit();
}

bool TargetInfo::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	Vec2 location = pTouch->getLocation();

	Vec2 pos = this->getPosition();
	Size size = this->getContentSize();
	Vec2 anchorPoint = this->getAnchorPointInPoints();
	pos = pos - anchorPoint;
	Rect rect(pos.x, pos.y, size.width, size.height);
	if(rect.containsPoint(location))
	{
		//如果是角色（不是怪） 弹出选项框
// 		FriendInfoList * friendInfoList = FriendInfoList::create(1);
// 		friendInfoList->setPosition(this->getPosition());
// 		GameView::getInstance()->getMainUIScene()->addChild(friendInfoList);
		return true;
	}
	//this->removeFromParent();
	return false;
}

void TargetInfo::onTouchMoved( Touch *pTouch, Event *pEvent )
{

}

void TargetInfo::onTouchEnded( Touch *pTouch, Event *pEvent )
{

}

void TargetInfo::onTouchCancelled( Touch *pTouch, Event *pEvent )
{

}

void TargetInfo::ReloadTargetData( BaseFighter * baseFighter, bool bInitTargetInfo)
{
	char s_maxhp[20];
	sprintf(s_maxhp,"%d",baseFighter->getActiveRole()->maxhp());
	Label_targetAllBlood->setString(s_maxhp);

	char s_hp[20];
	sprintf(s_hp,"%d",baseFighter->getActiveRole()->hp());
	Label_targetCurBlood->setString(s_hp);

	int nPreHp = baseFighter->getActiveRole()->lasthp();

	char * a = (char *)Label_targetCurBlood->getString().c_str();
	char * b = (char *)Label_targetAllBlood->getString().c_str();

	float _a = atof(a);
	float _b = atof(b);

	float blood_scale = _a/_b;
	float blood_preScale = nPreHp / _b;

	if (blood_scale > 1.0f)
	{
		blood_scale = 1.0f;
	}
		
	if (blood_preScale > 1.0f)
	{
		blood_preScale = 1.0f;
	}

	if (0.0f == blood_preScale)
	{
		m_preScale = 1.0f;
		m_nowScale = blood_scale;
		m_timeDelay = 0.0f;

		ImageView_targetBlood->setTextureRect(Rect(0,0,
			ImageView_targetBlood->getContentSize().width * blood_scale,
			ImageView_targetBlood->getContentSize().height));

	}
	else if (blood_preScale == blood_scale)
	{
		m_preScale = blood_preScale;
		m_nowScale = blood_scale;
		m_timeDelay = 0.0f;

		ImageView_targetBlood->setTextureRect(Rect(0,0,
			ImageView_targetBlood->getContentSize().width * blood_scale,
			ImageView_targetBlood->getContentSize().height));
	}
	else
	{
		if (bInitTargetInfo)
		{
			m_preScale = blood_preScale;
			m_nowScale = blood_scale;
			m_timeDelay = 0.0f;

			ImageView_targetBlood->setTextureRect(Rect(0,0,
				ImageView_targetBlood->getContentSize().width * blood_scale,
				ImageView_targetBlood->getContentSize().height));
		}
		else
		{
			if (m_preScale == blood_preScale && m_nowScale == blood_scale)
			{

			}
			else
			{
				m_preScale = blood_preScale;
				m_nowScale = blood_scale;
				m_timeDelay += (blood_preScale - blood_scale) * 100.0f / SPEED_ACTION_BLOOD;
			}

			ImageView_targetBlood->setTextureRect(Rect(0,0,
				ImageView_targetBlood->getContentSize().width * blood_scale,
				ImageView_targetBlood->getContentSize().height));
		}
	}

	char s_maxmp[20];
	sprintf(s_maxmp,"%d",baseFighter->getActiveRole()->maxmp());
	Label_targetAllMagic->setString(s_maxmp);

	char s_mp[20];
	sprintf(s_mp,"%d",baseFighter->getActiveRole()->mp());
	Label_targetCurMagic->setString(s_mp);

	char * a_mp = (char *)Label_targetCurMagic->getString().c_str();
	char * b_mp = (char *)Label_targetAllMagic->getString().c_str();
	float _a_mp = atof(a_mp);
	float _b_mp = atof(b_mp);
	float magic_scale = _a_mp/_b_mp;
	if (magic_scale > 1.0f)
		magic_scale = 1.0f;

	ImageView_targetMagic->setTextureRect(Rect(0,0,ImageView_targetMagic->getContentSize().width*magic_scale,ImageView_targetMagic->getContentSize().height));

	char s_level[10];
	sprintf(s_level,"%d",baseFighter->getActiveRole()->level());
	Label_targetLv->setString(s_level);
}

void TargetInfo::ReloadBuffData()
{
	for (int i = 0;i<10;++i)
	{
		if (this->getChildByTag(1000+i) != NULL)
		{
			auto exStatusItem = (ExStatusItem *)this->getChildByTag(1000+i);
			exStatusItem->removeFromParent();
		}
		else break;
	}
	
	auto curBaseFighter = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(baseFighterId));
	if(curBaseFighter == NULL)
		return;
	for (int i = 0;i<(int)curBaseFighter->getExStatusVector().size();++i)
	{
		if (i>4)
			break;

		auto tempExstatus = curBaseFighter->getExStatusVector().at(i);
		auto exStatusItem = ExStatusItem::create(tempExstatus);
		exStatusItem->setScale(0.5f);
		exStatusItem->setAnchorPoint(Vec2(0,0));
		exStatusItem->setPosition(Vec2(80+22*i,2));
		exStatusItem->setTag(1000+i);
		addChild(exStatusItem);
	}
}


void TargetInfo::ButtonTargetEvent(Ref *pSender, Widget::TouchEventType type)
{

	switch (type)
	{
	case Widget::TouchEventType::BEGAN:
		break;

	case Widget::TouchEventType::MOVED:
		break;

	case Widget::TouchEventType::ENDED:
	{
		if (GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::coliseum)
			return;

		int selfLevel_ = GameView::getInstance()->myplayer->getActiveRole()->level();
		int openlevel = 0;
		std::map<int, int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(20);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[20];
		}

		if (selfLevel_ < openlevel)
		{
			char openLevelStr[100];
			const char *strings = StringDataManager::getString("chatAndmailIsOpenInFiveLevel");
			sprintf(openLevelStr, strings, openlevel);
			GameView::getInstance()->showAlertDialog(openLevelStr);
			return;
		}

		auto obj = (Button *)pSender;
		Size winSize = Director::getInstance()->getVisibleSize();

		auto list = (TargetInfoList*)GameView::getInstance()->getMainUIScene()->getChildByTag(ktagMainSceneTargetInfo);
		if (list != NULL)
		{
			list->removeFromParentAndCleanup(true);
		}
		auto tarlist = TargetInfoList::create(baseFighterId, baseFightName, coutryId, viplevel_, level_, pressionId);
		tarlist->setIgnoreAnchorPointForPosition(false);
		tarlist->setAnchorPoint(Vec2(0, 1));
		tarlist->setTag(ktagMainSceneTargetInfo);
		tarlist->setPosition(Vec2(280, winSize.height - this->getContentSize().height + tarlist->getContentSize().height));
		GameView::getInstance()->getMainUIScene()->addChild(tarlist);
	}
	break;

	case Widget::TouchEventType::CANCELED:
		break;

	default:
		break;
	}



}

void TargetInfo::ButtonFrameEvent(Ref *pSender, Widget::TouchEventType type)
{

}

void TargetInfo::initNewTargetInfo( BaseFighter * baseFighter)
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	baseFighterId = baseFighter->getRoleId();
	baseFightName= baseFighter->getActorName();

	if (baseFighter->getType() == GameActor::type_player)
	{
		coutryId = baseFighter->getActiveRole()->playerbaseinfo().country();
		viplevel_ = baseFighter->getActiveRole()->playerbaseinfo().viplevel();
		level_ = baseFighter->getActiveRole()->level();
		pressionId = BasePlayer::getProfessionIdxByName(baseFighter->getActiveRole()->profession());
	}
	//头像
	if(baseFighter->getType() == GameActor::type_monster)
	{
		Button_headFrame->setTouchEnabled(false);

		imageView_head->setScale(1.0f);
		imageView_head->loadTexture("res_ui/generals46X45/monster.png");
	}
	else if (baseFighter->getType() == GameActor::type_player)
	{
		Button_headFrame->setTouchEnabled(true);

		auto basePlayer = dynamic_cast<BasePlayer*>(baseFighter);
		if (baseFighter)
		{
			imageView_head->loadTexture(BasePlayer::getHeadPathByProfession(basePlayer->getProfession()).c_str());
			imageView_head->setScale(1.0f);
		}
	}
	else if (baseFighter->getType() == GameActor::type_pet)
	{
		Button_headFrame->setTouchEnabled(false);

		if (baseFighter->getActiveRole()->generalbaseinfo().has_templateid())
		{
			std::map<int,CGeneralBaseMsg*>::const_iterator cIter;
			cIter = GeneralsConfigData::s_generalsBaseMsgData.find(baseFighter->getActiveRole()->generalbaseinfo().templateid());
			if (cIter == GeneralsConfigData::s_generalsBaseMsgData.end()) // 没找到就是指向END了  
			{
			}
			else
			{
				auto tempInfo = GeneralsConfigData::s_generalsBaseMsgData[baseFighter->getActiveRole()->generalbaseinfo().templateid()];
				imageView_head->setScale(1.2f);
				std::string str_generalHeadIcon = "res_ui/generals46X45/";
				str_generalHeadIcon.append(tempInfo->get_head_photo());
				str_generalHeadIcon.append(".png");
				imageView_head->loadTexture(str_generalHeadIcon.c_str());
			}
		}
	}
	else 
	{
		Button_headFrame->setTouchEnabled(false);
		imageView_head->setScale(1.0f);
	}

	Label_targetName->setString(baseFighter->getActorName().c_str());

	ReloadTargetData(baseFighter, true);
	ReloadBuffData();
}

void TargetInfo::update( float delta )
{
	if (0.0f >= m_timeDelay)
	{
		ImageView_targetBloodGray->setTextureRect(Rect(0,0,
			ImageView_targetBloodGray->getContentSize().width * m_nowScale,
			ImageView_targetBloodGray->getContentSize().height));
	}
	else
	{
		ImageView_targetBloodGray->setTextureRect(Rect(0,0,
			ImageView_targetBloodGray->getContentSize().width * (m_nowScale + m_timeDelay * SPEED_ACTION_BLOOD * 0.01f),
			ImageView_targetBloodGray->getContentSize().height));

		m_timeDelay -= delta;
	}
}

/////////////////////////////
TargetInfoList::TargetInfoList()
{

}

TargetInfoList::~TargetInfoList()
{

}

TargetInfoList* TargetInfoList::create(long long targetId ,std::string targetName,int countryid,int viplv,int level,int pressionId)
{
	auto list =new TargetInfoList();
	if (list&& list->init(targetId,targetName,countryid,viplv,level,pressionId))
	{
		list->autorelease();
		return list;
	}
	CC_SAFE_DELETE(list);
	return NULL;
}

bool TargetInfoList::init(long long targetId ,std::string targetName,int countryid,int viplv,int level,int pressionId)
{
	if (UIScene::init())
	{
		winSize = Director::getInstance()->getVisibleSize();

		objId = targetId;
		objName = targetName;
		objCountryId = countryid;
		objViplv = viplv;
		objLv = level;
		ObjPressionId = pressionId;

		const char *str_addfriend = StringDataManager::getString("friend_addfriend");
		char *addfriend_left=const_cast<char*>(str_addfriend);	

		const char *str_check = StringDataManager::getString("friend_check");
		char *check_left=const_cast<char*>(str_check);

		const char *str_team = StringDataManager::getString("friend_team");
		char *team_left=const_cast<char*>(str_team);

		const char *str_mail = StringDataManager::getString("friend_mail");
		char *mail_left=const_cast<char*>(str_mail);

		const char *str_black = StringDataManager::getString("friend_black");
		char *black_left=const_cast<char*>(str_black);

		const char *str_private = StringDataManager::getString("chatui_private");
		char *private_left=const_cast<char*>(str_private);

		const char *str_track = StringDataManager::getString("friend_track");
		char *track_left=const_cast<char*>(str_track);

		const char *str_fire = StringDataManager::getString("friend_of_fire");
		char *fire_left=const_cast<char*>(str_track);
		char * normalImage = "res_ui/new_button_5.png";
		char * selelctImage = "res_ui/new_button_5.png";
		char * disabledImage = "";
		char * highLightImgae = "res_ui/new_button_5.png";

		char * allNames[] = {private_left,check_left,team_left,addfriend_left,mail_left,black_left};
		auto Tab_target = UITab::createWithText(6,normalImage,selelctImage,disabledImage,allNames,VERTICAL,-2);
		Tab_target->setAnchorPoint(Vec2(0,0));
		Tab_target->setPosition(Vec2(0,0));
		Tab_target->setHighLightImage(highLightImgae);
		Tab_target->setDefaultPanelByIndex(0);
		Tab_target->addIndexChangedEvent(this,coco_indexchangedselector(TargetInfoList::TabTargetIndexChangedEvent));
		Tab_target->setPressedActionEnabled(true);
		m_pLayer->addChild(Tab_target);

		////this->setTouchEnabled(true);
		////this->setTouchMode(Touch::DispatchMode::ONE_BY_ONE);
		this->setContentSize(Size(110,240));
		return true;
	}
	return false;
}

void TargetInfoList::onEnter()
{
	UIScene::onEnter();
}

void TargetInfoList::onExit()
{
	UIScene::onExit();
}
void TargetInfoList::TabTargetIndexChangedEvent( Ref * obj )
{
	int index = ((UITab*)obj)->getCurrentIndex();
	switch(index)
	{
	case 0 :
		{

			auto mainscene_ = (MainScene *)GameView::getInstance()->getMainUIScene();
			mainscene_->addPrivateChatUi(objId,objName,objCountryId,objViplv,objLv,ObjPressionId);
			/*
			const char *str_private = StringDataManager::getString("chatui_private");
			char *private_left=const_cast<char*>(str_private);
			
			ChatUI * chatui_ =ChatUI::create();
			chatui_->setIgnoreAnchorPointForPosition(false);
			chatui_->setAnchorPoint(Vec2(0.5f,0.5f));
			chatui_->setPosition(Vec2(winSize.width/2,winSize.height/2));
			chatui_->setTag(kTabChat);
			GameView::getInstance()->getMainUIScene()->addChild(chatui_);
			chatui_->selectChannelId = 0;
			chatui_->labelchanel->setString(private_left);
			chatui_->channelLabel->setDefaultPanelByIndex(6);
			chatui_->addPrivate();

			AddPrivateUi * privateui=(AddPrivateUi *)chatui_->layer_3->getChildByTag(PRIVATEUI);
			privateui->textBox_private->onTextFieldInsertText(NULL,objName.c_str(),30);
			*/
		}break;
	case 1 :
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1142,(void *)objId);
		}break;
	case 2 :
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1401,(void *)objId);
		}break;
	case 3 :
		{
			TargetInfo::FriendStruct friend1={0,0,objId,objName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	case 4 :
		{
			auto mail_ui = MailUI::create();
			mail_ui->setIgnoreAnchorPointForPosition(false);
			mail_ui->setAnchorPoint(Vec2(0.5f,0.5f));
			mail_ui->setPosition(Vec2(winSize.width/2,winSize.height/2));
			GameView::getInstance()->getMainUIScene()->addChild(mail_ui,0,kTagMailUi);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2001, (void *)mail_ui->curMailPage);
			mail_ui->channelLabel->setDefaultPanelByIndex(1);
			mail_ui->callBackChangeMail(mail_ui->channelLabel);

			mail_ui->friendName->onTextFieldInsertText(NULL,objName.c_str(),30);
		}break;
	case 5:
		{
			TargetInfo::FriendStruct friend1={0,1,objId,objName};
			GameMessageProcessor::sharedMsgProcessor()->sendReq(2200,&friend1);
		}break;
	}
	this->removeFromParentAndCleanup(true);
}

bool TargetInfoList::onTouchBegan( Touch *pTouch, Event *pEvent )
{
	return resignFirstResponder(pTouch,this,true,false);
}

