#ifndef _SCENEELEMENT_EXSTATUSITEM_H_
#define _SCENEELEMENT_EXSTATUSITEM_H_

#include "cocos2d.h"
#include "../../ui/extensions/UIScene.h"

class ExStatus;

class ExStatusItem : public UIScene
{
public:
	ExStatusItem();
	~ExStatusItem();

	static ExStatusItem * create(ExStatus* exstatus);
	bool init(ExStatus* exstatus);

	ProgressTimer *mProgressTimer_skill;
	ActionInterval* action_progress_from_to;
	float mCDTime_skill;

private:
	void skillCoolDownCallBack(Node* node);
	ExStatus * curExstatus;

	virtual void update(float dt);
};

#endif

