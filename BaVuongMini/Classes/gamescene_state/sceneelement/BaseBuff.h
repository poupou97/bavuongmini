

#ifndef _GAMESCENESTATE_BASEBUFF_H_
#define _GAMESCENESTATE_BASEBUFF_H_

#include "cocos-ext.h"
#include "ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "ui\CocosGUI.h"

USING_NS_CC;
USING_NS_CC_EXT;

class BaseBuff : public ui::Widget
{
public:
	BaseBuff();
	~BaseBuff();
	static BaseBuff* create();
	bool init();

	void BuffInfo(Ref *pSender, Widget::TouchEventType type);
};
#endif

