#include "BuffCell.h"
#include "../exstatus/ExStatus.h"
#include "ExStatusItem.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"

BuffCell::BuffCell():
m_nRemainTime(0)
{
}


BuffCell::~BuffCell()
{
}


BuffCell* BuffCell::create(ExStatus * exstatus)
{
	auto buffCell = new BuffCell();
	if (buffCell && buffCell->init(exstatus))
	{
		buffCell->autorelease();
		return buffCell;
	}
	CC_SAFE_DELETE(buffCell);
	return NULL;
}

bool BuffCell::init(ExStatus * exstatus)
{
	if (TableViewCell::init())
	{
		curExstatus = exstatus;

		auto exStatusFromDB = ExStatusConfigData::s_exstatusData[exstatus->type()];
		CCAssert(exStatusFromDB != NULL, "exStatusFromDB should not be nil");

		exStatusItem = ExStatusItem::create(exstatus);
		exStatusItem->setIgnoreAnchorPointForPosition(false);
		exStatusItem->setAnchorPoint(Vec2(0,1.0f));
		exStatusItem->setPosition(Vec2(7,58));
		addChild(exStatusItem);

// 		Sprite_icon = Sprite::create("gamescene_state/buff/jiantou_up.png");
// 		Sprite_icon->setAnchorPoint(Vec2(0, 0));
// 		Sprite_icon->setPosition(Vec2(38,0));
// 		addChild(Sprite_icon);

		num = Label::createWithTTF("99",APP_FONT_NAME,10);
		num->setAnchorPoint(Vec2(0,0.0f));
		num->setPosition(Vec2(18,0));
		Color3B _color = Color3B(240,0,0);
		num->setColor(_color);
		//this->addChild(num);

		name = Label::createWithTTF(exStatusFromDB->getName().c_str(),APP_FONT_NAME,18);
		name->setAnchorPoint(Vec2(0,1.0f));
		name->setPosition(Vec2(40,60));
		if(exStatusFromDB->getFunctionType() == 1)  //���
		{
			name->setColor(Color3B(0,255,14));                  //���ɫ
		}   
		else                                                                     //���
		{
			name->setColor(Color3B(255,34,34));               //��ɫ
		}
		this->addChild(name);

		auto l_des = Label::createWithTTF(exStatusFromDB->getDescription().c_str(),APP_FONT_NAME,16,Size(165,0),TextHAlignment::LEFT,TextVAlignment::TOP);
		l_des->setAnchorPoint(Vec2(0,1.0f));
		l_des->setPosition(Vec2(40,40));
		if(exStatusFromDB->getFunctionType() == 1)  //���
		{
			l_des->setColor(Color3B(149,239,68));              //���ɫ
		}   
		else                                                                     //���
		{
			l_des->setColor(Color3B(254,98,105));              //��ɫ
		}
		this->addChild(l_des);

		time = Label::createWithTTF(ConvertToDateFormat(m_nRemainTime).c_str(),APP_FONT_NAME,20);
		time->setAnchorPoint(Vec2(0,1.0f));
		time->setPosition(Vec2(122,60));
		time->setColor(Color3B(255,255,172));
		this->addChild(time);
		time->setVisible(false);

		if (exstatus->has_duration() && exstatus->duration() != 0)
		{
			m_nRemainTime = exstatus->remaintime()/1000;
			time->setString(ConvertToDateFormat(m_nRemainTime).c_str());
			time->setVisible(true);
		}

		this->schedule(schedule_selector(BuffCell::update),1.0f);
		return true;
	}
	return false;
}

std::string BuffCell::ConvertToDateFormat( int t )
{
	std::string second = "s";
	std::string minitue = "m";
	std::string hours = "h";

	std::string timeString = "";
	if (t<60)
	{
		char str_m[10];
		sprintf(str_m,"%d",t);
		timeString = str_m;
		timeString.append(second);

		return timeString;
	}
	else
	{
		if (t<60*60)
		{
			int int_m = t/60;
			int int_s = t%60;

			char str_m[10];
			sprintf(str_m,"%d",int_m);
			timeString = str_m;
			timeString.append(minitue);

			char str_s[10];
			sprintf(str_s,"%d",int_s);
			timeString.append(str_s);
			timeString.append(second);

			return timeString;
		}
		else
		{
			int int_h = t/60/60;
			int int_m = (t%60)/60;
			int int_s = (t%60%60)/60;

			char str_h [10];
			sprintf(str_h,"%d",int_h);
			timeString = str_h;
			timeString.append(hours);

			char str_m[10];
			sprintf(str_m,"%d",int_m);
			timeString.append(str_m);
			timeString.append(minitue);

			char str_s[10];
			sprintf(str_s,"%d",int_s);
			timeString.append(str_s);
			timeString.append(second);

			return timeString;
		}
	}
}

void BuffCell::update( float delta )
{
	if (time->isVisible())
	{
		m_nRemainTime--;
		if (m_nRemainTime <= 0)
		{
			m_nRemainTime = 0;
		}

		time->setString(ConvertToDateFormat(m_nRemainTime).c_str());
	}
}

// 
// void BuffCell::update( float dt )
// {
// 	float remaScale = exStatusItem->mProgressTimer_skill->getPercentage()/100;
// 	float rematime  = curExstatus->duration()*remaScale;
// 	char * s = new char[10];
// 	sprintf(s,"%f",rematime/1000);
// 	time->setString(s);
// 	//delete []s;
// }

