#include "MissionCell.h"
#include "../../messageclient/element/MissionInfo.h"
#include "GameView.h"
#include "../role/MyPlayerOwnedCommand.h"
#include "../role/MyPlayer.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../messageclient/element/MissionInfo.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../../utils/StrUtils.h"
#include "../../gamescene_state/SimpleEffectManager.h"

MissionCell::MissionCell()
: curMissionInfo(NULL)
{
}


MissionCell::~MissionCell()
{
	CC_SAFE_DELETE(curMissionInfo);
}

MissionCell* MissionCell::Create(MissionInfo * missionInfo)
{
	auto missionCell = new MissionCell();
	if (missionCell && missionCell->init(missionInfo))
	{
		missionCell->autorelease();
		return missionCell;
	}
	CC_SAFE_DELETE(missionCell);
	return NULL;
}

bool MissionCell::init(MissionInfo * missionInfo)
{
	if (TableViewCell::init())
	{
		curMissionInfo = new MissionInfo();
		curMissionInfo->CopyFrom(*missionInfo);

// 		auto  cellFrame = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/zhezhao60.png"); 
// 		cellFrame->setPreferredSize(Size(155,58));
// 		cellFrame->setAnchorPoint(Vec2(0.0f, 1.0f));
// 		cellFrame->setPosition(Vec2(0,60));
// 		this->addChild(cellFrame);
		//auto  cellFrame = cocos2d::extension::Scale9Sprite::create("gamescene_state/zhujiemian3/renwuduiwu/renwu_di2.png");
		//cellFrame->setPreferredSize(Size(170,60));
		//cellFrame->setCapInsets(Rect(12,12,1,1));
		auto  cellFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao70.png");
		cellFrame->setPreferredSize(Size(170,60));
		cellFrame->setCapInsets(Rect(7,7,1,1));
		cellFrame->setOpacity(160);
		cellFrame->setAnchorPoint(Vec2(0,1.0f));
		cellFrame->setPosition(Vec2(0,60));
		this->addChild(cellFrame);


		//mission type
		if (curMissionInfo->missionpackagetype() == 1)
		{
			const char *str_mission_zhu = StringDataManager::getString("mission_zhu");
			missionType = Label::createWithTTF(str_mission_zhu,APP_FONT_NAME,18);
			auto shadowColor = Color4B::BLACK;   // black
			missionType->enableShadow(shadowColor,Size(1.0f, -1.0f), 1.0f);
		}
		else if(curMissionInfo->missionpackagetype() == 2)
		{
			const char *str_mission_zhi = StringDataManager::getString("mission_zhi");
			missionType = Label::createWithTTF(str_mission_zhi,APP_FONT_NAME,18);
			auto shadowColor = Color4B::BLACK;   // black
			missionType->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		}
		else if(curMissionInfo->missionpackagetype() == 7)
		{
			const char *str_mission_jia = StringDataManager::getString("mission_jia");
			missionType = Label::createWithTTF(str_mission_jia,APP_FONT_NAME,18);
			auto shadowColor = Color4B::BLACK;   // black
			missionType->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		}
		else if(curMissionInfo->missionpackagetype() == 12)
		{
			const char *str_mission_xuan = StringDataManager::getString("mission_xuan");
			missionType = Label::createWithTTF(str_mission_xuan,APP_FONT_NAME,18);
			auto shadowColor = Color4B::BLACK;   // black
			missionType->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		}
		else
		{
			const char *str_mission_ri = StringDataManager::getString("mission_ri");
			missionType = Label::createWithTTF(str_mission_ri,APP_FONT_NAME,18);
			auto shadowColor = Color4B::BLACK;   // black
			missionType->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		}
		
		missionType->setAnchorPoint(Vec2(0,1.0f));
		missionType->setPosition(Vec2(4,52));
		Color3B _color = Color3B(240,0,0);
		missionType->setColor(_color);
		this->addChild(missionType);

		//mission name
		//CCLOG(missionInfo->missionname().c_str());
		missionName = Label::createWithTTF(missionInfo->missionname().c_str(),APP_FONT_NAME,18);
		missionName->setAnchorPoint(Vec2(0,1.0f));
		missionName->setPosition(Vec2(missionType->getPositionX()+missionType->getContentSize().width,52));
		auto shadowColor = Color4B::BLACK;   // black
		missionName->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
		this->addChild(missionName);

		//mission state
// 		missionSummary = Label::createWithTTF(missionInfo->summary().c_str(),APP_FONT_NAME,14);
// 		missionSummary->setAnchorPoint(Vec2(0,1.0f));
// 		missionSummary->setPosition(Vec2(4,missionType->getPositionY()-missionType->getContentSize().height+1));
// 		missionSummary->enableShadow(Size(1.0f, -1.0f), 1.0f, 1.0, shadowColor);
// 		this->addChild(missionSummary);

		//CCLOG(missionInfo->summary().c_str());
		missionSummary = CCRichLabel::createWithString(missionInfo->summary().c_str(),Size(300,20),this,NULL,0,14);
		missionSummary->setAnchorPoint(Vec2(0,1.0f));
		missionSummary->setPosition(Vec2(4,missionType->getPositionY()-missionType->getContentSize().height-4));
		this->addChild(missionSummary);

		switch(curMissionInfo->missionstate())
		{
		case dispatched : //�ѷ��  
			missionSummary->setString(StrUtils::applyColor(StringDataManager::getString("mission_state_kejie"),Color3B(254,254,51)).c_str());
			missionType->setColor(Color3B(254,254,51));                       //䵭��ɫ
			missionName->setColor(Color3B(254,254,51));                     //����ɫ
			//missionSummary->setColor(Color3B(254,254,51));              //����ɫ
			break;
		case accepted : //�ѽ��
			missionType->setColor(Color3B(10,227,221));                     //��ɫ
			missionName->setColor(Color3B(10,227,221));                   //�ɫ
			//missionSummary->setColor(Color3B(255,255,255));          //��ɫ
			break;
		case done ://����
			missionSummary->setString(StrUtils::applyColor(StringDataManager::getString("mission_state_wancheng"),Color3B(0,255,0)).c_str());
			missionType->setColor(Color3B(0,255,0));                     //���ɫ
			missionName->setColor(Color3B(0,255,0));                   //��ɫ
			//missionSummary->setColor(Color3B(0,255,0));            //��ɫ
			break;
		//case failed ://��ʧ�
		//	break;
		case canceled ://���ȡ�
			break;
		case submitted ://����ύ
			break;
		}

		return true;
	}
	return false;
}

void MissionCell::MissionEvent(Ref * pSender)
{
}

void MissionCell::addFinishAnm()
{
	std::string _path = "animation/texiao/particledesigner/renwudacheng.plist";
	auto particleEffect = ParticleSystemQuad::create(_path.c_str());
	particleEffect->setPositionType(ParticleSystem::PositionType::FREE);
	particleEffect->setVisible(true);
	particleEffect->setStartSize(30.f);
	particleEffect->setPosition(Vec2(85,20));

	auto path2 = SimpleEffectManager::RoundRectPathAction(100, 55, 0 );
	particleEffect->runAction(path2);

	addChild(particleEffect);
}

void MissionCell::RefreshCell( MissionInfo * missionInfo )
{
	curMissionInfo->CopyFrom(*missionInfo);
	//mission type
	if (missionInfo->missionpackagetype() == 1)
	{
		const char *str_mission_zhu = StringDataManager::getString("mission_zhu");
		missionType->setString(str_mission_zhu);
	}
	else if(missionInfo->missionpackagetype() == 2)
	{
		const char *str_mission_zhi = StringDataManager::getString("mission_zhi");
		missionType->setString(str_mission_zhi);
	}
	else if(missionInfo->missionpackagetype() == 7)
	{
		const char *str_mission_jia = StringDataManager::getString("mission_jia");
		missionType->setString(str_mission_jia);
	}
	else if(missionInfo->missionpackagetype() == 12)
	{
		const char *str_mission_xuan = StringDataManager::getString("mission_xuan");
		missionType->setString(str_mission_xuan);
	}
	else
	{
		const char *str_mission_ri = StringDataManager::getString("mission_ri");
		missionType->setString(str_mission_ri);
	}
	missionType->setColor(Color3B(240,0,0));

	//mission name
	missionName->setString(missionInfo->missionname().c_str());

	missionSummary->setString(missionInfo->summary().c_str());

	switch(missionInfo->missionstate())
	{
	case dispatched : //�ѷ��  
		missionSummary->setString(StrUtils::applyColor(StringDataManager::getString("mission_state_kejie"),Color3B(254,254,51)).c_str());
		missionType->setColor(Color3B(254,254,51));                       //䵭��ɫ
		missionName->setColor(Color3B(254,254,51));                     //����ɫ
		break;
	case accepted : //�ѽ��
		missionType->setColor(Color3B(10,227,221));                     //��ɫ
		missionName->setColor(Color3B(10,227,221));                   //�ɫ
		break;
	case done ://����
		missionSummary->setString(StrUtils::applyColor(StringDataManager::getString("mission_state_wancheng"),Color3B(0,255,0)).c_str());
		missionType->setColor(Color3B(0,255,0));                     //���ɫ
		missionName->setColor(Color3B(0,255,0));                   //��ɫ
		break;
		//case failed ://��ʧ�
		//	break;
	case canceled ://���ȡ�
		break;
	case submitted ://����ύ
		break;
	}
}
