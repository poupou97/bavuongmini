#include "GameSceneCamera.h"

#include "./role/GameActor.h"
#include "GameSceneState.h"
#include "../legend_script/ScriptManager.h"

#define CAMERA_ACTION_SHAKE_SCREEN_TAG 101
#define CAMERA_ACTION_ZOOM_SCREEN_TAG 102

GameSceneCamera::GameSceneCamera()
: m_pCurScene(NULL)
{
}

GameSceneCamera::~GameSceneCamera()
{
}

GameSceneCamera* GameSceneCamera::create(GameSceneLayer* scene, int behaviour)
{
    auto pNode = new GameSceneCamera();
    if(pNode && pNode->init(scene, behaviour))
    {
        pNode->autorelease();
        return pNode;
    }
    CC_SAFE_DELETE(pNode);
    return NULL;
}

bool GameSceneCamera::init(GameSceneLayer* scene, int behaviour)
{
	m_behaviour = behaviour;
	m_pCurScene = scene;

	return true;
}

void GameSceneCamera::MoveTo(Vec2 target, float time)
{
	// convert world position to cocos position
	Vec2 cocosPosition = m_pCurScene->convertToCocos2DSpace(target);

	if(time > 0)
	{
		auto action = MoveTo::create(time, cocosPosition);
		this->runAction(action);
	}
	else if(time == 0)
	{
		this->setPosition(cocosPosition);
	}

	m_behaviour = camera_behaviour_move;
}

void GameSceneCamera::FollowActor(GameActor* actor)
{
	m_behaviour = camera_behaviour_follow_actor;
}

void GameSceneCamera::restoreCamera(float dt)
{
	// follow the main actor
	FollowActor(NULL);
}

void GameSceneCamera::shakeScreen(GameSceneLayer* scene, float time)
{
	// playing story
	if(ScriptManager::getInstance()->isInMovieMode())
		return;

	auto sceneLayer = scene->getChildByTag(GameSceneLayer::kTagSceneLayer);
	if(sceneLayer->getActionByTag(CAMERA_ACTION_SHAKE_SCREEN_TAG) != NULL)
		return;

	// shake screen behaviour
	m_behaviour = camera_behaviour_shakescreen;

	Vec2 size = sceneLayer->getPosition();
	const int offset = 10;
	const int shake_action_steps = 5;
	const float shake_time = SHAKE_SCREEN_DEFAULT_TIME / shake_action_steps;
	float one_shake_duration = SHAKE_SCREEN_DEFAULT_TIME;
	unsigned int times = time / one_shake_duration;
	if(times <= 0)
		times = 1;
	auto left = MoveTo::create(shake_time,Vec2(size.x+offset,size.y));
	auto right = MoveTo::create(shake_time,Vec2(size.x-offset,size.y));
	auto top = MoveTo::create(shake_time,Vec2(size.x,size.y+offset));
	auto rom = MoveTo::create(shake_time,Vec2(size.x,size.y-offset));
	auto init = MoveTo::create(shake_time,Vec2(size.x,size.y));
	auto shake_action = Sequence::create(left,right,top,rom,init,NULL);
	auto action = Repeat::create(shake_action, times);
	action->setTag(CAMERA_ACTION_SHAKE_SCREEN_TAG);
	sceneLayer->runAction(action);

	// restore camera behaviour
	scheduleOnce( schedule_selector(GameSceneCamera::restoreCamera), one_shake_duration * times); 
}

void GameSceneCamera::shakeScreen(float time, int offset)
{
	Scene * currentScene = Director::getInstance()->getRunningScene();
	if(currentScene->getActionByTag(CAMERA_ACTION_SHAKE_SCREEN_TAG) != NULL)
		return;

	Vec2 size = currentScene->getPosition();
	const int shake_action_steps = 5;
	const float shake_time = SHAKE_SCREEN_DEFAULT_TIME / shake_action_steps;
	float one_shake_duration = SHAKE_SCREEN_DEFAULT_TIME;
	unsigned int times = time / one_shake_duration;
	if(times <= 0)
		times = 1;
	auto left = MoveTo::create(shake_time,Vec2(size.x+offset,size.y));
	auto right = MoveTo::create(shake_time,Vec2(size.x-offset,size.y));
	auto top = MoveTo::create(shake_time,Vec2(size.x,size.y+offset));
	auto rom = MoveTo::create(shake_time,Vec2(size.x,size.y-offset));
	auto init = MoveTo::create(shake_time,Vec2(size.x,size.y));
	auto shake_action = Sequence::create(left,right,top,rom,init,NULL);
	auto action = Repeat::create(shake_action, times);
	action->setTag(CAMERA_ACTION_SHAKE_SCREEN_TAG);
	currentScene->runAction(action);
}

Vec2 GameSceneCamera::getSceneCameraPosition(GameActor* actor)
{
	return Vec2(actor->getPositionX(), actor->getPositionY()+SCENE_CAMERA_OFFSET_Y);
}

void GameSceneCamera::zoomScreen(GameSceneLayer* scene, Vec2& centerPoint)
{
	// playing story
	if(ScriptManager::getInstance()->isInMovieMode())
		return;

	auto sceneLayer = scene->getChildByTag(GameSceneLayer::kTagSceneLayer);
	if(sceneLayer->getActionByTag(CAMERA_ACTION_ZOOM_SCREEN_TAG) != NULL)
		return;

	sceneLayer->setContentSize(Size(scene->getMapSize().width, scene->getMapSize().height/2));
	Vec2 cocosWorldPosition = scene->convertToCocos2DWorldSpace(centerPoint);
	float ax = cocosWorldPosition.x / scene->getMapSize().width;
	float ay = cocosWorldPosition.y / scene->getMapSize().height;
	sceneLayer->setAnchorPoint(Vec2(ax, ay));

	// special screen behaviour
	//m_behaviour = camera_behaviour_shakescreen;
	const float zoominDuration = 0.25f;
	const float zoombackDuration = 0.15f;
	auto zoomin = ScaleTo::create(zoominDuration, 1.25f);
	auto zoomback = ScaleTo::create(zoombackDuration, 1.0f);
	auto action = Sequence::create(zoomin,zoomback,NULL);
	action->setTag(CAMERA_ACTION_ZOOM_SCREEN_TAG);
	sceneLayer->runAction(action);

	// restore camera behaviour
	//scheduleOnce( schedule_selector(GameSceneCamera::restoreCamera), (zoominDuration + zoombackDuration)); 
}

void GameSceneCamera::zoom(GameSceneLayer* scene, Vec2& centerPoint, float zoomFrom, float zoomTo, float duration)
{
	// playing story
	if(ScriptManager::getInstance()->isInMovieMode())
		return;

	auto sceneLayer = scene->getChildByTag(GameSceneLayer::kTagSceneLayer);
	if(sceneLayer->getActionByTag(CAMERA_ACTION_ZOOM_SCREEN_TAG) != NULL)
		return;

	// calc center point, consider the map edge
	float _from = 1.0f / zoomFrom;
	float _to = 1.0f / zoomTo;
	float zoomValue = MAX(_from, _to);
	if(zoomValue < 1.0f)
		zoomValue = 1.0f;
	Size winSize = Director::getInstance()->getVisibleSize();
	Vec2 worldPos = centerPoint;
	Vec2 sourcePoint = centerPoint;
	// handle x
	bool exceedLeft = worldPos.x - (winSize.width/2 * zoomValue) < 0;
	bool exceedRight = worldPos.x + (winSize.width/2 * zoomValue) > scene->getMapSize().width;
	if(exceedLeft && exceedRight)
	{
		sourcePoint.x = scene->getMapSize().width/2;
	}
	else
	{
		if(exceedLeft)
		{
			sourcePoint.x = 0;
		}
		else if(exceedRight)
		{
			sourcePoint.x = scene->getMapSize().width;
		}
	}
	// handle y
	bool exceedTop = worldPos.y - (winSize.height*zoomValue) < 0;
	bool exceedBottom = worldPos.y + (winSize.height*zoomValue) > scene->getMapSize().height;
	if(exceedTop && exceedBottom)
	{
		sourcePoint.y = scene->getMapSize().height/2;
	}
	else
	{
		if(exceedTop)
		{
			sourcePoint.y = 0;
		}
		else if(exceedBottom)
		{
			sourcePoint.y = scene->getMapSize().height;
		}
	}
	centerPoint = sourcePoint;

	sceneLayer->setContentSize(Size(scene->getMapSize().width, scene->getMapSize().height/2));
	Vec2 cocosWorldPosition = scene->convertToCocos2DWorldSpace(centerPoint);
	float ax = cocosWorldPosition.x / scene->getMapSize().width;
	float ay = cocosWorldPosition.y / scene->getMapSize().height;
	sceneLayer->setAnchorPoint(Vec2(ax, ay));

	// special screen behaviour
	//m_behaviour = camera_behaviour_shakescreen;
	auto zoomin = ScaleTo::create(0.0001f, zoomFrom);
	auto zoomback = ScaleTo::create(duration, zoomTo);
	auto action = Sequence::create(zoomin,zoomback,NULL);
	action->setTag(CAMERA_ACTION_ZOOM_SCREEN_TAG);
	sceneLayer->runAction(action);

	// restore camera behaviour
	//scheduleOnce( schedule_selector(GameSceneCamera::restoreCamera), (zoominDuration + zoombackDuration)); 
}