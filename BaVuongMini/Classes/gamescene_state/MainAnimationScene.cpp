#include "MainAnimationScene.h"
#include "role/WeaponEffect.h"
#include "../ui/GameUIConstant.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "GameSceneEffects.h"
#include "MainScene.h"

MainAnimationScene::MainAnimationScene()
{

}

MainAnimationScene::~MainAnimationScene()
{

}

MainAnimationScene* MainAnimationScene::create()
{
	auto mainAnmScene = new MainAnimationScene();
	if (mainAnmScene && mainAnmScene->init())
	{
		mainAnmScene->autorelease();
		return mainAnmScene;
	}
	CC_SAFE_DELETE(mainAnmScene);
	return NULL;
}

bool MainAnimationScene::init()
{
	if (UIScene::init())
	{
		//���涯���
		m_baseLayer = Layer::create();
		this->addChild(m_baseLayer);

		return true;
	}
	return false;
}

void MainAnimationScene::onEnter()
{
	UIScene::onEnter();
}

void MainAnimationScene::onExit()
{
	UIScene::onExit();
}
/***********************
change by yangjun 
data:2014.11.12
//�����������ȼ��Ƚϸ
***************************/
#define Anm_LevelUp_Tag 1122

void MainAnimationScene::addInterfaceAnm(const char * anmName)
{
	if (m_baseLayer->getChildByTag(Anm_LevelUp_Tag))
		return;

	m_baseLayer->removeAllChildren();

	Size winSize = Director::getInstance()->getVisibleSize();

	//ParticleSystem* particleEffect = ParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo.plist");
	auto particleEffect = WeaponEffect::getParticle(INTERFACE_ANM_PARTICLE_EFFECT_1);
	particleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	particleEffect->setPosition(Vec2(winSize.width/2,winSize.height*0.7f));
	particleEffect->setScale(1.0f);
	m_baseLayer->addChild(particleEffect);

	//ParticleSystem* particleEffect1 = ParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo1.plist");
	auto particleEffect1 = WeaponEffect::getParticle(INTERFACE_ANM_PARTICLE_EFFECT_2);
	particleEffect1->setPositionType(ParticleSystem::PositionType::GROUPED);
	particleEffect1->setPosition(Vec2(winSize.width/2+35,winSize.height*0.7f+55));
	particleEffect1->setScale(1.0f);
	m_baseLayer->addChild(particleEffect1);

	//ParticleSystem* particleEffect2 = ParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo2.plist");
	auto particleEffect2 = WeaponEffect::getParticle(INTERFACE_ANM_PARTICLE_EFFECT_3);
	particleEffect2->setPositionType(ParticleSystem::PositionType::GROUPED);
	particleEffect2->setPosition(Vec2(winSize.width/2-80,winSize.height*0.7f+55));
	particleEffect2->setScale(1.0f);
	m_baseLayer->addChild(particleEffect2);

	//ParticleSystem* particleEffect3 = ParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo3.plist");
	auto particleEffect3 = WeaponEffect::getParticle(INTERFACE_ANM_PARTICLE_EFFECT_4);
	particleEffect3->setPositionType(ParticleSystem::PositionType::GROUPED);
	particleEffect3->setPosition(Vec2(winSize.width/2+100,winSize.height*0.7f+40));
	particleEffect3->setScale(1.0f);
	m_baseLayer->addChild(particleEffect3);

	// load animation
	std::string animFileName = "animation/texiao/jiemiantexiao/";
	animFileName.append(anmName);
	//animFileName.append("/");
	//animFileName.append(anmName);
	//animFileName.append(".anm");
	auto m_pAnim = CCLegendAnimation::create(animFileName);
	m_pAnim->setReleaseWhenStop(true);
	m_pAnim->setPosition(Vec2(winSize.width/2,winSize.height*0.7f));
	m_baseLayer->addChild(m_pAnim);
	std::string str_anm_namePath = anmName;
	if (strcmp("GXSJ",str_anm_namePath.substr(0,4).c_str()) == 0)
	{
		m_pAnim->setTag(Anm_LevelUp_Tag);
	}
}

void MainAnimationScene::addFightPointChangeEffect( int oldFight,int newFight )
{
	auto fightEffect_ = (FightPointChangeEffect*)this->getChildByTag(kTagFightPointChange);
	if (!fightEffect_)
	{
		auto fightEffect_ = FightPointChangeEffect::create(oldFight,newFight);
		if (fightEffect_)
		{
			fightEffect_->setTag(kTagFightPointChange);
			this->addChild(fightEffect_);
		}
	}
}

