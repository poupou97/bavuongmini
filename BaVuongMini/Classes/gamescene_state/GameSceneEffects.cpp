﻿#include "GameSceneEffects.h"

#include "../utils/GameUtils.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "GameSceneState.h"
#include "role/SimpleActor.h"
#include "GameView.h"
#include "AppMacros.h"
#include "../utils/StaticDataManager.h"
#include "../newcomerstory/NewCommerStoryManager.h"
#include "role/MyPlayer.h"
#include "MainScene.h"

LinkSpecialEffect::LinkSpecialEffect()
: m_nMaxLink(0)
, m_nCurLinkNum(0)
{
	//this->scheduleUpdate();
}

LinkSpecialEffect::~LinkSpecialEffect()
{
}

bool LinkSpecialEffect::init(const char* effectFileName, int maxLink)
{
	m_nMaxLink = maxLink;

	// link special effect anim
	for(int i = 0; i < maxLink; i++)
	{
		auto pAnim = CCLegendAnimation::create(effectFileName);
		pAnim->setPlayLoop(true);
		pAnim->setReleaseWhenStop(false);   // only play once, then release
		//pAnim->setColor(Color3B(255, 200, 200));
		addChild(pAnim, 0, i);
	}

	return true;
}

LinkSpecialEffect* LinkSpecialEffect::create(const char* effectFileName, int maxLink)
{
    auto pRet = new LinkSpecialEffect();
    if(pRet && pRet->init(effectFileName, maxLink))
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

void LinkSpecialEffect::setLinkNumber(int linkNum)
{
	CCAssert(linkNum <= m_nMaxLink, "out of range");
	for(int i = 0; i < m_nMaxLink; i++)
	{
		auto node = this->getChildByTag(i);
		node->setVisible(false);
	}

	for(int i = 0; i < linkNum; i++)
	{
		auto node = this->getChildByTag(i);
		node->setVisible(true);
	}

	m_nCurLinkNum = linkNum;
}

int LinkSpecialEffect::getLinkNumber()
{
	return m_nCurLinkNum;
}

void LinkSpecialEffect::setLinkPosition(Node* effectNode, Vec2 startPoint, Vec2 endPoint)
{
	if(effectNode == NULL)
		return;

	float distance = startPoint.getDistance(endPoint);
	float degree = GameUtils::getDegree(startPoint, endPoint);

	effectNode->setPosition(startPoint);
	effectNode->setRotation(degree);
	effectNode->setScaleY(distance / 170);
}

void LinkSpecialEffect::updateLinkPosition(std::vector<Vec2>& pointList)
{
	int num = pointList.size();
	CCAssert(num % 2 == 0, "wrong size");
	CCAssert(num / 2 <= m_nMaxLink, "out of range");

	for(int i = 0; i < num/2; i++)
	{
		auto node = this->getChildByTag(i);
		setLinkPosition(node, pointList.at(i*2), pointList.at(i*2+1));
	}
}

void LinkSpecialEffect::update(float dt)
{
}

//////////////////////////////////////////////////////////////

MusouSpecialEffect::MusouSpecialEffect()
{
}

MusouSpecialEffect::~MusouSpecialEffect()
{
}

bool MusouSpecialEffect::init(Vec2 effectPosition)
{
	Size s =Director::getInstance()->getVisibleSize();

	m_duration = 1.0f;

	// alpha mask layer
	//LayerColor *mask = LayerColor::create(Color4B(0,0,0,128));
	auto mask = LayerColor::create(Color4B(0,0,0,0));
	mask->setContentSize(s);
	addChild(mask);
	auto  action0 = Sequence::create(
		//Show::create(),
		FadeTo::create(0.5f, 128),
		//DelayTime::create(m_duration),
		DelayTime::create(0.5f),
		RemoveSelf::create(),
		NULL);
	mask->runAction(action0);

	// lhjn.anm
	auto pFirstAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/lhjn/lhjn.anm");
	pFirstAnim->setPosition(Vec2(effectPosition.x, effectPosition.y + 10));
	pFirstAnim->setScale(0.7f);
	addChild(pFirstAnim);

	// background
	auto sprBackground = Sprite::create("res_ui/font/super_di.png");
	sprBackground->setAnchorPoint(Vec2(0.5f,0.5f));
	sprBackground->setScale(4.0f);
	addChild(sprBackground);

	// special text
	auto sprText = Sprite::create("res_ui/font/super_zi.png");
	sprText->setAnchorPoint(Vec2(0.5f,0.5f));
	addChild(sprText);

	float y = s.height/2 - 120;
	float sprMaxWidth = MAX(sprText->getContentSize().width, sprBackground->getContentSize().width);
	sprText->setPosition(Vec2(s.width + sprMaxWidth/2, y));
	sprBackground->setPosition(Vec2(s.width + sprMaxWidth/2, y));

	auto spawn_action = Spawn::create(
		FadeOut::create(0.3f),
		ScaleTo::create(0.3f, 2.0f),
		NULL);

	auto action1 = Sequence::create(
		DelayTime::create(0.1f),
		MoveTo::create(0.4f, Vec2(s.width/2, y)), 
		DelayTime::create(0.4f),
		spawn_action,
		RemoveSelf::create(),
		NULL);
	sprBackground->runAction(action1);

	auto action2 = Sequence::create(
		MoveTo::create(0.4f, Vec2(s.width/2, y)), 
		DelayTime::create(0.1f),
		DelayTime::create(0.4f),
		spawn_action,
		RemoveSelf::create(),
		NULL);
	sprText->runAction(action2);

	// add the launch effect on the attacker
	const char* extra_effect_path_name = "animation/texiao/renwutexiao/BOSS_TX_1/boss_tx_1.anm";
	auto pAnim_sword1 = CCLegendAnimation::create(extra_effect_path_name);
	pAnim_sword1->setPlayLoop(true);
	pAnim_sword1->setScale(0.75f);
	pAnim_sword1->setVisible(false);
	auto pSwordAction = Sequence::create(
		DelayTime::create(0.6f),
		Show::create(),
		DelayTime::create(0.6f + 1.0f),
		RemoveSelf::create(),
		NULL);
	pAnim_sword1->runAction(pSwordAction);

	auto pAnim_sword2 = CCLegendAnimation::create(extra_effect_path_name);
	pAnim_sword2->setPlayLoop(true);
	pAnim_sword2->setScale(0.75f);
	pAnim_sword2->setVisible(false);
	pSwordAction = Sequence::create(
		DelayTime::create(0.9f),
		Show::create(),
		DelayTime::create(0.9f + 1.0f),
		RemoveSelf::create(),
		NULL);
	pAnim_sword2->runAction(pSwordAction);

	auto pAnim_sword3 = CCLegendAnimation::create(extra_effect_path_name);
	pAnim_sword3->setPlayLoop(true);
	pAnim_sword3->setScale(0.75f);
	pAnim_sword3->setVisible(false);
	pSwordAction = Sequence::create(
		DelayTime::create(0.8f),
		Show::create(),
		DelayTime::create(0.8f + 1.0f),
		RemoveSelf::create(),
		NULL);
	pAnim_sword3->runAction(pSwordAction);

	auto pAnim_sword4 = CCLegendAnimation::create(extra_effect_path_name);
	pAnim_sword4->setPlayLoop(true);
	pAnim_sword4->setScale(0.75f);
	pAnim_sword4->setVisible(false);
	pSwordAction = Sequence::create(
		DelayTime::create(0.5f),
		Show::create(),
		DelayTime::create(0.5f + 1.0f),
		RemoveSelf::create(),
		NULL);
	pAnim_sword4->runAction(pSwordAction);
	 
	auto pMyPlayer = GameView::getInstance()->myplayer;
	if (NULL != pMyPlayer)
	{
		//pMyPlayer->addEffect(pAnim_sword, true, 0);

		auto scene = pMyPlayer->getGameScene();
		auto pActorLayer = scene->getActorLayer();

		pAnim_sword1->setPosition(Vec2(pMyPlayer->getPositionX(), pMyPlayer->getPositionY() + 128));
		pActorLayer->addChild(pAnim_sword1, SCENE_TOP_LAYER_BASE_ZORDER);

		pAnim_sword2->setPosition(Vec2(pMyPlayer->getPositionX() - 196, pMyPlayer->getPositionY()));
		pActorLayer->addChild(pAnim_sword2, SCENE_TOP_LAYER_BASE_ZORDER);

		pAnim_sword3->setPosition(Vec2(pMyPlayer->getPositionX() + 196, pMyPlayer->getPositionY()));
		pActorLayer->addChild(pAnim_sword3, SCENE_TOP_LAYER_BASE_ZORDER);

		pAnim_sword4->setPosition(Vec2(pMyPlayer->getPositionX(), pMyPlayer->getPositionY() - 128));
		pActorLayer->addChild(pAnim_sword4, SCENE_TOP_LAYER_BASE_ZORDER);
	}

	// 释放龙魂时，主角脚下的光圈
	this->createBgAction(pMyPlayer);

	return true;
}

MusouSpecialEffect* MusouSpecialEffect::create(Vec2 effectPosition)
{
   auto pRet = new MusouSpecialEffect();
    if(pRet && pRet->init(effectPosition))
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

void MusouSpecialEffect::createBgAction( MyPlayer* pMyPlayer )
{
	if (NULL == pMyPlayer)
	{
		return;
	}

	auto scene = pMyPlayer->getGameScene();
	if (NULL == scene)
	{
		return;
	}

	// add the launch effect on the foot
	auto pSprite = Sprite::create("animation/texiao/renwutexiao/buff/dragon.png");
	auto rotate_action = Spawn::create(
		RotateBy::create(1.1f, 180),
		NULL);
	pSprite->runAction(rotate_action);

	auto pNode = Node::create();
	pNode->setCascadeOpacityEnabled(true);
	pNode->setScaleX(1.0f);
	pNode->setScaleY(0.5f);
	Vec2 pos = scene->convertToCocos2DSpace(pMyPlayer->getWorldPosition());
	pNode->setPosition(pos);
	pNode->addChild(pSprite);

	auto spawn_action = Spawn::create(
		FadeOut::create(0.4f),
		ScaleTo::create(0.4f, 3.0f, 1.5f),
		NULL);

	auto node_action = Sequence::create(
		DelayTime::create(1.1f),
		spawn_action,
		RemoveSelf::create(),
		NULL);
	pNode->runAction(node_action);

	pMyPlayer->addEffect(pNode, true, 0);
}

//////////////////////////////////////////////////////////////

void RangeRandomSpecialEffect::generate(const char* effectFileName, GameSceneLayer* scene, Vec2 center, Size area)
{
	// random the position
	float random = CCRANDOM_0_1();
	float x = area.width * random - area.width/2;
	random = CCRANDOM_0_1();
	float y = area.height * random - area.height/2;
	Vec2 result = center + Vec2(x, y);   // center + offset

	auto simpleActor = new SimpleActor();
	simpleActor->setGameScene(scene);
	simpleActor->setWorldPosition(result);
	simpleActor->loadAnim(effectFileName);
	auto actorLayer = scene->getActorLayer();
	actorLayer->addChild(simpleActor, SCENE_ROLE_LAYER_BASE_ZORDER);
	simpleActor->release();
}

//////////////////////////////////////////////////////////////

ArrowIndicator::ArrowIndicator()
: m_targetRoleId(NULL_ROLE_ID)
{
	this->scheduleUpdate();
}

ArrowIndicator::~ArrowIndicator()
{
}

#define ARROWINDICATOR_ANIM_TAG 101
bool ArrowIndicator::init(long long targetRoleId, float duration)
{
	setTarget(targetRoleId, duration);

	auto pAnim = CCLegendAnimation::create("animation/texiao/changjingtexiao/GSGB_a_b/gsgb_h.anm");
	pAnim->setPlayLoop(true);
	pAnim->setReleaseWhenStop(false);
	pAnim->setTag(ARROWINDICATOR_ANIM_TAG);
	addChild(pAnim);

	this->setScaleY(0.6f);

	return true;
}

ArrowIndicator* ArrowIndicator::create(long long targetRoleId, float duration)
{
    auto pRet = new ArrowIndicator();
    if(pRet && pRet->init(targetRoleId, duration))
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

void ArrowIndicator::setTarget(long long targetRoleId, float duration)
{
	m_targetRoleId = targetRoleId;
	m_duration = duration;

	auto scene = GameView::getInstance()->getGameScene();
	auto pActor = scene->getActor(m_targetRoleId);
	if(pActor == NULL)
	{
		setVisible(false);
	}
	else
	{
		setVisible(true);
	}

	this->unschedule(schedule_selector(ArrowIndicator::hide));
	this->schedule( schedule_selector(ArrowIndicator::hide), m_duration, 1, 0); 
}

void ArrowIndicator::hide(float dt)
{
	setVisible(false);
}

void ArrowIndicator::update(float dt)
{
	auto scene = GameView::getInstance()->getGameScene();
	auto pActor = scene->getActor(m_targetRoleId);
	if(pActor != NULL)
	{
		// according the target, adjust the rotation
		Vec2 targetPosition = scene->convertToCocos2DSpace(pActor->getWorldPosition());
		// parent is GameActor
		float degree = GameUtils::getDegree(this->getParent()->getPosition(), targetPosition);
		this->getChildByTag(ARROWINDICATOR_ANIM_TAG)->setRotation(degree);
		//this->setRotation(degree);
	}
}

//////////////////////////////////////////////////////////////

BonusSpecialEffect::BonusSpecialEffect()
{
}

BonusSpecialEffect::~BonusSpecialEffect()
{
}

bool BonusSpecialEffect::init()
{
	if(Node::init())
	{
		// step 1: prepare
		// rotate the circle
		auto pSprite = Sprite::create("animation/texiao/particledesigner/xuanzhuantexiao.png");
		pSprite->setAnchorPoint(Vec2(0.5f,0.5f));
		pSprite->setScale(2.0f);
		auto fadeoutAction = Spawn::create(
			RotateBy::create(1.5f,240),
			FadeOut::create(1.5f),
			NULL);
		pSprite->runAction(fadeoutAction);
		this->addChild(pSprite);

		// show the particle at the center of the circle
		auto centerParticleEffect = ParticleSystemQuad::create("animation/texiao/particledesigner/yunyu.plist");
		centerParticleEffect->setPosition(Vec2(0,0));
		centerParticleEffect->setScale(2.0f);
		auto removeAction = Sequence::create(
			DelayTime::create(0.5f),
			RemoveSelf::create(),
			NULL);
		centerParticleEffect->runAction(removeAction);
		this->addChild(centerParticleEffect);

		// step 2: add explode effect
		auto particleEffect = ParticleSystemQuad::create("animation/texiao/particledesigner/fanpaizhakai.plist");
		particleEffect->setPosition(Vec2(0,0));
		particleEffect->setVisible(false);
		particleEffect->setAutoRemoveOnFinish(true);
		particleEffect->setLife(0.4f);
		auto pDelayAction = DelayTime::create(100.5f);
		auto pAction = Sequence::create(
			DelayTime::create(0.45f),
			Show::create(),
			NULL);
		particleEffect->runAction(pAction);

		// remove this at last
		auto rootAction = Sequence::create(
			DelayTime::create(2.0f),
			RemoveSelf::create(),
			NULL);
		this->runAction(rootAction);
		this->addChild(particleEffect);

		return true;
	}

	return false;
}

BonusSpecialEffect* BonusSpecialEffect::create()
{
    auto pRet = new BonusSpecialEffect();
    if(pRet && pRet->init())
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

//////////////////////////////////////////////////////////////

#define APTITUDE_EFFECT_SCALETOBIG_TIME 0.5f
#define APTITUDE_EFFECT_SCALETOSMALL_TIME 0.2f
#define APTITUDE_EFFECT_MIN_SCALE 0.7f
#define APTITUDE_EFFECT_MAX_SCALE 1.0f
#define APTITUDE_EFFECT_FINAL_SCALE 0.9f
#define APTITUDE_EFFECT_FADEOUT_TIME 0.5f
#define APTITUDE_EFFECT_INTERVAL_TIME 0.5f

AptitudePopupEffect::AptitudePopupEffect()
{
}

AptitudePopupEffect::~AptitudePopupEffect()
{
}

bool AptitudePopupEffect::init(Aptitude* oldAptitude, Aptitude* newAptitude)
{
	if(Node::init())
	{
		handleAptitudePromotion(oldAptitude, newAptitude);
		m_totleTime = 0;
		float total_duration = 0.f;
		float current_delay_time = 0.f;
		for(unsigned int i = 0; i < m_allPromotions.size(); i++)
		{
			std::string showString = m_allPromotions.at(i).aptitudeName;
			char numStr[20];
			sprintf(numStr, "%d", m_allPromotions.at(i).number);
			showString.append(" +");
			showString.append(numStr);
			generateOneEffect(showString.c_str(), m_allPromotions.at(i).color, current_delay_time);

			current_delay_time += APTITUDE_EFFECT_INTERVAL_TIME;
		}

		total_duration = current_delay_time
				+ APTITUDE_EFFECT_SCALETOBIG_TIME 
				+ APTITUDE_EFFECT_SCALETOSMALL_TIME 
				+ APTITUDE_EFFECT_FADEOUT_TIME;
		m_totleTime = total_duration;

		auto action = Sequence::create(
			DelayTime::create(total_duration),
			RemoveSelf::create(),
			NULL);
		this->runAction(action);

		return true;
	}

	return false;
}

AptitudePopupEffect* AptitudePopupEffect::create(Aptitude* oldAptitude, Aptitude* newAptitude)
{
    auto pRet = new AptitudePopupEffect();
    if(pRet && pRet->init(oldAptitude, newAptitude))
    {
        pRet->autorelease();
        return pRet;
    }
    CC_SAFE_DELETE(pRet);
    return NULL;
}

void AptitudePopupEffect::generateOneEffect(const char* effectContent, Color3B color, float delayTime)
{
	//Label* pLabel= Label::createWithTTF(aptitudeName,APP_FONT_NAME,20);
	auto pLabel = Label::createWithBMFont("res_ui/font/ziti_4.fnt", effectContent);
	pLabel->setScale(APTITUDE_EFFECT_MIN_SCALE);
	pLabel->setColor(color);
	pLabel->setVisible(false);

	// scale to big
	auto step_one_action = Spawn::create(
		MoveBy::create(APTITUDE_EFFECT_SCALETOBIG_TIME, Vec2(0, 60)),
		ScaleTo::create(APTITUDE_EFFECT_SCALETOBIG_TIME, APTITUDE_EFFECT_MAX_SCALE, APTITUDE_EFFECT_MAX_SCALE),
		NULL);
	// scale to big
	auto step_two_action = Spawn::create(
		MoveBy::create(APTITUDE_EFFECT_SCALETOSMALL_TIME, Vec2(0, 15)),
		ScaleTo::create(APTITUDE_EFFECT_SCALETOSMALL_TIME, APTITUDE_EFFECT_FINAL_SCALE, APTITUDE_EFFECT_FINAL_SCALE),
		NULL);
	auto action = Sequence::create(
		DelayTime::create(delayTime),
		Show::create(),
		step_one_action,
		step_two_action,
		FadeOut::create(APTITUDE_EFFECT_FADEOUT_TIME),
		RemoveSelf::create(),
		NULL);
	pLabel->runAction(action);

	addChild(pLabel);
}

void AptitudePopupEffect::handleAptitudePromotion(Aptitude* oldAptitude, Aptitude* newAptitude)
{
	AptitudePromotionStruct onePromotion;

	const Color3B bmfont_green = Color3B(255, 255, 255);   // hp, mp etc.
	const Color3B bmfont_purple = Color3B(128, 255, 255);   // attack aptitude, for example, minAttack etc.
	const Color3B bmfont_blue = Color3B(0, 255, 255);   // defend aptitude, for example, minDefend etc.
	const Color3B bmfont_orange = Color3B(255, 148, 255);   // main aptitude, for example, strength, focus etc.

	int delta = 0;
	//
	// main aptitude, bmfont_orange
	//
	//// strength
	//int delta = newAptitude->strength() - oldAptitude->strength();
	//if(delta > 0)
	//{
	//	onePromotion.type = -1;
	//	onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_1");
	//	onePromotion.number = delta;
	//	onePromotion.color = bmfont_orange;
	//	m_allPromotions.push_back(onePromotion);
	//}

	//// dexterity
	//delta = newAptitude->dexterity() - oldAptitude->dexterity();
	//if(delta > 0)
	//{
	//	onePromotion.type = -1;
	//	onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_2");
	//	onePromotion.number = delta;
	//	onePromotion.color = bmfont_orange;
	//	m_allPromotions.push_back(onePromotion);
	//}

	//// intelligence
	//delta = newAptitude->intelligence() - oldAptitude->intelligence();
	//if(delta > 0)
	//{
	//	onePromotion.type = -1;
	//	onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_3");
	//	onePromotion.number = delta;
	//	onePromotion.color = bmfont_orange;
	//	m_allPromotions.push_back(onePromotion);
	//}

	//// focus
	//delta = newAptitude->focus() - oldAptitude->focus();
	//if(delta > 0)
	//{
	//	onePromotion.type = -1;
	//	onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_4");
	//	onePromotion.number = delta;
	//	onePromotion.color = bmfont_orange;
	//	m_allPromotions.push_back(onePromotion);
	//}

	//
	// hp, mp aptitude, bmfont_green
	//
	// hpCapacity
	delta = newAptitude->hpcapacity() - oldAptitude->hpcapacity();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_11");
		onePromotion.number = delta;
		onePromotion.color = bmfont_green;
		m_allPromotions.push_back(onePromotion);
	}

	// mpCapacity
	delta = newAptitude->mpcapacity() - oldAptitude->mpcapacity();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_12");
		onePromotion.number = delta;
		onePromotion.color = bmfont_green;
		m_allPromotions.push_back(onePromotion);
	}

	//
	// attack aptitude, bmfont_purple
	//
	// minAttack
	delta = newAptitude->minattack() - oldAptitude->minattack();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_13");
		onePromotion.number = delta;
		onePromotion.color = bmfont_purple;
		m_allPromotions.push_back(onePromotion);
	}
	
	// maxAttack
	delta = newAptitude->maxattack() - oldAptitude->maxattack();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_14");
		onePromotion.number = delta;
		onePromotion.color = bmfont_purple;
		m_allPromotions.push_back(onePromotion);
	}
	
	// minMagicAttack
	delta = newAptitude->minmagicattack() - oldAptitude->minmagicattack();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_15");
		onePromotion.number = delta;
		onePromotion.color = bmfont_purple;
		m_allPromotions.push_back(onePromotion);
	}

	// maxMagicAttack
	delta = newAptitude->maxmagicattack() - oldAptitude->maxmagicattack();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_16");
		onePromotion.number = delta;
		onePromotion.color = bmfont_purple;
		m_allPromotions.push_back(onePromotion);
	}

	// hit
	delta = newAptitude->hit() - oldAptitude->hit();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_21");
		onePromotion.number = delta;
		onePromotion.color = bmfont_purple;
		m_allPromotions.push_back(onePromotion);
	}

	// attackSpeed
	delta = newAptitude->attackspeed() - oldAptitude->attackspeed();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_25");
		onePromotion.number = delta;
		onePromotion.color = bmfont_purple;
		m_allPromotions.push_back(onePromotion);
	}

	// crit
	delta = newAptitude->crit() - oldAptitude->crit();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_23");
		onePromotion.number = delta;
		onePromotion.color = bmfont_purple;
		m_allPromotions.push_back(onePromotion);
	}

	// critDamage
	delta = newAptitude->critdamage() - oldAptitude->critdamage();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_24");
		onePromotion.number = delta;
		onePromotion.color = bmfont_purple;
		m_allPromotions.push_back(onePromotion);
	}

	//
	// defend aptitude, bmfont_blue
	//
	// minDefend
	delta = newAptitude->mindefend() - oldAptitude->mindefend();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_17");
		onePromotion.number = delta;
		onePromotion.color = bmfont_blue;
		m_allPromotions.push_back(onePromotion);
	}

	// maxDefend
	delta = newAptitude->maxdefend() - oldAptitude->maxdefend();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_18");
		onePromotion.number = delta;
		onePromotion.color = bmfont_blue;
		m_allPromotions.push_back(onePromotion);
	}

	// minMagicDefend
	delta = newAptitude->minmagicdefend() - oldAptitude->minmagicdefend();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_19");
		onePromotion.number = delta;
		onePromotion.color = bmfont_blue;
		m_allPromotions.push_back(onePromotion);
	}

	// maxMagicDefend
	delta = newAptitude->maxmagicdefend() - oldAptitude->maxmagicdefend();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_20");
		onePromotion.number = delta;
		onePromotion.color = bmfont_blue;
		m_allPromotions.push_back(onePromotion);
	}

	// dodge
	delta = newAptitude->dodge() - oldAptitude->dodge();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_22");
		onePromotion.number = delta;
		onePromotion.color = bmfont_blue;
		m_allPromotions.push_back(onePromotion);
	}

	// moveSpeed
	delta = newAptitude->movespeed() - oldAptitude->movespeed();
	if(delta > 0)
	{
		onePromotion.type = -1;
		onePromotion.aptitudeName = StringDataManager::getString("generals_strategies_property_26");
		onePromotion.number = delta;
		onePromotion.color = bmfont_blue;
		m_allPromotions.push_back(onePromotion);
	}
}

float AptitudePopupEffect::getActionTotalTime()
{
	return m_totleTime;
}


///////////////////////////////////////////////////////////////////////////////
NumberRollEffect::NumberRollEffect()
{

}

NumberRollEffect::~NumberRollEffect()
{

}

NumberRollEffect* NumberRollEffect::create( int maxNumber,const char * fontName,float totalTime,int startNumber )
{
	auto pRet = new NumberRollEffect();
	if(pRet && pRet->init(maxNumber, fontName,totalTime,startNumber))
	{
		pRet->autorelease();
		return pRet;
	}
	CC_SAFE_DELETE(pRet);
	return NULL;
}

bool NumberRollEffect::init( int maxNumber,const char * fontName,float totalTime,int startNumber )
{
	if(Node::init())
	{
		m_nMaxNumber = maxNumber;
		m_fTotalTime = totalTime;
		m_nStartNumber = startNumber;
		m_currentNumber = m_nStartNumber;

		m_changeSpeed =  (m_nMaxNumber - startNumber)/m_fTotalTime;

		char s [20];
		sprintf(s,"%d",startNumber);
		pLabel = Label::createWithBMFont(fontName, s);
		pLabel->setAnchorPoint(Vec2(0,0));
		pLabel->setPosition(Vec2(0,0));
		addChild(pLabel);

		this->setContentSize(pLabel->getContentSize());
		this->scheduleUpdate();
		return true;
	}
	return false;
}

void NumberRollEffect::update( float dt )
{
	char s [20];
	m_currentNumber = m_currentNumber+m_changeSpeed*dt;
	if( m_currentNumber > m_nMaxNumber)
	{
		this->unscheduleUpdate();
		m_currentNumber = m_nMaxNumber;
	}
	sprintf(s,"%d", m_currentNumber);
	pLabel->setString(s);
	this->setContentSize(pLabel->getContentSize());
}

///////////////////////////////////////////////////
FightPointChangeEffect::FightPointChangeEffect()
{

}

FightPointChangeEffect::~FightPointChangeEffect()
{

}

FightPointChangeEffect * FightPointChangeEffect::create( int oldFight,int newFight )
{
	auto fightPoint_ = new FightPointChangeEffect();
	if (fightPoint_ && fightPoint_->init(oldFight,newFight))
	{
		fightPoint_->autorelease();
		return fightPoint_;
	}
	CC_SAFE_DELETE(fightPoint_);
	return NULL;
}

bool FightPointChangeEffect::init( int oldFight,int newFight )
{
	if (Node::init())
	{
		if (NewCommerStoryManager::getInstance()->IsNewComer())
			return false;

		winSize = Director::getInstance()->getVisibleSize();

		m_curShowFight = oldFight;
		m_newFightValue = newFight;

		auto backGroundSP1 = Sprite::create("res_ui/kuang_01.png");
		backGroundSP1->setAnchorPoint(Vec2(1.0f,0.5f));
		addChild(backGroundSP1);

		auto backGroundSP2 = Sprite::create("res_ui/kuang_01.png");
		backGroundSP2->setRotation(180);
		backGroundSP2->setAnchorPoint(Vec2(1.0f,0.5f));
		addChild(backGroundSP2);

		Color3B m_clor;
		int m_changeValue;
		if (newFight > oldFight)
		{
			m_state = 1;//value is up
			m_fightChangeSpeed =  (newFight - oldFight)/1.5f;
			sp_fight = Sprite::create("res_ui/font/fightup.png");
			sp_ = Sprite::create("res_ui/jt2_up.png");
			m_clor = Color3B(0,255,0);
			m_changeValue = newFight - oldFight;
		}else
		{
			m_state = 0;//value is down
			m_fightChangeSpeed =  (oldFight - newFight)/1.5f;
			sp_fight = Sprite::create("res_ui/font/fightup.png");
			sp_ = Sprite::create("res_ui/jt2_down.png");
			m_clor = Color3B(255,0,0);
			m_changeValue = oldFight - newFight;
		}

		sp_fight->setAnchorPoint(Vec2(0,0.5f));
		sp_fight->setPosition(Vec2(-15,0));
		addChild(sp_fight);

		char str[20];
		sprintf(str,"%d",m_curShowFight);

		m_fightPointLabel = Label::createWithBMFont("res_ui/font/ziti_1.fnt", str);
		m_fightPointLabel->setAnchorPoint(Vec2(0.5f,0.5f));
		m_fightPointLabel->setPosition(Vec2(sp_fight->getContentSize().width+m_fightPointLabel->getContentSize().width/2,-2));
		addChild(m_fightPointLabel);

		sp_->setAnchorPoint(Vec2(0,0.5f));
		sp_->setPosition(Vec2(m_fightPointLabel->getPositionX()+m_fightPointLabel->getContentSize().width/2+5,0));
		addChild(sp_);

		char strValue[20];
		sprintf(strValue,"%d",m_changeValue);
		label_changeValue = Label::createWithTTF(strValue, APP_FONT_NAME, 18);
		label_changeValue->setAnchorPoint(Vec2(0,0.5f));
		label_changeValue->setPosition(Vec2(sp_->getPositionX()+ sp_->getContentSize().width,-2));
		label_changeValue->setColor(m_clor);
		addChild(label_changeValue);
		
		int temp_labelWidth = sp_fight->getContentSize().width + sp_->getContentSize().width +m_fightPointLabel->getContentSize().width+label_changeValue->getContentSize().width;
		backGroundSP1->setPosition(Vec2(temp_labelWidth/2,0));
		backGroundSP2->setPosition(Vec2(temp_labelWidth/2,0));

		auto action =(ActionInterval *)Sequence::create(
			Show::create(),
			DelayTime::create(0.3f),
			CallFunc::create(CC_CALLBACK_0(FightPointChangeEffect::startChangeFightValue, this)),
			NULL);

		this->runAction(action);
		int roleH_ = GameView::getInstance()->myplayer->getContentSize().height;
		this->setIgnoreAnchorPointForPosition(false);
		this->setAnchorPoint(Vec2(0.5f,0.5f));
		this->setPosition(Vec2(winSize.width/2+45,winSize.height/2 - roleH_/2));
		//this->setContentSize(m_fightPointLabel->getContentSize());
		int w_ = backGroundSP1->getContentSize().width + backGroundSP2->getContentSize().width;
		int h_ = backGroundSP1->getContentSize().height;
		this->setContentSize(Size(w_,h_));
		return true;
	}
	return false;
}

void FightPointChangeEffect::update( float delta )
{
	if (m_state == 1)
	{
		int m_temp = m_fightChangeSpeed*delta;
		if (m_temp <1)
		{
			m_temp = 1;
		}
		m_curShowFight += m_temp;

		if( m_curShowFight >= m_newFightValue )
		{
			this->unscheduleUpdate();
			m_curShowFight = m_newFightValue;

			auto action =(ActionInterval *)Sequence::create(
				Show::create(),
				ScaleTo::create(0.1f,1.3f,1.3f),
				ScaleTo::create(0.1f,1.0f,1.0f),
				CallFunc::create(CC_CALLBACK_0(FightPointChangeEffect::endChangeFightValue, this)),
				NULL);
			m_fightPointLabel->runAction(action);
		}
	}else
	{
		m_curShowFight -= m_fightChangeSpeed*delta;

		if( m_curShowFight <= m_newFightValue )
		{
			this->unscheduleUpdate();
			m_curShowFight = m_newFightValue;

			auto action =(ActionInterval *)Sequence::create(
				Show::create(),
				ScaleTo::create(0.1f,1.3f,1.3f),
				ScaleTo::create(0.1f,1.0f,1.0f),
				CallFunc::create(CC_CALLBACK_0(FightPointChangeEffect::endChangeFightValue, this)),
				NULL);
			m_fightPointLabel->runAction(action);
		}
	}

	
	char str[20];
	sprintf(str,"%d",m_curShowFight);
	m_fightPointLabel->setString(str);

	sp_->setPosition(Vec2(m_fightPointLabel->getPositionX() +m_fightPointLabel->getContentSize().width/2+5,0));
	label_changeValue->setPosition(Vec2(sp_->getPositionX()+ sp_->getContentSize().width,-2));
	//this->setContentSize(m_fightPointLabel->getContentSize());
}

void FightPointChangeEffect::startChangeFightValue()
{
	this->scheduleUpdate();
}

void FightPointChangeEffect::endChangeFightValue()
{
	auto action =(ActionInterval *)Sequence::create(
		Show::create(),
		DelayTime::create(1.0f),
		RemoveSelf::create(),
		NULL);

	this->runAction(action);
}

////////////////////////////////////////////////////////////////////////////

#define TIME_CIRCLE_SECOND 0.05f

PhysicalBarEffect::PhysicalBarEffect()
	:m_nMaxNumber(0)
	,m_nTargetNumber(0)
	,m_fTotalTime(0.0f)
	,m_changeSpeed(0.0f)
	,m_currentNumber(0)
	,m_pImageView(NULL)
	,m_pLabel(NULL)
	,m_fDelayTime(0.0f)
	,m_delte(0.0f)
{

}

PhysicalBarEffect::~PhysicalBarEffect()
{

}

PhysicalBarEffect* PhysicalBarEffect::create( int nTargetPhy, int nMaxPhy, float duration, ImageView* pImageView, Text* pLabel, float delay /*= 0.0f*/ )
{
	auto pPhyBarEffect = new PhysicalBarEffect();
	if(pPhyBarEffect && pPhyBarEffect->init(nTargetPhy, nMaxPhy, duration, pImageView, pLabel, delay))
	{
		pPhyBarEffect->autorelease();
		return pPhyBarEffect;
	}
	CC_SAFE_DELETE(pPhyBarEffect);
	return NULL;
}

PhysicalBarEffect* PhysicalBarEffect::create( int nTargetPhy, int nMaxPhy, float duration, Text* pLabel )
{
	auto pPhyBarEffect = new PhysicalBarEffect();
	if(pPhyBarEffect && pPhyBarEffect->init(nTargetPhy, nMaxPhy, duration, pLabel))
	{
		pPhyBarEffect->autorelease();
		return pPhyBarEffect;
	}
	CC_SAFE_DELETE(pPhyBarEffect);
	return NULL;
}

bool PhysicalBarEffect::init( int nTargetPhy, int nMaxPhy, float ToatalDuration, ImageView* pImageView, Text* pLabel, float delay /*= 0.0f*/ )
{
	if (Node::init())
	{
		m_nMaxNumber = nMaxPhy;
		m_nTargetNumber = nTargetPhy;
		m_fTotalTime = ToatalDuration;

		m_changeSpeed = (float)m_nMaxNumber / m_fTotalTime;

		m_pImageView = pImageView;
		m_pLabel = pLabel;

		m_fDelayTime = delay;

		this->scheduleUpdate();

		return true;
	}

	return false;
}

bool PhysicalBarEffect::init( int nTargetPhy, int nMaxPhy, float ToatalDuration, Text* pLabel )
{
	if (Node::init())
	{
		m_nMaxNumber = nMaxPhy;
		m_nTargetNumber = nTargetPhy;
		m_fTotalTime = ToatalDuration;

		m_changeSpeed = (m_nMaxNumber - 0) / m_fTotalTime;

		m_pLabel = pLabel;

		this->schedule(schedule_selector(PhysicalBarEffect::updateForLabel), 0.01f);

		return true;
	}

	return false;
}

void PhysicalBarEffect::update( float dt )
{
	m_delte += dt;

	if (m_delte < m_fDelayTime)
	{
		return;
	}

	/*char charDelate[20];
	sprintf(charDelate, "%f", m_delte);
	GameView::getInstance()->showAlertDialog(charDelate);*/

	m_currentNumber += m_changeSpeed * dt;
	if (m_currentNumber > m_nTargetNumber)
	{
		this->unscheduleUpdate();
		m_currentNumber = m_nTargetNumber;
	}

	std::string str_phy_value;

	char char_maxPhy[20];
	sprintf(char_maxPhy, "%d", m_nMaxNumber);

	char char_nowPhy[20];
	sprintf(char_nowPhy, "%d", m_currentNumber);

	str_phy_value.append(char_nowPhy);
	str_phy_value.append("/");
	str_phy_value.append(char_maxPhy);

	if (NULL != m_pLabel)
	{
		m_pLabel->setString(str_phy_value.c_str());
	}

	float fNowPhy = atof(char_nowPhy);
	float fMaxPhy = atof(char_maxPhy);
	float phy_scale = fNowPhy / fMaxPhy;
	if (phy_scale > 1.0f)
	{
		phy_scale = 1.0f;
	}

	if (NULL != m_pImageView)
	{
		m_pImageView->setTextureRect(Rect(0, 0, 
			m_pImageView->getContentSize().width * phy_scale,
			m_pImageView->getContentSize().height));
	}

}

void PhysicalBarEffect::updateForLabel( float dt )
{
	m_currentNumber += m_changeSpeed * dt;
	if (m_currentNumber > m_nTargetNumber)
	{
		this->unschedule(schedule_selector(PhysicalBarEffect::updateForLabel));
		m_currentNumber = m_nTargetNumber;
	}

	char char_nowPhy[20];
	sprintf(char_nowPhy, "%d", m_currentNumber);

	if (NULL != m_pLabel)
	{
		m_pLabel->setString(char_nowPhy);
	}
}

	
///////////////////////////////////////////////////////////////////
#define random(x) (rand()%x)
#define Speed_Action_exp 160.f

GetExpEffect::GetExpEffect()
{

}

GetExpEffect::~GetExpEffect()
{

}

GetExpEffect* GetExpEffect::create( int curExp,int nexExp,bool isLevelUp /*= false*/ )
{
	auto getExpEffect = new GetExpEffect();
	if (getExpEffect && getExpEffect->init(curExp,nexExp,isLevelUp))
	{
		getExpEffect->autorelease();
		return getExpEffect;
	}
	CC_SAFE_DELETE(getExpEffect);
	return NULL;
}

bool GetExpEffect::init( int curExp,int nexExp,bool isLevelUp )
{
	if (UIScene::init())
	{
		Size winSize = Director::getInstance()->getVisibleSize();

		auto mainScene = GameView::getInstance()->getMainUIScene();
		if(mainScene == NULL)
			return NULL;

		for (int i = 0;i<10;i++)
		{
			auto image_green_exp = ImageView::create();
			image_green_exp->loadTexture("res_ui/diaoluo_exp.png");
			image_green_exp->setAnchorPoint(Vec2(.5f,.5f));
			double temp_i = (double)i;
			float temp = (float)pow(-1,temp_i);
			image_green_exp->setPosition(Vec2(winSize.width/2,winSize.height/2));
			//image_green_exp->setPosition(Vec2(winSize.width/2+random(60)*temp,winSize.height/2+random(40)*temp));
			m_pLayer->addChild(image_green_exp);

			char s_idx [5];
			sprintf(s_idx,"%d",i);
			std::string str_name = "image_green_exp_";
			str_name.append(s_idx);
			image_green_exp->setName(str_name.c_str());
		}

		int space = 15;
		float firstPosX = winSize.width/2-space*2;
		for (int i = 0;i<10;i++)
		{
			float firstPosY  = 0;
			if (i >= 5)
			{
				firstPosY = winSize.height/2+8;
			}
			else
			{
				firstPosY = winSize.height/2-8;
			}

			char s_idx [5];
			sprintf(s_idx,"%d",i);
			std::string str_name = "image_green_exp_";
			str_name.append(s_idx);
			auto image_green_exp = (ImageView*)m_pLayer->getChildByName(str_name.c_str());
			if (image_green_exp)
			{
				float oldScaleX = mainScene->ImageView_exp->getScaleX();
				float newScaleX = (float)(curExp)/(nexExp);
				float posX = 28;
				float posY = mainScene->ImageView_exp->getPosition().y;
				double temp_i = (double)i;
				float temp = (float)pow(-1,temp_i);

				if (isLevelUp)
				{
					auto sequence = Sequence::create(
						DelayTime::create(0.05f),
						MoveTo::create(0.1f,Vec2(winSize.width/2+random(60)*temp,winSize.height/2+random(40)*temp)),
						DelayTime::create(i*0.05f),
						CCEaseSineIn::create(MoveTo::create(0.6f,Vec2(oldScaleX*mainScene->ImageView_exp->getContentSize().width+posX,posY))),
						DelayTime::create(1.2f-i*0.05f),
						CCEaseSineIn::create(MoveTo::create((1.0f-oldScaleX)*100.0f/Speed_Action_exp,Vec2(mainScene->ImageView_exp->getContentSize().width*1.0f+posX,posY))),
						DelayTime::create(0.1f),
						MoveTo::create(0.001f,Vec2(posX,posY)),
						DelayTime::create(0.1f),
						CCEaseSineIn::create(MoveTo::create(newScaleX*100.0f/Speed_Action_exp,Vec2(mainScene->ImageView_exp->getContentSize().width*newScaleX*1.0f+posX,posY))),
						DelayTime::create(1.0f-i*0.05f),
						RemoveSelf::create(),
						NULL);
					image_green_exp->runAction(sequence);

					auto sequence_image_exp = Sequence::create(
						DelayTime::create(1.95f),
						CCEaseSineIn::create(ScaleTo::create((1.f-oldScaleX)*100.f/Speed_Action_exp,1.0f,1.0f)),
						DelayTime::create(0.1f),
						ScaleTo::create(0.001f,0.0f,1.0f),
						CCEaseSineIn::create(ScaleTo::create(newScaleX*100.f/Speed_Action_exp,newScaleX,1.0f)),
						NULL);
					mainScene->ImageView_exp->runAction(sequence_image_exp);	
				}
				else
				{
					auto sequence = Sequence::create(
						DelayTime::create(0.05f),
						MoveTo::create(0.1f,Vec2(winSize.width/2+random(60)*temp,winSize.height/2+random(40)*temp)),
						DelayTime::create(i*0.05f),
						CCEaseSineIn::create(MoveTo::create(0.6f,Vec2(oldScaleX*mainScene->ImageView_exp->getContentSize().width+posX,posY))),
						DelayTime::create(1.2f-i*0.05f),
						CCEaseSineIn::create(MoveTo::create((newScaleX-oldScaleX)*100.0f/Speed_Action_exp,Vec2(mainScene->ImageView_exp->getContentSize().width*newScaleX*1.0f+posX,posY))),
						DelayTime::create(1.0f-i*0.05f),
						RemoveSelf::create(),
						NULL);
					image_green_exp->runAction(sequence);

					auto sequence_image_exp = Sequence::create(
						DelayTime::create(1.95f),
						CCEaseSineIn::create(ScaleTo::create((newScaleX - mainScene->ImageView_exp->getScaleX())*100.f/Speed_Action_exp,newScaleX,1.0f)),
						NULL);
					mainScene->ImageView_exp->runAction(sequence_image_exp);
				}
			}
		}

		this->setContentSize(winSize);
		return true;
	}
	return false;
}
