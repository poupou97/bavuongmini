
#ifndef _GAMESCENESTATE_MAINANIMATIONSCENE_H_
#define _GAMESCENESTATE_MAINANIMATIONSCENE_H_

#include "../ui/extensions/uiscene.h"
#include "cocos-ext.h"
#include "cocos2d.h"
#include "../GameStateBasic.h"
#include "GameSceneState.h"

USING_NS_CC;
USING_NS_CC_EXT;

class MainAnimationScene :public UIScene
{
public:
	MainAnimationScene();
	~MainAnimationScene();
	static MainAnimationScene* create();
	bool init();
	void onEnter();
	void onExit();
	//��ӽ��涯��Ч�
	void addInterfaceAnm(const char * anmName);
	//����ս����仯Ч�
	void addFightPointChangeEffect(int oldFight,int newFight);

private:
	Layer * m_baseLayer;
};

#endif

