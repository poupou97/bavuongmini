#ifndef _GAMESCENESTATE_EXSTATUS_IMMOBILIZE_H_
#define _GAMESCENESTATE_EXSTATUS_IMMOBILIZE_H_

#include "ExStatus.h"

/**
 * 定身状态
 * @author zhaogang
 */
class ExStatusImmobilize : public ExStatus {
public:
	ExStatusImmobilize();
	virtual ~ExStatusImmobilize();
	
	virtual void onAdd(ExtStatusInfo* info);
	virtual void onCancel();
};

#endif