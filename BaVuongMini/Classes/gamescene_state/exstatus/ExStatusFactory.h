#ifndef _GAMESCENESTATE_EXSTATUSFACTORY_H_
#define _GAMESCENESTATE_EXSTATUSFACTORY_H_

class ExStatus;

/**
 * 异常状态的工厂类
 * @author zhaogang
 */
class ExStatusFactory {
public:
	static ExStatusFactory* getInstance();
	
	ExStatus* getExStatus(int exStatusType, long duration, long remaintime);

private:
	static ExStatusFactory* sInstance;

};

#endif