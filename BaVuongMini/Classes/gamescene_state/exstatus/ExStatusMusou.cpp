#include "ExStatusMusou.h"

#include "../role/BasePlayer.h"
#include "ExStatusType.h"
#include "../sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../GameView.h"
#include "../role/MyPlayer.h"
#include "../role/General.h"
#include "../role/ActorUtils.h"
#include "../GameSceneState.h"
	
ExStatusMusou::ExStatusMusou() {
}

ExStatusMusou::~ExStatusMusou(){
}

void ExStatusMusou::onAdd(ExtStatusInfo* info) {
	CCAssert(mFighter != NULL, "mFighter should not be null, please setFighter()");

	// the musou skill is not open, so ignore
	int musouSkillOpenLevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[7];
	if (GameView::getInstance()->myplayer->getActiveRole()->level() < musouSkillOpenLevel)
		return;

	// effect on body for the BasePlayer
	auto master = dynamic_cast<BasePlayer*>(mFighter);
	if(master != NULL && !master->hasExStatus(ExStatusType::musou))
	{
		// �����佫�б�
		// ���ȴ�ʩ���Խ��bug: ������ҵ��佫�б�δ��ʱ����
		master->updateAliveGeneralsList();

		// add link effect
		master->onAddMusouEffect();
	}

	// effect on body for the General
	auto pGeneral = dynamic_cast<General*>(mFighter);
	if(pGeneral != NULL)
	{
		auto pPlayer = dynamic_cast<BasePlayer*>(pGeneral->getGameScene()->getActor(pGeneral->getOwnerId()));
		if(pPlayer != NULL && !pGeneral->hasExStatus(ExStatusType::musou))
		{
			ActorUtils::addMusouBodyEffect(mFighter, pPlayer->getColorByProfession(pPlayer->getProfession()));
		}
	}
}

void ExStatusMusou::onCancel() {
	if(mFighter != NULL)
	{
		// remove link effect
		auto master = dynamic_cast<BasePlayer*>(mFighter);
		if(master != NULL)
			master->onRemoveMusouEffect();

		// remove effect on body
		int effectTag = BaseFighter::kTagMosouEffect+mFighter->getRoleId();
		auto container = mFighter->getChildByTag(BaseFighter::kTagFootEffectContainer);
		auto effect = container->getChildByTag(effectTag);
		if(effect != NULL)
			effect->removeFromParent();
	}
}