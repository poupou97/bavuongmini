#ifndef _GAMESCENESTATE_EXSTATUSTYPE_H_
#define _GAMESCENESTATE_EXSTATUSTYPE_H_

/**
 * 异常状态的各种类型
 * @author zhaogang
 */
class ExStatusType {
public:
	enum Type {
		non = 0,
		decrease_damage,
		defence_maxhp,
		magic_defence,
		invisible,
		trapped,
		dead,
		treatment,
		poisoning_grey,
		defend,
		hot_wheels,
		blaze,
		poisoning_yellow,
		hit,
		vertigo,
		skill_treatment,
		immobilize,
		damange,
		move_damange,
		decrease_attack,
		increase_magattack,
		increase_crit,
		// 魔法盾 22
		magic_shield,
		//免疫 23
		immunity,
		//特殊暴擊（受到傷害時候終止特效） 24
		hurt_crit,    
		//沉默(无法攻击和使用技能) 25 
		taciturn,
		//治疗 26
		cure, 
		//治疗结算 27
		cure_close, 
		//无双状态 28
		musou,
		//减防 29
		reduce_defend,
		//龙魂天赋 30
		dragon_talent_pojian,
		//龙魂天赋 31
		dragon_talent_jizhi,
		//龙魂天赋 32
		dragon_talent_jingzhun,
		//龙魂天赋 33
		dragon_talent_qingying,
		// 副本buff
		vip_copy,
		// 不可采集
		collect_disable,
		// 五倍经验
		mutil_exp
	};
};

#endif