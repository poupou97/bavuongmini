#include "ExStatusDeath.h"

#include "../role/BaseFighterOwnedCommand.h"
#include "../role/BaseFighterOwnedStates.h"
#include "../role/MyPlayer.h"
#include "../../utils/GameUtils.h"
#include "../../GameView.h"
#include "../GameSceneState.h"
#include "../../newcomerstory/NewCommerStoryManager.h"
#include "../role/BaseFighterConstant.h"

ExStatusDeath::ExStatusDeath() 
{
}

ExStatusDeath::~ExStatusDeath(){
}

void ExStatusDeath::onAdd(ExtStatusInfo* info) {
	if(mFighter == NULL)
	{
		CCAssert(false, "please setFighter()");
		return;
	}

	// enter death action
	if(!mFighter->isDead())
	{
		// prepare action's info
		ActionDeathContext context;
		float random = CCRANDOM_0_1();
		if(random < 0.6f)
			context.deathType = 1;   // more chance
		else
			context.deathType = 2;
		//context.deathType = 1;
		context.attackerRoleId = info->sourceid();
		context.startTime = GameUtils::millisecondNow();
		context.setContext(mFighter);

		// ���ԭ����ѡ��״̬
		MyPlayer* me = GameView::getInstance()->myplayer;
		if(mFighter->getRoleId() == me->getLockedActorId())
			me->setLockedActorId(NULL_ROLE_ID);
		// �����Ȧ
		mFighter->removeDangerCircle();

		if(mFighter->isMyPlayerGroup())
			mFighter->stopActionByTag(BASEFIGHTER_INVISIBLE_ACTION);

		mFighter->changeAction(ACT_DIE);
	}
}

void ExStatusDeath::onCancel() {
}