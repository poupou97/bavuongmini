#ifndef _GAMESCENESTATE_EXSTATUS_DEATH_H_
#define _GAMESCENESTATE_EXSTATUS_DEATH_H_

#include "ExStatus.h"

/**
 * 死了
 * @author zhaogang
 */
class ExStatusDeath : public ExStatus {
public:
	ExStatusDeath();
	virtual ~ExStatusDeath();
	
	virtual void onAdd(ExtStatusInfo* info);
	
	virtual void onCancel();
};

#endif