#ifndef _GAMESCENE_MUSIC_CONTROLLER_H_
#define _GAMESCENE_MUSIC_CONTROLLER_H_

#include "cocos2d.h"
USING_NS_CC;

/**
  * the game scene's background music controller
  * for example, the background music will be played once per 30 second
  */
class GameSceneMusicController
{
public:
	enum {
		music_state_init = 0,
		music_state_playing = 1,
		music_state_wait = 2,
		music_state_prewait = 3,
	};

public:
	GameSceneMusicController();
	~GameSceneMusicController();

	void update(float dt);

private:
	void playSceneMusic();
	char* getRandomMusicFileName(char** name, int size);
private:
	int m_state;
	float m_stateStartTime;
	float m_stateInitTime;
};

#endif