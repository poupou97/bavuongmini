#include "MyPlayerSimpleAI.h"

#include "MyPlayer.h"
#include "MyPlayerOwnedCommand.h"
#include "../GameSceneState.h"
#include "PickingActor.h"
#include "GameView.h"
#include "MyPlayerAIConfig.h"
#include "MyPlayerAI.h"

MyPlayerSimpleAI::MyPlayerSimpleAI()
: m_pMyPlayer(NULL)
{

}

MyPlayerSimpleAI::~MyPlayerSimpleAI()
{

}

void MyPlayerSimpleAI::setMyPlayer(MyPlayer* self)
{
	m_pMyPlayer = self;
}

void MyPlayerSimpleAI::stopAll()
{
	stopKill();
	stopPick();
}

void MyPlayerSimpleAI::startKill(long long roleId)
{
	targetRoleId = roleId;
	// timer
	this->unschedule(schedule_selector(MyPlayerSimpleAI::updateKill));
	this->schedule( schedule_selector(MyPlayerSimpleAI::updateKill), 1.2f); 
}
void MyPlayerSimpleAI::stopKill()
{
	this->unschedule(schedule_selector(MyPlayerSimpleAI::updateKill));   // finished
}
void MyPlayerSimpleAI::updateKill(float dt)
{
	auto targetActor = dynamic_cast<BaseFighter*>(m_pMyPlayer->getGameScene()->getActor(targetRoleId));
	if(targetActor == NULL 
		|| m_pMyPlayer->getLockedActorId() != targetActor->getRoleId()
		|| targetActor->isAction(ACT_DIE))
	{
		stopKill();
		return;
	}
	else
	{
		// 自动杀敌的优先级低于正常的command
		if (MyPlayerAIConfig::getAutomaticSkill() == 1)//if robot is open ignor DefaultSkill command
		{
			//GameView::getInstance()->showAlertDialog("robot is null");
		}else
		{
			if(m_pMyPlayer->getNextCommand() == NULL)
			{
				auto cmd = new MyPlayerCommandAttack();
				cmd->skillId = m_pMyPlayer->getDefaultSkill();
				cmd->skillProcessorId = cmd->skillId;
				cmd->targetId = targetRoleId;
				//cmd->finishedConditionKill = true;

				m_pMyPlayer->setNextCommand(cmd);
			}
		}
	}
}

void MyPlayerSimpleAI::startPick(long long roleId)
{
	pickingRoleId = roleId;
	// timer
	this->unschedule(schedule_selector(MyPlayerSimpleAI::updatePick));
	this->schedule( schedule_selector(MyPlayerSimpleAI::updatePick), 0.5f); 
}
void MyPlayerSimpleAI::stopPick()
{
	this->unschedule(schedule_selector(MyPlayerSimpleAI::updatePick));   // finished
}
void MyPlayerSimpleAI::updatePick(float dt)
{
	if(m_pMyPlayer->getNextCommand() == NULL)
	{
		//if(!m_pMyPlayer->isAction(ACT_PICK))
		//{
		//	PickingActor* actor = dynamic_cast<PickingActor*>(m_pMyPlayer->getGameScene()->getActor(pickingRoleId));
		//	CCAssert(actor != NULL, "PickingActor should not be null");

		//	if (actor->getActivied())
		//	{
		//		m_pMyPlayer->setLockedActorId(pickingRoleId);

		//		MyPlayerCommandPick* cmd = new MyPlayerCommandPick();
		//		cmd->targetId = pickingRoleId;
		//		m_pMyPlayer->setNextCommand(cmd, true);
		//	}
		//}

		if(!m_pMyPlayer->isAction(ACT_PICK))
		{
			if(m_pMyPlayer->selectPickActor(380))
			{
				PickingActor* actor = dynamic_cast<PickingActor*>(m_pMyPlayer->getLockedActor());
				CCAssert(actor != NULL, "PickingActor should not be null");
				if (actor->getActivied())
				{
					MyPlayerCommandPick* cmd = new MyPlayerCommandPick();
					cmd->targetId = actor->getRoleId();
					m_pMyPlayer->setNextCommand(cmd, true);
				}
			}
		}
	}
}

void MyPlayerSimpleAI::update(float dt)
{

}