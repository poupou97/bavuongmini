#include "BaseFighter.h"

#include "ActorCommand.h"
#include "../GameSceneState.h"
#include "../../utils/GameUtils.h"
#include "../../utils/pathfinder/AStarPathFinder.h"
#include "../skill/SkillSeed.h"
#include "../skill/CSkillResult.h"
#include "../skill/GameFightSkill.h"
#include "../skill/SkillManager.h"
#include "../skill/processor/SkillProcessor.h"
#include "../skill/TargetScopeType.h"
#include "../exstatus/ExStatus.h"
#include "../../GameView.h"
#include "../SimpleEffectManager.h"
#include "GameActorAnimation.h"
#include "../../utils/StaticDataManager.h"
#include "../exstatus/ExStatusType.h"
#include "AppMacros.h"
#include "BaseFighterOwnedStates.h"
#include "BasePlayer.h"
#include "ActorUtils.h"
#include "GameView.h"
#include "MyPlayer.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../sceneelement/TargetInfoMini.h"
#include "BaseFighterConstant.h"
#include "GameActor.h"

#define BASEFIGHTER_DANGER_CIRCLE_DURATION 8.0f

#define HEAD_LABEL_TAGS_NUM 7
const int HEAD_LABEL_TAGS[HEAD_LABEL_TAGS_NUM] = {
	PLAYER_COUNTRY_ICON_TAG,
	PLAYER_FAMILY_NAME_TAG,
	PLAYER_VIP_FLAG_TAG,
	GENERAL_RANK_TAG,
	GENERAL_RARE_TAG,
	PLAYER_AUTO_MOVE_ANIMATION_TAG,
	PLAYER_AUTO_FIGHT_ANIMATION_TAG,
};

BaseFighter::BaseFighter()
: m_animName(0)
, m_pAnim(NULL)
, mCurrentCommand(NULL)
, mNextCommand(NULL)
, mActiveRole(NULL)
{
	m_pPaths = new vector<Vec2*>();

	// init all action contextes
	m_pAllActionContextes[ACT_STAND] = new ActionStandContext();
	m_pAllActionContextes[ACT_RUN] = NULL;
	m_pAllActionContextes[ACT_ATTACK] = new ActionAttackContext();
	m_pAllActionContextes[ACT_DIE] = new ActionDeathContext();
	m_pAllActionContextes[ACT_PICK] = NULL;
	m_pAllActionContextes[ACT_BE_KNOCKBACK] = new ActionBeKnockedBackContext();
	m_pAllActionContextes[ACT_BE_PULL] = NULL;
	m_pAllActionContextes[ACT_CHARGE] = new ActionChargeContext();
	m_pAllActionContextes[ACT_JUMP] = new ActionJumpContext();
	m_pAllActionContextes[ACT_DISAPPEAR] = new ActionDisappearContext();
}

BaseFighter::~BaseFighter()
{
	clear();

    delete m_pPaths;

	// release all fightskill
    for (std::map<std::string,GameFightSkill*>::iterator it = m_FightSkillMap.begin(); it != m_FightSkillMap.end(); ++it) 
	{
		CC_SAFE_DELETE(it->second);
	}

	CC_SAFE_DELETE(mCurrentCommand);
	CC_SAFE_DELETE(mNextCommand);

	removeAllExStatus();

	// delete all action contextes
	for(int i = 0; i < ACT_MAX_NUM; i++)
	{
		if(m_pAllActionContextes[i] == NULL)
			continue;

		delete m_pAllActionContextes[i];
	}
}

void BaseFighter::clear()
{
	clearPath();

	// clear locked info
	if(m_gameScene != NULL)
	{
		auto lockedOwner = m_gameScene->getActor(this->getLockedOwnerId());
		if(lockedOwner != NULL && lockedOwner->getLockedActorId() == this->getRoleId())
			lockedOwner->setLockedActorId(NULL_ROLE_ID);
	}
	setLockedActorId(NULL_ROLE_ID);
	setLockedOwnerId(NULL_ROLE_ID);
}

void BaseFighter::setReflection(bool enabled)
{
	// showComponent() include loading animation, so it should NOT be set every tick
	if(m_reflectionEnabled != enabled)
	{
		m_reflectionEnabled = enabled;

		if(m_pAnim == NULL)
			return;

		m_pAnim->showComponent(ANI_COMPONENT_REFLECTION, m_reflectionEnabled);
		// 倒影
		if(m_reflectionEnabled)
		{
			for(int i = 0; i < m_pAnim->getAnimSize(); i++)
			{
				auto pAnim = m_pAnim->getAnim(ANI_COMPONENT_REFLECTION, i);
				if(pAnim != NULL)
				{
					pAnim->setScaleY(-0.9f);
					pAnim->setOpacity(128);   // 半透，模拟倒影效果
				}
			}

			m_pAnim->showComponent(ANI_COMPONENT_SHADOW, false);
		}
		else
		{
			m_pAnim->showComponent(ANI_COMPONENT_SHADOW, true);
		}
	}

	//CCLegendAnimation* bodyReflection = this->m_pAnim->getAnim(ANI_COMPONENT_REFLECTION, m_animName);   // 倒影组件
	//if(bodyReflection != NULL)
	//{
	//	if(m_reflectionEnabled)
	//		bodyReflection->setVisible(true);
	//	else
	//		bodyReflection->setVisible(false);
	//}
}

void BaseFighter::removeAllExStatus()
{
	for (std::vector<ExStatus*>::iterator it = m_ExStatusVector.begin(); it != m_ExStatusVector.end(); ++it) 
	{
		CC_SAFE_DELETE(*it);
	}
	m_ExStatusVector.clear();
}
bool BaseFighter::hasExStatus(int type)
{
	for (std::vector<ExStatus*>::iterator it = m_ExStatusVector.begin(); it != m_ExStatusVector.end(); ++it) 
	{
		auto status = *it;
		if(status->type() == type)
			return true;
	}
	return false;
}

void BaseFighter::updateCoverStatus()
{
	if(!hasExStatus(ExStatusType::invisible))
		GameActor::updateCoverStatus();
}

void BaseFighter::running(float dt)
{
	Vec2 offset;
	// 检测移动目标点是否存在
	if(getPaths()->size() > 0 && isAction(ACT_RUN))
	{
		//float speed = 180.0f;
		float speed = getMoveSpeed();
		float distance = speed * dt;

		// get direction by actor's current position and target pos
		offset = GameUtils::getDirection(getWorldPosition(), *(getPaths()->at(0)));
		offset = offset*distance;

		setMoveDir(Vec2(offset.x, -offset.y));
		//CCLOG("move dir: %f, %f", m_pMyPlayer->getMoveDir());

		// 每走一步，都检测是否超过了终点
		bool bReached = false;   // 是否到达终点
		if(offset.getLength() >= getWorldPosition().getDistance(*(getPaths()->at(0))))
		{
			bReached = true;
			offset = *(getPaths()->at(0)) - getWorldPosition();
		}

		// 检测是否碰到了障碍
		int targetX = getWorldPosition().x + offset.x;
		int targetY = getWorldPosition().y + offset.y;
		int targetTileX = this->getGameScene()->positionToTileX(targetX);
		int targetTileY = this->getGameScene()->positionToTileY(targetY);
		// collide, slide or stop
		bool bCollide = false;
		if(this->getGameScene()->isLimitOnGround(targetTileX, targetTileY))
		{
			bCollide = true;
			// slide on x
			if(!this->getGameScene()->isLimitOnGround(targetTileX, this->getGameScene()->positionToTileY(getWorldPosition().y)))
			{
				offset.y = 0;
			}
			// slide on y
			else if(!this->getGameScene()->isLimitOnGround(this->getGameScene()->positionToTileX(getWorldPosition().x), targetTileY))
			{
				offset.x = 0;
			}
			// stop
			else
			{
				offset.x = 0;
				offset.y = 0;
			}
		}

		// m_pMyPlayer->getWorldPosition().getDistance(*(m_pMyPlayer->getPaths()->at(0)))
		//if(m_pMyPlayer->mMovedDistance + offset.getLength() > m_pMyPlayer->getPathLength())
		//{
		//	offset = ccpSub(*(m_pMyPlayer->getPaths()->at(0)), m_pMyPlayer->getWorldPosition());
		//}

		//mMovedDistance += offset.getLength();   // 记录里程
		//if(fabs((mMovedDistance-getPathLength())) < 3)
		//{

		//}



		setWorldPosition(Vec2(getWorldPosition().x + offset.x, getWorldPosition().y + offset.y));

		//if(m_pMyPlayer->getPaths()->size() == 1)
		if(bCollide || bReached)
		{
			vector<Vec2*>::iterator iter = getPaths()->begin() + 0;
			Vec2* pRemovedPoint = *iter;
			getPaths()->erase(iter);
			delete pRemovedPoint;
		}
	}
}

void BaseFighter::checkAttack()
{
}

void BaseFighter::checkPick()
{
}

void BaseFighter::searchPath(Vec2 target)
{
	auto scene = this->m_gameScene;

	// start point
	short start[2] = {0, 0};   // tileY, tileX
	short tileX = scene->positionToTileX(getWorldPosition().x);
	short tileY = scene->positionToTileY(getWorldPosition().y);
	start[0] = tileY;
	start[1] = tileX;
	// out of map, ignore
	if(!scene->checkInMap(tileX, tileY))
	{
		clearPath();
		return;
	}

	// end point
	short end[2] = {0, 0};
	tileX = scene->positionToTileX(target.x);
	tileY = scene->positionToTileY(target.y);
	end[0] = tileY;
	end[1] = tileX;
	// out of map, ignore
	if(!scene->checkInMap(tileX, tileY))
	{
		clearPath();
		return;
	}

	// 判断目的点和角色之间是否有阻挡，若无，则直线移动；若有，则A*寻路
	if(scene->isLineIntersectBarrier(getWorldPosition().x, getWorldPosition().y, target.x, target.y))
	{
		std::vector<short*> *path = scene->getPathFinder()->searchPath(start, end);
		if(path != NULL)
		{
			clearPath();
			vector<short*>::iterator iter;
			for (iter = path->begin(); iter != path->end(); ++iter)
			{
				short x = (*iter)[1];
				short y = (*iter)[0];
				getPaths()->push_back(new Vec2(scene->tileToPositionX(x), scene->tileToPositionY(y)));
			}
			delete path;
		}
	}
	else
	{
		// if the same point, ignore
		if(!getWorldPosition().equals(target))
		{
			clearPath();
			getPaths()->push_back(new Vec2(target.x, target.y));
		}
	}

	// 浼樺寲瀵昏矾鑺傜偣锛屽敖閲忔秷闄ら敮榻垮瀷璺嚎
	if(getPaths()->size() >= 2)
	{
		vector<Vec2*>::iterator path_iter = getPaths()->begin() + 1;
		Vec2* pathPoint = *path_iter;
		// 浠庤鑹茬殑浣嶇疆鍚戝悇涓妭鐐硅瀵?鐩寸嚎瑙傚療)锛岃嫢鏈夐殰纰嶇墿锛屽垯鏃犳硶浼樺寲; 鑻ユ病鏈夌鍒伴殰纰嶇墿锛屽垯绉婚櫎鍓嶄竴涓妭鐐?
		if(!scene->isLineIntersectBarrier(m_worldPosition.x, m_worldPosition.y, pathPoint->x, pathPoint->y))
		{
			vector<Vec2*>::iterator previous_path_iter = getPaths()->begin();
			Vec2* pRemovedPoint = *previous_path_iter;
			getPaths()->erase(previous_path_iter);
			delete pRemovedPoint;
		}

		//vector<Vec2*>::iterator path_iter;
		//for (path_iter = getPaths()->begin() + 1; path_iter != getPaths()->end(); ++path_iter)
		//{
		//	Vec2* pathPoint = *path_iter;
		//	// 浠庤鑹茬殑浣嶇疆鍚戝悇涓妭鐐硅瀵?鐩寸嚎瑙傚療)锛岃嫢鏈夐殰纰嶇墿锛屽垯鏃犳硶浼樺寲; 鑻ユ病鏈夌鍒伴殰纰嶇墿锛屽垯绉婚櫎璇ヨ妭鐐?
		//	if(scene->isLineIntersectBarrier(m_worldPosition.x, m_worldPosition.y, pathPoint->x, pathPoint->y))
		//	{
		//		break;
		//	}
		//	//else
		//	//{
		//	//	Vec2* pRemovedPoint = *path_iter;
		//	//	getPaths()->erase(path_iter);
		//	//	delete pRemovedPoint;
		//	//}
		//}

		// code is not finished!
		//vector<Vec2*>::iterator path_iter = getPaths()->begin();
		//Vec2* firstPoint = new Vec2((*path_iter)->x, (*path_iter)->y);   // cache first point
		//while (path_iter != getPaths()->end())
		//{
		//	Vec2* pathPoint = *path_iter;
		//	// 浠庤鑹茬殑浣嶇疆鍚戝悇涓妭鐐硅瀵?鐩寸嚎瑙傚療)锛岃嫢鏈夐殰纰嶇墿锛屽垯鏃犳硶浼樺寲; 鑻ユ病鏈夌鍒伴殰纰嶇墿锛屽垯绉婚櫎璇ヨ妭鐐?
		//	if(scene->isLineIntersectBarrier(m_worldPosition.x, m_worldPosition.y, pathPoint->x, pathPoint->y))
		//	{
		//		break;
		//	}
		//	else
		//	{
		//		Vec2* pRemovedPoint = *path_iter;
		//		getPaths()->erase(path_iter);
		//		delete pRemovedPoint;
		//	}
		//	//path_iter++;
		//	path_iter = getPaths()->begin();
		//}
		//getPaths()->insert(getPaths()->begin(), firstPoint);   // add the first point again
	}
}

// 清除剩余路径点，停止移动
void BaseFighter::clearPath()
{
	if(m_pPaths != NULL)
	{
		vector<Vec2*>::iterator iter;
		for (iter = m_pPaths->begin(); iter != m_pPaths->end(); ++iter)
		{
			delete *iter;
		}
		m_pPaths->clear();
	}
}

void BaseFighter::changeAnimation(int animName, int dir, bool bLoop, bool bPlay)
{
	setAnimName(animName);

	// set GameActorAnimation
	m_pAnim->setAnimName(animName);
	m_pAnim->setAction(dir);
	m_pAnim->setPlayLoop(bLoop);
	if(bPlay)
		m_pAnim->play();
	else
		m_pAnim->stop();

	// 水中倒影
	if(this->isReflection())
	{
		m_pAnim->showComponent(ANI_COMPONENT_REFLECTION, isReflection());
		auto pAnim = m_pAnim->getAnim(ANI_COMPONENT_REFLECTION, animName);
		if(pAnim != NULL)
		{
			pAnim->setScaleY(-0.9f);
			pAnim->setOpacity(96);   // 半透，模拟倒影效果
		}

		m_pAnim->showComponent(ANI_COMPONENT_SHADOW, false);
	}
	/**
	else   // no water, has shadow
	{
		// 阴影
		m_pAnim->showComponent(ANI_COMPONENT_SHADOW, true);
		CCLegendAnimation* pAnim = m_pAnim->getAnim(ANI_COMPONENT_SHADOW, animName);
		if(pAnim != NULL)
		{
			#define BASEFIGHTER_SHADOW_ROTATION 15.f
			#define BASEFIGHTER_SHADOW_SKEW_X 35.f

			// 动画是否带有X翻转，若有X翻转，要正确的处理阴影的绘制方向
			bool bFlipX = (m_pAnim->getScaleX() < 0) ? true : false;
			if(bFlipX)
			{
				pAnim->setRotation(-1 * BASEFIGHTER_SHADOW_ROTATION);
				pAnim->setSkewX(-1 * BASEFIGHTER_SHADOW_SKEW_X);
			}
			else
			{
				pAnim->setRotation(BASEFIGHTER_SHADOW_ROTATION);
				pAnim->setSkewX(BASEFIGHTER_SHADOW_SKEW_X);
			}
			pAnim->setScaleX(1.3f);
			pAnim->setScaleY(0.5f);

			pAnim->setColor(ccBLACK);
			pAnim->setOpacity(80);
		}
	}
	**/
}

void BaseFighter::setAnimName(int animName)
{
	m_animName = animName;
}
int BaseFighter::getAnimName()
{
	return m_animName;
}

void BaseFighter::setAnimDir(int dir)
{
	//CCAssert(dir != -1, "invalid anim dir");
	if(dir >= 0 && dir < ANIM_DIR_MAX)
		m_animDirection = dir;
}

ActionContext* BaseFighter::getActionContext(int action)
{
	CCAssert(m_pAllActionContextes[action] != NULL, "should not be nil");
	return m_pAllActionContextes[action];
}

bool BaseFighter::isDead()
{
	return false;
}

void BaseFighter::setCommand(ActorCommand* command)
{
	stopCurrentCommand();

	if(command != NULL)
		command->Enter(this);
	mCurrentCommand = command;
}
ActorCommand* BaseFighter::getCommand()
{
	return mCurrentCommand;
}
void BaseFighter::setNextCommand(ActorCommand* command, bool bFinishedPreviousImmediately, bool bWaitingAction)
{
	if(command == NULL)
		return;

	// delete the command in the command pool, now the command pool size is only 1
	if(mNextCommand != NULL)
	{
		CCAssert(mNextCommand != mCurrentCommand, "next one should not be the same as the current one");
		CC_SAFE_DELETE(mNextCommand);
	}

	// 死亡状态，不再接收新的指令
	// 一些异常状态，也不接收新的指令
	if(isDead() 
		|| (isAction(ACT_BE_KNOCKBACK) && command->getType() != ActorCommand::type_attack)
		//|| isAction(ACT_CHARGE)
		|| hasExStatus(ExStatusType::vertigo))
		return;

	if(mCurrentCommand != NULL)
	{
		//if(command->isSame(mCurrentCommand))
		//{
		//	delete command;
		//	return;
		//}

		mCurrentCommand->isFinishImmediately = bFinishedPreviousImmediately;
		mCurrentCommand->isWaitingAction = bWaitingAction;
		if(mCurrentCommand->isFinishImmediately && !mCurrentCommand->isWaitingAction)
		{
			mCurrentCommand->setStatus(ActorCommand::status_finished);
		}
	}

	mNextCommand = command;
}
ActorCommand* BaseFighter::getNextCommand()
{
	return mNextCommand;
}

void BaseFighter::drive()
{
	// check next command
	if(mNextCommand != NULL)
	{
		if(getCommand() == NULL || getCommand()->isFinished())
		{
			// in some special actions, the command will not be switched, wait a second
			if( isAction(ACT_CHARGE) || isAction(ACT_JUMP)   )
				return;

			setCommand(mNextCommand);
			mNextCommand = NULL;
		}
	}

	// 无指令
	if(getCommand() == NULL)
		return;

	getCommand()->Execute(this);

	// command is finshed, destroy it
	if(getCommand()->isFinished())
		setCommand(NULL);
}

void BaseFighter::stopAllCommand()
{
	stopCurrentCommand();
	CC_SAFE_DELETE(mNextCommand);
}

void BaseFighter::stopCurrentCommand()
{
	if(mCurrentCommand != NULL)
	{
		// 当前指令销毁时，若其hasNextCommand()，且actor没有nextCommand，则将其设置为actor的nextCommand
		// 即actor的nextCommand优先级高于当前command的nextcommand
		if(mNextCommand == NULL)
		{
			if(mCurrentCommand->hasNextCommand())
				mNextCommand = mCurrentCommand->getNextCommand()->clone();
		}
		mCurrentCommand->Exit(this);
	}
	CC_SAFE_DELETE(mCurrentCommand);
}

GameFightSkill* BaseFighter::getGameFightSkill(const char* skillId, const char* skillProcessorId)
{
	if(strcmp(skillId, "") == 0)
		return NULL;

	if(m_FightSkillMap[skillId] == NULL)
	{
		auto newSkill = new GameFightSkill();
		newSkill->initSkill(skillId, skillProcessorId, 1, this->getType());

		m_FightSkillMap[skillId] = newSkill;
	}

	return m_FightSkillMap[skillId];
}

SkillSeed* BaseFighter::makeSkillSeed(const char* skillId, const char* skillProcessorId, BaseFighter* target, int x, int y)
{
	// init skill releaes position
	int fightSkillX = x;
	int fightSkillY = y;
	char targetScopeType = this->getGameFightSkill(skillId, skillProcessorId)->getTargetScopeType();
	if(!GameFightSkill::isNoneLock(targetScopeType))
	{
		if(target != NULL)
		{
			fightSkillX = target->getWorldPosition().x;
			fightSkillY = target->getWorldPosition().y;
		}
	}
	else
	{
		fightSkillX = this->getWorldPosition().x;
		fightSkillY = this->getWorldPosition().y;
	}

	// create skill processor
	auto skillSeed = new SkillSeed();
	skillSeed->skillId = skillId;
	skillSeed->skillProcessorId = skillProcessorId;
	skillSeed->attacker = this->getRoleId();
	skillSeed->defender = 0;
	if (target != NULL) {
		skillSeed->defender = target->getRoleId();
	}
	skillSeed->x = fightSkillX;
	skillSeed->y = fightSkillY;

	return skillSeed;
}

void BaseFighter::onSkillBegan(const char* skillId, const char* skillProcessorId, BaseFighter* target, short x, short y) {
	auto sp = SkillManager::createSkillProcessor(skillProcessorId);
	CCAssert(sp != NULL, "skill processor should not be null");

	auto skillSeed = makeSkillSeed(skillId, skillProcessorId, target, x, y);
	sp->setSkillSeed(skillSeed);
	sp->onSkillBegan(this);
	delete sp;
}

void BaseFighter::onSkillReleased(const char* skillId, const char* skillProcessorId, BaseFighter* target, short x, short y) 
{
	auto skillSeed = makeSkillSeed(skillId, skillProcessorId, target, x, y);
	if(skillSeed == NULL)
	{
		// do nothing
		return;
	}
	auto sp = m_gameScene->getSkillMgr()->addSkillProcessor(skillSeed);
	sp->onSkillReleased(this);

	// play skill sound effect
	auto pNode = this->getChildByTag(GameActor::kTagDangerCircle);
	std::vector<long long> playerActor;
	GameView::getInstance()->getGameScene()->getTypeActor(playerActor, GameActor::type_player);
	if(this->getRoleId() == getMyPlayerId()
		||GameView::getInstance()->myplayer->isTeammateOf(this->getRoleId())
		)
	{
		if(StaticDataSkillBin::s_data[skillProcessorId] != NULL)
			GameUtils::playGameSound(StaticDataSkillBin::s_data[skillProcessorId]->sound.c_str(), 2, false);
	}
	else if(this->isMyPlayerGroup()
		||pNode != NULL)
	{
		if(StaticDataSkillBin::s_data[skillProcessorId] != NULL)
		{
			if(memcmp(skillProcessorId ,"WarriorDefaultAttack", 20)
				&&memcmp(skillProcessorId, "MagicDefaultAttack", 18)
				&&memcmp(skillProcessorId, "WarlockDefaultAttack", 20)
				&&memcmp(skillProcessorId, "RangerDefaultAttack", 19))
			{
				GameUtils::playGameSound(StaticDataSkillBin::s_data[skillProcessorId]->sound.c_str(), 2, false);
			}
		}
	}
	else if(playerActor.size()>0)
	{
		for (std::vector<long long>::iterator it = playerActor.begin(); it != playerActor.end(); )
		{
			auto pActor = GameView::getInstance()->getGameScene()->getActor(*it);
			if(pActor != NULL
				&&pActor->getRoleId() == this->getRoleId())
			{
				if(memcmp(skillProcessorId ,"WarriorDefaultAttack", 20)
				&&memcmp(skillProcessorId, "MagicDefaultAttack", 18)
				&&memcmp(skillProcessorId, "WarlockDefaultAttack", 20)
				&&memcmp(skillProcessorId, "RangerDefaultAttack", 19))
				{
					GameUtils::playGameSound(StaticDataSkillBin::s_data[skillProcessorId]->sound.c_str(), 2, false);
				}
				break;
			}
			++it;
		}
	}
}

void BaseFighter::onSkillApplied(const char* skillId, CSkillResult* result)
{
	auto sp = m_gameScene->getSkillMgr()->addSkillProcessor(result);
	sp->onSkillApplied(this);
}

void BaseFighter::addEffect(Node* effectNode, bool bGround, int offset, bool bLoop)
{
	effectNode->setPosition(Vec2(0, offset));

	Node* container = NULL;
	if(bGround)
		container = this->getChildByTag(BaseFighter::kTagFootEffectContainer);
	else
		container = this->getChildByTag(BaseFighter::kTagHeadEffectContainer);

	container->addChild(effectNode);
}

void BaseFighter::addChatBubble(const char* text, float duration, int offsetY)
{
	auto bubble = Node::create();
	bubble->setPosition(Vec2(0,0));
	bubble->setAnchorPoint(Vec2(0.5f,0.f));

	const int MARGINS = 8;
	const int FONT_SIZE = 16;

	//const int MAX_WIDTH = 131;
	//// first, try to create a label; then check its width
	//Label* label = Label::createWithTTF(text, APP_FONT_NAME, FONT_SIZE);
	//if(label->getContentSize().width > MAX_WIDTH)
	//{
	//	label->release();
	//	label = Label::createWithTTF(text, APP_FONT_NAME, FONT_SIZE, Size(MAX_WIDTH, 0), TextHAlignment::LEFT);
	//}
	const int MAX_WIDTH = 141;
	auto label = CCRichLabel::createWithString(text,Size(MAX_WIDTH,20),NULL,NULL,0,FONT_SIZE,4);
	label->setAnchorPoint(Vec2(0.5f, 0));
	label->setPosition( Vec2(0, MARGINS) );
	bubble->addChild(label, 1);

	// then, according the above label to set background's size 
	Size s = Director::getInstance()->getVisibleSize();
    Rect insetRect = Rect(11,11,1,1);
	auto backGround = cocos2d::extension::Scale9Sprite::create("res_ui/dialog.png");
	backGround->setCapInsets(insetRect);
	backGround->setPreferredSize(Size(label->getContentSize().width + MARGINS*2, label->getContentSize().height + MARGINS*2));   //  Size(256, 50)
	//backGround->setPosition(Vec2(s.width/2, s.height/2));
    backGround->setAnchorPoint(Vec2(0.5f, 0.f));
	//backGround->setOpacity(200);
    bubble->addChild(backGround, 0);
	bubble->setTag(GameActor::kTagChatBubble);

	auto action = (ActionInterval*)Sequence::create
        (
            Show::create(),
			DelayTime::create(duration),
			RemoveSelf::create(),
            NULL
        );
	bubble->runAction(action);

	addEffect(bubble, false, offsetY);
}

void BaseFighter::addShadow()
{
	auto shadow = Sprite::create("res_ui/shadow.png");
	shadow->setAnchorPoint(Vec2(0.5f, 0.5f));
#if BASEFIGHTER_SHADOW_ENABLED
	shadow->setOpacity(255);
#endif
	//shadow->setScale(ACTOR_ANIM_SCALE);
	addChild(shadow, GameActor::ACTOR_SHADOW_ZORDER, GameActor::kTagActorShadow);
}

void BaseFighter::synchronizeHeadLabelPosition()
{
	auto nameLabel = this->getChildByTag(GameActor::kTagActorName);
	if(nameLabel != NULL)
	{
		for(int i = 0; i < HEAD_LABEL_TAGS_NUM; i++)
		{
			int tagId = HEAD_LABEL_TAGS[i];
			auto pNode = this->getChildByTag(tagId);
			if(pNode != NULL)
				//pNode->setPosition(getHeadLabelPosition(tagId));
				pNode->setPositionY(getHeadLabelPosition(tagId).y);
		}
	}
}
Vec2 BaseFighter::getHeadLabelPosition(int labelType)
{
	auto mainNameLabel = this->getChildByTag(GameActor::kTagActorName);
	if(mainNameLabel == NULL)
		return Vec2(0,0);

	float mainNameLabelY = mainNameLabel->getPositionY();
	Vec2 result = Vec2(0, 0);
	if(labelType == PLAYER_FAMILY_NAME_TAG)
	{
		auto pNode = this->getChildByTag(labelType);
		if(pNode != NULL)
			result.y = mainNameLabelY + pNode->getContentSize().height;
	}
	else
	{
		result.y = mainNameLabelY;
	}

	return result;
}

static long long s_MyPlayerId = NULL_ROLE_ID;
void BaseFighter::setMyPlayerId(long long myId)
{
	s_MyPlayerId = myId;
}
long long BaseFighter::getMyPlayerId()
{
	return s_MyPlayerId;
}

bool BaseFighter::isMyPlayer()
{
	return false;
}
bool BaseFighter::isMyPlayerGroup()
{
	return false;
}
int BaseFighter::getPosInGroup()
{
	return -1;
}

void BaseFighter::onDamaged(BaseFighter* source, int damageNum, bool bDoubleAttacked, CSkillResult* skillResult)
{
	// 头顶数字效果
	if( (source != NULL && source->isMyPlayerGroup()) || this->isMyPlayerGroup())
	{
		char str[10];
		sprintf(str,"%d", damageNum);
		
		if(this->isMyPlayerGroup())   // 己方受到伤害
		{
			if(!bDoubleAttacked)
			{
				std::string text = "";
				text.append("-");
				text.append(str);
				auto label = Label::createWithBMFont("res_ui/font/b123.fnt", text.c_str());   // red color
				label->setAnchorPoint(Vec2(0.5f,0.0f));
				label->setScale(1.3f);
				label->setVisible(false);

				//SimpleEffectManager::applyEffectAction(label, SimpleEffectManager::EFFECT_FLYINGUP);
				float randomTime = CCRANDOM_0_1() / 2.5f;   // import random time delay to avoid overlay
				auto  action = Sequence::create(
					DelayTime::create(randomTime),
					Show::create(),
					MoveBy::create(0.25f, Vec2(30, -10)),
					MoveBy::create(0.25f, Vec2(15, -10)),
					MoveBy::create(0.25f, Vec2(15, -20)),
					MoveBy::create(0.5f, Vec2(10, -40)),
					RemoveSelf::create(),
					NULL);
				label->runAction(action);

				//this->addEffect(label, false, 80/*BASEFIGHTER_ROLE_HEIGHT*/);
				label->setPosition(Vec2(this->getPosition().x, this->getPosition().y + BASEFIGHTER_ROLE_HEIGHT/4));
				this->getGameScene()->getActorLayer()->addChild(label, SCENE_TOP_LAYER_BASE_ZORDER + this->getGameScene()->getMapSize().height);
			}
			else
			{
				ActorUtils::addDoubleAttackDamageNumber(this, damageNum, "res_ui/font/b123.fnt");   // red color
			}
		}
		else   // 己方造成的伤害
		{
			// 这里只处理普通攻击的伤害数字，后面有技能伤害数字的处理
			if(skillResult == NULL 
				|| (skillResult != NULL && BasePlayer::isDefaultAttack(skillResult->showeffect().c_str())) )
			{
				if(!bDoubleAttacked)
				{
					auto label = Label::createWithBMFont("res_ui/font/c123.fnt", str);   // yellow color
					label->setScale(0.7f);
					SimpleEffectManager::applyEffectAction(label, SimpleEffectManager::EFFECT_PUTONG);
					label->setPosition(Vec2(this->getPosition().x, this->getPosition().y + BASEFIGHTER_ROLE_HEIGHT/4));
					this->getGameScene()->getActorLayer()->addChild(label, SCENE_TOP_LAYER_BASE_ZORDER + this->getGameScene()->getMapSize().height);

					//ActorUtils::addMultiAttackDamageNumber(this, damageNum, 1);
				}
				else   // 暴击Action
				{
					ActorUtils::addDoubleAttackDamageNumber(this, damageNum, "res_ui/font/c123.fnt");   // yellow color
				}
			}
		}
	}

	// 己方造成的伤害 ( 使用技能 )
	if( (source != NULL && source->isMyPlayerGroup()))
	{
		if(skillResult != NULL && !BasePlayer::isDefaultAttack(skillResult->showeffect().c_str()))
		{
			// in most situation, the attacktime is 1, few skills need more attacktime
			int attacktime = 1;
			// get attack time from client skill config
			std::string skillId = skillResult->showeffect();
			if(StaticDataSkillBin::s_data[skillId] != NULL) {
				attacktime = StaticDataSkillBin::s_data[skillId]->attack_time;
			}
			// get attack time from the skill result
			if(attacktime == 1)
			{
				attacktime = skillResult->attacktime();
				if(attacktime == 0)
					attacktime = 1;
			}

			ActorUtils::addMultiAttackDamageNumber(this, damageNum, attacktime);
		}
	}

	// 公共受击特效
	auto effect = CCLegendAnimation::create("animation/texiao/renwutexiao/GJ-SJ/SJ01/sj_common.anm");
	//effect->setOpacity(200);
	//effect->setScale(1.0f);
	this->addEffect(effect, false, 0/*BASEFIGHTER_ROLE_HEIGHT/3*/);

	// 附加受击晃动action
	if(this->isMyPlayerGroup() && !this->isAction(ACT_DIE))
	{
		SkillProcessor::shake(this, false, 4, 6.0f);
	}

	// 打断采集
	if(isAction(ACT_PICK))
	{
		// only the player and his generals can stop the picking action
		if(source != NULL 
			&& (source->getType() == GameActor::type_player || source->getType() == GameActor::type_pet))
		{
			changeAction(ACT_STAND);
		}
	}

	// 由于伤害由我方造成，为其增加危险光圈
	if(source != NULL && source->isMyPlayerGroup() && !this->isMyPlayerGroup())
	{
		this->addDangerCircle();
	}
}

void BaseFighter::onHealed(BaseFighter* source, int number)
{
	// show heal number on the head
	if (number > 0) {
		if(this->isMyPlayerGroup() || (source != NULL && source->isMyPlayerGroup())) {
			// the text on the head
			char str[10];
			sprintf(str,"%d",number);

			std::string healText = "+";
			healText.append(str);
			auto label = Label::createWithBMFont("res_ui/font/a123.fnt", healText.c_str());
			//label->setScale(0.8f);
			SimpleEffectManager::applyEffectAction(label, SimpleEffectManager::EFFECT_FLYINGUP);

			addEffect(label, false, BASEFIGHTER_ROLE_HEIGHT/2);

			// the special effect on the body
			CCLegendAnimation* effect = CCLegendAnimation::create("animation/texiao/renwutexiao/HMX/hmx.anm");
			this->addEffect(effect, false, 0);
		}
	}
}

void BaseFighter::onMPAdded(BaseFighter* source, int number)
{
	char str[10];
	std::string mpMumberText;
	// show mp number on the head
	if (number > 0) {
		if(this->isMyPlayer() || (source != NULL && source->isMyPlayer())) {
			sprintf(str,"%d",number);
			mpMumberText = "+";
			mpMumberText.append(str);
		}
	}
	else if(number < 0)
	{
		// 比如，被抽蓝
		if(!this->isMyPlayer() && source != NULL && source->isMyPlayer())
		{
			sprintf(str,"%d",number);
			//mpMumberText = "+";
			mpMumberText.append(str);
		}
	}

	if(mpMumberText.size() > 0)
	{
		auto label = Label::createWithBMFont("res_ui/font/d123.fnt", mpMumberText.c_str());
		//label->setScale(0.8f);
		SimpleEffectManager::applyEffectAction(label, SimpleEffectManager::EFFECT_FLYINGUP);

		addEffect(label, false, BASEFIGHTER_ROLE_HEIGHT);

		// the special effect on the body
		auto effect = CCLegendAnimation::create("animation/texiao/renwutexiao/HMX/hm.anm");
		this->addEffect(effect, false, 0);
	}
}

void BaseFighter::onAttack(const char* skillId, const char* skillProcessorId, long long targetId, bool bDoubleAttack, int skillType)
{
	// 攻击我方，为其增加危险光圈
	auto pActor = dynamic_cast<BaseFighter*>(this->getGameScene()->getActor(targetId));
	if(pActor != NULL && pActor->isMyPlayerGroup() && !this->isMyPlayerGroup())
	{
		this->addDangerCircle();
	}
}

void BaseFighter::onGotItem(std::string itemIcon)
{
}

void BaseFighter::addDangerCircle()
{
	if(this->hasExStatus(ExStatusType::invisible))
		return;

	// the red circle at the foot
	auto pNode = this->getChildByTag(kTagDangerCircle);
	if(pNode == NULL)
	{
		auto pCircleNode = CCLegendAnimation::create("animation/texiao/changjingtexiao/GSGB_a_b/gsgb_e.anm");
		pCircleNode->setOpacity(64);
		pCircleNode->setPlayLoop(true);
		pCircleNode->setReleaseWhenStop(false);
		addChild(pCircleNode, ACTOR_FOOTFLAG_ZORDER, kTagDangerCircle);

		auto action = Sequence::create(
			DelayTime::create(BASEFIGHTER_DANGER_CIRCLE_DURATION),
			CallFunc::create(CC_CALLBACK_0(BaseFighter::removeDangerCircle, this)),
			NULL);
		pCircleNode->runAction(action);
	}
	// refresh action
	if(pNode != NULL)
	{
		pNode->stopAllActions();
		auto action = Sequence::create(
			DelayTime::create(BASEFIGHTER_DANGER_CIRCLE_DURATION),
			CallFunc::create(CC_CALLBACK_0(BaseFighter::removeDangerCircle, this)),
			NULL);
		pNode->runAction(action);
	}

	// the blood bar on the head
	auto pBloodBar = (TargetInfoMini*)this->getChildByTag(kTagBloodBar);
	if(pBloodBar == NULL)
	{
		if(this->getRoleId() == m_gameScene->getActor(BaseFighter::getMyPlayerId())->getLockedActorId())
		{
			pBloodBar = TargetInfoMini::create(this);
			pBloodBar->setAnchorPoint(Vec2(0.5f, 0.5f));
			pBloodBar->setPosition(Vec2(0, 107));
			addChild(pBloodBar, ACTOR_HEAD_ZORDER, kTagBloodBar);
		}
	}
	// refresh
	if(pBloodBar != NULL)
	{
		// refresh action
		pBloodBar->stopAllActions();
		auto action = Sequence::create(
			DelayTime::create(BASEFIGHTER_DANGER_CIRCLE_DURATION),
			CallFunc::create(CC_CALLBACK_0(BaseFighter::removeDangerCircle, this)),
			NULL);
		pBloodBar->runAction(action);

		// refresh info
		pBloodBar->ReloadTargetData(this);
	}

	// reset the postions -- NAME label
	if(pBloodBar != NULL)
	{
		auto nameLabel = this->getChildByTag(GameActor::kTagActorName);
		if(nameLabel != NULL)
		{
			nameLabel->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT + 16));

			this->synchronizeHeadLabelPosition();
		}
	}
}
void BaseFighter::removeDangerCircle()
{
	// remove
	auto pNode = this->getChildByTag(kTagDangerCircle);
	if(pNode != NULL)
		pNode->removeFromParent();
	pNode = this->getChildByTag(kTagBloodBar);
	if(pNode != NULL)
		pNode->removeFromParent();

	// reset the postions
	// NAME label
	auto nameLabel = this->getChildByTag(GameActor::kTagActorName);
	if(nameLabel != NULL)
	{
		nameLabel->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT));

		this->synchronizeHeadLabelPosition();
	}
}

