#ifndef _LEGEND_GAMEACTOR_H_
#define _LEGEND_GAMEACTOR_H_

#include "cocos2d.h"
#include "StateMachine.h"
#include "../../legend_engine/LegendLevel.h"

USING_NS_CC;
using namespace std;

// animation's direction
//#define ANIM_DIR_UP			0
//#define ANIM_DIR_LEFT_UP	1
//#define ANIM_DIR_LEFT		2
//#define ANIM_DIR_LEFT_DOWN  3
//#define ANIM_DIR_DOWN		4
//#define ANIM_DIR_RIGHT_DOWN 5
//#define ANIM_DIR_RIGHT		6
//#define ANIM_DIR_RIGHT_UP	7

#define ANIM_DIR_UP			0
#define ANIM_DIR_LEFT			1
#define ANIM_DIR_DOWN		2
#define ANIM_DIR_RIGHT		3
#define ANIM_DIR_MAX 4

#define GAMEACTOR_NAME_FONT_SIZE 20

extern const float DEFAULT_ANGLES_DIRECTION_DATA[]; // 声明全局变量

class GameSceneLayer;
class CCLegendAnimation;

/**
 * 游戏场景中的actor的基类
 * @author zhaogang
 * @version 0.1.0
 */
class GameActor : public Node
{
public:
	// actor类型，玩家、NPC、怪物等
	enum Type
    {
		type_player		= 0,   // 玩家, the same as server
		type_monster	= 1,   // 怪物, the same as server
		type_pet		= 2,   // 宠物, the same as server
		type_npc		= 3,   // NPC
		type_summoned	= 4,   // 宠物
        type_shiwei		= 5,   // 侍卫
		type_picking    = 6,   // 采集物
		type_presentbox = 7,   // 宝箱
		type_other		= 8,
    };

	// actor type Mask, the value must be from above Type
	enum TypeMask
    {
		type_player_mask	= 1 << type_player,   // 玩家
		type_monster_mask	= 1 << type_monster,   // 怪物
		type_pet_mask		= 1 << type_pet,   // 宠物
		type_npc_mask		= 1 << type_npc,   // NPC
		type_summoned_mask	= 1 << type_summoned,   // 宠物
        type_shiwei_mask	= 1 << type_shiwei,   // 侍卫
		type_picking_mask   = 1 << type_picking,   // 采集物
		type_presentbox_mask= 1 << type_presentbox,   // 宝箱
		type_other_mask		= 1 << type_other,
    };

	enum ComponentTag {
		kTagActorShadow = 13,
		kTagFootEffectContainer = 14,
		kTagHeadEffectContainer = 15,
		kTagChatBubble = 16,
		kTagActorName = 50,
		kTagLockedCircle = 51,
		kTagDangerCircle = 52,
		kTagArrowIndicator = 53,
		kTagFunctionIconAndLabel = 54,
		kTagFootFlag = 103,
		kTagBloodBar = 105,
		kTagGreenBloodBar = 106,
		kTagPresentCountDown = 107,
	};

	enum ComponentZOrder {
		ACTOR_SHADOW_ZORDER = 5,
		ACTOR_FOOTFLAG_ZORDER = 10,
		ACTOR_LOCKED_CIRCLE_ZORDER = 11,
		ACTOR_ARROW_INDICATOR_ZORDER = 15,
		ACTOR_FOOTEFFECT_ZORDER = 16,   // 比如，脚下的buff特效
		ACTOR_BODY_ZORDER = 20,
		ACTOR_HEAD_ZORDER = 25,
		ACTOR_NAME_ZORDER = 30,
	};

public:
	GameActor();
	virtual ~GameActor();

	virtual void loadWithInfo(LevelActorInfo* levelActorInfo) {};

	virtual void update(float dt) {};

	virtual void setPosition(const Vec2 &position);
	/** 
	   * 上面的setPosition()在大多数情况下，并不会改变cocos position，只是改变了world position
	   * 而该函数则会立即修改cocos position
	  **/
	void setPositionImmediately(const Vec2 &position);

	virtual void setAnimScale(float scale) {};
	virtual float getAnimScale();

	/** 移动速度的单位: 像素/秒 */
	CC_SYNTHESIZE(float, m_pMoveSpeed, MoveSpeed)

	// x,y on map, 游戏世界坐标系
	void setWorldPosition(Vec2 newWorldPosition);
	Vec2 getWorldPosition();
	// x,y  真实的游戏世界坐标( 来自服务器的数据 )
	CC_SYNTHESIZE(Vec2, m_realWorldPosition, RealWorldPosition)
	void setWorldPositionZ(float z);
	float getWorldPositionZ();

	/**
       * actor的排序点（和actor的世界坐标相同或者不同）
	   * z值不参与排序
	   */
	virtual Vec2 getSortPoint();
	// actor所在的layer，用于排序
	CC_SYNTHESIZE(short, m_layerId, LayerId)

	// set the move direction
	inline void setMoveDir(Vec2 dir) { m_moveDir = dir;};
	inline Vec2 getMoveDir() { return m_moveDir;};

	//int getAnimDirection();
	int getAnimDirection(Vec2 from, Vec2 to, const float* angles_data);

	inline void setGameScene(GameSceneLayer* scene) { m_gameScene = scene; };
	inline GameSceneLayer* getGameScene() { return m_gameScene; };

	// 根据场景的Cover信息，决定角色是否半透明显示
	virtual void updateCoverStatus();

	// 根据场景的Water信息，决定角色是否有倒影
	void updateWaterStatus();

	// 是否开启倒影
	virtual void setReflection(bool enabled) { m_reflectionEnabled = enabled; };
	virtual bool isReflection() { return m_reflectionEnabled; };

	int getType();
	void setType(int typeValue);

	// locked actor
	inline long long getLockedActorId() { return m_lockedActorId; };
	virtual void setLockedActorId(long long lockedActorId);
	GameActor* getLockedActor();
	bool hasLockedActor();
	// locked owner, which means who locked me
	inline void setLockedOwnerId(long long lockedOwnerId) { m_lockedOwnerId = lockedOwnerId; };
	inline long long getLockedOwnerId() { return m_lockedOwnerId; };

	/**
       * display the foot circle to indicate that this actor has been selected
	   * the indicator when this actor has been selected
       **/
	void showLockedCircle(bool bShow, const char* circleName = NULL);

	/**
	 * Actor的名字，及在头顶的显示
	 * 目前，showActorName()内部会动态创建和移除名字Label，这样做或许可减轻Label占用的显存和Node数量开销
	 */
	virtual void showActorName(bool bShow);
	virtual std::string getActorName();
	virtual void setActorName(const char* name);
protected:
	virtual Label* createNameLabel(const char* name);

public:
	/**
	 * 得到Actor可视化区域
	 */
	virtual Rect getVisibleRect();

	inline void setRoleId(long long id) { m_roleId = id; };
	inline long long getRoleId() { return m_roleId; };

	bool isDestroy();
	void setDestroy(bool bDestroy);

	/** 从游戏场景中移除 */
	virtual void onRemoveFromGameScene() {};

protected:
	/** 唯一ID*/
	long long m_roleId;

	int m_actorClassId;

	// action id
	int m_animActionID;

	/** 是否处于生存状态，如果未处于生存状态，则对应的动画不进行播放 */
	bool m_bAlive;

	short* paramData;

	float m_animScale;

	GameSceneLayer* m_gameScene;

	bool m_reflectionEnabled;

	float m_worldPositionZ;
	Vec2 m_worldPosition;

private:
	int type;

	long long m_lockedActorId;
	long long m_lockedOwnerId;

	Vec2 m_moveDir;

	Rect m_visibleRect;                            /// Retangle of Texture2D

	/** 
	 * 表示已经不再需要，彻底销毁对象实例，释放内存 
	 * 一般来说，setDestroy(true)后，在后面一帧，该actor将会被彻底销毁
	 */
	bool m_bIsDestroy;

	std::string m_ActorName;
};

#endif