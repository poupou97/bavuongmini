#ifndef _LEGEND_GAMEACTOR_MYPLAYER_H_
#define _LEGEND_GAMEACTOR_MYPLAYER_H_

#include <vector>

#include "BasePlayer.h"
#include "cocos2d.h"
#include "../../utils/JoyStick.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "../../messageclient/GameProtobuf.h"
#include "../../messageclient/protobuf/PlayerMessage.pb.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class ActorCommand;
class GameActorAnimation;
class CPushCollectInfo;
class MyPlayerSimpleAI;
class MyPlayerAI;

/**
 * 玩家自己
 * @author zhaogang
 * @version 0.1.0
 */
class MyPlayer : public BasePlayer
{
// these state classes are friends, they can access the private members
friend class MyPlayerStand;
friend class MyPlayerRun;
friend class MyPlayerAttack;
friend class MyPlayerWhirl;
friend class MyPlayerSimpleAI;

friend class MyPlayerCommandAttack;
friend class MyPlayerCommandMove;

public:
	MyPlayer();
	virtual ~MyPlayer();

	virtual void update(float dt);

	virtual void onEnter();
	virtual void onExit();

	virtual void setAnimScale(float scale);

	CC_SYNTHESIZE(Joystick*, m_pJoyStick, JoyStick);
	virtual Vec2 getSortPoint();

	//an instance of the state machine class
	StateMachine<MyPlayer>*  m_pStateMachine;
	StateMachine<MyPlayer>*  GetFSM()const{return m_pStateMachine;}

	virtual GameFightSkill* getGameFightSkill(const char* skillId, const char* skillProcessorId);
	virtual GameFightSkill* getMusouSkill();
	virtual void setMusouSkill(const char* skillId);

	inline void setMoveTarget(Vec2 target) { m_moveTarget = target; };
	inline Vec2 getMoveTarget() { return m_moveTarget; };

	//inline void setPaths(__Array* paths) { m_Paths = paths; };
	inline float getPathLength() { return m_pathLength; };

	void runToDirection(Vec2 dir, float dt);

	void updatePos(float dt);

	void updateGotItem(float dt);

	// got item
	virtual void onGotItem(Goods item);

	void setReachedMap(Push5080 reachedmap);
	void addCurrentReachedMap(int countryId, std::string mapId);
	bool isMapReached(int countryId, const char* mapId);

	float mMovedDistance;

	// 基础行为
	//void running(float dt);
	virtual void checkPick();
	virtual void checkAttack();

	virtual void changeAction(int action);
	virtual bool isAction(int action);

	virtual bool isDead();
	virtual bool isAttacking();
	virtual bool isMoving();

	virtual void setLockedActorId(long long lockedActorId);

	virtual ActiveRole* getActiveRole();

	void init(const char* name);

	virtual void initWeaponEffect();

	/**
	 * 选择敌人
	 * @param range 表示选择敌人的搜索范围 
	 */
	bool selectEnemy(int range);
	/**
	 * 选择采集物
	 * @param range 表示选择采集物的搜索范围 
	 */
	bool selectPickActor(int range);
	/** 
	   * 使用指定的技能 
	   * @param bKillTarget, 表示使用完技能后，是否持续攻击直到杀死敌人
	   * @param bCheckCondition, 表示是否检测cd，mp等条件。默认是true（检查），在无双状态下，不予检查
	   * @param selectRange, 表示选择攻击目标的范围（单位: 像素），默认是接近一个屏幕的范围。挂机时，根据挂机的范围进行设置
	   * @param commandSource, 表示命令的来源，默认来自玩家自己。挂机时，设为来自挂机模块
	   */
	bool useSkill(std::string skillId, bool bKillTarget, bool bSendReq = true, bool bCheckCondition = true, int selectRange = 400, int commandSource = 0);
	virtual void updateGameFightSkill(float dt);
	/** 使用无双技 */
	void useMusouSkill();

	// 关系判断
	virtual bool isMyPlayer();
	virtual bool isMyPlayerGroup();
	virtual int getPosInGroup();
	/** check if MyPlayer is teammate of other */
	bool isTeammateOf(long long targetRoleId);
	// 判定参数表示的actor是否能被玩家攻击
	bool canAttackActor(long long targetRoleId);
	bool canAttackPlayer(long long targetRoleId);

	// override
	virtual void onAttack(const char* skillId, const char* skillProcessorId, long long targetId, bool bDoubleAttack, int skillType = -1);

	// override
	virtual void onDamaged(BaseFighter* source, int damageNum, bool bDoubleAttacked, CSkillResult* skillResult = NULL);

	void setPhysicalValue(int _value);
	int getPhysicalValue();
	void setPhysicalCapacity(int _value);
	int getPhysicalCapacity();

	void setAngerValue(int _value);
	int getAngerValue();
	void setAngerCapacity(int _value);
	int getAngerCapacity();

	void setMusouEssence(int _value);
	int getMusouEssence();

	// the blood bar on the role's head
	void addGreenBlood();
	void removeGreenBlood();
	bool hasGreenBlood();

	// the auto combat flag on the head
	void addAutoCombatFlag();
	void removeAutoCombatFlag();
	bool hasAutoCombatFlag();

	virtual Vec2 getHeadLabelPosition(int labelType);

public:
	// 侵略者列表
	inline std::map<long long, int>& getAggressorMap() { return m_aggressorMap; };

	//家族战场中自己家族的守卫ID
	inline std::vector<long long >& getGuardIdList() {return m_guardIdList;} ;

public:
	//threekingdoms::protocol::GamePlayer* player;
	GamePlayer* player;

	inline MyPlayerSimpleAI* getSimpleAI() { return m_simpleAI; };
	inline MyPlayerAI* getMyPlayerAI() { return m_playerAI; };
	
	// 公共技能CD
	void startCommonCD();
	bool isInCommonCD();

private:
	void updateGeneralShow();

private:
	float m_dt;

	std::vector<std::string> m_reachedMapList[6];

	Vec2 m_moveTarget;

	float m_pathLength;

	MyPlayerSimpleAI* m_simpleAI;
	MyPlayerAI* m_playerAI;

	std::string m_musouSkillid;

	std::vector<Goods> m_gotItemIconList;

	std::map<long long, int> m_aggressorMap;

	//家族战场中自己家族的守卫ID
	std::vector<long long > m_guardIdList;

	//体力
	int m_physicalValue;
	int m_physicalCapacity;
	//怒气
	int m_angerValue;
	int m_angerCapacity;
	//龙魂精华
	int m_musouEssence;

	static float s_commonCDTime;
};

#endif