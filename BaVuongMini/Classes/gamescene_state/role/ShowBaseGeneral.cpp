#include "ShowBaseGeneral.h"

#include "../GameSceneState.h"
#include "GameView.h"
#include "../role/MyPlayer.h"
#include "../../utils/StaticDataManager.h"
#include "../role/General.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CShowGeneralInfo.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../messageclient/protobuf/PlayerMessage.pb.h"
#include "../../messageclient/element/CMapInfo.h"
#include "OtherPlayer.h"

ShowBaseGeneral::ShowBaseGeneral()
	:m_longGeneralId(0)
	,m_nTemplateId(0)
	,m_nActionType(-1)
	,m_nCurrentQuality(-1)
	,m_nEvolution(-1)
{

}

ShowBaseGeneral::~ShowBaseGeneral()
{

}

void ShowBaseGeneral::set_actionType( int nActionType )
{
	this->m_nActionType = nActionType;
}

int ShowBaseGeneral::get_actionType()
{
	return this->m_nActionType;
}

void ShowBaseGeneral::set_generalId( long long longGeneralId )
{
	this->m_longGeneralId = longGeneralId;
}

long long ShowBaseGeneral::get_generalId()
{
	return this->m_longGeneralId;
}

void ShowBaseGeneral::set_templateId( int nTemplateId )
{
	this->m_nTemplateId = nTemplateId;
}

int ShowBaseGeneral::get_templateId()
{
	return this->m_nActionType;
}

void ShowBaseGeneral::set_currentQuality( int nCurrentQuality )
{
	this->m_nCurrentQuality = nCurrentQuality;
}

int ShowBaseGeneral::get_currentQuality()
{
	return this->m_nCurrentQuality;
}

void ShowBaseGeneral::set_evolution( int nEvolution )
{
	this->m_nEvolution = nEvolution;
}

int ShowBaseGeneral::get_evolution()
{
	return this->m_nEvolution;
}

void ShowBaseGeneral::generalHide()
{
	auto scene = GameView::getInstance()->getGameScene();
	if (NULL != scene)
	{
		auto actorLayer = scene->getActorLayer();
		if (NULL != actorLayer)
		{
			General * pActor = (General*)actorLayer->getChildByTag(m_longGeneralId * -1);
			if (NULL != pActor)
			{
				pActor->setDestroy(true);
			}
		}
	}
}

bool ShowBaseGeneral::hasOnShowState(long long generalId)
{
	bool bFlag = false;

	if (m_longGeneralId == generalId)
	{
		if (-1 == m_nActionType)
		{
			bFlag = false;
		}
		else if (SHOW_ACTIONTYPE == m_nActionType)
		{
			bFlag = true;
		}
		else if (HIDE_ACTIONTYPE == m_nActionType)
		{
			bFlag = false;
		}
	}

	return bFlag;
}

int ShowBaseGeneral::getShowState( long long generalId )
{
	int nState = 0;

	if (m_longGeneralId == generalId)
	{
		if (-1 == m_nActionType)
		{
			nState = 1;
		}
		else if (SHOW_ACTIONTYPE == m_nActionType)
		{
			nState = 0;
		}
		else if (HIDE_ACTIONTYPE == m_nActionType)
		{
			nState = 1;
		}
	}
	else
	{
		nState = 1;
	}

	return nState;
}

void ShowBaseGeneral::clearAllData()
{
	this->set_generalId(0);
	this->set_templateId(0);
	this->set_actionType(-1);
	this->set_currentQuality(-1);
	this->set_evolution(-1);
}

///////////////////////////////////////////////////////////////////////////////////////////

ShowMyGeneral * ShowMyGeneral::s_showMyGeneral = NULL;

ShowMyGeneral::ShowMyGeneral()
{

}

ShowMyGeneral::~ShowMyGeneral()
{

}

ShowMyGeneral * ShowMyGeneral::getInstance()
{
	if (NULL == s_showMyGeneral)
	{
		s_showMyGeneral = new ShowMyGeneral();
	}

	return s_showMyGeneral;
}

void ShowMyGeneral::generalShow( CShowGeneralInfo* pShowGeneralInfo )
{
	long long longGeneralId = pShowGeneralInfo->generalid();
	long long showGeneralId = longGeneralId * -1;

	auto scene = GameView::getInstance()->getGameScene();
	if(NULL == scene)
	{
		return;
	}

	// ���չʾ���佫���ڣ���ֹ
	auto actorLayer = scene->getActorLayer();
	if (NULL != actorLayer)
	{
		auto pGeneral = actorLayer->getChildByTag(showGeneralId);
		if (NULL != pGeneral)
		{
			return ;
		}
	}

	// չʾ�佫���ջ��佫��
	auto pMyPlayer = GameView::getInstance()->myplayer;
	float xPosReal = pMyPlayer->getRealWorldPosition().x;
	float yPosReal = pMyPlayer->getRealWorldPosition().y;
	float xPos = pMyPlayer->getWorldPosition().x - 64;
	float yPos = pMyPlayer->getWorldPosition().y;

	auto generalBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[pShowGeneralInfo->templateid()];

	auto pActor = new General();
	pActor->setRoleId(showGeneralId);
	scene->putActor(showGeneralId, pActor);

	// ���tag�ֵ�������佫id�����ʱ��tag�ֵ��٣�
	pActor->setTag(showGeneralId);

	pActor->setOwnerId(pMyPlayer->getRoleId());

	// init position
	pActor->setWorldPosition(Vec2(xPos, yPos));
	pActor->setRealWorldPosition(Vec2(xPos, yPos));
	// ��ʼ��ʱ�����cocos postion�Ҳֱ�ӽ��г�ʼ�������Խ���л��佫ʱ���������쳣���
	pActor->setPositionImmediately(scene->convertToCocos2DSpace(pActor->getWorldPosition()));

	// ������佫���
	pActor->setAnimDir(GameView::getInstance()->myplayer->getAnimDir());   // �����Լ����佫�������һ���

	pActor->init(generalBaseMsgFromDb->get_avatar().c_str());
	pActor->setActorName(generalBaseMsgFromDb->name().c_str());

	//�ˢ�³����е��佫ͷ��������ɫ
	pActor->modifyActorNameColor(GameView::getInstance()->getGeneralsColorByQuality(pShowGeneralInfo->currentquality()));

	pActor->showActorName(true);
	//pActor->showFlag(true);	// �˴���� ��Ƿ�չʾ�佫���µĹ�Ȧ
	pActor->setGameScene(scene);

	// չʾ�佫�Ľ�ȼ�
	pActor->addRank(pShowGeneralInfo->evolution());

	pActor->addRareIcon(generalBaseMsgFromDb->rare());

	// add new actor into cocos2d-x render scene
	actorLayer->addChild(pActor);
	pActor->release();

	//pActor->onBorn();

	// ��onBorn�actionĽ����� ����������
	auto action = Sequence::create(
		Show::create(),
		DelayTime::create(1.25f),
		CallFuncN::create(CC_CALLBACK_1(ShowMyGeneral::callBackOnBorn, this, (void *)pActor)),
		//CallFuncND::create(this, callfuncND_selector(ShowMyGeneral::callBackOnBorn), (void *)pActor),
		NULL);

	pActor->runAction(action);

	// Ǹ�����
	//pActor->followMaster(pMyPlayer->getRoleId(), 2);
}

void ShowMyGeneral::generalShow()
{
	auto gameView = GameView::getInstance();
	if (NULL == gameView)
	{
		return;
	}

	if (NULL == gameView->myplayer)
	{
		return;
	}

	// ������ж ��Ƿ�����ʾ
	if (SHOW_ACTIONTYPE == m_nActionType)
	{
		if ((0 != m_longGeneralId) && (0 != m_nTemplateId) && (-1 != m_nCurrentQuality) && (-1 != m_nEvolution))
		{
			auto pShowGeneralInfo = new CShowGeneralInfo();
			pShowGeneralInfo->set_generalid(m_longGeneralId);
			pShowGeneralInfo->set_templateid(m_nTemplateId);
			pShowGeneralInfo->set_currentquality(m_nCurrentQuality);
			pShowGeneralInfo->set_evolution(m_nEvolution);

			this->generalShow(pShowGeneralInfo);

			delete pShowGeneralInfo;
		}
	}
	else if(-1 == m_nActionType)
	{
		initGeneralData();
	}
}

bool ShowMyGeneral::canGeneralShow()
{
	bool bCanGeneralShow = true;

	// ����������ʾ ��չʾ���佫
	if (GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::coliseum)
	{
		bCanGeneralShow = false;

		return bCanGeneralShow;
	}

	auto pMyPlayer = GameView::getInstance()->myplayer;
	if (NULL == pMyPlayer)
	{
		bCanGeneralShow = false;

		return bCanGeneralShow;
	}

	auto pNode_blood = pMyPlayer->getChildByTag(GameActor::kTagGreenBloodBar);
	if (NULL == pNode_blood)
	{
		for(unsigned int i = 0 ; i < GameView::getInstance()->generalsInLineList.size();++i)
		{
			auto temp = GameView::getInstance()->generalsInLineList.at(i);
			if (2 == temp->fightstatus())
			{
				bCanGeneralShow = false;

				break;
			}
		}
	}
	else
	{
		bCanGeneralShow = false;
	}

	return bCanGeneralShow;
}

void ShowMyGeneral::callBackOnBorn( Ref * pSender, void * pActor )
{
	auto pGeneral = (General*)pActor;
	if (NULL != pGeneral)
	{
		auto pMyPlayer = GameView::getInstance()->myplayer;
		if (NULL != pMyPlayer)
		{
			// ������
			pGeneral->followMaster(pMyPlayer->getRoleId(), 2);
		}
	}
}

void ShowMyGeneral::initGeneralData()
{
	auto gameView = GameView::getInstance();
	if (NULL == gameView)
	{
		return;
	}

	if (NULL == gameView->myplayer)
	{
		return;
	}

	auto gamePlayer = gameView->myplayer->player;
	if (NULL == gamePlayer)
	{
		return;
	}

	ActiveRole activeRole = gamePlayer->activerole();
	PlayerBaseInfo playerBaseInfo = activeRole.playerbaseinfo();
	ShowGeneralInfo tmp = playerBaseInfo.showgenral();

	if (0 != tmp.generalid() && 0 != tmp.templateid())
	{
		this->set_actionType(SHOW_ACTIONTYPE);

		// ǽ� չʾ �佫��ݱ��
		this->set_generalId(tmp.generalid());
		this->set_templateId(tmp.templateid());
		this->set_currentQuality(tmp.currentquality());
		this->set_evolution(tmp.evolution());

		auto pShowGeneralInfo = new CShowGeneralInfo();
		pShowGeneralInfo->set_generalid(tmp.generalid());
		pShowGeneralInfo->set_templateid(tmp.templateid());
		pShowGeneralInfo->set_currentquality(tmp.currentquality());
		pShowGeneralInfo->set_evolution(tmp.evolution());

		this->generalShow(pShowGeneralInfo);

		delete pShowGeneralInfo;
	}
	else
	{
		this->set_actionType(HIDE_ACTIONTYPE);
	}
}

////////////////////////////////////////////////////////////////////////////////////

ShowOtherGeneral::ShowOtherGeneral()
	:m_pOtherPlayer(NULL)
{

}

ShowOtherGeneral::~ShowOtherGeneral()
{

}

void ShowOtherGeneral::generalShow( CShowGeneralInfo* pShowGeneralInfo )
{
	if (NULL == m_pOtherPlayer)
	{
		CCLOG("show otherGeneral error");

		return;
	}

	long long longGeneralId = pShowGeneralInfo->generalid();
	long long showGeneralId = longGeneralId * -1;

	auto scene = GameView::getInstance()->getGameScene();
	if(NULL == scene)
	{
		return;
	}

	// ����չʾ���佫���ڣ���ֹ
	auto actorLayer = scene->getActorLayer();
	if (NULL != actorLayer)
	{
		auto pGeneral = actorLayer->getChildByTag(showGeneralId);
		if (NULL != pGeneral)
		{
			return ;
		}
	}

	// չʾ�佫���ջ��佫��
	float xPosReal = m_pOtherPlayer->getRealWorldPosition().x;
	float yPosReal = m_pOtherPlayer->getRealWorldPosition().y;
	float xPos = m_pOtherPlayer->getWorldPosition().x - 64;
	float yPos = m_pOtherPlayer->getWorldPosition().y;

	auto generalBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[pShowGeneralInfo->templateid()];

	auto pActor = new General();
	pActor->setRoleId(showGeneralId);
	scene->putActor(showGeneralId, pActor);

	// ���tag�ֵ�������佫id�����ʱ��tag�ֵ��٣�
	pActor->setTag(showGeneralId);

	pActor->setOwnerId(m_pOtherPlayer->getRoleId());

	// init position
	pActor->setWorldPosition(Vec2(xPos, yPos));
	pActor->setRealWorldPosition(Vec2(xPos, yPos));
	// ��ʼ��ʱ�����cocos postion�Ҳֱ�ӽ��г�ʼ�������Խ���л��佫ʱ���������쳣���
	pActor->setPositionImmediately(scene->convertToCocos2DSpace(pActor->getWorldPosition()));

	// ������佫���
	pActor->setAnimDir(m_pOtherPlayer->getAnimDir());   // �����Լ����佫�������һ���

	pActor->init(generalBaseMsgFromDb->get_avatar().c_str());
	pActor->setActorName(generalBaseMsgFromDb->name().c_str());

	//�ˢ�³����е��佫ͷ��������ɫ
	pActor->modifyActorNameColor(GameView::getInstance()->getGeneralsColorByQuality(pShowGeneralInfo->currentquality()));

	pActor->showActorName(true);
	//pActor->showFlag(true);	// �˴���� ��Ƿ�չʾ�佫���µĹ�Ȧ
	pActor->setGameScene(scene);

	// չʾ�佫�Ľ�ȼ�
	pActor->addRank(pShowGeneralInfo->evolution());

	pActor->addRareIcon(generalBaseMsgFromDb->rare());

	// add new actor into cocos2d-x render scene
	actorLayer->addChild(pActor);
	pActor->release();

	//pActor->onBorn();

	// ��onBorn�actionĽ����� ����������
	auto action = Sequence::create(
		Show::create(),
		DelayTime::create(1.25f),
		//CallFuncND::create(this, callfuncND_selector(ShowOtherGeneral::callBackOnBorn), (void *)pActor),
		CallFuncN::create(CC_CALLBACK_1(ShowOtherGeneral::callBackOnBorn, this, (void *)pActor)),
		NULL);

	pActor->runAction(action);

	// Ǹ�����
	//pActor->followMaster(pMyPlayer->getRoleId(), 2);
}

void ShowOtherGeneral::generalShow()
{
	if (NULL == m_pOtherPlayer)
	{
		CCLOG("show otherGeneral error");

		return;
	}

	// ������ж ��Ƿ�����ʾ
	if (SHOW_ACTIONTYPE == m_nActionType)
	{
		if ((0 != m_longGeneralId) && (0 != m_nTemplateId) && (-1 != m_nCurrentQuality) && (-1 != m_nEvolution))
		{
			CShowGeneralInfo* pShowGeneralInfo = new CShowGeneralInfo();
			pShowGeneralInfo->set_generalid(m_longGeneralId);
			pShowGeneralInfo->set_templateid(m_nTemplateId);
			pShowGeneralInfo->set_currentquality(m_nCurrentQuality);
			pShowGeneralInfo->set_evolution(m_nEvolution);

			this->generalShow(pShowGeneralInfo);

			delete pShowGeneralInfo;
		}
	}
	else if(-1 == m_nActionType)
	{
		PlayerBaseInfo playerBaseInfo = m_pOtherPlayer->getActiveRole()->playerbaseinfo();
		ShowGeneralInfo tmp = playerBaseInfo.showgenral();

		if (0 != tmp.generalid() && 0 != tmp.templateid())
		{
			//m_nActionType = SHOW_ACTIONTYPE;

			// �� չʾ �佫��ݱ��
			this->set_generalId(tmp.generalid());
			this->set_templateId(tmp.templateid());
			this->set_currentQuality(tmp.currentquality());
			this->set_evolution(tmp.evolution());
			this->set_actionType(m_nActionType);

			CShowGeneralInfo* pShowGeneralInfo = new CShowGeneralInfo();
			pShowGeneralInfo->set_generalid(tmp.generalid());
			pShowGeneralInfo->set_templateid(tmp.templateid());
			pShowGeneralInfo->set_currentquality(tmp.currentquality());
			pShowGeneralInfo->set_evolution(tmp.evolution());

			this->generalShow(pShowGeneralInfo);

			delete pShowGeneralInfo;
		}
	}
}

bool ShowOtherGeneral::canGeneralShow()
{
	bool bCanGeneralShow = true;

	if (NULL == m_pOtherPlayer)
	{
		bCanGeneralShow = false;

		return bCanGeneralShow;
	}

	// 澺��������ʾ������ҵġ�չʾ���佫
	if (GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::coliseum)
	{
		bCanGeneralShow = false;

		return bCanGeneralShow;
	}

	Node* pNode_blood = m_pOtherPlayer->getChildByTag(GameActor::kTagBloodBar);
	if (NULL == pNode_blood)
	{
		auto scene = GameView::getInstance()->getGameScene();

		if (NULL != scene)
		{
			for (std::map<long long,GameActor*>::iterator it = scene->getActorsMap().begin(); it != scene->getActorsMap().end(); ++it)
			{
				General* pGeneral = dynamic_cast<General*>(it->second);
				if(NULL != pGeneral)
				{
					if (m_pOtherPlayer->getRoleId() == pGeneral->getOwnerId())
					{
						bCanGeneralShow = false;

						return bCanGeneralShow;
					}
				}
			}
		}

	}
	else
	{
		bCanGeneralShow = false;
	}

	return bCanGeneralShow;
}

void ShowOtherGeneral::callBackOnBorn( Ref * pSender, void * pActor )
{
	auto pGeneral = (General*)pActor;
	if (NULL != pGeneral)
	{
		if (NULL != m_pOtherPlayer)
		{
			// ������
			pGeneral->followMaster(m_pOtherPlayer->getRoleId(), 2);
		}
	}
}

void ShowOtherGeneral::set_otherPlayer( OtherPlayer* pOtherPlayer )
{
	this->m_pOtherPlayer = pOtherPlayer;
}
