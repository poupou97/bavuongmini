#ifndef _LEGEND_GAMEACTOR_FUNCTIONNPC_H_
#define _LEGEND_GAMEACTOR_FUNCTIONNPC_H_

#include "BaseFighter.h"
#include "cocos2d.h"
#include "../../messageclient/element/MissionInfo.h"

#ifndef ROLE_FUNCTIONNPC_DEBUG_DRAW
#define ROLE_FUNCTIONNPC_DEBUG_DRAW 1
#endif

#define NPC_ANI_STAND 0			
#define NPC_ANI_RUN 1			
#define NPC_ATTACK 2	
#define NPC_DEATH 3	
#define NPC_ANI_MAX 4

class GameActorAnimation;
class CCLegendAnimation;

/**
 * NPC
 * @author zhaogang
 * @version 0.1.0
 */
class FunctionNPC : public BaseFighter
{
friend class FunctionNPCStand;
friend class FunctionNPCRun;
friend class FunctionNPCAttack;
friend class FunctionNPCDeath;

public:
	static FunctionNPC* create(ActiveRole* activeRole, GameSceneLayer* scene);

public:
	FunctionNPC();
	virtual ~FunctionNPC();

	virtual void update(float dt);

	virtual Vec2 getSortPoint();

	//an instance of the state machine class
	StateMachine<FunctionNPC>*  m_pStateMachine;
	StateMachine<FunctionNPC>*  GetFSM()const{return m_pStateMachine;}

	bool init(const char* roleName);

	virtual Rect getVisibleRect();

	virtual void changeAction(int action);
	virtual bool isAction(int action);

	virtual bool isDead();
	virtual bool isStanding();
	virtual bool isAttacking();

	virtual void showActorName(bool bShow);

	void RefreshNpc();

#if ROLE_FUNCTIONNPC_DEBUG_DRAW
	virtual void draw(Renderer *renderer, const Mat4& transform, uint32_t flags);
#endif

	virtual void loadWithInfo(LevelActorInfo* levelActorInfo);
	
	/** check if the npc who has the icon on his head */
	bool isSpecificFunctionNpc();
	void showFunctionIcon(bool bShow);
	
	std::string mapIcon;//icon
	/**
	 * NPC类型	
	 * 1:地图切换点	 
	 * 2、3、4……:功能编号
	 */
	char function;//功能
	/**  NPC状态（标识叹号、问号等） 0=普通  1=任务可授予 2=有任务可提交 3=任务中（未完成）*/
	char stat;
	/** npc说明 */
	std::string intro;
	/** NPCMission*/
	std::vector<MissionInfo*> npcMissionList;
	/*function icon & text*/
	std::string m_sFunctionIcon;
	/*if show the functionIcon*/
	bool m_bIsFunctionIconShow;

	Sprite * m_npcMissionIndicate;
	struct FunctionIcon
	{
		long long id;   // npc's role id
		std::string icon;
	};

	CCLegendAnimation * getNpcCircleIndicate();
	void createNpcCircleIndicate();
private:
	void updateMissionList();
	bool isHasMission();
	void updateNpcState();
	void updateNpcStateIcon();  /******* refresh stateIcon ******/

	int m_index;
};

#endif