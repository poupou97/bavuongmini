#ifndef _FUNCTIONNPC_OWNED_STATES_H
#define _FUNCTIONNPC_OWNED_STATES_H

#include "State.h"

class FunctionNPC;

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class FunctionNPCGlobalState : public State<FunctionNPC>
{  
private:
  
  FunctionNPCGlobalState(){}
  
  //copy ctor and assignment should be private
  FunctionNPCGlobalState(const FunctionNPCGlobalState&);
  FunctionNPCGlobalState& operator=(const FunctionNPCGlobalState&);
 
public:

  static FunctionNPCGlobalState* Instance();
  
  virtual void Enter(FunctionNPC* self){}

  virtual void Execute(FunctionNPC* self);

  virtual void Exit(FunctionNPC* self){}
};


//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class FunctionNPCStand : public State<FunctionNPC>
{
private:
  
  FunctionNPCStand(){}

  //copy ctor and assignment should be private
  FunctionNPCStand(const FunctionNPCStand&);
  FunctionNPCStand& operator=(const FunctionNPCStand&); 
  
public:

  static FunctionNPCStand* Instance();
  
  virtual void Enter(FunctionNPC* self);

  virtual void Execute(FunctionNPC* self);

  virtual void Exit(FunctionNPC* self);

};



//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class FunctionNPCRun : public State<FunctionNPC>
{
private:
  
  FunctionNPCRun(){}

  //copy ctor and assignment should be private
  FunctionNPCRun(const FunctionNPCRun&);
  FunctionNPCRun& operator=(const FunctionNPCRun&);
 
public:

  static FunctionNPCRun* Instance();
  
  virtual void Enter(FunctionNPC* self);

  virtual void Execute(FunctionNPC* self);

  virtual void Exit(FunctionNPC* self);

};

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class FunctionNPCAttack : public State<FunctionNPC>
{
private:
  
  FunctionNPCAttack(){}

  //copy ctor and assignment should be private
  FunctionNPCAttack(const FunctionNPCAttack&);
  FunctionNPCAttack& operator=(const FunctionNPCAttack&);
 
public:

  static FunctionNPCAttack* Instance();
  
  virtual void Enter(FunctionNPC* self);

  virtual void Execute(FunctionNPC* self);

  virtual void Exit(FunctionNPC* self);

};

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class FunctionNPCDeath : public State<FunctionNPC>
{
private:
  
  FunctionNPCDeath(){}

  //copy ctor and assignment should be private
  FunctionNPCDeath(const FunctionNPCDeath&);
  FunctionNPCDeath& operator=(const FunctionNPCDeath&);
 
public:

  static FunctionNPCDeath* Instance();
  
  virtual void Enter(FunctionNPC* self);

  virtual void Execute(FunctionNPC* self);

  virtual void Exit(FunctionNPC* self);

};

#endif