#ifndef _GAMESCENESTATE_SHOWBASEGENERAL_H_
#define _GAMESCENESTATE_SHOWBASEGENERAL_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;
USING_NS_CC_EXT;

class CShowGeneralInfo;
class OtherPlayer;

// ��չʾ���佫�Ļ��
class ShowBaseGeneral: public Ref
{
public:
	ShowBaseGeneral();
	~ShowBaseGeneral();

public:
	// ����ṩ�Ĳ���չʾ�佫
	virtual void generalShow(CShowGeneralInfo* pShowGeneralInfo) {}

	// ��չʾ���佫
	virtual void generalShow() {}

	// ���ջء��佫
	virtual void generalHide();

	// �жϡ�չʾ�佫���Ƿ����չʾ
	virtual bool canGeneralShow() {return false;}

	virtual bool hasOnShowState(long long generalId);									// �Ƿ��ڡ�չʾ��״̬
	
	// �˷������ڷ����佫�б��չʾ or �ջء�״̬
	virtual int getShowState(long long generalId);										// 1:��չʾ��0���ջأ�2��չʾ����ť��أ�

	// ���������
	virtual void clearAllData();

protected:
	virtual void callBackOnBorn(Ref * pSender, void * pActor) {}

	// ݴ���ǵ�Э���ֶγ�ʼ����չʾ�佫��
	virtual void initGeneralData() {}

protected:
	int m_nActionType;			// չʾ��� 1�չʾ 0��ȡ� 2:����ߴ���
	long long m_longGeneralId;	// �佫Id
	int m_nTemplateId;			// �佫ģ�id
	int m_nCurrentQuality;		// 嵱ǰƷ�
	int m_nEvolution;			// ʽ�ȼ�
	
public:
	void set_actionType(int nActionType);
	int get_actionType();

	void set_generalId(long long longGeneralId);
	long long get_generalId();

	void set_templateId(int nTemplateId);
	int get_templateId();

	void set_currentQuality(int nCurrentQuality);
	int get_currentQuality();

	void set_evolution(int nEvolution);
	int get_evolution();

	enum
	{
		HIDE_ACTIONTYPE = 0,
		SHOW_ACTIONTYPE = 1,
		OTHER_ACTIONTYPE = 2
	};
};

/////////////////////////////////////////////////////////////////////////////

class ShowMyGeneral : public ShowBaseGeneral
{
private:
	ShowMyGeneral();
	~ShowMyGeneral();

public:
	static ShowMyGeneral * s_showMyGeneral;
	static ShowMyGeneral * getInstance();

public:
	// ����ṩ�Ĳ���չʾ�佫
	virtual void generalShow(CShowGeneralInfo* pShowGeneralInfo);

	// ��չʾ���佫
	virtual void generalShow();

	// �жϡ�չʾ�佫���Ƿ����չʾ
	virtual bool canGeneralShow();

protected:
	virtual void callBackOnBorn(Ref * pSender, void * pActor);

	// ����ǵ�Э���ֶγ�ʼ����չʾ�佫��
	virtual void initGeneralData();
};


/////////////////////////////////////////////////////////////////////////////

class ShowOtherGeneral : public ShowBaseGeneral
{
public:
	ShowOtherGeneral();
	~ShowOtherGeneral();

public:
	// ����ṩ�Ĳ���չʾ�佫
	virtual void generalShow(CShowGeneralInfo* pShowGeneralInfo);

	// ��չʾ���佫
	virtual void generalShow();

	// �жϡ�չʾ�佫���Ƿ����չʾ
	virtual bool canGeneralShow();

protected:
	virtual void callBackOnBorn(Ref * pSender, void * pActor);

private:
	OtherPlayer* m_pOtherPlayer;

public:
	void set_otherPlayer(OtherPlayer* pOtherPlayer);
};

#endif

