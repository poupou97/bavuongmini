#include "PresentBox.h"

#include "../../legend_engine/CCLegendAnimation.h"
#include "../GameSceneState.h"
#include "../../messageclient/element/CPushCollectInfo.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/CPickingInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CCollectInfo.h"

#define kTagParticleEffect 11
#define kTagActivityAnm 12
#define kTagInvaildAnm 13

PresentBox::PresentBox()
	:isActivied(true)
{
	this->setType(GameActor::type_presentbox);
}

PresentBox::~PresentBox()
{
	//CC_SAFE_RELEASE(m_pAnim);
}

bool PresentBox::init(const char* actorName)
{
	// shadow
    auto shadow = Sprite::create("res_ui/shadow.png");
	shadow->setAnchorPoint(Vec2(0.5f, 0.5f));
	shadow->setScale(0.8f);
	shadow->setOpacity(128);
    addChild(shadow, GameActor::ACTOR_SHADOW_ZORDER);

	// load animation
	std::string animFileName = "animation/collection/";
	animFileName.append(actorName);
	animFileName.append(".anm");
	m_pAnim = CCLegendAnimation::create(animFileName);
	m_pAnim->setReleaseWhenStop(false);
	addChild(m_pAnim, ACTOR_BODY_ZORDER);

	// load special effect
	auto particleEffect = ParticleSystemQuad::create("animation/texiao/particledesigner/caiji1.plist");
	particleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	particleEffect->setPosition(Vec2(0,0));
	particleEffect->setScale(1.0f);
	particleEffect->setTag(kTagParticleEffect);
	particleEffect->setVisible(false);
	addChild(particleEffect, ACTOR_HEAD_ZORDER);

	setContentSize(Size(40, PICKINGACTOR_ROLE_HEIGHT));

	return true;
}

void PresentBox::showActorName(bool bShow)
{
	std::string namestr = getActorName();
	const char* name = namestr.c_str();

	auto nameLabel = (Label*)this->getChildByTag(GameActor::kTagActorName);
	if(bShow)
	{
		if(nameLabel == NULL)
		{
			nameLabel = this->createNameLabel(name);
			this->addChild(nameLabel, GameActor::ACTOR_NAME_ZORDER);
		}
		else
		{
			nameLabel->setString(name);
		}
		nameLabel->setPosition(Vec2(0, PICKINGACTOR_ROLE_HEIGHT));
	}
	else
	{
		if(nameLabel != NULL)
			nameLabel->removeFromParent();
	}
}

void PresentBox::setActivied( bool isactive )
{
	isActivied = isactive;
	//���ƶ����Ĳ�����
	auto effect = this->getChildByTag(kTagParticleEffect);
	auto activityAnm = this->getChildByTag(kTagActivityAnm);
	auto invaildAnm = this->getChildByTag(kTagInvaildAnm);
	if (isActivied)
	{
		if(effect != NULL)
			effect->setVisible(true);

		if (activityAnm)
			activityAnm->setVisible(true);

		if (invaildAnm)
			invaildAnm->setVisible(false);
	}
	else
	{
		if(effect != NULL)
			effect->setVisible(false);
		//this->showCircle(false);
		if (activityAnm)
			activityAnm->setVisible(false);

		if (invaildAnm)
			invaildAnm->setVisible(true);
	}
}

bool PresentBox::getActivied()
{
	return isActivied;
}

bool PresentBox::initPresentBox( const char* actorName )
{
	// shadow
	auto shadow = Sprite::create("res_ui/shadow.png");
	shadow->setAnchorPoint(Vec2(0.5f, 0.5f));
	shadow->setScale(0.8f);
	shadow->setOpacity(128);
	addChild(shadow, GameActor::ACTOR_SHADOW_ZORDER);

	// load animation
	/*std::string animFileName = "animation/collection/";
	animFileName.append(actorName);
	animFileName.append(".anm");*/
	m_pAnim = CCLegendAnimation::create(actorName);
	m_pAnim->setReleaseWhenStop(false);
	addChild(m_pAnim, ACTOR_BODY_ZORDER);

	// load special effect
	auto particleEffect = ParticleSystemQuad::create("animation/texiao/particledesigner/caiji1.plist");
	particleEffect->setPositionType(ParticleSystem::PositionType::GROUPED);
	particleEffect->setPosition(Vec2(0,0));
	particleEffect->setScale(1.5f);
	particleEffect->setTag(kTagParticleEffect);
	particleEffect->setVisible(false);
	addChild(particleEffect, ACTOR_HEAD_ZORDER);

	setContentSize(Size(40, PICKINGACTOR_ROLE_HEIGHT));

	return true;
}

