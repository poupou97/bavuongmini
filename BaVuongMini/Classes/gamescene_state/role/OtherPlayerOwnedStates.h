#ifndef _OTHERPLAYER_OWNED_STATES_H
#define _OTHERPLAYER_OWNED_STATES_H

//------------------------------------------------------------------------
//
//  Name:   OtherPlayerOwnedStates.h
//
//  Desc:   All the states that can be assigned to the OtherPlayer class
//  该类以状态机的方式，定义了角色的基础行为、或者叫基础动作
//  Author: Zhao Gang 2013
//
//------------------------------------------------------------------------
#include "State.h"

class OtherPlayer;


//------------------------------------------------------------------------
class OtherPlayerGlobalState : public State<OtherPlayer>
{  
private:
  
  OtherPlayerGlobalState(){}
  
  //copy ctor and assignment should be private
  OtherPlayerGlobalState(const OtherPlayerGlobalState&);
  OtherPlayerGlobalState& operator=(const OtherPlayerGlobalState&);
 
public:

  static OtherPlayerGlobalState* Instance();
  
  virtual void Enter(OtherPlayer* self){}

  virtual void Execute(OtherPlayer* self);

  virtual void Exit(OtherPlayer* self){}
};


//------------------------------------------------------------------------
class OtherPlayerStand : public State<OtherPlayer>
{
private:
  
  OtherPlayerStand(){}

  //copy ctor and assignment should be private
  OtherPlayerStand(const OtherPlayerStand&);
  OtherPlayerStand& operator=(const OtherPlayerStand&); 
  
public:

  static OtherPlayerStand* Instance();
  
  virtual void Enter(OtherPlayer* self);

  virtual void Execute(OtherPlayer* self);

  virtual void Exit(OtherPlayer* self);

};


//------------------------------------------------------------------------
class OtherPlayerRun : public State<OtherPlayer>
{
private:
  
  OtherPlayerRun(){}

  //copy ctor and assignment should be private
  OtherPlayerRun(const OtherPlayerRun&);
  OtherPlayerRun& operator=(const OtherPlayerRun&);
 
public:

  static OtherPlayerRun* Instance();
  
  virtual void Enter(OtherPlayer* self);

  virtual void Execute(OtherPlayer* self);

  virtual void Exit(OtherPlayer* self);

};


//------------------------------------------------------------------------
class OtherPlayerAttack : public State<OtherPlayer>
{
private:
  
  OtherPlayerAttack(){}

  //copy ctor and assignment should be private
  OtherPlayerAttack(const OtherPlayerAttack&);
  OtherPlayerAttack& operator=(const OtherPlayerAttack&);
 
public:

  static OtherPlayerAttack* Instance();
  
  virtual void Enter(OtherPlayer* self);

  virtual void Execute(OtherPlayer* self);

  virtual void Exit(OtherPlayer* self);

private:
	bool m_bIsSkillReleased;
	float m_nReleasePhase;

};


//------------------------------------------------------------------------
class OtherPlayerDeath : public State<OtherPlayer>
{
private:
  
  OtherPlayerDeath(){}

  //copy ctor and assignment should be private
  OtherPlayerDeath(const OtherPlayerDeath&);
  OtherPlayerDeath& operator=(const OtherPlayerDeath&);
 
public:

  static OtherPlayerDeath* Instance();
  
  virtual void Enter(OtherPlayer* self);

  virtual void Execute(OtherPlayer* self);

  virtual void Exit(OtherPlayer* self);

};

//------------------------------------------------------------------------
class OtherPlayerKnockBack : public State<OtherPlayer>
{
private:
  
  OtherPlayerKnockBack(){}

  //copy ctor and assignment should be private
  OtherPlayerKnockBack(const OtherPlayerKnockBack&);
  OtherPlayerKnockBack& operator=(const OtherPlayerKnockBack&);
 
public:

  static OtherPlayerKnockBack* Instance();
  
  virtual void Enter(OtherPlayer* self);

  virtual void Execute(OtherPlayer* self);

  virtual void Exit(OtherPlayer* self);

};

//------------------------------------------------------------------------
class OtherPlayerCharge : public State<OtherPlayer>
{
private:
  
  OtherPlayerCharge(){}

  //copy ctor and assignment should be private
  OtherPlayerCharge(const OtherPlayerCharge&);
  OtherPlayerCharge& operator=(const OtherPlayerCharge&); 
  
public:

  static OtherPlayerCharge* Instance();
  
  virtual void Enter(OtherPlayer* self);
  virtual void Execute(OtherPlayer* self);
  virtual void Exit(OtherPlayer* self);
};

//------------------------------------------------------------------------
class OtherPlayerJump : public State<OtherPlayer>
{
private:
  
  OtherPlayerJump(){}

  //copy ctor and assignment should be private
  OtherPlayerJump(const OtherPlayerJump&);
  OtherPlayerJump& operator=(const OtherPlayerJump&); 
  
public:

  static OtherPlayerJump* Instance();
  
  virtual void Enter(OtherPlayer* self);
  virtual void Execute(OtherPlayer* self);
  virtual void Exit(OtherPlayer* self);
};

#endif