#include "FunctionNPCOwnedStates.h"

#include "FunctionNPC.h"
#include "GameActorAnimation.h"
#include "../../utils/GameUtils.h"
#include "BaseFighterOwnedCommand.h"
#include "../skill/GameFightSkill.h"
#include "../GameSceneState.h"

//-----------------------------------------------------------------------Global state
FunctionNPCGlobalState* FunctionNPCGlobalState::Instance()
{
  static FunctionNPCGlobalState instance;

  return &instance;
}

void FunctionNPCGlobalState::Execute(FunctionNPC* self)
{

}

//---------------------------------------

FunctionNPCStand* FunctionNPCStand::Instance()
{
  static FunctionNPCStand instance;

  return &instance;
}

void FunctionNPCStand::Enter(FunctionNPC* self)
{
	// update the display status of the role
	self->updateWaterStatus();
	self->updateCoverStatus();

	self->changeAnimation(NPC_ANI_STAND, self->getAnimDir(), true, true);
}

void FunctionNPCStand::Execute(FunctionNPC* self)
{
}

void FunctionNPCStand::Exit(FunctionNPC* self)
{
}


//------------------------------------------------------------------------
FunctionNPCRun* FunctionNPCRun::Instance()
{
  static FunctionNPCRun instance;

  return &instance;
}


void FunctionNPCRun::Enter(FunctionNPC* self)
{  

}


void FunctionNPCRun::Execute(FunctionNPC* self)
{

}

void FunctionNPCRun::Exit(FunctionNPC* self)
{

}


//------------------------------------------------------------------------
FunctionNPCAttack* FunctionNPCAttack::Instance()
{
  static FunctionNPCAttack instance;

  return &instance;
}


void FunctionNPCAttack::Enter(FunctionNPC* self)
{  

}


void FunctionNPCAttack::Execute(FunctionNPC* self)
{

}

void FunctionNPCAttack::Exit(FunctionNPC* self)
{

}

//------------------------------------------------------------------------
FunctionNPCDeath* FunctionNPCDeath::Instance()
{
  static FunctionNPCDeath instance;

  return &instance;
}


void FunctionNPCDeath::Enter(FunctionNPC* self)
{  

}

void FunctionNPCDeath::Execute(FunctionNPC* self)
{

}

void FunctionNPCDeath::Exit(FunctionNPC* self)
{

}