#include "OtherPlayer.h"

#include "OtherPlayerOwnedStates.h"
#include "BaseFighterOwnedCommand.h"
#include "../GameSceneState.h"
#include "../../utils/GameUtils.h"
#include "ActorCommand.h"
#include "GameActorAnimation.h"
#include "../skill/GameFightSkill.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/GameSceneEffects.h"
#include "BaseFighterConstant.h"
#include "ActorUtils.h"
#include "General.h"
#include "ShowBaseGeneral.h"

static std::string animActionNameList[] =
{
    "stand",
    "run",
    "attack1",
    "attack2",
    "death",
}; 

OtherPlayer::OtherPlayer()
{
	setType(GameActor::type_player);

	mActiveRole = new ActiveRole();

	// logic init
	setAnimDir(2);

	// the statemachine init must be put at the end
	// because some animations' instance will be used in the state::Enter() function
	m_pStateMachine = new StateMachine<OtherPlayer>(this);
    //m_pStateMachine->SetCurrentState(MyPlayerStand::Instance());
	//m_pStateMachine->ChangeState(MyPlayerStand::Instance());
    m_pStateMachine->SetGlobalState(OtherPlayerGlobalState::Instance());
}

OtherPlayer::~OtherPlayer()
{
	delete m_pStateMachine;

	delete mActiveRole;

	// 移除属于自己的“展示”武将
	this->removeShowGeneral();
}

bool OtherPlayer::init(const char* name)
{
	m_pFootEffectContainer = Node::create();
	m_pFootEffectContainer->setCascadeOpacityEnabled(true);
	this->addChild(m_pFootEffectContainer, GameActor::ACTOR_FOOTEFFECT_ZORDER);
	m_pFootEffectContainer->setTag(BaseFighter::kTagFootEffectContainer);

	addShadow();

	std::string roleName;
	if(strcmp(name, "") == 0)
		roleName = BasePlayer::getDefaultAnimation(this->getProfession());
	else
		roleName = name;
	//m_pAnim = new GameActorAnimation();
	//m_pAnim->load("animation/player/", animActionNameList, roleName.c_str(), ANI_COMPONENT_MAX, 5);
	m_pAnim = GameActorAnimation::create("animation/player/", animActionNameList, roleName.c_str(), ANI_COMPONENT_MAX, 5);
	m_pAnim->showComponent(ANI_COMPONENT_REFLECTION, false);
	//BasePlayer::switchWeapon(this->getActiveRole()->hand().c_str(), m_pAnim);   // "lance02"
	this->addChild(m_pAnim, GameActor::ACTOR_BODY_ZORDER);

	m_pHeadEffectContainer = Node::create();
	m_pHeadEffectContainer->setCascadeOpacityEnabled(true);
	this->addChild(m_pHeadEffectContainer, GameActor::ACTOR_HEAD_ZORDER);
	m_pHeadEffectContainer->setTag(BaseFighter::kTagHeadEffectContainer);

	this->setContentSize(Size(64, 100));

	this->setScale(BASEFIGHTER_BASE_SCALE);

	//this->GetFSM()->ChangeState(OtherPlayerStand::Instance());

	return true;
}

void OtherPlayer::initWeaponEffect()
{
	// first
	this->removeAllWeaponEffects();

	for (int i = 0;i <  this->getActiveRole()->equipmentshowinfo_size();i++)
	{
		const PlayerEquipmentShowInfo& equipmentInfo = this->getActiveRole()->equipmentshowinfo(i);
		if (equipmentInfo.clazz() == 1)
		{
			std::string pressionStr = this->getActiveRole()->profession();
			int pression_ = BasePlayer::getProfessionIdxByName(pressionStr);
			int refineLevel_ = equipmentInfo.gradelevel();
			int starLevel_ = equipmentInfo.starlevel();

			std::string effectNameForRefine = ActorUtils::getWeapEffectByRefineLevel(pression_,refineLevel_);
			addWeaponEffect(effectNameForRefine.c_str());

			std::string effectNameForStar = ActorUtils::getWeapEffectByStarLevel(pression_, starLevel_);
			addWeaponEffect(effectNameForStar.c_str());

			break;
		}
	}
}

void OtherPlayer::onRemoveFromGameScene()
{
	// remove the musou special effect
	auto actorLayer = this->getGameScene()->getActorLayer();
	auto effect = dynamic_cast<LinkSpecialEffect*>(actorLayer->getChildByTag((int)this->getRoleId()));
	if(effect != NULL)
	{
		effect->removeFromParent();
	}
}

void OtherPlayer::onEnter()
{
	Node::onEnter();

	changeAction(ACT_STAND);
}

void OtherPlayer::setAnimScale(float scale)
{
	m_animScale = scale;
	//for(int i = 0; i < ANI_MAX; i++)
	//{
	//	CCLegendAnimation* body = m_pAnimBody[i];
	//	if(body != NULL)
	//		body->setScale(scale);
	//}
	this->m_pAnim->setScale(scale);
}

Vec2 OtherPlayer::getSortPoint()
{
	return m_worldPosition;
}

void OtherPlayer::changeAction(int action)
{
	switch(action)
	{
	case ACT_STAND:
		GetFSM()->ChangeState(OtherPlayerStand::Instance());
		break;

	case ACT_RUN:
		GetFSM()->ChangeState(OtherPlayerRun::Instance());
		break;

	case ACT_ATTACK:
		GetFSM()->ChangeState(OtherPlayerAttack::Instance());
		break;

	case ACT_DIE:
		GetFSM()->ChangeState(OtherPlayerDeath::Instance());
		stopAllCommand();
		break;

	case ACT_BE_KNOCKBACK:
		GetFSM()->ChangeState(OtherPlayerKnockBack::Instance());
		stopAllCommand();
		break;

	case ACT_CHARGE:
		GetFSM()->ChangeState(OtherPlayerCharge::Instance());
		stopAllCommand();
		break;

	case ACT_JUMP:
		GetFSM()->ChangeState(OtherPlayerJump::Instance());
		break;
	}
}

bool OtherPlayer::isAction(int action)
{
	switch(action)
	{
	case ACT_STAND:
		if(GetFSM()->isInState(*(OtherPlayerStand::Instance())))
			return true;
		return false;
		break;

	case ACT_RUN:
		if(GetFSM()->isInState(*(OtherPlayerRun::Instance())))
			return true;
		return false;
		break;

	case ACT_ATTACK:
		if(GetFSM()->isInState(*(OtherPlayerAttack::Instance())))
			return true;
		return false;
		break;

	case ACT_DIE:
		if(GetFSM()->isInState(*(OtherPlayerDeath::Instance())))
			return true;
		return false;
		break;

	case ACT_BE_KNOCKBACK:
		if(GetFSM()->isInState(*(OtherPlayerKnockBack::Instance())))
			return true;
		return false;
		break;

	case ACT_CHARGE:
		if(GetFSM()->isInState(*(OtherPlayerCharge::Instance())))
			return true;
		return false;
		break;
	}

	//CCAssert(false, "action has not been handled");
	return false;
}

bool OtherPlayer::isStanding()
{
	if(GetFSM()->isInState(*(OtherPlayerStand::Instance())))
		return true;

	return false;
}

void OtherPlayer::update(float dt)
{
	m_dt = dt;

	drive();

	running(dt);   // 全局状态

	m_pStateMachine->Update();

	updateMusouState();

	updateGeneralShow();
}

void OtherPlayer::onAttack( const char* skillId, const char* skillProcessorId, long long targetId, bool bDoubleAttack, int skillType /*= -1*/ )
{
	BasePlayer::onAttack(skillId, skillProcessorId, targetId, bDoubleAttack, skillType);

	this->removeShowGeneral();
}

void OtherPlayer::onDamaged( BaseFighter* source, int damageNum, bool bDoubleAttacked, CSkillResult* skillResult /*= NULL*/ )
{
	BasePlayer::onDamaged(source, damageNum, bDoubleAttacked, skillResult);

	this->removeShowGeneral();
}

void OtherPlayer::removeShowGeneral()
{
	auto scene = this->getGameScene();
	if (NULL != scene)
	{
		for (std::map<long long,GameActor*>::iterator it = scene->getActorsMap().begin(); it != scene->getActorsMap().end(); ++it)
		{
			auto pGeneral = dynamic_cast<General*>(it->second);
			if(NULL != pGeneral)
			{
				if (this->getRoleId() == pGeneral->getOwnerId() && pGeneral->getRoleId() < 0)
				{
					pGeneral->setDestroy(true);
				}
			}
		}
	}
}

void OtherPlayer::updateGeneralShow()
{
	auto pShowOtherGeneral = new ShowOtherGeneral();
	pShowOtherGeneral->set_otherPlayer(this);
	pShowOtherGeneral->autorelease();

	if (pShowOtherGeneral->canGeneralShow())
	{
		pShowOtherGeneral->generalShow();
	}
}
