#include "DecorationActor.h"

#include "../../legend_engine/CCLegendAnimation.h"
#include "../GameSceneState.h"

DecorationActor::DecorationActor()
{
	//this->scheduleUpdate();
}

DecorationActor::~DecorationActor()
{
	//CC_SAFE_RELEASE(m_pAnim);
}

void DecorationActor::loadWithInfo(LevelActorInfo* levelActorInfo)
{
	DecorationActorInfo* actorInfo = (DecorationActorInfo*)levelActorInfo;

	// ����Ψһid
	m_roleId = actorInfo->instanceId + SCENE_ACTOR_BASE_ID;

	// actorclass���
	m_actorClassId = actorInfo->actorClassId;

	// x����
	m_worldPosition.x = actorInfo->posX;
	// y����
	m_worldPosition.y = (actorInfo->posY * 2);   // �ɱ༭������ת��Ϊ��Ϸ��������

	// �������
	m_animActionID = actorInfo->animActionID;

	// FLAG����
	unsigned char flag = actorInfo->flag;		
	// ��ת״̬��0�޷�ת��1�з�ת��
	bool bFlipX = flag & (1 << 0);
	if(bFlipX)
		this->setScaleX(-1.0f);
	// �ɼ��ԣ�0�ɼ���1���ɼ���
	this->setVisible((flag & (1 << 3)) == 0);
	
	//����
	int paramLength = actorInfo->paramLength;
	this->paramData = new short[paramLength];
	for (int i = 0; i < paramLength; i++) {
		this->paramData[i] = actorInfo->paramData[i];
	}

	// ��ȡlayerֵ
	this->m_layerId = paramData[2];



	// load animation
	m_pAnim = new CCLegendAnimation();
	ActorClass* ac = GameMetaData::getActorClass(m_actorClassId);
	m_pAnim->initWithAnmFilename(ac->aniName.c_str());
	m_pAnim->setAction(m_animActionID);

	addChild(m_pAnim);
	m_pAnim->release();
}
