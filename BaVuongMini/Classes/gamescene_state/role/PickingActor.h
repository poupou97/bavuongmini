#ifndef _LEGEND_GAMEACTOR_PICKINGACTOR_H_
#define _LEGEND_GAMEACTOR_PICKINGACTOR_H_

#include "GameActor.h"
#include "cocos2d.h"

class CCLegendAnimation;
class CPushCollectInfo;

#define PICKINGACTOR_ROLE_HEIGHT 50

/**
 * 采集物
 * @author zhaogang
 * @version 0.1.0
 */
class PickingActor : public GameActor
{
public:
	PickingActor();
	virtual ~PickingActor();

	bool init(const char* actorName);

	virtual void loadWithInfo(LevelActorInfo* levelActorInfo);

	virtual void showActorName(bool bShow);

	CPushCollectInfo * p_collectInfo;
	bool getActivied();
	void setActivied(bool isactive);

	void set_templateId(long _value);
	long get_templateId();

private:
	bool isActivied;
	CCLegendAnimation* m_pAnim;
	long m_nTemplateId;
};

#endif