#include "FunctionNPC.h"

#include "FunctionNPCOwnedStates.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "GameActorAnimation.h"
#include "../GameSceneState.h"
#include "../skill/GameFightSkill.h"
#include "../../messageclient/element/CNpcInfo.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../legend_engine/GameWorld.h"
#include "../GameSceneState.h"
#include "GameView.h"
#include "../../messageclient/protobuf/MissionMessage.pb.h"
#include "../../ui/missionscene/MissionManager.h"
#include "../../utils/GameUtils.h"
#include "AppMacros.h"
#include "BaseFighterConstant.h"
#include "../../utils/StaticDataManager.h"

static std::string NPCAnimActionNameList[] =
{
    "stand",
    "run",
    "attack",
    "death",
};

#define ELEMENT_NUM 24
static FunctionNPC::FunctionIcon FUNCTION_ICON_DEF[ELEMENT_NUM] = 
{
	{-4, "res_ui/npc_icon/jishouhang.png"}, 
	{-5, "res_ui/npc_icon/jiuguanrenwu.png"},  
	{-6, "res_ui/npc_icon/cangku.png"},  
	{-13, "res_ui/npc_icon/sadouchengbing.png"},  
	{-17, "res_ui/npc_icon/yaopin.png"},  
	{-18, "res_ui/npc_icon/tiejiang.png"}, 
	{-50, "res_ui/npc_icon/songshirenwu.png"},  
	{-51, "res_ui/npc_icon/songshirenwu.png"}, 
	{-52, "res_ui/npc_icon/songshirenwu.png"}, 
	{-53, "res_ui/npc_icon/songshirenwu.png"},  
	{-54, "res_ui/npc_icon/jiazu.png"}, 
	{-55, "res_ui/npc_icon/yexirenwu.png"}, 
	{-67, "res_ui/npc_icon/nanzhengbeizhan.png"}, 
	//{-87, "res_ui/npc_icon/chonghuanzhuanyuan.png"},
	{-99, "res_ui/npc_icon/guoguanzhanjiang.png"},
	{-100, "res_ui/npc_icon/chuansong.png"},
	{-101, "res_ui/npc_icon/chuansong.png"},
	{-102, "res_ui/npc_icon/chuansong.png"},
	{-103, "res_ui/npc_icon/chuansong.png"},
	{-104, "res_ui/npc_icon/caochuanjiejian.png"},
	{-108, "res_ui/npc_icon/jishouhang.png"},
	{-109, "res_ui/npc_icon/cangku.png"},
	{-110, "res_ui/npc_icon/yaopin.png"},
	{-111, "res_ui/npc_icon/tiejiang.png"},
	{-112, "res_ui/npc_icon/shijieboss.png"},
};

FunctionNPC::FunctionNPC()
{
	//this->scheduleUpdate();
	setType(GameActor::type_npc);

	//setMoveSpeed(40);

	setAnimDir(0);

	// the statemachine init must be put at the end
	// because some animations' instance will be used in the state::Enter() function
	m_pStateMachine = new StateMachine<FunctionNPC>(this);
    //m_pStateMachine->SetCurrentState(MyPlayerStand::Instance());
	//m_pStateMachine->ChangeState(MyPlayerStand::Instance());
    //m_pStateMachine->SetGlobalState(MonsterGlobalState::Instance());

	mActiveRole = new ActiveRole();
}

FunctionNPC::~FunctionNPC()
{
	//CC_SAFE_RELEASE(m_pCircle);

	//CC_SAFE_DELETE(m_pAnim);

	delete m_pStateMachine;

	delete mActiveRole;

	std::vector<MissionInfo*>::iterator iter;
	for (iter = npcMissionList.begin(); iter != npcMissionList.end(); ++iter)
	{
		delete *iter;
	}
	npcMissionList.clear();
}

Vec2 FunctionNPC::getSortPoint()
{
	// ��������
	return Vec2(m_worldPosition.x, m_worldPosition.y);
}

Rect FunctionNPC::getVisibleRect()
{
	// 
	//CCLegendAnimation* anim = this->m_pAnim->getAnim(ANI_COMPONENT_BODY, this->getAnimIdx());
	//return anim->boundingBox();

	//this->setContentSize(Size(100, 100));
	//Size size = this->getContentSize();

	int width = 64;
	int height = 100;
	Rect tmpRect = Rect(-width/2, 0, width, height);
	return tmpRect;
	//return Rect(this->getWorldPosition().x + tmpRect.origin.x, this->getWorldPosition().y + tmpRect.origin.y,
	//	100, 100);
}

bool FunctionNPC::init(const char* roleName)
{
	m_pFootEffectContainer = Node::create();
	m_pFootEffectContainer->setCascadeOpacityEnabled(true);
	this->addChild(m_pFootEffectContainer, GameActor::ACTOR_FOOTEFFECT_ZORDER);
	m_pFootEffectContainer->setTag(BaseFighter::kTagFootEffectContainer);

	addShadow();

	// load animation
	//m_pAnim = new CCLegendAnimation();
	//std::string animFileName = "animation/monster/";
	//animFileName.append(monsterName);
	//animFileName.append("/");
	//animFileName.append("stand");
	//animFileName.append(".anm");
	//m_pAnim->initWithAnmFilename(animFileName.c_str());
	//m_animActionID = 0;
	//m_pAnim->setAction(m_animActionID);
	//m_pAnim->retain();
	//addChild(m_pAnim, 2);

	//m_pAnim = new GameActorAnimation();
	//m_pAnim->load("animation/npc/", NPCAnimActionNameList, roleName, ANI_COMPONENT_MAX, NPC_ANI_MAX);
	m_pAnim = GameActorAnimation::create("animation/npc/", NPCAnimActionNameList, roleName, ANI_COMPONENT_MAX, NPC_ANI_MAX);
	m_pAnim->showComponent(ANI_COMPONENT_REFLECTION, false);
	m_pAnim->setAnimName(NPC_ANI_STAND);
	this->addChild(m_pAnim, GameActor::ACTOR_BODY_ZORDER);

	m_pHeadEffectContainer = Node::create();
	m_pHeadEffectContainer->setCascadeOpacityEnabled(true);
	this->addChild(m_pHeadEffectContainer, GameActor::ACTOR_HEAD_ZORDER);
	m_pHeadEffectContainer->setTag(BaseFighter::kTagHeadEffectContainer);

	// ��ANI_STANDݣ�����ȷ��Actor�Ĵ�С
	auto anim = this->m_pAnim->getAnim(ANI_COMPONENT_BODY, NPC_ANI_STAND);
	//return anim->boundingBox();

	//setVisibleRect();

	this->setContentSize(Size(64, 100));

	this->GetFSM()->ChangeState(FunctionNPCStand::Instance());

	m_npcMissionIndicate = Sprite::create("gamescene_state/zhujiemian3/other/jingtan.png");
	m_npcMissionIndicate->setAnchorPoint(Vec2(0.5f,0.5f));
	m_npcMissionIndicate->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT + 35));
	if (strlen(NpcData::s_npcFunctionIcon[m_roleId].c_str()))
	{
		m_npcMissionIndicate->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT + 60));
	}
	this->addChild(m_npcMissionIndicate, GameActor::ACTOR_NAME_ZORDER);
	auto moveToAction = MoveBy::create(0.5f,Vec2(0,-10));
	auto repeapAction = RepeatForever::create(Sequence::create(moveToAction,moveToAction->reverse(),NULL));
	m_npcMissionIndicate->runAction(repeapAction);
	m_npcMissionIndicate->setVisible(false);

	this->setScale(BASEFIGHTER_BASE_SCALE);

	return true;
}

void FunctionNPC::changeAction(int action)
{
	switch(action)
	{
	case ACT_STAND:
		GetFSM()->ChangeState(FunctionNPCStand::Instance());
		break;

	case ACT_RUN:
		GetFSM()->ChangeState(FunctionNPCRun::Instance());
		break;

	case ACT_ATTACK:
		GetFSM()->ChangeState(FunctionNPCAttack::Instance());
		break;

	case ACT_DIE:
		GetFSM()->ChangeState(FunctionNPCDeath::Instance());
		stopAllCommand();
		break;
	}
}

bool FunctionNPC::isAction(int action)
{
	switch(action)
	{
	case ACT_STAND:
		if(GetFSM()->isInState(*(FunctionNPCStand::Instance())))
			return true;
		return false;
		break;

	case ACT_RUN:
		if(GetFSM()->isInState(*(FunctionNPCRun::Instance())))
			return true;
		return false;
		break;

	case ACT_ATTACK:
		if(GetFSM()->isInState(*(FunctionNPCAttack::Instance())))
			return true;
		return false;
		break;

	case ACT_DIE:
		if(GetFSM()->isInState(*(FunctionNPCDeath::Instance())))
			return true;
		return false;
		break;
	}

	//CCAssert(false, "action has not been handled");
	return false;
}

bool FunctionNPC::isStanding()
{
	if(GetFSM()->isInState(*(FunctionNPCStand::Instance())))
		return true;

	return false;
}

bool FunctionNPC::isDead()
{
	if(GetFSM()->isInState(*(FunctionNPCDeath::Instance())))
		return true;

	return false;
}

bool FunctionNPC::isAttacking() 
{
	if( GetFSM()->isInState( *(FunctionNPCAttack::Instance()) ) )
		return true;

	return false;
}

void FunctionNPC::update(float dt)
{
	drive();

	this->running(dt);

	this->GetFSM()->Update();

	if(getGameScene()->isOutOfView(this))
	{
		GameActor* myplayer = getGameScene()->getActor(BaseFighter::getMyPlayerId());
		if(myplayer->getLockedActorId() == getRoleId())
		{
			myplayer->setLockedActorId(NULL_ROLE_ID);
		}
	}
}

#if ROLE_FUNCTIONNPC_DEBUG_DRAW
void FunctionNPC::draw(Renderer *renderer, const Mat4& transform, uint32_t flags)
{
    BaseFighter::draw(renderer, transform, flags);
	return;

    //const Size& s = this->getContentSize();
	Rect rect = getVisibleRect();
    Vec2 vertices[4]={
		Vec2(rect.origin.x,rect.origin.y),
		Vec2(rect.origin.x + rect.size.width, rect.origin.y),
        Vec2(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height),
		Vec2(rect.origin.x, rect.origin.y + rect.size.height),
    };

	//rect.size.width = 100;
	//rect.size.height = 100;
	//Size s = rect.size;
 //   Vec2 vertices[4]={
 //       Vec2(0,0),Vec2(rect.size.width,0),
 //       Vec2(s.width,s.height),Vec2(0,s.height),
 //   };

	DrawPrimitives::drawPoly(vertices, 4, true);
}
#endif

FunctionNPC* FunctionNPC::create(ActiveRole* activeRole, GameSceneLayer* scene)
{
	if(scene == NULL)
		return NULL;

	if (activeRole->type() == GameActor::type_npc) {
		auto actor = new FunctionNPC();
		long long roleId = activeRole->rolebase().roleid();
		actor->setRoleId(roleId);
		scene->putActor(roleId, actor);

		actor->getActiveRole()->CopyFrom(*activeRole);

		actor->setGameScene(scene);
		actor->init(actor->getActiveRole()->rolebase().figure().c_str());

		// add new actor into cocos2d-x render scene
		auto actorLayer = scene->getActorLayer();
		actorLayer->addChild(actor);
		actor->resume();
		actor->release();

		return actor;
	}

	return NULL;
}

void FunctionNPC::loadWithInfo(LevelActorInfo* levelActorInfo)
{
	auto actorInfo = (NPCActorInfo*)levelActorInfo;

	int instanceId = actorInfo->instanceId;
	std::string npc_name = actorInfo->npc_name;
	short rol = actorInfo->rol;
	short col = actorInfo->col;
	char actionID = actorInfo->actionID;
	char flag = actorInfo->flag;
	long templateId = actorInfo->templateId;

	this->getActiveRole()->set_type(GameActor::type_npc);
	this->getActiveRole()->set_hp(100);
	this->getActiveRole()->set_maxhp(10000);
	this->setRoleId(templateId);

	auto npcInfo = GameWorld::NpcInfos[templateId];
	auto base = this->getActiveRole()->mutable_rolebase();
	base->set_name(npcInfo->npcName);
	base->set_gender(1);
	base->set_faceid(npcInfo->icon);
	base->set_figure(npc_name);
	base->set_title("");

	//aActor.setFace(actionID);
	function = 0;

	// ��ת״̬��0�޷�ת��1�з�ת��
	bool bFlipX = flag & (1 << 0);
	// �ɼ��ԣ�0�ɼ1��ɼ
	this->setVisible((flag & (1 << 3)) == 0);

	intro = npcInfo->intro;

	setType(GameActor::type_npc);
	stat = 0;
	mapIcon = npcInfo->mapIcon;
	this->setWorldPosition(Vec2(GameSceneLayer::tileToPositionX(col), GameSceneLayer::tileToPositionY(rol)));

	// load animation resource
	init(npc_name.c_str());
	if(bFlipX)
	{
		this->getAnim()->setScaleX(-1.0f);
		this->GetFSM()->ChangeState(FunctionNPCStand::Instance());   // if the npc has flipX, apply it by calling changeState()
	}

	setActorName(npcInfo->npcName.c_str());
	showActorName(true);

	if(strlen(NpcData::s_npcFunctionIcon[templateId].c_str()))
	{
		showFunctionIcon(true);
	}

	// set the animation's direction by the scene editor's value
	setAnimDir(actionID);
	if(this->getAnim() != NULL)
		this->getAnim()->setAction(this->getAnimDir());
}

bool FunctionNPC::isSpecificFunctionNpc()
{
	for(int i = 0; i < ELEMENT_NUM; i++)
	{
		if(FUNCTION_ICON_DEF[i].id == m_roleId)
		{
			m_index = i;
			m_sFunctionIcon = FUNCTION_ICON_DEF[i].icon;
			m_bIsFunctionIconShow = true;
			return true;
		}
	}
	m_bIsFunctionIconShow = false;
	return false;
}

void FunctionNPC::showFunctionIcon(bool bShow)
{
	Sprite* pIconSprite = (Sprite*)this->getChildByTag(GameActor::kTagFunctionIconAndLabel);
	if(bShow)
	{
		if(pIconSprite == NULL)
		{
			std::string path;
			path.append("res_ui/npc_icon/");
			path.append(NpcData::s_npcFunctionIcon[m_roleId]);
			path.append(".png");
			m_sFunctionIcon = path;
			pIconSprite = Sprite::create(path.c_str());
			pIconSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
			pIconSprite->setTag(GameActor::kTagFunctionIconAndLabel);

			pIconSprite->runAction(
				RepeatForever::create(
					Sequence::create(
						MoveTo::create(0.4f, Vec2(0, BASEFIGHTER_ROLE_HEIGHT+23)), 
						MoveTo::create(0.6f, Vec2(0, BASEFIGHTER_ROLE_HEIGHT+29)), 
					NULL)
					)
				);

			this->addChild(pIconSprite, GameActor::ACTOR_NAME_ZORDER);
		}
		pIconSprite->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT+25));
	}
	else
	{
		if(pIconSprite != NULL)
			pIconSprite->removeFromParent();
	}
}

void FunctionNPC::showActorName(bool bShow)
{
	std::string namestr = getActorName();
	const char* name = namestr.c_str();

	auto nameLabel = (Label*)this->getChildByTag(GameActor::kTagActorName);
	if(bShow)
	{
		if(nameLabel == NULL)
		{
			//nameLabel = this->createNameLabel(name);
            
            // shadow + stroke label
            nameLabel= Label::createWithTTF(name,APP_FONT_NAME,GAMEACTOR_NAME_FONT_SIZE);
            nameLabel->setColor(Color3B(3, 247, 245));   // blue
            auto shadowColor = Color4B::BLACK;   // black
            nameLabel->enableShadow(shadowColor, Size(1.0f, -1.0f), 1.0f);
            nameLabel->setTag(GameActor::kTagActorName);
            
			this->addChild(nameLabel, GameActor::ACTOR_NAME_ZORDER);
		}
		else
		{
			nameLabel->setString(name);
		}
		nameLabel->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT));
	}
	else
	{
		if(nameLabel != NULL)
			nameLabel->removeFromParent();
	}
}

bool FunctionNPC::isHasMission()
{
	if (this->npcMissionList.size()>0)
	{
		return true;
	}
	else 
	{
		return false;
	}
}

void FunctionNPC::updateMissionList()
{
	//delete first
	if (npcMissionList.size()>0)
	{
		std::vector<MissionInfo*>::iterator iter;
		for (iter = npcMissionList.begin(); iter != npcMissionList.end(); ++iter)
		{
			delete *iter;
		}
		npcMissionList.clear();
	}
	
	//add
	for (unsigned int i = 0;i<GameView::getInstance()->missionManager->MissionList.size();i++)
	{
		if (GameView::getInstance()->missionManager->MissionList.at(i)->has_tip())
		{
			if (strcmp(GameView::getInstance()->missionManager->MissionList.at(i)->tip().mapid().c_str(),GameView::getInstance()->getMapInfo()->mapid().c_str()) == 0)
			{
				if (GameView::getInstance()->missionManager->MissionList.at(i)->tip().npcid() == this->getRoleId())
				{
					if (GameView::getInstance()->missionManager->MissionList.at(i)->missionpackagetype() != 12)
					{
						auto temp = new MissionInfo();
						temp->CopyFrom(*GameView::getInstance()->missionManager->MissionList.at(i));
						this->npcMissionList.push_back(temp);
					}
				}
			}
		}
	 	else if (GameView::getInstance()->missionManager->MissionList.at(i)->action().moveto().has_targetnpc())
	 	{
			if (strcmp(GameView::getInstance()->missionManager->MissionList.at(i)->action().moveto().mapid().c_str(),GameView::getInstance()->getMapInfo()->mapid().c_str()) == 0)
			{
				if (GameView::getInstance()->missionManager->MissionList.at(i)->action().moveto().targetnpc().npcid() == this->getRoleId())
				{
					if (GameView::getInstance()->missionManager->MissionList.at(i)->missionpackagetype() != 12)
					{
						auto temp = new MissionInfo();
						temp->CopyFrom(*GameView::getInstance()->missionManager->MissionList.at(i));
						this->npcMissionList.push_back(temp);
					}
				}
			}
	 	}
	}
}

void FunctionNPC::updateNpcState()
{
	if (this->npcMissionList.size()>0)
	{
		for (unsigned int i = 0;i<this->npcMissionList.size();i++)
		{
			if (i > 0)
			{
				switch(this->npcMissionList.at(i)->missionstate())
				{
				case accepted :   ///������
					{
						if (function == 1 || function ==2)
						{}
						else
						{
							function = 3;
						}
					}
					break;
				case dispatched : ///пɽ
					{
						if (function == 2)
						{
						}
						else
						{
							function = 1;
						}
					}
					break;
				case done :      ///ӿ��ύ
					function = 2;
					break;
				}
			}
			else
			{
				switch(this->npcMissionList.at(i)->missionstate())
				{
					//case failed :
					//	function = 0;
				case canceled :
					function = 0;
					break;
				case submitted :
					function = 0;
					break;
				case accepted :   ///�����
					function = 3;
					break;
				case dispatched : ///пɽ
					function = 1;
					break;
				case done :      ///ӿ��ύ
					function = 2;
					break;
				}
			}
		}
	}
	else
	{
		//did not need flag
		function = 0;
	}
	
}

void FunctionNPC::updateNpcStateIcon()
{
	switch((int)function)
	{
	case 0 :
		{
			//CCLOG("npc state: 0");
			m_npcMissionIndicate->setVisible(false);
			if (this->getNpcCircleIndicate())
			{
				this->getNpcCircleIndicate()->setVisible(false);
			}
			else
			{
				this->createNpcCircleIndicate();
				this->getNpcCircleIndicate()->setVisible(false);
			}
		}
		break;
	case 1 : ///�ɽ
		{
			//CCLOG("npc state: 1");
			auto texture = Director::getInstance()->getTextureCache()->addImage("gamescene_state/zhujiemian3/other/jingtan.png");
			m_npcMissionIndicate->setTexture(texture);
			m_npcMissionIndicate->setVisible(true);
			GameUtils::removeGray(m_npcMissionIndicate);

			if (this->getNpcCircleIndicate())
			{
				this->getNpcCircleIndicate()->setVisible(true);
			}
			else
			{
				this->createNpcCircleIndicate();
				this->getNpcCircleIndicate()->setVisible(true);
			}
		}
		break;
	case 2 :///ӿ��ύ
		{
			//CCLOG("npc state: 2");
			auto texture = Director::getInstance()->getTextureCache()->addImage("gamescene_state/zhujiemian3/other/yiwen.png");
			m_npcMissionIndicate->setTexture(texture);
			m_npcMissionIndicate->setVisible(true);
			GameUtils::removeGray(m_npcMissionIndicate);

			if (this->getNpcCircleIndicate())
			{
				this->getNpcCircleIndicate()->setVisible(true);
			}
			else
			{
				this->createNpcCircleIndicate();
				this->getNpcCircleIndicate()->setVisible(true);
			}
		}
		break;
	case 3 : ///�����
		{
			//CCLOG("npc state: 3");
			auto texture = Director::getInstance()->getTextureCache()->addImage("gamescene_state/zhujiemian3/other/yiwen.png");
			m_npcMissionIndicate->setTexture(texture);
			m_npcMissionIndicate->setVisible(true);
			GameUtils::addGray(m_npcMissionIndicate);

			if (this->getNpcCircleIndicate())
			{
				this->getNpcCircleIndicate()->setVisible(true);
			}
			else
			{
				this->createNpcCircleIndicate();
				this->getNpcCircleIndicate()->setVisible(true);
			}
		}
		break;
	case 4 :
		{
			//CCLOG("npc state: 4");
			m_npcMissionIndicate->setVisible(false);
			if (this->getNpcCircleIndicate())
			{
				this->getNpcCircleIndicate()->setVisible(false);
			}
			else
			{
				this->createNpcCircleIndicate();
				this->getNpcCircleIndicate()->setVisible(false);
			}
		}
		break;
	}
}

void FunctionNPC::RefreshNpc()
{
	this->updateMissionList();
	//if(isHasMission())
	//{
		this->updateNpcState();
		this->updateNpcStateIcon();
	//}
}

#define kTag_NpcCircleIndicate 300
CCLegendAnimation * FunctionNPC::getNpcCircleIndicate()
{
	auto temp = (CCLegendAnimation *)this->getChildByTag(kTag_NpcCircleIndicate);
	if (temp)
		return temp;

	return NULL;
}

void FunctionNPC::createNpcCircleIndicate()
{
	auto npcCircleIndicate = CCLegendAnimation::create("animation/texiao/renwutexiao/shou/circle.anm");
	npcCircleIndicate->setPlayLoop(true);
	npcCircleIndicate->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT + 35));
	npcCircleIndicate->setPlaySpeed(0.5f);
	if (strlen(NpcData::s_npcFunctionIcon[m_roleId].c_str()))
	{
		npcCircleIndicate->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT + 60));
	}
	this->addChild(npcCircleIndicate, GameActor::ACTOR_NAME_ZORDER-1,kTag_NpcCircleIndicate);
	npcCircleIndicate->setScale(0.7f);
	auto scaleAction = ScaleTo::create(1.2f,1.05f,1.05f);
	//FadeTo * fadeToAction = FadeTo::create(1.2f,130);
	auto fadeToAction = FadeOut::create(1.2f);
	auto sequence_npcCircleIndicate = Sequence::create(Spawn::create(scaleAction,fadeToAction,NULL),ScaleTo::create(0.01f,0.7f,0.7f),FadeTo::create(0.01f,255),NULL);
	auto repeapAction_npcCircleIndicate = RepeatForever::create(sequence_npcCircleIndicate);
	npcCircleIndicate->runAction(repeapAction_npcCircleIndicate);
}



