#ifndef _LEGEND_GAMEACTOR_ACTORUTILS_H_
#define _LEGEND_GAMEACTOR_ACTORUTILS_H_

#include "cocos2d.h"

#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../messageclient/GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

USING_NS_CC;

class GameActorAnimation;
class BaseFighter;
class CCLegendAnimation;

class ActorUtils {
public:
	/**
	 * 根据形象名,、武器名、武器特效名，生成动画, 放置于界面上, 用于展示
	 * @param figureName 具备自解释能力
	 *       比如zsgr_xxx 表示玩家类动画
	 *             zsgg_xxx 表示武将类动画
	 * @param weaponName 武器名
	 * @param weaponEffectName 武器特效名
	 * 
	 * 目前, 只生成站立且脸朝正面的动画形象
	 */
	static GameActorAnimation* createActorAnimation(const char* figureName, const char* weaponName, const char* weaponEffectFireName = "", const char* weaponEffectSparkleName = "");

	static std::string getActorFigureName(ActiveRole& activerole);

	// show the skill name at the head of the BaseFighter
	static Node* createSkillNameLabel(const char* skillName);
	// add art skill name to the game scene
	static void addSkillName(BaseFighter* actor, const char* skillId, const char* skillProcessorId);

	// 获取技能的默认释放地点
	static Vec2 getDefaultSkillPosition(BaseFighter* attacker, const char* skillId, const char* skillProcessorId);

	static Sprite* createGeneralRankSprite(int nRank);

	static void addMultiAttackDamageNumber(BaseFighter* defender, int damageNum, int attacktime);
	// 暴击数字效果
	static void addDoubleAttackDamageNumber(BaseFighter* target, int damageNum, const char* fontName);

	static std::string getWeapEffectByRefineLevel(int weapPression,int equipRefineLevel);
	static std::string getWeapEffectByStarLevel(int weapPression,int equipStarLevel);


	static void addMusouBodyEffect(BaseFighter* target, int color = 0xFFFFFF);

	// 增加阵法特效
	static void addStrategyEffect(BaseFighter* player, int fightWayId);
	static void removeStrategyEffect(BaseFighter* player);
	//add flower particle
	static void addFlowerParticle(BaseFighter* player);
	static void removeFlowerParticle(BaseFighter * player);

	/** pre-load all animations of one player's skills **/
	static void preloadSkillEffects(int profession);

	static void addGoldMessageToScene(int newGold);

private:
	static std::map<std::string, CCLegendAnimation*> s_skillsEffectsCache;
};

#endif