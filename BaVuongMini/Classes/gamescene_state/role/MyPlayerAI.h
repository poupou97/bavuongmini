#ifndef _LEGEND_GAMEACTOR_MYPLAYERAI_H_
#define _LEGEND_GAMEACTOR_MYPLAYERAI_H_

#include "cocos2d.h"
USING_NS_CC;

class MyPlayer;

/**
 * 玩家自己的挂机模块
 * 挂机负责AI，即何时吃药、何时逃跑、何时使用何种技能等。
 * MyPlayer可以允许有多个挂机模式，但一次只允许一个起作用
 * 借助各种Command，实现挂机功能
 * @author zhaogang
 * @version 0.1.0
 */
class MyPlayerAI : public Node
{
public:
	MyPlayerAI();
	virtual ~MyPlayerAI();

	virtual void update(float dt);

	void setMyPlayer(MyPlayer* self);

	void start();
	void stop();

	// begin to use skill to kill
	void startFight();

	void updateSkillList(std::string* list);

	// 设置挂机的起始点
	void setStartPoint(Vec2 startPoint);

	//当技能为0级时，重置挂机技能列表
	void reSetSkillList(std::string skillid);

	//得到当时可以吃药的武将id - 血少的武将优先吃
	long long getUsePotionGeneralIndex(std::vector<long long> fightGeneral);

	void setIsShowBuyPotionIsNotEnough(bool value_);
	bool getIsShowBuyPotionIsNotEnough();

	/**
		* 随机移动一段距离：范围range
		*/
	static void autoMove(MyPlayer* me, float range);

private:
	// 检测是否位于挂机范围之内
	bool isInRange();

	// 超出了挂机设定的范围，往回走
	void moveBack();

	// 某些情况下，挂机系统不能进行自动战斗
	bool canAutoCombat();

	// 根据规则，使用技能
	bool autoUseSkill();
	// 挂机时，使用技能
	bool useSkill(std::string& skillId);

	bool usePotion(const char* potionName, long long roleId = 0);
	//远程购买
	void robotBuyPotion(const char *potionName,int potionAmount );
	bool remindBuyPotionMoneyIsNotEnoug;
	//int remindBuyPotionTimeCd;
	
	int remindUsePotionTimeCd;
private:
	MyPlayer* m_pMyPlayer;

	// 挂机的起始位置
	Vec2 m_startPoint;
public:
	// 挂机技能列表
	std::vector<std::string> m_skillList;
};

#endif