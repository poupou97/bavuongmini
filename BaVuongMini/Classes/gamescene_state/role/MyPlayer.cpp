#include "MyPlayer.h"

#include "MyPlayerSimpleAI.h"
#include "MyPlayerAI.h"
#include "General.h"
#include "ActorUtils.h"
#include "MyPlayerOwnedStates.h"
#include "MyPlayerOwnedCommand.h"
#include "ActorCommand.h"
#include "GameActorAnimation.h"
#include "PickingActor.h"
#include "BaseFighterConstant.h"
#include "../GameSceneState.h"
#include "../skill/GameFightSkill.h"
#include "../exstatus/ExStatusType.h"
#include "../../utils/GameUtils.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../messageclient/element/CTeamMember.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CEquipment.h"
#include "../sceneelement/MyPlayerInfoMini.h"
#include "../../legend_script/ScriptManager.h"
#include "ShowBaseGeneral.h"

#define voidFramePath "res_ui/di_none.png"

#define whiteFramePath "res_ui/di_white.png"
#define greenFramePath "res_ui/di_green.png"
#define blueFramePath "res_ui/di_bule.png"
#define purpleFramePath "res_ui/di_purple.png"
#define orangeFramePath "res_ui/di_orange.png"

#define MYPLAYER_GREEN_BLOOD_DURATION 8.0f

static std::string animActionNameList[] =
{
    "stand",
    "run",
    "attack1",
    "attack2",
    "death",
}; 

MyPlayer::MyPlayer()
: mMovedDistance(0),
m_musouSkillid(""),
m_physicalValue(0),
m_physicalCapacity(0),
m_angerValue(0),
m_angerCapacity(1200),
m_playerAI(NULL)
{
	setType(GameActor::type_player);

	player = new GamePlayer();

	this->setActorName("Player");

	setPKMode(BasePlayer::PKMode_peace);

    setAnimDir(ANIM_DIR_DOWN);

	// the statemachine init must be put at the end
	// because some animations' instance will be used in the state::Enter() function
	m_pStateMachine = new StateMachine<MyPlayer>(this);
    //m_pStateMachine->SetCurrentState(MyPlayerStand::Instance());
	//m_pStateMachine->ChangeState(MyPlayerStand::Instance());
    m_pStateMachine->SetGlobalState(MyPlayerGlobalState::Instance());
}

MyPlayer::~MyPlayer()
{
	delete m_pStateMachine;

	delete player;

	m_AliveGenerals.clear();

	// delete array
	//for(int i = 0; i < ANI_MAX; i++)   
	//	CC_SAFE_DELETE(m_pAnimWeapons[i]);  

	//CC_SAFE_DELETE(m_pAnim);
}

void MyPlayer::setAnimScale(float scale)
{
	m_animScale = scale;
	//for(int i = 0; i < ANI_MAX; i++)
	//{
	//	CCLegendAnimation* body = m_pAnimBody[i];
	//	if(body != NULL)
	//		body->setScale(scale);
	//}
	this->m_pAnim->setScale(scale);
}

Vec2 MyPlayer::getSortPoint()
{
	return m_worldPosition;
}

void MyPlayer::runToDirection(Vec2 dir, float dt)
{
		//float speed = 180.0f;
		float speed = getMoveSpeed();
		float distance = speed * dt;
		Vec2 offset = dir;
		offset = offset* distance;
		//offset.x = (int)offset.x;
		//offset.y = (int)offset.y;

		float targetX = getWorldPosition().x + offset.x;
		float targetY = getWorldPosition().y - offset.y;
		//int targetTileX = m_gameScene->positionToTileX(targetX);
		//int targetTileY = m_gameScene->positionToTileY(targetY);
		//// collide, slide or stop
		//if(m_gameScene->isLimitOnGround(targetTileX, targetTileY))
		//{
		//	// slide on x
		//	if(!m_gameScene->isLimitOnGround(targetTileX, this->m_gameScene->positionToTileY(getWorldPosition().y)))
		//	{
		//		offset.y = 0;
		//	}
		//	// slide on y
		//	else if(!m_gameScene->isLimitOnGround(m_gameScene->positionToTileX(getWorldPosition().x), targetTileY))
		//	{
		//		offset.x = 0;
		//	}
		//	// stop
		//	else
		//	{
		//		offset.x = 0;
		//		offset.y = 0;
		//	}
		//}

		if(offset.x != 0 || offset.y != 0)
		{
			//if(getCommand()->
			//stopAllCommand();
			//if(!GetFSM()->isInState(MyPlayerStand::Instance())
			//this->GetFSM()->ChangeState(MyPlayerStand::Instance());

			targetX = getWorldPosition().x + offset.x;
			targetY = getWorldPosition().y - offset.y;

			//clearPath();
			//m_pPaths->push_back(new Vec2(targetX, targetY));
			//this->GetFSM()->ChangeState(MyPlayerRun::Instance());

			//// setup move command
			auto cmd = new MyPlayerCommandMove();
			cmd->autoMove = false;
			cmd->targetPosition = Vec2(targetX, targetY);
			cmd->method = MyPlayerCommandMove::method_direct;
			this->setNextCommand(cmd, true);

			//searchPath(Vec2(targetX, targetY));

			//m_pathLength = offset.getLength();
			//this->mMovedDistance = 0;
		}
}

void MyPlayer::updatePos(float dt)
{

}

GameFightSkill* MyPlayer::getGameFightSkill(const char* skillId, const char* skillProcessorId)
{
	std::map<std::string,GameFightSkill*>::const_iterator cIter;
	cIter = GameView::getInstance()->GameFightSkillList.find(skillId);
	if (cIter == GameView::getInstance()->GameFightSkillList.end()) // 没找到就是指向END了  
	{
		return BaseFighter::getGameFightSkill(skillId, skillProcessorId);
		//return NULL;
	}
	else
	{
		return GameView::getInstance()->GameFightSkillList[skillId];
	}
}

GameFightSkill* MyPlayer::getMusouSkill()
{
	std::map<std::string,GameFightSkill*>::const_iterator cIter;
	cIter = GameView::getInstance()->GameFightSkillList.find(m_musouSkillid);
	if (cIter == GameView::getInstance()->GameFightSkillList.end()) // 没找到就是指向END了  
	{
		return NULL;
	}
	else
	{
		return GameView::getInstance()->GameFightSkillList[m_musouSkillid];
	}
	
}

void MyPlayer::setMusouSkill( const char* skillId)
{
	m_musouSkillid = skillId;
}


void MyPlayer::checkAttack()
{
	CCAssert(getCommand()->getType() == ActorCommand::type_attack, "invalid command");
	MyPlayerCommandAttack* cmd = dynamic_cast<MyPlayerCommandAttack*>(this->getCommand());

	// if the player is attacking, ignore
	if(isAttacking())
		return;

	//CCAssert(getGameFightSkill(cmd->skillId.c_str()) != NULL, "should not be nil");
	if(getGameFightSkill(cmd->skillId.c_str(), cmd->skillProcessorId.c_str()) == NULL)
    {
        CCLOG("skill id: %s is null", cmd->skillId.c_str());
        return;
    }
	char targetScopeType = this->getGameFightSkill(cmd->skillId.c_str(), cmd->skillProcessorId.c_str())->getTargetScopeType();
	if(!GameFightSkill::isNoneLock(targetScopeType))   // no need to lock a target
	{
		BaseFighter* bf = NULL;
		if(cmd != NULL)
		{
			bf = dynamic_cast<BaseFighter*>(m_gameScene->getActor(cmd->targetId));
			if(bf == NULL)
				return;
		}

		// 调整攻击方向
		// get direction by actor's current position and target pos
		GameSceneLayer* scene = this->getGameScene();
		int animDirection = getAnimDirection(scene->convertToCocos2DWorldSpace(getWorldPosition()), 
			scene->convertToCocos2DWorldSpace(bf->getWorldPosition()), DEFAULT_ANGLES_DIRECTION_DATA);
		this->setAnimDir(animDirection);

		// 目标已经死亡，则不再进行追击，仅仅象征性的做完攻击动作
		if(bf->isAction(ACT_DIE))
		{
			GetFSM()->ChangeState(MyPlayerAttack::Instance(), false);
			return;
		}

		// move to attack target
		// 减少一些技能的距离，使角色尽量走进攻击目标。用于尽量避免发生“攻击距离不够”的情形
		int atk_distance = getGameFightSkill(cmd->skillId.c_str(), cmd->skillProcessorId.c_str())->getDistance() - 8;   // the attack distance of current skill
		//Vec2 targetWorldPosition = bf->getWorldPosition();
		Vec2 targetWorldPosition = bf->getRealWorldPosition();   // use the REAL world position
		int curDistance = (int)getWorldPosition().getDistance(targetWorldPosition);
		// 大于攻击距离，则追踪
		if(curDistance > atk_distance)
		{
			// 已经走到目标点，或者目标已经离开目标点距离大于攻击距离
			if (getPaths()->size() == 0) {   
				// 寻路目的地：离目标还有攻击距离远的地方
				Vec2 offset = GameUtils::getDirection(getWorldPosition(), targetWorldPosition);
				offset = offset*atk_distance;
				Vec2 target = targetWorldPosition - offset;

				short tileX = m_gameScene->positionToTileX(target.x);
				short tileY = m_gameScene->positionToTileY(target.y);
				if(m_gameScene->isLimitOnGround(tileX, tileY))   // 若是碰撞层，则直接移动到目标身上
				{
					searchPath(targetWorldPosition);
				}
				else
				{
					searchPath(target);
				}
			}
			if(getPaths()->size() > 0)
				GetFSM()->ChangeState(MyPlayerRun::Instance(), false);
			return;
		}
	}
	else
	{
		// now, attack directly
	}

	// 根据技能情况，决定使用哪种攻击动作( to do )
	// 目前，都是转到了MyPlayerAttack
	GetFSM()->ChangeState(MyPlayerAttack::Instance(), false);
	//if(cmd->skillId == FightSkill_SKILL_ID_PUTONGGONGJI)
	//{
	//	GetFSM()->ChangeState(MyPlayerAttack::Instance(), false);
	//}
	//else if(cmd->skillId == FightSkill_SKILL_ID_HUOQIUSHU)
	//{
	//	GetFSM()->ChangeState(MyPlayerWhirl::Instance(), false);
	//}
}


void MyPlayer::checkPick()
{
	CCAssert(getCommand()->getType() == ActorCommand::type_collectPresent, "invalid command");

	int commandType = getCommand()->getType();
	auto cmd = dynamic_cast<MyPlayerCommandPick*>(this->getCommand());

	Vec2 cp_role = GameView::getInstance()->myplayer->getWorldPosition();
	Vec2 cp_target = cmd->targetPos;

	if (cp_role.getDistance(cp_target) < 100)
	{
		if (GameView::getInstance()->getGameScene()->getActor(cmd->targetId) != NULL)
		{
			if (!GameView::getInstance()->myplayer->isAction(ACT_PICK))
			{
				GameView::getInstance()->myplayer->setLockedActorId(cmd->targetId);

				GetFSM()->ChangeState(MyPlayerPick::Instance(), false);
			}
		}
		else
		{
			// 宝箱已经被别人开走了
			GetFSM()->ChangeState(MyPlayerStand::Instance());
		}
	}
	else
	{
		if (getPaths()->size() == 0)
		{
			searchPath(cp_target);
		}

		if(getPaths()->size() > 0)
		{
			GetFSM()->ChangeState(MyPlayerRun::Instance(), false);
		}
			
		return ;
	}
}

bool MyPlayer::isDead()
{
	// 死亡状态，不再接收新的指令
	if(GetFSM()->isInState(*(MyPlayerDeath::Instance())))
		return true;

	return false;
}

bool MyPlayer::isAttacking() 
{ 
	if( GetFSM()->isInState( *(MyPlayerAttack::Instance()) ) )
		return true;

	return false;
}

bool MyPlayer::isMoving()
{
	if( GetFSM()->isInState( *(MyPlayerRun::Instance()) ) )
		return true;

	return false;
}

void MyPlayer::changeAction(int action)
{
	switch(action)
	{
	case ACT_STAND:
		GetFSM()->ChangeState(MyPlayerStand::Instance());
		break;

	case ACT_RUN:
		GetFSM()->ChangeState(MyPlayerRun::Instance());
		break;

	case ACT_ATTACK:
		GetFSM()->ChangeState(MyPlayerAttack::Instance());
		break;

	case ACT_DIE:
		GetFSM()->ChangeState(MyPlayerDeath::Instance());
		stopAllCommand();
		break;

	case ACT_PICK:
		GetFSM()->ChangeState(MyPlayerPick::Instance());
		break;

	case ACT_BE_KNOCKBACK:
		GetFSM()->ChangeState(MyPlayerKnockBack::Instance());
		stopAllCommand();
		break;

	case ACT_CHARGE:
		GetFSM()->ChangeState(MyPlayerCharge::Instance());
		break;

	case ACT_JUMP:
		GetFSM()->ChangeState(MyPlayerJump::Instance());
		break;
	}
}

bool MyPlayer::isAction(int action)
{
	switch(action)
	{
	case ACT_STAND:
		if(GetFSM()->isInState(*(MyPlayerStand::Instance())))
			return true;
		return false;
		break;

	case ACT_RUN:
		if(GetFSM()->isInState(*(MyPlayerRun::Instance())))
			return true;
		return false;
		break;

	case ACT_ATTACK:
		if(GetFSM()->isInState(*(MyPlayerAttack::Instance())))
			return true;
		return false;
		break;

	case ACT_DIE:
		if(GetFSM()->isInState(*(MyPlayerDeath::Instance())))
			return true;
		return false;
		break;

	case ACT_PICK:
		if(GetFSM()->isInState(*(MyPlayerPick::Instance())))
			return true;
		return false;
		break;

	case ACT_BE_KNOCKBACK:
		if(GetFSM()->isInState(*(MyPlayerKnockBack::Instance())))
			return true;
		return false;
		break;

	case ACT_CHARGE:
		if(GetFSM()->isInState(*(MyPlayerCharge::Instance())))
			return true;
		return false;
		break;
	}

	//CCAssert(false, "action has not been handled");
	return false;
}

void MyPlayer::setLockedActorId(long long lockedActorId)
{
	// remove old locked flag
	if(m_gameScene != NULL) {
		auto actor = m_gameScene->getActor(getLockedActorId());
		m_gameScene->unselectActor(actor);
	}

	GameActor::setLockedActorId(lockedActorId);

	// set new locked flag
	if(m_gameScene != NULL) {
		auto actor = this->m_gameScene->getActor(lockedActorId);
		m_gameScene->selectActor(actor);
	}
}

void MyPlayer::onEnter()
{
	Node::onEnter();
}
void MyPlayer::onExit()
{
	Node::onExit();
}

void MyPlayer::update(float dt)
{
	m_dt = dt;

	drive();

	running(dt);   // 全局状态

	m_pStateMachine->Update();

	updateGameFightSkill(dt);

	updateMusouState();

	updateStrategyEffect();

	updateGeneralShow();
}

ActiveRole* MyPlayer::getActiveRole()
{
	return player->mutable_activerole();
}

bool MyPlayer::isMyPlayer()
{
	return true;
}
bool MyPlayer::isMyPlayerGroup()
{
	return true;
}
int MyPlayer::getPosInGroup()
{
	return 0;
}

bool MyPlayer::isTeammateOf(long long targetRoleId)
{
	 // is teammate
	bool isTeammate = false;
	for(unsigned int i = 0; i < GameView::getInstance()->teamMemberVector.size(); i++)
	{
		//CCLOG("team id: %lld", GameView::getInstance()->teamMemberVector.at(i)->roleid());
		if(targetRoleId == GameView::getInstance()->teamMemberVector.at(i)->roleid())
		{
			isTeammate = true;
			break;
		}
	}

	return isTeammate;
}

bool MyPlayer::canAttackPlayer(long long targetRoleId)
{
	auto otherplayer = dynamic_cast<BasePlayer*>(getGameScene()->getActor(targetRoleId));
	if(otherplayer == NULL)
		return false;

	// myself, NEVER attack myself
	if(otherplayer->isMyPlayer())
		return false;

	//////////////////////////////////////////////////
    
	// map type: if it's arena ( pvp ), MUST attack!
    if(GameView::getInstance()->getMapInfo()->maptype() == coliseum)
		return true;

	 // 1. is teammate
	bool isTeammate = isTeammateOf(targetRoleId);
	if(isTeammate)
		return false;

	// 2. check map type
	if(GameView::getInstance()->getMapInfo()->maptype() == city)
		return false;

	// 3. check if aggressor
	bool isAggressor = false;
	// find it
    std::map<long long, int>::iterator iter = m_aggressorMap.find(targetRoleId);
    if( m_aggressorMap.end() != iter )
		return true;

	// 4. check pk mode
	if(getPKMode() == BasePlayer::PKMode_peace)
	{
		return false;
	}
	else if(getPKMode() == BasePlayer::PKMode_kill)
	{
		return true;
	}
	else if(getPKMode() == BasePlayer::PKMode_guide)
	{
		// 相同行会，不pk
		int myplayerFactionId = this->player->activerole().playerbaseinfo().factionid();
		int otherplayerFactionId = otherplayer->getActiveRole()->playerbaseinfo().factionid();
		if(myplayerFactionId == otherplayerFactionId) {
			return false;
		}
		else {
			return true;
		}
	}
	else if(getPKMode() == BasePlayer::PKMode_country)
	{
		// 相同国家，不pk
		int myplayerCountryId = this->player->activerole().playerbaseinfo().country();
		int otherplayerCountryId = otherplayer->getActiveRole()->playerbaseinfo().country();
		if(myplayerCountryId == otherplayerCountryId) {
			return false;
		}
		else {
			return true;
		}
	}

	return false;
}

bool MyPlayer::canAttackActor(long long targetRoleId)
{
	auto actor = this->getGameScene()->getActor(targetRoleId);
	if(actor == NULL)
		return false;

	if(actor->getType() == GameActor::type_monster)
	{
		if (this->getGuardIdList().size() <= 0)
			return true;

		for(unsigned int i = 0;i<this->getGuardIdList().size();i++)
		{
			if (targetRoleId == this->getGuardIdList().at(i))
			{
				return false;
			}
		}

		return true;
	}
	else if(actor->getType() == GameActor::type_player)
	{
		// 其他玩家
		return canAttackPlayer(targetRoleId);
	}
	else if(actor->getType() == GameActor::type_pet)
	{
		// 有危险红圈，可以攻击
		if(actor->getChildByTag(GameActor::kTagDangerCircle) != NULL)
			return true;

		// 武将
		auto general = dynamic_cast<General*>(actor);
		return canAttackPlayer(general->getOwnerId());
	}
	else
	{
		// for example, type_picking
		return false;
	}

	return false;
}

void MyPlayer::onAttack(const char* skillId, const char* skillProcessorId, long long targetId, bool bDoubleAttack, int skillType)
{
	// 攻击敌方，为自己的团队增加绿色血条(玩家自己 和 上阵的武将)
	auto pActor = dynamic_cast<BaseFighter*>(this->getGameScene()->getActor(targetId));
	if(pActor != NULL && !pActor->isMyPlayerGroup() && this->isMyPlayerGroup())
	{
		this->addGreenBlood();
		// 移除展示的武将
		ShowMyGeneral::getInstance()->generalHide();
	}

	if(BasePlayer::isDefaultAttack(skillId))
		return;

	if(skillType == 3)   // musou skill
	{
		ActorUtils::addSkillName(this, skillId, skillProcessorId);

		// more dynamic effect
		// when using musou skill, the player will be bigger in a short duration
		auto action = Sequence::create(
			ScaleTo::create(0.3f,1.4f), 
			DelayTime::create(1.7f),
			ScaleTo::create(0.3f,1.0f),
			NULL);
		this->getAnim()->runAction(action);
	}
}

void MyPlayer::onDamaged( BaseFighter* source, int damageNum, bool bDoubleAttacked, CSkillResult* skillResult /*= NULL*/ )
{
	BaseFighter::onDamaged(source, damageNum, bDoubleAttacked, skillResult);

	// 玩家自己受到伤害，为自己的团队增加绿色血条(玩家自己 和 上阵的武将)
	this->addGreenBlood();

	// 移除展示的武将
	ShowMyGeneral::getInstance()->generalHide();

	/**
	  ** be attacked, counterattack
	**/
	// check if can counterattack
	bool bCanCounterAttack = true;
	// 任务面板打开时，不反击
	auto handleMission = GameView::getInstance()->getMainUIScene()->getChildByTag(kTagHandleMission);
	if (handleMission != NULL)
		bCanCounterAttack = false;
	// 正在播放剧情，不反击
	if(ScriptManager::getInstance()->isInMovieMode())
		bCanCounterAttack = false;
	// only STAND status, the player can CounterAttack
	if(!isAction(ACT_STAND))
		bCanCounterAttack = false;

	auto pMonster = dynamic_cast<Monster*>(source);
	if(pMonster != NULL && bCanCounterAttack)
	{
		long long lockedActorId = this->getLockedActorId();
		if(lockedActorId == NULL_ROLE_ID)
		{
			this->setLockedActorId(source->getRoleId());
			m_simpleAI->startKill(source->getRoleId());
		}
		else
		{
			auto pActor = this->m_gameScene->getActor(lockedActorId);
			// the selected actor is danger, so can attack him
			if(pActor != NULL && pActor->getChildByTag(GameActor::kTagDangerCircle) != NULL)
			{
				m_simpleAI->startKill(lockedActorId);
			}
		}
	}
}

void MyPlayer::init(const char* name)
{
	m_pFootEffectContainer = Node::create();
	m_pFootEffectContainer->setCascadeOpacityEnabled(true);
	this->addChild(m_pFootEffectContainer, GameActor::ACTOR_FOOTEFFECT_ZORDER);
	m_pFootEffectContainer->setTag(BaseFighter::kTagFootEffectContainer);

	addShadow();

	// flag node
	auto animNode = CCLegendAnimation::create("animation/texiao/changjingtexiao/GSGB_a_b/gsgb_d.anm");
	animNode->setPlayLoop(true);
	animNode->setReleaseWhenStop(false);
    addChild(animNode, GameActor::ACTOR_FOOTFLAG_ZORDER, GameActor::kTagFootFlag);

	std::string roleName;
	if(strcmp(name, "") == 0)
		roleName = getDefaultAnimation(this->getProfession());
	else
		roleName = name;
	const char * animationPath="animation/player/";
	//const char* name = "zsgr_npj";   // zsgr_sym， zsgr_npj
	m_pAnim = GameActorAnimation::create(animationPath, animActionNameList, roleName.c_str(), ANI_COMPONENT_MAX ,5);
	//BasePlayer::switchWeapon(this->player->activerole().hand().c_str(), m_pAnim);   // "lance02"
	// must show body, reflection maybe later
	m_pAnim->showComponent(ANI_COMPONENT_REFLECTION, false);
	this->addChild(m_pAnim, GameActor::ACTOR_BODY_ZORDER);

	this->setReflection(false);

	this->setContentSize(Size(64, 100));

	m_pHeadEffectContainer = Node::create();
	m_pHeadEffectContainer->setCascadeOpacityEnabled(true);
	this->addChild(m_pHeadEffectContainer, GameActor::ACTOR_HEAD_ZORDER);
	m_pHeadEffectContainer->setTag(BaseFighter::kTagHeadEffectContainer);

	m_simpleAI = new MyPlayerSimpleAI();
	m_simpleAI->setMyPlayer(this);
	this->addChild(m_simpleAI);
	m_simpleAI->autorelease();

	schedule(schedule_selector(MyPlayer::updateGotItem), 0.8f);

	m_playerAI = new MyPlayerAI();
	m_playerAI->setMyPlayer(this);
	this->addChild(m_playerAI);
	m_playerAI->autorelease();

	// fixed bug: if the MyPlayer enter another map
	if(hasExStatus(ExStatusType::musou))
	{
		onAddMusouEffect();
	}

	this->setScale(BASEFIGHTER_BASE_SCALE);
}

void MyPlayer::initWeaponEffect()
{
	// first
	this->removeAllWeaponEffects();

	for (unsigned int i=0;i< GameView::getInstance()->EquipListItem.size();i++)
	{
		auto pEquipment = GameView::getInstance()->EquipListItem.at(i);
		int temp_ = pEquipment->part();
		if (temp_ == CEquipment::HAND)   // weapon in the HAND
		{
			int pression_ = pEquipment->goods().equipmentdetail().profession();
			int refineLevel_ = pEquipment->goods().equipmentdetail().gradelevel();
			int starLevel_ = pEquipment->goods().equipmentdetail().starlevel();
			std::string effectNameForRefine = ActorUtils::getWeapEffectByRefineLevel(pression_,refineLevel_);
			addWeaponEffect(effectNameForRefine.c_str());

			std::string effectNameForStar = ActorUtils::getWeapEffectByStarLevel(pression_, starLevel_);
			addWeaponEffect(effectNameForStar.c_str());
			break;
		}
	}
}

bool MyPlayer::selectEnemy(int range)
{
	// 若当前已经有玩家选定的怪物，则忽略
	if (hasLockedActor()) {
		auto lockedActor = this->getLockedActor();
		if(lockedActor != NULL)
		{
			auto lockedBasefighter = dynamic_cast<BaseFighter*>(lockedActor);
			if(lockedBasefighter != NULL)
			{
				if(this->canAttackActor(lockedBasefighter->getRoleId())
					&& !lockedBasefighter->isAction(ACT_DIE))
					return true;
			}
		}
	}

	auto scene = getGameScene();
	if(scene == NULL)
		return false;

	int shortestDistance = -1;
	long long selectedRoleId = NULL_ROLE_ID;

	Vec2 myPosition = this->getWorldPosition();

	// 1. select the nearest one in the enemies which are attacking us
	// in this case, ignore the select range
	for (std::map<long long,GameActor*>::iterator it = scene->getActorsMap().begin(); it != scene->getActorsMap().end(); ++it)
	{
		auto actor = it->second;
		if (actor != NULL && canAttackActor(actor->getRoleId()))
		{
			auto pNode = actor->getChildByTag(GameActor::kTagDangerCircle);
			if(pNode == NULL)
				continue;

			auto defender = dynamic_cast<BaseFighter*>(actor);
			CCAssert(defender != NULL, "should not be nil");
			if(defender->isAction(ACT_DIE))   // ignore the dead one
				continue;

			int currentDistance = myPosition.getDistance(actor->getWorldPosition());
			if(currentDistance > 500)   // too long, so ignore
				continue;
			if(shortestDistance == -1)   // first one
			{
				shortestDistance = currentDistance;
				selectedRoleId = actor->getRoleId();
			}
			else
			{
				if(shortestDistance > currentDistance)   // compare distance
				{
					shortestDistance = currentDistance;
					selectedRoleId = actor->getRoleId();
				}
			}
		}
	}
	if(selectedRoleId != NULL_ROLE_ID)
	{
		setLockedActorId(selectedRoleId);
		return true;
	}

	// 2. select the nearest one in all enemies
	for (std::map<long long,GameActor*>::iterator it = scene->getActorsMap().begin(); it != scene->getActorsMap().end(); ++it)
	{
		auto actor = it->second;
		if (actor != NULL && canAttackActor(actor->getRoleId()))
		{
			auto defender = dynamic_cast<BaseFighter*>(actor);
			CCAssert(defender != NULL, "should not be nil");
			if(defender->isAction(ACT_DIE))   // ignore the dead one
				continue;

			if(defender->hasExStatus(ExStatusType::invisible))
				continue;

			int currentDistance = myPosition.getDistance(actor->getWorldPosition());
			if(currentDistance > range)
				continue;

			if(shortestDistance == -1)   // first one
			{
				shortestDistance = currentDistance;
				selectedRoleId = actor->getRoleId();
			}
			else
			{
				if(shortestDistance > currentDistance)   // compare distance
				{
					shortestDistance = currentDistance;
					selectedRoleId = actor->getRoleId();
				}
			}
		}
	}

	if(selectedRoleId != NULL_ROLE_ID)
	{
		setLockedActorId(selectedRoleId);
		return true;
	}

	return false;
}

void MyPlayer::startCommonCD()
{
	// start common cd
	s_commonCDTime = 1.0f;   // 1 second
}
bool MyPlayer::isInCommonCD()
{
	if(s_commonCDTime > 0)
		return true;

	return false;
}

bool MyPlayer::useSkill(std::string skillId, bool bKillTarget, bool bSendReq, bool bCheckCondition, int selectRange, int commandSource)
{
	auto gamefighskill = this->getGameFightSkill(skillId.c_str(), skillId.c_str());
	if(gamefighskill == NULL)
		return false;

	if(bCheckCondition) {
		// in common cd
		//if(isInCommonCD())
		//	return false;

		// in cd
		if(gamefighskill->isInCD())
			return false;

		// mp is not enough
		if(this->getActiveRole()->mp() < gamefighskill->getCFightSkill()->mp())
			return false;
	}

	//CCLOG("skill id on shortcut: %s", skillId.c_str());
	bool bSuccess = false;

	// check the interval time for the same skill
	//MyPlayerCommandAttack* pCurCmd = dynamic_cast<MyPlayerCommandAttack*>(this->getCommand());
	//if(pCurCmd != NULL && pCurCmd->skillId == skillId)
	//{
	//	if(GameUtils::millisecondNow() - pCurCmd->startTime < 600)   // the interval is too short, so ignore
	//	{
	//		return false;
	//	}
	//}
	if(MyPlayerCommandAttack::s_latestSkillId == skillId)
	{
		//if(GameUtils::millisecondNow() - MyPlayerCommandAttack::s_latestSkillStartTime < gamefighskill->getCFightSkill()->intervaltime())   // the interval is too short ( less than cd ), so ignore
		if(GameUtils::millisecondNow() - MyPlayerCommandAttack::s_latestSkillStartTime < 600)
		{
			return false;
		}
	}

	char targetScopeType = gamefighskill->getTargetScopeType();
	float attackSpeed = 1.0f;
	int attackMaxCount = 1;
	if(gamefighskill->getSkillBin() != NULL)
	{
		attackMaxCount = gamefighskill->getSkillBin()->attack_time;
		if(attackMaxCount > 1)
		{
			float modifier = gamefighskill->getSkillBin()->attack_time_modifier;   // default 1.0f
			attackSpeed = attackMaxCount * modifier;
		}
	}
	if(GameFightSkill::isNoneLock(targetScopeType))
	{
		auto cmd = new MyPlayerCommandAttack();
		cmd->skillId = skillId;
		cmd->skillProcessorId = skillId;
		//cmd->targetId = bf->getRoleId();
		cmd->sendAttackRequest = bSendReq;
		cmd->commandSource = commandSource;
		cmd->attackSpeed = attackSpeed;
		cmd->attackMaxCount = attackMaxCount;
		this->setNextCommand(cmd, true, isAttacking());
		bSuccess = true;
	}
	else
	{
		// setup attack command
		if(this->selectEnemy(selectRange))
		{
			auto bf = dynamic_cast<BaseFighter*>(this->getLockedActor());
			CCAssert(bf != NULL, "basefighter should not be null");

			auto cmd = new MyPlayerCommandAttack();
			cmd->skillId = skillId;
			cmd->skillProcessorId = skillId;
			cmd->sendAttackRequest = bSendReq;
			cmd->targetId = bf->getRoleId();
			cmd->commandSource = commandSource;
			cmd->attackSpeed = attackSpeed;
			cmd->attackMaxCount = attackMaxCount;

			if(bKillTarget)
			{
				// set next command's next command
				//MyPlayerCommandAttack* nextnextcmd = (MyPlayerCommandAttack*)cmd->clone();
				//nextnextcmd->skillId = myplayer->getDefaultSkill();
				//nextnextcmd->finishedConditionKill = true;
				//cmd->setNextCommand(nextnextcmd);

				this->getSimpleAI()->startKill(bf->getRoleId());
			}

			this->setNextCommand(cmd, true, isAttacking());
			bSuccess = true;
		}
		else
		{
			// 由玩家发起的技能，允许空打，提高手感
			bool bAllowAttackWithoutTarget = true;
			if(commandSource == ActorCommand::source_from_player && bAllowAttackWithoutTarget)
			{
				auto cmd = new MyPlayerCommandAttack();
				cmd->skillId = skillId;
				cmd->skillProcessorId = skillId;
				cmd->sendAttackRequest = bSendReq;
				cmd->targetId = NULL_ROLE_ID;
				cmd->commandSource = commandSource;
				cmd->attackSpeed = attackSpeed;
				cmd->attackMaxCount = attackMaxCount;

				this->setNextCommand(cmd, true, isAttacking());
				bSuccess = true;
			}
		}
	}

	return bSuccess;
}

bool MyPlayer::selectPickActor(int range)
{
	auto scene = getGameScene();
	int shortestDistance = -1;
	long long selectedRoleId = NULL_ROLE_ID;

	Vec2 myPosition = this->getWorldPosition();
	for (std::map<long long,GameActor*>::iterator it = scene->getActorsMap().begin(); it != scene->getActorsMap().end(); ++it)
	{
		auto actor = it->second;
		if (actor != NULL)
		{
			auto pickActor = dynamic_cast<PickingActor*>(actor);
			//CCAssert(pickActor != NULL, "should not be nil");
			if (pickActor == NULL)
				continue;
			if(!(pickActor->getActivied()))   
				continue;

			int currentDistance = myPosition.getDistance(actor->getWorldPosition());
			if(currentDistance > range)
				continue;

			if(shortestDistance == -1)   // first one
			{
				shortestDistance = currentDistance;
				selectedRoleId = actor->getRoleId();
			}
			else
			{
				if(shortestDistance > currentDistance)   // compare distance
				{
					shortestDistance = currentDistance;
					selectedRoleId = actor->getRoleId();
				}
			}
		}
	}

	if(selectedRoleId != NULL_ROLE_ID)
	{
		setLockedActorId(selectedRoleId);
		return true;
	}

	return false;
}

void MyPlayer::useMusouSkill()
{
	SkillSeed* skillSeed = NULL;
	if (strcmp(m_musouSkillid.c_str(),"") != 0)
	{
		useSkill(m_musouSkillid, true, false, false);

		auto bf = dynamic_cast<BaseFighter*>(this->getLockedActor());
		skillSeed = makeSkillSeed(m_musouSkillid.c_str(), m_musouSkillid.c_str(), bf, getWorldPosition().x, getWorldPosition().y);
	}

	GameMessageProcessor::sharedMsgProcessor()->sendReq(1221, skillSeed);
	CC_SAFE_DELETE(skillSeed);
}

float MyPlayer::s_commonCDTime = 0.0f;
void MyPlayer::updateGameFightSkill(float dt)
{
	std::map<std::string, GameFightSkill*>::iterator it = GameView::getInstance()->GameFightSkillList.begin();
	for ( ; it != GameView::getInstance()->GameFightSkillList.end(); ++ it )
	{
		auto skill = it->second;
		if(skill != NULL)
		{
			skill->updateCD();
		}
	}

	s_commonCDTime -= dt;
}

void MyPlayer::onGotItem(Goods item)
{
	m_gotItemIconList.push_back(item);
}

void MyPlayer::updateGotItem(float dt)
{
	Size s = Director::getInstance()->getVisibleSize();

	if(m_gotItemIconList.empty())
	{
		return;
	}
	else
	{
		std::string Icon = "res_ui/props_icon/";
		Icon.append(m_gotItemIconList.front().icon());
		Icon.append(".png");
		//CCLOG("Icon.c_str() = %s\n", Icon.c_str());
		
		auto pSprite = cocos2d::extension::Scale9Sprite::create();

		std::string frameColorPath;
		if (m_gotItemIconList.front().quality() == 1)
		{
			frameColorPath = whiteFramePath;
		}
		else if (m_gotItemIconList.front().quality() == 2)
		{
			frameColorPath = greenFramePath;
		}
		else if (m_gotItemIconList.front().quality() == 3)
		{
			frameColorPath = blueFramePath;
		}
		else if (m_gotItemIconList.front().quality() == 4)
		{
			frameColorPath = purpleFramePath;
		}
		else if (m_gotItemIconList.front().quality() == 5)
		{
			frameColorPath = orangeFramePath;
		}
		else
		{
			frameColorPath = voidFramePath;
		}
		pSprite->initWithFile(frameColorPath.c_str());
		pSprite->setPosition(Vec2(s.width/2+225, s.height/2-100));
		pSprite->setAnchorPoint(Vec2(0.5f, 0.5f));
		pSprite->setContentSize(Size(83,75));
		pSprite->setOpacity(0);
		GameView::getInstance()->getMainUIScene()->addChild(pSprite, 1);
		
		auto pSpriteIcon = Sprite::create(Icon.c_str());
		if(pSpriteIcon == NULL)
		{
			pSpriteIcon = Sprite::create("res_ui/props_icon/prop.png");
		}
		pSpriteIcon->setPosition(Vec2(41, 38));
		pSpriteIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
		pSprite->addChild(pSpriteIcon);

		auto Lable_name = Label::createWithTTF(m_gotItemIconList.front().name().c_str(), "Arial", 12);
		Lable_name->setAnchorPoint(Vec2(0.5f,0.5f));
		Lable_name->setPosition(Vec2(41,12));
		Lable_name->setColor(GameView::getInstance()->getGoodsColorByQuality(m_gotItemIconList.front().quality()));
		
		pSprite->addChild(Lable_name);
		
		auto pParticle = Node::create();
		pParticle->setPosition(Vec2(s.width/2+75, s.height/2-100));
		pParticle->setAnchorPoint(Vec2(0.5f, 0.5f));
		GameView::getInstance()->getMainUIScene()->addChild(pParticle, 0);

		//// tail paritcles
		//ParticleSystem* particleEffect = ParticleSystemQuad::create("animation/texiao/particledesigner/tuowei1.plist");
		//particleEffect->setPositionType(ParticleSystem::PositionType::FREE);
		//particleEffect->setPosition(Vec2(0.5f,0.5f));
		//particleEffect->setAnchorPoint(Vec2(0.5f,0.5f));
		//particleEffect->setVisible(true);
		//particleEffect->setLife(0.5f);
		//particleEffect->setLifeVar(0);
		//pParticle->addChild(particleEffect, 1);
		
		//pSpriteIcon->setScale(0.8f);

		pSprite->runAction(Sequence::create(
								Spawn::create(FadeIn::create(0.2f),
												MoveTo::create(0.2f, Vec2(s.width/2+75, s.height/2-100)),
												NULL),
								MoveTo::create(1.25f,Vec2(s.width/2-75, s.height/2-100)),
								Spawn::create(
												MoveTo::create(0.75f,Vec2(50, 0)),
												RotateBy::create(0.75f, 360),
												ScaleTo::create(0.75f, 0),
												NULL),
								RemoveSelf::create(),
								NULL));
		pParticle->runAction(Sequence::create(
								MoveTo::create(1.25f,Vec2(s.width/2-75, s.height/2-100)),
								Spawn::create(
												MoveTo::create(0.75f,Vec2(50, 0)),
												ScaleTo::create(0.75f, 0),
												NULL),
								RemoveSelf::create(),
								NULL));

		m_gotItemIconList.erase(m_gotItemIconList.begin());
	}
}

void MyPlayer::setReachedMap(Push5080 reachedmap)
{
	int i,index;

	for(i = 0; i < reachedmap.allmap_size(); i++)
	{
		m_reachedMapList[i].clear();
		for(index = 0; index < reachedmap.allmap(i).mapid_size(); index++)
		{
			m_reachedMapList[i].push_back(reachedmap.allmap(i).mapid(index));
		}
	}
}

void MyPlayer::addCurrentReachedMap(int countryId, std::string mapId)
{
	if(countryId)
	{
		m_reachedMapList[countryId].push_back(mapId);
	}
	else
	{
		for(int i = 1; i < 4; i++)
		{
			m_reachedMapList[i].push_back(mapId);
		}
	}
}

bool MyPlayer::isMapReached(int countryId, const char* mapId)
{
	for(unsigned int i = 0; i < m_reachedMapList[countryId].size(); i++)
	{
		if(!strcmp(mapId, m_reachedMapList[countryId].at(i).c_str()))
		{
			return true;
		}
	}
	return false;
}

void MyPlayer::setPhysicalValue( int _value )
{
	m_physicalValue = _value;
}

int MyPlayer::getPhysicalValue()
{
	return m_physicalValue;
}

void MyPlayer::setPhysicalCapacity( int _value )
{
	m_physicalCapacity = _value;
}

int MyPlayer::getPhysicalCapacity()
{
	return m_physicalCapacity;
}

void MyPlayer::setAngerValue( int _value )
{
	m_angerValue = _value;
}

int MyPlayer::getAngerValue()
{
	return m_angerValue;
}

void MyPlayer::setAngerCapacity( int _value )
{
	m_angerCapacity = _value;
}

int MyPlayer::getAngerCapacity()
{
	return m_angerCapacity;
}

void MyPlayer::setMusouEssence( int _value )
{
	m_musouEssence = _value;
}

int MyPlayer::getMusouEssence()
{
	return m_musouEssence;
}

void MyPlayer::addGreenBlood()
{
	// the green blood bar on the head
	auto pBloodBar = (MyPlayerInfoMini*)this->getChildByTag(kTagGreenBloodBar);
	if(pBloodBar == NULL)
	{
		pBloodBar = MyPlayerInfoMini::create(this);
		pBloodBar->setAnchorPoint(Vec2(0.5f, 0.5f));
		pBloodBar->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT));
		this->addChild(pBloodBar, ACTOR_HEAD_ZORDER, kTagGreenBloodBar);
	}
	// refresh
	if(pBloodBar != NULL)
	{
		// refresh action
		pBloodBar->stopAllActions();
		auto action = Sequence::create(
			DelayTime::create(MYPLAYER_GREEN_BLOOD_DURATION),
			CallFunc::create(CC_CALLBACK_0(MyPlayer::removeGreenBlood, this)),
			NULL);
		pBloodBar->runAction(action);

		// refresh info
		pBloodBar->ReloadTargetData(this);
	}

	// reset the postions
	// NAME label
	auto nameLabel = this->getChildByTag(GameActor::kTagActorName);
	if(nameLabel != NULL)
	{
		nameLabel->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT + 16));

		this->synchronizeHeadLabelPosition();
	}

	std::vector<long long> vector_general = this->getAliveGenerals();
	for (unsigned int i = 0 ; i < vector_general.size(); i++)
	{
		long long generalId = vector_general.at(i);

		auto pActor = dynamic_cast<General*>(this->getGameScene()->getActor(generalId));
		if(pActor != NULL && pActor->isMyPlayerGroup() && this->isMyPlayerGroup())
		{
			pActor->addGreenBlood();
		}
	}
}

void MyPlayer::removeGreenBlood()
{
	// remove
	auto pNode = this->getChildByTag(kTagGreenBloodBar);
	if(pNode != NULL)
	{
		pNode->removeFromParent();
	}
		
	// reset the postions
	// NAME label
	auto nameLabel = this->getChildByTag(GameActor::kTagActorName);
	if(nameLabel != NULL)
	{
		nameLabel->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT));

		this->synchronizeHeadLabelPosition();
	}
}

bool MyPlayer::hasGreenBlood()
{
	bool bFlag = false;
	auto pBloodBar = (MyPlayerInfoMini*)this->getChildByTag(kTagGreenBloodBar);
	if (NULL != pBloodBar)
	{
		bFlag = true;
	}

	return bFlag;
}

// the auto combat flag
void MyPlayer::addAutoCombatFlag()
{
	auto pNode = this->getChildByTag(PLAYER_AUTO_FIGHT_ANIMATION_TAG);
	if (NULL == pNode)
	{
		CCLegendAnimation* pAnimNode = CCLegendAnimation::create("animation/texiao/jiemiantexiao/zdzd/zdzd.anm");
		pAnimNode->setScale(1.2f);
		pAnimNode->setTag(PLAYER_AUTO_FIGHT_ANIMATION_TAG);
		pAnimNode->setPlayLoop(true);
		pAnimNode->setReleaseWhenStop(false);

		pAnimNode->setPosition(this->getHeadLabelPosition(PLAYER_AUTO_FIGHT_ANIMATION_TAG));
		this->addChild(pAnimNode);
	}
}
void MyPlayer::removeAutoCombatFlag()
{
	// remove
	auto pNode = this->getChildByTag(PLAYER_AUTO_FIGHT_ANIMATION_TAG);
	if(pNode != NULL)
	{
		pNode->removeFromParent();
	}
	else
	{
		return;
	}

	// reset the postions
	// NAME label
	auto nameLabel = this->getChildByTag(GameActor::kTagActorName);
	if(nameLabel != NULL)
	{
		//nameLabel->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT));

		auto pGreenBlood = (MyPlayerInfoMini*)this->getChildByTag(kTagGreenBloodBar);
		if(NULL != pGreenBlood)
		{
			pGreenBlood->setPosition(Vec2(0,BASEFIGHTER_ROLE_HEIGHT));
			nameLabel->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT + 16));
		}
		else
		{
			nameLabel->setPosition(Vec2(0, BASEFIGHTER_ROLE_HEIGHT));
		}

		this->synchronizeHeadLabelPosition();
	}
}
bool MyPlayer::hasAutoCombatFlag()
{
	bool bFlag = false;

	auto pNode = this->getChildByTag(PLAYER_AUTO_FIGHT_ANIMATION_TAG);
	if (NULL != pNode)
	{
		bFlag = true;
	}

	return bFlag;
}

Vec2 MyPlayer::getHeadLabelPosition(int labelType)
{
	if(labelType == PLAYER_AUTO_MOVE_ANIMATION_TAG || labelType == PLAYER_AUTO_FIGHT_ANIMATION_TAG)
	{
		int baseY = BASEFIGHTER_ROLE_HEIGHT;
		int offsetY = 15;

		// has blood bar
		if ((this->hasGreenBlood()))
		{
			offsetY += 17;
		}
		// has family label
		if (strcmp(this->getActiveRole()->playerbaseinfo().factionname().c_str(), "") != 0)
		{
			offsetY += 27;
		}

		return Vec2(0, baseY+offsetY);
	}

	return BaseFighter::getHeadLabelPosition(labelType);
}

void MyPlayer::updateGeneralShow()
{
	if (ShowMyGeneral::getInstance()->canGeneralShow())
	{
		ShowMyGeneral::getInstance()->generalShow();
	}
}
