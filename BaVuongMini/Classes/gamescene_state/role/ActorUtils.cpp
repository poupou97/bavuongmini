#include "ActorUtils.h"

#include "GameActorAnimation.h"
#include "BaseFighter.h"
#include "BasePlayer.h"
#include "../../messageclient/element/CActiveRole.h"
#include "AppMacros.h"
#include "../skill/GameFightSkill.h"
#include "../GameSceneState.h"
#include "BaseFighterConstant.h"
#include "WeaponEffect.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/MapWeapEffect.h"
#include "../../messageclient/element/CFightWayBase.h"
#include "../../GameView.h"
#include "../../utils/GameUtils.h"
#include "../SimpleEffectManager.h"
#include "MyPlayer.h"

#define BASEPLAYER_STRATEGY_EFFECT_TAG 1021
#define FLOWERPARTICLEONROLE_ACTORUTILS_TAG  555

static std::string standActionNameList[] = { "stand" }; 
GameActorAnimation* ActorUtils::createActorAnimation(const char* figureName, const char* weaponName, const char* weaponEffectFireName, const char* weaponEffectSparkleName)
{
	std::string str_figureName = figureName;
	std::string name = str_figureName.substr(0,5);
	const int animName = 0;
	GameActorAnimation* pAnim = NULL;

	// 玩家类
	if(strcmp(name.c_str(),"zsgr_") == 0)
	{
		//const char* name = "zsgr_npj";   // zsgr_sym， zsgr_npj
		pAnim = GameActorAnimation::create("animation/player/", standActionNameList, figureName, ANI_COMPONENT_MAX, 1);

		// 脸正面，站立动画
		pAnim->setAnimName(animName);
		pAnim->setAction(ANIM_DIR_DOWN);
		pAnim->play();

		pAnim->showComponent(ANI_COMPONENT_REFLECTION, false);
		pAnim->showComponent(ANI_COMPONENT_SHADOW, true);

		// these call must be excuted after GameActorAnimation::setAnimName()
		int profession = BasePlayer::getProfessionIdxByFigureName(figureName);
		BasePlayer::switchWeapon(weaponName, pAnim, profession);   // "lance02"

		if(strcmp(weaponEffectFireName, "") != 0)
		{
			auto pBodyAnim = pAnim->getAnim(ANI_COMPONENT_BODY, animName);
			//weaponEffectName = "animation/texiao/particledesigner/caiji1.plist";
			WeaponEffectDefine effectDef;
			effectDef.weaponEffectType = WeaponEffect::getWeaponEffectType(weaponEffectFireName);
			effectDef.weaponEffectFileName = weaponEffectFireName;
			int weaponType = BasePlayer::getWeaponType(profession);
			WeaponEffect::attach(effectDef, weaponType, pBodyAnim);
		}
		if(strcmp(weaponEffectSparkleName, "") != 0)
		{
			auto pBodyAnim = pAnim->getAnim(ANI_COMPONENT_BODY, animName);
			//weaponEffectName = "animation/texiao/particledesigner/caiji1.plist";
			WeaponEffectDefine effectDef;
			effectDef.weaponEffectType = WeaponEffect::getWeaponEffectType(weaponEffectSparkleName);
			effectDef.weaponEffectFileName = weaponEffectSparkleName;
			int weaponType = BasePlayer::getWeaponType(profession);
			WeaponEffect::attach(effectDef, weaponType, pBodyAnim);
		}
	}
	// 武将类
	else if(strcmp(name.c_str(),"zsgg_") == 0)
	{
		pAnim = GameActorAnimation::create("animation/generals/", standActionNameList, figureName, ANI_COMPONENT_MAX, 1);

		// 脸正面，站立动画
		pAnim->setAnimName(animName);
		pAnim->setAction(ANIM_DIR_DOWN);
		pAnim->play();

		pAnim->showComponent(ANI_COMPONENT_REFLECTION, false);
		pAnim->showComponent(ANI_COMPONENT_SHADOW, true);

		// 武将没有切换武器形象的功能
		//BasePlayer::switchWeapon(weaponName, pAnim);   // "lance02"
	}
	else
	{
		CCAssert(false, "wrong figure name");
	}

	if(pAnim != NULL)
	{
		// 阴影
		auto pShadowAnim = pAnim->getAnim(ANI_COMPONENT_SHADOW, animName);
		if(pShadowAnim != NULL)
		{
			pShadowAnim->setColor(Color3B::BLACK);
			pShadowAnim->setScaleX(1.1f);
			pShadowAnim->setScaleY(0.5f);
			pShadowAnim->setRotation(15.f);
			pShadowAnim->setSkewX(35.f);
			pShadowAnim->setOpacity(80);
		}
	}

	return pAnim;
}

std::string ActorUtils::getActorFigureName(ActiveRole& activerole)
{
	std::string figureName = "";
	if (activerole.type() == GameActor::type_monster)  {
	}
	else if(activerole.type() == GameActor::type_player) 
	{
		if(activerole.has_body()) {
			figureName = activerole.body();
		}
		else {
			int profession = BasePlayer::getProfessionIdxByName(activerole.profession());
			figureName = BasePlayer::getDefaultAnimation(profession);
		}
	}
	else if(activerole.type() == GameActor::type_pet) {
		figureName = activerole.rolebase().figure();
	}
	else {
		CCAssert(false, "please implement this branch");
	}

	return figureName;
}

Node* ActorUtils::createSkillNameLabel(const char* skillName)
{
	auto node = Node::create();
	node->setCascadeOpacityEnabled(true);

	//Sprite* spr = Sprite::create("res_ui/font/super_di.png");
	//spr->setScaleX(3.0f);
 //   spr->setScaleY(2.0f);
	//spr->setAnchorPoint(Vec2(0.5f,0));
	//node->addChild(spr);

	//Label* skillNameLabel = Label::createWithTTF(skillName, APP_FONT_NAME, 40);
	////skillName->setColor(Color3B(0, 255, 0));
	//skillNameLabel->setAnchorPoint(Vec2(0.5f,0));
	//Color3B color = Color3B(0, 255, 0);
	//skillNameLabel->enableShadow(Size(0,0), 1.0f, 1.0f, color);
	//node->addChild(skillNameLabel);

	// art font for the skill
	// background
	auto spr = Sprite::create("res_ui/moji_4.png");
	spr->setScale(1.6f);
	spr->setAnchorPoint(Vec2(0.5f,0.5f));
	node->addChild(spr);
	// skill name
	auto skillNameLabel = Label::createWithBMFont("res_ui/font/ziti_5.fnt", skillName);
	skillNameLabel->setAnchorPoint(Vec2(0.5f,0.5f));
	node->addChild(skillNameLabel);
	node->setScale(0.7f);
	
	/**********************这是旧版技能名字的动画action******************************/
	/*FiniteTimeAction*  action = Sequence::create(
		ScaleTo::create(0.25f*1/2,1.2f), 
		ScaleTo::create(0.25f*1/3,0.8f),
		DelayTime::create(1.5f),
		FadeOut::create(0.5f),
		RemoveSelf::create(),
		NULL);
	node->runAction(action);*/
	/*************************************************n******************************/

	auto action_1 = MoveBy::create(0.7f, Vec2(0, 50));
	auto action_2 = Sequence::create(
		DelayTime::create(0.1f),
		ScaleTo::create(0.15f, 1.1f),
		ScaleTo::create(0.15f, 0.7f),
		NULL);
	auto action_spwan_1 = Spawn::create(
		action_1,
		action_2,
		NULL);

	auto action_spwan_2 = Spawn::create(
		FadeOut::create(0.3f),
		ScaleTo::create(0.3f, 1.1f),
		NULL);

	auto action_final = Sequence::create(
		action_spwan_1,
		DelayTime::create(0.3f),
		action_spwan_2,
		RemoveSelf::create(),
		NULL);

	node->runAction(action_final);
	
	return node;
}

Vec2 ActorUtils::getDefaultSkillPosition(BaseFighter* attacker, const char* skillId, const char* skillProcessorId)
{
	int atk_distance = attacker->getGameFightSkill(skillId, skillProcessorId)->getDistance();

	// calc the target position
	Vec2 targetPos = attacker->getWorldPosition();
	int dir = attacker->getAnimDir();
	if(dir == ANIM_DIR_UP)   // face to up
		targetPos.y -= atk_distance;
	else if(dir == ANIM_DIR_LEFT)   // face to left
		targetPos.x -= atk_distance;
	else if(dir == ANIM_DIR_DOWN)   // face to down
		targetPos.y += atk_distance;
	else if(dir == ANIM_DIR_RIGHT)   // face to right
		targetPos.x += atk_distance;

	Vec2* target = attacker->getGameScene()->findNearestPositionToTarget(
		attacker->getWorldPosition().x, attacker->getWorldPosition().y,
		targetPos.x, targetPos.y);
	if(target != NULL)
	{
		targetPos.x = target->x;
		targetPos.y = target->y;

		delete target;
	}

	auto scene = attacker->getGameScene();
	short tileX = scene->positionToTileX(targetPos.x);
	short tileY = scene->positionToTileY(targetPos.y);
	// out of map, ignore
	if(!scene->checkInMap(tileX, tileY))
	{
		return attacker->getWorldPosition();
	}

	return targetPos;
}

Sprite* ActorUtils::createGeneralRankSprite(int nRank)
{
	if(nRank <= 0)
		return NULL;
	std::string rankPathBase = "res_ui/rank/rank";
	char s[5];
	sprintf(s,"%d", nRank);
	rankPathBase.append(s);
	rankPathBase.append(".png");
	auto pRankSprite = Sprite::create(rankPathBase.c_str());
	return pRankSprite;
}

void ActorUtils::addSkillName(BaseFighter* actor, const char* skillId, const char* skillProcessorId)
{
	auto baseSkill = actor->getGameFightSkill(skillId, skillProcessorId)->getCBaseSkill();
	if(baseSkill == NULL)
		return;

	// general says the skill name
	//__Dictionary *strings = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
	//const char *str  = ((__String*)strings->objectForKey("general_useskill"))->m_sString.c_str();
	//std::string str_use_skill = str;
	//str_use_skill.append(" ");
	//str_use_skill.append(baseSkill->name());
	//this->addChatBubble(str_use_skill.c_str(), 1.0f);

	// show the skill name at the head of the general
	//Node* node = ActorUtils::createSkillNameLabel(baseSkill->name().c_str());
	//this->addEffect(node, false, 130);
	
	Size s = Director::getInstance()->getVisibleSize();
	auto node = ActorUtils::createSkillNameLabel(baseSkill->name().c_str());
	node->setAnchorPoint(Vec2(0.5f, 0.5f));
	//node->setPosition(Vec2(s.width - 130, s.height - 100));
	//GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	//scene->addChild(node, SCENE_TOP_LAYER_BASE_ZORDER);

	node->setPosition(Vec2(actor->getPositionX(), actor->getPositionY() + BASEFIGHTER_ROLE_HEIGHT));
	auto actorLayer =  actor->getGameScene()->getActorLayer();
	actorLayer->addChild(node, SCENE_TOP_LAYER_BASE_ZORDER + actor->getGameScene()->getMapSize().height);
}

void ActorUtils::addMultiAttackDamageNumber(BaseFighter* defender, int damageNum, int attacktime)
{
	if(defender == NULL)
		return;

	for(int i = 0; i < attacktime; i++)
	{
		int damage = damageNum / attacktime;

		// random number ( fake number )
		if(attacktime > 1)
		{
			const int range = 10;
			float random = CCRANDOM_0_1() - 0.5f;
			damage += random * range;
			if(damage <= 0)
				damage = 1;
		}
					
		// damage number effect
		char str[10];
		sprintf(str,"%d", damage);
		auto label = Label::createWithBMFont("res_ui/font/c123.fnt", str);   // yellow color
		label->setAnchorPoint(Vec2(0.5f,0.0f));
		label->setPosition(Vec2(defender->getPosition().x, defender->getPosition().y + BASEFIGHTER_ROLE_HEIGHT*1/3));
		//label->setPosition(label->getPosition() + SimpleEffectManager::getNumberOffset());
		label->setVisible(false);

		/*
		// move up and fade out
		FiniteTimeAction* out_sub_action = Spawn::create(
			MoveBy::create(0.7f, Vec2(0, 140)),
			FadeOut::create(2.5f),
			NULL);
		FiniteTimeAction*  action = Sequence::create(
			DelayTime::create(i*0.2f),
			Show::create(),
			ScaleTo::create(0.25f*1/2,1.7f),   // 2.5f
			ScaleTo::create(0.25f*1/3,1.2f),   // 1.5f
			out_sub_action,
			RemoveSelf::create(),
			NULL);
		label->runAction(action);
		*/

		// scale to big
		auto step_one_action = Spawn::create(
			MoveBy::create(0.4f, Vec2(0, 100)),
			ScaleTo::create(0.4f, 1.8f, 2.5f),
			NULL);
		// scale to small
		auto step_two_action = Spawn::create(
			MoveBy::create(0.25f, Vec2(0, 20)),
			ScaleTo::create(0.25f, 1.3f, 1.5f),
			NULL);
		// scale to small
		auto step_three_action = Spawn::create(
			MoveBy::create(0.3f, Vec2(0, -25)),
			ScaleTo::create(0.3f, 1.3f, 1.3f),
			NULL);
		// scale to big
		auto step_four_action = Spawn::create(
			MoveBy::create(0.3f, Vec2(0, -25)),
			ScaleTo::create(0.3f, 1.2f, 1.2f),
			NULL);
		auto action = Sequence::create(
			DelayTime::create(i*0.2f),
			Show::create(),
			step_one_action,
			step_two_action,
			step_three_action,
			step_four_action,
			//FadeOut::create(0.7f),
			FadeTo::create(1.0f, 192),
			RemoveSelf::create(),
			NULL);
		label->runAction(action);

		defender->getGameScene()->getActorLayer()->addChild(label, SCENE_TOP_LAYER_BASE_ZORDER + defender->getGameScene()->getMapSize().height);
	}
}

void ActorUtils::addDoubleAttackDamageNumber(BaseFighter* target, int damageNum, const char* fontName)
{
	char str[10];
	sprintf(str,"%d", damageNum);
	auto label = Label::createWithBMFont(fontName, str);
	label->setAnchorPoint(Vec2(0.5f,0.0f));
	//label->setScale(0.8f);

	auto node = Node::create();
	node->setAnchorPoint(Vec2(0,0));

	// special text
	auto pSprite = Sprite::create("res_ui/font/double.png");

	float maxHeight = MAX(label->getContentSize().height, pSprite->getContentSize().height);

	pSprite->setAnchorPoint(Vec2(0,0));
	pSprite->setPosition(Vec2(0,(maxHeight-pSprite->getContentSize().height)/2));
	node->addChild(pSprite);

	// number
	label->setAnchorPoint(Vec2(0,0));
	label->setPosition(Vec2(pSprite->getContentSize().width,(maxHeight-label->getContentSize().height)/2));
	node->addChild(label);

	node->setContentSize(Size(pSprite->getContentSize().width+label->getContentSize().width, maxHeight));
	node->setAnchorPoint(Vec2(0.5f,0));

	SimpleEffectManager::applyEffectAction(node, SimpleEffectManager::EFFECT_BAOJI);
	//this->addEffect(node, false, 80/*BASEFIGHTER_ROLE_HEIGHT*/);
	node->setPosition(Vec2(target->getPosition().x, target->getPosition().y + 80));
	target->getGameScene()->getActorLayer()->addChild(node, SCENE_TOP_LAYER_BASE_ZORDER + target->getGameScene()->getMapSize().height);
}

std::string ActorUtils::getWeapEffectByRefineLevel( int weapPression,int equipRefineLevel )
{
	std::string weapRefineEffectPath_ ="";
	std::map<EquipRefineEffectConfigData::weapEffect, std::string >::iterator it = EquipRefineEffectConfigData::s_EquipmentRefineEffect.begin();
	for ( ; it != EquipRefineEffectConfigData::s_EquipmentRefineEffect.end(); ++ it )
	{
		if (equipRefineLevel >= it->first.refineLevel )
		{
			EquipRefineEffectConfigData::weapEffect temp;
			temp.weapPression = weapPression;
			temp.refineLevel = it->first.refineLevel;
	
			std::string weapRefineEffectStr_ = EquipRefineEffectConfigData::s_EquipmentRefineEffect[temp];
			weapRefineEffectPath_ = "animation/weapon/weaponeffect/";
			weapRefineEffectPath_.append(weapRefineEffectStr_);			
		}
	}
	return weapRefineEffectPath_;
}

std::string ActorUtils::getWeapEffectByStarLevel( int weapPression,int equipStarLevel )
{
	std::string weapStarEffectPath_ ="";
	std::map<EquipStarEffectConfigData::weapEffect, std::string >::iterator it = EquipStarEffectConfigData::s_EquipmentStarEffect.begin();
	for ( ; it != EquipStarEffectConfigData::s_EquipmentStarEffect.end(); ++ it )
	{
		if (equipStarLevel >= it->first.starLevel)
		{
			EquipStarEffectConfigData::weapEffect temp;
			temp.weapPression = weapPression;
			temp.starLevel = it->first.starLevel;

			std::string weapStarEffectStr_ = EquipStarEffectConfigData::s_EquipmentStarEffect[temp];
			weapStarEffectPath_ = "animation/weapon/weaponeffect/";
			weapStarEffectPath_.append(weapStarEffectStr_.c_str());			
		}
	}
	return weapStarEffectPath_;
}



void ActorUtils::addMusouBodyEffect(BaseFighter* target, int color)
{
	// effect on body
	int effectTag = BaseFighter::kTagMosouEffect+target->getRoleId();
	auto container = target->getChildByTag(BaseFighter::kTagFootEffectContainer);
	CCLegendAnimation* pBodyEffect = NULL;
	if(container->getChildByTag(effectTag) == NULL)
	{
		//pBodyEffect = CCLegendAnimation::create("images/lightning_body.anm");
		//pBodyEffect->setPlayLoop(true);
		//pBodyEffect->setReleaseWhenStop(false);   // only play once, then release
		//pBodyEffect->setScaleY(1.5f);
		//pBodyEffect->setOpacity(200);
		//pBodyEffect->setTag(kTagMosouEffect+this->getRoleId());
		//this->addEffect(pBodyEffect, false, 30);

		auto shadow = Sprite::create("images/light_1.png");
		shadow->setAnchorPoint(Vec2(0.5f, 0.5f));
		shadow->setTag(effectTag);
		shadow->setScaleX(1.3f);   // 1.5f  1.3f  1.4f
		shadow->setScaleY(3.9f);   // 4.5f  3.9f  4.2f
		//shadow->setRotation(-60.f);
		Color3B color3b = GameUtils::convertToColor3B(color);
		shadow->setColor(color3b);
		//shadow->setPosition(Vec2(0, 40));

		auto  action1 = FadeTo::create(0.25f,64);
		auto  action2 = FadeTo::create(0.25f,192);
		auto  action3 = FadeTo::create(0.5f,255);
		auto  action4 = FadeTo::create(0.25f,192);
		auto  action5 = FadeTo::create(0.25f,64);
		auto  action6 = DelayTime::create(0.1f);
		auto repeapAction = RepeatForever::create(
			Sequence::create(action1,action2,action3,action4,action5,action6,NULL));
		shadow->runAction(repeapAction);

		target->addEffect(shadow, true, 40);
	}
}

void ActorUtils::addStrategyEffect(BaseFighter* player, int fightWayId)
{
	std::string str_icon = "";
	for (unsigned int i = 0;i<FightWayConfigData::s_fightWayBase.size();i++)
	{
		if (fightWayId == FightWayConfigData::s_fightWayBase.at(i)->fightwayid())
		{
			str_icon = FightWayConfigData::s_fightWayBase.at(i)->get_icon();
		}
	}
	if(str_icon == "")
		return;

	std::string str_iconPath = "res_ui/MatrixMethod/";
	str_iconPath.append(str_icon);
	str_iconPath.append("2.png");
	auto pSprite = Sprite::create(str_iconPath.c_str());
	pSprite->setAnchorPoint(Vec2(0.5f,0.5f));
	pSprite->setVisible(false);

	const float RATATION_SPEED = 112.5f;
	const float FADEIN_DURATION = 1.0f;
	const float ROTATE_DURATION = 5.0f;
	const float FADEOUT_DURATION = 1.0f;

	auto fadeinAction = Spawn::create(
		RotateBy::create(FADEIN_DURATION, FADEIN_DURATION*RATATION_SPEED),
		FadeIn::create(FADEIN_DURATION),
		NULL);
	auto fadeoutAction = Spawn::create(
		RotateBy::create(FADEOUT_DURATION, FADEOUT_DURATION * RATATION_SPEED),
		FadeOut::create(FADEOUT_DURATION),
		NULL);
	auto action1 = Sequence::create(
		Show::create(),
		fadeinAction,
		RotateBy::create(ROTATE_DURATION, ROTATE_DURATION * RATATION_SPEED),
		fadeoutAction,
		NULL);
	pSprite->runAction(action1);

	auto pNode = Node::create();
	pNode->addChild(pSprite);
	pNode->setScaleX(2.1f);   // 3.0f
	pNode->setScaleY(1.05f);   // 1.5f
	pNode->setTag(BASEPLAYER_STRATEGY_EFFECT_TAG);
	auto action2 = Sequence::create(
		DelayTime::create(FADEIN_DURATION+ROTATE_DURATION+FADEOUT_DURATION),
		RemoveSelf::create(),
		NULL);
	pNode->runAction(action2);

	player->addEffect(pNode, true, 0);
}
void ActorUtils::removeStrategyEffect(BaseFighter* player)
{
	auto container = player->getChildByTag(BaseFighter::kTagFootEffectContainer);
	auto pNode = container->getChildByTag(BASEPLAYER_STRATEGY_EFFECT_TAG);
	if(pNode != NULL)
		pNode->removeFromParent();
}

void ActorUtils::addFlowerParticle( BaseFighter* player )
{
	//REMOVE FLOWERpARTICLE
	ActorUtils::removeFlowerParticle(player);

	auto particle1 = WeaponEffect::getParticle("animation/texiao/particledesigner/guanghuan.plist");
	particle1->setPositionType(ParticleSystem::PositionType::GROUPED);
	particle1->setAnchorPoint(Vec2(0.5f,0.5f));
	particle1->setPosition(Vec2(0,0));
	particle1->setScaleX(0.3f);
	particle1->setScaleY(0.6f);

	auto particle2 = Sprite::create("images/light_1.png");
	particle2->setAnchorPoint(Vec2(0.5f, 0.5f));
	particle2->setScaleX(1.3f); 
	particle2->setScaleY(3.9f);
	particle2->setColor(Color3B(255, 153, 204));//set flower particle color

	auto  action1 = FadeTo::create(0.25f,64);
	auto  action2 = FadeTo::create(0.25f,192);
	auto  action3 = FadeTo::create(0.5f,255);
	auto  action4 = FadeTo::create(0.25f,192);
	auto  action5 = FadeTo::create(0.25f,64);
	auto  action6 = DelayTime::create(0.1f);
	auto repeapAction = RepeatForever::create(
		Sequence::create(action1,action2,action3,action4,action5,action6,NULL));
	particle2->runAction(repeapAction);

	auto node_ = Node::create();
	node_->setAnchorPoint(Vec2(0.5f,0.5f));
	node_->setPosition(Vec2(0,0));
	node_->setTag(FLOWERPARTICLEONROLE_ACTORUTILS_TAG);
	//add all node
	node_->addChild(particle2);
	node_->addChild(particle1);

	player->addEffect(node_, true, 40);

	//add player flowerPacticle
	/*
	MyPlayer* me = GameView::getInstance()->myplayer;
	int flowerNum_ = GameView::getInstance()->getFlowerNum();
	if (flowerNum_ >= 999 )
	{
	}else
	{
		if (flowerNum_ >=365)
		{
		}else
		{
			if (flowerNum_ >=11)
			{
			}else
			{
			}
		}
	}
	*/
}

void ActorUtils::removeFlowerParticle( BaseFighter * player )
{	
	if (player)
	{
		auto pNode = (Node*)player->getChildByTag(FLOWERPARTICLEONROLE_ACTORUTILS_TAG);
		if (pNode)
		{
			pNode->removeFromParentAndCleanup(true);
		}
	}
}

std::map<std::string, CCLegendAnimation*> ActorUtils::s_skillsEffectsCache;
void ActorUtils::preloadSkillEffects(int profession)
{
	// 1. get all skills for one profession
	std::vector<std::string> allSkills;
	std::map<std::string, CBaseSkill*>& baseSkillData = StaticDataBaseSkill::s_baseSkillData;
    for (std::map<std::string, CBaseSkill*>::iterator it = baseSkillData.begin(); it != baseSkillData.end(); ++it) 
	{
		auto _skillInfo = it->second;
		if(_skillInfo->get_profession() == profession)
		{
			allSkills.push_back(_skillInfo->id());
		}
	}

	// 2. get all effects' names
	std::vector<std::string> allSkillsEffectNames;
	for(unsigned int i = 0; i < allSkills.size(); i++)
	{
		auto pSkillBin = StaticDataSkillBin::s_data[allSkills.at(i)];
		if(pSkillBin != NULL)
		{
			std::string effectName = pSkillBin->releaseEffect;
			if(effectName != "" && effectName != "null")
				allSkillsEffectNames.push_back(effectName);

			effectName = pSkillBin->flyingItemEffect;
			if(effectName != "" && effectName != "null")
				allSkillsEffectNames.push_back(effectName);

			effectName = pSkillBin->flyingEndedEffect;
			if(effectName != "" && effectName != "null")
				allSkillsEffectNames.push_back(effectName);
		}
	}

	// 3. load animations
	for(unsigned int i = 0; i < allSkillsEffectNames.size(); i++)
	{
		std::string skillEffectName = allSkillsEffectNames.at(i);
		if(s_skillsEffectsCache[skillEffectName] == NULL)
		{
			CCLegendAnimation* pAnim = CCLegendAnimation::create(skillEffectName);
			//pAnim->retain();
			s_skillsEffectsCache[skillEffectName] = pAnim;
		}
	}
}

void ActorUtils::addGoldMessageToScene(int newGold)
{
	// add message to the game scene
	auto scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	auto me = GameView::getInstance()->myplayer;

	std::string _str = "+";
	char diffStr[20];
	sprintf(diffStr,"%d", newGold);
	_str.append(diffStr);
	const char * goldStr  = StringDataManager::getString("getPlayerAddOfGold");
	_str.append(goldStr);

	auto pLabel = Label::createWithBMFont("res_ui/font/ziti_3.fnt",_str.c_str() );
	pLabel->setScale(1.2f);
	pLabel->setAnchorPoint(Vec2(0.5f,0.0f));
	pLabel->setPosition(Vec2(me->getPositionX() + 80, me->getPositionY()));
	auto  action = Sequence::create(
		MoveBy::create(0.8f, Vec2(0, 40)),
		RemoveSelf::create(),
		NULL);
	pLabel->runAction(action);
	scene->getActorLayer()->addChild(pLabel, SCENE_TOP_LAYER_BASE_ZORDER);
}