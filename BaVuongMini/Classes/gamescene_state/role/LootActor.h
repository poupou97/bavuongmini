#ifndef _LEGEND_GAMEACTOR_LOOTACTOR_H_
#define _LEGEND_GAMEACTOR_LOOTACTOR_H_

#include "GameActor.h"
#include "cocos2d.h"

USING_NS_CC;

/**
 * loot, this actor is on the ground
 * @author zhaogang
 * @date 2014.12.10 
 */
class LootActor : public GameActor
{
public:
	LootActor();
	virtual ~LootActor();

	bool init(const char* actorName, int number);

	virtual void update(float dt);

	virtual void showActorName(bool bShow);

private:
	void updateActorName();
	Node* m_pNameNode;

private:
	int m_state;
	float m_fStartTime;
	MotionStreak* m_streak;
	float m_flyingSpeed;
	float m_flyingScale;

	int m_lootType;
	int m_nLootAmout;
	int m_lootQulity;
};

#endif