#include "GameActorAnimation.h"

#include "GameActor.h"
#include "BaseFighter.h"
#include "../../legend_engine/LegendAnimationCache.h"

GameActorAnimation::GameActorAnimation()
: mComponentSize(0)
, mAnimSize(0)
, m_actIdx(0)
, m_lastAnimName(0)
, m_curAnimName(0)
{
	this->setCascadeOpacityEnabled(true);   // opacity可作用于子结点
	this->setCascadeColorEnabled(true);
}

GameActorAnimation::~GameActorAnimation()
{
	// delete array ( this will NOT delete CCLegendAnimation instance )
	for(int i = 0; i < mComponentSize; i++)
		CC_SAFE_DELETE_ARRAY(m_pAllAnims[i]);
	CC_SAFE_DELETE_ARRAY(m_pAllAnims);
}

GameActorAnimation* GameActorAnimation::create(const char* resPath, std::string* animList, const char* roleName, int componentSize, int animSize)
{
	auto pAnim=new GameActorAnimation();
	if(pAnim && pAnim->init(resPath, animList, roleName, componentSize, animSize))
	{
		pAnim->autorelease();
		return pAnim;
	}
	CC_SAFE_DELETE(pAnim);
	return NULL;
}

bool GameActorAnimation::init(const char* resPath, std::string* animList, const char* roleName, int componentSize, int animSize)
{
	mResPath = resPath;
	mRoleName = roleName;
	for(int i = 0; i < animSize; i++)
	{
		mAnimNameList.push_back(animList[i]);
	}
	mComponentSize = componentSize;
	mAnimSize = animSize;

	mComponentVisible.clear();
	for(int j = 0; j < componentSize; j++)
		mComponentVisible.push_back(true);

	// init animation's container
	m_pAllAnims = new CCLegendAnimation**[componentSize];
	for(int i = 0; i < componentSize; i++)
	{
		m_pAllAnims[i] = new CCLegendAnimation*[animSize];
		for(int j = 0; j < animSize; j++)
		{
			m_pAllAnims[i][j] = NULL;
			//loadAnim(i, j);
		}
	}
	return true;
}

void GameActorAnimation::loadAnim(int componentIdx, int animIdx)
{
	if(componentIdx >= mComponentSize || animIdx >= mAnimSize)
		CCAssert( false, "out of range");

	std::string file = mResPath;
	file.append(mRoleName);
	file.append("/");
	file.append(mAnimNameList.at(animIdx));
	file.append(".anm");
		
	if(FileUtils::getInstance()->isFileExist(file))
	{
		//m_pAllAnims[i][j] = new CCLegendAnimation();
		//m_pAllAnims[i][j]->autorelease();
		//m_pAllAnims[i][j]->initWithAnmFilename(file.c_str());
		//m_pAllAnims[i][j]->setVisible(false);
		m_pAllAnims[componentIdx][animIdx] = CCLegendAnimationCache::sharedCache()->addCCLegendAnimation(file.c_str());
		m_pAllAnims[componentIdx][animIdx]->setVisible(false);
		//m_pAllAnims[i][j]->setScale(ACTOR_ANIM_SCALE);
		this->addChild(m_pAllAnims[componentIdx][animIdx], componentIdx);
	}
	else
	{
		CCLOG("could not find animation: %s", file.c_str());
		//CCAssert(false, "could not find animation");
		m_pAllAnims[componentIdx][animIdx] = NULL;
	}
}

CCLegendAnimation* GameActorAnimation::getAnim(int componentIdx, int animIdx)
{
	if(componentIdx >= mComponentSize || animIdx >= mAnimSize)
		CCAssert( false, "out of range");

	// first, if this componet has not been loaded, load it
	if(m_pAllAnims[componentIdx][animIdx] == NULL)
		loadAnim(componentIdx, animIdx);

	return  m_pAllAnims[componentIdx][animIdx];
}

void GameActorAnimation::setScale(float scale)
{
	//m_animScale = scale;
	for(int i = 0; i < mComponentSize; i++)
	{
		for(int j = 0; j < mAnimSize; j++)
		{
			auto anim = m_pAllAnims[i][j];
			if(anim != NULL)
			{
				anim->setScale(scale);
			}
		}
	}
}

void GameActorAnimation::setPlaySpeed(float speed)
{
	for(int i = 0; i < mComponentSize; i++)
	{
		for(int j = 0; j < mAnimSize; j++)
		{
			auto anim = m_pAllAnims[i][j];
			if(anim != NULL)
			{
				anim->setPlaySpeed(speed);
			}
		}
	}
}

void GameActorAnimation::setPlayLoop(bool loop)
{
	for(int i = 0; i < mComponentSize; i++)
	{
		for(int j = 0; j < mAnimSize; j++)
		{
			auto anim = m_pAllAnims[i][j];
			if(anim != NULL)
			{
				anim->setPlayLoop(loop);
			}
		}
	}
}

void GameActorAnimation::setAction(int action)
{
	//CCAssert(action >= 0 && action < 4, "invalid action");
	if(action < 0 || action >= ANIM_DIR_MAX)
		return;

	for(int i = 0; i < mComponentSize; i++)
	{
		for(int j = 0; j < mAnimSize; j++)
		{
			auto anim = m_pAllAnims[i][j];
			if(anim != NULL)
			{
				anim->setAction(action);
			}
		}
	}

	m_actIdx = action;
}
int GameActorAnimation::getActionIdx()
{
	return m_actIdx;
}

void GameActorAnimation::play()
{
	for(int i = 0; i < mComponentSize; i++)
	{
		for(int j = 0; j < mAnimSize; j++)
		{
			auto anim = m_pAllAnims[i][j];
			if(anim != NULL)
			{
				anim->play();
			}
		}
	}
}

void GameActorAnimation::stop()
{
	for(int i = 0; i < mComponentSize; i++)
	{
		for(int j = 0; j < mAnimSize; j++)
		{
			auto anim = m_pAllAnims[i][j];
			if(anim != NULL)
			{
				anim->stop();
			}
		}
	}
}

void GameActorAnimation::showComponent(int componentIdx, bool bVisible)
{
	// record the visible compnent
	int usingComponentIdx = -1;
	for(unsigned int i = 0; i < mComponentVisible.size(); i++)
	{
		if(mComponentVisible[i])
		{
			usingComponentIdx = i;
			break;
		}
	}

	mComponentVisible[componentIdx] = bVisible;

	if(!bVisible)
	{
		for(int j = 0; j < mAnimSize; j++)
		{
			auto anim = m_pAllAnims[componentIdx][j];
			if(anim != NULL)
			{
				anim->setVisible(false);
			}
		}
		return;
	}

	// first, if this componet has not been loaded, load it
	if(m_pAllAnims[componentIdx][m_curAnimName] == NULL)
		loadAnim(componentIdx, m_curAnimName);

	// then, synchronize the animation
	if(usingComponentIdx == -1)
		return;   // no need to synchronize the animation

	if(m_pAllAnims[usingComponentIdx][m_curAnimName] != NULL)
		m_pAllAnims[componentIdx][m_curAnimName]->synchronizeFrom(m_pAllAnims[usingComponentIdx][m_curAnimName]);
	m_pAllAnims[componentIdx][m_curAnimName]->setAction(this->m_actIdx);
}

void GameActorAnimation::setAnimName(int animName)
{
	// if you want to display one anim
	// first, you should load it
	for(int i = 0; i < mComponentSize; i++)
	{
		if(mComponentVisible[i])
		{
			if(m_pAllAnims[i][animName] == NULL)
				loadAnim(i, animName);
		}
	}

	m_lastAnimName = m_curAnimName;

	//for(int i = 0; i < mComponentSize; i++)
	//{
	//	for(int j = 0; j < mAnimSize; j++)
	//	{
	//		auto anim = m_pAllAnims[i][j];
	//		if(anim != NULL)
	//		{
	//			anim->setVisible(false);
	//		}
	//	}
	//}

	for(int i = 0; i < mComponentSize; i++)
	{
		auto anim = m_pAllAnims[i][m_lastAnimName];
		if(anim != NULL)
			anim->setVisible(false);
	}

	for(int i = 0; i < mComponentSize; i++)
	{
		if(!mComponentVisible[i])
			continue;

		auto curAnim = m_pAllAnims[i][animName];
		if(curAnim != NULL)
		{
			curAnim->restart();
			curAnim->play();
			//curAnim->setAction(mMainActor->get);
			curAnim->setVisible(true);
		}
	}

	m_curAnimName = animName;
}
int GameActorAnimation::getAnimName()
{
	return m_curAnimName;
}
