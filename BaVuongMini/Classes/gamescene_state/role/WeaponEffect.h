#ifndef _LEGEND_GAMEACTOR_WEAPONEFFECT_H_
#define _LEGEND_GAMEACTOR_WEAPONEFFECT_H_

#include "cocos2d.h"

USING_NS_CC;

class CCLegendAnimation;

#define GAME_WEAPON_EFFECT_KEYWORD "WeaponEffect"
#define GAME_WEAPON_EFFECT_TYPE_FIRE "WeaponEffectFire"
#define GAME_WEAPON_EFFECT_TYPE_SPARKLE "WeaponEffectSparkle"

#define GAME_WEAPON_TYPE_LANCE 0
#define GAME_WEAPON_TYPE_FAN 1
#define GAME_WEAPON_TYPE_KNIFE 2
#define GAME_WEAPON_TYPE_BOW 3

class WeaponEffectDefine
{
public:
	std::string weaponEffectType;   // 武器特效类型，比如火焰等
	std::string weaponEffectFileName;   // 具体的粒子特效文件名，同一个特效类型，可以用不同的粒子特效
};

class WeaponEffectCache
{
public:
	std::string effectFileName;
	ParticleSystem* effectInstance;
};

/**
 * 武器特效处理类
 * @author zhaogang
 * @version 0.1.0
 */
class WeaponEffect
{
public:
	static void attach(WeaponEffectDefine& effectDef, int weaponType, CCLegendAnimation* roleAnim);

	// remove the specific weapon effect
	static void removeWeaponEffect(CCLegendAnimation* roleAnim, const char* effectName);
	// remove all weapon special effect
	static void removeAllWeaponEffects(CCLegendAnimation* roleAnim);

	static bool hasWeaponEffect(const char* effectFileName, CCLegendAnimation* roleAnim);

	static std::string getWeaponEffectType(const char* effectFileName);

	// get the pointer of the effect name
	static const char* getEffectName(const char* effectName);

	static ParticleSystem* getParticle(const char* plistName);

private:
	static std::map<std::string, int> s_effectNameId;

	static std::vector<WeaponEffectCache> s_effectCache;
};

/**
 * 武器特效 - 火焰特效
 * @author zhaogang
 * @version 0.1.0
 */
class WeaponEffectFire
{
public:
	static void attach(WeaponEffectDefine& effectDef, int weaponType, CCLegendAnimation* roleAnim);

private:
	static void attachToLance(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim);
	static void attachToFan(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim);
	static void attachToKnife(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim);
	static void attachToBow(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim);

private:
	static std::string s_effectTag;
};

/**
 * 武器特效 - sparkle
 * @author zhaogang
 * @version 0.1.0
 */
class WeaponEffectSparkle
{
public:
	static void attach(WeaponEffectDefine& effectDef, int weaponType, CCLegendAnimation* roleAnim);

private:
	static void attachToLance(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim);
	static void attachToFan(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim);
	static void attachToKnife(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim);
	static void attachToBow(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim);

private:
	static std::string s_effectTag;
};

#endif