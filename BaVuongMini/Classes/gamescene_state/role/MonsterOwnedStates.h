#ifndef _MONSTER_OWNED_STATES_H
#define _MONSTER_OWNED_STATES_H

#include "State.h"

class Monster;
class ActionDeathContext;

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class MonsterGlobalState : public State<Monster>
{  
private:
  
  MonsterGlobalState(){}
  
  //copy ctor and assignment should be private
  MonsterGlobalState(const MonsterGlobalState&);
  MonsterGlobalState& operator=(const MonsterGlobalState&);
 
public:

  static MonsterGlobalState* Instance();
  
  virtual void Enter(Monster* self){}

  virtual void Execute(Monster* self);

  virtual void Exit(Monster* self){}
};


//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class MonsterStand : public State<Monster>
{
private:
  
  MonsterStand(){}

  //copy ctor and assignment should be private
  MonsterStand(const MonsterStand&);
  MonsterStand& operator=(const MonsterStand&); 
  
public:

  static MonsterStand* Instance();
  
  virtual void Enter(Monster* self);

  virtual void Execute(Monster* self);

  virtual void Exit(Monster* self);

};



//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class MonsterRun : public State<Monster>
{
private:
  
  MonsterRun(){}

  //copy ctor and assignment should be private
  MonsterRun(const MonsterRun&);
  MonsterRun& operator=(const MonsterRun&);
 
public:

  static MonsterRun* Instance();
  
  virtual void Enter(Monster* self);

  virtual void Execute(Monster* self);

  virtual void Exit(Monster* self);

};

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class MonsterAttack : public State<Monster>
{
private:
  
  MonsterAttack(){}

  //copy ctor and assignment should be private
  MonsterAttack(const MonsterAttack&);
  MonsterAttack& operator=(const MonsterAttack&);
 
public:

  static MonsterAttack* Instance();
  
  virtual void Enter(Monster* self);

  virtual void Execute(Monster* self);

  virtual void Exit(Monster* self);

};

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class MonsterDeath : public State<Monster>
{
private:
  
  MonsterDeath(){}

  //copy ctor and assignment should be private
  MonsterDeath(const MonsterDeath&);
  MonsterDeath& operator=(const MonsterDeath&);
 
public:

  static MonsterDeath* Instance();
  
  virtual void Enter(Monster* self);

  virtual void Execute(Monster* self);

  virtual void Exit(Monster* self);

private:
	void initDeathAction(ActionDeathContext* context, Monster* self);
	void updateDeathAction(ActionDeathContext* context, Monster* self);
};

//------------------------------------------------------------------------
class MonsterKnockBack : public State<Monster>
{
private:
  
  MonsterKnockBack(){}

  //copy ctor and assignment should be private
  MonsterKnockBack(const MonsterKnockBack&);
  MonsterKnockBack& operator=(const MonsterKnockBack&);
 
public:

  static MonsterKnockBack* Instance();
  
  virtual void Enter(Monster* self);

  virtual void Execute(Monster* self);

  virtual void Exit(Monster* self);

};

//------------------------------------------------------------------------
class MonsterCharge : public State<Monster>
{
private:
  
  MonsterCharge(){}

  //copy ctor and assignment should be private
  MonsterCharge(const MonsterCharge&);
  MonsterCharge& operator=(const MonsterCharge&); 
  
public:

  static MonsterCharge* Instance();
  
  virtual void Enter(Monster* self);
  virtual void Execute(Monster* self);
  virtual void Exit(Monster* self);
};

//------------------------------------------------------------------------
class MonsterJump : public State<Monster>
{
private:
  
  MonsterJump(){}

  //copy ctor and assignment should be private
  MonsterJump(const MonsterJump&);
  MonsterJump& operator=(const MonsterJump&); 
  
public:

  static MonsterJump* Instance();
  
  virtual void Enter(Monster* self);
  virtual void Execute(Monster* self);
  virtual void Exit(Monster* self);
};

//------------------------------------------------------------------------
class MonsterDisappear : public State<Monster>
{
private:
  
  MonsterDisappear(){}

  //copy ctor and assignment should be private
  MonsterDisappear(const MonsterDisappear&);
  MonsterDisappear& operator=(const MonsterDisappear&); 
  
public:

  static MonsterDisappear* Instance();
  
  virtual void Enter(Monster* self);
  virtual void Execute(Monster* self);
  virtual void Exit(Monster* self);
};

#endif