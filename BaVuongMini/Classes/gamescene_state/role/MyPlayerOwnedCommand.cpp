#include "MyPlayerOwnedCommand.h"

#include "MyPlayer.h"
#include "MyPlayerAI.h"
#include "MyPlayerOwnedStates.h"
#include "../GameSceneState.h"
#include "../../utils/GameUtils.h"
#include "../../legend_engine/GameWorld.h"
#include "../../GameView.h"
#include "../../messageclient/element/CMapInfo.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../exstatus/ExStatusType.h"
#include "../skill/GameFightSkill.h"
#include "PresentBox.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CPresentBox.h"

std::string MyPlayerCommandAttack::s_latestSkillId = "";
long MyPlayerCommandAttack::s_latestSkillStartTime = 0;

MyPlayerCommandAttack::MyPlayerCommandAttack()
: finishedConditionKill(false)
, startTime(0)
{
	setType(ActorCommand::type_attack);
}

MyPlayerCommandAttack::~MyPlayerCommandAttack()
{
}

// this function is not good soluton, please attention!!!
bool MyPlayerCommandAttack::isSame(ActorCommand* otherCmd)
{
	if(otherCmd->getStatus() != this->getStatus())
		return false;

	auto cmd = dynamic_cast<MyPlayerCommandAttack*>(otherCmd);
	if(cmd->skillId == this->skillId
		&& cmd->targetId == this->targetId)
		return true;

	return false;
}

ActorCommand* MyPlayerCommandAttack::clone()
{
	auto cmd = new MyPlayerCommandAttack();
	cmd->targetId = this->targetId;
	cmd->skillId = this->skillId;
	cmd->skillProcessorId = skillId;
	cmd->attackMaxCount = this->attackMaxCount;
	cmd->attackSpeed = this->attackSpeed;
	cmd->sendAttackRequest = this->sendAttackRequest;
	cmd->finishedConditionKill = this->finishedConditionKill;

	return cmd;
}

bool MyPlayerCommandAttack::checkConditionKill(BaseFighter* mainActor)
{
	auto targetActor = dynamic_cast<BaseFighter*>(mainActor->getGameScene()->getActor(targetId));
	if(targetActor == NULL 
		|| mainActor->getLockedActorId() != targetActor->getRoleId()
		|| targetActor->isAction(ACT_DIE))
	{
		mainActor->changeAction(ACT_STAND);
		setStatus(ActorCommand::status_finished);
		return true;
	}
	else
	{
		// attack again, until the target is dead
		// finished one attck, continue
		if(attackCount > lastAttackCount)
		{
			// attack again, until reach the max attack count
			mainActor->changeAction(ACT_ATTACK);
			lastAttackCount = attackCount;
		}
	}

	return false;
}

void MyPlayerCommandAttack::Enter(GameActor* mainActor)
{
	this->startTime = GameUtils::millisecondNow();

	s_latestSkillId = this->skillId;
	s_latestSkillStartTime = this->startTime;
}

void MyPlayerCommandAttack::Execute(GameActor* mainActor)
{
	auto self = (MyPlayer*)mainActor;

	// when to stop attacking ?
	// if actor is silent, BUT the actor can do moving
	if(self->hasExStatus(ExStatusType::taciturn))
	{
		setStatus(ActorCommand::status_finished);
		return;
	}

	self->checkAttack();

	if(getStatus() == ActorCommand::status_new)
	{
		char targetScopeType = self->getGameFightSkill(skillId.c_str(), this->skillProcessorId.c_str())->getTargetScopeType();
		if(!GameFightSkill::isNoneLock(targetScopeType))
		{
			// check the target's validation
			auto bf = dynamic_cast<BaseFighter*>(mainActor->getGameScene()->getActor(targetId));
			if(bf != NULL)
			{
				// target is invalid
				if(bf->getType() == GameActor::type_picking || bf->getType() == GameActor::type_npc)
				{
					// temp solution
					self->GetFSM()->ChangeState(MyPlayerStand::Instance());
					setStatus(ActorCommand::status_finished);
					return;
				}
			}
			else
			{
				// 如果攻击指令来自玩家自己，则允许空打
				if(this->commandSource == ActorCommand::source_from_player)
				{
					self->changeAction(ACT_ATTACK);
					setStatus(ActorCommand::status_doing);
					return;
				}
			}
		}

		setStatus(ActorCommand::status_doing);
	}
	else if(getStatus() == ActorCommand::status_doing)
	{
		// 立即结束指令
		if(this->isFinishImmediately && this->isWaitingAction)
		{
			// finished the attack animation completely
			if(attackCount > lastAttackCount)
			{
				self->changeAction(ACT_STAND);
				setStatus(ActorCommand::status_finished);
				return;
			}
		}

		// this attack command has been finished
		if(finishedConditionKill)
		{
			checkConditionKill(self);
		}
		else
		{
			bool result = checkConditionAttackMaxCount(self);
		}
	}
	else if(getStatus() == ActorCommand::status_finished)
	{
		self->GetFSM()->ChangeState(MyPlayerStand::Instance());
	}
}

void MyPlayerCommandAttack::Exit(GameActor* mainActor)
{
	// after attack command, the player will be standing
	// 主要用于一个攻击动作完成后，使其恢复为站立状态，以便下一个紧接的攻击指令，能够重新正确的changeAction(ACT_ATTACK)
	auto bf = dynamic_cast<BaseFighter*>(mainActor);
	if(bf != NULL && !bf->isDead())
	{
		if(bf->isAction(ACT_ATTACK))
			bf->changeAction(ACT_STAND);
	}

	// if the command comes from source_from_robot, ignore it
	if(this->commandSource == source_from_player)
	{
		auto self = (MyPlayer*)mainActor;
		self->getMyPlayerAI()->setStartPoint(self->getWorldPosition());
	}
}

//--------------------------------------------------------------------

#define END_OF_CROSS_MAP "none"

MyPlayerCommandMove::MyPlayerCommandMove()
{
	setType(ActorCommand::type_move);
	targetMapId = "";
	targetPosition.x = -1;
	targetPosition.y = -1;

	autoMove = true;
}

MyPlayerCommandMove::~MyPlayerCommandMove()
{
}

bool MyPlayerCommandMove::isSame(ActorCommand* otherCmd)
{
	auto cmd = dynamic_cast<MyPlayerCommandMove*>(otherCmd);

	if(cmd->targetMapId == this->targetMapId
		&& cmd->targetPosition.equals(this->targetPosition)
		&& cmd->method == this->method
		&& cmd->playerOperationMethod == this->playerOperationMethod)
		return true;

	return false;
}

void MyPlayerCommandMove::Enter(GameActor* mainActor)
{
}

void MyPlayerCommandMove::EnterStatusDoing(GameActor* mainActor)
{
	return;
	if(playerOperationMethod == operation_touchscreen)
	{
		auto self = (MyPlayer*)mainActor;
		int size = self->getPaths()->size();
		// when self is moving, send 1102.
		// because 1102 has been sent when player go to the MyPlayerRun::enter()
		if(size >= 1 && self->isMoving())   
		{
			Vec2* destination = self->getPaths()->at(size - 1);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1102, destination);
		}
	}
}

void MyPlayerCommandMove::Execute(GameActor* mainActor)
{
	auto self = (MyPlayer*)mainActor;

	MyPlayerCommandMove* cmd = this;

	// when to stop moving ?
	// if actor is immobilized, BUT the actor can do attacking
	if(self->hasExStatus(ExStatusType::immobilize))
	{
		self->GetFSM()->ChangeState(MyPlayerStand::Instance(), false);
		cmd->setStatus(ActorCommand::status_finished);
		return;
	}

	std::string currentMapId = GameView::getInstance()->getMapInfo()->mapid();

	// status: new
	if(cmd->getStatus() == ActorCommand::status_new)
	{
		if(cmd->method == MyPlayerCommandMove::method_direct)
		{
			self->clearPath();

			Vec2* target = new Vec2(cmd->targetPosition);
			self->getPaths()->push_back(target);

			if(self->getPaths()->size() > 0)
			{
				self->GetFSM()->ChangeState(MyPlayerRun::Instance(), false);

				//EnterStatusDoing(mainActor);
				cmd->setStatus(ActorCommand::status_doing);
			}
			else
			{
				self->GetFSM()->ChangeState(MyPlayerStand::Instance(), false);
				cmd->setStatus(ActorCommand::status_finished);
			}
		}
		else
		{
			Vec2 target = Vec2(-1, -1);

			// no cross map
			if(cmd->targetMapId == "")
			{
				if(cmd->targetPosition.x == -1 && cmd->targetPosition.y == -1)
					CCAssert(false, "invalid move command");
				else
				{
					target = cmd->targetPosition;
				}
			}
			else if(cmd->targetMapId == currentMapId)
			{
				// reached
				if(cmd->targetPosition.x == -1 && cmd->targetPosition.y == -1)
				{
					self->GetFSM()->ChangeState(MyPlayerStand::Instance(), false);
					cmd->setStatus(ActorCommand::status_finished);
					return;
				}
				else
				{
					target = cmd->targetPosition;
					cmd->nextMapId = END_OF_CROSS_MAP;   // the final map, no the next one
				}
			}
			// cross map
			else if(cmd->targetMapId != currentMapId)
			{
				std::vector<std::string>* crossMapPaths = GameWorld::searchLevelPath(currentMapId, cmd->targetMapId);
				if(crossMapPaths == NULL)
				{
					self->GetFSM()->ChangeState(MyPlayerStand::Instance(), false);
					cmd->setStatus(ActorCommand::status_finished);
				}
				else
				{
					// 寻找到的路径可能跨越很多张地图，但只导航到邻接的地图，所以使用1
					cmd->nextMapId = crossMapPaths->at(1);

					// search path
					const std::string& currentMapId = GameView::getInstance()->getMapInfo()->mapid();
					Vec2 targetPos = GameView::getInstance()->getGameScene()->getDoorPosition(currentMapId, cmd->nextMapId);
					target = targetPos;
				}
				CC_SAFE_DELETE(crossMapPaths);
			}

			// got target, search path to it
			if(target.x == -1 && target.y == -1)
			{
				// do nothing
			}
			else
			{
				self->searchPath(target);

				if(self->getPaths()->size() > 0)
				{
					self->GetFSM()->ChangeState(MyPlayerRun::Instance(), false);

					EnterStatusDoing(mainActor);
					cmd->setStatus(ActorCommand::status_doing);
				}
				else
				{
					self->GetFSM()->ChangeState(MyPlayerStand::Instance(), false);
					cmd->setStatus(ActorCommand::status_finished);
					CCLOG("the target position is NOT reachable, please check the point in the scene editor");
				}
			}
		}
	}
	// status: moving
	else if(cmd->getStatus() == ActorCommand::status_doing)
	{
		// 同地图内移动
		if(cmd->targetMapId == "" || cmd->nextMapId == END_OF_CROSS_MAP)
		{
			if (self->getPaths()->size() <= 0)
			{
				self->GetFSM()->ChangeState(MyPlayerStand::Instance());
				cmd->setStatus(ActorCommand::status_finished);
				return;
			}
		}
		// 跨地图移动
		else 
		{
			// when current mapId is command.nextMapId, re-search path
			if(cmd->nextMapId == currentMapId)
			{
				cmd->setStatus(ActorCommand::status_new);
			}
			else
			{
				if(self->isAction(ACT_STAND))
				{
					// patch! confirm the player enter teleport door
					static int tick = 0;
					if(tick++ > 15)
					{
						GameMessageProcessor::sharedMsgProcessor()->sendReq(1104, self);
						tick = 0;
					}
				}
			}
		}
	}
}

void MyPlayerCommandMove::Exit(GameActor* mainActor)
{
	auto self = (MyPlayer*)mainActor;

	self->clearPath();
	if(this->commandSource == source_from_player)
	{
		self->getMyPlayerAI()->setStartPoint(self->getWorldPosition());
	}
}


//--------------------------------------------------------------------
MyPlayerCommandPick::MyPlayerCommandPick()
	:pickingNumber(0)
	,pickingMaxNumber(1)
	,pickType(1)
	,targetPos(Vec2(0, 0))
{
	setType(ActorCommand::type_pick);
}

MyPlayerCommandPick::~MyPlayerCommandPick()
{
}

// this function is not good soluton, please attention!!!
bool MyPlayerCommandPick::isSame(ActorCommand* otherCmd)
{
	return false;
}

void MyPlayerCommandPick::Enter(GameActor* mainActor)
{
	if (GameView::getInstance()->myplayer->hasExStatus(ExStatusType::collect_disable))
	{
		if (GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::guildFight)
		{
			GameView::getInstance()->showAlertDialog(StringDataManager::getString("FamilyFightUI_canNotPick"));
		}

		setStatus(ActorCommand::status_finished);
	}
}

void MyPlayerCommandPick::Execute(GameActor* mainActor)
{
	auto self = (MyPlayer*)mainActor;

	// 若是拾取宝箱，调用checkPick（自动寻路走到宝箱身边，再拾取宝箱）
	if (PICK_TYPE_PRESENTBOX == pickType)
	{
		self->checkPick();
	}

	if(getStatus() == ActorCommand::status_new)
	{
		if (PICK_TYPE_PRESENTBOX == pickType && MyPlayerRun::Instance() == self->GetFSM()->CurrentState())
		{
			return ;
		}

		self->GetFSM()->ChangeState(MyPlayerPick::Instance());

		setStatus(ActorCommand::status_doing);
	}
	else if(getStatus() == ActorCommand::status_doing)
	{
		if (PICK_TYPE_PRESENTBOX == pickType)
		{
			// 指令已完成
			if(pickingNumber >= pickingMaxNumber)
			{
				auto presentBox = (PresentBox*)GameView::getInstance()->getGameScene()->getActor(targetId);
				if (presentBox)
				{
					if (!presentBox->getActivied())
					{
						// 打断采集
						self->changeAction(ACT_STAND);

						const char * charInfo = StringDataManager::getString("PresentBox_openFailed");
						GameView::getInstance()->showAlertDialog(charInfo);
					}
					else
					{
						auto pPresentBox = new CPresentBox();
						pPresentBox->set_id(targetId);

						// 发送请求
						GameMessageProcessor::sharedMsgProcessor()->sendReq(5014, (void *)pPresentBox);

						self->GetFSM()->ChangeState(MyPlayerStand::Instance());
						setStatus(ActorCommand::status_finished);

						delete pPresentBox;
					}
				}
				else
				{
					// 打断采集
					self->changeAction(ACT_STAND);

					const char * charInfo = StringDataManager::getString("PresentBox_openFailed");
					GameView::getInstance()->showAlertDialog(charInfo);

					setStatus(ActorCommand::status_finished);
				}
			}
			else
			{
				// todo
			}
		}
		else
		{
			// 指令已完成
			if(pickingNumber >= pickingMaxNumber)
			{
				// 请求
				// ReqStartCollect5011
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5011, GameView::getInstance()->myplayer->getLockedActor());

				self->GetFSM()->ChangeState(MyPlayerStand::Instance());
				setStatus(ActorCommand::status_finished);
			}
			else
			{
				// todo
			}
		}
	}
	else if(getStatus() == ActorCommand::status_finished)
	{
		self->GetFSM()->ChangeState(MyPlayerStand::Instance());
	}
}

void MyPlayerCommandPick::Exit(GameActor* mainActor)
{
	auto self = (MyPlayer*)mainActor;
	if(this->commandSource == source_from_player)
	{
		self->getMyPlayerAI()->setStartPoint(self->getWorldPosition());
	}
}

void MyPlayerCommandPick::init( int nType )
{
	if (PICK_TYPE_GENERAL == nType)
	{
		pickType = PICK_TYPE_GENERAL;
		this->setType(ActorCommand::type_pick);
	}
	else if (PICK_TYPE_PRESENTBOX == nType)
	{
		pickType = PICK_TYPE_PRESENTBOX;
		this->setType(ActorCommand::type_collectPresent);
	}
}


