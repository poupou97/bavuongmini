#ifndef _LEGEND_GAMEACTOR_OTHERPLAYER_H_
#define _LEGEND_GAMEACTOR_OTHERPLAYER_H_

#include <vector>

#include "BasePlayer.h"
#include "cocos2d.h"
#include "../../utils/JoyStick.h"
#include "../../legend_engine/CCLegendAnimation.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

class ActorCommand;
class GameActorAnimation;

/**
 * 其他玩家
 * @author zhaogang
 * @version 0.1.0
 */
class OtherPlayer : public BasePlayer
{
// these state classes are friends, they can access the private members
friend class OtherPlayerStand;
friend class OtherPlayerRun;
friend class OtherPlayerAttack;

friend class MyPlayerCommandAttack;
friend class MyPlayerCommandMove;

public:
	OtherPlayer();
	virtual ~OtherPlayer();

	virtual void update(float dt);

	virtual void onEnter();

	void changeAction(int action);
	virtual bool isAction(int action);

	virtual bool isStanding();

	virtual void setAnimScale(float scale);

	virtual Vec2 getSortPoint();

	//an instance of the state machine class
	StateMachine<OtherPlayer>*  m_pStateMachine;
	StateMachine<OtherPlayer>*  GetFSM()const{return m_pStateMachine;}
	
	bool init(const char* name);

	virtual void initWeaponEffect();

	virtual void onRemoveFromGameScene();

	/**
	* 施展攻击
	* @param skillId
	* @param skillType, 技能类型, -1表示不关心技能类型
	*/
	virtual void onAttack(const char* skillId, const char* skillProcessorId, long long targetId, bool bDoubleAttack, int skillType = -1);

	/** 受到伤害 */
	virtual void onDamaged(BaseFighter* source, int damageNum, bool bDoubleAttacked, CSkillResult* skillResult = NULL);

public:
	void removeShowGeneral();

private:
	void updateGeneralShow();

private:	
	float m_dt;
};

#endif