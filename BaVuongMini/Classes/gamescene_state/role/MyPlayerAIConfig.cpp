#include "MyPlayerAIConfig.h"

#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/role/MyPlayerAI.h"
#include "../../messageclient/element/MapRobotDrug.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/robot_ui/RobotMainUI.h"
#include "../GameSceneState.h"
#include "../MainScene.h"


bool MyPlayerAIConfig::sEnableRobot = false;
std::string MyPlayerAIConfig::sRobotSkillsId[ROBOT_SETUP_SKILL_MAX];
int MyPlayerAIConfig::sMoveRange = 20;
int MyPlayerAIConfig::sGenerallimitMp = 50;
int MyPlayerAIConfig::sGenerallimitHp = 50;
int MyPlayerAIConfig::sRolelimitMp = 50;
int MyPlayerAIConfig::sRolelimitHp =50;
int MyPlayerAIConfig::sGoodsPickAndSell[ROBOT_SETUP_PICKANDSELL_MAX];
int MyPlayerAIConfig::sRepairEquipment =1;
int MyPlayerAIConfig::sGeneralAutoBuyHp=0;
int MyPlayerAIConfig::sGeneralAutoBuyMp=0;
int MyPlayerAIConfig::sGeneralAutoUseMp=1;
int MyPlayerAIConfig::sGeneralAutoUseHp=1;
int MyPlayerAIConfig::sRoleAutoBuyMp=0;
int MyPlayerAIConfig::sRoleAutoBuyHp=0;
int MyPlayerAIConfig::sRoleAutoUseMp=1;
int MyPlayerAIConfig::sRoleAutoUseHp=1;
int MyPlayerAIConfig::sAutomaticSkill=0;

MyPlayerAIConfig::MyPlayerAIConfig()
{
	m_isSaveState = false;
}

MyPlayerAIConfig::~MyPlayerAIConfig()
{

}

void MyPlayerAIConfig::enableRobot(bool bValue) {
	sEnableRobot = bValue;
}
bool MyPlayerAIConfig::isEnableRobot() {
	return sEnableRobot;
}

void MyPlayerAIConfig::setMoveRange(int range)
{
	sMoveRange = range;

	MyPlayerAIConfig::pushConfig(ROBOT_SETUP_MOVE_RANGE,ROBOT_SAVE_TYPE_INT,"",sMoveRange);
}
int MyPlayerAIConfig::getMoveRange()
{
	return sMoveRange;
}

void MyPlayerAIConfig::saveMoveRange(int range)
{
	//saveConfig(ROBOT_SETUP_MOVE_RANGE,sMoveRange);
}

void MyPlayerAIConfig::setSkillList(std::vector<std::string> allSkills)
{
	CCAssert(allSkills.size() <= ROBOT_SETUP_SKILL_MAX, "out of range");
	for(unsigned int i = 0; i < allSkills.size(); i++)
	{
		sRobotSkillsId[i] = allSkills.at(i);
	}
}
void MyPlayerAIConfig::setSkillList(int index, std::string skillId)
{
	CCAssert(index < ROBOT_SETUP_SKILL_MAX, "out of range");
	sRobotSkillsId[index] = skillId;
	std::string playerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
	for(int i = 0; i < ROBOT_SETUP_SKILL_MAX; i++)
	{
		char indexStr[3];
		sprintf(indexStr, "%d", i);
		UserDefault::getInstance()->setStringForKey(
			(playerName+ROBOT_SETUP_SKILL_ID_PREFIX+indexStr).c_str(), sRobotSkillsId[i]);
	}

	//saveConfig();

	GameView::getInstance()->myplayer->getMyPlayerAI()->updateSkillList(sRobotSkillsId);
}

void MyPlayerAIConfig::setRoleUseHp( int isSelect )
{
	sRoleAutoUseHp = isSelect;

	MyPlayerAIConfig::pushConfig("setCheckBoxuseHp",ROBOT_SAVE_TYPE_INT,"",sRoleAutoUseHp);

	//saveConfig("setCheckBoxuseHp",sRoleAutoUseHp);
}

int MyPlayerAIConfig::getRoleUseHp()
{
	return sRoleAutoUseHp;
}

void MyPlayerAIConfig::setRoleUseMp( int isSelect )
{
	sRoleAutoUseMp = isSelect;

	MyPlayerAIConfig::pushConfig("setCheckBoxuseMp",ROBOT_SAVE_TYPE_INT,"",sRoleAutoUseMp);

	//saveConfig("setCheckBoxuseMp",sRoleAutoUseMp);
}

int MyPlayerAIConfig::getRoleUseMp()
{
	return sRoleAutoUseMp;
}

void MyPlayerAIConfig::setRoleBuyHp( int isSelect )
{
	sRoleAutoBuyHp = isSelect;

	MyPlayerAIConfig::pushConfig("setCheckBoxBuyHp",ROBOT_SAVE_TYPE_INT,"",sRoleAutoBuyHp);

	//saveConfig("setCheckBoxBuyHp",sRoleAutoBuyHp);
}

int MyPlayerAIConfig::getRoleBuyHp()
{
	return sRoleAutoBuyHp;
}

void MyPlayerAIConfig::setRoleBuyMp( int isSelect )
{
	sRoleAutoBuyMp = isSelect;
	
	MyPlayerAIConfig::pushConfig("setCheckBoxBuyMp",ROBOT_SAVE_TYPE_INT,"",sRoleAutoBuyMp);

	//saveConfig("setCheckBoxBuyMp",sRoleAutoBuyMp);
}

int MyPlayerAIConfig::getRoleBuyMp()
{
	return sRoleAutoBuyMp;
}

void MyPlayerAIConfig::setGeneralUseHp( int isSelect )
{
	sGeneralAutoUseHp = isSelect;

	MyPlayerAIConfig::pushConfig("setCheckBoxGeneralHp",ROBOT_SAVE_TYPE_INT,"",sGeneralAutoUseHp);

	//saveConfig("setCheckBoxGeneralHp",sGeneralAutoUseHp);
}

int MyPlayerAIConfig::getGeneralUseHp()
{
	return sGeneralAutoUseHp;
}

void MyPlayerAIConfig::setGeneralUseMp( int isSelect )
{
	sGeneralAutoUseMp = isSelect;
	MyPlayerAIConfig::pushConfig("setCheckBoxGeneralMp",ROBOT_SAVE_TYPE_INT,"",sGeneralAutoUseMp);
	//saveConfig("setCheckBoxGeneralMp",sGeneralAutoUseMp);
}

int MyPlayerAIConfig::getGeneralUseMp()
{
	return sGeneralAutoUseMp;
}

void MyPlayerAIConfig::setGeneralBuyHp( int isSelect )
{
	sGeneralAutoBuyHp = isSelect;
	MyPlayerAIConfig::pushConfig("setCheckBoxGeneralBuyHp",ROBOT_SAVE_TYPE_INT,"",sGeneralAutoBuyHp);
	//saveConfig("setCheckBoxGeneralBuyHp",sGeneralAutoBuyHp);
}

int MyPlayerAIConfig::getGeneralBuyHp()
{
	return sGeneralAutoBuyHp;
}

void MyPlayerAIConfig::setGeneralBuyMp( int isSeclect )
{
	sGeneralAutoBuyMp = isSeclect;
	MyPlayerAIConfig::pushConfig("setCheckBoxGeneralBuyMp",ROBOT_SAVE_TYPE_INT,"",sGeneralAutoBuyMp);
	//saveConfig("setCheckBoxGeneralBuyMp",sGeneralAutoBuyMp);
}

int MyPlayerAIConfig::getGeneralBuyMp()
{
	return sGeneralAutoBuyMp;
}

void MyPlayerAIConfig::setAutoRepairEquip( int isSelect )
{
	sRepairEquipment = isSelect;
	MyPlayerAIConfig::pushConfig("setCheckBoxRepairEquip",ROBOT_SAVE_TYPE_INT,"",sRepairEquipment);
	//saveConfig("setCheckBoxRepairEquip",sRepairEquipment);
}

int MyPlayerAIConfig::getAutoRepairEquip()
{
	return sRepairEquipment;
}


// std::string MyPlayerAIConfig::getDefaultDrug(std::string drugIndex_ )
// {
// 	std::string playerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
// 	std::string drugName_;
// 	if (strcmp(drugIndex_.c_str(),"Button_retore_1")==0)
// 	{
// 		playerName.append("Button_retore_1");
// 		drugName_ = UserDefault::getInstance()->getStringForKey(playerName.c_str());
// 		//drugName_ = sdrugId;
// 	}
// 	if (strcmp(drugIndex_.c_str(),"Button_retore_3")==0)
// 	{
// 		playerName.append("Button_retore_3");
// 		drugName_ = UserDefault::getInstance()->getStringForKey(playerName.c_str());
// 		//drugName_ = sdrugId;
// 	}
// 	if (strcmp(drugIndex_.c_str(),"Button_retore_2")==0)
// 	{
// 		playerName.append("Button_retore_2");
// 		drugName_ = UserDefault::getInstance()->getStringForKey(playerName.c_str());
// 		//drugName_ = sdrugId;
// 	}
// 	if (strcmp(drugIndex_.c_str(),"Button_retore_4")==0)
// 	{
// 		playerName.append("Button_retore_4");
// 		drugName_ = UserDefault::getInstance()->getStringForKey(playerName.c_str());
// 		//drugName_ = sdrugId;
// 	}
// 	return drugName_;
// }

void MyPlayerAIConfig::setGoodsPickAndSell( int checkBoxIndex,int isSelect )
{
	sgoodsCheckName = "";
	std::string playerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
	sgoodsCheckName.append(playerName.c_str());
	sgoodsCheckName.append("CheckBox_honggou_");
	char checkIndex[3];
	sprintf(checkIndex,"%d",checkBoxIndex);
	sgoodsCheckName.append(checkIndex);

	// the correct index is in 1 - 9
	CCAssert(checkBoxIndex > 0 && checkBoxIndex < ROBOT_SETUP_PICKANDSELL_MAX, "out of range");
	sGoodsPickAndSell[checkBoxIndex] = isSelect;
	
	// save config
	for (int i=1;i<ROBOT_SETUP_PICKANDSELL_MAX;i++)
	{
		std::string keyStr_setGoodPickAndSell = playerName;
		keyStr_setGoodPickAndSell.append("CheckBox_honggou_");
		char checkIndex[5];
		sprintf(checkIndex,"%d",i);
		keyStr_setGoodPickAndSell.append(checkIndex);

		if (strcmp(sgoodsCheckName.c_str(),keyStr_setGoodPickAndSell.c_str())==0)
		{
			UserDefault::getInstance()->setIntegerForKey(keyStr_setGoodPickAndSell.c_str(),sGoodsPickAndSell[checkBoxIndex]);
		}
	}
}

int MyPlayerAIConfig::getGoodsPickAndSell(int checkBoxIndex)
{
	// the correct index is in 1 - 9
	CCAssert(checkBoxIndex > 0 && checkBoxIndex < ROBOT_SETUP_PICKANDSELL_MAX, "out of range");
	return sGoodsPickAndSell[checkBoxIndex];
}

void MyPlayerAIConfig::saveConfig(std::string saveNameString,std::string value)
{	
	std::string playerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
	std::string keyStr_roleUseDrugMpName = playerName;
	keyStr_roleUseDrugMpName.append(saveNameString);
	UserDefault::getInstance()->setStringForKey(keyStr_roleUseDrugMpName.c_str(),value);
}

void MyPlayerAIConfig::saveConfig(std::string saveNameString,int value)
{
	std::string playerName = GameView::getInstance()->myplayer->getActiveRole()->rolebase().name();
	std::string keyStr_setRepair = playerName;
	keyStr_setRepair.append(saveNameString);
	UserDefault::getInstance()->setIntegerForKey(keyStr_setRepair.c_str(), value);

	/*
	for(int i = 0; i < ROBOT_SETUP_SKILL_MAX; i++)
	{
		char indexStr[3];
		sprintf(indexStr, "%d", i);
		UserDefault::getInstance()->setStringForKey(
			(playerName+ROBOT_SETUP_SKILL_ID_PREFIX+indexStr).c_str(), sRobotSkillsId[i]);
	}
	std::string keyStr = playerName;
	keyStr.append(ROBOT_SETUP_MOVE_RANGE);
	UserDefault::getInstance()->setIntegerForKey(keyStr.c_str(), sMoveRange);
	
	//protect panel
	std::string keyStr_setRoleUseHp = playerName;
	keyStr_setRoleUseHp.append("setCheckBoxuseHp");
	UserDefault::getInstance()->setIntegerForKey(keyStr_setRoleUseHp.c_str(), sRoleAutoUseHp);
	
	std::string keyStr_setRoleUseMp = playerName;
	keyStr_setRoleUseMp.append("setCheckBoxuseMp");
	UserDefault::getInstance()->setIntegerForKey(keyStr_setRoleUseMp.c_str(), sRoleAutoUseMp);

	std::string keyStr_setRoleBuyHp = playerName;
	keyStr_setRoleBuyHp.append("setCheckBoxBuyHp");
	UserDefault::getInstance()->setIntegerForKey(keyStr_setRoleBuyHp.c_str(), sRoleAutoBuyHp);

	std::string keyStr_setRoleBuyMp = playerName;
	keyStr_setRoleBuyMp.append("setCheckBoxBuyMp");
	UserDefault::getInstance()->setIntegerForKey(keyStr_setRoleBuyMp.c_str(), sRoleAutoBuyMp);
	//general set
	std::string keyStr_setGeneralUseHp = playerName;
	keyStr_setGeneralUseHp.append("setCheckBoxGeneralHp");
	UserDefault::getInstance()->setIntegerForKey(keyStr_setGeneralUseHp.c_str(), sGeneralAutoUseHp);

	std::string keyStr_setGeneralUseMp = playerName;
	keyStr_setGeneralUseMp.append("setCheckBoxGeneralMp");
	UserDefault::getInstance()->setIntegerForKey(keyStr_setGeneralUseMp.c_str(), sGeneralAutoUseMp);

	std::string keyStr_setGeneralBuyHp = playerName;
	keyStr_setGeneralBuyHp.append("setCheckBoxGeneralBuyHp");
	UserDefault::getInstance()->setIntegerForKey(keyStr_setGeneralBuyHp.c_str(), sGeneralAutoBuyHp);

	std::string keyStr_setGeneralBuyMp = playerName;
	keyStr_setGeneralBuyMp.append("setCheckBoxGeneralBuyMp");
	UserDefault::getInstance()->setIntegerForKey(keyStr_setGeneralBuyMp.c_str(), sGeneralAutoBuyMp);

	std::string keyStr_setRepair = playerName;
	keyStr_setRepair.append("setCheckBoxRepairEquip");
	UserDefault::getInstance()->setIntegerForKey(keyStr_setRepair.c_str(), sRepairEquipment);
	
	////
	std::string keyStr_sliderRoleHp = playerName;
	keyStr_sliderRoleHp.append("setSliderRoleHp");
	UserDefault::getInstance()->setIntegerForKey(keyStr_sliderRoleHp.c_str(), sRolelimitHp);

	std::string keyStr_sliderRoleMp = playerName;
	keyStr_sliderRoleMp.append("setSliderRoleMp");
	UserDefault::getInstance()->setIntegerForKey(keyStr_sliderRoleMp.c_str(), sRolelimitMp);

	std::string keyStr_sliderGeneralHp = playerName;
	keyStr_sliderGeneralHp.append("setSliderGeneralHp");
	UserDefault::getInstance()->setIntegerForKey(keyStr_sliderGeneralHp.c_str(),sGenerallimitHp);

	std::string keyStr_sliderGeneralMp = playerName;
	keyStr_sliderGeneralMp.append("setSliderGeneralMp");
	UserDefault::getInstance()->setIntegerForKey(keyStr_sliderGeneralMp.c_str(),sGenerallimitMp);
	
	//goods
	for (int i=1;i<21;i++)
	{
		std::string keyStr_setGoodPickAndSell = playerName;
		keyStr_setGoodPickAndSell.append("CheckBox_honggou_");
		char checkIndex[5];
		sprintf(checkIndex,"%d",i);
		keyStr_setGoodPickAndSell.append(checkIndex);

		if (strcmp(sgoodsCheckName.c_str(),keyStr_setGoodPickAndSell.c_str())==0)
		{
			UserDefault::getInstance()->setIntegerForKey(keyStr_setGoodPickAndSell.c_str(),sGoodsPickAndSell);
		}
	}
	
	//��� ý�ɫ �佫ʹ��ҩƷ�id
	std::string keyStr_roleUseDrugHpName = playerName;
	keyStr_roleUseDrugHpName.append("Button_retore_1");
	UserDefault::getInstance()->setStringForKey(keyStr_roleUseDrugHpName.c_str(),sRoleUseDrugHpId);

	std::string keyStr_roleUseDrugMpName = playerName;
	keyStr_roleUseDrugMpName.append("Button_retore_3");
	UserDefault::getInstance()->setStringForKey(keyStr_roleUseDrugMpName.c_str(),sRoleUseDrugMpId);

	std::string keyStr_generalUseDrugHpName = playerName;
	keyStr_generalUseDrugHpName.append("Button_retore_2");
	UserDefault::getInstance()->setStringForKey(keyStr_generalUseDrugHpName.c_str(),sGeneralUseDrugHpId);

	std::string keyStr_generalUseDrugMpName = playerName;
	keyStr_generalUseDrugMpName.append("Button_retore_4");
	UserDefault::getInstance()->setStringForKey(keyStr_generalUseDrugMpName.c_str(),sGeneralUseDrugMpId);
	*/

// 	for (int i=1;i<5;i++)
// 	{
// 		std::string keyStr_setDefault = playerName;
// 		keyStr_setDefault.append("Button_retore_");
// 		char checkIndex[5];
// 		sprintf(checkIndex,"%d",i);
// 		keyStr_setDefault.append(checkIndex);
// 
// 		if (strcmp(sdrugIndex.c_str(),keyStr_setDefault.c_str())==0)
// 		{
// 			UserDefault::getInstance()->setStringForKey(keyStr_setDefault.c_str(),sdrugId);
// 		}
// 	}

}

void MyPlayerAIConfig::loadConfig(std::string playerName)
{
	// please supply the default value !!!

	for(int i = 0; i < ROBOT_SETUP_SKILL_MAX; i++)
	{
		char indexStr[3];
		sprintf(indexStr, "%d", i);
		sRobotSkillsId[i] = UserDefault::getInstance()->getStringForKey(
			(playerName + ROBOT_SETUP_SKILL_ID_PREFIX + indexStr).c_str(), "");
	}
	std::string keyStr_moveRange = playerName;
	keyStr_moveRange.append(ROBOT_SETUP_MOVE_RANGE);
	sMoveRange = UserDefault::getInstance()->getIntegerForKey(keyStr_moveRange.c_str(), 100);

	//protect panel
	//role set
	std::string keyStr_setRoleUseHp = playerName;
	keyStr_setRoleUseHp.append("setCheckBoxuseHp");
	sRoleAutoUseHp = UserDefault::getInstance()->getIntegerForKey(keyStr_setRoleUseHp.c_str(), 1);

	std::string keyStr_setRoleUseMp = playerName;
	keyStr_setRoleUseMp.append("setCheckBoxuseMp");
	sRoleAutoUseMp = UserDefault::getInstance()->getIntegerForKey(keyStr_setRoleUseMp.c_str(), 1);

	std::string keyStr_roleBuyHp = playerName;
	keyStr_roleBuyHp.append("setCheckBoxBuyHp");
	sRoleAutoBuyHp = UserDefault::getInstance()->getIntegerForKey(keyStr_roleBuyHp.c_str(), 1);

	std::string keyStr_roleBuyMp = playerName;
	keyStr_roleBuyMp.append("setCheckBoxBuyMp");
	sRoleAutoBuyMp = UserDefault::getInstance()->getIntegerForKey(keyStr_roleBuyMp.c_str(), 1);
	//general set
	std::string keyStr_setGeneralUseHp = playerName;
	keyStr_setGeneralUseHp.append("setCheckBoxGeneralHp");
	sGeneralAutoUseHp = UserDefault::getInstance()->getIntegerForKey(keyStr_setGeneralUseHp.c_str(), 1);

	std::string keyStr_setGeneralUseMp = playerName;
	keyStr_setGeneralUseMp.append("setCheckBoxGeneralMp");
	sGeneralAutoUseMp = UserDefault::getInstance()->getIntegerForKey(keyStr_setGeneralUseMp.c_str(), 1);

	std::string keyStr_GeneralBuyHp = playerName;
	keyStr_GeneralBuyHp.append("setCheckBoxGeneralBuyHp");
	sGeneralAutoBuyHp = UserDefault::getInstance()->getIntegerForKey(keyStr_GeneralBuyHp.c_str(), 1);

	std::string keyStr_GeneralBuyMp = playerName;
	keyStr_GeneralBuyMp.append("setCheckBoxGeneralBuyMp");
	sGeneralAutoBuyMp = UserDefault::getInstance()->getIntegerForKey(keyStr_GeneralBuyMp.c_str(), 1);

	std::string keyStr_RepairEquip = playerName;
	keyStr_RepairEquip.append("setCheckBoxRepairEquip");
	sRepairEquipment = UserDefault::getInstance()->getIntegerForKey(keyStr_RepairEquip.c_str(), 0);
	/*role and general slider*/
	std::string keyStr_sliderRoleHp = playerName;
	keyStr_sliderRoleHp.append("setSliderRoleHp");
	sRolelimitHp = UserDefault::getInstance()->getIntegerForKey(keyStr_sliderRoleHp.c_str(), 60);

	std::string keyStr_sliderRoleMp = playerName;
	keyStr_sliderRoleMp.append("setSliderRoleMp");
	sRolelimitMp = UserDefault::getInstance()->getIntegerForKey(keyStr_sliderRoleMp.c_str(), 60);

	std::string keyStr_sliderGeneralHp = playerName;
	keyStr_sliderGeneralHp.append("setSliderGeneralHp");
	sGenerallimitHp = UserDefault::getInstance()->getIntegerForKey(keyStr_sliderGeneralHp.c_str(),60);

	std::string keyStr_sliderGeneralMp = playerName;
	keyStr_sliderGeneralMp.append("setSliderGeneralMp");
	sGenerallimitMp = UserDefault::getInstance()->getIntegerForKey(keyStr_sliderGeneralMp.c_str(),60);

	//role use drug name
	std::string keyStr_roleUseDrugHpName = playerName;
	keyStr_roleUseDrugHpName.append("Button_retore_1");
	sRoleUseDrugHpId = UserDefault::getInstance()->getStringForKey(keyStr_roleUseDrugHpName.c_str(),"liqidan");

	std::string keyStr_roleUseDrugMpName = playerName;
	keyStr_roleUseDrugMpName.append("Button_retore_3");
	sRoleUseDrugMpId = UserDefault::getInstance()->getStringForKey(keyStr_roleUseDrugMpName.c_str(),"liqisan");

	std::string keyStr_generalUseDrugHpName = playerName;
	keyStr_generalUseDrugHpName.append("Button_retore_2");
	sGeneralUseDrugHpId = UserDefault::getInstance()->getStringForKey(keyStr_generalUseDrugHpName.c_str(),"wuyuecao");

	std::string keyStr_generalUseDrugMpName = playerName;
	keyStr_generalUseDrugMpName.append("Button_retore_4");
	sGeneralUseDrugMpId = UserDefault::getInstance()->getStringForKey(keyStr_generalUseDrugMpName.c_str(),"wuyueshui");

	// ļ���ʰȡ��������Ʒ�Ĺһ����
	for (int i=1;i<ROBOT_SETUP_PICKANDSELL_MAX;i++)
	{
		std::string keyStr_setGoodPickAndSell = playerName;
		keyStr_setGoodPickAndSell.append("CheckBox_honggou_");
		char checkIndex[5];
		sprintf(checkIndex,"%d",i);
		keyStr_setGoodPickAndSell.append(checkIndex);

		if (i <=4)
		{
			sGoodsPickAndSell[i] = UserDefault::getInstance()->getIntegerForKey(keyStr_setGoodPickAndSell.c_str(),1);
		}else
		{
			sGoodsPickAndSell[i] =UserDefault::getInstance()->getIntegerForKey(keyStr_setGoodPickAndSell.c_str(),0);
		}
	}
}

void MyPlayerAIConfig::setRoleLimitHp( int isSelect )
{
	sRolelimitHp = isSelect;
	MyPlayerAIConfig::pushConfig("setSliderRoleHp",ROBOT_SAVE_TYPE_INT,"",sRolelimitHp);
	//saveConfig("setSliderRoleHp",sRolelimitHp);
}

int MyPlayerAIConfig::getRoleLimitHp()
{
	return sRolelimitHp;
}

void MyPlayerAIConfig::setRoleLimitMp( int isSelect )
{
	sRolelimitMp =isSelect;
	MyPlayerAIConfig::pushConfig("setSliderRoleMp",ROBOT_SAVE_TYPE_INT,"",sRolelimitMp);
	//saveConfig("setSliderRoleMp",sRolelimitMp);
}

int MyPlayerAIConfig::getRoleLimitMp()
{
	return sRolelimitMp;
}

void MyPlayerAIConfig::setGeneralLimitHp( int isSelect )
{
	sGenerallimitHp =isSelect;
	MyPlayerAIConfig::pushConfig("setSliderGeneralHp",ROBOT_SAVE_TYPE_INT,"",sGenerallimitHp);
	//saveConfig("setSliderGeneralHp",sGenerallimitHp);
}

int MyPlayerAIConfig::getGeneralLimitHp()
{
	return sGenerallimitHp;
}

void MyPlayerAIConfig::setGeneralLimitMp( int isSelect )
{
	sGenerallimitMp = isSelect;
	MyPlayerAIConfig::pushConfig("setSliderGeneralMp",ROBOT_SAVE_TYPE_INT,"",sGenerallimitMp);
	//saveConfig("setSliderGeneralMp",sGenerallimitMp);
}

int MyPlayerAIConfig::getGeneralLimitMp()
{
	return sGenerallimitMp;
}

void MyPlayerAIConfig::setAutomaticSkill( int isSelect )
{
	sAutomaticSkill = isSelect;

	if (0 == isSelect)
	{
		auto pMyPlayer = GameView::getInstance()->myplayer;
		// ùر ��Զ�ս��� ж���
		pMyPlayer->removeAutoCombatFlag();
	}
}

int MyPlayerAIConfig::getAutomaticSkill()
{
	return sAutomaticSkill;
}

void MyPlayerAIConfig::setRoleUseDrugHpName(std::string drugId)
{
	sRoleUseDrugHpId = drugId;
	MyPlayerAIConfig::pushConfig("Button_retore_1",ROBOT_SAVE_TYPE_STRING,sRoleUseDrugHpId,0);
	//saveConfig("Button_retore_1",sRoleUseDrugHpId);
}

std::string MyPlayerAIConfig::getRoleUseDrugHpName()
{
	return sRoleUseDrugHpId;
}

void MyPlayerAIConfig::setRoleUseDrugMpName(std::string drugId)
{
	sRoleUseDrugMpId =drugId;
	MyPlayerAIConfig::pushConfig("Button_retore_3",ROBOT_SAVE_TYPE_STRING,sRoleUseDrugMpId,0);
	//saveConfig("Button_retore_3",sRoleUseDrugMpId);
}

std::string MyPlayerAIConfig::getRoleUseDrugMpName()
{
	return sRoleUseDrugMpId;
}

void MyPlayerAIConfig::setGeneralUseDrugHpName(std::string drugId)
{
	sGeneralUseDrugHpId = drugId;
	MyPlayerAIConfig::pushConfig("Button_retore_2",ROBOT_SAVE_TYPE_STRING,sGeneralUseDrugHpId,0);
	//saveConfig("Button_retore_2",sGeneralUseDrugHpId);
}

std::string MyPlayerAIConfig::getGeneralUseDrugHpName()
{
	return sGeneralUseDrugHpId;
}

void MyPlayerAIConfig::setGeneralUseDrugMpName(std::string drugId)
{
	sGeneralUseDrugMpId = drugId;
	MyPlayerAIConfig::pushConfig("Button_retore_4",ROBOT_SAVE_TYPE_STRING,sGeneralUseDrugMpId,0);
	//saveConfig("Button_retore_4",sGeneralUseDrugMpId);
}

std::string MyPlayerAIConfig::getGeneralUseDrugMpName()
{
	return sGeneralUseDrugMpId;
}

int MyPlayerAIConfig::getEquipMentQualityByRobotAutoSell( int checkBoxIndexOfEquip )
{
	int equipQuality = 1;
	switch(checkBoxIndexOfEquip)
	{
	case 6:
		{
			equipQuality = 2;
		}break;
	case 7:
		{
			equipQuality = 3;
		}break;
	case 8:
		{
			equipQuality = 1;
		}break;
	}
	return equipQuality;
}

int MyPlayerAIConfig::getGoodsClazzByRobotAutoSell( int checkBoxIndexOfGoods )
{
	/*
	int equipQuality = 1;
	switch(checkBoxIndexOfGoods)
	{
	case 15:
		{
			equipQuality = 5;//
		}break;
	case 20:
		{
			equipQuality = 4;
		}
	}
	return equipQuality;
	*/
	return 1;
}

int MyPlayerAIConfig::getDrugsObjectByRobotAutoSell( int checkBoxIndexOfDrugs )
{
	/*
	int equipQuality = 1;
	switch(checkBoxIndexOfDrugs)
	{
	case 14:
		{
			equipQuality = 5;//
		}break;
	case 19:
		{
			equipQuality = 4;
		}
	}
	return equipQuality;
	*/
	return 1;
}

void MyPlayerAIConfig::setAcceptTeam( bool isSelect )
{
	sAcceptTeam = isSelect;
	//saveConfig("setCheckBoxAcceptteam",sAcceptTeam);
}

bool MyPlayerAIConfig::getAcceptTeam()
{
	return sAcceptTeam;
}

void MyPlayerAIConfig::setRefuseTeam( bool isSelect )
{
	sRefuseTeam = isSelect;
	//saveConfig("setCheckBoxRefuseTeam",sRefuseTeam);
}

bool MyPlayerAIConfig::getRefuseTeam()
{
	return sRefuseTeam;
}

void MyPlayerAIConfig::reSetRobotUseDrug( int roleLevel )
{
	int mapCurLevel = 0;
	std::string goodsIcon = "";
	std::string goodsid = "";
	std::string goodsName = "";
	auto robotui_ = (RobotMainUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagRobotUI);
	for (int i=1;i<5;i++)
	{
		std::map<int,MapRobotDrug *> * robotdrugMap = RobotDrugConfigData::s_RobotDrugConfig[i];
		for (int mapIndex =0;mapIndex<robotdrugMap->size();mapIndex++)
		{
			if (mapIndex ==0 )
			{
				mapCurLevel = robotdrugMap->at(1)->get_level();
			}else
			{
				mapCurLevel = robotdrugMap->at(mapIndex*15)->get_level();
			}

			if (mapCurLevel == roleLevel)
			{
				mapCurLevel = robotdrugMap->at(roleLevel)->get_level();
				goodsIcon = robotdrugMap->at(roleLevel)->get_icon();
				goodsid = robotdrugMap->at(roleLevel)->get_id();
				goodsName = robotdrugMap->at(roleLevel)->get_name();

				if (robotui_ != NULL)
				{
					std::string goodsIcon_ = "res_ui/props_icon/";
					goodsIcon_.append(goodsIcon);
					goodsIcon_.append(".png");
					switch(i)
					{
					case 1:
						{
							robotui_->imageRoleHp->loadTexture(goodsIcon_.c_str());
							robotui_->labelRolebuyHpId_->setString(goodsName.c_str());
							robotui_->label_roleHp->setString(goodsName.c_str());
						}break;
					case 2:
						{
							robotui_->imageRoleMp->loadTexture(goodsIcon_.c_str());
							robotui_->labelRolebuyMpId_->setString(goodsName.c_str());
							robotui_->label_roleMp->setString(goodsName.c_str());
						}break;
					case 3:
						{
							robotui_->imageGeneralHp->loadTexture(goodsIcon_.c_str());
							robotui_->labelGeneralbuyHpId_->setString(goodsName.c_str());
							robotui_->label_GeneralHp->setString(goodsName.c_str());
						}break;
					case 4:
						{
							robotui_->imageGeneralMp->loadTexture(goodsIcon_.c_str());
							robotui_->labelGeneralbuyMpId_->setString(goodsName.c_str());
							robotui_->label_GeneralMp->setString(goodsName.c_str());
						}break;
					}
				}
				switch(i)
				{
				case 1:
					{
						setRoleUseDrugHpName(goodsid.c_str());
					}break;
				case 2:
					{
						setRoleUseDrugMpName(goodsid.c_str());
					}break;
				case 3:
					{
						setGeneralUseDrugHpName(goodsid.c_str());
					}break;
				case 4:
					{
						setGeneralUseDrugMpName(goodsid.c_str());
					}break;
				}
			}
		}
	}

	setRoleUseHp(1);
	setRoleUseMp(1);
	setRoleBuyHp(1);
	setRoleBuyMp(1);
	setGeneralUseHp(1);
	setGeneralUseMp(1);
	setGeneralBuyHp(1);
	setGeneralBuyMp(1);

	if (robotui_ != NULL)
	{
		//set checkBox is select state
		robotui_->checkBoxSetUseHp->setSelected(1);
		robotui_->checkBoxSetUseMp->setSelected(1);
		robotui_->checkBoxbuyHp->setSelected(1);
		robotui_->checkBoxbuyMp->setSelected(1);

		robotui_->checkBoxGeneralHp->setSelected(1);
		robotui_->checkBoxGeneralMp->setSelected(1);
		robotui_->checkBoxGeneralBuyHp->setSelected(1);
		robotui_->checkBoxGeneralBuyMp->setSelected(1);
	}
}

void MyPlayerAIConfig::pushConfig(std::string key, int type,std::string value1, int value2)
{
	bool has_ = false;
	for (int i= 0;i<MyPlayerAIConfig::configInfoVector.size();i++)
	{
		std::string temp_key = MyPlayerAIConfig::configInfoVector.at(i)->key;
		if (strcmp(temp_key.c_str(),key.c_str()) == 0)
		{
			has_ = true;

			if (type == ROBOT_SAVE_TYPE_STRING)
			{
				MyPlayerAIConfig::configInfoVector.at(i)->strValue = value1;
			}else
			{
				MyPlayerAIConfig::configInfoVector.at(i)->intValue = value2;
			}
		}
	}

	if (!has_)
	{
		auto saveInfo = new saveConfigInfo();
		saveInfo->key = key;
		saveInfo->type = type;
		saveInfo->strValue = value1;
		saveInfo->intValue = value2;
		
		MyPlayerAIConfig::configInfoVector.push_back(saveInfo);
	}

	MyPlayerAIConfig::setRobotSaveState(true);
}

void MyPlayerAIConfig::setRobotSaveState( bool value_ )
{
	m_isSaveState = value_;
}

bool MyPlayerAIConfig::getRobotSaveState()
{
	return m_isSaveState;
}

void MyPlayerAIConfig::savePushConfig()
{
	if (MyPlayerAIConfig::getRobotSaveState())
	{
		for (int i = 0;i<MyPlayerAIConfig::configInfoVector.size();i++)
		{
			auto saveInfo = MyPlayerAIConfig::configInfoVector.at(i);

			std::string key_ = saveInfo->key;
			int type_ = saveInfo->type;
			std::string strValue_ = saveInfo->strValue;
			int intValue_ = saveInfo->intValue;

			//type == 1 string
			//type ==2 int  
			if (type_ == ROBOT_SAVE_TYPE_STRING)
			{
				saveConfig(key_,strValue_.c_str());
			}else
			{
				saveConfig(key_,intValue_);
			}
		}
		std::vector<saveConfigInfo *>::iterator iter;
		for (iter=MyPlayerAIConfig::configInfoVector.begin();iter!=MyPlayerAIConfig::configInfoVector.end();iter++)
		{
			delete *iter;
		}
		MyPlayerAIConfig::configInfoVector.clear();

		MyPlayerAIConfig::setRobotSaveState(false);
	}
}

std::vector<MyPlayerAIConfig::saveConfigInfo *> MyPlayerAIConfig::configInfoVector;

bool MyPlayerAIConfig::m_isSaveState = false;

bool MyPlayerAIConfig::sRefuseTeam = false;

bool MyPlayerAIConfig::sAcceptTeam = false;

std::string MyPlayerAIConfig::sGeneralUseDrugMpId = "";

std::string MyPlayerAIConfig::sGeneralUseDrugHpId= "";

std::string MyPlayerAIConfig::sRoleUseDrugMpId= "";

std::string MyPlayerAIConfig::sRoleUseDrugHpId= "";

//std::string MyPlayerAIConfig::sdrugId="";

//std::string MyPlayerAIConfig::sdrugIndex="";

std::string MyPlayerAIConfig::sgoodsCheckName="";










