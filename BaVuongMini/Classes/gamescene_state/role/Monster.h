#ifndef _LEGEND_GAMEACTOR_MONSTER_H_
#define _LEGEND_GAMEACTOR_MONSTER_H_

#include "BaseFighter.h"
#include "cocos2d.h"

#define MONSTER_ANI_STAND 0			
#define MONSTER_ANI_RUN 1			
#define MONSTER_ANI_ATTACK 2	
#define MONSTER_ANI_DEATH 3	
#define MONSTER_ANI_MAX 4

#ifndef ROLE_MONSTER_DEBUG_DRAW
#define ROLE_MONSTER_DEBUG_DRAW 0
#endif

class CCLegendAnimation;
class GameActorAnimation;

/**
 * 怪物
 * @author zhaogang
 * @version 0.1.0
 */
class Monster : public BaseFighter
{
friend class MonsterStand;
friend class MonsterRun;
friend class MonsterAttack;
friend class MonsterDeath;

public:
	Monster();
	virtual ~Monster();

	virtual void update(float dt);

	virtual void onEnter();
	virtual void onExit();

	virtual Vec2 getSortPoint();

	//an instance of the state machine class
	StateMachine<Monster>*  m_pStateMachine;
	StateMachine<Monster>*  GetFSM()const{return m_pStateMachine;}

	virtual bool init(const char* actorName);

	virtual Rect getVisibleRect();

	virtual void changeAction(int action);
	virtual bool isAction(int action);

	virtual bool isDead();
	virtual bool isStanding();
	virtual bool isAttacking();

	/** 各种战斗状态变化的回调函数 */
	/** 受到伤害 */
	virtual void onDamaged(BaseFighter* source, int damageNum, bool bDoubleAttacked, CSkillResult* skillResult = NULL);

	// override
	virtual void onAttack(const char* skillId, const char* skillProcessorId, long long targetId, bool bDoubleAttack, int skillType = -1);

	// 出生事件
	virtual void onBorn();

	virtual void showActorName(bool bShow);
	void modifyActorNameColor(Color3B color);

#if ROLE_MONSTER_DEBUG_DRAW
    virtual void draw();
#endif // CC_Label_DEBUG_DRAW

	void setBornPosition(Vec2 pos);
	Vec2 getBornPosition();

	void setClazz(int clazz);
	int getClazz();

protected:
	virtual void updateWander();
	virtual void updateWorldPosition();

protected:
	std::string m_resPath;

	// special death animation
	CCLegendAnimation* m_DeathAnim;
	ParticleSystem* m_deathEffect;

	Vec2 m_bornPosition;

	// CMonsterBaseInfo::monster_clazz
	int m_clazz;

	//add by yangjun 2014.10.13
	/********用于判定武将死亡状态，防止同一个武将死亡两次*********/
private:
	int m_nDeadStatus; //0:无  1:第一次死亡  其他:非第一次死亡
public:
	void addDeadStatus();
	bool isFirstDead();
};

#endif