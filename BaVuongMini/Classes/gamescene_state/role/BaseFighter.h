#ifndef _LEGEND_GAMEACTOR_BASEFIGHTER_H_
#define _LEGEND_GAMEACTOR_BASEFIGHTER_H_

#include "GameActor.h"
#include "cocos2d.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../messageclient/GameProtobuf.h"

USING_NS_THREEKINGDOMS_PROTOCOL;

// 是否开启角色脚下的阴影，影响性能
#define BASEFIGHTER_SHADOW_ENABLED 1

#if BASEFIGHTER_SHADOW_ENABLED
// 组件定义时的顺序，同时代表了ZORDER顺序
#define ANI_COMPONENT_REFLECTION 0
#define ANI_COMPONENT_SHADOW 1
#define ANI_COMPONENT_BODY 2
#define ANI_COMPONENT_MAX 3
#else
#define ANI_COMPONENT_REFLECTION 0
#define ANI_COMPONENT_BODY 1
#define ANI_COMPONENT_MAX 2
#endif

#define BASEFIGHTER_ROLE_HEIGHT 110
#define BASEFIGHTER_BASIC_MOVESPEED 200.f

class ActorCommand;
class GameFightSkill;
class SkillProcessor;
class ExStatus;
class GameActorAnimation;
class SkillSeed;
class CSkillResult;
class ActionContext;

/** 角色动作 */
#define ACT_STAND 0 // 站立
#define ACT_RUN   1 // 跑|走
#define ACT_ATTACK 2 // 攻击
#define ACT_DIE 3 // 死亡
#define ACT_PICK 4 // 采集
#define ACT_BE_KNOCKBACK 5 // 被击退
#define ACT_BE_PULL 6 // 被拉过去
#define ACT_CHARGE 7 // 冲锋
#define ACT_JUMP 8 // 跳跃, 可向前或向后跳跃，用于远程职业的后撤，或者近战职业的向前跳跃
#define ACT_DISAPPEAR 9 // disappear, used in push 1107 ( the actor is out of view )
#define ACT_MAX_NUM 10   // total number

/**
 * 可战斗actor
 * @author zhaogang
 * @version 0.1.0
 */
class BaseFighter : public GameActor
{
public:
	enum SubLevelTagDefine {
		kTagExStatusEffect = 100,
		kTagMosouEffect = 200,   // 无双效果
	};

public:
	BaseFighter();
	virtual ~BaseFighter();

	inline std::vector<Vec2*>* getPaths() { return m_pPaths; };
	void searchPath(Vec2 target);
	void clearPath();

	// 基础行为
	virtual void changeAction(int action) {} ;   // action: ACT_STAND, ACT_RUN, ACT_ATTACK etc 
	virtual bool isAction(int action) { return false; };
	ActionContext* getActionContext(int action);

	virtual void running(float dt);
	virtual void checkAttack();
	virtual void checkPick();

	virtual bool isDead();
	virtual bool isAttacking() { return false; };
	virtual bool isStanding() { return false; };
	virtual bool isMoving() { return false; };

	//
	// 动画相关
	//
	// 切换动画( 跑，站立，攻击等动作，一个动作对应一个动画文件）
	virtual void changeAnimation(int animName, int dir, bool bLoop, bool bPlay);
	// 设置动画( 跑，站立，攻击等动作，一个动作对应一个动画文件）
	virtual void setAnimName(int animName);
	virtual int getAnimName();
	// 设置动画中action的索引
	void setAnimDir(int dir);
	inline int getAnimDir() { return m_animDirection; };
	// 获取动画实例
	inline GameActorAnimation* getAnim() { return m_pAnim; };

	/**
	* now, one BaseFighter only have one command at the same time
	*/
	virtual void setCommand(ActorCommand* command);
	virtual ActorCommand* getCommand();
	virtual void setNextCommand(ActorCommand* command, bool bFinishedPreviousImmediately = false, bool bWaitingAction = false);
	virtual ActorCommand* getNextCommand();
	virtual void stopAllCommand();
	virtual void stopCurrentCommand();

	/** generate skill seed
	* SkillSeed为玩家使用技能传给SkillManage的数据
	* 其中数据为技能流程和发送给服务器端所需数据
	*/
	SkillSeed* makeSkillSeed(const char* skillId, const char* skillProcessorId, BaseFighter* target, int x = 0, int y = 0);
	/**
	* 技能的起手动作
	*/
	void onSkillBegan(const char* skillId, const char* skillProcessorId, BaseFighter* target, short x, short y);
	/**
	* 释放技能，并将玩家使用的技能传给SkillManager
	* @param skillId
	* @param target
	*/
	void onSkillReleased(const char* skillId, const char* skillProcessorId, BaseFighter* target, short x, short y);
	/**
	* 应用技能效果，根据服务器返回的实际技能信息
	* @param skillId
	* @param result
	*/
	void onSkillApplied(const char* skillId, CSkillResult* result);
	virtual GameFightSkill* getGameFightSkill(const char* skillId, const char* skillProcessorId);
	/** 目前主要用于更新技能CD */
	virtual void updateGameFightSkill() {};

	virtual ActiveRole* getActiveRole() { return mActiveRole; };

	virtual void clear();

	// 是否开启倒影
	virtual void setReflection(bool enabled);

	// 角色身上的各种效果
	void addEffect(Node* effectNode, bool bGround, int offset, bool bLoop = false);
	/** 在头顶增加聊天泡泡 */
	void addChatBubble(const char* text, float duration, int offsetY = 124);

	// 脚下的圆形阴影片
	void addShadow();

	/** 
	  * according to the main name lable, 
	  * set other info's position, for example, country icon, family name etc. 
	  **/
	virtual void synchronizeHeadLabelPosition();
	virtual Vec2 getHeadLabelPosition(int labelType);

	/** buff and debuff */
	void removeAllExStatus();
	std::vector<ExStatus*>& getExStatusVector() { return m_ExStatusVector; };
	bool hasExStatus(int type);   // @param type is from ExStatusType

	// 各种关系判断
	/**
	 * 记录玩家自己的ID，用于关系判断
	 */
	static void setMyPlayerId(long long myId);
	static long long getMyPlayerId();
	inline bool isFriend(BaseFighter* other) { return false; };
	inline bool isPet(BaseFighter* other) { return false; };
	// 是否玩家自己
	virtual bool isMyPlayer();
	/**
	 * 是否玩家自己的团队，包括玩家自己、武将，宠物等
     * 返回值，表示其在队伍中的位置
     * 比如，1是主角，2表示武将1，3表示武将2，等
	 */
	virtual bool isMyPlayerGroup();
	/**
	 * 获取actor在玩家队伍中的位置
     * 返回值，表示其在队伍中的位置
     * 比如，0是主角，1表示武将1，2表示武将2，等
	 */
	virtual int getPosInGroup();
	
	/** 各种战斗状态变化的回调函数 */
	/** 受到伤害 */
	virtual void onDamaged(BaseFighter* source, int damageNum, bool bDoubleAttacked, CSkillResult* skillResult = NULL);
	/** 被治疗 */
	virtual void onHealed(BaseFighter* source, int number);
	/** 回蓝 */
	virtual void onMPAdded(BaseFighter* source, int number);
	/**
	* 施展攻击
	* @param skillId
	* @param skillType, 技能类型, -1表示不关心技能类型
	*/
	virtual void onAttack(const char* skillId, const char* skillProcessorId, long long targetId, bool bDoubleAttack, int skillType = -1);
	/** 获得道具 */
	virtual void onGotItem(std::string itemIcon);

	virtual void updateCoverStatus();

	// display the foot red circle to indicate that this actor is dander ( attacking us )
	void addDangerCircle();
	void removeDangerCircle();

protected:
	/**
	 * 逻辑驱动
	 */
	virtual void drive();

	std::vector<Vec2*> *m_pPaths;

	int m_animName;
	GameActorAnimation* m_pAnim;

	std::map<std::string, GameFightSkill*> m_FightSkillMap;   // 已学技能列表

	std::vector<ExStatus*> m_ExStatusVector;// 状态记录

	ActiveRole* mActiveRole;

	Node* m_pFootEffectContainer;   // 角色脚下的Node节点，比如某些技能特效
	Node* m_pHeadEffectContainer;   // 角色头顶的Node节点，比如伤害数字

private:
	// all kinds of OwnedStates context ( parameters )
	// for example, MyPlayerRun, MyPlayerAttack, MonsterRun, MonsterAttack, etc.
	ActionContext* m_pAllActionContextes[ACT_MAX_NUM];

private:
	int m_animDirection;

	ActorCommand* mCurrentCommand;   // 正在执行的指令
	ActorCommand* mNextCommand;      // 将要执行的指令
};

#endif