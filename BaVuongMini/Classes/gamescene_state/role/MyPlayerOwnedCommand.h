#ifndef _MYPLAYER_OWNED_COMMAND_H_
#define _MYPLAYER_OWNED_COMMAND_H_

#include "cocos2d.h"
#include "ActorCommand.h"
#include "BaseFighterOwnedCommand.h"

USING_NS_CC;

class BaseFighter;
class MyPlayer;

/**
 * 攻击指令，需指定攻击目标和攻击方式
 * @author zhaogang
 * @version 0.1.0
 */
class MyPlayerCommandAttack : public BaseFighterCommandAttack
{
friend class MyPlayerAttack;

public:
	MyPlayerCommandAttack();
	virtual ~MyPlayerCommandAttack();

	virtual void Enter(GameActor* mainActor);
	virtual void Execute(GameActor* mainActor);
	virtual void Exit(GameActor* mainActor);

	virtual bool isSame(ActorCommand* otherCmd);

	virtual ActorCommand* clone();

	/** 
	 * 完成条件：攻击一个敌人，直到其死亡
	 * 杀死敌人条件( finishedConditionKill ) 的优先级，高于最大攻击次数条件( attackMaxCount )
	*/
	bool finishedConditionKill;
	long startTime;   // the time when the command begin

public:
	static std::string s_latestSkillId;
	static long s_latestSkillStartTime;

protected:
	bool checkConditionKill(BaseFighter* mainActor);
};

/**
 * 玩家自己的移动指令，需指定目的地等参数
 * @author zhaogang
 * @version 0.1.0
 */
class MyPlayerCommandMove : public BaseFighterCommandMove
{
public:
	enum Type
	{
		method_searchpath = 0,   // 寻路到目的地
		method_direct = 1   // 直接到目的地
	};

	enum OperationMethod
	{
		operation_joystick = 0,   // 摇杆方式
		operation_touchscreen = 1   // 触屏方式
	};

public:
	MyPlayerCommandMove();
	virtual ~MyPlayerCommandMove();

	virtual void Enter(GameActor* mainActor);
	virtual void Execute(GameActor* mainActor);
	virtual void Exit(GameActor* mainActor);

	virtual bool isSame(ActorCommand* otherCmd);

	std::string targetMapId;   // target map, it is ok for "" value
	int playerOperationMethod;   // using joystick or touch screen

	bool autoMove;   // if false, the player use joysitck or touch screen to control the role moving

private:
	void EnterStatusDoing(GameActor* mainActor);

private:
	std::string nextMapId;   // middle phase when cross map path-finding
};


/**
 * 采集指令，需指定采集物Id
 * @author zhaogang
 * @version 0.1.0
 */
class MyPlayerCommandPick : public ActorCommand
{
public:
	MyPlayerCommandPick();
	virtual ~MyPlayerCommandPick();

	virtual void Enter(GameActor* mainActor);
	virtual void Execute(GameActor* mainActor);
	virtual void Exit(GameActor* mainActor);

	virtual bool isSame(ActorCommand* otherCmd);

	void init(int nType);

	// command content
	long long targetId;
	Vec2 targetPos;
	int pickingNumber;   // 采集数量
	int pickingMaxNumber;

	int pickType;			// 1 代表 普通采集物， 2 代表 宝箱
	
	enum
	{
		PICK_TYPE_GENERAL = 1,
		PICK_TYPE_PRESENTBOX = 2,
	};
};

#endif