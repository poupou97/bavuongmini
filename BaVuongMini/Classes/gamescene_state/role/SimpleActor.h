#ifndef _LEGEND_GAMEACTOR_SIMPLEACTOR_H_
#define _LEGEND_GAMEACTOR_SIMPLEACTOR_H_

#include "GameActor.h"
#include "cocos2d.h"

class CCLegendAnimation;

/**
 * 简单actor
 * 基本无AI，主要用于动画播放
 * 比如触摸屏幕后出现的移动目的地动画
 * @author zhaogang
 * @version 0.1.1
 */
class SimpleActor : public GameActor
{
public:
	SimpleActor();
	virtual ~SimpleActor();

	virtual void update(float dt);

	virtual Vec2 getSortPoint();

	void loadAnim(const char* animName, bool bLoop = false,  int actionId = 0);
	void loadSprite(const char* spriteName);

	CCLegendAnimation* getLegendAnim();

private:
	int m_type;
};

#endif