#include "OtherPlayerOwnedStates.h"

#include "OtherPlayer.h"
#include "GameActorAnimation.h"
#include "../../utils/GameUtils.h"
#include "BaseFighterOwnedCommand.h"
#include "../skill/GameFightSkill.h"
#include "../GameSceneState.h"
#include "BaseFighterOwnedStates.h"
#include "ActorUtils.h"

//-----------------------------------------------------------------------Global state
OtherPlayerGlobalState* OtherPlayerGlobalState::Instance()
{
  static OtherPlayerGlobalState instance;

  return &instance;
}

void OtherPlayerGlobalState::Execute(OtherPlayer* self)
{

}

//---------------------------------------

OtherPlayerStand* OtherPlayerStand::Instance()
{
  static OtherPlayerStand instance;

  return &instance;
}

void OtherPlayerStand::Enter(OtherPlayer* self)
{
	// update the display status of the role
	self->updateWaterStatus();
	self->updateCoverStatus();

	self->changeAnimation(ANI_STAND, self->getAnimDir(), true, true);
}

void OtherPlayerStand::Execute(OtherPlayer* self)
{
}

void OtherPlayerStand::Exit(OtherPlayer* self)
{
}


//------------------------------------------------------------------------
OtherPlayerRun* OtherPlayerRun::Instance()
{
  static OtherPlayerRun instance;

  return &instance;
}


void OtherPlayerRun::Enter(OtherPlayer* self)
{  
	Vec2 offset = GameUtils::getDirection(self->getWorldPosition(), *self->getPaths()->at(0));
	self->setMoveDir(Vec2(offset.x, -offset.y));

	int animDirection = self->getAnimDirection(Vec2(0, 0), self->getMoveDir(), DEFAULT_ANGLES_DIRECTION_DATA);
	CCAssert(animDirection != -1, "should not be -1");
	self->setAnimDir(animDirection);

	self->changeAnimation(ANI_RUN, self->getAnimDir(), true, true);
}


void OtherPlayerRun::Execute(OtherPlayer* self)
{
	if(self->getPaths()->size() <= 0)
	{
		//self->GetFSM()->ChangeState(OtherPlayerStand::Instance());
		//CCLOG("other player( %d ) will stop.", self->getRoleId());
	}
	else
	{
		// according by the move direction to set animation's action
		int animDirection = self->getAnimDirection(Vec2(0, 0), self->getMoveDir(), DEFAULT_ANGLES_DIRECTION_DATA);
		//CCLOG("run state, move dir: (%f,%f), anim dir: %f", dir.x, dir.y, animDirection);
		self->setAnimDir(animDirection);
		self->getAnim()->setAction(animDirection);
		self->getAnim()->setPlaySpeed(self->getMoveSpeed()/BASIC_PLAYER_MOVESPEED);
	}
}

void OtherPlayerRun::Exit(OtherPlayer* self)
{
	// restore animation play speed
	self->getAnim()->setPlaySpeed(1.0f);
}


//------------------------------------------------------------------------
OtherPlayerAttack* OtherPlayerAttack::Instance()
{
  static OtherPlayerAttack instance;

  return &instance;
}


void OtherPlayerAttack::Enter(OtherPlayer* self)
{  
	// init skill effect by skill info
	auto cmd = (BaseFighterCommandAttack*)self->getCommand();
	CCAssert(cmd->getType() == ActorCommand::type_attack, "invalid command");

	// record the attack action's animation index
	ActionAttackContext context;
	context.currentActionIndex = self->getGameFightSkill(cmd->skillId.c_str(), cmd->skillProcessorId.c_str())->getSkillBin()->action;
	context.setContext(self);

	self->changeAnimation(context.currentActionIndex, self->getAnimDir(), false, true);
	self->getAnim()->setPlaySpeed(cmd->attackSpeed);

	m_bIsSkillReleased = false;   // skill has not been released
	m_nReleasePhase = self->getGameFightSkill(cmd->skillId.c_str(), cmd->skillProcessorId.c_str())->getSkillBin()->releasePhase;

	auto scene = self->getGameScene();
	auto bf = dynamic_cast<BaseFighter*>(scene->getActor(cmd->targetId));

	Vec2 skillPosition = ActorUtils::getDefaultSkillPosition(self, cmd->skillId.c_str(), cmd->skillProcessorId.c_str());

	self->onSkillBegan(cmd->skillId.c_str(), cmd->skillProcessorId.c_str(), bf, skillPosition.x, skillPosition.y);

	// 为改善技能释放，展示不及时的问题
	// 1. 在收到1202消息后，其他玩家立即调用onSkillReleased()
	// release表示伤害的时间节点
	// 2. 玩家自己在release时，才sendReq1201
	self->onSkillReleased(cmd->skillId.c_str(), cmd->skillProcessorId.c_str(), bf, skillPosition.x, skillPosition.y);
}


void OtherPlayerAttack::Execute(OtherPlayer* self)
{
	auto scene = self->getGameScene();

	auto context = (ActionAttackContext*)self->getActionContext(ACT_ATTACK);
	auto attack = self->getAnim()->getAnim(ANI_COMPONENT_BODY, context->currentActionIndex);

	// 立即结束指令
	auto cmd = (BaseFighterCommandAttack*)self->getCommand();
	if(cmd != NULL && cmd->isFinishImmediately)
	{
		CCAssert(cmd->getType() == ActorCommand::type_attack, "invalid command");

		self->clearPath();
		self->GetFSM()->ChangeState(OtherPlayerStand::Instance());
		cmd->setStatus(ActorCommand::status_finished);
		return;
	}

	if(attack->getPlayPercent() >= (m_nReleasePhase / 100.f) && !m_bIsSkillReleased)
	{
		//BaseFighter* bf = dynamic_cast<BaseFighter*>(scene->getActor(cmd->targetId));
		////bf = (BaseFighter*)self->getLockedActor();
		//self->onSkillReleased(cmd->skillId.c_str(), bf, 0, 0);

		m_bIsSkillReleased = true;
	}

	// attack is stop
	if(!attack->isPlaying())
	{
		if(cmd != NULL)
		{
			cmd->attackCount++;
		}
	}
}

void OtherPlayerAttack::Exit(OtherPlayer* self)
{
	self->getAnim()->setPlaySpeed(1.0f);   // restore to default speed
}

//------------------------------------------------------------------------
OtherPlayerDeath* OtherPlayerDeath::Instance()
{
  static OtherPlayerDeath instance;

  return &instance;
}


void OtherPlayerDeath::Enter(OtherPlayer* self)
{  
	self->changeAnimation(ANI_DEATH, self->getAnimDir(), false, true);
	self->setPKStatus(false);
}

void OtherPlayerDeath::Execute(OtherPlayer* self)
{

}

void OtherPlayerDeath::Exit(OtherPlayer* self)
{

}

//------------------------------------------------------------------------
OtherPlayerKnockBack* OtherPlayerKnockBack::Instance()
{
  static OtherPlayerKnockBack instance;

  return &instance;
}


void OtherPlayerKnockBack::Enter(OtherPlayer* self)
{  
	self->changeAnimation(ANI_STAND, self->getAnimDir(), false, false);

	self->clearPath();
}

void OtherPlayerKnockBack::Execute(OtherPlayer* self)
{

}

void OtherPlayerKnockBack::Exit(OtherPlayer* self)
{

}

//------------------------------------------------------------------------
OtherPlayerCharge* OtherPlayerCharge::Instance()
{
  static OtherPlayerCharge instance;

  return &instance;
}

void OtherPlayerCharge::Enter(OtherPlayer* self)
{
	auto context = (ActionChargeContext*)self->getActionContext(ACT_CHARGE);

	// adjust the direction
	Vec2 targetWorldPosition = self->getGameScene()->convertToGameWorldSpace(context->chargetTargetPosition);
	Vec2 offset = GameUtils::getDirection(self->getWorldPosition(), targetWorldPosition);
	int animDirection = self->getAnimDirection(Vec2(0, 0), Vec2(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);
	//CCAssert(animDirection != -1, "invalid anim dir");
	self->setAnimDir(animDirection);

	self->changeAnimation(ANI_RUN, self->getAnimDir(), true, true);

	// 获取动画播放速度
	self->getAnim()->setPlaySpeed(context->chargeMoveSpeed/BASIC_PLAYER_MOVESPEED);

	//MoveTo
	auto action = Sequence::create(
		MoveTo::create(context->chargeTime, context->chargetTargetPosition),
		NULL);
	self->runAction(action);

	self->clearPath();
}

void OtherPlayerCharge::Execute(OtherPlayer* self)
{
	auto context = (ActionChargeContext*)self->getActionContext(ACT_CHARGE);
	if(context->finished)
		return;

	//if(self->getPosition().equals( context->chargetTargetPosition) )   // using position to decide is not reliable
	if(GameUtils::millisecondNow() - context->startTime >= context->chargeTime * 1000)
	{
		context->finished = true;
	}
}

void OtherPlayerCharge::Exit(OtherPlayer* self)
{
	// restore animation play speed
	self->getAnim()->setPlaySpeed(1.0f);
}

//------------------------------------------------------------------------
OtherPlayerJump* OtherPlayerJump::Instance()
{
  static OtherPlayerJump instance;

  return &instance;
}

void OtherPlayerJump::Enter(OtherPlayer* self)
{
	BaseFighterJump::setJumpAnimId(ANI_STAND);
	BaseFighterJump::Enter(self);   // common handler for the jump action
}

void OtherPlayerJump::Execute(OtherPlayer* self)
{
	BaseFighterJump::Execute(self);   // common handler for the jump action
}

void OtherPlayerJump::Exit(OtherPlayer* self)
{
	BaseFighterJump::Exit(self);   // common handler for the jump action
}