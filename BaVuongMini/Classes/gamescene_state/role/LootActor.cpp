#include "LootActor.h"

#include "../../legend_engine/CCLegendAnimation.h"
#include "../../utils/GameUtils.h"
#include "../../utils/StaticDataManager.h"
#include "../../GameView.h"
#include "../../messageclient/element/CPropInfo.h"
#include "../GameSceneState.h"
#include "MyPlayer.h"
#include "ActorUtils.h"
#include "AppMacros.h"

#define kTagLootIcon 10
#define kTagLootNameBG 11

#define LOOT_ACTOR_DROP_ACTION_TIME_1 0.1f
#define LOOT_ACTOR_DROP_ACTION_TIME_2 0.2f

#define LOOT_ACTOR_GOLD_ID "jinbi1"

enum LootActor_SubState
{
	State_Init = 0,
	State_Wait,
	State_Flying,
};

enum Loot_Type
{
	loot_type_gold = 0,
	loot_type_other,
};

LootActor::LootActor()
: m_state(State_Init)
, m_streak(NULL)
, m_flyingSpeed(600)
, m_lootType(loot_type_other)
, m_flyingScale(1.0f)
, m_pNameNode(NULL)
{
	this->scheduleUpdate();
	setType(GameActor::type_other);
}

LootActor::~LootActor()
{
	CC_SAFE_RELEASE(m_pNameNode);
}

bool LootActor::init(const char* actorName, int number)
{
	// shadow
    auto shadow = Sprite::create("res_ui/shadow.png");
	shadow->setAnchorPoint(Vec2(0.5f, 0.5f));
	shadow->setScale(0.8f);
	shadow->setOpacity(128);
    addChild(shadow, GameActor::ACTOR_SHADOW_ZORDER);

	auto pInfo = StaticPropInfoConfig::s_propInfoData[actorName];

	if(strcmp(actorName, LOOT_ACTOR_GOLD_ID) == 0)
		m_lootType = loot_type_gold;
	else
		m_lootType = loot_type_other;
	m_nLootAmout = number;
	m_lootQulity = pInfo->get_quality();

	// add the loot icon
	std::string animFileName;
	Node* pLootIcon = NULL;
	if(m_lootType == loot_type_gold)
	{
		animFileName = "animation/collection/jinbi03.anm";
		auto pAnim = CCLegendAnimation::create(animFileName);
		pAnim->setPlayLoop(true);
		//pAnim->setScale(0.8f);
		pLootIcon = pAnim;

		m_lootQulity = 1;   // white color
	}
	else
	{
		bool bHasIcon = true;
		if(pInfo != NULL)
		{
			std::string Icon = "res_ui/props_icon/";
			Icon.append(pInfo->get_icon());
			Icon.append(".png");
			//CCLOG("Icon.c_str() = %s\n", Icon.c_str());
		
			auto pSpriteIcon = Sprite::create(Icon.c_str());
			if(pSpriteIcon == NULL)
			{
				bHasIcon = false;
			}
			else
			{
				pSpriteIcon->setAnchorPoint(Vec2(0.5f, 0.5f));
				pLootIcon = pSpriteIcon;
			}
		}
		else
		{
			bHasIcon = false;
		}

		// no correct icon, so using the default one ( "present box" )
		if(!bHasIcon)
		{
			//animFileName = "animation/collection/baoxiang1.anm";
			//CCLegendAnimation* pAnim = CCLegendAnimation::create(animFileName);
			//pAnim->setScale(0.8f);
			//pAnim->setPlayLoop(true);
			//pLootIcon = pAnim;

			auto pSpriteIcon = Sprite::create("res_ui/props_icon/coins1.png");
			pLootIcon = pSpriteIcon;			
		}
	}
	addChild(pLootIcon, ACTOR_BODY_ZORDER, kTagLootIcon);

	// add special effect
	auto pShineAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/loot_shine.anm");
	pShineAnim->setPlayLoop(true);
	pShineAnim->setPlaySpeed(0.3f);
	pShineAnim->setScale(1.1f);
	pShineAnim->setPosition(Vec2(17, 17));
	addChild(pShineAnim, ACTOR_BODY_ZORDER);

	m_fStartTime = GameUtils::millisecondNow();

	pLootIcon->setPosition(Vec2(0, 70));
	auto action = Sequence::create(
		MoveBy::create(LOOT_ACTOR_DROP_ACTION_TIME_1, Vec2(0, 70)),
		MoveBy::create(LOOT_ACTOR_DROP_ACTION_TIME_2, Vec2(0, -140)),
		NULL);
	pLootIcon->runAction(action);

	if(m_lootType == loot_type_gold)
	{
		char numStr[10];
		sprintf(numStr, "%d", number);
		std::string goldFullName = numStr;
		goldFullName.append(pInfo->get_name());
		this->setActorName(goldFullName.c_str());
	}
	else
	{
		this->setActorName(pInfo->get_name().c_str());
	}
	this->showActorName(true);

	setContentSize(Size(40, 50));

	return true;
}

void LootActor::update(float dt)
{
	// state 1: drop from the monster
	if(m_state == State_Init)
	{
		if(GameUtils::millisecondNow() - m_fStartTime > (LOOT_ACTOR_DROP_ACTION_TIME_1 + LOOT_ACTOR_DROP_ACTION_TIME_2) * 1000.f )
		{
			m_fStartTime = GameUtils::millisecondNow();

			m_state = State_Wait;
		}
	}
	// state 2: stay on the ground
	else if(m_state == State_Wait)
	{
		if(GameUtils::millisecondNow() - m_fStartTime > 3000.f)
		{
			auto scene = this->getGameScene();
			// create streak
			m_streak=CCMotionStreak::create(1.0f, 1.0f, 20.f, Color3B(255,217,85), "images/streak.png");
			m_streak->setPosition(scene->convertToCocos2DSpace(this->getWorldPosition()));
			scene->getActorLayer()->addChild(m_streak, SCENE_ROLE_LAYER_BASE_ZORDER);

			// prepare to fly from the ground
			this->setLayerId(1);

			this->showActorName(false);

			m_state = State_Flying;
		}
	}
	// state 3: fly to the player, and remove self
	else if(m_state == State_Flying)
	{
		// update loot's postion
		bool bReached = false;
		auto me = GameView::getInstance()->myplayer;
		Vec2 targetWorldPos = Vec2(me->getWorldPosition().x, me->getWorldPosition().y - 100);
		float distance = m_flyingSpeed * dt;
		// get direction by actor's current position and target pos
		Vec2 offset = GameUtils::getDirection(this->getWorldPosition(), targetWorldPos);
		offset = offset* distance;
		if(offset.getLength() >= this->getWorldPosition().getDistance(targetWorldPos))
		{
			bReached = true;
			offset = targetWorldPos - this->getWorldPosition();

			if(m_lootType == loot_type_gold)
				ActorUtils::addGoldMessageToScene(m_nLootAmout);
		}
		this->setWorldPosition(Vec2(this->getWorldPosition().x + offset.x, this->getWorldPosition().y + offset.y));

		// update scale
		m_flyingScale -= 0.04f;
		if(m_flyingScale < 0.8f)
			m_flyingScale = 0.8f;
		this->setScale(m_flyingScale);

		// update streak effect
		if(m_streak)
		{
			m_streak->setPosition(Vec2(this->getPosition().x, this->getPosition().y));
		}

		// remove self
		if(bReached)
		{
			this->removeFromParent();
			m_streak->removeFromParent();
		}
	}
}

/*
void LootActor::showActorName(bool bShow)
{
	std::string namestr = getActorName();
	const char* name = namestr.c_str();

	Label* nameLabel = (Label*)this->getChildByTag(GameActor::kTagActorName);
	if(bShow)
	{
		if(nameLabel == NULL)
		{
			// create name label
			nameLabel = Label::createWithTTF(name, APP_FONT_NAME, 17);
			nameLabel->setAnchorPoint(Vec2(0.5f,0.5f));
			nameLabel->setTag(GameActor::kTagActorName);
			nameLabel->setColor(GameView::getInstance()->getGoodsColorByQuality(m_lootQulity));

			// name background
			auto  sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80_new.png");
			sprite_nameFrame->setAnchorPoint(Vec2(0.5f,0.5f));
			sprite_nameFrame->setPosition(Vec2(0,40));
			float width = nameLabel->getContentSize().width + 8;
			float height = nameLabel->getContentSize().height + 2;
			sprite_nameFrame->setPreferredSize(Size(width, height));
			this->addChild(sprite_nameFrame, GameActor::ACTOR_NAME_ZORDER, kTagLootNameBG);

			this->addChild(nameLabel, GameActor::ACTOR_NAME_ZORDER);
		}
		else
		{
			nameLabel->setString(name);
		}
		nameLabel->setPosition(Vec2(0, 40));
	}
	else
	{
		if(nameLabel != NULL)
		{
			Node* pNode = this->getChildByTag(kTagLootNameBG);
			if(pNode != NULL)
				pNode->removeFromParent();
			nameLabel->removeFromParent();
		}
	}
}
*/
void LootActor::showActorName(bool bShow)
{
	std::string namestr = getActorName();
	const char* name = namestr.c_str();

	if(bShow)
	{
		if(m_pNameNode == NULL)
		{
			m_pNameNode = Node::create();

			// create name label
			auto nameLabel = Label::createWithTTF(name, APP_FONT_NAME, 17);
			nameLabel->setAnchorPoint(Vec2(0.5f,0.5f));
			nameLabel->setTag(GameActor::kTagActorName);
			nameLabel->setColor(GameView::getInstance()->getGoodsColorByQuality(m_lootQulity));
			
			// name background
			auto  sprite_nameFrame = cocos2d::extension::Scale9Sprite::create("res_ui/zhezhao80_new.png");
			sprite_nameFrame->setAnchorPoint(Vec2(0.5f,0.5f));
			float width = nameLabel->getContentSize().width + 8;
			float height = nameLabel->getContentSize().height + 2;
			sprite_nameFrame->setPreferredSize(Size(width, height));
			m_pNameNode->addChild(sprite_nameFrame);

			m_pNameNode->addChild(nameLabel);

			Vec2 cocosPos = this->getGameScene()->convertToCocos2DSpace(this->getWorldPosition());
			m_pNameNode->setPosition(Vec2(cocosPos.x, cocosPos.y + 40));
			this->getGameScene()->getActorLayer()->addChild(m_pNameNode, SCENE_UI_LAYER_BASE_ZORDER);
		}
	}
	else
	{
		if(m_pNameNode != NULL)
		{
			m_pNameNode->removeFromParent();
			m_pNameNode = NULL;
		}
	}
}

void LootActor::updateActorName()
{
	// update the name node's position
}
