#ifndef _LEGEND_GAMEACTOR_PRESENTBOX_H_
#define _LEGEND_GAMEACTOR_PRESENTBOX_H_

#include "GameActor.h"
#include "cocos2d.h"

class CCLegendAnimation;
class CPushCollectInfo;

#define PICKINGACTOR_ROLE_HEIGHT 50

/**
 * 宝箱
 * @author liuliang
 * @version 0.1.0
 */
class PresentBox : public GameActor
{
public:
	PresentBox();
	virtual ~PresentBox();

	bool init(const char* actorName);
	bool initPresentBox(const char* actorName);					// 参数为 宝箱动画的 路径+名称

	virtual void showActorName(bool bShow);

	bool getActivied();
	void setActivied(bool isactive);


private:
	bool isActivied;
	CCLegendAnimation* m_pAnim;
};

#endif