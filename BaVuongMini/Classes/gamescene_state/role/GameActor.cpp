#include "GameActor.h"

#include "../GameSceneState.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "AppMacros.h"

//const float DEFAULT_ANGLES_DIRECTION_DATA[] = {
//	22.5f, 67.5f, 112.5f, 157.5f, 202.5f, 247.5f, 292.5f, 337.5f
//	//30.f, 60.f, 120.f, 150.f, 210.0f, 240.f, 300.f, 330.0f
//	//35.f, 60.f, 120.f, 145.f, 215.0f, 240.f, 300.f, 325.0f
//	//35.f, 65.f, 115.f, 145.f, 215.0f, 245.f, 295.f, 325.0f
//	//30.f, 65.f, 115.f, 150.f, 30+180, 65+180.f, 115+180.f, 150+180.f
//	//39.6f, 78.f, 102.f, 140.4f, 39.6f+180.f, 78.f+180.f, 102.f+180.f, 140.f+180.f
//};

const float DEFAULT_ANGLES_DIRECTION_DATA[] = {
	//45.f, 135.f, 225.f, 315.f
	30.f, 150.f, 210.f, 330.f
};

GameActor::GameActor()
: m_worldPosition(Vec2(0, 0))
, m_animScale(1.0f)
, m_layerId(1)
, m_bIsDestroy(false)
, paramData(NULL)
, m_gameScene(NULL)
, m_lockedActorId(NULL_ROLE_ID)
, m_lockedOwnerId(NULL_ROLE_ID)
, m_reflectionEnabled(false)
, m_worldPositionZ(0)
, m_animActionID(0)
{
	setCascadeOpacityEnabled(true);   // opacity可作用于子结点
}

GameActor::~GameActor()
{
	CC_SAFE_DELETE_ARRAY(paramData);
}

void GameActor::setPosition(const Vec2 &position)
{
	if(m_gameScene != NULL)
	{
		if(m_gameScene->isSynchronizeCocosPosition())
		{
			Node::setPosition(position);
		}
		else
		{
			// ONLY schronize the world position
			Vec2 worldPosition = m_gameScene->convertToGameWorldSpace(position);
			m_worldPosition.setPoint(worldPosition.x, worldPosition.y);
		}
	}
	else
	{
		Node::setPosition(position);
	}
}

void GameActor::setPositionImmediately(const Vec2 &position)
{
	Node::setPosition(position);
}

void GameActor::setWorldPosition(Vec2 newWorldPosition)
{
	if(m_gameScene == NULL)
	{
		m_worldPosition.setPoint(newWorldPosition.x, newWorldPosition.y);
	}
	else
	{
		// the actor must not be out of the map, if out of map, re-set it
		if(newWorldPosition.x < 0)
			newWorldPosition.x = 0;
		if(newWorldPosition.y < 0)
			newWorldPosition.y = 0;
		if(newWorldPosition.x >= m_gameScene->getMapSize().width)
			newWorldPosition.x = m_gameScene->getMapSize().width - 1;
		if(newWorldPosition.y >= m_gameScene->getMapSize().height)
			newWorldPosition.y = m_gameScene->getMapSize().height - 1;

		m_worldPosition.setPoint(newWorldPosition.x, newWorldPosition.y);

		// synchronize other scene info
		this->updateCoverStatus();
		this->updateWaterStatus();
	}
}
Vec2 GameActor::getWorldPosition()
{
	return m_worldPosition;
}

void GameActor::setWorldPositionZ(float z)
{
	m_worldPositionZ = z;
}
float GameActor::getWorldPositionZ()
{
	return m_worldPositionZ;
}

int GameActor::getType()
{
	return type;
}
void GameActor::setType(int typeValue)
{
	type = typeValue;
}

bool GameActor::hasLockedActor() 
{
	return m_lockedActorId != NULL_ROLE_ID;
}
GameActor* GameActor::getLockedActor() 
{
	if (!hasLockedActor()) {
		return NULL;
	}

	return m_gameScene->getActor(m_lockedActorId);
}
void GameActor::setLockedActorId(long long lockedActorId) 
{ 
	m_lockedActorId = lockedActorId; 
	if(lockedActorId == NULL_ROLE_ID)
		return;

	auto lockedActor = m_gameScene->getActor(m_lockedActorId);
	if(lockedActor != NULL)
		lockedActor->setLockedOwnerId(this->getRoleId());
};

void GameActor::showLockedCircle(bool bShow, const char* circleName)
{
	if(bShow)
	{
		if(circleName == NULL)
			return;

		// create the locked circle node
		auto pCircle = CCLegendAnimation::create(circleName);
		pCircle->setPlayLoop(true);
		pCircle->setReleaseWhenStop(false);
		pCircle->setPlaySpeed(0.6f);
		addChild(pCircle, GameActor::ACTOR_LOCKED_CIRCLE_ZORDER, kTagLockedCircle);
	}
	else
	{
		// remove the circle
		auto pNode = this->getChildByTag(GameActor::kTagLockedCircle);
		if(pNode != NULL)
			pNode->removeFromParent();
	}
}



Label* GameActor::createNameLabel(const char* name)
{
	Color3B tintColorRed      =  { 255, 0, 0   };
    Color3B tintColorYellow   =  { 255, 255, 0 };
    Color3B tintColorWhite     =  { 255, 255, 255   };
    Color3B strokeColor       =  { 0, 10, 255  };
    Color3B strokeShadowColor =  { 0, 0, 0   };

	// create the label stroke and shadow
    //FontDefinition strokeShaodwTextDef;
    //strokeShaodwTextDef._fontSize = GAMEACTOR_NAME_FONT_SIZE;
    //strokeShaodwTextDef._fontName = std::string(APP_FONT_NAME);
    
    //strokeShaodwTextDef.m_stroke.m_strokeEnabled = true;
    //strokeShaodwTextDef.m_stroke.m_strokeColor   = strokeShadowColor;
    //strokeShaodwTextDef.m_stroke.m_strokeSize    = 1.5;
    
    //strokeShaodwTextDef._shadow._shadowEnabled = true;
    //strokeShaodwTextDef._shadow._shadowOffset  = Size(1.0f, -1.0f);
    //strokeShaodwTextDef._shadow._shadowOpacity = 1.0;
    //strokeShaodwTextDef._shadow._shadowBlur    = 1.0;
    //strokeShaodwTextDef._shadow._   = strokeShadowColor;
    
    Color4B nameColor = { 52, 255, 53, 255 };
    //strokeShaodwTextDef._fontFillColor   = nameColor;
    
    // shadow + stroke label
	//auto nameLabel = Label::createWithTTFWithFontDefinition(name, strokeShaodwTextDef);
	//nameLabel->setTag(kTagActorName);

	auto nameLabel = Label::createWithTTF(name, APP_FONT_NAME, GAMEACTOR_NAME_FONT_SIZE);
	nameLabel->enableShadow(Color4B::BLACK, Size(1.0f, -1.0f), 1.0f);
	nameLabel->setTextColor(nameColor);
	nameLabel->setTag(kTagActorName);

	//Label* nameLabel = Label::createWithTTF(name, "Arial", 24);
	//nameLabel->setTag(kTagActorName);

	return nameLabel;
}

void GameActor::showActorName(bool bShow)
{
	// kTagActorName
}
std::string GameActor::getActorName()
{
	return m_ActorName;
}
void GameActor::setActorName(const char* name)
{
	m_ActorName = name;
}

Vec2 GameActor::getSortPoint()
{
	//CCAssert(paramData != NULL, "data should not be null");
	if(paramData == NULL)
		return m_worldPosition;

	return Vec2(paramData[0] + m_worldPosition.x, paramData[1] + m_worldPosition.y);
}

float GameActor::getAnimScale()
{
	return m_animScale;
}

//int GameActor::getAnimDirection()
//{
//	return getAnimDirection(Vec2(0, 0), m_moveDir, DEFAULT_ANGLES_DIRECTION_DATA);
//}

/**
 * 根据2点间X，Y偏差得到2点的方向关系
 */
//int GameActor::getAnimDirection(Vec2 from, Vec2 to, const float* angles_data) {
//	float o = to.x - from.x;
//    float a = to.y - from.y;
//
//	// the joystick is not moved
//	if(o == 0 && a == 0)
//		return -1;
//
//	float degree = (float) CC_RADIANS_TO_DEGREES( atan2f( o, a) );
//	if(o < 0)
//	{
//		degree += 360;
//	}
//	//CCLOG("joystick angle: (%f)", at);
//
//	int direction = -1;
//	if (degree >= angles_data[7] || degree < angles_data[0]) {// up
//		direction = 0;
//	} else if (degree >= angles_data[6] && degree < angles_data[7]) {// left up
//		direction = 1;
//	} else if (degree >= angles_data[5] && degree < angles_data[6]) {// left
//		direction = 2;
//	} else if (degree >= angles_data[4] && degree < angles_data[5]) {// left down
//		direction = 3;
//	} else if (degree >= angles_data[3] && degree < angles_data[4]) {// down
//		direction = 4;
//	} else if (degree >= angles_data[2] && degree < angles_data[3]) {// right down
//		direction = 5;
//	} else if (degree >= angles_data[1] && degree < angles_data[2]) {// right
//		direction = 6;
//	} else if (degree >= angles_data[0] && degree < angles_data[1]) {// up right
//		direction = 7;
//	}
//	//CCLOG("joystick direction: (%d)", direction);
//
//	return direction;
//}

/**
 * 根据2点间X，Y偏差得到2点的方向关系
 */
int GameActor::getAnimDirection(Vec2 from, Vec2 to, const float* angles_data) {
	float o = to.x - from.x;
    float a = to.y - from.y;

	// the joystick is not moved
	if(o == 0 && a == 0)
		return -1;

	float degree = (float) CC_RADIANS_TO_DEGREES( atan2f( o, a) );
	if(o < 0)
	{
		degree += 360;
	}
	//CCLOG("joystick angle: (%f)", at);

	int direction = -1;
	if (degree >= angles_data[3] || degree < angles_data[0]) {// up
		direction = 0;
	} else if (degree >= angles_data[2] && degree < angles_data[3]) {// left
		direction = 1;
	} else if (degree >= angles_data[1] && degree < angles_data[2]) {// down
		direction = 2;
	} else if (degree >= angles_data[0] && degree < angles_data[1]) {// right
		direction = 3;
	}
	//CCLOG("animation direction: (%d)", direction);

	return direction;
}

void GameActor::updateCoverStatus()
{
	// 检测是否碰到了Cover
	int tileX = m_gameScene->positionToTileX(getWorldPosition().x);
	int tileY = m_gameScene->positionToTileY(getWorldPosition().y);
	if(m_gameScene->checkInMap(tileX, tileY))
	{
		if(m_gameScene->isCover(tileX, tileY))
			this->setOpacity(80);
		else
			this->setOpacity(255);
	}
}

void GameActor::updateWaterStatus()
{
	int tileX = m_gameScene->positionToTileX(getWorldPosition().x);
	int tileY = m_gameScene->positionToTileY(getWorldPosition().y);
	if(m_gameScene->checkInMap(tileX, tileY))
	{
		if(m_gameScene->isWater(tileX, tileY))
			setReflection(true);
		else
			setReflection(false);
	}
}

Rect GameActor::getVisibleRect()
{
	return getBoundingBox();
}

bool GameActor::isDestroy() {
	return m_bIsDestroy;
}
void GameActor::setDestroy(bool bDestroy) {
	this->m_bIsDestroy = bDestroy;
}