#ifndef _GAMEAUDIO_H_
#define _GAMEAUDIO_H_

//mission 
#define MISSION_ACCEPT "chuandaizhuangbei1"
#define MISSION_COMPLETED "chuandaizhuangbei1"
#define MISSION_CANCEL "anniu1"
//mail
#define MAIL_NEW "youjiantixing"
#define MAIL_DELETE "Click03"
#define ANNEX_EXTRACT "getPrize"
#define MAIL_SEND "Click03"
//package
#define PACKAGE_SORT "anniu2"
#define PACKAGE_OPEN "Click03"
#define EQUIP_DRESS "chuandaizhuangbei"
#define EQUIP_UNLOAD "xiezaizhuangbei"
#define PROP_USE "anniu3"
#define ITEM_DISCARD "Click01"
//auction
#define AUCTION_SUCCESS "Click03"
#define PURCHASE_SUCCESS "Click03"
//interface
#define SCENE_OPEN "Click03"
#define SCENE_CLOSE "Click03"
//#define ERROR_TIPS "MaceMetalHitChainCrit"
//other
#define MYPLAYER_UPGRADE "chuandaizhuangbei1"
#define PROP_SALE "Click03"
#define PROP_PURCHASE "Click03"
#define REVIVAL_TRIGGER "jiemiantanchu"
#define NPC_CLICK "Click03"
#define CHAT_PRIVATE "youjiantixing"
//#define REPAIR_TRIGGER "xiuli1"
#define SKILL_LEARN "chuandaizhuangbei1"
//unfulfilment
#define BUTTON_CLICK "anniu3"//wait
#define TEXT_INPUT "Click01"//wait
#define MYPLAYER_STEP "anniu2"//wait
#define PROP_PICK "xiezaizhuangbei"//wait
#define MISSION_FAIL "anniu1"			//without
#define OFFLINE_TRIGGER "jiemiantanchu"//without
#define OPERATE_SUCCESS "chuandaizhuangbei1"//unknown
#define OPERATE_FAIL "xiuli2"//unknown
#define TELEPORT_USE "jiemiantanchu"//without
//old effect
#define MYPLAYER_DIED "caiwenji"
#define STAR_SUCCESS "touxiang"
#define STAR_FAIL "sunshangxiang"
#define REFINE_CLICK "touxiang"
#define GEM_CLICK "touxiang"
//backgroud music
#define MUSIC_KING "003"
#define MUSIC_IMPERIAL "015"
#define MUSIC_ROOKIE "005"
#define MUSIC_RAID "fight2"
#define MUSIC_BORDER "010"
#define MUSIC_OTHER "016"
//#define MUSIC_LOGIN "001"
#define MUSIC_NEW1 "fight4"
#define MUSIC_NEW2 "fight3"

#define EFFECT_MUSOU "musou"
#define EFFECT_GENERAL "general"
#endif