﻿#ifndef _GAMEUTILS_GRAPH_GRAPHSTDIJKSTRA_H_
#define _GAMEUTILS_GRAPH_GRAPHSTDIJKSTRA_H_

#include <vector>

class NextAdjNode;

/**
 * Graph的邻接表表示
 */
class GraphAdjList {
public:
	/**
	 * 与此结点邻接的第一个结点信息
	 */
	NextAdjNode* firstNode;

	/**
	 * 第一个结点
	 * @param first
	 */
	GraphAdjList(NextAdjNode* first);
};

//////////////////////////////////////////////////////////

/**
 * 点对点(S-T)Dijkstra算法的具体实现
 */
class GraphSTDijkstra {
public:
	//图的邻接链表
	std::vector<GraphAdjList*>* graphAdjList;
	
	//图中总的结点数
	int totalNodeNum;
	
	//求得的最短路径长度
	int distance;
	
	//目标结点到源结点的一条通路
	//std::vector<int>* preNodes;
	int* preNodes;
	
	GraphSTDijkstra();
	
	void setGraph(std::vector<GraphAdjList*>* graph);

	/**
	 * 最短路径长度
	 */
	int getDistance();

	/**
	 * 返回所求结点间最短路径经过的结点序列。其中，path[0]为目标结点。
	 * @return
	 * 所求结点间最短路径经过的结点序列
	 */
	std::vector<int>* getPath(int sourceNode, int targetNode);

	/**
	 * Dijkstra(S-T)算法的具体实现。
	 */
	bool searchPath(int sourceNode, int targetNode);


private:
	/**
	 * 不在子图中的下一个结点，都在子图中返回-1
	 * 
	 * @param disTemp   从源结点到各结点的距离
	 * @param isInPath  已经在最短路径子图中的结点
	 * @return
	 */
	int chooseNextPathNode(int* disTemp, bool* isInPath);

	/**
	 * 通过新找到的结点，比较剩余结点到源结点的路径。
	 * 若源结点通过新结点到剩余某结点的总路径小于目前
	 * 源结点到某结点的路径，则修改源结点到某结点的路径
	 * @param currentNode 
	 * @param disTemp      从源结点到各结点的距离
	 * @param isInPath     已经在最短路径子图中的结点
	 */
	void updateDistance(int currentNode, int* disTemp, bool* isInPath);
};

#endif