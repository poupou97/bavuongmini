﻿#include "GraphSTDijkstra.h"

#include "cocos2d.h"
#include "NextAdjNode.h"


GraphAdjList::GraphAdjList(NextAdjNode* first) {
	firstNode = first;
}

/////////////////////////////////////////////////////////////////////
	
GraphSTDijkstra::GraphSTDijkstra()
{
	totalNodeNum = 0;
	graphAdjList = NULL;
	preNodes = NULL;
	distance = -1;
}
	
void GraphSTDijkstra::setGraph(std::vector<GraphAdjList*>* graph) {
	graphAdjList = graph;
	totalNodeNum = graphAdjList->size();
	preNodes = new int[totalNodeNum];
}

int GraphSTDijkstra::getDistance() {
	return distance;
}

std::vector<int>* GraphSTDijkstra::getPath(int sourceNode, int targetNode) {
	int temp = targetNode;
	int nodeNumInPath = 1; 
	while (temp != sourceNode) {
		nodeNumInPath++;
		temp = preNodes[temp];
	}

	std::vector<int>* path = new std::vector<int>();
	path->resize(nodeNumInPath);

	path->at(0) = targetNode;
	temp = targetNode;
	int nextPathNode = 1;
	while (temp != sourceNode) {
		temp = preNodes[temp];
		path->at(nextPathNode) = temp;
		nextPathNode++;
	}

	return path;
}

bool GraphSTDijkstra::searchPath(int sourceNode, int targetNode) {

	distance = -1;
	for (int i = 0; i < totalNodeNum; i++) {
		preNodes[i] = sourceNode;
	}

	//用于存储从源结点到其他各个结点的最短路径长度的temp
	int* distanceTemp = new int[totalNodeNum];

	//初始时将从源结点到其他各个结点的最短路径长度全都设置为无穷远。除源结点到自身的长度为0。
	for (int i = 0; i < totalNodeNum; i++) {
		distanceTemp[i] = 0X0FFFFFFF;
	}
	distanceTemp[sourceNode] = 0;

	//根据图的邻接表具有的信息，将于源结点邻接的结点的路径长度设置为边的权值，其余不变，作为长度初始值。
	NextAdjNode* temp = NULL;
	temp = graphAdjList->at(sourceNode)->firstNode;
	if(temp == NULL){
		delete[] distanceTemp;
		return false;
	}

	distanceTemp[temp->nodeNum] = temp->edgeWeight;
	while (temp->nextNode != NULL) {
		temp = temp->nextNode;
		distanceTemp[temp->nodeNum] = temp->edgeWeight;
	}

	//用于记录最短路径   子图中的结点     存在设置为true
	bool* hasInPath = new bool[totalNodeNum];
	for (int i = 0; i < totalNodeNum; i++) {
		hasInPath[i] = false;
	}

	//下一个结点
	int nextNode = -1;
		
	//不是目标结点时
	while (nextNode != targetNode) {
			
		//不在子图中的下一个节点
		nextNode = chooseNextPathNode(distanceTemp, hasInPath);
			
		//所有点都找到过了
		if(nextNode == -1){
			delete[] distanceTemp;
			delete[] hasInPath;
			return false;
		}

		//加入到最短路径子图中
		hasInPath[nextNode] = true;
			
		//通过新找到的结点，比较剩余结点结点到源结点的路径。
		//若源结点通过新结点到剩余某结点的总路径小于目前源结点到某结点的路径，则修改源结点到某结点的路径。
		updateDistance(nextNode, distanceTemp, hasInPath);
	}
		
	//最短路径长度
	distance = distanceTemp[targetNode];

	delete[] distanceTemp;
	delete[] hasInPath;
	return true;
}

int GraphSTDijkstra::chooseNextPathNode(int* disTemp, bool* isInPath) {
	int temp = 0X0FFFFFFF;
	int nextNode = -1;
	for (int i = 0; i < totalNodeNum; i++) {
		if (!isInPath[i]) {
			if (temp > disTemp[i]) {
				temp = disTemp[i];
				nextNode = i;
			}
		}
	}
	return nextNode;
}

void GraphSTDijkstra::updateDistance(int currentNode, int* disTemp, bool* isInPath) {
	auto temp = graphAdjList->at(currentNode)->firstNode;
	while (temp != NULL) {
		if (!isInPath[temp->nodeNum]) {
			if (disTemp[currentNode] + temp->edgeWeight < disTemp[temp->nodeNum]) {
				disTemp[temp->nodeNum] = disTemp[currentNode] + temp->edgeWeight;

				preNodes[temp->nodeNum] = currentNode;
			}
		}
		temp = temp->nextNode;
	}
}
