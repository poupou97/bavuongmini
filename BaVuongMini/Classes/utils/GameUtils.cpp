/*
 *  GameUtils.cpp
 *
 *  Created by Zhao Gang on 07-30-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#include "GameUtils.h"
#include "SimpleAudioEngine.h"

using namespace CocosDenshion;

Vec2 GameUtils::getDirection(Vec2 source, Vec2 target)
{
	Vec2 subPoint = target - source;
	if(subPoint.x == 0 && subPoint.y == 0)
		return subPoint;
	else
		return subPoint.getNormalized();
}

float GameUtils::getDegree(Vec2 source, Vec2 target)
{
	float o = target.x - source.x;
    float a = target.y - source.y;

	if(o == 0 && a == 0)
		return 0;

    float at = (float) CC_RADIANS_TO_DEGREES( atanf( o/a) );
    
    if( a < 0 ) 
    {
        if(  o < 0 )
            at = 180 + fabs(at);
        else
            at = 180 - fabs(at);    
    }

	return at;
}

long long GameUtils::millisecondNow()  
{ 
	struct timeval now; 
	gettimeofday(&now, NULL); 

	return (now.tv_sec * 1000 + now.tv_usec / 1000.f); 
}

long long GameUtils::getDateSecond()
{
	time_t timep; 
#if (CC_TARGET_PLATFORM == CC_PLATFORM_WIN32)  
	time(&timep); 
#else  
	struct cc_timeval now;  
	CCTime::gettimeofdayCocos2d(&now, NULL);  
	timep = now.tv_sec; 
#endif
	return timep;
}

void GameUtils::getDateNow(int level) 
{ 
	struct tm *tm; 
	time_t timep = getDateSecond();
	tm = localtime(&timep); 
	int year = tm->tm_year + 1900; 
	int month = tm->tm_mon + 1; 
	int day = tm->tm_mday; 
	int hour=tm->tm_hour; 
	int min=tm->tm_min; 
	int second=tm->tm_sec; 
}

std::string GameUtils::getFullPathFilename ( std::string filename, std::string relativeTo )
{
	CCAssert(relativeTo.size() > 0, "relativeTo is empty");

	int lastSlashIndex = relativeTo.find_last_of("/\\");
	if(lastSlashIndex == -1)   // not found
		relativeTo = "";
	else
		relativeTo = relativeTo.substr(0, lastSlashIndex+1);

	std::string str = "../";
	while(true)
	{
		int found = filename.find(str);
		if(found < 0)
			break;

		// remove "../"
		filename = filename.substr(found+str.size());

		// remove one parent path
		int lastPos = relativeTo.find_last_of("/");
		CCAssert(lastPos >= 0, "../ does not match with /");

		lastPos = relativeTo.find_last_of("/", lastPos - 1);
		if(lastPos == -1)
			relativeTo = "";
		else
			relativeTo = relativeTo.substr(0, lastPos+1);
	}
	filename = relativeTo.append(filename);
	return filename;
}

void GameUtils::addGray(Sprite* sp)  
{  
    do  
    {  
        auto pProgram = ShaderCache::getInstance()->getGLProgram(GLProgram::SHADER_NAME_ETC1AS_POSITION_TEXTURE_GRAY);
        sp->setGLProgram(pProgram);
        CHECK_GL_ERROR_DEBUG();  
          
        sp->getGLProgram()->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        sp->getGLProgram()->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_COLOR, GLProgram::VERTEX_ATTRIB_COLOR);
        sp->getGLProgram()->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        CHECK_GL_ERROR_DEBUG();  
          
        sp->getGLProgram()->link();
        CHECK_GL_ERROR_DEBUG();  
          
        sp->getGLProgram()->updateUniforms();
        CHECK_GL_ERROR_DEBUG();  
    } while (0);  
}  
void GameUtils::removeGray(Sprite* sp)  
{  
    do  
    {  
        auto pProgram = ShaderCache::getInstance()->getGLProgram(GLProgram::SHADER_NAME_POSITION_TEXTURE_COLOR);
        sp->setGLProgram(pProgram);
        CHECK_GL_ERROR_DEBUG();  
          
        sp->getGLProgram()->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_POSITION, GLProgram::VERTEX_ATTRIB_POSITION);
        sp->getGLProgram()->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_COLOR, GLProgram::VERTEX_ATTRIB_COLOR);
        sp->getGLProgram()->bindAttribLocation(GLProgram::ATTRIBUTE_NAME_TEX_COORD, GLProgram::VERTEX_ATTRIB_TEX_COORD);
        CHECK_GL_ERROR_DEBUG();  
          
        sp->getGLProgram()->link();
        CHECK_GL_ERROR_DEBUG();  
          
        sp->getGLProgram()->updateUniforms();
        CHECK_GL_ERROR_DEBUG();  
    } while (0);  
}

Color3B GameUtils::convertToColor3B(int color)
{
    // rgb color
    int red = (color >> 16) & 0xFF;
    int green = (color >> 8) & 0xFF;
    int blue = (color) & 0xFF;
    
    return Color3B(red, green, blue);
}

int GameUtils::convertToColorInt( Color3B color )
{
	return color.r << 16| color.g<<8| color.b;
}

void GameUtils::playGameSound(const char* resName, int type, bool mode)
{
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
	std::string soundPath("sound/win32/");
#elif CC_TARGET_PLATFORM==CC_PLATFORM_IOS
	std::string soundPath("sound/ios/");
#elif CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID
	std::string soundPath("sound/android/");
#endif
	soundPath.append(resName);

#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
	soundPath.append(".wav");
#elif CC_TARGET_PLATFORM==CC_PLATFORM_IOS
	soundPath.append(".aiff");
#elif CC_TARGET_PLATFORM==CC_PLATFORM_ANDROID
	soundPath.append(".ogg");
#endif

	float volume = 0;
	switch(type)
	{
	case 1:
#if CC_TARGET_PLATFORM != CC_PLATFORM_ANDROID
		// on Android platform, getBackgroundMusicVolume() always return 0, it's an Engine bug 
		volume = SimpleAudioEngine::getInstance()->getBackgroundMusicVolume();
		//CCLOG("music sound volume: %f", volume);
		if(volume <= 0)
			break;
#endif
		SimpleAudioEngine::getInstance()->playBackgroundMusic(soundPath.c_str(), mode);
		break;

	case 2:
		volume = SimpleAudioEngine::getInstance()->getEffectsVolume();
		//CCLOG("effect sound volume: %f", volume);
		if(volume <= 0)
			break;
		SimpleAudioEngine::getInstance()->playEffect(soundPath.c_str(), mode);
		break;
	default:
		break;
	}
}

std::string GameUtils::getAppUniqueID()
{
	#define APP_UNIQUE_ID "app_unique_id"
	std::string id = UserDefault::getInstance()->getStringForKey(APP_UNIQUE_ID, "");
	if(id == "")
	{
		char seconds[65];
		sprintf(seconds, "%lld", GameUtils::getDateSecond());

		char milliseconds[65];
		sprintf(milliseconds, "%lld", GameUtils::millisecondNow());

		id = seconds;
		id.append(milliseconds);
		UserDefault::getInstance()->setStringForKey(APP_UNIQUE_ID, id);
	}

	return id;
}

unsigned int GameUtils::getRandomNum(unsigned int size)
{
	CCAssert(size > 0, "size can't equal 0");
	unsigned int index = size * CCRANDOM_0_1();
	if(index >= size)
	{
		--index;
	}
	return index;
}