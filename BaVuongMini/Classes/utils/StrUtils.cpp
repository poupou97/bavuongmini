#include "StrUtils.h"

// more about this function,
// please see: http://stackoverflow.com/questions/216823/whats-the-best-way-to-trim-stdstring
std::string StrUtils::trim( std::string & str )
{
    size_t end = str.find_last_not_of( " \n\r\t" );
    if ( end != std::string::npos )
        str.resize( end + 1 );

    std::string value;
    size_t start = str.find_first_not_of( " \n\r\t" );
    if ( start != std::string::npos )
        value = str.substr( start );

    //std::move(str);
    return value;
}

bool StrUtils::startsWith(std::string& str, std::string prefix)
{
	//prefix = "//";
	if (str.compare(0, prefix.size(), prefix) == 0)
		return true;

	return false;
}

//void StrUtils::split(vector<string> &result, string str, char delim ) {
//	string tmp;
//	string::iterator i;
//	result.clear();
//
//	for(i = str.begin(); i <= str.end(); ++i) {
//		if((const char)*i != delim  && i != str.end()) {
//			tmp += *i;
//		} else {
//			result.push_back(tmp);
//			tmp = "";
//		}
//	}
//}

vector <string> StrUtils::split(const string& str, const string& delimiter) {
    vector <string> tokens;

    string::size_type lastPos = 0;
    string::size_type pos = str.find(delimiter, lastPos);

    while (string::npos != pos) {
        // Found a token, add it to the vector.
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = pos + delimiter.size();
        pos = str.find(delimiter, lastPos);
    }

    tokens.push_back(str.substr(lastPos, str.size() - lastPos));
    return tokens;
}

std::string StrUtils::applyColor(const char* src, Color3B color)
{
	std::string srcStr = src;
	int index = srcStr.find("/#");
	CCAssert(index < 0, "the string has already include color, so error");

	char colorChars[7];
	sprintf(colorChars, "%02x%02x%02x", color.r, color.g, color.b);

	std::string newStr = "/#";
	newStr.append(colorChars);
	newStr.append(" ");
	newStr.append(src);
	newStr.append("/");

	return newStr;
}

std::string StrUtils::unApplyColor( const char* src )
{
	std::string srcStr = src;
	int index = srcStr.find("/#");
	if (index<0)
	{
		return srcStr;
	}
	else
	{
		std::string newStr = srcStr.substr(9,srcStr.size()-10);
		return newStr;
	}
}

int StrUtils::calcCharCount(const char * pszText)
{
    int n = 0;
    char ch = 0;
    while ((ch = *pszText))
    {
        CC_BREAK_IF(! ch);

        if (0x80 != (0xC0 & ch))
        {
            ++n;
        }
        ++pszText;
    }
    return n;
}

