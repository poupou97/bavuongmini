/*
 *  StrUtils.h
 *
 *  Created by Zhao Gang on 01-00-2014.
 *  Copyright 2013 Perfect Future. All rights reserved.
 */

#ifndef _UTILS_STRING_UTILS_H
#define _UTILS_STRING_UTILS_H

#include "cocos2d.h"

USING_NS_CC;

#include <string>
#include <vector>

using namespace std;

class StrUtils {
public :
	static std::string trim( std::string & str );
	static bool startsWith(std::string& str, std::string prefix);
	//static void split(vector<string> &result, string str, char delim );
	static vector <string> split(const string& str, const string& delimiter = " ");

	static std::string applyColor(const char* src, Color3B color);
	static std::string unApplyColor(const char* src);

	static int calcCharCount(const char * pszText);
};

#endif