/*
*  Joystick.cpp
*  YaoGan
*
*  Created by Liu Yanghui on 11-10-27.
*  Copyright 2011 ard8. All rights reserved.
*
*/

#include "Joystick.h"
#include "GameView.h"
#include "../gamescene_state/role/MyPlayer.h"
#include "../gamescene_state/role/BaseFighterConstant.h"

#define JOYSTICK_BG_TAG 100

Joystick::Joystick()
: active(false)
{
}

void Joystick::updatePos(float delta){
	jsSprite->setPosition(jsSprite->getPosition() + (currentPoint - jsSprite->getPosition()) * 0.5);
}

void Joystick::Active()
{
	if (!active) {
		active = true;
		schedule(schedule_selector(Joystick::updatePos));//添加刷新函数

		//auto pDirector = Director::getInstance();
		//pDirector->getEventDispatcher()-> addTargetedDelegate(this, 0, true);

		auto touchListener = EventListenerTouchOneByOne::create();
		if (nullptr == touchListener) {
			return;
		}
		touchListener->setSwallowTouches(true);
		touchListener->onTouchBegan = CC_CALLBACK_2(Joystick::onTouchBegan, this);
		touchListener->onTouchMoved = CC_CALLBACK_2(Joystick::onTouchMoved, this);
		touchListener->onTouchEnded = CC_CALLBACK_2(Joystick::onTouchEnded, this);
		_eventDispatcher->addEventListenerWithSceneGraphPriority(touchListener,this);

	}else {

	}
}
//冻结摇杆
void   Joystick::Inactive()
{
	if (active) {
		active=false;
		currentPoint = centerPoint;
		Node* bg = this->getChildByTag(JOYSTICK_BG_TAG);
		if(bg != NULL)
			bg->setVisible(false);

		this->unschedule(schedule_selector(Joystick::updatePos));//删除刷新

		//CCDirector* pDirector = Director::getInstance();
		//pDirector->getTouchDispatcher()->removeDelegate(this);//删除委托
		_eventDispatcher->removeEventListenersForTarget(this);
	}else {

	}
}

bool Joystick::isActive()
{
	return active;
}

bool Joystick::onTouchBegan(Touch* touch, Event* event)
{
	if (!active)
		return false;

	Vec2 touchPoint = touch->getLocation();
	if (touchPoint.getDistance(centerPoint) > radius)
		return false;
	currentPoint = touchPoint;

	Node* bg = this->getChildByTag(JOYSTICK_BG_TAG);
	if(bg != NULL)
		bg->setVisible(true);

	// 去掉 “自动战斗”的动画（如果有的话）
	MyPlayer* pMyPlayer = GameView::getInstance()->myplayer;
	Node* pNode = pMyPlayer->getChildByTag(PLAYER_AUTO_FIGHT_ANIMATION_TAG);
	if (NULL != pNode)
	{
		pMyPlayer->removeAutoCombatFlag();
	}

	return true;
}

void  Joystick::onTouchMoved(Touch* touch, Event* event)
{
	Vec2 touchPoint = touch->getLocation();
	if (touchPoint.getDistance(centerPoint) > radius)
	{
		currentPoint = centerPoint + (touchPoint - centerPoint).getNormalized() * radius;
	}else {
		currentPoint = touchPoint;
	}
}

void  Joystick::onTouchEnded(Touch* touch, Event* event)
{
	currentPoint = centerPoint;

	Node* bg = this->getChildByTag(JOYSTICK_BG_TAG);
	if(bg != NULL)
		bg->setVisible(false);

	
}

//获取摇杆方位,注意是单位向量
Vec2 Joystick::getDirection()
{
	Vec2 curPoint = currentPoint - centerPoint;
	if(curPoint.x == 0 && curPoint.y == 0)
		return curPoint;
	else
		return (currentPoint-centerPoint).getNormalized();
}

//获取摇杆力度
float Joystick::getVelocity()
{
	return centerPoint.getDistance(currentPoint);
}

Joystick* Joystick:: JoystickWithCenter(Vec2 aPoint ,float aRadius ,Sprite* aJsSprite,Sprite* aJsBg){
	Joystick *jstick = new Joystick();
	jstick->autorelease();
	jstick->initWithCenter(aPoint,aRadius,aJsSprite,aJsBg);
	return jstick;
}

Joystick* Joystick::initWithCenter(Vec2 aPoint ,float aRadius ,Sprite* aJsSprite,Sprite* aJsBg){
	active = false;
	radius = aRadius;
	centerPoint = aPoint;
	currentPoint = centerPoint;
	jsSprite = aJsSprite;
	jsSprite->setPosition(centerPoint);
	this->addChild(jsSprite, 1);
	if(aJsBg != NULL)
	{
		aJsBg->setVisible(false);   // default, the bg is not visible
		aJsBg->setTag(JOYSTICK_BG_TAG);
		aJsBg->setPosition(centerPoint);
		this->addChild(aJsBg, 0);
	}
	
	return this;
}
