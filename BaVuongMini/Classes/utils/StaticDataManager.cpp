#include "StaticDataManager.h"

#include "../gamescene_state/skill/GameFightSkill.h"
#include "../gamescene_state/exstatus/ExStatus.h"
#include "../gamescene_state/exstatus/ExStatusType.h"
#include "../gamescene_state/exstatus/ExStatusFactory.h"
#include "../messageclient/element/CGeneralBaseMsg.h"
#include <sqlite3.h>
#include "../messageclient/element/CGeneralDetail.h"
#include "../messageclient/element/CGeneralsEvolution.h"
#include "../messageclient/element/CGeneralsTeach.h"
#include "../messageclient/element/CFivePersonInstance.h"
#include "../messageclient/element/CFightWayBase.h"
#include "../messageclient/element/CFightWayGrowingUp.h"
#include "../messageclient/element/CFateBaseMsg.h"
#include "../messageclient/element/CDrug.h"
#include "../messageclient/element/MapRobotDrug.h"
#include "../messageclient/element/CFunctionOpenLevel.h"
#include "../messageclient/element/MapStrngthStone.h"
#include "../gamescene_state/role/General.h"
#include "../messageclient/element/CMonsterBaseInfo.h"
#include "../messageclient/element/CMusouTalent.h"
#include "../messageclient/element/MapEquipAddExp.h"
#include "../messageclient/element/MapStarPickAmount.h"
#include "../messageclient/element/MapStrengthGem.h"
#include "../messageclient/element/MapMarquee.h"
#include "../messageclient/element/MapRobotPotionLimit.h"
#include "../messageclient/element/MapRefineDiffAddValue.h"
#include "../messageclient/element/CAdditionProperty.h"
#include "../messageclient/element/MapRefineValueFactor.h"
#include "../messageclient/element/MapWeapEffect.h"
#include "../messageclient/element/CNewFunctionRemind.h"
#include "../messageclient/element/CVipInfo.h"
#include "../messageclient/element/CCollectInfo.h"
#include "../messageclient/element/CRechargeInfo.h"
#include "../messageclient/element/CGeneralTeachAddedProperty.h"
#include "../messageclient/element/CPowerSendInfo.h"
#include "../messageclient/element/CSysthesisInfo.h"
#include "../messageclient/element/MapStrengthUseStone.h"
#include "../messageclient/element/CSingCopyClazz.h"
#include "../messageclient/element/CSingCopyLevel.h"
#include "../messageclient/element/CGemPropertyMap.h"
#include "../messageclient/element/CRewardBase.h"
#include "../messageclient/element/CPropInfo.h"


std::string getDBPath(const char* configFileName)
{
#if CC_TARGET_PLATFORM==CC_PLATFORM_WIN32
	return FileUtils::getInstance()->fullPathForFilename(configFileName);
#else
	return FileUtils::getInstance()->getWritablePath() + configFileName;
#endif
}

//////////////////////////////////////////////////////////

std::map<std::string, CBaseSkill *> StaticDataBaseSkill::s_baseSkillData;

void StaticDataBaseSkill::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	const char *query_stmt = "SELECT skill_id,profession,name,description,icon,use_model,sequence,required_props,max_level,quality,skill_type,complex_order,skill_variety from t_skill";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char* skillIdField =(char*)sqlite3_column_text(statement, 0);
			int profession=sqlite3_column_int(statement, 1);
			char* nameField=(char*)sqlite3_column_text(statement, 2);
			char* descriptionField=(char*)sqlite3_column_text(statement, 3);
			char* iconField=(char*)sqlite3_column_text(statement, 4);
			int use_modelField=sqlite3_column_int(statement, 5);
			int sequenceField=sqlite3_column_int(statement, 6);
			char* required_propsField=(char*)sqlite3_column_text(statement, 7);
			int max_level=sqlite3_column_int(statement,8);
			int quality = sqlite3_column_int(statement,9);
			int owner = sqlite3_column_int(statement,10);
			int complex_order = sqlite3_column_int(statement,11);
			int skill_variety = sqlite3_column_int(statement,12);
			//CCLOG("skill id: %s, name: : %s", skillIdField, nameField);

			// create map
			auto baseSkill = new CBaseSkill();
			baseSkill->set_id(skillIdField);
			baseSkill->set_profession(profession);
			baseSkill->set_name(nameField);
			baseSkill->set_description(descriptionField);
			baseSkill->set_icon(iconField);
			baseSkill->set_usemodel(use_modelField);
			baseSkill->set_sequence(sequenceField);
			baseSkill->set_maxlevel(max_level);
			baseSkill->set_quality(quality);
			baseSkill->set_owner(owner);
			baseSkill->set_complex_order(complex_order);
			baseSkill->set_skill_variety(skill_variety);
			s_baseSkillData.insert(make_pair(baseSkill->id(),baseSkill));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
} 

//////////////////////////////////////////////////////////

std::map<std::string, std::map<int, CFightSkill*>*> StaticDataFightSkill::s_data;

void StaticDataFightSkill::load(const char* configFileName)
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// according base skill info
    for (std::map<std::string, CBaseSkill*>::iterator it = StaticDataBaseSkill::s_baseSkillData.begin(); it != StaticDataBaseSkill::s_baseSkillData.end(); ++it) 
	{
		auto baseSkill = it->second;

		// query data
		char query_stmt[512];
		sprintf(query_stmt, "SELECT level,description,scope_clazz,distance,interval_time,consume_mp,pre_skill,pri_skill_level,required_level,required_gold,required_point,required_prop,required_num,back_prop,back_num from t_skill_effect where skill_id = '%s'", baseSkill->id().c_str());
		sqlite3_stmt *statement;
		result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
		if (result == SQLITE_OK) {
			std::map<int, CFightSkill*>* fightSkillMap = new std::map<int, CFightSkill*>();
			while (sqlite3_step(statement) == SQLITE_ROW) {
				int levelField =sqlite3_column_int(statement, 0);
				char* descriptionField=(char*)sqlite3_column_text(statement, 1);
				int scope_clazzField=sqlite3_column_int(statement, 2);
				int distanceField=sqlite3_column_int(statement, 3);
				int interval_timeField=sqlite3_column_int(statement, 4);
				int cosume_mp = sqlite3_column_int(statement, 5);
				char* pre_skill = (char*)sqlite3_column_text(statement, 6);
				int pre_skill_level = sqlite3_column_int(statement,7);
				int required_level=sqlite3_column_int(statement, 8);
				int required_gold=sqlite3_column_int(statement, 9);
				int required_point=sqlite3_column_int(statement, 10);
				char* required_prop=(char*)sqlite3_column_text(statement, 11);
				int required_num=sqlite3_column_int(statement, 12);
				char* back_prop = (char*)sqlite3_column_text(statement, 13);
				int back_num =sqlite3_column_int(statement, 14);

				std::string next_description = "";
				int next_required_level = 0;
				int next_required_gold = 0;
				int next_required_point = 0;
				std::string next_required_prop = "";
				int next_required_num = 0;
				std::string  next_pre_skill = "";
				int next_pre_skill_level = 0;
				//CCLOG("skill id: %s, description: %s", baseSkill->id().c_str(), descriptionField);

				char query_stmt1[512];
				sprintf(query_stmt1, "SELECT description,required_level,required_gold,required_point,required_prop,required_num,pre_skill,pri_skill_level from t_skill_effect where skill_id = '%s' and level = '%d'", baseSkill->id().c_str(),levelField+1);
				sqlite3_stmt *statement1;
				result = sqlite3_prepare_v2(pdb, query_stmt1, strlen(query_stmt1), &statement1, NULL);
				if (result == SQLITE_OK) {
					while (sqlite3_step(statement1) == SQLITE_ROW) {
						char* _next_description=(char*)sqlite3_column_text(statement1, 0);
						next_description = _next_description;
						next_required_level = sqlite3_column_int(statement1,1);
						next_required_gold = sqlite3_column_int(statement1,2);
						next_required_point = sqlite3_column_int(statement1,3);
						next_required_prop = (char*)sqlite3_column_text(statement1,4);
						next_required_num = sqlite3_column_int(statement1,5);
						char* _next_pre_skill = (char*)sqlite3_column_text(statement1, 6);
						next_pre_skill = _next_pre_skill;
						next_pre_skill_level = sqlite3_column_int(statement1,7);
					}
				}
				else {
					CCLOG("query data failed!, the reason is: %d", result);
					CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
				}

				// create map
				auto fightSkill = new CFightSkill();
				fightSkill->set_id(baseSkill->id().c_str());
				fightSkill->set_level(levelField);
				fightSkill->set_targetscope(scope_clazzField);
				fightSkill->set_description(descriptionField);
				fightSkill->set_distance(distanceField);
				fightSkill->set_intervaltime(interval_timeField);
				fightSkill->set_mp(cosume_mp);
				fightSkill->set_required_level(required_level);
				fightSkill->set_required_gold(required_gold);
				fightSkill->set_required_point(required_point);
				fightSkill->set_required_prop(required_prop);
				fightSkill->set_required_num(required_num);
				fightSkill->set_pre_skill(pre_skill);
				fightSkill->set_pre_skill_level(pre_skill_level);
				fightSkill->set_back_prop(back_prop);
				fightSkill->set_back_num(back_num);

				fightSkill->set_next_description(next_description);
				fightSkill->set_next_required_level(next_required_level);
				fightSkill->set_next_required_gold(next_required_gold);
				fightSkill->set_next_required_point(next_required_point);
				fightSkill->set_next_required_prop(next_required_prop);
				fightSkill->set_next_required_num(next_required_num);
				fightSkill->set_next_pre_skill(next_pre_skill);
				fightSkill->set_next_pre_skill_level(next_pre_skill_level);

				//std::map<int, CFightSkill*>* fightSkillMap = new std::map<int, CFightSkill*>();
				fightSkillMap->insert(make_pair(fightSkill->level(), fightSkill));

				
			}

			s_data.insert(make_pair(baseSkill->id(),fightSkillMap));
		}
		else {
			CCLOG("query data failed!, the reason is: %d", result);
			CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
		}
		sqlite3_finalize(statement);

	}

	sqlite3_close(pdb); 
}

//////////////////////////////////////////////////////////

std::map<std::string, CFightSkill *> StaticDataMonsterFightSkill::s_data;

void StaticDataMonsterFightSkill::load(const char* configFileName)
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	const char* query_stmt = "SELECT skill_id,scope_clazz from t_monster_skill";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char* skill_idField =(char*)sqlite3_column_text(statement, 0);
			int scope_clazzField=sqlite3_column_int(statement, 1);
			//CCLOG("skill id: %s, scope_clazz: %d", skill_idField, scope_clazzField);

			// create map
			auto fightSkill = new CFightSkill();
			fightSkill->set_id(skill_idField);
			fightSkill->set_targetscope(scope_clazzField);

			s_data.insert(make_pair(fightSkill->id(),fightSkill));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}


//////////////////////////////////////////////////////////

std::map<std::string, SkillBin *> StaticDataSkillBin::s_data;

void StaticDataSkillBin::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	const char *query_stmt = "SELECT id,action,releasePhase,releaseEffect,releaseEffectDir,releaseEffectLayer,releaseEffectPos,releaseEffectToGround,flyingItemEffect,flyingEndedEffect,allow_twiceattack,canKnockBack,attack_time,attack_time_modifier,sound from t_skillbin";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char* skillIdField =(char*)sqlite3_column_text(statement, 0);
			int actionField=sqlite3_column_int(statement, 1);
			int releasePhaseField=sqlite3_column_int(statement, 2);
			char* releaseEffectField=(char*)sqlite3_column_text(statement, 3);
			int releaseEffectDirField=sqlite3_column_int(statement, 4);
			int releaseEffectLayerField=sqlite3_column_int(statement, 5);
			int releaseEffectPosField=sqlite3_column_int(statement, 6);
			int releaseEffectToGroundField=sqlite3_column_int(statement, 7);
			char* flyingItemEffectField=(char*)sqlite3_column_text(statement, 8);
			char* flyingEndedEffectField=(char*)sqlite3_column_text(statement, 9);
			int allowTwiceAttackField=sqlite3_column_int(statement, 10);
			int canKnockBackField=sqlite3_column_int(statement, 11);
			int attack_time=sqlite3_column_int(statement, 12);
			float attack_time_modifier=sqlite3_column_double(statement, 13);
			char* sound = (char*)sqlite3_column_text(statement, 14);
			//CCLOG("skill id: %s, effect: %s", skillIdField, releaseEffectField);

			// create map
			auto skillBin = new SkillBin();
			skillBin->ID = skillIdField;
			skillBin->action = actionField;
			skillBin->releasePhase = releasePhaseField;
			skillBin->releaseEffect = releaseEffectField;
			skillBin->releaseEffectDir = releaseEffectDirField;
			skillBin->releaseEffectLayer = releaseEffectLayerField;
			skillBin->releaseEffectPos = releaseEffectPosField;
			skillBin->releaseEffectToGround = releaseEffectToGroundField;
			skillBin->flyingItemEffect = flyingItemEffectField;
			skillBin->flyingEndedEffect = flyingEndedEffectField;
			skillBin->allow_twiceattack = allowTwiceAttackField == 1 ? true : false;
			skillBin->canKnockBack = canKnockBackField == 1 ? true : false;
			skillBin->attack_time = attack_time;
			skillBin->attack_time_modifier = attack_time_modifier;
			skillBin->sound = sound;
			s_data.insert(make_pair(skillBin->ID,skillBin));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
} 

//////////////////////////////////////////////////////////////

std::map<int,ExStatus *> ExStatusConfigData::s_exstatusData;

void ExStatusConfigData::load( const char* configFileName )
{ 
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// 查询数据
	const char *query_stmt = "SELECT * from t_exstatus_config";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		//通过列来读取数据，获得结果集 statement结果集
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int exstatusTypeField =sqlite3_column_int(statement, 0);
			char* nameField=(char*)sqlite3_column_text(statement, 1);
			char* descriptionField=(char*)sqlite3_column_text(statement, 2);
			char* iconField=(char*)sqlite3_column_text(statement, 3);
			char* effectField=(char*)sqlite3_column_text(statement, 4);
			int footField=sqlite3_column_int(statement, 5);
			float scaleField=sqlite3_column_double(statement, 6);
			int offsetXField=sqlite3_column_int(statement, 7);
			int offsetYField=sqlite3_column_int(statement, 8);
			int effectLoopField=sqlite3_column_int(statement, 9);
			int functionType=sqlite3_column_int(statement, 10);
			//CCLOG("exstatus type: %d, description: %s", exstatusTypeField, descriptionField);

			// create map
			auto exStatus = ExStatusFactory::getInstance()->getExStatus(exstatusTypeField, 0 , 0);
			exStatus->setName(nameField);
			exStatus->setIcon(iconField);
			exStatus->setDescription(descriptionField);
			exStatus->setEffect(effectField);
			exStatus->setFootEffect(footField == 1);
			exStatus->setEffectLoop(effectLoopField == 1);
			exStatus->setFunctionType(functionType);
			s_exstatusData.insert(make_pair(exStatus->type(),exStatus));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
} 

////////////////////////////////////////////////////////////

std::map<int ,CGeneralBaseMsg *> GeneralsConfigData::s_generalsBaseMsgData;

void GeneralsConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data                     SELECT  id,name,description,half_photo,head_photo from t_general"
	const char* query_stmt = "SELECT id,name,description,rare,profession,avatar,half_photo,head_photo from t_general";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int modelid = sqlite3_column_int(statement, 0);
			char* name = (char*)sqlite3_column_text(statement, 1);
			char* description =(char*)sqlite3_column_text(statement, 2);
			int rare = sqlite3_column_int(statement, 3);
			int profession = sqlite3_column_int(statement, 4);
			char* avatar =(char*)sqlite3_column_text(statement, 5);
			char* half_photo =(char*)sqlite3_column_text(statement, 6);
			char* head_photo =(char*)sqlite3_column_text(statement, 7);
			//CCLOG("description id: %s, half_photo: %s,head_photo:%s", description, half_photo,head_photo);

			// create map
			auto generalBaseMsg = new CGeneralBaseMsg();
			generalBaseMsg->set_modelID(modelid);
			generalBaseMsg->set_name(name);
			generalBaseMsg->set_description(description);
			generalBaseMsg->set_rare(rare);
			generalBaseMsg->set_profession(profession);
			generalBaseMsg->set_avatar(avatar);
			generalBaseMsg->set_half_photo(half_photo);
			generalBaseMsg->set_head_photo(head_photo);

			s_generalsBaseMsgData.insert(make_pair(generalBaseMsg->get_modelId(),generalBaseMsg));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

////////////////////////////////////////////////////////////

std::map<int, std::map<int, std::vector<std::string>*>*> GeneralsDialogData::s_generalsDialogData;

// general dialog config
int GeneralsDialogData::s_dialog_stay_duration;
int GeneralsDialogData::s_dialog_interval_duration;
float GeneralsDialogData::s_chance_gossip;
float GeneralsDialogData::s_chance_attack;
float GeneralsDialogData::s_chance_lowhp;
int GeneralsDialogData::s_lowhp_percent;

void GeneralsDialogData::load( const char* configFileName )
{
	int result; 
	std::string path=getDBPath(configFileName);
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	//CCLOG("db path name: %s", path.c_str());

	if(result!=SQLITE_OK) 
		CCLOG("open database failed, number%d",result); 

	for (std::map<int ,CGeneralBaseMsg *>::iterator it = GeneralsConfigData::s_generalsBaseMsgData.begin(); it != GeneralsConfigData::s_generalsBaseMsgData.end(); ++it) 
	{
		auto pGeneralBase = it->second;
		// query data
		char query_stmt[512];
		sprintf(query_stmt, "SELECT id,gossip,attack,lowhp,enter_fight from t_general_dialog where id = '%d'", pGeneralBase->get_modelId());
		sqlite3_stmt *statement;
		result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
		if (result == SQLITE_OK) {
			std::map<int, std::vector<std::string>*>* dialogStringMap = new std::map<int, std::vector<std::string>*>();

			std::vector<std::string>* generalTextVectors[GENERAL_DIAGLOG_CONDITION_MAX];
			for(int i = 0; i < GENERAL_DIAGLOG_CONDITION_MAX; i++)
			{
				generalTextVectors[i] = new std::vector<std::string>();
			}

			while (sqlite3_step(statement) == SQLITE_ROW) {
				int modelid = sqlite3_column_int(statement, 0);
				char* dialog_text[GENERAL_DIAGLOG_CONDITION_MAX];
				for(int i = 0; i < GENERAL_DIAGLOG_CONDITION_MAX; i++)
				{
					dialog_text[i] = (char*)sqlite3_column_text(statement, i+1);
					if(strcmp(dialog_text[i], "null") != 0)
					{
						generalTextVectors[i]->push_back(dialog_text[i]);
					}
				}
			}

			for(int i = 0; i < GENERAL_DIAGLOG_CONDITION_MAX; i++)
			{
				dialogStringMap->insert(make_pair(i, generalTextVectors[i]));
			}

			s_generalsDialogData.insert(make_pair(pGeneralBase->get_modelId(), dialogStringMap));
		}
		else {
			CCLOG("query data failed!, the reason is: %d", result);
			CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
		}
		sqlite3_finalize(statement);
	}

	// load general dialog config
	// query data
	char query_stmt[512];
	sqlite3_stmt *statement;
	sprintf(query_stmt, "SELECT * from t_general_dialog_config");
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			s_dialog_stay_duration = sqlite3_column_int(statement, 0);
			s_dialog_interval_duration = sqlite3_column_int(statement, 1);
			s_chance_gossip = sqlite3_column_double(statement, 2);
			s_chance_attack = sqlite3_column_double(statement, 3);
			s_chance_lowhp = sqlite3_column_double(statement, 4);
			s_lowhp_percent = sqlite3_column_int(statement, 5);
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

////////////////////////////////////////////////////////////

std::map<GeneralEvolutionConfigData::ProAndRare,CGeneralsEvolution *> GeneralEvolutionConfigData::s_generalEvolution;

void GeneralEvolutionConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data                     SELECT  id,name,description,half_photo,head_photo from t_general"
	const char* query_stmt = "SELECT profession,rare,level,strength,dexterity,intelligence,focus,need_card,cost from t_general_evolution_growingup";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int profession =sqlite3_column_int(statement, 0);
			int rare=sqlite3_column_int(statement, 1);
			int evolution=sqlite3_column_int(statement, 2);
			int strength=sqlite3_column_int(statement, 3);
			int dexterity=sqlite3_column_int(statement, 4);
			int intelligence=sqlite3_column_int(statement, 5);
			int focus =sqlite3_column_int(statement, 6);
			int need_card=sqlite3_column_int(statement, 7);
			int cost=sqlite3_column_int(statement, 8);
			//CCLOG("skill id: %s, description: %s", baseSkill->id().c_str(), descriptionField);

			// create map
			ProAndRare  key;
			key.profession = profession;
			key.rare = rare;
			key.evolution = evolution;

			auto generalsEvolution = new CGeneralsEvolution();
			generalsEvolution->set_profession(profession);
			generalsEvolution->set_rare(rare);
			generalsEvolution->set_evolution(evolution);
			generalsEvolution->set_strenth(strength);
			generalsEvolution->set_dexterity(dexterity);
			generalsEvolution->set_intelligence(intelligence);
			generalsEvolution->set_focus(focus);
			generalsEvolution->set_need_card(need_card);
			generalsEvolution->set_cost(cost);

			s_generalEvolution.insert(make_pair(key, generalsEvolution));

		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}


////////////////////////////////////////////////////////////

std::map<int,CGeneralsTeach *> GeneralTeachConfigData::s_generalTeach;

void GeneralTeachConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char* query_stmt = "SELECT quality_level,need_exp from t_general_quality_growingup";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int quality_level =sqlite3_column_int(statement, 0);
			int need_exp=sqlite3_column_int(statement, 1);
			//CCLOG("quality_level: %d, need_exp: %s", quality_level, need_exp);

			// create map
			auto generalsTeach = new CGeneralsTeach();
			generalsTeach->set_quality_level(quality_level);
			generalsTeach->set_need_exp(need_exp);

			s_generalTeach.insert(make_pair(quality_level, generalsTeach));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}


////////////////////////////////////////////////////////////

std::vector<CFivePersonInstance*> FivePersonInstanceConfigData::s_fivePersonInstance;

bool SortByLevel(CFivePersonInstance * instance1 , CFivePersonInstance *instance2)
{
	if (instance1->get_open_level() < instance2->get_open_level())
	{
		return true;
	}
	else if (instance1->get_open_level() == instance2->get_open_level())
	{
		return instance1->get_difficult() < instance2->get_difficult();
	}

	return false;
}

void FivePersonInstanceConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char* query_stmt = "SELECT id,keyValue,name,difficult,open_level,day_time,suggest_num,start_time,end_time,consume,map_id,enter_x,enter_y,reward_xp,reward_gold,reward_gold_type,description,follow_map,fight_point,picture,dragon_value,boss_count,monster_count from t_copy";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int id =sqlite3_column_int(statement, 0);
			char * keyvalue = (char *)sqlite3_column_text(statement,1);
			char * name = (char *)sqlite3_column_text(statement,2);
			int difficult=sqlite3_column_int(statement, 3);
			int open_level = sqlite3_column_int(statement,4);
			int day_time=sqlite3_column_int(statement,5);
			int suggest_num = sqlite3_column_int(statement,6);
			char * start_time = (char *)sqlite3_column_text(statement,7);
			char * end_time = (char *)sqlite3_column_text(statement,8);
			int consume = sqlite3_column_int(statement,9);
			char * map_id = (char *)sqlite3_column_text(statement,10);
			int enter_x=sqlite3_column_int(statement, 12);
			int enter_y = sqlite3_column_int(statement,12);
			int reward_xp=sqlite3_column_int(statement,13);
			int reward_gold=sqlite3_column_int(statement,14);
			int reward_gold_type=sqlite3_column_int(statement,15);
			char * description = (char *)sqlite3_column_text(statement,16);
			char * follow_map = (char *)sqlite3_column_text(statement,17);
			int fight_point = sqlite3_column_int(statement,18);
			char * picture = (char *)sqlite3_column_text(statement,19);
			int dragon_value = sqlite3_column_int(statement,20);
			int boss_count = sqlite3_column_int(statement,21);
			int monster_count = sqlite3_column_int(statement,22);
			//CCLOG("quality_level: %d, need_exp: %s", quality_level, need_exp);

			// create map
			auto fivePersonInstance = new CFivePersonInstance();
			fivePersonInstance->set_id(id);
			fivePersonInstance->set_keyValue(keyvalue);
			fivePersonInstance->set_name(name);
			fivePersonInstance->set_difficult(difficult);
			fivePersonInstance->set_open_level(open_level);
			fivePersonInstance->set_day_time(day_time);
			fivePersonInstance->set_suggest_num(suggest_num);
			fivePersonInstance->set_start_time(start_time);
			fivePersonInstance->set_end_time(end_time);
			fivePersonInstance->set_consume(consume);
			fivePersonInstance->set_map_id(map_id);
			fivePersonInstance->set_enter_x(enter_x);
			fivePersonInstance->set_enter_y(enter_y);
			fivePersonInstance->set_reward_xp(reward_xp);
			fivePersonInstance->set_reward_gold(reward_gold);
			fivePersonInstance->set_reward_gold_type(reward_gold_type);
			fivePersonInstance->set_description(description);
			fivePersonInstance->set_follow_map(follow_map);
			fivePersonInstance->set_fight_point(fight_point);
			fivePersonInstance->set_picture(picture);
			fivePersonInstance->set_dragon_value(dragon_value);
			fivePersonInstance->set_boss_count(boss_count);
			fivePersonInstance->set_monster_count(monster_count);

			s_fivePersonInstance.push_back(fivePersonInstance);
		}

		sort(s_fivePersonInstance.begin(),s_fivePersonInstance.end(),SortByLevel);
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}


////////////////////////////////////////////////////////////

std::vector<CFightWayBase*> FightWayConfigData::s_fightWayBase;

void FightWayConfigData::load( const char* configFileName )
{  
	int result; 
 
	std::string path=getDBPath(configFileName);

	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char* query_stmt = "SELECT id,name,keyValue,icon,description,nu_order from t_fightway";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int id = sqlite3_column_int(statement,0);
			char * name = (char *)sqlite3_column_text(statement,1);
			char * keyValue = (char *)sqlite3_column_text(statement,2);
			char * icon = (char *)sqlite3_column_text(statement,3);
			char * description = (char *)sqlite3_column_text(statement,4);
			int sort_order = sqlite3_column_int(statement,5);
			//CCLOG("quality_level: %d, need_exp: %s", quality_level, need_exp);
			// create map
			auto fightwayBase = new CFightWayBase();
			fightwayBase->set_fightwayid(id);
			fightwayBase->set_name(name);
			fightwayBase->set_kValue(keyValue);
			fightwayBase->set_icon(icon);
			fightwayBase->set_description(description);
			fightwayBase->set_level(0);
			fightwayBase->set_activeflag(0);
			fightwayBase->set_sortingOrder(sort_order);

			s_fightWayBase.push_back(fightwayBase);
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

//////////////////////////////////////////////////////////

std::map<int, std::map<int, CFightWayGrowingUp*>*> FightWayGrowingUpConfigData::s_fightWayGrowingUp;

void FightWayGrowingUpConfigData::load(const char* configFileName)
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// according base skill info
	for (unsigned int i = 0;i<FightWayConfigData::s_fightWayBase.size();++i) 
	{
		auto fightWayBase = FightWayConfigData::s_fightWayBase.at(i);

		// query data
		char query_stmt[512];
		sprintf(query_stmt, "SELECT id,fightway_id,level,required_experience,grid1_property,grid1_value,grid2_property,grid2_value,grid3_property,grid3_value,grid4_property,grid4_value,grid5_property,grid5_value,complex_property,complex_value,required_gold from t_fightway_growingup where fightway_id = '%d'", fightWayBase->fightwayid());
		sqlite3_stmt *statement;
		result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
		if (result == SQLITE_OK) {
			std::map<int, CFightWayGrowingUp*>* fightWayGrowingUpMap = new std::map<int, CFightWayGrowingUp*>();
			while (sqlite3_step(statement) == SQLITE_ROW) {
				int id =sqlite3_column_int(statement, 0);
				int fightway_id =sqlite3_column_int(statement, 1);
				int level =sqlite3_column_int(statement, 2);
				int required_experience=sqlite3_column_int(statement,3);
				int grid1_property=sqlite3_column_int(statement, 4);
				float grid1_value=sqlite3_column_double(statement, 5);
				int grid2_property=sqlite3_column_int(statement, 6);
				float grid2_value=sqlite3_column_double(statement, 7);
				int grid3_property=sqlite3_column_int(statement, 8);
				float grid3_value=sqlite3_column_double(statement,9);
				int grid4_property=sqlite3_column_int(statement, 10);
				float grid4_value=sqlite3_column_double(statement, 11);
				int grid5_property=sqlite3_column_int(statement, 12);
				float grid5_value=sqlite3_column_double(statement, 13);
				int complex_property=sqlite3_column_int(statement, 14);
				float complex_value=sqlite3_column_double(statement, 15);
				int required_gold=sqlite3_column_double(statement, 16);

				// create map
				auto fightwayGrowingUp = new CFightWayGrowingUp();
				fightwayGrowingUp->set_id(id);
				fightwayGrowingUp->set_fightway_id(fightway_id);
				fightwayGrowingUp->set_level(level);
				fightwayGrowingUp->set_required_experience(required_experience);
				fightwayGrowingUp->set_grid1_property(grid1_property);
				fightwayGrowingUp->set_grid1_value(grid1_value);
				fightwayGrowingUp->set_grid2_property(grid2_property);
				fightwayGrowingUp->set_grid2_value(grid2_value);
				fightwayGrowingUp->set_grid3_property(grid3_property);
				fightwayGrowingUp->set_grid3_value(grid3_value);
				fightwayGrowingUp->set_grid4_property(grid4_property);
				fightwayGrowingUp->set_grid4_value(grid4_value);
				fightwayGrowingUp->set_grid5_property(grid5_property);
				fightwayGrowingUp->set_grid5_value(grid5_value);
				fightwayGrowingUp->set_complex_property(complex_property);
				fightwayGrowingUp->set_complex_value(complex_value);
				fightwayGrowingUp->set_required_gold(required_gold);

				fightWayGrowingUpMap->insert(make_pair(fightwayGrowingUp->get_level(), fightwayGrowingUp));
			}

			s_fightWayGrowingUp.insert(make_pair(fightWayBase->fightwayid(),fightWayGrowingUpMap));
		}
		else {
			CCLOG("query data failed!, the reason is: %d", result);
			CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
		}
		sqlite3_finalize(statement);

	}

	sqlite3_close(pdb); 
}

////////////////////////////////////////////////////////////

std::map<std::string,CFateBaseMsg *> FateBaseMsgConfigData::s_fateBaseMsg;

void FateBaseMsgConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char* query_stmt = "SELECT fateId,fateName,fateType,property,valueType,addValue,extendId1,extendId2,extendId3,extendId4,extendId5,description from t_fate";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char * fateid = (char *)sqlite3_column_text(statement,0);
			char * fatename = (char *)sqlite3_column_text(statement,1);
			int fatetype = sqlite3_column_int(statement,2);
			int property = sqlite3_column_int(statement,3);
			int valuetype = sqlite3_column_int(statement,4);
			float addvalue=sqlite3_column_double(statement, 5);
			char * extendld1 = (char *)sqlite3_column_text(statement,6);
			char * extendld2 = (char *)sqlite3_column_text(statement,7);
			char * extendld3 = (char *)sqlite3_column_text(statement,8);
			char * extendld4 = (char *)sqlite3_column_text(statement,9);
			char * extendld5 = (char *)sqlite3_column_text(statement,10);
			char * description = (char *)sqlite3_column_text(statement,11);

			//CCLOG("quality_level: %d, need_exp: %s", quality_level, need_exp);

			// create map
			auto fateBaseMsg = new CFateBaseMsg();
			fateBaseMsg->set_fateid(fateid);
			fateBaseMsg->set_fatename(fatename);
			fateBaseMsg->set_fatetype(fatetype);
			fateBaseMsg->set_property(property);
			fateBaseMsg->set_valuetype(valuetype);
			fateBaseMsg->set_addvalue(addvalue);
			fateBaseMsg->set_extendld1(extendld1);
			fateBaseMsg->set_extendld2(extendld2);
			fateBaseMsg->set_extendld3(extendld3);
			fateBaseMsg->set_extendld4(extendld4);
			fateBaseMsg->set_extendld5(extendld5);
			fateBaseMsg->set_description(description);

			s_fateBaseMsg.insert(make_pair(fateid, fateBaseMsg));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}


////////////////////////////////////////////////////////////

std::map<std::string,CDrug *> CDrugMsgConfigData::s_drugBaseMsg;

void CDrugMsgConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char* query_stmt = "SELECT id,prop_id,hp,mp,once_hp,once_mp,once_hp_upper,once_mp_upper,type_flag,cool_time from t_prop_drug";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int id = sqlite3_column_int(statement,0);
			char * prop_id = (char *)sqlite3_column_text(statement,1);
			int hp = sqlite3_column_int(statement,2);
			int mp = sqlite3_column_int(statement,3);
			int once_hp = sqlite3_column_int(statement,4);
			int once_mp = sqlite3_column_int(statement, 5);
			int once_hp_upper = sqlite3_column_int(statement,6);
			int once_mp_upper = sqlite3_column_int(statement, 7);
			int type_flag = sqlite3_column_int(statement, 8);
			int cool_time = sqlite3_column_int(statement, 9);
			//CCLOG("quality_level: %d, need_exp: %s", quality_level, need_exp);

			// create map
			auto drug = new CDrug();
			drug->set_id(id);
			drug->set_hp(hp);
			drug->set_mp(mp);
			drug->set_once_hp(once_hp);
			drug->set_once_mp(once_mp);
			drug->set_once_hp_upper(once_hp_upper);
			drug->set_once_mp_upper(once_mp_upper);
			drug->set_type_flag(type_flag);
			drug->set_cool_time(cool_time);

			s_drugBaseMsg.insert(make_pair(prop_id, drug));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}


////////////////////////////////////////////////////////////

__Dictionary* StringDataManager::s_stringsData = NULL;

void StringDataManager::load()
{
	s_stringsData = __Dictionary::createWithContentsOfFile("res_ui/font/strings.xml");
	s_stringsData->retain();
}

const char* StringDataManager::getString(const char* stringId)
{
	const char *str  = ((__String*)s_stringsData->objectForKey(stringId))->getCString();
	return str;
}

//////////////////////////////////////////////////////
std::map<int, std::map<int,MapRobotDrug*>*> RobotDrugConfigData::s_RobotDrugConfig;

void RobotDrugConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	for (int i = 1;i<5;++i)
	{
		// query data
		char query_stmt[512];
		sprintf(query_stmt,"SELECT id,name,level,type,target,icon from t_robotDrug_config where type = '%d'",i);
		sqlite3_stmt *statement;
		result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
		if (result == SQLITE_OK) {
			std::map<int, MapRobotDrug*>* robotDrugMap = new std::map<int, MapRobotDrug*>();
			while (sqlite3_step(statement) == SQLITE_ROW) {
				char* drugId_ =(char*)sqlite3_column_text(statement, 0);
				char* drugName_=(char*)sqlite3_column_text(statement,1);
				int level_=sqlite3_column_int(statement,2);
				int type_=sqlite3_column_int(statement, 3);
				int target=sqlite3_column_int(statement,4);
				char* icon_ = (char*)sqlite3_column_text(statement, 5);
				// create MapRobotDrug
				auto robotDrug = new MapRobotDrug();
				robotDrug->set_id(drugId_);
				robotDrug->set_name(drugName_);
				robotDrug->set_level(level_);
				robotDrug->set_type(type_);
				robotDrug->set_target(target);
				robotDrug->set_icon(icon_);
				robotDrugMap->insert(make_pair(level_,robotDrug));
			}
			s_RobotDrugConfig.insert(make_pair(i,robotDrugMap));
		}
		else {
			CCLOG("query data failed!, the reason is: %d", result);
			CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
		}

	sqlite3_finalize(statement);

	}

	sqlite3_close(pdb);
}

//////////////////////////////////////////////////////
std::map<std::string, std::string> StaticDataMapName::s_mapname;

/////
std::map<std::string,MapStrngthStone*> StrengthStoneConfigData::s_StrengthStrone;

void StrengthStoneConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char* query_stmt = "SELECT exp,prop_id from t_prop_strengthen_stone";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int exp_ = sqlite3_column_int(statement,0);
			char * propId_ = (char *)sqlite3_column_text(statement,1);
			// create map
			auto strengthStone = new MapStrngthStone();
			strengthStone->set_exp(exp_);
			strengthStone->set_id(propId_);

			s_StrengthStrone.insert(make_pair(propId_,strengthStone));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}




void StaticDataMapName::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	const char *query_stmt = "SELECT id,map_name from t_map_property";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char* mapIdField = (char*)sqlite3_column_text(statement, 0);
			char* mapName = (char*)sqlite3_column_text(statement, 1);
			//CCLOG("skill id: %s, name: : %s", skillIdField, nameField);

			s_mapname.insert(make_pair(mapIdField,mapName));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}


//////////////////////////////////////////////////////
std::map<int, CMonsterBaseInfo*> StaticDataMonsterBaseInfo::s_monsterBase;

void StaticDataMonsterBaseInfo::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	const char *query_stmt = "SELECT id,name,clazz,level from t_monster";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int id = sqlite3_column_int(statement, 0);
			char* monsterName = (char*)sqlite3_column_text(statement, 1);
			int clazz = sqlite3_column_int(statement, 2);
			int level = sqlite3_column_int(statement, 3);
			//CCLOG("skill id: %s, name: : %s", skillIdField, nameField);

			auto pMonsterBase = new CMonsterBaseInfo();
			pMonsterBase->id = id;
			pMonsterBase->name = monsterName;
			pMonsterBase->clazz = clazz;
			pMonsterBase->level = level;

			s_monsterBase.insert(make_pair(id, pMonsterBase));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}



//////////////////////////////////////////////////////
std::vector<CFunctionOpenLevel*> FunctionOpenLevelConfigData::s_functionOpenLevelMsg;

void FunctionOpenLevelConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	const char* query_stmt = "SELECT functionid,functionname,functiontype,requiredlevel,postionindex from t_function_openlevel";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char * functionid = (char *)sqlite3_column_text(statement,0);
			char * functionname = (char *)sqlite3_column_text(statement,1);
			int functiontype = sqlite3_column_int(statement,2);
			int requiredlevel = sqlite3_column_int(statement,3);
			int postionindex = sqlite3_column_int(statement,4);

			auto functionOpenLevel = new CFunctionOpenLevel();
			functionOpenLevel->set_functionId(functionid);
			functionOpenLevel->set_functionName(functionname);
			functionOpenLevel->set_functionType(functiontype);
			functionOpenLevel->set_requiredLevel(requiredlevel);
			functionOpenLevel->set_positionIndex(postionindex);

			s_functionOpenLevelMsg.push_back(functionOpenLevel);
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);

	sqlite3_close(pdb);
}


//////////////////////////////////////////////////////
std::map<int,int> ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg;

void ShortcutSlotOpenLevelConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	const char* query_stmt = "SELECT * from t_shortcutslot_openlevel";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int index = sqlite3_column_int(statement,0);
			int openlevel = sqlite3_column_int(statement,1);

			s_shortcutSlotOpenLevelMsg.insert(make_pair(index,openlevel));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);

	sqlite3_close(pdb);
}


//////////////////////////////////////////////////////
std::map<MusouTalentConfigData::IdAndLevel,CMusouTalent*> MusouTalentConfigData::s_musouTalentMsg;

void MusouTalentConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	const char* query_stmt = "SELECT * from t_dragon_talent";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int id = sqlite3_column_int(statement,0);
			int clazz = sqlite3_column_int(statement,1);
			int idx = sqlite3_column_int(statement,2);
			int level = sqlite3_column_int(statement,3);
			char * name = (char *)sqlite3_column_text(statement,4);
			char * description = (char *)sqlite3_column_text(statement,5);
			int require_level = sqlite3_column_int(statement,6);
			int require_essence = sqlite3_column_int(statement,7);
			char * icon = (char *)sqlite3_column_text(statement,10);

			IdAndLevel t;
			t.idx = idx;
			t.level = level;

			auto musouTalent = new CMusouTalent();
			musouTalent->set_id(id);
			musouTalent->set_level(level);
			musouTalent->set_clazz(clazz);
			musouTalent->set_idx(idx);
			musouTalent->set_name(name);
			musouTalent->set_description(description);
			musouTalent->set_require_level(require_level);
			musouTalent->set_require_essence(require_essence);
			musouTalent->set_icon(icon);

			s_musouTalentMsg.insert(make_pair(t,musouTalent));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);

	sqlite3_close(pdb);
}


//////////////////////////////////////////////////////
std::vector<std::string> LoadingTextData::s_LoadingTexts;

void LoadingTextData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	const char* query_stmt = "SELECT text from t_loading_text";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char * text = (char *)sqlite3_column_text(statement,0);

			s_LoadingTexts.push_back(text);
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);

	sqlite3_close(pdb);
}
//////////////////////////////////////
std::map<int ,MapEquipAddExp*> StrengthEquipmentConfigData::s_EquipUpLevelExp;
void StrengthEquipmentConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char* query_stmt = "SELECT level,exp from t_equip_strengthen_cfg";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int level_ = sqlite3_column_int(statement,0);
			int exp_ = sqlite3_column_int(statement,1);
			// create map
			auto equipExp = new MapEquipAddExp();
			equipExp->set_level(level_);
			equipExp->set_exp(exp_);

			s_EquipUpLevelExp.insert(make_pair(level_,equipExp));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}
//////////////////////////////////////
std::map<int ,MapStarPickAmount*> StrengthStarConfigData::s_EquipStarAmount;

void StrengthStarConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char* query_stmt = "SELECT level,count from t_equip_removestar_cfg";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int level_ = sqlite3_column_int(statement,0);
			int count_ = sqlite3_column_int(statement,1);
			// create map
			auto starPick = new MapStarPickAmount();
			starPick->set_level(level_);
			starPick->set_count(count_);

			s_EquipStarAmount.insert(make_pair(level_,starPick));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}
///////////////////////////////////////////////////
std::map<std::string ,MapStrengthGem*> StrengthGemConfigData::s_EquipSGemAmount;
void StrengthGemConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char* query_stmt = "SELECT prop_id,gemlevel from t_prop_inlay_material";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char * propId =(char *) sqlite3_column_text(statement,0);
			int level_ = sqlite3_column_int(statement,1);
			// create map
			auto gemLevel = new MapStrengthGem();
			gemLevel->set_id(propId);
			gemLevel->set_gemLevel(level_);

			s_EquipSGemAmount.insert(make_pair(propId,gemLevel));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

/////////////////////////////////////////
std::map<int ,MapMarquee*> MarqueeStringConfigData::s_MarqueeStrings;

void MarqueeStringConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char* query_stmt = "SELECT id,message,priority from t_marquee_message";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int id_ = sqlite3_column_int(statement,0);
			char * message_ =(char *) sqlite3_column_text(statement,1);
			int prioity_ = sqlite3_column_int(statement,2);
			// create map
			auto marquees_ = new MapMarquee();
			marquees_->set_id(id_);
			marquees_->set_message(message_);
			marquees_->set_prioity(prioity_);
			s_MarqueeStrings.insert(make_pair(id_,marquees_));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

//////////////////////////
std::map<std::string ,MapRobotPotionLimit*> RobotPotionMilitData::s_RobotPotionMilit;

void RobotPotionMilitData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char* query_stmt = "SELECT id,level,price from t_robotPotionLimit_cfg";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char * id_ = (char *)sqlite3_column_text(statement,0);
			int level_ = sqlite3_column_int(statement,1);
			int price_ = sqlite3_column_int(statement,2);
			// create map
			MapRobotPotionLimit * potionLimit = new MapRobotPotionLimit();
			potionLimit->set_id(id_);
			potionLimit->set_level(level_);
			potionLimit->set_price(price_);

			s_RobotPotionMilit.insert(make_pair(id_,potionLimit));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}
////////////////////////////////////////////////
std::map<int, std::map<int,MapRefineDiffAddValue*>*> EquipRefineAddValueConfigData::s_EquipRefineValue;

void EquipRefineAddValueConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	int refineLevelAmount = 20;//refinelevel is max 20
	for (int refineLevel = 1;refineLevel<= refineLevelAmount;refineLevel++)
	{
		char query_stmt[512];
		sprintf(query_stmt,"SELECT level,exp,clazz,min_attack,max_attack,min_magic_attack,max_magic_attack,min_defend,max_defend,min_magic_defend,max_magic_defend,hp,mp from t_equip_strengthen_cfg where level = '%d'",refineLevel);
		sqlite3_stmt *statement;
		result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);

		if (result == SQLITE_OK) 
		{
			std::map<int, MapRefineDiffAddValue*>* mapValue = new std::map<int, MapRefineDiffAddValue*>();
			while (sqlite3_step(statement) == SQLITE_ROW) 
			{
				int level_ = sqlite3_column_int(statement,0);
				int exp_ = sqlite3_column_int(statement,1);
				int clazz_ = sqlite3_column_int(statement,2);
				int minattack_ = sqlite3_column_int(statement,3);
				int maxattack_ = sqlite3_column_int(statement,4);
				int minmagicattack_ =sqlite3_column_int(statement,5);
				int maxmagicattack_ =sqlite3_column_int(statement,6);
				int mindefend_ =sqlite3_column_int(statement,7);
				int maxdefend_ =sqlite3_column_int(statement,8);
				int minmagicdefend_ =sqlite3_column_int(statement,9);
				int maxmagicdefend_ =sqlite3_column_int(statement,10);
				int hp_ =sqlite3_column_int(statement,11);
				int mp_ =sqlite3_column_int(statement,12);
				// create map
				MapRefineDiffAddValue * equipAddValue = new MapRefineDiffAddValue();
				equipAddValue->set_level(level_);
				equipAddValue->set_exp(exp_);
				equipAddValue->set_clazz(clazz_);
				equipAddValue->set_minAttack(minattack_);
				equipAddValue->set_maxAttack(maxattack_);
				equipAddValue->set_minmagic_Attack(minmagicattack_);
				equipAddValue->set_maxmagic_Attack(maxmagicattack_);
				equipAddValue->set_min_define(mindefend_);
				equipAddValue->set_max_define(maxdefend_);
				equipAddValue->set_min_magic_define(minmagicdefend_);
				equipAddValue->set_max_magic_define(maxmagicdefend_);
				equipAddValue->set_hp(hp_);
				equipAddValue->set_mp(mp_);

				mapValue->insert(make_pair(clazz_,equipAddValue));
			}
			s_EquipRefineValue.insert(make_pair(refineLevel,mapValue));	
		}else 
		{
			CCLOG("query data failed!, the reason is: %d", result);
			CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
		}
	sqlite3_finalize(statement);
	}
	sqlite3_close(pdb); 
}

std::vector<CAdditionProperty*> EquipRefineAddValueConfigData::getAddvalueByDiffClazzAndLevel( int clazz,int level )
{
	auto mapVaule = s_EquipRefineValue[level]->at(clazz);
	 
	std::vector<CAdditionProperty*> AdditionPropertyList;

	switch(clazz)
	{
	case weapon:
		{
			auto temp1 = new CAdditionProperty();
			temp1->set_property(14);
			temp1->add_value(0);
			temp1->add_value(mapVaule->get_maxAttack());
			AdditionPropertyList.push_back(temp1);

			auto temp2 = new CAdditionProperty();
			temp2->set_property(16);
			temp2->add_value(0);
			temp2->add_value(mapVaule->get_maxmagicAttack());
			AdditionPropertyList.push_back(temp2);

			auto temp3 = new CAdditionProperty();
			temp3->set_property(11);
			temp3->add_value(0);
			temp3->add_value(mapVaule->get_hp());
			AdditionPropertyList.push_back(temp3);
		}break;
	case dress:
		{
			auto temp1 = new CAdditionProperty();
			temp1->set_property(18);
			temp1->add_value(0);
			temp1->add_value(mapVaule->get_max_define());
			AdditionPropertyList.push_back(temp1);

			auto temp2 = new CAdditionProperty();
			temp2->set_property(20);
			temp2->add_value(0);
			temp2->add_value(mapVaule->get_max_magic_define());
			AdditionPropertyList.push_back(temp2);
		}break;
	case cap:
		{
			auto temp1 = new CAdditionProperty();
			temp1->set_property(17);
			temp1->add_value(0);
			temp1->add_value(mapVaule->get_min_define());
			AdditionPropertyList.push_back(temp1);

			auto temp3 = new CAdditionProperty();
			temp3->set_property(11);
			temp3->add_value(0);
			temp3->add_value(mapVaule->get_hp());
			AdditionPropertyList.push_back(temp3);
		}break;
	case shoes:
		{
			auto temp1 = new CAdditionProperty();
			temp1->set_property(19);
			temp1->add_value(0);
			temp1->add_value(mapVaule->get_min_magic_define());
			AdditionPropertyList.push_back(temp1);

			auto temp3 = new CAdditionProperty();
			temp3->set_property(11);
			temp3->add_value(0);
			temp3->add_value(mapVaule->get_hp());
			AdditionPropertyList.push_back(temp3);
		}break;
	case necklace:
		{
			auto temp1 = new CAdditionProperty();
			temp1->set_property(13);
			temp1->add_value(0);
			temp1->add_value(mapVaule->get_minAttack());
			AdditionPropertyList.push_back(temp1);

			auto temp2 = new CAdditionProperty();
			temp2->set_property(15);
			temp2->add_value(0);
			temp2->add_value(mapVaule->get_minmagicAttack());
			AdditionPropertyList.push_back(temp2);

			auto temp3 = new CAdditionProperty();
			temp3->set_property(12);
			temp3->add_value(0);
			temp3->add_value(mapVaule->get_mp());
			AdditionPropertyList.push_back(temp3);
		}break;
	case belt:
		{
			auto temp1 = new CAdditionProperty();
			temp1->set_property(13);
			temp1->add_value(0);
			temp1->add_value(mapVaule->get_minAttack());
			AdditionPropertyList.push_back(temp1);

			auto temp2 = new CAdditionProperty();
			temp2->set_property(15);
			temp2->add_value(0);
			temp2->add_value(mapVaule->get_minmagicAttack());
			AdditionPropertyList.push_back(temp2);

			auto temp3 = new CAdditionProperty();
			temp3->set_property(12);
			temp3->add_value(0);
			temp3->add_value(mapVaule->get_mp());
			AdditionPropertyList.push_back(temp3);
		}break;
	}
	return AdditionPropertyList;
}

/////////////////////////////////////////////////////////////////
std::map<EquipRefineFactorConfigData::useLevelAndQuality, MapRefineValueFactor*> EquipRefineFactorConfigData::s_EquipRefineValueFactor;

void EquipRefineFactorConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 
	const char* query_stmt = "SELECT level,quality,factor from t_equip_strengthen_factor";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int level_ = sqlite3_column_int(statement,0);
			int quality_ = sqlite3_column_int(statement,1);
			float factor_ =sqlite3_column_double(statement,2);

			useLevelAndQuality temp;
			temp.useLevel = level_;
			temp.quality = quality_;
			// create map
			auto equipFactor = new MapRefineValueFactor();
			equipFactor->set_level(level_);
			equipFactor->set_quality(quality_);
			equipFactor->set_factor(factor_);

			s_EquipRefineValueFactor.insert(make_pair(temp,equipFactor));
		}
	}else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

/////////////////////////////////////////////////////////////////////

std::map<EquipRefineEffectConfigData::weapEffect,std::string> EquipRefineEffectConfigData::s_EquipmentRefineEffect;

void EquipRefineEffectConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 
	const char* query_stmt = "SELECT priority,weapProfession,refineLevel,weapRefineEffect from t_weapEffectRefine_cfg";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int priority_ = sqlite3_column_int(statement,0);
			int weapPression_ = sqlite3_column_int(statement,1);
			int refineLevel_ = sqlite3_column_int(statement,2);
			char * refineEffect_ =(char*) sqlite3_column_text(statement,3);

			// create map
			auto equipEffect = new MapWeapEffect();
			equipEffect->set_priority(priority_);
			equipEffect->set_weapPression(weapPression_);
			equipEffect->set_RefineLevel(refineLevel_);
			equipEffect->set_weapEffectRefine(refineEffect_);

			weapEffect temp;
			temp.weapPression = weapPression_;
			temp.refineLevel = refineLevel_;
			s_EquipmentRefineEffect.insert(make_pair(temp,refineEffect_));
		}
	}else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}
////////////////////////////////////////////////////////////////
std::map<EquipStarEffectConfigData::weapEffect,std::string> EquipStarEffectConfigData::s_EquipmentStarEffect;

void EquipStarEffectConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 
	const char* query_stmt = "SELECT priority,weapProfession,starLevel,weapStarEffect from t_weapEffectStar_cfg";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int priority_ = sqlite3_column_int(statement,0);
			int weapPression_ = sqlite3_column_int(statement,1);
			int starLevel_ = sqlite3_column_int(statement,2);
			char * starEffect_ =(char*) sqlite3_column_text(statement,3);

			// create map
			auto equipEffect = new MapWeapEffect();
			equipEffect->set_priority(priority_);
			equipEffect->set_weapPression(weapPression_);
			equipEffect->set_RefineLevel(starLevel_);
			equipEffect->set_weapEffectRefine(starEffect_);

			weapEffect temp;
			temp.weapPression = weapPression_;
			temp.starLevel = starLevel_;
			s_EquipmentStarEffect.insert(make_pair(temp,starEffect_));
		}
	}else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

/////////////////////////////////////////////////////////////////
std::map<int, std::vector<CNewFunctionRemind*> > NewFunctionRemindConfigData::s_newFunctionRemindList;

void NewFunctionRemindConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	for(int i = 0;i<50;++i)
	{
		char query_stmt[512];
		sprintf(query_stmt, "SELECT * from t_newfunction_remind where level = '%d'", i);
		sqlite3_stmt *statement;
		result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
		if (result == SQLITE_OK) 
		{
			std::vector<CNewFunctionRemind*> _list;
			while (sqlite3_step(statement) == SQLITE_ROW) 
			{
				int id = sqlite3_column_int(statement,0);
				int level = sqlite3_column_int(statement,1);
				char* des=(char*)sqlite3_column_text(statement, 2);
				char* icon=(char*)sqlite3_column_text(statement, 3);

				auto newFunctionRemind = new CNewFunctionRemind();
				newFunctionRemind->setId(id);
				newFunctionRemind->setLevel(level);
				newFunctionRemind->setDes(des);
				newFunctionRemind->setIcon(icon);
				_list.push_back(newFunctionRemind);
			}
			if(_list.size()>0)
			{
				s_newFunctionRemindList.insert(make_pair(i,_list));
			}
		}
		else 
		{
			CCLOG("query data failed!, the reason is: %d", result);
			CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
		}

		sqlite3_finalize(statement);
	}
	sqlite3_close(pdb); 
}


/////////////////////////////////////////////////////////////////
std::map<int, std::string> SystemInfoConfigData::s_systemInfoList;

void SystemInfoConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char*  query_stmt = "SELECT id,description from t_systeminfo";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int id = sqlite3_column_int(statement,0);
			char* des=(char*)sqlite3_column_text(statement, 1);
			s_systemInfoList.insert(make_pair(id,des));
		}	
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);
	sqlite3_close(pdb); 
}


/////////////////////////////////////////////////////////////////
std::map<int, int> BtnOpenLevelConfigData::s_btnOpenLevel;

void BtnOpenLevelConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char*  query_stmt = "SELECT id,level from t_btn_openlevel";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int id = sqlite3_column_int(statement,0);
			int level = sqlite3_column_int(statement, 1);
			s_btnOpenLevel.insert(make_pair(id,level));
		}	
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);
	sqlite3_close(pdb); 
}



/////////////////////////////////////////////////////////////////
std::map<VipConfigData::TypeIdAndVipLevel,CVipInfo*> VipConfigData::s_vipConfig;

void VipConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char*  query_stmt = "SELECT * from t_vip";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int vip_level = sqlite3_column_int(statement,0);
			char* type=(char*)sqlite3_column_text(statement, 1);
			int type_id = sqlite3_column_int(statement,2);
			int add_type = sqlite3_column_int(statement, 3);
			int add_number = sqlite3_column_int(statement, 4);

			TypeIdAndVipLevel tempKey;
			tempKey.typeName = type;
			tempKey.vipLevel = vip_level;

			auto tempData = new CVipInfo();
			tempData->set_vip_level(vip_level);
			tempData->set_type(type);
			tempData->set_type_id(type_id);
			tempData->set_add_type(add_type);
			tempData->set_add_number(add_number);

			s_vipConfig.insert(make_pair(tempKey,tempData));
		}	
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);
	sqlite3_close(pdb); 
}



/////////////////////////////////////////////////////////////////
std::map<int,std::string> VipDescriptionConfig::s_vipDes;

void VipDescriptionConfig::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char*  query_stmt = "SELECT * from t_vip_description";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int vip_level = sqlite3_column_int(statement,0);
			char* des=(char*)sqlite3_column_text(statement, 1);

			s_vipDes.insert(make_pair(vip_level,des));
		}	
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);
	sqlite3_close(pdb); 
}


/////////////////////////////////////////////////////////////////
std::map<int, CCollectInfo*> CollectInfoConfig::s_collectInfo;

void CollectInfoConfig::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char*  query_stmt = "SELECT id,name,icon,avatar,description,activity_animation,aoffsetx,aoffsety,invaild_animation,ioffsetx,ioffsety from t_collect";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int templateId = sqlite3_column_int(statement,0);
			char* name=(char*)sqlite3_column_text(statement, 1);
			char* icon=(char*)sqlite3_column_text(statement, 2);
			char* avatar=(char*)sqlite3_column_text(statement, 3);
			char* description=(char*)sqlite3_column_text(statement, 4);
			char* activity_animation=(char*)sqlite3_column_text(statement, 5);
			int aoffset_x = sqlite3_column_int(statement,6);
			int aoffset_y = sqlite3_column_int(statement,7);
			char* invaild_animation=(char*)sqlite3_column_text(statement, 8);
			int ioffset_x = sqlite3_column_int(statement,9);
			int ioffset_y = sqlite3_column_int(statement,10);

			auto temp = new CCollectInfo();
			temp->set_templateId(templateId);
			temp->set_name(name);
			temp->set_icon(icon);
			temp->set_avatar(avatar);
			temp->set_des(description);
			temp->set_activity_anm(activity_animation);
			temp->set_activity_offSet_x(aoffset_x);
			temp->set_activity_offSet_y(aoffset_y);
			temp->set_invaild_anm(invaild_animation);
			temp->set_invaild_offSet_x(ioffset_x);
			temp->set_invaild_offSet_y(ioffset_y);

			s_collectInfo.insert(make_pair(templateId,temp));
		}	
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);
	sqlite3_close(pdb); 
}


/////////////////////////////////////////////////////////////////
std::map<int, std::string> VipValueConfig::s_vipValue;

void VipValueConfig::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char*  query_stmt = "SELECT id,value from t_config";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int id = sqlite3_column_int(statement,0);
			char* value=(char*)sqlite3_column_text(statement, 1);
			
			s_vipValue.insert(make_pair(id,value));
		}	
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);
	sqlite3_close(pdb); 
}



/////////////////////////////////////////////////////////////////
std::map<int, VipPriceConfig::priceValueAndType*> VipPriceConfig::s_vipPriceValue;

void VipPriceConfig::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char*  query_stmt = "SELECT vip_level,price,price_type from t_vip_price";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int vip_level = sqlite3_column_int(statement,0);
			int price = sqlite3_column_int(statement, 1);
			int price_type = sqlite3_column_int(statement, 2);

			VipPriceConfig::priceValueAndType* temp = new VipPriceConfig::priceValueAndType();
			temp->priceValue = price;
			temp->priceType = price_type;
			s_vipPriceValue.insert(make_pair(vip_level,temp));
		}	
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);
	sqlite3_close(pdb); 
}

/////////////////////////////////////////////////////////////////
std::map<int, CRechargeInfo*> RechargeConfig::s_rechargeValue;

void RechargeConfig::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char*  query_stmt = "SELECT id,recharge_value,send_value,suggest,icon,orderId from t_recharge_config";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int id = sqlite3_column_int(statement,0);
			int recharge_value = sqlite3_column_int(statement, 1);
			int send_value = sqlite3_column_int(statement, 2);
			int suggest = sqlite3_column_int(statement, 3);
			char* icon = (char*)sqlite3_column_text(statement, 4);
			int orderId = sqlite3_column_int(statement, 5);

			auto temp = new CRechargeInfo();
			temp->set_id(id);
			temp->set_recharge_value(recharge_value);
			temp->set_icon(icon);
			temp->set_send_value(send_value);
			temp->set_suggest(suggest);
			temp->set_orderId(orderId);

			s_rechargeValue.insert(make_pair(orderId,temp));
		}	
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);
	sqlite3_close(pdb); 
}

////////////////////////////////////////////////////////////

std::map<GeneralTeachAddedPropertyConfigData::TeachAddedKey,CGeneralTeachAddedProperty *> GeneralTeachAddedPropertyConfigData::s_generalTeachAdded;

void GeneralTeachAddedPropertyConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char*  query_stmt = "SELECT id,quality,star,profession,hp_capacity,max_attack,max_magic_attack from t_general_quality_add";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int id =sqlite3_column_int(statement, 0);
			int quality=sqlite3_column_int(statement, 1);
			int star=sqlite3_column_int(statement, 2);
			int profession =sqlite3_column_int(statement, 3);
			int hp_capacity=sqlite3_column_int(statement, 4);
			int max_attack=sqlite3_column_int(statement, 5);
			int max_magic_attack=sqlite3_column_int(statement, 6);
			//CCLOG("skill id: %s, description: %s", baseSkill->id().c_str(), descriptionField);

			// create map
			TeachAddedKey  key;
			key.profession = profession;
			key.star = star;
			key.quality = quality;

			auto generalsTeachAdded = new CGeneralTeachAddedProperty();
			generalsTeachAdded->set_id(id);
			generalsTeachAdded->set_quality(quality);
			generalsTeachAdded->set_star(star);
			generalsTeachAdded->set_profession(profession);
			generalsTeachAdded->set_hp_capacity(hp_capacity);
			generalsTeachAdded->set_max_attack(max_attack);
			generalsTeachAdded->set_max_magic_attack(max_magic_attack);

			s_generalTeachAdded.insert(make_pair(key, generalsTeachAdded));

		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

/////////////////////////////////////////////////////////////////
std::map<int, CPowerSendInfo*> PowerSendConfig::s_map_powerSendConfig;

void PowerSendConfig::load(const char* configFileName)
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char*  query_stmt = "SELECT id,APPpush_time,send_value from t_physical_power_config";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int nId = sqlite3_column_int(statement,0);
			char* charSendTime = (char*)sqlite3_column_text(statement, 1);
			int nSendValue = sqlite3_column_int(statement, 2);

			auto pPowerSendInfo = new CPowerSendInfo();
			pPowerSendInfo->set_id(nId);
			pPowerSendInfo->set_sendTime(charSendTime);
			pPowerSendInfo->set_powerValue(nSendValue);

			s_map_powerSendConfig.insert(make_pair(nId,pPowerSendInfo));
		}	
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);
	sqlite3_close(pdb); 
}

/////////////////////////////////////////////////////////////////
std::vector<CSysthesisInfo*> SysthesisItemConfig::s_systhesisInfo;

void SysthesisItemConfig::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	const char*  query_stmt = "SELECT m.*,o.output_prop_id output_prop_id,o.output_prop_name output_prop_name,o.output_prop_icon output_prop_icon,o.output_prop_quality output_prop_quality FROM \
							  (SELECT m.id id,m.merge_prop_id merge_prop_id,p.name merge_prop_name,p.icon merge_prop_icon,p.quality merge_prop_quality, m.need_num need_num,m.need_price need_price,m.output_num output_num,m.goodsname goodsname \
							  FROM t_prop_merge_item m,t_prop p WHERE m.merge_prop_id = p.id) m,(SELECT m.id id,m.output_prop_id output_prop_id,p.name output_prop_name,p.icon output_prop_icon,p.quality output_prop_quality FROM \
							  t_prop_merge_item m,t_prop p WHERE m.output_prop_id = p.id) o WHERE m.id=o.id";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int id = sqlite3_column_int(statement,0);
			char* merge_prop_id = (char*)sqlite3_column_text(statement, 1);
			char* merge_prop_name = (char*)sqlite3_column_text(statement, 2);
			char* merge_prop_icon = (char*)sqlite3_column_text(statement, 3);
			int merge_prop_quality = sqlite3_column_int(statement, 4);
			int need_num = sqlite3_column_int(statement, 5);
			int need_price = sqlite3_column_int(statement, 6);
			int output_num = sqlite3_column_int(statement, 7);
			char* goodsName = (char*)sqlite3_column_text(statement, 8);
			char* output_prop_id = (char*)sqlite3_column_text(statement, 9);
			char* output_prop_name = (char*)sqlite3_column_text(statement, 10);
			char* output_prop_icon = (char*)sqlite3_column_text(statement, 11);
			int output_prop_quality = sqlite3_column_int(statement, 12);

			auto temp = new CSysthesisInfo();
			temp->set_id(id);
			temp->set_merge_prop_id(merge_prop_id);
			temp->set_merge_prop_name(merge_prop_name);
			temp->set_merge_prop_icon(merge_prop_icon);
			temp->set_merge_prop_quality(merge_prop_quality);
			temp->set_need_num(need_num);
			temp->set_need_price(need_price);
			temp->set_output_num(output_num);
			temp->set_output_prop_id(output_prop_id);
			temp->set_output_prop_name(output_prop_name);
			temp->set_output_prop_icon(output_prop_icon);
			temp->set_output_prop_quality(output_prop_quality);
			temp->set_typename(goodsName);

			s_systhesisInfo.push_back(temp);
		}	
	}
	else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);
	sqlite3_close(pdb); 
}

////////////////////
std::map<EquipStrengthStoneLevelConfigData::typeAndLevle, MapStrengthUseStone*> EquipStrengthStoneLevelConfigData::s_EquipStrengthUseStone;
void EquipStrengthStoneLevelConfigData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 
	const char* query_stmt = "SELECT typeId,level,useStoneId,stoneName,limitLevel from t_strength_level";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) 
	{
		while (sqlite3_step(statement) == SQLITE_ROW) 
		{
			int m_type = sqlite3_column_int(statement,0);
			int m_level = sqlite3_column_int(statement,1);
			char* m_stoneId = (char*)sqlite3_column_text(statement,2);
			char* m_stoneName = (char*)sqlite3_column_text(statement,3);
			char* m_limitLevel = (char*)sqlite3_column_text(statement,4);

			typeAndLevle temp;
			temp.type_ = m_type;
			temp.level_ = m_level;
			// create map
			auto strengthui = new MapStrengthUseStone();
			strengthui->set_type(m_type);
			strengthui->set_level(m_level);
			strengthui->set_stoneId(m_stoneId);
			strengthui->set_stoneName(m_stoneName);
			strengthui->set_limitLevel(m_limitLevel);

			s_EquipStrengthUseStone.insert(make_pair(temp,strengthui));
		}
	}else 
	{
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

//////////////////////////////////////////////////////
std::vector<std::string> MonsterDialogData::s_monsterDialogData;

void MonsterDialogData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	const char* query_stmt = "SELECT attack from t_monster_dialog";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char * text = (char *)sqlite3_column_text(statement,0);

			s_monsterDialogData.push_back(text);
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}

	sqlite3_finalize(statement);

	sqlite3_close(pdb);
}

//////////////////////////////////
std::map<int ,CSingCopyClazz *> SingleCopyClazzConfig::s_SingCopyClazzMsgData;

void SingleCopyClazzConfig::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data 
	const char* query_stmt = "SELECT clazz,start_level,end_level,name,icon,resume,reward_info from t_tower_clazz";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int clazzId_ = sqlite3_column_int(statement, 0);
			int starLv_ = sqlite3_column_int(statement, 1);
			int endLv_ = sqlite3_column_int(statement, 2);
			char* name_ = (char*)sqlite3_column_text(statement,3);
			char* icon_ =(char*)sqlite3_column_text(statement, 4);
			int resume_ = sqlite3_column_int(statement, 5);
			char* reward_ =(char*)sqlite3_column_text(statement, 6);
			
			// create map
			auto copyClazzMsg = new CSingCopyClazz();
			copyClazzMsg->set_clazz(clazzId_);
			copyClazzMsg->set_starLevel(starLv_);
			copyClazzMsg->set_endLevel(endLv_);
			copyClazzMsg->set_name(name_);
			copyClazzMsg->set_icon(icon_);
			copyClazzMsg->set_resume(resume_);
			copyClazzMsg->set_reward(reward_);
			
			s_SingCopyClazzMsgData.insert(make_pair(clazzId_,copyClazzMsg));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

//////////////////////////////////
std::map<int ,CSingCopyLevel *> SingleCopyLevelConfig::s_SingCopyLevelMsgData;

void SingleCopyLevelConfig::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data 
	const char* query_stmt = "SELECT level,clazz from t_tower_base";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int level_ = sqlite3_column_int(statement, 0);
			int clazz_ = sqlite3_column_int(statement, 1);
			
			// create map
			auto copyLevelMsg = new CSingCopyLevel();
			copyLevelMsg->set_level(level_);
			copyLevelMsg->set_clazz(clazz_);
			
			s_SingCopyLevelMsgData.insert(make_pair(level_,copyLevelMsg));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

////////////////////////////////////
std::map<std::string ,CGemPropertyMap *> gempropertyConfig::s_GemPropertyMsgData;
void gempropertyConfig::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data 
	const char* query_stmt = "SELECT prop_id,property_clazz from t_prop_inlay_material";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char* gemId_ = (char*)sqlite3_column_text(statement,0);
			int gemclazz_ = sqlite3_column_int(statement, 1);

			// create map
			auto gemproperty_ = new CGemPropertyMap();
			gemproperty_->set_propId(gemId_);
			gemproperty_->set_propClazz(gemclazz_);

			s_GemPropertyMsgData.insert(make_pair(gemId_,gemproperty_));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}

//////////////////////////////
std::map<int ,CRewardBase *> RewardListConfig::s_RewardListMsgData;
void RewardListConfig::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data 
	const char* query_stmt = "SELECT id,name,icon,des,isClose,isState from t_rewardList_config";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int id_ = sqlite3_column_int(statement, 0);
			char* name_ = (char*)sqlite3_column_text(statement,1);
			char* icon_ = (char*)sqlite3_column_text(statement,2);
			char* des_ = (char*)sqlite3_column_text(statement,3);
			int isClose_ = sqlite3_column_int(statement,4);
			int isState_ = sqlite3_column_int(statement,5);

			// create map
			auto reward_ = new CRewardBase();
			reward_->setId(id_);
			reward_->setName(name_);
			reward_->setIcon(icon_);
			reward_->setDes(des_);
			reward_->setClose(isClose_);
			reward_->setRewardState(isState_);

			s_RewardListMsgData.insert(make_pair(id_,reward_));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}


//////////////////////////////////////////////////////
std::map<int, std::string> NpcData::s_npcFunctionIcon;

void NpcData::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	const char *query_stmt = "SELECT id,function_icon from t_npc";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			int npcId = sqlite3_column_int(statement, 0);
			char* npcFunctionIcon = (char*)sqlite3_column_text(statement, 1);
			//CCLOG("skill id: %s, name: : %s", skillIdField, nameField);

			s_npcFunctionIcon.insert(make_pair(npcId,npcFunctionIcon));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
}



//////////////////////////////////////////////////////////

std::map<std::string, CPropInfo *> StaticPropInfoConfig::s_propInfoData;

void StaticPropInfoConfig::load( const char* configFileName )
{
	//记录返回结果是否成功  
	int result; 

	//获取保存路径  + 保存文件名  
	std::string path=getDBPath(configFileName);

	//数据库对象  
	sqlite3 *pdb;  

	result=sqlite3_open(path.c_str(),&pdb); 
	CCLOG("db path name: %s", path.c_str());
	//result=sqlite3_open_v2(path.c_str(), &pdb, SQLITE_OPEN_READONLY, NULL);
	if(result!=SQLITE_OK) 
		CCLOG("open database failed,  number%d",result); 

	// query data
	const char *query_stmt = "SELECT id,idx,name,type,icon,quality from t_prop";
	sqlite3_stmt *statement;
	result = sqlite3_prepare_v2(pdb, query_stmt, strlen(query_stmt), &statement, NULL);
	if (result == SQLITE_OK) {
		while (sqlite3_step(statement) == SQLITE_ROW) {
			char* id =(char*)sqlite3_column_text(statement, 0);
			int idx=sqlite3_column_int(statement, 1);
			char* name=(char*)sqlite3_column_text(statement, 2);
			int type=sqlite3_column_int(statement, 3);
			char* icon=(char*)sqlite3_column_text(statement, 4);
			int quality=sqlite3_column_int(statement, 5);

			// create map
			auto propInfo = new CPropInfo();
			propInfo->set_id(id);
			propInfo->set_idx(idx);
			propInfo->set_name(name);
			propInfo->set_type(type);
			propInfo->set_icon(icon);
			propInfo->set_quality(quality);
			s_propInfoData.insert(make_pair(propInfo->get_id(),propInfo));
		}
	}
	else {
		CCLOG("query data failed!, the reason is: %d", result);
		CCLOG("sql Error: %s",sqlite3_errmsg(pdb));
	}
	sqlite3_finalize(statement);

	sqlite3_close(pdb); 
} 
