/*
 *  GameUtils.h
 *
 *  Created by Zhao Gang on 07-30-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#ifndef GAMEUTILS_H
#define GAMEUTILS_H

#include "cocos2d.h"

USING_NS_CC;

class GameUtils {
public :
	static Vec2 getDirection(Vec2 source, Vec2 target);
	static float getDegree(Vec2 source, Vec2 target);

	// utils: date, time
	static long long millisecondNow();
	static long long getDateSecond();
	static void getDateNow(int level);

    /**
     * translate a filename into absolute full path filename
     * @param filename String        filename will be translated.
     *                               filename will be view as a path relative to the path specifed with relativeTo.
     * @param relativeTo String      must be absolute (path or file)
     * @return String
     */
	static std::string getFullPathFilename ( std::string filename, std::string relativeTo );

	//  
	//  from quickcocos2dx
	//  CCColorUtil
	//  
	//  Created by Terran Tian on 13-11-19.  
	//  Copyright (c) 2013� qeeplay.com. All rights reserved.  
	//  
	static void addGray(Sprite* sp);  
	static void removeGray(Sprite* sp);
    
    // convert color value (int format) to Color3B format
    static Color3B convertToColor3B(int color);
	static int convertToColorInt(Color3B color);
	/*
	 * play music and effect in three platform:  win32,  ios,  android
	 * @param resName char*      the name of the sound resource
     * @param type int      two value:  1  -  play background music,  2  -  play effect
	 * @param mode bool       true  -  cycleplay,   false  -  play only once
     * @return void
	 *
	 *Created by LiuTao on 14-04-12.
     */
	static void playGameSound(const char* resName, int type, bool mode);

	static std::string getAppUniqueID();
	//get [0,size-1] random int value
	static unsigned int getRandomNum(unsigned int size);
};

#endif