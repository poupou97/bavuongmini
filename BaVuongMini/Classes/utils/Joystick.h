/*
 *  Joystick.h
 *  YaoGan
 *
 *  Created by Liu Yanghui on 11-10-27.
 *  Copyright 2011 ard8. All rights reserved.
 *
 */

#ifndef JOYSTICK_H
#define JOYSTICK_H

#include "cocos2d.h"
using namespace cocos2d;

class Joystick : public Layer {
public :
	Joystick();

	Vec2 centerPoint;//摇杆中心
	Vec2 currentPoint;//摇杆当前位置
	bool active;//是否激活摇杆
	float radius;//摇杆半径
	Sprite *jsSprite;

	void Active();
	void Inactive();
	bool isActive();

	Vec2 getDirection();
	float getVelocity();
	void updatePos(float delta);

	//初始化 aPoint是摇杆中心 aRadius是摇杆半径 aJsSprite是摇杆控制点 aJsBg是摇杆背景
	static Joystick*  JoystickWithCenter(Vec2 aPoint ,float aRadius ,Sprite* aJsSprite,Sprite* aJsBg);
	Joystick * initWithCenter(Vec2 aPoint ,float aRadius ,Sprite* aJsSprite,Sprite* aJsBg);

	virtual bool onTouchBegan(Touch *pTouch, Event *pEvent);
	virtual void onTouchMoved(Touch *pTouch, Event *pEvent);
	virtual void onTouchEnded(Touch *pTouch, Event *pEvent);

private:
	int getDirection(Vec2 from, Vec2 to, const float* angles_data);
};

#endif