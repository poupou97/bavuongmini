#include "GameResourceManager.h"

#include <string>
#include "GameUserDefault.h"

GameResourceManager::GameResourceManager()
{
}

GameResourceManager::~GameResourceManager()
{
}

void GameResourceManager::prepareStaticData()
{
	// prepare resource
	#define GAME_STATICDATA_DB "game.db"
	std::string dbPath = FileUtils::getInstance()->getWritablePath();  
	dbPath += GAME_STATICDATA_DB;

    bool bNewVersion = false;
    int resVersion = UserDefault::getInstance()->getIntegerForKey(KEY_OF_RES_VERSION);
    if(GAME_RES_VERSION > resVersion)
    {
        bNewVersion = true;
		UserDefault::getInstance()->setIntegerForKey(KEY_OF_RES_VERSION, GAME_RES_VERSION);
    }
    
	if(!FileUtils::getInstance()->isFileExist(dbPath))
	{
		copyFile(GAME_STATICDATA_DB);
	}
    else
    {
        if(bNewVersion)
        {
            copyFile(GAME_STATICDATA_DB);
        }
    }

	////////////////////////////////////////////

	char* game_static_data_db_file = "gamestaticdata.db";
	dbPath = FileUtils::getInstance()->getWritablePath();  
	dbPath += game_static_data_db_file;

	if(!FileUtils::getInstance()->isFileExist(dbPath))
	{
		copyFile(game_static_data_db_file);
	}
    else
    {
        if(bNewVersion)
        {
            copyFile(game_static_data_db_file);
        }
    }
	////////////////////////////////////////////

	// LiuLiang add 2014.4.8(���а���ݿ)
	#define GAME_RANK_DB "rank.db"
	std::string rankDbPath = FileUtils::getInstance()->getWritablePath();  
	rankDbPath += GAME_RANK_DB;

	if (!FileUtils::getInstance()->isFileExist(rankDbPath))
	{
		copyFile(GAME_RANK_DB); //�Ҫʹ�õsqliteĿ��ļ
	}
    else
    {
        if(bNewVersion)
        {
            copyFile(GAME_RANK_DB);
        }
    }
}

void GameResourceManager::copyFile(const char* pFileName) {  

	std::string strPath = FileUtils::getInstance()->fullPathForFilename(pFileName);  

	ssize_t len = 0;
	unsigned char* data = NULL;  
	// must use "rb", otherwise you maybe get wrong the len of filedata
	data = FileUtils::getInstance()->getFileData(strPath.c_str(), "rb", &len);  

	std::string destPath = FileUtils::getInstance()->getWritablePath();
	destPath += pFileName;

	// must use "wb", otherwise you maybe write a wrong file
	FILE *pFp = fopen(destPath.c_str(), "wb");  
	fwrite(data, sizeof(unsigned char), len, pFp);  
	fclose(pFp);  
	delete[] data;  
	data = NULL;  
}  
