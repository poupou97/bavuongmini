/*
 *  FinderContext.h
 *
 *  Created by Zhao Gang on 07-31-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#ifndef __PATHFINDER_FINDERCONTEXT_H__
#define __PATHFINDER_FINDERCONTEXT_H__

#include "cocos2d.h"

USING_NS_CC;

class SimpleOpenList;
class Table;
class AStarNode;
class AStarTiledMap;

class FinderContext {
public:
	FinderContext();
	virtual ~FinderContext();
	
	void setMap(char** data, int hNum, int vNum);
	AStarTiledMap* getMap();
	
	void reset(AStarNode* source, AStarNode* target);
	
	__Array* getPath();
	
	__Array* getOpenList();
	
	AStarNode* getMinFNode();
	
	AStarNode* getOpenNode(AStarNode* node);
	
	bool isClosedListContains(AStarNode* node);
	
	bool isClosedListContains(int x, int y);
	
	bool isEmpty();
	
	AStarNode* getSource();

	AStarNode* getTarget();

	bool isAchieved();
	
	void achieved();
	
	void addToClosedList(AStarNode* node);
	
	void addToOpenList(AStarNode* node);
	
	void touchNeighbor(AStarNode* neighbor, AStarNode* current, AStarNode* target);

private:
	bool bAchieved;
	/*
	 * OpenList有两个实现，简单实现SimpleOpenList和二叉堆实现BinaryHeapOpenList.
	 * iOS移植时，如果二叉堆实现移植困难，可以直接用SimpleOpenList，也足够快了。
	 */
    SimpleOpenList *openList;
    Table *closeMap;
    AStarNode *source;
    AStarNode *target;
    AStarTiledMap *map;
	__Array *path; // List
};

#endif