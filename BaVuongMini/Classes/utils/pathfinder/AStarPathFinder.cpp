/*
 *  AStarPathFinder.cpp
 *
 *  Created by Zhao Gang on 07-31-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#include "AStarPathFinder.h"

#include "AStarNode.h"
#include "FinderContext.h"
#include "AStarTiledMap.h"
	
AStarPathFinder::AStarPathFinder() {
	// 初始化缓存的AStarNode
	AStarNode::getNode(0, 0);

    ctx = new FinderContext();
    maxCost = 0;
    minCost = LONG_MAX;
    count = 0;
    avgCost = 0;
    totCost = 0;
}

AStarPathFinder::~AStarPathFinder() {
	delete ctx;
}

/**
	* 地图格子信息，第一维Y轴，第二维X轴，值为1表示可通过
	*/
void AStarPathFinder::setMap(char** mapData, int hNum, int vNum) {
	ctx->setMap(mapData, hNum, vNum);
}

AStarTiledMap* AStarPathFinder::getMap() {
	return ctx->getMap();
}

__Array* AStarPathFinder::getOpenList() {
	return ctx->getOpenList();
}

std::vector<short*>* AStarPathFinder::searchPath(short* startPos, short* objectPos) {
	//long beginMillis = System.currentTimeMillis();
	long beginMillis = 0;

	__Array* astarPath = findPath(startPos, objectPos);
	if (astarPath == NULL) {
		return NULL;
	} else {
		std::vector<short*> *path = new std::vector<short*>();
		Ref* pObj = NULL;
		CCARRAY_FOREACH(astarPath, pObj) {
			AStarNode* n = (AStarNode*)pObj;
			path->push_back(n->toShortArray());
		}

		return path;
	}

	// finally
	//long cost = System.currentTimeMillis() - beginMillis;
	long cost = 0;
	if (cost > maxCost) {
		maxCost = cost;
	}
	if (cost < minCost) {
		minCost = cost;
	}
	++count;
	totCost += cost;
	avgCost = totCost / count;
}

void AStarPathFinder::setSearchTimes(short maxTimes) {
}

void AStarPathFinder::resetList() {
}

void AStarPathFinder::setLimit(char* limit) {
}

void AStarPathFinder::release() {
}

void AStarPathFinder::clear() {
}

bool AStarPathFinder::positionEquals(short* pos1, short* pos2) {
	return pos1[0] == pos2[0] && pos1[1] == pos2[1];
}

/**
	* 搜索算法
	*/
__Array* AStarPathFinder::findPath(short* srcPos, short* targetPos) {
	AStarTiledMap* map = ctx->getMap();
	if (!map->isPath(targetPos[1], targetPos[0])
			|| positionEquals(srcPos, targetPos)) {
//			Logger.error("AStar", String.format(
//					"ERROR position:start(%d,%d:%b) end(%d,%d:%b)", srcPos[1],
//					srcPos[0], map.isPath(srcPos[1], srcPos[0]), targetPos[1],
//					targetPos[0], map.isPath(targetPos[1], targetPos[0])));
		return NULL;
	}

	// 初始化数据 开启列表和关闭列表 将源结点加入到开启列表中
	AStarNode::clearAllNodes();
	AStarNode* source = AStarNode::getNode(srcPos);
	AStarNode* target = AStarNode::getNode(targetPos);
	source->init(target);
	ctx->reset(source, target);

	AStarNode* current = NULL;
	int x, y;
	int hNum = getMap()->getHorizontalTilesNum();
	int vNum = getMap()->getVerticalTilesNum();

	while (!ctx->isEmpty() && !ctx->isAchieved()) {
		current = ctx->getMinFNode();
		if (isAchieve(current, target)) { // 是否已经完成寻路
			ctx->achieved();
			return buildPath(ctx, current);
		} else {
			ctx->addToClosedList(current);
			for (int i = 0; i < 9; ++i) { // 遍历临近节点
				x = current->getX() + i / 3 - 1;
				y = current->getY() + i % 3 - 1;

				if (x < 0 || y < 0 || x >= hNum || y >= vNum
						|| (x == current->getX() && y == current->getY()) /* 自己 */
						|| ctx->isClosedListContains(x, y)
						|| isCannotGo(map, current, x, y)) {
					continue;
				} else {
					AStarNode* neighbor = AStarNode::getNode(x, y);
					ctx->touchNeighbor(neighbor, current, target);
				}
			}
		}
	}

	return NULL;
}

/**
	* 判断从from结点到to结点是否不可行
	* 
	* @param from
	* @param to
	* @return
	*/
bool AStarPathFinder::isCannotGo(AStarTiledMap* tiledMap, AStarNode* from, int toX,
		int toY) {
	if (tiledMap->isBarrier(toX, toY)) { /* 如果这一格已经是障碍物，那么不能走 */
		return true;
	} else { /* 如果他旁边 */
		int offsetX = from->getX() - toX;
		int offsetY = from->getY() - toY;
		if (abs(offsetX) == 1 && abs(offsetY) == 1) { // 只有在走斜线的时候才要继续判断
			if ((offsetX == 1
					&& offsetY == -1
					&& (isValidX(from->getX() - 1)
							&& tiledMap->isBarrier(from->getX() - 1,
									from->getY()) || isValidY(from->getY() + 1)
							&& tiledMap->isBarrier(from->getX(),
									from->getY() + 1)) || (offsetX == 1
					&& offsetY == 1
					&& (isValidY(from->getY() - 1)
							&& tiledMap->isBarrier(from->getX(),
									from->getY() - 1) || isValidX(from->getX() - 1)
							&& tiledMap->isBarrier(from->getX() - 1,
									from->getY()))
					|| (offsetX == -1 && offsetY == 1 && (isValidX(from->getX() + 1)
							&& tiledMap->isBarrier(from->getX() + 1,
									from->getY()) || isValidY(from->getY() - 1)
							&& tiledMap->isBarrier(from->getX(),
									from->getY() - 1))) || (offsetX == -1
					&& offsetY == -1 && (isValidX(from->getX() + 1)
					&& tiledMap->isBarrier(from->getX() + 1, from->getY()) || isValidY(from->getY() + 1)
					&& tiledMap->isBarrier(from->getX(), from->getY() + 1))))))
				return true;
		}
	}
	return false;
}

bool AStarPathFinder::isValidX(int x) {
	return x >= 0 && x < getMap()->getHorizontalTilesNum();
}

bool AStarPathFinder::isValidY(int y) {
	return y >= 0 && y < getMap()->getVerticalTilesNum();
}

__Array* AStarPathFinder::buildPath(FinderContext* ctx, AStarNode* current) {
	__Array* path = ctx->getPath();
	path->removeAllObjects();
	while (current != NULL) {
		path->insertObject(current, 0);
		current = current->getFather();
	}

	return path;
}

/**
	* 比较指定结点是否是目标结点
	* 
	* @param current
	* @return
	*/
bool AStarPathFinder::isAchieve(AStarNode* current, AStarNode* target) {
	return current->isEqual(target);
}

long AStarPathFinder::getMaxCost() {
	return maxCost;
}

long AStarPathFinder::getMinCost() {
	return minCost;
}

int AStarPathFinder::getCount() {
	return count;
}

long AStarPathFinder::getAvgCost() {
	return avgCost;
}

long AStarPathFinder::getTotCost() {
	return totCost;
}