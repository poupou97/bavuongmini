
#ifndef __PATHFINDER_OPENLIST_H__
#define __PATHFINDER_OPENLIST_H__

#include "cocos2d.h"

USING_NS_CC;

class AStarNode;

class OpenList
{
public:
	virtual void add(AStarNode* node) = 0;
	virtual AStarNode* poll() = 0;
	virtual __Array* values() = 0;
	virtual AStarNode* get(int x, int y) = 0;
	virtual int size() = 0;
	virtual void clear() = 0;
	virtual void touchNeighbor(AStarNode* neighbor, AStarNode* current, AStarNode* target) = 0;
};

#endif