/*
 *  Table.h
 *
 *  Created by Zhao Gang on 07-31-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#ifndef PATHFINDER_TABLE_H
#define PATHFINDER_TABLE_H

#include "Table.h"

#include "cocos2d.h"

USING_NS_CC;

class AStarNode;

class Table {
public:
	Table();
	virtual ~Table();

	AStarNode* get(int x, int y);
	
	void put(int x, int y, AStarNode* node);
	
	AStarNode* remove(int x, int y);
	
	void clear();
	
	//Collection<AStarNode> getAllNodes();
	__Dictionary* getMap();
	
	bool contains(int x, int y);
	
	int size();
	
private:
	long getKey(short x, short y);

	__Dictionary *map;
};

#endif