
#ifndef __PATHFINDER_COMPARABLE_H__
#define __PATHFINDER_COMPARABLE_H__

template<class T>
class Comparable
{
public:
	virtual int compareTo(T another) = 0;
};

#endif