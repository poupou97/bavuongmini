/*
 *  FinderContext.cpp
 *
 *  Created by Zhao Gang on 07-31-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#include "FinderContext.h"

#include "AStarTiledMap.h"
#include "SimpleOpenList.h"
#include "Table.h"
#include "AStarNode.h"

FinderContext::FinderContext() {
	openList = new SimpleOpenList();
	closeMap = new Table();
	source = NULL;
	target = NULL;
	map = new AStarTiledMap();
	path = __Array::create();
	path->retain();
}

FinderContext::~FinderContext() {
	delete openList;
	delete closeMap;
	delete map;
	CC_SAFE_RELEASE(path);
}
	
void FinderContext::setMap(char** data, int hNum, int vNum) {
	map->setData(data, hNum, vNum);
}

AStarTiledMap* FinderContext::getMap() {
	return map;
}
	
void FinderContext::reset(AStarNode* source, AStarNode* target) {
	this->source = source;
	this->target = target;
		
	bAchieved = false;
	openList->clear();
	closeMap->clear();
	path->removeAllObjects();
		
	// 把起点(source)加入到openList
	openList->add(source);
}
	
__Array* FinderContext::getPath() {
	return path;
}
	
__Array* FinderContext::getOpenList() {
	return openList->values();
}
	
AStarNode* FinderContext::getMinFNode() {
	return openList->poll();
}
	
AStarNode* FinderContext::getOpenNode(AStarNode* node) {
	return openList->get(node->getX(), node->getY());
}
	
 bool FinderContext::isClosedListContains(AStarNode* node) {
	return isClosedListContains(node->getX(), node->getY());
}
	
bool FinderContext::isClosedListContains(int x, int y) {
	return closeMap->contains(x, y);
}
	
bool FinderContext::isEmpty() {
	return openList->size() == 0;
}
	
AStarNode* FinderContext::getSource() {
	return source;
}

AStarNode* FinderContext::getTarget() {
	return target;
}

bool FinderContext::isAchieved() {
	return bAchieved;
}
	
void FinderContext::achieved() {
	bAchieved = true;
}
	
void FinderContext::addToClosedList(AStarNode* node) {
	closeMap->put(node->getX(), node->getY(), node);
}
	
void FinderContext::addToOpenList(AStarNode* node) {
	openList->add(node);
}
	
void FinderContext::touchNeighbor(AStarNode* neighbor, AStarNode* current, AStarNode* target) {
	openList->touchNeighbor(neighbor, current, target);
}