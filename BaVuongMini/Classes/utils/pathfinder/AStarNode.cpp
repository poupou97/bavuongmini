/*
 *  AStarNode.cpp
 *
 *  Created by Zhao Gang on 07-31-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#include "AStarNode.h"
#include "Table.h"

static const int GMatrix[3][3] = { { 14, 10, 14 }, { 10, 10, 10 }, { 14, 10, 14 } };

static Table* allNodes;

AStarNode* AStarNode::getNode(int x, int y) {
	// init static variable
	if(allNodes == NULL) {
		allNodes = new Table();
		for (int i = 0; i < 100; ++i) {
			for (int j = 0; j < 100; ++j) {
				allNodes->put(i, j, AStarNode::newNode(i, j));
			}
		}
	}

	AStarNode* node = allNodes->get(x, y);
	if (node == NULL) {
		node = new AStarNode(x, y);
		allNodes->put(x, y, node);
	}
		
	return node;
}
	
void AStarNode::clearAllNodes() {
	//for (AStarNode node : allNodes.getAllNodes()) {
	//	node.clear();
	//}

	DictElement* pElement;
	__Dictionary* dic = allNodes->getMap();
	CCDICT_FOREACH(dic, pElement)
	{
		auto pNode = (AStarNode*)pElement->getObject();
		pNode->clear();
	}
}
	
AStarNode* AStarNode::getNode(short* posi) {
	return getNode(posi[1], posi[0]);
}
	
AStarNode* AStarNode::newNode(int x, int y) {
	return new AStarNode(x, y);
}

AStarNode::AStarNode() {
	position = new short[2];
}

AStarNode::~AStarNode() {
	delete position;
}

AStarNode::AStarNode(int x, int y) {
	this->x = (short) x;
	this->y = (short) y;

	position = new short[2];
}
	
AStarNode::AStarNode(short* posi) {
	this->x = posi[1];
	this->y = posi[0];

	position = new short[2];
}
	
void AStarNode::clear() {
	g = h = f = 0;
	father = NULL;
}
	
short* AStarNode::toShortArray() {
	position[0] = this->y;
	position[1] = this->x;
	return position;
}

int AStarNode::getX() {
	return this->x;
}

void AStarNode::setX(int x) {
	this->x = (short) x;
}

int AStarNode::getY() {
	return this->y;
}

void AStarNode::setY(int y) {
	this->y = (short) y;
}

AStarNode* AStarNode::getFather() {
	return father;
}

void AStarNode::setFather(AStarNode* father) {
	this->father = father;
}

void AStarNode::init(AStarNode* target) {
	this->g = 0;
	this->h = heuristicCostEstimate(this, target);
	this->f = g + h;
}

/**
	* 计算H
	* 
	* @param source
	* @param target
	* @return
	*/
int AStarNode::heuristicCostEstimate(AStarNode* source, AStarNode* target) {
	return (abs(source->getX() - target->getX()) + abs(source->getY() - target->getY()))
			* GMatrix[1][1];
}

int AStarNode::compareTo(AStarNode* o) {
	return this->f < o->f ? -1 : 1;
}

bool AStarNode::isEqual(const Ref* obj) {
	if (obj == NULL || !(typeid(AStarNode) == typeid(*obj))) {
		return false;
	}

	AStarNode* node = (AStarNode*) obj;
	return node->getX() == this->getX() && node->getY() == this->getY();
}

//@Override
//public String toString() {
//	return new StringBuilder().append(getX()).append(',').append(getY()).toString();
//}

int AStarNode::calculatorF(AStarNode* father, AStarNode* target) {
	this->g = getDistinctG(father);
	this->h = heuristicCostEstimate(this, target);
	this->f = g + h;
	this->father = father;
	return f;
}

int AStarNode::getDistinctG(AStarNode* father) {
	int offsetX = getX() - father->getX();
	int offsetY = getY() - father->getY();
	return GMatrix[offsetX + 1][offsetY + 1] + father->g;
}

int AStarNode::getG() {
	return g;
}

/**
	* 是否比指定的点更好
	* 
	* @param node
	* @return
	*/
bool AStarNode::isBetter(AStarNode* node) {
	return isGBetter(node);
}

bool AStarNode::isGBetter(AStarNode* node) {
	return g + getDistinctG(node) < node->g;
}

bool AStarNode::isFBetter(AStarNode* node) {
	return f < node->f;
}

int AStarNode::getF() {
	return f;
}