/*
 *  SimpleOpenList.cpp
 *
 *  Created by Zhao Gang on 07-31-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#include "SimpleOpenList.h"

#include "AStarNode.h"
#include "Table.h"
	
SimpleOpenList::SimpleOpenList() {
	table = new Table();
	list = __Array::create();
	list->retain();
}

SimpleOpenList::~SimpleOpenList() {
	delete table;
	CC_SAFE_RELEASE(list);
}

void SimpleOpenList::add(AStarNode* node) {
	int x = node->getX();
	int y = node->getY();
	if (table->get(x, y) == NULL) {
		table->put(x, y, node);
		list->addObject(node);
	}
}

AStarNode* SimpleOpenList::poll() {
	if (table->size() == 0) {
		return NULL;
	}
		
	AStarNode* result = NULL;
	AStarNode* node = NULL;
	int minF = INT_MAX;
	int idx = 0, position = 0;
	while (idx < list->count()) {
        node = (AStarNode*)list->getObjectAtIndex(idx);
        if (node->getF() < minF) {
            minF = node->getF();
            result = node;
            position = idx;
        }
        
        ++idx;
    }
		
	table->remove(result->getX(), result->getY());
	list->removeObjectAtIndex(position);
		
	return result;
}

__Array* SimpleOpenList::values() {
	return list;
}

AStarNode* SimpleOpenList::get(int x, int y) {
	return table->get(x, y);
}

int SimpleOpenList::size() {
	return table->size();
}

void SimpleOpenList::clear() {
	table->clear();
	list->removeAllObjects();
}

void SimpleOpenList::touchNeighbor(AStarNode* neighbor, AStarNode* current,
		AStarNode* target) {
	auto node = get(neighbor->getX(), neighbor->getY());
	if (node == NULL) {
		neighbor->calculatorF(current, target);
		add(neighbor);
	} else if (node->getDistinctG(current) < node->getG()) {
		node->calculatorF(current, target);
	}
}