/*
 *  AStarTiledMap.h
 *
 *  Created by Zhao Gang on 07-31-2013.
 *  Copyright 2013 Perfect Future. All rights reserved.
 *
 */

#ifndef PATHFINDER_ASTARTILEDMAP_H
#define PATHFINDER_ASTARTILEDMAP_H

#define ASTARTILEDMAP_PATH 1
#define ASTARTILEDMAP_BARRIER 0

class AStarTiledMap {
public:
	AStarTiledMap();
	AStarTiledMap(int hNum, int vNum);
	virtual ~AStarTiledMap();
	
	void clear();
	void setData(char** data, int hNum, int vNum);
	char** getData();

	int getHorizontalTilesNum();
	int getVerticalTilesNum();
	
	char get(int x, int y);
	void set(int x, int y, int v);
	
	bool isBarrier(int x, int y);
	bool isPath(int x, int y);
	
	void toggleBarrier(int x, int y);
	void setToBarrierNode(int x, int y);

private:
	int horizontalTilesNum;
    int verticalTilesNum;
    char **mapData;
};

#endif