#include "GameConfig.h"

////////////////////////////////////////////////////////////

__Dictionary* GameConfig::s_configData = NULL;

void GameConfig::load()
{
	s_configData = __Dictionary::createWithContentsOfFile("game_config.xml");
	s_configData->retain();
}

const char* GameConfig::getStringForKey(const char* pKey)
{
	const char *str  = ((__String*)s_configData->objectForKey(pKey))->getCString();
	return str;
}

bool GameConfig::getBoolForKey(const char* pKey)
{
	const char *str  = ((__String*)s_configData->objectForKey(pKey))->getCString();

	bool ret = false;
	if (str)
	{
		ret = (! strcmp(str, "true"));
	}

	return ret;
}

int GameConfig::getIntForKey( const char* pKey )
{
	const char *str  = ((__String*)s_configData->objectForKey(pKey))->getCString();

	int ret = atoi(str);

	return ret;
}
