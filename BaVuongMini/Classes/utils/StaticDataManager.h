#ifndef _UTILS_STATICDATAMANAGER_H
#define _UTILS_STATICDATAMANAGER_H

/*
 * this class managers the static data used in this game
 * these data is from sqlite db
 * @Author, zhaogang
 * @Date, 2013/12/7
 */

#include "cocos2d.h"
#include <map>



using namespace cocos2d;

class CBaseSkill;
class CFightSkill;
class SkillBin;
class ExStatus;
class CGeneralBaseMsg;
class CGeneralDetail;
class CGeneralsEvolution;
class CGeneralsTeach;
class CFivePersonInstance;
class CFightWayBase;
class CFightWayGrowingUp;
class CFateBaseMsg;
class CDrug;
class MapRobotDrug;
class CFunctionOpenLevel;
class MapStrngthStone;
class CMonsterBaseInfo;
class CMusouTalent;
class MapEquipAddExp;
class MapStarPickAmount;
class MapStrengthGem;
class MapMarquee;
class MapRobotPotionLimit;
class MapRefineDiffAddValue;
class CAdditionProperty;
class MapRefineValueFactor;
class MapWeapEffect;
class CNewFunctionRemind;
class CVipInfo;
class CCollectInfo;
class CRechargeInfo;
class CGeneralTeachAddedProperty;
class CPowerSendInfo;
class CSysthesisInfo;
class MapStrengthUseStone;
class CSingCopyClazz;
class CSingCopyLevel;
class CGemPropertyMap;
class CRewardBase;
class CPropInfo;

class StaticDataBaseSkill
{
public:
	static void load(const char* configFileName);

	static std::map<std::string, CBaseSkill*> s_baseSkillData;
};

class StaticDataFightSkill
{
public:
	static void load(const char* configFileName);

	static std::map<std::string, std::map<int, CFightSkill*>*> s_data;
};

class StaticDataMonsterFightSkill
{
public:
	static void load(const char* configFileName);

	static std::map<std::string, CFightSkill*> s_data;
};

class StaticDataSkillBin
{
public:
	static void load(const char* configFileName);

	static std::map<std::string, SkillBin*> s_data;
};

class ExStatusConfigData
{
public:
	static void load(const char* configFileName);

	static std::map<int,ExStatus *> s_exstatusData;
};

class GeneralsConfigData
{
public:
	static void load(const char* configFileName);

	static std::map<int ,CGeneralBaseMsg *> s_generalsBaseMsgData;

};

class GeneralsDialogData
{
public:
	static void load(const char* configFileName);

	static std::map<int, std::map<int, std::vector<std::string>*>*> s_generalsDialogData;

	// general dialog config
	static int s_dialog_stay_duration;
	static int s_dialog_interval_duration;
	static float s_chance_gossip;
	static float s_chance_attack;
	static float s_chance_lowhp;
	static int s_lowhp_percent;
};

class GeneralEvolutionConfigData
{
public:

	struct ProAndRare{
		int profession;
		int rare;
		int evolution;

		bool operator <(const ProAndRare& other) const
		{
			if (profession < other.profession)        //���Ͱ�profession��������
			{
				return true;
			}
			else if (profession == other.profession)  //���profession��ͬ����rare��������
			{
				if (rare < other.rare)      
				{
					return true;
				}
				else if (rare == other.rare)//���rare��ͬ����level��������
				{
					return evolution < other.evolution;
				}
			}

			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<ProAndRare,CGeneralsEvolution*> s_generalEvolution;

};

class GeneralTeachConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<int,CGeneralsTeach*> s_generalTeach;

};

class FivePersonInstanceConfigData
{
public:
	static void load(const char* configFileName);
	//static std::map<int,CFivePersonInstance*> s_fivePersonInstance;
	static std::vector<CFivePersonInstance*> s_fivePersonInstance;
};

class FightWayConfigData
{
public:
	static void load(const char* configFileName);
	//static std::map<int,CFivePersonInstance*> s_fivePersonInstance;
	static std::vector<CFightWayBase*> s_fightWayBase;
};

class FightWayGrowingUpConfigData
{
public:
	static void load(const char* configFileName);

	static std::map<int, std::map<int, CFightWayGrowingUp*>*> s_fightWayGrowingUp;
};


class FateBaseMsgConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<std::string,CFateBaseMsg*> s_fateBaseMsg;

};

class CDrugMsgConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<std::string,CDrug*> s_drugBaseMsg;

};

class StringDataManager
{
public:
	static void load();
	static const char* getString(const char* stringId);

private:
	static __Dictionary *s_stringsData;
};

class RobotDrugConfigData
{
public:
	//map<int type,map<int level ,MapRobotDrug*>*>
	static void load(const char* configFileName);
	static std::map<int, std::map<int,MapRobotDrug*>*> s_RobotDrugConfig;

};

//t_map_property	*	get map_name by map_id	*	by:liutao	140211
class StaticDataMapName
{
public:
	static void load(const char* configFileName);

	static std::map<std::string, std::string> s_mapname;
};

class StaticDataMonsterBaseInfo
{
public:
	static void load(const char* configFileName);

	static std::map<int, CMonsterBaseInfo*> s_monsterBase;
};

/*��ȡ�������������ӵľ���*/
///����ʯ���ӵľ���
class StrengthStoneConfigData
{
public:
	
	static void load(const char* configFileName);
	static std::map<std::string,MapStrngthStone*> s_StrengthStrone;
};

/////////////////////�������/////////////////////////
class FunctionOpenLevelConfigData
{
public:
	static void load(const char* configFileName);
	static std::vector<CFunctionOpenLevel*> s_functionOpenLevelMsg;

};

/////////////////////��������ŵȼ�/////////////////////////
class ShortcutSlotOpenLevelConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<int,int> s_shortcutSlotOpenLevelMsg;

};

/////////////////////�����츳/////////////////////////
class MusouTalentConfigData
{
public:

	struct IdAndLevel{
		int idx;
		int level;

		bool operator <(const IdAndLevel& other) const
		{
			if (idx < other.idx)        //���Ͱ�id��������
			{
				return true;
			}
			else if (idx == other.idx)  //���id��ͬ����level��������
			{
				if (level < other.level)      
				{
					return true;
				}
			}

			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<IdAndLevel,CMusouTalent*> s_musouTalentMsg;
};

/////////////////////���ؽ����ı�/////////////////////////
class LoadingTextData
{
public:
	static void load(const char* configFileName);
	static std::vector<std::string> s_LoadingTexts;

};

///װ����������Ҫ�ľ���
class StrengthEquipmentConfigData
{
public:

	static void load(const char* configFileName);
	static std::map<int ,MapEquipAddExp*> s_EquipUpLevelExp;
};

//ժ��
class StrengthStarConfigData
{
public:

	static void load(const char* configFileName);
	static std::map<int ,MapStarPickAmount*> s_EquipStarAmount;
};
////��ʯ
class StrengthGemConfigData
{
public:

	static void load(const char* configFileName);
	static std::map<std::string ,MapStrengthGem*> s_EquipSGemAmount;
};
////�����
class MarqueeStringConfigData
{
public:

	static void load(const char* configFileName);
	static std::map<int ,MapMarquee*> s_MarqueeStrings;
};
//////////
class RobotPotionMilitData
{
public:

	static void load(const char* configFileName);
	static std::map<std::string ,MapRobotPotionLimit*> s_RobotPotionMilit;
};
/////equip refine addValue baseValue

class EquipRefineAddValueConfigData
{
	enum valueType
	{
		weapon = 1,//����
		dress =2 ,//����
		cap =3,//ͷ��
		shoes =8,//Ь��
		necklace=5,//����
		belt = 7,//����
	};
public:
	//clazz level get deff addValue
	static void load(const char* configFileName);
	static std::map<int, std::map<int,MapRefineDiffAddValue*>*> s_EquipRefineValue;

	static std::vector<CAdditionProperty*> getAddvalueByDiffClazzAndLevel(int clazz,int level);
	
};
///////equip refine addValue factor
class EquipRefineFactorConfigData
{
public:

	struct useLevelAndQuality{
		int useLevel;
		int quality;

		bool operator <(const useLevelAndQuality& other) const
		{
			if (useLevel < other.useLevel)        //���Ͱ�uselevel��������
			{
				return true;
			}
			else if (useLevel == other.useLevel)  //���level��ͬ����quality��������
			{
				if (quality < other.quality)      
				{
					return true;
				}
			}

			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<useLevelAndQuality, MapRefineValueFactor*> s_EquipRefineValueFactor;
};


///////equip refine effect cfg
class EquipRefineEffectConfigData
{
public:
	
	struct weapEffect{
		int weapPression;
		int refineLevel;
		
		bool operator <(const weapEffect& other) const
		{
			if (weapPression < other.weapPression)
			{
				return true;
			}
			else if (weapPression == other.weapPression)  
			{
				if (refineLevel < other.refineLevel)
				{
					return true;
				}
			}
			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<weapEffect,std::string> s_EquipmentRefineEffect;
};

///////equip star effect cfg
class EquipStarEffectConfigData
{
public:
	
	struct weapEffect{
		int weapPression;
		int starLevel;
		
		bool operator <(const weapEffect& other) const
		{
			if (weapPression < other.weapPression)
			{
				return true;
			}
			else if (weapPression == other.weapPression)  
			{
				if (starLevel < other.starLevel)
				{
					return true;
				}
			}
			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<weapEffect,std::string> s_EquipmentStarEffect;
};


/////////////////////�¹�������/////////////////////////
class NewFunctionRemindConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<int, std::vector<CNewFunctionRemind*> > s_newFunctionRemindList;
};

/////////////////////ϵͳ����/////////////////////////
class SystemInfoConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<int, std::string> s_systemInfoList;
};

/////////////////////��ť����ȼ�/////////////////////////
class BtnOpenLevelConfigData
{
public:
	static void load(const char* configFileName);
	static std::map<int, int> s_btnOpenLevel;
};

/////////////////////vip��/////////////////////////
class VipConfigData
{
public:

	struct TypeIdAndVipLevel{
		std::string typeName;
		int vipLevel;

		bool operator <(const TypeIdAndVipLevel& other) const
		{
			if (typeName < other.typeName)        //���Ͱ�id��������
			{
				return true;
			}
			else if (typeName == other.typeName)  //���id��ͬ����level��������
			{
				if (vipLevel < other.vipLevel)      
				{
					return true;
				}
			}

			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<TypeIdAndVipLevel,CVipInfo*> s_vipConfig;

};

/////////////////////vip������Ϣ��/////////////////////////
class VipDescriptionConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int, std::string> s_vipDes;
};

/////////////////////�ɼ�����Ϣ��/////////////////////////
class CollectInfoConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int, CCollectInfo*> s_collectInfo;
};

/////////////////////������Ϣ��/////////////////////////
class VipValueConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int, std::string> s_vipValue;
};


/////////////////////VIP�۸��/////////////////////////
class VipPriceConfig
{
public:
	struct priceValueAndType{
		int priceValue;
		int priceType;

		bool operator <(const priceValueAndType& other) const
		{
			if (priceValue < other.priceValue)        //���Ͱ�id��������
			{
				return true;
			}
			else if (priceType == other.priceType)  //���id��ͬ����level��������
			{
				if (priceType < other.priceType)      
				{
					return true;
				}
			}

			return false;
		}
	};
	static void load(const char* configFileName);
	static std::map<int, priceValueAndType*> s_vipPriceValue;
};

/////////////////////��ֵ������Ϣ/////////////////////////
class RechargeConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int, CRechargeInfo*> s_rechargeValue;
};

/////////////////////////�佫�������Ա仯��////////////////////////////////

class GeneralTeachAddedPropertyConfigData
{
public:

	struct TeachAddedKey{
		int profession;
		int star;
		int quality;

		bool operator <(const TeachAddedKey& other) const
		{
			if (profession < other.profession)        //���Ͱ�profession��������
			{
				return true;
			}
			else if (profession == other.profession)  //���profession��ͬ����star��������
			{
				if (star < other.star)      
				{
					return true;
				}
				else if (star == other.star)//���star��ͬ����quality��������
				{
					return quality < other.quality;
				}
			}

			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<TeachAddedKey,CGeneralTeachAddedProperty*> s_generalTeachAdded;

};

/////////////////////������Ϣ��/////////////////////////
class PowerSendConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int, CPowerSendInfo*> s_map_powerSendConfig;
};
/////////////////////�ϳ�ϵͳ������Ϣ/////////////////////////
class SysthesisItemConfig
{
public:
	static void load(const char* configFileName);
	static std::vector<CSysthesisInfo*> s_systhesisInfo;
};

///////strength stone level
class EquipStrengthStoneLevelConfigData
{
public:

	struct typeAndLevle{
		int type_;
		int level_;

		bool operator <(const typeAndLevle& other) const
		{
			if (type_ < other.type_)
			{
				return true;
			}
			else if (type_ == other.type_)
			{
				if (level_ < other.level_)      
				{
					return true;
				}
			}
			return false;
		}
	};

	static void load(const char* configFileName);
	static std::map<typeAndLevle, MapStrengthUseStone*> s_EquipStrengthUseStone;
};

/////////////////////Monster dialog/////////////////////////
class MonsterDialogData
{
public:
	static void load(const char* configFileName);
	static std::vector<std::string> s_monsterDialogData;
};
//////////���˸��� �ؿ�
class SingleCopyClazzConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int ,CSingCopyClazz *> s_SingCopyClazzMsgData;
};

class SingleCopyLevelConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int ,CSingCopyLevel *> s_SingCopyLevelMsgData;
};
/////get cur gem property by gemId
class gempropertyConfig
{
public:
	static void load(const char* configFileName);
	static std::map<std::string ,CGemPropertyMap *> s_GemPropertyMsgData;
};
// get cur reward des by index
class RewardListConfig
{
public:
	static void load(const char* configFileName);
	static std::map<int ,CRewardBase *> s_RewardListMsgData;
};

//get function_npc's icon
class NpcData
{
public:
	static void load(const char* configFileName);

	static std::map<int, std::string> s_npcFunctionIcon;
};
//get propInfo in t_prop
class StaticPropInfoConfig
{
public:
	static void load(const char* configFileName);

	static std::map<std::string, CPropInfo*> s_propInfoData;
};

#endif

