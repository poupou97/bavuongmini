#ifndef _TEST_BASIC_H_
#define _TEST_BASIC_H_

#include "cocos2d.h"
#include "messageclient/ClientNetEngine.h"

USING_NS_CC;

/**
 * the GameState is the basic state class for this game
 * author: Zhao Gang
 */
class GameState : public Scene, public ClientNetEngine::Delegate
{
public: 
    GameState();

    virtual void onEnter();

	virtual void update(float dt);

    virtual void runThisState() = 0;

	virtual void onSocketOpen(ClientNetEngine* socketEngine) {};
	virtual void onSocketClose(ClientNetEngine* socketEngine) {};
	virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error) {};

protected:
	virtual bool canHandleMessage();
};

#endif
