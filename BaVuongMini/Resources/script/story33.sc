
// PART I
// 开启屏幕上下的黑边
MovieStart()

// 为剧情，清除场景
ClearScene()

// 创建角色
// dir参数的说明  up: 0, left: 1, down: 2, right: 3
CreateNPC(1, animation/monster/zsgr_hg, 1888, 464, 大黄, 1)
Dialog(大黄，不要怕，是你主人让我来寻你的，这是你最爱的苹果哦~, hero, 1,hero)
PlayAct(1, 2)
Say(1, 汪汪~, 500)
Wait(500)
PlayAct(1, 2)
Say(1, 汪汪~, 500)

Dialog(哈哈，大黄终于不害怕了，乖~我送你回去找主人吧~, hero, 1,hero)

// 剧情结束，恢复为正常场景
ResumeScene()
Wait(250)
DeleteNPC(1)

// 关闭屏幕上下的黑边
MovieEnd()
