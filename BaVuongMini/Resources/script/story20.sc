
// PART I
// 开启屏幕上下的黑边
MovieStart()

Dialog(大将颜良甚是厉害，如今究竟有何良方能突破白马呢？, general/caocao.png, 0,曹操)
Dialog(这有何难，刚才我好像见到了关将军的身影，如果能说服他助阵，那颜良只是小菜一碟呀！, hero, 1,hero)
Dialog(那日我与大哥三弟走散，一直打听不到他们的下落。不知如何是好。, general/guanyu.png, 0,关羽)
Dialog(既然如此，关将军不如暂住于此，再慢慢打听你大哥消息也不迟吧。, general/caocao.png, 0,曹操)
Dialog(如此说来也好，至少还能有个安身之处吧。, general/guanyu.png, 0,关羽)
Dialog(能有美髯公帮我，那真是天助我也！, general/caocao.png, 0,曹操)


//Wait(1000)

// 关闭屏幕上下的黑边
MovieEnd()
