
// PART I
// 开启屏幕上下的黑边
MovieStart()

// 为剧情，清除场景
ClearScene()

Dialog(为什么我拿起玉玺后会有一种似曾相识的感觉呢？, hero, 0,hero)

CreateNPC(1, animation/npc/zsgn_cz, 1696, 336, 算命先生, 0)
CreateNPC(2, heroine, 1440, 272, heroine, 2)

Dialog(嘿嘿，那是因为在这玉玺之中，有一颗/#fefe33 金灵珠/ /e34。, general/cz.png, 1,算命先生)
BlackScreenStart()
// 屏幕中心出现简单对话
SimpleDialog(获得一颗/#fefe33 金灵珠//e34！,500)
// 关闭黑屏
BlackScreenEnd()
Wait(500)
MoveNPC(2, 1568, 336, 800)
Wait(800)
Say(2, /e06 /e34 /e06, 500)
Wait(500)
Dialog(/e06 bling /e06 bling /e06，对这完全木有抵抗力呀~, heroine, 0,heroine)
Say(2, /e06 /e34 /e06, 500)
Wait(500)
Dialog(也不看是谁出手，果然不同凡响吧！/e03，那接下来我还要去哪里呢？, hero, 0,hero)
Say(2, /e06 /e34 /e06, 500)
Wait(500)
Dialog(这就要看上天的指引咯，将计就计，顺水推舟……, general/cz.png, 1,算命先生)
// 开启黑屏
BlackScreenStart()
// 屏幕中心出现简单对话
SimpleDialog(/e30 ZZZ~~~,500)
SimpleDialog(咦？为什么又睡着了……,500)
// 关闭黑屏
BlackScreenEnd()
Dialog(什么嘛，每次话都没说完就不见了，是有多急呀？, hero, 0,hero)

// 剧情结束，恢复为正常场景
ResumeScene()
Wait(500)
DeleteNPC(1)
DeleteNPC(2)
//Wait(1000)

// 关闭屏幕上下的黑边
MovieEnd()
