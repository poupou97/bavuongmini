
// PART I
// 开启屏幕上下的黑边
MovieStart()

Dialog(放火！, general/zhugeliang.png, 1,诸葛亮)
Dialog(诸葛先生果然神机妙算，我们在博陵口给了曹操的大军一个迎头痛击！, general/guanyu.png, 1,关羽)
Dialog(如今我这两个不听管教的兄弟都对诸葛先生敬佩的不得了呢！, general/liubei.png, 1,刘备)
Dialog(在诸葛先生计谋的英明指导下，我军已经取得了大胜！军师快说，下一步我们该怎么办？, general/zhangfei.png, 1,张飞)
Dialog(下一步啊，当然是赶快逃跑，越快越好了！就请两位将军先带着主公离开。, general/zhugeliang.png, 1,诸葛亮)
Dialog(我们用一万人打掉了曹操的五万人，已经赚足了，估计现在曹操也明白过来了，所以现在不跑，更待何时啊！, hero, 1,hero)
Dialog(少侠说得是！我们速速撤退吧。, general/liubei.png, 1,刘备)

//Wait(1000)

// 关闭屏幕上下的黑边
MovieEnd()

