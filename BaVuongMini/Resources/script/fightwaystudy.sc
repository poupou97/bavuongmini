 // PART I
TutorialMainScene1(点击左上角【头像】显示底部菜单栏)
TutorialMainSceneForSecond(点击【武将】按钮打开武将界面)
TutorialGeneralSceneStep13(点击右侧【阵法】页签进入阵法界面)
TutorialGeneralSceneStep14(点击【学习阵法】按钮学会当前选中的阵法，学会之后才能启用)
TutorialStrategiesUpgradeStep1(点击【学习】按钮学会当前阵法，学习、升级阵法需要消耗荣誉值)
TutorialStrategiesUpgradeStep2(学习完毕，关闭此界面)
TutorialGeneralSceneStep15(点击【启用阵法】按钮启用阵法，启用之后将享受额外属性加成)
TutorialGeneralSceneStep6(设置完毕，关闭此界面)