// 开启屏幕上下的黑边
MovieStart()

// // 为剧情，清除场景
ClearScene()

SetSELoop(animation/texiao/changjingtexiao/ZHAMEN/zhamen2.anm, 370, 640)
CreateNPC(hero,hero,-1,-1,hero,3)
CreateNPC(1, animation/generals/zsgg_zhangjiao, 860, 845, 张角, 1)
MoveNPC(hero,560,820,500)
MoveCam(560,780,500)
Wait(800)
SetDir(hero,3)
Wait(500)

Dialog(有点儿本事，但可惜终究只有一个人，护卫何在！, general/zhangjiao.png, 0,张角)

//前排
CreateNPC(2, animation/monster/zsgr_jgro, 670, 785, , 1)
CreateNPC(3, animation/monster/zsgr_jgro, 670, 850, , 1)
CreateNPC(4, animation/monster/zsgr_jgro, 670, 915, , 1)


//中排
CreateNPC(5, animation/monster/zsgr_bn, 735, 785, , 1)
CreateNPC(6, animation/monster/zsgr_bn, 735, 850, , 1)
CreateNPC(7, animation/monster/zsgr_bn, 735, 915, , 1)


//后排
CreateNPC(8, animation/monster/zsgr_nsj, 800, 785, , 1)
CreateNPC(9, animation/monster/zsgr_nsj, 800, 850, , 1)
CreateNPC(10, animation/monster/zsgr_nsj, 800, 915, , 1)

Wait(500)

Dialog(看来总一个人宅在家里，没有朋友真是不行啊。, hero, 1,hero)

Dialog(人多欺负人少，算什么好汉！, general/lvbu.png, 0,吕布)

MoveCam(400,670,600)
Wait(600)

CreateNPC(11, animation/generals/zsgg_lbu, 400, 665, 吕布, 3)
SetSE(animation/texiao/renwutexiao/SJTX/sjtx1.anm, 400, 670)
Wait(1000)

MoveNPC(11,480,785,600)
MoveCam(560,780,600)
Wait(600)
SetDir(11,3)
Wait(500)

Dialog(英雄莫怕，我与你并肩作战！全凭英雄调遣！, general/lvbu.png, 0,吕布)
Dialog(无名之辈，给我挡住！（再溜）, general/zhangjiao.png, 0,张角)

Wait(500)
MoveNPC(1,1080,645,1000)
MoveCam(1080,645,1000)
Wait(1000)
SetSE(animation/texiao/renwutexiao/SJTX/sjtx1.anm, 1080, 646)
Wait(100)
DeleteNPC(1)
Wait(500)

MoveCam(560,780,1000)
Wait(1000)
DeleteNPC(11)

MoveNPC(hero,-1,-1,500)
MoveCam(-1,-1,500)
Wait(1000)
ResumeScene()

MovieEnd()