
// PART I
// 开启屏幕上下的黑边
MovieStart()
Dialog(【喃喃自语】昨夜观星象，发现/#00ffff 贪狼/移位，/#00ffff 破军/妄动，/#00ffff 紫薇/暗淡，/#00ffff 魔星/耀眼。不久后/#ff0000 天下必然大乱/。, general/newbieguide.png, 0,于吉仙子)
Dialog(发生什么事了？这是哪里啊？！我是不是在/#00ffff 做梦/呢，为什么这位神仙姐姐说的话我怎么一句也听不懂……, hero, 1,hero)
Dialog(你终于醒了！你前日摔落悬崖，是/#00ffff 左慈仙子/救你回来的，既然醒了，你去告诉她这个好消息吧！, general/newbieguide.png, 0,于吉仙子)

// 关闭屏幕上下的黑边
MovieEnd()
