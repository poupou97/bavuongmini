
// PART I
// 开启屏幕上下的黑边
MovieStart()

Dialog(公瑾，真的是你，公瑾，我是仲谋啊！还记得那些年我们一起追的菇凉吗？, general/sunquan.png, 0,孙权)
Dialog(我们都十年未见了，如今仲谋你已经是一方诸侯，真是厉害啊！, general/zhouyu.png, 1,周瑜)
Dialog(公瑾，同窗之时我记得我曾经说过，如果我能成为一方霸主，必给你相印，我们一同平定江东！, general/sunquan.png, 0,孙权)
Dialog(十年前孩童间的玩笑之语，哪里能当真。, general/zhouyu.png, 1,周瑜)
Dialog(十年前公瑾你就胸怀天下，才智过人，如今你难道忍心看到江东父老忍受诸侯乱战之苦吗？, general/sunquan.png, 0,孙权)
Dialog(公瑾兄，孙兄一片赤诚，我看你就不必推辞了。, hero, 1,hero)
Dialog(公瑾请放心，以后跟着我，有肉吃。, general/sunquan.png, 0,孙权)
Dialog(那些年没有白穿一条裤子呀！感谢权兄！, general/zhouyu.png, 1,周瑜)

//Wait(1000)

// 关闭屏幕上下的黑边
MovieEnd()
