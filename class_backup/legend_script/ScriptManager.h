#ifndef _LEGEND_SCRIPT_SCRIPTMANAGER_H_
#define _LEGEND_SCRIPT_SCRIPTMANAGER_H_

#include "cocos2d.h"

USING_NS_CC;

class Script;

class ScriptManager
{
public:
	ScriptManager();
	virtual ~ScriptManager(void);

	static ScriptManager* getInstance();

	/**
	  * 曾经创建出来的所有脚本都被记录在mGlobalScriptsMap里，
	  * getScriptById(int id) 从mGlobalScriptsMap中查找Script
	  */
	Script* getScriptById(int id);

	/**
	 * 更新
	 */
	void update();

	/** 执行某段脚本 */
	bool runScript(std::string filename);

	/** 跳过整段的脚本 */
	void skipScript();

private:
	/**
	* load脚本数据
	*/
	void loadScript(std::string filename);

	Script* decodeCmd(std::string& line);

	/**
	  * 增加下一批script commands 
	  * 方法是，循环至中断型脚本或者脚本结束
	  */
	void addNextCommands();

	/** 开始运行脚本 */
	void start();

	/** 结束 */
	void end();
	/** 脚本是否运行 */
	bool isStart();

	/** 强制教学时需要关闭的界面信息 */
	void initCloseUIData();

public:
	/** 中断脚本，非正常中断 */
	void stop();

	/**
	   * movie
	   **/
	bool isInMovieMode();
	CCNode* getMovieBaseNode();
	/** 中断剧情模式 */
	void stopMovie();
	/** 在剧情模式时，可能会清除场景，需要进行恢复 **/
	void resumeScene();
	/** 记录MyPlayer的位置，用于剧情结束后的位置恢复 **/
	void recordMyPlayerPosition(CCPoint realPosition);
	CCPoint getRecordMyPlayerPosition();

private:
	/** 清空所有脚本 */
	void clear();

private:
	/** 所有脚本数据 */
	std::vector<Script*> mAllScriptsList;
	/** 正在执行的脚本 */
	std::vector<Script*> mExcutingScriptsList;

	/** 脚本运行 */
	bool mStart;

	/** 当前的脚本进度 */
	unsigned int mCurScriptIndex;

	std::map<int, Script*> mGlobalScriptsMap;

	CCPoint m_myplayerRealPosition;

public:
	/** 强制教学时需要关闭的界面信息 */
	std::vector<int> mCloseUIData;
};

#endif