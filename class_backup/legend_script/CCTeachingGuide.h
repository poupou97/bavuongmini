#ifndef _LEGEND_SCRIPT_CCTEACHINGGUIDE_H_
#define _LEGEND_SCRIPT_CCTEACHINGGUIDE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

/*******
auto : yangjun 
date : 2014.10.410
des : 带黑色蒙版的教学指引，强制点击
*******/

class CCTeachingGuide :public CCLayer
{
public:
	CCTeachingGuide(void);
	~CCTeachingGuide(void);

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	static CCTeachingGuide * create(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction,int clipWidth,int clipHeight,bool isAddToMainScene = false,bool isDesFrameVisible = true,bool isHandFlip = false);
	bool init(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction,int clipWidth,int clipHeight,bool isAddToMainScene,bool isDesFrameVisible,bool isHandFlip );

	//设置裁剪区域的坐标
	void setDrawNodePos(CCPoint pt);

	//教学触发时强制关闭一些界面(指引点击主界面上元素时)
	static void CloseUIForTutorial();
private:
	CCTutorialIndicator::Direction m_curDirection;
	CCTutorialIndicator * tutorialIndicator;
	CCClippingNode * clip;
	CCDrawNode * front;
	//外发光的黄色边框
	CCScale9Sprite* sp_yellowFrame;

	int m_nClipWidth;
	int m_nClipHeight;

	void playYellowFrameAnm();

	void updateTime(float dt);
	int m_nDelayTime;
	bool m_bIsTimeUP;
};

#endif;

