#include "SCTutorialOffLineArena.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameScene.h"
#include "../../gamescene_state/MainScene.h"
#include "../../ui/missionscene/HandleMission.h"
#include "../../ui/offlinearena_ui/OffLineArenaUI.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "../../ui/offlinearena_ui/OffLineArenaData.h"


///////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialOffLineArena)

	SCTutorialOffLineArena::SCTutorialOffLineArena()
{
}

SCTutorialOffLineArena::~SCTutorialOffLineArena()
{
}

void* SCTutorialOffLineArena::createInstance()
{
	return new SCTutorialOffLineArena() ;
}

void SCTutorialOffLineArena::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialOffLineArena()");

			std::string content = mPara.at(0);

			if (OffLineArenaData::getInstance()->m_nMyRoleRanking <= 1)
			{
				setState(STATE_END);
				return;
			}

			GuideMap * guideMap = GameView::getInstance()->getMainUIScene()->guideMap;
			if (guideMap->getActionLayer())
			{
				if (!guideMap->getActionLayer()->getWidgetByName("btn_offLineArena"))
				{
					setState(STATE_END);
					return;
				}
			}
			
			guideMap->addCCTutorialIndicator(content.c_str(),guideMap->btn_offLineArena->getPosition(),CCTutorialIndicator::Direction_RU);
			//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
			guideMap->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(guideMap->btn_offLineArena) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialOffLineArena::init() {
	CCLOG("SCTutorialOffLineArena() init");
}

void SCTutorialOffLineArena::release() {
	CCLOG("SCTutorialOffLineArena() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GuideMap * guideMap = GameView::getInstance()->getMainUIScene()->guideMap;
	if(guideMap != NULL)
		guideMap->removeCCTutorialIndicator();
}

/////////////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialOffLineArena1)

SCTutorialOffLineArena1::SCTutorialOffLineArena1()
{
}

SCTutorialOffLineArena1::~SCTutorialOffLineArena1()
{
}

void* SCTutorialOffLineArena1::createInstance()
{
	return new SCTutorialOffLineArena1() ;
}

void SCTutorialOffLineArena1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialOffLineArena1()");

			std::string content = mPara.at(0);

			if (OffLineArenaData::getInstance()->m_nMyRoleRanking <= 1)
			{
				setState(STATE_END);
				return;
			}

			if (!GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI))
			{
				setState(STATE_START);
				return;
			}
			else
			{
				OffLineArenaUI * offLineArenaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
				if (offLineArenaUI->curArenaList.size()<=0)
				{
					setState(STATE_START);
				}
				else
				{ 
					if (offLineArenaUI->getTutorialRoleItem())
					{
						CCTutorialIndicator::Direction temp = CCTutorialIndicator::Direction_RU;
						if (offLineArenaUI->getTutorialRoleItemIdx() >= 2)
						{
							temp = CCTutorialIndicator::Direction_LU;
						}

						offLineArenaUI->addCCTutorialIndicator1(content.c_str(),offLineArenaUI->getTutorialRoleItem()->getPosition(),temp);
						//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
						offLineArenaUI->registerScriptCommand(this->getId()) ; 
						this->setCommandHandler(offLineArenaUI->getTutorialRoleItem()) ;
						setState(STATE_UPDATE);
					}
					else
					{
						setState(STATE_START);
					}

				}
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialOffLineArena1::init() {
	CCLOG("SCTutorialOffLineArena1() init");
}

void SCTutorialOffLineArena1::release() {
	CCLOG("SCTutorialOffLineArena1() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	OffLineArenaUI * offLineArenaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
	if(offLineArenaUI != NULL)
		offLineArenaUI->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialOffLineArena2)

	SCTutorialOffLineArena2::SCTutorialOffLineArena2()
{
}

SCTutorialOffLineArena2::~SCTutorialOffLineArena2()
{
}

void* SCTutorialOffLineArena2::createInstance()
{
	return new SCTutorialOffLineArena2() ;
}

void SCTutorialOffLineArena2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialOffLineArena2()");

			std::string content = mPara.at(0);

			if (OffLineArenaData::getInstance()->m_nMyRoleRanking <= 1)
			{
				setState(STATE_END);
				return;
			}

			if (!GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI))
			{
				setState(STATE_START);
				return;
			}

			OffLineArenaUI * offLineArenaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
			if (!offLineArenaUI->getTutorialRoleItem())
			{
				setState(STATE_START);
				return;
			}
			 
			offLineArenaUI->addCCTutorialIndicator2(content.c_str(),offLineArenaUI->btn_challenge->getPosition(),CCTutorialIndicator::Direction_LD);
			//REGISTER_SCRIPT_COMMAND(myHeadInfo, this->getId())
			offLineArenaUI->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(offLineArenaUI->btn_challenge) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialOffLineArena2::init() {
	CCLOG("SCTutorialOffLineArena2() init");
}

void SCTutorialOffLineArena2::release() {
	CCLOG("SCTutorialOffLineArena2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	OffLineArenaUI * offLineArenaUI = (OffLineArenaUI*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagOffLineArenaUI);
	if(offLineArenaUI != NULL)
		offLineArenaUI->removeCCTutorialIndicator();
}

