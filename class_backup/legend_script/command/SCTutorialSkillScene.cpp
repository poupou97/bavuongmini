#include "SCTutorialSkillScene.h"

#include "../../utils/GameUtils.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../ui/skillscene/MusouSkillScene.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutConfigure.h"
#include "../CCTutorialIndicator.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../ui/extensions/UITab.h"

IMPLEMENT_CLASS(SCTutorialSkillSceneStep1)

SCTutorialSkillSceneStep1::SCTutorialSkillSceneStep1()
{
}

SCTutorialSkillSceneStep1::~SCTutorialSkillSceneStep1()
{
}

void* SCTutorialSkillSceneStep1::createInstance()
{
	return new SCTutorialSkillSceneStep1() ;
}

void SCTutorialSkillSceneStep1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep1()");

			std::string content = mPara.at(0);

			SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			skillscene->addCCTutorialIndicator1(content.c_str(),skillscene->m_firstAtkButton->getPosition(),CCTutorialIndicator::Direction_LD);
			//REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
			skillscene->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(skillscene->m_firstAtkButton) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStep1::init() {
	CCLOG("SCTutorialTestStep1() init");
}

void SCTutorialSkillSceneStep1::release() {
	CCLOG("SCTutorialTestStep1() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(skillscene != NULL)
		skillscene->removeCCTutorialIndicator();
}


///////////////////////////////////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialSkillSceneStepSetMusou1)

	SCTutorialSkillSceneStepSetMusou1::SCTutorialSkillSceneStepSetMusou1()
{
}

SCTutorialSkillSceneStepSetMusou1::~SCTutorialSkillSceneStepSetMusou1()
{
}

void* SCTutorialSkillSceneStepSetMusou1::createInstance()
{
	return new SCTutorialSkillSceneStepSetMusou1() ;
}

void SCTutorialSkillSceneStepSetMusou1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStepSetMusou1()");

			std::string content = mPara.at(0);

			if (GameView::getInstance()->myplayer->getMusouSkill() != NULL)
			{
				ScriptManager::getInstance()->stop();
			}
			else
			{
				SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
				skillscene->addCCTutorialIndicator1(content.c_str(),skillscene->m_firstAtkButton->getPosition(),CCTutorialIndicator::Direction_LD);
				//REGISTER_SCRIPT_COMMAND(skillscene, this->getId())
				skillscene->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(skillscene->m_firstAtkButton) ;
				setState(STATE_UPDATE);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStepSetMusou1::init() {
	CCLOG("SCTutorialSkillSceneStepSetMusou1() init");
}

void SCTutorialSkillSceneStepSetMusou1::release() {
	CCLOG("SCTutorialSkillSceneStepSetMusou1() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(skillscene != NULL)
		skillscene->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialSkillSceneStep2)

SCTutorialSkillSceneStep2::SCTutorialSkillSceneStep2()
{
}

SCTutorialSkillSceneStep2::~SCTutorialSkillSceneStep2()
{
}

void* SCTutorialSkillSceneStep2::createInstance()
{
	return new SCTutorialSkillSceneStep2() ;
}

void SCTutorialSkillSceneStep2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialTestStep2()");

			std::string content = mPara.at(0);

			SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			skillscene->addCCTutorialIndicator2(content.c_str(),skillscene->Button_study->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			skillscene->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(skillscene->Button_study) ;
				setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStep2::init() {
	CCLOG("SCTutorialSkillSceneStep2() init");
}

void SCTutorialSkillSceneStep2::release() {
	CCLOG("SCTutorialSkillSceneStep2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(skillscene != NULL)
		skillscene->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialSkillSceneStep3)

SCTutorialSkillSceneStep3::SCTutorialSkillSceneStep3()
{
}

SCTutorialSkillSceneStep3::~SCTutorialSkillSceneStep3()
{
}

void* SCTutorialSkillSceneStep3::createInstance()
{
	return new SCTutorialSkillSceneStep3() ;
}

void SCTutorialSkillSceneStep3::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep3()");

			std::string content = mPara.at(0);

			SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			skillscene->addCCTutorialIndicator2(content.c_str(),skillscene->Button_convenient->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			skillscene->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(skillscene->Button_convenient) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStep3::init() {
	CCLOG("SCTutorialSkillSceneStep3() init");
}

void SCTutorialSkillSceneStep3::release() {
	CCLOG("SCTutorialSkillSceneStep3() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(skillscene != NULL)
		skillscene->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialSkillSceneStep4)

SCTutorialSkillSceneStep4::SCTutorialSkillSceneStep4()
{
}

SCTutorialSkillSceneStep4::~SCTutorialSkillSceneStep4()
{
}

void* SCTutorialSkillSceneStep4::createInstance()
{
	return new SCTutorialSkillSceneStep4() ;
}

void SCTutorialSkillSceneStep4::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep4()");

			std::string content = mPara.at(0);

			ShortcutConfigure * shortcutConfigure = (ShortcutConfigure*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutConfigure);
			shortcutConfigure->addCCTutorialIndicator1(content.c_str(),shortcutConfigure->btn_first->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			shortcutConfigure->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(shortcutConfigure->btn_first) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStep4::init() {
	CCLOG("SCTutorialSkillSceneStep3() init");
}

void SCTutorialSkillSceneStep4::release() {
	CCLOG("SCTutorialSkillSceneStep4() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	ShortcutConfigure * shortcutConfigure = (ShortcutConfigure*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutConfigure);
	if(shortcutConfigure != NULL)
		shortcutConfigure->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialSkillSceneStep5)

	SCTutorialSkillSceneStep5::SCTutorialSkillSceneStep5()
{
}

SCTutorialSkillSceneStep5::~SCTutorialSkillSceneStep5()
{
}

void* SCTutorialSkillSceneStep5::createInstance()
{
	return new SCTutorialSkillSceneStep5() ;
}

void SCTutorialSkillSceneStep5::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep3()");

			std::string content = mPara.at(0);

			ShortcutConfigure * shortcutConfigure = (ShortcutConfigure*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutConfigure);
			shortcutConfigure->addCCTutorialIndicator2(content.c_str(),shortcutConfigure->btn_close->getPosition(),CCTutorialIndicator::Direction_RU);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			shortcutConfigure->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(shortcutConfigure->btn_close) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStep5::init() {
	CCLOG("SCTutorialSkillSceneStep5() init");
}

void SCTutorialSkillSceneStep5::release() {
	CCLOG("SCTutorialSkillSceneStep5() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	ShortcutConfigure * shortcutConfigure = (ShortcutConfigure*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutConfigure);
	if(shortcutConfigure != NULL)
		shortcutConfigure->removeCCTutorialIndicator();
}


void SCTutorialSkillSceneStep5::endCommand(CCObject* pSender)
{
	Script::endCommand(pSender);

	release();
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialSkillSceneStep6)

SCTutorialSkillSceneStep6::SCTutorialSkillSceneStep6()
{
}

SCTutorialSkillSceneStep6::~SCTutorialSkillSceneStep6()
{
}

void* SCTutorialSkillSceneStep6::createInstance()
{
	return new SCTutorialSkillSceneStep6() ;
}

void SCTutorialSkillSceneStep6::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep6()");

			std::string content = mPara.at(0);

			ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			shortcutLayer->addCCTutorialIndicator(content.c_str(),shortcutLayer->getChildByTag(101)->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			shortcutLayer->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(shortcutLayer->getChildByTag(101)) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStep6::init() {
	CCLOG("SCTutorialSkillSceneStep6() init");
}

void SCTutorialSkillSceneStep6::release() {
	CCLOG("SCTutorialSkillSceneStep6() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	if(shortcutLayer != NULL)
		shortcutLayer->removeCCTutorialIndicator();
}

void SCTutorialSkillSceneStep6::endCommand(CCObject* pSender)
{
	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	if(shortcutLayer != NULL)
	{
		if (pSender == shortcutLayer->getChildByTag(101))
		{
			Script::endCommand(pSender);

			release();
		}
	}
}



/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialSkillSceneStep7)

	SCTutorialSkillSceneStep7::SCTutorialSkillSceneStep7()
{
}

SCTutorialSkillSceneStep7::~SCTutorialSkillSceneStep7()
{
}

void* SCTutorialSkillSceneStep7::createInstance()
{
	return new SCTutorialSkillSceneStep7() ;
}

void SCTutorialSkillSceneStep7::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialTestStep7()");

			std::string content = mPara.at(0);

			SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			skillscene->addCCTutorialIndicator2(content.c_str(),skillscene->Button_preeless->getPosition(),CCTutorialIndicator::Direction_LD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			skillscene->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(skillscene->Button_preeless) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStep7::init() {
	CCLOG("SCTutorialSkillSceneStep7() init");
}

void SCTutorialSkillSceneStep7::release() {
	CCLOG("SCTutorialSkillSceneStep7() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(skillscene != NULL)
		skillscene->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialSkillSceneStep8)

	SCTutorialSkillSceneStep8::SCTutorialSkillSceneStep8()
{
}

SCTutorialSkillSceneStep8::~SCTutorialSkillSceneStep8()
{
}

void* SCTutorialSkillSceneStep8::createInstance()
{
	return new SCTutorialSkillSceneStep8() ;
}

void SCTutorialSkillSceneStep8::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep8()");

			std::string content = mPara.at(0);

			SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			skillscene->addCCTutorialIndicator3(content.c_str(),skillscene->Button_close->getPosition(),CCTutorialIndicator::Direction_RU);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			skillscene->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(skillscene->Button_close) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStep8::init() {
	CCLOG("SCTutorialSkillSceneStep8() init");
}

void SCTutorialSkillSceneStep8::release() {
	CCLOG("SCTutorialSkillSceneStep8() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(skillscene != NULL)
		skillscene->removeCCTutorialIndicator();
}


void SCTutorialSkillSceneStep8::endCommand(CCObject* pSender)
{
	Script::endCommand(pSender);
	release();
}



/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialSkillSceneStep9)

SCTutorialSkillSceneStep9::SCTutorialSkillSceneStep9()
{
}

SCTutorialSkillSceneStep9::~SCTutorialSkillSceneStep9()
{
}

void* SCTutorialSkillSceneStep9::createInstance()
{
	return new SCTutorialSkillSceneStep9() ;
}

void SCTutorialSkillSceneStep9::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep9()");

			std::string content = mPara.at(0);

			SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			skillscene->addCCTutorialIndicator4(content.c_str(),skillscene->SkillTypeTab->getPosition(),CCTutorialIndicator::Direction_LU);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			skillscene->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(skillscene->SkillTypeTab) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStep9::init() {
	CCLOG("SCTutorialSkillSceneStep9() init");
}

void SCTutorialSkillSceneStep9::release() {
	CCLOG("SCTutorialSkillSceneStep9() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(skillscene != NULL)
		skillscene->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialSkillSceneStep10)

	SCTutorialSkillSceneStep10::SCTutorialSkillSceneStep10()
{
}

SCTutorialSkillSceneStep10::~SCTutorialSkillSceneStep10()
{
}

void* SCTutorialSkillSceneStep10::createInstance()
{
	return new SCTutorialSkillSceneStep10() ;
}

void SCTutorialSkillSceneStep10::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep10()");

			std::string content = mPara.at(0);

			SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			skillscene->musouSkillLayer->addCCTutorialIndicator1(content.c_str(),skillscene->musouSkillLayer->btn_suggestSet->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			skillscene->musouSkillLayer->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(skillscene->musouSkillLayer->btn_suggestSet) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStep10::init() {
	CCLOG("SCTutorialSkillSceneStep10() init");
}

void SCTutorialSkillSceneStep10::release() {
	CCLOG("SCTutorialSkillSceneStep10() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	SkillScene * skillscene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(skillscene != NULL)
		skillscene->musouSkillLayer->removeCCTutorialIndicator();
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialSkillSceneStep11)

SCTutorialSkillSceneStep11::SCTutorialSkillSceneStep11()
{
	beginTime = 0;
}

SCTutorialSkillSceneStep11::~SCTutorialSkillSceneStep11()
{
}

void* SCTutorialSkillSceneStep11::createInstance()
{
	return new SCTutorialSkillSceneStep11() ;
}

void SCTutorialSkillSceneStep11::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep11()");
			if (beginTime == 0)
			{
				beginTime = GameUtils::millisecondNow();
			}

			std::string content = mPara.at(0);

			ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			if (shortcutLayer)
			{
				if (GameUtils::millisecondNow() - beginTime >= 4000)
				{
					shortcutLayer->addCCTutorialIndicator(content.c_str(),shortcutLayer->getChildByTag(104)->getPosition(),CCTutorialIndicator::Direction_RD);
					//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
					shortcutLayer->registerScriptCommand(this->getId()) ; 
					this->setCommandHandler(shortcutLayer->getChildByTag(104)) ;
					setState(STATE_UPDATE);
				}
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStep11::init() {
	CCLOG("SCTutorialSkillSceneStep11() init");
}

void SCTutorialSkillSceneStep11::release() {
	CCLOG("SCTutorialSkillSceneStep11() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	if(shortcutLayer != NULL)
		shortcutLayer->removeCCTutorialIndicator();
}


/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialSkillSceneStep12)

SCTutorialSkillSceneStep12::SCTutorialSkillSceneStep12()
{
}

SCTutorialSkillSceneStep12::~SCTutorialSkillSceneStep12()
{
}

void* SCTutorialSkillSceneStep12::createInstance()
{
	return new SCTutorialSkillSceneStep12() ;
}

void SCTutorialSkillSceneStep12::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep12()");

			std::string content = mPara.at(0);

			SkillScene * skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
			skillScene->addCCTutorialIndicator3(content.c_str(),skillScene->Button_close->getPosition(),CCTutorialIndicator::Direction_RU);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			skillScene->registerScriptCommand(this->getId()) ; 
//			this->setCommandHandler(skillScene->Button_close) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialSkillSceneStep12::init() {
	CCLOG("SCTutorialSkillSceneStep12() init");
}

void SCTutorialSkillSceneStep12::release() {
	CCLOG("SCTutorialSkillSceneStep12() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	SkillScene * skillScene = (SkillScene*)GameView::getInstance()->getMainUIScene()->getChildByTag(KTagSkillScene);
	if(skillScene != NULL)
		skillScene->removeCCTutorialIndicator();
}


void SCTutorialSkillSceneStep12::endCommand(CCObject* pSender)
{
	Script::endCommand(pSender);

	release();
}

