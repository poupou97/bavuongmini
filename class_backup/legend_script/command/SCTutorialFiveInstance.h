#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALFIVEINSTANCE_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALFIVEINSTANCE_H_

#include "../Script.h"

/**
 *副本教学
 *
 * @author yangjun
 * @date 2014-1-7
 */

//////////////////////点击右上角箭头////////////////////////
class SCTutorialFiveInstance1 : public Script {
private:
	DECLARE_CLASS(SCTutorialFiveInstance1)

public:
	SCTutorialFiveInstance1();
	virtual ~SCTutorialFiveInstance1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////点击副本按钮////////////////////////
class SCTutorialFiveInstance2 : public Script {
private:
	DECLARE_CLASS(SCTutorialFiveInstance2)

public:
	SCTutorialFiveInstance2();
	virtual ~SCTutorialFiveInstance2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////进入副本////////////////////////
class SCTutorialFiveInstance3 : public Script {
private:
	DECLARE_CLASS(SCTutorialFiveInstance3)

public:
	SCTutorialFiveInstance3();
	virtual ~SCTutorialFiveInstance3();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


//////////////////////select first instance////////////////////////
class SCTutorialFiveInstance4 : public Script {
private:
	DECLARE_CLASS(SCTutorialFiveInstance4)

public:
	SCTutorialFiveInstance4();
	virtual ~SCTutorialFiveInstance4();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////select second instance////////////////////////
class SCTutorialFiveInstance5 : public Script {
private:
	DECLARE_CLASS(SCTutorialFiveInstance5)

public:
	SCTutorialFiveInstance5();
	virtual ~SCTutorialFiveInstance5();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////select third instance////////////////////////
class SCTutorialFiveInstance6 : public Script {
private:
	DECLARE_CLASS(SCTutorialFiveInstance6)

public:
	SCTutorialFiveInstance6();
	virtual ~SCTutorialFiveInstance6();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////select fouth instance////////////////////////
class SCTutorialFiveInstance7 : public Script {
private:
	DECLARE_CLASS(SCTutorialFiveInstance7)

public:
	SCTutorialFiveInstance7();
	virtual ~SCTutorialFiveInstance7();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif