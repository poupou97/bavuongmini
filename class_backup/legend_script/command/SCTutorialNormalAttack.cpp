#include "SCTutorialNormalAttack.h"
#include "../../utils/GameUtils.h"
#include "../../GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "../../gamescene_state/sceneelement/MyHeadInfo.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutLayer.h"

IMPLEMENT_CLASS(SCTutorialNormalAttack)

SCTutorialNormalAttack::SCTutorialNormalAttack()
{
}

SCTutorialNormalAttack::~SCTutorialNormalAttack()
{
}

void* SCTutorialNormalAttack::createInstance()
{
	return new SCTutorialNormalAttack() ;
}

void SCTutorialNormalAttack::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep6()");

			std::string content = mPara.at(0);

			ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			shortcutLayer->addCCTutorialIndicator(content.c_str(),shortcutLayer->getChildByTag(100)->getPosition(),CCTutorialIndicator::Direction_RD);
			//REGISTER_SCRIPT_COMMAND(skillscene->Button_convenient , this->getId())
			shortcutLayer->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(shortcutLayer->getChildByTag(100)) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialNormalAttack::init() {
	CCLOG("SCTutorialNormalAttack() init");
}

void SCTutorialNormalAttack::release() {
	CCLOG("SCTutorialNormalAttack() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	if(shortcutLayer != NULL)
		shortcutLayer->removeCCTutorialIndicator();
}
