#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALGETFIRSTGENERAL_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALGETFIRSTGENERAL_H_

#include "../Script.h"

/**
 * get first general remind
 *
 */
class SCTutorialGetFirstGeneral : public Script {
private:
    DECLARE_CLASS(SCTutorialGetFirstGeneral)

public:
	SCTutorialGetFirstGeneral();
	virtual ~SCTutorialGetFirstGeneral();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////
/**
 * get second general remind
 *
 */
class SCTutorialGetSecondGeneral : public Script {
private:
    DECLARE_CLASS(SCTutorialGetSecondGeneral)

public:
	SCTutorialGetSecondGeneral();
	virtual ~SCTutorialGetSecondGeneral();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////////////////
/**
 * get first skill remind
 *
 */
class SCTutorialGetFirstSkill: public Script {
private:
    DECLARE_CLASS(SCTutorialGetFirstSkill)

public:
	SCTutorialGetFirstSkill();
	virtual ~SCTutorialGetFirstSkill();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};
////////////////////////////////////////////
/**
 * use first skill 
 *
 */
class SCTutorialUseFirstSkill: public Script {
private:
    DECLARE_CLASS(SCTutorialUseFirstSkill)

public:
	SCTutorialUseFirstSkill();
	virtual ~SCTutorialUseFirstSkill();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};
#endif