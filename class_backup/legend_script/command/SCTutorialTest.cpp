#include "SCTutorialTest.h"

#include "../../utils/GameUtils.h"
#include "../../test_state/TestState.h"

IMPLEMENT_CLASS(SCTutorialTestStep1)

SCTutorialTestStep1::SCTutorialTestStep1()
{
}

SCTutorialTestStep1::~SCTutorialTestStep1()
{
}

void* SCTutorialTestStep1::createInstance()
{
	return new SCTutorialTestStep1() ;
}

void SCTutorialTestStep1::update() {
	switch (mState) {
	case STATE_START:
		{
		CCLOG("excute SCTutorialTestStep1()");

		std::string content = mPara.at(0);

		TestLayer* layer = (TestLayer*)CCDirector::sharedDirector()->getRunningScene()->getChildByTag(1);
		layer->addTutorialIndicator(content.c_str());
		layer->setTutorialIndicatorPosition(ccp(200, 200));
		REGISTER_SCRIPT_COMMAND(layer, this->getId())
		setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialTestStep1::init() {
	CCLOG("SCTutorialTestStep1() init");
}

void SCTutorialTestStep1::release() {
	CCLOG("SCTutorialTestStep1() release");

	TestLayer* layer = (TestLayer*)CCDirector::sharedDirector()->getRunningScene()->getChildByTag(1);
	if(layer != NULL)
		layer->removeTutorialIndicator();
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialTestStep2)

SCTutorialTestStep2::SCTutorialTestStep2()
{
}

SCTutorialTestStep2::~SCTutorialTestStep2()
{
}

void* SCTutorialTestStep2::createInstance()
{
	return new SCTutorialTestStep2() ;
}

void SCTutorialTestStep2::update() {
	switch (mState) {
	case STATE_START:
		{
		CCLOG("excute SCTutorialTestStep2()");

		std::string content = mPara.at(0);

		TestLayer* layer = (TestLayer*)CCDirector::sharedDirector()->getRunningScene()->getChildByTag(1);
		layer->addTutorialIndicator(content.c_str());
		layer->setTutorialIndicatorPosition(ccp(300, 300));
		REGISTER_SCRIPT_COMMAND(layer, this->getId())
		setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialTestStep2::init() {
	CCLOG("SCTutorialTestStep2() init");
}

void SCTutorialTestStep2::release() {
	CCLOG("SCTutorialTestStep2() release");

	TestLayer* layer = (TestLayer*)CCDirector::sharedDirector()->getRunningScene()->getChildByTag(1);
	if(layer != NULL)
		layer->removeTutorialIndicator();
}
