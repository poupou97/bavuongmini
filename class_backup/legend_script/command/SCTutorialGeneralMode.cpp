#include "SCTutorialGeneralMode.h"
#include "../../gamescene_state/sceneelement/GuideMap.h"
#include "GameView.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"

IMPLEMENT_CLASS(SCTutorialGeneralModeStep1)

SCTutorialGeneralModeStep1::SCTutorialGeneralModeStep1()
{
}

SCTutorialGeneralModeStep1::~SCTutorialGeneralModeStep1()
{
}

void* SCTutorialGeneralModeStep1::createInstance()
{
	return new SCTutorialGeneralModeStep1() ;
}

void SCTutorialGeneralModeStep1::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialGeneralModeStep1()");

			std::string content = mPara.at(0);

			GuideMap * guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			int generalMode = guideMap->getGeneralMode();
			if (generalMode == 1)
			{
				guideMap->addCCTutorialIndicator(content.c_str(),guideMap->getBtnGeneralMode()->getPosition(),CCTutorialIndicator::Direction_RU);
				guideMap->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(guideMap->getBtnGeneralMode()) ;
				setState(STATE_UPDATE);
			}else
			{
				setState(STATE_END);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGeneralModeStep1::init() {
	CCLOG("SCTutorialGeneralModeStep1() init");
}

void SCTutorialGeneralModeStep1::release() {
	CCLOG("SCTutorialGeneralModeStep1() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GuideMap * guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (guideMap != NULL)
		guideMap->removeCCTutorialIndicator();
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGeneralModeStep2)

	SCTutorialGeneralModeStep2::SCTutorialGeneralModeStep2()
{
}

SCTutorialGeneralModeStep2::~SCTutorialGeneralModeStep2()
{
}

void* SCTutorialGeneralModeStep2::createInstance()
{
	return new SCTutorialGeneralModeStep2() ;
}

void SCTutorialGeneralModeStep2::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialSkillSceneStep1()");

			std::string content = mPara.at(0);

			GuideMap * guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			int generalMode = guideMap->getGeneralMode();
			if (generalMode == 2)
			{
				guideMap->addCCTutorialIndicator(content.c_str(),guideMap->getBtnGeneralMode()->getPosition(),CCTutorialIndicator::Direction_RU);
				guideMap->registerScriptCommand(this->getId()) ; 
				this->setCommandHandler(guideMap->getBtnGeneralMode()) ;
				setState(STATE_UPDATE);
			}else
			{
				setState(STATE_END);
			}
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGeneralModeStep2::init() {
	CCLOG("SCTutorialGeneralModeStep2() init");
}

void SCTutorialGeneralModeStep2::release() {
	CCLOG("SCTutorialGeneralModeStep2() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GuideMap * guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (guideMap != NULL)
		guideMap->removeCCTutorialIndicator();
}



/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGeneralModeStep3)

SCTutorialGeneralModeStep3::SCTutorialGeneralModeStep3()
{
}

SCTutorialGeneralModeStep3::~SCTutorialGeneralModeStep3()
{
}

void* SCTutorialGeneralModeStep3::createInstance()
{
	return new SCTutorialGeneralModeStep3() ;
}

void SCTutorialGeneralModeStep3::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialGeneralModeStep3()");

			std::string content = mPara.at(0);

			GuideMap * guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
			guideMap->addCCTutorialIndicator(content.c_str(),guideMap->getBtnGeneralMode()->getPosition(),CCTutorialIndicator::Direction_RU);
			guideMap->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(guideMap->getBtnGeneralMode()) ;
			setState(STATE_UPDATE);

		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGeneralModeStep3::init() {
	CCLOG("SCTutorialGeneralModeStep3() init");
}

void SCTutorialGeneralModeStep3::release() {
	CCLOG("SCTutorialGeneralModeStep3() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	GuideMap * guideMap =(GuideMap *)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagGuideMapUI);
	if (guideMap != NULL)
		guideMap->removeCCTutorialIndicator();
}
