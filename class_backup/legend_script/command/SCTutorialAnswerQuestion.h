#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALANSWERQUESTION_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALANSWERQUESTION_H_

#include "../Script.h"

/**
 *答题教学
 *
 * @author yangjun
 * @date 2014-10-22
 */

//////////////////////点击活动按钮(用于答题)////////////////////////
class SCTutorialBtnActivity : public Script {
private:
	DECLARE_CLASS(SCTutorialBtnActivity)

public:
	SCTutorialBtnActivity();
	virtual ~SCTutorialBtnActivity();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};



//////////////////////点击参加答题////////////////////////
class SCTutorialBeginAnswer : public Script {
private:
	DECLARE_CLASS(SCTutorialBeginAnswer)

public:
	SCTutorialBeginAnswer();
	virtual ~SCTutorialBeginAnswer();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif
