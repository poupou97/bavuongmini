#include "SCTutorialGetFirstGeneral.h"
#include "../../utils/GameUtils.h"
#include "../../gamescene_state/GameSceneState.h"
#include "../../gamescene_state/MainScene.h"
#include "../../GameView.h"
#include "../CCTutorialIndicator.h"
#include "../../gamescene_state/sceneelement/shortcutelement/ShortcutConfigure.h"
#include "../../ui/extensions/RecruitGeneralCard.h"
#include "../../messageclient/element/CGeneralDetail.h"
#include "../../gamescene_state/EffectDispatch.h"


IMPLEMENT_CLASS(SCTutorialGetFirstGeneral)

SCTutorialGetFirstGeneral::SCTutorialGetFirstGeneral()
{
}

SCTutorialGetFirstGeneral::~SCTutorialGetFirstGeneral()
{
}

void* SCTutorialGetFirstGeneral::createInstance()
{
	return new SCTutorialGetFirstGeneral() ;
}

void SCTutorialGetFirstGeneral::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialGetFirstGeneral()");

			std::string content = mPara.at(0);
			
			if (!GameView::getInstance()->getGameScene())
				return;

			if (!GameView::getInstance()->getMainUIScene())
				return;

			//first recruit general --study
			EffectRecruit * recruitGeneral_ = new EffectRecruit();
			recruitGeneral_->setType(type_recruit);
			EffectDispatcher::pushEffectToVector(recruitGeneral_);

			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGetFirstGeneral::init() {
	CCLOG("SCTutorialGetFirstGeneral() init");
}

void SCTutorialGetFirstGeneral::release() {
	CCLOG("SCTutorialGetFirstGeneral() release");

}

/////////////////////////////////////////
IMPLEMENT_CLASS(SCTutorialGetSecondGeneral)

SCTutorialGetSecondGeneral::SCTutorialGetSecondGeneral()
{
}

SCTutorialGetSecondGeneral::~SCTutorialGetSecondGeneral()
{
}

void* SCTutorialGetSecondGeneral::createInstance()
{
	return new SCTutorialGetSecondGeneral() ;
}

void SCTutorialGetSecondGeneral::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialGetSecondGeneral()");

			std::string content = mPara.at(0);

			if (!GameView::getInstance()->getGameScene())
				return;

			if (!GameView::getInstance()->getMainUIScene())
				return;

			EffectRecruit * recruitGeneral_ = new EffectRecruit();
			recruitGeneral_->setType(type_recruit);
			EffectDispatcher::pushEffectToVector(recruitGeneral_);

			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGetSecondGeneral::init() {
}

void SCTutorialGetSecondGeneral::release() {
}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialGetFirstSkill)

SCTutorialGetFirstSkill::SCTutorialGetFirstSkill()
{
}

SCTutorialGetFirstSkill::~SCTutorialGetFirstSkill()
{
}

void* SCTutorialGetFirstSkill::createInstance()
{
	return new SCTutorialGetFirstSkill() ;
}

void SCTutorialGetFirstSkill::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialGetFirstSkill()");

			std::string content = mPara.at(0);

			EffectStudySkill * skillEffect_ = new EffectStudySkill();
			skillEffect_->setType(type_skillStudy);
			EffectDispatcher::pushEffectToVector(skillEffect_);
			setState(STATE_END);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialGetFirstSkill::init() {
	CCLOG("SCTutorialGetFirstSkill() init");
}

void SCTutorialGetFirstSkill::release() {
	CCLOG("SCTutorialGetFirstSkill() release");

}

/////////////////////////////////////////////////////////////////////

IMPLEMENT_CLASS(SCTutorialUseFirstSkill)

SCTutorialUseFirstSkill::SCTutorialUseFirstSkill()
{
}

SCTutorialUseFirstSkill::~SCTutorialUseFirstSkill()
{
}

void* SCTutorialUseFirstSkill::createInstance()
{
	return new SCTutorialUseFirstSkill() ;
}

void SCTutorialUseFirstSkill::update() {
	switch (mState) {
	case STATE_START:
		{
			CCLOG("excute SCTutorialUseFirstSkill()");
			std::string content = mPara.at(0);
			
			ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
			shortcutLayer->addCCTutorialIndicator(content.c_str(),shortcutLayer->getChildByTag(shortcutLayer->shortcurTag[1]+100)->getPosition(),CCTutorialIndicator::Direction_RD);
			shortcutLayer->registerScriptCommand(this->getId()) ; 
			this->setCommandHandler(shortcutLayer->getChildByTag(shortcutLayer->shortcurTag[1]+100)) ;
			setState(STATE_UPDATE);
		}
		break;

	case STATE_UPDATE:
		// wait the player to finish this tutorial
		break;

	case STATE_END:
		break;

	default:
		break;
	}
}

void SCTutorialUseFirstSkill::init() {
	CCLOG("SCTutorialUseFirstSkill() init");
}

void SCTutorialUseFirstSkill::release() {
	CCLOG("SCTutorialUseFirstSkill() release");

	if (!GameView::getInstance()->getGameScene())
		return;

	if (!GameView::getInstance()->getMainUIScene())
		return;

	ShortcutLayer * shortcutLayer = (ShortcutLayer*)GameView::getInstance()->getMainUIScene()->getChildByTag(kTagShortcutLayer);
	if(shortcutLayer != NULL)
		shortcutLayer->removeCCTutorialIndicator();
}