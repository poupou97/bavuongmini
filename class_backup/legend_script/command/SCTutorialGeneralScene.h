#ifndef _LEGEND_SCRIPT_COMMAND_TUTORIALGENERALSCENE_H_
#define _LEGEND_SCRIPT_COMMAND_TUTORIALGENERALSCENE_H_

#include "../Script.h"

/**
 * 武将界面教学
 *
 * @author yangjun
 * @date 2014-1-7
 */

//////////////////////////点将台////////////////////////////////////
class SCTutorialGeneralSceneStep1 : public Script {
private:
    DECLARE_CLASS(SCTutorialSkillSceneStep1)

public:
	SCTutorialGeneralSceneStep1();
	virtual ~SCTutorialGeneralSceneStep1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////极品招募/////////////////////////////

class SCTutorialGeneralSceneStep2 : public Script {
private:
    DECLARE_CLASS(SCTutorialGeneralSceneStep2)

public:
	SCTutorialGeneralSceneStep2();
	virtual ~SCTutorialGeneralSceneStep2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////我的武将//////////////////////////////

class SCTutorialGeneralSceneStep3 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep3)

public:
	SCTutorialGeneralSceneStep3();
	virtual ~SCTutorialGeneralSceneStep3();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

///////////////////////第一次上阵///////////////////////////////

class SCTutorialGeneralSceneStep4 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep4)

public:
	SCTutorialGeneralSceneStep4();
	virtual ~SCTutorialGeneralSceneStep4();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////第一次出战//////////////////////////////

class SCTutorialGeneralSceneStep5 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep5)

public:
	SCTutorialGeneralSceneStep5();
	virtual ~SCTutorialGeneralSceneStep5();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////////////////////////////////

class SCTutorialGeneralSceneStep6 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep6)

public:
	SCTutorialGeneralSceneStep6();
	virtual ~SCTutorialGeneralSceneStep6();

	virtual bool isBlockFunction() { return true; };

	virtual void endCommand(CCObject* pSender);

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////优秀招募/////////////////////////////

class SCTutorialGeneralSceneStep7 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep7)

public:
	SCTutorialGeneralSceneStep7();
	virtual ~SCTutorialGeneralSceneStep7();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////第二次上阵(上阵第三个武将)////////////////////////////

class SCTutorialGeneralSceneStep8 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep8)

public:
	SCTutorialGeneralSceneStep8();
	virtual ~SCTutorialGeneralSceneStep8();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

//////////////////////////第二次出战////////////////////////////

class SCTutorialGeneralSceneStep9 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep9)

public:
	SCTutorialGeneralSceneStep9();
	virtual ~SCTutorialGeneralSceneStep9();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////选择第一个技能////////////////////////////

class SCTutorialGeneralSceneStep10 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep10)

public:
	SCTutorialGeneralSceneStep10();
	virtual ~SCTutorialGeneralSceneStep10();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

/////////////////////////选择龙魂按钮////////////////////////////

class SCTutorialGeneralSceneStep11 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep11)

public:
	SCTutorialGeneralSceneStep11();
	virtual ~SCTutorialGeneralSceneStep11();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////关闭单次招募结果界面//////////////////////////////

class SCTutorialGeneralSceneStep12 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep12)

public:
	SCTutorialGeneralSceneStep12();
	virtual ~SCTutorialGeneralSceneStep12();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////阵法//////////////////////////////

class SCTutorialGeneralSceneStep13 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep13)

public:
	SCTutorialGeneralSceneStep13();
	virtual ~SCTutorialGeneralSceneStep13();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////学习阵法//////////////////////////////

class SCTutorialGeneralSceneStep14 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep14)

public:
	SCTutorialGeneralSceneStep14();
	virtual ~SCTutorialGeneralSceneStep14();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////启用阵法//////////////////////////////

class SCTutorialGeneralSceneStep15 : public Script {
private:
	DECLARE_CLASS(SCTutorialGeneralSceneStep15)

public:
	SCTutorialGeneralSceneStep15();
	virtual ~SCTutorialGeneralSceneStep15();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

////////////////////////学习阵法（阵法升级界面）//////////////////////////////

class SCTutorialStrategiesUpgradeStep1 : public Script {
private:
	DECLARE_CLASS(SCTutorialStrategiesUpgradeStep1)

public:
	SCTutorialStrategiesUpgradeStep1();
	virtual ~SCTutorialStrategiesUpgradeStep1();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};


////////////////////////关闭界面（阵法升级界面）//////////////////////////////

class SCTutorialStrategiesUpgradeStep2 : public Script {
private:
	DECLARE_CLASS(SCTutorialStrategiesUpgradeStep2)

public:
	SCTutorialStrategiesUpgradeStep2();
	virtual ~SCTutorialStrategiesUpgradeStep2();

	virtual bool isBlockFunction() { return true; };

	static void* createInstance() ;

	virtual void update();

	virtual void init();

	virtual void release();
};

#endif