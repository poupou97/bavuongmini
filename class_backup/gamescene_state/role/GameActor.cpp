#include "GameActor.h"

#include "../GameSceneState.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "AppMacros.h"

//const float DEFAULT_ANGLES_DIRECTION_DATA[] = {
//	22.5f, 67.5f, 112.5f, 157.5f, 202.5f, 247.5f, 292.5f, 337.5f
//	//30.f, 60.f, 120.f, 150.f, 210.0f, 240.f, 300.f, 330.0f
//	//35.f, 60.f, 120.f, 145.f, 215.0f, 240.f, 300.f, 325.0f
//	//35.f, 65.f, 115.f, 145.f, 215.0f, 245.f, 295.f, 325.0f
//	//30.f, 65.f, 115.f, 150.f, 30+180, 65+180.f, 115+180.f, 150+180.f
//	//39.6f, 78.f, 102.f, 140.4f, 39.6f+180.f, 78.f+180.f, 102.f+180.f, 140.f+180.f
//};

const float DEFAULT_ANGLES_DIRECTION_DATA[] = {
	//45.f, 135.f, 225.f, 315.f
	30.f, 150.f, 210.f, 330.f
};

GameActor::GameActor()
: m_worldPosition(ccp(0, 0))
, m_animScale(1.0f)
, m_layerId(1)
, m_bIsDestroy(false)
, paramData(NULL)
, m_gameScene(NULL)
, m_lockedActorId(NULL_ROLE_ID)
, m_lockedOwnerId(NULL_ROLE_ID)
, m_reflectionEnabled(false)
, m_worldPositionZ(0)
, m_animActionID(0)
{
	setCascadeOpacityEnabled(true);   // opacity可作用于子结点
}

GameActor::~GameActor()
{
	CC_SAFE_DELETE_ARRAY(paramData);
}

void GameActor::setPosition(const CCPoint &position)
{
	if(m_gameScene != NULL)
	{
		if(m_gameScene->isSynchronizeCocosPosition())
		{
			CCNode::setPosition(position);
		}
		else
		{
			// ONLY schronize the world position
			CCPoint worldPosition = m_gameScene->convertToGameWorldSpace(position);
			m_worldPosition.setPoint(worldPosition.x, worldPosition.y);
		}
	}
	else
	{
		CCNode::setPosition(position);
	}
}

void GameActor::setPositionImmediately(const CCPoint &position)
{
	CCNode::setPosition(position);
}

void GameActor::setWorldPosition(CCPoint newWorldPosition)
{
	if(m_gameScene == NULL)
	{
		m_worldPosition.setPoint(newWorldPosition.x, newWorldPosition.y);
	}
	else
	{
		// the actor must not be out of the map, if out of map, re-set it
		if(newWorldPosition.x < 0)
			newWorldPosition.x = 0;
		if(newWorldPosition.y < 0)
			newWorldPosition.y = 0;
		if(newWorldPosition.x >= m_gameScene->getMapSize().width)
			newWorldPosition.x = m_gameScene->getMapSize().width - 1;
		if(newWorldPosition.y >= m_gameScene->getMapSize().height)
			newWorldPosition.y = m_gameScene->getMapSize().height - 1;

		m_worldPosition.setPoint(newWorldPosition.x, newWorldPosition.y);

		// synchronize other scene info
		this->updateCoverStatus();
		this->updateWaterStatus();
	}
}
CCPoint GameActor::getWorldPosition()
{
	return m_worldPosition;
}

void GameActor::setWorldPositionZ(float z)
{
	m_worldPositionZ = z;
}
float GameActor::getWorldPositionZ()
{
	return m_worldPositionZ;
}

int GameActor::getType()
{
	return type;
}
void GameActor::setType(int typeValue)
{
	type = typeValue;
}

bool GameActor::hasLockedActor() 
{
	return m_lockedActorId != NULL_ROLE_ID;
}
GameActor* GameActor::getLockedActor() 
{
	if (!hasLockedActor()) {
		return NULL;
	}

	return m_gameScene->getActor(m_lockedActorId);
}
void GameActor::setLockedActorId(long long lockedActorId) 
{ 
	m_lockedActorId = lockedActorId; 
	if(lockedActorId == NULL_ROLE_ID)
		return;

	GameActor* lockedActor = m_gameScene->getActor(m_lockedActorId);
	if(lockedActor != NULL)
		lockedActor->setLockedOwnerId(this->getRoleId());
};

void GameActor::showLockedCircle(bool bShow, const char* circleName)
{
	if(bShow)
	{
		if(circleName == NULL)
			return;

		// create the locked circle node
		CCLegendAnimation* pCircle = CCLegendAnimation::create(circleName);
		pCircle->setPlayLoop(true);
		pCircle->setReleaseWhenStop(false);
		pCircle->setPlaySpeed(0.6f);
		addChild(pCircle, GameActor::ACTOR_LOCKED_CIRCLE_ZORDER, kTagLockedCircle);
	}
	else
	{
		// remove the circle
		CCNode* pNode = this->getChildByTag(GameActor::kTagLockedCircle);
		if(pNode != NULL)
			pNode->removeFromParent();
	}
}



CCLabelTTF* GameActor::createNameLabel(const char* name)
{
	ccColor3B tintColorRed      =  { 255, 0, 0   };
    ccColor3B tintColorYellow   =  { 255, 255, 0 };
    ccColor3B tintColorWhite     =  { 255, 255, 255   };
    ccColor3B strokeColor       =  { 0, 10, 255  };
    ccColor3B strokeShadowColor =  { 0, 0, 0   };

	// create the label stroke and shadow
    ccFontDefinition strokeShaodwTextDef;
    strokeShaodwTextDef.m_fontSize = GAMEACTOR_NAME_FONT_SIZE;
    strokeShaodwTextDef.m_fontName = std::string(APP_FONT_NAME);
    
    //strokeShaodwTextDef.m_stroke.m_strokeEnabled = true;
    //strokeShaodwTextDef.m_stroke.m_strokeColor   = strokeShadowColor;
    //strokeShaodwTextDef.m_stroke.m_strokeSize    = 1.5;
    
    strokeShaodwTextDef.m_shadow.m_shadowEnabled = true;
    strokeShaodwTextDef.m_shadow.m_shadowOffset  = CCSizeMake(1.0f, -1.0f);
    strokeShaodwTextDef.m_shadow.m_shadowOpacity = 1.0;
    strokeShaodwTextDef.m_shadow.m_shadowBlur    = 1.0;
    strokeShaodwTextDef.m_shadow.m_shadowColor   = strokeShadowColor;
    
    ccColor3B nameColor = { 52, 255, 53 };
    strokeShaodwTextDef.m_fontFillColor   = nameColor;
    
    // shadow + stroke label
    CCLabelTTF* nameLabel = CCLabelTTF::createWithFontDefinition(name, strokeShaodwTextDef);
	nameLabel->setTag(kTagActorName);

	//CCLabelTTF* nameLabel = CCLabelTTF::create(name, "Arial", 24);
	//nameLabel->setTag(kTagActorName);

	return nameLabel;
}

void GameActor::showActorName(bool bShow)
{
	// kTagActorName
}
std::string GameActor::getActorName()
{
	return m_ActorName;
}
void GameActor::setActorName(const char* name)
{
	m_ActorName = name;
}

CCPoint GameActor::getSortPoint()
{
	//CCAssert(paramData != NULL, "data should not be null");
	if(paramData == NULL)
		return m_worldPosition;

	return ccp(paramData[0] + m_worldPosition.x, paramData[1] + m_worldPosition.y);
}

float GameActor::getAnimScale()
{
	return m_animScale;
}

//int GameActor::getAnimDirection()
//{
//	return getAnimDirection(ccp(0, 0), m_moveDir, DEFAULT_ANGLES_DIRECTION_DATA);
//}

/**
 * 根据2点间X，Y偏差得到2点的方向关系
 */
//int GameActor::getAnimDirection(CCPoint from, CCPoint to, const float* angles_data) {
//	float o = to.x - from.x;
//    float a = to.y - from.y;
//
//	// the joystick is not moved
//	if(o == 0 && a == 0)
//		return -1;
//
//	float degree = (float) CC_RADIANS_TO_DEGREES( atan2f( o, a) );
//	if(o < 0)
//	{
//		degree += 360;
//	}
//	//CCLog("joystick angle: (%f)", at);
//
//	int direction = -1;
//	if (degree >= angles_data[7] || degree < angles_data[0]) {// up
//		direction = 0;
//	} else if (degree >= angles_data[6] && degree < angles_data[7]) {// left up
//		direction = 1;
//	} else if (degree >= angles_data[5] && degree < angles_data[6]) {// left
//		direction = 2;
//	} else if (degree >= angles_data[4] && degree < angles_data[5]) {// left down
//		direction = 3;
//	} else if (degree >= angles_data[3] && degree < angles_data[4]) {// down
//		direction = 4;
//	} else if (degree >= angles_data[2] && degree < angles_data[3]) {// right down
//		direction = 5;
//	} else if (degree >= angles_data[1] && degree < angles_data[2]) {// right
//		direction = 6;
//	} else if (degree >= angles_data[0] && degree < angles_data[1]) {// up right
//		direction = 7;
//	}
//	//CCLog("joystick direction: (%d)", direction);
//
//	return direction;
//}

/**
 * 根据2点间X，Y偏差得到2点的方向关系
 */
int GameActor::getAnimDirection(CCPoint from, CCPoint to, const float* angles_data) {
	float o = to.x - from.x;
    float a = to.y - from.y;

	// the joystick is not moved
	if(o == 0 && a == 0)
		return -1;

	float degree = (float) CC_RADIANS_TO_DEGREES( atan2f( o, a) );
	if(o < 0)
	{
		degree += 360;
	}
	//CCLog("joystick angle: (%f)", at);

	int direction = -1;
	if (degree >= angles_data[3] || degree < angles_data[0]) {// up
		direction = 0;
	} else if (degree >= angles_data[2] && degree < angles_data[3]) {// left
		direction = 1;
	} else if (degree >= angles_data[1] && degree < angles_data[2]) {// down
		direction = 2;
	} else if (degree >= angles_data[0] && degree < angles_data[1]) {// right
		direction = 3;
	}
	//CCLog("animation direction: (%d)", direction);

	return direction;
}

void GameActor::updateCoverStatus()
{
	// 检测是否碰到了Cover
	int tileX = m_gameScene->positionToTileX(getWorldPosition().x);
	int tileY = m_gameScene->positionToTileY(getWorldPosition().y);
	if(m_gameScene->checkInMap(tileX, tileY))
	{
		if(m_gameScene->isCover(tileX, tileY))
			this->setOpacity(80);
		else
			this->setOpacity(255);
	}
}

void GameActor::updateWaterStatus()
{
	int tileX = m_gameScene->positionToTileX(getWorldPosition().x);
	int tileY = m_gameScene->positionToTileY(getWorldPosition().y);
	if(m_gameScene->checkInMap(tileX, tileY))
	{
		if(m_gameScene->isWater(tileX, tileY))
			setReflection(true);
		else
			setReflection(false);
	}
}

CCRect GameActor::getVisibleRect()
{
	return boundingBox();
}

bool GameActor::isDestroy() {
	return m_bIsDestroy;
}
void GameActor::setDestroy(bool bDestroy) {
	this->m_bIsDestroy = bDestroy;
}