#include "Monster.h"

#include "MonsterOwnedStates.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "GameActorAnimation.h"
#include "../GameSceneState.h"
#include "../skill/GameFightSkill.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "BaseFighterOwnedCommand.h"
#include "../../utils/GameUtils.h"
#include "BaseFighterOwnedStates.h"
#include "../GameScene.h"
#include "BaseFighterConstant.h"
#include "../../messageclient/element/CMonsterBaseInfo.h"
#include "BasePlayer.h"
#include "../../utils/StaticDataManager.h"
#include "../sceneelement/TargetInfoMini.h"
#include "AppMacros.h"

#define	kTagMonsterName 100
#define kTagMonsterLevel 101

#define BOSS_CHATBUBBLE_OFFSETY 144

static std::string MonsterAnimActionNameList[] =
{
    "stand",
    "run",
    "attack",
    "stand",   // die animation is the same as stand animation
}; 

Monster::Monster()
: m_DeathAnim(NULL)
, m_deathEffect(NULL)
, m_clazz(-1)
, m_nDeadStatus(0)
{
	//this->scheduleUpdate();
	setType(GameActor::type_monster);

	m_resPath = "animation/monster/";

	//setMoveSpeed(40);

	// the statemachine init must be put at the end
	// because some animations' instance will be used in the state::Enter() function
	m_pStateMachine = new StateMachine<Monster>(this);
    //m_pStateMachine->SetCurrentState(MyPlayerStand::Instance());
	//m_pStateMachine->ChangeState(MyPlayerStand::Instance());
    m_pStateMachine->SetGlobalState(MonsterGlobalState::Instance());

	mActiveRole = new ActiveRole();
}

Monster::~Monster()
{
	delete m_pStateMachine;

	delete mActiveRole;
}

CCPoint Monster::getSortPoint()
{
	// 计算排序点
	return ccp(m_worldPosition.x, m_worldPosition.y);
}

CCRect Monster::getVisibleRect()
{
	// 
	//CCLegendAnimation* anim = this->m_pAnim->getAnim(ANI_COMPONENT_BODY, this->getAnimIdx());
	//return anim->boundingBox();

	//this->setContentSize(CCSizeMake(100, 100));
	//CCSize size = this->getContentSize();

	int width = 60;
	int height = 100;
	CCRect tmpRect = CCRectMake(-width/2, 0, width, height);
	return tmpRect;
	//return CCRectMake(this->getWorldPosition().x + tmpRect.origin.x, this->getWorldPosition().y + tmpRect.origin.y,
	//	100, 100);
}

bool Monster::init(const char* monsterName)
{
	m_pFootEffectContainer = CCNodeRGBA::create();
	m_pFootEffectContainer->setCascadeOpacityEnabled(true);
	this->addChild(m_pFootEffectContainer, GameActor::ACTOR_FOOTEFFECT_ZORDER);
	m_pFootEffectContainer->setTag(BaseFighter::kTagFootEffectContainer);

	addShadow();

	// load animation
	//m_pAnim = new CCLegendAnimation();
	//std::string animFileName = "animation/monster/";
	//animFileName.append(monsterName);
	//animFileName.append("/");
	//animFileName.append("stand");
	//animFileName.append(".anm");
	//m_pAnim->initWithAnmFilename(animFileName.c_str());
	//m_animActionID = 0;
	//m_pAnim->setAction(m_animActionID);
	//m_pAnim->retain();
	//addChild(m_pAnim, 2);

	//m_pAnim = new GameActorAnimation();
	//m_pAnim->load(m_resPath.c_str(), MonsterAnimActionNameList, monsterName, ANI_COMPONENT_MAX, MONSTER_ANI_MAX);
	m_pAnim = GameActorAnimation::create(m_resPath.c_str(), MonsterAnimActionNameList, monsterName, ANI_COMPONENT_MAX, MONSTER_ANI_MAX);
	m_pAnim->setAnimName(MONSTER_ANI_STAND);
	m_pAnim->showComponent(ANI_COMPONENT_REFLECTION, false);
	this->addChild(m_pAnim, GameActor::ACTOR_BODY_ZORDER);

	m_pHeadEffectContainer = CCNodeRGBA::create();
	m_pHeadEffectContainer->setCascadeOpacityEnabled(true);
	this->addChild(m_pHeadEffectContainer, GameActor::ACTOR_HEAD_ZORDER);
	m_pHeadEffectContainer->setTag(BaseFighter::kTagHeadEffectContainer);

	//// death animation
	//m_deathEffect = CCParticleSystemQuad::create("animation/texiao/renwutexiao/death/xuedi.plist");
	//m_deathEffect->setPositionType(kCCPositionTypeGrouped);
	//m_deathEffect->setPosition(ccp(0,0));
	//m_deathEffect->setScale(1.0f);
	//m_deathEffect->setVisible(true);
	//m_deathEffect->stopSystem();
	//addChild(m_deathEffect, ACTOR_HEAD_ZORDER);

	//m_DeathAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/daodi2.anm");
	//m_DeathAnim->setScale(0.4f);
	////m_DeathAnim = CCLegendAnimation::create("animation/texiao/renwutexiao/death/death.anm");
	//m_DeathAnim->setVisible(false);
	//m_DeathAnim->setReleaseWhenStop(false);
	//m_DeathAnim->stop();
	//this->addChild(m_DeathAnim, GameActor::ACTOR_HEAD_ZORDER);

	// 根据ANI_STAND，大致确定Actor的大小
	CCLegendAnimation* anim = this->m_pAnim->getAnim(ANI_COMPONENT_BODY, MONSTER_ANI_STAND);
	//return anim->boundingBox();

	//setVisibleRect();

	this->setContentSize(CCSizeMake(64, 100));

	this->setScale(BASEFIGHTER_BASE_SCALE);

	this->GetFSM()->ChangeState(MonsterStand::Instance());

	return true;
}

/*
void Monster::showActorName(bool bShow)
{
	std::string namestr = getActorName();
	const char* name = namestr.c_str();

	CCRichLabel* nameLabel = (CCRichLabel*)this->getChildByTag(GameActor::kTagActorName);
	if(bShow)
	{
		if(nameLabel == NULL)
		{
			//nameLabel = this->createNameLabel(name);

			nameLabel = CCRichLabel::createWithString(name, CCSizeMake(256,64),NULL,NULL);
			nameLabel->setTag(GameActor::kTagActorName);
			nameLabel->setAnchorPoint(ccp(0.5f,0.5f));
			
			// 设置名字位置时，先判断怪物头顶是否有血条
			TargetInfoMini* pBloodBar = (TargetInfoMini*)this->getChildByTag(kTagBloodBar);
			if(pBloodBar == NULL)
			{
				nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT));
			}
			else
			{
				nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT + 16));
			}

			this->addChild(nameLabel, GameActor::ACTOR_NAME_ZORDER);
		}
		else
		{
			nameLabel->setString(name);
		}
	}
	else
	{
		if(nameLabel != NULL)
			nameLabel->removeFromParent();
	}
}
*/

void Monster::showActorName(bool bShow)
{
	std::string namestr = getActorName();
	const char* name = namestr.c_str();
	char _level[10];
	sprintf(_level,"%d", this->getActiveRole()->level());
	std::string strLevel = _level;
	strLevel.append(StringDataManager::getString("fivePerson_ji"));

	CCNode* actorNamePanel = this->getChildByTag(GameActor::kTagActorName);
	if(bShow)
	{
		CCRichLabel* nameLabel = NULL;
		CCLabelTTF* levelLabel = NULL;
		if(actorNamePanel == NULL)
		{
			//nameLabel = this->createNameLabel(name);

			actorNamePanel = CCNode::create();
			actorNamePanel->setTag(GameActor::kTagActorName);
			actorNamePanel->setAnchorPoint(ccp(0.5f, 0.5f));

			nameLabel = CCRichLabel::createWithString(name, CCSizeMake(256,64),NULL,NULL);
			nameLabel->setTag(kTagMonsterName);
			nameLabel->setAnchorPoint(ccp(0.f,0.5f));
			nameLabel->setPosition(ccp(-nameLabel->getContentSize().width/2, 0));

			// add the role's level
			levelLabel = CCLabelTTF::create(strLevel.c_str(),APP_FONT_NAME,15);   // GAMEACTOR_NAME_FONT_SIZE
			levelLabel->setTag(kTagMonsterLevel);
			levelLabel->setAnchorPoint(ccp(0.f,0.5f));
			levelLabel->setPosition(ccp(nameLabel->getContentSize().width/2, 0));

			actorNamePanel->addChild(nameLabel);
			actorNamePanel->addChild(levelLabel);

			// 设置名字位置时，先判断怪物头顶是否有血条
			TargetInfoMini* pBloodBar = (TargetInfoMini*)this->getChildByTag(kTagBloodBar);
			if(pBloodBar == NULL)
				actorNamePanel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT));
			else
				actorNamePanel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT + 16));

			this->addChild(actorNamePanel, GameActor::ACTOR_NAME_ZORDER);
		}
		else
		{
			nameLabel = (CCRichLabel*)actorNamePanel->getChildByTag(kTagMonsterName);
			nameLabel->setString(name);
			nameLabel->setPosition(ccp(-nameLabel->getContentSize().width/2, 0));

			// set the role's level
			levelLabel = (CCLabelTTF*)actorNamePanel->getChildByTag(kTagMonsterLevel);
			levelLabel->setString(strLevel.c_str());
			levelLabel->setPosition(ccp(nameLabel->getContentSize().width/2, 0));
		}
	}
	else
	{
		if(actorNamePanel != NULL)
			actorNamePanel->removeFromParent();
	}
}

void Monster::modifyActorNameColor(ccColor3B color)
{
	char colorChars[7];
	sprintf(colorChars, "%02x%02x%02x", color.r, color.g, color.b);
	//char red[2];
	//sprintf(red, "%x", color.r);
	//char green[2];
	//sprintf(green, "%x", color.g);
	//char blue[2];
	//sprintf(blue, "%x", color.b);

	//std::string colorStr;
	//colorStr.assign(colorChars, 6);
	std::string newNameStr = "/#";
	//newNameStr.append(colorStr);
	newNameStr.append(colorChars);
	//newNameStr.append(red);
	//newNameStr.append(green);
	//newNameStr.append(blue);
	newNameStr.append(" ");

	std::string wholeName = getActorName();
	std::string nameStrWithoutColor = wholeName;
	int index = wholeName.find("/#");
	if(index >= 0)   // contain color format
	{
		const int COLOR_FORMAT_HEAD_LEN = 9;   // "/#ff00ff "
		nameStrWithoutColor = wholeName.substr(COLOR_FORMAT_HEAD_LEN, wholeName.size()-1-COLOR_FORMAT_HEAD_LEN);   // /#ff00ff DOG/
	}

	newNameStr.append(nameStrWithoutColor);
	newNameStr.append("/");

	this->setActorName(newNameStr.c_str());
}

void Monster::onDamaged(BaseFighter* source, int damageNum, bool bDoubleAttacked, CSkillResult* skillResult)
{
	BaseFighter::onDamaged(source, damageNum, bDoubleAttacked, skillResult);

	// 由于伤害由我方造成，名字也变色
	if(source != NULL && source->isMyPlayerGroup())
	{
		// modify its name's color
		std::string oldName = this->getActorName();
		modifyActorNameColor(ccc3(255, 0, 0));

		bool bShowName = false;
		if(oldName != this->getActorName())
			bShowName = true;
		CCNode* actorNamePanel = this->getChildByTag(GameActor::kTagActorName);
		if(actorNamePanel == NULL)
			bShowName = true;
		CCNode* pNode = this->getChildByTag(GameActor::kTagLockedCircle);
		if(pNode != NULL && bShowName)
		{
			this->showActorName(true);
		}
	}
}

void Monster::onAttack(const char* skillId, const char* skillProcessorId, long long targetId, bool bDoubleAttack, int skillType)
{
	BaseFighter::onAttack(skillId, skillProcessorId, targetId, bDoubleAttack, skillType);

	// the boss in the instance
	if(this->getClazz() == CMonsterBaseInfo::clazz_boss)
	{
		// the monster's default skill is same as Player
		if(BasePlayer::isDefaultAttack(skillId))
			return;

		// when using skill, this actor will be bigger in a short duration
		CCFiniteTimeAction*  action = CCSequence::create(
			CCScaleTo::create(0.4f,1.3f),   // 1.8f
			CCDelayTime::create(2.0f),
			CCScaleTo::create(0.4f,1.0f),
			NULL);
		action->setTag(BASEFIGHTER_GROW_ACTION);
		this->getAnim()->runAction(action);
	}

	// all bosses
	if(this->getClazz() == CMonsterBaseInfo::clazz_boss || this->getClazz() == CMonsterBaseInfo::clazz_world_boss)
	{
		// the monster's default skill is same as the Player
		if(BasePlayer::isDefaultAttack(skillId))
			return;

		// show chat bubble
		CCNode* container = this->getChildByTag(GameActor::kTagHeadEffectContainer);
		CCNode* pChatBubbleNode = container->getChildByTag(GameActor::kTagChatBubble);
		if(pChatBubbleNode == NULL)
		{
			int size = MonsterDialogData::s_monsterDialogData.size();
			if(size > 0)
			{
				int index = CCRANDOM_0_1() * size;
				if(index >= 0 && index < size)
				{
					this->addChatBubble(MonsterDialogData::s_monsterDialogData[index].c_str(), 2.0f, BOSS_CHATBUBBLE_OFFSETY);
				}
			}
		}
	}
}

void Monster::changeAction(int action)
{
	switch(action)
	{
	case ACT_STAND:
		GetFSM()->ChangeState(MonsterStand::Instance());
		break;

	case ACT_RUN:
		GetFSM()->ChangeState(MonsterRun::Instance(), false);
		break;

	case ACT_ATTACK:
		GetFSM()->ChangeState(MonsterAttack::Instance());
		break;

	case ACT_DIE:
		GetFSM()->ChangeState(MonsterDeath::Instance());
		stopAllCommand();
		break;

	case ACT_BE_KNOCKBACK:
		GetFSM()->ChangeState(MonsterKnockBack::Instance());
		stopAllCommand();
		break;

	case ACT_CHARGE:
		GetFSM()->ChangeState(MonsterCharge::Instance());
		stopAllCommand();   // pay attention, stop all commands when begin to chage
		break;

	case ACT_JUMP:
		GetFSM()->ChangeState(MonsterJump::Instance());
		break;

	case ACT_DISAPPEAR:
		GetFSM()->ChangeState(MonsterDisappear::Instance());
		stopAllCommand();
		break;
	}
}

bool Monster::isAction(int action)
{
	switch(action)
	{
	case ACT_STAND:
		if(GetFSM()->isInState(*(MonsterStand::Instance())))
			return true;
		return false;
		break;

	case ACT_RUN:
		if(GetFSM()->isInState(*(MonsterRun::Instance())))
			return true;
		return false;
		break;

	case ACT_ATTACK:
		if(GetFSM()->isInState(*(MonsterAttack::Instance())))
			return true;
		return false;
		break;

	case ACT_DIE:
		if(GetFSM()->isInState(*(MonsterDeath::Instance())))
			return true;
		return false;
		break;

	case ACT_BE_KNOCKBACK:
		if(GetFSM()->isInState(*(MonsterKnockBack::Instance())))
			return true;
		return false;
		break;

	case ACT_CHARGE:
		if(GetFSM()->isInState(*(MonsterCharge::Instance())))
			return true;
		return false;
		break;

	case ACT_JUMP:
		if(GetFSM()->isInState(*(MonsterJump::Instance())))
			return true;
		return false;
		break;

	case ACT_DISAPPEAR:
		if(GetFSM()->isInState(*(MonsterDisappear::Instance())))
			return true;
		return false;
		break;
	}

	//CCAssert(false, "action has not been handled");
	return false;
}

bool Monster::isStanding()
{
	if(GetFSM()->isInState(*(MonsterStand::Instance())))
		return true;

	return false;
}

bool Monster::isDead()
{
	if(GetFSM()->isInState(*(MonsterDeath::Instance())))
		return true;

	return false;
}

bool Monster::isAttacking() 
{
	if( GetFSM()->isInState( *(MonsterAttack::Instance()) ) )
		return true;

	return false;
}

void Monster::setBornPosition(CCPoint pos)
{
	m_bornPosition = pos;
}
CCPoint Monster::getBornPosition()
{
	return m_bornPosition;
}

void Monster::onEnter()
{
	CCNode::onEnter();
}
void Monster::onExit()
{
	CCNode::onExit();
}

void Monster::onBorn()
{
	CCFadeIn *fadeInAction = CCFadeIn::create(0.1f);
	this->runAction(fadeInAction);

	// born animation
	CCParticleSystemQuad* pParticleEffect = CCParticleSystemQuad::create("animation/texiao/particledesigner/born.plist");
	pParticleEffect->setPositionType(kCCPositionTypeGrouped);
	pParticleEffect->setPosition(ccp(0,0));
	pParticleEffect->setScale(1.0f);
	pParticleEffect->setAutoRemoveOnFinish(true);
	this->addChild(pParticleEffect, GameActor::ACTOR_HEAD_ZORDER);
}

void Monster::update(float dt)
{
	drive();

	this->running(dt);

	this->GetFSM()->Update();
}


#if ROLE_MONSTER_DEBUG_DRAW
void Monster::draw()
{
    BaseFighter::draw();

    //const CCSize& s = this->getContentSize();
	CCRect rect = getVisibleRect();
    CCPoint vertices[4]={
		ccp(rect.origin.x,rect.origin.y),
		ccp(rect.origin.x + rect.size.width, rect.origin.y),
        ccp(rect.origin.x + rect.size.width, rect.origin.y + rect.size.height),
		ccp(rect.origin.x, rect.origin.y + rect.size.height),
    };

	//rect.size.width = 100;
	//rect.size.height = 100;
	//CCSize s = rect.size;
 //   CCPoint vertices[4]={
 //       ccp(0,0),ccp(rect.size.width,0),
 //       ccp(s.width,s.height),ccp(0,s.height),
 //   };

    ccDrawPoly(vertices, 4, true);
}
#endif // CC_LABELBMFONT_DEBUG_DRAW

void Monster::updateWander()
{
	ActionStandContext* context = (ActionStandContext*)getActionContext(ACT_STAND);
	if(GameUtils::millisecondNow() - context->idleStartTime > 3000)
	{
		context->idleStartTime = GameUtils::millisecondNow();
        
        if(isDead())
            return;

		float random = CCRANDOM_0_1();
		if(random < 0.4f)
			return;

		const int range = 128;

		setMoveSpeed(100.f);

		// find the wander target
		random = CCRANDOM_0_1() - 0.5f;
		float x = m_bornPosition.x + random * range;
		random = CCRANDOM_0_1() - 0.5f;
		float y = m_bornPosition.y + random * range;

		BaseFighterCommandMove* cmd = new BaseFighterCommandMove();
		cmd->targetPosition = ccp(x, y);
		setNextCommand(cmd, false);

		// wander more
		BaseFighterCommandMove* parentCommand = cmd;
		const int searchPathTimes = 4;
		int number = CCRANDOM_0_1() * searchPathTimes;
		for(int i = 0; i < number; i++)
		{
			// find the wander target
			random = CCRANDOM_0_1() - 0.5f;
			x = m_bornPosition.x + random * range;
			random = CCRANDOM_0_1() - 0.5f;
			y = m_bornPosition.y + random * range;

			BaseFighterCommandMove* moreCommand = new BaseFighterCommandMove();
			moreCommand->targetPosition = ccp(x, y);
			parentCommand->setNextCommand(moreCommand);

			parentCommand = moreCommand;
		}
	}
}

void Monster::updateWorldPosition()
{
	if(isAction(ACT_STAND))
	{
		// schronize the real world position
		if(!this->getWorldPosition().equals(this->getRealWorldPosition()))
		{
			// "假击退"(比如被群体技能命中后)，该状态下，暂时不进行位置的同步
			if(this->getActionByTag(BASEFIGHTER_HITBACK_ACTION) != NULL
				|| this->getActionByTag(BASEFIGHTER_FLYUP_ACTION) != NULL)
				return;
			// the position of the monster has not been synchronized
			CCPoint cocos2dPos = this->getGameScene()->convertToCocos2DSpace(this->getWorldPosition());
			if(!cocos2dPos.equals(this->getPosition()))
				return;
			//// too close to his real postion, so ignore
			//if(ccpDistance(this->getWorldPosition(), this->getRealWorldPosition()) <= 64)
			//	return;

			BaseFighterCommandMove* cmd = new BaseFighterCommandMove();
			this->setMoveSpeed(BASEFIGHTER_BASIC_MOVESPEED);
			//cmd->moveSpeed = 300.0f;
			cmd->targetPosition = this->getRealWorldPosition();
			this->setNextCommand(cmd, false);
		}
	}
}

int Monster::getClazz()
{
	return m_clazz;
}
void Monster::setClazz(int clazz)
{
	m_clazz = clazz;
}

void Monster::addDeadStatus()
{
	m_nDeadStatus++;
}

bool Monster::isFirstDead()
{
	if (m_nDeadStatus == 1)
		return true;

	return false;
}
