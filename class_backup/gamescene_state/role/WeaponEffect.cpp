#include "WeaponEffect.h"

#include "../../legend_engine/CCLegendAnimation.h"
#include "BasePlayer.h"

//#define PLAYER_ANIM_WEAPON_IMG_IDX 2
#define PLAYER_ANIM_WEAPON_AMOD_IDX 0

#define MAX_WEAPON_PER_ROLE 2   // 由于豪杰的双刀，所以是2

std::map<std::string, int> WeaponEffect::s_effectNameId;

std::vector<WeaponEffectCache> WeaponEffect::s_effectCache;

void WeaponEffect::attach(WeaponEffectDefine& effectDef, int weaponType, CCLegendAnimation* roleAnim)
{
	if(effectDef.weaponEffectType == GAME_WEAPON_EFFECT_TYPE_FIRE)
	{
		WeaponEffectFire::attach(effectDef, weaponType, roleAnim);
		return;
	}
	else if(effectDef.weaponEffectType == GAME_WEAPON_EFFECT_TYPE_SPARKLE)
	{
		WeaponEffectSparkle::attach(effectDef, weaponType, roleAnim);
		return;
	}
	CCAssert(false, "other weapon effect has not been implemented");
}

void WeaponEffect::removeWeaponEffect(CCLegendAnimation* roleAnim, const char* effectName)
{
	if(roleAnim == NULL)
		return;

	//int imgIdx = PLAYER_ANIM_WEAPON_IMG_IDX;
	int imgIdx = BasePlayer::getWeaponImgIdx(roleAnim);
    if(imgIdx == -1)
        return;

	for(int i = 0; i < MAX_WEAPON_PER_ROLE; i++)
	{
		CCLegendAModule* pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, i);
		if(pWeaponAModule == NULL)
			continue;

		CCArray* pArray = pWeaponAModule->getChildren();
		std::vector<CCNode*> needRemoveVector;
		CCObject* pObj = NULL;
		CCARRAY_FOREACH(pArray, pObj)
		{
			CCNode* pNode = (CCNode*)(pObj);
			if(pNode != NULL && pNode->getUserData() != NULL)
			{
				// it's the wanted weapon effect, so remove it
				const char* _name = (const char*)pNode->getUserData();
				if(strcmp(effectName, _name) == 0)
				{
					needRemoveVector.push_back(pNode);
				}
			}
		}
		for(unsigned int i = 0; i < needRemoveVector.size(); i++)
		{
			pWeaponAModule->removeChild(needRemoveVector.at(i), true);
		}
	}
}

void WeaponEffect::removeAllWeaponEffects(CCLegendAnimation* roleAnim)
{
	if(roleAnim == NULL)
		return;

	//int imgIdx = PLAYER_ANIM_WEAPON_IMG_IDX;
	int imgIdx = BasePlayer::getWeaponImgIdx(roleAnim);
    if(imgIdx == -1)
        return;

	for(int i = 0; i < MAX_WEAPON_PER_ROLE; i++)
	{
		CCLegendAModule* pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, i);
		if(pWeaponAModule == NULL)
			continue;

		CCArray* pArray = pWeaponAModule->getChildren();
		std::vector<CCNode*> needRemoveVector;
		CCObject* pObj = NULL;
		CCARRAY_FOREACH(pArray, pObj)
		{
			CCNode* pNode = (CCNode*)(pObj);
			if(pNode != NULL && pNode->getUserData() != NULL)
			{
				std::string _name = (const char*)pNode->getUserData();
				// it's weapon effect, so remove it
				int index = _name.find(GAME_WEAPON_EFFECT_KEYWORD);
				if(index >= 0)
				{
					needRemoveVector.push_back(pNode);
				}
			}
		}
		for(unsigned int i = 0; i < needRemoveVector.size(); i++)
		{
			pWeaponAModule->removeChild(needRemoveVector.at(i), true);
		}
	}
}

bool WeaponEffect::hasWeaponEffect(const char* effectFileName, CCLegendAnimation* roleAnim)
{
	//int imgIdx = PLAYER_ANIM_WEAPON_IMG_IDX;
	int imgIdx = BasePlayer::getWeaponImgIdx(roleAnim);
    if(imgIdx == -1)   // no weapon module, no weapon effect
        return false;
    
	CCLegendAModule* pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, PLAYER_ANIM_WEAPON_AMOD_IDX);
	if(pWeaponAModule == NULL)
		return false;

	CCArray* pArray = pWeaponAModule->getChildren();
	CCObject* pObj = NULL;
	CCARRAY_FOREACH(pArray, pObj)
	{
		CCNode* pNode = (CCNode*)(pObj);
		if(pNode != NULL && pNode->getUserData() != NULL)
		{
			// it's weapon effect, so remove it
			const char* _name = (const char*)pNode->getUserData();
			if(strcmp(effectFileName, _name) == 0)
			{
				return true;
			}
		}
	}

	return false;
}

std::string WeaponEffect::getWeaponEffectType(const char* effectFileName)
{
	std::string name = effectFileName;
	int index = name.find(GAME_WEAPON_EFFECT_TYPE_FIRE);
	if(index >= 0)
		return GAME_WEAPON_EFFECT_TYPE_FIRE;

	index = name.find(GAME_WEAPON_EFFECT_TYPE_SPARKLE);
	if(index >= 0)
		return GAME_WEAPON_EFFECT_TYPE_SPARKLE;

	CCAssert(false, "other has not been implemented");
	return "";
}

const char* WeaponEffect::getEffectName(const char* effectName)
{
	if(strcmp(effectName, "") == 0)
		CCAssert(false, "should not be empty");

    std::map<std::string, int>::const_iterator iter ;

    iter = s_effectNameId.find(effectName);
    if ( iter == s_effectNameId.end() )
	{
		int newId = s_effectNameId.size();
		s_effectNameId.insert(make_pair(effectName, newId));
		return getEffectName(effectName);
	}
    else 
	{
		return iter->first.c_str();
	}
}

CCParticleSystem* WeaponEffect::getParticle(const char* plistName)
{
	CCParticleSystem* oneParticle = NULL;
	for(unsigned int i = 0; i < s_effectCache.size(); i++)
	{
		WeaponEffectCache& oneEffect = s_effectCache.at(i);
		if(oneEffect.effectFileName == plistName && oneEffect.effectInstance->retainCount() == 1)
		{
			oneParticle = oneEffect.effectInstance;
			oneParticle->setRotation(0);
			oneParticle->scheduleUpdateWithPriority(1);   // reset
			oneParticle->resetSystem();
			return oneParticle;
		}
	}

	if(oneParticle == NULL)
	{
		WeaponEffectCache oneEffect;
		oneEffect.effectFileName = plistName;
		oneParticle = CCParticleSystemQuad::create(plistName);   // caiji1.plist
		oneParticle->retain();
		oneEffect.effectInstance = oneParticle;
		s_effectCache.push_back(oneEffect);
	}

	return oneParticle;
}

///////////////////////////////////////////////////////////////////////

std::string WeaponEffectFire::s_effectTag = GAME_WEAPON_EFFECT_TYPE_FIRE;

void WeaponEffectFire::attach(WeaponEffectDefine& effectDef, int weaponType, CCLegendAnimation* roleAnim)
{
	switch(weaponType)
	{
	case GAME_WEAPON_TYPE_LANCE:
		attachToLance(effectDef, roleAnim);
		break;
	case GAME_WEAPON_TYPE_FAN:
		attachToFan(effectDef, roleAnim);
		break;
	case GAME_WEAPON_TYPE_KNIFE:
		attachToKnife(effectDef, roleAnim);
		break;
	case GAME_WEAPON_TYPE_BOW:
		attachToBow(effectDef, roleAnim);
		break;
	}
}

void WeaponEffectFire::attachToLance(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim)
{
	if(roleAnim == NULL)
		return;

	int imgIdx = BasePlayer::getWeaponImgIdx(roleAnim);
    if(imgIdx == -1)
        return;

	const char* effectName = effectDef.weaponEffectFileName.c_str();

	// first, remove the old one
	WeaponEffect::removeWeaponEffect(roleAnim, effectName);

	const float effectScale = 0.4f;

	// 武器：枪，分为上下2部分
	// 0 is upper part
	CCLegendAModule* pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, 0);
	if(pWeaponAModule != NULL)
	{
		const int maxAttachPoint = 5;

		// attach the particle effect to the special amodule
		const float attachPoints[maxAttachPoint][2] = {
			 //{32, 55}, {32, 70}, {32, 85}, {32, 100},
			{32, 55}, {32, 70}, {32, 85}, {32, 100},  {32, 115}
		};
		for(int p = 0; p < maxAttachPoint; p++)
		{
			//CCParticleSystem* particleEffect = CCParticleSystemQuad::create(effectName);   // caiji1.plist
			CCParticleSystem* particleEffect = WeaponEffect::getParticle(effectName);
			particleEffect->setPosition(ccp(attachPoints[p][0], attachPoints[p][1]));
			particleEffect->setScale(effectScale);
			//particleEffect->setTotalParticles(2);
			//particleEffect->setStartSize(100.f);
			particleEffect->setPositionType(kCCPositionTypeRelative);
			//particleEffect->setUserData(&WeaponEffectFire::s_effectTag);
			particleEffect->setUserData((void*)WeaponEffect::getEffectName(effectName));
			pWeaponAModule->addChild(particleEffect, p);
		}
	}

	// 1 is the lower part
	pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, 1);
	if(pWeaponAModule != NULL)
	{
		const int maxAttachPoint = 3;

		// attach the particle effect to the special amodule
		const float attachPoints[maxAttachPoint][2] = {
			//{32, 25}, {32, 40},
			{32, 5}, {32, 25}, {32, 40},
		};
		for(int p = 0; p < maxAttachPoint; p++)
		{
			//CCParticleSystem* particleEffect = CCParticleSystemQuad::create(effectName);   // caiji1.plist
			CCParticleSystem* particleEffect = WeaponEffect::getParticle(effectName);
			particleEffect->setPosition(ccp(attachPoints[p][0], attachPoints[p][1]));
			particleEffect->setScale(effectScale);
			//particleEffect->setTotalParticles(2);
			//particleEffect->setStartSize(100.f);
			particleEffect->setPositionType(kCCPositionTypeRelative);
			//particleEffect->setUserData(&WeaponEffectFire::s_effectTag);
			particleEffect->setUserData((void*)WeaponEffect::getEffectName(effectName));
			pWeaponAModule->addChild(particleEffect, p);
		}
	}
}
void WeaponEffectFire::attachToFan(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim)
{
	if(roleAnim == NULL)
		return;

	//int imgIdx = PLAYER_ANIM_WEAPON_IMG_IDX;
	int imgIdx = BasePlayer::getWeaponImgIdx(roleAnim);
    if(imgIdx == -1)
        return;

	CCLegendAModule* pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, 0);
	if(pWeaponAModule == NULL)
		return;

	const char* effectName = effectDef.weaponEffectFileName.c_str();

	// first, remove the old one
	WeaponEffect::removeWeaponEffect(roleAnim, effectName);

	const int maxAttachPoint = 3;

	// attach the particle effect to the special amodule
	const float attachPoints[maxAttachPoint][2] = {
		{16, 30}, {16, 37}, {16, 53},
	};
	for(int p = 0; p < maxAttachPoint; p++)
	{
		//CCParticleSystem* particleEffect = CCParticleSystemQuad::create(effectName);   // caiji1.plist
		CCParticleSystem* particleEffect = WeaponEffect::getParticle(effectName);
		//ccColor4F color = ccc4FFromccc3B(ccc3(255, 0, 255));
		//particleEffect->setStartColor(color);
		particleEffect->setPosition(ccp(attachPoints[p][0], attachPoints[p][1]));
		particleEffect->setScale(0.5f);
		//particleEffect->setTotalParticles(2);
		//particleEffect->setStartSize(160.f);
		particleEffect->setPositionType(kCCPositionTypeRelative);
		//particleEffect->setUserData(&WeaponEffectFire::s_effectTag);
		particleEffect->setUserData((void*)WeaponEffect::getEffectName(effectName));
		pWeaponAModule->addChild(particleEffect, p);
	}
}
void WeaponEffectFire::attachToKnife(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim)
{
	if(roleAnim == NULL)
		return;

	//int imgIdx = PLAYER_ANIM_WEAPON_IMG_IDX;
	int imgIdx = BasePlayer::getWeaponImgIdx(roleAnim);
    if(imgIdx == -1)
        return;
    
	const char* effectName = effectDef.weaponEffectFileName.c_str();

	// first, remove the old one
	WeaponEffect::removeWeaponEffect(roleAnim, effectName);

	const int maxWeaponNum = MAX_WEAPON_PER_ROLE;
	for(int i = 0; i < maxWeaponNum; i++)
	{
		CCLegendAModule* pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, i);
		if(pWeaponAModule == NULL)
			return;

		const int maxAttachPoint = 4;

		// attach the particle effect to the special amodule
		const float attachPoints[maxAttachPoint][2] = {
			{16, 55}, {16, 70}, {16, 85}, {16, 100}, 
		};
		for(int p = 0; p < maxAttachPoint; p++)
		{
			//CCParticleSystem* particleEffect = CCParticleSystemQuad::create(effectName);   // caiji1.plist
			CCParticleSystem* particleEffect = WeaponEffect::getParticle(effectName);
			particleEffect->setPosition(ccp(attachPoints[p][0], attachPoints[p][1]));
			particleEffect->setScale(0.5f);
			//particleEffect->setTotalParticles(2);
			//particleEffect->setStartSize(100.f);
			particleEffect->setPositionType(kCCPositionTypeRelative);
			//particleEffect->setUserData(&WeaponEffectFire::s_effectTag);
			particleEffect->setUserData((void*)WeaponEffect::getEffectName(effectName));
			pWeaponAModule->addChild(particleEffect, p);
		}
	}
}
void WeaponEffectFire::attachToBow(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim)
{
	if(roleAnim == NULL)
		return;

	//int imgIdx = PLAYER_ANIM_WEAPON_IMG_IDX;
	int imgIdx = BasePlayer::getWeaponImgIdx(roleAnim);
    if(imgIdx == -1)
        return;

	CCLegendAModule* pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, 0);
	if(pWeaponAModule == NULL)
		return;

	const int maxAttachPoint = 7;

	const float effectScale = 0.4f;

	const char* effectName = effectDef.weaponEffectFileName.c_str();

	// first, remove the old one
	WeaponEffect::removeWeaponEffect(roleAnim, effectName);

	// attach the particle effect to the special amodule
	const float attachPoints[maxAttachPoint][2] = {
		//{42, 24}, {31, 35}, {21, 50}, {22, 68}, {35, 82}, {41, 96},   // 6
		{42, 24}, {31, 33}, {28, 41}, {11, 54}, {26, 72}, {36, 84}, {41, 96},
	};
	for(int p = 0; p < maxAttachPoint; p++)
	{
		//CCParticleSystem* particleEffect = CCParticleSystemQuad::create(effectName);   // caiji1.plist
		CCParticleSystem* particleEffect = WeaponEffect::getParticle(effectName);
		particleEffect->setPosition(ccp(attachPoints[p][0], attachPoints[p][1]));
		particleEffect->setScale(effectScale);
		//particleEffect->setTotalParticles(2);
		//particleEffect->setStartSize(100.f);
		particleEffect->setPositionType(kCCPositionTypeRelative);
		//particleEffect->setUserData(&WeaponEffectFire::s_effectTag);
		particleEffect->setUserData((void*)WeaponEffect::getEffectName(effectName));
		pWeaponAModule->addChild(particleEffect, p);
	}
}

///////////////////////////////////////////////////////////////////////

std::string WeaponEffectSparkle::s_effectTag = GAME_WEAPON_EFFECT_TYPE_SPARKLE;

void WeaponEffectSparkle::attach(WeaponEffectDefine& effectDef, int weaponType, CCLegendAnimation* roleAnim)
{
	switch(weaponType)
	{
	case GAME_WEAPON_TYPE_LANCE:
		attachToLance(effectDef, roleAnim);
		break;
	case GAME_WEAPON_TYPE_FAN:
		attachToFan(effectDef, roleAnim);
		break;
	case GAME_WEAPON_TYPE_KNIFE:
		attachToKnife(effectDef, roleAnim);
		break;
	case GAME_WEAPON_TYPE_BOW:
		attachToBow(effectDef, roleAnim);
		break;
	}
}

void WeaponEffectSparkle::attachToLance(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim)
{
	if(roleAnim == NULL)
		return;

	int imgIdx = BasePlayer::getWeaponImgIdx(roleAnim);
    if(imgIdx == -1)
        return;

	const char* effectName = effectDef.weaponEffectFileName.c_str();

	// first, remove the old one
	WeaponEffect::removeWeaponEffect(roleAnim, effectName);

	const float effectScale = 1.0f;

	// 武器：枪，分为上下2部分
	// 0 is upper part
	CCLegendAModule* pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, 0);
	if(pWeaponAModule != NULL)
	{
		const int maxAttachPoint = 1;

		// attach the particle effect to the special amodule
		const float attachPoints[maxAttachPoint][2] = {
			 //{32, 55}, {32, 70}, {32, 85}, {32, 100},
			//{32, 55}, {32, 70}, {32, 85}, {32, 100},  {32, 115}
			{32, 80},
		};
		for(int p = 0; p < maxAttachPoint; p++)
		{
			//CCParticleSystem* particleEffect = CCParticleSystemQuad::create(effectName);   // caiji1.plist
			CCParticleSystem* particleEffect = WeaponEffect::getParticle(effectName);
			particleEffect->setPosition(ccp(attachPoints[p][0], attachPoints[p][1]));
			particleEffect->setScale(effectScale);
			particleEffect->setRotation(180);
			//particleEffect->setTotalParticles(2);
			//particleEffect->setStartSize(100.f);
			particleEffect->setPositionType(kCCPositionTypeRelative);
			//particleEffect->setUserData(&WeaponEffectSparkle::s_effectTag);
			particleEffect->setUserData((void*)WeaponEffect::getEffectName(effectName));
			pWeaponAModule->addChild(particleEffect, p);
		}
	}

	// 1 is the lower part
	//CCLegendAModule* pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, 1);
	//if(pWeaponAModule != NULL)
	//{
	//	const int maxAttachPoint = 1;

	//	// remove the old one
	//	for(int p = 0; p < maxAttachPoint; p++)
	//	{
	//		if(pWeaponAModule->getChildByTag(tagBaseId+p) != NULL)
	//			pWeaponAModule->removeChildByTag(tagBaseId+p, true);
	//	}

	//	// attach the particle effect to the special amodule
	//	const float attachPoints[maxAttachPoint][2] = {
	//		//{32, 5}, {32, 25}, {32, 40},
	//		{32, 35},
	//	};
	//	for(int p = 0; p < maxAttachPoint; p++)
	//	{
	//		CCParticleSystem* particleEffect = CCParticleSystemQuad::create(effectName);   // caiji1.plist
	//		//CCParticleSystem* particleEffect = WeaponEffectFire::getParticle(effectName);
	//		particleEffect->setPosition(ccp(attachPoints[p][0], attachPoints[p][1]));
	//		particleEffect->setScale(effectScale);
	//		//particleEffect->setTotalParticles(2);
	//		//particleEffect->setStartSize(100.f);
	//		particleEffect->setPositionType(kCCPositionTypeRelative);
	//		particleEffect->setTag(tagBaseId + p);
	//		particleEffect->setUserData(&WeaponEffectSparkle::s_effectTag);
	//		pWeaponAModule->addChild(particleEffect, p);
	//	}
}
void WeaponEffectSparkle::attachToFan(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim)
{
	if(roleAnim == NULL)
		return;

	//int imgIdx = PLAYER_ANIM_WEAPON_IMG_IDX;
	int imgIdx = BasePlayer::getWeaponImgIdx(roleAnim);
    if(imgIdx == -1)
        return;

	CCLegendAModule* pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, 0);
	if(pWeaponAModule == NULL)
		return;

	const char* effectName = effectDef.weaponEffectFileName.c_str();

	// first, remove the old one
	WeaponEffect::removeWeaponEffect(roleAnim, effectName);

	const int maxAttachPoint = 1;

	// attach the particle effect to the special amodule
	const float attachPoints[maxAttachPoint][2] = {
		//{16, 30}, {16, 37}, {16, 44},
		{16, 50},
	};
	for(int p = 0; p < maxAttachPoint; p++)
	{
		//CCParticleSystem* particleEffect = CCParticleSystemQuad::create(effectName);   // caiji1.plist
		CCParticleSystem* particleEffect = WeaponEffect::getParticle(effectName);
		//ccColor4F color = ccc4FFromccc3B(ccc3(255, 0, 255));
		//particleEffect->setStartColor(color);
		particleEffect->setPosition(ccp(attachPoints[p][0], attachPoints[p][1]));
		particleEffect->setScale(0.8f);
		//particleEffect->setTotalParticles(2);
		//particleEffect->setStartSize(160.f);
		particleEffect->setPositionType(kCCPositionTypeRelative);
		//particleEffect->setUserData(&WeaponEffectSparkle::s_effectTag);
		particleEffect->setUserData((void*)WeaponEffect::getEffectName(effectName));
		pWeaponAModule->addChild(particleEffect, p);
	}
}
void WeaponEffectSparkle::attachToKnife(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim)
{
	if(roleAnim == NULL)
		return;

	//int imgIdx = PLAYER_ANIM_WEAPON_IMG_IDX;
	int imgIdx = BasePlayer::getWeaponImgIdx(roleAnim);
    if(imgIdx == -1)
        return;

	const char* effectName = effectDef.weaponEffectFileName.c_str();

	// first, remove the old one
	WeaponEffect::removeWeaponEffect(roleAnim, effectName);

	const int maxWeaponNum = MAX_WEAPON_PER_ROLE;
	for(int i = 0; i < maxWeaponNum; i++)
	{
		CCLegendAModule* pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, i);
		if(pWeaponAModule == NULL)
			return;

		const int maxAttachPoint = 1;

		// attach the particle effect to the special amodule
		const float attachPoints[maxAttachPoint][2] = {
			//{16, 55}, {16, 70}, {16, 85}, {16, 100}, 
			{16, 95}, 
		};
		for(int p = 0; p < maxAttachPoint; p++)
		{
			//CCParticleSystem* particleEffect = CCParticleSystemQuad::create(effectName);   // caiji1.plist
			CCParticleSystem* particleEffect = WeaponEffect::getParticle(effectName);
			particleEffect->setPosition(ccp(attachPoints[p][0], attachPoints[p][1]));
			particleEffect->setScale(0.8f);
			//particleEffect->setTotalParticles(2);
			//particleEffect->setStartSize(100.f);
			particleEffect->setPositionType(kCCPositionTypeRelative);
			//particleEffect->setUserData(&WeaponEffectSparkle::s_effectTag);
			particleEffect->setUserData((void*)WeaponEffect::getEffectName(effectName));
			pWeaponAModule->addChild(particleEffect, p);
		}
	}
}
void WeaponEffectSparkle::attachToBow(WeaponEffectDefine& effectDef, CCLegendAnimation* roleAnim)
{
	if(roleAnim == NULL)
		return;

	//int imgIdx = PLAYER_ANIM_WEAPON_IMG_IDX;
	int imgIdx = BasePlayer::getWeaponImgIdx(roleAnim);
    if(imgIdx == -1)
        return;

	CCLegendAModule* pWeaponAModule = roleAnim->addSpecialAModule(imgIdx, 0);
	if(pWeaponAModule == NULL)
		return;

	const int maxAttachPoint = 1;

	const float effectScale = 0.9f;

	const char* effectName = effectDef.weaponEffectFileName.c_str();

	// first, remove the old one
	WeaponEffect::removeWeaponEffect(roleAnim, effectName);

	// attach the particle effect to the special amodule
	const float attachPoints[maxAttachPoint][2] = {
		//{42, 24}, {31, 35}, {21, 50}, {22, 68}, {35, 82}, {41, 96},   // 6
		//{42, 24}, {31, 33}, {28, 41}, {11, 54}, {26, 72}, {36, 84}, {41, 96},
		 {20, 64}, 
	};
	for(int p = 0; p < maxAttachPoint; p++)
	{
		//CCParticleSystem* particleEffect = CCParticleSystemQuad::create(effectName);   // caiji1.plist
		CCParticleSystem* particleEffect = WeaponEffect::getParticle(effectName);
		particleEffect->setPosition(ccp(attachPoints[p][0], attachPoints[p][1]));
		particleEffect->setScale(effectScale);
		particleEffect->setRotation(-90);
		//particleEffect->setTotalParticles(2);
		//particleEffect->setStartSize(100.f);
		particleEffect->setPositionType(kCCPositionTypeRelative);
		//particleEffect->setUserData(&WeaponEffectSparkle::s_effectTag);
		particleEffect->setUserData((void*)WeaponEffect::getEffectName(effectName));
		pWeaponAModule->addChild(particleEffect, p);
	}
}
