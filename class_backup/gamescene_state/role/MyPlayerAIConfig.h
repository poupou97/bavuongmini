#ifndef _LEGEND_GAMEACTOR_MYPLAYERAICONFIG_H_
#define _LEGEND_GAMEACTOR_MYPLAYERAICONFIG_H_

#include "cocos2d.h"
USING_NS_CC;

// 内挂设置的存储ID
#define ROBOT_SETUP_MOVE_RANGE "ROBOT_SETUP_MOVE_RANGE"

// ROBOT_SETUP_SKILL_ID_0, ROBOT_SETUP_SKILL_ID_1, etc
#define ROBOT_SETUP_SKILL_ID_PREFIX "ROBOT_SETUP_SKILL_ID_"
#define ROBOT_SETUP_SKILL_ID_0 "ROBOT_SETUP_SKILL_ID_0"
#define ROBOT_SETUP_SKILL_ID_1 "ROBOT_SETUP_SKILL_ID_1"
#define ROBOT_SETUP_SKILL_ID_2 "ROBOT_SETUP_SKILL_ID_2"
#define ROBOT_SETUP_SKILL_ID_3 "ROBOT_SETUP_SKILL_ID_3"
#define ROBOT_SETUP_SKILL_ID_4 "ROBOT_SETUP_SKILL_ID_4"
#define ROBOT_SETUP_SKILL_MAX 5

#define ROBOT_SETUP_PICKANDSELL_MAX 9

#define ROBOT_SAVE_TYPE_STRING 1
#define ROBOT_SAVE_TYPE_INT 2
/**
 * 挂机系统的配置项
 * @author zhaogang
 * @Date 2013-12-24
 */
class MyPlayerAIConfig
{
public:
	MyPlayerAIConfig();
	virtual ~MyPlayerAIConfig();

	/**
	 * 根据角色名，存储和获取相应的挂机设置
	 * @param playerName, 角色名
	 */
	static void saveConfig(std::string saveNameString,int value);
	static void loadConfig(std::string playerName);
	static void saveConfig(std::string saveNameString,std::string value);

public:
	// 是否开启挂机
	static void enableRobot(bool bValue);
	static bool isEnableRobot() ;

	// 挂机时的移动范围
	static void setMoveRange(int range);
	static int getMoveRange();
	static void saveMoveRange(int range);
	
	// 设置挂机技能
	static void setSkillList(std::vector<std::string> allSkills);
	static void setSkillList(int index, std::string skillId);

	static void setAutomaticSkill(int isSelect);
	static int getAutomaticSkill();
	//protect panel
	static void setRoleUseHp(int isSelect);
	static int getRoleUseHp();
	static void setRoleUseMp(int isSelect);
	static int getRoleUseMp();

	static void setRoleBuyHp(int isSelect);
	static int getRoleBuyHp();
	static void setRoleBuyMp(int isSelect);
	static int getRoleBuyMp();

	static void setGeneralUseHp(int isSelect);
	static int getGeneralUseHp();
	static void setGeneralUseMp(int isSelect);
	static int getGeneralUseMp();

	static void setGeneralBuyHp(int isSelect);
	static int getGeneralBuyHp();
	static void setGeneralBuyMp(int isSeclect);
	static int getGeneralBuyMp();
	static void setAutoRepairEquip(int isSelect);
	static int getAutoRepairEquip();

	static void setRoleLimitHp(int isSelect);
	static int getRoleLimitHp();
	static void setRoleLimitMp(int isSelect);
	static int getRoleLimitMp();
	static void setGeneralLimitHp(int isSelect);
	static int getGeneralLimitHp();
	static void setGeneralLimitMp(int isSelect);
	static int getGeneralLimitMp();

	/* 设置角色使用的HP*/
	static void setRoleUseDrugHpName(std::string drugId);
	static std::string getRoleUseDrugHpName();
	/* 设置角色使用的MP*/
	static void setRoleUseDrugMpName(std::string drugId);
	static std::string getRoleUseDrugMpName();
	/* 设置武将使用的HP*/
	static void setGeneralUseDrugHpName(std::string drugId);
	static std::string getGeneralUseDrugHpName();
	/* 设置武将使用的MP*/
	static void setGeneralUseDrugMpName(std::string drugId);
	static std::string getGeneralUseDrugMpName();

	//static std::string getDefaultDrug(std::string drugIndex_);
	/*物品panel*/
	static void setGoodsPickAndSell(int checkBoxIndex ,int isSelect);
	static int getGoodsPickAndSell(int checkBoxIndex);
	//get
	static int getEquipMentQualityByRobotAutoSell(int checkBoxIndexOfEquip);
	static int getGoodsClazzByRobotAutoSell(int checkBoxIndexOfGoods);
	static int getDrugsObjectByRobotAutoSell(int checkBoxIndexOfDrugs);

	//when level is up, robot is change the drug hp mp 
	static void reSetRobotUseDrug(int roleLevel);
	
	//when game is exit,callback this method,save robot data
	static void pushConfig(std::string key, int type,std::string value1, int value2);

	static void savePushConfig();

	static void setRobotSaveState(bool value_);
	static bool getRobotSaveState();
	static bool m_isSaveState;


	struct saveConfigInfo{
		std::string key;
		int type;
		std::string strValue;
		int intValue;
	};

	static std::vector<saveConfigInfo *> configInfoVector; 
public:
	//设置自动接收组队 自动拒绝组队
	static void setAcceptTeam(bool isSelect);
	static bool getAcceptTeam();

	static void setRefuseTeam(bool isSelect);
	static bool getRefuseTeam();	
public:
	/** 技能列表 */
	static std::string sRobotSkillsId[ROBOT_SETUP_SKILL_MAX];

private:
	/** 是否开启挂机 */
	static bool sEnableRobot;
	static int sAutomaticSkill;
	/** 移动范围 */
	static int sMoveRange;

	/** 主角自动使用HP */
	static int sRoleAutoUseHp;
	/** 主角自动使用HP */
	static int sRoleAutoUseMp;
	/** 主角自动购买HP */
	static int sRoleAutoBuyHp;
	/** 主角自动购买HP */
	static int sRoleAutoBuyMp;

	/** 主角自动使用的HP药品 */
	static std::string sRoleUseDrugHpId;
	/** 主角自动使用的MP药品 */
	static std::string sRoleUseDrugMpId;
	/** 武将自动使用的HP药品 */
	static std::string sGeneralUseDrugHpId;
	/** 武将自动使用的MP药品 */
	static std::string sGeneralUseDrugMpId;

	/** 武将自动使用HP */
	static int sGeneralAutoUseHp;
	/** 武将自动使用MP */
	static int sGeneralAutoUseMp;
	/** 武将自动购买HP */
	static int sGeneralAutoBuyHp;
	/** 武将自动购买MP */
	static int sGeneralAutoBuyMp;

	/**自动修理装备 */
	static int sRepairEquipment;

	static int sRolelimitHp;
	/** 主角M的使用限制 */
	static int sRolelimitMp;
	/** 武将HP的使用限制 */
	static int sGenerallimitHp;
	/** 武将MP的使用限制 */
	static int sGenerallimitMp;
	/**物品的拾取和售出 */
	static std::string sgoodsCheckName;
	static int sGoodsPickAndSell[ROBOT_SETUP_PICKANDSELL_MAX];
	/*挂机默认设置药品*/
	//static std::string sdrugIndex;
	//static std::string sdrugId;
	/*是否接收自动组队*/
	static bool sAcceptTeam;
	static bool sRefuseTeam;
};

#endif