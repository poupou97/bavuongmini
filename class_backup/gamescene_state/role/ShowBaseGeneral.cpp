#include "ShowBaseGeneral.h"

#include "../GameSceneState.h"
#include "GameView.h"
#include "../role/MyPlayer.h"
#include "../../utils/StaticDataManager.h"
#include "../role/General.h"
#include "../../messageclient/element/CGeneralBaseMsg.h"
#include "../../messageclient/element/CShowGeneralInfo.h"
#include "../../messageclient/protobuf/ModelMessage.pb.h"
#include "../../messageclient/protobuf/PlayerMessage.pb.h"
#include "../../messageclient/element/CMapInfo.h"
#include "OtherPlayer.h"

ShowBaseGeneral::ShowBaseGeneral()
	:m_longGeneralId(0)
	,m_nTemplateId(0)
	,m_nActionType(-1)
	,m_nCurrentQuality(-1)
	,m_nEvolution(-1)
{

}

ShowBaseGeneral::~ShowBaseGeneral()
{

}

void ShowBaseGeneral::set_actionType( int nActionType )
{
	this->m_nActionType = nActionType;
}

int ShowBaseGeneral::get_actionType()
{
	return this->m_nActionType;
}

void ShowBaseGeneral::set_generalId( long long longGeneralId )
{
	this->m_longGeneralId = longGeneralId;
}

long long ShowBaseGeneral::get_generalId()
{
	return this->m_longGeneralId;
}

void ShowBaseGeneral::set_templateId( int nTemplateId )
{
	this->m_nTemplateId = nTemplateId;
}

int ShowBaseGeneral::get_templateId()
{
	return this->m_nActionType;
}

void ShowBaseGeneral::set_currentQuality( int nCurrentQuality )
{
	this->m_nCurrentQuality = nCurrentQuality;
}

int ShowBaseGeneral::get_currentQuality()
{
	return this->m_nCurrentQuality;
}

void ShowBaseGeneral::set_evolution( int nEvolution )
{
	this->m_nEvolution = nEvolution;
}

int ShowBaseGeneral::get_evolution()
{
	return this->m_nEvolution;
}

void ShowBaseGeneral::generalHide()
{
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if (NULL != scene)
	{
		CCNode* actorLayer = scene->getActorLayer();
		if (NULL != actorLayer)
		{
			General * pActor = (General*)actorLayer->getChildByTag(m_longGeneralId * -1);
			if (NULL != pActor)
			{
				pActor->setDestroy(true);
			}
		}
	}
}

bool ShowBaseGeneral::hasOnShowState(long long generalId)
{
	bool bFlag = false;

	if (m_longGeneralId == generalId)
	{
		if (-1 == m_nActionType)
		{
			bFlag = false;
		}
		else if (SHOW_ACTIONTYPE == m_nActionType)
		{
			bFlag = true;
		}
		else if (HIDE_ACTIONTYPE == m_nActionType)
		{
			bFlag = false;
		}
	}

	return bFlag;
}

int ShowBaseGeneral::getShowState( long long generalId )
{
	int nState = 0;

	if (m_longGeneralId == generalId)
	{
		if (-1 == m_nActionType)
		{
			nState = 1;
		}
		else if (SHOW_ACTIONTYPE == m_nActionType)
		{
			nState = 0;
		}
		else if (HIDE_ACTIONTYPE == m_nActionType)
		{
			nState = 1;
		}
	}
	else
	{
		nState = 1;
	}

	return nState;
}

void ShowBaseGeneral::clearAllData()
{
	this->set_generalId(0);
	this->set_templateId(0);
	this->set_actionType(-1);
	this->set_currentQuality(-1);
	this->set_evolution(-1);
}

///////////////////////////////////////////////////////////////////////////////////////////

ShowMyGeneral * ShowMyGeneral::s_showMyGeneral = NULL;

ShowMyGeneral::ShowMyGeneral()
{

}

ShowMyGeneral::~ShowMyGeneral()
{

}

ShowMyGeneral * ShowMyGeneral::getInstance()
{
	if (NULL == s_showMyGeneral)
	{
		s_showMyGeneral = new ShowMyGeneral();
	}

	return s_showMyGeneral;
}

void ShowMyGeneral::generalShow( CShowGeneralInfo* pShowGeneralInfo )
{
	long long longGeneralId = pShowGeneralInfo->generalid();
	long long showGeneralId = longGeneralId * -1;

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(NULL == scene)
	{
		return;
	}

	// 如果展示的武将存在，终止
	CCNode* actorLayer = scene->getActorLayer();
	if (NULL != actorLayer)
	{
		CCNode* pGeneral = actorLayer->getChildByTag(showGeneralId);
		if (NULL != pGeneral)
		{
			return ;
		}
	}

	// 展示武将（收回武将）
	MyPlayer* pMyPlayer = GameView::getInstance()->myplayer;
	float xPosReal = pMyPlayer->getRealWorldPosition().x;
	float yPosReal = pMyPlayer->getRealWorldPosition().y;
	float xPos = pMyPlayer->getWorldPosition().x - 64;
	float yPos = pMyPlayer->getWorldPosition().y;

	CGeneralBaseMsg * generalBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[pShowGeneralInfo->templateid()];

	General* pActor = new General();
	pActor->setRoleId(showGeneralId);
	scene->putActor(showGeneralId, pActor);

	// 设置tag值（负的武将id，销毁时根据tag值销毁）
	pActor->setTag(showGeneralId);

	pActor->setOwnerId(pMyPlayer->getRoleId());

	// init position
	pActor->setWorldPosition(ccp(xPos, yPos));
	pActor->setRealWorldPosition(ccp(xPos, yPos));
	// 初始化时，将其cocos postion也直接进行初始化，可以解决切换武将时的龙魂链条异常问题
	pActor->setPositionImmediately(scene->convertToCocos2DSpace(pActor->getWorldPosition()));

	// 设置武将方向
	pActor->setAnimDir(GameView::getInstance()->myplayer->getAnimDir());   // 主角自己的武将，跟主角一个方向

	pActor->init(generalBaseMsgFromDb->get_avatar().c_str());
	pActor->setActorName(generalBaseMsgFromDb->name().c_str());

	//刷新场景中的武将头顶名字颜色
	pActor->modifyActorNameColor(GameView::getInstance()->getGeneralsColorByQuality(pShowGeneralInfo->currentquality()));

	pActor->showActorName(true);
	//pActor->showFlag(true);	// 此代码是 是否展示武将脚下的光圈
	pActor->setGameScene(scene);

	// 展示武将的进化等级
	pActor->addRank(pShowGeneralInfo->evolution());

	pActor->addRareIcon(generalBaseMsgFromDb->rare());

	// add new actor into cocos2d-x render scene
	actorLayer->addChild(pActor);
	pActor->release();

	//pActor->onBorn();

	// 当onBorn的action结束后再 令其跟随主角
	CCActionInterval * action = CCSequence::create(
		CCShow::create(),
		CCDelayTime::create(1.25f),
		CCCallFuncND::create(this, callfuncND_selector(ShowMyGeneral::callBackOnBorn), (void *)pActor),
		NULL);

	pActor->runAction(action);

	// 跟随主角
	//pActor->followMaster(pMyPlayer->getRoleId(), 2);
}

void ShowMyGeneral::generalShow()
{
	GameView* gameView = GameView::getInstance();
	if (NULL == gameView)
	{
		return;
	}

	if (NULL == gameView->myplayer)
	{
		return;
	}

	// 首先判断 是否是显示
	if (SHOW_ACTIONTYPE == m_nActionType)
	{
		if ((0 != m_longGeneralId) && (0 != m_nTemplateId) && (-1 != m_nCurrentQuality) && (-1 != m_nEvolution))
		{
			CShowGeneralInfo* pShowGeneralInfo = new CShowGeneralInfo();
			pShowGeneralInfo->set_generalid(m_longGeneralId);
			pShowGeneralInfo->set_templateid(m_nTemplateId);
			pShowGeneralInfo->set_currentquality(m_nCurrentQuality);
			pShowGeneralInfo->set_evolution(m_nEvolution);

			this->generalShow(pShowGeneralInfo);

			delete pShowGeneralInfo;
		}
	}
	else if(-1 == m_nActionType)
	{
		initGeneralData();
	}
}

bool ShowMyGeneral::canGeneralShow()
{
	bool bCanGeneralShow = true;

	// 竞技场不显示 “展示”武将
	if (GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::coliseum)
	{
		bCanGeneralShow = false;

		return bCanGeneralShow;
	}

	MyPlayer* pMyPlayer = GameView::getInstance()->myplayer;
	if (NULL == pMyPlayer)
	{
		bCanGeneralShow = false;

		return bCanGeneralShow;
	}

	CCNode* pNode_blood = pMyPlayer->getChildByTag(GameActor::kTagGreenBloodBar);
	if (NULL == pNode_blood)
	{
		for(unsigned int i = 0 ; i < GameView::getInstance()->generalsInLineList.size();++i)
		{
			CGeneralBaseMsg * temp = GameView::getInstance()->generalsInLineList.at(i);
			if (2 == temp->fightstatus())
			{
				bCanGeneralShow = false;

				break;
			}
		}
	}
	else
	{
		bCanGeneralShow = false;
	}

	return bCanGeneralShow;
}

void ShowMyGeneral::callBackOnBorn( CCObject * pSender, void * pActor )
{
	General * pGeneral = (General*)pActor;
	if (NULL != pGeneral)
	{
		MyPlayer* pMyPlayer = GameView::getInstance()->myplayer;
		if (NULL != pMyPlayer)
		{
			// 跟随主角
			pGeneral->followMaster(pMyPlayer->getRoleId(), 2);
		}
	}
}

void ShowMyGeneral::initGeneralData()
{
	GameView* gameView = GameView::getInstance();
	if (NULL == gameView)
	{
		return;
	}

	if (NULL == gameView->myplayer)
	{
		return;
	}

	GamePlayer* gamePlayer = gameView->myplayer->player;
	if (NULL == gamePlayer)
	{
		return;
	}

	ActiveRole activeRole = gamePlayer->activerole();
	PlayerBaseInfo playerBaseInfo = activeRole.playerbaseinfo();
	ShowGeneralInfo tmp = playerBaseInfo.showgenral();

	if (0 != tmp.generalid() && 0 != tmp.templateid())
	{
		this->set_actionType(SHOW_ACTIONTYPE);

		// 将 展示 武将数据保存
		this->set_generalId(tmp.generalid());
		this->set_templateId(tmp.templateid());
		this->set_currentQuality(tmp.currentquality());
		this->set_evolution(tmp.evolution());

		CShowGeneralInfo* pShowGeneralInfo = new CShowGeneralInfo();
		pShowGeneralInfo->set_generalid(tmp.generalid());
		pShowGeneralInfo->set_templateid(tmp.templateid());
		pShowGeneralInfo->set_currentquality(tmp.currentquality());
		pShowGeneralInfo->set_evolution(tmp.evolution());

		this->generalShow(pShowGeneralInfo);

		delete pShowGeneralInfo;
	}
	else
	{
		this->set_actionType(HIDE_ACTIONTYPE);
	}
}

////////////////////////////////////////////////////////////////////////////////////

ShowOtherGeneral::ShowOtherGeneral()
	:m_pOtherPlayer(NULL)
{

}

ShowOtherGeneral::~ShowOtherGeneral()
{

}

void ShowOtherGeneral::generalShow( CShowGeneralInfo* pShowGeneralInfo )
{
	if (NULL == m_pOtherPlayer)
	{
		CCLOG("show otherGeneral error");

		return;
	}

	long long longGeneralId = pShowGeneralInfo->generalid();
	long long showGeneralId = longGeneralId * -1;

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(NULL == scene)
	{
		return;
	}

	// 如果展示的武将存在，终止
	CCNode* actorLayer = scene->getActorLayer();
	if (NULL != actorLayer)
	{
		CCNode* pGeneral = actorLayer->getChildByTag(showGeneralId);
		if (NULL != pGeneral)
		{
			return ;
		}
	}

	// 展示武将（收回武将）
	float xPosReal = m_pOtherPlayer->getRealWorldPosition().x;
	float yPosReal = m_pOtherPlayer->getRealWorldPosition().y;
	float xPos = m_pOtherPlayer->getWorldPosition().x - 64;
	float yPos = m_pOtherPlayer->getWorldPosition().y;

	CGeneralBaseMsg * generalBaseMsgFromDb = GeneralsConfigData::s_generalsBaseMsgData[pShowGeneralInfo->templateid()];

	General* pActor = new General();
	pActor->setRoleId(showGeneralId);
	scene->putActor(showGeneralId, pActor);

	// 设置tag值（负的武将id，销毁时根据tag值销毁）
	pActor->setTag(showGeneralId);

	pActor->setOwnerId(m_pOtherPlayer->getRoleId());

	// init position
	pActor->setWorldPosition(ccp(xPos, yPos));
	pActor->setRealWorldPosition(ccp(xPos, yPos));
	// 初始化时，将其cocos postion也直接进行初始化，可以解决切换武将时的龙魂链条异常问题
	pActor->setPositionImmediately(scene->convertToCocos2DSpace(pActor->getWorldPosition()));

	// 设置武将方向
	pActor->setAnimDir(m_pOtherPlayer->getAnimDir());   // 主角自己的武将，跟主角一个方向

	pActor->init(generalBaseMsgFromDb->get_avatar().c_str());
	pActor->setActorName(generalBaseMsgFromDb->name().c_str());

	//刷新场景中的武将头顶名字颜色
	pActor->modifyActorNameColor(GameView::getInstance()->getGeneralsColorByQuality(pShowGeneralInfo->currentquality()));

	pActor->showActorName(true);
	//pActor->showFlag(true);	// 此代码是 是否展示武将脚下的光圈
	pActor->setGameScene(scene);

	// 展示武将的进化等级
	pActor->addRank(pShowGeneralInfo->evolution());

	pActor->addRareIcon(generalBaseMsgFromDb->rare());

	// add new actor into cocos2d-x render scene
	actorLayer->addChild(pActor);
	pActor->release();

	//pActor->onBorn();

	// 当onBorn的action结束后再 令其跟随主角
	CCActionInterval * action = CCSequence::create(
		CCShow::create(),
		CCDelayTime::create(1.25f),
		CCCallFuncND::create(this, callfuncND_selector(ShowOtherGeneral::callBackOnBorn), (void *)pActor),
		NULL);

	pActor->runAction(action);

	// 跟随主角
	//pActor->followMaster(pMyPlayer->getRoleId(), 2);
}

void ShowOtherGeneral::generalShow()
{
	if (NULL == m_pOtherPlayer)
	{
		CCLOG("show otherGeneral error");

		return;
	}

	// 首先判断 是否是显示
	if (SHOW_ACTIONTYPE == m_nActionType)
	{
		if ((0 != m_longGeneralId) && (0 != m_nTemplateId) && (-1 != m_nCurrentQuality) && (-1 != m_nEvolution))
		{
			CShowGeneralInfo* pShowGeneralInfo = new CShowGeneralInfo();
			pShowGeneralInfo->set_generalid(m_longGeneralId);
			pShowGeneralInfo->set_templateid(m_nTemplateId);
			pShowGeneralInfo->set_currentquality(m_nCurrentQuality);
			pShowGeneralInfo->set_evolution(m_nEvolution);

			this->generalShow(pShowGeneralInfo);

			delete pShowGeneralInfo;
		}
	}
	else if(-1 == m_nActionType)
	{
		PlayerBaseInfo playerBaseInfo = m_pOtherPlayer->getActiveRole()->playerbaseinfo();
		ShowGeneralInfo tmp = playerBaseInfo.showgenral();

		if (0 != tmp.generalid() && 0 != tmp.templateid())
		{
			//m_nActionType = SHOW_ACTIONTYPE;

			// 将 展示 武将数据保存
			this->set_generalId(tmp.generalid());
			this->set_templateId(tmp.templateid());
			this->set_currentQuality(tmp.currentquality());
			this->set_evolution(tmp.evolution());
			this->set_actionType(m_nActionType);

			CShowGeneralInfo* pShowGeneralInfo = new CShowGeneralInfo();
			pShowGeneralInfo->set_generalid(tmp.generalid());
			pShowGeneralInfo->set_templateid(tmp.templateid());
			pShowGeneralInfo->set_currentquality(tmp.currentquality());
			pShowGeneralInfo->set_evolution(tmp.evolution());

			this->generalShow(pShowGeneralInfo);

			delete pShowGeneralInfo;
		}
	}
}

bool ShowOtherGeneral::canGeneralShow()
{
	bool bCanGeneralShow = true;

	if (NULL == m_pOtherPlayer)
	{
		bCanGeneralShow = false;

		return bCanGeneralShow;
	}

	// 竞技场不显示其他玩家的“展示”武将
	if (GameView::getInstance()->getMapInfo()->maptype() == com::future::threekingdoms::server::transport::protocol::coliseum)
	{
		bCanGeneralShow = false;

		return bCanGeneralShow;
	}

	CCNode* pNode_blood = m_pOtherPlayer->getChildByTag(GameActor::kTagBloodBar);
	if (NULL == pNode_blood)
	{
		GameSceneLayer* scene = GameView::getInstance()->getGameScene();

		if (NULL != scene)
		{
			for (std::map<long long,GameActor*>::iterator it = scene->getActorsMap().begin(); it != scene->getActorsMap().end(); ++it)
			{
				General* pGeneral = dynamic_cast<General*>(it->second);
				if(NULL != pGeneral)
				{
					if (m_pOtherPlayer->getRoleId() == pGeneral->getOwnerId())
					{
						bCanGeneralShow = false;

						return bCanGeneralShow;
					}
				}
			}
		}

	}
	else
	{
		bCanGeneralShow = false;
	}

	return bCanGeneralShow;
}

void ShowOtherGeneral::callBackOnBorn( CCObject * pSender, void * pActor )
{
	General * pGeneral = (General*)pActor;
	if (NULL != pGeneral)
	{
		if (NULL != m_pOtherPlayer)
		{
			// 跟随主角
			pGeneral->followMaster(m_pOtherPlayer->getRoleId(), 2);
		}
	}
}

void ShowOtherGeneral::set_otherPlayer( OtherPlayer* pOtherPlayer )
{
	this->m_pOtherPlayer = pOtherPlayer;
}
