#ifndef _BASEFIGHTER_OWNED_STATES_H
#define _BASEFIGHTER_OWNED_STATES_H

#include "cocos2d.h"

USING_NS_CC;

class BaseFighter;

class ActionContext
{
public:
	ActionContext();
	virtual ~ActionContext();

	virtual void setContext(BaseFighter* self) = 0;
    
    void setData(ActionContext* dest, float startTime, bool finished);

	float startTime;
	bool finished;
};

/////////////////////////////////////////////////////

class ActionChargeContext : public ActionContext
{
public:
	ActionChargeContext();
	virtual ~ActionChargeContext();

public:
	float chargeMoveSpeed;
	float chargeTime;
	CCPoint chargetTargetPosition;

	virtual void setContext(BaseFighter* self);
};

/////////////////////////////////////////////////////

class ActionAttackContext : public ActionContext
{
public:
	int currentActionIndex;   // the animation index which is used for this attack

	virtual void setContext(BaseFighter* self);
};

/////////////////////////////////////////////////////

class ActionDeathContext : public ActionContext
{
public:
	enum ActionDeathState {
		death_state_wait = 0,
		death_state_update = 1,
	};

public:
	ActionDeathContext();
	virtual ~ActionDeathContext();

public:
	int deathState;   // enum ActionDeathState
	int deathType;   // type 0. disappear directly; type 1. knock back; 2. fly up; etc.
	long long attackerRoleId;
	bool bAddDustEffect;
	float toGroundTime;   // 被打倒在地的耗时
	float totalTime;   // the duration for the death action 

	virtual void setContext(BaseFighter* self);
};

/////////////////////////////////////////////////////

class ActionStandContext : public ActionContext
{
public:
	virtual void setContext(BaseFighter* self);

	float idleStartTime;
};

/////////////////////////////////////////////////////

class ActionBeKnockedBackContext : public ActionContext
{
public:
	ActionBeKnockedBackContext();
	virtual ~ActionBeKnockedBackContext();

public:
	float beKnockedBackSpeed;
	float beKnockedBackDuration;
	CCPoint targetPosition;
	bool bReturnBack;   // the actor has been knocked back, return or not

	virtual void setContext(BaseFighter* self);
};

/////////////////////////////////////////////////////

class ActionJumpContext : public ActionContext
{
public:
	ActionJumpContext();
	virtual ~ActionJumpContext();

public:
	float jumpDuration;
	CCPoint targetPosition;

	// jump direction, it will decide the animation's direction
	// 0, back
	// 1, ahead
	int jumpDirection;

	float jumpHeight;

	ccColor3B shadowColor;
	int shadowAlphaStep;

	virtual void setContext(BaseFighter* self);
};

/////////////////////////////////////////////////////

class ActionDisappearContext : public ActionContext
{
public:
	ActionDisappearContext();
	virtual ~ActionDisappearContext();

public:

	virtual void setContext(BaseFighter* self);
};

/////////////////////////////////////////////////////////////////////
// BaseFighter的一些公共ACT状态类
// 更像是一些工具函数
/////////////////////////////////////////////////////////////////////

class BaseFighterJump
{
public:
	static void Enter(BaseFighter* self);
	static void Execute(BaseFighter* self);
	static void Exit(BaseFighter* self);

	static void setJumpAnimId(int animId);

private:
	static int jumpAnimId;
};

class BaseFighterDisappear
{
public:
	static void Enter(BaseFighter* self);
	static void Execute(BaseFighter* self);
	static void Exit(BaseFighter* self);
};

#endif