#include "SimpleFighter.h"

#include "SimpleFighterOwnedStates.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "GameActorAnimation.h"
#include "../GameSceneState.h"
#include "../skill/GameFightSkill.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "BaseFighterOwnedCommand.h"
#include "../../utils/GameUtils.h"
#include "BaseFighterOwnedStates.h"
#include "../GameScene.h"
#include "BaseFighterConstant.h"
#include "AppMacros.h"

static std::string SimpleFighterAnimActionNameList[] =
{
    "stand",
    "run",
    "attack",
    "stand",   // die animation is the same as stand animation
}; 

SimpleFighter::SimpleFighter()
{
	setType(GameActor::type_other);

	// the statemachine init must be put at the end
	// because some animations' instance will be used in the state::Enter() function
	m_pStateMachine = new StateMachine<SimpleFighter>(this);
    m_pStateMachine->SetGlobalState(SimpleFighterGlobalState::Instance());

	this->setAnimDir(ANIM_DIR_DOWN);
}

SimpleFighter::SimpleFighter(GameSceneLayer* scene)
{
	setType(GameActor::type_other);

	// the statemachine init must be put at the end
	// because some animations' instance will be used in the state::Enter() function
	m_pStateMachine = new StateMachine<SimpleFighter>(this);
    m_pStateMachine->SetGlobalState(SimpleFighterGlobalState::Instance());

	this->setGameScene(scene);

	this->setAnimDir(ANIM_DIR_DOWN);
}

SimpleFighter::~SimpleFighter()
{
	delete m_pStateMachine;
}

CCPoint SimpleFighter::getSortPoint()
{
	return ccp(m_worldPosition.x, m_worldPosition.y);
}

bool SimpleFighter::init(const char* resPath, const char* actorName)
{
	// foot container
	m_pFootEffectContainer = CCNodeRGBA::create();
	m_pFootEffectContainer->setCascadeOpacityEnabled(true);
	this->addChild(m_pFootEffectContainer, GameActor::ACTOR_FOOTEFFECT_ZORDER);
	m_pFootEffectContainer->setTag(BaseFighter::kTagFootEffectContainer);

	addShadow();

	// main animation
	m_pAnim = GameActorAnimation::create(resPath, SimpleFighterAnimActionNameList, actorName, ANI_COMPONENT_MAX, SIMPLEFIGHTER_ANI_MAX);
	m_pAnim->showComponent(ANI_COMPONENT_REFLECTION, false);
	m_pAnim->setAnimName(SIMPLEFIGHTER_ANI_STAND);
	this->addChild(m_pAnim, GameActor::ACTOR_BODY_ZORDER);

	// head container
	m_pHeadEffectContainer = CCNodeRGBA::create();
	m_pHeadEffectContainer->setCascadeOpacityEnabled(true);
	this->addChild(m_pHeadEffectContainer, GameActor::ACTOR_HEAD_ZORDER);
	m_pHeadEffectContainer->setTag(BaseFighter::kTagHeadEffectContainer);

	this->setContentSize(CCSizeMake(64, 100));

	this->setScale(BASEFIGHTER_BASE_SCALE);

	this->GetFSM()->ChangeState(SimpleFighterStand::Instance());

	return true;
}

void SimpleFighter::showActorName(bool bShow)
{
	std::string namestr = getActorName();
	const char* name = namestr.c_str();

	CCLabelTTF* nameLabel = (CCLabelTTF*)this->getChildByTag(GameActor::kTagActorName);
	if(bShow)
	{
		if(nameLabel == NULL)
		{
			nameLabel = CCLabelTTF::create(name, APP_FONT_NAME, GAMEACTOR_NAME_FONT_SIZE);
			nameLabel->setColor(ccc3(3, 247, 245));   // blue
			this->addChild(nameLabel, GameActor::ACTOR_NAME_ZORDER);
		}
		else
		{
			nameLabel->setString(name);
		}
		nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT));
	}
	else
	{
		if(nameLabel != NULL)
			nameLabel->removeFromParent();
	}
}

void SimpleFighter::changeAction(int action)
{
	switch(action)
	{
	case ACT_STAND:
		GetFSM()->ChangeState(SimpleFighterStand::Instance());
		break;

	case ACT_RUN:
		GetFSM()->ChangeState(SimpleFighterRun::Instance(), false);
		break;

	case ACT_ATTACK:
		GetFSM()->ChangeState(SimpleFighterAttack::Instance());
		break;

	case ACT_DIE:
		GetFSM()->ChangeState(SimpleFighterDeath::Instance());
		stopAllCommand();
		break;
	}
}

bool SimpleFighter::isAction(int action)
{
	switch(action)
	{
	case ACT_STAND:
		if(GetFSM()->isInState(*(SimpleFighterStand::Instance())))
			return true;
		return false;
		break;

	case ACT_RUN:
		if(GetFSM()->isInState(*(SimpleFighterRun::Instance())))
			return true;
		return false;
		break;

	case ACT_ATTACK:
		if(GetFSM()->isInState(*(SimpleFighterAttack::Instance())))
			return true;
		return false;
		break;

	case ACT_DIE:
		if(GetFSM()->isInState(*(SimpleFighterDeath::Instance())))
			return true;
		return false;
		break;
	}

	//CCAssert(false, "action has not been handled");
	return false;
}

void SimpleFighter::onEnter()
{
	CCNode::onEnter();
}
void SimpleFighter::onExit()
{
	CCNode::onExit();
}

void SimpleFighter::update(float dt)
{
	drive();

	this->running(dt);

	this->GetFSM()->Update();
}
