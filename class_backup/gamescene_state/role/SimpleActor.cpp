#include "SimpleActor.h"

#include "../../legend_engine/CCLegendAnimation.h"
#include "../../legend_engine/LegendAnimationCache.h"
#include "../GameSceneState.h"

#define ELEMENT_TYPE_NONE 0 
#define ELEMENT_TYPE_LEGEND_ANM 1 
#define ELEMENT_TYPE_SPRITE 2

#define SIMPLEACTOR_ANIM_ROOT_TAG 200
#define SIMPLEACTOR_ANIM_TAG 203

SimpleActor::SimpleActor()
{
	this->scheduleUpdate();
	this->setCascadeColorEnabled(true);

	this->m_type = ELEMENT_TYPE_NONE;
}

SimpleActor::~SimpleActor()
{
}

void SimpleActor::loadAnim(const char* animName, bool bLoop, int actionId)
{
	std::string file = animName;
	if(CCFileUtils::sharedFileUtils()->isFileExist(file))
	{
		CCNodeRGBA* pRoot = CCNodeRGBA::create();
		pRoot->setCascadeColorEnabled(true);
		pRoot->setCascadeOpacityEnabled(true);
		pRoot->setTag(SIMPLEACTOR_ANIM_ROOT_TAG);
		this->addChild(pRoot);

		//m_pAnim = new CCLegendAnimation();
		//m_pAnim->autorelease();
		//m_pAnim->initWithAnmFilename(file.c_str());
		CCLegendAnimation* pAnim = CCLegendAnimationCache::sharedCache()->addCCLegendAnimation(file.c_str());
		pAnim->setVisible(true);
		pAnim->setPlayLoop(bLoop);
		pAnim->setAction(actionId);
		pAnim->play();

		// child animation will be affected by the Z value
		pAnim->setPosition(ccp(0, this->getWorldPositionZ()));
		pRoot->addChild(pAnim, 0, SIMPLEACTOR_ANIM_TAG);

		setPosition(getGameScene()->convertToCocos2DSpace(getWorldPosition()));
	}
    else
    {
        CCLOG("file not found: %s", animName);
    }
	m_type = ELEMENT_TYPE_LEGEND_ANM;
}

CCLegendAnimation* SimpleActor::getLegendAnim()
{
	CCNode* pNode = this->getChildByTag(SIMPLEACTOR_ANIM_ROOT_TAG);
    if(pNode == NULL)
        return NULL;
	return (CCLegendAnimation*)pNode->getChildByTag(SIMPLEACTOR_ANIM_TAG);
}

void SimpleActor::loadSprite(const char* spriteName)
{
	CCSprite* sprite = CCSprite::create(spriteName);
	this->addChild(sprite);

	m_type = ELEMENT_TYPE_SPRITE;
}

void SimpleActor::update(float dt)
{
	if(m_type == ELEMENT_TYPE_LEGEND_ANM)
	{
		CCLegendAnimation* pAnim = getLegendAnim();
		if(pAnim == NULL || !pAnim->isPlaying())
		{
			this->removeFromParentAndCleanup(true);
		}
	}
}

CCPoint SimpleActor::getSortPoint()
{
	// ���������
	return ccp(m_worldPosition.x, m_worldPosition.y);
}
