#include "PickingActor.h"

#include "../../legend_engine/CCLegendAnimation.h"
#include "../GameSceneState.h"
#include "../../messageclient/element/CPushCollectInfo.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/CPickingInfo.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/element/CCollectInfo.h"
#include "../../utils/StrUtils.h"

#define kTagParticleEffect 11
#define kTagActivityAnm 12
#define kTagInvaildAnm 13

PickingActor::PickingActor():
isActivied(true),
m_nTemplateId(0)
{
	//this->scheduleUpdate();
	p_collectInfo = new CPushCollectInfo();
	setType(GameActor::type_picking);
}

PickingActor::~PickingActor()
{
	//CC_SAFE_RELEASE(m_pAnim);
	CC_SAFE_DELETE(p_collectInfo);
}

bool PickingActor::init(const char* actorName)
{
	// shadow
    CCSprite* shadow = CCSprite::create("res_ui/shadow.png");
	shadow->setAnchorPoint(ccp(0.5f, 0.5f));
	shadow->setScale(0.8f);
	shadow->setOpacity(128);
    addChild(shadow, GameActor::ACTOR_SHADOW_ZORDER);

	// load animation
	std::string animFileName = "animation/collection/";
	animFileName.append(actorName);
	animFileName.append(".anm");
	m_pAnim = CCLegendAnimation::create(animFileName);
	m_pAnim->setReleaseWhenStop(false);
	m_pAnim->setPlayLoop(true);
	m_pAnim->setAction(m_animActionID);
	addChild(m_pAnim, ACTOR_BODY_ZORDER);

	std::string activePath = "";
	std::string invaildPath = "";
	std::map<int,CCollectInfo*>::const_iterator cIter;
	cIter = CollectInfoConfig::s_collectInfo.find(m_nTemplateId);
	if (cIter == CollectInfoConfig::s_collectInfo.end()) // 没找到就是指向END了  
	{
		
	}
	else
	{
		CCollectInfo * collectInfo = CollectInfoConfig::s_collectInfo[m_nTemplateId];

		//动画类型 -1: null  0：anm  1: plist
		int activityAnm_type = -1;
		std::vector<std::string> str_suffix_activity = StrUtils::split(collectInfo->get_activity_anm().c_str(),".");
		if (str_suffix_activity.at(str_suffix_activity.size()-1) == "anm")
		{
			activityAnm_type = 0;
			activePath.append("animation/texiao/changjingtexiao/");
			activePath.append(str_suffix_activity.at(0));
			activePath.append("/");
			activePath.append(collectInfo->get_activity_anm().c_str());
		}
		else if (str_suffix_activity.at(str_suffix_activity.size()-1) == "plist")
		{
			activityAnm_type = 1;
			activePath.append("animation/texiao/particledesigner/");
			activePath.append(collectInfo->get_activity_anm().c_str());
		}

		int invaildAnm_type = -1;
		std::vector<std::string> str_suffix_invaild = StrUtils::split(collectInfo->get_invaild_anm().c_str(),".");
		if (str_suffix_invaild.at(str_suffix_invaild.size()-1) == "anm")
		{
			invaildAnm_type = 0;
			invaildPath.append("animation/texiao/changjingtexiao/");
			invaildPath.append(str_suffix_invaild.at(0));
			invaildPath.append("/");
			invaildPath.append(collectInfo->get_invaild_anm().c_str());
		}
		else if (str_suffix_invaild.at(str_suffix_invaild.size()-1) == "plist")
		{
			invaildPath = 1;
			invaildPath.append("animation/texiao/particledesigner/");
			invaildPath.append(collectInfo->get_invaild_anm().c_str());
		}
		
		//add activityAnm
		if (activityAnm_type == 0)
		{
			if (strcmp(collectInfo->get_activity_anm().c_str(),"") != 0)
			{
				if (this->getChildByTag(kTagActivityAnm) == NULL)
				{
					CCLegendAnimation* activityAnm = CCLegendAnimation::create(activePath.c_str());
					activityAnm->setPlayLoop(true);
					activityAnm->setReleaseWhenStop(false);
					activityAnm->setPlaySpeed(1.0f);
					activityAnm->setPosition(ccp(collectInfo->get_activity_offSet_x(),collectInfo->get_activity_offSet_y()));
					activityAnm->setScale(1.0f);
					activityAnm->setTag(kTagActivityAnm);
					activityAnm->setVisible(true);
					addChild(activityAnm, ACTOR_HEAD_ZORDER);
				}
			}
		}
		else if (activityAnm_type == 1)
		{
			if (strcmp(collectInfo->get_activity_anm().c_str(),"") != 0)
			{
				if (this->getChildByTag(kTagActivityAnm) == NULL)
				{
					CCParticleSystem* activityAnm = CCParticleSystemQuad::create(activePath.c_str());
					activityAnm->setPositionType(kCCPositionTypeGrouped);
					activityAnm->setPosition(ccp(collectInfo->get_activity_offSet_x(),collectInfo->get_activity_offSet_y()));
					activityAnm->setScale(1.0f);
					activityAnm->setTag(kTagActivityAnm);
					activityAnm->setVisible(true);
					addChild(activityAnm, ACTOR_HEAD_ZORDER);
				}
			}
		}
		
		//add invaildAnm
		if (invaildAnm_type == 0)
		{
			if (strcmp(collectInfo->get_invaild_anm().c_str(),"") != 0)
			{
				if (this->getChildByTag(kTagInvaildAnm) == NULL)
				{
					CCLegendAnimation* invaildAnm = CCLegendAnimation::create(invaildPath.c_str());
					invaildAnm->setPlayLoop(true);
					invaildAnm->setReleaseWhenStop(false);
					invaildAnm->setPlaySpeed(1.0f);
					invaildAnm->setPosition(ccp(collectInfo->get_invaild_offSet_x(),collectInfo->get_invaild_offSet_y()));
					invaildAnm->setScale(1.0f);
					invaildAnm->setTag(kTagInvaildAnm);
					invaildAnm->setVisible(true);
					addChild(invaildAnm, ACTOR_HEAD_ZORDER);
				}
			}
		}
		else if (invaildAnm_type == 1)
		{
			if (strcmp(collectInfo->get_invaild_anm().c_str(),"") != 0)
			{
				if (this->getChildByTag(kTagInvaildAnm) == NULL)
				{
					//CCParticleSystem* invaildAnm = CCParticleSystemQuad::create("animation/texiao/particledesigner/caiji1.plist");
					CCParticleSystem* invaildAnm = CCParticleSystemQuad::create(invaildPath.c_str());
					invaildAnm->setPositionType(kCCPositionTypeGrouped);
					invaildAnm->setPosition(ccp(collectInfo->get_invaild_offSet_x(),collectInfo->get_invaild_offSet_y()));
					invaildAnm->setScale(1.0f);
					invaildAnm->setTag(kTagInvaildAnm);
					invaildAnm->setVisible(true);
					addChild(invaildAnm, ACTOR_HEAD_ZORDER);
				}
			}
		}
	}

	// load special effect
	CCParticleSystem* particleEffect = CCParticleSystemQuad::create("animation/texiao/particledesigner/caiji1.plist");
	particleEffect->setPositionType(kCCPositionTypeGrouped);
	particleEffect->setPosition(ccp(0,0));
	particleEffect->setScale(1.0f);
	particleEffect->setTag(kTagParticleEffect);
	particleEffect->setVisible(false);
	addChild(particleEffect, ACTOR_HEAD_ZORDER);

	setContentSize(CCSizeMake(40, PICKINGACTOR_ROLE_HEIGHT));

	return true;
}

void PickingActor::loadWithInfo(LevelActorInfo* levelActorInfo)
{
	PickingActorInfo* actorInfo = (PickingActorInfo*)levelActorInfo;

	int instanceId = actorInfo->instanceId;
	short rol = actorInfo->rol;
	short col = actorInfo->col;
	//char actionID = actorInfo->actionID;
	m_animActionID = actorInfo->actionID;
	char flag = actorInfo->flag;
	long templateId = actorInfo->templateId;
	m_nTemplateId = templateId;

	this->setRoleId(instanceId+SCENE_ACTOR_BASE_ID);

	// 翻转状态（0无反转，1有反转）
	bool bFlipX = flag & (1 << 0);
	// 可见性（0可见，1不可见）
	this->setVisible((flag & (1 << 3)) == 0);

	setType(GameActor::type_picking);

	this->setWorldPosition(ccp(GameSceneLayer::tileToPositionX(col), GameSceneLayer::tileToPositionY(rol)));

	CPickingInfo* pickingInfo = GameWorld::PickingInfos[templateId];
	this->setActorName(pickingInfo->name.c_str());
	this->init(pickingInfo->avatar.c_str());   // load animation resource

	if(bFlipX)
		this->setScaleX(-1.0f);

	//showActorName(true);

	// set the animation's direction by the scene editor's value
	//setAnimDir(actionID);
	//if(this->getAnim() != NULL)
	//	this->getAnim()->setAction(this->getAnimDir());
}

void PickingActor::showActorName(bool bShow)
{
	std::string namestr = getActorName();
	const char* name = namestr.c_str();

	CCLabelTTF* nameLabel = (CCLabelTTF*)this->getChildByTag(GameActor::kTagActorName);
	if(bShow)
	{
		if(nameLabel == NULL)
		{
			nameLabel = this->createNameLabel(name);
			this->addChild(nameLabel, GameActor::ACTOR_NAME_ZORDER);
		}
		else
		{
			nameLabel->setString(name);
		}
		nameLabel->setPosition(ccp(0, PICKINGACTOR_ROLE_HEIGHT));
	}
	else
	{
		if(nameLabel != NULL)
			nameLabel->removeFromParent();
	}
}

void PickingActor::setActivied( bool isactive )
{
	isActivied = isactive;
	//控制动画的播放与否
	CCNode* effect = this->getChildByTag(kTagParticleEffect);
	CCNode* activityAnm = this->getChildByTag(kTagActivityAnm);
	CCNode* invaildAnm = this->getChildByTag(kTagInvaildAnm);
	if (isActivied)
	{
		if(effect != NULL)
			effect->setVisible(true);

		if (activityAnm)
			activityAnm->setVisible(true);

		if (invaildAnm)
			invaildAnm->setVisible(false);
	}
	else
	{
		if(effect != NULL)
			effect->setVisible(false);
		//this->showCircle(false);
		if (activityAnm)
			activityAnm->setVisible(false);

		if (invaildAnm)
			invaildAnm->setVisible(true);

		this->getGameScene()->unselectActor(this);
	}
}

bool PickingActor::getActivied()
{
	return isActivied;
}

void PickingActor::set_templateId( long _value )
{
	m_nTemplateId = _value;
}

long PickingActor::get_templateId()
{
	return m_nTemplateId;
}
