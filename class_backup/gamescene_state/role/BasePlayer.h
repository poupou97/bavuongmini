#ifndef _LEGEND_GAMEACTOR_BASEPLAYER_H_
#define _LEGEND_GAMEACTOR_BASEPLAYER_H_

#include "BaseFighter.h"
#include "cocos2d.h"

#define PROFESSION_MJ_NAME "猛将"
#define PROFESSION_GM_NAME "鬼谋"
#define PROFESSION_HJ_NAME "豪杰"
#define PROFESSION_SS_NAME "神射"

#define PROFESSION_MJ_INDEX 1
#define PROFESSION_GM_INDEX 2
#define PROFESSION_HJ_INDEX 3
#define PROFESSION_SS_INDEX 4
#define MAX_PROFESSION_COUNT 4;  // profession max number

#define ANI_STAND 0			
#define ANI_RUN 1			
#define ANI_ATTACK_PHY 2	
#define ANI_ATTACK_MAG 3	
#define ANI_DEATH 4			
#define ANI_MAX 5

//role sex
#define ROLE_SEX_MEN 1
#define ROLE_SEX_WOMEMEN 2

#define BASIC_PLAYER_MOVESPEED 200.f

class GameActorAnimation;

/**
 * 玩家角色，包括玩家和其他玩家
 * @author zhaogang
 * @version 0.1.0
 */
class BasePlayer : public BaseFighter
{
public:
	enum PKMode {
		PKMode_peace = 0,    // 和平
		PKMode_kill,             // 杀戮
		PKMode_guide,         // 家族
		PKMode_country,     // 国家
	};

public:
	static int getProfessionIdxByName(std::string profession);
	/*增加 根据职业id 找职业名字 change by liuzhenxing*/
	static std::string getProfessionNameIdxByIndex(int pressionIndex);
	// figureName, for example, zsgr_npj, zsgr_sdw etc ( in the animation/player directory ).
	static int getProfessionIdxByFigureName(const char* figureName);

	//profession icon path
	static std::string getProfessionIconByStr(std::string professionStr);
	static std::string getProfessionIconByIndex(int professionIndex);

	//get player sex by profession
	static int getRoleSexByProfessionId(int professionId);

public:
	BasePlayer();
	virtual ~BasePlayer();

	static bool isDefaultAttack(const char* skillId);

	virtual void changeAnimation(int animName, int dir, bool bLoop, bool bPlay);

	int getProfession();
	void setProfession(int profession);

	/**
	* 是否暂离状态
	* 比如角色进入副本（竞技场等）后，在野外地图处于暂离状态
	*/
	bool isDissociate();
	/**
	* 设置游离状态
	* @param isDissociate
	*/
	void setDissociate(bool isDissociate);

	/**
	 * 根据职业，获取默认形象的动画名
	 */
	static std::string getDefaultAnimation(int profession);

	/**
	 * 根据职业，获取默认武器形象名
     * 比如: bow01, knife01, lance01, fan01, etc.
	 */
	static std::string getDefaultWeaponFigure(int profession);

	/**
	 * 根据职业，获取其用于普通攻击的技能名
	 */
	std::string getDefaultSkill();

	/** alive generals */
	void updateAliveGeneralsList();
	inline std::vector<long long>& getAliveGenerals() { return m_AliveGenerals; };

	// 增加无双状态效果
	void onAddMusouEffect();
	// 移除无双状态效果
	void onRemoveMusouEffect();
	/** 更新武将无双状态效果 */
	void updateMusouState();

	/**
	  * 更新阵法效果
	  **/
	void updateStrategyEffect();
	void removeStrategyEffect();
	/**
	  * 正在使用的阵法
	  */
	void setActiveStrategyId(int id);
	int getActiveStrategyId();

	inline void setPKMode(int mode) { m_nPKMode = mode; };
	inline int getPKMode() { return m_nPKMode; };

	/** 根据职业获得最小的头像全路径 */
	static std::string getSmallHeadPathByProfession(int profession);
	/** 根据职业获得小头像全路径 */
	static std::string getHeadPathByProfession(int profession);
	/** 根据职业获得大头像全路径 */
	static std::string getBigHeadPathByProfession(int profession);
	/** 根据职业获得半身像全路径 */
	static std::string getHalfBodyPathByProfession(int profession);
	/** get the profession's color
	   * now, the color is used for musou body effect
	  **/
	static int getColorByProfession(int profession);

	virtual void showActorName(bool bShow);

	/**
       * 根据武器名称，切换武器
	   * 也需要职业信息，用于空手时，根据职业获取默认武器信息
       **/
	static void switchWeapon(const char* weaponName, GameActorAnimation* pActorAnim, int profession);
	/** 
	   * 获取动画的武器图片索引
	   * 关键词: "weapon"
	   */
	static int getWeaponImgIdx(CCLegendAnimation* anim);

	/**
	   * weapon effect
	   * the special effect attached with the player's weapon
	   */
	void addWeaponEffect(const char* effectName);
	bool hasWeaponEffect(const char* effectName);
	void removeWeaponEffect(const char* effectName);
	void removeAllWeaponEffects();

	void attachWeaponEffect(const char* effectName);
	bool hasAttachedWeaponEffect(const char* effectName);
	void deattachWeaponEffect(const char* effectName);

	static int getWeaponType(int profession);

	virtual void initWeaponEffect();
	void refreshWeaponEffect(float delayTime);

//protected:
	void addCountryIcon(int countryId);
	/** the familay name and faction position in the family*/
	void addFamilyName(const char* familyName, int faction_position);
	void addVIPFlag(int vipLevel);

	std::vector<long long> m_AliveGenerals;   // 当前在游戏场景中，活着在战斗的武将

	/**
	   * fight or pk status
	   */
	// check if the player is fighting with other player
	bool isPKStatus();
	void setPKStatus(bool bValue);
	void clearPKStatus(float dt);

	// override
	virtual void onDamaged(BaseFighter* source, int damageNum, bool bDoubleAttacked, CSkillResult* skillResult = NULL);

private:
	std::vector<std::string> m_weaponEffects;

	int m_nProfession;
	bool m_bIsDissociate;
	int m_nPKMode;

	int m_activeStrategyId;
	float m_stragetyEffectTimer;

	bool m_bIsPkStatus;
};

#endif