#include "MyPlayerOwnedStates.h"

#include "MyPlayer.h"
#include "MyPlayerOwnedCommand.h"
#include "BaseFighterOwnedStates.h"
#include "../GameSceneState.h"
#include "GameActorAnimation.h"
#include "../../utils/GameUtils.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../skill/GameFightSkill.h"
#include "../skill/processor/SkillProcessor.h"
#include "../../legend_engine/GameWorld.h"
#include "../../messageclient/element/CPickingInfo.h"
#include "../../ui/pick_ui/PickUI.h"
#include "GameView.h"
#include "../../gamescene_state/MainScene.h"
#include "../../messageclient/element/CPushCollectInfo.h"
#include "PickingActor.h"
#include "SimpleAudioEngine.h"
#include "GameAudio.h"
#include "../../gamescene_state/role/ActorUtils.h"
#include "PresentBox.h"
#include "../../utils/GameConfig.h"
#include "BaseFighterConstant.h"
#include "../../utils/StaticDataManager.h"
#include "../sceneelement/MyPlayerInfoMini.h"

using namespace CocosDenshion;

//-----------------------------------------------------------------------Global state
MyPlayerGlobalState* MyPlayerGlobalState::Instance()
{
  static MyPlayerGlobalState instance;

  return &instance;
}

void MyPlayerGlobalState::Execute(MyPlayer* myplayer)
{

}

//------------------------------------------------------------------------
MyPlayerStand* MyPlayerStand::Instance()
{
  static MyPlayerStand instance;

  return &instance;
}

void MyPlayerStand::Enter(MyPlayer* self)
{
	// update the display status of the role
	self->updateWaterStatus();
	self->updateCoverStatus();

	// if the actor's anim is doing some special ACTION, do not reset position
	if(self->getActionByTag(BASEFIGHTER_FLYUP_ACTION) == NULL)
		self->getAnim()->setPosition(ccp(0,0));

	self->changeAnimation(ANI_STAND, self->getAnimDir(), true, true);

	self->clearPath();
}

void MyPlayerStand::Execute(MyPlayer* self)
{
	if(self->getPaths()->size() > 0)
	{
		self->GetFSM()->ChangeState(MyPlayerRun::Instance());
	}
}

void MyPlayerStand::Exit(MyPlayer* self)
{
}


//------------------------------------------------------------------------
MyPlayerRun* MyPlayerRun::Instance()
{
  static MyPlayerRun instance;

  return &instance;
}


void MyPlayerRun::Enter(MyPlayer* self)
{  
	CCAssert(self->getPaths()->size() > 0, "no move target ? error!");
	CCPoint offset = GameUtils::getDirection(self->getWorldPosition(), *self->getPaths()->at(0));
	self->setMoveDir(ccp(offset.x, -offset.y));

	self->changeAnimation(ANI_RUN, self->getAnimDir(), true, true);

	m_dBeginTime = GameUtils::millisecondNow();

	int size = self->getPaths()->size();
	CCPoint* destination = self->getPaths()->at(size - 1);
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1102, destination);

	// the first synchronize interval is very small
	// why it's small?
	// because send the position earlier, it will let other player know MyPlayer's position earlier
	m_nSynchronizeInterval = 100;   // millisecond

	this->setMoveTargetType(Type_Invalid);
}


void MyPlayerRun::Execute(MyPlayer* self)
{
	//CCLegendAnimation* run = self->m_pAnimBody[ANI_RUN];
	CCLegendAnimation* run = self->getAnim()->getAnim(ANI_COMPONENT_BODY, ANI_RUN);
	//CCLegendAnimation* runReflection = self->m_pAnimBodyReflection[ANI_RUN];
	CCLegendAnimation* runReflection = self->getAnim()->getAnim(ANI_COMPONENT_REFLECTION, ANI_RUN);
	//CCLegendAnimation* weaponRun = self->m_pAnimWeapons[ANI_RUN];

	// show or hide automove animation
	MyPlayerCommandMove* cmd = dynamic_cast<MyPlayerCommandMove*>(self->getCommand());
	if(cmd != NULL)
	{
		CCNode* pNode = self->getChildByTag(PLAYER_AUTO_MOVE_ANIMATION_TAG);

		if(cmd->autoMove)
		{
			if(pNode == NULL)
			{
				// 去掉 “自动战斗”的动画（如果有的话）
				CCNode* pNode = self->getChildByTag(PLAYER_AUTO_FIGHT_ANIMATION_TAG);
				if (NULL != pNode)
				{
					self->removeAutoCombatFlag();
				}

				CCLegendAnimation* pAnimNode = CCLegendAnimation::create("animation/texiao/jiemiantexiao/zdxl/zdxl.anm");
				pAnimNode->setScale(1.2f);
				pAnimNode->setTag(PLAYER_AUTO_MOVE_ANIMATION_TAG);
				pAnimNode->setPlayLoop(true);
				pAnimNode->setReleaseWhenStop(false);

				pAnimNode->setPosition(self->getHeadLabelPosition(PLAYER_AUTO_MOVE_ANIMATION_TAG));
				self->addChild(pAnimNode, GameActor::ACTOR_NAME_ZORDER);
			}
		}
		else
		{
			if(pNode != NULL)
			{
				pNode->removeFromParent();
			}
		}
	}

	// 立即结束指令
	if(cmd != NULL && cmd->isFinishImmediately)
	{
		self->clearPath();
		//self->GetFSM()->ChangeState(MyPlayerStand::Instance());
		cmd->setStatus(ActorCommand::status_finished);
		return;
	}

	CCPoint dir = self->getMoveDir();

	float dt = self->m_dt;
	float speed = 180.0f;
	float distance = speed * dt;

	// no input, then stand
	//if(dir.x == 0 && dir.y == 0)
	if(self->getPaths()->size() <= 0)
	{
		// 没有指令的情况下，当没有移动目标时，到站立状态
		//if(cmd == NULL)
		//	self->GetFSM()->ChangeState(MyPlayerStand::Instance());
	}
	else
	{
		//CCPoint offset = ccpMult(joystick->getDirection(), distance);
		//run->setPosition( ccpSub(run->getPosition(), offset) );

		// 只能以8方向中的1个方向进行移动

		//int animDirection = joystick->getAnimDirection();
		//int animDirection = self->getAnimDirection();
		int animDirection = self->getAnimDirection(ccp(0, 0), dir, DEFAULT_ANGLES_DIRECTION_DATA);
		//CCLog("run state, move dir: (%f,%f), anim dir: %f", dir.x, dir.y, animDirection);
		//CCLog("run state, anim dir: %d", animDirection);
		self->setAnimDir(animDirection);

		//CCPoint joystickDirection;
		//if(animDirection == 0)
		//	joystickDirection = ccpNormalize(CCPointMake(0, -2));
		//else if(animDirection == 1)
		//	joystickDirection = ccpNormalize(CCPointMake(2, -2));
		//else if(animDirection == 2)
		//	joystickDirection = ccpNormalize(CCPointMake(2, 0));
		//else if(animDirection == 3)
		//	joystickDirection = ccpNormalize(CCPointMake(2, 2));
		//else if(animDirection == 4)
		//	joystickDirection = ccpNormalize(CCPointMake(0, 2));
		//else if(animDirection == 5)
		//	joystickDirection = ccpNormalize(CCPointMake(-2, 2));
		//else if(animDirection == 6)
		//	joystickDirection = ccpNormalize(CCPointMake(-2, 0));
		//else if(animDirection == 7)
		//	joystickDirection = ccpNormalize(CCPointMake(-2, -2));

		//CCPoint joystickDirection;
		//if(animDirection == 0)
		//	joystickDirection = (CCPointMake(0, -1.3f));
		//else if(animDirection == 1)
		//	joystickDirection = (CCPointMake(1.55f, -0.75f));
		//else if(animDirection == 2)
		//	joystickDirection = (CCPointMake(2, 0));
		//else if(animDirection == 3)
		//	joystickDirection = (CCPointMake(1.55f, 0.75f));
		//else if(animDirection == 4)
		//	joystickDirection = (CCPointMake(0, 1.3f));
		//else if(animDirection == 5)
		//	joystickDirection = (CCPointMake(-1.55, 0.75f));
		//else if(animDirection == 6)
		//	joystickDirection = (CCPointMake(-2, 0));
		//else if(animDirection == 7)
		//	joystickDirection = (CCPointMake(-1.55f, -0.75f));

		////float speed = 180.0f;
		//float speed = 100.0f;
		//float distance = speed * dt;
		//CCPoint offset = ccpMult(joystickDirection, distance);
		//run->setPosition( ccpSub(run->getPosition(), offset) );

		int tempaction = animDirection;
		//if(animDirection == 0)
		//	tempaction = 0;
		//else if(animDirection == 1)
		//	tempaction = 2;
		//else if(animDirection == 2)
		//	tempaction = 2;
		//else if(animDirection == 3)
		//	tempaction = 2;
		//else if(animDirection == 4)
		//	tempaction = 4;
		//else if(animDirection == 5)
		//	tempaction = 6;
		//else if(animDirection == 6)
		//	tempaction = 6;
		//else if(animDirection == 7)
		//	tempaction = 6;

		//run->setAction(tempaction);
		//run->setPlaySpeed(self->getMoveSpeed()/BASIC_PLAYER_MOVESPEED);
		//runReflection->setAction(tempaction);
		//runReflection->setPlaySpeed(self->getMoveSpeed()/BASIC_PLAYER_MOVESPEED);
		//weaponRun->setAction(tempaction);
		//weaponRun->setPlaySpeed(self->getMoveSpeed()/BASIC_PLAYER_MOVESPEED);

		self->getAnim()->setAction(tempaction);
		self->getAnim()->setPlaySpeed((float)(self->getMoveSpeed()/BASIC_PLAYER_MOVESPEED));
	}

	// send player's current position to game server
	double currentTime = GameUtils::millisecondNow();
	if(currentTime - m_dBeginTime >= m_nSynchronizeInterval)
	{
		m_nSynchronizeInterval = 2000;   // millisecond
		// if the player has taget, send it to server, otherwize, send the player's current position
		int size = self->getPaths()->size();
		if(size >= 1)
		{
			CCPoint* destination = self->getPaths()->at(size - 1);
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1102, destination);

		}
		else
		{
            CCPoint* newPoint = new CCPoint(self->getWorldPosition());
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1102, newPoint);
            delete newPoint;
		}

		GameMessageProcessor::sharedMsgProcessor()->sendReq(1103, self);
		m_dBeginTime = currentTime;
	}
}

void MyPlayerRun::Exit(MyPlayer* self)
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1104, self);

	CCNode* pNode = self->getChildByTag(PLAYER_AUTO_MOVE_ANIMATION_TAG);
	if(pNode != NULL)
		pNode->removeFromParent();

	// restore animation play speed
	self->getAnim()->setPlaySpeed(1.0f);
}


//------------------------------------------------------------------------
MyPlayerAttack* MyPlayerAttack::Instance()
{
  static MyPlayerAttack instance;

  return &instance;
}


void MyPlayerAttack::Enter(MyPlayer* self)
{  
	// init skill effect by skill info
	
	CCAssert(self->getCommand()->getType() == ActorCommand::type_attack, "invalid command");
	MyPlayerCommandAttack* cmd = dynamic_cast<MyPlayerCommandAttack*>(self->getCommand());

	GameFightSkill* fightSkill = self->getGameFightSkill(cmd->skillId.c_str(), cmd->skillProcessorId.c_str());
	CCAssert(fightSkill != NULL, "should not be nil");

	int animName = self->getGameFightSkill(cmd->skillId.c_str(), cmd->skillProcessorId.c_str())->getSkillBin()->action;
	if(BasePlayer::isDefaultAttack(cmd->skillId.c_str()))   // normal attack, not skill
	{
		// action index
		// 0 "stand",
		// 1 "run",
		// 2 "attack1",
		// 3 "attack2",
		// 4 "death",
		if(CCRANDOM_0_1() <= 0.6f)
			animName = 2;
		else
			animName = 3;
	}
	self->changeAnimation(animName, self->getAnimDir(), false, true);
	self->getAnim()->setPlaySpeed(cmd->attackSpeed);

	GameSceneLayer* scene = self->getGameScene();
	BaseFighter* bf = dynamic_cast<BaseFighter*>(scene->getActor(cmd->targetId));
	CCPoint skillPosition = ActorUtils::getDefaultSkillPosition(self, cmd->skillId.c_str(), cmd->skillProcessorId.c_str());
	self->onSkillBegan(cmd->skillId.c_str(), cmd->skillProcessorId.c_str(), bf, skillPosition.x, skillPosition.y);

	m_bIsSkillReleased = false;   // skill has not been released
	m_nReleasePhase = self->getGameFightSkill(cmd->skillId.c_str(), cmd->skillProcessorId.c_str())->getSkillBin()->releasePhase;
}

void MyPlayerAttack::Execute(MyPlayer* self)
{
	GameSceneLayer* scene = self->getGameScene();
	//CCLegendAnimation* attack = self->m_pAnimBody[ANI_ATTACK_PHY];
	//CCLegendAnimation* attackReflection = self->m_pAnimBodyReflection[ANI_ATTACK_PHY];
	CCLegendAnimation* attack = self->getAnim()->getAnim(ANI_COMPONENT_BODY, self->getAnimName());
	CCLegendAnimation* attackReflection = self->getAnim()->getAnim(ANI_COMPONENT_REFLECTION, self->getAnimName());

	MyPlayerCommandAttack* cmd = dynamic_cast<MyPlayerCommandAttack*>(self->getCommand());
	if(cmd == NULL)
		return;

	if(attack->getPlayPercent() >= (m_nReleasePhase / 100.f) && !m_bIsSkillReleased)
	{
		//CCLOG("skill id in Attack action: %s", cmd->skillId.c_str());
		BaseFighter* bf = dynamic_cast<BaseFighter*>(scene->getActor(cmd->targetId));
		
		CCPoint skillPosition = ActorUtils::getDefaultSkillPosition(self, cmd->skillId.c_str(), cmd->skillProcessorId.c_str());
		self->onSkillReleased(cmd->skillId.c_str(), cmd->skillProcessorId.c_str(), bf, skillPosition.x, skillPosition.y);
		m_bIsSkillReleased = true;

		// first attack action and the player use the skill by himself, "shout" the art skill name
		if(cmd->attackCount == 0 /*&& cmd->commandSource == ActorCommand::source_from_player*/)
		{
			if(BasePlayer::isDefaultAttack(cmd->skillId.c_str()))
				return;

			ActorUtils::addSkillName(self, cmd->skillId.c_str(), cmd->skillProcessorId.c_str());
		}
	}

	// attack is stop
	if(!attack->isPlaying())
	{
		BaseFighter* bf = NULL;

		short x = 0;
		short y = 0;

		//byte lockMode = getLockMode(currSkillIdx);

		//if (FightSkill.isLockSingle(lockMode)) {
			bf = (BaseFighter*)self->getLockedActor();
			//if(bf == NULL){
			//	x = skills[currSkillIdx].fightSkill.x;
			//	y = skills[currSkillIdx].fightSkill.y;
			//}else{
			//	x = bf.getX();
			//	y = bf.getY();
			//}
		//} else if (FightSkill.isNoneLock(lockMode)) {
		//	bf = this;
		//	x = getX();
		//	y = getY();
		//} else {
		//	x = skills[currSkillIdx].fightSkill.x;
		//	y = skills[currSkillIdx].fightSkill.y;
		//}

		//self->useSkill(FightSkill_SKILL_ID_PUTONGGONGJI, bf, 0, 0);
		//self->onSkillReleased(cmd->skillId.c_str(), bf, 0, 0);

		//// 立即结束指令
		//if(cmd->isFinishImmediately)
		//{
		//	self->GetFSM()->ChangeState(MyPlayerStand::Instance());
		//	cmd->setStatus(ActorCommand::status_finished);
		//	return;
		//}

		cmd->attackCount++;

		//if(self->m_pressedSkillIdx == 0)   // the skill button is still pressed, so continue attack
		if(cmd != NULL && cmd->skillId != "")
		{
			// when attacking, the direction still can be modified by the joystick
			//Joystick* joystick = self->m_pJoyStick;
			////int animDirection = joystick->getAnimDirection();
			//int animDirection = self->getAnimDirection(ccp(0, 0), joystick->getDirection(), DEFAULT_ANGLES_DIRECTION_DATA);
			//if(animDirection != -1)
			//{
			//	self->setAnimDir(animDirection);
			//	//attack->setAction(self->m_animDirection);
			//	//attackReflection->setAction(self->m_animDirection);
			//	self->m_pAnim->setAction(self->getAnimDir());
			//}

			BaseFighter* bf = dynamic_cast<BaseFighter*>(scene->getActor(cmd->targetId));
			if(bf == NULL)
				return;
			int animDirection = self->getAnimDirection(scene->convertToCocos2DWorldSpace(self->getWorldPosition()), 
				scene->convertToCocos2DWorldSpace(bf->getWorldPosition()), DEFAULT_ANGLES_DIRECTION_DATA);
			self->setAnimDir(animDirection);
			self->getAnim()->setAction(self->getAnimDir());
			self->getAnim()->play();
			return;
		}
		else
		{
			self->GetFSM()->ChangeState(MyPlayerStand::Instance());
		}
	}
}

void MyPlayerAttack::Exit(MyPlayer* self)
{
	self->getAnim()->setPlaySpeed(1.0f);   // restore to normal attack speed
}

//------------------------------------------------------------------------
MyPlayerDeath* MyPlayerDeath::Instance()
{
  static MyPlayerDeath instance;

  return &instance;
}


void MyPlayerDeath::Enter(MyPlayer* self)
{  
	GameUtils::playGameSound(MYPLAYER_DIED, 2, false);

	self->changeAnimation(ANI_DEATH, self->getAnimDir(), false, true);

	self->setPKStatus(false);
}

void MyPlayerDeath::Execute(MyPlayer* self)
{

}

void MyPlayerDeath::Exit(MyPlayer* self)
{

}

//------------------------------------------------------------------------
MyPlayerPick* MyPlayerPick::Instance()
{
  static MyPlayerPick instance;

  return &instance;
}

void MyPlayerPick::Enter(MyPlayer* self)
{
	self->changeAnimation(ANI_STAND, self->getAnimDir(), true, true);

	self->clearPath();

	// 根据采集指令的目标，初始化动作
	CCAssert(self->getCommand()->getType() == ActorCommand::type_pick || self->getCommand()->getType() == ActorCommand::type_collectPresent, "invalid command");

	if (self->getCommand()->getType() == ActorCommand::type_collectPresent)
	{
		MyPlayerCommandPick* cmd = (MyPlayerCommandPick*)self->getCommand();
		m_pickingTargetId = cmd->targetId;

		CCNode* pPickingUI = GameView::getInstance()->getMainUIScene()->getChildByTag(kTagPickUI);
		if (pPickingUI != NULL)
		{
			pPickingUI->removeFromParent();
		}

		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		// 进度条读取的时间
		int nMilliSecond = GameConfig::getIntForKey("present_time");
		m_ftime = nMilliSecond;
		PickUI * pickUI = PickUI::create(m_ftime);
		pickUI->setTag(kTagPickUI);
		pickUI->ignoreAnchorPointForPosition(false);
		pickUI->setAnchorPoint(ccp(0.5f,0.5f));
		pickUI->setPosition(ccp(winsize.width/2,winsize.height/2-50));
		GameView::getInstance()->getMainUIScene()->addChild(pickUI);

		m_fPickingTime = GameUtils::millisecondNow();
	}
	else
	{
		MyPlayerCommandPick* cmd = (MyPlayerCommandPick*)self->getCommand();
		m_pickingTargetId = cmd->targetId;

		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();

		// 获取采集物的采集时长，创建进度条来显示采集进度
		// 	
		// 	CCLabelTTF *pLabel = CCLabelTTF::create("picking...", "Marker Felt", 30);
		// 	pLabel->setPosition(ccp(visibleSize.width/2, visibleSize.height/2));
		// 	self->getGameScene()->addChild(pLabel, SCENE_TOP_LAYER_BASE_ZORDER);
		// 	pLabel->setTag(12123456);

		// 获取进度条时长
		PickingActor * pickingActor = (PickingActor*)GameView::getInstance()->getGameScene()->getActor(m_pickingTargetId);

		CCNode* pPickingUI = GameView::getInstance()->getMainUIScene()->getChildByTag(kTagPickUI);
		if (pPickingUI != NULL)
		{
			pPickingUI->removeFromParent();
		}

		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();
		CPickingInfo* pickingInfo = GameWorld::PickingInfos[pickingActor->p_collectInfo->id()];   // get template Id
		m_ftime = pickingInfo->actionTime;
		PickUI * pickUI = PickUI::create(pickingInfo);
		pickUI->setTag(kTagPickUI);
		pickUI->ignoreAnchorPointForPosition(false);
		pickUI->setAnchorPoint(ccp(0.5f,0.5f));
		pickUI->setPosition(ccp(winsize.width/2,winsize.height/2-50));
		GameView::getInstance()->getMainUIScene()->addChild(pickUI);

		m_fPickingTime = GameUtils::millisecondNow();
	}
}

void MyPlayerPick::Execute(MyPlayer* self)
{
	// 根据采集指令的目标，初始化动作
	CCAssert(self->getCommand()->getType() == ActorCommand::type_pick || self->getCommand()->getType() == ActorCommand::type_collectPresent, "invalid command");
	if (self->getCommand()->getType() == ActorCommand::type_collectPresent)
	{
		// 根据采集指令的目标，初始化动作
		CCAssert(self->getCommand()->getType() == ActorCommand::type_collectPresent, "invalid command");
		MyPlayerCommandPick* cmd = (MyPlayerCommandPick*)self->getCommand();

		PresentBox * presentBox = (PresentBox*)GameView::getInstance()->getGameScene()->getActor(m_pickingTargetId);
		if (NULL == presentBox)
		{
			// 打断采集
			self->changeAction(ACT_STAND);

			// 发送宝箱已经被采集走的消息
			const char * charInfo = StringDataManager::getString("PresentBox_openFailed");
			GameView::getInstance()->showAlertDialog(charInfo);
		}


		// 采集进度完毕
		if(GameUtils::millisecondNow() - m_fPickingTime > m_ftime)
		{
			cmd->pickingNumber++;   // 向采集指令汇报
		}
	}
	else
	{
		MyPlayerCommandPick* cmd = (MyPlayerCommandPick*)self->getCommand();

		PickingActor * pickingActor = (PickingActor*)GameView::getInstance()->getGameScene()->getActor(m_pickingTargetId);
		if (pickingActor)
		{
			if (!pickingActor->getActivied())
			{
				// 打断采集
				self->changeAction(ACT_STAND);
			}
		}

		// 采集进度完毕
		if(GameUtils::millisecondNow() - m_fPickingTime > m_ftime)
			cmd->pickingNumber++;   // 向采集指令汇报


		// 	if()
		// 	{
		// 		self->GetFSM()->ChangeState(MyPlayerStand::Instance());
		// 	}
	}
}

void MyPlayerPick::Exit(MyPlayer* self)
{
	// 若进度条还在，则移除进度条
// 	CCNode* pLabel = self->getGameScene()->getChildByTag(12123456);
// 	if(self->getGameScene()->getChildByTag(12123456) != NULL)
// 		pLabel->removeFromParent();

	MainScene* mainUIScene = GameView::getInstance()->getMainUIScene();
	if(mainUIScene != NULL)
	{
		PickUI * pickUI = (PickUI *)mainUIScene->getChildByTag(kTagPickUI);
		if (pickUI != NULL)
		{
			pickUI->removeFromParent();
		}
	}
}

//------------------------------------------------------------------------
MyPlayerKnockBack* MyPlayerKnockBack::Instance()
{
  static MyPlayerKnockBack instance;

  return &instance;
}

void MyPlayerKnockBack::Enter(MyPlayer* self)
{
	self->changeAnimation(ANI_STAND, self->getAnimDir(), false, false);

	self->clearPath();
}

void MyPlayerKnockBack::Execute(MyPlayer* self)
{
}

void MyPlayerKnockBack::Exit(MyPlayer* self)
{
}

//------------------------------------------------------------------------
MyPlayerWhirl* MyPlayerWhirl::Instance()
{
  static MyPlayerWhirl instance;

  return &instance;
}


void MyPlayerWhirl::Enter(MyPlayer* self)
{  
	self->setAnimName(ANI_ATTACK_MAG);

	CCLegendAnimation* attack1 = self->getAnim()->getAnim(ANI_COMPONENT_BODY, ANI_ATTACK_MAG);
	attack1->setVisible(true);
	attack1->setPlayLoop(true);
	attack1->play();

	//CCLegendAnimation* whirl = self->m_pAnimOverlap;
	//whirl->setVisible(true);
	//whirl->setPlayLoop(true);
	//whirl->play();

	m_timer = 0;
}

void MyPlayerWhirl::Execute(MyPlayer* self)
{
	// when attacking, the direction still can be modified
	Joystick* joystick = self->m_pJoyStick;
	//int animDirection = joystick->getAnimDirection();
	int animDirection = self->getAnimDirection(ccp(0, 0), joystick->getDirection(), DEFAULT_ANGLES_DIRECTION_DATA);
	if(animDirection != -1)
	{
		self->setAnimDir(animDirection);
		//self->m_pAnimAttack->setAction(self->m_animDirection);
	}

	// time over, stand or run
	m_timer += (self->m_dt * 1000);
	if(m_timer > 3000)
	{
		if(animDirection != -1)
		{
			self->GetFSM()->ChangeState(MyPlayerRun::Instance());
		}
		else
		{
			self->GetFSM()->ChangeState(MyPlayerStand::Instance());
		}
	}
}

void MyPlayerWhirl::Exit(MyPlayer* self)
{
	//self->m_pAnimBody[ANI_ATTACK_MAG]->setVisible(false);
	//self->m_pAnimBodyReflection[ANI_ATTACK_MAG]->setVisible(false);

	self->getAnim()->getAnim(ANI_COMPONENT_BODY, ANI_ATTACK_MAG)->setVisible(false);
	self->getAnim()->getAnim(ANI_COMPONENT_REFLECTION, ANI_ATTACK_MAG)->setVisible(false);

	//self->m_pAnimOverlap->setVisible(false);
}

//------------------------------------------------------------------------
MyPlayerCharge* MyPlayerCharge::Instance()
{
  static MyPlayerCharge instance;

  return &instance;
}

void MyPlayerCharge::Enter(MyPlayer* self)
{
	ActionChargeContext* context = (ActionChargeContext*)self->getActionContext(ACT_CHARGE);

	// adjust the direction
	CCPoint targetWorldPosition = self->getGameScene()->convertToGameWorldSpace(context->chargetTargetPosition);
	CCPoint offset = GameUtils::getDirection(self->getWorldPosition(), targetWorldPosition);
	int animDirection = self->getAnimDirection(ccp(0, 0), ccp(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);
	//CCAssert(animDirection != -1, "invalid anim dir");
	self->setAnimDir(animDirection);

	self->changeAnimation(ANI_RUN, self->getAnimDir(), true, true);

	// 获取动画播放速度
	self->getAnim()->setPlaySpeed(context->chargeMoveSpeed/BASIC_PLAYER_MOVESPEED);

	//CCMoveTo
	CCFiniteTimeAction*  action = CCSequence::create(
		CCMoveTo::create(context->chargeTime, context->chargetTargetPosition),
		NULL);
	self->runAction(action);

	self->clearPath();
}

void MyPlayerCharge::Execute(MyPlayer* self)
{
	ActionChargeContext* context = (ActionChargeContext*)self->getActionContext(ACT_CHARGE);
	if(context->finished)
		return;

	//if(self->getPosition().equals( context->chargetTargetPosition) )   // using position to decide is not reliable
	if(GameUtils::millisecondNow() - context->startTime >= context->chargeTime * 1000)
	{
		context->finished = true;
	}
}

void MyPlayerCharge::Exit(MyPlayer* self)
{
	// restore animation play speed
	self->getAnim()->setPlaySpeed(1.0f);
}

//------------------------------------------------------------------------
MyPlayerJump* MyPlayerJump::Instance()
{
  static MyPlayerJump instance;

  return &instance;
}

void MyPlayerJump::Enter(MyPlayer* self)
{
	BaseFighterJump::setJumpAnimId(ANI_STAND);
	BaseFighterJump::Enter(self);   // common handler for the jump action
}

void MyPlayerJump::Execute(MyPlayer* self)
{
	BaseFighterJump::Execute(self);   // common handler for the jump action
}

void MyPlayerJump::Exit(MyPlayer* self)
{
	BaseFighterJump::Exit(self);   // common handler for the jump action
}

