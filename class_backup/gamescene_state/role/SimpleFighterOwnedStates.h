#ifndef _SIMPLEFIGHTER_OWNED_STATES_H
#define _SIMPLEFIGHTER_OWNED_STATES_H

#include "State.h"

class SimpleFighter;

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class SimpleFighterGlobalState : public State<SimpleFighter>
{  
private:
  
  SimpleFighterGlobalState(){}
  
  //copy ctor and assignment should be private
  SimpleFighterGlobalState(const SimpleFighterGlobalState&);
  SimpleFighterGlobalState& operator=(const SimpleFighterGlobalState&);
 
public:

  static SimpleFighterGlobalState* Instance();
  
  virtual void Enter(SimpleFighter* self){}

  virtual void Execute(SimpleFighter* self);

  virtual void Exit(SimpleFighter* self){}
};


//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class SimpleFighterStand : public State<SimpleFighter>
{
private:
  
  SimpleFighterStand(){}

  //copy ctor and assignment should be private
  SimpleFighterStand(const SimpleFighterStand&);
  SimpleFighterStand& operator=(const SimpleFighterStand&); 
  
public:

  static SimpleFighterStand* Instance();
  
  virtual void Enter(SimpleFighter* self);

  virtual void Execute(SimpleFighter* self);

  virtual void Exit(SimpleFighter* self);

};



//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class SimpleFighterRun : public State<SimpleFighter>
{
private:
  
  SimpleFighterRun(){}

  //copy ctor and assignment should be private
  SimpleFighterRun(const SimpleFighterRun&);
  SimpleFighterRun& operator=(const SimpleFighterRun&);
 
public:

  static SimpleFighterRun* Instance();
  
  virtual void Enter(SimpleFighter* self);

  virtual void Execute(SimpleFighter* self);

  virtual void Exit(SimpleFighter* self);

};

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class SimpleFighterAttack : public State<SimpleFighter>
{
private:
  
  SimpleFighterAttack(){}

  //copy ctor and assignment should be private
  SimpleFighterAttack(const SimpleFighterAttack&);
  SimpleFighterAttack& operator=(const SimpleFighterAttack&);
 
public:

  static SimpleFighterAttack* Instance();
  
  virtual void Enter(SimpleFighter* self);

  virtual void Execute(SimpleFighter* self);

  virtual void Exit(SimpleFighter* self);

};

//------------------------------------------------------------------------
//

//------------------------------------------------------------------------
class SimpleFighterDeath : public State<SimpleFighter>
{
private:
  
  SimpleFighterDeath(){}

  //copy ctor and assignment should be private
  SimpleFighterDeath(const SimpleFighterDeath&);
  SimpleFighterDeath& operator=(const SimpleFighterDeath&);
 
public:

  static SimpleFighterDeath* Instance();
  
  virtual void Enter(SimpleFighter* self);

  virtual void Execute(SimpleFighter* self);

  virtual void Exit(SimpleFighter* self);

};

#endif