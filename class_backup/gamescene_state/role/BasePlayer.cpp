#include "BasePlayer.h"

#include "General.h"
#include "ActorCommand.h"
#include "GameActorAnimation.h"
#include "../GameSceneState.h"
#include "../GameSceneEffects.h"
#include "../../utils/GameUtils.h"
#include "../../utils/pathfinder/AStarPathFinder.h"
#include "../skill/SkillSeed.h"
#include "../skill/SkillManager.h"
#include "../skill/GameFightSkill.h"
#include "../../legend_engine/LegendAAnimation.h"
#include "../../legend_engine/CCLegendAnimation.h"
#include "../../messageclient/element/CMapInfo.h"
#include "AppMacros.h"
#include "WeaponEffect.h"
#include "ActorUtils.h"
#include "BaseFighterConstant.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../../messageclient/element/CGuildBase.h"
#include "../../utils/StrUtils.h"

BasePlayer::BasePlayer()
: m_activeStrategyId(-1)
, m_stragetyEffectTimer(0)
, m_bIsPkStatus(false)
, m_nProfession(PROFESSION_MJ_INDEX)
{

}

BasePlayer::~BasePlayer()
{

}

bool BasePlayer::isDefaultAttack(const char* skillId)
{
	std::string id = skillId;
	if(id == FightSkill_SKILL_ID_WARRIORDEFAULTATTACK)
		return true;
	else if(id == FightSkill_SKILL_ID_MAGICDEFAULTATTACK)
		return true;
	else if(id == FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK)
		return true;
	else if(id == FightSkill_SKILL_ID_RANGERDEFAULTATTACK)
		return true;

	return false;
}

int BasePlayer::getProfessionIdxByName(std::string profession){
	if (profession == PROFESSION_MJ_NAME) {
		return PROFESSION_MJ_INDEX;
	} else if (profession == PROFESSION_GM_NAME) {
		return PROFESSION_GM_INDEX;
	} else if (profession == PROFESSION_HJ_NAME) {
		return PROFESSION_HJ_INDEX;
	} else if (profession == PROFESSION_SS_NAME) {
		return PROFESSION_SS_INDEX;
	}

	CCAssert(false, "profession is invalid.");
	return -1;
}

std::string BasePlayer::getProfessionNameIdxByIndex( int pressionIndex )
{
	std::string pressionName_="";
	switch(pressionIndex)
	{
	case PROFESSION_MJ_INDEX:
		{
			pressionName_.append("猛将");
		}break;
	case PROFESSION_GM_INDEX:
		{
			pressionName_.append("鬼谋");
		}break;
	case PROFESSION_HJ_INDEX:
		{
			pressionName_.append("豪杰");
		}break;
	case PROFESSION_SS_INDEX:
		{
			pressionName_.append("神射");
		}break;
	}
	return pressionName_;
}

int BasePlayer::getProfessionIdxByFigureName(const char* figureName)
{
	std::string str_figureName = figureName;

	int index = str_figureName.find("zsgr_npj");
	if(index >= 0)
	{
		return PROFESSION_MJ_INDEX;
	}
	index = str_figureName.find("zsgr_tjw");
	if (index >= 0)
	{
		return PROFESSION_GM_INDEX;
	} 
	index = str_figureName.find("zsgr_sdw");
	if (index >= 0)
	{
		return PROFESSION_HJ_INDEX;
	} 
	index = str_figureName.find("zsgr_sym");
	if (index >= 0)
	{
		return PROFESSION_SS_INDEX;
	}

	CCAssert(false, "profession is invalid.");
	return -1;
}

int BasePlayer::getProfession() {
	return m_nProfession;
}
void BasePlayer::setProfession(int profession) {
	m_nProfession = profession;
}

bool BasePlayer::isDissociate() {
	return m_bIsDissociate;
}
void BasePlayer::setDissociate(bool isDissociate) {
	this->m_bIsDissociate = isDissociate;
}

std::string BasePlayer::getDefaultAnimation(int profession)
{
	switch(profession)
	{
	case PROFESSION_MJ_INDEX:
		return "zsgr_npj";
		break;
	case PROFESSION_GM_INDEX:
		return "zsgr_tjw";
		break;
	case PROFESSION_HJ_INDEX:
		return "zsgr_sdw";   // zsgr_sdm
		break;
	case PROFESSION_SS_INDEX:
		return "zsgr_sym";
		break;
	}

	return "";
}

std::string BasePlayer::getDefaultWeaponFigure(int profession)
{
	switch(profession)
	{
	case PROFESSION_MJ_INDEX:
		return "lance01";
		break;
	case PROFESSION_GM_INDEX:
		return "fan01";
		break;
	case PROFESSION_HJ_INDEX:
		return "knife01";
		break;
	case PROFESSION_SS_INDEX:
		return "bow01";
		break;
	}

	return "";
}

std::string BasePlayer::getDefaultSkill()
{
	switch(m_nProfession)
	{
	case PROFESSION_MJ_INDEX:
		return FightSkill_SKILL_ID_WARRIORDEFAULTATTACK;
		break;
	case PROFESSION_GM_INDEX:
		return FightSkill_SKILL_ID_MAGICDEFAULTATTACK;
		break;
	case PROFESSION_HJ_INDEX:
		return FightSkill_SKILL_ID_WARLOCKDEFAULTATTACK;
		break;
	case PROFESSION_SS_INDEX:
		return FightSkill_SKILL_ID_RANGERDEFAULTATTACK;
		break;
	}

	return "";
}

void BasePlayer::changeAnimation(int animName, int dir, bool bLoop, bool bPlay)
{
	BaseFighter::changeAnimation(animName, dir, bLoop, bPlay);

	// after change anim, swith his weapon
	//BasePlayer::switchWeapon(this->player->activerole().hand().c_str(), m_pAnim);   // "lance02"
	BasePlayer::switchWeapon(this->getActiveRole()->hand().c_str(), m_pAnim, this->getProfession());   // "lance02"

	// update the weapon effect
	// 1. remove weapon effects
	CCLegendAnimation* pAnim = m_pAnim->getAnim(ANI_COMPONENT_BODY, animName);
	WeaponEffect::removeAllWeaponEffects(pAnim);
	// 2. add weapon effects
	for (std::vector<std::string>::iterator it = m_weaponEffects.begin(); it != m_weaponEffects.end(); ++it) 
	{
		std::string effectName = *it;
		if(effectName != "")
		{
			if(!hasAttachedWeaponEffect(effectName.c_str()))
			{
				attachWeaponEffect(effectName.c_str());
			}
		}
	}
}

void BasePlayer::updateAliveGeneralsList()
{
	m_AliveGenerals.clear();

	GameSceneLayer* scene = this->getGameScene();
	for (std::map<long long,GameActor*>::iterator it = scene->getActorsMap().begin(); it != scene->getActorsMap().end(); ++it)
	{
		General* pGeneral = dynamic_cast<General*>(it->second);
		if(pGeneral != NULL)
		{
			if(pGeneral->isAction(ACT_DIE))
				continue;

			// check the general belongs to this player
			if(pGeneral->getOwnerId() == this->getRoleId())
				m_AliveGenerals.push_back(pGeneral->getRoleId());
		}
	}
}

void BasePlayer::onAddMusouEffect()
{
	// link effect on foot
	CCNode* actorLayer = this->getGameScene()->getActorLayer();
	CCNode* node = actorLayer->getChildByTag((int)this->getRoleId());
	if(node == NULL)
	{
		LinkSpecialEffect* effect = LinkSpecialEffect::create("animation/texiao/renwutexiao/lightning.anm");
		//actorLayer->addChild(effect, SCENE_GROUND_LAYER_BASE_ZORDER, (int)this->getRoleId());
		actorLayer->addChild(effect, SCENE_ROLE_LAYER_BASE_ZORDER, (int)this->getRoleId());

		//CCActionInterval *action = (CCActionInterval*)CCSequence::create
		//	(
		//		CCShow::create(),
		//		CCDelayTime::create(5.0f),
		//		CCRemoveSelf::create(),
		//		NULL
		//	);
		//effect->runAction(action);
	}

	// effect on body
	ActorUtils::addMusouBodyEffect(this, this->getColorByProfession(this->getProfession()));

	//for(unsigned int i = 0; i < m_AliveGenerals.size(); i++)
	//{
	//	General* general = dynamic_cast<General*>(this->getGameScene()->getActor(m_AliveGenerals.at(i)));
	//	if(general != NULL)
	//	{
	//		effectTag = kTagMosouEffect+general->getRoleId();
	//		CCNode* container = general->getChildByTag(BaseFighter::kTagFootEffectContainer);
	//		if(container->getChildByTag(effectTag) == NULL)
	//		{
	//			//pBodyEffect = CCLegendAnimation::create("images/lightning_body.anm");
	//			//pBodyEffect->setPlayLoop(true);
	//			//pBodyEffect->setReleaseWhenStop(false);   // only play once, then release
	//			//pBodyEffect->setScaleY(1.5f);
	//			//pBodyEffect->setOpacity(200);
	//			//pBodyEffect->setTag(effectTag);
	//			//general->addEffect(pBodyEffect, false, 30);

	//			CCSprite* shadow = CCSprite::create("images/light_1.png");
	//			shadow->setAnchorPoint(ccp(0.5f, 0.5f));
	//			shadow->setTag(effectTag);
	//			shadow->setScaleX(1.5f);
	//			shadow->setScaleY(4.5f);
	//			//shadow->setRotation(-60.f);
	//			if(this->getRoleId() == 18)
	//				//shadow->setColor(ccc3(122, 246, 122));
	//				shadow->setColor(ccc3(122, 122, 246));
	//			else
	//				shadow->setColor(ccc3(246, 122, 122));
	//			//shadow->setPosition(ccp(0, 40));
	//			general->addEffect(shadow, true, 40);
	//		}
	//	}
	//}
}
void BasePlayer::onRemoveMusouEffect()
{
	CCNode* actorLayer = this->getGameScene()->getActorLayer();
	CCNode* node = actorLayer->getChildByTag((int)this->getRoleId());
	if(node != NULL)
		node->removeFromParent();

	int effectTag = BaseFighter::kTagMosouEffect + this->getRoleId();
	CCNode* container = this->getChildByTag(BaseFighter::kTagFootEffectContainer);
	CCNode* effect = container->getChildByTag(effectTag);
	if(effect != NULL) {
		effect->removeFromParent();
	}

	//for(unsigned int i = 0; i < m_AliveGenerals.size(); i++)
	//{
	//	General* general = dynamic_cast<General*>(this->getGameScene()->getActor(m_AliveGenerals.at(i)));
	//	if(general != NULL)
	//	{
	//		effectTag = kTagMosouEffect+general->getRoleId();
	//		container = general->getChildByTag(BaseFighter::kTagFootEffectContainer);
	//		effect = container->getChildByTag(effectTag);
	//		if(effect != NULL)
	//			effect->removeFromParent();
	//	}
	//}
}
void BasePlayer::updateMusouState()
{
	CCNode* actorLayer = this->getGameScene()->getActorLayer();
	LinkSpecialEffect* effect = dynamic_cast<LinkSpecialEffect*>(actorLayer->getChildByTag((int)this->getRoleId()));
	if(effect == NULL)
		return;

	// 无武将，返回
	if(m_AliveGenerals.size() <= 0)
	{
		effect->setLinkNumber(0);
		return;
	}

	General* general0 = dynamic_cast<General*>(this->getGameScene()->getActor(m_AliveGenerals.at(0)));
	General* general1 = NULL;
	if(m_AliveGenerals.size() > 1)
		general1 = dynamic_cast<General*>(this->getGameScene()->getActor(m_AliveGenerals.at(1)));

	std::vector<General*> generals;
	// check the distance from MyPlayer
	if(general0 != NULL && ccpDistance(general0->getWorldPosition(), this->getWorldPosition()) <= 288)
	{
		generals.push_back(general0);
	}
	if(general1 != NULL && ccpDistance(general1->getWorldPosition(), this->getWorldPosition()) <= 288)
	{
		generals.push_back(general1);
	}

	// refresh the musou special effect
	// update the number
	int num = generals.size();
	if(num == 2)
		num = 3;
	effect->setLinkNumber(num);

	// update each link
	std::vector<CCPoint> pointList;
	// component 1
	if(general0 != NULL)
	{
		pointList.push_back(ccp(this->getPositionX(), this->getPositionY()));
		pointList.push_back(ccp(general0->getPositionX(), general0->getPositionY()));
	}

	// component 2
	if(general0 != NULL && general1 != NULL)
	{
		pointList.push_back(ccp(general0->getPositionX(), general0->getPositionY()));
		pointList.push_back(ccp(general1->getPositionX(), general1->getPositionY()));
	}

	// component 3
	if(general1 != NULL)
	{
		pointList.push_back(ccp(general1->getPositionX(), general1->getPositionY()));
		pointList.push_back(ccp(this->getPositionX(), this->getPositionY()));
	}

	effect->updateLinkPosition(pointList);
}

void BasePlayer::updateStrategyEffect()
{
	int activeId = this->getActiveStrategyId();
	if(activeId == -1)
		return;

	if(GameUtils::millisecondNow() - m_stragetyEffectTimer > 30000)   // 30 seconds
	{
		ActorUtils::addStrategyEffect(this, activeId);

		std::vector<long long>& generals = this->getAliveGenerals();
		for(unsigned int i = 0; i < generals.size(); i++)
		{
			BaseFighter* pActor = dynamic_cast<BaseFighter*>(this->getGameScene()->getActor(generals.at(i)));
			if(pActor != NULL)
			{
				ActorUtils::addStrategyEffect(pActor, activeId);
			}
		}

		m_stragetyEffectTimer = GameUtils::millisecondNow();
	}
}
void BasePlayer::removeStrategyEffect()
{
	ActorUtils::removeStrategyEffect(this);

	std::vector<long long>& generals = this->getAliveGenerals();
	for(unsigned int i = 0; i < generals.size(); i++)
	{
		BaseFighter* pActor = dynamic_cast<BaseFighter*>(this->getGameScene()->getActor(generals.at(i)));
		if(pActor != NULL)
		{
			ActorUtils::removeStrategyEffect(pActor);
		}
	}
}
void BasePlayer::setActiveStrategyId(int id)
{
	// remove old strategy effect
	if(m_activeStrategyId != id)
	{
		removeStrategyEffect();
	}

	m_activeStrategyId = id;
}
int BasePlayer::getActiveStrategyId()
{
	return m_activeStrategyId;
}

std::string BasePlayer::getSmallHeadPathByProfession( int profession )
{
	std::string headPath = "res_ui/avatar47x58/";
	if(profession == PROFESSION_MJ_INDEX)
	{
		headPath.append("nanmengjiang.png");
	}
	else if (profession == PROFESSION_GM_INDEX)
	{
		headPath.append("nvguimou.png");
	}
	else if (profession == PROFESSION_HJ_INDEX)
	{
		headPath.append("nvhaojie.png");
	}
	else if (profession == PROFESSION_SS_INDEX)
	{
		headPath.append("nanshenshe.png");
	}

	return headPath;
}

std::string BasePlayer::getHeadPathByProfession( int profession )
{
	std::string headPath = "res_ui/protangonis56x55/";
	if(profession == PROFESSION_MJ_INDEX)
	{
		headPath.append("mengjiangNAN.png");
	}
	else if (profession == PROFESSION_GM_INDEX)
	{
		headPath.append("guimouNV.png");
	}
	else if (profession == PROFESSION_HJ_INDEX)
	{
		headPath.append("haojieNV.png");
	}
	else if (profession == PROFESSION_SS_INDEX)
	{
		headPath.append("shensheNAN.png");
	}

	return headPath;
}

std::string  BasePlayer::getBigHeadPathByProfession( int profession )
{
	std::string headPath = "res_ui/protagonist/";
	if(profession == PROFESSION_MJ_INDEX)
	{
		headPath.append("mengjiangNAN.png");
	}
	else if (profession == PROFESSION_GM_INDEX)
	{
		headPath.append("guimouNV.png");
	}
	else if (profession == PROFESSION_HJ_INDEX)
	{
		headPath.append("haojieNV.png");
	}
	else if (profession == PROFESSION_SS_INDEX)
	{
		headPath.append("shensheNAN.png");
	}
	return headPath;
}

std::string  BasePlayer::getHalfBodyPathByProfession( int profession )
{
	std::string headPath = "res_ui/protagonist_hero/";
	if(profession == PROFESSION_MJ_INDEX)
	{
		headPath.append("mengjiangNAN_hero.png");
	}
	else if (profession == PROFESSION_GM_INDEX)
	{
		headPath.append("guimouNV_hero.png");
	}
	else if (profession == PROFESSION_HJ_INDEX)
	{
		headPath.append("haojieNV_hero.png");
	}
	else if (profession == PROFESSION_SS_INDEX)
	{
		headPath.append("shensheNAN_hero.png");
	}
	return headPath;
}

int BasePlayer::getColorByProfession(int profession)
{
	int color = BASEPLAYER_PROFESSION_MJ_COLOR;
	if(profession == PROFESSION_MJ_INDEX)
	{
		color = BASEPLAYER_PROFESSION_MJ_COLOR;
	}
	else if (profession == PROFESSION_GM_INDEX)
	{
		color = BASEPLAYER_PROFESSION_GM_COLOR;
	}
	else if (profession == PROFESSION_HJ_INDEX)
	{
		color = BASEPLAYER_PROFESSION_HJ_COLOR;
	}
	else if (profession == PROFESSION_SS_INDEX)
	{
		color = BASEPLAYER_PROFESSION_SS_COLOR;
	}
	return color;
}

void BasePlayer::addCountryIcon(int countryId)
{
	CCNode* pIconNode = this->getChildByTag(PLAYER_COUNTRY_ICON_TAG);
	if(pIconNode != NULL)
		pIconNode->removeFromParent();

	if(countryId <= 0 && countryId > CMapInfo::country_max)
		return;

	// create the icon
	std::string countryIdName = "country_";
	char id[2];
	sprintf(id, "%d", countryId);
	countryIdName.append(id);

	std::string iconPathName = "res_ui/country_icon/";
	iconPathName.append(countryIdName);
	iconPathName.append(".png");

	CCSprite* pIconSprite = CCSprite::create(iconPathName.c_str());
	if (pIconSprite)
	{
		pIconSprite->setAnchorPoint(ccp(0.5f, 0.5f));

		// get the actor's name position, and the set the rank's position
		CCNode* pActorName = this->getChildByTag(GameActor::kTagActorName);
		// clac y
		float y = pActorName->getPositionY();
		// calc x
		float x = pActorName->getPositionX() - pActorName->getContentSize().width/2 - pIconSprite->getContentSize().width/2;
		pIconSprite->setPosition(ccp(x, y));

		pIconSprite->setTag(PLAYER_COUNTRY_ICON_TAG);
		this->addChild(pIconSprite);
	}
}

void BasePlayer::showActorName(bool bShow)
{
	std::string namestr = getActorName();
	const char* name = namestr.c_str();

	CCLabelTTF* nameLabel = (CCLabelTTF*)this->getChildByTag(GameActor::kTagActorName);
	if(bShow)
	{
		if(nameLabel == NULL)
		{
			//// 玩家名字前，附上国家简称
			//int countryId = this->getActiveRole()->playerbaseinfo().country();
			//std::string full_name = CMapInfo::getCountrySimpleName(countryId);
			//full_name.append(StringDataManager::getString("country_separator"));
			//full_name.append(name);
			//nameLabel = this->createNameLabel(full_name.c_str());
			nameLabel = this->createNameLabel(name);
			this->addChild(nameLabel, GameActor::ACTOR_NAME_ZORDER);
		}
		else
		{
			nameLabel->setString(name);
		}
		nameLabel->setPosition(ccp(0, BASEFIGHTER_ROLE_HEIGHT));

		// add country icon
		int countryId = this->getActiveRole()->playerbaseinfo().country();
		this->addCountryIcon(countryId);

		// add family name
		this->addFamilyName(this->getActiveRole()->playerbaseinfo().factionname().c_str(),
			this->getActiveRole()->playerbaseinfo().factionposition());

		// add vip flag
		addVIPFlag(this->getActiveRole()->playerbaseinfo().viplevel());
	}
	else
	{
		if(nameLabel != NULL)
			nameLabel->removeFromParent();

		CCSprite* pCountryIcon = (CCSprite*)this->getChildByTag(PLAYER_COUNTRY_ICON_TAG);
		if(pCountryIcon != NULL)
			pCountryIcon->removeFromParent();

		CCNode* pFamilyNameNode =this->getChildByTag(PLAYER_FAMILY_NAME_TAG);
		if(pFamilyNameNode != NULL)
			pFamilyNameNode->removeFromParent();
	}
}

void BasePlayer::addFamilyName(const char* familyName, int faction_position)
{
	CCNode* pFamilyNameNode = this->getChildByTag(PLAYER_FAMILY_NAME_TAG);
	if(pFamilyNameNode != NULL)
		pFamilyNameNode->removeFromParent();

	// not in a family
	if(strcmp(familyName, "") == 0)
		return;

	std::string wholeString = familyName;
	wholeString = StrUtils::applyColor(wholeString.c_str(), ccc3(255, 201, 14));   // (18, 235, 235)

	const char *faction_position_name;
	switch(faction_position)
	{
	case CGuildBase::position_master:
		faction_position_name = StringDataManager::getString("family_member_Master");
		break;
	case CGuildBase::position_second_master:
		faction_position_name = StringDataManager::getString("family_member_SecondMaster");
		break;
	}

	if(faction_position != CGuildBase::position_member && faction_position != CGuildBase::position_none)
	{
		//wholeString.append("/");
		std::string separatorStr = StringDataManager::getString("country_separator");
		separatorStr = StrUtils::applyColor(separatorStr.c_str(), ccc3(163, 73, 164));
		wholeString.append(separatorStr);
		std::string faction_pos_str = StrUtils::applyColor(faction_position_name, ccc3(163, 73, 164));
		wholeString.append(faction_pos_str);
	}
	//wholeString = StrUtils::applyColor(wholeString.c_str(), ccc3(18, 235, 235));

	CCRichLabel* pFamilyNameLabel = CCRichLabel::createWithString(wholeString.c_str(), CCSizeMake(256,64),NULL,NULL);
	pFamilyNameLabel->setAnchorPoint(ccp(0.5f, 0.5f));

	// get the actor's name position, and the set the rank's position
	CCNode* pActorName = this->getChildByTag(GameActor::kTagActorName);
	// clac y
	float y = pActorName->getPositionY() + pFamilyNameLabel->getContentSize().height;
	// calc x
	float x = pActorName->getPositionX();
	pFamilyNameLabel->setPosition(ccp(x, y));

	pFamilyNameLabel->setTag(PLAYER_FAMILY_NAME_TAG);
	this->addChild(pFamilyNameLabel, ACTOR_NAME_ZORDER);
}

void BasePlayer::addVIPFlag(int vipLevel)
{
	CCNodeRGBA* pVipNode = (CCNodeRGBA*)this->getChildByTag(PLAYER_VIP_FLAG_TAG);
	if(pVipNode != NULL)
		pVipNode->removeFromParent();

	// not a vip
	if(vipLevel <= 0)
		return;

	pVipNode = CCNodeRGBA::create();
	pVipNode->setCascadeOpacityEnabled(true);

	CCSprite* pVipFlagSprite = CCSprite::create("gamescene_state/zhujiemian3/zhujuetouxiang/vip1.png");
	pVipFlagSprite->setAnchorPoint(ccp(0.f, 0.5f));
	pVipNode->addChild(pVipFlagSprite);

	char level[10];
	sprintf(level,"%d", vipLevel);
	CCLabelBMFont* pVipLevelLabel = CCLabelBMFont::create(level, "res_ui/font/ziti_3.fnt");
	pVipLevelLabel->setAnchorPoint(ccp(0.f, 0.5f));
	pVipLevelLabel->setScale(1.0f);
	pVipLevelLabel->setPositionX(pVipFlagSprite->getContentSize().width);
	pVipLevelLabel->setPositionY(-5);   // offset
	pVipNode->addChild(pVipLevelLabel);

	// get the actor's name position, and the set the rank's position
	CCNode* pActorName = this->getChildByTag(GameActor::kTagActorName);
	// clac y
	float y = pActorName->getPositionY();
	// calc x
	float x = pActorName->getPositionX() + pActorName->getContentSize().width/2;
	pVipNode->setPosition(ccp(x, y));

	pVipNode->setTag(PLAYER_VIP_FLAG_TAG);
	this->addChild(pVipNode, ACTOR_NAME_ZORDER);
}

int BasePlayer::getWeaponImgIdx(CCLegendAnimation* anim)
{
	if(anim == NULL)
		return -1;

	int imgCount = anim->getAnimationData()->getImgNum();
	for(int i = 0; i < imgCount; i++)
	{
		std::string fileName = anim->getAnimationData()->getImageFileName(i);
		int index = fileName.find("weapon/");   // 图片文件名包含weapon，则表示其是武器图片
		if(index >= 0)
			return i;
	}

	return -1;
}

void BasePlayer::switchWeapon(const char* weaponName, GameActorAnimation* pActorAnim, int profession)
{
	if(pActorAnim == NULL)
		return;

	std::string weaponFigure = weaponName;
	// 在该游戏里，即使卸下了武器，手中还是会拿着一把"默认武器"
	if(strcmp(weaponName, "") == 0)
	{
		weaponFigure = BasePlayer::getDefaultWeaponFigure(profession);
	}

	//int imgIdx = PLAYER_ANIM_WEAPON_IMG_IDX;
	for(int i = 0; i < pActorAnim->getComponentSize(); i++)
	{
		for(int j = 0; j < pActorAnim->getAnimSize(); j++)
		{
			CCLegendAnimation* anim = pActorAnim->getAnim(i, j);
			if(anim == NULL)
				continue;

			int imgIdx = BasePlayer::getWeaponImgIdx(anim);

			// 死亡动画没有武器图片
			if(j >= ANI_DEATH)
				continue;

			// 切换纹理
			if(imgIdx < 0)
				continue;

			std::string textureFile = anim->getAnimationData()->getImagePath(imgIdx);
			textureFile.append(weaponFigure);
			textureFile.append(".png");
			
			CCTexture2D* texture = CCTextureCache::sharedTextureCache()->addImage(textureFile.c_str());
			anim->setTexture(imgIdx, texture);

			anim->setImageFileName(imgIdx, textureFile);
		}
	}
}

void BasePlayer::addWeaponEffect(const char* effectName)
{
	if(strcmp(effectName, "") == 0)
		return;

	if(!hasWeaponEffect(effectName))
		m_weaponEffects.push_back(effectName);

	attachWeaponEffect(effectName);
}
bool BasePlayer::hasWeaponEffect(const char* effectName)
{
	for (std::vector<std::string>::iterator it = m_weaponEffects.begin(); it != m_weaponEffects.end(); ++it) 
	{
		if((*it) == effectName)
			return true;
	}
	return false;
}
void BasePlayer::removeWeaponEffect(const char* effectName)
{
	CCAssert(false, "this function has not been implemented");
}
void BasePlayer::removeAllWeaponEffects()
{
	m_weaponEffects.clear();

	// remove animation
	int animName = this->getAnimName();
	CCLegendAnimation* pAnim = m_pAnim->getAnim(ANI_COMPONENT_BODY, animName);
	if(pAnim != NULL)
	{
		WeaponEffect::removeAllWeaponEffects(pAnim);
	}
}

void BasePlayer::attachWeaponEffect(const char* effectName)
{
	int animName = this->getAnimName();
	CCLegendAnimation* pAnim = m_pAnim->getAnim(ANI_COMPONENT_BODY, animName);
	if(pAnim == NULL)
		return;

	if(strcmp(effectName, "") == 0)
		return;

	WeaponEffectDefine effectDefine;
	effectDefine.weaponEffectType = WeaponEffect::getWeaponEffectType(effectName);
	effectDefine.weaponEffectFileName = effectName;
	int weaponType = BasePlayer::getWeaponType(getProfession());
	WeaponEffect::attach(effectDefine, weaponType, pAnim);
}

bool BasePlayer::hasAttachedWeaponEffect(const char* effectName)
{
	int animName = this->getAnimName();
	CCLegendAnimation* pAnim = m_pAnim->getAnim(ANI_COMPONENT_BODY, animName);
	if(pAnim == NULL)
		return false;

	return WeaponEffect::hasWeaponEffect(effectName, pAnim);
}
void BasePlayer::deattachWeaponEffect(const char* effectName)
{
	CCAssert(false, "this function has not been implemented");
}

int BasePlayer::getWeaponType(int profession)
{
	switch(profession)
	{
	case PROFESSION_MJ_INDEX:
		return GAME_WEAPON_TYPE_LANCE;
		break;
	case PROFESSION_GM_INDEX:
		return GAME_WEAPON_TYPE_FAN;
		break;
	case PROFESSION_HJ_INDEX:
	return GAME_WEAPON_TYPE_KNIFE;
		break;
	case PROFESSION_SS_INDEX:
		return GAME_WEAPON_TYPE_BOW;
		break;
	}

	CCAssert(false, "wrong path");
	return -1;
}

void BasePlayer::initWeaponEffect()
{
}

void BasePlayer::refreshWeaponEffect(float delayTime)
{
	CCFiniteTimeAction*  action = CCSequence::create(
		CCDelayTime::create(delayTime),
		CCCallFunc::create(this,callfunc_selector(BasePlayer::initWeaponEffect)),
		NULL);
	this->runAction(action);
}

void BasePlayer::setPKStatus(bool bValue)
{
	//if(bValue)
	//	CCLOG("start pk status");
	//else
	//	CCLOG("end pk status");
	m_bIsPkStatus = bValue;
}
bool BasePlayer::isPKStatus()
{
	return m_bIsPkStatus;
}
void BasePlayer::clearPKStatus(float dt)
{
	//CCLOG("clear pk status");
	m_bIsPkStatus = false;
}

void BasePlayer::onDamaged(BaseFighter* source, int damageNum, bool bDoubleAttacked, CSkillResult* skillResult)
{
	BaseFighter::onDamaged(source, damageNum, bDoubleAttacked, skillResult);

	// update pk status
	if(source != NULL)
	{
		BasePlayer* pSourcePlayer = dynamic_cast<BasePlayer*>(source);
		if(pSourcePlayer != NULL)
		{
			this->setPKStatus(true);
			this->unschedule(schedule_selector(BasePlayer::clearPKStatus));
			this->schedule( schedule_selector(BasePlayer::clearPKStatus), 1, 1, 45.f);    // seconds

			pSourcePlayer->setPKStatus(true);
			pSourcePlayer->unschedule(schedule_selector(BasePlayer::clearPKStatus));
			pSourcePlayer->schedule( schedule_selector(BasePlayer::clearPKStatus), 1, 1, 45.f);
		}
	}
}

std::string BasePlayer::getProfessionIconByStr( std::string professionStr )
{
	std::string profession_icon = "res_ui/renwubeibao/";
	if (strcmp(PROFESSION_MJ_NAME,professionStr.c_str()) == 0)
	{
		profession_icon.append("mj");
	}
	if (strcmp(PROFESSION_GM_NAME,professionStr.c_str()) == 0)
	{
		profession_icon.append("gm");
	}
	if (strcmp(PROFESSION_HJ_NAME,professionStr.c_str()) == 0)
	{
		profession_icon.append("hj");
	}
	if (strcmp(PROFESSION_SS_NAME,professionStr.c_str()) == 0)
	{
		profession_icon.append("ss");
	}

	profession_icon.append(".png");
	return profession_icon;
}

std::string BasePlayer::getProfessionIconByIndex( int professionIndex )
{
	std::string profession_path_str = "";

	std::string profession_icon = BasePlayer::getProfessionNameIdxByIndex(professionIndex);
	std::string profession_path = BasePlayer::getProfessionIconByStr(profession_icon);
	profession_path_str.append(profession_path);
	return profession_path_str;
}

int BasePlayer::getRoleSexByProfessionId( int professionId )
{
	int sexId = 1;
	switch(professionId)
	{
	case PROFESSION_MJ_INDEX:
		{
			sexId = ROLE_SEX_MEN;
		}break;
	case PROFESSION_GM_INDEX:
		{
			sexId = ROLE_SEX_WOMEMEN;
		}break;
	case PROFESSION_HJ_INDEX:
		{
			sexId = ROLE_SEX_WOMEMEN;
		}break;
	case PROFESSION_SS_INDEX:
		{
			sexId = ROLE_SEX_MEN;
		}break;
	}
	return sexId;
}
