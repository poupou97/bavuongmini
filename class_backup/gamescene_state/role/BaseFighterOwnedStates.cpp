#include "BaseFighterOwnedStates.h"

#include "BaseFighter.h"
#include "../GameSceneState.h"
#include "../../utils/GameUtils.h"
#include "SimpleActor.h"
#include "GameActorAnimation.h"
#include "../../legend_engine/LegendAAnimation.h"

ActionContext::ActionContext()
:finished(false)
{
}
ActionContext::~ActionContext()
{
}

void ActionContext::setData(ActionContext* dest, float startTime, bool finished)
{
    dest->startTime = startTime;
    dest->finished = finished;
}

/////////////////////////////////////////////////////

ActionChargeContext::ActionChargeContext()
{
}

ActionChargeContext::~ActionChargeContext()
{
}

void ActionChargeContext::setContext(BaseFighter* self)
{
	CCAssert(self != NULL, "should not be nil");

	ActionChargeContext* context = (ActionChargeContext*)self->getActionContext(ACT_CHARGE);
	context->chargeMoveSpeed = chargeMoveSpeed;
	context->chargetTargetPosition = chargetTargetPosition;
	context->chargeTime = chargeTime;

    setData(context, startTime, finished);
}

/////////////////////////////////////////////////////

void ActionAttackContext::setContext(BaseFighter* self)
{
	CCAssert(self != NULL, "should not be nil");

	ActionAttackContext* context = (ActionAttackContext*)self->getActionContext(ACT_ATTACK);
	context->currentActionIndex = currentActionIndex;
    
    setData(context, startTime, finished);
}

/////////////////////////////////////////////////////

ActionDeathContext::ActionDeathContext()
:bAddDustEffect(false)
,deathState(death_state_wait)
{
}

ActionDeathContext::~ActionDeathContext()
{
}

void ActionDeathContext::setContext(BaseFighter* self)
{
	CCAssert(self != NULL, "should not be nil");

	ActionDeathContext* context = (ActionDeathContext*)self->getActionContext(ACT_DIE);
	context->deathType = deathType;
	context->attackerRoleId = attackerRoleId;
	context->toGroundTime = toGroundTime;
	context->totalTime = totalTime;
    
    setData(context, startTime, finished);
}

/////////////////////////////////////////////////////

void ActionStandContext::setContext(BaseFighter* self)
{
	CCAssert(self != NULL, "should not be nil");

	ActionStandContext* context = (ActionStandContext*)self->getActionContext(ACT_STAND);
	context->idleStartTime = idleStartTime;
    
    setData(context, startTime, finished);
}

/////////////////////////////////////////////////////

ActionBeKnockedBackContext::ActionBeKnockedBackContext()
:bReturnBack(false)
{
}

ActionBeKnockedBackContext::~ActionBeKnockedBackContext()
{
}

void ActionBeKnockedBackContext::setContext(BaseFighter* self)
{
	CCAssert(self != NULL, "should not be nil");

	ActionBeKnockedBackContext* context = (ActionBeKnockedBackContext*)self->getActionContext(ACT_BE_KNOCKBACK);
	context->beKnockedBackSpeed = beKnockedBackSpeed;
	context->beKnockedBackDuration = beKnockedBackDuration;
	context->targetPosition = targetPosition;
	context->bReturnBack = bReturnBack;
    
    setData(context, startTime, finished);
}

/////////////////////////////////////////////////////

ActionJumpContext::ActionJumpContext()
: jumpDirection(0)
, jumpHeight(30)
, shadowAlphaStep(30)
{
	shadowColor = ccc3(255, 255, 255); // default color is white
}

ActionJumpContext::~ActionJumpContext()
{
}

void ActionJumpContext::setContext(BaseFighter* self)
{
	CCAssert(self != NULL, "should not be nil");

	ActionJumpContext* context = (ActionJumpContext*)self->getActionContext(ACT_JUMP);
	context->targetPosition = targetPosition;
	context->jumpDuration = jumpDuration;
	context->jumpHeight = jumpHeight;
	context->jumpDirection = jumpDirection;
	context->shadowColor = shadowColor;
	context->shadowAlphaStep = shadowAlphaStep;

    setData(context, startTime, finished);
}

/////////////////////////////////////////////////////

ActionDisappearContext::ActionDisappearContext()
{
}

ActionDisappearContext::~ActionDisappearContext()
{
}

void ActionDisappearContext::setContext(BaseFighter* self)
{
	CCAssert(self != NULL, "should not be nil");

	ActionDisappearContext* context = (ActionDisappearContext*)self->getActionContext(ACT_DISAPPEAR);

    setData(context, startTime, finished);
}

///////////////////////////////////////////////////////////////////////////

int BaseFighterJump::jumpAnimId = -1;

void BaseFighterJump::Enter(BaseFighter* self)
{
	CCAssert(jumpAnimId != -1, "must setJumpAnimId()");

	self->changeAnimation(jumpAnimId, self->getAnimDir(), false, true);

	ActionJumpContext* context = (ActionJumpContext*)self->getActionContext(ACT_JUMP);

	// adjust the direction
	CCPoint targetWorldPosition = self->getGameScene()->convertToGameWorldSpace(context->targetPosition);
	CCPoint offset;
	if(context->jumpDirection == 0)   // back
	{
		offset = GameUtils::getDirection(targetWorldPosition, self->getWorldPosition());
	}
	else   // ahead
	{
		offset = GameUtils::getDirection(self->getWorldPosition(), targetWorldPosition);
	}
	int animDirection = self->getAnimDirection(ccp(0, 0), ccp(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);
	//CCAssert(animDirection != -1, "invalid anim dir");
	self->setAnimDir(animDirection);
	self->getAnim()->setAction(animDirection);

	self->runAction(CCMoveTo::create(context->jumpDuration, context->targetPosition));
	self->getAnim()->runAction(CCJumpBy::create(context->jumpDuration, CCPointZero, context->jumpHeight, 1));

	// ��ӰЧ��
	CCLegendAnimation* pBody = self->getAnim()->getAnim(ANI_COMPONENT_BODY, BaseFighterJump::jumpAnimId);
	std::string animFileName = pBody->getAnimationData()->getAnmFileName();

	GameSceneLayer* scene = self->getGameScene();
	for(int i = 1; i < 4; i++)
	{
		SimpleActor* pActor = new SimpleActor();
		pActor->setGameScene(scene);
		pActor->setWorldPosition(self->getWorldPosition());
		pActor->loadAnim(animFileName.c_str());
		scene->getActorLayer()->addChild(pActor, SCENE_ROLE_LAYER_BASE_ZORDER);
		pActor->release();

		int alpha = 255 - i * context->shadowAlphaStep;
		if(alpha < 0)
			alpha = 0;
		pActor->setOpacity(alpha);
		pActor->setColor(context->shadowColor);

		CCFiniteTimeAction* simpleActorAction = CCSequence::create(
			CCDelayTime::create(i * 0.05f),
			CCMoveTo::create(context->jumpDuration, context->targetPosition),
			CCRemoveSelf::create(),
			NULL);
		pActor->runAction(simpleActorAction);

		CCFiniteTimeAction* actorJumpAction = CCSequence::create(
			CCDelayTime::create(i * 0.05f),
			CCJumpBy::create(context->jumpDuration, CCPointZero, context->jumpHeight, 1),
			NULL);
		pActor->getLegendAnim()->runAction(actorJumpAction);
	}

	self->clearPath();
}

void BaseFighterJump::Execute(BaseFighter* self)
{
	ActionJumpContext* context = (ActionJumpContext*)self->getActionContext(ACT_JUMP);
	if(context->finished)
	{
		self->changeAction(ACT_STAND);
		return;
	}

	if(GameUtils::millisecondNow() -  context->startTime > context->jumpDuration * 1000)
	{
		// add dust effect
		CCParticleSystem* particleEffect = CCParticleSystemQuad::create("animation/texiao/particledesigner/dust1.plist");
		particleEffect->setPositionType(kCCPositionTypeGrouped);
		particleEffect->setPosition(ccp(0.5f,0.5f));
		particleEffect->setAnchorPoint(ccp(0.5f,0.5f));
		//particleEffect->setScaleY(0.7f);
		particleEffect->setStartSize(150.f);
		particleEffect->setScaleX(1.2f);
		particleEffect->setScaleY(0.84f);
		//particleEffect->setLife(1.5f);
		//particleEffect->setLifeVar(0);
		particleEffect->setAutoRemoveOnFinish(true);
		self->addChild(particleEffect, GameActor::ACTOR_SHADOW_ZORDER);

		context->finished = true;
	}
}

void BaseFighterJump::Exit(BaseFighter* self)
{
}

void BaseFighterJump::setJumpAnimId(int animId)
{
	jumpAnimId = animId;
}

///////////////////////////////////////////////////////////////////////////

#define DISAPPEAR_FADEOUT_TIME 1.5f

void BaseFighterDisappear::Enter(BaseFighter* self)
{
	ActionDisappearContext context;
	context.startTime = GameUtils::millisecondNow();
	context.setContext(self);

	CCFiniteTimeAction* action = CCFadeOut::create(DISAPPEAR_FADEOUT_TIME);
	self->runAction(action);
}

void BaseFighterDisappear::Execute(BaseFighter* self)
{
	ActionDisappearContext* context = (ActionDisappearContext*)self->getActionContext(ACT_DISAPPEAR);
	if(GameUtils::millisecondNow() -  context->startTime > DISAPPEAR_FADEOUT_TIME * 1000)
		self->setDestroy(true);
}

void BaseFighterDisappear::Exit(BaseFighter* self)
{
}
