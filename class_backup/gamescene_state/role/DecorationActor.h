#ifndef _LEGEND_GAMEACTOR_DECORATIONACTOR_H_
#define _LEGEND_GAMEACTOR_DECORATIONACTOR_H_

#include "GameActor.h"
#include "cocos2d.h"

class CCLegendAnimation;

/**
 * 装饰actor，即场景编辑(Scene Editor)中的"摆件"
 * @author zhaogang
 * @version 0.1.0
 */
class DecorationActor : public GameActor
{
public:
	DecorationActor();
	virtual ~DecorationActor();

	virtual void loadWithInfo(LevelActorInfo* levelActorInfo);

private:
	CCLegendAnimation* m_pAnim;
};

#endif