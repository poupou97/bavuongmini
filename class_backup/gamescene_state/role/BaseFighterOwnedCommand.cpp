#include "BaseFighterOwnedCommand.h"

#include "BaseFighter.h"
#include "../GameSceneState.h"
#include "../../utils/GameUtils.h"
#include "../skill/GameFightSkill.h"
#include "../exstatus/ExStatusType.h"

BaseFighterCommandAttack::BaseFighterCommandAttack()
: attackCount(0)
, lastAttackCount(0)
, attackMaxCount(1)   // default, attack once
, attackSpeed(1.0f)
, sendAttackRequest(true)
, targetId(NULL_ROLE_ID)
{
	setType(ActorCommand::type_attack);
}

BaseFighterCommandAttack::~BaseFighterCommandAttack()
{
}

bool BaseFighterCommandAttack::isSame(ActorCommand* otherCmd)
{
	if(otherCmd->getStatus() != this->getStatus())
		return false;

	BaseFighterCommandAttack* cmd = (BaseFighterCommandAttack*)otherCmd;
	if(cmd->skillId == this->skillId
		&& cmd->targetId == this->targetId)
		return true;

	return false;
}

bool BaseFighterCommandAttack::checkConditionAttackMaxCount(BaseFighter* mainActor)
{
	BaseFighter* self = mainActor;

	if(this->attackCount >= attackMaxCount)
	{
		self->changeAction(ACT_STAND);
		setStatus(ActorCommand::status_finished);

		return true;
	}
	else
	{
		// finished one attck, continue
		if(attackCount > lastAttackCount)
		{
			// attack again, until reach the max attack count
			self->changeAction(ACT_ATTACK);
			lastAttackCount = attackCount;
			this->sendAttackRequest = false;   // the request only be sent in the first attack action
		}
	}

	return false;
}

void BaseFighterCommandAttack::Execute(GameActor* mainActor)
{
	BaseFighter* self = (BaseFighter*)mainActor;

	// when to stop attacking ?
	// if actor is silent, BUT the actor can do moving
	if(self->hasExStatus(ExStatusType::taciturn))
	{
		if(self->isAction(ACT_ATTACK))
			self->changeAction(ACT_STAND);
		setStatus(ActorCommand::status_finished);
		return;
	}

	self->checkAttack();

	//BaseFighterCommandAttack* cmd = (BaseFighterCommandAttack*)self->getCommand();
	//cmd->canAttack = true;   // other player will enter attack state when recieve server message( Push1102 )
	//if(cmd->getStatus() == ActorCommand::status_finished)
	//{
	//	cmd->setDestroy(true);
	//	//self->setCommand(NULL);
	//}

	if(this->getStatus() == ActorCommand::status_new)
	{
		// other player will enter attack state when recieve server message( Push1202 )
		if(!self->isAttacking())
		{
			GameSceneLayer* scene = self->getGameScene();
			char targetScopeType = self->getGameFightSkill(this->skillId.c_str(), this->skillProcessorId.c_str())->getTargetScopeType();
			if(!GameFightSkill::isNoneLock(targetScopeType))
			{
				BaseFighter* bf = dynamic_cast<BaseFighter*>(scene->getActor(this->targetId));
				if(bf != NULL)
				{
					// 调整攻击方向
					GameSceneLayer* scene = self->getGameScene();
					int animDirection = self->getAnimDirection(scene->convertToCocos2DWorldSpace(self->getWorldPosition()), 
						scene->convertToCocos2DWorldSpace(bf->getWorldPosition()), DEFAULT_ANGLES_DIRECTION_DATA);
					self->setAnimDir(animDirection);
				}
			}
			self->changeAction(ACT_ATTACK);
		}

		setStatus(ActorCommand::status_doing);
	}
	else if(this->getStatus() == ActorCommand::status_doing)
	{
		// this attack command has been finished
		this->checkConditionAttackMaxCount(self);
	}
	else if(this->getStatus() == ActorCommand::status_finished)
	{
		//self->changeAction(ACT_STAND);
	}
}

//--------------------------------------------------------------------
BaseFighterCommandMove::BaseFighterCommandMove()
: method(method_searchpath)
, stopAtEndPoint(true)
{
	setType(ActorCommand::type_move);
}

BaseFighterCommandMove::~BaseFighterCommandMove()
{
}

ActorCommand* BaseFighterCommandMove::clone()
{
	BaseFighterCommandMove* cmd = new BaseFighterCommandMove();
	cmd->targetPosition = this->targetPosition;
	cmd->method = this->method;
	cmd->stopAtEndPoint = this->stopAtEndPoint;
	cmd->moveSpeed = this->moveSpeed;
	if(this->m_nextCmd != NULL)
		cmd->m_nextCmd = this->m_nextCmd->clone();

	return cmd;
}

void BaseFighterCommandMove::Execute(GameActor* mainActor)
{
	BaseFighter* self = (BaseFighter*)mainActor;

	//BaseFighterCommandMove* cmd = (BaseFighterCommandMove*)self->getCommand();
	BaseFighterCommandMove* cmd = this;

	// actor is immobilized, so stop moving
	if(self->hasExStatus(ExStatusType::immobilize))
	{
		self->changeAction(ACT_STAND);
		cmd->setStatus(ActorCommand::status_finished);
		return;
	}

	if(cmd->getStatus() == ActorCommand::status_new)
	{
		if(cmd->method == BaseFighterCommandMove::method_direct)
		{
			self->clearPath();
			CCPoint* target = new CCPoint(cmd->targetPosition);
			self->getPaths()->push_back(target);
		}
		else
		{
			self->searchPath(cmd->targetPosition);
		}

		if(self->getPaths()->size() > 0)
		{
			self->changeAction(ACT_RUN); // to stand action
			cmd->setStatus(ActorCommand::status_doing);
		}
		else
		{
			if(cmd->stopAtEndPoint)
				self->changeAction(ACT_STAND); // to stand action
			cmd->setStatus(ActorCommand::status_finished);
		}
	}
	else if(cmd->getStatus() == ActorCommand::status_doing)
	{
		if (self->getPaths()->size() <= 0)
		{
			if(stopAtEndPoint)
			{
				self->changeAction(ACT_STAND);   // to stand action
				cmd->setStatus(ActorCommand::status_finished);
				return;
			}
			else
			{
				cmd->setStatus(ActorCommand::status_waiting);
			}
		}
	}
	else if(cmd->getStatus() == ActorCommand::status_waiting)
	{
		// continue moving, until got next command

		CCPoint offset = ccpMult(self->getMoveDir(), 64);
		float targetX = self->getWorldPosition().x + offset.x;
		float targetY = self->getWorldPosition().y - offset.y;

		self->clearPath();
		CCPoint* target = new CCPoint(targetX, targetY);
		self->getPaths()->push_back(target);
	}
}

void BaseFighterCommandMove::Exit(GameActor* mainActor)
{
	BaseFighter* self = (BaseFighter*)mainActor;

	self->clearPath();
}


//--------------------------------------------------------------------
BaseFighterCommandFollow::BaseFighterCommandFollow()
{
	setType(ActorCommand::type_follow);
}

BaseFighterCommandFollow::~BaseFighterCommandFollow()
{
}

void BaseFighterCommandFollow::Execute(GameActor* mainActor)
{
	BaseFighter* self = dynamic_cast<BaseFighter*>(mainActor);

	// 跟随状态，不要轻易改变状态，或许会引起问题
	// actor is immobilized, so stop moving
	//if(self->hasExStatus(ExStatusType::immobilize))
	//{
	//	self->changeAction(ACT_STAND);
	//	setStatus(ActorCommand::status_finished);
	//	return;
	//}

	GameSceneLayer* scene = self->getGameScene();
	BaseFighter* followingTarget = dynamic_cast<BaseFighter*>(scene->getActor(this->targetRoleId));
	if(followingTarget == NULL)
	{
		self->changeAction(ACT_STAND);
		setStatus(ActorCommand::status_finished);
		return;
	}

	if(getStatus() == ActorCommand::status_new)
	{
		setStatus(ActorCommand::status_doing);
	}
	else if(getStatus() == ActorCommand::status_doing)
	{
		float angle = ccpToAngle(followingTarget->getMoveDir());
		CCPoint curOffset = this->offset;
		curOffset = ccpRotateByAngle(offset, ccp(0,0), angle);
		float targetX = followingTarget->getWorldPosition().x + curOffset.x;
		float targetY = followingTarget->getWorldPosition().y - curOffset.y;

		CCPoint targetPosition = ccp(targetX, targetY);

		//if(followingTarget->isAction(ACT_RUN))    // target is running, follow him
		if(!targetPosition.equals(self->getWorldPosition()))
		{
			self->searchPath(ccp(targetX, targetY));

			if(self->getPaths()->size() > 0)
			{
				if(ccpDistance(targetPosition, self->getWorldPosition()) > TILE_SIZE * 2)
				{
					self->setMoveSpeed(300.f);
				}
				else
				{
					// normal speed
					self->setMoveSpeed(BASEFIGHTER_BASIC_MOVESPEED);
				}
				//self->setMoveSpeed(350.f);

				self->changeAction(ACT_RUN); // to run action
			}
			else
			{
				if(!self->isAction(ACT_STAND))
					self->changeAction(ACT_STAND); // to stand action
			}
		}
		else
		{
			if(!self->isAction(ACT_STAND))
				self->changeAction(ACT_STAND); // to stand action
		}
	}
	else if(getStatus() == ActorCommand::status_finished)
	{
	}
}

void BaseFighterCommandFollow::Exit(GameActor* mainActor)
{
	BaseFighter* self = dynamic_cast<BaseFighter*>(mainActor);

	self->clearPath();
}