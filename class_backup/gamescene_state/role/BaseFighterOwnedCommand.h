#ifndef _BASEFIGHTER_OWNED_COMMAND_H
#define _BASEFIGHTER_OWNED_COMMAND_H

#include "cocos2d.h"
#include "ActorCommand.h"

USING_NS_CC;

class BaseFighter;

/**
 * 攻击指令，需指定攻击目标和攻击方式
 * CommandAttack，并不是仅仅针对BaseFighterOwnedAttack状态 ( 比如MyPlayerAttack, MonsterAttack )。
 * CommandAttack可以利用多个Action来完成一次技能攻击。
 * @author zhaogang
 * @version 0.1.0
 */
class BaseFighterCommandAttack : public ActorCommand
{
public:
	BaseFighterCommandAttack();
	virtual ~BaseFighterCommandAttack();

	virtual void Execute(GameActor* mainActor);

	virtual bool isSame(ActorCommand* otherCmd);

	long long targetId;
	std::string skillId;
	std::string skillProcessorId;
	int attackCount;
	int attackMaxCount;   // default, attack once
	float attackSpeed;   // attack animation's playing speed, default, 1.0f, more bigger more quickier
	bool sendAttackRequest;   // defalut value is true, attacker will send Req1201 to game server

protected:
	bool checkConditionAttackMaxCount(BaseFighter* mainActor);

protected:
	int lastAttackCount;   // last attack count which is reported from BaseFighter

};

/**
 * 移动指令，需指定目的地
 * @author zhaogang
 * @version 0.1.0
 */
class BaseFighterCommandMove : public ActorCommand
{
public:
	enum Type
	{
		method_searchpath = 0,   // 寻路到目的地
		method_direct = 1   // 直接到目的地
	};

public:
	BaseFighterCommandMove();
	virtual ~BaseFighterCommandMove();

	virtual ActorCommand* clone();

	virtual void Execute(GameActor* mainActor);
	virtual void Exit(GameActor* mainActor);

	CCPoint targetPosition;
	int method;
	/**  if true, the actor will stop at the end point. if false, the actor will continue moving */
	bool stopAtEndPoint; 
	float moveSpeed;
};

/**
 * 跟随指令，需指定跟随角色
 * 跟随指令的offset，是指以跟随的角色（targetRoleId）朝向右方为矢量的offset
 * @author zhaogang
 * @version 0.1.0
 */
class BaseFighterCommandFollow : public ActorCommand
{
public:
	BaseFighterCommandFollow();
	virtual ~BaseFighterCommandFollow();

	virtual void Execute(GameActor* mainActor);
	virtual void Exit(GameActor* mainActor);

	long long targetRoleId;
	CCPoint offset;
};

#endif