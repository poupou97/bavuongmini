#include "SkillProcessor.h"

#include "../../role/BaseFighter.h"
#include "../../GameSceneState.h"
#include "../SkillSeed.h"
#include "../CSkillResult.h"
#include "../../../utils/GameUtils.h"
#include "../../../messageclient/GameMessageProcessor.h"
#include "../../../GameView.h"
#include "../../role/BaseFighterOwnedCommand.h"
#include "../../role/BaseFighterOwnedStates.h"
#include "../../role/MyPlayer.h"
#include "../../role/GameActorAnimation.h"
#include "../../role/SimpleActor.h"
#include "../../exstatus/ExStatusType.h"
#include "../../role/BaseFighterConstant.h"
#include "../../role/Monster.h"
#include "../../../messageclient/element/CMonsterBaseInfo.h"
	
SkillProcessor::SkillProcessor()
: m_bIsRelease(false)
, m_bIsHide(false)
, skillSeed(NULL)
, mSkillResult(NULL)
, m_bSendAttackReq(false)
, m_effectScaleX(1.0f)
, m_effectScaleY(1.0f)
{
	apEffects = CCArray::create();
}

SkillProcessor::~SkillProcessor()
{
	CC_SAFE_DELETE(skillSeed);
	CC_SAFE_DELETE(mSkillResult);
}

void SkillProcessor::init()
{
	BaseFighter* bf = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(this->getSkillSeed()->attacker));
	if(bf != NULL)
	{
		BaseFighterCommandAttack* cmd = dynamic_cast<BaseFighterCommandAttack*>(bf->getCommand());
		if(cmd != NULL) {
			setSendAttackReq(cmd->sendAttackRequest);
		}
	}
}
	
/**
* 播放技能释放时音效
*/
void SkillProcessor::playFightSound(){

}

void SkillProcessor::loadSkillEffect(const char* currEffect){

}

SkillSeed* SkillProcessor::getSkillSeed() {
	return skillSeed;
}
void SkillProcessor::setSkillSeed(SkillSeed* skillSeed) {
	this->skillSeed = skillSeed;
}

CSkillResult* SkillProcessor::getSkillResult() {
	return this->mSkillResult;
}
void SkillProcessor::setSkillResult(CSkillResult* skillResult) {
	this->mSkillResult = skillResult;
}
	
CCArray* SkillProcessor::getApEffects() {
	return apEffects;
}

void SkillProcessor::setApEffects(CCArray* apEffects) {
	this->apEffects = apEffects;
}
	
GameSceneLayer* SkillProcessor::getScene() {
	return scene;
}

void SkillProcessor::setScene(GameSceneLayer* scene) {
	this->scene = scene;
}

float SkillProcessor::getPosX() {
	return m_x;
}
void SkillProcessor::setPosX(float mX) {
	m_x = mX;
}
float SkillProcessor::getPosY() {
	return m_y;
}
void SkillProcessor::setPosY(float mY) {
	m_y = mY;
}

void SkillProcessor::setEffectScaleX(float sx)
{
	m_effectScaleX = sx;
}
void SkillProcessor::setEffectScaleY(float sy)
{
	m_effectScaleY = sy;
}
	
bool SkillProcessor::isHide(){
	return m_bIsHide;
}
	
void SkillProcessor::setHide(bool isHide) {
	this->m_bIsHide = isHide;
}
	
bool SkillProcessor::isRelease() {
	return m_bIsRelease;
}

void SkillProcessor::setRelease(bool isRelease) {
	this->m_bIsRelease = isRelease;
}
	
/**
	* 玩家自己发送协议给服务器和出招
	* @param attacker
	*/
void SkillProcessor::skillDamage(BaseFighter* attacker){
	sendSkillCommand(attacker);
	countDamage(attacker);
}
	
void SkillProcessor::sendSkillCommand(BaseFighter* attacker) {
	//if (attacker == null 
	//		|| LocalManager.getInstance().isLocal()
	//		|| !getScene().isOwn(attacker.roleId)) {
	//	return;
	//}
	//// 游戏联网并且攻击者是主角自己
	//sendAttackReq(attacker);
}

/**
	* 技能中使用 发送攻击技能到服务器并计算伤害
	*/
void SkillProcessor::countDamage(BaseFighter* attacker) {
	//if (attacker == null
	//		|| (!LocalManager.getInstance().isLocal() && !getScene().isOwn(attacker.roleId))) {
	//	return;
	//}
	//// 游戏不联网 | 游戏联网时并且是主角自己
	//doTrick(attacker);
}
	
void SkillProcessor::hideOutScreen(){
	//// 超出屏幕则隐藏
	//float tx = (getScene().getSceneX() + getM_x());
	//float ty = (getScene().getSceneY() + getM_y());
	//if (tx < 0 || tx > getScene().viewWidth || ty < 0
	//		|| ty > getScene().viewHeight) {
	//	setHide(true);
	//}
}

void SkillProcessor::onSkillBegan(BaseFighter* attacker)
{
	// nothing, subclass will implement it
}
void SkillProcessor::onSkillReleased(BaseFighter* attacker)
{
	// nothing, subclass will implement it
}
void SkillProcessor::onSkillApplied(BaseFighter* attacker)
{
	this->setRelease(true);
}

void SkillProcessor::update(float dt)
{
	// nothing
}

void SkillProcessor::updateResume(BaseFighter* attacker){
	// check the cooldown time
	bool bIsCooldownOver = true;

	// todo
	//if(GameView::getInstance()->isOwn(getSkillSeed()->attacker))
	//{
	//	GameFightSkill* mfs = *(getSkillSeed()->mfSkill);
	//	if(getSkillSeed()->mfSkill == NULL){
	//		setRelease(true);
	//		return;
	//	}

	//	// 得到技能的CD时间
	//	int coolTime = mfs->getCoolTime();
	//	// 还原
	//	if (mfs->getCoolTimeCount() != -1
	//		&& GameUtils::millisecondNow() - mfs->getCoolTimeCount() > coolTime) {
	//		//mfs.resume();
	//		mfs->setCoolTimeCount(-1);
	//		bIsCooldownOver = true;
	//	}
	//	bIsCooldownOver = false;
	//}

	// 释放&隐藏
	if(isHide() && bIsCooldownOver){
		setRelease(true);
	}
}

void SkillProcessor::sendAttackReq(BaseFighter* attacker){
	// only MyPlayer can send attack message to game server
	if(GameView::getInstance()->isOwn(getSkillSeed()->attacker))
	{
		if(m_bSendAttackReq)
		{
			GameMessageProcessor::sharedMsgProcessor()->sendReq(1201, skillSeed);
			MyPlayer* me = (MyPlayer*)attacker;
			me->startCommonCD();
		}
	}
}

bool SkillProcessor::knockback(BaseFighter* attacker, float backDuration, float hitRecoverTime)
{
	CSkillResult* result = getSkillResult();
	CCAssert(result != NULL, "skill result should not be nil");

	// no defender, ignore
	if(result->defenders_size() <= 0)
	{
		this->setRelease(true);   // release the skill processor immediatelly
		return false;
	}

	//CCAssert(result->defenders_size() == 1, "if not 1, maybe error");

	for (int i = 0; i < result->defenders_size(); i++) {
		Defender def = result->defenders(i);
		BaseFighter* defender = dynamic_cast<BaseFighter*>(attacker->getGameScene()->getActor(def.target()));
		if(defender == NULL)
		{
			this->setRelease(true);   // release the skill processor immediatelly
			return false;
		}
		//defender->setWorldPosition(ccp(def.targetx(), def.targety()));
		//defender->setPosition(attacker->getGameScene()->convertToCocos2DSpace(defender->getWorldPosition()));
		CCPoint targetWorldPosition = ccp(def.targetx(), def.targety());
		CCPoint target = attacker->getGameScene()->convertToCocos2DSpace(targetWorldPosition);
		CCFiniteTimeAction*  action = CCSequence::create(
			CCMoveTo::create(backDuration, target),
			CCDelayTime::create(hitRecoverTime),
			NULL);
		defender->runAction(action);

		CCPoint offset = GameUtils::getDirection(targetWorldPosition, attacker->getWorldPosition());
		int animDirection = defender->getAnimDirection(ccp(0, 0), ccp(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);
		//CCAssert(animDirection != -1, "invalid anim dir");
		defender->setAnimDir(animDirection);

		defender->changeAction(ACT_BE_KNOCKBACK);
	}

	return true;
}

bool SkillProcessor::knockback(BaseFighter* attacker, BaseFighter* defender, float knockDistance, float backDuration, float hitRecoverTime)
{
	if(defender == NULL)
	{
		return false;
	}

	if(defender->isAction(ACT_DIE))
		return false;

	if(!defender->isAction(ACT_STAND)  
		|| defender->hasExStatus(ExStatusType::immobilize)   // 被定身
		|| defender->hasExStatus(ExStatusType::vertigo)   // 眩晕状态
		|| (defender->getAnim()->getActionByTag(BASEFIGHTER_GROW_ACTION) != NULL)
		//|| isBoss(defender)
		|| defender->isMyPlayerGroup()
		|| defender->getType() == GameActor::type_player)   // 玩家角色不会被"假击退"
	{
		return false;
	}

	// the defender is too far from his real postion, so ignore
	if(ccpDistance(defender->getWorldPosition(), defender->getRealWorldPosition()) >= 128)
		return false;

	// calc the knockback offset
	CCPoint dir = GameUtils::getDirection(attacker->getWorldPosition(), defender->getWorldPosition());
	// check if the target is barrier, if true, try to modify the target to reachable point
	float distance = knockDistance;   // 64 * 2.5f
	CCPoint offset = ccpMult(dir, distance);
	CCPoint targetWorldPos = ccpAdd(defender->getWorldPosition(), offset);
	GameSceneLayer* scene = attacker->getGameScene();
	int tileX = scene->positionToTileX(targetWorldPos.x);
	int tileY = scene->positionToTileY(targetWorldPos.y);
	if(scene->isLimitOnGround(tileX, tileY))
	{
		// return directly, the modification will be implemented in the future
		return false;
	}
	offset.y /= 2;
	offset.y = -offset.y;

	CCPoint targetPos = scene->convertToCocos2DSpace(targetWorldPos);
	CCFiniteTimeAction*  action = CCSequence::create(
		CCMoveTo::create(backDuration, targetPos),
		//CCMoveBy::create(backDuration, offset),
		CCDelayTime::create(hitRecoverTime),
		NULL);
	defender->runAction(action);

	CCPoint cocos2dPosition = attacker->getGameScene()->convertToCocos2DSpace(attacker->getWorldPosition());
	CCPoint cocos2dTargetPos = ccpAdd(cocos2dPosition, offset);
	targetWorldPos = attacker->getGameScene()->convertToGameWorldSpace(cocos2dTargetPos);   // get the world position
	offset = GameUtils::getDirection(targetWorldPos, attacker->getWorldPosition());
	int animDirection = defender->getAnimDirection(ccp(0, 0), ccp(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);
	//CCAssert(animDirection != -1, "invalid anim dir");
	defender->setAnimDir(animDirection);

	// ActionContext should be set before changeAction()
	ActionBeKnockedBackContext context;
	context.beKnockedBackDuration = backDuration + hitRecoverTime;
	context.bReturnBack = true;
	context.startTime = GameUtils::millisecondNow();
	context.setContext(defender);

	defender->changeAction(ACT_BE_KNOCKBACK);

	return true;
}

bool SkillProcessor::hitback(BaseFighter* attacker, BaseFighter* defender, float knockDistance, float backDuration)
{
	if(defender == NULL)
	{
		return false;
	}

	if(defender->isAction(ACT_DIE))
		return false;

	if(defender->hasExStatus(ExStatusType::immobilize)   // 被定身
		|| defender->hasExStatus(ExStatusType::vertigo)   // 眩晕状态
		|| (defender->getAnim()->getActionByTag(BASEFIGHTER_GROW_ACTION) != NULL)
		|| (defender->getActionByTag(BASEFIGHTER_FLYUP_ACTION) != NULL)
		//|| isBoss(defender)
		|| defender->isMyPlayerGroup()
		|| defender->getType() == GameActor::type_player)   // 玩家角色不会被"假击退"
	{
		return false;
	}

	// the defender is too far from his real postion, so ignore
	if(ccpDistance(defender->getWorldPosition(), defender->getRealWorldPosition()) >= 192)
		return false;

	// calc the knockback offset
	CCPoint dir = GameUtils::getDirection(attacker->getWorldPosition(), defender->getWorldPosition());
	// check if the target is barrier, if true, try to modify the target to reachable point
	float distance = knockDistance;   // 64 * 2.5f
	CCPoint offset = ccpMult(dir, distance);
	CCPoint targetWorldPos = ccpAdd(defender->getWorldPosition(), offset);
	GameSceneLayer* scene = attacker->getGameScene();
	int tileX = scene->positionToTileX(targetWorldPos.x);
	int tileY = scene->positionToTileY(targetWorldPos.y);
	if(scene->isLimitOnGround(tileX, tileY))
	{
		// return directly, the modification will be implemented in the future
		return false;
	}
	offset.y /= 2;
	offset.y = -offset.y;

	const float idle_duration = 0.5f;
	if(defender->getAnimDir() == 1 || defender->getAnimDir() == 3)   // face to left or right
	{
		const float target_angle = 25.f;
		bool flyingToRight = true;
		CCPoint attackWorldPosition = attacker->getWorldPosition();
		// adjust the animation's direction and rotation by attacker and this monster's position
		if(attackWorldPosition.x >=  defender->getWorldPosition().x)
			flyingToRight = false;
		else
			flyingToRight = true;
		float targetAngle = flyingToRight ? target_angle : -target_angle ;
		int animDir = flyingToRight ? 1 : 3;
		defender->setAnimDir(animDir);
		if(defender->getAnim()->getRotation() != 0)
			targetAngle = 0;

		CCFiniteTimeAction* action1 = CCSequence::create(
				CCRotateBy::create(backDuration, targetAngle),
				CCDelayTime::create(0.2f),
				//CCRotateBy::create(0.0001f, -targetAngle),
				CCRotateTo::create(0.0001f, 0),
				CCDelayTime::create(backDuration + idle_duration - 0.2f),
				NULL);
		defender->getAnim()->runAction(action1);
	}

	CCPoint targetPos = scene->convertToCocos2DSpace(targetWorldPos);
	CCFiniteTimeAction*  action = CCSequence::create(
		CCMoveTo::create(backDuration, targetPos),
		//CCMoveBy::create(backDuration, offset),
		CCDelayTime::create(idle_duration),
		NULL);
	action->setTag(BASEFIGHTER_HITBACK_ACTION);
	defender->runAction(action);

	CCPoint cocos2dPosition = attacker->getGameScene()->convertToCocos2DSpace(attacker->getWorldPosition());
	CCPoint cocos2dTargetPos = ccpAdd(cocos2dPosition, offset);
	targetWorldPos = attacker->getGameScene()->convertToGameWorldSpace(cocos2dTargetPos);   // get the world position
	offset = GameUtils::getDirection(targetWorldPos, attacker->getWorldPosition());
	int animDirection = defender->getAnimDirection(ccp(0, 0), ccp(offset.x, -offset.y), DEFAULT_ANGLES_DIRECTION_DATA);
	//CCAssert(animDirection != -1, "invalid anim dir");
	defender->setAnimDir(animDirection);

	return true;
}

void SkillProcessor::quake(BaseFighter* actor, float duration)
{
	const float one_jump_time = 0.13f;
	int times = duration / one_jump_time;
	// jump effect
	if(!actor->isMyPlayerGroup() /**&& !actor->isDead()**/)
	{
		CCAction* action = CCJumpBy::create(duration, CCPointZero, 25, times);
		action->setTag(BASEFIGHTER_QUAKE_ACTION);
		actor->getAnim()->runAction(action);
	}
}

void SkillProcessor::shake(BaseFighter* actor, bool shakeLeftRight, int shakeTimes, float range)
{
	if(actor == NULL)
		return;

	// 附加受击晃动action
	int dir = actor->getAnimDir();
	CCPoint shakePoint1, shakePoint2;
	if(shakeLeftRight)
	{
		if(dir == ANIM_DIR_UP || dir == ANIM_DIR_DOWN)
		{
			shakePoint1.setPoint(range, 0);
			shakePoint2.setPoint(-range, 0);
		}
		else
		{
			shakePoint1.setPoint(0, range);
			shakePoint2.setPoint(0, -range);
		}
	}
	else
	{
		if(dir == ANIM_DIR_UP || dir == ANIM_DIR_DOWN)
		{
			shakePoint1.setPoint(0, range);
			shakePoint2.setPoint(0, -range);
		}
		else
		{
			shakePoint1.setPoint(range, 0);
			shakePoint2.setPoint(-range, 0);
		}
	}

	const float shakeDuration = 0.06f;
	CCFiniteTimeAction* action1 = CCRepeat::create(
		CCSequence::create(
		CCMoveBy::create(shakeDuration, shakePoint1),
		CCMoveBy::create(shakeDuration, shakePoint2),
		NULL), shakeTimes);
	const float extraTime = 0.15f;
	CCFiniteTimeAction* action2 = CCSequence::create(
			CCTintTo::create(0.001f, 255, 96, 96),
			CCDelayTime::create((shakeDuration+shakeDuration)*shakeTimes + extraTime),
			CCTintTo::create(0.001f, 255, 255, 255),
			NULL);

	CCAction* action = CCSpawn::create(action1, action2, NULL);
	action->setTag(BASEFIGHTER_SHAKE_ACTION);
	actor->getAnim()->runAction(action);
}

void SkillProcessor::flyup(BaseFighter* attacker, BaseFighter* defender)
{
	GameSceneLayer* scene = attacker->getGameScene();
	BaseFighter* target = defender;
	if(target != NULL)
	{
		if(isBoss(target))
			return;

		const float jumpDuration = 0.5f;
		const float jumpHeight = BASEFIGHTER_ROLE_HEIGHT * 2.5f;
		const float delayTime = 0.15f;

		// jump action
		CCFiniteTimeAction* action = CCSequence::create(
			CCDelayTime::create(delayTime),
			CCJumpBy::create(jumpDuration, CCPointZero, jumpHeight, 1),
			NULL);
		target->getAnim()->runAction(action);

		bool flyingToRight = true;
		CCPoint attackWorldPosition = attacker->getWorldPosition();
		// adjust the animation's direction and rotation by attacker and this monster's position
		if(attackWorldPosition.x >=  defender->getWorldPosition().x)
			flyingToRight = false;
		else
			flyingToRight = true;
		float targetAngle = flyingToRight ? 90.f : -90.f ;
		int animDir = flyingToRight ? 1 : 3;
		defender->setAnimDir(animDir);

		CCFiniteTimeAction* rotate_action = CCSequence::create(
			CCDelayTime::create(delayTime),
			CCRotateBy::create(jumpDuration*4/5, targetAngle),
			CCDelayTime::create(jumpDuration*1/5),
			CCDelayTime::create(0.25f),
			//CCRotateBy::create(0.0001f, -targetAngle),
			CCRotateTo::create(0.0001f, 0),
			NULL);
		target->getAnim()->runAction(rotate_action);

		// get flying offset
		CCPoint dir = GameUtils::getDirection(attacker->getWorldPosition(), target->getWorldPosition());
		// check if the target is barrier, if true, try to modify the target to reachable point
		float distance = 64;
		if(target->getType() == GameActor::type_player)
			distance = 0;
		CCPoint offset = ccpMult(dir, distance);
		CCPoint targetPos = ccpAdd(target->getWorldPosition(), offset);
		int tileX = scene->positionToTileX(targetPos.x);
		int tileY = scene->positionToTileY(targetPos.y);
		if(scene->isLimitOnGround(tileX, tileY))
		{
			offset.setPoint(0,0);
		}
		offset.y /= 2;
		offset.y = -offset.y;

		// move back action
		CCFiniteTimeAction* action2 = CCSequence::create(
			CCDelayTime::create(delayTime),
			CCMoveBy::create(jumpDuration, offset),
			CCScaleTo::create(0.06f, 1.0f, 0.8f),
			CCScaleTo::create(0.1f, 1.0f, 1.0f),
			CCDelayTime::create(0.2f),
			NULL);
		action2->setTag(BASEFIGHTER_FLYUP_ACTION);
		target->runAction(action2);
	}
}

void SkillProcessor::showWeaponEffect(BaseFighter* actor, const char* effectFileName, float stayTime)
{
	BasePlayer* pPlayer = dynamic_cast<BasePlayer*>(actor);
	if(pPlayer != NULL)
	{
		std::string wholeEffectFileName = "animation/weapon/weaponeffect/";
		wholeEffectFileName.append(effectFileName);

		BaseFighterCommandAttack* cmd = dynamic_cast<BaseFighterCommandAttack*>(pPlayer->getCommand());
		if(cmd != NULL && cmd->attackCount == 0)
		{
			// "animation/weapon/weaponeffect/WeaponEffectFire_blue.plist"
			//if(!pPlayer->hasWeaponEffect(effectFileName))
			{
				pPlayer->removeAllWeaponEffects();
				pPlayer->addWeaponEffect(wholeEffectFileName.c_str());
				pPlayer->refreshWeaponEffect(stayTime);
			}
		}
	}
}

void SkillProcessor::showGroundEffect(const char* effectName, GameSceneLayer* scene, float firstDelayTime, float secondDelayTime, float fadeOutTime)
{
	// show ground effect
	SimpleActor* simpleActor = new SimpleActor();
	simpleActor->setLayerId(0);   // ground layer
	simpleActor->setGameScene(scene);
	simpleActor->setWorldPosition(ccp(this->getPosX(), this->getPosY()));
	simpleActor->loadAnim(effectName, true);
	simpleActor->setVisible(false);

	CCFiniteTimeAction*  action = CCSequence::create(
		CCDelayTime::create(firstDelayTime),   // the ground effect start time in the "effectName".anm
		CCShow::create(),
		CCDelayTime::create(secondDelayTime),
		CCFadeOut::create(fadeOutTime),
		CCRemoveSelf::create(),
		NULL);
	simpleActor->runAction(action);

	CCNode* actorLayer = scene->getActorLayer();
	actorLayer->addChild(simpleActor);
	simpleActor->release();
}

void SkillProcessor::scatter(BaseFighter* attacker)
{
	GameSceneLayer* scene = attacker->getGameScene();

	// it is not My General
	if(attacker->getPosInGroup() <= 0)
		return;

	// if the my general is close to my group, adjust his position
	const int range = 96;   // 80
	bool isCloseToMyGroup = false;
	/*
	if(attacker->getPosInGroup() == 1)
	{
		// check other Generals
		std::vector<long long>& generals = GameView::getInstance()->myplayer->getAliveGenerals();
		for(unsigned int i = 0; i < generals.size(); i++)
		{
			BaseFighter* pActor = dynamic_cast<BaseFighter*>(scene->getActor(generals.at(i)));
			if(pActor == NULL)
				continue;
			if(pActor->getRoleId() == attacker->getRoleId())   // it's me
				continue;

			if(ccpDistance(pActor->getWorldPosition(), attacker->getWorldPosition()) < 64)
			{
				isCloseToMyGroup = true;
				break;
			}
		}
	}
	*/
	// check the MyPlayer
	if(ccpDistance(GameView::getInstance()->myplayer->getWorldPosition(), attacker->getWorldPosition()) < range)
	{
		isCloseToMyGroup = true;
	}

	if(isCloseToMyGroup)
	{
		// try to find the target which is not close to MyPlayer
		float random = CCRANDOM_0_1();
		float x, y;
		bool isCloseToMyPlayer = false;
		for(int i = 0; i < 1000; i++)
		{
			const float randomRange = 128;   // 96
			// find the near target
			random = CCRANDOM_0_1() - 0.5f;
			x = attacker->getRealWorldPosition().x + random * randomRange;
			//x = attacker->getWorldPosition().x + random * range;
			random = CCRANDOM_0_1() - 0.5f;
			y = attacker->getRealWorldPosition().y + random * randomRange;
			//y = attacker->getWorldPosition().y + random * range;

			// check the MyPlayer
			if(ccpDistance(GameView::getInstance()->myplayer->getWorldPosition(), ccp(x,y)) < range)
			{
				isCloseToMyPlayer = true;
				continue;
			}
			else
			{
				isCloseToMyPlayer = false;
				break;
			}
		}

		// setup the move command
		BaseFighterCommandMove* cmd = new BaseFighterCommandMove();
		cmd->targetPosition = ccp(x, y);
		attacker->setNextCommand(cmd, false);
		attacker->setMoveSpeed(200.f);
	}
}

void SkillProcessor::assemble(BaseFighter* attacker)
{
	// it is not My General
	if(attacker->getPosInGroup() <= 0)
		return;

	// if the my general is too far to MyPlayer, adjust his position
	const int long_range = 64 * 4;
	bool isFarToMyPlayer = false;
	// check the MyPlayer
	if(ccpDistance(GameView::getInstance()->myplayer->getWorldPosition(), attacker->getWorldPosition()) >= long_range)
	{
		isFarToMyPlayer = true;
	}

	// back to his REAL position
	if(isFarToMyPlayer)
	{
		// setup the move command
		BaseFighterCommandMove* cmd = new BaseFighterCommandMove();
		cmd->targetPosition = attacker->getRealWorldPosition();
		attacker->setNextCommand(cmd, false);
		attacker->setMoveSpeed(BASEFIGHTER_BASIC_MOVESPEED);
	}
}

void SkillProcessor::returnHome(BaseFighter* attacker)
{
}

void SkillProcessor::_addEffectToGround(GameSceneLayer* scene, float x, float y, std::string& effectName)
{
	SimpleActor* simpleActor = new SimpleActor();
	simpleActor->setGameScene(scene);
	const int OFFSET = 2;
	simpleActor->setWorldPosition(ccp(x, y + OFFSET));
	simpleActor->loadAnim(effectName.c_str());
	simpleActor->setScale(m_effectScaleX, m_effectScaleY);
	CCNode* actorLayer = scene->getActorLayer();
	actorLayer->addChild(simpleActor, SCENE_ROLE_LAYER_BASE_ZORDER);
	simpleActor->release();

	setPosX(simpleActor->getWorldPosition().x);
	setPosY(simpleActor->getWorldPosition().y);
}

void SkillProcessor::addEffectToGround(BaseFighter* actor, std::string& effectName)
{
	GameSceneLayer* scene = actor->getGameScene();

	_addEffectToGround(scene, actor->getWorldPosition().x, actor->getWorldPosition().y, effectName);
}

void SkillProcessor::addEffectToBody(BaseFighter* actor, std::string& effectName, int animDirection, bool bFoot)
{
	CCLegendAnimation* pAnim = CCLegendAnimation::create(effectName, animDirection);
	pAnim->setScale(m_effectScaleX, m_effectScaleY);
	actor->addEffect(pAnim, bFoot, 0);

	setPosX(actor->getWorldPosition().x);
	setPosY(actor->getWorldPosition().y);
}

void SkillProcessor::addMagicHaloToGround(GameSceneLayer* scene, CCPoint worldPosition)
{
	const float TOTAL_DURATION = 3.0f;
	const float STAY_DURATION = 1.5f;
	CCSprite* pSprite = CCSprite::create("animation/texiao/renwutexiao/magic_halo.png");
	CCFiniteTimeAction* rotate_action = CCSpawn::create(
		CCRotateBy::create(TOTAL_DURATION, 60),
		//CCFadeOut::create(DURATION),
		NULL);
	pSprite->runAction(rotate_action);

	CCNodeRGBA* pNode = CCNodeRGBA::create();
	pNode->setCascadeOpacityEnabled(true);
	pNode->setScaleX(3.0f);
	pNode->setScaleY(1.5f);
	CCPoint pos = scene->convertToCocos2DSpace(worldPosition);
	pNode->setPosition(pos);
	pNode->addChild(pSprite);
	CCAction* node_action = CCSequence::create(
		CCDelayTime::create(STAY_DURATION),
		CCFadeOut::create(TOTAL_DURATION - STAY_DURATION),
		CCRemoveSelf::create(),
		NULL);
	pNode->runAction(node_action);

	scene->getActorLayer()->addChild(pNode, SCENE_ROLE_LAYER_BASE_ZORDER);
}

void SkillProcessor::addMagicHaloToGround(GameSceneLayer* scene, CCPoint worldPosition, std::string str_spritePath, 
	float total_duration, float stay_duration, float scale_x, float scale_y)
{
	const float TOTAL_DURATION = total_duration;
	const float STAY_DURATION = stay_duration;
	CCSprite* pSprite = CCSprite::create(str_spritePath.c_str());
	CCFiniteTimeAction* rotate_action = CCSpawn::create(
		CCRotateBy::create(TOTAL_DURATION, 90),
		//CCFadeOut::create(DURATION),
		NULL);
	pSprite->runAction(rotate_action);

	CCNodeRGBA* pNode = CCNodeRGBA::create();
	pNode->setCascadeOpacityEnabled(true);
	pNode->setScaleX(scale_x);
	pNode->setScaleY(scale_y);
	CCPoint pos = scene->convertToCocos2DSpace(worldPosition);
	pNode->setPosition(pos);
	pNode->addChild(pSprite);
	CCAction* node_action = CCSequence::create(
		CCDelayTime::create(STAY_DURATION),
		CCFadeOut::create(TOTAL_DURATION - STAY_DURATION),
		CCRemoveSelf::create(),
		NULL);
	pNode->runAction(node_action);

	scene->getActorLayer()->addChild(pNode, SCENE_ROLE_LAYER_BASE_ZORDER);
}

void SkillProcessor::addWaveHaloToGround(GameSceneLayer* scene, CCPoint worldPosition, std::string spritePathName, float interval, int times, float maxScale)
{
	for(int i = 0; i < times; i++)
	{
		CCNode* pNode = NULL;
		int indexPng = spritePathName.find(".png");
		int indexAnm = spritePathName.find(".anm");
		if(indexPng >= 0)
		{
			pNode = CCSprite::create(spritePathName.c_str());
		}
		else if(indexAnm >= 0)
		{
			SimpleActor* simpleActor = new SimpleActor();
			simpleActor->setLayerId(0);   // ground layer
			simpleActor->setGameScene(scene);
			simpleActor->setWorldPosition(worldPosition);
			simpleActor->loadAnim(spritePathName.c_str(), true);
			simpleActor->setVisible(false);
			simpleActor->autorelease();

			pNode = simpleActor;
		}
		else
		{
			CCAssert(false, "wrong parameter");
		}
		pNode->setVisible(false);

		CCAction* spawnAction = CCSpawn::create(
			CCScaleTo::create(1.0f, maxScale),
			CCFadeOut::create(1.0f),
			NULL);
		CCSequence* action = CCSequence::create(
			CCDelayTime::create(i * interval),
			CCShow::create(),
			spawnAction,
			CCRemoveSelf::create(),
			NULL);
		pNode->runAction(action);

		scene->getActorLayer()->addChild(pNode, SCENE_ROLE_LAYER_BASE_ZORDER);
	}
}

void SkillProcessor::addBreathingHaloToGround(GameSceneLayer* scene, CCPoint worldPosition, const char*effectPathName)
{
	// ground effect
	CCLegendAnimation* pGroundAnim = CCLegendAnimation::create(effectPathName);
	pGroundAnim->setScale(2.0f, 1.0f);
	pGroundAnim->setPlayLoop(true);
	pGroundAnim->setVisible(false);
	pGroundAnim->setOpacity(26);
	pGroundAnim->setPosition(scene->convertToCocos2DSpace(worldPosition));
	CCRepeat * repeapAction = CCRepeat::create(CCSequence::create(
		CCFadeTo::create(0.35f,110),
		CCFadeTo::create(0.35f,160),
		CCFadeTo::create(0.5f,218),
		CCFadeTo::create(0.35f,160),
		CCFadeTo::create(0.35f,110),
		//CCDelayTime::create(0.1f),
		NULL), 2);
	pGroundAnim->runAction(CCSequence::create(
		CCDelayTime::create(0.7f),
		CCShow::create(),
		CCFadeTo::create(0.5f, 110),
		//CCDelayTime::create(2.5f),
		repeapAction,
		CCFadeTo::create(0.8f, 1),
		CCRemoveSelf::create(),
		NULL));
	scene->getActorLayer()->addChild(pGroundAnim, SCENE_GROUND_LAYER_BASE_ZORDER);
}

bool SkillProcessor::isBoss(BaseFighter* actor)
{
	Monster* pBoss = dynamic_cast<Monster*>(actor);
	if(pBoss != NULL)
	{
		if(pBoss->getClazz() == CMonsterBaseInfo::clazz_world_boss)
			return true;
		else if(pBoss->getClazz() == CMonsterBaseInfo::clazz_boss)
			return true;
	}
	return false;
}

////////////////////////////////////////////////////////////////

//
// CCPlayLegendAnimation
//

CCPlayLegendAnimation* CCPlayLegendAnimation::create() 
{
    CCPlayLegendAnimation* pRet = new CCPlayLegendAnimation();

    if (pRet) {
        pRet->autorelease();
    }

    return pRet;
}

void CCPlayLegendAnimation::update(float time) {
    CC_UNUSED_PARAM(time);
    //m_pTarget->setVisible(true);
	CCLegendAnimation* pTargetAnim = dynamic_cast<CCLegendAnimation*>(m_pTarget);
	if(pTargetAnim != NULL)
	{
		pTargetAnim->play();
	}
}
