#ifndef _SKILL_MOMENTSKILLPROCESSOR_H_
#define _SKILL_MOMENTSKILLPROCESSOR_H_

#include "SkillProcessor.h"
#include "../../../common/CKBaseClass.h"

/**
 * 瞬发技能的基类
 * 在施展攻击动作时，立即开始显示技能特效，并造成技能实际效果
 * @author zhao gang
 */
class BaseFighter;

class MomentSkillProcessor : public CKBaseClass, public SkillProcessor {
public:
	MomentSkillProcessor();
	virtual ~MomentSkillProcessor();
	
	virtual void init();

	virtual void update(float dt);

	virtual void onSkillBegan(BaseFighter* attacker);
	virtual void onSkillReleased(BaseFighter* attacker);

	virtual void release();

protected:
	BaseFighter* attacker;
	BaseFighter* defender;
};

#endif