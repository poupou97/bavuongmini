#include "FlyingSkillProcessor.h"

#include "../GameFightSkill.h"
#include "../../../gamescene_state/role/SimpleActor.h"
#include "../../../gamescene_state/role/BaseFighter.h"
#include "../../../gamescene_state/GameSceneState.h"
#include "../../../utils/GameUtils.h"
#include "../SkillSeed.h"
#include "../../../GameView.h"
#include "../../../legend_engine/CCLegendAnimation.h"
#include "../../../gamescene_state/role/BaseFighterOwnedStates.h"
#include "../../../gamescene_state/role/ActorUtils.h"

IMPLEMENT_CLASS(FlyingSkillProcessor)

FlyingSkillProcessor::FlyingSkillProcessor()
: m_nExtraDistance(0)
, m_flyingActor(NULL)
, m_bFollowBehaviour(true)
, m_bReached(false)
, m_streak(NULL)
, m_bExtraStreak(false)
, m_bExtraParticle(true)
, m_extraParticle(NULL)
, m_state(k_state_check)
, m_bEnableRandom(false)
, m_fRandomRange(0.0f)
, m_distance_for_reached(0.0f)
{
	attacker = NULL;
	mFlyingEndedEffect = "null";

	m_flyingSpeed = 64*6;

	m_ExtraParticleFileName = "animation/texiao/particledesigner/flying_tail.plist";

	// init streak parameter
	m_fStreakFade = 0.4f;
	m_fStreakMinSeg = 16.0f;
	m_fStreakStrokeWidth = 16.0f;
	m_StreakColor = ccc3(255, 255, 0);
}

FlyingSkillProcessor::~FlyingSkillProcessor()
{
	CC_SAFE_RELEASE(m_flyingActor);
	CC_SAFE_RELEASE(m_streak);
	CC_SAFE_RELEASE(m_extraParticle);
}

void* FlyingSkillProcessor::createInstance()
{
    return new FlyingSkillProcessor() ;
}

void FlyingSkillProcessor::init() {
	SkillProcessor::init();   // must call super calss's init()

	//sendAttackReq(NULL);

	attacker = dynamic_cast<BaseFighter*>(GameView::getInstance()->getGameScene()->getActor(getSkillSeed()->attacker));
	//defender = getScene().getBaseFighter(getSkillSeed().defender);

	// get the flyingEndedEffect
	if(attacker != NULL)
	{
		GameFightSkill* fightSkill = attacker->getGameFightSkill(getSkillSeed()->skillId.c_str(), getSkillSeed()->skillProcessorId.c_str());
		mFlyingEndedEffect = fightSkill->getSkillBin()->flyingEndedEffect;
	}
}

void FlyingSkillProcessor::release()
{
	// remove the flying actors
	//if(m_flyingActor)
	//	m_flyingActor->removeFromParent();

	//if(m_streak)
	//	m_streak->removeFromParent();

	//if(m_extraParticle)
	//	m_extraParticle->removeFromParent();
}

void FlyingSkillProcessor::update(float dt)
{
	if(isRelease()){
		return;
	}

	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(m_state == k_state_check)
	{
		if(checkHitTarget(dt))
		{
			// send message when the flying item hit the target
			sendAttackReq(NULL);

			// if the target is alive, play effect for this damage
			BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
			if(target != NULL && mFlyingEndedEffect != "null")
			{
				//// add effect on the target's body
				//CCLegendAnimation* pAnim = CCLegendAnimation::create(mFlyingEndedEffect);
				//target->addEffect(pAnim, false, 0);

				addEffectToGround(target, mFlyingEndedEffect);
			}

			// �Ǹ����ͷ��е��ߣ��ſ����д�͸
			if(!m_bFollowBehaviour && m_nExtraDistance > 0)
			{
				m_state = k_state_extra_distance;
				m_stateBeginTime = GameUtils::millisecondNow();
			}
			else
			{
				// remove the flying actors
				if(m_flyingActor)
					m_flyingActor->removeFromParent();

				if(m_streak)
					m_streak->removeFromParent();

				// end
				setRelease(true);
			}

			if(m_bExtraParticle)
			{
				if(m_extraParticle->getParent() != NULL)
				{
					CCFiniteTimeAction*  action = CCSequence::create(
						CCDelayTime::create(m_extraParticle->getLife()),
						CCRemoveSelf::create(),
						NULL);
					m_extraParticle->runAction(action);
				}
			}
		}
	}
	else if(m_state == k_state_extra_distance)
	{
		if(m_flyingActor->isSingleReference())
		{
			// remove the flying actors
			if(m_flyingActor)
				m_flyingActor->removeFromParent();

			if(m_streak)
				m_streak->removeFromParent();

			// end
			setRelease(true);
		}
	}

	// update streak
	if(m_bExtraStreak && m_streak)
	{
		m_streak->setPosition(ccp(m_flyingActor->getPosition().x, m_flyingActor->getPosition().y + m_flyingActor->getWorldPositionZ()));
	}
	// update particle
	if(m_bExtraParticle && m_extraParticle)
	{
		//m_extraParticle->setPosition(m_flyingActor->getPosition());
		m_extraParticle->setPosition(ccp(m_flyingActor->getPosition().x, m_flyingActor->getPosition().y + m_flyingActor->getWorldPositionZ()));
	}
	
	// following the target
	if(m_bFollowBehaviour && !m_bReached)
	{
		BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(m_targetRoleId));
		if(target == NULL)   // target disappear, so consider it has been readched
		{
			m_bReached = true;
		}
		else
		{
			//FLYING_ITEM_OFFSET_FROM_GROUND
			CCPoint targetPosition = ccp(target->getPosition().x, target->getPosition().y);
			float degree = GameUtils::getDegree(m_flyingActor->getPosition(), targetPosition);
			//float currentRotation = m_flyingActor->getRotation();
			float currentRotation = m_flyingActor->getLegendAnim()->getRotationX();
			float deltaDegree = degree - currentRotation;
			if(deltaDegree > 2)
			{
				deltaDegree = 2;
			}
			else if(deltaDegree < -2)
			{
				deltaDegree = -2;
			}
			//m_flyingActor->setRotation(currentRotation + deltaDegree);
			m_flyingActor->getLegendAnim()->setRotation(currentRotation + deltaDegree);

			CCPoint offset;
			CCPoint targetWorldPos = target->getWorldPosition();

			float distance = m_flyingSpeed * dt;
			// get direction by actor's current position and target pos
			offset = GameUtils::getDirection(m_flyingActor->getWorldPosition(), targetWorldPos);
			offset = ccpMult(offset, distance);

			if(m_distance_for_reached == 0)
			{
				if(offset.getLength() >= ccpDistance(m_flyingActor->getWorldPosition(), targetWorldPos))
				{
					m_bReached = true;
					offset = ccpSub(targetWorldPos, m_flyingActor->getWorldPosition());
				}
			}
			else
			{
				if(offset.getLength() + m_distance_for_reached >= ccpDistance(m_flyingActor->getWorldPosition(), targetWorldPos))
				{
					m_bReached = true;
				}
			}

			m_flyingActor->setWorldPosition(ccp(m_flyingActor->getWorldPosition().x + offset.x, m_flyingActor->getWorldPosition().y + offset.y));
		}
	}

	updateResume(attacker);
}

void FlyingSkillProcessor::onSkillBegan(BaseFighter* attacker)
{

}

void FlyingSkillProcessor::onSkillReleased(BaseFighter* attacker)
{
	SkillSeed* seed = this->getSkillSeed();
	GameSceneLayer* scene = attacker->getGameScene();
	GameFightSkill* fightSkill = attacker->getGameFightSkill(seed->skillId.c_str(), seed->skillProcessorId.c_str());
    
    //CCLOG("role: %lld, release ( %s )", attacker->getRoleId(), getSkillSeed()->skillId.c_str());

	// effect on body
	if(fightSkill->getSkillBin()->releaseEffect != "")
	{
		CCLegendAnimation* pAnim = CCLegendAnimation::create(fightSkill->getSkillBin()->releaseEffect, 0);
		attacker->addEffect(pAnim, false, 0);
	}

	// the arrow will be launched
	SimpleActor* pFlyingActor = new SimpleActor();
	pFlyingActor->setWorldPositionZ(FLYING_ITEM_OFFSET_FROM_GROUND);
	pFlyingActor->setScale(m_effectScaleX, m_effectScaleY);
	m_flyingActor = pFlyingActor;
	m_flyingActor->retain();
	pFlyingActor->setGameScene(attacker->getGameScene());
	//m_pArrowActor->loadSprite("animation/weapon/arrow01.png");
	// "animation/texiao/renwutexiao/skill/YCGJ01.anm"
	pFlyingActor->loadAnim(fightSkill->getSkillBin()->flyingItemEffect.c_str(), true);
	pFlyingActor->setAnchorPoint(ccp(0.5f, 0));
	//m_pFlyingActor->setScale(0.5f);

	BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
	CCPoint targetPosition = ActorUtils::getDefaultSkillPosition(attacker, seed->skillId.c_str(), seed->skillProcessorId.c_str());
	targetPosition = scene->convertToCocos2DSpace(targetPosition);
	if(target != NULL)
	{
		targetPosition = target->getPosition();

		m_targetRoleId = target->getRoleId();
	}
	// random factor
	if(m_bEnableRandom)
	{
		targetPosition.x += (CCRANDOM_0_1() - 0.5f) * m_fRandomRange;
		targetPosition.y += (CCRANDOM_0_1() - 0.5f) * m_fRandomRange;
	}
	
	scene->getActorLayer()->addChild(pFlyingActor, SCENE_ROLE_LAYER_BASE_ZORDER);
	pFlyingActor->release();

	CCPoint start = attacker->getPosition();
	// adjust the arrow's start position
	//int dir = attacker->getAnimDir();
	//if(dir == 1)   // face to left
	//	start.x -= 64;
	//else if(dir == 3)   // face to right
	//	start.x += 64;
	//m_pFlyingActor->setPosition(start);

	//// adjust the arrow's start position
	//float distance = ccpDistance(start, targetPosition);
	//if(distance > FLYING_ITEM_DISTANCE_OFFSET_WHEN_LAUNCHING)
	//	distance = FLYING_ITEM_DISTANCE_OFFSET_WHEN_LAUNCHING;
	//CCPoint offset = GameUtils::getDirection(start, targetPosition);
	//offset = ccpMult(offset, distance);
	//offset.y /= 2;
	//start = ccpAdd(start, offset);

	//pFlyingActor->setPosition(start);
	pFlyingActor->setPositionImmediately(start);
	pFlyingActor->setWorldPosition(attacker->getWorldPosition());

	if(target == NULL)
		m_bFollowBehaviour = false;

	float degree = GameUtils::getDegree(start, targetPosition);
	//pFlyingActor->setRotation(degree);
	pFlyingActor->getLegendAnim()->setRotation(degree);

	float _duration = 0;
	if(m_bFollowBehaviour)
	{
		// todo
	}
	else
	{
		m_fStartTime = GameUtils::millisecondNow();
		// calc the arrow's flying time
		CCPoint worldPos1 = scene->convertToGameWorldSpace(pFlyingActor->getPosition());
		CCPoint worldPos2 = scene->convertToGameWorldSpace(targetPosition);
		_duration = ccpDistance(worldPos1, worldPos2) / m_flyingSpeed;
		m_fDuration = _duration;

		// extra distance
		if(m_nExtraDistance > 0)
		{
			CCPoint offset = GameUtils::getDirection(start, targetPosition);
			float distance = ccpDistance(start, targetPosition);
			offset = ccpMult(offset, distance + m_nExtraDistance);
			targetPosition = ccpAdd(targetPosition, offset);

			_duration = ccpDistance(pFlyingActor->getPosition(), targetPosition) / m_flyingSpeed;
		}

		CCFiniteTimeAction*  action = CCSequence::create(
			CCMoveTo::create(_duration, targetPosition),
			CCRemoveSelf::create(),
			NULL);
		pFlyingActor->runAction(action);
	}

	// streak trail
	if(m_bExtraStreak)
	{
		m_streak=CCMotionStreak::create(m_fStreakFade, m_fStreakMinSeg, m_fStreakStrokeWidth, m_StreakColor, "images/streak.png");
		m_streak->retain();   // you must retain it
		m_streak->setPosition(start);

		scene->getActorLayer()->addChild(m_streak, SCENE_ROLE_LAYER_BASE_ZORDER);
	}

	// particle effect
	if(m_bExtraParticle)
	{
		// images/Particle.plist
		m_extraParticle = CCParticleSystemQuad::create(m_ExtraParticleFileName.c_str());
		m_extraParticle->setPosition(start);
		//m_extraParticle->setDuration(2.6f);
		m_extraParticle->setLife(0.5f);
		//m_extraParticle->setStartSize(1.0f);
		m_extraParticle->setPositionType(kCCPositionTypeRelative);

		m_extraParticle->retain();

		scene->getActorLayer()->addChild(m_extraParticle, SCENE_ROLE_LAYER_BASE_ZORDER);
	}
}

bool FlyingSkillProcessor::checkHitTarget(float dt)
{
	if(m_bFollowBehaviour)
	{
		return m_bReached;
	}
	else
	{
		if(GameUtils::millisecondNow() - m_fStartTime > m_fDuration * 1000)
		{
			return true;
		}
	}

	return false;
}

bool FlyingSkillProcessor::jumpAndShoot(BaseFighter* attacker)
{
	GameSceneLayer* scene = attacker->getGameScene();

	////////////////////////////////////////////////////////////
	// jump back
	////////////////////////////////////////////////////////////
	// it's not my generals, ignore
	if(attacker->getPosInGroup() < 1)
		return false;

	BaseFighter* target = dynamic_cast<BaseFighter*>(scene->getActor(this->getSkillSeed()->defender));
	if(target == NULL)
		return false;

	const int maxJumpDistance = 64 * 3.5;
	const int jumpDistance = 64 * 2.5;

	// 1. jump back
	if(ccpDistance(target->getWorldPosition(), attacker->getWorldPosition()) > maxJumpDistance)
		return false;

	// calc the back offset
	CCPoint dir = GameUtils::getDirection(target->getWorldPosition(), attacker->getWorldPosition());
	float back_distance = jumpDistance;  // 64*2;   // 64 * 2.5f
	CCPoint offset = ccpMult(dir, back_distance);
	CCPoint targetPos = ccpAdd(attacker->getWorldPosition(), offset);
	int tileX = scene->positionToTileX(targetPos.x);
	int tileY = scene->positionToTileY(targetPos.y);
	if(scene->isLimitOnGround(tileX, tileY))
	{
		return false;
	}
	offset.y /= 2;
	offset.y = -offset.y;

	CCPoint cocos2dPosition = scene->convertToCocos2DSpace(attacker->getWorldPosition());
	CCPoint cocos2dTargetPos = ccpAdd(cocos2dPosition, offset);
	CCPoint targetWorldPosition =  scene->convertToGameWorldSpace(cocos2dTargetPos);

	if(ccpDistance(scene->getActor(BaseFighter::getMyPlayerId())->getWorldPosition(), targetWorldPosition) > 64 * 4)
		return false;

	ActionJumpContext context;
	context.startTime = GameUtils::millisecondNow();
	context.targetPosition = cocos2dTargetPos;
	context.shadowColor = ccc3(150, 150, 255);
	float jumpBackSpeed = 350.f;
	context.jumpDuration = ccpDistance(attacker->getWorldPosition(), targetWorldPosition) / jumpBackSpeed;
	context.setContext(attacker);

	attacker->changeAction(ACT_JUMP);

	//////////////////////////////////////////////////////////////
	// 2. after jump back, shoot one arrow! it's cool!

	// the arrow will be launched
	SimpleActor* pFlyingActor = new SimpleActor();
	pFlyingActor->setGameScene(attacker->getGameScene());
	pFlyingActor->setWorldPositionZ(FLYING_ITEM_OFFSET_FROM_GROUND);
	SkillSeed* seed = this->getSkillSeed();
	GameFightSkill* fightSkill = attacker->getGameFightSkill(seed->skillId.c_str(), seed->skillProcessorId.c_str());
	pFlyingActor->loadAnim(fightSkill->getSkillBin()->flyingItemEffect.c_str(), true);
	pFlyingActor->setAnchorPoint(ccp(0.5f, 0));
	scene->getActorLayer()->addChild(pFlyingActor, SCENE_ROLE_LAYER_BASE_ZORDER);
	pFlyingActor->release();

	// �Ӻ���ɺ��λ�÷���һ֧��
	CCPoint startPosition = ccp(cocos2dTargetPos.x, cocos2dTargetPos.y);
	pFlyingActor->setPositionImmediately(startPosition);
	pFlyingActor->setWorldPosition(targetWorldPosition);

	CCPoint targetPosition = target->getPosition();

	float degree = GameUtils::getDegree(pFlyingActor->getPosition(), targetPosition);
	//pFlyingActor->setRotation(degree);
	pFlyingActor->getLegendAnim()->setRotation(degree);

	// calc the arrow's flying time
	float flyingSpeed = 64*6;
	float time = ccpDistance(startPosition, targetPosition) / flyingSpeed;

	CCFiniteTimeAction*  action = CCSequence::create(
		CCDelayTime::create(context.jumpDuration),   // delay the time for jumping back
		CCMoveTo::create(time, targetPosition),
		CCRemoveSelf::create(),
		NULL);
	pFlyingActor->runAction(action);

	return true;
}