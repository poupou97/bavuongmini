//
//  ServerFightConstants.m
//
//  Created by zhao gang.
//  Copyright (c) 2013 Perfect Future. All rights reserved.
//

#include "ServerFightConstants.h"

const char* PoisonType_greyProps[] = {
    "daozunlingduhong13",
    "hongseduyaoshaoliang8",
    "hongseduyaozhongliang9",
    "hongseduyaodaliang10",
    "xinshouhongseduyaoshaoliang16"
};

const char* PoisonType_yellowProps[] = {
    "daozunlingdulv12",
    "lvseduyaoshaoliang5",
    "lvseduyaozhongliang6",
    "lvseduyaodaliang7",
    "xinshoulvseduyaoshaoliang15"
};

