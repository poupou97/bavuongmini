#ifndef _SKILL_SKILLMANAGER_H_
#define _SKILL_SKILLMANAGER_H_

#include "cocos2d.h"
#include "cocos-ext.h"

USING_NS_CC;

/**
 * 技能管理者，根据传入的技能参数SkillSeed或CSkillResult，进行相应的技能流程处理
 * @author zhaogang
 */
class SkillSeed;
class CSkillResult;
class SkillProcessor;

class SkillManager{
public:
	static SkillProcessor* createSkillProcessor(const char* processorId);

public:
	SkillManager();
	virtual ~SkillManager();
	
	/**
	 * 添加一个技能流程
	 * @param skillSeed
	 */
	SkillProcessor* addSkillProcessor(SkillSeed* skillSeed);
		/**
	 * 添加一个技能流程
	 * @param skillSeed
	 */
	SkillProcessor* addSkillProcessor(CSkillResult* result);
	
	/**
	 * 技能流程逻辑
	 */
	void update(float dt);
	
	std::vector<SkillProcessor*>* getSkillProcessorList();
	void setSkillProcessorList(std::vector<SkillProcessor*>* skillProcessorList);

private:
	/**
	 * 需要更新绘制的技能流程
	 */
	std::vector<SkillProcessor*>* skillProcessorList;
};

#endif