#ifndef _GAMESTATE_GAMESCENE_STATE_H_
#define _GAMESTATE_GAMESCENE_STATE_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../GameStateBasic.h"

USING_NS_CC_EXT;

// TILE is square ( 64 * 64 )
#define TILE_SIZE 64
#define TILE_SIZE_SHIFT 6   // 2 ^ 6 = 64
#define TILE_SIZE_HALF 32

#define TILE_WIDTH	64
#define TILE_WIDTH_SHIFT 6   // 2 ^ 6 = 64
#define TILE_HEIGHT 32
#define TILE_HEIGHT_SHIFT 5   // 2 ^ 5 = 32
#define TILE_WIDTH_HALF 32
#define TILE_HEIGHT_HALF 16

#define NULL_ROLE_ID LLONG_MAX

#define SCENE_ACTOR_BASE_ID 10000000

// pickActor() 所用的参数之一的常量定义
#define PICK_ACTOR_ALGORITHM_CROSS 0   // 矩形相交算法，便于玩家选中怪物
#define PICK_ACTOR_ALGORITHM_CONTAIN 1   // 包含算法，选取点包含于被选择物体的矩形内，选取规则更严格

#define TILEDMAP_COLLISIONLAYER_ID 2

// definication of paint order
#define SCENE_TILEDMAP_BASE_ZORDER 0
#define SCENE_GROUND_LAYER_BASE_ZORDER 1
#define SCENE_ROLE_LAYER_BASE_ZORDER 4000
#define SCENE_SKY_LAYER_BASE_ZORDER 8000
#define SCENE_TOP_LAYER_BASE_ZORDER 12000
#define SCENE_UI_LAYER_BASE_ZORDER 16000
#define SCENE_STORY_LAYER_BASE_ZORDER 20000   // 剧情对话等内容位于绝对部分UI之上，但在跑马灯UI之下

#define UI_PRIORITY_CUSTOM_UI (-150)   // greater than kCCMenuHandlerPriority = -128

////////////////////////////////////////////////////////////////

class GameActor;
class MyPlayer;
class AStarTiledMap;
class AStarPathFinder;
class BaseFighter;
class MainScene;
class SkillManager;
class MapDoor;
class GameSceneCamera;
class LegendLevel;
class GameSceneMusicController;
class TiledMapLayer;
class MainAnimationScene;

class GameSceneState : public GameState
{
public:
	enum {
		TAG_GAMESCENE = 49,
		TAG_MAINSCENE = 50,
		TAG_ANIMATIONSCENE = 51,
		TAG_LOADSCENE = 52,
	};

	enum {
		state_load = 0,
		state_game = 1,
	};

public:
	GameSceneState();

    virtual void runThisState();

	virtual void onSocketOpen(ClientNetEngine* socketEngine);
    virtual void onSocketClose(ClientNetEngine* socketEngine);
    virtual void onSocketError(ClientNetEngine* ssocketEngines, const ClientNetEngine::ErrorCode& error);

	bool canHandleMessage();

	void changeToLoadState();
	/**
	   * GameSceneLayer::create() will be excuted in this function
	   * GameSceneLayer::create() is a function which cost much time
	  **/
	void prepareGameState();
	void changeToGameState();

	bool isInLoadState();
	bool isInGameState();

	int getState();
private:
	void backToLoginState(float dt);

private:
	MainScene * mainScene;
	MainAnimationScene * mainAnmScene;
	int m_state;
};

//////////////////////////////////////////////////////////////////////

class CCLegendTiledMap;
class GameSceneLayer : public cocos2d::CCLayer
{
public:
	enum {
		// the tag of the main layers
		kTagSceneLayer = 2,				// the root node for the actor layers and tiled map
		kTagTiledMapNode = 21,			// background
		kTagActorLayer = 22,				// actor layer
		kTagMovieActorLayer = 23,		// 供剧情模式使用的actor layer
	};

public:
	GameSceneLayer();
	~GameSceneLayer();

	static std::vector<std::string>* generateResourceList(const char* levelName);

	static GameSceneLayer* create();

	bool init();
	void initMyPlayer();

	virtual void update(float dt);

	virtual void registerWithTouchDispatcher();
	virtual bool ccTouchBegan(CCTouch* touch, CCEvent* event);
	virtual void ccTouchMoved(CCTouch* touch, CCEvent* event);
	virtual void ccTouchEnded(CCTouch* touch, CCEvent* event);
	//virtual void ccTouchesEnded(CCSet *pTouches, CCEvent *pEvent);

	inline AStarPathFinder* getPathFinder() { return s_pathFinder; };

	void setView(CCNode* sceneLayer, CCPoint viewPos);
	bool isOutOfView(GameActor* actor);

	// the functions of grid map
	static short positionToTileX(int x);
	static short positionToTileY(int y);
	static int tileToPositionX(short x);
	static int tileToPositionY(short y);
	static bool isLimitOnOtherMapGround(short tileX, short tileY, LegendLevel* pLevelData);
	static bool checkInOtherMap(short tileX, short tileY, TiledMapLayer* collisionLayer);
	bool checkInMap(short tileX, short tileY);
	bool isLimitOnGround(short tileX, short tileY);
	void setTiledMapLimit(short tileX, short tileY,int tiledData);
	bool isWater(short tileX, short tileY);
	bool isCover(short tileX, short tileY);
	// 游戏世界坐标下，场景的尺寸
	CCSize getMapSize();
	/**
	 * 判断起点和终点之间是否有障碍物
	 * (起点和终点采用游戏世界坐标，像素值)
	 */
	bool isLineIntersectBarrier(float startX, float startY, float endX, float endY);
	/**
	 * 若目标点(Target)是障碍物，则以出发点为依托，尝试寻找离目标点最近的坐标
	 * (起点和终点采用游戏世界坐标，像素值)
	 * @return  返回值依旧是像素坐标
	 *          if NULL, can not find nearest position
	 */
	CCPoint* findNearestPositionToTarget(float startX, float startY, float endX, float endY);
	/**
	 * 若目标点(Target)是障碍物，则以出发点为依托，尝试寻找离目标点的次最近点
	 * (起点和终点采用游戏世界坐标，像素值)
	 * @return  返回值依旧是像素坐标
	 *          if NULL, can not find second nearest position
	 */
	CCPoint* findSecondNearestPositionToTarget(float startX, float startY, float endX, float endY);

	CCPoint getNearReachablePoint(CCPoint& targetPoint);

	//
	// 逻辑运算（角色移动、释放技能等）都采用世界坐标系，最终绘制时，才转换为cocos2d坐标
	// GameWorldSpace: 游戏世界坐标，左上角为(0, 0)，向右、向下为正方向
	// Cocos2DWorldSpace: 和Cocos2D引擎相同的游戏世界坐标，左下角为(0, 0)，向右、向上为正方向
	// Cocos2DSpace: Cocos2D引擎的绘制坐标，左下角为(0, 0)，向右、向上为正方向
	//
	// 3d坐标转换，游戏采用固定的斜45度视角，y值和z值都是原来的固定化的比例（简化投影变换）
	CCPoint convertToCocos2DSpace(const CCPoint& gameWorldPoint);
	CCPoint convertToCocos2DWorldSpace(const CCPoint& gameWorldPoint);
	CCPoint convertToGameWorldSpace(const CCPoint& cocosNodePoint);

	void putActor(long long roleId, GameActor* actor);
	GameActor* getActor(long long roleId);
	void removeActor(long long roleId);
	void getTypeActor(vector<long long>& typeActor, int actorType);

	void selectActor(GameActor* actor);
	void unselectActor(GameActor* actor);

	void putActorToClientVector(GameActor* actor);

	void addComboEffect();
	void removeComboEffect(CCObject * obj);

	inline SkillManager* getSkillMgr() { return m_pSkillManager; };

	//CCPoint getDoorPosition(std::string sourceMap, std::string targetMapId);
	CCPoint getDoorPosition(std::string sourceMap, std::string targetMapId, LegendLevel* pLevelData = NULL);

	void LoadScene();
	void LoadMovieScene();
	// remove the actors who are created by script CreateNPC ( SimpleFighter )
	void removeMovieActors();

	MainScene* getMainUIScene();
	CCLayer* getActorLayer();
	CCLayer* getMovieActorLayer();
	MainAnimationScene * getMainAnimationScene();

	inline std::map<long long, GameActor*>& getActorsMap() { return m_ActorsMap; };

	inline GameSceneCamera* getSceneCamera() { return m_pCamera; };

	/**
	   * 用于方便和CCAction机制结合起来
	   * 不同步时，仅仅根据规则，修改game world position。等待同步开启后，进行位置同步
	   * 同步时，真正的将actor::worldposition设置为cocos的坐标(CCNode::setPosition()
	   */
	void setSynchronizeCocosPosition(bool bSynchronize);
	bool isSynchronizeCocosPosition();

private:
	int getActorZOrder(GameActor* actor);

	// 生成用于A*寻路算法的地图信息
	void generateSearchMap();

	/**
	 * 根据参数指定的选择点，在可见actors中进行选择，返回被选中的actor id
	 * @param actorTypes，表示actor类型的集合(mask)
	 * @param targetPos
	 * @param algorithmType，不同的选择算法
	 * @param canSelectSelf, 是否可以选择玩家自己
	 */
	long long pickActor(int actorTypes, CCPoint targetPos, int algorithmType, bool canSelectSelf);

private:
	MyPlayer* m_pMyPlayer;
	CCLegendTiledMap* m_tiledmap;
	CCArray* m_actors;

	AStarTiledMap* m_searchMap;
	CCSize m_mapSize;

	static AStarPathFinder* s_pathFinder;

	std::map<long long, GameActor*> m_ActorsMap;
	/** 该容器存放各种client actor，比如将要死亡的怪物，会被击飞
	  *  这个过程中，该actor实例已经和服务器脱离了关系，故放在这个容器里进行update
	  */
	std::vector<GameActor*> m_ClientActorsVector;

	/* 技能管理 */
	SkillManager* m_pSkillManager;

	GameSceneCamera* m_pCamera;

	LegendLevel* m_curLevelData;

	bool m_bInitMovieScene;

	bool m_bSynchronizeCocosPosition;

	GameSceneMusicController* m_pMusicController;
};

#endif // _GAMESTATE_GAMESCENE_STATE_H_
