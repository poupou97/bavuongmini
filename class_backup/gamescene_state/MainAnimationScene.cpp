#include "MainAnimationScene.h"
#include "role/WeaponEffect.h"
#include "../ui/GameUIConstant.h"
#include "../legend_engine/CCLegendAnimation.h"
#include "GameSceneEffects.h"
#include "MainScene.h"

MainAnimationScene::MainAnimationScene()
{

}

MainAnimationScene::~MainAnimationScene()
{

}

MainAnimationScene* MainAnimationScene::create()
{
	MainAnimationScene * mainAnmScene = new MainAnimationScene();
	if (mainAnmScene && mainAnmScene->init())
	{
		mainAnmScene->autorelease();
		return mainAnmScene;
	}
	CC_SAFE_DELETE(mainAnmScene);
	return NULL;
}

bool MainAnimationScene::init()
{
	if (UIScene::init())
	{
		//界面动画层
		m_baseLayer = UILayer::create();
		this->addChild(m_baseLayer);

		return true;
	}
	return false;
}

void MainAnimationScene::onEnter()
{
	UIScene::onEnter();
}

void MainAnimationScene::onExit()
{
	UIScene::onExit();
}
/***********************
change by yangjun 
data:2014.11.12
//人物升级动画优先级比较高
***************************/
#define Anm_LevelUp_Tag 1122

void MainAnimationScene::addInterfaceAnm(const char * anmName)
{
	if (m_baseLayer->getChildByTag(Anm_LevelUp_Tag))
		return;

	m_baseLayer->removeAllChildren();

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	//CCParticleSystem* particleEffect = CCParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo.plist");
	CCParticleSystem* particleEffect = WeaponEffect::getParticle(INTERFACE_ANM_PARTICLE_EFFECT_1);
	particleEffect->setPositionType(kCCPositionTypeGrouped);
	particleEffect->setPosition(ccp(winSize.width/2,winSize.height*0.7f));
	particleEffect->setScale(1.0f);
	m_baseLayer->addChild(particleEffect);

	//CCParticleSystem* particleEffect1 = CCParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo1.plist");
	CCParticleSystem* particleEffect1 = WeaponEffect::getParticle(INTERFACE_ANM_PARTICLE_EFFECT_2);
	particleEffect1->setPositionType(kCCPositionTypeGrouped);
	particleEffect1->setPosition(ccp(winSize.width/2+35,winSize.height*0.7f+55));
	particleEffect1->setScale(1.0f);
	m_baseLayer->addChild(particleEffect1);

	//CCParticleSystem* particleEffect2 = CCParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo2.plist");
	CCParticleSystem* particleEffect2 = WeaponEffect::getParticle(INTERFACE_ANM_PARTICLE_EFFECT_3);
	particleEffect2->setPositionType(kCCPositionTypeGrouped);
	particleEffect2->setPosition(ccp(winSize.width/2-80,winSize.height*0.7f+55));
	particleEffect2->setScale(1.0f);
	m_baseLayer->addChild(particleEffect2);

	//CCParticleSystem* particleEffect3 = CCParticleSystemQuad::create("animation/texiao/particledesigner/shanshuo3.plist");
	CCParticleSystem* particleEffect3 = WeaponEffect::getParticle(INTERFACE_ANM_PARTICLE_EFFECT_4);
	particleEffect3->setPositionType(kCCPositionTypeGrouped);
	particleEffect3->setPosition(ccp(winSize.width/2+100,winSize.height*0.7f+40));
	particleEffect3->setScale(1.0f);
	m_baseLayer->addChild(particleEffect3);

	// load animation
	std::string animFileName = "animation/texiao/jiemiantexiao/";
	animFileName.append(anmName);
	//animFileName.append("/");
	//animFileName.append(anmName);
	//animFileName.append(".anm");
	CCLegendAnimation *m_pAnim = CCLegendAnimation::create(animFileName);
	m_pAnim->setReleaseWhenStop(true);
	m_pAnim->setPosition(ccp(winSize.width/2,winSize.height*0.7f));
	m_baseLayer->addChild(m_pAnim);
	std::string str_anm_namePath = anmName;
	if (strcmp("GXSJ",str_anm_namePath.substr(0,4).c_str()) == 0)
	{
		m_pAnim->setTag(Anm_LevelUp_Tag);
	}
}

void MainAnimationScene::addFightPointChangeEffect( int oldFight,int newFight )
{
	FightPointChangeEffect * fightEffect_ = (FightPointChangeEffect*)this->getChildByTag(kTagFightPointChange);
	if (!fightEffect_)
	{
		FightPointChangeEffect * fightEffect_ = FightPointChangeEffect::create(oldFight,newFight);
		if (fightEffect_)
		{
			fightEffect_->setTag(kTagFightPointChange);
			this->addChild(fightEffect_);
		}
	}
}

