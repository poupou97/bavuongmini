#include "EffectDispatch.h"
#include "GameSceneEffects.h"
#include "GameView.h"
#include "MainScene.h"
#include "../messageclient/element/GoodsInfo.h"
#include "MainAnimationScene.h"
#include "../messageclient/element/FolderInfo.h"
#include "../ui/extensions/ShortCutItem.h"
#include "../ui/extensions/RecruitGeneralCard.h"
#include "../utils/StaticDataManager.h"
#include "role/BasePlayer.h"
#include "role/MyPlayer.h"
#include "../messageclient/element/CBaseSkill.h"
#include "../ui/extensions/GetSkillTeach.h"
#include "messageclient/element/CEquipment.h"
#include "messageclient/element/CGeneralBaseMsg.h"
#include "messageclient/element/CGeneralDetail.h"


EffectDispatcher::EffectDispatcher(void)
{
}

EffectDispatcher::~EffectDispatcher(void)
{
	std::vector<EffectBaseInfo*>::iterator iter;
	for (iter = effectCollectionVector.begin(); iter != effectCollectionVector.end(); ++iter)
	{
		delete *iter;
	}
	effectCollectionVector.clear();
}

EffectDispatcher * EffectDispatcher::create()
{
	EffectDispatcher * effect_ = new EffectDispatcher();
	if (effect_ && effect_->init())
	{
		effect_->autorelease();
		return effect_;
	}
	CC_SAFE_DELETE(effect_);
	return NULL;
}

bool EffectDispatcher::init()
{
	if (UIScene::init())
	{
		schedule(schedule_selector(EffectDispatcher::updateEffect),0.5f);
		return true;
	}
	return false;
}

void EffectDispatcher::onEnter()
{
	UIScene::onEnter();
}

void EffectDispatcher::onExit()
{
	UIScene::onExit();
}

void EffectDispatcher::pushEffectToVector(EffectBaseInfo * effectInfo)
{
	effectCollectionVector.push_back(effectInfo);
}

void EffectDispatcher::updateEffect( float dt )
{
	if (effectCollectionVector.size() <= 0)
	{
		return;
	}

	EffectBaseInfo * baseInfo_ = (EffectBaseInfo *)effectCollectionVector.at(0);
	int type_ = baseInfo_->getType();

	switch(type_)
	{
	case type_fightPoint:
		{
			EffectOfFightPointChange * temp_fightPoint = (EffectOfFightPointChange *)effectCollectionVector.at(0);
			temp_fightPoint->showEffect();
		};
	case type_itemWave:
		{
			EffectOfItemWave * temp_levelUp = (EffectOfItemWave *)effectCollectionVector.at(0);
			temp_levelUp->showEffect();
		}break;
	case type_normal:
		{
			EffectOfNormal * temp_normalEffect = (EffectOfNormal *)effectCollectionVector.at(0);
			temp_normalEffect->showEffect();
		}break;
	case type_equipWave:
		{
			EffectEquipItem *temp_teachRemind = (EffectEquipItem *)effectCollectionVector.at(0);
			temp_teachRemind->showEffect();
		}break;
	case type_skillStudy:
		{
			EffectStudySkill *temp_SkillStudy = (EffectStudySkill *)effectCollectionVector.at(0);
			temp_SkillStudy->showEffect();
		}break;
	case type_recruit:
		{
			EffectRecruit *temp_recruit = (EffectRecruit *)effectCollectionVector.at(0);
			temp_recruit->showEffect();
		}break;
	}

	std::vector<EffectBaseInfo *>::iterator iter= effectCollectionVector.begin();
	EffectBaseInfo * baseInfo_iterm =*iter ;
	effectCollectionVector.erase(iter);
	delete baseInfo_iterm;
}

std::vector<EffectBaseInfo *> EffectDispatcher::effectCollectionVector;

///////////////////////////////////
EffectBaseInfo::EffectBaseInfo()
{

}

EffectBaseInfo::~EffectBaseInfo()
{

}

void EffectBaseInfo::setType( int type_ )
{
	m_type = type_;
}

int EffectBaseInfo::getType()
{
	return m_type;
}

/////////////////////////////////////
EffectOfFightPointChange::EffectOfFightPointChange()
{

}

EffectOfFightPointChange::~EffectOfFightPointChange()
{

}

void EffectOfFightPointChange::showEffect()
{
	MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
	if (mainAnmScene)
	{
		mainAnmScene->addFightPointChangeEffect(getOldFight(),getNewFight());
	}
}

void EffectOfFightPointChange::setOldFight( int value_ )
{
	m_oldFight = value_;
}

int EffectOfFightPointChange::getOldFight()
{
	return m_oldFight;
}

void EffectOfFightPointChange::setNewFight( int value_ )
{
	m_newFight = value_;
}

int EffectOfFightPointChange::getNewFight()
{
	return m_newFight;
}

//////////////////////////// item wave effect
EffectOfItemWave::EffectOfItemWave()
{

}

EffectOfItemWave::~EffectOfItemWave()
{

}

void EffectOfItemWave::setItemInfo( GoodsInfo * goods )
{
	m_goodsinfo = new GoodsInfo();
	m_goodsinfo->CopyFrom(*goods);
}

GoodsInfo * EffectOfItemWave::getItemInfo()
{
	return m_goodsinfo;
}

void EffectOfItemWave::showEffect()
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	std::string goodsIcon_ = getItemInfo()->icon();
	int goodsQuality_ = getItemInfo()->quality();

	std::string Icon = "res_ui/props_icon/";
	Icon.append(goodsIcon_.c_str());
	Icon.append(".png");
	
	CCScale9Sprite* pSprite = CCScale9Sprite::create();

	std::string frameColorPath;
	switch(goodsQuality_)
	{
	case 1:
		{
			frameColorPath = "res_ui/di_white.png";
		}break;
	case 2:
		{
			frameColorPath = "res_ui/di_green.png";
		}break;
	case 3:
		{
			frameColorPath = "res_ui/di_bule.png";
		}break;
	case 4:
		{
			frameColorPath = "res_ui/di_purple.png";
		}break;
	case 5:
		{
			frameColorPath = "res_ui/di_orange.png";
		}break;
	default:
		{
			frameColorPath = "res_ui/di_none.png";
		}break;
	}

	pSprite->initWithFile(frameColorPath.c_str());
	pSprite->setPosition(ccp(winSize.width/2+225, winSize.height/2-100));
	pSprite->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite->setContentSize(CCSizeMake(83,75));
	pSprite->setOpacity(0);
	GameView::getInstance()->getMainUIScene()->addChild(pSprite, 1);

	CCSprite* pSpriteIcon = CCSprite::create(Icon.c_str());
	if(pSpriteIcon == NULL)
	{
		pSpriteIcon = CCSprite::create("res_ui/props_icon/prop.png");
	}
	pSpriteIcon->setPosition(ccp(41, 38));
	pSpriteIcon->setAnchorPoint(ccp(0.5f, 0.5f));
	pSprite->addChild(pSpriteIcon);

	std::string goodsName_ = getItemInfo()->name();
	CCLabelTTF* Lable_name = CCLabelTTF::create(goodsName_.c_str(), "Arial", 12);
	Lable_name->setAnchorPoint(ccp(0.5f,0.5f));
	Lable_name->setPosition(ccp(41,12));
	Lable_name->setColor(GameView::getInstance()->getGoodsColorByQuality(goodsQuality_));
	pSprite->addChild(Lable_name);

	pSprite->runAction(CCSequence::create(
		CCSpawn::create(
		CCFadeIn::create(0.2f),
		CCMoveTo::create(0.2f, ccp(winSize.width/2+75, winSize.height/2-100)),
		NULL),
		CCMoveTo::create(1.0f,ccp(winSize.width/2-100, winSize.height/2-100)),
		CCSpawn::create(
		CCMoveTo::create(0.75f,ccp(50, 0)),
		CCRotateBy::create(0.75f, 360),
		CCScaleTo::create(0.75f, 0),
		NULL),
		CCRemoveSelf::create(),
		NULL));
}

////////////////////////////////normal effect
EffectOfNormal::EffectOfNormal()
{

}

EffectOfNormal::~EffectOfNormal()
{

}

void EffectOfNormal::showEffect()
{
	MainAnimationScene * mainAnmScene = GameView::getInstance()->getMainAnimationScene();
	if (mainAnmScene)
	{
		mainAnmScene->addInterfaceAnm(getEffectStrPath().c_str());
	}
}

void EffectOfNormal::setEffectStrPath( std::string effectPath )
{
	m_effectStr = effectPath;
}

std::string EffectOfNormal::getEffectStrPath()
{
	return m_effectStr;
}

/////////////////////////////////////////////
EffectEquipItem::EffectEquipItem()
{

}

EffectEquipItem::~EffectEquipItem()
{

}

void EffectEquipItem::showEffect()
{
	FolderInfo * folder_ =new FolderInfo();
	folder_->CopyFrom(*getFolderInfo());
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	if (folder_->goods().equipmentclazz() >0 && folder_->goods().equipmentclazz() <10)
	{
		if (GameView::getInstance()->getMainUIScene()->getMainUIElementLayer()->getChildByTag(501) == NULL)
		{
			ShortCutItem * equip_wave = ShortCutItem::create(folder_,getId(),getPriority());
			equip_wave->ignoreAnchorPointForPosition(false);
			equip_wave->setAnchorPoint(ccp(1.0f,0));
			equip_wave->setPosition(ccp(winSize.width+ equip_wave->getContentSize().width+50,165));	
			equip_wave->setTag(501);
			GameView::getInstance()->getMainUIScene()->getMainUIElementLayer()->addChild(equip_wave);

			CCMoveTo * moveTo_ = CCMoveTo::create(0.5f,ccp(winSize.width - 90,165));
			CCEaseBackOut * easebackOut = CCEaseBackOut::create(moveTo_);
			equip_wave->runAction(easebackOut);
		}
	}else
	{
		ShortCutItem * equip_wave = ShortCutItem::create(folder_,getId(),getPriority());
		equip_wave->ignoreAnchorPointForPosition(false);
		equip_wave->setAnchorPoint(ccp(1.0f,0));
		equip_wave->setPosition(ccp(winSize.width+ equip_wave->getContentSize().width+50,165));	
		equip_wave->setTag(501);
		GameView::getInstance()->getMainUIScene()->getMainUIElementLayer()->addChild(equip_wave);

		CCMoveTo * moveTo_ = CCMoveTo::create(0.5f,ccp(winSize.width - 90,165));
		CCEaseBackOut * easebackOut = CCEaseBackOut::create(moveTo_);
		equip_wave->runAction(easebackOut);
	}
	delete folder_;

	this->reloadShortItem();
}

void EffectEquipItem::setFolderInfo( FolderInfo * foler )
{
	m_folderInfo = new FolderInfo();
	m_folderInfo->CopyFrom(*foler);
}

FolderInfo * EffectEquipItem::getFolderInfo()
{
	return m_folderInfo;
}

void EffectEquipItem::setId( long long id_ )
{
	m_id = id_;
}

long long EffectEquipItem::getId()
{
	return m_id;
}

void EffectEquipItem::setPriority( bool isValue )
{
	m_priority = isValue;
}

bool EffectEquipItem::getPriority()
{
	return m_priority;
}

bool compareItemFightCapacity(ShortCutItem * item1 ,ShortCutItem * item2)
{
	return item1->getFightcapacity() > item2->getFightcapacity();
}

bool compareFolderFightCapacity(FolderInfo * folder1 ,FolderInfo * folder2)
{
	return folder1->goods().equipmentdetail().fightcapacity() > folder2->goods().equipmentdetail().fightcapacity();
}

void EffectEquipItem::reloadShortItem()
{
	//folderItemVector
	//create new itemshort
	//delete old itemshort
	if (folderItemVector.size() <= 0)
	{
		return;
	}
	refreshFolderIndex();
	bool isHasLayer = false;
	if (GameView::getInstance()->getMainUIScene()->getMainUIElementLayer()->getChildByTag(501))
	{
		//ShortCutItem * equip_wave = (ShortCutItem *)GameView::getInstance()->getMainUIScene()->getMainUIElementLayer()->getChildByTag(501);
		//equip_wave->removeFromParentAndCleanup(true);

		CCLayer * layer_ = (CCLayer *)GameView::getInstance()->getMainUIScene()->getMainUIElementLayer();
		layer_->removeAllChildren();
		isHasLayer = true;
	}

	sort(folderItemVector.begin(),folderItemVector.end(),compareFolderFightCapacity);

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	for (int i =0;i<folderItemVector.size() ;i++)
	{
		FolderInfo * folder_ =new FolderInfo();
		folder_->CopyFrom(*folderItemVector.at(i));

		bool isCreate = false;
		int playerEquipFight_ = 0;
		for (int i = 0;i<GameView::getInstance()->EquipListItem.size();i++)
		{
			if (folder_->goods().equipmentclazz() == GameView::getInstance()->EquipListItem.at(i)->goods().equipmentclazz())
			{
				playerEquipFight_ = GameView::getInstance()->EquipListItem.at(i)->goods().equipmentdetail().fightcapacity();
			}
		}

		//level  profession general role
		int curRoleLevel_ = GameView::getInstance()->myplayer->getActiveRole()->level();
		int openlevel = 0;
		std::map<int,int>::const_iterator cIter;
		cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(26);
		if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end())
		{
		}
		else
		{
			openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[26];
		}

		if (folder_->goods().equipmentclazz() >0 && folder_->goods().equipmentclazz() <10)
		{
			if ( GameView::getInstance()->myplayer->getProfession() == folder_->goods().equipmentdetail().profession() && 
				folder_->goods().equipmentdetail().fightcapacity() > playerEquipFight_)
			{
				ShortCutItem * equip_wave = ShortCutItem::create(folder_,0);
				equip_wave->ignoreAnchorPointForPosition(false);
				equip_wave->setAnchorPoint(ccp(1.0f,0));
				equip_wave->setTag(501);
				GameView::getInstance()->getMainUIScene()->getMainUIElementLayer()->addChild(equip_wave);

				if (isHasLayer)
				{
					equip_wave->setPosition(ccp(winSize.width - 90,165));
				}else
				{
					equip_wave->setPosition(ccp(winSize.width+ equip_wave->getContentSize().width+50,165));	


					CCMoveTo * moveTo_ = CCMoveTo::create(0.5f,ccp(winSize.width - 90,165));
					CCEaseBackOut * easebackOut = CCEaseBackOut::create(moveTo_);
					equip_wave->runAction(easebackOut);
				}

				isCreate = true;
			}else
			{
				if (curRoleLevel_ >= openlevel)
				{
					bool isFind_ = false;
					for (int i = 0;i<GameView::getInstance()->generalsInLineList.size();i++)
					{
						if (isFind_ == true)
						{
							break;
						}

						for (int j = 0;j< GameView::getInstance()->generalsInLineDetailList.size();j++)
						{
							if (GameView::getInstance()->generalsInLineList.at(i)->id() == GameView::getInstance()->generalsInLineDetailList.at(j)->generalid())
							{
								int generalLevel_ = GameView::getInstance()->generalsInLineDetailList.at(j)->activerole().level();
								std::string generalPression_str = GameView::getInstance()->generalsInLineDetailList.at(j)->profession();
								int generalPression_id = BasePlayer::getProfessionIdxByName(generalPression_str.c_str());
								int generalEquipFight_ = 0;
								for (int index_ = 0;index_<GameView::getInstance()->generalsInLineDetailList.at(j)->equipments_size();index_++)
								{
									if (GameView::getInstance()->generalsInLineDetailList.at(j)->equipments(index_).goods().equipmentclazz() == folder_->goods().equipmentclazz())
									{
										generalEquipFight_ = GameView::getInstance()->generalsInLineDetailList.at(j)->equipments(index_).goods().equipmentdetail().fightcapacity();
									}
								}
								long long generalId_ = GameView::getInstance()->generalsInLineDetailList.at(j)->generalid();
								if ( generalPression_id == folder_->goods().equipmentdetail().profession() && 
									folder_->goods().equipmentdetail().fightcapacity() > generalEquipFight_)
								{
									ShortCutItem * equip_wave = ShortCutItem::create(folder_,generalId_);
									equip_wave->ignoreAnchorPointForPosition(false);
									equip_wave->setAnchorPoint(ccp(1.0f,0));
									//equip_wave->setPosition(ccp(winSize.width - 90,165));
									equip_wave->setTag(501);
									GameView::getInstance()->getMainUIScene()->getMainUIElementLayer()->addChild(equip_wave);

									if (isHasLayer)
									{
										equip_wave->setPosition(ccp(winSize.width - 90,165));
									}else
									{
										equip_wave->setPosition(ccp(winSize.width+ equip_wave->getContentSize().width+50,165));	

										CCMoveTo * moveTo_ = CCMoveTo::create(0.5f,ccp(winSize.width - 90,165));
										CCEaseBackOut * easebackOut = CCEaseBackOut::create(moveTo_);
										equip_wave->runAction(easebackOut);
									}

									isCreate = true;
									isFind_ = true;
									break;
								}
							}
						}
					}
				}
			}
		}else
		{
			ShortCutItem * equip_wave = ShortCutItem::create(folder_,0);
			equip_wave->ignoreAnchorPointForPosition(false);
			equip_wave->setAnchorPoint(ccp(1.0f,0));
			equip_wave->setTag(501);
			GameView::getInstance()->getMainUIScene()->getMainUIElementLayer()->addChild(equip_wave);

			if (isHasLayer)
			{
				equip_wave->setPosition(ccp(winSize.width - 90,165));
			}else
			{
				equip_wave->setPosition(ccp(winSize.width+ equip_wave->getContentSize().width+50,165));
				CCMoveTo * moveTo_ = CCMoveTo::create(0.5f,ccp(winSize.width - 90,165));
				CCEaseBackOut * easebackOut = CCEaseBackOut::create(moveTo_);
				equip_wave->runAction(easebackOut);
			}
			isCreate = true;
		}
		delete folder_;
		if (isCreate == true)
		{
			break;
		}
	}
}

void EffectEquipItem::refreshFolderIndex()
{
	for (int i = 0;i<folderItemVector.size();i++)
	{
		//std::string goodsId = folderItemVector.at(i)->goods().instanceid();
		bool isHas = false;
		for(int j = 0;j<GameView::getInstance()->AllPacItem.size();j++)
		{
			//strcmp(goodsId.c_str(),GameView::getInstance()->AllPacItem.at(j)->goods().id().c_str()) == 0
			if (GameView::getInstance()->AllPacItem.at(j)->has_goods() &&
				folderItemVector.at(i)->goods().instanceid() == GameView::getInstance()->AllPacItem.at(j)->goods().instanceid())
			{				
				folderItemVector.at(i)->set_id(GameView::getInstance()->AllPacItem.at(j)->id());
				folderItemVector.at(i)->set_quantity(GameView::getInstance()->AllPacItem.at(j)->quantity());
				isHas = true;
			}
		}
		if (isHas == false)
		{
			std::vector<FolderInfo *>::iterator iter_ = EffectEquipItem::folderItemVector.begin()+i;
			FolderInfo * iterFoler =  * iter_;
			EffectEquipItem::folderItemVector.erase(iter_);
			delete iterFoler;
		}
	}
}

void EffectEquipItem::starCallBack( CCObject * obj )
{
	//this->reloadShortItem();
}

std::vector<FolderInfo * > EffectEquipItem::folderItemVector;

///////////////////////////////////
EffectStudySkill::EffectStudySkill()
{

}

EffectStudySkill::~EffectStudySkill()
{

}

void EffectStudySkill::showEffect()
{
	std::string skillId = this->getRoleFirstSkillId();
	std::string skillName = this->getSkillName(skillId);
	std::string skillIcon = this->getSkillIcon(skillId);

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	GetSkillTeach * effect_skill = GetSkillTeach::create(skillId,skillName,skillIcon);
	effect_skill->ignoreAnchorPointForPosition(false);
	effect_skill->setAnchorPoint(ccp(0.5f,0.5f));
	effect_skill->setPosition(ccp(winSize.width/2,winSize.height/2));
	GameView::getInstance()->getMainUIScene()->addChild(effect_skill);

}

std::string EffectStudySkill::getRoleFirstSkillId()
{
	std::string skillId = "";
	if(GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_MJ_NAME)
	{
		skillId.append("AZ1ltnh");
	}
	else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_GM_NAME)
	{
		skillId.append("BZ1ssb");
	}
	else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_HJ_NAME)
	{
		skillId.append("CX1ycww");
	}
	else if (GameView::getInstance()->myplayer->getActiveRole()->profession() == PROFESSION_SS_NAME)
	{
		skillId.append("DX1lys");
	}
	return skillId;
}

std::string EffectStudySkill::getSkillName( std::string m_skillId )
{
	std::string m_skillName = "";
	std::map<std::string,CBaseSkill*>::const_iterator cIter;
	cIter = StaticDataBaseSkill::s_baseSkillData.find(m_skillId);
	if (cIter == StaticDataBaseSkill::s_baseSkillData.end())
	{
	}
	else
	{
		m_skillName = cIter->second->name();
		
	}
	return m_skillName;
}

std::string EffectStudySkill::getSkillIcon( std::string m_skillId )
{
	std::string m_skillIcon = "";
	std::map<std::string,CBaseSkill*>::const_iterator cIter;
	cIter = StaticDataBaseSkill::s_baseSkillData.find(m_skillId);
	if (cIter == StaticDataBaseSkill::s_baseSkillData.end())
	{
	}
	else
	{
		m_skillIcon = cIter->second->icon();
	}
	return m_skillIcon;
}

////////////////////////////////////
EffectRecruit::EffectRecruit()
{

}

EffectRecruit::~EffectRecruit()
{

}

void EffectRecruit::showEffect()
{
	CCSize winSize = CCDirector::sharedDirector()->getWinSize();

	RecruitGeneralCard * recuitCard_ = RecruitGeneralCard::create();
	recuitCard_->ignoreAnchorPointForPosition(false);
	recuitCard_->setAnchorPoint(ccp(0.5f,0.5f));
	recuitCard_->setPosition(ccp(winSize.width/2,winSize.height/2));
	recuitCard_->setTag(kTagRecuriteGeneralCardUI);
	GameView::getInstance()->getMainUIScene()->addChild(recuitCard_);
}

