﻿#ifndef _GAMESCENESTATE_GAMESCENEEFFECTS_H_
#define _GAMESCENESTATE_GAMESCENEEFFECTS_H_

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../messageclient/GameProtobuf.h"
#include "../messageclient/protobuf/ModelMessage.pb.h"
#include "../ui/extensions/uiscene.h"

USING_NS_CC;
USING_NS_CC_EXT;

USING_NS_THREEKINGDOMS_PROTOCOL;

/**
 * 该文件包含游戏场景用到的特效集合
 * 目前主要用于战斗过程中，角色头顶的战斗效果指示等等
 * @Author: zhaogang
 * 2013-12-26
 */

class GameSceneLayer;
class MyPlayer;

/**
 * 主角和武将之间的脚底链接特效
 */
class LinkSpecialEffect : public CCNode {
public:
	LinkSpecialEffect();
	virtual ~LinkSpecialEffect();

	virtual void update(float dt);

	static LinkSpecialEffect* create(const char* effectFileName, int maxLink = 3);
	bool init(const char* effectFileName, int maxLink);

	void setLinkNumber(int linkNum);
	int getLinkNumber();

	void updateLinkPosition(std::vector<CCPoint>& pointList);

private:
	void setLinkPosition(CCNode* effectNode, CCPoint startPoint, CCPoint endPoint);

private:
	int m_nMaxLink;
	int m_nCurLinkNum;
};

//////////////////////////////////////////////////////////////

/**
 * 全屏特效，用在无双技能的释放
 */
class MusouSpecialEffect : public CCNodeRGBA {
public:
	MusouSpecialEffect();
	virtual ~MusouSpecialEffect();

	static MusouSpecialEffect* create(CCPoint effectPosition);
	bool init(CCPoint effectPosition);

	inline float getDuration() { return m_duration; };

private:
	float m_duration;   // unit: second

private:
	void createBgAction(MyPlayer* pMyPlayer);
};

//////////////////////////////////////////////////////////////

/**
 * 类似火雨之类的特效
 * 在某个范围内，随机产生特效
 */
class RangeRandomSpecialEffect {
public:
	static void generate(const char* effectFileName, GameSceneLayer* scene, CCPoint center, CCSize area);
};


//////////////////////////////////////////////////////////////

/**
 * 主角和武将脚下的箭头
 * 目前箭头主要用于指向当前攻击对象
 */
class ArrowIndicator : public CCNodeRGBA {
public:
	ArrowIndicator();
	virtual ~ArrowIndicator();

	virtual void update(float dt);

	static ArrowIndicator* create(long long targetRoleId, float duration = 3.0f);
	bool init(long long targetRoleId, float duration);

	void setTarget(long long targetRoleId, float duration = 3.0f);

protected:
	void hide(float dt);

private:
	long long m_targetRoleId;
	float m_duration;
};

//////////////////////////////////////////////////////////////

/**
 * bonus special effect
 * where: new general card, skill level up, equipment level up etc.
 */
class BonusSpecialEffect : public CCNodeRGBA {
public:
	BonusSpecialEffect();
	virtual ~BonusSpecialEffect();

	static BonusSpecialEffect* create();
	bool init();
};

//////////////////////////////////////////////////////////////

class AptitudePromotionStruct {
public:
	int type;
	std::string aptitudeName;   // strength, maxHp, etc.
	int number;
	ccColor3B color;
};

/**
 * aptitude effect
 * if the player's aptitude has been promoted, the text and numbers will pop up
 * when equip new item, or updade the item, or level up, the aptitude will be promoted
 */
class AptitudePopupEffect : public CCNodeRGBA {
public:
	AptitudePopupEffect();
	virtual ~AptitudePopupEffect();

	static AptitudePopupEffect* create(Aptitude* oldAptitude, Aptitude* newAptitude);
	bool init(Aptitude* oldAptitude, Aptitude* newAptitude);

	float getActionTotalTime();
protected:
	void generateOneEffect(const char* effectContent, ccColor3B color, float delayTime = 0.0f);
	void handleAptitudePromotion(Aptitude* oldAptitude, Aptitude* newAptitude);

private:
	std::vector<AptitudePromotionStruct> m_allPromotions;
	int m_totleTime;
};

/**
 * numRoll effect
 */
class NumberRollEffect : public CCNodeRGBA {
public:
	NumberRollEffect();
	virtual ~NumberRollEffect();

	static NumberRollEffect* create(int maxNumber,const char * fontName = "res_ui/font/ziti_7.fnt",float totalTime = 1.5f,int startNumber = 0);
	bool init(int maxNumber,const char * fontName,float totalTime,int startNumber);

private:
	virtual void update(float dt);

private:
	CCLabelBMFont* pLabel;
	//最终停止的数字
	int m_nMaxNumber;
	//整个过程的总时间
	float m_fTotalTime;
	//开始滚动的数字
	int m_nStartNumber;

	float m_changeSpeed;
	int m_currentNumber;
};

/**
 *fight change
 */
class FightPointChangeEffect:public CCNodeRGBA
{
public:
	FightPointChangeEffect();
	virtual ~FightPointChangeEffect();

	static FightPointChangeEffect * create(int oldFight,int newFight);
	bool init(int oldFight,int newFight);
	void startChangeFightValue();
	void endChangeFightValue();
private:
	virtual void update(float delta);

	CCSize winSize;

	CCLabelBMFont * m_fightPointLabel;
	CCLabelTTF * label_changeValue;

	float m_fightChangeSpeed;
	int m_curShowFight;
	int m_newFightValue;

	int m_state;

	CCSprite * sp_fight;
	CCSprite * sp_;
};

////////////////////////////////////////////////////////////////////////////
/**
 * 体力条从零累加到当前体力值的动画效果（包含 体力值的累加动画效果）
 *
 * add by LiuLiang
 */

class PhysicalBarEffect : public CCNode
{
public:
	PhysicalBarEffect();
	~PhysicalBarEffect();

public:
	static PhysicalBarEffect* create(int nTargetPhy, int nMaxPhy, float duration, UIImageView* pImageView, UILabelBMFont* pLabel, float delay = 0.0f);
	bool init(int nTargetPhy, int nMaxPhy, float ToatalDuration, UIImageView* pImageView, UILabelBMFont* pLabel, float delay = 0.0f);

	static PhysicalBarEffect* create(int nTargetPhy, int nMaxPhy, float duration, UILabelBMFont* pLabel);
	bool init(int nTargetPhy, int nMaxPhy, float ToatalDuration, UILabelBMFont* pLabel);

private:
	virtual void update(float dt);

	void updateForLabel(float dt);

private:
	int m_nMaxNumber;			// 最大值（最大值 可能比 targetNumber小）
	int m_nTargetNumber;		// 最终停止的数字
	
	float m_fTotalTime;			// 整个过程的总时间

	float m_changeSpeed;		// speed
	int m_currentNumber;		// 当前数字
	// 
	float m_fDelayTime;			// 延时X秒后开始执行动作
	float m_delte;
	
	
	UIImageView* m_pImageView;
	UILabelBMFont* m_pLabel;
};



//////////////////////////////////////////////////////////////

/**
 * 得到经验特效
 */
class GetExpEffect : public UIScene {
public:
	GetExpEffect();
	virtual ~GetExpEffect();

	static GetExpEffect* create(int curExp,int nexExp,bool isLevelUp = false);
	bool init(int curExp,int nexExp,bool isLevelUp);

};


#endif