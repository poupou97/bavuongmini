#include "ExStatusImmobilize.h"

#include "../../utils/StaticDataManager.h"
#include "../role/ActorCommand.h"
#include "../skill/processor/SkillProcessor.h"
	
ExStatusImmobilize::ExStatusImmobilize() 
{
}

ExStatusImmobilize::~ExStatusImmobilize(){
	
}

void ExStatusImmobilize::onAdd(ExtStatusInfo* info) {
	CCAssert(mFighter != NULL, "mFighter should not be null, please setFighter()");

	this->addExstatusName();

	this->addSpecialEffect();

	// stop moving
	if(mFighter->isMoving())
	{
		mFighter->changeAction(ACT_STAND);
	}
	// shake the body
	SkillProcessor::shake(mFighter, true, 18);
}

void ExStatusImmobilize::onCancel() 
{
	this->removeSpecialEffect();
}