#include "ExStatusInvisible.h"

#include "../../GameView.h"
#include "../role/MyPlayer.h"
#include "../GameSceneState.h"
#include "ExStatusType.h"
#include "../role/OtherPlayer.h"
#include "../role/BaseFighterConstant.h"
	
ExStatusInvisible::ExStatusInvisible() {
}

ExStatusInvisible::~ExStatusInvisible(){
}

void ExStatusInvisible::onAdd(ExtStatusInfo* info) {
	CCAssert(mFighter != NULL, "mFighter should not be null, please setFighter()");

	if(mFighter->isMyPlayerGroup())
	{
		//mFighter->setOpacity(128);

		// when invisble, the player will flash
		CCFadeTo*  action1 = CCFadeTo::create(0.25f,16);
		CCFadeTo*  action2 = CCFadeTo::create(0.25f,128);
		//CCRepeatForever * repeapAction = CCRepeatForever::create(
		//	CCSequence::create(action1,action2,NULL));
		CCRepeat * repeapAction = CCRepeat::create(
			CCSequence::create(action1,action2,NULL), 15.0f / (0.25f + 0.25f));   // 15.0f maybe the max invisible time
		repeapAction->setTag(BASEFIGHTER_INVISIBLE_ACTION);
		mFighter->runAction(repeapAction);
	}
	else
	{
		//mFighter->setVisible(false);
		mFighter->setOpacity(1);   // 1 is so small, indicate invisible

		// 清除原来的选中状态
		MyPlayer* me = GameView::getInstance()->myplayer;
		if(mFighter->getRoleId() == me->getLockedActorId())
			me->setLockedActorId(NULL_ROLE_ID);

		// 清除红圈
		mFighter->removeDangerCircle();

		// 若其他玩家处于龙魂状态，则停止龙魂特效
		OtherPlayer* pOther = dynamic_cast<OtherPlayer*>(mFighter);
		if(pOther != NULL)
		{
			if(pOther->hasExStatus(ExStatusType::musou))
			{
				pOther->onRemoveMusouEffect();
			}
		}
	}
}

void ExStatusInvisible::onCancel() {
	if(mFighter != NULL)
	{
		if(!mFighter->isMyPlayerGroup())
		{
			mFighter->changeAction(ACT_STAND);
		}

		if(!mFighter->isDead())
		{
			mFighter->setOpacity(255);
			if(mFighter->isMyPlayerGroup())
			{
				mFighter->stopActionByTag(BASEFIGHTER_INVISIBLE_ACTION);
			}
		}

		// 若其他玩家处于龙魂状态，则恢复龙魂特效
		OtherPlayer* pOther = dynamic_cast<OtherPlayer*>(mFighter);
		if(pOther != NULL)
		{
			if(pOther->hasExStatus(ExStatusType::musou))
			{
				pOther->onAddMusouEffect();
			}
		}
	}
}