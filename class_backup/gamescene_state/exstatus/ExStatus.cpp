#include "ExStatus.h"

#include "../../legend_engine/CCLegendAnimation.h"
#include "../../utils/StaticDataManager.h"
#include "AppMacros.h"
#include "../GameSceneState.h"
	
ExStatus::ExStatus() 
: mFighter(NULL)
, m_bEffectLoop(true)
, bIsFootEffect(false)
{
}

ExStatus::~ExStatus(){
}

void ExStatus::onAdd(ExtStatusInfo* info)
{
	this->addExstatusName();

	this->addSpecialEffect();
}

void ExStatus::onCancel()
{
	this->removeSpecialEffect();	
}

std::string ExStatus::getIcon()
{
	return icon;
}

void ExStatus::setIcon( std::string _icon )
{
	icon = _icon;
}

std::string ExStatus::getName()
{
	return name; 
}

void ExStatus::setName( std::string _name )
{
	name = _name;
}

std::string ExStatus::getDescription()
{
	return description; 
}

void ExStatus::setDescription( std::string _des )
{
	description = _des;
}

void ExStatus::addSpecialEffect()
{
	ExStatus* statusData = ExStatusConfigData::s_exstatusData[this->type()];
	if(statusData == NULL)
		return;

	// no effect
	if(statusData->getEffect() == "")
		return;

	// 特效是否在脚下
	bool bFoot = statusData->isFootEffect();
	int containerTag = BaseFighter::kTagHeadEffectContainer;
	if(bFoot)
		containerTag = BaseFighter::kTagFootEffectContainer;

	CCNode* effectNode = mFighter->getChildByTag(containerTag)->getChildByTag(BaseFighter::kTagExStatusEffect + this->type());
	if(effectNode != NULL)
		return;

	std::string effectAnimName = statusData->getEffect();
	if(effectAnimName == "null" || effectAnimName == "")
		return;
	CCLegendAnimation* pAnimNode = CCLegendAnimation::create(effectAnimName);
	if(statusData->isEffectLoop())
	{
		pAnimNode->setPlayLoop(true);
		pAnimNode->setReleaseWhenStop(false);
	}
	pAnimNode->setTag(BaseFighter::kTagExStatusEffect + this->type());
	mFighter->addEffect(pAnimNode, bFoot, 0);
}

void ExStatus::removeSpecialEffect()
{
	if(mFighter != NULL)
	{
		// 特效是否在脚下
		bool bFoot = bIsFootEffect;
		ExStatus* statusData = ExStatusConfigData::s_exstatusData[this->type()];
		if(statusData != NULL)
		{
			bFoot = statusData->isFootEffect();
		}
		int containerTag = BaseFighter::kTagHeadEffectContainer;
		if(bFoot)
			containerTag = BaseFighter::kTagFootEffectContainer;

		CCNode* effectNode = mFighter->getChildByTag(containerTag)->getChildByTag(BaseFighter::kTagExStatusEffect + this->type());
		if(effectNode != NULL)
		{
			effectNode->runAction(CCSequence::create(
				CCFadeOut::create(0.8f),
				CCRemoveSelf::create(),
				NULL));
			//effectNode->removeFromParent();
		}
	}
}

void ExStatus::addExstatusName()
{
	ExStatus* statusData = ExStatusConfigData::s_exstatusData[this->type()];
	if(statusData != NULL && statusData->getEffect() != "")
	{
		if(mFighter->hasExStatus(this->type()))
			return;

		//CCLabelTTF* pNameLabel= CCLabelTTF::create(statusData->getName().c_str(),APP_FONT_NAME,20);
		//pNameLabel->setColor(ccc3(255, 255, 0));
		//pNameLabel->setAnchorPoint(ccp(0.5f,0.0f));

		std::string buffSpriteName = "res_ui/buff/buff";
		char strType[5];
		sprintf(strType, "%d", this->type());
		buffSpriteName.append(strType);
		buffSpriteName.append(".png");
		CCSprite* pNameLabel = CCSprite::create(buffSpriteName.c_str());
		if (pNameLabel)
		{
			CCAction*  action = CCSequence::create(
				CCMoveBy::create(0.5f, ccp(0, 30)),
				CCDelayTime::create(1.0f),
				CCFadeOut::create(0.5f),
				CCRemoveSelf::create(),
				NULL);
			pNameLabel->runAction(action);

			pNameLabel->setScale(1.2f);
			pNameLabel->setPosition(ccp(mFighter->getPositionX(), mFighter->getPositionY()));
			mFighter->getGameScene()->getActorLayer()->addChild(pNameLabel, SCENE_TOP_LAYER_BASE_ZORDER);
		}
	}
}

int ExStatus::getFunctionType()
{
	return function_type;
}

void ExStatus::setFunctionType( int type )
{
	function_type = type;
}
