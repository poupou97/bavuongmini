#include "ExStatusMusou.h"

#include "../role/BasePlayer.h"
#include "ExStatusType.h"
#include "../sceneelement/shortcutelement/ShortcutLayer.h"
#include "../../GameView.h"
#include "../role/MyPlayer.h"
#include "../role/General.h"
#include "../role/ActorUtils.h"
#include "../GameSceneState.h"
	
ExStatusMusou::ExStatusMusou() {
}

ExStatusMusou::~ExStatusMusou(){
}

void ExStatusMusou::onAdd(ExtStatusInfo* info) {
	CCAssert(mFighter != NULL, "mFighter should not be null, please setFighter()");

	// the musou skill is not open, so ignore
	int musouSkillOpenLevel = ShortcutSlotOpenLevelConfigData::s_shortcutSlotOpenLevelMsg[7];
	if (GameView::getInstance()->myplayer->getActiveRole()->level() < musouSkillOpenLevel)
		return;

	// effect on body for the BasePlayer
	BasePlayer* master = dynamic_cast<BasePlayer*>(mFighter);
	if(master != NULL && !master->hasExStatus(ExStatusType::musou))
	{
		// 更新武将列表
		// 补救措施，以解决bug: 其他玩家的武将列表未及时更新
		master->updateAliveGeneralsList();

		// add link effect
		master->onAddMusouEffect();
	}

	// effect on body for the General
	General* pGeneral = dynamic_cast<General*>(mFighter);
	if(pGeneral != NULL)
	{
		BasePlayer* pPlayer = dynamic_cast<BasePlayer*>(pGeneral->getGameScene()->getActor(pGeneral->getOwnerId()));
		if(pPlayer != NULL && !pGeneral->hasExStatus(ExStatusType::musou))
		{
			ActorUtils::addMusouBodyEffect(mFighter, pPlayer->getColorByProfession(pPlayer->getProfession()));
		}
	}
}

void ExStatusMusou::onCancel() {
	if(mFighter != NULL)
	{
		// remove link effect
		BasePlayer* master = dynamic_cast<BasePlayer*>(mFighter);
		if(master != NULL)
			master->onRemoveMusouEffect();

		// remove effect on body
		int effectTag = BaseFighter::kTagMosouEffect+mFighter->getRoleId();
		CCNode* container = mFighter->getChildByTag(BaseFighter::kTagFootEffectContainer);
		CCNode* effect = container->getChildByTag(effectTag);
		if(effect != NULL)
			effect->removeFromParent();
	}
}