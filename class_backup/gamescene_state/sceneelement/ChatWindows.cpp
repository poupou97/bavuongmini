#include "ChatWindows.h"
#include "../../ui/extensions/CCMoveableMenu.h"
#include "../../ui/extensions/RichTextInput.h"
#include "../../ui/Chat_ui/ChatUI.h"
#include "../GameSceneState.h"
#include "GameView.h"
#include "../../ui/Chat_ui/ChatCell.h"
#include "../../ui/extensions/CCRichLabel.h"
#include "../MainScene.h"
#include "../../utils/StaticDataManager.h"
#include "../../ui/extensions/UITab.h"
#include "../../ui/Chat_ui/PrivateChatUi.h"
#include "../../legend_script/CCTutorialParticle.h"

#define CHATWINDOW_PREFERRED_WIDTH 260   // 330
#define CHATWINDOW_PREFERRED_HEIGHT 60
#define CHATWINDOW_MAX_HEIGHT 66

ChatWindows::ChatWindows(void)
{
	showMainScene=true;
	m_chatWindowsState = chatWindowsState_showUp;
	remTime = 45;
	time_=0;
}

ChatWindows::~ChatWindows(void)
{
}

ChatWindows * ChatWindows::create()
{
	ChatWindows *chatWindows=new ChatWindows();
	if (chatWindows && chatWindows->init())
	{
		chatWindows->autorelease();
		return chatWindows;
	}
	CC_SAFE_DELETE(chatWindows);
	return NULL;

}
bool ChatWindows::init()
{
	if (UIScene::init())
	{
		CCSize winSize=CCDirector::sharedDirector()->getVisibleSize();
		UILayer * m_Layer= UILayer::create();
		m_Layer->ignoreAnchorPointForPosition(false);
		m_Layer->setAnchorPoint(ccp(0.5f,0.5f));
		m_Layer->setPosition(ccp(winSize.width/2,winSize.height/2));
		m_Layer->setContentSize(CCSizeMake(UI_DESIGN_RESOLUTION_WIDTH, UI_DESIGN_RESOLUTION_HEIGHT));
		this->addChild(m_Layer);
		
		pImageView_chatBg = CCScale9Sprite::create(CCRectMake(7, 7, 1, 1) , "res_ui/zhezhao70.png");
		pImageView_chatBg->setOpacity(160);
		pImageView_chatBg->setPreferredSize(CCSize(CHATWINDOW_PREFERRED_WIDTH, CHATWINDOW_PREFERRED_HEIGHT));
		pImageView_chatBg->setAnchorPoint(CCPointZero);
		//pImageView_chatBg->setPosition(ccp(58,3));   // 0
		pImageView_chatBg->setPosition(ccp(0,3));
		addChild(pImageView_chatBg);

		tableView = CCTableView::create(this, CCSizeMake(255, 75));  //390,75
		tableView->setDirection(kCCScrollViewDirectionVertical);
		tableView->setAnchorPoint(ccp(0,0));
		//tableView->setPosition(ccp(63,0));   // 60
		tableView->setPosition(ccp(5,0));   // 60
		tableView->setDelegate(this);
		tableView->setVerticalFillOrder(kCCTableViewFillTopDown);
		tableView->setTouchEnabled(false);
		addChild(tableView, 1,TABVIEWCHATWINDOS_TAG);
		tableView->reloadData();

		schedule(schedule_selector(ChatWindows::showMainUi),0.5f);
		
		setTouchEnabled(true);
		setContentSize(CCSizeMake(CHATWINDOW_PREFERRED_WIDTH,CHATWINDOW_PREFERRED_HEIGHT));
		return true;
	}
	return false;
}
void ChatWindows::onEnter()
{
	UIScene::onEnter();
}
void ChatWindows::onExit()
{
	UIScene::onExit();
}

void ChatWindows::callBack( CCObject * obj )
{
	CCSize s=CCDirector::sharedDirector()->getVisibleSize();
	ChatUI * chat_ui = ChatUI::create();
	chat_ui->ignoreAnchorPointForPosition(false);
	chat_ui->setAnchorPoint(ccp(0.5f, 0.5f));
	chat_ui->setPosition(ccp(s.width/2,s.height/2));
	this->getParent()->addChild(chat_ui,0,kTabChat);
}

void ChatWindows::tableCellTouched( cocos2d::extension::CCTableView* table, cocos2d::extension::CCTableViewCell* cell )
{
	CCLOG("cell touched at index: %i", cell->getIdx());
}

cocos2d::CCSize ChatWindows::tableCellSizeForIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	int height= s_chatCellsDataVector.at(idx)->getContentSize().height*0.6f;
	return CCSizeMake(400,height);  //400,height
}

cocos2d::extension::CCTableViewCell* ChatWindows::tableCellAtIndex( cocos2d::extension::CCTableView *table, unsigned int idx )
{
	CCTableViewCell *cell =table->dequeueCell();   // this method must be called
	cell = s_chatCellsDataVector.at(idx);
	cell->setScale(0.6f);
	return cell;
}

unsigned int ChatWindows::numberOfCellsInTableView( cocos2d::extension::CCTableView *table )
{
	int cellLine=s_chatCellsDataVector.size();
	return cellLine;
}

bool ChatWindows::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return false;
}

void ChatWindows::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ChatWindows::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ChatWindows::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ChatWindows::scrollViewDidScroll( cocos2d::extension::CCScrollView* view )
{

}

void ChatWindows::scrollViewDidZoom( cocos2d::extension::CCScrollView* view )
{

}


void ChatWindows::showMainUi( float delta )
{
	remTime-=delta;
	MainScene * mainScene =(MainScene *)GameView::getInstance()->getMainUIScene();
	if (mainScene == NULL)
	{
		return;
	}
	UIButton * btn_chatBubble = NULL;
	if (mainScene->chatLayer->getChildByTag(100))//#define BUTTONLAYER_TAG = 100 is mainscene define
	{
		UILayer * layer_ = (UILayer *)mainScene->chatLayer->getChildByTag(100);
		if (layer_->getWidgetByTag(101))
		{
			btn_chatBubble = (UIButton *)layer_->getWidgetByTag(101);//#define CHATBUTTON_TAG = 101 is mainscene define
		}
	}

	if (remTime <= 0)
	{
		remTime = 0;
		setShowMainScene(NULL);

		//set chatBtton is setOpacity
		if (btn_chatBubble)
		{
			btn_chatBubble->setOpacity(255);
		}
		
	}else
	{
		switch (this->getChatWindowsState())
		{
		case chatWindowsState_show:
			{
				//set chatBtton is setOpacity
				if (btn_chatBubble)
				{
					btn_chatBubble->setOpacity(100);
				}
				return;
			}break;
		case chatWindowsState_showUp:
			{
				if (showMainScene == true )
				{
					if (mainScene->isHeadMenuOn==true)
					{
						this->runAction(CCMoveTo::create(0.5f,ccp(this->getPositionX(),this->getContentSize().height/2+10)));
					}else
					{
						this->runAction(CCMoveTo::create(0.5f,ccp(this->getPositionX(),this->getContentSize().height/2+10)));
					}
					this->setChatWindowsState(chatWindowsState_show);
				}
			};
		}
	}

	

	/*
	MainScene * mainScene =(MainScene *)this->getParent();
	if (showMainScene==true )
	{
		if (this->getActionByTag(2204) != NULL)
		{
			return;
		}
		
		if (mainScene->isHeadMenuOn==true)
		{
			CCActionInterval * action =(CCActionInterval *)CCSequence::create(
				CCMoveTo::create(0.5f,ccp(this->getPositionX(),77)),
				CCDelayTime::create(45.0f),
				CCCallFuncO::create(this,callfuncO_selector(ChatWindows::setShowMainScene),NULL),
				NULL);
			action->setTag(2204);
			this->runAction(action);
		}else
		{
			CCActionInterval * action =(CCActionInterval *)CCSequence::create(
				CCMoveTo::create(0.5f,ccp(this->getPositionX(),8)),
				CCDelayTime::create(45.0f),
				CCCallFuncO::create(this,callfuncO_selector(ChatWindows::setShowMainScene),NULL),
					NULL);
			action->setTag(2204);
			this->runAction(action);
		}
	}
	*/
}

void ChatWindows::setShowMainScene(CCObject * obj)
{
	this->runAction(CCMoveTo::create(0.5f,ccp(this->getPositionX(),-122)));	
	showMainScene=false;
	this->setChatWindowsState(chatWindowsState_showDown);
}

void ChatWindows::chatPrivate( CCObject * obj )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	int m_vectorSize = GameView::getInstance()->chatPrivateSpeakerVector.size();
	PrivateChatUi * privateUi = PrivateChatUi::create(m_vectorSize - 1);
	privateUi->ignoreAnchorPointForPosition(false);
	privateUi->setAnchorPoint(ccp(0.5f,0.5f));
	privateUi->setPosition(ccp(winSize.width/2,winSize.height/2));
	privateUi->setTag(ktagPrivateUI);
	GameView::getInstance()->getMainUIScene()->addChild(privateUi);
	
}

std::vector<ChatCell *> ChatWindows::s_chatCellsDataVector;

void ChatWindows::addToMiniChatWindow( int channel_id,std::string play_name,std::string player_country,std::string describe,long long playerid,bool isMainChat,int viplevel,long long listenerId,std::string listenerName )
{
	GameSceneLayer* scene = GameView::getInstance()->getGameScene();
	if(scene == NULL)
		return;

	// when the chat main window is opened, ignore the mini chat window at the bottom of the main ui interface
	MainScene* mainUILayer = scene->getMainUIScene();
	if (strlen(describe.c_str()) > 0)
	{
		ChatCell * chatCellSmall = ChatCell::create(channel_id,play_name,player_country, describe, playerid,isMainChat,viplevel,listenerId,listenerName);
		chatCellSmall->retain();
		ChatWindows::s_chatCellsDataVector.push_back(chatCellSmall);

		int chatOfWindowsCellLine = 0;
		bool firstFlag = true;
		int chatCellSizeOfChatWindow = ChatWindows::s_chatCellsDataVector.size();
		for (int i=chatCellSizeOfChatWindow;i > 0; i--)
		{
			ChatCell * chatcell_ = (ChatCell *)ChatWindows::s_chatCellsDataVector.at(i - 1);
			chatOfWindowsCellLine += chatcell_->getOneCellOfLine();

			if (chatOfWindowsCellLine > 4 && i != ChatWindows::s_chatCellsDataVector.size())
			{
				std::vector<ChatCell *>::iterator iterCell= ChatWindows::s_chatCellsDataVector.begin()+ (i-1);
				ChatCell * cellv_=* iterCell;
				ChatWindows::s_chatCellsDataVector.erase(iterCell);
				cellv_->release();
			}
		}

		ChatWindows * chatWindows=(ChatWindows *)mainUILayer->chatLayer->getChildByTag(kTagChatWindows);
		if (chatWindows == NULL)
		{
			return;
		}
		chatWindows->showMainScene = true;

		switch(chatWindows->getChatWindowsState())
		{
		case chatWindowsState_showDown:
			{
				chatWindows->setChatWindowsState(chatWindowsState_showUp);
			}break;
		case chatWindowsState_show:
			{
				//keep show
			}break;

		}
		chatWindows->remTime = 45.0f;

		CCTableView* tableViewWindows = (CCTableView*)chatWindows->getChildByTag(TABVIEWCHATWINDOS_TAG);
		tableViewWindows->reloadData();

		if(tableViewWindows->getContentSize().height >= CHATWINDOW_PREFERRED_HEIGHT)
		{
			chatWindows->pImageView_chatBg->setPreferredSize(CCSizeMake(CHATWINDOW_PREFERRED_WIDTH,tableViewWindows->getContentSize().height+6));
			tableViewWindows->setPositionY(tableViewWindows->getContentSize().height - tableViewWindows->getViewSize().height+6);
			
			int temp_x = mainUILayer->btnRemin->getPosition().x;
			//int temp_y = 120+ tableViewWindows->getContentSize().height - CHATWINDOW_PREFERRED_HEIGHT;
			int temp_y = 130;

			mainUILayer->btnRemin->setPosition(ccp(temp_x,temp_y));
			mainUILayer->btnReminOfNewMessageParticle->setPosition(ccp(mainUILayer->btnRemin->getPosition().x,
										mainUILayer->btnRemin->getPosition().y - mainUILayer->btnRemin->getContentSize().height/2));
										
			mainUILayer->btnreminRepair->setPosition(ccp(mainUILayer->btnRemin->getPosition().x +mainUILayer->btnRemin->getContentSize().width/2 + mainUILayer->btnreminRepair->getContentSize().width/2+10,
												mainUILayer->btnRemin->getPosition().y));
		}else
		{
			chatWindows->pImageView_chatBg->setPreferredSize(CCSizeMake(CHATWINDOW_PREFERRED_WIDTH,CHATWINDOW_MAX_HEIGHT));
			tableViewWindows->setPositionY(CHATWINDOW_PREFERRED_HEIGHT- tableViewWindows->getViewSize().height+6);
			
			int temp_x = mainUILayer->btnRemin->getPosition().x;
			int temp_y = 130;

			mainUILayer->btnRemin->setPosition(ccp(temp_x,temp_y));
			mainUILayer->btnReminOfNewMessageParticle->setPosition(ccp(mainUILayer->btnRemin->getPosition().x,
						mainUILayer->btnRemin->getPosition().y - mainUILayer->btnRemin->getContentSize().height/2));
						
			mainUILayer->btnreminRepair->setPosition(ccp(mainUILayer->btnRemin->getPosition().x +mainUILayer->btnRemin->getContentSize().width/2 + mainUILayer->btnreminRepair->getContentSize().width/2+10,
				mainUILayer->btnRemin->getPosition().y));
		}

		if(tableViewWindows->getContentSize().height > tableViewWindows->getViewSize().height)
		{
			tableViewWindows->setContentOffset(tableViewWindows->maxContainerOffset(), false);
		}
	}
}

void ChatWindows::setChatWindowsState( int value_ )
{
	m_chatWindowsState = value_;
}

int ChatWindows::getChatWindowsState()
{
	return m_chatWindowsState;
}
