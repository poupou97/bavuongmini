#include "ShowTeamPlayer.h"
#include "../../messageclient/element/CTeamMember.h"
#include "../../ui/Friend_ui/TeamMemberList.h"
#include "../GameSceneState.h"
#include "GameView.h"
#include "../role/GameActor.h"
#include "../role/MyPlayer.h"
#include "../MainScene.h"
#include "../role/BasePlayer.h"
#include "../../utils/GameUtils.h"
#include "AppMacros.h"


ShowTeamPlayer::ShowTeamPlayer(void)
{
}


ShowTeamPlayer::~ShowTeamPlayer(void)
{
	
}

ShowTeamPlayer * ShowTeamPlayer::create(CTeamMember * members,long long leader)
{
	ShowTeamPlayer * teamplayers=new ShowTeamPlayer();
	if (teamplayers && teamplayers->init(members,leader))
	{
		teamplayers->autorelease();
		return teamplayers;
	}
	CC_SAFE_DELETE(teamplayers);
	return NULL;
}

bool ShowTeamPlayer::init(CTeamMember * members,long long leader)
{
	if (UIScene::init())
	{
		m_pUiLayer->setSwallowsTouches(false);
		
		CCSprite * playerBg = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/team_di.png");
		playerBg->setAnchorPoint(ccp(0,0));
		playerBg->setPosition(ccp(0,-3));
		addChild(playerBg);

		int pression_ = members->profession();
		std::string icon_path =  BasePlayer::getHeadPathByProfession(pression_);
		CCSprite * playerHead = CCSprite::create(icon_path.c_str());
		playerHead->setAnchorPoint(ccp(0,0));
		playerHead->setPosition(ccp(3,3));
		playerHead->setScale(0.6f);
		playerBg->addChild(playerHead);

		int memberHp_ = members->hp();
		int memberMaxHp_ = members->maxhp();
		float scaleHp = (float)memberHp_/memberMaxHp_;
		CCSprite * sp_hp = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/team_hp.png");
		sp_hp->setAnchorPoint(ccp(0,0));
		sp_hp->setPosition(ccp(43,13));
		sp_hp->setScaleX(scaleHp);
		playerBg->addChild(sp_hp);

		int memberMp_ = members->mp();
		int memberMaxMp_ = members->maxmp();
		float scaleMp = (float)memberMp_/memberMaxMp_;
		CCSprite * sp_mp = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/team_mp.png");
		sp_mp->setAnchorPoint(ccp(0,0));
		sp_mp->setPosition(ccp(42,6));
		sp_mp->setScaleX(scaleMp);
		playerBg->addChild(sp_mp);

		CCSprite * headFrame = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/aa.png");
		headFrame->setAnchorPoint(ccp(1,0));
		headFrame->setPosition(ccp(playerBg->getContentSize().width,3));
		playerBg->addChild(headFrame);

		CCSprite * headLevelSp = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/lv_di.png");
		headLevelSp->setAnchorPoint(ccp(0.5f,0.5f));
		headLevelSp->setPosition(ccp(36,27));
		playerBg->addChild(headLevelSp);

		int memberLevel = members->level();
		char LevelStr[10];
		sprintf(LevelStr,"%d",memberLevel);
		CCLabelTTF *label_level = CCLabelTTF::create(LevelStr, APP_FONT_NAME, 14);
		label_level->setAnchorPoint(ccp(0.5f,0.5f));
		label_level->setPosition(ccp(headLevelSp->getContentSize().width/2,headLevelSp->getContentSize().height/2));
		headLevelSp->addChild(label_level);

		CCLabelTTF *label_name = CCLabelTTF::create(members->name().c_str(), APP_FONT_NAME, 14);
		label_name->setAnchorPoint(ccp(0.5f,0));
		label_name->setPosition(ccp(34+(playerBg->getContentSize().width-34)/2,21));
		playerBg->addChild(label_name);

		int vipLevel_ = members->viplevel();
		CCSprite * vip_node;
		if (vipLevel_ > 0)
		{
			vip_node =(CCSprite *)MainScene::addVipInfoByLevelForNode(vipLevel_);
			vip_node->setScale(0.6f);
			playerBg->addChild(vip_node);
			vip_node->setPosition(ccp(label_name->getPosition().x + label_name->getContentSize().width/2 + vip_node->getContentSize().width/2,
										playerBg->getContentSize().height/2 + 11));
		}
		CCSprite * leaderIcon_;
		if (members->roleid() == leader)
		{
			leaderIcon_ = CCSprite::create("res_ui/dui.png");
			leaderIcon_->setAnchorPoint(ccp(0,0));
			leaderIcon_->setPosition(ccp(0,18));
			playerBg->addChild(leaderIcon_);
		}
		CCSprite * playerBgOnLine = CCSprite::create("gamescene_state/zhujiemian3/renwuduiwu/team_mengban.png");
		playerBgOnLine->setAnchorPoint(ccp(0,0));
		playerBgOnLine->setPosition(ccp(0,-3));
		addChild(playerBgOnLine);
		//is not onLine
		if (members->isonline() == 1)
		{
			playerBgOnLine->setVisible(false);
		}else
		{
			playerBgOnLine->setVisible(true);
		}

		this->ignoreAnchorPointForPosition(false);
		this->setAnchorPoint(ccp(0,0));
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(CCSizeMake(playerBg->getContentSize().width,playerBg->getContentSize().height));
		return true;
	}
	return false;
}

void ShowTeamPlayer::onEnter()
{
	UIScene::onEnter();
}

void ShowTeamPlayer::onExit()
{
	UIScene::onExit();
}

void ShowTeamPlayer::playerInfo( CCObject * obj )
{
	/*
	MainScene * mainscene =(MainScene *)GameView::getInstance()->getMainUIScene();
	CCSize winsize =CCDirector::sharedDirector()->getVisibleSize();
	long long selfId =GameView::getInstance()->myplayer->getRoleId();
	long long numIdx_ =m_member->roleid();
	if (selfId != numIdx_)
	{
		TeamMemberListOperator * teamlistPlayer =TeamMemberListOperator::create(m_member,m_leader);
		teamlistPlayer->setAnchorPoint(ccp(0,0));
		teamlistPlayer->setPosition(ccp(mainscene->getChildByTag(kTagMissionAndTeam)->getPositionX()+this->getContentSize().width+70,
												winsize.height/2+teamlistPlayer->getContentSize().height/2));
		//teamlistPlayer->setPosition(ccp(100,450));
		teamlistPlayer->setZOrder(10);
		mainscene->addChild(teamlistPlayer);
	}
	*/
}

bool ShowTeamPlayer::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return false;
}

void ShowTeamPlayer::ccTouchMoved( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ShowTeamPlayer::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{

}

void ShowTeamPlayer::ccTouchCancelled( CCTouch *pTouch, CCEvent *pEvent )
{

}
