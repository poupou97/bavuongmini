#include "NewFunctionRemindUI.h"
#include "../../GameView.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "AppMacros.h"
#include "../../messageclient/element/CNewFunctionRemind.h"
#include "../../utils/StaticDataManager.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../MainScene.h"
#include "../../ui/generals_ui/GeneralsUI.h"
#include "../../ui/generals_ui/RecuriteActionItem.h"
#include "../../ui/generals_ui/GeneralsListUI.h"
#include "../../ui/generals_ui/GeneralsRecuriteUI.h"
#include "../../utils/GameUtils.h"
#include "../../ui/skillscene/SkillScene.h"
#include "../../ui/Active_ui/WorldBoss_ui/WorldBossUI.h"

NewFunctionRemindUI::NewFunctionRemindUI():
time(0)
{
}


NewFunctionRemindUI::~NewFunctionRemindUI()
{
}

NewFunctionRemindUI* NewFunctionRemindUI::create(int level)
{
	NewFunctionRemindUI * newFunctionRemindUI = new NewFunctionRemindUI();
	if(newFunctionRemindUI && newFunctionRemindUI->init(level))
	{
		newFunctionRemindUI->autorelease();
		return newFunctionRemindUI;
	}
	CC_SAFE_DELETE(newFunctionRemindUI);
	return NULL;
}

bool NewFunctionRemindUI::init(int level)
{
	if (UIScene::init())
	{
		std::map<int, std::vector<CNewFunctionRemind*> >::const_iterator cIter;
		cIter = NewFunctionRemindConfigData::s_newFunctionRemindList.find(level);
		if (cIter == NewFunctionRemindConfigData::s_newFunctionRemindList.end()) // 没找到就是指向END了  
		{
			return false;
		}
		else
		{
			for(int i = 0;i<NewFunctionRemindConfigData::s_newFunctionRemindList[level].size();++i)
			{
				int id = NewFunctionRemindConfigData::s_newFunctionRemindList[level].at(i)->getId();
				std::string str_iconPath =  NewFunctionRemindConfigData::s_newFunctionRemindList[level].at(i)->getIcon();
				newFunctionList.insert(make_pair(id,str_iconPath));
			}
		}

		if (newFunctionList.size() <= 0)
			return false;

		CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
		UIImageView *mengban = UIImageView::create();
		mengban->setTexture("res_ui/zhezhao80.png");
		mengban->setScale9Enable(true);
		mengban->setScale9Size(winSize);
		mengban->setAnchorPoint(ccp(.5f,.5f));
		mengban->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(mengban);
		
		//light
		UIImageView * imageView_light = UIImageView::create();
		imageView_light->setTexture("gamescene_state/zhujiemian3/newFunction/yellowLight.png");
		imageView_light->setScale9Enable(true);
		imageView_light->setScale9Size(CCSizeMake(272,94));
		imageView_light->setAnchorPoint(ccp(0,0));
		imageView_light->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(imageView_light);
		//bg
		UIImageView * imageView_bg = UIImageView::create();
		imageView_bg->setTexture("gamescene_state/zhujiemian3/newFunction/di.png");
		imageView_bg->setAnchorPoint(ccp(0,0));
		imageView_bg->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(imageView_bg);
		mengban->setPosition(ccp(imageView_bg->getSize().width/2,imageView_bg->getSize().height/2));
		//close Button
		UIButton* btn_close = UIButton::create();
		btn_close->setTextures("res_ui/close.png","res_ui/close.png","");
		btn_close->setTouchEnable(true);
		btn_close->setPressedActionEnabled(true);
		btn_close->setAnchorPoint(ccp(0.5f,0.5f));
		btn_close->setPosition(ccp(imageView_bg->getContentSize().width-5,imageView_bg->getContentSize().height-35));
		btn_close->addReleaseEvent(this,coco_releaseselector(NewFunctionRemindUI::CloseEvent));
		m_pUiLayer->addWidget(btn_close);

		//new function icon
		int i = 0;
		std::map<int, std::string>::iterator it = newFunctionList.begin();
		for ( ; it != newFunctionList.end(); ++ it )
		{
			int id = it->first;
			std::string icon = it->second;

			UIButton * btn_newFunction_light = UIButton::create();
			btn_newFunction_light->setTextures("gamescene_state/zhujiemian3/newFunction/greenLight.png","gamescene_state/zhujiemian3/newFunction/greenLight.png","");
			btn_newFunction_light->setScale(0.9f);
			btn_newFunction_light->setTouchEnable(true);
			btn_newFunction_light->setPressedActionEnabled(true);
			btn_newFunction_light->addReleaseEvent(this,coco_releaseselector(NewFunctionRemindUI::BtnEvent));
			btn_newFunction_light->setAnchorPoint(ccp(0.5f,0.5f));
			float space = (227 - newFunctionList.size()*btn_newFunction_light->getContentSize().width*btn_newFunction_light->getScale())*1.0f/(newFunctionList.size()+1);
			btn_newFunction_light->setPosition(ccp(25+space*(i+1)+btn_newFunction_light->getContentSize().width*btn_newFunction_light->getScale()*(0.5f+i*1.0f),48));
			btn_newFunction_light->setTag(200+id);
			
			m_pUiLayer->addWidget(btn_newFunction_light);

			std::string rootPath = "gamescene_state/zhujiemian3/newFunction/";
			rootPath.append(icon);
			rootPath.append(".png");
			UIImageView * imageView_newFunction = UIImageView::create();
			imageView_newFunction->setTexture(rootPath.c_str());
			imageView_newFunction->setAnchorPoint(ccp(0.5f,0.5f));
			imageView_newFunction->setPosition(ccp(0,-1));
			btn_newFunction_light->addChild(imageView_newFunction);

			i++;
		}

		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);
		this->setContentSize(imageView_bg->getSize());
		this->setTouchPriority(TPriority_ToolTips);
		m_pUiLayer->setTouchPriority(TPriority_ToolTips-1);
		//this->schedule(schedule_selector(NewFunctionRemindUI::update),1.0f);

		return true;
	}
	return false;
}
void NewFunctionRemindUI::update( float dt )
{
	time++;
	if (time >= 3)
	{
		time = 0;
		this->closeAnim();
	}
}

void NewFunctionRemindUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void NewFunctionRemindUI::onExit()
{
	UIScene::onExit();
}

bool NewFunctionRemindUI::ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent)
{
	// convert the touch point to OpenGL coordinates
// 	CCPoint location = pTouch->getLocation();
// 
// 	CCPoint pos = this->getPosition();
// 	CCSize size = this->getContentSize();
// 	CCPoint anchorPoint = this->getAnchorPointInPoints();
// 	pos = ccpSub(pos, anchorPoint);
// 	CCRect rect(pos.x, pos.y, size.width, size.height);
// 	if(rect.containsPoint(location))
// 	{
// 		return true;
// 	}
// 
// 	return false;
	//return this->resignFirstResponder(pTouch,this,false);
	return true;
}
void NewFunctionRemindUI::ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent)
{

}
void NewFunctionRemindUI::ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent)
{

}
void NewFunctionRemindUI::ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent)
{

}

void NewFunctionRemindUI::CloseEvent( CCObject *pSender )
{
	this->closeAnim();
}

void NewFunctionRemindUI::BtnEvent( CCObject *pSender )
{
	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();

	UIButton * btn_cur = dynamic_cast<UIButton*>(pSender);
	if (btn_cur)
	{
		int curID = btn_cur->getTag()-200;
		switch(curID)
		{
		case 4 :
			{
				CCSize s = CCDirector::sharedDirector()->getVisibleSize();
				if (!MainScene::GeneralsScene)
					return;

				if(MainScene::GeneralsScene->getParent() != NULL)
				{
					MainScene::GeneralsScene->removeFromParentAndCleanup(false);
				}
				GeneralsUI * generalsUI = (GeneralsUI *)MainScene::GeneralsScene;
				GameView::getInstance()->getMainUIScene()->addChild(generalsUI,0,kTagGeneralsUI);
				generalsUI->ignoreAnchorPointForPosition(false);
				generalsUI->setAnchorPoint(ccp(0.5f, 0.5f));
				generalsUI->setPosition(ccp(s.width/2, s.height/2));

				MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
				//招募
				for (int i = 0;i<3;++i)
				{
					if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50+i))
					{
						RecuriteActionItem * tempRecurite = ( RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50+i);
						tempRecurite->cdTime -= GameUtils::millisecondNow() - tempRecurite->lastChangeTime;
					}
				}

				//generalsUI->setToDefaultTab();
				generalsUI->setToTabByIndex(1);

				//武将列表 
				GeneralsUI::generalsListUI->isReqNewly = true;
				GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
				temp->page = 0;
				temp->pageSize = generalsUI->generalsListUI->everyPageNum;
				temp->type = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
				delete temp;
				//技能列表
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5055, this);
				//阵法列表
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5071, this);
				//已上阵武将列表 
				GeneralsUI::ReqListParameter * temp1 = new GeneralsUI::ReqListParameter();
				temp1->page = 0;
				temp1->pageSize = generalsUI->generalsListUI->everyPageNum;
				temp1->type = 5;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp1);
				delete temp1;

				mainScene->remindOfGeneral();
			}
			break;
		case 7 :
			{
				CCSize s = CCDirector::sharedDirector()->getVisibleSize();
				if (!MainScene::GeneralsScene)
					return;

				if(MainScene::GeneralsScene->getParent() != NULL)
				{
					MainScene::GeneralsScene->removeFromParentAndCleanup(false);
				}
				GeneralsUI * generalsUI = (GeneralsUI *)MainScene::GeneralsScene;
				GameView::getInstance()->getMainUIScene()->addChild(generalsUI,0,kTagGeneralsUI);
				generalsUI->ignoreAnchorPointForPosition(false);
				generalsUI->setAnchorPoint(ccp(0.5f, 0.5f));
				generalsUI->setPosition(ccp(s.width/2, s.height/2));

				MainScene * mainScene = (MainScene*)GameView::getInstance()->getMainUIScene();
				//招募
				for (int i = 0;i<3;++i)
				{
					if (GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50+i))
					{
						RecuriteActionItem * tempRecurite = ( RecuriteActionItem *)GeneralsUI::generalsRecuriteUI->Layer_recruit->getChildByTag(50+i);
						tempRecurite->cdTime -= GameUtils::millisecondNow() - tempRecurite->lastChangeTime;
					}
				}

				//generalsUI->setToDefaultTab();
				generalsUI->setToTabByIndex(1);

				//武将列表 
				GeneralsUI::generalsListUI->isReqNewly = true;
				GeneralsUI::ReqListParameter * temp = new GeneralsUI::ReqListParameter();
				temp->page = 0;
				temp->pageSize = generalsUI->generalsListUI->everyPageNum;
				temp->type = 1;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp);
				delete temp;
				//技能列表
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5055, this);
				//阵法列表
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5071, this);
				//已上阵武将列表 
				GeneralsUI::ReqListParameter * temp1 = new GeneralsUI::ReqListParameter();
				temp1->page = 0;
				temp1->pageSize = generalsUI->generalsListUI->everyPageNum;
				temp1->type = 5;
				GameMessageProcessor::sharedMsgProcessor()->sendReq(5051,temp1);
				delete temp1;

				mainScene->remindOfGeneral();
			}
			break;
		case 10 :
			{
				MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
				CCLayer *skillScene = (CCLayer*)mainscene->getChildByTag(KTagSkillScene);
				if(skillScene == NULL)
				{
					skillScene = SkillScene::create();
					GameView::getInstance()->getMainUIScene()->addChild(skillScene,0,KTagSkillScene);
					skillScene->ignoreAnchorPointForPosition(false);
					skillScene->setAnchorPoint(ccp(0.5f, 0.5f));
					skillScene->setPosition(ccp(winSize.width/2, winSize.height/2));
				}
				mainscene->remindOfSkill();
			}
			break;
		case 12:
			{
				MainScene* mainUILayer = dynamic_cast<MainScene*>(GameView::getInstance()->getMainUIScene());
				CCAssert(mainUILayer != NULL, "should not be nil");
				
				CCLayer *worldBossUI = (CCLayer*)mainUILayer->getChildByTag(kTagWorldBossUI);
				if(worldBossUI == NULL)
				{
					worldBossUI = WorldBossUI::create();
					GameView::getInstance()->getMainUIScene()->addChild(worldBossUI,0,kTagWorldBossUI);
					worldBossUI->ignoreAnchorPointForPosition(false);
					worldBossUI->setAnchorPoint(ccp(0.5f, 0.5f));
					worldBossUI->setPosition(ccp(winSize.width/2, winSize.height/2));

					GameMessageProcessor::sharedMsgProcessor()->sendReq(5301);
				}
			}
			break;
		case 13 :
			{
				MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
				CCLayer *skillScene = (CCLayer*)mainscene->getChildByTag(KTagSkillScene);
				if(skillScene == NULL)
				{
					skillScene = SkillScene::create();
					GameView::getInstance()->getMainUIScene()->addChild(skillScene,0,KTagSkillScene);
					skillScene->ignoreAnchorPointForPosition(false);
					skillScene->setAnchorPoint(ccp(0.5f, 0.5f));
					skillScene->setPosition(ccp(winSize.width/2, winSize.height/2));
				}
				mainscene->remindOfSkill();
			}
			break;
		case 14 :
			{
				MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
				CCLayer *skillScene = (CCLayer*)mainscene->getChildByTag(KTagSkillScene);
				if(skillScene == NULL)
				{
					skillScene = SkillScene::create();
					GameView::getInstance()->getMainUIScene()->addChild(skillScene,0,KTagSkillScene);
					skillScene->ignoreAnchorPointForPosition(false);
					skillScene->setAnchorPoint(ccp(0.5f, 0.5f));
					skillScene->setPosition(ccp(winSize.width/2, winSize.height/2));
				}
				mainscene->remindOfSkill();
			}
			break;
		case 15 :
			{
				MainScene *mainscene = (MainScene *)GameView::getInstance()->getMainUIScene();
				CCLayer *skillScene = (CCLayer*)mainscene->getChildByTag(KTagSkillScene);
				if(skillScene == NULL)
				{
					skillScene = SkillScene::create();
					GameView::getInstance()->getMainUIScene()->addChild(skillScene,0,KTagSkillScene);
					skillScene->ignoreAnchorPointForPosition(false);
					skillScene->setAnchorPoint(ccp(0.5f, 0.5f));
					skillScene->setPosition(ccp(winSize.width/2, winSize.height/2));
				}
				mainscene->remindOfSkill();
			}
			break;
		}
	}
	this->closeAnim();
}
