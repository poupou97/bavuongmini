#include "GeneralsModeUI.h"
#include "../../messageclient/GameMessageProcessor.h"
#include "../../GameView.h"
#include "../../utils/StaticDataManager.h"


GeneralsModeUI::GeneralsModeUI(void)
{
}


GeneralsModeUI::~GeneralsModeUI(void)
{
}

GeneralsModeUI * GeneralsModeUI::create(int generalMode,int autoFight)
{
	GeneralsModeUI * generalsModeUI = new GeneralsModeUI();
	if (generalsModeUI && generalsModeUI->init(generalMode,autoFight))
	{
		generalsModeUI->autorelease();
		return generalsModeUI;
	}
	CC_SAFE_DELETE(generalsModeUI);
	return NULL;
}

bool GeneralsModeUI::init(int generalMode,int autoFight)
{
	if (UIScene::init())
	{
		//武将模式
		m_generalMode = generalMode;
		//武将自动出战
		m_autoFight = autoFight;

		CCSize winsize = CCDirector::sharedDirector()->getVisibleSize();

		UIPanel * ppanel = (UIPanel *)GUIReader::shareReader()->widgetFromJsonFile("res_ui/wujiangmoshi_1.json");
		ppanel->setAnchorPoint(ccp(0.0f,0.0f));
		ppanel->setPosition(CCPointZero);	
		ppanel->setTouchEnable(true);
		m_pUiLayer->addWidget(ppanel);

		//开启主动
		Button_driving = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_zhudong");
		Button_driving->setTouchEnable(true);
		Button_driving->setPressedActionEnabled(true);
		Button_driving->addReleaseEvent(this, coco_releaseselector(GeneralsModeUI::DrivingEvent));
		//开启协助
		Button_assistance = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_xiezhu");
		Button_assistance->setTouchEnable(true);
		Button_assistance->setPressedActionEnabled(true);
		Button_assistance->addReleaseEvent(this, coco_releaseselector(GeneralsModeUI::AssistanceEvent));
		//开启出战
		Button_beginAutoFight = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_chuzhan");
		Button_beginAutoFight->setTouchEnable(true);
		Button_beginAutoFight->setPressedActionEnabled(true);
		Button_beginAutoFight->addReleaseEvent(this, coco_releaseselector(GeneralsModeUI::BeginAutoFightEvent));
		//结束出战
		Button_endAutoFight = (UIButton*)UIHelper::seekWidgetByName(ppanel,"Button_buchuzhan");
		Button_endAutoFight->setTouchEnable(true);
		Button_endAutoFight->setPressedActionEnabled(true);
		Button_endAutoFight->addReleaseEvent(this, coco_releaseselector(GeneralsModeUI::EndAutoFightEvent));

		applyGeneralMode(generalMode,autoFight);

		this->setContentSize(ppanel->getContentSize());
		this->setTouchEnabled(true);
		this->setTouchMode(kCCTouchesOneByOne);

		return true;
	}
	return false;
}


void GeneralsModeUI::onEnter()
{
	UIScene::onEnter();
	this->openAnim();
}

void GeneralsModeUI::onExit()
{
	UIScene::onExit();
}


bool GeneralsModeUI::ccTouchBegan(CCTouch *touch, CCEvent * pEvent)
{
	return this->resignFirstResponder(touch,this,false);
}

void GeneralsModeUI::ccTouchEnded(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralsModeUI::ccTouchCancelled(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralsModeUI::ccTouchMoved(CCTouch *touch, CCEvent * pEvent)
{
}

void GeneralsModeUI::DrivingEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1206,(void *)2,(void *)m_autoFight);
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("general_openAutoAttack"));
}

void GeneralsModeUI::AssistanceEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1206,(void *)1,(void *)m_autoFight);
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("general_closeAutoAttack"));
}

void GeneralsModeUI::BeginAutoFightEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1206,(void *)m_generalMode,(void *)1);
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("general_openAutoOut"));
}

void GeneralsModeUI::EndAutoFightEvent( CCObject *pSender )
{
	GameMessageProcessor::sharedMsgProcessor()->sendReq(1206,(void *)m_generalMode,(void *)2);
	GameView::getInstance()->showAlertDialog(StringDataManager::getString("general_closeAutoOut"));
}


void GeneralsModeUI::applyGeneralMode( int idx,int autoFight )
{
	m_generalMode = idx;
	m_autoFight = autoFight;

	switch(idx)
	{
	case 1:   //跟随
		{
			Button_driving->setVisible(true);
			Button_assistance->setVisible(false);
		}
		break;
	case 2:   //攻击
		{
			Button_driving->setVisible(false);
			Button_assistance->setVisible(true);
		}
		break;
	case 3:  //休息
		{
			Button_driving->setVisible(true);
			Button_assistance->setVisible(false);
		}
		break;
	}

	switch(autoFight)
	{
	case 1:   //自动
		{
			Button_beginAutoFight->setVisible(false);
			Button_endAutoFight->setVisible(true);
		}
		break;
	case 2:   //手动
		{
			Button_beginAutoFight->setVisible(true);
			Button_endAutoFight->setVisible(false);
		}
		break;
	}
}