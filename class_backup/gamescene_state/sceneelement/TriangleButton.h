#ifndef _UTILS_TRIANGLENODE_H
#define _UTILS_TRIANGLENODE_H

#include "cocos2d.h"
#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class TriangleButton :public UIScene
{
public:
	TriangleButton(void);
	~TriangleButton(void);

	static TriangleButton* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);

	void FrameEvent(CCObject *pSender);
	void setAllOpen();

public:
	UIImageView * imageView_arrow;

	bool isOpen;
};

#endif

