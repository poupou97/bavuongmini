
#ifndef _GAMESCENESTATE_BUFFCELL_H_
#define _GAMESCENESTATE_BUFFCELL_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"

USING_NS_CC;
USING_NS_CC_EXT;

class ExStatus;
class ExStatusItem;
class CCMoveableMenu;
class MissionInfo;

class BuffCell : public CCTableViewCell
{
public:
	BuffCell();
	~BuffCell();
	static BuffCell* create(ExStatus * exstatus);
	bool init(ExStatus * exstatus);

	virtual void update(float delta);

private:
	ExStatusItem * exStatusItem;
	CCSprite * Sprite_icon;
	CCLabelTTF * num;
	CCLabelTTF * name;
	CCLabelTTF * time;
	int m_nRemainTime;

	ExStatus * curExstatus;

	std::string ConvertToDateFormat(int t);
};

#endif;