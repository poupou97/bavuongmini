#include "ExStatusItem.h"
#include "../exstatus/ExStatus.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"
#include "GameView.h"


ExStatusItem::ExStatusItem():
mCDTime_skill(0)
{
}


ExStatusItem::~ExStatusItem()
{
	delete curExstatus;
}

ExStatusItem * ExStatusItem::create(ExStatus* exstatus)
{
	ExStatusItem *exStatusItem = new ExStatusItem();
	if (exStatusItem && exStatusItem->init(exstatus))
	{
		exStatusItem->autorelease();
		return exStatusItem;
	}
	CC_SAFE_DELETE(exStatusItem);
	return NULL;
}

bool ExStatusItem::init(ExStatus* exstatus)
{
	if (UIScene::init())
	{
		curExstatus = new ExStatus();
		curExstatus->CopyFrom(*exstatus);
		if (exstatus->has_remaintime())
		{
			mCDTime_skill = exstatus->remaintime()/1000;
		}
		else
		{
			mCDTime_skill = 0;
		}
		
		CCSize screenSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();

		ExStatus * exStatusFromDB = ExStatusConfigData::s_exstatusData[exstatus->type()];
		CCAssert(exStatusFromDB != NULL, "exStatusFromDB should not be nil");

		std::string str_iconPath = "res_ui/buff/" ;
		str_iconPath.append(exStatusFromDB->getIcon());
		str_iconPath.append(".png");
		CCSprite * sprite = CCSprite::create(str_iconPath.c_str());
		sprite->setAnchorPoint(CCPointZero);
		sprite->setPosition(CCPointZero);
		addChild(sprite);

		if ((!exstatus->has_duration()) || exstatus->duration() == 0)
		{

		}
		else
		{
			if (mCDTime_skill > 0)
			{
				CCSprite *progress_skill = CCSprite::create("gamescene_state/zhujiemian3/zhujuetouxiang/buffmengban.png");
				CCProgressTimer *mProgressTimer_skill = CCProgressTimer::create(progress_skill);
				mProgressTimer_skill->setAnchorPoint(ccp(0.5f, 0.5f));
				mProgressTimer_skill->setPosition(ccp(sprite->getContentSize().width/2,sprite->getContentSize().height/2));
				mProgressTimer_skill->setVisible(true);
				addChild(mProgressTimer_skill);

				CCCallFunc* action_callback;
				mProgressTimer_skill->setVisible(true);
				mProgressTimer_skill->setType(kCCProgressTimerTypeRadial);
				mProgressTimer_skill->setReverseProgress(true); // 设置进度条为逆时针

				float _scale = exstatus->remaintime()*100.f/exstatus->duration();
				mProgressTimer_skill->setPercentage(_scale);
				action_progress_from_to = CCProgressFromTo::create(mCDTime_skill, _scale, 0);     
				action_callback = CCCallFuncN::create(this, callfuncN_selector(ExStatusItem::skillCoolDownCallBack));
				mProgressTimer_skill->runAction(CCSequence::create(action_progress_from_to, action_callback, NULL));
			}
		}

		this->setContentSize(sprite->getContentSize());
		this->schedule(schedule_selector(ExStatusItem::update),1.0f);
		return true;
	}
	return false;
}

void ExStatusItem::skillCoolDownCallBack( CCNode* node )
{
	//mProgressTimer_skill->setVisible(false);
}

void ExStatusItem::update( float dt )
{
	curExstatus->set_remaintime(curExstatus->remaintime()-1000);
	for(int i = 0;i<GameView::getInstance()->myplayer->getExStatusVector().size();++i)
	{
		ExStatus * exStatus = GameView::getInstance()->myplayer->getExStatusVector().at(i);
		if (exStatus->type() == curExstatus->type() && exStatus->sourceid() == curExstatus->sourceid())
		{
			exStatus->set_remaintime(curExstatus->remaintime());
			break;
		}
	}

	if(curExstatus->remaintime()/1000 <= 0)
	{
		curExstatus->set_remaintime(0);
	}
}
