#include "TriangleButton.h"
#include "GameView.h"
#include "../MainScene.h"
#include "GeneralsInfoUI.h"
#include "../../utils/Joystick.h"
#include "shortcutelement/ShortcutLayer.h"
#include "../../utils/StaticDataManager.h"
#include "../../gamescene_state/role/MyPlayer.h"


#define kTag_Action_Move 10

TriangleButton::TriangleButton(void):
isOpen(true)
{
}


TriangleButton::~TriangleButton(void)
{
}

bool TriangleButton::ccTouchBegan( CCTouch *pTouch, CCEvent *pEvent )
{
	return false;
}

void TriangleButton::ccTouchEnded( CCTouch *pTouch, CCEvent *pEvent )
{
	CCLOG("ccTouchEnd");
}

TriangleButton* TriangleButton::create()
{
	TriangleButton* pRet = new TriangleButton();
	if (pRet && pRet->init())
	{
		pRet->autorelease();
		return pRet;
	}
	
	CC_SAFE_DELETE(pRet);
	return NULL;
}

bool TriangleButton::init()
{
	if (UIScene::init())
	{
		UIButton * btn_frame = UIButton::create();
		btn_frame->setTextures("gamescene_state/zhujiemian3/jingyantiao/ssuu.png","gamescene_state/zhujiemian3/jingyantiao/ssuu.png","");
		btn_frame->setAnchorPoint(ccp(0.5f,0.5f));
		btn_frame->setPosition(ccp(btn_frame->getContentSize().width/2,btn_frame->getContentSize().height/2));
		btn_frame->setTouchEnable(true);
		btn_frame->setPressedActionEnabled(true);
		btn_frame->addReleaseEvent(this,coco_releaseselector(TriangleButton::FrameEvent));
		m_pUiLayer->addWidget(btn_frame);

		imageView_arrow = UIImageView::create();
		imageView_arrow->setTexture("gamescene_state/zhujiemian3/renwuduiwu/arrow.png");
		imageView_arrow->setAnchorPoint(ccp(0.5f,0.5f));
		imageView_arrow->setPosition(ccp(5,-5));
		imageView_arrow->setScale(0.5f);
		imageView_arrow->setRotation(45);
		btn_frame->addChild(imageView_arrow);

		this->setContentSize(btn_frame->getContentSize());
		this->setTouchEnabled(false);

		return true;
	}
	return false;
}

void TriangleButton::onEnter()
{
	UIScene::onEnter();
}

void TriangleButton::onExit()
{
	UIScene::onExit();
}

void TriangleButton::FrameEvent( CCObject *pSender )
{
	//开启等级
	int openlevel = 0;
	std::map<int,int>::const_iterator cIter;
	cIter = BtnOpenLevelConfigData::s_btnOpenLevel.find(22);
	if (cIter == BtnOpenLevelConfigData::s_btnOpenLevel.end()) // 没找到就是指向END了  
	{
	}
	else
	{
		openlevel = BtnOpenLevelConfigData::s_btnOpenLevel[22];
	}

	if (openlevel > GameView::getInstance()->myplayer->getActiveRole()->level())
	{
		char s_des [200];
		const char* str_des = StringDataManager::getString("shrinkage_will_be_open");
		sprintf(s_des,str_des,openlevel);
		GameView::getInstance()->showAlertDialog(s_des);
		return;
	}

	MainScene * maiScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	if (!isOpen)
	{
		CCLayer * shortcutLayer = (CCLayer*)maiScene->getChildByTag(kTagShortcutLayer);
		if (shortcutLayer)
		{
			if (shortcutLayer->getActionByTag(kTag_Action_Move) == NULL)
			{
				CCMoveTo * moveLeft = CCMoveTo::create(0.3f,ccp(shortcutLayer->getPositionX()-200,shortcutLayer->getPositionY()));
				shortcutLayer->runAction(moveLeft);
				moveLeft->setTag(kTag_Action_Move);

				isOpen = true;
				imageView_arrow->setRotation(45);
				imageView_arrow->setPosition(ccp(5,-5));
			}
		}
		//generalInfoUI down
		GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI*)maiScene->getGeneralInfoUI();
		if (generalsInfoUI)
		{
			if (generalsInfoUI->getActionByTag(kTag_Action_Move) == NULL)
			{
				CCMoveTo * moveUp = CCMoveTo::create(0.3f,ccp(generalsInfoUI->getPositionX(),0));
				generalsInfoUI->runAction(moveUp);
				moveUp->setTag(kTag_Action_Move);
			}
		}
		//摇杆
		Joystick* pJoystick = (Joystick*)maiScene->getChildByTag(kTagVirtualJoystick);
		if(pJoystick)
		{
			if (pJoystick->getActionByTag(kTag_Action_Move) == NULL)
			{
				CCMoveTo * moveRight = CCMoveTo::create(0.3f,ccp(pJoystick->getPositionX()+200,pJoystick->getPositionY()));
				pJoystick->runAction(moveRight);
				moveRight->setTag(kTag_Action_Move);

				pJoystick->Active();
			}
		}
	}	
	else
	{
		CCLayer * shortcutLayer = (CCLayer*)maiScene->getChildByTag(kTagShortcutLayer);
		if (shortcutLayer)
		{
			if (shortcutLayer->getActionByTag(kTag_Action_Move) == NULL)
			{
				CCMoveTo * moveRight = CCMoveTo::create(0.3f,ccp(shortcutLayer->getPositionX()+200,shortcutLayer->getPositionY()));
				shortcutLayer->runAction(moveRight);
				moveRight->setTag(kTag_Action_Move);

				isOpen = false;
				imageView_arrow->setRotation(-135);
				imageView_arrow->setPosition(ccp(4,-4));
			}
		}
		//generalInfoUI down
		GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI*)maiScene->getGeneralInfoUI();
		if (generalsInfoUI)
		{
			if (generalsInfoUI->getActionByTag(kTag_Action_Move) == NULL)
			{
				CCMoveTo * moveDown = CCMoveTo::create(0.3f,ccp(generalsInfoUI->getPositionX(),-105));
				generalsInfoUI->runAction(moveDown);
				moveDown->setTag(kTag_Action_Move);
			}
		}
		//摇杆
		Joystick* pJoystick = (Joystick*)maiScene->getChildByTag(kTagVirtualJoystick);
		if(pJoystick)
		{
			if (pJoystick->getActionByTag(kTag_Action_Move) == NULL)
			{
				CCMoveTo * moveLeft = CCMoveTo::create(0.3f,ccp(pJoystick->getPositionX()-200,pJoystick->getPositionY()));
				pJoystick->runAction(moveLeft);
				moveLeft->setTag(kTag_Action_Move);

				pJoystick->Inactive();
			}
		}
	}
}

void TriangleButton::setAllOpen()
{
	isOpen = true;
	imageView_arrow->setRotation(45);
	imageView_arrow->setPosition(ccp(5,-5));

	CCSize winSize = CCDirector::sharedDirector()->getVisibleSize();
	MainScene * maiScene = (MainScene*)GameView::getInstance()->getMainUIScene();
	ShortcutLayer * shortcutLayer = (ShortcutLayer*)maiScene->getChildByTag(kTagShortcutLayer);
	if (shortcutLayer)
	{
		shortcutLayer->setPosition(ccp(winSize.width/2,winSize.height/2));
	}
	//generalInfoUI down
	GeneralsInfoUI * generalsInfoUI = (GeneralsInfoUI*)maiScene->getGeneralInfoUI();
	if (generalsInfoUI)
	{
		generalsInfoUI->setPosition(ccp(winSize.width/2,0));
	}
	//摇杆
	Joystick* pJoystick = (Joystick*)maiScene->getChildByTag(kTagVirtualJoystick);
	if(pJoystick)
	{
		pJoystick->setPosition(ccp(0,pJoystick->getPositionY()));
	}
}
