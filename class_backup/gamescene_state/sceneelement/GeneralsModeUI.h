
#ifndef _GAMESCENESTATE_GENERALSMODEUI_H_
#define _GAMESCENESTATE_GENERALSMODEUI_H_

#include "cocos-ext.h"
#include "cocos2d.h"
#include "../../ui/extensions/UIScene.h"

USING_NS_CC;
USING_NS_CC_EXT;

class GeneralsModeUI : public UIScene
{
public:
	GeneralsModeUI(void);
	~GeneralsModeUI(void);

	static GeneralsModeUI * create(int generalMode,int autoFight);
	bool init(int generalMode,int autoFight);

	virtual void onEnter();
	virtual void onExit();
	virtual bool ccTouchBegan(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchEnded(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchCancelled(CCTouch *touch, CCEvent * pEvent);
	virtual void ccTouchMoved(CCTouch *touch, CCEvent * pEvent);

	void DrivingEvent(CCObject *pSender);
	void AssistanceEvent(CCObject *pSender);
	void BeginAutoFightEvent(CCObject *pSender);
	void EndAutoFightEvent(CCObject *pSender);

	void applyGeneralMode( int idx,int autoFight );

private:
	//武将模式
	int m_generalMode;
	//武将自动出战
	int m_autoFight;

	UIButton * Button_driving;
	UIButton * Button_assistance;
	UIButton * Button_beginAutoFight;
	UIButton * Button_endAutoFight;
};

#endif