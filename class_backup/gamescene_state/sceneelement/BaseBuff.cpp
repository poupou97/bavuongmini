#include "BaseBuff.h"


BaseBuff::BaseBuff()
{
}


BaseBuff::~BaseBuff()
{
}


BaseBuff * BaseBuff::create()
{
	BaseBuff * baseBuff = new BaseBuff();
	if (baseBuff && baseBuff->init())
	{
		baseBuff->autorelease();
		return baseBuff;
	}
	CC_SAFE_DELETE(baseBuff);
	return NULL;
}

bool BaseBuff::init()
{
	if (UIWidget::init())
	{
		CCSize visibleSize = CCDirector::sharedDirector()->getVisibleSize();
		CCPoint origin = CCDirector::sharedDirector()->getVisibleOrigin();
		
		/*UIImageView * ImageView_background = UIImageView::create();
		ImageView_background->setTouchEnable(true);
		ImageView_background->setScale9Enable(true);
		ImageView_background->setTexture("ChatUiRes/LV4_right_di.png");
		ImageView_background->setScale9Size(CCSizeMake(400, 80));
		ImageView_background->setAnchorPoint(CCPointZero);
		ImageView_background->setPosition(ccp(0,0));
		m_pUiLayer->addWidget(ImageView_background);*/

		UIButton * Button_test = UIButton::create();
		Button_test->setTouchEnable(true);
		Button_test->setTextures("LV4_right_di.png", "LV4_right_di.png", "");
		Button_test->setAnchorPoint(ccp(0,0));
		Button_test->setPosition(ccp(0,0));
		Button_test->addReleaseEvent(this, coco_releaseselector(BaseBuff::BuffInfo));
		this->addChild(Button_test);

		this->setAnchorPoint(ccp(0,0));
		this->setPosition(ccp(0,0));
		
		return true;
	}
	return false;
}

void BaseBuff::BuffInfo( CCObject * pSender )
{
	//CCLog("create BuffInfo");
}

