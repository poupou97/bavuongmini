
#ifndef _GAMESCENESTATE_MYHEADINFO_H_
#define _GAMESCENESTATE_MYHEADINFO_H_

#include "cocos-ext.h"
#include "../../ui/extensions/uiscene.h"
#include "cocos2d.h"
#include "../../legend_script/ScriptHandlerProtocol.h"
#include "../../legend_script/CCTutorialIndicator.h"

USING_NS_CC;
USING_NS_CC_EXT;

class MainScene;
class UITab;

class MyHeadInfo : public UIScene, public ScriptHandlerProtocol
{
public:
	MyHeadInfo();
	~MyHeadInfo();
	static MyHeadInfo* create();
	bool init();

	virtual void onEnter();
	virtual void onExit();

	virtual bool ccTouchBegan(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchMoved(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchEnded(CCTouch *pTouch, CCEvent *pEvent);
	virtual void ccTouchCancelled(CCTouch *pTouch, CCEvent *pEvent);

	UIImageView * ImageView_vipLv;
	UILabel * Label_allBlood;
	UILabel * Label_curBlood;
	UILabel * Label_allMagic;
	UILabel * Label_curMagic;
	UILabel * Label_allPhy;
	UILabel * Label_curPhy;
	UILabel * Label_name;
	UILabel * Label_level;
	UIButton * Button_headFrame;
	UIImageView * ImageView_blood;
	UIImageView * ImageView_magic;
	UIImageView * ImageView_phy;

	UIImageView * upBuff;
	UIImageView * downBuff;

	UIImageView * ImageView_Vip;
	UILabelBMFont * lbt_vipLevel;

	UITab * Tab_state;
	bool isTabStateOn;

	UIButton *Button_state;
	int pkModeOpenLevel;

	void applyPKMode(int PKMode);
	void TabStateIndexChangedEvent(CCObject* pSender);
	void ButtonStateEvent(CCObject *pSender);

	void ReloadMyPlayerData();
	void ReloadBuffData();
	void ButtonHeadEvent(CCObject *pSender);
	void ButtonFrameEvent(CCObject *pSender);
	void ButtonLVTeamEvent(CCObject *pSender);
	void ReloadVipInfo();

	/////////team button (change by liuzhenxing)
	void callBackExitTeam(CCObject * obj);
	UIButton * teamInLine;

	//教学
	virtual void registerScriptCommand(int scriptId);
	void addCCTutorialIndicator(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	//和平
	void addCCTutorialIndicator1(const char* content,CCPoint pos,CCTutorialIndicator::Direction direction);
	void removeCCTutorialIndicator();

private:
	MainScene * mainScene;
	UIImageView * stateBG;

	UILayer * layer_pkmode;
	UILayer * layer_headIndo;
	//教学
	int mTutorialScriptInstanceId;
};

#endif;

